//
// cranimation/framedata.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "framedata.h"

#include "atl/array_struct.h"
#include "data/safestruct.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

const char *GetTrackName(u8 track)
{
	const char *szTrackName = NULL;

	switch(track)
	{
	case kTrackBoneTranslation             : { szTrackName = "kTrackBoneTranslation"             ; } break;
	case kTrackBoneRotation                : { szTrackName = "kTrackBoneRotation"                ; } break;
	case kTrackBoneScale                   : { szTrackName = "kTrackBoneScale"                   ; } break;
	case kTrackBoneConstraint              : { szTrackName = "kTrackBoneConstraint"              ; } break;
	case kTrackVisibility                  : { szTrackName = "kTrackVisibility"                  ; } break;
	case kTrackMoverTranslation            : { szTrackName = "kTrackMoverTranslation"            ; } break;
	case kTrackMoverRotation               : { szTrackName = "kTrackMoverRotation"               ; } break;
	case kTrackCameraTranslation           : { szTrackName = "kTrackCameraTranslation"           ; } break;
	case kTrackCameraRotation              : { szTrackName = "kTrackCameraRotation"              ; } break;
	case kTrackCameraScale                 : { szTrackName = "kTrackCameraScale"                 ; } break;
	case kTrackCameraFocalLength           : { szTrackName = "kTrackCameraFocalLength"           ; } break;
	case kTrackCameraHorizontalFilmAperture: { szTrackName = "kTrackCameraHorizontalFilmAperture"; } break;
	case kTrackCameraAperture              : { szTrackName = "kTrackCameraAperture"              ; } break;
	case kTrackCameraFocalPoint            : { szTrackName = "kTrackCameraFocalPoint"            ; } break;
	case kTrackCameraFStop                 : { szTrackName = "kTrackCameraFStop"                 ; } break;
	case kTrackCameraFocusDistance         : { szTrackName = "kTrackCameraFocusDistance"         ; } break;
	case kTrackShaderFrameIndex            : { szTrackName = "kTrackShaderFrameIndex"            ; } break;
	case kTrackShaderSlideU                : { szTrackName = "kTrackShaderSlideU"                ; } break;
	case kTrackShaderSlideV                : { szTrackName = "kTrackShaderSlideV"                ; } break;
	case kTrackShaderRotateUV              : { szTrackName = "kTrackShaderRotateUV"              ; } break;
	case kTrackMoverScale                  : { szTrackName = "kTrackMoverScale"                  ; } break;
	case kTrackBlendShape                  : { szTrackName = "kTrackBlendShape"                  ; } break;
	case kTrackVisemes                     : { szTrackName = "kTrackVisemes"                     ; } break;
	case kTrackAnimatedNormalMaps          : { szTrackName = "kTrackAnimatedNormalMaps"          ; } break;
	case kTrackFacialControl               : { szTrackName = "kTrackFacialControl"               ; } break;
	case kTrackFacialTranslation           : { szTrackName = "kTrackFacialTranslation"           ; } break;
	case kTrackFacialRotation              : { szTrackName = "kTrackFacialRotation"              ; } break;
	case kTrackCameraFieldOfView           : { szTrackName = "kTrackCameraFieldOfView"           ; } break;
	case kTrackCameraDepthOfField          : { szTrackName = "kTrackCameraDepthOfField"          ; } break;
	case kTrackColor                       : { szTrackName = "kTrackColor"                       ; } break;
	case kTrackLightIntensity              : { szTrackName = "kTrackLightIntensity"              ; } break;
	case kTrackLightFallOff                : { szTrackName = "kTrackLightFallOff"                ; } break;
	case kTrackLightConeAngle              : { szTrackName = "kTrackLightConeAngle"              ; } break;
	case kTrackGenericControl              : { szTrackName = "kTrackGenericControl"              ; } break;
	case kTrackGenericTranslation          : { szTrackName = "kTrackGenericTranslation"          ; } break;
	case kTrackGenericRotation             : { szTrackName = "kTrackGenericRotation"             ; } break;
	case kTrackCameraDepthOfFieldStrength  : { szTrackName = "kTrackCameraDepthOfFieldStrength"  ; } break;
	case kTrackFacialScale                 : { szTrackName = "kTrackFacialScale"                 ; } break;
	case kTrackGenericScale                : { szTrackName = "kTrackGenericScale"                ; } break;
	case kTrackCameraShallowDepthOfField   : { szTrackName = "kTrackCameraShallowDepthOfField"   ; } break;
	case kTrackCameraMotionBlur            : { szTrackName = "kTrackCameraMotionBlur"            ; } break;
	case kTrackParticleData                : { szTrackName = "kTrackParticleData"                ; } break;
	case kTrackLightDirection              : { szTrackName = "kTrackLightDirection"              ; } break;
	}

	return szTrackName;
}

const char *GetFormatTypeName(u8 type)
{
	const char *szFormatTypeName = NULL;

	switch(type)
	{
	case kFormatTypeVector3   : { szFormatTypeName = "kFormatTypeVector3"   ; } break;
	case kFormatTypeQuaternion: { szFormatTypeName = "kFormatTypeQuaternion"; } break;
	case kFormatTypeFloat     : { szFormatTypeName = "kFormatTypeFloat"     ; } break;
	}

	return szFormatTypeName;
}

////////////////////////////////////////////////////////////////////////////////

crFrameData::crFrameData()
: m_RefCount(1)
, m_Signature(0)
, m_FrameBufferSize(0)
, m_NumDofs(0)
, m_Dofs(NULL)
, m_DofsBufferSize(0)
, m_DofsBufferOwner(true)
{
	CompileTimeAssert(!OffsetOf(crFrameData, m_Dofs));
}

////////////////////////////////////////////////////////////////////////////////

crFrameData::crFrameData(const crFrameData& src)
: m_RefCount(1)
, m_Signature(0)
, m_FrameBufferSize(0)
, m_NumDofs(0)
, m_Dofs(NULL)
, m_DofsBufferSize(0)
, m_DofsBufferOwner(true)
{
	*this = src;
}

////////////////////////////////////////////////////////////////////////////////

crFrameData::~crFrameData()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameData::Init(u8* dofsBuffer, u32 dofsBufferSize, bool dofsBufferOwner)
{
	Assert(!m_Dofs);
	Assert16(dofsBuffer);

	if(dofsBuffer)
	{
		Assert(dofsBufferSize > 0);

		m_Dofs = reinterpret_cast<Dof*>(dofsBuffer);
		m_DofsBufferOwner = dofsBufferOwner;
		Assign(m_DofsBufferSize, dofsBufferSize);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameData::Shutdown()
{
	m_Signature = 0;
	m_FrameBufferSize = 0;

	if(m_DofsBufferOwner)
	{
		delete [] (u8*)m_Dofs;
	}
	m_Dofs = NULL;

	m_DofsBufferSize = 0;
	m_DofsBufferOwner = true;

	m_NumDofs = 0;
}

////////////////////////////////////////////////////////////////////////////////

const crFrameData& crFrameData::operator=(const crFrameData& src)
{
	Shutdown();

	ReserveDofs(src.m_NumDofs);

	sysMemCpy(m_Dofs, src.m_Dofs, m_DofsBufferSize);
	sysMemCpy(m_NumTypes, src.m_NumTypes, sizeof(m_NumTypes));

	m_NumDofs = src.m_NumDofs;
	m_Signature = src.m_Signature;
	m_FrameBufferSize = src.m_FrameBufferSize;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameData::RemoveDof(u8 track, u16 id)
{
	u32 idx;
	if(FindDofIndex(track, id, idx))
	{
		m_Dofs[idx] = m_Dofs[--m_NumDofs];

		SortDofs();

		CalcSignature();
		CalcOffsets();

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameData::FastAddDof(u8 track, u16 id, u8 type, crFrameFilter* filter)
{
	float weight = 1.f;
	if(!filter || filter->FilterDof(track, id, weight))
	{
		FastAddDof(track, id, type);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameData::ReserveDofs(u32 numDofs, bool destroyExistingDofs)
{
	Assert(numDofs > 0);

	if(destroyExistingDofs)
	{
		m_NumDofs = 0;
		m_Signature = 0;
	}

	u32 newDofsBufferSize = CalcDofsBufferSize(numDofs);
	if(newDofsBufferSize > m_DofsBufferSize)
	{
		AssertMsg(m_DofsBufferOwner, "crFrameData::ReserveDofs - dof buffer insufficient, unable to reallocate as externally owned");
		if(m_DofsBufferOwner)
		{
			Dof* newDofs = reinterpret_cast<Dof*>(rage_aligned_new(16) u8[newDofsBufferSize]);
			if(m_Dofs)
			{
				if(!destroyExistingDofs)
				{
					Assert(m_NumDofs*sizeof(Dof) < newDofsBufferSize);
					sysMemCpy(newDofs, m_Dofs, m_NumDofs*sizeof(Dof));  // copy only the already configured
				}

				delete [] (u8*)m_Dofs;
			}

			m_Dofs = newDofs;
			Assign(m_DofsBufferSize, newDofsBufferSize);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameData::SortDofs(bool removeDuplicates)
{
	int numDofs = m_NumDofs;
	if(numDofs > 1)
	{
		std::sort(m_Dofs, m_Dofs+m_NumDofs);
		if(removeDuplicates)
		{
			for(int i=0; ++i != numDofs;)
			{
				int j = i-1;
				if(m_Dofs[i] == m_Dofs[j])
				{
					while(++i != numDofs)
					{
						if(m_Dofs[i] != m_Dofs[j])
						{
							m_Dofs[++j] = m_Dofs[i];					
						}
					}
					m_NumDofs = j+1;
					break;
				}
			}
		}
	}

	// regenerate signature and offsets
	m_Signature = 0;
	m_FrameBufferSize = 0;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameData::CalcSignature()
{
	// fletcher's checksum, cheap, but not the best choice: poor coverage over small byte range
	u32 idx = 0;
	u32 len = m_NumDofs;
	u32 sum1 = 0xffff, sum2 = 0xffff;

	while (len)
	{
		u32 tlen = len > 180 ? 180 : len;
		len -= tlen;
		do
		{
			const Dof& dof = m_Dofs[idx++];

			u16 data = dof.m_Track<<8 | dof.m_Type | 0x80;

			sum1 += data;
			sum2 += sum1;

			data = dof.m_Id;

			sum1 += data;
			sum2 += sum1;
		}
		while (--tlen);

		sum1 = (sum1 & 0xffff) + (sum1 >> 16);
		sum2 = (sum2 & 0xffff) + (sum2 >> 16);
	}

	sum1 = (sum1 & 0xffff) + (sum1 >> 16);
	sum2 = (sum2 & 0xffff) + (sum2 >> 16);

	// guarantees zero is not used, that represents no checksum
	m_Signature = (sum2 << 16) | sum1;
	Assert(m_Signature);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameData::CalcOffsets()
{
	static const u32 formatSizes[kFormatTypeNum] =  { sizeof(Vec3V), sizeof(QuatV), sizeof(f32) };

	// now pack the values, a pass for each alignment
	u32 offset = 0;
	u32 numDofs = m_NumDofs;
	for(u32 i=0; i<kFormatTypeNum; ++i)
	{
		const u32 a = formatSizes[i];

		u32 count = 0;
		for(u32 j=0; j<numDofs; ++j)
		{
			Dof& dof = m_Dofs[j];

			// set offset and increment alignment for each matching type
			if(dof.m_Type == i)
			{
				Assign(dof.m_Offset, offset);
				offset += a;
				count++;
			}
		}

		Assign(m_NumTypes[i], count);

		// add padding
		while(count % 4)
		{
			offset += a;
			count++;
		}
	}

	// set read and write only offsets
	offset += 2*sizeof(Vec4V);
	Assign(m_FrameBufferSize, offset);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
