//
// cranimation/vcrwidget.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "vcrwidget.h"

#include "bank/pane.h"
#include "system/xtl.h"

namespace rage {

//#############################################################################

#if __WIN32PC

static WNDPROC s_oldWindowEditMessageCallback;

static LRESULT CALLBACK WindowEditMessageCallback( HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam )
{
    crVCRWidget *vcrWidget = (crVCRWidget *)GetWindowLongPtr( hwnd, GWLP_USERDATA );

    switch ( iMsg )
    {
    case WM_CREATE:
        return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

    case WM_KEYDOWN:
        if ( wParam == VK_RETURN )
        {
            ::SetFocus( NULL );

            vcrWidget->ButtonPressed( vcrWidget->GetButtonInFocus() );

            return 0;
        }
        else if ( (wParam == VK_LEFT) || (wParam == VK_RIGHT) || (wParam == VK_UP) || (wParam == VK_DOWN)
            || (wParam == VK_HOME) || (wParam == VK_END) )
        {
            return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
        }
        else if ( wParam == VK_TAB )
        {
            if ( vcrWidget->GetButtonInFocus() + 1 >= crVCRWidget::NUM_BUTTONS )
            {
                vcrWidget->SetButtonInFocus( (crVCRWidget::Button)0, false );

                bkWidget *i = vcrWidget->GetNext();
                while ( i && !i->SetFocus() )
                {
                    i = i->GetNext();
                }

                if ( !i )
                {
                    i = vcrWidget->GetParent()->GetChild();
                    while ( i && !i->SetFocus() )
                    {
                        i = i->GetNext();
                    }
                }

                //Displayf( "Next control!" );
            }
            else
            {
                vcrWidget->SetButtonInFocus( (crVCRWidget::Button)(vcrWidget->GetButtonInFocus() + 1), true );
            }

            return 0;
        }

        return 0;

    case WM_SETFOCUS:
        vcrWidget->SetWindowInFocus( hwnd );
        //vcrWidget->ButtonPressed( vcrWidget->GetButtonInFocus() );
        ::SendMessage( hwnd, EM_SETSEL, 0, ~0 );
        return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

    default:
        return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
    }
}

#endif // __WIN32PC

//#############################################################################

crVCRWidget::crVCRWidget( const char *title, const char *memo, ButtonStyle buttonStyle, u32 enabledButtons, const char *fillColor, bool readOnly )
: bkWidget( NullCallback, title, memo, fillColor, readOnly )
, m_buttonStyle(buttonStyle)
, m_enabledButtons(enabledButtons)
, m_drawLocalExpanded(0)
{
    // add the handler if it hasn't been added all ready:
    static bool addedHandler = false;
    if ( addedHandler == false )
    {
        bkRemotePacket::AddType( GetStaticGuid(), RemoteHandler );
        addedHandler = true;
    }

#if __WIN32PC
    m_titleWindowHandle = NULL;

    for ( int i = 0; i < NUM_BUTTONS; ++i )
    {
        m_windowHandles[i] = NULL;
    }

    m_buttonInFocus = PAUSE;
#endif // __WIN32PC
}

crVCRWidget::~crVCRWidget()
{

}

int crVCRWidget::GetGuid() const { return GetStaticGuid(); }

bool crVCRWidget::SetFocus()
{
#if __WIN32PC
    if ( m_windowHandles[m_buttonInFocus] != NULL )
    {
        ::SetFocus( m_windowHandles[m_buttonInFocus] );
    }
#endif

    sm_Focus = this;
    return true;
}

void crVCRWidget::RemoteCreate()
{
    bkWidget::RemoteCreate();

    bkRemotePacket p;
    p.Begin( bkRemotePacket::CREATE, GetStaticGuid(), this );
    p.WriteWidget( m_Parent );
    p.Write_const_char( m_Title );
    p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool(IsReadOnly());
    p.Write_u32( m_buttonStyle );
    p.Write_u32( m_enabledButtons );
    p.Send();
}

void crVCRWidget::RemoteUpdate()
{
    // do nothing
}

#if __WIN32PC

void crVCRWidget::WindowCreate()
{
    if ( !bkRemotePacket::IsConnectedToRag() )
    {	
        m_titleWindowHandle = GetPane()->AddWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX | SS_NOTIFY );
        SetWindowText( m_titleWindowHandle, m_Title );

        m_windowHandles[REWIND_OR_GO_TO_START] = GetPane()->AddWindow( this, "BUTTON", 0, BS_PUSHBUTTON );
        m_windowHandles[GO_TO_PREVIOUS_OR_STEP_BACKWARD] = GetPane()->AddWindow( this, "BUTTON", 0, BS_PUSHBUTTON );
        m_windowHandles[PLAY_BACKWARDS] = GetPane()->AddWindow( this, "BUTTON", 0, BS_PUSHBUTTON );
        m_windowHandles[PAUSE] = GetPane()->AddWindow( this, "BUTTON", 0, BS_PUSHBUTTON );
        m_windowHandles[PLAY_FORWARDS] = GetPane()->AddWindow( this, "BUTTON", 0, BS_PUSHBUTTON );
        m_windowHandles[GO_TO_NEXT_OR_STEP_FORWARD] = GetPane()->AddWindow( this, "BUTTON", 0, BS_PUSHBUTTON );
        m_windowHandles[FAST_FORWARD_OR_GO_TO_END] = GetPane()->AddWindow( this, "BUTTON", 0, BS_PUSHBUTTON );

        SetWindowText( m_windowHandles[PLAY_BACKWARDS], "<" );
        SetWindowText( m_windowHandles[PAUSE], "||" );
        SetWindowText( m_windowHandles[PLAY_FORWARDS], ">" );

        if ( m_buttonStyle == VCR )
        {
            SetWindowText( m_windowHandles[REWIND_OR_GO_TO_START], "<<" );
            SetWindowText( m_windowHandles[GO_TO_PREVIOUS_OR_STEP_BACKWARD], "|<" );
            SetWindowText( m_windowHandles[GO_TO_NEXT_OR_STEP_FORWARD], ">|" );
            SetWindowText( m_windowHandles[FAST_FORWARD_OR_GO_TO_END], ">>" );
        }
        else
        {
            SetWindowText( m_windowHandles[REWIND_OR_GO_TO_START], "|<<" );
            SetWindowText( m_windowHandles[GO_TO_PREVIOUS_OR_STEP_BACKWARD], "|<" );
            SetWindowText( m_windowHandles[GO_TO_NEXT_OR_STEP_FORWARD], ">|" );
            SetWindowText( m_windowHandles[FAST_FORWARD_OR_GO_TO_END], ">>|" );
        }

        for ( int i = 0; i < NUM_BUTTONS; ++i )
        {
            s_oldWindowEditMessageCallback = (WNDPROC)SetWindowLongPtr( m_windowHandles[i], GWLP_WNDPROC,
                (LONG_PTR)WindowEditMessageCallback );
        }
    }
}

void crVCRWidget::WindowUpdate()
{
    // do nothing	
}

void crVCRWidget::WindowDestroy()
{
    if ( m_titleWindowHandle != NULL )
    {
        DestroyWindow( m_titleWindowHandle );
    }

    for ( int i = 0; i < NUM_BUTTONS; ++i )
    {
        if ( m_windowHandles[i] != NULL )
        {
            DestroyWindow( m_windowHandles[i] );
            m_windowHandles[i] = NULL;
        }
    }

    bkWidget::WindowDestroy();
}

rageLRESULT crVCRWidget::WindowMessage( rageUINT msg, rageWPARAM wParam, rageLPARAM /*lParam*/ )
{
    if ( (msg == WM_COMMAND) && (HIWORD(wParam) == BN_CLICKED) )
    {
        WindowMessage( DIAL, +1 );
    }

    return 0;
}

void crVCRWidget::WindowMessage( Action action, float /*value*/ )
{
    if ( action == DIAL )
    {
        SetWindowInFocus( bkPane::WindowMessageHwnd );

        ButtonPressed( m_buttonInFocus );
    }
}

int crVCRWidget::WindowResize( int x, int y, int width, int height )
{
    int buttonWidth = 35;

    int buttonX = width - buttonWidth;
    for ( int i = NUM_BUTTONS - 1; i >= 0; --i, buttonX -= buttonWidth )
    {
        if ( m_windowHandles[i] != NULL )
        {
            SetWindowPos( m_windowHandles[i], 0, buttonX, y, buttonWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );
        }
    }

    if ( m_titleWindowHandle != NULL )
    {
        SetWindowPos( m_titleWindowHandle, 0, x, y, width - (buttonWidth * 7), height, SWP_NOACTIVATE | SWP_NOZORDER );
    }

    return y + height;
}

void crVCRWidget::SetButtonInFocus( Button button, bool setWindowFocus )
{
    if ( (button != m_buttonInFocus) && (button >= 0) && (button < NUM_BUTTONS) )
    {
        if ( (m_buttonInFocus >= 0) && (m_buttonInFocus < NUM_BUTTONS) )
        {
            // deselect any highlighted text
            if ( m_windowHandles[m_buttonInFocus] != NULL )
            {
                ::SendMessage( m_windowHandles[m_buttonInFocus], EM_SETSEL, 0, 0 );
            }
        }

        m_buttonInFocus = button;

        if ( setWindowFocus && (m_windowHandles[m_buttonInFocus] != NULL) )
        {
            ::SendMessage( m_windowHandles[m_buttonInFocus], WM_SETFOCUS, 0, 0 );
        }

        //Displayf( "ButtonInFocus=%d", m_buttonInFocus );
    }
}

void crVCRWidget::SetWindowInFocus( struct HWND__* hwnd )
{
    for ( int i = 0; i < NUM_BUTTONS; ++i )
    {
        if ( m_windowHandles[i] == hwnd )
        {
            SetButtonInFocus( (crVCRWidget::Button)i, false );
            break;
        }
    }
}

struct HWND__* crVCRWidget::GetWindowInFocus() const
{
    if ( (m_buttonInFocus >= 0) && (m_buttonInFocus < NUM_BUTTONS) )
    {
        return m_windowHandles[m_buttonInFocus];
    }
    else
    {
        return m_titleWindowHandle;
    }
}

#endif // __WIN32PC

void crVCRWidget::Message( Action action, float value )
{
    if ( action == DIAL )
    {
        if ( m_drawLocalExpanded )
        {
            if ( m_drawLocalExpanded > 0 )
            {
                int button = sm_Cursor - sm_TopLine - m_drawLocalExpanded;
                if ( button == 0 )
                {
                    if ( value < 0.0f )
                    {
                        m_drawLocalExpanded = 0;
                    }
                }
                else
                {
                    ButtonPressed( (crVCRWidget::Button)(button - 1) );
                }
            }
        }
        else
        {
            if ( value > 0.0f )
            {
                m_drawLocalExpanded = -1;
            }
        }
    }
}

int crVCRWidget::DrawLocal( int x, int y )
{
    if ( m_drawLocalExpanded )
    {
        m_drawLocalExpanded = y;

        Drawf( x, y, "(-)%s", m_Title );
        y = bkWidget::DrawLocal( x, y );

        if ( m_buttonStyle == VCR )
        {
            static const char* buttonNames[] = { "Rewind", "Previous", "Play Backwards", "Pause", "Play Forwards", "Next", "Fast Forward" };
            for ( int i = 0; i < NUM_BUTTONS; ++i )
            {
                Drawf( x, y, "  %s", buttonNames[i] );
                y = bkWidget::DrawLocal( x, y );
            }
        }
        else
        {
           static const char* buttonNames[] = { "Go To Start", "Step Backward", "Play Backwards", "Pause", "Play Forwards", "Step Forward", "Go To End" };
            for ( int i = 0; i < NUM_BUTTONS; ++i )
            {
                Drawf( x, y, "  %s", buttonNames[i] );
                y = bkWidget::DrawLocal( x, y );
            }
        }

        return y;
    }
    else
    {
        Drawf( x, y, "(+)%s", m_Title );

        return bkWidget::DrawLocal( x, y );
    }
}

void crVCRWidget::RemoteHandler(const bkRemotePacket& packet)
{
    switch ( packet.GetCommand() )
    {
    case bkRemotePacket::CREATE:
        {
            packet.Begin();
            u32 id = packet.GetId();
            bkWidget* parent = packet.ReadWidget<bkWidget>();
            if (!parent) return;
            const char *title = packet.Read_const_char();
            const char *memo = packet.Read_const_char();
			const char *fillColor = packet.Read_const_char();
			bool readOnly = packet.Read_bool();
            u32 vcrButtonStyle = packet.Read_u32();
            u32 enabledButtons = packet.Read_u32();
            packet.End();
            crVCRWidget& widget = *(rage_new crVCRWidget(title,memo,(crVCRWidget::ButtonStyle)vcrButtonStyle,enabledButtons,fillColor,readOnly));
            bkRemotePacket::SetWidgetId(widget, id);
            parent->AddChild(widget);
        }
        break;
    default:
        {
            int button = packet.GetCommand() - bkRemotePacket::USER;
            if ( (button >= 0) && (button < NUM_BUTTONS) )
            {
                packet.Begin();		

                crVCRWidget *widget = packet.ReadWidget<crVCRWidget>();
                if ( !widget ) return;
                packet.End();

                widget->ButtonPressed( (crVCRWidget::Button)button );
            }
        }
        break;
    }
}

void crVCRWidget::ButtonPressed( Button button )
{
    EnabledButton enabled = (EnabledButton)(1 << button);
    if ( (m_enabledButtons & enabled) == 0 )
    {
        return;
    }

    //Displayf( "ButtonPressed=%d", button );

    switch ( button )
    {
    case REWIND_OR_GO_TO_START:
        {
            if ( m_RewindOrGoToStartFunc.IsValid() )
            {
                m_RewindOrGoToStartFunc();
            }
        }
        break;
    case GO_TO_PREVIOUS_OR_STEP_BACKWARD:
        {
            if ( m_GoToPreviousOrStepBackwardFunc.IsValid() )
            {
                m_GoToPreviousOrStepBackwardFunc();
            }
        }
        break;
    case PLAY_BACKWARDS:
        {
            if ( m_PlayBackwardsFunc.IsValid() )
            {
                m_PlayBackwardsFunc();
            }
        }
        break;
    case PAUSE:
        {
            if ( m_PauseFunc.IsValid() )
            {
                m_PauseFunc();
            }
        }
        break;
    case PLAY_FORWARDS:
        {
            if ( m_PlayForwardsFunc.IsValid() )
            {
                m_PlayForwardsFunc();
            }
        }
        break;
    case GO_TO_NEXT_OR_STEP_FORWARD:
        {
            if ( m_GoToNextOrStepForwardFunc.IsValid() )
            {
                m_GoToNextOrStepForwardFunc();
            }
        }
        break;
    case FAST_FORWARD_OR_GO_TO_END:
        {
            if ( m_FastForwardOrGoToEndFunc.IsValid() )
            {
                m_FastForwardOrGoToEndFunc();
            }
        }
        break;
    default:
        break;
    }
}

void crVCRWidget::SetEnabledButtons( u32 enabledButtons )
{
    m_enabledButtons = enabledButtons;

    bkRemotePacket p;
    p.Begin( bkRemotePacket::CHANGED, GetStaticGuid(), this );
    p.Write_u32( m_enabledButtons );
    p.Send();
}

void crVCRWidget::Update()
{
    // do nothing
}

} // namespace rage

#endif // __BANK