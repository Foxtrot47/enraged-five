//
// cranimation/channelsmallestthree.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELSMALLESTTHREE_H
#define CRANIMATION_CHANNELSMALLESTTHREE_H

#include "animation_config.h"

#if CR_DEV
#include "animchannel.h"
#include "channelquantize.h"

namespace rage
{


class crAnimChannelSmallestThreeQuaternion : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelSmallestThreeQuaternion() : crAnimChannel(AnimChannelTypeSmallestThreeQuaternion) {}
	virtual ~crAnimChannelSmallestThreeQuaternion() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelSmallestThreeQuaternion);
	virtual void EvaluateQuaternion(float time, QuatV_InOut outQuaternion) const;
	virtual bool CreateQuaternion(QuatV* quaternions, int num, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);

private:
	void ReconstructQuaternion(int t, QuatV_InOut q) const;

public:

	struct QuantizedFloats
	{
		QuantizedFloats()
		{
			if(datResource_sm_Current == NULL)
			{
				m_Offset = 0.f;
				m_ScaleAndValues = NULL;
			}
		}

		QuantizedFloats(const QuantizedFloats& r) { m_Offset = r.m_Offset; m_ScaleAndValues = r.m_ScaleAndValues ? rage_new ScaleAndValues(*r.m_ScaleAndValues) : NULL; }
		~QuantizedFloats() { if(m_ScaleAndValues) delete m_ScaleAndValues; }
		int ComputeSize() const;

		void Serialize(crArchive&);

		struct ScaleAndValues
		{
			ScaleAndValues() : m_Scale(0.f) {}
			ScaleAndValues(datResource&);
			int ComputeSize() const;
			void Serialize(crArchive&);

			float m_Scale;
			atPackedArray m_QuantizedValues;
		};

		float m_Offset;
		ScaleAndValues* m_ScaleAndValues;
	};

private:
	atRangeArray<QuantizedFloats, 3> m_QuantizedFloats;
	u32 m_QuantizedOrder;
};


} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELSMALLESTTHREE_H

