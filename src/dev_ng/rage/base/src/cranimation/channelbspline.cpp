//
// cranimation/channelbspline.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "channelbspline.h"

#if CR_DEV
#include "animation.h"
#include "animarchive.h"
#include "animstream.h"
#include "animtolerance.h"
#include "channelquantize.h"

#include "mathext/linearalgebra.h"

namespace rage
{

//////////////////////////////////////////////////////////////////////////////////////////////

struct crFloatEncoder
{
	enum
	{
		kMantissaBits32 = 23,
		kExponentBits32 = 8,
		kMantissaMask32 = 0x007fffff,
		kExponentMask32 = 0x7f800000,
		kSignMask32 = 0x80000000,
		kExponentBias32 = 127,
		kSignShift32 = 31,
	};

	crFloatEncoder(int exponentBits, int mantissaBits)
		: m_ExponentBits(exponentBits)
		, m_MantissaBits(mantissaBits)
	{
		Assert(m_ExponentBits <= kExponentBits32);
		Assert(m_MantissaBits <= kMantissaBits32);
		Assert(m_ExponentBits > 0);
		Assert(m_MantissaBits > 0);
		m_ExponentBias = (1<<(m_ExponentBits-1))-1;
		m_SignShift = m_ExponentBits + m_MantissaBits;

		m_SignMask = m_ExponentBits + m_MantissaBits;
		m_ExponentMask = ((1<<m_ExponentBits)-1)<<m_MantissaBits;
		m_MantissaMask = (1<<m_MantissaBits)-1;

		m_ExponentMax = (1<<(m_ExponentBits-1))-1;
		m_ExponentMin = -m_ExponentMax-1;
	}

	int GetOutputSize() const
	{
		return m_ExponentBits + m_MantissaBits + 1;
	}

	u32 Encode(float f, bool rounding)
	{
		if (f == 0.0F)
		{
			return 0;
		}
		u32 src = *(u32*)&f;

		int mantissaShift = kMantissaBits32 - m_MantissaBits;

		u32 mantissa = (src & kMantissaMask32);
		int exponent = (src & kExponentMask32) >> kMantissaBits32;
		u32 sign = (src >> kSignShift32);

		exponent -= kExponentBias32;

		if (rounding)
		{
			int roundingConstant = 1<<(mantissaShift-1);
			int testBit = 1<<kMantissaBits32;
			mantissa += roundingConstant;
			if (mantissa & testBit)
			{
				mantissa = 0;
				++exponent;
			}
		}

		mantissa >>= mantissaShift;

		if (exponent < m_ExponentMin)
		{
			if (exponent < m_ExponentMin-1)
			{
				return 0;
			}
			exponent = m_ExponentMin;
		}

		if (exponent > m_ExponentMax)
		{
			exponent = m_ExponentMax;
		}

		exponent = (exponent-m_ExponentMin);

		u32 result = (sign<<m_SignShift)|(exponent<<m_MantissaBits)|(mantissa);

		return result;
	}

	float Decode(u32 encoded)
	{
		if (encoded == 0)
		{
			return 0.0F;
		}

		u32 mantissa = (encoded&m_MantissaMask);
		int exponent = (encoded&m_ExponentMask)>>m_MantissaBits;
		u32 sign = (encoded>>m_SignShift);

		exponent += m_ExponentMin;
		exponent += kExponentBias32;

		mantissa <<= (kMantissaBits32-m_MantissaBits);

		u32 result = (sign<<kSignShift32)|(exponent<<kMantissaBits32)|(mantissa);

		return *(float*)&result;
	}

	int m_ExponentBits, m_MantissaBits;
	int m_SignMask, m_MantissaMask, m_ExponentMask;
	int m_ExponentBias;
	int m_SignShift;
	int m_ExponentMin, m_ExponentMax;
};

//////////////////////////////////////////////////////////////////////////////////////////////

template <typename DataType>
struct SplineFitBasis
{
	SplineFitBasis(int count, int degree)
		: m_Count(count)
	{
		m_Value = rage_new DataType[degree+1];
		m_Knot = rage_new DataType[2*degree];
	}

	~SplineFitBasis()
	{
		delete[] m_Knot;
		delete[] m_Value;
	}

	DataType* m_Value;
	DataType* m_Knot;
	int m_Count;
};

//////////////////////////////////////////////////////////////////////////////////////////////

template <typename DataType>
void Compute(DataType* values, DataType* knots, int count, int degree, DataType t, int& min, int& max)
{
	Assert(0.0F <= t && t <= 1.0F);
	DataType qmd = DataType(count - degree);
	DataType value;
	if (t <= 0.0F)
	{
		value = 0.0F;
		min = 0;
		max = degree;
	}
	else if (t >= 1.0F)
	{
		value = qmd;
		max = count - 1;
		min = max - degree;
	}
	else
	{
		value = qmd * t;
		min = (int)value;
		max = min + degree;
	}

	// Precompute the knots
	for (int i0 = 0, i1 = max + 1 - degree; i0 < 2 * degree; ++i0, ++i1)
	{
		if (i1 <= degree)
		{
			knots[i0] = 0.0F;
		}
		else if (i1 >= count)
		{
			knots[i0] = qmd;
		}
		else
		{
			knots[i0] = float(i1 - degree);
		}
	}

	values[degree] = 1.f;

	for (int row = degree-1; row >= 0; --row)
	{
		int k0 = degree, k1 = row;
		DataType knot0 = knots[k0], knot1 = knots[k1];
		DataType inv_denom = 1.f/(knot0 - knot1);
		DataType c1 = (knot0 - value)*inv_denom, c0;
		values[row] = c1 * values[row + 1];

		for (int col = row + 1; col < degree; ++col)
		{
			c0 = (value - knot1)*inv_denom;
			values[col] *= c0;

			knot0 = knots[++k0];
			knot1 = knots[++k1];
			inv_denom = 1.f/(knot0 - knot1);
			c1 = (knot0 - value)*inv_denom;
			values[col] += c1*values[col+1];
		}

		c0 = (value - knot1)*inv_denom;
		values[degree] *= c0;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

template <typename DataType>
void Compute(SplineFitBasis<DataType>* b, DataType t, int degree, int& min, int& max)
{
	Compute(b->m_Value, b->m_Knot, b->m_Count, degree, t, min, max);
}

//////////////////////////////////////////////////////////////////////////////////////////////

template <typename DataType>
bool FitCurve(int dimension, const float* sampleData, const short sampleCount, const int skip, int degree, DataType* controlData, const short controlCount, SplineFitBasis<DataType>& basis)
{
	Assert(dimension >= 1);
	Assert(1 <= degree && degree <= controlCount);
	Assert(controlCount <= sampleCount);

	DataType* cend0 = controlData;
	const float* send0 = sampleData;
	DataType* cend1 = &controlData[dimension*(controlCount - 1)];
	const float* send1 = &sampleData[dimension*(sampleCount - 1)*(skip+1)];

	DataType multiplier = 1.0F/DataType(sampleCount-1);

	DataType t;
	int min, max;
	MatrixT<DataType>& ATA = *rage_new MatrixT<DataType>(controlCount, controlCount);
	for (short i0 = 0; i0 < controlCount; ++i0)
	{
		for (short i1 = 0; i1 < i0; ++i1)
		{
			ATA[i0][i1] = ATA[i1][i0];
		}
		int i1max = i0 + degree;
		if (i1max >= controlCount)
		{
			i1max = controlCount - 1;
		}

		for (short i1 = i0; i1 <= i1max; ++i1)
		{
			DataType value = 0.0;
			for (short i2 = 0; i2 < sampleCount; ++i2)
			{
				t = multiplier * DataType(i2);
				Compute(&basis, t, degree, min, max);
				if (min <= i0 && i0 <= max && min <= i1 && i1 <= max)
				{
					DataType db0 = basis.m_Value[i0 - min];
					DataType db1 = basis.m_Value[i1 - min];
					value += db0 * db1;
				}
			}
			ATA[i0][i1] = value;
		}
	}

	MatrixT<DataType>& AT = *rage_new MatrixT<DataType>(controlCount, sampleCount);
	for (short i0 = 0; i0 < controlCount; ++i0)
	{
		for (short i1 = 0; i1 < sampleCount; ++i1)
		{
			t = multiplier*DataType(i1);
			Compute(&basis, t, degree, min, max);
			if (min <= i0 && i0 <= max)
			{
				AT[i0][i1] = basis.m_Value[i0 - min];
			}
		}
	}

	bool solved = CholeskyFactorization(ATA, controlCount);
	if (!solved)
	{
		goto end;
	}

	// Solve Lower
	for (short row = 0; row < ATA.GetRows(); ++row)
	{
		DataType lowerrr = ATA[row][row];
		if (fabs(lowerrr) < SMALL_FLOAT)
		{
			solved = false;
			break;
		}
		for (short column = 0; column < row; ++column)
		{
			DataType lowerrc = ATA[row][column];
			for (short bcol = 0; bcol < AT.GetCols(); ++bcol)
			{
				AT[row][bcol] -= lowerrc * AT[column][bcol];
			}
		}
		DataType inverse = 1.0/lowerrr;
		for (short bcol = 0; bcol < AT.GetCols(); ++bcol)
		{
			AT[row][bcol] *= inverse;
		}
	}

	if (!solved)
	{
		goto end;
	}

	// Solve Upper
	for (short row = ATA.GetRows()-1; row >= 0; --row)
	{
		DataType upperrr = ATA[row][row];
		if (fabs(upperrr) < SMALL_FLOAT)
		{
			solved = false;
			break;
		}
		for (short column = row + 1; column < ATA.GetCols(); ++column)
		{
			DataType upperrc = ATA[row][column];
			for (short bcol = 0; bcol < AT.GetCols(); ++bcol)
			{
				AT[row][bcol] -= upperrc * AT[column][bcol];
			}
		}
		DataType inverse = 1.0/upperrr;
		for (short bcol = 0; bcol < AT.GetCols(); ++bcol)
		{
			AT[row][bcol] *= inverse;
		}
	}

	if (!solved)
	{
		goto end;
	}

	memset(controlData, 0, controlCount*dimension*sizeof(DataType));
	for (short i0 = 0; i0 < controlCount; ++i0)
	{
		DataType* q = controlData + i0 * dimension;
		for (short i1 = 0; i1 < sampleCount; ++i1)
		{
			const float* p = sampleData + (i1 * dimension)*(skip+1);
			DataType xval = AT[i0][i1];

			for (int j = 0; j < dimension; j++)
			{
				q[j] += xval*p[j];
			}
		}
	}

	for (int j = 0; j < dimension; j++)
	{
		*cend0++ = *send0++;
		*cend1++ = *send1++;
	}

end:
	delete &AT;
	delete &ATA;
	return solved;
}

//////////////////////////////////////////////////////////////////////////////////////////////

static float EvaluateFloat(float time, const atPackedArray& packed, int count, int degree, float scale, float offset)
{
	float values[4], knots[6];
	int min, max;
	Compute(values, knots, count, degree, time, min, max);
	int index = 1*min;
	u32 pack = packed.GetElement(index);
	float val = values[0];
	float source = float(pack) * scale + offset;
	float out = val*source;
	++index;
	for (int i = min+1, idx = 1; i <= max; ++i, ++idx)
	{
		pack = packed.GetElement(index);
		source = float(pack) * scale + offset;
		val = values[idx];

		out += val*source;

		++index;
	}

	return out;
}

//////////////////////////////////////////////////////////////////////////////////////////////

static float EvaluateFloat(float time, const atArray<float>& controls, int count, int degree)
{
	float values[4], knots[6];
	int min, max;
	Compute(values, knots, count, degree, time, min, max);
	const float* source = &controls[1*min];
	float val = values[0];
	float out = val*(*source);
	++source;
	for (int i = min+1, idx = 1; i <= max; ++i, ++idx)
	{
		val = values[idx];
		out += val*(*source);
		++source;
	}

	return out;
}

//////////////////////////////////////////////////////////////////////////////////////////////

__forceinline void SelectBranch(bool passed, const int curr, int* min, int* max, int* optimal)
{
	if (passed)
	{
		*optimal = curr;
		*max = curr-1;
	}
	else
	{
		*min = curr+1;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

static bool CreateBSpline(const int dimension, const float* floats, const int frames, const int skip, const int degree, atPackedArray& packed, float tolerance, int* count, float& scale, float& offset)
{
	int minCount = degree + 2;
	int maxCount = frames;

	int optimalCount = -1;

	while (minCount <= maxCount)
	{
		int currCount = minCount + (maxCount-minCount)/2;
		int len = dimension * currCount;

		atArray<float> controls;
		controls.Resize(len);

		double* data = rage_new double[len];
		SplineFitBasis<double> basis(currCount, degree);
		bool solved = FitCurve<double>(1, floats, short(frames), skip, degree, data, short(currCount), basis);
		if (!solved)
		{
			// If the solver fails assume we need more control data.
			SelectBranch(false, currCount, &minCount, &maxCount, &optimalCount);

			delete[] data;
			continue;
		}

		for (int i = 0; i < len; ++i)
		{
			controls[i] = static_cast<float>(data[i]);
		}

		delete[] data;

		// Check the error at each sample
		bool passed = true;

		float mult = 1.0F/(frames-1);
		for (int i = 0; i < frames; ++i)
		{
			float result = EvaluateFloat(mult*i, controls, currCount, degree);
			if (!IsClose(result, floats[i*(skip+1)], tolerance))
			{
				passed = false;
				break;
			}
		}

		if (passed)
		{
			// Now encode the floats and make sure we're still within tolerance, keep doing this until we find something that can handle as much error as possible.
			int minIter = 1;
			int maxIter = 1000;

			int optimalIter = -1;

			while (minIter <= maxIter)
			{
				int currIter = minIter +(maxIter-minIter)/2;

				bool failed = false;

				if (crAnimChannelQuantizeFloat::QuantizeFloats(&controls[0], controls.GetCount(), 0, SMALL_FLOAT*currIter, packed, scale, offset))
				{
					for (int i=0; i<frames; ++i)
					{
						float result = EvaluateFloat(mult*i, packed, currCount, degree, scale, offset);
						if (!IsClose(result, floats[i*(skip+1)], tolerance))
						{
							failed = true;
							break;
						}
					}
				}

				if (failed)
				{
					minIter = currIter+1;
				}
				else
				{
					optimalIter = currIter;
					maxIter = currIter-1;
				}
			}

			if (optimalIter > 0)
			{
				crAnimChannelQuantizeFloat::QuantizeFloats(&controls[0], controls.GetCount(), 0, SMALL_FLOAT*(optimalIter), packed, scale, offset);
			}
			else
			{
				passed = false;
			}
		}

		SelectBranch(passed, currCount, &minCount, &maxCount, &optimalCount);
	}

	if (optimalCount >0)
	{
		*count = optimalCount;
		return true;
	}

	*count = 0;
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////

// Decompression cost isn't really maximum. It's just set to that so it's not used for now.
IMPLEMENT_ANIM_CHANNEL(crAnimChannelQuadraticBSpline, crAnimChannel::AnimChannelTypeQuadraticBSpline, crAnimTolerance::kCompressionCostVeryHigh, crAnimTolerance::kDecompressionCostMaximum);

//////////////////////////////////////////////////////////////////////////////////////////////

crAnimChannelQuadraticBSpline::crAnimChannelQuadraticBSpline()
: crAnimChannel(AnimChannelTypeQuadraticBSpline)
, m_Count(0)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////

crAnimChannelQuadraticBSpline::~crAnimChannelQuadraticBSpline()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelQuadraticBSpline::EvaluateFloat(float time, float& out) const
{
	float values[4], knots[6];
	int min, max;
	Compute(values, knots, m_Count, 2, time*m_Multiplier, min, max);
	int elem = 1*min;
	u32 packed = m_Packed.GetElement(elem);
	float val = values[0];
	float source = float(packed) * m_Scale + m_Offset;

	out = val*source;
	++elem;

	for (int i = min+1, idx = 1; i <= max; ++i, ++idx)
	{
		packed = m_Packed.GetElement(elem);
		source = float(packed) * m_Scale + m_Offset;
		val = values[idx];

		out += val*source;

		++elem;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelQuadraticBSpline::CreateFloat(float* floats, int frames, int skip, float tolerance)
{
	m_Multiplier = 1.0F/(frames-1);
	return CreateBSpline(1, floats, frames, skip, 2, m_Packed, tolerance, &m_Count, m_Scale, m_Offset);
}

//////////////////////////////////////////////////////////////////////////////////////////////

int crAnimChannelQuadraticBSpline::ComputeSize() const
{
	return 4*sizeof(u32) + m_Packed.ComputeSize();
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelQuadraticBSpline::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	m_Packed.Serialize(a);

	a <<= m_Multiplier;

	a <<= m_Scale;
	a <<= m_Offset;

	a <<= m_Count;
}

//////////////////////////////////////////////////////////////////////////////////////////////

// Decompression cost isn't really maximum. It's just set to that so it's not used for now.
IMPLEMENT_ANIM_CHANNEL(crAnimChannelCubicBSpline, crAnimChannel::AnimChannelTypeCubicBSpline, crAnimTolerance::kCompressionCostVeryHigh, crAnimTolerance::kDecompressionCostVeryHigh);

//////////////////////////////////////////////////////////////////////////////////////////////

crAnimChannelCubicBSpline::crAnimChannelCubicBSpline()
: crAnimChannel(AnimChannelTypeCubicBSpline)
, m_Count(0)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////

crAnimChannelCubicBSpline::~crAnimChannelCubicBSpline()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelCubicBSpline::EvaluateFloat(float time, float& out) const
{
	float values[4], knots[6];
	int min, max;
	Compute(values, knots, m_Count, 3, time*m_Multiplier, min, max);
	int elem = 1*min;
	u32 packed = m_Packed.GetElement(elem);
	float val = values[0];
	float source = float(packed) * m_Scale + m_Offset;

	out = val*source;
	++elem;

	for (int i = min+1, idx = 1; i <= max; ++i, ++idx)
	{
		packed = m_Packed.GetElement(elem);
		source = float(packed) * m_Scale + m_Offset;
		val = values[idx];

		out += val*source;

		++elem;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelCubicBSpline::CreateFloat(float* floats, int frames, int skip, float tolerance)
{
	m_Multiplier = 1.0F/(frames-1);
	return CreateBSpline(1, floats, frames, skip, 3, m_Packed, tolerance, &m_Count, m_Scale, m_Offset);
}

//////////////////////////////////////////////////////////////////////////////////////////////

int crAnimChannelCubicBSpline::ComputeSize() const
{
	return 4*sizeof(u32) + m_Packed.ComputeSize();
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelCubicBSpline::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	m_Packed.Serialize(a);

	a <<= m_Multiplier;

	a <<= m_Scale;
	a <<= m_Offset;

	a <<= m_Count;
}

//////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
