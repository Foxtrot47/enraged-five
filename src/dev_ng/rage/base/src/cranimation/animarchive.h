//
// cranimation/animarchive.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMARCHIVE_H
#define CRANIMATION_ANIMARCHIVE_H


#include "file/stream.h"
#include "vectormath/vec3v.h"
#include "vectormath/quatv.h"

namespace rage
{

// PURPOSE
// Used by animations to enable load and save operations to be implemented in a single function.
// Allows animations to be self exporting, and ensures loading and saving code remains in sync.
// NOTE - this is DEPRECATED, plan is to replace this with datSerialize in the near future.
class crArchive
{
public:
	crArchive(fiStream* f, bool loading, u16 version) : m_Flags(0), m_Version(0), m_Stream(0)
	{
		FastAssert(f);
		m_Stream = f;
		m_Version = version;
		m_Flags |= loading ? Loading : 0;
	}

	bool IsLoading() const
	{
		return (m_Flags & Loading) > 0;
	}

	bool IsStoring() const
	{
		return !IsLoading();
	}

	u16 GetVersion() const
	{
		return m_Version;
	}

	void operator<<=(unsigned char& c)
	{
		if(IsLoading())
			m_Stream->ReadByte(&c, 1);
		else
			m_Stream->WriteByte(&c, 1);
	}
	void operator<<=(char& c)
	{
		if(IsLoading())
			m_Stream->ReadByte(&c, 1);
		else
			m_Stream->WriteByte(&c, 1);
	}

	void operator<<=(unsigned short& i)
	{
		if(IsLoading())
			m_Stream->ReadShort(&i, 1);
		else
			m_Stream->WriteShort(&i, 1);
	}
	void operator<<=(short& i)
	{
		if(IsLoading())
			m_Stream->ReadShort(&i, 1);
		else
			m_Stream->WriteShort(&i, 1);
	}

	void operator<<=(unsigned int& i)
	{
		if(IsLoading())
			m_Stream->ReadInt(&i, 1);
		else
			m_Stream->WriteInt(&i, 1);
	}
	void operator<<=(int& i)
	{
		if(IsLoading())
			m_Stream->ReadInt(&i, 1);
		else
			m_Stream->WriteInt(&i, 1);
	}

	void operator<<=(float& f)
	{
		if(IsLoading())
			m_Stream->ReadFloat(&f, 1);
		else
			m_Stream->WriteFloat(&f, 1);
	}

	void operator<<=(Vec3V_InOut v)
	{
		*this<<=v[0];
		*this<<=v[1];
		*this<<=v[2];
	}

	void operator<<=(QuatV_InOut q)
	{
		*this<<=q[0];
		*this<<=q[1];
		*this<<=q[2];
		*this<<=q[3];
	}

private:
	enum
	{
		Loading = 0x01,
	};
	u16 m_Flags;
	u16 m_Version;
	fiStream* m_Stream;
};

} // namespace rage

#endif // CRANIMATION_ANIMARCHIVE_H
