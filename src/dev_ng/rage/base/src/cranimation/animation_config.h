//
// cranimation/animation_config.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMATION_CONFIG_H
#define CRANIMATION_ANIMATION_CONFIG_H

#include "diag/stats.h"

// enable animation development code
#define CR_DEV (!__FINAL)

#if CR_DEV
#define CR_DEV_ONLY(...)    __VA_ARGS__
#else
#define CR_DEV_ONLY(...)
#endif

// enable statistics on animation system
#define CR_STATS (__STATS && !__OPTIMIZED)

// enable collection of potential blocks to the animation system
#define CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE 0

const int crAnimationResourceBaseVersion = 24 + __64BIT;



#endif // CRANIMATION_ANIMATION_CONFIG_H
