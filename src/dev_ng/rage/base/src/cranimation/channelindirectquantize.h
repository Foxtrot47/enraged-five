//
// cranimation/channelindirectquantize.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELINDIRECTQUANTIZE_H
#define CRANIMATION_CHANNELINDIRECTQUANTIZE_H

#include "animchannel.h"

#if CR_DEV
namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Indirect float quantization
// Quantizes floats into discrete integer values, which are stored in a table
// indexed by a second sequence of integers
class crAnimChannelIndirectQuantizeFloat : public crAnimChannel
{
	friend class crAnimPacker;
public:

	// PURPOSE: Default constructor
	crAnimChannelIndirectQuantizeFloat();

	// PURPOSE: Destructor
	virtual ~crAnimChannelIndirectQuantizeFloat();

	// PURPOSE: Registration
	DECLARE_ANIM_CHANNEL(crAnimChannelIndirectQuantizeFloat);

	// PURPOSE: Simple evaluation
	virtual void EvaluateFloat(float fTime, float& outFloat) const;

	// PURPOSE: Creation
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance);

	// PURPOSE: Storage computation
	virtual int ComputeSize() const;

	// PURPOSE: Serialization
	virtual void Serialize(crArchive& a);

private:

	// PURPOSE: Internal function, reconstruction of single value
	float ReconstructFloat(int frame) const;

private:
	atPackedArray m_QuantizedIndices;
	atPackedArray m_QuantizedValues;
	float m_Scale;
	float m_Offset;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELINDIRECTQUANTIZE_H
