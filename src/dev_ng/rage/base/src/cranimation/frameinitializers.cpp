//
// cranimation/frameinitializers.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "frameinitializers.h"

#include "animation.h"
#include "animchunk.h"
#include "animiterator.h"
#include "animtrack.h"
#include "frameiterators.h"

#include "creature/component.h"
#include "creature/creature.h"
#include "crskeleton/skeletondata.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializer::crFrameDataInitializer(bool destroyExistingDofs, crFrameFilter* filter)
: m_FrameData(NULL)
, m_Filter(filter)
, m_DestroyExistingDofs(destroyExistingDofs)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializer::~crFrameDataInitializer()
{
}

////////////////////////////////////////////////////////////////////////////////

void crFrameDataInitializer::InitializeFrameData(crFrameData& frameData) const
{
	m_FrameData = &frameData;

	// reserve sufficient dofs
	u32 numReserveDofs = PreInitialize();
	m_FrameData->ReserveDofs(numReserveDofs, m_DestroyExistingDofs);

	// initialize the frame
	bool inOrder = Initialize();

	// sort the dofs in the frame and recalculate the signature
	if(!inOrder || !m_DestroyExistingDofs)
	{
		m_FrameData->SortDofs();
	}

	m_FrameData->CalcSignature();
	m_FrameData->CalcOffsets();

	m_FrameData = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameDataInitializer::FastAddFrameData(const crFrameData& frameData) const
{
	AddFrameDataIterator it(frameData, *this);
	it.Iterate(m_Filter);
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerFrameData::crFrameDataInitializerFrameData(const crFrameData& sourceFrameData, bool destroyExistingDofs, crFrameFilter* filter)
: crFrameDataInitializer(destroyExistingDofs, filter)
, m_SourceFrameData(&sourceFrameData)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerFrameData::~crFrameDataInitializerFrameData()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerFrameData::PreInitialize() const
{
	return m_SourceFrameData->GetNumDofs();
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerFrameData::Initialize() const
{
	FastAddFrameData(*m_SourceFrameData);
	return true;
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerBoneAndMover::crFrameDataInitializerBoneAndMover(const crSkeletonData& skelData, bool createMoverDofs, u16 moverId, bool destroyExistingDofs, crFrameFilter* filter)
: crFrameDataInitializer(destroyExistingDofs, filter)
, m_SkeletonData(&skelData)
, m_CreateMoverDofs(createMoverDofs)
, m_MoverId(moverId)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerBoneAndMover::~crFrameDataInitializerBoneAndMover()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerBoneAndMover::PreInitialize() const
{
	u32 numDofsRequired = PreInitializeSkeleton(*m_SkeletonData);

	if(m_CreateMoverDofs)
	{
		numDofsRequired += 2;
	}

	return numDofsRequired;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerBoneAndMover::Initialize() const
{
	bool inOrder = InitializeSkeleton(*this, *m_SkeletonData);

	if(m_CreateMoverDofs)
	{
		FastAddDof(kTrackMoverTranslation, m_MoverId, kFormatTypeVector3);
		FastAddDof(kTrackMoverRotation, m_MoverId, kFormatTypeQuaternion);
	}

	return inOrder;
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerBoneAndMover::PreInitializeSkeleton(const crSkeletonData& skelData)
{
	// calculate the number of bones that have translation and rotation dofs
	u32 numDofsRequired = 0;
	int numBones = skelData.GetNumBones();
	for(int i=0; i<numBones; ++i)
	{
		const crBoneData* boneData = skelData.GetBoneData(i);
		if(boneData)
		{
			// NOTE: the DOFs in boneData are currently channel dofs NOT track dofs!
			if(boneData->HasDofs(crBoneData::ROTATION))
			{
				++numDofsRequired;
			}
			if(boneData->HasDofs(crBoneData::TRANSLATION))
			{
				++numDofsRequired;
			}
			if(boneData->HasDofs(crBoneData::SCALE))
			{
				++numDofsRequired;
			}
		}
	}

	return numDofsRequired;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerBoneAndMover::InitializeSkeleton(const crFrameDataInitializer& initializer, const crSkeletonData& skelData)
{
	const int numBones = skelData.GetNumBones();
	bool inOrder = true;

	for(int i=0; i<numBones; ++i)
	{
		const crBoneData* boneData = skelData.GetBoneData(i);
		if(boneData && boneData->HasDofs(crBoneData::TRANSLATION))
		{
			u16 boneId;
			if(skelData.ConvertBoneIndexToId(u16(i), boneId))
			{
				inOrder = inOrder && (u16(i) == boneId);

				initializer.FastAddDof(kTrackBoneTranslation, boneId, kFormatTypeVector3);
			}
		}
	}

	for(int i=0; i<numBones; ++i)
	{
		const crBoneData* boneData = skelData.GetBoneData(i);
		if(boneData && boneData->HasDofs(crBoneData::ROTATION))
		{
			u16 boneId;
			if(skelData.ConvertBoneIndexToId(u16(i), boneId))
			{
				inOrder = inOrder && (u16(i) == boneId);

				initializer.FastAddDof(kTrackBoneRotation, boneId, kFormatTypeQuaternion);
			}
		}
	}

	for(int i=0; i<numBones; ++i)
	{
		const crBoneData* boneData = skelData.GetBoneData(i);
		if(boneData && boneData->HasDofs(crBoneData::SCALE))
		{
			u16 boneId;
			if(skelData.ConvertBoneIndexToId(u16(i), boneId))
			{
				inOrder = inOrder && (u16(i) == boneId);

				initializer.FastAddDof(kTrackBoneScale, boneId, kFormatTypeVector3);
			}
		}
	}

	return inOrder;
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerRootBone::crFrameDataInitializerRootBone(bool destroyExistingDofs, crFrameFilter* filter)
: crFrameDataInitializer(destroyExistingDofs, filter)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerRootBone::~crFrameDataInitializerRootBone()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerRootBone::PreInitialize() const
{
	return 2;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerRootBone::Initialize() const
{
	FastAddDof(kTrackBoneTranslation, 0, kFormatTypeVector3);
	FastAddDof(kTrackBoneRotation, 0, kFormatTypeQuaternion);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerMover::crFrameDataInitializerMover(u16 moverId, bool destroyExistingDofs, crFrameFilter* filter)
: crFrameDataInitializer(destroyExistingDofs, filter)
, m_MoverId(moverId)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerMover::~crFrameDataInitializerMover()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerMover::PreInitialize() const
{
	return 2;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerMover::Initialize() const
{
	FastAddDof(kTrackMoverTranslation, m_MoverId, kFormatTypeVector3);
	FastAddDof(kTrackMoverRotation, m_MoverId, kFormatTypeQuaternion);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerDofs::crFrameDataInitializerDofs(int numDofs, u8 tracks[], u16 ids[], u8 types[], bool destroyExistingDofs, crFrameFilter* filter)
: crFrameDataInitializer(destroyExistingDofs, filter)
, m_NumDofs(numDofs)
, m_Tracks(tracks)
, m_Ids(ids)
, m_Types(types)
{
	Assert(m_NumDofs >= 0);
	Assert(m_Tracks);
	Assert(m_Ids);
	Assert(m_Types);
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerDofs::~crFrameDataInitializerDofs()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerDofs::PreInitialize() const
{
	return m_NumDofs;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerDofs::Initialize() const
{
	for(int i=0; i<m_NumDofs; ++i)
	{
		FastAddDof(m_Tracks[i], m_Ids[i], m_Types[i]);
	}

	return (m_NumDofs <= 1);
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerAnimation::crFrameDataInitializerAnimation(const crAnimation& anim, bool destroyExistingDofs, crFrameFilter* filter)
: crFrameDataInitializer(destroyExistingDofs, filter)
, m_Animation(&anim)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerAnimation::~crFrameDataInitializerAnimation()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerAnimation::PreInitialize() const
{
	return m_Animation->GetNumDofs();
}

////////////////////////////////////////////////////////////////////////////////

class FrameDataInitAnimIterator : public crAnimIterator<FrameDataInitAnimIterator>
{
public:

	FrameDataInitAnimIterator(const crAnimation& anim, const crFrameDataInitializer& initializer)
		: crAnimIterator<FrameDataInitAnimIterator>(anim)
		, m_Initializer(&initializer)
	{
	}

	__forceinline void IterateDof(u8 track, u16 id, u8 type, float)
	{
		m_Initializer->FastAddDof(track, id, type);
	}

	const crFrameDataInitializer* m_Initializer;
};

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerAnimation::Initialize() const
{
	FrameDataInitAnimIterator it(*m_Animation, *this);
	it.Iterate(0.f);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage



