//
// cranimation/animpacker.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "animpacker.h"

#if CR_DEV
#include "animation.h"
#include "animchunk.h"
#include "animtrack.h"
#include "channelbasic.h"
#include "channelbspline.h"
#include "channelindirectquantize.h"
#include "channellinear.h"
#include "channelquantize.h"
#include "channelsmallestthree.h"

#include "data/compress.h"
#include "system/memory.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

u16 CreateChannelIndex(u32 dof, u32 comp) { return u16((dof<<2) | comp); }

////////////////////////////////////////////////////////////////////////////////

u32 crAnimPacker::Pack(bool compact, bool byteSwap, crAnimation& anim)
{
	// create tracks
	u32 numTracks = anim.GetNumTracks();
	anim.m_Dofs.Resize(numTracks);
	for(u32 i=0; i < numTracks; i++)
	{
		const crAnimTrack* animTrack = anim.GetTrack(i);
		crAnimation::Dof& dof = anim.m_Dofs[i];
		dof.m_Id = animTrack->GetId();
		dof.m_Track = animTrack->GetTrack();
		dof.m_Type = animTrack->GetType();
	}

	// create blocks
	const u32 numBlocks = numTracks > 0 ? anim.GetTrack(0)->GetNumChunks() : 0;
	anim.m_Blocks.Resize(numBlocks);

	u32 maxBlockSize = 0;
	for(u32 i=0; i < numBlocks; ++i)
	{
		// Create temporary block
		sysMemStartTemp();
		Streamer<crBlockStream> block;
		Create(block, anim, u16(i));

		u32 blockSize = block->GetBlockSize();

		u32 dataSize = block->m_DataSize;
		if(byteSwap)
		{
			Swap(block);
			if(compact)
			{
				Compact(block, dataSize);
			}
		}
		sysMemEndTemp();

		maxBlockSize = Max(maxBlockSize, blockSize);

		// Allocate and copy a new block
		u32 totalSize = block.GetSize();	
		anim.m_Blocks[i] = reinterpret_cast<crBlockStream*>(rage_aligned_new(16) u8[totalSize]);
		sysMemCpy(anim.m_Blocks[i], block.GetPtr(), totalSize);

		// Delete temporary block
		sysMemStartTemp();
		block.Clear();
		sysMemEndTemp();
	}

	return maxBlockSize;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimPacker::Create(Streamer<crBlockStream>& block, const crAnimation& anim, u16 chunkIdx)
{
	// convert legacy channels
	for(s32 i=anim.GetNumTracks()-1; i >=0; --i)
	{
		const crAnimTrack* track = anim.GetTrack(i);
		const crAnimChunk* chunk = track->GetChunk(chunkIdx);
		const crAnimChannel* channel = chunk->GetChannel(0);
		if(channel)
		{
			ConvertLegacyChannel(const_cast<crAnimChunk*>(chunk), channel);
		}
	}

	// create channels
	s32 moverTransIdx = -1, moverRotIdx = -1;
	u32 quantizeSize = 0;
	u32 indirectSize = 0;
	atArray<u16> indices[crBlockStream::kNumType];
	atArray<u32,0,u32> constants[crBlockStream::kNumType];
	for(u32 trackIdx=0; trackIdx < anim.GetNumTracks(); ++trackIdx)
	{
		const crAnimTrack* track = anim.GetTrack(trackIdx);
		const crAnimChunk* chunk = track->GetChunk(chunkIdx);

		for(u16 channelIdx=0; channelIdx < chunk->GetNumChannels(); ++channelIdx)
		{
			AddChannel(chunk->GetChannel(channelIdx), trackIdx, channelIdx, indices, constants, quantizeSize, indirectSize);
		}

		if(track->GetType() == kFormatTypeQuaternion)
		{
			u8 reconstruct = chunk->GetReconstruct();
			if(reconstruct < 4)
			{
				indices[crBlockStream::kReconstruct].Grow() = CreateChannelIndex(trackIdx, reconstruct);
			}
			else if(chunk->GetNumChannels() > 1)
			{
				indices[crBlockStream::kNormalize].Grow() = CreateChannelIndex(trackIdx, 0);
			}
		}

		if(track->GetTrack() == kTrackMoverTranslation)
		{
			moverTransIdx = trackIdx;
		}
		else if(track->GetTrack() == kTrackMoverRotation)
		{
			moverRotIdx = trackIdx;
		}
	}
	Assign(block->m_QuantizeSize, quantizeSize);
	Assign(block->m_IndirectSize, indirectSize);

	// add constant
	for(u32 i=0; i < crBlockStream::kNumType; ++i)
	{
		u32 size = constants[i].GetCount()*sizeof(u32);
		block.Grow(size, constants[i].GetElements());
		block->m_ConstantSize += size;
	}

	// add frames
	u32 blockSize = block.GetSize();
	u32 numFrames = anim.CalcNumFramesInBlock(chunkIdx);
	Assign(block->m_NumFrames, numFrames);
	for(u32 i=0; i < numFrames; ++i)
	{
		CreateFrame(chunkIdx, i, block, anim, indices);
		block.Align(4);
	}
	u32 frameSize = (block.GetSize() - blockSize) / numFrames;
	Assign(block->m_FrameSize, frameSize);

	// add indices count
	for(int i=0; i < crBlockStream::kNumType; ++i)
	{
		u16 count = u16(indices[i].GetCount());
		block.Grow(sizeof(u16), &count);
	}

	u32 numTracks = anim.GetNumTracks();
	for(int i=0; i < crBlockStream::kNumType; ++i)
	{
		// add padding
		while((indices[i].GetCount()%4)!=0)
		{
			indices[i].Grow() = CreateChannelIndex(numTracks, 0);
		}

		block.Grow(indices[i].GetCount()*sizeof(u16), indices[i].GetElements());
	}

	// find mover channels
	u32 numTransChannels = 0, numRotChannels = 0;
	crBlockStream::Channel channels[5];
	block->m_ChannelOffset = block.GetSize();
	if(moverTransIdx >= 0)
	{
		block->FindChannels(moverTransIdx, numTransChannels, channels);
		block.Grow(numTransChannels*sizeof(crBlockStream::Channel), channels);
	}
	if(moverRotIdx >= 0)
	{
		block->FindChannels(moverRotIdx, numRotChannels, channels);
		block.Grow(numRotChannels*sizeof(crBlockStream::Channel), channels);
	}
	Assign(block->m_NumMoverChannels, (numTransChannels << 4) | numRotChannels);
	block->m_DataSize = block.GetSize() - sizeof(crBlockStream);

	// find segment size
	block->m_SegmentSize = UCHAR_MAX;
	for(u32 trackIdx=0; trackIdx < anim.GetNumTracks(); ++trackIdx)
	{
		const crAnimTrack* track = anim.GetTrack(trackIdx);
		const crAnimChunk* chunk = track->GetChunk(chunkIdx);
		for(u16 channelIdx=0; channelIdx < chunk->GetNumChannels(); ++channelIdx)
		{
			const crAnimChannel* channel = chunk->GetChannel(channelIdx);
			if(channel->GetType() == crAnimChannel::AnimChannelTypeLinearFloat)
			{
				const crAnimChannelLinearFloat* linearChannel = static_cast<const crAnimChannelLinearFloat*>(channel);
				Assign(block->m_SegmentSize, linearChannel->m_SegmentSize);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimPacker::ConvertLegacyChannel(crAnimChunk* chunk, const crAnimChannel* input)
{
	switch(input->GetType())
	{
	case crAnimChannel::AnimChannelTypeStaticQuaternion:
		{
			const crAnimChannelStaticQuaternion* channel = static_cast<const crAnimChannelStaticQuaternion*>(input);
			chunk->m_Channels.Resize(4);
			for(u32 i=0; i < 4; ++i)
			{
				crAnimChannelStaticFloat* newChannel = rage_new crAnimChannelStaticFloat();
				newChannel->m_Float = (*channel->m_Quaternion)[i];
				chunk->m_Channels[i] = newChannel;
			}
			delete channel;
			break;
		}

	case crAnimChannel::AnimChannelTypeRawVector3:
		{
			const crAnimChannelRawVector3* channel = static_cast<const crAnimChannelRawVector3*>(input);
			chunk->m_Channels.Resize(3);
			u32 numValues = channel->m_Vectors.GetCount();
			for(u32 i=0; i < 3; ++i)
			{
				crAnimChannelRawFloat* newChannel = rage_new crAnimChannelRawFloat();
				for(u32 j=0; j < numValues; ++j)
				{
					newChannel->m_Floats.Grow() = channel->m_Vectors[j][i];
				}
				chunk->m_Channels[i] = newChannel;
			}
			delete channel;
			break;
		}

	case crAnimChannel::AnimChannelTypeRawQuaternion:
		{
			const crAnimChannelRawQuaternion* channel = static_cast<const crAnimChannelRawQuaternion*>(input);
			chunk->m_Channels.Resize(4);
			u32 numValues = channel->m_Quaternions.GetCount();
			for(u32 i=0; i < 4; ++i)
			{
				crAnimChannelRawFloat* newChannel = rage_new crAnimChannelRawFloat();
				for(u32 j=0; j < numValues; ++j)
				{
					newChannel->m_Floats.Grow() = channel->m_Quaternions[j][i];
				}
				chunk->m_Channels[i] = newChannel;
			}
			delete channel;
			break;
		}

	case crAnimChannel::AnimChannelTypeSmallestThreeQuaternion:
		{
			const crAnimChannelSmallestThreeQuaternion* channel = static_cast<const crAnimChannelSmallestThreeQuaternion*>(input);
			chunk->m_Channels.Resize(3);
			for(u32 i=0; i < 3; ++i)
			{
				const crAnimChannelSmallestThreeQuaternion::QuantizedFloats& f = channel->m_QuantizedFloats[i];
				if(f.m_ScaleAndValues)
				{
					crAnimChannelQuantizeFloat* newChannel = rage_new crAnimChannelQuantizeFloat();
					newChannel->m_Offset = f.m_Offset;
					newChannel->m_Scale = f.m_ScaleAndValues->m_Scale;
					newChannel->m_QuantizedValues = f.m_ScaleAndValues->m_QuantizedValues;
					chunk->m_Channels[i] = newChannel;
				}
				else
				{
					crAnimChannelStaticFloat* newChannel = rage_new crAnimChannelStaticFloat();
					newChannel->m_Float = f.m_Offset;
					chunk->m_Channels[i] = newChannel;
				}
			}
			chunk->m_Reconstruct = u8(channel->m_QuantizedOrder);
			delete channel;
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimPacker::AddChannel(const crAnimChannel* input, u32 trackIdx, u32 channelIdx, atArray<u16>* indices, atArray<u32,0,u32>* constants, u32& quantizeSize, u32& indirectSize)
{
	switch(input->GetType())
	{
	case crAnimChannel::AnimChannelTypeStaticSmallestThreeQuaternion:
		{
			const crAnimChannelStaticSmallestThreeQuaternion* channel = static_cast<const crAnimChannelStaticSmallestThreeQuaternion*>(input);
			constants[crBlockStream::kConstantQuat].Grow() = reinterpret_cast<const u32&>(channel->m_X);
			constants[crBlockStream::kConstantQuat].Grow() = reinterpret_cast<const u32&>(channel->m_Y);
			constants[crBlockStream::kConstantQuat].Grow() = reinterpret_cast<const u32&>(channel->m_Z);
			indices[crBlockStream::kConstantQuat].Grow() = CreateChannelIndex(trackIdx, channelIdx);
			break;
		}

	case crAnimChannel::AnimChannelTypeStaticVector3:
		{
			const crAnimChannelStaticVector3* channel = static_cast<const crAnimChannelStaticVector3*>(input);
			for(u32 i=0; i < 3; ++i)
			{
				constants[crBlockStream::kConstantVec].Grow() = reinterpret_cast<const u32&>((*channel->m_Vector)[i]);
			}
			indices[crBlockStream::kConstantVec].Grow() = CreateChannelIndex(trackIdx, channelIdx);
			break;
		}

	case crAnimChannel::AnimChannelTypeStaticFloat:
		{
			const crAnimChannelStaticFloat* channel = static_cast<const crAnimChannelStaticFloat*>(input);
			constants[crBlockStream::kConstant].Grow() = reinterpret_cast<const u32&>(channel->m_Float);
			indices[crBlockStream::kConstant].Grow() = CreateChannelIndex(trackIdx, channelIdx);
			break;
		}

	case crAnimChannel::AnimChannelTypeRawFloat:
		{
			indices[crBlockStream::kRaw].Grow() = CreateChannelIndex(trackIdx, channelIdx);
			break;
		}

	case crAnimChannel::AnimChannelTypeQuantizeFloat:
		{
			const crAnimChannelQuantizeFloat* channel = static_cast<const crAnimChannelQuantizeFloat*>(input);
			const atPackedArray& values = channel->m_QuantizedValues;
			constants[crBlockStream::kQuantize].Grow() = values.m_ElementSize;
			constants[crBlockStream::kQuantize].Grow() = reinterpret_cast<const u32&>(channel->m_Scale);
			constants[crBlockStream::kQuantize].Grow() = reinterpret_cast<const u32&>(channel->m_Offset);
			indices[crBlockStream::kQuantize].Grow() = CreateChannelIndex(trackIdx, channelIdx);
			quantizeSize += values.m_ElementSize;
			break;
		}

	case crAnimChannel::AnimChannelTypeIndirectQuantizeFloat:
		{
			const crAnimChannelIndirectQuantizeFloat* channel = static_cast<const crAnimChannelIndirectQuantizeFloat*>(input);
			u32 numValues = channel->m_QuantizedValues.GetElementCount();
			constants[crBlockStream::kIndirect].Grow() = channel->m_QuantizedIndices.m_ElementSize;
			constants[crBlockStream::kIndirect].Grow() = channel->m_QuantizedValues.m_ElementSize;
			constants[crBlockStream::kIndirect].Grow() = numValues;
			constants[crBlockStream::kIndirect].Grow() = reinterpret_cast<const u32&>(channel->m_Scale);
			constants[crBlockStream::kIndirect].Grow() = reinterpret_cast<const u32&>(channel->m_Offset);
			for(u32 i=0; i < numValues; ++i)
			{
				constants[crBlockStream::kIndirect].Grow() = channel->m_QuantizedValues.m_Elements[i];
			}
			indices[crBlockStream::kIndirect].Grow() = CreateChannelIndex(trackIdx, channelIdx);
			indirectSize += numValues+5;
			break;
		}

	case crAnimChannel::AnimChannelTypeLinearFloat:
		{
			const crAnimChannelLinearFloat* channel = static_cast<const crAnimChannelLinearFloat*>(input);
			const atPackedArray& addresses = channel->m_Addresses, initials = channel->m_Initials;
			const atPackedSequence& values = channel->m_Values;
			u32 numBits = 4*sizeof(u32)*8 + addresses.GetElementMax()*addresses.GetElementSize() + initials.GetElementMax()*initials.GetElementSize() + values.GetSequenceMax();
			u32 numWords = (numBits+31)>>5;

			// build bit stream with all the channel data
			BaseStreamer stream;
			stream.GrowBits(32, numWords);
			stream.GrowBits(32, values.m_Divisor<<16 | initials.GetElementSize()<<8 | addresses.GetElementSize());
			stream.GrowBits(32, reinterpret_cast<const u32&>(channel->m_Scale));
			stream.GrowBits(32, reinterpret_cast<const u32&>(channel->m_Offset));
			for(u32 i=0; i < addresses.GetElementMax(); i++)
			{
				stream.GrowBits(addresses.GetElementSize(), addresses.GetElement(i));
			}
			for(u32 i=0; i < initials.GetElementMax(); i++)
			{
				stream.GrowBits(initials.GetElementSize(), initials.GetElement(i));
			}
			u32 lastSequence = values.GetSequenceMax() >> 5;
			for(u32 i=0; i < lastSequence; i++)
			{
				stream.GrowBits(32, values.m_Sequence[i]);
			}
			stream.GrowBits(values.GetSequenceMax()&31, values.m_Sequence[lastSequence]);

			// copy bit stream to the constant stream
			for(u32 i=0; i < numWords; i++)
			{
				constants[crBlockStream::kLinear].Grow() = reinterpret_cast<const u32*>(stream.GetData())[i];
			}
			indices[crBlockStream::kLinear].Grow() = CreateChannelIndex(trackIdx, channelIdx);
			break;
		}

	default: Assertf(false, "Unsupported animation channel %d", input->GetType());
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimPacker::CreateFrame(u16 chunkIdx, u32 frameIdx, BaseStreamer& frame, const crAnimation& anim, atArray<u16>* channels)
{
	for(u32 i=0; i < crBlockStream::kNumType; ++i)
	{
		atArray<u16>& indices = channels[i];
		for(u32 j=0; j < u32(indices.GetCount()); ++j)
		{
			u32 idx = indices[j];
			u32 trackIdx = idx>>2;
			u16 channelIdx = idx&3;
			const crAnimTrack* track = anim.GetTrack(trackIdx);
			const crAnimChunk* chunk = track->GetChunk(chunkIdx);
			switch(i)
			{
			case crBlockStream::kRaw:
				{
					const crAnimChannelRawFloat* channel = static_cast<const crAnimChannelRawFloat*>(chunk->GetChannel(channelIdx));
					frame.Grow(sizeof(f32), &channel->m_Floats[frameIdx]);
					break;
				}

			case crBlockStream::kQuantize:
				{
					const crAnimChannelQuantizeFloat* channel = static_cast<const crAnimChannelQuantizeFloat*>(chunk->GetChannel(channelIdx));
					const atPackedArray& values = channel->m_QuantizedValues;
					u32 sample = values.GetElement(frameIdx);
					frame.GrowBits(values.m_ElementSize, sample);
					break;
				}

			case crBlockStream::kIndirect:
				{
					const crAnimChannelIndirectQuantizeFloat* channel = static_cast<const crAnimChannelIndirectQuantizeFloat*>(chunk->GetChannel(channelIdx));
					u32 index = channel->m_QuantizedIndices.GetElement(frameIdx);
					frame.GrowBits(channel->m_QuantizedIndices.m_ElementSize, index);
					break;
				}
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

void crAnimPacker::Compact(Streamer<crBlockStream>& block, u32 dataSize)
{
	// Try to compress and get a working slop size
	u32 compressBufferSize = Min(dataSize, u32(CR_DEFAULT_MAX_BLOCK_SIZE));
	u8* compressBuffer = rage_new u8[compressBufferSize];
	u8* decompressBuffer = rage_new u8[2*dataSize];

	memcpy(decompressBuffer, block->m_Data, dataSize);

	int slopSize = -1;
	const u32 slideSize = 1024*4;
	u32 compressSize = datCompress(compressBuffer, compressBufferSize, decompressBuffer, dataSize, slideSize);
	if(compressSize > 0)
	{
		const u32 slopSearchStep = 16;
		for(u32 slop=0; slop<compressSize; slop+=slopSearchStep)
		{
			u32 offset = dataSize - compressSize + slop;
			memcpy(decompressBuffer + offset, compressBuffer, compressSize);
			if(datDecompressInPlace(decompressBuffer, dataSize, offset, compressSize))
			{
				slopSize = slop;
				break;
			}
		}
	}

	// Create new compact block
	if(slopSize >= 0 && compressSize < dataSize)
	{
		Streamer<crBlockStream> compactBlock;
		*compactBlock = *block;
		compactBlock->m_CompactSize = compressSize;
		Assign(compactBlock->m_SlopSize, slopSize);
		compactBlock.Grow(compressSize, compressBuffer);
		block = compactBlock;

		BYTESWAP(block->m_CompactSize);
		BYTESWAP(block->m_SlopSize);
	}

	delete [] compressBuffer;
	delete [] decompressBuffer;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimPacker::Swap(Streamer<crBlockStream>& block)
{
	u32* words = reinterpret_cast<u32*>(block->m_Data);
	u32 count = (block->m_ConstantSize+block->m_NumFrames*block->m_FrameSize)>>2;
	for(u32 i=0; i < count; ++i)
	{
		BYTESWAP(words[i]);
	}

	u16* indices = reinterpret_cast<u16*>(words + count);
	u16* indicesEnd = reinterpret_cast<u16*>(reinterpret_cast<u8*>(block.GetPtr()) + block->m_ChannelOffset);
	u32 numIndices = u32(indicesEnd - indices);
	for(u32 i=0; i < numIndices; ++i)
	{
		BYTESWAP(indices[i]);
	}

	crBlockStream::Channel* channels = reinterpret_cast<crBlockStream::Channel*>(indicesEnd);
	u32 numChannels = (block->m_NumMoverChannels >> 4) + (block->m_NumMoverChannels & 15);
	for(u32 i=0; i < numChannels; ++i)
	{
		crBlockStream::Channel& ch = channels[i];
		BYTESWAP(ch.m_Type);
		BYTESWAP(ch.m_Component);
		BYTESWAP(ch.m_Offset);
		BYTESWAP(ch.m_Address);
	}

	// computed hash on swapped block
	block->m_Hash = atDataHash((const char*)block->m_Data, block->m_DataSize);

	BYTESWAP(block->m_Hash);
	BYTESWAP(block->m_DataSize);
	BYTESWAP(block->m_CompactSize);
	BYTESWAP(block->m_ChannelOffset);
	BYTESWAP(block->m_SlopSize);
	BYTESWAP(block->m_NumFrames);
	BYTESWAP(block->m_FrameSize);
	BYTESWAP(block->m_ConstantSize);
	BYTESWAP(block->m_IndirectSize);
	BYTESWAP(block->m_QuantizeSize);
	BYTESWAP(block->m_SegmentSize);
	BYTESWAP(block->m_NumMoverChannels);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
