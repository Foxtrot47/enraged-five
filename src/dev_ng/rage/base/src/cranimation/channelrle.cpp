//
// cranimation/channelrle.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "channelrle.h"

#if CR_DEV
#include "animarchive.h"
#include "animation.h"
#include "animstream.h"
#include "animtolerance.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crAnimChannelRleInt::crAnimChannelRleInt()
: crAnimChannel(AnimChannelTypeRleInt)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimChannelRleInt::~crAnimChannelRleInt()
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelRleInt, crAnimChannel::AnimChannelTypeRleInt, crAnimTolerance::kCompressionCostMed, crAnimTolerance::kDecompressionCostMed);

////////////////////////////////////////////////////////////////////////////////

void crAnimChannelRleInt::EvaluateInt(float time, int& outInt) const
{
	// TODO --- optional support of blending on int data!
	int frame = int(time+0.5f);

	u32 addr = 0;

	const int numVals = m_Values.GetCount();
	for(int i=0; i<numVals; ++i)
	{
		frame -= m_Runs.GetValue(addr);
		if(frame < 0)
		{
			outInt = m_Values[i];
			return;
		}
	}

	outInt = m_Values[numVals-1];
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelRleInt::CreateInt(int* ints, int frames)
{
	// run length encoding not supported on data with no runs
	if(frames < 2)
	{
		return false;
	}

	// scan for runs of identical values
	int last = ints[0];
	int run = 1;
	atArray<s32> runs;

	for(int i=1; i<frames; ++i)
	{
		if(last == ints[i])
		{
			run++;
		}
		else
		{
			m_Values.Grow() = last;
			runs.Grow() = run;

			run=1;
			last=ints[i];
		}
	}

	m_Values.Grow() = last;
	runs.Grow() = run;

	u32 lowestAddressMax;
	if(!m_Runs.InitAndCreate(runs, lowestAddressMax))
	{
		return false;
	}

	// TODO --- packed sequence needs unsigned option
	// TODO --- need to add option of windows to jump into sequence mid stream

	return true;
}

////////////////////////////////////////////////////////////////////////////////

int crAnimChannelRleInt::ComputeSize(void) const
{
	return 2*sizeof(u32) + sizeof(int) * m_Values.GetCount() + m_Runs.ComputeSize();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChannelRleInt::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	u16 numValues = u16(m_Values.GetCount());
	a<<=numValues;

	if(a.IsLoading())
	{
		m_Values.Resize(numValues);
	}

	for(int i=0; i<numValues; ++i)
	{
		int val = 0;
		if(!a.IsLoading())
		{
			val = m_Values[i];
		}
		a<<=val;
		if(a.IsLoading())
		{
			m_Values[i] = val;
		}
	}

	m_Runs.Serialize(a);

	// TODO --- windows
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
