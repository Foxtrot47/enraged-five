//
// cranimation/weightset.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_WEIGHTSET_H
#define CRANIMATION_WEIGHTSET_H

#include "atl/string.h"
#include "data/base.h"

#include "cranimation/animation_config.h"

namespace rage
{

class bkBank;
class crSkeletonData;

#if CR_DEV
class crDumpOutput;
#endif // CRDEV


// PURPOSE: Animation weight set
// This structure is used to provide a weight value per bone contained
// within a skeleton.  It's used by various classes and interfaces
// to control behavior on a per bone basis.
class crWeightSet : public datBase
{
public:
	
	// PURPOSE: Default constructor
	crWeightSet();

	// PURPOSE: Initializing constructor
	// PARAMS: skelData - const reference to skeleton data
	crWeightSet(const crSkeletonData& skelData);

	// PURPOSE: Initializing constructor
	// PARAMS: maxBones - number of bones to store weights for
	crWeightSet(int maxBones);

	// PURPOSE: Resource constructor
	crWeightSet(datResource &rsc);

	// PURPOSE: Copy constructor
	crWeightSet(const crWeightSet&);

	// PURPOSE: Destructor
	~crWeightSet();

	// PURPOSE: Placement
	DECLARE_PLACE(crWeightSet);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Assignment operator
	const crWeightSet& operator=(const crWeightSet&);


	// PURPOSE: Initializer
	// PARAMS: skelData - const reference to skeleton data
	void Init(const crSkeletonData& skelData);

	// PURPOSE: Shutdown, free/release resources
	void Shutdown();


	// PURPOSE: Allocate and load an animation weight set
	// PARAMS: name - filename
	// skelData - const reference to skeleton data
	// RETURNS: newly allocated and loaded weight set (or NULL if load failed)
	static crWeightSet* AllocateAndLoad(const char* name, const crSkeletonData& skelData);

	// PURPOSE: Load an animation weight set
	// PARAMS: name - filename
	// skelData - const reference to skeleton data
	// RETURNS: true if load operation successful, false if load failed
	bool Load(const char* name, const crSkeletonData& skelData);

	// PURPOSE: Save animation weight set
	// PARAMS: name - filename
	// RETURNS: true if save operation successful, false if save failed
	bool Save(const char* name) const;

	// PURPOSE: Serialize animation weight set
	// PARAMS: s - stream
	void Serialize(datSerialize& s);

	// PURPOSE: Get animation weight set name
	// RETURNS: const atString reference (converts to const char*)
	const atString& GetName() const;

	// PURPOSE: Get number of weights inside this animation weight set
	// RETURNS: Number of weights (should match bones count in associated skeleton data)
	u32 GetNumAnimWeights() const;

	// PURPOSE: Get animation weight
	// PARAMS: weightIdx - animation weight index
	// RETURNS: current weight
    float GetAnimWeight(int weightIdx) const;

	// PURPOSE: Set animation weight
	// PARAMS: weightIdx - animation weight index
	// newWeight - new animation weight
	// andChildren - set this weight on all the children of this bone too
	void SetAnimWeight(int weightIdx, float newWeight, bool andChildren=false);

	// PURPOSE: Get pointer to animation weights
	// RETURNS: const pointer to first weight in set, will return NULL if no weights
	const float* GetAnimWeights() const;

	// PURPOSE: Set weights from weight set
	// PARAMS: weightSet - source weights set (must have matching number of weights)
	void Set(const crWeightSet& weightSet);


	// PURPOSE: const operator[]
	// SEE ALSO: Get animation weight
	const float& operator[](int weightIdx) const;

	// PURPOSE: non-const operator[]
	// SEE ALSO: Set animation weight
	float& operator[](int weightIdx);


	// PURPOSE: Bank widgets
#if __BANK
	void AddWidgets(bkBank& bk);
#endif // __BANK

	// PURPOSE: Output information about this point cloud for debugging
	// PARAMS: output - dump object
#if CR_DEV
	void Dump(crDumpOutput& output) const;
#endif // CR_DEV

private:

	// PURPOSE: Internal assignment operation
	void CopyThis(const crWeightSet&);


	atString m_Name;
	atArray<float> m_Weights;

	datRef<const crSkeletonData> m_SkelData;
};


// inline functions

////////////////////////////////////////////////////////////////////////////////

inline const atString& crWeightSet::GetName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crWeightSet::GetNumAnimWeights() const
{
	return m_Weights.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline float crWeightSet::GetAnimWeight(int weightIdx) const
{
	return m_Weights[weightIdx];
}

////////////////////////////////////////////////////////////////////////////////

inline const float* crWeightSet::GetAnimWeights() const
{
	if(m_Weights.GetCount())
	{
		return &m_Weights[0];
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline const float& crWeightSet::operator[](int weightIdx) const
{
	return m_Weights[weightIdx];
}

////////////////////////////////////////////////////////////////////////////////

inline float& crWeightSet::operator[](int weightIdx)
{
	return m_Weights[weightIdx];
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
#endif // CRANIMATION_WEIGHTSET_H
