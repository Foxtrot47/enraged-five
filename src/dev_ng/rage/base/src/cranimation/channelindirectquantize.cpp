//
// cranimation/channelindirectquantize.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "channelindirectquantize.h"

#if CR_DEV
#include "animarchive.h"
#include "animation.h"
#include "animstream.h"
#include "animtolerance.h"
#include "channelquantize.h"

#include "atl/hashsimple.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelIndirectQuantizeFloat, crAnimChannel::AnimChannelTypeIndirectQuantizeFloat, crAnimTolerance::kCompressionCostMed, crAnimTolerance::kDecompressionCostMed);

////////////////////////////////////////////////////////////////////////////////

crAnimChannelIndirectQuantizeFloat::crAnimChannelIndirectQuantizeFloat()
: crAnimChannel(AnimChannelTypeIndirectQuantizeFloat)
, m_Scale(0.f)
, m_Offset(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimChannelIndirectQuantizeFloat::~crAnimChannelIndirectQuantizeFloat()
{
}

////////////////////////////////////////////////////////////////////////////////

float crAnimChannelIndirectQuantizeFloat::ReconstructFloat(int it) const
{
	u32 idx = m_QuantizedIndices.GetElement(Clamp(u32(it), u32(0), m_QuantizedIndices.GetElementMax()-1));
	u32 qv = m_QuantizedValues.GetElement(idx);

	return (float(qv) * m_Scale) + m_Offset;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChannelIndirectQuantizeFloat::EvaluateFloat(float ft, float& out) const
{
	int it = int(floor(ft));
	float fract = ft - float(it);

	if(fract > (1.f - INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE))
	{
		it++;
	}
	else if(fract > INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE)
	{
		out = Lerp(fract, ReconstructFloat(it), ReconstructFloat(it+1));
		return;
	}

	out = ReconstructFloat(it);
}

////////////////////////////////////////////////////////////////////////////////

struct ValueIndex
{
	inline bool IsEqual(const ValueIndex* other) const
	{
		return m_Val == other->m_Val;
	}

	inline u32 GenerateHash() const
	{
		return m_Val;
	}

	u32 m_Val, m_Idx;
};

////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelIndirectQuantizeFloat::CreateFloat(float* floats, int frames, int skip, float tolerance)
{
	atPackedArray quantizedValues;
	if(crAnimChannelQuantizeFloat::QuantizeFloats(floats, frames, skip, tolerance, quantizedValues, m_Scale, m_Offset))
	{
		atArray<u32> values, indices;
		SimpleHash<ValueIndex> valueIndices;

		values.Reserve(frames);
		indices.Resize(frames);
		valueIndices.Init(frames);

		for(int i=0; i<frames; i++)
		{
			u32 qv = quantizedValues.GetElement(i);

			ValueIndex entry;
			entry.m_Val = qv;

			const ValueIndex* found = valueIndices.Search(&entry);
			if(found)
			{
				indices[i] = found->m_Idx;
			}
			else
			{
				u32 idx = values.GetCount();
				indices[i] = idx;
				values.Append() = qv;
				entry.m_Idx = idx;
				valueIndices.Insert(entry);
			}
		}

		u32 numBits = 0;
		u32 numValues = u32(values.GetCount());
		while(numValues > 0)
		{
			numValues >>= 1;
			numBits++;
		}

		m_QuantizedIndices.Init(numBits, indices);
		m_QuantizedValues.Init(quantizedValues.GetElementSize(), values);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

int crAnimChannelIndirectQuantizeFloat::ComputeSize(void) const
{
	return 5*sizeof(u32) + m_QuantizedIndices.ComputeSize() + m_QuantizedValues.ComputeSize();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChannelIndirectQuantizeFloat::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	a<<=m_Scale;
	a<<=m_Offset;

	m_QuantizedIndices.Serialize(a);
	m_QuantizedValues.Serialize(a);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
