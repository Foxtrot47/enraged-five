//
// cranimation/commonpool.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_COMMONPOOL_H
#define CRANIMATION_COMMONPOOL_H

#include "atl/pool.h"
#include "system/criticalsection.h"
#include "system/tinyheap.h"

#define COMMON_POOL_THREAD_SAFE (1)

namespace rage
{

// PURPOSE: Common pool used to allocate relocatable frame and frame data
class crCommonPool
{
public:

	// PURPOSE: Default constructor
	crCommonPool();

	// PURPOSE: Destructor
	~crCommonPool();

	// PURPOSE: Initialization
	// PARAMS:
	// maxNumElems - maximum number of element that can be simultaneously allocated by this pool
	// size - size of buffer, in bytes
	void Init(u32 maxNumElems, u32 bufferSize);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Allocate an element and its associated buffer
	// PARAMS:
	// outBuffer - output for the allocated buffer
	// elemSize - size of the element
	// bufferSize  - size of the buffer
	void* Allocate(u8** outBuffer, u32 elemSize, u32 bufferSize);

	// PURPOSE: Release an element and its associated buffer
	// PARAMS:
	// elem - pointer to element
	// buffer - pointer to buffer of this element
	void Release(void* elem, u8* buffer);

	// PURPOSE: Memory compaction
	// PARAMS:
	// maxToCompact - maximum number of bytes that can be moved in one pass
	// NOTES: Call only when no motion tree processing is occurring
	void Compact(u32 maxToCompact);

	// PURPOSE: Get maximum number of elements
	u32 GetSize() const;

protected:

	// PURPOSE: Private copy constructor to prevent copying
	crCommonPool(const crCommonPool&);
	crCommonPool& operator=(const crCommonPool&);

	// PURPOSE: Internal callback
	static void CompactCallback(void* ptr, ptrdiff_t diff);

#if COMMON_POOL_THREAD_SAFE
	sysCriticalSectionToken m_CsToken;
#endif // COMMON_POOL_THREAD_SAFE

	// PURPOSE: Allocator for variable sized buffers
	static const u32 sm_HeaderSize = 16;
	sysTinyHeap m_Heap;
	u8* m_Buffer;

	// PURPOSE: Pool for fixed sized elements
	static const u32 sm_ElemSize = __64BIT ? 48:32;
	typedef u8 Elem[sm_ElemSize];
	atPool<Elem> m_Pool;
};

} // namespace rage

#endif // CRANIMATION_COMMONPOOL_H