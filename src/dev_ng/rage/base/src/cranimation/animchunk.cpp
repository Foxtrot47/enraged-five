//
// cranimation/animchunk.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "animchunk.h"

#if CR_DEV
#include "animation.h"
#include "animarchive.h"
#include "animchannel.h"
#include "animtrack.h"
#include "channelbasic.h"
#include "channelquantize.h"
#include "channelsmallestthree.h"

#include "system/nelem.h"

#include "crskeleton/skeletondata.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crAnimChunk::crAnimChunk()
: m_Format(0)
, m_Reconstruct(FORMAT_RECONSTRUCT_NONE)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimChunk::~crAnimChunk()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChunk::Shutdown()
{
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChunk::Serialize(crArchive& a)
{
	a<<=m_Track;
	a<<=m_Format;
	a<<=m_Id;

	if(a.GetVersion() < 15)
	{
		m_Reconstruct = u8(GetPack() == FORMAT_PACK_QUATERNION_XYZ_RECONSTRUCT ? FORMAT_RECONSTRUCT_W : FORMAT_RECONSTRUCT_NONE);
	}
	else
	{
		a<<=m_Reconstruct;
	}

	u16 nChannels = u16(m_Channels.GetCount());
	a<<=nChannels;

	for(int i=0; i<nChannels; i++)
	{
		crAnimChannel* channel=NULL;
		u8 channelType;

		if(a.IsStoring())
		{
			channel = m_Channels[i];
			channelType = channel->GetType();
		}
		a<<=channelType;
		if(a.IsLoading())
		{
			channel = crAnimChannel::CreateChannel(channelType);
			m_Channels.Append() = channel;
		}

		channel->Serialize(a);
	}
}

////////////////////////////////////////////////////////////////////////////////

crAnimChunk* crAnimChunk::Clone() const
{
	crAnimChunk* chunk = rage_new crAnimChunk();
	chunk->m_Track = m_Track;
	chunk->m_Format = m_Format;
	chunk->m_Id = m_Id;
	chunk->m_Reconstruct = m_Reconstruct;
	
	chunk->m_Channels.Resize(m_Channels.GetCount());
	for(int i=0; i<m_Channels.GetCount(); ++i)
	{
		chunk->m_Channels[i] = NULL;

		if(!chunk->m_Channels[i])
		{
			chunk->m_Channels[i] = m_Channels[i]->Clone();
		}
	}

	return chunk;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChunk::CreateVector3(u8 track, u16 id, Vec3V* vectors, int num, const ToleranceHelper& toleranceHelper)
{
	Reset();

	m_Track = track;
	m_Id = id;
	m_Format = kFormatTypeVector3;

	float maxError = toleranceHelper.GetMaxAbsoluteError();
	crAnimTolerance::eCompressionCost compressCost = toleranceHelper.GetMaxCompressionCost();
	crAnimTolerance::eDecompressionCost decompressCost = toleranceHelper.GetMaxDecompressionCost();

	crAnimChannelStaticVector3* staticVec = rage_new crAnimChannelStaticVector3();
	if(staticVec->CreateVector3(vectors, num, maxError))
	{
		m_Channels.Append() = staticVec;
		return;
	}
	else
	{
		delete staticVec;
	}

	// only try decomposing the vector if max costs are low or greater
	if(compressCost >= crAnimTolerance::kCompressionCostLow &&
		decompressCost >= crAnimTolerance::kDecompressionCostLow)
	{
		m_Format = kFormatTypeVector3 | FORMAT_PACK_VECTOR3_XYZ;

		// try a three channels approach
		for(int i=0; i<3; i++)
		{
			m_Channels.Append() = CreateFloatChannel(&(vectors[0][i]), num, 3, 1.f, toleranceHelper);
		}
	}
	else
	{
		m_Format = kFormatTypeVector3 | FORMAT_PACK_VECTOR3_RAW;

		// if this fails, fall back on raw storage - always works
		crAnimChannel* channel = rage_new crAnimChannelRawVector3();
		channel->CreateVector3(vectors, num, maxError);
		m_Channels.Append() = channel;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChunk::CreateQuaternion(u8 track, u16 id, QuatV* quaternions, int num, const ToleranceHelper& toleranceHelper)
{
	Reset();

	m_Track = track;
	m_Id = id;
	m_Format = kFormatTypeQuaternion;

	float maxError = toleranceHelper.GetMaxAbsoluteError();
	crAnimTolerance::eCompressionCost compressCost = toleranceHelper.GetMaxCompressionCost();
	crAnimTolerance::eDecompressionCost decompressCost = toleranceHelper.GetMaxDecompressionCost();

	crAnimChannelStaticSmallestThreeQuaternion* staticQuat = rage_new crAnimChannelStaticSmallestThreeQuaternion();
	if(staticQuat->CreateQuaternion(quaternions, num, maxError))
	{
		m_Channels.Append() = staticQuat;
		return;
	}
	else
	{
		delete staticQuat;
	}

	// if costs are low or above, try decomposing into float channels
	if(compressCost >= crAnimTolerance::kCompressionCostLow &&
		decompressCost >= crAnimTolerance::kDecompressionCostLow)
	{
		u32 bestSize = UINT_MAX;
		for(u8 i=0; i < 5; ++i)
		{
			OptimizeTolerance(bestSize, i, &quaternions[0], num, 1.f, toleranceHelper);
		}
	}

	// if this fails, fall back on raw storage - always works
	if(m_Channels.IsEmpty())
	{
		crAnimChannel* channel = rage_new crAnimChannelRawQuaternion();
		channel->CreateQuaternion(quaternions, num, maxError);
		m_Format = kFormatTypeQuaternion | FORMAT_PACK_QUATERNION_RAW;
		m_Reconstruct = FORMAT_RECONSTRUCT_NONE;
		m_Channels.Append() = channel;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChunk::OptimizeTolerance(u32& bestSize, u8 reconstruct, QuatV* quaternions, int num, float maxErrorFactor, const ToleranceHelper& toleranceHelper)
{
	const float minFactor = 1.f;
	const float maxFactor = 3.f;
	const float stepFactor = 0.5f;

	float maxError = toleranceHelper.GetMaxAbsoluteError() * maxErrorFactor;

	float* floats = reinterpret_cast<float*>(&quaternions[0]);
	ScalarV error = ScalarVFromF32(maxError/2.f);

	// Change the quaternion to have a positive missing channel
	if(reconstruct != FORMAT_RECONSTRUCT_NONE && quaternions[0][reconstruct] < 0.f)
	{
		quaternions[0] = Negate(quaternions[0]);
	}

	// Prepare all quaternions
	for(int i=0; i<num-1; ++i)
	{
		quaternions[i+1] = PrepareSlerp(quaternions[i], quaternions[i+1]);
	}

	crAnimChunk chunk;
	chunk.m_Reconstruct = reconstruct;
	chunk.m_Format = kFormatTypeQuaternion | FORMAT_PACK_QUATERNION_XYZ_RECONSTRUCT;

	for(float factor = minFactor; factor <= maxFactor; factor += stepFactor)
	{
		// Construct new channels with tolerance factor
		float toleranceFactor = maxErrorFactor / factor;
		for(int i=0; i<4; i++)
		{
			if(i != reconstruct)
			{
				chunk.m_Channels.Append() = chunk.CreateFloatChannel(floats+i, num, 3, toleranceFactor, toleranceHelper);
			}
		}

		// Check if we respect the tolerance
		int j=0;
		for(; j < num; ++j)
		{
			QuatV q;
			chunk.EvaluateQuaternion(float(j), q);
			if(!IsCloseAll(quaternions[j], q, error) && !IsCloseAll(quaternions[j], Negate(q), error))
				break;
		}

		// If we succeed, we keep the channels if they are smaller
		if(j==num)
		{
			u32 size = chunk.ComputeSize();
			if(size < bestSize)
			{
				m_Format = chunk.m_Format;
				m_Channels = chunk.m_Channels;
				m_Reconstruct = chunk.m_Reconstruct;
				bestSize = size;
				chunk.m_Channels.Reset();
			}
			break;
		}

		chunk.Reset();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChunk::CreateFloat(u8 track, u16 id, float* floats, int num, const ToleranceHelper& toleranceHelper)
{
	Reset();

	m_Track = track;
	m_Id = id;
	m_Format = kFormatTypeFloat | FORMAT_PACK_FLOAT_RAW;

	float* maxErrors = Alloca(float, num);
	for(int i=0; i<num; ++i)
	{
		maxErrors[i] = toleranceHelper.GetMaxAbsoluteError(i);
	}

	m_Channels.Append() = CreateFloatChannel(floats, num, 0, 1.f, toleranceHelper);
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimChunk::ComputeSize() const
{
	u32 size = sizeof(crAnimChunk);
	u32 numChannels = m_Channels.GetCount();
	for(u32 i=0; i<numChannels; i++)
	{
		size += m_Channels[i]->ComputeSize();
	}

	return size;
}

////////////////////////////////////////////////////////////////////////////////

crAnimChannel* crAnimChunk::CreateFloatChannel(float* floats, int num, int skip, float maxErrorFactor, const ToleranceHelper& toleranceHelper) const
{
	float maxError = toleranceHelper.GetMaxAbsoluteError() * maxErrorFactor;

	// try static channel first
	crAnimChannel* staticChannel = crAnimChannel::CreateChannel(crAnimChannel::AnimChannelTypeStaticFloat);
	if(staticChannel->CreateFloat(floats, num, skip, maxError))
	{
		return staticChannel;
	}
	delete staticChannel;

	crAnimTolerance::eCompressionCost compressCost = toleranceHelper.GetMaxCompressionCost();
	crAnimTolerance::eDecompressionCost decompressCost = toleranceHelper.GetMaxDecompressionCost();

	// if not static, select optimal channel
	static const u8 channels[] = {
		crAnimChannel::AnimChannelTypeLinearFloat,
		crAnimChannel::AnimChannelTypeIndirectQuantizeFloat,
		crAnimChannel::AnimChannelTypeQuantizeFloat
	};
	const f32 biases[] = { toleranceHelper.GetLinearBias(), 1.f, 1.f };
	CompileTimeAssert(NELEM(channels) == NELEM(biases));

	crAnimChannel* optimalChannel = NULL;
	float optimalRatio = FLT_MAX;
	int numCompressionStrategies = NELEM(channels);
	for(int i=0; i<numCompressionStrategies; i++)
	{
		const crAnimChannel::ChannelTypeInfo* info = crAnimChannel::GetChannelTypeInfo(channels[i]);
		AssertMsg(info , "CreateFloatChannel - failed to find type info on compression strategy");
		if(info->m_CompressCost <= compressCost && info->m_DecompressCost <= decompressCost)
		{
			crAnimChannel* newChannel = crAnimChannel::CreateChannel(channels[i]);

			if(newChannel->CreateFloat(floats, num, skip, maxError))
			{
				float ratio = newChannel->ComputeSize() * biases[i];
				if(ratio <= optimalRatio)
				{
					delete optimalChannel;
					optimalChannel = newChannel;	
					optimalRatio = ratio;
				}
			}

			if(optimalChannel != newChannel)
			{
				delete newChannel;
			}
		}
	}

	if(optimalChannel)
	{
		return optimalChannel;
	}

	// fall back to raw channels if everything fails
	crAnimChannel* rawChannel = crAnimChannel::CreateChannel(crAnimChannel::AnimChannelTypeRawFloat);
	AssertVerify(rawChannel->CreateFloat(floats, num, skip, maxError));
	return rawChannel;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChunk::Reset()
{
	int numChannels = m_Channels.GetCount();
	for(int i=0; i<numChannels; i++)
	{
		delete m_Channels[i];
	}
	m_Channels.Reset();
}

////////////////////////////////////////////////////////////////////////////////


};  // namespace rage

#endif // CR_DEV