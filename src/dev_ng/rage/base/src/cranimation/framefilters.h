//
// cranimation/framefilters.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRANIMATION_FRAMEFILTERS_H
#define CRANIMATION_FRAMEFILTERS_H

#include "atl/array.h"
#include "atl/map.h"
#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "math/simplemath.h"
#include "system/interlocked.h"
#include "data/safestruct.h"

namespace rage
{

class crBoneData;
class crWeightSet;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// Base frame filter class, all frame filters must be derived from this.
// NOTES:
// Filters allow per dof control over participation (and weight) of individual
// dofs in almost all frame operations.
class crFrameFilter
{
public:

	// PURPOSE: Enumeration of built in frame filter types
	enum eFrameFilterType
	{
		kFrameFilterTypeNone = -1,

		kFrameFilterType = 0,
		kFrameFilterTypeBone,
		kFrameFilterTypeBoneBasic,
		kFrameFilterTypeBoneMultiWeight,
		kFrameFilterTypeMultiWeight,
		kFrameFilterTypeTrackMultiWeight,
		kFrameFilterTypeMover,
		kFrameFilterTypeBlendShape,
		kFrameFilterTypeExpression,
		kFrameFilterTypeMoveTransition,
		kFrameFilterTypeShaderVar,

		// must follow RAGE filter enumeration
		kFrameFilterTypeRageFilterNum,

		// base values for higher level filters
		kFrameFilterTypeFrameworkFilters = 0x40,
		kFrameFilterTypeProjectFilters = 0x80,
	};


	// PURPOSE: Constructor
	crFrameFilter(eFrameFilterType filterType=kFrameFilterTypeNone);

	// PURPOSE: Resource constructor
	crFrameFilter(datResource &rsc);

	// PURPOSE: Destructor
	virtual ~crFrameFilter();

	// PURPOSE: Placement
	DECLARE_PLACE(crFrameFilter);

	// PURPOSE: Off line resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Clone frame filter
	virtual crFrameFilter* Clone() const = 0;

	// PURPOSE: Allocate a frame filter by type
	static crFrameFilter* Allocate(eFrameFilterType filterType);

	// PURPOSE: Allocate and load a filter frame file
	// RETURNS: Newly allocated filter frame, or NULL if load operation fails
	static crFrameFilter* AllocateAndLoad(const char* filename);

	// PURPOSE: Load a filter frame file
	// PARAMS:
	// filename - .expr file to load
	// pack - pack expressions into contiguous buffers (default false)
	// RETURNS: true - if load operation succeeds, false - if load operation fails
	bool Load(const char* filename);

	// PURPOSE: Save a frame filter file
	// PARAMS: filename - filename to use when saving
	// RETURNS: true - if save operation succeeds, false - if save operation fails
	// NOTES: If filename is NULL, will attempt to use same filename used when loading
	bool Save(const char* filename) const;

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE: Off line resource version
	static const int RORC_VERSION = 4;

	// PURPOSE: RTTI for frame filters
	// NOTES: Prefer using crFrameFilter::DynamicCast<typename> if speculatively casting
	// to other filter pointer types as it uses the class names directly (avoiding the enum types).
	virtual bool IsCastableToType(eFrameFilterType filterType) const;

	// PURPOSE: Dynamic cast for frame filter
	// NOTES: Will return NULL if filter cannot be safely recast
	template<typename _T> const _T* DynamicCast() const;
	template<typename _T> _T* DynamicCast();

	// PRUPOSE: Get frame filter type
	eFrameFilterType GetType() const;

#if CR_DEV
	// PURPOSE: Get frame filter type name as string
	const char* GetTypeName() const;
#endif

	// PURPOSE: Individual dof filter callback, override to implement frame filter
	// operations on a per dof level.  Default implementation permits everything.
	// PARAMS:
	// track - track index of dof (see crAnimTrack::TRACK_xxx)
	// id - id of dof (meaning depends on track)
	// inoutWeight - current weight applied to dof
	// RETURNS:
	// true - don't filter dof (though a weight of 0.0 may still result in dof being filtered)
	// false - filter the dof
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

	// PURPOSE: Individual dof filter callback, override to implement frame filter
	// operations on a per dof level.  Default implementation permits everything.
	// This is an extended version that provides an optional filter of the filter.
	// PARAMS:
	// track - track index of dof (see crAnimTrack::TRACK_xxx)
	// id - id of dof (meaning depends on track)
	// inoutWeight - current weight applied to dof
	// filter - optional filter of the filter (can be NULL)
	// RETURNS:
	// true - don't filter dof (though a weight of 0.0 may still result in dof being filtered)
	// false - filter the dof
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight, crFrameFilter* filter);

	// PURPOSE: TODO --- provide binary dof filtering as well?
	// virtual bool IsBinary() const;

	// PURPOSE: Get filter signature.
	// RETURNS: Current filter signature, which is a hash uniquely identifying
	// what the filter does.  Zero implies no valid signature.
	u32 GetSignature() const;

	// PURPOSE: Widgets
#if __BANK
	virtual void AddWidgets(bkBank&);
#endif // __BANK

	// PURPOSE: Get current reference count
	// RETURNS: Reference count
	u32 GetRef() const;

	// PURPOSE: Take out an additional reference
	void AddRef() const;

	// PURPOSE: Release a reference (will destroy object, if references now zero)
	// RETURNS: New reference count
	u32 Release() const;


	// PURPOSE: Init class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();

	// PURPOSE: Definition of frame filter allocation call
	typedef crFrameFilter* AllocateFn();

	// PURPOSE: Definition of frame filter placement call
	typedef void PlaceFn(crFrameFilter*, datResource&);

	// PURPOSE: Frame filter type information
	struct TypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: filterType - filter type identifier
		// (see crFrameFilter::eFrameFilterType enumeration for built in filter types)
		// name - filter type name string
		// allocateFn - filter type allocation call
		// placeFn - filter type resource placement call
		TypeInfo(eFrameFilterType filterType, const char* name, AllocateFn* allocateFn, PlaceFn* placeFn);

		// PURPOSE: Destructor
		~TypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get filter type identifier
		// RETURNS: filter type identifier
		// (see crFrameFilter::eFrameFilterType enumeration for built in filter types)
		eFrameFilterType GetType() const;

		// PURPOSE: Get filter type name string
		// RETURNS: string of filter type name
		const char* GetName() const;

		// PURPOSE: Get filter allocation function
		// RETURNS: filter allocation function pointer
		AllocateFn* GetAllocateFn() const;

		// PURPOSE: Get filter resource placement function
		// RETURNS: filter resource placement function pointer
		PlaceFn* GetPlaceFn() const;

	private:
		eFrameFilterType m_Type;
		const char* m_Name;
		AllocateFn* m_AllocateFn;
		PlaceFn* m_PlaceFn;		
	};

	// PURPOSE: Get info about a filter type
	// PARAMS: filterType - filter type identifier
	// (see crFrameFilter::eFrameFilterType enumeration for built in filter types)
	// RETURNS: const pointer to filter type info structure (may be NULL if filter type unknown)
	static const TypeInfo* FindTypeInfo(eFrameFilterType type);

	// PURPOSE: Get info about this filter
	// RETURNS: const reference to filter type info structure about this filter
	virtual const TypeInfo& GetTypeInfo() const = 0;

	// PURPOSE: Declare functions necessary to register a new filter type
	#define CR_DECLARE_FRAME_FILTER_TYPE(__typename) \
		static crFrameFilter* AllocateFrameFilter(); \
		static void PlaceFrameFilter(crFrameFilter* base, datResource& rsc); \
		virtual crFrameFilter* Clone() const; \
		virtual bool IsCastableToType(eFrameFilterType filterType) const; \
		static void InitClass(); \
		virtual const TypeInfo& GetTypeInfo() const; \
		static const crFrameFilter::TypeInfo sm_TypeInfo;

	// PURPOSE: Implement functions necessary to register a new filter type
	#define CR_IMPLEMENT_FRAME_FILTER_TYPE(__typename, __typeid, __basename) \
		crFrameFilter* __typename::AllocateFrameFilter() \
		{ \
			__typename* filter = rage_new __typename; \
			return filter; \
		} \
		void __typename::PlaceFrameFilter(crFrameFilter* base, datResource& rsc) \
		{ \
			::new (base) __typename(rsc); \
		} \
		crFrameFilter* __typename::Clone() const \
		{ \
			return rage_new __typename(*this); \
		} \
		bool __typename::IsCastableToType(eFrameFilterType filterType) const \
		{ \
			return (filterType == __typeid) || __basename::IsCastableToType(filterType); \
		} \
		void __typename::InitClass() \
		{ \
			sm_TypeInfo.Register(); \
		} \
		const crFrameFilter::TypeInfo& __typename::GetTypeInfo() const \
		{ \
			return sm_TypeInfo; \
		} \
		const crFrameFilter::TypeInfo __typename::sm_TypeInfo(__typeid, CR_DEV ? #__typename :  NULL, AllocateFrameFilter, PlaceFrameFilter);

protected:

	// PURPOSE: Calculate filter signature.
	// RETURNS: Current filter signature, which is a hash uniquely identifying
	// what the filter does.  Zero implies no valid signature.
	virtual u32 CalcSignature() const = 0;

	// PURPOSE: Set filter signature.
	// PARAMS: signature - a unique hash of the data that controls the filter operation.
	void SetSignature(u32 signature);

protected:

	// PURPOSE: Register a new filter type (only call from TypeInfo::Register)
	// PARAMS: filter type info (must be global/class static, persist for entire execution)
	static void RegisterTypeInfo(const TypeInfo& info);

private:

	mutable u32 m_RefCount;
	u32 m_Signature;
	eFrameFilterType m_Type;

	static atArray<const TypeInfo*> sm_TypeInfos;
	static bool sm_InitClassCalled;

protected:
	static int s_FrameFilterVersion;

public:
	static const char* s_FileExtension;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// Common base class for filters involving bones.
class crFrameFilterBone : public crFrameFilter
{
protected:

	// PURPOSE: Internal constructor for derived classes only
	crFrameFilterBone(eFrameFilterType filterType);

public:

	// PURPOSE: Constructor
	crFrameFilterBone();

	// PURPOSE: Resource constructor
	crFrameFilterBone(datResource &rsc);

	// PURPOSE: Destructor
	virtual ~crFrameFilterBone();

	// PURPOSE: Register frame filter
	CR_DECLARE_FRAME_FILTER_TYPE(crFrameFilterBone)

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE: Are non-bone dofs allowed by this filter?
	// RETURNS: true - non-bone dofs allowed, false - non-bone dofs filtered
	bool GetNonBoneDofsAllowed() const;

	// PURPOSE: Set if non-bone dofs are allowed by this filter
	// PARAMS: allow - true to allow non-bone dofs, false to filter them
	void SetNonBoneDofsAllowed(bool allow);

	// PURPOSE: Widgets
#if __BANK
	virtual void AddWidgets(bkBank&);
#endif // __BANK

protected:

	// PURPOSE: Internal function, recalculate and set the signature
	virtual u32 CalcSignature() const;

	bool m_NonBoneDofsAllowed;
	datPadding<3> m_Padding;
	// TODO --- needs to be more complex, filter movers/blend shapes etc
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE:
// A basic bone DOF callback, with data structure.  Allows filtering
// of any frame operation on a per dof (bone) level.
class crFrameFilterBoneBasic : public crFrameFilterBone
{
public:

	// PURPOSE: Default constructor
	crFrameFilterBoneBasic();

	// PURPOSE: Constructor
	// PARAMS: skelData - skeleton data to use with this filter
	crFrameFilterBoneBasic(const crSkeletonData& skelData);

	// PURPOSE: Constructor
	// PARAMS: maxBones - maximum number of bones in skeletons this filter will operate on
	crFrameFilterBoneBasic(int maxBones);

	// PURPOSE: Resource constructor
	crFrameFilterBoneBasic(datResource &rsc);

	// PURPOSE: Destructor
	virtual ~crFrameFilterBoneBasic();

	// PURPOSE: Initializer
	// PARAMS: skelData - skeleton data to use with this filter
	void Init(const crSkeletonData& skelData);

	// PURPOSE: Initializer
	// PARAMS: maxBones - maximum number of bones in skeletons this filter will operate on
	void Init(int maxBones);

	// PURPOSE: Register frame filter
	CR_DECLARE_FRAME_FILTER_TYPE(crFrameFilterBoneBasic)

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE: Add a weight set
	// PARAMS:
	// weightSet - weight set to add (must have same number of bones as the filter)
	void AddWeightSet(const crWeightSet& weightSet);

	// PURPOSE:	Add a bone (and optionally it's children) to the filter, with a particular weight.
	// PARAMS:
	// bd - pointer to the bone data.
	// andChildren - in addition to adding the bone, add all it's children.
	// weight - weight to use when adding the bone.
	void AddBone(const crBoneData* bd, bool andChildren=false, float weight=1.f);

	// PURPOSE: Add a bone (and optionally it's children) to the filter, with a particular weight.
	// PARAMS:
	// boneIdx - index of bone.
	// andChildren - in addition to adding the bone, add all it's children.
	// weight - weight to use when adding the bone.
	void AddBoneIndex(int boneIdx, bool andChildren=false, float weight=1.f);

	// PURPOSE: Add a bone (and optionally it's children) to the filter, with a particular weight.
	// PARAMS:
	// boneId - id of bone.
	// andChildren - in addition to adding the bone, add all it's children.
	// weight - weight to use when adding the bone.
	void AddBoneId(u16 boneId, bool andChildren=false, float weight=1.f);

	// PURPOSE: Removes a bone (and optionally it's children) from the filter.
	// PARAMS:
	// bd - pointer to the bone data.
	// andChildren - in addition to removing the bone, remove all it's children.
	// weight - weight to use when adding the bone.
	void RemoveBone(const crBoneData* bd, bool andChildren=false);

	// PURPOSE:	Removes a bone (and optionally it's children) from the filter.
	// PARAMS:
	// boneIdx - index of bone.
	// andChildren - in addition to removing the bone, remove all it's children.
	// weight - weight to use when adding the bone.
	void RemoveBoneIndex(int boneIdx, bool andChildren=false);

	// PURPOSE: Removes a bone (and optionally it's children) from the filter.
	// PARAMS:
	// boneId - id of bone.
	// andChildren - in addition to removing the bone, remove all it's children.
	// weight - weight to use when adding the bone.
	void RemoveBoneId(u16 boneId, bool andChildren=false);

	// PURPOSE: Invert all the weights in the filter.
	// NOTES:
	// new weight = 1.0 - old weight.  ie:
	// 0.0 -> 1.0
	// 1.0 -> 0.0
	// 0.25 -> 0.75
	void InvertWeights();

	// PURPOSE: Widgets
#if __BANK
	virtual void AddWidgets(bkBank&);
#endif // __BANK

	// PURPOSE: Filter dof override
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

protected:

	// PURPOSE: Internal constructor for derived classes only
	crFrameFilterBoneBasic(eFrameFilterType filterType);

	// PURPOSE: Internal function, recalculate and set the signature
	virtual u32 CalcSignature() const;

	datRef<const crSkeletonData> m_SkelData;
	datOwner<crWeightSet> m_WeightSet;
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE:
// A bone DOF callback, with data structure.  Allows filtering
// of any frame operation on a per dof (bone) level.
// NOTES:
// Designed for partial body blending, allows several weights
// to control blending on different [non-overlapping] sets of bones
class crFrameFilterBoneMultiWeight : public crFrameFilterBone
{
public:

	// PURPOSE: Default constructor
	crFrameFilterBoneMultiWeight();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// skelData - skeleton data to use with this filter
	// numWeightGroups - number of different weight groups to support
	crFrameFilterBoneMultiWeight(const crSkeletonData& skelData, int numWeightGroups);

	// PURPOSE: Resource constructor
	crFrameFilterBoneMultiWeight(datResource &rsc);

	// PURPOSE: Destructor
	virtual ~crFrameFilterBoneMultiWeight();

	// PURPOSE: Initializer
	// PARAMS:
	// skelData - skeleton data to use with this filter
	// numWeightGroups - number of different weight groups to support
	void Init(const crSkeletonData& skelData, int numWeightGroups);
	
	// PURPOSE: Register frame filter
	CR_DECLARE_FRAME_FILTER_TYPE(crFrameFilterBoneMultiWeight)

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE:
	// Add a bone (and optionally it's children) to the filter, with a particular weight group index.
	// PARAMS:
	// bd - pointer to the bone data.
	// weightGroupIdx - weight group index.
	// andChildren - in addition to adding the bone, add all it's children.
	// NOTES:
	// If the bone (or it's children) previously belonged to a different weight group,
	// their membership will be changed to the supplied weight group.
	// A bone be a member of at most one group at a time.
	void AddBone(const crBoneData* bd, int weightGroupIdx, bool andChildren=false);

	// PURPOSE:
	// Add a bone (and optionally it's children) to the filter, with a particular weight group index.
	// PARAMS:
	// boneIdx - index of bone.
	// weightGroupIdx - weight group index.
	// andChildren - in addition to adding the bone, add all it's children.
	// NOTES:
	// If the bone (or it's children) previously belonged to a different weight group,
	// their membership will be changed to the supplied weight group.
	// A bone be a member of at most one group at a time.
	void AddBoneIndex(int boneIdx, int weightGroupIdx, bool andChildren=false);

	// PURPOSE:
	// Add a bone (and optionally it's children) to the filter, with a particular weight group index.
	// PARAMS:
	// boneId - id of bone.
	// weightGroupIdx - weight group index.
	// andChildren - in addition to adding the bone, add all it's children.
	// NOTES:
	// If the bone (or it's children) previously belonged to a different weight group,
	// their membership will be changed to the supplied weight group.
	// A bone be a member of at most one group at a time.
	void AddBoneId(u16 boneId, int weightGroupIdx, bool andChildren=false);

	// PURPOSE:
	// Remove a bone (and optionally it's children) from the filter.
	// PARAMS:
	// bd - pointer to the bone data.
	// andChildren - in addition to removing the bone, remove all it's children.
	void RemoveBone(const crBoneData* bd, bool andChildren=false);

	// PURPOSE:
	// Remove a bone (and optionally it's children) from the filter.
	// PARAMS:
	// boneIdx - index of bone.
	// andChildren - in addition to removing the bone, remove all it's children.
	void RemoveBoneIndex(int boneIdx, bool andChildren=false);

	// PURPOSE:
	// Remove a bone (and optionally it's children) from the filter.
	// PARAMS:
	// boneId - id of bone.
	// andChildren - in addition to removing the bone, remove all it's children.
	void RemoveBoneId(u16 boneId, bool andChildren=false);

	// PURPOSE:
	// Return the number of weight groups.
	// RETURNS:
	// The number of weight groups.
	int GetNumWeightGroups() const;

	// PURPOSE:
	// Return the controlling weight value.
	// PARAMS:
	// weightGroupIdx - the index of the weight group.
	// RETURNS:
	// The current value of the controlling weight.
	float GetWeight(int weightGroupIdx) const;

	// PURPOSE:
	// Sets the controlling weight value.
	// PARAMS:
	// weight - the new controlling weight value.
	// weightGroupIdx - the index of the weight group.
	void SetWeight(float weight, int weightGroupIdx);

	// PURPOSE: Filter dof override
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

protected:

	// PURPOSE: Internal function, recalculate and set the signature
	virtual u32 CalcSignature() const;

	datRef<const crSkeletonData> m_SkelData;
	atArray<int> m_WeightIndices;
	atArray<float> m_Weights;
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE:
// A DOF callback, with data structure.  Allows filtering
// of any frame operation on a per DOF level.
// NOTES:
// Designed to work with bone and non-bone based DOFs.
// Allows grouping DOFs into sets, and controlling each set with independent weights.
class crFrameFilterMultiWeight : public crFrameFilter
{
protected:

	// PURPOSE: Internal constructor for derived classes only
	crFrameFilterMultiWeight(eFrameFilterType filterType);

public:

	// PURPOSE: Default constructor
	crFrameFilterMultiWeight();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// numWeightGroups - number of distinct weight groups
	// skelData - optional pointer to skeleton data structure (required for bone based calls)
	crFrameFilterMultiWeight(int numWeightGroups, const crSkeletonData* skelData=NULL);

	// PURPOSE: Resource constructor
	crFrameFilterMultiWeight(datResource &rsc);

	// PURPOSE: Destructor
	virtual ~crFrameFilterMultiWeight();

	// PURPOSE: Initialization
	// PARAMS:
	// numWeightGroups - number of distinct weight groups
	// skelData - optional pointer to skeleton data structure (required for bone based calls)
	void Init(int numWeightGroups, const crSkeletonData* skelData=NULL);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Register frame filter
	CR_DECLARE_FRAME_FILTER_TYPE(crFrameFilterMultiWeight)

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE: Add track/id pair to weight group
	// PARAMS:
	// track/id - track id pair to add (for tracks see crAnimTrack::TRACK_XXX enumeration)
	// weightGroupIdx - weight group index to add this pair too
	void Add(u8 track, u16 id, int weightGroupIdx);

	// PURPOSE: Add bone DOF (and optionally child bones) to weight group by bone index
	// PARAMS:
	// boneIdx - bone index
	// translation, rotation, scale - select which type of bone DOFs to add
	// andChildren - include all bones which are descendants of this bone
	// weightGroupIdx - weight group index to add these bone DOF(s) too
	// NOTES: skelData must have been specified at initialization for this call to function
	void AddBoneIndex(int boneIdx, bool translation, bool rotation, bool scale, bool andChildren, int weightGroupIdx);

	// PURPOSE: Add bone DOF (and optionally child bones) to weight group by bone id
	// boneId - bone id
	// translation, rotation, scale - select which type of bone DOFs to add
	// andChildren - include all bones which are descendants of this bone
	// weightGroupIdx - weight group index to add these bone DOF(s) too
	// NOTES: skelData must have been specified at initialization for this call to function
	void AddBoneId(u16 boneId, bool translation, bool rotation, bool scale, bool andChildren, int weightGroupIdx);

	// PURPOSE: Add bone DOF (and optionally child bones) to weight group by bone data
	// boneData - bone data
	// translation, rotation, scale - select which type of bone DOFs to add
	// andChildren - include all bones which are descendants of this bone
	// weightGroupIdx - weight group index to add these bone DOF(s) too
	// NOTES: skelData must have been specified at initialization for this call to function
	void AddBoneData(const crBoneData& boneData, bool translation, bool rotation, bool scale, bool andChildren, int weightGroupIdx);


	// PURPOSE: Remove track/id pair from weight group
	// PARAMS:
	// track/id - track id pair to remove (for tracks see crAnimTrack::TRACK_XXX enumeration)
	void Remove(u8 track, u16 id);

	// PURPOSE: Remove bone DOF (and optionally child bones) from weight group by bone index
	// boneIdx - bone index
	// translation, rotation, scale - select which type of bone DOFs to remove
	// andChildren - include all bones which are descendants of this bone
	// NOTES: skelData must have been specified at initialization for this call to function
	void RemoveBoneIndex(int boneIdx, bool translation, bool rotation, bool scale, bool andChildren);

	// PURPOSE: Remove bone DOF (and optionally child bones) from weight group by bone id
	// boneId - bone id
	// translation, rotation, scale - select which type of bone DOFs to remove
	// andChildren - include all bones which are descendants of this bone
	// NOTES: skelData must have been specified at initialization for this call to function
	void RemoveBoneId(u16 boneId, bool translation, bool rotation, bool scale, bool andChildren);

	// PURPOSE: Remove bone DOF (and optionally child bones) from weight group by bone data
	// boneData - bone data
	// translation, rotation, scale - select which type of bone DOFs to remove
	// andChildren - include all bones which are descendants of this bone
	// NOTES: skelData must have been specified at initialization for this call to function
	void RemoveBoneData(const crBoneData& boneData, bool translation, bool rotation, bool scale, bool andChildren);



	// PURPOSE:
	// Return the number of weight groups.
	// RETURNS:
	// The number of weight groups.
	int GetNumWeightGroups() const;

	// PURPOSE:
	// Return the controlling weight value.
	// PARAMS:
	// weightGroupIdx - the index of the weight group.
	// RETURNS:
	// The current value of the controlling weight.
	float GetWeight(int weightGroupIdx) const;

	// PURPOSE:
	// Sets the controlling weight value.
	// PARAMS:
	// weight - the new controlling weight value.
	// weightGroupIdx - the index of the weight group.
	void SetWeight(int weightGroupIdx, float weight);

	// PURPOSE: Filter DOF override
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

	int GetEntryCount() const;

	void GetEntry(int i, u8 &track, u16 &id, int &index, float &weight) const;

protected:

	// PURPOSE: Internal function, recalculate and set the signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Internal function, find track/id pair index (or insertion point)
	bool FindTrackIdIndex(u8 track, u16 id, int& outIdx);

	// PURPOSE:
	struct TrackIdIndex
	{
		// PURPOSE: Default constructor
		TrackIdIndex();

		// PURPOSE: Initializing constructor
		TrackIdIndex(u8 track, u16 id, int index);

		// PURPOSE: Resource constructor
		TrackIdIndex(datResource&);

		// PURPOSE: Destructor
		~TrackIdIndex();

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(TrackIdIndex);

		// PURPOSE: Resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Serialize an frame filter file (for internal use only)
		void Serialize(datSerialize& s);

		datPadding<1> m_Padding;

		u8 m_Track;
		u16 m_Id;

		int m_Index;
	};

	atArray<TrackIdIndex> m_TrackIdIndices;
	atArray<float> m_Weights;

	datRef<const crSkeletonData> m_SkelData;
};

////////////////////////////////////////////////////////////////////////////////


// PURPOSE:
// A DOF callback, with data structure.  Allows filtering
// of any frame operation on a per track level.
// NOTES:
// Designed to work with all DOF types, grouping by track type.
class crFrameFilterTrackMultiWeight : public crFrameFilter
{
public:

	// PURPOSE: Default constructor
	crFrameFilterTrackMultiWeight();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// numWeightGroups - number of distinct weight groups
	crFrameFilterTrackMultiWeight(int numWeightGroups);

	// PURPOSE: Resource constructor
	crFrameFilterTrackMultiWeight(datResource &rsc);

	// PURPOSE: Destructor
	virtual ~crFrameFilterTrackMultiWeight();

	// PURPOSE: Initialization
	// PARAMS:
	// numWeightGroups - number of distinct weight groups
	void Init(int numWeightGroups);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Register frame filter
	CR_DECLARE_FRAME_FILTER_TYPE(crFrameFilterTrackMultiWeight)

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE: Add track to weight group
	// PARAMS:
	// track - track to add (see crAnimTrack::TRACK_XXX enumeration)
	// weightGroupIdx - weight group index to add this pair too
	void Add(u8 track, int weightGroupIdx);


	// PURPOSE: Remove track from weight group
	// PARAMS:
	// track - track to remove (see crAnimTrack::TRACK_XXX enumeration)
	void Remove(u8 track);


	// PURPOSE:
	// Return the number of weight groups.
	// RETURNS:
	// The number of weight groups.
	int GetNumWeightGroups() const;

	// PURPOSE:
	// Return the controlling weight value.
	// PARAMS:
	// weightGroupIdx - the index of the weight group.
	// RETURNS:
	// The current value of the controlling weight.
	float GetWeight(int weightGroupIdx) const;

	// PURPOSE:
	// Sets the controlling weight value.
	// PARAMS:
	// weight - the new controlling weight value.
	// weightGroupIdx - the index of the weight group.
	void SetWeight(int weightGroupIdx, float weight);

	// PURPOSE: Filter DOF override
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

protected:

	// PURPOSE: Internal function, recalculate and set the signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Internal function, find track index (or insertion point)
	bool FindTrackIndex(u8 track, int& outIdx);

	// PURPOSE:
	struct TrackIndex
	{
		// PURPOSE: Default constructor
		TrackIndex();

		// PURPOSE: Initializing constructor
		TrackIndex(u8 track, int index);

		// PURPOSE: Resource constructor
		TrackIndex(datResource&);

		// PURPOSE: Destructor
		~TrackIndex();

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(TrackIndex);

		// PURPOSE: Resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Serialize an frame filter file (for internal use only)
		void Serialize(datSerialize& s);

		datPadding<3> m_Padding;

		u8 m_Track;

		int m_Index;
	};

	atArray<TrackIndex> m_TrackIndices;
	atArray<float> m_Weights;
};

////////////////////////////////////////////////////////////////////////////////


// PURPOSE:
// Specialized filter for filtering degrees of freedom on a mover
class crFrameFilterMover : public crFrameFilter
{
public:

	// PURPOSE: Default constructor
	crFrameFilterMover();

	// PURPOSE: Resource constructor
	crFrameFilterMover(datResource &rsc);

	// PURPOSE: Destructor
	virtual ~crFrameFilterMover();

	// PURPOSE: Register frame filter
	CR_DECLARE_FRAME_FILTER_TYPE(crFrameFilterMover)

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE: Get current mover dof filtering status
	// PARAMS: outTranslateWeight - weight currently applied by filter to mover translate dof
	// outRotateWeight - weight currently applied by filter to mover rotate dof
	void GetMoverDofWeights(float& outTranslateWeight, float& outRotateWeight) const;

	// PURPOSE: Set current mover dof filtering status
	// PARAMS: translateWeight - weight filter applies to mover translate dof
	// rotateWeight - weight filter applies to mover rotate dof
	void SetMoverDofWeights(float translateWeight, float rotateWeight);

	// PURPOSE: Get id of mover being filtered
	// RETURNS: mover id
	u16 GetMoverId() const;

	// PURPOSE: Set id of mover to filter
	// RETURNS: moverId - new mover id
	void SetMoverId(u16 moverId);

	// PURPOSE: Get filter behavior when encountering dofs other than the target mover
	// RETURNS: true - if other dofs allowed to pass filter (ie filter returns 1.0)
	// false - if other dofs are blocked by filter (ie filter returns 0.0)
	bool GetNonMoverDofsAllowed() const;

	// PURPOSE: Set filter behavior when encountering dofs other than the target mover
	// PARAMS: allow - true to allow other dofs passed filter, false - to block other dofs
	void SetNonMoverDofsAllowed(bool allow);

	// PURPOSE: Override filter callback
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

	// PURPOSE: Add bank widgets
#if __BANK
	virtual void AddWidgets(bkBank&);
#endif // __BANK

protected:

	// PURPOSE: Internal function, recalculate and set the signature
	virtual u32 CalcSignature() const;

	float m_MoverTranslation;
	float m_MoverRotation;
	u16 m_MoverId;
	bool m_NonMoverDofsAllowed;
	datPadding<1> m_Padding;
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE:
// Combines multiple frame filters into a single filter
// PARAMS:
// N - maximum number of filters to combines together
// refCount - adjust the reference counts on the filters (safer but reduces performance)
// cacheSig - cache the combined signatures of the sub filters (faster, but only use if sub filters are constant)
// NOTES:
// This filter combines up to N separate filters into a single filter.
template <u32 N, bool refCount=true, bool cacheSig=false>
class crFrameFilters : public crFrameFilter
{
public:

	// PURPOSE: Constructor
	crFrameFilters();

	// PURPOSE: Resource constructor
	crFrameFilters(datResource&);

	// PURPOSE: Destructor
	virtual ~crFrameFilters();

	// PURPOSE: Placement
	static void Place(crFrameFilters<N, refCount>* that, datResource& rsc);

#if __DECLARESTRUCT
	// PURPOSE: Off line resourcing
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Serialize an frame filter file (for internal use only)
	virtual void Serialize(datSerialize& s);

	// PURPOSE: Add a filter
	bool AddFilter(crFrameFilter& filter);

	// PURPOSE: Insert a filter
	bool InsertFilter(int idx, crFrameFilter&);

	// PURPOSE: Remove a filter
	bool RemoveFilter(crFrameFilter& filter);

	// PURPOSE: Remove a filter by index
	bool RemoveFilter(int idx);

	// PURPOSE: Remove all filters
	void RemoveAllFilters();

	// PURPOSE: Get a filter by index (const)
	const crFrameFilter* GetFilter(int idx) const;

	// PURPOSE: Get a filter by index (non-const)
	crFrameFilter* GetFilter(int idx);

	// PURPOSE: Find a filter
	// RETURNS: filter index, or -1 if not found
	int FindFilter(crFrameFilter&);

	// PURPOSE: Get current number of filters
	u32 GetNumFilters() const;

	// PURPOSE: Get maximum number of filters
	u32 GetMaxNumFilters() const;

	// PURPOSE: Override filter callback
	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

protected:

	// PURPOSE: Internal function, get the signature (recalculating, if !cacheSig)
	virtual u32 GetSignature() const;

	// PURPOSE: Internal function, recalculate the signature
	virtual u32 CalcSignature() const;


	atFixedArray< datRef<crFrameFilter>, N > m_Filters;

	static u32 sm_Hash;
};


////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline const _T* crFrameFilter::DynamicCast() const
{
	if(IsCastableToType(_T::sm_TypeInfo.GetType()))
	{
		return static_cast<const _T*>(this);
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline _T* crFrameFilter::DynamicCast()
{
	if(IsCastableToType(_T::sm_TypeInfo.GetType()))
	{
		return static_cast<_T*>(this);
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameFilter::eFrameFilterType crFrameFilter::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameFilter::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrameFilter::SetSignature(u32 signature)
{
	m_Signature = signature;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameFilter::GetRef() const
{
	return m_RefCount;
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrameFilter::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameFilter::Release() const
{
	u32 refCount = sysInterlockedDecrement(&m_RefCount);
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return refCount;
	}
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
crFrameFilters<N, refCount, cacheSig>::crFrameFilters()
{
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
crFrameFilters<N, refCount, cacheSig>::crFrameFilters(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
crFrameFilters<N, refCount, cacheSig>::~crFrameFilters()
{
	RemoveAllFilters();
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
void Place(crFrameFilters<N, refCount, cacheSig>* that, datResource& rsc)
{
	::new (that) crFrameFilters<N, refCount>(rsc);
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
template <u32 N, bool refCount, bool cacheSig>
void crFrameFilters<N, refCount, cacheSig>::DeclareStruct(datTypeStruct& s)
{
	crFrameFilter::DeclareStruct(s);
	typedef crFrameFilters<N, refCount> TemplatedFrameFilters;
	SSTRUCT_BEGIN_BASE(TemplatedFrameFilters, crFrameFilter)
	SSTRUCT_FIELD(TemplatedFrameFilters, m_Filters)
	SSTRUCT_END(TemplatedFrameFilters)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
void crFrameFilters<N, refCount, cacheSig>::Serialize(datSerialize& s)
{
	crFrameFilter::Serialize(s);
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
bool crFrameFilters<N, refCount, cacheSig>::AddFilter(crFrameFilter& filter)
{
	if(m_Filters.GetCount() < m_Filters.GetMaxCount())
	{
		if(refCount)
		{
			filter.AddRef();
		}

		m_Filters.Append() = &filter;

		if(cacheSig)
		{
			SetSignature(CalcSignature());
		}
		return true;
	}
	return false;
}


////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
bool crFrameFilters<N, refCount, cacheSig>::InsertFilter(int idx, crFrameFilter& filter)
{
	if(m_Filters.GetCount() < m_Filters.GetMaxCount())
	{
		if(refCount)
		{
			filter.AddRef();
		}

		m_Filters.Insert(idx) = &filter;

		if(cacheSig)
		{
			SetSignature(CalcSignature());
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
bool crFrameFilters<N, refCount, cacheSig>::RemoveFilter(crFrameFilter& filter)
{
	bool found = false;
	for(int i=0; i<m_Filters.GetCount(); ++i)
	{
		if(m_Filters[i] == &filter)
		{
			if(refCount)
			{
				m_Filters[i]->Release();
			}
			m_Filters.Delete(i--);
			found = true;
		}
	}

	if(found && cacheSig)
	{
		SetSignature(CalcSignature());
	}

	return found;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
bool crFrameFilters<N, refCount, cacheSig>::RemoveFilter(int idx)
{
	if(refCount)
	{
		m_Filters[idx]->Release();
	}

	m_Filters.Delete(idx);
	
	if(cacheSig)
	{
		SetSignature(CalcSignature());
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
void crFrameFilters<N, refCount, cacheSig>::RemoveAllFilters()
{
	if(refCount)
	{
		const int numFilters = m_Filters.GetCount();
		for(int i=0; i<numFilters; ++i)
		{
			crFrameFilter& filter = *m_Filters[i];
			filter.Release();
		}
	}
	m_Filters.Reset();

	if(cacheSig)
	{
		SetSignature(CalcSignature());
	}
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
const crFrameFilter* crFrameFilters<N, refCount, cacheSig>::GetFilter(int idx) const
{
	return m_Filters[idx];
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
crFrameFilter* crFrameFilters<N, refCount, cacheSig>::GetFilter(int idx)
{
	return m_Filters[idx];
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
int crFrameFilters<N, refCount, cacheSig>::FindFilter(crFrameFilter& filter)
{
	for(int i=0; i<m_Filters.GetCount(); ++i)
	{
		if(m_Filters[i] == &filter)
		{
			return i;
		}
	}

	return -1;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
u32 crFrameFilters<N, refCount, cacheSig>::GetNumFilters() const
{
	return m_Filters.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
u32 crFrameFilters<N, refCount, cacheSig>::GetMaxNumFilters() const
{
	return N;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
bool crFrameFilters<N, refCount, cacheSig>::FilterDof(u8 track, u16 id, float& inoutWeight)
{
	const int numFilters = m_Filters.GetCount();
	for(int i=0; i<numFilters; ++i)
	{
		crFrameFilter& filter = *m_Filters[i];
		if(!filter.FilterDof(track, id, inoutWeight))
		{
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
u32 crFrameFilters<N, refCount, cacheSig>::GetSignature() const
{
	if(cacheSig)
	{
		Assert(crFrameFilter::GetSignature() == CalcSignature());
		return crFrameFilter::GetSignature();
	}
	else
	{
		return CalcSignature();
	}
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
u32 crFrameFilters<N, refCount, cacheSig>::CalcSignature() const
{
	u32 signature = sm_Hash;

	const u32 numFilters = m_Filters.GetCount();
	signature ^= numFilters;
	for(u32 i=0; i<numFilters; ++i)
	{
		u32 filterSignature = m_Filters[i]->GetSignature();
		if(!filterSignature)
		{
			return 0;
		}

		const u32 rot = 7;
		signature = filterSignature ^ ((signature<<rot)|(signature>>(32-rot)));
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 N, bool refCount, bool cacheSig>
u32 crFrameFilters<N, refCount, cacheSig>::sm_Hash = atHash_const_char("crFrameFilters<>");

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRANIMATION_FRAMEFILTERS_H
