//
// cranimation/channelbspline.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELBSPLINE_H
#define CRANIMATION_CHANNELBSPLINE_H

#include "animchannel.h"

#if CR_DEV
namespace rage
{

// PURPOSE:
class crAnimChannelQuadraticBSpline : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelQuadraticBSpline();
	virtual ~crAnimChannelQuadraticBSpline();

	virtual void EvaluateFloat(float time, float& out) const;
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive&);

	DECLARE_ANIM_CHANNEL(crAnimChannelQuadraticBSpline);
private:
	atPackedArray m_Packed;
	float m_Multiplier;
	float m_Scale, m_Offset;
	int m_Count;
};

// PURPOSE:
class crAnimChannelCubicBSpline : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelCubicBSpline();
	virtual ~crAnimChannelCubicBSpline();

	virtual void EvaluateFloat(float time, float& out) const;
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive&);

	DECLARE_ANIM_CHANNEL(crAnimChannelCubicBSpline);
private:
	atPackedArray m_Packed;
	float m_Multiplier;
	float m_Scale, m_Offset;
	int m_Count;
};

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELBSPLINE_H