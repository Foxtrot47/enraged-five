//
// cranimation/weightmodifier.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CR_WEIGHTMODIFIER_H
#define CR_WEIGHTMODIFIER_H

#include "system/interlocked.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Weight modifier abstract base class
// Weight modifiers provide a mechanism for globally modifying
// weight parameters to things like frame operations with a class
// that wraps up both the operation and any parameters that drive it.
// Rage provides some commonly encountered modifying operations
// (eg slowinslowout), but projects are free to derive from this
// base class and implement their own custom modification operations.
class crWeightModifier
{
public:

	// PURPOSE: Default constructor
	crWeightModifier();

	// PURPOSE: Destructor
	virtual ~crWeightModifier();

	// TODO --- bunch of other stuff, placement, resourcing, banks etc...


	// PURPOSE: Override this to modify the weight
	// PARAMS: originalWeight - original weight [0..1]
	// RETURNS: modified weight [0..1]
	virtual float ModifyWeight(float originalWeight) const;


	// PURPOSE: Get current reference count
	// RETURNS: Reference count
	u32 GetRef() const;

	// PURPOSE: Take out an additional reference
	void AddRef() const;

	// PURPOSE: Release a reference (will destroy object, if references now zero)
	// RETURNS: New reference count
	u32 Release() const;

private:
	mutable u32 m_RefCount;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Slow in slow out weight modifier
// Supports slow in, slow out and slow in out weight modification.
class crWeightModifierSlowInSlowOut : public crWeightModifier
{
public:

	// PURPOSE: Default constructor
	crWeightModifierSlowInSlowOut();

	// PURPOSE: Initializing constructor
	crWeightModifierSlowInSlowOut(bool slowIn, bool slowOut);

	// PURPOSE: Destructor
	virtual ~crWeightModifierSlowInSlowOut();


	// PURPOSE: Override this to modify the weight
	virtual float ModifyWeight(float originalWeight) const;


	// PURPOSE: Set Slow In
	// PARAMS: slowIn - slow in
	// NOTES: Both slow in and slow out can be set simultaneously for slow in out
	void SetSlowIn(bool slowIn);

	// PURPOSE: Set Slow Out
	// PARAMS: slowOut - slow out
	// NOTES: Both slow in and slow out can be set simultaneously for slow in out
	void SetSlowOut(bool slowOut);

	// PURPOSE: Is Slow In
	// RETURNS: true - if slow in enabled
	bool IsSlowIn() const;

	// PURPOSE: Is Slow Out
	// RETURNS: true if slow out enabled
	bool IsSlowOut() const;

private:
	bool m_SlowIn;
	bool m_SlowOut;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Step weight modifier
// Converts linear input into step shaped output
class crWeightModifierStep : public crWeightModifier
{
public:

	// PURPOSE: Default constructor
	crWeightModifierStep();

	// PURPOSE: Initializing constructor
	crWeightModifierStep(float step);

	// PURPOSE: Destructor
	virtual ~crWeightModifierStep();


	// PURPOSE: Override this to modify the weight
	virtual float ModifyWeight(float originalWeight) const;


	// PURPOSE: Set step value
	// PARAMS: step - value at which step occurs [0..1]
	// NOTES: Modified weight will be 1.0 for all input weights >= step, and 0.0 otherwise
	void SetStep(float step);

	// PURPOSE: Get step value
	// RETURNS: value at which step occurs
	float GetStep() const;

private:
	float m_Step;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Skew weight modifier
// Compresses linear input into shorter linear transition
class crWeightModifierSkew : public crWeightModifier
{
public:

	// PURPOSE: Default constructor
	crWeightModifierSkew();

	// PURPOSE: Initializing constructor
	crWeightModifierSkew(float start, float end);

	// PURPOSE: Destructor
	virtual ~crWeightModifierSkew();


	// PURPOSE: Override this to modify the weight
	virtual float ModifyWeight(float originalWeight) const;


	// PURPOSE: Set skew values
	// PARAMS: start, end - values at which skew occurs [0..1]
	// NOTES: start must be less than or equal to end
	// Modified weight will be linear transition between start and end, 0.0 before start, 1.0 after end.
	void SetSkew(float start, float end);

	// PURPOSE: Get skew value
	// PARAMS: outStart, outEnd - values at which skew occurs
	void GetSkew(float& outStart, float& outEnd) const;

private:
	float m_Start;
	float m_End;
};


////////////////////////////////////////////////////////////////////////////////

inline u32 crWeightModifier::GetRef() const
{
	return m_RefCount;
}

////////////////////////////////////////////////////////////////////////////////

inline void crWeightModifier::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crWeightModifier::Release() const
{
	u32 refCount = sysInterlockedDecrement(&m_RefCount);
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return refCount;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void crWeightModifierSlowInSlowOut::SetSlowIn(bool slowIn)
{
	m_SlowIn = slowIn;
}

////////////////////////////////////////////////////////////////////////////////

inline void crWeightModifierSlowInSlowOut::SetSlowOut(bool slowOut)
{
	m_SlowOut = slowOut;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crWeightModifierSlowInSlowOut::IsSlowIn() const
{
	return m_SlowIn;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crWeightModifierSlowInSlowOut::IsSlowOut() const
{
	return m_SlowOut;
}

////////////////////////////////////////////////////////////////////////////////

inline void crWeightModifierStep::SetStep(float step)
{
	m_Step = step;
}

////////////////////////////////////////////////////////////////////////////////

inline float crWeightModifierStep::GetStep() const
{
	return m_Step;
}

////////////////////////////////////////////////////////////////////////////////

inline void crWeightModifierSkew::SetSkew(float start, float end)
{
	Assert(start <= end);

	m_Start = start;
	m_End = end;
}

////////////////////////////////////////////////////////////////////////////////

inline void crWeightModifierSkew::GetSkew(float& outStart, float& outEnd) const
{
	outStart = m_Start;
	outEnd = m_End;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_WEIGHTMODIFIER_H
