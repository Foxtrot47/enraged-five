//
// cranimation/animstream.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "animstream.h"

#include "math/amath.h"
#include "system/cache.h"

#include <limits.h>

namespace rage
{

CompileTimeAssert16(sizeof(crBlockStream));

using namespace Vec;

////////////////////////////////////////////////////////////////////////////////

const u32 g_MaskTable[] = { 0x0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff, 0x1ff, 0x3ff, 0x7ff, 0xfff, 0x1fff, 0x3fff, 0x7fff, 0xffff, 0x1ffff, 0x3ffff, 0x7ffff, 0xfffff, 0x001fffff, 0x003fffff, 0x007fffff, 0x00ffffff, 0x01ffffff, 0x03ffffff, 0x07ffffff, 0x0fffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff, 0xffffffff };

__forceinline u32 GetQuantizedValue(u32 address, const u32* data, u32 elemSize)
{
	u32 block = address >> 5, bit = address & 31;
	u64 word = data[block] | (u64(data[block+1])<<32);
	return u32((word>>bit) & g_MaskTable[elemSize]);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline s32 GetSequenceValue(u32 divisor, u32& address, const u32* sequence)
{
	u32 mask = g_MaskTable[divisor];

	// extract quantized remainder
	u32 block0 = address >> 5, bit0 = address & 31;
	u32 data0 = u32((sequence[block0] | (u64(sequence[block0+1])<<32)) >> bit0);
	u32 remainder = data0 & mask;

	//  decode Rice quotient
	u32 address1 = address + divisor;
	u32 block1 = address1 >> 5, bit1 = address1 & 31;
	u32 data1 = u32((sequence[block1] | (u64(sequence[block1+1])<<32)) >> bit1);
	u32 quotient = CountTrailingZeros(data1);

	// change sign
	u32 data2 = data1 >> (quotient+1);
	s32 neg = data2 & 1;
	u32 pow = quotient << divisor;
	s32 v =  pow | remainder;
	address += divisor + quotient + 1 + (v != 0);

	return neg ? -v : v;
}

////////////////////////////////////////////////////////////////////////////////

static void EvaluateConstantQuaternion(u32 count, const f32* constants, const u16* channels, u8* output, const u16* offsets)
{
	const Vector_4V _one = V4VConstant(V_ONE);
	const Vector_4V _min = V4VConstant(V_FLT_MIN);

	u32 count4 = count >> 2;
	for(; count4; count4--)
	{
		// load x y z components
		Vector_4V q0 = V4LoadUnaligned(&constants[0]);
		Vector_4V q1 = V4LoadUnaligned(&constants[3]);
		Vector_4V q2 = V4LoadUnaligned(&constants[6]);
		Vector_4V q3 = V4LoadUnaligned(&constants[9]);

		// compute w component
		Vector_4V sq0 = V4Subtract(_one, V3DotV(q0, q0));
		Vector_4V sq1 = V4Subtract(_one, V3DotV(q1, q1));
		Vector_4V sq2 = V4Subtract(_one, V3DotV(q2, q2));
		Vector_4V sq3 = V4Subtract(_one, V3DotV(q3, q3));

		Vector_4V positive0 = V4IsGreaterThanV(sq0, _min);
		Vector_4V positive1 = V4IsGreaterThanV(sq1, _min);
		Vector_4V positive2 = V4IsGreaterThanV(sq2, _min);
		Vector_4V positive3 = V4IsGreaterThanV(sq3, _min);

		Vector_4V comp0 = V4And(positive0, V4Scale(sq0, V4InvSqrt(sq0)));
		Vector_4V comp1 = V4And(positive1, V4Scale(sq1, V4InvSqrt(sq1)));
		Vector_4V comp2 = V4And(positive2, V4Scale(sq2, V4InvSqrt(sq2)));
		Vector_4V comp3 = V4And(positive3, V4Scale(sq3, V4InvSqrt(sq3)));

		// store quaternions
		Vector_4V* dest0 = reinterpret_cast<Vector_4V*>(output + offsets[channels[0]>>2]);
		Vector_4V* dest1 = reinterpret_cast<Vector_4V*>(output + offsets[channels[1]>>2]);
		Vector_4V* dest2 = reinterpret_cast<Vector_4V*>(output + offsets[channels[2]>>2]);
		Vector_4V* dest3 = reinterpret_cast<Vector_4V*>(output + offsets[channels[3]>>2]);

		*dest0 = V4PermuteTwo<X1,Y1,Z1,X2>(q0, comp0);
		*dest1 = V4PermuteTwo<X1,Y1,Z1,X2>(q1, comp1);
		*dest2 = V4PermuteTwo<X1,Y1,Z1,X2>(q2, comp2);
		*dest3 = V4PermuteTwo<X1,Y1,Z1,X2>(q3, comp3);

		constants += 12;
		channels += 4;
	}

	u32 count1 = count & 3;
	for(; count1; count1--)
	{
		Vector_4V q = V4LoadUnaligned(constants);
		Vector_4V sq = V4Subtract(_one, V3DotV(q, q));
		Vector_4V positive = V4IsGreaterThanV(sq, _min);
		Vector_4V comp = V4And(positive, V4Scale(sq, V4InvSqrt(sq)));

		Vector_4V* dest = reinterpret_cast<Vector_4V*>(output + offsets[channels[0]>>2]);

		*dest = V4PermuteTwo<X1,Y1,Z1,X2>(q, comp);

		constants += 3;
		channels += 1;
	}
}

////////////////////////////////////////////////////////////////////////////////

static void EvaluateConstantVector(u32 count, const f32* constants, const u16* channels, u8* output, const u16* offsets)
{
	u32 count4 = count >> 2;
	for(; count4; count4--)
	{
		Vector_4V v0 = V4LoadUnaligned(&constants[0]);
		Vector_4V v1 = V4LoadUnaligned(&constants[3]);
		Vector_4V v2 = V4LoadUnaligned(&constants[6]);
		Vector_4V v3 = V4LoadUnaligned(&constants[9]);

		Vector_4V* dest0 = reinterpret_cast<Vector_4V*>(output + offsets[channels[0]>>2]);
		Vector_4V* dest1 = reinterpret_cast<Vector_4V*>(output + offsets[channels[1]>>2]);
		Vector_4V* dest2 = reinterpret_cast<Vector_4V*>(output + offsets[channels[2]>>2]);
		Vector_4V* dest3 = reinterpret_cast<Vector_4V*>(output + offsets[channels[3]>>2]);

		*dest0 = v0;
		*dest1 = v1;
		*dest2 = v2;
		*dest3 = v3;

		constants += 12;
		channels += 4;
	}

	u32 count1 = count & 3;
	for(; count1; count1--)
	{
		Vector_4V v = V4LoadUnaligned(constants);
		Vector_4V* dest = reinterpret_cast<Vector_4V*>(output + offsets[channels[0]>>2]);
		*dest = v;
		constants += 3;
		channels += 1;
	}
}

////////////////////////////////////////////////////////////////////////////////

static void EvaluateConstant(u32 count, const f32* constants, const u16* channels, u8* output, const u16* offsets)
{
	count = RAGE_COUNT(count, 2);
	for(; count; count--)
	{
		// load floats
		f32 f0 = constants[0];
		f32 f1 = constants[1];
		f32 f2 = constants[2];
		f32 f3 = constants[3];

		// store floats
		u32 idx0 = channels[0];
		u32 idx1 = channels[1];
		u32 idx2 = channels[2];
		u32 idx3 = channels[3];

		f32* dest0 = reinterpret_cast<f32*>(output + offsets[idx0>>2] + ((idx0&3)<<2));
		f32* dest1 = reinterpret_cast<f32*>(output + offsets[idx1>>2] + ((idx1&3)<<2));
		f32* dest2 = reinterpret_cast<f32*>(output + offsets[idx2>>2] + ((idx2&3)<<2));
		f32* dest3 = reinterpret_cast<f32*>(output + offsets[idx3>>2] + ((idx3&3)<<2));

		*dest0 = f0;
		*dest1 = f1;
		*dest2 = f2;
		*dest3 = f3;

		constants += 4;
		channels += 4;
	}
}

////////////////////////////////////////////////////////////////////////////////

static void EvaluateRaw(u32 count, const u16* channels, f32 tr, u8* output, const u32* prevFrame, const u32* nextFrame, const u16* offsets)
{
	const f32* prevRaw = reinterpret_cast<const f32*>(prevFrame);
	const f32* nextRaw = reinterpret_cast<const f32*>(nextFrame);

	count = RAGE_COUNT(count, 2);
	for(; count; count--)
	{
		// load previous/next floats
		f32 prev0 = prevRaw[0];  f32 next0 = nextRaw[0];
		f32 prev1 = prevRaw[1];	 f32 next1 = nextRaw[1];
		f32 prev2 = prevRaw[2];	 f32 next2 = nextRaw[2];
		f32 prev3 = prevRaw[3];	 f32 next3 = nextRaw[3];

		// store interpolate floats
		u32 idx0 = channels[0];
		u32 idx1 = channels[1];
		u32 idx2 = channels[2];
		u32 idx3 = channels[3];

		f32* dest0 = reinterpret_cast<f32*>(output + offsets[idx0>>2] + ((idx0&3)<<2));
		f32* dest1 = reinterpret_cast<f32*>(output + offsets[idx1>>2] + ((idx1&3)<<2));
		f32* dest2 = reinterpret_cast<f32*>(output + offsets[idx2>>2] + ((idx2&3)<<2));
		f32* dest3 = reinterpret_cast<f32*>(output + offsets[idx3>>2] + ((idx3&3)<<2));

		*dest0 = FPLerp(tr, prev0, next0);
		*dest1 = FPLerp(tr, prev1, next1);
		*dest2 = FPLerp(tr, prev2, next2);
		*dest3 = FPLerp(tr, prev3, next3);

		prevRaw += 4;
		nextRaw += 4;
		channels += 4;
	}
}

////////////////////////////////////////////////////////////////////////////////

static void EvaluateQuantized(u32 count, const f32* constants, const u16* channels, u32 addr, f32 tr, u8* output, const u32* prevFrame, const u32* nextFrame, const u16* offsets)
{
	count = RAGE_COUNT(count, 2);
	for(; count; count--)
	{
		// compute quantized floats
		const u32* numBits = reinterpret_cast<const u32*>(constants);
		u32 numBit0 = numBits[0]&31; f32 scale0 = constants[1];   f32 offset0 = constants[2];
		u32 numBit1 = numBits[3]&31; f32 scale1 = constants[4];	  f32 offset1 = constants[5];
		u32 numBit2 = numBits[6]&31; f32 scale2 = constants[7];	  f32 offset2 = constants[8];
		u32 numBit3 = numBits[9]&31; f32 scale3 = constants[10];  f32 offset3 = constants[11];

		u32 prev0 = GetQuantizedValue(addr, prevFrame, numBit0);
		u32 next0 = GetQuantizedValue(addr, nextFrame, numBit0);
		addr += numBit0;

		u32 prev1 = GetQuantizedValue(addr, prevFrame, numBit1);
		u32 next1 = GetQuantizedValue(addr, nextFrame, numBit1);
		addr += numBit1;

		u32 prev2 = GetQuantizedValue(addr, prevFrame, numBit2);
		u32 next2 = GetQuantizedValue(addr, nextFrame, numBit2);
		addr += numBit2;

		u32 prev3 = GetQuantizedValue(addr, prevFrame, numBit3);
		u32 next3 = GetQuantizedValue(addr, nextFrame, numBit3);
		addr += numBit3;

		// store interpolated floats
		u32 idx0 = channels[0];
		u32 idx1 = channels[1];
		u32 idx2 = channels[2];
		u32 idx3 = channels[3];

		f32* dest0 = reinterpret_cast<f32*>(output + offsets[idx0>>2] + ((idx0&3)<<2));
		f32* dest1 = reinterpret_cast<f32*>(output + offsets[idx1>>2] + ((idx1&3)<<2));
		f32* dest2 = reinterpret_cast<f32*>(output + offsets[idx2>>2] + ((idx2&3)<<2));
		f32* dest3 = reinterpret_cast<f32*>(output + offsets[idx3>>2] + ((idx3&3)<<2));

		*dest0 = scale0 * FPLerp(tr, f32(prev0), f32(next0)) + offset0;
		*dest1 = scale1 * FPLerp(tr, f32(prev1), f32(next1)) + offset1;
		*dest2 = scale2 * FPLerp(tr, f32(prev2), f32(next2)) + offset2;
		*dest3 = scale3 * FPLerp(tr, f32(prev3), f32(next3)) + offset3;

		constants += 12;
		channels += 4;
	}
}

////////////////////////////////////////////////////////////////////////////////

static void EvaluateIndirect(u32 count, const f32* constants, const u16* channels, u32 addr, f32 tr, u8* output, const u32* prevFrame, const u32* nextFrame, const u16* offsets)
{
	for(; count; count--)
	{
		// compute indirect quantized float
		const u32* values = reinterpret_cast<const u32*>(constants);
		u32 numIndiceBits = values[0];
		u32 numValueBits = values[1];
		u32 numValues = values[2];
		f32 scale = constants[3];
		f32 offset = constants[4];
		u32 prevIdx = GetQuantizedValue(addr, prevFrame, numIndiceBits);
		u32 nextIdx = GetQuantizedValue(addr, nextFrame, numIndiceBits);
		u32 prev0 = GetQuantizedValue(prevIdx*numValueBits, &values[5], numValueBits);
		u32 next0 = GetQuantizedValue(nextIdx*numValueBits, &values[5], numValueBits);
		f32 sample = scale * FPLerp(tr, f32(prev0), f32(next0)) + offset;

		// store float
		u32 idx = *channels++;
		f32* dest = reinterpret_cast<f32*>(output + offsets[idx>>2] + ((idx&3)<<2));
		*dest = sample;

		constants += 5+numValues;
		addr += numIndiceBits;
	}
}

////////////////////////////////////////////////////////////////////////////////

static void EvaluateLinear(u32 count, const f32* constants, const u16* channels, u32 tq, f32 tr, u8* output, const u16* offsets, u32 numFrames, u32 N)
{
	const u32 segmentMask = N-1;
	const u32 numSegments = (numFrames+segmentMask)/N;
	const u32 valueIdx = tq % N;
	const u32 segmentIdx = tq / N;
	const u32 headerSize = 4*sizeof(u32)*8;
	const u32* sequence = reinterpret_cast<const u32*>(constants);

	u32 count2 = count >> 1;
	for(; count2; count2--)
	{
		const u32* RESTRICT sequence0 = sequence;
		const u32* RESTRICT sequence1 = sequence0 + sequence0[0];

		u32 packedBytes0 = sequence0[1];
		u32 packedBytes1 = sequence1[1];

		u32 numBitAddr0 = packedBytes0 & UCHAR_MAX;
		u32 numBitAddr1 = packedBytes1 & UCHAR_MAX;

		u32 numBitInitial0 = (packedBytes0 >> 8) & UCHAR_MAX;
		u32 numBitInitial1 = (packedBytes1 >> 8) & UCHAR_MAX;

		u32 divisor0 = (packedBytes0 >> 16) & UCHAR_MAX;
		u32 divisor1 = (packedBytes1 >> 16) & UCHAR_MAX;

		u32 address0 = headerSize + numSegments * numBitAddr0;
		u32 address1 = headerSize + numSegments * numBitAddr1;

		u32 addressOffset0 = GetQuantizedValue(headerSize + segmentIdx * numBitAddr0, sequence0, numBitAddr0);
		u32 addressOffset1 = GetQuantizedValue(headerSize + segmentIdx * numBitAddr1, sequence1, numBitAddr1);

		u32 addrSegment0 = address0 + segmentIdx * numBitInitial0;
		u32 addrSegment1 = address1 + segmentIdx * numBitInitial1;

		s32 prevSegment0 = GetQuantizedValue(addrSegment0, sequence0, numBitInitial0);
		s32 prevSegment1 = GetQuantizedValue(addrSegment1, sequence1, numBitInitial1);

		s32 nextSegment0 = GetQuantizedValue(addrSegment0 + numBitInitial0, sequence0, numBitInitial0);
		s32 nextSegment1 = GetQuantizedValue(addrSegment1 + numBitInitial1, sequence1, numBitInitial1);

		address0 += numSegments * numBitInitial0 + addressOffset0;
		address1 += numSegments * numBitInitial1 + addressOffset1;

		s32 vel0 = 0, vel1 = 0;
		s32 prev0 = 0, prev1 = 0;
		s32 next0 = prevSegment0, next1 = prevSegment1;

		for(u32 j=0; j<=valueIdx; ++j)
		{
			vel0 += GetSequenceValue(divisor0, address0, sequence0);
			vel1 += GetSequenceValue(divisor1, address1, sequence1);

			prev0 = next0;
			prev1 = next1;
			
			next0 += vel0;
			next1 += vel1;
		}

		u32 idx0 = *channels++;
		u32 idx1 = *channels++;

		f32* dest0 = reinterpret_cast<f32*>(output + offsets[idx0>>2] + ((idx0&3)<<2));
		f32* dest1 = reinterpret_cast<f32*>(output + offsets[idx1>>2] + ((idx1&3)<<2));

		f32 scale0 = *reinterpret_cast<const f32*>(&sequence0[2]);
		f32 scale1 = *reinterpret_cast<const f32*>(&sequence1[2]);

		f32 offset0 = *reinterpret_cast<const f32*>(&sequence0[3]);
		f32 offset1 = *reinterpret_cast<const f32*>(&sequence1[3]);

		next0 = valueIdx == segmentMask ? nextSegment0 : next0;
		next1 = valueIdx == segmentMask ? nextSegment1 : next1;

		*dest0 = scale0 * FPLerp(tr, f32(prev0), f32(next0)) + offset0;
		*dest1 = scale1 * FPLerp(tr, f32(prev1), f32(next1)) + offset1;

		sequence = sequence1 + sequence1[0];
	}

	if(count & 1)
	{
		// get address inside packed sequence
		u32 packedBytes = sequence[1];
		u32 numBitAddr = packedBytes & UCHAR_MAX;
		u32 addressOffset = GetQuantizedValue(headerSize + segmentIdx * numBitAddr, sequence, numBitAddr);
		u32 address = headerSize + numSegments * numBitAddr;

		// get initial value
		u32 numBitInitial = (packedBytes >> 8) & UCHAR_MAX;
		u32 addrSegment = address + segmentIdx * numBitInitial;
		s32 prevSegment = GetQuantizedValue(addrSegment, sequence, numBitInitial);
		s32 nextSegment = GetQuantizedValue(addrSegment + numBitInitial, sequence, numBitInitial);
		address += numSegments * numBitInitial + addressOffset;

		// integrate the linear prediction
		u32 divisor = (packedBytes >> 16) & UCHAR_MAX;
		s32 vel = 0;
		s32 prev = 0, next = prevSegment;
		for(u32 j=0; j<=valueIdx; ++j)
		{
			vel += GetSequenceValue(divisor, address, sequence);
			prev = next;
			next += vel;
		}
		next = valueIdx == segmentMask ? nextSegment : next;

		// store float
		u32 idx = *channels++;
		f32* dest = reinterpret_cast<f32*>(output + offsets[idx>>2] + ((idx&3)<<2));
		f32 scale = *reinterpret_cast<const f32*>(&sequence[2]);
		f32 offset = *reinterpret_cast<const f32*>(&sequence[3]);
		*dest = scale * FPLerp(tr, f32(prev), f32(next)) + offset;
	}
}

////////////////////////////////////////////////////////////////////////////////

static const uaVector_4 _permutations[] = { {X2,X1,Y1,Z1}, {X1,X2,Y1,Z1}, {X1,Y1,X2,Z1}, {X1,Y1,Z1,X2} };

static void Reconstruct(u32 count, const u16* channels, u8* output, const u16* offsets)
{
	const Vector_4V* permutations = reinterpret_cast<const Vector_4V*>(_permutations);
	const Vector_4V _one = V4VConstant(V_ONE);
	const Vector_4V _min = V4VConstant(V_FLT_MIN);

	count = RAGE_COUNT(count, 2);
	for(; count; count--)
	{
		u32 idx0 = channels[0];
		u32 idx1 = channels[1];
		u32 idx2 = channels[2];
		u32 idx3 = channels[3];

		// load components
		Vector_4V* dest0 = reinterpret_cast<Vector_4V*>(output + offsets[idx0>>2]);
		Vector_4V* dest1 = reinterpret_cast<Vector_4V*>(output + offsets[idx1>>2]);
		Vector_4V* dest2 = reinterpret_cast<Vector_4V*>(output + offsets[idx2>>2]);
		Vector_4V* dest3 = reinterpret_cast<Vector_4V*>(output + offsets[idx3>>2]);

		Vector_4V q0 = *dest0;
		Vector_4V q1 = *dest1;
		Vector_4V q2 = *dest2;
		Vector_4V q3 = *dest3;

		// compute remaining component
		Vector_4V sq0 = V4Subtract(_one, V3DotV(q0, q0));
		Vector_4V sq1 = V4Subtract(_one, V3DotV(q1, q1));
		Vector_4V sq2 = V4Subtract(_one, V3DotV(q2, q2));
		Vector_4V sq3 = V4Subtract(_one, V3DotV(q3, q3));

		Vector_4V positive0 = V4IsGreaterThanV(sq0, _min);
		Vector_4V positive1 = V4IsGreaterThanV(sq1, _min);
		Vector_4V positive2 = V4IsGreaterThanV(sq2, _min);
		Vector_4V positive3 = V4IsGreaterThanV(sq3, _min);

		Vector_4V comp0 = V4And(positive0, V4Scale(sq0, V4InvSqrt(sq0)));
		Vector_4V comp1 = V4And(positive1, V4Scale(sq1, V4InvSqrt(sq1)));
		Vector_4V comp2 = V4And(positive2, V4Scale(sq2, V4InvSqrt(sq2)));
		Vector_4V comp3 = V4And(positive3, V4Scale(sq3, V4InvSqrt(sq3)));

		// store quaternions
		*dest0 = V4PermuteTwo(q0, comp0, permutations[idx0&3]);
		*dest1 = V4PermuteTwo(q1, comp1, permutations[idx1&3]);
		*dest2 = V4PermuteTwo(q2, comp2, permutations[idx2&3]);
		*dest3 = V4PermuteTwo(q3, comp3, permutations[idx3&3]);

		channels += 4;
	}
}

////////////////////////////////////////////////////////////////////////////////

static void Normalize(u32 count, const u16* channels, u8* output, const u16* offsets)
{
	count = RAGE_COUNT(count, 2);
	for(; count; count--)
	{
		// load quaternions		
		u32 idx0 = channels[0];
		u32 idx1 = channels[1];
		u32 idx2 = channels[2];
		u32 idx3 = channels[3];

		Vector_4V* dest0 = reinterpret_cast<Vector_4V*>(output + offsets[idx0>>2]);
		Vector_4V* dest1 = reinterpret_cast<Vector_4V*>(output + offsets[idx1>>2]);
		Vector_4V* dest2 = reinterpret_cast<Vector_4V*>(output + offsets[idx2>>2]);
		Vector_4V* dest3 = reinterpret_cast<Vector_4V*>(output + offsets[idx3>>2]);

		Vector_4V q0 = *dest0;
		Vector_4V q1 = *dest1;
		Vector_4V q2 = *dest2;
		Vector_4V q3 = *dest3;

		// store normalized quaternions
		*dest0 = V4Normalize(q0);
		*dest1 = V4Normalize(q1);
		*dest2 = V4Normalize(q2);
		*dest3 = V4Normalize(q3);

		channels += 4;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crBlockStream::EvaluateFrame(u8* output, u32 tq, f32 tr, const u16* offsets) const
{
	const u8* constant = m_Data;
	const u8* variable = constant + m_ConstantSize;
	const u32* prevFrame = reinterpret_cast<const u32*>(variable + tq*m_FrameSize);
	const u32* nextFrame = reinterpret_cast<const u32*>(variable + tq*m_FrameSize + m_FrameSize);
	const u16* indices = reinterpret_cast<const u16*>(variable + m_FrameSize*m_NumFrames);

#if __WIN32
	const u32 cacheLine = 128;
	for(u32 i=0; i < m_ConstantSize; i+=cacheLine)
	{
		PrefetchDC2(constant, i);
	}
	for(u32 i=0; i < u32(m_FrameSize<<1); i+=cacheLine)
	{
		PrefetchDC2(prevFrame, i);
	}
#endif

	// compute addresses and offsets
	const u16* counts = indices;
	const u16* channels[kNumType];
	const f32* constants[kNumType];
	u32 variables[kNumType];
	indices += kNumType;
	for(u32 i=0; i < kNumType; i++)
	{
		channels[i] = indices;
		indices += (counts[i]+3)&~3;
	}
	constants[kConstantQuat] = reinterpret_cast<const f32*>(constant);
	constants[kConstantVec] = constants[kConstantQuat] + counts[kConstantQuat]*3;
	constants[kConstant] = constants[kConstantVec] + counts[kConstantVec]*3;
	constants[kQuantize] = constants[kConstant] + counts[kConstant];
	constants[kIndirect] = constants[kQuantize] + counts[kQuantize]*3;
	constants[kLinear] = constants[kIndirect] + m_IndirectSize;
	variables[kQuantize] = counts[kRaw] << 5;
	variables[kIndirect] = variables[kQuantize] + m_QuantizeSize;

	// process dofs
	EvaluateConstantQuaternion(counts[kConstantQuat], constants[kConstantQuat], channels[kConstantQuat], output, offsets);
	EvaluateConstantVector(counts[kConstantVec], constants[kConstantVec], channels[kConstantVec], output, offsets);

	// process channels
	EvaluateConstant(counts[kConstant], constants[kConstant], channels[kConstant], output, offsets);
	EvaluateRaw(counts[kRaw], channels[kRaw], tr, output, prevFrame, nextFrame, offsets);
	EvaluateQuantized(counts[kQuantize], constants[kQuantize], channels[kQuantize], variables[kQuantize], tr, output, prevFrame, nextFrame, offsets);
	EvaluateIndirect(counts[kIndirect], constants[kIndirect], channels[kIndirect], variables[kIndirect], tr, output, prevFrame, nextFrame, offsets);
	EvaluateLinear(counts[kLinear], constants[kLinear], channels[kLinear], tq, tr, output, offsets, m_NumFrames, m_SegmentSize);

	// reprocess channels
	Reconstruct(counts[kReconstruct], channels[kReconstruct], output, offsets);
	Normalize(counts[kNormalize], channels[kNormalize], output, offsets);
}

////////////////////////////////////////////////////////////////////////////////

void crBlockStream::FindChannels(u32 dofIdx, u32& outCount, Channel* outChannels) const
{
	// get dof cursor
	const u8* constants = m_Data;
	const u16* channels = reinterpret_cast<const u16*>(constants + m_ConstantSize + m_NumFrames*m_FrameSize);
	const u16* indices = channels + kNumType;
	const u32* values = reinterpret_cast<const u32*>(constants);

	u32 count = 0, offset = 0, address = 0;
	for(u32 i=0; i < kNumType; ++i)
	{
		u32 numChannels = channels[i];

		for(u32 j=0; j < numChannels; ++j)
		{
			u32 idx = indices[j];
			if((idx>>2) == dofIdx)
			{
				Channel& ch = outChannels[count];
				ch.m_Type = u8(i);
				ch.m_Component = u8(idx&3);
				ch.m_Offset = u16(offset);
				ch.m_Address = u16(address);
				count++;
			}

			switch(i)
			{
			case kConstantQuat:
			case kConstantVec:	offset += 3; break;
			case kConstant:		offset += 1; break;
			case kRaw:			address += 32; break;
			case kQuantize:		address += values[offset]; offset += 3; break;
			case kIndirect:		address += values[offset]; offset += 5 + values[offset+2]; break;
			case kLinear:		offset += values[offset]; break;
			case kReconstruct:	
			case kNormalize:	break;
			default:			Assert(false);
			}
		}

		indices += (numChannels+3)&~3;
	}

	outCount = count;
}

////////////////////////////////////////////////////////////////////////////////

void crBlockStream::EvaluateChannels(u8* output, u32 tq, f32 tr, u32 count, const Channel* channels) const
{
	const u8* constants = m_Data;
	const u8* variables = constants + m_ConstantSize;
	const u32* prevFrame = reinterpret_cast<const u32*>(variables + tq*m_FrameSize);
	const u32* nextFrame = reinterpret_cast<const u32*>(variables + tq*m_FrameSize + m_FrameSize);
	u32 numSegments = (m_NumFrames+m_SegmentSize-1)/m_SegmentSize;

	for(u32 i=0; i < count; ++i)
	{
		const Channel& ch = channels[i];
		const f32* constant = reinterpret_cast<const f32*>(constants) + ch.m_Offset;
		f32* dest = reinterpret_cast<f32*>(output) + ch.m_Component;

		switch(ch.m_Type)
		{
		case kConstantQuat:
			{
				Vector_4V q = V4LoadUnaligned(constant);
				Vector_4V sq = V4Subtract(V4VConstant(V_ONE), V3DotV(q, q));
				Vector_4V positive = V4IsGreaterThanV(sq, V4VConstant(V_FLT_MIN));
				Vector_4V comp = V4And(positive, V4Scale(sq, V4InvSqrt(sq)));
				*reinterpret_cast<Vector_4V*>(dest) = V4PermuteTwo<X1,Y1,Z1,X2>(q, comp);
				break;
			}

		case kConstantVec:
			{
				*reinterpret_cast<Vector_4V*>(dest) = V4LoadUnaligned(constant);
				break;
			}

		case kConstant:	
			{
				*dest = *constant;
				break;
			}

		case kRaw:
			{
				u32 offset = ch.m_Address>>5;
				f32 prevRaw = *reinterpret_cast<const f32*>(prevFrame+offset);
				f32 nextRaw = *reinterpret_cast<const f32*>(nextFrame+offset);
				*dest = FPLerp(tr, prevRaw, nextRaw);
				break;
			}

		case kQuantize:
			{
				const u32* values = reinterpret_cast<const u32*>(constant);
				u32 addr = ch.m_Address;
				u32 numBit = values[0];
				FastAssert(numBit > 0 && numBit <= 32);
				u32 prev0 = GetQuantizedValue(addr, prevFrame, numBit);
				u32 next0 = GetQuantizedValue(addr, nextFrame, numBit);
				*dest = constant[1] * FPLerp(tr, f32(prev0), f32(next0)) + constant[2];
				break;
			}

		case kIndirect:
			{
				const u32* values = reinterpret_cast<const u32*>(constant);
				u32 addr = ch.m_Address;
				u32 numIndiceBits = values[0], numValueBits = values[1];
				FastAssert(numIndiceBits > 0 && numIndiceBits <= 32);
				FastAssert(numValueBits > 0 && numValueBits <= 32);
				u32 prevIdx = GetQuantizedValue(addr, prevFrame, numIndiceBits);
				u32 nextIdx = GetQuantizedValue(addr, nextFrame, numIndiceBits);
				u32 prev0 = GetQuantizedValue(prevIdx*numValueBits, &values[5], numValueBits);
				u32 next0 = GetQuantizedValue(nextIdx*numValueBits, &values[5], numValueBits);
				*dest = constant[3] * FPLerp(tr, f32(prev0), f32(next0)) + constant[4];
				break;
			}

		case kLinear:
			{
				const u32* sequence = reinterpret_cast<const u32*>(constant);
				u32 N = m_SegmentSize;
				u32 segmentMask = N-1;
				u32 valueIdx = tq % N;
				u32 segmentIdx = tq  / N;

				u32 packedBytes = sequence[1];
				f32 scale = *reinterpret_cast<const f32*>(&sequence[2]);
				f32 offset = *reinterpret_cast<const f32*>(&sequence[3]);
				u32 address = 4*sizeof(u32)*8;

				u32 numBitAddr = packedBytes & UCHAR_MAX;
				u32 addressOffset = GetQuantizedValue(address + segmentIdx * numBitAddr, sequence, numBitAddr);
				address += numSegments * numBitAddr;

				u32 numBitInitial = (packedBytes >> 8) & UCHAR_MAX;
				u32 addrSegment = address + segmentIdx * numBitInitial;
				s32 prevSegment = GetQuantizedValue(addrSegment, sequence, numBitInitial);
				s32 nextSegment = GetQuantizedValue(addrSegment + numBitInitial, sequence, numBitInitial);
				address += numSegments * numBitInitial + addressOffset;

				u32 divisor = (packedBytes >> 16) & UCHAR_MAX;
				s32 vel = 0;
				s32 prev = 0, next = prevSegment;
				for(u32 j=0; j<=valueIdx; ++j)
				{
					vel += GetSequenceValue(divisor, address, sequence);
					prev = next;
					next += vel;
				}
				next = valueIdx == segmentMask ? nextSegment : next;
				*dest = scale * FPLerp(tr, f32(prev), f32(next)) + offset;
				break;
			}

		case kReconstruct:
			{
				const Vector_4V* permutations = reinterpret_cast<const Vector_4V*>(_permutations);
				Vector_4V* qDest = reinterpret_cast<Vector_4V*>(output);
				Vector_4V q = *qDest;
				Vector_4V sq = V4Subtract(V4VConstant(V_ONE), V3DotV(q, q));
				Vector_4V positive = V4IsGreaterThanV(sq, V4VConstant(V_FLT_MIN));
				Vector_4V comp = V4And(positive, V4Scale(sq, V4InvSqrt(sq)));
				*qDest = V4PermuteTwo(q, comp, permutations[ch.m_Component]);
				break;
			}

		case kNormalize:
			{
				Vector_4V* qDest = reinterpret_cast<Vector_4V*>(output);
				*qDest = V4Normalize(*qDest);
				break;
			}

		default: Assert(false);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage
