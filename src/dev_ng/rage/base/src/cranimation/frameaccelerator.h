//
// cranimation/frameaccelerator.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_FRAMEACCELERATOR_H
#define CRANIMATION_FRAMEACCELERATOR_H

#include "animation_config.h"

#include "atl/array.h"
#include "system/criticalsection.h"
#include "system/tinyheap.h"


namespace rage
{

class crAnimation;
class crFrameData;
class crFrameFilter;
class crSkeletonData;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: An accelerator is a recently used cache of signature uniquely identified data
// Typically used as a special container within another wrapping class (like crFrameAccelerator)
class crAccelerator
{
private:

	struct Entry;

public:

	// PURPOSE: Constructor
	crAccelerator();

	// PURPOSE: Destructor
	~crAccelerator();

	// PURPOSE: Initialize
	// PARAMS:
	// numSlots - number of slots for the hash map
	// entrySize - size in bytes of each entry
	void Init(u32 numSlots, u32 heapSize);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Accelerator lock.
	// Locks accelerator buffer so cached results can be safely accessed in multi threaded environment.
	// Other threads can not recycle accelerator buffer, while it is locked.
	// Lock automatically released when lock object is destroyed.
	struct Lock
	{
		friend class crAccelerator;

		// PURPOSE: Default constructor
		Lock();

		// PURPOSE: Initializing constructor
		Lock(Entry* entry);

		// PURPOSE: Copy constructor
		Lock(const Lock& lock);

		// PURPOSE: Assignment operator
		void operator=(const Lock& lock);

		// PURPOSE: Destructor.  Will unlock if still locked.
		~Lock();

		// PURPOSE: Set accelerator entry, can be NULL
		void Set(Entry* entry);

		// PURPOSE: Get the accelerator
		// RETURNS: pointer to accelerator (can be NULL)
		u8* Get() const;

		// PURPOSE: Get size of accelerator
		u32 Size() const;

		// PURPOSE: Unlock accelerator entry (called automatically on lock destruction)
		void Unlock();

	private:

		Entry* m_Entry;
		u32 m_Size;
	};

	typedef u32 CalcFn(void* calcData, u8* buffer);

	// PURPOSE: Get entry from signature, if not already present insert it
	void GetEntry(u64 signature, u32 size, CalcFn calcFn, void* calcData, Lock& outLock);

#if CR_STATS
	// PURPOSE: Get total heap size
	size_t GetTotalHeapSize() const;

	// PURPOSE: Calculate locked heap size
	u32 CalcLockedHeapSize() const;
#endif // CR_STATS
	
private:

	// PURPOSE: Internal call, implements find accelerator, given signature
	bool FindInternal(u64 signature, Lock& outLock) const;

	// PURPOSE: Internal call, implements insert accelerator
	void InsertInternal(u64 signature, u32 size, CalcFn calcFn, void* calcData, Lock& outLock);

	// PURPOSE: Free one accelerator entry
	void FreeEntry(Entry* entry);

	// PURPOSE: Remove one entry of greater or equal size
	void RemoveEntry(u32 size);

	// PURPOSE: Remove all unused entries
	void Flush();

	// PURPOSE: Get slot index from signature
	u32 GetSlotIdx(u64 signature) const;

	// PURPOSE: Internal structure, represents accelerator entry with signature and stamp
	struct ALIGNAS(16) Entry
	{
		u64 m_Signature;
		Entry* m_Next;
		mutable u32 m_Stamp;
		u32 m_Lock;
		u32 m_Size;
		u32 m_Padding[2];
	} ;

	sysTinyHeap m_Heap;
	Entry** m_Slots;
	u32 m_NumSlots;
	mutable u32 m_Stamp;
	u8* m_Buffer;
};

// PURPOSE: Less verbose type for accelerator lock
typedef crAccelerator::Lock crLock;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Thread safe version of the accelerator
class crThreadSafeAccelerator : public crAccelerator
{
public:

	// PURPOSE: Get entry from signature in a thread-safe way
	void GetEntry(u64 signature, u32 size, CalcFn calcFn, void* calcData, Lock& outLock);

private:

	sysCriticalSectionToken m_CsToken;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Frame accelerator, used to speed up frame operations
// Accelerators generate on demand and cache tables to speed up operations on
// frames and related classes.
class crFrameAccelerator
{
	friend class crExpressions;

public:

	// PURPOSE: Constructor
	crFrameAccelerator();

	// PURPOSE: Destructor
	~crFrameAccelerator();

	// PURPOSE: Shutdown, release all dynamic allocations
	void Shutdown();

	// PURPOSE: Initialize
	// PARAMS:
	// numSlots - number of slots used by the accelerator hash maps
	// heapSizes - number of bytes used by the accelerator heaps 
	void Init(const u32* numSlots, const u32* heapSizes);

	// PURPOSE: Get size of the frame indices
	static u32 GetSizeFrameIndices(u32 numDofs);

	// PURPOSE: Find frame to frame indices, based on two frame signatures
	// PARAMS:
	// outLock - lock wrapped pointer to indices (NULL if indices not found, and not, or can't be generated)
	// frameData - frame structure
	// srcFrame - second frame structure
	// NOTES: Pointer is wrapped in lock structure to protect it from being recycled by other threads.
	void FindFrameIndices(crLock& outLock, const crFrameData& frameData, const crFrameData& srcFrameData);

	// PURPOSE: Calculate indices, based on two frames
	// NOTES: Forces calculation of indices every time.
	// Used internally by FindFrameIndices, to generate indices if not already cached.
	static u32 CalcFrameIndices(const crFrameData& frameData, const crFrameData& srcFrameData, u16* indices);

	// PURPOSE: Get size of frame animation indices
	static u32 GetSizeFrameAnimIndices(u32 numDofs);

	// PURPOSE: Find frame to animation indices, based on frame and animation signatures
	// PARAMS:
	// outLock - lock wrapped pointer to indices (NULL if indices not found, and not, or can't be generated)
	// frameData - frame structure
	// anim - animation
	// NOTES: Pointer is wrapped in lock structure to protect it from being recycled by other threads.
	void FindFrameAnimIndices(crLock& outLock, const crFrameData& frameData, u32 numAnims, const crAnimation** anims);

	// PURPOSE: Calculate indices, based on frame and animation
	// NOTES: Forces calculation of indices every time.
	// Used internally by FindFrameAnimIndices, to generate indices if not already cached.
	static u32 CalcFrameAnimIndices(const crFrameData& frameData, const crAnimation& anim, u16* indices);

	// PURPOSE: Get size of the frame skeleton indices
	static u32 GetSizeFrameSkelIndices(u32 numDofs);

	// PURPOSE: Find frame to skeleton indices, based on frame and skeleton signatures
	// PARAMS:
	// outLock - lock wrapped pointer to indices (NULL if indices not found, and not, or can't be generated)
	// frameData - frame structure
	// skelData - skeleton data
	// NOTES: Pointer is wrapped in lock structure to protect if from being recycled by other threads.
	void FindFrameSkelIndices(crLock& outLock, const crFrameData& frameData, const crSkeletonData& skelData, crFrameFilter* filter=NULL);

	// PURPOSE: Calculate indices, based on frame and skeleton
	// NOTES: Forces calculation of indices every time.
	// Used internally by FindFrameSkelIndices, to generate indices if not already cached.
	static u32 CalcFrameSkelIndices(const crFrameData& frameData, const crSkeletonData& skelData, crFrameFilter* filter, u16* indices);

	// PURPOSE: Get size of the weights
	static u32 GetSizeFilterWeights(u32 numDofs);

	// PURPOSE: Find frame filter weights, based on a frame and filter signature
	// PARAMS:
	// outLock - lock wrapped pointer to frame filter weights (NULL if weights not found, and not, or can't be generated)
	// frameData - frame structure
	// filter - animation filter
	// NOTES: Pointer is wrapped in a lock structure to protect it from being recycled by other threads.
	void FindFilterWeights(crLock& outLock, const crFrameData& frameData, crFrameFilter& filter);

	// PURPOSE: Calculate filter weights, based on frame and filter
	// NOTES: Forces calculation of weights every time.
	// Used internally my FindFilterWeights, to generate weights if not already cached.
	static u32 CalcFilterWeights(const crFrameData& frameData, crFrameFilter& filter, f32* weights);

private:

	// PURPOSE: Private copy constructor to prevent copying
	crFrameAccelerator(const crFrameAccelerator&);
	crFrameAccelerator& operator=(const crFrameAccelerator&);

	enum { kFilterWeights, kFrameIndices, kFrameAnimIndices, kFrameSkelIndices, kExpressionFrameIndices, kExpressionSkelIndices, kNumAccelerators};
	crThreadSafeAccelerator m_Accelerators[kNumAccelerators];
};

////////////////////////////////////////////////////////////////////////////////

inline u8* crAccelerator::Lock::Get() const
{
	return m_Entry ? reinterpret_cast<u8*>(m_Entry + 1) : NULL; 
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAccelerator::Lock::Size() const
{
	return m_Size;
}

////////////////////////////////////////////////////////////////////////////////

inline void crThreadSafeAccelerator::GetEntry(u64 signature, u32 size, CalcFn calcFn, void* calcData, Lock& outLock)
{
	sysCriticalSection cs(m_CsToken);
	crAccelerator::GetEntry(signature, size, calcFn, calcData, outLock);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_FRAMEACCELERATOR_H