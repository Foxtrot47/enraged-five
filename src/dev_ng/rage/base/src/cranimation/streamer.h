//
// cranimation/streamer.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_STREAMER_H
#define CRANIMATION_STREAMER_H

#include "data/struct.h"
#include "math/amath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

class BaseStreamer
{
public:
	// PURPOSE: Default constructor/destructor
	BaseStreamer();
	~BaseStreamer();

	// PURPOSE: Destroy the content
	void Clear();

	// PURPOSE: Get the number of bytes used
	u32 GetSize() const;

	// PURPOSE: Align to a number of bytes
	void Align(u32 size);

	// PURPOSE: Grow the stream from another buffer
	void Grow(u32 size, const void* ptr=NULL);

	// PURPOSE: Grow the stream from another stream
	void Grow(const BaseStreamer& that);

	// PURPOSE: Grow the stream with bits
	void GrowBits(u32 numBits, u32 bits);

	// PURPOSE: Copy operator to an existing streamer
	BaseStreamer& operator=(const BaseStreamer& that);

	// PURPOSE: Get data
	u8* GetData();

protected:
	// PURPOSE: Internal method to reserve more memory
	void Reserve(u32 size);

	u32 m_Reserve;
	u32 m_NumBits;
	u8* m_Data;
};

////////////////////////////////////////////////////////////////////////////////

inline BaseStreamer::BaseStreamer()
: m_NumBits(0)
, m_Reserve(0)
, m_Data(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

inline BaseStreamer::~BaseStreamer()
{
	Clear();
}

////////////////////////////////////////////////////////////////////////////////

inline void BaseStreamer::Clear()
{	
	delete [] m_Data;
	m_Data = NULL;
	m_NumBits = m_Reserve = 0;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 BaseStreamer::GetSize() const
{
	return (m_NumBits+7) >> 3;
}

////////////////////////////////////////////////////////////////////////////////

inline void BaseStreamer::Align(u32 size)
{
	u32 maxSize = size-1;
	u32 offset = GetSize();
	u32 alignSize = (offset+maxSize)&~maxSize;
	Reserve(alignSize);
	m_NumBits = alignSize << 3;
}

////////////////////////////////////////////////////////////////////////////////

inline void BaseStreamer::Reserve(u32 size)
{
	if(size <= m_Reserve)
		return;

	u32 offset = GetSize();
	u32 reserve = Max(m_Reserve << 1, size); // try doubling the buffer to avoid future reallocations
	u8* data = rage_new u8[reserve];
	memset(data, 0, reserve);
	memcpy(data, m_Data, offset);
	delete [] m_Data;
	m_Data = data;
	m_Reserve = reserve;
}

////////////////////////////////////////////////////////////////////////////////

inline void BaseStreamer::Grow(u32 size, const void* ptr)
{
	u32 offset = GetSize();
	Reserve(offset + size);
	if(ptr)
	{
		memcpy(m_Data + offset, ptr, size);
	}
	m_NumBits += size << 3;
}

////////////////////////////////////////////////////////////////////////////////

inline void BaseStreamer::Grow(const BaseStreamer& that)
{
	Grow(that.GetSize(), that.m_Data);
}

////////////////////////////////////////////////////////////////////////////////

inline void BaseStreamer::GrowBits(u32 numBits, u32 val)
{
	u32 addr = m_NumBits;
	u32 totalBits = (addr+numBits+31)&~31;
	u32 requiredSize = (totalBits >> 3) + 1;
	Reserve(requiredSize);

	u32 word = addr >> 5;
	u32 bit = addr & 31;

	u32* words = reinterpret_cast<u32*>(m_Data);
	words[word] |= val << bit;
	words[word+1] |= u64(val) >> (32-bit);

	m_NumBits += numBits;
}

////////////////////////////////////////////////////////////////////////////////

inline BaseStreamer& BaseStreamer::operator=(const BaseStreamer& that)
{
	Clear();
	Grow(that);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline u8* BaseStreamer::GetData()
{
	return m_Data;
}

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Utility class for the generation of a stream
template<typename _Type>
class Streamer : public BaseStreamer
{
public:
	// PURPOSE: Default constructor/destructor
	Streamer();

	// PURPOSE: Get the beginning pointer
	const _Type* GetPtr() const;
	_Type* GetPtr();

	// PURPOSE: Operator for a pointer to the underlying object
	_Type* operator->();

	// PURPOSE: Operator for a reference to the underlying object
	_Type& operator*() const;
};

////////////////////////////////////////////////////////////////////////////////

template<typename _Type>
inline Streamer<_Type>::Streamer()
{
	Grow(sizeof(_Type));
}

////////////////////////////////////////////////////////////////////////////////

template<typename _Type>
inline const _Type* Streamer<_Type>::GetPtr() const
{
	return reinterpret_cast<const _Type*>(m_Data);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _Type>
inline _Type* Streamer<_Type>::GetPtr()
{
	return reinterpret_cast<_Type*>(m_Data);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _Type>
inline  _Type* Streamer<_Type>::operator->()
{
	return reinterpret_cast<_Type*>(m_Data);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _Type>
inline  _Type& Streamer<_Type>::operator*() const
{
	return *reinterpret_cast<_Type*>(m_Data);
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
#define BYTESWAP(x) datSwapper(x)
#define BYTESWAP_ARRAY(x,y) datSwapperArray(x,y)

inline void datSwapperArray(void* array, u32 size)
{
	u32 count = size  / sizeof(u32);
	u32* ptr = reinterpret_cast<u32*>(array);
	for(; count; count--)
	{
		datSwapper(*ptr++);
	}
}

#else // __DECLARESTRUCT
#define BYTESWAP(x) (void)x
#define BYTESWAP_ARRAY(x,y) (void)x
#endif

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_STREAMER_H
