//
// cranimation/framedatafactory.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_FRAMEDATAFACTORY_H
#define CRANIMATION_FRAMEDATAFACTORY_H

#include "framedata.h"

#include "atl/map.h"
#include "system/criticalsection.h"

// Make frame data factory thread safe (not normally required)
// Does not remove requirement that GarbageCollect is called from a safe DMZ
#define FRAME_DATA_FACTORY_THREAD_SAFE (1)

namespace rage
{

class crCommonPool;
class crCreature;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Frame data factory
// Creates frame data from creature templates
// Uses pool and allocator (with defragmentation) to create frame data headers and buffers
// Shares underlying frame data across duplicate requests
// Supports lazy garbage collection of frame data (so references else where have time to clean up)
class crFrameDataFactory
{
public:

	// PURPOSE: Default constructor
	crFrameDataFactory();

	// PURPOSE: Destructor
	~crFrameDataFactory();

	// PURPOSE: Initialize
	// PARAMS: pool - common pool for allocation, shared by all frame data frame buffers
	void Init(crCommonPool& pool);

	// PURPOSE: Shutdown, frees up all internal resources
	void Shutdown();

	// PURPOSE: Allocate a new frame data
	// PARAMS: creature - creature to use as template for new frame data
	// filter - optional frame filter
	// RETURNS: newly allocated (or shared) frame data
	// NOTES: May not allocate a new frame data, may return new reference to existing match instead
	// Important: *MUST* make a matching ReleaseFrameData call, with the returned frame data,
	// when finished with it - though may make this call prior to all threads using it
	// having cleaned up (i.e. motion tree worker threads etc)
	crFrameData* AllocateFrameData(const crCreature& creature, crFrameFilter* filter=NULL);

	// PURPOSE: Duplicate an existing frame data
	// RETURNS: newly allocated (or shared) frame data
	crFrameData* DuplicateFrameData(const crFrameData& src);

	// PURPOSE: Release a frame data (when no longer used)
	// PARAMS: frameData - frame data to release
	// NOTES: May not immediately destroy the frame data,
	// if there are still other outstanding references to it
	// In this case, frame data will be automatically destroyed in GarbageCollect call
	// once all other references have finally cleaned up
	void ReleaseFrameData(crFrameData& frameData);


	// PURPOSE: Garbage collect
	// *** CALL ONCE PER TIMESTEP, FROM SAFE DMZ ONLY ***
	// NOTES: Checks all previously allocated frame data
	// If a frame data has been released (via ReleaseFrameData), from all matching
	// allocations (via AllocateFrameData) AND all other uses of the frame data have
	// released too (i.e. from crFrame's etc) then the frame data will be garbage collected
	void GarbageCollect();

protected:

	// PURPOSE: Private copy constructor to prevent copying
	crFrameDataFactory(const crFrameDataFactory&);
	crFrameDataFactory& operator=(const crFrameDataFactory&);

	// PURPOSE: Internal function, finds existing frame data based on creature signature
	crFrameData* FindFrameData(u32 signature) const;

	// PURPOSE: Internal function, insert new frame data inside the factory
	crFrameData* InsertFrameData(u32 signature, u32 numDofs);

	// PURPOSE: Internal function, deletes frame data (see ReleaseFrameData for end user version)
	void DeleteFrameData(crFrameData& frameData);

private:

	typedef atMapMemoryPool<u32, crFrameData*, 4> MapPool;
	typedef atMap<u32, crFrameData*, atMapHashFn<u32>, atMapEquals<u32>, MapPool::MapFunctor > Map;

#if FRAME_DATA_FACTORY_THREAD_SAFE
	sysCriticalSectionToken m_CsToken;
#endif // FRAME_DATA_FACTORY_THREAD_SAFE

	crCommonPool* m_CommonPool;
	MapPool m_MapPool;
	Map m_Map;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_FRAMEDATAFACTORY_H
