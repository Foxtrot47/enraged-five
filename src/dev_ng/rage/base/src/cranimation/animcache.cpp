//
// cranimation/animcache.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "animcache.h"

#include "animation.h"
#include "animstream.h"

#include "atl/map.h"
#include "data/compress.h"
#include "profile/profiler.h"

namespace rage
{


////////////////////////////////////////////////////////////////////////////////

#if CR_STATS
PF_PAGE(crAnimCachePage, "cr AnimCache");
PF_GROUP(crAnimCacheStats);
PF_COUNTER(crAnimCache_FindAnimBlock, crAnimCacheStats);
PF_COUNTER(crAnimCache_DecompressAnimBlock, crAnimCacheStats);
PF_LINK(crAnimCachePage, crAnimCacheStats);
#endif // CR_STATS

////////////////////////////////////////////////////////////////////////////////

crAnimCache::crAnimCache()
: m_MaxBlockSize(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimCache::crAnimCache(u32 maxBlockSize, u32 maxNumBlocks)
: m_MaxBlockSize(0)
{
	Init(maxBlockSize, maxNumBlocks);
}

////////////////////////////////////////////////////////////////////////////////

crAnimCache::~crAnimCache()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimCache::Init(u32 maxBlockSize, u32 maxNumBlocks)
{
	m_MaxBlockSize = maxBlockSize;

	m_BlockCache.Init(maxNumBlocks, maxBlockSize);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimCache::Shutdown()
{
	m_MaxBlockSize = 0;

	m_BlockCache.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

struct DecompressAnimBlockCallbackData
{
	const crBlockStream* m_Block;
};

////////////////////////////////////////////////////////////////////////////////

static u32 DecompressAnimBlockCallback(void* calcData, u8* buffer)
{
	DecompressAnimBlockCallbackData& cbData = *reinterpret_cast<DecompressAnimBlockCallbackData*>(calcData);
	return crAnimCache::DecompressAnimBlock(*cbData.m_Block, buffer);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimCache::FindAnimBlock(crLock& outLock, const crBlockStream& block, const crAnimation& anim)
{
	Assert(block.IsCompact());

#if CR_STATS
	PF_INCREMENT(crAnimCache_FindAnimBlock);
#endif // CR_STATS

	// stronger signature (not broken by reallocation of memory)
	u32 sig = u32(uptr(&block));
	sig ^= anim.GetSignature();
	sig ^= atHash_const_char(anim.GetName());

	u32 blockSize = block.GetBlockSize();
	Assertf(blockSize <= m_MaxBlockSize, "crAnimCache::FindAnimBlock - unable to decompress block, block size %d exceeds anim cache max block size %d.", blockSize, m_MaxBlockSize);
	DecompressAnimBlockCallbackData data;
	data.m_Block = &block;

	m_BlockCache.GetEntry(sig, blockSize, DecompressAnimBlockCallback, &data, outLock);
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimCache::DecompressAnimBlock(const crBlockStream& block, u8* decompressedBlock)
{
	sysMemCpy(decompressedBlock, &block, sizeof(crBlockStream));

	ASSERT_ONLY(u32 decompressSize = ) datDecompress(decompressedBlock+sizeof(crBlockStream), block.m_DataSize, (u8*)&block+sizeof(crBlockStream), block.m_CompactSize);
	Assert(decompressSize == block.m_DataSize);

#if CR_STATS
	PF_INCREMENT(crAnimCache_DecompressAnimBlock);
#endif // CR_STATS
	return block.GetBlockSize();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimCache::ShutdownClass()
{
	// TODO HACKGTA4 JA DO A RESET TEST ON PS3
	if(sm_Instance)
	{
		delete sm_Instance;
		sm_Instance = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crAnimCache* crAnimCache::sm_Instance = NULL;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
