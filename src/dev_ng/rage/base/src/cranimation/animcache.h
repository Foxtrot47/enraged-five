//
// cranimation/animcache.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMCACHE_H
#define CRANIMATION_ANIMCACHE_H

#include "animtolerance.h"
#include "frameaccelerator.h"

namespace rage
{

struct crBlockStream;

// PURPOSE: Animation cache, used to decompress and store
// animation blocks that have been subject to secondary compression (compacted)
class crAnimCache
{
public:

	// PURPOSE: Default constructor
	crAnimCache();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// maxBlockSize - maximum block size supported by cache (in bytes)
	// maxNumBlocks - maximum number of blocks in the cache
	crAnimCache(u32 maxBlockSize, u32 maxNumBlocks);

	// PURPOSE: Destructor
	~crAnimCache();

	// PURPOSE: Initialization
	// maxBlockSize - maximum block size supported by cache (in bytes)
	// maxNumBlocks - maximum number of blocks in the cache
	void Init(u32 maxBlockSize, u32 maxNumBlocks);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Find decompressed compacted animation block based on signature
	// PARAMS:
	// outLock - lock wrapped pointer to decompressed compacted block (NULL if block not found, and not, or can't be generated)
	// block - compacted animation block
	// anim - compacted animation
	// generate - if find fails, decompress and cache compacted block
	// NOTES: Pointer is wrapped in lock structure to protect it from being recycled by other threads.
	void FindAnimBlock(crLock& outLock, const crBlockStream& block, const crAnimation& anim);

	// PURPOSE: Calculate indices, based on two frames
	// NOTES: Forces calculation of indices every time.
	// Used internally by FindFrameIndices, to generate indices if not already cached.
	static u32 DecompressAnimBlock(const crBlockStream& block, u8* decompressedBlock);

	// PURPOSE: Maximum block size supported by cache (in bytes)
	u32 GetMaxBlockSize() const;


	// PURPOSE: Get static animation cache instance
	// NOTES: Will automatically create an instance, if none exists (to override default, call SetInstance first)
	static crAnimCache& GetInstance();

	// PURPOSE: Set static animation cache instance
	// NOTES: Allows external setup of the animation cache instance (call before any on demand creation)
	// Animation system will assume responsibility for destruction on shutting down
	static void SetInstance(crAnimCache& cache);

	// PURPOSE: Shutdown class, automatically called from higher level crAnimation::ShutdownClass
	static void ShutdownClass();

private:

	crThreadSafeAccelerator m_BlockCache;

	u32 m_MaxBlockSize;

	static crAnimCache* sm_Instance;
};

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimCache::GetMaxBlockSize() const
{
	return m_MaxBlockSize;
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimCache& crAnimCache::GetInstance()
{
	if(!sm_Instance)
	{
		// defaults for built in on demand animation cache
		// to override call SetInstance with own cache first
		const u32 maxBlockSize = CR_DEFAULT_MAX_BLOCK_SIZE;
		const u32 maxNumBlocks = 4;
		sm_Instance = rage_new crAnimCache(maxBlockSize, maxNumBlocks);
	}
	return *sm_Instance;
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimCache::SetInstance(crAnimCache& cache)
{
	// check an instance hasn't already been set or created
	// if hitting this assert, the SetInstance call is too late in startup (must be before any GetInstance calls)
	Assert(!sm_Instance);

	sm_Instance = &cache;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_ANIMCACHE_H
