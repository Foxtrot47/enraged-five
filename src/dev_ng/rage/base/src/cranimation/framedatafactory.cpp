//
// cranimation/framedatafactory.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "framedatafactory.h"

#include "commonpool.h"

#include "creature/creature.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crFrameDataFactory::crFrameDataFactory()
: m_Map(atMapHashFn<u32>(), atMapEquals<u32>(), m_MapPool.m_Functor)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataFactory::~crFrameDataFactory()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameDataFactory::Init(crCommonPool& pool)
{
	m_CommonPool = &pool;

	u32 poolSize = pool.GetSize();
	Assert(poolSize);
	m_MapPool.Create<Map::Entry>(poolSize);
	m_Map.Create((u16)poolSize, false);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameDataFactory::Shutdown()
{
	m_Map.Kill();
	m_MapPool.Destroy();
	m_CommonPool = NULL;
}

////////////////////////////////////////////////////////////////////////////////

crFrameData* crFrameDataFactory::AllocateFrameData(const crCreature& creature, crFrameFilter* filter)
{
#if FRAME_DATA_FACTORY_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // FRAME_DATA_FACTORY_THREAD_SAFE

	// check if matching frame data already exists
	u32 signature = creature.GetSignature();
	if(filter)
	{
		signature ^= filter->GetSignature();
	}
	crFrameData* frameData = FindFrameData(signature);
	if(frameData)
	{
		frameData->AddRef();
		return frameData;
	}

	// create new frame data
	frameData = InsertFrameData(signature, creature.GetNumDofs());
	creature.InitDofs(*frameData, filter);

	return frameData;
}

////////////////////////////////////////////////////////////////////////////////

crFrameData* crFrameDataFactory::DuplicateFrameData(const crFrameData& src)
{
#if FRAME_DATA_FACTORY_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // FRAME_DATA_FACTORY_THREAD_SAFE

	// check if matching frame data already exists
	u32 signature = src.GetSignature();
	crFrameData* frameData = FindFrameData(signature);
	if(frameData)
	{
		frameData->AddRef();
		return frameData;
	}

	// create new frame data
	frameData = InsertFrameData(signature, src.GetNumDofs());

	sysMemCpy(frameData->m_Dofs, src.m_Dofs, frameData->m_DofsBufferSize);
	sysMemCpy(frameData->m_NumTypes, src.m_NumTypes, sizeof(frameData->m_NumTypes));

	frameData->m_Signature = src.m_Signature;
	frameData->m_NumDofs = src.m_NumDofs;
	frameData->m_FrameBufferSize = src.m_FrameBufferSize;

	return frameData;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameDataFactory::ReleaseFrameData(crFrameData& frameData)
{
#if FRAME_DATA_FACTORY_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // FRAME_DATA_FACTORY_THREAD_SAFE

	Assert(frameData.GetRef() > 1);

	// release use reference
	frameData.Release();

	// if only garbage collection reference remains, clean up now
	if(frameData.GetRef() == 1)
	{	
		DeleteFrameData(frameData);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameDataFactory::GarbageCollect()
{
	// check all the frame data for garbage collection
	Map::Iterator it = m_Map.CreateIterator();
	while(!it.AtEnd())
	{
		crFrameData* frameData = it.GetData();

		// increment iterator now - before potentially removing data from map
		it.Next();

		// if only garbage collection reference remains, clean up now
		if(frameData->GetRef() == 1)
		{
			DeleteFrameData(*frameData);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrameData* crFrameDataFactory::FindFrameData(u32 signature) const
{
	crFrameData*const* frameDataPtr = m_Map.Access(signature);
	if(frameDataPtr)
	{
		return *frameDataPtr;
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crFrameData* crFrameDataFactory::InsertFrameData(u32 signature, u32 numDofs)
{
	u32 bufferSize = crFrameData::CalcDofsBufferSize(numDofs);

	// initialize frame data
	u8* buffer;
	crFrameData* frameData = rage_placement_new(m_CommonPool->Allocate(&buffer, sizeof(crFrameData), bufferSize)) crFrameData();
	Assert(frameData->GetRef() == 1);
	frameData->Init(buffer, bufferSize, false);

	// also store creature signature in top of buffer (used for fast cleanup)
	*reinterpret_cast<u32*>(buffer - sizeof(u32)) = signature;

	// add additional reference - for this first use (frame data already has a base one for garbage collection)
	frameData->AddRef();

	// add to map
	m_Map.Insert(signature, frameData);

	return frameData;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameDataFactory::DeleteFrameData(crFrameData& frameData)
{
	u8* buffer = reinterpret_cast<u8*>(frameData.m_Dofs);
	Assert(buffer);

	// remove the frame data from the map
	u32 signature = *reinterpret_cast<const u32*>(buffer - sizeof(u32));
	Assert(FindFrameData(signature) == &frameData);
	m_Map.Delete(signature);

	// release memory
	frameData.Shutdown();
	m_CommonPool->Release(&frameData, buffer);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage