//
// cranimation/animation.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "animation.h"

#include "animarchive.h"
#include "animcache.h"
#include "animchannel.h"
#include "animpacker.h"
#include "animstream.h"
#include "animtrack.h"
#include "frame.h"
#include "frameiterators.h"
#include "framefilters.h"

#include "crmetadata/property.h"
#include "file/asset.h"
#include "paging/rscbuilder.h"
#include "system/magicnumber.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crAnimation::crAnimation()
: m_Flags(0)
, m_ProjectFlags(0)
, m_NumFrames(0)
, m_FramesPerChunk(sm_DefaultFramesPerChunk)
, m_Duration(0.f)
, m_Signature(0)
, m_Name(NULL)
, m_MaxBlockSize(0)
, m_RefCount(1)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimation::crAnimation(datResource &rsc)
: m_Blocks(rsc, true)
, m_Dofs(rsc, true)
{
	rsc.PointerFixup(m_Name);
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crAnimation);

#if __DECLARESTRUCT
void crAnimation::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(crAnimation);
	STRUCT_FIELD(m_Flags);
	STRUCT_FIELD(m_ProjectFlags);
	STRUCT_FIELD(m_NumFrames);
	STRUCT_FIELD(m_FramesPerChunk);
	STRUCT_FIELD(m_Duration);
	STRUCT_FIELD(m_Signature);
	STRUCT_FIELD(m_Name);
	STRUCT_SKIP(m_Tracks, sizeof(m_Tracks));
	STRUCT_FIELD(m_MaxBlockSize);
	STRUCT_FIELD(m_RefCount);
	STRUCT_FIELD(m_Blocks);
	STRUCT_FIELD(m_Dofs);
	STRUCT_END();
}
////////////////////////////////////////////////////////////////////////////////

void crAnimation::Dof::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(Dof);
	STRUCT_FIELD(m_Id);
	STRUCT_FIELD(m_Type);
	STRUCT_FIELD(m_Track);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crAnimation::ResourcePageIn(datResource &rsc)
{
	::new(rsc.GetBase()) crAnimation(rsc);
}

////////////////////////////////////////////////////////////////////////////////

crAnimation::~crAnimation()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::Shutdown()
{
#if CR_DEV
	const int numTracks = m_Tracks.GetCount();
	for(int i=0; i<numTracks; ++i)
	{
		delete m_Tracks[i];
	}
	m_Tracks.Reset();
#endif

	for(int i=0; i<m_Blocks.GetCount(); ++i)
	{
		delete m_Blocks[i];
	}
	m_Blocks.Reset();

	delete m_Name;
	m_Name = 0;

	m_Flags = 0;
	m_NumFrames = 0;
	m_FramesPerChunk = 0;
	m_Duration = 0.f;
	m_Signature = 0;

	m_ProjectFlags = 0;

	m_MaxBlockSize = 0;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::CompositeFrame(float time, crFrame& inoutFrame, bool closest, bool clear) const
{
	float frameTime = closest ? float(int(ConvertTimeToInternalFrame(time) + 0.5f)) : ConvertTimeToInternalFrame(time);

	if(clear)
	{
		inoutFrame.Zero();
	}
	inoutFrame.Composite(*this, ConvertInternalFrameToTime(frameTime));
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::CompositeFrameWithMover(float time, float delta, crFrame& inoutFrame, bool closest, bool clear) const
{
	float frameTime = closest ? float(int(ConvertTimeToInternalFrame(time) + 0.5f)) : ConvertTimeToInternalFrame(time);

	if(clear)
	{
		inoutFrame.Zero();
	}
	inoutFrame.CompositeWithDelta(*this, ConvertInternalFrameToTime(frameTime), delta);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::SetName(const char* name)
{
	delete m_Name;
	m_Name = StringDuplicate(name);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

#if CR_DEV
		crAnimChannel::InitClass();
#endif
		crProperty::InitClass();
		crFrameFilter::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::ShutdownClass()
{
	crFrameFilter::ShutdownClass();
#if CR_DEV
	crAnimChannel::ShutdownClass();
#endif
	crAnimCache::ShutdownClass();
	crProperty::ShutdownClass();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

const crAnimation::Dof* crAnimation::FindDof(u8 track, u16 id) const
{
	int low = 0;
	int high = m_Dofs.GetCount()-1;

	u32 trackId = u32(track)<<16|u32(id);
	while(low <= high)
	{
		int idx = (low+high) >> 1;
		const Dof& currDof = m_Dofs[idx];
		u32 currTrackId = u32(currDof.m_Track)<<16|u32(currDof.m_Id);

		if(currTrackId == trackId)
		{
			return &currDof;
		}
		else if(currTrackId > trackId)
		{
			high = idx-1;
		}
		else
		{
			low = idx+1;
		}
	}

	return NULL;		
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
crAnimTrack* crAnimation::FindTrack(u8 track, u16 id) const
{
	int low = 0;
	int high = m_Tracks.GetCount()-1;

	while(low <= high)
	{
		int mid = (low+high) >> 1;
		crAnimTrack* currTrack = m_Tracks[mid];
		bool bMissed;
		if(currTrack->TestTrackAndId(track, id, bMissed))
		{
			return currTrack;
		}
		else if(bMissed)
		{
			high = mid-1;
		}
		else
		{
			low = mid+1;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crAnimTrack* crAnimation::FindTrackIndex(u8 track, u16 id, int& outIdx) const
{
	int low = 0;
	int high = m_Tracks.GetCount()-1;

	while(low <= high)
	{
		outIdx = (low+high) >> 1;
		crAnimTrack* currTrack = m_Tracks[outIdx];
		bool bMissed;
		if(currTrack->TestTrackAndId(track, id, bMissed))
		{
			return currTrack;
		}
		else if(bMissed)
		{
			high = outIdx-1;
		}
		else
		{
			low = outIdx+1;
		}
	}

	outIdx = low;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::AddTrack(crAnimTrack* track)
{
	int idx;
	if(FindTrackIndex(track->GetTrack(), track->GetId(), idx))
	{
		// can't add track, its already present!
		return false;
	}
	else
	{
		// check track uses same frames per chunk alignment
		//Assert(track->GetNumInternalFramesPerChunk() == m_FramesPerChunk);

		// if attempting to add track to packed animation, must unpack first
		Assert(!IsPacked());

		m_Tracks.Grow(1);
		m_Tracks.DeleteFast(m_Tracks.GetCount()-1);
		m_Tracks.Insert(idx) = track;

		// invalidate signature
		m_Signature = 0;
		return true;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::DeleteTrack(int idx)
{
	if(idx < 0 || idx >= m_Tracks.GetCount())
	{
		return false;
	}

	delete m_Tracks[idx];
	m_Tracks.Delete(idx);

	CalcSignature();
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::Pack(bool compact, bool trackStrip, bool byteSwap)
{
	if(!IsPacked())
	{
		m_MaxBlockSize = crAnimPacker::Pack(compact, byteSwap, *this);

		sysMemStartTemp();
		if(trackStrip)
		{
			for(int i=0; i < m_Tracks.GetCount(); ++i)
			{
				delete m_Tracks[i];
			}
			m_Tracks.Reset();
		}
		sysMemEndTemp();

		// set packed, compact and track stripped bits
		m_Flags |= kPacked;
		if(compact)
		{
			m_Flags |= kCompact;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crAnimation* crAnimation::AllocateAndLoad(const char* filename, u8* outVersion, bool pack, bool compact, bool trackStrip)
{
	crAnimation* anim = rage_aligned_new(16) crAnimation;
	if(!anim->Load(filename, outVersion, pack, compact, trackStrip))
	{
		Errorf("crAnimation::AllocateAndLoad - failed to load '%s'", filename);
		delete anim;
		return NULL;
	}

	return anim;
}

////////////////////////////////////////////////////////////////////////////////

const u8 crAnimation::sm_SerializationVersions[] =
{
	16, // 121412 Save segment size inside linear channel
	15, // 102710 Improve packed sequence decompression and reduce padding
	14, // 111208 JM added variable bit quantization compression
	12, // 031209 JM added indirect quantization compression
	11, // 121907 JM changed layout of chunk format pack and type bits
};

////////////////////////////////////////////////////////////////////////////////

void crAnimation::SerializeANI8(crArchive& a)
{
	u16 flags = m_Flags & ~kNonSerializableMask;
	a<<=flags;
	if(a.IsLoading())
	{
		m_Flags = flags;
	}

	a<<=m_ProjectFlags;
	a<<=m_NumFrames;
	a<<=m_Duration;
	a<<=m_FramesPerChunk;

	// m_RefCount and m_Name intentionally not serialized

	const u32 numBlocks = CalcNumBlocks();

	u16 nTracks = u16(m_Tracks.GetCount());
	a<<=nTracks;
	if(a.IsLoading())
	{
		m_Tracks.Reserve(nTracks);
	}
	for(u16 i=0; i<nTracks; i++)
	{
		crAnimTrack* track;
		if(a.IsLoading())
		{
			track = rage_new crAnimTrack();
			m_Tracks.Append() = track;
			track->m_Chunks.Reserve(numBlocks);
		}
		else
		{
			track = m_Tracks[i];
		}

		track->Serialize(a);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::Load(const char* filename, u8* outVersion, bool pack, bool compact, bool trackStrip)
{
	fiStream *f = ASSET.Open(filename, "anim");
	if(f==0)
	{
		Errorf("crAnimation::Load - failed to open file '%s'", filename);
		return false;
	}

	int formatId = 0;
	f->ReadInt(&formatId, 1);

	bool success = false;
	bool rscLoad = pgRscBuilder::IsBuilding() && (&sysMemAllocator::GetCurrent() != &sysMemAllocator::GetMaster());

	switch(formatId)
	{
	case MAKE_MAGIC_NUMBER('A','N','I','8'):
		{
			if(!rscLoad)
			{
				m_Name = StringDuplicate(filename);
			}
			pack = pack || rscLoad;

			u8 version = 0;
			f->ReadByte(&version, 1);

			const u32 numVersions = NELEM(sm_SerializationVersions);
			u32 i;
			for(i=0; i<numVersions; ++i)
			{
				if(version == sm_SerializationVersions[i])
				{
					break;
				}
			}
			if(i == numVersions)
			{
				Errorf("crAnimation - attempting to load unsupported animation version '%d' (latest version '%d' supported)", version, sm_SerializationVersions[0]);
				break;
			}

			if(outVersion)
			{
				*outVersion = version;
			}

			crArchive a(f, true, version);

			// load animation data
			if(pack)
			{
				// if packed, prevent serialized animation data being allocated directly into resource
				sysMemStartTemp();
			}
			SerializeANI8(a);
			if(pack)
			{
				sysMemEndTemp();
			}

			CalcSignature();
			
			if(!m_Tracks.GetCount())
			{
				Errorf("crAnimation::Load - cannot load empty animation file '%s'", filename);
				break;
			}

			if(pack)
			{
				// force data to be packed (and possibly compacted)
				Pack(compact, trackStrip);
			}

			success = true;
		}
		break;

	case MAKE_MAGIC_NUMBER('A','N','I','1'):
	case MAKE_MAGIC_NUMBER('c','m','p',0):
	case MAKE_MAGIC_NUMBER('a','n','i',0):
	case MAKE_MAGIC_NUMBER('A','N','I','5'):
		Errorf("crAnimation::Load - animation file '%s' is of format 0x%x.  This file format is no longer supported.  Please re-export using up-to-date version of exporter...", filename, formatId);
		break;

	default:
		Errorf("crAnimation::Load - file '%s' has unknown format identifier 0x%x.  Corrupt, or not an animation file?", filename, formatId);
		break;
	}

	f->Close();	

	return success;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::Save(const char* filename)
{
	fiStream *f = ASSET.Create(filename ? filename : m_Name, "anim");
	if(f==0)
	{
		return false;
	}

	int formatId = MAKE_MAGIC_NUMBER('A','N','I','8');
	f->WriteInt(&formatId, 1);

	u8 version = sm_SerializationVersions[0];
	f->WriteByte(&version, 1);

	crArchive a(f, false, version);
	SerializeANI8(a);

	f->Close();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::Create(u32 numFrames, float duration, bool looped, bool mover, u32 framesPerChunk, bool raw)
{
	if(m_NumFrames)
	{
		return false;
	}

	Assert(numFrames > 0);
	Assert(framesPerChunk > 1);

	Assign(m_NumFrames, numFrames);
	Assign(m_FramesPerChunk, framesPerChunk);
	m_Duration = duration;
	m_Flags |= looped ? kLooped : 0;
	m_Flags |= mover ? kMoverTracks : 0;
	m_Flags |= raw ? kRaw : 0;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::CreateTrack(crAnimTrack* track)
{
	Assert(m_NumFrames);

	if(AddTrack(track))
	{
		CalcSignature();
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crAnimTrack* crAnimation::GetTrack(int idx) const
{
	return m_Tracks[idx];
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimation::GetNumTracks() const
{
	return m_Tracks.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::HasTrack(u8 track, u16 id) const
{
	return FindTrack(track, id) != NULL;
}

////////////////////////////////////////////////////////////////////////////////

template<bool fast>
class CreateIterator : public crFrameIteratorByType<CreateIterator<fast> >
{
public:
	CreateIterator(const atArray<const crFrame*>& frames, const crAnimTolerance& tolerance, crAnimation& anim)
		: crFrameIteratorByType<CreateIterator<fast> >(*frames[0])
		, m_Frames(&frames)
		, m_Animation(&anim)
		, m_Tolerance(&tolerance)
	{
	}

	void Iterate(crFrameFilter* filter, float weight, bool validOnly)
	{
		crFrameIteratorByType<CreateIterator<fast> >::Iterate(filter, weight, validOnly);

		m_Animation->CalcSignature();
	}

	template<typename _T>
	void IterateDofByType(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		crAnimTrack* pTrack = rage_new crAnimTrack(dof.m_Track, dof.m_Id);
		AssertVerify(m_Animation->AddTrack(pTrack));

		u32 frameOffset = dof.m_Offset;
		atArray<_T> values;
		values.Resize(m_Animation->m_NumFrames);

		values[0] = dest.GetUnsafe<_T>();
		for(int n=1; n<m_Frames->GetCount(); n++)
		{
			const crFrame& frame = *(*m_Frames)[n];

			if(fast)
			{
				Assert(frame.HasDof(dof.m_Track, dof.m_Id));
				Assert(frame.HasDofValid(dof.m_Track, dof.m_Id));
				Assert((*m_Frames)[0]->GetSignature() == (*m_Frames)[n]->GetSignature());
				Assert((*m_Frames)[0]->GetNumDofs() == (*m_Frames)[n]->GetNumDofs());

				values[n] = *reinterpret_cast<const _T*>(frame.GetBuffer() + frameOffset);
			}
			else if(!frame.GetValue(dof.m_Track, dof.m_Id, values[n]))
			{
				values[n] = values[0];
			}
		}

		pTrack->Create(values, *m_Tolerance, m_Animation->m_FramesPerChunk);
	}

	const atArray<const crFrame*>* m_Frames;
	crAnimation* m_Animation;
	const crAnimTolerance* m_Tolerance;
};

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::CreateFromFrames(const atArray<const crFrame*>& frames, const crAnimTolerance& tolerance)
{
	Assert(m_NumFrames);
	Assert(m_NumFrames == frames.GetCount());

	bool identical = true;
	for(int n=1; n<frames.GetCount(); ++n)
	{
		if(frames[0]->GetSignature() != frames[n]->GetSignature())
		{
			identical = false;
		}
	}
	if(identical)
	{
		return CreateFromFramesFast(frames, tolerance);
	}

	CreateIterator<false> it(frames, tolerance, *this);
	it.Iterate(NULL, 1.f, true);

	return m_Tracks.GetCount()!=0;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimation::CreateFromFramesFast(const atArray<const crFrame*>& frames, const crAnimTolerance& tolerance)
{
	if(!m_NumFrames || m_NumFrames != frames.GetCount())
	{
		return false;
	}

	CreateIterator<true> it(frames, tolerance, *this);
	it.Iterate(NULL, 1.f, true);

	return m_Tracks.GetCount()!=0;
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimation::ComputeSize() const
{
	u32 size = sizeof(crAnimation);

	for(int i=0; i<m_Blocks.GetCount(); i++)
	{
		size += m_Blocks[i]->GetBlockSize();
	}

	for(int i=0; i<m_Tracks.GetCount(); i++)
	{
		size += m_Tracks[i]->ComputeSize() + sizeof(crAnimTrack*);
	}

	if(m_Name)
	{
		size += ustrlen(m_Name) + 1;
	}

	return size;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimation::CalcSignature()
{
	// fletcher's checksum, cheap, but not the best choice: poor coverage over small byte range
	u32 idx = 0;
	u32 len = m_Tracks.GetCount();
	u32 sum1 = 0xffff, sum2 = 0xffff;

	while (len)
	{
		u32 tlen = len > 180 ? 180 : len;
		len -= tlen;
		do
		{
			const crAnimTrack& track = *m_Tracks[idx++];

			u16 data = track.GetTrack()<<8 | track.GetType() | 0x80;

			sum1 += data;
			sum2 += sum1;

			data = track.GetId();

			sum1 += data;
			sum2 += sum1;
		}
		while (--tlen);

		sum1 = (sum1 & 0xffff) + (sum1 >> 16);
		sum2 = (sum2 & 0xffff) + (sum2 >> 16);
	}

	sum1 = (sum1 & 0xffff) + (sum1 >> 16);
	sum2 = (sum2 & 0xffff) + (sum2 >> 16);

	// guarantees zero is not used, that represents no checksum
	m_Signature = (sum2 << 16) | sum1;
	Assert(m_Signature);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage