//
// cranimation/framebuffers.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "framebuffers.h"

#include "commonpool.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crFrameBufferDynamic::crFrameBufferDynamic()
: crFrameBuffer()
, m_Free(NULL)
, m_Allocated(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferDynamic::crFrameBufferDynamic(const crSkeletonData& skelData, bool hasMover, u16 moverId)
: crFrameBuffer()
, m_Free(NULL)
, m_Allocated(NULL)
{
	Init(skelData, hasMover, moverId);
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferDynamic::crFrameBufferDynamic(crCreature& creature)
: crFrameBuffer()
, m_Free(NULL)
, m_Allocated(NULL)
{
	Init(creature);
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferDynamic::crFrameBufferDynamic(const crFrame& frame, crFrameFilter* filter)
: crFrameBuffer()
, m_Free(NULL)
, m_Allocated(NULL)
{
	Init(frame, filter);
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferDynamic::crFrameBufferDynamic(int numDofs, u8 tracks[], u16 ids[], u8 types[])
: crFrameBuffer()
, m_Free(NULL)
, m_Allocated(NULL)
{
	Init(numDofs, tracks, ids, types);
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferDynamic::~crFrameBufferDynamic()
{
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferDynamic::Init(const crSkeletonData& skelData, bool hasMover, u16 moverId)
{
	Reset();
	Entry* entry = rage_new Entry;
	entry->m_Frame.InitCreateBoneAndMoverDofs(skelData, hasMover, moverId);
	entry->m_Next = NULL;
	m_Free = entry;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferDynamic::Init(crCreature& creature)
{
	Reset();
	Entry* entry = rage_new Entry;
	crFrameData* frameData = rage_new crFrameData;
	creature.InitDofs(*frameData);
	entry->m_Frame.Init(*frameData);
	frameData->Release();
	entry->m_Next = NULL;
	m_Free = entry;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferDynamic::Init(const crFrame& frame, crFrameFilter* filter)
{
	Reset();
	Entry* entry = rage_new Entry;
	entry->m_Frame.Init(frame, true, filter);
	entry->m_Next = NULL;
	m_Free = entry;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferDynamic::Init(int numDofs, u8 tracks[], u16 ids[], u8 types[])
{
	Reset();
	Entry* entry = rage_new Entry;
	entry->m_Frame.InitCreateDofs(numDofs, tracks, ids, types);
	entry->m_Next = NULL;
	m_Free = entry;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferDynamic::Reset()
{
	while(m_Allocated)
	{
		Entry* entry = m_Allocated;
		m_Allocated = entry->m_Next;
		delete entry;
	}

	while(m_Free)
	{
		Entry* entry = m_Free;
		m_Free = entry->m_Next;
		delete entry;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferDynamic::Reserve(int numFrames)
{
	Entry* entry = m_Free;
	while(entry)
	{
		numFrames--;
		entry = entry->m_Next;
	}

	entry = m_Allocated;
	while(entry)
	{
		numFrames--;
		entry = entry->m_Next;
	}

	while(numFrames-- > 0)
	{
		entry = rage_new Entry;
		entry->m_Frame.Init(m_Free?m_Free->m_Frame:m_Allocated->m_Frame);
		entry->m_Next = m_Free;
		m_Free = entry;
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrame* crFrameBufferDynamic::AllocateFrame()
{
#if FRAME_BUFFERS_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // FRAME_BUFFERS_THREAD_SAFE

	Entry* entry = m_Free;
	if(entry)
	{
		m_Free = entry->m_Next;
	}
	else
	{
		// free list exhausted, allocate new entry - copy frame dofs from head of allocated list
		entry = rage_new Entry;
		FastAssert(m_Allocated);
		entry->m_Frame.Init(m_Allocated->m_Frame);
	}

	entry->m_Next = m_Allocated;
	m_Allocated = entry;

	return &entry->m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferDynamic::ReleaseFrame(crFrame* frame)
{
#if FRAME_BUFFERS_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // FRAME_BUFFERS_THREAD_SAFE

	Entry** ppEntry = &m_Allocated;
	while(*ppEntry)
	{
		Entry* entry = *ppEntry;
		if(&entry->m_Frame == frame)
		{
			// found frame, release back to free list
			*ppEntry = entry->m_Next;
			entry->m_Next = m_Free;
			m_Free = entry;
			return;
		}

		ppEntry = &entry->m_Next;
	}

	// failed to find frame, if not null then this wasn't allocated from this buffer (or is a double free)?
	FastAssert(!frame);
}

////////////////////////////////////////////////////////////////////////////////

const crFrameData* crFrameBufferDynamic::GetFrameData() const
{
	if(m_Free)
	{
		return m_Free->m_Frame.GetFrameData();
	}
	else if(m_Allocated)
	{
		return m_Allocated->m_Frame.GetFrameData();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferFrameData::crFrameBufferFrameData()
: m_CommonPool(NULL)
, m_FrameData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferFrameData::crFrameBufferFrameData(crCommonPool& pool)
: m_FrameData(NULL)
{
	Init(pool);
}

////////////////////////////////////////////////////////////////////////////////

crFrameBufferFrameData::~crFrameBufferFrameData()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferFrameData::Init(crCommonPool& pool)
{
	m_CommonPool = &pool;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferFrameData::SetFrameData(const crFrameData& frameData)
{
	frameData.AddRef();
	if(m_FrameData)
	{
		m_FrameData->Release();
	}
	m_FrameData = &frameData;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferFrameData::Shutdown()
{
	if(m_FrameData)
	{
		m_FrameData->Release();
		m_FrameData = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrame* crFrameBufferFrameData::AllocateFrame()
{
	FastAssert(m_FrameData);
	const crFrameData* frameData = m_FrameData;
	u32 frameBufferSize = frameData->GetFrameBufferSize();

	u8* buffer;
	crFrame* frame = rage_placement_new(m_CommonPool->Allocate(&buffer, sizeof(crFrame), frameBufferSize)) crFrame();
	Assert(frame->GetRef() == 1);
	frame->Init(*frameData, buffer, frameBufferSize, false);

	return frame;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameBufferFrameData::ReleaseFrame(crFrame* frame)
{
	Assert(frame);
	u8* buffer = frame->GetBuffer();
	frame->Shutdown();
	m_CommonPool->Release(frame, buffer);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
