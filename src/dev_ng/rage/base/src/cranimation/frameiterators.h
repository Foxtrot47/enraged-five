//
// cranimation/frameiterators.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_FRAMEITERATORS_H
#define CRANIMATION_FRAMEITERATORS_H

#include "animation.h"
#include "animation_config.h"
#include "animcache.h"
#include "animstream.h"
#include "animtrack.h"
#include "frame.h"
#include "framefilters.h"
#include "frameaccelerator.h"

#include "crskeleton/skeletondata.h"
#include "math/simplemath.h"
#include "system/cache.h"
#include "system/memory.h"

#define FRAMEITERATOR_TEMPLATE_OPTIMIZATIONS (__OPTIMIZED)
#define FRAMEITERATOR_PAIR_TEMPLATE_OPTIMIZATIONS (__OPTIMIZED)
#define FRAMEITERATOR_N_TEMPLATE_OPTIMIZATIONS (__OPTIMIZED && !HACK_GTA4)
#define FRAMEITERATOR_ANIM_TEMPLATE_OPTIMIZATIONS (__OPTIMIZED)
#define FRAMEITERATOR_SKEL_TEMPLATE_OPTIMIZATIONS (__OPTIMIZED)

// recently enabled changes, remove if you experience problems
#define FRAME_ACCELERATOR_ANIM_INDICES (1)

// experimental change
#define FRAMEITERATOR_ZERO_WEIGHT_OPTIMIZATION (1)


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Frame iterator
template <typename _T>
class crFrameIterator
{
public:

	// PURPOSE: Initialization
	// PARAMS: frame - frame to iterate
	crFrameIterator(crFrame& frame);

	// PURPOSE: Initialization
	// PARAMS: frame - constant frame to iterate
	crFrameIterator(const crFrame& frame);


	// PURPOSE: Simple frame iterator.  Iterates a single frame.
	// PARAMS:
	// filter - optional filter to use during iteration
	// weight - global weight to use during iteration
	// validOnly - only iterate valid degrees of freedom
	void Iterate(crFrameFilter* filter, float weight, bool validOnly);

	// PURPOSE: Pair frame iterator.  Iterates a pair of frames
	// PARAMS:
	// frame - frame to iterate against
	// filter - optional filter to use during iteration
	// weight - global weight to use during iteration
	// validOnly - only iterate matching valid degrees of freedom in supplied frame
	void IteratePair(const crFrame& frame, crFrameFilter* filter, float weight, bool validOnly);

	// PURPOSE: N-way frame iterator.  Iterates a base frame and N other frames
	// PARAMS:
	// num - number of frames to iterate against
	// frames - pointer to list of frame pointers to iterate against
	// filter - optional global filter to use during iteration
	// filters - optional pointer to list of optional per frame filters to use during iteration
	// weight - global weight to use during iteration
	// fixWeights - correct weights for serial application
	// validOnly - only iterate matching valid degrees of freedom in supplied frame
	void IterateN(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights, bool validOnly);

	// TODO ---- animation iteration needs to:
	// add delta support?

	// PURPOSE: Animation frame iterator.  Iterates a frame and an animation.
	// PARAMS:
	// anim - animation to iterate against
	// time - animation time to iterate against (in internal frames)
	// filter - optional global filter to use during iteration
	// weight - global weight to use during iteration
	bool IterateAnim(const crAnimation& anim, float time, crFrameFilter* filter, float weight);

	// PURPOSE: Skeleton frame iterator.  Iterates a frame and a skeleton.
	void IterateSkel(const crSkeletonData& skelData, crFrameFilter* filter, float weight, bool validOnly);


protected:
	crFrame* m_Frame;

private:

#if FRAMEITERATOR_TEMPLATE_OPTIMIZATIONS
	template<bool validOnly>
	void IterateTemplate1(crFrameFilter* filter, float weight);	
	
	template<bool validOnly, bool hasFilter>
	void IterateTemplate2(crFrameFilter* filter, float weight);	
#endif // FRAMEITERATOR_TEMPLATE_OPTIMIZATIONS


#if FRAMEITERATOR_PAIR_TEMPLATE_OPTIMIZATIONS
	template<bool validOnly>
	void IteratePairTemplate1(const crFrame& frame, crFrameFilter* filter, float weight);

	template<bool validOnly, bool noFilter>
	void IteratePairTemplate2(const crFrame& frame, crFrameFilter* filter, float weight);

	template<bool validOnly, bool noFilter, bool sigMatch>
	void IteratePairTemplate3(const crFrame& frame, crFrameFilter* filter, float weight);
#endif // FRAMEITERATOR_PAIR_TEMPLATE_OPTIMIZATIONS


#if FRAMEITERATOR_N_TEMPLATE_OPTIMIZATIONS
	template<bool validOnly>
	void IterateNTemplate1(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights);

	template<bool validOnly, bool noFilter>
	void IterateNTemplate2(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights);

	template<bool validOnly, bool noFilter, bool noFilters>
	void IterateNTemplate3(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights);

	template<bool validOnly, bool noFilter, bool noFilters, bool fixWeights>
	void IterateNTemplate4(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights);

	template<bool validOnly, bool noFilter, bool noFilters, bool fixWeights, int sigMatch>
	void IterateNTemplate5(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights);
#endif // FRAMEITERATOR_N_TEMPLATE_OPTIMIZATIONS


#if FRAMEITERATOR_ANIM_TEMPLATE_OPTIMIZATIONS
	template<bool noFilter>
	bool IterateAnimTemplate1(const crAnimation& anim, float time, crFrameFilter* filter, float weight);
#endif // FRAMEITERATOR_ANIM_TEMPLATE_OPTIMIZATIONS


#if FRAMEITERATOR_SKEL_TEMPLATE_OPTIMIZATIONS
	template<bool validOnly>
	void IterateSkelTemplate1(const crSkeletonData& skelData, crFrameFilter* filter, float weight);	

	template<bool validOnly, bool hasFilter>
	void IterateSkelTemplate2(const crSkeletonData& skelData, crFrameFilter* filter, float weight);	
#endif // FRAMEITERATOR_SKEL_TEMPLATE_OPTIMIZATIONS

};

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
class crFrameIteratorByType : public crFrameIterator<_T>
{
public:

	// PURPOSE: Initialization
	// PARAMS: frame - frame to iterate
	crFrameIteratorByType(crFrame& frame);

	// PURPOSE: Initialization
	// PARAMS: frame - constant frame to iterate
	crFrameIteratorByType(const crFrame& frame);

	// PURPOSE: Implement iterator callback, to intercept types
	void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float weight);

	// PURPOSE: Implement pair based iterator callback, to intercept types
	void IterateDofs(const crFrameData::Dof& dof, crFrame::Dof& dest, const crFrame::Dof& src, float weight);

	// PURPOSE: Implement track based iterator callback, to intercept types
	void IterateDofTrack(const crFrameData::Dof& dof, crFrame::Dof& dest, const crAnimTrack& track, float time, float weight);

	// PURPOSE: Implement bone based iterator callback, to intercept types
	void IterateDofBone(const crFrameData::Dof& dof, crFrame::Dof& dest, int boneIdx, float weight);
};

////////////////////////////////////////////////////////////////////////////////

#define BURN_UNUSED_TEMPLATE_PARAMETERS (!__PPU)

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// Internal call used to fix an array of weights so that sequential application will
// look like a percentage based application.
__forceinline void FixWeights(float* weights, int numWeights)
{
	float totalWeight = 0.f;
	for(int i=0; i<numWeights; ++i)
	{
		FastAssert(weights[i] >= 0.f);
		totalWeight += weights[i];
	}
	FastAssert(totalWeight <= (1.f + WEIGHT_ROUNDING_ERROR_TOLERANCE));
	totalWeight = Min(totalWeight, 1.f);

	float sumWeight = (totalWeight > (1.f - WEIGHT_ROUNDING_ERROR_TOLERANCE)) ? 0.f : (1.f - totalWeight);
	for(int i=0; i<numWeights; ++i)
	{
		sumWeight += weights[i];
		if(sumWeight > WEIGHT_ROUNDING_ERROR_TOLERANCE)
		{
			weights[i] = weights[i] / sumWeight;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// Internal call used to fix an array of weights so that sequential application will
// look like a percentage based application.
__forceinline void FixWeights(float destWeight, float* weights, int numWeights)
{
	FastAssert(InRange(destWeight, 0.f, 1.f));

	float sumWeight = destWeight;
	for(int i=0; i<numWeights; ++i)
	{
		FastAssert(weights[i] >= 0.f);

		sumWeight += weights[i];
		if(sumWeight >= WEIGHT_ROUNDING_ERROR_TOLERANCE)
		{
			weights[i] = Clamp(weights[i] / sumWeight, 0.f, 1.f);
		}
		else
		{
			weights[i] = 0.f;
			sumWeight = 0.f;
		}
	}
}


////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// Same as FixWeights but in reverse order
__forceinline void FixWeightsReverse(float destWeight, float* weights, int numWeights)
{
	FastAssert(InRange(destWeight, 0.f, 1.f));

	float sumWeight = destWeight;
	for(int i=numWeights-1; i >=0; --i)
	{
		FastAssert(weights[i] >= 0.f);

		sumWeight += weights[i];
		if(sumWeight >= WEIGHT_ROUNDING_ERROR_TOLERANCE)
		{
			weights[i] = 1.f - Clamp(weights[i] / sumWeight, 0.f, 1.f);
		}
		else
		{
			weights[i] = 1.f;
			sumWeight = 0.f;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// Internal call used to calculate the destination weight in n-way blending
__forceinline float DestWeight(const float* weights, int numWeights)
{
	float totalWeight = 0.f;
	for(int i=0; i<numWeights; ++i)
	{
		FastAssert(weights[i] >= 0.f);
		totalWeight += weights[i];
	}
	//	FastAssert(totalWeight <= (1.f + WEIGHT_ROUNDING_ERROR_TOLERANCE));
	totalWeight = Min(totalWeight, 1.f);

	return (totalWeight > (1.f - WEIGHT_ROUNDING_ERROR_TOLERANCE)) ? 0.f : (1.f - totalWeight);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crFrameIterator<_T>::crFrameIterator(crFrame& frame)
: m_Frame(&frame)
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crFrameIterator<_T>::crFrameIterator(const crFrame& frame)
: m_Frame(const_cast<crFrame*>(&frame))
{
}

////////////////////////////////////////////////////////////////////////////////

#if FRAMEITERATOR_TEMPLATE_OPTIMIZATIONS
template<typename _T>
inline void crFrameIterator<_T>::Iterate(crFrameFilter* filter, float weight, bool validOnly)
{
	if(!m_Frame->m_FrameData)
	{
		return;
	}
	if(validOnly)
	{
		IterateTemplate1<true>(filter, weight);
	}
	else
	{
		IterateTemplate1<false>(filter, weight);
	}
}
template<typename _T> template<bool validOnly>
__forceinline void crFrameIterator<_T>::IterateTemplate1(crFrameFilter* filter, float weight)
{
	if(filter != NULL)
	{
		IterateTemplate2<validOnly, true>(filter, weight);
	}
	else
	{
		IterateTemplate2<validOnly, false>(filter, weight);
	}
}
template<typename _T> template<bool validOnly, bool hasFilter>
__forceinline void crFrameIterator<_T>::IterateTemplate2(crFrameFilter* filter, float weight)
{
#if BURN_UNUSED_TEMPLATE_PARAMETERS
	(void)filter;
#endif // BURN_UNUSED_TEMPLATE_PARAMETERS

#else // FRAMEITERATOR_TEMPLATE_OPTIMIZATIONS
template<typename _T>
void crFrameIterator<_T>::Iterate(crFrameFilter* filter, float weight, bool validOnly)
{
	if(!m_Frame->m_FrameData)
	{
		return;
	}

	const bool hasFilter = (filter != NULL);
#endif // FRAMEITERATOR_TEMPLATE_OPTIMIZATIONS

	const crFrameData* frameData = m_Frame->m_FrameData;
	Assert(frameData);

	const int nDestDofs = frameData->GetNumDofs();
	if(nDestDofs > 0)
	{
		const crFrameData::Dof* pDestDof = reinterpret_cast<const crFrameData::Dof*>(&(frameData->m_Dofs[0]));
		for(int iDestDof=0; iDestDof<nDestDofs; ++iDestDof, ++pDestDof)
		{
			const crFrameData::Dof& destDof = *pDestDof;

			float opWeight = 1.f;

			if(!validOnly || !m_Frame->FastIsDofInvalid(iDestDof))
			{
				if(!hasFilter || filter->FilterDof(destDof.m_Track, destDof.m_Id, opWeight))
				{
					crFrame::Dof dest(*m_Frame, u16(iDestDof), destDof.m_Offset);

					_T& t = *(_T*)(this);
					t.IterateDof(destDof, dest, weight*opWeight);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

#if FRAMEITERATOR_PAIR_TEMPLATE_OPTIMIZATIONS
template<typename _T>
inline void crFrameIterator<_T>::IteratePair(const crFrame& frame, crFrameFilter* filter, float weight, bool validOnly)
{
	if(!m_Frame->m_FrameData || !frame.m_FrameData)
	{
		return;
	}
#if FRAMEITERATOR_ZERO_WEIGHT_OPTIMIZATION
	if(IsNearZero(weight))
	{
		return;
	}
#endif // FRAMEITERATOR_ZERO_WEIGHT_OPTIMIZATION
	if(validOnly)
	{
		IteratePairTemplate1<true>(frame, filter, weight);
	}
	else
	{
		IteratePairTemplate1<false>(frame, filter, weight);
	}
}
template<typename _T> template<bool validOnly>
__forceinline void crFrameIterator<_T>::IteratePairTemplate1(const crFrame& frame, crFrameFilter* filter, float weight)
{
	if(filter != NULL)
	{
		IteratePairTemplate2<validOnly, false>(frame, filter, weight);
	}
	else
	{
		IteratePairTemplate2<validOnly, true>(frame, filter, weight);
	}
}
template<typename _T> template<bool validOnly, bool noFilter>
__forceinline void crFrameIterator<_T>::IteratePairTemplate2(const crFrame& frame, crFrameFilter* filter, float weight)
{
	if(m_Frame->GetSignature() && m_Frame->GetSignature() == frame.GetSignature())
	{
		IteratePairTemplate3<validOnly, noFilter, true>(frame, filter, weight);
	}
	else
	{
		IteratePairTemplate3<validOnly, noFilter, false>(frame, filter, weight);
	}
}
template<typename _T> template<bool validOnly, bool noFilter, bool sigMatch>
__forceinline void crFrameIterator<_T>::IteratePairTemplate3(const crFrame& frame, crFrameFilter* filter, float weight)
{
#if BURN_UNUSED_TEMPLATE_PARAMETERS
	(void)filter;
#endif // BURN_UNUSED_TEMPLATE_PARAMETERS

#else // FRAMEITERATOR_PAIR_TEMPLATE_OPTIMIZATIONS
template<typename _T>
void crFrameIterator<_T>::IteratePair(const crFrame& frame, crFrameFilter* filter, float weight, bool validOnly)
{
	if(!m_Frame->m_FrameData || !frame.m_FrameData)
	{
		return;
	}
#if FRAMEITERATOR_ZERO_WEIGHT_OPTIMIZATION
	if(IsNearZero(weight))
	{
		return;
	}
#endif // FRAMEITERATOR_ZERO_WEIGHT_OPTIMIZATION

	const bool noFilter = (filter == NULL);
	const bool sigMatch = (m_Frame->GetSignature() && m_Frame->GetSignature() == frame.GetSignature());
#endif // FRAMEITERATOR_PAIR_TEMPLATE_OPTIMIZATIONS

	const int nSrcDofs = frame.GetNumDofs();
	int iSrcDof = 0;

	const int nDestDofs = m_Frame->GetNumDofs();

	if(sigMatch)
	{
		// really quick path if frame signatures match
		if(nDestDofs > 0)
		{
			const crFrameData::Dof* pDestDof = reinterpret_cast<const crFrameData::Dof*>(&(m_Frame->m_FrameData->m_Dofs[0]));

			for(int iDof=0; iDof<nDestDofs; ++iDof, ++pDestDof)
			{
				const crFrameData::Dof& destDof = *pDestDof;

				FastAssert(destDof == frame.m_FrameData->m_Dofs[iDof]);
				FastAssert(destDof.m_Type == frame.m_FrameData->m_Dofs[iDof].m_Type);

				float opWeight = 1.f;

				if((!validOnly || !frame.FastIsDofInvalid(iDof)) && (noFilter || filter->FilterDof(destDof.m_Track, destDof.m_Id, opWeight)))
				{
					const crFrame::Dof src(frame, u16(iDof), destDof.m_Offset);
					crFrame::Dof dest(*m_Frame, u16(iDof), destDof.m_Offset);
	
					_T& t = *(_T*)(this);
					t.IterateDofs(destDof, dest, src, weight*opWeight);
				}
			}
		}
	}
	else
	{
		if(nDestDofs > 0 && nSrcDofs > 0)
		{
			const crFrameData::Dof* pDestDof = reinterpret_cast<const crFrameData::Dof*>(&(m_Frame->m_FrameData->m_Dofs[0]));
			const crFrameData::Dof* pSrcDof = reinterpret_cast<const crFrameData::Dof*>(&(frame.m_FrameData->m_Dofs[0]));

			for(int iDestDof=0; iDestDof<nDestDofs; ++iDestDof, ++pDestDof)
			{
				const crFrameData::Dof& destDof = *pDestDof;

				float opWeight = 1.f;

				for(; iSrcDof<nSrcDofs; ++iSrcDof, ++pSrcDof)
				{
					const crFrameData::Dof& srcDof = *pSrcDof;

					// it may look like the srcDof and destDof in this test are the wrong way round,
					// but think of the cases like this:
					// 1) srcDof == destDof then we have a match!
					// 2) srcDof > destDof (ie srcTrack > destTrack, or srcTrack == destTrack and srcId > destId)
					// then no advancing of srcDof will re-sync it, destDof needs to advance.
					// 3) srcDof < destDof (ie srcTrack < destTrack, or srcTrack == destTrack and srcId < destId)
					// then srcDof needs to advance to re-sync.
					if(srcDof == destDof)
					{
						if((!validOnly || !frame.FastIsDofInvalid(iSrcDof)) && (noFilter || filter->FilterDof(destDof.m_Track, destDof.m_Id, opWeight)))
						{
							const crFrame::Dof src(frame, u16(iSrcDof), srcDof.m_Offset);
							crFrame::Dof dest(*m_Frame, u16(iDestDof), destDof.m_Offset);

							_T& t = *(_T*)(this);
							t.IterateDofs(destDof, dest, src, weight*opWeight);
						}
						++iSrcDof;
						++pSrcDof;
						break;
					}
					else if(srcDof > destDof)
					{
						// missed, dest dof isn't present in src - carry on searching from next dof
						break;
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

#if FRAMEITERATOR_N_TEMPLATE_OPTIMIZATIONS
template<typename _T>
inline void crFrameIterator<_T>::IterateN(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights, bool validOnly)
{
	if(!m_Frame->m_FrameData)
	{
		return;
	}
	if(validOnly)
	{
		IterateNTemplate1<true>(num, frames, filter, filters, weights, fixWeights);
	}
	else
	{
		IterateNTemplate1<false>(num, frames, filter, filters, weights, fixWeights);
	}
}

template<typename _T> template<bool validOnly>
__forceinline void crFrameIterator<_T>::IterateNTemplate1(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights)
{
	if(!filter)
	{
		IterateNTemplate2<validOnly, true>(num, frames, filter, filters, weights, fixWeights);
		return;
	}
	IterateNTemplate2<validOnly, false>(num, frames, filter, filters, weights, fixWeights);
}

template<typename _T> template<bool validOnly, bool noFilter>
__forceinline void crFrameIterator<_T>::IterateNTemplate2(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights)
{
	if(!filters)
	{
		IterateNTemplate3<validOnly, noFilter, true>(num, frames, filter, filters, weights, fixWeights);
		return;
	}
	IterateNTemplate3<validOnly, noFilter, false>(num, frames, filter, filters, weights, fixWeights);
}

template<typename _T> template<bool validOnly, bool noFilter, bool noFilters>
__forceinline void crFrameIterator<_T>::IterateNTemplate3(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights)
{
	if(fixWeights)
	{
		IterateNTemplate4<validOnly, noFilter, noFilters, true>(num, frames, filter, filters, weights);
		return;
	}
	IterateNTemplate4<validOnly, noFilter, noFilters, false>(num, frames, filter, filters, weights);
}

template<typename _T> template<bool validOnly, bool noFilter, bool noFilters, bool fixWeights>
__forceinline void crFrameIterator<_T>::IterateNTemplate4(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights)
{
	const u32 firstSignature = frames[0]->GetSignature();
	bool match = (firstSignature != 0);
	if(match)
	{
		for(u32 iSrcFrame=1; iSrcFrame<num; ++iSrcFrame)
		{
			FastAssert(frames[iSrcFrame]);
			const u32 signature = frames[iSrcFrame]->GetSignature();

			if(signature != firstSignature)
			{
				match = false;
				break;
			}
		}
	}

	if(match)
	{
		if(m_Frame->GetSignature() == firstSignature)
		{
			IterateNTemplate5<validOnly, noFilter, noFilters, fixWeights, 1>(num, frames, filter, filters, weights);
			return;
		}
		IterateNTemplate5<validOnly, noFilter, noFilters, fixWeights, 0>(num, frames, filter, filters, weights);
		return;
	}

	IterateNTemplate5<validOnly, noFilter, noFilters, fixWeights, -1>(num, frames, filter, filters, weights);
}

template<typename _T> template<bool validOnly, bool noFilter, bool noFilters, bool fixWeights, int sigMatch>
inline void crFrameIterator<_T>::IterateNTemplate5(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights)
{
#if BURN_UNUSED_TEMPLATE_PARAMETERS
	filter;
	filters;
#endif // BURN_UNUSED_TEMPLATE_PARAMETERS
#else // FRAMEITERATOR_N_TEMPLATE_OPTIMIZATIONS
template<typename _T>
void crFrameIterator<_T>::IterateN(u32 num, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters, const float* weights, bool fixWeights, bool validOnly)
{
	if(!m_Frame->m_FrameData)
	{
		return;
	}

	const bool noFilter = !filter;
	const bool noFilters = !filters;
	const int sigMatch = -1; // -1 == no match, 0 == src match, 1 == src and dest match
#endif // FRAMEITERATOR_N_TEMPLATE_OPTIMIZATIONS

	FastAssert(num > 0);
	FastAssert(frames);
	FastAssert(frames[0]);
	FastAssert(weights);  // TODO --- maybe make this optional?

	float destWeight = 0.f;
	if(fixWeights)
	{
		destWeight = DestWeight(weights, num);
	}

	// OPTIMIZE - weights not required if no filter, no filters and not fix weights?
	float* opWeights = Alloca(float, num);

	int* iSrcDofs = Alloca(int, num);

	const int nDestDofs = m_Frame->GetNumDofs();
	for(int iDestDof=0; iDestDof<nDestDofs; ++iDestDof)
	{
		const crFrameData::Dof& destDof = m_Frame->m_FrameData->m_Dofs[iDestDof];

		float opWeight = 1.f;
		if(noFilter || filter->FilterDof(destDof.m_Track, destDof.m_Id, opWeight))
		{
			if(opWeight != 1.f)
			{
				for(u32 i=0; i<num; ++i)
				{
					opWeights[i] = weights[i] * opWeight;
				}
			}
			else
			{
				sysMemCpy(opWeights, weights, sizeof(float)*num);
			}

			bool allMissed = true;

			for(u32 iSrcFrame=0; iSrcFrame<num; ++iSrcFrame)
			{
				FastAssert(frames[iSrcFrame]);
				const crFrame& srcFrame = *frames[iSrcFrame];

				if(sigMatch<=0)
				{
					if(sigMatch<0 || iSrcFrame==0)
					{
						int& iSrcDof = iSrcDofs[iSrcFrame];
						const int& nSrcDofs = srcFrame.GetNumDofs();

						bool missed = true;
						for(iSrcDof=0; iSrcDof<nSrcDofs; ++iSrcDof)
						{
							const crFrameData::Dof& srcDof = srcFrame.m_FrameData->m_Dofs[iSrcDof];

							if(srcDof == destDof)
							{
								missed = false;
								break;
							}
							else if(srcDof > destDof)
							{
								break;
							}
						}

						if(missed)
						{
							iSrcDof = -1;

							if(sigMatch==0)
							{
								break;
							}
						}
					}
					else
					{
						iSrcDofs[iSrcFrame] = iSrcDofs[0];
					}
				}
				else if(sigMatch>0)
				{
					iSrcDofs[iSrcFrame] = iDestDof;
				}

				if(sigMatch>0 || iSrcDofs[iSrcFrame]>=0)
				{
					float srcOpWeight = 1.f;
					if((validOnly && frames[iSrcFrame]->FastIsDofInvalid(iSrcDofs[iSrcFrame])) || (!noFilters && filters[iSrcFrame] && !filters[iSrcFrame]->FilterDof(destDof.m_Track, destDof.m_Id, srcOpWeight)))
					{
						iSrcDofs[iSrcFrame] = -1;
						opWeights[iSrcFrame] = 0.f;
					}
					else
					{
						opWeights[iSrcFrame] *= srcOpWeight;
						allMissed = false;
					}
				}
				else
				{
					opWeights[iSrcFrame] = 0.f;
				}
			}

			if(allMissed)
			{
				continue;
			}

			if(fixWeights)
			{
				FixWeights(destWeight, opWeights, num);
			}

			const float* pWeight = opWeights;
			const int* piSrcDof = iSrcDofs;
			const crFrame*const* ppSrcFrame = frames;
			for(u32 iSrcFrame=0; iSrcFrame<num; ++iSrcFrame,++piSrcDof,++pWeight,++ppSrcFrame)
			{
				if(*piSrcDof>=0)
				{
					const float weight = *pWeight;

					// ASSUMPTION - 0.0 represents no operation
					if(weight > 0.f)
					{
						const crFrame& srcFrame = **ppSrcFrame;

						const crFrame::Dof src(srcFrame, u16(*piSrcDof));
						crFrame::Dof dest(*m_Frame, u16(iDestDof));

						_T& t = *(_T*)(this);
						t.IterateDofs(destDof, dest, src, weight);
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

#if FRAMEITERATOR_ANIM_TEMPLATE_OPTIMIZATIONS
template<typename _T>
inline bool crFrameIterator<_T>::IterateAnim(const crAnimation& anim, float time, crFrameFilter* filter, float weight)
{
	if(!m_Frame->m_FrameData)
	{
		return true;
	}
	if(filter != NULL)
	{
		return IterateAnimTemplate1<false>(anim, time, filter, weight);
	}
	else
	{
		return IterateAnimTemplate1<true>(anim, time, filter, weight);
	}
}
template<typename _T> template<bool noFilter>
__forceinline bool crFrameIterator<_T>::IterateAnimTemplate1(const crAnimation& anim, float time, crFrameFilter* filter, float weight)
{
#if BURN_UNUSED_TEMPLATE_PARAMETERS
	(void)filter;
#endif // BURN_UNUSED_TEMPLATE_PARAMETERS

#else // FRAMEITERATOR_ANIM_TEMPLATE_OPTIMIZATIONS
template<typename _T>
bool crFrameIterator<_T>::IterateAnim(const crAnimation& anim, float time, crFrameFilter* filter, float weight)
{
	if(!m_Frame->m_FrameData)
	{
		return true;
	}

	const bool noFilter = (filter == NULL);
#endif // FRAMEITERATOR_ANIM_TEMPLATE_OPTIMIZATIONS

	int numComposite = 0;
	const int numDofs = m_Frame->GetNumDofs();
	const crFrameData& frameData = *m_Frame->GetFrameData();
	(void)weight; // burn unused params, to be removed later on
	(void)filter;
	time = Clamp(time, 0.f, float(anim.GetNumInternalFrames()-1));
	const u32 blockIdx = anim.ConvertInternalFrameToBlockIndex(time);
	const crBlockStream* block = anim.GetBlock(blockIdx);

	if(block)
	{
		crLock blockLock;
		if(block->IsCompact())
		{
			crAnimCache::GetInstance().FindAnimBlock(blockLock, *block, anim);
			block = reinterpret_cast<crBlockStream*>(blockLock.Get());
		}

		const float blockTime = anim.ConvertInternalFrameToBlockTime(time, blockIdx);
		const u32 quotientTime = u32(Clamp(floor(blockTime), 0.f, float(block->m_NumFrames-2)));
		const float tr = Clamp(blockTime - float(quotientTime), 0.f, 1.f);

		u16* indices = NULL;
		crLock indicesLock;
		if(FRAME_ACCELERATOR_ANIM_INDICES)
		{
			crFrameAccelerator* accelerator = m_Frame->m_Accelerator;
			if(accelerator && m_Frame->GetSignature() && anim.GetSignature())
			{
				const crAnimation* ptr = &anim;
				accelerator->FindFrameAnimIndices(indicesLock, frameData, 1, &ptr);
				indices = reinterpret_cast<u16*>(indicesLock.Get());
			}
		}

		if(!indices)
		{
			u32 size = crFrameAccelerator::GetSizeFrameAnimIndices(anim.GetNumDofs());
			indices = reinterpret_cast<u16*>(AllocaAligned(u8, size, 16));
			ASSERT_ONLY(u32 checkSize =) crFrameAccelerator::CalcFrameAnimIndices(frameData, anim, indices);
			Assert(checkSize <= size);
		}

		block->EvaluateFrame(m_Frame->GetBuffer(), quotientTime, tr, indices);
	}
#if CR_DEV
	else
	{
		int trackIdx = 0;

		const int numTracks = anim.m_Tracks.GetCount();

		for(int dofIdx=0; dofIdx<numDofs; ++dofIdx)
		{
			const crFrameData::Dof& dof = m_Frame->m_FrameData->m_Dofs[dofIdx];
			float opWeight = 1.f;

			if(trackIdx<numTracks)
			{
				do
				{
					const crAnimTrack& track = *anim.m_Tracks[trackIdx];
					bool bMissed;
					if(track.TestTrackAndId(dof.m_Track, dof.m_Id, bMissed))
					{
						if(noFilter || filter->FilterDof(dof.m_Track, dof.m_Id, opWeight))
						{
							crFrame::Dof dest(*m_Frame, u16(dofIdx));

							static_cast<_T*>(this)->IterateDofTrack(dof, dest, track, time, weight*opWeight);
						}
						++trackIdx;
						++numComposite;
						break;
					}
					else if(bMissed)
					{
						// this dof doesn't have a track in this animation, can't composite it
						break;
					}
					else if((++trackIdx)<numTracks)
					{
						// unconventional way of structuring while loop
						// but allows use of loop conditional to control an optional clear on exiting the loop
						// without any additional conditionals being required
						continue;
					}
					break;
				}
				while(1);
			}
		}
	}
#endif // CR_DEV

	return (numComposite == numDofs);
}

////////////////////////////////////////////////////////////////////////////////

#if FRAMEITERATOR_SKEL_TEMPLATE_OPTIMIZATIONS
template<typename _T>
inline void crFrameIterator<_T>::IterateSkel(const crSkeletonData& skelData, crFrameFilter* filter, float weight, bool validOnly)
{
	if(!m_Frame->m_FrameData)
	{
		return;
	}
	if(validOnly)
	{
		IterateSkelTemplate1<true>(skelData, filter, weight);
	}
	else
	{
		IterateSkelTemplate1<false>(skelData, filter, weight);
	}
}
template<typename _T> template<bool validOnly>
__forceinline void crFrameIterator<_T>::IterateSkelTemplate1(const crSkeletonData& skelData, crFrameFilter* filter, float weight)
{
	if(filter != NULL)
	{
		IterateSkelTemplate2<validOnly, true>(skelData, filter, weight);
	}
	else
	{
		IterateSkelTemplate2<validOnly, false>(skelData, filter, weight);
	}
}
template<typename _T> template<bool validOnly, bool hasFilter>
__forceinline void crFrameIterator<_T>::IterateSkelTemplate2(const crSkeletonData& skelData, crFrameFilter* filter, float weight)
{
#if BURN_UNUSED_TEMPLATE_PARAMETERS
	(void)filter;
#endif // BURN_UNUSED_TEMPLATE_PARAMETERS

#else // FRAMEITERATOR_SKEL_TEMPLATE_OPTIMIZATIONS
template<typename _T>
void crFrameIterator<_T>::IterateSkel(const crSkeletonData& skelData, crFrameFilter* filter, float weight, bool validOnly)
{
	if(!m_Frame->m_FrameData)
	{
		return;
	}

	const bool hasFilter = (filter != NULL);

#endif // FRAMEITERATOR_SKEL_TEMPLATE_OPTIMIZATIONS

	const int nDestDofs = m_Frame->GetNumDofs();
	if(nDestDofs > 0)
	{
		const crFrameData::Dof* pDestDof = reinterpret_cast<const crFrameData::Dof*>(&(m_Frame->m_FrameData->m_Dofs[0]));
		for(int iDestDof=0; iDestDof<nDestDofs; ++iDestDof, ++pDestDof)
		{
			const crFrameData::Dof& destDof = *pDestDof;

			float opWeight = 1.f;

			if(!validOnly || !m_Frame->FastIsDofInvalid(iDestDof))
			{
				switch(destDof.m_Track)
				{
				case kTrackBoneTranslation:
					if(!hasFilter || (filter->FilterDof(destDof.m_Track, destDof.m_Id, opWeight)))
					{
						int boneIdx;
						if(skelData.ConvertBoneIdToIndex(destDof.m_Id, boneIdx))
						{
							const crBoneData* boneData = skelData.GetBoneData(boneIdx);
							if(boneData->GetDofs() & (crBoneData::TRANSLATE_X|crBoneData::TRANSLATE_Y|crBoneData::TRANSLATE_Z))
							{
								crFrame::Dof dest(*m_Frame, u16(iDestDof), destDof.m_Offset);

								_T& t = *(_T*)(this);
								t.IterateDofBone(destDof, dest, boneIdx, weight*opWeight);
							}
						}
					}
					break;

				case kTrackBoneRotation:
					if(!hasFilter || (filter->FilterDof(destDof.m_Track, destDof.m_Id, opWeight)))
					{
						int boneIdx;
						if(skelData.ConvertBoneIdToIndex(destDof.m_Id, boneIdx))
						{
							const crBoneData* boneData = skelData.GetBoneData(boneIdx);								
							if(boneData->GetDofs() & (crBoneData::ROTATE_X|crBoneData::ROTATE_Y|crBoneData::ROTATE_Z))
							{
								crFrame::Dof dest(*m_Frame, u16(iDestDof), destDof.m_Offset);

								_T& t = *(_T*)(this);
								t.IterateDofBone(destDof, dest, boneIdx, weight*opWeight);
							}
						}
					}
					break;

				case kTrackBoneScale:
					if(!hasFilter || (filter->FilterDof(destDof.m_Track, destDof.m_Id, opWeight)))
					{
						int boneIdx;
						if(skelData.ConvertBoneIdToIndex(destDof.m_Id, boneIdx))
						{
							const crBoneData* boneData = skelData.GetBoneData(boneIdx);								
							if(boneData->GetDofs() & (crBoneData::SCALE_X|crBoneData::SCALE_Y|crBoneData::SCALE_Z))
							{
								crFrame::Dof dest(*m_Frame, u16(iDestDof), destDof.m_Offset);

								_T& t = *(_T*)(this);
								t.IterateDofBone(destDof, dest, boneIdx, weight*opWeight);
							}
						}
					}
					break;
				}
			}
		}
	}	
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crFrameIteratorByType<_T>::crFrameIteratorByType(crFrame& frame)
: crFrameIterator<_T>(frame)
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crFrameIteratorByType<_T>::crFrameIteratorByType(const crFrame& frame)
: crFrameIterator<_T>(frame)
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void crFrameIteratorByType<_T>::IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float weight)
{
	_T& t = *(_T*)(this);

	switch(dof.m_Type)
	{
	case kFormatTypeVector3:
		t.template IterateDofByType<Vec3V>(dof, dest, weight);
		break;

	case kFormatTypeQuaternion:
		t.template IterateDofByType<QuatV>(dof, dest, weight);
		break;

	case kFormatTypeFloat:
		t.template IterateDofByType<float>(dof, dest, weight);
		break;

	default:
		Assert(0);
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void crFrameIteratorByType<_T>::IterateDofs(const crFrameData::Dof& dof, crFrame::Dof& dest, const crFrame::Dof& src, float weight)
{
	_T& t = *(_T*)(this);

	switch(dof.m_Type)
	{
	case kFormatTypeVector3:
		t.template IterateDofsByType<Vec3V>(dof, dest, src, weight);
		break;

	case kFormatTypeQuaternion:
		t.template IterateDofsByType<QuatV>(dof, dest, src, weight);
		break;

	case kFormatTypeFloat:
		t.template IterateDofsByType<float>(dof, dest, src, weight);
		break;

	default:
		Assert(0);
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void crFrameIteratorByType<_T>::IterateDofTrack(const crFrameData::Dof& dof, crFrame::Dof& dest, const crAnimTrack& track, float time, float weight)
{
	_T& t = *(_T*)(this);

	switch(dof.m_Type)
	{
	case kFormatTypeVector3:
		t.template IterateDofTrackByType<Vec3V>(dof, dest, track, time, weight);
		break;

	case kFormatTypeQuaternion:
		t.template IterateDofTrackByType<QuatV>(dof, dest, track, time, weight);
		break;

	case kFormatTypeFloat:
		t.template IterateDofTrackByType<float>(dof, dest, track, time, weight);
		break;

	default:
		Assert(0);
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void crFrameIteratorByType<_T>::IterateDofBone(const crFrameData::Dof& dof, crFrame::Dof& dest, int boneIdx, float weight)
{
	_T& t = *(_T*)(this);

	switch(dof.m_Type)
	{
	case kFormatTypeVector3:
		t.template IterateDofBoneByType<Vec3V>(dof, dest, boneIdx, weight);
		break;

	case kFormatTypeQuaternion:
		t.template IterateDofBoneByType<QuatV>(dof, dest, boneIdx, weight);
		break;

	case kFormatTypeFloat:
		t.template IterateDofBoneByType<float>(dof, dest, boneIdx, weight);
		break;

	default:
		Assert(0);
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_FRAMEITERATORS_H
