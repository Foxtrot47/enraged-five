//
// cranimation/channelvariablequantize.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "channelvariablequantize.h"

#if CR_DEV
#include "animarchive.h"
#include "animation.h"
#include "animtolerance.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelVariableQuantizeFloat, crAnimChannel::AnimChannelTypeVariableQuantizeFloat, crAnimTolerance::kCompressionCostHigh, crAnimTolerance::kDecompressionCostHigh);

////////////////////////////////////////////////////////////////////////////////

void crAnimChannelVariableQuantizeFloat::EvaluateFloat(float time, float& outFloat) const
{
	u32 timeQuotient = u32(time);
	float timeRemainder = time - float(timeQuotient);

	u32 address = 0;
	for(u32 i=0; i<(timeQuotient-1); ++i)
	{
		m_QuantizedValues.GetValue(address);
	}

	s32 u0 = m_QuantizedValues.GetValue(address);
	s32 u1 = m_QuantizedValues.GetValue(address);

	float f0 = float(u0)/float((GetNextPow2(abs(u0))>>1)-1);
	float f1 = float(u1)/float((GetNextPow2(abs(u1))>>1)-1);

	f0 *= m_Scale;
	f1 *= m_Scale;

	f0 += m_Offset;
	f1 += m_Offset;

	outFloat = Lerp(timeRemainder, f0, f1);
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelVariableQuantizeFloat::CreateFloat(float* floats, int num, int skip, float tolerance, float* tolerances)
{
	// quantizing is only worth it when trying to store more than 3 frames
	if(num < 3)
	{
		return false;
	}

	// can only quantize if tolerance is non-zero
	if(tolerance == 0.f)
	{
		return false;
	}

	const int stride = 1 + skip;

	// find range of floats
	float maxfloat = -FLT_MAX;
	float minfloat = FLT_MAX;

	float *fp = floats;
	for(int i=0; i<num; i++)
	{
		maxfloat = Max(maxfloat, *fp);
		minfloat = Min(minfloat, *fp);
		fp += stride;
	}

	// normalize into 0->1 range
	m_Offset = (minfloat + maxfloat)*0.5f;
	m_Scale = maxfloat - minfloat;
	const float invScale = 1.f/m_Scale;

	atArray<s32> quantizedValues;
	quantizedValues.Resize(num);

	fp = floats;
	for(int i=0; i<num; ++i)
	{
		float numValues = Max(ceil(m_Scale / tolerances[i]), 2.f);

		// can range of values be quantized within tolerance
		if(numValues >= float(0x80000000))
		{
			return false;
		}

		u32 numBits = 0;
		u32 values = u32(numValues)>>1;
		while(values > 0)
		{
			values >>= 1;
			++numBits;
		}

		Assert(numBits > 0);
		Assert(numBits < 32);

		float f = ((*fp - m_Offset) * invScale);
		u32 e = Clamp(u32(fabs(f)*numBits+0.5f), 0u, u32(0xffffffff>>(32-numBits)));
		e |= 1 << numBits;

		while(!(e&0x1))
		{
			e>>=1;
		}

		quantizedValues[i] = (f<0)?-s32(e):e;

//		Printf("[%2d] numbits %2d tolerance %f encoded %d\n", i, numBits, tolerances[i], quantizedValues[i]);

		fp += stride;
	}

	u32 lowestAddressMax;
	if(!m_QuantizedValues.InitAndCreate(quantizedValues, lowestAddressMax))
	{
		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

int crAnimChannelVariableQuantizeFloat::ComputeSize() const
{
	int size = sizeof(crAnimChannelVariableQuantizeFloat);
	size += m_QuantizedValues.ComputeSize();

	return size;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimChannelVariableQuantizeFloat::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	m_QuantizedValues.Serialize(a);
	a <<= m_Offset;
	a <<= m_Scale;
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CR_DEV
