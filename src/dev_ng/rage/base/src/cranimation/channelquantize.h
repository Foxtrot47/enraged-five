//
// cranimation/channelquantize.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELQUANTIZE_H
#define CRANIMATION_CHANNELQUANTIZE_H

#include "animchannel.h"

#if CR_DEV
namespace rage
{
	
class crAnimChannelQuantizeFloat : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelQuantizeFloat() : crAnimChannel(AnimChannelTypeQuantizeFloat), m_Scale(0.0f), m_Offset(0.0f) {}
	virtual ~crAnimChannelQuantizeFloat() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelQuantizeFloat);
	virtual void EvaluateFloat(float fTime, float& outFloat) const;
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);

	static bool QuantizeFloats(float* floats, int numFloats, int skip, float tolerance, atPackedArray& outQuantizedValues, float& outScale, float& outOffset);

private:
	float ReconstructFloat(int frame) const;
private:
	atPackedArray m_QuantizedValues;
	float m_Scale;
	float m_Offset;
};

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELQUANTIZE_H
