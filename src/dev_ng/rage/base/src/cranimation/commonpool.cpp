//
// cranimation/commonpool.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "commonpool.h"

#include "animation_config.h"

#include "profile/group.h"
#include "profile/page.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

#if CR_STATS
PF_PAGE(crCommonPoolPage, "cr CommonPool");
PF_GROUP(crCommonPoolStats);
PF_VALUE_INT(crCommonPool_PoolCount, crCommonPoolStats);
PF_VALUE_INT(crCommonPool_PoolCountPeak, crCommonPoolStats);
PF_VALUE_INT(crCommonPool_HeapSize, crCommonPoolStats);
PF_VALUE_INT(crCommonPool_HeapSizePeak, crCommonPoolStats);
PF_LINK(crCommonPoolPage, crCommonPoolStats);
#endif // CR_STATS

////////////////////////////////////////////////////////////////////////////////

crCommonPool::crCommonPool()
: m_Buffer(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crCommonPool::~crCommonPool()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCommonPool::Init(u32 maxNumElems, u32 bufferSize)
{
#if COMMERCE_CONTAINER
	m_Pool.Init((u16)maxNumElems, true);
#else
	m_Pool.Init((u16)maxNumElems);
#endif

	Assert(bufferSize > 0);
	Assert16(bufferSize);
	u8* buffer = rage_aligned_new(16) u8[bufferSize];
	m_Heap.Init(buffer, bufferSize);
	m_Buffer = buffer;
}

////////////////////////////////////////////////////////////////////////////////

void crCommonPool::Shutdown()
{
	delete [] m_Buffer;
	m_Buffer = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crCommonPool::Compact(u32 maxToCompact)
{
	m_Heap.Defragment(maxToCompact, &CompactCallback);
}

////////////////////////////////////////////////////////////////////////////////

void crCommonPool::CompactCallback(void* ptr, ptrdiff_t diff)
{
	// assume that the first member is the pointer to buffer
	Elem* elem = *reinterpret_cast<Elem**>(ptr);
	u8** buffer = reinterpret_cast<u8**>(elem);
	*buffer += diff;
}

////////////////////////////////////////////////////////////////////////////////

void* crCommonPool::Allocate(u8** outBuffer, u32 ASSERT_ONLY(elemSize), u32 bufferSize)
{
#if COMMON_POOL_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // COMMON_POOL_THREAD_SAFE

	Assert(elemSize <= sm_ElemSize);
	Elem* elem = m_Pool.New();
	if(!elem)
	{
		Quitf(ERR_ANI_POOL_1,"crCommonPool entries are exhausted, %" SIZETFMT "u entries used", m_Pool.GetSize());
	}

	u8* buffer = reinterpret_cast<u8*>(m_Heap.Allocate(bufferSize + sm_HeaderSize));
	if(!buffer)
	{
		Quitf(ERR_ANI_POOL_2,"crCommonPool can't allocate %u bytes", bufferSize);
	}

	// store back pointer to element, for compaction
	*reinterpret_cast<Elem**>(buffer) = elem;

	// skip header for the output buffer
	*outBuffer = buffer + sm_HeaderSize;

#if CR_STATS
	PF_SET(crCommonPool_PoolCount, u32(m_Pool.GetCount()));
	PF_SET(crCommonPool_PoolCountPeak, u32(m_Pool.GetPeakSlotsUsed()));
	PF_SET(crCommonPool_HeapSize, u32(m_Heap.GetHeapSize() - m_Heap.GetMemoryAvailable()) >> 10);
	PF_SET(crCommonPool_HeapSizePeak, u32(m_Heap.GetHeapSize() - m_Heap.GetLowWaterMark(false)) >> 10);
#endif // CR_STATS

	return elem;
}

////////////////////////////////////////////////////////////////////////////////

void crCommonPool::Release(void* elem, u8* buffer)
{
#if COMMON_POOL_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // COMMON_POOL_THREAD_SAFE

	m_Heap.Free(buffer - sm_HeaderSize);

	m_Pool.Delete(elem);
}

////////////////////////////////////////////////////////////////////////////////

u32 crCommonPool::GetSize() const
{
	return (u32)m_Pool.GetSize();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
