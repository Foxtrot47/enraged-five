//
// cranimation/animplayer.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "animplayer.h"

#include "frame.h"

#include "bank/bank.h"
#include "bank/slider.h"
#include "math/simplemath.h"

namespace rage
{

/*
PURPOSE
 	Default constructor.
 */
crAnimPlayer::crAnimPlayer()
: m_Flags(0)
, m_Time(0.f)
, m_Delta(0.f)
, m_DeltaSupplement(0.f)
, m_DeltaHiddenSupplement(0.f)
, m_Rate(1.f)
, m_Animation(NULL)
{
}

/*
PURPOSE
 	Constructor.
PARAMS
	anim - animation to play (or NULL for none).
	startTime - animation start time (in seconds).
	rate - playback rate (multiplier).
	ignoreAnimsLooped, isLooped - control looped behavior (see SetLooped for explanation)
 */
crAnimPlayer::crAnimPlayer(const crAnimation* anim, float startTime, float rate, bool ignoreAnimsLooped, bool isLooped)
: m_Flags(ignoreAnimsLooped ? (isLooped ? kLoopedAlways : kLoopedNever) : kLoopedAnimControlled)
, m_Time(startTime)
, m_Delta(0.f)
, m_DeltaSupplement(0.f)
, m_DeltaHiddenSupplement(0.f)
, m_Rate(rate)
, m_Animation(NULL)
{
	SetAnimation(anim);
}

/*
PURPOSE
 	Resource constructor.
 */
crAnimPlayer::crAnimPlayer(datResource&)
{
}

/*
PURPOSE
 	Destructor.
 */
crAnimPlayer::~crAnimPlayer()
{
	Shutdown();
}

IMPLEMENT_PLACE(crAnimPlayer);

#if __DECLARESTRUCT
/*
PURPOSE
 	Offline resource declaration.
 */
void crAnimPlayer::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crAnimPlayer);
	STRUCT_FIELD(m_Flags);
	STRUCT_FIELD(m_Time);
	STRUCT_FIELD(m_Delta);
	STRUCT_FIELD(m_DeltaSupplement);
	STRUCT_FIELD(m_DeltaHiddenSupplement);
	STRUCT_FIELD(m_Rate);
	STRUCT_FIELD_VP(m_Animation);
	STRUCT_IGNORE(m_AnimCallback);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

/*
PURPOSE
 	Shutdown, call to clear and release current animation.
 */
void crAnimPlayer::Shutdown()
{
	SetAnimation(NULL);
}

/*
PURPOSE
 	Updates time.
PARAMS
	deltaTime - amount of time to advance (in seconds). Must be >= 0.0.
	outUnusedTime - returns amount of unused time (in seconds).  Will be in range [0..deltaTime].
	outHasLoopedOrEnded - returns true if update causes animation to loop (or reach start/end if non-looping).
RETURNS
	amount of unused time (in seconds).  Will be >= 0.0 and <= deltaTime.
NOTES
	Advances current animation time, loops time if looped behavior indicated,
	otherwise clamps time to animation duration limits [0..duration] (and in the later case
	the delta time unused in reaching the start/end of the animation is passed back).
	Overwrites animation delta time with rage_new value.
 */
void crAnimPlayer::Update(float deltaTime, float& outUnusedTime, bool& outHasLoopedOrEnded)
{
	Assert(deltaTime >= 0.f);

	outUnusedTime = 0.f;
	outHasLoopedOrEnded = false;

	float internalDelta = (deltaTime * m_Rate) + m_DeltaSupplement;

	m_Delta = 0.f;
	m_DeltaSupplement = 0.f;

	if(!IsNearZero(internalDelta))
	{
		float prevTime = m_Time;
		m_Time += internalDelta;

		// valid animation
		if(m_Animation != NULL)
		{
			const float duration = m_Animation->GetDuration();

			// moving forward in animation
			if(internalDelta > 0.f)
			{
				// at or beyond end of animation
				if(m_Time >= duration)
				{
					float remainingTime = Min(internalDelta, m_Time - duration);
					if(IsLooped())
					{
						// if looped, use remaining time at start of animation (large deltas could wrap multiple times)
						m_Time = (remainingTime <= duration) ? remainingTime : fmodf(remainingTime, duration);
						m_Delta = internalDelta;

						outHasLoopedOrEnded = true;
					}
					else
					{
						// if not looped, clamp to end and return wasted time
						m_Time = duration;
						m_Delta = Max(internalDelta - remainingTime, 0.f);

						outHasLoopedOrEnded = (prevTime < duration);
						outUnusedTime = Min(remainingTime / m_Rate, deltaTime);
					}
				}
				else
				{
					m_Delta = internalDelta;
				}
			}
			else  // moving backwards in animation
			{
				// at or beyond start of animation
				if(m_Time <= 0.f)
				{
					float remainingTime = Max(internalDelta, m_Time);
					if(IsLooped())
					{
						// if looped, use remaining time at end of animation (large deltas could wrap multiple times)
						m_Time = (-remainingTime <= duration) ? (duration + remainingTime) : (duration - fmodf(-remainingTime, duration));
						m_Delta = internalDelta;

						outHasLoopedOrEnded = true;
					}
					else
					{
						// if not looped, clamp to start and return wasted time
						m_Time = 0.f;
						m_Delta = Min(internalDelta - remainingTime, 0.f);

						outHasLoopedOrEnded = (prevTime > 0.f);
						outUnusedTime = Min(remainingTime / m_Rate, deltaTime);
					}
				}
				else
				{
					m_Delta = internalDelta;
				}
			}
		}
	}

	m_Delta += m_DeltaHiddenSupplement;
	m_DeltaHiddenSupplement = 0.f;
}


/*
PURPOSE
 	Composite frame with animation at the current time.
PARAMS
	frame - frame to composite
	clear - clear missing dofs (default true)
RETURNS
	true - if composite successful.
 */
bool crAnimPlayer::Composite(crFrame& frame, bool UNUSED_PARAM(clear))
{
	if(m_Animation != NULL)
	{
		// TODO --- switch to frame composite calls
		if(m_Animation->HasMoverTracks())
		{
			// TODO --- CONSTANT mover id 0 - replace with calls that don't require mover ids
			frame.CompositeWithDelta(*m_Animation, m_Time, m_Delta);
		}
		else
		{
			frame.Composite(*m_Animation, m_Time);
		}
		return true;
	}

	return false;
}

/*
PURPOSE
 	Directly set the animation being played.
PARAMS
	anim - pointer to animation, or NULL for none.
NOTES
	Releases ref on existing animation (if not null) and
	adds ref on rage_new animation (if not null).
	If rage_new animation is valid, current animation time will be clamped to be
	within range [0..duration].
 */
void crAnimPlayer::SetAnimation(const crAnimation* anim)
{
	const crAnimation* oldAnim = m_Animation;
	if(anim != oldAnim)
	{
		if(anim != NULL)
		{
			anim->AddRef();
			m_Time = Clamp(m_Time, 0.f, anim->GetDuration());
		}

		m_Animation = anim;

		if(oldAnim != NULL)
		{
			oldAnim->Release();
		}

		if(m_AnimCallback)
		{
			m_AnimCallback();
		}
	}
}


#if __BANK
void crAnimPlayer::AddWidgets(bkBank& bk)
{
	bk.PushGroup("Simple Anim Player");
	bk.AddToggle("Ignore Anim Loop Flag", &m_Flags, BIT0);
	bk.AddToggle("Force Looping", &m_Flags, BIT1);
	bk.AddSlider("Time", &m_Time, 0.f, bkSlider::FLOAT_MAX_VALUE, 1.f/3000.f);
	bk.AddSlider("Delta", &m_Delta, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 1.f/3000.f);
	bk.AddSlider("Rate", &m_Rate, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.001f);
	bk.PopGroup();
}
#endif // __BANK

void crAnimPlayer::SetFunctor(const Functor0& functor)
{
	m_AnimCallback = functor;
}

} // namespace rage
