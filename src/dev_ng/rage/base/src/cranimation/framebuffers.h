//
// cranimation/framebuffers.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_FRAMEBUFFERS_H
#define CRANIMATION_FRAMEBUFFERS_H

#include "frame.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "atl/pool.h"
#include "creature/creature.h"
#include "system/criticalsection.h"

#define FRAME_BUFFERS_THREAD_SAFE (1)

namespace rage
{

class crCommonPool;
class crFrameFilter;
class crSkeletonData;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Defines interface for supplying a buffer of frames to the composer
class crFrameBuffer
{
public:

	// PURPOSE: Default constructor
	crFrameBuffer();

	// PURPOSE: Destructor
	virtual ~crFrameBuffer();

	// PURPOSE: Obtain a newly allocated frame from the buffer
	// RETURNS: pointer to new frame, or NULL if allocation failed
	// NOTES: If using motion tree scheduler and sharing frame buffers,
	// ensure any overridden implementation is thread safe
	virtual crFrame* AllocateFrame() = 0;

	// PURPOSE: Release a previously allocated frame back to the buffer
	// PARAMS: frame - pointer to the anim frame
	// NOTES: Only release frames allocated by this buffer.
	// Do not call release multiple times.
	// If using motion tree scheduler and sharing frame buffers,
	// ensure any overridden implementation is thread safe
	virtual void ReleaseFrame(crFrame* frame) = 0;

	// PURPOSE: Obtain the frame data used by the buffer
	virtual const crFrameData* GetFrameData() const = 0;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Implements simple interface for providing fixed array of
// frames to the composer
template<u32 N>
class crFrameBufferFixed : public crFrameBuffer
{
public:

	// PURPOSE:  Default constructor
	crFrameBufferFixed();

	// PURPOSE: Initializing constructor, build frames from skeleton (and optional mover)
	// PARAMS: skelData - reference to skeleton data to use to obtain frame dofs
	// hasMover - should mover dofs be constructed too
	// moverId - if mover dofs constructed, what id's are assigned (default 0)
	crFrameBufferFixed(const crSkeletonData& skelData, bool hasMover, u16 moverId=0);

	// PURPOSE: Initializing constructor, build frames from creature
	// PARAMS: creature - reference to creature to use to obtain frame dofs
	crFrameBufferFixed(crCreature& creature);

	// PURPOSE: Initializing constructor, build frames from example frame (and optional filter)
	// PARAMS: frame - example frame
	// filter - optional filter (NULL for none)
	crFrameBufferFixed(const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Initializing constructor, build frames from dof tracks, ids and types
	// PARAMS: numDofs - number of dofs to add
	// tracks - track indices (see crAnimTrack::TRACK_xxx)
	// ids - ids (meaning depends on track index)
	// types - dof type (see crAnimTrack::FORMAT_TYPE_xxx)
	crFrameBufferFixed(int numDofs, u8 tracks[], u16 ids[], u8 types[]);

	// PURPOSE: Destructor
	virtual ~crFrameBufferFixed();


	// PURPOSE: Initialize, build frames from skeleton (and optional mover)
	// PARAMS: skelData - reference to skeleton data to use to obtain frame dofs
	// hasMover - should mover dofs be constructed too
	// moverId - if mover dofs constructed, what id's are assigned (default 0)
	void Init(const crSkeletonData& skelData, bool hasMover, u16 moverId=0);

	// PURPOSE: Initialize, build frames from creature
	// PARAMS: creature - reference to creature to use to obtain frame dofs
	void Init(crCreature& creature);

	// PURPOSE: Initialize, build frames from dof tracks, ids and types
	// PARAMS: frame - example frame
	// filter - optional filter (NULL for none)
	void Init(const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Initializing constructor, build frames from dof tracks, ids and types
	// PARAMS: numDofs - number of dofs to add
	// tracks - track indices (see crAnimTrack::TRACK_xxx)
	// ids - ids (meaning depends on track index)
	// types - dof type (see crAnimTrack::FORMAT_TYPE_xxx)
	void Init(int numDofs, u8 tracks[], u16 ids[], u8 types[]);

	// PURPOSE: Reset the buffer
	// NOTES: Removes all the dofs from the frames, resets all the allocation tracking
	// Must call init again before using this frame buffer
	void Reset();


	// PURPOSE: Implement frame allocation call
	// NOTES: This is thread safe
	virtual crFrame* AllocateFrame();

	// PURPOSE: Implement frame release call
	// NOTES: This is thread safe
	virtual void ReleaseFrame(crFrame* frame);

	// PURPOSE: Implement get frame data
	virtual const crFrameData* GetFrameData() const;

protected:
#if FRAME_BUFFERS_THREAD_SAFE
	sysCriticalSectionToken m_CsToken;
#endif // FRAME_BUFFERS_THREAD_SAFE

	atRangeArray<crFrame, N> m_Frames;
	atFixedBitSet<N> m_Allocated;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Implements simple interface for providing lazy dynamic array of
// frames to the composer
class crFrameBufferDynamic : public crFrameBuffer
{
public:

	// PURPOSE:  Default constructor
	crFrameBufferDynamic();

	// PURPOSE: Initializing constructor, build frames from skeleton (and optional mover)
	// PARAMS: skelData - reference to skeleton data to use to obtain frame dofs
	// hasMover - should mover dofs be constructed too
	// moverId - if mover dofs constructed, what id's are assigned (default 0)
	crFrameBufferDynamic(const crSkeletonData& skelData, bool hasMover, u16 moverId=0);

	// PURPOSE: Initializing constructor, build frames from creature
	// PARAMS: creature - reference to creature to use to obtain frame dofs
	crFrameBufferDynamic(crCreature& creature);

	// PURPOSE: Initializing constructor, build frames from example frame (and optional filter)
	// PARAMS: frame - example frame
	// filter - optional filter (NULL for none)
	crFrameBufferDynamic(const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Initializing constructor, build frames from dof tracks, ids and types
	// PARAMS: numDofs - number of dofs to add
	// tracks - track indices (see crAnimTrack::TRACK_xxx)
	// ids - ids (meaning depends on track index)
	// types - dof type (see crAnimTrack::FORMAT_TYPE_xxx)
	crFrameBufferDynamic(int numDofs, u8 tracks[], u16 ids[], u8 types[]);

	// PURPOSE: Destructor
	virtual ~crFrameBufferDynamic();


	// PURPOSE: Initialize, build frames from skeleton (and optional mover)
	// PARAMS: skelData - reference to skeleton data to use to obtain frame dofs
	// hasMover - should mover dofs be constructed too
	// moverId - if mover dofs constructed, what id's are assigned (default 0)
	void Init(const crSkeletonData& skelData, bool hasMover, u16 moverId=0);

	// PURPOSE: Initialize, build frames from creature
	// PARAMS: creature - reference to creature to use to obtain frame dofs
	void Init(crCreature& creature);

	// PURPOSE: Initialize, build frames from dof tracks, ids and types
	// PARAMS: frame - example frame
	// filter - optional filter (NULL for none)
	void Init(const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Initializing constructor, build frames from dof tracks, ids and types
	// PARAMS: numDofs - number of dofs to add
	// tracks - track indices (see crAnimTrack::TRACK_xxx)
	// ids - ids (meaning depends on track index)
	// types - dof type (see crAnimTrack::FORMAT_TYPE_xxx)
	void Init(int numDofs, u8 tracks[], u16 ids[], u8 types[]);

	// PURPOSE: Reserve number of frames
	// PARAMS: numFrames - number of frames to reserve
	// NOTES: Dynamic buffer lazily allocates and initializes frames as demand
	// dictates.  Reserving helps performance by forcing an initial allocation of
	// frames, reducing or eliminating any later dynamic allocation.
	void Reserve(int numFrames);

	// PURPOSE: Reset the buffer
	// NOTES: Removes all the frames, resets all the allocation tracking
	// Must call init again before using this frame buffer
	void Reset();


	// PURPOSE: Implement frame allocation call
	// NOTES: This is thread safe
	virtual crFrame* AllocateFrame();

	// PURPOSE: Implement frame release call
	// NOTES: This is thread safe
	virtual void ReleaseFrame(crFrame* frame);

	// PURPOSE: Implement get frame data
	virtual const crFrameData* GetFrameData() const;

protected:

#if FRAME_BUFFERS_THREAD_SAFE
	sysCriticalSectionToken m_CsToken;
#endif // FRAME_BUFFERS_THREAD_SAFE

	struct Entry
	{
		crFrame m_Frame;
		Entry* m_Next;
	};
	Entry* m_Free;
	Entry* m_Allocated;
};

////////////////////////////////////////////////////////////////////////////////

class crFrameBufferFrameData : public crFrameBuffer
{
public:

	// PURPOSE: Default constructor
	crFrameBufferFrameData();

	// PURPOSE: Initializing constructor
	// PARAMS: pool - common pool for allocation, shared by all frame data frame buffers
	crFrameBufferFrameData(crCommonPool& pool);

	// PURPOSE: Destructor
	virtual ~crFrameBufferFrameData();

	// PURPOSE: Initialize
	// PARAMS: pool - common pool for allocation, shared by all frame data frame buffers
	void Init(crCommonPool& pool);

	// PURPOSE: Shutdown, release the frame data
	void Shutdown();

	// PURPOSE: Initializer
	// frameData - frame data describing structure of frames to be allocated by this buffer
	// NOTES: Can be called repeated, to change frame data used by this buffer over time
	void SetFrameData(const crFrameData& frameData);

	// PURPOSE: Implement frame allocation call
	// NOTES: This is thread safe
	virtual crFrame* AllocateFrame();

	// PURPOSE: Implement frame release call
	// NOTES: This is thread safe
	virtual void ReleaseFrame(crFrame* frame);

	// PURPOSE: Implement get frame data
	virtual const crFrameData* GetFrameData() const;

protected:

	crCommonPool* m_CommonPool;
	const crFrameData* m_FrameData;
};

////////////////////////////////////////////////////////////////////////////////

inline crFrameBuffer::crFrameBuffer()
{
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameBuffer::~crFrameBuffer()
{
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
crFrameBufferFixed<N>::crFrameBufferFixed()
: crFrameBuffer()
{
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
crFrameBufferFixed<N>::crFrameBufferFixed(const crSkeletonData& skelData, bool hasMover, u16 moverId)
: crFrameBuffer()
{
	Init(skelData, hasMover, moverId);
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
crFrameBufferFixed<N>::crFrameBufferFixed(crCreature& creature)
: crFrameBuffer()
{
	Init(creature);
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
crFrameBufferFixed<N>::crFrameBufferFixed(const crFrame& frame, crFrameFilter* filter)
: crFrameBuffer()
{
	Init(frame, filter);
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
crFrameBufferFixed<N>::crFrameBufferFixed(int numDofs, u8 tracks[], u16 ids[], u8 types[])
: crFrameBuffer()
{
	Init(numDofs, tracks, ids, types);
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
crFrameBufferFixed<N>::~crFrameBufferFixed()
{
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
void crFrameBufferFixed<N>::Init(const crSkeletonData& skelData, bool hasMover, u16 moverId)
{
	Reset();
	for(u32 i=0; i<N; ++i)
	{
		m_Frames[i].InitCreateBoneAndMoverDofs(skelData, hasMover, moverId);
	}
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
void crFrameBufferFixed<N>::Init(crCreature& creature)
{
	Reset();
	crFrameData* frameData = rage_new crFrameData;
	creature.InitDofs(*frameData);
	m_Frames[0].Init(*frameData);
	frameData->Release();
	for(u32 i=1; i<N; ++i)
	{
		m_Frames[i] = m_Frames[0];
	}
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
void crFrameBufferFixed<N>::Init(const crFrame& frame, crFrameFilter* filter)
{
	Reset();
	for(u32 i=0; i<N; ++i)
	{
		m_Frames[i].Init(frame, true, filter);
	}
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
void crFrameBufferFixed<N>::Init(int numDofs, u8 tracks[], u16 ids[], u8 types[])
{
	Reset();
	for(u32 i=0; i<N; ++i)
	{
		m_Frames[i].InitCreateDofs(numDofs, tracks, ids, types);
	}
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
void crFrameBufferFixed<N>::Reset()
{
	for(u32 i=0; i<N; ++i)
	{
		m_Frames[i].Shutdown();
	}
	m_Allocated.Reset();
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
crFrame* crFrameBufferFixed<N>::AllocateFrame()
{
#if FRAME_BUFFERS_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // FRAME_BUFFERS_THREAD_SAFE
	for(u32 i=0; i<N; ++i)
	{
		if(!m_Allocated.GetAndSet(i))
		{
			return &m_Frames[i];
		}
	}
	// failed to allocate, buffer exhausted
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
void crFrameBufferFixed<N>::ReleaseFrame(crFrame* frame)
{
#if FRAME_BUFFERS_THREAD_SAFE
	sysCriticalSection cs(m_CsToken);
#endif // FRAME_BUFFERS_THREAD_SAFE
	for(u32 i=0; i<N; ++i)
	{
		if(&m_Frames[i] == frame)
		{
			// if not allocated, indicates probable double free?
			FastAssert(m_Allocated.IsSet(i));
			m_Allocated.Clear(i);
			return;
		}
	}
	// failed to find frame, if not null then this wasn't allocated from this buffer?
	FastAssert(!frame);
}

////////////////////////////////////////////////////////////////////////////////

template<u32 N>
inline const crFrameData* crFrameBufferFixed<N>::GetFrameData() const
{
	return m_Frames[0]->GetFrameData();
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData* crFrameBufferFrameData::GetFrameData() const
{
	return m_FrameData;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_FRAMEBUFFERS_H
