// 
// rline/rltitlestorage.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rltitlestorage.h"
#include "rldiag.h"
#include "diag/seh.h"
#include "net/status.h"

#if __USE_TUS

#if RSG_NP
#include "rlnp.h"
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, titlestorage);
#undef __rage_channel
#define __rage_channel rline_titlestorage

///////////////////////////////////////////////////////////////////////////////
//  rlTitleStorage::WorkerInfo
///////////////////////////////////////////////////////////////////////////////
rlTitleStorage::WorkerInfo::WorkerInfo()
: m_Buf(0)
, m_DownloadSize(0)
, m_Status(0)
{
    Clear();
}

void
rlTitleStorage::WorkerInfo::Clear()
{
    if(m_Status && m_Status->Pending())
    {
        m_Status->SetFailed();
    }

    m_Buf = 0;
    m_BufSize = 0;
    m_DownloadSize = 0;
    m_Status = 0;
}

///////////////////////////////////////////////////////////////////////////////
//  rlTitleStorage::Worker
///////////////////////////////////////////////////////////////////////////////
rlTitleStorage::Worker::Worker()
: m_Ts(0)
{
}

bool
rlTitleStorage::Worker::Start(rlTitleStorage* ts, 
                              const int cpuAffinity)
{
    const bool success = this->rlWorker::Start("[RAGE] rlTitleStorage Worker",
                                                sysIpcMinThreadStackSize,
                                                cpuAffinity);
    if(success)
    {
        m_Ts = ts;
    }

    return success;
}

bool
rlTitleStorage::Worker::Stop()
{
    const bool success = this->rlWorker::Stop();
    m_Ts = 0;
    return success;
}

void
rlTitleStorage::Worker::Perform()
{
    SYS_CS_SYNC(m_Ts->m_Cs);  

    rlTitleStorage::WorkerInfo& info = m_Ts->m_WorkerInfo;

    info.m_Status->SetPending();

    if(NativePerform())
    {
        rlDebug("rlTitleStorage::DownloadFile: Downloaded %u bytes", *info.m_DownloadSize);
        info.m_Status->SetSucceeded();
    }
    else
    {
        rlDebug("rlTitleStorage::DownloadFile: Download failed");
        info.m_Status->SetFailed();
    }

    info.Clear();
}    

#if RSG_NP

bool
rlTitleStorage::Worker::NativePerform()
{
    rlError("This should not be called on RSG_NP");
    return false;
}

#else

bool
rlTitleStorage::Worker::NativePerform()
{
    rlError("Unsupported platform");
    return false;
}

#endif

///////////////////////////////////////////////////////////////////////////////
//  rlTitleStorage
///////////////////////////////////////////////////////////////////////////////

rlTitleStorage::rlTitleStorage()
: m_Initialized(false)
{
}

rlTitleStorage::~rlTitleStorage()
{
    Shutdown();
}

bool 
rlTitleStorage::Init(const int cpuAffinity)
{
    rlAssert(!m_Initialized);

    m_Initialized = rlVerify(m_Worker.Start(this, cpuAffinity));

    return m_Initialized;
}

void 
rlTitleStorage::Shutdown()
{
    if(m_Initialized)
    {
        m_Worker.Stop();
        m_Initialized = false;
    }
}

#if RSG_NP
bool
rlTitleStorage::DownloadFile(const rlGamerInfo& ASSERT_ONLY(gamerInfo),
                             const char* OUTPUT_ONLY(filename),
#else
bool
rlTitleStorage::DownloadFile(const rlGamerInfo& gamerInfo,
                             const char* filename,
#endif
                             char* buf,
                             const unsigned bufSize,
                             const unsigned timeoutMs,
                             unsigned* downloadSize,
                             netStatus* status)
{
    rlAssert(m_Initialized);
    rlAssert(gamerInfo.IsValid() && 
           gamerInfo.IsLocal() && 
           filename && 
           buf && 
           bufSize && 
           downloadSize && 
           status);

    bool success = false;

    if(rlVerify(!Pending()))
    {
        rlDebug("rlTitleStorage::DownloadFile(filename=%s)...", filename);

#if RSG_NP

        if(g_rlNp.GetLookup().DownloadTitleStorageFile(buf,
                                                        bufSize,
                                                        timeoutMs,
                                                        downloadSize,
                                                        status))
        {
            success = true;
        }
        else
        {
           rlError("Failed to start title storage download");
        }
#else

        SYS_CS_SYNC(m_Cs);

        m_WorkerInfo.m_GamerInfo    = gamerInfo;
        m_WorkerInfo.m_Filename     = filename;
        m_WorkerInfo.m_Buf          = buf;
        m_WorkerInfo.m_BufSize      = bufSize;
        m_WorkerInfo.m_TimeoutMs    = timeoutMs;
        m_WorkerInfo.m_DownloadSize = downloadSize;
        m_WorkerInfo.m_Status       = status;   

        m_Worker.Wakeup();

        success = true;

#endif  //RSG_NP
    }

    return success;
}

bool 
rlTitleStorage::Pending() const
{
    return m_Worker.IsPerforming();
}

}   //namespace rage

#endif //__USE_TUS