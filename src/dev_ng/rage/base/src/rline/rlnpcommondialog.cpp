// 
// rline/rlnpcommondialog.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_NP

#define LEGACY_SCE_ORBIS_SDK (SCE_ORBIS_SDK_VERSION < (0x07000000u))

#include <sdk_version.h>
#include "rlnpcommondialog.h"

#include "rline/rlpresence.h"
#include "rline/rltitleid.h"
#include "rline/rlnpwebapi.h"
#include "string/string.h"
#include "diag/seh.h"
#include "system/timer.h"
#include "system/service.h"

#include <libsysmodule.h>
#include <web_browser_dialog.h>
#include <np_commerce_dialog.h>
#if RSG_ORBIS
#include <np_profile_dialog.h>
#include <np_friendlist_dialog.h>
#include <game_custom_data_dialog.h>
#else
#include <player_invitation_dialog.h>
#include <player_selection_dialog.h>
#include <signin_dialog.h>
#include <player_review_dialog.h>
#endif
#include <message_dialog.h>
#include <system_service.h>


#if LEGACY_SCE_ORBIS_SDK == false
class SceSaveDataTitleId;
class SceSaveDataDirName;
#endif

#pragma comment(lib,"libSceCommonDialog_stub_weak.a")
#pragma comment(lib,"libSceSaveDataDialog_stub_weak.a")
#pragma comment(lib,"libSceWebBrowserDialog_stub_weak.a")
#pragma comment(lib,"libSceNpCommerce_stub_weak.a")
#pragma comment(lib,"libSceMsgDialog_stub_weak.a")
//RUFFGH There was an issue that magically went away linking this.
#if LEGACY_SCE_ORBIS_SDK
#pragma comment(lib,"libSceNpSns_stub_weak.a")
#endif
#if RL_FACEBOOK_ENABLED
#pragma comment(lib,"libSceNpSnsFacebookDialog_stub_weak.a")
#endif // RL_FACEBOOK_ENABLED
#if RSG_ORBIS
#pragma comment(lib,"libSceInvitationDialog_stub_weak.a")
#pragma comment(lib,"libSceNpProfileDialog_stub_weak.a")
#pragma comment(lib,"libSceNpFriendListDialog_stub_weak.a")
#pragma comment(lib,"libSceGameCustomDataDialog_stub_weak.a")
#pragma comment(lib,"libSceMsgDialog_stub_weak.a")
#else
#pragma comment(lib,"libScePlayerInvitationDialog_stub_weak.a")
#pragma comment(lib,"libScePlayerSelectionDialog_stub_weak.a")
#endif

#if RSG_PROSPERO
#pragma comment(lib,"libSceSigninDialog_stub_weak.a")
#pragma comment(lib,"libSceCdlgPlayerReview_stub_weak.a")
#endif

#define ENABLE_UNUSED_MODULES 0
#define NP_COMMON_DIALOG_MAX_WIDTH (1920)
#define NP_COMMON_DIALOG_MAX_HEIGHT (1080)

namespace rage
{
bool rlNpCommonDialog::ms_bLibsInitialized = false;

RAGE_DEFINE_SUBCHANNEL(rline, npcommondialog);
#undef __rage_channel
#define __rage_channel rline_npcommondialog

extern const rlTitleId* g_rlTitleId;
extern SceUserServiceUserId g_initialUserId;

PARAM(netDisableBrowserSafeRegion, "Disables safe region for web browser on PlayStation.");

#if RL_NP_DEEP_LINK_MANUAL_CLOSE
unsigned rlNpCommonDialog::ms_BrowserDeepLinkCloseTime = 5000;
void rlNpCommonDialog::SetBrowserDeepLinkCloseTime(const unsigned browserDeepLinkCloseTime)
{
    if(ms_BrowserDeepLinkCloseTime != browserDeepLinkCloseTime)
    {
        rlDebug("SetBrowserDeepLinkCloseTime :: %ums -> %ums", ms_BrowserDeepLinkCloseTime, browserDeepLinkCloseTime);
        ms_BrowserDeepLinkCloseTime = browserDeepLinkCloseTime;
    }
}
#endif

bool rlNpCommonDialog::ms_FailWhenInitialiseFails = false;
void rlNpCommonDialog::SetFailWhenInitialiseFails(const bool bFailWhenInitialiseFails)
{
    if(ms_FailWhenInitialiseFails != bFailWhenInitialiseFails)
    {
        rlDebug("SetFailWhenInitialiseFails :: %s -> %s", ms_FailWhenInitialiseFails ? "True" : "False", bFailWhenInitialiseFails ? "True" : "False");
        ms_FailWhenInitialiseFails = bFailWhenInitialiseFails;
    }
}

rlNpCommonDialog::rlNpCommonDialog() 
	: m_CurrentDialogType(DIALOG_NONE)
	, m_bDialogShowing(false)
	, m_CurrentDialogStatus(SCE_COMMON_DIALOG_STATUS_NONE)
	, m_CommerceMode(SCE_NP_COMMERCE_DIALOG_MODE_CATEGORY)
	, m_QueuedId(-1)
	, m_UgcMsgQueued(false)
	, m_ChatMsgQueued(false)
	, m_UserId(SCE_USER_SERVICE_USER_ID_INVALID)
    , m_ScreenWidth(NP_COMMON_DIALOG_MAX_WIDTH) // Default - Game will fill out new values after initializing the device.
    , m_ScreenHeight(NP_COMMON_DIALOG_MAX_HEIGHT) // Default - Game will fill out new values after initializing the device.
    , m_BrowserTitleHeight(DEFAULT_BROWSER_TITLE_HEIGHT)
    , m_BrowserAddressHeight(DEFAULT_BROWSER_ADDR_HEIGHT)
    , m_BrowserFooterHeight(DEFAULT_BROWSER_FOOTER_HEIGHT)
	, m_DeepLinkUrlState(DeepLinkUrlState::DLS_Inactive)
#if RL_NP_DEEP_LINK_MANUAL_CLOSE
	, m_DeepLinkStateStartedTime(0)
	, m_DeepLinkCloseTime(0)
#endif
{

}

rlNpCommonDialog::~rlNpCommonDialog()
{
	Shutdown();
}

void rlNpCommonDialog::InitLibs()
{
    rlDebug("InitLibs");

	rtry
	{
		int ret = SCE_OK;
		ret = sceCommonDialogInitialize();
		rverify(ret == SCE_OK, catchall, rlError("Common Dialog could not be initialized. ret = 0x%x\n", ret));

#if RSG_ORBIS
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_NP_PROFILE_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_NP_PROFILE_DIALOG) failed. ret = 0x%x\n", ret));
#else
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_PLAYER_SELECTION_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_PLAYER_SELECTION_DIALOG) failed. ret = 0x%x\n", ret));
#endif

		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_WEB_BROWSER_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_WEB_BROWSER_DIALOG) failed. ret = 0x%x\n", ret));

#if RSG_ORBIS
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_INVITATION_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_INVITATION_DIALOG) failed. ret = 0x%x\n", ret));
#else
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_PLAYER_INVITATION_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_PLAYER_INVITATION_DIALOG) failed. ret = 0x%x\n", ret));
#endif

		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_NP_COMMERCE);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_NP_COMMERCE) failed. ret = 0x%x\n", ret));

#if RL_FACEBOOK_ENABLED
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_NP_SNS_FACEBOOK);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_NP_SNS_FACEBOOK) failed. ret = 0x%x\n", ret));
#endif // RL_FACEBOOK_ENABLED

		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_SAVE_DATA_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_SAVE_DATA_DIALOG) failed. ret = 0x%x\n", ret));

		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_MESSAGE_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_MESSAGE_DIALOG) failed. ret = 0x%x\n", ret));

#if ENABLE_UNUSED_MODULES
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_NP_FRIEND_LIST_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_NP_FRIEND_LIST_DIALOG) failed. ret = 0x%x\n", ret));

		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_GAME_CUSTOM_DATA_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_GAME_CUSTOM_DATA_DIALOG) failed. ret = 0x%x\n", ret));
#endif

#if RSG_PROSPERO
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_SIGNIN_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_SIGNIN_DIALOG) failed. ret = 0x%x\n", ret));

		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_PLAYER_REVIEW_DIALOG);
		rverify(ret == SCE_OK, catchall, rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_PLAYER_REVIEW_DIALOG) failed. ret = 0x%x\n", ret));
#endif

	}
	rcatchall
	{
		ms_bLibsInitialized = false;
	}
	ms_bLibsInitialized = true;
}

bool rlNpCommonDialog::Init()
{
	m_CurrentDialogStatus = SCE_COMMON_DIALOG_STATUS_NONE;
    if(!ms_bLibsInitialized)
    {
        rlDebug("Init :: Libs not initialised!");
        return false;
    }

    rlDebug("Init");

    // preload browser dialog as that's needed early for PS+ promotion
	PreloadBrowserDialog();
		
	return true;
}

void rlNpCommonDialog::Shutdown()
{
    rlDebug("Shutdown");

	if(!ms_bLibsInitialized)
		return;

	int ret = SCE_OK;

	ret = ShutdownCurrentDialog();
	if(ret < SCE_OK)
	{
		rlError("Dialog Terminate for type %d failed : 0x%08x\n", m_CurrentDialogType, ret);
	}

#if RSG_PROSPERO
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_SIGNIN_DIALOG);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_SIGNIN_DIALOG) failed. ret = 0x%x\n", ret);
	}

	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_PLAYER_REVIEW_DIALOG);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_PLAYER_REVIEW_DIALOG) failed. ret = 0x%x\n", ret);
	}
#endif

	// Unload the Profile Dialog
#if RSG_ORBIS
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_PROFILE_DIALOG) ;
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_PROFILE_DIALOG) failed. ret = 0x%x\n", ret);
	}
#else
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_PLAYER_SELECTION_DIALOG);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_PLAYER_SELECTION_DIALOG) failed. ret = 0x%x\n", ret);
	}
#endif

	// Unload the Web Browser Dialog
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_WEB_BROWSER_DIALOG) ;
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_WEB_BROWSER_DIALOG) failed. ret = 0x%x\n", ret);
	}

	// Unload the Invitation Dialog
#if RSG_ORBIS
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_INVITATION_DIALOG);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_INVITATION_DIALOG) failed. ret = 0x%x\n", ret);
	}
#else
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_PLAYER_INVITATION_DIALOG);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_PLAYER_INVITATION_DIALOG) failed. ret = 0x%x\n", ret);
	}
#endif

	// Unload the Commerce Dialog
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_COMMERCE);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_COMMERCE) failed. ret = 0x%x\n", ret);
	}

#if RL_FACEBOOK_ENABLED
	// Unload the Facebook Dialog
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_SNS_FACEBOOK);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_SNS_FACEBOOK) failed. ret = 0x%x\n", ret);
	}
#endif // RL_FACEBOOK_ENABLED

	// Unload the SaveData Dialog
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_SAVE_DATA_DIALOG) ;
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_SAVE_DATA_DIALOG) failed. ret = 0x%x\n", ret);
	}

	// Unload the MsgDialog Dialog
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_MESSAGE_DIALOG) ;
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_MESSAGE_DIALOG) failed. ret = 0x%x\n", ret);
	}

#if ENABLE_UNUSED_MODULES
	// Unload the Friends List Dialog
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_FRIEND_LIST_DIALOG);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_FRIEND_LIST_DIALOG) failed. ret = 0x%x\n", ret);
	}

	// Unload the Game Custom Data Dialog
	ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_GAME_CUSTOM_DATA_DIALOG);
	if(ret != SCE_OK)
	{
		rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_GAME_CUSTOM_DATA_DIALOG) failed. ret = 0x%x\n", ret);
	}
#endif

	ms_bLibsInitialized = false;
}

void rlNpCommonDialog::Update()
{
    // Only one dialog can be open at a time
	switch(m_CurrentDialogType)
	{
	case DIALOG_NONE:
		break;
	case DIALOG_MSG:
		UpdateMessageDialog();
		break;
	case DIALOG_COMMERCE:
		UpdateCommerceDialog();
		break;
	case DIALOG_PROFILE:
		UpdateProfileDialog();
		break;
	case DIALOG_SAVEDATA:
		UpdateSaveDataDialog();
		break;
	case DIALOG_BROWSER:
        UpdateBrowserDialog();
		break;
	case DIALOG_INVITATION:
		UpdateInvitationDialog();
		break;
#if RL_FACEBOOK_ENABLED
	case DIALOG_FACEBOOK:
		UpdateFacebookDialog();
		break;
#endif // RL_FACEBOOK_ENABLED
	case DIALOG_FRIENDS:
		UpdateFriendsDialog();
		break;
#if RSG_ORBIS
	case DIALOG_CUSTOM_GAMEDATA:
		UpdateGameCustomDataDialog();
		break;
#endif
#if RSG_PROSPERO
	case DIALOG_SIGNIN:
		UpdateSigninDialog();
		break;
	case DIALOG_PLAYER_REVIEW:
		UpdatePlayerReviewDialog();
		break;
#endif
	default:
		break;
	}

	// Message Queue
	if(!m_bDialogShowing)
	{
#if RSG_ORBIS
		if(m_UgcMsgQueued)
		{
			m_UgcMsgQueued = false;
			ShowUGCRestrictionMsg(m_QueuedId);
		}
		else
#endif
		if(m_ChatMsgQueued)
		{
			m_ChatMsgQueued = false;
			ShowChatRestrictionMsg(m_QueuedId);
		}
	}
}

void rlNpCommonDialog::UpdateMessageDialog()
{
	if(m_bDialogShowing)
	{
		m_CurrentDialogStatus = sceMsgDialogUpdateStatus();
		if( m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED )
		{
            rlDebug("UpdateMessageDialog :: Finished");

			m_bDialogShowing = false;

			SceMsgDialogResult result;
			memset( &result, 0, sizeof(result) );
			int ret = sceMsgDialogGetResult(&result);
			if(ret < SCE_OK)
			{
				rlError("sceMsgDialogGetResult failed. ret = 0x%x\n", ret);
				return;
			}
		}
	}
}

void rlNpCommonDialog::UpdateCommerceDialog()
{
	if(m_bDialogShowing)
	{
		m_CurrentDialogStatus = sceNpCommerceDialogUpdateStatus();
		if( m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED )
		{
            rlDebug("UpdateCommerceDialog :: Finished");
                
            m_bDialogShowing = false;
			//ShutdownCurrentDialog();

			sysMemSet(&m_CommerceResult, 0, sizeof(m_CommerceResult));

			int ret = sceNpCommerceDialogGetResult(&m_CommerceResult);
			if(ret < SCE_OK)
			{
				rlError("sceNpCommerceDialogGetResult failed. ret = 0x%x\n", ret);
				return;
			}

#if RSG_ORBIS
			if(m_CommerceMode == SCE_NP_COMMERCE_DIALOG_MODE_PLUS)
			{
				// Notify NpAuth that a PSPlus Commerce Window was finished
				g_rlNp.GetNpAuth().NpPlusEventCallback(m_UserId, SCE_NP_PLUS_EVENT_RECHECK_NEEDED, &g_rlNp);
#else
			if(m_CommerceMode == SCE_NP_COMMERCE_DIALOG_MODE_PREMIUM)
			{
				g_rlNp.GetNpAuth().NpPremiumEventCallback(m_UserId, SCE_NP_PREMIUM_EVENT_RECHECK_NEEDED, &g_rlNp);
#endif// !RSG_PROSPERO
				m_UserId = SCE_USER_SERVICE_USER_ID_INVALID;
			}
		}
	}
}

void rlNpCommonDialog::UpdateProfileDialog()
{
	if(m_bDialogShowing)
	{
#if RSG_ORBIS
		m_CurrentDialogStatus = sceNpProfileDialogUpdateStatus();
		if(m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED)
		{
            rlDebug("UpdateProfileDialog :: Finished");
                
            m_bDialogShowing = false;
			//ShutdownCurrentDialog();
		}
#else // RSG_PROSPERO
		SceSystemServiceStatus status;
		int ret = sceSystemServiceGetStatus(&status);
		if(ret == SCE_OK)
		{
			// Set flag if system UI has actually been shown 
			// (there's a delay between call to show and isSystemUiOverlaid flag being set) 
			if(m_bSystemUiShown == false)
			{
				if(status.isSystemUiOverlaid == true)
				{
					m_bSystemUiShown = true;
				}
			}
			else
			if(status.isSystemUiOverlaid == false)
			{
				m_bDialogShowing = false;
				m_bSystemUiShown = false;
				m_CurrentDialogType = DIALOG_NONE;
			}
		}
#endif
	}
}

void rlNpCommonDialog::UpdateSaveDataDialog()
{
	if(m_bDialogShowing)
	{
		m_CurrentDialogStatus = sceSaveDataDialogUpdateStatus();
		if(m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED)
		{
            rlDebug("UpdateSaveDataDialog :: Finished");
                
            m_bDialogShowing = false;
			//ShutdownCurrentDialog();

			sysMemSet(&m_SaveDataResult, 0, sizeof(m_SaveDataResult));

			int ret = sceSaveDataDialogGetResult(&m_SaveDataResult);
			if(ret < SCE_OK)
			{
				rlError("sceSaveDataDialogGetResult failed. ret = 0x%x\n", ret);
				return;
			}
		}
	}
}

void rlNpCommonDialog::UpdateBrowserDialog()
{
	if(m_bDialogShowing)
	{
		m_CurrentDialogStatus = sceWebBrowserDialogUpdateStatus();
		if(m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED)
		{
			rlDebug("UpdateBrowserDialog :: Finished");

#if RSG_PROSPERO
			SceWebBrowserDialogCallbackResultParam callbackResult;
			sysMemSet(&callbackResult, 0, sizeof(callbackResult));
			callbackResult.size = sizeof(callbackResult);

			SceWebBrowserDialogResult result;
			sysMemSet(&result, 0, sizeof(result));
			result.callbackResultParam = &callbackResult;

			int ret = sceWebBrowserDialogGetResult(&result);
			if(ret == SCE_COMMON_DIALOG_ERROR_PARAM_INVALID)
			{
				rlDebug1("UpdateBrowserDialog :: sceWebBrowserDialogGetResult invalid param, allocating extended url");
				if(rlVerify(callbackResult.bufferSize <= SCE_WEB_BROWSER_DIALOG_URL_SIZE_EXTENDED))
				{
					char* buffer = (char*)Alloca(char, callbackResult.bufferSize);
					if(rlVerify(buffer))
					{
						sysMemSet(buffer, 0, callbackResult.bufferSize);
						callbackResult.buffer = buffer;
						ret = sceWebBrowserDialogGetResult(&result);
					}
				}
			}

			if(rlVerifyf(ret == SCE_OK, "UpdateBrowserDialog :: sceWebBrowserDialogGetResult failed, ret = 0x%x", ret))
			{
				rlDebug1("UpdateBrowserDialog :: Closed with URL: %s", callbackResult.data);

				rlNpEventBrowserCallbackUrl evt(callbackResult.data);
				g_rlNp.DispatchEvent(&evt);
			}
#endif

			m_bDialogShowing = false;
			//ShutdownCurrentDialog();
		}
	}

	switch(m_DeepLinkUrlState)
	{
	case DeepLinkUrlState::DLS_Inactive:
		break;

	case DeepLinkUrlState::DLS_Opening:
		if(m_bDialogShowing && g_SysService.IsUiOverlaid())
		{
			// once the UI is up, move to the DLS_Open state to manage closing the dummy browser
			rlDebug1("UpdateBrowserDialog :: Deep Link - Url Open");
			m_DeepLinkUrlState = DeepLinkUrlState::DLS_Open;
			RL_NP_DEEP_LINK_MANUAL_CLOSE_ONLY(m_DeepLinkStateStartedTime = sysTimer::GetSystemMsTime());
		}
		break;

	case DeepLinkUrlState::DLS_Open:
		if(m_bDialogShowing && 
		  (!g_SysService.IsUiOverlaid() RL_NP_DEEP_LINK_MANUAL_CLOSE_ONLY(|| ((sysTimer::GetSystemMsTime() - m_DeepLinkStateStartedTime) > m_DeepLinkCloseTime))))
		{
			rlDebug("UpdateBrowserDialog :: Deep Link - Browser Closed");
			m_DeepLinkUrlState = DeepLinkUrlState::DLS_Closed;
			RL_NP_DEEP_LINK_MANUAL_CLOSE_ONLY(m_DeepLinkStateStartedTime = sysTimer::GetSystemMsTime());

			// we need to close the browser behind our deep link
			int ret = sceWebBrowserDialogClose();
			if(ret != SCE_OK)
			{
				rlError("UpdateBrowserDialog :: sceWebBrowserDialogClose failed with code: 0x%08x", ret);
			}
		}
		break;

	case DeepLinkUrlState::DLS_Closed:
		if(!g_SysService.IsInBackgroundExecution())
		{
			rlDebug("UpdateBrowserDialog :: Deep Link - Complete");
			m_DeepLinkUrlState = DeepLinkUrlState::DLS_Inactive;

			// we always need to shutdown when using deep link
			ShutdownCurrentDialog();

			m_bDialogShowing = false;
			m_CurrentDialogType = DIALOG_NONE;

			// check if we require a PS+ check
			if((m_BrowserConfig.m_Flags & rlSystemBrowserConfig::FLAG_PLATFORM_SUBSCRIPTION_CHECK) != 0)
			{
				rlDebug("UpdateBrowserDialog :: Deep Link - Issuing PS+ check");

				// Notify NpAuth that a PSPlus Commerce Window was finished
				int localGamerIndex = rlPresence::GetActingUserIndex();
				if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
				{
					localGamerIndex = 0;
				}
				int userId = g_rlNp.GetUserServiceId(localGamerIndex);
#if RSG_ORBIS
                g_rlNp.GetNpAuth().NpPlusEventCallback(userId, SCE_NP_PLUS_EVENT_RECHECK_NEEDED, &g_rlNp);
#else
                g_rlNp.GetNpAuth().NpPremiumEventCallback(userId, SCE_NP_PREMIUM_EVENT_RECHECK_NEEDED, &g_rlNp);
#endif
			}
		}
		break;
	}
}

void rlNpCommonDialog::UpdateInvitationDialog()
{
	if(m_bDialogShowing)
	{
#if RSG_ORBIS
		m_CurrentDialogStatus = sceInvitationDialogUpdateStatus();
#else
		m_CurrentDialogStatus = scePlayerInvitationDialogUpdateStatus();
#endif
		if( m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED )
		{
            rlDebug("UpdateInvitationDialog :: Finished");

            m_bDialogShowing = false;
			//ShutdownCurrentDialog();
		}
	}
}

#if RL_FACEBOOK_ENABLED

void rlNpCommonDialog::UpdateFacebookDialog()
{
#if LEGACY_SCE_ORBIS_SDK
	//Not touching this as it's not on by default.
	if(m_bDialogShowing)
	{
		SceCommonDialogStatus status = sceNpSnsFacebookDialogUpdateStatus();
		if( status == SCE_COMMON_DIALOG_STATUS_FINISHED )
		{
            rlDebug("UpdateFacebookDialog :: Finished");

            m_bDialogShowing = false;

			memset(&m_FacebookResult, 0, sizeof(m_FacebookResult));

			int ret = sceNpSnsFacebookDialogGetResult(&m_FacebookResult);
			if(ret != SCE_OK)
			{	
				rlError("Error in sceNpSnsFacebookDialogGetResult. ret = 0x%x\n", ret);
			}

			// writing status after getting result for thread safety
			m_CurrentDialogStatus = status;

			ret = sceNpSnsFacebookDialogTerminate();
			if(ret != SCE_OK)
			{	
				rlError("Error in sceNpSnsFacebookDialogTerminate. ret = 0x%x\n", ret);
			}

			m_CurrentDialogType = DIALOG_NONE;
		}
	}
#endif
}
#endif // RL_FACEBOOK_ENABLED

void rlNpCommonDialog::UpdateFriendsDialog()
{
#if RSG_ORBIS
	if(m_bDialogShowing)
	{
		m_CurrentDialogStatus = sceNpFriendListDialogUpdateStatus();
		if(m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED)
		{
            rlDebug("UpdateFriendsDialog :: Finished");
            m_bDialogShowing = false;
		}
	}
#endif
}

#if RSG_ORBIS
void rlNpCommonDialog::UpdateGameCustomDataDialog()
{
	if(m_bDialogShowing)
	{
		m_CurrentDialogStatus = sceGameCustomDataDialogUpdateStatus();
		if(m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED)
		{
            rlDebug("UpdateGameCustomDataDialog :: Finished");
            m_bDialogShowing = false;
			//ShutdownCurrentDialog();
		}
	}
}
#endif

#if RSG_PROSPERO
void rlNpCommonDialog::UpdateSigninDialog()
{
	if(m_bDialogShowing)
	{
		SceSigninDialogStatus status = sceSigninDialogUpdateStatus();
		switch (status)
		{
		case  SCE_SIGNIN_DIALOG_STATUS_NONE:
			m_CurrentDialogStatus = SCE_COMMON_DIALOG_STATUS_NONE;
			break;
		case SCE_SIGNIN_DIALOG_STATUS_INITIALIZED:
			m_CurrentDialogStatus = SCE_COMMON_DIALOG_STATUS_INITIALIZED;
			break;
		case SCE_SIGNIN_DIALOG_STATUS_RUNNING:
			m_CurrentDialogStatus = SCE_COMMON_DIALOG_STATUS_RUNNING;
			break;
		case SCE_SIGNIN_DIALOG_STATUS_FINISHED:
			rlDebug("UpdateSigninDialog :: Finished");
			m_CurrentDialogStatus = SCE_COMMON_DIALOG_STATUS_FINISHED;
			m_bDialogShowing = false;
			break;
		}
	}
}

void rlNpCommonDialog::UpdatePlayerReviewDialog()
{
	if(m_bDialogShowing)
	{
		m_CurrentDialogStatus = scePlayerReviewDialogUpdateStatus();

		if(m_CurrentDialogStatus == SCE_COMMON_DIALOG_STATUS_FINISHED)
		{
			rlDebug("UpdatePlayerReviewDialog :: Finished");

			m_bDialogShowing = false;

			ScePlayerReviewDialogResult result;
			memset(&result, 0, sizeof(result));

			int ret = scePlayerReviewDialogGetResult(&result);

			if(ret < SCE_OK)
			{
				rlError("scePlayerReviewDialogGetResult failed. ret = 0x%x\n", ret);
				return;
			}
		}
	}
}
#endif //RSG_PROSPERO

bool rlNpCommonDialog::PreloadBrowserDialog()
{
    if(m_bDialogShowing)
    {
        rlDebug("PreloadBrowserDialog :: %d is currently showing!", m_CurrentDialogType);
        return false;
    }

    if(m_CurrentDialogType == DIALOG_BROWSER)
    {
        rlDebug("PreloadBrowserDialog :: Browser is current dialog");
        return true;
    }

    rlDebug("PreloadBrowserDialog");

    if(PrepareDialog(DIALOG_BROWSER) == SCE_OK)
    {
        m_CurrentDialogType = DIALOG_BROWSER;
        return true; 
    }

    return false;
}

int rlNpCommonDialog::PrepareDialog(DialogType desiredDialogType)
{
    rlDebug("PrepareDialog :: Dialog: %d", desiredDialogType);

	if(m_bDialogShowing)
	{
		rlError("PrepareDialog :: Dialog %d currently showing!", m_CurrentDialogType);
		return -1;
	}
	else if(m_CurrentDialogType != desiredDialogType)
	{
		int ret = ShutdownCurrentDialog();
		if(ret != SCE_OK)
		{
			rlError("Error shutting down dialog %d. ret = 0x%x\n", m_CurrentDialogType, ret);
		}

		ret = InitializeDialog(desiredDialogType);
		if(ret != SCE_OK)
		{
			rlError("Error initializing dialog %d. ret = 0x%x\n", desiredDialogType, ret);
            if(ms_FailWhenInitialiseFails)
            {
                return ret;
            }
		}
	}

	return SCE_OK;
}

int rlNpCommonDialog::InitializeDialog(DialogType desiredDialogType)
{
    rlDebug("InitializeDialog :: Dialog: %d", desiredDialogType);
        
    int ret = SCE_OK;

	switch(desiredDialogType)
	{
	case DIALOG_NONE:
		break;
	case DIALOG_MSG:
		ret = sceMsgDialogInitialize();
		break;
	case DIALOG_COMMERCE:
		ret = sceNpCommerceDialogInitialize();
		break;
	case DIALOG_PROFILE:
#if RSG_ORBIS
		ret = sceNpProfileDialogInitialize();
#endif
		break;
	case DIALOG_SAVEDATA:
		ret = sceSaveDataDialogInitialize();
		break;
	case DIALOG_BROWSER:
		ret = sceWebBrowserDialogInitialize();
		break;
	case DIALOG_INVITATION:
#if RSG_ORBIS
		ret = sceInvitationDialogInit();
#else
		ret = scePlayerInvitationDialogInitialize();
#endif
		break;
#if RL_FACEBOOK_ENABLED
	case DIALOG_FACEBOOK:
		ret = sceNpSnsFacebookDialogInitialize();
		break;
#endif // RL_FACEBOOK_ENABLED
	case DIALOG_FRIENDS:
#if RSG_ORBIS
		ret = sceNpFriendListDialogInitialize();
#else // RSG_PROSPERO
		ret = scePlayerSelectionDialogInitialize();
#endif
		break;
#if RSG_ORBIS
	case DIALOG_CUSTOM_GAMEDATA:
		ret = sceGameCustomDataDialogInitialize();
		break;
#endif
#if RSG_PROSPERO
	case DIALOG_SIGNIN:
		ret = sceSigninDialogInitialize();
		break;
	case DIALOG_PLAYER_REVIEW:
		ret = scePlayerReviewDialogInitialize();
		break;
#endif
	default:
		break;
	}

	return ret;
}

int rlNpCommonDialog::ShutdownCurrentDialog()
{
    rlDebug("ShutdownCurrentDialog :: Dialog: %d", m_CurrentDialogType);
        
    int ret = SCE_OK;

	switch(m_CurrentDialogType)
	{
	case DIALOG_NONE:
		break;
	case DIALOG_MSG:
		ret = sceMsgDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceMsgDialogTerminate failed, ret=0x%x", ret);
		break;
	case DIALOG_COMMERCE:
		ret = sceNpCommerceDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceNpCommerceDialogTerminate failed, ret=0x%x", ret);
		break;
	case DIALOG_PROFILE:
#if RSG_ORBIS
		ret = sceNpProfileDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceNpProfileDialogTerminate failed, ret=0x%x", ret);
#endif
		break;
	case DIALOG_SAVEDATA:
		ret = sceSaveDataDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceSaveDataDialogTerminate failed, ret=0x%x", ret);
		break;
	case DIALOG_BROWSER:
		ret = sceWebBrowserDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceWebBrowserDialogTerminate failed, ret=0x%x", ret);
		break;
	case DIALOG_INVITATION:
#if RSG_ORBIS
		ret = sceInvitationDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceInvitationDialogTerminate failed, ret=0x%x", ret);
#else
		ret = scePlayerInvitationDialogTerminate();
		rlAssertf(ret == SCE_OK, "scePlayerInvitationDialogTerminate failed, ret=0x%x", ret);
#endif
		break;
#if RL_FACEBOOK_ENABLED
	case DIALOG_FACEBOOK:
		ret = sceNpSnsFacebookDialogTerminate();
		break;
#endif // RL_FACEBOOK_ENABLED
	case DIALOG_FRIENDS:
#if RSG_ORBIS
		ret = sceNpFriendListDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceNpFriendListDialogTerminate failed, ret=0x%x", ret);
#else
		ret = scePlayerSelectionDialogTerminate();
		rlAssertf(ret == SCE_OK, "scePlayerSelectionDialogTerminate failed, ret=0x%x", ret);
#endif
		break;
#if RSG_ORBIS
	case DIALOG_CUSTOM_GAMEDATA:
		ret = sceGameCustomDataDialogTerminate();
		rlAssertf(ret == SCE_OK, "sceGameCustomDataDialogTerminate failed, ret=0x%x", ret);
		break;
#endif
#if RSG_PROSPERO
	case DIALOG_SIGNIN:
		ret = sceSigninDialogTerminate();
		break;
	case DIALOG_PLAYER_REVIEW:
		ret = scePlayerReviewDialogTerminate();
		break;
#endif
	default:
		break;
	}

	m_CurrentDialogType = DIALOG_NONE;
	m_bDialogShowing = false;

	return ret;
}

#if RSG_ORBIS
bool rlNpCommonDialog::ShowProfile(SceUserServiceUserId userId, const rlGamerHandle& targetGamer)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_PROFILE_DIALOG))
	{
		rlError("SCE_SYSMODULE_NP_PROFILE_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_PROFILE);
	if( ret != SCE_OK ) 
	{
		rlError("ShowProfile() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowProfile");

	SceNpProfileDialogParam param;
	sceNpProfileDialogParamInitialize( &param );

	param.mode = SCE_NP_PROFILE_DIALOG_MODE_NORMAL;
	param.userId = userId; // User ID of the operating user
	param.targetOnlineId = targetGamer.GetNpOnlineId().AsSceNpOnlineId(); // Online ID of the display target player

	ret = sceNpProfileDialogOpen(&param);
	if( ret != SCE_OK ) 
	{
		rlError("sceNpProfileDialogOpen() failed. ret = 0x%x\n", ret);
		return false;
	}

	m_CurrentDialogType = DIALOG_PROFILE;
	m_bDialogShowing = true;

	return true;
}
#endif

bool rlNpCommonDialog::ShowUrl(int userId, const rlSystemBrowserConfig& config)
{
    if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_WEB_BROWSER_DIALOG))
    {
        rlError("SCE_SYSMODULE_WEB_BROWSER_DIALOG is not loaded");
        return false;
    }

    int ret = PrepareDialog(DIALOG_BROWSER);
    if(ret != SCE_OK)
    {
        rlError("ShowUrl() failed. ret = 0x%x\n", ret);
        return false;
	}

	// copy out config
	m_BrowserConfig = config;

    rlDebug("ShowUrl :: Url: %s", m_BrowserConfig.m_Url);

    SceWebBrowserDialogParam param;
    sceWebBrowserDialogParamInitialize(&param);
    param.mode = SCE_WEB_BROWSER_DIALOG_MODE_DEFAULT;
    param.url = m_BrowserConfig.m_Url;
    param.userId = userId;

#if RSG_PROSPERO
	SceWebBrowserDialogWebViewParam	webview_param;
	memset(&webview_param, 0x0, sizeof(webview_param));
	webview_param.size = sizeof(SceWebBrowserDialogWebViewParam);
	webview_param.option = SCE_WEB_BROWSER_DIALOG_WEBVIEW_OPTION_NONE;
	param.webviewParam = &webview_param;
#endif

    // If the code is requesting a custom web browser dialog..
    if(m_BrowserConfig.m_DialogMode == rlSystemBrowserConfig::DM_CUSTOM)
    {
        // inform the SDK we're using a custom dialog
        param.mode = SCE_WEB_BROWSER_DIALOG_MODE_CUSTOM;

        // Create the desired X/Y Width/Height for either FULLSCREEN or CUSTOM mode
        int desiredWidth = 0, desiredHeight = 0;
        int desiredX = 0, desiredY = 0;
        if(m_BrowserConfig.m_ScreenMode == rlSystemBrowserConfig::SM_FULLSCREEN)
        {
            // Position at 0,0 with the full screen width/height
            desiredWidth = m_ScreenWidth;
            desiredHeight = m_ScreenHeight;
            desiredX = 0;
            desiredY = 0;
        }
        else
        {
            // assert if the user provides out of bounds values
            rlAssert(m_BrowserConfig.m_Width <= NP_COMMON_DIALOG_MAX_WIDTH);
            rlAssert(m_BrowserConfig.m_Height <= NP_COMMON_DIALOG_MAX_HEIGHT);

            // Position at X,Y with Width/Height
            desiredWidth = m_BrowserConfig.m_Width;
            desiredHeight = m_BrowserConfig.m_Height;
            desiredX = m_BrowserConfig.m_PosX;
            desiredY = m_BrowserConfig.m_PosY;
        }

        // We can automatically scale down the browser based on the safe region.
		if(m_BrowserConfig.m_SafeRegion && !PARAM_netDisableBrowserSafeRegion.Get())
		{
            // Get the safe area. Sony docs say this returns a value between 0.9-1.0f
            SceSystemServiceDisplaySafeAreaInfo info;
            if(rlVerify(sceSystemServiceGetDisplaySafeAreaInfo(&info) == SCE_OK))
            {
                rlDebug3("Safe Area: %f", info.ratio);

                // clamp the ratio just in case
                float clampedRatio = rage::Clamp(info.ratio, 0.0f, 1.0f);

                // calculate the % of the screen outside the safe area
                float boundaryRatio = (1.0f - clampedRatio);

                // calculate the offset on either side of the window
                int xOffset = ((float)desiredWidth * boundaryRatio) / 2.0f;
                int yOffset = ((float)desiredHeight * boundaryRatio) / 2.0f;

                // push the browser into the safe area
                desiredX += xOffset;			// safe region: left
                desiredY += yOffset;			// safe region: top
                desiredWidth -= (2 * xOffset);	// safe region: width (right)
                desiredHeight -= yOffset;		// safe region: height (bottom)

				// if no footer - apply the safe region to the bottom as well
                if((m_BrowserConfig.m_Parts & rlSystemBrowserConfig::PARTS_FOOTER) == 0)
                    desiredHeight -= yOffset; // safe region: height (bottom)
            }
        }

        // Setup the header position
        param.headerWidth = desiredWidth;
        param.headerPositionX = desiredX;
        param.headerPositionY = desiredY;

        // Move the browser down in position to account for visible header components
        if((m_BrowserConfig.m_Parts & rlSystemBrowserConfig::PARTS_TITLE) != 0)
        {
            desiredHeight -= m_BrowserTitleHeight;
            desiredY += m_BrowserTitleHeight;
        }
        if((m_BrowserConfig.m_Parts & rlSystemBrowserConfig::PARTS_ADDRESS) != 0)
        {
            desiredHeight -= m_BrowserAddressHeight;
            desiredY += m_BrowserAddressHeight;
        }
        if((m_BrowserConfig.m_Parts & rlSystemBrowserConfig::PARTS_FOOTER) != 0)
        {
            desiredHeight -= m_BrowserFooterHeight;
            // don't increase desiredY -> footer is at the bottom
        }

#if RSG_PROSPERO
		if((m_BrowserConfig.m_ViewOptions & rlSystemBrowserConfig::VIEW_OPTION_TRANSPARENT) != 0)
		{
			webview_param.option |= SCE_WEB_BROWSER_DIALOG_WEBVIEW_OPTION_BACKGROUND_TRANSPARENCY;
		}

		if((m_BrowserConfig.m_ViewOptions & rlSystemBrowserConfig::VIEW_OPTION_NO_CURSOR) != 0)
		{
			webview_param.option |= SCE_WEB_BROWSER_DIALOG_WEBVIEW_OPTION_CURSOR_NONE;
		}
#endif

		rlDebug1("ShowUrl :: Url: %s, Mode: %s, Dimensions: %dx%d, Pos: %d,%d, Parts: 0x%x, Control: 0x%x, Flags: 0x%x, Animate: %s",
			m_BrowserConfig.m_Url,
			m_BrowserConfig.m_DialogMode == rlSystemBrowserConfig::DialogMode::DM_DEFAULT ? "Default" : "Custom",
			desiredWidth,
			desiredHeight,
			desiredX,
			desiredY,
			m_BrowserConfig.m_Parts,
			m_BrowserConfig.m_Controls,
			m_BrowserConfig.m_Flags,
			m_BrowserConfig.m_Animate ? "True" : "False");

		param.width = desiredWidth;
		param.height = desiredHeight;
		param.positionX = desiredX;
		param.positionY = desiredY;
		param.parts = m_BrowserConfig.m_Parts;
		param.control = m_BrowserConfig.m_Controls;
#if RSG_PROSPERO
		param.animation = m_BrowserConfig.m_Animate ? SCE_WEB_BROWSER_DIALOG_ANIMATION_DEFAULT : SCE_WEB_BROWSER_DIALOG_ANIMATION_DISABLE;
#endif
    }

    // is there a callback url
    SceWebBrowserDialogCallbackInitParam callback;
    if(StringLength(m_BrowserConfig.m_CallbackUrl) > 0)
    {
        // setup callback url
        sysMemSet(&callback, 0, sizeof(callback));
        callback.size = sizeof(callback);
        callback.data = m_BrowserConfig.m_CallbackUrl;
        callback.type = SCE_WEB_BROWSER_DIALOG_CALLBACK_PARAM_TYPE_URL;

        // pass to browser dialog
        param.callbackInitParam = &callback;
    }

	// if this is a deep link, we have to manage closing the browser separately on our PS4 SDK
	if((m_BrowserConfig.m_Flags & rlSystemBrowserConfig::FLAG_DEEP_LINK_URL) != 0)
	{
		rlDebug1("ShowUrl :: Using Deep Link Url");
		m_DeepLinkUrlState = DeepLinkUrlState::DLS_Opening;
#if RL_NP_DEEP_LINK_MANUAL_CLOSE
		m_DeepLinkCloseTime = (m_BrowserConfig.m_DeepLinkCloseTime > 0) ? m_BrowserConfig.m_DeepLinkCloseTime : ms_BrowserDeepLinkCloseTime;
#endif
	}

    ret = sceWebBrowserDialogOpen(&param);
    if(ret != SCE_OK)
    {
        rlError("sceWebBrowserDialogOpen() failed. ret = 0x%x\n", ret);
        return false;
    }

    m_CurrentDialogType = DIALOG_BROWSER;
    m_bDialogShowing = true;
    return true;
}

bool rlNpCommonDialog::CloseBrowser()
{
	if(m_CurrentDialogType == DIALOG_BROWSER && m_bDialogShowing)
	{
		int ret = sceWebBrowserDialogClose();
		rlAssert(ret == SCE_OK);
		return ret == SCE_OK;
	}
	return false;
}

int rlNpCommonDialog::ShowEmptyStoreMsg()
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_MESSAGE_DIALOG))
	{
		rlError("SCE_SYSMODULE_MESSAGE_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_MSG);
	if( ret != SCE_OK ) 
	{
		rlError("ShowEmptyStoreMsg() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowEmptyStoreMsg");

	SceMsgDialogParam param;
	sceMsgDialogParamInitialize( &param );
	param.mode = SCE_MSG_DIALOG_MODE_SYSTEM_MSG;

	SceMsgDialogSystemMessageParam messageParam;
	memset( &messageParam, 0, sizeof(messageParam) );
#if RSG_ORBIS
	messageParam.sysMsgType = SCE_MSG_DIALOG_SYSMSG_TYPE_TRC_EMPTY_STORE;
#else
	messageParam.sysMsgType = SCE_MSG_DIALOG_SYSMSG_TYPE_EMPTY_STORE;
#endif
	param.sysMsgParam = &messageParam;

	ret = sceMsgDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceMsgDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_MSG;
	m_bDialogShowing = true;
	return ret;
}

int rlNpCommonDialog::ShowChatRestrictionMsg(int userId)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_MESSAGE_DIALOG))
	{
		rlError("SCE_SYSMODULE_MESSAGE_DIALOG is not loaded");
		return false;
	}

	// Queue
	if(m_bDialogShowing)
	{
		m_ChatMsgQueued = true;
		m_QueuedId = userId;
		rlDebug3("Queued chat restriction message for user id %d", m_QueuedId);
		return SCE_OK;
	}

	int ret = PrepareDialog(DIALOG_MSG);
	if( ret != SCE_OK ) 
	{
		rlError("ShowChatRestrictionMsg() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowChatRestrictionMsg");

	SceMsgDialogParam param;
	sceMsgDialogParamInitialize( &param );
	param.mode = SCE_MSG_DIALOG_MODE_SYSTEM_MSG;
	param.userId = userId;

	SceMsgDialogSystemMessageParam messageParam;
	memset( &messageParam, 0, sizeof(messageParam) );
	// PS4 SDK 8 
	// Unified what is displayed in the system defined messages listed below with SCE_MSG_DIALOG_SYSMSG_TYPE_PSN_COMMUNICATION_RESTRICTION. 
	// These system defined messages will be removed from a future version of the SDK; do not use them.
	/// SCE_MSG_DIALOG_SYSMSG_TYPE_TRC_PSN_CHAT_RESTRICTION
	///	SCE_MSG_DIALOG_SYSMSG_TYPE_TRC_PSN_UGC_RESTRICTION
#if RSG_ORBIS
	messageParam.sysMsgType = SCE_MSG_DIALOG_SYSMSG_TYPE_TRC_PSN_CHAT_RESTRICTION;
#else
	messageParam.sysMsgType = SCE_MSG_DIALOG_SYSMSG_TYPE_PSN_COMMUNICATION_RESTRICTION;
#endif
	param.sysMsgParam = &messageParam;

	ret = sceMsgDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceMsgDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_MSG;
	m_bDialogShowing = true;
	return ret;
}

int rlNpCommonDialog::ShowUGCRestrictionMsg(int userId)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_MESSAGE_DIALOG))
	{
		rlError("SCE_SYSMODULE_MESSAGE_DIALOG is not loaded");
		return false;
	}	

	// Queue
	if(m_bDialogShowing)
	{
		m_UgcMsgQueued = true;
		m_QueuedId = userId;
		rlDebug3("Queued ugc restriction message for user id %d", m_QueuedId);
		return SCE_OK;
	}

	int ret = PrepareDialog(DIALOG_MSG);
	if( ret != SCE_OK ) 
	{
		rlError("ShowUGCRestrictionMsg() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowUGCRestrictionMsg");

	SceMsgDialogParam param;
	sceMsgDialogParamInitialize( &param );
	param.mode = SCE_MSG_DIALOG_MODE_SYSTEM_MSG;
	param.userId = userId;

	SceMsgDialogSystemMessageParam messageParam;
	memset( &messageParam, 0, sizeof(messageParam) );
	// PS4 SDK 8 
	// Unified what is displayed in the system defined messages listed below with SCE_MSG_DIALOG_SYSMSG_TYPE_PSN_COMMUNICATION_RESTRICTION. 
	// These system defined messages will be removed from a future version of the SDK; do not use them.
	/// SCE_MSG_DIALOG_SYSMSG_TYPE_TRC_PSN_CHAT_RESTRICTION
	///	SCE_MSG_DIALOG_SYSMSG_TYPE_TRC_PSN_UGC_RESTRICTION
#if RSG_ORBIS
	messageParam.sysMsgType = SCE_MSG_DIALOG_SYSMSG_TYPE_TRC_PSN_UGC_RESTRICTION;
#else
	messageParam.sysMsgType = SCE_MSG_DIALOG_SYSMSG_TYPE_PSN_COMMUNICATION_RESTRICTION;
#endif
	param.sysMsgParam = &messageParam;

	ret = sceMsgDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceMsgDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_MSG;
	m_bDialogShowing = true;
	return ret;
}

int rlNpCommonDialog::ShowSaveListDialog(int userId, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, const SceSaveDataDirName *dirNames, const uint32_t hitNum, bool showNewItem)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_SAVE_DATA_DIALOG))
	{
		rlError("SCE_SYSMODULE_SAVE_DATA_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_SAVEDATA);
	if( ret != SCE_OK ) 
	{
		rlError("ShowSaveListDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowSaveListDialog");

	SceSaveDataDialogParam param;
	sceSaveDataDialogParamInitialize(&param);
	param.mode = SCE_SAVE_DATA_DIALOG_MODE_LIST;
	param.dispType = type;

	SceSaveDataDialogItems items;
	memset(&items, 0x00, sizeof(items));
	SceSaveDataDialogNewItem newItem;
	memset(&newItem, 0x00, sizeof(newItem));
	items.userId = userId;
	items.titleId = titleId;
	items.dirName = dirNames;
	items.dirNameNum = hitNum;
	items.newItem = showNewItem ? & newItem : NULL;
	items.focusPos = SCE_SAVE_DATA_DIALOG_FOCUS_POS_DATAOLDEST;
	items.itemStyle = SCE_SAVE_DATA_DIALOG_ITEM_STYLE_TITLE_DATESIZE_SUBTITLE;

	param.items = &items;
	ret = sceSaveDataDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceSaveDataDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_SAVEDATA;
	m_bDialogShowing = true;
	return ret;
}

int rlNpCommonDialog::ShowSaveDialogConfirm(int userId, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, const SceSaveDataDirName *dirName, const SceSaveDataDialogSystemMessageType systemMessageType)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_SAVE_DATA_DIALOG))
	{
		rlError("SCE_SYSMODULE_SAVE_DATA_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_SAVEDATA);
	if( ret != SCE_OK ) 
	{
		rlError("ShowSaveDialogConfirm() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowSaveDialogConfirm");

	SceSaveDataDialogParam param;
	sceSaveDataDialogParamInitialize(&param);
	param.mode = SCE_SAVE_DATA_DIALOG_MODE_SYSTEM_MSG;
	param.dispType = type;

	SceSaveDataDialogItems items;
	memset(&items, 0x00, sizeof(items));
	items.userId = userId;
	items.titleId = titleId;
	items.dirName = dirName;
	items.dirNameNum = (dirName == NULL) ? 0 : 1;

	SceSaveDataDialogSystemMessageParam sysm;
	memset(&sysm, 0x00, sizeof(sysm));
	sysm.sysMsgType = systemMessageType;

	param.items = &items;
	param.sysMsgParam = &sysm;
	ret = sceSaveDataDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceSaveDataDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_SAVEDATA;
	m_bDialogShowing = true;
	return SCE_OK;
}

int rlNpCommonDialog::ShowSaveDialogOperating(int userId, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, SceSaveDataDirName* dirName)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_SAVE_DATA_DIALOG))
	{
		rlError("SCE_SYSMODULE_SAVE_DATA_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_SAVEDATA);
	if( ret != SCE_OK ) 
	{
		rlError("ShowSaveDialogOperating() failed. ret = 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

    rlDebug("ShowSaveDialogOperating");

	SceSaveDataDialogParam param;
	sceSaveDataDialogParamInitialize(&param);
	param.mode = SCE_SAVE_DATA_DIALOG_MODE_PROGRESS_BAR;
	param.dispType = type;

	SceSaveDataDialogItems items;
	memset(&items, 0x00, sizeof(items));
	items.userId = userId;
	items.titleId = titleId;
	items.dirName = dirName;
	items.dirNameNum = (dirName == NULL) ? 0 : 1;

	SceSaveDataDialogProgressBarParam bparam;
	memset(&bparam, 0x00, sizeof(bparam));
	bparam.barType = SCE_SAVE_DATA_DIALOG_PROGRESSBAR_TYPE_PERCENTAGE;
	bparam.msg = NULL;			// NULL == system message

	param.items = &items;
	param.progBarParam = &bparam;
	ret = sceSaveDataDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceSaveDataDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_SAVEDATA;
	m_bDialogShowing = true;
	return SCE_OK;
}

int rlNpCommonDialog::ShowSaveDialogError(int userId, int errCode, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, SceSaveDataDirName* dirName)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_SAVE_DATA_DIALOG))
	{
		rlError("SCE_SYSMODULE_SAVE_DATA_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_SAVEDATA);
	if( ret != SCE_OK ) 
	{
		rlError("ShowSaveDialogError() failed. ret = 0x%x\n", ret);
		return false;
	}
        
    rlDebug("ShowSaveDialogError");

	SceSaveDataDialogParam param;
	sceSaveDataDialogParamInitialize(&param);
	param.mode = SCE_SAVE_DATA_DIALOG_MODE_ERROR_CODE;
	param.dispType = type;

	SceSaveDataDialogItems items;
	memset(&items, 0x00, sizeof(items));
	items.userId = userId;
	items.titleId = titleId;
	items.dirName = dirName;
	items.dirNameNum = 1;

	SceSaveDataDialogErrorCodeParam eparam;
	memset(&eparam, 0x00, sizeof(eparam));
	eparam.errorCode = errCode;

	param.items = &items;
	param.errorCodeParam = &eparam;
	ret = sceSaveDataDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceSaveDataDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_SAVEDATA;
	m_bDialogShowing = true;
	return ret;
}

int rlNpCommonDialog::ShowSaveDialogNoSpace(int userId, const SceSaveDataTitleId * titleId, const SceSaveDataDirName* dirName, u64 requiredBlocks)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_SAVE_DATA_DIALOG))
	{
		rlError("SCE_SYSMODULE_SAVE_DATA_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_SAVEDATA);
	if( ret != SCE_OK ) 
	{
		rlError("ShowSaveDialogError() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowSaveDialogNoSpace");

	SceSaveDataDialogParam param;
	sceSaveDataDialogParamInitialize(&param);
	param.mode = SCE_SAVE_DATA_DIALOG_MODE_SYSTEM_MSG;
	param.dispType = SCE_SAVE_DATA_DIALOG_TYPE_SAVE;

	SceSaveDataDialogAnimationParam animParam;
	memset(&animParam, 0x00, sizeof(animParam));
	animParam.userOK = SCE_SAVE_DATA_DIALOG_ANIMATION_ON;
	animParam.userCancel = SCE_SAVE_DATA_DIALOG_ANIMATION_ON;
	param.animParam = &animParam;

	SceSaveDataDialogItems items;
	memset(&items, 0x00, sizeof(items));
	items.userId = userId;
	items.titleId = titleId;
	items.dirName = dirName;
	if(dirName)
	{
		items.dirNameNum = dirName->data[0] == '\0' ? 0 : 1;
	}
	items.newItem = NULL;

	SceSaveDataDialogSystemMessageParam sysMessageParam;
	memset(&sysMessageParam, 0x00, sizeof(sysMessageParam));
	sysMessageParam.sysMsgType = SCE_SAVE_DATA_DIALOG_SYSMSG_TYPE_NOSPACE_CONTINUABLE;
	sysMessageParam.value = requiredBlocks;

	param.items = &items;
	param.sysMsgParam = &sysMessageParam;
	ret = sceSaveDataDialogOpen(&param);

	if(ret < SCE_OK)
	{
		rlError("sceSaveDataDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_SAVEDATA;
	m_bDialogShowing = true;
	return ret;
}

int rlNpCommonDialog::ShowSaveDialogFileCorrupt(int userId, const SceSaveDataTitleId * titleId, const SceSaveDataDirName* dirName)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_SAVE_DATA_DIALOG))
	{
		rlError("SCE_SYSMODULE_SAVE_DATA_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_SAVEDATA);
	if( ret != SCE_OK ) 
	{
		rlError("ShowSaveDialogError() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowSaveDialogFileCorrupt");

	SceSaveDataDialogParam param;
	sceSaveDataDialogParamInitialize(&param);
	param.mode = SCE_SAVE_DATA_DIALOG_MODE_SYSTEM_MSG;
	param.dispType = SCE_SAVE_DATA_DIALOG_TYPE_SAVE;

	SceSaveDataDialogAnimationParam animParam;
	memset(&animParam, 0x00, sizeof(animParam));
	animParam.userOK = SCE_SAVE_DATA_DIALOG_ANIMATION_ON;
	animParam.userCancel = SCE_SAVE_DATA_DIALOG_ANIMATION_ON;
	param.animParam = &animParam;

	SceSaveDataDialogItems items;
	memset(&items, 0x00, sizeof(items));
	items.userId = userId;
	items.titleId = titleId;
	items.dirName = dirName;
	if(dirName)
	{
		items.dirNameNum = dirName->data[0] == '\0' ? 0 : 1;
	}
	items.newItem = NULL;

	SceSaveDataDialogSystemMessageParam sysMessageParam;
	memset(&sysMessageParam, 0x00, sizeof(sysMessageParam));
	sysMessageParam.sysMsgType = SCE_SAVE_DATA_DIALOG_SYSMSG_TYPE_FILE_CORRUPTED;

	param.items = &items;
	param.sysMsgParam = &sysMessageParam;
	ret = sceSaveDataDialogOpen(&param);

	if(ret < SCE_OK)
	{
		rlError("sceSaveDataDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_SAVEDATA;
	m_bDialogShowing = true;
	return ret;
}

#if RSG_ORBIS
bool rlNpCommonDialog::SendInvitation(int userId, const rlSceNpSessionId& sessionId, const char * salutation)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_INVITATION_DIALOG))
	{
		rlError("SCE_SYSMODULE_INVITATION_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_INVITATION);
	if( ret != SCE_OK ) 
	{
		rlError("SendInvitation() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("SendInvitation");

#if LEGACY_SCE_ORBIS_SDK
	SceInvitationDialogParam param;
	sceInvitationDialogParamInit( &param );
	SceInvitationDialogDataParam dataParam;
#else
	SceInvitationDialogParamA param;
	sceInvitationDialogParamInitializeA(&param);
	SceInvitationDialogDataParamA dataParam;
#endif
		
	memset( &dataParam, 0, sizeof(dataParam) );

	SceNpSessionId sceSessionId = sessionId.AsSceNpSessionId();
	dataParam.SendInfo.sessionId = &sceSessionId; // Obtain in advance
	dataParam.SendInfo.userMessage = salutation;
	dataParam.SendInfo.addressParam.addressType = SCE_INVITATION_DIALOG_ADDRESS_TYPE_USERENABLE;

#if LEGACY_SCE_ORBIS_SDK
	dataParam.SendInfo.addressParam.addressInfo.UserSelectEnableAddress.onlineIdsMaxCount = 4;
#else
	dataParam.SendInfo.addressParam.addressInfo.UserSelectEnableAddress.userMaxCount = 4;
#endif

	param.mode = SCE_INVITATION_DIALOG_MODE_SEND;
	param.dataParam = &dataParam;
	param.userId = userId;

#if LEGACY_SCE_ORBIS_SDK
	ret = sceInvitationDialogOpen(&param);
#else
	ret = sceInvitationDialogOpenA(&param);
#endif

	if(ret < SCE_OK)
	{
		rlError("sceInvitationDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_INVITATION;
	m_bDialogShowing = true;
	return ret;
}

bool rlNpCommonDialog::ReceiveInvitation(int userId)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_INVITATION_DIALOG))
	{
		rlError("SCE_SYSMODULE_INVITATION_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_INVITATION);
	if( ret != SCE_OK ) 
	{
		rlError("ReceiveInvitation() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ReceiveInvitation");

#if LEGACY_SCE_ORBIS_SDK
	SceInvitationDialogParam param;
	sceInvitationDialogParamInit(&param);
	SceInvitationDialogDataParam dataParam;
#else
	SceInvitationDialogParamA param;
	sceInvitationDialogParamInitializeA(&param);
	SceInvitationDialogDataParamA dataParam;
#endif
	memset( &dataParam, 0, sizeof(dataParam) );

	param.mode = SCE_INVITATION_DIALOG_MODE_RECV;
	param.dataParam = &dataParam;
	param.userId = userId;

#if LEGACY_SCE_ORBIS_SDK
	ret = sceInvitationDialogOpen(&param);
#else
	ret = sceInvitationDialogOpenA(&param);
#endif
	if(ret < SCE_OK)
	{
		rlError("sceInvitationDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return false;
	}

	m_CurrentDialogType = DIALOG_INVITATION;
	m_bDialogShowing = true;
	return true;
}
#else //RSG_ORBIS
bool rlNpCommonDialog::SendInvitation(int userId, const rlSceNpSessionId& sessionId, const char* UNUSED_PARAM(salutation))
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_PLAYER_INVITATION_DIALOG))
	{
		rlError("SCE_SYSMODULE_PLAYER_INVITATION_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_INVITATION);
	if(ret != SCE_OK)
	{
		rlError("SendInvitation() failed. ret = 0x%x\n", ret);
		return false;
	}

	ScePlayerInvitationDialogParam param;
	scePlayerInvitationDialogParamInitialize(&param);

	ScePlayerInvitationDialogSendParam dataParam;
	memset(&dataParam, 0, sizeof(dataParam));

	param.userId = userId;
	param.mode = SCE_PLAYER_INVITATION_DIALOG_MODE_SEND;
	dataParam.sessionId = sessionId.GetData();
	param.sendParam = &dataParam;

	ret = scePlayerInvitationDialogOpen(&param);
	if(ret < SCE_OK)
	{
		rlError("sceInvitationDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return false;
	}

	m_CurrentDialogType = DIALOG_INVITATION;
	m_bDialogShowing = true;
	return true;
}
#endif //!RSG_ORBIS

bool rlNpCommonDialog::ShowCommerceCategoryDialog(const int localGamerIndex, int userId, int numLabels, const char * const *categoryLabel)
{
	if(!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		rlError("Invalid gamer index");
		return false;
	}

	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_COMMERCE))
	{
		rlError("SCE_SYSMODULE_NP_COMMERCE is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_COMMERCE);
	if( ret != SCE_OK ) 
	{
		rlError("ShowCommerceCategoryDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowCommerceCategoryDialog");

	SceNpCommerceDialogParam param;
	sceNpCommerceDialogParamInitialize( &param );
	param.userId = userId;
	param.mode = SCE_NP_COMMERCE_DIALOG_MODE_CATEGORY;
	param.targets = categoryLabel;
	param.numTargets = numLabels;

	ret = sceNpCommerceDialogOpen( &param );
	if( ret < SCE_OK ) 
	{
		rlError("sceNpCommerceDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_COMMERCE;
	m_bDialogShowing = true;
	m_CommerceMode = param.mode;
	return true;
}

bool rlNpCommonDialog::ShowCommerceProductDialog(int userId, int numLabels, const char * const * productLabels)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_COMMERCE))
	{
		rlError("SCE_SYSMODULE_NP_COMMERCE is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_COMMERCE);
	if( ret != SCE_OK ) 
	{
		rlError("ShowCommerceProductDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowCommerceProductDialog");

	SceNpCommerceDialogParam param;
	sceNpCommerceDialogParamInitialize( &param );
	param.userId = userId;
	param.mode = SCE_NP_COMMERCE_DIALOG_MODE_PRODUCT;
	param.targets = productLabels;
	param.numTargets = numLabels;

	ret = sceNpCommerceDialogOpen( &param );
	if( ret < SCE_OK ) 
	{
		rlError("ShowCommerceProductDialog : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_COMMERCE;
	m_bDialogShowing = true;
	m_CommerceMode = param.mode;
	return true;
}

bool rlNpCommonDialog::ShowCommerceCheckoutDialog(int userId, int numLabels, const char * const * skuIds)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_COMMERCE))
	{
		rlError("SCE_SYSMODULE_NP_COMMERCE is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_COMMERCE);
	if( ret != SCE_OK ) 
	{
		rlError("ShowCommerceCheckoutDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowCommerceCheckoutDialog");

	SceNpCommerceDialogParam param;
	sceNpCommerceDialogParamInitialize( &param );
	param.userId = userId;
	param.mode = SCE_NP_COMMERCE_DIALOG_MODE_CHECKOUT;
	param.targets = skuIds;
	param.numTargets = numLabels;

	ret = sceNpCommerceDialogOpen( &param );
	if( ret < SCE_OK ) 
	{
		rlError("ShowCommerceCheckoutDialog : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_COMMERCE;
	m_bDialogShowing = true;
	m_CommerceMode = param.mode;
	return true;
}

bool rlNpCommonDialog::ShowCommerceDownloadDialog(int userId, int numLabels, const char * const * skuIds)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_COMMERCE))
	{
		rlError("SCE_SYSMODULE_NP_COMMERCE is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_COMMERCE);
	if( ret != SCE_OK ) 
	{
		rlError("ShowCommerceDownloadDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowCommerceDownloadDialog");

	SceNpCommerceDialogParam param;
	sceNpCommerceDialogParamInitialize( &param );
	param.userId = userId;
	param.mode = SCE_NP_COMMERCE_DIALOG_MODE_DOWNLOADLIST;
	param.targets = skuIds;
	param.numTargets = numLabels;
		
	//Dont think we will use this.
	//param.serviceLabel = serviceLabel;

	ret = sceNpCommerceDialogOpen( &param );
	if( ret < SCE_OK ) 
	{
		rlError("ShowCommerceCheckoutDialog : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_COMMERCE;
	m_bDialogShowing = true;
	m_CommerceMode = param.mode;
	return true;
}

bool rlNpCommonDialog::ShowCommerceCodeEntryDialog(int userId)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_COMMERCE))
	{
		rlError("SCE_SYSMODULE_NP_COMMERCE is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_COMMERCE);
	if( ret != SCE_OK ) 
	{
		rlError("ShowCommerceCodeEntryDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowCommerceCodeEntryDialog");

	SceNpCommerceDialogParam param;
	sceNpCommerceDialogParamInitialize( &param );
	param.userId = userId;
	param.mode = SCE_NP_COMMERCE_DIALOG_MODE_PRODUCT_CODE;
	param.targets = NULL;
	param.numTargets = 0;
	param.serviceLabel = 0;

	ret = sceNpCommerceDialogOpen( &param );
	if( ret < SCE_OK ) 
	{
		rlError("ShowCommerceCommerceCodeEntryDialog : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_COMMERCE;
	m_bDialogShowing = true;
	m_CommerceMode = param.mode;
	return true;
}

bool rlNpCommonDialog::ShowCommercePSPlusDialog(int userId)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_COMMERCE))
	{
		rlError("SCE_SYSMODULE_NP_COMMERCE is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_COMMERCE);
	if( ret != SCE_OK ) 
	{
		rlError("ShowCommercePSPlusDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowCommercePSPlusDialog");

	SceNpCommerceDialogParam param;
	sceNpCommerceDialogParamInitialize( &param );

	param.userId = userId;
	param.targets = NULL;
	param.numTargets = 0;
#if RSG_ORBIS
	param.mode = SCE_NP_COMMERCE_DIALOG_MODE_PLUS;
	param.features = SCE_NP_PLUS_FEATURE_REALTIME_MULTIPLAY; // Specify PlayStation�Plus member exclusive feature type to apply for
#else
	param.serviceLabel = g_rlNp.GetNpCommerceServiceLabel();
	param.mode = SCE_NP_COMMERCE_DIALOG_MODE_PREMIUM;
	param.features = SCE_NP_PREMIUM_FEATURE_REALTIME_MULTIPLAY; // Specify PlayStation�Plus member exclusive feature type to apply for
#endif
	
	ret = sceNpCommerceDialogOpen( &param );
	if( ret < SCE_OK ) 
	{
		rlError("ShowCommerceCheckoutDialog : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_COMMERCE;
	m_bDialogShowing = true;
	m_UserId = userId;
	m_CommerceMode = param.mode;
	return true;
}

#if RL_FACEBOOK_ENABLED
// Facebook
bool rlNpCommonDialog::ShowFacebookPermissionDialog(int userId, u64 appId, const char * permissions)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_SNS_FACEBOOK))
	{
		rlError("SCE_SYSMODULE_NP_SNS_FACEBOOK is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_FACEBOOK);
	if( ret != SCE_OK ) 
	{
		rlError("ShowFacebookPermissionDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowFacebookPermissionDialog");

	SceNpSnsFacebookDialogParam		dialogParam;
	sceNpSnsFacebookDialogParamInit( &dialogParam );
	dialogParam.accessTokenParam.userId = userId;
	dialogParam.accessTokenParam.fbAppId = appId;
	safecpy(dialogParam.accessTokenParam.permissions, permissions);

	ret = sceNpSnsFacebookDialogOpen( &dialogParam );
	if(ret < SCE_OK) 
	{
		rlError("sceNpSnsFacebookDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_FACEBOOK;
	m_bDialogShowing = true;
	return true;
}
#endif // RL_FACEBOOK_ENABLED

// Friends
#if RSG_ORBIS
bool rlNpCommonDialog::ShowFriendsDialog(int /*userId*/, bool bSearch, bool bOnlineOnly)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_FRIEND_LIST_DIALOG))
	{
		rlError("SCE_SYSMODULE_NP_FRIEND_LIST_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_FRIENDS);
	if( ret != SCE_OK ) 
	{
		rlError("ShowFriendsDialog() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowFriendsDialog");

#if LEGACY_SCE_ORBIS_SDK
	SceNpFriendListDialogParam param;
	sceNpFriendListDialogParamInitialize( &param );
#else
	SceNpFriendListDialogParamA param;
	sceNpFriendListDialogParamInitializeA(&param);
#endif
	param.mode = SCE_NP_FRIEND_LIST_DIALOG_MODE_FRIEND_LIST;

	if(bOnlineOnly)
	{
		param.menuItems = SCE_NP_FRIEND_LIST_DIALOG_MENU_ITEM_ONLINE;
	}
	else
	{
		param.menuItems = SCE_NP_FRIEND_LIST_DIALOG_MENU_ITEM_ALL_FRIENDS;
	}

	if(bSearch)
	{
		param.menuItems |= SCE_NP_FRIEND_LIST_DIALOG_MENU_ITEM_SEARCH;
	}

#if LEGACY_SCE_ORBIS_SDK
	ret = sceNpFriendListDialogOpen( &param );
#else
	ret = sceNpFriendListDialogOpenA( &param );
#endif

	if(ret < SCE_OK) 
	{
		rlError("sceNpFriendListDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_FRIENDS;
	m_bDialogShowing = true;
	return true;
}
#endif

#if RSG_ORBIS
bool rlNpCommonDialog::ShowGameCustomData(int userId, void * data, int dataLen, u8* imgBuf, int imgLen, const char * userMessage, const char * dataDetail, const char * dataName, int expireMinutes)
{
	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_GAME_CUSTOM_DATA_DIALOG))
	{
		rlError("SCE_SYSMODULE_GAME_CUSTOM_DATA_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_CUSTOM_GAMEDATA);
	if( ret != SCE_OK ) 
	{
		rlError("ShowGameCustomData() failed. ret = 0x%x\n", ret);
		return false;
	}

    rlDebug("ShowGameCustomData");

#if LEGACY_SCE_ORBIS_SDK
	SceGameCustomDataDialogParam param;
#else
	SceGameCustomDataDialogParamA param;
#endif
	sysMemSet(&param, 0, sizeof(param));
#if LEGACY_SCE_ORBIS_SDK
	sceGameCustomDataDialogParamInit( &param );
#else
	sceGameCustomDataDialogParamInitializeA(&param);
#endif
		
	const char *platform_list[1];
	platform_list[0] = "PS4";

#if LEGACY_SCE_ORBIS_SDK
	SceGameCustomDataDialogDataParam dataParam;
#else
	SceGameCustomDataDialogDataParamA dataParam;
#endif
	sysMemSet( &dataParam, 0, sizeof(dataParam) );
	dataParam.SendInfo.data = data;
	dataParam.SendInfo.dataSize = dataLen;
	dataParam.SendInfo.userMessage = userMessage;
	dataParam.SendInfo.expireMinutes = expireMinutes;
	dataParam.SendInfo.dataDetail = dataDetail;
	dataParam.SendInfo.dataName = dataName;
	dataParam.SendInfo.availablePlatform.count = 1;
	dataParam.SendInfo.availablePlatform.platformName = platform_list;

	dataParam.SendInfo.thumbnail = imgBuf;
	dataParam.SendInfo.thumbnailSize = imgLen;

	dataParam.SendInfo.addressParam.addressType = SCE_GAME_CUSTOM_DATA_DIALOG_ADDRESS_TYPE_USERENABLE;
#if LEGACY_SCE_ORBIS_SDK
	dataParam.SendInfo.addressParam.addressInfo.UserSelectEnableAddress.onlineIdsMaxCount = 4;
#else
	dataParam.SendInfo.addressParam.addressInfo.UserSelectEnableAddress.userMaxCount = 4;
#endif

		
	param.mode = SCE_GAME_CUSTOM_DATA_DIALOG_MODE_SEND;
	param.dataParam = &dataParam;
	param.userId = userId;
	param.size = sizeof(param);

#if LEGACY_SCE_ORBIS_SDK
	ret = sceGameCustomDataDialogOpen(&param);
#else
	ret = sceGameCustomDataDialogOpenA(&param);
#endif

	if( ret < SCE_OK ) 
	{
		rlError("sceGameCustomDataDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_CUSTOM_GAMEDATA;
	m_bDialogShowing = true;
	return true;
}
#endif

bool rlNpCommonDialog::ShowCommerceLogo( eCommerceLogoPosition pos )
{
	return ( sceNpCommerceShowPsStoreIcon(static_cast<SceNpCommercePsStoreIconPos>(pos)) == SCE_OK);
}

bool rlNpCommonDialog::HideCommerceLogo()
{
	return ( sceNpCommerceHidePsStoreIcon() == SCE_OK);	
}

#if RSG_PROSPERO
bool rlNpCommonDialog::ShowSigninDialog(const int localGamerIndex)
{
	if(!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		rlError("Invalid gamer index");
		return false;
	}

	if(SCE_OK != sceSysmoduleIsLoaded(SCE_SYSMODULE_SIGNIN_DIALOG))
	{
		rlError("SCE_SYSMODULE_SIGNIN_DIALOG is not loaded");
		return false;
	}

	int ret = PrepareDialog(DIALOG_SIGNIN);
	if(ret != SCE_OK)
	{
		rlError("ShowGameCustomData() failed. ret = 0x%x\n", ret);
		return false;
	}

	SceUserServiceUserId initialUserId = SCE_USER_SERVICE_USER_ID_INVALID;
	ret = sceUserServiceGetInitialUser(&initialUserId);
	if(ret != SCE_OK)
	{
		rlError("sceUserServiceGetInitialUser() failed. ret = 0x%x\n", ret);
		return false;
	}

	SceSigninDialogParam dialogParam;
	sceSigninDialogParamInitialize(&dialogParam);

	dialogParam.userId = initialUserId;
	ret = sceSigninDialogOpen(&dialogParam);
	if(ret < SCE_OK)
	{
		rlError("sceSigninDialogOpen : 0x%x\n", ret);
		m_bDialogShowing = false;
		return ret;
	}

	m_CurrentDialogType = DIALOG_SIGNIN;
	m_bDialogShowing = true;
	return true;
}

bool rlNpCommonDialog::ShowPlayerReviewDialog(const int localGamerIndex, const ePlayerReviewMode mode)
{
	rtry
	{
		const SceUserServiceUserId userId = g_rlNp.GetUserServiceId(localGamerIndex);
		rverify(userId != SCE_USER_SERVICE_USER_ID_INVALID, catchall, rlError("Invalid user id"));

		rlNpMatchId matchId;
		rverify(g_rlNp.GetWebAPI().GetMostRecentMatchId(localGamerIndex, matchId), catchall, rlError("No match stored"));
		sysGuidFixedString matchIdString = sysGuidUtil::GuidToFixedString(matchId, sysGuidUtil::kGuidStringFormatNonDecorated);

		rverify(sceSysmoduleIsLoaded(SCE_SYSMODULE_PLAYER_REVIEW_DIALOG) == SCE_OK, catchall, rlError("SCE_SYSMODULE_PLAYER_REVIEW_DIALOG is not loaded"));

		int ret = PrepareDialog(DIALOG_PLAYER_REVIEW);
		rcheck(ret == SCE_OK, catchall, rlError("ShowPlayerReviewDialog - PrepareDialog failed. ret = 0x%x", ret));

		ScePlayerReviewDialogParam param = {0};
		scePlayerReviewDialogParamInitialize(&param);
		param.userId = userId;
		safecpy(param.matchId, matchIdString.c_str());
		param.mode = mode == PLAYER_REVIEW_TEAM_ONLY ? SCE_PLAYER_REVIEW_MODE_TEAM_ONLY : SCE_PLAYER_REVIEW_MODE_ALL_PLAYERS;

		ret = scePlayerReviewDialogOpen(&param);
		rcheck(ret == SCE_OK, catchall, rlError("scePlayerReviewDialogOpen failed. ret = 0x%x", ret));

		m_CurrentDialogType = DIALOG_PLAYER_REVIEW;
		m_bDialogShowing = true;
		return true;
	}
	rcatchall
	{
	}

	return false;
}
#endif

void rlNpCommonDialog::SetScreenDimensions(const int width, const int height)
{
    // Cannot go beyond 1920x1080 (max width/height) - sony docs
    m_ScreenWidth = rage::Clamp(width, 0, NP_COMMON_DIALOG_MAX_WIDTH);
    m_ScreenHeight = rage::Clamp(height, 0, NP_COMMON_DIALOG_MAX_HEIGHT);

    rlDebug3("Update screen dimensions for fullscreen dialogs: %dx%d", m_ScreenWidth, m_ScreenHeight);
}

} // namespace rage

#endif // RSG_ORBIS
