// 
// rline/rlxbl.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLXBL_H
#define RLINE_RLXBL_H

#include "string/unicode.h"
#include "facebook/rlfacebookcommon.h"
#include "file/file_config.h"
#include "atl/inlist.h"
#include "net/status.h"
#include "rl.h"
#include "rlgamerinfo.h"
#include "rlmatchingattributes.h"
#include "rlsessioninfo.h"
#include "rltask.h"

#if __LIVE

namespace rage
{

#define RL_XBL_SOCIAL_NETWORK_ID_FACEBOOK         (2)

RAGE_DECLARE_SUBCHANNEL(rline, xbl)
RAGE_DECLARE_SUBCHANNEL(rline_xbl, tasks)

//PURPOSE
//  Structure used to hold results of XUID->gamertag (and vice versa) mapping services.
struct rlXblXuidGamertagPair
{
    u64 m_Xuid;
    char m_Gamertag[RL_MAX_NAME_BUF_SIZE];

    rlXblXuidGamertagPair()
    : m_Xuid(0)
    {
        m_Gamertag[0] = '\0';
    }
};

//PURPOSE
//  Holds info retrieved by rlXbl::GetPartyMembers()
struct rlXblPartyMemberInfo
{
    rlGamerHandle m_GamerHandle;
    char m_GamerName[RL_MAX_NAME_BUF_SIZE];
    unsigned m_TitleId;
    rlSessionInfo m_SessionInfo;    //Only valid if m_IsInSession is true;

    bool m_IsInSession : 1;
    bool m_IsTalking : 1;
    bool m_IsInPartyVoice : 1;
};

//PURPOSE
//  Interface to Xbox Live/GFWL matchmaking.
class rlXbl
{
public:
    //PURPOSE
    //  Returns the region for which the game was built.
    static rlGameRegion GetGameRegion();

    //PURPOSE
    //  Uses a GS service to do XUID->gamertag lookups.
    //NOTE
    //  Maximum number of gamertags that can be looked up is rlFriendsPage::MAX_FRIEND_PAGE_SIZE.
    /*static bool GetGamertagsFromXuids(rlXblXuidGamertagPair* pairs,
                                      const unsigned numPairs,
                                      netStatus* status);*/

    rlXbl();
    ~rlXbl();

    //PURPOSE
    //  Initialize the matching service.
    bool Init();

    //PURPOSE
    //  Shut down the matching service.
    void Shutdown();

    //PURPOSE
    //  Update the matching service.  Should be called once per frame.
    void Update();

    //PURPOSE
    //  Creates a new session as the host.
    //PARAMS
    //  localGamerIndex - Index of local gamer creating the session.
    //  netMode         - RL_NETMODE_ONLINE or RL_NETMODE_LAN.
    //  maxPubSlots     - Maximum number of public slots.
    //  maxPrivSlots    - Maximum number of private slots.
    //  attrs           - Matchmaking attributes.
    //  createFlags     - Session creation flags.  See rlSession::CreateFlags.
    //  presenceFlags   - Session creation flags.  See rlSession::PresenceFlags.
    //  hSession        - On successful completion will contain the session handle.
    //  arbCookie       - On successful completion will contain the arbitration cookie.
    //  sessionInfo     - On successful completion will contain new session info.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Sessions with no public slots are considered private sessions and
    //  will not be advertised on matchmaking.  Private sessions thus
    //  require no server resources.  Consider making sessions private when
    //  possible.  For example, invite-only sessions, such as those used
    //  for parties, should be private.
    //
    //  Matching attributes cannot be nil (unset) for sessions with public
    //  slots.  Private sessions are permitted to have nil attributes, but
    //  they must still specify a valid game mode and game type.
    //
    //  This is an asynchronous operation.  The handle, arbCookie, sessionInfo,
    //  and status parameters must remain valid until the operation completes.
    //  They must not be stack variables.
    bool HostSession(const int localGamerIndex,
                    const rlNetworkMode netMode,
                    const unsigned maxPubSlots,
                    const unsigned maxPrivSlots,
                    const rlMatchingAttributes& attrs,
                    const unsigned createFlags,
                    const unsigned presenceFlags,
                    void** hSession,
                    u64* arbCookie,
                    rlSessionInfo* sessionInfo,
                    netStatus* status);

    //PURPOSE
    //  Creates a new session as a guest.
    //PARAMS
    //  localGamerIndex - Index of local gamer creating the session.
    //  sessionInfo     - Info for an existing session hosted by someone else.
    //  netMode         - RL_NETMODE_ONLINE or RL_NETMODE_LAN.
    //  gameType        - RL_GAMETYPE_RANKED or RL_GAMETYPE_STANDARD.
    //  createFlags     - Session creation flags.  See rlSession::CreateFlags.
    //  presenceFlags   - Session creation flags.  See rlSession::PresenceFlags.
    //  maxPublicSlots  - Maximum public slots
    //  maxPrivateSlots - Maximum private slots
    //  hSession        - On successful completion will contain the session handle.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  This is an asynchronous operation.  The handle and status parameters
    //  must remain valid until the operation completes.  They must not be stack
    //  variables.
    bool CreateSession(const int localGamerIndex,
                        const rlSessionInfo& sessionInfo,
                        const rlNetworkMode netMode,
                        const rlGameType gameType,
                        const unsigned createFlags,
                        const unsigned presenceFlags,
                        const unsigned maxPublicSlots,
                        const unsigned maxPrivateSlots,
                        void** hSession,
                        netStatus* status);

    //PURPOSE
    //  Destroys a session.
    //PARAMS
    //  hSession        - Handle of an existing session.
    //  localGamerIndex - Local owner of the session.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool DestroySession(const int localGamerIndex,
                        const void* hSession,
                        netStatus* status);

    //PURPOSE
    //  Joins gamers to the local instance of a session.
    //PARAMS
    //  hSession        - Handle of an existing session.
    //  joiners         - Array of gamers to join.
    //  slotTypes       - Slot types in which they wish to join.
    //  numJoiners      - Number of joiners.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Call this function to join local as well as remote gamers to the
    //  local instance of the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool JoinSession(const void* hSession,
                    const rlGamerHandle* joiners,
                    const rlSlotType* slotTypes,
                    const unsigned numJoiners,
                    netStatus* status);

    //PURPOSE
    //  Leaves gamers from the local instance of a session.
    //PARAMS
    //  hSession        - Handle of an existing session.
    //  leavers         - Array of gamers to leave.
    //  numLeavers      - Number of leavers.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Call this function to leave local as well as remote gamers from the
    //  local instance of the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool LeaveSession(const void* hSession,
                    const rlGamerHandle* leavers,
                    const unsigned numLeavers,
                    netStatus* status);

    //PURPOSE
    //  Changes attributes on the local instance of the session.
    //PARAMS
    //  localGamerIndex - Index of local owner of the session (not necessarily the host).
    //  hSession        - Handle of an existing session.
    //  netMode         - Network mode used to create the session.
    //  newMaxPubSlots  - New maximum number of public slots.
    //  newMaxPrivSlots - New maximum number of private slots.
    //  newAttrs        - New attributes.
    //  createFlags     - Flags used to create the session.
    //  newPresenceFlags- New presence flags.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Attributes with nil values will not be changed on the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool ChangeSessionAttributes(const int localGamerIndex,
                                const void* hSession,
                                const rlNetworkMode netMode,
                                const unsigned newMaxPubSlots,
                                const unsigned newMaxPrivSlots,
                                const rlMatchingAttributes& newAttrs,
                                const unsigned createFlags,
                                const unsigned newPresenceFlags,
                                netStatus* status);

	//PURPOSE
	//  Changes user attributes using matching attributes
	//PARAMS
	//  localGamerIndex - Index of local owner of the session (not necessarily the host).
	//  attrs           - Attributes to apply.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  Attributes with nil values will not be changed on the session.
	//
	//  This is an asynchronous operation.  The status parameter must remain
	//  valid until the operation completes.  It must not be a stack variable.
	bool ApplyAttributes(const int localGamerIndex,
                         const rlMatchingAttributes& attrs,
                         netStatus* status);

    //PURPOSE
    //  Changes who is the session host.
    //PARAMS
    //  hSession        - Handle of an existing session.
    //  localHostIndex  - Index of local gamer that is to become the new host.
    //                    Must be -1 if the new host is remote.
    //  newSessionInfo  - Session info for the new session.  If a local
    //                    gamer is to be the new host then newSessionInfo
    //                    will be populated with info for the migrated session.
    //                    if a remote gamer is the new host then newSessionInfo
    //                    must contain session info received from that gamer.
    //  status          - Optional.  Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  The status and newSessionInfo
    //  parameters must remain valid until the operation completes.
    //  They must not be stack variables.
    bool MigrateHost(const void* hSession,
                    const int localHostIndex,
                    rlSessionInfo* newSessionInfo,
                    netStatus* status);

    //PURPOSE
    //  Invites gamers to the presence-enabled session in which the inviter
    //  is a member.
    //PARAMS
    //  localInviterIndex   - Local gamer index of inviter
    //  invitees        - Array of gamer handles specifying recipients.
    //  numInvitees     - Number of handles in the array.
    //  salutation      - String that can be included in the invitation.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool SendInvites(const int localInviterIndex,
                    const rlGamerHandle* invitees,
                    const unsigned numInvitees,
                    const char* salutation,
                    netStatus* status);

    //PURPOSE
    //  Invites gamers to the party session in which the inviter is a member.
    //PARAMS
    //  localInviterIndex   - Local gamer index of inviter
    //  status              - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool SendPartyInvites(const int localInviterIndex,
                        netStatus* status);

    //PURPOSE
    //  Returns number and status of all LIVE party members.
    //
    //  This can be used to determine if a party exists on the local console.
    //  If no results are returned, then there is no party.  If results are 
    //  returned, at least one of the party members will be a local gamer.
    //
    //  Title code should only need to call this after getting a "party
    //  changed" presence event, and can cache results between changes.
    //RETURNS
    //  False on error, true on success.   
    //NOTE
    //  LIVE parties can have up to eight members, so be sure that maxResults
    //  is >= that limit to get all the members.
    bool GetPartyMembers(rlXblPartyMemberInfo* results,
                         const unsigned maxResult,
                         unsigned* numResults);
    //TODO: Complete

    //PURPOSE
    //  Enables/disables QOS on the given session.
    //PARAMS
    //  sessionInfo     - Identifies the session.
    //  enabled         - True to enable QOS.
    //RETURNS
    //  True on success.
    bool SetQosEnabled(const rlSessionInfo& sessionInfo,
                        const bool enabled);

    //PURPOSE
    //  Set app-specific data returned to QoS queries.  This can be set at any
    //  time.  To reset, call with a null data pointer.
    //PARAMS
    //  sessionInfo - Identifies the session.
    //  data        - Pointer to data
    //  size        - Size of user data (max is RL_MAX_QOS_USER_DATA_SIZE)
    //RETURNS
    //  True in success, false on failure.
    bool SetQosUserData(const rlSessionInfo& sessionInfo,
                        const u8* data,
                        const u16 size);


    //PURPOSE
    //  Shows the UI for getting a social post user token (if applicable). The User's facebook token
	//  is returned upon success of the task. If the user already has permissions, an Xbox UI will still
	//  pop up briefly before disappearing.
    //PARAMS
	//
	//
    //RETURNS
    //  True in success, false on failure.
	bool ShowSocialGetUserTokenUI(const int localGamerIndex,
								  const int socialNetworkId,
								  const char* desiredPermissions,
								  char* tokenBuffer,
								  unsigned tokenBufferSize,
								  netStatus* status);

    //PURPOSE
    //  Shows the UI for getting a social post user token (if applicable). The User's facebook token
	//  is returned upon success of the task. If the user already has permissions, an Xbox UI will still
	//  pop up briefly before disappearing.
    //PARAMS
	//
	//
    //RETURNS
    //  True in success, false on failure.
	bool SocialGetCachedUserToken(const int localGamerIndex,
								  const int socialNetworkId,
								  const char* desiredPermissions,
								  char* tokenBuffer,
								  unsigned tokenBufferSize,
								  netStatus* status);

	//PURPOSE
	//  Cancels the operation associated with the status object.
	void Cancel(netStatus* status);
    
#if __DEV
    //Resets all the stats for a particular leaderboard ID.
    static bool ResetAllUsersStats(const unsigned long leaderboardId);
#endif

private:

    struct rlXblTask : public rlTask<rlXbl>
    {
        RL_TASK_USE_CHANNEL(rline_xbl_tasks);
    };

    struct ReadWriteAttrsTask : public rlXblTask
    {
        RL_TASK_DECL(ReadWriteAttrsTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_READ,
            STATE_WRITE,
            STATE_PENDING
        };

        ReadWriteAttrsTask();

        bool ConfigureForRead(rlXbl* ctx,
                            const int localGamerIndex,
                            rlMatchingAttributes* readAttrs,
                            netStatus* status);

        bool ConfigureForWrite(rlXbl* ctx,
                            const int localGamerIndex,
                            const rlMatchingAttributes& writeAttrs,
                            netStatus* status);

        //ConfigureForRead calls this
        bool Configure(rlXbl* ctx,
                        const int localGamerIndex,
                        rlMatchingAttributes* readAttrs);

        //ConfigureForWrite calls this
        bool Configure(rlXbl* ctx,
                        const int localGamerIndex,
                        const rlMatchingAttributes& writeAttrs);

        virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        bool WriteAttrs();

        bool ReadAttrs();

        int m_State;
        int m_LocalGamerIndex;
        rlMatchingAttributes* m_ReadAttrs;
        rlMatchingAttributes m_WriteAttrs;
        netStatus m_MyStatus[RL_MAX_MATCHING_ATTRS + 2];
        rlXovStatus m_XovStatus[RL_MAX_MATCHING_ATTRS + 2];

        XUSER_CONTEXT* m_ReadContexts;
        XUSER_PROPERTY* m_ReadProperties;
        unsigned* m_ReadPropertySizes;
        unsigned m_NumReadContexts;
        unsigned m_NumReadProperties;
    };

    struct CreateSessionTask : public rlXblTask
    {
        RL_TASK_DECL(CreateSessionTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_COMMIT_GAME_TYPE,
            STATE_COMMITTING_GAME_TYPE,
            STATE_CREATE_SESSION,
            STATE_CREATING_SESSION
        };

        CreateSessionTask();

        bool Configure(rlXbl* ctx,
                        const int localGamerIndex,
                        const rlSessionInfo* sessionInfo,
                        const rlNetworkMode netMode,
                        const rlGameType gameType,
                        const unsigned maxPubSlots,
                        const unsigned maxPrivSlots,
                        const unsigned createFlags,
                        const unsigned presenceFlags,
                        void** handle);

        virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        bool CreateSession();

        int m_LocalGamerIndex;
        rlNetworkMode m_NetMode;
        rlGameType m_GameType;
        unsigned m_MaxPubSlots;
        unsigned m_MaxPrivSlots;
        unsigned m_CreateFlags;
        unsigned m_PresenceFlags;
        rlSessionInfo m_SessionInfo;
        void** m_Handle;
        u64 m_ArbCookie;
        int m_State;
        netStatus m_MyStatus;
        rlXovStatus m_XovStatus;

        bool m_AsHost   : 1;
    };

    struct HostSessionTask : public rlXblTask
    {
        RL_TASK_DECL(HostSessionTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_GET_DEFAULT_ATTRIBUTES,
            STATE_GETTING_DEFAULT_ATTRIBUTES,
            STATE_COMMIT_ATTRIBUTES,
            STATE_COMMITTING_ATTRIBUTES,
            STATE_CREATE_SESSION,
            STATE_CREATING_SESSION
        };

        HostSessionTask();

        bool Configure(rlXbl* ctx,
                        const int localGamerIndex,
                        const rlNetworkMode netMode,
                        const unsigned maxPubSlots,
                        const unsigned maxPrivSlots,
                        const rlMatchingAttributes& attrs,
                        const unsigned createFlags,
                        const unsigned presenceFlags,
                        void** handle,
                        u64* arbCookie,
                        rlSessionInfo* sessionInfo);

        virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        int m_LocalGamerIndex;
        rlNetworkMode m_NetMode;
        unsigned m_MaxPubSlots;
        unsigned m_MaxPrivSlots;
        rlMatchingAttributes m_Attrs;
        rlMatchingAttributes m_DefaultAttrs;
        unsigned m_CreateFlags;
        unsigned m_PresenceFlags;
        void** m_Handle;
        u64* m_ArbCookie;
        rlSessionInfo* m_SessionInfo;
        int m_State;
        netStatus m_MyStatus;

        ReadWriteAttrsTask m_ReadWriteAttrsTask;
        CreateSessionTask m_CreateSessionTask;
    };

    struct JoinSessionTask : public rlXblTask
    {
        RL_TASK_DECL(JoinSessionTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_JOIN_LOCAL,
            STATE_JOINING_LOCAL,
            STATE_JOIN_REMOTE,
            STATE_JOINING_REMOTE,
        };

        JoinSessionTask();

        bool Configure(rlXbl* ctx,
                        const void* hSession,
                        const rlGamerHandle* gamerHandles,
                        const rlSlotType* slotTypes,
                        const unsigned numGamers);

		virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        const void* m_hSession;
        int m_LocalGamerIndexes[RL_MAX_LOCAL_GAMERS];
        u64 m_RemoteGamerHandles[RL_MAX_GAMERS_PER_SESSION - 1];
        int m_LocalPrivSlots[RL_MAX_LOCAL_GAMERS];
        int m_RemotePrivSlots[RL_MAX_GAMERS_PER_SESSION - 1];
        unsigned m_NumLocalGamers;
        unsigned m_NumRemoteGamers;
        int m_State;
        netStatus m_MyStatus;
        rlXovStatus m_XovStatus;
    };

    struct LeaveSessionTask : public rlXblTask
    {
        RL_TASK_DECL(LeaveSessionTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_LEAVE_LOCAL,
            STATE_LEAVING_LOCAL,
            STATE_LEAVE_REMOTE,
            STATE_LEAVING_REMOTE,
        };

        LeaveSessionTask();

        bool Configure(rlXbl* ctx,
                        const void* hSession,
                        const rlGamerHandle* gamerHandles,
                        const unsigned numGamers);

		virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        const void* m_hSession;
        int m_LocalGamerIndexes[RL_MAX_LOCAL_GAMERS];
        u64 m_RemoteGamerHandles[RL_MAX_GAMERS_PER_SESSION - 1];
        unsigned m_NumLocalGamers;
        unsigned m_NumRemoteGamers;
        int m_State;
        netStatus m_MyStatus;
        rlXovStatus m_XovStatus;
    };

    struct DestroySessionTask : public rlXblTask
    {
        RL_TASK_DECL(DestroySessionTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_DESTROY,
            STATE_DESTROYING,
        };

        DestroySessionTask();

        bool Configure(rlXbl* ctx,
                        const int localGamerIndex,
                        const void* hSession);

		virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const;

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        int m_LocalGamerIndex;
        const void* m_hSession;
        int m_State;
        netStatus m_MyStatus;
        rlXovStatus m_XovStatus;
    };

    struct ChangeAttributesTask : public rlXblTask
    {
        RL_TASK_DECL(ChangeAttributesTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_COMMIT_ATTRIBUTES,
            STATE_COMMITTING_ATTRIBUTES,
            STATE_MODIFY_SESSION,
            STATE_MODIFYING_SESSION
        };

        ChangeAttributesTask();

        bool Configure(rlXbl* ctx,
                        const int localGamerIndex,
                        const void* hSession,
                        const rlNetworkMode netMode,
                        const unsigned maxPubSlots,
                        const unsigned maxPrivSlots,
                        const rlMatchingAttributes& attrs,
                        const unsigned createFlags,
                        const unsigned presenceFlags);

		virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        bool ModifySession();

        int m_LocalGamerIndex;
        const void* m_hSession;
        rlNetworkMode m_NetMode;
        unsigned m_MaxPubSlots;
        unsigned m_MaxPrivSlots;
        rlMatchingAttributes m_Attrs;
        unsigned m_CreateFlags;
        unsigned m_PresenceFlags;
        int m_State;
		netStatus m_MyStatus;
        rlXovStatus m_XovStatus;

        ReadWriteAttrsTask m_ReadWriteAttrsTask;
    };

    struct MigrateHostTask : public rlXblTask
    {
        RL_TASK_DECL(MigrateHostTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_MIGRATE,
            STATE_MIGRATING,
        };

        MigrateHostTask();

        bool Configure(rlXbl* ctx,
                        const void* hSession,
                        const int localGamerIndex,
                        rlSessionInfo* newSessionInfo);

		virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        bool MigrateHost();

        const void* m_hSession;
        int m_LocalHostIndex;
        rlSessionInfo* m_NewSessionInfo;
        int m_State;
        netStatus m_MyStatus;
        rlXovStatus m_XovStatus;
    };

    struct SendInvitesTask : public rlXblTask
    {
        RL_TASK_DECL(SendInvitesTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_SEND_INVITES,
            STATE_SENDING_INVITES,
        };

        SendInvitesTask();

        bool Configure(rlXbl* ctx,
                        const int localInviterIndex,
                        const rlGamerHandle* invitees,
                        const unsigned numInvitees,
                        const char* salutation);

		virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        bool SendInvites();

        int m_LocalInviterIndex;
        u64 m_InviteeIds[RL_MAX_GAMERS_PER_SESSION - 1];
        unsigned m_NumInvitees;
        char m_Salutation[RL_MAX_INVITE_SALUTATION_CHARS];
        int m_State;
        netStatus m_MyStatus;
        rlXovStatus m_XovStatus;
    };

    struct SendPartyInvitesTask : public rlXblTask
    {
        RL_TASK_DECL(SendPartyInvitesTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_SEND_INVITES,
            STATE_SENDING_INVITES,
        };

        SendPartyInvitesTask();

        bool Configure(rlXbl* ctx,
            const int localInviterIndex);

		virtual void Start();

		virtual void DoCancel();
		virtual bool IsCancelable() const { return true; }

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

        bool SendPartyInvites();

        int m_LocalInviterIndex;
        int m_State;
        netStatus m_MyStatus;
        rlXovStatus m_XovStatus;
    };

	struct SocialGetUserTokenTask : public rlXblTask
	{
		RL_TASK_DECL(SocialGetUserTokenUITask);

		enum State
		{
			STATE_INVALID = -1,
			STATE_SHOW_GETUSERTOKEN_UI,
			STATE_SHOWING_GETUSERTOKEN_UI,
		};

		SocialGetUserTokenTask();

		bool Configure(const int localGamerIndex,
					   const int socialNetworkId,
					   const char* desiredPermissions,
					   const bool showUi,
					   char* tokenBuffer,
					   unsigned tokenBufferSize);

		virtual void Start();

		virtual void DoCancel();
		
		virtual void Finish(const FinishType finishType, const int resultCode = 0);

		virtual void Update(const unsigned timeStep);

		u32 GetUserToken();

		char m_DesiredPermissions[RL_FACEBOOK_PERMISSIONS_LENGTH_MAX];
		netStatus m_MyStatus;
		rlXovStatus m_XovStatus;

		char* m_TokenBuffer;
		u32 m_TokenBufferSize;
		int m_LocalGamerIndex;
		int m_SocialNetworkId;
		int m_State;

		char16 m_TokenBufferWide[RL_FACEBOOK_TOKEN_MAX_LENGTH];

		bool m_ShowUi : 1;
	};
};

//Global instance
extern rlXbl g_rlXbl;

}   //namespace rage

#endif  //__LIVE

#endif  //RLINE_RLXBL_H
