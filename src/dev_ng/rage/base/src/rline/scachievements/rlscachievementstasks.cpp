// 
// rline/rlscachievementstask.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscachievementstasks.h"
#include "data/base64.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/device.h"
#include "parser/manager.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroscommon.h"

namespace rage
{

#define ERROR_CASE(code, codeEx, resCode) \
    if(result.IsError(code, codeEx)) \
    { \
        resultCode = resCode; \
        return; \
    }

//Helper for reading achievement XML nodes.
static bool ReadAchievement(const parTreeNode* node, rlScAchievementInfo* a)
{
    rtry
    {
        int id;
        rverify(rlHttpTaskHelper::ReadInt(id, node, NULL, "id"), catchall, );

        s64 posixTime;
        rverify(rlHttpTaskHelper::ReadInt64(posixTime, node, NULL, "p"), catchall, );

        int offline;
        rverify(rlHttpTaskHelper::ReadInt(offline, node, NULL, "o"), catchall, );

        a->m_Id = id;
        a->m_AwardedOffline = (offline != 0);
        a->m_AwardedTime = posixTime;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScAchievementsAwardAchievementTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScAchievementsAwardAchievementTask::Configure(const int localGamerIndex,
                                                const int achievementId,
                                                rlScAchievementInfo* achievement)
{
    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("achievementId", achievementId), catchall, );
        rverify(AddStringParameter("achievedOffline", NULL, true), catchall, );

        m_Achievement = achievement;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScAchievementsAwardAchievementTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                                     const parTreeNode* node, 
                                                     int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        if (m_Achievement)
        {
            const parTreeNode* result = node->FindChildWithName("Result");
            rverify(result, catchall, );
            rverify(ReadAchievement(result, m_Achievement), catchall, );
        }

        return true;
    }
    rcatchall
    {
        resultCode = -1;
        return false;
    }
}

void
rlScAchievementsAwardAchievementTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);
    ERROR_CASE("DoesNotExist", "Achievement", RLSC_ERROR_DOESNOTEXIST_ACHIEVEMENT);
    ERROR_CASE("DoesNotExist", "PlayerAccount", RLSC_ERROR_DOESNOTEXIST_PLAYERACCOUNT);
    ERROR_CASE("DoesNotExist", "RockstarAccount", RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT);

    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScAchievementsGetAchievementsTask
///////////////////////////////////////////////////////////////////////////////
rlScAchievementsGetAchievementsTask::rlScAchievementsGetAchievementsTask()
{
}

rlScAchievementsGetAchievementsTask::~rlScAchievementsGetAchievementsTask()
{
}

bool 
rlScAchievementsGetAchievementsTask::Configure(const int localGamerIndex,
                                               const PlayerAccountId accountId,
                                               const char* userId,
                                               const RockstarId rockstarId,
                                               rlScAchievementInfo* achievements,
                                               const int firstAchievementId,
                                               const unsigned maxAchievements,
                                               unsigned* count,
                                               unsigned* total)
{
    rtry
    { 
        rverify(maxAchievements || total, 
                catchall, 
                rlTaskError("maxAchievements and/or total must be specified"));

        if (maxAchievements)
        {
            rverify(achievements && count, 
                    catchall, 
                    rlTaskError("maxAchievements is > 0, but achievements or count are NULL"));
        }

        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("accountId", accountId), catchall, );
        rverify(AddStringParameter("userId", userId, true), catchall, );
        rverify(AddIntParameter("rockstarId", rockstarId), catchall, );
        rverify(AddIntParameter("firstAchievementId", firstAchievementId), catchall, );
        rverify(AddIntParameter("maxAchievements", maxAchievements), catchall, );

        m_Achievements = achievements;
        m_Count = count;
        m_Total = total;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScAchievementsGetAchievementsTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                                    const parTreeNode* node, 
                                                    int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* result = node->FindChildWithName("Result");
        rverify(result, catchall, );

        //Get count and total attributes
        int val;
        if (m_Count) 
        {
            rverify(rlHttpTaskHelper::ReadInt(val, result, NULL, "Count"), catchall, );
            *m_Count = (unsigned)val;
        }

        if (m_Total)
        {
            rverify(rlHttpTaskHelper::ReadInt(val, result, NULL, "Total"), catchall, );
            *m_Total = (unsigned)val;
        }

        //Read achievements
        if (m_Achievements)
        {
            int index = 0;
            parTreeNode::ChildNodeIterator iter = result->BeginChildren();
            while(iter != result->EndChildren())
            {
                parTreeNode* childNode = *iter;
                rlAssert(childNode);

                if(!stricmp(childNode->GetElement().GetName(), "a"))
                {
                    rverify(ReadAchievement(childNode, &m_Achievements[index]), catchall, );
                    ++index;
                }

                ++iter;
            }
        }

        return true;
    }
    rcatchall
    {
        resultCode = -1;
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScAchievementsGetAchievementDefinitionsTask
///////////////////////////////////////////////////////////////////////////////
rlScAchievementsGetAchievementDefinitionsTask::rlScAchievementsGetAchievementDefinitionsTask()
{
}

rlScAchievementsGetAchievementDefinitionsTask::~rlScAchievementsGetAchievementDefinitionsTask()
{
}

bool 
rlScAchievementsGetAchievementDefinitionsTask::Configure(const int localGamerIndex,
                                                         rlScAchievementDefinitionInfo* definitions,
                                                         const int firstAchievementId,
                                                         const unsigned maxDefinitions,
                                                         unsigned* count,
                                                         unsigned* total)
{
    rtry
    { 
        rverify(maxDefinitions || total, 
                catchall, 
                rlTaskError("maxDefintions and/or total must be specified"));

        if (maxDefinitions)
        {
            rverify(definitions && count, 
                    catchall, 
                    rlTaskError("maxDefinitions is > 0, but defintions or count are NULL"));
        }

        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("firstAchievementId", firstAchievementId), catchall, );
        rverify(AddIntParameter("maxDefinitions", maxDefinitions), catchall, );
        rverify(AddStringParameter("locale", "en-US", true), catchall, );

        m_Definitions = definitions;
        m_Count = count;
        m_Total = total;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScAchievementsGetAchievementDefinitionsTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                                              const parTreeNode* node, 
                                                              int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* result = node->FindChildWithName("Result");
        rverify(result, catchall, );

        //Get count and total attributes
        int val;
        
        if (m_Count) 
        {
            rverify(rlHttpTaskHelper::ReadInt(val, result, NULL, "Count"), catchall, );
            *m_Count = (unsigned)val;
        }

        if (m_Total)
        {
            rverify(rlHttpTaskHelper::ReadInt(val, result, NULL, "Total"), catchall, );
            *m_Total = (unsigned)val;
        }

        //Read definitions
        if (m_Definitions)
        {
            int index = 0;
            parTreeNode::ChildNodeIterator iter = result->BeginChildren();
            while(iter != result->EndChildren())
            {
                parTreeNode* childNode = *iter;
                rlAssert(childNode);

                if(!stricmp(childNode->GetElement().GetName(), "a"))
                {
                    int id;
                    rverify(rlHttpTaskHelper::ReadInt(id, childNode, NULL, "id"), catchall, );

                    int points;
                    rverify(rlHttpTaskHelper::ReadInt(points, childNode, NULL, "p"), catchall, );

                    int hidden;
                    rverify(rlHttpTaskHelper::ReadInt(hidden, childNode, NULL, "h"), catchall, );

                    const char* name = rlHttpTaskHelper::ReadString(childNode, "n", NULL);
                    rverify(name, catchall, );

                    const char* desc = rlHttpTaskHelper::ReadString(childNode, "d", NULL);
                    rverify(desc, catchall, );

                    const char* howTo = rlHttpTaskHelper::ReadString(childNode, "h", NULL);
                    rverify(howTo, catchall, );

                    const char* imageUrl = rlHttpTaskHelper::ReadString(childNode, "i", NULL);
                    rverify(imageUrl, catchall, );

                    rverify(m_Definitions[index].Reset(id, 
                                                       points, 
                                                       hidden != 0,
                                                       name,
                                                       desc,
                                                       howTo,
                                                       imageUrl),
                            catchall,
                            );

                    ++index;
                }

                ++iter;
            }
        }

        return true;
    }
    rcatchall
    {
        resultCode = -1;
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScAchievementsSynchronizeTask
///////////////////////////////////////////////////////////////////////////////
rlScAchievementsSynchronizeTask::rlScAchievementsSynchronizeTask()
: m_B64Submission(0)
, m_LocalGamerIndex(-1)
{
}

rlScAchievementsSynchronizeTask::~rlScAchievementsSynchronizeTask()
{
    if (m_B64Submission)
    {
        RL_FREE(m_B64Submission);
        m_B64Submission = 0;
    }
}

bool
rlScAchievementsSynchronizeTask::Configure(const int localGamerIndex,
                                           const rlScAchievementInfo* achievements,
                                           const unsigned numAchievements)
{
    u8* submission = 0;
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), 
                catchall, 
                rlTaskError("Failed to configure base class"));

		m_LocalGamerIndex = localGamerIndex;

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        //Allocate buffers
        //  v0 = {u8 Format} + {int32 AchievementIds}
        //  v1 = {u8 Format} + {u8 BitArrayByteSize} + {BitArrayBytes}        
        int maxId = 0;
        for(unsigned i = 0; i < numAchievements; i++)
        {
            const rlScAchievementInfo& a = achievements[i];
            if (a.m_Id > maxId) maxId = a.m_Id;
        }

        rverify(maxId < RLSCACHIEVEMENTS_MAX_ACHIEVEMENTS,
                catchall,
                rlTaskError("Max achievement ID (%d) is beyond valid range (%d); capping", maxId, RLSCACHIEVEMENTS_MAX_ACHIEVEMENTS));

        u8 bitArrayByteSize = (u8)((maxId / 8) + 1);
        u8 submissionByteSize = 1 +                 //Format
                                1 +                 //Bit array byte size
                                bitArrayByteSize;   //Bit array bytes

        submission = (u8*)RL_ALLOCATE(rlScAchievementsSynchronizeTask, submissionByteSize);
        rverify(submission, 
                catchall, 
                rlTaskError("Failed to allocate %d for submission buffer", submissionByteSize));
        sysMemSet(submission, 0, submissionByteSize);

        rlAssert(!m_B64Submission);
        unsigned maxBase64Size = datBase64::GetMaxEncodedSize(submissionByteSize);        
        m_B64Submission = (char*)RL_ALLOCATE(rlScAchievementsSynchronizeTask, maxBase64Size);
        rverify(m_B64Submission, 
                catchall, 
                rlTaskError("Failed to allocate %d for b64 buffer", maxBase64Size));

        //Serialize the format byte, and all achieved achievements.
        unsigned offset = 0;
        submission[offset++] = 1;  //Achievements.SynchronizeFormat.v1 on C# side
        submission[offset++] = bitArrayByteSize;

        for(unsigned i = 0; i < numAchievements; i++)
        {
            const rlScAchievementInfo& a = achievements[i];
            
            int byteIndex = (a.m_Id / 8) + offset;
            int bitIndex = a.m_Id % 8;
            submission[byteIndex] |= (u8)(1 << bitIndex);
        }

        //Convert to base64
        unsigned charsUsed;
        rcheck(datBase64::Encode(submission, submissionByteSize, m_B64Submission, maxBase64Size, &charsUsed), 
              catchall, 
              rlTaskError("Failed to convert achievements to base64"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add ticket param"));
        rverify(AddStringParameter("submission", m_B64Submission, true), catchall, rlTaskError("Failed to add achievements param"));

        success = true;
    }
    rcatchall
    {
        success = false;
    }

    if (submission)
    {
        RL_FREE(submission);
        submission = 0;
    }

    return success;
}

void
rlScAchievementsSynchronizeTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);
    ERROR_CASE("DoesNotExist", "Achievement", RLSC_ERROR_DOESNOTEXIST_ACHIEVEMENT);
    ERROR_CASE("DoesNotExist", "PlayerAccount", RLSC_ERROR_DOESNOTEXIST_PLAYERACCOUNT);
    ERROR_CASE("DoesNotExist", "RockstarAccount", RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT);

    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;

	rlRosEventCanReadAchievements e(m_LocalGamerIndex, false);
	rlRos::DispatchEvent(e);
}

bool
rlScAchievementsSynchronizeTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
	resultCode = 0;

	rlRosEventCanReadAchievements e(m_LocalGamerIndex, true);
	rlRos::DispatchEvent(e);

	return true;
}


}; //namespace rage
