// 
// rline/rlscachievements.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscachievements.h"
#include "rlscachievementstasks.h"
#include "atl/bitset.h"
#include "diag/seh.h"
#include "net/status.h"
#include "rline/rlachievement.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroscommon.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "string/string.h"
#include "system/timer.h"

namespace rage
{

static atBitSet s_SynchedAchievementIds[RL_MAX_LOCAL_GAMERS];
static rlRos::Delegate s_RosDlgt;
static bool s_RlScAchievementsInitialized = false;
static sysCriticalSectionToken s_RlScAchievementsCs;

RAGE_DEFINE_SUBCHANNEL(rline, scachievements);
#undef __rage_channel
#define __rage_channel rline_scachievements

////////////////////////////////////////////////////////////////////////////////
// rlScAchievementInfo
////////////////////////////////////////////////////////////////////////////////
rlScAchievementInfo::rlScAchievementInfo()
: m_Id(0)
{
}

////////////////////////////////////////////////////////////////////////////////
// rlScAchievementDefinitionInfo
////////////////////////////////////////////////////////////////////////////////
bool 
rlScAchievementDefinitionInfo::Reset(int id,
                                     int points,
                                     bool hidden,
                                     const char* name,
                                     const char* desc,
                                     const char* howTo,
                                     const char* imageUrl)
{
    m_Id = id;
    m_Points = points;
    m_Hidden = hidden;

    safecpy(m_Name, name);
    safecpy(m_Description, desc);
    safecpy(m_HowTo, howTo);
    safecpy(m_ImageUrl, imageUrl);

    return true;
}

////////////////////////////////////////////////////////////////////////////////
// rlScAchievements
////////////////////////////////////////////////////////////////////////i////////
static void OnRosEvent(const rlRosEvent& evt)
{
    if(RLROS_EVENT_ONLINE_STATUS_CHANGED == evt.GetId())
    {
        SYS_CS_SYNC(s_RlScAchievementsCs);

        //Clear the synchronized IDs for the local gamer.
        const rlRosEventOnlineStatusChanged* e = 
            (const rlRosEventOnlineStatusChanged*)(&evt);

        //rlDebug3("rlScAchievements::OnRosEvent: Gamer %d status changed, clearing synched ids", 
        //        e->m_LocalGamerIndex);

        s_SynchedAchievementIds[e->m_LocalGamerIndex].Reset();
    }
}

bool 
rlScAchievements::InitClass()
{
    if (rlVerify(!s_RlScAchievementsInitialized))
    {
        s_RosDlgt.Bind(OnRosEvent);
        rlRos::AddDelegate(&s_RosDlgt);

        for(unsigned i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            s_SynchedAchievementIds[i].Init(RLSCACHIEVEMENTS_MAX_ACHIEVEMENTS);
        }

        return true;
    }
    return false;
}

void 
rlScAchievements::ShutdownClass()
{
    if (s_RlScAchievementsInitialized)
    {
        rlRos::RemoveDelegate(&s_RosDlgt);

        for(unsigned i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            s_SynchedAchievementIds[i].Reset();
        }

        s_RlScAchievementsInitialized = false;
    }
}

void
rlScAchievements::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//Convenience macro to create and queue Social Club tasks
#define RLSCACHIEVEMENTS_CREATETASK(T, status, localGamerIndex, ...) 	                    \
    bool success = false;																	\
    rlTaskBase* task = NULL;                                                                \
    rtry																					\
    {																						\
        rcheck(rlRos::IsOnline(localGamerIndex), catchall, rlDebug("Not signed into ROS")); \
        if (status)                                                                         \
        {                                                                                   \
            T* nonfafTask = rlGetTaskManager()->CreateTask<T>();		                    \
            rverify(nonfafTask,catchall,);													\
            task = nonfafTask;                                                              \
            rverify(rlTaskBase::Configure(nonfafTask, localGamerIndex, __VA_ARGS__, status),\
                    catchall, rlError("Failed to configure task"));                         \
        }                                                                                   \
        else                                                                                \
        {                                                                                   \
            rlFireAndForgetTask<T>* fafTask = NULL;											\
            rverify(rlGetTaskManager()->CreateTask(&fafTask), catchall, );  				\
            task = fafTask;                                                                 \
            rverify(rlTaskBase::Configure(fafTask, localGamerIndex, __VA_ARGS__, &fafTask->m_Status), \
                    catchall, rlError("Failed to configure fire-and-forget task"));         \
        }                                                                                   \
        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, );         			\
        success = true;	                                                                    \
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
            rlGetTaskManager()->DestroyTask(task);								            \
        }																					\
    }																						\
    return success;

bool 
rlScAchievements::AwardAchievement(const int localGamerIndex,
                                   const int achievementId,
                                   rlScAchievementInfo* achievement,
                                   netStatus* status)
{
    RLSCACHIEVEMENTS_CREATETASK(rlScAchievementsAwardAchievementTask,
        status,
        localGamerIndex,
        achievementId,
        achievement);
}

bool 
rlScAchievements::GetAchievementDefinitions(const int localGamerIndex,
                                            rlScAchievementDefinitionInfo* definitions,
                                            const int firstAchievementId,
                                            const unsigned maxDefinitions,
                                            unsigned* count,
                                            unsigned* total,
                                            netStatus* status)
{
    RLSCACHIEVEMENTS_CREATETASK(rlScAchievementsGetAchievementDefinitionsTask,
        status,
        localGamerIndex,
        definitions,
        firstAchievementId,
        maxDefinitions,
        count,
        total);
}

bool
rlScAchievements::GetAchievementsByGamer(const int localGamerIndex,
                                         const rlGamerHandle& gamerHandle,
                                         rlScAchievementInfo* achievements,
                                         const int firstAchievementId,
                                         const unsigned maxAchievements,
                                         unsigned* count,
                                         unsigned* total,
                                         netStatus* status)
{
    char userId[RLROS_MAX_USERID_SIZE];
    userId[0] = '\0';

    //For remote gamers we get their UserID.
    if (-1 == rlPresence::GetLocalIndex(gamerHandle))
    {
        if (!rlRosUserId::FromGamerHandle(gamerHandle, userId, sizeof(userId)))
        {
            rlError("Failed to convert gamerhandle to ROS user ID");
            return false;
        }
    }

    RLSCACHIEVEMENTS_CREATETASK(rlScAchievementsGetAchievementsTask,
        status,
        localGamerIndex,
        InvalidPlayerAccountId,
        userId,
        InvalidRockstarId,
        achievements,
        firstAchievementId,
        maxAchievements,
        count,
        total);
}

bool 
rlScAchievements::Synchronize(const int localGamerIndex,
                              const rlAchievementInfo* achievements,
                              const unsigned numAchievements,
                              netStatus* status)
{
    if (rlVerify(achievements && numAchievements))
    {
        //Only synchronize achievements that are achieved, and have not already
        //been synchronized for this gamer.
        rlScAchievementInfo* scAchievements = 
            (rlScAchievementInfo*)alloca(sizeof(rlScAchievementInfo) * numAchievements);

        if (rlVerify(scAchievements))
        {
            SYS_CS_SYNC(s_RlScAchievementsCs);

            unsigned numScAchievements = 0;

            for(unsigned i = 0; i < numAchievements; i++)
            {
                const rlAchievementInfo& a = achievements[i];
                if (a.IsAchieved())
                {
                    if (!s_SynchedAchievementIds[localGamerIndex].IsSet((unsigned)a.GetId()))
                    {
                        scAchievements[numScAchievements].m_Id = a.GetId();
                        scAchievements[numScAchievements].m_AwardedOffline = true;
                        scAchievements[numScAchievements].m_AwardedTime = 0;

                        ++numScAchievements;

                        s_SynchedAchievementIds[localGamerIndex].Set((unsigned)a.GetId(), true);
                    }
                    else
                    {
                        //rlDebug3("Achievement %d was already synched", a.GetId());
                    }
                }
            }

            if (numScAchievements > 0)
            {
                RLSCACHIEVEMENTS_CREATETASK(rlScAchievementsSynchronizeTask,
                    status,
                    localGamerIndex,
                    scAchievements,
                    numScAchievements);
            }
            else
            {
				//Lets trigger event as if rlScAchievementsSynchronizeTask is done.
				rlRosEventCanReadAchievements e(localGamerIndex, true);
				rlRos::DispatchEvent(e);

                if (status) status->ForceSucceeded();
                return true;                
            }
        }
    }

    return false;
}

}   //namespace rage
