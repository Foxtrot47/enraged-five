// 
// rline/rlScAchievementsTask.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCACHIEVEMENTSTASK_H
#define RLINE_RLSCACHIEVEMENTSTASK_H

#include "rlscachievements.h"
#include "atl/bitset.h"
#include "rline/ros/rlroshttptask.h"

namespace rage
{

class parTree;
class parTreeNode;

///////////////////////////////////////////////////////////////////////////////
//  rlScAchievementsAwardAchievementTask
///////////////////////////////////////////////////////////////////////////////
class rlScAchievementsAwardAchievementTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScAchievementsAwardAchievementTask);

    rlScAchievementsAwardAchievementTask() {}
    virtual ~rlScAchievementsAwardAchievementTask() {}

    bool Configure(const int localGamerIndex,
                   const int achievementId,
                   rlScAchievementInfo* achievement);

protected:
    virtual const char* GetServiceMethod() const { return "achievements.asmx/AwardAchievement"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlScAchievementInfo* m_Achievement;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScAchievementsGetAchievementDefinitionsTask
///////////////////////////////////////////////////////////////////////////////
class rlScAchievementsGetAchievementDefinitionsTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScAchievementsGetAchievementDefinitionsTask);
    RL_TASK_USE_CHANNEL(rline_scachievements);

    rlScAchievementsGetAchievementDefinitionsTask();
    virtual ~rlScAchievementsGetAchievementDefinitionsTask();

    bool Configure(const int localGamerIndex,
                   rlScAchievementDefinitionInfo* definitions,
                   const int firstAchievementId,
                   const unsigned maxDefinitions,
                   unsigned* count,
                   unsigned* total);

protected:
    virtual const char* GetServiceMethod() const { return "Achievements.asmx/GetAchievementDefinitions"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlScAchievementDefinitionInfo* m_Definitions;
    unsigned* m_Count;
    unsigned* m_Total;
};

/////////////////////////////////////////////////////////////////////////////////
////  rlScAchievementsGetAchievementsTask
/////////////////////////////////////////////////////////////////////////////////
class rlScAchievementsGetAchievementsTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScAchievementsGetAchievementsTask);
    RL_TASK_USE_CHANNEL(rline_scachievements);

    rlScAchievementsGetAchievementsTask();
    virtual ~rlScAchievementsGetAchievementsTask();

    bool Configure(const int localGamerIndex,
                   const PlayerAccountId accountId,
                   const char* userId,
                   const RockstarId rockstarId,
                   rlScAchievementInfo* achievements,
                   const int firstAchievementId,
                   const unsigned maxAchievements,
                   unsigned* count,
                   unsigned* total);

protected:
    virtual const char* GetServiceMethod() const { return "Achievements.asmx/GetAchievements"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlScAchievementInfo* m_Achievements;
    unsigned* m_Count;
    unsigned* m_Total;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScAchievementsSynchronizeTask
///////////////////////////////////////////////////////////////////////////////
class rlScAchievementsSynchronizeTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScAchievementsSynchronizeTask);
    RL_TASK_USE_CHANNEL(rline_scachievements);

    rlScAchievementsSynchronizeTask();
    virtual ~rlScAchievementsSynchronizeTask();

    bool Configure(const int localGamerIndex,
                   const rlScAchievementInfo* achievements,
                   const unsigned numAchievements);

protected:
    virtual const char* GetServiceMethod() const { return "achievements.asmx/Synchronize"; }
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    char* m_B64Submission;
	int m_LocalGamerIndex;
};

} //namespace rage

#endif  //RLINE_RLSCACHIEVEMENTSTASK_H
