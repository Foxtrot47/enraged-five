// 
// rline/rlscachievements.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCACHIEVEMENTS_H
#define RLINE_RLSCACHIEVEMENTS_H

#include "rline/rl.h"
#include "rline/rldiag.h"
#include "rline/rlgamerinfo.h"

namespace rage
{

class netStatus;
class rlAchievementInfo;

RAGE_DECLARE_SUBCHANNEL(rline, scachievements);

enum
{
    //Maximum number of achievements tracked
    RLSCACHIEVEMENTS_MAX_ACHIEVEMENTS = 256,
    
    //Size limits.  For string, these include any terminating nulls.
    RLSCACHIEVEMENTS_MAX_NAME_SIZE = 128,       //Unicode string
    RLSCACHIEVEMENTS_MAX_DESC_SIZE = 256,       //Unicode string
    RLSCACHIEVEMENTS_MAX_HOW_TO_SIZE = 256,     //Unicode string
    RLSCACHIEVEMENTS_MAX_IMAGE_URL_SIZE = 128,
};

//PURPOSE
//  Describes the status of an achievement relative to a gamer.
class rlScAchievementInfo
{
public:
    rlScAchievementInfo();

    //Unique achievement ID.
    int m_Id;

    //True if the achievement was awarded while offline.
    bool m_AwardedOffline;

    //Posix UTC time the achievement was awarded.  Note that if the 
    //achievement was awarded offline, this will be the time it was
    //reported to the backend, and not the actual time it was 
    //achieved.
    time_t m_AwardedTime;
};

//PURPOSE
//  Static data associated with an achievement.
class rlScAchievementDefinitionInfo
{
public:   
    //Unique acheivement ID
    int m_Id;

    //Number of points this achievement is worth
    int m_Points;

    //Display name of the achievement
    char m_Name[RLSCACHIEVEMENTS_MAX_NAME_SIZE];        

    //Achievement description
    char m_Description[RLSCACHIEVEMENTS_MAX_DESC_SIZE];

    //Instructions on how to achieve
    char m_HowTo[RLSCACHIEVEMENTS_MAX_HOW_TO_SIZE];

    //The achievement's image URL
    char m_ImageUrl[RLSCACHIEVEMENTS_MAX_IMAGE_URL_SIZE];

    //If true, achievement should not be shown until player has achieved it
    bool m_Hidden : 1;

    bool Reset(int id,
               int points,
               bool hidden,
               const char* name,
               const char* desc,
               const char* howTo,
               const char* imageUrl);
};

//PURPOSE
//  API for reading and awarding achievements tracked by ROS.
class rlScAchievements
{
public:
    //PURPOSE
    //  Init/Update/Shutdown.  Users do not need to call these; they are
    //  called automatically by rlInit and rlShutdown.
    static bool InitClass();
    static void ShutdownClass();

    //PURPOSE
    //  Cancels the task that the specified status object is monitoring.
    static void Cancel(netStatus* status);

    //PURPOSE
    //  Awards an achievement to a local gamer.  This should ONLY be called
    //  at the moment it is earned.  The backend will timestamp the achievement
    //  and return the result.
    //
    //  For non-SC platforms, this is automatically called after each successful
    //  rlAchievement::Write() call, so title code shouldn't need to call this.
    //
    //PARAMS
    //  localGamerIndex     - Gamer to award achievement to
    //  achievementId       - Achievement to award
    //  achievement         - (OPTIONAL) If non-null, will contain resulting awarded achievement
    //RETURNS
    //  False if failed to start request.  This most likely indicates !rlRos::IsOnline().
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    static bool AwardAchievement(const int localGamerIndex,
                                 const int achievementId,
                                 rlScAchievementInfo* achievement,
                                 netStatus* status);

    //PURPOSE
    //  Reads achievement definitions from the ROS backend, and the total number
    //  of achievements defined for the title.
    //PARAMS
    //  localGamerIndex     - Gamer doing the reading
    //  definitions         - (Optional) Array to store achievements definitions in.
    //                        If only the total is desired this can be null.
    //  firstAchievementId  - (Optional) ID of first achievement to read.  If there is
    //                        no achievement by this ID, reading will start at the next
    //                        valid achievement ID.  If maxDefinitions is 0, this value
    //                        is ignored.
    //  maxDefinitions      - Max number of defitions to read, or 0 if only interested
    //                        in getting the total.
    //  count               - (Optional) Stores number of definitions read.
    //                        If only the total is desired this can be null.
    //  total               - (Optional) Stores total number of achievements defined
    //                        for the title.  If only reading defintions and don't care
    //                        about the total, this can be null.
    //RETURNS
    //  False if failed to start request.  This most likely indicates !rlRos::IsOnline().
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.   
    static bool GetAchievementDefinitions(const int localGamerIndex,
                                          rlScAchievementDefinitionInfo* definitions,
                                          const int firstAchievementId,
                                          const unsigned maxDefinitions,
                                          unsigned* count,
                                          unsigned* total,
                                          netStatus* status);

    //PURPOSE
    //  Returns achievements specified player has earned.
    //PARAMS
    //  localGamerIndex     - Gamer doing the reading
    //  gamerHandle         - Gamer to read achievements for
    //  achievements        - (Optional) Array to store achievements in.
    //                        If only the total is desired this can be null.
    //  firstAchievementId  - (Optional) ID of first achievement to read.  If there is
    //                        no achievement by this ID, reading will start at the next
    //                        valid achievement ID.  If maxAchievements is 0, this value
    //                        is ignored.
    //  maxAchievements      - Max number of achievements to read, or 0 if only interested
    //                        in getting the total.
    //  count               - (Optional) Stores number of achievements read.
    //                        If only the total is desired this can be null.
    //  total               - (Optional) Stores total number of achievements awarded
    //                        to the player.  If only reading achievements and don't care
    //                        about the total, this can be null.
    //RETURNS
    //  False if failed to start request.  This most likely indicates !rlRos::IsOnline().
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.   
    static bool GetAchievementsByGamer(const int localGamerIndex,
                                       const rlGamerHandle& gamerHandle,
                                       rlScAchievementInfo* achievements,
                                       const int firstAchievementId,
                                       const unsigned maxAchievements,
                                       unsigned* count,
                                       unsigned* total,
                                       netStatus* status);

    //PURPOSE
    //  Bulk method for awarding achievements.  It is provided for synchronizing the 
    //  backend with achievements earned while not connected to ROS, and should only
    //  called once per boot per local gamer profile. 
    //
    //  For non-SC platforms, this is automatically called after each successful
    //  rlAchievement::Read() call, so title code shouldn't need to call this.
    //
    //PARAMS
    //  localGamerIndex     - Gamer to award achievements to
    //  achievements        - Achievements to award.  This array does NOT need to be
    //                        kept in memory during the operation;  it is read only 
    //                        during this call.
    //  numAchievements     - Number of elements in the achievements array
    //RETURNS
    //  False if failed to start request.  This most likely indicates !rlRos::IsOnline().
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    static bool Synchronize(const int localGamerIndex,
                            const rlAchievementInfo* achievements,
                            const unsigned numAchievements,
                            netStatus* status);

private:
    //Don't allow creating instances.  Users only call static methods.
    rlScAchievements();
};

} //namespace rage

#endif  //RLINE_RLSCACHIEVEMENTS_H
