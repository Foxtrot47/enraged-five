// 
// rline/rlachievement.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlstats.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "rltitleid.h"
#include "ros/rlros.h"
#include "ros/rlroshttptask.h"
#include "string/string.h"
#include "system/memory.h"
#include "rline/rlpresence.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, stats)
#undef __rage_channel
#define __rage_channel rline_stats

extern const rlTitleId* g_rlTitleId;

#if !__NO_OUTPUT
static const char* GetLeaderboardServiceName(const rlLeaderboardService lbSvc)
{
    switch((int)lbSvc)
    {
    case RL_LEADERBOARD_SERVICE_XBL:
        return "XBL";
    case RL_LEADERBOARD_SERVICE_ROS:
        return "ROS";
    }

    return "UNKNOWN";
}

static const char* GetLeaderboardTypeName(const rlLeaderboardType lbType)
{
    switch((int)lbType)
    {
    case RL_LEADERBOARD_TYPE_PLAYER:
        return "PLAYER";
    case RL_LEADERBOARD_TYPE_GROUP:
        return "GROUP";
    case RL_LEADERBOARD_TYPE_GROUP_MEMBER:
        return "GROUP_MEMBER";
    }

    return "UNKNOWN";
}
#endif

//////////////////////////////////////////////////////////////////////////
//  rlInputStatValue
//////////////////////////////////////////////////////////////////////////
rlLeaderboardInputValue::rlLeaderboardInputValue()
: InputId(~0u)
{
}

int
rlLeaderboardInputValue::GetType() const
{
    return RL_STAT_TYPE_FROM_ID(InputId);
}

const char*
rlLeaderboardInputValue::GetTypeString() const
{
    switch(GetType())
    {
    case RL_STAT_TYPE_INT32: return "int32";
    case RL_STAT_TYPE_INT64: return "int64";
    case RL_STAT_TYPE_DOUBLE: return "double";
    case RL_STAT_TYPE_FLOAT: return "float";
    }

    return "unknown";
}


//////////////////////////////////////////////////////////////////////////
//  rlLeaderboardGroupHandle
//////////////////////////////////////////////////////////////////////////
rlLeaderboardGroupHandle::rlLeaderboardGroupHandle()
{
    Clear();
}

rlLeaderboardGroupHandle::rlLeaderboardGroupHandle(const char* handle)
{
    Init(handle);
}

rlLeaderboardGroupHandle::rlLeaderboardGroupHandle(const s64 groupId)
{
    Init(groupId);
}

void
rlLeaderboardGroupHandle::Init(const char* handle)
{
    rlAssertf(strlen(handle) < RL_GROUP_HANDLE_MAX_CHARS,
                "Group handle '%s' is too long.  Max length is %d",
                handle, RL_GROUP_HANDLE_MAX_CHARS - 1);

    safecpy(Handle, handle);
}

void
rlLeaderboardGroupHandle::Init(const s64 groupId)
{
    formatf(Handle, "%" I64FMT "d", groupId);
}

void
rlLeaderboardGroupHandle::Clear()
{
    Handle[0] = '\0';
}

bool
rlLeaderboardGroupHandle::IsValid() const
{
    return '\0' != Handle[0];
}

bool
rlLeaderboardGroupHandle::operator==(const rlLeaderboardGroupHandle& that) const
{
    return (0 == strcmp(Handle, that.Handle));
}

bool
rlLeaderboardGroupHandle::operator!=(const rlLeaderboardGroupHandle& that) const
{
    return (0 != strcmp(Handle, that.Handle));
}
    
//////////////////////////////////////////////////////////////////////////
//  rlLeaderboardRowId
//////////////////////////////////////////////////////////////////////////
rlLeaderboardRowId::rlLeaderboardRowId()
{
    this->Clear();
}

rlLeaderboardRowId::rlLeaderboardRowId(const rlGamerHandle& gamerHandle)
{
    this->Init(gamerHandle);
}

rlLeaderboardRowId::rlLeaderboardRowId(const rlLeaderboardGroupHandle& groupHandle)
{
    this->Init(groupHandle);
}

void
rlLeaderboardRowId::Init(const rlGamerHandle& gamerHandle)
{
    this->Clear();
    GamerHandle = gamerHandle;
}

void
rlLeaderboardRowId::Init(const rlLeaderboardGroupHandle& groupHandle)
{
    this->Clear();
    GroupHandle = groupHandle;
}

void
rlLeaderboardRowId::Clear()
{
    GamerHandle.Clear();
    GroupHandle.Clear();
}

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboardRow
//////////////////////////////////////////////////////////////////////////
rlLeaderboardRow::rlLeaderboardRow()
{
    this->Clear();
}

void
rlLeaderboardRow::Clear()
{
    GamerDisplayName[0] = '\0';
    GroupDisplayName[0] = '\0';
    GroupDisplayTag[0] = '\0';
    Rank = 0;
    RowId.Clear();
    sysMemSet(ColumnValues, 0, sizeof(ColumnValues));
    NumValues = 0;
}

//////////////////////////////////////////////////////////////////////////
// rlLeaderboardUpdate
//////////////////////////////////////////////////////////////////////////
rlLeaderboardUpdate::rlLeaderboardUpdate()
: LeaderboardService(RL_LEADERBOARD_SERVICE_DEFAULT)
, LeaderboardType(RL_LEADERBOARD_TYPE_PLAYER)
, GroupType(-1)
, LeaderboardId(~0u)
, NumValues(0)
{
}

bool
rlLeaderboardUpdate::IsPlayerUpdate() const
{
    return RL_LEADERBOARD_TYPE_PLAYER == LeaderboardType
            && !GroupHandle.IsValid();
}

bool
rlLeaderboardUpdate::IsGroupUpdate() const
{
    return RL_LEADERBOARD_TYPE_GROUP == LeaderboardType
            && GroupHandle.IsValid()
            && GroupType >= 0;
}

//////////////////////////////////////////////////////////////////////////
//  rlRosReadStatsTask
//////////////////////////////////////////////////////////////////////////
class rlRosReadStatsTask : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlRosReadStatsTask);

	rlRosReadStatsTask();

    virtual ~rlRosReadStatsTask()
    {
    }

    bool CommonConfigure(const int leaderboardId,
                        const rlLeaderboardType lbType,
                        const int groupType,
                        const unsigned numRows,
                        rlLeaderboardRow* rows,
                        unsigned* numRowsReturned,
                        unsigned* totalRows,
                        const rlLeaderboardColumnInfo* columns,
                        const unsigned numColumns);

    //ReadByRank
    bool Configure(const int leaderboardId,
                    const rlLeaderboardType lbType,
                    const int groupType,
                    const rlLeaderboardGroupHandle& groupHandle,
                    const int startRank,
                    const unsigned numRows,
                    rlLeaderboardRow* rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns);

    //ReadByRow
    bool Configure(const int leaderboardId,
                    const rlLeaderboardType lbType,
                    const int groupType,
                    const rlLeaderboardGroupHandle& groupHandle,
                    const rlLeaderboardRowId* rowIds,
                    const int numRowsRequested,
                    rlLeaderboardRow* rows,
                    unsigned* totalRows,
                    unsigned* numRowsReturned,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns);

    //ReadMemberByGroups
    bool Configure(const int leaderboardId,
                    const int groupType,
                    const rlGamerHandle& member,
                    const rlLeaderboardGroupHandle* groupHandles,
                    const int numRowsRequested,
                    rlLeaderboardRow* rows,
                    unsigned* numRowsReturned,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns);

    //ReadByRadius
    bool Configure(const int leaderboardId,
                    const rlLeaderboardType lbType,
                    const int groupType,
                    const rlLeaderboardRowId& pivotRowId,
                    const unsigned radius,
                    rlLeaderboardRow* rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns);

    //GetLeaderboardSize
    bool Configure(const int leaderboardId,
                    const rlLeaderboardType lbType,
                    const int groupType,
                    const rlLeaderboardGroupHandle& groupHandle,
                    unsigned* lbSize);

protected:

    //The web method we call to get the URI.
    virtual const char* GetServiceMethod() const
    {
        switch(m_ReadType)
        {
        case READ_BY_RANK:
        case READ_LEADERBOARD_SIZE:
            return "Leaderboards.asmx/ReadLeaderboard";
        case READ_BY_PLAYER:
            return "Leaderboards.asmx/ReadLeaderboardByHandle";
        case READ_BY_GROUP:
            return "Leaderboards.asmx/ReadLeaderboardByHandle";
        case READ_BY_GROUP_MEMBER:
            return "Leaderboards.asmx/ReadLeaderboardByGroupMember";
        case READ_BY_PLAYER_RADIUS:
            return "Leaderboards.asmx/ReadLeaderboardNearHandle";
        case READ_BY_GROUP_RADIUS:
            return "Leaderboards.asmx/ReadLeaderboardNearHandle";
        case READ_INVALID:
            break;
        }

        return "InvalidStatsReadType";
    }

    virtual bool ProcessSuccess(const rlRosResult& result,
                                    const parTreeNode* node,
                                    int& resultCode);

private:

    enum ReadType
    {
        READ_INVALID        = -1,
        READ_BY_RANK,
        READ_BY_PLAYER,
        READ_BY_GROUP,
        READ_BY_GROUP_MEMBER,
        READ_BY_PLAYER_RADIUS,
        READ_BY_GROUP_RADIUS,
        READ_LEADERBOARD_SIZE
    };

    ReadType m_ReadType;

    int m_LbId;
    rlLeaderboardType m_LbType;
    unsigned m_StartRank;
    rlLeaderboardRow* m_Rows;
    unsigned* m_NumRowsReturned;
    unsigned* m_TotalRows;
    unsigned m_MaxRows;
    rlLeaderboardColumnInfo m_Columns[RL_MAX_LEADERBOARD_COLUMNS];
    unsigned m_NumColumns;

    rlLeaderboardRowId m_PivotRowId;

    netStatus m_MyStatus;
};

rlRosReadStatsTask::rlRosReadStatsTask()
    : m_ReadType(READ_INVALID)
    , m_LbId(-1)
    , m_LbType(RL_LEADERBOARD_TYPE_INVALID)
    , m_StartRank(0)
    , m_Rows(NULL)
    , m_NumRowsReturned(NULL)
    , m_TotalRows(NULL)
    , m_MaxRows(0)
    , m_NumColumns(0)
{
}

bool
rlRosReadStatsTask::CommonConfigure(const int leaderboardId,
                                    const rlLeaderboardType lbType,
                                    const int groupType,
                                    const unsigned numRows,
                                    rlLeaderboardRow* rows,
                                    unsigned* numRowsReturned,
                                    unsigned* totalRows,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns)
{
    bool success = false;
    m_LbId = leaderboardId;
    m_LbType = lbType;
    m_Rows = rows;
    m_NumRowsReturned = numRowsReturned;
    //m_NumRowsReturned will be NULL when calling GetLeaderboardSize()
    if(m_NumRowsReturned){*m_NumRowsReturned = 0;}
    m_TotalRows = totalRows;
    //m_TotalRows is an optional parameter.
    if(m_TotalRows){*m_TotalRows = 0;}
    m_MaxRows = numRows;

    rtry
    {
        rverify(numColumns <= RL_MAX_LEADERBOARD_COLUMNS,catchall,);

        rverify(RL_LEADERBOARD_TYPE_PLAYER == lbType
                || RL_LEADERBOARD_TYPE_GROUP == lbType
                || RL_LEADERBOARD_TYPE_GROUP_MEMBER == lbType,
                catchall,
                rlTaskError("Unknown leaderboard type: %d", lbType));

        for(int i = 0; i < (int) numRows; ++i)
        {
            rows[i].Clear();
        }

        for(int i = 0; i < (int) numColumns; ++i)
        {
            m_Columns[i] = columns[i];
        }

        m_NumColumns = numColumns;

        //Find someone's, anyone's, credentials and use them to call the web service.
        int localGamerIndex;
        const rlRosCredentials& cred = rlRos::GetFirstValidCredentials(&localGamerIndex);

        rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(rlRosHttpTask::Configure(localGamerIndex),
                catchall,
                rlTaskError("Failed to configure base class"));

        //Common parameters used by all LB methods
        int typeId;
        switch(lbType)
        {
        case RL_LEADERBOARD_TYPE_PLAYER:
            typeId = RL_LEADERBOARD_TYPE_PLAYER;
            break;
        case RL_LEADERBOARD_TYPE_GROUP:
            rverify(groupType >= 0,
                    catchall,
                    rlTaskError("Invalid group type: %d", groupType));
            typeId = groupType+RL_LEADERBOARD_TYPE_GROUP;
            break;
        case RL_LEADERBOARD_TYPE_GROUP_MEMBER:
            rverify(groupType >= 0,
                    catchall,
                    rlTaskError("Invalid group type: %d", groupType));
            typeId = groupType+RL_LEADERBOARD_TYPE_GROUP_MEMBER;
            break;
        default:
            rverify(false,
                    catchall,
                    rlTaskError("Unknown leaderboard type: %d", lbType));
            //break;
        }

        rcheck(AddStringParameter("ticket", cred.GetTicket(), true),
                catchall,
                rlTaskError("Error adding ticket parameter"));

        rcheck(AddIntParameter("typeId", typeId),
                catchall,
                rlTaskError("Error adding typeId parameter"));

        rcheck(AddIntParameter("leaderboardId", leaderboardId),
                catchall,
                rlTaskError("Error adding leaderboardId parameter"));

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

//ReadByRank
bool
rlRosReadStatsTask::Configure(const int leaderboardId,
                            const rlLeaderboardType lbType,
                            const int groupType,
                            const rlLeaderboardGroupHandle& groupHandle,
                            const int startRank,
                            const unsigned numRows,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns)
{
    bool success = false;

    rlTaskDebug("Reading leaderboard 0x%08x by rank...", leaderboardId);

    m_ReadType = READ_BY_RANK;

    if(rlVerify(CommonConfigure(leaderboardId,
                                lbType,
                                groupType,
                                numRows,
                                rows,
                                numRowsReturned,
                                totalRows,
                                columns,
                                numColumns)))
    {
        if(AddStringParameter("groupHandle", groupHandle.Handle, true)
            && AddIntParameter("startRank", startRank)
            && AddIntParameter("maxCount", numRows))
        {
            m_StartRank = startRank;

            success = true;
        }
        else
        {
            rlTaskError("Failed to add HTTP request params");
            success = false;
        }
    }

    return success;
}

//ReadByRow
bool
rlRosReadStatsTask::Configure(const int leaderboardId,
                            const rlLeaderboardType lbType,
                            const int groupType,
                            const rlLeaderboardGroupHandle& groupHandle,
                            const rlLeaderboardRowId* rowIds,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns)
{
    bool success = false;

    rlTaskDebug("Reading %d rows of leaderboard 0x%08x...",
                numRowsRequested,
                leaderboardId);

    const char* listParamName;

    if(RL_LEADERBOARD_TYPE_PLAYER == lbType)
    {
        m_ReadType = READ_BY_PLAYER;
        listParamName = "handles";
    }
    else if(RL_LEADERBOARD_TYPE_GROUP == lbType)
    {
        m_ReadType = READ_BY_GROUP;
        listParamName = "handles";
    }
    else if(RL_LEADERBOARD_TYPE_GROUP_MEMBER == lbType)
    {
        if(!rlVerify(groupHandle.IsValid()))
        {
            rlTaskError("Attempt to read a group member leaderboard with an invalid group ID");
            return false;
        }
        m_ReadType = READ_BY_GROUP_MEMBER;
        listParamName = "players";
    }
    else
    {
        rlAssert(false);
        rlTaskError("Unknown leaderboard type: %d", lbType);
        return false;
    }

    if(rlVerify(rowIds)
        && rlVerify(CommonConfigure(leaderboardId,
                                    lbType,
                                    groupType,
                                    numRowsRequested,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    columns,
                                    numColumns))
        && rlVerify(RL_LEADERBOARD_TYPE_GROUP_MEMBER != lbType
                    //The parameter name is "groups" b/c the web API supports
                    //asking for multiple groups.  We only pass one group.
                    || AddStringParameter("groups", groupHandle.Handle, true))
        && rlVerify(BeginListParameter(listParamName)))
    {
        for(int i = 0; i < numRowsRequested; ++i)
        {
            if(RL_LEADERBOARD_TYPE_GROUP == lbType)
            {
                //Add list of group IDs for which we want stats

                const rlLeaderboardGroupHandle& tmpGrpHandle = rowIds[i].GroupHandle;
                if(rlVerify(tmpGrpHandle.IsValid()))
                {
                    if(!rlVerify(AddListParamStringValue(tmpGrpHandle.Handle)))
                    {
                        rlTaskError("Error adding group handle '%s' to HTTP request", tmpGrpHandle.Handle);
                        return false;
                    }
                }
                else
                {
                    rlAssert(m_MaxRows > 0);
                    --m_MaxRows;
                }
            }
            else
            {
                //Add list of gamer handles for which we want stats

                const rlGamerHandle& gh = rowIds[i].GamerHandle;
                if(rlVerify(gh.IsValid()))
                {
                    char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
                    if(!rlVerify(gh.ToString(ghStr)))
                    {
                        rlTaskError("Error retrieving gamer handle string");
                        return false;
                    }
                    else if(!rlVerify(AddListParamStringValue(ghStr)))
                    {
                        rlTaskError("Error adding gamer handle string to HTTP request");
                        return false;
                    }
                }
                else
                {
                    rlAssert(m_MaxRows > 0);
                    --m_MaxRows;
                }
            }
        }

        if(m_MaxRows > 0)
        {
            success = true;
        }
        else
        {
            rlTaskError("No valid row IDs");
        }
    }

    return success;
}

//ReadMemberByGroups
bool
rlRosReadStatsTask::Configure(const int leaderboardId,
                                const int groupType,
                                const rlGamerHandle& member,
                                const rlLeaderboardGroupHandle* groupHandles,
                                const int numRowsRequested,
                                rlLeaderboardRow* rows,
                                unsigned* numRowsReturned,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns)
{
    bool success = false;

    rlTaskDebug("Reading %d rows of leaderboard 0x%08x...",
                numRowsRequested,
                leaderboardId);


    m_ReadType = READ_BY_GROUP_MEMBER;

    if(rlVerify(groupHandles) 
       && rlVerify(member.IsValid())
       && rlVerify(CommonConfigure(leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP_MEMBER,
                                    groupType,
                                    numRowsRequested,
                                    rows,
                                    numRowsReturned,
                                    NULL,    //totalRows
                                    columns,
                                    numColumns)))
    {
        char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
        if(!rlVerify(member.ToString(ghStr)))
        {
            rlTaskError("Error retrieving gamer handle string");
            return false;
        }
        else if(!rlVerify(AddStringParameter("players", ghStr, true)))
        {
            rlTaskError("Error adding gamer handle string to HTTP request");
            return false;
        }
        else if (rlVerify(BeginListParameter("groups")))
        {
            for(int i = 0; i < numRowsRequested; ++i)
            {
                if(rlVerify(groupHandles[i].IsValid()))
                {
                    if(!rlVerify(AddListParamStringValue(groupHandles[i].Handle)))
                    {
                        rlTaskError("Error adding group ID to HTTP request");
                        return false;
                    }
                }
                else
                {
                    rlAssert(m_MaxRows > 0);
                    --m_MaxRows;
                }
            }
        }

        if(m_MaxRows > 0)
        {
            success = true;
        }
        else
        {
            rlTaskError("No valid row IDs");
        }
    }

    return success;
}

//ReadByRadius
bool
rlRosReadStatsTask::Configure(const int leaderboardId,
                            const rlLeaderboardType lbType,
                            const int groupType,
                            const rlLeaderboardRowId& pivotRowId,
                            const unsigned radius,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns)
{
    bool success = false;

#if !__NO_OUTPUT
    if(RL_LEADERBOARD_TYPE_PLAYER == lbType)
    {
        char handleStr[RL_MAX_GAMER_HANDLE_CHARS];
        rlVerify(pivotRowId.GamerHandle.ToString(handleStr));

        rlDebug("Reading leaderboard 0x%08x by by radius %u around gamer '%s'...", 
                  leaderboardId,
                  radius,
                  handleStr);
    }
    else if(RL_LEADERBOARD_TYPE_GROUP == lbType)
    {
        rlDebug("Reading leaderboard 0x%08x by by radius %u around group '%s'...", 
                  leaderboardId,
                  radius,
                  pivotRowId.GroupHandle.Handle);
    }
    else
    {
        //RL_LEADERBOARD_TYPE_GROUP_MEMBER is not allowed when reading by radius.
    }
#endif  //__NO_OUTPUT

    const unsigned numRows = 1 + (2 * radius);

    if(RL_LEADERBOARD_TYPE_PLAYER == lbType)
    {
        m_ReadType = READ_BY_PLAYER_RADIUS;
    }
    else if(RL_LEADERBOARD_TYPE_GROUP == lbType)
    {
        m_ReadType = READ_BY_GROUP_RADIUS;
    }
    else if(RL_LEADERBOARD_TYPE_GROUP_MEMBER == lbType)
    {
        rlAssert(false);
        rlTaskError("Can't read a radius on a group member leaderboard.");
        return false;
    }
    else
    {
        rlAssert(false);
        rlTaskError("Unknown leaderboard type: %d", lbType);
        return false;
    }

    if(rlVerify(CommonConfigure(leaderboardId,
                                    lbType,
                                    groupType,
                                    numRows,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    columns,
                                    numColumns)))
    {
        if(RL_LEADERBOARD_TYPE_GROUP == lbType)
        {
            //Group ID to use as the pivot

            if(rlVerify(pivotRowId.GroupHandle.IsValid()))
            {
                if(AddStringParameter("handle", pivotRowId.GroupHandle.Handle, true))
                {
                    m_PivotRowId = pivotRowId;
                    success = true;
                }
            }
        }
        else if(rlVerify(pivotRowId.GamerHandle.IsValid()))
        {
            //Gamer handle to use as the pivot

            char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
            if(!rlVerify(pivotRowId.GamerHandle.ToString(ghStr)))
            {
                rlTaskError("Error retrieving gamer handle string");
                return false;
            }
            else if(AddStringParameter("handle", ghStr, true))
            {
                m_PivotRowId = pivotRowId;
                success = true;
            }
        }

        if(!success
            || !AddIntParameter("numBefore", (int) radius)
            || !AddIntParameter("numAfter", (int) radius))
        {
            rlTaskError("Failed to add HTTP request params");
            success = false;
        }
    }

    return success;
}

//GetLeaderboardSize
bool
rlRosReadStatsTask::Configure(const int leaderboardId,
                                const rlLeaderboardType lbType,
                                const int groupType,
                                const rlLeaderboardGroupHandle& groupHandle,
                                unsigned* lbSize)
{
    bool success = false;

    m_ReadType = READ_LEADERBOARD_SIZE;

    //To get the size of a LB we just read the top ranked player.
    //Part of the result we receive will contain the LB size.
    if(rlVerify(CommonConfigure(leaderboardId,
                                lbType,
                                groupType,
                                0,      //numRows
                                NULL,   //rows
                                NULL,   //numRowsReturned
                                lbSize,
                                NULL,   //columns
                                0)))
    {
        if(AddStringParameter("groupHandle", groupHandle.Handle, true)
            && AddIntParameter("startRank", 1)
            && AddIntParameter("maxCount", 1))
        {
            success = true;
        }
        else
        {
            rlTaskError("Failed to add HTTP request params");
            success = false;
        }
    }

    return success;
}

//protected:
bool
rlRosReadStatsTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* node,
                                    int& /*resultCode*/)
{
    rlAssert(node);

    rtry
    {
        rverify(RL_LEADERBOARD_TYPE_PLAYER == m_LbType
                || RL_LEADERBOARD_TYPE_GROUP == m_LbType
                || RL_LEADERBOARD_TYPE_GROUP_MEMBER == m_LbType,
                catchall,
                rlTaskError("Unhandled leaderboard type %d", m_LbType));

        const parTreeNode* resultsNode = node->FindChildWithName("Results");
        rcheck(resultsNode, catchall, 
            rlTaskError("Failed to find <Results> element"));

        const parAttribute* attr;
        const parElement* el = &resultsNode->GetElement();

        //Number of results
        attr = el->FindAttribute("count");
        rcheck(attr, catchall, rlTaskError("Failed to find 'count' attribute"));
        const int rowCount = attr->FindIntValue();

        //Total number of rows in the leaderboard
        attr = el->FindAttribute("total");
        rcheck(attr, catchall, rlTaskError("Failed to find 'total' attribute"));
        const int totalRows = attr->FindIntValue();

        rlTaskDebug("row count: %d total rows: %d", rowCount, totalRows);

        if(m_TotalRows)
        {
            *m_TotalRows = totalRows;
        }

        if(READ_LEADERBOARD_SIZE != m_ReadType)
        {
            rlAssert(m_NumRowsReturned);

            const parTreeNode* rowNode = resultsNode->GetChild();

            rlLeaderboardRow* row = m_Rows;

            for(int i = 0; i < rowCount && rowNode; ++i, rowNode = rowNode->GetSibling())
            {
                rlTaskDebug3("row=%d", i);

                el = &rowNode->GetElement();

                //Rank
                attr = el->FindAttribute("r");
                rcheck(attr, catchall, rlTaskError("Failed to find 'r' attribute"));
                row->Rank = attr->FindIntValue();

                //Rating
                rlStatValue ratingValue;
                attr = el->FindAttribute("v");
                rcheck(attr, catchall, rlTaskError("Failed to find 'v' attribute"));
                const char* strVal = attr->GetStringValue();
                rverify(1 == sscanf(strVal, "%" I64FMT "d", &ratingValue.Int64Val),
                        catchall,
                        rlTaskError("Error parsing rating value for leaderboard: 0x%08x", m_LbId));

                char hstr[64];

                switch(m_LbType)
                {
                case RL_LEADERBOARD_TYPE_PLAYER:
                    //Gamer handle
                    attr = el->FindAttribute("ph");
                    rcheck(attr, catchall, rlTaskError("Failed to find 'ph' attribute"));
                    attr->GetStringRepr(hstr, true);
                    rcheck(row->RowId.GamerHandle.FromString(hstr),
                            catchall,
                            rlTaskError("Failed to parse gamer handle: %s", hstr));
                    //Gamer name
                    attr = el->FindAttribute("pn");
                    rcheck(attr, catchall, rlTaskError("Failed to find 'pn' attribute"));
                    attr->GetStringRepr(row->GamerDisplayName, true);
                    break;
                case RL_LEADERBOARD_TYPE_GROUP_MEMBER:
                    //Gamer handle
                    attr = el->FindAttribute("ph");
                    rcheck(attr, catchall, rlTaskError("Failed to find 'ph' attribute"));
                    attr->GetStringRepr(hstr, true);
                    rcheck(row->RowId.GamerHandle.FromString(hstr),
                            catchall,
                            rlTaskError("Failed to parse gamer handle: %s", hstr));
                    //Gamer name
                    attr = el->FindAttribute("pn");
                    rcheck(attr, catchall, rlTaskError("Failed to find 'pn' attribute"));
                    attr->GetStringRepr(row->GamerDisplayName, true);

                    //fall thru to get group identifiers

                case RL_LEADERBOARD_TYPE_GROUP:
                    //Group Handle
                    attr = el->FindAttribute("gh");
                    rcheck(attr, catchall, rlTaskError("Failed to find 'gh' attribute"));
                    char groupHandle[RL_GROUP_HANDLE_MAX_CHARS];
                    attr->GetStringRepr(groupHandle, true);
                    row->RowId.GroupHandle.Init(groupHandle);
                    //Group name
                    attr = el->FindAttribute("gn");
                    if(attr)
                    {
                        attr->GetStringRepr(row->GroupDisplayName, true);
                    }
                    else
                    {
                        safecpy(row->GroupDisplayName, groupHandle);
                    }
                    //Group tag
                    attr = el->FindAttribute("gt");
                    if(attr)
                    {
                        attr->GetStringRepr(row->GroupDisplayTag, true);
                    }
                    else
                    {
                        safecpy(row->GroupDisplayTag, row->GroupDisplayName);
                    }
                    break;
                case RL_LEADERBOARD_TYPE_INVALID:
                default:
                    rlAssert(false);
                }

                //Parse the stats values from the CSV.
                const char* columnCsv = rowNode->GetData();
                rlStatValue colVals[RL_MAX_LEADERBOARD_COLUMNS];
                unsigned colIds[RL_MAX_LEADERBOARD_COLUMNS];
                unsigned numColumnsReturned = 0;

                bool done = false;
                for(int i = 0; i < RL_MAX_LEADERBOARD_COLUMNS && !done; ++i, ++numColumnsReturned)
                {
                    char kvPair[64];
                    unsigned bufLen = sizeof(kvPair);
                    done = parMemberArray::ReadCsvDatum(kvPair, &bufLen, columnCsv);

                    if(bufLen)
                    {
                        rverify(2 == sscanf(kvPair, "%u=%" I64FMT "d", &colIds[i], &colVals[i].Int64Val),
                                catchall,
                                rlTaskError("Error parsing column result %d for leaderboard: 0x%08x",
                                            i, m_LbId));

                        rlTaskDebug3("  column index=%u  columnId=0x%08x, value=%" I64FMT "d",
                                    i, colIds[i], colVals[i].Int64Val);
                    }
                }

                for(int i = 0; i < (int)m_NumColumns; ++i)
                {
                    if(0x0000FFFE == m_Columns[i].Id)
                    {
                        row->ColumnValues[i].Int64Val = ratingValue.Int64Val;
                        ++row->NumValues;
                        continue;
                    }

                    bool found = false;
                    for(int j = 0; j < (int)numColumnsReturned; ++j)
                    {
                        if(colIds[j] == m_Columns[i].Id)
                        {
                            row->ColumnValues[i].Int64Val = colVals[j].Int64Val;
                            ++row->NumValues;
                            found = true;
                            break;
                        }
                    }

                    if(!found)
                    {
                        row->ColumnValues[i].Int64Val = 0;
                        rlTaskWarning("Column id: %u was not found in the results", m_Columns[i].Id);
                    }
                }

                ++row;
            }

            *m_NumRowsReturned = unsigned(row - m_Rows);
        }
    }
    rcatchall
    {
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
// rlRosWriteStatsTask
///////////////////////////////////////////////////////////////////////////////
class rlRosWriteStatsTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlRosWriteStatsTask);
    RL_TASK_USE_CHANNEL(rline_stats);

    rlRosWriteStatsTask()
    {
    }

    virtual ~rlRosWriteStatsTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const rlLeaderboardUpdate* updates,
                    const unsigned numUpdates);

    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    virtual const char* GetServiceMethod() const { return "Leaderboards.asmx/UpdateLeaderboards"; }
};

bool
rlRosWriteStatsTask::Configure(const int localGamerIndex,
                                const rlLeaderboardUpdate* updates,
                                const unsigned numUpdates)
{
    if(!rlVerify(numUpdates > 0))
    {
        return false;
    }

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add ticket parameter");
        return false;
    }

    BeginListParameter("updates");
    for(int i = 0; i < (int)numUpdates; ++i)
    {
        const rlLeaderboardUpdate& lbu = updates[i];

        if(RL_LEADERBOARD_SERVICE_ROS != lbu.LeaderboardService)
        {
            continue;
        }

        if(!rlVerify(RL_LEADERBOARD_TYPE_PLAYER == lbu.LeaderboardType
                    || RL_LEADERBOARD_TYPE_GROUP == lbu.LeaderboardType))
        {
            rlTaskError("Invalid leaderboard type:%d", lbu.LeaderboardType);
            return false;
        }

        if(RL_LEADERBOARD_TYPE_PLAYER == lbu.LeaderboardType
            && lbu.GroupHandle.IsValid())
        {
            rlTaskError("GroupHandle must be empty for PLAYER leaderboards");
            return false;
        }

        if(RL_LEADERBOARD_TYPE_PLAYER != lbu.LeaderboardType
            && !lbu.GroupHandle.IsValid())
        {
            rlTaskError("Invalid group handle for leaderboard type:%d", lbu.LeaderboardType);
            return false;
        }

        char kvpair[128];
        int typeId;
        switch(lbu.LeaderboardType)
        {
        case RL_LEADERBOARD_TYPE_PLAYER:
            typeId = RL_LEADERBOARD_TYPE_PLAYER;
            break;
        case RL_LEADERBOARD_TYPE_GROUP:
            if(lbu.GroupType < 0)
            {
                rlTaskError("Invalid group type: %d", lbu.GroupType);
                return false;
            }
            typeId = lbu.GroupType+RL_LEADERBOARD_TYPE_GROUP;
            break;
        case RL_LEADERBOARD_TYPE_GROUP_MEMBER:
            if(lbu.GroupType < 0)
            {
                rlTaskError("Invalid group type: %d", lbu.GroupType);
                return false;
            }
            typeId = lbu.GroupType+RL_LEADERBOARD_TYPE_GROUP_MEMBER;
            break;
        default:
            rlTaskError("Unknown leaderboard type: %d", lbu.LeaderboardType);
            return false;
            //break;
        }

        formatf(kvpair,
                "T=%u,L=%u,G=%s",
                typeId, lbu.LeaderboardId, lbu.GroupHandle.Handle);
        AddListParamStringValue(kvpair);
        for(int j = 0; j < (int)lbu.NumValues; ++j)
        {
            const rlLeaderboardInputValue& val = lbu.InputValues[j];
            switch(val.GetType())
            {
            case RL_STAT_TYPE_INT32:
            case RL_STAT_TYPE_INT64:
                formatf(kvpair, "%u=%" I64FMT "d", val.InputId, val.Value.Int64Val);
                break;
            case RL_STAT_TYPE_FLOAT:
            case RL_STAT_TYPE_DOUBLE:
                //Submit float/double as int64 to avoid precision issues
                //when converting to/from strings.
                formatf(kvpair, "%u=%" I64FMT "d", val.InputId, val.Value.Int64Val);
                break;
            default:
                rlTaskError("Unrecognized type: %d in input value %d for leaderboard: 0x%08x",
                            val.GetType(), j, lbu.LeaderboardId);
                return false;
            }

            AddListParamStringValue(kvpair);
        }
    }

    return true;
}

bool 
rlRosWriteStatsTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* NET_ASSERTS_ONLY(node),
                                    int& /*resultCode*/)
{
    rlAssert(node);

    return true;
}

///////////////////////////////////////////////////////////////////////////////
// rlStats
///////////////////////////////////////////////////////////////////////////////

bool
rlStats::GetLeaderboardSize(const int leaderboardId,
                            const rlLeaderboardType lbType,
                            const rlLeaderboardService lbSvc,
                            const int groupType,
                            const rlLeaderboardGroupHandle& groupHandle,
                            unsigned* numRows,
                            netStatus* status)
{
    rlAssert(numRows);

    rlDebug("Attempting to get size of leaderboard 0x%08x...", leaderboardId);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(RL_LEADERBOARD_TYPE_GROUP_MEMBER != lbType
                    || groupHandle.IsValid(),
                    catchall,
                    rlError("Attempt to read a group member leaderboard with an invalid group ID"));

            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        lbType,
                                        groupType,
                                        groupHandle,
                                        numRows,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlStats::ReadByRank(const int leaderboardId,
                    const rlLeaderboardType lbType,
                    const rlLeaderboardService lbSvc,
                    const int groupType,
                    const rlLeaderboardGroupHandle& groupHandle,
                    const int startRank,
                    const unsigned numRows,
                    rlLeaderboardRow* rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);

    rlDebug("Reading '%s' leaderboard 0x%08x by rank from '%s', starting with rank %d...",
            GetLeaderboardTypeName(lbType),
            leaderboardId,
            GetLeaderboardServiceName(lbSvc),
            startRank);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(RL_LEADERBOARD_TYPE_GROUP_MEMBER != lbType
                    || groupHandle.IsValid(),
                    catchall,
                    rlError("Attempt to read a group member leaderboard with an invalid group ID"));

            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        lbType,
                                        groupType,
                                        groupHandle,
                                        startRank,
                                        numRows,
                                        rows,
                                        numRowsReturned,
                                        totalRows,
                                        columns,
                                        numColumns,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlStats::ReadByRow(const int leaderboardId,
                    const rlLeaderboardType lbType,
                    const rlLeaderboardService lbSvc,
                    const int groupType,
                    const rlLeaderboardGroupHandle& groupHandle,
                    const rlLeaderboardRowId* rowIds,
                    const int numRowsRequested,
                    rlLeaderboardRow* rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    netStatus* status)
{
    rlAssert(rowIds);
    rlAssert(numRowsRequested > 0);

    rlDebug("Reading %d rows of '%s' leaderboard 0x%08x from '%s'...",
            numRowsRequested,
            GetLeaderboardTypeName(lbType),
            leaderboardId,
            GetLeaderboardServiceName(lbSvc));

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(RL_LEADERBOARD_TYPE_GROUP_MEMBER != lbType
                    || groupHandle.IsValid(),
                    catchall,
                    rlError("Attempt to read a group member leaderboard with an invalid group ID"));

            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        lbType,
                                        groupType,
                                        groupHandle,
                                        rowIds,
                                        numRowsRequested,
                                        rows,
                                        numRowsReturned,
                                        totalRows,
                                        columns,
                                        numColumns,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlStats::ReadMemberByGroups(const int leaderboardId,
                            const int groupType,
                            const rlGamerHandle& member,
                            const rlLeaderboardGroupHandle* groupHandles,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status)
{
    rlAssert(groupHandles);
    rlAssert(numRowsRequested > 0);

    rlDebug("Reading %d rows of '%s' leaderboard 0x%08x from '%s'...",
            numRowsRequested,
            GetLeaderboardTypeName(RL_LEADERBOARD_TYPE_GROUP_MEMBER),
            leaderboardId,
            GetLeaderboardServiceName(RL_LEADERBOARD_SERVICE_ROS));

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(rosTask,
                                      leaderboardId,
                                      groupType,
                                      member,
                                      groupHandles,
                                      numRowsRequested,
                                      rows,
                                      numRowsReturned,
                                      columns,
                                      numColumns,
                                      status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlStats::ReadByRadius(const int leaderboardId,
                        const rlLeaderboardType lbType,
                        const rlLeaderboardService lbSvc,
                        const int groupType,
                        const rlLeaderboardRowId& pivotRowId,
                        const unsigned radius,
                        rlLeaderboardRow* rows,
                        unsigned* numRowsReturned,
                        unsigned* totalRows,
                        const rlLeaderboardColumnInfo* columns,
                        const unsigned numColumns,
                        netStatus* status)
{
#if !__NO_OUTPUT
    if(RL_LEADERBOARD_TYPE_GROUP == lbType)
    {
        rlDebug("Reading '%s' leaderboard 0x%08x with a radius of %d around the group '%s' from '%s'...",
                GetLeaderboardTypeName(lbType),
                leaderboardId,
                radius,
                pivotRowId.GroupHandle.Handle,
                GetLeaderboardServiceName(lbSvc));
    }
    else if(RL_LEADERBOARD_TYPE_PLAYER == lbType)
    {
        char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
        rlDebug("Reading '%s' leaderboard 0x%08x with a radius of %d around the gamer '%s' from '%s'...",
                GetLeaderboardTypeName(lbType),
                leaderboardId,
                radius,
                pivotRowId.GamerHandle.ToString(ghStr),
                GetLeaderboardServiceName(lbSvc));
    }
    else
    {
        //RL_LEADERBOARD_TYPE_GROUP_MEMBER isn't supported when reading by radius
    }
#endif

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        rverify(RL_LEADERBOARD_TYPE_PLAYER == lbType
                || RL_LEADERBOARD_TYPE_GROUP == lbType,
                catchall,
                rlError("ReadByRadius can be called only for PLAYER and GROUP leaderboards."));

        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        lbType,
                                        groupType,
                                        pivotRowId,
                                        radius,
                                        rows,
                                        numRowsReturned,
                                        totalRows,
                                        columns,
                                        numColumns,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlStats::Write(const int localGamerIndex,
                const void* LIVE_ONLY(sessionHandle),
                const rlGamerHandle& gamerHandle,
                const rlLeaderboardUpdate* updates,
                const unsigned numUpdates,
                netStatus* status)
{
    OUTPUT_ONLY(char ghStr[RL_MAX_GAMER_HANDLE_CHARS]);
    rlDebug("Attempting to update leaderboards for gamer %s...",
            gamerHandle.ToString(ghStr));

    bool success = false;

    rlRosWriteStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblWriteStatsTask* xblTask = NULL;)

    rtry
    {
        rverify(rlRos::HasPrivilege(localGamerIndex,
                                    RLROS_PRIVILEGEID_LEADERBOARD_WRITE),
                catchall,
                rlError("Local gamer %d doesn't have permission to write to leaderboards",
                        localGamerIndex));

        for(int i = 0; i < (int)numUpdates; ++i)
        {
            rverify(RL_LEADERBOARD_TYPE_PLAYER == updates[i].LeaderboardType
                    || RL_LEADERBOARD_TYPE_GROUP == updates[i].LeaderboardType,
                    catchall,
                    rlError("Invalid attempt to write to leaderboard type %s(%d);  Writing is permitted only to PLAYER and GROUP leaderboards",
                            GetLeaderboardTypeName(updates[i].LeaderboardType),
                            updates[i].LeaderboardType));

            rverify(RL_LEADERBOARD_TYPE_PLAYER == updates[i].LeaderboardType
                    || updates[i].GroupHandle.IsValid(),
                    catchall,
                    rlError("Non-player leaderboards must have a valid group ID"));

            rverify(RL_LEADERBOARD_TYPE_PLAYER != updates[i].LeaderboardType
                    || !updates[i].GroupHandle.IsValid(),
                    catchall,
                    rlError("Player leaderboards must not have a group ID"));

            rverify(RL_LEADERBOARD_TYPE_PLAYER == updates[i].LeaderboardType
                    || RL_LEADERBOARD_SERVICE_ROS == updates[i].LeaderboardService,
                    catchall,
                    rlError("Non-player leaderboards must use the ROS leaderboard service"));
        }

        for(int i = 0; i < (int)numUpdates && !rosTask LIVE_ONLY(&& !xblTask); ++i)
        {
            if(RL_LEADERBOARD_SERVICE_ROS == updates[i].LeaderboardService)
            {
                if(!rosTask)
                {
                    if(!gamerHandle.IsLocal())
                    {
                        //We currently only handle writing stats for local gamers
                        continue;
                    }

                    rverify(rlGetTaskManager()->CreateTask(&rosTask),
                            catchall,
                            rlError("Error allocating rlRosWriteStatsTask"));

                    //Pass the entire updates array.  The task will pick
                    //out the ROS-only ones.
                    rverify(rlTaskBase::Configure(rosTask,
                                                gamerHandle.GetLocalIndex(),
                                                updates,
                                                numUpdates,
                                                status),
                            catchall,
                            rlError("Error configuring rlRosWriteStatsTask"));

                    rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);
                }
            }
            else
            {
                rverify(false,
                        catchall,
                        rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                                updates[i].LeaderboardService,
                                updates[i].LeaderboardId));
            }
        }

        if(!rosTask LIVE_ONLY(&& !xblTask))
        {
            //Nothing to do
            status->SetPending();
            status->SetSucceeded();
        }

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

void
rlStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

#if __DEV
bool 
rlStats::ResetAllUsersStats(const unsigned long LIVE_ONLY(leaderboardId))
{
	return false;
}
#endif  //__DEV


///////////////////////////////////////////////////////////////////////////////
// rlPlayerStats
///////////////////////////////////////////////////////////////////////////////
bool
rlPlayerStats::GetLeaderboardSize(const int leaderboardId,
                            const rlLeaderboardService lbSvc,
                            unsigned* numRows,
                            netStatus* status)
{
    rlAssert(numRows);

    rlDebug("Attempting to get size of PLAYER leaderboard 0x%08x...", leaderboardId);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        RL_LEADERBOARD_TYPE_PLAYER,
                                        -1,
                                        rlLeaderboardGroupHandle(),
                                        numRows,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlPlayerStats::ReadByRank(const int leaderboardId,
                    const rlLeaderboardService lbSvc,
                    const int startRank,
                    const unsigned numRows,
                    rlLeaderboardRow* rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);

    rlDebug("Reading PLAYER leaderboard 0x%08x by rank from '%s', starting with rank %d...",
            leaderboardId,
            GetLeaderboardServiceName(lbSvc),
            startRank);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        RL_LEADERBOARD_TYPE_PLAYER,
                                        -1,
                                        rlLeaderboardGroupHandle(),
                                        startRank,
                                        numRows,
                                        rows,
                                        numRowsReturned,
                                        totalRows,
                                        columns,
                                        numColumns,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlPlayerStats::ReadByGamers(const int leaderboardId,
                            const rlLeaderboardService lbSvc,
                            const rlGamerHandle* gamerHandles,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status)
{
    rlAssert(gamerHandles);
    rlAssert(numRowsRequested > 0);

    rlDebug("Reading %d rows of PLAYER leaderboard 0x%08x from '%s'...",
            numRowsRequested,
            leaderboardId,
            GetLeaderboardServiceName(lbSvc));

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        rlLeaderboardRowId* rowIds = Alloca(rlLeaderboardRowId, numRowsRequested);
        for(int i = 0; i < (int)numRowsRequested; ++i)
        {
            new (&rowIds[i]) rlLeaderboardRowId(gamerHandles[i]);
        }

        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        RL_LEADERBOARD_TYPE_PLAYER,
                                        -1,
                                        rlLeaderboardGroupHandle(),
                                        rowIds,
                                        numRowsRequested,
                                        rows,
                                        numRowsReturned,
                                        totalRows,
                                        columns,
                                        numColumns,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlPlayerStats::ReadByRadius(const int leaderboardId,
                        const rlLeaderboardService lbSvc,
                        const rlGamerHandle& pivotGamer,
                        const unsigned radius,
                        rlLeaderboardRow* rows,
                        unsigned* numRowsReturned,
                        unsigned* totalRows,
                        const rlLeaderboardColumnInfo* columns,
                        const unsigned numColumns,
                        netStatus* status)
{
#if !__NO_OUTPUT
    char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
    rlDebug("Reading PLAYER leaderboard 0x%08x with a radius of %d around the gamer '%s' from '%s'...",
            leaderboardId,
            radius,
            pivotGamer.ToString(ghStr),
            GetLeaderboardServiceName(lbSvc));
#endif

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;
    LIVE_ONLY(rlXblReadStatsTask* xblTask = NULL;)

    rtry
    {
        if(RL_LEADERBOARD_SERVICE_ROS == lbSvc)
        {
            rverify(rlGetTaskManager()->CreateTask(&rosTask),
                    catchall,
                    rlError("Error allocating rlRosReadStatsTask"));

            rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        RL_LEADERBOARD_TYPE_PLAYER,
                                        -1,
                                        rlLeaderboardRowId(pivotGamer),
                                        radius,
                                        rows,
                                        numRowsReturned,
                                        totalRows,
                                        columns,
                                        numColumns,
                                        status),
                    catchall,
                    rlError("Error configuring rlRosReadStatsTask"));

            rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

            success = true;
        }
        else
        {
            rverify(false,
                    catchall,
                    rlError("Unrecognized learderboard service: %d for leaderboard: 0x%08x",
                            lbSvc, leaderboardId));
        }
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

void
rlPlayerStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//////////////////////////////////////////////////////////////////////////
//  rlGroupStats
//////////////////////////////////////////////////////////////////////////
bool
rlGroupStats::GetLeaderboardSize(const int leaderboardId,
                                const int groupType,
                                unsigned* numRows,
                                netStatus* status)
{
    rlAssert(numRows);

    rlDebug("Attempting to get size of GROUP(%d) leaderboard 0x%08x...",
            groupType,
            leaderboardId);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(rosTask,
                                    leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP,
                                    groupType,
                                    rlLeaderboardGroupHandle(),
                                    numRows,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlGroupStats::ReadByRank(const int leaderboardId,
                        const int groupType,
                        const int startRank,
                        const unsigned numRows,
                        rlLeaderboardRow* rows,
                        unsigned* numRowsReturned,
                        unsigned* totalRows,
                        const rlLeaderboardColumnInfo* columns,
                        const unsigned numColumns,
                        netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);

    rlDebug("Reading GROUP(%d) leaderboard 0x%08x by rank starting with rank %d...",
            groupType,
            leaderboardId,
            startRank);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(rosTask,
                                    leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP,
                                    groupType,
                                    rlLeaderboardGroupHandle(),
                                    startRank,
                                    numRows,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    columns,
                                    numColumns,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlGroupStats::ReadByGroups(const int leaderboardId,
                            const int groupType,
                            const rlLeaderboardGroupHandle* groupHandles,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status)
{
    rlAssert(groupHandles);
    rlAssert(numRowsRequested > 0);

    rlDebug("Reading %d rows of GROUP(%d) leaderboard 0x%08x...",
            numRowsRequested,
            groupType,
            leaderboardId);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rlLeaderboardRowId* rowIds = Alloca(rlLeaderboardRowId, numRowsRequested);
        for(int i = 0; i < (int)numRowsRequested; ++i)
        {
            new (&rowIds[i]) rlLeaderboardRowId(groupHandles[i]);
        }

        rverify(rlTaskBase::Configure(rosTask,
                                    leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP,
                                    groupType,
                                    rlLeaderboardGroupHandle(),
                                    rowIds,
                                    numRowsRequested,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    columns,
                                    numColumns,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlGroupStats::ReadByRadius(const int leaderboardId,
                            const int groupType,
                            const rlLeaderboardGroupHandle& pivotGroup,
                            const unsigned radius,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status)
{
    rlDebug("Reading GROUP(%d) leaderboard 0x%08x with a radius of %d around the group '%s'...",
            groupType,
            leaderboardId,
            radius,
            pivotGroup.Handle);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(rosTask,
                                    leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP,
                                    groupType,
                                    rlLeaderboardRowId(pivotGroup),
                                    radius,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    columns,
                                    numColumns,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

void
rlGroupStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//////////////////////////////////////////////////////////////////////////
//  rlGroupMemberStats
//////////////////////////////////////////////////////////////////////////
bool
rlGroupMemberStats::GetLeaderboardSize(const int leaderboardId,
                                        const int groupType,
                                        const rlLeaderboardGroupHandle& groupHandle,
                                        unsigned* numRows,
                                        netStatus* status)
{
    rlAssert(numRows);

    rlDebug("Attempting to get size of GROUP(%d) MEMBER leaderboard 0x%08x for group '%s'...",
            groupType,
            leaderboardId,
            groupHandle.Handle);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(rosTask,
                                    leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP_MEMBER,
                                    groupType,
                                    groupHandle,
                                    numRows,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlGroupMemberStats::ReadByRank(const int leaderboardId,
                                const int groupType,
                                const rlLeaderboardGroupHandle& groupHandle,
                                const int startRank,
                                const unsigned numRows,
                                rlLeaderboardRow* rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);

    rlDebug("Reading GROUP(%d) MEMBER leaderboard 0x%08x by rank starting with rank %d for group '%s'...",
            groupType,
            leaderboardId,
            startRank,
            groupHandle.Handle);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(rosTask,
                                    leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP_MEMBER,
                                    groupType,
                                    groupHandle,
                                    startRank,
                                    numRows,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    columns,
                                    numColumns,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlGroupMemberStats::ReadByMembers(const int leaderboardId,
                                    const int groupType,
                                    const rlLeaderboardGroupHandle& groupHandle,
                                    const rlGamerHandle* gamerHandles,
                                    const int numRowsRequested,
                                    rlLeaderboardRow* rows,
                                    unsigned* numRowsReturned,
                                    unsigned* totalRows,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    netStatus* status)
{
    rlAssert(numRowsRequested > 0);

    rlDebug("Reading %d rows of GROUP(%d) MEMBER leaderboard 0x%08x by member...",
            numRowsRequested,
            groupType,
            leaderboardId);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rlLeaderboardRowId* rowIds = Alloca(rlLeaderboardRowId, numRowsRequested);
        for(int i = 0; i < (int)numRowsRequested; ++i)
        {
            new (&rowIds[i]) rlLeaderboardRowId(gamerHandles[i]);
        }

        rverify(rlTaskBase::Configure(rosTask,
                                    leaderboardId,
                                    RL_LEADERBOARD_TYPE_GROUP_MEMBER,
                                    groupType,
                                    groupHandle,
                                    rowIds,
                                    numRowsRequested,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    columns,
                                    numColumns,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

bool
rlGroupMemberStats::ReadMemberByGroups(const int leaderboardId,
                                    const int groupType,
                                    const rlGamerHandle& member,
                                    const rlLeaderboardGroupHandle* groupHandles,
                                    const int numRowsRequested,
                                    rlLeaderboardRow* rows,
                                    unsigned* numRowsReturned,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    netStatus* status)
{
    rlAssert(groupHandles);
    rlAssert(numRowsRequested > 0);

    rlDebug("Reading %d rows of GROUP(%d) MEMBER leaderboard 0x%08x by member/group...",
            numRowsRequested,
            groupType,
            leaderboardId);

    bool success = false;

    rlRosReadStatsTask* rosTask = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&rosTask),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(rosTask,
                                        leaderboardId,
                                        groupType,
                                        member,
                                        groupHandles,
                                        numRowsRequested,
                                        rows,
                                        numRowsReturned,
                                        columns,
                                        numColumns,
                                        status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(rosTask),catchall,);

        success = true;
    }
    rcatchall
    {
        if(rosTask)
        {
            rlGetTaskManager()->DestroyTask(rosTask);
        }
    }

    return success;
}

void
rlGroupMemberStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2WriteTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2WriteTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlLeaderboard2WriteTask);
    RL_TASK_USE_CHANNEL(rline_stats);

    rlLeaderboard2WriteTask()
    {
    }

    virtual ~rlLeaderboard2WriteTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const rlLeaderboard2Update** updates,
                    const unsigned numUpdates)
    {
        if(!rlRosHttpTask::Configure(localGamerIndex))
        {
            rlTaskError("Failed to configure base class");
            return false;
        }

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        if(!cred.IsValid())
        {
            rlTaskError("Credentials are invalid");
            return false;
        }

        if(!rlVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
        {
            rlTaskError("Failed to add 'ticket' parameter");
            return false;
        }

        if(!rlVerify(BeginListParameter("updates")))
        {
            rlTaskError("Failed to add 'updates' parameter");
            return false;
        }

        for(int i = 0; i < (int)numUpdates; ++i)
        {
            char kvpair[128];
            const rlLeaderboard2Update& lbu = *updates[i];

            //Leaderboard ID and clan ID
            if(lbu.m_ClanId > 0)
            {
                formatf(kvpair,
                        "L=%u,C=%" I64FMT "d",
                        lbu.m_LeaderboardId,
                        lbu.m_ClanId);
            }
            else
            {
                formatf(kvpair,
                        "L=%u",
                        lbu.m_LeaderboardId);
            }

            if(!rlVerify(AddListParamStringValue(kvpair)))
            {
                rlTaskError("Failed to construct submission");
                return false;
            }

            //Category IDs
            for(int i = 0; i < (int)lbu.m_GroupSelector.m_NumGroups; ++i)
            {
                const rlLeaderboard2Group& grp = lbu.m_GroupSelector.m_Group[i];
                if(!rlVerify(strlen(grp.m_Category) > 0))
                {
                    rlTaskError("Invalid category name");
                    return false;
                }
                if(!rlVerify(strlen(grp.m_Id) > 0))
                {
                    rlTaskError("Invalid group ID");
                    return false;
                }

                formatf(kvpair, "G=%s:%s", grp.m_Category, grp.m_Id);
                if(!rlVerify(AddListParamStringValue(kvpair)))
                {
                    rlTaskError("Failed to add category identifier");
                    return false;
                }
            }

            //Stat input values
            for(int j = 0; j < (int)lbu.m_NumValues; ++j)
            {
                const rlLeaderboardInputValue& val = lbu.m_InputValues[j];
				formatf(kvpair, "%u=%" I64FMT "d", val.InputId, val.Value.Int64Val);
                if(!rlVerify(AddListParamStringValue(kvpair)))
                {
                    rlTaskError("Failed to construct submission");
                    return false;
                }
            }
        }

        return true;
    }

    virtual bool ProcessSuccess(const rlRosResult& /*result*/,
                                const parTreeNode* NET_ASSERTS_ONLY(node),
                                int& /*resultCode*/)
    {
        rlAssert(node);

        return true;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Leaderboards.asmx/SubmitUpdate";
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2CommonReadTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2CommonReadTask : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2CommonReadTask);

protected:

	rlLeaderboard2CommonReadTask()
        : m_LbId(~0)
        , m_LbType(RL_LEADERBOARD2_TYPE_INVALID)
        , m_NumColumns(0)
        , m_Rows(NULL)
        , m_NumRowsReturned(NULL)
        , m_TotalRows(NULL)
        , m_MaxRows(0)
        , m_ServiceMethod(NULL)
		, m_LocalGamerIndex(0)
    {
    }

    virtual ~rlLeaderboard2CommonReadTask()
    {
    }

    bool Configure(const int localGamerIndex,
                        const int leaderboardId,
                        const rlLeaderboard2Type lbType,
                        const rlLeaderboard2GroupSelector& groupSelector,
                        const unsigned numRows,
                        const rlLeaderboardColumnInfo* columns,
                        const unsigned numColumns,
                        rlLeaderboard2Row** rows,
                        unsigned* numRowsReturned,
                        unsigned* totalRows)
    {
        bool success = false;

		m_LocalGamerIndex = localGamerIndex;
        m_LbId = leaderboardId;
        m_LbType = lbType;
        m_Rows = rows;
        m_NumRowsReturned = numRowsReturned;
        //m_NumRowsReturned will be NULL when calling GetLeaderboardSize()
        if(m_NumRowsReturned){*m_NumRowsReturned = 0;}
        m_TotalRows = totalRows;
        //m_TotalRows is an optional parameter.
        if(m_TotalRows){*m_TotalRows = 0;}
        m_MaxRows = numRows;

        rtry
        {
            rverify(RL_LEADERBOARD2_TYPE_PLAYER == lbType
                    || RL_LEADERBOARD2_TYPE_CLAN == lbType
                    || RL_LEADERBOARD2_TYPE_CLAN_MEMBER == lbType
                    || RL_LEADERBOARD2_TYPE_GROUP == lbType,
                    catchall,
                    rlTaskError("Unknown leaderboard type: %d", lbType));

            for(int i = 0; i < (int) numRows; ++i)
            {
                rverify(rows[i]->m_MaxValues > 0,
                        catchall,
                        rlTaskError("Improperly configured row"));

                rows[i]->Clear();
            }

			rverify(columns, catchall, rlTaskError("Column Infos are NULL"));
			m_Columns = columns;

			rverify(numColumns > 0, catchall, rlTaskError("Invalid number of columns %d", numColumns));
            m_NumColumns = numColumns;

            rverify(rlRosHttpTask::Configure(localGamerIndex),
                    catchall,
                    rlTaskError("Failed to configure base class"));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

            rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

            rcheck(AddStringParameter("ticket", cred.GetTicket(), true),
                    catchall,
                    rlTaskError("Error adding 'ticket' parameter"));

            rcheck(AddIntParameter("leaderboardId", leaderboardId),
                    catchall,
                    rlTaskError("Error adding 'leaderboardId' parameter"));

			rcheck(BeginListParameter("categoryIdentifiers"),
				catchall,
				rlTaskError("Error adding 'categoryIdentifiers' parameter"));

            if(groupSelector.m_NumGroups > 0)
            {
                for(int i = 0; i < (int)groupSelector.m_NumGroups; ++i)
                {
                    const rlLeaderboard2Group& grp = groupSelector.m_Group[i];
                    rverify(strlen(grp.m_Category) > 0,
                            catchall,
                            rlTaskError("Invalid category name"));

                    char buf[256];
                    formatf(buf, "%s=%s", grp.m_Category, grp.m_Id);
                    rcheck(AddListParamStringValue(buf),
                            catchall,
                            rlTaskError("Error adding category identifier parameter"));
                }
            }

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    //ReadByGroup
    bool Configure(const int localGamerIndex,
                        const int leaderboardId,
                        const rlLeaderboard2Type lbType,
                        const rlLeaderboard2GroupSelector* groupSelectors,
                        const unsigned numRows,
                        const rlLeaderboardColumnInfo* columns,
                        const unsigned numColumns,
                        rlLeaderboard2Row** rows,
                        unsigned* numRowsReturned,
                        unsigned* totalRows)
    {
        bool success = false;

		m_LocalGamerIndex = localGamerIndex;
        m_LbId = leaderboardId;
        m_LbType = lbType;
        m_Rows = rows;
        m_NumRowsReturned = numRowsReturned;
        //m_NumRowsReturned will be NULL when calling GetLeaderboardSize()
        if(m_NumRowsReturned){*m_NumRowsReturned = 0;}
        m_TotalRows = totalRows;
        //m_TotalRows is an optional parameter.
        if(m_TotalRows){*m_TotalRows = 0;}
        m_MaxRows = numRows;

        rtry
        {
            rverify(RL_LEADERBOARD2_TYPE_GROUP == lbType,
                    catchall,
                    rlTaskError("Unhandled leaderboard type: %d", lbType));

            for(int i = 0; i < (int) numRows; ++i)
            {
                rverify(rows[i]->m_MaxValues > 0,
                        catchall,
                        rlTaskError("Improperly configured row"));

                rows[i]->Clear();
            }

			rverify(columns, catchall, rlTaskError("Column Infos are NULL"));
			m_Columns = columns;

			rverify(numColumns > 0, catchall, rlTaskError("Invalid number of columns %d", numColumns));
            m_NumColumns = numColumns;

            rverify(rlRosHttpTask::Configure(localGamerIndex),
                    catchall,
                    rlTaskError("Failed to configure base class"));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

            rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

            rcheck(AddStringParameter("ticket", cred.GetTicket(), true),
                    catchall,
                    rlTaskError("Error adding 'ticket' parameter"));

            rcheck(AddIntParameter("leaderboardId", leaderboardId),
                    catchall,
                    rlTaskError("Error adding 'leaderboardId' parameter"));

			rcheck(BeginListParameter("categoryIdentifiers"),
				catchall,
				rlTaskError("Error adding 'categoryIdentifiers' parameter"));

            for(int i = 0; i < (int)numRows; ++i)
            {
                const rlLeaderboard2GroupSelector& gs = groupSelectors[i];

                for(int j = 0; j < (int)gs.m_NumGroups; ++j)
                {
                    const rlLeaderboard2Group& grp = gs.m_Group[j];
                    rverify(strlen(grp.m_Category) > 0,
                            catchall,
                            rlTaskError("Invalid category name"));
                    rverify(strlen(grp.m_Id) > 0,
                            catchall,
                            rlTaskError("Invalid category ID - must be non-empty when reading by group"));

                    char buf[256];
                    formatf(buf, "%s=%s", grp.m_Category, grp.m_Id);
                    rcheck(AddListParamStringValue(buf),
                            catchall,
                            rlTaskError("Error adding group identifier parameter category '%s' ID '%s'",
                                        grp.m_Category,
                                        grp.m_Id));
                }
            }

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    bool ProcessSuccess(const rlRosResult& /*result*/,
                        const parTreeNode* node,
                        int& /*resultCode*/)
    {
        rlAssert(node);

        rtry
        {
            rverify(RL_LEADERBOARD2_TYPE_PLAYER == m_LbType
                    || RL_LEADERBOARD2_TYPE_CLAN == m_LbType
                    || RL_LEADERBOARD2_TYPE_CLAN_MEMBER == m_LbType
                    || RL_LEADERBOARD2_TYPE_GROUP == m_LbType,
                    catchall,
                    rlTaskError("Unhandled leaderboard type %d", m_LbType));

            const parTreeNode* resultsNode = node->FindChildWithName("Results");
            rcheck(resultsNode, catchall, 
                rlTaskError("Failed to find <Results> element"));

            const parAttribute* attr;
            const parElement* el = &resultsNode->GetElement();

            //Number of results
            attr = el->FindAttribute("count");
            rcheck(attr, catchall, rlTaskError("Failed to find 'count' attribute"));
            const int rowCount = attr->FindIntValue();
			rcheck(rowCount <= (int)m_MaxRows, catchall, 
				rlTaskError("Failed rowCount > m_MaxRows"));

            //Total number of rows in the leaderboard
            attr = el->FindAttribute("total");
            rcheck(attr, catchall, rlTaskError("Failed to find 'total' attribute"));
            const int totalRows = attr->FindIntValue();

            rlTaskDebug("row count: %d total rows: %d", rowCount, totalRows);

            if(m_TotalRows)
            {
                *m_TotalRows = totalRows;
            }

            if(m_NumRowsReturned)
            {
                const parTreeNode* rowNode = resultsNode->GetChild();

                rlLeaderboard2Row** pRow = m_Rows;

                for(int i = 0; i < rowCount && rowNode; ++i, rowNode = rowNode->GetSibling())
                {
                    rlLeaderboard2Row* curRow = *pRow;
                    curRow->Clear();

                    el = &rowNode->GetElement();

                    //Rank
                    attr = el->FindAttribute("r");
                    rcheck(attr, catchall, rlTaskError("Failed to find 'r' attribute"));
                    curRow->m_Rank = attr->FindIntValue();

					rlTaskDebug3("row=%d, rank=%d", i, attr->FindIntValue());

                    //Rating
                    rlStatValue ratingValue;
                    attr = el->FindAttribute("v");
                    rcheck(attr, catchall, rlTaskError("Failed to find 'v' attribute"));
                    const char* strVal = attr->GetStringValue();
                    rverify(1 == sscanf(strVal, "%" I64FMT "d", &ratingValue.Int64Val),
                            catchall,
                            rlTaskError("Error parsing rating value for leaderboard: 0x%08x", m_LbId));

                    //Groups
                    attr = el->FindAttribute("group");
                    if(attr)
                    {
                        char groupsCsv[260]; // Up to 4 groups (256) + 3 commas
                        safecpy(groupsCsv, attr->GetStringValue());
                        char* p = groupsCsv;
                        unsigned numGroups = 0;
						bool commaDelimFound = true;
                        while(p && commaDelimFound && numGroups < RL_LEADERBOARD2_MAX_GROUPS)
                        {
							char* commaDelim = strchr(p, ',');

							if(commaDelim)
							{
								*commaDelim = '\0';
							}
							else
							{
								commaDelimFound = false;
							}

                            char* eqDelim = strchr(p, '=');
                            if(!rlVerifyf(eqDelim, "Missing '=' in group selector '%s'",
                                            attr->GetStringValue()))
                            {
                                numGroups = 0;
                                break;
                            }

                            *eqDelim = '\0';
                            safecpy(curRow->m_GroupSelector.m_Group[numGroups].m_Category, p);
                            p = eqDelim+1;
                            safecpy(curRow->m_GroupSelector.m_Group[numGroups].m_Id, p);
							p = commaDelim+1;

                            ++numGroups;
                        }

                        curRow->m_GroupSelector.m_NumGroups = numGroups;
                    }

                    char hstr[64];

                    switch(m_LbType)
                    {
                    case RL_LEADERBOARD2_TYPE_PLAYER:
                        //Gamer handle
                        attr = el->FindAttribute("ph");
                        rcheck(attr, catchall, rlTaskError("Failed to find 'ph' attribute"));
                        attr->GetStringRepr(hstr, true);

						//Do we need to deal with rlLeaderboard2ReadByGamerByPlatformTask.
						if (IsCrossPlatformQuery())
						{
							if (!curRow->m_GamerHandle.FromString(hstr))
							{
								rlTaskWarning("Failed to parse gamer handle: %s", hstr);

								rcheck(rlPresence::GetGamerHandle(m_LocalGamerIndex, &curRow->m_GamerHandle),
									catchall,
									rlTaskError("Failed to parse local gamer handle: m_LocalGamerIndex=%d", m_LocalGamerIndex));
							}
						}
						else
						{
							rcheck(curRow->m_GamerHandle.FromString(hstr),
								catchall,
								rlTaskError("Failed to parse gamer handle: %s", hstr));
						}
	
                        //Gamer name
                        attr = el->FindAttribute("pn");
                        rcheck(attr, catchall, rlTaskError("Failed to find 'pn' attribute"));
                        attr->GetStringRepr(curRow->m_GamerDisplayName, true);
                        break;
                    case RL_LEADERBOARD2_TYPE_CLAN_MEMBER:
                        //Gamer handle
                        attr = el->FindAttribute("ph");
                        rcheck(attr, catchall, rlTaskError("Failed to find 'ph' attribute"));
                        attr->GetStringRepr(hstr, true);
                        rcheck(curRow->m_GamerHandle.FromString(hstr),
                                catchall,
                                rlTaskError("Failed to parse gamer handle: %s", hstr));
                        //Gamer name
                        attr = el->FindAttribute("pn");
                        rcheck(attr, catchall, rlTaskError("Failed to find 'pn' attribute"));
                        attr->GetStringRepr(curRow->m_GamerDisplayName, true);

                        //fall thru to get group identifiers

                    case RL_LEADERBOARD2_TYPE_CLAN:
                        //Clan ID
                        attr = el->FindAttribute("ch");
                        rcheck(attr, catchall, rlTaskError("Failed to find 'ch' attribute"));
                        strVal = attr->GetStringValue();
                        rverify(1 == sscanf(strVal, "%" I64FMT "d", &curRow->m_ClanId),
                                catchall,
                                rlTaskError("Error parsing clan ID for leaderboard: 0x%08x", m_LbId));
                        //Clan name
                        attr = el->FindAttribute("cn");
                        if(attr)
                        {
                            attr->GetStringRepr(curRow->m_ClanName, true);
                        }
                        else
                        {
                            formatf(curRow->m_ClanName, "%" I64FMT "d", curRow->m_ClanId);
                        }
                        //Clan tag
                        attr = el->FindAttribute("ct");
                        if(attr)
                        {
                            attr->GetStringRepr(curRow->m_ClanTag, true);
                        }
                        else
                        {
                            safecpy(curRow->m_ClanTag, curRow->m_ClanName);
                        }
                        //Clan member count
                        attr = el->FindAttribute("cc");
                        if(attr)
                        {
                            strVal = attr->GetStringValue();

                            if(1 != sscanf(strVal, "%u", &curRow->m_ClanMemberCount))
                            {
                                curRow->m_ClanMemberCount = 0;
                            }
                            else
                            {
                                rlTaskError("Error parsing 'cc' (clan count) '%s'",
                                            strVal ? strVal : "null");
                            }
                        }
                        else
                        {
                            curRow->m_ClanMemberCount = 0;
                            rlTaskError("'cc' (clan count) attribute is missing");
                        }
                        break;
                    case RL_LEADERBOARD2_TYPE_GROUP:
                        break;
                    case RL_LEADERBOARD_TYPE_INVALID:
                    default:
                        rlAssert(false);
                    }

                    curRow->m_NumValues = 0;

                    //Get the value for the ranking column
					for(int i = 0; i < (int)m_NumColumns; ++i)
					{
						if(m_Columns[i].IsRankingColumn)
						{
							curRow->m_ColumnValues[i].Int64Val = ratingValue.Int64Val;
							++curRow->m_NumValues;
                            break;
						}

                        curRow->m_ColumnValues[i].Int64Val = 0;
					}

                    //Parse the stats values from the CSV.
                    const char* columnCsv = rowNode->GetData();
                    unsigned numColumnsReturned = 0;

                    //It's possible to read a row with no columns if
                    //the only stat value submitted was the ranking column.
                    bool done = (NULL == columnCsv);

                    for(; curRow->m_NumValues < curRow->m_MaxValues && !done; ++numColumnsReturned)
                    {
                        char kvPair[64];
                        unsigned bufLen = sizeof(kvPair);
                        done = parMemberArray::ReadCsvDatum(kvPair, &bufLen, columnCsv);

                        if(!bufLen)
                        {
                            //Empty result.
                            continue;
                        }

                        unsigned colId;
                        rlStatValue colVal;

                        rverify(2 == sscanf(kvPair, "%u=%" I64FMT "d", &colId, &colVal.Int64Val),
                                catchall,
                                rlTaskError("Error parsing column result %d for leaderboard: 0x%08x",
                                            numColumnsReturned, m_LbId));

                        rlTaskDebug3("  column index=%u  columnId=0x%08x, value=%" I64FMT "d",
                                    numColumnsReturned, colId, colVal.Int64Val);

						bool found = false;
					    for(int i = 0; i < (int)m_NumColumns; ++i)
					    {
                            if(colId == m_Columns[i].Id)
                            {
								curRow->m_ColumnValues[i].Int64Val = colVal.Int64Val;
								++curRow->m_NumValues;
								found = true;
								break;
                            }
                        }

						if(!found)
						{
							rlTaskError("Extraneous column id: %u was returned in the results", colId);
						}
                    }

                    ++pRow;
                }

                *m_NumRowsReturned = unsigned(pRow - m_Rows);
            }
        }
        rcatchall
        {
            return false;
        }

        return true;
    }

    virtual const char* GetServiceMethod() const
    {
        return m_ServiceMethod;
    }

	virtual bool IsCrossPlatformQuery() const
	{
		return false;
	}

    int m_LbId;
    rlLeaderboard2Type m_LbType;
    const rlLeaderboardColumnInfo* m_Columns;
    unsigned m_NumColumns;
    rlLeaderboard2Row** m_Rows;
    unsigned* m_NumRowsReturned;
    unsigned* m_TotalRows;
    unsigned m_MaxRows;
    const char* m_ServiceMethod;
	int m_LocalGamerIndex;
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2GetLeaderboardSizeTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2GetLeaderboardSizeTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2GetLeaderboardSizeTask);

	rlLeaderboard2GetLeaderboardSizeTask()
    {
    }

    virtual ~rlLeaderboard2GetLeaderboardSizeTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    rlClanId clanId,
                    unsigned* totalRows)
    {
        rtry
        {
            switch(lbType)
            {
            case RL_LEADERBOARD2_TYPE_PLAYER:
                m_ServiceMethod = "Leaderboards.asmx/ReadPlayersByRank";
                break;
            case RL_LEADERBOARD2_TYPE_CLAN:
                m_ServiceMethod = "Leaderboards.asmx/ReadCrewsByRank";
                break;
            case RL_LEADERBOARD2_TYPE_CLAN_MEMBER:
                m_ServiceMethod = "Leaderboards.asmx/ReadCrewMembersByRank";
                break;
            case RL_LEADERBOARD2_TYPE_GROUP:
                m_ServiceMethod = "Leaderboards.asmx/ReadGroupsByRank";
                break;
            default:
                m_ServiceMethod = "UNKNOWN";
                break;
            }

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            0,
                                                            NULL,
                                                            0,
                                                            NULL,
                                                            NULL,
                                                            totalRows), catchall,);

            if(RL_LEADERBOARD2_TYPE_CLAN_MEMBER == lbType)
            {
                rlAssert(clanId > 0);

                rcheck(AddIntParameter("crewId", clanId),
                        catchall,
                        rlTaskError("Error adding 'crewId' parameter"));
            }

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadByRankTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadByRankTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadByRankTask);

	rlLeaderboard2ReadByRankTask()
    {
    }

    virtual ~rlLeaderboard2ReadByRankTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    rlClanId clanId,
                    const int startRank,
                    const unsigned numRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            switch(lbType)
            {
            case RL_LEADERBOARD2_TYPE_PLAYER:
                m_ServiceMethod = "Leaderboards.asmx/ReadPlayersByRank";
                break;
            case RL_LEADERBOARD2_TYPE_CLAN:
                m_ServiceMethod = "Leaderboards.asmx/ReadCrewsByRank";
                break;
            case RL_LEADERBOARD2_TYPE_CLAN_MEMBER:
                m_ServiceMethod = "Leaderboards.asmx/ReadCrewMembersByRank";
                break;
            case RL_LEADERBOARD2_TYPE_GROUP:
                m_ServiceMethod = "Leaderboards.asmx/ReadGroupsByRank";
                break;
            default:
                m_ServiceMethod = "UNKNOWN";
                break;
            }

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            numRows,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            rcheck(AddIntParameter("startRank", startRank),
                    catchall,
                    rlTaskError("Error adding 'startRank' parameter"));

            rcheck(AddIntParameter("numRows", numRows),
                    catchall,
                    rlTaskError("Error adding 'numRows' parameter"));

            if(RL_LEADERBOARD2_TYPE_CLAN_MEMBER == lbType)
            {
                rlAssert(clanId > 0);

                rcheck(AddIntParameter("crewId", clanId),
                        catchall,
                        rlTaskError("Error adding 'crewId' parameter"));
            }

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadByGamerTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadByGamerTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadByGamerTask);

	rlLeaderboard2ReadByGamerTask()
    {
    }

    virtual ~rlLeaderboard2ReadByGamerTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    rlClanId clanId,
                    const rlGamerHandle* gamerHandles,
                    const unsigned numGamers,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            switch(lbType)
            {
            case RL_LEADERBOARD2_TYPE_PLAYER:
                m_ServiceMethod = "Leaderboards.asmx/ReadByPlayers";
                break;
            case RL_LEADERBOARD2_TYPE_CLAN_MEMBER:
                m_ServiceMethod = "Leaderboards.asmx/ReadByCrewMembers";
                break;
            default:
                m_ServiceMethod = "UNKNOWN";
                break;
            }

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            numGamers,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            rcheck(BeginListParameter("gamerHandleCsv"),
                    catchall,
                    rlTaskError("Error adding 'gamerHandleCsv' parameter"));

            for(int i = 0; i < (int)numGamers; ++i)
            {
                char ghStr[256];
                rcheck(AddListParamStringValue(gamerHandles[i].ToString(ghStr)),
                        catchall,
                        rlTaskError("Error adding gamer handle parameter"));
            }

            if(RL_LEADERBOARD2_TYPE_CLAN_MEMBER == lbType)
            {
                rlAssert(clanId > 0);

                rcheck(AddIntParameter("crewIdCsv", clanId),
                        catchall,
                        rlTaskError("Error adding 'crewIdCsv' parameter"));
            }

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadByGamerByPlatformTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadByGamerByPlatformTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadByGamerByPlatformTask);

	rlLeaderboard2ReadByGamerByPlatformTask()
    {
    }

    virtual ~rlLeaderboard2ReadByGamerByPlatformTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type UNUSED_PARAM(lbType),
                    const rlLeaderboard2GroupSelector& groupSelector,
                    const char* gamerHandle, //Note: This isn't an rlGamerHandle since it's for an arbitrary platform
                    const char* platformName,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            m_ServiceMethod = "Leaderboards.asmx/ReadByPlayersByPlatform";

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            RL_LEADERBOARD2_TYPE_PLAYER,
                                                            groupSelector,
                                                            1, //Only supporting a single gamer currently
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            rcheck(BeginListParameter("gamerHandleCsv"),
                    catchall,
                    rlTaskError("Error adding 'gamerHandleCsv' parameter"));

            rcheck(AddListParamStringValue(gamerHandle),
                        catchall,
                        rlTaskError("Error adding gamer handle parameter"));

            rcheck(AddStringParameter("platformName", platformName, true),
                        catchall,
                        rlTaskError("Error adding 'platformName' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

	virtual bool IsCrossPlatformQuery() const
	{
		return true;
	}

};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadByClanTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadByClanTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadByClanTask);

	rlLeaderboard2ReadByClanTask()
    {
    }

    virtual ~rlLeaderboard2ReadByClanTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    const rlClanId* clanIds,
                    const unsigned numClans,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            m_ServiceMethod = "Leaderboards.asmx/ReadByCrews";

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            RL_LEADERBOARD2_TYPE_CLAN,
                                                            groupSelector,
                                                            numClans,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            rcheck(BeginListParameter("crewIdCsv"),
                    catchall,
                    rlTaskError("Error adding 'crewIdCsv' parameter"));

            for(int i = 0; i < (int)numClans; ++i)
            {
                rcheck(AddListParamIntValue(clanIds[i]),
                        catchall,
                        rlTaskError("Error adding clan ID parameter"));
            }

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadByGroupTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadByGroupTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadByGroupTask);

	rlLeaderboard2ReadByGroupTask()
    {
    }

    virtual ~rlLeaderboard2ReadByGroupTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2GroupSelector* groupSelectors,
                    const unsigned numGroups,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            m_ServiceMethod = "Leaderboards.asmx/ReadByGroups";

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            RL_LEADERBOARD2_TYPE_GROUP,
                                                            groupSelectors,
                                                            numGroups,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadGamersByRadiusTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadGamersByRadiusTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadGamersByRadiusTask);

	rlLeaderboard2ReadGamersByRadiusTask()
    {
    }

    virtual ~rlLeaderboard2ReadGamersByRadiusTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    rlClanId clanId,
                    const rlGamerHandle& pivotGamerHandle,
                    const unsigned numRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            switch(lbType)
            {
            case RL_LEADERBOARD2_TYPE_PLAYER:
                m_ServiceMethod = "Leaderboards.asmx/ReadNearPlayer";
                break;
            case RL_LEADERBOARD2_TYPE_CLAN_MEMBER:
                m_ServiceMethod = "Leaderboards.asmx/ReadNearCrewMember";
                break;
            default:
                m_ServiceMethod = "UNKNOWN";
                break;
            }

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            numRows,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            char ghStr[256];
            rcheck(AddStringParameter("gamerHandle", pivotGamerHandle.ToString(ghStr), true),
                    catchall,
                    rlTaskError("Error adding 'gamerHandle' parameter"));

            rcheck(AddIntParameter("numRows", numRows),
                    catchall,
                    rlTaskError("Error adding 'numRows' parameter"));

            if(RL_LEADERBOARD2_TYPE_CLAN_MEMBER == lbType)
            {
                rlAssert(clanId > 0);

                rcheck(AddIntParameter("crewId", clanId),
                        catchall,
                        rlTaskError("Error adding 'crewId' parameter"));
            }

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadGamersByScoreTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadGamersByScoreTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadGamersByScoreTask);

	rlLeaderboard2ReadGamersByScoreTask()
    {
    }

    virtual ~rlLeaderboard2ReadGamersByScoreTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    rlClanId clanId,
                    const rlStatValue pivotScore,
                    const int pivotScoreType,
                    const unsigned numRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            switch(lbType)
            {
            case RL_LEADERBOARD2_TYPE_PLAYER:
                m_ServiceMethod = "Leaderboards.asmx/ReadNearPlayerValue";
                break;
            case RL_LEADERBOARD2_TYPE_CLAN_MEMBER:
                m_ServiceMethod = "Leaderboards.asmx/ReadNearCrewMemberValue";
                break;
            default:
                m_ServiceMethod = "UNKNOWN";
                break;
            }

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            numRows,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            if(RL_STAT_TYPE_INT32 == pivotScoreType || RL_STAT_TYPE_INT64 == pivotScoreType)
            {
                rcheck(AddIntParameter("value", pivotScore.Int64Val),
                        catchall,
                        rlTaskError("Error adding 'value' parameter"));
            }
            else if(RL_STAT_TYPE_FLOAT == pivotScoreType || RL_STAT_TYPE_DOUBLE == pivotScoreType)
            {
                rcheck(AddDoubleParameter("value", pivotScore.DoubleVal),
                        catchall,
                        rlTaskError("Error adding 'value' parameter"));
            }
            else
            {
                rverify(false,
                        catchall,
                        rlTaskError("Invalid pivot score type: %d", pivotScoreType));
            }

            rcheck(AddIntParameter("numRows", numRows),
                    catchall,
                    rlTaskError("Error adding 'numRows' parameter"));

            if(RL_LEADERBOARD2_TYPE_CLAN_MEMBER == lbType)
            {
                rlAssert(clanId > 0);

                rcheck(AddIntParameter("crewId", clanId),
                        catchall,
                        rlTaskError("Error adding 'crewId' parameter"));
            }

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadClansByRadiusTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadClansByRadiusTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadClansByRadiusTask);

	rlLeaderboard2ReadClansByRadiusTask()
    {
    }

    virtual ~rlLeaderboard2ReadClansByRadiusTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    const rlClanId pivotClanId,
                    const unsigned numRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            m_ServiceMethod = "Leaderboards.asmx/ReadNearCrew";

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            numRows,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            rcheck(AddIntParameter("crewId", pivotClanId),
                    catchall,
                    rlTaskError("Error adding 'crewId' parameter"));

            rcheck(AddIntParameter("numRows", numRows),
                    catchall,
                    rlTaskError("Error adding 'numRows' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadClansByScoreTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadClansByScoreTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadClansByScoreTask);

	rlLeaderboard2ReadClansByScoreTask()
    {
    }

    virtual ~rlLeaderboard2ReadClansByScoreTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    const rlStatValue pivotScore,
                    const int pivotScoreType,
                    const unsigned numRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            m_ServiceMethod = "Leaderboards.asmx/ReadNearCrewValue";

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            numRows,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            if(RL_STAT_TYPE_INT32 == pivotScoreType || RL_STAT_TYPE_INT64 == pivotScoreType)
            {
                rcheck(AddIntParameter("value", pivotScore.Int64Val),
                        catchall,
                        rlTaskError("Error adding 'value' parameter"));
            }
            else if(RL_STAT_TYPE_FLOAT == pivotScoreType || RL_STAT_TYPE_DOUBLE == pivotScoreType)
            {
                rcheck(AddDoubleParameter("value", pivotScore.DoubleVal),
                        catchall,
                        rlTaskError("Error adding 'value' parameter"));
            }
            else
            {
                rverify(false,
                        catchall,
                        rlTaskError("Invalid pivot score type: %d", pivotScoreType));
            }

            rcheck(AddIntParameter("numRows", numRows),
                    catchall,
                    rlTaskError("Error adding 'numRows' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadGroupsByRadiusTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadGroupsByRadiusTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadGroupsByRadiusTask);

	rlLeaderboard2ReadGroupsByRadiusTask()
    {
    }

    virtual ~rlLeaderboard2ReadGroupsByRadiusTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& pivotGroupSelector,
                    const unsigned numRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            m_ServiceMethod = "Leaderboards.asmx/ReadNearGroup";

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            pivotGroupSelector,
                                                            numRows,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            rcheck(AddIntParameter("numRows", numRows),
                    catchall,
                    rlTaskError("Error adding 'numRows' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ReadGroupsByScoreTask
//////////////////////////////////////////////////////////////////////////
class rlLeaderboard2ReadGroupsByScoreTask : public rlLeaderboard2CommonReadTask
{
public:
	RL_TASK_USE_CHANNEL(rline_stats);
	RL_TASK_DECL(rlLeaderboard2ReadGroupsByScoreTask);

	rlLeaderboard2ReadGroupsByScoreTask()
    {
    }

    virtual ~rlLeaderboard2ReadGroupsByScoreTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const int leaderboardId,
                    const rlLeaderboard2Type lbType,
                    const rlLeaderboard2GroupSelector& groupSelector,
                    const rlStatValue pivotScore,
                    const int pivotScoreType,
                    const unsigned numRows,
                    const rlLeaderboardColumnInfo* columns,
                    const unsigned numColumns,
                    rlLeaderboard2Row** rows,
                    unsigned* numRowsReturned,
                    unsigned* totalRows)
    {
        rtry
        {
            m_ServiceMethod = "Leaderboards.asmx/ReadNearGroup";

            rcheck(rlLeaderboard2CommonReadTask::Configure(localGamerIndex,
                                                            leaderboardId,
                                                            lbType,
                                                            groupSelector,
                                                            numRows,
                                                            columns,
                                                            numColumns,
                                                            rows,
                                                            numRowsReturned,
                                                            totalRows), catchall,);

            if(RL_STAT_TYPE_INT32 == pivotScoreType || RL_STAT_TYPE_INT64 == pivotScoreType)
            {
                rcheck(AddIntParameter("value", pivotScore.Int64Val),
                        catchall,
                        rlTaskError("Error adding 'value' parameter"));
            }
            else if(RL_STAT_TYPE_FLOAT == pivotScoreType || RL_STAT_TYPE_DOUBLE == pivotScoreType)
            {
                rcheck(AddDoubleParameter("value", pivotScore.DoubleVal),
                        catchall,
                        rlTaskError("Error adding 'value' parameter"));
            }
            else
            {
                rverify(false,
                        catchall,
                        rlTaskError("Invalid pivot score type: %d", pivotScoreType));
            }

            rcheck(AddIntParameter("numRows", numRows),
                    catchall,
                    rlTaskError("Error adding 'numRows' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2Row
//////////////////////////////////////////////////////////////////////////
rlLeaderboard2Row::rlLeaderboard2Row()
    : m_ColumnValues(NULL)
    , m_MaxValues(0)
{
    this->Clear();
}

void
rlLeaderboard2Row::Init(rlStatValue* columnValues, const unsigned maxValues)
{
    m_ColumnValues = columnValues;
    m_MaxValues = maxValues;
    this->Clear();
}

void
rlLeaderboard2Row::Clear()
{
    m_GamerHandle.Clear();
    m_GamerDisplayName[0] = '\0';
    m_GroupSelector.Clear();
    m_ClanId = RL_INVALID_CLAN_ID;
    m_ClanName[0] = '\0';
    m_ClanTag[0] = '\0';
    m_ClanMemberCount = 0;
    m_Rank = 0;
    if(m_MaxValues > 0)
    {
        sysMemSet(m_ColumnValues, 0, sizeof(m_ColumnValues[0])*m_MaxValues);
    }
    m_NumValues = 0;
}

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2Update
//////////////////////////////////////////////////////////////////////////
rlLeaderboard2Update::rlLeaderboard2Update()
: m_LeaderboardId(~0u)
, m_ClanId(RL_INVALID_CLAN_ID)
, m_InputValues(NULL)
, m_NumValues(0)
{
}

void
rlLeaderboard2Update::Init(rlLeaderboardInputValue* inputValues,
                            const unsigned maxValues)
{
    m_LeaderboardId = ~0u;
    m_ClanId = RL_INVALID_CLAN_ID;
    m_GroupSelector.Clear();

    m_InputValues = inputValues;
    m_NumValues = 0;
    m_MaxValues = maxValues;
}

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2PlayerStats
//////////////////////////////////////////////////////////////////////////
bool
rlLeaderboard2PlayerStats::GetLeaderboardSize(const int localGamerIndex,
                                            const int leaderboardId,
                                            const rlLeaderboard2GroupSelector& groupSelector,
                                            unsigned* numRows,
                                            netStatus* status)
{
    rlDebug("Reading size of PLAYER leaderboard 0x%08x...", leaderboardId);
#if !__NO_OUTPUT
    if(groupSelector.m_NumGroups > 0)
    {
        rlDebug("Group selector");
        for(int i = 0; i < (int)groupSelector.m_NumGroups; ++i)
        {
            rlDebug("   %s:%s", groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
        }
    }
#endif

    bool success = false;

    rlLeaderboard2GetLeaderboardSizeTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_PLAYER,
                                    groupSelector,
                                    0,      //clanId
                                    numRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2PlayerStats::ReadByRank(const int localGamerIndex,
                                        const int leaderboardId,
                                        const rlLeaderboard2GroupSelector& groupSelector,
                                        const int startRank,
                                        const unsigned numRows,
                                        const rlLeaderboardColumnInfo* columns,
                                        const unsigned numColumns,
                                        rlLeaderboard2Row** rows,
                                        unsigned* numRowsReturned,
                                        unsigned* totalRows,
                                        netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);

    rlDebug("Reading PLAYER leaderboard 0x%08x by rank, starting with rank %d...",
            leaderboardId,
            startRank);
#if !__NO_OUTPUT
    if(groupSelector.m_NumGroups > 0)
    {
        rlDebug("Group selector");
        for(int i = 0; i < (int)groupSelector.m_NumGroups; ++i)
        {
            rlDebug("   %s:%s", groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
        }
    }
#endif

    bool success = false;

    rlLeaderboard2ReadByRankTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadByRankTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_PLAYER,
                                    groupSelector,
                                    0,  //clanId
                                    startRank,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByRankTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2PlayerStats::ReadByGamer(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlGamerHandle* gamerHandles,
                            const unsigned numGamers,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status)
{
    rlAssert(gamerHandles);
    rlAssert(numGamers > 0);

    rlDebug("Reading %d rows of PLAYER leaderboard 0x%08x...",
            numGamers,
            leaderboardId);

    bool success = false;

    rlLeaderboard2ReadByGamerTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadByGamerTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_PLAYER,
                                    groupSelector,
                                    0,      //clanId
                                    gamerHandles,
                                    numGamers,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByGamerTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2PlayerStats::ReadByRadius(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlGamerHandle& pivotGamer,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status)
{
    rlAssert(numRows > 0);

#if !__NO_OUTPUT
    char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
    rlDebug("Reading %d rows of PLAYER leaderboard 0x%08x around the gamer '%s'...",
            numRows,
            leaderboardId,
            pivotGamer.ToString(ghStr));
#endif

    bool success = false;

    rlLeaderboard2ReadGamersByRadiusTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadGamersByRadiusTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_PLAYER,
                                    groupSelector,
                                    0,//clanId
                                    pivotGamer,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadGamersByRadiusTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

static bool ReadPlayersByScore(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const rlStatValue pivotScore,
                                const int pivotScoreType,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status)
{
    rlAssert(numRows > 0);

    if(RL_STAT_TYPE_DOUBLE == pivotScoreType || RL_STAT_TYPE_FLOAT == pivotScoreType)
    {
        rlDebug("Reading %d rows of PLAYER leaderboard 0x%08x around the score %f...",
                numRows,
                leaderboardId,
                pivotScore.DoubleVal);
    }
    else
    {
        rlDebug("Reading %d rows of PLAYER leaderboard 0x%08x around the score %" I64FMT "d...",
                numRows,
                leaderboardId,
                pivotScore.Int64Val);
    }

    bool success = false;

    rlLeaderboard2ReadGamersByScoreTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadGamersByScoreTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_PLAYER,
                                    groupSelector,
                                    0,      //clanId
                                    pivotScore,
                                    pivotScoreType,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadGamersByScoreTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2PlayerStats::ReadByScoreFloat(const int localGamerIndex,
                                            const int leaderboardId,
                                            const rlLeaderboard2GroupSelector& groupSelector,
                                            const double pivotScore,
                                            const unsigned numRows,
                                            const rlLeaderboardColumnInfo* columns,
                                            const unsigned numColumns,
                                            rlLeaderboard2Row** rows,
                                            unsigned* numRowsReturned,
                                            unsigned* totalRows,
                                            netStatus* status)
{
    rlStatValue pivotValue;
    pivotValue.DoubleVal = pivotScore;

    return ReadPlayersByScore(localGamerIndex,
                                leaderboardId,
                                groupSelector,
                                pivotValue,
                                RL_STAT_TYPE_DOUBLE,
                                numRows,
                                columns,
                                numColumns,
                                rows,
                                numRowsReturned,
                                totalRows,
                                status);
}

bool
rlLeaderboard2PlayerStats::ReadByScoreInt(const int localGamerIndex,
                                            const int leaderboardId,
                                            const rlLeaderboard2GroupSelector& groupSelector,
                                            const s64 pivotScore,
                                            const unsigned numRows,
                                            const rlLeaderboardColumnInfo* columns,
                                            const unsigned numColumns,
                                            rlLeaderboard2Row** rows,
                                            unsigned* numRowsReturned,
                                            unsigned* totalRows,
                                            netStatus* status)
{
    rlStatValue pivotValue;
    pivotValue.Int64Val = pivotScore;

    return ReadPlayersByScore(localGamerIndex,
                                leaderboardId,
                                groupSelector,
                                pivotValue,
                                RL_STAT_TYPE_INT64,
                                numRows,
                                columns,
                                numColumns,
                                rows,
                                numRowsReturned,
                                totalRows,
                                status);
}

bool
rlLeaderboard2PlayerStats::ReadByGamerByPlatformTask(const int localGamerIndex,
														const int leaderboardId,
														const rlLeaderboard2GroupSelector& groupSelector,
														const char* gamerHandle,
														const char* platformName,
														const rlLeaderboardColumnInfo* columns,
														const unsigned numColumns,
														rlLeaderboard2Row** rows,
														unsigned* numRowsReturned,
														unsigned* totalRows,
														netStatus* status)
{
	rlDebug("ReadByGamerByPlatformTask leaderboard 0x%08x...", leaderboardId);

	bool success = false;

	rlLeaderboard2ReadByGamerByPlatformTask* task = NULL;

	rtry
	{
		rverify(gamerHandle,
			catchall,
			rlError("Gamer handle is NULL"));

		rverify(platformName,
			catchall,
			rlError("Platform Name is NULL"));

		rlDebug("Reading rows of PLAYER %s for platform %s..."
			,gamerHandle
			,platformName);

		rverify(rlGetTaskManager()->CreateTask(&task),
			catchall,
			rlError("Error allocating rlLeaderboard2ReadByGamerByPlatformTask"));

		rverify(rlTaskBase::Configure(task,
										localGamerIndex,
										leaderboardId,
										RL_LEADERBOARD2_TYPE_PLAYER,
										groupSelector,
										gamerHandle, //Note: This isn't an rlGamerHandle since it's for an arbitrary platform
										platformName,
										columns,
										numColumns,
										rows,
										numRowsReturned,
										totalRows,
										status),
										catchall,
										rlError("Error configuring rlLeaderboard2ReadByGamerByPlatformTask"));

		rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}

bool
rlLeaderboard2PlayerStats::Write(const int localGamerIndex,
                                const rlLeaderboard2Update** updates,
                                const unsigned numUpdates,
                                netStatus* status)
{
    rlAssert(numUpdates > 0);

    rlDebug("Writing stats for local gamer %d...", localGamerIndex);

    bool success = false;

    rlLeaderboard2WriteTask* task = NULL;

    rtry
    {
		rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_LEADERBOARD_WRITE),
			catchall,
			rlError("Leaderboard write privilege is revoked"));

        for(int i = 0; i < (int)numUpdates; ++i)
        {
            rverify(updates[i],
                    catchall,
                    rlError("Invalid leaderboard update"));

            rverify(updates[i]->m_NumValues > 0,
                    catchall,
                    rlError("Invalid number of input values in leaderboard update"));
        }

        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlRosReadStatsTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    updates,
                                    numUpdates,
                                    status),
                catchall,
                rlError("Error configuring rlRosReadStatsTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

void
rlLeaderboard2PlayerStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ClanStats
//////////////////////////////////////////////////////////////////////////
bool
rlLeaderboard2ClanStats::GetLeaderboardSize(const int localGamerIndex,
                                            const int leaderboardId,
                                            const rlLeaderboard2GroupSelector& groupSelector,
                                            unsigned* numRows,
                                            netStatus* status)
{
    rlDebug("Reading size of CLAN leaderboard 0x%08x...", leaderboardId);
#if !__NO_OUTPUT
    if(groupSelector.m_NumGroups > 0)
    {
        rlDebug("Group selector");
        for(int i = 0; i < (int)groupSelector.m_NumGroups; ++i)
        {
            rlDebug("   %s:%s", groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
        }
    }
#endif

    bool success = false;

    rlLeaderboard2GetLeaderboardSizeTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN,
                                    groupSelector,
                                    0,      //clanId
                                    numRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanStats::ReadByRank(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& groupSelector,
                                    const int startRank,
                                    const unsigned numRows,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    rlLeaderboard2Row** rows,
                                    unsigned* numRowsReturned,
                                    unsigned* totalRows,
                                    netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);

    rlDebug("Reading CLAN leaderboard 0x%08x by rank, starting with rank %d...",
            leaderboardId,
            startRank);
#if !__NO_OUTPUT
    if(groupSelector.m_NumGroups > 0)
    {
        rlDebug("Group selector");
        for(int i = 0; i < (int)groupSelector.m_NumGroups; ++i)
        {
            rlDebug("   %s:%s", groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
        }
    }
#endif

    bool success = false;

    rlLeaderboard2ReadByRankTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadByRankTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN,
                                    groupSelector,
                                    0,  //clanId
                                    startRank,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByRankTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanStats::ReadByClan(const int localGamerIndex,
                        const int leaderboardId,
                        const rlLeaderboard2GroupSelector& groupSelector,
                        const rlClanId* clanIds,
                        const unsigned numClanIds,
                        const rlLeaderboardColumnInfo* columns,
                        const unsigned numColumns,
                        rlLeaderboard2Row** rows,
                        unsigned* numRowsReturned,
                        unsigned* totalRows,
                        netStatus* status)
{
    rlAssert(clanIds);
    rlAssert(numClanIds > 0);

    rlDebug("Reading %d rows of CLAN leaderboard 0x%08x...",
            numClanIds,
            leaderboardId);

    bool success = false;

    rlLeaderboard2ReadByClanTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadByClanTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    groupSelector,
                                    clanIds,
                                    numClanIds,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByClanTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanStats::ReadByRadius(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlClanId pivotClanId,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status)
{
    rlAssert(numRows > 0);

#if !__NO_OUTPUT
    rlDebug("Reading %d rows of CLAN leaderboard 0x%08x around the clan %" I64FMT "d...",
            numRows,
            leaderboardId,
            pivotClanId);
#endif

    bool success = false;

    rlLeaderboard2ReadClansByRadiusTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadClansByRadiusTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN,
                                    groupSelector,
                                    pivotClanId,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadClansByRadiusTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

static bool ReadClansByScore(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& groupSelector,
                                    const rlStatValue pivotScore,
                                    const int pivotScoreType,
                                    const unsigned numRows,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    rlLeaderboard2Row** rows,
                                    unsigned* numRowsReturned,
                                    unsigned* totalRows,
                                    netStatus* status)
{
    rlAssert(numRows > 0);

    if(RL_STAT_TYPE_DOUBLE == pivotScoreType || RL_STAT_TYPE_FLOAT == pivotScoreType)
    {
        rlDebug("Reading %d rows of CLAN leaderboard 0x%08x around the score %f...",
                numRows,
                leaderboardId,
                pivotScore.DoubleVal);
    }
    else
    {
        rlDebug("Reading %d rows of CLAN leaderboard 0x%08x around the score %" I64FMT "d...",
                numRows,
                leaderboardId,
                pivotScore.Int64Val);
    }

    bool success = false;

    rlLeaderboard2ReadClansByScoreTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadClansByScoreTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN,
                                    groupSelector,
                                    pivotScore,
                                    pivotScoreType,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadClansByScoreTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanStats::ReadByScoreFloat(const int localGamerIndex,
                                        const int leaderboardId,
                                        const rlLeaderboard2GroupSelector& groupSelector,
                                        const double pivotScore,
                                        const unsigned numRows,
                                        const rlLeaderboardColumnInfo* columns,
                                        const unsigned numColumns,
                                        rlLeaderboard2Row** rows,
                                        unsigned* numRowsReturned,
                                        unsigned* totalRows,
                                        netStatus* status)
{
    rlStatValue pivotValue;
    pivotValue.DoubleVal = pivotScore;

    return ReadClansByScore(localGamerIndex,
                            leaderboardId,
                            groupSelector,
                            pivotValue,
                            RL_STAT_TYPE_DOUBLE,
                            numRows,
                            columns,
                            numColumns,
                            rows,
                            numRowsReturned,
                            totalRows,
                            status);
}

bool
rlLeaderboard2ClanStats::ReadByScoreInt(const int localGamerIndex,
                                        const int leaderboardId,
                                        const rlLeaderboard2GroupSelector& groupSelector,
                                        const s64 pivotScore,
                                        const unsigned numRows,
                                        const rlLeaderboardColumnInfo* columns,
                                        const unsigned numColumns,
                                        rlLeaderboard2Row** rows,
                                        unsigned* numRowsReturned,
                                        unsigned* totalRows,
                                        netStatus* status)
{
    rlStatValue pivotValue;
    pivotValue.Int64Val = pivotScore;

    return ReadClansByScore(localGamerIndex,
                            leaderboardId,
                            groupSelector,
                            pivotValue,
                            RL_STAT_TYPE_INT64,
                            numRows,
                            columns,
                            numColumns,
                            rows,
                            numRowsReturned,
                            totalRows,
                            status);
}

void
rlLeaderboard2ClanStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2ClanMemberStats
//////////////////////////////////////////////////////////////////////////
bool
rlLeaderboard2ClanMemberStats::GetLeaderboardSize(const int localGamerIndex,
                                                    const int leaderboardId,
                                                    const rlLeaderboard2GroupSelector& groupSelector,
                                                    const rlClanId clanId,
                                                    unsigned* numRows,
                                                    netStatus* status)
{
    rlDebug("Reading size of CLAN_MEMBER leaderboard 0x%08x for clan %" I64FMT "d...",
            leaderboardId,
            clanId);
#if !__NO_OUTPUT
    if(groupSelector.m_NumGroups > 0)
    {
        rlDebug("Group selector");
        for(int i = 0; i < (int)groupSelector.m_NumGroups; ++i)
        {
            rlDebug("   %s:%s", groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
        }
    }
#endif

    bool success = false;

    rlLeaderboard2GetLeaderboardSizeTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN_MEMBER,
                                    groupSelector,
                                    clanId,
                                    numRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanMemberStats::ReadByRank(const int localGamerIndex,
                                            const int leaderboardId,
                                            const rlLeaderboard2GroupSelector& groupSelector,
                                            const rlClanId clanId,
                                            const int startRank,
                                            const unsigned numRows,
                                            const rlLeaderboardColumnInfo* columns,
                                            const unsigned numColumns,
                                            rlLeaderboard2Row** rows,
                                            unsigned* numRowsReturned,
                                            unsigned* totalRows,
                                            netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);

    rlDebug("Reading CLAN_MEMBER leaderboard 0x%08x for clan %" I64FMT "d by rank, starting with rank %d...",
            leaderboardId,
            clanId,
            startRank);
#if !__NO_OUTPUT
    if(groupSelector.m_NumGroups > 0)
    {
        rlDebug("Group selector");
        for(int i = 0; i < (int)groupSelector.m_NumGroups; ++i)
        {
            rlDebug("   %s:%s", groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
        }
    }
#endif

    bool success = false;

    rlLeaderboard2ReadByRankTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadByRankTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN_MEMBER,
                                    groupSelector,
                                    clanId,
                                    startRank,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByRankTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanMemberStats::ReadByMember(const int localGamerIndex,
                                            const int leaderboardId,
                                            const rlLeaderboard2GroupSelector& groupSelector,
                                            const rlClanId clanId,
                                            const rlGamerHandle* gamerHandles,
                                            const unsigned numMembers,
                                            const rlLeaderboardColumnInfo* columns,
                                            const unsigned numColumns,
                                            rlLeaderboard2Row** rows,
                                            unsigned* numRowsReturned,
                                            unsigned* totalRows,
                                            netStatus* status)
{
    rlAssert(gamerHandles);
    rlAssert(numMembers > 0);

    rlDebug("Reading %d rows of CLAN_MEMBER leaderboard 0x%08x for clan %" I64FMT "d...",
            numMembers,
            leaderboardId,
            clanId);

    bool success = false;

    rlLeaderboard2ReadByGamerTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadByGamerTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN_MEMBER,
                                    groupSelector,
                                    clanId,
                                    gamerHandles,
                                    numMembers,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByGamerTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanMemberStats::ReadByRadius(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const rlClanId clanId,
                                const rlGamerHandle& pivotMember,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status)
{
    rlAssert(numRows > 0);

#if !__NO_OUTPUT
    char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
    rlDebug("Reading %d rows of CLAN_MEMBER leaderboard 0x%08x around the gamer '%s' for clan %" I64FMT "d...",
            numRows,
            leaderboardId,
            pivotMember.ToString(ghStr),
            clanId);
#endif

    bool success = false;

    rlLeaderboard2ReadGamersByRadiusTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadGamersByRadiusTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN_MEMBER,
                                    groupSelector,
                                    clanId,
                                    pivotMember,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadGamersByRadiusTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

static bool ReadClanMembersByScore(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& groupSelector,
                                    const rlClanId clanId,
                                    const rlStatValue pivotScore,
                                    const int pivotScoreType,
                                    const unsigned numRows,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    rlLeaderboard2Row** rows,
                                    unsigned* numRowsReturned,
                                    unsigned* totalRows,
                                    netStatus* status)
{
    rlAssert(numRows > 0);

    if(RL_STAT_TYPE_DOUBLE == pivotScoreType || RL_STAT_TYPE_FLOAT == pivotScoreType)
    {
        rlDebug("Reading %d rows of CLAN_MEMBER leaderboard 0x%08x around the score %f for clan %" I64FMT "d...",
                numRows,
                leaderboardId,
                pivotScore.DoubleVal,
                clanId);
    }
    else
    {
        rlDebug("Reading %d rows of CLAN_MEMBER leaderboard 0x%08x around the score %" I64FMT "d for clan %" I64FMT "d...",
                numRows,
                leaderboardId,
                pivotScore.Int64Val,
                clanId);
    }

    bool success = false;

    rlLeaderboard2ReadGamersByScoreTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadGamersByScoreTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_CLAN_MEMBER,
                                    groupSelector,
                                    clanId,
                                    pivotScore,
                                    pivotScoreType,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadGamersByScoreTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2ClanMemberStats::ReadByScoreFloat(const int localGamerIndex,
                                                const int leaderboardId,
                                                const rlLeaderboard2GroupSelector& groupSelector,
                                                const rlClanId clanId,
                                                const double pivotScore,
                                                const unsigned numRows,
                                                const rlLeaderboardColumnInfo* columns,
                                                const unsigned numColumns,
                                                rlLeaderboard2Row** rows,
                                                unsigned* numRowsReturned,
                                                unsigned* totalRows,
                                                netStatus* status)
{
    rlStatValue pivotValue;
    pivotValue.DoubleVal = pivotScore;

    return ReadClanMembersByScore(localGamerIndex,
                                leaderboardId,
                                groupSelector,
                                clanId,
                                pivotValue,
                                RL_STAT_TYPE_DOUBLE,
                                numRows,
                                columns,
                                numColumns,
                                rows,
                                numRowsReturned,
                                totalRows,
                                status);
}

bool
rlLeaderboard2ClanMemberStats::ReadByScoreInt(const int localGamerIndex,
                                                const int leaderboardId,
                                                const rlLeaderboard2GroupSelector& groupSelector,
                                                const rlClanId clanId,
                                                const s64 pivotScore,
                                                const unsigned numRows,
                                                const rlLeaderboardColumnInfo* columns,
                                                const unsigned numColumns,
                                                rlLeaderboard2Row** rows,
                                                unsigned* numRowsReturned,
                                                unsigned* totalRows,
                                                netStatus* status)
{
    rlStatValue pivotValue;
    pivotValue.Int64Val = pivotScore;

    return ReadClanMembersByScore(localGamerIndex,
                                leaderboardId,
                                groupSelector,
                                clanId,
                                pivotValue,
                                RL_STAT_TYPE_INT64,
                                numRows,
                                columns,
                                numColumns,
                                rows,
                                numRowsReturned,
                                totalRows,
                                status);
}

void
rlLeaderboard2ClanMemberStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2GroupStats
//////////////////////////////////////////////////////////////////////////
bool
rlLeaderboard2GroupStats::GetLeaderboardSize(const int localGamerIndex,
                                            const int leaderboardId,
                                            const rlLeaderboard2GroupSelector& categorySelector,
                                            unsigned* numRows,
                                            netStatus* status)
{
    rlAssertf(categorySelector.m_NumGroups > 0, "No categories defined in the category selector");

    rlDebug("Reading size of GROUP leaderboard 0x%08x...", leaderboardId);
#if !__NO_OUTPUT
    rlDebug("Group selector");
    for(int i = 0; i < (int)categorySelector.m_NumGroups; ++i)
    {
        rlDebug("   %s:%s", categorySelector.m_Group[i].m_Category, categorySelector.m_Group[i].m_Id);
    }
#endif

    bool success = false;

    rlLeaderboard2GetLeaderboardSizeTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_GROUP,
                                    categorySelector,
                                    0,      //clanId
                                    numRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2GetLeaderboardSizeTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2GroupStats::ReadByRank(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& categorySelector,
                                    const int startRank,
                                    const unsigned numRows,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    rlLeaderboard2Row** rows,
                                    unsigned* numRowsReturned,
                                    unsigned* totalRows,
                                    netStatus* status)
{
    rlAssertf(startRank >= 1, "Start rank must be at least 1");
    rlAssert(numRows > 0);
    rlAssertf(categorySelector.m_NumGroups > 0, "No categories defined in the category selector");

    rlDebug("Reading GROUP leaderboard 0x%08x by rank, starting with rank %d...",
            leaderboardId,
            startRank);
#if !__NO_OUTPUT
    rlDebug("Group selector");
    for(int i = 0; i < (int)categorySelector.m_NumGroups; ++i)
    {
        rlDebug("   %s:%s", categorySelector.m_Group[i].m_Category, categorySelector.m_Group[i].m_Id);
    }
#endif

    bool success = false;

    rlLeaderboard2ReadByRankTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadByRankTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    RL_LEADERBOARD2_TYPE_GROUP,
                                    categorySelector,
                                    0,      //clanId
                                    startRank,
                                    numRows,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByRankTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2GroupStats::ReadByGroup(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector* groupSelectors,
                            const unsigned numGroups,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status)
{
    rlAssert(groupSelectors);
    rlAssert(numGroups > 0);

    rlDebug("Reading %d rows of GROUP leaderboard 0x%08x...",
            numGroups,
            leaderboardId);

    bool success = false;

    rlLeaderboard2ReadByGroupTask* task = NULL;

    rtry
    {
        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error allocating rlLeaderboard2ReadTask"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    leaderboardId,
                                    groupSelectors,
                                    numGroups,
                                    columns,
                                    numColumns,
                                    rows,
                                    numRowsReturned,
                                    totalRows,
                                    status),
                catchall,
                rlError("Error configuring rlLeaderboard2ReadByGroupTask"));

        rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlLeaderboard2GroupStats::ReadByRadius(const int localGamerIndex,
                                        const int leaderboardId,
                                        const rlLeaderboard2GroupSelector& pivotGroupSelector,
                                        const unsigned numRows,
                                        const rlLeaderboardColumnInfo* columns,
                                        const unsigned numColumns,
                                        rlLeaderboard2Row** rows,
                                        unsigned* numRowsReturned,
                                        unsigned* totalRows,
                                        netStatus* status)
{
    rlAssert(numRows > 0);
    rlAssertf(pivotGroupSelector.m_NumGroups > 0, "No categories defined in the pivot group selector");

#if !__NO_OUTPUT
    rlDebug("Reading %d rows of GROUP leaderboard 0x%08x around the group:",
            numRows,
            leaderboardId);

    for(int i = 0; i < (int)pivotGroupSelector.m_NumGroups; ++i)
    {
        rlDebug("   %s:%s", pivotGroupSelector.m_Group[i].m_Category, pivotGroupSelector.m_Group[i].m_Id);
    }
#endif

    bool success = false;

    rlLeaderboard2ReadGroupsByRadiusTask* task = NULL;

    rtry
    {
            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error allocating rlLeaderboard2ReadGroupsByRadiusTask"));

            rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        leaderboardId,
                                        RL_LEADERBOARD2_TYPE_GROUP,
                                        pivotGroupSelector,
                                        numRows,
                                        columns,
                                        numColumns,
                                        rows,
                                        numRowsReturned,
                                        totalRows,
                                        status),
                    catchall,
                    rlError("Error configuring rlLeaderboard2ReadGroupsByRadiusTask"));

            rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

            success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

void
rlLeaderboard2GroupStats::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

}   //namespace rage
