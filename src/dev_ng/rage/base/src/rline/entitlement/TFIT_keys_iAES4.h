#ifndef _TFIT_KEY_IAES4_H_
#define _TFIT_KEY_IAES4_H_

#ifdef __RGSC_DLL
	#if RSG_CPU_X64 && (RSG_PC || __RGSC_DLL) && !PRELOADED_SOCIAL_CLUB
		#define ENTITLEMENT_TRANSFORMIT_APPLICABLE 1
	#else	
		#define ENTITLEMENT_TRANSFORMIT_APPLICABLE 0
	#endif
#else
	#if RSG_CPU_X64 && RSG_PC 
		#define ENTITLEMENT_TRANSFORMIT_APPLICABLE 1
	#else	
		#define ENTITLEMENT_TRANSFORMIT_APPLICABLE 0
	#endif
#endif

#if ENTITLEMENT_TRANSFORMIT_APPLICABLE

#ifndef _INTPTR
#define _INTPTR 1
#endif

#ifdef __cplusplus
extern "C" { 
#endif

/* Include the header that defines the key type for this instance */
#include "TFIT_defs_iAES4.h"

extern TFIT_key_iAES4_t TFIT_key_iAES4;

#ifdef __cplusplus
}
#endif

#endif // ENTITLEMENT_TRANSFORMIT_APPLICABLE
#endif //_TFIT_KEY_IAES4_H_

