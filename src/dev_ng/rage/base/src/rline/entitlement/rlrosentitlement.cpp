// 
// rline/entitlement/rlrosentitlement.cpp
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 
#include "rlrosentitlement.h"
#include "rlrosentitlementtask.h"

#include "atl/string.h"
#include "data/base64.h"
#include "data/datprotect.h"
#include "data/sha1.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "parser/treenode.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroscommon.h"
#include "string/stringhash.h"
#include "system/endian.h"
#include "system/nelem.h"
#include "system/service.h"
#include "system/xtl.h"
#include <time.h>
#if RSG_PC  // Entitlements are only for PC
//We're explicitly using CyaSSL's AES since dat/aes isn't standard AES
#include "wolfssl/wolfcrypt/aes.h"
#include "TFIT_keys_iAES4.h"

#define ENTITLEMENT_TRANSFORMIT_DECRYPT 1 && ENTITLEMENT_TRANSFORMIT_APPLICABLE

const int timeBuffLengthSize = 128;
#if ENTITLEMENT_TRANSFORMIT_DECRYPT 
	#include <TFIT_defs_iAES4.h>
	#include <TFIT_AES_CBC_Decrypt_iAES4.h>
	#pragma comment(lib, "TFIT_AES_CBC_Decrypt_iAES4_64.lib")
	#pragma comment(lib, "TFIT_Utils_64.lib")
#endif
namespace rage
{

	RAGE_DECLARE_SUBCHANNEL(rline, entitlement)
	RAGE_DEFINE_SUBCHANNEL(rline, entitlement)
	#undef __rage_channel
	#define __rage_channel rline_entitlement

#define RLENTITLEMENT_CREATETASK(T, status, localGamerIndex, ...) 	                        \
	bool success = false;																	\
	rlTaskBase* task = NULL;                                                                \
	rtry																					\
	{																						\
		rcheck(rlRos::IsOnline(localGamerIndex),                                            \
				catchall,                                                                   \
				rlDebug("Local gamer %d is not signed into ROS", localGamerIndex));         \
		T* newTask = rlGetTaskManager()->CreateTask<T>();									\
		rverify(newTask,catchall,);															\
		task = newTask;																		\
		rverify(rlTaskBase::Configure(newTask, localGamerIndex, __VA_ARGS__, status),		\
					catchall, rlError("Failed to configure task"));                         \
		rverify(rlGetTaskManager()->AppendSerialTask(task), catchall, );       			    \
		success = true;	                                                                    \
	}																						\
	rcatchall																				\
	{																						\
		if(task)																			\
		{																					\
			rlGetTaskManager()->DestroyTask(task);								            \
		}																					\
	}																						\
	return success;																			

#define RLENTITLEMENT_CREATE_IOTASK(T, status, ...)												\
	if (IsEntitlementIOInProgress())															\
	{																							\
		return false;																			\
	}																							\
	T* task;																					\
	rtry																						\
	{																							\
		rverify(netTask::Create(&task, status), 												\
			catchall, rlError("Error allocating task"));										\
																								\
		rverify(task->Configure(__VA_ARGS__),													\
			catchall, rlError("Error configuring task"));										\
																								\
		rverify(netTask::Run(task), catchall, rlError("Error scheduling task"));				\
																								\
	}																							\
	rcatchall																					\
	{																							\
		netTask::Destroy(task);																	\
		return false;																			\
	}																							\
	return true;


netStatus	rlRosEntitlement::sm_EntitlementIOStatus;
bool		rlRosEntitlement::sm_RlRosEntitlementInitialized = false;
u8*			rlRosEntitlement::sm_OfflineEntitlementData = NULL;
unsigned	rlRosEntitlement::sm_OfflineEntitlementDataLen = 0;

#if __STEAM_BUILD
cSteamDlcCallbackObject rlRosEntitlement::sm_SteamDlcCallbackObject;
#endif

bool rlRosEntitlement::Init()
{
	//@@: location RLROSENTITLEMENT_INIT
    sm_RlRosEntitlementInitialized = true;
	return true;
}

bool 
rlRosEntitlement::IsInitialized()
{
	return sm_RlRosEntitlementInitialized;
}

void 
rlRosEntitlement::Shutdown()
{
	if(sm_RlRosEntitlementInitialized)
	{
		sm_RlRosEntitlementInitialized = false;
	}

	if (sm_EntitlementIOStatus.Pending())
	{
		netTask::Cancel(&sm_EntitlementIOStatus);
	}
#if !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
	ClearOfflineEntitlement();
#endif
}

void 
rlRosEntitlement::Update()
{

}

bool
rlRosEntitlement::GetSkuDownloadToken( const int localGamerIndex, const char* skuName, const char* serialNum, atMap<u32, atString> *downloadTokens, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosEntitlementDownloadTokenTask,
								status,
								localGamerIndex,
								skuName,
								serialNum,
								downloadTokens);
}

bool
rlRosEntitlement::GetValidateContentPlayerAge(const int localGamerIndex, const char* contentName, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosEntitlementValidateContentPlayerAgeTask,
								status,
								localGamerIndex,
								contentName);
}

bool
rlRosEntitlement::GetValidateContentAge(const int localGamerIndex, const char* contentName, const char* countryCode, int age, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosEntitlementValidateContentAgeTask,
								status,
								localGamerIndex,
								contentName,
								countryCode,
								age);
}

bool
rlRosEntitlement::GetEntitlementDownloadURL(const int localGamerIndex, const char* entitlementCode, char* downloadUrl, unsigned int downloadUrlBufLen, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosEntitlementDownloadURLTask,
								status,
								localGamerIndex,
								entitlementCode,
								downloadUrl,
								downloadUrlBufLen);
}

#if RSG_CPU_X64
bool 
rlRosEntitlement::GetVoucherContent(int localGamerIndex, const char* voucherKey, const char* languageCode, rlV2VoucherPreviewResponse* responseData, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosEntitlementPreviewVoucher,
		status,
		localGamerIndex,
		voucherKey,
		languageCode,
		responseData
		);
}

bool 
rlRosEntitlement::ConsumeVoucher(int localGamerIndex, const char* voucherKey, int grantTokenVersion, const char* grantTokenString, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosEntitlementVoucherConsume,
		status,
		localGamerIndex,
		voucherKey,
		grantTokenVersion,
		grantTokenString
		);
}

bool
rlRosEntitlement::GetCommerceManifest(int localGamerIndex, const char* languageCode, const char* countryCode, const char* versionString, atString* manifestString, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosFetchCommerceManifest,
		status,
		localGamerIndex,
		languageCode,
		countryCode,
		versionString,
		manifestString);
}

#if !PRELOADED_SOCIAL_CLUB 
bool
rlRosEntitlement::GetEntitlements(int localGamerIndex, const char* locale, const char* machineHash, rlV2EntitlementResponse* responseData, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosGetEntitlements,
							status,
							localGamerIndex,
							locale,
							machineHash,
							responseData);
}

#if RSG_ENTITLEMENT_ENABLED
bool 
rlRosEntitlement::GetEntitlementBlock(int localGamerIndex, const char* machineHash, const char* locale, char* outputBuffer, int outputBufferSize, int* outputVersionNum, unsigned int* outputDataSize, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosGetEntitlementBlock,
		status,
		localGamerIndex,
		machineHash,
		locale,
		outputBuffer,
		outputBufferSize,
		outputVersionNum,
		outputDataSize);
}

//This is the full fat entitlement block function, used to get the instances of a function
//Returns true on success, false on error. Output is via the entitlement array
bool rlRosEntitlement::DoesDataBlockGrantEntitlement( s64 rockstarId, const char* machineHash, void* data, int dataSize, const char* entitlementName)
{
	//DO AMIR MAGIC HERE!
	rlV2EntitlementDataBlock dataBlock;

	// Create our buffer for building the string
	char timeBuff[timeBuffLengthSize] = {0};
	rtry
	{
		
		//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CHECK_SIZE
		rverify(dataSize > 0, catchall, );
		//@@: range RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_PERTINENT_CODE {
		
		rcheck(ENTITLEMENT_DB_ERROR_NONE == 
		//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_DECODE_CALL
			rlV2EntitlementDataBlock::Decode(static_cast<u8*>(data), (unsigned)dataSize, dataBlock), catchall, );

		//Make sure the machineHash/account matches
		//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_VERIFY_ROCKSTAR_ID
		rverify(rockstarId == dataBlock.m_RockstarId, catchall, );
		rverify(strcmp(machineHash, dataBlock.m_MachineHash) == 0, catchall, );

		//@@: } RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_PERTINENT_CODE

		
		
		// Our time in seconds
		time_t currentTimeInSeconds;
		struct tm tm_struct;
		// Fetch the time
		//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_GET_CURRENT_TIME
		time(&currentTimeInSeconds);

		//@@: range RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_EARLY_OUTS {
		if (currentTimeInSeconds == -1) 
			return false;

		// Convert the time in seconds to the time structure.
#if RSG_ORBIS
		if(gmtime_s(&currentTimeInSeconds, &tm_struct) == NULL)
			return false;
#else
		//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CALL_GMTIME
		errno_t err = gmtime_s(&tm_struct, &currentTimeInSeconds);

		if (err) 
			return false;
#endif 
		//@@: } RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_EARLY_OUTS


		// I think this serves as our 30 day window too? That assumes
		// SCS has the expiry date as 30 days from date of issue.

		//TODO: I hate using strcmp here, but converting it to epoch time
		// will cause a lot of bloat. I can't think of a scenario will this
		// would fail. Someone keep me honest.
		//@@: range RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CHECK_ENTITLED_A {
		// Sprintf it into our buffer, adding a month to account for months starting at 0
		sprintf(timeBuff, "%04d-%02d-%02dT%02d:%02d:%02d", tm_struct.tm_year + 1900,
			tm_struct.tm_mon+1,
			tm_struct.tm_mday,
			tm_struct.tm_hour,
			tm_struct.tm_min,
			tm_struct.tm_sec);
		//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_STRCMP_EXPIRY
		rverify(strcmp(timeBuff, dataBlock.m_ExpiryDate) <= 0, catchall, );
		// Now lets check for more than 30 days
		// Empty out our time-buff
		memset(timeBuff, 0, timeBuffLengthSize);
		// Add two months to the struct, but now we need to normalize because
		// that could spill into the next year
		tm_struct.tm_mon+=2;
		tm_struct.tm_mday+=1;
		mktime(&tm_struct);
		sprintf(timeBuff, "%04d-%02d-%02dT%02d:%02d:%02d", tm_struct.tm_year + 1900,
			tm_struct.tm_mon,
			tm_struct.tm_mday,
			tm_struct.tm_hour,
			tm_struct.tm_min,
			tm_struct.tm_sec);
		// Check the max time
		rverify(strcmp(dataBlock.m_ExpiryDate, timeBuff) <= 0, catchall, );
		//@@: } RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CHECK_ENTITLED_A

	}
	rcatchall
	{
		//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_RCATCHALL
		dataBlock.Reset();
		memset(timeBuff, 0, timeBuffLengthSize);
		return false;
	}
	//Add any entitlements matching this code
	
	bool isEntitled = false;
	//@@: range RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CHECK_ENTITLED_B {
	for (int k = 0; k < dataBlock.m_EntitlementResponse.m_EntitlementInstanceArray.GetCount(); k++)
	{
		const rlV2EntitlementInstance& instance = dataBlock.m_EntitlementResponse.m_EntitlementInstanceArray[k];
		
		if (instance.m_Count > 0
			&& strcmp(instance.m_Code, entitlementName) == 0 
			&& (
				strcmp(timeBuff, dataBlock.m_EntitlementResponse.m_EntitlementInstanceArray[k].m_ExpireDateUtc)<=0 
				|| dataBlock.m_EntitlementResponse.m_EntitlementInstanceArray[k].m_ExpireDateUtc==0
				)
			)
		{
			//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_FOUND_ENTITLEMENTS
			isEntitled = true;
		}	
	}
	//@@: } RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CHECK_ENTITLED_B
	//@@: range RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CHECK_RETURNS {
	//@@: location RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_AFTER_CHECKS
	memset(timeBuff, 0, timeBuffLengthSize);
	dataBlock.Reset();
	return isEntitled;
    
	//@@: } RLROSENTITLEMENT_DOESDATABLOCKGRANTENTITLEMENT_CHECK_RETURNS	
}

bool 
rlRosEntitlement::GetEntitlementUrl(int localGamerIndex, const char* entitlementCode, char* urlBuffer, int urlBufferLength, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosGetEntitlementDownloadUrl,
		status,
		localGamerIndex,
		entitlementCode,
		urlBuffer,
		urlBufferLength);
}
#endif //RSG_ENTITLEMENT_ENABLED
#endif //!PRELOADED_SOCIAL_CLUB

bool 
rlRosEntitlement::GetContentUrl(int localGamerIndex, const char* contentPath, char* urlBuffer, int urlBufferLength, netStatus* status)
{
    RLENTITLEMENT_CREATETASK(rlRosGetContentUrl,
        status,
        localGamerIndex,
        contentPath,
        urlBuffer,
        urlBufferLength);
}

bool
rlRosEntitlement::GetCheckoutUrl(int localGamerIndex, const char* offerId, const char* priceId, const char* language, const char* country, char* checkoutBuffer, int checkoutBufferLength, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosGetCheckoutUrl,
		status,
		localGamerIndex,
		offerId,
		priceId,
		language,
		country,
		checkoutBuffer,
		checkoutBufferLength);
}

bool 
rlRosEntitlement::BeginSteamPurchase(int localGamerIndex, u32 appId, const char* offerId, s64 steamId, const char* language, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosBeginSteamPurchase,
		status,
		localGamerIndex,
		appId,
		offerId,
		steamId,
		language);
}

bool 
rlRosEntitlement::FinaliseSteamPurchase(int localGamerIndex, u32 appId, s64 orderId, netStatus* status)
{
	RLENTITLEMENT_CREATETASK(rlRosFinaliseSteamPurchase,
		status,
		localGamerIndex,
		appId,
		orderId);
}
#endif

#if !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
u8* rlRosEntitlement::AllocateOfflineEntitlementBlob(unsigned size)
{
	rtry
	{
		rverify(sm_OfflineEntitlementData == NULL, catchall, );
		rverify(sm_OfflineEntitlementDataLen == 0, catchall, );

		sm_OfflineEntitlementData = rage_new u8[size];

		rverify(sm_OfflineEntitlementData, catchall, );
		sm_OfflineEntitlementDataLen = size;
		
		return sm_OfflineEntitlementData;
	}
	rcatchall
	{
		return NULL;
	}
}

#if RSG_PC
bool rlRosEntitlement::SaveOfflineEntitlement(u8* entitlementBuf, unsigned entitlementBufLen, u8* machineHash)
{
	RLENTITLEMENT_CREATE_IOTASK(rlRosEntitlementSaveOfflineTask, &sm_EntitlementIOStatus, entitlementBuf, entitlementBufLen, machineHash);
}

bool rlRosEntitlement::LoadOfflineEntitlement(u8* machineHash)
{
	RLENTITLEMENT_CREATE_IOTASK(rlRosEntitlementLoadOfflineTask, &sm_EntitlementIOStatus, machineHash);
}
#endif // RSG_PC

bool rlRosEntitlement::ClearOfflineEntitlement()
{
	rtry
	{
		rverify(!IsEntitlementIOInProgress(), catchall, );

		if (sm_OfflineEntitlementData)
		{
			delete[] sm_OfflineEntitlementData;
		}

		sm_OfflineEntitlementData = NULL;
		sm_OfflineEntitlementDataLen = 0;

		return true;
	}
	rcatchall
	{
		return false;
	}
}
#endif //!PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED

//Container function for horrible sku ifdef action
const char* rlRosEntitlement::GetSkuName()
{
#if __STEAM_BUILD
	return "steam";
#else
	return "dr";
#endif
}

#if __STEAM_BUILD
bool rlRosEntitlement::IsSteamDlcInstalled(int appId)
{
	rtry
	{
		rverify(SteamApps(), catchall, rlError("SteamApps() was null, is Steam API initialized?"));
		return SteamApps()->BIsDlcInstalled((AppId_t)appId);
	}
	rcatchall
	{
		return false;
	}
}

cSteamDlcCallbackObject::cSteamDlcCallbackObject() 
	: m_SteamDlcInstalled(this, &cSteamDlcCallbackObject::OnSteamDlcInstalled) 
{
}

void cSteamDlcCallbackObject::OnSteamDlcInstalled( DlcInstalled_t *pParam )
{
	if (pParam != NULL)
	{
		rlDebug3("OnSteamDlcInstalled event raised for app id: %d", pParam->m_nAppID);

		// Trigger entitlement updated event
		sysServiceEvent e(sysServiceEvent::ENTITLEMENT_UPDATED);
		g_SysService.TriggerEvent(&e);
	}
}

cSteamEntitlementCallbackObject::cSteamEntitlementCallbackObject( int localGamerIndex, steamTransactionAuthorisedCallback callback ) :
	m_SteamCallbackHandle(this, &cSteamEntitlementCallbackObject::OnSteamTransactionAuthorizationResponse),
    m_TransactionAuthorisedCallback(callback),
	m_LocalGamerIndex(localGamerIndex)
{

}

void cSteamEntitlementCallbackObject::OnSteamTransactionAuthorizationResponse( MicroTxnAuthorizationResponse_t *pParam )
{
	if(pParam != NULL && pParam->m_bAuthorized)
	{
		Errorf("OrderId %d",pParam->m_ulOrderID);
		if ( !m_FinaliseSteamOrderStatus.Pending() )
		{
			rlRosEntitlement::FinaliseSteamPurchase(m_LocalGamerIndex,SteamUtils()->GetAppID(),pParam->m_ulOrderID,&m_FinaliseSteamOrderStatus);
		}    
	}

    //Callback to the game with the authorisation flag
    if (m_TransactionAuthorisedCallback != NULL)
    {
        (m_TransactionAuthorisedCallback)(pParam->m_bAuthorized == 1);
    }
}
#endif

#if RSG_CPU_X64 && RSG_ENTITLEMENT_ENABLED
rlV2EntitlementDataBlock::rlV2EntitlementDataBlock()
: m_Version(0)
, m_RockstarId(0)
, m_MachineHash()
, m_ExpiryDate()
, m_EntitlementResponse()
{
	//@@: location RLV2ENTITLEMENTDATABLOCK_CONSTRUCTOR
}

#if !PRELOADED_SOCIAL_CLUB 
#if ENTITLEMENT_TRANSFORMIT_DECRYPT 
	//Getting our keys from the TFIT_keys_iAES4.h file
#else
	// Get our key from the static base64 key
	// TODO: Talk with the server guys about using a different key.
	volatile char base64Key[] = "2ncuilqLtredzL6+Qf+ZHrfycIlEBc60szMx1i+o0Y0=";
#endif
/*static*/
rlV2EntitlementDataBlockError rlV2EntitlementDataBlock::Decode(u8* data, unsigned dataLen, rlV2EntitlementDataBlock& outBlock)
{
	//Read the plaintext/length
	u8* plaintext = NULL;
	int plaintextLength = 0;
	rtry
	{
		
		unsigned readOffset = 0;

		//Read 4 byte version
		rverify(dataLen >= readOffset + sizeof(outBlock.m_Version), catchall, );
		sysMemCpy(&outBlock.m_Version, data + readOffset, sizeof(outBlock.m_Version));
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_BTON_VERSION
		outBlock.m_Version = sysEndian::BtoN(outBlock.m_Version);
		//We only support version 1 currently
		rcheck(outBlock.m_Version == 1, version, rlError("Version %d does not match expected", outBlock.m_Version));
		readOffset += sizeof(outBlock.m_Version);
		
		//Read 16 byte IV
		u8 aesIV[AES_BLOCK_SIZE];
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_READ_IV_FROM_BLOB
		rverify(dataLen >= readOffset + sizeof(aesIV), catchall, );
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_READ_IV_FROM_BLOB_TWO
		sysMemCpy(aesIV, data + readOffset, sizeof(aesIV));
		readOffset += sizeof(aesIV);

#if ENTITLEMENT_TRANSFORMIT_DECRYPT
		//@@: range RLV2ENTITLEMENTDATABLOCK_DECODE_TFIT_DECRYPT {
		wbaes_ctx_t cbcCtx; 
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_GET_DECODED_SIZE
		rverify(TFIT_init_wbaes_cbc_iAES4(&cbcCtx, &TFIT_key_iAES4, aesIV) == 0, catchall, );
		unsigned int bytesWritten = 0;
		int numBytesToWrite = (dataLen - readOffset) - ((dataLen-readOffset) % AES_BLOCK_SIZE);
		rverify(TFIT_update_wbaes_iAES4(&cbcCtx, data+readOffset, numBytesToWrite, data+readOffset, numBytesToWrite, &bytesWritten) == 0, catchall, );
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_TFIT_FINAL_AES
		rverify(TFIT_final_wbaes_iAES4(&cbcCtx, data+readOffset, numBytesToWrite,  &bytesWritten) == 0, catchall, );
#else
		//Decode our AES key

		
		unsigned keyLen = datBase64::GetMaxDecodedSize((const char *)base64Key);
		u8* key = Alloca(u8, keyLen);
		rverify(key, catchall,);
		rverify(datBase64::Decode((const char *)base64Key, keyLen, key, &keyLen), catchall, );
		//Decrypt the block (in-place)
		Aes aes;
		sysMemSet(&aes, 0, sizeof(aes));
		wc_AesInit(&aes, nullptr, INVALID_DEVID);
		rverify(0 == AesSetKey(&aes, key, keyLen, aesIV, AES_DECRYPTION), catchall, );
		rverify(0 == AesCbcDecrypt(&aes, data + readOffset, data + readOffset, dataLen - readOffset), catchall, );
#endif
		
		
		rverify(dataLen >= readOffset + sizeof(plaintextLength), catchall, );
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODEVERIFY_LENGTH
		sysMemCpy(&plaintextLength, data + readOffset, sizeof(plaintextLength));
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODEVERIFY_BTON_PLAINTEXTLENGTH
		plaintextLength = sysEndian::BtoN(plaintextLength);
		readOffset += sizeof(plaintextLength);
		//Make sure the plaintextlength is within our bounds
		rverify(plaintextLength >= 0 && dataLen >= readOffset + plaintextLength, catchall, );
		//Store our plaintext
		plaintext = data + readOffset;
		//Skip past our plaintext
		readOffset += plaintextLength;
		//@@: } RLV2ENTITLEMENTDATABLOCK_DECODE_TFIT_DECRYPT

		//Read the signature/length
		u8* signature = NULL;
		int signatureLength;
		rverify(dataLen >= readOffset + sizeof(signatureLength), catchall, );
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_RVERIFY_LENGTH
		sysMemCpy(&signatureLength, data + readOffset, sizeof(signatureLength));
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_SET_SIGNATURE_LENGTH
		signatureLength = sysEndian::BtoN(signatureLength);
		readOffset += sizeof(signatureLength);
		//Make sure the signatureLength is within our bounds
		rverify(signatureLength >= 0 && dataLen >= readOffset + signatureLength, catchall, );
		//Store our signature
		signature = data + readOffset;
		
		//Copy the signature to our rlRosContentSignature construct
		rlRosContentSignature signatureCopy;
		rverify(signatureLength <= NELEM(signatureCopy.m_ContentSignature), catchall, );
		sysMemCpy(signatureCopy.m_ContentSignature, signature, signatureLength);
		signatureCopy.m_Length = signatureLength;

		//@@: range RLV2ENTITLEMENTDATABLOCK_DECODE_C {
		//Compute the sha1 hash so we can compute our signature
		Sha1 sha1;
		
		sha1.Update(plaintext, plaintextLength);
		u8 hash[Sha1::SHA1_DIGEST_LENGTH];
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_FINAL_SHA
		sha1.Final(hash);

		//And finally verify our signature
		rverify(rlRosHttpFilter::VerifySignature(signatureCopy, 
												 RLROS_SIGNATURE_ENTITLEMENTS,
												 NULL,
												 0,
												 hash,
												 Sha1::SHA1_DIGEST_LENGTH),
				signature, );

		//Read rockstar id from plaintext
		int plaintextOffset = 0;
		rverify(plaintextLength >= plaintextOffset + (int)sizeof(outBlock.m_RockstarId), catchall, );
		sysMemCpy(&outBlock.m_RockstarId, plaintext + plaintextOffset, sizeof(outBlock.m_RockstarId));
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_BTON_ROCKSTARID
		outBlock.m_RockstarId = sysEndian::BtoN(outBlock.m_RockstarId);
		plaintextOffset += sizeof(outBlock.m_RockstarId);

		//Read machine hash
		int machineHashLength;
		rverify(plaintextLength >= plaintextOffset + (int)sizeof(machineHashLength), catchall, );

		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_SZ_HASH_LEN
		sysMemCpy(&machineHashLength, plaintext + plaintextOffset, sizeof(machineHashLength));
		machineHashLength = sysEndian::BtoN(machineHashLength);
		plaintextOffset += sizeof(machineHashLength);
		//Make sure machineHashLength is within our bounds
		rverify(machineHashLength >= 0 && plaintextLength >= plaintextOffset + machineHashLength, catchall, );
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_HASH_PTR_CALC
		u8* machineHash = plaintext + plaintextOffset;
		//rlVerify the null term is where we expect
		rverify(machineHash[machineHashLength] == '\0', catchall, );
		outBlock.m_MachineHash = (char*)machineHash;

		//Skip past the machine hash
		plaintextOffset += machineHashLength;

		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_SKIP_HASH
		//Read expiry date
		int expiryDateLength;
		rverify(plaintextLength >= plaintextOffset + (int)sizeof(expiryDateLength), catchall, );
		sysMemCpy(&expiryDateLength, plaintext + plaintextOffset, sizeof(expiryDateLength));
		expiryDateLength = sysEndian::BtoN(expiryDateLength);
		plaintextOffset += sizeof(expiryDateLength);
		//Make sure expiryDateLength is within our bounds
		rverify(expiryDateLength >= 0 && plaintextLength >= plaintextOffset + expiryDateLength, catchall, );

		u8* expiryDate = plaintext + plaintextOffset;
		//rlVerify the null term is where we expect
		rverify(expiryDate[expiryDateLength] == '\0', catchall, );
		outBlock.m_ExpiryDate = (char*)expiryDate;

		//Skip past the expiry date
		plaintextOffset += expiryDateLength;


		//Read the block of entitlements data
		int entitlementsDataLength;
		rverify(plaintextLength >= plaintextOffset + (int)sizeof(entitlementsDataLength), catchall, );
		sysMemCpy(&entitlementsDataLength, plaintext + plaintextOffset, sizeof(entitlementsDataLength));
		entitlementsDataLength = sysEndian::BtoN(entitlementsDataLength);
		plaintextOffset += sizeof(entitlementsDataLength);
		//Make sure entitlementsDataLength is valid
		rverify(entitlementsDataLength >= 0 && plaintextLength >= plaintextOffset + entitlementsDataLength, catchall, );

		u8* entitlementsData = plaintext + plaintextOffset;

		//Now actually construct our entitlements XML in memory
		char memFileName[RAGE_MAX_PATH ];
		rlDebug3("Entitlement XML:\n%s", entitlementsData);

		fiDeviceMemory::MakeMemoryFileName(memFileName, sizeof(memFileName), (const char*)entitlementsData, entitlementsDataLength, false, NULL);

        // Initializing the parser for usage
		INIT_PARSER;
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
		parTree* tree = PARSER.LoadTree(memFileName, "xml");
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);
		rverify(tree && tree->GetRoot(), catchall, );
		//Use our task to parse the <Entitlements> node that our root should be
		rverify(rlRosGetEntitlements::ParseEntitlementResponse(tree->GetRoot(), outBlock.m_EntitlementResponse), catchall, );
		//@@: } RLV2ENTITLEMENTDATABLOCK_DECODE_C

		SanitizeRecursive(tree->GetRoot());

        // Dropping the shutdown below the tree accessors
		
		SHUTDOWN_PARSER;
		memset(memFileName, 0, sizeof(memFileName));
		memset(plaintext, 0, plaintextLength);
		//@@: range RLV2ENTITLEMENTDATABLOCK_DECODE_RETURNS {
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_EXIT_A
		return ENTITLEMENT_DB_ERROR_NONE;
	}
	rcatch(version)
	{
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_EXIT_B
		memset(plaintext, 0,plaintextLength);
		return ENTITLEMENT_DB_ERROR_VERSION_MISMATCH;
	}
	//TODO:
	/*
	rcatch(expired)
	{
	return ENTITLEMENT_DB_ERROR_EXPIRED;
	}
	*/
	rcatch(signature)
	{
		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_EXIT_D
		memset(plaintext, 0,plaintextLength);
		return ENTITLEMENT_DB_ERROR_SIGNATURE_MISMATCH;
	}
	rcatchall
	{

		//@@: location RLV2ENTITLEMENTDATABLOCK_DECODE_EXIT_E
		memset(plaintext, 0,plaintextLength);
		return ENTITLEMENT_DB_ERROR_UNKNOWN;
	}
	//@@: } RLV2ENTITLEMENTDATABLOCK_DECODE_RETURNS

}

void rlV2EntitlementDataBlock::SanitizeRecursive(parTreeNode* node)
{
	if (!node)
		return;
	
	// Call recurively, depth-first
	SanitizeRecursive(node->GetChild());
	SanitizeRecursive(node->GetSibling());

	parElement& element = node->GetElement();
	
	if (element.GetFlags().IsSet(parElement::FLAG_OWNS_NAME_STR))
	{
		const char* name = element.GetName();
		size_t len = strlen(name);

		sysMemSet(const_cast<char*>(name), 0, len);
	}

	
	atArray<parAttribute>& attrs = element.GetAttributes();

	class sanitizableParAttribute : public parAttribute
	{
	public:
		void Sanitize()
		{
			if (m_Flags.IsSet(FLAG_OWNS_NAME_STR))
			{
				char* str = const_cast<char*>(m_Name);
				sysMemSet(str, 0, strlen(str));
			}

			if (GetType() == STRING && m_Flags.IsSet(FLAG_OWNS_VALUE_STR))
			{
				char* str = const_cast<char*>(GetStringValue());
				sysMemSet(str, 0, strlen(str));
			}
		}
	};

	CompileTimeAssert(sizeof(sanitizableParAttribute) == sizeof(parAttribute));

	for (int i = 0; i < attrs.GetCount(); i++)
	{
		parAttribute& attr = attrs[i];

		sanitizableParAttribute* sattr = static_cast<sanitizableParAttribute*>(&attr);
		
		if (sattr)
		{
			sattr->Sanitize();
		}
	}
}

#endif
#endif //RSG_CPU_X64 && RSG_ENTITLEMENT_ENABLED

}
#endif // RSG_PC
