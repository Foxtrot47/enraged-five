// 
// rline/entitlement/rlrosentitlementtask.h
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROSENTITLEMENTTASKS_H
#define RLINE_RLROSENTITLEMENTTASKS_H

#include "atl/string.h"
#include "net/task.h"
#include "rline/entitlement/rlrosentitlement.h"
#include "rline/rlworker.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/ros/rlroscredtasks.h"

#if RSG_PC  // Entitlements are only for PC
namespace rage
{

// Note that this is duplicated in //depot/gta5/src/dev_ng/extra/launcher/Launcher/Entitlement.h - please keep these in sync and ensure Andrew Boothroyd, Ross Childs and Amir Soofi are aware of any changes
enum rlEntitlementErrorCode
{
	RLEN_ERROR_UNEXPECTED_RESULT = -1,              //Unhandled error code
	RLEN_ERROR_NONE = 0,                            //Success
	RLEN_ERROR_AUTHENTICATIONFAILED_TICKET,			//Invalid ROS Ticket
	RLEN_ERROR_ALREADY_EXISTS,						
	RLEN_ERROR_SERIAL_DOES_NOT_EXIST,				//Serial Number does not exist in DB
	RLEN_ERROR_SERIAL_DOES_NOT_MATCH,				//Serial Number does not match
	RLEN_ERROR_REDUNDANT_SERIAL,					//Redundant serial 
	RLEN_ERROR_SERIAL_ALREADY_ACTIVATED,			//Serial Number already activated by another user.
	RLEN_ERROR_SERIAL_NOT_ACTIVATED_ON_SERVER,		//Serial Number is owned but has not been activated on the license server
	RLEN_ERROR_INVALID_SKU,							//Invalid SKU to activate/purchase
	RLEN_ERROR_NOT_ENTITLED,					    //User is not entitled to particular content
    RLEN_ERROR_NOT_ALLOWED_RULE                     //User is not allowed to complete due to rule restriction
};

class netStatus;

class rlRosEntitlementDownloadTokenTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosEntitlementDownloadTokenTask);

	rlRosEntitlementDownloadTokenTask();

	virtual ~rlRosEntitlementDownloadTokenTask();

	bool Configure(int localGamerIndex, const char* skuName, const char* serialNum, atMap<u32, atString>* downloadTokens);

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	virtual const char* GetServiceMethod() const;

public:
	atMap<u32, atString> *m_DownloadTokens;
};

class rlRosEntitlementValidateContentPlayerAgeTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosEntitlementValidateContentPlayerAgeTask);
	rlRosEntitlementValidateContentPlayerAgeTask();
	virtual ~rlRosEntitlementValidateContentPlayerAgeTask();

	bool Configure (int localGamerIndex, const char* contentName);

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	virtual const char * GetServiceMethod() const;

};

class rlRosEntitlementValidateContentAgeTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosEntitlementValidateContentAgeTask);
	rlRosEntitlementValidateContentAgeTask();
	virtual ~rlRosEntitlementValidateContentAgeTask();

	bool Configure (int localGamerIndex, const char* contentName, const char* countryCode, int age);

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	virtual const char * GetServiceMethod() const;
};

class rlRosEntitlementDownloadURLTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosEntitlementDownloadURLTask);

	rlRosEntitlementDownloadURLTask();

	virtual ~rlRosEntitlementDownloadURLTask();

	bool Configure(int localGamerIndex, const char* entitlementCode, char* downloadURL, unsigned int downloadURLBufLen);

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	virtual const char* GetServiceMethod() const;

public:
	char* m_downloadURL;
	unsigned int m_downloadURLBufLen;
};
#if RSG_CPU_X64

class rlRosEntitlementPreviewVoucher : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosEntitlementPreviewVoucher);

	rlRosEntitlementPreviewVoucher();
	virtual ~rlRosEntitlementPreviewVoucher();

	bool Configure(int localGamerIndex, const char* redemptionCode, const char* languageCode, rlV2VoucherPreviewResponse* responseData);
	const char* GetServiceMethod() const;

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
private:
	rlV2VoucherPreviewResponse*	m_ResponseData;
};

class rlRosEntitlementVoucherConsume : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosEntitlementVoucherConsume);

	rlRosEntitlementVoucherConsume();
	virtual ~rlRosEntitlementVoucherConsume();

	bool Configure(int localGamerIndex, const char* redemptionCode, int tokenVersion, const char* tokenString);
	const char* GetServiceMethod() const;

	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
private:
};

class rlRosFetchCommerceManifest : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosFetchCommerceManifest);

	rlRosFetchCommerceManifest();
	virtual ~rlRosFetchCommerceManifest();

	bool Configure(int localGamerIndex, const char* languageCode, const char* countryCode, const char* versionString, atString* manifestString);
	const char* GetServiceMethod() const;

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
private:
	atString *m_ManifestString;
};

#if !PRELOADED_SOCIAL_CLUB 
class rlRosGetEntitlements : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosGetEntitlements);

	rlRosGetEntitlements();
	virtual ~rlRosGetEntitlements();

	bool Configure( int localGamerIndex, const char* locale, const char* machineHash, rlV2EntitlementResponse* entitlementResponseData );

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	static bool ParseEntitlementResponse(const parTreeNode* node, rlV2EntitlementResponse& outResponse);

	const char* GetServiceMethod() const;

private:
	rlV2EntitlementResponse* m_ResponseData;
};

#if RSG_ENTITLEMENT_ENABLED
class rlRosGetEntitlementBlock : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosGetEntitlementBlock);

	rlRosGetEntitlementBlock();
	virtual ~rlRosGetEntitlementBlock();

	bool Configure(int localGamerIndex, const char* machineHash, const char* locale, char* outputBuffer, int outputBufferSize, int* outputVersionNumber, unsigned int* outputDataSize);

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	const char* GetServiceMethod() const;

private:
	char*	m_OutputBuffer;
	int		m_OutputBufferSize;
	int*	m_OutputVersionNumber;
	unsigned int*	m_OutDataSize;
};
#endif //RSG_ENTITLEMENT_ENABLED
#endif //!PRELOADED_SOCIAL_CLUB

class rlRosGetCheckoutUrl : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosGetCheckoutUrl);

	rlRosGetCheckoutUrl();
	virtual ~rlRosGetCheckoutUrl();

	bool Configure( int localGamerIndex, const char* offerCode, const char* priceId, const char* language, const char* country, char* checkoutBuffer, int checkoutBufferLength );

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    //Set to true if request should be sent via HTTPS.
    virtual bool UseHttps() const
    {
        //Response contains sensitive data
        return true;
    }

	const char* GetServiceMethod() const;

private:
	char* m_CheckoutUrlBuffer;
	unsigned int m_CheckoutUrlBufferLength;
};

class rlRosBeginSteamPurchase : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosBeginSteamPurchase);

	rlRosBeginSteamPurchase();
	virtual ~rlRosBeginSteamPurchase();

	bool Configure( int localGamerIndex, u32 appId, const char* offerCode, s64 steamId, const char* language);

	const char* GetServiceMethod() const;
};

class rlRosFinaliseSteamPurchase : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosFinaliseSteamPurchase);

	rlRosFinaliseSteamPurchase();
	virtual ~rlRosFinaliseSteamPurchase();

	bool Configure( int localGamerIndex, u32 appId, s64 orderId );

	const char* GetServiceMethod() const;
};

#if !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
class rlRosGetEntitlementDownloadUrl : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosGetEntitlementDownloadUrl);

	rlRosGetEntitlementDownloadUrl();
	virtual ~rlRosGetEntitlementDownloadUrl();

	bool Configure( int localGamerIndex, const char* entitlementCode, char* urlBuffer, int urlBufferLength );

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	const char* GetServiceMethod() const;

private:
	char* m_UrlBuffer;
	unsigned int m_UrlBufferLength;
};
#endif //!PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED

class rlRosGetContentUrl : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlRosGetContentUrl);

    rlRosGetContentUrl();
    virtual ~rlRosGetContentUrl();

    bool Configure( int localGamerIndex, const char* contentPath, char* urlBuffer, int urlBufferLength );

    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    const char* GetServiceMethod() const;

private:
    char* m_UrlBuffer;
    unsigned int m_UrlBufferLength;
};
#endif //RSG_CPU_X64

#if !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
class rlRosEntitlementSaveOfflineTask : public netTask
{
public:
	RL_TASK_DECL(rlRosEntitlementSaveOfflineTask);

	rlRosEntitlementSaveOfflineTask();
	virtual ~rlRosEntitlementSaveOfflineTask();

	bool Configure(u8* entitlementBuf, unsigned entitlementBufLen, u8* machineHash);

	virtual void OnCancel();
	virtual void Complete(const netTaskStatus status);
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);

private:

	bool SaveFile();

	enum State
	{
		STATE_SAVE_FILE,
		STATE_SAVING_FILE
	};

	struct EntitlementSaveFileWorker : public rlWorker
	{
		EntitlementSaveFileWorker();
		bool Configure(u8* buf, unsigned bufLen, u8* machineHash,  netStatus* status);
		bool Start(const int cpuAffinity);

	private:

		//Make Start() un-callable
		bool Start(const char* /*name*/,
			const unsigned /*stackSize*/,
			const int /*cpuAffinity*/)
		{
			FastAssert(false);
			return false;
		}

		virtual void Perform();

		unsigned m_BufSize;
		u8* m_MachineHash;
		u8* m_BufPtr;
		unsigned m_ExportSize;

		netStatus* m_Status;
	};

	u8* m_EntitlementSaveBuf;
	unsigned m_EntitlementBufLen;
	EntitlementSaveFileWorker m_Worker;

	unsigned m_ExportSize;
	State m_State;
	netStatus m_WorkerStatus;
};

class rlRosEntitlementLoadOfflineTask : public netTask
{
public:
	RL_TASK_DECL(rlRosEntitlementLoadOfflineTask);

	rlRosEntitlementLoadOfflineTask();
	virtual ~rlRosEntitlementLoadOfflineTask();

	bool Configure(u8* machineHash);

	virtual void OnCancel();
	virtual void Complete(const netTaskStatus status);
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);

private:

	bool LoadFile();
	bool Import();

	enum State
	{
		STATE_LOAD_FILE,
		STATE_LOADING_FILE,
	};

	struct EntitlementLoadFileWorker : public rlWorker
	{
		EntitlementLoadFileWorker();
		bool Configure(u8* machineHash, netStatus* status);
		bool Start(const int cpuAffinity);

	private:

		//Make Start() un-callable
		bool Start(const char* /*name*/,
			const unsigned /*stackSize*/,
			const int /*cpuAffinity*/)
		{
			FastAssert(false);
			return false;
		}

		virtual void Perform();

		u8* m_BufPtr;
		u8* m_MachineHash;
		netStatus* m_Status;
	};

	EntitlementLoadFileWorker m_Worker;
	State m_State;
	netStatus m_WorkerStatus;
	unsigned m_BytesRead;
	bool m_FileExists;
};
#endif // !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
}
#endif //RSG_PC
#endif //!RLINE_RLROSENTITLEMENTTASKS_H
