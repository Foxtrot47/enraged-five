// 
// rline/entitlement/rlrosentitlement.h
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLROSENTITLEMENT_H
#define RLROSENTITLEMENT_H

#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"
#include "net/status.h"
#include "file/file_config.h"

#if !RSG_PC
#define RSG_ENTITLEMENT_ENABLED 0
#endif

#if RSG_PC  // Entitlements are only for PC
#if RSG_CPU_X86
// always remove entitlement code from 32-bit builds since they're not secured by GuardIT
#define PRELOADED_SOCIAL_CLUB 1
#else
#if !defined(PRELOADED_SOCIAL_CLUB)
#define PRELOADED_SOCIAL_CLUB 0
#endif
#endif

#if defined(MASTER_NO_ENTITLEMENTS)
#define RSG_ENTITLEMENT_ENABLED 0
#else
#define RSG_ENTITLEMENT_ENABLED (1 && RSG_PC && !PRELOADED_SOCIAL_CLUB)
#endif

#if __STEAM_BUILD
#pragma warning(disable: 4265)
#include "../../3rdParty/Steam/public/steam/steam_api.h"
#pragma warning(error: 4265)
#endif // __STEAM_BUILD

namespace rage
{

class parTreeNode;

//Start of new Rage Entitlement system classes
//NOTE! Names are temporary, we need to find a good way to name these to avoid confusion.

enum eV2EntitlementType
{
	ENTITLEMENT_TYPE_DURABLE = 0,
	ENTITLEMENT_TYPE_CONSUMABLE,
	ENTITLEMENT_TYPE_UNKNOWN
};

//An actual instance of an entitlement, which the user will have when they have purchased or been otherwise granted an offer
class rlV2EntitlementInstance
{
public:
	~rlV2EntitlementInstance()
	{
		Reset();
	}

	void Reset()
	{
		
		for(int i = 0; i < m_Code.GetLength(); i++)
			m_Code[i] = 0;
		for(int i = 0; i < m_FriendlyName.GetLength(); i++)
			m_FriendlyName[i] = 0;
		for(int i = 0; i < m_ExpireDateUtc.GetLength(); i++)
			m_ExpireDateUtc[i] = 0;
		for(int i = 0; i < m_CreateDateUtc.GetLength(); i++)
			m_CreateDateUtc[i] = 0;
		
		m_Code.Reset();
		m_FriendlyName.Reset();
		m_ExpireDateUtc.Reset();
		m_CreateDateUtc.Reset();
		m_Type = ENTITLEMENT_TYPE_UNKNOWN;
		m_Count = 0;
		m_InstanceId = 0;
        m_Visible = true;
	}
	atString	m_Code;
	atString	m_FriendlyName;
	atString	m_ExpireDateUtc;
	atString	m_CreateDateUtc;
    eV2EntitlementType m_Type;
	int			m_Count;
    s64       m_InstanceId;
    bool        m_Visible;
};


//Represents an entitlement entry within an offer.
//So an offer can feature one to many of these.
class rlV2OfferEntitlementData : public rlV2EntitlementInstance
{
public:
	
};

class rlV2Offer
{
public:

	void Reset()
	{
		m_Code.Reset();
		m_FriendlyName.Reset();
		m_EntitlementDataArray.Reset();
	}

	atString	m_Code;
	atString	m_FriendlyName;
	atArray<rlV2OfferEntitlementData> m_EntitlementDataArray;
};

class rlV2VoucherPreviewResponse
{
public:
	void Reset()
	{
		m_EntitlementsToGrant.Reset();
		m_Offers.Reset();
		m_GrantToken.Reset();
	}

	atArray<rlV2OfferEntitlementData>	m_EntitlementsToGrant;
	atArray<rlV2Offer>					m_Offers;
	atString							m_GrantToken;
	int									m_GrantTokenVersion;
};

class rlV2EntitlementResponse
{
public:
	void Reset()
	{
		for(int i = 0; i < m_Signature.GetLength(); i++)
			m_Signature[i] = 0;
		for(int i = 0; i < m_MachineHash.GetLength(); i++)
			m_MachineHash[i] = 0;

		m_EntitlementInstanceArray.Reset();
		m_Signature.Reset();
		m_MachineHash.Reset();
	}

	atArray<rlV2EntitlementInstance> m_EntitlementInstanceArray;
	atString m_Signature;
	atString m_MachineHash;
};

#if RSG_ENTITLEMENT_ENABLED
enum rlV2EntitlementDataBlockError
{
	ENTITLEMENT_DB_ERROR_NONE,
	ENTITLEMENT_DB_ERROR_EXPIRED,
	ENTITLEMENT_DB_ERROR_SIGNATURE_MISMATCH,
	ENTITLEMENT_DB_ERROR_VERSION_MISMATCH,
	ENTITLEMENT_DB_ERROR_UNKNOWN,
};

class rlV2EntitlementDataBlock
{
public:
	rlV2EntitlementDataBlock();

	void Reset()
	{
		//@@: location RLV2ENTITLEMENTDATABLOCK_RESET 
		m_Version = 0;
		m_RockstarId = 0;
		for(int i = 0; i < m_MachineHash.GetLength(); i++)
			m_MachineHash[i] = 0;
		for(int i = 0; i < m_ExpiryDate.GetLength(); i++)
			m_ExpiryDate[i] = 0;

		m_MachineHash.Reset();
		m_ExpiryDate.Reset();
		m_EntitlementResponse.Reset();
	}
	static rlV2EntitlementDataBlockError Decode(u8* data, unsigned dataLen, rlV2EntitlementDataBlock& outBlock);
	static void SanitizeRecursive(parTreeNode* node);

	s32 m_Version;
	s64 m_RockstarId;
	atString m_MachineHash;
	atString m_ExpiryDate;
	rlV2EntitlementResponse m_EntitlementResponse;
};
#endif //RSG_ENTITLEMENT_ENABLED

//End of new Rage Entitlement system classes
#if __STEAM_BUILD
class cSteamDlcCallbackObject
{
public:
	cSteamDlcCallbackObject();
protected:
	STEAM_CALLBACK(cSteamDlcCallbackObject, OnSteamDlcInstalled, DlcInstalled_t, m_SteamDlcInstalled);
};

//Whoever is responsible for calling back into the SCS system to notify of completed transactions should instantiate one of these, or use it as an example to 
//For added fun, this function supports passing a function pointer to allow game level reaction to transaction authorisation.
//Probably don't want to instantiate multiple versions.


typedef void (*steamTransactionAuthorisedCallback)(bool authorised);

class cSteamEntitlementCallbackObject
{
public:
	cSteamEntitlementCallbackObject( int m_LocalGamerIndex , steamTransactionAuthorisedCallback callback = NULL);
protected:
	STEAM_CALLBACK(cSteamEntitlementCallbackObject, OnSteamTransactionAuthorizationResponse, MicroTxnAuthorizationResponse_t, m_SteamCallbackHandle);

	int m_LocalGamerIndex;
    steamTransactionAuthorisedCallback m_TransactionAuthorisedCallback;

	netStatus m_FinaliseSteamOrderStatus;
};
#endif

class rlRosEntitlement
{
public:

	// PURPOSE:
	//	Initializes the rlRosEntitlement interface
	static bool Init();

	// PURPOSE:
	//	Returns true if the  rlRosEntitlement interface has been initialized
	static bool IsInitialized();

	// PURPOSE:
	//	Shuts down the rlRosEntitlement interface and clears entitlement data
	static void Shutdown();

	// PURPOSE:
	//	Updates the rlRosEntitlement interface. Should be called once per frame.
	static void Update();
	
	// PURPOSE
	//	Tasks responsible for retrieving download URLs and validating the content age with the given country code and age.
	static bool GetSkuDownloadToken( const int localGamerIndex, const char* skuName, const char* serialNum, atMap<u32, atString> *downloadTokens, netStatus* status);
	static bool GetValidateContentPlayerAge(const int localGamerIndex, const char* contentName, netStatus* status);
	static bool GetValidateContentAge(int localGamerIndex, const char* contentName, const char* countryCode, int age, netStatus* status);
	static bool GetEntitlementDownloadURL(const int localGamerIndex, const char* entitlementCode, char* downloadUrl, unsigned int downloadUrlBufLen, netStatus* status);
	
	// PURPOSE
	// New voucher consumption functions
	static bool GetVoucherContent(int localGamerIndex, const char* voucherKey, const char* languageCode, rlV2VoucherPreviewResponse* responseData, netStatus* status);
	static bool ConsumeVoucher(int localGamerIndex, const char* voucherKey, int grantTokenVersion, const char* grantTokenString, netStatus* status);

	// PURPOSE
	// New commerce manifest functions
	static bool GetCommerceManifest(int localGamerIndex, const char* languageCode, const char* countryCode, const char* versionString, atString* manifestString, netStatus* status);

	// PURPOSE
	// New entitlement fetch functions
#if !PRELOADED_SOCIAL_CLUB
	static bool GetEntitlements(int localGamerIndex, const char* locale, const char* machineHash, rlV2EntitlementResponse* responseData, netStatus* status);
#if RSG_ENTITLEMENT_ENABLED
	static bool GetEntitlementBlock(int localGamerIndex, const char* machineHash, const char* locale, char* outputBuffer, int outputBufferSize, int* outputVersionNum, unsigned int* outputDataSize, netStatus* status);
	static bool DoesDataBlockGrantEntitlement( s64 rockstarId, const char* machineHash, void* data, int dataSize, const char* entitlementName);
#endif // RSG_ENTITLEMENT_ENABLED
	static bool GetEntitlementUrl( int localGamerIndex, const char* entitlementCode, char* urlBuffer, int urlBufferLength, netStatus* status ); 
#endif

	static bool GetContentUrl( int localGamerIndex, const char* contentPath, char* urlBuffer, int urlBufferLength, netStatus* status );
	// PURPOSE
	// Get the checkout URL for a product
	static bool GetCheckoutUrl(int localGamerIndex, const char* offerId, const char* priceId, const char* language, const char* country, char* checkoutBuffer, int checkoutBufferLength, netStatus* status);

	// PURPOSE
	// Functions for commencing and completing a steam consumables order
	static bool BeginSteamPurchase(int localGamerIndex, u32 appId, const char* offerId, s64 steamId, const char* language, netStatus* status);
	static bool FinaliseSteamPurchase(int localGamerIndex, u32 appId, s64 orderId, netStatus* status);
#if !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
	// PURPOSE
	// Allocates an offline entitlement buffer of length 'size', returns pointer to buffer (NULL if failed).
	static u8* AllocateOfflineEntitlementBlob(unsigned size);

	// PURPOSE
	//	Manages offline (cached) entitlement data
	static bool SaveOfflineEntitlement(u8* entitlementBuf, unsigned entitlementBufLen, u8* machineHash);
	static bool LoadOfflineEntitlement(u8* machineHash);

	// PURPOSE
	//	Returns TRUE if offline entitlement state is present
	static bool HasOfflineEntitlement() { return sm_OfflineEntitlementData != NULL; }
	static u8* GetOfflineEntitlement() { return sm_OfflineEntitlementData; }
	static unsigned GetOfflineEntitlementLength() { return sm_OfflineEntitlementDataLen; }
	
	// PURPOSE
	//	Deallocates the entitlement buffer.
	static bool ClearOfflineEntitlement();

	// PURPOSE
	// Returns TRUE if an entitlement IO operation is in progress.
	static bool IsEntitlementIOInProgress() { return sm_EntitlementIOStatus.Pending(); }
#endif 
	// PURPOSE
	// Isolate the ifdef horror to get the current sku name
	static const char* GetSkuName();

#if __STEAM_BUILD
	// PURPOSE
	// Returns TRUE if Steam DLC entitled and installed
	static bool IsSteamDlcInstalled(int appId);
	static cSteamDlcCallbackObject sm_SteamDlcCallbackObject;
#endif

private:
	
	static netStatus sm_EntitlementIOStatus;
	static bool sm_RlRosEntitlementInitialized;
	static u8* sm_OfflineEntitlementData;
	static unsigned sm_OfflineEntitlementDataLen;
};

}

#endif //RSG_PC
#endif

