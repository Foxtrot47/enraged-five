// 
// rline/entitlement/rlrosentitlementtask.cpp
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#include "data/datprotect.h"
#include "diag/seh.h"
#include "parser/treenode.h"
#include "rline/ros/rlros.h"
#include "rline/entitlement/rlrosentitlement.h"
#include "string/stringhash.h"
#include "atl/string.h"

#include "rlrosentitlementtask.h"

#if RSG_PC  // Entitlements are only for PC

#if RSG_PC
#include "rline/rlpc.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/rgsc_interface.h"
#endif

#if __RGSC_DLL
#include "../../../suite/src/rgsc/rgsc/rgsc/rgsc.h"
#endif
#define MAX_DAT_PROTECT_KEY_SIZE 50

namespace rage
{

#define XML_ENTRY_LENGTH 256
#define TOKEN_ENTRY_LENGTH 4096

#define ENTITLED_SKU_XML_TAG "EntitledSKUs"
#define CONTENTS_XML_TAG "Contents"

#define SKU_NAME_XML_TAG "SkuName"
#define INTERNAL_NAME_XML_TAG "InternalName"
#define FULL_NAME_XML_TAG "FullName"
#define SERIAL_NUM_XML_TAG "SerialNumber"
#define ACTIVATED_XML_TAG "IsActivated"

#define CONTENT_ID_XML_TAG "Id"
#define CONTENT_NAME_XML_TAG "Name"
#define SKU_NAMES_XML_TAG "SkuNames"
#define CONTENT_TITLE_ID_XML_TAG "TitleId"
#define CONTENT_PLATFORM_ID_XML_TAG "PlatformId"
#define REGISTERED_SKU_XML_TAG "RegisteredSku"
#define IS_ACTIVATED_XML_TAG "IsActivated"

#define TOKENS_XML_TAG "Tokens"
#define URI_XML_TAG "Uri"

#define VOUCHER_OFFERS_TAG "Offers"
#define VOUCHER_OFFER_DATA_TAG "OfferData"
#define VOUCHER_OFFER_OFFERCODE_TAG "OfferCode"
#define VOUCHER_OFFER_FRIENDLY_NAME_TAG "FriendlyName"
#define VOUCHER_ENTITLEMENTS_TAG "Entitlements"
#define VOUCHER_ENTITLEMENTS_GRANTED_TAG "EntitlementsToGrant"


#define VOUCHER_GRANT_TOKEN_TAG "GrantToken"
#define VOUCHER_GRANT_TOKEN_STRING_ATTRIBUTE "TokenString"
#define VOUCHER_GRANT_TOKEN_VERSION_ATTRIBUTE "Version"
#if !PRELOADED_SOCIAL_CLUB 
#define GET_ENTITLEMENT_DATABLOCK_TAG "Result"
#define GET_ENTITLEMENT_DATABLOCK_VERSION_ATTRIBUTE "Version"
#define GET_ENTITLEMENT_DATABLOCK_DATA_TAG "Data"
#define GET_ENTITLEMENTS_ENTITLEMENT_ARRAY_TAG "Entitlements"
#define GET_ENTITLEMENTS_MACHINE_HASH_TAG "MachineHash"
#define GET_ENTITLEMENTS_SIGNATURE_TAG "Signature"


#define ENTITLEMENT_CONSUMABLE_ATTRIBUTE_VAL "Durable"
#define ENTITLEMENT_DATE_CREATED_ATTRIBUTE "CreatedDateUtc"
#define	ENTITLEMENT_DATE_EXPIRES_ATTRIBUTE "ExpireDateUtc"
#define ENTITLEMENT_DURABLE_ATTRIBUTE_VAL "Durable"

#define ENTITLEMENT_INSTANCE_ID_ATTRIBUTE "InstanceId"

#define ENTITLEMENT_TYPE_ATTRIBUTE "Type"


#define ENTITLEMENT_DAT_NAME "cfg.dat"
#endif
#define ENTITLEMENT_CODE_TAG "EntitlementCode"
#define ENTITLEMENT_FRIENDLY_NAME_TAG "FriendlyName"
#define ENTITLEMENT_TYPE_TAG "Type"
#define COMMERCE_ENTITLEMENT_MANIFEST_TAG "Manifest"


#define ERROR_CASE(code, codeEx, resCode) \
	if(result.IsError(code, codeEx)) \
	{ \
	resultCode = resCode; \
	return; \
}

//////////////////////////////////////////////////////////////////////////
// rlRosEntitlementDownloadTokenTask
//////////////////////////////////////////////////////////////////////////
rlRosEntitlementDownloadTokenTask::rlRosEntitlementDownloadTokenTask()
{
	m_DownloadTokens = NULL;
}

rlRosEntitlementDownloadTokenTask::~rlRosEntitlementDownloadTokenTask()
{

}

bool rlRosEntitlementDownloadTokenTask::Configure( const int localGamerIndex, const char* skuName, const char* serialNum, atMap<u32, atString> *downloadTokens)
{
	rtry
	{
		rverify(downloadTokens, catchall, );
		rverify(skuName, catchall, );
		rverify(serialNum, catchall, );

		m_DownloadTokens = downloadTokens;

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

		rverify(BeginListParameter("skuNames"), catchall, );
		rverify(AddListParamStringValue(skuName), catchall, );

		rverify(BeginListParameter("serialNumbers"), catchall, );
		rverify(AddListParamStringValue(serialNum), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosEntitlementDownloadTokenTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		rverify(m_DownloadTokens, catchall, rlTaskError("Invalid download token array to populate"));

		parTreeNode* pTokensNode = node->FindChildWithName(TOKENS_XML_TAG);
		rverify(pTokensNode, catchall,);

		for(parTreeNode::ChildNodeIterator tokenNodeIt = pTokensNode->BeginChildren();
			*tokenNodeIt != NULL;
			++tokenNodeIt)
		{
			const parTreeNode &tokenInfo = *(*tokenNodeIt);

			char skuName[XML_ENTRY_LENGTH] = { 0 };
			char downloadToken[TOKEN_ENTRY_LENGTH] = { 0 };	

			const char *pSkuName = tokenInfo.GetElement().FindAttributeStringValue(SKU_NAME_XML_TAG, "", skuName, XML_ENTRY_LENGTH, true);
			const char *pDownloadToken = safecpy(downloadToken, tokenInfo.GetData(), sizeof(downloadToken));

			u32 skuNameHash = atStringHash(pSkuName);
			m_DownloadTokens->Insert(skuNameHash, atString(pDownloadToken));
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlRosEntitlementDownloadTokenTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
	ERROR_CASE("AuthenticationFailed", "Ticket", RLEN_ERROR_AUTHENTICATIONFAILED_TICKET);

	rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

const char* rlRosEntitlementDownloadTokenTask::GetServiceMethod() const
{
	return "entitlements.asmx/GetSkuDownloadToken";
}

//////////////////////////////////////////////////////////////////////////
// rlRosEntitlementValidateContentPlayerAgeTask
//////////////////////////////////////////////////////////////////////////
rlRosEntitlementValidateContentPlayerAgeTask::rlRosEntitlementValidateContentPlayerAgeTask()
{

}

rlRosEntitlementValidateContentPlayerAgeTask::~rlRosEntitlementValidateContentPlayerAgeTask()
{

}

bool rlRosEntitlementValidateContentPlayerAgeTask::Configure (int localGamerIndex, const char* contentName)
{
	rtry
	{
		rverify(contentName, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );
		
		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("contentName", contentName, true), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosEntitlementValidateContentPlayerAgeTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& resultCode)
{
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		resultCode = 1;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlRosEntitlementValidateContentPlayerAgeTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
	ERROR_CASE("AuthenticationFailed", "Ticket", RLEN_ERROR_AUTHENTICATIONFAILED_TICKET);
	rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

const char* rlRosEntitlementValidateContentPlayerAgeTask::GetServiceMethod() const
{
	return "entitlements.asmx/ValidateContentPlayerAge";
}

//////////////////////////////////////////////////////////////////////////
// rlRosEntitlementValidateContentAgeTask
//////////////////////////////////////////////////////////////////////////
rlRosEntitlementValidateContentAgeTask::rlRosEntitlementValidateContentAgeTask()
{

}

rlRosEntitlementValidateContentAgeTask::~rlRosEntitlementValidateContentAgeTask()
{

}

bool rlRosEntitlementValidateContentAgeTask::Configure (int localGamerIndex, const char* contentName, const char* countryCode, int age)
{
	rtry
	{
		rverify(contentName, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("contentName", contentName, true), catchall, );
		rverify(AddStringParameter("countryCode", countryCode, true), catchall, );
		rverify(AddIntParameter("age", age, true), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosEntitlementValidateContentAgeTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& resultCode)
{
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		resultCode = 1;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlRosEntitlementValidateContentAgeTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
	ERROR_CASE("AuthenticationFailed", "Ticket", RLEN_ERROR_AUTHENTICATIONFAILED_TICKET);
	rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

const char* rlRosEntitlementValidateContentAgeTask::GetServiceMethod() const
{
	return "entitlements.asmx/ValidateContentAge";
}

// rlRosEntitlementDownloadURLTask
//////////////////////////////////////////////////////////////////////////
rlRosEntitlementDownloadURLTask::rlRosEntitlementDownloadURLTask()
{
	m_downloadURL = NULL;
	m_downloadURLBufLen = 0;
}

rlRosEntitlementDownloadURLTask::~rlRosEntitlementDownloadURLTask()
{

}

bool rlRosEntitlementDownloadURLTask::Configure(int localGamerIndex, const char* entitlementCode, char* downloadURL, unsigned int downloadURLBufLen)
{
	rtry
	{
		rverify(entitlementCode, catchall, );
		rverify(downloadURL, catchall, );
		rverify(downloadURLBufLen > 0, catchall, );

		m_downloadURL = downloadURL;
		m_downloadURLBufLen = downloadURLBufLen;

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("entitlementCode", entitlementCode, true), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosEntitlementDownloadURLTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		//@@: range RLROSENTITLEMENTDOWNLOADURLTASK_PROCESSSUCCESS {
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		rverify(m_downloadURL, catchall, rlTaskError("Invalid download URL string"));

		parTreeNode* pSku = node->FindChildWithName(URI_XML_TAG);
		rverify(pSku, catchall, rlTaskError("Invalid download URL response"));
		//@@: } RLROSENTITLEMENTDOWNLOADURLTASK_PROCESSSUCCESS


		safecpy(m_downloadURL, pSku->GetData(), m_downloadURLBufLen);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlRosEntitlementDownloadURLTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
	//@@: range RLROSENTITLEMENTDOWNLOADURLTASK_PROCESSERROR {
	ERROR_CASE("DoesNotExist", "RockstarAccount", RLEN_ERROR_AUTHENTICATIONFAILED_TICKET);
	ERROR_CASE("NotAllowed", "EntitlementCode", RLEN_ERROR_NOT_ENTITLED);

	rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	//@@: } RLROSENTITLEMENTDOWNLOADURLTASK_PROCESSERROR

}

const char* rlRosEntitlementDownloadURLTask::GetServiceMethod() const
{
	return "entitlements.asmx/GetEntitlementDownloadUri";
}

#if RSG_CPU_X64
rlRosEntitlementPreviewVoucher::rlRosEntitlementPreviewVoucher() :
	m_ResponseData(NULL)
{
	
}

rlRosEntitlementPreviewVoucher::~rlRosEntitlementPreviewVoucher()
{

}

bool rlRosEntitlementPreviewVoucher::Configure(int localGamerIndex, const char* redemptionCode, const char* languageCode,  rlV2VoucherPreviewResponse* responseData)
{	
	
	
	rtry
	{
		rverify(redemptionCode, catchall, );
		rverify(responseData, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		m_ResponseData = responseData;

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("voucherCode", redemptionCode, true), catchall, );
		rverify(AddStringParameter("locale", languageCode, true), catchall, );
	}
	rcatchall
	{
		return false;
	}


	return true;
}

const char* rlRosEntitlementPreviewVoucher::GetServiceMethod() const
{
	return "entitlements.asmx/PreviewVoucherConsume";
}

bool rlRosEntitlementPreviewVoucher::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		rverify(m_ResponseData, catchall, rlTaskError("Invalid response data"));


		parTreeNode* pGrantTokenNode = node->FindChildWithName(VOUCHER_GRANT_TOKEN_TAG);
		rverify(pGrantTokenNode, catchall, );

		char grantTokenString[XML_ENTRY_LENGTH];
		const char* pGrantTokenString = pGrantTokenNode->GetElement().FindAttributeStringValue(VOUCHER_GRANT_TOKEN_STRING_ATTRIBUTE,"",grantTokenString,XML_ENTRY_LENGTH,true);

		m_ResponseData->m_GrantTokenVersion = pGrantTokenNode->GetElement().FindAttributeIntValue(VOUCHER_GRANT_TOKEN_VERSION_ATTRIBUTE,0,false);

		m_ResponseData->m_GrantToken = pGrantTokenString;

		parTreeNode* pOffersNode = node->FindChildWithName(VOUCHER_OFFERS_TAG);
		rverify(pOffersNode, catchall, );

		for (parTreeNode::ChildNodeIterator offerNodeIt = pOffersNode->BeginChildren();
			*offerNodeIt != NULL;
			++offerNodeIt)
		{
			const parTreeNode &offerInfo = *(*offerNodeIt);

			//Check this offer grants entitlements, otherwise it isnt worth adding
			parTreeNode* pEntitlementsNode = offerInfo.FindChildWithName(VOUCHER_ENTITLEMENTS_TAG);

			if (pEntitlementsNode == NULL)
			{
				continue;
			}

			rlV2Offer newOffer;

			char code[XML_ENTRY_LENGTH];
			newOffer.m_Code = offerInfo.GetElement().FindAttributeStringValue(VOUCHER_OFFER_OFFERCODE_TAG, "", code, XML_ENTRY_LENGTH, true);

            char friendlyOfferName[XML_ENTRY_LENGTH];
            newOffer.m_FriendlyName = offerInfo.GetElement().FindAttributeStringValue(VOUCHER_OFFER_FRIENDLY_NAME_TAG, "", friendlyOfferName, XML_ENTRY_LENGTH, true);

			for (parTreeNode::ChildNodeIterator entitlementIt = pEntitlementsNode->BeginChildren();
				*entitlementIt != NULL;
				++entitlementIt)
			{
				const parTreeNode &entitlementInfo = *(*entitlementIt);
				
				char entitlementCode[XML_ENTRY_LENGTH];
				char friendlyName[XML_ENTRY_LENGTH];
				char entitlementType[XML_ENTRY_LENGTH];
				
				int count = entitlementInfo.GetElement().FindAttributeIntValue("Count",0,false);
                //int visibilityType = entitlementInfo.GetElement().FindAttributeIntValue("Count",0,false);

				const char *pEntitlementCode = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_CODE_TAG,"",entitlementCode,XML_ENTRY_LENGTH,true);
				const char *pFriendlyName = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_FRIENDLY_NAME_TAG,"",friendlyName,XML_ENTRY_LENGTH,true);
				const char *pEntitlementType = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_TYPE_TAG,"", entitlementType,XML_ENTRY_LENGTH,true);
                bool visible = entitlementInfo.GetElement().FindAttributeBoolValue("Visible",true);

				rlV2OfferEntitlementData newEntitlementData;

				if (!strcmp("Consumable", pEntitlementType))
				{
					newEntitlementData.m_Type = ENTITLEMENT_TYPE_CONSUMABLE;
				}
				else if (!strcmp("Durable", pEntitlementType))
				{
					newEntitlementData.m_Type = ENTITLEMENT_TYPE_DURABLE;
				} 
				else
				{
					newEntitlementData.m_Type = ENTITLEMENT_TYPE_UNKNOWN;
				}

				newEntitlementData.m_FriendlyName = pFriendlyName;
				newEntitlementData.m_Code = pEntitlementCode;
				newEntitlementData.m_Count = count;
                newEntitlementData.m_Visible = visible;

				newOffer.m_EntitlementDataArray.PushAndGrow(newEntitlementData);
			}
			
			m_ResponseData->m_Offers.PushAndGrow(newOffer);
		}


		parTreeNode* pEntitlementsGrantedNode = node->FindChildWithName(VOUCHER_ENTITLEMENTS_GRANTED_TAG);
		rverify(pEntitlementsGrantedNode, catchall,);
		
		for(parTreeNode::ChildNodeIterator entitlementNodeIt = pEntitlementsGrantedNode->BeginChildren();
			*entitlementNodeIt != NULL;
			++entitlementNodeIt)
		{
			const parTreeNode &entitlementInfo = *(*entitlementNodeIt);

			char entitlementCode[XML_ENTRY_LENGTH];
			char friendlyName[XML_ENTRY_LENGTH];
			char entitlementType[XML_ENTRY_LENGTH];

			int count = entitlementInfo.GetElement().FindAttributeIntValue("Count",0,false);

			const char *pEntitlementCode = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_CODE_TAG,"",entitlementCode,XML_ENTRY_LENGTH,true);
			const char *pFriendlyName = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_FRIENDLY_NAME_TAG,"",friendlyName,XML_ENTRY_LENGTH,true);
			const char *pEntitlementType = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_TYPE_TAG,"", entitlementType,XML_ENTRY_LENGTH,true);
            bool visible = entitlementInfo.GetElement().FindAttributeBoolValue("Visible",true);

			rlV2OfferEntitlementData newEntitlementData;

			if (!strcmp("Consumable", pEntitlementType))
			{
				newEntitlementData.m_Type = ENTITLEMENT_TYPE_CONSUMABLE;
			}
			else if (!strcmp("Durable", pEntitlementType))
			{
				newEntitlementData.m_Type = ENTITLEMENT_TYPE_DURABLE;
			} 
			else
			{
				newEntitlementData.m_Type = ENTITLEMENT_TYPE_UNKNOWN;
			}

			newEntitlementData.m_FriendlyName = pFriendlyName;
			newEntitlementData.m_Code = pEntitlementCode;
			newEntitlementData.m_Count = count;
            newEntitlementData.m_Visible = visible;

			m_ResponseData->m_EntitlementsToGrant.PushAndGrow(newEntitlementData);
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlRosEntitlementPreviewVoucher::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
{
	ERROR_CASE("DoesNotExist", "RockstarAccount", RLEN_ERROR_AUTHENTICATIONFAILED_TICKET);
	ERROR_CASE("DoesNotExist", "Voucher", RLEN_ERROR_SERIAL_DOES_NOT_EXIST);
	ERROR_CASE("NotAllowed", "EntitlementCode", RLEN_ERROR_NOT_ENTITLED);
	ERROR_CASE("NotAllowed", "Voucher", RLEN_ERROR_SERIAL_ALREADY_ACTIVATED);
    ERROR_CASE("NotAllowed", "Rule", RLEN_ERROR_NOT_ALLOWED_RULE);
	
	rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}


rlRosEntitlementVoucherConsume::rlRosEntitlementVoucherConsume()
{

}

rlRosEntitlementVoucherConsume::~rlRosEntitlementVoucherConsume()
{

}

bool rlRosEntitlementVoucherConsume::Configure(int localGamerIndex, const char* redemptionCode, int tokenVersion, const char* tokenString)
{	
	bool success = false;
	rtry
	{
		rverify(redemptionCode, catchall, );
		rverify(tokenVersion!=-1, catchall, );
		rverify(tokenString, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("voucherCode", redemptionCode, true), catchall, );
		rverify(AddIntParameter("grantTokenVersion", tokenVersion), catchall, );
		rverify(AddStringParameter("grantTokenStr", tokenString, true), catchall, );
		rverify(AddStringParameter("locale", "en", true), catchall, );

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

const char* rlRosEntitlementVoucherConsume::GetServiceMethod() const
{
	return "entitlements.asmx/ConsumeVoucher";
}

void rlRosEntitlementVoucherConsume::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
	ERROR_CASE("DoesNotExist", "RockstarAccount", RLEN_ERROR_AUTHENTICATIONFAILED_TICKET);
	ERROR_CASE("DoesNotExist", "Voucher", RLEN_ERROR_SERIAL_DOES_NOT_EXIST);
	ERROR_CASE("NotAllowed", "EntitlementCode", RLEN_ERROR_NOT_ENTITLED);
	ERROR_CASE("NotAllowed", "Voucher", RLEN_ERROR_SERIAL_ALREADY_ACTIVATED);

	rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}


/////////////////////////////////
// rlRosFetchCommerceManifest
/////////////////////////////////
rlRosFetchCommerceManifest::rlRosFetchCommerceManifest()
: m_ManifestString(NULL)
{

}

rlRosFetchCommerceManifest::~rlRosFetchCommerceManifest()
{
	
}

bool rlRosFetchCommerceManifest::Configure(int localGamerIndex, const char* languageCode, const char* countryCode, const char* versionString, atString* manifestString)
{
	bool success = false;
	rtry
	{
		rverify(languageCode, catchall, );
		rverify(manifestString, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("language", languageCode, true), catchall, );
		rverify(AddStringParameter("country", countryCode, true), catchall, );


		//Going to do this like this for now, but these must be a better way.
		//Ideally SKU should be passed up with the ticket, that would cover us for SCEE and SCEA etc in future.
		
		rverify(AddStringParameter("Sku", versionString, true), catchall, );

		m_ManifestString = manifestString;
		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

const char* rlRosFetchCommerceManifest::GetServiceMethod() const
{
	return "entitlements.asmx/GetCommerceManifest";
}

bool rlRosFetchCommerceManifest::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		rverify(m_ManifestString, catchall, rlTaskError("Invalid manifest destination string"));

		parTreeNode* pManifest = node->FindChildWithName(COMMERCE_ENTITLEMENT_MANIFEST_TAG);
		rverify(pManifest, catchall, rlTaskError("Invalid commerce manifest response"));

		*m_ManifestString = pManifest->GetData();

		return true;
	}
	rcatchall
	{
		return false;
	}
}


#if !PRELOADED_SOCIAL_CLUB 
rlRosGetEntitlements::rlRosGetEntitlements() :
	m_ResponseData(NULL)
{

}

rlRosGetEntitlements::~rlRosGetEntitlements()
{

}

bool rlRosGetEntitlements::Configure(int localGamerIndex, const char* locale, const char* UNUSED_PARAM(machineHash), rlV2EntitlementResponse* entitlementResponseData)
{
	rtry
	{
		rverify(locale, catchall, );
		rverify(entitlementResponseData, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );
		//Not passing machineHash until we're sure the endpoint will use this
		//rverify(machineHash, catchall, );

		m_ResponseData = entitlementResponseData;

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("locale", locale, true), catchall, );
		//rverify(AddStringParameter("machineHash", machineHash, true), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlRosGetEntitlements::GetServiceMethod() const
{
	return "entitlements.asmx/GetEntitlements";
}

/*static*/
bool rlRosGetEntitlements::ParseEntitlementResponse(const parTreeNode* node, rlV2EntitlementResponse& outResponse)
{
	rtry
	{
		rverify(node, catchall, rlError("No valid XML node to process"));

		const parTreeNode* pEntitlementsGrantedNode = node;

		for(parTreeNode::ChildNodeIterator entitlementNodeIt = pEntitlementsGrantedNode->BeginChildren();
			*entitlementNodeIt != NULL;
			++entitlementNodeIt)
		{
			const parTreeNode &entitlementInfo = *(*entitlementNodeIt);

			char entitlementCode[XML_ENTRY_LENGTH];
			char friendlyName[XML_ENTRY_LENGTH];
			//char entitlementType[XML_ENTRY_LENGTH];
			char createdDate[XML_ENTRY_LENGTH];
			char expiryDate[XML_ENTRY_LENGTH];
            char typeString[XML_ENTRY_LENGTH];

			int count = entitlementInfo.GetElement().FindAttributeIntValue("Count",0,false);

			const char *pEntitlementCode = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_CODE_TAG,"",entitlementCode,XML_ENTRY_LENGTH,true);
			const char *pFriendlyName = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_FRIENDLY_NAME_TAG,"",friendlyName,XML_ENTRY_LENGTH,true);
			//const char *pEntitlementType = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_TYPE_TAG,"", entitlementType,XML_ENTRY_LENGTH,true);
			const char *pCreatedDate = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_DATE_CREATED_ATTRIBUTE,"",createdDate, XML_ENTRY_LENGTH,true);
			const char *pExpiryDate = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_DATE_EXPIRES_ATTRIBUTE, "",expiryDate, XML_ENTRY_LENGTH, true);
            const char *pTypeString = entitlementInfo.GetElement().FindAttributeStringValue(ENTITLEMENT_TYPE_ATTRIBUTE, "",typeString, XML_ENTRY_LENGTH, true);
            
            bool visible = entitlementInfo.GetElement().FindAttributeBoolValue("Visible",true);

            s64 instanceId = entitlementInfo.GetElement().FindAttributeInt64Value(ENTITLEMENT_INSTANCE_ID_ATTRIBUTE, 0); 

			rlV2EntitlementInstance newEntitlementData;

			newEntitlementData.m_FriendlyName = pFriendlyName;
			newEntitlementData.m_Code = pEntitlementCode;
			newEntitlementData.m_Count = count;
			newEntitlementData.m_CreateDateUtc = pCreatedDate;
			newEntitlementData.m_ExpireDateUtc = pExpiryDate;
            newEntitlementData.m_InstanceId = instanceId;
            newEntitlementData.m_Visible = visible;


            if (pTypeString && strcmp(pTypeString, "Durable") == 0)
            {
                newEntitlementData.m_Type = ENTITLEMENT_TYPE_DURABLE;
            }
            else if (pTypeString && strcmp(pTypeString, "Consumable") == 0)
            {
                newEntitlementData.m_Type = ENTITLEMENT_TYPE_CONSUMABLE;
            }
            else
            {
                newEntitlementData.m_Type = ENTITLEMENT_TYPE_UNKNOWN;
            }

			outResponse.m_EntitlementInstanceArray.PushAndGrow(newEntitlementData);
			newEntitlementData.Reset();
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosGetEntitlements::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
	rtry
	{
		rverify(m_ResponseData, catchall, rlTaskError("Invalid response data"));
		rverify(node, catchall, rlError("No valid XML node to process"));

		//Not parsing signature/machineHash for now until we're sure the endpoint will return these
		/*
		parTreeNode* pSignatureNode = node->FindChildWithName(GET_ENTITLEMENTS_SIGNATURE_TAG);
		rverify(pSignatureNode, catchall,);
		m_ResponseData->m_Signature = pSignatureNode->GetData();

		parTreeNode* pMachineHashNode = node->FindChildWithName(GET_ENTITLEMENTS_MACHINE_HASH_TAG);
		rverify(pMachineHashNode, catchall,);
		m_ResponseData->m_MachineHash = pMachineHashNode->GetData();
		*/


		return ParseEntitlementResponse(node->FindChildWithName(GET_ENTITLEMENTS_ENTITLEMENT_ARRAY_TAG), *m_ResponseData);
	}
	rcatchall
	{
		return false;
	}
}

#if RSG_ENTITLEMENT_ENABLED
rlRosGetEntitlementBlock::rlRosGetEntitlementBlock() :
	m_OutputBuffer(NULL),
	m_OutputBufferSize(0),
	m_OutputVersionNumber(NULL),
	m_OutDataSize(NULL)
{
	
}

rlRosGetEntitlementBlock::~rlRosGetEntitlementBlock()
{

}

bool rlRosGetEntitlementBlock::Configure(int localGamerIndex, const char* machineHash, const char* locale, char* outputBuffer, int outputBufferSize, int* outputVersionNumber, unsigned int* outputDataSize)
{
	rtry
	{
		//@@: location RLROSGETENTITLEMENTBLOCK_CONFIGURE_ENTRY
		rverify(outputBuffer, catchall, );
		rverify(outputVersionNumber, catchall, );
		rverify(outputBufferSize > 0, catchall, );
		//@@: location RLROSGETENTITLEMENTBLOCK_CONFIGURE_CALL_CONFIGURE_HTTP
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );
		rverify(machineHash, catchall, );
		rverify(outputDataSize, catchall, );
		//@@: range RLROSGETENTITLEMENTBLOCK_CONFIGURE {
		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

		//@@: location RLROSGETENTITLEMENTBLOCK_VALID_CREDS
		rverify(cred.IsValid(), catchall,);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("machineHash", machineHash, true), catchall, );
		rverify(AddStringParameter("locale", locale, true), catchall, );

		m_OutputBuffer = outputBuffer;
		m_OutputBufferSize = outputBufferSize;
		m_OutputVersionNumber = outputVersionNumber;
		m_OutDataSize = outputDataSize;
		//@@: } RLROSGETENTITLEMENTBLOCK_CONFIGURE

		
	}
	rcatchall
	{
		//@@: location RLROSGETENTITLEMENTBLOCK_CONFIGURE_EXIT_A
		return false;
	}

	//@@: location RLROSGETENTITLEMENTBLOCK_CONFIGURE_EXIT_B
	return true;
}

const char* rlRosGetEntitlementBlock::GetServiceMethod() const
{
	return "entitlements.asmx/GetEntitlementBlock";
}

bool rlRosGetEntitlementBlock::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
	//@@: range RLROSGETENTITLEMENTBLOCK_PROCESSSUCCESS {
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		rverify(m_OutputBuffer, catchall, rlTaskError("Invalid data buffer for response"));

		parTreeNode* pDataBlockNode = node->FindChildWithName(GET_ENTITLEMENT_DATABLOCK_TAG);
		rverify(pDataBlockNode, catchall,);
		//@@: location RLROSGETENTITLEMENTBLOCK_PROCESSSUCCESS_GET_VERSION
		int versionNumber = pDataBlockNode->GetElement().FindAttributeIntValue(GET_ENTITLEMENT_DATABLOCK_VERSION_ATTRIBUTE,-1,false);
		rverify(versionNumber != -1, catchall,);

		if (m_OutputVersionNumber)
		{
			*m_OutputVersionNumber = versionNumber;
		}

		parTreeNode* pDataNode = pDataBlockNode->FindChildWithName(GET_ENTITLEMENT_DATABLOCK_DATA_TAG);
		rverify(pDataNode, catchall,);
		//@@: location RLROSGETENTITLEMENTBLOCK_PROCESSSUCCESS_DECODE_DATA

		rcheck(datBase64::Decode((const char*)pDataNode->GetData(),m_OutputBufferSize,(u8*)m_OutputBuffer,m_OutDataSize), catchall,);
	}
	rcatchall
	{
		return false;
	}

	return true;
	//@@: } RLROSGETENTITLEMENTBLOCK_PROCESSSUCCESS 

}
#endif //RSG_ENTITLEMENT_ENABLED
#endif //!PRELOADED_SOCIAL_CLUB

rlRosGetCheckoutUrl::rlRosGetCheckoutUrl() :
	m_CheckoutUrlBuffer(NULL),
	m_CheckoutUrlBufferLength(0)
{
	
}

rlRosGetCheckoutUrl::~rlRosGetCheckoutUrl()
{

}

bool rlRosGetCheckoutUrl::Configure(int localGamerIndex, const char* offerCode, const char* priceId, const char* language, const char* country, char* checkoutBuffer, int checkoutBufferLength)
{
	rtry
	{
		rverify(language, catchall, );
		rverify(priceId, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );
		rverify(checkoutBuffer,catchall,);
		rverify(checkoutBufferLength > 0,catchall,);


		m_CheckoutUrlBuffer = checkoutBuffer;
		m_CheckoutUrlBufferLength = checkoutBufferLength;

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("offerCode", offerCode, true), catchall, );
		rverify(AddStringParameter("priceId", priceId, true), catchall, );
		rverify(AddStringParameter("language", language, true), catchall, );
		rverify(AddStringParameter("country", country, true), catchall, );
		rverify(AddStringParameter("Sku", "", true), catchall, );

	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosGetCheckoutUrl::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		rverify(m_CheckoutUrlBuffer, catchall, );

		parTreeNode* pUrlNode = node->FindChildWithName("Url");
		rverify(pUrlNode, catchall,);
		safecpy(m_CheckoutUrlBuffer, pUrlNode->GetData(), m_CheckoutUrlBufferLength);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlRosGetCheckoutUrl::GetServiceMethod() const
{
	return "entitlements.asmx/GetCheckoutUrl";
}


rlRosBeginSteamPurchase::rlRosBeginSteamPurchase()
{

}

rlRosBeginSteamPurchase::~rlRosBeginSteamPurchase()
{

}

bool rlRosBeginSteamPurchase::Configure(int localGamerIndex, u32 appId, const char* offerCode, s64 steamId, const char* language)
{
	rtry
	{
		rverify(language, catchall, );
		rverify(offerCode, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );
		rverify(steamId,catchall,);
		

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);

		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddIntParameter("appId", appId), catchall, );
		rverify(AddStringParameter("offerCode", offerCode, true), catchall, );
		rverify(AddIntParameter("steamId", steamId, true), catchall, );
		rverify(AddStringParameter("language", language, true), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlRosBeginSteamPurchase::GetServiceMethod() const
{
	return "entitlements.asmx/BeginSteamPurchase";
}


rlRosFinaliseSteamPurchase::rlRosFinaliseSteamPurchase()
{

}

rlRosFinaliseSteamPurchase::~rlRosFinaliseSteamPurchase()
{

}

bool rlRosFinaliseSteamPurchase::Configure(int localGamerIndex, u32 appId, s64 orderId)
{
	rtry
	{
		
		rverify(orderId, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);

		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddIntParameter("appId", appId), catchall, );
		rverify(AddIntParameter("orderId", orderId), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlRosFinaliseSteamPurchase::GetServiceMethod() const
{
	return "entitlements.asmx/FinaliseSteamPurchase";
}

#if !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
rlRosGetEntitlementDownloadUrl::rlRosGetEntitlementDownloadUrl():
	m_UrlBuffer(NULL),
	m_UrlBufferLength(0)
{

}

rlRosGetEntitlementDownloadUrl::~rlRosGetEntitlementDownloadUrl()
{

}

bool rlRosGetEntitlementDownloadUrl::Configure(int localGamerIndex, const char* entitlementCode, char* urlBuffer, int urlBufferLength)
{
	rtry
	{
		
		
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );
		rverify(urlBuffer,catchall,);
		rverify(urlBufferLength > 0,catchall,);
		rverify(entitlementCode,catchall,);


		m_UrlBuffer = urlBuffer;
		m_UrlBufferLength = urlBufferLength;

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(cred.IsValid(), catchall,);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("entitlementCode", entitlementCode, true), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosGetEntitlementDownloadUrl::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
	rtry
	{
		rverify(node, catchall, rlTaskError("No valid XML node to process"));
		rverify(m_UrlBuffer, catchall, );

		parTreeNode* pUrlNode = node->FindChildWithName("Uri");
		rverify(pUrlNode, catchall,);
		safecpy(m_UrlBuffer, pUrlNode->GetData(), m_UrlBufferLength);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlRosGetEntitlementDownloadUrl::GetServiceMethod() const
{
	return "entitlements.asmx/GetEntitlementDownloadUri";
}
#endif //!PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED

rlRosGetContentUrl::rlRosGetContentUrl():
    m_UrlBuffer(NULL),
    m_UrlBufferLength(0)
{

}

rlRosGetContentUrl::~rlRosGetContentUrl()
{

}

bool rlRosGetContentUrl::Configure(int localGamerIndex, const char* contentPath, char* urlBuffer, int urlBufferLength)
{
    rtry
    {


        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );
        rverify(urlBuffer,catchall,);
        rverify(urlBufferLength > 0,catchall,);
        rverify(contentPath,catchall,);


        m_UrlBuffer = urlBuffer;
        m_UrlBufferLength = urlBufferLength;

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rverify(cred.IsValid(), catchall,);
        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("path", contentPath, true), catchall, );
    }
    rcatchall
    {
        return false;
    }

    return true;
}

bool rlRosGetContentUrl::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    rtry
    {
        rverify(node, catchall, rlTaskError("No valid XML node to process"));
        rverify(m_UrlBuffer, catchall, );

        parTreeNode* pUrlNode = node->FindChildWithName("Url");
        rverify(pUrlNode, catchall,);
        safecpy(m_UrlBuffer, pUrlNode->GetData(), m_UrlBufferLength);
    }
    rcatchall
    {
        return false;
    }

    return true;
}

void rlRosGetContentUrl::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", NULL, RLEN_ERROR_NOT_ENTITLED);
    ERROR_CASE("NotAllowed", "EntitlementCode", RLEN_ERROR_NOT_ENTITLED);
    
    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

const char* rlRosGetContentUrl::GetServiceMethod() const
{
    return "entitlements.asmx/GetDownloadUrlFromPath";
}

#endif

#if !PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
/////////////////////////////////
// rlRosEntitlementSaveOfflineTask
/////////////////////////////////
rlRosEntitlementSaveOfflineTask::rlRosEntitlementSaveOfflineTask()
	: m_State(STATE_SAVE_FILE)
	, m_EntitlementSaveBuf(NULL)
	, m_EntitlementBufLen(0)
	, m_ExportSize(0)
{

}

rlRosEntitlementSaveOfflineTask::~rlRosEntitlementSaveOfflineTask()
{
	if (m_EntitlementSaveBuf)
	{
		delete[]m_EntitlementSaveBuf;
		m_EntitlementBufLen = 0;
	}
}


bool rlRosEntitlementSaveOfflineTask::Configure(u8* entitlementBuf, unsigned entitlementBufLen, u8* machineHash)
{
	bool success = false;

	rtry
	{
		rverify(entitlementBuf, catchall, );
		rverify(entitlementBufLen > 0, catchall, );

		// Save entitlement buf len
		m_EntitlementBufLen = entitlementBufLen;

		// Allocate enough space for the buffer + datProtect signature size
		m_EntitlementSaveBuf = rage_new u8[entitlementBufLen + datProtectSignatureSize];

		// rlVerify the allocation was successful
		rverify(m_EntitlementSaveBuf, catchall, );

		// Copy the memory over
		sysMemSet(m_EntitlementSaveBuf, 0, sizeof(m_EntitlementSaveBuf));
		sysMemCpy(m_EntitlementSaveBuf, entitlementBuf, m_EntitlementBufLen);

		// Configure the worker
		//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_CONFIGURE_CONFIGURE_WORKER
		m_Worker.Configure(m_EntitlementSaveBuf, entitlementBufLen, machineHash, &m_WorkerStatus);

		m_State = STATE_SAVE_FILE;

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}


void rlRosEntitlementSaveOfflineTask::OnCancel()
{
	
}

void rlRosEntitlementSaveOfflineTask::Complete(const netTaskStatus )
{

}

netTaskStatus rlRosEntitlementSaveOfflineTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;
	//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_ONUPDATE
	switch(m_State)
	{
	case STATE_SAVE_FILE:
		if(WasCanceled())
		{
			netTaskDebug("Canceled - bailing...");
			status = NET_TASKSTATUS_FAILED;
		}
		else if(SaveFile())
		{
			m_State = STATE_SAVING_FILE;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_SAVING_FILE:
		if(m_WorkerStatus.Succeeded())
		{
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(m_WorkerStatus.Failed())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool rlRosEntitlementSaveOfflineTask::SaveFile()
{
	bool bSuccess = m_Worker.Start(0);
	if (bSuccess)
	{
		m_WorkerStatus.SetPending();
		m_Worker.Wakeup();
	}

	return bSuccess;
}

rlRosEntitlementSaveOfflineTask::EntitlementSaveFileWorker::EntitlementSaveFileWorker()
	: m_BufSize(0)
	, m_BufPtr(NULL)
	, m_Status(NULL)
	, m_ExportSize(0)
{
}

bool rlRosEntitlementSaveOfflineTask::EntitlementSaveFileWorker::Configure(u8* buf, unsigned bufLen, u8* machineHash, netStatus* status)
{
	bool success = false;

	rtry
	{
		rverify(buf, catchall, );
		rverify(status, catchall, );
		rverify(bufLen > 0, catchall, );

		// create new buffer
		m_BufSize = bufLen;
		m_BufPtr = buf;
		m_MachineHash = machineHash;
		m_Status = status;

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

bool rlRosEntitlementSaveOfflineTask::EntitlementSaveFileWorker::Start(const int cpuAffinity)
{
	//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_START
#if RSG_FINAL
	bool success = this->rlWorker::Start("", sysIpcMinThreadStackSize, cpuAffinity, true);
#else
	bool success = this->rlWorker::Start("[RAGE] Entitlement Save File", sysIpcMinThreadStackSize, cpuAffinity, true);
#endif
	return success;
}

void rlRosEntitlementSaveOfflineTask::EntitlementSaveFileWorker::Perform()
{
	rtry
	{
		//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_PERFORM_ENTRY
		// you need to be signed in to save profile-specific data.
#if !__RGSC_DLL
		rverify(g_rlPc.GetProfileManager()->IsSignedIn(), catchall, );
#else
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn(), catchall, );
#endif
		

		rgsc::Profile profile;
#if !__RGSC_DLL
		rverify(SUCCEEDED(g_rlPc.GetProfileManager()->GetSignInInfo(&profile)), catchall, );
#else
		rverify(SUCCEEDED(GetRgscConcreteInstance()->_GetProfileManager()->GetSignInInfo(&profile)), catchall, );
#endif
		//@@: range RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_PERFORM {
		const u8* key = m_MachineHash == NULL ?  profile.GetProfileUniqueKey(): m_MachineHash;
		rverify(key, catchall, );

		unsigned sizeOfBuf = m_BufSize;

		// Protect the data and calculate the signature
		u8 signature[datProtectSignatureSize] = {0};
		datProtectContext context(false, key, m_MachineHash == NULL ? rgsc::RGSC_PROFILE_UNIQUE_KEY_LEN : (u32)strlen((const char*)m_MachineHash));
		datProtect(m_BufPtr, sizeOfBuf, context, signature);

		//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_PERFORM_MEM_CPY
		// copy the signature to the end of the buffer, incrementing the size
		sysMemCpy(m_BufPtr + sizeOfBuf, signature, sizeof(signature));
		sizeOfBuf += sizeof(signature);

		char fullPath[rgsc::RGSC_MAX_PATH] = {0};
#if !__RGSC_DLL
		rverify(SUCCEEDED(g_rlPc.GetFileSystem()->GetTitleProfileDirectory(fullPath, true)), catchall, );
#else
		rverify(SUCCEEDED(GetRgscConcreteInstance()->_GetFileSystem()->GetTitleProfileDirectory(fullPath, true)), catchall, );
#endif

		// add the file name
		safecat(fullPath, ENTITLEMENT_DAT_NAME);

		// create a device at this path
		const fiDevice *device = fiDevice::GetDevice(fullPath, false);
		//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_PERFORM_CREATE_FILE
		u32 dstAttributes = device->GetAttributes(fullPath);
		if (dstAttributes & FILE_ATTRIBUTE_READONLY)
		{
			rlDebug3("Removing read-only attribute from path '%s'", fullPath);
			device->SetAttributes(fullPath, dstAttributes & ~FILE_ATTRIBUTE_READONLY);
		}

		fiHandle hFile = device->Create(fullPath);
		rverify(fiIsValidHandle(hFile), catchall, );

		// write and close the file
		int bytesWritten = device->Write(hFile, m_BufPtr, sizeOfBuf);
		device->Close(hFile);
		//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_PERFORM_CLOSE_FILE

		rverify(bytesWritten == (int)sizeOfBuf, catchall, );
		//@@: } RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_PERFORM 
		m_Status->SetSucceeded(0);
	}
	rcatchall
	{
		m_Status->SetFailed(0);
	}
	//@@: location RLROSENTITLEMENTSAVEOFFLINETASK_ENTITLEMENTSAVEFILEWORKER_PERFORM_EXIT
	
}

/////////////////////////////////
// rlRosEntitlementLoadOfflineTask
/////////////////////////////////
rlRosEntitlementLoadOfflineTask::rlRosEntitlementLoadOfflineTask()
	: m_State(STATE_LOAD_FILE)
	, m_BytesRead(0)
{
	
}

rlRosEntitlementLoadOfflineTask::~rlRosEntitlementLoadOfflineTask()
{

}


bool rlRosEntitlementLoadOfflineTask::Configure(u8* machineHash)
{
	bool success = false;

	rtry
	{
		rverify(!rlRosEntitlement::IsEntitlementIOInProgress(), catchall, );
		//@@: location RLROSENTITLEMENTLOADOFFLINETASK_CONFIGURE_CALL_CONFIGURE
		rverify(m_Worker.Configure(machineHash, &m_WorkerStatus), catchall, );
		m_State = STATE_LOAD_FILE;

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}


void rlRosEntitlementLoadOfflineTask::OnCancel()
{

}

void rlRosEntitlementLoadOfflineTask::Complete(const netTaskStatus )
{

}

netTaskStatus rlRosEntitlementLoadOfflineTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;
	//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ONUPDATE
	switch(m_State)
	{
	case STATE_LOAD_FILE:
		if(WasCanceled())
		{
			netTaskDebug("Canceled - bailing...");
			status = NET_TASKSTATUS_FAILED;
		}
		else if(LoadFile())
		{
			m_State = STATE_LOADING_FILE;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_LOADING_FILE:
		if(m_WorkerStatus.Succeeded())
		{
			//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ONUPDATE_LOADING_FINISHED
			if (WasCanceled())
			{
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				status = NET_TASKSTATUS_SUCCEEDED;
			}
		}
		else if(m_WorkerStatus.Failed())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool rlRosEntitlementLoadOfflineTask::LoadFile()
{
	bool bSuccess = m_Worker.Start(0);
	if (bSuccess)
	{
		m_WorkerStatus.SetPending();
		m_Worker.Wakeup();
	}

	return bSuccess;
}

rlRosEntitlementLoadOfflineTask::EntitlementLoadFileWorker::EntitlementLoadFileWorker()
	: m_BufPtr(NULL)
	, m_Status(NULL)
{
}

bool rlRosEntitlementLoadOfflineTask::EntitlementLoadFileWorker::Configure(u8* machineHash, netStatus* status)
{
	bool success = false;

	rtry
	{
		rverify(status, catchall, );
		m_Status = status;
		m_MachineHash = machineHash;
		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

bool rlRosEntitlementLoadOfflineTask::EntitlementLoadFileWorker::Start(const int cpuAffinity)
{
#if RSG_FINAL
	bool success = this->rlWorker::Start("", sysIpcMinThreadStackSize, cpuAffinity, true);
#else
	bool success = this->rlWorker::Start("[RAGE] Entitlement Load File", sysIpcMinThreadStackSize, cpuAffinity, true);
#endif
	return success;
}

void rlRosEntitlementLoadOfflineTask::EntitlementLoadFileWorker::Perform()
{
	fiHandle hFile = fiHandleInvalid;
	const fiDevice * device = NULL;

	rtry
	{
		char fullPath[rgsc::RGSC_MAX_PATH] = {0};
		//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM_ENTRY
		// you need to be signed in to load profile-specific data.
#if !__RGSC_DLL
		rverify(g_rlPc.GetProfileManager()->IsSignedIn(), catchall, );
		rverify(SUCCEEDED(g_rlPc.GetFileSystem()->GetTitleProfileDirectory(fullPath, true)), catchall, );
#else
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn(), catchall, );
		rverify(SUCCEEDED(GetRgscConcreteInstance()->_GetFileSystem()->GetTitleProfileDirectory(fullPath, true)), catchall, );
#endif

		// add the file name
		//@@: range RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM {
		safecat(fullPath, ENTITLEMENT_DAT_NAME);

		device = fiDevice::GetDevice(fullPath);
		//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM_OPEN_ENTITLEMENT
		fiHandle hFile = device->Open(fullPath, true);

		// if file does not exist, tasks succeeds but returns no buffer
		if (fiIsValidHandle(hFile))
		{
			// file exists, check its file size
			unsigned size = device->Seek(hFile, 0, seekEnd);
			m_BufPtr = rlRosEntitlement::AllocateOfflineEntitlementBlob(size);

			rverify(m_BufPtr, catchall, );

			// Read
			device->Seek(hFile, 0, seekSet);
			unsigned bytesRead = device->Read(hFile, m_BufPtr, size);

			// decrypt and verify integrity and authenticity
			u8 signature[datProtectSignatureSize];
			//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM_DECRYPT_SIGNATURE
			bytesRead -= sizeof(signature);
			memcpy(signature, m_BufPtr + bytesRead, sizeof(signature));

			// get unique profile key from profile
			//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM_GET_SIGNIN_INFO
			rgsc::Profile profile;
#if !__RGSC_DLL
			g_rlPc.GetProfileManager()->GetSignInInfo(&profile);
#else
			GetRgscConcreteInstance()->_GetProfileManager()->GetSignInInfo(&profile);
#endif
			const u8* key = m_MachineHash == NULL ?  profile.GetProfileUniqueKey(): m_MachineHash;
			rverify(key, catchall, );

			// decrypt and verify integrity of entitlement file
			datProtectContext context(false, key,  m_MachineHash == NULL ? rgsc::RGSC_PROFILE_UNIQUE_KEY_LEN : (u32)strlen((const char*)m_MachineHash));
			bool valid = datUnprotect(m_BufPtr, bytesRead, context, signature);
			//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM_AFTER_UNPROTECT
			rverify(valid, catchall, rlError("Entitlement file is corrupt or has been modified by an untrusted source."));
			//@@: } RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM
			// close the file
			device->Close(hFile);
		}
		//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM_EXIT_A
		m_Status->SetSucceeded(0);
	}
	rcatchall
	{
		// Close file if necessary
		if (device && fiIsValidHandle(hFile))
		{
			device->Close(hFile);
		}
		
		// Offline task failed
		//@@: location RLROSENTITLEMENTLOADOFFLINETASK_ENTITLEMENTLOADFILEWORKER_PERFORM_EXIT_B
		m_Status->SetFailed(0);
	}
	
}
#endif //!PRELOADED_SOCIAL_CLUB && RSG_ENTITLEMENT_ENABLED
}
#endif //RSG_PC
