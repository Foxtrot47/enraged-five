// 
// rline/rlnpcommondialog.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS

#ifndef RLINE_RLNPCOMMONDIALOG_H
#define RLINE_RLNPCOMMONDIALOG_H

#include "rline/rldiag.h"
#include "rline/rl.h"
#include "rline/rlgamerinfo.h"
#include "rline/rlsystemui.h"
#include "rline/facebook/rlfacebookcommon.h"

#include <np.h>
#include <save_data_dialog.h>
#include <invitation_dialog.h>
#include <np_commerce_dialog.h>

#if RL_FACEBOOK_ENABLED
#include <np_sns_facebook_dialog.h>
#endif // RL_FACEBOOK_ENABLED

namespace rage
{

class rlNpCommonDialog
{
public:
	rlNpCommonDialog();
	~rlNpCommonDialog();

    static void InitLibs();

	bool Init();
	void Shutdown();
	void Update();

    // Global
    void SetScreenDimensions(const int width, const int height);

	// MSG
	int ShowEmptyStoreMsg();
	int ShowChatRestrictionMsg(int userId);
	int ShowUGCRestrictionMsg(int userId);

	// SAVEDATA
	int ShowSaveListDialog(int userId, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, const SceSaveDataDirName *dirNames, const uint32_t hitNum, bool showNewItem);
	int ShowSaveDialogConfirm(int userId, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, const SceSaveDataDirName *dirName, const SceSaveDataDialogSystemMessageType systemMessageType);
	int ShowSaveDialogOperating(int userId, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, SceSaveDataDirName* dirName);
	int ShowSaveDialogError(int userId, int errCode, const SceSaveDataDialogType type, const SceSaveDataTitleId * titleId, SceSaveDataDirName* dirName);
	int ShowSaveDialogNoSpace(int userId, const SceSaveDataTitleId * titleId, const SceSaveDataDirName* dirName, u64 requiredBlocks);
	int ShowSaveDialogFileCorrupt(int userId, const SceSaveDataTitleId * titleId, const SceSaveDataDirName* dirName);
	const SceSaveDataDialogResult* GetSaveDataResult() {return &m_SaveDataResult;}

	void ShowLoadDialog(int signInId);
	void ShowDeleteDialog(int signInId);

	// PROFILE
	bool ShowProfile(SceUserServiceUserId userId, const rlGamerHandle& targetGamer);

	// BROWSER
    static const int DEFAULT_BROWSER_TITLE_HEIGHT = 50;
    static const int DEFAULT_BROWSER_ADDR_HEIGHT = 65;
    static const int DEFAULT_BROWSER_FOOTER_HEIGHT = 136;
    bool ShowUrl(int userId, const rlSystemBrowserConfig& config);
    void SetBrowserTitleHeight(int titleHeight) { m_BrowserTitleHeight = titleHeight; }
    void SetBrowserAddressHeight(int addressHeight) { m_BrowserAddressHeight = addressHeight; }
    void SetBrowserFooterHeight(int footerHeight) { m_BrowserFooterHeight = footerHeight; }
	bool CloseBrowser();

	// Invitation
	bool SendInvitation(int userId, const rlSceNpSessionId& sessionId, const char * salutation);
	bool ReceiveInvitation(int userId);

	// Commerce
	bool ShowCommerceCategoryDialog(const int localGamerIndex, int userId, int numLabels, const char * const *categoryLabel);
	bool ShowCommerceProductDialog(int userId, int numLabels, const char * const * productLabels);
	bool ShowCommerceCheckoutDialog(int userId, int numLabels, const char * const * skuIds);
	bool ShowCommerceDownloadDialog(int userId, int numLabels, const char * const * skuIds);
	bool ShowCommerceCodeEntryDialog(int userId);
	bool ShowCommercePSPlusDialog(int userId);
	const SceNpCommerceDialogResult* GetCommerceResult() {return &m_CommerceResult;}

	enum eCommerceLogoPosition
	{
		COMMERCE_LOGO_POS_CENTER = SCE_NP_COMMERCE_PS_STORE_ICON_CENTER,
		COMMERCE_LOGO_POS_LEFT = SCE_NP_COMMERCE_PS_STORE_ICON_LEFT,
		COMMERCE_LOGO_POS_RIGHT = SCE_NP_COMMERCE_PS_STORE_ICON_RIGHT,
	};

	bool ShowCommerceLogo(eCommerceLogoPosition pos);
	bool HideCommerceLogo();

#if RL_FACEBOOK_ENABLED
	// Facebook
	bool ShowFacebookPermissionDialog(int userId, u64 appId, const char * permissions);
	SceNpSnsFacebookDialogResult* GetFacebookResult() { return &m_FacebookResult; }
#endif // RL_FACEBOOK_ENABLED

	// Friends
	bool ShowFriendsDialog(int userId, bool bSearch, bool bOnlineOnly);

	// Game Custom Data
	bool ShowGameCustomData(int userId, void * data, int dataLen, u8* imgBuf, int imgLen, const char * userMessage, const char * dataDetail, const char * dataName, int expireMinutes);

	// STATUS
	SceCommonDialogStatus GetStatus() { return m_CurrentDialogStatus; }
	
	// PURPOSE
	//	If any dialog is running this will return true
	bool IsDialogShowing() { return m_bDialogShowing; }

#if RL_NP_DEEP_LINK_MANUAL_CLOSE
    // PURPOSE
    //	Sets time at which we close the browser after making a deep link request
    static void SetBrowserDeepLinkCloseTime(const unsigned browserDeepLinkCloseTime);
#endif

    // PURPOSE
    //	Sets whether an initialise fail should fail the Prepare call
    static void SetFailWhenInitialiseFails(const bool bFailWhenInitialiseFails);

    // PURPOSE
    //  This loads the browser module ahead of time (helpful for deep link opening)
    bool PreloadBrowserDialog();

private:

	enum DialogType
	{
		DIALOG_NONE,
		DIALOG_MSG,
		DIALOG_COMMERCE,
		DIALOG_PROFILE,
		DIALOG_SAVEDATA,
		DIALOG_BROWSER,
		DIALOG_INVITATION,
#if RL_FACEBOOK_ENABLED
		DIALOG_FACEBOOK,
#endif // RL_FACEBOOK_ENABLED
		DIALOG_FRIENDS,
		DIALOG_CUSTOM_GAMEDATA,
	};

	int PrepareDialog(DialogType desiredDialogType);
	int InitializeDialog(DialogType desiredDialogType);
	int ShutdownCurrentDialog();

	void UpdateMessageDialog();
	void UpdateCommerceDialog();
	void UpdateProfileDialog();
	void UpdateSaveDataDialog();
	void UpdateBrowserDialog();
	void UpdateInvitationDialog();
#if RL_FACEBOOK_ENABLED
	void UpdateFacebookDialog();
#endif // RL_FACEBOOK_ENABLED
	void UpdateFriendsDialog();
	void UpdateGameCustomDataDialog();

	DialogType m_CurrentDialogType;
	bool m_bDialogShowing;
	SceCommonDialogStatus m_CurrentDialogStatus;

#if RL_FACEBOOK_ENABLED
	SceNpSnsFacebookDialogResult m_FacebookResult;
#endif // RL_FACEBOOK_ENABLED

	SceNpCommerceDialogMode m_CommerceMode;
	SceNpCommerceDialogResult m_CommerceResult;
	SceSaveDataDialogResult m_SaveDataResult;

	SceUserServiceUserId m_UserId;

	// Restriction Message Queuing
	bool m_ChatMsgQueued;
	bool m_UgcMsgQueued;
	int m_QueuedId;

    int m_ScreenWidth;
    int m_ScreenHeight; 
    int m_BrowserTitleHeight;
    int m_BrowserAddressHeight;
	int m_BrowserFooterHeight;
	rlSystemBrowserConfig m_BrowserConfig;

	enum DeepLinkUrlState
	{
		DLS_Inactive,
		DLS_Opening,
		DLS_Open,
		DLS_Closed,
	};
	DeepLinkUrlState m_DeepLinkUrlState;

#if RL_NP_DEEP_LINK_MANUAL_CLOSE
	unsigned m_DeepLinkStateStartedTime;
	unsigned m_DeepLinkCloseTime;
	static unsigned ms_BrowserDeepLinkCloseTime;
#endif

    static bool ms_bLibsInitialized;
    static bool ms_FailWhenInitialiseFails;
};

} // namespace rage

#endif // RLINE_RLNPPROFILEDIALOG_H
#endif // RSG_ORBIS