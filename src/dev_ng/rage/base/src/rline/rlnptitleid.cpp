// 
// rline/rlnptitleid.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_NP

#include "rlnptitleid.h"
#include "string/string.h"
#include "system/nelem.h"

namespace rage
{

rlNpTitleId::rlNpTitleId(
						 const SceNpCommunicationId* commId,
                         const SceNpCommunicationSignature* commSig,
                         const SceNpCommunicationPassphrase* commPassphrase,
#if RSG_ORBIS
						 const SceNpTitleId* sceNpTitleId,
						 const SceNpTitleSecret* sceNpTitleSecret
#else
                         const char* ticketingServiceId,
                         const char* serviceId
#endif
						 )
    : m_HaveCommId(false)
    , m_HaveCommSig(false)
    , m_HaveCommPassphrase(false)
#if RSG_ORBIS
	, m_HaveTitleId(false)
	, m_HaveTitleSecret(false)
#else
    , m_HaveTicketingServiceId(false)
    , m_HaveServiceId(false)
#endif
{
    if(commId)
    {
        m_CommId = *commId;
        m_HaveCommId = true;
    }

    if(commSig)
    {
        m_CommSig = *commSig;
        m_HaveCommSig = true;
    }

    if(commPassphrase)
    {
        m_CommPassphrase = *commPassphrase;
        m_HaveCommPassphrase = true;
    }

#if RSG_ORBIS
	if(sceNpTitleId)
	{
		m_SceNpTitleId = *sceNpTitleId;
		m_HaveTitleId = true;
	}

	if(sceNpTitleSecret)
	{
		m_SceNpTitleSecret = *sceNpTitleSecret;
		m_HaveTitleSecret = true;
	}
#else
    if(ticketingServiceId)
    {
        rlAssert(19 == strlen(ticketingServiceId));

        m_HaveTicketingServiceId = true;

        safecpy(m_TicketingServiceId, ticketingServiceId, COUNTOF(m_TicketingServiceId));
    }

    if(serviceId)
    {
        rlAssert(19 == strlen(serviceId));

        m_HaveServiceId = true;

        safecpy(m_ServiceId, serviceId, COUNTOF(m_ServiceId));
    }
#endif
}

const SceNpCommunicationId*
rlNpTitleId::GetCommunicationId() const
{
    return m_HaveCommId ? &m_CommId : NULL;
}

const SceNpCommunicationSignature*
rlNpTitleId::GetCommunicationSignature() const
{
    return m_HaveCommSig ? &m_CommSig : NULL;
}

const SceNpCommunicationPassphrase*
rlNpTitleId::GetCommunicationPassphrase() const
{
    return m_HaveCommPassphrase ? &m_CommPassphrase : NULL;
}

#if RSG_ORBIS
const SceNpTitleId*
rlNpTitleId::GetSceNpTitleId() const
{
	return m_HaveTitleId ? &m_SceNpTitleId : NULL;
}

const SceNpTitleSecret*
rlNpTitleId::GetSceNpTitleSecret() const
{
	return m_HaveTitleSecret ? &m_SceNpTitleSecret : NULL;
}
#else
const char* 
rlNpTitleId::GetTicketingServiceId() const
{
    return m_HaveTicketingServiceId ? m_TicketingServiceId : NULL;
}

const char* 
rlNpTitleId::GetServiceId() const
{
    return m_HaveServiceId ? m_ServiceId : NULL;
}
#endif

}   //namespace rage

#endif //RSG_NP
