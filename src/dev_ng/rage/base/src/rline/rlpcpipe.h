// 
// rline/rlpcpipe.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_PC

#ifndef RLINE_RLPCPIPE_H
#define RLINE_RLPCPIPE_H


#include "file/file_config.h"

#if !__RGSC_DLL
#include "system/xtl.h"
#endif

#include "atl/queue.h"
#include "system/timer.h"
#include "net/status.h"
#include "net/task.h"
#include "rline/rlhttptask.h"
#include "rline/ros/rlroshttptask.h"
#include "system/threadpool.h"

#define PIPE_BUFFER_SIZE		4096
#define SKU_NAME_SIZE			64
#define MSG_HEADER_SIZE			4
#define MSG_TYPE_SIZE			5
#define SKU_NAME_OFFSET (MSG_HEADER_SIZE + MSG_TYPE_SIZE)

#define ENABLE_LAUNCHER_CHALLENGE (RSG_CPU_X64 && !__RGSC_DLL)

#if ENABLE_LAUNCHER_CHALLENGE
#define CURVE_BUFFER_SIZE			64
#define CURVE_BUFFER_STRING_SIZE	((CURVE_BUFFER_SIZE *2) + 1)
#define SHARED_SECRET_SIZE			32
#define HMAC_SIZE					32
#define IV_SIZE						16
#define HMAC_STRING_SIZE			((HMAC_SIZE *2) + 1)
#define GENERIC_BUFFER_SIZE			256
// Have a response anywhere between 5 and 10 minutes
#define CHALLENGE_REQUEST_MIN 1000*60*5
#define CHALLENGE_REQUEST_MAX 1000*60*10
#endif
namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, pcpipe);

// Fwd Declare
class rlPcEvent;

typedef void (*pipeDownloadProgressCallback)(int progress, const char* skuName);
typedef void (*pipeTransferCompleteCallback)(const char* skuName);
typedef void (*pipeDownloadErrorCallback)(const char* errorString, const char* skuName);

///////////////////////////////////////////////////////////////////////////////
// rlPcPipeUtility
///////////////////////////////////////////////////////////////////////////////
#if ENABLE_LAUNCHER_CHALLENGE
class rlPcPipeUtility
{
public:
	// Utility functions to facilitate the implementation of requiring ASCII to communicate
	// with the launcher
	static void hexArrayToString(char * dst, unsigned char *ptr, unsigned int len)
	{
		unsigned int i = 0;
		for (i=0; i<len; i++) 
		{
			sprintf(dst+i*2, "%02X", ptr[i]);
		}
		dst[i*2] = 0;

	}

	static void hexStringToArray(unsigned char * dst, const char *src, unsigned int len)
	{
		int byteArrayLen = (len-1) / 2;
		const char * ptrWalker = src;
		char temp = 0;
		for(int i = 0; i < byteArrayLen; i++) {
			sscanf(ptrWalker, "%02X", &temp);
			dst[i] = temp;
			ptrWalker += 2 * sizeof(char);
		}
	}
#if !RSG_FINAL
	//static char * printPointer(char * indicator, unsigned char * ptr, unsigned int len)
	//{
	//
	//	char *debugBuff = rage_new char[GENERIC_BUFFER_SIZE];
	//	char *debugBuffPtr = debugBuff;
	//	int debugBuffPtrIdx = 0;
	//	debugBuffPtrIdx+=sprintf(debugBuffPtr+debugBuffPtrIdx, "%s:		0x:", indicator);
	//
	//	for(unsigned int i = 0; i < len; i ++)
	//		debugBuffPtrIdx+=sprintf(debugBuffPtr+debugBuffPtrIdx, "%02X",ptr[i] );
	//	
	//	return debugBuff;
	//}
#endif // !RSG_FINAL
};
#endif // ENABLE_LAUNCHER_CHALLENGE

////////////////////////////////////////////////////////////////////////////////
// rlPcPipeMessage
///////////////////////////////////////////////////////////////////////////////
#define PCPIPE_DECL(x)							\
const char* GetMessageName() { return x; }		\
static const int PREFIX_LEN = COUNTOF(x);
			

class rlPcPipeMessageBase
{
public:
	rlPcPipeMessageBase() {}
	virtual const char* GetBuffer() = 0;
	virtual const char* GetMessageName() = 0;
};

class rlPcPipeSignOutMessage : public rlPcPipeMessageBase
{
public:
	PCPIPE_DECL("SCSO:")
	bool Init();
	const char* GetBuffer() { return m_Buffer; }

private:
	static const int MAX_BUF_SIZE = PREFIX_LEN +		// message prefix
									1;					// null terminator

	char m_Buffer[MAX_BUF_SIZE];
};

class rlPcPipeSignInMessage : public rlPcPipeMessageBase
{
public:
	PCPIPE_DECL("SCSI:")
	bool Init(RockstarId id);
	const char* GetBuffer() { return m_Buffer; }

private:
	static const int MAX_BUF_SIZE = PREFIX_LEN +			// message prefix
									sizeof(RockstarId) +	// rockstar id
									1;						// null terminator

	char m_Buffer[MAX_BUF_SIZE];
};

class rlPcPipeTicketChangedMessage : public rlPcPipeMessageBase
{
public:
	PCPIPE_DECL("SCTC:")
	rlPcPipeTicketChangedMessage();
	virtual ~rlPcPipeTicketChangedMessage();

	bool Init(RockstarId id, const char* xmlResponse);
	const char* GetBuffer() { return m_Buffer; }

private:
	static const int MSG_BUF_SIZE = PREFIX_LEN +			// message prefix
									sizeof(RockstarId) +	// rockstar id
									1;						// deliminator

	char* m_Buffer;
};

#if ENABLE_LAUNCHER_CHALLENGE
class rlPcPipeLauncherChallengeMessage : public rlPcPipeMessageBase
{
public:
	rlPcPipeLauncherChallengeMessage();
	virtual ~rlPcPipeLauncherChallengeMessage();
	PCPIPE_DECL("TRVR:")
	const char* GetBuffer() { return m_Buffer; }
	bool Init(const char *);
private:
	static const int MSG_BUF_SIZE = PREFIX_LEN +			// message prefix
									GENERIC_BUFFER_SIZE +	// rockstar id
									1;						// deliminator
	char* m_Buffer;
};
#endif

///////////////////////////////////////////////////////////////////////////////
// rlPCPipe
///////////////////////////////////////////////////////////////////////////////
class rlPCPipe
{
public:
	rlPCPipe();

	void	OnPcEvent(const rlPcEvent* evt);

	void	Connect(const char* pipeName = NULL);
	void	Disconnect();

	void	Update();

	void	Read();
	bool	Write(const char* writeBuffer);
	void	ProcessMessageQueue();

	void	CheckIncomingData(DWORD bytesRead);
	void	HandleMessage(char* header, char* skuName, char* messageBuffer, int bufferSize);

	bool	IsConnected() { return m_bConnected && (m_pipeHandle != INVALID_HANDLE_VALUE); }
	bool	IsStandardUser() { return m_bIsStandardUser; }

	bool	IsAlive() { return m_bIsAlive; }
	float	GetLastAliveTime() { return m_LauncherAliveTimer.GetMsTime(); }

	int		ReadHeaderAmount(char* buffer);
	void	SizeToHeaderSize(int size, unsigned char* header);

	void	AddCallbacks(	pipeDownloadProgressCallback progressCallback, 
							pipeTransferCompleteCallback completeCallback,
							pipeDownloadErrorCallback errorCallback );

	void	RemoveCallbacks();

	void	PauseDownloads();
	void	ResumeDownloads();

	void	RestartGame();

	bool	QueuePcPipeMessage(rlPcPipeMessageBase* msg);
	bool    RequestSignInTransferFile(RockstarId id);
	bool	ForwardCredentialsToLauncher(const char* xmlResponse);

	void	StartDownloadWithToken(const char* skuName, const char* downloadToken);
	void	StartDownloadWithUrl(const char* skuName, const char* downloadUrl, const char* localFilename = NULL, const char* arguments = NULL);

#if ENABLE_LAUNCHER_CHALLENGE
	void	SendGameEccKey();
	void	SendLauncherChallenge();
	bool    CalculateHMACAndCompare(const int localGamerIndex, netStatus* status, unsigned char *challenge, unsigned char *clientHmac);	
#endif

private:
	bool		m_bConnected;
	bool		m_bIsStandardUser;
	bool		m_bIsAlive;
#if ENABLE_LAUNCHER_CHALLENGE
	netStatus	m_Status;
	netStatus	m_ChallengeStatus;
	bool		m_bIsPreparedToChallenge;
	bool		m_bIsWaitingForChallenge;
#endif

	static const int MAX_MESSAGES = 10;
	atQueue<rlPcPipeMessageBase*, MAX_MESSAGES> m_MessageQueue;

	netStatus m_SignInTaskStatus;

	HANDLE		m_pipeHandle;
	OVERLAPPED	m_overlap;
	sysTimer	m_LauncherAliveTimer;

	char	m_ReadBuffer[PIPE_BUFFER_SIZE];

	pipeDownloadProgressCallback m_downloadProgressCallback;
	pipeTransferCompleteCallback m_transferCompleteCallback;
	pipeDownloadErrorCallback m_downloadErrorCallback;
#if ENABLE_LAUNCHER_CHALLENGE
	static u32 sm_LauncherCheckStartTime;
	static u32 sm_LauncherCheckEndTime;
#endif
};


class rlPcPipeCreateSignInTransferDataWorkItem : public sysThreadPool::WorkItem
{
public:
	rlPcPipeCreateSignInTransferDataWorkItem();
	virtual ~rlPcPipeCreateSignInTransferDataWorkItem();

	bool Configure(bool* resultPtr);
	virtual void DoWork();

private:
	bool* m_ResultPtr;
};

class rlPcPipeSigninTask : public netTask
{
public:

	NET_TASK_DECL(rlPcPipeSigninTask)
	NET_TASK_USE_CHANNEL(rline_pcpipe);

	rlPcPipeSigninTask();

	virtual bool Configure(RockstarId id);
	virtual netTaskStatus OnUpdate(int* resultCode);

private:
	enum State
	{
		STATE_CREATE_SIGNIN_TRANSFER,
		STATE_CREATING_SIGNIN_TRANSFER,
		STATE_SEND_PC_PIPE,
	};

	State m_State;
	bool m_SignInResult;
	RockstarId m_RockstarId;
	rlPcPipeCreateSignInTransferDataWorkItem m_WorkItem;
};

#if (RSG_CPU_X64 && !__RGSC_DLL)
class rlPcPipeLauncherBaseWorkItem : public sysThreadPool::WorkItem
{
public:
	void	EncryptAndStringify(char * dst, unsigned char * input);
	void    Decrypt(unsigned char*dst, unsigned char*input);
	void    Encrypt(unsigned char*dst, unsigned char*input);
};

class rlPcPipeSendLauncherChallengeWorkItem : public rlPcPipeLauncherBaseWorkItem
{
public:
	rlPcPipeSendLauncherChallengeWorkItem();
	virtual ~rlPcPipeSendLauncherChallengeWorkItem();
	bool Configure(unsigned char *);
	virtual void DoWork();
private:
	unsigned char * m_challengePtr;
};

class rlPcPipeSendLauncherChallengeTask : public netTask
{
public:

	NET_TASK_DECL(rlPcPipeSendLauncherChallengeTask)
		NET_TASK_USE_CHANNEL(rline_pcpipe);

	rlPcPipeSendLauncherChallengeTask();

	virtual bool Configure(unsigned char*);
	virtual netTaskStatus OnUpdate(int*);

private:
	enum State
	{
		STATE_START_CHALLENGE,
		STATE_PENDING_CHALLENGE,
		STATE_CHALLENGE_SENT
	};

	State m_State;
	unsigned char * m_challengePtr;
	rlPcPipeSendLauncherChallengeWorkItem m_WorkItem;
};
#endif // (RSG_CPU_X64 && !__RGSC_DLL)

} // namespace rage

#endif

#endif // RSG_PC
