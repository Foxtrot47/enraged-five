// 
// rline/rlscmatchmanager.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscmatchmanager.h"
#include "rline/rlsessionmanager.h"
#include "rline/rl.h"
#include "rline/rltask.h"
#include "diag/seh.h"
#include "system/timer.h"

namespace rage
{

#undef __rage_channel
#define __rage_channel rline_scmatchmaking

static bool s_MatchReaperEnabled = true;
static bool s_EnableSessionIdAttribute = false;

////////////////////////////////////////////////////////////////////////////////
// rlScMatch
////////////////////////////////////////////////////////////////////////i////////
rlScMatch::rlScMatch()
{
	m_UnadvertiseAttempts = 0;
	Clear();
}

rlScMatch::~rlScMatch()
{
	Shutdown();
}

void
rlScMatch::Clear()
{
	m_LocalGamerIndex = -1;
	m_NumSlots = 0;
	m_AvailableSlots = 0;
	m_SessionInfo.Clear();
	m_Attrs.Clear();
	m_NextCheckTime = 0;
	if(m_Status.Pending())
	{
		rlDebug("rlScMatch::Clear() - Status for match [%s] pending. Canceling task!", m_MatchId.IsValid() ? m_MatchId.ToString() : "INVALID");
		rlGetTaskManager()->CancelTask(&m_Status);	
		if(!netVerify(!m_Status.Pending()))
		{
			rlDebug("rlScMatch::Clear() - Status for match [%s] pending after task cancellation!", m_MatchId.IsValid() ? m_MatchId.ToString() : "INVALID");
		}
	}
	m_MatchId.Clear();
}

bool
rlScMatch::Reset(const int localGamerIndex,
				   const rlScMatchmakingMatchId& matchId,
				   const unsigned numSlots,
				   const unsigned availableSlots,
				   const rlSessionInfo& sessionInfo,
				   const rlMatchingAttributes& attrs)
{
	rtry
	{
		Clear();

		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(matchId.IsValid(), catchall, );
		rverify(sessionInfo.IsValid(), catchall, );

		m_LocalGamerIndex = localGamerIndex;
		m_MatchId = matchId;
		m_NumSlots = numSlots;
		m_AvailableSlots = availableSlots;
		m_SessionInfo = sessionInfo;
		m_Attrs = attrs;

		m_NextCheckTime = sysTimer::GetSystemMsTime() + rlScMatchManager::ms_CheckIntervalMs;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
rlScMatch::Update()
{
	if(!s_MatchReaperEnabled)
	{
		return;
	}

	if(m_Status.Succeeded())
	{
		// rlScMatchmakingUnadvertiseTask should delete this match on success, but delete it here just in case it doesn't
		rlScMatchManager::DeleteMatch(this);
		return;
	}
	else if(m_Status.Failed())
	{
		// can get here if we get an HTTP error code. If the HTTP request succeeded but we got an SCS error back, rlScMatchmakingUnadvertiseTask deletes the match.
		m_UnadvertiseAttempts++;
		if(m_UnadvertiseAttempts >= 10)
		{
			// don't attempt to unadvertise forever
			rlScMatchManager::DeleteMatch(this);
			return;
		}

		m_NextCheckTime = sysTimer::GetSystemMsTime() + rlScMatchManager::ms_RetryIntervalMs;
		m_Status.Reset();
	}

	// don't attempt to schedule matchmaking tasks when offline, we'll pick this back up when we sign back in (if we sign back in)
	// we still poll and respond to in-flight tasks as above
	if(!rlRos::IsOnline(m_LocalGamerIndex))
	{
		return;
	}

	bool timeToCheck = (m_NextCheckTime > 0) && (sysTimer::GetSystemMsTime() >= m_NextCheckTime);

	if(timeToCheck && !IsPending())
	{
		if(!rlSessionManager::IsHostingSession(m_SessionInfo, m_MatchId))
		{
			rlDebug("rlScMatch::Update() - Unadvertising match [%s] since we're not hosting it!", m_MatchId.IsValid() ? m_MatchId.ToString() : "INVALID");

			if(rlVerify(rlScMatchmaking::Unadvertise(m_LocalGamerIndex, m_MatchId, &m_Status)) == false)
			{
				rlError("rlScMatch::Update() - Failed to Unadvertise match [%s]", m_MatchId.IsValid() ? m_MatchId.ToString() : "INVALID");

				// error starting async task. Assume further attempts will also fail.
				rlScMatchManager::DeleteMatch(this);
				return;
			}
		}
		else
		{
			m_NextCheckTime = sysTimer::GetSystemMsTime() + rlScMatchManager::ms_CheckIntervalMs;
		}
	}
}

void
rlScMatch::Shutdown()
{
	Clear();
}

bool
rlScMatch::IsPending()
{
	return m_Status.Pending();
}

void 
rlScMatch::Unadvertising()
{
	if(!m_Status.Pending())
	{
		// someone else is trying to unadvertise this match, wait one extra cycle before checking again
		// this eliminates some duplicate unadvertise calls in cases where there is already an in-flight unadvertise for this match
		m_NextCheckTime += rlScMatchManager::ms_CheckIntervalMs;
	}
}

////////////////////////////////////////////////////////////////////////////////
// rlScMatchManager
////////////////////////////////////////////////////////////////////////////////
typedef inlist<rlScMatch, &rlScMatch::m_ListLink> MatchList;
static MatchList s_MatchList;

unsigned rlScMatchManager::ms_CheckIntervalMs = rlScMatchManager::DEFAULT_CHECK_INTERVAL_MS;
unsigned rlScMatchManager::ms_RetryIntervalMs = rlScMatchManager::DEFAULT_RETRY_INTERVAL_MS;

bool 
rlScMatchManager::InitClass()
{
	return true;
}

void 
rlScMatchManager::ShutdownClass()
{
	DeleteAllMatches();
}

void
rlScMatchManager::UpdateClass()
{
	MatchList::iterator it = s_MatchList.begin();
	MatchList::iterator next = it;
	MatchList::const_iterator stop = s_MatchList.end();
	for(++next; stop != it; it = next, ++next)
	{
		// Note: match->Update() can delete a match from the list
		rlScMatch* match = (*it);
		match->Update();
	}
}

void
rlScMatchManager::EnableSessionIdAttribute(const bool enable)
{
	if(s_EnableSessionIdAttribute != enable)
	{
		rlDebug("EnableSessionIdAttribute :: %s", enable ? "Enabling" : "Disabling");
		s_EnableSessionIdAttribute = enable;
	}
}

bool 
rlScMatchManager::IsSessionIdAttributeEnabled()
{
	return s_EnableSessionIdAttribute;
}

void
rlScMatchManager::EnableMatchReaper(const bool enable)
{
	if(s_MatchReaperEnabled != enable)
	{
		rlDebug("EnableMatchReaper :: %s", enable ? "Enabling" : "Disabling");
		s_MatchReaperEnabled = enable;
	}
}

void 
rlScMatchManager::SetReaperIntervals(const unsigned checkIntervalMs, const unsigned retryIntervalMs)
{
	rlDebug("SetReaperIntervals :: Check: %u, Retry: %u", checkIntervalMs, retryIntervalMs);
	ms_CheckIntervalMs = checkIntervalMs;
	ms_RetryIntervalMs = retryIntervalMs;
}

rlScMatch* 
rlScMatchManager::AllocateMatch()
{
	if(!s_MatchReaperEnabled)
	{
		return NULL;
	}

	rlScMatch* match = NULL;

	rtry
	{
		match = RL_ALLOCATE_T(rlScMatchManager, rlScMatch);
		rverify(match, catchall, );
		new(match) rlScMatch();
	}
	rcatchall
	{

	}

	return match;
}

bool 
rlScMatchManager::AddMatch(rlScMatch* match)
{
	s_MatchList.push_back(match);
	return true;
}

void  
rlScMatchManager::DeleteMatch(rlScMatch* match)
{
	s_MatchList.erase(match);
	RL_DELETE(match);
}

bool
rlScMatchManager::HasAnyMatches()
{
	return !s_MatchList.empty();
}

void 
rlScMatchManager::DeleteAllMatches()
{
	MatchList::iterator it = s_MatchList.begin();
	MatchList::iterator next = it;
	MatchList::const_iterator stop = s_MatchList.end();
	for(++next; stop != it; it = next, ++next)
	{
		rlScMatch* match = (*it);
		DeleteMatch(match);
	}
}

void 
rlScMatchManager::UnadvertiseAllMatches()
{
	MatchList::iterator it = s_MatchList.begin();
	MatchList::iterator next = it;
	MatchList::const_iterator stop = s_MatchList.end();
	for(++next; stop != it; it = next, ++next)
	{
		rlScMatch* match = (*it);
		match->Unadvertising();
	}
}

rlScMatch*
rlScMatchManager::GetMatchById(const rlScMatchmakingMatchId& matchId)
{
	MatchList::iterator it = s_MatchList.begin();
	MatchList::iterator next = it;
	MatchList::const_iterator stop = s_MatchList.end();
	for(++next; stop != it; it = next, ++next)
	{
		rlScMatch* match = (*it);
		if(match->m_MatchId == matchId)
		{
			return match;
		}
	}	

	return NULL;
}

}   //namespace rage
