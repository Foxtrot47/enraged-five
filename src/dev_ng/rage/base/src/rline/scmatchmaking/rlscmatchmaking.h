// 
// rline/rlscmatchmaking.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCMATCHMAKING_H
#define RLINE_RLSCMATCHMAKING_H

#include "rline/rl.h"
#include "rline/rldiag.h"
#include "rline/rlgamerinfo.h"
#include "rline/rlmatchingattributes.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroscommon.h"

namespace rage
{

class netStatus;
class rlSessionInfo;

RAGE_DECLARE_SUBCHANNEL(rline, scmatchmaking);

class rlScMatchmakingMatchId
{
public:
	rlScMatchmakingMatchId();

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = 16;

	bool operator==(const rlScMatchmakingMatchId& that) const;

	void Clear();

	//PURPOSE
	//  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
	//  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

	const char* ToString() const;
	bool FromString(const char* s);

	//PURPOSE
	//	Has valid content
	bool IsValid() const;

private:
	void SetString();

	// example: 98e58b97-b3a7-4e89-9b47-5a696f992a38
	// making this 38 chars instead of 37 because of a bug in formatf
	char m_GuidString[38];
	u8 m_Guid[16];
};

//PURPOSE
//  API for SC matchmaking.
class rlScMatchmaking
{
public:
    //PURPOSE
    //  Init/Update/Shutdown.  Users do not need to call these; they are
    //  called automatically by rlInit and rlShutdown.
    static bool InitClass();
    static void ShutdownClass();
	static void UpdateClass();

    //PURPOSE
    //  Cancels the task that the specified status object is monitoring.
    static void Cancel(netStatus* status);

    //PURPOSE
	// Advertises a session in matchmaking.
    //PARAMS
    //  localGamerIndex     - Gamer who is advertising the session.
    //  numSlots			- Maximum number of player slots.
	//  availableSlots		- Currently available number of player slots.
	//  attrs				- Matchmaking attributes.
	//  sessionId			- The globally unique id of the session (doesn't change with host migrations)
	//  sessionInfo			- The info for the session being advertised.
	//  matchId				- Upon successful completion, this contains the SC match id.
	//						  Caller must not deallocate this until the operation completes.
    //RETURNS
    //  False if failed to start request.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    static bool Advertise(const int localGamerIndex,
						  const unsigned numSlots,
						  const unsigned availableSlots,
						  const rlMatchingAttributes& attrs,
						  const u64 sessionId,
						  const rlSessionInfo& sessionInfo,
						  rlScMatchmakingMatchId* matchId,
						  netStatus* status);

    //PURPOSE
	// Updates the advertised data for a session being advertised in matchmaking.
    //PARAMS
    //  localGamerIndex     - Gamer who is advertising the session.
	//  matchId				- Unique match identifier returned from a prior call to Advertise.
    //  numSlots			- Maximum number of player slots.
	//  availableSlots		- Currently available number of player slots.
	//  sessionInfo			- The info for the session being updated.
	//  attrs				- Matchmaking attributes.
    //RETURNS
    //  False if failed to start request.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    static bool Update(const int localGamerIndex,
					  const rlScMatchmakingMatchId& matchId,
					  const unsigned numSlots,
					  const unsigned availableSlots,
					  const rlSessionInfo& sessionInfo,
					  const rlMatchingAttributes& attrs,
					  netStatus* status);

    //PURPOSE
	// Finds advertised sessions.
	//PARAMS
    //  localGamerIndex     - Gamer who is advertising the session.
	//  availableSlots		- Minimum number of available slots.
	//  filter				- Contains the filter name and conditions used to filter matches.
	//						- These must match the filter pre-configured on the SC server.
	//  maxResults			- Maximum number of results to be retrieved.
	//  results				- Will be populated with details from found sessions.
	//						  Caller must not deallocate this until the operation completes.
	//  numResults			- Will be populated with the number of results retrieved
	//RETURNS
    //  False if failed to start request.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    static bool Find(const int localGamerIndex,
					 const unsigned availableSlots,
					 const rlMatchingFilter& filter,
					 const unsigned maxResults,
					 rlSessionInfo* results,
					 unsigned* numResults,
					 netStatus* status);

    //PURPOSE
	// Removes an existing advertisement.
    //PARAMS
    //  localGamerIndex     - Gamer who is unadvertising the session.
	//  matchId				- Unique match identifier returned from a prior call to Advertise.
    //RETURNS
    //  False if failed to start request.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    static bool Unadvertise(const int localGamerIndex,
							const rlScMatchmakingMatchId& matchId,
							netStatus* status);

    //PURPOSE
	// Removes all advertisements owned by a local player.
    //PARAMS
    //  localGamerIndex     - Gamer who is unadvertising sessions.
    //RETURNS
    //  False if failed to start request.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    static bool UnadvertiseAll(const int localGamerIndex,
							   netStatus* status);
	
	//PURPOSE
	//When true, we advertise the session id as an attribute.
	static void EnableSessionIdAttribute(const bool enable);

	//PURPOSE
	//When true, we periodically iterate through our list of advertised matches and
	//check whether we're hosting a session with the session info that we're advertising.
	//If we're not hosting a session with that session info, we unadvertise it.
	static void EnableMatchReaper(const bool enable);
	static void SetMatchReaperIntervals(const unsigned checkIntervalMs, const unsigned retryIntervalMs);

private:
	static void OnRosEvent(const rlRosEvent& evt);

    //Don't allow creating instances. Users only call static methods.
    rlScMatchmaking();

	//Registered with rlRos to listen for events.
	static rlRos::Delegate m_RosDelegate;

	static bool sm_Initialized;
};

} //namespace rage

#endif  //RLINE_RLSCMATCHMAKING_H
