// 
// rline/rlscmatchmakingtasks.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCMATCHMAKINGTASKS_H
#define RLINE_RLSCMATCHMAKINGTASKS_H

#include "rlscmatchmaking.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/rlmatchingattributes.h"
#include "rline/rlsessioninfo.h"

namespace rage
{

class parTree;
class parTreeNode;

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingAdvertiseTask
///////////////////////////////////////////////////////////////////////////////
class rlScMatchmakingAdvertiseTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_scmatchmaking);
    RL_TASK_DECL(rlScMatchmakingAdvertiseTask);

    rlScMatchmakingAdvertiseTask() {}
    virtual ~rlScMatchmakingAdvertiseTask() {}

	bool Configure(const int localGamerIndex,
				   const unsigned numSlots,
				   const unsigned availableSlots,
				   const rlMatchingAttributes& attrs,
				   const u64 sessionId,
				   const rlSessionInfo& sessionInfo,
				   rlScMatchmakingMatchId* matchId);

protected:
    virtual const char* GetServiceMethod() const { return "matchmaking.asmx/Advertise"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
	int m_LocalGamerIndex;
	unsigned m_NumSlots;
	unsigned m_AvailableSlots;
	rlMatchingAttributes m_Attrs;
	rlSessionInfo m_SessionInfo;
	rlScMatchmakingMatchId* m_MatchId;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingUpdateTask
///////////////////////////////////////////////////////////////////////////////
class rlScMatchmakingUpdateTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_scmatchmaking);
    RL_TASK_DECL(rlScMatchmakingUpdateTask);

    rlScMatchmakingUpdateTask() {}
    virtual ~rlScMatchmakingUpdateTask() {}

	bool Configure(const int localGamerIndex,
				   const rlScMatchmakingMatchId& matchId,
				   const unsigned numSlots,
				   const unsigned availableSlots,
				   const rlSessionInfo& sessionInfo,
				   const rlMatchingAttributes& attrs);

protected:
    virtual const char* GetServiceMethod() const { return "matchmaking.asmx/Update"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingFindTask
///////////////////////////////////////////////////////////////////////////////
class rlScMatchmakingFindTask : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_scmatchmaking);
	RL_TASK_DECL(rlScMatchmakingFindTask);

	rlScMatchmakingFindTask() {}
	virtual ~rlScMatchmakingFindTask() {}

	bool Configure(const int localGamerIndex,
				   const unsigned availableSlots,
				   const rlMatchingFilter& filter,
				   const unsigned maxResults,
				   rlSessionInfo* results,
				   unsigned* numResults);

protected:
	virtual const char* GetServiceMethod() const { return "matchmaking.asmx/Find"; }
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
	bool ReadSession(const parTreeNode* node, rlSessionInfo* s);

	unsigned m_MaxResults;
	rlSessionInfo* m_Results;
	unsigned* m_NumResults;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingUnadvertiseTask
///////////////////////////////////////////////////////////////////////////////
class rlScMatchmakingUnadvertiseTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_scmatchmaking);
    RL_TASK_DECL(rlScMatchmakingUnadvertiseTask);

    rlScMatchmakingUnadvertiseTask() {}
    virtual ~rlScMatchmakingUnadvertiseTask() {}

	bool Configure(const int localGamerIndex,
				   const rlScMatchmakingMatchId& matchId);

	virtual ShutdownBehaviour GetShutdownBehaviour() { return EXECUTE_ON_SHUTDOWN; }

protected:
    virtual const char* GetServiceMethod() const { return "matchmaking.asmx/Unadvertise"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
	rlScMatchmakingMatchId m_MatchId;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingUnadvertiseAllTask
///////////////////////////////////////////////////////////////////////////////
class rlScMatchmakingUnadvertiseAllTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_scmatchmaking);
    RL_TASK_DECL(rlScMatchmakingUnadvertiseAllTask);

    rlScMatchmakingUnadvertiseAllTask() {}
    virtual ~rlScMatchmakingUnadvertiseAllTask() {}

	bool Configure(const int localGamerIndex);

	virtual bool IsCancelable() const { return false; }
	virtual ShutdownBehaviour GetShutdownBehaviour() { return EXECUTE_ON_SHUTDOWN; }

protected:
    virtual const char* GetServiceMethod() const { return "matchmaking.asmx/UnadvertiseAll"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

} //namespace rage

#endif  //RLINE_RLSCMATCHMAKINGTASKS_H
