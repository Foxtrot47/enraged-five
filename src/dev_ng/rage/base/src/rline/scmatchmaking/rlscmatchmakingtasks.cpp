// 
// rline/rlscmatchmakingtask.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscmatchmakingtasks.h"
#include "rlscmatchmanager.h"
#include "data/base64.h"
#include "data/rson.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/device.h"
#include "parser/manager.h"
#include "rline/ros/rlros.h"
#include "rline/rlsessioninfo.h"
#include "system/nelem.h"

namespace rage
{

#undef __rage_channel
#define __rage_channel rline_scmatchmaking

static const unsigned MAX_NUM_SC_ATTRIBUTES = 16;

// example matchmaking attributes:
// {"GAME_MODE":2,"GAME_TYPE":1,"GAMES_PER_MATCH":0,"MATCH_POINT_LIMIT":7,"Data":"JWQ3ZTg0NjY4LTBhZjktNDlhNS04ZjkyLWE3ZDJjOGYyMDRmMwAOU2FtcJU8CD0pRSciZ70BCgIUOQ=="}
static const unsigned MAX_SC_JSON_FILTER_PARAMS_SIZE = MAX_NUM_SC_ATTRIBUTES *
													  (RL_MAX_MATCHING_FILTER_NAME_LENGTH
													  + 2 /*quotes*/
													  + 1 /*colon*/
													  + 11 /*(for max negative 32-bit int as string)*/	// TODO: NS - change this now that we have server support for 64-bit attributes
													  + 1 /*comma*/)
													  + 2 /*curly braces*/
													  + 256 /*slush just in case our estimate is off*/;

// max size of the 'Data' element allowed to be advertised per-session on the SC server.
static const unsigned MAX_SC_MATCHMAKING_DATA_SIZE = 1024;
static const unsigned MAX_SC_JSON_ATTRIBUTES_SIZE = MAX_SC_JSON_FILTER_PARAMS_SIZE + MAX_SC_MATCHMAKING_DATA_SIZE + 32 /*"Data":"" + slush*/;

CompileTimeAssert(rlSessionInfo::TO_STRING_BUFFER_SIZE < MAX_SC_MATCHMAKING_DATA_SIZE);
CompileTimeAssert(MAX_NUM_SC_ATTRIBUTES >= (RL_MAX_MATCHING_ATTRS + 2)); // + 2 for game mode and game type

#define ERROR_CASE(code, codeEx, resCode) \
    if(result.IsError(code, codeEx)) \
    { \
        resultCode = resCode; \
        return; \
    }

static bool PopulateAttrsJson(RsonWriter* rw, const rlSessionInfo& sessionInfo, const u64 sessionId, bool isUpdate, const rlMatchingAttributes& attrs)
{
	rtry
	{
		char buf[rlSessionInfo::TO_STRING_BUFFER_SIZE];

		rlAssert(attrs.GetCount() <= MAX_NUM_SC_ATTRIBUTES);

		rverify(rw->Begin(NULL, NULL), catchall, );

		rverify(rw->WriteInt("GAME_MODE", static_cast<int>(attrs.GetGameModeAttribute())), catchall, );
		
		if(!isUpdate)
		{
			if(rlScMatchManager::IsSessionIdAttributeEnabled())
			{
				rverify(rw->WriteInt64("SESSION_ID", sessionId), catchall, );
			}
		}

		for(int i = 0; i < (int) attrs.GetCount(); ++i)
		{
			const u32* val = attrs.GetValueByIndex(i);
			rverify(val, catchall, );

			const char* attributeName = attrs.GetAttrName(i);

#if !__FINAL
			// TODO: NS - workaround for SC matchmaking breaking samplesession
			if((stricmp(attributeName, "MATCH_POINT_LIMIT") == 0)
				|| (stricmp(attributeName, "GAMES_PER_MATCH") == 0))
			{
				break;
			}
#endif

			// SC wants signed ints
			rverify(rw->WriteInt(attributeName, (int)*val), catchall, );
		}
		rverify(sessionInfo.ToString(buf), catchall, );
		rverify(rw->WriteString("Data", buf), catchall, );
		rverify(rw->End(), catchall, );

		return true;
	}
	rcatchall
	{

	}

	return false;
}

static bool PopulateFiltersJson(RsonWriter* rw, const rlMatchingFilter& filter)
{
	rtry
	{
		rverify(rw->Begin(NULL, NULL), catchall, );
		rverify(rw->WriteInt("GAME_MODE", static_cast<int>(filter.GetGameModeAttribute())), catchall, );
		
		for(int i = 0; i < (int) filter.GetConditionCount(); ++i)
		{
			const u32* filterVal = filter.GetValue(i);

			if(!filterVal)
			{
				// this is a wildcard
				continue;
			}

			const char* attributeName = filter.GetSessionAttrFieldNameForCondition(i);
			rverify(attributeName, catchall, );
			rverify(attributeName[0] != '\0', catchall, );

#if !__FINAL
			// TODO: NS - workaround for SC matchmaking breaking samplesession
			if((stricmp(attributeName, "MATCH_POINT_LIMIT") == 0)
				|| (stricmp(attributeName, "GAMES_PER_MATCH") == 0))
			{
				break;
			}
#endif

			// SC wants signed ints
			rverify(rw->WriteInt(attributeName, (int)*filterVal), catchall, );
		}
		rverify(rw->End(), catchall, );

		return true;
	}
	rcatchall
	{

	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingAdvertiseTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScMatchmakingAdvertiseTask::Configure(const int localGamerIndex,
										const unsigned numSlots,
										const unsigned availableSlots,
										const rlMatchingAttributes& attrs,
										const u64 sessionId,
										const rlSessionInfo& origSessionInfo,
										rlScMatchmakingMatchId* matchId)
{
    rtry
    {
		// the session info advertised to matchmaking never has the player's real address
		rlSessionInfo sessionInfo = origSessionInfo;
		sessionInfo.ClearNonAdvertisableData();

		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));
		rverify(sessionInfo.IsValid(), catchall, );
		rverify(matchId, catchall, );
#if RSG_PC
		rlAssert(sessionInfo.GetSessionId() != 0);
#endif

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddUnsParameter("numSlots", numSlots), catchall, );
        rverify(AddUnsParameter("availableSlots", availableSlots), catchall, );

		char buf[MAX_SC_JSON_ATTRIBUTES_SIZE] = {0};
		RsonWriter rw;
		rw.Init(buf, RSON_FORMAT_JSON);
		rverify(PopulateAttrsJson(&rw, sessionInfo, sessionId, false, attrs), catchall, rlTaskError("Could not populate json matchmaking attributes"));
		rverify(AddStringParameter("attrsJson", rw.ToString(), true), catchall, );

		m_LocalGamerIndex = localGamerIndex;
		m_NumSlots = numSlots;
		m_AvailableSlots = availableSlots;
		m_Attrs = attrs;
		m_SessionInfo = sessionInfo;
		m_MatchId = matchId;
		m_MatchId->Clear();

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScMatchmakingAdvertiseTask::ProcessSuccess(const rlRosResult& /*result*/, 
											 const parTreeNode* node, 
											 int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
		const parTreeNode* result = node->FindChildWithName("MatchId");
        rverify(result, catchall, );
		const char* id = rlHttpTaskHelper::ReadString(result, NULL, NULL);
		rverify(id, catchall, );

		rlScMatchmakingMatchId matchId;
		rverify(matchId.FromString(id), catchall, );
		rverify(matchId.IsValid(), catchall, );
		
		*m_MatchId = matchId;

		rlDebug("Tracking :: Advertise - MatchID: %s, Token: 0x%016" I64FMT "x", m_MatchId->ToString(), m_SessionInfo.GetToken().GetValue());

		rlScMatch* scMatch = rlScMatchManager::AllocateMatch();
		if(scMatch)
		{
			scMatch->Reset(m_LocalGamerIndex, matchId, m_NumSlots, m_AvailableSlots, m_SessionInfo, m_Attrs);
			rlScMatchManager::AddMatch(scMatch);
		}

        return true;
    }
    rcatchall
    {
        resultCode = -1;
        return false;
    }
}

void
rlScMatchmakingAdvertiseTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);

    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingUpdateTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScMatchmakingUpdateTask::Configure(const int localGamerIndex,
									 const rlScMatchmakingMatchId& matchId,
									 const unsigned numSlots,
									 const unsigned availableSlots,
									 const rlSessionInfo& origSessionInfo,
									 const rlMatchingAttributes& attrs)
{
    rtry
    {
		// the session info advertised to matchmaking never has the player's real address
		rlSessionInfo sessionInfo = origSessionInfo;
		sessionInfo.ClearNonAdvertisableData();

		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(matchId.IsValid(), catchall, );
#if RSG_PC
		rlAssert(sessionInfo.GetSessionId() != 0);
#endif

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("matchId", matchId.ToString(), true), catchall, );
        rverify(AddUnsParameter("numSlots", numSlots), catchall, );
        rverify(AddUnsParameter("availableSlots", availableSlots), catchall, );

		char buf[MAX_SC_JSON_ATTRIBUTES_SIZE] = {0};
		RsonWriter rw;
		rw.Init(buf, RSON_FORMAT_JSON);
		rverify(PopulateAttrsJson(&rw, sessionInfo, 0, true, attrs), catchall, rlTaskError("Could not populate json matchmaking attributes"));
		rverify(AddStringParameter("attrsJson", rw.ToString(), true), catchall, );

		rlScMatch* scMatch = rlScMatchManager::GetMatchById(matchId);
		if(scMatch)
		{
			scMatch->Reset(localGamerIndex, matchId, numSlots, availableSlots, sessionInfo, attrs);
		}

		return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScMatchmakingUpdateTask::ProcessSuccess(const rlRosResult& /*result*/, 
											 const parTreeNode* /*node*/, 
											 int& resultCode)
{
    resultCode = 0;
	return true;
}

void
rlScMatchmakingUpdateTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);

    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingFindTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScMatchmakingFindTask::Configure(const int localGamerIndex,
								   const unsigned availableSlots,
								   const rlMatchingFilter& filter,
								   const unsigned maxResults,
								   rlSessionInfo* results,
								   unsigned* numResults)
{
    rtry
    {
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(results, catchall, );
		rverify(maxResults > 0, catchall, );

        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddUnsParameter("availableSlots", availableSlots), catchall, );
		rverify(AddStringParameter("filterName", filter.GetName(), true), catchall, );

		char buf[MAX_SC_JSON_FILTER_PARAMS_SIZE] = {0};
		RsonWriter rw;
		rw.Init(buf, RSON_FORMAT_JSON);
		rverify(PopulateFiltersJson(&rw, filter), catchall, rlTaskError("Could not populate json matchmaking attributes"));
		rverify(AddStringParameter("filterParamsJson", rw.ToString(), true), catchall, );

		rverify(AddUnsParameter("maxResults", maxResults), catchall, );

		m_MaxResults = maxResults;
		m_Results = results;
		m_NumResults = numResults;

		if(m_NumResults)
		{
			*m_NumResults = 0;
		}

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlScMatchmakingFindTask::ReadSession(const parTreeNode* node, rlSessionInfo* sessionInfo)
{
	/*
	<R MatchId="17518965-459e-406a-8e28-241022f01e4c" Owner="123">
		<Data>session info blob</Data>
	</R>
	*/

	rtry
	{
		const char* sessionInfoBase64 = rlHttpTaskHelper::ReadString(node, "Data", NULL);
		rverify(sessionInfoBase64 && (sessionInfoBase64[0] != '\0'), catchall, );
		rverify(sessionInfo->FromString(sessionInfoBase64), catchall, );
		rverify(sessionInfo->IsValid(), catchall, );
#if RSG_PC
		rlAssert(sessionInfo->GetSessionId() != 0);
#endif

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
rlScMatchmakingFindTask::ProcessSuccess(const rlRosResult& /*result*/, 
										const parTreeNode* node, 
										int& resultCode)
{
	/*
		<Status>1</Status>
		<Results Count="2">
			<R MatchId="17518965-459e-406a-8e28-241022f01e4c" Owner="123">
				<Data>session info blob</Data>
			</R>
			<R MatchId="07fa31ed-092d-41c8-91b0-f79ce7bba69f" Owner="123">
				<Data>session info blob</Data>
			</R>
		</Results>
	*/

    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* result = node->FindChildWithName("Results");
        rverify(result, catchall, );

        unsigned numResults = 0;
        parTreeNode::ChildNodeIterator iter = result->BeginChildren();
        while(iter != result->EndChildren())
        {
            parTreeNode* childNode = *iter;
            rlAssert(childNode);

            if(!stricmp(childNode->GetElement().GetName(), "R"))
            {
				rlSessionInfo session;

                if(ReadSession(childNode, &session))
				{
					if(session.GetHostPeerAddress().IsLocal() == false)
					{
						m_Results[numResults] = session;

						++numResults;
						if(numResults >= m_MaxResults)
						{
							break;
						}
					}
					else
					{
						rlTaskDebug3("Discarding local session");
					}
				}
				else
				{
					rlTaskDebug3("Discarding invalid session");
				}
            }

            ++iter;
        }

		if(m_NumResults)
		{
			*m_NumResults = numResults;
		}

        return true;
    }
    rcatchall
    {
        resultCode = -1;
        return false;
    }
}

void
rlScMatchmakingFindTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);

    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingUnadvertiseTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScMatchmakingUnadvertiseTask::Configure(const int localGamerIndex,
										  const rlScMatchmakingMatchId& matchId)
{
    rtry
    {
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));
		rverify(matchId.IsValid(), catchall, rlTaskError("Invalid matchId"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("matchId", matchId.ToString(), true), catchall, );

		m_MatchId = matchId;

		rlScMatch* scMatch = rlScMatchManager::GetMatchById(m_MatchId);
		if(scMatch)
		{
			scMatch->Unadvertising();
		}

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScMatchmakingUnadvertiseTask::ProcessSuccess(const rlRosResult& /*result*/, 
											 const parTreeNode* /*node*/, 
											 int& resultCode)
{
    resultCode = 0;
	
	rlScMatch* scMatch = rlScMatchManager::GetMatchById(m_MatchId);
	if(scMatch)
	{
		rlScMatchManager::DeleteMatch(scMatch);
	}

	return true;
}

void
rlScMatchmakingUnadvertiseTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);

	// for now we assume any SCS error from the unadvertise service means the match doesn't exist, so we shouldn't keep trying to unadvertise it
	rlScMatch* scMatch = rlScMatchManager::GetMatchById(m_MatchId);
	if(scMatch)
	{
		rlScMatchManager::DeleteMatch(scMatch);
	}

    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingUnadvertiseAllTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScMatchmakingUnadvertiseAllTask::Configure(const int localGamerIndex)
{
    rtry
    {
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

		// flag all matches as being unadvertised
		rlScMatchManager::UnadvertiseAllMatches();

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScMatchmakingUnadvertiseAllTask::ProcessSuccess(const rlRosResult& /*result*/, 
												  const parTreeNode* /*node*/, 
												  int& resultCode)
{
    resultCode = 0;

	rlScMatchManager::DeleteAllMatches();

	return true;
}

void
rlScMatchmakingUnadvertiseAllTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);

	rlScMatchManager::DeleteAllMatches();

    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

}; //namespace rage
