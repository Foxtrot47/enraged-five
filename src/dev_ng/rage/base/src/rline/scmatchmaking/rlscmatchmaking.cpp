// 
// rline/rlscmatchmaking.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscmatchmaking.h"
#include "rlscmatchmakingtasks.h"
#include "rlscmatchmanager.h"
#include "atl/bitset.h"
#include "diag/seh.h"
#include "net/status.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroscommon.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "string/string.h"
#include "system/timer.h"

#include <stdio.h>

namespace rage
{

//Registered with rlRos to listen for events.
rlRos::Delegate rlScMatchmaking::m_RosDelegate;

bool rlScMatchmaking::sm_Initialized = false;

RAGE_DEFINE_SUBCHANNEL(rline, scmatchmaking);
#undef __rage_channel
#define __rage_channel rline_scmatchmaking

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingGuid
///////////////////////////////////////////////////////////////////////////////
struct rlScMatchmakingGuid
{
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	unsigned char  Data4[8];
};

///////////////////////////////////////////////////////////////////////////////
//  rlScMatchmakingMatchId
///////////////////////////////////////////////////////////////////////////////
rlScMatchmakingMatchId::rlScMatchmakingMatchId()
{
	Clear();
}

void rlScMatchmakingMatchId::Clear()
{
	sysMemSet(m_GuidString, 0, sizeof(m_GuidString));
	sysMemSet(m_Guid, 0, sizeof(m_Guid));
}

void 
rlScMatchmakingMatchId::SetString()
{
	rlScMatchmakingGuid guid;
	sysMemCpy(&guid, m_Guid, sizeof(guid));

	formatf(m_GuidString,
			"%08x-%04hx-%04hx-%02x%02x-%02x%02x%02x%02x%02x%02x",
			guid.Data1, guid.Data2, guid.Data3, guid.Data4[0],
			guid.Data4[1], guid.Data4[2], guid.Data4[3], guid.Data4[4],
			guid.Data4[5], guid.Data4[6], guid.Data4[7]);
}

bool
rlScMatchmakingMatchId::Import(const void* buf,
							   const unsigned sizeofBuf,
							   unsigned* size)
{
 	datBitBuffer bb;
 	bb.SetReadOnlyBytes(buf, sizeofBuf);
 
	bool success = bb.ReadBytes(m_Guid, sizeof(m_Guid));

	SetString();

 	if(size){*size = success ? bb.GetNumBytesRead() : 0;}
 
 	return success;
}

bool 
rlScMatchmakingMatchId::operator==(const rlScMatchmakingMatchId& that) const
{
	for(unsigned i = 0; i < sizeof(m_Guid); ++i)
	{
		if(m_Guid[i] != that.m_Guid[i])
		{
			return false;
		}
	}

	return true;
}

bool 
rlScMatchmakingMatchId::Export(void* buf,
							   const unsigned sizeofBuf,
							   unsigned* size) const
{
	// if you change the exported size, make sure
	// to change MAX_EXPORTED_SIZE_IN_BYTES

	datBitBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	bool success = bb.WriteBytes(m_Guid, sizeof(m_Guid));

	rlAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool 
rlScMatchmakingMatchId::FromString(const char* s)
{
	bool success = false;

	rtry
	{
		rlScMatchmakingGuid guid;

		// Visual Studio doesn't support the %hhx length specifier for chars
		// so read in as unsigned shorts, then copy to unsigned chars
		unsigned short data[8] = {0};
		rverify(sscanf(s, "%08lx-%04hx-%04hx-%02hx%02hx-%02hx%02hx%02hx%02hx%02hx%02hx", 
						 &guid.Data1, &guid.Data2, &guid.Data3, &data[0],
						 &data[1], &data[2], &data[3], &data[4],
						 &data[5], &data[6], &data[7]) == 11, catchall, );
				
		for(unsigned i = 0; i < 8; ++i)
		{
			guid.Data4[i] = (unsigned char)data[i];
		}

		sysMemCpy(m_Guid, &guid, sizeof(m_Guid));
		safecpy(m_GuidString, s);
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

const char* 
rlScMatchmakingMatchId::ToString() const
{
	rlAssert(IsValid() && m_GuidString[0] != '\0');
	return m_GuidString;
}

bool rlScMatchmakingMatchId::IsValid() const
{
	for(unsigned i = 0; i < sizeof(m_Guid); ++i)
	{
		if(m_Guid[i] != 0)
		{
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////
// rlScMatchmaking
////////////////////////////////////////////////////////////////////////i////////
bool 
rlScMatchmaking::InitClass()
{
	rtry
	{
		rverify(!sm_Initialized, catchall, );

		rverify(rlScMatchManager::InitClass(), catchall, );

		m_RosDelegate.Bind(&rlScMatchmaking::OnRosEvent);
		rlRos::AddDelegate(&m_RosDelegate);

		sm_Initialized = true;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void 
rlScMatchmaking::ShutdownClass()
{
    if(sm_Initialized)
    {
		rlScMatchManager::ShutdownClass();

		rlRos::RemoveDelegate(&m_RosDelegate);

		sm_Initialized = false;
	}
}

void
rlScMatchmaking::UpdateClass()
{
	if(sm_Initialized)
	{
		rlScMatchManager::UpdateClass();
	}
}

void 
rlScMatchmaking::EnableSessionIdAttribute(const bool enable)
{
	rlScMatchManager::EnableSessionIdAttribute(enable);
}

void 
rlScMatchmaking::EnableMatchReaper(const bool enable)
{
	rlScMatchManager::EnableMatchReaper(enable);
}

void 
rlScMatchmaking::SetMatchReaperIntervals(const unsigned checkIntervalMs, const unsigned retryIntervalMs)
{
	rlScMatchManager::SetReaperIntervals(checkIntervalMs, retryIntervalMs);
}

void
rlScMatchmaking::OnRosEvent(const rlRosEvent& evt)
{
	if(RLROS_EVENT_ONLINE_STATUS_CHANGED == evt.GetId())
	{
		const rlRosEventOnlineStatusChanged& osc =
			static_cast<const rlRosEventOnlineStatusChanged&>(evt);

		UnadvertiseAll(osc.m_LocalGamerIndex, NULL);
	}
}

void
rlScMatchmaking::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//Convenience macro to create and queue Social Club tasks
#define RLSCMATCHMAKING_CREATETASK(T, status, ...) 											\
    bool success = false;																	\
    rlTaskBase* task = NULL;                                                                \
    rtry																					\
    {																						\
        rcheck(rlRos::IsOnline(localGamerIndex), catchall, rlDebug("Not signed into ROS")); \
        if (status)                                                                         \
        {                                                                                   \
            T* nonfafTask = rlGetTaskManager()->CreateTask<T>();		                    \
            rverify(nonfafTask,catchall,);													\
            task = nonfafTask;                                                              \
            rverify(rlTaskBase::Configure(nonfafTask, __VA_ARGS__, status),					\
                    catchall, rlError("Failed to configure task"));                         \
        }                                                                                   \
        else                                                                                \
        {                                                                                   \
            rlFireAndForgetTask<T>* fafTask = NULL;											\
            rverify(rlGetTaskManager()->CreateTask(&fafTask), catchall, );  				\
            task = fafTask;                                                                 \
            rverify(rlTaskBase::Configure(fafTask, __VA_ARGS__, &fafTask->m_Status),		\
                    catchall, rlError("Failed to configure fire-and-forget task"));         \
        }                                                                                   \
        rverify(rlGetTaskManager()->AppendSerialTask(task), catchall, );         			\
        success = true;	                                                                    \
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
            rlGetTaskManager()->DestroyTask(task);								            \
        }																					\
    }																						\
    return success;

bool 
rlScMatchmaking::Advertise(const int localGamerIndex,
						   const unsigned numSlots,
						   const unsigned availableSlots,
						   const rlMatchingAttributes& attrs,
						   const u64 sessionId,
						   const rlSessionInfo& sessionInfo,
						   rlScMatchmakingMatchId* matchId,
						   netStatus* status)
{
	RLSCMATCHMAKING_CREATETASK(rlScMatchmakingAdvertiseTask,
							   status,
							   localGamerIndex,
							   numSlots,
							   availableSlots,
							   attrs,
							   sessionId,
							   sessionInfo,
							   matchId);
}

bool 
rlScMatchmaking::Update(const int localGamerIndex,
						const rlScMatchmakingMatchId& matchId,
						const unsigned numSlots,
						const unsigned availableSlots,
						const rlSessionInfo& sessionInfo,
						const rlMatchingAttributes& attrs,
						netStatus* status)
{
	rlDebug("Tracking :: Update - MatchId: %s, Token: 0x%016" I64FMT "x", matchId.IsValid() ? matchId.ToString() : "INVALID", sessionInfo.GetToken().GetValue());
    RLSCMATCHMAKING_CREATETASK(rlScMatchmakingUpdateTask,
							   status,
							   localGamerIndex,
							   matchId,
							   numSlots,
							   availableSlots,
							   sessionInfo,
							   attrs);
}

bool 
rlScMatchmaking::Find(const int localGamerIndex,
					  const unsigned availableSlots,
					  const rlMatchingFilter& filter,
					  const unsigned maxResults,
					  rlSessionInfo* results,
					  unsigned* numResults,
					  netStatus* status)
{
	RLSCMATCHMAKING_CREATETASK(rlScMatchmakingFindTask,
							   status,
							   localGamerIndex,
							   availableSlots,
							   filter,
							   maxResults,
							   results,
							   numResults);
}

bool 
rlScMatchmaking::Unadvertise(const int localGamerIndex,
							 const rlScMatchmakingMatchId& matchId,
							 netStatus* status)
{
	rlDebug("Tracking :: Unadvertise - MatchId: %s", matchId.IsValid() ? matchId.ToString() : "INVALID");
	RLSCMATCHMAKING_CREATETASK(rlScMatchmakingUnadvertiseTask,
							   status,
							   localGamerIndex,
							   matchId);
}

bool 
rlScMatchmaking::UnadvertiseAll(const int localGamerIndex,
								netStatus* status)
{
	rlDebug("Tracking :: UnadvertiseAll");
	RLSCMATCHMAKING_CREATETASK(rlScMatchmakingUnadvertiseAllTask,
							   status,
							   localGamerIndex);
}

}   //namespace rage
