// 
// rline/rlscmatchmanager.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCMATCHMANAGER_H
#define RLINE_RLSCMATCHMANAGER_H

#include "atl/inlist.h"
#include "rline/rl.h"
#include "rline/rldiag.h"
#include "rline/rlmatchingattributes.h"
#include "rline/rlsessioninfo.h"
#include "rline/scmatchmaking/rlscmatchmaking.h"
#include "net/status.h"

namespace rage
{

class rlScMatch
{
public:
	rlScMatch();
	~rlScMatch();
	bool Reset(const int localGamerIndex,
			   const rlScMatchmakingMatchId& matchId,
			   const unsigned numSlots,
			   const unsigned availableSlots,
			   const rlSessionInfo& sessionInfo,
			   const rlMatchingAttributes& attrs);
	void Clear();
	void Update();
	void Shutdown();
	bool IsPending();

	// called when an unadvertise task is configured for this match
	void Unadvertising();
	
	inlist_node<rlScMatch> m_ListLink;

	int m_LocalGamerIndex;
	rlScMatchmakingMatchId m_MatchId;
	unsigned m_NumSlots;
	unsigned m_AvailableSlots;
	rlSessionInfo m_SessionInfo;
	rlMatchingAttributes m_Attrs;

	unsigned m_NextCheckTime;
	unsigned m_UnadvertiseAttempts;
	netStatus m_Status;
};

//PURPOSE
//  Manager for sessions advertised via rlscmatchmaking.
class rlScMatchManager
{
	friend class rlScMatch;
	friend class rlScMatchmaking;
	friend class rlScMatchmakingAdvertiseTask;
	friend class rlScMatchmakingUpdateTask;
	friend class rlScMatchmakingUnadvertiseTask;
	friend class rlScMatchmakingUnadvertiseAllTask;

public:

	enum 
	{
		DEFAULT_CHECK_INTERVAL_MS = 20 * 1000, // how often to check to see if we should unadvertise this match
		DEFAULT_RETRY_INTERVAL_MS = 90 * 1000, // if a previous unadvertise failed, we retry again after this many ms
	};

	static bool IsSessionIdAttributeEnabled();
	static bool HasAnyMatches();

private:
	//Don't allow creating instances.
	rlScMatchManager();

	//PURPOSE
    //  Init/Update/Shutdown. Called by rlscmatchmaking.
    static bool InitClass();
    static void ShutdownClass();
	static void UpdateClass();

	static void EnableSessionIdAttribute(const bool enable);
	static void EnableMatchReaper(const bool enable);
	static void SetReaperIntervals(const unsigned checkIntervalMs, const unsigned retryIntervalMs);

	static rlScMatch* AllocateMatch();
	static bool AddMatch(rlScMatch* match);
	static void DeleteMatch(rlScMatch* match);
	static void DeleteAllMatches();
	static void UnadvertiseAllMatches();
	static rlScMatch* GetMatchById(const rlScMatchmakingMatchId& handle);

	static unsigned ms_CheckIntervalMs;
	static unsigned ms_RetryIntervalMs;
};

} //namespace rage

#endif  //RLINE_RLSCMATCHMANAGER_H

