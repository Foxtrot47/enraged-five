// 
// rline/rlgamerhandle.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rldiag.h"
#include "rlpresence.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "diag/output.h"
#include "net/nethardware.h"
#include "system/nelem.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "system/memory.h"
#include "system/param.h"

#if RSG_DURANGO
#include "rline/rlprivileges.h"
#include "rline/durango/rlxblprivileges_interface.h"
#endif

#include <stdio.h>

#if RSG_PC
#include "rlpc.h"
#include "ros/rlRos.h"
#endif

#if RSG_NP
#include "rlnp.h"
#include "string/unicode.h"
#include <np.h>
#endif

namespace rage
{
bool rlGamerHandle::sm_AllowExtraValidationOnImport = true;
const rlGamerHandle RL_INVALID_GAMER_HANDLE;

#if RLGAMERHANDLE_XBL

//Make sure INVALID_XUID is defined.
#if !defined(INVALID_XUID)
#define INVALID_XUID 0
#endif

#endif //RLGAMERHANDLE_XBL

//////////////////////////////////////////////////////////////////////////
// rlGamerHandle
//////////////////////////////////////////////////////////////////////////

static char s_ServiceAbbrev[rlGamerHandle::NUM_SERVICES][RL_MAX_SERVICE_ABBREV_CHARS + 1] =
{
    { "INV" },    //SERVICE_INVALID
    { "XBL" },    //SERVICE_XBL
    { "NP" },     //SERVICE_NP
    { "SC" },     //SERVICE_SC
};

CompileTimeAssert(COUNTOF(s_ServiceAbbrev) == rlGamerHandle::NUM_SERVICES);

bool rlGamerHandle::s_IsUsingAccountIdAsKey = true;

bool rlGamerHandle::IsUsingAccountIdAsKey()
{
    return s_IsUsingAccountIdAsKey;
}

void rlGamerHandle::SetUsingAccountIdAsKey(const bool useAccountIdAsKey)
{
    if(s_IsUsingAccountIdAsKey != useAccountIdAsKey)
    {
        rlDebug1("SetUsingAccountIdAsKey :: %s", useAccountIdAsKey ? "True" : "False");
        s_IsUsingAccountIdAsKey = useAccountIdAsKey;
    }
}

rlGamerHandle::rlGamerHandle()
{
    this->Clear();
}

void
rlGamerHandle::Clear()
{
    sysMemSet(this, 0, sizeof(*this));

#if RSG_DURANGO
    m_Data.m_XblData.m_Xuid = INVALID_XUID;
#elif RSG_NP 
    m_Data.m_NpData.m_NpEnv = RLNP_ENV_UNKNOWN;
#endif

    m_Service = SERVICE_INVALID;

#if HACK_GTA4
    m_BotIndex = INVALID_BOT_INDEX;
#endif // HACK_GTA4
}

bool
rlGamerHandle::IsValid() const
{
    if((m_Service < SERVICE_FIRST) || (m_Service > SERVICE_LAST))
    {
        return false;
    }

#if HACK_GTA4
	if(m_BotIndex > MAX_BOT_INDEX)
	{
		return false;
	}
#endif // #HACK_GTA4

#if RLGAMERHANDLE_XBL    
    if(GetService() == SERVICE_XBL)
    {
        return (INVALID_XUID != m_Data.m_XblData.m_Xuid);
    }
#endif
#if RLGAMERHANDLE_NP
    if(GetService() == SERVICE_NP)
    {
        if(IsUsingAccountIdAsKey())
        {
            // handles can contain just an account Id (from SCS), just an online Id (some Sony APIs) or both (p2p)
            return (RL_INVALID_NP_ACCOUNT_ID != m_Data.m_NpData.m_AccountId) || (m_Data.m_NpData.m_OnlineIdData[0]);
        }
        else
        {
            // all handles must contain an online Id in this mode
            return (m_Data.m_NpData.m_OnlineIdData[0]);
        }
    }
#endif
#if RLGAMERHANDLE_SC
    if(GetService() == SERVICE_SC)
    {
        return m_Data.m_ScData.m_RockstarId != InvalidRockstarId;
    }
#endif

    return rlVerify(false);
}

bool
rlGamerHandle::IsValidForRos() const
{
    if((m_Service < SERVICE_FIRST) || (m_Service > SERVICE_LAST))
    {
        return false;
    }

#if RLGAMERHANDLE_XBL    
    if(GetService() == SERVICE_XBL)
    {
        return (INVALID_XUID != m_Data.m_XblData.m_Xuid);
    }
#endif
#if RLGAMERHANDLE_NP
    if(GetService() == SERVICE_NP)
    {
        if(IsUsingAccountIdAsKey())
        {
            // we can only pass for Ros with a valid account Id
            return (RL_INVALID_NP_ACCOUNT_ID != m_Data.m_NpData.m_AccountId);
        }
        else
        {
            // all handles must contain an online Id in this mode
            return (m_Data.m_NpData.m_OnlineIdData[0]);
        }
    }
#endif
#if RLGAMERHANDLE_SC
    if(GetService() == SERVICE_SC)
    {
        return m_Data.m_ScData.m_RockstarId != InvalidRockstarId;
    }
#endif

    return rlVerify(false);
}

#if !__NO_OUTPUT

void
rlGamerHandle::OutputValidity() const
{
    if((m_Service < SERVICE_FIRST) || (m_Service > SERVICE_LAST))
    {
        rlDebug1("Invalid m_Service=%d", m_Service);
        return;
    }

#if RLGAMERHANDLE_XBL    
    if(GetService() == SERVICE_XBL)
    {
        if(INVALID_XUID == m_Data.m_XblData.m_Xuid)
        {
            rlDebug1("Invalid m_Data.m_XblData.m_Xuid");
        }
        return;
    }
#endif
#if RLGAMERHANDLE_NP
    if(GetService() == SERVICE_NP)
    {
        if(IsUsingAccountIdAsKey())
        {
            if(RL_INVALID_NP_ACCOUNT_ID == m_Data.m_NpData.m_AccountId)
            {
                rlDebug1("Invalid m_AccountId");
            }
        }
        else
        {
            if(!(m_Data.m_NpData.m_OnlineIdData[0]))
            {
                rlDebug1("Invalid m_OnlineIdData");
            }
        }
    }
#endif
#if RLGAMERHANDLE_SC
    if(GetService() == SERVICE_SC)
    {
        if(m_Data.m_ScData.m_RockstarId == InvalidRockstarId)
        {
            rlDebug1("Invalid m_GsData.InvalidRockstarId");
        }
        return;
    }
#endif

    rlAssert(false);
}
#endif // !__NO_OUTPUT

bool
rlGamerHandle::IsLocal() const
{
    return rlPresence::IsLocal(*this);
}

int
rlGamerHandle::GetLocalIndex() const
{
    return rlPresence::GetLocalIndex(*this);
}

#if HACK_GTA4
void
rlGamerHandle::SetBotIndex(u8 botIndex)
{
    m_BotIndex = botIndex;
}
#endif // #HACK_GTA4

const char*
rlGamerHandle::ToString() const
{
	return rlGamerHandle::ToString(*this);
}

const char*
rlGamerHandle::ToString(char* buf, const int bufSize) const
{
    //******************
    //NOTE:  If the format of gamer handle strings changes be sure
    //to update RL_MAX_HANDLE_CHARS.
    //******************

    rlAssert(buf);
    rlAssert(bufSize >= RL_MAX_GAMER_HANDLE_CHARS);

    sysMemSet(buf, 0, bufSize);

    char tmpBuf[128];
    sysMemSet(tmpBuf, 0, 128);

#if HACK_GTA4
    u8 botIndex = m_BotIndex;
#else
    u8 botIndex = 0;
#endif // HACK_GTA4

    switch(GetService())
    {
#if RLGAMERHANDLE_XBL    
        case SERVICE_XBL:
            //NOTE: We print the string in hex because sscanf appears to have problems
            //      parsing u64 values using %I64FMTu.
            //Capital letters must be used in the hex number because that's what
            //some server side key/value stores expect (e.g. Redis).

            if(0 == botIndex)
            {
                formatf(tmpBuf, sizeof(tmpBuf),
                        "%s %" I64FMT "X",             //DON'T CHANGE X to x.
                        s_ServiceAbbrev[SERVICE_XBL],
                        m_Data.m_XblData.m_Xuid);
            }
            else
            {
                formatf(tmpBuf, sizeof(tmpBuf),
                        "%s %" I64FMT "X %d",             //DON'T CHANGE X to x.
                        s_ServiceAbbrev[SERVICE_XBL],
                        m_Data.m_XblData.m_Xuid,
                        botIndex);
            }
            break;
#endif
#if RLGAMERHANDLE_NP
        case SERVICE_NP:
            rlAssert(m_Data.m_NpData.m_NpEnv <= 9999); //Max range we allow for environment portion of handle

            if(IsUsingAccountIdAsKey())
            {
                if(0 == botIndex)
                {
                    formatf(tmpBuf, sizeof(tmpBuf), "%s %d %" I64FMT "u",
                            s_ServiceAbbrev[SERVICE_NP],
                            m_Data.m_NpData.m_NpEnv,
                            m_Data.m_NpData.m_AccountId);
                }
                else
                {
                    formatf(tmpBuf, sizeof(tmpBuf), "%s %d %" I64FMT "u %u",
                            s_ServiceAbbrev[SERVICE_NP],
                            m_Data.m_NpData.m_NpEnv,
                            m_Data.m_NpData.m_AccountId,
                            botIndex);
                }
            }
            else
            {
                if(0 == botIndex)
                {
                    formatf(tmpBuf, sizeof(tmpBuf), "%s %d \"%s\"",
                            s_ServiceAbbrev[SERVICE_NP],
                            m_Data.m_NpData.m_NpEnv,
                            m_Data.m_NpData.m_OnlineIdData);
                }
                else
                {
                    formatf(tmpBuf, sizeof(tmpBuf), "%s %d \"%s\" %u",
                            s_ServiceAbbrev[SERVICE_NP],
                            m_Data.m_NpData.m_NpEnv,
                            m_Data.m_NpData.m_OnlineIdData,
                            botIndex);
                }
            }
            break;
#endif
#if RLGAMERHANDLE_SC
        case SERVICE_SC:
            if(0 == botIndex)
            {
                formatf(tmpBuf, sizeof(tmpBuf), "%s %" I64FMT "d",
                        s_ServiceAbbrev[SERVICE_SC],
                        m_Data.m_ScData.m_RockstarId);
            }
            else
            {
                formatf(tmpBuf, sizeof(tmpBuf), "%s %" I64FMT "x %d",
                        s_ServiceAbbrev[SERVICE_SC],
                        m_Data.m_ScData.m_RockstarId,
                        botIndex);
            }
            break;
#endif

        default:
#if RSG_PC
            &bufSize;
#endif
            rlAssertf(false, "rlGamerHandle::ToString() - Invalid Service %d for handle, probably the handle is not valid.", GetService());
    }

    if(rlVerify(strlen(tmpBuf) < (size_t)bufSize))
    {
        safecpy(buf, tmpBuf, bufSize);
    }

    return (buf[0] != '\0') ? buf : NULL;
}

bool
rlGamerHandle::FromString(const char* str)
{
    this->Clear();

    rtry
    {
        rverify(str, catchall,);

        char buf[RL_MAX_GAMER_HANDLE_CHARS];
        safecpy(buf, str, sizeof(buf));

        char* dataBuf = strchr(buf, ' ');
        rcheck(dataBuf,
                catchall,
                rlError("Improperly formed gamer handle string:\"%s\"", str));

        *dataBuf = '\0';
        ++dataBuf;

        bool done = false;

        unsigned botIndex = 0;

#if RLGAMERHANDLE_XBL
        if(!done && !strcmp(s_ServiceAbbrev[SERVICE_XBL], buf))
        {
            u64 xuid = 0;

            if(2 != sscanf(dataBuf, "%" I64FMT "X %d", &xuid, &botIndex)) // Don't change X to x)
            {
                rcheck(1 == sscanf(dataBuf, "%" I64FMT "X", &xuid), catchall, rlError("Improperly formed XUID string:\"%s\"", str));
            }

            ResetXbl(xuid);
            done = true;
        }
#endif

#if RLGAMERHANDLE_NP
        if(!done && !strcmp(s_ServiceAbbrev[SERVICE_NP], buf))
        {
            if(IsUsingAccountIdAsKey())
            {      
                rlSceNpAccountId accountId = RL_INVALID_NP_ACCOUNT_ID;
                rlNpEnvironment npEnv = RLNP_ENV_UNKNOWN;

                if(3 != sscanf(dataBuf, "%d %" I64FMT "u %u", &npEnv, &accountId, &botIndex))
                {
                    rcheck(2 == sscanf(dataBuf, "%d %" I64FMT "u", &npEnv, &accountId), catchall, rlError("Improperly formed NpAccountId string:\"%s\"", str));
                }

                ResetNp(npEnv, accountId, nullptr);
                done = true;
            }
            else
            {
                char* token = dataBuf;

                //NP environment        
                char* endOfToken = strchr(token, ' ');
                rcheck(endOfToken,
                        catchall,
                        rlError("Improperly formed NP id string:\"%s\"", str));
                *endOfToken = '\0';

                int npEnv = RLNP_ENV_UNKNOWN;

                rcheck(1 == sscanf(token, "%d", &npEnv),
                        catchall,
                        rlError("Improperly formed NP id string:\"%s\"", str));

                rcheck((rlNpEnvironment)npEnv != RLNP_ENV_UNKNOWN,
                        catchall,
                        rlError("Invalid NP environment:\"%s\"", str));

                //Handle
                token = endOfToken + 1;
                while(*token)
                {
                    if(*token == '\"')
                    {
                        ++token;
                        break;
                    }
                    ++token;
                }
                rcheck(*token,
                        catchall,
                        rlError("Improperly formed NP id string:\"%s\"", str));

                endOfToken = strchr(token, '\\');
                if(!endOfToken)
                {
                    endOfToken = strchr(token, '\"');
                }

                rcheck(endOfToken,
                        catchall,
                        rlError("Improperly formed NP id string:\"%s\"", str));

                *endOfToken = '\0';

                if(isdigit(*token))
                {
                    rcheck(1 == sscanf(token, "%u", &botIndex),
                            catchall,
                            rlError("Improperly formed NP id string:\"%s\"", str));
                }

                rlSceNpOnlineId onlineId;
                onlineId.Clear();
                onlineId.FromString(token);
                this->ResetNp(npEnv, RL_INVALID_NP_ACCOUNT_ID, &onlineId);
                done = true;
            }
        }
#endif // RLGAMERHANDLE_NP

#if RLGAMERHANDLE_SC
        if(!done && !strcmp(s_ServiceAbbrev[SERVICE_SC], buf))
        {
            RockstarId rockstarId = InvalidRockstarId;

            if(2 != sscanf(dataBuf, "%" I64FMT "d %d", &rockstarId, &botIndex))
            {
                rcheck(1 == sscanf(dataBuf, "%" I64FMT "d", &rockstarId),
                        catchall,
                        rlError("Improperly formed RockstarId string:\"%s\"", str));
            }

            ResetSc(rockstarId);
            done = true;
        }
#endif

        if(!done)
        {
            rlError("Error parsing gamerhandle, or service %s is not supported on this platform", buf);
            return false;
        }

#if HACK_GTA4
        m_BotIndex = (u8)botIndex;
#endif // HACK_GTA4

        return true;
    }
    rcatchall
    {
        return false;
    }
}

const char*
rlGamerHandle::ToUserId(char* buf, const int bufSize) const
{
    rlAssert(buf);
    rlAssert(bufSize >= 16);

    sysMemSet(buf, 0, bufSize);

    char tmpBuf[128];
    sysMemSet(tmpBuf, 0, 128);

    switch(GetService())
    {
#if RLGAMERHANDLE_XBL    
        case SERVICE_XBL:
            formatf(tmpBuf, sizeof(tmpBuf), "%" I64FMT "u", m_Data.m_XblData.m_Xuid);
            break;
#endif
#if RLGAMERHANDLE_NP
        case SERVICE_NP:
            if(IsUsingAccountIdAsKey())
            {
                formatf(tmpBuf, sizeof(tmpBuf), "%" I64FMT "u", m_Data.m_NpData.m_AccountId);
            }
            else
            {
                formatf(tmpBuf, sizeof(tmpBuf), "%s", m_Data.m_NpData.m_OnlineIdData);
            }
            break;
#endif
#if RLGAMERHANDLE_SC
        case SERVICE_SC:
            formatf(tmpBuf, sizeof(tmpBuf), "%" I64FMT "d", m_Data.m_ScData.m_RockstarId);
            break;
#endif

        default:
#if RSG_PC
            &bufSize;
#endif
            rlAssertf(false, "Cannot read service from buffer: %s", buf);
    }

    if(rlVerifyf(strlen(tmpBuf) < (size_t)bufSize, "Buffer too small: Required:%u, Size:%d", (unsigned)strlen(tmpBuf), bufSize))
    {
        safecpy(buf, tmpBuf, bufSize);
    }

    return (buf[0] != '\0') ? buf : NULL;
}

bool
rlGamerHandle::FromUserId(const char* userId)
{
    this->Clear();

    rtry
    {
        rverify(userId, catchall,);

#if RLGAMERHANDLE_XBL
        u64 xuid = 0;

        const int result = sscanf(userId, "%" I64FMT "u", &xuid);
        rverify(1 == result,
                catchall,
                rlError("Improperly formed XUID string:\"%s\", return=\"%d\", expected=\"1\", this string needs to be a number.", userId, result));

        rcheck(userId != 0,
                catchall,
                rlError("Improperly formed XUID string:\"%s\". 0 is not a valid XUID.", userId));

        ResetXbl(xuid);
#elif RSG_NP
        const rlNpEnvironment npEnv = g_rlNp.GetEnvironment();
        rverify((rlNpEnvironment)npEnv != RLNP_ENV_UNKNOWN, catchall, rlError("Invalid NP environment"));
        
        if(IsUsingAccountIdAsKey())
        {
            rlSceNpAccountId accountId = RL_INVALID_NP_ACCOUNT_ID;
            const int result = sscanf(userId, "%" I64FMT "u", &accountId);
            rverify(1 == result, catchall, rlError("Improperly formed rlSceNpAccountId string:\"%s\", return=\"%d\", expected=\"1\", this string needs to be a number.", userId, result));
            rcheck(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, rlError("Improperly formed rlSceNpAccountId string:\"%s\". 0 is not a valid rlSceNpAccountId.", userId));
            ResetNp(npEnv, accountId, nullptr);
        }
        else
        {
            rlSceNpOnlineId onlineId;
            onlineId.FromString(userId);
            ResetNp(npEnv, RL_INVALID_NP_ACCOUNT_ID, &onlineId);
        }
#elif RLGAMERHANDLE_SC
        RockstarId rockstarId = InvalidRockstarId;
        rverify(1 == sscanf(userId, "%" I64FMT "d", &rockstarId),
                catchall,
                rlError("Improperly formed Rockstar ID string:\"%s\"", userId));

        ResetSc(rockstarId);
#else
#error Unhandled online service
#endif

        return true;
    }
    rcatchall
    {
    }

    return false;
}

int
rlGamerHandle::GetServiceFromString(const char* ghstr)
{
    const char* eos = strchr(ghstr, ' ');
    if(!eos)
    {
        rlError("Improperly formed gamer handle string:'%s'", ghstr);
        return SERVICE_INVALID;
    }

    const size_t svcLen = eos - ghstr;

    CompileTimeAssert(COUNTOF(s_ServiceAbbrev) == NUM_SERVICES);

    for(int i = 0; i < COUNTOF(s_ServiceAbbrev); ++i)
    {
        const size_t testLen = strlen(s_ServiceAbbrev[i]);

        if(testLen > svcLen)
        {
            continue;
        }

        bool isMatch = true;
        for(int j = 0; j < (int)svcLen && isMatch; ++j)
        {
            isMatch = (s_ServiceAbbrev[i][j] == ghstr[j]);
        }

        if(isMatch)
        {
            return i;
        }
    }

    return SERVICE_INVALID;
}

RockstarId
rlGamerHandle::GetRockstarId(const char* ghstr)
{
    if(!IsSc(ghstr))
    {
        return InvalidRockstarId;
    }

    const char* eos = strchr(ghstr, ' ');
    if(!eos)
    {
        rlError("Improperly formed gamer handle string:'%s'", ghstr);
        return InvalidRockstarId;
    }

    RockstarId rockstarId;

    if(1 != sscanf(eos + 1, "%" I64FMT "d", &rockstarId))
    {
        rlError("Improperly formed RockstarId string:\'%s\'", ghstr);
        return InvalidRockstarId;
    }

    return rockstarId;
}

#if RLGAMERHANDLE_XBL
void
rlGamerHandle::ResetXbl(const u64 xuid)
{
    rlAssert(INVALID_XUID != xuid);
    Clear();

    m_Service = SERVICE_XBL;
    m_Data.m_XblData.m_Xuid = xuid;
}

u64
rlGamerHandle::GetXuid() const
{
    rlAssert(GetService() == SERVICE_XBL);
    return m_Data.m_XblData.m_Xuid;
}
#endif //RLGAMERHANDLE_XBL

#if RLGAMERHANDLE_NP
void
rlGamerHandle::ResetNp(const int npEnv, const rlSceNpAccountId accountId, const rlSceNpOnlineId* onlineId)
{
#if USE_NET_ASSERTS
    if(IsUsingAccountIdAsKey())
    {
        // this is possible when retrieving gamer handles from Sony services on the GTA SDK
    }
    else
    {
        rlAssert(onlineId != nullptr);
    }
#endif

    Clear();
    m_Service = SERVICE_NP;
    m_Data.m_NpData.m_NpEnv = npEnv;
    m_Data.m_NpData.m_AccountId = accountId;

    if(onlineId)
    {
        safecpy(m_Data.m_NpData.m_OnlineIdData, onlineId->data, sizeof(m_Data.m_NpData.m_OnlineIdData));
    }
}

const rlSceNpAccountId
rlGamerHandle::GetNpAccountId() const
{
    rlAssert(GetService() == SERVICE_NP);
    return m_Data.m_NpData.m_AccountId;
}

rlSceNpOnlineId rlGamerHandle::GetNpOnlineId() const
{
    rlAssert(GetService() == SERVICE_NP);

    rlSceNpOnlineId onlineId;// return-value optimization
    onlineId.Clear();
    onlineId.FromString(m_Data.m_NpData.m_OnlineIdData);
    return onlineId;
}
#endif

#if RLGAMERHANDLE_SC
void
rlGamerHandle::ResetSc(const RockstarId profileId)
{
    rlAssert(profileId != 0);
    rlAssert(profileId != -1);

    Clear();

    m_Service = SERVICE_SC;
    m_Data.m_ScData.m_RockstarId = profileId;
}

RockstarId
rlGamerHandle::GetRockstarId() const
{
    rlAssertf(GetService() == SERVICE_SC, "Invalid service[%d] RockstarId[%" I64FMT "u]", GetService(), m_Data.m_ScData.m_RockstarId);
    return m_Data.m_ScData.m_RockstarId;
}
#endif //RLGAMERHANDLE_SC

bool
rlGamerHandle::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    bool success = false;

    rtry
    {
        rcheck(bb.WriteUns(m_Service, 8), catchall,);

        switch(GetService())
        {
#if RLGAMERHANDLE_XBL
            case SERVICE_XBL:
                rcheck(bb.WriteUns(m_Data.m_XblData.m_Xuid, 64), catchall,);
                break;
#endif
#if RLGAMERHANDLE_NP
            case SERVICE_NP:
                rcheck(bb.SerInt(m_Data.m_NpData.m_NpEnv, RLNP_ENV_NUM_BITS), catchall,);

                // note: rlSceNpOnlineId::MAX_DATA_BUF_SIZE is 16 bytes, the buffer is 17 bytes and must be terminated on the other side
                rcheckall(bb.SerBytes(m_Data.m_NpData.m_OnlineIdData, rlSceNpOnlineId::MAX_DATA_BUF_SIZE));
                rcheckall(bb.SerUns(m_Data.m_NpData.m_AccountId, 64));
                break;
#endif
#if RLGAMERHANDLE_SC
            case SERVICE_SC:
                rcheck(bb.WriteInt(m_Data.m_ScData.m_RockstarId, sizeof(RockstarId) << 3), catchall,);
                break;
#endif

            default:
                rcheck(false, catchall, rlError("Invalid service in Export()"));
        }

#if HACK_GTA4
        rcheck(bb.WriteUns(m_BotIndex, sizeof(u8) << 3), catchall,);
#endif // HACK_GTA4

        success = true;
    }
    rcatchall
    {
    }

    if(size) 
    { 
        *size = success ? bb.GetNumBytesWritten() : 0; 
    }

    return success;
}

bool
rlGamerHandle::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    bool success = false;

    Clear();

    rtry
    {
        rcheck(bb.ReadUns(m_Service, 8), catchall,);

        switch(GetService())
        {
#if RLGAMERHANDLE_XBL
            case SERVICE_XBL:
                rcheck(bb.ReadUns(m_Data.m_XblData.m_Xuid, 64), catchall,);
                break;
#endif
#if RLGAMERHANDLE_NP
            case SERVICE_NP:
                rcheck(bb.SerInt(m_Data.m_NpData.m_NpEnv, RLNP_ENV_NUM_BITS), catchall,);

                // note: rlSceNpOnlineId::MAX_DATA_BUF_SIZE is 16 bytes, the buffer is 17 bytes.
                rcheckall(bb.SerBytes(m_Data.m_NpData.m_OnlineIdData, rlSceNpOnlineId::MAX_DATA_BUF_SIZE));

                // terminate the last byte in the online id data.
                m_Data.m_NpData.m_OnlineIdData[rlSceNpOnlineId::MAX_DATA_BUF_SIZE] = '\0';

                rcheckall(bb.SerUns(m_Data.m_NpData.m_AccountId, 64));
                break;
#endif
#if RLGAMERHANDLE_SC
            case SERVICE_SC:
                rcheck(bb.ReadInt(m_Data.m_ScData.m_RockstarId, sizeof(RockstarId) << 3), catchall,);
                break;
#endif

            default:
                rcheck(false, catchall, rlError("Invalid service %u in Import()", GetService()));
        }

#if HACK_GTA4
        rcheck(bb.ReadUns(m_BotIndex, sizeof(u8) << 3), catchall,);
#endif // HACK_GTA4

		// Don't allow importing invalid gamer handles. This helps to prevent exploits that inject
		// invalid account IDs into network messages.
		if(sm_AllowExtraValidationOnImport)
		{
			rverify(IsValid(), catchall, rlError("Invalid gamerhandle in Import()"));
		}

        success = true;
    }
    rcatchall
    {
    }

    if(size) 
    { 
        *size = success ? bb.GetNumBytesRead() : 0; 
    }

    return success;
}

bool 
rlGamerHandle::Equals(const rlGamerHandle& that, const bool allowInvalid) const
{
    if(m_Service != that.m_Service)
    {
        return false;
    }

    if(allowInvalid)
    {
        // if we do allow invalid, bail out early if both handles are invalid with a positive result
        // we check the service for specific comparisons on the unique handle below which doesn't make
        // sense with invalid handles
        if(!IsValid() && !that.IsValid())
        {
            return true;
        }
    }
    else
    {
		// if we don't allow invalid, then check whether either handle is invalid and return false
        // this prevents two invalid handles returning true
		if(!IsValid() || !that.IsValid())
        {
            return false;
        }
    }

#if HACK_GTA4
    if(m_BotIndex != that.m_BotIndex)
    {
        return false;
    }
#endif // HACK_GTA4

#if RLGAMERHANDLE_XBL
    if(GetService() == SERVICE_XBL)
    {
        return m_Data.m_XblData.m_Xuid == that.m_Data.m_XblData.m_Xuid;
    }
#endif
#if RLGAMERHANDLE_NP
    if(GetService() == SERVICE_NP)
    {
        if(m_Data.m_NpData.m_NpEnv != that.m_Data.m_NpData.m_NpEnv)
            return false; 

        if(IsUsingAccountIdAsKey())
        {
            if(m_Data.m_NpData.m_AccountId != RL_INVALID_NP_ACCOUNT_ID &&
               that.m_Data.m_NpData.m_AccountId != RL_INVALID_NP_ACCOUNT_ID)
            {
                if(m_Data.m_NpData.m_AccountId == that.m_Data.m_NpData.m_AccountId)
                {
                    return true;
                }
                else
                {
                    // if both account Ids are valid, don't allow the online Id fallback
                    return false;
                }
            }

            // fallback to online Id (some of our handles sourced from Sony APIs don't have an account Id)
            return m_Data.m_NpData.m_OnlineIdData[0] &&
                that.m_Data.m_NpData.m_OnlineIdData[0] &&
                !strncmp(m_Data.m_NpData.m_OnlineIdData, that.m_Data.m_NpData.m_OnlineIdData, sizeof(m_Data.m_NpData.m_OnlineIdData));
        }
        else
        {
            return m_Data.m_NpData.m_OnlineIdData[0] &&
                that.m_Data.m_NpData.m_OnlineIdData[0] &&
                !strncmp(m_Data.m_NpData.m_OnlineIdData, that.m_Data.m_NpData.m_OnlineIdData, sizeof(m_Data.m_NpData.m_OnlineIdData));
        }
    }
#endif // RLGAMERHANDLE_NP
#if RLGAMERHANDLE_SC
    if(GetService() == SERVICE_SC)
    {
        return m_Data.m_ScData.m_RockstarId == that.m_Data.m_ScData.m_RockstarId;
    }
#endif

    rlAssertf(false, "Unrecognized or invalid service '%d' in Equals", GetService());

    return false;
}

bool
rlGamerHandle::operator==(const rlGamerHandle& that) const
{
    return Equals(that, false);
}

bool
rlGamerHandle::operator!=(const rlGamerHandle& that) const
{
    return !(*this == that);
}

u64 rlGamerHandle::ToUniqueId() const
{
#if RLGAMERHANDLE_XBL
	if(IsXbl())
	{
		return GetXuid();
	}
#endif // RLGAMERHANDLE_XBL
#if RLGAMERHANDLE_NP
	if(IsNp())
	{
		return GetNpAccountId();
	}
#endif // RLGAMERHANDLE_NP
#if RLGAMERHANDLE_SC
	if(IsSc())
	{
		return static_cast<u64>(GetRockstarId());
	}
#endif // RLGAMERHANDLE_SC
	return 0;
}

void rlGamerHandle::FromUniqueId(const int service, const u64 uniqueId)
{
#if RLGAMERHANDLE_XBL
	if(service == SERVICE_XBL)
	{
		ResetXbl(uniqueId);
	}
#endif
#if RLGAMERHANDLE_NP
	if(service == SERVICE_NP)
	{
#if RSG_NP
		ResetNp(g_rlNp.GetEnvironment(), uniqueId, nullptr);
#else
		ResetNp(RLNP_ENV_UNKNOWN, uniqueId, nullptr);
#endif // RSG_NP
	}
#endif // RLGAMERHANDLE_NP
#if RLGAMERHANDLE_SC
	if(service == SERVICE_SC)
	{
		ResetSc(uniqueId);
	}
#endif // #if RLGAMERHANDLE_SC
}

const char* rlGamerHandle::ToString(const rlGamerHandle& hGamer)
{
	static const unsigned MAX_HANDLES = 5;
	static const unsigned MAX_STRING = 256;
	static char szString[MAX_HANDLES][MAX_STRING];
	static unsigned nHandleIndex = 0;

	// get handle
	static char szHandleString[RL_MAX_GAMER_HANDLE_CHARS];
	if (hGamer.IsValid())
		hGamer.ToString(szHandleString, RL_MAX_GAMER_HANDLE_CHARS);
	else
		safecpy(szHandleString, "Invalid");

#if RSG_DURANGO
	snprintf(szString[nHandleIndex], MAX_STRING, "[%s - %" I64FMT "u]", szHandleString, hGamer.IsXbl() && hGamer.IsValid() ? hGamer.GetXuid() : 0);
#else
	formatf(szString[nHandleIndex], "[%s]", szHandleString);
#endif

	// having a rolling buffer allows for multiple calls to this function in the same logging line
	int nHandleIndexUsed = nHandleIndex;
	nHandleIndex++;
	if (nHandleIndex >= MAX_HANDLES)
		nHandleIndex = 0;

	return szString[nHandleIndexUsed];
}

void 
rlGamerHandle::SetAllowExtraValidationOnImport(const bool allowExtraValidationOnImport)
{
	if(sm_AllowExtraValidationOnImport != allowExtraValidationOnImport)
	{
		rlDebug("SetAllowExtraValidationOnImport :: %s", allowExtraValidationOnImport ? "true" : "false");
		sm_AllowExtraValidationOnImport = allowExtraValidationOnImport;
	}
}

}   //namespace rage
