// 
// rline/rltitleid.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLTITLEID_H
#define RLINE_RLTITLEID_H

#include "ros/rlrostitleid.h"

#if RSG_NP
#include "rlnptitleid.h"
#endif

#if RSG_DURANGO
#include "rline/durango/rlxbltitleid_interface.h"
#endif

namespace rage
{

class rlTitleId
{
public:
    rlTitleId(
#if RSG_DURANGO
			  const rlXblTitleId &xblTitleId,
#elif RSG_NP
              const rlNpTitleId &npTitleId,
#endif
              const rlRosTitleId& rosTitleId)
    : m_RosTitleId(rosTitleId)
#if RSG_XBL || RSG_DURANGO
    , m_XblTitleId(xblTitleId)
#elif RSG_NP
    , m_NpTitleId(npTitleId)
#endif
    {
    }

	void Init()
	{
#if RSG_XBOX
		m_XblTitleId.Init();
#endif
	}

    rlRosTitleId m_RosTitleId;

#if RSG_DURANGO
	rlXblTitleId m_XblTitleId;
#elif RSG_NP
    rlNpTitleId m_NpTitleId;
#endif
};

//This is defined in rl.cpp
extern const rlTitleId* g_rlTitleId;

}   //namespace rage

#endif  //RLINE_RLTITLEID_H
