// 
// rline/rlxbl.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rlxbl.h"
#include "rlpresence.h"
#include "rlsession.h"
#include "rlxhttp.h"
#include "diag/seh.h"
#include "string/unicode.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"

#include "rline/facebook/rlfacebookcommon.h"

#if __LIVE

#include "system/xtl.h"
#include <xparty.h>
#include <xsocialpost.h>

#if __FINAL
#pragma comment(lib, "xparty.lib")
#pragma comment(lib, "xsocialpost.lib")
#else
#pragma comment(lib, "xpartyd.lib")
#pragma comment(lib, "xsocialpostd.lib")
#endif

namespace rage
{

#if __DEV && 0
PARAM(rlXblLbResetStartId,  "ID of first LB to reset in Init()");
PARAM(rlXblLbResetNum,      "Number of LBs to reset in Init(), including the start ID");
#endif

RAGE_DEFINE_SUBCHANNEL(rline, xbl)
RAGE_DEFINE_SUBCHANNEL(rline_xbl, tasks)
#undef __rage_channel
#define __rage_channel rline_xbl

//Global instance
rlXbl g_rlXbl;

static
unsigned XMakeCreateFlags(const rlNetworkMode netMode,
                            const rlGameType gameType,
                            const unsigned createFlags,
                            const unsigned presenceFlags,
                            const bool isPrivate,
                            const bool isHost,
                            const bool isOwnerOnline)
{
    unsigned xFlags = 0;

    //Anyone who calls this should make sure that if these are
    //disabled in the create flags then the presence flags has
    //this info, and it's cleared from the create flags.
    rlAssert(!(createFlags & RL_SESSION_CREATE_FLAG_INVITES_DISABLED));
	rlAssert(!(createFlags & RL_SESSION_CREATE_FLAG_JOIN_VIA_PRESENCE_DISABLED));

    if(RL_NETMODE_ONLINE == netMode)
    {
        if(RL_GAMETYPE_RANKED == gameType)
        {
            rlAssert(!(presenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE));
			rlAssert(!(presenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE));

            xFlags |= XSESSION_CREATE_LIVE_MULTIPLAYER_RANKED;
        }
        else
        {
            rlAssert(RL_GAMETYPE_STANDARD == gameType);

            xFlags |= XSESSION_CREATE_LIVE_MULTIPLAYER_STANDARD;
        }

        if(createFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED)
        {
            rlAssert(!(presenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE));
			rlAssert(!(presenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE));

            xFlags &= ~XSESSION_CREATE_USES_PRESENCE;
        }
        else
        {
            if(!(presenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE))
            {
                if(RL_GAMETYPE_RANKED != gameType)
                {
                    xFlags |= XSESSION_CREATE_INVITES_DISABLED;
                }
            }

			if(!(presenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE))
			{
				if(RL_GAMETYPE_RANKED != gameType)
				{
					xFlags |= XSESSION_CREATE_JOIN_VIA_PRESENCE_DISABLED;
				}
			}

            if(createFlags & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED)
            {
                if(RL_GAMETYPE_RANKED != gameType)
                {
                    xFlags |= XSESSION_CREATE_JOIN_IN_PROGRESS_DISABLED;
                }
            }
        }
    }
    else if(RL_NETMODE_LAN == netMode)
    {
        rlAssert(RL_GAMETYPE_STANDARD == gameType);

        xFlags |= XSESSION_CREATE_SYSTEMLINK;
    }
    else
    {
        rlAssert(RL_NETMODE_OFFLINE == netMode);
        rlAssert(RL_GAMETYPE_STANDARD == gameType);

        xFlags |= XSESSION_CREATE_SINGLEPLAYER_WITH_STATS;

        if(!isOwnerOnline)
        {
            xFlags &= ~XSESSION_CREATE_USES_PRESENCE;
            xFlags &= ~XSESSION_CREATE_INVITES_DISABLED;
            xFlags &= ~XSESSION_CREATE_JOIN_VIA_PRESENCE_DISABLED;
            xFlags &= ~XSESSION_CREATE_JOIN_IN_PROGRESS_DISABLED;
            xFlags &= ~XSESSION_CREATE_USES_STATS;
        }
    }

    if(isHost && RL_NETMODE_OFFLINE != netMode)
    {
        xFlags |= XSESSION_CREATE_HOST;
    }

    //If the session is private then don't advertise on matchmaking.
    //Unless the presence flag is also cleared, because for some
    //reason clearing both flags appears to prevent online network
    //communications.
    if(isPrivate && (xFlags & XSESSION_CREATE_USES_PRESENCE))
    {
        xFlags &= ~XSESSION_CREATE_USES_MATCHMAKING;
    }

    return xFlags;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::ReadWriteAttrsTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::ReadWriteAttrsTask::ReadWriteAttrsTask()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_ReadAttrs(NULL)
, m_ReadContexts(NULL)
, m_ReadProperties(NULL)
, m_ReadPropertySizes(NULL)
, m_NumReadContexts(0)
, m_NumReadProperties(0)
{
}

bool
rlXbl::ReadWriteAttrsTask::ConfigureForRead(rlXbl* ctx,
                                            const int localGamerIndex,
                                            rlMatchingAttributes* readAttrs,
                                            netStatus* status)
{
    return rlTaskBase::Configure(this, ctx, localGamerIndex, readAttrs, status);
}

bool
rlXbl::ReadWriteAttrsTask::ConfigureForWrite(rlXbl* ctx,
                                            const int localGamerIndex,
                                            const rlMatchingAttributes& writeAttrs,
                                            netStatus* status)
{
    return rlTaskBase::Configure(this, ctx, localGamerIndex, writeAttrs, status);
}

bool
rlXbl::ReadWriteAttrsTask::Configure(rlXbl* /*ctx*/,
                                    const int localGamerIndex,
                                    rlMatchingAttributes* readAttrs)
{
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
    rlAssert(readAttrs->GetCount() > 0);

    rlAssert(!m_ReadAttrs);
    rlAssert(!m_NumReadContexts);
    rlAssert(!m_NumReadProperties);
    rlAssert(!m_ReadContexts);
    rlAssert(!m_ReadProperties);
    rlAssert(!m_ReadPropertySizes);

    m_LocalGamerIndex = localGamerIndex;
    m_ReadAttrs = readAttrs;
    m_State = STATE_INVALID;

    return true;
}

bool
rlXbl::ReadWriteAttrsTask::Configure(rlXbl* /*ctx*/,
                                    const int localGamerIndex,
                                    const rlMatchingAttributes& writeAttrs)
{
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
    rlAssert(writeAttrs.IsValid());

    rlAssert(!m_ReadAttrs);
    rlAssert(!m_NumReadContexts);
    rlAssert(!m_NumReadProperties);
    rlAssert(!m_ReadContexts);
    rlAssert(!m_ReadProperties);
    rlAssert(!m_ReadPropertySizes);

    m_LocalGamerIndex = localGamerIndex;
    m_WriteAttrs = writeAttrs;
    m_State = STATE_INVALID;

    return true;
}

void
rlXbl::ReadWriteAttrsTask::Start()
{
    rlTaskDebug("%s attributes...", (m_ReadAttrs ? "Reading" : "Writing"));

    this->rlXblTask::Start();

    if(m_ReadAttrs)
    {
        m_ReadAttrs->ClearAllValues();
        m_State = STATE_READ;
    }
    else
    {
        m_State = STATE_WRITE;

#if !__NO_OUTPUT
        rlTaskDebug("Attributes:");
        for(int i = 0; i < (int) m_WriteAttrs.GetCount(); ++i)
        {
            const u32* val = m_WriteAttrs.GetValueByIndex(i);
            if(val)
            {
                rlTaskDebug("  0x%08x:       0x%08x", m_WriteAttrs.GetAttrId(i), *val);
            }
            else
            {
                rlTaskDebug("  0x%08x:       nil", m_WriteAttrs.GetAttrId(i));
            }
        }

        rlTaskDebug("  Game Mode:    0x%08x", m_WriteAttrs.GetGameMode());
        rlTaskDebug("  Session Purpose: 0x%08x", m_WriteAttrs.GetSessionPurpose());
        rlTaskDebug("  Game Type:    %s",
                    m_WriteAttrs.GetGameType() == RL_GAMETYPE_RANKED
                    ? "RANKED" : "PLAYER");
#endif  //__NO_OUTPUT
    }
}

void 
rlXbl::ReadWriteAttrsTask::DoCancel()
{
	this->rlXblTask::DoCancel();
	
	for(int i = 0; i < COUNTOF(m_XovStatus); ++i)
	{
		m_XovStatus[i].Cancel();
	}
}

void
rlXbl::ReadWriteAttrsTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("%s attributes %s",
            (m_ReadAttrs ? "Reading" : "Writing"),
            FinishString(finishType));

    if(FinishSucceeded(finishType) && m_ReadAttrs)
	{
        m_ReadAttrs->ClearAllValues();

        //Game mode
        if(m_MyStatus[0].Succeeded())
        {
            m_ReadAttrs->SetGameMode(m_ReadContexts[0].dwValue);
        }

        //Game type
        if(m_MyStatus[1].Succeeded())
        {
            if(X_CONTEXT_GAME_TYPE_RANKED == m_ReadContexts[1].dwValue)
            {
                m_ReadAttrs->SetGameType(RL_GAMETYPE_RANKED);
            }
            else if(X_CONTEXT_GAME_TYPE_STANDARD == m_ReadContexts[1].dwValue)
            {
                m_ReadAttrs->SetGameType(RL_GAMETYPE_STANDARD);
            }
            else
            {
                rlAssert(false);
            }
        }

        int ctxIdx = 2, propIdx = 0;

        for(int i = 0; i < (int) m_ReadAttrs->GetCount(); ++i)
        {
            if(!m_MyStatus[i + 2].Succeeded())
            {
                //If we failed to read this value then leave it nil.
                continue;
            }

            const unsigned fieldId = m_ReadAttrs->GetAttrId(i);

            if(XUSER_DATA_TYPE_CONTEXT == XPROPERTYTYPEFROMID(fieldId))
            {
                rlAssert(fieldId == m_ReadContexts[ctxIdx].dwContextId);
                m_ReadAttrs->SetValueByIndex(i, m_ReadContexts[ctxIdx].dwValue);
                ++ctxIdx;
            }
            else
            {
                rlAssert(fieldId == m_ReadProperties[propIdx].dwPropertyId);
                m_ReadAttrs->SetValueByIndex(i, m_ReadProperties[propIdx].value.nData);
                ++propIdx;
            }
        }
    }

    if(m_ReadContexts)
    {
        rlDelete(m_ReadContexts, m_NumReadContexts);
    }

    if(m_ReadProperties)
    {
        rlDelete(m_ReadProperties, m_NumReadProperties);
    }

    if(m_ReadPropertySizes)
    {
        rlDelete(m_ReadPropertySizes, m_NumReadProperties);
    }

    m_ReadAttrs = NULL;
    m_NumReadContexts = m_NumReadProperties = 0;
    m_ReadContexts = NULL;
    m_ReadProperties = NULL;
    m_ReadPropertySizes = NULL;

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::ReadWriteAttrsTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		switch(m_State)
		{
		case STATE_READ:
			if(this->ReadAttrs())
			{
				m_State = STATE_PENDING;
			}
			else
			{
				this->Finish(FINISH_FAILED);
			}
			break;

		case STATE_WRITE:
			if(this->WriteAttrs())
			{
				m_State = STATE_PENDING;
			}
			else
			{
				this->Finish(FINISH_FAILED);
			}
			break;

		case STATE_PENDING:
			{
				bool pending = false, failed = false;
				for(int i = 0; i < COUNTOF(m_XovStatus); ++i)
				{
					if(!m_MyStatus[i].Pending())
					{
						continue;
					}

					m_XovStatus[i].Update();

					pending = pending || m_MyStatus[i].Pending();

					//If we failed to read a context/property because it
					//hasn't been set (error code is S_FALSE), then just leave
					//it as nil, but don't fail the operation.
					if(m_MyStatus[i].Failed()
						&& i >= 2   //skip game mode/type
						&& m_ReadAttrs
						&& m_XovStatus[i].GetResult() == S_FALSE)
					{
					}
					else
					{
						failed = failed || m_MyStatus[i].Failed();
					}
				}

				if(!pending)
				{
					this->Finish(failed ? FINISH_FAILED : FINISH_SUCCEEDED);
				}
			}
			break;
		}
	}
}

bool
rlXbl::ReadWriteAttrsTask::ReadAttrs()
{
    bool success = false;

    rlAssert(m_WriteAttrs.GetCount() <= COUNTOF(m_XovStatus) - 2);

    rlAssert(!m_NumReadContexts);
    rlAssert(!m_NumReadProperties);
    rlAssert(!m_ReadContexts);
    rlAssert(!m_ReadProperties);
    rlAssert(!m_ReadPropertySizes);
        
    rtry
    {
        //Count the number of contexts/properties.
        for(int i = 0; i < (int) m_ReadAttrs->GetCount(); ++i)
        {
            const unsigned fieldId = m_ReadAttrs->GetAttrId(i);
            if(XUSER_DATA_TYPE_CONTEXT == XPROPERTYTYPEFROMID(fieldId))
            {
                ++m_NumReadContexts;
            }
            else
            {
                ++m_NumReadProperties;
            }
        }

        //Add 2 contexts for game mode/type
        m_NumReadContexts += 2;

        m_ReadContexts = RL_NEW_ARRAY(rlXbl, XUSER_CONTEXT, m_NumReadContexts);

        rverify(m_ReadContexts,
                catchall,
                rlTaskError("Error allocating XUSER_CONTEXT array"));

        sysMemSet(m_ReadContexts, 0, m_NumReadContexts * sizeof(XUSER_CONTEXT));

        if(m_NumReadProperties > 0)
        {
            m_ReadProperties = RL_NEW_ARRAY(rlXbl, XUSER_PROPERTY, m_NumReadProperties);

            rverify(m_ReadProperties,
                    catchall,
                    rlTaskError("Error allocating XUSER_PROPERTY array"));

            sysMemSet(m_ReadProperties, 0, m_NumReadProperties * sizeof(XUSER_PROPERTY));

            m_ReadPropertySizes = RL_NEW_ARRAY(rlXbl, unsigned, m_NumReadProperties);

            rverify(m_ReadPropertySizes,
                    catchall,
                    rlTaskError("Error allocating property sizes array"));
        }

        rlXovStatus* xovStatus = &m_XovStatus[0];
        netStatus* status = &m_MyStatus[0];

        m_ReadContexts[0].dwContextId = X_CONTEXT_GAME_MODE;
        xovStatus->Begin("XUserGetContext", status);
        DWORD dw = XUserGetContext(m_LocalGamerIndex,
                                    &m_ReadContexts[0],
                                    xovStatus->ToXov());
        ++xovStatus; ++status;

        rverify(ERROR_IO_PENDING == dw,
                catchall,
                rlTaskError("Error:0x%08x calling XUserGetContext for game mode", dw));

        m_ReadContexts[1].dwContextId = X_CONTEXT_GAME_TYPE;
        xovStatus->Begin("XUserGetContext", status);
        dw = XUserGetContext(m_LocalGamerIndex,
                            &m_ReadContexts[1],
                            xovStatus->ToXov());
        ++xovStatus; ++status;

        rverify(ERROR_IO_PENDING == dw,
                catchall,
                rlTaskError("Error:0x%08x calling XUserGetContext for game type", dw));

        unsigned ctxIdx = 2, propIdx = 0;

        for(int i = 0; i < (int) m_ReadAttrs->GetCount(); ++i, ++xovStatus, ++status)
        {
            const unsigned fieldId = m_ReadAttrs->GetAttrId(i);

            if(XUSER_DATA_TYPE_CONTEXT == XPROPERTYTYPEFROMID(fieldId))
            {
                m_ReadContexts[ctxIdx].dwContextId = fieldId;
                xovStatus->Begin("XUserGetContext", status);
                dw = XUserGetContext(m_LocalGamerIndex,
                                    &m_ReadContexts[ctxIdx],
                                    xovStatus->ToXov());

                ++ctxIdx;

                rverify(ERROR_IO_PENDING == dw,
                        catchall,
                        rlTaskError("Error:0x%08x calling XUserGetContext", dw));
            }
            else
            {
                CompileTimeAssert(sizeof(DWORD) == sizeof(m_ReadPropertySizes[0]));

                m_ReadProperties[propIdx].dwPropertyId = fieldId;
                m_ReadPropertySizes[propIdx] = sizeof(XUSER_PROPERTY);
                xovStatus->Begin("XUserGetProperty", status);
                dw = XUserGetProperty(m_LocalGamerIndex,
                                    (DWORD*) &m_ReadPropertySizes[propIdx],
                                    &m_ReadProperties[propIdx],
                                    xovStatus->ToXov());

                ++propIdx;

                rverify(ERROR_IO_PENDING == dw,
                        catchall,
                        rlTaskError("Error:0x%08x calling XUserGetProperty", dw));
            }
        }

        success = true;
    }
    rcatchall
    {
        for(int i = 0; i < COUNTOF(m_XovStatus); ++i)
        {
            if(m_XovStatus[i].Pending())
            {
                m_XovStatus[i].Cancel();
            }
        }

        if(m_ReadContexts)
        {
            rlDelete(m_ReadContexts, m_NumReadContexts);
        }

        if(m_ReadProperties)
        {
            rlDelete(m_ReadProperties, m_NumReadProperties);
        }

        if(m_ReadPropertySizes)
        {
            rlDelete(m_ReadPropertySizes, m_NumReadProperties);
        }

        m_NumReadContexts = m_NumReadProperties = 0;
        m_ReadContexts = NULL;
        m_ReadProperties = NULL;
        m_ReadPropertySizes = NULL;
    }

    return success;
}

bool
rlXbl::ReadWriteAttrsTask::WriteAttrs()
{
    bool success = false;

    rlXovStatus* xovStatus = &m_XovStatus[0];
    netStatus* status = &m_MyStatus[0];

    xovStatus->Begin("XUserSetContextEx", status);
    DWORD dw = XUserSetContextEx(m_LocalGamerIndex,
                                X_CONTEXT_GAME_MODE,
                                m_WriteAttrs.GetGameMode(),
                                xovStatus->ToXov());
    ++xovStatus; ++status;

    if(!rlVerify(ERROR_IO_PENDING == dw))
    {
        rlTaskError("Error:0x%08x calling XUserSetContextEx for game mode", dw);
    }
    else
    {
        rlAssert(m_WriteAttrs.GetCount() <= COUNTOF(m_XovStatus) - 2);

        unsigned numSuccessful = 0;
        unsigned numNil = 0;

        for(int i = 0; i < (int) m_WriteAttrs.GetCount(); ++i, ++xovStatus, ++status)
        {
            const u32* val = m_WriteAttrs.GetValueByIndex(i);
            if(!val)
            {
                ++numNil;
                continue;
            }

            const unsigned fieldId = m_WriteAttrs.GetAttrId(i);

            if(XUSER_DATA_TYPE_CONTEXT == XPROPERTYTYPEFROMID(fieldId))
            {
                xovStatus->Begin("XUserSetContextEx", status);
                dw = XUserSetContextEx(m_LocalGamerIndex,
                                        fieldId,
                                        *val,
                                        xovStatus->ToXov());

                if(!rlVerify(ERROR_IO_PENDING == dw))
                {
                    rlTaskError("Error:0x%08x calling XUserSetContextEx for attribute %d",
                            dw, i);
                }
                else
                {
                    ++numSuccessful;
                }
            }
            else
            {
                xovStatus->Begin("XUserSetPropertyEx", status);
                dw = XUserSetPropertyEx(m_LocalGamerIndex,
                                          fieldId,
                                          sizeof(*val),
                                          val,
                                          xovStatus->ToXov());

                if(!rlVerify(ERROR_IO_PENDING == dw))
                {
                    rlTaskError("Error:0x%08x calling XUserSetPropertyEx for attribute %d",
                            dw, i);
                }
                else
                {
                    ++numSuccessful;
                }
            }
        }

        success = (m_WriteAttrs.GetCount() == numNil + numSuccessful);
    }

    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::CreateSessionTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::CreateSessionTask::CreateSessionTask()
: m_NetMode(RL_NETMODE_INVALID)
, m_GameType(RL_GAMETYPE_INVALID)
, m_MaxPubSlots(0)
, m_MaxPrivSlots(0)
, m_CreateFlags(0)
, m_PresenceFlags(0)
, m_Handle(NULL)
, m_ArbCookie(0)
, m_State(STATE_INVALID)
, m_AsHost(false)
{
}

bool
rlXbl::CreateSessionTask::Configure(rlXbl* /*ctx*/,
                                const int localGamerIndex,
                                const rlSessionInfo* sessionInfo,
                                const rlNetworkMode netMode,
                                const rlGameType gameType,
                                const unsigned maxPubSlots,
                                const unsigned maxPrivSlots,
                                const unsigned createFlags,
                                const unsigned presenceFlags,
                                void** handle)
{
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
    rlAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_OFFLINE == netMode
            || RL_NETMODE_ONLINE == netMode);
    rlAssert(maxPubSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPubSlots + maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(RL_GAMETYPE_RANKED == gameType
            || RL_GAMETYPE_STANDARD == gameType);
    rlAssert(handle);
    rlAssert(RL_NETMODE_ONLINE == netMode
            || !(RL_SESSION_PRESENCE_FLAG_INVITABLE & presenceFlags));
	rlAssert(RL_NETMODE_ONLINE == netMode
            || !(RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE & presenceFlags));

    *handle = NULL;

    m_LocalGamerIndex = localGamerIndex;
    m_NetMode = netMode;
    m_GameType = gameType;
    m_MaxPubSlots = maxPubSlots;
    m_MaxPrivSlots = maxPrivSlots;
    m_CreateFlags = createFlags;
    m_PresenceFlags = presenceFlags;
    if(sessionInfo)
    {
        m_SessionInfo = *sessionInfo;
    }
    else
    {
        m_SessionInfo.Clear();
    }

    m_Handle = handle;
    m_AsHost = (NULL == sessionInfo);
    m_State = STATE_INVALID;

    return true;
}

void
rlXbl::CreateSessionTask::Start()
{
    rlTaskDebug("Creating session as %s...", m_AsHost ? "host" : "guest");

    this->rlXblTask::Start();

    m_State = STATE_COMMIT_GAME_TYPE;
}

void 
rlXbl::CreateSessionTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	m_XovStatus.Cancel();
}

void
rlXbl::CreateSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Creating session as %s %s",
                m_AsHost ? "host" : "guest",
                FinishString(finishType));

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::CreateSessionTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		do
		{
			switch(m_State)
			{
			case STATE_COMMIT_GAME_TYPE:
				rlTaskDebug("Committing game type...");

				m_XovStatus.Begin("XUserSetContextEx", &m_MyStatus);

				{
					const unsigned gameType =
						(RL_GAMETYPE_RANKED == m_GameType)
							? X_CONTEXT_GAME_TYPE_RANKED
							: X_CONTEXT_GAME_TYPE_STANDARD;

					DWORD dw = XUserSetContextEx(m_LocalGamerIndex,
												X_CONTEXT_GAME_TYPE,
												gameType,
												m_XovStatus.ToXov());

					if(ERROR_IO_PENDING != dw)
					{
						rlXovError("XUserSetContextEx", dw, m_XovStatus.ToXov());
						this->Finish(FINISH_FAILED);
					}
					else
					{
						m_State = STATE_COMMITTING_GAME_TYPE;
					}
				}

				break;

			case STATE_COMMITTING_GAME_TYPE:
				m_XovStatus.Update();

				if(m_MyStatus.Failed())
				{
					rlTaskDebug("Committing game type failed");

					this->Finish(FINISH_FAILED);
				}
				else if(!m_MyStatus.Pending())
				{
					m_State = STATE_CREATE_SESSION;
				}
				break;

			case STATE_CREATE_SESSION:
				rlTaskDebug("Creating session...");

				if(!this->CreateSession())
				{
					this->Finish(FINISH_FAILED);
				}
				else
				{
					m_State = STATE_CREATING_SESSION;
				}
				break;

			case STATE_CREATING_SESSION:
				m_XovStatus.Update();

				if(!m_MyStatus.Pending())
				{
					this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

bool
rlXbl::CreateSessionTask::CreateSession()
{
    bool success = false;

    const unsigned xflags =
        XMakeCreateFlags(m_NetMode,
                        m_GameType,
                        m_CreateFlags,
                        m_PresenceFlags,
                        (0 == m_MaxPubSlots),
                        m_AsHost,
                        rlPresence::IsOnline(m_LocalGamerIndex));

#if !__NO_OUTPUT
    char gamerName[RL_MAX_NAME_BUF_SIZE];
    XUserGetName(m_LocalGamerIndex, gamerName, sizeof(gamerName));
    rlTaskDebug("Gamer:\"%s\" creating session as %s:...",
                gamerName,
                m_AsHost ? "Host" : "Guest");

    rlTaskDebug("  Net mode:      %s",
                (RL_NETMODE_ONLINE == m_NetMode) ? "online" : (RL_NETMODE_LAN == m_NetMode) ? "LAN" : "offline");

    rlTaskDebug("  Presence:      %s",
                (m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED) ? "false" : "true");

    rlTaskDebug("  Invitable:     %s",
                (m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE) ? "true" : "false");
	
	rlTaskDebug("  JvP:           %s",
                (m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE) ? "true" : "false");

    rlTaskDebug("  JIP:           %s",
                (m_CreateFlags & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED) ? "false" : "true");

    rlTaskDebug("  Ranked:        %s",
                (XSESSION_CREATE_LIVE_MULTIPLAYER_RANKED == (xflags & XSESSION_CREATE_LIVE_MULTIPLAYER_RANKED)) ? "true" : "false");

    rlTaskDebug("  Stats:         %s",
                (xflags & XSESSION_CREATE_USES_STATS) ? "true" : "false");

    rlTaskDebug("  Slots:         %d(pub),%d(priv)", m_MaxPubSlots, m_MaxPrivSlots);

    rlTaskDebug("  XFlags:        0x%08x", xflags);
#endif  //__NO_OUTPUT

    rtry
    {
        const XUSER_SIGNIN_STATE signinState =
            XUserGetSigninState(m_LocalGamerIndex);

        //Make sure the gamer is signed in.
        rverify(eXUserSigninState_SignedInToLive == signinState
                || eXUserSigninState_SignedInLocally == signinState,
                catchall,
                rlTaskError("Cannot create session - gamer:\"%s\" is not signed in",
                        gamerName));

        //If the gamer is signed in, but not signed in to Live,
        //make sure the network mode is LAN or OFFLINE.
        rverify(eXUserSigninState_SignedInToLive == signinState
                || (eXUserSigninState_SignedInLocally == signinState
                    && (RL_NETMODE_LAN == m_NetMode
                        || RL_NETMODE_OFFLINE == m_NetMode)),
                catchall,
                rlTaskError("Cannot create session - owner:\"%s\" is not online",
                        gamerName));

        if(RL_NETMODE_ONLINE == m_NetMode)
        {
            rcheck(rlGamerInfo::HasMultiplayerPrivileges(m_LocalGamerIndex),
                    catchall,
                    rlTaskError("Gamer:\"%s\" does not have multiplayer privileges",
                            gamerName));
        }

        CompileTimeAssert(sizeof(XSESSION_INFO) == sizeof(m_SessionInfo));
        XSESSION_INFO* xsinfo = (XSESSION_INFO*) &m_SessionInfo;

        m_XovStatus.Begin("XSessionCreate", &m_MyStatus);

        const DWORD dw =
            XSessionCreate(xflags,
                            m_LocalGamerIndex,
                            m_MaxPubSlots,
                            m_MaxPrivSlots,
                            &m_ArbCookie,
                            xsinfo,
                            m_XovStatus.ToXov(),
                            m_Handle);

        rcheck(ERROR_SUCCESS == dw || ERROR_IO_PENDING == dw,
                catchall,
                rlXovError("XCreateSession", dw, m_XovStatus.ToXov()));

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::HostSessionTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::HostSessionTask::HostSessionTask()
: m_LocalGamerIndex(-1)
, m_NetMode(RL_NETMODE_INVALID)
, m_MaxPubSlots(0)
, m_MaxPrivSlots(0)
, m_CreateFlags(0)
, m_PresenceFlags(0)
, m_Handle(NULL)
, m_ArbCookie(0)
, m_SessionInfo(NULL)
, m_State(STATE_INVALID)
{
}

bool
rlXbl::HostSessionTask::Configure(rlXbl* /*ctx*/,
                                const int localGamerIndex,
                                const rlNetworkMode netMode,
                                const unsigned maxPubSlots,
                                const unsigned maxPrivSlots,
                                const rlMatchingAttributes& attrs,
                                const unsigned createFlags,
                                const unsigned presenceFlags,
                                void** handle,
                                u64* arbCookie,
                                rlSessionInfo* sessionInfo)
{
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
    rlAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_OFFLINE == netMode
            || RL_NETMODE_ONLINE == netMode);
    rlAssert(maxPubSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPubSlots + maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPubSlots + maxPrivSlots > 0);
    rlAssert(attrs.IsValid());
    rlAssert(handle);
    rlAssert(arbCookie);
    rlAssert(sessionInfo);

    *handle = NULL;
    *arbCookie = 0;
    sessionInfo->Clear();

    m_LocalGamerIndex = localGamerIndex;
    m_NetMode = netMode;
    m_MaxPubSlots = maxPubSlots;
    m_MaxPrivSlots = maxPrivSlots;
    m_Attrs = attrs;
    m_CreateFlags = createFlags;
    m_PresenceFlags = presenceFlags;
    m_Handle = handle;
    m_ArbCookie = arbCookie;
    m_SessionInfo = sessionInfo;
    m_State = STATE_INVALID;

    return true;
}

void
rlXbl::HostSessionTask::Start()
{
    rlTaskDebug("Hosting session...");

    this->rlXblTask::Start();

    m_State = STATE_COMMIT_ATTRIBUTES;

    //If we have any nil attributes then fill them in with
    //the default attribute values.
    for(int i = 0; i < (int) m_Attrs.GetCount(); ++i)
    {
        if(!m_Attrs.GetValueByIndex(i))
        {
            rlTaskDebug("At least one matching attribute is nil");

            //Copy the attribute ids.
            m_DefaultAttrs = m_Attrs;

            m_State = STATE_GET_DEFAULT_ATTRIBUTES;
            break;
        }
    }
}

void 
rlXbl::HostSessionTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	if(m_MyStatus.Pending())
	{
		switch(m_State)
		{
			case STATE_GETTING_DEFAULT_ATTRIBUTES:
			case STATE_COMMITTING_ATTRIBUTES:
				{
					m_ReadWriteAttrsTask.DoCancel();
				}
				break;

			case STATE_CREATING_SESSION:
				{
					m_CreateSessionTask.DoCancel();
				}
				break;

			default:
				{
					rlAssertf(0, "Pending status in invalid state!");
				}
				break;
		}
	}
}

void
rlXbl::HostSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Hosting session %s", FinishString(finishType));

    if(FinishSucceeded(finishType))
    {
        *m_ArbCookie = m_CreateSessionTask.m_ArbCookie;
        *m_SessionInfo = m_CreateSessionTask.m_SessionInfo;
    }

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::HostSessionTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		do 
		{
			switch(m_State)
			{
			case STATE_GET_DEFAULT_ATTRIBUTES:
				rlTaskDebug("Retrieving default matching attributes...");

				if(m_ReadWriteAttrsTask.ConfigureForRead(m_Ctx,
														m_LocalGamerIndex,
														&m_DefaultAttrs,
														&m_MyStatus))
				{
					m_ReadWriteAttrsTask.Start();
					m_State = STATE_GETTING_DEFAULT_ATTRIBUTES;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_GETTING_DEFAULT_ATTRIBUTES:
				m_ReadWriteAttrsTask.Update(timeStep);

				if(m_MyStatus.Succeeded())
				{
					for(int i = 0; i < (int) m_Attrs.GetCount(); ++i)
					{
						if(!m_Attrs.GetValueByIndex(i))
						{
							//If we failed to retrieve the default value
							//then just use a random value.
							const u32* p = m_DefaultAttrs.GetValueByIndex(i);
							const u32 val = p ? *p : (u32) sysTimer::GetTicks();
							m_Attrs.SetValueByIndex(i, val);
						}
					}

					m_State = STATE_COMMIT_ATTRIBUTES;
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_COMMIT_ATTRIBUTES:
				rlTaskDebug("Committing matching attributes...");

				if(m_ReadWriteAttrsTask.ConfigureForWrite(m_Ctx,
														m_LocalGamerIndex,
														m_Attrs,
														&m_MyStatus))
				{
					m_ReadWriteAttrsTask.Start();
					m_State = STATE_COMMITTING_ATTRIBUTES;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_COMMITTING_ATTRIBUTES:
				m_ReadWriteAttrsTask.Update(timeStep);

				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_CREATE_SESSION;
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_CREATE_SESSION:
				rlTaskDebug("Creating session...");

				if(rlTaskBase::Configure(&m_CreateSessionTask,
											m_Ctx,
											m_LocalGamerIndex,
											(rlSessionInfo*)NULL,
											m_NetMode,
											m_Attrs.GetGameType(),
											m_MaxPubSlots,
											m_MaxPrivSlots,
											m_CreateFlags,
											m_PresenceFlags,
											m_Handle,
											&m_MyStatus))
				{
					m_CreateSessionTask.Start();
					m_State = STATE_CREATING_SESSION;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_CREATING_SESSION:
				m_CreateSessionTask.Update(timeStep);

				if(!m_MyStatus.Pending())
				{
					this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::JoinSessionTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::JoinSessionTask::JoinSessionTask()
: m_hSession(NULL)
, m_NumLocalGamers(0)
, m_NumRemoteGamers(0)
, m_State(STATE_INVALID)
{
}

bool
rlXbl::JoinSessionTask::Configure(rlXbl* /*ctx*/,
                                    const void* hSession,
                                    const rlGamerHandle* gamerHandles,
                                    const rlSlotType* slotTypes,
                                    const unsigned numGamers)
{
    rlAssert(hSession);

    bool success = false;

    rtry
    {
        m_hSession = hSession;
        m_NumLocalGamers = m_NumRemoteGamers = 0;

        for(int i = 0; i < (int) numGamers; ++i)
        {
            const rlGamerHandle& gamerHandle = gamerHandles[i];

            if(gamerHandle.IsLocal())
            {
                rverify(m_NumLocalGamers < COUNTOF(m_LocalGamerIndexes), catchall,);

                m_LocalGamerIndexes[m_NumLocalGamers] = gamerHandle.GetLocalIndex();
                m_LocalPrivSlots[m_NumLocalGamers] = (RL_SLOT_PRIVATE == slotTypes[i]);
                ++m_NumLocalGamers;
            }
            else
            {
                rverify(m_NumRemoteGamers < COUNTOF(m_RemoteGamerHandles), catchall,);

                m_RemoteGamerHandles[m_NumRemoteGamers] = gamerHandle.GetXuid();
                m_RemotePrivSlots[m_NumRemoteGamers] = (RL_SLOT_PRIVATE == slotTypes[i]);
                ++m_NumRemoteGamers;
            }
        }

        m_State = STATE_INVALID;

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

void
rlXbl::JoinSessionTask::Start()
{
    rlTaskDebug("Joining session...");

    this->rlXblTask::Start();

    m_State = STATE_JOIN_LOCAL;
}

void 
rlXbl::JoinSessionTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	m_XovStatus.Cancel();
}

void
rlXbl::JoinSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Joining session %s", FinishString(finishType));

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::JoinSessionTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		do
		{
			switch(m_State)
			{
			case STATE_JOIN_LOCAL:
				if(m_NumLocalGamers)
				{
					rlTaskDebug("Joining %d local gamers...", m_NumLocalGamers);

					CompileTimeAssert(sizeof(m_LocalGamerIndexes[0]) == sizeof(DWORD));
					CompileTimeAssert(sizeof(m_LocalPrivSlots[0]) == sizeof(BOOL));

					m_XovStatus.Begin("XSessionJoinLocal", &m_MyStatus);
					const DWORD dw =
						XSessionJoinLocal((HANDLE) m_hSession,
											m_NumLocalGamers,
											(const DWORD*) m_LocalGamerIndexes,
											(const BOOL*) m_LocalPrivSlots,
											m_XovStatus.ToXov());

					if(ERROR_IO_PENDING == dw || ERROR_SUCCESS == dw)
					{
						m_State = STATE_JOINING_LOCAL;
					}
					else
					{
						rlXovError("XSessionJoinLocal", dw, m_XovStatus.ToXov());
						this->Finish(FINISH_FAILED);
					}
				}
				else
				{
					m_State = STATE_JOIN_REMOTE;
				}
				break;

			case STATE_JOINING_LOCAL:
				m_XovStatus.Update();

				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_JOIN_REMOTE;
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_JOIN_REMOTE:
				if(m_NumRemoteGamers)
				{
					rlTaskDebug("Joining %d remote gamers...", m_NumRemoteGamers);

					CompileTimeAssert(sizeof(m_RemoteGamerHandles[0]) == sizeof(XUID));
					CompileTimeAssert(sizeof(m_RemotePrivSlots[0]) == sizeof(BOOL));

					m_XovStatus.Begin("XSessionJoinRemote", &m_MyStatus);
					const DWORD dw =
						XSessionJoinRemote((HANDLE) m_hSession,
											m_NumRemoteGamers,
											(const XUID*) m_RemoteGamerHandles,
											(const BOOL*) m_RemotePrivSlots,
											m_XovStatus.ToXov());

					if(ERROR_IO_PENDING == dw || ERROR_SUCCESS == dw)
					{
						m_State = STATE_JOINING_REMOTE;
					}
					else
					{
						rlXovError("XSessionJoinRemote", dw, m_XovStatus.ToXov());
						this->Finish(FINISH_FAILED);
					}
				}
				else
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				break;

			case STATE_JOINING_REMOTE:
				m_XovStatus.Update();

				if(!m_MyStatus.Pending())
				{
					this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::LeaveSessionTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::LeaveSessionTask::LeaveSessionTask()
: m_hSession(NULL)
, m_NumLocalGamers(0)
, m_NumRemoteGamers(0)
, m_State(STATE_INVALID)
{
}

bool
rlXbl::LeaveSessionTask::Configure(rlXbl* /*ctx*/,
                                    const void* hSession,
                                    const rlGamerHandle* gamerHandles,
                                    const unsigned numGamers)
{
    rlAssert(hSession);

    bool success = false;

    rtry
    {
        m_hSession = hSession;
        m_NumLocalGamers = m_NumRemoteGamers = 0;

        for(int i = 0; i < (int) numGamers; ++i)
        {
            const rlGamerHandle& gamerHandle = gamerHandles[i];

            if(gamerHandle.IsLocal())
            {
                rverify(m_NumLocalGamers < COUNTOF(m_LocalGamerIndexes), catchall,);

                m_LocalGamerIndexes[m_NumLocalGamers] = gamerHandle.GetLocalIndex();
                ++m_NumLocalGamers;
            }
            else
            {
                rverify(m_NumRemoteGamers < COUNTOF(m_RemoteGamerHandles), catchall,);

                m_RemoteGamerHandles[m_NumRemoteGamers] = gamerHandle.GetXuid();
                ++m_NumRemoteGamers;
            }
        }

        m_State = STATE_INVALID;

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

void
rlXbl::LeaveSessionTask::Start()
{
    rlTaskDebug("Leaving session...");

    this->rlXblTask::Start();

    m_State = STATE_LEAVE_LOCAL;
}

void 
rlXbl::LeaveSessionTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	m_XovStatus.Cancel();
}

void
rlXbl::LeaveSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Leaving session %s", FinishString(finishType));

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::LeaveSessionTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		do
		{
			switch(m_State)
			{
			case STATE_LEAVE_LOCAL:
				if(m_NumLocalGamers)
				{
					rlTaskDebug("Leaving %d local gamers...", m_NumLocalGamers);

					CompileTimeAssert(sizeof(m_LocalGamerIndexes[0]) == sizeof(DWORD));

					m_XovStatus.Begin("XSessionLeaveLocal", &m_MyStatus);
					const DWORD dw =
						XSessionLeaveLocal((HANDLE) m_hSession,
											m_NumLocalGamers,
											(const DWORD*) m_LocalGamerIndexes,
											m_XovStatus.ToXov());

					if(ERROR_IO_PENDING == dw || ERROR_SUCCESS == dw)
					{
						m_State = STATE_LEAVING_LOCAL;
					}
					else
					{
						rlXovError("XSessionLeaveLocal", dw, m_XovStatus.ToXov());
						this->Finish(FINISH_FAILED);
					}
				}
				else
				{
					m_State = STATE_LEAVE_REMOTE;
				}
				break;

			case STATE_LEAVING_LOCAL:
				m_XovStatus.Update();

				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_LEAVE_REMOTE;
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_LEAVE_REMOTE:
				if(m_NumRemoteGamers)
				{
					rlTaskDebug("Leaving %d remote gamers...", m_NumRemoteGamers);

					CompileTimeAssert(sizeof(m_RemoteGamerHandles[0]) == sizeof(XUID));

					m_XovStatus.Begin("XSessionLeaveRemote", &m_MyStatus);
					const DWORD dw =
						XSessionLeaveRemote((HANDLE) m_hSession,
											m_NumRemoteGamers,
											(const XUID*) m_RemoteGamerHandles,
											m_XovStatus.ToXov());

					if(ERROR_IO_PENDING == dw || ERROR_SUCCESS == dw)
					{
						m_State = STATE_LEAVING_REMOTE;
					}
					else
					{
						rlXovError("XSessionLeaveRemote", dw, m_XovStatus.ToXov());
						this->Finish(FINISH_FAILED);
					}
				}
				else
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				break;

			case STATE_LEAVING_REMOTE:
				m_XovStatus.Update();

				if(!m_MyStatus.Pending())
				{
					this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::DestroySessionTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::DestroySessionTask::DestroySessionTask()
: m_LocalGamerIndex(-1)
, m_hSession(NULL)
, m_State(STATE_INVALID)
{
}

bool
rlXbl::DestroySessionTask::Configure(rlXbl* /*ctx*/,
                                    const int localGamerIndex,
                                    const void* hSession)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    rlAssert(hSession);

    m_LocalGamerIndex = localGamerIndex;
    m_hSession = hSession;
    m_State = STATE_INVALID;

    return true;
}

void
rlXbl::DestroySessionTask::Start()
{
    rlTaskDebug("Destroying session...");

    this->rlXblTask::Start();

    m_State = STATE_DESTROY;
}

void 
rlXbl::DestroySessionTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	// we cannot cancel an in-flight XSessionDestroy call 
	if(m_State != STATE_DESTROYING)
	{
		m_XovStatus.Cancel();
	}
}

bool
rlXbl::DestroySessionTask::IsCancelable() const
{
	// we cannot cancel an in-flight XSessionDestroy call 
	return (m_State != STATE_DESTROYING);
}

void
rlXbl::DestroySessionTask::Finish(const FinishType finishType, const int resultCode)
{
	rlAssertf(!(WasCanceled() && m_XovStatus.Pending()), 
		"Task cancelled and still processing XSessionDestroy call!");

	rlTaskDebug("Destroying session %s", FinishString(finishType));

	if(FinishSucceeded(finishType))
	{
		XCloseHandle((HANDLE) m_hSession);
	}
	
	this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::DestroySessionTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{
		do 
		{
			switch(m_State)
			{
			case STATE_DESTROY:
				if(rlPresence::IsSignedIn(m_LocalGamerIndex))
				{
					m_XovStatus.Begin("XSessionDelete", &m_MyStatus);
					const DWORD dw =
						XSessionDelete((HANDLE) m_hSession, m_XovStatus.ToXov());

					if(ERROR_IO_PENDING == dw || ERROR_SUCCESS == dw)
					{
						m_State = STATE_DESTROYING;
					}
					else
					{
						rlXovError("XSessionDelete", dw, m_XovStatus.ToXov());
						this->Finish(FINISH_FAILED);
					}
				}
				else
				{
					//Can't call XSession* functions for gamers who are not signed in.
					rlTaskDebug("Local owner of session is no longer signed in - not calling XSessionDelete()");
					this->Finish(FINISH_SUCCEEDED);
				}
				break;

			case STATE_DESTROYING:
				m_XovStatus.Update();

				if(!m_MyStatus.Pending())
				{
					this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::ChangeAttributesTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::ChangeAttributesTask::ChangeAttributesTask()
: m_LocalGamerIndex(-1)
, m_hSession(NULL)
, m_NetMode(RL_NETMODE_INVALID)
, m_MaxPubSlots(0)
, m_MaxPrivSlots(0)
, m_CreateFlags(0)
, m_PresenceFlags(0)
, m_State(STATE_INVALID)
{
}

bool
rlXbl::ChangeAttributesTask::Configure(rlXbl* /*ctx*/,
                                        const int localGamerIndex,
                                        const void* hSession,
                                        const rlNetworkMode netMode,
                                        const unsigned maxPubSlots,
                                        const unsigned maxPrivSlots,
                                        const rlMatchingAttributes& attrs,
                                        const unsigned createFlags,
                                        const unsigned presenceFlags)
{
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
    rlAssert(hSession);
    rlAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_OFFLINE == netMode
            || RL_NETMODE_ONLINE == netMode);
    rlAssert(maxPubSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPubSlots + maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(attrs.IsValid());

    m_LocalGamerIndex = localGamerIndex;
    m_hSession = hSession;
    m_NetMode = netMode;
    m_MaxPubSlots = maxPubSlots;
    m_MaxPrivSlots = maxPrivSlots;
    m_Attrs = attrs;
    m_CreateFlags = createFlags;
    m_PresenceFlags = presenceFlags;
    m_State = STATE_INVALID;

    return true;
}

void
rlXbl::ChangeAttributesTask::Start()
{
    rlTaskDebug("Changing session attributes...");

    this->rlXblTask::Start();

    m_State = STATE_MODIFY_SESSION;
}

void 
rlXbl::ChangeAttributesTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	if(m_State == STATE_COMMITTING_ATTRIBUTES)
	{
		m_ReadWriteAttrsTask.DoCancel();
	}
	else
	{
		m_XovStatus.Cancel();
	}
}

void
rlXbl::ChangeAttributesTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Changing session attributes %s", FinishString(finishType));

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::ChangeAttributesTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);
	
	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{
		do
		{
			switch(m_State)
			{
			case STATE_COMMIT_ATTRIBUTES:
				rlTaskDebug("Committing session attributes...");

				if(m_ReadWriteAttrsTask.ConfigureForWrite(m_Ctx,
					m_LocalGamerIndex,
					m_Attrs,
					&m_MyStatus))
				{
					m_ReadWriteAttrsTask.Start();
					m_State = STATE_COMMITTING_ATTRIBUTES;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_COMMITTING_ATTRIBUTES:
				m_ReadWriteAttrsTask.Update(timeStep);

				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_MODIFY_SESSION;
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_MODIFY_SESSION:
				rlTaskDebug("Modifying session...");

				if(this->ModifySession())
				{
					m_State = STATE_MODIFYING_SESSION;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_MODIFYING_SESSION:
				m_XovStatus.Update();

				if(!m_MyStatus.Pending())
				{
					this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

bool
rlXbl::ChangeAttributesTask::ModifySession()
{
    XSESSION_LOCAL_DETAILS localDetails = {0};
    DWORD sizeofDetails = sizeof(localDetails);

    XSessionGetDetails((HANDLE) m_hSession, &sizeofDetails, &localDetails, NULL);

    const unsigned xflags =
        XMakeCreateFlags(m_NetMode,
                        m_Attrs.GetGameType(),
                        m_CreateFlags,
                        m_PresenceFlags,
                        (0 == m_MaxPubSlots),
                        (XUSER_INDEX_NONE != localDetails.dwUserIndexHost),
                        rlPresence::IsOnline(m_LocalGamerIndex));

#if !__NO_OUTPUT
    char gamerName[RL_MAX_NAME_BUF_SIZE];
    XUserGetName(m_LocalGamerIndex, gamerName, sizeof(gamerName));
#endif
    rlTaskDebug("Gamer:\"%s\" modifying session...", gamerName);

    rlTaskDebug("  Net mode:      %s",
                (RL_NETMODE_ONLINE == m_NetMode) ? "online" : (RL_NETMODE_LAN == m_NetMode) ? "LAN" : "offline");

    rlTaskDebug("  Presence:      %s",
                (m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED) ? "false" : "true");

    rlTaskDebug("  Invitable:     %s",
                (m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE) ? "true" : "false");
	
	rlTaskDebug("  JvP:           %s",
                (m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE) ? "true" : "false");

    rlTaskDebug("  JIP:           %s",
                (m_CreateFlags & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED) ? "false" : "true");

    rlTaskDebug("  Ranked:        %s",
                (XSESSION_CREATE_LIVE_MULTIPLAYER_RANKED == (xflags & XSESSION_CREATE_LIVE_MULTIPLAYER_RANKED)) ? "true" : "false");

    rlTaskDebug("  Stats:         %s",
                (xflags & XSESSION_CREATE_USES_STATS) ? "true" : "false");

    rlTaskDebug("  Slots:         %d(pub),%d(priv)", m_MaxPubSlots, m_MaxPrivSlots);

    rlTaskDebug("  XFlags:        0x%08x", xflags);

    m_XovStatus.Begin("XSessionModify", &m_MyStatus);

    const DWORD dw = XSessionModify((HANDLE) m_hSession,
                                    xflags,
                                    m_MaxPubSlots,
                                    m_MaxPrivSlots,
                                    m_XovStatus.ToXov());

    const bool success =
        (ERROR_SUCCESS == dw || ERROR_IO_PENDING == dw);

    if(!success)
    {
        rlXovError("XSessionModify", dw, m_XovStatus.ToXov());
    }

    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::MigrateHostTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::MigrateHostTask::MigrateHostTask()
: m_hSession(NULL)
, m_LocalHostIndex(-1)
, m_NewSessionInfo(NULL)
, m_State(STATE_INVALID)
{
}

bool
rlXbl::MigrateHostTask::Configure(rlXbl* /*ctx*/,
                                    const void* hSession,
                                    const int localGamerIndex,
                                    rlSessionInfo* newSessionInfo)
{
    if(rlVerifyf(hSession, "Invalid session handle")
        && rlVerifyf(newSessionInfo, "NULL session info pointer"))
    {
        if(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        {
            newSessionInfo->Clear();
            m_LocalHostIndex = localGamerIndex;
        }
        else
        {
            rlAssert(newSessionInfo->IsValid()
                    && newSessionInfo->GetHostPeerAddress().IsRemote());
            m_LocalHostIndex = XUSER_INDEX_NONE;
        }

        m_hSession = hSession;
        m_NewSessionInfo = newSessionInfo;
        m_State = STATE_INVALID;

        return true;
    }

    return false;
}

void
rlXbl::MigrateHostTask::Start()
{
    rlTaskDebug("Migrating host with token 0x%016" I64FMT "x...", m_NewSessionInfo->GetToken().m_Value);

    this->rlXblTask::Start();

    m_State = STATE_MIGRATE;
}

void 
rlXbl::MigrateHostTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	m_XovStatus.Cancel();
}

void
rlXbl::MigrateHostTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Migrating host %s", FinishString(finishType));

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::MigrateHostTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		do 
		{
			switch(m_State)
			{
			case STATE_MIGRATE:
				if(this->MigrateHost())
				{
					m_State = STATE_MIGRATING;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_MIGRATING:
				m_XovStatus.Update();

				if(m_MyStatus.Succeeded())
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

bool
rlXbl::MigrateHostTask::MigrateHost()
{
    CompileTimeAssert(sizeof(XSESSION_INFO) == sizeof(*m_NewSessionInfo));
    XSESSION_INFO* xsinfo = (XSESSION_INFO*) m_NewSessionInfo;

    m_XovStatus.Begin("XSessionMigrateHost", &m_MyStatus);

    DWORD dw = XSessionMigrateHost((HANDLE) m_hSession,
                                    m_LocalHostIndex,
                                    xsinfo,
                                    m_XovStatus.ToXov());

    const bool success =
        (ERROR_SUCCESS == dw || ERROR_IO_PENDING == dw);

    if(!success)
    {
        rlXovError("XSessionMigrateHost", dw, m_XovStatus.ToXov());

        // wrong state error, log out session details
#if !__NO_OUTPUT
        if(dw == XONLINE_E_SESSION_WRONG_STATE)
        {
            XSESSION_LOCAL_DETAILS sDetails;
            DWORD dwSize = sizeof(XSESSION_LOCAL_DETAILS);

            XSessionGetDetails((HANDLE)m_hSession, &dwSize, &sDetails, NULL);
            rlTaskDebug("Session in wrong state to migrate");
            rlTaskDebug("Details :: State: %d", sDetails.eState);
            rlTaskDebug("Details :: Flags: %d", sDetails.dwFlags);
            rlTaskDebug("Details :: Member Count: %d", sDetails.dwActualMemberCount);
            rlTaskDebug("Details :: Slots: Priv: %d / %d, Pub: %d / %d", sDetails.dwAvailablePublicSlots, 
                                                                         sDetails.dwMaxPublicSlots,
                                                                         sDetails.dwAvailablePrivateSlots,
                                                                         sDetails.dwMaxPrivateSlots);
        }
#endif
    }

    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::SendInvitesTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::SendInvitesTask::SendInvitesTask()
: m_NumInvitees(0)
, m_State(STATE_INVALID)
{
}

bool
rlXbl::SendInvitesTask::Configure(rlXbl* /*ctx*/,
                                    const int localInviterIndex,
                                    const rlGamerHandle* invitees,
                                    const unsigned numInvitees,
                                    const char* salutation)
{
    rlAssert(salutation);

    bool success = false;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localInviterIndex))
        && rlVerify(numInvitees > 0)
        && rlVerify(numInvitees <= COUNTOF(m_InviteeIds)))
    {
        m_LocalInviterIndex = localInviterIndex;

        for(int i = 0; i < (int) numInvitees; ++i)
        {
            m_InviteeIds[i] = invitees[i].GetXuid();
        }

        m_NumInvitees = numInvitees;

        safecpy(m_Salutation, salutation, COUNTOF(m_Salutation));

        m_State = STATE_INVALID;

        success = true;
    }

    return success;
}

void
rlXbl::SendInvitesTask::Start()
{
    rlTaskDebug("Sending invites...");

    this->rlXblTask::Start();

    m_State = STATE_SEND_INVITES;
}

void 
rlXbl::SendInvitesTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	m_XovStatus.Cancel();
}

void
rlXbl::SendInvitesTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Sending invites %s", FinishString(finishType));

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::SendInvitesTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{  
		do 
		{
			switch(m_State)
			{
			case STATE_SEND_INVITES:
				if(this->SendInvites())
				{
					m_State = STATE_SENDING_INVITES;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_SENDING_INVITES:
				m_XovStatus.Update();

				if(m_MyStatus.Succeeded())
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

bool
rlXbl::SendInvitesTask::SendInvites()
{
    CompileTimeAssert(sizeof(char16) == sizeof(WCHAR));
    WCHAR salutation[RL_MAX_INVITE_SALUTATION_CHARS];
    Utf8ToWide((char16*) salutation, m_Salutation, COUNTOF(salutation));

    CompileTimeAssert(sizeof(m_InviteeIds[0]) == sizeof(XUID));

    m_XovStatus.Begin("XInviteSend", &m_MyStatus);

    DWORD dw = XInviteSend(m_LocalInviterIndex,
                            m_NumInvitees,
                            (const XUID*) m_InviteeIds,
                            salutation,
                            m_XovStatus.ToXov());

    const bool success =
        (ERROR_SUCCESS == dw || ERROR_IO_PENDING == dw);

    if(!success)
    {
        rlXovError("XInviteSend", dw, m_XovStatus.ToXov());
    }

    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::SendPartyInvitesTask
///////////////////////////////////////////////////////////////////////////////

rlXbl::SendPartyInvitesTask::SendPartyInvitesTask()
: m_State(STATE_INVALID)
{
}

bool
rlXbl::SendPartyInvitesTask::Configure(rlXbl* /*ctx*/,
                                  const int localInviterIndex)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localInviterIndex));

    m_LocalInviterIndex = localInviterIndex;

    m_State = STATE_INVALID;

    return true;
}

void
rlXbl::SendPartyInvitesTask::Start()
{
    rlTaskDebug("Sending invites...");

    this->rlXblTask::Start();

    m_State = STATE_SEND_INVITES;
}

void	
rlXbl::SendPartyInvitesTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	m_XovStatus.Cancel();
}

void
rlXbl::SendPartyInvitesTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Sending invites %s", FinishString(finishType));

    this->rlXblTask::Finish(finishType, resultCode);
}

void
rlXbl::SendPartyInvitesTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		do 
		{
			switch(m_State)
			{
			case STATE_SEND_INVITES:
				if(this->SendPartyInvites())
				{
					m_State = STATE_SENDING_INVITES;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
				break;

			case STATE_SENDING_INVITES:
				m_XovStatus.Update();

				if(m_MyStatus.Succeeded())
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}
}

bool
rlXbl::SendPartyInvitesTask::SendPartyInvites()
{
    m_XovStatus.Begin("XInviteSend", &m_MyStatus);

    DWORD dw = XPartySendGameInvites(m_LocalInviterIndex,
                                    m_XovStatus.ToXov());

    const bool success =
        (ERROR_SUCCESS == dw || ERROR_IO_PENDING == dw);

    if(!success)
    {
        rlXovError("XInviteSend", dw, m_XovStatus.ToXov());
    }

    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXbl::ShowSocialGetUserTokenUITask Methods
///////////////////////////////////////////////////////////////////////////////

rlXbl::SocialGetUserTokenTask::SocialGetUserTokenTask()
: m_State(STATE_INVALID)
{
}

bool
rlXbl::SocialGetUserTokenTask::Configure(const int localGamerIndex,
									     const int socialNetworkId,
										 const char* desiredPermissions,
										 const bool showUi,
										 char* tokenBuffer,
										 unsigned tokenBufferSize)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));

    m_LocalGamerIndex = localGamerIndex;
	m_SocialNetworkId = socialNetworkId;

	safecpy(m_DesiredPermissions, desiredPermissions, RL_FACEBOOK_PERMISSIONS_LENGTH_MAX);
	rlAssertf(strlen(m_DesiredPermissions) == strlen(desiredPermissions),
		      "m_DesiredPermissions must be large enough to hold the desired permissions passed in");

	// Xbox wants the permissions semi-colon separated instead of comma separated. So we do any search and replace here.
	//
	for(u32 i = 0; i < strlen(m_DesiredPermissions); i++)
	{
		if (m_DesiredPermissions[i] == ',')
		{
			m_DesiredPermissions[i] = ';';
		}
	}

	m_TokenBuffer = tokenBuffer;
	m_TokenBufferSize = tokenBufferSize;

    m_State = STATE_INVALID;

	m_ShowUi = showUi;

    return true;
}

void
rlXbl::SocialGetUserTokenTask::Start()
{	
    rlTaskDebug("Showing GetUserToken UI...");

    this->rlXblTask::Start();

    m_State = STATE_SHOW_GETUSERTOKEN_UI;
}

void	
rlXbl::SocialGetUserTokenTask::DoCancel()
{
	this->rlXblTask::DoCancel();

	m_XovStatus.Cancel();
}

void
rlXbl::SocialGetUserTokenTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("GetUserToken Task: %s", FinishString(finishType));

	int result = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
    if(!FinishSucceeded(finishType))
    {
        if (resultCode == ERROR_ACCESS_DENIED)
		{
			result = RL_FB_GET_TOKEN_FAILED_NOT_ALLOWED;
			rlTaskDebug("GetUserToken task failed because user has social sharing disabled.");
		}
		else if (resultCode ==  E_ACCESSDENIED)
		{
			result = RL_FB_GET_TOKEN_FAILED_NOT_CONFIGURED;
			rlTaskDebug("GetUserToken task failed because user does not have app approved. Please Show the GetTokenUI to the user.");
		}
		else if (resultCode == ERROR_CANCELLED)
		{
			result = RL_FB_GET_TOKEN_FAILED_USER_CANCELLED;
			rlTaskDebug("GetUserToken task failed because user cancelled the dialog.");
		}
		else if (resultCode == ERROR_UNIDENTIFIED_ERROR)
		{
			result = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
			rlTaskDebug("GetUserToken task failed. It's possible the user doesn't have social sharing enabled.");
		}
		else if (resultCode == ERROR_INSUFFICIENT_BUFFER)
		{
			result = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
			rlTaskDebug("GetUserToken task failed: The buffer allocated is not large enough to contain the token. Needs to be %d characters.", m_TokenBufferSize);
		}
    }
	else
	{
		result = RL_FB_GET_TOKEN_SUCCESS;
		WideToUtf8(m_TokenBuffer, m_TokenBufferWide, m_TokenBufferSize);
		if (strlen(m_TokenBuffer) != wcslen(m_TokenBufferWide))
		{
			rlAssertf(false, "Conversion of wchar to char must result in the same number of characters.");
		}
		rlTaskDebug("GetUserToken succeeded: %s", m_TokenBuffer);
	}

    this->rlXblTask::Finish(finishType, result);
}

void
rlXbl::SocialGetUserTokenTask::Update(const unsigned timeStep)
{
    this->rlXblTask::Update(timeStep);

	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}
	else
	{ 
		u32 dw = ERROR_SUCCESS;
		bool success = false;
		switch(m_State)
		{
		case STATE_SHOW_GETUSERTOKEN_UI:
			dw = this->GetUserToken();
			success = (ERROR_SUCCESS == dw || ERROR_IO_PENDING == dw);

			if(!success)
			{
				rlXovError("XSocialGetUserToken", dw, m_XovStatus.ToXov());
				// Cancel the Xoverlapped because we can't even properly start
				// the operation at this point.
				m_XovStatus.Cancel();
				this->Finish(FINISH_FAILED, dw);
			}
			else
			{
				m_State = STATE_SHOWING_GETUSERTOKEN_UI;
			}
			break;

		case STATE_SHOWING_GETUSERTOKEN_UI:
			m_XovStatus.Update();

			if(m_MyStatus.Succeeded())
			{
				this->Finish(FINISH_SUCCEEDED, m_XovStatus.GetResult());
			}
			else if(!m_MyStatus.Pending())
			{
				this->Finish(FINISH_FAILED, m_XovStatus.GetResult());
			}
			break;
		}
	}
}

u32
rlXbl::SocialGetUserTokenTask::GetUserToken()
{
    CompileTimeAssert(sizeof(char16) == sizeof(WCHAR));
	CompileTimeAssert(sizeof(u32) == sizeof(DWORD));

    WCHAR permissions[RL_FACEBOOK_PERMISSIONS_LENGTH_MAX];
    Utf8ToWide((char16*) permissions, m_DesiredPermissions, COUNTOF(permissions));

	rlAssertf(strlen(m_DesiredPermissions) == wcslen(permissions), 
			    "Conversion of char to wchar must result in the same number of characters.");
	u32 dw;
	if (m_ShowUi)
	{
		// Show Xbox Ui (even if just briefly if the user already has token)
		m_XovStatus.Begin("XShowSocialGetUserToken", &m_MyStatus);

		dw = XShowSocialGetUserToken(m_LocalGamerIndex,
										   m_SocialNetworkId,
										   permissions,
										   (LPWSTR)m_TokenBufferWide,
										   (DWORD*)&m_TokenBufferSize,
										   m_XovStatus.ToXov());
	}
	else
	{
		// No ui and get cached
		// Show Xbox Ui (even if just briefly if the user already has token)
		m_XovStatus.Begin("XSocialGetUserToken", &m_MyStatus);

		dw = XSocialGetUserToken(m_LocalGamerIndex,
									   m_SocialNetworkId,
									   permissions,
									   (LPWSTR)m_TokenBufferWide,
									   (DWORD*)&m_TokenBufferSize,
									   m_XovStatus.ToXov());
	}

	rlAssertf(m_TokenBufferSize >= RL_FACEBOOK_TOKEN_MAX_LENGTH, 
		"Social Network Token Buffer needs to be large enough to contain the tokens returned from XBL");

	return dw;
}



///////////////////////////////////////////////////////////////////////////////
//  rlXbl
///////////////////////////////////////////////////////////////////////////////

rlGameRegion
rlXbl::GetGameRegion()
{
    DWORD result = XGetGameRegion();

    switch(result)
    {
    case XC_GAME_REGION_NA_ALL:             
        return RL_GAME_REGION_AMERICA;
    
    case XC_GAME_REGION_ASIA_JAPAN:
        return RL_GAME_REGION_JAPAN;

    case XC_GAME_REGION_EUROPE_ALL:
    case XC_GAME_REGION_EUROPE_AUNZ:
    case XC_GAME_REGION_EUROPE_REST:
        return RL_GAME_REGION_EUROPE;

    case XC_GAME_REGION_ASIA_ALL:
    case XC_GAME_REGION_ASIA_CHINA:
    case XC_GAME_REGION_ASIA_REST:
    case XC_GAME_REGION_RESTOFWORLD_ALL:
    default:
        rlError("Unknown/unhandled game region (%u)", result);
        return RL_GAME_REGION_INVALID;
    };
}

rlXbl::rlXbl()
{
}

rlXbl::~rlXbl()
{
    this->Shutdown();
}

bool
rlXbl::Init()
{
#if __DEV && 0
    //For convenience, allow resetting leaderboards via cmdline params.
    int startId = 0;
    int numToReset = 1;

    PARAM_rlXblLbResetStartId.Get(startId);
    PARAM_rlXblLbResetNum.Get(numToReset);

    if(startId && numToReset)
    {
        rlDebug("Resetting LBs from 0x%08x to 0x%08x...", 
                (unsigned)startId, 
                (unsigned)(startId + numToReset - 1));

        int numReset = 0;

        for(int i = startId; i < (startId + numToReset); i++)
        {
            if(rlXbl::ResetAllUsersStats((u64)i))
            {
                rlDebug("Reset LB 0x%08x", i);
                ++numReset;
            }
            else
            {
                rlError("Failed to reset leaderboard 0x%08x", (unsigned)i);
            }
        }

        rlDebug("Successfully reset %d leaderboards", numReset);
    }
#endif

    return rlXhttp::Init();
}

void
rlXbl::Shutdown()
{
	rlXhttp::Shutdown();
}

void
rlXbl::Update()
{
    rlXhttp::Update();
}

bool
rlXbl::HostSession(const int localGamerIndex,
                    const rlNetworkMode netMode,
                    const unsigned maxPubSlots,
                    const unsigned maxPrivSlots,
                    const rlMatchingAttributes& attrs,
                    const unsigned createFlags,
                    const unsigned presenceFlags,
                    void** hSession,
                    u64* arbCookie,
                    rlSessionInfo* sessionInfo,
                    netStatus* status)
{
    bool success = false;

	HostSessionTask* task = NULL;

    rtry
    {
		task = rlGetTaskManager()->CreateTask<HostSessionTask>();

		rverify(task,
                catchall,
                rlError("Error allocating HostSessionTask"));

        rverify(rlTaskBase::Configure(task,
                                    this,
                                    localGamerIndex,
                                    netMode,
                                    maxPubSlots,
                                    maxPrivSlots,
                                    attrs,
                                    createFlags,
                                    presenceFlags,
                                    hSession,
                                    arbCookie,
                                    sessionInfo,
                                    status),
                catchall,
                rlError("Error configuring HostSessionTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::CreateSession(const int localGamerIndex,
                    const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const rlGameType gameType,
                    const unsigned createFlags,
                    const unsigned presenceFlags,
                    const unsigned maxPublicSlots,
                    const unsigned maxPrivateSlots,
                    void** hSession,
                    netStatus* status)
{
    bool success = false;

	CreateSessionTask* task = NULL;

	rtry
    {
		task = rlGetTaskManager()->CreateTask<CreateSessionTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating CreateSessionTask"));

        rverify(rlTaskBase::Configure(task,
                                    this,
                                    localGamerIndex,
                                    &sessionInfo,
                                    netMode,
                                    gameType,
                                    maxPublicSlots,
                                    maxPrivateSlots,
                                    createFlags,
                                    presenceFlags,
                                    hSession,
                                    status),
                catchall,
                rlError("Error configuring CreateSessionTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::DestroySession(const int localGamerIndex,
                        const void* hSession,
                        netStatus* status)
{
    bool success = false;

	DestroySessionTask* task = NULL;

    rtry
    {
        //Destroy can run as a detached task
        if(status)
        {
		    task = rlGetTaskManager()->CreateTask<DestroySessionTask>();
        }
        else
        {
            rlFireAndForgetTask<DestroySessionTask>* taskFireAndForget = NULL;
            rlGetTaskManager()->CreateTask(&taskFireAndForget);
            task = taskFireAndForget;
            status = &taskFireAndForget->m_Status;
        }

		rverify(task,
			    catchall,
			    rlError("Error allocating DestroySessionTask"));

        rverify(rlTaskBase::Configure(task, 
                                      this, 
									  localGamerIndex, 
									  hSession, 
									  status),
                catchall,
                rlError("Error configuring DestroySessionTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::JoinSession(const void* hSession,
                    const rlGamerHandle* joiners,
                    const rlSlotType* slotTypes,
                    const unsigned numJoiners,
                    netStatus* status)
{
    bool success = false;

	JoinSessionTask* task = NULL;

    rtry
    {
		task = rlGetTaskManager()->CreateTask<JoinSessionTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating JoinSessionTask"));

        rverify(rlTaskBase::Configure(task, 
                                      this, 
									  hSession, 
									  joiners, 
									  slotTypes, 
									  numJoiners, 
									  status),
                catchall,
                rlError("Error configuring JoinSessionTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::LeaveSession(const void* hSession,
                    const rlGamerHandle* leavers,
                    const unsigned numLeavers,
                    netStatus* status)
{
    bool success = false;

	LeaveSessionTask* task = NULL;

    rtry
    {
		task = rlGetTaskManager()->CreateTask<LeaveSessionTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating HostSessionTask"));

		rverify(rlTaskBase::Configure(task, 
                                      this, 
									  hSession, 
									  leavers, 
									  numLeavers, 
									  status),
                catchall,
                rlError("Error configuring LeaveSessionTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::ChangeSessionAttributes(const int localGamerIndex,
                                const void* hSession,
                                const rlNetworkMode netMode,
                                const unsigned newMaxPubSlots,
                                const unsigned newMaxPrivSlots,
                                const rlMatchingAttributes& newAttrs,
                                const unsigned createFlags,
                                const unsigned newPresenceFlags,
                                netStatus* status)
{
    bool success = false;

	ChangeAttributesTask* task = NULL;

    rtry
    {
		task = rlGetTaskManager()->CreateTask<ChangeAttributesTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating ChangeAttributesTask"));

		rverify(rlTaskBase::Configure(task,
                                    this,
                                    localGamerIndex,
                                    hSession,
                                    netMode,
                                    newMaxPubSlots,
                                    newMaxPrivSlots,
                                    newAttrs,
                                    createFlags,
                                    newPresenceFlags,
                                    status),
                catchall,
                rlError("Error configuring ChangeAttributesTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::ApplyAttributes(const int localGamerIndex, 
					   const rlMatchingAttributes& attrs, 
					   netStatus* status)
{
	bool success = false;

	ReadWriteAttrsTask* task = NULL;

	rtry
	{
		task = rlGetTaskManager()->CreateTask<ReadWriteAttrsTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating ReadWriteAttrsTask"));

		rverify(rlTaskBase::Configure(task,
                                      this,
                                      localGamerIndex,
                                      attrs,
                                      status),
                catchall,
			    rlError("Error configuring ReadWriteAttrsTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
	}

	return success;
}

bool
rlXbl::MigrateHost(const void* hSession,
                    const int localHostIndex,
                    rlSessionInfo* newSessionInfo,
                    netStatus* status)
{
    bool success = false;

	rlDebug("Migrating Host...");

	MigrateHostTask* task = NULL;

    rtry
    {
        rverify(newSessionInfo, catchall,);

        if(RL_IS_VALID_LOCAL_GAMER_INDEX(localHostIndex))
        {
            newSessionInfo->Clear();
        }
        else
        {
            rverify(newSessionInfo->IsValid() && newSessionInfo->GetHostPeerAddress().IsRemote(),
                catchall,
                rlError("Invalid remote session info"));
        }

		task = rlGetTaskManager()->CreateTask<MigrateHostTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating MigrateHostTask"));

		rverify(rlTaskBase::Configure(task,
                                      this,
                                      hSession,
                                      localHostIndex,
                                      newSessionInfo,
                                      status),
                catchall,
                rlError("Error configuring MigrateHostTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::SendInvites(const int localInviterIndex,
                    const rlGamerHandle* invitees,
                    const unsigned numInvitees,
                    const char* salutation,
                    netStatus* status)
{
    bool success = false;

	SendInvitesTask* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localInviterIndex),catchall,);

		task = rlGetTaskManager()->CreateTask<SendInvitesTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating SendInvitesTask"));

		rverify(rlTaskBase::Configure(task,
                                    this,
                                    localInviterIndex,
                                    invitees,
                                    numInvitees,
                                    salutation,
                                    status),
                catchall,
                rlError("Error configuring SendInvitesTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlXbl::SendPartyInvites(const int localInviterIndex,
                        netStatus* status)
{
    bool success = false;

	SendPartyInvitesTask* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localInviterIndex),catchall,);
		
		task = rlGetTaskManager()->CreateTask<SendPartyInvitesTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating SendPartyInvitesTask"));

        rverify(rlTaskBase::Configure(task, 
                                      this, 
									  localInviterIndex, 
									  status),
                catchall,
                rlError("Error configuring SendPartyInvitesTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool 
rlXbl::GetPartyMembers(rlXblPartyMemberInfo* results,
                       const unsigned maxResults,
                       unsigned* numResults)
{
    rlAssert(results);
    rlAssert(maxResults);
    rlAssert(numResults);

    *numResults = 0;

    XPARTY_USER_LIST userList;
    
    HRESULT hr = XPartyGetUserList(&userList);

    if(hr != XPARTY_E_NOT_IN_PARTY)
    {
        for(int i = 0; i < (int) userList.dwUserCount && *numResults < maxResults; i++)
        {
            const XPARTY_USER_INFO& info = userList.Users[i];

            rlXblPartyMemberInfo& r = results[*numResults];

            r.m_GamerHandle.ResetXbl(info.Xuid);
            safecpy(r.m_GamerName, info.GamerTag, sizeof(r.m_GamerName));
            r.m_TitleId = info.dwTitleId;
            r.m_IsInSession = (info.dwFlags & XPARTY_USER_ISINGAMESESSION) != 0;
            r.m_IsTalking = (info.dwFlags & XPARTY_USER_ISTALKING) != 0;
            r.m_IsInPartyVoice = (info.dwFlags & XPARTY_USER_ISINPARTYVOICE) != 0;
            
            if(r.m_IsInSession)
            {
                CompileTimeAssert(sizeof(r.m_SessionInfo) == sizeof(info.SessionInfo));
                sysMemCpy(&r.m_SessionInfo, &info.SessionInfo, sizeof(r.m_SessionInfo));
            }
            else
            {
                r.m_SessionInfo = rlSessionInfo();
            }

            ++*numResults;
        }
    }

    return true;
}


bool
rlXbl::SetQosEnabled(const rlSessionInfo& sessionInfo,
                    const bool enabled)
{
    CompileTimeAssert(sizeof(XSESSION_INFO) == sizeof(sessionInfo));

    const XSESSION_INFO& xsinfo = (const XSESSION_INFO&) sessionInfo;
    XNKID xnkid = xsinfo.sessionID;
    int err;

    if(enabled)
    {
        err = XNetQosListen(&xnkid,
                            NULL,
                            0,
                            1024 * 16,
                            XNET_QOS_LISTEN_ENABLE | XNET_QOS_LISTEN_SET_BITSPERSEC);
    }
    else
    {
        err = XNetQosListen(&xnkid,
                            NULL,
                            0,
                            0,
                            XNET_QOS_LISTEN_RELEASE);
    }

    if(0 != err)
    {
       rlError("XNetQosListen failed (errcode=%d)", err);
    }

    return (0 == err);
}

bool 
rlXbl::SetQosUserData(const rlSessionInfo& sessionInfo,
                      const u8* data,
                      const u16 size)
{
    rlAssert(!data || (data && size));
    CompileTimeAssert(sizeof(XSESSION_INFO) == sizeof(sessionInfo));

    const XSESSION_INFO& xsinfo = (const XSESSION_INFO&) sessionInfo;
    XNKID xnkid = xsinfo.sessionID;
    int err;

    err = XNetQosListen(&xnkid,
                        data,
                        size,
                        1024 * 16,
                        XNET_QOS_LISTEN_SET_DATA);

    if(0 != err)
    {
       rlError("XNetQosListen failed (errcode=%d)", err);
    }

    return (0 == err);
}

bool 
rlXbl::ShowSocialGetUserTokenUI(const int localGamerIndex,
						  const int socialNetworkId,
						  const char* desiredPermissions,
						  char* tokenBuffer,
						  unsigned tokenBufferSize,
						  netStatus* status)
{
	bool success = false;

    rlAssert(tokenBuffer);

	SocialGetUserTokenTask* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),catchall,);

		task = rlGetTaskManager()->CreateTask<SocialGetUserTokenTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating SocialGetUserTokenTask"));

		rverify(rlTaskBase::Configure(task,
                                      localGamerIndex,
                                      socialNetworkId,
                                      desiredPermissions,
									  true, // Show UI
                                      tokenBuffer,
									  tokenBufferSize,
                                      status),
                catchall,
                rlError("Error configuring SocialGetUserTokenTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool 
rlXbl::SocialGetCachedUserToken(const int localGamerIndex, const int socialNetworkId, const char* desiredPermissions, char* tokenBuffer, unsigned tokenBufferSize, netStatus* status )
{
	bool success = false;

    rlAssert(tokenBuffer);

    SocialGetUserTokenTask* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),catchall,);

		task = rlGetTaskManager()->CreateTask<SocialGetUserTokenTask>();

		rverify(task,
			    catchall,
			    rlError("Error allocating SocialGetUserTokenTask"));

		rverify(rlTaskBase::Configure(task,
                                      localGamerIndex,
                                      socialNetworkId,
                                      desiredPermissions,
									  false, // Don't show UI
                                      tokenBuffer,
									  tokenBufferSize,
                                      status),
                catchall,
                rlError("Error configuring SocialGetUserTokenTask"));

		rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

void 
rlXbl::Cancel(netStatus* status)
{
    // Check that we can cancel this task - this will return true in all cases apart from an 
    // in flight DestroySessionTask. This cannot be cancelled or we might generate an invalid
    // session state (per XDK documentation)
    rlTaskBase* task = rlGetTaskManager()->FindTask(status);
	if(task)
	{
        if(task->IsCancelable())
        {
		    rlGetTaskManager()->CancelTask(status);
        }
        // we have an in flight DestroySessionTask
        else
        {
            // We only get a cancel for this on Shutdown. We cannot leave the status attached to the task
            // as rlSession::Shutdown removes the command (deleting the status).
            // XSessionDelete (called by DestroySessionTask) must be allowed to complete if it has started. 
            // Consequences include no longer being able to attach the player to a presence session. 
            // The calling code no longer cares about the result of the task, so we can detach the status
            // instead and allow the task to complete in the background

            // first, flag the status as cancelled
            if(status->Pending())
            {
                status->SetCanceled();
            }

            // we need to disassociate the task from the status, allowing it to complete
            rlGetTaskManager()->DetachStatus(task);
        }
	}
}

#if __DEV
bool 
rlXbl::ResetAllUsersStats(const unsigned long leaderboardId)
{
	bool success = false;

	XOVERLAPPED xov = {0};
	xov.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	if(rlVerify(xov.hEvent))
	{
		// Reset Stats
		DWORD dwStatus = XUserResetStatsViewAllUsers(leaderboardId, &xov);

		if(ERROR_IO_PENDING == dwStatus)
		{
			WaitForSingleObject(xov.hEvent, INFINITE);
		}

		dwStatus = XGetOverlappedExtendedError(&xov);

		CloseHandle(xov.hEvent);

		success = (ERROR_SUCCESS == dwStatus);

		if(!success)
		{
			rlXovError("XUserResetStatsViewAllUsers", dwStatus, &xov);
		}
	}

	return success;
}
#endif  //__DEV

}   //namespace rage

#endif  //__LIVE
