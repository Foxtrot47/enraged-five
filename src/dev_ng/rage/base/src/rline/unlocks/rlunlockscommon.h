// 
// rline/rlunlockscommon.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUNLOCKSCOMMON_H
#define RLINE_RLUNLOCKSCOMMON_H

namespace rage
{

#define RL_UNLOCKS_BYTES 32

} //namespace rage

#endif  //RLINE_RLUNLOCKSCOMMON_H
