// 
// rline/rlunlocksclient.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlunlocksclient.h"
#include "rlunlockscommon.h"
#include "rlunlockstasks.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"

namespace rage
{

//Override output macros.
RAGE_DECLARE_SUBCHANNEL(rline, unlocks)
#undef __rage_channel
#define __rage_channel rline_unlocks

//------------------------------------------------------------------------------
// rlUnlocksClient
//------------------------------------------------------------------------------
rlUnlocksClient::rlUnlocksClient()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_HasValidUnlocks(false)
, m_Initialized(false)
{
	// NOOP
}

rlUnlocksClient::~rlUnlocksClient()
{
	Shutdown();
}

bool 
rlUnlocksClient::Init(const int localGamerIndex)
{
	m_LocalGamerIndex = localGamerIndex;
	m_State = STATE_WAIT_FOR_ONLINE;
	m_Initialized = true;
	return true;
}

void 
rlUnlocksClient::Shutdown()
{
	if (m_MyStatus.Pending())
	{
		m_UnlocksReadTask.DoCancel();
	}
	m_State = STATE_INVALID;
	m_Initialized = false;
}

void 
rlUnlocksClient::Restart()
{
	m_State = STATE_WAIT_FOR_ONLINE;
	m_HasValidUnlocks = false;
}

void 
rlUnlocksClient::Update(const unsigned timeStep)
{
	switch (m_State)
	{
	case STATE_WAIT_FOR_ONLINE:
		if (rlRos::IsOnline(m_LocalGamerIndex))
		{
			if (m_HasValidUnlocks)
			{
				m_State = STATE_RUNNING;
			}
			else
			{
                if(rlTaskBase::Configure(&m_UnlocksReadTask, m_LocalGamerIndex, m_UnlockBits, sizeof(m_UnlockBits), &m_MyStatus))
				{
					m_UnlocksReadTask.Start();
					m_State = STATE_READING_UNLOCKS;
				}
				else
				{
					rlError("rlUnlocksClient[%d]::Update: Failed to configure UnlocksReadTask", 
						m_LocalGamerIndex);
				}
			}
		}
		break;
	case STATE_READING_UNLOCKS:
		m_UnlocksReadTask.Update(timeStep);
		if (!m_MyStatus.Pending())
		{
			if (m_MyStatus.Succeeded())
			{
				m_HasValidUnlocks = true;
				m_State = STATE_RUNNING;

#if !__NO_OUTPUT
				// Builds a string so that the LSB is on the right.
				char unlockStr[1+RL_UNLOCKS_BYTES*2];
				int binI;
				int strI;
				char nibOne;
				char nibTwo;
				for (binI = RL_UNLOCKS_BYTES-1, strI=0; binI>=0; --binI,strI+=2)
				{
					nibOne = (m_UnlockBits[binI] & 0xF0) >> 4;
					nibTwo = (m_UnlockBits[binI] & 0xF);
					unlockStr[strI] = (nibOne < 10) ? (nibOne + '0') : (nibOne - 10 + 'A');
					unlockStr[strI+1] = (nibTwo < 10) ? (nibTwo + '0') : (nibTwo - 10 + 'A');
				}
				unlockStr[RL_UNLOCKS_BYTES*2]='\0';
				rlDebug("rlUnlocksClient[%d]::Update: Got unlocks: 0x%s",
					m_LocalGamerIndex, unlockStr);
#endif
			}
			else
			{
				rlError("rlUnlocksClient[%d]::Update: Failed to read unlocks, restarting", 
					m_LocalGamerIndex);
				Restart();
			}
		}
		break;
	case STATE_RUNNING:
		// NOOP
		break;
	default:
		rlError("rlUnlocksClient[%d]::Update: Unhandled state %d", m_LocalGamerIndex, m_State);
	}
}

};
