// 
// rline/rlunlocksclient.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUNLOCKSCLIENT_H
#define RLINE_RLUNLOCKSCLIENT_H

#include "rlunlockscommon.h"
#include "rlunlockstasks.h"
#include "rline\rltask.h"
#include "atl\bitset.h"
#include "net\status.h"

namespace rage
{

//PURPOSE
//  Helper class for rlUnlocks that manages the state of a local gamer.
//  This should not be referenced by title code.
class rlUnlocksClient
{
public:
	rlUnlocksClient();
	~rlUnlocksClient();

	bool Init(const int localGamerIndex);
	void Shutdown();
	void Update(const unsigned timeStep);

	void Restart();

	int GetLocalGamerIndex() const { return (!m_Initialized) ? -1 : m_LocalGamerIndex; }
	//PURPOSE
	//  Returns true if the unlock bits are valid for a gamer.
	bool IsUnlocksValid() const { return m_HasValidUnlocks; }
	//PURPOSE
	//  Returns the unlock bits as defined on the backend for a gamer.
	const u8* GetUnlockBits() const { return m_UnlockBits; }

private:
	enum eState
	{
		STATE_INVALID = -1,
		STATE_WAIT_FOR_ONLINE,
		STATE_READING_UNLOCKS,
		STATE_RUNNING
	};

	eState m_State;
	int m_LocalGamerIndex;

	netStatus m_MyStatus;

	rlUnlocksRosReadTask m_UnlocksReadTask;
	u8 m_UnlockBits[RL_UNLOCKS_BYTES];
	bool m_HasValidUnlocks : 1;

	bool m_Initialized  : 1;
};

} //namespace rage

#endif  //RLINE_RLUNLOCKSCLIENT_H
