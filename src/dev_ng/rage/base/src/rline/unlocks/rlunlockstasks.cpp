// 
// rline/rlunlockstasks.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlunlockstasks.h"
#include "rlunlockscommon.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "system/timer.h"
#include "string/string.h"

namespace rage
{

//Override output macros.
RAGE_DECLARE_SUBCHANNEL(rline, unlocks)
#undef __rage_channel
#define __rage_channel rline_unlocks

//////////////////////////////////////////////////////////////////////////
// rlUnlocksRosReadTask
//////////////////////////////////////////////////////////////////////////
rlUnlocksRosReadTask::rlUnlocksRosReadTask()
{
    m_UnlockBits = NULL;
}

bool 
rlUnlocksRosReadTask::Configure(const int localGamerIndex,
                                u8* unlockBits,
                                const size_t unlockBitsLen)
{
    if (!rlVerify(NULL != unlockBits))
    {
        rlTaskError("Invalid arguments: unlockBits");
        return false;
    }

    if (!rlVerify(unlockBitsLen >= RL_UNLOCKS_BYTES))
    {
        rlTaskError("Unlock bits array must be at least %d bytes", RL_UNLOCKS_BYTES);
        return false;
    }

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    rlRosCredentials cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    // NOTE: groupId is hard coded to 0.
    if(!AddStringParameter("ticket", cred.GetTicket(), true) ||
       !AddIntParameter("groupId", 0))
    {
        rlTaskError("Failed to add params");
        return false;
    }


    m_UnlockBits = unlockBits;

    return true;
}

bool 
rlUnlocksRosReadTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* node,
                                    int& /*resultCode*/)
{
    rlAssert(node);

    rtry
    {
        const parTreeNode* unlocksData = node->FindChildWithName("Unlocks");
        rcheck(unlocksData, catchall, 
            rlTaskError("Failed to find <Unlocks> element"));

        //Reset the metadata.
        const char* hexBits = rlHttpTaskHelper::ReadString(unlocksData, "HexBits", NULL);
        rcheck(hexBits, catchall, 
            rlTaskError("Failed to read <HexBits> element"));
        rverify(((2*RL_UNLOCKS_BYTES) > StringLength(hexBits)), catchall, );

        bool firstNibble = true;
        u8 byteIndex = 0;
        u8 nibble;

        sysMemSet(m_UnlockBits, 0, RL_UNLOCKS_BYTES);

        // The MSB of the hexBits string represents the 0 bit of the bit-array.
        // Because of this, the bits will need to be flipped.

        for (u8 i = 0; hexBits[i]; ++i)
        {
            rverify((RL_UNLOCKS_BYTES > byteIndex), catchall, 
                rlTaskError("<HexBits> element invalid format"));

            // Convert a Hexadecimal ASCII character into 
            // its binary nibble equivalent.
            nibble = ((hexBits[i] <= '9') 
                      ? (hexBits[i] - '0') 
                      : (hexBits[i] <= 'F') 
                        ? (hexBits[i] - 'A' + 10) 
                        : (hexBits[i] - 'a' + 10)) & 0xF;

            // Flip the nibble such that the LSB swaps with the MSB.
            // i.e. 0x8 => 0x1, 0x4 => 0x2, etc.
            nibble = ((nibble >> 1) & 0x5) + ((nibble << 1) & 0xA);
            nibble = ((nibble >> 2) & 0x3) + ((nibble << 2) & 0xc);

            if (firstNibble)
            {
                m_UnlockBits[byteIndex] = nibble;
                firstNibble = false;
            }
            else
            {
                m_UnlockBits[byteIndex] |= (nibble << 4);
                firstNibble = true;
                byteIndex++;
            }
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

}   //namespace rage
