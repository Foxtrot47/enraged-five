// 
// rline/rlunlocks.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUNLOCKS_H
#define RLINE_RLUNLOCKS_H

#include "rlunlockscommon.h"
#include "rline\rltask.h"
#include "atl\delegate.h"
#include "net\status.h"

namespace rage
{

//PURPOSE
//  "Unlocks" are unlock bits tracked on the backend.  They cannot be changed
//  from the local system.
//
//USAGE
//  Each title defines the usage of the unlock bits. The bits will need to be
//  be modified via a backend server such as Social Club.
//
//  To integrate unlocks into your title, do the following:
//
//  1. Contact RAGE networking to do any necessary ROS setup to allow your
//     title to use unlocks.
//  
//  2. During initialization, call rlUnlocks::Init().
//  
//  3. Call rlUnlocks::Update() each frame.
//  
//  4. At approrpiate times, call rlUnlocks::IsUnlocked() to check the
//     state of a specific bit.  rlUnlocks::GetUnlockBits() can be called
//     to retrieve the state of all bits (see BIT FORMAT for details).
// 
//  5. Call rlUnlocks::Shutdown() when shutting down networking. 
//    
//BIT FORMAT
//  The result of GetUnlockBits() is formated such that bit 0 is stored in the LSB (bit 0) of byte 0.  
//  The value of bit N is stored in the (N%8) bit of the (N/8) byte.  
//  With those rules the following function will always return true:
//    bool AlwaysTrue(int myIdx)
//    {
//      // Perform the GetUnlockBits call
//      int nCount;
//      const u8* bytes = rlUnlocks::GetUnlockBits(myIdx, &nCount);
//      bool result = true;
//      // Compare bit 0 of the returned byte array with bit 0 byte 0 of the stored byte array.
//      result = result && ((bytes[0] & (1<<0)) == rlUnlocks::IsUnlocked(myIdx, 0));
//      // Compare bit 3 of the returned byte array with bit 3 byte 0 of the stored byte array.
//      result = result && ((bytes[0] & (1<<3)) == rlUnlocks::IsUnlocked(myIdx, 3));
//      // Compare bit 5 of the returned byte array with bit 5 byte 0 of the stored byte array.
//      result = result && ((bytes[0] & (1<<5)) == rlUnlocks::IsUnlocked(myIdx, 5));
//      if (nCount < 1) return result;
//      // Compare bit 10 of the returned byte array with bit 2 byte 1 of the stored byte array.
//      result = result && ((bytes[1] & (1<<2)) == rlUnlocks::IsUnlocked(myIdx, 10));
//      return result;
//    }
//NOTE
//  The database stores the values differently.  Since it is a bitstream, bit 0 will be located
//  at the MSB of byte 0.  One of the benefits of this is that when viewing the database via 
//  SQL Server Manager the left most byte and bit is bit 0.

class rlUnlocks
{
public:
	//PURPOSE
	//  Init/Update/Shutdown
	static bool Init();
	static bool IsInitialized();
	static void Shutdown();
	static void Update();

	//PURPOSE
	//  Returns true if the unlock bits are valid for a gamer.
	static bool IsUnlocksValid(int localGamerIndex);

	//PURPOSE
	//  Returns the unlock bits as defined on the backend for a gamer.
    //  See BIT FORMAT above for details on the format of the result.
	static const u8* GetUnlockBits(int localGamerIndex, int* nCount);

	//PURPOSE
	//  Returns true if the unlock bit is 1 for the given bit for a gamer.
	static bool IsUnlocked(int localGamerIndex, int bitIndex);

private:

	static bool m_Initialized;
};

} //namespace rage

#endif  //RLINE_RLUNLOCKS_H
