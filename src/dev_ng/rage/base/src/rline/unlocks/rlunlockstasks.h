// 
// rline/rlunlockstasks.h
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUNLOCKSTASKS_H
#define RLINE_RLUNLOCKSTASKS_H

#include "rlunlockscommon.h"
#include "atl/bitset.h"
#include "rline/ros/rlroshttptask.h"

namespace rage
{

//------------------------------------------------------------------------------
// rlUnlocksRosReadTask
//------------------------------------------------------------------------------
class rlUnlocksRosReadTask : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlUnlocksRosReadTask);

	rlUnlocksRosReadTask();
	virtual ~rlUnlocksRosReadTask() {};

	bool Configure(const int localGamerIndex,
                    u8* unlockBits,
                    const size_t unlockBitsLen    //Must be at least RL_UNLOCKS_BYTES
                    );

protected:
    virtual const char* GetServiceMethod() const { return "Unlocks.asmx/ReadUnlocks"; }
	virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

private:
	u8* m_UnlockBits;
};

};

#endif //RLINE_RLUNLOCKSTASKS_H