// 
// rline/rlunlocks.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlunlocks.h"
#include "rlunlocksclient.h"
#include "rlunlockscommon.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "system/timer.h"

namespace rage
{

//Override output macros.
RAGE_DECLARE_SUBCHANNEL(rline, unlocks)
#undef __rage_channel
#define __rage_channel rline_unlocks

bool rlUnlocks::m_Initialized;
static rlUnlocksClient m_RlUnlocksClients[RL_MAX_LOCAL_GAMERS];

static netTimeStep s_RlUnlocksTimestep;

bool 
rlUnlocks::Init()
{
    return rlVerifyf(false, "NOT USED!!!");

    /*if(rlVerify(!m_Initialized))
    {
        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            if(!m_RlUnlocksClients[i].Init(i))
            {
                rlError("Failed to init client %d", i);
                return false;
            }
        }

        m_Initialized = true;
    }

    return m_Initialized;*/
}

bool 
rlUnlocks::IsInitialized()
{
    return m_Initialized;
}

void 
rlUnlocks::Shutdown()
{
    if(m_Initialized)
    {
        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            m_RlUnlocksClients[i].Shutdown();
        }

        m_Initialized = false;
    }
}

void 
rlUnlocks::Update()
{
    s_RlUnlocksTimestep.SetTime(sysTimer::GetSystemMsTime());
    if(m_Initialized)
    {
        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            m_RlUnlocksClients[i].Update(s_RlUnlocksTimestep.GetTimeStep());
        }
    }
}

const u8*
rlUnlocks::GetUnlockBits(int localGamerIndex,
                         int* nCount)
{
    if(rlVerifyf(m_Initialized, "Not initialized"))
    {
        rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
        rlAssert(NULL != nCount);
        *nCount = RL_UNLOCKS_BYTES;
        return m_RlUnlocksClients[localGamerIndex].GetUnlockBits();
    }

    return NULL;
}

bool 
rlUnlocks::IsUnlocksValid(int localGamerIndex)
{
    if(rlVerifyf(m_Initialized, "Not initialized"))
    {
        rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
        return m_RlUnlocksClients[localGamerIndex].IsUnlocksValid();
    }

    return false;
}

bool
rlUnlocks::IsUnlocked(int localGamerIndex, int bitIndex)
{
    if(rlVerifyf(m_Initialized, "Not initialized"))
    {
        if (rlVerify(!IsUnlocksValid(localGamerIndex))) return false;

        int bitI = bitIndex % 8;
        int bytI = bitIndex / 8;

        if (rlVerify(bytI < RL_UNLOCKS_BYTES)) return false;

        const u8* pBits = m_RlUnlocksClients[localGamerIndex].GetUnlockBits();

        return (0 != (pBits[bytI] & (1 << bitI)));
    }

    return false;
}

};