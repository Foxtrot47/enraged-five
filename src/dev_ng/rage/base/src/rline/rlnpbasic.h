// 
// rline/rlnpbasic.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNPBASIC_H
#define RLINE_RLNPBASIC_H

#if RSG_NP

#include "atl/array.h"
#include "net/time.h"
#include "rline/facebook/rlfacebookcommon.h"
#include "rline/presence/rlpresencequery.h"
#include "rline/rlnpcommon.h"
#include "rline/rlfriend.h"
#include "rline/rlnpingamemessagemgr.h"
#include "rline/rlnpwebapi.h"
#include "system/criticalsection.h"

#include <np.h>

#define USE_WEBAPI_INVITES 1

// Presence Web API rate limits are 45/15 minutes = 20s intervals
#define PRESENCE_STATUS_RATE_LIMIT_MS 20000
#define PRESENCE_BLOB_RATE_LIMIT_MS 20000
struct SceNpBasicMessageDetails;

namespace rage
{

class rlFriend;
class rlSessionInfo;
class netStatus;
class npBasicAddFriendTask;

class rlNpBasic
{
    friend class npBasicAddFriendTask;
public:
    rlNpBasic();
    ~rlNpBasic();

    bool Init();
    void Update(const int localGamerIndex);
    void Shutdown();

    bool IsInitialized() const;
    bool IsPresenceInitialized() const;

    //Returns true if the friend is on our friends list.
    bool IsFriend(const int localGamerIndex, const rlFriend &f) const;

	//Sends an invitation to a player to become your friend.
	bool AddFriend(const SceNpId& npId, const char* msg, netStatus* status);

    //Sends an invitation to a player to become your friend.
    bool AddFriend(const SceNpOnlineId& npOnlineId, const char* msg, netStatus* status);

    //Send a reliable message to another NP ID.  This is sent via NP Basic,
    //so it can only be up to 512 bytes.  The receiver will be notified 
    //via a "msg received" delegate event.
    //IMPORTANT NOTE: Np limits the rate at which these messages can be sent
    //                to about 4 per minute, so failure almost certainly 
    //                means that messages were being sent too often.
	eNpSendMsgResult SendMsg(const int localGamerIndex, const rlGamerHandle& recipient, const void* data, const unsigned size);
	eNpSendMsgResult SendGuiMsg(const int localGamerIndex, const rlGamerHandle* recipients, const unsigned numRecipients, const char* subject, const char* message);
	eNpSendMsgResult SendMsgToFriend(const int localGamerIndex, const rlFriend& f, const void* data, const unsigned size);
	eNpSendMsgResult SendGeneralMsg(const int localGamerIndex, const rlGamerHandle& recipient, const void* data, const unsigned size);

	eNpSendMsgResult SendInviteMsg(const int localGamerIndex, 
		const rlSceNpSessionId* sessionId, 
		const rlGamerHandle* aInvitees,
		const unsigned nInvitees,
		const rlSessionInfo& sessionInfo, 
		const char* salutation);

	//Show the system UI to accept multiplayer invites.
	bool ShowAcceptMultiplayerInviteUi(const int localGamerIndex) const;

#if RL_FACEBOOK_ENABLED
	//Returns a facebook token on the PS platform for the player given the app id.
	//The work buffer passed in needs to be 8 kb.
	bool GetFacebookAccessToken(const u64 fbAppId, const char* permissions, u32 options, char* token, u64* expiration, netStatus* status);
#endif // RL_FACEBOOK_ENABLED

    //Sets our current presence blob.  Because PSN limits presence updates to only
    //once per minute, we instead cache the data from the last call to this 
    //method, and continue to attempt setting it until PSN allows us.
    //NOTE: The maximum presence data size is SCE_NP_BASIC_MAX_PRESENCE_SIZE (128 bytes).
    bool SetPresenceBlob(const void* blob, const unsigned size);

    //Sets our current presence string.  Because NP limits presence updates to only
    //once per minute, we instead cache the data from the last call to this 
    //method, and continue to attempt setting it until NP allows us.
    //NOTE: The maximum presence string size is SCE_NP_BASIC_PRESENCE_EXTENDED_STATUS_SIZE_MAX
    //(192 bytes).
    bool SetPresenceStatusString(const char* statusStr);

	// Handle a message sent from the Orbis NpInGameMessage library
	void HandleInGameMessage(int32_t libCtxId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom, const SceNpInGameMessageData *pMessage, void *pUserArg);

	// Blocklist
	void RefreshBlockList(int localGamerIndex);
	void ClearBlocklist();
	bool IsPlayerOnBlockList(const rlGamerHandle& player) const;
	void AddPlayerToBlocklist(const rlGamerHandle& player);

	// PS4 Web API Invites
	void UpdateWebApiInvites(const int localGamerIndex);
	void HandleWebApiInviteAccepted(char * invitationId);
	void HandleWebApiJoinSession(const rlSceNpSessionId& sessionId, const rlSceNpAccountId& target, const rlSceNpOnlineId& onlineId, const rlNpGameIntentMemberType memberType);
	void HandleWebApiJoinTarget(const rlSceNpAccountId target, const rlSceNpOnlineId& onlineId);
	void SetInviteListDirty();
	bool IsInviteBeingProcessed() const;
	
	//Types of messages we send internally
    enum NpBasicMsg
    {
        MSG_GENERAL = 0,
    };

private:

    void Reset(const int localGamerIndex);
    void UpdateSetPresence(const int localGamerIndex);

    //NP event handler
    rlNpEventDelegator::Delegate m_NpDlgt;
    void OnNpEvent(rlNp* np, const rlNpEvent* evt);

	u8 m_PresenceBlob[RL_PRESENCE_MAX_BUF_SIZE];
	unsigned m_PresenceBlobSize;
	char m_PresenceStatusStr[SCE_NP_GAME_STATUS_MAX_BUF_SIZE];

	atFixedArray<rlGamerHandle, RL_MAX_BLOCK_LIST> m_UserBlockList;

	// Invitations
	bool m_WebApiInvitesDirty;
	bool m_RefreshingWebApiInvites;
	bool m_WebApiInviteAccepted;
	char m_WebApiAcceptedInvitationId[60];
	netStatus m_WebApiInviteStatus;
	rlNpWebApiInvitationDetail m_InvitationDetails[RL_MAX_WEB_API_INVITES];
	int m_NumWebApiInvitations;

	bool m_WebApiJoinSession;
	bool m_GettingWebApiJoinSessionDetails;
	rlSceNpSessionId m_WebApiJoinSessionId;
	rlSceNpAccountId m_WebApiJoinSessionTarget;
	netStatus m_WebApiJoinSessionStatus;
	rlSessionInfo m_WebApiJoinSessionInfo;
	rlGamerHandle m_WebApiJoinSessionHost;

	bool m_WebApiJoinTarget;
	bool m_GettingWebApiJoinTargetDetails;
	unsigned m_WebApiJoinTargetNumResults;
	rlSessionQueryData m_WebApiJoinTargetQuery;
	rlSceNpAccountId m_WebApiJoinTargetId;
	rlSceNpOnlineId m_WebApiJoinTargetOnlineId;
	netStatus m_WebApiJoinTargetStatus;
	rlNpGameIntentMemberType m_WebApiJoinMemberType;

	// Presence
	u32 m_PresenceBlobLastWriteTime;
	u32 m_PresenceStatusLastWriteTime;

    mutable sysCriticalSectionToken m_Cs;    

    bool m_Initialized                  : 1;
    bool m_PresenceDirty                : 1;
	bool m_PresenceBlobDirty			: 1;
};

}

#endif //RSG_NP

#endif  //RLINE_NPBASIC_H
