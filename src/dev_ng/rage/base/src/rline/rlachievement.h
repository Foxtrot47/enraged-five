// 
// rline/rlachievement.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLACHIEVEMENT_H
#define RLINE_RLACHIEVEMENT_H

#include "net/status.h"
#include "rlgamerinfo.h"
#include "rlworker.h"
#include "system/criticalsection.h"

#include <sys/types.h>  //for time_t

namespace rage
{

class rlAchievement;
class grcImage;

//PURPOSE
//  Contains achievement details.  Achievements are populated by
//  rlAchievement.
class rlAchievementInfo
{
    friend class rlAchievement;

#if RSG_NP
    friend class rlNpTrophy;
#endif

#if RSG_DURANGO
	friend class XblGetAchievementsTask;
#endif

public:

    rlAchievementInfo();

    virtual ~rlAchievementInfo();

    //PURPOSE
    //  Returns the achievement id.
    int GetId() const;

    //PURPOSE
    //  Returns number of cred this achievement is worth.
    int GetCred() const;

    //PURPOSE
    //  Returns the label (name) of the achievement.
    const char* GetLabel() const;

    //PURPOSE
    //  Returns the achievement description.
    const char* GetDescription() const;

    //PURPOSE
    //  Returns true if the achievement has been achieved.
    bool IsAchieved() const;

protected:

    //PURPOSE
    //  Simple RTTI mechanic to maintain backward compatibility.
	virtual int RTTI() const {return 0;}

    //PURPOSE
    //  Clears achievement data buffer.
    void Clear();

    void Reset(const int id,
                const int cred,
                const char* label,
                const char* description,
                const bool achieved);

	void SetAchieved(bool achieved) {m_Achieved = achieved;}

private:

    enum
    {
        //From xbox.h
		// in xbox.h, achievement string lengths are defined as n * sizeof(WCHAR)
        ACH_MAX_LABEL_LEN   = 32 * 2,
        ACH_MAX_DESC_LEN    = 100 * 2,
    };

    int m_Id;
    int m_Cred;
    char m_Label[ACH_MAX_LABEL_LEN];
    char m_Description[ACH_MAX_DESC_LEN];

    bool m_Achieved : 1;
};

class rlAchievementInfoEx : public rlAchievementInfo
{
	friend class rlAchievement;

public:
	enum AchievementType
	{
        //From xbox.h
		ACHIEVEMENT_TYPE_INVALID = 0,
		ACHIEVEMENT_TYPE_COMPLETION = 1,
		ACHIEVEMENT_TYPE_LEVELING = 2,
		ACHIEVEMENT_TYPE_UNLOCK = 3,
		ACHIEVEMENT_TYPE_EVENT = 4,
		ACHIEVEMENT_TYPE_TOURNAMENT = 5,
		ACHIEVEMENT_TYPE_CHECKPOINT = 6,
		ACHIEVEMENT_TYPE_OTHER = 7,
	};

    rlAchievementInfoEx();
    ~rlAchievementInfoEx();

	//PURPOSE
	//  Returns true if this achievement is valid, false otherwise.
	bool IsValid() const;

    //PURPOSE
    //  Returns the type of the achievement.
    AchievementType GetType() const;

	//PURPOSE
    //  Returns the unachieved string of the achievement.
    const char* GetUnachievedString() const;

    //PURPOSE
    //  Returns the achievement's image.
    grcImage* GetImage() const;

	//PURPOSE
    //  Returns whether to display the achievement before it is earned.
    bool ShowUnachieved() const;

	//PURPOSE
    //  Returns !ShowUnachieved().
    bool IsHidden() const;

	//PURPOSE
    //  If the achievement has been awarded, returns the time at which it was awarded, else zero.
	time_t GetAchievedTime() const;

protected:
	virtual int RTTI() const {return 1;}
	void ClearEx();
	void SetTimestamp(time_t timestamp) {m_AchievedTime = timestamp;}
    void ResetEx(const u32 id,
				 const AchievementType type,
				 const u32 cred,
				 const char* label,
				 const char* description,
				 const char* unachievedString,
				 grcImage* image,
				 const bool showUnachieved,
				 const bool isAchieved,
				 const time_t achievedTime);

private:

    enum
    {
        //From xbox.h
		// in xbox.h, achievement string lengths are defined as n * sizeof(WCHAR)
        ACH_MAX_UNACHIEVED_LEN = 100 * 2,
    };

    char m_UnachievedString[ACH_MAX_UNACHIEVED_LEN];
	grcImage* m_Image;
	time_t m_AchievedTime;
	u8 m_Type : 3;
	bool m_ShowUnachieved : 1;
};

//PURPOSE
//  Reads/writes achievements.
class rlAchievement
{
public:

	// flags that specify which details to retrieve about each achievement during a Read() operation
	enum DetailFlags
	{
		ACHIEVEMENT_DETAILS_STRINGS = 1,										// retrieves the string metadata for each achievement
		ACHIEVEMENT_DETAILS_IMAGE = 2,											// retrieves the image metadata
		ACHIEVEMENT_DETAILS_IS_AWARDED = 4,										// retrieves player-specific awarded flag
		ACHIEVEMENT_DETAILS_TIMESTAMP = 8,										// retrieves player-specific awarded timestamp

		ACHIEVEMENT_DETAILS_ALL = ACHIEVEMENT_DETAILS_STRINGS |
								  ACHIEVEMENT_DETAILS_IMAGE | 
								  ACHIEVEMENT_DETAILS_IS_AWARDED |
								  ACHIEVEMENT_DETAILS_TIMESTAMP,
	};

    rlAchievement();

    ~rlAchievement();

    //PARAMS
    //  cpuAffinity     - CPU on which the worker thread will run.
    bool Init(const int cpuAffinity);

	void Update();

    void Shutdown();

    //PURPOSE
    //  Reads a set of achievements (both achieved and not yet achieved)
    //  for the gamer identified by the gamerInfo parameter.  Achievements
    //  are read starting from the first achievable achievement up to the
    //  maximum number of achievements requested.
    //PARAMS
    //  requesterInfo   - Local gamer requesting achievements.
    //  gamerInfo       - Gamer for whom achievements are requested
    //                    (can be remote).
    //  achievements    - Array of achievement instances.
    //  maxAchievements - Maximum number of achievements accommodated by the
    //                    achievements array.
    //  numAchievements - Upon completion will contain the number of achievements
    //                    actually retrieved.
    //  status          - Status object that can be polled for completion.
    //RETURNS
    //  True on successful initiation of the asynchronous operation.
    //NOTES
    //  This is an asynchronous operation and is not complete until the
    //  status object is no longer in a Pending state.
    //  The memory referenced by achievements, numAchievements, and status
    //  must not be deallocated until the operation is complete.
    //  An operation cannot be initiated if rlAchievementManager::Pending()
    //  return true.
    bool Read(const rlGamerInfo& requesterInfo,
			  const rlGamerInfo& gamerInfo,
			  rlAchievementInfo* achievements,
			  const int maxAchievements,
			  int* numAchievements,
			  netStatus* status);

	//PURPOSE
    //  Reads a set of achievements (both achieved and not yet achieved)
    //  for the gamer identified by the gamerInfo parameter.  Achievements
    //  are read starting from the specified achievement id, up to the
    //  maximum number of achievements requested. You can request a single
	//  achievement, a range of achievements, or all achievements this way.
	//  Also, this version only requires the gamerHandle which is useful
	//  when you don't have the gamer's full rlGamerInfo structure.
    //PARAMS
    //  requesterInfo      - Local gamer requesting achievements.
    //  gamerHandle        - Gamer handle for whom achievements are requested
    //                       (can be remote).
	//	detailFlags		   - specifies which details to retrieve
    //  achievements       - Array of achievement instances.
	//  firstAchievementId - Id of the first achievement to retreive.
	//                       If the specified achievement id doesn't exist, 
	//						 it will start at the next valid achievement id.
    //  maxAchievements    - Maximum number of achievements accommodated by the
    //                       achievements array.
    //  numAchievements    - Upon completion will contain the number of achievements
    //                       actually retrieved.
    //  status             - Status object that can be polled for completion.
    //RETURNS
    //  True on successful initiation of the asynchronous operation.
    //NOTES
    //  This is an asynchronous operation and is not complete until the
    //  status object is no longer in a Pending state.
    //  The memory referenced by achievements, numAchievements, and status
    //  must not be deallocated until the operation is complete.
    //  An operation cannot be initiated if rlAchievementManager::Pending()
    //  return true.
    bool Read(const rlGamerInfo& requesterInfo,
			  const rlGamerHandle& gamerHandle,
			  const DetailFlags detailFlags,
			  rlAchievementInfo* achievements,
			  const int firstAchievementId,
			  const int maxAchievements,
			  int* numAchievements,
			  netStatus* status);

	//PURPOSE
    //  Sets the achievement identified by achievementId as "achieved"
    //  by the given local gamer.
    //PARAMS
    //  gamerInfo       - Local gamer who was awarded an achievement.
    //  achievementId   - Id of achievement.
    //  status          - Status object that can be polled for completion.
    //RETURNS
    //  True on successful initiation of the asynchronous operation.
    //NOTES
    //  This is an asynchronous operation and is not complete until the
    //  status object is no longer in a Pending state.
    //  The memory referenced by achievements, numAchievements, and status
    //  must not be deallocated until the operation is complete.
    //  An operation cannot be initiated if rlAchievementManager::Pending()
    //  return true.
    bool Write(const rlGamerInfo& gamerInfo,
                const int achievementId,
                netStatus* status);

    //PURPOSE
    //  Returns true if an operation is pending.
    bool Pending() const;

	// Sync Social Club
	void SyncWithSocialClubRead();
	void SyncWithSocialClubWrite();

#if RSG_PC && __DEV
	//PURPOSE
	//  For testing purposes only.
	//  Erases all awarded achievements for the signed in player.
	//PARAMS
	//  status - Status object that can be polled for completion.
	//RETURNS
	//  True on successful initiation of the asynchronous operation.
	bool DeleteAllAchievements(const rlGamerInfo& gamerInfo,
							   netStatus* status);
#endif

private:
    struct ReadCommand
    {
        rlGamerHandle m_RequestingGamer;
		rlGamerHandle m_TargetGamer;
		DetailFlags m_DetailFlags;
        rlAchievementInfo* m_Achievements;
		int m_FirstAchievementId;
        int m_MaxAchievements;
        int* m_NumAchievements;
		int m_GamerIndex;
		netStatus m_ReadStatus;
    };

    struct WriteCommand
    {
        rlGamerInfo m_GamerInfo;
        int m_AchievementId;
		int m_GamerIndex;
    };

#if RSG_PC && __DEV
	struct DeleteCommand
	{
		rlGamerInfo m_GamerInfo;
	};
#endif

    //Performs async achievement operations.
    static void AchievementWorker(void* p);

    //Platform-specific read/write operations.
    bool NativeRead();
    bool NativeWrite();

#if RSG_PC && __DEV
	bool NativeDelete();
#endif

    union
    {
        u8 m_RC[sizeof(ReadCommand)];
        u8 m_WC[sizeof(WriteCommand)];
#if RSG_PC && __DEV
		u8 m_DC[sizeof(DeleteCommand)];
#endif
	};

    ReadCommand* m_ReadCommand;
    WriteCommand* m_WriteCommand;

#if RSG_PC && __DEV
	DeleteCommand* m_DeleteCommand;
#endif

    struct Worker : public rlWorker
    {
        Worker();

        bool Start(rlAchievement* ach,
                    const int cpuAffinity);

        bool Stop();

    private:

        //Make Start() un-callable
        bool Start(const char* /*name*/,
                    const unsigned /*stackSize*/,
                    const int /*cpuAffinity*/)
        {
			FastAssert(false);
            return false;
        }

        virtual void Perform();

        rlAchievement* m_Ach;
    };

    Worker m_Worker;

    sysCriticalSectionToken m_Cs;
    netStatus* m_Status;
    netStatus m_MyStatus;

    bool m_Initialized  : 1;
};

}   //namespace rage

#endif  //RLINE_RLACHIEVEMENT_H
