// 
// rline/rlNpProfanityCheck.cpp
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include "rlnpprofanitycheck.h"

#include "diag/seh.h"
#include "rline/rlnp.h"

#include <np.h>
#include <libsysmodule.h>

#pragma comment(lib, "libSceNpUtility_stub_weak.a")

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, npprofanitycheck);
#undef __rage_channel
#define __rage_channel rline_npprofanitycheck

//Up to 32 title contexts can be created at one time, however, try to use around 
// one title context wherever possible.
static int s_titleId = 0;

class rlNpProfanityCheckTask : public netTask
{
public:

	NET_TASK_DECL(rlNpProfanityCheckTask);
	NET_TASK_USE_CHANNEL(rline_npprofanitycheck);

	/*
		* Reverse memchr()
		* Find the last occurrence of 'c' in the buffer 's' of size 'n'.
		*/
	void* memrchr(const void *s, int c, size_t n)
	{
		const unsigned char *cp;

		if (n != 0) {
			cp = (unsigned char *)s + n;
			do {
				if (*(--cp) == (unsigned char)c)
					return (void *)cp;
			} while (--n != 0);
		}
		return (void *)0;
	}

	rlNpProfanityCheckTask()
		: m_profaneWord(NULL)
		, m_numProfaneWord(NULL)
	{
		memset(m_testedString, 0, SCE_NP_WORD_FILTER_SANITIZE_COMMENT_MAXLEN);
		memset(m_sanitizedString, 0, SCE_NP_WORD_FILTER_SANITIZE_COMMENT_MAXLEN);
	}

	bool Configure(const int localGamerIndex, const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord)
	{
		if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
			return false;

		m_numProfaneWord  = numProfaneWord;
		*m_numProfaneWord = 0;

		m_profaneWord = profaneWord;
		memset(m_profaneWord, 0, profaneWordBufferSize);
		safecpy(m_testedString, text);

		SceNpId npId;
		int err = sceNpGetNpId(g_rlNp.GetUserServiceId(localGamerIndex), &npId);
		if (err != SCE_OK)
		{
			rlNpError("sceNpGetNpId failed", err);
			return false;
		}

		if (0 == s_titleId)
		{
			int titleId = sceNpWordFilterCreateTitleCtx(&npId);
			if (titleId <= 0)
			{
				rlError("sceNpWordFilterCreateTitleCtx returned %x", titleId);
				return false;
			}

			s_titleId = titleId;
		}

		SceNpWordFilterCreateAsyncRequestParameter param;
		memset(&param, 0, sizeof(param));
		param.size = sizeof(param);
		param.cpuAffinityMask = SCE_KERNEL_CPUMASK_USER_ALL;
		param.threadPriority = SCE_KERNEL_PRIO_FIFO_DEFAULT;

		m_reqId = sceNpWordFilterCreateAsyncRequest(s_titleId, &param);
		if (m_reqId <= 0)
		{
			rlError("sceNpWordFilterCreateAsyncRequest returned %x", m_reqId);
			return false;
		}

		int result = sceNpWordFilterSanitizeComment(m_reqId, m_testedString, m_sanitizedString, NULL);
		if (result < 0)
		{
			rlError("sceNpWordFilterSanitizeComment : Error %x", result);
			return false;
		}
		return true;
	}

	virtual netTaskStatus OnUpdate(int* resultCode)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		int result;
		int ret = sceNpWordFilterPollAsync(m_reqId, &result);
		if (ret < 0)
		{
			rlError("sceNpWordFilterPollAsync : Error %x", ret);
			status = NET_TASKSTATUS_FAILED;
		}

		if (ret == SCE_NP_WORD_FILTER_POLL_ASYNC_RET_FINISHED)
		{
			status = NET_TASKSTATUS_SUCCEEDED;

			int len = strlen(m_sanitizedString);
			// rlVerify if a word was censored with * char.
			for (int i = 0; i < len; ++i)
			{
				// different characters, sanitized string?
				if(m_sanitizedString[i] != m_testedString[i])
				{
					//Search for the first previous space (or start of the string if none is found)
					size_t wordStart = (memrchr(m_testedString, ' ', i)) ? (size_t) memrchr(m_testedString, ' ', i)+1 : (size_t) m_testedString;

					//search for the next space, or the end of the tested string
					size_t wordEnd = (strstr(&m_testedString[i], " ")) ? (size_t)(strstr(&m_testedString[i], " "))+1 : (size_t)m_testedString+strlen(m_testedString);

					//Copy the word in the return buffer
					safecpy(m_profaneWord, (char*)wordStart, wordEnd-wordStart);

					*m_numProfaneWord += 1;
				}
			}
		}

#if !__NO_OUTPUT
		if (status != NET_TASKSTATUS_PENDING)
		{
			rlDebug("Profanity check finished, testedString='%s', sanitizedString='%s', profaneWord='%s'.", m_testedString, m_sanitizedString, m_profaneWord);
		}
#endif //!__NO_OUTPUT

		return status;
	}

private:

	int        m_reqId;
	char       m_testedString[SCE_NP_WORD_FILTER_SANITIZE_COMMENT_MAXLEN];
	char       m_sanitizedString[SCE_NP_WORD_FILTER_SANITIZE_COMMENT_MAXLEN];
	char*      m_profaneWord;
	unsigned*  m_numProfaneWord;
};

bool rlNpProfanityCheck::ProfanityCheck(const int localGamerIndex, const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord, netStatus* status)
{
	bool success = false;
	rlNpProfanityCheckTask* task;
	if(!netTask::Create(&task, status)
		|| !task->Configure(localGamerIndex, text, profaneWord, profaneWordBufferSize, numProfaneWord)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		rlDebug("rlNpProfanityCheck started, testedString='%s'.", text);
		success = true;
	}

	if(!success && status)
	{
		status->SetPending();
		status->SetFailed();
	}

	return success;
}
} // namespace rage

#endif // RSG_ORBIS