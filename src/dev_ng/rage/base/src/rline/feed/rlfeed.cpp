// 
// rline/feed/rlfeed.cpp
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
// 

#include "rlfeed.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "net/task.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/rltitleid.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parser/manager.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, feed)
#undef __rage_channel
#define __rage_channel rline_feed

///////////////////////////////////////////////////////////////////////////////
// rlFeedTask
///////////////////////////////////////////////////////////////////////////////
class rlFeedTask : public rlHttpTaskWithBounceBuffer<rlRosHttpTask>
{
public:
    RL_TASK_DECL(rlFeedTask);
    RL_TASK_USE_CHANNEL(rline_feed);

    rlFeedTask()
        : m_FeedIter(nullptr)
        , m_MaxFeeds(0)
    {
    }

    virtual ~rlFeedTask()
    {
    }

    bool ConfigureBasics(const int localGamerIndex, const rlScLanguage language, rlFeedMessageIterator* iter)
    {
        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlTaskError("Invalid gamer index: %d", localGamerIndex));
            rverify(iter->Begin(), catchall, rlTaskError("Error initializing message iterator"));
            rcheck(rlRosHttpTask::Configure(localGamerIndex, &iter->m_ParserTree), catchall, rlTaskError("Failed to configure base class"));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
            rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

            rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add 'ticket' parameter"));

            const char* langStr = rlScLanguageToString(RLSC_LANGUAGE_ENGLISH);
            if (rlVerifyf(language != RLSC_LANGUAGE_UNKNOWN && language < RLSC_NUM_LANGUAGES, "Unknown language %d", static_cast<int>(language)))
            {
                langStr = rlScLanguageToString(language);
            }

            rverify(AddStringParameter("languageCode", langStr, true), catchall, rlTaskError("Failed to add 'languageCode' parameter"));

            m_FeedIter = iter;

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    bool Configure(const int localGamerIndex,
        const char** tags,
        const unsigned numTags,
        const unsigned offset,
        const unsigned limit,
        const rlScLanguage language,
        rlFeedMessageIterator* iter)
    {

        rtry
        {
            rcheck(ConfigureBasics(localGamerIndex, language, iter), catchall,);
            rverify(BeginListParameter("tagsCsv"), catchall, rlTaskError("Failed to add 'tagsCsv' parameter"));

            if (numTags > 0 && tags)
            {
                for (int i = 0; i < (int)numTags; ++i)
                {
                    rverify(AddListParamStringValue(tags[i]), catchall, rlTaskError("Failed to add tag '%s'", tags[i]));
                }
            }

            rverify(AddUnsParameter("offset", offset), catchall, rlTaskError("Failed to add 'offset' parameter"));
            rverify(AddUnsParameter("limit", limit), catchall, rlTaskError("Failed to add 'limit' parameter"));

            m_MaxFeeds = limit;

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    virtual bool ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
    {
        rlAssert(node);

        rtry
        {
            m_FeedIter->m_NumMessagesRetrieved = 0;

            const parTreeNode* messagesNode = node->FindChildWithName("Result");
            rcheck(messagesNode, catchall,
                rlTaskError("Failed to find <Messages> element"));

            const parAttribute* attr;
            const parElement* el = &messagesNode->GetElement();

            //Number of messages
            attr = el->FindAttribute("c");
            rcheck(attr, catchall, rlTaskError("Failed to find 'Count' attribute"));
            const int msgCount = attr->FindIntValue();

            //Terminal order of the messages.  Only sent if it's non-zero.
            attr = el->FindAttribute("to");
            const int terminalOrder = attr ? attr->FindIntValue() : 0;

            //Total number of messages
            attr = el->FindAttribute("pa");
            rcheck(attr, catchall, rlTaskError("Failed to find 'PostsAvailable' attribute"));
            const bool postsAvailable = attr->FindBoolValue();

            rlTaskDebug2("Received '%d'. TerminalOrder '%d'. Has more posts [%s]", msgCount, terminalOrder, postsAvailable ? "yes" : "no");

            rverify((unsigned)msgCount <= m_MaxFeeds,
                catchall,
                rlTaskError("Message count returned: %d exceeds max requested: %d",
                    msgCount, m_MaxFeeds));

            m_FeedIter->m_TerminalOrder = static_cast<unsigned>(terminalOrder);
            m_FeedIter->m_NumMessagesRetrieved = static_cast<unsigned>(msgCount);
            m_FeedIter->m_MorePostsAvailable = postsAvailable;
        }
        rcatchall
        {
            return false;
        }

        return true;
    }

    void ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
    {
        resultCode = ConvertError(result);
    }

    int ConvertError(const rlRosResult& result) const
    {
        if (result.IsError("NotAllowed", "Privilege")) return static_cast<int>(rlFeedError::NOT_ALLOWED_PRIVILEGE.GetHash());

        rlWarning("Not supported error value [%s.%s]", result.m_Code ? result.m_Code : "null", result.m_CodeEx ? result.m_CodeEx : "null");

        // We hash the Code and set it as resultCode. The feed error text on screen shows a hash number and showing -1 gets bugged by QA.
        return result.m_Code ? atHashString(result.m_Code) : rlFeedError::GENERIC_ERROR;
    }

protected:
    rlFeedMessageIterator* m_FeedIter;
    unsigned m_MaxFeeds;
};

///////////////////////////////////////////////////////////////////////////////
// rlGetFeedTask
///////////////////////////////////////////////////////////////////////////////
class rlGetFeedTask : public rlFeedTask
{
public:
    RL_TASK_DECL(rlGetFeedTask);
    RL_TASK_USE_CHANNEL(rline_feed);

    rlGetFeedTask()
    {
    }

    virtual ~rlGetFeedTask()
    {
    }

    bool Configure(const int localGamerIndex,
        const char** tags,
        const unsigned numTags,
        const unsigned offset,
        const unsigned limit,
        const rlScLanguage language,
        const char* actorTypeFilter,
        rlFeedMessageIterator* iter)
    {
        rtry
        {
            rcheck(rlFeedTask::Configure(localGamerIndex, tags, numTags, offset, limit, language, iter), catchall, );
            rverify(actorTypeFilter && AddStringParameter("actorTypeFilter", actorTypeFilter, true), catchall, rlTaskError("Failed to add 'actorTypeFilter' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Feed.asmx/GetMessages";
    }
};


///////////////////////////////////////////////////////////////////////////////
// rlGetFeedWallTask
///////////////////////////////////////////////////////////////////////////////
class rlGetFeedWallTask : public rlFeedTask
{
public:
    RL_TASK_DECL(rlGetFeedWallTask);
    RL_TASK_USE_CHANNEL(rline_feed);

    rlGetFeedWallTask()
    {
    }

    virtual ~rlGetFeedWallTask()
    {
    }

    bool Configure(const int localGamerIndex,
        const u32 wallType,
        const char* wallOwnerId,
        const char** tags,
        const unsigned numTags,
        const unsigned offset,
        const unsigned limit,
        const rlScLanguage language,
        rlFeedMessageIterator* iter)
    {
        rtry
        {
            rcheck(rlFeedTask::Configure(localGamerIndex, tags, numTags, offset, limit, language, iter), catchall,);
            rverify(AddIntParameter("wallType", wallType), catchall, rlTaskError("Failed to add 'ticket' parameter"));
            rverify(AddStringParameter("wallOwnerId", wallOwnerId, true), catchall, rlTaskError("Failed to add 'ticket' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Feed.asmx/GetWall";
    }
};

///////////////////////////////////////////////////////////////////////////////
// rlGetFeedPostsTask
///////////////////////////////////////////////////////////////////////////////
class rlGetFeedPostsTask : public rlFeedTask
{
public:
    RL_TASK_DECL(rlGetFeedPostsTask);
    RL_TASK_USE_CHANNEL(rline_feed);

    rlGetFeedPostsTask()
    {
    }

    virtual ~rlGetFeedPostsTask()
    {
    }

    bool Configure(const int localGamerIndex,
        const char** posts,
        const unsigned numPosts,
        const rlScLanguage language,
        rlFeedMessageIterator* iter)
    {
        rtry
        {
            rverify(posts != nullptr, catchall, rlTaskError("posts are null"));
            rverify(numPosts > 0, catchall, rlTaskError("numPosts are 0"));

            rcheck(rlFeedTask::ConfigureBasics(localGamerIndex, language, iter), catchall,);
            rverify(BeginListParameter("postIdsCsv"), catchall, rlTaskError("Failed to add 'tagsCsv' parameter"));

            for (unsigned i = 0; i < numPosts; ++i)
            {
                rverify(AddListParamStringValue(posts[i]), catchall, rlTaskError("Failed to add post Id '%s'", posts[i]));
            }

            m_MaxFeeds = numPosts;

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Feed.asmx/GetPostsByPostIdentifiers";
    }
};

///////////////////////////////////////////////////////////////////////////////
// rlAddReactionFeedTask
///////////////////////////////////////////////////////////////////////////////
class rlAddReactionFeedTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlAddReactionFeedTask);
    RL_TASK_USE_CHANNEL(rline_feed);

    rlAddReactionFeedTask() 
    {
    }

    virtual ~rlAddReactionFeedTask()
    {
    }

    bool Configure(const int localGamerIndex,
        const char* messageId,
        const int reactionType,
        const char* rating)
    {
        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlTaskError("Invalid gamer index: %d", localGamerIndex));
            rcheck(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
            rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

            rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add 'ticket' parameter"));

            rverify(AddStringParameter("messageId", messageId, true), catchall, rlTaskError("Failed to add 'messageId' parameter"));
            rverify(AddIntParameter("reactionType", reactionType), catchall, rlTaskError("Failed to add 'reactionType' parameter"));
            rverify(AddStringParameter("rating", rating, true), catchall, rlTaskError("Failed to add 'rating' parameter"));


            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Feed.asmx/AddReaction";
    }
};

///////////////////////////////////////////////////////////////////////////////
// rlClearReactionFeedTask
///////////////////////////////////////////////////////////////////////////////
class rlClearReactionFeedTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlClearReactionFeedTask);
	RL_TASK_USE_CHANNEL(rline_feed);

	rlClearReactionFeedTask()
	{
	}

	virtual ~rlClearReactionFeedTask()
	{
	}

	bool Configure(const int localGamerIndex,
		const char* messageId)
	{
        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlTaskError("Invalid gamer index: %d", localGamerIndex));
            rcheck(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
            rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

            rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add 'ticket' parameter"));

            rverify(AddStringParameter("messageId", messageId, true), catchall, rlTaskError("Failed to add 'messageId' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
	}

	virtual const char* GetServiceMethod() const
	{
		return "Feed.asmx/ClearReaction";
	}
};

///////////////////////////////////////////////////////////////////////////////
// rlDeleteMessagesFeedTask
///////////////////////////////////////////////////////////////////////////////
class rlDeleteMessagesFeedTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlDeleteMessagesFeedTask);
    RL_TASK_USE_CHANNEL(rline_feed);

    rlDeleteMessagesFeedTask()
    {
    }

    virtual ~rlDeleteMessagesFeedTask()
    {
    }

    bool Configure(const int localGamerIndex, const char* messageIdsCsv)
    {
        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlTaskError("Invalid gamer index: %d", localGamerIndex));
            rcheck(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
            rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

            rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add 'ticket' parameter"));

            rverify(AddStringParameter("messageIdsCsv", messageIdsCsv, true), catchall, rlTaskError("Failed to add 'messageIdsCsv' parameter"));

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Feed.asmx/HideMessagesForPlayer";
    }
};

///////////////////////////////////////////////////////////////////////////////
// rlSubmitPresetMessageFeedTask
///////////////////////////////////////////////////////////////////////////////
class rlSubmitPresetMessageFeedTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlSubmitPresetMessageFeedTask);
    RL_TASK_USE_CHANNEL(rline_feed);

    rlSubmitPresetMessageFeedTask()
    {
    }

    virtual ~rlSubmitPresetMessageFeedTask()
    {
    }

    bool Configure(const int localGamerIndex, const int messageType, const int messageSubType)
    {
        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlTaskError("Invalid gamer index: %d", localGamerIndex));
            rcheck(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
            rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

            rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add 'ticket' parameter"));

            rverify(AddIntParameter("presetMessageType", messageType), catchall, rlTaskError("Failed to add 'presetMessageType' parameter [%d]", messageType));
            rverify(AddIntParameter("presetMessageSubType", messageSubType), catchall, rlTaskError("Failed to add 'presetMessageSubType' parameter [%d]", messageSubType));

            //TODO_ANDI: this is currently needed but will be removed
            AddStringParameter("languageCode", rlScLanguageToString(RLSC_LANGUAGE_ENGLISH), true);
            BeginListParameter("tagsCsv");

            return true;
        }
        rcatchall
        {
        }

        return false;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Feed.asmx/SubmitPresetMessage";
    }
};

//////////////////////////////////////////////////////////////////////////
//  rlFeedMessageIterator
//////////////////////////////////////////////////////////////////////////
rlFeedMessageIterator::rlFeedMessageIterator()
    : m_ParserTree(nullptr)
    , m_MsgNode(nullptr)
    , m_NumMessagesRetrieved(0)
    , m_TerminalOrder(0)
    , m_MorePostsAvailable(false)
    , m_MessageIndex(0)
    , m_State(FeedIterState::STATE_NONE)
{
}

rlFeedMessageIterator::~rlFeedMessageIterator()
{
    ReleaseResources();
}

void
rlFeedMessageIterator::ReleaseResources()
{
    if (m_State > FeedIterState::STATE_NONE)
    {
        if (m_ParserTree)
        {
            delete m_ParserTree;
            m_ParserTree = nullptr;
        }

        SHUTDOWN_PARSER;

        m_MsgNode = nullptr;
        m_NumMessagesRetrieved = 0;
        m_TerminalOrder = 0;
        m_MorePostsAvailable = false;
        m_MessageIndex = 0;
        m_State = FeedIterState::STATE_NONE;
    }
}

bool
rlFeedMessageIterator::IsReleased() const
{
    return m_State == FeedIterState::STATE_NONE;
}

bool
rlFeedMessageIterator::NextFeed(const char** feedBody)
{
    if (m_ParserTree)
    {
        if (NextFeed(m_ParserTree->GetRoot(), &m_MsgNode, feedBody))
        {
        #if !__NO_OUTPUT
            // These strings can be long so to avoid spamming we just print a bit. The meat of the content is usually at the beginning so it should be enough for debugging.
            char temp[600] = { 0 };
            strncpy(temp, *feedBody, sizeof(temp) - 1);
            temp[sizeof(temp) - 1] = 0;
            rlDebug2("  FeedMsg %d: '%s'", m_MessageIndex, temp);
        #endif
            ++m_MessageIndex;
            return true;
        }
        else
        {
            ReleaseResources();
        }
    }

    return false;
}

unsigned
rlFeedMessageIterator::GetNumMessagesRetrieved() const
{
    return m_NumMessagesRetrieved;
}

unsigned
rlFeedMessageIterator::GetTerminalOrder() const
{
    return m_TerminalOrder;
}

bool
rlFeedMessageIterator::HasMorePostsAvailabel() const
{
    return m_MorePostsAvailable;
}


#if TEST_FAKE_FEED
bool
rlFeedMessageIterator::SetParserTree(parTree* tree, const unsigned numMessages, const unsigned terminalOrder, const bool morePostsAvailable)
{
    if (tree == nullptr)
    {
        return false;
    }
    
    ReleaseResources();

    INIT_PARSER;

    m_ParserTree = tree;
    
    m_NumMessagesRetrieved = numMessages;
    m_TerminalOrder = terminalOrder;
    m_MorePostsAvailable = morePostsAvailable;

    m_State = FeedIterState::STATE_INITIALIZED;

    return true;
}

bool
rlFeedMessageIterator::GetFeedAtIndex(const unsigned index,
                                      const char** feedBody)
{
    const parTreeNode* messagesNode = m_ParserTree && m_ParserTree->GetRoot() ? m_ParserTree->GetRoot()->FindChildWithName("Result") : nullptr;
    if (!messagesNode)
    {
        return false;
    }

    if (index == 0)
    {
        ReturnToStart();
        return NextFeed(m_ParserTree->GetRoot(), &m_MsgNode, feedBody);
    }

    const parTreeNode* msgNode = messagesNode->FindChildWithIndex(static_cast<int>(index - 1));
    if (!msgNode)
    {
        return false;
    }
    m_MsgNode = msgNode;
    return NextFeed(m_ParserTree->GetRoot(), &m_MsgNode, feedBody);
}

void
rlFeedMessageIterator::ReturnToStart()
{
    rlAssertf(!IsReleased(), "Iterator has already been released");

    m_MessageIndex = 0;
    m_MsgNode = nullptr;
}

#endif //TEST_FAKE_FEED

bool
rlFeedMessageIterator::Begin()
{
    rlAssertf(IsReleased(), "Iterator has not been released - need to call rlInboxMessageIterator::ReleaseResources()");

    ReleaseResources();

    INIT_PARSER;
    m_State = FeedIterState::STATE_INITIALIZED;
    return true;
}

bool
rlFeedMessageIterator::NextFeed(const parTreeNode* rootNode,
                                const parTreeNode** msgNode,
                                const char** feedBody)
{
    rtry
    {
        if (NULL == *msgNode)
        {
            const parTreeNode* messagesNode = rootNode->FindChildWithName("Result");
            rcheck(messagesNode, catchall, rlError("Failed to find <Messages> element"));

            *msgNode = messagesNode->GetChild();
        }
        else
        {
            *msgNode = (*msgNode)->GetSibling();
        }

        if (*msgNode)
        {
            //Feed content
            *feedBody = (*msgNode)->GetData();
            rverify(*feedBody, catchall, rlError("No feed body data"));

            return true;
        }
    }
    rcatchall
    {
    }

    return false;
}


bool
rlFeed::GetFeed(const int localGamerIndex,
    const char** tags,
    const unsigned numTags,
    const unsigned offset,
    const unsigned limit,
    const rlScLanguage language,
    const char* actorTypeFilter,
    rlFeedMessageIterator* iter,
    netStatus* status)
{
    bool success = false;
    rlGetFeedTask* task = nullptr;

    rlDebug2("Retrieving feeds for local gamer: %d...", localGamerIndex);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
        rverify(numTags == 0 || tags != nullptr, catchall, rlError("specificed %d numTags but tags list is NULL", numTags));
        rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
            localGamerIndex,
            tags,
            numTags,
            offset,
            limit,
            language,
            actorTypeFilter,
            iter,
            status),
            catchall,
            rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }
    return success;
}



bool
rlFeed::GetWall(const int localGamerIndex,
    const u32 wallType,
    const char* wallOwnerId,
    const char** tags,
    const unsigned numTags,
    const unsigned offset,
    const unsigned limit,
    const rlScLanguage language,
    rlFeedMessageIterator* iter,
    netStatus* status)
{
    bool success = false;
    rlGetFeedWallTask* task = nullptr;

    rlDebug2("Retrieving wall local gamer [%d] for wall owner[%s]", localGamerIndex, wallOwnerId);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
        rverify(numTags == 0 || tags != nullptr, catchall, rlError("specificed %d numTags but tags list is NULL", numTags));
        rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
            localGamerIndex,
            wallType,
            wallOwnerId,
            tags,
            numTags,
            offset,
            limit,
            language,
            iter,
            status),
            catchall,
            rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }
    return success;
}

bool
rlFeed::GetFeedPosts(const int localGamerIndex,
    const char** posts,
    const unsigned numPosts,
    const rlScLanguage language,
    rlFeedMessageIterator* iter,
    netStatus* status)
{
    bool success = false;
    rlGetFeedPostsTask* task = nullptr;

    rlDebug2("Retrieving [%u] posts for local gamer [%d]", numPosts, localGamerIndex);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
        rverify(posts != nullptr, catchall, rlError("posts is nullptr"));
        rverify(numPosts > 0, catchall, rlError("numPosts is 0"));
        rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
            localGamerIndex,
            posts,
            numPosts,
            language,
            iter,
            status),
            catchall,
            rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlFeed::AddReaction(const int localGamerIndex,
    const char* messageId,
    const int reactionType,
    const char* rating,
    netStatus* status)
{
    bool success = false;
    rlAddReactionFeedTask* task = nullptr;

    rlDebug2("Local gamer [%d] reacts to feed [%s] with [%d]", localGamerIndex, messageId, reactionType);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
        rverify(messageId != nullptr, catchall, rlError("messageId is nullptr"));
        rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
            localGamerIndex,
            messageId,
            reactionType,
            rating,
            status),
            catchall,
            rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }
    return success;
}

bool
rlFeed::ClearReaction(const int localGamerIndex,
	const char* messageId,
	netStatus* status)
{
	bool success = false;
	rlClearReactionFeedTask* task = nullptr;

	rlDebug2("Local gamer [%d] clears to feed reaction [%s]", localGamerIndex, messageId);

	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
		rverify(messageId != nullptr, catchall, rlError("messageId is nullptr"));
		rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

		rverify(rlTaskBase::Configure(task,
			localGamerIndex,
			messageId,
			status),
			catchall,
			rlError("Error configuring task"));

		rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

		success = true;
	}
	rcatchall
	{
		if (task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}
	return success;
}

bool
rlFeed::DeleteFeeds(const int localGamerIndex,
    const char* messageIdsCsv,
    netStatus* status)
{
    bool success = false;
    rlDeleteMessagesFeedTask* task = nullptr;

    rlDebug2("Local gamer [%d] deletes feeds [%s]", localGamerIndex, messageIdsCsv);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
        rverify(messageIdsCsv != nullptr, catchall, rlError("messageId is nullptr"));
        rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
            localGamerIndex,
            messageIdsCsv,
            status),
            catchall,
            rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }
    return success;
}

bool
rlFeed::SubmitPresetMessage(const int localGamerIndex, 
                            const int messageType,
                            const int messageSubType,
                            netStatus* status)
{
    bool success = false;
    rlSubmitPresetMessageFeedTask* task = nullptr;

    rlDebug2("Local gamer [%d] submits preset message. type[%d] subType[%d]", localGamerIndex, messageType, messageSubType);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
        rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
            localGamerIndex,
            messageType,
            messageSubType,
            status),
            catchall,
            rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }
    return success;
}

void
rlFeed::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

}
