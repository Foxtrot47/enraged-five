// 
// rline/feed/rlfeed.h
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
// 

#ifndef RLINE_RLFEED_H
#define RLINE_RLFEED_H

#define TEST_FAKE_FEED !__FINAL

#if TEST_FAKE_FEED
#define TEST_FAKE_FEED_ONLY(...) __VA_ARGS__
#else
#define TEST_FAKE_FEED_ONLY(...)
#endif

#include "rline/socialclub/rlsocialclubcommon.h"
#include "atl/hashstring.h"

namespace rage
{

class netStatus;
class parTree;
class parTreeNode;

enum rlAvatarImageSize
{
    RL_AVATAR_IMAGE_SIZE_SMALL,
    RL_AVATAR_IMAGE_SIZE_NORMAL,
    RL_AVATAR_IMAGE_SIZE_LARGE,

    RL_AVATAR_IMAGE_NUM_SIZES
};

namespace rlFeedError
{
    const atHashString GENERIC_ERROR = ATSTRINGHASH("feed_error_generic", 0xE1C8A52D);
    const atHashString NOT_ALLOWED_PRIVILEGE = ATSTRINGHASH("NotAllowed.Privilege", 0x22384BFC);
}

class rlFeedMessageIterator
{
    friend class rlFeedTask;
    friend class rlGetFeedTask;
    friend class rlGetFeedWallTask;

public:
    rlFeedMessageIterator();
    ~rlFeedMessageIterator();

    //PURPOSE
    //  Releases memory used by the iterator.
    //  Be sure to call this when finished with the iterator.
    void ReleaseResources();

    //PURPOSE
    //  Returns true when resources have been released
    bool IsReleased() const;

    //PURPOSE
    //  Retrieves the next message in the iteration
    //PARAMS
    //  feedBody    - The json data of the feed post
    //RETURNS
    //  True if a message was retrieved, false if at the end of the iteration.
    //NOTES
    //  At the end of the iteration RelaseResources() will be called
    //  to free resources.
    bool NextFeed(const char** feedBody);

    //PURPOSE
    //  Returns the number of messages retrieved by GetMessages()
    unsigned GetNumMessagesRetrieved() const;

    //PURPOSE
    //  Returns the server-index of the last message from this read request. The next read should be from m_TerminalOrder + 1
    unsigned GetTerminalOrder() const;

    //PURPOSE
    //  If true there should be more messages on the server from GetTerminalOrder onwards.
    //  It's a should because things may have changed since the last read
    bool HasMorePostsAvailabel() const;

#if TEST_FAKE_FEED
    //PURPOSE
    // Mainly used for debug reasons to try out custom made data
    bool SetParserTree(parTree* tree, const unsigned numMessages, const unsigned terminalOrder, const bool morePostsAvailable);

    //PURPOSE
    // Return a feed by index for debugging purpose
    bool GetFeedAtIndex(const unsigned index,
        const char** feedBody);

    //PURPOSE
    // Sets the iterator back to the beginning so the next NextFeed call returns the first item.
    // Works only if we hadn't gone beyond the end of the list which deletes the parser tree.
    void ReturnToStart();
#endif //TEST_FAKE_FEED

private:
    enum class FeedIterState
    {
        STATE_NONE,
        STATE_INITIALIZED   //Set when INIT_PARSER is called
    };

    //PURPOSE
    // Initializes the iterator
    bool Begin();

    //PURPOSE
    // Parses the xml part of the next feed and returns the data. See the public NextFeed for a parameter desc
    static bool NextFeed(const parTreeNode* rootNode,
        const parTreeNode** msgNode,
        const char** feedBody);

    parTree* m_ParserTree;
    const parTreeNode* m_MsgNode;

    unsigned m_NumMessagesRetrieved; // The number of messages in this read request
    unsigned m_TerminalOrder; // The server-index of the last message from this read request. The next read should be from m_TerminalOrder onwards
    bool m_MorePostsAvailable; // If true there should be more messages on the server from m_TerminalOrder + 1

    int m_MessageIndex;

    FeedIterState m_State;
};

class rlFeed
{
public:

    //PURPOSE
    //  Retrieves the feed list metadata for the user.
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose feed will be read
    //  tags                - tags to filter on (NULL for none)
    //  numTags             - number of tags in 'tags' list
    //  offset              - Feed message offset into the feeds.  Messages are
    //                        ordered from most recent to least recent.
    //                        The most recent message is at offset zero.
    //  limit               - Max number of feeds to read.
    //  language            - The language of the game/feeds
    //  actorTypeFilter     - The actor filter (All, Friends, Crew, Me)
    //  iter                - Upon completion will be initialized as a feed message iterator
    //  status              - Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate iter
    //  or status until the operation completes.
    //
    //  Be sure to call ReleaseResources() on the iterator when finished.
    static bool GetFeed(const int localGamerIndex,
        const char** tags,
        const unsigned numTags,
        const unsigned offset,
        const unsigned limit,
        const rlScLanguage language,
        const char* actorTypeFilter,
        rlFeedMessageIterator* iter,
        netStatus* status);

    //PURPOSE
    //  Retrieves the feed wall for the user.
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose feed will be read
    //  wallType            - This is an enum defined on the server as InboxRecipientKeyType. It defines what the wallOwnerId is (crewId, rockstarId ...)
    //  wallOwnerId         - The id of the user (the rockstar id for the 'rockstar id'-wall)
    //  tags                - tags to filter on (NULL for none)
    //  numTags             - number of tags in 'tags' list
    //  offset              - Feed message offset into the feeds.  Messages are
    //                        ordered from most recent to least recent.
    //                        The most recent message is at offset zero.
    //  limit               - Max number of feeds to read.
    //  iter                - Upon completion will be initialized as a feed message iterator
    //  status              - Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate iter
    //  or status until the operation completes.
    //
    //  Be sure to call ReleaseResources() on the iterator when finished.
    static bool GetWall(const int localGamerIndex,
        const u32 wallType,
        const char* wallOwnerId,
        const char** tags,
        const unsigned numTags,
        const unsigned offset,
        const unsigned limit,
        const rlScLanguage language,
        rlFeedMessageIterator* iter,
        netStatus* status);

    //PURPOSE
    //  Retrieves a list of posts.
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose feed will be read
    //  posts               - The Ids of posts to retrieve
    //  numPosts            - number of pots to retrieve
    //  iter                - Upon completion will be initialized as a feed message iterator
    //  status              - Can be polled for completion.
    static bool GetFeedPosts(const int localGamerIndex,
        const char** posts,
        const unsigned numPosts,
        const rlScLanguage language,
        rlFeedMessageIterator* iter,
        netStatus* status);

    //PURPOSE
    //  Like or dislike a feed
    //PARAMS
    //  localGamerIndex    - Local index of gamer whose doing the reaction.
    //  messageIds         - The id of the message to react to
    //  reactionType       - Unknown ==0, Like == 1, Dislike == 2
    //  rating             - float in a string from 0.0 to 1.0
    //  status             - Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate status until the operation completes.
    static bool AddReaction(const int localGamerIndex,
        const char* messageId,
        const int reactionType,
        const char* rating,
        netStatus* status);

	//PURPOSE
	//  Clear your like/dislike
	//PARAMS
	//  localGamerIndex    - Local index of gamer whose doing the reaction.
	//  messageIds         - The id of the message to react to
	//  status             - Can be polled for completion.
	//NOTES
	//  This is an asynchronous operation.  Do not deallocate status until the operation completes.
	static bool ClearReaction(const int localGamerIndex,
		const char* messageId,
		netStatus* status);

    //PURPOSE
    //  Delete a list of feeds
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose doing the reaction.
    //  messageIdsCsv       - The comma separated list of IDs to delete.
    //  status              - Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate status until the operation completes.
    static bool DeleteFeeds(const int localGamerIndex,
        const char* messageIdsCsv,
        netStatus* status);

    //PURPOSE
    //  Generates a feed post from a predefined set of posts. Currently this is used for challenges and missions
    //  but it can be used for any set of messages where client-side triggering is not too risky.
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose creating the post
    //  messageType         - The type of the message. Every type will map to a specific feed type.
    //  messageSubType      - Assuming the type is 'mission' the subtype would indicate which mission it is.
    //  status              - Can be polled for completion.
    static bool SubmitPresetMessage(const int localGamerIndex,
        const int messageType,
        const int messageSubType,
        netStatus* status);


    //PURPOSE
    //  Cancels a pending operation.
    static void Cancel(netStatus* status);
};


} //namespace rage

#endif  //RLINE_RLFEED_H
