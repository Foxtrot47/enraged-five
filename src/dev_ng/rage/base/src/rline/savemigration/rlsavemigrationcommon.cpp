// 
// rline/savemigration/rlsavemigrationcommon.cpp
// 
// Copyright (C) 2021 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsavemigrationcommon.h"

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rline, savemigration);
}