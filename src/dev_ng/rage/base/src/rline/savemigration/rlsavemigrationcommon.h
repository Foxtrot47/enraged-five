// 
// rline/savemigration/rlsavemigrationcommon.h 
// 
// Copyright (C) 2021 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_SAVEMIGRATION_COMMON_H
#define RLINE_SAVEMIGRATION_COMMON_H

#include "diag/channel.h"
#include "atl/array.h"
#include "atl/string.h"
#include "rline/rldiag.h"

namespace rage
{
    RAGE_DECLARE_SUBCHANNEL(rline, savemigration);

    #define rlsmAssert(cond)					RAGE_ASSERT(rline_savemigration,cond)
    #define rlsmAssertf(cond,fmt,...)			RAGE_ASSERTF(rline_savemigration,cond,fmt,##__VA_ARGS__)
    #define rlsmVerifyf(cond,fmt,...)			RAGE_VERIFYF(rline_savemigration,cond,fmt,##__VA_ARGS__)
    #define rlsmErrorf(fmt,...)					RAGE_ERRORF(rline_savemigration,fmt,##__VA_ARGS__)
    #define rlsmCWarningf(cond,fmt,...)			RAGE_CONDLOGF(!(cond),rline_savemigration,DIAG_SEVERITY_WARNING,fmt,##__VA_ARGS__)
    #define rlsmCErrorf(cond,fmt,...)			RAGE_CONDLOGF(!(cond),rline_savemigration,DIAG_SEVERITY_ERROR,fmt,##__VA_ARGS__)
    #define rlsmWarningf(fmt,...)				RAGE_WARNINGF(rline_savemigration,fmt,##__VA_ARGS__)
    #define rlsmDisplayf(fmt,...)				RAGE_DISPLAYF(rline_savemigration,fmt,##__VA_ARGS__)
    #define rlsmDebugf1(fmt,...)				RAGE_DEBUGF1(rline_savemigration,fmt,##__VA_ARGS__)
    #define rlsmDebugf2(fmt,...)				RAGE_DEBUGF2(rline_savemigration,fmt,##__VA_ARGS__)
    #define rlsmDebugf3(fmt,...)				RAGE_DEBUGF3(rline_savemigration,fmt,##__VA_ARGS__)
    #define rlsmLogf(severity,fmt,...)			RAGE_LOGF(rline_savemigration,severity,fmt,##__VA_ARGS__)
    #define rlsmCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,rline_savemigration,severity,fmt,##__VA_ARGS__)

    #define SAVEMIGRATION_ERROR_CASE(code, codeEx, resCode) \
            if(result.IsError(code, codeEx)) \
            { \
                resultCode = resCode; \
                return; \
            }

    #define SAVEMIGRATION_STATE_CASE(code, resCode) \
            if (stricmp(state, code) == 0) \
            { \
                m_StateId = resCode; \
                return; \
            }

    static const int RLSM_INVALID_ACCOUNT_ID = 0;
    static const int RLSM_INVALID_TRANSACTION_ID = 0;

    //String error codes
    static const char* RLSM_STATE_CODE_FINISHED   = "Finished";
    static const char* RLSM_STATE_CODE_CANCELED   = "Canceled";
    static const char* RLSM_STATE_CODE_ROLLEDBACK = "RolledBack";
    static const char* RLSM_STATE_CODE_ERROR      = "Error";

    //Maximum length of a save migration attributes
    const static int RLSM_MAX_STATS_NAME_CHARS = 64;
    const static int RLSM_MAX_STATS_VALUE_CHARS = 64;
    const static int RLSM_MAX_PLATFORM_CHARS = 32;
    const static int RLSM_MAX_GAMERTAG_CHARS = 64;
    const static int RLSM_MAX_GAMERHANDLE_CHARS = 256;
    const static int RLSM_MAX_ERROR_CODE = 64;
    const static int RLSM_MAX_SAVE_STATE_CHAR = 32;

    //Maximum number of multiplayer accounts a user can have available
    const static int RLSM_MAX_MP_ACCOUNTS = 5;

    //Maximum number of stats that can be returned for each account
    const static int RLSM_MAX_AVAILABLE_STATS = 32;

    //PURPOSE
    //  Save migration error codes
    enum rlSaveMigrationErrorCode
    {
        RLSM_ERROR_UNKNOWN = -1,
        RLSM_ERROR_NONE = 0,
        // The authentication ticket is no longer valid, and the player must re-authenticate and start over
        RLSM_ERROR_AUTHENTICATION_FAILED = 1,
        // Transfers are currently disabled for maintenance
        RLSM_ERROR_MAINTENANCE = 2,
        // The account attempting to be transferred is not currently linked to the same rockstar account as 
        // the account that is attempting to initiate the transfer. This would usually indicate that the 
        // account link was changed by the user in between the call to GetSourceAccountsMP and MigrateAccountMP
        RLSM_ERROR_DOES_NOT_MATCH,
		// The platform account is not linked to a Rockstar account.
		RLSM_ERROR_ROCKSTAR_ACCOUNT_REQUIRED,
		// Title/Platform is not a valid source for save.
		RLSM_ERROR_INVALID_TITLE,
		// Save Migration is disabled.
		RLSM_ERROR_DISABLED,
		// A migration is already in progress from the player account making the request (The file has been
		// downloaded to the destination client, but the process not marked as complete).
		RLSM_ERROR_IN_PROGRESS,
		// A migration has already been completed from the player account making the request.
		RLSM_ERROR_ALREADY_COMPLETE,
		// FileData is missing or invalid.
		RLSM_ERROR_INVALID_FILEDATA,
		// Missing parameter
		RLSM_ERROR_INVALID_PARAMETER,
		// File storage is unavailable.
		RLSM_ERROR_FILESTORAGE_UNAVAILABLE,
		// Invalid record token
		RLSM_ERROR_INVALID_TOKEN,
		// Expired record token
		RLSM_ERROR_EXPIRED_TOKEN,
		// The save Migration has expired 
		RLSM_ERROR_EXPIRED_SAVE_MIGRATION,
		// The player does not have a save migration in progress to complete 
		RLSM_ERROR_NOT_IN_PROGRESS,
		// No migration records exist
		RLSM_ERROR_NO_MIGRATION_RECORDS,

        //End of errors
        RLSM_ERROR_END
    };

    //PURPOSE
    //  Save migration states
    enum rlSaveMigrationStateCode
    {
        RLSM_STATE_INVALID = 0,
        // The transfer is in progress
        RLSM_STATE_INPROGRESS = 1,
        // The transfer completed successfully
        RLSM_STATE_FINISHED = 2,
        // The transfer was canceled/not completed
        RLSM_STATE_CANCELED = 3,
        // The transfer was undone/rolled back
        RLSM_STATE_ROLLEDBACK = 4,
        // It is not safe to proceed into MP, as the account is currently in a corrupt state
        RLSM_STATE_ERROR = 5,

        //End of states
        RLSM_STATE_END
    };

    //------------------------------------------------------------------------------
    // rlSaveMigrationState
    //
    // Describes information about a save migration status
    //------------------------------------------------------------------------------
    struct rlSaveMigrationState
    {
    public:
        rlSaveMigrationState()
        {
            Reset();
        }

        const char*         GetState() const { return m_State; }
        u32               GetStateId() const { return m_StateId; }
        bool              InProgress() const { return m_InProgress; }
        int         GetTransactionId() const { return m_TransactionId; }

    public:
        void Reset()
        {
            Set(RLSM_INVALID_TRANSACTION_ID, false, NULL);
        }
        void Set(const int transactionId, const bool inProgress, const char* state)
        {
            m_State[0] = '\0';
            m_StateId = RLSM_STATE_INVALID;

            m_TransactionId = RLSM_INVALID_TRANSACTION_ID;
            if (transactionId > RLSM_INVALID_TRANSACTION_ID)
                m_TransactionId = transactionId;

            m_InProgress = inProgress;
            if (m_InProgress)
            {
                m_StateId = RLSM_STATE_INPROGRESS;
            }
            else if (state)
            {
                safecpy(m_State, state);
                SAVEMIGRATION_STATE_CASE("Finished",    RLSM_STATE_FINISHED);
                SAVEMIGRATION_STATE_CASE("Canceled",    RLSM_STATE_CANCELED);
                SAVEMIGRATION_STATE_CASE("RolledBack",  RLSM_STATE_ROLLEDBACK);
                SAVEMIGRATION_STATE_CASE("Error",       RLSM_STATE_ERROR);
            }
        }

    private:
        bool m_InProgress;
        int  m_TransactionId;
        u32  m_StateId;
        char m_State[RLSM_MAX_SAVE_STATE_CHAR];
    };

    //------------------------------------------------------------------------------
    // rlSaveMigrationMPStat
    //
    // Describes information about a single stat
    // Stats can be used to display information such as XP, rank, etc... to the user.
    //------------------------------------------------------------------------------
    struct rlSaveMigrationMPStat
    {
        char m_Name[RLSM_MAX_STATS_NAME_CHARS];
        char m_Value[RLSM_MAX_STATS_VALUE_CHARS];

        rlSaveMigrationMPStat()
        {
            m_Name[0] = '\0';
            m_Value[0] = '\0';
        }
    };
    typedef atFixedArray<rlSaveMigrationMPStat, RLSM_MAX_AVAILABLE_STATS> rlSaveMigrationMPStatsArray;

    //------------------------------------------------------------------------------
    // rlSaveMigrationMPAccount
    //
    // Describes information about a multiplayer account
    //------------------------------------------------------------------------------
    struct rlSaveMigrationMPAccount
    {
        // Their account id
        int m_AccountId;
        // The platform id
        char m_Platform[RLSM_MAX_PLATFORM_CHARS];
        // Their gamertag
        char m_Gamertag[RLSM_MAX_GAMERTAG_CHARS];
        // Their gamerhandle (this has to be cross-platform, so can't use
        // RL_MAX_GAMER_HANDLE_CHARS
        char m_GamerHandle[RLSM_MAX_GAMERHANDLE_CHARS];
        // The error code if the save isn't available
        //  ERROR_ALREADY_DONE - The account has already been transferred, and thus not eligible to transfer again
        //	ERROR_NOT_AVAILABLE - The account doesn't have any MP characters/progress, and thus can't be transferred
        char m_ErrorCode[RLSM_MAX_ERROR_CODE];
		// The number of gta5 missions that this account has published
		int m_ugcPublishCount;
        // Whether or not this save is available to transfer
        bool m_Available;
        // Stats returned for this save
        rlSaveMigrationMPStatsArray m_Stats;

        rlSaveMigrationMPAccount()
            : m_Stats()
        {
            m_AccountId = RLSM_INVALID_ACCOUNT_ID;
            m_Platform[0] = '\0';
            m_Gamertag[0] = '\0';
            m_GamerHandle[0] = '\0';
			m_ugcPublishCount = -1;
            m_Available = false;
            m_ErrorCode[0] = '\0';
        }
    };
    typedef atFixedArray<rlSaveMigrationMPAccount, RLSM_MAX_MP_ACCOUNTS> rlSaveMigrationMPAccountsArray;

	//------------------------------------------------------------------------------
	// rlSaveMigrationRecordMetadata
	//
	// Describes information about a Single Player save metadata
	//------------------------------------------------------------------------------
	struct rlSaveMigrationRecordMetadata
	{
	public:
		atString m_Token;
		atString m_PlayerDisplayName;
		float m_CompletionPercentage;
		u32 m_LastCompletedMissionId;
		u32 m_SavePosixTime;
		u32 m_UploadPosixTime;
		int m_RockstarId;
		int m_AccountId;
		int m_PlatformId;

		rlSaveMigrationRecordMetadata()
			: m_RockstarId(-1)
			, m_AccountId(-1)
			, m_PlatformId(-1)
			, m_CompletionPercentage(0.0f)
			, m_LastCompletedMissionId(0)
			, m_SavePosixTime(0)
			, m_UploadPosixTime(0)
		{
		}

		//PURPOSE
		// For the record to be valid the token must be set.
		bool IsValid() const { return !m_Token.IsNull(); }

		void Clear()
		{
			m_Token.Clear();
			m_PlayerDisplayName.Clear();
			m_RockstarId = -1;
			m_AccountId = -1;
			m_PlatformId = -1;
			m_CompletionPercentage = 0.0f;
			m_LastCompletedMissionId = 0;
			m_SavePosixTime = 0;
			m_UploadPosixTime = 0;
		}
	};

}

#endif // RLINE_SAVEMIGRATION_COMMON_H
