// 
// rline/savemigration/rlsavemigration.h 
// 
// Copyright (C) 2021 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_SAVEMIGRATION_H
#define RLINE_SAVEMIGRATION_H

#include "rlsavemigrationcommon.h"
#include "rlsavemigrationtasks.h"

namespace rage
{
	class netStatus;

	//------------------------------------------------------------------------------
	// rlSaveMigration
	//------------------------------------------------------------------------------
	class rlSaveMigration
	{
	public:

		//PURPOSE
		//	Fills in a supplied array of saves that are available for the user multiplayer accounts
		static bool GetMPAccounts(const int localGamerIndex, rlSaveMigrationMPAccountsArray* accounts, netStatus* status);

		//PURPOSE
		//  Migrates a multiplayer account to the current platform from sourcePlatform using the account id
		static bool  MigrateMPAccount(const int localGamerIndex, const int sourceAccountid, const char* sourcePlatform, rlSaveMigrationState* state, netStatus* status);

		//PURPOSE
		//  Given a transaction id previously returned from the initiation of a save migration, returns the state of it
		static bool GetTransactionState(const int localGamerIndex, const int transactionId, rlSaveMigrationState* state, netStatus* status);

		//PURPOSE
		// Get the current status of the users save migration
		//
		// Multiplayer:
		//
		// Should be called by the game client before entering into multiplayer, on both gen8 and gen9 consoles, so that a user doesn't enter into MP while a transfer 
		// is in progress for them, as this would cause them to stomp on the changes being made, potentially corrupting the account.
		//
		// This is also necessary to check before entering MP, as the user may not always be the one initiating the transfer, due to e.g.an administrator initiating it.
		//
		// This should be called to prevent both the sourceand the destination accounts involved in the transferred from entering into MP while it is still in progress.
		static bool GetState(const int localGamerIndex, rlSaveMigrationState* state, netStatus* status);

		//PURPOSE
		//  Determine whether the player is eligible to upload a save
		//PARAMS
		//	localGamerIndex - Local gamer index.
		//  recordMetadata  - Struct that holds the metadata if the Migration not completed by the player from this platform, but a save has been previously uploaded.
		//  status          - Can be polled for completion.
		static bool GetSingleplayerSaveState(const int localGamerIndex, rlSaveMigrationRecordMetadata* recordMetadata, netStatus* status);

		//PURPOSE
		//  Upload a Single Player save.
		//PARAMS
		//	localGamerIndex			- Local gamer index.
		//	savePosixTime 			- Posix time for when the save was made
		//	completionPercentage 	- Game percentage complete
		//	lastCompletedMissionId 	- Identifier for the last completed mission
		//  data                    - Savegame payload.
		//  size                    - Size of Savegame payload.
		//  status                  - Can be polled for completion.
		static bool UploadSingleplayerSave(const int localGamerIndex
											, const u32 savePosixTime
											, const float completionPercentage
											, const u32 lastCompletedMissionId
											, const u8* data
											, const unsigned size
											, netStatus* status);

		//PURPOSE
		//  For Gen9 client to get data for all saves that can be migrated..
		//PARAMS
		//	localGamerIndex			- Local gamer index.
		//	recordMetadata 			- Holds any records of saves uploaded.
		//  status                  - Can be polled for completion.
		static bool GetAvailableSingleplayerSaves(const int localGamerIndex, atArray< rlSaveMigrationRecordMetadata >* recordMetadata, netStatus* status);

		//PURPOSE
		//  For Gen9 client to Download an uploaded save file.
		//PARAMS
		//	localGamerIndex - Local gamer index.
		//	record 			- Info about the save being downloaded.
		//  callback        - Callback called when the download finishes.
		//  status          - Can be polled for completion.
		static bool DownloadSingleplayerSave(const int localGamerIndex, const rlSaveMigrationRecordMetadata& record, rlSaveMigrationDownloadSinglePlayerSave::ProcessResponseCallback callback, netStatus* status);

		//PURPOSE
		//  For Gen9 client to complete a save migration. Must be called after a save has been download and verified.
		//PARAMS
		//	localGamerIndex - Local gamer index.
		//  status          - Can be polled for completion.
		static bool CompleteSingleplayerMigration(const int localGamerIndex, netStatus* status);

		// PURPOSE
		//  Cancel an on going operation.
		static void  Cancel(netStatus * status);
	};
}

#endif//#ifndef RLINE_SAVEMIGRATION_H