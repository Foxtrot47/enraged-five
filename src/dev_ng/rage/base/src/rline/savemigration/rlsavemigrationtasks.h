// 
// rline/savemigration/rlsavemigrationtasks.h 
// 
// Copyright (C) 2021 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_SAVEMIGRATION_TASKS_H
#define RLINE_SAVEMIGRATION_TASKS_H

#include "rlsavemigrationtasks.h"
#include "rlsavemigrationcommon.h"

#include "rline/ros/rlroshttptask.h"

namespace rage
{
	//------------------------------------------------------------------------------
	// rlSaveMigrationGetMPAccounts
	//
	// Description
	//	Used to discover a list of linked accounts that are eligible/ineligible for transfer, along with some metadata for each, such as key profile stats and basic account information.
	//
	//	This is intended to be called automatically only once per account, e.g.the first time they boot into the gameand we want to give them the option to transfer.The game is responsible 
	//	for keeping track of whether or not it has ever called this for an account to avoid calling it more than once.Subsequent calls that are manually triggered by the user via 
	//	e.g. an in - game UI option to initiate a transfer later is fine.
	//
	// Parameters
	//  ticket - Authentication ticket
	//
	// Response
	//  Returns a list of accounts, with an indicator for each as to whether or not it is available for transfer, and an errorCode indicating why if not eligible.The errorCode can be used 
	//  to display a reason to the user, and currently includes :
	//
	//  ERROR_ALREADY_DONE - The account has already been transferred, and thus not eligible to transfer again
	//	ERROR_NOT_AVAILABLE - The account doesn't have any MP characters/progress, and thus can't be transferred
	// 
	// Error Codes
	//  AuthenticationFailed.Ticket - The authentication ticket is no longer valid, and the player must re - authenticate and start over
	//	NotAvailable.Maintenance - The service is temporarily unavailable due to planned maintenance
	//------------------------------------------------------------------------------
	class rlSaveMigrationGetMPAccounts : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationGetMPAccounts);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		bool Configure(const int localGamerIndex, rlSaveMigrationMPAccountsArray* accounts);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigration.asmx/GetSourceAccountsMP"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* rootNode, int& resultCode) override;

		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:
		rlSaveMigrationMPAccountsArray* m_Accounts;
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationMigrateMPAccount
	//
	// Description
	//  Used to initiate a transfer of the specified account, returning a transaction id that can then be used to track the status of it to determine once it's complete 
	//  (see GetSaveMigrationTransactionStatus).
	//
	// Parameters
	//  ticket - Authentication ticket
	//  sourcePlatform - The platform of the account to transfer, discovered by calling GetSourceAccountsMP
	//  sourceAccountId - The account to transfer, discovered by calling GetSourceAccountsMP
	//
	// Response
	//  Returns a transactionId that can be used to poll for the completion of the transfer(see GetSaveMigrationTransactionStatus)
	//
	// Error Codes
	//  AuthenticationFailed.Ticket - The authentication ticket is no longer valid, and the player must re - authenticate and start over
	//  NotAvailable.Maintenance - The service is temporarily unavailable due to planned maintenance
	//  DoesNotMatch.RockstarId - The account attempting to be transferred is not currently linked to the same rockstar account as the account that is attempting to initiate the transfer.This would usually indicate that the account link was changed by the user in between the call to GetSourceAccountsMP and MigrateAccountMP
	//  NotAllowed.RockstarAccountRequired - The account attempting to initiate the transfer is no longer linked to a rockstar account, which is currently enabled as a requirement on the backend
	//  NotAllowed.* - Used in several other cases where a transfer isn't allowed, such as the player being banned, in the cheater pool, violating some static threshold/limit, etc... Unless requirements state otherwise, a generic error message should be displayed to the user for all cases of NotAllowed.*, except those noted above
	//------------------------------------------------------------------------------
	class rlSaveMigrationMigrateMPAccount : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationMigrateMPAccount);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		bool Configure(const int localGamerIndex, const int accountid, const char* platform, rlSaveMigrationState* status);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigration.asmx/MigrateAccountMP"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* rootNode, int& resultCode) override;

		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:

		rlSaveMigrationState* m_State;
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetTransactionState
	//
	// Description
	//  Used to periodically poll the status of a previously initiated migration, in order to determine once it is completeand it is safe for the user to proceed into multiplayer.
	//
	// Parameters
	//  ticket - Authentication ticket
	//  transactionId - The transaction id for the previously initiated transfer, which can be discovered through either MigrateAccountMP, or GetSaveMigrationStatus
	//
	// Response
	//  Returns whether or not the transaction is still in - progress.
	//
	// Error Codes
	//  AuthenticationFailed.Ticket - The authentication ticket is no longer valid, and the player must re - authenticate and start over
	//------------------------------------------------------------------------------
	class rlSaveMigrationGetTransactionState : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationGetTransactionState);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		bool Configure(const int localGamerIndex, const int transactionId, rlSaveMigrationState* status);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigration.asmx/GetSaveMigrationTransactionStatus"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* rootNode, int& resultCode) override;

		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:

		rlSaveMigrationState* m_State;
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetState
	//
	// Description
	//  Should be called by the game client before entering into multiplayer, on both gen8 and gen9 consoles, so that a user doesn't enter into MP while a transfer is in progress for them, as this would cause them to stomp on the changes being made, potentially corrupting the account.
	//
	//  This is also necessary to check before entering MP, as the user may not always be the one initiating the transfer, due to e.g.an administrator initiating it.
	//
	//	This should be called to prevent both the sourceand the destination accounts involved in the transferred from entering into MP while it is still in progress.
	//
	// Parameters
	//  ticket - Authentication ticket
	//
	// Response
	//  Returns whether or not the transaction is still in - progress.
	//
	//  If InProgress = false, the game must also check the State string to determine if the transfer completed successfully or not.Possible options for State when InProgress is false include:
	//
	//  Finished - The transfer completed successfully
	//  Canceled - The transfer was canceled / not completed
	//  RolledBack - The transfer was undone / rolled back
	//  Error - It is not safe to proceed into MP, as the account is currently in a corrupt state
	//
	// Error Codes
	//  AuthenticationFailed.Ticket - The authentication ticket is no longer valid, and the player must re - authenticate and start over
	//------------------------------------------------------------------------------
	class rlSaveMigrationGetState : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationGetState);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		bool Configure(const int localGamerIndex, rlSaveMigrationState* status);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigration.asmx/GetSaveMigrationStatus"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* rootNode, int& resultCode) override;

		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:

		rlSaveMigrationState* m_State;
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetSingleplayerSaveState
	//
	// Description
	//  Should be called by the gen8 game clients to determine whether the player is eligible to upload a save.
	//
	// Parameters
	//	ticket		string		GameServices ticket
	//
	// Responses
	//	Migration not completed by the player from this platform, but a save has been previously uploaded.
	//  Migration not completed by the player from this platform, and no save uploaded (or the uploaded save has expired).
	//  Migration already completed by the player from this platform.
	//
	// Errors
	// 	AuthenticationFailed 					- Error authenticating the ticket.
	// 	NotAvailable.SaveMigration 				- Save migration is disabled.
	// 	NotAllowed.TitleInfo 					- Title/Platform is not a valid source for saves.
	// 	NotAllowed.RockstarAccount 				- The platform account is not linked to a Rockstar account.
	// 	NotAllowed.SourceMigrationInProgress 	- A migration is already in progress from the player account making the request (The file has been downloaded to the destination client, but the process not marked as complete).
	// 	NotAllowed.SourceMigrationComplete 		- A migration has already been completed from the player account making the request.
	//
	//------------------------------------------------------------------------------
	class rlSaveMigrationGetSingleplayerSaveState : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationGetSingleplayerSaveState);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		//PURPOSE
		//  Determine whether the player is eligible to upload a save
		//PARAMS
		//	localGamerIndex - Local gamer index.
		//  recordMetadata  - Struct that holds the metadata if the Migration not completed by the player from this platform, but a save has been previously uploaded.
		bool Configure(const int localGamerIndex, rlSaveMigrationRecordMetadata* recordMetadata);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigrationSingleplayer.asmx/GetUploadedSingleplayerSaveMetadata"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:
		rlSaveMigrationRecordMetadata* m_RecordMetadata;
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationUploadSinglePlayerSave
	//
	// Description
	//  Should be called by the gen8 game clients to upload a Single Player save.
	//
	// Parameters
	//	ticket 					string 		GameServices ticket
	//	savePosixTime 			long 		Posix time for when the save was made
	//	completionPercentage 	float 		Game percentage complete
	//	lastCompletedMissionId 	long 		Identifier for the last completed mission
	//
	// Responses
	//  Successful save upload.
	//  Failed save upload.
	//
	// Errors
	//  AuthenticationFailed 					- Error authenticating the ticket.
	//  NotAvailable.SaveMigration 				- Save migration is disabled.
	//  InvalidData.FileData 					- FileData is missing or invalid.
	//  InvalidArgument.SavePosixTime 			- SaveTimestamp value is missing or invalid.
	//  InvalidArgument.CompletionPercentage 	- CompletionPercentage value is missing or invalid.
	//  InvalidArgument.LastCompletedMissionId 	- LastCompletedMissionId value is missing or invalid.
	//  NotAllowed.TitleInfo 					- Title/Platform is not a valid source
	//  NotAllowed.RockstarAccount				- The platform account is not linked to a Rockstar account.
	//  NotAllowed.SourceMigrationInProgress 	- A migration is already in progress from the player account making the request (The file has been downloaded to the destination client, but the process not marked as complete).
	//  NotAllowed.SourceMigrationComplete		- A migration has already been completed from the platform account making the request.
	//  NotAvailable.FileStorage 				- File storage is unavailable.
	//------------------------------------------------------------------------------
	class rlSaveMigrationUploadSinglePlayerSave : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationUploadSinglePlayerSave);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		rlSaveMigrationUploadSinglePlayerSave(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0) 
			: rlRosHttpTask(allocator, bounceBuffer, sizeofBounceBuffer)
		{
		}

		//PURPOSE
		//  Upload a Single Player save.
		//PARAMS
		//	localGamerIndex			- Local gamer index.
		//	savePosixTime 			- Posix time for when the save was made
		//	completionPercentage 	- Game percentage complete
		//	lastCompletedMissionId 	- Identifier for the last completed mission
		//  data                    - Savegame payload.
		//  size                    - Size of Savegame payload.
		bool Configure(const int localGamerIndex
						, const u32 savePosixTime
						, const float completionPercentage
						, const u32 lastCompletedMissionId
						, const u8* data
						, const unsigned size);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigrationSingleplayer.asmx/UploadSingleplayerSave"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetAvailableSingleplayerSaves
	//
	// Description
	//  Should be called by the gen9 game clients to determine whether the player has saves that can be migrated.
	//
	// Parameters
	//  ticket 	string 	GameServices ticket	
	//
	// Responses
	//  Migration not completed by the player to this platform, saves uploaded from both XboxOne and PS4 by accounts linked to the player's Rockstar account.
	//  Migration not completed by the player to this platform, but no saves uploaded by any account account linked to their Rockstar Account.
	//  Migration already completed by the player to this platform.
	//
	// Errors
	//  AuthenticationFailed 						Error authenticating the ticket.
	//  NotAvailable.SaveMigration 					Save migration is disabled.
	//  NotAllowed.TitleInfo 						Title/Platform is not a valid destination.
	//  NotAllowed.RockstarAccount 					The platform account is not linked to a Rockstar account.
	//  NotAllowed.DestinationMigrationComplete 	A migration has already been completed to the platform account making the request.
	//
	//------------------------------------------------------------------------------
	class rlSaveMigrationGetAvailableSingleplayerSaves : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationGetAvailableSingleplayerSaves);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		//PURPOSE
		//  Get data for all saves that can be migrated..
		//PARAMS
		//	localGamerIndex			- Local gamer index.
		//	recordMetadata 			- Holds any records of saves uploaded.
		bool Configure(const int localGamerIndex, atArray< rlSaveMigrationRecordMetadata >* recordMetadata);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigrationSingleplayer.asmx/GetAvailableSingleplayerSaveMetadata"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:
		atArray< rlSaveMigrationRecordMetadata >* m_RecordMetadata;
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationDownloadSinglePlayerSave
	//
	// Description
	//  Should be called by the gen9 game clients for downloading an uploaded save file on the destination client.
	//  The save to be migrated is selected by passing the SaveMigrationRecordToken, as taken from the GetAvailableSingleplayerSaveMetadata response.
	//  Marks the save migration as in progress on the services side.
	//
	// Parameters
	//  ticket 						string 	Regular GameServices ticket
	//  saveMigrationRecordToken 	string 	Token to identify the save migration record for which the stored file should be returned.
	//
	// Response
	//  Successful save download header.
	//  Failed save download
	//
	// Errors
	//  AuthenticationFailed 						Error authenticating the ticket
	//  NotAvailable.SaveMigration 					Save migration is disabled.
	//  NotAllowed.TitleInfo 						Title/Platform is not a valid destination.
	//  InvalidArgument.SaveMigrationRecordToken 	Invalid saveMigrationRecordToken value.
	//  DoesNotExist.SaveMigrationRecord 			There is no existing save migration record matching the saveMigrationRecordToken value provided.
	//  Expired.SaveMigrationRecordToken 			The SaveMigrationRecordToken has expired.
	//  Expired.SaveMigrationRecord 				The save migration record has expired.
	//  NotAllowed.DestinationMigrationInProgress 	The migration of a recordId other than the one requested is already in progress to the destination account.
	//  NotAllowed.SourceMigrationComplete 			A migration has already been completed from the platform account from which the save was uploaded.
	//  NotAllowed.DestinationMigrationComplete 	A migration has already been completed to the platform account attempting to download the save.
	//  NotAvailable.SaveFile 						An error occurred when attempting to get the save data file from storage.
	//  
	//------------------------------------------------------------------------------
	class rlSaveMigrationDownloadSinglePlayerSave : public rlRosHttpTask
	{
	public:
		typedef void (*ProcessResponseCallback)(const void* pData, const unsigned nDataSize);

	public:
		RL_TASK_DECL(rlSaveMigrationDownloadSinglePlayerSave);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		rlSaveMigrationDownloadSinglePlayerSave(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0) 
			: rlRosHttpTask(allocator, bounceBuffer, sizeofBounceBuffer)
		{
		}

		//PURPOSE
		//  Download an uploaded save file.
		//PARAMS
		//	localGamerIndex - Local gamer index.
		//	record 			- Info about the save being downloaded.
		//  callback        - Callback called when the download finishes.
		bool Configure(const int localGamerIndex, const rlSaveMigrationRecordMetadata& record, ProcessResponseCallback callback);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigrationSingleplayer.asmx/DownloadSinglePlayerSave"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessResponse(const char* response, int& resultCode) override;
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:
		ProcessResponseCallback m_ProcessResponseCallback;
	};

	//------------------------------------------------------------------------------
	// rlSaveMigrationCompleteSingleplayerMigration
	//
	// Description
	//  To be called by the destination client after it has received and validated the save file from the GetSingleplayerSave endpoint.
	//  This endpoint will migrate achievements and mark the migration as complete.
	//
	// Parameters
	//  ticket	string 	Regular GameServices ticket
	//
	// Response
	//  Success.
	//  Failure.
	//
	// Errors
	//  AuthenticationFailed 						Error authenticating the ticket.
	//  NotAvailable.SaveMigration 					Save migration is disabled.
	//  NotAllowed.TitleInfo 						Title/Platform is not a valid destination.
	//  NotAllowed.RockstarAccount 					The platform account is not linked to a Rockstar account.
	//  NotAllowed.DestinationMigrationComplete 	A migration has already been completed to the platform account making the request.
	//  InvalidState.NotInProgress 					The player does not have a save migration in progress to complete.
	//  
	//------------------------------------------------------------------------------
	class rlSaveMigrationCompleteSingleplayerMigration : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlSaveMigrationCompleteSingleplayerMigration);
		RL_TASK_USE_CHANNEL(rline_savemigration);

		//PURPOSE
		//  Complete a save migration.
		//PARAMS
		//	localGamerIndex - Local gamer index.
		bool Configure(const int localGamerIndex);

	protected:

		virtual const char* GetServiceMethod() const override { return "SaveMigrationSingleplayer.asmx/CompleteSingleplayerMigration"; }

		virtual bool IsCancelable() const override { return true; }

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

	private:
	};

}

#endif // RLINE_SAVEMIGRATION_TASKS_H