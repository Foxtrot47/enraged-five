// 
// rline/savemigration/rlsavemigrationtasks.cpp
// 
// Copyright (C) 2021 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsavemigrationtasks.h"
#include "diag/seh.h"
#include "rline/ros/rlros.h"
#include "parser/treenode.h"

namespace rage
{
	//------------------------------------------------------------------------------
	// rlSaveMigrationMPSourceAccounts
	//------------------------------------------------------------------------------

	bool rlSaveMigrationGetMPAccounts::Configure(const int localGamerIndex, rlSaveMigrationMPAccountsArray* accounts)
	{
		rtry
		{
			rverify(accounts != NULL, catchall,);
			m_Accounts = accounts;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			return true;
		}
			rcatchall
		{
			return false;
		}
	}

	bool rlSaveMigrationGetMPAccounts::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* rootNode, int& resultCode)
	{
		rtry
		{
			/*
			Response format is:
			<?xml version="1.0" encoding="utf-8"?>
			<Response xmlns="GetSourceAccountsMP">
			  <Result>
				<Accounts>
				  <Account accountId="int" platform="string" gamertag="string" gamerhandle="string" available="boolean" errorCode="string">
					<Stats>
					  <Stat name="string" value="string" />
					  <Stat name="string" value="string" />
					</Stats>
				  </Account>
				  <Account accountId="int" platform="string" gamertag="string" gamerhandle="string" available="boolean" errorCode="string">
					<Stats>
					  <Stat name="string" value="string" />
					  <Stat name="string" value="string" />
					</Stats>
				  </Account>
				</Accounts>
			  </Result>
			  <Status>int</Status>
			</Response>
			*/

			parTreeNode * resultNode = rootNode->FindChildWithName("Result", NULL, false);
			rverify(resultNode != NULL, catchall, rlError("Missing result node"));

			parTreeNode* accountsNode = resultNode->FindChildWithName("Accounts", NULL, false);
			rverify(accountsNode != NULL, catchall, rlError("Missing acounts node"));
			if (accountsNode)
			{
				for (parTreeNode::ChildNodeIterator accountIt = accountsNode->BeginChildren(); accountIt != accountsNode->EndChildren(); ++accountIt)
				{
					parTreeNode* accountNode = *accountIt;

					if (stricmp(accountNode->GetElement().GetName(), "Account") == 0)
					{
						rlSaveMigrationMPAccount account;

						// Read the account id
						rverify(rlHttpTaskHelper::ReadInt(account.m_AccountId, accountNode, NULL, "accountId"), catchall,);

						// Read platform
						const char* platform = rlHttpTaskHelper::ReadString(accountNode, NULL, "platform");
						rverify(platform != NULL, catchall,);
						safecpy(account.m_Platform, platform, RLSM_MAX_PLATFORM_CHARS);

						// Read gamertag
						const char* gamertag = rlHttpTaskHelper::ReadString(accountNode, NULL, "gamertag");
						rverify(gamertag != NULL, catchall,);
						safecpy(account.m_Gamertag, gamertag, RLSM_MAX_GAMERTAG_CHARS);

						// Read gamerhandle
						const char* gamerhandle = rlHttpTaskHelper::ReadString(accountNode, NULL, "gamerhandle");
						rverify(gamerhandle != NULL, catchall,);
						safecpy(account.m_GamerHandle, gamerhandle, RLSM_MAX_GAMERHANDLE_CHARS);

						// Read ugcPublishCount 
						if (!rlHttpTaskHelper::ReadInt(account.m_ugcPublishCount, accountNode, NULL, "ugcPublishCount"))
						{
							account.m_ugcPublishCount = -1; // Empty or missing
						}

						// Read whether or not this save is available to transfer
						rverify(rlHttpTaskHelper::ReadBool(account.m_Available, accountNode, NULL, "available"), catchall,);

						// Read the error code
						const char* errorcode = rlHttpTaskHelper::ReadString(accountNode, NULL, "errorCode");
						if (errorcode != NULL)
						{
							safecpy(account.m_ErrorCode, errorcode, RLSM_MAX_ERROR_CODE);
						}

						// Write out the account information
						rlsmDebugf2("rlSaveMigrationGetMPAccounts: Account=%d, Platform='%s', Gamertag='%s', Gamerhandle='%s', Available='%s' ErrorCode='%s'"
							, account.m_AccountId
							, account.m_Platform
							, account.m_Gamertag
							, account.m_GamerHandle
							, account.m_Available ? "True" : "False"
							, account.m_ErrorCode);
						// Make sure we have room to add this account
						rverify(m_Accounts->GetCount() < m_Accounts->GetMaxCount(), catchall,);

						// Read the stats information if there is any
						parTreeNode* statsNode = accountNode->FindChildWithName("Stats", NULL, false);
						if (statsNode)
						{
							for (parTreeNode::ChildNodeIterator statIt = statsNode->BeginChildren(); statIt != statsNode->EndChildren(); ++statIt)
							{
								parTreeNode* statNode = *statIt;
								if (stricmp(statNode->GetElement().GetName(), "Stat") == 0)
								{
									rlSaveMigrationMPStat stat;

									// Read the stat name
									const char* name = rlHttpTaskHelper::ReadString(statNode, NULL, "name");
									rverify(name != NULL, catchall,);
									safecpy(stat.m_Name, name, RLSM_MAX_STATS_NAME_CHARS);

									// Read the stat value
									const char* value = rlHttpTaskHelper::ReadString(statNode, NULL, "value");
									rverify(value != NULL, catchall,);
									safecpy(stat.m_Value, value, RLSM_MAX_STATS_VALUE_CHARS);

									// Write out the stat information
									rlsmDebugf2("rlSaveMigrationGetMPAccounts Stat: %s, Value: %s", stat.m_Name, stat.m_Value);
									// Make sure we have room to add this stat
									rverify(account.m_Stats.GetCount() < account.m_Stats.GetMaxCount(), catchall,);
									// Add the new stat to the account
									account.m_Stats.Push(stat);
								}
							}
						}
						else
						{
							rlsmDebugf1("rlSaveMigrationGetMPAccounts : Account %d[%s] for platform %s has no stats!", account.m_AccountId, account.m_Gamertag, account.m_Platform);
						}

						m_Accounts->Push(account);
					}
				}
			}
			else
			{
				rlsmWarningf("rlSaveMigrationGetMPAccounts : No accounts node found!");
			}

			resultCode = RLSM_ERROR_NONE;

			return true;
		}
		rcatchall
		{
			resultCode = RLSM_ERROR_UNKNOWN;

			return false;
		}

	}

	void rlSaveMigrationGetMPAccounts::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "Maintenance", RLSM_ERROR_MAINTENANCE);

		resultCode = RLSM_ERROR_UNKNOWN;
	}

	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationMigrateMPAccount
	//------------------------------------------------------------------------------

	bool rlSaveMigrationMigrateMPAccount::Configure(const int localGamerIndex, const int accountid, const char* platform, rlSaveMigrationState* state)
	{
		rtry
		{
			rverify(state != NULL, catchall,);
			m_State = state;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			rverify(platform != NULL, catchall,);
			rverify(AddStringParameter("sourcePlatform", platform, true), catchall,);

			rverify(accountid != RLSM_INVALID_ACCOUNT_ID, catchall,);
			rverify(AddIntParameter("sourceAccountId", accountid, true), catchall,);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlSaveMigrationMigrateMPAccount::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* rootNode, int& resultCode)
	{
		rtry
		{
			/*
			Response format is:
			<?xml version="1.0" encoding="utf-8" ? >
			<Response xmlns="MigrateSave">
				<Result>
					<TransactionId>int</TransactionId>
				</Result>
				<Status>int</Status>
			</Response>
			*/

			int transactionId = RLSM_INVALID_TRANSACTION_ID;

			parTreeNode * resultNode = rootNode->FindChildWithName("Result", NULL, false);
			rverify(resultNode != NULL, catchall, rlError("Missing result node"));

			parTreeNode* transactionIdNode = resultNode->FindChildWithName("TransactionId", NULL, false);
			rverify(transactionIdNode != NULL, catchall, rlError("Missing transaction id node"));
			rverify(rlHttpTaskHelper::ReadInt(transactionId, transactionIdNode, NULL, NULL), catchall, rlError("transaction id node missing data"));

			m_State->Set(transactionId, true, NULL);

			rlsmDebugf2("rlSaveMigrationMigrateMPAccount: State=%s, State=%d, TransactionId=%d"
				, m_State->GetState()
				, m_State->GetStateId()
				, m_State->GetTransactionId());

			resultCode = RLSM_ERROR_NONE;

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlSaveMigrationMigrateMPAccount::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "Maintenance", RLSM_ERROR_MAINTENANCE);
		SAVEMIGRATION_ERROR_CASE("DoesNotMatch", "RockstarId", RLSM_ERROR_MAINTENANCE);
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "RockstarAccountRequired", RLSM_ERROR_MAINTENANCE);
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "*", RLSM_ERROR_UNKNOWN);

		resultCode = RLSM_ERROR_UNKNOWN;
	}

	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetTransactionState
	//------------------------------------------------------------------------------

	bool rlSaveMigrationGetTransactionState::Configure(const int localGamerIndex, const int transactionId, rlSaveMigrationState* state)
	{
		rtry
		{
			rverify(state != NULL, catchall,);
			m_State = state;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			rverify(transactionId != RLSM_INVALID_TRANSACTION_ID, catchall,);
			rverify(AddIntParameter("transactionId", transactionId, true), catchall,);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlSaveMigrationGetTransactionState::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* rootNode, int& resultCode)
	{
		rtry
		{
			/*
			Response format is:
			<?xml version="1.0" encoding="utf-8"?>
			<Response xmlns="GetSaveMigrationStatus">
			  <Result>
				<TransactionId>int</TransactionId>
				<InProgress>boolean</InProgress>
				<State>string</State>
			  </Result>
			  <Status>int</Status>
			</Response>
			*/

			int transactionId = RLSM_INVALID_TRANSACTION_ID;
			bool inProgress = false;
			const char* state = nullptr;

			parTreeNode* resultNode = rootNode->FindChildWithName("Result", NULL, false);
			rverify(resultNode != NULL, catchall, rlError("Missing result node"));

			parTreeNode* stateNode = resultNode->FindChildWithName("State", NULL, false);
			rverify(stateNode != NULL, catchall, rlError("Missing State"));
			state = rlHttpTaskHelper::ReadString(stateNode, NULL, NULL);
			rverify(state != NULL, catchall, rlTaskError("Failed to find State attribute"));

			parTreeNode* transactionIdNode = resultNode->FindChildWithName("TransactionId", NULL, false);
			rverify(transactionIdNode != NULL, catchall, rlError("Missing TransactionIdnode"));
			rverify(rlHttpTaskHelper::ReadInt(transactionId, transactionIdNode, NULL, NULL), catchall, rlError("TransactionId node missing data"));

			parTreeNode* inProgressNode = resultNode->FindChildWithName("InProgress", NULL, false);
			rverify(inProgressNode != NULL, catchall, rlError("Missing InProgressnode"));
			rverify(rlHttpTaskHelper::ReadBool(inProgress, inProgressNode, NULL, NULL), catchall, rlError("InProgress node missing data"));

			m_State->Set(transactionId, inProgress, state);

			rlsmDebugf2("rlSaveMigrationGetTransactionState: State=%s, State=%d, TransactionId=%d"
				, m_State->GetState()
				, m_State->GetStateId()
				, m_State->GetTransactionId());

			resultCode = RLSM_ERROR_NONE;

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlSaveMigrationGetTransactionState::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);

		resultCode = RLSM_ERROR_UNKNOWN;
	}

	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetState
	//------------------------------------------------------------------------------

	bool rlSaveMigrationGetState::Configure(const int localGamerIndex, rlSaveMigrationState* state)
	{
		rtry
		{
			rverify(state != NULL, catchall,);
			m_State = state;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlSaveMigrationGetState::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* rootNode, int& resultCode)
	{
		rtry
		{
			/*
			Response format is:
			<?xml version="1.0" encoding="utf-8"?>
			<Response xmlns="GetSaveMigrationStatus">
			  <Result>
				<TransactionId>int</TransactionId>
				<InProgress>boolean</InProgress>
				<State>string</State>
			  </Result>
			  <Status>int</Status>
			</Response>
			*/
			
			int transactionId = RLSM_INVALID_TRANSACTION_ID;
			bool inProgress = false;
			const char* state = nullptr;

			parTreeNode * resultNode = rootNode->FindChildWithName("Result", NULL, false);
			rverify(resultNode != NULL, catchall, rlError("Missing result node"));

			parTreeNode* transactionIdNode = resultNode->FindChildWithName("TransactionId", NULL, false);
			if (transactionIdNode){
				rverify(rlHttpTaskHelper::ReadInt(transactionId, transactionIdNode, NULL, NULL), catchall, rlError("TransactionId node missing data"));
			}
		
			parTreeNode* inProgressNode = resultNode->FindChildWithName("InProgress", NULL, false);
			if (inProgressNode) {
				rverify(rlHttpTaskHelper::ReadBool(inProgress, inProgressNode, NULL, NULL), catchall, rlError("InProgress node missing data"));
			}		

			parTreeNode* stateNode = resultNode->FindChildWithName("State", NULL, false);
			if (stateNode) {
				state = rlHttpTaskHelper::ReadString(stateNode, NULL, NULL);
				rcheck(state, catchall, rlTaskError("Failed to find State attribute"));
			}

			m_State->Set(transactionId, inProgress, state);

			resultCode = RLSM_ERROR_NONE;

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlSaveMigrationGetState::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);

		resultCode = RLSM_ERROR_UNKNOWN;
	}

	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetSingleplayerSaveState
	//------------------------------------------------------------------------------

	bool rlSaveMigrationGetSingleplayerSaveState::Configure(const int localGamerIndex, rlSaveMigrationRecordMetadata* recordMetadata)
	{
		rtry
		{
			rverify(recordMetadata != NULL, catchall,);
			m_RecordMetadata = recordMetadata;

			rlHttpTaskConfig config;
			config.m_HttpVerb = NET_HTTP_VERB_GET;
			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

			//Add GameServices ticket
			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlSaveMigrationGetSingleplayerSaveState::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& resultCode)
	{
		rtry
		{
			/*
			Response format is:
			<?xml version="1.0" encoding="utf-8"?>
			<Response xmlns="GetUploadedSingleplayerSaveMetadata">
				<Status>1</Status>
				<SaveMigrationRecordMetadata>
					<SaveMigrationRecordToken>string</SaveMigrationRecordToken>
					<SourceRockstarId>int</SourceRockstarId>
					<SourcePlayerAccountId>int</SourcePlayerAccountId>
					<SourcePlatformId>int</SourcePlatformId>
					<UploadPosixTime>long</UploadPosixTime>
					<CompletionPercentage>float</CompletionPercentage>
					<LastCompletedMissionId>long</LastCompletedMissionId>
					<SavePosixTime>long</SavePosixTime>
				</SaveMigrationRecordMetadata>
			</Response>
			*/

			rverify(node != nullptr
			, catchall
				, rlError("Missing result root node."));

			rverify(m_RecordMetadata != nullptr
				, catchall
				, rlError("m_RecordMetadata is a nullptr."));
			m_RecordMetadata->Clear();

			parTreeNode* resultNode = node->FindChildWithName("SaveMigrationRecordMetadata", NULL, false);
			rverify(resultNode != NULL
				, catchall
				, rlError("Missing result node"));

			//SaveMigrationRecordToken
			parTreeNode* tokenNode = resultNode->FindChildWithName("SaveMigrationRecordToken", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'SaveMigrationRecordToken' element"));
			m_RecordMetadata->m_Token = rlHttpTaskHelper::ReadString(tokenNode, NULL, NULL);

			//SourceRockstarId
			parTreeNode* rockstarIdNode = resultNode->FindChildWithName("SourceRockstarId", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'SourceRockstarId' element"));
			rverify(rlHttpTaskHelper::ReadInt(m_RecordMetadata->m_RockstarId, rockstarIdNode, NULL, NULL), catchall, rlError("SourceRockstarId node missing data"));

			//SourcePlayerAccountId
			parTreeNode* playerAccountIdNode = resultNode->FindChildWithName("SourcePlayerAccountId", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'SourcePlayerAccountId' element"));
			rverify(rlHttpTaskHelper::ReadInt(m_RecordMetadata->m_AccountId, playerAccountIdNode, NULL, NULL), catchall, rlError("SourcePlayerAccountId node missing data"));

			//SourcePlatformId
			parTreeNode* platformIdNode = resultNode->FindChildWithName("SourcePlatformId", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'SourcePlatformId' element"));
			rverify(rlHttpTaskHelper::ReadInt(m_RecordMetadata->m_PlatformId, platformIdNode, NULL, NULL), catchall, rlError("SourcePlatformId node missing data"));

			//UploadPosixTime
			parTreeNode* uploadPosixTimeNode = resultNode->FindChildWithName("UploadPosixTime", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'UploadPosixTime' element"));
			rverify(rlHttpTaskHelper::ReadUns(m_RecordMetadata->m_UploadPosixTime, uploadPosixTimeNode, NULL, NULL), catchall, rlError("UploadPosixTime node missing data"));

			//CompletionPercentage
			parTreeNode* completionPercentageNode = resultNode->FindChildWithName("CompletionPercentage", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'CompletionPercentage' element"));
			rverify(rlHttpTaskHelper::ReadFloat(m_RecordMetadata->m_CompletionPercentage, completionPercentageNode, NULL, NULL), catchall, rlError("CompletionPercentage node missing data"));

			//LastCompletedMissionId
			parTreeNode* lastCompletedMissionIdNode = resultNode->FindChildWithName("LastCompletedMissionId", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'LastCompletedMissionId' element"));
			rverify(rlHttpTaskHelper::ReadUns(m_RecordMetadata->m_LastCompletedMissionId, lastCompletedMissionIdNode, NULL, NULL), catchall, rlError("LastCompletedMissionId node missing data"));

			//SavePosixTime
			parTreeNode* savePosixTimeNode = resultNode->FindChildWithName("SavePosixTime", NULL, false);
			rverify(tokenNode, catchall, rlTaskError("No 'SavePosixTime' element"));
			rverify(rlHttpTaskHelper::ReadUns(m_RecordMetadata->m_SavePosixTime, savePosixTimeNode, NULL, NULL), catchall, rlError("SavePosixTime node missing data"));

			resultCode = RLSM_ERROR_NONE;

			return true;
		}
		rcatchall
		{
			if (m_RecordMetadata)
			{
				m_RecordMetadata->Clear();
			}

			resultCode = RLSM_ERROR_UNKNOWN;
		}

		return false;
	}

	void rlSaveMigrationGetSingleplayerSaveState::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		// Error authenticating the ticket.
		SAVEMIGRATION_ERROR_CASE("DoesNotExist", "SourceMigrationRecord", RLSM_ERROR_NO_MIGRATION_RECORDS);

		// Error authenticating the ticket.
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);

		// Save migration is disabled.
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "SaveMigration", RLSM_ERROR_DISABLED);

		// Title/Platform is not a valid source for save.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "TitleInfo", RLSM_ERROR_INVALID_TITLE);

		// The platform account is not linked to a Rockstar account.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "RockstarAccount", RLSM_ERROR_ROCKSTAR_ACCOUNT_REQUIRED);

		// A migration is already in progress from the player account making the request (The file has been
		// downloaded to the destination client, but the process not marked as complete).
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "SourceMigrationInProgress", RLSM_ERROR_IN_PROGRESS);

		// A migration has already been completed from the player account making the request.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "SourceMigrationComplete", RLSM_ERROR_ALREADY_COMPLETE);

		resultCode = RLSM_ERROR_UNKNOWN;
	}

	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationUploadSinlgePlayerSave
	//------------------------------------------------------------------------------

	bool rlSaveMigrationUploadSinglePlayerSave::Configure(const int localGamerIndex, 
														const u32 savePosixTime, 
														const float completionPercentage, 
														const u32 lastCompletedMissionId,
														const u8* data, 
														const unsigned size)
	{
		bool success = false;

		rtry
		{
			rverify(data, catchall, rlTaskError("Save 'data' is nullptr."));
			rverify(size > 0, catchall, rlTaskError("Invalid 'data' size, must be > 0."));
			rverify(completionPercentage > 0.0f, catchall, rlTaskError("Value 'completionPercentage' is 0.0f."));

			rlHttpTaskConfig config;
			config.m_AddDefaultContentTypeHeader = false;
			rverify(rlRosHttpTask::Configure(localGamerIndex, &config), catchall, );

			rverify(StartMultipartFormData(), catchall, );

			//Add GameServices ticket
			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));
			rverify(AddMultipartStringParameter("ticket", cred.GetTicket(), true), catchall,);

			rverify(AddMultipartUnsParameter("savePosixTime", savePosixTime), catchall,);
			rverify(AddMultipartDoubleParameter("completionPercentage", completionPercentage), catchall,);
			rverify(AddMultipartUnsParameter("lastCompletedMissionId", lastCompletedMissionId), catchall,);

			rverify(AddMultipartBinaryParameter("file1", "SinglePlayerSave", data, size), catchall, );
			rverify(EndMultipartFormData(), catchall, );

			success = true;
		}
		rcatchall
		{
		}

		return success;
	}

	bool rlSaveMigrationUploadSinglePlayerSave::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		resultCode = RLSM_ERROR_NONE;
		return true;
	}

	void rlSaveMigrationUploadSinglePlayerSave::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		// Error authenticating the ticket.
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);

		// Save migration is disabled.
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "SaveMigration", RLSM_ERROR_DISABLED);

		// FileData is missing or invalid.
		SAVEMIGRATION_ERROR_CASE("InvalidData", "FileData", RLSM_ERROR_INVALID_FILEDATA);

		// Missing or invalid argument.
		SAVEMIGRATION_ERROR_CASE("InvalidArgument", "*", RLSM_ERROR_INVALID_PARAMETER);

		// Title/Platform is not a valid source for saves.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "TitleInfo", RLSM_ERROR_INVALID_TITLE);

		// The platform account is not linked to a Rockstar account.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "RockstarAccount", RLSM_ERROR_ROCKSTAR_ACCOUNT_REQUIRED);

		// A migration is already in progress from the player account making the request (The file has been downloaded 
		// to the destination client, but the process not marked as complete).
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "SourceMigrationInProgress", RLSM_ERROR_IN_PROGRESS);

		// A migration has already been completed from the platform account making the request
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "SourceMigrationComplete", RLSM_ERROR_ALREADY_COMPLETE);

		// File storage is unavailable.
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "FileStorage", RLSM_ERROR_FILESTORAGE_UNAVAILABLE);

		// Unknown error
		resultCode = RLSM_ERROR_UNKNOWN;
	}

	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationGetAvailableSingleplayerSaves
	//------------------------------------------------------------------------------

	bool rlSaveMigrationGetAvailableSingleplayerSaves::Configure(const int localGamerIndex, atArray< rlSaveMigrationRecordMetadata >* recordMetadata)
	{
		rtry
		{
			rverify(recordMetadata != NULL, catchall,);
			m_RecordMetadata = recordMetadata;

			rlHttpTaskConfig config;
			config.m_HttpVerb = NET_HTTP_VERB_GET;
			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

			//Add GameServices ticket
			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlSaveMigrationGetAvailableSingleplayerSaves::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& resultCode)
	{
		bool success = true;
		resultCode = RLSM_ERROR_NONE;

		rtry
		{
			/*
				// Migration not completed by the player to this platform, but no saves uploaded by any 
				//  account account linked to their Rockstar Account.

				<Response xmlns="GetAvailableSingleplayerSaveMetadata">
					<Status>1</Status>
					<AvailableSaves />
				</Response>


				// Migration not completed by the player to this platform, saves uploaded from both 
				//  XboxOne and PS4 by accounts linked to the player's Rockstar account.

				<Response xmlns="GetAvailableSingleplayerSaveMetadata">
					<Status>1</Status>
					<AvailableSaves>
						<SaveMigrationRecordMetadata>
							<SaveMigrationRecordToken>StringRecordToken</SaveMigrationRecordToken>
							<SourceRockstarId>984361</SourceRockstarId>
							<SourcePlayerAccountId>368015</SourcePlayerAccountId>
							<SourcePlatformId>12</SourcePlatformId>
							<UploadPosixTime>1612746583</UploadPosixTime>
							<CompletionPercentage>100</CompletionPercentage>
							<LastCompletedMissionId>22445566</LastCompletedMissionId>
							<SavePosixTime>1611759377</SavePosixTime>
						</SaveMigrationRecordMetadata>
						<SaveMigrationRecordMetadata>
							<SaveMigrationRecordToken>StringRecordToken2</SaveMigrationRecordToken>
							<SourceRockstarId>984361</SourceRockstarId>
							<SourcePlayerAccountId>442612</SourcePlayerAccountId>
							<SourcePlatformId>11</SourcePlatformId>
							<UploadPosixTime>1614294856</UploadPosixTime>
							<CompletionPercentage>3.20982</CompletionPercentage>
							<LastCompletedMissionId>22445567</LastCompletedMissionId>
							<SavePosixTime>1609751046</SavePosixTime>
						</SaveMigrationRecordMetadata>
					</AvailableSaves>
				</Response>
			*/


			rverify(node != nullptr
				, catchall
				, rlError("Missing result root node."));

			rverify(m_RecordMetadata != nullptr
				, catchall
				, rlError("m_RecordMetadata is a nullptr."));

			parTreeNode* resultNode = node->FindChildWithName("AvailableSaves", NULL, false);
			rverify(resultNode != NULL
				, catchall
				, rlError("Missing result node"));

			parTreeNode::ChildNodeIterator iter = resultNode->BeginChildren();
			for (; iter != resultNode->EndChildren(); ++iter)
			{
				parTreeNode* record = *iter;

				rlSaveMigrationRecordMetadata metadata;

				//SaveMigrationRecordToken
				parTreeNode* tokenNode = record->FindChildWithName("SaveMigrationRecordToken", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'SaveMigrationRecordToken' element"));
				metadata.m_Token = rlHttpTaskHelper::ReadString(tokenNode, NULL, NULL);

				//SourceRockstarId
				parTreeNode* rockstarIdNode = record->FindChildWithName("SourceRockstarId", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'SourceRockstarId' element"));
				rverify(rlHttpTaskHelper::ReadInt(metadata.m_RockstarId, rockstarIdNode, NULL, NULL), catchall, rlError("SourceRockstarId node missing data"));

				//SourcePlayerAccountId
				parTreeNode* playerAccountIdNode = record->FindChildWithName("SourcePlayerAccountId", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'SourcePlayerAccountId' element"));
				rverify(rlHttpTaskHelper::ReadInt(metadata.m_AccountId, playerAccountIdNode, NULL, NULL), catchall, rlError("SourcePlayerAccountId node missing data"));

				//SourcePlayerDisplayName
				parTreeNode* playerDisplayNameNode = record->FindChildWithName("SourcePlayerDisplayName", NULL, false);
				rverify(playerDisplayNameNode, catchall, rlTaskError("No 'SourcePlayerDisplayName' element"));
				metadata.m_PlayerDisplayName = rlHttpTaskHelper::ReadString(playerDisplayNameNode, NULL, NULL);
				rverify(metadata.m_PlayerDisplayName != NULL, catchall, rlError("SourcePlayerDisplayName node missing data"));

				//SourcePlatformId
				parTreeNode* platformIdNode = record->FindChildWithName("SourcePlatformId", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'SourcePlatformId' element"));
				rverify(rlHttpTaskHelper::ReadInt(metadata.m_PlatformId, platformIdNode, NULL, NULL), catchall, rlError("SourcePlatformId node missing data"));

				//UploadPosixTime
				parTreeNode* uploadPosixTimeNode = record->FindChildWithName("UploadPosixTime", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'UploadPosixTime' element"));
				rverify(rlHttpTaskHelper::ReadUns(metadata.m_UploadPosixTime, uploadPosixTimeNode, NULL, NULL), catchall, rlError("UploadPosixTime node missing data"));

				//CompletionPercentage
				parTreeNode* completionPercentageNode = record->FindChildWithName("CompletionPercentage", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'CompletionPercentage' element"));
				rverify(rlHttpTaskHelper::ReadFloat(metadata.m_CompletionPercentage, completionPercentageNode, NULL, NULL), catchall, rlError("CompletionPercentage node missing data"));

				//LastCompletedMissionId
				parTreeNode* lastCompletedMissionIdNode = record->FindChildWithName("LastCompletedMissionId", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'LastCompletedMissionId' element"));
				rverify(rlHttpTaskHelper::ReadUns(metadata.m_LastCompletedMissionId, lastCompletedMissionIdNode, NULL, NULL), catchall, rlError("LastCompletedMissionId node missing data"));

				//SavePosixTime
				parTreeNode* savePosixTimeNode = record->FindChildWithName("SavePosixTime", NULL, false);
				rverify(tokenNode, catchall, rlTaskError("No 'SavePosixTime' element"));
				rverify(rlHttpTaskHelper::ReadUns(metadata.m_SavePosixTime, savePosixTimeNode, NULL, NULL), catchall, rlError("SavePosixTime node missing data"));

				m_RecordMetadata->PushAndGrow(metadata);
			}
		}
		rcatchall
		{
			success = false;
			resultCode = RLSM_ERROR_UNKNOWN;
		}

		return success;
	}

	void rlSaveMigrationGetAvailableSingleplayerSaves::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		// Error authenticating the ticket.
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);

		// Save migration is disabled.
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "SaveMigration", RLSM_ERROR_DISABLED);

		// Title/Platform is not a valid source for save.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "TitleInfo", RLSM_ERROR_INVALID_TITLE);

		// The platform account is not linked to a Rockstar account.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "RockstarAccount", RLSM_ERROR_ROCKSTAR_ACCOUNT_REQUIRED);

		// A migration has already been completed from the player account making the request.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "DestinationMigrationComplete", RLSM_ERROR_ALREADY_COMPLETE);

		resultCode = RLSM_ERROR_UNKNOWN;
	}


	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationDownloadSinglePlayerSave
	//------------------------------------------------------------------------------

	bool rlSaveMigrationDownloadSinglePlayerSave::Configure(const int localGamerIndex, const rlSaveMigrationRecordMetadata& record, ProcessResponseCallback callback)
	{
		bool success = false;

		rtry
		{
			rlHttpTaskConfig config;
			config.m_HttpVerb = NET_HTTP_VERB_GET;
			rverify(rlRosHttpTask::Configure(localGamerIndex, &config), catchall, );

			//Add GameServices ticket
			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			rverify(AddStringParameter("saveMigrationRecordToken", record.m_Token, true), catchall,);

			m_ProcessResponseCallback = callback;

			success = true;
		}
		rcatchall
		{
		}

		return success;
	}

	bool rlSaveMigrationDownloadSinglePlayerSave::ProcessResponse(const char* response, int& resultCode)
	{
		bool result = false;
		resultCode = RLSM_ERROR_UNKNOWN;

		rtry
		{
			rlTaskDebug("rlSaveMigrationDownloadSinglePlayerSave::ProcessResponse:: %s", response);

			rcheck(response 
				,catchall
				,rlTaskError("Response nullptr"));

			RsonReader responseReader;

			//We need the length of the response, which is contained in the grow buffer.
			const unsigned int responseLength = m_GrowBuffer.Length();
			responseReader.Init(response, responseLength);

			rcheck(responseLength > 0
				,catchall
				,rlTaskError("Invalid responseLength size"));

			int status = 0;
			rcheck(responseReader.ReadInt("Status", status)
				,catchall
				,rlTaskError("No 'Status' element"));

			if (status == 1)
			{
				rcheck(m_ProcessResponseCallback 
					,catchall
					,rlTaskError("Response callback not set"));

				//Callback 
				m_ProcessResponseCallback(response, responseLength);

				result = true;
				resultCode = RLSM_ERROR_NONE;
			}
			else
			{
				rlRosHttpTask::ProcessResponse(response, resultCode);
			}
		}
		rcatchall
		{
		}

		return result;
	}

	void rlSaveMigrationDownloadSinglePlayerSave::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		// Error authenticating the ticket.
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);

		// Save migration is disabled.
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "SaveMigration", RLSM_ERROR_DISABLED);

		// Missing or invalid argument.
		SAVEMIGRATION_ERROR_CASE("InvalidArgument", "*", RLSM_ERROR_INVALID_PARAMETER);

		// Title/Platform is not a valid source for saves.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "TitleInfo", RLSM_ERROR_INVALID_TITLE);

		// Not linked to a Rockstar Account.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "RockstarAccount", RLSM_ERROR_ROCKSTAR_ACCOUNT_REQUIRED);

		// Invalid record token
		SAVEMIGRATION_ERROR_CASE("DoesNotExist", "SaveMigrationRecord", RLSM_ERROR_INVALID_TOKEN);

		// The save migration record token has expired
		SAVEMIGRATION_ERROR_CASE("Expired", "SaveMigrationRecordToken", RLSM_ERROR_EXPIRED_TOKEN);

		// The save migration record has expired
		SAVEMIGRATION_ERROR_CASE("Expired", "SaveMigrationRecord", RLSM_ERROR_EXPIRED_SAVE_MIGRATION);
		
		// Unknown error
		resultCode = RLSM_ERROR_UNKNOWN;
	}

	//------------------------------------------------------------------------------

	//------------------------------------------------------------------------------
	// rlSaveMigrationCompleteSingleplayerMigration
	//------------------------------------------------------------------------------

	bool rlSaveMigrationCompleteSingleplayerMigration::Configure(const int localGamerIndex)
	{
		bool success = false;

		rtry
		{
			rlHttpTaskConfig config;
			config.m_HttpVerb = NET_HTTP_VERB_GET;
			rverify(rlRosHttpTask::Configure(localGamerIndex, &config), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

			success = true;
		}
		rcatchall
		{
		}

		return success;
	}

	bool rlSaveMigrationCompleteSingleplayerMigration::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		resultCode = RLSM_ERROR_NONE;
		return true;
	}

	void rlSaveMigrationCompleteSingleplayerMigration::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
	{
		//  .DestinationMigrationComplete 	
		//  . 					.
		//  


		// Error authenticating the ticket.
		SAVEMIGRATION_ERROR_CASE("AuthenticationFailed", "Ticket", RLSM_ERROR_AUTHENTICATION_FAILED);

		// Save migration is disabled.
		SAVEMIGRATION_ERROR_CASE("NotAvailable", "SaveMigration", RLSM_ERROR_DISABLED);

		// Title/Platform is not a valid source for saves.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "TitleInfo", RLSM_ERROR_INVALID_TITLE);

		// Not linked to a Rockstar Account.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "RockstarAccount", RLSM_ERROR_ROCKSTAR_ACCOUNT_REQUIRED);

		// A migration has already been completed to the platform account making the request.
		SAVEMIGRATION_ERROR_CASE("NotAllowed", "DestinationMigrationComplete", RLSM_ERROR_ALREADY_COMPLETE);

		// The player does not have a save migration in progress to complete.
		SAVEMIGRATION_ERROR_CASE("InvalidState", "NotInProgress", RLSM_ERROR_NOT_IN_PROGRESS);

		// Unknown error
		resultCode = RLSM_ERROR_UNKNOWN;
	}
}