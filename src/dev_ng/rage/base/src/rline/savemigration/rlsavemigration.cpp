// 
// rline/savemigration/rlsavemigration.cpp
// 
// Copyright (C) 2021 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsavemigration.h"

#include "diag/seh.h"
#include "rline/ros/rlros.h"
#include "rline/rldiag.h"
#include "rline/rltask.h"
#include "rline/rl.h"

namespace rage
{

	//------------------------------------------------------------------------------
	// rlSaveMigration
	//------------------------------------------------------------------------------

	bool rlSaveMigration::GetMPAccounts(const int localGamerIndex, rlSaveMigrationMPAccountsArray* accounts, netStatus* status)
	{
		rlSaveMigrationGetMPAccounts* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationGetMPAccounts>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, accounts, status), catchall, rlError("Failed to configire task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool  rlSaveMigration::MigrateMPAccount(const int localGamerIndex, const int sourceAccountid, const char* sourcePlatform, rlSaveMigrationState* state, netStatus* status)
	{
		rlSaveMigrationMigrateMPAccount* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationMigrateMPAccount>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, sourceAccountid, sourcePlatform, state, status), catchall, rlError("Failed to configire task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool rlSaveMigration::GetTransactionState(const int localGamerIndex, const int transactionId, rlSaveMigrationState* state, netStatus* status)
	{
		rlSaveMigrationGetTransactionState* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationGetTransactionState>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, transactionId, state, status), catchall, rlError("Failed to configire task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool rlSaveMigration::GetState(const int localGamerIndex, rlSaveMigrationState* state, netStatus* status)
	{
		rlSaveMigrationGetState* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationGetState>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, state, status), catchall, rlError("Failed to configure task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool rlSaveMigration::GetSingleplayerSaveState(const int localGamerIndex, rlSaveMigrationRecordMetadata* recordMetadata, netStatus* status)
	{
		rlSaveMigrationGetSingleplayerSaveState* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationGetSingleplayerSaveState>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, recordMetadata, status), catchall, rlError("Failed to configure task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool rlSaveMigration::UploadSingleplayerSave(const int localGamerIndex
												, const u32 savePosixTime
												, const float completionPercentage
												, const u32 lastCompletedMissionId
												, const u8* data
												, const unsigned size
												, netStatus* status)
	{
		rlSaveMigrationUploadSinglePlayerSave* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationUploadSinglePlayerSave>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, savePosixTime, completionPercentage, lastCompletedMissionId, data, size, status), catchall, rlError("Failed to configure task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool rlSaveMigration::GetAvailableSingleplayerSaves(const int localGamerIndex, atArray< rlSaveMigrationRecordMetadata >* recordMetadata, netStatus* status)
	{
		rlSaveMigrationGetAvailableSingleplayerSaves* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationGetAvailableSingleplayerSaves>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, recordMetadata, status), catchall, rlError("Failed to configure task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool rlSaveMigration::DownloadSingleplayerSave(const int localGamerIndex, const rlSaveMigrationRecordMetadata& record, rlSaveMigrationDownloadSinglePlayerSave::ProcessResponseCallback callback, netStatus* status)
	{
		rlSaveMigrationDownloadSinglePlayerSave* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationDownloadSinglePlayerSave>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, record, callback, status), catchall, rlError("Failed to configure task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}

	bool rlSaveMigration::CompleteSingleplayerMigration(const int localGamerIndex, netStatus* status)
	{
		rlSaveMigrationCompleteSingleplayerMigration* task = NULL;

		rtry
		{
			rverify(status, catchall, rlError("NULL status."));

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index %d.", localGamerIndex));

			task = rlGetTaskManager()->CreateTask<rlSaveMigrationCompleteSingleplayerMigration>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, localGamerIndex, status), catchall, rlError("Failed to configure task."));
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

			return true;
		}
		rcatchall
		{
			if (task)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED, -1);
				}
				else
				{
					status->ForceFailed();
				}

				rlGetTaskManager()->DestroyTask(task);
			}

			return false;
		}
	}


	void rlSaveMigration::Cancel(netStatus* status)
	{
		rlGetTaskManager()->CancelTask(status);
	}
}