#ifndef __TFIT_HMAC_KEY_IHMACREDK_H__
#define __TFIT_HMAC_KEY_IHMACREDK_H__

#include "file/file_config.h"
#if (RSG_CPU_X64 && !__RGSC_DLL && RSG_PC)
#include <stdint.h>

#ifdef __cplusplus
extern "C" { 
#endif

extern uint8_t TFIT_hmac_key_iHMACredK[];
extern unsigned int TFIT_hmac_key_len_iHMACredK;

#ifdef __cplusplus
}
#endif

#endif

#endif
