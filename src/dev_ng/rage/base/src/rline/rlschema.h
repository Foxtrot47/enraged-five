// 
// rline/rlschema.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCHEMA_H
#define RLINE_RLSCHEMA_H

#include "rl.h"

#include <string.h>

namespace rage
{

template< typename T > class rlConcreteSchema;

//PURPOSE
//  A schema is a collection of data fields.
//
//  Schemas are used to pass data to rl APIs that, for example, create,
//  sessions, search for sessions, set rich presence, etc.
//
//  rlSchema provides a generic interface to concrete schemas.
//  Concrete schemas are typically defined in a .h file that is automatically
//  generated from a schema definition file (e.g. *.xlast).
//
//  A concrete schema is provided as the argument to the rlSchema constructor.
//  Schema fields can then be accessed in a generic way using the rlSchema
//  methods.
class rlSchema
{
    template< typename T > friend class rlConcreteSchema;

public:

    enum
    {
        MAX_FIELD_COUNT     = 64,
        MAX_DATA_SIZE       = 256,
        MAX_FIELD_SIZE      = 16,
    };

    enum
    {
        INVALID_FIELD_ID    = -1
    };

    enum FieldType
    {
        FIELDTYPE_INVALID   = -1,
        FIELDTYPE_INT32,
        FIELDTYPE_INT64,
        FIELDTYPE_FLOAT,
        FIELDTYPE_DOUBLE,
    };

    //PURPOSE
    //  Flags passed to Commit()
    enum CommitFlags
    {
        //Disallow nil fields
        COMMIT_NO_NIL_FIELDS    = 0x01,
    };

    rlSchema();

    rlSchema(const rlSchema& that);

    rlSchema& operator=(const rlSchema& that);

    //PURPOSE
    //  Clears a schema of all field data and all field descriptions.
    //NOTES
    //  After calling this function the schema will have a field count of zero.
    //  This function will assert if the schema is a concrete schema.
    void Clear();

    //PURPOSE
    //  Sets all fields to nil.
    //NOTES
    //  This function does not change the field count.  It simply sets the
    //  value of all fields to nil.  Calling this function on a concrete
    //  schema is permitted.
    void ClearFieldData();

    //PURPOSE
    //  Adds a new field with the given id to the schema.
    //RETURNS
    //  Field index of new field, or -1 on failure.
    int AddField(const unsigned newFieldId);

    //PURPOSE
    //  Returns the number of fields in the schema.
    int GetFieldCount() const;

    //PURPOSE
    //  Sets the field at the given index to nil.
    bool SetNil(const int index);

    //PURPOSE
    //  Returns true if the field at the given index is nil (not set).
    bool IsNil(const int index) const;

    //PURPOSE
    //  Returns true if any of the fields are nil.
    bool HasNilFields() const;

    //PURPOSE
    //  Returns true if the schema has a field with the given id.
    bool HasField(const unsigned fieldId) const;

    //PURPOSE
    //  Returns the field id of the field at the given index.
    unsigned GetFieldId(const int index) const;

    //PURPOSE
    //  Returns the field type of the field at the given index.
    int GetFieldType(const int index) const;

    //PURPOSE
    //  Returns the field index of the field with the given id, or
    //  -1 on failure.
    int GetFieldIndex(const unsigned fieldId) const;

    //PURPOSE
    //  Sets the value of the field at the given index.
    //RETURNS
    //  True on success.
    bool SetFieldData(const int index,
                       const void* data,
                       const int dataSize);

    //PURPOSE
    //  Returns the value of the field at the given index.
    const void* GetFieldData(const int index) const;

    //PURPOSE
    //  Copies the value of the field at the given index to the memory pointed
    //  to by dst.  dst must be large enough to hold the data type at the given
    //  index.
    //RETURNS
    //  True on success.
    bool GetFieldData(const int index,
                       void* dst,
                       const int dstSize) const;

    //PURPOSE
    //  Returns the size of the field data at the given index, or -1 for
    //  invalid field index.
    int GetSizeofFieldData(const int index) const;

    //PURPOSE
    //  Returns the total size of all field data.
    int GetTotalSizeofFieldData() const;

    //PURPOSE
    //  Returns the size in bytes of the schema data.
    //NOTES
    //  This is not necessarily the size of the schema buffer (which
    //  may be larger), rather it is the cumulative size of all the fields
    //  and field meta-data in the schema.
    int GetSize() const;

    //PURPOSE
    //  Copies fields from that to this.
    //RETURNS
    //  Number of fields copied.
    //NOTES
    //  If the destination is not a concrete schema then fields from the
    //  source will be added if they don't exist in the destination, even
    //  if they are nil in the source.
    //
    //  Nil fields in the source schema will not cause existing fields in the
    //  destination schema to become nil.
    int CopyFieldsFrom(const rlSchema& that);

    //PURPOSE
    //  Copies fields from that to this.
    //RETURNS
    //  Number of fields copied.
    //NOTES
    //  New fields will not be added.
    //
    //  Nil fields in the source schema will not cause existing fields in the
    //  destination schema to become nil.
    int CopyExistingFieldsFrom(const rlSchema& that);

    //PURPOSE
    //  Commits the schema to the underlying platform-specific API.
    //  This method is typically called only by functions internal to the
    //  RL module.
    bool Commit(const int localGamerIndex,
                 const unsigned flags) const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

#if RSG_PC

    //PURPOSE
    //  Returns the number of Property fields.
    int GetPropertyCount() const;

    //PURPOSE
    //  Returns the number of Context fields.
    int GetContextCount() const;

    //PURPOSE
    //  Returns true if the field at the given index is a Property.
    bool IsProperty(const int index) const;

    //PURPOSE
    //  Returns true if the field at the given index is a Context.
    bool IsContext(const int index) const;
#endif

    //PURPOSE
    //  Returns the size in bytes of a field with the given id.
    static int FieldSizeFromId(const unsigned fieldId);

    //PURPOSE
    //  Returns type of a field with the given id.
    static FieldType FieldTypeFromId(const unsigned fieldId);

protected:

    explicit rlSchema(const bool isConcrete)
        : m_IsConcrete(isConcrete)
    {
    }

    enum SchemaHeader
    {
        OFFSETOF_COUNT  = 0,
        OFFSETOF_MASK   = OFFSETOF_COUNT + sizeof(u32),
        OFFSETOF_FIELDS = OFFSETOF_MASK + sizeof(u64),

        SIZEOF_SCHEMA_HEADER    = OFFSETOF_FIELDS,
    };

    enum FieldHeader
    {
        OFFSETOF_ID     = 0,
        OFFSETOF_DATA   = OFFSETOF_ID + sizeof(u32),

        SIZEOF_FIELD_HEADER     = OFFSETOF_DATA
    };

    struct Field
    {
        unsigned GetId() const
        {
            return *(const u32*) &((const u8*) this)[OFFSETOF_ID];
        }

        void SetId(const unsigned id)
        {
            *(u32*) &((u8*) this)[OFFSETOF_ID] = id;
        }

        int GetSizeofData() const
        {
            return rlSchema::FieldSizeFromId(this->GetId());
        }

        void* GetData()
        {
            return &((u8*) this)[OFFSETOF_DATA];
        }

        const void* GetData() const
        {
            return &((const u8*) this)[OFFSETOF_DATA];
        }
    };

    Field* GetFields()
    {
        return (Field*) &m_Data[OFFSETOF_FIELDS];
    }

    const Field* GetFields() const
    {
        return (const Field*) &m_Data[OFFSETOF_FIELDS];
    }

    //PURPOSE
    //  Returns a pointer to the field at the given index.
    Field* SkipTo(const int index);
    const Field* SkipTo(const int index) const;

    //PURPOSE
    //  Returns a pointer to the field after the given field.
    Field* Next(const Field* field);
    const Field* Next(const Field* field) const;

    //PURPOSE
    //  Sets the bitmask that determines which fields are nil.
    void SetMask(const u64 mask);

    //PURPOSE
    //  Returns the bitmask that determines which fields are nil.
    u64 GetMask() const;

    //PURPOSE
    //  Returns a pointer to the first field in the schema.
    u8* GetData();
    const u8* GetData() const;

private:

    u8 m_Data[MAX_DATA_SIZE];

protected:

    const bool m_IsConcrete     : 1;
};

template< typename T >
class rlConcreteSchema : public rlSchema
{
    CompileTimeAssert(sizeof(T) <= rlSchema::MAX_DATA_SIZE);

public:

    rlConcreteSchema()
        : rlSchema(true)
    {
        CompileTimeAssert(sizeof(m_Data) >= sizeof(T));
        new (m_Data) T;
    }

    rlConcreteSchema(const rlSchema& that)
        : rlSchema(true)
    {
        CompileTimeAssert(sizeof(m_Data) >= sizeof(T));
        new (m_Data) T;
        *this = that;
    }

    T* GetData() { return (T*) m_Data; }
    const T* GetData() const { return (const T*) m_Data; }

    T* operator->()
    {
        return this->GetData();
    }

    const T* operator->() const
    {
        return this->GetData();
    }

    rlConcreteSchema< T >& operator=(const rlSchema& that)
    {
        if(this != &that)
        {
            this->rlSchema::operator=(that);
        }

        return *this;
    }
};

}   //namespace rage

#endif  //RLINE_RLSCHEMA_H
