// 
// rline/rllivestreamevents.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "rllivestreamevents.h"

#if LIVESTREAM_ENABLED

namespace rage
{
	AUTOID_IMPL(rlLiveStreamEvent);
	AUTOID_IMPL(rlLiveStreamInitializedEvent);
	AUTOID_IMPL(rlLiveStreamAuthenticatedEvent);
	AUTOID_IMPL(rlLiveStreamLoggedInEvent);
	AUTOID_IMPL(rlLiveStreamFoundIngestServerEvent);
	AUTOID_IMPL(rlLiveStreamAuthenticatingEvent);
	AUTOID_IMPL(rlLiveStreamStartedEvent);
	AUTOID_IMPL(rlLiveStreamStoppedEvent);
	AUTOID_IMPL(rlLiveStreamPausedEvent);
	AUTOID_IMPL(rlLiveStreamUnPausedEvent);
	AUTOID_IMPL(rlLiveStreamUninitialized);

	/////////////////////////////////////////////////////////////
	// rlLiveStreamInitializedEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamInitializedEvent::rlLiveStreamInitializedEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamAuthenticatingEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamAuthenticatingEvent::rlLiveStreamAuthenticatingEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamAuthenticatedEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamAuthenticatedEvent::rlLiveStreamAuthenticatedEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamLoggedInEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamLoggedInEvent::rlLiveStreamLoggedInEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamFoundIngestServerEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamFoundIngestServerEvent :: rlLiveStreamFoundIngestServerEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamStartedEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamStartedEvent::rlLiveStreamStartedEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamStoppedEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamStoppedEvent::rlLiveStreamStoppedEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamPausedEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamPausedEvent::rlLiveStreamPausedEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamUnPausedEvent
	/////////////////////////////////////////////////////////////
	rlLiveStreamUnPausedEvent::rlLiveStreamUnPausedEvent(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

	/////////////////////////////////////////////////////////////
	// rlLiveStreamUninitialized
	/////////////////////////////////////////////////////////////
	rlLiveStreamUninitialized::rlLiveStreamUninitialized(StreamPlatform platform)
		: rlLiveStreamEvent(platform)
	{

	}

}   //namespace rage

#endif // LIVESTREAM_ENABLED