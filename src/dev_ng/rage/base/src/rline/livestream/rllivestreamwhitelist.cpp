// 
// rline/rllivestreamwhitelist.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "rllivestreamwhitelist.h"
#include "rllivestream.h"

#if LIVESTREAM_ENABLED

#include "rline/cloud/rlcloud.h"
#include "data/growbuffer.h"
#include "diag/channel.h"
#include "diag/seh.h"
#include "net/task.h"
#include "rline/rl.h"
#include "rline/rldiag.h"
#include "system/param.h"

namespace rage
{
	// Channels
	RAGE_DEFINE_SUBCHANNEL(rline, livestreamwhitelist)
	#undef __rage_channel
	#define __rage_channel rline_livestreamwhitelist

	// Globals
	extern sysMemAllocator* g_rlAllocator;

	/////////////////////////////////////////////////////////////////////////
	// rlLiveStreamGetWhitelistTask
	/////////////////////////////////////////////////////////////////////////
	rlLiveStreamGetWhitelistTask::rlLiveStreamGetWhitelistTask()
		: m_State(STATE_GET_WHITELIST)
		, m_LocalGamerIndex(-1)
	{

	}

	bool rlLiveStreamGetWhitelistTask::Configure(const int localGamerIndex)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			m_State = STATE_GET_WHITELIST;
			m_LocalGamerIndex = localGamerIndex;

			// Initialize the growbuffer
			m_GrowBuffer.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlLiveStreamGetWhitelistTask::OnCancel()
	{
		if (m_CloudStatus.Pending())
		{
			netTask::Cancel(&m_CloudStatus);
		}
	}

	netTaskStatus rlLiveStreamGetWhitelistTask::OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_GET_WHITELIST:
			{
				if(WasCanceled())									
				{			
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if (GetWhiteList())
				{
					m_State = STATE_GETTING_WHITELIST;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		case STATE_GETTING_WHITELIST:
			{
				if (m_CloudStatus.Succeeded())
				{
					status = NET_TASKSTATUS_SUCCEEDED;
				}
				else if (!m_CloudStatus.Pending())
				{
					rlTaskError("Could not retrieve live stream white list");
					status = NET_TASKSTATUS_FAILED;
				}
			}
		default:
			break;
		}

		if (status != NET_TASKSTATUS_PENDING)
		{
			Complete(status);
		}

		return status;
	}

	bool rlLiveStreamGetWhitelistTask::GetWhiteList()
	{
		rtry
		{
			rverify(rlCloud::GetTitleFile(GetPath(),
											RLROS_SECURITY_DEFAULT,
											0,  //ifModifiedSincePosixTime
											m_GrowBuffer.GetFiDevice(),
											m_GrowBuffer.GetFiHandle(),
											NULL,   //rlCloudFileInfo
											NULL,   //allocator
											&m_CloudStatus), 
											catchall, );

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlLiveStreamGetWhitelistTask::Complete(netTaskStatus status)
	{
		if (!WasCanceled() && status == NET_TASKSTATUS_SUCCEEDED)
		{
			rtry
			{
				RsonReader rr((const char*)m_GrowBuffer.GetBuffer(), m_GrowBuffer.Length());

				RsonReader rrAllowed;
				rverify(rr.GetMember("whitelist", &rrAllowed), catchall, );

				//Get the gamer list
				RsonReader rrProfile;
				rverify(rrAllowed.GetFirstMember(&rrProfile),  catchall, );

				bool done = false;
				for(int i = 0; i < rlLiveStreamWhitelistLimits::MAX_WHITELIST_SIZE && !done; ++i, done = !rrProfile.GetNextSibling(&rrProfile))
				{
					rlLiveStreamWhiteListEntry e;

					RsonReader rrSocialClub;
					rrProfile.GetMember("rgsc", &rrSocialClub);					
					rrSocialClub.AsString(e.GamerTag, RL_MAX_NAME_BUF_SIZE);

					RsonReader rrTwitch;
					rrProfile.GetMember("twitch", &rrTwitch);
					rrTwitch.AsString(e.StreamAccount, rlLiveStreamWhitelistLimits::MAX_STREAM_NAME_SIZE);

					rlLiveStream::AddWhiteListEntry(e);
				}
			}
			rcatchall
			{

			}
		}
	}

	/////////////////////////////////////////////////////////////////////////
	// rlLiveStreamWhitelist
	/////////////////////////////////////////////////////////////////////////
	rlLiveStreamWhitelist::rlLiveStreamWhitelist()
	{
		m_bNeedsToUpdateWhitelist = true;
		m_bRetrievedWhitelist = true;
	}

	void rlLiveStreamWhitelist::Update()
	{
		rtry
		{
			rcheck(m_bNeedsToUpdateWhitelist, catchall, );

			const int localGamerIndex = rlPresence::GetActingUserIndex();
			rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rcheck(rlPresence::IsOnline(localGamerIndex), catchall, );
			rcheck(m_WhiteListStatus.None(), catchall, );

			rverify(GetWhiteList(), catchall, "Failed to create white list retrieval task");
			m_bNeedsToUpdateWhitelist = false;
		}
		rcatchall
		{

		}

		// Check the status object
		if (m_WhiteListStatus.Succeeded())
		{
			m_bRetrievedWhitelist = true;
			m_WhiteListStatus.Reset();
		}
		else if (m_WhiteListStatus.Failed())
		{
			m_bRetrievedWhitelist = false;
			m_WhiteListStatus.Reset();
		}
	}

	bool rlLiveStreamWhitelist::GetWhiteList()
	{
		rtry
		{
			rlLiveStreamGetWhitelistTask* task;
			rverify(netTask::Create(&task, &m_WhiteListStatus), catchall, );
			rverify(task->Configure(rlPresence::GetActingUserIndex()), catchall, );
			rverify(netTask::Run(task), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlLiveStreamWhitelist::IsWhitelisted(const char* streamName)
	{
		rtry
		{
			rverify(m_bRetrievedWhitelist, catchall, );

			rlLiveStreamWhiteListEntry* entry = NULL;

			for(int i = 0; i < m_WhiteList.GetCount(); i++)
			{
				if (!strcmp(m_WhiteList[i].StreamAccount, streamName))
				{
					entry = &m_WhiteList[i];
					break;
				}
			}

			if (entry != NULL)
			{
				rlGamertag gamerTag;
				rlPresence::GetName(rlPresence::GetActingUserIndex(), gamerTag);
				if (!strcmp(gamerTag, entry->GamerTag))
				{
					return true;
				}
			}
		}
		rcatchall
		{
			
		}

		return false;
	}

	bool rlLiveStreamWhitelist::AddWhiteListEntry(rlLiveStreamWhiteListEntry e)
	{
		rtry
		{
			rverify(m_WhiteList.GetCount() < MAX_WHITELIST_SIZE, catchall, );
			m_WhiteList.PushAndGrow(e);
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

}   //namespace rage

#endif // LIVESTREAM_ENABLED