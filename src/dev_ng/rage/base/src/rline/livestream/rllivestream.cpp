// 
// rline/rllivestream.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "rllivestream.h"
#include "rllivestreamwhitelist.h"

#if LIVESTREAM_ENABLED

#include "rline/cloud/rlcloud.h"
#include "data/growbuffer.h"
#include "diag/channel.h"
#include "diag/seh.h"
#include "net/task.h"
#include "rline/rl.h"
#include "rline/rldiag.h"
#include "system/param.h"

namespace rage
{
	// Channels
	RAGE_DEFINE_SUBCHANNEL(rline, livestream)
	#undef __rage_channel
	#define __rage_channel rline_livestream

	// Globals
	extern sysMemAllocator* g_rlAllocator;

	// Static Members
	rlLiveStreamWhitelist rlLiveStream::sm_WhiteList;
	rlLiveStream::Delegator	rlLiveStream::sm_Delegator;
	void (*rlLiveStream::sm_VideoCaptureCallback)(unsigned, unsigned, unsigned char * );

	// Stream-Platform Specific
	rlTwitchManager rlLiveStream::sm_TwitchManager;

	/////////////////////////////////////////////////////////////////////////
	// rlLiveStream
	/////////////////////////////////////////////////////////////////////////
	bool rlLiveStream::InitClass()
	{
		bool bSuccess = false;

		rtry
		{
			rverify(sm_TwitchManager.Init(), catchall, );

			bSuccess = true;
		}
		rcatchall
		{
				
		}

		return bSuccess;
	}


	bool rlLiveStream::ShutdownClass()
	{
		sm_TwitchManager.Shutdown();
		return true;
	}

	void rlLiveStream::Update()
	{
		sm_WhiteList.Update();
		sm_TwitchManager.Update();
	}

	bool rlLiveStream::IsInitialized()
	{
		return sm_TwitchManager.IsInitialized();
	}

	bool rlLiveStream::AddWhiteListEntry(rlLiveStreamWhiteListEntry e)
	{
		rlDebug3("Adding whitelist entry for RGSC(%s) and Twitch(%s)", e.GamerTag, e.StreamAccount);
		return sm_WhiteList.AddWhiteListEntry(e);
	}

	bool rlLiveStream::Authenticate(const char* username, const char* password)
	{
		rtry
		{
			rverify(sm_WhiteList.IsWhitelisted(username), catchall, rlError("User: {%s} is not whitelisted for live streaming", username));
			return sm_TwitchManager.Authenticate(username, password);
		}
		rcatchall
		{
			return false;
		}
	}

	int rlLiveStream::GetNumIngestServers()
	{
		return sm_TwitchManager.GetNumIngestServers();
	}


	int rlLiveStream::GetDefaultIngestServerIndex()
	{
		return sm_TwitchManager.GetDefaultIngestServerIndex();
	}

	const char* rlLiveStream::GetIngestServer(int index)
	{
		return sm_TwitchManager.GetIngestServer(index);
	}

	bool rlLiveStream::IsAuthenticating()
	{
		return sm_TwitchManager.IsAuthenticating();
	}

	bool rlLiveStream::IsAuthenticated()
	{
		return sm_TwitchManager.IsAuthenticated();
	}

	bool rlLiveStream::IsStreaming()
	{
		return sm_TwitchManager.IsStreaming();
	}

	bool rlLiveStream::IsPaused()
	{
		return sm_TwitchManager.IsPaused();
	}

	bool rlLiveStream::StartStreaming(unsigned ingestServerIndex, unsigned outputWidth, unsigned outputHeight, unsigned targetFPS)
	{
		return sm_TwitchManager.StartStreaming(ingestServerIndex, outputWidth, outputHeight, targetFPS);
	}

	bool rlLiveStream::StopStreaming()
	{
		return sm_TwitchManager.StopStreaming();
	}

	bool rlLiveStream::TogglePaused()
	{
		return sm_TwitchManager.TogglePaused();
	}

	void rlLiveStream::PostPresentCallback()
	{
		rtry
		{
			rcheck(sm_TwitchManager.CanSubmitVideoFrame(), catchall, );
			rcheck(sm_VideoCaptureCallback != NULL, catchall, );

			unsigned char* buffer = sm_TwitchManager.GetNextBuffer();
			rverify(buffer, catchall, );

			sm_VideoCaptureCallback(sm_TwitchManager.GetWidth(), sm_TwitchManager.GetHeight(), buffer);
		}
		rcatchall
		{

		}
	}

	void rlLiveStream::SubmitVideoFrame(unsigned char* buffer)
	{
		sm_TwitchManager.SubmitVideoFrame(buffer);
	}

	void rlLiveStream::DispatchEvent(const rlLiveStreamEvent* e)
	{
		rlDebug("Dispatching '%s'", e->GetName());
		sm_Delegator.Dispatch(e);
	}

	void rlLiveStream::AddDelegate(Delegate* dlgt)
	{
		sm_Delegator.AddDelegate(dlgt);
	}

	void rlLiveStream::RemoveDelegate(Delegate* dlgt)
	{
		sm_Delegator.RemoveDelegate(dlgt);
	}

}   //namespace rage

#endif // LIVESTREAM_ENABLED