// 
// rline/rlLiveStream.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLTWITCH_H
#define RLINE_RLTWITCH_H

// RAGE Headers
#include "atl/string.h"
#include "atl/vector.h"
#include "rline/rl.h"
#include "system/memory.h"
#include "system/simpleallocator.h"

#if LIVESTREAM_ENABLED

// Twitch Defines
#define TTV_PLATFORM_MAC 0
#define TTV_PLATFORM_IOS 0
#define TARGET_OS_IPHONE 0
#define TARGET_IPHONE_SIMULATOR 0

// Twitch Headers
#include "../../3rdParty/twitch/include/twitchsdk.h"

namespace rage
{
	class rlTwitchManager
	{
	public:

		enum TwitchConsts
		{
			// TODO: Test on high resolutions, initialize only for streamers, etc.
			TWITCH_HEAP_SIZE = 1024 * 1024 * 60, // 60 MB
			TWITCH_CAPTURE_BUFFERS = 3
		};

		enum StreamState
		{
			SS_Uninitialized,
			SS_Initialized,
			SS_Authenticating,
			SS_Authenticated,
			SS_LoggingIn,
			SS_LoggedIn,
			SS_FindingIngestServer,
			SS_FoundIngestServer,
			SS_ReadyToStream,
			SS_Streaming,
			SS_Paused,
			SS_MAX
		};

		rlTwitchManager();
		~rlTwitchManager();

		// PURPOSE
		// Initializes Twitch SDK
		bool Init();

		// PURPOSE
		// Updates the Twitch Manager
		void Update();

		// PURPOSE
		// Shuts down the Twitch SDK
		bool Shutdown();
		 
		// PURPOSE 
		// Sets the Twitch Manager into a new state
		void SetState(StreamState state);

		// PURPOSE
		// Returns TRUE if initialized
		bool IsInitialized() { return m_StreamState >= SS_Initialized; }

		// PURPOSE
		// Authenticates with Twitch, returns TRUE if successful
		bool Authenticate(const char* userName, const char* password);

		// PURPOSE
		//  Returns TRUE if we have ingest servers
		bool HasIngestServers() { return m_StreamState >= SS_FoundIngestServer; }

		//PURPOSE
		//  Returns the number of ingest servers
		int GetNumIngestServers();

		// PURPOSE
		//  returns the default ingest server index (-1 if not found)
		int GetDefaultIngestServerIndex();

		//PURPOSE
		//	Returns the ingest server at a given index
		const char* GetIngestServer(int index);

		// PURPOSE
		//	Returns the requested stream settings
		unsigned GetWidth() { return m_VideoParams.outputWidth; }
		unsigned GetHeight() { return m_VideoParams.outputHeight; }

		// PURPOSE
		// Starts streaming, allocates capture buffers.
		bool StartStreaming(unsigned ingestServerIndex, unsigned outputWidth, unsigned outputHeight, unsigned targetFPS);

		// PURPOSE
		// Stops Streaming
		bool StopStreaming();

		// PURPOSE
		// Pauses streaming
		bool TogglePaused();

		// PURPOSE
		//	Returns TRUE if the manager is in the requested state
		bool IsAuthenticating();
		bool IsAuthenticated();
		bool IsStreaming();
		bool IsPaused();

		// PURPOSE
		// Returns the next free buffer
		unsigned char* GetNextBuffer();

		// PURPOSE
		//	Returns TRUE if the manager is ready to accept and submit video frames
		bool CanSubmitVideoFrame();

		// PURPOSE
		// Submits a video frame to the twitch SDK
		bool SubmitVideoFrame(unsigned char* buffer);

		// PURPOSE
		//	Returns the string representation of the stream state
		const char* GetState();

	private:

		// PURPOSE
		// Memory callbacks
		static void* AllocCallback(size_t size, size_t alignment);
		static void FreeCallback(void* ptr);
		void FreeBuffer(unsigned char* buffer);

		// Standard Callbacks
		static void LoginCallback(TTV_ErrorCode result, void* userData);
		static void IngestListCallback(TTV_ErrorCode result, void* userData);
		static void UserInfoDoneCallback(TTV_ErrorCode result, void* userData);
		static void StreamInfoDoneCallback(TTV_ErrorCode result, void* userData);
		static void AuthDoneCallback(TTV_ErrorCode result, void* userData);
		static void FrameUnlockCallback(const uint8_t* buffer, void* userData);

		// State
		StreamState m_StreamState;

		// TTV Structs
		TTV_MemCallbacks m_MemCallbacks;// The memory callbacks for twitch sdk
		TTV_AuthToken m_AuthToken;		// The unique key that allows the client to stream.
		TTV_ChannelInfo m_ChannelInfo;	// The information about the channel associated with the auth token
		TTV_IngestServer m_IngestServer;// The ingest server to use.
		TTV_IngestList m_IngestList;	// Will contain valid data the callback triggered due to TTV_GetIngestServers is called.
		TTV_UserInfo m_UserInfo;		// Profile information about the local user.
		TTV_StreamInfo m_StreamInfo;	// Information about the stream the user is streaming on.
		TTV_VideoParams m_VideoParams;  // Video parameters for streaming

		// Memory
		static sysMemSimpleAllocator* sm_TwitchAllocator;

		// Login
		atString m_Username;
		atString m_Password;
		atString m_ClientId;
		atString m_ClientSecret;

		// Stream Buffers
		atVector<unsigned char*> m_FreeBufferList;	// The List of Free buffers
		atVector<unsigned char*> m_CaptureBuffers;	// The List of all Buffers
	};
}

#endif // LIVESTREAM_ENABLED
#endif // RLINE_RLTWITCH_H
