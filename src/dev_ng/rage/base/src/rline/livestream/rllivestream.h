// 
// rline/rlLiveStream.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLLIVESTREAM_H
#define RLINE_RLLIVESTREAM_H

// Livestream includes
#include "rllivestreamwhitelist.h"
#include "rltwitch.h"
#include "rllivestreamevents.h"

// Rage Headers
#include "atl/array.h"
#include "atl/delegate.h"
#include "data/growbuffer.h"
#include "diag/channel.h"
#include "net/status.h"
#include "net/time.h"
#include "net/task.h"
#include "rline/rl.h"

#if LIVESTREAM_ENABLED

namespace rage
{
	// channel decl
	RAGE_DECLARE_SUBCHANNEL(rline, livestream)

	class rlLiveStream
	{
	public:
		typedef atDelegator<void (const rlLiveStreamEvent*)> Delegator;
		typedef Delegator::Delegate Delegate;

		//PURPOSE
		//	Initializes the rlLiveStream manager
		//	Cannot be re-initialized.
		static bool InitClass();

		//PURPOSE
		//	Updates the rlLiveStream state machine
		static void Update();

		//PURPOSE
		//	Shuts down the rlLiveStream manager
		static bool ShutdownClass();

		//PURPOSE
		// Returns TRUE if the rlLiveStream is initialized
		static bool IsInitialized();

		//PURPOSE
		//  Adds a whitelist entry
		static bool AddWhiteListEntry(rlLiveStreamWhiteListEntry e);

		//PURPOSE
		//  Authenticates the given stream username and password
		static bool Authenticate(const char* username, const char* password);

		//PURPOSE
		//  Returns the number of ingest servers
		static int GetNumIngestServers();

		// PURPOSE
		//  returns the default ingest server index (-1 if not found)
		static int GetDefaultIngestServerIndex();

		// PURPOSE
		//  Returns TRUE if the stream manager is in the requested state
		static bool IsAuthenticating();
		static bool IsAuthenticated();
		static bool IsStreaming();
		static bool IsPaused();

		//PURPOSE
		//	Returns the ingest server at a given index
		static const char* GetIngestServer(int index);

		// PURPOSE
		//  Starts, Stops or Pauses Streaming
		static bool StartStreaming(unsigned ingestServerIndex, unsigned outputWidth, unsigned outputHeight, unsigned targetFPS);
		static bool StopStreaming();
		static bool TogglePaused();

		// PURPOSE
		// Submits a video frame to the twitch SDK
		static void PostPresentCallback();
		static void SubmitVideoFrame(unsigned char* buffer);

		//PURPOSE
		//  Add or remove a delegate that will be called with event notifications.
		static void AddDelegate(Delegate* dlgt);
		static void RemoveDelegate(Delegate* dlgt);

		//PURPOSE
		//	Dispatches rlFriendEvents 
		static void DispatchEvent(const rlLiveStreamEvent* e);

		// PURPOSE
		//  The callback to be used for capturing video to submit while streaming
		static void (*sm_VideoCaptureCallback)(unsigned width, unsigned height, unsigned char* buffer);

		// PURPOSE
		//	Returns the string representation of the stream state
		static const char* GetState() { return sm_TwitchManager.GetState(); }

	private:

		static rlLiveStreamWhitelist sm_WhiteList;
		static Delegator sm_Delegator;

		// Stream-Platform specific members
		static rlTwitchManager sm_TwitchManager;
	};

}   //namespace rage

#else
class rlLiveStream
{
public:
	static bool InitClass() { return true; }
	static void Update() {}
	static bool ShutdownClass()  { return true; }

private:
};
#endif // LIVESTREAM_ENABLED
#endif // RLINE_RLLIVESTREAM_H
