// 
// rline/rllivestreamwhitelist.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLLIVESTREAMWHITELIST_H
#define RLINE_RLLIVESTREAMWHITELIST_H

#include "rline/rl.h"

// Rage Headers
#include "atl/array.h"
#include "data/growbuffer.h"
#include "diag/channel.h"
#include "net/status.h"
#include "net/time.h"
#include "net/task.h"

#if LIVESTREAM_ENABLED

namespace rage
{
	RAGE_DECLARE_SUBCHANNEL(rline, livestreamwhitelist)

	enum rlLiveStreamWhitelistLimits
	{
		MAX_WHITELIST_SIZE = 10,
		MAX_STREAM_NAME_SIZE = 25
	};

	struct rlLiveStreamWhiteListEntry
	{
		char StreamAccount[MAX_STREAM_NAME_SIZE];
		rlGamertag GamerTag;
	};

	class rlLiveStreamGetWhitelistTask : public netTask
	{
	public:

		NET_TASK_DECL(rlLiveStreamGetWhitelistTask);
		NET_TASK_USE_CHANNEL(rline_livestreamwhitelist);

		rlLiveStreamGetWhitelistTask();

		bool Configure(const int localGamerIndex);
		virtual void OnCancel();
		virtual netTaskStatus OnUpdate(int* /*resultCode*/);

	private:

		bool GetWhiteList();
		void Complete(netTaskStatus status);

		const char* GetPath()
		{
			return "/twitch/whitelist.json";
		}

		enum eState
		{
			STATE_GET_WHITELIST,
			STATE_GETTING_WHITELIST
		};

		eState m_State;
		netStatus m_CloudStatus;
		datGrowBuffer m_GrowBuffer;
		int m_LocalGamerIndex;
	};

	class rlLiveStreamWhitelist
	{
		friend class rlLiveStreamGetWhitelistTask;

	public:

		// PURPOSE
		// Constructor
		rlLiveStreamWhitelist();

		//PURPOSE
		// Adds an entry to the whitelist
		bool AddWhiteListEntry(rlLiveStreamWhiteListEntry e);

		//PURPOSE
		// Updates the whitelist
		void Update();

		// PURPOSE
		// Requests a download of the whitelist
		bool GetWhiteList();

		// PURPOSE
		// Returns true if a stream name is whitelisted
		bool IsWhitelisted(const char* streamName);

	private:

		bool m_bNeedsToUpdateWhitelist;
		bool m_bRetrievedWhitelist;
		netStatus m_WhiteListStatus;

		atArray<rlLiveStreamWhiteListEntry> m_WhiteList;
	};

}   //namespace rage

#endif // LIVESTREAM_ENABLED
#endif // RLINE_RLLIVESTREAMWHITELIST_H