// 
// rline/rlxlsp.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "rl.h"

#if __LIVE

#include "rlxlsp.h"
#include <algorithm>
#include "data/rson.h"
#include "diag/seh.h"
#include "math/random.h"
#include "net/netaddress.h"
#include "net/nethardware.h"
#include "net/time.h"
#include "rldiag.h"
#include "string/string.h"
#include "system/criticalsection.h"
#include "system/memops.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"
#include "system/xtl.h"

#include <time.h>

/*
** NOTE **

Currently we rely on users of rlXlspManager to throttle their requests to
resolve server address.  We don't do any throttling but will assert if requests
are made too frequently.
*/

namespace rage
{

PARAM(rlxlspclusterfilter, "Must match the \"cluster\" element of the SG userdata string.  Used primarily to limit SG selection to a single cluster for local debugging.");
PARAM(rlxlspdiscoverytimeout, "Server discover timeout in seconds.  Overrides the application defined timeout");

RAGE_DEFINE_SUBCHANNEL(rline, xlsp)
#undef __rage_channel
#define __rage_channel rline_xlsp

//////////////////////////////////////////////////////////////////////////
//  rlXlspSecureGateway
//////////////////////////////////////////////////////////////////////////
class rlXlspSecureGateway
{
public:
    rlXlspSecureGateway();

    void Init(const netIpAddress& publicIp,
            const netIpAddress& secureIp);

    void Clear();

    netIpAddress m_PublicIp;
    netIpAddress m_SecureIp;
};

//////////////////////////////////////////////////////////////////////////
//  rlXlspResolveRequest
//////////////////////////////////////////////////////////////////////////
class rlXlspResolveRequest
{
public:
    enum { MAX_NAME_CHARS = 20 };

    rlXlspResolveRequest();
    ~rlXlspResolveRequest();

    //PURPOSE
    //  Initializes a host resolution by name.
    bool Init(const char* serverName,
                netSocketAddress* serverAddr,
                const unsigned timeoutSecs,
                netStatus* status);

    //PURPOSE
    //  Initializes a host resolution by public IP.
    bool Init(const netIpAddress& publicIp,
                const u16 port,
                netSocketAddress* serverAddr,
                const unsigned timeoutSecs,
                netStatus* status);

    //PURPOSE
    //  Returns true if the operation is pending.
    bool Pending() const;

    //PURPOSE
    //  Clears to initial values
    void Clear();

    void Link(rlXlspResolveRequest* next);
    void Unlink();

    enum State
    {
        STATE_NONE,
        STATE_WAITING,
    };

    State m_State;

    netTimeout m_Timeout;
    char m_ServerName[MAX_NAME_CHARS];
    netSocketAddress* m_SecureAddr;
    netIpAddress m_PublicIp;
    netIpAddress m_SecureIp;
    netStatus* m_Status;

    rlXlspResolveRequest* m_Next;
    rlXlspResolveRequest* m_Prev;

    u16 m_Port;

private:
    rlXlspResolveRequest(const rlXlspResolveRequest&);
    rlXlspResolveRequest& operator=(const rlXlspResolveRequest&);
};

//////////////////////////////////////////////////////////////////////////
//  rlXlspSecureGateway Implementation
//////////////////////////////////////////////////////////////////////////
rlXlspSecureGateway::rlXlspSecureGateway()
: m_PublicIp()
, m_SecureIp()
{
}

void 
rlXlspSecureGateway::Init(const netIpAddress& publicIp, const netIpAddress& secureIp)
{
    rlAssert((m_PublicIp.IsValid() == false) && (m_SecureIp.IsValid() == false));

    m_PublicIp = publicIp;
    m_SecureIp = secureIp;
}

void
rlXlspSecureGateway::Clear()
{
	m_PublicIp.Clear();
	m_SecureIp.Clear();
}

//////////////////////////////////////////////////////////////////////////
//  rlXlspResolveRequest Implementation
//////////////////////////////////////////////////////////////////////////
rlXlspResolveRequest::rlXlspResolveRequest()
: m_SecureAddr(NULL)
, m_Status(NULL)
{
    m_Next = m_Prev = this;
    this->Clear();
}

rlXlspResolveRequest::~rlXlspResolveRequest()
{
    rlAssert(!this->Pending());

    if(this->Pending())
    {
        rlXlspManager::Cancel(m_Status);
    }
}

bool
rlXlspResolveRequest::Pending() const
{
    return NULL != m_Status;
}

void
rlXlspResolveRequest::Clear()
{
    rlAssert(!this->Pending());

    if(this->Pending())
    {
        rlXlspManager::Cancel(m_Status);
    }

    m_State = STATE_NONE;
    m_ServerName[0] = '\0';
	m_PublicIp.Clear();
	m_SecureIp.Clear();
    m_Port = 0;
    m_Status = NULL;
    m_SecureAddr = NULL;
}

bool
rlXlspResolveRequest::Init(const char* serverName,
                            netSocketAddress* serverAddr,
                            const unsigned timeoutSecs,
                            netStatus* status)
{
    rlAssert(!this->Pending());

    this->Clear();

    rlAssert(strlen(serverName) < COUNTOF(m_ServerName));

    safecpy(m_ServerName, serverName);
    unsigned tmpTimeout;
    if(!PARAM_rlxlspdiscoverytimeout.Get(tmpTimeout))
    {
        m_Timeout.InitSeconds(timeoutSecs);
    }
    else
    {
        m_Timeout.InitSeconds(tmpTimeout);
    }
    m_SecureAddr = serverAddr;
    m_Status = status;
    m_Status->SetPending();
    m_State = STATE_WAITING;

    return true;
}

bool
rlXlspResolveRequest::Init(const netIpAddress& publicIp,
                            const u16 port,
                            netSocketAddress* serverAddr,
                            const unsigned timeoutSecs,
                            netStatus* status)
{
    rlAssert(!this->Pending());

    this->Clear();

    unsigned tmpTimeout;
    if(!PARAM_rlxlspdiscoverytimeout.Get(tmpTimeout))
    {
        m_Timeout.InitSeconds(timeoutSecs);
    }
    else
    {
        m_Timeout.InitSeconds(tmpTimeout);
    }
    m_PublicIp = publicIp;
    m_Port = port;
    m_SecureAddr = serverAddr;
    m_Status = status;
    m_Status->SetPending();
    m_State = STATE_WAITING;

    return true;
}

void
rlXlspResolveRequest::Link(rlXlspResolveRequest* next)
{
    if(rlVerify(m_Next == m_Prev && m_Next == this))
    {
        m_Next = next;
        m_Prev = next->m_Prev;
        m_Next->m_Prev = m_Prev->m_Next = this;
    }
}

void
rlXlspResolveRequest::Unlink()
{
    m_Next->m_Prev = m_Prev;
    m_Prev->m_Next = m_Next;
    m_Next = m_Prev = this;
}

//////////////////////////////////////////////////////////////////////////
//  rlXlspManager
//////////////////////////////////////////////////////////////////////////
enum rlXlspManagerState
{
    MGRSTATE_INITIAL,
    MGRSTATE_DISCOVERING_SERVERS,
    MGRSTATE_ONLINE
};

rlXlspManagerState s_MgrState = MGRSTATE_INITIAL;

//Maximum number of SGs
static const int MAX_SECURE_GATEWAYS    = 64;
//Maximum number of pending resolve requests. This should match 
//MAX_NET_RESOLVE_REQUEST_QUEUE_LENGTH in resolver.cpp
static const int MAX_RESOLVE_REQUEST_QUEUE_LENGTH   = 8;

static HANDLE s_hServerEnum             = INVALID_HANDLE_VALUE;
static rlXlspEnvironment s_XlspEnv      = RLXLSP_ENV_UNKNOWN;
static unsigned s_ServiceId             = 0;
static rlXovStatus s_XovStatus;
static netStatus s_MyStatus;

static sysCriticalSectionToken s_RlXlspCs;

//Cache of discovered title servers.  Periodically refreshed on demand
static XTITLE_SERVER_INFO s_XTitleServerCache[MAX_SECURE_GATEWAYS];
static unsigned s_NumXTitleServerInfos;
static netTimeout s_XTitleServerDiscoveryRetryTimer;
static const unsigned XTITLE_SERVER_INFO_RETRY_SECONDS  = 60;

//Connected secure gateways
static rlXlspSecureGateway s_ConnectedSgs[MAX_SECURE_GATEWAYS];
static unsigned s_NumConnectedSgs = 0;

//Pending host resolution requests
static rlXlspResolveRequest s_ResolveReqPool[MAX_RESOLVE_REQUEST_QUEUE_LENGTH]; //The pool
static rlXlspResolveRequest s_FreeRequests;                                     //The free list
static rlXlspResolveRequest s_PendingRequests;                                  //Pending requests

static bool s_RlXlspInitialized = false;

bool
rlXlspManager::Init()
{
    SYS_CS_SYNC(s_RlXlspCs);

    bool success = false;

    if(rlVerifyf(!s_RlXlspInitialized, "rlXlspManager Already initialized"))
    {
        rlAssert(INVALID_HANDLE_VALUE == s_hServerEnum);

        for(int i = 0; i < COUNTOF(s_ConnectedSgs); ++i)
        {
            s_ConnectedSgs[i].Clear();
        }

        //Add requests to the free list.
        for(int i = 0; i < COUNTOF(s_ResolveReqPool); ++i)
        {
            s_ResolveReqPool[i].Unlink();
            s_ResolveReqPool[i].Link(&s_FreeRequests);
        }

        s_XlspEnv = RLXLSP_ENV_UNKNOWN;
        s_ServiceId = 0;
        s_NumXTitleServerInfos = 0;
        s_NumConnectedSgs = 0;

        //First time through set this to timed out
        //so we attempt a server discovery immediately.
        s_XTitleServerDiscoveryRetryTimer.ForceTimeout();

        s_MgrState = MGRSTATE_INITIAL;

        success = s_RlXlspInitialized = true;
    }

    return success;
}

void
rlXlspManager::Shutdown()
{
    SYS_CS_SYNC(s_RlXlspCs);

    if(s_RlXlspInitialized)
    {
        while(&s_PendingRequests != s_PendingRequests.m_Next)
        {
            rlXlspManager::Cancel(s_PendingRequests.m_Next->m_Status);
        }

        while(s_NumConnectedSgs)
        {
            rlXlspManager::Disconnect(s_ConnectedSgs[0].m_SecureIp);
        }

        if(s_XovStatus.Pending())
        {
            s_XovStatus.Cancel();
        }

        if(INVALID_HANDLE_VALUE != s_hServerEnum)
        {
            XCloseHandle(s_hServerEnum);
            s_hServerEnum = INVALID_HANDLE_VALUE;
        }

        s_XlspEnv = RLXLSP_ENV_UNKNOWN;
        s_ServiceId = 0;
        s_NumXTitleServerInfos = 0;

        s_RlXlspInitialized = false;
    }
}

bool
rlXlspManager::IsInitialized()
{
    return s_RlXlspInitialized;
}

void
rlXlspManager::Update()
{
    SYS_CS_SYNC(s_RlXlspCs);

    s_XTitleServerDiscoveryRetryTimer.Update();

    switch(s_MgrState)
    {
    case MGRSTATE_INITIAL:
        if(netHardware::IsOnline() && s_XTitleServerDiscoveryRetryTimer.IsTimedOut())
        {
            rlDebug("Discovering servers...");
            if(rlVerify(BeginDiscovery()))
            {
                s_XTitleServerDiscoveryRetryTimer.InitSeconds(XTITLE_SERVER_INFO_RETRY_SECONDS);
                s_MgrState = MGRSTATE_DISCOVERING_SERVERS;
            }
        }
        break;
    case MGRSTATE_DISCOVERING_SERVERS:
        s_XovStatus.Update();
        if(s_MyStatus.Succeeded())
        {
            rlXlspManager::EndDiscovery();

            if(GetEnvironment() != RLXLSP_ENV_UNKNOWN)
            {
                rlDebug("Discovered %d servers in environment: \"%s\"",
                        s_NumXTitleServerInfos,
                        GetEnvironmentName(GetEnvironment()));

                s_MgrState = MGRSTATE_ONLINE;
            }
            else
            {
                rlError("EndDiscovery() failed, will retry in %d seconds.",
                        s_XTitleServerDiscoveryRetryTimer.GetSecondsUntilTimeout());
                s_NumXTitleServerInfos = 0;
                s_MgrState = MGRSTATE_INITIAL;
            }
        }
        else if(!s_MyStatus.Pending())
        {
			if(INVALID_HANDLE_VALUE != s_hServerEnum)
			{
				XCloseHandle(s_hServerEnum);
				s_hServerEnum = INVALID_HANDLE_VALUE;
			}

			rlXovError("Server discovery failed.",
                        s_XovStatus.GetResult(),
                        s_XovStatus.ToXov());
			rlDebug("Will retry in %d seconds.",
                        s_XTitleServerDiscoveryRetryTimer.GetSecondsUntilTimeout());
            s_NumXTitleServerInfos = 0;
            s_MgrState = MGRSTATE_INITIAL;
        }
        break;
    case MGRSTATE_ONLINE:
        if(!netHardware::IsOnline())
        {
            s_XTitleServerDiscoveryRetryTimer.ForceTimeout();
            s_NumXTitleServerInfos = 0;
            s_MgrState = MGRSTATE_INITIAL;
        }
        break;
    }

    ProcessPendingRequests();

    //Maintain connected servers
    for(int i = 0; i < (int)s_NumConnectedSgs; ++i)
    {
        const rlXlspSecureGateway& sg = s_ConnectedSgs[i];
        IN_ADDR in_addr;

        in_addr.s_addr = sg.m_SecureIp.ToV4().ToU32();

        const DWORD result = XNetGetConnectStatus(in_addr);

        rlAssert(XNET_CONNECT_STATUS_CONNECTED == result
                || XNET_CONNECT_STATUS_PENDING == result
                || XNET_CONNECT_STATUS_LOST == result);

        if(XNET_CONNECT_STATUS_CONNECTED != result
            && XNET_CONNECT_STATUS_PENDING != result)
        {
            rlDebug("Lost connection to server: " NET_IP_FMT " (" NET_IP_FMT ")", 
                        NET_IP_FOR_PRINTF(sg.m_PublicIp),
                        NET_IP_FOR_PRINTF(sg.m_SecureIp));

            rlXlspManager::Disconnect(sg.m_SecureIp);

            //Disconnect removes the server
            --i;
        }
    }
}

void
rlXlspManager::Disconnect(const netIpAddress& secureIp)
{
    SYS_CS_SYNC(s_RlXlspCs);

    rlXlspSecureGateway* sg = FindSgBySecureIp(secureIp);
    if(rlVerifyf(sg, "No SG at IP " NET_IP_FMT, NET_IP_FOR_PRINTF(secureIp)))
    {
        rlDebug("Disconnecting from server: " NET_IP_FMT " (" NET_IP_FMT ")", 
                    NET_IP_FOR_PRINTF(sg->m_PublicIp),
                    NET_IP_FOR_PRINTF(sg->m_SecureIp));

        IN_ADDR in_addr;
        in_addr.s_addr = secureIp.ToV4().ToU32();

        XNetUnregisterInAddr(in_addr);

        *sg = s_ConnectedSgs[s_NumConnectedSgs-1];
        s_ConnectedSgs[s_NumConnectedSgs-1].Clear();
        --s_NumConnectedSgs;
    }
}

bool
rlXlspManager::ResolveServerAddr(const char* name,
                                netSocketAddress* secureAddr,
                                const unsigned timeoutSecs,
                                netStatus* status)
{
    SYS_CS_SYNC(s_RlXlspCs);

    bool success = false;

    rlDebug2("Resolving server:\"%s\", timeout: %u seconds...", name, timeoutSecs);

    if(rlVerifyf(s_RlXlspInitialized, "Not initialized"))
    {
        netIpAddress publicIp, secureIp;
        u16 port;

        //Have we discovered a server by that name?
        if(FindPublicIpAndPortByName(name, &publicIp, &port))
        {
            //Are we connected to it?
            if(FindSecureIpByPublicIp(publicIp, &secureIp))
        {
                secureAddr->Init(secureIp, port);
            rlDebug2("  Already connected");
            rlDebug2("Resolved server:\"%s\" addr="NET_ADDR_FMT,
                        name,
                        NET_ADDR_FOR_PRINTF(*secureAddr));

            status->ForceSucceeded();
            success = true;
        }
            //Not connected - try connecting
            else if(ConnectToServer(publicIp, port, secureAddr))
            {
                status->ForceSucceeded();
                success = true;
            }
            else
            {
                rlError("ConnectToServer() failed");
            }
        }
        else if(0 == s_NumXTitleServerInfos)
        {
            //Wait for title servers to be discovered

            rlXlspResolveRequest* resreq = s_FreeRequests.m_Next;

            if(&s_FreeRequests == resreq)
            {
                rlError("Pool of resolve requests is exhausted");
            }
            else if(resreq->Init(name, secureAddr, timeoutSecs, status))
            {
                resreq->Unlink();
                resreq->Link(&s_PendingRequests);
                success = true;
            }
        }
        else
        {
            //No servers by that name.
            rlError("There are no servers named \"%s\"", name);
        }
    }

    if(!success){status->ForceFailed();}

    return success;
}

bool
rlXlspManager::ConnectToServer(const netIpAddress& publicIp,
                                const u16 port,
                                netSocketAddress* secureAddr)
{
    SYS_CS_SYNC(s_RlXlspCs);

    bool success = false;

    rtry
    {
        rverify(s_RlXlspInitialized, catchall, rlError("Not initialized"));
        rverify(0 != s_ServiceId, catchall, rlError("Invalid service ID"))
        rverify(publicIp.IsValid(), catchall, rlError("Invalid server IP"));

        //Are we already connected?
        netIpAddress secureIp;
        if(FindSecureIpByPublicIp(publicIp, &secureIp))
        {
            secureAddr->Init(secureIp, port);
            success = true;
        }
        else
        {
            rlDebug2("Connecting to server: " NET_IP_FMT ":%u...",
                    NET_IP_FOR_PRINTF(publicIp), port);

            rcheck(s_NumConnectedSgs < COUNTOF(s_ConnectedSgs),
                    catchall,
                    rlError("SG pool is exhausted"));

            IN_ADDR in_addr;
            in_addr.s_addr = XSocketHTONL(publicIp.ToV4().ToU32());

			unsigned secureIpV4 = 0;
            CompileTimeAssert(sizeof(IN_ADDR) == sizeof(secureIpV4));

            int result = XNetServerToInAddr(in_addr, s_ServiceId, (IN_ADDR*)&secureIpV4);

            rcheck(!result,
                   catchall,
                   rlError("XNetServerToInAddr failed (%d)", result));

			secureIp = netIpAddress(netIpV4Address(secureIpV4));
            rlDebug2("  Secure IP:" NET_IP_FMT, NET_IP_FOR_PRINTF(secureIp));

            result = XNetConnect(*(IN_ADDR*)&secureIpV4);
            rcheck(!result,
                   catchall,
                   rlError("XNetConnect failed (%d)", result));

            rlDebug2("Connected to server: " NET_IP_FMT " (" NET_IP_FMT ")",
                        NET_IP_FOR_PRINTF(publicIp),
                        NET_IP_FOR_PRINTF(secureIp));

            s_ConnectedSgs[s_NumConnectedSgs].Init(publicIp, secureIp);

            ++s_NumConnectedSgs;

            secureAddr->Init(secureIp, port);

            success = true;
        }
    }
    rcatchall
    {
    }

    return success;
}

void
rlXlspManager::Cancel(netStatus* status)
{
    SYS_CS_SYNC(s_RlXlspCs);

    for(rlXlspResolveRequest* rqst = s_PendingRequests.m_Next;
        &s_PendingRequests != rqst;
        rqst = rqst->m_Next)
    {
        if(rqst->m_Status == status)
        {
            rlDebug("Canceled request for \"%s\"", rqst->m_ServerName);
            rqst->m_Status->SetCanceled();
            rqst->m_Status = NULL;

            if(rqst->m_SecureIp.IsValid())
            {
				unsigned secureIpV4 = rqst->m_SecureIp.ToV4().ToU32();
                CompileTimeAssert(sizeof(IN_ADDR) == sizeof(secureIpV4));
                XNetUnregisterInAddr(*(IN_ADDR*)&secureIpV4);
            }

            rqst->Clear();

            rqst->Unlink();
            rqst->Link(&s_FreeRequests);

            break;
        }
    }
}

rlXlspEnvironment
rlXlspManager::GetEnvironment()
{
    return s_XlspEnv;
}

const char*
rlXlspManager::GetEnvironmentName(const rlXlspEnvironment env)
{
    switch(env)
    {
    case RLXLSP_ENV_PARTNET: return "RLXLSP_ENV_PARTNET"; break;
    case RLXLSP_ENV_CERTNET: return "RLXLSP_ENV_CERTNET"; break;
	case RLXLSP_ENV_CERTNET_2: return "RLXLSP_ENV_CERTNET2"; break;
    case RLXLSP_ENV_PRODNET: return "RLXLSP_ENV_PRODNET"; break;
    case RLXLSP_ENV_UNKNOWN:
    default:
         return "RLXLSP_ENV_UNKNOWN"; break;
    }
}

bool
rlXlspManager::GetPublicIpFromSecureIp(const netIpAddress& secureIp, netIpAddress* publicIp)
{
    //Secure IPs have 0 in the top octet
    rlAssert(0 == (0xFF000000 & secureIp.ToV4().ToU32()));

    return FindPublicIpBySecureIp(secureIp, publicIp);
}

bool
rlXlspManager::GetSecureIpFromPublicIp(const netIpAddress& publicIp, netIpAddress* secureIp)
{
    //Public IPs do not have 0 in the top octet
    rlAssert(0 != (0xFF000000 & publicIp.ToV4().ToU32()));

    return FindSecureIpByPublicIp(publicIp, secureIp);
}

//private:

void
rlXlspManager::ProcessPendingRequests()
{
    SYS_CS_SYNC(s_RlXlspCs);

    //Check for request timeouts
    rlXlspResolveRequest* rqst = s_PendingRequests.m_Next;
    rlXlspResolveRequest* next = rqst->m_Next;

    for(; &s_PendingRequests != rqst; rqst = next, next = next->m_Next)
    {
        rqst->m_Timeout.Update();

        if(rqst->m_Timeout.IsTimedOut())
        {
            rlError("Request for \"%s\" timed out after %d seconds",
                    rqst->m_ServerName,
                    rqst->m_Timeout.GetTimeoutIntervalSeconds());
            CompleteRequest(rqst, netStatus::NET_STATUS_FAILED);
        }
    }

    rqst = s_PendingRequests.m_Next;

    if(&s_PendingRequests != rqst && MGRSTATE_ONLINE == s_MgrState)
    {
        switch(rqst->m_State)
        {
        case rlXlspResolveRequest::STATE_NONE:
            rlError("Invalid request state: %d", rqst->m_State);
            rlAssert(false);
            CompleteRequest(rqst, netStatus::NET_STATUS_FAILED);
            break;

        case rlXlspResolveRequest::STATE_WAITING:
            //Have we already completed a server discovery?
            if(s_NumXTitleServerInfos)
            {
                rlDebug("Resolving address for \"%s\"...", rqst->m_ServerName);

                //Are we already connected?
                if(FindPublicIpAndPortByName(rqst->m_ServerName, &rqst->m_PublicIp, &rqst->m_Port))
                {
                    if(FindSecureIpByPublicIp(rqst->m_PublicIp, &rqst->m_SecureIp))
                    {
                        rqst->m_SecureAddr->Init(rqst->m_SecureIp, rqst->m_Port);

                        rlDebug("  Already connected");
                        CompleteRequest(rqst, netStatus::NET_STATUS_SUCCEEDED);
                    }
                    //We discovered a server with that name, but we're
                    //not yet connected
                    else if(ConnectToServer(rqst->m_PublicIp, rqst->m_Port, rqst->m_SecureAddr))
                    {
                        CompleteRequest(rqst, netStatus::NET_STATUS_SUCCEEDED);
                    }
                    else
                    {
                        rlError("ConnectToServer() failed");
                        CompleteRequest(rqst, netStatus::NET_STATUS_FAILED);
                    }
                }
                else
                {
                    //No servers by that name.
                    rlError("There are no servers named \"%s\"", rqst->m_ServerName);
                    CompleteRequest(rqst, netStatus::NET_STATUS_FAILED);
                }
            }
            else
            {
                //Wait for server discovery to complete
            }
            break;
        }
    }
}

bool
rlXlspManager::FindPublicIpAndPortByName(const char* titleServerName,
                                    netIpAddress* publicIp,
                                    u16* port)
{
    SYS_CS_SYNC(s_RlXlspCs);

    for(int i = 0; i < (int)s_NumXTitleServerInfos; ++i)
    {
        const XTITLE_SERVER_INFO* xsf = &s_XTitleServerCache[i];

        RsonReader rr;
        if(!rlVerifyf(rr.Init(xsf->szServerInfo, strlen(xsf->szServerInfo)),
                    "Invalid RSON string: \"%s\"", xsf->szServerInfo))
        {
            continue;
        }

        bool more = rr.GetMember("ts", &rr);
        if(more)
        {
            char name[200];

            //Protect from possible infinite loop.
            for(int j = 0;
                more && rr.GetName(name) && !strcmp("ts", name) && j < 100;
                ++j, more = rr.GetNextSibling(&rr))
            {
                char tsName[rlXlspResolveRequest::MAX_NAME_CHARS];
                unsigned tsPort;

                if(!rlVerifyf(rr.ReadString("n", tsName, sizeof(tsName)),
                            "Error reading \"n\"")
                    || !rlVerifyf(rr.ReadUns("p", tsPort),
                                    "Error reading \"p\""))
                {
                    continue;
                }

                rlAssert(tsPort <= 0xFFFF);

                if(!stricmp(titleServerName, tsName))
                {
                    *publicIp = netIpAddress(netIpV4Address(XSocketNTOHL(xsf->inaServer.s_addr)));
                    *port = (u16)tsPort;
                    return true;
                }
            }
        }
    }

    return false;
}

rlXlspSecureGateway*
rlXlspManager::FindSgByPublicIp(const netIpAddress& publicIp)
{
    SYS_CS_SYNC(s_RlXlspCs);

    for(int i = 0; i < (int)s_NumConnectedSgs; ++i)
    {
        if(publicIp == s_ConnectedSgs[i].m_PublicIp)
        {
            return &s_ConnectedSgs[i];
        }
    }

    return NULL;
}

rlXlspSecureGateway*
rlXlspManager::FindSgBySecureIp(const netIpAddress& secureIp)
{
    SYS_CS_SYNC(s_RlXlspCs);

    for(int i = 0; i < (int)s_NumConnectedSgs; ++i)
    {
        if(secureIp == s_ConnectedSgs[i].m_SecureIp)
        {
            return &s_ConnectedSgs[i];
        }
    }

    return NULL;
}

bool
rlXlspManager::FindSecureIpByPublicIp(const netIpAddress& publicIp, netIpAddress* secureIp)
{
    SYS_CS_SYNC(s_RlXlspCs);

    //Public IPs do not have 0 in the top octet
    rlAssert(0 != (0xFF000000 & publicIp.ToV4().ToU32()));

    rlXlspSecureGateway* sg = FindSgByPublicIp(publicIp);
    if(sg)
    {
        *secureIp = sg->m_SecureIp;
        rlAssert(secureIp->IsValid());
        return true;
    }

    return false;
}

bool
rlXlspManager::FindPublicIpBySecureIp(const netIpAddress& secureIp, netIpAddress* publicIp)
{
    SYS_CS_SYNC(s_RlXlspCs);

    //Secure IPs have 0 in the top octet
    rlAssert(0 == (0xFF000000 & secureIp.ToV4().ToU32()));

    rlXlspSecureGateway* sg = FindSgBySecureIp(secureIp);
    if(sg)
    {
        *publicIp = sg->m_PublicIp;
        rlAssert(publicIp->IsValid());
        return true;
    }

    return false;
}

void
rlXlspManager::CompleteRequest(rlXlspResolveRequest* resReq,
                                netStatus::StatusCode statusCode)
{
    SYS_CS_SYNC(s_RlXlspCs);

    if(resReq && rlVerify(resReq->Pending()))
    {
        if(netStatus::NET_STATUS_SUCCEEDED == statusCode)
        {
            rlDebug("Resolved server:\"%s\" addr:" NET_IP_FMT " (" NET_IP_FMT ")",
                        resReq->m_ServerName,
                        NET_IP_FOR_PRINTF(resReq->m_PublicIp),
                        NET_IP_FOR_PRINTF(resReq->m_SecureIp));
        }
        else
        {
            rlError("Failed to resolve server:\"%s\"", resReq->m_ServerName);
        }

        //If this request was canceled m_Status will be NULL.
        if(resReq->m_Status)
        {
            resReq->m_Status->SetStatus(statusCode);
        }
        resReq->m_SecureAddr = NULL;
        resReq->m_Status = NULL;
        resReq->Unlink();
        resReq->Link(&s_FreeRequests);
    }
}

bool
rlXlspManager::BeginDiscovery()
{
    SYS_CS_SYNC(s_RlXlspCs);

    bool success = false;

    rtry
    {
        rcheck(netHardware::IsOnline(),
                catchall,
                rlError("Can't discover title servers - console is offline"));

        rlDebug("Discovering title servers...");

        s_NumXTitleServerInfos = 0;

        rverify(INVALID_HANDLE_VALUE == s_hServerEnum, catchall,);

        DWORD dwBufferSize = 0;
        DWORD dwResult = XTitleServerCreateEnumerator(0, //We can't use userdata matching, since we use it for attributes
                                                      COUNTOF(s_XTitleServerCache), 
                                                      &dwBufferSize, 
                                                      &s_hServerEnum);

        rlAssert(dwBufferSize == sizeof(s_XTitleServerCache));
        
        rcheck(ERROR_SUCCESS == dwResult,
               catchall,
               rlError("XTitleServerCreateEnumerator failed with 0x%0x.", dwResult));

        s_XovStatus.Begin("XEnumerate", &s_MyStatus);

        dwResult = XEnumerate(s_hServerEnum, 
                              s_XTitleServerCache, 
                              dwBufferSize, 
                              NULL, 
                              s_XovStatus.ToXov());

        rcheck(ERROR_SUCCESS == dwResult || ERROR_IO_PENDING == dwResult,
               catchall,
               rlXovError("XEnumerate failed to enumerate servers",
                            dwResult, s_XovStatus.ToXov()));

        success = true;
    }
    rcatchall
    {
        if(INVALID_HANDLE_VALUE != s_hServerEnum)
        {
            XCloseHandle(s_hServerEnum);
            s_hServerEnum = INVALID_HANDLE_VALUE;
        }
    }

    return success;
}

void
rlXlspManager::EndDiscovery()
{
    SYS_CS_SYNC(s_RlXlspCs);

    rtry
    {
        rverify(0 == s_NumXTitleServerInfos,
                catchall,
                rlError("Already have title server infos - why did we discover again???"));

        rverify(INVALID_HANDLE_VALUE != s_hServerEnum, catchall,);
        rverify(!s_XovStatus.Pending(), catchall,);

        XCloseHandle(s_hServerEnum);
        s_hServerEnum = INVALID_HANDLE_VALUE;

        rcheck(ERROR_SUCCESS == s_XovStatus.GetResult(),
                catchall,
               rlXovError("XEnumerate failed to enumerate SGs",
                            s_XovStatus.GetResult(),
                            s_XovStatus.ToXov()));

        s_NumXTitleServerInfos = s_XovStatus.ToXov()->InternalHigh;

        rlDebug("Discovered %d title servers", s_NumXTitleServerInfos);

        const char* clusterFilter = NULL;
        PARAM_rlxlspclusterfilter.Get(clusterFilter);

        for(unsigned i = 0; i < s_NumXTitleServerInfos; i++)
        {
            rlDebug("Server %d:", i);

            const XTITLE_SERVER_INFO* xsf = &s_XTitleServerCache[i];

            rlDebug("  userdata(%u/%u chars)=\"%s\"",
                         strlen(xsf->szServerInfo),
                         sizeof(xsf->szServerInfo),
                         xsf->szServerInfo);

            rlDebug("  ip=" NET_IP_FMT, NET_IP_FOR_PRINTF(netIpAddress(netIpV4Address(XSocketNTOHL(xsf->inaServer.s_addr)))));

            RsonReader rr;
            RsonReader rrChild;
            char rrChildName[200];
            char serverName[rlXlspResolveRequest::MAX_NAME_CHARS] = {""};
            char clusterName[rlXlspResolveRequest::MAX_NAME_CHARS] = {""};
            //unsigned titleId = 0;
            unsigned serviceId = 0;
            char envName[64] = {""};
            rlXlspEnvironment env = RLXLSP_ENV_UNKNOWN;

            if(!rlVerifyf(rr.Init(xsf->szServerInfo, strlen(xsf->szServerInfo)),
                        "Invalid RSON string: \"%s\"", xsf->szServerInfo)
                || !rlVerifyf(rr.ReadUns("svcid", serviceId), "Error reading \"svcid\"")
                //|| !rlVerifyf(rr.ReadUns("titleid", titleId), "Error reading \"titleid\"")
                || !rlVerifyf(rr.ReadString("cluster", clusterName), "Error reading \"cluster\"")
                || !rlVerifyf(rr.ReadString("name", serverName), "Error reading \"name\"")
                || !rlVerifyf(rr.ReadString("env", envName), "Error reading \"env\""))
            {
                s_XTitleServerCache[i] = s_XTitleServerCache[s_NumXTitleServerInfos-1];
                --s_NumXTitleServerInfos;
                --i;
                continue;
            }

            //rlDebug("   service ID/title ID= 0x%08X/0x%08X", serviceId, titleId);
            rlDebug("   service ID = 0x%08X", serviceId);
            rlDebug("   cluster = \"%s\"", clusterName);
            rlDebug("   server = \"%s\"", serverName);

            //Filter out unwanted title servers
            if(clusterFilter && '\0' != clusterFilter[0])
            {
                if(0 != stricmp(clusterName, clusterFilter))
                {
                    rlDebug("Server filtered out (clusterFilter=%s)", clusterFilter);
                    s_XTitleServerCache[i] = s_XTitleServerCache[s_NumXTitleServerInfos-1];
                    --s_NumXTitleServerInfos;
                    --i;
                    continue;
                }
            }

            if(!stricmp(envName, "part"))
            {
                env = RLXLSP_ENV_PARTNET;
            }
            else if(!stricmp(envName, "cert"))
            {
                env = RLXLSP_ENV_CERTNET;
            }
			else if(!stricmp(envName, "cert2"))
			{
				env = RLXLSP_ENV_CERTNET_2;
			}
            else if(!stricmp(envName, "prod"))
            {
                env = RLXLSP_ENV_PRODNET;
            }
            else
            {
                rlError("Unknown XLSP environment id:\"%s\"", envName);
                s_XTitleServerCache[i] = s_XTitleServerCache[s_NumXTitleServerInfos-1];
                --s_NumXTitleServerInfos;
                --i;
                continue;
            }

            rlDebug("   env=\"%s\"", rlXlspManager::GetEnvironmentName(env));

            if(RLXLSP_ENV_UNKNOWN == s_XlspEnv)
            {
                s_XlspEnv = env;
                s_ServiceId = serviceId;
            }

            if(GetEnvironment() != env
                && RLXLSP_ENV_UNKNOWN != GetEnvironment())
            {
                rlWarning("Server %d's environment (%s) doesn't match environment for other servers (%s), skipping",
                        i,
                        rlXlspManager::GetEnvironmentName(env),
                        rlXlspManager::GetEnvironmentName(GetEnvironment()));
                s_XTitleServerCache[i] = s_XTitleServerCache[s_NumXTitleServerInfos-1];
                --s_NumXTitleServerInfos;
                --i;
                continue;
            }

            bool more = rr.GetMember("ts", &rrChild);
            if(more)
            {
                //Protect from possible infinite loop.
                for(int j = 0;
                    more && rrChild.GetName(rrChildName) && !strcmp("ts", rrChildName) && j < 100;
                    ++j, more = rrChild.GetNextSibling(&rrChild))
                {
                    char tsName[rlXlspResolveRequest::MAX_NAME_CHARS];
                    unsigned tsPort;

                    if(!rlVerifyf(rrChild.ReadString("n", tsName, sizeof(tsName)),
                                "Error reading \"n\"")
                        || !rlVerifyf(rrChild.ReadUns("p", tsPort),
                                        "Error reading \"p\""))
                    {
                        continue;
                    }

                    rlDebug("      title server=\"%s\", port=%u", tsName, tsPort);

                    rlAssert(tsPort <= 0xFFFF);
                }
            }
        }

        if(s_NumXTitleServerInfos > 0)
        {
            //Shuffle the title servers to effectively load balance.
            std::random_shuffle(&s_XTitleServerCache[0], &s_XTitleServerCache[s_NumXTitleServerInfos]);
        }
    }
    rcatchall
    {
    }
}

};

#endif //__LIVE
