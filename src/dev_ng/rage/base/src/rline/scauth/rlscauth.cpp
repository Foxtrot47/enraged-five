// 
// rline/rlScAuth.cpp
// 
// Copyright (C) 2017 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscauth.h"

#include "net/http.h"
#include "rline/rltitleid.h"
#include "rline/scpresence/rlscpresence.h"
#include "rline/rlsystemui.h"
#include "rline/socialclub/rlsocialclub.h"
#include "string/stringutil.h"
#include "system/appcontent.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/timer.h"

#if RSG_ORBIS
#include <net.h>
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, scauth)
#undef __rage_channel
#define __rage_channel rline_scauth

PARAM(scAuthStagingEnv, "Force an environment for ScAuth linking outside of the normal environment (i.e. -scAuthStagingEnv=stage-dev, -scAuthStagingEnv=stage-prod)");

extern const rlTitleId* g_rlTitleId;

bool rlScAuth::s_UseLinkTokens = rlScAuth::DEFAULT_LINK_TOKENS_ENABLED;

// Turn ScAuth ON by default
bool rlScAuth::sm_bEnabled = true;

// 'Disable Exit' by default
bool rlScAuth::sm_bDisableExit = true;

char rlScAuth::sm_TitleId[RLROS_MAX_TITLE_NAME_SIZE] = {0};

rlScAuth::rlScAuth()
	: m_State(STATE_NONE)
	, m_AuthMode(SIGN_IN)
	, m_bInitialized(false)
	, m_LocalGamerIndex(RL_INVALID_GAMER_INDEX)
	, m_WebBrowserShowTime(0)
	, m_CredentialsRefreshTime(0)
	, m_Status(nullptr)
	, m_bUseFullScreen(false)
	, m_bUseSafeRegion(false)
	, m_bUseHeader(false)
	, m_bUseFooter(false)
	, m_bLinkSuccessful(false)
	, m_bCredentialsRefreshed(false)
{
	m_Url[0] = '\0';
	m_CallbackUrl[0] = '\0';
	m_LinkToken[0] = '\0';
}

rlScAuth::~rlScAuth()
{
	Shutdown();
}

bool rlScAuth::Init(const int localGamerIndex, AuthMode mode, netStatus* status)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverifyall(!m_bInitialized);

		// Ensure the user has a valid ticket...
		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		rverifyall(creds.IsValid());

		// ...but is not already linked (valid rockstar id) if signin/signup
		if (mode == SIGN_IN || mode == SIGN_UP)
		{
			rverify(creds.GetRockstarId() == RL_INVALID_ROCKSTAR_ID, catchall, rlError("Already linked to social club"));
		}
		else if (mode == UNLINK)
		{
			rverify(creds.GetRockstarId() != RL_INVALID_ROCKSTAR_ID, catchall, rlError("Not linked to social club"));
		}

		// Register presence delegate
		m_PresenceDlgt.Bind(this, &rlScAuth::OnPresenceEvent);
		rlPresence::AddDelegate(&m_PresenceDlgt);

		// Register ROS delegate
		m_RosDlgt.Bind(this, &rlScAuth::OnRosEvent);
		rlRos::AddDelegate(&m_RosDlgt);

		// Setup properties
		m_LocalGamerIndex = localGamerIndex;
		m_AuthMode = mode;
		m_State = rlScAuth::UseLinkTokens() ? STATE_GET_LINK_TOKEN : STATE_SHOW_WEB_BROWSER;

		// Reset timers
		m_WebBrowserShowTime = 0;
		m_CredentialsRefreshTime = 0;
		m_bLinkSuccessful = false;
		m_bCredentialsRefreshed = false;

		// If a status object is attached, store it and set it to pending.
		if (status)
		{
			m_Status = status;
			m_Status->SetPending();
		}

		m_bInitialized = true;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlScAuth::Shutdown()
{
	if (m_PresenceDlgt.IsBound())
	{
		m_PresenceDlgt.Unbind();
		rlPresence::RemoveDelegate(&m_PresenceDlgt);
	}

	if (m_RosDlgt.IsBound())
	{
		m_RosDlgt.Unbind();
		rlRos::RemoveDelegate(&m_RosDlgt);
	}
	 
	m_LocalGamerIndex = RL_INVALID_GAMER_INDEX;
	m_WebBrowserShowTime = 0;
	m_CredentialsRefreshTime = 0;
	m_bLinkSuccessful = false;
	m_bCredentialsRefreshed = false;
	m_State = STATE_NONE;
	m_Status = nullptr;

	m_bInitialized = false;
}

bool rlScAuth::IsAvailable(const int localGamerIndex) const
{
	// We could be unavailable because an operation is already in progress.
	if (IsInitialized())
		return false;

	// A ticket is required for linking to Social Club. Other checks (i.e. platform privileges?) could also go here.
	const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
	return creds.IsValid();
}

void rlScAuth::Update()
{
	switch (m_State)
	{
	case STATE_NONE:
		break;
	case STATE_GET_LINK_TOKEN:
		if (GetLinkToken())
		{
			m_State = STATE_GETTING_LINK_TOKEN;
		}
		else
		{
			OnError();
		}
		break;
	case STATE_GETTING_LINK_TOKEN:
		if (!m_MyStatus.Pending())
		{
			if (m_MyStatus.Succeeded())
			{
				m_State = STATE_SHOW_WEB_BROWSER;
			}
			else
			{
				OnError();
			}
		}
		break;
	case STATE_SHOW_WEB_BROWSER:
		if (ShowBrowser())
		{
			m_State = STATE_SHOWING_WEB_BROWSER;
		}
		else
		{
			OnError();
		}
		break;
	case STATE_SHOWING_WEB_BROWSER:
		if (g_SystemUi.IsUiShowing() ORBIS_ONLY( || g_rlNp.GetCommonDialog().IsDialogShowing()))
		{
			// Update any time the web browser is showing
			m_WebBrowserShowTime = sysTimer::GetSystemMsTime() | 0x01;
		}
		// Legal policy have no presence callbacks, so we can complete immediately.
		else if (m_AuthMode == LEGAL)
		{
			rlDebug1("Legal policy UI closed, proceeding to completion state.");
			OnComplete();
		}
		else if (m_bLinkSuccessful)
		{
			if (m_bCredentialsRefreshed)
			{
				rlDebug1("Link/Unlink was successful and browser is closed with updated credentials. Checking results.");
				m_State = STATE_RENEWING_CREDENTIALS;
			}
			else
			{
				rlDebug1("Link/Unlink was successful and browser is closed - attempting a credentials refresh.");
				m_State = STATE_RENEW_CREDENTIALS;
			}
		}
		else if (BrowserTimedOut())
		{
			// Attempt to renew our credentials anyway
			rlDebug1("Browser timed out -- attempting a credentials refresh in case the presence message was missed.");
			m_State = STATE_RENEW_CREDENTIALS;
		}
		break;
	case STATE_RENEW_CREDENTIALS:
		{
			m_CredentialsRefreshTime = sysTimer::GetSystemMsTime() | 0x01;
			m_State = STATE_RENEWING_CREDENTIALS;

			rlDebug1("Renewing credentials.");
			rlRos::RenewCredentials(m_LocalGamerIndex);
		}
		break;
	case STATE_RENEWING_CREDENTIALS:
		if (m_bCredentialsRefreshed)
		{
			const rlRosCredentials& creds = rlRos::GetCredentials(m_LocalGamerIndex);

			// Unlink -> Wants no rockstar ID to indicate success.
			// Signin/Signup/Legal -> Wants a valid rockstar ID to indicate success
			if (m_AuthMode == UNLINK)
			{
				if (creds.GetRockstarId() == RL_INVALID_ROCKSTAR_ID)
				{
					rlDebug1("No valid rockstar id, unlink was a success.");
					OnComplete();
				}
				else
				{
					rlDebug1("Valid rockstar ID, unlink was not performed");
					OnError();
				}
			}
			else
			{
				if (creds.GetRockstarId() == RL_INVALID_ROCKSTAR_ID)
				{
					rlDebug1("No valid rockstar id, link was not complete.");
					OnError();
				}
				else
				{
					rlDebug1("Successfully linked or signed in.");
					OnComplete();
				}
			}
		}
		else if (CredentialsTimedOut())
		{
			rlError("Timed out waiting for new credentials.");
			OnError();
		}
		break;
	case STATE_ERROR:
		break;
	case STATE_SUCCESS:
		break;
	}
}

bool rlScAuth::IsInitialized() const
{
	return m_bInitialized;
}

bool rlScAuth::IsComplete() const
{
	return m_State == STATE_SUCCESS;
}

bool rlScAuth::HadError() const
{
	return m_State == STATE_ERROR;
}

bool rlScAuth::IsShowingBrowser() const
{
	return m_State == STATE_SHOW_WEB_BROWSER || m_State == STATE_SHOWING_WEB_BROWSER;
}

bool rlScAuth::IsRenewingCredentials() const
{
	return m_State == STATE_RENEW_CREDENTIALS || m_State == STATE_RENEWING_CREDENTIALS;
}

void rlScAuth::SetTitleId(const char* titleId)
{
	rlAssert(!StringNullOrEmpty(titleId));
	safecpy(sm_TitleId, titleId);

	rlDebug1("ScAuth will now use '%s' when generating the client ID", sm_TitleId);
}

bool rlScAuth::FormatWeblinkUri(char* result, const size_t resultBufSize, const int localGamerIndex, const char* feature, const char* action, const char* identifier)
{
	rtry
	{
		rverifyall(result != nullptr && feature != nullptr && action != nullptr && identifier != nullptr);

		//For now this is hard-coded. If we get an endpoint to get this I'll replace it. In that case we'll probably have to 'implement' the SocialClubMgr for the companion app
		const char* uriTemplate = "/go/<feature>/<action>/<identifier>?cid=<clientId>&playerTicket=<playerTicket>&lang=<lang>";

		// Ensure we have a valid ticket
		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		rverifyall(creds.IsValid());

		// As it's safer for now we append the hostname the same way other methods do. If one day there's an endpoint to get the full url this may change.
		char hostname[RL_MAX_URL_BUF_LENGTH];
		rverifyall(rlScAuth::FormatHostName(hostname, RL_MAX_URL_BUF_LENGTH));

		atString build;
		build.Reserve(static_cast<u16>(RLROS_MAX_TICKET_SIZE) + static_cast<u16>(strlen(uriTemplate) * 2) + static_cast<u16>(strlen(hostname)));
		build = "https://";
		build += hostname;
		build += uriTemplate;

		char cidBuf[RLROS_MAX_TITLE_NAME_SIZE + RLROS_MAX_PLATFORM_NAME_SIZE + 8] = { 0 };
		rverifyall(rlScAuth::FormatClientId(cidBuf, sizeof(cidBuf)));

		char ticketBuf[RLROS_MAX_TICKET_SIZE] = { 0 };
		unsigned inLen = RLROS_MAX_TICKET_SIZE, dstLen = 0;
		rverifyall(netHttpRequest::UrlEncode(&ticketBuf[0], &inLen, creds.GetTicket(), (int)strlen(creds.GetTicket()), &dstLen, NULL));

		const sysLanguage lang = rlGetLanguage();

		build.Replace("<feature>", feature);
		build.Replace("<action>", action);
		build.Replace("<identifier>", identifier);

		build.Replace("<clientId>", cidBuf);
		build.Replace("<playerTicket>", ticketBuf);

		// ScWeb prefers 'zh-TW' for Traditional Chinese
		if (lang == LANGUAGE_CHINESE_TRADITIONAL)
		{
			build.Replace("<lang>", "zh-TW");
		}
		// ScWeb prefers 'pt-BR' for Brazilian Portuguese
		else if (lang == LANGUAGE_PORTUGUESE)
		{
			build.Replace("<lang>", "pt-BR");
		}
		else
		{
			build.Replace("<lang>", rlGetLanguageCode(lang));
		}

		rverifyall(resultBufSize > static_cast<size_t>(build.length()));

		safecpy(result, build.c_str(), resultBufSize);

		return true;
	}
	rcatchall
	{
	}

	return false;
}

#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

bool rlScAuth::BuildUrl()
{
	rtry
	{
		// Ensure we have a valid ticket
		const rlRosCredentials& creds = rlRos::GetCredentials(m_LocalGamerIndex);
		rverifyall(creds.IsValid());

		char hostname[RL_MAX_URL_BUF_LENGTH];
		rverifyall(rlScAuth::FormatHostName(hostname, RL_MAX_URL_BUF_LENGTH));

		// Format the url with the host name and append the /console/ path
		formatf(m_Url, "https://%s/console/", hostname);

		// Format the callback url with the host name and append the /console/complete/success path
		rverifyall(BuildCallbackUrl(m_CallbackUrl));

		// Client ID
		char cidBuf[RLROS_MAX_TITLE_NAME_SIZE + RLROS_MAX_PLATFORM_NAME_SIZE + 8] = { 0 };
		rverifyall(rlScAuth::FormatClientId(cidBuf, sizeof(cidBuf)));
		safecatf(m_Url, "?cid=%s", cidBuf);

		// Url Encode the Ticket or Link Token, and add it to the request
		char tokenBuf[4 * MAX(RLROS_MAX_TICKET_SIZE, RLSC_MAX_LINKTOKEN_CHARS)] = { 0 };

		if (rlScAuth::UseLinkTokens())
		{
			unsigned inLen = RLSC_MAX_LINKTOKEN_CHARS, dstLen = 0;
			rverifyall(netHttpRequest::UrlEncode(&tokenBuf[0], &inLen, m_LinkToken, (int)strlen(m_LinkToken), &dstLen, NULL));
			safecatf(m_Url, "&scAuthLinkToken=%s", tokenBuf);
		}
		else
		{
			unsigned inLen = RLROS_MAX_TICKET_SIZE, dstLen = 0;
			rverifyall(netHttpRequest::UrlEncode(&tokenBuf[0], &inLen, creds.GetTicket(), (int)strlen(creds.GetTicket()), &dstLen, NULL));
			safecatf(m_Url, "&playerTicket=%s", tokenBuf);
		}

#if RSG_ORBIS
		// Xbox One:
		//	 The machineID is retrieved from the XSTS token on the server. No need to send.
		// PS4:
		//	 No native machine ID, so we'll use the MAC address for now.
		// PC:
		//	ScAuth not supported, Social Club UI is used for signup
		char machineId[DAT_BASE64_MAX_ENCODED_SIZE(SCE_NET_ETHER_ADDR_LEN)] = { 0 };
		SceNetEtherAddr addr;
		if (rlVerify(sceNetGetMacAddress(&addr, 0) == SCE_OK))
		{
			unsigned unused;
			rlVerify(datBase64::Encode(addr.data, COUNTOF(addr.data), machineId, COUNTOF(machineId), &unused));
		}
#else
		const char* machineId = "";
#endif

		// Machine ID
		safecatf(m_Url, "&machineId=%s", machineId);
		
		// Optional - Language
		const char* szLanguage = GetLanguageStr();
		safecatf(m_Url, "&lang=%s", szLanguage);

		// Optional - Send Auth Mode
		if (m_AuthMode == SIGN_IN)
		{
			safecatf(m_Url, "&mode=%s", "signin");
		}
		else if (m_AuthMode == SIGN_UP)
		{
			safecatf(m_Url, "&mode=%s", "signup");
		}
		else if (m_AuthMode == LEGAL)
		{
			safecatf(m_Url, "&mode=%s", "legal");
		}
		else if (m_AuthMode == UNLINK)
		{
			safecatf(m_Url, "&mode=%s", "unlink");
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlScAuth::SetUseLinkTokens(bool bUseLinkTokens)
{
	if (s_UseLinkTokens != bUseLinkTokens)
	{
		rlDebug1("rlScAuth::SetUseLinkTokens - Enabled = %s", bUseLinkTokens ? "true" : "false");
		s_UseLinkTokens = bUseLinkTokens;
	}
}

bool rlScAuth::UseLinkTokens()
{
	return s_UseLinkTokens;
}

bool rlScAuth::FormatHostName(char* hostname, const size_t hostnameBufSize)
{
	rtry
	{
		rverifyall(hostname != nullptr && hostnameBufSize > 0);

		// Setup the base url
		// Dev/Cert/etc - {env}.scauth.rockstargames.com
		// Prod			- signin.rockstargames.com
		const char* stageEnv = nullptr;

#if RL_FORCE_STAGE_ENVIRONMENT
		if (rlIsForcedEnvironment())
		{
			formatf(hostname, hostnameBufSize, "stage-%s.scauth.rockstargames.com", g_rlTitleId->m_RosTitleId.GetEnvironmentName());
		}
		else
#endif
		if (PARAM_scAuthStagingEnv.Get(stageEnv))
		{
			formatf(hostname, hostnameBufSize, "%s.scauth.rockstargames.com", stageEnv);
		}
		else if (g_rlTitleId->m_RosTitleId.GetEnvironment() == RLROS_ENV_PROD)
		{
			safecpy(hostname, "signin.rockstargames.com", hostnameBufSize);
		}
		else
		{
			formatf(hostname, hostnameBufSize, "%s.scauth.rockstargames.com", g_rlTitleId->m_RosTitleId.GetEnvironmentName());
		}

		// Allow for an rlRos service host override.
		char hostnameBuf[256] = { 0 };
		const char* hostOverride = rlRos::GetServiceHost(hostname, hostnameBuf);
		if (hostOverride)
		{
			safecpy(hostname, hostOverride, hostnameBufSize);
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlScAuth::FormatClientId(char* cid, const size_t cidBufSize)
{
	rtry
	{
		rverifyall(cid != nullptr && cidBufSize > 0);
	
		// If a title ID override is not specified, we cannot continue
		rverifyall(!StringNullOrEmpty(sm_TitleId));

		// Use the titleID passed to ScAuth
		formatf(cid, cidBufSize, "%s-%s", RSG_PLATFORM_ID, sm_TitleId);

		// On XB1, a separate content id for Japanese build is required and appended to the client ID.
		if (RSG_DURANGO && sysAppContent::IsJapaneseBuild())
		{
			safecat(cid, "jp", cidBufSize);
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlScAuth::BuildCallbackUrl(char(&callbackUrl)[RL_MAX_URL_BUF_LENGTH])
{
	rtry
	{
		char hostname[RL_MAX_URL_BUF_LENGTH];
		rverifyall(rlScAuth::FormatHostName(hostname, RL_MAX_URL_BUF_LENGTH));
		formatf(callbackUrl, "https://%s/console/complete/success", hostname);
		return true;
	}
	rcatchall
	{
		callbackUrl[0] = '\0';
		return false;
	}
}

const char* rlScAuth::GetLanguageStr(sysLanguage language)
{
	if (language == LANGUAGE_UNDEFINED)
	{
		language = rlGetLanguage();
	} 

	// ScWeb prefers 'zh-TW' for Traditional Chinese
	if (language == LANGUAGE_CHINESE_TRADITIONAL)
	{
		return "zh-TW";
	}
	// ScWeb prefers 'pt-BR' for Brazilian Portuguese
	else if (language == LANGUAGE_PORTUGUESE)
	{
		return "pt-BR";
	}
	else
	{
		return rlGetLanguageCode(language);
	}
}

bool rlScAuth::GetLinkToken()
{
	return rlSocialClub::CreateLinkToken(m_LocalGamerIndex, m_LinkToken, COUNTOF(m_LinkToken), &m_MyStatus);
}

bool rlScAuth::ShowBrowser()
{
	rtry
	{
		rverifyall(BuildUrl());

		rlDebug1("Loading ScAuth url in browser: %s", m_Url);
		rlDebug1("Callback url: %s", m_CallbackUrl);

		rlSystemBrowserConfig config;
		config.m_Url = m_Url;
		config.m_CallbackUrl = m_CallbackUrl;

		// Opt into full screen mode for sc auth
		if (m_bUseFullScreen)
		{
			rlDebug1("Fullscreen mode.");
			config.SetFullScreen();
			config.SetUseSafeRegion(m_bUseSafeRegion);
			config.SetHeaderEnabled(m_bUseHeader);
			config.SetFooterEnabled(m_bUseFooter);
		}

		if (sm_bDisableExit)
		{
			config.DisableExit();
		}

		rverifyall(g_SystemUi.ShowWebBrowser(m_LocalGamerIndex, config));
		m_WebBrowserShowTime = sysTimer::GetSystemMsTime() | 0x01; // Set initial time
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlScAuth::BrowserTimedOut()
{
	const int TIMEOUT_TIME = (4 * 1000);

	// The web browser was shown -- we're waiting for a result, and it has been closed for a TIMEOUT_TIME interval.
	return m_State == STATE_SHOWING_WEB_BROWSER && m_WebBrowserShowTime && sysTimer::HasElapsedIntervalMs(m_WebBrowserShowTime, TIMEOUT_TIME);
}

bool rlScAuth::CredentialsTimedOut()
{
	const int TIMEOUT_TIME = (30 * 1000);

	// We were waiting for credentials - but no result came after TIMEOUT_TIME milliseconds.
	return m_State == STATE_RENEWING_CREDENTIALS && m_CredentialsRefreshTime && sysTimer::HasElapsedIntervalMs(m_CredentialsRefreshTime, TIMEOUT_TIME);
}

void rlScAuth::OnComplete()
{
	if (m_Status)
	{
		m_Status->SetSucceeded();
	}

	m_State = STATE_SUCCESS;
}

void rlScAuth::OnError()
{
	if (m_Status)
	{
		m_Status->SetFailed();
	}

	m_State = STATE_ERROR;
}

void rlScAuth::OnPresenceEvent(const rlPresenceEvent* evt)
{
	if (PRESENCE_EVENT_SC_MESSAGE == evt->GetId())
	{
		const rlScPresenceMessage& msgBuf = evt->m_ScMessage->m_Message;
		if (msgBuf.IsA<rlScPresenceAccountLinkResult>())
		{
			if (m_State != STATE_SHOWING_WEB_BROWSER)
			{
				rlDebug3("Ignoring link result, not on the browser wait state. Credentials may have arrived or the operation was aborted.");
				return;
			}

			rlScPresenceAccountLinkResult msg;
			if (rlVerifyf(msg.Import(msgBuf), "rlScAuth::OnPresenceEvent - Error importing '%s'", msg.Name()))
			{
				// Ensure the presence message was for Sc Auth - ignore other networks
				if (!stricmp(msg.m_Network, "sc"))
				{
					if (msg.m_Success)
					{
						rlDebug1("Link succeeded, route: %s", msg.m_Route);
						m_bLinkSuccessful = true;
					}
					else
					{
						rlError("Link failed, error: %s", msg.m_ErrorId);
						OnError();
					}
				}
			}
		}
		else if (msgBuf.IsA<rlScPresenceAccountUnlinkResult>())
		{
			if (m_State != STATE_SHOWING_WEB_BROWSER)
			{
				rlDebug3("Ignoring unlink result, not on the browser wait state. Credentials may have arrived or the operation was aborted.");
				return;
			}

			rlScPresenceAccountUnlinkResult msg;
			if (rlVerifyf(msg.Import(msgBuf), "rlScAuth::OnPresenceEvent - Error importing '%s'", msg.Name()))
			{
				// Ensure the presence message was for Sc Auth - ignore other networks
				if (!stricmp(msg.m_Network, "sc"))
				{
					if (msg.m_Success)
					{
						rlDebug1("Unlink succeeded, route: %s", msg.m_Route);
						m_State = STATE_RENEW_CREDENTIALS;
					}
					else
					{
						rlError("Unlink failed, error: %s", msg.m_ErrorId);
						OnError();
					}
				}
			}
		}
	}
}

void rlScAuth::OnRosEvent(const rlRosEvent& evt)
{
	if (RLROS_EVENT_GET_CREDENTIALS_RESULT == evt.GetId())
	{
		const rlRosEventGetCredentialsResult& credEvent = static_cast<const rlRosEventGetCredentialsResult&>(evt);
		if (credEvent.m_LocalGamerIndex == m_LocalGamerIndex)
		{
			if (credEvent.m_Success)
			{
				rlDebug1("New credentials were successful.");
				m_bCredentialsRefreshed = true;
			}
			else
			{
				rlError("Credentials refresh failed, auth was a fail.");
				OnError();
			}
		}
	}
}

} // namespace rage
