// 
// rline/rlScAuth.h 
// 
// Copyright (C) 2017 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_SC_AUTH_H
#define RLINE_SC_AUTH_H

#include "rline/rl.h"
#include "rline/rldiag.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"

namespace rage
{

class rlScAuth
{
public:

	rlScAuth();
	~rlScAuth();

	enum AuthMode
	{
		SIGN_IN,
		SIGN_UP,
		LEGAL,
		UNLINK
	};

	// PURPOSE
	//	Default Link Token behaviour
	static const bool DEFAULT_LINK_TOKENS_ENABLED = true;

	// PURPOSE
	//	Global tunable for ScAuth
	static void SetEnabled(const bool bEnabled) { sm_bEnabled = bEnabled; }
	static bool IsEnabled() { return sm_bEnabled; }

	// Global tunable for disabling exit in ScAuth
	static void DisableExit(bool bDisableExit) { sm_bDisableExit = bDisableExit; }
	static bool IsExitDisabled() { return sm_bDisableExit; }

	// PURPOSE
	//	Set the TitleId used by ScAuth's client id
	static void SetTitleId(const char* titleId);

	// PURPOSE
	//	Enable/Disable the use of link tokens.
	static void SetUseLinkTokens(bool bUseLinkTokens);
	static bool UseLinkTokens();

	// PURPOSE
	//	Initialize the auth request with a local gamer index and authorization type
	// PARAMS
	//	localGamerIndex - must be valid gamer index
	//	mode - the path to prefer for authorization (signup or signin)
	//  status - (OPTIONAL) netStatus to attach to this request.
	bool Init(const int localGamerIndex, AuthMode mode, netStatus* status = nullptr);

	// PURPOSE
	//	Returns 'true' if the ScAuth system is available (i.e. valid ROS credentials)
	bool IsAvailable(const int localGamerIndex) const;

	// PURPOSE
	//	Update the request - should be called once per frame while active.
	void Update();

	// PURPOSE
	//	Terminates the request -- will be called automatically if the request loses scope.
	void Shutdown();

	// PURPOSE
	//	State information
	bool IsInitialized() const;
	bool IsComplete() const;
	bool HadError() const;
	bool IsShowingBrowser() const;
	bool IsRenewingCredentials() const;

	// PURPOSE
	//	Browser layout details
	void SetUseFullscreen(bool bFullScreenAuth) { m_bUseFullScreen = bFullScreenAuth; }
	void SetUseSafeRegion(bool bSafeRegion) { m_bUseSafeRegion = bSafeRegion; }
	void SetHeaderEnabled(bool bHeader) { m_bUseHeader = bHeader; }
	void SetFooterEnabled(bool bFooter) { m_bUseFooter = bFooter; }

	// PURPOSE
	//	Builds a url to be opened in a browser
	// PARAMS
	//	result          - The generated url
	//	resultBufSize   - The size in bytes of the string buffer
	//	localGamerIndex - Must be a valid gamer index
	//	uriTemplate     - The uri template. Will be something like "https://<env>scauth.rockstargames.com/go/<feature>/<action>/<identifier>?cid=<clientId>&playerTicket=<playerTicket>&lang=<lang>"
	//	feature         - The feature area. Example: "feed"
	//	action          - The action. Example: "report", "view" ...
	//	identifier      - The identifier of the item. This can be a RockstarId, a feed guid ...
	static bool FormatWeblinkUri(char* result, const size_t resultBufSize, const int localGamerIndex, const char* feature, const char* action, const char* identifier);

	// PURPOSE
	//	Builds the ClientId component of ScAuth urls
	// PARAMS
	//	cid              - The string containing the result
	//	cidBufSize       - The size in bytes of the buffer
	static bool FormatClientId(char* cid, const size_t cidBufSize);

	// PURPOSE
	//	Builds a callback url
	static bool BuildCallbackUrl(char(&callbackUrl)[RL_MAX_URL_BUF_LENGTH]);

	// PURPOSE
	//	Returns the ScAuth string representation of the game language.
	static const char* GetLanguageStr(sysLanguage language = LANGUAGE_UNDEFINED);

private:

	// PURPOSE
	//	Based on the current environment, setup the url for SC signin/signup.
	bool BuildUrl();

	// Begins the link token download task.
	bool GetLinkToken();

	// PURPOSE
	//	Builds the hostname part of the URL
	// PARAMS
	//	hostname          - The string containing the result
	//	hostnameBufSize   - The size in bytes of the buffer
	static bool FormatHostName(char* hostname, const size_t hostnameBufSize);

	// PURPOSE
	//	Triggers the system web browser UI
	bool ShowBrowser();

	// PURPOSE
	//	Returns true when the web browser operation times out
	bool BrowserTimedOut();
	
	// PURPOSE
	//	Returns true when the credential refresh times out
	bool CredentialsTimedOut();

	// PURPOSE
	//	Internal completion events
	void OnComplete();
	void OnError();

	// PURPOSE
	//	Handle presence events, looking for link notifications.
	void OnPresenceEvent(const rlPresenceEvent* evt);

	// PURPOSE
	//	Handle ROS events, looking for ticket refresh notifications after a link.
	void OnRosEvent(const rlRosEvent& evt);

	enum State
	{
		STATE_NONE,
		STATE_GET_LINK_TOKEN,
		STATE_GETTING_LINK_TOKEN,
		STATE_SHOW_WEB_BROWSER,
		STATE_SHOWING_WEB_BROWSER,
		STATE_RENEW_CREDENTIALS,
		STATE_RENEWING_CREDENTIALS,
		STATE_ERROR,
		STATE_SUCCESS
	};

	rlPresence::Delegate m_PresenceDlgt;
	rlRos::Delegate m_RosDlgt;
	netStatus* m_Status;

	State m_State;
	AuthMode m_AuthMode;
	netStatus m_MyStatus;

	char m_Url[RL_MAX_URL_BUF_LENGTH];
	char m_CallbackUrl[RL_MAX_URL_BUF_LENGTH];
	char m_LinkToken[RLSC_MAX_LINKTOKEN_CHARS];
	bool m_bInitialized;
	int m_LocalGamerIndex;
	u32 m_WebBrowserShowTime;
	u32 m_CredentialsRefreshTime;
	bool m_bUseFullScreen;
	bool m_bUseSafeRegion;
	bool m_bUseHeader;
	bool m_bUseFooter;
	bool m_bLinkSuccessful;
	bool m_bCredentialsRefreshed;
	
	static char sm_TitleId[RLROS_MAX_TITLE_NAME_SIZE];
	static bool sm_bEnabled;
	static bool sm_bDisableExit;
	static bool s_UseLinkTokens;
};


} //namespace rage

#endif // RLINE_SC_AUTH_H
