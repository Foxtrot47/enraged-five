// 
// rline/rlxhttp.h
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 
 
#ifndef RLINE_RLXHTTP_H
#define RLINE_RLXHTTP_H

#if __LIVE

namespace rage
{

class netSocketAddress;
class netStatus;
class rlXhttp;

struct rlXhttpRequestData
{
    rlXhttpRequestData();

    void Clear();

    void ClearReadData();

    enum WriteState
    {
        WS_NONE,
        WS_ERROR,
        WS_CONNECTED,
        WS_SENDING_REQUEST,
        WS_SENT_REQUEST,
        WS_WRITE_DATA,
        WS_WRITING_DATA,
    };

    enum ReadState
    {
        RS_NONE,
        RS_ERROR,
        RS_WAITING_FOR_HEADERS,
        RS_READ_DATA,
        RS_READING_DATA,
    };

    const u8* m_WriteBuf;
    unsigned m_WriteBufLen;
    unsigned m_NumBytesWritten;

    u8* m_ReadBuf;
    unsigned m_ReadBufLen;
    unsigned* m_NumBytesRead;

    WriteState m_WriteState;
    ReadState m_ReadState;

    unsigned m_RequestId;

    netStatus* m_WriteStatus;
    netStatus* m_ReadStatus;
};

enum rlXhttpConnectFlags
{
    RL_XHTTP_CONNECT_SSL    = 0x01
};

// mirrors the XDK's RELYING_PARTY_TOKEN struct to avoid including xauth.h header
struct rlRelyingPartyToken
{
	unsigned long Reserved;
	unsigned long Length;
	unsigned char* pToken;
};

class rlXhttpRequest
{
    friend class rlXhttp;

public:

    rlXhttpRequest();

    ~rlXhttpRequest();

    bool Connect(const char* verb,
                const char* hostname,
                const char* path,
                const char* userAgent,
                const netSocketAddress* proxyAddr,
                const unsigned flags);

    bool SendRequest(const char* headers,
                    const unsigned length,
                    const char* content,
                    const unsigned contentLength,
                    const unsigned totalLength,
                    netStatus* status);

    bool WriteData(const u8* data, const unsigned dataLen, netStatus* status);

    bool ReadData(u8* data, const unsigned maxLen, unsigned* numRead, netStatus* status);

    bool HaveHeaders() const;

    unsigned GetHttpStatusCode() const;

    bool GetResponseHeaderLength(unsigned* headerLen) const;

    bool GetResponseHeader(char* headerBuf,
                            unsigned* headerLen) const;

    bool GetResponseHeaderValue(const char* fieldName,
                                char* value,
                                unsigned* valLen) const;
    void FinishSuccess();

    void FinishFail();

    void Cancel();

    bool Pending() const;

    void Update();

private:

    void CloseHandles();

    void Close();

    void Link(rlXhttpRequest* next);
    void Unlink();

    rlXhttpRequest* m_Next;
    rlXhttpRequest* m_Prev;
    netStatus* m_SendStatus;

    rlXhttpRequestData m_Data;

    void* m_hSession;
    void* m_hConnection;
    void* m_hRequest;
};

class rlXhttp
{
public:

    static bool Init();

    static void Shutdown();

    static void Update();

    static bool IsOnline();

	static bool GetAuthToken(const int localGamerIndex,
							 const char* uri,
							 rlRelyingPartyToken** authCode,
							 netStatus* status);

	static void FreeAuthToken(rlRelyingPartyToken* token);
};

}   //namespace rage

#endif  //__LIVE

#endif  //RLINE_RLXHTTP_H
