// 
// rline/rlsocialclubtask.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsocialclub.h"
#include "rlsocialclubtask.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/device.h"
#include "parser/manager.h"
#include "system/param.h"
#include "rline/rlgamerinfo.h"
#include "rline/rltitleid.h"
#include "rline/ros/rlros.h"
#include "system/nelem.h"
#include "rline/ugc/rlugccommon.h"
#include "rline/rlprofanitycheck.h"

#if __RGSC_DLL
#include "../../../../suite/src/rgsc/rgsc/diag/output.h"
#endif

namespace rage
{

extern const rlTitleId* g_rlTitleId;

#define ERROR_CASE(code, codeEx, resCode) \
    if(result.IsError(code, codeEx)) \
    { \
        resultCode = resCode; \
        return; \
    }

#define WARNING_CASE(code, codeEx) \
    if(result.IsError(code, codeEx)) \
    { \
        resultCode = 0; \
        return; \
    }

///////////////////////////////////////////////////////////////////////////////
//  rlScTaskBase
///////////////////////////////////////////////////////////////////////////////
void
rlScTaskBase::HandleError(const rlRosResult& result, int& resultCode)
{
    ERROR_CASE("AlreadyExists", "Email", RLSC_ERROR_ALREADYEXISTS_EMAIL);
    ERROR_CASE("AlreadyExists", "EmailOrNickname", RLSC_ERROR_ALREADYEXISTS_EMAILORNICKNAME);
    WARNING_CASE("AlreadyExists", "AcceptedVersion");
    ERROR_CASE("AlreadyExists", "Nickname", RLSC_ERROR_ALREADYEXISTS_NICKNAME);
    ERROR_CASE("AuthenticationFailed", "Password", RLSC_ERROR_AUTHENTICATIONFAILED_PASSWORD);
	ERROR_CASE("AuthenticationFailed", "InvalidCredentials", RLSC_ERROR_AUTHENTICATIONFAILED_PASSWORD);
    ERROR_CASE("AuthenticationFailed", "Ticket", RLSC_ERROR_AUTHENTICATIONFAILED_TICKET);
	ERROR_CASE("AuthenticationFailed", "LoginAttempts", RLSC_ERROR_AUTHENTICATIONFAILED_LOGINATTEMPTS);
	ERROR_CASE("AuthenticationFailed", "MFA.NotAuthenticated", RLSC_ERROR_AUTHENTICATIONFAILED_MFA);
    ERROR_CASE("DoesNotExist", "PlayerAccount", RLSC_ERROR_DOESNOTEXIST_PLAYERACCOUNT);
    ERROR_CASE("DoesNotExist", "RockstarAccount", RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT);
    ERROR_CASE("InvalidArgument", "countryCode", RLSC_ERROR_INVALIDARGUMENT_COUNTRYCODE);
    ERROR_CASE("InvalidArgument", "dob", RLSC_ERROR_INVALIDARGUMENT_DOB);
    ERROR_CASE("InvalidArgument", "email", RLSC_ERROR_INVALIDARGUMENT_EMAIL);
    ERROR_CASE("InvalidArgument", "Nickname", RLSC_ERROR_INVALIDARGUMENT_NICKNAME);
    ERROR_CASE("InvalidArgument", "password", RLSC_ERROR_INVALIDARGUMENT_PASSWORD);
    ERROR_CASE("InvalidArgument", "zipCode", RLSC_ERROR_INVALIDARGUMENT_ZIPCODE);
	ERROR_CASE("InvalidArgument", "RockstarId", RLSC_ERROR_INVALIDARGUMENT_ROCKSTARID);
    ERROR_CASE("NotAllowed", "MaxEmailsReached", RLSC_ERROR_NOTALLOWED_MAXEMAILSREACHED);
	ERROR_CASE("NotAllowed", "Privacy", RLSC_ERROR_NOTALLOWED_PRIVACY);
    ERROR_CASE("OutOfRange", "Age", RLSC_ERROR_OUTOFRANGE_AGE);
    ERROR_CASE("OutOfRange", "AgeLocale", RLSC_ERROR_OUTOFRANGE_AGELOCALE);
    ERROR_CASE("OutOfRange", "NicknameLength", RLSC_ERROR_OUTOFRANGE_NICKNAMELENGTH);
    ERROR_CASE("OutOfRange", "PasswordLength", RLSC_ERROR_OUTOFRANGE_PASSWORDLENGTH);
    ERROR_CASE("Profane", "Nickname", RLSC_ERROR_PROFANE_NICKNAME);
    ERROR_CASE("Redundant", NULL, RLSC_ERROR_DOESNOTEXIST_PLAYERACCOUNT);
    ERROR_CASE("SystemError", "SendEmail", RLSC_ERROR_SYSTEMERROR_SENDMAIL);
	ERROR_CASE("Profane", "Text", RLSC_ERROR_PROFANE_TEXT);

    resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

void
rlScTaskBase::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    HandleError(result, resultCode);

    if (resultCode == RLSC_ERROR_UNEXPECTED_RESULT)
    {
        rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScCreateAccountTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScCreateAccountTask::Configure(const int localGamerIndex,
                                         const bool acceptNewsletter,
                                         const char* avatarUrl,
                                         const char* countryCode,
                                         const char* dob,
                                         const char* email,
                                         const bool isApproxDob,
                                         const char* languageCode,
                                         const char* nickname,		
                                         const char* password,
                                         const char* phone,
                                         const char* zipCode)
{
    if(!rlRosCredentialsChangingTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

#if !RLROS_SC_PLATFORM
    //Non-SC platforms require a ticket with the request.
    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }
    else if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add ticket param");
        return false;
    }
#endif

    //Add required params.
    if(!AddStringParameter("acceptNewsletter", acceptNewsletter ? "true" : "false", true)
        || !AddStringParameter("avatarUrl", avatarUrl, true)
        || !AddStringParameter("countryCode", countryCode, true)
        || !AddStringParameter("dob", dob, true)
        || !AddStringParameter("email", email, true)
        || !AddStringParameter("isApproxDob", isApproxDob ? "true" : "false", true)
        || !AddStringParameter("languageCode", languageCode, true)
        || !AddStringParameter("nickname", nickname, true)
        || !AddStringParameter("password", password, true)
        || !AddStringParameter("phone", phone, true)
        || !AddStringParameter("zipCode", zipCode, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

#if RLROS_SC_PLATFORM
    safecpy(m_Email, email, sizeof(m_Email));
    safecpy(m_Password, password, sizeof(m_Password));
#endif

    return true;
}

#if RLROS_SC_PLATFORM
bool 
rlScCreateAccountTask::ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
    //Set our login so we'll be able to refresh the ticket we set.
    return rlRosCredentialsChangingTask::ProcessSuccess(result, node, resultCode);
}
#endif

void
rlScCreateAccountTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    rlScTaskBase::HandleError(result, resultCode);


    if (resultCode == RLSC_ERROR_UNEXPECTED_RESULT)
    {
        rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    }
}

#if !RLROS_SC_PLATFORM

///////////////////////////////////////////////////////////////////////////////
//  rlScLinkAccountTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScLinkAccountTask::Configure(const int localGamerIndex,
                               const char* email,
                               const char* nickname,
                               const char* password)
{
    if(!rlRosCredentialsChangingTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("email", email, true)
        || !AddStringParameter("nickname", nickname, true)
        || !AddStringParameter("password", password, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

void
rlScLinkAccountTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    rlScTaskBase::HandleError(result, resultCode);

    if (resultCode == RLSC_ERROR_UNEXPECTED_RESULT)
    {
        rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScUnlinkAccountTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScUnlinkAccountTask::Configure(const int localGamerIndex)
{
    if(!rlRosCredentialsChangingTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

void
rlScUnlinkAccountTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    rlScTaskBase::HandleError(result, resultCode);

    if (resultCode == RLSC_ERROR_UNEXPECTED_RESULT)
    {
        rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
    }
}

#endif //!RLROS_SC_PLATFORM

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckEmailTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScCheckEmailTask::Configure(const int localGamerIndex,
                              const char* email)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    if(!AddStringParameter("email", email, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckNicknameTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScCheckNicknameTask::Configure(const int localGamerIndex,
                                 const char* nickname)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("nickname", nickname, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckPasswordTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScCheckPasswordTask::Configure(const int localGamerIndex,
                                 const char* password,
                                 rlScPasswordReqs* passwordReqs)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("password", password, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_PasswordReqs = passwordReqs;

    return true;
}

void
rlScCheckPasswordTask::ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
    rlScTaskBase::ProcessError(result, node, resultCode);

    // If we have a response, try to parse the password reqs out of it into our m_PasswordReqs if set
    if (m_PasswordReqs != NULL)
    {
        rtry
        {
            rcheck(node != NULL, catchall, rlTaskError("No response node"));

            const parTreeNode* result = node->FindChildWithName("Result");
            rcheck(result != NULL, catchall, rlTaskError("Failed to find <Result> element"));
            
            rcheck(rlHttpTaskHelper::ReadUns(m_PasswordReqs->MinLength, result, "MinLength", NULL), catchall, rlTaskError("Failed to find <MinLength> element"));
            rcheck(rlHttpTaskHelper::ReadUns(m_PasswordReqs->MaxLength, result, "MaxLength", NULL), catchall, rlTaskError("Failed to find <MaxLength> element"));
            const char *validSymbols = rlHttpTaskHelper::ReadString(result, "ValidSymbols", NULL);
            rcheck(validSymbols != NULL, catchall, rlTaskError("Failed to find <ValidSymbols> element"));
            
            // Use less than, since we need to leave room for null term
            rverify(StringLength(validSymbols) < COUNTOF(m_PasswordReqs->ValidSymbols), catchall, rlTaskError("Too many valid password symbols"));
            safecpy(m_PasswordReqs->ValidSymbols, validSymbols);
            m_PasswordReqs->bValid = true;
        }
        rcatchall
        {
            m_PasswordReqs->bValid = false;
        }
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlScGetPasswordRequirementsTask
///////////////////////////////////////////////////////////////////////////////
bool rlScGetPasswordRequirementsTask::Configure(const int localGamerIndex, rlScPasswordReqs* passwordReqs)
{
	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}

	if(!AddStringParameter("ticket", cred.GetTicket(), true))
	{
		rlTaskError("Failed to add params");
		return false;
	}


	m_PasswordReqs = passwordReqs;

	return true;
}

bool rlScGetPasswordRequirementsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
	// If we have a response, try to parse the password reqs out of it into our m_PasswordReqs if set
	rtry
	{
		rverify(m_PasswordReqs, catchall, );
		rcheck(node != NULL, catchall, rlTaskError("No response node"));

		const parTreeNode* result = node->FindChildWithName("Result");
		rcheck(result != NULL, catchall, rlTaskError("Failed to find <Result> element"));

		rcheck(rlHttpTaskHelper::ReadUns(m_PasswordReqs->MinLength, result, "MinLength", NULL), catchall, rlTaskError("Failed to find <MinLength> element"));
		rcheck(rlHttpTaskHelper::ReadUns(m_PasswordReqs->MaxLength, result, "MaxLength", NULL), catchall, rlTaskError("Failed to find <MaxLength> element"));
		const char *validSymbols = rlHttpTaskHelper::ReadString(result, "ValidSymbols", NULL);
		rcheck(validSymbols != NULL, catchall, rlTaskError("Failed to find <ValidSymbols> element"));

		// Use less than, since we need to leave room for null term
		rverify(StringLength(validSymbols) < COUNTOF(m_PasswordReqs->ValidSymbols), catchall, rlTaskError("Too many valid password symbols"));
		safecpy(m_PasswordReqs->ValidSymbols, validSymbols);
		m_PasswordReqs->bValid = true;

		return true;
	}
	rcatchall
	{
		m_PasswordReqs->bValid = false;
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckTextTask
///////////////////////////////////////////////////////////////////////////////
bool 
rlScCheckTextTask::Configure(const int localGamerIndex, 
                             const char* text, 
							 const rlScLanguage language,
							 char* profaneWord)
{
	if(strlen(text)==0)
	{
		rlTaskError("Cannot check for empty string");
		return false;
	}
	
	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}

	if(!AddStringParameter("ticket", cred.GetTicket(), true)
       || !AddStringParameter("text", text, true)
       || !AddStringParameter("language", rlScLanguageToString(language), true))
    {
		rlTaskError("Failed to add params");
		return false;
	}

	if(profaneWord)
	{
		m_profaneWord=profaneWord;
	}

	return true;
}

void rlScCheckTextTask::ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
	if(result.IsError("Profane", "Text"))
	{
		resultCode = RLUGC_ERROR_NOTALLOWED_PROFANE;
		if(m_profaneWord)
		{
			if(!rlProfanityCheck::ExtractProfaneWord(node, m_profaneWord, MAX_PROFANE_WORD_LENGTH))
			{
				// Removed because certain UGC flows do not return the result (i.e. if using the word "exploit", it is
				// returned as an illegal UGC word rather than a profane word).

				// rlAssertf(0, "Could not extract profane word");
				rlTaskError("Could not extract profane word");
			}
		}
	}
	return rlScTaskBase::ProcessError(result, node, resultCode);
}


///////////////////////////////////////////////////////////////////////////////
//  rlScGetCountriesTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScGetCountriesTask::Configure(const int localGamerIndex,
                                rlScCountries* countries)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    m_Countries = countries;

    return true;
}

bool 
rlScGetCountriesTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    rtry
    {
        for(unsigned i = 0; i < (unsigned)node->FindNumChildren(); i++)
        {
            parTreeNode* pNode = node->FindChildWithIndex(i);
            rverify(pNode, catchall, );

            if(stricmp(pNode->GetElement().GetName(), "r") != 0)
            {
                continue;
            }

            const char* code = rlHttpTaskHelper::ReadString(pNode, NULL, "c");
            rverify(code, catchall, );

            const char* name = rlHttpTaskHelper::ReadString(pNode, NULL, "n");
            rverify(name, catchall, );

            rlScCountries::Country *country = RL_NEW(SocialClubGetCountriesTask, rlScCountries::Country);
            rverify(country, catchall, );

            bool valid = country->Reset(code, name);

            if(valid)
            {
                m_Countries->m_List.push_back(country);
            }
            else
            {
                RL_DELETE(country);
            }
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScGetAccountInfoTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScGetAccountInfoTask::Configure(const int localGamerIndex,
                                  rlScAccountInfo* acct)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_Acct = acct;

    return true;
}

bool 
rlScGetAccountInfoTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    rtry
    {
        m_Acct->Clear();

        //Get the 'RockstarAccount' node
        node = node->FindChildWithNameIgnoreNs("RockstarAccount");
        rverify(node, catchall, );

        //Get all the fields.  Only email and RockstarId are required.
        RockstarId rockstarId = InvalidRockstarId;
        rverify(rlHttpTaskHelper::ReadInt64(rockstarId, node, "RockstarId", NULL), catchall, );

        const char* email = rlHttpTaskHelper::ReadString(node, "Email", NULL);
        rverify(email, catchall, );

        const char* avatarUrl = rlHttpTaskHelper::ReadString(node, "AvatarUrl", NULL);
        const char* countryCode = rlHttpTaskHelper::ReadString(node, "CountryCode", NULL);
        const char* dob = rlHttpTaskHelper::ReadString(node, "Dob", NULL);
        const char* languageCode = rlHttpTaskHelper::ReadString(node, "AvatarUrl", NULL);
        const char* nickname = rlHttpTaskHelper::ReadString(node, "AvatarUrl", NULL);
        const char* phone = rlHttpTaskHelper::ReadString(node, "Phone", NULL);
        const char* zipCode = rlHttpTaskHelper::ReadString(node, "ZipCode", NULL);

        int age = 0;
        rlHttpTaskHelper::ReadInt(age, node, "Age", NULL);

        bool isApproxDob = false;
        rlHttpTaskHelper::ReadBool(isApproxDob, node, "IsApproxDob", NULL);

        //Copy them to the account.       
        m_Acct->m_Age = age;
        m_Acct->m_IsApproxDob = isApproxDob;
        m_Acct->m_RockstarId = rockstarId;
        if(avatarUrl) m_Acct->SetAvatarUrl(avatarUrl);
        if(countryCode) safecpy(m_Acct->m_CountryCode, countryCode, sizeof(m_Acct->m_CountryCode));
        if(dob) safecpy(m_Acct->m_Dob, dob, sizeof(m_Acct->m_Dob));
        if(email) safecpy(m_Acct->m_Email, email, sizeof(m_Acct->m_Email));
        if(languageCode) safecpy(m_Acct->m_LanguageCode, languageCode, sizeof(m_Acct->m_LanguageCode));
        if(nickname) safecpy(m_Acct->m_Nickname, nickname, sizeof(m_Acct->m_Nickname));
        if(phone) safecpy(m_Acct->m_Phone, phone, sizeof(m_Acct->m_Phone));
        if(zipCode) safecpy(m_Acct->m_ZipCode, zipCode, sizeof(m_Acct->m_ZipCode));

        return true;
    }
    rcatchall
    {
        m_Acct->Clear();
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScGetAlternateNicknamesTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScGetAlternateNicknamesTask::Configure(const int localGamerIndex,
                                         const char* nickname,
                                         char (*alternates)[RLSC_MAX_NICKNAME_CHARS],
                                         const unsigned maxAlternates,
                                         unsigned* numAlternates)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("nickname", nickname, true)
       || !AddIntParameter("maxAlternates", maxAlternates))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_Alternates = alternates;
    m_MaxAlternates = maxAlternates;
    m_NumAlternates = numAlternates;

    return true;
}

bool 
rlScGetAlternateNicknamesTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    rtry
    {
        const parTreeNode* resultNode = node->FindChildWithNameIgnoreNs("Result");
        if (resultNode != NULL)
        {
            int index = 0;
            parTreeNode::ChildNodeIterator iter = resultNode->BeginChildren();
            for( ; iter != resultNode->EndChildren(); ++iter)
            {
                if (index >= (int)m_MaxAlternates) break;

                parTreeNode* curNode = *iter;

                if (!strcmp(curNode->GetElement().GetName(), "alt"))
                {
                    char* dest = m_Alternates[index];

                    const char* alt = rlHttpTaskHelper::ReadString(curNode, NULL, NULL);
                    rverify((alt != NULL) && (strlen(alt) < RLSC_MAX_NICKNAME_CHARS), catchall, );
                    safecpy(dest, alt, RLSC_MAX_NICKNAME_CHARS);

                    ++index;
                }
            }

            if (m_NumAlternates) *m_NumAlternates = index;
        }
        else
        {
            if (m_NumAlternates) *m_NumAlternates = 0;
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlScGetScAuthTokenTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScGetScAuthTokenTask::Configure(const int localGamerIndex,
                                  char* buf,
                                  const unsigned bufSize)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_Buf = buf;
    m_BufSize = bufSize;

    return true;
}

bool 
rlScGetScAuthTokenTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                       const parTreeNode* node, 
                                       int& resultCode)
{
    rtry
    {
        const parTreeNode* result = node->FindChildWithName("Result");
        rverify(result, catchall, rlTaskError("Missing <Result>"));

		const char* scAuthToken = rlHttpTaskHelper::ReadString(result, NULL, NULL);
        rverify(scAuthToken, catchall, rlTaskError("Missing <Result> text"));

        rlTaskDebug3("SC auth token: %s", scAuthToken);

        safecpy(m_Buf, scAuthToken, m_BufSize);

		resultCode = RLSC_ERROR_NONE;

        return true;
    }
    rcatchall
    {
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
		return false;
    }
}

void rlScGetScAuthTokenTask::Finish(const FinishType finishType, const int resultCode)
{
#if __RGSC_DLL
	if (finishType == rlTaskBase::FINISH_FAILED)
	{
		netHttpRequest::DebugInfo debugInfo;
		m_HttpRequest.GetDebugInfo(debugInfo);

		RgscDisplay("Social Club Auth Token Error: commit:%d, http:%d, alloc:%d, recv:%d, send:%d, tcpe:%d, tcpr:%d)", 
			debugInfo.m_Committed, 
			debugInfo.m_HttpStatusCode, 
			debugInfo.m_ChunkAllocationError, 
			debugInfo.m_RecvState, 
			debugInfo.m_SendState, 
			debugInfo.m_TcpError, 
			debugInfo.m_TcpResult);
	}
#endif

	rlHttpTask::Finish(finishType, resultCode);
}

///////////////////////////////////////////////////////////////////////////////
//  rlScCreateScAuthTokenTask
///////////////////////////////////////////////////////////////////////////////
rlScCreateScAuthTokenTask::rlScCreateScAuthTokenTask()
	: m_AuthTokenBuf(NULL)
	, m_AuthTokenBufSize(0)
{

}

bool rlScCreateScAuthTokenTask::Configure(const int localGamerIndex, const char* scTicket, 
										   char* scAuthTokenBuf, const unsigned scAuthTokenBufSize, 
										   const char* audience, const int desiredTimeToLive, const char* userData)
{
	rtry
	{
		rverify(scAuthTokenBuf, catchall, );
		rverify(scAuthTokenBufSize >= RLSC_MAX_SCAUTHTOKEN_CHARS, catchall, );
		rverify(strlen(audience) < RLSC_MAX_AUDIENCE_CHARS, catchall, );

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

		rverify(AddStringParameter("ticket", scTicket, true), catchall, rlTaskError("Failed to add params"));
		rverify(AddStringParameter("audience", audience, true), catchall, rlTaskError("Failed to add params"));
		rverify(AddIntParameter("ttlminutes", desiredTimeToLive, true), catchall, rlTaskError("Failed to add params"));

		if (userData)
		{
			rverify(AddStringParameter("userdata", userData, true), catchall, rlTaskError("Failed to add params"));
		}
		else
		{
			rverify(AddStringParameter("userdata", "", true), catchall, rlTaskError("Failed to add params"));
		}

		m_AuthTokenBuf = scAuthTokenBuf;
		m_AuthTokenBufSize = scAuthTokenBufSize;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlScCreateScAuthTokenTask::UseHttps() const
{ 
	return true;
}

const char* rlScCreateScAuthTokenTask::GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
{
	//Look up the host name from the service we're calling
	char servicePath[1024];
	const char* hn = NULL;
	if(GetServicePath(servicePath, sizeof(servicePath)))
	{
		hn = rlRos::GetServiceHost(servicePath, hostnameBuf, sizeofBuf);
	}

	if(!hn)
	{
		//If no service/hostname mapping was found then just use the default with auth- prefix
		hn = formatf(hostnameBuf, sizeofBuf, "auth-%s", g_rlTitleId->m_RosTitleId.GetDomainName());
	}

	return hn;
}

const char* rlScCreateScAuthTokenTask::GetServiceMethod() const  
{ 
	if (rlRosHttpTask::GetDefaultServices() == rlRosHttpTask::LAUNCHER_SERVICES)
	{
		return "auth.asmx/CreateScAuthToken";
	}
	else
	{
		return "socialclub.asmx/CreateScAuthToken";
	}
}

bool rlScCreateScAuthTokenTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
	rtry
	{
		const parTreeNode* result = node->FindChildWithName("Result");
		rverify(result, catchall, rlTaskError("Missing <Result>"));

		const char* scAuthToken = rlHttpTaskHelper::ReadString(result, NULL, NULL);
		rverify(scAuthToken, catchall, rlTaskError("Missing <Result> text"));

		rverify(istrlen(scAuthToken) < m_AuthTokenBufSize, catchall, rlTaskError("AuthToken was too long"));

		rlTaskDebug3("SC auth token: %s", scAuthToken);

		safecpy(m_AuthTokenBuf, scAuthToken, m_AuthTokenBufSize);

		resultCode = RLSC_ERROR_NONE;

		return true;
	}
	rcatchall
	{
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlScCreateScAuthToken2Task
///////////////////////////////////////////////////////////////////////////////
rlScCreateScAuthToken2Task::rlScCreateScAuthToken2Task()
	: m_AuthTokenBuf(NULL)
	, m_MachineTokenBuf(NULL)
	, m_AuthTokenBufSize(0)
	, m_MachineTokenBufSize(0)
	, m_SaveMachineToken(false)
{

}

bool rlScCreateScAuthToken2Task::Configure(const int localGamerIndex, const char* scTicket, 
										  char* scAuthTokenBuf, const unsigned scAuthTokenBufSize, 
										  char* machineTokenBuf, const unsigned machineTokenBufSize, bool saveMachineToken, const bool keepMeSignedIn,
										  const char* fingerprint, const char* audience, const int desiredTimeToLive, const char* userData)
{
	rtry
	{
		rverify(scAuthTokenBuf && machineTokenBuf, catchall, );
		rverify(scAuthTokenBufSize >= RLSC_MAX_SCAUTHTOKEN_CHARS, catchall, );
		rverify(machineTokenBufSize >= RLSC_MAX_MACHINETOKEN_CHARS, catchall, );
		rverify(strlen(audience) < RLSC_MAX_AUDIENCE_CHARS, catchall, );

		// if keepMeSignedIn is true, we must be saving the machine token.
		if (keepMeSignedIn)
		{
			rverify(saveMachineToken, catchall, );
		}

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

		rverify(AddStringParameter("ticket", scTicket, true), catchall, rlTaskError("Failed to add params"));
		rverify(AddStringParameter("audience", audience, true), catchall, rlTaskError("Failed to add params"));
		rverify(AddIntParameter("ttlminutes", desiredTimeToLive, true), catchall, rlTaskError("Failed to add params"));

		if (userData)
		{
			rverify(AddStringParameter("userdata", userData, true), catchall, rlTaskError("Failed to add params"));
		}
		else
		{
			rverify(AddStringParameter("userdata", "", true), catchall, rlTaskError("Failed to add params"));
		}

		rverify(AddStringParameter("rememberedMachineToken", machineTokenBuf, true), catchall, rlTaskError("Failed to add params"));
		rverify(AddStringParameter("rememberMe", saveMachineToken ? "true" : "false", true), catchall, rlTaskError("Failed to add params"));
		rverify(AddStringParameter("keepMeSignedIn", keepMeSignedIn ? "true" : "false", true), catchall, rlTaskError("Failed to add params"));

		m_HttpRequest.AddRequestHeaderValue("SCS-MFAMachineFingerPrint", fingerprint, istrlen(fingerprint));

		m_AuthTokenBuf = scAuthTokenBuf;
		m_AuthTokenBufSize = scAuthTokenBufSize;

		m_MachineTokenBuf = machineTokenBuf;
		m_MachineTokenBufSize = machineTokenBufSize;
		m_SaveMachineToken = saveMachineToken;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlScCreateScAuthToken2Task::UseHttps() const
{ 
	return true;
}

const char* rlScCreateScAuthToken2Task::GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
{
	//Look up the host name from the service we're calling
	char servicePath[1024];
	const char* hn = NULL;
	if(GetServicePath(servicePath, sizeof(servicePath)))
	{
		hn = rlRos::GetServiceHost(servicePath, hostnameBuf, sizeofBuf);
	}

	if(!hn)
	{
		//If no service/hostname mapping was found then just use the default with auth- prefix
		hn = formatf(hostnameBuf, sizeofBuf, "auth-%s", g_rlTitleId->m_RosTitleId.GetDomainName());
	}

	return hn;
}

const char* rlScCreateScAuthToken2Task::GetServiceMethod() const  
{ 
	if (rlRosHttpTask::GetDefaultServices() == rlRosHttpTask::LAUNCHER_SERVICES)
	{
		return "auth.asmx/CreateScAuthToken2";
	
	}
	else
	{
		return "socialclub.asmx/CreateScAuthToken2";
	}
}

bool rlScCreateScAuthToken2Task::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
	rtry
	{
		// Auth Token MUST be returned in response
		const char* scAuthToken = rlHttpTaskHelper::ReadString(node, NULL, "ScAuthToken");
		rverify(scAuthToken, catchall, rlTaskError("Missing <ScAuthToken> attribute text"));
		rverify(istrlen(scAuthToken) < m_AuthTokenBufSize, catchall, rlTaskError("AuthToken was too long"));

		rlTaskDebug3("SC auth token: %s", scAuthToken);
		safecpy(m_AuthTokenBuf, scAuthToken, m_AuthTokenBufSize);

		// Machine Token MAY be returned in response.
		const char* scMachineToken = rlHttpTaskHelper::ReadString(node, NULL, "RememberedMachineToken");
		if (scMachineToken)
		{
			rlTaskDebug3("SC machine token: %s", scMachineToken);

			if (m_SaveMachineToken)
			{
				safecpy(m_MachineTokenBuf, scMachineToken, m_MachineTokenBufSize);
			}
		}

		resultCode = RLSC_ERROR_NONE;

		return true;
	}
	rcatchall
	{
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlScCreateLinkTokenTask
///////////////////////////////////////////////////////////////////////////////
rlScCreateLinkTokenTask::rlScCreateLinkTokenTask()
	: m_Buf(NULL)
	, m_BufSize(0)
	, m_LocalGamerIndex(RL_INVALID_GAMER_INDEX)
{

}

bool rlScCreateLinkTokenTask::Configure(const int localGamerIndex, char* buf, const unsigned bufSize)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverifyall(buf);
		rverifyall(bufSize >= RLSC_MAX_LINKTOKEN_CHARS);
	
		rverifyall(rlRosJsonHttpTask::Configure(localGamerIndex));

		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		rverifyall(creds.IsValid());

		AddRequestHeader("Authorization", creds.GetTicket());

		const char* emptyJsonBlob = "{}";
		rverifyall(SetContent(emptyJsonBlob, istrlen(emptyJsonBlob)));

		m_Buf = buf;
		m_BufSize = bufSize;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* rlScCreateLinkTokenTask::GetServiceMethod() const
{
	return "accounts.svc/GetScAuthLinkToken";
}

bool rlScCreateLinkTokenTask::ProcessSuccess(const rlRosJsonResult& /*result*/, RsonReader* resultNode, int& /*resultCode*/)
{
	rtry
	{
		rverifyall(resultNode);

		rverifyall(resultNode->AsString(m_Buf, m_BufSize));

		rlDebug3("Link Token: %s", m_Buf);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlScRequestResetPasswordTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScRequestResetPasswordTask::Configure(const char* email)
{
    //We should have valid credentials so use them for security purposes
    int localGamerIndex = NO_LOCAL_GAMER;
    rlRos::GetFirstValidCredentials(&localGamerIndex);

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    if(!AddStringParameter("email", email, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScUpdateAccountTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScUpdateAccountTask::Configure(const int localGamerIndex,
                                 const char* avatarUrl,
                                 const char* countryCode,
                                 const char* dob,
                                 const char* email,
                                 const char* isApproxDob,
                                 const char* languageCode,
                                 const char* nickname,
                                 const char* phone,
                                 const char* zipCode)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("avatarUrl", avatarUrl, true)
        || !AddStringParameter("countryCode", countryCode, true)
        || !AddStringParameter("dob", dob, true)
        || !AddStringParameter("email", email, true)
        || !AddStringParameter("isApproxDob", isApproxDob, true)
        || !AddStringParameter("languageCode", languageCode, true)
        || !AddStringParameter("nickname", nickname, true)
        || !AddStringParameter("phone", phone, true)
        || !AddStringParameter("zipCode", zipCode, true)
        )
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScUpdateAcceptNewsletterTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScUpdateAcceptNewsletterTask::Configure(const int localGamerIndex,
                                          const bool acceptNewsletter)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("acceptNewsletter", acceptNewsletter ? "true" : "false", true)
        )
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScUpdatePasswordTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScUpdatePasswordTask::Configure(const int localGamerIndex,
                                  const char* password,
                                  const char* newPassword)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("password", password, true)
        || !AddStringParameter("newPassword", newPassword, true)
        )
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScPostUserFeedActivityTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScPostUserFeedActivityTask::Configure(const int localGamerIndex,
									    const char* activityType,
									    const s64 activityTarget,
									    const char* activityData,
									    const char* extraParams)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("activityType", activityType, true)
        || !AddIntParameter("activityTarget", activityTarget)
		|| !AddStringParameter("activityData", activityData, true)
		|| !AddStringParameter("extraParams", extraParams, true)
		)
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScRegisterComplaintTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScRegisterComplaintTask::Configure(const int localGamerIndex,
                                  const char* complaintDoc)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    //Add required params.
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("complaintDoc", complaintDoc, true)
        )
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScAcceptLegalPolicyTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScAcceptLegalPolicyTask::Configure(const int localGamerIndex,
                                     const char* policyTag,
                                     const s16 version)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("policyTag", policyTag, true)
        || !AddIntParameter("version", (int)version))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScGetAcceptedLegalPolicyVersionTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScGetAcceptedLegalPolicyVersionTask::Configure(const int localGamerIndex,
                                                 const char* policyTag,
                                                 s16* version)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("policyTag", policyTag, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_Version = version;

    return true;
}

bool 
rlScGetAcceptedLegalPolicyVersionTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    int version;
    if(rlVerify(rlHttpTaskHelper::ReadInt(version, node, "Version", NULL)))
    {
        *m_Version = (s16)version;
        return true;
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////
//  rlCashTransactionPackUSDEInfoTableRequestTask
///////////////////////////////////////////////////////////////////////////////

bool rlCashTransactionPackUSDEInfoTableRequestTask::Configure( const int localGamerIndex, rlCashPackUSDEInfo* outPackInfos, unsigned numInfos, unsigned* count )
{
	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}

	if(!AddStringParameter("ticket", cred.GetTicket(), true))
	{
		rlTaskError("Failed to add params");
		return false;
	}

	m_PackInfoList = outPackInfos;
	m_InfosCount = count;
	m_maxNumInfos = numInfos;

	return true;
}

bool rlCashTransactionPackUSDEInfoTableRequestTask::ProcessSuccess( const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/ )
{
	*m_InfosCount = 0;

	rtry
	{
		const parTreeNode* pItemListNode = node->FindChildWithName("ItemList");
		rcheck(pItemListNode, catchall, rlTaskError("Failed to find <ItemList> element"));

		unsigned count;
		rverify(rlHttpTaskHelper::ReadUns(count, pItemListNode, NULL, "count"), catchall, );
		
		if (count > m_maxNumInfos)
		{
			rlTaskError("Request is returning %d results, but there is only room to return %d", count, m_maxNumInfos);
		}
		
		if (0 != count)
		{
			parTreeNode::ChildNodeIterator iter = pItemListNode->BeginChildren();

			unsigned infoCount = 0;
			for( ;iter != pItemListNode->EndChildren() && infoCount < m_maxNumInfos; ++iter)
			{
				parTreeNode* record = *iter;
				rlAssert(record);

				const char* pPackId = rlHttpTaskHelper::ReadString(record, NULL, "id");
				rcheck(pPackId, catchall, rlTaskError("Failed to find <ItemList.Pack[%d].Id> attribute", infoCount));
				safecpy(m_PackInfoList[infoCount].m_PackName, pPackId);

				s64 vcValue = 0;
				rverify(rlHttpTaskHelper::ReadInt64(vcValue, record, NULL, "vcValue"), catchall,
					rlTaskError("Failed to find <ItemList.Pack[%d].value> attribute", infoCount));
				m_PackInfoList[infoCount].m_Value = vcValue;

				float usde = 0.0f;
				rverify(rlHttpTaskHelper::ReadFloat(usde, record, NULL, "usde"), catchall,
					rlTaskError("Failed to find <ItemList.Pack[%d].usde> attribute", infoCount));
				m_PackInfoList[infoCount].m_USDE = usde;

				infoCount++;		
			}


			*m_InfosCount = infoCount;
		}

		return true;
	}
	rcatchall
	{

	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScGetLicensePlateInfoRequestTask
///////////////////////////////////////////////////////////////////////////////

bool rlScGetLicensePlateInfoRequestTask::Configure( const int localGamerIndex, rlScLicensePlateInfo* outPlateInfos, unsigned numInfos, unsigned* count )
{
	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}
		
	if(!AddStringParameter("ticket", cred.GetTicket(), true))
	{
		rlTaskError("Failed to add param ticket");
		return false;
	}

	if(!AddIntParameter("rockstarId", cred.GetRockstarId()))
	{
		rlTaskError("Failed to add param rockstarId");
		return false;
	}

	if(!AddIntParameter("pageNumber", 0))
	{
		rlTaskError("Failed to add param pageNumber");
		return false;
	}

	if(!AddIntParameter("pageSize", numInfos))
	{
		rlTaskError("Failed to add param pageSize");
		return false;
	}

	m_LicensePlateInfoList = outPlateInfos;
	m_InfosCount = count;
	m_maxNumInfos = numInfos;

	return true;
}

bool rlScGetLicensePlateInfoRequestTask::ProcessSuccess( const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/ )
{
	*m_InfosCount = 0;

	rtry
	{
		const parTreeNode* pItemListNode = node->FindChildWithName("Plates");
		rcheck(pItemListNode, catchall, rlTaskError("Failed to find <Plates> element"));

		{
			parTreeNode::ChildNodeIterator iter = pItemListNode->BeginChildren();

			unsigned infoCount = 0;
			for( ;iter != pItemListNode->EndChildren() && infoCount < m_maxNumInfos; ++iter)
			{
				parTreeNode* plateInfo = *iter;
				rlAssert(plateInfo);

				//------------------------------

				parTreeNode* plateTextNode = plateInfo->FindChildWithName("PlateText");
				rcheck(plateTextNode, catchall, rlTaskError("Failed to find <PlateText> element"));
				if (plateTextNode)
				{
					const char* pPlateText = rlHttpTaskHelper::ReadString(plateTextNode, NULL, NULL);
					rcheck(pPlateText, catchall, rlTaskError("Failed to find <PlateText> infocount[%u] attribute", infoCount));
					safecpy(m_LicensePlateInfoList[infoCount].m_plateText, pPlateText);
				}

				//------------------------------

				parTreeNode* plateDataNode = plateInfo->FindChildWithName("PlateData");
				if (plateDataNode)
				{
					const char* pPlateData = rlHttpTaskHelper::ReadString(plateDataNode, NULL, NULL);
					if (pPlateData)
						safecpy(m_LicensePlateInfoList[infoCount].m_plateData, pPlateData);
				}

				//------------------------------

				infoCount++;		
			}


			*m_InfosCount = infoCount;
		}

		return true;
	}
	rcatchall
	{

	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
//  rlScAddLicensePlateRequestTask
///////////////////////////////////////////////////////////////////////////////

bool rlScAddLicensePlateRequestTask::Configure( const int localGamerIndex, const char* inText, const char* inData )
{
	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}

	if(!AddStringParameter("ticket", cred.GetTicket(), true))
	{
		rlTaskError("Failed to add param ticket");
		return false;
	}

	if(!AddStringParameter("plateText", inText, true))
	{
		rlTaskError("Failed to add param plateText");
		return false;
	}

	if (!inData)
	{
		if(!AddStringParameter("plateData", "", true))
		{
			rlTaskError("Failed to add param plateData");
			return false;
		}
	}
	else
	{
		if(!AddStringParameter("plateData", inData, true))
		{
			rlTaskError("Failed to add param plateData");
			return false;
		}
	}

	return true;
}


///////////////////////////////////////////////////////////////////////////////
//  rlScIsValidLicensePlateRequestTask
///////////////////////////////////////////////////////////////////////////////

bool rlScIsValidLicensePlateRequestTask::Configure( const int localGamerIndex, const char* inText, bool* pbIsValid, bool* pbIsProfane, bool* pbIsReserved, bool* pbIsMalformed )
{
	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}

	if(!AddStringParameter("ticket", cred.GetTicket(), true))
	{
		rlTaskError("Failed to add param ticket");
		return false;
	}

	if(!AddStringParameter("plateText", inText, true))
	{
		rlTaskError("Failed to add param plateText");
		return false;
	}

	if(!AddStringParameter("plateData", "", true))
	{
		rlTaskError("Failed to add param plateData");
		return false;
	}

	m_pbIsValid = pbIsValid;
	m_pbIsProfane = pbIsProfane;
	m_pbIsReserved= pbIsReserved;
	m_pbIsMalformed = pbIsMalformed;

	return true;
}

bool rlScIsValidLicensePlateRequestTask::ProcessSuccess( const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/ )
{
	rtry
	{
		bool bIsValid = false;

		rcheck(node, catchall, rlTaskError("node is invalid"));
		if (node)
		{
			parTreeNode* pIsValidNode = node->FindChildWithName("IsValid");
			if (pIsValidNode && m_pbIsValid)
			{
				rlHttpTaskHelper::ReadBool(*m_pbIsValid,pIsValidNode,NULL,NULL);

				bIsValid = *m_pbIsValid;
			}

			parTreeNode* pIsProfaneNode = node->FindChildWithName("IsProfane");
			if (pIsProfaneNode && m_pbIsProfane)
			{
				rlHttpTaskHelper::ReadBool(*m_pbIsProfane,pIsProfaneNode,NULL,NULL);	
			}

			parTreeNode* pIsReservedNode = node->FindChildWithName("IsReserved");
			if (pIsReservedNode && m_pbIsReserved)
			{
				rlHttpTaskHelper::ReadBool(*m_pbIsReserved,pIsReservedNode,NULL,NULL);		
			}

			parTreeNode* pIsMalformedNode = node->FindChildWithName("IsMalformed");
			if (pIsMalformedNode && m_pbIsMalformed)
			{
				rlHttpTaskHelper::ReadBool(*m_pbIsMalformed,pIsMalformedNode,NULL,NULL);
			}
		}

		return bIsValid;
	}
	rcatchall
	{

	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  rlScChangeLicensePlateInfoRequestTask
///////////////////////////////////////////////////////////////////////////////

bool rlScChangeNoInsertLicensePlateInfoRequestTask::Configure(const int localGamerIndex
															  ,rlScLicensePlateInfo* outPlateInfos
															  ,unsigned numInfos
															  ,unsigned* count
															  ,const char* oldPlateText
															  ,const char* newPlateText
															  ,const char* plateData)
{
	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}

	if(!AddStringParameter("ticket", cred.GetTicket(), true))
	{
		rlTaskError("Failed to add param ticket");
		return false;
	}

	if(!AddStringParameter("oldPlateText", oldPlateText, true))
	{
		rlTaskError("Failed to add param oldPlateText");
		return false;
	}

	if(!AddStringParameter("newPlateText", newPlateText, true))
	{
		rlTaskError("Failed to add param newPlateText");
		return false;
	}

	if(!AddStringParameter("plateData", plateData, true))
	{
		rlTaskError("Failed to add param plateData");
		return false;
	}

	m_LicensePlateInfoList = outPlateInfos;
	m_InfosCount = count;
	m_maxNumInfos = numInfos;

	return true;
}

bool rlScChangeNoInsertLicensePlateInfoRequestTask::ProcessSuccess( const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/ )
{
	*m_InfosCount = 0;

	rtry
	{
		const parTreeNode* pItemListNode = node->FindChildWithName("Plates");
		rcheck(pItemListNode, catchall, rlTaskError("Failed to find <Plates> element"));

		{
			parTreeNode::ChildNodeIterator iter = pItemListNode->BeginChildren();

			unsigned infoCount = 0;
			for( ;iter != pItemListNode->EndChildren() && infoCount < m_maxNumInfos; ++iter)
			{
				parTreeNode* plateInfo = *iter;
				rlAssert(plateInfo);

				//------------------------------

				parTreeNode* plateTextNode = plateInfo->FindChildWithName("PlateText");
				rcheck(plateTextNode, catchall, rlTaskError("Failed to find <PlateText> element"));
				if (plateTextNode)
				{
					const char* pPlateText = rlHttpTaskHelper::ReadString(plateTextNode, NULL, NULL);
					rcheck(pPlateText, catchall, rlTaskError("Failed to find <PlateText> infocount[%u] attribute", infoCount));
					safecpy(m_LicensePlateInfoList[infoCount].m_plateText, pPlateText);
				}

				//------------------------------

				parTreeNode* plateDataNode = plateInfo->FindChildWithName("PlateData");
				rcheck(plateDataNode, catchall, rlTaskError("Failed to find <PlateData> element"));
				if (plateDataNode)
				{
					const char* pPlateData = rlHttpTaskHelper::ReadString(plateDataNode, NULL, NULL);
					rcheck(pPlateData, catchall, rlTaskError("Failed to find <PlateData> infocount[%u] attribute", infoCount));
					safecpy(m_LicensePlateInfoList[infoCount].m_plateData, pPlateData);
				}

				//------------------------------

				infoCount++;		
			}


			*m_InfosCount = infoCount;
		}

		return true;
	}
	rcatchall
	{

	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
//  rlGetLegalTerritoryRestrictionsTask
///////////////////////////////////////////////////////////////////////////////

bool 
rlGetLegalTerritoryRestrictionsTask::Configure(const int localGamerIndex, 
												rlLegalTerritoryRestriction* outRestrictions, 
												const unsigned maxNumberRestrictions, 
												unsigned* countReturned)
{
	if (!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
	if (!cred.IsValid())
	{
		rlTaskError("Credentials are invalid");
		return false;
	}

	if (!AddStringParameter("ticket", cred.GetTicket(), true))
	{
		rlTaskError("Failed to add param ticket");
		return false;
	}

	//Grab the geo loc off from the client. Ultimately, the service doesn't use it
	//but it we'll take it for debug purposes.
	rlRosGeoLocInfo geoLocInfo;
	if (!rlRos::GetGeoLocInfo(&geoLocInfo))
	{
		rlTaskError("Failed to grab geolocation information.");
		return false;
	}

	//@TODO This is where we would slide in a command line to force a specific country/territory
	//   I think there is already support somewhere buried in GetGeoLocInfo for that.

	if (!AddStringParameter("countryCode", geoLocInfo.m_CountryCode, true))
	{
		rlTaskError("Failed to add param countryCode");
		return false;
	}

	m_Restrictions = outRestrictions;
	m_MaxNumRestrictions = maxNumberRestrictions;
	m_NumRestrictionsReturned = countReturned;

	return true;

}

bool 
rlGetLegalTerritoryRestrictionsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
	rtry
	{
		const parTreeNode* pRestrictionList = node->FindChildWithName("restrictions");
		rcheck(pRestrictionList, catchall, rlTaskError("Failed to find <restrictions> element"));

		parTreeNode::ChildNodeIterator iter = pRestrictionList->BeginChildren();

		for (u32 i = 0; i < m_MaxNumRestrictions; i++)
		{
			m_Restrictions[i].Clear();
		}

		unsigned restrictionCount = 0;
		for (; iter != pRestrictionList->EndChildren() && restrictionCount < m_MaxNumRestrictions; ++iter)
		{
			parTreeNode* record = *iter;
			rlAssert(record);

			const char* restrictionId = rlHttpTaskHelper::ReadString(record, NULL, "resId");
			rcheck(restrictionId, catchall, rlTaskError("Failed to find <restrictions.restriction[%d].resId> attribute", restrictionCount));

			const char* gameType = rlHttpTaskHelper::ReadString(record, NULL, "gameType");
			rcheck(gameType, catchall, rlTaskError("Failed to find <restrictions.restriction[%d].gameType> attribute", restrictionCount));

			bool pvcAllowed = false;
			rcheck(rlHttpTaskHelper::ReadBool(pvcAllowed, record, NULL, "pvcAllowed"),
					catchall,
					rlTaskError("Failed to find <restrictions.restriction[%d].pvcAllowed> attribute", restrictionCount));

			bool evcAllowed = false;
			rcheck(rlHttpTaskHelper::ReadBool(evcAllowed, record, NULL, "evcAllowed"),
				catchall,
				rlTaskError("Failed to find <restrictions.restriction[%d].evcAllowed> attribute", restrictionCount));

			m_Restrictions[restrictionCount].SetData(restrictionId, gameType, evcAllowed, pvcAllowed);
			restrictionCount += 1;
		}

		*m_NumRestrictionsReturned = restrictionCount;

		resultCode = RLSC_ERROR_NONE;
		return true;
	}
	rcatchall
	{
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	}

	return false;
}

}; //namespace rage
