// 
// rline/rlSocialClub.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsocialclub.h"
#include "diag/seh.h"
#include "net/status.h"
#include "string/stringutil.h"
#include "system/memory.h"
#include "system/nelem.h"

namespace rage
{
RAGE_DEFINE_SUBCHANNEL(rline, socialclub);
#undef __rage_channel
#define __rage_channel rline_socialclub

////////////////////////////////////////////////////////////////////////////////
//rlScLanguageId
////////////////////////////////////////////////////////////////////////////////
static const char* s_rlScLanguageAbbrev[RLSC_NUM_LANGUAGES] = 
{ 
    "zh",       //RLSC_LANGUAGE_CHINESE
    "en",       //RLSC_LANGUAGE_ENGLISH
    "fr",       //RLSC_LANGUAGE_FRENCH
    "de",       //RLSC_LANGUAGE_GERMAN
    "it",       //RLSC_LANGUAGE_ITALIAN
    "ja",       //RLSC_LANGUAGE_JAPANESE
    "ko",       //RLSC_LANGUAGE_KOREAN
    "pl",       //RLSC_LANGUAGE_POLISH
    "pt-eu",    //RLSC_LANGUAGE_PORTUGUESE_EUROPEAN
    "pt",       //RLSC_LANGUAGE_PORTUGUESE_BRAZILIAN,
    "ru",       //RLSC_LANGUAGE_RUSSIAN
    "es",       //RLSC_LANGUAGE_SPANISH
    "es-mx",    //RLSC_LANGUAGE_SPANISH_MEXICAN
	"zh-cn",	//RLSC_LANGUAGE_CHINESE_SIMPILIFIED
};

const char*
rlScLanguageToString(const rlScLanguage id)
{
    int index = (int)id;

    if (index >= 0 && index < RLSC_NUM_LANGUAGES)
    {
        return s_rlScLanguageAbbrev[index];
    }

    rlError("Unknown or invalid language (%d)", (int)id);
    return "UNKNOWN";
}

rlScLanguage
rlScLanguageFromString(const char* abbrev)
{
    if (abbrev == NULL) return RLSC_LANGUAGE_UNKNOWN;

    for(int i = 0; i < RLSC_NUM_LANGUAGES; i++)
    {
        if (!stricmp(abbrev, s_rlScLanguageAbbrev[i])) 
        {
            return (rlScLanguage)(i);
        }
    }

    rlError("Unknown or invalid language (%s)", abbrev);
    return RLSC_LANGUAGE_UNKNOWN;
}

////////////////////////////////////////////////////////////////////////////////
// rlScAccountInfo
////////////////////////////////////////////////////////////////////////////////
rlScAccountInfo::rlScAccountInfo()
{
    Clear();
}

void 
rlScAccountInfo::Clear()
{
    m_Age = 0;
    m_AvatarUrl[0] = '\0';
    m_CountryCode[0] = '\0';
    m_Dob[0] = '\0';
    m_Email[0] = '\0';
    m_IsApproxDob = false;
    m_LanguageCode[0] = '\0';
    m_Nickname[0] = '\0';
    m_Phone[0] = '\0';
    m_RockstarId = InvalidRockstarId;
    m_ZipCode[0] = '\0';
}

void
rlScAccountInfo::SetAvatarUrl(const char* url)
{
	if (StringNullOrEmpty(url) || strlen(url) >= sizeof(m_AvatarUrl))
	{
		rlDebug1("Invalid avatar url %s", url);
		m_AvatarUrl[0] = '\0';
	}
	else
	{
		safecpy(m_AvatarUrl, url, sizeof(m_AvatarUrl));
	}
}

////////////////////////////////////////////////////////////////////////////////
// rlScCountries
////////////////////////////////////////////////////////////////////////////////
rlScCountries::Country::Country()
: m_Name(NULL)
{
}

rlScCountries::Country::~Country()
{
    Clear();
}

void 
rlScCountries::Country::Clear()
{
    if(m_Name && !IsConstString(m_Name))
    {
        StringFree(m_Name);
        m_Name = NULL;
    }
}

bool
rlScCountries::Country::Reset(const char* code, const char* name)
{
    bool success = false;

    Clear();

    safecpy(m_Code, code, sizeof(m_Code));
    m_Name = ConstStringDuplicate(name);
    success = true;

    return success;
}

rlScCountries::rlScCountries()
{
}

rlScCountries::~rlScCountries()
{
    while(!m_List.empty())
    {
        rlScCountries::Country* country = m_List.front();

        m_List.pop_front();

        RL_DELETE(country);
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlLegalTerritoryRestrictionSto
////////////////////////////////////////////////////////////////////////////////

const rlLegalTerritoryRestriction rlLegalTerritoryRestriction::INVALID_RESTRICTION;

rlLegalTerritoryRestriction::rlLegalTerritoryRestriction()
	: m_PvcAllowed(false)
	, m_EvcAllowed(false)
	, m_RestrictionId(INVALID_RESTRICTION_ID)
	, m_GameType(INVALID_GAMETYPE)
{
	OUTPUT_ONLY(m_strValue[0] = '\0';)
}

void 
rlLegalTerritoryRestriction::Clear()
{
	*this = rlLegalTerritoryRestriction::INVALID_RESTRICTION;
	OUTPUT_ONLY(m_strValue[0] = '\0';)
}

void 
rlLegalTerritoryRestriction::SetData(const char* restrictionId, const char* gameType, const bool evcAllowed, const bool pvcAllowed)
{
	m_RestrictionId.SetFromString(restrictionId);
	m_GameType.SetFromString(gameType);
	m_EvcAllowed = evcAllowed;
	m_PvcAllowed = pvcAllowed;
	OUTPUT_ONLY(m_strValue[0] = '\0';)
}

rlLegalTerritoryRestriction& 
rlLegalTerritoryRestriction::operator=(const rlLegalTerritoryRestriction& that)
{
	m_RestrictionId = that.m_RestrictionId;
	m_GameType = that.m_GameType;
	m_PvcAllowed = that.m_PvcAllowed;
	m_EvcAllowed = that.m_EvcAllowed;
	OUTPUT_ONLY(m_strValue[0] = '\0';)

	return *this;
}

#if !__NO_OUTPUT
const char*
rlLegalTerritoryRestriction::c_str() const
{
	if('\0' == m_strValue[0])
	{
		formatf(m_strValue, "id='%s : %08X', type='%s : %08X', evc='%s', pvc='%s'", m_RestrictionId.TryGetCStr(), m_RestrictionId.GetHash(), m_GameType.TryGetCStr(), m_GameType.GetHash(), m_EvcAllowed ? "true":"false", m_PvcAllowed ? "true":"false");
	}

	return m_strValue;
}
#endif  //!__NO_OUTPUT
}   //namespace rage
