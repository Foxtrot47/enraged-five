// 
// rline/rlSocialClubTask.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSOCIALCLUBTASK_H
#define RLINE_RLSOCIALCLUBTASK_H

#include "rlsocialclubcommon.h"
#include "rline/rlhttptask.h"
#include "rline/ros/rlroscredtasks.h"

namespace rage
{

class parTree;
class parTreeNode;

///////////////////////////////////////////////////////////////////////////////
//  rlScTaskBase
///////////////////////////////////////////////////////////////////////////////
class rlScTaskBase : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScTaskBase);

    rlScTaskBase() {}
    virtual ~rlScTaskBase() {}

    //Version for non-derived tasks.  Does same this as overridden ProcessError.
    static void HandleError(const rlRosResult& result, int& resultCode);

protected:
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

///////////////////////////////////////////////////////////////////////////////
//  rlScCreateAccountTask
///////////////////////////////////////////////////////////////////////////////
class rlScCreateAccountTask : public rlRosCredentialsChangingTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScCreateAccountTask);

    rlScCreateAccountTask() {}
    virtual ~rlScCreateAccountTask() {}

    bool Configure(const int localGamerIndex,
                   const bool acceptNewsletter,
                   const char* avatarUrl,
                   const char* countryCode,
                   const char* dob,
                   const char* email,
                   const bool isApproxDob,
                   const char* languageCode,
                   const char* nickname,		
                   const char* password,
                   const char* phone,
                   const char* zipCode);

#if RLROS_SC_PLATFORM
    virtual bool UseHttps() const { return true; } //Required because blade also calls this
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/CreateAccountSc"; }
#else
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/CreateAccount"; }
#endif

    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

#if RLROS_SC_PLATFORM
protected:
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    char m_Email[RLSC_MAX_EMAIL_CHARS];
    char m_Password[RLSC_MAX_PASSWORD_CHARS];
#endif
};

#if !RLROS_SC_PLATFORM

///////////////////////////////////////////////////////////////////////////////
//  rlScLinkAccountTask
///////////////////////////////////////////////////////////////////////////////
class rlScLinkAccountTask : public rlRosCredentialsChangingTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScLinkAccountTask);

    rlScLinkAccountTask() {}
    virtual ~rlScLinkAccountTask() {}

    bool Configure(const int localGamerIndex,
                   const char* email,
                   const char* nickname,		
                   const char* password);

    virtual bool UseHttps() const 
    {
#if RSG_NP
        return rlRosCredentialsChangingTask::UseHttps();
#else
        return true; //Required because blade calls this w/o encryption
#endif
    }
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/LinkAccount2"; }
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

///////////////////////////////////////////////////////////////////////////////
//  rlScUnlinkAccountTask
///////////////////////////////////////////////////////////////////////////////
class rlScUnlinkAccountTask : public rlRosCredentialsChangingTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScUnlinkAccountTask);

    rlScUnlinkAccountTask() {}
    virtual ~rlScUnlinkAccountTask() {}

    bool Configure(const int localGamerIndex);

    virtual const char* GetServiceMethod() const { return "socialclub.asmx/UnlinkAccount"; }
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

#endif  //!RLROS_SC_PLATFORM

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckEmailTask
///////////////////////////////////////////////////////////////////////////////
class rlScCheckEmailTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScCheckEmailTask);

    rlScCheckEmailTask() {}
    virtual ~rlScCheckEmailTask() {}

    bool Configure(const int localGamerIndex, const char* email);

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/CheckEmail"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckNicknameTask
///////////////////////////////////////////////////////////////////////////////
class rlScCheckNicknameTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScCheckNicknameTask);

    rlScCheckNicknameTask() {}
    virtual ~rlScCheckNicknameTask() {}

    bool Configure(const int localGamerIndex, const char* nickname);

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/CheckNickname"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckPasswordTask
///////////////////////////////////////////////////////////////////////////////
class rlScCheckPasswordTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScCheckPasswordTask);

    rlScCheckPasswordTask() : m_PasswordReqs(NULL) {}
    virtual ~rlScCheckPasswordTask() {}

    bool Configure(const int localGamerIndex, const char* password, rlScPasswordReqs* passwordReqs);

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/ValidatePassword"; }

    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlScPasswordReqs* m_PasswordReqs;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScGetPasswordRequirementsTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetPasswordRequirementsTask : public rlScTaskBase
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlScGetPasswordRequirementsTask);

	rlScGetPasswordRequirementsTask() {}
	virtual ~rlScGetPasswordRequirementsTask() {}

	bool Configure(const int localGamerIndex, rlScPasswordReqs* passwordReqs);

protected:
	virtual const char* GetServiceMethod() const { return "socialclub.asmx/GetPasswordRequirements"; }
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
	rlScPasswordReqs* m_PasswordReqs;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScCheckTextTask
///////////////////////////////////////////////////////////////////////////////
class rlScCheckTextTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScCheckTextTask);

    rlScCheckTextTask() : m_profaneWord(NULL) {}
    virtual ~rlScCheckTextTask() {}

    bool Configure(const int localGamerIndex, const char* text, const rlScLanguage language, char* profaneWord);

	static const unsigned MAX_PROFANE_WORD_LENGTH = 64;

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/CheckText"; }

	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	char* m_profaneWord;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScGetCountriesTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetCountriesTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScGetCountriesTask);

    rlScGetCountriesTask() {}
    virtual ~rlScGetCountriesTask() {}

    bool Configure(const int localGamerIndex, rlScCountries* countries);

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/GetCountries"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlScCountries* m_Countries;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScGetAccountInfoTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetAccountInfoTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScGetAccountInfoTask);

    rlScGetAccountInfoTask() {}
    virtual ~rlScGetAccountInfoTask() {}

    bool Configure(const int localGamerIndex, rlScAccountInfo* acct);

protected:
    virtual bool UseHttps() const 
    {
#if RSG_NP
        return rlScTaskBase::UseHttps();
#else
        return true; //Required because blade calls this w/o encryption
#endif
    }
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/GetAccountInfo"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlScAccountInfo* m_Acct;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScGetAlternateNicknamesTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetAlternateNicknamesTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScGetAlternateNicknamesTask);

    rlScGetAlternateNicknamesTask() {}
    virtual ~rlScGetAlternateNicknamesTask() {}

    bool Configure(const int localGamerIndex, 
                   const char* nickname,
                   char (*alternates)[RLSC_MAX_NICKNAME_CHARS],
                   const unsigned maxAlternates,
                   unsigned* numAlternates);

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/GetAlternateNicknames"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    char (*m_Alternates)[RLSC_MAX_NICKNAME_CHARS];
    unsigned m_MaxAlternates;
    unsigned* m_NumAlternates;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScGetScAuthTokenTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetScAuthTokenTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScGetScAuthTokenTask);

    rlScGetScAuthTokenTask() {}
    virtual ~rlScGetScAuthTokenTask() {}

    bool Configure(const int localGamerIndex,  char* buf,  const unsigned bufSize);
	void Finish(const FinishType finishType, const int resultCode = 0);

    virtual const char* GetServiceMethod() const { return "socialclub.asmx/GetScAuthToken"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    char* m_Buf;
    unsigned m_BufSize;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScCreateScAuthTokenTask
///////////////////////////////////////////////////////////////////////////////
class rlScCreateScAuthTokenTask : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlScCreateScAuthTokenTask);

	rlScCreateScAuthTokenTask();
	virtual ~rlScCreateScAuthTokenTask() {}

	bool Configure(const int localGamerIndex, 
		const char* scTicket, 
		char* authTokenBuf, 
		const unsigned authTokenBufSize,
		const char* audience, 
		const int desiredTimeToLive, 
		const char* userData);

protected:

	bool UseHttps() const;
	virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	virtual const char* GetServiceMethod() const;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	char* m_AuthTokenBuf;
	unsigned m_AuthTokenBufSize;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScCreateScAuthToken2Task
///////////////////////////////////////////////////////////////////////////////
class rlScCreateScAuthToken2Task : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlScCreateScAuthToken2Task);

	rlScCreateScAuthToken2Task();
	virtual ~rlScCreateScAuthToken2Task() {}

	bool Configure(const int localGamerIndex, 
				   const char* scTicket, 
				   char* authTokenBuf, 
				   const unsigned authTokenBufSize, 
				   char* machineTokenBuf, 
				   const unsigned machineTokenBufSize, 
				   const bool saveMachine,
				   const bool keepMeSignedIn,
				   const char* fingerprint,
				   const char* audience, 
				   const int desiredTimeToLive, 
				   const char* userData);

protected:
	
	bool UseHttps() const;
	virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	virtual const char* GetServiceMethod() const;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	char* m_AuthTokenBuf;
	unsigned m_AuthTokenBufSize;
	char* m_MachineTokenBuf;
	unsigned m_MachineTokenBufSize;
	bool m_SaveMachineToken;
};

///////////////////////////////////////////////////////////////////////////////
// rlScCreateLinkTokenTask
///////////////////////////////////////////////////////////////////////////////
class rlScCreateLinkTokenTask : public rlRosJsonHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlScCreateLinkTokenTask);

	rlScCreateLinkTokenTask();
	virtual ~rlScCreateLinkTokenTask() {}

	bool Configure(const int localGamerIndex, char* buf, const unsigned bufSize);

protected:

	virtual const char* GetServiceMethod() const override;
	virtual bool ProcessSuccess(const rlRosJsonResult& result, RsonReader* resultNode, int& resultCode) override;

	char* m_Buf;
	unsigned m_BufSize;
	int m_LocalGamerIndex;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScRequestResetPasswordTask
///////////////////////////////////////////////////////////////////////////////
class rlScRequestResetPasswordTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScRequestResetPasswordTask);

    rlScRequestResetPasswordTask() {}
    virtual ~rlScRequestResetPasswordTask() {}

    bool Configure(const char* email);

protected:
    virtual bool UseHttps() const 
    {
#if RSG_NP
        return rlScTaskBase::UseHttps();
#else
        return true; //Required because blade calls this w/o encryption
#endif
    }
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/RequestResetPassword"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScUpdateAccountTask
///////////////////////////////////////////////////////////////////////////////
class rlScUpdateAccountTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScUpdateAccountTask);

    rlScUpdateAccountTask() {}
    virtual ~rlScUpdateAccountTask() {}

    bool Configure(const int localGamerIndex,
                   const char* avatarUrl,
                   const char* countryCode,
                   const char* dob,
                   const char* email,
                   const char* isApproxDob,
                   const char* languageCode,
                   const char* nickname,
                   const char* phone,
                   const char* zipCode);

protected:
    virtual bool UseHttps() const 
    {
#if RSG_NP
        return rlScTaskBase::UseHttps();
#else
        return true; //Required because blade calls this w/o encryption
#endif
    }
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/UpdateAccount"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScUpdateAcceptNewsletterTask
///////////////////////////////////////////////////////////////////////////////
class rlScUpdateAcceptNewsletterTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScUpdateAcceptNewsletterTask);

    rlScUpdateAcceptNewsletterTask() {}
    virtual ~rlScUpdateAcceptNewsletterTask() {}

    bool Configure(const int localGamerIndex,
                   const bool acceptNewsletter);

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/UpdateAcceptNewsletter"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScUpdatePasswordTask
///////////////////////////////////////////////////////////////////////////////
class rlScUpdatePasswordTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScUpdatePasswordTask);

    rlScUpdatePasswordTask() {}
    virtual ~rlScUpdatePasswordTask() {}

    bool Configure(const int localGamerIndex,
                   const char* password,
                   const char* newPassword);

protected:
    virtual bool UseHttps() const 
    {
#if RSG_NP
        return rlScTaskBase::UseHttps();
#else
        return true; //Required because blade calls this w/o encryption
#endif
    }
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/UpdatePassword"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScPostUserFeedActivityTask
///////////////////////////////////////////////////////////////////////////////
class rlScPostUserFeedActivityTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScPostUserFeedActivityTask);

    rlScPostUserFeedActivityTask() {}
    virtual ~rlScPostUserFeedActivityTask() {}

    bool Configure(const int localGamerIndex,
                   const char* activityType,
				   const s64 activityTarget,
				   const char* activityData,
				   const char* extraParams);

protected:
    virtual const char* GetServiceMethod() const { return "socialclub.asmx/PostUserFeedActivity"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScRegisterComplaintTask
///////////////////////////////////////////////////////////////////////////////
class rlScRegisterComplaintTask : public rlScTaskBase
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlScRegisterComplaintTask);

	rlScRegisterComplaintTask() {}
	virtual ~rlScRegisterComplaintTask() {}

    bool Configure(const int localGamerIndex,
                   const char* complaintDoc);

protected:
	virtual const char* GetServiceMethod() const { return "Complaint.asmx/WriteComplaint"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScAcceptLegalPolicyTask
///////////////////////////////////////////////////////////////////////////////
class rlScAcceptLegalPolicyTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScAcceptLegalPolicyTask);

    rlScAcceptLegalPolicyTask() {}
    virtual~ rlScAcceptLegalPolicyTask() {}

    bool Configure(const int localGamerIndex,
                   const char* policyTag,
                   const s16 version);

protected:
    virtual const char* GetServiceMethod() const { return "legalpolicies.asmx/AcceptVersion"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScGetAcceptedLegalPolicyVersionTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetAcceptedLegalPolicyVersionTask : public rlScTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlScGetAcceptedLegalPolicyVersionTask);

    rlScGetAcceptedLegalPolicyVersionTask() {}
    virtual~ rlScGetAcceptedLegalPolicyVersionTask() {}

    bool Configure(const int localGamerIndex,
                   const char* policyTag,
                   s16* version);

protected:
    virtual const char* GetServiceMethod() const { return "legalpolicies.asmx/GetAcceptedVersion"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    s16* m_Version;
};

///////////////////////////////////////////////////////////////////////////////
//  rlCashTransactionPackUSDEInfoTableRequestTask
///////////////////////////////////////////////////////////////////////////////
class rlCashTransactionPackUSDEInfoTableRequestTask : public rlRosHttpTask
{
public:
	
	RL_TASK_USE_CHANNEL(rline);
	RL_TASK_DECL(rlCashTransactionPackUSDEInfoTableRequestTask);

	rlCashTransactionPackUSDEInfoTableRequestTask() {}
	virtual ~rlCashTransactionPackUSDEInfoTableRequestTask() {}

	bool Configure(const int localGamerIndex, rlCashPackUSDEInfo* outPackInfos, unsigned numInfos, unsigned* count);

protected:
	virtual const char* GetServiceMethod() const { return "CashTransactions.asmx/GetPackValueUSDE"; }
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	rlCashPackUSDEInfo* m_PackInfoList;
	unsigned* m_InfosCount;
	unsigned m_maxNumInfos;
	
};

///////////////////////////////////////////////////////////////////////////////
//  rlGetLegalTerritoryRestrictionsTask
///////////////////////////////////////////////////////////////////////////////
class rlGetLegalTerritoryRestrictionsTask : public rlRosHttpTask
{
public:

	RL_TASK_USE_CHANNEL(rline);
	RL_TASK_DECL(rlGetLegalTerritoryRestrictionsTask);

	rlGetLegalTerritoryRestrictionsTask() {}
	virtual ~rlGetLegalTerritoryRestrictionsTask() {}

	bool Configure(const int localGamerIndex, rlLegalTerritoryRestriction* outRestrictions, const unsigned maxNumRestrictions, unsigned* countReturned);

protected:
	virtual const char* GetServiceMethod() const { return "CashTransactions.asmx/GetLegalTerritoryRestrictions"; }
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	rlLegalTerritoryRestriction* m_Restrictions;
	unsigned m_MaxNumRestrictions;
	unsigned* m_NumRestrictionsReturned;

};

///////////////////////////////////////////////////////////////////////////////
//  rlScGetLicensePlateInfoRequestTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetLicensePlateInfoRequestTask : public rlRosHttpTask
{
public:

	RL_TASK_USE_CHANNEL(rline);
	RL_TASK_DECL(rlScGetLicensePlateInfoRequestTask);

	rlScGetLicensePlateInfoRequestTask() {}
	virtual ~rlScGetLicensePlateInfoRequestTask() {}

	bool Configure(const int localGamerIndex, rlScLicensePlateInfo* outPackInfos, unsigned numInfos, unsigned* count);

protected:
	virtual const char* GetServiceMethod() const { return "licenseplates.asmx/Get"; }
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	rlScLicensePlateInfo* m_LicensePlateInfoList;
	unsigned* m_InfosCount;
	unsigned m_maxNumInfos;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScAddLicensePlateRequestTask
///////////////////////////////////////////////////////////////////////////////
class rlScAddLicensePlateRequestTask : public rlRosHttpTask
{
public:

	RL_TASK_USE_CHANNEL(rline);
	RL_TASK_DECL(rlScAddLicensePlateRequestTask);

	rlScAddLicensePlateRequestTask() {}
	virtual ~rlScAddLicensePlateRequestTask() {}

	bool Configure(const int localGamerIndex, const char* inText, const char* inData);

protected:
	virtual const char* GetServiceMethod() const { return "licenseplates.asmx/Add"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlScIsValidLicensePlateRequestTask
///////////////////////////////////////////////////////////////////////////////
class rlScIsValidLicensePlateRequestTask : public rlRosHttpTask
{
public:

	RL_TASK_USE_CHANNEL(rline);
	RL_TASK_DECL(rlScIsValidLicensePlateRequestTask);

	rlScIsValidLicensePlateRequestTask() {}
	virtual ~rlScIsValidLicensePlateRequestTask() {}

	bool Configure(const int localGamerIndex, const char* inText, bool* pbIsValid, bool* pbIsProfane, bool* pbIsReserved, bool* pbIsMalformed);

protected:
	virtual const char* GetServiceMethod() const { return "licenseplates.asmx/IsValid"; }
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	bool* m_pbIsValid;
	bool* m_pbIsProfane;
	bool* m_pbIsReserved;
	bool* m_pbIsMalformed;
};

///////////////////////////////////////////////////////////////////////////////
//  rlScChangeLicensePlateInfoRequestTask
///////////////////////////////////////////////////////////////////////////////
class rlScChangeNoInsertLicensePlateInfoRequestTask : public rlRosHttpTask
{
public:

	RL_TASK_USE_CHANNEL(rline);
	RL_TASK_DECL(rlScChangeNoInsertLicensePlateInfoRequestTask);

	rlScChangeNoInsertLicensePlateInfoRequestTask() {}
	virtual ~rlScChangeNoInsertLicensePlateInfoRequestTask() {}

	bool Configure(const int localGamerIndex
					,rlScLicensePlateInfo* outPackInfos
					,unsigned numInfos
					,unsigned* count
					,const char* oldPlateText
					,const char* newPlateText
					,const char* plateData);

protected:
	virtual const char* GetServiceMethod() const { return "licenseplates.asmx/ChangeNoInsert"; }
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	rlScLicensePlateInfo* m_LicensePlateInfoList;
	unsigned* m_InfosCount;
	unsigned m_maxNumInfos;
};

} //namespace rage

#endif  //RLINE_RLSOCIALCLUBTASK_H
