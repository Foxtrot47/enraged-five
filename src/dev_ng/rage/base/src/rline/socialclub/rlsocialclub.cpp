// 
// rline/rlSocialClub.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsocialclub.h"
#include "rlsocialclubtask.h"
#include "diag/seh.h"
#include "net/status.h"
#include "rline/ros/rlros.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

namespace rage
{

#if RLROS_SC_PLATFORM
//Logins for each local gamer.  When set, ROS will automatically attempt
//to create and refresh credentials for them.
char rlSocialClub::m_Email[RL_MAX_LOCAL_GAMERS][RLSC_MAX_EMAIL_CHARS];
char rlSocialClub::m_ScAuthToken[RL_MAX_LOCAL_GAMERS][RLSC_MAX_SCAUTHTOKEN_CHARS];
#endif

////////////////////////////////////////////////////////////////////////////////
// rlSocialClub
////////////////////////////////////////////////////////////////////////i////////
void
rlSocialClub::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//Convenience macro to create and queue Social Club tasks
#define RLSOCIALCLUB_CREATETASK(T, ...)									                    \
    bool success = false;																	\
    T* task = NULL;																			\
    rtry																					\
    {																						\
        task = rlGetTaskManager()->CreateTask<T>();								            \
        rverify(task,catchall,);															\
        rverify(rlTaskBase::Configure(task, __VA_ARGS__), catchall,);		                \
        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);			            \
        success = true;																		\
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
            if(task->IsPending())															\
            {																				\
                task->Finish(rlTaskBase::FINISH_FAILED,										\
                RLSC_ERROR_UNEXPECTED_RESULT);						                        \
            }																				\
            else																			\
            {																				\
                status->ForceFailed();														\
            }																				\
            rlGetTaskManager()->DestroyTask(task);								            \
        }																					\
    }																						\
    return success;

#if RLROS_SC_PLATFORM

void
rlSocialClub::ClearLogin(const int localGamerIndex)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    
	m_Email[localGamerIndex][0] = '\0';
    m_ScAuthToken[localGamerIndex][0] = '\0';

    //Immediately clear the user's credentials.
    rlRos::SetCredentials(localGamerIndex, NULL);
}

const char* 
rlSocialClub::GetLoginEmail(const int localGamerIndex)
{
	if(HasLogin(localGamerIndex))
	{
		return m_Email[localGamerIndex];
	}
	return NULL;    
}

const char* 
rlSocialClub::GetLoginScAuthToken(const int localGamerIndex)
{
    if(HasLogin(localGamerIndex))
    {
        return m_ScAuthToken[localGamerIndex];
    }
    return NULL;    
}

bool 
rlSocialClub::HasLogin(const int localGamerIndex)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));

    return (m_Email[localGamerIndex][0] != '\0') && (m_ScAuthToken[localGamerIndex][0] != '\0');
}

bool 
rlSocialClub::SetLogin(const int localGamerIndex, const char* scEmail, const char* scAuthToken)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
	rlAssert(scEmail);
    rlAssert(scAuthToken);

	safecpy(m_Email[localGamerIndex], scEmail, sizeof(m_Email[localGamerIndex]));
    safecpy(m_ScAuthToken[localGamerIndex], scAuthToken, sizeof(m_ScAuthToken[localGamerIndex]));

	if(rlVerify(strchr(m_Email[localGamerIndex], '@') != NULL) &&
		rlVerify(strchr(m_Email[localGamerIndex], '.') != NULL))
	{
		return true;
	}

	return false;
}

#else

bool 
rlSocialClub::LinkAccount(const int localGamerIndex,
                          const char* email,
                          const char* nickname,		
                          const char* password,
                          netStatus* status)
{
    if (!CanLinkAccounts(localGamerIndex))
    {
        if (status)
        {
            status->ForceFailed(RLSC_ERROR_PLATFORM_PRIVILEGE);
        }
        return true;
    }

    RLSOCIALCLUB_CREATETASK(rlScLinkAccountTask,
        localGamerIndex,
        email,
        nickname,		
        password,
        status);
}

bool 
rlSocialClub::UnlinkAccount(const int localGamerIndex, netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScUnlinkAccountTask,
        localGamerIndex,
        status);
}

#endif

bool 
rlSocialClub::CanLinkAccounts(const int /*localGamerIndex*/)
{
    return true;
}

bool
rlSocialClub::CheckEmail(const int localGamerIndex,
                         const char* email,
                         netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScCheckEmailTask,
        localGamerIndex,
        email,
        status);
}

bool
rlSocialClub::CheckNickname(const int localGamerIndex,
                            const char* nickname,
                            netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScCheckNicknameTask,
        localGamerIndex,
        nickname,
        status);
}

bool
rlSocialClub::CheckPassword(const int localGamerIndex,
                            const char* password,
                            rlScPasswordReqs* passwordReqs,
                            netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScCheckPasswordTask,
        localGamerIndex,
        password,
        passwordReqs,
        status);
}

bool 
rlSocialClub::GetPasswordRequirements(const int localGamerIndex, rlScPasswordReqs* passwordReqs, netStatus* status)
{
	RLSOCIALCLUB_CREATETASK(rlScGetPasswordRequirementsTask, localGamerIndex, passwordReqs, status);
}


bool 
rlSocialClub::CheckText(const int localGamerIndex, 
                        const char* text, 
						const rlScLanguage language,
						char* profaneWord,
                        netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScCheckTextTask,
        localGamerIndex,
        text,
		language,
		profaneWord,
        status);
}

bool
rlSocialClub::GetAlternateNicknames(const int localGamerIndex,
                                    const char* nickname,
                                    char (*alternates)[RLSC_MAX_NICKNAME_CHARS],
                                    const unsigned maxAlternates,
                                    unsigned* numAlternates,
                                    netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScGetAlternateNicknamesTask,
        localGamerIndex,
        nickname,
        alternates,
        maxAlternates,
        numAlternates,
        status);
}

bool 
rlSocialClub::CreateAccount(const int localGamerIndex,
                            const bool acceptNewsletter,
                            const char* avatarUrl,
                            const char* countryCode,
                            const char* dob,
                            const char* email,
                            const bool isApproxDob,
                            const char* languageCode,
                            const char* nickname,		
                            const char* password,
                            const char* phone,
                            const char* zipCode,
                            netStatus* status)
{
    if (!CanLinkAccounts(localGamerIndex))
    {
        if (status)
        {
            status->ForceFailed(RLSC_ERROR_PLATFORM_PRIVILEGE);
        }
        return true;
    }

    RLSOCIALCLUB_CREATETASK(rlScCreateAccountTask,
        localGamerIndex,
        acceptNewsletter,
        avatarUrl,
        countryCode,
        dob,
        email,
        isApproxDob,
        languageCode,
        nickname,		
        password,
        phone,
        zipCode,
        status);
}

bool 
rlSocialClub::GetAccountInfo(const int localGamerIndex,
                             rlScAccountInfo* acct,
                             netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScGetAccountInfoTask,
        localGamerIndex,
        acct,
        status);
}

bool 
rlSocialClub::GetCountries(const int localGamerIndex,
                           rlScCountries& countries,
                           netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScGetCountriesTask,
        localGamerIndex,
        &countries,
        status);
}

bool 
rlSocialClub::GetScAuthToken(const int localGamerIndex,
                             char* buf,
                             const unsigned bufSize,
                             netStatus* status)
{
    rlAssert(buf);
    rlAssert(bufSize >= RLSC_MAX_SCAUTHTOKEN_CHARS);    

    RLSOCIALCLUB_CREATETASK(rlScGetScAuthTokenTask,
        localGamerIndex,
        buf,
        bufSize,
        status);
}

bool 
rlSocialClub::CreateScAuthToken(const int localGamerIndex,
								const char* scTicket,
								char* authTokenBuf,
								const unsigned authTokenBufSize,
								const char* audience,
								const int ttlMinutes,
								const char* userData,
								netStatus* status)
{
	rlAssert(scTicket);
	rlAssert(strlen(scTicket) < RLROS_MAX_TICKET_SIZE);
	rlAssert(authTokenBuf);
	rlAssert(authTokenBufSize >= RLSC_MAX_SCAUTHTOKEN_CHARS);
	rlAssert(audience);
	rlAssert(strlen(audience) < RLSC_MAX_AUDIENCE_CHARS);

	RLSOCIALCLUB_CREATETASK(rlScCreateScAuthTokenTask, 
		localGamerIndex, 
		scTicket, 
		authTokenBuf, 
		authTokenBufSize,
		audience, 
		ttlMinutes, 
		userData, 
		status);
}

bool 
rlSocialClub::CreateScAuthToken2(const int localGamerIndex,
								const char* scTicket,
								char* authTokenBuf,
								const unsigned authTokenBufSize,
								char* machineTokenBuf,
								const unsigned machineTokenBufSize,
								const bool saveMachine,
								const bool keepMeSignedIn,
								const char* fingerprint,
								const char* audience,
								const int ttlMinutes,
								const char* userData,
								netStatus* status)
{
	rlAssert(scTicket);
	rlAssert(strlen(scTicket) < RLROS_MAX_TICKET_SIZE);
	rlAssert(authTokenBuf);
	rlAssert(authTokenBufSize >= RLSC_MAX_SCAUTHTOKEN_CHARS);
	rlAssert(machineTokenBuf);
	rlAssert(machineTokenBufSize >= RLSC_MAX_MACHINETOKEN_CHARS);
	rlAssert(audience);
	rlAssert(strlen(audience) < RLSC_MAX_AUDIENCE_CHARS);
	
	// We can't keep the user MFA signed in without saving their machine
	if (keepMeSignedIn)
	{
		rlAssert(saveMachine);
	}

	RLSOCIALCLUB_CREATETASK(rlScCreateScAuthToken2Task, 
		localGamerIndex, 
		scTicket, 
		authTokenBuf, 
		authTokenBufSize, 
		machineTokenBuf, 
		machineTokenBufSize, 
		saveMachine, 
		keepMeSignedIn,
		fingerprint, 
		audience, 
		ttlMinutes, 
		userData, 
		status);
}

bool rlSocialClub::CreateLinkToken(const int localGamerIndex, char* buf, const unsigned bufSize, netStatus* status)
{
	rlAssert(buf);
	rlAssert(bufSize >= RLSC_MAX_LINKTOKEN_CHARS);

	RLSOCIALCLUB_CREATETASK(rlScCreateLinkTokenTask, localGamerIndex, buf, bufSize, status);
}

bool
rlSocialClub::IsConnectedToSocialClub(const int localGamerIndex)
{
    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    return cred.IsValid() && (cred.GetRockstarId() != InvalidRockstarId);
}

bool 
rlSocialClub::RequestResetPassword(const char* email, netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScRequestResetPasswordTask,
        email,
        status);
}

bool 
rlSocialClub::UpdateAccount(const int localGamerIndex,
                            const char* avatarUrl,
                            const char* countryCode,
                            const char* dob,
                            const char* email,
                            const char* isApproxDob,
                            const char* languageCode,
                            const char* nickname,
                            const char* phone,
                            const char* zipCode,
                            netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScUpdateAccountTask,
        localGamerIndex,
        avatarUrl,
        countryCode,
        dob,
        email,
        isApproxDob,
        languageCode,
        nickname,
        phone,
        zipCode,
        status);
}

bool 
rlSocialClub::UpdateAcceptNewsletter(const int localGamerIndex,
                                     const bool acceptNewsletter,
                                     netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScUpdateAcceptNewsletterTask,
        localGamerIndex,
        acceptNewsletter,
        status);
}

bool 
rlSocialClub::UpdatePassword(const int localGamerIndex,
                             const char* password,
                             const char* newPassword,
                             netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScUpdatePasswordTask,
        localGamerIndex,
        password,
        newPassword,
        status);
}

bool 
rlSocialClub::PostUserFeedActivity(const int localGamerIndex,
								   const char* activityType,
								   const s64 activityTarget,
								   const char* activityData,
								   const char* extraParams)
{
	bool success = false;														
	rlFireAndForgetTask<rlScPostUserFeedActivityTask>* task = NULL;															
	rtry																	
	{																			
		rverify(rlGetTaskManager()->CreateTask(&task),catchall,);								    
		rverify(task,catchall,);													
		rverify(rlTaskBase::Configure(	task,
										localGamerIndex,
										activityType,
										activityTarget,
										activityData,
										extraParams,
										&task->m_Status), catchall,);	

		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);			    
		success = true;																
	}																			
	rcatchall																	
	{																			
		if(task)																	
		{																			
			if(task->IsPending())														
			{																			
				task->Finish(rlTaskBase::FINISH_FAILED,										
					RLSC_ERROR_UNEXPECTED_RESULT);						                        
			}																			
																						
			rlGetTaskManager()->DestroyTask(task);								        
		}																			
	}																			
	return success;
}

bool 
rlSocialClub::RegisterComplaint(const int localGamerIndex,
								const char* complaintDoc,
								netStatus* status)
{
	RLSOCIALCLUB_CREATETASK(rlScRegisterComplaintTask,
							localGamerIndex,
							complaintDoc,
							status);
}

bool 
rlSocialClub::AcceptLegalPolicy(const int localGamerIndex, 
                                const char* policyTag, 
                                const s16 version,
                                netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScAcceptLegalPolicyTask,
        localGamerIndex,
        policyTag,
        version,
        status);
}

bool 
rlSocialClub::GetAcceptedLegalPolicyVersion(const int localGamerIndex, 
                                            const char* policyTag, 
                                            s16* version,
                                            netStatus* status)
{
    RLSOCIALCLUB_CREATETASK(rlScGetAcceptedLegalPolicyVersionTask,
        localGamerIndex,
        policyTag,
        version,
        status);
}

bool rlSocialClub::GetCashTransactionsPackUSDEInfo( const int localGamerIndex, rlCashPackUSDEInfo* packInfos, unsigned maxCount, unsigned* count, netStatus* status )
{
	RLSOCIALCLUB_CREATETASK(rlCashTransactionPackUSDEInfoTableRequestTask,
		localGamerIndex,
		packInfos,
		maxCount,
		count,
		status
		)
}

bool rlSocialClub::GetLegalTerritoryRestrictions(const int localGamerIndex, rlLegalTerritoryRestriction* restrictions, const unsigned maxCount, unsigned* numReturned, netStatus* status)
{
	RLSOCIALCLUB_CREATETASK(rlGetLegalTerritoryRestrictionsTask,
		localGamerIndex,
		restrictions, maxCount, numReturned,
		status);
}

bool rlSocialClub::GetLicensePlateInfo( const int localGamerIndex, rlScLicensePlateInfo* plateInfos, unsigned maxCount, unsigned* count, netStatus* status )
{
	RLSOCIALCLUB_CREATETASK(rlScGetLicensePlateInfoRequestTask,
		localGamerIndex,
		plateInfos,
		maxCount,
		count,
		status
		)
}

bool rlSocialClub::AddLicensePlate( const int localGamerIndex, const char* inText, const char* inData, netStatus* status)
{
	RLSOCIALCLUB_CREATETASK(rlScAddLicensePlateRequestTask,
		localGamerIndex,
		inText,
		inData,
		status
		)
}

bool rlSocialClub::IsValidLicensePlate( const int localGamerIndex, const char* inText, bool* pbIsValid, bool* pbIsProfane, bool* pbIsReserved, bool* pbIsMalformed, netStatus* status)
{
	RLSOCIALCLUB_CREATETASK(rlScIsValidLicensePlateRequestTask,
		localGamerIndex,
		inText,
		pbIsValid,
		pbIsProfane,
		pbIsReserved,
		pbIsMalformed,
		status
		)
}

bool rlSocialClub::ChangePlateDataNoInsert(const int localGamerIndex
										,rlScLicensePlateInfo* plateInfos
										,unsigned maxCount
										,unsigned* count 
										,const char* oldPlateText
										,const char* plateText
										,const char* plateData
										,netStatus* status)
{
	RLSOCIALCLUB_CREATETASK(rlScChangeNoInsertLicensePlateInfoRequestTask
		,localGamerIndex
		,plateInfos
		,maxCount
		,count
		,oldPlateText
		,plateText
		,plateData
		,status)
}


}   //namespace rage
