// 
// rline/rlsocialclubcommon.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSOCIALCLUBCOMMON_H
#define RLINE_RLSOCIALCLUBCOMMON_H

#include "rline/rl.h"
#include "rline/rldiag.h"
#include "atl/inlist.h"
#include "atl/hashstring.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, socialclub);

//PURPOSE
//  Buffer size limits.  All including the terminating null.
enum 
{
    RLSC_MAX_AVATAR_URL_CHARS       = 160,
    RLSC_MAX_COUNTRY_CODE_CHARS     = 3,
    RLSC_MAX_DATE_CHARS             = 32,
    RLSC_MAX_EMAIL_CHARS            = 101,
    RLSC_MAX_LANGUAGE_ABBREV_CHARS  = 10,
    RLSC_MAX_NICKNAME_CHARS         = 51,
    RLSC_MAX_PASSWORD_CHARS         = 513,
    RLSC_MAX_PHONE_CHARS            = 16,
    RLSC_MAX_SCAUTHTOKEN_CHARS      = 256,
    RLSC_MAX_ZIP_CODE_CHARS         = 10,
    RLSC_MAX_VALID_PASSWORD_SYMBOL_CHARS = 65,
	RLSC_MAX_AUDIENCE_CHARS			= 16,
	RLSC_MAX_USERDATA_CHARS			= 256,
	RLSC_MAX_MACHINETOKEN_CHARS		= 129,
	RLSC_MAX_LINKTOKEN_CHARS		= 512
};

//PURPOSE
//  Language the SC uses.  These must stay in sync with the backend.
//	Add new language codes at the end of this enum
enum rlScLanguage
{
    RLSC_LANGUAGE_UNKNOWN = -1,
    RLSC_LANGUAGE_CHINESE,
    RLSC_LANGUAGE_ENGLISH,
    RLSC_LANGUAGE_FRENCH,
    RLSC_LANGUAGE_GERMAN,
    RLSC_LANGUAGE_ITALIAN,
    RLSC_LANGUAGE_JAPANESE,
    RLSC_LANGUAGE_KOREAN,
    RLSC_LANGUAGE_POLISH,
    RLSC_LANGUAGE_PORTUGUESE_EUROPEAN,
    RLSC_LANGUAGE_PORTUGUESE_BRAZILIAN,
    RLSC_LANGUAGE_RUSSIAN,
    RLSC_LANGUAGE_SPANISH,
    RLSC_LANGUAGE_SPANISH_MEXICAN,
	RLSC_LANGUAGE_CHINESE_SIMPILIFIED,
    RLSC_NUM_LANGUAGES
};

//PURPOSE
//  Converts to and from rlScLanguage enum and string form.
const char* rlScLanguageToString(const rlScLanguage id);
rlScLanguage rlScLanguageFromString(const char* abbrev);

//PURPOSE
//  Error codes returned by rlSocialClub methods.
enum rlScErrorCode
{
    RLSC_ERROR_UNEXPECTED_RESULT = -1,              //Unhandled error code
    RLSC_ERROR_NONE = 0,                            //Success    
    RLSC_ERROR_PLATFORM_PRIVILEGE,                  //User lacks a platform-level privilege (like an XBL privilege)

    //Errors that follow the Code_CodeEx convention
    RLSC_ERROR_ALREADYEXISTS_EMAIL,                 //Account already exists for this email
    RLSC_ERROR_ALREADYEXISTS_EMAILORNICKNAME,       //(DEPRECATED AS OF v11; ONLY RETURNED TO LANOIRE) Account already exists for this email or nickname
    RLSC_ERROR_ALREADYEXISTS_NICKNAME,              //Account already exists for this nickname
    RLSC_ERROR_AUTHENTICATIONFAILED_PASSWORD,       //Password failed authentication
    RLSC_ERROR_AUTHENTICATIONFAILED_TICKET,         //Ticket failed authentication
	RLSC_ERROR_AUTHENTICATIONFAILED_LOGINATTEMPTS,  //Too many failed login attempts
	RLSC_ERROR_AUTHENTICATIONFAILED_MFA,			//This account is secured by 2-Step Verification. Please visit https://socialclub.rockstargames.com/settings/linkedaccounts to link this account.
    RLSC_ERROR_DOESNOTEXIST_ACHIEVEMENT,            //Unknown acheivement ID
    RLSC_ERROR_DOESNOTEXIST_PLAYERACCOUNT,          //No ROS shadow account (aka PlayerAccount)
    RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT,        //No SC account (aka RockstarAccount)
    RLSC_ERROR_INVALIDARGUMENT_AVATARURL,
    RLSC_ERROR_INVALIDARGUMENT_COUNTRYCODE,
    RLSC_ERROR_INVALIDARGUMENT_DOB,
    RLSC_ERROR_INVALIDARGUMENT_EMAIL,
    RLSC_ERROR_INVALIDARGUMENT_NICKNAME,
    RLSC_ERROR_INVALIDARGUMENT_PASSWORD,
    RLSC_ERROR_INVALIDARGUMENT_PHONE,
    RLSC_ERROR_INVALIDARGUMENT_ZIPCODE,
	RLSC_ERROR_INVALIDARGUMENT_ROCKSTARID,
    RLSC_ERROR_NOTALLOWED_MAXEMAILSREACHED,
	RLSC_ERROR_NOTALLOWED_PRIVACY,					//User's privacy setting prevented the action from being completed
    RLSC_ERROR_OUTOFRANGE_AGE,                      //Too young
    RLSC_ERROR_OUTOFRANGE_AGELOCALE,                //Too young for specific country or zip code
    RLSC_ERROR_OUTOFRANGE_NICKNAMELENGTH,
    RLSC_ERROR_OUTOFRANGE_PASSWORDLENGTH,
    RLSC_ERROR_PROFANE_NICKNAME,                    //Nickname contains a profane substring
    RLSC_ERROR_SYSTEMERROR_SENDMAIL,                //Failed to send email
	RLSC_ERROR_PROFANE_TEXT,
};

//PURPOSE
//  Structure that describes a Social Club account.
struct rlScAccountInfo
{
    rlScAccountInfo();

    void Clear();
    void SetAvatarUrl(const char* url);

    int m_Age;
    char m_AvatarUrl[RLSC_MAX_AVATAR_URL_CHARS];
    char m_CountryCode[RLSC_MAX_COUNTRY_CODE_CHARS];
    char m_Dob[RLSC_MAX_DATE_CHARS];
    char m_Email[RLSC_MAX_EMAIL_CHARS];
    bool m_IsApproxDob;
    char m_LanguageCode[RLSC_MAX_LANGUAGE_ABBREV_CHARS];
    char m_Nickname[RLSC_MAX_NICKNAME_CHARS];
    char m_Phone[RLSC_MAX_PHONE_CHARS];
    RockstarId m_RockstarId;
    char m_ZipCode[RLSC_MAX_ZIP_CODE_CHARS];
};

//PURPOSE
//  Describes the countries in which a SC account owner may reside.
//  This list is used during account creation.
class rlScCountries
{
public:
    struct Country
    {
        Country();
        ~Country();

        void Clear();

        bool Reset(const char* code, const char* name);

        char m_Code[3];			// 2 character country code
        const char* m_Name;		// UI-friendly name of the country

        inlist_node<Country> m_ListLink;
    };

    rlScCountries();
    ~rlScCountries();

    typedef inlist<Country, &Country::m_ListLink> CountryList;
    typedef CountryList::iterator iterator;
    typedef CountryList::const_iterator const_iterator;

    CountryList m_List;

private:
    //prevent copying
    rlScCountries(const rlScCountries&);
    rlScCountries& operator=(const rlScCountries&);
};

//PURPOSE
//  Describes the requirements for an SC account password
struct rlScPasswordReqs
{
    bool bValid;
    u32 MinLength;
    u32 MaxLength;
    // Null-terminated (if bValid is true) array of valid symbols
    // that can appear in a password
    char ValidSymbols[RLSC_MAX_VALID_PASSWORD_SYMBOL_CHARS];

    rlScPasswordReqs()
    {
        Clear();
    }

	void Clear()
	{
		MinLength = 0;
		MaxLength = 0;
		ValidSymbols[0] = '\0';
		bValid = false;
	}
};

struct rlCashPackUSDEInfo
{
	static const unsigned MAX_PACK_NAME_CHARS = 64;
	char m_PackName[MAX_PACK_NAME_CHARS];
	s64 m_Value;							//Virtual cash value
	float m_USDE;							//US Dollar value of the pack
};

//PURPOSE
//  Describes the requirements for Legal Territory Restrictions.
struct rlLegalTerritoryRestriction
{
	rlLegalTerritoryRestriction();
	void SetData(const char* restrictionId, const char* gameType, const bool evcAllowed, const bool pvcAllowed);

	void Clear();

	rlLegalTerritoryRestriction& operator=(const rlLegalTerritoryRestriction& that);

	inline bool operator==(const rlLegalTerritoryRestriction& that) const {return (m_RestrictionId == that.m_RestrictionId && m_GameType == that.m_GameType);}
	inline bool operator!=(const rlLegalTerritoryRestriction& that) const {return (m_RestrictionId != that.m_RestrictionId || m_GameType != that.m_GameType);}

	inline bool IsValid() const { return (m_RestrictionId.GetHash() != INVALID_RESTRICTION_ID && m_GameType.GetHash() != INVALID_GAMETYPE); }

	atHashString m_RestrictionId;
	atHashString m_GameType;
	bool m_EvcAllowed;
	bool m_PvcAllowed;

	OUTPUT_ONLY(mutable char m_strValue[256];);
	OUTPUT_ONLY(const char* c_str() const;);

	static const rlLegalTerritoryRestriction INVALID_RESTRICTION;
	static const u32 INVALID_RESTRICTION_ID = 2219964751;//ATSTRINGHASH("INVALID", 0x8451f94f)
	static const u32 INVALID_GAMETYPE = 2219964751;//ATSTRINGHASH("INVALID", 0x8451f94f)
};

struct rlScLicensePlateInfo
{
	static const unsigned MAX_PLATE_NAME_CHARS = 9;
	char m_plateText[MAX_PLATE_NAME_CHARS];

	static const unsigned MAX_PLATE_DATA_CHARS = 20;
	char m_plateData[MAX_PLATE_DATA_CHARS];
};

} //namespace rage

#endif  //RLINE_RLSOCIALCLUBCOMMON_H
