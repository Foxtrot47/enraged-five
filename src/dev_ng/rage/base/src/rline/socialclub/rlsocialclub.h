// 
// rline/rlsocialclub.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSOCIALCLUB_H
#define RLINE_RLSOCIALCLUB_H

#include "rlsocialclubcommon.h"
#include "rline/ros/rlroscommon.h"

namespace rage
{

class netStatus;

//PURPOSE
//  API for SC web services.
class rlSocialClub
{
public:
    //PURPOSE
    //  Cancels the task that the specified status object is monitoring.
    static void Cancel(netStatus* status);

#if RLROS_SC_PLATFORM
    //PURPOSE
    //  Clears a previously set login.  This immediately logs the user
    //  out (i.e. IsConnectedToSocialClub will return false), and prevents 
    //  login to until SetLogin is called again.
    static void ClearLogin(const int localGamerIndex);

    //PURPOSE
    //  Returns email scauth token set via SetLogin(), or null if none.
	static const char* GetLoginEmail(const int localGamerIndex);
    static const char* GetLoginScAuthToken(const int localGamerIndex);

    //PURPOSE
    //  Returns true if login has been set.
    static bool HasLogin(const int localGamerIndex);

    //PURPOSE
    //  Sets the SC login.  Once set, ROS will automatially try to login
    //  to the SC in the background, retrying as necessary according to 
    //  ROS TRCs.
    //
    //  It is expected that the game will obtain the user's login
    //  once through the UI, save it, and thereafter when the game starts
    //  up it will call SetLogin() and rline will automatically login
    //  the player as soon as possible.
    static bool SetLogin(const int localGamerIndex, const char* scEmail, const char* scAuthToken);

#else
    //PURPOSE
    //  Links an existing Social Club account to the user's platform account and 
    //  logs the user into the Social Club.
    //PARAMS
    //  email               (OPTIONAL) Email address that was used when creating the SC account.
    //  nickname            (OPTIONAL) User's Social Club nickname.
    //  password            User's Social Club password.
    //NOTES
    //	*either* the email *or* nickname can be NULL, but not both.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Expired or invalid ticket.
    //  RLSC_ERROR_AUTHENTICATIONFAILED_PASSWORD: Incorrect password.
    //  RLSC_ERROR_DOESNOTEXIST_PLAYERACCOUNT: Could not find account.
    //  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Could not find account.
    //  RLSC_ERROR_INVALID_ARGUMENT_{ARGUMENT}: Malformed or generically invalid argument.
    //  RLSC_ERROR_PLATFORM_PRIVILEGE: User lacks a required platform-level privilege
    static bool LinkAccount(const int localGamerIndex,
                            const char* email,
                            const char* nickname,		
                            const char* password,
                            netStatus* status);

    //PURPOSE
    //  Unlinks the local gamer's platform account from a Rockstar account.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    //  RLSC_ERROR_INVALID_ARGUMENT_{ARGUMENT}: Malformed or generically invalid argument.
    //  RLSC_ERROR_REDUNDANT: Account is not linked to a RockstarId.
    static bool UnlinkAccount(const int localGamerIndex,
                              netStatus* status);
#endif

    //PURPOSE
    //  Accepts a version of a legal policy.
    //NETSTATUS_RESULT_CODES
    //  TODO
    static bool AcceptLegalPolicy(const int localGamerIndex, 
                                  const char* policyTag, 
                                  const s16 version,
                                  netStatus* status);

    //PURPOSE
    //  Returns true if the local gamer is allowed to link to an SC account.
    //  On some consoles, the local gamer's settings/privileges may prohibit them
    //  from linking to other social networks, including R* Social Club.
    //  Game code could call this to prohibit going down a UI path, rather than
    //  waiting until the call to LinkAccount/CreateAccount to discover the
    //  gamer doesn't have a necessary privilege.
    //RETURNS
    //  True if the gamer is allowed to create or link accounts.
    static bool CanLinkAccounts(const int localGamerIndex);

    //PURPOSE
    //  Checks for the validity & availability of an email address.
    //  Note that validity checking is limited to making sure it looks like an
    //  email address; we cannot verify it is an actual address.
    //PARAMS
    //  email       - Email address to check
    static bool CheckEmail(const int localGamerIndex,
                           const char* email,
                           netStatus* status);

    //PURPOSE
    //  Checks for the validity & availability of a Social Club nickname.
    //PARAMS
    //  nickname    Nickname to check.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_ALREADYEXISTS_NICKNAME: Nickname has been taken.
    //  InvalidArgument.nickname: Nickname contains illegal characters, or is malformed.
    //  RLSC_ERROR_OUTOFRANGE_NICKNAMELENGTH: Nickname is too short or long.
    //  RLSC_ERROR_PROFANE_NICKNAME: Nickname contains profanity.
    static bool CheckNickname(const int localGamerIndex,
                              const char* nickname,
                              netStatus* status);

    //PURPOSE
    //  Checks for the validity of a Social Club password.
    //PARAMS
    //  password        Password to check.
    //  passwordReqs    Pointer to a rlScPasswordReqs struct that will receive the password reqs
    //                  if the password is malformed. Must remain valid for duration of task.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_INVALIDARGUMENT_PASSWORD: Password contains illegal characters, or is malformed.
    //  RLSC_ERROR_OUTOFRANGE_PASSWORDLENGTH: Password is too short or too long.
    static bool CheckPassword(const int localGamerIndex,
                              const char* password,
                              rlScPasswordReqs* passwordReqs,
                              netStatus* status);

	//PURPOSE
	//  Gets the current SCS password requirements
	//PARAMS
	//  passwordReqs    Pointer to a rlScPasswordReqs struct that will receive the password reqs.
	static bool GetPasswordRequirements(const int localGamerIndex, rlScPasswordReqs* passwordReqs, netStatus* status);

    //PURPOSE
    //  Checks to see if text is profane or contains reserved words.
    //PARAMS
    //  text            Text to check
    //  language        Language the text is in
	//  profaneWord     A pointer to an allocated buffer of minimum size MAX_PROFANE_WORD_LENGTH
    static bool CheckText(const int localGamer,
                          const char* text, 
						  const rlScLanguage language,
						  char* profaneWord,
						  netStatus* status);

    //PURPOSE
    //  Creates a new SC account.
    //  On non-SC platforms, this will also link it to the user's platform account.
    //PARAMS
    //  acceptNewsletter    True if account accepts receiving R* emails.
    //  dob                 Date of birth of account creator.
    //  avatarUrl           (OPTIONAL) URL to avatar image.
    //  countryCode         (OPTIONAL) Country code retrieved from calling GetCountries().
    //  email               Email address used to identify account.
    //  languageCode        (OPTIONAL) 2-letter language code.
    //  nickname            (OPTIONAL) Account nickname.
    //  password            Account password.
    //  phone               (OPTIONAL) User's phone number.
    //  zipCode             (OPTIONAL) User's zip code.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_ALREADYEXISTS_EMAIL: Account already exists for this email.
    //  RLSC_ERROR_ALREADYEXISTS_EMAILORNICKNAME: (DEPRECATED AS OF v11; ONLY RETURNED TO LANOIRE) Account already exists for this email or nickname.
    //  RLSC_ERROR_ALREADYEXISTS_NICKNAME: Account already exists for this nickname.
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Expired or invalid ticket.
    //  RLSC_ERROR_DOESNOTEXIST_PLAYERACCOUNT: Could not find player account.
    //  RLSC_ERROR_INVALID_ARGUMENT_{ARGUMENT}: Malformed or generically invalid argument.
    //  RLSC_ERROR_OUTOFRANGE_AGE: Account is too young.
    //  RLSC_ERROR_OUTOFRANGE_NICKNAMELENGTH: Nickname is too short or long.
    //  RLSC_ERROR_OUTOFRANGE_PASSWORDLENGTH: Password is too short or long.
    //  RLSC_ERROR_PLATFORM_PRIVILEGE: User lacks a required platform-level privilege
    //  RLSC_ERROR_PROFANE_NICKNAME: Nickname contains profanity.
    //  It is otherwise the same.
    static bool CreateAccount(const int localGamerIndex,
                              const bool acceptNewsletter,
                              const char* avatarUrl,
                              const char* countryCode,
                              const char* dob,
                              const char* email,
                              const bool isApproxDob,
                              const char* languageCode,
                              const char* nickname,		
                              const char* password,
                              const char* phone,
                              const char* zipCode,
                              netStatus* status);

    //PURPOSE
    //  Returns the highest version accepted of a policy.
    //NETSTATUS_RESULT_CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    //  RLSC_ERROR_INVALID_ARGUMENT_{ARGUMENT}: Malformed or generically invalid argument.
    static bool GetAcceptedLegalPolicyVersion(const int localGamerIndex, 
                                              const char* policyTag, 
                                              s16* version,
                                              netStatus* status);

    //PURPOSE
    //  Retrieves the SC account info for the local gamer.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    //  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Could not find account.
    static bool GetAccountInfo(const int localGamerIndex,
                               rlScAccountInfo* acct,
                               netStatus* status);

    //PURPOSE
    //  Checks if given nickname is valid.  If not, it returns valid alternates based upon
    //  that nickname.  Note that you can pass in a NULL nickname to just create some alternates.
    //PARAMS
    //  nickname        - Nickname to check, or NULL if you just want to generate some alternates
    //  alternates      - Array in which to store alternate nicknames
    //  maxAlternates   - Maximum number of alternates to generate
    //  numAlternates   - Number of alternates returned, or 0 if the nickname was valid and available
    static bool GetAlternateNicknames(const int localGamerIndex,
                                      const char* nickname,
                                      char (*alternates)[RLSC_MAX_NICKNAME_CHARS],
                                      const unsigned maxAlternates,
                                      unsigned* numAlternates,
                                      netStatus* status);

    //PURPOSE
    //  Retrieves the list of countries that can be passed to CreateAccount.
    //PARAMS
    //  countries           Returns the list of countries, must remain valid until the operation completes.
    static bool GetCountries(const int localGamerIndex,
                             rlScCountries &countries,
                             netStatus* status);


	//PURPOSE
	//  Creates an SC auth token for the local gamer.
	//PARAMS
	//  localGamerIndex     - Gamer to get token for. If they are not linked,
	//                        this is guaranteed to fail.
	//  buf                 - Buffer to store token in.
	//  bufSize             - Size of buffer. Must be at least RLSC_MAX_SCAUTHTOKEN_CHARS.
	//NETSTATUS RESULT CODES
	//  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
	//  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Gamer is not linked, or account not found.
	static bool GetScAuthToken(const int localGamerIndex,
								char* buf,
								const unsigned bufSize,
								netStatus* status);

	//PURPOSE
	//  Creates an SC auth token for the local gamer with the specified audience, TTL and user data.
	//PARAMS
	//  localGamerIndex     - Gamer to get token for. If they are not linked, this is guaranteed to fail.
	//	scTicket			- Social club ticket
	//  authTokenBuf        - Buffer to store token in.
	//  authTokenBufSize    - Size of buffer. Must be at least RLSC_MAX_SCAUTHTOKEN_CHARS.
	//	audience			- The audience for the ScAuthToken (i.e. "Game")
	//	ttlMinutes			- The lifetime of the ticket in minutes. (i.e. 30 days)
	//	userData			- User data to associate with the ticket. Can be NULL.
	//NETSTATUS RESULT CODES
	//  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
	//  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Gamer is not linked, or account not found.
	static bool CreateScAuthToken(const int localGamerIndex,
									const char* scTicket,
									char* authTokenBuf,
									const unsigned authTokenBufSize,
									const char* audience,
									const int ttlMinutes,
									const char* userData,
									netStatus* status);

	//PURPOSE
	//  Creates an SC auth token for the local gamer with the specified audience, TTL and user data.
	//PARAMS
	//  localGamerIndex     - Gamer to get token for. If they are not linked, this is guaranteed to fail.
	//	scTicket			- Social club ticket
	//  authTokenBuf		- Buffer to store token in.
	//  authTokenBufSize    - Size of buffer. Must be at least RLSC_MAX_SCAUTHTOKEN_CHARS.
	//  machineTokenBuf     - Buffer to store device token in.
	//  machineTokenBufSize - Size of machine buffer. Must be at least RLSC_MAX_MACHINETOKEN_CHARS.
	//	saveMachine			- Indicates to the server if this machine should be stored for subsequent logins
	//	keepMeSignedIn		- Should be set to true when the user is using automatic signin
	//	fingerprint			- Identifies the machine
	//	audience			- The audience for the ScAuthToken (i.e. "Game")
	//	ttlMinutes			- The lifetime of the ticket in minutes. (i.e. 30 days)
	//	userData			- User data to associate with the ticket. Can be NULL.
	//NETSTATUS RESULT CODES
	//  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
	//  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Gamer is not linked, or account not found.
	static bool CreateScAuthToken2(const int localGamerIndex,
									const char* scTicket,
									char* authTokenBuf,
									const unsigned authTokenBufSize,
									char* machineTokenBuf,
									const unsigned machineTokenBufSize,
									const bool saveMachine,
									const bool keepMeSignedIn,
									const char* fingerprint,
									const char* audience,
									const int ttlMinutes,
									const char* userData,
									netStatus* status);

	//PURPOSE
	//	Creates a link token for the local gamer.
	//PARAMS
	//  localGamerIndex			- Gamer to get token for. If they are not linked, this is guaranteed to fail.
	//  buf						- Buffer to store token in.
	//  bufSize					- Size of buffer. Must be at least RLSC_MAX_LINKTOKEN_CHARS.
	static bool CreateLinkToken(const int localGamerIndex,
								char* buf,
								const unsigned bufSize,
								netStatus* status);

    //PURPOSE
    //  Returns true if local gamer is currently connected (logged in) to SC.
    static bool IsConnectedToSocialClub(const int localGamerIndex);

    //PURPOSE
    //  Sends an email containing a link to reset the account password.
    //PARAMS
    //  email               Email address for SC account to retrieve.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Could not find account.
    //  RLSC_ERROR_NOTALLOWED_MAXEMAILSREACHED: Max number of reminder emails have been sent.
    //  RLSC_ERROR_SYSTEMERROR_SENDEMAIL: Failed to send validation email.
    static bool RequestResetPassword(const char* email,
                                     netStatus* status);

    //PURPOSE
    //  Notifies SC of an activity in game which translates to posts on the user's social club feed
	//  including possibly his friends or his crew, depending on the type of the activity.
	//PARAMS
    //  activityType        Type of the activity, which will determine how the data is interpreted and
	//                      what actions are taken. E.g "SHARE_SPOTIFY_SONG", "CREW_SET_MOTD", etc.
	//                      Unsupported action types will return an error InvalidArgument or NotImplemented
    //  activityTarget      The target of the activity. E.g. CREW_JOINED, the target will be the id of the crew
	//                      Leave at 0 if the activity doesn't contain a specific target or the target is also
	//                      the user who is posting the activity.
	//  activityData        Generic string data to be used as part of the activity. For SHARE_SPOTIFY_SONG, the
	//                      activityData is simply the id of the song being "liked". Other uses of this is TBD
	//  extraParams         Generic set of parameters in the form of key=value&key2=value2 used to add more
	//                      parameters to the activityData
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    //  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Could not find account.
    //  RLSC_ERROR_INVALID_ARGUMENT_{ARGUMENT}: Malformed or generically invalid argument.
	//  RLSC_ERROR_NOTALLOWED_PRIVACY: User has his social club privacy setting set to prevent game from posting to his feed
	static bool PostUserFeedActivity(const int localGamerIndex,
									 const char* activityType,
									 const s64 activityTarget,
									 const char* activityData,
									 const char* extraParams);

    //PURPOSE
    //  Updates one or more optional fields on the linked Rockstar account.
    //  avatarUrl           (OPTIONAL) URL to avatar image.
    //  countryCode         (OPTIONAL) Country code retrieved from calling GetCountries().
    //  dob                 (OPTIONAL) Date of birth, expressed as "MM/DD/YYYY".
    //  isApproxDob         (OPTIONAL) "true' if dob is approximate (i.e. only YYYY is known)
    //  languageCode        (OPTIONAL) 2-letter language code.
    //  phone               (OPTIONAL) User's phone number.
    //  zipCode             (OPTIONAL) User's zip code.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    //  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Could not find account.
    //  RLSC_ERROR_INVALID_ARGUMENT_{ARGUMENT}: Malformed or generically invalid argument.
    //  RLSC_ERROR_OUTOFRANGE_AGE: Account is too young.
    //  RLSC_ERROR_OUTOFRANGE_AGELOCALE: Account is too young for the country and/or zip code.
    static bool UpdateAccount(const int localGamerIndex,
                              const char* avatarUrl,
                              const char* countryCode,
                              const char* dob,
                              const char* email,
                              const char* isApproxDob,
                              const char* languageCode,
                              const char* nickname,
                              const char* phone,
                              const char* zipCode,
                              netStatus* status);

    //PURPOSE
    //  Sets whether the linked Rockstar account accepts newsletters.
    //PARAMS
    //  acceptNewsletter    True if account accepts receiving R* emails.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    //  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Could not find account.
    static bool UpdateAcceptNewsletter(const int localGamerIndex,
                                       const bool acceptNewsletter,
                                       netStatus* status);
    
    //PURPOSE
    //  Updates password on the linked Rockstar account.
    //  password            Current password.
    //  newPassword         New password.
    //NETSTATUS RESULT CODES
    //  RLSC_ERROR_AUTHENTICATIONFAILED_TICKET: Ticket was expired or invalid.
    //  RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT: Could not find account.
    //  RLSC_ERROR_INVALID_ARGUMENT_{ARGUMENT}: Malformed or generically invalid argument.
    static bool UpdatePassword(const int localGamerIndex,
                               const char* password,
                               const char* newPassword,
                               netStatus* status);
	//PURPOSE
	//  Sends a complaint document to ROS/SC.
	//  complaintDoc        Document in json format.
	static bool RegisterComplaint(const int localGamerIndex,
								  const char* complaintDoc,
								  netStatus* status);

	//PURPOSE
	//	Get the Virtual cash pack infos so we can know the US dollar equivalent for each cash pack.
	//PARAMS
	//	packInfos - output array of rlCashPackUSDEInfo to fill with the response
	//	maxCount - the length of the array packInfos
	//	count - actual number of pack infos returned
	static bool GetCashTransactionsPackUSDEInfo(const int localGamerIndex, 
												rlCashPackUSDEInfo* packInfos, 
												unsigned maxCount, 
												unsigned* count, 
												netStatus* status );
	
	/// <summary>
	/// Gets the legal territory restrictions.
	/// </summary>
	/// <param name="localGamerIndex">Index of the local gamer.</param>
	/// <param name="restrictions">The restrictions.</param>
	/// <param name="maxCount">The maximum count.</param>
	/// <param name="numReturned">The number returned.</param>
	/// <param name="stattus">The stattus.</param>
	/// <returns></returns>
	static bool GetLegalTerritoryRestrictions(const int localGamerIndex,
											  rlLegalTerritoryRestriction* restrictions, 
											  const unsigned maxCount, 
											  unsigned* numReturned, 
											  netStatus* status);


	//PURPOSE
	//	Get the license plate infos so we can compare them to the current stat/cloud pulled license plate to ensure it is valid.
	//PARAMS
	//	plateInfos - output array of rlScLicensePlateInfo to fill with the response
	//	maxCount - the length of the array packInfos
	//	count - actual number of pack infos returned
	static bool GetLicensePlateInfo( const int localGamerIndex, 
									 rlScLicensePlateInfo* plateInfos, 
									 unsigned maxCount, 
									 unsigned* count, 
									 netStatus* status );

	//PURPOSE
	//	Add the license plate.
	//PARAMS
	//	inText - the text for the new license plate
	static bool AddLicensePlate( const int localGamerIndex, 
								 const char* inText, 
								 const char* inData, 
								 netStatus* status );

	//PURPOSE
	//	Is the given license plate text valid.
	//PARAMS
	//	inText - the text for the new license plate
	static bool IsValidLicensePlate( const int localGamerIndex, 
		const char* inText, 
		bool* pbIsValid,
		bool* pbIsProfane,
		bool* pbIsReserved,
		bool* pbIsMalformed,
		netStatus* status );

	//PURPOSE
	//	Change plate data.
	//PARAMS
	//	OldPlateText - old plate text.
	//	newPlateText - new plate text.
	//	plateData - the new plate data.
	static bool ChangePlateDataNoInsert(const int localGamerIndex
										,rlScLicensePlateInfo* plateInfos
										,unsigned maxCount
										,unsigned* count 
										,const char* oldPlateText
										,const char* newPlateText
										,const char* plateData
										,netStatus* status);

private:
    //PURPOSE
    //  Don't allow creating instances.  Users only call static methods.
    rlSocialClub();

#if RLROS_SC_PLATFORM
    friend struct rlRosPlatformCredentials;

	static char m_Email[RL_MAX_LOCAL_GAMERS][RLSC_MAX_EMAIL_CHARS];
	static char m_ScAuthToken[RL_MAX_LOCAL_GAMERS][RLSC_MAX_SCAUTHTOKEN_CHARS];
#endif
};

} //namespace rage

#endif  //RLINE_RLSOCIALCLUB_H
