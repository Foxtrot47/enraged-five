// 
// rline/rlprivileges.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPRIVILEGES_H
#define RLINE_RLPRIVILEGES_H

#include "net/task.h"
#include "rline/rl.h"

#if RSG_DURANGO
#include "durango/rlxbl_interface.h"
#endif  //RSG_DURANGO

namespace rage
{

//////////////////////////////////////////////////////////////////////////
//  rlPrivileges
//////////////////////////////////////////////////////////////////////////
class rlPrivileges
{
public:
	enum PrivilegeTypes
	{
		PRIVILEGE_INVALID = -1,
#if RSG_DURANGO
		// Keep this synchronized with:
		//  1)  rlprivileges.cpp: const int privilegeBits[rlPrivileges::PRIVILEGE_MAX]
		//	2)	rlxblprivileges.cpp: const int privilegeCodes[rlPrivileges::PRIVILEGE_MAX]
		//  3)  commands_network.sch: PRIVILEGE_TYPE_DURANGO_X bit flags
		PRIVILEGE_ADD_FRIEND = 0,
		PRIVILEGE_CLOUD_GAMING_JOIN_SESSION,
		PRIVILEGE_CLOUD_GAMING_MANAGE_SESSION,
		PRIVILEGE_CLOUD_SAVED_GAMES,
		PRIVILEGE_COMMUNICATIONS,
		PRIVILEGE_COMMUNICATION_VOICE_INGAME,
		PRIVILEGE_COMMUNICATION_VOICE_SKYPE,
		PRIVILEGE_GAME_DVR,
		PRIVILEGE_MULTIPLAYER_PARTIES,
		PRIVILEGE_MULTIPLAYER_SESSIONS,
		PRIVILEGE_PREMIUM_CONTENT,
		PRIVILEGE_PREMIUM_VIDEO,
		PRIVILEGE_PROFILE_VIEWING,
		PRIVILEGE_DOWNLOAD_FREE_CONTENT,
		PRIVILEGE_PURCHASE_CONTENT,
		PRIVILEGE_SHARE_KINECT_CONTENT,
		PRIVILEGE_SOCIAL_NETWORK_SHARING,
		PRIVILEGE_SUBSCRIPTION_CONTENT,
		PRIVILEGE_USER_CREATED_CONTENT,
		PRIVILEGE_VIDEO_COMMUNICATIONS,
		PRIVILEGE_VIEW_FRIENDS_LIST,
#else
		PRIVILEGE_NONE = 0,
#endif //RSG_DURANGO
		PRIVILEGE_MAX
	};

public:

#if RSG_DURANGO
	class PrivilegeWorker
	{
	public:
		PrivilegeWorker();

		// Updates the privileges for a given gamer index
		// Returns TRUE if the update resulted in work.
		// Returns FALSE if the worker was unable to update the index.
		bool Update(const int actingUserIndex);

		bool IsPending() const { return (m_Pending>-1);}
		bool   Refresh() const { return m_Refresh;}

	public:
		int m_Pending;
		bool m_Refresh;
		IPrivileges::ePrivilegeCheckResult m_Value[PRIVILEGE_MAX];
		bool m_RefreshValue[PRIVILEGE_MAX];
	};
#endif // RSG_DURANGO

public:
	static bool Init();

	static void Shutdown();

	static bool IsInitialized();

	//PURPOSE
	// Polls platform-specific API for events.
	//NOTES
	// Should be called at least once every 100 ms.
	static void Update();

	//PURPOSE
	// Get the bit flag associated with a privilege type.
	static int GetPrivilegeBit(PrivilegeTypes privilegeType);

	//PURPOSE
	// Begin a check through the platform API to see if the specified local user has the specified privileges.
	static bool CheckPrivileges(const int localGamerIndex, const int privilegeTypeBitfield, const bool attemptResolution);

	//PURPOSE
	// Check if the current privilege check has a result.
	static bool IsPrivilegeCheckResultReady();

	//PURPOSE
	// Check if a privilege check is in progress.
	static bool IsPrivilegeCheckInProgress();

	//PURPOSE
	// Check if the current privilege check was successful.
	static bool IsPrivilegeCheckSuccessful();

	//PURPOSE
	// Set that the privilege check result is no longer needed. 
	static void SetPrivilegeCheckResultNotNeeded();

	//PURPOSE
	// Handle a user signing out
	static void OnSignOut(const int localGamerIndex);

#if RSG_DURANGO

	//PURPOSE
	// Refresh all privileges.
	static void RefreshPrivileges(const int localGamerIndex);

	//PURPOSE
	// Checks if a privilege update request is pending
	static bool IsRefreshPrivilegesPending(const int localGamerIndex);

	//PURPOSE
	// Check if a certain privilege has been granted.
	static bool HasPrivilege(const int localGamerIndex, const rlPrivileges::PrivilegeTypes privilegeId, const IPrivileges::ePrivilegeCheckResult privtype = IPrivileges::PRIV_RESULT_NOISSUE);

	//PURPOSE
	// Updates a privilege value
	static void UpdatePrivilege(const int localGamerIndex, const rlPrivileges::PrivilegeTypes privilegeId, const IPrivileges::ePrivilegeCheckResult privtype = IPrivileges::PRIV_RESULT_NOISSUE);

#endif // RSG_DURANGO

#if !__NO_OUTPUT
	//PURPOSE
	// Return the privilege name.
	static const char* GetPrivilegeName(const int id);
#endif // !__NO_OUTPUT

private:
	static bool NativeInit();
	static void NativeShutdown();
	static void NativeUpdate();

	static bool m_Initialized;

#if RSG_DURANGO
	static PrivilegeWorker      sm_Privileges[RL_MAX_LOCAL_GAMERS];			
#endif // RSG_DURANGO
};

}   //namespace rage

#endif  //RLINE_RLPRIVILEGES_H
