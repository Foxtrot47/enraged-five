// 
// rline/rlnpbandwidth.h
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 
#ifndef RLINE_RLNPBANDWIDTH_H
#define RLINE_RLNPBANDWIDTH_H

#define RL_NP_BANDWIDTH RSG_NP

#if RL_NP_BANDWIDTH
#define RL_NP_BANDWIDTH_ONLY(...) __VA_ARGS__
#else
#define RL_NP_BANDWIDTH_ONLY(...)
#endif

#if RL_NP_BANDWIDTH

#include "net/bandwidth.h"
#include "net/status.h"
#include "rline/rldiag.h"
#include "rline/rlnpwebapi.h"

#include <np.h>

namespace rage
{

class bkBank;

enum class rlNpBandwidthState
{
    None,
    Testing,
    ReadingResult,
    Failed
};

// gen8 has no nettask2 so we use a work item
class rlNpShutdownBandwidthWorkItem : public sysThreadPool::WorkItem
{
public:
	rlNpShutdownBandwidthWorkItem();
	void Configure(netBandwidthTestResult* result, int* contextPtr);
	void DoWork();
	bool Succeeded() const { return m_Success; }

private:
	netBandwidthTestResult* m_PendingResult;
	int* m_Ctx;
	bool m_Success;
};

class rlNpBandwidth
{
public:
    rlNpBandwidth();
    ~rlNpBandwidth();

    //PURPOSE
    // Init the sce-sys module
    static void InitLibs();

    //PURPOSE
    //  Init/Shutdown/Update calls
    bool Init();
    void Shutdown();
    void Update();

    //PURPOSE
    //  True if we have data from a previous test
    bool HasValidResult() const;

    //PURPOSE
    //  True while a test is in progress
    bool IsTestActive() const;

    //PURPOSE
    //  The test result data
    const netBandwidthTestResult& GetTestResult() const;
    void ClearTestResult();

    //PURPOSE
    //  Runs a bandwidth test. The netStatus is optional.
    //  Only one test at a time is allowed.
    bool TestBandwidth(netStatus* status);
    void AbortBandwidthTest();

    BANK_ONLY(void AddWidgets(bkBank *pBank));

protected:
    void InitParam(SceNpBandwidthTestInitParam& param);
    bool Start();
    void CheckStatus(rlNpBandwidthState state);
    void CheckResultStatus(rlNpBandwidthState state);
    bool RetrieveResult(rlNpBandwidthState state);
    void SetFailed();
    void SetSucceeded();

    // To abort and shutdown on the current thread
    void AbortBandwidthTestBlocking();

    // To shutdown a failed check on a worker thread
    void ShutdownBandwidthTest();

#if RSG_BANK
    void BANK_TestBandwidth();
#endif

protected:
    static const unsigned DEFAULT_TEST_TIMEOUT_SEC;

    rlNpBandwidthState m_State;
    netBandwidthTestResult m_Result; // The stored result data
    netBandwidthTestResult m_PendingResult; // Temp data while retrieving the result
    netStatus* m_ExternalNetStatus;
	rlNpShutdownBandwidthWorkItem m_ShutdownWorkItem;
    int m_TestCtx;
};

} //namespace rage

#endif //RL_NP_BANDWIDTH

#endif //RLINE_RLNPBANDWIDTH_H
