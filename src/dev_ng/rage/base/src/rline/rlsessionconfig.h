// 
// rline/rlsessionconfig.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSESSIONCONFIG
#define RLINE_RLSESSIONCONFIG

#include "rl.h"
#include "rlmatchingattributes.h"

namespace rage
{

enum rlSessionCreateFlags
{
    // disables platform presence on a session (cannot be joined via platform invite or JvP)
	// used in models where we have a presence session and one or more matchmaking / game sessions
    RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED				= BIT0,

    // creates the session with invites disabled - can be enabled afterwards
    RL_SESSION_CREATE_FLAG_INVITES_DISABLED					= BIT1,

	// creates the session with join via presence disabled - can be enabled afterwards
	RL_SESSION_CREATE_FLAG_JOIN_VIA_PRESENCE_DISABLED		= BIT2,

	// ---- GDK Only ---- //
	// creates the session with joinability for friends 
	RL_SESSION_CREATE_FLAG_JOIN_VIA_PRESENCE_FRIENDS_ONLY	= BIT3,

	// creates the session with join-in-progress disabled.
    RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED		= BIT4,

	// session will not be advertised to matchmaking services
	// used for presence sessions
	RL_SESSION_CREATE_FLAG_MATCHMAKING_DISABLED				= BIT5,

	// ---- XDK Only ---- //
	// session will not have an associated platform party session
	RL_SESSION_CREATE_FLAG_PARTY_SESSION_DISABLED			= BIT6,

	// ---- Xbox Only ---- //
	// display names are not required when adding players to the session
	RL_SESSION_CREATE_FLAG_DISPLAY_NAMES_NOT_REQUIRED		= BIT7,

	// ---- PS5 Only ---- //
	// creates a UDS match session for statistic gathering
	RL_SESSION_CREATE_UDS_MATCH_ENABLED						= BIT8,

	// number of flags required for syncing
	RL_SESSION_CREATE_FLAG_NUM_FLAGS						= 9,

	// ---- Local Mask ---- //
	RL_SESSION_CREATE_FLAG_LOCAL_MASK						= (0xFFFFFFFF << RL_SESSION_CREATE_FLAG_NUM_FLAGS),

	// ---- Local Only ---- //
	// initially not advertised to matchmaking, the requesting code can 
	RL_SESSION_CREATE_FLAG_MATCHMAKING_ADVERTISE_DISABLED	= (1 << RL_SESSION_CREATE_FLAG_NUM_FLAGS),
};

enum rlSessionPresenceFlags
{
    RL_SESSION_PRESENCE_FLAG_INVITABLE                  = BIT0,
	RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE          = BIT1,

    RL_SESSION_PRESENCE_FLAG_NUM_FLAGS  = 2
};

//PURPOSE
//  Session configuration parameters.
//  These are used to configure a guest instance of a session once
//  a connection to the host has been established and the host has
//  transmitted its config parameters to the guest.
class rlSessionConfig
{
public:
    rlSessionConfig();

    void Clear();

    void Reset(const rlNetworkMode netMode,
               const unsigned numPublicSlots,
               const unsigned numPrivateSlots,
               const rlMatchingAttributes& attrs,
               const unsigned createFlags,
               const unsigned presenceFlags,
               const u64 sessionId,
               const u64 arbCookie);

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

	// PURPOSE
	//	Returns TRUE if the config is presence enabled
	bool IsPresenceEnabled() const;

	// PURPOSE
	//	Returns TRUE if the config is matchmaking enabled
	bool IsMatchmakingEnabled() const;

	// PURPOSE
	//	Returns TRUE if the config enables matchmaking advertising
	bool IsMatchmakingAdvertisingEnabled() const;

    rlNetworkMode m_NetworkMode;
    unsigned    m_MaxPublicSlots;
    unsigned    m_MaxPrivateSlots;
    rlMatchingAttributes m_Attrs;
    unsigned    m_CreateFlags;      //From CreateFlags enum
    unsigned    m_PresenceFlags;    //From PresenceFlags enum
    u64         m_SessionId;        //Globally unique session id.
    u64         m_ArbCookie;        //Arbitration cookie
};

}   //namespace rage

#endif  //RLINE_RLSESSIONCONFIG
