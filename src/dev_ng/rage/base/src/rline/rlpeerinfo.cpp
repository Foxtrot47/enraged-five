// 
// rline/rlpeerinfo.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlpeerinfo.h"
#include "rl.h"
#include "rldiag.h"
#include "data/bitbuffer.h"
#include "math/random.h"
#include "net/netaddress.h"
#include "string/stringhash.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/timer.h"

#include <time.h>

namespace rage
{

static sysCriticalSectionToken s_GlpidCs;
static u64 s_localPeerId = RL_INVALID_PEER_ID;

bool
rlPeerInfo::GetLocalPeerId(u64 *id)
{
    SYS_CS_SYNC(s_GlpidCs);

    bool success = false;

    if(s_localPeerId == RL_INVALID_PEER_ID)
    {
        success = rlCreateUUID(&s_localPeerId);
    }
    else
    {
        success = true;
    }

    *id = s_localPeerId;

    return success;
}

bool
rlPeerInfo::GetLocalPeerInfo(rlPeerInfo* info, const int localGamerIndex)
{
    //rlDebug2("rlPeerInfo::GetLocalPeerInfo(): Retrieving local peer info...");

    info->Clear();

	rlPeerAddress::GetLocalPeerAddress(localGamerIndex, &info->m_PeerAddr);
    return rlPeerInfo::GetLocalPeerId(&info->m_PeerId);
}

rlPeerInfo::rlPeerInfo()
{
    this->Clear();
}

void
rlPeerInfo::Clear()
{
    m_PeerAddr.Clear();
    m_PeerId = RL_INVALID_PEER_ID;
}

bool rlPeerInfo::Init(const u64 peerId, const rlPeerAddress& peerAddr)
{
	m_PeerId = peerId;
	m_PeerAddr = peerAddr;
	return IsValid();
}

bool
rlPeerInfo::IsValid() const
{
    return this->GetPeerId() != RL_INVALID_PEER_ID;
}

bool
rlPeerInfo::HasValidPeerAddress() const
{
    return m_PeerAddr.IsValid();
}

u64
rlPeerInfo::GetPeerId() const
{
    return m_PeerId;
}

const rlPeerAddress&
rlPeerInfo::GetPeerAddress() const
{
    rlAssert(m_PeerAddr.IsValid());

    return m_PeerAddr;
}

bool
rlPeerInfo::IsLocal() const
{
    u64 local_id;

    if(rlPeerInfo::GetLocalPeerId(&local_id))
    {
        return RL_INVALID_PEER_ID != local_id && (this->GetPeerId() == local_id);
    }
    else
    {
        rlError("rlPeerInfo::IsLocal(): Failed to get local peer ID for comparison");
    }

    return false;
}

bool
rlPeerInfo::IsRemote() const
{
    return this->IsValid() && !this->IsLocal();
}

bool
rlPeerInfo::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    rlAssert(m_PeerAddr.IsValid());

    bool success = false;

    unsigned sizeofAddr;

    if(m_PeerAddr.Export(buf, sizeofBuf, &sizeofAddr)
        && sizeofBuf >= sizeofAddr + sizeof(m_PeerId))
    {
        datBitBuffer::WriteUnsigned(buf, u32(m_PeerId >> 32), 32, sizeofAddr << 3);
        datBitBuffer::WriteUnsigned(buf, u32(m_PeerId), 32, (sizeofAddr << 3) + 32);
        success = true;
    }

    if(size){*size = success ? sizeofAddr + sizeof(m_PeerId) : 0;}
    return success;
}

bool
rlPeerInfo::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    bool success = false;

    unsigned sizeofAddr;

    if(m_PeerAddr.Import(buf, sizeofBuf, &sizeofAddr)
        && sizeofBuf >= sizeofAddr + sizeof(m_PeerId))
    {
        u32 a, b;
        datBitBuffer::ReadUnsigned(buf, a, 32, sizeofAddr << 3);
        datBitBuffer::ReadUnsigned(buf, b, 32, (sizeofAddr << 3) + 32);
        m_PeerId = (u64(a) << 32) | b;
        success = true;
    }

    if(size){*size = success ? sizeofAddr + sizeof(m_PeerId) : 0;}
    return success;
}

bool
rlPeerInfo::operator==(const rlPeerInfo& that) const
{
    return this->GetPeerId() == that.GetPeerId();
}

bool
rlPeerInfo::operator!=(const rlPeerInfo& that) const
{
    return !this->operator==(that);
}

}   //namespace rage
