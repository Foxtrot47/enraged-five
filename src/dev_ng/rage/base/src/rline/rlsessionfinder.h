// 
// rline/rlsessionfinder.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
 
#ifndef RLINE_RLSESSIONFINDER_H
#define RLINE_RLSESSIONFINDER_H

#include "data/bitbuffer.h"
#include "net/message.h"
#include "rline/rlmatchingattributes.h"
#include "rline/rlsessioninfo.h"
#include "rline/rlsessionmanager.h"

namespace rage
{

//PURPOSE
//  Base class for LAN broadcasts containing queries and results.
struct rlMsgSearch
{
    //Signature to identify finder packets from other types of data.
    static const unsigned SEARCH_MSG_SIG =
        NET_MAKE_SIG4('R'-'A', 'L'-'A', 'S'-'A', 'F'-'A');

    rlMsgSearch()
        : m_Sig(SEARCH_MSG_SIG)
        , m_Nonce(0)
    {
    }

    void Reset(const u64 nonce)
    {
        m_Nonce = nonce;
    }

    template<typename T, typename M>
    static bool Ser(T& bb, M& msg)
    {
        return bb.SerUns(msg.m_Sig, datBitsNeeded<SEARCH_MSG_SIG>::COUNT)
                && SEARCH_MSG_SIG == msg.m_Sig
                && bb.SerUns(msg.m_Nonce, 64);
    }

    unsigned m_Sig;
    u64 m_Nonce;
};

//PURPOSE
//  Broadcast on a LAN to answer session queries.
struct rlMsgSearchRequest : public rlMsgSearch
{
    NET_MESSAGE_DECL(rlMsgSearchRequest, RL_MSG_SEARCH_REQUEST);

    void Reset(const u64 nonce, const rlMatchingFilter& filter)
    {
        rlAssert(filter.IsValid());
        rlAssert(filter.GetConditionCount() <= RL_MAX_MATCHING_CONDITIONS);

        this->rlMsgSearch::Reset(nonce);
        m_MatchmakingSearch = true;
        m_Filter = filter;
    }

    void Reset(const u64 nonce, const rlSessionInfo& sessionInfo)
    {
        this->rlMsgSearch::Reset(nonce);
        m_MatchmakingSearch = false;
        m_SessionInfo = sessionInfo;
        m_Filter.Clear();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return rlMsgSearch::Ser(bb, msg)
                && bb.SerBool(msg.m_MatchmakingSearch)
                && (!msg.m_MatchmakingSearch || bb.SerUser(msg.m_Filter));
    }

    rlMatchingFilter m_Filter;
    rlSessionInfo m_SessionInfo;
    bool m_MatchmakingSearch;
};

//PURPOSE
//  Broadcast on a LAN to search for LAN sessions.
struct rlMsgSearchResponse : public rlMsgSearch
{
    NET_MESSAGE_DECL(rlMsgSearchResponse, RL_MSG_SEARCH_RESPONSE);

    void Reset(const u64 nonce,
                const rlSessionDetail& result)
    {
        this->rlMsgSearch::Reset(nonce);
        m_Result = result;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return rlMsgSearch::Ser(bb, msg)
                && bb.SerUser(msg.m_Result);
    }

    rlSessionDetail m_Result;
};

//PURPOSE
//  Asynchronously searches for sessions that meet conditions set
//  in a search filter.
class rlSessionFinder
{
public:
    //PURPOSE
    //  Initiates an asynchronous search for sessions that match the
    //  conditions in the filter.
    //PARAMS
    //  localGamerIndex - Index of user requesting the query.
    //  netMode         - NETMODE_ONLINE or NETMODE_LAN.
    //  channelId       - Network channel on which to communicate with session hosts.
    //  numGamers		- Number of gamers that will participate
    //                    in the session.  Only sessions that can accommodate
    //                    this number of gamers will be returned by the query.
    //  filter          - Contains conditions used to filter matches
    //  results         - Will be populated with details;s from found sessions.
    //                    Caller must not deallocate this until the operation
    //                    completes.
    //  maxResults      - Maximum number of results to be retrieved.
	//  numResultsBeforeEarlyOut - Sets the minimum number of successful tunnel/host query results we want before
	//							   we early out of the rlSessionQueryDetailTask. This prevents long-running errant
	//							   tunnel requests or host queries to delay the game from joining sessions
	//							   (or...don't let one bad apple spoil the whole damn bunch)
    //  numResults      - Will be populated with the number of results retrieved
    //  status          - Status of the operation.  Can be polled for completion.
    //NOTES
    //  Each condition in the filter is a logical operation (==, !=, <, etc.)
    //  whose operands come from fields in filterVals.
    //  The conditions are used to compare values registered by a host
    //  to values in filterVals.  Nil fields in filterVals are treated
    //  as wild cards and match any value registered by the host.
    //
    //  filterVals should be an instance of rlConcreteSchema<> with the
    //  template parameter being the match schema from a .schema.h file.
    //  The filter should be an instance of rlConcreteSessionFilter<>, with 
	//  the template parameter coming from a filter definition in a .schema.h file.
    //
    //  The call returns immediately but the search is not complete until
    //  status->Pending() returns false.
    //
    //  The caller must not de-allocate the filter, results, or status objects
    //  until the operation completes.
    //
    //  When using Sony NP the filter can contain only 7 conditions.  NP
    //  permits 8 but we use one to filter on game mode/type.
    static bool Find(const int localGamerIndex,
                    const rlNetworkMode netMode,
                    const unsigned channelId,
                    const int numGamers,
                    const rlMatchingFilter& filter,
                    rlSessionDetail* results,
                    const unsigned maxResults,
					const unsigned numResultsBeforeEarlyOut,
                    unsigned* numResults,
                    netStatus* status);
    //PURPOSE
    //  Initiates an asynchronous social matchmaking search for sessions.
    //  Social matchmaking searches for sessions containing players who match
    //  the search criteria.
    //PARAMS
    //  localGamerIndex - Index of user requesting the query.
    //  channelId       - Network channel on which to communicate with session hosts.
    //  queryName       - Name of predefined social matchmaking query
    //  queryParams     - Query parameters.  Each parameter is a name/value
    //                    pair.  The name is prefixed with '@'.
    //                    Example:
    //                      @crewid,1234,@longitude,44,@latitude,76
    //
    //  results         - Will be populated with details from found sessions.
    //                    Caller must not deallocate this until the operation
    //                    completes.
    //  maxResults      - Maximum number of results to be retrieved.
	//  numResultsBeforeEarlyOut - Sets the minimum number of successful tunnel/host query results we want before
	//							   we early out of the rlSessionQueryDetailTask. This prevents long-running errant
	//							   tunnel requests or host queries to delay the game from joining sessions
	//							   (or...don't let one bad apple spoil the whole damn bunch)
    //  numResults      - Will be populated with the number of results retrieved
    //  status          - Status of the operation.  Can be polled for completion.
    //NOTES
    //  Unlike traditional matchmaking, which searches for sessions that match
    //  the search criteria, social matchmaking searches for sessions containing
    //  players that match the search criteria.
    //
    //  For example, we might search for sessions containing players located in
    //  North America.  Or we might search for sessions containing players who
    //  are in the same crew.
    //
    //  The call returns immediately but the search is not complete until
    //  status->Pending() returns false.
    //
    //  The caller must not de-allocate the results, numResults, or status
    //  objects until the operation completes.
    //EXAMPLE
    //  In this example we search for sessions containing players that match the
    //  predefined query called "CrewmateSessions".
    //  The single parameter to "CrewmateSessions" is "@crewid".
    //
    //      rlSessionDetail results[10];
    //      unsigned numResults;
    //      netStatus status;
    //
    //      rlSessionFinder::FindSocial(localGamerIndex,
    //                                    LiveManager::SESSION_CHANNEL_ID,
    //                                    "CrewmateSessions",
    //                                      "@crewid,1642",
    //                                      results,
    //                                      10,
    //                                      &numResults,
    //                                      &status);
    //
    static bool FindSocial(const int localGamerIndex,
                            const unsigned channelId,
                            const char* queryName,
                            const char* queryParams,
                            rlSessionDetail* results,
                            const unsigned maxResults,
							const unsigned numResultsBeforeEarlyOut,
                            unsigned* numResults,
                            netStatus* status);

private:

    rlSessionFinder();
    ~rlSessionFinder();
};

}   //namespace rage

#endif  //RLINE_RLSESSIONFINDER_H
