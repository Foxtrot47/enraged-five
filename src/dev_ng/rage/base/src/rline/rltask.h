// 
// rline/rltask.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLTASKS_H
#define RLINE_RLTASKS_H

#include "rl.h"
#include "rldiag.h"
#include "atl/inlist.h"
#include "atl/inmap.h"
#include "diag/seh.h"
#include "net/status.h"
#include "string/string.h"
#include "system/memory.h"

namespace rage
{

class diagChannel;
class netStatus;
class rlTaskManager;
class sysMemAllocator;

#if !__NO_OUTPUT

//PURPOSE
//  Place this macro in the declaration of a task, so that it's
//  name will be correctly set.
//
#define RL_TASK_DECL(name)\
    virtual const char* GetTaskName() const {return #name;}

//PURPOSE
//  Place RL_TASK_USE_CHANNEL in the definition of a task to set which
//  channel the rlTaskXXXX macros (defined below) output to.
//
#define RL_TASK_USE_CHANNEL(tag)\
    virtual const diagChannel* GetDiagChannel() const {return &Channel_##tag;}

//PURPOSE
//  Macros that prefix the task's name and ID to output,
//  which is sent to channel declared by RL_TASK_USE_CHANNEL/SUBCHANNEL.

#define rlTaskLogfHelper(task,channel,severity,fmt,...)\
    diagLogfHelper(channel, severity, "%s[%u]%s: " fmt, (task)->GetTaskName(), (task)->GetTaskId(), (task)->GetTaskLogContext(), ##__VA_ARGS__);

#define rlTaskVerifyHelper(task,channel,cond,fmt,...)\
	diagVerifyfHelper(cond, channel, "%s[%u]%s: " fmt, (task)->GetTaskName(), (task)->GetTaskId(), (task)->GetTaskLogContext(), ##__VA_ARGS__)

#define rlTaskAssertHelper(task,channel,cond,fmt,...)\
	diagAssertfHelper(cond, channel, "%s[%u]%s: " fmt, (task)->GetTaskName(), (task)->GetTaskId(), (task)->GetTaskLogContext(), ##__VA_ARGS__);

//Use this version when calling from outside the task.
#define rlTaskDebugPtr(task, fmt, ...)			rlTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define rlTaskDebug1Ptr(task, fmt, ...)			rlTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define rlTaskDebug2Ptr(task, fmt, ...)			rlTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG2, fmt, ##__VA_ARGS__);
#define rlTaskDebug3Ptr(task, fmt, ...)			rlTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG3, fmt, ##__VA_ARGS__);
#define rlTaskWarningPtr(task, fmt, ...)		rlTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_WARNING, fmt, ##__VA_ARGS__);
#define rlTaskErrorPtr(task, fmt, ...)			rlTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_ERROR, fmt, ##__VA_ARGS__);
#define rlTaskVerifyPtr(task, cond)				rlTaskVerifyHelper(task,(*(task)->GetDiagChannel()), cond, "")
#define rlTaskVerifyfPtr(task, cond, fmt, ...)	rlTaskVerifyHelper(task,(*(task)->GetDiagChannel()), cond, fmt, ##__VA_ARGS__)
#define rlTaskAssertPtr(task, cond)				rlTaskAssertHelper(task,(*(task)->GetDiagChannel()), cond, "");
#define rlTaskAssertfPtr(task, cond, fmt, ...)	rlTaskAssertHelper(task,(*(task)->GetDiagChannel()), cond, fmt, ##__VA_ARGS__);

#else

#define RL_TASK_DECL(name)\
    virtual const char* GetTaskName() const {return "";}

#define RL_TASK_USE_CHANNEL(tag)\
    virtual const diagChannel* GetDiagChannel() const {return NULL;}

#define rlTaskDebugPtr(task, fmt, ...)
#define rlTaskDebug1Ptr(task, fmt, ...)
#define rlTaskDebug2Ptr(task, fmt, ...)
#define rlTaskDebug3Ptr(task, fmt, ...)
#define rlTaskWarningPtr(task, fmt, ...)
#define rlTaskErrorPtr(task, fmt, ...)
#define rlTaskVerifyPtr(task, cond)				(cond)
#define rlTaskVerifyfPtr(task, cond, fmt, ...)	(cond)
#define rlTaskAssertPtr(task, cond)
#define rlTaskAssertfPtr(task, cond, fmt, ...)

#endif //!__NO_OUTPUT

//Use this version when calling from within the task (most common case).
#define rlTaskDebug(fmt, ...)			rlTaskDebugPtr(this,fmt,##__VA_ARGS__)
#define rlTaskDebug1(fmt, ...)			rlTaskDebug1Ptr(this,fmt,##__VA_ARGS__)
#define rlTaskDebug2(fmt, ...)			rlTaskDebug2Ptr(this,fmt,##__VA_ARGS__)
#define rlTaskDebug3(fmt, ...)			rlTaskDebug3Ptr(this,fmt,##__VA_ARGS__)
#define rlTaskWarning(fmt, ...)			rlTaskWarningPtr(this,fmt,##__VA_ARGS__)
#define rlTaskError(fmt, ...)			rlTaskErrorPtr(this,fmt,##__VA_ARGS__)
#define rlTaskVerify(cond)				rlTaskVerifyPtr(this,cond)
#define rlTaskVerifyf(cond, fmt, ...)   rlTaskVerifyfPtr(this,cond,fmt,##__VA_ARGS__)
#define rlTaskAssert(cond)				rlTaskAssertPtr(this,cond)
#define rlTaskAssertf(cond, fmt, ...)   rlTaskAssertfPtr(this,cond,fmt,##__VA_ARGS__)

template<typename T> class rlTask;

//PURPOSE
//  Used for fire and forget tasks.  The embedded status
//  object can be passed as the netStatus* parameter to Configure().
//NOTES
//  It's not advisable to keep a reference to the embedded status
//  object as it could be freed at any time.

//  It's therefore usually not possible to cancel a fire and forget task
//  by passing the status object to rlTaskManager::Cancel().
//
//  Instead, keep a reference to the task ID and pass it to rlTaskManager::Cancel().
//
//EXAMPLE
//  rlFireAndForgetTask<SomeOtherTaskType>* task;
//  rlGetTaskManager()->CreateTask(&task);
//  rlTaskBase::Configure(task, &task->m_Status);
//  rlGetTaskManager()->AppendSerialTask(task);
//
template<typename T>
class rlFireAndForgetTask : public T
{
public:

    netStatus m_Status;
};

//PURPOSE
//  Base class for all tasks.
class rlTaskBase
{
    friend class rlTaskManager;

public:

    enum FinishType
    {
        FINISH_FAILED,
        FINISH_SUCCEEDED,
        FINISH_CANCELED
    };

	enum ShutdownBehaviour
	{
		CANCEL_ON_SHUTDOWN,
		EXECUTE_ON_SHUTDOWN,
	};

    //PURPOSE
    //  Returns string name for finishType.
    //  Ex. FINISH_SUCCEEDED returns "SUCCEEDED".
    static const char* FinishString(const FinishType finishType);

    //PURPOSE
    //  Convenience function that returns true if finishType is FINISH_SUCCEEDED.
    static bool FinishSucceeded(const FinishType finishType);

    rlTaskBase();

    virtual ~rlTaskBase();

    //PURPOSE
    //  Returns unique ID of task instance, which is assigned in ctor.
    unsigned GetTaskId() const;

    //PURPOSE
    //  Returns the name of the task.
    virtual const char* GetTaskName() const;

    //PURPOSE
    //  Starts the task.  After calling Start(), call Update()
    //  every frame with the number of milliseconds elapsed since the
    //  last call to Update().
    //NOTES
    //  The status object passed to Configure() will be set to
    //  Pending().
    virtual void Start();

    //PURPOSE
    //  Performs the task's work.
    //  After calling Start(), call Update() every frame with the number
    //  of milliseconds elapsed since the last call to Update().
    virtual void Update(const unsigned timeStepMs);

    //PURPOSE
    //  Returns true if the task was started.
    bool IsStarted() const;

    //PURPOSE
    //  Returns true if the task is finished.
    bool IsFinished() const;

    //PURPOSE
    //  Returns true if the task is currently active (i.e. after
    //  calling Start() and before calling Finish()).
    bool IsActive() const;

    //PURPOSE
    //  Returns true if the task has been configured but has not finished.
    //NOTES
    //  This is different than IsActive().  IsActive() will return true
    //  only if the task has been started, but not finished.  IsPending()
    //  will return true even if the task has not started, but only if it
    //  has been configured.
    bool IsPending() const;

    //PURPOSE
    //  Returns true if the task was canceled.  The task might still be
    //  active until it has a chance to finish.
    //NOTES
    //  If WasCanceled() returns true it implies the task was cancelable.
    //  A cancelable task can cleanly handle being canceled.
    //  A canceled task is not necessarily finished.  If an underlying
    //  operation on which the task depends cannot be canceled then the
    //  task must run until the operation completes.  The netStatus object
    //  passed to Configure(), however, will have its state set to canceled
    //  and higher level processing can continue as if the task had finished
    //  unsuccessfully.
    //
    //  Cancelable tasks MUST handle being canceled.  That means that
    //  any asynchronous operations on which the task depends must also
    //  be cancelable.  If they are not then the task must be able to
    //  continue running in a canceled state until the underlying operation
    //  finishes.
    //
    //  A canceled task MUST NOT access memory it doesn't own, e.g. pointers
    //  passed to Configure().
    bool WasCanceled() const;

    //PURPOSE
    //  Returns true if the task can be reliably canceled.
    //NOTES
    //  Cancelable tasks MUST handle being canceled.  That means that
    //  any asynchronous operations on which the task depends must also
    //  be cancelable.  If they are not then the task must be able to
    //  continue running in a canceled state until the underlying operation
    //  finishes.
    //
    //  A canceled task MUST NOT access memory it doesn't own, e.g. pointers
    //  passed to Configure().
    virtual bool IsCancelable() const;

	//PURPOSE
	//  Cancels this task (IsCancelable needs to be true for this to work)
	void Cancel();

	//PURPOSE
	//  Returns true if the status object for this task is set to canceled.
	bool IsStatusCanceled() const;

    //PURPOSE
    //  Returns the channel to be used for debug output.  Overridden by use
    //  of the RL_TASK_USE_CHANNEL macro.
    virtual const diagChannel* GetDiagChannel() const;

#if !__NO_OUTPUT
	//PURPOSE
	//  Overridable prefix for logging
	virtual const char* GetTaskLogContext() const { return ""; }
#endif

	virtual ShutdownBehaviour GetShutdownBehaviour() { return CANCEL_ON_SHUTDOWN; }

    //ConfigureContext handles configuring rlTask<> instances with their context.

    protected:

    //PURPOSE
    //  For the case where the task is not a subclass of rlTask<>.
    template<typename CTX>
    static bool ConfigureContext(void* /*task*/, CTX /*ctx*/)
    {
        return true;
    }

    //PURPOSE
    //  For the case where the task is a subclass of rlTask<>.
    template<typename CTX>
    static bool ConfigureContext(rlTask<CTX>* task, CTX* ctx)
    {
		if(rlVerify(!task->m_Ctx))
        {
			task->m_Ctx = ctx;
			return true;
        }
        else
        {
            return false;
        }
    }

    public:

    template<typename TASK_TYPE>
    static bool Configure(TASK_TYPE* task, netStatus* status)
    {
        rlTaskDebug2Ptr(task, "Configuring...");

        if(!status || rlVerify(!status->Pending()))
        {
		    if(task->rlTaskBase::Configure(status))
            {
                return true;
            }
            else if(status && !status->Failed())
            {
                status->SetPending();status->SetFailed();
            }
        }

        return false;
    }

    template<typename TASK_TYPE, typename A0>
    static bool Configure(TASK_TYPE* task, A0 a0, netStatus* status)
    {
        rlTaskDebug2Ptr(task, "Configuring...");

        if(!status || rlVerify(!status->Pending()))
        {
		    if(task->ConfigureContext(task, a0)
                && task->Configure(a0)
                && task->rlTaskBase::Configure(status))
            {
                return true;
            }
            else if(status && !status->Failed())
            {
				rlTaskErrorPtr(task, "Failed to configure");
				status->SetPending();status->SetFailed();
            }
        }

        return false;
    }

    template<typename TASK_TYPE, typename A0, typename A1>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
	}

    template<typename TASK_TYPE, typename A0, typename A1, typename A2>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9, typename A10>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, A10 a10, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

    template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9, typename A10, typename A11>
    static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, A10 a10, A11 a11, netStatus* status)
    {
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
    }

	template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9, typename A10, typename A11, typename A12>
	static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, A10 a10, A11 a11, A12 a12, netStatus* status)
	{
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
	}

	template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9, typename A10, typename A11, typename A12, typename A13>
	static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, A10 a10, A11 a11, A12 a12, A13 a13, netStatus* status)
	{
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
	}

	template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9, typename A10, typename A11, typename A12, typename A13, typename A14>
	static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, A10 a10, A11 a11, A12 a12, A13 a13, A14 a14, netStatus* status)
	{
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
	}

	template<typename TASK_TYPE, typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9, typename A10, typename A11, typename A12, typename A13, typename A14, typename A15>
	static bool Configure(TASK_TYPE* task, A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, A10 a10, A11 a11, A12 a12, A13 a13, A14 a14, A15 a15, netStatus* status)
	{
		bool success = false;

		rtry
		{
			rverify(!status || !status->Pending(), catchall, rlTaskErrorPtr(task, "Configure - Status pending!"));

			rcheck(task->ConfigureContext(task, a0), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure context!"));

			rlTaskDebug2Ptr(task, "Configuring...");
			rcheck(task->Configure(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure task!"));
			rcheck(task->rlTaskBase::Configure(status), catchall, rlTaskErrorPtr(task, "Configure - Failed to configure base!"));

			success = true;
		}
		rcatchall
		{
			if(status && !status->Failed())
			{
				status->SetPending();status->SetFailed();
			}
		}

		return success;
	}
protected:

    //PURPOSE
    //  Finishes the task.
    //PARAMS
    //  finishType  - How the task was finished
    //NOTES
    //  This is a simple way to finish for tasks that don't need
    //  to communicate a result code to their status object.
    //
    //  The status object passed to Configure() will be set to
    //  Succeeded or Failed based on the value of the success
    //  parameter. The result code will be 0 and -1, respectively.
    virtual void Finish(const FinishType finishType);
    virtual void Finish(const FinishType finishType, const int resultCode);

    virtual void DoCancel();

    //PURPOSE
    //  Sets the task's timeout.
    //PARAMS
    //  timeout     - Timeout in milliseconds.
    void SetTimeoutMs(const unsigned timeoutMs);

    //PURPOSE
    //  Returns the number of milliseconds until timeout.
    unsigned GetMsUntilTimeout() const;

    //PURPOSE
    //  Returns GetMsUntilTimeout() / 1000.
    unsigned GetSecondsUntilTimeout() const;

    void ClearTimeout();

    bool TimedOut() const;

private:

    enum State
    {
        STATE_INVALID       = -1,
        STATE_CONFIGURED,
        STATE_STARTED,
        STATE_FINISHED
    };

    enum Flags
    {
        FLAG_CANCELING      = 0x01,
        FLAG_CANCELED       = 0x02,
    };

    //PURPOSE
    //  Configure the task prior to running.
    //NOTES
    //  Subclasses should override this method with task-specific
    //  config parameters.  Subclasses must pass a netStatus 
    //  parameter to the base class Configure().
    bool Configure(netStatus* status);

#if !__NO_OUTPUT
	u32 m_ConfigureTime;
	u32 m_StartTime;
#endif

    unsigned m_Id;
    netStatus* m_Status;
    State m_State;

    unsigned m_Flags;

    int m_TimeoutTimerMs;

    //Task manager that's processing this instance.
    rlTaskManager* m_Owner;

    //Task manager that created this instance.
    rlTaskManager* m_Creator;

    inmap_node<unsigned, rlTaskBase> m_MapNodeByTaskId;
    inmap_node<const netStatus*, rlTaskBase> m_MapNodeByStatus;
    inmap_node<size_t, rlTaskBase> m_MapNodeByQueueId;
    rlTaskBase* m_Next;

    //Prevent copying
    rlTaskBase(const rlTaskBase&);
    rlTaskBase& operator=(const rlTaskBase&);
};

template<typename CTX>
class rlTask : public rlTaskBase
{
    friend class rlTaskBase;
public:

    rlTask()
    : m_Ctx(NULL)
    {
    }

protected:

    virtual void Finish(const FinishType finishType)
    {
        this->Finish(finishType, 0);
    }

    virtual void Finish(const FinishType finishType, const int resultCode)
    {
        this->rlTaskBase::Finish(finishType, resultCode);
        m_Ctx = NULL;
    }

    //Context on which the task operates.
    CTX* m_Ctx;
};

//PURPOSE
//  Manages two sets of running tasks.  One set runs in parallel, the other
//  in serial.  A serial task won't run until all tasks queued before it have
//  completed.
//
//  Tasks created with CreateTask() are automatically deleted when they
//  complete, but only if they're queued using one of the functions that
//  queues tasks.
//  If a task is created with CreateTask() but not queued for execution
//  be sure to call DestroyTask() to release resources.
class rlTaskManager
{
    friend class rlTaskBase;

public:
    rlTaskManager();
    ~rlTaskManager();

    //PURPOSE
    //  Initializes the rlTaskManager instance.
    //PARAMS
    //  allocator   - Optional.  Used to allocate new tasks.
    //NOTES
    //  If allocator is NULL then CreateTask() can't be used.
    bool Init(sysMemAllocator* allocator);

    //PURPOSE
    //  Shuts down the rlTaskManager instance.
    void Shutdown();

    //PURPOSE
    //  Returns true if Init() has been called and Shutdown() has not
    //  been called.
    bool IsInitialized() const;

    //PURPOSE
    //  Updates pending tasks.  Should be called once per frame.
    void Update();

	//PURPOSE
	//  Updates a particular task
	void UpdateTask(const netStatus* status);

    //PURPOSE
    //  Adds a task for execution in parallel to all other tasks added
    //  with this function.
    //  If the task was created with CreateTask(), it will be automatically
    //  destroyed when it finishes.
    bool AddParallelTask(rlTaskBase* task);

    //PURPOSE
    //  Appends a task to the default queue.
    //  It will be executed after all prior serial tasks have completed.
    //  If the task was created with CreateTask(), it will be automatically
    //  destroyed when it finishes.
    bool AppendSerialTask(rlTaskBase* task);

    //PURPOSE
    //  Appends a task to the serial task queue identified by the queue ID.
    //  It will be executed after all prior serial tasks have completed.
    //  If the task was created with CreateTask(), it will be automatically
    //  destroyed when it finishes.
    //PARAMS
    //  queueId     - Id of task queue.  CANNOT be zero.
    //  task        - The task to run.
    bool AppendSerialTask(const size_t queueId,
                            rlTaskBase* task);

    //PURPOSE
    //  Returns true if the queue contains a pending task
    bool HasPendingTask(const size_t queueId) const;
	bool HasAnyPendingTasks();

    //PURPOSE
    //  Retrieves a task instance with the given id.
    //  Returns NULL if the task has completed.
    rlTaskBase* FindTask(const unsigned taskId);

    //PURPOSE
    //  Retrieves a task instance which the status object is monitoring.
    //  Returns NULL if the task has completed.
    rlTaskBase* FindTask(const netStatus* status);

    #define RLTASK_CREATETASK(T, ...)                                                   \
        rlAssert(m_Initialized && m_Allocator);                                           \
                                                                                        \
        RL_RAGE_TRACK(TaskManager);                                                     \
                                                                                        \
        T* task = (T*)m_Allocator->LoggedAllocate(sizeof(T), 0, 0, __FILE__, __LINE__); \
                                                                                        \
        if(rlVerifyf(task, "Error allocating task"))                                      \
        {                                                                               \
            new(task) T(__VA_ARGS__);                                                   \
            task->rlTaskBase::m_Creator = this;                                         \
        }                                                                               \
                                                                                        \
        return task;                                                                    \


    //PURPOSE
    //  Creates a task and queues it for execution.  The manager owns this
    //  task, and will automatically destroy it when it finishes.
    template<class T>
    T* CreateTask()
    {
        RLTASK_CREATETASK(T);
    }

    template<class T, typename A0>
    T* CreateTask(A0 a0)
    {
        RLTASK_CREATETASK(T, a0);
    }

    template<class T, typename A0, typename A1>
    T* CreateTask(A0 a0, A1 a1)
    {
        RLTASK_CREATETASK(T, a0, a1);
    }

    template<class T, typename A0, typename A1, typename A2>
    T* CreateTask(A0 a0, A1 a1, A2 a2)
    {
        RLTASK_CREATETASK(T, a0, a1, a2);
    }

    template<class T, typename A0, typename A1, typename A2, typename A3>
    T* CreateTask(A0 a0, A1 a1, A2 a2, A3 a3)
    {
        RLTASK_CREATETASK(T, a0, a1, a2, a3);
    }

    #undef RLTASK_CREATETASK

    template<class T>
    bool CreateTask(T** task)
    {
        *task = this->CreateTask<T>();
        return *task ? true : false;
    }

    //PURPOSE
    //  Cancels the task with the given id.
    //NOTES
    //  This will assert if the task has not been queued for execution.
    //  The task MUST be cancelable.
    //  The task will continue to run in a canceled state until all
    //  operations on which the task depends have been canceled or completed.
    void CancelTask(const unsigned taskId);

    //PURPOSE
    //  Cancels the task which the status object is monitoring.
    //NOTES
    //  This will assert if the task has not been queued for execution.
    //  The task MUST be cancelable.
    //  The task will continue to run in a canceled state until all
    //  operations on which the task depends have been canceled or completed.
    void CancelTask(const netStatus* status);

    //PURPOSE
    //  Cancels the task.
    //NOTES
    //  This will assert if the task has not been queued for execution.
    //  The task MUST be cancelable.
    //  The task will continue to run in a canceled state until all
    //  operations on which the task depends have been canceled or completed.
    void CancelTask(rlTaskBase* task);

    //PURPOSE
    //  Cancels all currently running tasks for the given queue ID.
    //NOTES
    //  Tasks that have not been queued will not be canceled.
    //  Canceled tasks will continue to run in a canceled state until all
    //  operations on which the task depends have been canceled or completed.
    void CancelAll(const size_t queueId);

    //PURPOSE
    //  Cancels all currently running tasks.
    //NOTES
    //  Tasks that have not been queued will not be canceled.
    //  Canceled tasks will continue to run in a canceled state until all
    //  operations on which the task depends have been canceled or completed.
    void CancelAll();

	// PURPOSE
	//	Cancels all cancellable running tasks on shutdown.
	//	Certain tasks are marked as EXECUTE_ON_SHUTDOWN so we avoid cancelling those.
	void ShutdownCancel();

    //PURPOSE
    //  Detaches the netStatus object associated with this task.
    //NOTES
    //  This will assert if the task has not been queued for execution.
    //  The task status must not be Pending.
    //  The task will remain and complete as normal. 
    void DetachStatus(rlTaskBase* task);

    //PURPOSE
    //  Destroys a task created with CreateTask().
    //NOTES
    //  The most common use of this (really, perhaps the only use)
    //  is to destroy a task rather than queuing it when its Configure()
    //  fails.
    void DestroyTask(rlTaskBase* task);

	//PURPOSE
	//  Dumps task queue to log window
#if !__NO_OUTPUT
	void Dump();
#endif // !__NO_OUTPUT

private:

    bool DoAddTask(const size_t queueId,
                    rlTaskBase* task);

    void UpdateTask(rlTaskBase* task,
                    const unsigned deltaMs);

    void RemoveTask(rlTaskBase* task);

    sysMemAllocator* m_Allocator;

    typedef inmap<unsigned, rlTaskBase, &rlTaskBase::m_MapNodeByTaskId> TaskMapById;
    typedef inmap<const netStatus*, rlTaskBase, &rlTaskBase::m_MapNodeByStatus> TaskMapByStatus;
    typedef inmultimap<size_t, rlTaskBase, &rlTaskBase::m_MapNodeByQueueId> TaskQueue;

    TaskMapById m_TaskMapById;
    TaskMapByStatus m_TaskMapByStatus;
    TaskQueue m_TaskQ;

    unsigned m_LastUpdateTime;
    
    bool m_Initialized  : 1;
};

}   //namespace rage

#endif  //RLINE_RLTASK_H
