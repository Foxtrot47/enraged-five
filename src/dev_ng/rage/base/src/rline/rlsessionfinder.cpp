// 
// rline/rlsessionfinder.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
 
#include "rlsessionfinder.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "net/nethardware.h"
#include "net/netnonce.h"
#include "net/task.h"
#include "net/time.h"
#include "rlpresence.h"
#include "rltask.h"
#include "ros/rlros.h"
#include "scmatchmaking/rlscmatchmaking.h"
#include "system/nelem.h"
#include "rltitleid.h"

#if RSG_DURANGO
#include "durango/rlxbl_interface.h"
#elif RSG_NP
#include "rlnp.h"
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, sessionfinder);
#undef __rage_channel
#define __rage_channel rline_sessionfinder

NET_MESSAGE_IMPL(rlMsgSearchRequest);
NET_MESSAGE_IMPL(rlMsgSearchResponse);

//////////////////////////////////////////////////////////////////////////
//  rlFindSessionsTask
//////////////////////////////////////////////////////////////////////////
class rlFindSessionsTask : public netTask
{
public:

    static const int MAX_SESSIONS_TO_FIND   = 20;

	NET_TASK_USE_CHANNEL(rline_sessionfinder);
	NET_TASK_DECL(rlFindSessionsTask);

    rlFindSessionsTask();
    ~rlFindSessionsTask();

    bool Configure(const int localGamerIndex,
                   const rlNetworkMode netMode,
                   const unsigned channelId,
                   const int numGamers,
                   const rlMatchingFilter& filter,
                   const unsigned lanPort,
                   rlSessionDetail* results,
                   const unsigned maxResults,
				   const unsigned numResultsBeforeEarlyOut,
                   unsigned* numResults);

	virtual netTaskStatus OnUpdate(int* resultCode);

    virtual void OnCancel();

    virtual void OnCleanup();

private:

#if SUPPORT_LAN_MULTIPLAYER
    bool LanBeginFind(netStatus* status);
    void LanUpdate(netStatus* status);
#endif

    enum State
    {
		STATE_SEARCH,
		STATE_SEARCHING,
        STATE_QUERY_DETAILS,
        STATE_QUERYING_DETAILS,
    };

    State m_State;

    rlSessionInfo m_SessionInfos[MAX_SESSIONS_TO_FIND];
    unsigned m_NumSessionsFound;

    int m_LocalGamerIndex;
    rlNetworkMode m_NetMode;
    unsigned m_ChannelId;

    //Matchmaking search parameters
    int m_NumGamers;
    rlMatchingFilter m_Filter;

    rlSessionDetail* m_Results;
    unsigned m_MaxResults;
	unsigned m_NumResultsBeforeEarlyOut;
	unsigned* m_NumResults;
    netStatus m_MyStatus;

#if SUPPORT_LAN_MULTIPLAYER
	static const unsigned LAN_RETRY_INTERVAL_MS = 250;
    u64 m_LanBcastNonce;
    netSocket m_LanBcastSkt;
    unsigned m_LanBcastPort;
    unsigned m_LanBcastAttempts;
    netRetryTimer m_LanBcastRetryTimer;
#endif
};

rlFindSessionsTask::rlFindSessionsTask()
: m_State(STATE_SEARCH)
, m_NumSessionsFound(0)
, m_LocalGamerIndex(-1)
, m_NetMode(RL_NETMODE_INVALID)
, m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_NumGamers(0)
, m_Results(NULL)
, m_MaxResults(0)
, m_NumResultsBeforeEarlyOut(rlSessionManager::DEFAULT_NUM_QUERY_RESULTS_BEFORE_EARLY_OUT)
, m_NumResults(NULL)
#if SUPPORT_LAN_MULTIPLAYER
, m_LanBcastNonce(0)
, m_LanBcastPort(0)
, m_LanBcastAttempts(0)
#endif
{

}

rlFindSessionsTask::~rlFindSessionsTask()
{

}

bool
rlFindSessionsTask::Configure(const int localGamerIndex,
                                const rlNetworkMode netMode,
                                const unsigned channelId,
                                const int numGamers,
                                const rlMatchingFilter& filter,
#if SUPPORT_LAN_MULTIPLAYER
                                const unsigned lanPort,
#else
								const unsigned UNUSED_PARAM(lanPort),
#endif
                                rlSessionDetail* results,
                                const unsigned maxResults,
								const unsigned numResultsBeforeEarlyOut,
                                unsigned* numResults)
{
    if(!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        netTaskError("Invalid LocalGamerIndex: %d", localGamerIndex);
        return false;
    }
    m_LocalGamerIndex = localGamerIndex;
    m_NetMode = netMode;
    m_ChannelId = channelId;
    m_NumGamers = numGamers;
    m_Filter = filter;

#if SUPPORT_LAN_MULTIPLAYER
    m_LanBcastPort = lanPort;
#endif

    m_Results = results;
    m_MaxResults = maxResults;
    m_NumResults = numResults;
	m_NumResultsBeforeEarlyOut = numResultsBeforeEarlyOut;

    if(m_MaxResults > MAX_SESSIONS_TO_FIND)
    {
        m_MaxResults = MAX_SESSIONS_TO_FIND;
    }

    *m_NumResults = 0;

    netTaskDebug("Find :: NetMode: %d, ChannelId: %u, NumGamers: %d, MaxResults: %u", netMode, channelId, numGamers, maxResults);

	m_State = STATE_SEARCH;

	return true;
}

netTaskStatus
rlFindSessionsTask::OnUpdate(int* /*resultCode*/)
{
	if(WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

    switch(m_State)
    {
    case STATE_SEARCH:
        if(RL_NETMODE_ONLINE == m_NetMode)
        {
            netTaskDebug("Finding using SC Matchmaking");
            if(rlScMatchmaking::Find(m_LocalGamerIndex,
									 m_NumGamers,
									 m_Filter,
									 m_MaxResults,
									 m_SessionInfos,
									 &m_NumSessionsFound,
									 &m_MyStatus))
            {
                netTaskDebug("Search: Finding...");
                m_State = STATE_SEARCHING;
            }
            else
            {
                netTaskError("Search: Failed: rlScMatchmaking::Find failed");
                return NET_TASKSTATUS_FAILED;
            }
        }
#if SUPPORT_LAN_MULTIPLAYER
		else if(RL_NETMODE_LAN == m_NetMode)
		{
            if(LanBeginFind(&m_MyStatus))
            {
                netTaskDebug("Search: Finding LAN...");
                m_State = STATE_SEARCHING;
            }
            else
            {
                netTaskError("Search: Failed: LanBeginFind failed");
                return NET_TASKSTATUS_FAILED;
            }
		}
#endif
        else
        {
            netTaskError("Search: Failed: Invalid NetMode: %d", m_NetMode);
            return NET_TASKSTATUS_FAILED;
        }
        break;

    case STATE_SEARCHING:
		if(RL_NETMODE_ONLINE == m_NetMode)
		{
			if(m_MyStatus.Succeeded())
			{
#if !__NO_OUTPUT
				netTaskDebug("Searching: Found %u compatible sessions", m_NumSessionsFound);
				for(int i = 0; i < (int)m_NumSessionsFound; i++)
				{
					netTaskDebug("\tSession %d - 0x%016" I64FMT "x",
								 i,
								 m_SessionInfos[i].GetToken().m_Value);
				}
#endif

				m_State = STATE_QUERY_DETAILS;
			}
			else if(!m_MyStatus.Pending())
			{
				m_NumSessionsFound = 0;
                netTaskError("Searching: Failed: Finding failed!");
				return NET_TASKSTATUS_FAILED;
			}
		}
#if SUPPORT_LAN_MULTIPLAYER
		else if(RL_NETMODE_LAN == m_NetMode)
		{
			LanUpdate(&m_MyStatus);

			if(m_MyStatus.Succeeded())
			{
				//We already have the session details, just skip to the end.
                netTaskDebug("Searching: Found %u compatible sessions", m_NumSessionsFound);
                return NET_TASKSTATUS_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				netTaskError("Searching: Failed: Finding failed");
				return NET_TASKSTATUS_FAILED;
			}
		}
#endif
		else
		{
            netTaskError("Searching: Failed: Invalid NetMode: %d", m_NetMode);
            return NET_TASKSTATUS_FAILED;
		}
        break;

    case STATE_QUERY_DETAILS:
		netTaskAssertf(RL_NETMODE_ONLINE == m_NetMode, "QueryDetails: Invalid NetMode: %d", m_NetMode);

        if(m_NumSessionsFound > 0)
        {
            netTaskAssertf(m_NumSessionsFound <= m_MaxResults, "QueryDetails: Exceeded Maximum (%u): %d", m_MaxResults, m_NumSessionsFound);
            if(rlSessionManager::QueryDetail(m_NetMode,
                                            m_ChannelId,
											0,
											0,
											m_NumResultsBeforeEarlyOut,
											false,
                                            m_SessionInfos,
                                            m_NumSessionsFound,
                                            m_Results,
                                            m_NumResults,
                                            &m_MyStatus))
            {
                netTaskDebug("QueryDetails: Querying Detail");
                m_State = STATE_QUERYING_DETAILS;
            }
            else
            {
                netTaskError("QueryDetails: Failed to Query Details");
                return NET_TASKSTATUS_FAILED;
            }
        }
        else
        {
			// no results to query, complete the task with success
            netTaskDebug("QueryDetails: No sessions to query");
            return NET_TASKSTATUS_SUCCEEDED;
        }
        break;

    case STATE_QUERYING_DETAILS:
        if(m_MyStatus.Succeeded())
        {
            netTaskDebug("QueryingDetails: Succeeded");
            return NET_TASKSTATUS_SUCCEEDED;
        }
        else if(!m_MyStatus.Pending())
        {
            netTaskError("QueryingDetails: Failed");
            return NET_TASKSTATUS_FAILED;
        }
        break;
    }

    return NET_TASKSTATUS_PENDING;
}

void
rlFindSessionsTask::OnCancel()
{
	if(m_MyStatus.Pending())
	{
		switch(m_State)
		{
		case STATE_SEARCH:
		case STATE_SEARCHING:
            netTaskDebug("OnCancel: Cancelling Find...");
            if(RL_NETMODE_ONLINE == m_NetMode)
			{
                rlGetTaskManager()->CancelTask(&m_MyStatus);
			}
			else
			{
                m_MyStatus.SetCanceled();
			}
			break;
		case STATE_QUERY_DETAILS:
		case STATE_QUERYING_DETAILS:
            netTaskAssertf(RL_NETMODE_ONLINE == m_NetMode, "OnCancel: Invalid NetMode: %d", m_NetMode);
            netTaskDebug("OnCancel: Cancelling QueryDetails...");
            rlSessionManager::Cancel(&m_MyStatus);
			break;
		}
	}
}

void
rlFindSessionsTask::OnCleanup()
{
#if SUPPORT_LAN_MULTIPLAYER
    netTaskDebug("OnCleanup: Destroying LAN broadcast socket");
    netHardware::DestroySocket(&m_LanBcastSkt);
#endif
}

#if SUPPORT_LAN_MULTIPLAYER
bool
rlFindSessionsTask::LanBeginFind(netStatus* status)
{
    if(netHardware::CreateSocket(&m_LanBcastSkt, 0, NET_PROTO_UDP, NET_SOCKET_NONBLOCKING))
    {
        m_LanBcastNonce = netGenerateNonce();
        m_LanBcastAttempts = 0;
        m_LanBcastRetryTimer.InitMilliseconds(LAN_RETRY_INTERVAL_MS);
        m_LanBcastRetryTimer.ForceRetry();
        *m_NumResults = 0;
        status->SetPending();
        netTaskDebug("LanBeginFind: Finding...");
        return true;
    }
    else
    {
        netTaskError("LanBeginFind: Error creating socket");
    }

    status->ForceFailed();
    return false;
}

void
rlFindSessionsTask::LanUpdate(netStatus* status)
{
    if(WasCanceled())
    {
        status->SetCanceled();
        return;
    }

    netTaskAssert(status->Pending());

    u8 pktBuf[netPacket::MAX_BYTE_SIZEOF_PACKET];
    unsigned sizeofPkt;
    const netSocketAddress bcastAddr(netIpAddress(netIpV4Address::GetBroadcastAddress()), (unsigned short)m_LanBcastPort);

    m_LanBcastRetryTimer.Update();

    if(m_LanBcastRetryTimer.IsTimeToRetry())
    {
        if(m_LanBcastAttempts >= 8)
        {
            status->SetSucceeded();
        }
        else
        {
            netTaskDebug("LanUpdate: Broadcasting session query on port:%d - attempt:%d", m_LanBcastPort, m_LanBcastAttempts);

            ++m_LanBcastAttempts;

            rlMsgSearchRequest rqst;
            rqst.Reset(m_LanBcastNonce, m_Filter);

            if(netTaskVerify(rqst.Export(&pktBuf[2], sizeof(pktBuf)-2, &sizeofPkt)))
            {
                //First 2 bytes is packet size.
                *(u16*)pktBuf = u16(sizeofPkt + 2);

                m_LanBcastSkt.Send(bcastAddr, pktBuf, sizeofPkt + 2);
            }
        }

        m_LanBcastRetryTimer.Reset();
    }

    if(status->Pending())
    {
        netSocketAddress sender;
        rlMsgSearchResponse resp;

        const int len = m_LanBcastSkt.Receive(&sender, pktBuf, sizeof(pktBuf));

        unsigned sizeofResp;
        //First 2 bytes are the size of the packet.
        if(len >= 2
            && resp.Import(&pktBuf[2], len - 2, &sizeofResp)
            && int(sizeofResp + 2) == len
            && resp.m_Nonce == m_LanBcastNonce)
        {
            bool haveNewResult = true;

            for(int i = 0; i < (int) *m_NumResults; ++i)
            {
                if(m_Results[i].m_SessionInfo == resp.m_Result.m_SessionInfo)
                {
                    haveNewResult = false;
                    break;
                }
            }

            if(haveNewResult)
            {
                netTaskDebug("Received response to session query from:" NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(sender));

                if(resp.m_Result.m_SessionInfo.GetHostPeerAddress().IsLocal())
                {
                    netTaskDebug("Found locally hosted session, discarding");
                }
                else
                {
                    m_Results[*m_NumResults] = resp.m_Result;
                    ++*m_NumResults;

                    netTaskDebug("Received: %d responses", *m_NumResults);

                    if(*m_NumResults == m_MaxResults)
                    {
                        status->SetSucceeded();
                    }
                }
            }
            else if(*m_NumResults > 0)
            {
                //If we didn't get a new result and we already have
                //some results, assume from this point we'll receive
                //mostly duplicate responses, and speed up the operation.
                ++m_LanBcastAttempts;
            }
        }
    }
}
#endif

///////////////////////////////////////////////////////////////////////////////
// rlSessionFindSocialTask
///////////////////////////////////////////////////////////////////////////////
class rlSessionFindSocialTask : public netTask
{
public:

	RL_TASK_USE_CHANNEL(rline_sessionfinder);
	RL_TASK_DECL(rlSessionFindSocialTask);

    rlSessionFindSocialTask();

    ~rlSessionFindSocialTask();

    bool Configure(const int localGamerIndex,
                    const unsigned channelId,
                    const char* queryName,
                    const char* queryParams,
                    rlSessionDetail* results,
                    const unsigned maxResults,
					const unsigned numResultsBeforeEarlyOut,
                    unsigned* numResults);

    virtual void OnCancel();

    virtual netTaskStatus OnUpdate(int* resultCode);

private:

    enum State
    {
        STATE_FIND_MATCHES,
        STATE_FINDING_MATCHES,
        STATE_QUERY_DETAIL,
        STATE_QUERYING_DETAIL
    };

    bool ParseResults();

    static const int MAX_SESSIONS_TO_FIND   = rlFindSessionsTask::MAX_SESSIONS_TO_FIND;

    State m_State;
    int m_LocalGamerIndex;
    unsigned m_ChannelId;

    //Results from presence query
    char m_QName[64];
    char m_QParams[256];
    char m_QRecordsBuf[RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE*MAX_SESSIONS_TO_FIND];
    char* m_QRecords[MAX_SESSIONS_TO_FIND];
    unsigned m_NumQRecordsRetrieved;
    unsigned m_NumQRecords;

    //Results from querying session hosts
    rlSessionInfo m_SessionInfos[MAX_SESSIONS_TO_FIND];
    unsigned m_NumSessionInfos;
	rlSessionDetail* m_Results;
    unsigned m_MaxResults;
	unsigned m_NumResultsBeforeEarlyOut;
    unsigned* m_NumResults;
    netStatus m_MyStatus;
};

rlSessionFindSocialTask::rlSessionFindSocialTask()
    : m_State(STATE_FIND_MATCHES)
    , m_LocalGamerIndex(-1)
    , m_ChannelId(NET_INVALID_CHANNEL_ID)
    , m_NumQRecordsRetrieved(0)
    , m_NumQRecords(0)
    , m_NumSessionInfos(0)
    , m_Results(NULL)
    , m_MaxResults(0)
	, m_NumResultsBeforeEarlyOut(rlSessionManager::DEFAULT_NUM_QUERY_RESULTS_BEFORE_EARLY_OUT)
    , m_NumResults(NULL)
{
}

rlSessionFindSocialTask::~rlSessionFindSocialTask()
{
}

bool
rlSessionFindSocialTask::Configure(const int localGamerIndex,
                                    const unsigned channelId,
                                    const char* queryName,
                                    const char* queryParams,
                                    rlSessionDetail* results,
                                    const unsigned maxResults,
									const unsigned numResultsBeforeEarlyOut,
                                    unsigned* numResults)
{
    if(!netTaskVerifyf(strlen(queryName) < sizeof(m_QName),
                "Query name '%s' is too long - max is %" SIZETFMT "u",
                queryName,
                sizeof(m_QName)-1))
    {
        return false;
    }
    else if(!netTaskVerifyf(strlen(queryParams) < sizeof(m_QParams),
                        "Query params '%s' is too long - max is %" SIZETFMT "u",
                        queryParams,
                        sizeof(m_QParams)-1))
    {
        return false;
    }

    if(!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        netTaskError("Invalid LocalGamerIndex: %d", localGamerIndex);
        return false;
    }

    m_LocalGamerIndex = localGamerIndex;
    m_ChannelId = channelId;
    m_Results = results;
    m_MaxResults = maxResults;
	m_NumResultsBeforeEarlyOut = numResultsBeforeEarlyOut;
    m_NumResults = numResults;

    if(m_MaxResults > MAX_SESSIONS_TO_FIND)
    {
        m_MaxResults = MAX_SESSIONS_TO_FIND;
    }

    *m_NumResults = 0;
    m_State = STATE_FIND_MATCHES;

    netTaskDebug("Find :: ChannelId: %u, MaxResults: %u, NumResultsBeforeEarlyOut: %u", channelId, maxResults, numResultsBeforeEarlyOut);

    safecpy(m_QName, queryName);
    safecpy(m_QParams, queryParams);
    return true;
}

void
rlSessionFindSocialTask::OnCancel()
{
	if(m_MyStatus.Pending())
	{
		switch(m_State)
		{
		case STATE_FIND_MATCHES:
		case STATE_FINDING_MATCHES:
            netTaskDebug("OnCancel: Cancelling Presence Query...");
            rlPresence::CancelQuery(&m_MyStatus);
			break;
		case STATE_QUERY_DETAIL:
		case STATE_QUERYING_DETAIL:
            netTaskDebug("OnCancel: Cancelling QueryDetails...");
            rlSessionManager::Cancel(&m_MyStatus);
			break;
		}
	}
}

netTaskStatus
rlSessionFindSocialTask::OnUpdate(int* /* resultCode */)
{
	if(WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

    switch(m_State)
    {
    case STATE_FIND_MATCHES:
        netTaskDebug("FindMatches: Matching on '%s': '%s'...", m_QName, m_QParams);
        if(rlPresence::Query(m_LocalGamerIndex,
							 m_QName,
							 m_QParams,
							 0,
							 m_MaxResults,
							 m_QRecordsBuf,
							 sizeof(m_QRecordsBuf),
							 m_QRecords,
							 &m_NumQRecordsRetrieved,
							 &m_NumQRecords,
							 &m_MyStatus))
        {
            netTaskDebug("FindMatches: Finding...");
            m_State = STATE_FINDING_MATCHES;
        }
        else
        {
            netTaskError("FindMatches: rlPresence::Query failed!");
            return NET_TASKSTATUS_FAILED;
        }
        break;

    case STATE_FINDING_MATCHES:
        if(m_MyStatus.Succeeded())
        {
            netTaskDebug("FindingMatches: Retrieved %d results", m_NumQRecords);
            if(ParseResults())
            {
                netTaskDebug("FindingMatches: Parsed Results, Querying Details...");
                m_State = STATE_QUERY_DETAIL;
            }
            else
            {
                netTaskError("FindingMatches: Error parsing results");
                return NET_TASKSTATUS_FAILED;
            }
        }
        else if(!m_MyStatus.Pending())
        {
            netTaskError("FindingMatches: Presence Query failed!");
            return NET_TASKSTATUS_FAILED;
        }
        break;

    case STATE_QUERY_DETAIL:
		if(m_NumSessionInfos > 0)
        {
            netTaskDebug("QueryDetail: Querying details for %d sessions...", m_NumSessionInfos);
            if(rlSessionManager::QueryDetail(RL_NETMODE_ONLINE,
                                                m_ChannelId,
												0,
												0, 
												m_NumResultsBeforeEarlyOut,
												false,
                                                m_SessionInfos,
                                                m_NumSessionInfos,
                                                m_Results,
                                                m_NumResults,
                                                &m_MyStatus))
            {
                netTaskDebug("QueryDetail: Querying Detail");
                m_State = STATE_QUERYING_DETAIL;
            }
            else
            {
                netTaskError("QueryDetail: rlSessionManager::QueryDetail Failed!");
                return NET_TASKSTATUS_FAILED;
            }
        }
        else
        {
            netTaskDebug("QueryDetails: No sessions found");
            return NET_TASKSTATUS_SUCCEEDED;
        }
        break;

    case STATE_QUERYING_DETAIL:
        if(m_MyStatus.Succeeded())
        {
            netTaskDebug("QueryingDetail: Succeeded");
            return NET_TASKSTATUS_SUCCEEDED;
        }
        else if(!m_MyStatus.Pending())
        {
            netTaskError("QueryingDetail: Failed");
            return NET_TASKSTATUS_FAILED;
        }
        break;
    }

    return NET_TASKSTATUS_PENDING;
}

//private:

bool
rlSessionFindSocialTask::ParseResults()
{
    bool success = false;
    if(WasCanceled())
    {
        success = true;
    }
    else
    {
        //Randomize the records
        std::random_shuffle(&m_QRecords[0], &m_QRecords[m_NumQRecords]);

        m_NumSessionInfos = 0;

        for(int i = 0; i < (int) m_NumQRecords && m_NumSessionInfos < m_MaxResults; ++i)
        {
            RsonReader rr;
            if(!rr.Init(m_QRecords[i], RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE))
            {
                netTaskError("ParseResults: Error parsing '%s'", m_QRecords[i]);
            }
            else
            {
                rlSessionInfo sinfo;
                char buf[rlSessionInfo::TO_STRING_BUFFER_SIZE];
                if(!rr.ReadString("gsinfo", buf))
                {
                    netTaskError("ParseResults: Error parsing '%s' - no 'gsinfo'", m_QRecords[i]);
                }
                else if(0 == strlen(buf))
                {
                    netTaskError("ParseResults: Empty session info");
                }
				// fail silently, this can pull data socially and cross version
                else if(!sinfo.FromString(buf, NULL, true))
                {
                    netTaskError("ParseResults: Invalid session info '%s'", buf);
                }
				else if(sinfo.GetHostPeerAddress().IsLocal())
				{
					netTaskError("ParseResults: Local session info 0x%016" I64FMT "x", sinfo.GetToken().m_Value);
				}
				else if(rlSessionManager::IsInSession(sinfo))
				{
					netTaskError("ParseResults: Already in session 0x%016" I64FMT "x", sinfo.GetToken().m_Value);
				}
                else
                {
					netTaskDebug("ParseResults: Found session: 0x%016" I64FMT "x", sinfo.GetToken().m_Value);

                    //Check for duplicates
                    bool haveIt = false;
                    for(int j = 0; j < (int)m_NumSessionInfos; ++j)
                    {
                        if(m_SessionInfos[j] == sinfo)
                        {
                            haveIt = true;
                            break;
                        }
                    }

                    if(!haveIt)
                    {
                        m_SessionInfos[m_NumSessionInfos] = sinfo;
                        ++m_NumSessionInfos;
                    }
                }
            }
        }

        success = true;
    }

    return success;
}

///////////////////////////////////////////////////////////////////////////////
// rlSessionFinder
///////////////////////////////////////////////////////////////////////////////
bool
rlSessionFinder::Find(const int localGamerIndex,
                        const rlNetworkMode netMode,
                        const unsigned channelId,
                        const int numGamers,
                        const rlMatchingFilter& filter,
                        rlSessionDetail* results,
                        const unsigned maxResults,
						const unsigned numResultsBeforeEarlyOut,
                        unsigned* numResults,
                        netStatus* status)
{
    bool success = false;

    rlAssert(results && maxResults > 0);
    rlAssert(filter.IsValid());

    rlDebug("Find");

    rlFindSessionsTask* task = NULL;

    rtry
    {
        rverify(rlRos::HasPrivilege(localGamerIndex,
                                    RLROS_PRIVILEGEID_MULTIPLAYER),
                catchall,
                rlError("Find :: Local gamer %d doesn't have permission to join a session",
                        localGamerIndex));

        rverify(netTask::Create(&task, status),
                catchall,
                rlError("Find :: Error allocating rlFindSessionsTask"));
        rverify(task->Configure(localGamerIndex,
                                  netMode,
                                  channelId,
                                  numGamers,
                                  filter,
                                  rlSessionManager::GetLanMatchingPort(),
                                  results,
                                  maxResults,
								  numResultsBeforeEarlyOut,
                                  numResults),
                catchall,
                rlError("Find :: Error configuring rlFindSessionsTask"));
        rverify(netTask::Run(task),
                catchall,
                rlError("Find :: Error scheduling rlFindSessionsTask"));

        success = true;
    }
    rcatchall
    {
        netTask::Destroy(task);
        //this->FreeResources();
    }

    if(!success)
    {
        status->SetPending(); status->SetFailed();
    }

    return success;
}

bool
rlSessionFinder::FindSocial(const int localGamerIndex,
                            const unsigned channelId,
                            const char* queryName,
                            const char* queryParams,
                            rlSessionDetail* results,
                            const unsigned maxResults,
							const unsigned numResultsBeforeEarlyOut,
                            unsigned* numResults,
                            netStatus* status)
{
	bool success = false;

    rlDebug("FindSocial :: Searching for sessions: %s(%s)...",
            queryName,
            queryParams);

	rlSessionFindSocialTask* task = NULL;
	rtry
	{
        rverify(netTask::Create(&task, status),
                catchall,
                rlError("FindSocial :: Error creating rlSessionFindSocialTask"));

		rverify(task->Configure(localGamerIndex,
                                channelId,
                                queryName,
                                queryParams,
                                results,
                                maxResults,
								numResultsBeforeEarlyOut,
                                numResults),
                    catchall,
                    rlError("FindSocial :: Error configuring rlSessionFindSocialTask"));

        rverify(netTask::Run(task),
                catchall,
                rlError("FindSocial :: Error scheduling rlSessionFindSocialTask"));

		success = true;
	}
	rcatchall
	{
        netTask::Destroy(task);
	}

	return success;
}

}   //namespace rage
