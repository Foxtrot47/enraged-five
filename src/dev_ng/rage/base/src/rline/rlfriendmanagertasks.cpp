// 
// rline/rlfriendmanagertasks.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlfriendmanagertasks.h"

#include "diag/seh.h"
#include "rline/rlfriendsmanager.h"

namespace rage
{
	PARAM(nofriends, "Cause the friends list initialization task to fail.");

	RAGE_DEFINE_SUBCHANNEL(rline, friendtasks)
	#undef __rage_channel
	#define __rage_channel rline_friendtasks

	//////////////////////////////////////////////////////////////////////////
	// Friends Sorting by Id
	//////////////////////////////////////////////////////////////////////////
	int rlFriendsManagerTask::SortFriendsById(rlFriendsReference* const* lhs, rlFriendsReference* const* rhs)
	{
		if((*lhs)->m_Id < (*rhs)->m_Id)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}

#if RSG_DURANGO

	rlFriendsMgrInitializationTask::rlFriendsMgrInitializationTask()
		: m_State(STATE_GET_FRIENDS)
		, m_TotalFriendsPtr(NULL)
		, m_FriendRefsByStatus(NULL)
		, m_FriendRefsById(NULL)
	{

	}

	bool rlFriendsMgrInitializationTask::Configure(const int localGamerIndex, atArray<rlFriendsReference*>* friendsRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, unsigned * totalFriendsPtr)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(totalFriendsPtr, catchall, );
			m_State = STATE_GET_FRIENDS;
			m_LocalGamerIndex = localGamerIndex;
			m_FriendRefsByStatus = friendsRefsByStatus;
			m_FriendRefsById = friendRefsById;
			m_TotalFriendsPtr = totalFriendsPtr;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlFriendsMgrInitializationTask::OnCancel()
	{
		rlFriendsManager::CancelRead(&m_ReadStatus);

		if (rlGetTaskManager() && m_PresenceStatus.Pending())
		{
			rlGetTaskManager()->CancelTask(&m_PresenceStatus);
		}
	}

	netTaskStatus rlFriendsMgrInitializationTask::OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		// -nofriends will return a friends list initialization failure
		if (PARAM_nofriends.Get())
		{
			return NET_TASKSTATUS_FAILED;
		}

		switch(m_State)
		{
		case STATE_GET_FRIENDS:
			{
				if(WasCanceled())									
				{			
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if (GetInitialFriendsList())
				{
					m_State = STATE_GETTING_FRIENDS;
				}
			}
			break;
		case STATE_GETTING_FRIENDS:
			{
				if (!m_ReadStatus.Pending())
				{
					if (m_ReadStatus.Succeeded())
					{
						m_FriendRefsById->QSort(0, -1, &rlFriendsManagerTask::SortFriendsById);
						m_State = STATE_GET_PRESENCE;
					}
					else if (m_ReadStatus.Failed())
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
			}
			break;
		case STATE_GET_PRESENCE:
			{
				if(WasCanceled())									
				{			
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if (GetInitialFriendPresence())
				{
					m_State = STATE_GETTING_PRESENCE;
				}
			}
			break;
		case STATE_GETTING_PRESENCE:
			{
				if (!m_PresenceStatus.Pending())
				{
					if (m_PresenceStatus.Succeeded())
					{
						// Larger friends lists must rely on manual updates, too many to connect to.
						if (m_FriendRefsById->GetCount() < IRealTimeActivity::MAX_SUBSCRIPTIONS_BATCH_SIZE)
						{
							m_State = STATE_SUBSCRIBE;
						}
						else
						{
							status = NET_TASKSTATUS_SUCCEEDED;
						}
					}
					else if (m_PresenceStatus.Failed())
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
			}
			break;
		case STATE_SUBSCRIBE:
			{
				if(WasCanceled())									
				{			
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if (SubscribeToFriends())
				{
					status = NET_TASKSTATUS_SUCCEEDED;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		default:
			break;
		}

		if (status != NET_TASKSTATUS_PENDING)
		{
			Complete(status);
		}

		return status;
	}

	void rlFriendsMgrInitializationTask::Complete(netTaskStatus status)
	{
		if (!WasCanceled() && status == NET_TASKSTATUS_SUCCEEDED)
		{
			*m_TotalFriendsPtr = m_FriendRefsById->GetCount();
		}
	}

	bool rlFriendsMgrInitializationTask::GetInitialFriendsList()
	{
		return g_rlXbl.GetPlayerManager()->GetSocialRelationships(m_LocalGamerIndex, m_FriendRefsByStatus, m_FriendRefsById, &m_ReadStatus);
	}

	bool rlFriendsMgrInitializationTask::GetInitialFriendPresence()
	{
		return g_rlXbl.GetPlayerManager()->GetPeoplePresence(m_LocalGamerIndex, m_FriendRefsByStatus, m_FriendRefsById, &m_PresenceStatus);
	}

	bool rlFriendsMgrInitializationTask::SubscribeToFriends()
	{
		return g_rlXbl.GetRealtimeActivityManager()->SubscribeToFriendsPresence(m_LocalGamerIndex, m_FriendRefsById);
	}

#elif RSG_ORBIS

rlFriendsMgrInitializationTask::rlFriendsMgrInitializationTask()
	: m_State(STATE_GET_FRIENDS)
	, m_CurrentStartIndex(0)
	, m_FriendRefsById(NULL)
{

}

bool rlFriendsMgrInitializationTask::Configure(const int localGamerIndex, bool bSubscribe, atArray<rlFriendsReference*>* friendRefsById, rlFriendsPage* initialPage)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(friendRefsById, catchall, );
		rverify(initialPage, catchall, );

		m_State = STATE_GET_FRIENDS;
		m_LocalGamerIndex = localGamerIndex;

		m_InitialPagePtr = initialPage;
		m_FriendRefsById = friendRefsById;
		m_CurrentStartIndex = 0;

		m_bSubscribe = bSubscribe;

		m_FriendPage.Init(initialPage->m_MaxFriends);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlFriendsMgrInitializationTask::OnCancel()
{
	rlFriendsManager::CancelRead(&m_ReadStatus);
}

netTaskStatus rlFriendsMgrInitializationTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_GET_FRIENDS:
		{
			if(WasCanceled())									
			{			
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else if (GetFriends())
			{
				netTaskDebug("Getting friends - Index: %d, Max: %u", m_CurrentStartIndex, m_FriendPage.m_MaxFriends);
				m_State = STATE_GETTING_FRIENDS;
			}
		}
		break;
	case STATE_GETTING_FRIENDS:
		{
			if (!m_ReadStatus.Pending())
			{
				if (m_ReadStatus.Canceled() || WasCanceled())
				{
					status = NET_TASKSTATUS_FAILED;
				}
				else if (m_ReadStatus.Succeeded())
				{
					unsigned totalFriends = rlFriendsManager::GetFriendReadResultCount();
					m_CurrentStartIndex += m_FriendPage.m_MaxFriends;

					netTaskDebug("Adding friends - Index: %d, Max: %u, Total: %u", m_CurrentStartIndex, m_FriendPage.m_MaxFriends, totalFriends);

					// if first traversal through, reserve space for our friends and copy to the initial page pointer
					if (m_CurrentStartIndex == m_FriendPage.m_MaxFriends)
					{
						m_FriendRefsById->Reset(true);
						m_FriendRefsById->Reserve(totalFriends);
						m_InitialPagePtr->m_NumFriends = m_FriendPage.m_NumFriends;
						for (int i = 0; i < m_FriendPage.m_NumFriends; i++)
						{
							m_InitialPagePtr->m_Friends[i] = m_FriendPage.m_Friends[i];
							AddFriend(m_FriendPage.m_Friends[i].GetReference());
						}
					}
					else
					{
						for (int i = 0; i < m_FriendPage.m_NumFriends; i++)
						{
							AddFriend(m_FriendPage.m_Friends[i].GetReference());
						}
					}

					m_FriendPage.ClearFriendsArray();
					m_FriendPage.m_StartIndex = m_CurrentStartIndex;

					if (m_CurrentStartIndex >= totalFriends)
					{
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						m_State = STATE_GET_FRIENDS;
					}
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
			}
		}
		break;
	default:
		break;
	}

	if (status != NET_TASKSTATUS_PENDING)
	{
		Complete(status);
	}

	return status;
}

void rlFriendsMgrInitializationTask::Complete(netTaskStatus status)
{
	m_FriendPage.Shutdown();
	if (!WasCanceled() && status == NET_TASKSTATUS_SUCCEEDED)
	{
		m_FriendRefsById->QSort(0, -1, &rlFriendsManagerTask::SortFriendsById);

		if (m_bSubscribe)
		{
			g_rlNp.GetWebAPI().RegisterPushEventCallbacks(m_LocalGamerIndex, false);
		}
	}
}

bool rlFriendsMgrInitializationTask::GetFriends()
{
	int friendFlags = rlFriendsReader::FRIENDS_ALL | rlFriendsReader::FRIENDS_PRESORT_ONLINE | rlFriendsReader::FRIENDS_PRESORT_ID;
	return rlFriendsManager::GetFriends(m_LocalGamerIndex, &m_FriendPage, friendFlags, &m_ReadStatus);
}

bool rlFriendsMgrInitializationTask::AddFriend(const rlFriendsReference& ref)
{
	if(HasFriend(ref))
	{
		netTaskDebug("Not adding friend: %s - Already added", ref.m_Id.m_OnlineId.data);
		return false;
	}

	if(m_FriendRefsById->GetCount() >= m_FriendRefsById->GetCapacity())
	{
		netTaskDebug("Not adding friend: %s - No space", ref.m_Id.m_OnlineId.data);
		return false;
	}

	rlFriendsReference* newRef;
	newRef = RL_ALLOCATE_T(rlFriendsMgrInitializationTask, rlFriendsReference);
	if (!newRef)
	{
		netTaskDebug("Not adding friend: %s - Failed to allocate friend reference", ref.m_Id.m_OnlineId.data);
		return false;
	}

	*newRef = ref;

	m_FriendRefsById->Push(newRef);
	netTaskDebug("[%d] Adding friend: %s", m_FriendRefsById->GetCount(), newRef->m_Id.m_OnlineId.data);

	return true; 
}

bool rlFriendsMgrInitializationTask::HasFriend(const rlFriendsReference& ref)
{
	int count = m_FriendRefsById->GetCount();
	for(int i = 0; i < count; i++)
	{
		rlFriendsReference* myRef = m_FriendRefsById->GetElements()[i];
		if(myRef && (*myRef == ref))
			return true;
	}
	return false;
}

#endif

}   //namespace rage
