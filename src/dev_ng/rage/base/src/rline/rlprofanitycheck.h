// 
// rline/rlprofanitycheck.h
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLPROFANITYCHECK_H
#define RLPROFANITYCHECK_H

namespace rage
{

class netStatus;
class parTreeNode;

class rlProfanityCheck
{
public:
     rlProfanityCheck() {;}
    ~rlProfanityCheck() {;}

    //PURPOSE
    //  Begins profanity check of a text.
	static bool ProfanityCheck(const int localGamerIndex
								,const char* text
								,char* profaneWord
								,unsigned profaneWordBufferSize
                                ,bool* passedProfanityCheck
								,unsigned* numProfaneWord
								,bool ugcCheck
								,netStatus* status);

    //PURPOSE
    //  Helper function, extracts the profane word from the XML response when the check failed
    // NOTE: even if the function returns true, the buffer might not be modified
    static bool ExtractProfaneWord(const parTreeNode* node, char* profaneWordBuffer, int profaneWordBufferLength);
};

}   //namespace rage


#endif //RLPROFANITYCHECK_H
