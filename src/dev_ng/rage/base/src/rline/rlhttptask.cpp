// 
// rline/rlHttpTask.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlhttptask.h"
#include "data/base64.h"
#include "data/sax.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "net/nethardware.h"
#include "net/http.h"
#include "rldiag.h"
#include "rline/ros/rlros.h"
#include "parser/manager.h"
#include "system/timer.h"
#include "system/nelem.h"

namespace rage
{
RAGE_DEFINE_SUBCHANNEL(rline, http);
#undef __rage_channel
#define __rage_channel rline_http

#define DEFAULT_TIMEOUT_SECS 30

extern sysMemAllocator* g_rlAllocator;

////////////////////////////////////////////////////////////////////////////////
// rlHttpTaskHelper
////////////////////////////////////////////////////////////////////////////////
bool 
rlHttpTaskHelper::ReadBinaryLen(const parTreeNode* node, 
                                const char* element, 
                                const char* attribute, 
                                unsigned* len)
{
    rlAssert(node && len);

    *len = 0;

    rtry
    {
        const parTreeNode* n = element ? node->FindChildWithNameIgnoreNs(element) : node;            
        rverify(n, catchall, );

        const char* val = NULL;

        if(attribute)
        {
            const parAttribute *attr = n->GetElement().FindAttributeAnyCase(attribute);           
            rverify(attr, catchall, rlError("Could not find attribute %s", attribute));
            val = attr->GetStringValue();
        }
        else
        {
            val = n->GetData();                    
        }

        rverify(val, catchall, );

        *len = datBase64::GetMaxDecodedSize(val);

        return true;
    }
    rcatchall
    {
        *len = 0;
        return false;
    }
}

bool 
rlHttpTaskHelper::ReadBinary(const parTreeNode* node, 
                             const char* element,
                             const char* attribute,
                             u8* buf,
                             const unsigned bufMaxLen,
                             unsigned* bufLen)
{
    rlAssert(node && buf && bufMaxLen && bufLen);

    *bufLen = 0;

    rtry
    {
        const parTreeNode* n = element ? node->FindChildWithNameIgnoreNs(element) : node;            
        rcheck(n, catchall, rlDebug("Could not find element or node"));

        const char* val = NULL;

        if(attribute)
        {
            const parAttribute *attr = n->GetElement().FindAttributeAnyCase(attribute);           
            rcheck(attr, catchall, rlDebug("Could not find attribute %s", attribute));
            val = attr->GetStringValue();
        }
        else
        {
            val = n->GetData();                    
        }

        rverify(val, catchall, );

        unsigned valMaxLen = datBase64::GetMaxDecodedSize(val);

        rverify(valMaxLen <= bufMaxLen,
                catchall,
                rlError("Buffer is not large enough (%u bytes, need %u bytes)", bufMaxLen, valMaxLen));

        rverify(datBase64::Decode(val, bufMaxLen, buf, bufLen),
                catchall,
                rlError("Failed to decode %s%s%s", 
                        element ? element : "",
                        attribute ? ":" : "",
                        attribute ? attribute : ""));

        return true;
    }
    rcatchall
    {
        *bufLen = 0;
        return false;
    }
}

const char* 
rlHttpTaskHelper::ReadString(const parTreeNode* node, 
                             const char* element,
                             const char* attribute)
{
    const char* result = NULL;

    if(node)
    {
        const parTreeNode* n;
        if(element)
        {
            if(parUtils::StringEquals(element, node->GetElement().GetNameNoNs(), true))
            {
                n = node;
            }
            else
            {
                n = node->FindChildWithNameIgnoreNs(element);
            }
        }
        else
        {
            n = node;
        }

        if(n)
        {
            if(attribute)
            {
                const parAttribute *attr = n->GetElement().FindAttributeAnyCase(attribute);
                if(attr)
                {
                    result = attr->GetStringValue();
                }
            }
            else
            {
                result = n->GetData();
            }
        }
    }

    return result;
}

bool 
rlHttpTaskHelper::ReadBool(bool &val, 
                           const parTreeNode* node, 
                           const char* element,
                           const char* attribute)
{
    int intVal = -1;
    
    //See if the value is in integer form.
    if(ReadInt(intVal, node, element, attribute))
    {
        if(intVal == 0 || intVal == 1)
        {
            val = (intVal == 1) ? true : false;
            return true;
        }
    }

    //See if the value is in string form.
    else
    {
        const char* strVal = ReadString(node, element, attribute);
        if(strVal)
        {
            if(!stricmp(strVal, "true"))
            {
                val = true;
                return true;
            }
            else if(!stricmp(strVal, "false"))
            {
                val = false;
                return true;
            }
        }
    }
    
    return false;
}

bool 
rlHttpTaskHelper::ReadFloat(float &val, 
                            const parTreeNode* node, 
                            const char* element,
                            const char* attribute)
{
    const char* str = ReadString(node, element, attribute);
    if(str) return (1 == sscanf(str, "%f", &val));
    return false;
}

bool 
rlHttpTaskHelper::ReadInt(int &val, 
                          const parTreeNode* node, 
                          const char* element,
                          const char* attribute)
{
    const char* str = ReadString(node, element, attribute);
    if(str) return (1 == sscanf(str, "%d", &val));
    return false;
}

bool 
rlHttpTaskHelper::ReadInt64(s64 &val, 
                            const parTreeNode* node, 
                            const char* element,
                            const char* attribute)
{
    const char* str = ReadString(node, element, attribute);
    if(str) return (1 == sscanf(str, "%" I64FMT "d", &val));
    return false;
}

bool
rlHttpTaskHelper::ReadUInt64(u64 &val,
                             const parTreeNode* node,
							 const char* element,
							 const char* attribute)
{
    const char* str = ReadString(node, element, attribute);
    if(str) return (1 == sscanf(str, "%" I64FMT "u", &val));
    return false;
}


bool 
rlHttpTaskHelper::ReadUns(unsigned &val, 
                          const parTreeNode* node, 
                          const char* element,
                          const char* attribute)
{
    const char* str = ReadString(node, element, attribute);
    if(str) return (1 == sscanf(str, "%u", &val));
    return false;
}

////////////////////////////////////////////////////////////////////////////////
// rlHttpTask
////////////////////////////////////////////////////////////////////////////////
rlHttpTask::rlHttpTask(sysMemAllocator* allocator, u8* bounceBuffer, const unsigned sizeofBounceBuffer)
: m_StartTime(0)
, m_SaxReader(NULL)
, m_State(STATE_RECEIVING)
, m_AddingListParam(false)
, m_TimeOutSecs(DEFAULT_TIMEOUT_SECS)
, m_bHasResponseDevice(false)
{
	m_MultipartBoundary[0] = '\0';

    rlAssert((bounceBuffer == NULL && sizeofBounceBuffer == 0) 
        || (bounceBuffer != NULL && sizeofBounceBuffer > 0));

    m_GrowBuffer.Init(allocator ? allocator : g_rlAllocator, datGrowBuffer::NULL_TERMINATE);
    
    if (bounceBuffer != NULL)
    {
        m_HttpRequest.Init(allocator ? allocator : g_rlAllocator, nullptr, bounceBuffer, sizeofBounceBuffer);
    }
    else
    {
        m_HttpRequest.Init(allocator ? allocator : g_rlAllocator, nullptr);
    }
}

rlHttpTask::~rlHttpTask()
{
    if(m_HttpRequest.Pending())
    {
        m_HttpRequest.Cancel();
    }
}

const char*
rlHttpTask::BuildUrl(char* url, const unsigned sizeofUrl) const
{
    rtry
    {
        char hostnameBuf[128];
        const char* baseUrl = GetUrlHostName(hostnameBuf);
        rcheck(baseUrl, catchall, rlTaskError("Could not get base URL; most likely environment is unknown atm"));

        char svcPath[256];
        rverify(GetServicePath(svcPath, sizeof(svcPath)),
                catchall,
                rlTaskError("Failed to retrieve service path"));

        const unsigned urlLen = ustrlen("https://") + ustrlen(baseUrl) + ustrlen("/") + ustrlen(svcPath) + 1;

        rverify(sizeofUrl > urlLen,
                catchall,
                rlTaskError("URL buffer is too small - size is %u, required size is %u",
                            sizeofUrl-1, urlLen));

        formatf(url, sizeofUrl, "%s%s/%s", UseHttps() ? "https://" : "http://", baseUrl, svcPath);
    }
    rcatchall
    {
        url[0] = '\0';
    }

    return url;
}

bool 
rlHttpTask::AppendUserAgentString(const char* userAgentString)
{
    return m_HttpRequest.AppendUserAgentString(userAgentString);
}

bool rlHttpTask::StartMultipartFormData(const char* boundary)
{
	rtry
	{
		rverify(!m_HttpRequest.HasRequestHeader("Content-Type"), catchall, rlTaskError("Content-Type already set for request"));
		rverify(m_HttpRequest.GetHttpVerb() == NET_HTTP_VERB_POST, catchall, rlTaskError("Invalid verb for multipart form data"));

		if (boundary)
		{
			// validate boundary length if specified
			rverify(StringLength(boundary) <= MAX_MULTIPART_BOUNDARY_LENGTH, catchall, );
			safecpy(m_MultipartBoundary, boundary);
		}
		else
		{
			// otherwise, use system time
			formatf(m_MultipartBoundary, "--------------------%" I64FMT "x", sysTimer::GetSystemMsTime());
		}

		// create the content type buf
		char contentTypeBuf[MAX_MULTIPART_BOUNDARY_BUF_SIZE + 64];
		formatf(contentTypeBuf, "multipart/form-data; boundary=%s", m_MultipartBoundary);

		rlTaskDebug1("Setting content-type to multi part boundary using '%s'", m_MultipartBoundary);
		return m_HttpRequest.AddRequestHeaderValue("Content-Type", contentTypeBuf);
	}
	rcatchall
	{
		return false;
	}
}


bool
rlHttpTask::AddMultipartStringParameter(const char* name, const char* value, const unsigned minLen, const unsigned maxLen)
{
    bool success = false;

    rtry
    {
        unsigned len = 0;
        if(value)
        {
            len = ustrlen(value);
        }

		rverify(len >= minLen, catchall,);
		rverify(len <= maxLen, catchall,);

		rverify(m_HttpRequest.AppendContent("--", 2),catchall,);
		rverify(m_HttpRequest.AppendContent(m_MultipartBoundary, ustrlen(m_MultipartBoundary)),catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		char buf[256];
		formatf(buf, "Content-Disposition: form-data; name=\"%s\"\r\n\r\n", name);
		rverify(m_HttpRequest.AppendContent(buf, ustrlen(buf)),catchall,);

		rverify(m_HttpRequest.AppendContent(value,len), catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool rlHttpTask::AddMultipartIntParameter(const char* name, const s64 value, const s64 minVal, const s64 maxVal)
{
	bool success = false;

	rtry
	{
		rverify(value >= minVal, catchall,);
		rverify(value <= maxVal, catchall,);

		rverify(m_HttpRequest.AppendContent("--", 2),catchall,);
		rverify(m_HttpRequest.AppendContent(m_MultipartBoundary, ustrlen(m_MultipartBoundary)),catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		char buf[256];
		formatf(buf, "Content-Disposition: form-data; name=\"%s\"\r\n\r\n", name);
		rverify(m_HttpRequest.AppendContent(buf, ustrlen(buf)),catchall,);

		char tmp[64];
		formatf(buf, COUNTOF(tmp), "%" I64FMT "d", value);
		rverify(m_HttpRequest.AppendContent(tmp,istrlen(tmp)), catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		success = true;
	}
	rcatchall
	{
	}
	return success;
}

bool rlHttpTask::AddMultipartUnsParameter(const char* name, const u64 value)
{
	bool success = false;

	rtry
	{
		rverify(m_HttpRequest.AppendContent("--", 2),catchall,);
		rverify(m_HttpRequest.AppendContent(m_MultipartBoundary, ustrlen(m_MultipartBoundary)),catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		char buf[256];
		formatf(buf, "Content-Disposition: form-data; name=\"%s\"\r\n\r\n", name);
		rverify(m_HttpRequest.AppendContent(buf, ustrlen(buf)),catchall,);

		char tmp[64];
		formatf(tmp, COUNTOF(tmp), "%" I64FMT "u", value);
		rverify(m_HttpRequest.AppendContent(tmp,istrlen(tmp)), catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool rlHttpTask::AddMultipartDoubleParameter(const char* name, const double value)
{
	bool success = false;

	rtry
	{
		rverify(m_HttpRequest.AppendContent("--", 2),catchall,);
		rverify(m_HttpRequest.AppendContent(m_MultipartBoundary, ustrlen(m_MultipartBoundary)),catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		char buf[256];
		formatf(buf, "Content-Disposition: form-data; name=\"%s\"\r\n\r\n", name);
		rverify(m_HttpRequest.AppendContent(buf, ustrlen(buf)),catchall,);

		char tmp[64];
		formatf(tmp, COUNTOF(tmp), "%#.*g", netHttpRequest::DEFAULT_FLOAT_PRECISION, value);
		rverify(m_HttpRequest.AppendContent(tmp,istrlen(tmp)), catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		success = true;
	}
		rcatchall
	{
	}

	return success;
}
bool rlHttpTask::AddMultipartBinaryParameter(const char* name, const char* filename, const u8* data, const unsigned size)
{
	bool success = false;

	rtry
	{
		rverify(m_HttpRequest.AppendContent("--", 2),catchall,);
		rverify(m_HttpRequest.AppendContent(m_MultipartBoundary, ustrlen(m_MultipartBoundary)),catchall,);
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		char buf[512];

		if (filename)
		{
			formatf(buf, "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", name, filename);
		}
		else
		{
			formatf(buf, "Content-Disposition: form-data; name=\"%s\"\r\n", name);
		}
		
		rverify(m_HttpRequest.AppendContent(buf, ustrlen(buf)),catchall,);

		const char* octetStream = "Content-Type: application/octet-stream\r\n\r\n";
		rverify(m_HttpRequest.AppendContent(octetStream,istrlen(octetStream)), catchall,);
		
#if RSG_DURANGO
		// Durango's IXHR2 doesn't support chunked encoding anyway
		rverify(m_HttpRequest.AppendContent(data, size), catchall,);
#else
		const int MULTI_PART_CHUNK_SIZE = 32 * 1024;
		for (unsigned offset = 0; offset < size; offset += MULTI_PART_CHUNK_SIZE)
		{
			const u8* chunkStart = data + offset;
			
			unsigned bytesRemaining = size - offset;
			if (bytesRemaining < MULTI_PART_CHUNK_SIZE)
			{
				rverify(m_HttpRequest.AppendContent(chunkStart, bytesRemaining), catchall,);
			}
			else
			{
				rverify(m_HttpRequest.AppendContent(chunkStart, MULTI_PART_CHUNK_SIZE), catchall,);
			}
		}
#endif
		
		rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool rlHttpTask::EndMultipartFormData()
{
	rtry
	{
		rverify(m_MultipartBoundary[0] != '\0', catchall, rlTaskError("Missing multipart boundary"));

		char endOfContent[MAX_MULTIPART_BOUNDARY_BUF_SIZE + 64];
		formatf(endOfContent, "--%s--\r\n", m_MultipartBoundary);

		rverify(m_HttpRequest.AppendContent(endOfContent, ustrlen(endOfContent)), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
rlHttpTask::AddStringParameter(const char* name,
                               const char* value,
                               const bool required,
                               const unsigned minLen,
                               const unsigned maxLen)
{
    bool success = false;

    rtry
    {
        /*
        rules:

        [required]	[minLen]	[len can be 0]		[add parameter]
        false		0			yes					len > 0
        false		> 0			yes					len > 0
        true		0			yes					yes
        true		> 0			no					yes

        this allows for required parameters that can have empty values (i.e. password='')
        and also ensures that non-required parameters still have a min length if specified
        (i.e. not passing a phone argument might be ok, but phone='3' might be invalid because it's too short)
        lastly, if an empty or NULL value is passed for a non-required parameter,
        then we omit the parameter completely
        */

        unsigned len = 0;
        if(value)
        {
            len = ustrlen(value);
        }

        const bool omit = (len == 0) && (required == false);

        if(!omit)
        {
            rverify(len >= minLen, catchall,);
            rverify(len <= maxLen, catchall,);

            rverify(m_HttpRequest.AddFormParam(name, value ? value : ""),catchall,);
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool
rlHttpTask::AddIntParameter(const char* name,
                            const s64 value,
                            const s64 minVal,
                            const s64 maxVal)
{
    bool success = false;
    rtry
    {
        rverify(value >= minVal, catchall,);
        rverify(value <= maxVal, catchall,);
        rverify(m_HttpRequest.AddIntFormParam(name, value),catchall,);
        success = true;
    }
    rcatchall
    {
    }
    return success;
}

bool
rlHttpTask::AddUnsParameter(const char* name, const u64 value)
{
    return m_HttpRequest.AddUnsFormParam(name, value);
}

bool
rlHttpTask::AddDoubleParameter(const char* name,
                            const double value,
                            const double minVal,
                            const double maxVal)
{
    rtry
    {
        rverify(value >= minVal, catchall,);
        rverify(value <= maxVal, catchall,);
        rverify(m_HttpRequest.AddDoubleFormParam(name, value),catchall,);

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlHttpTask::AddBinaryParameter(const char* name, 
                               const void* data, 
                               const unsigned len)
{
    return m_HttpRequest.AddBinaryFormParam(name, data, len);
}

bool
rlHttpTask::AppendContentUrlEncoded(const void* data, const unsigned len)
{
    return rlVerify(m_HttpRequest.AppendContentUrlEncoded(data, len));
}

bool
rlHttpTask::AppendContent(const void* data, const unsigned len)
{
    return rlVerify(m_HttpRequest.AppendContent(data, len));
}

bool
rlHttpTask::BeginListParameter(const char* name)
{
    return m_HttpRequest.BeginListFormParam(name);
}

bool
rlHttpTask::AddListParamStringValue(const char* value)
{
    return m_HttpRequest.AddListFormParamValue(value);
}

bool
rlHttpTask::AddListParamIntValue(const s64 value, const s64 minVal, const s64 maxVal)
{
    return rlVerify(value >= minVal)
            && rlVerify(value <= maxVal)
            && m_HttpRequest.AddListFormParamIntValue(value);
}

bool
rlHttpTask::AddListParamUnsValue(const u64 value)
{
    return m_HttpRequest.AddListFormParamUnsValue(value);
}

bool
rlHttpTask::AddListParamDoubleValue(const double value, const double minVal, const double maxVal)
{
    return rlVerify(value >= minVal)
            && rlVerify(value <= maxVal)
            && m_HttpRequest.AddListFormParamDoubleValue(value);
}

bool
rlHttpTask::AddRequestHeaderValue(const char* name, const char* value)
{
	return m_HttpRequest.AddRequestHeaderValue(name, value);
}

bool 
rlHttpTask::AddQueryParam(const char * name, const char * value, unsigned int length)
{
	return m_HttpRequest.AddQueryParam(name, value, length);
}

void
rlHttpTask::Reset()
{
    rlAssert(!this->IsPending());

    m_StartTime = 0;
    m_HttpRequest.Clear();
    m_GrowBuffer.Clear();
    m_SaxReader = NULL;
    m_State = STATE_RECEIVING;
}

bool
rlHttpTask::Configure()
{
    rlHttpTaskConfig config;

    return Configure(&config);
}

bool
rlHttpTask::Configure(netHttpFilter* filter)
{
    rlHttpTaskConfig config;
    config.m_Filter = filter;

    return Configure(&config);
}

bool
rlHttpTask::Configure(datSaxReader* saxReader, netHttpFilter* filter)
{
    rlHttpTaskConfig config;
    config.m_Filter = filter;
    config.m_SaxReader = saxReader;

    return Configure(&config);
}

bool
rlHttpTask::Configure(const rlHttpTaskConfig* config)
{
    bool success = false;

    rtry
    {
        rverify(!this->IsPending(),catchall,);

        Reset();

        char url[1024];

        BuildUrl(url);
        rlAssert(strlen(url) > 0);

        //If using https, install the SslCtx to use
        if (UseHttps())
        {
            SSL_CTX* const sslCtx = GetSslCtx();
            rverify(sslCtx != nullptr, catchall, );
            m_HttpRequest.SetSslContext(sslCtx);
        }

        rlTaskDebug2("Sending request to: %s...", url);

#if !__NO_OUTPUT
        char contextStr[64];
        formatf(contextStr, "%s[%u]", this->GetTaskName(), this->GetTaskId());
#else
        const char* contextStr = NULL;
#endif

		if(config && config->m_IgnoreProxy)
		{
			m_HttpRequest.SetIgnoreProxy(true);
		}

		if (config && config->m_ResponseDevice != NULL)
		{
			rverify(config->m_ResponseHandle != fiHandleInvalid, catchall, );
			m_bHasResponseDevice = true;
		}

		rverify(config->m_HttpVerb == NET_HTTP_VERB_POST
				|| config->m_HttpVerb == NET_HTTP_VERB_GET
				|| config->m_HttpVerb == NET_HTTP_VERB_PUT, catchall, rlError("Only POST, PUT and GET is currently supported by rlHttpTask"));

		switch(config->m_HttpVerb)
		{
		case NET_HTTP_VERB_POST:
                //POST with no response device, we'll poll the response ourselves
        		rverify(m_HttpRequest.BeginPost(url,
												NULL,   //proxyAddr
												m_TimeOutSecs,
												config->m_ResponseDevice,
												config->m_ResponseHandle,
												contextStr,
												config ? config->m_Filter : NULL,
												&m_MyStatus),
						catchall,
						rlTaskError("Error beginning POST"));
			break;
		case NET_HTTP_VERB_PUT:
			//PUT with no response device, we'll poll the response ourselves
			rverify(m_HttpRequest.BeginPut(url,
											NULL,   //proxyAddr
											m_TimeOutSecs,
											config->m_ResponseDevice,
											config->m_ResponseHandle,
											contextStr,
											config ? config->m_Filter : NULL,
											&m_MyStatus),
											catchall,
											rlTaskError("Error beginning PUT"));
			break;
		case NET_HTTP_VERB_GET:
            //GET with no response device, we'll poll the response ourselves
			rverify(m_HttpRequest.BeginGet(url,
										   NULL,   //proxyAddr
										   m_TimeOutSecs,
										   config->m_ResponseDevice,
										   config->m_ResponseHandle,
										   contextStr,
										   config ? config->m_Filter : NULL,
										   &m_MyStatus),
					catchall,
					rlTaskError("Error beginning GET"));
		default:
			break;
		}

        if(config && config->m_HttpHeaders)
        {
            rverify(m_HttpRequest.AddRequestHeaders(config->m_HttpHeaders),
                    catchall,
                    rlTaskError("Error adding HTTP headers"));
        }

        //If Content-Type is not already specified, default to application/x-www-form-urlencoded.
        if(config->m_AddDefaultContentTypeHeader && !m_HttpRequest.HasRequestHeader("Content-Type"))
        {
            rverify(m_HttpRequest.AddRequestHeaderValue("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"),
                    catchall,
                    rlTaskError("Error adding content type to HTTP request"));
        }

        if(config && config->m_SaxReader)
        {
            config->m_SaxReader->Begin();
        }

        m_SaxReader = config->m_SaxReader;

        success = true;
    }
    rcatchall
    {
        m_HttpRequest.Clear();
    }

    return success;
}

void
rlHttpTask::Start()
{
    rlTaskDebug2("Start");

    rlTaskBase::Start();

    m_StartTime = sysTimer::GetSystemMsTime();

    if(!this->SendRequest())
    {
        this->Finish(FINISH_FAILED, -1);
    }
}

void
rlHttpTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug2("%s (%ums)", FinishString(finishType), sysTimer::GetSystemMsTime()- m_StartTime);

    if(m_HttpRequest.Pending())
    {
        m_HttpRequest.Cancel();
    }

    rlTaskBase::Finish(finishType, resultCode);

    m_AddingListParam = false;
}

void
rlHttpTask::DoCancel()
{
    m_HttpRequest.Cancel();
}

void 
rlHttpTask::Update(const unsigned timeStep)
{
    if(!this->IsActive())
    {
        return;
    }

    rlTaskBase::Update(timeStep);

    m_HttpRequest.Update();

#if !RSG_PC
    // this doesn't work on PC since we have to use a web service in order to become online
    //If we went offline then bail.
    if(!netHardware::IsOnline() && !WasCanceled() && IsCancelable())
    {
        this->Cancel();
    }
#endif

    if(WasCanceled())
    {
        this->Finish(FINISH_CANCELED, -1);
    }
    else
    {
		int httpStatusCode = m_HttpRequest.GetStatusCode();
		bool httpError = httpStatusCode >= 400;

        if(m_SaxReader)
        {
            // Note: We're assuming that it's okay for our writeBuffer to be the same size as our readBuffer. This assumption would be invalid if it were
            // possible to have more written than the size of our read buffer. E.g. if:
            // 1. We had a filter that expanded read data, such as a compression filter
            // 2. The filter itself buffered response data. Previously this was true of rosHttpFilter
            // Alternatively, we could pass in a callback or some kind of stream interface that ReadBody would then write the response into,
            // and that would be responsible for consuming all of the response. In the case of a sax reader, it would simply write directly into
            // the sax parser, and anything that couldn't be parsed yet (hopefully nothing) would be buffered in our grow buffer, similar
            // to what we're doing here after everything is copied over to our writeBuffer. That would also be faster and avoid some unnecessary
            // memory copying into these temporary writeBuffer.
            u8 readBuffer[8192];
            u8 writeBuffer[8192];

            // Compute how much to read. Generally this will be the same size as our writeBuffer. However, if we're already
            // buffering data in our m_GrowBuffer that we previously read but wasn't parsed, then take away that from what
            // we can read
            unsigned amountToRead = COUNTOF(readBuffer);
            //The offset into our writeBuffer to which we should write.
            unsigned writeOffset = 0;

            // Move our grow buffer over to our write buffer so it's in order
            if (m_GrowBuffer.Length() > 0)
            {
                unsigned amountToCopy = Min(amountToRead, m_GrowBuffer.Length());
                sysMemCpy(writeBuffer, m_GrowBuffer.GetBuffer(), amountToCopy);
                writeOffset += amountToCopy;
                m_GrowBuffer.Remove(0, amountToCopy);
                amountToRead -= amountToCopy;

                // If our grow buffer still has anything left in it, then don't bother reading anything,
                // since it would be out of order
                if (m_GrowBuffer.Length() > 0)
                {
                    amountToRead = 0;
                }
            }

            // Now actually read the request response
            unsigned amountReceived;
            m_HttpRequest.ReadBody(readBuffer, amountToRead, &writeBuffer[writeOffset], COUNTOF(writeBuffer) - writeOffset, &amountReceived);

            const char* response = (const char*) writeBuffer;
            const unsigned responseLen = amountReceived+writeOffset;

            if (!httpError && response && responseLen)
            {
                const unsigned numConsumed = m_SaxReader->Parse(response, 0, responseLen);

                // Append whatever to our grow buffer that wasn't consumed...
                // Hopefully nothing...
                // If we fail, then the task fails since we can't buffer the data that we consumed and now
                // must lose
                if (responseLen - numConsumed > 0)
                {
                    rlWarning("Had to buffer %d bytes in grow buffer", responseLen - numConsumed);
                    if (!m_GrowBuffer.AppendOrFail(&writeBuffer[numConsumed], responseLen - numConsumed))
                    {
                        rlError("Failed to append unparsed response");
                        this->Finish(FINISH_FAILED, NET_HTTPSTATUS_CANT_BUFFER_RESPONSE);
                        return;
                    }
                }
            }
        }
		// If we don't have a response device, we must read the body of the http request into our default growbuffer. Internally.
		//	netHttpRequest will call ReadBody if a device is present.
        else if (!m_bHasResponseDevice)
        {
            u8 readBuffer[8192];

            fiHandle outputHandle = m_GrowBuffer.GetFiHandle();
            const fiDevice &outputDevice = *m_GrowBuffer.GetFiDevice();

            // If not using a sax reader, receive directly into our grow buffer
            m_HttpRequest.ReadBody(readBuffer, COUNTOF(readBuffer), outputHandle, outputDevice);
            outputDevice.Flush(outputHandle);
        }

        switch(m_State)
        {
        case STATE_RECEIVING:
            if(m_MyStatus.Succeeded())
            {
                if(!m_SaxReader)
                {
                    const char* response = (const char*)m_GrowBuffer.GetBuffer();

					if(!httpError)
					{
						int resultCode;
						if(this->ProcessResponse(response, resultCode))
						{
							this->Finish(FINISH_SUCCEEDED, resultCode);
						}
						else
						{
							this->Finish(FINISH_FAILED, resultCode);
						}
					}
					else
					{
						this->Finish(FINISH_FAILED, httpStatusCode);
					}
                }
                else
                {
					if(httpError)
					{
						this->Finish(FINISH_FAILED, httpStatusCode);
					}
					else
					{
						m_State = STATE_SAX_PROCESSING_RESPONSE;
					}
                }
            }
            else if(!m_MyStatus.Pending())
            {
                this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            }
            break;
        case STATE_SAX_PROCESSING_RESPONSE:
            rlAssert(m_SaxReader);
            rlAssert(m_MyStatus.Succeeded());
            if(m_SaxReader->Failed())
            {
                rlTaskError("Failed to parse XML response");
                this->Finish(FINISH_FAILED);
            }
            else if(m_GrowBuffer.Length() == 0)
            {
                m_SaxReader->End();
                m_State = STATE_SAX_DONE;
            }
            break;
        case STATE_SAX_DONE:
            //We should get here only if we have a SAX reader.
            rlAssert(m_SaxReader);
            //Wait for the subclass to call Finish().
            break;
        }
    }
}

bool
rlHttpTask::SendRequest()
{
    bool success = false;

    if(rlVerify(m_HttpRequest.Commit()))
    {
        success = true;
    }
    else
    {
        m_HttpRequest.Clear();
        m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
    }

    return success;
}

////////////////////////////////////////////////////////////////////////////////
// rlHttpDownloadTask
////////////////////////////////////////////////////////////////////////////////
bool 
rlHttpDownloadTask::DownloadFile(const char* downloadUrl,
                                 const char* localPath,
                                 rlHttpDownloadTask::OverwriteOptions overwrite,
                                 rlHttpDownloadTask::ProgressCallback progressCallback,
                                 void* callbackData,
                                 netStatus* status)
{
    bool success = false;
    rlHttpDownloadTask* task = NULL;
    rtry
    {
        task = rlGetTaskManager()->CreateTask<rlHttpDownloadTask>();
        rverify(task,catchall,);
        rverify(rlTaskBase::Configure(task, 
                                      downloadUrl,
                                      localPath,
                                      overwrite,
                                      progressCallback,
                                      callbackData,
                                      status),
                catchall,);
        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
        success = true;
    }
    rcatchall
    {
        if(task)
        {
            if(task->IsPending())
            {
                task->Finish(rlTaskBase::FINISH_FAILED, -1);
            }
            else
            {
                status->ForceFailed();
            }
            rlGetTaskManager()->DestroyTask(task);
        }
    }
    return success;
}

bool 
rlHttpDownloadTask::DownloadFile(const char* downloadUrl,
                                 const char* localPath,
                                 rlHttpDownloadTask::OverwriteOptions overwrite,
                                 netStatus* status)
{
    return DownloadFile(downloadUrl, localPath, overwrite, NULL, NULL, status);
}

rlHttpDownloadTask::rlHttpDownloadTask()
: m_State(STATE_INVALID)
, m_StartTime(0)
, m_fiHandle(fiHandleInvalid)
, m_fiDevice(NULL)
, m_DownloadBuffer(NULL)
, m_HaveResponseHeader(false)
, m_HttpRequestInited(false)
, m_CurrentFileSize(0)
, m_ContentFileSize(0)
, m_fiResponseHandle(fiHandleInvalid)
, m_fiResponseDevice(NULL)
, m_DeviceMemoryBuffer(NULL)
{
}

rlHttpDownloadTask::~rlHttpDownloadTask()
{
	Reset();
}

void
rlHttpDownloadTask::Reset()
{
    rlAssert(!this->IsPending());

	if(m_DownloadBuffer)
	{
		g_rlAllocator->Free(m_DownloadBuffer);
		m_DownloadBuffer = NULL;
	}

    m_State = STATE_INVALID;
    m_StartTime = 0;
	m_HttpRequest.Clear();
	m_fiHandle = fiHandleInvalid;
	m_fiDevice = NULL;

	if (m_DeviceMemoryBuffer)
	{
		g_rlAllocator->Free(m_DeviceMemoryBuffer);
		m_DeviceMemoryBuffer = NULL;
	}

	m_fiResponseHandle = fiHandleInvalid;
	m_fiResponseDevice = NULL;
	m_CurrentFileSize = 0;
	m_ContentFileSize = 0;

	m_HaveResponseHeader = false;
	m_HttpRequestInited = false;
}

bool
rlHttpDownloadTask::Configure(const char* downloadUrl, 
                              const char* localPath, 
                              OverwriteOptions overwrite, 
                              ProgressCallback callback, 
                              void* callbackData)
{
    bool success = false;

    rtry
    {
 		m_ProgressCallback = callback;
 		m_ProgressCallbackData = callbackData;

        rverify(STATE_INVALID == m_State || STATE_FINISHED == m_State, catchall,);

		m_Overwrite = overwrite;

        Reset();

		rverify(SetupPaths(downloadUrl, localPath), catchall, );

        success = true;
    }
    rcatchall
    {

	}

    return success;
}

bool 
rlHttpDownloadTask::SetupPaths(const char* downloadUrl, const char* localPath)
{
	bool success = false;

	rtry
	{
		rverify(downloadUrl != NULL, catchall, );
		rverify(strlen(downloadUrl) < sizeof(m_RemotePath), catchall, );
		rverify(strlen(downloadUrl) > strlen("http://"), catchall, );

		// download url can begin with http:// or https://
		rverify(strnicmp(downloadUrl, "http", 4) == 0, catchall, );

		// SECURITY - only support fully qualified paths
		rverify(strstr(downloadUrl, ".\\") == NULL, catchall, );
		rverify(strstr(downloadUrl, "..\\") == NULL, catchall, );
		rverify(strstr(downloadUrl, "./") == NULL, catchall, );
		rverify(strstr(downloadUrl, "../") == NULL, catchall, );

		safecpy(m_RemotePath, downloadUrl);

		m_RemoteFileName = ASSET.FileName(m_RemotePath);
		rverify(m_RemoteFileName != NULL, catchall, );

		char* lastSlash = Max(strrchr(m_RemotePath, '\\'), strrchr(m_RemotePath, '/'));

		if(lastSlash)
		{
			*lastSlash = '\0';
		}

		rverify(localPath != NULL, catchall, );
		rverify(localPath[0] != '\0', catchall, );
		rverify(strlen(localPath) < sizeof(m_LocalPath), catchall, );

		rverify(strstr(localPath, ".\\") == NULL, catchall, );
		rverify(strstr(localPath, "..\\") == NULL, catchall, );
		rverify(strstr(localPath, "./") == NULL, catchall, );
		rverify(strstr(localPath, "../") == NULL, catchall, );

		// these characters are invalid in the Windows filesystem
		char invalidChars[] = {'*' , '?', '<', '>', '|', '"'};
		for(unsigned int i = 0; i < sizeof(invalidChars); i++)
		{
			rverify(strchr(localPath, invalidChars[i]) == NULL, catchall, );
		}

		safecpy(m_LocalPath, localPath);

		m_LocalFileName = ASSET.FileName(m_LocalPath);

		// if localPath doesn't contain a filename (only a directory path),
		// then make the filename the same as the remote filename
		if((m_LocalFileName == NULL) || (m_LocalFileName[0] == '\0'))
		{
			m_LocalFileName = m_RemoteFileName;
		}
		rverify(m_LocalFileName != NULL, catchall, );

		lastSlash = Max(strrchr(m_LocalPath, '\\'), strrchr(m_LocalPath, '/'));

		if(lastSlash)
		{
			*lastSlash = '\0';
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
rlHttpDownloadTask::Start()
{
    rlTaskDebug2("Start");

    rlTaskBase::Start();
    rlAssert(STATE_INVALID == m_State);

    m_StartTime = sysTimer::GetSystemMsTime();

	if (m_Overwrite == RL_HTTP_RESUME)
	{
		m_State = STATE_GET_SERVER_FILE_SIZE;
	}
	else
	{
		m_State = STATE_CHECK_FOR_EXISTING_FILE;
	}
}

void
rlHttpDownloadTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug2("%s (%ums)", FinishString(finishType), sysTimer::GetSystemMsTime() - m_StartTime);

    rlTaskBase::Finish(finishType, resultCode);

    m_State = STATE_FINISHED;
}

void
rlHttpDownloadTask::DoCancel()
{
    if(m_State == STATE_DOWNLOADING)
    {
		if(m_HttpRequestInited)
		{
			m_HttpRequest.Cancel();
		}
    }
}

void 
rlHttpDownloadTask::Update(const unsigned timeStep)
{
    rlTaskBase::Update(timeStep);

	if(m_HttpRequestInited)
	{
		m_HttpRequest.Update();
	}

    switch(m_State)
    {
	case STATE_GET_SERVER_FILE_SIZE:
		if (this->SendFileSizeRequest())
		{
			m_State = STATE_GET_SERVER_FILE_SIZE_RESPONSE;
		}
		else
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_GET_SERVER_FILE_SIZE_RESPONSE:
		if (m_MyStatus.Succeeded())
		{
			unsigned int contentLength = m_HttpRequest.GetResponseContentLength();

			if (contentLength == 0)
			{
				m_State = STATE_FAILED;
			}
			else
			{
				m_ContentFileSize = contentLength;
				m_State = STATE_CHECK_FOR_EXISTING_FILE;
				m_HttpRequest.Clear();
			}
		}
		else if (!m_MyStatus.Pending())
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_CHECK_FOR_EXISTING_FILE:
		if (DoesFileExist(false) && (m_Overwrite == RL_HTTP_RESUME))
		{
			if (ResumeFile())
			{
				m_State = STATE_DOWNLOAD;
			}
			else
			{
				m_State = STATE_FAILED;
			}
		}
		else if(DoesFileExist() && (m_Overwrite == RL_HTTP_OVERWRITE_NEVER))
		{
			this->Finish(FINISH_SUCCEEDED);
		}
		else
		{
			if(_CreateFile())
			{
				m_State = STATE_DOWNLOAD;
			}
			else
			{
				m_State = STATE_FAILED;
			}
		}
		break;

    case STATE_DOWNLOAD:
        if(WasCanceled())
        {
			m_State = STATE_FAILED;
		}
        else if(this->SendRequest())
        {
            m_State = STATE_DOWNLOADING;
        }
        else
        {
			m_State = STATE_FAILED;
		}
        break;

    case STATE_DOWNLOADING:
        if(WasCanceled())
        {
			m_State = STATE_FAILED;
		}
        else if(m_MyStatus.Succeeded())
        {
			CloseFile();

			int httpStatusCode = m_HttpRequest.GetStatusCode();
			if (httpStatusCode >= 400)
			{
				m_State = STATE_FAILED;
			}
			else
			{
				this->Finish(FINISH_SUCCEEDED);
			}
        }
        else if(!m_MyStatus.Pending())
        {
			m_State = STATE_FAILED;
        }
 		else if(m_HaveResponseHeader || (m_HttpRequestInited && m_HttpRequest.HaveResponseHeader()))
 		{
			if(m_HaveResponseHeader == false)
			{
				SetTimeoutMs(0x7FFFFFFF);
				m_HaveResponseHeader = true;
			}

			// Added current file location addition for resumable downloads to take into account what has already been downloaded.
			if(m_ProgressCallback && m_HttpRequestInited)
			{
				m_ProgressCallback(m_HttpRequest.GetResponseContentNumBytesReceived() + m_CurrentFileSize, m_HttpRequest.GetResponseContentLength() + m_CurrentFileSize, m_ProgressCallbackData);
			}
 		}
        break;

	case STATE_FAILED:
		m_HttpRequest.Clear();

		if(DoesFileExist())
		{
			RemoveFile();
		}
		this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
		break;

	case STATE_INVALID:
    default:
        rlAssert(false);
    }
}

bool
rlHttpDownloadTask::SendRequest()
{
    bool success = false;

	rtry
	{
		const int urlLen = istrlen(m_RemotePath) + istrlen("/") + istrlen(m_RemoteFileName) + 1;

		char* url = Alloca(char, urlLen);

		rverify(url, catchall, rlError("Error allocating URL buffer"));

		formatf(url, urlLen, "%s/%s", m_RemotePath, m_RemoteFileName);

		rlTaskDebug2("Sending request to download: %s...", url);

#if !__NO_OUTPUT
        char contextStr[64];
        formatf(contextStr, "%s[%u]", this->GetTaskName(), this->GetTaskId());
#else
        const char* contextStr = NULL;
#endif

		unsigned bufSize = 64 * 1024;
		m_DownloadBuffer = NULL;
		while((m_DownloadBuffer == NULL) && (bufSize >= 1024))
		{
			m_DownloadBuffer = g_rlAllocator->Allocate(bufSize, 16, 0);
			bufSize >>= 1;
		}
		bufSize <<= 1;

		m_HttpRequest.Init(g_rlAllocator, rlRos::GetRosSslContext(), m_DownloadBuffer, bufSize);
		m_HttpRequestInited = true;

		rverify(m_HttpRequest.BeginGet(url,
                                       NULL,   //proxyAddr
                                       DEFAULT_TIMEOUT_SECS,
                                       m_fiDevice,
                                       m_fiHandle,
                                       contextStr,
                                       NULL,
                                       &m_MyStatus),
				catchall,
				rlTaskError("Error beginning GET"));

		if (m_Overwrite == RL_HTTP_RESUME && m_ContentFileSize > 0 && (m_CurrentFileSize < m_ContentFileSize))
		{
			m_HttpRequest.SetByteRange(m_CurrentFileSize, m_ContentFileSize);
		}

        rverify(m_HttpRequest.Commit(),
                catchall,
				rlTaskError("Error committing GET"));

		success = true;
	}
	rcatchall
	{
	}

    return success;
}

bool
rlHttpDownloadTask::SendFileSizeRequest()
{
	bool success = false;

	rtry
	{
		const int urlLen = istrlen(m_RemotePath) + istrlen("/") + istrlen(m_RemoteFileName) + 1;

		char* url = Alloca(char, urlLen);

		rverify(url, catchall, rlError("Error allocating URL buffer"));

		formatf(url, urlLen, "%s/%s", m_RemotePath, m_RemoteFileName);

		rlTaskDebug2("Sending request for file size of: %s...", url);

#if !__NO_OUTPUT
		char contextStr[64];
		formatf(contextStr, "%s[%u]", this->GetTaskName(), this->GetTaskId());
#else
		const char* contextStr = NULL;
#endif

		unsigned bufSize = 64 * 1024;
		m_DownloadBuffer = NULL;
		while((m_DownloadBuffer == NULL) && (bufSize >= 1024))
		{
			m_DownloadBuffer = g_rlAllocator->Allocate(bufSize, 16, 0);
			bufSize >>= 1;
		}
		bufSize <<= 1;

		m_fiResponseHandle = m_fiResponseDevice->Open(m_DeviceMemoryFilename, false);
		rverify(m_fiResponseHandle != fiHandleInvalid, catchall, );

		m_HttpRequest.Init(g_rlAllocator, rlRos::GetRosSslContext(), m_DownloadBuffer, bufSize);
		m_HttpRequestInited = true;

		rverify(m_HttpRequest.BeginHead(url,
			NULL,   //proxyAddr
			DEFAULT_TIMEOUT_SECS,
			contextStr,
			&m_MyStatus),
			catchall,
			rlTaskError("Error beginning HEAD"));

		rverify(m_HttpRequest.Commit(),
			catchall,
			rlTaskError("Error committing HEAD"));

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool 
rlHttpDownloadTask::DoesFileExist(bool deleteFile)
{
	char path[RAGE_MAX_PATH];

	// Note: there was a crash reported here with formatf so I changed it to safecpy/safecats
	//formatf(path, "%s/%s", m_LocalPath, m_LocalFileName);

	safecpy(path, m_LocalPath);
	safecat(path, "/");
	safecat(path, m_LocalFileName);
		
	m_fiDevice = fiDevice::GetDevice(path, false);

	bool exists = false;

	if(m_fiDevice)
	{
		exists = m_fiDevice->GetAttributes(path) != FILE_ATTRIBUTE_INVALID;
		if(exists)
		{
			// if the file is 0 bytes in length, it was probably downloaded incorrectly previously
			u64 size = m_fiDevice->GetFileSize(path);
			if(size == 0 && deleteFile)
			{
				RemoveFile();
				exists = false;
			}
		}
	}

	return exists;
}

bool 
rlHttpDownloadTask::ResumeFile()
{
	bool success = false;

	rtry
	{
		char path[RAGE_MAX_PATH];

		formatf(path, "%s/%s", m_LocalPath, m_LocalFileName);

		rverify(ASSET.CreateLeadingPath(path), catchall, );

		if(m_fiDevice == NULL)
		{
			formatf(path, "%s/%s", m_LocalPath, m_LocalFileName);
			m_fiDevice = fiDevice::GetDevice(path, false);
		}

		m_fiHandle = m_fiDevice->Open(path, false);
		if (m_fiHandle != fiHandleInvalid)
		{
			u64 fileSize = m_fiDevice->GetFileSize(path);
			u64 position = m_fiDevice->Seek64(m_fiHandle, fileSize, seekSet);

			rverify(fileSize == position, catchall, );

			m_CurrentFileSize = (unsigned int)position;
		}
		else
		{
			if (_CreateFile())
			{
				m_CurrentFileSize = 0;
			}
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
rlHttpDownloadTask::_CreateFile()
{
	bool success = false;

	rtry
	{
		char path[RAGE_MAX_PATH];

		formatf(path, "%s/%s", m_LocalPath, m_LocalFileName);

		rverify(ASSET.CreateLeadingPath(path), catchall, );

		if(m_fiDevice == NULL)
		{
			formatf(path, "%s/%s", m_LocalPath, m_LocalFileName);
			m_fiDevice = fiDevice::GetDevice(path, false);
		}

		m_fiHandle = m_fiDevice->Create(path);
		rverify(m_fiHandle != fiHandleInvalid, catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}


void 
rlHttpDownloadTask::CloseFile()
{
	if(m_fiDevice)
	{
		m_fiDevice->Flush(m_fiHandle);
		m_fiDevice->Close(m_fiHandle);
		m_fiHandle = fiHandleInvalid;
		m_fiDevice = NULL;
	}

	if (m_fiResponseDevice)
	{
		m_fiResponseDevice->Flush(m_fiResponseHandle);
		m_fiResponseDevice->Close(m_fiResponseHandle);
		m_fiResponseHandle = fiHandleInvalid;
		m_fiResponseDevice = NULL;
	}
}

void 
rlHttpDownloadTask::RemoveFile()
{
	char path[RAGE_MAX_PATH];

	formatf(path, "%s/%s", m_LocalPath, m_LocalFileName);

	CloseFile();

	if(m_fiDevice)
	{
		rlVerify(m_fiDevice->Delete(path));
	}
	else
	{
		const fiDevice *device = fiDevice::GetDevice(path, false);
		rlVerify(device->Delete(path));
	}
}

////////////////////////////////////////////////////////////////////////////////
// rlHttpFileSizeTask
////////////////////////////////////////////////////////////////////////////////
rlHttpFileSizeTask::rlHttpFileSizeTask()
: m_State(STATE_INVALID)
, m_StartTime(0)
, m_HaveResponseHeader(false)
, m_HttpRequestInited(false)
{
}

rlHttpFileSizeTask::~rlHttpFileSizeTask()
{
	Reset();
}

void
rlHttpFileSizeTask::Reset()
{
	rlAssert(!this->IsPending());

	m_State = STATE_INVALID;
	m_StartTime = 0;
	m_HttpRequest.Clear();

	m_HaveResponseHeader = false;
	m_HttpRequestInited = false;
}

bool
rlHttpFileSizeTask::Configure(const char* urlPath, unsigned int* contentLength)
{
	bool success = false;

	rtry
	{
		rverify(STATE_INVALID == m_State || STATE_FINISHED == m_State, catchall,);

		m_ContentLength = contentLength;

		Reset();

		rverify(SetupPaths(urlPath), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
rlHttpFileSizeTask::SetupPaths(const char* downloadUrl)
{
	bool success = false;

	rtry
	{
		rverify(downloadUrl != NULL, catchall, );
		rverify(strlen(downloadUrl) < sizeof(m_RemotePath), catchall, );
		rverify(strlen(downloadUrl) > strlen("http://"), catchall, );

		// download url can begin with http:// or https://
		rverify(strnicmp(downloadUrl, "http", 4) == 0, catchall, );

		// SECURITY - only support fully qualified paths
		rverify(strstr(downloadUrl, ".\\") == NULL, catchall, );
		rverify(strstr(downloadUrl, "..\\") == NULL, catchall, );
		rverify(strstr(downloadUrl, "\\\\") == NULL, catchall, );
		rverify(strstr(downloadUrl, "./") == NULL, catchall, );
		rverify(strstr(downloadUrl, "../") == NULL, catchall, );

		safecpy(m_RemotePath, downloadUrl);

		m_RemoteFileName = ASSET.FileName(m_RemotePath);

		char* lastSlash = Max(strrchr(m_RemotePath, '\\'), strrchr(m_RemotePath, '/'));

		if(lastSlash)
		{
			*lastSlash = '\0';
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
rlHttpFileSizeTask::Start()
{
	rlTaskDebug2("Start");

	rlTaskBase::Start();
	rlAssert(STATE_INVALID == m_State);

	m_StartTime = sysTimer::GetSystemMsTime();

	m_State = STATE_GET_SERVER_FILE_SIZE;
}

void
rlHttpFileSizeTask::Finish(const FinishType finishType, const int resultCode)
{
	rlTaskDebug2("%s (%ums)", FinishString(finishType), sysTimer::GetSystemMsTime() - m_StartTime);

	rlTaskBase::Finish(finishType, resultCode);

	m_State = STATE_FINISHED;
}

void
rlHttpFileSizeTask::DoCancel()
{
	if(m_State == STATE_GET_SERVER_FILE_SIZE_RESPONSE)
	{
		if(m_HttpRequestInited)
		{
			m_HttpRequest.Cancel();
		}
	}
}

void 
rlHttpFileSizeTask::Update(const unsigned timeStep)
{
	rlTaskBase::Update(timeStep);

	if(m_HttpRequestInited)
	{
		m_HttpRequest.Update();
	}

	switch(m_State)
	{
	case STATE_GET_SERVER_FILE_SIZE:
		if (this->SendFileSizeRequest())
		{
			m_State = STATE_GET_SERVER_FILE_SIZE_RESPONSE;
		}
		else
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_GET_SERVER_FILE_SIZE_RESPONSE:
		if (m_MyStatus.Succeeded())
		{
			unsigned int contentLength = m_HttpRequest.GetResponseContentLength();

			if (contentLength == 0)
			{
				m_State = STATE_FAILED;
			}
			else
			{
				*m_ContentLength = contentLength;
				m_State = STATE_FINISHED;
				m_HttpRequest.Clear();

				this->Finish(FINISH_SUCCEEDED);
			}
		}
		else if (!m_MyStatus.Pending())
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_FAILED:
		m_HttpRequest.Clear();
		this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
		break;
	case STATE_INVALID:
	default:
		rlAssert(false);
	}
}

bool
rlHttpFileSizeTask::SendFileSizeRequest()
{
	bool success = false;

	rtry
	{
		const int urlLen = istrlen(m_RemotePath) + istrlen("/") + istrlen(m_RemoteFileName) + 1;

		char* url = Alloca(char, urlLen);

		rverify(url, catchall, rlError("Error allocating URL buffer"));

		formatf(url, urlLen, "%s/%s", m_RemotePath, m_RemoteFileName);

		rlTaskDebug2("Sending request for file size of: %s...", url);

#if !__NO_OUTPUT
		char contextStr[64];
		formatf(contextStr, "%s[%u]", this->GetTaskName(), this->GetTaskId());
#else
		const char* contextStr = NULL;
#endif

		m_HttpRequest.Init(g_rlAllocator, rlRos::GetRosSslContext());
		m_HttpRequestInited = true;

		rverify(m_HttpRequest.BeginHead(url,
			NULL,   //proxyAddr
			DEFAULT_TIMEOUT_SECS,
			contextStr,
			&m_MyStatus),
			catchall,
			rlTaskError("Error beginning HEAD"));

		rverify(m_HttpRequest.Commit(),
			catchall,
			rlTaskError("Error committing HEAD"));

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////
// rlHttpLlnwUploadVideoTask
////////////////////////////////////////////////////////////////////////////////
rlHttpLlnwUploadVideoTask::rlHttpLlnwUploadVideoTask()
: m_State(STATE_INVALID)
, m_SrcData(NULL)
, m_SrcDataLen(0)
, m_NumPieces(0)
, m_NumPiecesUploaded(0)
, m_CurrentPiece(0)
, m_RequestedPiece(0)
{
}

rlHttpLlnwUploadVideoTask::~rlHttpLlnwUploadVideoTask()
{
	Reset();
}

void
rlHttpLlnwUploadVideoTask::Reset()
{
	rlAssert(!this->IsPending());

	m_State = STATE_INVALID;
	m_HttpRequest.Clear();
}

bool
rlHttpLlnwUploadVideoTask::Configure(const char* name,
                                    const char* url,
                                    const char* authToken,
                                    const char* refId,
                                    const int piece,
                                    const void* srcData,
                                    const unsigned srcDataLength)
{
	bool success = false;

	rtry
	{
        if(piece)
        {
            rlTaskDebug("Uploading piece %d of '%s'...", piece, name);
        }
        else
        {
            rlTaskDebug("Uploading entire contents of '%s'...", name);
        }

		rverify(STATE_INVALID == m_State || STATE_FINISHED == m_State, catchall,);

        safecpy(m_StagingName, name);
        safecpy(m_StagingUrl, url);
        safecpy(m_StagingAuthToken, authToken);
        safecpy(m_StagingRefId, refId);

        rverify(strlen(m_StagingName) == strlen(name), catchall, rlTaskError("Name string buffer is too small for '%s'", name));
        rverify(strlen(m_StagingUrl) == strlen(url), catchall, rlTaskError("URL string buffer is too small for '%s'", url));
        rverify(strlen(m_StagingAuthToken) == strlen(authToken), catchall, rlTaskError("Auth token string buffer is too small for '%s'", authToken));
        rverify(strlen(m_StagingRefId) == strlen(refId), catchall, rlTaskError("Ref ID string buffer is too small for '%s'", refId));

        m_SrcData = (const u8*)srcData;
		m_SrcDataLen = srcDataLength;
        if(!piece)
        {
            m_NumPieces = (int)((srcDataLength + (PIECE_SIZE)-1) / (PIECE_SIZE));
        }
        else
        {
            m_NumPieces = 1;
        }

        m_NumPiecesUploaded = 0;

        m_RequestedPiece = m_CurrentPiece = piece;

		Reset();

        m_State = STATE_UPLOAD_PIECE;

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool
rlHttpLlnwUploadVideoTask::Configure(const char* name,
                                    const char* url,
                                    const char* username,
                                    const char* password,
                                    const char* refId,
                                    const int piece,
                                    const void* srcData,
                                    const unsigned srcDataLength)
{
	bool success = false;

	rtry
	{
        if(piece)
        {
            rlTaskDebug("Uploading piece %d of '%s'...", piece, name);
        }
        else
        {
            rlTaskDebug("Uploading entire contents of '%s'...", name);
        }

		rverify(STATE_INVALID == m_State || STATE_FINISHED == m_State, catchall,);

        safecpy(m_StagingName, name);
        safecpy(m_StagingUrl, url);
        safecpy(m_StagingUserName, username);
        safecpy(m_StagingPassword, password);
        safecpy(m_StagingRefId, refId);

        rverify(strlen(m_StagingName) == strlen(name), catchall, rlTaskError("Name string buffer is too small for '%s'", name));
        rverify(strlen(m_StagingUrl) == strlen(url), catchall, rlTaskError("URL string buffer is too small for '%s'", url));
        rverify(strlen(m_StagingUserName) == strlen(username), catchall, rlTaskError("Username string buffer is too small for '%s'", username));
        rverify(strlen(m_StagingPassword) == strlen(password), catchall, rlTaskError("Password string buffer is too small for '%s'", password));
        rverify(strlen(m_StagingRefId) == strlen(refId), catchall, rlTaskError("Ref ID string buffer is too small for '%s'", refId));

        m_SrcData = (const u8*)srcData;
		m_SrcDataLen = srcDataLength;
        if(!piece)
        {
            m_NumPieces = (int)((srcDataLength + (PIECE_SIZE)-1) / (PIECE_SIZE));
        }
        else
        {
            m_NumPieces = 1;
        }

        m_NumPiecesUploaded = 0;

        m_RequestedPiece = m_CurrentPiece = piece;

		Reset();

        m_State = STATE_GET_AUTH_TOKEN;

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

void
rlHttpLlnwUploadVideoTask::Start()
{
	rlTaskDebug2("Starting video upload for '%s'", m_StagingName);

	rlTaskBase::Start();
    rlAssert(STATE_UPLOAD_PIECE == m_State || STATE_GET_AUTH_TOKEN == m_State);

    if(!m_RequestedPiece)
    {
        m_CurrentPiece = 0;
    }

    m_NumPiecesUploaded = 0;
}

void
rlHttpLlnwUploadVideoTask::Finish(const FinishType finishType, const int resultCode)
{
	rlTaskBase::Finish(finishType, resultCode);

	m_State = STATE_FINISHED;
}

void
rlHttpLlnwUploadVideoTask::DoCancel()
{
	m_HttpRequest.Cancel();
}

void 
rlHttpLlnwUploadVideoTask::Update(const unsigned timeStep)
{
	rlTaskBase::Update(timeStep);

    if(WasCanceled())
    {
        return;
    }

	switch(m_State)
	{
    case STATE_GET_AUTH_TOKEN:
        if(GetAuthToken())
        {
            m_State = STATE_GETTING_AUTH_TOKEN;
        }
        else
        {
			m_State = STATE_FAILED;
        }
        break;
    case STATE_GETTING_AUTH_TOKEN:
        m_HttpRequest.Update();
		if (m_MyStatus.Succeeded())
		{
            unsigned tokenLen = sizeof(m_StagingAuthToken)-1;

            if(m_HttpRequest.GetResponseHeaderValue("X-Agile-Token", m_StagingAuthToken, &tokenLen))
            {
                m_StagingAuthToken[tokenLen] = '\0';
                m_State = STATE_UPLOAD_PIECE;
            }
            else
            {
                rlTaskError("Error getting token from header 'X-Agile-Token'");
    			m_State = STATE_FAILED;
            }
		}
		else if (!m_MyStatus.Pending())
		{
			m_State = STATE_FAILED;
		}
        break;
	case STATE_UPLOAD_PIECE:
        if(!m_RequestedPiece)
        {
            //Piece numbers start at 1, not zero.
            ++m_CurrentPiece;
        }

        if(this->UploadPiece())
		{
			m_State = STATE_UPLOADING_PIECE;
		}
		else
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_UPLOADING_PIECE:
        m_HttpRequest.Update();
		if (m_MyStatus.Succeeded())
		{
            ++m_NumPiecesUploaded;

            if(m_NumPiecesUploaded >= m_NumPieces || m_RequestedPiece)
            {
                m_State = STATE_FINISHED;
                this->Finish(FINISH_SUCCEEDED);
            }
            else
            {
                //Upload the next piece
                m_State = STATE_UPLOAD_PIECE;
            }
		}
		else if (!m_MyStatus.Pending())
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_FAILED:
		m_HttpRequest.Clear();
		this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
		break;
	case STATE_INVALID:
	default:
		rlAssert(false);
	}
}

bool
rlHttpLlnwUploadVideoTask::GetAuthToken()
{
	bool success = false;

	rtry
	{
        rlTaskDebug("Getting auth token...");

		m_HttpRequest.Init(g_rlAllocator, rlRos::GetRosSslContext());

        char ctxStr[256];
        formatf(ctxStr, "%s:%s", GetTaskName(), m_StagingName);

        const char* url = "http://api.agile.lldns.net:8080/account/login";

        rverify(m_HttpRequest.BeginGet(url, NULL, 30, NULL, ctxStr, NULL, &m_MyStatus),
                catchall,
			    rlTaskError("Error beginning GET to '%s'", url));

        rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Username", m_StagingUserName),
                catchall,
			    rlTaskError("Error adding header 'X-Agile-Username'"));

        rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Password", m_StagingPassword),
                catchall,
			    rlTaskError("Error adding header 'X-Agile-Password'"));

		rverify(m_HttpRequest.Commit(),
			catchall,
			rlTaskError("Error committing GET to '%s'", url));

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool
rlHttpLlnwUploadVideoTask::UploadPiece()
{
	bool success = false;

	rtry
	{
        rlTaskDebug("Uploading piece %d for '%s'...", m_CurrentPiece, m_StagingName);

        if(!m_RequestedPiece)
        {
            //We have the entire video buffer.
            //Fire off a sub task to upload the current piece.

            const unsigned offset = (m_CurrentPiece-1)*PIECE_SIZE;
            unsigned len = PIECE_SIZE;

            if(offset+len > m_SrcDataLen)
            {
                rlAssert(m_CurrentPiece == m_NumPieces);

                //The buffer might not be a multiple of PIECE_SIZE,
                //in which case the last piece will be smaller than PIECE_SIZE.
                len = m_SrcDataLen - offset;
            }

            rlHttpLlnwUploadVideoTask* task = rlGetTaskManager()->CreateTask<rlHttpLlnwUploadVideoTask>();
            rverify(rlTaskBase::Configure(task,
                                        m_StagingName,
                                        m_StagingUrl,
                                        m_StagingAuthToken,
                                        m_StagingRefId,
                                        m_CurrentPiece,
                                        &m_SrcData[offset],
                                        len,
                                        &m_MyStatus),
                    catchall,
			        rlTaskError("Error creating rlHttpLlnwUploadVideoTask"));

            rverify(rlGetTaskManager()->AddParallelTask(task),
                    catchall,
			        rlTaskError("Error queuing rlHttpLlnwUploadVideoTask"));
        }
        else
        {
            //We only have one piece of a larger video buffer.
            //Upload the piece.

		    m_HttpRequest.Init(g_rlAllocator, rlRos::GetRosSslContext());

            char ctxStr[256];
            formatf(ctxStr, "%s:%s", GetTaskName(), m_StagingName);

            rverify(m_HttpRequest.BeginPost(m_StagingUrl, NULL, 30, NULL, ctxStr, NULL, &m_MyStatus),
                    catchall,
			        rlTaskError("Error beginning POST to '%s'", m_StagingUrl));

            char pieceStr[64];
            formatf(pieceStr, "%d", m_CurrentPiece);

            rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Authorization", m_StagingAuthToken),
                    catchall,
			        rlTaskError("Error adding header 'X-Agile-Authorization'"));

            rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-MultiPart", m_StagingRefId),
                    catchall,
			        rlTaskError("Error adding header 'X-Agile-MultiPart'"));

            rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Part", pieceStr),
                    catchall,
			        rlTaskError("Error adding header 'X-Agile-Part'"));

            rverify(m_HttpRequest.AppendContentZeroCopy(m_SrcData, m_SrcDataLen),
                    catchall,
			        rlTaskError("Error adding content for piece %d", m_CurrentPiece));

		    rverify(m_HttpRequest.Commit(),
			    catchall,
			    rlTaskError("Error committing POST to '%s'", m_StagingUrl));
        }

		success = true;
	}
	rcatchall
	{
	}

	return success;
}


////////////////////////////////////////////////////////////////////////////////
// rlHttpCdnUploadPhotoTask
////////////////////////////////////////////////////////////////////////////////
rlHttpCdnUploadPhotoTask::rlHttpCdnUploadPhotoTask()
	: m_State(STATE_INVALID)
	, m_SrcData(NULL)
	, m_SrcDataLen(0)
	, m_ResponseHeaderValueToGet(NULL)
	, m_ResponseHeaderValueBuffer(NULL)
	, m_ResponseHeaderValueBufferSize(0)
{
}

rlHttpCdnUploadPhotoTask::~rlHttpCdnUploadPhotoTask()
{
	Reset();
}

void
rlHttpCdnUploadPhotoTask::Reset()
{
	rlAssert(!this->IsPending());

	m_State = STATE_INVALID;
	m_HttpRequest.Clear();
}

bool
rlHttpCdnUploadPhotoTask::Configure(const char* name,
	const char* url,
	const char* authToken,
	const char* dirName,
	const char* fileName,
	const void* srcData,
	const unsigned srcDataLength,
	const char* responseHeaderValueToGet,
	char* responseHeaderValueBuffer,
	const unsigned responseHeaderValueBufferSize,
	const bool akamai)
{
	bool success = false;

	rtry
	{
		rlTaskDebug("Uploading photo with name '%s' and filename %s...", name, fileName);

		rverify(STATE_INVALID == m_State || STATE_FINISHED == m_State, catchall,);

		safecpy(m_StagingName, name);
		safecpy(m_StagingUrl, url);
		safecpy(m_StagingAuthToken, authToken);
		safecpy(m_StagingDirName, dirName);
		safecpy(m_StagingFileName, fileName);

		rverify(strlen(m_StagingName) == strlen(name), catchall, rlTaskError("Name string buffer is too small for '%s'", name));
		rverify(strlen(m_StagingUrl) == strlen(url), catchall, rlTaskError("URL string buffer is too small for '%s'", url));
		rverify(strlen(m_StagingAuthToken) == strlen(authToken), catchall, rlTaskError("Auth token string buffer is too small for '%s'", authToken));
		rverify(strlen(m_StagingDirName) == strlen(dirName), catchall, rlTaskError("Directory name string buffer is too small for '%s'", dirName));
		rverify(strlen(m_StagingFileName) == strlen(fileName), catchall, rlTaskError("Filename string buffer is too small for '%s'", fileName));

		formatf(m_RealUrl, "%s%s%s?%s", m_StagingUrl, m_StagingDirName, m_StagingFileName, m_StagingAuthToken);

		m_SrcData = (const u8*)srcData;
		m_SrcDataLen = srcDataLength;

		m_ResponseHeaderValueToGet = responseHeaderValueToGet;
		m_ResponseHeaderValueBuffer = responseHeaderValueBuffer;
		m_ResponseHeaderValueBufferSize = responseHeaderValueBufferSize;
		m_Akamai = akamai;

		Reset();

		m_State = STATE_UPLOAD;

		success = true;
	}
	rcatchall
	{
	}

	return success;
}


void
rlHttpCdnUploadPhotoTask::Start()
{
	rlTaskDebug2("Starting photo upload for '%s' with filename %s", m_StagingName, m_StagingFileName);

	rlTaskBase::Start();
	rlAssert(STATE_UPLOAD == m_State);
}

void
rlHttpCdnUploadPhotoTask::Finish(const FinishType finishType, const int resultCode)
{
	rlTaskBase::Finish(finishType, resultCode);

	m_State = STATE_FINISHED;
}

void
rlHttpCdnUploadPhotoTask::DoCancel()
{
	m_HttpRequest.Cancel();
}

void 
rlHttpCdnUploadPhotoTask::Update(const unsigned timeStep)
{
	rlTaskBase::Update(timeStep);

	if(WasCanceled())
	{
		return;
	}

	switch(m_State)
	{
	case STATE_UPLOAD:
		if(this->Upload())
		{
			m_State = STATE_UPLOADING;
		}
		else
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_UPLOADING:
		m_HttpRequest.Update();
		if (m_MyStatus.Succeeded())
		{
			if (m_HttpRequest.Succeeded())
			{
				rlDebug("Uploaded %s", m_HttpRequest.GetUri());

				if (m_ResponseHeaderValueToGet && m_ResponseHeaderValueBuffer)
				{
					m_HttpRequest.GetResponseHeaderValue(
						m_ResponseHeaderValueToGet, 
						m_ResponseHeaderValueBuffer, 
						&m_ResponseHeaderValueBufferSize);
				}

				this->Finish(FINISH_SUCCEEDED, m_HttpRequest.GetStatusCode());
			}
			else
			{
				rlTaskError("Failed to upload %s, status code:%d",
					m_HttpRequest.GetUri(),
					m_HttpRequest.GetStatusCode());
				
				this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
			}

			m_HttpRequest.Clear();
			m_State = STATE_FINISHED;
		}
		else if (!m_MyStatus.Pending())
		{
			m_State = STATE_FAILED;
		}
		break;
	case STATE_FAILED:
		rlTaskError("Failed to upload %s, Http Request status code:%d, MyStatus result code:%d",
			m_HttpRequest.GetUri(),
			m_HttpRequest.GetStatusCode(),
			m_MyStatus.GetResultCode());

		m_HttpRequest.Clear();
		this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
		break;
	case STATE_INVALID:
	default:
		rlAssert(false);
	}
}

bool
rlHttpCdnUploadPhotoTask::Upload()
{
	bool success = false;

	rtry
	{
		rlTaskDebug("Uploading '%s' with filename %s...", m_StagingName, m_StagingFileName);

		m_HttpRequest.Init(g_rlAllocator, rlRos::GetRosSslContext());
		
		char ctxStr[256];
		formatf(ctxStr, "%s:%s", GetTaskName(), m_StagingName);	
		
		if (m_Akamai)
		{
			m_HttpRequest.SetUrlEncode(false);

			rverify(m_HttpRequest.BeginPost(m_RealUrl, NULL, 30, NULL, ctxStr, NULL, &m_MyStatus),
				catchall,
				rlTaskError("Error beginning POST to '%s'", m_RealUrl));
		}
		else
		{
			rverify(m_HttpRequest.BeginPost(m_StagingUrl, NULL, 30, NULL, ctxStr, NULL, &m_MyStatus),
				catchall,
				rlTaskError("Error beginning POST to '%s'", m_StagingUrl));
	
			rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Authorization", m_StagingAuthToken),
				catchall,
				rlTaskError("Error adding header 'X-Agile-Authorization'"));
	
			rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Directory", m_StagingDirName),
				catchall,
				rlTaskError("Error adding header 'X-Agile-Directory'"));
	
			rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Basename", m_StagingFileName),
				catchall,
				rlTaskError("Error adding header 'X-Agile-Basename'"));
	
			rverify(m_HttpRequest.AddRequestHeaderValue("X-Agile-Content-Detect", m_StagingName),
				catchall,
				rlTaskError("Error adding header 'X-Agile-Content-Detect'"));
		}

		rverify(m_HttpRequest.AppendContentZeroCopy(m_SrcData, m_SrcDataLen),
			catchall,
			rlTaskError("Error adding content for photo with filename '%s'", m_StagingFileName));

		rverify(m_HttpRequest.Commit(),
			catchall,
			rlTaskError("Error committing POST to '%s'", m_Akamai ? m_RealUrl : m_StagingUrl));

		success = true;
	}
	rcatchall
	{
	}

	return success;
}
}; //namespace rage
