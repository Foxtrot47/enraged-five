// 
// rline/rlgamerinfo.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlgamerinfo.h"
#include "rldiag.h"
#include "rlpresence.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "diag/output.h"
#include "net/nethardware.h"
#include "system/nelem.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "string/unicode.h"
#include "system/memory.h"
#include "system/param.h"

#if RSG_DURANGO
#include "rline/rlprivileges.h"
#include "rline/durango/rlxblprivileges_interface.h"
#endif

#include <stdio.h>

#if RSG_PC
#include "rlpc.h"
#include "ros/rlRos.h"
#endif

#if RSG_NP
#include "rlnp.h"
#include "string/unicode.h"
#include <np.h>
#endif

namespace rage
{

PARAM(disabledevprivacycheck, "If present the dev-only privacy check is disabled");

#if !__FINAL
PARAM(overridePlayerAge, "Overrides local player age (for testing game rating)");
#endif

#if RSG_NP
PARAM(disabledevspintcheck, "If present the dev-only sp-int check is disabled");
#endif

//////////////////////////////////////////////////////////////////////////
// rlGamerId
//////////////////////////////////////////////////////////////////////////

rlGamerId::rlGamerId()
{
    this->Clear();
}

void
rlGamerId::Clear()
{
    m_Id = ~u64(0);
}

bool
rlGamerId::IsValid() const
{
    return (m_Id != ~u64(0));
}

const char*
rlGamerId::ToString(char* dst, const unsigned dstLen) const
{
    return formatf(dst, dstLen, "%" I64FMT "u", m_Id);
}

const char*
rlGamerId::ToHexString(char* dst, const unsigned dstLen) const
{
    return formatf(dst, dstLen, "0x%" I64FMT "x", m_Id);
}

bool
rlGamerId::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    if(!rlVerify(this->IsValid()))
    {
        return false;
    }

    datBitBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    const bool success = bb.WriteUns(m_Id, sizeof(m_Id) * 8);

    if(size) { *size = success ? bb.GetNumBytesWritten() : 0; }

    return success;
}

bool
rlGamerId::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datBitBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    const bool success = bb.ReadUns(m_Id, sizeof(m_Id) * 8);

    if(size) { *size = success ? bb.GetNumBytesRead() : 0; }

    return success;
}

bool
rlGamerId::operator==(const rlGamerId& that) const
{
    return (m_Id == that.m_Id);
}

bool
rlGamerId::operator!=(const rlGamerId& that) const
{
    return (m_Id != that.m_Id);
}

bool
rlGamerId::operator<(const rlGamerId& that) const
{
    return m_Id < that.m_Id;
}

//////////////////////////////////////////////////////////////////////////
// rlGamerInfo
//////////////////////////////////////////////////////////////////////////

bool
rlGamerInfo::HasMultiplayerPrivileges(const int localGamerIndex)
{
    bool hasPriv = false;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        hasPriv = true;

#if RSG_ORBIS
		hasPriv = g_rlNp.CanAccessRealtimeMultiplayer(localGamerIndex) && g_rlNp.GetNpAuth().IsNpAvailable(localGamerIndex);
#elif RSG_DURANGO
        hasPriv = rlPrivileges::HasPrivilege(localGamerIndex, rlPrivileges::PRIVILEGE_MULTIPLAYER_SESSIONS);
#elif RSG_PC
        // currently we don't have profile restrictions on PC
        hasPriv = true;
#else
        rlAssert(false);
#endif
    }

    return hasPriv;
}

bool rlGamerInfo::IsRestrictedByAge(const int ORBIS_ONLY(localGamerIndex))
{
#if RSG_ORBIS
    if(!g_rlNp.GetNpAuth().IsNpAvailable(localGamerIndex))
    {
        if(g_rlNp.GetNpAuth().GetNpUnavailabilityReason(localGamerIndex) == rlNpAuth::NP_REASON_AGE)
        {
            return true;
        }
    }
#endif

    return false;
}

bool
rlGamerInfo::HasChatPrivileges(const int localGamerIndex)
{
    bool hasPriv = false;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        hasPriv = true;

#if RSG_ORBIS
        bool isRestricted = 0;
        const int err = g_rlNp.GetChatRestriction(localGamerIndex, &isRestricted);
        if(SCE_OK == err)
        {
            hasPriv = g_rlNp.CanAccessRealtimeMultiplayer(localGamerIndex) && !isRestricted;
        }
        else
        {
            hasPriv = false;
            rlError("Error calling g_rlNp.GetChatRestriction (0x%08x)", err);
        }
#elif RSG_DURANGO
        hasPriv = rlPrivileges::HasPrivilege(localGamerIndex, rlPrivileges::PRIVILEGE_COMMUNICATION_VOICE_INGAME, IPrivileges::PRIV_RESULT_NOISSUE);
#elif RSG_PC
        // currently we don't have profile restrictions on PC
        hasPriv = true;
#else
        rlAssert(false);
#endif
    }

    return hasPriv;
}

bool
rlGamerInfo::HasTextPrivileges(const int localGamerIndex)
{
    bool hasPriv = false;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        hasPriv = true;

#if RSG_DURANGO
        hasPriv = rlPrivileges::HasPrivilege(localGamerIndex, rlPrivileges::PRIVILEGE_COMMUNICATIONS, IPrivileges::PRIV_RESULT_NOISSUE);
#else
        // other platforms don't have a unique permission for text versus chat
        hasPriv = HasChatPrivileges(localGamerIndex);
#endif
    }

    return hasPriv;
}

bool
rlGamerInfo::HasUserContentPrivileges(const int localGamerIndex)
{
    bool hasPriv = true;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        hasPriv = true;

#if RSG_ORBIS
		bool isRestricted = false;
		int err = g_rlNp.GetUgcRestriction(localGamerIndex, &isRestricted);
		hasPriv = (err == SCE_OK) && !isRestricted;
#elif RSG_DURANGO
        hasPriv = rlPrivileges::HasPrivilege(localGamerIndex, rlPrivileges::PRIVILEGE_USER_CREATED_CONTENT, IPrivileges::PRIV_RESULT_NOISSUE);
#else

#endif
    }

    return hasPriv;
}

bool rlGamerInfo::HasSocialNetworkingPrivileges(const int localGamerIndex)
{
    bool hasPriv = false;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        hasPriv = true;
#if RSG_ORBIS
        bool isRestricted = false;
        int age = 0;
        int err = g_rlNp.GetAge(localGamerIndex, &age);
        err = (SCE_OK == err) && g_rlNp.GetUgcRestriction(localGamerIndex, &isRestricted);

        if(SCE_OK == err)
        {
            hasPriv = (!isRestricted && age >= (int)rlGetMinAgeRating());
        }
        else
        {
            hasPriv = false;
        }
#elif RSG_DURANGO
        hasPriv = rlPrivileges::HasPrivilege(localGamerIndex, rlPrivileges::PRIVILEGE_SOCIAL_NETWORK_SHARING);
#else
#endif
    }

    return hasPriv;
}

//Separate due to a different check in XUserCheckPrivilege
bool
rlGamerInfo::HasStorePrivileges(const int localGamerIndex)
{
    bool hasPriv = false;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        hasPriv = true;

#if RSG_ORBIS
        return !IsRestrictedByAge(localGamerIndex);
#elif RSG_DURANGO
        hasPriv = rlPrivileges::HasPrivilege(localGamerIndex, rlPrivileges::PRIVILEGE_PURCHASE_CONTENT) || rlPrivileges::HasPrivilege(localGamerIndex, rlPrivileges::PRIVILEGE_DOWNLOAD_FREE_CONTENT);
#elif RSG_PC
        // currently we don't have profile restrictions on PC
        hasPriv = true;
#else
        rlAssert(false);
#endif
    }

    return hasPriv;
}

#if RSG_ORBIS
bool rlGamerInfo::HasPlayStationPlusSubscription(const int localGamerIndex)
{
	return g_rlNp.GetNpAuth().IsPlusAuthorized(localGamerIndex);
}

bool rlGamerInfo::IsPlaystationPlusCheckPending(const int localGamerIndex)
{
	return g_rlNp.IsRequestPermissionsPending(localGamerIndex);
}
#endif

bool rlGamerInfo::IsPlaystationSubAccount(const int ORBIS_ONLY(localGamerIndex))
{
#if RSG_ORBIS
    return g_rlNp.GetNpAuth().IsSubAccount(localGamerIndex);
#else
    return false;
#endif
}

bool rlGamerInfo::IsSignedOutOfPSN(const int ORBIS_ONLY(localGamerIndex))
{
#if RSG_ORBIS
    if(!g_rlNp.GetNpAuth().IsNpAvailable(localGamerIndex))
    {
        if(g_rlNp.GetNpAuth().GetNpUnavailabilityReason(localGamerIndex) == rlNpAuth::NP_REASON_SIGNED_OUT)
            return true;
    }
#endif
    return false;
}

//Use -disabledevprivacycheck on the command line to disable this check.
bool rlGamerInfo::CheckDevPrivacyPrivilegesAndBail(const int NOTFINAL_ONLY(localGamerIndex))
{
#if __FINAL
    return true;
#else
    if(PARAM_disabledevprivacycheck.Get())
    {
        return true;
    }

    bool everythingsOk = true;

    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        if(rlPresence::IsSignedIn(localGamerIndex))
        {
            Displayf("Account logged in - checking privileges for Development secrecy:");

#if RSG_NP
            //Must be running in sp-int.
            everythingsOk =
                PARAM_disabledevspintcheck.Get()    //Bypass the sp-int check?
                || (g_rlNp.GetEnvironment() == RLNP_ENV_SP_INT);
            if(!everythingsOk)
            {
                Quitf("You're not playing in the sp-int environment.\n"
                        "Please go to the XMB and select Settings->Debug Settings->NP Environment and enter \"sp-int\".");
            }
#endif
        }
    }

    return everythingsOk;
#endif  //__FINAL
}

int
rlGamerInfo::GetAge(const int localGamerIndex)
{
    return rlPresence::GetAge(localGamerIndex);
}

rlAgeGroup
rlGamerInfo::GetAgeGroup(const int localGamerIndex)
{
    return rlPresence::GetAgeGroup(localGamerIndex);
}

rlGamerInfo::rlGamerInfo()
{
    this->Clear();
}

void
rlGamerInfo::Clear()
{
    m_PeerInfo.Clear();
    m_GamerId.Clear();
    m_GamerHandle.Clear();
    m_LocalIndex = -1;
    sysMemSet(m_Name, 0, sizeof(m_Name));
#if RSG_DURANGO
    sysMemSet(m_DisplayName, 0, sizeof(m_DisplayName));
#endif
    m_IsSignedIn = m_IsOnline = false;
}

bool
rlGamerInfo::IsValid() const
{
    return m_GamerId.IsValid()
        && m_GamerHandle.IsValid()
        && m_PeerInfo.IsValid()
        && m_LocalIndex >= 0 && m_LocalIndex < RL_MAX_LOCAL_GAMERS;
}

int
rlGamerInfo::GetLocalIndex() const
{
    return m_LocalIndex;
}

bool
rlGamerInfo::IsLocal() const
{
    return m_PeerInfo.IsLocal();
}

bool
rlGamerInfo::IsRemote() const
{
    return m_PeerInfo.IsValid() && !m_PeerInfo.IsLocal();
}

bool
rlGamerInfo::IsSignedIn() const
{
    return this->IsLocal() ? m_IsSignedIn : true;
}

bool
rlGamerInfo::IsOnline() const
{
    return this->IsLocal() ? m_IsOnline : true;
}

const rlPeerInfo&
rlGamerInfo::GetPeerInfo() const
{
    return m_PeerInfo;
}

const rlGamerId&
rlGamerInfo::GetGamerId() const
{
    return m_GamerId;
}

const rlGamerHandle&
rlGamerInfo::GetGamerHandle() const
{
    return m_GamerHandle;
}

const char*
rlGamerInfo::GetName() const
{
    return m_Name;
}

bool
rlGamerInfo::HasDisplayName() const
{
#if RSG_DURANGO
    if((m_DisplayName[0] == '\0') && IsLocal())
    {
        PopulateLocalDisplayName();
    }
    return m_DisplayName[0] != '\0';
#else
    return true;
#endif
}

const char*
rlGamerInfo::GetDisplayName() const
{
#if RSG_DURANGO
    if((m_DisplayName[0] == '\0') && IsLocal())
    {
        PopulateLocalDisplayName();
    }

    // if we don't have the display name, this will fall back to returning GetName()
    if(HasDisplayName())
    {
        return m_DisplayName;
    }
#endif

    return GetName();
}

#if RSG_DURANGO
void
rlGamerInfo::PopulateLocalDisplayName() const
{
    if((m_DisplayName[0] == '\0') && IsLocal())
    {
        char displayName[RL_MAX_DISPLAY_NAME_BUF_SIZE] = { 0 };
        if(rlPresence::GetDisplayName(m_LocalIndex, displayName))
        {
            sysMemCpy((void*)&m_DisplayName, displayName, sizeof(m_DisplayName));
        }
    }
}

void
rlGamerInfo::SetDisplayName(rlDisplayName* displayName)
{
    safecpy(m_DisplayName, (char*)displayName, sizeof(m_DisplayName));
}
#endif

#if RSG_DURANGO || RSG_ORBIS
char*
rlGamerInfo::DisplayNameToUtf8(char(&out)[RL_MAX_DISPLAY_NAME_BUF_SIZE], const wchar_t *in)
{
    wchar_t name[RL_MAX_DISPLAY_NAME_LENGTH + 1] = { 0 }; // + 1 for null terminator

                                                            // In theory even in wchar_t some utf-16 letters could take more than 2 bytes so this logic is not 100% correct but we should be fine.
    size_t len = wcslen(in);

    if(len > RL_MAX_DISPLAY_NAME_LENGTH)
    {
        safecpy(name, in, RL_MAX_DISPLAY_NAME_LENGTH); // Do NOT put a -1 here safecpy adds the 0 as last 'char'

                                                        // Info: The following code should not be needed.
                                                        // It's there for safety as there was a case which I can't repro any longer where safecpy or safecat did misbehave and the ellipsis was not attached
        name[RL_MAX_DISPLAY_NAME_LENGTH] = 0;
        name[RL_MAX_DISPLAY_NAME_LENGTH - 1] = 0;

        safecat(name, L"\u2026"); // append an ellipsis

        len = RL_MAX_DISPLAY_NAME_LENGTH;
    }
    else // If it's the same length as 16+ellipsis (==17) we don't append the ellipsis and if it's smaller we don't need it
    {
        safecpy(name, in, RL_MAX_DISPLAY_NAME_LENGTH + 1);
    }

    return WideToUtf8(out, (char16*)name, static_cast<int>(len));
}
#endif //RSG_DURANGO || RSG_ORBIS

#if HACK_GTA4
void
rlGamerInfo::SetGamerInfoData(const rlPeerInfo    &peerInfo,
                                    const rlGamerId     &gamerId,
                                    const rlGamerHandle &gamerHandle,
                                    const int            localGamerIndex,
                                    const char          *name)
{
    m_PeerInfo = peerInfo;
    m_GamerId = gamerId;
    m_GamerHandle = gamerHandle;
    m_LocalIndex = localGamerIndex;

    safecpy(m_Name, name, sizeof(m_Name));
}
#endif

bool
rlGamerInfo::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    unsigned numBytes = 0;
    unsigned numGidBytes, numPiBytes, numGhBytes;

    if(m_GamerId.Export(buf, sizeofBuf, &numGidBytes)
        && m_PeerInfo.Export(&((u8*)buf)[numGidBytes], sizeofBuf - numGidBytes, &numPiBytes)
        && m_GamerHandle.Export(&((u8*)buf)[numGidBytes + numPiBytes], sizeofBuf - (numGidBytes + numPiBytes), &numGhBytes))
    {
        numBytes = numGidBytes + numPiBytes + numGhBytes;
        datBitBuffer bb;
        bb.SetReadWriteBytes(&((u8*)buf)[numBytes], sizeofBuf - numBytes);

        if(bb.WriteStr(m_Name, sizeof(m_Name))
            && bb.WriteInt(m_LocalIndex, datBitsNeeded<RL_MAX_LOCAL_GAMERS>::COUNT + 1))
        {
            numBytes += bb.GetNumBytesWritten();
        }
        else
        {
            numBytes = 0;
        }
    }

    if(size) { *size = numBytes; }

    return numBytes > 0;
}

bool
rlGamerInfo::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    unsigned numBytes = 0;
    unsigned numGidBytes, numPiBytes, numGhBytes;

#if RSG_DURANGO
    sysMemSet(m_DisplayName, 0, sizeof(m_DisplayName));
#endif

    if(m_GamerId.Import(buf, sizeofBuf, &numGidBytes)
        && m_PeerInfo.Import(&((const u8*)buf)[numGidBytes], sizeofBuf - numGidBytes, &numPiBytes)
        && m_GamerHandle.Import(&((const u8*)buf)[numGidBytes + numPiBytes], sizeofBuf - (numGidBytes + numPiBytes), &numGhBytes))
    {
        numBytes = numGidBytes + numPiBytes + numGhBytes;
        datBitBuffer bb;
        bb.SetReadOnlyBytes(&((const u8*)buf)[numBytes], sizeofBuf - numBytes);

        if(bb.ReadStr(m_Name, sizeof(m_Name))
            && bb.ReadInt(m_LocalIndex, datBitsNeeded<RL_MAX_LOCAL_GAMERS>::COUNT + 1))
        {
            numBytes += bb.GetNumBytesRead();
        }
        else
        {
            numBytes = 0;
        }
    }

    if(size) { *size = numBytes; }

    return numBytes > 0;
}

bool
rlGamerInfo::operator==(const rlGamerInfo& that) const
{
    rlAssert(m_GamerId != that.m_GamerId || m_PeerInfo == that.m_PeerInfo);
    return this->IsValid() && that.IsValid() && m_GamerId == that.m_GamerId;
}

bool
rlGamerInfo::operator!=(const rlGamerInfo& that) const
{
    return m_GamerId != that.m_GamerId;
}

}   //namespace rage
