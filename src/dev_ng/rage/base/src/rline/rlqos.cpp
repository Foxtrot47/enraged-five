// 
// rline/rlqos.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlqos.h"
#include "rldiag.h"
#include "diag/output.h"
#include "system/timer.h"
#include "net/connectionmanager.h"
#include "net/net.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, qos);
#undef __rage_channel
#define __rage_channel rline_qos

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "0x%016" I64FMT "x: " fmt, m_SessionInfo.GetToken().m_Value

NET_MESSAGE_IMPL(rlMsgQosProbeRequest);
NET_MESSAGE_IMPL(rlMsgQosProbeResponse);

static const unsigned QUERY_TIMEOUT_MS  = 500;

static const unsigned TUNNEL_TIMEOUT_MS = 5000;

void
rlQos::ProbeInfo::Reset()
{
    m_SendTime  = 0;
    m_Rtt       = 0;
    m_Result    = PROBE_RESULT_UNKNOWN;

    if(m_RespHandler.Pending())
    {
        m_RespHandler.Cancel();
    }

    m_UserDataSize = 0;
    sysMemSet(m_UserData, 0, sizeof(m_UserData));
}

rlQos::rlQos()
: m_State(STATE_INVALID)
, m_NetMode(RL_NETMODE_INVALID)
, m_CxnMgr(0)
, m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_NumProbesSent(0)
{
    for(int i = 0; i < NUM_PROBES; ++i)
    {
        m_Probes[i].m_RespHandler.Bind(this, &rlQos::OnResponse);
    }
}

rlQos::~rlQos()
{
    Cancel();
}

bool
rlQos::QueryPeer(const rlSessionInfo& sessionInfo,
                const netPeerAddress& peerAddr,
                const rlNetworkMode netMode,
                netConnectionManager* cxnMgr,
                const unsigned channelId)
{
    rlAssertf(!IsPending(), "Attempt to query a peer when another operation is pending");

    m_SessionInfo = sessionInfo;
    m_PeerAddr = peerAddr;
    m_NetMode = netMode;

    rlAssert(cxnMgr);
    m_CxnMgr    = cxnMgr;
    m_ChannelId = channelId;
    m_Transactor.Init(m_CxnMgr);

    rlDebug("Querying session...");

    m_State = STATE_OPEN_TUNNEL;

    return true;
}

void
rlQos::Update()
{
    m_TimeStep.SetTime(sysTimer::GetSystemMsTime());
    if(IsPending())
    {
        m_Transactor.Update(m_TimeStep.GetTimeStep());
    }

    switch(m_State)
    {
    case STATE_OPEN_TUNNEL:
		{
            m_NumProbesSent  = 0;
            for(int i = 0; i < NUM_PROBES; ++i)
            {
                m_Probes[i].Reset();
            }

			const netTunnelType tunnelType = 
				(RL_NETMODE_LAN == m_NetMode)
				? NET_TUNNELTYPE_LAN
				: NET_TUNNELTYPE_ONLINE;

			const netTunnelDesc tunnelDesc(tunnelType, NET_TUNNEL_REASON_QOS, m_SessionInfo.GetToken().GetValue(), false);

			if(m_CxnMgr->OpenTunnel(m_PeerAddr,
                                    tunnelDesc,
									&m_TunnelRqst,
									&m_Status))
			{
                m_Timeout.InitMilliseconds(TUNNEL_TIMEOUT_MS);
                m_State = STATE_OPENING_TUNNEL; 
            }
            else
            {
				rlDebug("Failed to open tunnel to peer.");
				Finish(false);
			}
		}
		break;
    case STATE_OPENING_TUNNEL:
        m_Timeout.Update();
        if(m_Status.Succeeded())
        {
            if(SendProbe())
            {            
                m_State = STATE_PROBING;
            }
            else
            {
                Finish(false);
            }
        }
        else
        {
            /*if(m_Timeout.IsTimedOut())
            {
                rlDebug("Timed out after %d seconds", m_Timeout.GetTimeoutIntervalSeconds());
                m_CxnMgr->CancelTunnelRequest(&m_TunnelRqst);
            }*/

            if(!m_Status.Pending())
            {
                Finish(false);
            }
        }
        break;
    case STATE_PROBING:
        if(PROBE_RESULT_PENDING != m_Probes[m_NumProbesSent-1].m_Result)
        {
            if(m_NumProbesSent < NUM_PROBES)
            {
                if(!SendProbe())
                {            
                    Finish(false);
                }
            }
            else
            {
                Finish(true);
            }
        }
        break;
    case STATE_INVALID:
    default:
        break;
    }
}

bool
rlQos::IsPending() const
{
	return m_State != STATE_INVALID;
}

void
rlQos::Cancel()
{
	if ( m_CxnMgr )
	{
		m_CxnMgr->CancelTunnelRequest(&m_TunnelRqst);
	}
    
    m_NumProbesSent  = 0;
    for(int i = 0; i < NUM_PROBES; ++i)
    {
        m_Probes[i].Reset();
    }

    m_Transactor.Shutdown();

    m_State = STATE_INVALID;
}

bool
rlQos::SendProbe()
{
    rlDebug("Sending probe: %d...", m_NumProbesSent);

    if(rlVerifyf(m_NumProbesSent < NUM_PROBES, "Too many probe attempts"))
    {
        rlMsgQosProbeRequest req;
        req.Reset(m_SessionInfo);

        unsigned curTime = sysTimer::GetSystemMsTime();

        if(m_Transactor.SendRequestOutOfBand(m_TunnelRqst.GetEndpointId(),
											m_ChannelId,
											NULL,
											req,
											QUERY_TIMEOUT_MS,
											&m_Probes[m_NumProbesSent].m_RespHandler))
        {
            m_Probes[m_NumProbesSent].m_SendTime = curTime;
            m_Probes[m_NumProbesSent].m_Result = PROBE_RESULT_PENDING;
            ++m_NumProbesSent;
            return true;
        }
        else
        {
            m_Probes[m_NumProbesSent].m_Result = PROBE_RESULT_ERROR;
            rlDebug("Error sending probe");
        }
    }

    return false;
}

//Helper for ReportResults
static int s_CompareRtt(const void *a, const void *b) 
{
    unsigned rttA = *(unsigned*)a;
    unsigned rttB = *(unsigned*)b;

	if (rttA > rttB)
		return 1;
	else if (rttA < rttB)
		return -1;
	else
		return 0;
}

void
rlQos::ReportResults()
{
    unsigned numAnswered = 0;
    QosResult result;

    unsigned rtt[NUM_PROBES];
    sysMemSet(rtt, 0, sizeof(rtt));

    u16 userDataSize = 0;
    u8 userData[RL_MAX_QOS_USER_DATA_SIZE];

    for(unsigned i = 0; i < NUM_PROBES; i++)
    {
        const ProbeInfo& p = m_Probes[i];
        if(p.m_Result == PROBE_RESULT_ANSWERED)
        {
            rtt[numAnswered++] = p.m_Rtt;

            userDataSize = p.m_UserDataSize;

            if(userDataSize)
            {
                rlAssert(p.m_UserDataSize <= RL_MAX_QOS_USER_DATA_SIZE);
                sysMemCpy(userData, p.m_UserData, userDataSize);
            }
        }
    }

    if(numAnswered)
    {
        unsigned medianRtt = rtt[0];

        if(numAnswered > 1)
        {
    	    qsort(rtt, numAnswered, sizeof(rtt[0]), s_CompareRtt);       
            medianRtt = rtt[numAnswered/2];
        }

        result.m_MinRtTimeMS    = rtt[0];
        result.m_MedRtTimeMS    = medianRtt;
        result.m_UpBitsPerSec   = 64000; //NOTE: Dummy value, because even MS says these aren't reliable
        result.m_DnBitsPerSec   = 64000; //NOTE: Dummy value, because even MS says these aren't reliable
        result.m_UserDataSize   = userDataSize;

        if(result.m_UserDataSize)
        {
            rlAssert(result.m_UserDataSize <= RL_MAX_QOS_USER_DATA_SIZE);
            sysMemCpy(result.m_UserData, userData, result.m_UserDataSize);
        }
    }

    if(numAnswered)
    {
        rlDebug("Query succeeded (%u answered, minRTT=%ums, medianRTT=%ums)",
                  numAnswered,
                  result.m_MinRtTimeMS,
                  result.m_MedRtTimeMS);

        this->Notify(NOTIFY_QOS_QUERY_SUCCEEDED, &result);
    }
    else
    {
        rlDebug("Query failed");
        this->Notify(NOTIFY_QOS_QUERY_FAILED, NULL);
    }
}

void
rlQos::Finish(const bool success)
{
    if(success)
    {
        ReportResults();    //Report our results
        Cancel();           //Reset everything
    }
    else
    {
        Cancel();           //Reset everything first
        ReportResults();    //Report the empty results
    }
}

void
rlQos::OnResponse(netTransactor* /*transactor*/,
                  netResponseHandler* handler,
                  const netResponse* resp)

{
    if(m_State != STATE_PROBING)
    {
        rlDebug("rlQos::OnResponse: Received probe response while not probing; ignoring...");
        return;
    }

    for(unsigned i = 0; i < NUM_PROBES; i++)
    {
        ProbeInfo& p = m_Probes[i];

        if(handler == &p.m_RespHandler)
        {
            if(resp->Answered())
            {
                rlMsgQosProbeResponse msg;
                if(msg.Import(resp->m_Data, resp->m_SizeofData))
                {
                    if(rlVerify(msg.m_UserDataSize <= RL_MAX_QOS_USER_DATA_SIZE))
                    {
                        p.m_UserDataSize = msg.m_UserDataSize;
                        if(p.m_UserDataSize)
                        {
                            sysMemCpy(p.m_UserData, msg.m_UserData, p.m_UserDataSize);
                        }
                    }
                    else
                    {
                        rlError("Userdata was too large (%u, max is %u); ignoring", 
                                 msg.m_UserDataSize, RL_MAX_QOS_USER_DATA_SIZE);
                    }
                }
                else
                {
                    rlError("Failed to import %s; userdata will be ignored", msg.GetMsgName());
                }

                p.m_Rtt = sysTimer::GetSystemMsTime() - p.m_SendTime;
                p.m_Result = PROBE_RESULT_ANSWERED;
                
                rlDebug("rlQos::OnResponse(" NET_ADDR_FMT "): Probe %d answered (RTT=%ums, userDataSize=%u)", 
                          NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr), i, p.m_Rtt, p.m_UserDataSize);
            }
            else if(resp->TimedOut())
            {
                p.m_Result = PROBE_RESULT_TIMED_OUT;
                rlDebug("rlQos::OnResponse(" NET_ADDR_FMT "): Probe %d timed out", 
                          NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr), i);
            }
            else if(resp->Complete())
            {
                p.m_Result = PROBE_RESULT_ERROR;
                rlDebug("rlQos::OnResponse(" NET_ADDR_FMT "): Got invalid response for probe %d", 
                         NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr), i);
            }

            return;
        }
    }

    rlDebug("rlQos::OnResponse: Received response for unknown handler");
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

}   //namespace rage
