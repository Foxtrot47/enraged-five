// 
// rline/rlcloudsave.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "data/base64.h"
#include "diag/seh.h"
#include "file/device.h"
#include "parser/treenode.h"
#include "rline/cloudsave/rlcloudsave.h"
#include "rline/rltask.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "rline/rltitleid.h"
#include "system/param.h"
#include "system/timer.h"
#include "wolfssl/wolfcrypt/md5.h"

#define CLOUDSAVE_DEFAULT_TIMEOUT (60)

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rline, cloudsave)
	#undef __rage_channel
	#define __rage_channel rline_cloudsave

	extern const rlTitleId* g_rlTitleId;

	// Error handling macros
#define ERROR_CASE(code, codeEx, resCode)	\
	if(result.IsError(code, codeEx))		\
	{										\
		resultCode = resCode;				\
		return;								\
	}

#define WARNING_CASE(code, codeEx)			\
	if(result.IsError(code, codeEx))		\
	{										\
		resultCode = 0;						\
		return;								\
	}

	static u64 s_CloudSavesMaxFileSize = rlCloudSaveFile::DEFAULT_MAX_FILE_SIZE;

	// Task Manager for the Cloud Save Worker Thread
	rlTaskManager rlCloudSave::s_WorkerTaskManager;

	// Critical Section makes task queuing and task manager execution mutually exclusive.
	sysCriticalSectionToken rlCloudSave::s_WorkerThreadCs;

	// Thread ID for the Cloud Saves Worker Thread
	sysIpcThreadId rlCloudSave::s_WorkerThreadId = sysIpcThreadIdInvalid;

	// Wakeup Event - The Worker thread will pause until receiving this event when the task manager is idle.
	sysIpcEvent rlCloudSave::s_WakeupEvent = NULL;

	// State values - If the system is initialized, and if the worker thread should run.
	bool rlCloudSave::s_Initialized = false;
	bool rlCloudSave::s_RunWorkerThread = false;

	// rline allocator - external
	extern sysMemAllocator* g_rlAllocator;

	//////////////////////////////////////////////////////////////////////////
	//  rlCloudSave
	//////////////////////////////////////////////////////////////////////////
	bool rlCloudSave::Init()
	{
		// rlInit model expects Init() functions to return bool, so if future functionality allows this
		//	to fail a 'false' value could be returned. For now, set the initialized flag and return true.
		s_Initialized = true;
		return true;
	}

	void rlCloudSave::Shutdown()
	{
		// Reset the initialized and worker thread bools. The worker thread will stop executing
		//	when this is false.
		s_Initialized = false;

		// Shutdown the worker thread.
		ShutdownWorker();
	}

	void rlCloudSave::SetMaximumFileSize(u64 fileSize)
	{
		s_CloudSavesMaxFileSize = fileSize;
	}

	bool rlCloudSave::GetCloudSaveManifest(const int localGamerIndex, rlCloudSaveManifest* manifest, const char* titleAccessToken, netStatus* status)
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		bool success = false;

		rlGetCloudSaveManifestTask* task = NULL;

		// Get the task manager - either the worker thread task manager, or the global rline rlGetTaskManager()
		rlTaskManager* manager = GetTaskManager();

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

			// Validate manifest -- titleAccessToken can be null
			rverify(manifest, catchall, );

			rverify(manager->CreateTask(&task),catchall,);
			rverify(rlTaskBase::Configure(task,localGamerIndex,manifest,titleAccessToken,status),catchall,);
			rverify(manager->AppendSerialTask(task), catchall,);

			// Signal the worker thread. If no worker thread is present, this is a no-op. Otherwise,
			//	the worker will wait on this event to free up resources.
			SignalWorker();
			success = true;
		}
		rcatchall
		{
			if(task)
			{
				manager->DestroyTask(task);
			}
		}

		return success;
	}

	bool rlCloudSave::GetFile(const int localGamerIndex, rlCloudSaveFile* file, eCloudSaveResolveType resolveType, const char* titleAccessToken, netStatus* status)
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		bool success = false;

		rlGetCloudSaveFileTask* task = NULL;

		// Get the task manager - either the worker thread task manager, or the global rline rlGetTaskManager()
		rlTaskManager* manager = GetTaskManager();

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

			// Validate file -- titleAccessToken can be null
			rverify(file, catchall, );

			rverify(manager->CreateTask(&task),catchall,);
			rverify(rlTaskBase::Configure(task,localGamerIndex,file,resolveType,titleAccessToken,status),catchall,);
			rverify(manager->AppendSerialTask(task), catchall,);

			// Signal the worker thread. If no worker thread is present, this is a no-op. Otherwise,
			//	the worker will wait on this event to free up resources.
			SignalWorker();

			success = true;
		}
		rcatchall
		{
			if(task)
			{
				manager->DestroyTask(task);
			}
		}

		return success;
	}

	bool rlCloudSave::DownloadFile(const int localGamerIndex, rlCloudSaveFile* file, eCloudSaveResolveType resolveType, const char* filePathUtf8, 
									u8* bounceBuffer, const unsigned bounceBufLen, const char* titleAccessToken, netStatus* status)
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		bool success = false;

		rlGetCloudSaveFileTask* task = NULL;

		// Get the task manager - either the worker thread task manager, or the global rline rlGetTaskManager()
		rlTaskManager* manager = GetTaskManager();

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

			// Validate file and filePath -- titleAccessToken can be null
			rverify(file, catchall, );
			rverify(filePathUtf8 && filePathUtf8[0] != '\0', catchall, );

			task = manager->CreateTask<rlGetCloudSaveFileTask>(bounceBuffer, bounceBufLen);
			rverify(task,catchall,);
			rverify(rlTaskBase::Configure(task,localGamerIndex,file,resolveType,filePathUtf8,titleAccessToken,status),catchall,);
			rverify(manager->AppendSerialTask(task), catchall,);

			// Signal the worker thread. If no worker thread is present, this is a no-op. Otherwise,
			//	the worker will wait on this event to free up resources.
			SignalWorker();

			success = true;
		}
		rcatchall
		{
			if(task)
			{
				manager->DestroyTask(task);
			}
		}

		return success;
	}

	bool rlCloudSave::PostFile(const int localGamerIndex, rlCloudSaveFile* file, eCloudSaveResolveType resolveType, const char* titleAccessToken, netStatus* status)
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		bool success = false;

		rlPostCloudSaveFileTask* task = NULL;

		// Get the task manager - either the worker thread task manager, or the global rline rlGetTaskManager()
		rlTaskManager* manager = GetTaskManager();

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

			// Validate file -- titleAccessToken can be null
			rverify(file, catchall, );
			rverify(file->GetDataLength() <= s_CloudSavesMaxFileSize, catchall, );

			rverify(manager->CreateTask(&task),catchall,);
			rverify(rlTaskBase::Configure(task,localGamerIndex,file,resolveType,titleAccessToken,status),catchall,);
			rverify(manager->AppendSerialTask(task), catchall,);

			// Signal the worker thread. If no worker thread is present, this is a no-op. Otherwise,
			//	the worker will wait on this event to free up resources.
			SignalWorker();

			success = true;
		}
		rcatchall
		{
			if(task)
			{
				manager->DestroyTask(task);
			}
		}

		return success;
	}

	bool rlCloudSave::UnregisterFile(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* titleAccessToken, netStatus* status)
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		bool success = false;

		rlUnregisterCloudSaveFileTask* task = NULL;

		// Get the task manager - either the worker thread task manager, or the global rline rlGetTaskManager()
		rlTaskManager* manager = GetTaskManager();

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

			// Validate saveFile -- titleAccessToken can be null
			rverify(saveFile, catchall, );

			rverify(manager->CreateTask(&task),catchall,);
			rverify(rlTaskBase::Configure(task,localGamerIndex,saveFile,resolveType,titleAccessToken,status),catchall,);
			rverify(manager->AppendSerialTask(task), catchall,);

			// Signal the worker thread. If no worker thread is present, this is a no-op. Otherwise,
			//	the worker will wait on this event to free up resources.
			SignalWorker();

			success = true;
		}
		rcatchall
		{
			if(task)
			{
				manager->DestroyTask(task);
			}
		}

		return success;
	}

	const char* rlCloudSave::GetResolveTypeString(eCloudSaveResolveType resolveType)
	{
		#define CLOUD_RESOLVE_TO_STR(x) case x: return #x;

		switch(resolveType)
		{
		CLOUD_RESOLVE_TO_STR(None)
		CLOUD_RESOLVE_TO_STR(AcceptRemote)
		CLOUD_RESOLVE_TO_STR(AcceptLocal)
		}

		#undef CLOUD_RESOLVE_TO_STR

		return "None";
	}

	bool rlCloudSave::VerifyMd5Hash(const char* fullPathUtf8, const char* expectedMd5Hash, netStatus* /*status*/)
	{
		rtry
		{
			rverify(fullPathUtf8 && fullPathUtf8[0] != '\0', catchall, );
			rverify(expectedMd5Hash && expectedMd5Hash[0] != '\0', catchall, );
			//rverify(status, catchall, );

			const fiDevice* device = fiDevice::GetDevice(fullPathUtf8);
			rverify(device, catchall, );

			fiHandle hFile = device->Open(fullPathUtf8, true);
			rverify(fiIsValidHandle(hFile), catchall, );

			wc_Md5 md5;

			wc_InitMd5(&md5);

			u8 readBuf[1024] = {0};
			const u8* buffer = &readBuf[0];

			int bytesRead = 0;
			do
			{
				bytesRead = device->Read(hFile, readBuf, COUNTOF(readBuf));
				if (bytesRead > 0)
				{
					wc_Md5Update(&md5, buffer, bytesRead);
				}
			} while (bytesRead > 0);

			byte digest[WC_MD5_DIGEST_SIZE] = {0};
			wc_Md5Final(&md5, digest);

			device->Close(hFile);

			char digestBase64[DAT_BASE64_MAX_ENCODED_SIZE(sizeof(digest))] = {0};
			rverify(datBase64::Encode(digest, sizeof(digest), digestBase64, sizeof(digestBase64), NULL), catchall, );

			rverify(stricmp(digestBase64, expectedMd5Hash) == 0, catchall, );

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlCloudSave::CalculateMd5Hash(const char* fullPathUtf8, char (&out_hash)[rlCloudSaveFile::MD5_HASH_BUF_SIZE])
	{
		rtry
		{
			rverify(fullPathUtf8 && fullPathUtf8[0] != '\0', catchall, );

			const fiDevice* device = fiDevice::GetDevice(fullPathUtf8);
			rverify(device, catchall, );

			fiHandle hFile = device->Open(fullPathUtf8, true);
			rverify(fiIsValidHandle(hFile), catchall, );

			wc_Md5 md5;

			wc_InitMd5(&md5);

			u8 readBuf[1024] = {0};
			const u8* buffer = &readBuf[0];

			int bytesRead = 0;
			do
			{
				bytesRead = device->Read(hFile, readBuf, COUNTOF(readBuf));
				if (bytesRead > 0)
				{
					wc_Md5Update(&md5, buffer, bytesRead);
				}
			} while (bytesRead > 0);

			byte digest[WC_MD5_DIGEST_SIZE] = {0};
			wc_Md5Final(&md5, digest);

			device->Close(hFile);

			rverify(datBase64::Encode(digest, sizeof(digest), out_hash, sizeof(out_hash), NULL), catchall, );
			return true;
		}
		rcatchall
		{
			out_hash[0] = '\0';
			return false;
		}
	}
	
	bool rlCloudSave::QueryHasBetaAccess(const int localGamerIndex, const char* cohortsCloudFile, bool* resultsPtr, netStatus* status)
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		bool success = false;

		rlQueryCloudSaveBetaAccess* task = NULL;

		// Get the task manager - either the worker thread task manager, or the global rline rlGetTaskManager()
		rlTaskManager* manager = GetTaskManager();

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(cohortsCloudFile && cohortsCloudFile[0] != '\0', catchall, );
			rverify(resultsPtr, catchall, );

			rverify(manager->CreateTask(&task),catchall,);
			rverify(rlTaskBase::Configure(task,localGamerIndex,cohortsCloudFile,resultsPtr,status),catchall,);
			rverify(manager->AppendSerialTask(task), catchall,);

			// Signal the worker thread. If no worker thread is present, this is a no-op. Otherwise,
			//	the worker will wait on this event to free up resources.
			SignalWorker();

			success = true;
		}
		rcatchall
		{
			if(task)
			{
				manager->DestroyTask(task);
			}
		}

		return success;
	}

	bool rlCloudSave::GetFileMetadata(const int localGamerIndex, rlCloudSaveFile* saveFile, const char* titleAccessToken, netStatus* status)
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		bool success = false;

		rlGetCloudSaveMetadataTask* task = NULL;

		// Get the task manager - either the worker thread task manager, or the global rline rlGetTaskManager()
		rlTaskManager* manager = GetTaskManager();

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

			// Validate saveFile -- titleAccessToken can be null
			rverify(saveFile, catchall, );

			rverify(manager->CreateTask(&task),catchall,);
			rverify(rlTaskBase::Configure(task,localGamerIndex,saveFile,titleAccessToken,status),catchall,);
			rverify(manager->AppendSerialTask(task), catchall,);

			// Signal the worker thread. If no worker thread is present, this is a no-op. Otherwise,
			//	the worker will wait on this event to free up resources.
			SignalWorker();

			success = true;
		}
		rcatchall
		{
			if(task)
			{
				manager->DestroyTask(task);
			}
		}

		return success;
	}

	void rlCloudSave::CloudSaveWorker(void* )
	{
		rlDebug1("Cloud Save Worker Thread - Begin");

		// Initialize the task manager with the global rline allocator
		s_WorkerTaskManager.Init(g_rlAllocator);

		// While the CloudSave system is initialized, and the worker thread is running.
		//	s_Initialized is reset to false when rlCloudSave::Shutdown() is called.
		//	s_RunWorkerThread is called during shutdown, or when rlCloudSave::ShutdownWorker() is called.
		while(s_Initialized && s_RunWorkerThread)
		{
			bool bHasAnyTasks = false;

			// Scope the critical section to the task manager update and query if tasks are pending.
			//	We don't want to have the critical section locekd when we do the sysIpcWaitEvent below,
			//  or we'd encounter deadlock.
			{
				SYS_CS_SYNC(s_WorkerThreadCs);
				s_WorkerTaskManager.Update();
				bHasAnyTasks = s_WorkerTaskManager.HasAnyPendingTasks();
			}
			
			// If no tasks are queued, wait until the wakeup event is signaled. When a task is created/queued,
			//	this wakeup event is set to its signaled state. 
			if (!bHasAnyTasks && rlVerify(s_WakeupEvent))
			{
				sysIpcWaitEvent(s_WakeupEvent);
			}
			else
			{
				sysIpcSleep(0);
			}
		}

		// Shutdown the task manager.
		s_WorkerTaskManager.Shutdown();

		rlDebug1("Cloud Save Worker Thread - End");
	}

	void rlCloudSave::CreateWorker()
	{
		SYS_CS_SYNC(s_WorkerThreadCs);

		// Ensure that the worker thread is not created twice.
		if(rlVerify(s_WorkerThreadId == sysIpcThreadIdInvalid))
		{
			rlDebug1("Creating cloud save worker thread.");

			// Create the wakeup event
			s_WakeupEvent = sysIpcCreateEvent();

			// Set the 'run worker thread' flag to true. The worker thread will continue to run
			// until this is reset to false, or the rlCloudSave::Shutdown() is called.
			s_RunWorkerThread = true;

			// Create the worker thread.
			s_WorkerThreadId = sysIpcCreateThread(&rlCloudSave::CloudSaveWorker, NULL, sysIpcMinThreadStackSize, PRIO_NORMAL, "[RAGE] CloudSave Worker", 0, "RageNetCloudSaveWorker");
		}
	}

	void rlCloudSave::ShutdownWorker()
	{
		// Simply clears the 'run worker thread' flag allowing the thread to shut down.
		s_RunWorkerThread = false;

		// Signal the worker in case it is asleep, so it will continue and shut down.
		SignalWorker();

		// Wait for the worker thread to exit.
		if (s_WorkerThreadId != sysIpcThreadIdInvalid)
		{
			sysIpcWaitThreadExit(s_WorkerThreadId);
			s_WorkerThreadId = sysIpcThreadIdInvalid;
		}
	}

	void rlCloudSave::SignalWorker()
	{
		// Ensure the event was created properly before attempting to set it.
		if (s_WakeupEvent != NULL)
		{
			sysIpcSetEvent(s_WakeupEvent);
		}	
	}

	rlTaskManager* rlCloudSave::GetTaskManager()
	{
		// If the worker create is active, cloud save tasks should be created using the
		//	worker thread's task manager. Otherwise, default to rlGetTaskManager()
		if (s_WorkerThreadId != sysIpcThreadIdInvalid)
		{
			return &s_WorkerTaskManager;
		}
		else
		{
			return rlGetTaskManager();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//  rlCloudSaveManifest
	//////////////////////////////////////////////////////////////////////////
	rlCloudSaveManifest::rlCloudSaveManifest()
		: m_BytesUsed(0)
	{

	}

	rlCloudSaveManifest::~rlCloudSaveManifest()
	{
		Clear();
	}

	void rlCloudSaveManifest::Clear()
	{
		while(!m_SaveFiles.IsEmpty())
		{
			rlCloudSaveFile* file = m_SaveFiles.Pop();
			delete file;
		}

		m_BytesUsed = 0;
	}

	u32 rlCloudSaveManifest::GetCount() const 
	{ 
		return m_SaveFiles.GetCount(); 
	}

	void rlCloudSaveManifest::SetBytesUsed(u64 bytesUsed)
	{
		m_BytesUsed = bytesUsed;
	}

	u64 rlCloudSaveManifest::GetBytesUsed() const
	{
		return m_BytesUsed;
	}

	bool rlCloudSaveManifest::AddFile(rlCloudSaveFile* file)
	{
		if (rlVerify(!m_SaveFiles.IsFull()))
		{
			m_SaveFiles.Push(file);
			return true;
		}
		
		return false;
	}

	rlCloudSaveFile* rlCloudSaveManifest::GetSaveFile(unsigned index) 
	{ 
		if (rlVerify(index < GetCount()))
		{
			return m_SaveFiles[index]; 
		}

		return NULL; 
	}

	//////////////////////////////////////////////////////////////////////////
	//  rlCloudSaveFile
	//////////////////////////////////////////////////////////////////////////
	rlCloudSaveFile::rlCloudSaveFile()
		: m_Data(NULL)
		, m_DataLen(0)
	{
		Clear();
	}

	rlCloudSaveFile::~rlCloudSaveFile()
	{
		Clear();
	}

	void rlCloudSaveFile::Clear()
	{
		m_ImportVersion = 0;
		m_FileId = 0;
		m_FileVersion = 0;
		m_NextExpectedVersion = 0;
		m_ClientLastModifiedDate = 0;
		m_ServerLastModifiedDate = 0;
		m_FileName[0] = '\0';
		m_Md5Hash[0] = '\0';
		m_HardwareId[0] = '\0';
		m_MetaData[0] = '\0';
		m_ServerMetaData[0] = '\0';
		m_ClientFileSize = 0;
		m_ServerFileSize = 0;
		m_ServerLatestVersion = 0;

		m_IpAddress.Clear();

		FreeSaveData();
	}

	void rlCloudSaveFile::FreeSaveData()
	{
		if (m_Data)
		{
			sysMemVirtualFree(m_Data);
			m_Data = NULL;
		}

		m_DataLen = 0;
	}

	bool rlCloudSaveFile::LoadData(const char* path)
	{
		rtry
		{
			rverify(m_Data == NULL, catchall, );

			const fiDevice* device = fiDevice::GetDevice(path);
			rverify(device, catchall, );
					
			// extract file size
			u32 fileSize = (u32)device->GetFileSize(path);
			rverify(fileSize > 0, catchall, );

			// make sure the file is reasonably sized
			rverify(fileSize < s_CloudSavesMaxFileSize, catchall, );

			// Allocate memory for the file
			m_Data = (u8*)sysMemVirtualAllocate(sizeof(u8) * fileSize);
			rverify(m_Data, catchall, );
			m_DataLen = fileSize;

			// open file for reading
			fiHandle hFile = device->Open(path, true);
			rverify(fiIsValidHandle(hFile), catchall, );

			// read the manifest and import
			int bytesRead = device->Read(hFile, m_Data, fileSize);
			device->Close(hFile);

			rverify(bytesRead == (int)fileSize, catchall, );
			
			rlDebug3("Cloud save file: read '%s' as %d bytes", path, fileSize);

			return true;
		}
		rcatchall
		{
			FreeSaveData();
			return false;
		}
	}

	void rlCloudSaveFile::SetFileId(u64 fileId)
	{
		m_FileId = fileId;
	}

	u64 rlCloudSaveFile::GetFileId() const
	{
		return m_FileId;
	}

	void rlCloudSaveFile::SetMd5Hash(const char* md5Hash)
	{
		safecpy(m_Md5Hash, md5Hash);
	}

	const char* rlCloudSaveFile::GetMd5Hash() const
	{
		return m_Md5Hash;
	}

	const char* rlCloudSaveFile::GetFileName() const
	{
		return m_FileName;
	}

	void rlCloudSaveFile::SetFileName(const char* fileName)
	{
		safecpy(m_FileName, fileName);
	}

	void rlCloudSaveFile::SetFileVersion(u64 version)
	{
		m_FileVersion = version;
	}

	u64 rlCloudSaveFile::GetFileVersion() const
	{
		return m_FileVersion;
	}

	void rlCloudSaveFile::SetNextExpectedVersion(u64 version)
	{
		m_NextExpectedVersion = version;
	}

	u64 rlCloudSaveFile::GetNextExpectedVersion() const
	{
		return m_NextExpectedVersion;
	}

	void rlCloudSaveFile::SetClientLastModifiedDate(u64 date)
	{
		m_ClientLastModifiedDate = date;
	}

	u64 rlCloudSaveFile::GetClientLastModifiedDate() const
	{
		return m_ClientLastModifiedDate;
	}

	void rlCloudSaveFile::SetServerLastModifiedDate(u64 date)
	{
		m_ServerLastModifiedDate = date;
	}

	u64 rlCloudSaveFile::GetServerLastModifiedDate() const
	{
		return m_ServerLastModifiedDate;
	}

	void rlCloudSaveFile::SetHardwareId(const char* hardwareId)
	{
		safecpy(m_HardwareId, hardwareId);
	}

	const char* rlCloudSaveFile::GetHardwareId() const
	{
		return m_HardwareId;
	}

	void rlCloudSaveFile::SetLastIpAddress(netIpAddress& address)
	{
		m_IpAddress = address;
	}

	const netIpAddress& rlCloudSaveFile::GetLastIpAddress() const
	{
		return m_IpAddress;
	}

	void rlCloudSaveFile::CopyData(u8* data, unsigned dataLen)
	{
		if (rlVerify(m_Data == NULL) && 
			rlVerify(data) && 
			rlVerify(dataLen > 0) &&
			rlVerify(dataLen <= s_CloudSavesMaxFileSize))
		{
			m_Data = RL_NEW_ARRAY(rlCloudSaveFile, u8, dataLen);
			if (m_Data)
			{
				sysMemCpy(m_Data, data, dataLen);
				m_DataLen = dataLen;
			}
		}
	}

	u8* rlCloudSaveFile::GetData() const
	{
		return m_Data;
	}

	u32 rlCloudSaveFile::GetDataLength() const
	{
		return m_DataLen;
	}

	const char* rlCloudSaveFile::GetMetaData()
	{
		return m_MetaData;
	}

	void rlCloudSaveFile::SetMetaData(const char* metaData)
	{
		safecpy(m_MetaData, metaData);
	}

	const char* rlCloudSaveFile::GetServerMetadata() const
	{
		return m_ServerMetaData;
	}

	void rlCloudSaveFile::SetServerMetadata(const char* metaData)
	{
		safecpy(m_ServerMetaData, metaData);
	}

	void rlCloudSaveFile::SetClientFileSize(u64 fileSize)
	{
		m_ClientFileSize = fileSize;
	}

	u64 rlCloudSaveFile::GetClientFileSize() const
	{
		return m_ClientFileSize;
	}

	void rlCloudSaveFile::SetServerFileSize(u64 fileSize)
	{
		m_ServerFileSize = fileSize;
	}

	u64 rlCloudSaveFile::GetServerFileSize() const
	{
		return m_ServerFileSize;
	}

	void rlCloudSaveFile::SetLatestServerVersion(u64 fileVersion)
	{
		m_ServerLatestVersion = fileVersion;
	}

	u64 rlCloudSaveFile::GetLatestServerVersion() const
	{
		return m_ServerLatestVersion;
	}

	bool rlCloudSaveFile::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
	{
		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		sysMemSet(buf, 0, sizeofBuf);

		bool success = false;

		rtry
		{
			// Export as the latest version: version 1
			unsigned exportVersion = VERSION_1;
			rverify(bb.WriteUns(exportVersion, sizeof(exportVersion) << 3), catchall, );

			// Export our data
			rverify(bb.WriteUns(m_FileId, sizeof(m_FileId) << 3), catchall, );
			rverify(bb.WriteBytes(m_FileName, sizeof(m_FileName)), catchall, );
			rverify(bb.WriteBytes(m_Md5Hash, sizeof(m_Md5Hash)), catchall, );
			rverify(bb.WriteUns(m_FileVersion, sizeof(m_FileVersion) << 3), catchall, );
			rverify(bb.WriteUns(m_NextExpectedVersion, sizeof(m_NextExpectedVersion) << 3), catchall, );
			rverify(bb.WriteUns(m_ClientLastModifiedDate, sizeof(m_ClientLastModifiedDate) << 3), catchall, );
			rverify(bb.WriteUns(m_ServerLastModifiedDate, sizeof(m_ServerLastModifiedDate) << 3), catchall, );
			rverify(bb.WriteBytes(m_HardwareId, sizeof(m_HardwareId)), catchall, );

			// Note:
			//	Previously was naively serializing the netIpAddress structure -- since this structure can change
			//	(in fact, does on a later title), replicate the original netIpV4Address import/export functionality.
			const netIpV4Address& ipv4 = m_IpAddress.ToV4();
			rverify(bb.WriteUns(ipv4.ToU32(), 32), catchall, );

			rverify(bb.WriteBytes(m_MetaData, sizeof(m_MetaData)), catchall, );
			rverify(bb.WriteBytes(m_ServerMetaData, sizeof(m_ServerMetaData)), catchall, );
			rverify(bb.WriteUns(m_ServerFileSize, sizeof(m_ServerFileSize) << 3), catchall, );
			rverify(bb.WriteUns(m_ClientFileSize, sizeof(m_ClientFileSize) << 3), catchall, );
			success = true;
		}
		rcatchall
		{

		}

		rlAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

		if(size)
		{
			*size = success ? bb.GetNumBytesWritten() : 0;
		}

		return success;
	}

	bool rlCloudSaveFile::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
	{
		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		Clear();

		bool success = false;

		rtry
		{
			// Read the import version and validate it
			rverify(bb.ReadUns(m_ImportVersion, sizeof(m_ImportVersion) << 3), catchall, );
			rverify(IsValidExportVersion(m_ImportVersion), catchall, );

			// Additional import versions would be handled below.
			if (m_ImportVersion == VERSION_1)
			{
				rverify(bb.ReadUns(m_FileId, sizeof(m_FileId) << 3), catchall, );
				rverify(bb.ReadBytes(m_FileName, sizeof(m_FileName)), catchall, );
				rverify(bb.ReadBytes(m_Md5Hash, sizeof(m_Md5Hash)), catchall, );
				rverify(bb.ReadUns(m_FileVersion, sizeof(m_FileVersion) << 3), catchall, );
				rverify(bb.ReadUns(m_NextExpectedVersion, sizeof(m_NextExpectedVersion) << 3), catchall, );
				rverify(bb.ReadUns(m_ClientLastModifiedDate, sizeof(m_ClientLastModifiedDate) << 3), catchall, );
				rverify(bb.ReadUns(m_ServerLastModifiedDate, sizeof(m_ServerLastModifiedDate) << 3), catchall, );
				rverify(bb.ReadBytes(m_HardwareId, sizeof(m_HardwareId)), catchall, );
				
				// Note:
				//	Previously was naively serializing the netIpAddress structure -- since this structure can change
				//	(in fact, does on a later title), replicate the original netIpV4Address import/export functionality.
				unsigned ip;
				rverify(bb.ReadUns(ip, 32), catchall, );
				const netIpV4Address ipv4(ip);
				m_IpAddress = ipv4;

				rverify(bb.ReadBytes(m_MetaData, sizeof(m_MetaData)), catchall, );
				rverify(bb.ReadBytes(m_ServerMetaData, sizeof(m_ServerMetaData)), catchall, );
				rverify(bb.ReadUns(m_ServerFileSize, sizeof(m_ServerFileSize) << 3), catchall, );
				rverify(bb.ReadUns(m_ClientFileSize, sizeof(m_ClientFileSize) << 3), catchall, );
				success = true;
			}
			else
			{
				success = false;
			}
		}
		rcatchall
		{
			Clear();
		}

		if(size)
		{
			*size = success ? bb.GetNumBytesRead() : 0;
		}

		return success;
	}

	bool rlCloudSaveFile::IsMd5HashValidForData()
	{
		rtry
		{
			rverify(m_Data, catchall, );

			wc_Md5 md5;

			wc_InitMd5(&md5);

			wc_Md5Update(&md5, m_Data, m_DataLen);

			byte digest[WC_MD5_DIGEST_SIZE] = {0};
			wc_Md5Final(&md5, digest);

			char digestBase64[DAT_BASE64_MAX_ENCODED_SIZE(sizeof(digest))] = {0};
			rverify(datBase64::Encode(digest, sizeof(digest), digestBase64, sizeof(digestBase64), NULL), catchall, );

			rverify(stricmp(digestBase64, m_Md5Hash) == 0, catchall, );

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//  rlGetCloudSaveManifestTask
	//////////////////////////////////////////////////////////////////////////
	rlGetCloudSaveManifestTask::rlGetCloudSaveManifestTask()
		: m_Manifest(NULL)
	{

	}

	rlGetCloudSaveManifestTask::~rlGetCloudSaveManifestTask()
	{

	}

	bool rlGetCloudSaveManifestTask::Configure(const int localGamerIndex, rlCloudSaveManifest* manifest, const char* titleAccessToken)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(manifest, catchall, );
			rverify(manifest->GetCount() == 0, catchall, );

			m_TimeOutSecs = CLOUDSAVE_DEFAULT_TIMEOUT;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			m_Manifest = manifest;

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall,);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

			if (titleAccessToken != NULL)
			{
				rverify(AddStringParameter("titleAccessToken", titleAccessToken, true), catchall, );
			}
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlGetCloudSaveManifestTask::Finish(const FinishType finishType, const int resultCode /* = 0 */)
	{
		// Extract debugging information for error reporting.
		if (m_Manifest)
		{
			netHttpRequest::DebugInfo& debugInfo = m_Manifest->GetHttpDebugInfo();
			m_HttpRequest.GetDebugInfo(debugInfo);
		}

		rlHttpTask::Finish(finishType, resultCode);
	}

	bool rlGetCloudSaveManifestTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
	{
		rtry
		{
			rverify(node, catchall, rlTaskError("No valid XML node to process"));

			RockstarId rockstarId;
			rverify(rlHttpTaskHelper::ReadInt64(rockstarId, node, "RockstarId", NULL), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(GetLocalGamerIndex());
			rverify(cred.IsValid(), catchall, );
			rverify(cred.GetRockstarId() == rockstarId, catchall, resultCode = RLCS_ERROR_INVALIDARGUMENT_BAD_ROCKSTARID);

			u64 bytesUsed;
			rverify(rlHttpTaskHelper::ReadUInt64(bytesUsed, node, "BytesUsed", NULL), catchall, );
			m_Manifest->SetBytesUsed(bytesUsed);

			const parTreeNode* fileNode = node->FindChildWithName("Files");
			if (fileNode != NULL)
			{
				parTreeNode::ChildNodeIterator iter = fileNode->BeginChildren();
				for( ; iter != fileNode->EndChildren(); ++iter)
				{
					rlCloudSaveFile* file = rage_new rlCloudSaveFile();
					m_Manifest->AddFile(file);

					parTreeNode* curNode = *iter;
					
					u64 fileId;
					rverify(rlHttpTaskHelper::ReadUInt64(fileId, curNode, NULL, "Id"), catchall, );
					rlAssert(fileId > 0);
					file->SetFileId(fileId);

					u64 fileVersion;
					rverify(rlHttpTaskHelper::ReadUInt64(fileVersion, curNode, NULL, "Version"), catchall, );
					file->SetFileVersion(fileVersion);

					u64 nextExpectedVersion;
					rverify(rlHttpTaskHelper::ReadUInt64(nextExpectedVersion, curNode, NULL, "NextVersion"), catchall, );
					rlAssert(nextExpectedVersion > 0);
					file->SetNextExpectedVersion(nextExpectedVersion);

					u64 fileSize;
					rverify(rlHttpTaskHelper::ReadUInt64(fileSize, curNode, NULL, "Size"), catchall, );
					rlAssert(fileSize > 0);
					file->SetServerFileSize(fileSize);

					const char* filePath = rlHttpTaskHelper::ReadString(curNode, NULL, "Path");
					rverify(filePath, catchall, );
					file->SetFileName(filePath);

					const char* serverLastModTime = rlHttpTaskHelper::ReadString(curNode, NULL, "ServerLastModifiedUtc");
					rverify(serverLastModTime, catchall, );
					u64 serverLastModTime64 = netHttpRequest::ParseDateString(serverLastModTime);
					rlAssert(serverLastModTime64 > 0);
					file->SetServerLastModifiedDate(serverLastModTime64);

					const char* lastHardwareId = rlHttpTaskHelper::ReadString(curNode, NULL, "LastHardwareId");
					rverify(lastHardwareId, catchall, );
					file->SetHardwareId(lastHardwareId);

					const char* md5Hash = rlHttpTaskHelper::ReadString(curNode, NULL, "MD5Hash");
					rverify(md5Hash, catchall, );
					file->SetMd5Hash(md5Hash);

					const char* lastIP = rlHttpTaskHelper::ReadString(curNode, NULL, "LastIP");
					rverify(lastIP, catchall, );
					netIpAddress address;
					if (address.FromString(lastIP))
					{
						file->SetLastIpAddress(address);
					}
				}
			}
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlGetCloudSaveManifestTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("InvalidArgument", "", RLCS_ERROR_INVALIDARGUMENT);
		ERROR_CASE("Exception", "", RLCS_ERROR_EXCEPTION);
		ERROR_CASE("SqlException", "UnknownError", RLCS_ERROR_SQLEXCEPTION_UNKNOWN);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLCS_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlGetCloudSaveManifestTask::GetServiceMethod() const
	{
		return "cloudsave.asmx/GetCloudSaveManifest";
	}

	//////////////////////////////////////////////////////////////////////////
	//  rlGetCloudSaveFileTask
	//////////////////////////////////////////////////////////////////////////
	rlGetCloudSaveFileTask::rlGetCloudSaveFileTask()
		: m_SaveFile(NULL)
		, m_Device(NULL)
		, m_FiHandle(fiHandleInvalid)
	{
		m_DownloadPath[0] = '\0';
	}

	rlGetCloudSaveFileTask::rlGetCloudSaveFileTask(u8* bounceBuffer, const unsigned sizeofBounceBuffer)
		: rlRosHttpTask(NULL, bounceBuffer, sizeofBounceBuffer)
		, m_SaveFile(NULL)
		, m_Device(NULL)
		, m_FiHandle(fiHandleInvalid)

	{
		m_DownloadPath[0] = '\0';
	}

	rlGetCloudSaveFileTask::~rlGetCloudSaveFileTask()
	{

	}

	bool rlGetCloudSaveFileTask::Configure(const int localGamerIndex, rlCloudSaveFile* file, eCloudSaveResolveType resolveType, const char* titleAccessToken)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(file, catchall, );

			m_TimeOutSecs = CLOUDSAVE_DEFAULT_TIMEOUT;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			m_SaveFile = file;

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall,);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

			rverify(AddUnsParameter("fileId", file->GetFileId()), catchall, );

			const char* resolveString = rlCloudSave::GetResolveTypeString(resolveType);
			rverify(AddStringParameter("resolveType", resolveString, true), catchall, );

			rverify(AddStringParameter("hardwareId", file->GetHardwareId(), true), catchall, );

			if (titleAccessToken != NULL)
			{
				rverify(AddStringParameter("titleAccessToken", titleAccessToken, true), catchall, );
			}
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	// Normally, we'd call &fiDeviceLocal::GetInstance() but this returns a const device and the HTTP library
	// requires a non-const device ptr. I am not a fan of removing const casts to suit an interface, so I've elected 
	// to extern the static local device. Internally, this just maps to a bunch of WinAPIs anyway. - JM
	extern fiDeviceLocal s_DeviceLocal;

	bool rlGetCloudSaveFileTask::Configure(const int localGamerIndex, rlCloudSaveFile* file, eCloudSaveResolveType resolveType, const char* filePathUtf8, const char* titleAccessToken)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(file, catchall, );
			rverify(filePathUtf8 && filePathUtf8[0] != '\0', catchall, );
			rverify(strlen(filePathUtf8) < RAGE_MAX_PATH, catchall, );

			safecpy(m_DownloadPath, filePathUtf8);

			m_Device = &s_DeviceLocal;
			rverify(m_Device, catchall, );

			u32 dstAttributes = m_Device->GetAttributes(filePathUtf8);
			if (dstAttributes & FILE_ATTRIBUTE_READONLY)
			{
				rlDebug3("Removing read-only attribute from path '%s'", filePathUtf8);
				m_Device->SetAttributes(filePathUtf8, dstAttributes & ~FILE_ATTRIBUTE_READONLY);
			}

			m_FiHandle = m_Device->Create(filePathUtf8);
			rverify(m_FiHandle != fiHandleInvalid, catchall, );

			m_TimeOutSecs = CLOUDSAVE_DEFAULT_TIMEOUT;

			rlHttpTaskConfig config;
			config.m_ResponseDevice = m_Device;
			config.m_ResponseHandle = m_FiHandle;

			rverify(rlRosHttpTask::Configure(localGamerIndex, &config), catchall, );

			m_Filter.DisableHttpDump(true);
			m_SaveFile = file;

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall,);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

			rverify(AddUnsParameter("fileId", file->GetFileId()), catchall, );

			const char* resolveString = rlCloudSave::GetResolveTypeString(resolveType);
			rverify(AddStringParameter("resolveType", resolveString, true), catchall, );

			rverify(AddStringParameter("hardwareId", file->GetHardwareId(), true), catchall, );

			if (titleAccessToken != NULL)
			{
				rverify(AddStringParameter("titleAccessToken", titleAccessToken, true), catchall, );
			}
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlGetCloudSaveFileTask::Update(const unsigned timeStep)
	{
		rlRosHttpTask::Update(timeStep);

		UpdateDebugInfo();
	}

	bool rlGetCloudSaveFileTask::ProcessResponse(const char* /*response*/, int& resultCode)
	{
		rtry
		{
			char fileVersionBuf[64] = {0};
			unsigned bufLen = COUNTOF(fileVersionBuf);
			if(m_HttpRequest.GetResponseHeaderValue("SCS-File-Version", fileVersionBuf, &bufLen))
			{
				u64 fileVersion = 0;
				if (sscanf_s(fileVersionBuf, "%" I64FMT "u", &fileVersion))
				{
					// rlVerify that the file version we recovered matched what we expected in the manifest
					rverify(fileVersion == m_SaveFile->GetLatestServerVersion(), catchall, );
					m_SaveFile->SetFileVersion(fileVersion);
				}
			}

			//Last-Modified
			char buf[64] = {0};
			bufLen = COUNTOF(buf);
			if(m_HttpRequest.GetResponseHeaderValue("Last-Modified", buf, &bufLen))
			{
				m_SaveFile->SetServerLastModifiedDate(netHttpRequest::ParseDateString(buf));
			}

			char fileHashBuf[256] = {0};
			bufLen = COUNTOF(fileHashBuf);
			if(m_HttpRequest.GetResponseHeaderValue("SCS-File-Hash", fileHashBuf, &bufLen))
			{
				m_SaveFile->SetMd5Hash(fileHashBuf);
			}

			resultCode = 0;
			return true;
		}
		rcatchall
		{
			resultCode = RLCS_ERROR_UNEXPECTED_RESULT;
			return false;
		}
	} 

	void rlGetCloudSaveFileTask::Finish(const FinishType finishType, const int resultCode /* = 0 */)
	{
		rlTaskDebug2("%s (%ums)", FinishString(finishType), sysTimer::GetSystemMsTime()- m_StartTime);

		int finishResultCode = resultCode;

		// Extract debugging information for error reporting.
		UpdateDebugInfo();

		if(m_HttpRequest.Pending())
		{
			m_HttpRequest.Cancel();
		}

		if (finishType == FINISH_SUCCEEDED)
		{
			if (m_FiHandle != fiHandleInvalid)
			{
				m_SaveFile->SetClientFileSize(m_Device->GetFileSize(m_DownloadPath));
			}
			else
			{
				m_SaveFile->CopyData((u8*)m_GrowBuffer.GetBuffer(), m_GrowBuffer.Length());
				m_SaveFile->SetClientFileSize(m_GrowBuffer.Length());
			}
		}
		else if (finishType == FINISH_FAILED)
		{
			// This call can return a 404 if the file is not found, or a 500 for all other errors
			if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_NOT_FOUND ||
				m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_INTERNAL_SERVER_ERROR)
			{
				// These 2 status codes are unique in their response. rlHttpTask doesn't normally parse the response
				//	for http errors, but they are included here.
				const char* response = (const char*)m_GrowBuffer.GetBuffer();
				rlRosHttpTask::ProcessResponse(response, finishResultCode);
			}
		}
		
		rlTaskBase::Finish(finishType, resultCode);

		if (m_Device)
		{
			bool fileExists = false;
			if (m_FiHandle != fiHandleInvalid)
			{
				m_Device->Close(m_FiHandle);
				m_FiHandle = fiHandleInvalid;
				fileExists = true;
			}

			// If the task was canceled or failed, delete it.
			if (fileExists && finishType != FINISH_SUCCEEDED)
			{
				m_Device->Delete(m_DownloadPath);
			}
		}

		m_AddingListParam = false;
	}

	void rlGetCloudSaveFileTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("InvalidArgument", "", RLCS_ERROR_INVALIDARGUMENT);
		ERROR_CASE("Exception", "", RLCS_ERROR_EXCEPTION);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLCS_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlGetCloudSaveFileTask::GetServiceMethod() const
	{
		return "cloudsave.asmx/GetFile";
	}

	void rlGetCloudSaveFileTask::UpdateDebugInfo()
	{
		// Extract debugging information for error reporting.
		if (m_SaveFile)
		{
			netHttpRequest::DebugInfo& info = m_SaveFile->GetHttpDebugInfo();
			m_HttpRequest.GetDebugInfo(info);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//  rlPostCloudSaveFileTask
	//////////////////////////////////////////////////////////////////////////
	rlPostCloudSaveFileTask::rlPostCloudSaveFileTask()
		: m_SaveFile(NULL)
	{

	}

	rlPostCloudSaveFileTask::~rlPostCloudSaveFileTask()
	{

	}

	bool rlPostCloudSaveFileTask::Configure(const int localGamerIndex, rlCloudSaveFile* file, eCloudSaveResolveType resolveType, const char* titleAccessToken)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(file, catchall, );

			m_TimeOutSecs = CLOUDSAVE_DEFAULT_TIMEOUT;

			rlHttpTaskConfig config;
			config.m_AddDefaultContentTypeHeader = false;
			rverify(rlRosHttpTask::Configure(localGamerIndex, &config), catchall, );

			m_SaveFile = file;

			rverify(StartMultipartFormData(), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall,);

			rverify(AddMultipartStringParameter("ticket", cred.GetTicket(), true), catchall, );
			rverify(AddMultipartStringParameter("hardwareId", m_SaveFile->GetHardwareId(), true), catchall, );
			rverify(AddMultipartStringParameter("resolveType", rlCloudSave::GetResolveTypeString(resolveType), true), catchall, );

			if (titleAccessToken != NULL)
			{
				rverify(AddMultipartStringParameter("titleAccessToken", titleAccessToken, true), catchall, );
			}

			char http_date[256] = {0};
			netHttpRequest::MakeDateString(file->GetClientLastModifiedDate(), http_date);
			rverify(AddMultipartStringParameter("lastModified", http_date, true), catchall, );

			// Attempt to use the expected version. If its zero, check the file version.
			//	If we have a file version but no expected version, pass in FileVersion+1.
			//  If we have no expected version and no file version, write 0.
			u64 expectedVersion = file->GetNextExpectedVersion();
			if (expectedVersion == 0)
			{
				if (file->GetFileVersion() == 0)
				{
					expectedVersion = 0;
				}
				else
				{
					expectedVersion = file->GetFileVersion() + 1;
				}
			}

			rverify(AddMultipartUnsParameter("expectedVersion", expectedVersion), catchall, );

			// If metadata has been provided
			if (istrlen(file->GetMetaData()) > 0)
			{
				rlTaskDebug3("Posting with metadata: %s", file->GetMetaData());
				rverify(AddMultipartStringParameter("fileMetadata", file->GetMetaData()), catchall, );
			}

			rverify(AddMultipartBinaryParameter("file1", file->GetFileName(), file->GetData(), file->GetDataLength()), catchall, );

			rverify(EndMultipartFormData(), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlPostCloudSaveFileTask::Update(const unsigned timeStep)
	{
		rlRosHttpTask::Update(timeStep);

		// Extract debugging information for error reporting.
		UpdateDebugInfo();
	}

	void rlPostCloudSaveFileTask::Finish(const FinishType finishType, const int resultCode /* = 0 */)
	{
		// Extract debugging information for error reporting.
		UpdateDebugInfo();

		rlHttpTask::Finish(finishType, resultCode);
	}

	bool rlPostCloudSaveFileTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
	{
		rtry
		{
			rverify(node, catchall, rlTaskError("No valid XML node to process"));

			const parTreeNode* resultsNode = node->FindChildWithName("Results");
			rcheck(resultsNode, catchall, rlTaskError("Failed to find <Results> element"));

			const parTreeNode* fileNode = resultsNode->FindChildWithName("Result");
			rcheck(fileNode, catchall, rlTaskError("Failed to find <Result> element"));

			u64 fileId;
			rverify(rlHttpTaskHelper::ReadUInt64(fileId, fileNode, NULL, "FileId"), catchall, );
			m_SaveFile->SetFileId(fileId);

			u64 fileVersion;
			rverify(rlHttpTaskHelper::ReadUInt64(fileVersion, fileNode, NULL, "Version"), catchall, );
			m_SaveFile->SetFileVersion(fileVersion);

			const char* filePath = rlHttpTaskHelper::ReadString(fileNode, NULL, "Path");
			rverify(filePath, catchall, );
			m_SaveFile->SetFileName(filePath);

			const char* serverLastModTime = rlHttpTaskHelper::ReadString(fileNode, NULL, "ServerLastModifiedUtc");
			rverify(serverLastModTime, catchall, );
			u64 serverLastModTime64 = netHttpRequest::ParseDateString(serverLastModTime);
			m_SaveFile->SetServerLastModifiedDate(serverLastModTime64);
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlPostCloudSaveFileTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("InvalidArgument", "", RLCS_ERROR_INVALIDARGUMENT);
		ERROR_CASE("Exception", "", RLCS_ERROR_EXCEPTION);
		ERROR_CASE("InvalidVersion", "ExpectedVersion", RLCS_ERROR_INVALIDVERSION_EXPECTEDVERSION);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLCS_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlPostCloudSaveFileTask::GetServiceMethod() const
	{
		return "cloudsave.asmx/PostFile";
	}

	void rlPostCloudSaveFileTask::UpdateDebugInfo()
	{
		// Extract debugging information for error reporting.
		if (m_SaveFile)
		{
			netHttpRequest::DebugInfo& info = m_SaveFile->GetHttpDebugInfo();
			m_HttpRequest.GetDebugInfo(info);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//  rlUnregisterCloudSaveFileTask
	//////////////////////////////////////////////////////////////////////////
	rlUnregisterCloudSaveFileTask::rlUnregisterCloudSaveFileTask()
		: m_SaveFile(NULL)
	{

	}

	rlUnregisterCloudSaveFileTask::~rlUnregisterCloudSaveFileTask()
	{

	}

	bool rlUnregisterCloudSaveFileTask::Configure(const int localGamerIndex, rlCloudSaveFile* file, eCloudSaveResolveType resolveType, const char* titleAccessToken)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(file, catchall, );

			m_TimeOutSecs = CLOUDSAVE_DEFAULT_TIMEOUT;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			m_SaveFile = file;

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall,);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

			rverify(AddUnsParameter("fileId", file->GetFileId()), catchall, );

			rverify(AddUnsParameter("expectedVersion", file->GetFileVersion()), catchall, );

			const char* resolveString = rlCloudSave::GetResolveTypeString(resolveType);
			rverify(AddStringParameter("resolveType", resolveString, true), catchall, );

			rverify(AddStringParameter("hardwareId", m_SaveFile->GetHardwareId(), true), catchall, );

			if (titleAccessToken != NULL)
			{
				rverify(AddStringParameter("titleAccessToken", titleAccessToken, true), catchall, );
			}
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlUnregisterCloudSaveFileTask::Finish(const FinishType finishType, const int resultCode /* = 0 */)
	{
		// Extract debugging information for error reporting.
		if (m_SaveFile)
		{
			netHttpRequest::DebugInfo& debugInfo = m_SaveFile->GetHttpDebugInfo();
			m_HttpRequest.GetDebugInfo(debugInfo);
		}

		rlHttpTask::Finish(finishType, resultCode);
	}

	bool rlUnregisterCloudSaveFileTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
	{
		resultCode = 0;
		return true;
	}

	void rlUnregisterCloudSaveFileTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("InvalidArgument", "", RLCS_ERROR_INVALIDARGUMENT);
		ERROR_CASE("Exception", "", RLCS_ERROR_EXCEPTION);
		ERROR_CASE("InvalidVersion", "ExpectedVersion", RLCS_ERROR_INVALIDVERSION_EXPECTEDVERSION);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLCS_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlUnregisterCloudSaveFileTask::GetServiceMethod() const
	{
		return "cloudsave.asmx/DeleteFile";
	}

	///////////////////////////////////////////////////////////////////////////////
	//  rlQueryCloudSaveBetaAccess
	///////////////////////////////////////////////////////////////////////////////
	rlQueryCloudSaveBetaAccess::rlQueryCloudSaveBetaAccess()
		: m_ResultsPtr(NULL)
		, m_bHasCloudSaveAccess(false)
	{
		m_CohortsCloudfile[0] = '\0';
	}

	rlQueryCloudSaveBetaAccess::~rlQueryCloudSaveBetaAccess()
	{

	}

	bool rlQueryCloudSaveBetaAccess::Configure(const int localGamerIndex, const char* cohortsCloudFile, bool* resultsPtr)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(resultsPtr, catchall, );

			safecpy(m_CohortsCloudfile, cohortsCloudFile);
			m_ResultsPtr = resultsPtr;
			*m_ResultsPtr = false;

			m_TimeOutSecs = CLOUDSAVE_DEFAULT_TIMEOUT;

			rlHttpTaskConfig config;
			config.m_HttpVerb = NET_HTTP_VERB_GET;
			rverify(rlRosHttpTask::Configure(localGamerIndex, &config), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall,);
			rverify(AddRequestHeaderValue("SCS-Ticket", cred.GetTicket()), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlQueryCloudSaveBetaAccess::ProcessResponse(const char* response, int& /*resultCode*/)
	{
		rtry
		{
			rverify(response, catchall, rlError("Null body in response when a body is expected"));

			unsigned len = istrlen(response);
			RsonReader rr(response, len);

			RsonReader rrCloudSavesAccess;
			rverify(rr.GetMember("v", &rrCloudSavesAccess), catchall, );

			int iCloudAccess = 0;
			rverify(rrCloudSavesAccess.AsInt(iCloudAccess), catchall, );

			// { "v" : 1 } or { "v" : 0 }
			m_bHasCloudSaveAccess = (iCloudAccess == 1);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlQueryCloudSaveBetaAccess::Finish(const FinishType finishType, const int resultCode /* = 0 */)
	{
		rlTaskDebug2("%s (%ums)", FinishString(finishType), sysTimer::GetSystemMsTime() - m_StartTime);

		if(m_HttpRequest.Pending())
		{
			m_HttpRequest.Cancel();
		}

		// a successful response means that beta access is valid.
		if (finishType == FINISH_SUCCEEDED)
		{
			rlTaskDebug3("Cloud save beta access: %s", m_bHasCloudSaveAccess ? "confirmed" : "denied");
			*m_ResultsPtr = m_bHasCloudSaveAccess;
		}
		else if (finishType == FINISH_FAILED)
		{
			// a 404 means the user is not in the beta.
			// any other error code should be interpreted the same way.
			if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_NOT_FOUND)
			{
				rlTaskDebug3("Cloud save beta access denied - 404 file.");
				*m_ResultsPtr = false;
			}
			else
			{
				rlTaskDebug3("Cloud save beta access denied - error. Status Code: %d", m_HttpRequest.GetStatusCode());
				*m_ResultsPtr = false;
			}
		}

		rlTaskBase::Finish(finishType, resultCode);
	}

	bool rlQueryCloudSaveBetaAccess::GetServicePath(char* svcPath, const unsigned maxLen) const
	{
		formatf(svcPath, maxLen, "cloud/%d/cloudservices/titles/%s/%s/%s", 
			g_rlTitleId->m_RosTitleId.GetTitleVersion(),
			g_rlTitleId->m_RosTitleId.GetTitleName(),
			g_rlTitleId->m_RosTitleId.GetPlatformName(), 
			GetServiceMethod());

		return true;
	}

	const char* rlQueryCloudSaveBetaAccess::GetServiceMethod() const
	{
		return m_CohortsCloudfile;
	}

	//////////////////////////////////////////////////////////////////////////
	//  rlGetCloudSaveMetadataTask
	//////////////////////////////////////////////////////////////////////////
	rlGetCloudSaveMetadataTask::rlGetCloudSaveMetadataTask()
		: m_SaveFile(NULL)
	{
	}

	rlGetCloudSaveMetadataTask::~rlGetCloudSaveMetadataTask()
	{

	}

	bool rlGetCloudSaveMetadataTask::Configure(const int localGamerIndex, rlCloudSaveFile* file, const char* titleAccessToken)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(file, catchall, );

			m_TimeOutSecs = CLOUDSAVE_DEFAULT_TIMEOUT;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			m_SaveFile = file;

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall,);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

			rverify(AddUnsParameter("fileId", file->GetFileId()), catchall, );

			rverify(AddUnsParameter("fileVersion", file->GetFileVersion()), catchall, );

			if (titleAccessToken != NULL)
			{
				rverify(AddStringParameter("titleAccessToken", titleAccessToken, true), catchall, );
			}
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlGetCloudSaveMetadataTask::Finish(const FinishType finishType, const int resultCode /* = 0 */)
	{
		// Extract debugging information for error reporting.
		if (m_SaveFile)
		{
			netHttpRequest::DebugInfo& debugInfo = m_SaveFile->GetHttpDebugInfo();
			m_HttpRequest.GetDebugInfo(debugInfo);
		}

		rlHttpTask::Finish(finishType, resultCode);
	}

	bool rlGetCloudSaveMetadataTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
	{
		rtry
		{
			rverify(node, catchall, rlTaskError("No valid XML node to process"));

			const parTreeNode* results = node->FindChildWithName("Result");
			rcheck(results, catchall, rlTaskError("Failed to find <Result> element"));

			const char* metaData = rlHttpTaskHelper::ReadString(results, "Json", NULL);
			rverify(metaData, catchall, );
			rverify(istrlen(metaData) <= rlCloudSaveFile::METADATA_MAX_LEN, catchall, );

			m_SaveFile->SetServerMetadata(metaData);
			
			resultCode = 0;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlGetCloudSaveMetadataTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("InvalidArgument", NULL, RLCS_ERROR_INVALIDARGUMENT);
		ERROR_CASE("Exception", NULL, RLCS_ERROR_EXCEPTION);
		ERROR_CASE("SqlException", "UnknownError", RLCS_ERROR_SQLEXCEPTION_UNKNOWN);
		ERROR_CASE("DoesNotExist", NULL, RLCS_ERROR_DOES_NOT_EXIST);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLCS_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlGetCloudSaveMetadataTask::GetServiceMethod() const
	{
		return "cloudsave.asmx/GetFileMetadata";
	}

} // namespace rage
