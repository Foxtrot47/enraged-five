// 
// rline/rlcoudsave.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLCLOUDSAVE_H
#define RLINE_RLCLOUDSAVE_H

#include "net/status.h"
#include "net/netaddress.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/rltask.h"
#include "system/criticalsection.h"

namespace rage
{
	RAGE_DECLARE_SUBCHANNEL(rline, cloudsave)

	//PURPOSE
	//  Error codes returned by rlCloudSave methods.
	enum rlCloudSaveErrorCode
	{
		RLCS_ERROR_UNEXPECTED_RESULT = -1,              //Unhandled error code
		RLCS_ERROR_NONE = 0,                            //Success    

		//Errors that follow the Code_CodeEx convention
		RLCS_ERROR_INVALIDARGUMENT,
		RLCS_ERROR_INVALIDARGUMENT_BAD_ROCKSTARID,
		RLCS_ERROR_OUTOFRANGE,
		RLCS_ERROR_OUTOFRANGE_DISKQUOTA,
		RLCS_ERROR_INVALIDVERSION,
		RLCS_ERROR_INVALIDVERSION_EXPECTEDVERSION,
		RLCS_ERROR_EXCEPTION,
		RLCS_ERROR_SQLEXCEPTION_UNKNOWN,
		RLCS_ERROR_TICKET,
		RLCS_ERROR_DOES_NOT_EXIST,
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlCloudSaveFile
	///////////////////////////////////////////////////////////////////////////////
	class rlCloudSaveFile
	{
	public:

		// Arbitrary Maximum Length of a FileName
		static const unsigned FILENAME_MAX_LEN = 256;
		static const unsigned FILENAME_MAX_BUF_SIZE = FILENAME_MAX_LEN + 1;

		// Maximum Length of the SCS MD5 hash
		static const unsigned MD5_HASH_LEN = 32;
		static const unsigned MD5_HASH_BUF_SIZE = MD5_HASH_LEN + 1; 

		// Arbitrary maximum length of the hardware id
		static const unsigned HARDWARE_ID_MAX_LEN = 64;
		static const unsigned HARDWARE_ID_MAX_BUF_SIZE = HARDWARE_ID_MAX_LEN + 1;

		// Arbitrary maximum file size (default), can be overridden
		static const u64 DEFAULT_MAX_FILE_SIZE = (64 * 1024 * 1024); // 64MB

		// Arbitrary maximum length of metadata (JSON)
		static const unsigned METADATA_MAX_LEN = 1024;
		static const unsigned METADATA_MAX_BUF_SIZE = METADATA_MAX_LEN + 1;

		// Export Sizes
		static const unsigned VERSION_1 = 1;
		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(unsigned) +										// m_ImportVersion
															sizeof(u64) +											// m_FileId
														    datMaxBytesNeededForString<FILENAME_MAX_LEN>::COUNT	+	// m_FileName
															datMaxBytesNeededForString<MD5_HASH_BUF_SIZE>::COUNT +	// m_Md5Hash
															sizeof(u64) +											// m_FileVersion
															sizeof(u64) +											// m_NextExpectedVersion
															sizeof(u64) +											// m_ClientLastModifiedDate
															sizeof(u64) +											// m_ServerLastModifiedDate
															datMaxBytesNeededForString<HARDWARE_ID_MAX_BUF_SIZE>::COUNT +	// m_HardwareId
															datMaxBytesNeededForString<METADATA_MAX_BUF_SIZE>::COUNT +	// m_MetaData
															datMaxBytesNeededForString<METADATA_MAX_BUF_SIZE>::COUNT +	// m_ServerMetadata
															sizeof(u64) +											// m_ClientFileSize
															sizeof(u64) +											// m_ServerFileSize
															netIpAddress::MAX_EXPORTED_SIZE_IN_BYTES;				// m_IpAddress

	public:

		// Cloud Save File Constructors
		rlCloudSaveFile();
		~rlCloudSaveFile();

		// PURPOSE
		//	Resets the data for the file, deallocating memory if present.
		void Clear();

		// PURPOSE
		//	Frees the memory used for the save data, but maintains the file properties.
		void FreeSaveData();

		// PURPOSE
		//	Loads data from a full path on disk
		bool LoadData(const char* path);

		// PURPOSE
		//	Returns the File Id for the file, which can be used to retrieve it.
		void SetFileId(u64 fileId);
		u64 GetFileId() const;

		// PURPOSE
		//	Set or get the MD5 Hash of the file
		void SetMd5Hash(const char* md5Hash);
		const char* GetMd5Hash() const;

		// PURPOSE
		//	Set or get the File Name
		void SetFileName(const char* fileName);
		const char* GetFileName() const;

		// PURPOSE
		//	Set or get the File Version for the file, which can be used for conflict resolution purposes
		void SetFileVersion(u64 version);
		u64 GetFileVersion() const;

		// PURPOSE
		//	Get or set the next expected version on the server.
		void SetNextExpectedVersion(u64 version);
		u64 GetNextExpectedVersion() const;

		// PURPOSE
		//	Set or get the user-controlled last modified date, set by the client. Should be generated using rlGetPosixTime()
		void SetClientLastModifiedDate(u64 date);
		u64 GetClientLastModifiedDate() const;

		// PURPOSE
		//	Returns the UTC date of when the server recorded the last write action.
		void SetServerLastModifiedDate(u64 date);
		u64 GetServerLastModifiedDate() const;

		// PURPOSE
		//	Set or get the last hardware identifier set by the client, which can be used for conflict resolution purposes.
		const char* GetHardwareId() const;
		void SetHardwareId(const char* hardwareId);

		// PURPOSE
		//	Set or get the Ip addreess last used for this file
		void SetLastIpAddress(netIpAddress& address);
		const netIpAddress& GetLastIpAddress() const;

		// PURPOSE
		//	Creates a local copy of the input data
		void CopyData(u8* data, unsigned dataLen);

		// PURPOSE
		//	Set or get the data.
		u8* GetData() const;
		u32 GetDataLength() const;

		// PURPOSE
		//	Set or get the metadata that is associated with the save file.
		const char* GetMetaData();
		void SetMetaData(const char* metaData);

		// PURPOSE
		//	Set or get the metadata that is associated with the save file as known by the server.
		const char* GetServerMetadata() const;
		void SetServerMetadata(const char* metadata);

		// PURPOSE
		//	Gets or sets the file size on disk
		void SetClientFileSize(u64 fileSize);
		u64 GetClientFileSize() const;

		// PURPOSE
		//	Gets or sets the file size known by server.
		void SetServerFileSize(u64 fileSize);
		u64 GetServerFileSize() const;

		// PURPOSE
		//	Gets or sets the latest file version known on the server
		void SetLatestServerVersion(u64 fileVersion);
		u64 GetLatestServerVersion() const;

		//PURPOSE
		//  Serializes
		//PARAMS
		//  buf         - Destination buffer.
		//  sizeofBuf   - Size of destination buffer.
		//  size        - Set to number of bytes exported.
		//RETURNS
		//  True on success.
		bool Export(void* buf, const unsigned sizeofBuf, unsigned* size = 0) const;

		//PURPOSE
		//  Deserializes
		//PARAMS
		//  buf         - Source buffer.
		//  sizeofBuf   - Size of source buffer.
		//  size        - Set to number of bytes imported.
		//RETURNS
		//  True on success.
		bool Import(const void* buf, const unsigned sizeofBuf, unsigned* size = 0);

		// PURPOSE
		//	Returns TRUE if a md5 hash of the file's data matches the expected md5 hash
		bool IsMd5HashValidForData();

		// PURPOSE
		//	Returns a reference to the debug info object.
		netHttpRequest::DebugInfo& GetHttpDebugInfo() { return m_HttpDebugInfo; }

	private:

		bool IsValidExportVersion(int version) { return version == VERSION_1; }

		// EXPORTED VALUES
		unsigned m_ImportVersion;
		u64 m_FileId;
		char m_FileName[FILENAME_MAX_BUF_SIZE];
		char m_Md5Hash[MD5_HASH_BUF_SIZE];
		u64 m_FileVersion;
		u64 m_NextExpectedVersion;
		u64 m_ClientLastModifiedDate;
		u64 m_ServerLastModifiedDate;
		char m_HardwareId[HARDWARE_ID_MAX_BUF_SIZE];
		char m_MetaData[METADATA_MAX_BUF_SIZE];
		char m_ServerMetaData[METADATA_MAX_BUF_SIZE];
		u64 m_ClientFileSize;
		u64 m_ServerFileSize;
		netIpAddress m_IpAddress;

		// NON-EXPORTED VALUES
		u8* m_Data;
		u32 m_DataLen;
		u64 m_ServerLatestVersion;

		netHttpRequest::DebugInfo m_HttpDebugInfo;
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlCloudSaveManifest
	///////////////////////////////////////////////////////////////////////////////
	class rlCloudSaveManifest
	{
	public:
		static const unsigned MANIFEST_VERSION = 1;
		static const unsigned MAX_CLOUD_SAVE_SLOTS = 64;

	public:

		// PURPOSE
		//	Constructor
		rlCloudSaveManifest();
		~rlCloudSaveManifest();

		// PURPOSE
		//	Resets the manifest, destroying all files contained within.
		void Clear();

		// PURPOSE
		//	Returns the number of save files
		u32 GetCount() const;

		// PURPOSE
		//	Sets or Returns the number of bytes used
		void SetBytesUsed(u64 bytesUsed);
		u64 GetBytesUsed() const;

		// PURPOSE
		//	Adds a cloud save file to the manifest
		bool AddFile(rlCloudSaveFile* file);

		// PURPOSE
		//	Returns the cloud file at the given index
		rlCloudSaveFile* GetSaveFile(unsigned index);

		// PURPOSE
		//	Returns a reference to the debug info object.
		netHttpRequest::DebugInfo& GetHttpDebugInfo() { return m_HttpDebugInfo; }

	private:

		u64 m_BytesUsed;
		atFixedArray<rlCloudSaveFile*, MAX_CLOUD_SAVE_SLOTS> m_SaveFiles;
		netHttpRequest::DebugInfo m_HttpDebugInfo;
	};

	///////////////////////////////////////////////////////////////////////////////
	//  eCloudSaveResolveType
	///////////////////////////////////////////////////////////////////////////////
	enum eCloudSaveResolveType
	{
		None,
		// Indicates that there's a conflict. The user's local save file is newer than the server, but the user has chosen 
		// to retrieve the file that's remote anyway. Used in the GetFile Operation
		AcceptRemote,
		// The user's local save file is older / different from what's on the server. But the user chooses to overwrite the server 
		// with his local save anyway. Used in the PostFile Operation.
		AcceptLocal,
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlCloudSave - Class for accessing static cloud save functionality
	///////////////////////////////////////////////////////////////////////////////
	class rlCloudSave
	{
	public:

		static bool Init();
		static void Shutdown();

		// PURPOSE
		//	Sets the maximum file size for a cloud save file
		static void SetMaximumFileSize(u64 fileSize);

		// PURPOSE
		//	Enumerate the list of files that are stored on the server for the player for the title and platform in the ticket. 
		//	The API will also report the number of bytes consumed by the user and metadata regarding each file.
		static bool GetCloudSaveManifest(const int localGamerIndex, rlCloudSaveManifest* manifest, const char* titleAccessToken, netStatus* status);

		// PURPOSE
		//	This endpoint will return the binary payload of the file being requested based on the user's ticket and the file id being requested.
		static bool GetFile(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* titleAccessToken, netStatus* status);

		// PURPOSE
		//	This endpoint will return the binary payload of the file being requested based on the user's ticket and the file id being requested.
		static bool DownloadFile(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* filePathUtf8, 
								u8* bounceBuffer, const unsigned bounceBufLen, const char* titleAccessToken, netStatus* status);

		// PURPOSE
		//	Writes a file to the cloud storage for the user. 
		static bool PostFile(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* titleAccessToken, netStatus* status);

		// PURPOSE
		//	Unregisters a file from the cloud, effectively deleting it.
		static bool UnregisterFile(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* titleAccessToken, netStatus* status);

		// PURPOSE
		//	Returns the string form of the cloud save resolve type
		static const char* GetResolveTypeString(eCloudSaveResolveType resolveType);

		// PURPOSE
		//	From the given save file path, validate the hash to match the expected hash.
		//	status will return SUCCEEDED if it matches, or FAILED if the file cannot be found or the hash does not match.
		//	The function will return TRUE if the task is queued, or FALSE if it cannot be queued.
		static bool VerifyMd5Hash(const char* fullPathUtf8, const char* expectedMd5Hash, netStatus* status);

		// PURPOSE
		//	Calculates the md5 hash of a cloud file from its local full path
		static bool CalculateMd5Hash(const char* fullPathUtf8, char (&out_hash)[rlCloudSaveFile::MD5_HASH_BUF_SIZE]);

		// PURPOSE
		//	Returns if the user has beta access to cloud saves using the cohorts system.
		static bool QueryHasBetaAccess(const int localGamerIndex, const char* cohortsCloudFile, bool* resultsPtr, netStatus* status);

		// PURPOSE
		//	Retrieves metadata for a file.
		static bool GetFileMetadata(const int localGamerIndex, rlCloudSaveFile* saveFile, const char* titleAccessToken, netStatus* status);

	public:

		// PURPOSE
		//	Worker thread for cloud save operations
		static void CloudSaveWorker(void* );

		// PURPOSE
		//	Create or Destroy the 'CloudSaveWorker' thread.
		static void CreateWorker();
		static void ShutdownWorker();

	private:

		// PURPOSE
		//	Signals the 'WakeupEvent' to clear any CloudSaveWorker waits
		static void SignalWorker();

		// PURPOSE
		//	Contextually returns our own task manager, or the rlTaskManager
		static rlTaskManager* GetTaskManager();

		// Worker Thread Objects
		static bool s_Initialized;
		static bool s_RunWorkerThread;
		static rlTaskManager s_WorkerTaskManager;
		static sysCriticalSectionToken s_WorkerThreadCs;
		static sysIpcThreadId s_WorkerThreadId;
		static sysIpcEvent s_WakeupEvent;
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlGetCloudSaveManifestTask
	///////////////////////////////////////////////////////////////////////////////
	class rlGetCloudSaveManifestTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlGetCloudSaveManifestTask);
		RL_TASK_USE_CHANNEL(rline_cloudsave);

		rlGetCloudSaveManifestTask();
		virtual ~rlGetCloudSaveManifestTask();

		bool Configure(const int localGamerIndex, rlCloudSaveManifest* manifest, const char* titleAccessToken);

		virtual void Finish(const FinishType finishType, const int resultCode = 0);
		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		const char* GetServiceMethod() const;

	private:

		rlCloudSaveManifest* m_Manifest;
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlGetCloudSaveFileTask
	///////////////////////////////////////////////////////////////////////////////
	class rlGetCloudSaveFileTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlGetCloudSaveFileTask);
		RL_TASK_USE_CHANNEL(rline_cloudsave);

		rlGetCloudSaveFileTask();
		rlGetCloudSaveFileTask(u8* bounceBuffer, const unsigned sizeofBounceBuffer);
		virtual ~rlGetCloudSaveFileTask();

		bool Configure(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* titleAccessToken);
		bool Configure(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* filePathUtf8, const char* titleAccessToken);
		void Update(const unsigned timeStep);

		virtual bool ProcessResponse(const char* response, int& resultCode);
		virtual void Finish(const FinishType finishType, const int resultCode = 0);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		const char* GetServiceMethod() const;

	private:

		void UpdateDebugInfo();

		rlCloudSaveFile* m_SaveFile;
		char m_DownloadPath[RAGE_MAX_PATH];
		fiDevice* m_Device;
		fiHandle m_FiHandle;
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlPostCloudSaveFileTask
	///////////////////////////////////////////////////////////////////////////////
	class rlPostCloudSaveFileTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlPostCloudSaveFileTask);
		RL_TASK_USE_CHANNEL(rline_cloudsave);

		rlPostCloudSaveFileTask();
		virtual ~rlPostCloudSaveFileTask();

		bool Configure(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* titleAccessToken);
		void Update(const unsigned timeStep);

		virtual void Finish(const FinishType finishType, const int resultCode = 0);
		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		const char* GetServiceMethod() const;

	private:

		void UpdateDebugInfo();

		rlCloudSaveFile* m_SaveFile;
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlUnregisterCloudSaveFileTask
	///////////////////////////////////////////////////////////////////////////////
	class rlUnregisterCloudSaveFileTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlUnregisterCloudSaveFileTask);
		RL_TASK_USE_CHANNEL(rline_cloudsave);

		rlUnregisterCloudSaveFileTask();
		virtual ~rlUnregisterCloudSaveFileTask();

		bool Configure(const int localGamerIndex, rlCloudSaveFile* saveFile, eCloudSaveResolveType resolveType, const char* titleAccessToken);

		virtual void Finish(const FinishType finishType, const int resultCode = 0);
		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		const char* GetServiceMethod() const;

	private:

		rlCloudSaveFile* m_SaveFile;
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlQueryCloudSaveBetaAccess
	///////////////////////////////////////////////////////////////////////////////
	class rlQueryCloudSaveBetaAccess : public rlRosHttpTask
	{
	public:

		static const int MAX_COHORTS_CLOUD_FILE_LENGTH = 63;
		static const int MAX_COHORTS_CLOUD_FILE_BUF_SIZE = MAX_COHORTS_CLOUD_FILE_LENGTH + 1;

		RL_TASK_DECL(rlQueryCloudSaveBetaAccess);
		RL_TASK_USE_CHANNEL(rline_cloudsave);

		rlQueryCloudSaveBetaAccess();
		virtual ~rlQueryCloudSaveBetaAccess();

		bool Configure(const int localGamerIndex, const char* cohortsCloudFile, bool* resultsPtr);
		virtual bool ProcessResponse(const char* response, int& resultCode);
		virtual void Finish(const FinishType finishType, const int resultCode = 0);
		virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const;
		const char* GetServiceMethod() const;

	private:

		bool* m_ResultsPtr;
		bool m_bHasCloudSaveAccess;
		char m_CohortsCloudfile[MAX_COHORTS_CLOUD_FILE_BUF_SIZE];
	};

	///////////////////////////////////////////////////////////////////////////////
	//  rlGetCloudSaveMetadataTask
	///////////////////////////////////////////////////////////////////////////////
	class rlGetCloudSaveMetadataTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlGetCloudSaveMetadataTask);
		RL_TASK_USE_CHANNEL(rline_cloudsave);

		rlGetCloudSaveMetadataTask();
		virtual ~rlGetCloudSaveMetadataTask();

		bool Configure(const int localGamerIndex, rlCloudSaveFile* saveFile, const char* titleAccessToken);

		virtual void Finish(const FinishType finishType, const int resultCode = 0);
		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		const char* GetServiceMethod() const;

	private:

		rlCloudSaveFile* m_SaveFile;
	};

} //namespace rage

#endif  // RLINE_RLCLOUDSAVE_H
