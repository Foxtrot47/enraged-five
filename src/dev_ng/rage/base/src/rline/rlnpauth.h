// 
// rline/rlnpauth.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#ifndef RLINE_RLNPAUTH_H
#define RLINE_RLNPAUTH_H

#include "net/status.h"
#include "net/time.h"
#include "rline/rldiag.h"
#include "rline/rl.h"
#include "system/memops.h"

#include <np.h>

namespace rage
{
	enum rlRealtimeMultiplayerProperty
	{
		Property_None = 0,
		Property_CrossPlatform = BIT0,
		Property_InEngineSpectating = BIT1,
	};

	class rlNpAuth
	{
	public:
		rlNpAuth();
		~rlNpAuth();

		bool Init();
		void Shutdown();

		void Reset(const int localGamerIndex);
		void Update();
		
		// Auth Code Logic
		bool RefreshAuthCode(int localGamerIndex);
		void WaitForAuthCode(const int localGamerIndex);
		bool IsRefreshingAuthCode() { return m_bRefreshingAuthCode; }
		bool CanRequestAuthCode() { return m_AuthRetryTimeout.IsTimedOut(); }

		// Shared code between NpAuth and the one-off auth calls needed for CreateTicket
		int GetAuthCode(const int localGamerIndex, rlSceNpAuthorizationCode& authCode, int* issuerId, netStatus* status);
		int UpdateAuthCodeRequest(const int localGamerIndex, int reqId, netStatus* status);
		void CancelAuthCodeRequest(const int localGamerIndex, int reqId, netStatus* status);

		// Auth Code Results: Issuer ID
		bool HasRetrievedIssuerId() { return m_bHasRetrievedIssuerId; }
		int GetIssuerId() { return m_IssuerId; }

		// Permissions Logic
		bool RequestPermissions(const int localGamerIndex);
		void WaitPermissions(const int localGamerIndex);
		void CancelPermissions(const int localGamerIndex);
		bool IsRefreshingPermissions(const int localGamerIndex) { return m_PermissionsRequest[localGamerIndex].m_bRefreshing; }
		bool CanRequestPermissions(const int localGamerIndex) { return m_PermissionsRequest[localGamerIndex].m_PermissionCooldown.IsTimedOut(); }	
		bool HasRetrievedPermissions(const int localGamerIndex);

		// Permissions Results
		bool HasValidPermissions(const int localGamerIndex) const { return m_bHasValidPermissions[localGamerIndex]; }
		bool GetChatRestrictionFlag(const int localGamerIndex) const { return m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.chatRestriction; }
		int GetAge(const int localGamerIndex) const { return m_PermissionsResult[localGamerIndex].m_Age; }
		
		bool GetUgcRestriction(const int localGamerIndex) const { return m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.ugcRestriction; }
		bool IsPlusAuthorized(const int localGamerIndex) const { return m_PermissionsResult[localGamerIndex].m_PlusResult.authorized; }

		// Sub Account
		bool IsSubAccount(const int localGamerIndex) const { return m_bSubAccount[localGamerIndex]; }
		void SetIsSubAccount(const int localGamerIndex, bool bIsSubAccount) { m_bSubAccount[localGamerIndex] = bIsSubAccount; }

		// Notify PS Premium Feature
		bool NotifyPremiumFeature(const int localGamerIndex, const unsigned properties);
		bool RegisterPlusCallbacks();
		void UnregisterPlusCallbacks();
		static void NpPlusEventCallback(SceUserServiceUserId userId, SceNpPlusEventType evtId, void *userdata);

		// This reason codes are used by script (commands_network.sch) to drive TRC UI
		enum NpUnavailabilityReason
		{
			NP_REASON_INVALID = -1,
			NP_REASON_OTHER = 0,
			NP_REASON_SYSTEM_UPDATE,
			NP_REASON_GAME_UPDATE,
			NP_REASON_SIGNED_OUT,
			NP_REASON_AGE,
			NP_REASON_CONNECTION
		};

		// NP Availability checks - Orbis R4109
		bool     CanCheckNPAvailability(const int localGamerIndex);
		bool        CheckNPAvailability(const int localGamerIndex);
		void         WaitNpAvailability(const int localGamerIndex);
		bool              IsNpAvailable(const int localGamerIndex);
		bool                HasNpResult(const int localGamerIndex);
		bool IsRefreshingNpAvailability(const int localGamerIndex) const { return m_NpAvailable[localGamerIndex].m_Refreshing; }
		bool IsNpUnavailabilityReasonValidForRetry(const int localGamerIndex);
		NpUnavailabilityReason GetNpUnavailabilityReason(const int localGamerIndex);

#if !__NO_OUTPUT
		const char* GetNpUnavailabilityReasonAsString(const NpUnavailabilityReason reason);
#endif

	private:

		bool StartPermissionsRequest(const int localGamerIndex);
		bool StartParentalControlRequest(const int localGamerIndex);
		bool StartPlusRequest(const int localGamerIndex);
		
		// All calls to retrieve permissions are blocking and require Async tasks. PsPlus membership
		// is updated through events. We can't call the SDK directly like PS3, so lets cache for now.
		struct rlScePermissionsRequest
		{
			int8_t m_Age;
			SceNpParentalControlInfo m_ParentalControlInfo;
			int m_ParamId;
			int m_PlusId;
			SceNpCheckPlusResult m_PlusResult;
			bool m_ParentalControlInfoReceived;
			bool m_PlusInfoReceived;
			netTimeout m_PermissionCooldown;
			bool m_bRefreshing;

			// manage retries
			static const unsigned NUM_RETRY_ATTEMPTS = 6;
			static unsigned ms_RetryBackoff[NUM_RETRY_ATTEMPTS];

			unsigned m_ParentalControlRetryTimestamp;
			unsigned m_ParentalControlRetryCount;
			unsigned m_PlusRetryTimestamp;
			unsigned m_PlusRetryCount;

			void Clear()
			{
				m_Age = -1;
				m_ParamId = -1;
				m_PlusId = -1;
				memset(&m_ParentalControlInfo, 0, sizeof(m_ParentalControlInfo));
				memset(&m_PlusResult, 0, sizeof(m_PlusResult)) ;
				m_ParentalControlInfoReceived = false;
				m_PlusInfoReceived = false;
				m_PermissionCooldown.Clear();
				m_bRefreshing = false;
				m_ParentalControlRetryTimestamp = 0;
				m_ParentalControlRetryCount = 0;
				m_PlusRetryTimestamp = 0;
				m_PlusRetryCount = 0;
			}
		};

		struct rlScePermissionsResult
		{
			int8_t m_Age;
			SceNpParentalControlInfo m_ParentalControlInfo;
			SceNpCheckPlusResult m_PlusResult;

			void Clear()
			{
				m_Age = -1;
				memset(&m_ParentalControlInfo, 0, sizeof(m_ParentalControlInfo));
				memset(&m_PlusResult, 0, sizeof(m_PlusResult)) ;
			}

			void Set(const rlScePermissionsRequest& request)
			{
				m_Age = request.m_Age;
				sysMemCpy(&m_ParentalControlInfo, &request.m_ParentalControlInfo, sizeof(m_ParentalControlInfo));
				sysMemCpy(&m_PlusResult, &request.m_PlusResult, sizeof(m_PlusResult));
			}
		};

		//PURPOSE
		// Used to check np availability - R4109. The following processing is all 
		// implemented before using network features other than the NP library.
		struct rlSceNpAvailable
		{
		public:
			enum {INVALID_REQUEST=-1};

		public:
			rlSceNpAvailable() 
				: m_RequestId(INVALID_REQUEST)
				, m_ResultCode(0)
				, m_RequestTimestamp(0)
				, m_Available(false) 
				, m_Refreshing(false) 
				, m_InfoReceived(false) 
			{}

			void Clear()
			{
				m_RequestId = INVALID_REQUEST;
				m_ResultCode = 0;
				m_RequestTimestamp = 0;
				m_Available = false;
				m_Refreshing = false;
				m_InfoReceived = false;
			}

		public:
			int m_RequestId;
			int m_ResultCode;
			unsigned m_RequestTimestamp;
			bool m_Available;
			bool m_Refreshing;
			bool m_InfoReceived;
		};

		netStatus m_AuthStatus;
		int m_ReqId;
		SceNpAuthCreateAsyncRequestParameter asyncParam;
		SceNpAuthGetAuthorizationCodeParameter authParam;
		rlSceNpAuthorizationCode resultAuthCode;
		int m_IssuerId;
		netTimeout m_AuthRetryTimeout;

		rlScePermissionsRequest m_PermissionsRequest[RL_MAX_LOCAL_GAMERS];
		rlScePermissionsResult m_PermissionsResult[RL_MAX_LOCAL_GAMERS];
		bool m_bSubAccount[RL_MAX_LOCAL_GAMERS];
		bool m_bRefreshingAuthCode;
		bool m_bHasRetrievedIssuerId;
		bool m_bHasValidPermissions[RL_MAX_LOCAL_GAMERS];

		rlSceNpAvailable  m_NpAvailable[RL_MAX_LOCAL_GAMERS];

		bool m_bCallbacksRegistered;
	};

} // namespace rage

#endif // rlNpAuth
#endif // RSG_ORBIS