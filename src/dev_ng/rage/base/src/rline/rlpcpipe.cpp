#if RSG_PC

#include "system/ipc.h"

// rage headers
#include "file/file_config.h"
#include "rline/rldiag.h"
#include "rline/rlpcpipe.h"
#include "rline/rlpc.h"
#include "rline/rldiag.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "string/string.h"

// framework headers
#include "fwmaths/random.h"

#include "wolfssl/wolfcrypt/random.h"
#include "system/xtl.h"

#define PIPE_NAME "\\\\.\\pipe\\MP3LauncherPipe"

#define ENCRYPT_COMMS 1

#if (RSG_CPU_X64 && !__RGSC_DLL)
// HMAC Headers
#include "TFIT_SHA_HMAC_iHMACredK.h"
#include "TFIT_hmac_key_iHMACredK.h"
// Our own headers
#include "TFIT_hmac_key_iHMACredK.h"
// Other important headers
#include "wolfssl/wolfcrypt/random.h"
#endif // (RSG_CPU_X64 && !__RGSC_DLL)

#if ENABLE_LAUNCHER_CHALLENGE

// Diffie Helman Headers
	#include "TFIT_ECC_DH_iECDHgreenSSfastP256.h"
	#include "TFIT_ECC_DH_iECDHredKgreenSS.h"
// AES CTR Headers
	#include "TFIT_DK_AES_CBC_Encrypt_iAES13_green.h"	
	#include "TFIT_DK_AES_CBC_Decrypt_iAES14_green.h"
	#include "TFIT_AES_CBC_Encrypt_iAES13.h"
	#include "TFIT_AES_CBC_Decrypt_iAES14.h"	
// Libraries
	#pragma comment(lib, "TFIT_SHA_HMAC_iHMACredK_64.lib")
	#pragma comment(lib, "TFIT_ECC_DH_iECDHgreenSSfastP256_64.lib")
	#pragma comment(lib, "TFIT_Utils_64.lib")
	#pragma comment(lib, "TFIT_AES_CBC_Encrypt_iAES13_64.lib")
	#pragma comment(lib, "TFIT_AES_CBC_Decrypt_iAES14_64.lib")
// Our own headers
	#include "TFIT_ecc_fast_iECDHgreenSSfastP256.h"
	#include "TFIT_hmac_key_iHMACredK.h"


// The following variables are intentionally not included in the 
// rlpcpipe header file. This is because they introduce dependencies
// that are used literally by every other VCProject in the game
// To introduce the changes that way, I'd have to do a progjen on 
// Every project to include the TFIT libraries and headers
// While seemingly it seems like laziness, I'm actually doing this
// in the event that we need to strip it out, it becomes less cumbersome
// for us over time.

// Context for ECC key-sharing
static wbecc_dh_ctx_t		sm_gamePipeEccCtx;
// Local ECC key
static uint8_t				sm_gamePipeEccKey[CURVE_BUFFER_SIZE];
// Buffer for our shared secret
static uint8_t				sm_gamePipeSharedSecret[SHARED_SECRET_SIZE];
// Key length - this will likely always be 64, but having it as a variable
// lets us be flexible
static unsigned int			sm_gamePipeEccKeyLength;
// AES key for encrypting communication
static TFIT_key_iAES13_t		sm_gamePipeEncryptionKey;
static TFIT_key_iAES14_t		sm_gamePipeDecryptionKey;
// CyaSSL random number generator
static WC_RNG					sm_rng;
// Define our initialization vectors
static block_t				sm_decryptionIV = {0};
static block_t				sm_encryptionIV = {0};
// Challenge that we issue the launcher
static unsigned char		sm_challenge[HMAC_SIZE];
// HMAC that's calculated from the launcher
static unsigned char		sm_clientHmac[HMAC_SIZE];

#define TIME_ALLOWED_WITHOUT_LAUNCHER 60*1000
#endif


namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, pcpipe);
#undef __rage_channel
#define __rage_channel rline_pcpipe

extern sysThreadPool netThreadPool;

#if ENABLE_LAUNCHER_CHALLENGE
	u32 rlPCPipe::sm_LauncherCheckStartTime = 0;
	u32 rlPCPipe::sm_LauncherCheckEndTime = 0;

	// Fast ephemeral generation for key-preparation
	int ephemeralFast(const unsigned int data_size, wbecc_enc_t *const data, const wbecc_enc_cfg * const cfg)
	{
		(void)cfg;
		wc_RNG_GenerateBlock(&sm_rng, data, data_size);
		return 0;
	}
#endif

#if ENABLE_LAUNCHER_CHALLENGE
//////////////////////////////////////////////////////
// rlComputeHMACWorkItem
//////////////////////////////////////////////////////
class rlComputeHMACWorkItem : public rlPcPipeLauncherBaseWorkItem
{
public:

	rlComputeHMACWorkItem();
	virtual ~rlComputeHMACWorkItem();

	bool Configure(const int localGamerIndex, unsigned char * challengeToCompute, unsigned char * clientHmac, netStatus *status);
	virtual void DoWork();

	netStatus* GetStatus() { return m_Status;}

	virtual void Cancel() {}
	virtual bool Pending() { return m_Status->Pending(); }
	virtual bool Succeeded() 
	{ 
		//@@: location RLPCPIPEWORKITEM_SUCCEEDED
		return m_Status->Succeeded(); 
	}
	virtual bool Failed()
	{
		//@@: location RLPCPIPEWORKITEM_FAILED
		return m_Status->Failed(); 
	}
	virtual bool ProcessSuccess() { return true; };

protected:
#if !__NO_OUTPUT
	const char * GetMethod();
#endif
private:

	int m_LocalGamerIndex;

	// Store our local gamer index
	int m_localGamerIndex;
	netStatus *m_Status;
	unsigned char * m_challenge;
	unsigned char * m_clientHmac;
	wbsha_hmac_ctx_t m_hmacCtx;
};

//////////////////////////////////////////////////////
// rlComputerHMACTask
//////////////////////////////////////////////////////
class rlComputeHMACTask : public netTask
{
public:
	NET_TASK_DECL(rlComputeHMACTask)
		NET_TASK_USE_CHANNEL(rline_pcpipe);

	rlComputeHMACTask::rlComputeHMACTask();

	virtual bool Configure(const int localGamerIndex, unsigned char * challengeToCompute, unsigned char * clientHmac);
	virtual netTaskStatus OnUpdate(int* resultCode);
	virtual void OnCancel();
	virtual void OnCleanup();
	virtual bool CreateRequest();
	virtual rlComputeHMACWorkItem* GetWorkItem() { return &m_WorkItem;}
private:

	enum State
	{
		STATE_BEGIN_REQUEST,
		STATE_WAIT_REQUEST
	};


	int m_localGamerIndex;
	unsigned char * m_challenge;
	unsigned char * m_clientHmac;
	rlComputeHMACWorkItem m_WorkItem;
	State m_State;
	netStatus m_MyStatus;

};
#endif // #if ENABLE_LAUNCHER_CHALLENGE

/////////////////////////////////////////////////////////////////////////////////////////////////
// rlPcPipeSignOutMessage
/////////////////////////////////////////////////////////////////////////////////////////////////
bool rlPcPipeSignOutMessage::Init()
{
	formatf(m_Buffer, "%s", GetMessageName());
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// rlPcPipeSignInMessage
/////////////////////////////////////////////////////////////////////////////////////////////////
bool rlPcPipeSignInMessage::Init(RockstarId id)
{
	rtry
	{
		rverify(id != InvalidRockstarId, catchall, );
		formatf(m_Buffer, "%s%"I64FMT"d", GetMessageName(), id);
		return true;
	}
	rcatchall
	{
		return false;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// rlPcPipeTicketChangedMessage
/////////////////////////////////////////////////////////////////////////////////////////////////
rlPcPipeTicketChangedMessage::rlPcPipeTicketChangedMessage()
	: m_Buffer(NULL)
{

}
rlPcPipeTicketChangedMessage::~rlPcPipeTicketChangedMessage()
{
	if (m_Buffer)
	{
		RL_FREE(m_Buffer);
		m_Buffer = NULL;
	}
}
bool rlPcPipeTicketChangedMessage::Init(RockstarId id, const char* xmlResponse)
{
	unsigned size;

	rtry
	{
		rverify(id != InvalidRockstarId, catchall, );

		int responseLen = istrlen(xmlResponse);
		rcheck(responseLen > 0, catchall, );
		
		int maxLength = MSG_BUF_SIZE + datBase64::GetMaxEncodedSize(responseLen);
		m_Buffer = (char*)RL_ALLOCATE(RgscProfileManager, maxLength);
		rverify(m_Buffer, catchall, );
		formatf(m_Buffer, maxLength, "%s%"I64FMT"d:", GetMessageName(), id);

		// offset by chars written so message reads "SCTC:MSG"
		int offset = istrlen(m_Buffer);
		rcheck(datBase64::Encode((const u8*)xmlResponse, responseLen, &m_Buffer[offset], maxLength-offset, &size), catchall, rlError("Error encoding XML response"));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

#if ENABLE_LAUNCHER_CHALLENGE
/////////////////////////////////////////////////////////////////////////////////////////////////
// rlPcPipeLauncherChallengeMessage
/////////////////////////////////////////////////////////////////////////////////////////////////
rlPcPipeLauncherChallengeMessage::rlPcPipeLauncherChallengeMessage()
	: m_Buffer(NULL)
{

}
rlPcPipeLauncherChallengeMessage::~rlPcPipeLauncherChallengeMessage()
{
	if (m_Buffer)
	{
		RL_FREE(m_Buffer);
		m_Buffer = NULL;
	}
}

bool rlPcPipeLauncherChallengeMessage::Init(const char *str)
{
	rtry
	{
		rverify(str, catchall, );

		m_Buffer = (char*)RL_ALLOCATE(RgscProfileManager, MSG_BUF_SIZE);
		rverify(m_Buffer, catchall, );
		formatf(m_Buffer, MSG_BUF_SIZE, "%s%s", GetMessageName(), str);
		return true;
	}
	rcatchall
	{
		return false;
	}
}
#endif // #if ENABLE_LAUNCHER_CHALLENGE

/////////////////////////////////////////////////////////////////////////////////////////////////
// rlPcPipe
/////////////////////////////////////////////////////////////////////////////////////////////////
rlPCPipe::rlPCPipe() : 
#if ENABLE_LAUNCHER_CHALLENGE
m_bIsPreparedToChallenge(false),
m_bIsWaitingForChallenge(false),
#endif
m_bConnected(false),
m_bIsStandardUser(false),
m_bIsAlive(false),
m_pipeHandle(NULL),
m_downloadProgressCallback(NULL),
m_transferCompleteCallback(NULL),
m_downloadErrorCallback(NULL)
{
	//@@: range RLPCPIPE_CONSTRUCTOR {
#if ENABLE_LAUNCHER_CHALLENGE
	rlDebug("Initializing RNG");
	//@@: location RLPCPIPE_CONSTRUCTOR_INITIALIZE_RNG
	wc_InitRng(&sm_rng);
#if !__NO_OUTPUT
	unsigned int currTimer = 0;
	int retVal = 0;
	// The first thing we want to do is to create our key and send it over to the Launcher
	currTimer = sysTimer::GetSystemMsTime();
	retVal += 
#endif
		TFIT_init_wbecc_dh_iECDHgreenSSfastP256(
		&sm_gamePipeEccCtx, // the context to use
		&TFIT_ecc_fast_iECDHgreenSSfastP256game,
		&ephemeralFast // a stand in ephemeral generation function
		);
#if !__NO_OUTPUT
	rlDebug("TFIT_init_wbecc_dh_iECDHgreenSSfastP256: %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
#endif
#if !__NO_OUTPUT
	currTimer = sysTimer::GetSystemMsTime();
	retVal += 
#endif
		TFIT_wbecc_dh_generate_key_iECDHgreenSSfastP256(&sm_gamePipeEccCtx);
#if !__NO_OUTPUT
	rlDebug("TFIT_wbecc_dh_generate_key_iECDHgreenSSfastP256: %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
#endif
#if !__NO_OUTPUT
	currTimer = sysTimer::GetSystemMsTime();
	retVal += 
#endif
		TFIT_wbecc_dh_get_ephemeral_public_key_iECDHgreenSSfastP256(
		&sm_gamePipeEccCtx, // the TransformIT context
		sm_gamePipeEccKey, // the buffer to store the ephmeral key in
		sizeof(sm_gamePipeEccKey), // the size of the buffer to store the ephemeral key in
		&sm_gamePipeEccKeyLength // will be written with how long of
		);
#if !__NO_OUTPUT
	rlDebug("TFIT_wbecc_dh_get_ephemeral_public_key_iECDHgreenSSfastP256: %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
#endif
	// Set our timers, accordingly
	//@@: location RLPCPIPE_CONSTRUCTOR_SET_END_TIMES
	sm_LauncherCheckStartTime = sysTimer::GetSystemMsTime();
	sm_LauncherCheckEndTime = fwRandom::GetRandomNumberInRange(CHALLENGE_REQUEST_MIN, CHALLENGE_REQUEST_MAX);
	//@@: } RLPCPIPE_CONSTRUCTOR

#endif
}

void rlPCPipe::OnPcEvent(const rlPcEvent* evt)
{
	if (!IsConnected())
		return;

	const unsigned evtId = evt->GetId();

	if(RLPC_EVENT_SIGN_IN_STATE_CHANGED == evtId)
	{
		rlDebug2("rlPCPipe::OnPcEvent(RLPC_EVENT_SIGN_IN_STATE_CHANGED)");

		rlPcEventSignInStateChanged* msg = (rlPcEventSignInStateChanged*)evt;
		if(msg->m_State & rgsc::STATE_SIGNED_OUT) 
		{
			rlPcPipeSignOutMessage* m = RL_NEW(rlPCPipe, rlPcPipeSignOutMessage);
			if (rlVerify(m))
			{
				if (!rlVerify(m->Init()) || !rlVerify(QueuePcPipeMessage(m)))
				{
					rlError("Failed to queue pipe message");
					RL_DELETE(m);
				}
			}
		}
		else if (msg->m_State & rgsc::STATE_SIGNED_IN)
		{
			// Extract the rockstar ID from the Profile Manager
			rgsc::Profile profile;
			HRESULT hr = g_rlPc.GetProfileManager()->GetSignInInfo(&profile);
			if(hr == ERROR_SUCCESS)
			{
				rgsc::RockstarId profileId = profile.GetProfileId();
				RequestSignInTransferFile(profileId);		
			}
		}
	}
}

void rlPCPipe::Connect(const char* pipeName)
{
	m_ReadBuffer[0] = '\0';
	memset(&m_overlap, 0, sizeof(OVERLAPPED));

	const char* localPipeName = (pipeName == NULL) ? PIPE_NAME : pipeName;

	m_pipeHandle = CreateFileA(localPipeName, GENERIC_READ | GENERIC_WRITE, 0, NULL, 3, FILE_FLAG_OVERLAPPED, NULL);
	m_bConnected = (m_pipeHandle != INVALID_HANDLE_VALUE);
	
	m_bIsStandardUser = false;

	m_bIsAlive = m_bConnected;
	m_LauncherAliveTimer.Reset();
#if ENABLE_LAUNCHER_CHALLENGE
	if(!m_bIsPreparedToChallenge)
	{
		//@@: location RLPCPIPE_CONNECT_SENDGAMEECCKEY
		SendGameEccKey();
	}
	//@@: location RLPCPIPE_CONNECT_EXIT
#endif

}

void rlPCPipe::Disconnect()
{	
	CloseHandle(m_pipeHandle);
	
	RemoveCallbacks();

	m_pipeHandle = INVALID_HANDLE_VALUE;
	m_bConnected = false;
	m_bIsStandardUser = false;
	m_bIsAlive = false;
}

void rlPCPipe::Update()
{
	if (IsConnected())
	{
		Read();
		ProcessMessageQueue();
	}

	//@@: range RLPCPIPE_UPDATE {
#if ENABLE_LAUNCHER_CHALLENGE
	if (m_bIsPreparedToChallenge && !m_bIsWaitingForChallenge && !m_Status.Pending() && 
#if RSG_FINAL
#if !__NO_OUTPUT
		sysTimer::HasElapsedIntervalMs(sm_LauncherCheckStartTime, 30*1000))
#else
		sysTimer::HasElapsedIntervalMs(sm_LauncherCheckStartTime, sm_LauncherCheckEndTime))
#endif
#else
		sysTimer::HasElapsedIntervalMs(sm_LauncherCheckStartTime, 1000))
#endif
	{
#if RSG_LAUNCHER_CHECK
		SendLauncherChallenge();
		sm_LauncherCheckStartTime = sysTimer::GetSystemMsTime();
		m_bIsWaitingForChallenge = true;
#endif
	}
#endif
#if ENABLE_LAUNCHER_CHALLENGE && RSG_LAUNCHER_CHECK
	else if(m_bIsWaitingForChallenge && sysTimer::HasElapsedIntervalMs(sm_LauncherCheckStartTime, TIME_ALLOWED_WITHOUT_LAUNCHER) )
	{
		//@@: location RLPCPIPE_UPDATE_SET_FAILED
		rlAssertf(false, "You've played for one full minute without the Launcher. This is not good.");
		m_Status.SetFailed();
		m_bIsWaitingForChallenge = false;
	}
#endif

	if (m_LauncherAliveTimer.GetMsTime() > 10000.0f)
	{
		m_bIsAlive = false;
	}
	//@@: } RLPCPIPE_UPDATE

}

void rlPCPipe::Read()
{
	if (IsConnected())
	{
		DWORD bytesRead = 0;

		if (GetOverlappedResult(m_pipeHandle, &m_overlap, &bytesRead, FALSE))
		{
			CheckIncomingData(bytesRead);

			memset(&m_overlap, 0, sizeof(OVERLAPPED));
			m_ReadBuffer[0] = '\0';

			BOOL result = ReadFile(m_pipeHandle, m_ReadBuffer, PIPE_BUFFER_SIZE, &bytesRead, &m_overlap);

			if (!result)
			{
				DWORD err = GetLastError();

				if (err == ERROR_IO_PENDING)
				{
					return;
				}
				else if (err == ERROR_HANDLE_EOF)
				{
					CheckIncomingData(bytesRead);
				}
			}
			else
			{
				CheckIncomingData(bytesRead);
			}
		}	
	}
}

bool rlPCPipe::Write(const char* writeBuffer)
{
	bool result = false;

	rlDebug3("rlPCPipe::Write: %s", writeBuffer);
	if (IsConnected() && writeBuffer)
	{
		DWORD bytesWritten = 0;
		unsigned char headerBuffer[4];

		// Build up our header buffer based on the size of the data we want to send over.
		SizeToHeaderSize(istrlen(writeBuffer), headerBuffer);

		char buffer[PIPE_BUFFER_SIZE];
		formatf(buffer, PIPE_BUFFER_SIZE, "%c%c%c%c%s", headerBuffer[0], headerBuffer[1], headerBuffer[2], headerBuffer[3], writeBuffer);

		// Length of the request string + 4 for header bytes.
		int bufferSize = istrlen(writeBuffer) + 4;

		// Write to the pipe.
		BOOL bResult = WriteFile(m_pipeHandle, buffer, bufferSize, &bytesWritten, NULL);

		result = (bResult == TRUE);
	}

	return result;
}

void rlPCPipe::ProcessMessageQueue()
{
	if (!IsConnected())
		return;

	// An async pipe task is in progress, 
	if (m_SignInTaskStatus.Pending())
		return;

	while (m_MessageQueue.GetCount() > 0)
	{
		rlPcPipeMessageBase* msg = m_MessageQueue.Pop();
		rlVerify(Write(msg->GetBuffer()));
		RL_DELETE(msg);
	}
}

void rlPCPipe::CheckIncomingData(DWORD bytesRead)
{
	char content[PIPE_BUFFER_SIZE] = {0};
	char contentType[MSG_TYPE_SIZE + 1] = {0};
	char skuName[SKU_NAME_SIZE] = {0};

	char* buffer = m_ReadBuffer;
	DWORD offset = 0;

	// Nothing to check
	if (!bytesRead)
	{
		return;
	}

	// Incoming Data Format
	// <4 bytes> - Content Size
	// <5 bytes> - Content Type (ex. PROG:)
	// <? bytes> - Sku Name
	// <? bytes> - Message Buffer ( ContentSize - ContentTypeSize - SkuNameSize)
	while (offset < bytesRead)
	{
		int contentSize = ReadHeaderAmount(buffer);
		
		if (offset + contentSize > PIPE_BUFFER_SIZE)
		{
			rlError("rlPCPipe -> Message buffer overflow.");
			return;
		}

		// Invalid content size
		if (contentSize == 0)
		{
			rlError("rlPCPipe -> Message sent with zero size");
			return;
		}

		char* incomingData = (char*)alloca(contentSize);
		memcpy(incomingData, buffer + MSG_HEADER_SIZE, contentSize);

		// Read the packet type
		for (int i = 0; i < 5; i++)
		{
			contentType[i] = incomingData[i];
			contentType[i + 1] = '\0';
		}

		int skuNameSize = -1;

		// Read the sku name
		for (int i = 0; i < SKU_NAME_SIZE; i++)
		{
			char currentChar = incomingData[i + MSG_TYPE_SIZE];
			if (currentChar == ':')
			{
				skuName[i] = '\0';
				skuNameSize = i + 1;
				break;
			}
			else
			{
				skuName[i] = currentChar;
				skuName[i + 1] = '\0';
			}
		}

		// Make sure we have a valid sku name
		if (skuNameSize == -1)
		{
			rlError("rlPCPipe -> Invalid sku name size");
			return;
		}

		int contentSizeNoType = contentSize - (skuNameSize + MSG_TYPE_SIZE);

		// Read the content
		for (int i = 0; i < contentSizeNoType; i++)
		{
			// Offset from the content size, content type, sku name size
			// content size = 4
			// content type = 5 <PROG:>
			content[i] = incomingData[i + MSG_TYPE_SIZE + skuNameSize];
			content[i + 1] = '\0';
		}
		//@@: location RLPCPIPE_CHECKINCOMINGDATA_HANDLE_MESSAGE
		HandleMessage(contentType, skuName, content, contentSizeNoType);

		buffer += (contentSize + MSG_HEADER_SIZE);
		offset += (contentSize + MSG_HEADER_SIZE);
		//@@: location RLPCPIPE_CHECKINCOMINGDATA_HANDLE_MESSAGE_END_LOOP
	}
}

void rlPCPipe::HandleMessage(char* type, char* skuName, char* messageBuffer, int UNUSED_PARAM(bufferSize))
{
	if (!stricmp(type, "PROG:"))
	{
		// We always null terminate the message buffer so this should be fine.
		int progress = atoi(messageBuffer);

#if !__DEV
		char fullString[2048];
		formatf(fullString, 2048, "Progress: %d\n", progress);
		Printf(fullString);
#endif

		if (m_downloadProgressCallback)
			m_downloadProgressCallback(progress, skuName);
	}
	else if (!stricmp(type, "COMP:"))
	{
		if (m_transferCompleteCallback)
			m_transferCompleteCallback(skuName);
	}
	else if (!stricmp(type, "ERRO:"))
	{
		if (m_downloadErrorCallback)
			m_downloadErrorCallback(messageBuffer, skuName);
	}
	else if (!stricmp(type, "UACE:"))
	{
		// Launcher needs to let us know we are running as a standard user because we cannot install DLC as a standard user.
		m_bIsStandardUser = true;
	}
	else if (!stricmp(type, "PING:"))
	{
		m_bIsAlive = true;
		m_LauncherAliveTimer.Reset();
	}
	else if(!stricmp(type, "LSCT:"))
	{
		RockstarId rockstarId;
		if (rlVerify(1 == sscanf(skuName, "%" I64FMT "d", &rockstarId)))
		{
			g_rlPc.GetProfileManager()->OnLauncherReceivedUpdatedSocialClubTicket(rockstarId, messageBuffer);
		}
	}
#if ENABLE_LAUNCHER_CHALLENGE
	//@@: range RLPCPIPE_HANDLEMESSAGE_CHALLENGE_RESPONSE_SECTION {
	else if (!stricmp(type, "TINI:") && !m_bIsPreparedToChallenge)
	{
		// This is the back end of the INIT call, that carries back
		// the launcher key
#if !__NO_OUTPUT
		unsigned int currTimer = 0;
		int retVal = 0;
#endif
		// First lets bring it in as a byte array
		unsigned char launcherPipeEccKey[CURVE_BUFFER_SIZE] = {0};
		unsigned int  sharedSecretLen = 0;
		// Copy it into our byte array
		rlDebug("%s:%s:%s", type, skuName, messageBuffer);
		//@@: location RLPCPIPE_HANDLEMESSAGE_TINI_CONVERT_ECC_KEY
		rlPcPipeUtility::hexStringToArray(launcherPipeEccKey, messageBuffer, CURVE_BUFFER_STRING_SIZE);
		// Compute the shared secret
#if !__NO_OUTPUT
		currTimer = sysTimer::GetSystemMsTime();
		retVal+=
#endif
			TFIT_wbecc_dh_compute_secret_iECDHgreenSSfastP256(
			&sm_gamePipeEccCtx,
			TFIT_WBECC_DH_EPHEMERAL_UNIFIED_MODEL,
			NULL, 
			launcherPipeEccKey, 
			sm_gamePipeEccKeyLength,
			sm_gamePipeSharedSecret,
			SHARED_SECRET_SIZE,
			&sharedSecretLen);
		// Prepare our dynamic key
#if !__NO_OUTPUT
		rlAssertf(retVal == 0, "TFIT_wbecc_dh_compute_secret_iECDHgreenSSfastP256 failed.");
		rlDebug("TFIT_wbecc_dh_compute_secret_iECDHgreenSSfastP256 %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
		char outBuff[GENERIC_BUFFER_SIZE] = {0};
		rlPcPipeUtility::hexArrayToString(outBuff, sm_gamePipeSharedSecret, SHARED_SECRET_SIZE);
		rlDebug("Shared Secret Key: 0x%s", outBuff);
#endif

		unsigned int dynamicKeyLength = 0;

#if !__NO_OUTPUT
		currTimer = sysTimer::GetSystemMsTime();
		retVal+=
#endif

		//@@: range RLPCPIPE_HANDLEMESSAGE_TINI_PREPARE_DYNAMIC_KEYS {
			TFIT_prepare_dynamic_key_iAES13_green(sm_gamePipeSharedSecret, sharedSecretLen/2,(uint8_t *)&sm_gamePipeEncryptionKey, sizeof(sm_gamePipeEncryptionKey), &dynamicKeyLength);
#if !__NO_OUTPUT
		rlAssertf(retVal == 0, "TFIT_prepare_dynamic_key_iAES13_green failed.");
		rlDebug("TFIT_prepare_dynamic_key_iAES13_green %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
		//rlDebug(rlPcPipeUtility::printPointer("TFIT_prepare_dynamic_key_iAES13_green:input:", (uint8_t *)&sm_gamePipeEncryptionKey, sizeof(sm_gamePipeEncryptionKey)));
		retVal+=
#endif
			TFIT_prepare_dynamic_key_iAES14_green(sm_gamePipeSharedSecret, sharedSecretLen/2,(uint8_t *)&sm_gamePipeDecryptionKey, sizeof(sm_gamePipeDecryptionKey), &dynamicKeyLength);
#if !__NO_OUTPUT
		rlAssertf(retVal == 0, "TFIT_prepare_dynamic_key_iAES14_green failed.");
		rlDebug("TFIT_prepare_dynamic_key_iAES14_green %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
		//rlDebug(rlPcPipeUtility::printPointer("TFIT_prepare_dynamic_key_iAES14_green:input:", (uint8_t *)&sm_gamePipeDecryptionKey, sizeof(sm_gamePipeDecryptionKey)));
#endif
		// Set this only in final builds, if we want to do the launcher check. 
		// This is mostly so people's bank release builds don't break if they run
		// The game without the launcher.
		//@@: location RLPCPIPE_HANDLEMESSAGE_TINI_PREPARE_CHALLENGE
		m_bIsPreparedToChallenge = true;
		//@@: } RLPCPIPE_HANDLEMESSAGE_TINI_PREPARE_DYNAMIC_KEYS

	}

	else if (!stricmp(type, "MCHL:") && m_bIsWaitingForChallenge)
	{
		unsigned char *launcherResponse = rage_new unsigned char[HMAC_SIZE];
		//@@: location RLPCPIPE_HANDLEMESSAGE_MCHL_CONVERT_RESPONSE
		rlPcPipeUtility::hexStringToArray(launcherResponse, messageBuffer, HMAC_STRING_SIZE);
		//@@: location RLPCPIPE_HANDLEMESSAGE_MCHL_WAIT_FOR_CHALLENGE
		m_bIsWaitingForChallenge = false;
		//@@: location RLPCPIPE_HANDLEMESSAGE_MCHL_CALCULATE_HMAC
		CalculateHMACAndCompare(0, &m_Status, sm_challenge, launcherResponse);
	}
	//@@: } RLPCPIPE_HANDLEMESSAGE_CHALLENGE_RESPONSE_SECTION

#endif
}

void rlPCPipe::PauseDownloads()
{
	Write("PAUSE");
}

void rlPCPipe::ResumeDownloads()
{
	Write("RESUME");
}

void rlPCPipe::RestartGame()
{
	Write("RESTART");
}

bool rlPCPipe::QueuePcPipeMessage(rlPcPipeMessageBase* msg)
{
	rlDebug3("Queued pipe message with name: %s", msg->GetMessageName());

	if (!IsConnected())
		return false;

	return m_MessageQueue.Push(msg);
}

bool rlPCPipe::ForwardCredentialsToLauncher(const char* xmlResponse)
{
	rlPcPipeTicketChangedMessage* msg = NULL;
	rtry
	{
		rgsc::Profile profile;
		HRESULT hr = g_rlPc.GetProfileManager()->GetSignInInfo(&profile);
		rcheck(hr == ERROR_SUCCESS, catchall, );

		msg = RL_NEW(rlRosCredentialsHelper, rlPcPipeTicketChangedMessage);
		rverify(msg, catchall, );
		rverify(msg->Init(profile.GetProfileId(), xmlResponse), catchall, );

		rverify(g_rlPc.GetDownloaderPipe()->QueuePcPipeMessage(msg), catchall, );

		return true;
	}
	rcatchall
	{
		if (msg)
		{
			RL_DELETE(msg);
		}
	}

	return false;
}

bool rlPCPipe::RequestSignInTransferFile(RockstarId id)
{
	if (!IsConnected())
		return false;

	rlDebug3("RequestSignInTransferFile for Rockstar ID: %"I64FMT"d", id);

	if (m_SignInTaskStatus.Pending())
		return false;

	// Define our task
	rlPcPipeSigninTask* task = NULL;

	rtry												
	{	
		// Create, configure, and call our task
		rverify(netTask::Create(&task, &m_SignInTaskStatus), catchall, );																								
		rverify(task->Configure(id), catchall, );	
		rverify(netTask::Run(task), catchall, );			
		return true;										
	}													
	rcatchall											
	{													
		if(task != NULL)									
		{													
			netTask::Destroy(task);								
		}		
		return false;										
	}
}

void rlPCPipe::StartDownloadWithToken(const char* skuName, const char* downloadToken)
{
	if (rlVerify(skuName) && rlVerify(downloadToken))
	{
		char buffer[PIPE_BUFFER_SIZE];
		formatf(buffer, PIPE_BUFFER_SIZE, "DOWN:%s:%s", skuName, downloadToken);
		Write(buffer);
	}
}
#if ENABLE_LAUNCHER_CHALLENGE
bool rlPCPipe::CalculateHMACAndCompare(const int localGamerIndex, netStatus* status, unsigned char *challenge, unsigned char *clientHmac)
{
	// Define our task
	netFireAndForgetTask<rlComputeHMACTask>* task = NULL;
	rtry												
	{	
		// Be sure that our varaibles aren't NULL
		rverify(challenge!=NULL, catchall, );
		rverify(clientHmac!=NULL, catchall, );

		// Create, configure, and call our task
		//@@: range RLPCPIPE_CALCULATEHMACANDCOMPARE {
		rverify(netTask::Create(&task, status), catchall, );																								
		rverify(task->Configure(localGamerIndex, challenge, clientHmac), catchall, );	
		rverify(netTask::Run(task), catchall, );	
		//@@: } RLPCPIPE_CALCULATEHMACANDCOMPARE

		return true;										
	}													
	rcatchall											
	{													
		rlDebug("Failed to create CalculateHMAC task. Destroying");
		if(task != NULL)									
		{													
			netTask::Destroy(task);								
		}		
		return false;										
	}
}

#define POST_DECRYPT_CHECK 1

void rlPCPipe::SendLauncherChallenge()
{
	// Define our task
	netFireAndForgetTask<rlPcPipeSendLauncherChallengeTask>* task = NULL;
	rtry												
	{	
		// Create, configure, and call our task
		rverify(netTask::Create(&task, &m_ChallengeStatus), catchall, );																								
		rverify(task->Configure(sm_challenge), catchall, );	
		rverify(netTask::Run(task), catchall, );
		return;										
	}													
	rcatchall											
	{													
		rlDebug("Failed to create CalculateHMAC task. Destroying");
		if(task != NULL)									
		{													
			netTask::Destroy(task);								
		}		
		return;										
	}

}
void rlPCPipe::SendGameEccKey()
{
	char outputBuffer[GENERIC_BUFFER_SIZE*2] = {0};
	char keyBuff[GENERIC_BUFFER_SIZE] = {0};
	char ivBuff[GENERIC_BUFFER_SIZE] = {0};
	
	block_t generatedIV;
	//@@: range RLPCPIPE_SENDGAMEECCKEY {

	wc_RNG_GenerateBlock(&sm_rng, generatedIV, IV_SIZE);
	memcpy(sm_decryptionIV, generatedIV, IV_SIZE);
	memcpy(sm_encryptionIV, generatedIV, IV_SIZE);
	//@@: location RLPCPIPE_SENDGAMEECCKEY_CONVERT_STRINGS
	rlPcPipeUtility::hexArrayToString(keyBuff, sm_gamePipeEccKey, sm_gamePipeEccKeyLength);
	rlPcPipeUtility::hexArrayToString(ivBuff, generatedIV, IV_SIZE);
	formatf(outputBuffer, "INIT:%s%s", ivBuff, keyBuff);
	//@@: location RLPCPIPE_SENDGAMEECCKE_WRITEOUTPUTBUFFER
	Write(outputBuffer);
	//@@: } RLPCPIPE_SENDGAMEECCKEY


}
#endif

void rlPCPipe::StartDownloadWithUrl(const char* skuName, const char* downloadUrl, const char* localFilename, const char* arguments)
{
	if (rlVerify(skuName) && rlVerify(downloadUrl))
	{
		char filenameBuffer[FILENAME_MAX];
		if (arguments && !localFilename)
		{
			formatf(filenameBuffer, FILENAME_MAX, "%s.exe", skuName);
			localFilename = filenameBuffer;
		}

		char buffer[PIPE_BUFFER_SIZE];

		if (localFilename)
		{
			if (arguments)
			{
				formatf(buffer, PIPE_BUFFER_SIZE, "DOWNURL\n%s\n%s\n%s\n%s", skuName, downloadUrl, localFilename, arguments);
			}
			else
			{
				formatf(buffer, PIPE_BUFFER_SIZE, "DOWNURL\n%s\n%s\n%s", skuName, downloadUrl, localFilename);
			}
		}
		else
		{
			formatf(buffer, PIPE_BUFFER_SIZE, "DOWNURL\n%s\n%s", skuName, downloadUrl);
		}

		Write(buffer);
	}
}


int rlPCPipe::ReadHeaderAmount(char* buffer)
{
	// Fixing the read-header-amount logic on the game-end as well
	// Not and-ing gainst the appropriate masks actually yields
	// incorrectly signed values
	int amount = 0;
	amount = (((buffer[3] << 24) & 0xFF000000) |
			  ((buffer[2] << 16) & 0x00FF0000) | 
			  ((buffer[1] <<  8) & 0x0000FF00) |
			  ((buffer[0])		 & 0x000000FF));
	return amount;
}

void rlPCPipe::SizeToHeaderSize(int size, unsigned char* header)
{
	header[0] = size & 0x000000FF;
	header[1] = (size >> 8) & 0x000000FF;
	header[2] = (size >> 16) & 0x000000FF;
	header[3] = (size >> 24) & 0x000000FF;
}

void rlPCPipe::AddCallbacks(pipeDownloadProgressCallback progressCallback, pipeTransferCompleteCallback completeCallback, pipeDownloadErrorCallback errorCallback)
{
	if (progressCallback != NULL && m_downloadProgressCallback == NULL)
		m_downloadProgressCallback = progressCallback;
	if (completeCallback != NULL && m_transferCompleteCallback == NULL)
		m_transferCompleteCallback = completeCallback;
	if (errorCallback != NULL && m_downloadErrorCallback == NULL)
		m_downloadErrorCallback = errorCallback;
}

void rlPCPipe::RemoveCallbacks()
{
	m_transferCompleteCallback = NULL;
	m_downloadProgressCallback = NULL;
	m_downloadErrorCallback = NULL;
}


#if (RSG_CPU_X64 && !__RGSC_DLL)

	//////////////////////////////////////////////////////
	// rlComputeHMACTask
	//////////////////////////////////////////////////////
	rlComputeHMACTask::rlComputeHMACTask()
		: m_State(STATE_BEGIN_REQUEST),
		m_challenge(NULL),
		m_clientHmac(NULL)
	{

	}

	rlComputeHMACWorkItem::rlComputeHMACWorkItem()
		: m_challenge(NULL),
		m_clientHmac(NULL)
	{

	}

	rlComputeHMACWorkItem::~rlComputeHMACWorkItem()
	{

	}
	void rlComputeHMACWorkItem::DoWork()
	{
		//@@: location RLCOMPUTEHMACWORKITEM_DOWORK_ENTRY
#if !__NO_OUTPUT
		unsigned int startTimer = sysTimer::GetSystemMsTime();
#endif
		// Now we're moving the decryption of the AES into here to offload some of the guard work.
		
#if !__NO_OUTPUT && ENCRYPT_COMMS
		// Decrypt the contents - create a dynamic buffer to be deleted later
		rlDebug("MCHL:encrypted:%s", m_clientHmac);
		char outBuff[GENERIC_BUFFER_SIZE] = {0};
		rlPcPipeUtility::hexArrayToString(outBuff, sm_decryptionIV, 16);
		rlDebug("block_t counter:0x%s", outBuff);
#endif
#if ENCRYPT_COMMS
		unsigned char *decryptedLauncherResponse = rage_new unsigned char [HMAC_SIZE];
		memset(decryptedLauncherResponse, 0, HMAC_SIZE);
		//@@: range RLCOMPUTEHMACWORKITEM_DOWORK_BODY {
		Decrypt(decryptedLauncherResponse, m_clientHmac);
#else
		rlDebug("MCHL:plaintext:%s", messageBuffer);

#endif

#if !__NO_OUTPUT && ENCRYPT_COMMS
		memset(outBuff, 0, GENERIC_BUFFER_SIZE);
		rlPcPipeUtility::hexArrayToString(outBuff, sm_decryptionIV, 16);
		rlDebug("MCHL:decrypted:%s",outBuff);
#endif
		rlDebug("Launcher Challenge Decrypt Took %d ms",sysTimer::GetSystemMsTime()-startTimer);
#if !__NO_OUTPUT
		unsigned int currTimer = 0;
#endif
		unsigned int retVal = 0;
		m_Status->SetPending();
		wbsha_hmac_key_t *hmacKey = (wbsha_hmac_key_t*)&TFIT_hmac_key_iHMACredK;
#if !__NO_OUTPUT
		currTimer = sysTimer::GetSystemMsTime();
#endif 
		retVal +=	TFIT_init_wbsha_hmac_iHMACredK(
			&m_hmacCtx, // Transformit context
			hmacKey, // The whitebox HMAC key
			TFIT_WBSHA_SHA2_256 // Specify SHA-256 as the underlying hash function
			);

		rlAssertf(retVal == 0, "TFIT_init_wbsha_hmac_iHMACredK failed.");
		rlDebug("TFIT_init_wbsha_hmac_iHMACredK %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
		
#if !__NO_OUTPUT
		currTimer = sysTimer::GetSystemMsTime();
		char inputBuff[GENERIC_BUFFER_SIZE];
		rlPcPipeUtility::hexArrayToString(inputBuff, m_challenge, HMAC_SIZE);
		rlDebug("TFIT_init_wbsha_hmac_iHMACredK:input:%s", inputBuff);
#endif

		//@@: location RLCOMPUTEHMACWORKITEM_DOWORK_UPDATE_WBSHA
		retVal = TFIT_update_wbsha_hmac_iHMACredK(
			&m_hmacCtx,		// TransformIT context
			m_challenge,	// The buffer to generate an HMAC tag for
			HMAC_SIZE		// The size of the buffer
			);
		rlAssertf(retVal == 0, "TFIT_update_wbsha_hmac_iHMACredK failed.");
		rlDebug("TFIT_update_wbsha_hmac_iHMACredK %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);

		/*
		* Generate the HMAC tag using Trasnformit whitebox
		* HMAC-SHA-256
		*/
		uint8_t hmacTag[HMAC_SIZE] = {0};
		unsigned int hmacTagLen = 0;
	#if !__NO_OUTPUT
		currTimer = sysTimer::GetSystemMsTime();
		retVal = 
	#endif
		TFIT_final_wbsha_hmac_iHMACredK(
				&m_hmacCtx,			// TransformIT context
				WBSHA_HMAC_SIGN,	// Specify tag generation
				NULL,				// No signature to verify
				0,					// No signature length
				hmacTag,			// Store results here
				HMAC_SIZE,			// Size of the results buffer
				&hmacTagLen // Record the length of the generated tag
				);
#if !__NO_OUTPUT
		rlAssertf(retVal == 0, "TFIT_final_wbsha_hmac_iHMACredK failed.");
		rlDebug("TFIT_final_wbsha_hmac_iHMACredK %d - %d ms", retVal, sysTimer::GetSystemMsTime() - currTimer);
#endif
		char hmacDebuff[GENERIC_BUFFER_SIZE];
		char clihDebuff[GENERIC_BUFFER_SIZE]; 
		rlPcPipeUtility::hexArrayToString(hmacDebuff, hmacTag, HMAC_SIZE);
		rlPcPipeUtility::hexArrayToString(clihDebuff, decryptedLauncherResponse, HMAC_SIZE);

		if(retVal!=0 || memcmp(hmacTag, decryptedLauncherResponse, HMAC_SIZE)!=0)
		{
			rlDebug("====/==== HMACs were NOT equal");
			rlDebug("====/==== Client: %s", clihDebuff);
			rlDebug("====/==== Mine  : %s", hmacDebuff);
			m_Status->SetFailed();
		}
		else
		{
			rlDebug("========= HMACs were equal");
			m_Status->SetSucceeded();
		}

		//@@: } RLCOMPUTEHMACWORKITEM_DOWORK_BODY 

		//@@: location RLCOMPUTEHMACWORKITEM_DOWORK_EXIT
		memset(decryptedLauncherResponse, 0, HMAC_SIZE);
		memset(hmacDebuff, 0, GENERIC_BUFFER_SIZE);
		memset(clihDebuff, 0, GENERIC_BUFFER_SIZE);
#if !__NO_OUTPUT
		memset(inputBuff, 0, GENERIC_BUFFER_SIZE);
		memset(outBuff, 0, GENERIC_BUFFER_SIZE);
#endif
	}
	bool rlComputeHMACWorkItem::Configure(const int localGamerIndex, unsigned char * challengeToCompute, unsigned char *clientHmac, netStatus *status)
	{
		rtry
		{
			rverify(challengeToCompute!=NULL, catchall,)
				m_challenge = challengeToCompute;
			rverify(clientHmac!=NULL, catchall, )
				m_clientHmac = clientHmac;
			rverify(status!=NULL, catchall, )
				m_Status = status;
			rverify(m_clientHmac == clientHmac, catchall,)
				m_localGamerIndex = localGamerIndex;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlComputeHMACTask::Configure(const int localGamerIndex, unsigned char * challengeToCompute, unsigned char *clientHmac)
	{
		rtry
		{
			//@@: range RLCOMPUTEHMACTASK_CONFIGURE {
			rverify(challengeToCompute!=NULL, catchall,)
				m_challenge = challengeToCompute;
			//@@: location RLCOMPUTEHMACTASK_CONFIGURE_MIDWAY
			rverify(m_challenge == challengeToCompute, catchall,)
				rverify(clientHmac!=NULL, catchall, )
				m_clientHmac = clientHmac;
			rverify(m_clientHmac == clientHmac, catchall,)
				m_localGamerIndex = localGamerIndex;
			//@@: } RLCOMPUTEHMACTASK_CONFIGURE

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlComputeHMACTask::OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);

		if (GetWorkItem())
		{
			GetWorkItem()->Cancel();
		}
	}

	bool rlComputeHMACTask::CreateRequest()
	{
		if (!GetWorkItem()->Configure(m_localGamerIndex, m_challenge, m_clientHmac, &m_MyStatus))
		{
			rlDebug("Failed to configure rlComputeHMACTask");
			return false;
		}

		if (!netThreadPool.QueueWork(GetWorkItem()))
		{
			rlError("netThreadPool failed to queue WorkItem");
			return false;
		}

		return true;

	}
	void rlComputeHMACTask::OnCleanup()
	{
		//@@: location RLCOMPUTEHMACTASK_ONCLEANUP
		if(m_clientHmac)
		{
			for(int i = 0; i < HMAC_SIZE; i++)
				m_clientHmac[i]=0;
			m_clientHmac = NULL;
			delete m_clientHmac;
		}
		if(m_challenge)
		{
			for(int i = 0; i < HMAC_SIZE; i++)
				m_challenge[i]=0;
			m_challenge = NULL;
			delete m_challenge;
		}
	}

	netTaskStatus rlComputeHMACTask::OnUpdate(int* /*resultCode*/)
	{
		{
			netTaskStatus status = NET_TASKSTATUS_PENDING;

			switch(m_State)
			{
			case STATE_BEGIN_REQUEST:
				if(WasCanceled())
				{
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if(CreateRequest())
				{
					m_State = STATE_WAIT_REQUEST;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
				break;

			case STATE_WAIT_REQUEST:
				if (GetWorkItem() && GetWorkItem()->Finished() && !GetWorkItem()->Pending())
				{
					if (WasCanceled())
					{
						status = NET_TASKSTATUS_FAILED;
					}
					else if (GetWorkItem()->Succeeded())
					{
						//@@: location RLCOMPUTEHMACTASK_ONUPDATE_SUCCEEDED
						netTaskDebug("Calculating the HMAC succeeded");
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else if (GetWorkItem()->Failed())
					{
						//@@: location RLCOMPUTEHMACTASK_ONUPDATE_FAILED
						netTaskDebug("Calculating the HMAC failed");
						status = NET_TASKSTATUS_FAILED;
					}
				}
			}

			return status;
		}
	}
#endif

	///////////////////////////////////////////////////////////////////////
	// rlPcPipeCreateSignInTransferDataWorkItem
	///////////////////////////////////////////////////////////////////////
	rlPcPipeCreateSignInTransferDataWorkItem::rlPcPipeCreateSignInTransferDataWorkItem()
		: m_ResultPtr(NULL)
	{

	}

	rlPcPipeCreateSignInTransferDataWorkItem::~rlPcPipeCreateSignInTransferDataWorkItem()
	{

	}

	bool rlPcPipeCreateSignInTransferDataWorkItem::Configure(bool* resultPtr)
	{
		rtry
		{
			rverify(resultPtr, catchall, );
			m_ResultPtr = resultPtr;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlPcPipeCreateSignInTransferDataWorkItem::DoWork()
	{
		bool success = g_rlPc.GetProfileManager()->CreateSigninTransferProfile();
		*m_ResultPtr = success;
	}

	///////////////////////////////////////////////////////////////////////
	// rlPcPipeSigninTask
	///////////////////////////////////////////////////////////////////////
	rlPcPipeSigninTask::rlPcPipeSigninTask()
		: m_State(STATE_CREATE_SIGNIN_TRANSFER)
		, m_SignInResult(false)
		, m_RockstarId(InvalidRockstarId)
	{

	}

	bool rlPcPipeSigninTask::Configure(RockstarId id)
	{
		rtry
		{
			rverify(id != InvalidRockstarId, catchall, );
			m_RockstarId = id;

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	netTaskStatus rlPcPipeSigninTask::OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		if (WasCanceled())
		{
			status = NET_TASKSTATUS_FAILED;
			return status;
		}

		switch(m_State)
		{
		case STATE_CREATE_SIGNIN_TRANSFER:
			if (m_WorkItem.Configure(&m_SignInResult) && netThreadPool.QueueWork(&m_WorkItem))
			{
				m_State = STATE_CREATING_SIGNIN_TRANSFER;
			}
			else
			{
				netTaskError("Failed in STATE_CREATE_SIGNIN_TRANSFER");
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_CREATING_SIGNIN_TRANSFER:
			if (m_WorkItem.Finished())
			{
				if (m_SignInResult)
				{
					m_State = STATE_SEND_PC_PIPE;
				}
				else
				{
					netTaskError("Creating signin transfer file failed.");
					m_State = STATE_SEND_PC_PIPE;
				}
			}
			else if (!m_WorkItem.Pending())
			{
				netTaskError("Failed in STATE_CREATING_SIGNIN_TRANSFER");
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_SEND_PC_PIPE:
			{
				rlPcPipeSignInMessage* m = RL_NEW(rlPCPipe, rlPcPipeSignInMessage);
				if (m)
				{
					if (m->Init(m_RockstarId) &&
						g_rlPc.GetDownloaderPipe()->QueuePcPipeMessage(m))
					{
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						netTaskError("Failed to queue pipe message");
						RL_DELETE(m);
						status = NET_TASKSTATUS_FAILED;
					}
				}			
				else
				{
					netTaskError("Failed to allocate rlPcPipeSignInMessage");
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		}

		return status;
	};

#if ENABLE_LAUNCHER_CHALLENGE
	///////////////////////////////////////////////////////////////////////
	// rlPcPipeSendLauncherChallengeWorkItem
	///////////////////////////////////////////////////////////////////////
	rlPcPipeSendLauncherChallengeWorkItem::rlPcPipeSendLauncherChallengeWorkItem()
		: m_challengePtr(NULL)
	{

	}

	rlPcPipeSendLauncherChallengeWorkItem::~rlPcPipeSendLauncherChallengeWorkItem()
	{

	}

	bool rlPcPipeSendLauncherChallengeWorkItem::Configure(unsigned char * cp)
	{
		rtry
		{
			rverify(cp, catchall, );
			m_challengePtr = cp;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void rlPcPipeSendLauncherChallengeWorkItem::DoWork()
	{

		
		// Generate Random Bytes as our challenge
		//@@: location RLPCPIPESENDLAUNCHERCHALLENGEWORKITEM_DOWORK_ENTRY
		wc_RNG_GenerateBlock(&sm_rng, sm_challenge, HMAC_SIZE);
#if ENCRYPT_COMMS
		// Encrypt the data
		char encryptedBuffer[GENERIC_BUFFER_SIZE] = {0};

#if !__NO_OUTPUT
		char printBuff[GENERIC_BUFFER_SIZE] = {0};
		//@@: range RLPCPIPESENDLAUNCHERCHALLENGEWORKITEM_DOWORK_BODY {
		rlPcPipeUtility::hexArrayToString(printBuff, sm_challenge,HMAC_SIZE);
		rlDebug("TRVR:decrypted:%s", printBuff);
		memset(printBuff, 0, GENERIC_BUFFER_SIZE);

		rlPcPipeUtility::hexArrayToString(printBuff, sm_encryptionIV, 16);
		rlDebug("block_t counter:0x%s", printBuff);
		memset(printBuff, 0, GENERIC_BUFFER_SIZE);

		char tempBuff[GENERIC_BUFFER_SIZE] = {0};
		rlPcPipeUtility::hexArrayToString(tempBuff, sm_challenge,HMAC_SIZE);
		rlDebug("TRVR:decrypted:%s",tempBuff);
#endif
		//@@: location RLPCPIPESENDLAUNCHERCHALLENGEWORKITEM_DOWORK_ENCRYPT_AND_STRINGIFY
		EncryptAndStringify(encryptedBuffer, sm_challenge);

		// Convert the data to a string, and send it along
		rlDebug("TRVR:encrypted:%s", encryptedBuffer);

#else
		// Print it out into ASCII with proper formatting
		char printBuff[GENERIC_BUFFER_SIZE] = {0};
		rlPcPipeUtility::hexArrayToString(printBuff, sm_challenge, CHALLENGE_SIZE);
		formatf(outputBuffer, "TRVR:%s", printBuff);
		rlDebug("TRVR:plaintext:%s", printBuff);
#endif

		// Write it out to the pipe
		rlPcPipeLauncherChallengeMessage* m = RL_NEW(rlPCPipe, rlPcPipeLauncherChallengeMessage);
		// Initialize the buffer
		m->Init(encryptedBuffer);
		// Queue up the message
		//@@: } RLPCPIPESENDLAUNCHERCHALLENGEWORKITEM_DOWORK_BODY

		//@@: location RLPCPIPESENDLAUNCHERCHALLENGEWORKITEM_DOWORK_EXIT
		g_rlPc.GetDownloaderPipe()->QueuePcPipeMessage(m);
		
	}

	void rlPcPipeLauncherBaseWorkItem::EncryptAndStringify(char * dst, unsigned char * input)
	{
		// Encrypt the data
		unsigned char encryptedBuffer[GENERIC_BUFFER_SIZE] = {0};
		Encrypt(encryptedBuffer, input);
		// Convert the data to a string, and send it along
		rlPcPipeUtility::hexArrayToString(dst, encryptedBuffer, HMAC_SIZE);
	}

	void rlPcPipeLauncherBaseWorkItem::Encrypt(unsigned char*dst, unsigned char*input)
	{
#if !__NO_OUTPUT
		int retVal = 0;
		int currTimer = sysTimer::GetSystemMsTime();
		//rlDebug(rlPcPipeUtility::printPointer("pre_crypt:input:", input, HMAC_SIZE));
		//rlDebug(rlPcPipeUtility::printPointer("pre_crypt:desti:", dst, HMAC_SIZE));
		char tempBuff[GENERIC_BUFFER_SIZE] = {0};
		rlPcPipeUtility::hexArrayToString(tempBuff, sm_encryptionIV,16);
		rlDebug("block_t counter:0x%s",tempBuff);
		retVal+=
#endif
			TFIT_wbaes_cbc_encrypt_iAES13(&sm_gamePipeEncryptionKey, input, HMAC_SIZE, sm_encryptionIV, dst);
#if !__NO_OUTPUT
		rlAssertf(retVal == 0, "TFIT_wbaes_cbc_encrypt_iAES13 failed.");
		rlDebug("TFIT_wbaes_cbc_encrypt_iAES13:%d:%dms", retVal, sysTimer::GetSystemMsTime()-currTimer);
		//rlDebug(rlPcPipeUtility::printPointer("pst_crypt:input:", input, HMAC_SIZE));
		//rlDebug(rlPcPipeUtility::printPointer("pst_crypt:desti:", dst, HMAC_SIZE));
#endif

	}

	void rlPcPipeLauncherBaseWorkItem::Decrypt(unsigned char*dst, unsigned char*input)
	{
#if !__NO_OUTPUT
		int retVal = 0;
		int currTimer = sysTimer::GetSystemMsTime();
		//rlDebug(rlPcPipeUtility::printPointer("pre_crypt:input:", input, HMAC_SIZE));
		//rlDebug(rlPcPipeUtility::printPointer("pre_crypt:desti:", dst, HMAC_SIZE));
		char tempBuff[GENERIC_BUFFER_SIZE] = {0};
		rlPcPipeUtility::hexArrayToString(tempBuff, sm_decryptionIV,16);
		//rlDebug("block_t counter:0x%s",tempBuff);
		retVal+=
#endif
			TFIT_wbaes_cbc_decrypt_iAES14(&sm_gamePipeDecryptionKey, input, HMAC_SIZE, sm_decryptionIV, dst);
#if !__NO_OUTPUT
		rlAssertf(retVal == 0, "TFIT_wbaes_cbc_decrypt_iAES14 failed.");
		rlDebug("TFIT_wbaes_cbc_decrypt_iAES14:%d:%dms", retVal, sysTimer::GetSystemMsTime()-currTimer);
		rlPcPipeUtility::hexArrayToString(tempBuff, sm_decryptionIV,16);
		rlDebug("block_t counter:0x%s",tempBuff);
		//rlDebug(rlPcPipeUtility::printPointer("pst_crypt:input:", input, HMAC_SIZE));
		//rlDebug(rlPcPipeUtility::printPointer("pst_crypt:desti:", dst, HMAC_SIZE));
#endif
	}


	///////////////////////////////////////////////////////////////////////
	// rlPcPipeSendLauncherChallengeTask
	///////////////////////////////////////////////////////////////////////
	rlPcPipeSendLauncherChallengeTask::rlPcPipeSendLauncherChallengeTask()
		: m_State(STATE_START_CHALLENGE)
		, m_challengePtr(NULL)
	{

	}

	bool rlPcPipeSendLauncherChallengeTask::Configure(unsigned char * cp)
	{
		rtry
		{
			rverify(cp, catchall, );
			m_challengePtr = cp;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	netTaskStatus rlPcPipeSendLauncherChallengeTask::OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		if (WasCanceled())
		{
			status = NET_TASKSTATUS_FAILED;
			return status;
		}

		if(m_State == STATE_START_CHALLENGE)
		{
			if (m_WorkItem.Configure(m_challengePtr) && netThreadPool.QueueWork(&m_WorkItem))
			{
				m_State = STATE_PENDING_CHALLENGE;
			}
			else
			{
				netTaskError("Failed in STATE_CREATE_SIGNIN_TRANSFER");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if(m_State == STATE_PENDING_CHALLENGE)
		{
			if (m_WorkItem.Finished())
			{
				m_State = STATE_CHALLENGE_SENT;
			}
		}
		else if(m_State == STATE_CHALLENGE_SENT)
		{
			m_State = STATE_CHALLENGE_SENT;
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		return status;
	};

#endif // #if ENABLE_LAUNCHER_CHALLENGE

} // namespace rage

#endif // RSG_PC

