// 
// rline/rlfacebookcommon.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLFACEBOOK_COMMON_H
#define RLINE_RLFACEBOOK_COMMON_H

#include "file/file_config.h"

namespace rage
{

// Global switch for facebook enabled.
#define RL_FACEBOOK_ENABLED (__RGSC_DLL)

#if RL_FACEBOOK_ENABLED
#define RL_FACEBOOK_SWITCH(on,off)	on
#define RL_FACEBOOK_ONLY(...) __VA_ARGS__
#else
#define RL_FACEBOOK_SWITCH(on,off)	off
#define RL_FACEBOOK_ONLY(...)
#endif // RL_FACEBOOK_ENABLED

// Other facebook defines
#define RL_FACEBOOK_OG_APP_NAMESPACE_LENGTH_MAX	(32)
#define RL_FACEBOOK_OG_ACTION_LENGTH_MAX		(64)
#define RL_FACEBOOK_PERMISSIONS_LENGTH_MAX		(255)
#define RL_FACEBOOK_TOKEN_MAX_LENGTH			(1024)
#define RL_FACEBOOK_LINK_URL_LENGTH_MAX			(2000)
#define RL_FACEBOOK_APP_NAMESPACE_LENGTH_MAX	(32)

enum rlFbGetTokenResult
{
	RL_FB_ERROR_UNEXPECTED_RESULT = -1,              //Unhandled error code

	// FACEBOOK 
	RL_FB_GET_TOKEN_SUCCESS = 0,
	RL_FB_GET_TOKEN_FAILED_UNKNOWN,
	RL_FB_GET_TOKEN_FAILED_NOT_CONFIGURED,	// User has not configured (Go ahead and show the platform dialog)
	RL_FB_GET_TOKEN_FAILED_NOT_ALLOWED,		// User explicitly disabled (User cannot share due to privacy settings. Popping up the Gui won't change that)
	RL_FB_GET_TOKEN_FAILED_USER_CANCELLED,  // USer cancelled. (User either doesn't have facebook credentials or doesn't want to share on facebook)
	// SCS
	RL_FB_ERROR_AUTHENTICATIONFAILED_TICKET,         //Ticket failed authentication
	RL_FB_ERROR_AUTHENTICATIONFAILED_OAUTHEXCEPTION, //User token is invalid or server cannot post for user
	RL_FB_DOESNOTEXIST_ROCKSTARACCOUNT			     //User doesn't have a linked rockstar account
};


}

#endif // RLINE_RLFACEBOOK_COMMON_H
