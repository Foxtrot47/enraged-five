// 
// rline/rlfacebook.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLFACEBOOK_H
#define RLINE_RLFACEBOOK_H

#include "rlfacebookcommon.h"
#include "rline/rlpresence.h"

#if RL_FACEBOOK_ENABLED

namespace rage
{
#define RL_FACEBOOK_GRAPH_API_HOSTNAME			"graph.facebook.com"

// Forward declaration
class rlFacebookGetAppInfoAndPlatformAccesTokenTask;
class rlFacebookShowAppPermissionsUiTask;

//PURPOSE
//  API for Facebook interactions.
//  
//  This class is NOT thread-safe. Only call these functions from the main thread.
//
class rlFacebook
{
	friend rlFacebookGetAppInfoAndPlatformAccesTokenTask;
	friend rlFacebookShowAppPermissionsUiTask;
public:

	static bool Init();
	static void Shutdown();

	//PURPOSE
	//  Make opengraph POST requests to facebook on behalf of the user whose token is obtained through the GetPlatformAccessToken call.
	//  Currently only one user is supported at any one time. 
	//
	//  Function will faill immediately if there's no valid token for the player.
	//
	//PARAMS
	//  action - action (e.g. share,take,like)
	//  objName - object of the action (e.g. screenshot,mission)
	//  objTargetData - permalink to the object that facebook can access (http://....), or for UGC types like gta5mission, gta5missionplaylist,
	//                  or screenshots, pass in the UGC Id of the object.
	//  extraParams - any extra params to pass as part of the facebook action. Format: "key=value&key2=value2&key3=value3". Doesn't need to be url-escaped
	//  outPostId - Pass in a buffer to retrieve the id of the post (Only valid if post is successful)
	//  maxPostIdLength - size of the buffer passed in to retrieve the post ID
	//  status - a status object to monitor the task for success.
	//
	//RETURNS
	//  Whether the task is successfully queued and fired off. If the task fails to queue it's possible that
	//  you haven't obtained a valid access token for the user indicated by the localGamerIndex.
	static bool PostOpenGraph(const int localGamerIndex, const char* action, const char* objName, const char* objTargetData,
						      const char* extraParams, char* outPostId, int maxPostIdLength, netStatus* status);

	//PURPOSE
	//  Loads rlFacebook class with the access token of the gamer specified with localGamerIndex. 
	//  Calling this function will invalidate any previous token. This version will allow the platform
	//  to pop up native dialog boxes to prompt the user for Facebook linking credentials if possible.
	//  Don't call this version if you don't want to interrupt the game flow.
	// 
	//  Function will fail immediately there's another task to retrieve the user token in progress.
	//
	//PARAMS
	//  localGamerIndex - The local gamer index for whom to retrieve a token
	//  fbAppId - The facebook app ID that's linked to the game. (For Xbox360 this param is ignored as the value is confingured catalog-side)
	//  permissions - A comma-sparated list of facebook permissions to request. (e.g. "publish_stream,publish_actions")
	//  workBuffer - a 8kb block of memory for the NP system to initialize its facebook module. Pass in NULL on the 360
	//  workBufferSize - size in bytes of the work buffer. (0 for 360, 8*1024 for NP)
	//  status - status object to monitor the task
	static bool GetPlatformAccessTokenWithGui(int localGamerIndex,  netStatus* status);

	//PURPOSE 
	//  Loads rlFacebook class with the access token of the game specified with localGamerIndex. 
	//  Calling this function will invalidate any previous token. This version will simply return an error if the
	//  user has social sharing disabled or hasn't configured his console to link to a Facebook account.
	//
	//  Function will fail immediately there's another task to retrieve the user token in progress.
	//
	//PARAMS
	//  localGamerIndex - The local gamer index for whom to retrieve a token
	//  fbAppId - The facebook app ID that's linked to the game.
	//  status - status object to monitor the task
	static bool GetPlatformAccessTokenNoGui(const int localGamerIndex, netStatus* status);

	//PURPOSE
	//      Checks if the currently signed in user has granted 'permissions' to our application
	//PARAMS
	//	localGamerIndex - The local gamer index for whom to check app access permissions
	//		havePermissions - pointer to the bool you want to set with whether the user has permissions or not
	static bool HaveAppPermissions(const int localGamerIndex, netStatus* status);

	//PURPOSE
	//  Determines whether rlFacebook owns a valid token for the specified user.
	static bool HasValidToken(const int localGamerIndex);

	//PURPOSE
	static void OnPresenceEvent(const rlPresenceEvent* evt);

	//PURPOSE 
	// Gets various FB app info: the app id, the permissions, the namespace and the link URL
	static bool GetAppInfo(const int localGamerIndex, u64* appIdBuffer, char* appPermissionsBuffer, const int appPermissionsBufferSize,	char* appNamespaceBuffer, 
						   const int appNamespaceBufferSize,  char* scLinkUrlBuffer, const int scLinkUrlBufferSize, netStatus* status);

	// PURPOSE
	//	Clears app permissions
	static void ClearAppPermissions();

	// Kill Switch
	static bool IsKillSwitchEnabled() { return s_KillSwitchEnabled; }
	static void SetKillSwitchEnabled(bool b) { s_KillSwitchEnabled = b; }

private:
    //PURPOSE
    //  Don't allow creating instances.  Users only call static methods.
    rlFacebook();

	static u64  s_UserTokenValidTimeSecs;
	static s32	s_CurrentTokenGamerIndex;
	static bool s_HasAppPermissions;
	static bool s_IsInitialized;

	static char s_UserToken[RL_FACEBOOK_TOKEN_MAX_LENGTH];
	static char s_DesiredPermissions[RL_FACEBOOK_PERMISSIONS_LENGTH_MAX];
	static netStatus s_GetTokenStatus;

	static bool s_KillSwitchEnabled;
};

} //namespace rage

#endif // #if RL_FACEBOOK_ENABLED
#endif  //RLINE_RLFACEBOOK_H
