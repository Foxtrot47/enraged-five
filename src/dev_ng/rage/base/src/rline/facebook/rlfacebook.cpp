// 
// rline/rlFacebook.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlfacebook.h"

#if RL_FACEBOOK_ENABLED

#include "data/rson.h"
#include "diag/seh.h"
#include "net/status.h"
#include "net/task.h"
#include "parser/manager.h"

#include "rline/rlhttptask.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/rlsystemui.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/scauth/rlscauth.h"

#if RSG_NP
#include "rline/rlnp.h"
#endif // if RSG_NP

#if RSG_ORBIS
#define SCE_NP_SNS_FB_ACCESS_TOKEN_PARAM_OPTIONS_SILENT 1
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, facebook)
#undef __rage_channel
#define __rage_channel rline_facebook

// Forward declarations
class netStatus;
class rlHttpTask;

// Static Variables
u64  rlFacebook::s_UserTokenValidTimeSecs;
s32	rlFacebook::s_CurrentTokenGamerIndex;
bool rlFacebook::s_IsInitialized;
bool rlFacebook::s_HasAppPermissions;
char rlFacebook::s_UserToken[RL_FACEBOOK_TOKEN_MAX_LENGTH];
netStatus rlFacebook::s_GetTokenStatus;
static rlPresence::Delegate s_PresenceDlgt;
char rlFacebook::s_DesiredPermissions[RL_FACEBOOK_PERMISSIONS_LENGTH_MAX] = {0};
bool rlFacebook::s_KillSwitchEnabled = false;

// Error handling macros
#define ERROR_CASE(code, codeEx, resCode) \
    if(result.IsError(code, codeEx)) \
    { \
        resultCode = resCode; \
        return; \
    }

#define WARNING_CASE(code, codeEx) \
    if(result.IsError(code, codeEx)) \
    { \
        resultCode = 0; \
        return; \
    }

//////////////////////////////////////////////////////////////////////////
//  rlFacebookOpenGraphTask
//////////////////////////////////////////////////////////////////////////
// Represents a task that gets fired when a Facebook OpenGraph request is made
class rlRosFacebookOpenGraphTask : public rlRosHttpTask
{
public:

	RL_TASK_DECL(rlRosFacebookOpenGraphTask);
	RL_TASK_USE_CHANNEL(rline_facebook)

	rlRosFacebookOpenGraphTask()
	{
	}

	virtual ~rlRosFacebookOpenGraphTask()
	{

	}

	bool Configure(const int localGamerIndex, ORBIS_ONLY(const char* accessToken,) const char* ogAction, const char* ogObjectName,
				   const char* ogObjectTargetData, const char* ogExtraParams, char* outPostIdBuffer, int postIdBufferSize)
	{
		if( rlFacebook::IsKillSwitchEnabled() )
		{
			return false;
		}

		m_PostIdBuffer = outPostIdBuffer;
		m_PostIdBufferSize = postIdBufferSize;

		if(!rlRosHttpTask::Configure(localGamerIndex))
		{
			rlTaskError("Failed to configure base class");
			return false;
		}

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		if(!cred.IsValid())
		{
			rlTaskError("Credentials are invalid");
			return false;
		}

		//Add required params.
		if(!AddStringParameter("ticket", cred.GetTicket(), true) ||
#if RSG_ORBIS
		   !AddStringParameter("fbUserToken", accessToken, true) ||
#endif
		   !AddStringParameter("action", ogAction, true) ||
		   !AddStringParameter("objName", ogObjectName, true) ||
		   !AddStringParameter("objUrl", ogObjectTargetData, true))
		{
			rlTaskError("Failed to add params");
			return false;
		}

		//Add optional params
		if (ogExtraParams != NULL)
		{
			if (!AddStringParameter("extraParams", ogExtraParams, true))
			{
				rlTaskError("Failed to add param: extraParams");
				return false;
			}
		}
		else
		{
			if (!AddStringParameter("extraParams", "", true))
			{
				rlTaskError("Failed to add param: extraParams");
				return false;
			}
		}

		return true;
	}

#if RSG_DURANGO || RSG_PC
	virtual const char* GetServiceMethod() const { return "socialclub.asmx/PostOpenGraphAppToken"; }
#else
	virtual const char* GetServiceMethod() const { return "socialclub.asmx/PostOpenGraphUserToken"; }
#endif
    
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed", "Ticket", RL_FB_ERROR_AUTHENTICATIONFAILED_TICKET);
		ERROR_CASE("AuthenticationFailed", "OAuthException", RL_FB_ERROR_AUTHENTICATIONFAILED_OAUTHEXCEPTION);
		ERROR_CASE("DoesNotExist", "RockstarAccount", RL_FB_DOESNOTEXIST_ROCKSTARACCOUNT);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RL_FB_ERROR_UNEXPECTED_RESULT;
	}

	virtual bool ProcessSuccess(const rlRosResult& /* result */, const parTreeNode* node, int& resultCode)
	{
		rtry
		{
			const parTreeNode* result = node->FindChildWithName("Result");
			rverify(result, catchall, rlTaskError("Missing <Result>"));

			const char* postId = rlHttpTaskHelper::ReadString(result, NULL, NULL);
			rverify(postId, catchall, rlTaskError("Missing <Result> text"));

			rlTaskDebug3("Facebook PostID is: %s", postId);

			safecpy(m_PostIdBuffer, postId, m_PostIdBufferSize);

			resultCode = RLSC_ERROR_NONE;
			return true;
		}
		rcatchall
		{
			resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
			return false;
		}
	}

private:

	int m_PostIdBufferSize;
	char* m_PostIdBuffer;
};

//////////////////////////////////////////////////////////////////////////
//  rlRosFacebookGetAppInfoTask
//////////////////////////////////////////////////////////////////////////
// Represents a task that gets fired to retrieve basic set of info needed to work with Facebook apps
class rlRosFacebookGetAppInfoTask : public rlRosHttpTask
{
public:

	RL_TASK_DECL(rlRosFacebookGetAppInfoTask);
	RL_TASK_USE_CHANNEL(rline_facebook)

	rlRosFacebookGetAppInfoTask()
	{
	}

	virtual ~rlRosFacebookGetAppInfoTask()
	{

	}

	bool Configure(const int localGamerIndex, u64* appId, char* appPermissionsBuffer, const int appPermissionsBufferSize, char* appNamespaceBuffer, 
					const int appNamespaceBufferSize, char* scLinkUrlBuffer, const int scLinkUrlBufferSize)
	{

		if( rlFacebook::IsKillSwitchEnabled() )
		{
			return false;
		}

		rtry
		{
			m_AppId = appId;
			m_PermissionsBuffer = appPermissionsBuffer;
			m_NamespaceBuffer = appNamespaceBuffer;
			m_ScLinkUrlBuffer = scLinkUrlBuffer;

			m_PermissionsBufferSize = appPermissionsBufferSize;
			m_NamespaceBufferSize = appNamespaceBufferSize;
			m_ScLinkUrlBufferSize = scLinkUrlBufferSize;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Failed to configure base class"));

			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add params"));
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

    virtual const char* GetServiceMethod() const 
	{ 
		return "Facebook.asmx/GetAppInfo"; 
	}
    
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed", "Ticket", RL_FB_ERROR_AUTHENTICATIONFAILED_TICKET);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RL_FB_ERROR_UNEXPECTED_RESULT;
	}

	virtual bool ProcessSuccess(const rlRosResult& /* result */, const parTreeNode* node,  int& resultCode)
	{
		rtry
		{
			const parTreeNode* appInfoNode = node->FindChildWithName("FacebookAppInfo");
			rverify(appInfoNode, catchall, rlTaskError("Missing <FacebookAppInfo>"));

			u64 appId = 0;
			bool success = rlHttpTaskHelper::ReadUInt64(appId, appInfoNode, NULL, "Id");
			rverify(success, catchall, rlTaskError("Error parsing <Id> attribute"));

			*m_AppId = appId;

			const char* appNamespace = rlHttpTaskHelper::ReadString(appInfoNode, NULL, "Namespace");
			rverify(appNamespace, catchall, rlTaskError("Missing <Namespace> attribute"));
			safecpy(m_NamespaceBuffer, appNamespace, m_NamespaceBufferSize);

			const char* appPermissions = rlHttpTaskHelper::ReadString(appInfoNode, NULL, "Permissions");
			rverify(appPermissions, catchall, rlTaskError("Missing <Permissions> attribute"));
			safecpy(m_PermissionsBuffer, appPermissions, m_PermissionsBufferSize);

			const char* scLinkUrl = rlHttpTaskHelper::ReadString(appInfoNode, NULL, "ScLinkUrl");
			rverify(scLinkUrl, catchall, rlTaskError("Missing <ScLinkUrl> attribute"));
			safecpy(m_ScLinkUrlBuffer, scLinkUrl, m_ScLinkUrlBufferSize);

			rlTaskDebug3("Facebook AppInfo: id=[%" I64FMT "u], namespace=[%s], permissions=[%s]" , *m_AppId, appNamespace, appPermissions);

			resultCode = RLSC_ERROR_NONE;
			return true;
		}
		rcatchall
		{
			resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
			return false;
		}
	}

private:
	u64* m_AppId;
	char* m_PermissionsBuffer;
	char* m_NamespaceBuffer;
	char* m_ScLinkUrlBuffer;

	// int m_IdBufferSize;
	int m_NamespaceBufferSize;
	int m_PermissionsBufferSize;
	int m_ScLinkUrlBufferSize;
};

//////////////////////////////////////////////////////////////////////////
//  rlFacebookGetAppInfoAndPlatformAccesTokenTask
//////////////////////////////////////////////////////////////////////////

class rlFacebookGetAppInfoAndPlatformAccesTokenTask : public netTask
{
	friend netTask;
public:

    NET_TASK_DECL(rlFacebookGetAppInfoAndPlatformAccesTokenTask);
    NET_TASK_USE_CHANNEL(rline_facebook);

    rlFacebookGetAppInfoAndPlatformAccesTokenTask()
        : m_State(STATE_GET_ACCESS_TOKEN)
    {
    }

	~rlFacebookGetAppInfoAndPlatformAccesTokenTask()
	{
	}

    bool Configure(const int localGamerIndex, const bool showUi, char* tokenBuffer, const u32 tokenBufferSize, u64* expiration, netStatus* myStatus)
    {
		if( rlFacebook::IsKillSwitchEnabled() )
		{
			return false;
		}

		rlFacebook::s_DesiredPermissions[0] = '\0';

		m_FbAppId = 0;
		m_TokenBuffer = tokenBuffer;
		m_TokenBufferSize = tokenBufferSize;
		m_Expiration = expiration;
		m_LocalGamerIndex = localGamerIndex;
		m_MyStatus = myStatus;
		m_ShowUi = showUi;
		m_MyStatus->Reset();
		m_GetAppInfoStatus.Reset();
		m_State = STATE_GET_FACEBOOK_APP_INFO;

        return true;
    }

    virtual void OnCancel()
    {
        netTaskDebug("Canceled while in state %d", m_State);

		// Cancel subtasks
		if (STATE_GETTING_ACCESS_TOKEN)
		{
#if RSG_ORBIS
			netTask::Cancel(m_MyStatus);
#else
			if (rlGetTaskManager())
			{
				rlGetTaskManager()->CancelTask(m_MyStatus);
			}
#endif	
		}
		else if (STATE_GETTING_FACEBOOK_APP_INFO)
		{
			if (rlGetTaskManager())
			{
				rlGetTaskManager()->CancelTask(&m_GetAppInfoStatus);
			}
		}
    }

    virtual netTaskStatus OnUpdate(int* resultCode)
    {
		netTaskStatus status = NET_TASKSTATUS_PENDING;

        switch(m_State)
        {
		case STATE_GET_FACEBOOK_APP_INFO:
			if (WasCanceled())
			{
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				bool success = rlFacebook::GetAppInfo(m_LocalGamerIndex, &m_FbAppId, 
														rlFacebook::s_DesiredPermissions, RL_FACEBOOK_PERMISSIONS_LENGTH_MAX, 
														m_AppNamespace, RL_FACEBOOK_APP_NAMESPACE_LENGTH_MAX, 
														m_ScLinkUrl, RL_FACEBOOK_LINK_URL_LENGTH_MAX,
														&m_GetAppInfoStatus);
				if (!success)
				{
					status = NET_TASKSTATUS_FAILED;
				}
				else
				{
					m_State = STATE_GETTING_FACEBOOK_APP_INFO;
				}
			}
			break;

		case STATE_GETTING_FACEBOOK_APP_INFO:
            if(WasCanceled())
            {
                netTaskDebug("Canceled - bailing...");
				*resultCode = RL_FB_GET_TOKEN_FAILED_USER_CANCELLED;
                status = NET_TASKSTATUS_FAILED;
            }
			else
			{
				// wait for work to be done
				if (!m_GetAppInfoStatus.Pending())
				{
					if (m_GetAppInfoStatus.Succeeded())
					{
						m_State = STATE_GET_ACCESS_TOKEN;
					}
					else
					{
						m_ResultCode = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
						status = NET_TASKSTATUS_FAILED;
					}
				}
			}
            break;
		case STATE_GET_ACCESS_TOKEN:

			if(WasCanceled())
			{
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				netTaskDebug("Getting fb access token");

				bool success = false;
#if RSG_NP
				int opts = m_ShowUi ? 0 : SCE_NP_SNS_FB_ACCESS_TOKEN_PARAM_OPTIONS_SILENT;
				success = g_rlNp.GetBasic().GetFacebookAccessToken(m_FbAppId, rlFacebook::s_DesiredPermissions, opts, m_TokenBuffer, m_Expiration, m_MyStatus);
#else
				success = rlFacebook::HaveAppPermissions(m_LocalGamerIndex, m_MyStatus);
#endif
				if (!success)
				{
					m_ResultCode = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
					status = NET_TASKSTATUS_FAILED;
				}
				else
				{
					m_State = STATE_GETTING_ACCESS_TOKEN;
				}
			}
			break;

        case STATE_GETTING_ACCESS_TOKEN:
            if(WasCanceled())
            {
                netTaskDebug("Canceled - bailing...");
				*resultCode = RL_FB_GET_TOKEN_FAILED_USER_CANCELLED;
                status = NET_TASKSTATUS_FAILED;
            }
			else
			{
				// wait for work to be done
				if (!m_MyStatus->Pending())
				{
					*resultCode = m_MyStatus->GetResultCode();
					if (m_MyStatus->Succeeded())
					{
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}

#if RSG_DURANGO || RSG_PC
					if (!rlFacebook::HasValidToken(m_LocalGamerIndex) && m_ShowUi)
					{
						const char * ticket = rlRos::GetCredentials(m_LocalGamerIndex).GetTicket();
						char ticketBuf[RLROS_MAX_TICKET_SIZE] = {0};
						unsigned inLen = RLROS_MAX_TICKET_SIZE, dstLen = 0;
						netHttpRequest::UrlEncode(&ticketBuf[0], &inLen, ticket, (int)strlen(ticket), &dstLen, NULL);
						safecatf(m_ScLinkUrl, "?userdata=%s", ticketBuf);

						// add scauth client id
						char clientIdBuf[RLROS_MAX_TITLE_NAME_SIZE + RLROS_MAX_PLATFORM_NAME_SIZE + 8] = { 0 };
						if (rlVerify(rlScAuth::FormatClientId(clientIdBuf, sizeof(clientIdBuf))))
						{
							safecatf(m_ScLinkUrl, "&cid=%s", clientIdBuf);
						}

						g_SystemUi.ShowWebBrowser(m_LocalGamerIndex, m_ScLinkUrl);
					}
#endif
				}
			}
            break;
        }
        return status;
    }

	virtual void OnCleanup()
	{
		netTask::OnCleanup();
	}

private:

    enum State
    {
		STATE_GET_FACEBOOK_APP_INFO,
		STATE_GETTING_FACEBOOK_APP_INFO,
        STATE_GET_ACCESS_TOKEN,
        STATE_GETTING_ACCESS_TOKEN
    };

	char m_AppNamespace[RL_FACEBOOK_APP_NAMESPACE_LENGTH_MAX];
	char m_ScLinkUrl[RL_FACEBOOK_LINK_URL_LENGTH_MAX];

	netStatus m_GetAppInfoStatus;

	u64 m_FbAppId;
	u64* m_Expiration;
	netStatus* m_MyStatus;
	s32 m_ResultCode;
	s32 m_LocalGamerIndex;
	u32 m_TokenBufferSize;
	char* m_TokenBuffer;
    State m_State;

	bool m_ShowUi : 1;
};

//////////////////////////////////////////////////////////////////////////
//  rlRosFacebookHasAppPermissions
//////////////////////////////////////////////////////////////////////////
// Represents a task that gets fired to validate app permissions
class rlRosFacebookHasAppPermissionsTask : public rlRosHttpTask
{
private:
	bool* m_HavePermissions;

public:

	RL_TASK_DECL(rlRosFacebookHasAppPermissionsTask);
	RL_TASK_USE_CHANNEL(rline_facebook)

	rlRosFacebookHasAppPermissionsTask() 
		: m_HavePermissions(NULL)
	{

	}

	virtual ~rlRosFacebookHasAppPermissionsTask() {}

	bool Configure(const int localGamerIndex, const char * permissions, bool * havePermissions)
	{
		if( rlFacebook::IsKillSwitchEnabled() )
		{
			return false;
		}

		bool success = false;

		rtry
		{
			rverify(permissions, catchall, );
			rverify(permissions[0] != '\0', catchall, rlTaskError("Permissions have not been retrieved from GetAppInfo"));
			rverify(havePermissions, catchall, );

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add params"));
			rverify(AddStringParameter("requiredPermissions", permissions, true), catchall, );

			m_HavePermissions = havePermissions;


			success = true;
		}
		rcatchall
		{
			success = false;
		}

		return success;
	}

	virtual const char* GetServiceMethod() const { return "Facebook.asmx/CheckUserFacebookPermissions"; }

	virtual void ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed",	"Ticket",			RL_FB_ERROR_AUTHENTICATIONFAILED_TICKET);
		ERROR_CASE("AuthenticationFailed",	"OAuthException",	RL_FB_ERROR_AUTHENTICATIONFAILED_OAUTHEXCEPTION); // treat these 2 responses the same
		ERROR_CASE("NotAllowed",			"Permission",		RL_FB_ERROR_AUTHENTICATIONFAILED_OAUTHEXCEPTION); // treat these 2 responses the same
		ERROR_CASE("DoesNotExist",			NULL,				RL_FB_DOESNOTEXIST_ROCKSTARACCOUNT);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RL_FB_ERROR_UNEXPECTED_RESULT;
	}

	virtual bool ProcessSuccess(const rlRosResult& /* result */, const parTreeNode* node, int& resultCode)
	{
		rtry
		{
			rverify(node, catchall, );
			resultCode = RLSC_ERROR_NONE;

			const parTreeNode* result = node->FindChildWithName("Result");
			rverify(result, catchall, rlTaskError("Missing <Result>"));

			bool appPermissions;
			rverify(rlHttpTaskHelper::ReadBool(appPermissions, result, NULL, NULL), catchall, );
			rlTaskDebug3("Able to post to facebook is: %s", appPermissions ? "true" : "false");

			resultCode = RLSC_ERROR_NONE;

			if (!WasCanceled())
			{
				*m_HavePermissions = appPermissions;
			}

			return true;
		}
		rcatchall
		{
			resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
			return false;
		}
	}

private:

};


//////////////////////////////////////////////////////////////////////////
//  rlFacebook
//////////////////////////////////////////////////////////////////////////
bool rlFacebook::Init()
{
	if(rlVerify(!s_IsInitialized))
	{
		s_UserToken[0] = '\0';
		s_GetTokenStatus.Reset();

		s_UserTokenValidTimeSecs = 0;
		s_IsInitialized = true;

		if (!s_PresenceDlgt.IsBound())
		{
			s_PresenceDlgt.Bind(rlFacebook::OnPresenceEvent);
			rlPresence::AddDelegate(&s_PresenceDlgt);
		}

		return true;
	}
	else
	{
		return false;
	}
}

void 
rlFacebook::Shutdown()
{
	s_IsInitialized = false;
	if (s_PresenceDlgt.IsBound())
	{
		rlPresence::RemoveDelegate(&s_PresenceDlgt);
	}
}

bool
rlFacebook::PostOpenGraph(const int localGamerIndex, const char* action, const char* objName, const char* objTargetData,
						  const char* extraParams, char* outPostId, int maxPostIdLength, netStatus* status)
{
	if (!HasValidToken(localGamerIndex))
	{
		rlError("Cannot post to open graph without first obtaining a valid token for the user.");
		return false;
	}

	if( rlFacebook::IsKillSwitchEnabled() )
	{
		return false;
	}

	bool success = false;
	rlRosFacebookOpenGraphTask* task = NULL;
	rtry
	{
		task = rlGetTaskManager()->CreateTask<rlRosFacebookOpenGraphTask>();
		rverify(task,catchall,);
#if !RSG_DURANGO && !RSG_PC
		rverify(rlTaskBase::Configure(task, localGamerIndex, s_UserToken, action, objName, objTargetData, extraParams, outPostId, maxPostIdLength, status), catchall,);
#else
		rverify(rlTaskBase::Configure(task, localGamerIndex, action, objName, objTargetData, extraParams, outPostId, maxPostIdLength, status), catchall,);
#endif
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}
	return success;
}

bool rlFacebook::GetAppInfo(const int localGamerIndex, u64* appId, char* appPermissionsBuffer, const int appPermissionsBufferSize, char* appNamespaceBuffer, 
							const int appNamespaceBufferSize, char* scLinkUrlBuffer, const int scLinkUrlBufferSize, netStatus* status)
{
	if( rlFacebook::IsKillSwitchEnabled() )
	{
		return false;
	}

	bool success = false;
	rlRosFacebookGetAppInfoTask* task = NULL;
	s_DesiredPermissions[0] = '\0';

	rtry
	{
		task = rlGetTaskManager()->CreateTask<rlRosFacebookGetAppInfoTask>();
		rverify(task,catchall,);
		rverify(rlTaskBase::Configure(task,
			                          localGamerIndex,
									  appId,
									  appPermissionsBuffer,
									  appPermissionsBufferSize,
									  appNamespaceBuffer,
									  appNamespaceBufferSize,
									  scLinkUrlBuffer,
									  scLinkUrlBufferSize,
									  status),
 									  catchall,);
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}
	return success;
}


// PURPOSE
//	Clears app permissions
void rlFacebook::ClearAppPermissions()
{
	s_UserToken[0] = '\0';
	s_DesiredPermissions[0] = '\0';
	s_GetTokenStatus.Reset();
	s_UserTokenValidTimeSecs = 0;

	if (s_GetTokenStatus.Pending())
	{
		netTask::Cancel(&s_GetTokenStatus);
	}

	s_HasAppPermissions = false;
}


bool 
rlFacebook::GetPlatformAccessTokenWithGui(const int localGamerIndex, netStatus* status)
{
	if (!rlVerify(s_IsInitialized && !s_GetTokenStatus.Pending()))
	{
		rlError("Cannot obtain an access token if the system is not initialized or there's a task pending to retrieve a token already.");
		return false;
	}

	if( rlFacebook::IsKillSwitchEnabled() )
	{
		return false;
	}

	s_CurrentTokenGamerIndex = localGamerIndex;
	rlFacebookGetAppInfoAndPlatformAccesTokenTask* task;
	s_GetTokenStatus.Reset();

	rtry
	{
		// "status" gets passed in here because we want our client to monitor the outer status of the task
		rverify(netTask::Create(&task, status), catchall, rlError("Error allocating rlFacebookGetPlatformAccesTokenTask"));

		rverify(task->Configure(localGamerIndex, true, s_UserToken, RL_FACEBOOK_TOKEN_MAX_LENGTH, &s_UserTokenValidTimeSecs, &s_GetTokenStatus), 
				catchall, rlError("Error configuring rlFacebookGetPlatformAccesTokenTask"));

		rverify(netTask::Run(task), catchall, rlError("Error scheduling rlFacebookGetPlatformAccesTokenTask"));
		return true;
	}
	rcatchall
	{
		netTask::Destroy(task);
		return false;
	}
}

bool rlFacebook::GetPlatformAccessTokenNoGui(int localGamerIndex, netStatus* status)
{
	if (!rlVerify(s_IsInitialized && !s_GetTokenStatus.Pending()))
	{
		rlError("Cannot obtain an access token if the system is not initialized or there's a task pending to retrieve a token already.");
		return false;
	}

	if( rlFacebook::IsKillSwitchEnabled() )
	{
		return false;
	}

	s_GetTokenStatus.Reset();
	s_CurrentTokenGamerIndex = localGamerIndex;
	rlFacebookGetAppInfoAndPlatformAccesTokenTask* task;

	rtry
	{
		rverify(netTask::Create(&task, status), catchall, rlError("Error allocating rlFacebookGetPlatformAccesTokenTask"));
		rverify(task->Configure(localGamerIndex, false, s_UserToken, RL_FACEBOOK_TOKEN_MAX_LENGTH, &s_UserTokenValidTimeSecs, &s_GetTokenStatus), catchall, 
				rlError("Error configuring rlFacebookGetPlatformAccesTokenTask"));
		rverify(netTask::Run(task), catchall, rlError("Error scheduling rlFacebookGetPlatformAccesTokenTask"));
		return true;
	}
	rcatchall
	{
		netTask::Destroy(task);
		return false;
	}
}

bool rlFacebook::HaveAppPermissions(const int localGamerIndex, netStatus* status)
{
	bool success = false;
	rlRosFacebookHasAppPermissionsTask* task = NULL;

	if( rlFacebook::IsKillSwitchEnabled() )
	{
		return false;
	}

	rtry
	{
		task = rlGetTaskManager()->CreateTask<rlRosFacebookHasAppPermissionsTask>();
		rverify(task,catchall,);
		rverify(rlTaskBase::Configure(task, localGamerIndex, s_DesiredPermissions, &s_HasAppPermissions, status), catchall,);
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}
	return success;
}

bool rlFacebook::HasValidToken(const int localGamerIndex)
{
#if RSG_DURANGO || RSG_PC
	return localGamerIndex == s_CurrentTokenGamerIndex && s_HasAppPermissions && s_IsInitialized;
#else
	return localGamerIndex == s_CurrentTokenGamerIndex && s_IsInitialized && s_GetTokenStatus.Succeeded();
#endif
}

void rlFacebook::OnPresenceEvent(const rlPresenceEvent* evt)
{
	if(PRESENCE_EVENT_SC_MESSAGE == evt->GetId())
	{
		const rlScPresenceMessage& msgBuf = evt->m_ScMessage->m_Message;
		if(msgBuf.IsA<rlScPresenceAccountLinkResult>())
		{
			rlScPresenceAccountLinkResult msg;
			if (rlVerifyf(msg.Import(msgBuf), "rlFacebook::OnPresenceEvent - Error importing '%s'", msg.Name()))
			{
				if (msg.m_Success && stricmp(msg.m_Network, "facebook") == 0)
				{
					// SUCCESS
					s_HasAppPermissions = true;
					return;
				}
			}
		}
	}
}

}   //namespace rage

#endif // #if RL_FACEBOOK_ENABLED