// 
// rline/rlnpwebapiworkitem.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
// Sony's new WebAPI has a variety of blocking calls - these calls are to be wrapped in
// a sysThreadPool::WorkItem and to be called by rlNpWebApiNetTasks.
//
// Documentation: User_Profile_WebAPI-Reference_e.pdf, Session_Invitation-Overview_e.pdf, Session_Invitation_WebAPI-Reference_e.pdf
// Default Location: C:\Program Files (x86)\SCE\ORBIS\documentation\pdf\en\SDK_doc\Network
// 
#if RSG_ORBIS
#include <sdk_version.h>
#ifndef RLINE_RLWEBAPIWORKITEM_H
#define RLINE_RLWEBAPIWORKITEM_H

#include "atl/string.h"
#include "data/growbuffer.h"
#include "data/rson.h"
#include "file/limits.h"
#include "net/status.h"
#include "rline/rl.h"
#include "rline/rlnpcommon.h"
#include "rline/rlfriend.h"
#include "rline/rlsessioninfo.h"
#include "rline/rlfriendsreader.h"
#include "system/threadpool.h"

#if RSG_BANK
#include "rline/rlnpfaulttolerance.h"
#endif

#include <invitation_dialog.h>
#include <np.h>

// Cap the max sessions/invitations we receive
#define NP_WEB_API_MAX_INVITATIONS (RL_MAX_WEB_API_INVITES)
#define NP_WEB_API_MAX_SESSIONS 10
#define NP_WEB_API_MAX_MAPPED_IDS 1000

// Content limits
#define MAPPER_MAX_LENGTH 4096

const int API_PATH_MAXIMUM_SIZE = 1024; // Must contain at least 50 online Ids

namespace rage
{

// Fwd Declarations
class rlSession;
class rlNpWebApi;

// Some of the NpWebAPI initialization functions that specify a service
// are blocking network calls and need to be initialized separately
class rlNpWebApiRegisterCallbacksWorkItem : public sysThreadPool::WorkItem
{
public:
	rlNpWebApiRegisterCallbacksWorkItem();

	void Configure(const int localGamerIndex, int libCtxId, int userCtxId, bool bUnregister);
	virtual void DoWork();

protected:
	bool RegisterPresenceGameTitleInfoCB();
	bool RegisterPresenceGameStatusCB();
	bool RegisterPresenceGameDataCB();
	bool RegisterGameInviteCB();

	void UnregisterPresenceGameTitleInfoCB();
	void UnregisterPresenceGameStatusCB();
	void UnregisterPresenceGameDataCB();
	void UnregisterGameInviteCB();

	int m_LibCtxId;
	int m_UserCtxId;
	int m_LocalGamerIndex;
	bool m_bUnRegister;

	static int sm_GameTitleFilterIds[RL_MAX_LOCAL_GAMERS];
	static int sm_GameTitleCallbackIds[RL_MAX_LOCAL_GAMERS];

	static int sm_GameStatusFilterIds[RL_MAX_LOCAL_GAMERS];
	static int sm_GameStatusCallbackIds[RL_MAX_LOCAL_GAMERS];

	static int sm_GameDataFilterIds[RL_MAX_LOCAL_GAMERS];
	static int sm_GameDataCallbackIds[RL_MAX_LOCAL_GAMERS];

	static int sm_InvitationFilterIds[RL_MAX_LOCAL_GAMERS];
	static int sm_InvitationCallbackIds[RL_MAX_LOCAL_GAMERS];

	netStatus m_Status;
};

// Base class for WebAPIWorkItems. Contains a basic structure for creating a web API call
// and reading the results. GetApiGroup and SetupApiPath determine which WebAPI is to be used.
class rlNpWebApiWorkItem : public sysThreadPool::WorkItem
{
public:
    rlNpWebApiWorkItem();
    virtual ~rlNpWebApiWorkItem();

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId);
	virtual void DoWork();

	netStatus* GetStatus() { return &m_Status; }

	virtual void Cancel() {}
	virtual bool Pending() { return m_Status.Pending(); }
	virtual bool Succeeded() { return m_Status.Succeeded(); }
	virtual bool Failed() { return m_Status.Failed(); }
	virtual bool ProcessSuccess() { return true; };

	OUTPUT_ONLY(virtual const char* GetWorkItemName() const = 0);

	virtual void OnCanceled() 
	{ 
		if(m_Status.Pending())
			m_Status.SetCanceled(); 
	}

protected:
	virtual const char* GetApiGroup() = 0;
	virtual void SetupApiPath(rlSceNpAccountId accountId) = 0;
	virtual const char* GetRequestBody() { return NULL; }
	virtual size_t GetRequestLength() { return 0; }
	virtual bool ReadResponseData();

	virtual void OnComplete() { m_Status.SetSucceeded(); }
	virtual void OnFailure() { m_Status.SetFailed(); }

	const char* GetBuffer() { return (const char*) m_ReadBuffer.GetBuffer(); }
	void CaptureOutput();
	const char * GetIdentifierName();

#if !__NO_OUTPUT
	const char * GetMethod();
#endif

	SceNpWebApiHttpMethod m_HttpMethod;
	SceNpWebApiContentParameter m_Content;
	bool m_bAddContent;

	int m_HttpStatusCode;
	int m_WebApiUserCtxId;
	int64_t m_WebApiRequestId;
	char m_ApiPath[API_PATH_MAXIMUM_SIZE];

	datGrowBuffer m_ReadBuffer;
	size_t m_ReadSize;
	netStatus m_Status;
	int m_LocalGamerIndex;

	enum WebApiIdentifier
	{
		DELETE_GAME_DATA,
		DELETE_GAME_STATUS,
		DELETE_MEMBER,
		DELETE_SESSION,
		GET_BLOCKLIST,
		GET_CATALOG,
		GET_PRODUCT_INFO,
		GET_ENTITLEMENTS,
		PUT_ENTITLEMENT_COUNT,
		POST_ACTIVITYFEED_STORY,
		POST_ACTIVITYFEED_PLAYEDWITH,
		GET_ACTIVITYFEED_USER,
		GET_FRIENDSLIST,
		GET_INVITATION_DATA,
		GET_INVITATIONLIST,
		GET_INVITATION,
		GET_PROFILE,
		GET_PROFILES,
		GET_PRESENCE,
		GET_SESSION_DATA,
		GET_CHANGEABLE_SESSION_DATA,
		GET_SESSONLIST,
		GET_SESSION,
		POST_INVITATION,
		POST_MEMBER,
		POST_SESSION,
		PUT_CHANGEABLE_SESSION_DATA,
		PUT_GAME_DATA,
		PUT_GAME_DATABLOB,
		PUT_GAME_STATUS,
		PUT_INVITATION,
		PUT_SESSION_IMAGE,
		PUT_SESSION,
		GET_ACCOUNTID,
		GET_ACCOUNTIDS,
		GET_ONLINEID,
		GET_ONLINEIDS,
	};

	WebApiIdentifier m_Identifier;

#if RSG_BANK
	rlNpWebApiFaultToleranceRule* m_FaultToleranceRule;
#endif
};

#if !__NO_OUTPUT
#define RLNP_WORK_ITEM_NAME(name) const char* GetWorkItemName() const override {return #name;}
#else
#define RLNP_WORK_ITEM_NAME(name)
#endif

// Get user profile
// GET UserProfileBaseUrl/v1/users/onlineId/profile
class rlNpGetProfileWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetProfileWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
};

// Get user profiles
// GET UserProfileBaseUrl/v1/profiles
class rlNpGetProfilesWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetProfilesWorkItem);

	enum ProfileFilters
	{
		PF_DEFAULT = 0x01,
		PF_USER = 0x02,
		PF_REGION = 0x04,
		PF_NPID = 0x08,
		PF_AVATAR = 0x10,
		PF_ABOUTME = 0x20,
		PF_LANGUAGES = 0x40,
		PF_PERSONAL = 0x80
	};

    enum Flags
    {
        FLAGS_DEFAULT         = 0,
        FLAG_FRIENDS_ONLY     = 0x1,
        FLAG_ALLOW_REAL_NAMES = 0x2,
    };

	static const int MAX_GETPROFILES_ONLINEIDS = 100;
	static const int MAX_GETPROFILES_ONLINEIDS_BATCH = 50;

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlGamerHandle* gamerHandles, int startIndex,
						   int numGamerHandles, rlDisplayName* displayNames, rlGamertag* gamerTags, int filters = PF_DEFAULT, unsigned flags = FLAGS_DEFAULT);

	virtual bool ProcessSuccess();
	virtual void DoWork();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:

	int m_Filter;
	unsigned m_NumGamerHandles;
	unsigned m_NumValidHandles;
    unsigned m_UseAccountIds;
	rlGamerHandle m_GamerHandles[MAX_GETPROFILES_ONLINEIDS];
	rlDisplayName* m_DisplayNames;
	rlGamertag* m_GamerTags;
	unsigned m_Flags;
	int m_StartIndex;
};

// Get friends list
// GET UserProfileBaseUrl/v1/users/onlineId/friendList
class rlNpGetFriendsListWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetFriendsListWorkItem);

	rlNpGetFriendsListWorkItem();

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, unsigned* totalFriendsPtr, int startIndex,
				   rlFriend* friends, unsigned* numFriends, int maxFriends, int friendFlags);

	virtual bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlFriend* m_FriendsPtr;
	unsigned* m_NumFriends;
	unsigned* m_TotalFriendsPtr;
	int m_FirstFriendIndex;
	int m_MaxFriends;

	int m_FriendFlags;
};

// Delete arbitrarily-defined data for the InGame presence from presence information
// DELETE UserProfileBaseUrl/v1/users/onlineId/presence/gameData
class rlNpDeleteGameDataWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpDeleteGameDataWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
};

// Delete status regarding gameplay from the presence information
// DELETE UserProfileBaseUrl/v1/users/onlineId/presence/gameStatus
class rlNpDeleteGameStatusWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpDeleteGameStatusWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
};

// Get presence information
// GET UseRProfileBaseUrl/v1/users/onlineId/presence
class rlNpGetPresenceWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetPresenceWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpAccountId targetAccountId, const rlSceNpOnlineId& targetOnlineId, bool bIsLocalUserPresence);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	bool m_bIsLocalUserPresence;
	rlSceNpAccountId m_TargetAccountId;
	rlSceNpOnlineId m_TargetOnlineId;
};

// Set arbitrarily-defined data for the InGame presence in presence information - using binary blob
// PUT UserProfileBaseUrl/v1/users/onlineId/presence/gameData
class rlNpPutGameDataBlobWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPutGameDataBlobWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, void* blob, int blobLength);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return rw.ToString(); }
	virtual size_t GetRequestLength() { return rw.Length(); }

private:
	RsonWriter rw;
	char rsonBuf[2048];
};

// Set the status regarding gameplay in the presence information
// PUT UserProfileBaseUrl/v1/users/onlineId/presence/gameStatus
class rlNpPutGameStatusWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPutGameStatusWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * gameStatus);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return rw.ToString(); }
	virtual size_t GetRequestLength() { return rw.Length(); }

private:
	RsonWriter rw;
	char rsonBuf[2048];
};

// Set statuses related to gameplay and arbitrarily-defined data for in-game presence in presence information
// PUT UserProfileBaseUrl/v1/users/onlineId/presence/inGamePresence
class rlNpPutGamePresenceWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPutGamePresenceWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * gameStatus, const void* gameData, int gameDataLength);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return rw.ToString(); }
	virtual size_t GetRequestLength() { return rw.Length(); }

private:
	RsonWriter rw;
	char rsonBuf[2048];
};

// Get block list
// GET UserProfileBaseUrl/v1/users/onlineId/blockList
class rlNpGetBlocklistWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetBlocklistWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
};

struct rlNpWebApiSessionDetail
{
	rlNpWebApiSessionDetail()
	{
		memset(&info, 0, sizeof(info));
		memset(&sessionId, 0, sizeof(sessionId));
		sessionCreator = RL_INVALID_NP_ACCOUNT_ID;

		sessionType[0] = '\0';
		npTitleType[0] = '\0';
		sessionPrivacy[0] = '\0';
		sessionName[0] = '\0';
		sessionStatus[0] = '\0';
		joinableFlag = false;
	}

	rlSessionInfo info;
	rlSceNpSessionId sessionId;
	rlSceNpAccountId sessionCreator;
	char sessionType[16];
	char npTitleType[16];
	char sessionPrivacy[16];
	char sessionName[RL_MAX_SESSION_NAME_BUF_SIZE];
	char sessionStatus[RL_MAX_SESSION_STATUS_BUF_SIZE];
	int sessionMaxUser;
	bool joinableFlag;
};

struct rlNpWebApiInvitationDetail
{
	rlNpWebApiInvitationDetail()
	{
		accountId = RL_INVALID_NP_ACCOUNT_ID;
		onlineId.Clear();
		invitationId[0] = '\0';
		receivedDate[0] = '\0';
		receivedTimestamp = 0;
		expired = false;
		used = false;
		message[0] = '\0';
		memset(&session, 0, sizeof(session));
	}

	rlSceNpAccountId accountId;
	rlSceNpOnlineId onlineId;
	char invitationId[60];
	char receivedDate[24];
	u64 receivedTimestamp; 
	bool expired;
	bool used;
	char message[SCE_INVITATION_DIALOG_MAX_USER_MSG_SIZE];
	rlNpWebApiSessionDetail session;

	netStatus m_DetailStatus;
	netStatus m_DataStatus;
};

struct rlNpWebApiPostInvitationParam
{
	rlNpWebApiPostInvitationParam()
	{
		m_SessionId = nullptr;
		m_SessionInfo = nullptr;
		m_AccountIds = nullptr;
		m_NumAccountIds = 0;
		m_Message = nullptr;
	}

	const rlSceNpSessionId* m_SessionId;
	const rlSessionInfo* m_SessionInfo;
    const char* m_Message;
    
    const rlSceNpAccountId* m_AccountIds;
	unsigned m_NumAccountIds;

    const rlSceNpOnlineId* m_OnlineIds;
    unsigned m_NumOnlineIds;
};

// Get information on a received invitation
// GET SessionInvitationBaseUrl/v1/users/onlineId/invitations/invitationId
class rlNpGetInvitationWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetInvitationWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* detail);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlNpWebApiInvitationDetail* m_InvitationPtr;
};

// Get invitation data for a received invitation
// GET SessionInvitationBaseUrl/v1/users/onlineId/invitations/invitationData
class rlNpGetInvitationDataWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetInvitationDataWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* invitation);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlNpWebApiInvitationDetail* m_InvitationPtr;
};

// Get list of received invitations
// GET SessionInvitationBaseUrl/v1/users/onlineId/invitations/invitations
class rlNpGetInvitationListWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetInvitationListWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, int maxNumInvitations);
	bool ProcessSuccess();

	void GetInvitations(rlNpWebApiInvitationDetail* details, int * numInvitations);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlNpWebApiInvitationDetail m_Invitations[NP_WEB_API_MAX_INVITATIONS];
	int m_MaxInvitations;
	int m_NumInvitations;
};

// Set data for received invitation to use
// PUT SessionInvitationBaseUrl/v1/users/onlineId/invitations/invitationId
class rlNpPutInvitationWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPutInvitationWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * invitationId, bool used);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return rw.ToString(); }
	virtual size_t GetRequestLength() { return rw.Length(); }

private:
	RsonWriter rw;
	char rsonBuf[2048];
	char m_InvitationId[65];
};

// Set data for received invitation to use
// PUT SessionInvitationBaseUrl/v1/sessions/sessionId/invitations
class rlNpPostInvitationWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPostInvitationWorkItem);

	rlNpPostInvitationWorkItem();
	~rlNpPostInvitationWorkItem();
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiPostInvitationParam* param);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return m_Body; }
	virtual size_t GetRequestLength() { return m_Content.contentLength; }

private:
	RsonWriter rw;
	char * m_Body;
	rlSceNpSessionId m_SessionId;
    bool m_UseAccountIds; 
};

// Leave session
// DELETE SessionInvitationBaseUrl/v1/sessions/sessionId/members/onlineId
class rlNpDeleteMemberWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpDeleteMemberWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId, rlSceNpAccountId targetId);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlSceNpSessionId m_SessionId;
	rlSceNpAccountId m_TargetId;
};

// Delete session
// DELETE SessionInvitationBaseUrl/v1/sessions/sessionId
class rlNpDeleteSessionWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpDeleteSessionWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlSceNpSessionId m_SessionId;
};

// Get session information
// GET SessionInvitationBaseUrl/v1/sessions/sessionId
class rlNpGetSessionWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetSessionWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlSceNpSessionId m_SessionId;
	rlNpWebApiSessionDetail m_Session;
};

// Get session data for session
// GET SessionInvitationBaseUrl/v1/sessions/sessionData
class rlNpGetSessionDataWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetSessionDataWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId);
	bool ProcessSuccess();
	bool GetSessionData(rlSessionInfo* sessionInfo);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlSceNpSessionId m_SessionId;
	rlSessionInfo m_SessionInfo;
};

// Get list of sessions where user is participating
// GET SessionInvitationBaseUrl/v1/users/onlineId/sessions
class rlNpGetSessionListWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetSessionListWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, int maxNumSessions);
	bool ProcessSuccess();

	void GetSessions(rlNpWebApiSessionDetail* sessions, int * numSessions);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlNpWebApiSessionDetail m_Sessions[NP_WEB_API_MAX_SESSIONS];
	int m_MaxSessions;
	int m_NumSessions;
};

// Join session
// POST SessionInvitationBaseUrl/v1/sessions/sessionId/members
class rlNpPostMemberWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPostMemberWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId);
	
protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlSceNpSessionId m_SessionId;
};

// Create session
// POST SessionInvitationBaseUrl/v1/sessions
class rlNpPostSessionWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPostSessionWorkItem);

	rlNpPostSessionWorkItem();
	~rlNpPostSessionWorkItem();
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * sessionImagePath, rlSession* session);
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, uint8_t* imageData, int imageSize, rlSession* session);
	bool ProcessSuccess();
	rlSceNpSessionId GetSessionId() { return m_SessionId; }

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return m_Body; }
	virtual size_t GetRequestLength() { return m_Length; }

private:
	char* m_Body;
	int m_Length;
	rlSceNpSessionId m_SessionId;
	rlSession* m_SessionPtr;
};

// Update session information
// PUT SessionInvitationBaseUrl/v1/sessions/sessionId
class rlNpPutSessionWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPutSessionWorkItem);

	rlNpPutSessionWorkItem();
	~rlNpPutSessionWorkItem();

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId, const char* sessionName, const char* sessionStatus);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return rw.ToString(); }
	virtual size_t GetRequestLength() { return rw.Length(); }

private:
	RsonWriter rw;
	char rsonBuf[2048];
	rlSceNpSessionId m_SessionId;
};

// Update changeable session information
// PUT SessionInvitationBaseUrl/v1/sessions/sessionId/changeableSessionData
class rlNpPutChangeableDataWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPutChangeableDataWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSession* session);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return m_Body; }
	virtual size_t GetRequestLength() { return m_Length; }

private:
	char m_Body[rlSessionInfo::TO_STRING_BUFFER_SIZE];
	unsigned m_Length;
	rlSceNpSessionId m_SessionId;
};

// Get changeable session data for session
// GET SessionInvitationBaseUrl/v1/sessions/changeableSessionData
class rlNpGetChangeableDataWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetChangeableDataWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId);
	bool ProcessSuccess();
	bool GetChangeableSessionData(rlSessionInfo* sessionInfo);
	
protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

private:
	rlSceNpSessionId m_SessionId;
	rlSessionInfo m_SessionInfo;
	rlGamerHandle m_SessionHost;
};

// Update session information image
// PUT SessionInvitationBaseUrl/v1/sessions/sessionId/sessionImage
class rlNpPutSessionImageWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPutSessionImageWorkItem);

	rlNpPutSessionImageWorkItem();
	~rlNpPutSessionImageWorkItem();
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId, const u8* sessionImage, const u32 sessionImageLength);
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId, const char* imagePath);

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody();
	virtual size_t GetRequestLength() { return m_Length; }

private:
	char m_ImagePath[RAGE_MAX_PATH];
	const u8* m_SessionImage;
	u32 m_SessionImageLength;
	u8* m_SessionImgBuf;
	int m_Length;
	rlSceNpSessionId m_SessionId;
};


// Get catalog detail
// GET commerce/v1/users/me/container
class rlNpGetCatalogDetail
{
public:
	rlNpGetCatalogDetail();
	datGrowBuffer &GetBuffer() { return m_DataBuffer; }

private:
	datGrowBuffer m_DataBuffer;
};

class rlNpGetCatalogWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetCatalogWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetCatalogDetail *detail);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	rlNpGetCatalogDetail* m_Detail;
};

// Get product detail
// GET commerce/v1/users/me/container/[PRODUCTID]
class rlNpGetProductDetail
{
public:
	rlNpGetProductDetail();

	datGrowBuffer &GetBuffer() { return m_DataBuffer; }

	void Reset();

	bool AddRequestedProduct( const char* productId );
	int GetNumRequestedProducts() { return m_NumRequestedProducts; }

	const char* GetRequestedProductId(int requestedProductArrayIndex);

	enum 
	{
		MAX_REQUESTABLE_PRODUCTS = 64
	};

private:
	int m_NumRequestedProducts;
	atString m_RequestedProducts[MAX_REQUESTABLE_PRODUCTS]; 
	datGrowBuffer m_DataBuffer;
};

class rlNpGetProductInfoWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetProductInfoWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetProductDetail *detail);
	bool ProcessSuccess();

	datGrowBuffer &GetBuffer() { return m_DataBuffer; }

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	rlNpGetProductDetail* m_Detail;
	datGrowBuffer m_DataBuffer;
};


// Set entitlement used count (for consumption)
// PUT entitlement/users/me/entitlements/[PRODUCTID]

class rlNpSetEntitlementsDetail
{
public:
	rlNpSetEntitlementsDetail();


	int GetNewUseLimit() { return m_NewUseLimit; }
	void SetNewUseLimit(int newUseLimit) { m_NewUseLimit = newUseLimit; }


	bool DidCallSucceed() {return m_CallSucceeded;}
	void SetCallSuccessFlag(bool succeeded) { m_CallSucceeded = succeeded; }
private:

	bool m_CallSucceeded;
	int m_NewUseLimit;

};

class rlNpSetEntitlementCountWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpSetEntitlementCountWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpSetEntitlementsDetail* detail, const char* entitlementId, int amount);
	bool ProcessSuccess();

	enum 
	{
		MAX_ENTITLEMENT_ID_LEN = 32,
		SET_ENTITLEMENT_RSON_BUFFER_LENGTH = 2048
	};

	virtual const char* GetRequestBody() { return rw.ToString(); }
	virtual size_t GetRequestLength() { return rw.Length(); }

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	rlNpSetEntitlementsDetail* m_Detail;
	atString m_EntitlementId;
	int m_AmountToConsume;

	RsonWriter rw;
	char rsonBuf[SET_ENTITLEMENT_RSON_BUFFER_LENGTH];
};

// Get entitlements
// GET entitlement/users/me/entitlements
class rlNpGetEntitlementsDetail
{
public:
	rlNpGetEntitlementsDetail();
	datGrowBuffer &GetBuffer() { return m_DataBuffer; }

	void Reset();

private:
	datGrowBuffer m_DataBuffer;
};

class rlNpGetEntitlementsWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetEntitlementsWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetEntitlementsDetail *detail);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	rlNpGetEntitlementsDetail* m_Detail;
};

class rlNpActivityFeedStory
{
public:
	enum
	{
		aspectRatioStringLength = 8,
		maxSubStringLength = 32,
		maxSubValueLength = 16,
		maxNoofSubStrings = 3,
		maxNoofSubValues = 3,
		maxTagStringLength = 20,
		maxCondensedStringLength = 100,
		maxStringLength = 256
	};

	struct ContentType
	{
		enum Enum
		{
			smallImage = 0,
			largeImage,
			thumbnail,
			video,
			customCaption,
			URL,
			startAction,
			store,

			Noof
		};
	};

	struct ActionTab
	{
		enum Enum
		{
			URL,
			startAction,
			store,

			Noof
		};
	};

	rlNpActivityFeedStory() { Reset(); }

	unsigned int GetPostType() const { return m_postTypeEnum; };
	bool HasCaptions() const { return m_hasCaptions; };

	const char* GetCaption(sysLanguage language, bool condensed) const { return condensed ? m_condensedCaption[language] : m_caption[language]; }
	const char* GetContent(ContentType::Enum contentType) const { return m_content[contentType]; }
	int GetNoofSubStrings() const { return m_noofSubStrings; };
	void BuildAllCaptions();

	const char* GetTag(sysLanguage language, ContentType::Enum contentType) const;
	const char* GetThumbnail(ContentType::Enum contentType) const;
	const char* GetImageAspectRatio() const;

	void SetImageAspectRatio(const char* aspectRatio);

	bool HasContent(ContentType::Enum contentType) const;
	bool HasSubStrings() const { return m_noofSubStrings + m_noofSubValues; };

	void StartContent(unsigned int postType);
	void AddCaptions(sysLanguage language, const char* caption, const char* condensedCaption);
	void AddContent(ContentType::Enum contentType, const char* string);
	void AppendContent(ContentType::Enum contentType, const char* string);
	void AddContentTag(sysLanguage language, ContentType::Enum contentType, const char* tag);
	void AddContentThumbnail(ContentType::Enum contentType, const char* thumbnail);

	void AddCaptionSubString(sysLanguage language, const char* subString);
	void ConfirmCaptionSubStringComplete();
	void AddCaptionSubFloat(float subValue, int noofDecimalPlaces = 0);
	void AddCaptionSubInt(int subValue);

private:
	void Reset();

	void BuildCaption(sysLanguage language);

	char m_caption[MAX_LANGUAGES][maxStringLength];
	char m_condensedCaption[MAX_LANGUAGES][maxStringLength];

	char m_subString[maxNoofSubStrings][MAX_LANGUAGES][maxSubStringLength];
	char m_subValue[maxNoofSubValues][maxSubValueLength];

	char m_content[ContentType::Noof][maxStringLength];
	char m_tag[ActionTab::Noof][MAX_LANGUAGES][maxStringLength];
	char m_thumbnail[ActionTab::Noof][maxStringLength];

	unsigned int m_postTypeEnum;
	bool m_hasCaptions;
	unsigned char m_contentUsedFlag;
	unsigned char m_thumbnailUsedFlag;

	unsigned char m_noofSubStrings;
	unsigned char m_noofSubValues;

#if __ASSERT
	unsigned char m_noofMainThumbnailCalls;
#endif

	char m_smallImageAspectRatio[aspectRatioStringLength];

};

class rlNpPostActivityFeedStoryWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPostActivityFeedStoryWorkItem);

	rlNpPostActivityFeedStoryWorkItem();
	~rlNpPostActivityFeedStoryWorkItem();

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpActivityFeedStory* detail);

	virtual const char* GetRequestBody() { return m_Body; }
	virtual size_t GetRequestLength() { return m_Length; }

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	void BuildCaptionsString(char* o_data, int dataSize, bool isCondensed, rlNpActivityFeedStory *detail);
	bool BuildActionString(char* o_data, int dataSize, rlNpActivityFeedStory::ContentType::Enum contentType, rlNpActivityFeedStory *detail);
	bool BuildImageString(char* o_data, int dataSize, rlNpActivityFeedStory::ContentType::Enum contentType, rlNpActivityFeedStory *detail);

private:
	char * m_Body;
	int m_Length;
};

class rlNpActivityFeedPlayedWithDetails
{
public:
	enum
	{
		maxNoofPlayers = 32,
		maxStringLength = 128
	};

	rlNpActivityFeedPlayedWithDetails() { Reset(); }

	void Start(const char* gameMode);

	void AddPlayerToList(const rlGamerHandle& player);
	void IgnoreLocalPlayer(const rlGamerHandle& player);

	const rlGamerHandle& GetPlayer(int index) const;
	const char* GetGameMode() const;
	bool HasPlayer(int index) const;
	int GetNoofPlayers() const;

private:

	void Reset();

	rlGamerHandle m_player[maxNoofPlayers];
	char m_gameMode[maxStringLength];
	int m_noofPlayersInList;
	unsigned int m_playerUsed;
};

class rlNpPostActivityFeedPlayedWithWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpPostActivityFeedPlayedWithWorkItem);

	rlNpPostActivityFeedPlayedWithWorkItem();
	~rlNpPostActivityFeedPlayedWithWorkItem();

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpActivityFeedPlayedWithDetails* detail);

	virtual const char* GetRequestBody() { return m_Body; }
	virtual size_t GetRequestLength() { return m_Length; }

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	void BuildPlayerString(char* o_data, int dataSize, rlNpActivityFeedPlayedWithDetails* detail);

private:
	char * m_Body;
	int m_Length;
};

class rlNpGetActivityFeedVideoDataDetails
{
public:
	enum 
	{
		maxNoofStories = 100,
		maxNoofMeta = 12,
		maxNoofTitleIds = 6,
		maxValueStringLength = 32,
		maxNoofRecordedVideos = 20
	};

	rlNpGetActivityFeedVideoDataDetails();

	void SetDateToCheckTo(const char* time);
	bool IsNewerThanCheckDate(const char* time);

	void Reset();
	bool AddVideoID(const char* videoID);
	bool AddTitleID(const char* titleID);
	bool AddDate(const char* date);
	bool FinaliseEntry();
private:

	struct VideoPostInfo {
		VideoPostInfo() : m_noofTitleIds(0) { }

		char m_videoId[maxValueStringLength];
		char m_titleId[maxNoofTitleIds][maxValueStringLength];
		char m_date[maxValueStringLength];
		int m_noofTitleIds;
	};

	VideoPostInfo m_recordedVideos[maxNoofRecordedVideos];
	int m_noofRecordedVideos;

	SceRtcTick m_dateToCheckTo;
};


class rlNpGetActivityFeedVideoDataWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetActivityFeedVideoDataWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetActivityFeedVideoDataDetails* details);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	rlNpGetActivityFeedVideoDataDetails* m_Details;
};

class rlNpIdentityMapperGetAccountIdWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpIdentityMapperGetAccountIdWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpOnlineId& targetOnlineId, rlSceNpAccountId* out_accountId);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	rlSceNpOnlineId m_TargetOnlineId;
	rlSceNpAccountId* m_TargetAccountId;
};

class rlNpIdentityMapperGetAccountIdsWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpIdentityMapperGetAccountIdsWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpOnlineId* targetOnlineIds, const unsigned numIds, rlSceNpAccountId* out_accountIds);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return m_ContentWriter.ToString(); }
	virtual size_t GetRequestLength() { return m_ContentWriter.Length(); }

private:

	RsonWriter m_ContentWriter;
	char m_ContentBuffer[MAPPER_MAX_LENGTH];
	unsigned m_NumIds;
	rlSceNpAccountId* m_TargetAccountIds;
};

class rlNpIdentityMapperGetOnlineIdWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpIdentityMapperGetOnlineIdWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpAccountId targetAccountId, rlSceNpOnlineId* out_onlineId);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);

	rlSceNpAccountId m_TargetAccountId;
	rlSceNpOnlineId* m_TargetOnlineId;
};

class rlNpIdentityMapperGetOnlineIdsWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpIdentityMapperGetOnlineIdsWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpAccountId* targetAccountIds, const unsigned numIds, rlSceNpOnlineId* out_onlineIds);
	bool ProcessSuccess();

protected:
	virtual const char* GetApiGroup();
	virtual void SetupApiPath(rlSceNpAccountId accountId);
	virtual const char* GetRequestBody() { return m_ContentWriter.ToString(); }
	virtual size_t GetRequestLength() { return m_ContentWriter.Length(); }

private:

	RsonWriter m_ContentWriter;
	char m_ContentBuffer[MAPPER_MAX_LENGTH];
	unsigned m_NumIds;
	rlSceNpOnlineId* m_TargetOnlineIds;
};

#if RL_NP_CPPWEBAPI
class rlNpCheckProfanityWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpCheckProfanityWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const sysLanguage language,
					const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord);

	void DoWork() override;

protected:
	// On Orbis SCE_NP_WORD_FILTER_SANITIZE_COMMENT_MAXLEN was 1024. 
	// There's no define on Prospero.
	static const unsigned PROFANITY_BUFFER_MAX_LEN = 4096;

	char m_Text[PROFANITY_BUFFER_MAX_LEN];
	char* m_ProfaneWord;
	unsigned  m_ProfaneWordBufferSize;
	unsigned* m_NumProfaneWords;
	sysLanguage m_Language;
};

class rlNpCommunicationRestrictionWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpCommunicationRestrictionWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, bool* restricted);
	void DoWork() override;

protected:
	bool* m_Restricted;
};

class rlNpCreatePlayerSessionWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpCreatePlayerSessionWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, 
					const rlNpWebApiPostSessionParams& sessionParams, const SceNpWebApi2PushEventPushContextId& pushContextId);
	void DoWork() override;

protected:
	rlNpWebApiPostSessionParams m_SessionParams;
	SceNpWebApi2PushEventPushContextId m_PushContextId;
};

class rlNpJoinPlayerSessionAsPlayerWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpJoinPlayerSessionAsPlayerWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
					const rlSceNpSessionId& sessionId, const SceNpWebApi2PushEventPushContextId& pushContextId);
	void DoWork() override;

protected:
	rlSceNpSessionId m_SessionId;
	SceNpWebApi2PushEventPushContextId m_PushContextId;
};

class rlNpJoinPlayerSessionAsSpectatorWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpJoinPlayerSessionAsSpectatorWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
					const rlSceNpSessionId& sessionId, const SceNpWebApi2PushEventPushContextId& pushContextId);
	void DoWork() override;

protected:
	rlSceNpSessionId m_SessionId;
	SceNpWebApi2PushEventPushContextId m_PushContextId;
};

class rlNpLeavePlayerSessionWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpLeavePlayerSessionWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId);
	void DoWork() override;

protected:
	rlSceNpSessionId m_SessionId;
};

class rlNpGetPlayerSessionWorkItem : public rlNpCppWebApiWorkItem
{
public:
	rlNpGetPlayerSessionWorkItem();

	RLNP_WORK_ITEM_NAME(rlNpGetPlayerSessionWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId* sessionId, 
		rlNpWebApiSessionDetail* outDetails, const unsigned numSessions, const bool failOnMissing);
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, rlSessionInfo* outInfo);
	void DoWork() override;

protected:
	static const unsigned MAX_REQUESTABLE_SESSIONS = 100;
	static const unsigned SESSION_ID_BUFFER_SIZE = (rlSceNpSessionId::MAX_DATA_BUF_SIZE + 1) * MAX_REQUESTABLE_SESSIONS + 1;
	CompileTimeAssert(MAX_REQUESTABLE_SESSIONS >= NP_WEB_API_MAX_INVITATIONS);

	rlSceNpSessionId m_SessionId[MAX_REQUESTABLE_SESSIONS];
	rlNpWebApiSessionDetail* m_SessionDetails;
	rlSessionInfo* m_SessionInfo;
	unsigned m_NumSessions;
	bool m_FailOnMissing;
};

class rlNpGetFriendsPlayerSessionsWorkItem : public rlNpCppWebApiWorkItem
{
public:
	rlNpGetFriendsPlayerSessionsWorkItem();

	RLNP_WORK_ITEM_NAME(rlNpGetFriendsPlayerSessionsWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlFriend* friends, const unsigned numFriends);
	void DoWork() override;

protected:
	rlFriend* m_Friends;
	unsigned m_NumFriends;
};

class rlNpGetJoinedPlayerSessionsWorkItem : public rlNpCppWebApiWorkItem
{
public:
    rlNpGetJoinedPlayerSessionsWorkItem();

    RLNP_WORK_ITEM_NAME(rlNpGetJoinedPlayerSessionsWorkItem);

    bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
        rlSceNpSessionId* sessionIds, const unsigned numSessions);
    void DoWork() override;

protected:
    static const unsigned MAX_REQUESTABLE_SESSIONS = 20; // This isn't a hard limit

    rlSceNpSessionId* m_SessionIds;
    unsigned m_NumSessions;
};

class rlNpSendPlayerSessionInvitationsWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpSendPlayerSessionInvitationsWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiPostInvitationParam* param);
	void DoWork() override;

protected:
	rlNpWebApiPostInvitationParam m_InviteParam;
};

class rlNpCreateMatchWorkItem : public rlNpCppWebApiWorkItem
{
public:
	rlNpCreateMatchWorkItem();

	RLNP_WORK_ITEM_NAME(rlNpCreateMatchWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchCreateParams& param, rlNpMatchId* matchId);
	void DoWork() override;

protected:
	rlNpMatchCreateParams m_Param;
	rlNpMatchId* m_MatchId;
};

class rlNpUpdateMatchDetailWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpUpdateMatchDetailWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchUpdateParams& param);
	void DoWork() override;

protected:
	rlNpMatchUpdateParams m_Param;
};

class rlNpUpdateMatchStatusWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpUpdateMatchStatusWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId, rlNpMatchStatus matchStatus);
	void DoWork() override;

protected:
	rlNpMatchId m_MatchId;
	rlNpMatchStatus m_MatchStatus;
};

class rlNpJoinMatchWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpJoinMatchWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId, const rlNpMatchJoiningPlayers& joining);
	void DoWork() override;

protected:
	rlNpMatchJoiningPlayers m_Players;
	rlNpMatchId m_MatchId;
};

class rlNpLeaveMatchWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpLeaveMatchWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId, const rlNpMatchLeavingPlayers& leaving);
	void DoWork() override;

protected:
	rlNpMatchLeavingPlayers m_Players;
	rlNpMatchId m_MatchId;
};

class rlNpReportMatchResultWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpReportMatchResultWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchResultParams& result);
	void DoWork() override;

protected:
	rlNpMatchResultParams m_Result;
};


class rlNpGetMatchDetailWorkItem : public rlNpCppWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetMatchDetailWorkItem);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId);
	void DoWork() override;

protected:
	rlNpMatchCreateParams m_Param;
	rlNpMatchId m_MatchId;
};
#endif //RL_NP_CPPWEBAPI

#if RL_NP_UDS
class rlNpGetPlayerActiveActivitiesWorkItem : public rlNpWebApiWorkItem
{
public:
	RLNP_WORK_ITEM_NAME(rlNpGetPlayerActiveActivitiesWorkItem);

	virtual bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId);

protected:
	virtual const char* GetApiGroup() override;
	virtual void SetupApiPath(rlSceNpAccountId accountId) override;
	virtual bool ProcessSuccess() override;
};
#endif //RL_NP_UDS

} // namespace rage

#endif // RLINE_RLWEBAPIWORKITEM_H
#endif // RSG_ORBIS
