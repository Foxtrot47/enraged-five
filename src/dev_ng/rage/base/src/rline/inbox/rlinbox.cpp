// 
// rline/rlinbox.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "rlinbox.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "net/task.h"
#include "parser/manager.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/clan/rlclan.h"
#include "rline/rlfriend.h"
#include "rline/rlpresence.h"
#include "rline/rlfriendsreader.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, inbox)
#undef __rage_channel
#define __rage_channel rline_inbox

///////////////////////////////////////////////////////////////////////////////
// rlInboxPostMessageTask
///////////////////////////////////////////////////////////////////////////////
class rlInboxPostMessageTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlInboxPostMessageTask);
    RL_TASK_USE_CHANNEL(rline_inbox);

    rlInboxPostMessageTask()
    {
    }

    virtual ~rlInboxPostMessageTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const rlGamerHandle* recipients,
                    const unsigned numRecipients,
                    const char* message,
                    const char** tags,
                    const unsigned numTags,
                    const unsigned ttlSeconds)
    {
        if(!rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
        {
            rlTaskError("Invalid gamer index: %d", localGamerIndex);
            return false;
        }

        if(!rlRosHttpTask::Configure(localGamerIndex))
        {
            rlTaskError("Failed to configure base class");
            return false;
        }

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        if(!cred.IsValid())
        {
            rlTaskError("Credentials are invalid");
            return false;
        }

        if(!rlVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
        {
            rlTaskError("Failed to add 'ticket' parameter");
            return false;
        }

        if(!rlVerify(BeginListParameter("userIds")))
        {
            rlTaskError("Failed to add 'userIds' parameter");
            return false;
        }

        unsigned addedIds = 0;

        for(int i = 0; i < (int)numRecipients; ++i)
        {
            if (!recipients[i].IsValid() || recipients[i].IsBot())
            {
                rlTaskWarning("Invalid recipient gamer handle. idx: %d", i);
                continue;
            }

            char userIdStr[128];
            recipients[i].ToUserId(userIdStr);
            if(!rlVerify(AddListParamStringValue(userIdStr)))
            {
                rlTaskError("Failed to add user ID");
                return false;
            }

            ++addedIds;
        }

        if (!rlVerify(addedIds > 0))
        {
            rlTaskError("Faild to add any recipient out of %u", numRecipients);
            return false;
        }

        if(!rlVerify(AddStringParameter("message", message, true)))
        {
            rlTaskError("Failed to add 'message' parameter");
            return false;
        }

        if(!rlVerify(BeginListParameter("tagsCsv")))
        {
            rlTaskError("Failed to add 'tagsCsv' parameter");
            return false;
        }

        for(int i = 0; i < (int)numTags; ++i)
        {
            if(!rlVerify(AddListParamStringValue(tags[i])))
            {
                rlTaskError("Failed to add tag '%s'", tags[i]);
                return false;
            }
        }

        if(!rlVerify(AddUnsParameter("ttlSeconds", ttlSeconds)))
        {
            rlTaskError("Failed to add 'ttlSeconds' parameter");
            return false;
        }

        return true;
    }

    virtual bool ProcessSuccess(const rlRosResult& /*result*/,
                                const parTreeNode* /*node*/,
                                int& /*resultCode*/)
    {
        return true;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Inbox.asmx/PostMessageToRecipients";
    }
};

///////////////////////////////////////////////////////////////////////////////
// rlInboxPublishMessageTask
///////////////////////////////////////////////////////////////////////////////
class rlInboxPublishMessageTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlInboxPublishMessageTask);
    RL_TASK_USE_CHANNEL(rline_inbox);

    rlInboxPublishMessageTask()
    {
    }

    virtual ~rlInboxPublishMessageTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const char* channel,
                    const char* message,
                    const char** tags,
                    const unsigned numTags,
                    const unsigned ttlSeconds)
    {
        if(!rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
        {
            rlTaskError("Invalid gamer index: %d", localGamerIndex);
            return false;
        }

        if(!rlRosHttpTask::Configure(localGamerIndex))
        {
            rlTaskError("Failed to configure base class");
            return false;
        }

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        if(!cred.IsValid())
        {
            rlTaskError("Credentials are invalid");
            return false;
        }

        if(!rlVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
        {
            rlTaskError("Failed to add 'ticket' parameter");
            return false;
        }

        if(!rlVerify(AddStringParameter("channel", channel, true)))
        {
            rlTaskError("Failed to add 'channel' parameter");
            return false;
        }

        if(!rlVerify(AddStringParameter("message", message, true)))
        {
            rlTaskError("Failed to add 'message' parameter");
            return false;
        }

        if(!rlVerify(BeginListParameter("tagsCsv")))
        {
            rlTaskError("Failed to add 'tagsCsv' parameter");
            return false;
        }

        for(int i = 0; i < (int)numTags; ++i)
        {
            if(!rlVerify(AddListParamStringValue(tags[i])))
            {
                rlTaskError("Failed to add tag '%s'", tags[i]);
                return false;
            }
        }

        if(!rlVerify(AddUnsParameter("ttlSeconds", ttlSeconds)))
        {
            rlTaskError("Failed to add 'ttlSeconds' parameter");
            return false;
        }

        return true;
    }

    virtual bool ProcessSuccess(const rlRosResult& /*result*/,
                                const parTreeNode* /*node*/,
                                int& /*resultCode*/)
    {
        return true;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Inbox.asmx/PublishMessage";
    }
};

///////////////////////////////////////////////////////////////////////////////
// rlInboxGetMessagesTask
///////////////////////////////////////////////////////////////////////////////
class rlInboxGetMessagesTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlInboxGetMessagesTask);
    RL_TASK_USE_CHANNEL(rline_inbox);

    enum Option
    {
        OPTION_INVALID      = -1,
        OPTION_GET_ALL_MESSAGES,
        OPTION_GET_UNREAD_ONLY
    };

    rlInboxGetMessagesTask()
        : m_Option(OPTION_INVALID)
        , m_MaxMessages(0)
        , m_MessageIter(NULL)
    {
    }

    virtual ~rlInboxGetMessagesTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const unsigned offset,
                    const unsigned count,
                    const Option option,
					const char** tags,
					const unsigned numTags,
                    rlInboxMessageIterator* iter)
    {
        if(!rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
        {
            rlTaskError("Invalid gamer index: %d", localGamerIndex);
            return false;
        }

        if(!rlVerify(iter->Begin()))
        {
            rlTaskError("Error initializing message iterator");
        }

        //Set the option before calling rlRosHttpTask::Configure()
        //to make sure the correct service method is returned.
        m_Option = option;

        if(!rlRosHttpTask::Configure(localGamerIndex, &iter->m_ParserTree))
        {
            rlTaskError("Failed to configure base class");
            return false;
        }

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        if(!cred.IsValid())
        {
            rlTaskError("Credentials are invalid");
            return false;
        }

        if(!rlVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
        {
            rlTaskError("Failed to add 'ticket' parameter");
            return false;
		}
		
		if(!rlVerify(BeginListParameter("tagsCsv")))
		{
			rlTaskError("Failed to add 'tagsCsv' parameter");
			return false;
		}

		if(numTags > 0 && rlVerify(tags))
		{
			for(int i = 0; i < (int)numTags; ++i)
			{
				if(!rlVerify(AddListParamStringValue(tags[i])))
				{
					rlTaskError("Failed to add tag '%s'", tags[i]);
					return false;
				}
			}
		}

        if(!rlVerify(AddUnsParameter("offset", offset)))
        {
            rlTaskError("Failed to add 'offset' parameter");
            return false;
        }

        if(!rlVerify(AddUnsParameter("limit", count)))
        {
            rlTaskError("Failed to add 'limit' parameter");
            return false;
        }	

        m_MaxMessages = count;
        m_MessageIter = iter;

        return true;
    }

    virtual bool ProcessSuccess(const rlRosResult& /*result*/,
                                const parTreeNode* node,
                                int& /*resultCode*/)
    {
        rlAssert(node);

        rtry
        {
            m_MessageIter->m_NumMessagesRetrieved = 0;

            const parTreeNode* messagesNode = node->FindChildWithName("Messages");
            rcheck(messagesNode, catchall, 
                    rlTaskError("Failed to find <Messages> element"));

            const parAttribute* attr;
            const parElement* el = &messagesNode->GetElement();

            //Number of messages
            attr = el->FindAttribute("Count");
            rcheck(attr, catchall, rlTaskError("Failed to find 'Count' attribute"));
            const int msgCount = attr->FindIntValue();

            //Total number of messages
            attr = el->FindAttribute("Total");
            rcheck(attr, catchall, rlTaskError("Failed to find 'Total' attribute"));
            const int msgTotal = attr->FindIntValue();

            rlTaskDebug2("Received %d out of %d total messages", msgCount, msgTotal);

            rverify((unsigned)msgCount <= m_MaxMessages,
                    catchall,
                    rlTaskError("Message count returned: %d exceeds max requested: %d",
                                msgCount, m_MaxMessages));

            m_MessageIter->m_TotalMessages = msgTotal;
            m_MessageIter->m_NumMessagesRetrieved = msgCount;
        }
        rcatchall
        {
            return false;
        }

        return true;
    }

    virtual const char* GetServiceMethod() const
    {
        switch(m_Option)
        {
        case OPTION_GET_ALL_MESSAGES:
            return "Inbox.asmx/GetMessages";
        case OPTION_GET_UNREAD_ONLY:
            return "Inbox.asmx/GetUnreadMessages";
        case OPTION_INVALID:
        default:
            rlAssert(false);
            rlTaskError("Invalid option %d", m_Option);
            return "";
        }
    }

private:

    Option m_Option;
    unsigned m_MaxMessages;
    rlInboxMessageIterator* m_MessageIter;
};

///////////////////////////////////////////////////////////////////////////////
// rlInboxDeleteMessagesTask
///////////////////////////////////////////////////////////////////////////////
class rlInboxDeleteMessagesTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlInboxDeleteMessagesTask);
    RL_TASK_USE_CHANNEL(rline_inbox);

    rlInboxDeleteMessagesTask()
    {
    }

    virtual ~rlInboxDeleteMessagesTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const char** messageIds,
                    const unsigned numMessageIds)
    {
        if(!rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
        {
            rlTaskError("Invalid gamer index: %d", localGamerIndex);
            return false;
        }

        if(!rlRosHttpTask::Configure(localGamerIndex))
        {
            rlTaskError("Failed to configure base class");
            return false;
        }

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        if(!cred.IsValid())
        {
            rlTaskError("Credentials are invalid");
            return false;
        }

        if(!rlVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
        {
            rlTaskError("Failed to add 'ticket' parameter");
            return false;
		}
		
		if(!rlVerify(BeginListParameter("messageIdsCsv")))
		{
			rlTaskError("Failed to add 'messageIdsCsv' parameter");
			return false;
		}

		for(int i = 0; i < (int)numMessageIds; ++i)
		{
			if(!rlVerify(AddListParamStringValue(messageIds[i])))
			{
				rlTaskError("Failed to add message id '%s'", messageIds[i]);
				return false;
			}
		}	

        return true;
    }

    virtual bool ProcessSuccess(const rlRosResult& /*result*/,
                                const parTreeNode* /*node*/,
                                int& /*resultCode*/)
    {
        return true;
    }

    virtual const char* GetServiceMethod() const
    {
        return "Inbox.asmx/DeleteMessages";
    }
};

class rlInboxPublishToManyFriendsTask : public rlTaskBase
{
public:

	NET_TASK_DECL(rlInboxPublishToManyFriendsTask);
	NET_TASK_USE_CHANNEL(rline_inbox);

	rlInboxPublishToManyFriendsTask()
		: m_LocalGamerindex(-1)
		, m_State(STATE_GET_ONLINE_FRIENDS)
	{
		m_Message[0] = '\0';
		m_FriendPage.Init();
	}

	~rlInboxPublishToManyFriendsTask()
	{

	}

	bool Configure(const int localGamerindex, const char* message, const char** tags, const unsigned numTags, const unsigned ttlSeconds)
	{
		m_LocalGamerindex = localGamerindex;

		bool success = true;
		rtry
		{
			rverify(m_FriendReader.Init(0), catchall, rlError("Could not initialize friend reader"));
			rverify(numTags <= RL_INBOX_MAX_TAGS, catchall, rlError("Too many inbox tags - increase RL_INBOX_MAX_TAGS"));

			m_TtlSeconds = ttlSeconds;
			m_NumTags = numTags;

			safecpy(m_Message, message);
			
			for(unsigned i = 0; i < numTags; i++)
			{
				m_TagList.Add(tags[i]);
			}

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}
	
	virtual void Update(const unsigned )
	{
		switch(m_State)
		{
		case STATE_GET_ONLINE_FRIENDS:
			{
				int flags = rlFriendsReader::FRIENDS_ONLINE_IN_TITLE
					| rlFriendsReader::FRIENDS_FAVORITES
					| rlFriendsReader::FRIENDS_TWOWAY;

				if (m_FriendReader.Read(m_LocalGamerindex, 0, m_FriendPage.m_Friends, sizeof(m_FriendPage.m_Friends), &m_NumFriends, &m_TotalFriends, flags, &m_FriendStatus))
				{
					m_State = STATE_GETTING_ONLINE_FRIENDS;
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
			}
			break;
		case STATE_GETTING_ONLINE_FRIENDS:
			if (!m_FriendStatus.Pending())
			{
				if (m_FriendStatus.Succeeded())
				{
					if (m_NumFriends == 0)
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else
					{
						m_State = STATE_POST_MESSAGE;
					}
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
			}
			break;
		case STATE_POST_MESSAGE:
			if (BeginPostMessage())
			{
				this->Finish(FINISH_SUCCEEDED);
			}
			else
			{
				this->Finish(FINISH_FAILED);
			}
			break;
		}
	}

	virtual void DoCancel()
	{
		rlGetTaskManager()->CancelTask(&m_FriendStatus);
	}

private:

	bool BeginPostMessage()
	{
		//Collect the gamer handles of my friends.
		rlGamerHandle gamerHandles[rlFriendsPage::MAX_FRIEND_PAGE_SIZE];
		unsigned numGamerHandles = 0;

		for(int i = 0; i < (int)m_NumFriends; ++i)
		{
			if (!m_FriendPage.m_Friends[i].IsValid())
			{
				continue;
			}

			if(m_FriendPage.m_Friends[i].IsPendingFriend())
			{
				//Not full friends yet
				continue;
			}

			if (!m_FriendPage.m_Friends[i].IsOnline())
			{
				// friend is not online
				continue;
			}

			if(!rlVerifyf(m_FriendPage.m_Friends[i].GetGamerHandle(&gamerHandles[numGamerHandles]), "Error getting friend's gamer handle"))
			{
				continue;
			}

			if (!gamerHandles[numGamerHandles].IsValid())
			{
				continue;
			}

			numGamerHandles++;
		}

		if(numGamerHandles > 0)
		{
			if (m_NumTags == 0)
			{
				return rlInbox::PostMessage(m_LocalGamerindex, gamerHandles, numGamerHandles, m_Message, NULL, 0, m_TtlSeconds);
			}
			else
			{
				const char** tagsList = m_TagList.GetPointerList();
				return rlInbox::PostMessage(m_LocalGamerindex, gamerHandles, numGamerHandles, m_Message, tagsList, m_NumTags, m_TtlSeconds);
			}
		}
		else
		{
			return true;
		}
	}

	enum MessageState
	{
		STATE_GET_ONLINE_FRIENDS,
		STATE_GETTING_ONLINE_FRIENDS,
		STATE_POST_MESSAGE,
	};

	rlFriendsReader m_FriendReader;
	netStatus m_FriendStatus;
	unsigned m_NumFriends;
	unsigned m_TotalFriends;
	rlFriendsPage m_FriendPage;

	int m_LocalGamerindex;
	MessageState m_State;

	unsigned m_TtlSeconds;
	unsigned m_NumTags;
	char m_Message[RL_INBOX_MAX_CONTENT_LENGTH];
	rlInboxTagList m_TagList;
};

//////////////////////////////////////////////////////////////////////////
//  rlInboxMessageIterator
//////////////////////////////////////////////////////////////////////////
rlInboxMessageIterator::rlInboxMessageIterator()
    : m_ParserTree(NULL)
    , m_MsgNode(NULL)
    , m_NumMessagesRetrieved(0)
    , m_TotalMessages(0)
    , m_MessageIndex(0)
    , m_State(STATE_NONE)
{
}

void
rlInboxMessageIterator::ReleaseResources()
{
    if(m_State > STATE_NONE)
    {
        if(m_ParserTree)
        {
            delete m_ParserTree;
            m_ParserTree = NULL;
        }

        SHUTDOWN_PARSER;

        m_MsgNode = NULL;
        m_NumMessagesRetrieved = 0;
        m_TotalMessages = 0;
        m_MessageIndex = 0;
        m_State = STATE_NONE;
    }
}

bool
rlInboxMessageIterator::IsReleased() const
{
    return STATE_NONE == m_State;
}

bool
rlInboxMessageIterator::NextMessage(const char** message,
                                    const char** messageId,
                                    u64* createPosixTime,
                                    u64* readPosixTime)
{
    if(m_ParserTree)
    {
        if(NextMessage(m_ParserTree->GetRoot(), &m_MsgNode, message, messageId, createPosixTime, readPosixTime))
        {
            rlDebug2("  Message %d: '%s'", m_MessageIndex, *message);
            ++m_MessageIndex;
            return true;
        }
        else
        {
            ReleaseResources();
        }
    }

    return false;
}

unsigned
rlInboxMessageIterator::GetNumMessagesRetrieved() const
{
    return m_NumMessagesRetrieved;
}

unsigned
rlInboxMessageIterator::GetTotalMessages() const
{
    return m_TotalMessages;
}

//private:

bool
rlInboxMessageIterator::Begin()
{
    rlAssertf(IsReleased(), "Iterator has not been released - need to call rlInboxMessageIterator::ReleaseResources()");

    ReleaseResources();

    INIT_PARSER;
    m_State = STATE_INITIALIZED;
    return true;
}

bool
rlInboxMessageIterator::NextMessage(const parTreeNode* rootNode,
                                    const parTreeNode** msgNode,
                                    const char** message,
                                    const char** messageId,
                                    u64* createPosixTime,
                                    u64* readPosixTime)
{
    rtry
    {
        if(NULL == *msgNode)
        {
            const parTreeNode* messagesNode = rootNode->FindChildWithName("Messages");
            rcheck(messagesNode, catchall, 
                    rlError("Failed to find <Messages> element"));

            *msgNode = messagesNode->GetChild();
        }
        else
        {
            *msgNode = (*msgNode)->GetSibling();
        }

        if(*msgNode)
        {
            const parElement* el = &(*msgNode)->GetElement();

			//inbox message id
			const parAttribute* attr = el->FindAttribute("id");
			rverify(attr, catchall, rlError("Failed to find 'id' message attribute"));
			*messageId = attr->GetStringValue();
			rverify(strlen(*messageId) <= RL_INBOX_ID_MAX_LENGTH,
                    catchall,
                    rlError("msgId %s is larger than expected size [%d]", *messageId, RL_INBOX_ID_MAX_LENGTH));
				
            //Create timestamp
            attr = el->FindAttribute("t");
            rverify(attr, catchall, rlError("Failed to find 't' message attribute"));
            const char* tsStr = attr->GetStringValue();
            rverify(1 == sscanf(tsStr, "%" I64FMT "u", createPosixTime),
                    catchall,
                    rlError("Failed to parse create timestamp: %s", tsStr));

            //Read timestamp
            attr = el->FindAttribute("rt");
			rverify(attr, catchall, rlError("Failed to find 'rt' message attribute"));
			const char* readStr = attr->GetStringValue();
			rverify(1 == sscanf(readStr, "%" I64FMT "u", readPosixTime),
				catchall,
				rlError("Failed to parse read timestamp: %s", readStr));
             
			//Message content
            *message = (*msgNode)->GetData();
            rverify(*message, catchall, rlError("No message data"));

            return true;
        }
    }
    rcatchall
    {
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
//  rlInbox
//////////////////////////////////////////////////////////////////////////

bool
rlInbox::PostMessage(const int localGamerIndex,
                    const rlGamerHandle* recipients,
                    const unsigned numRecipients,
                    const char* message,
                    const char** tags,
                    const unsigned numTags,
                    const unsigned ttlSeconds)
{
    bool success = false;

    rlDebug2("Posting a '%s' to %d recipients...",
            message,
            numRecipients);

    rlFireAndForgetTask<rlInboxPostMessageTask>* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    recipients,
                                    numRecipients,
                                    message,
                                    tags,
                                    numTags,
                                    ttlSeconds,
                                    &task->m_Status),
                catchall,
                rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task),
                catchall,
                rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool rlInbox::PostMessageToManyFriends(const int localGamerIndex, const char* message, const char** tags, const unsigned numTags, const unsigned ttlSeconds)
{
	bool success = false;
	rlDebug2("Posting a '%s' to the many friends of gamer %d...", message, localGamerIndex);
	rlFireAndForgetTask<rlInboxPublishToManyFriendsTask>* task = NULL;

	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
		rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));
		rverify(rlTaskBase::Configure(task, localGamerIndex, message, tags, numTags, ttlSeconds, &task->m_Status), catchall, rlError("Error configuring task"));
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}

bool
rlInbox::PostMessageToClan(const int localGamerIndex,
                            const s64 clanId,
                            const char* message,
                            const char** tags,
                            const unsigned numTags,
                            const unsigned ttlSeconds)
{
    bool success = false;

    rlDebug2("Publishing a '%s' to clan %" I64FMT "d...",
            message,
            clanId);

    rlFireAndForgetTask<rlInboxPublishMessageTask>* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error creating task"));

        char channel[64];
        rlClan::CreateMessageChannelName(channel, clanId);

        rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    channel,
                                    message,
                                    tags,
                                    numTags,
                                    ttlSeconds,
                                    &task->m_Status),
                catchall,
                rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task),
                catchall,
                rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

static bool GetInboxMessages(const int localGamerIndex,
                            const unsigned offset,
                            const unsigned count,
					        const char** tags,
					        const unsigned numTags,
                            rlInboxMessageIterator* iter,
                            const rlInboxGetMessagesTask::Option option,
                            netStatus* status)
{
    bool success = false;

    rlDebug2("Retrieving messages for local gamer: %d...",
            localGamerIndex);

    rlInboxGetMessagesTask* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

		rverify(numTags == 0 || tags != NULL,
			catchall,
			rlError("specificed %d numTags but tags list is NULL", numTags));

        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        offset,
                                        count,
                                        option,
										tags,
										numTags,
                                        iter,
                                        status),
                catchall,
                rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task),
                catchall,
                rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlInbox::GetMessages(const int localGamerIndex,
                    const unsigned offset,
                    const unsigned count,
					const char** tags,
					const unsigned numTags,
                    rlInboxMessageIterator* iter,
                    netStatus* status)
{
    return GetInboxMessages(localGamerIndex, offset, count, tags, numTags, iter, rlInboxGetMessagesTask::OPTION_GET_ALL_MESSAGES, status);
}

bool
rlInbox::GetUnreadMessages(const int localGamerIndex,
                            const unsigned offset,
                            const unsigned count,
					        const char** tags,
					        const unsigned numTags,
                            rlInboxMessageIterator* iter,
                            netStatus* status)
{
    return GetInboxMessages(localGamerIndex, offset, count, tags, numTags, iter, rlInboxGetMessagesTask::OPTION_GET_UNREAD_ONLY, status);
}

bool
rlInbox::DeleteMessages(const int localGamerIndex,
						const char** messageIds,
						const unsigned numMessageIds)
{
    bool success = false;

    rlDebug2("Deleting messages for local gamer: %d...",
            localGamerIndex);

    rlFireAndForgetTask<rlInboxDeleteMessagesTask>* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),
                catchall,
                rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        messageIds,
                                        numMessageIds,
                                        &task->m_Status),
                catchall,
                rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task),
                catchall,
                rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

void
rlInbox::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

void rlInboxTagList::Add( const char* inNewTag)
{
	//Make sure we don't already have the tag in the list.
	for (int i = 0; i < m_tags.GetCount(); ++i)
	{
		if(m_tags[i] == inNewTag)
			return;
	}

	rlInboxTag& rNewTag = m_tags.Grow(RL_INBOX_MAX_TAGS);
	rNewTag = inNewTag;
	m_pointers.Grow(8) = (const char*)rNewTag;
}

bool rlInboxTagList::rlInboxTag::operator==( const char* str ) const
{
	return atStringHash(m_tag) == atStringHash(str);
}

void rlInboxTagList::Clear()
{
	m_tags.Reset(false);
	m_pointers.Reset(false);
}

unsigned rlInboxTagList::GetCount() const
{
	rageAssert(m_tags.GetCount() == m_pointers.GetCount());
	return (unsigned)m_pointers.GetCount();
}

const char** rlInboxTagList::GetPointerList()
{
	return m_pointers.GetCount() > 0 ? &(m_pointers[0]) : NULL;
}

}
