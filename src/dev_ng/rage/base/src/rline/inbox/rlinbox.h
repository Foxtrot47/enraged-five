// 
// rline/rlinbox.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLINBOX_H
#define RLINE_RLINBOX_H

#include "atl/array.h"
#include "rline/rldiag.h"

namespace rage
{

class rlGamerHandle;
class netStatus;
class parTree;
class parTreeNode;

//Maximum length of a message ID.
#define RL_INBOX_ID_MAX_LENGTH 32
#define RL_INBOX_MAX_CONTENT_LENGTH 384
#define RL_INBOX_MAX_TAGS 8
#define RL_INBOX_MAX_TAGS_LENGTH 32

//PURPOSE
//  Used to iterate over messages retrieved with rlInbox::GetMessages()
class rlInboxMessageIterator
{
    friend class rlInboxGetMessagesTask;
public:
    rlInboxMessageIterator();

    //PURPOSE
    //  Releases memory used by the iterator.
    //  Be sure to call this when finished with the iterator.
    void ReleaseResources();

    //PURPOSE
    //  Returns true when resources have been released
    bool IsReleased() const;

    //PURPOSE
    //  Retrieves the next message in the iteration
    //RETURNS
    //  True if a message was retrieved, false if at the end
    //  of the iteration.
    //NOTES
    //  At the end of the iteration RelaseResources() will be called
    //  to free resources.
    bool NextMessage(const char** message,
                    const char** messageId,
                    u64* createPosixTime,
                    u64* readPosixTime);

    //PURPOSE
    //  Returns the number of messages retrieved by GetMessages()
    unsigned GetNumMessagesRetrieved() const;

    //PURPOSE
    //  Returns the total number of messages in the inbox.
    unsigned GetTotalMessages() const;

private:

    enum State
    {
        STATE_NONE,
        STATE_INITIALIZED   //Set when INIT_PARSER is called
    };

    bool Begin();

    static bool NextMessage(const parTreeNode* rootNode,
                            const parTreeNode** msgNode,
                            const char** message,
                            const char** messageId,
                            u64* createPosixTime,
                            u64* readPosixTime);

    parTree* m_ParserTree;
    const parTreeNode* m_MsgNode;
    unsigned m_NumMessagesRetrieved;
    unsigned m_TotalMessages;

    int m_MessageIndex;

    State m_State;
};

class rlInbox
{
public:

    //PURPOSE
    //  Posts a message to recipients' Inboxes
    //PARAMS
    //  localGamerIndex     - Local index of gamer making the call
    //  recipients          - Array of recipients
    //  numRecipients       - Number of recipients
    //  message             - The message
    //  tags                - Array of tags that can be used for query filtering
    //  numTags             - Number of tags
    //  ttlSeconds          - Time to live in seconds.  Pass zero for no expiry.
    static bool PostMessage(const int localGamerIndex,
                            const rlGamerHandle* recipients,
                            const unsigned numRecipients,
                            const char* message,
                            const char** tags,
                            const unsigned numTags,
                            const unsigned ttlSeconds);


	//PURPOSE
	//  Publishes a message to friends of the caller. Uses XB1/PS4 tasks that 
	//  can request a subset of their friends list for only ONLINE/INCONTEXT friends.
	//PARAMS
	//  localGamerIndex     - Local index of gamer making the call
	//  message             - The message
	//  tags                - Array of tags that can be used for query filtering
	//  numTags             - Number of tags
	//  ttlSeconds          - Time to live in seconds.  Pass zero for no expiry.
	static bool PostMessageToManyFriends(const int localGamerIndex, 
										const char* message, 
										const char** tags, 
										const unsigned numTags,
										const unsigned ttlSeconds);

    //PURPOSE
    //  Publishes a message to members of the given clan.
    //PARAMS
    //  localGamerIndex     - Local index of gamer making the call
    //  clanId              - ID of the clan to which to publish
    //  message             - The message
    //  tags                - Array of tags that can be used for query filtering
    //  numTags             - Number of tags
    //  ttlSeconds          - Time to live in seconds.  Pass zero for no expiry.
    static bool PostMessageToClan(const int localGamerIndex,
                                const s64 clanId,
                                const char* message,
                                const char** tags,
                                const unsigned numTags,
                                const unsigned ttlSeconds);

    //PURPOSE
    //  Retrieves messages from a user's inbox
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose inbox will be read
    //  offset              - Message offset into the inbox.  Messages are
    //                        ordered from most recent to least recent.
    //                        The most recent message is at offset zero.
    //  count               - Number of messages to read.
	//	tags				- tags to filter on (NULL for none)
	//  numTags				- number of tags in 'tags' list
    //  iter                - Upon completion will be initialized as a message iterator
    //  status              - Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate iter
    //  or status until the operation completes.
    //
    //  Be sure to call ReleaseResources() on the iterator when finished.
    static bool GetMessages(const int localGamerIndex,
                            const unsigned offset,
                            const unsigned count,
							const char** tags,
							const unsigned numTags,
                            rlInboxMessageIterator* iter,
                            netStatus* status);

    //PURPOSE
    //  Retrieves unread messages from a user's inbox
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose inbox will be read
    //  offset              - Message offset into the inbox.  Messages are
    //                        ordered from most recent to least recent.
    //                        The most recent message is at offset zero.
    //  count               - Number of messages to read.
	//	tags				- tags to filter on (NULL for none)
	//  numTags				- number of tags in 'tags' list
    //  iter                - Upon completion will be initialized as a message iterator
    //  status              - Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate iter
    //  or status until the operation completes.
    //
    //  Be sure to call ReleaseResources() on the iterator when finished.
    static bool GetUnreadMessages(const int localGamerIndex,
                                const unsigned offset,
                                const unsigned count,
							    const char** tags,
							    const unsigned numTags,
                                rlInboxMessageIterator* iter,
                                netStatus* status);

    //PURPOSE
    //  Deletes messages from a user's inbox
    //PARAMS
    //  localGamerIndex     - Local index of gamer whose messages will be deleted
    //  messageIds          - Array of IDs identifying messages to delete
	//  numIds				- number of IDs in 'messageIds' list
    static bool DeleteMessages(const int localGamerIndex,
							    const char** messageIds,
							    const unsigned numMessageIds);

    //PURPOSE
    //  Cancels a pending operation.
    static void Cancel(netStatus* status);
};

class rlInboxTagList
{
public:
	rlInboxTagList() {}
	void Add(const char*);
	void Clear();
	unsigned GetCount() const;
	const char** GetPointerList();
	explicit rlInboxTagList(const rlInboxTagList& that) 
		: m_tags(that.m_tags)
	{
		//set up the point list
		for (int i = 0; i < m_tags.GetCount(); ++i)
		{
			m_pointers.Grow(RL_INBOX_MAX_TAGS) = m_tags[i];
		}
		rlAssert(m_tags.GetCount() == m_pointers.GetCount());
	}
protected:
	struct rlInboxTag
	{
		char m_tag[RL_INBOX_MAX_TAGS_LENGTH];

		rlInboxTag& operator=(const char* tag)
		{
			safecpy(m_tag, tag);
			return *this;
		}
		rlInboxTag& operator=(const rlInboxTag& other)
		{
			safecpy(m_tag, other.m_tag);
			return *this;
		}
		bool operator==(const char* str) const;

		operator const char * () const { return m_tag; }

	};
	atArray<const char*> m_pointers;
	atArray<rlInboxTag> m_tags; //List of tags to apply to the next send inbox message
};

} //namespace rage

#endif  //RLINE_RLINBOX_H
