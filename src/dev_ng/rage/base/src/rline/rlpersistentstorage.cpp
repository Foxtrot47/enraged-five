// 
// rline/rlpersistentstorage.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "rlPersistentStorage.h"

#include "string/string.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/new.h"

#if __XENON
#include "system/xtl.h"
#endif  //__XENON

#if __PPU || __WIN32PC
#include "rlgamespysake.h"
#endif

namespace rage
{

extern sysMemAllocator* g_rlAllocator;

///////////////////////////////////////////////////////////////////////////////
// rlPersistentStorage::Worker
///////////////////////////////////////////////////////////////////////////////

rlPersistentStorage::Worker::Worker()
: m_Owner(0)
{
}

bool
rlPersistentStorage::Worker::Start(rlPersistentStorage* owner, const char* name)
{
    const bool success = this->rlWorker::Start(name);
    if(success)
    {
        m_Owner = owner;
    }
    return success;
}

bool
rlPersistentStorage::Worker::Stop()
{
    const bool success = this->rlWorker::Stop();
    m_Owner = 0;
    return success;
}

//private:

void
rlPersistentStorage::Worker::Perform()
{
    bool success = false;

    Command* cmd;

    {
        SYS_CS_SYNC(m_Owner->m_CsCmdQ);

        rlAssert(!m_Owner->m_CmdQ.empty());
        cmd = *m_Owner->m_CmdQ.begin();
        m_Owner->m_CmdQ.pop_front();
    }

    switch(cmd->m_CmdId)
    {
        case rlPersistentStorage::Command::CMD_QUERY:
            success = m_Owner->NativeQuery((QueryCmd*) cmd);
            break;
        case rlPersistentStorage::Command::CMD_UPLOAD:
            success = m_Owner->NativeUpload((UploadCmd*) cmd);
            break;
        case rlPersistentStorage::Command::CMD_DOWNLOAD:
            success = m_Owner->NativeDownload((DownloadCmd*) cmd);
            break;
        case rlPersistentStorage::Command::CMD_DELETE:
            success = m_Owner->NativeDelete((DeleteCmd*) cmd);
            break;
        default:
            rlAssert(false);
            break;
    }

    if(cmd->m_Status)
    {
        success ? cmd->m_Status->SetSucceeded() : cmd->m_Status->SetFailed();
    }

    cmd->~Command();
    g_rlAllocator->Free(cmd);
}

///////////////////////////////////////////////////////////////////////////////
// rlPersistentStorage::Command
///////////////////////////////////////////////////////////////////////////////
rlPersistentStorage::Command::Command(const CommandId cmdId,
                                netStatus* status)
: m_CmdId(cmdId)
, m_Status(status)
{
}

///////////////////////////////////////////////////////////////////////////////
// rlPersistentStorage::QueryCmd
///////////////////////////////////////////////////////////////////////////////
rlPersistentStorage::QueryCmd::QueryCmd(const rlGamerInfo& gamerInfo,
                                        const char* tableId,
                                        unsigned* numFileInfos,
                                        unsigned* numFileInfosLimit,
                                        FileInfo* fileInfos,
                                        const unsigned maxFileInfos,
                                        netStatus* status)
: Command(CMD_QUERY, status)
, m_GamerInfo(gamerInfo)
, m_TableId(tableId)
, m_NumFileInfos(numFileInfos)
, m_NumFileInfosLimit(numFileInfosLimit)
, m_FileInfos(fileInfos)
, m_MaxFileInfos(maxFileInfos)
{
    rlAssert(tableId && numFileInfos && numFileInfosLimit && fileInfos && maxFileInfos);
}

///////////////////////////////////////////////////////////////////////////////
// rlPersistentStorage::UploadCmd
///////////////////////////////////////////////////////////////////////////////
rlPersistentStorage::UploadCmd::UploadCmd(const rlGamerInfo& gamerInfo,
                                          const char* tableId,
                                          const void* buf,
                                          const unsigned bufSize,
                                          FileInfo* fileInfo,
                                          netStatus* status)
: Command(CMD_UPLOAD, status)
, m_GamerInfo(gamerInfo)
, m_TableId(tableId)
, m_Buf(buf)
, m_BufSize(bufSize)
, m_FileInfo(fileInfo)
{
    rlAssert(tableId && buf && bufSize && fileInfo);
}

///////////////////////////////////////////////////////////////////////////////
// rlPersistentStorage::DownloadCmd
///////////////////////////////////////////////////////////////////////////////
rlPersistentStorage::DownloadCmd::DownloadCmd(const rlGamerInfo& gamerInfo,
                                              const FileInfo& fileInfo,
                                              void* buf,
                                              const unsigned bufSize,
                                              unsigned* numBytesDownloaded,
                                              netStatus* status)
: Command(CMD_DOWNLOAD, status)
, m_GamerInfo(gamerInfo)
, m_FileInfo(fileInfo)
, m_Buf(buf)
, m_BufSize(bufSize)
, m_NumBytesDownloaded(numBytesDownloaded)
{
    rlAssert(buf && bufSize && numBytesDownloaded);
}

///////////////////////////////////////////////////////////////////////////////
// rlPersistentStorage::DeleteCmd
///////////////////////////////////////////////////////////////////////////////
rlPersistentStorage::DeleteCmd::DeleteCmd(const rlGamerInfo& gamerInfo,
                                          const char* tableId,
                                          const FileInfo& fileInfo,
                                          netStatus* status)
: Command(CMD_DELETE, status)
, m_GamerInfo(gamerInfo)
, m_TableId(tableId)
, m_FileInfo(fileInfo)
{
}

///////////////////////////////////////////////////////////////////////////////
// rlPersistentStorage
///////////////////////////////////////////////////////////////////////////////

rlPersistentStorage::rlPersistentStorage()
: m_Initialized(false)
{
}

rlPersistentStorage::~rlPersistentStorage()
{
    this->Shutdown();
}

bool
rlPersistentStorage::Init()
{
    bool success = false;

    rtry
    {
        rverify(!m_Initialized, catchall,);

        rlAssert(m_CmdQ.empty());

        rverify(m_Worker.Start(this, "rlPersistentStorage_Worker"),
                catchall,
                rlError(("Error starting rlPersistentStorage worker")));

        rlDebug2(("Started rlPersistentStorage worker"));

        success = m_Initialized = true;
    }
    rcatchall
    {
        if(m_Worker.IsRunning()){m_Worker.Stop();}
    }

    return success;
}

void
rlPersistentStorage::Shutdown()
{
    if(m_Initialized)
    {
        rlDebug2(("Stopping rlPersistentStorage worker..."));
        rlVerify(m_Worker.Stop());
        rlDebug2(("Stopped rlPersistentStorage worker"));

        while(!m_CmdQ.empty())
        {
            Command* cmd = *m_CmdQ.begin();
            m_CmdQ.pop_front();
            cmd->~Command();
            g_rlAllocator->Free(cmd);
        }

        m_Initialized = false;
    }
}

bool 
rlPersistentStorage::Query(const rlGamerInfo& gamerInfo,
                           const char* tableId,
                           unsigned* numFileInfos,    
                           unsigned* numFileInfosLimit, 
                           FileInfo* fileInfos,       
                           const unsigned maxFileInfos,
                           netStatus* status)
{
    rlDebug2(("rlPersistentStorage:Query: Attempting to query files owned by %s in table %s", 
              gamerInfo.GetName(), tableId));

    QueryCmd* cmd = (QueryCmd*)g_rlAllocator->Allocate(sizeof(QueryCmd), 0);

    new (cmd) QueryCmd(gamerInfo,
                       tableId,
                       numFileInfos,
                       numFileInfosLimit,
                       fileInfos,
                       maxFileInfos,
                       status);

    {
        SYS_CS_SYNC(m_CsCmdQ);
        m_CmdQ.push_back(cmd);
    }

    if(status){status->SetPending();}
    m_Worker.Wakeup();

    return true;
}

bool 
rlPersistentStorage::Upload(const rlGamerInfo& gamerInfo,
                            const char* tableId,
                            const void* buf,
                            const unsigned bufSize,
                            FileInfo* fileInfo,
                            netStatus* status)
{
    rlDebug2(("rlPersistentStorage:Upload: Attempting to upload %d bytes in table %s for %s", 
              bufSize, tableId, gamerInfo.GetName()));

    UploadCmd* cmd = (UploadCmd*)g_rlAllocator->Allocate(sizeof(UploadCmd), 0);

    new (cmd) UploadCmd(gamerInfo,
                        tableId,
                        buf,
                        bufSize,
                        fileInfo,
                        status);

    {
        SYS_CS_SYNC(m_CsCmdQ);
        m_CmdQ.push_back(cmd);
    }

    if(status){status->SetPending();}
    m_Worker.Wakeup();

    return true;
}

bool 
rlPersistentStorage::Download(const rlGamerInfo& gamerInfo,
                              const FileInfo& fileInfo,
                              void* buf,
                              const unsigned bufSize,
                              unsigned* numBytesDownloaded,
                              netStatus* status)
{
    rlDebug2(("rlPersistentStorage:Download: Attempting to download fileid %d for %s", 
              fileInfo.m_FileId, gamerInfo.GetName()));

    DownloadCmd* cmd = (DownloadCmd*)g_rlAllocator->Allocate(sizeof(DownloadCmd), 0);

    new (cmd) DownloadCmd(gamerInfo,
                          fileInfo,
                          buf,
                          bufSize,
                          numBytesDownloaded,
                          status);

    {
        SYS_CS_SYNC(m_CsCmdQ);
        m_CmdQ.push_back(cmd);
    }

    if(status){status->SetPending();}
    m_Worker.Wakeup();

    return true;
}

bool 
rlPersistentStorage::Delete(const rlGamerInfo& gamerInfo,
                            const char* tableId,
                            const FileInfo& fileInfo,
                            netStatus* status)
{
    rlDebug2(("rlPersistentStorage:Delete: Attempting to delete recordId %d in table %s for %s", 
              fileInfo.m_RecordId, tableId, gamerInfo.GetName()));

    DeleteCmd* cmd = (DeleteCmd*)g_rlAllocator->Allocate(sizeof(DeleteCmd), 0);

    new (cmd) DeleteCmd(gamerInfo,
                        tableId,
                        fileInfo,
                        status);

    {
        SYS_CS_SYNC(m_CsCmdQ);
        m_CmdQ.push_back(cmd);
    }

    if(status){status->SetPending();}
    m_Worker.Wakeup();

    return true;
}

//private:

#if __PPU || __WIN32PC

bool 
rlPersistentStorage::NativeQuery(const QueryCmd* cmd)
{
    //First, find out how many records the gamer currently has, and what the limit is.
    netStatus status;
    rlGamespySakeGetRecordLimitRequest limitReq;

    if(!limitReq.Start(g_rlGamespy.GetSake(),
                       cmd->m_TableId,
                       &status))
    {
        rlError(("rlPersistentStorage::NativeQuery: Failed to start limit request"));
        return false;
    }

    while(status.Pending())
    {
        sysIpcSleep(100);
    }

    if(!status.Succeeded())
    {
        rlError(("rlPersistentStorage::NativeUpload: limit request failed"));
        return false;
    }

    *cmd->m_NumFileInfos        = limitReq.GetNumOwned();
    *cmd->m_NumFileInfosLimit   = limitReq.GetLimitPerOwner();

    if(limitReq.GetNumOwned() > cmd->m_MaxFileInfos)
    {
        rlError(("rlPersistentStorage::NativeUpload: maxFileInfos(%d) not large enough to hold results(%d)",
                 cmd->m_MaxFileInfos,
                 limitReq.GetNumOwned()));
        return false;
    }

    //Query all the gamer's uploaded files.
    rlGamespySakeFileInfo* fileInfos = 
        (rlGamespySakeFileInfo*)g_rlAllocator->Allocate(sizeof(rlGamespySakeFileInfo) * limitReq.GetNumOwned(), 0);

    new (fileInfos) rlGamespySakeFileInfo[limitReq.GetNumOwned()];

    rlGamespySakeSearchForFilesRequest fileReq;

    if(!fileReq.Start(g_rlGamespy.GetSake(),
                      cmd->m_TableId,
                      cmd->m_NumFileInfos,
                      fileInfos,
                      cmd->m_MaxFileInfos,
                      &status))
    {
        rlError(("rlPersistentStorage::NativeQuery: Failed to start SearchForFiles request"));
        return false;
    }

    while(status.Pending())
    {
        sysIpcSleep(100);
    }

    if(!status.Succeeded())
    {
        rlError(("rlPersistentStorage::NativeUpload: SearchForFiles request failed"));
        g_rlAllocator->Free(fileInfos);
        return false;
    }

    //Convert the rlGamespySakeFileInfos to FileInfos.
    for(unsigned i = 0; i < *cmd->m_NumFileInfos; i++)
    {
        FileInfo& f = cmd->m_FileInfos[i];

        f.m_RecordId = fileInfos[i].m_RecordId;
        f.m_FileId   = fileInfos[i].m_FileId;
        f.m_FileSize = fileInfos[i].m_FileSize;
    }

    g_rlAllocator->Free(fileInfos);

    return true;
}

bool 
rlPersistentStorage::NativeUpload(const UploadCmd* cmd)
{
    //Upload the file, then create a record for it in the table.
    netStatus status;
    rlGamespySakeUploadRequest uploadReq;

    //static char remoteFilename[256];
    //formatf(remoteFilename, sizeof(remoteFilename), 
    //        "%s_%s_%d", 
    //        cmd->m_TableId,
    //        cmd->m_GamerInfo.GetName(),
    //        g_rlGamespy.GetProfileId());

    if(!uploadReq.Start(g_rlGamespy.GetSake(),
                        cmd->m_Buf,
                        cmd->m_BufSize,
                        "test_file", //remoteFilename,
                        &status))
    {
        rlError(("rlPersistentStorage::NativeUpload: Failed to start upload request"));
        return false;
    }

    while(status.Pending())
    {
        sysIpcSleep(100);
    }

    if(!status.Succeeded())
    {
        rlError(("rlPersistentStorage::NativeUpload: Upload request failed"));
        return false;
    }

    cmd->m_FileInfo->m_FileId   = uploadReq.GetUploadedFileId();
    cmd->m_FileInfo->m_FileSize = cmd->m_BufSize;

    //Now create a record that references the file.  Without this, Sake will 
    //automatically delete the file we uploaded in 24 hours.
    rlGamespySakeCreateRecordRequest createReq;

    SAKEField fields[2];

    fields[0].mName         = (char*)g_rlGamespySakeFieldFileId;
    fields[0].mType         = SAKEFieldType_INT;
    fields[0].mValue.mInt   = cmd->m_FileInfo->m_FileId;

    fields[1].mName         = (char*)g_rlGamespySakeFieldFileSize;
    fields[1].mType         = SAKEFieldType_INT;
    fields[1].mValue.mInt   = cmd->m_FileInfo->m_FileSize;

    if(!createReq.Start(g_rlGamespy.GetSake(),
                        cmd->m_TableId,
                        fields,
                        NELEM(fields),
                        &status))
    {
        rlError(("rlPersistentStorage::NativeUpload: Failed to start create request"));
        return false;
    }

    while(status.Pending())
    {
        sysIpcSleep(100);
    }

    cmd->m_FileInfo->m_RecordId = createReq.GetRecordId();

    rlDebug2(("rlPersistentStorage::NativeUpload: %s uploading file %d for %s (%u bytes, %d fileid, %d recordid)",
              status.Succeeded() ? "succeeded" : "failed",
              cmd->m_FileInfo->m_FileId,
              cmd->m_GamerInfo.GetName(),
              cmd->m_FileInfo->m_FileSize,
              cmd->m_FileInfo->m_FileId,
              cmd->m_FileInfo->m_RecordId));

    return status.Succeeded();
}

bool 
rlPersistentStorage::NativeDownload(const DownloadCmd* cmd)
{
    rlGamespySakeDownloadRequest req;
    netStatus status;

    if(!req.Start(g_rlGamespy.GetSake(),
                  cmd->m_FileInfo.m_FileId,
                  cmd->m_Buf,
                  cmd->m_BufSize,
                  &status))
    {
        rlError(("rlPersistentStorage::NativeDownload: Failed to start request"));
        return false;
    }

    while(status.Pending())
    {
        sysIpcSleep(100);
    }

    *cmd->m_NumBytesDownloaded = status.Succeeded() ? req.GetNumBytesDownloaded() : 0;

    rlDebug2(("rlPersistentStorage::NativeDownload: %s downloading file %d for %s (%u bytes)",
              status.Succeeded() ? "succeeded" : "failed",
              cmd->m_FileInfo.m_FileId,
              cmd->m_GamerInfo.GetName(),
              *cmd->m_NumBytesDownloaded));

    return status.Succeeded();
}

bool 
rlPersistentStorage::NativeDelete(const DeleteCmd* cmd)
{    
    rlGamespySakeDeleteRecordRequest req;
    netStatus status;

    if(!req.Start(g_rlGamespy.GetSake(),
                  (char*)cmd->m_TableId,
                  cmd->m_FileInfo.m_RecordId,
                  &status))
    {
        rlError(("rlPersistentStorage::NativeDelete: Failed to start request"));
        return false;
    }

    while(status.Pending())
    {
        sysIpcSleep(100);
    }

    rlDebug2(("rlPersistentStorage::NativeDelete: %s deleting file %d from %s for %s",
              status.Succeeded() ? "succeeded" : "failed",
              cmd->m_FileInfo.m_FileId,
              cmd->m_TableId,
              cmd->m_GamerInfo.GetName()));

    return status.Succeeded();
}

#else

bool 
rlPersistentStorage::NativeQuery(const QueryCmd* /*cmd*/)
{
    rlAssert(false && "Not implemented on this platform");
    return false;
}

bool 
rlPersistentStorage::NativeUpload(const UploadCmd* /*cmd*/)
{
    rlAssert(false && "Not implemented on this platform");
    return false;
}

bool 
rlPersistentStorage::NativeDownload(const DownloadCmd* /*cmd*/)
{
    rlAssert(false && "Not implemented on this platform");
    return false;
}

bool 
rlPersistentStorage::NativeDelete(const DeleteCmd* /*cmd*/)
{
    rlAssert(false && "Not implemented on this platform");
    return false;
}

#endif

}   //namespace rage
