// 
// rline/rlpresence.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPRESENCE_H
#define RLINE_RLPRESENCE_H

#include "atl/delegate.h"
#include "data/autoid.h"
#include "net/natdetector.h"
#include "net/nethardware.h"
#include "net/relay.h"
#include "net/tcp.h"        //for thread pool
#include "net/time.h"
#include "rlfriend.h"
#include "rlgamerinfo.h"
#include "rl.h"
#include "rlfriendsmanager.h"
#include "rlschema.h"
#include "rlsessioninfo.h"
#include "ros/rlros.h"
#include "scpresence/rlscpresence.h"
#include "scpresence/rlscpresencemessage.h"

#if RSG_NP
#include "rlnpcommon.h"
#endif

#if __STEAM_BUILD
#pragma warning(disable: 4265)
#include "../../3rdParty/Steam/public/steam/steam_api.h"
#pragma warning(error: 4265)
#endif // __STEAM_BUILD

#if __RGSC_DLL
#if defined(PostMessage)
// windows headers conflict
#undef PostMessage
#endif // defined(PostMessage)
#endif // __RGSC_DLL

namespace rage
{

/*
    rlPresence manages all aspects of presence for local gamers.

    To receive presence events, create and instance of rlPresence
    and register with it an instance of rlPresence::Delegate.  The
    the function bound to the delegate will be called when presence
    events occur, as long as Update() is called on a regular interval.
*/

//Presence event ids.
enum
{
    //network has become available or unavailable.
	//Peer address information should be refreshed for all local gamers.
    PRESENCE_EVENT_NETWORK_STATUS_CHANGED,

	//The local peer address has changed
	PRESENCE_EVENT_PEER_ADDRESS_CHANGED,
	
	//Gamer's sign-in status changed (either sign-in and/or online status).
    PRESENCE_EVENT_SIGNIN_STATUS_CHANGED,
    
    //The gamer's profile data changed.  Profile data should be refreshed
    //by reading the gamer's profile.  This event can also indicate the
    //gamer's name has changed, in which case the instance returned
    //by GetInfo() will contain the new gamer name.    
    PRESENCE_EVENT_PROFILE_CHANGED,

    //Local gamer accepted an invitation from another player while offline.
    //The game should present the gamer with a sign in dialog.
    PRESENCE_EVENT_INVITE_ACCEPTED_WHILE_OFFLINE,

	//Local gamer accepted an invitation from another player but cannot process
	//due to platform restrictions (NP unavailable / silver profile)
	//Invite was rejected. The game should present the gamer with a dialog with 
	//the failure 
	PRESENCE_EVENT_INVITE_UNAVAILABLE,

    //Local gamer accepted an invitation from another player.  The game should
    //immediately begin joining the specified session.  No confirmation is
    //necessary, as this action was initiated by the local gamer.
    PRESENCE_EVENT_INVITE_ACCEPTED,

    //Local gamer joined a game in progress (e.g. via the 360 system guide).
    //The game should immediately begin joining the specified session.  No
    //confirmation is necessary, as this action was initiated by the local gamer.
    PRESENCE_EVENT_JOINED_VIA_PRESENCE,

    //Local gamer received an invitation from another player to join a session.
    //As this action was not initiated by the invited gamer, games will usually
    //implement confirmation UI before joining the session.
    PRESENCE_EVENT_INVITE_RECEIVED,

	//Local gamer received an invitation from another player to join a party.
	//As this action was not initiated by the invited gamer, games will usually
	//implement confirmation UI before joining the party
	PRESENCE_EVENT_PARTY_INVITE_RECEIVED,

	//GameSessionReady event on the Xbox LIVE party. 
	PRESENCE_EVENT_GAME_SESSION_READY,

    //Local gamer received a game-specific message.  Messages are usually sent this way
    //when the game has no direct way to send packets to us, and instead must
    //go through the online service (ex. sending a message to a friend, when you
    //only have their online name and no host information).
    PRESENCE_EVENT_MSG_RECEIVED,

    //Platform service party membership changed. 
    //NOTE: This should not be confused with game-level party sessions,
    //      which are completely different.
    PRESENCE_EVENT_PARTY_CHANGED,

	//The number of game players available in the platform party changed.
	PRESENCE_EVENT_PARTY_MEMBERS_AVAILABLE,

	//Platform party has invalid session
	PRESENCE_EVENT_PARTY_SESSION_INVALID,

	//A ROS message was received
	PRESENCE_EVENT_SC_MESSAGE,

	//Local gamer's online permissions are invalid and require a refresh
	PRESENCE_EVENT_ONLINE_PERMISSIONS_INVALID,

	//Local gamer's online permissions have changed
	PRESENCE_EVENT_ONLINE_PERMISSIONS_CHANGED,

	//The list of muted users has changed
	PRESENCE_EVENT_MUTE_LIST_CHANGED,

#if RSG_ORBIS
	PRESENCE_EVENT_PLAY_TOGETHER_HOST,
#endif
};

#define PRESENCE_EVENT_COMMON_DECL( name )\
    static unsigned EVENT_ID() { return name::GetAutoId(); }\
    virtual unsigned GetId() const { return name::GetAutoId(); }

#define PRESENCE_EVENT_DECL( name, id )\
    AUTOID_DECL_ID( name, rage::rlPresenceEvent, id )\
    PRESENCE_EVENT_COMMON_DECL( name )

class rlPresence;

enum rlSignInStatusChangeFlags
{
	NONE = 0,
	SIGN_IN_CHANGE_DUPLICATE_LOGIN = BIT0,
	SIGN_IN_CHANGE_CONNECTION_LOST = BIT1,
};

class rlPresenceEventSigninStatusChanged;
class rlPresenceEventProfileChanged;
class rlPresenceEventFriendStatusChanged;
class rlPresenceEventFriendTitleChanged;
class rlPresenceEventFriendSessionChanged;
class rlPresenceEventInviteAccepted;
class rlPresenceEventInviteUnavailable;
class rlPresenceEventJoinedViaPresence;
class rlPresenceEventInviteReceived;
class rlPresenceEventPartyInviteReceived;
class rlPresenceEventGameSessionReady;
class rlPresenceEventMsgReceived;
class rlPresenceEventPartyChanged;
class rlPresenceEventScMessage;
class rlPresenceEventMuteListChanged;
class rlPresenceOnlinePermissionsInvalid;
class rlPresenceOnlinePermissionsChanged;
#if RSG_ORBIS
class rlPresenceEventPlayTogetherHost;
#endif

#if RSG_PC
class rlPcEvent;
class rlPc;
#elif RSG_DURANGO
class rlXblEvent;
class rlXbl;
#endif

//PURPOSE
//  Base class for all presence event classes.
class rlPresenceEvent
{
public:

    AUTOID_DECL_ROOT(rlPresenceEvent);
    PRESENCE_EVENT_COMMON_DECL(rlPresenceEvent);

	rlPresenceEvent()
	{
		m_GamerInfo.Clear();
		m_Event = this;
	}

    rlPresenceEvent(const rlGamerInfo& gamerInfo)
    : m_GamerInfo(gamerInfo)
    {
        m_Event = this;
    }

    virtual ~rlPresenceEvent() {}

    //PURPOSE
    //  Convenient access to the concrete event instance.
    union
    {
        rlPresenceEvent*							m_Event;
        rlPresenceEventSigninStatusChanged*			m_SigninStatusChanged;
        rlPresenceEventProfileChanged*				m_ProfileChanged;
        rlPresenceEventFriendStatusChanged*			m_FriendStatusChanged;
		rlPresenceEventFriendTitleChanged*			m_FriendTitleChanged;
		rlPresenceEventFriendSessionChanged*		m_FriendSessionChanged;
		rlPresenceEventInviteAccepted*				m_InviteAccepted;
		rlPresenceEventInviteUnavailable*			m_InviteUnavailable;
        rlPresenceEventJoinedViaPresence*			m_JoinedViaPresence;
        rlPresenceEventInviteReceived*				m_InviteReceived;
		rlPresenceEventPartyInviteReceived*			m_PartyInviteReceived;
		rlPresenceEventGameSessionReady*			m_GameSessionReady;
		rlPresenceEventMsgReceived*					m_MsgReceived;
		rlPresenceEventPartyChanged*				m_PartyChanged;
		rlPresenceEventScMessage*			        m_ScMessage;
		rlPresenceEventMuteListChanged*				m_MuteListChanged;
		rlPresenceOnlinePermissionsInvalid*			m_OnlinePermissionsInvalid;
		rlPresenceOnlinePermissionsChanged*			m_OnlinePermissionsChanged;
#if RSG_ORBIS
		rlPresenceEventPlayTogetherHost*			m_PlayTogetherHost;
#endif
	};

    rlGamerInfo m_GamerInfo;
};

//PURPOSE
//  Dispatched when a gamer's sign-in status changes.
class rlPresenceEventSigninStatusChanged : public rlPresenceEvent
{
    friend class rlPresence;

public:
    PRESENCE_EVENT_DECL(rlPresenceEventSigninStatusChanged, PRESENCE_EVENT_SIGNIN_STATUS_CHANGED);

    rlPresenceEventSigninStatusChanged(const rlGamerInfo& gamerInfo,
                                       const int statusChange,
                                       const unsigned flags)
    : rlPresenceEvent(gamerInfo)
    , m_StatusChange(statusChange)
    , m_SignInStatusChangeFlags(flags)
    {
    }

    //Only one of the following will return true for each instance
    //of a SigninStatusChanged event.
    bool SignedIn() const {return SIGNED_IN == m_StatusChange;}
    bool SignedOut() const {return SIGNED_OUT == m_StatusChange;}
    bool SignedOnline() const {return SIGNED_ONLINE == m_StatusChange;}
    bool SignedOffline() const {return SIGNED_OFFLINE == m_StatusChange;}

    bool WasDuplicateLogin() const { return (m_SignInStatusChangeFlags & rlSignInStatusChangeFlags::SIGN_IN_CHANGE_DUPLICATE_LOGIN) != 0; }
    bool WasConnectionLost() const { return (m_SignInStatusChangeFlags & rlSignInStatusChangeFlags::SIGN_IN_CHANGE_CONNECTION_LOST) != 0; }
    unsigned GetFlags() const { return m_SignInStatusChangeFlags; }

private:
    enum
    {        
        SIGNED_IN,      //Gamer signed in locally        
        SIGNED_OUT,     //Gamer signed out locally        
        SIGNED_ONLINE,  //Gamer went online        
        SIGNED_OFFLINE, //Gamer went offline
    };

    int m_StatusChange;
    unsigned m_SignInStatusChangeFlags : 2;
};

//PURPOSE
//  Dispatched when a local gamer's profile information changes.
//  A refresh of profile information should be issued.
class rlPresenceEventProfileChanged : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventProfileChanged, PRESENCE_EVENT_PROFILE_CHANGED);

    rlPresenceEventProfileChanged(const rlGamerInfo& gamerInfo)
    : rlPresenceEvent(gamerInfo)
    {
    }
};

//PURPOSE
//  Dispatched when the network becomes available or becomes unavailable.
//  A refresh of peer address information should be issued.
class rlPresenceEventNetworkStatusChanged : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventNetworkStatusChanged, PRESENCE_EVENT_NETWORK_STATUS_CHANGED);

    rlPresenceEventNetworkStatusChanged(const rlGamerInfo& gamerInfo)
    : rlPresenceEvent(gamerInfo)
	{
    }
};

//PURPOSE
//  The local peer address has changed
class rlPresenceEventPeerAddressChanged : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventPeerAddressChanged, PRESENCE_EVENT_PEER_ADDRESS_CHANGED);

	rlPresenceEventPeerAddressChanged(const rlGamerInfo& gamerInfo)
		: rlPresenceEvent(gamerInfo)
	{
	}
};

//PURPOSE
//  Dispatched when a local gamer accepts an invitation from another player.
//  The game should immediately begin joining the session specified by
//  m_SessionInfo.
class rlPresenceEventInviteAccepted : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventInviteAccepted, PRESENCE_EVENT_INVITE_ACCEPTED);

    rlPresenceEventInviteAccepted(
		const rlGamerInfo& invitee,
		const rlGamerHandle& inviter,
		const rlSessionInfo& sessionInfo,
		const unsigned inviteFlags)
    : rlPresenceEvent(invitee)
    , m_SessionInfo(sessionInfo)
    , m_Inviter(inviter)
	, m_InviteFlags(inviteFlags)
    {
    }

    rlSessionInfo m_SessionInfo;
    rlGamerHandle m_Inviter;
	unsigned m_InviteFlags;
};


//PURPOSE
//  Dispatched when a Game Session becomes available on our Xbox LIVE Party
//NOTES
//  Currently this event is Xbox One only.
class rlPresenceEventGameSessionReady : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventGameSessionReady, PRESENCE_EVENT_GAME_SESSION_READY);

	rlPresenceEventGameSessionReady(const rlGamerInfo& invitee,
									const rlSessionInfo& sessionInfo, 
									int numPartyPlayers, 
									rlGamerHandle* players,
									int origin)
		: rlPresenceEvent(invitee)
		, m_SessionInfo(sessionInfo)
		, m_NumPartyPlayers(numPartyPlayers)
		, m_Origin(origin)
	{
		for (int i = 0; i < numPartyPlayers; i++)
		{
			m_PartyPlayers[i] = players[i];
		}	
	}

	rlSessionInfo m_SessionInfo;
	rlGamerHandle m_PartyPlayers[RL_MAX_PARTY_SIZE];
	int m_NumPartyPlayers;
	int m_Origin;
};

//PURPOSE
//  Dispatched when a local gamer accepts an invitation from another player
//  while offline.
//  The game should present the gamer with a sign in dialog.
class rlPresenceEventInviteAcceptedWhileOffline : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventInviteAcceptedWhileOffline,
                        PRESENCE_EVENT_INVITE_ACCEPTED_WHILE_OFFLINE);

    rlPresenceEventInviteAcceptedWhileOffline(const rlGamerInfo& invitee)
    : rlPresenceEvent(invitee)
    {
    }
};

//PURPOSE
//  Dispatched gamer accepted an invitation from another player but NP is unavailable
//  Invite was rejected. The game should present the gamer with a dialog with 
//  the failure 
class rlPresenceEventInviteUnavailable : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventInviteUnavailable, PRESENCE_EVENT_INVITE_UNAVAILABLE);

	rlPresenceEventInviteUnavailable(const rlGamerInfo& invitee, const rlPresenceInviteUnavailableReason reason)
		: rlPresenceEvent(invitee)
		, m_Reason(reason)
	{
	}

	rlPresenceInviteUnavailableReason m_Reason;
};

//PURPOSE
//  Dispatched when a local gamer joins a game in progress (e.g. via the 360 system
//  guide).  The game should immediately begin joining the session specified by
//  m_SessionInfo.
class rlPresenceEventJoinedViaPresence : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventJoinedViaPresence, PRESENCE_EVENT_JOINED_VIA_PRESENCE);

	rlPresenceEventJoinedViaPresence(
		const rlGamerInfo& gamerInfo,
		const rlGamerHandle& targetHandle,
		const rlSessionInfo& sessionInfo,
		const unsigned inviteFlags)
    : rlPresenceEvent(gamerInfo)
	, m_SessionInfo(sessionInfo)
	, m_TargetHandle(targetHandle)
	, m_InviteFlags(inviteFlags)
    {
    }

    rlSessionInfo m_SessionInfo;
	rlGamerHandle m_TargetHandle;
	unsigned m_InviteFlags;
};

//PURPOSE
//  Dispatched when a local gamer has received an invitation to a session.
//  The gamer should be prompted to join the session.
class rlPresenceEventInviteReceived : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventInviteReceived, PRESENCE_EVENT_INVITE_RECEIVED);

    rlPresenceEventInviteReceived(const rlGamerInfo& gamerInfo,
                                  const rlSessionInfo& sessionInfo,
                                  const rlGamerHandle& inviter,
                                  const char* inviterName,
                                  const char* salutation)
    : rlPresenceEvent(gamerInfo)
    , m_SessionInfo(sessionInfo)
    , m_Inviter(inviter)
    , m_InviterName(inviterName)
    , m_Salutation(salutation)
    {
    }
    
    rlSessionInfo m_SessionInfo;
    rlGamerHandle m_Inviter;
    const char* m_InviterName;
    const char* m_Salutation;
};

// PURPOSE
// Dispatched when a invite is received for a party.
class rlPresenceEventPartyInviteReceived : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventPartyInviteReceived, PRESENCE_EVENT_PARTY_INVITE_RECEIVED);

	rlPresenceEventPartyInviteReceived(const rlGamerInfo& gamerInfo,
		const rlSessionInfo& sessionInfo,
		const rlGamerHandle& inviter,
		const char* inviterName,
		const char* salutation)
		: rlPresenceEvent(gamerInfo)
		, m_SessionInfo(sessionInfo)
		, m_Inviter(inviter)
		, m_InviterName(inviterName)
		, m_Salutation(salutation)
	{
	}

	rlGamerHandle m_Inviter;
	rlSessionInfo m_SessionInfo;
	const char* m_InviterName;
	const char* m_Salutation;
};

//PURPOSE
//  Dispatched when a local gamer has received a msg from another gamer.
class rlPresenceEventMsgReceived : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventMsgReceived, PRESENCE_EVENT_MSG_RECEIVED);

    rlPresenceEventMsgReceived(const rlGamerInfo& gamerInfo,
                               const char* senderName,
                               const void* data,
                               const unsigned size)
    : rlPresenceEvent(gamerInfo)
    , m_SenderName(senderName)
    , m_Data(data)
    , m_Size(size)
    {
    }

    const char* m_SenderName;
    const void* m_Data;
    unsigned m_Size;
};

//PURPOSE
//  Dispatched when the platform service party status has changed.
//NOTE
//  This event is currently on LIVE platforms only.
//  Currently the gamerInfo provided by the event will be invalid and should be ignored.
class rlPresenceEventPartyChanged : public rlPresenceEvent
{
public:
    PRESENCE_EVENT_DECL(rlPresenceEventPartyChanged, PRESENCE_EVENT_PARTY_CHANGED);

    rlPresenceEventPartyChanged(const rlGamerInfo& gamerInfo)
        : rlPresenceEvent(gamerInfo)
    {
    }
};

//PURPOSE
//  Dispatched when the platform service party has an invalid session
class rlPresenceEventPartySessionInvalid : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventPartySessionInvalid, PRESENCE_EVENT_PARTY_SESSION_INVALID);

	rlPresenceEventPartySessionInvalid(const rlGamerInfo& gamerInfo)
		: rlPresenceEvent(gamerInfo)
	{
	}
};

//PURPOSE
//  Dispatched when a SC message was received.
class rlPresenceEventScMessage : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventScMessage, PRESENCE_EVENT_SC_MESSAGE);

	rlPresenceEventScMessage(const rlGamerInfo& gamerInfo,
                            const rlScPresenceMessage& message,
                            const rlScPresenceMessageSender& sender)
		: rlPresenceEvent(gamerInfo)
		, m_Message(message)
        , m_Sender(sender)
	{
	}

	const rlScPresenceMessage& m_Message;
    const rlScPresenceMessageSender& m_Sender;
};

//PURPOSE
//  Dispatched when the list of muted users has changed
class rlPresenceEventMuteListChanged : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventMuteListChanged, PRESENCE_EVENT_MUTE_LIST_CHANGED);

	rlPresenceEventMuteListChanged(const rlGamerInfo& gamerInfo)
		: rlPresenceEvent(gamerInfo)
	{
	}
};

//PURPOSE
//  Dispatched when platform party has game players available.
class rlPresenceEventPartyMembersAvailable : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventPartyMembersAvailable, PRESENCE_EVENT_PARTY_MEMBERS_AVAILABLE);

	rlPresenceEventPartyMembersAvailable(const rlGamerInfo& gamerInfo, const rlGamerHandle* availablePlayers, const u64* lastInviteTime, int numAvailablePlayers)
		: rlPresenceEvent(gamerInfo)
	{
		rlAssertf(numAvailablePlayers <= RL_MAX_PARTY_SIZE, "Only RL_MAX_PARTY_SIZE available players allowed");
		if (numAvailablePlayers > RL_MAX_PARTY_SIZE)		
		{
			numAvailablePlayers = RL_MAX_PARTY_SIZE;	
		} 

		for (int i = 0; i < numAvailablePlayers; i++)
		{
			m_AvailablePlayers[i] = availablePlayers[i];
			m_LastInviteTimes[i] = lastInviteTime[i];
		}
		m_NumAvailablePlayers = numAvailablePlayers;
	}

	rlGamerHandle m_AvailablePlayers[RL_MAX_PARTY_SIZE];
	u64 m_LastInviteTimes[RL_MAX_PARTY_SIZE];
	int m_NumAvailablePlayers;
};

//PURPOSE
//  Dispatched when the local user's online permissions have become invalid (and require updating)
class rlPresenceOnlinePermissionsInvalid : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceOnlinePermissionsInvalid, PRESENCE_EVENT_ONLINE_PERMISSIONS_INVALID);

	rlPresenceOnlinePermissionsInvalid(const rlGamerInfo& gamerInfo)
		: rlPresenceEvent(gamerInfo)
	{
	}
};

//PURPOSE
//  Dispatched when the local user's online permissions have changed
class rlPresenceOnlinePermissionsChanged : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceOnlinePermissionsChanged, PRESENCE_EVENT_ONLINE_PERMISSIONS_CHANGED);

	rlPresenceOnlinePermissionsChanged(const rlGamerInfo& gamerInfo, const bool hadActivePromotion)
		: rlPresenceEvent(gamerInfo)
        , m_HadActivePromotion(hadActivePromotion)
	{
	}

    // indicates whether this change is due to a promotion period ending
    bool m_HadActivePromotion;
};

#if RSG_ORBIS
//PURPOSE
//  Dispatched when we receive a play together event from NP
class rlPresenceEventPlayTogetherHost : public rlPresenceEvent
{
public:
	PRESENCE_EVENT_DECL(rlPresenceEventPlayTogetherHost, PRESENCE_EVENT_PLAY_TOGETHER_HOST);

	rlPresenceEventPlayTogetherHost(int localGamerIndex, unsigned nInvitees, rlGamerHandle* invitees)
		: m_LocalGamerIndex(localGamerIndex)
		, m_nInvitees(nInvitees)
	{
		if(m_nInvitees > RL_MAX_PLAY_TOGETHER_GROUP)
		{
			m_nInvitees = RL_MAX_PLAY_TOGETHER_GROUP;
		}
		for(unsigned i = 0; i < nInvitees; i++)
		{
			m_Invitees[i] = invitees[i];
		}
	}

	int m_LocalGamerIndex;
	unsigned m_nInvitees;
	rlGamerHandle m_Invitees[RL_MAX_PLAY_TOGETHER_GROUP];
};
#endif

//////////////////////////////////////////////////////////////////////////
//  rlPresence
//////////////////////////////////////////////////////////////////////////
class rlPresence
{
    typedef atDelegator<void (const rlPresenceEvent*)> Delegator;
public:

	//After receiving a message notification we delay retrieving messages
	//in case more notifications come in, effectively aggregating multiple
	//notifications into a single GetMessages() call.
	static const unsigned DEFAULT_RETRIEVE_MESSAGE_DELAY_MS = 2500;

	//Maximum number of messages to retrieve when calling GetMessages()
	static const unsigned DEFAULT_MAX_PRESENCE_MESSAGES_TO_RETRIEVE = 10;

    //PURPOSE
    //  Event delegate type.
    //  The signature for event handlers is:
    //
    //  void OnEvent(rlPresence* presence, rlPresenceEvent* event)
    //
    typedef Delegator::Delegate Delegate;

    static bool Init();

    static void Shutdown();

    static bool IsInitialized();

    //PURPOSE
    //  Polls platform-specific API for events.
    //NOTES
    //  Should be called at least once every 100 ms.
    static void Update();

    //PURPOSE
    //  Returns true if the gamer is signed in to the local system.
    static bool IsSignedIn(const int localGamerIndex);

    //PURPOSE
    //  Returns true if the gamer is logged in to the platform's
    //  online service.  XBL for xbox, PSN for NP, ROS for all others.
    static bool IsOnline(const int localGamerIndex);

    //PURPOSE
    //  Retrieves the platform name for the gamer.
    //PARAMS
    //  localGamerIndex     - Index of local gamer
    //  name                - Name buffer
    //  sizeofName          - Size of the name buffer
    static bool GetName(const int localGamerIndex,
                        char* name,
                        const unsigned sizeofName);
    template<int SIZE>
    static bool GetName(const int localGamerIndex,
                        char (&name)[SIZE])
    {
        return GetName(localGamerIndex, name, SIZE);
    }

	//PURPOSE
    //  Retrieves the display name for the gamer.
    //PARAMS
    //  localGamerIndex     - Index of local gamer
    //  name                - Name buffer
    //  sizeofName          - Size of the name buffer
    static bool GetDisplayName(const int localGamerIndex,
							   char* name,
							   const unsigned sizeofName);
    template<int SIZE>
    static bool GetDisplayName(const int localGamerIndex,
							   char (&name)[SIZE])
    {
        return GetDisplayName(localGamerIndex, name, SIZE);
    }

    //PURPOSE
    //  Retrieves the gamer ID for the gamer.
    //PARAMS
    //  localGamerIndex     - Index of local gamer
    //  gamerId             - Will be populated with the gamer ID.
    //NOTES
    //  The gamer ID is a short lived ID to uniquely identify the
    //  gamer.  It will be different every time the game boots.
    //  Don't store this ID to disk!
    static bool GetGamerId(const int localGamerIndex,
                            rlGamerId* gamerId);

    //PURPOSE
    //  Retrieves the gamer handle associated with the local gamer.
    //PARAMS
    //  localGamerIndex     - Index of local gamer
    //  gamerHandle         - The gamer handle to be populated.
    static bool GetGamerHandle(const int localGamerIndex,
                                rlGamerHandle* gamerHandle);

    //PURPOSE
    //  Retrieves the native gamer handle of the local gamer who has a linked SC
    //  account matching scGamerHandleStr.
    //  scGamerHandleStr    - String representation of a SC gamer handle
    //  gamerHandle         - The local native gamer handle to be populated.
    static bool GetLocalGamerHandleFromScGamerHandle(const char* scGamerHandleStr,
                                                    rlGamerHandle* gamerHandle);

    //PURPOSE
    //  Retrieves the native gamer handle of the local gamer who has a linked SC
    //  account matching rockstarId.
    //  rockstarId      - Rockstar ID
    //  gamerHandle     - The local native gamer handle to be populated.
    static bool GetLocalGamerHandleFromRockstarId(const RockstarId rockstarId,
                                                    rlGamerHandle* gamerHandle);

    //PURPOSE
    //  Retrieves the gamer info associated with the local gamer.
    //PARAMS
    //  localGamerIndex     - Index of local gamer
    //  gamerInfo           - The gamer info to be populated.
    static bool GetGamerInfo(const int localGamerIndex,
                              rlGamerInfo* gamerInfo);

    //PURPOSE
    //  Retrieves the gamer info of the local gamer who has a linked SC
    //  account matching scGamerHandleStr.
    //  scGamerHandleStr    - String representation of a SC gamer handle
    //  gamerInfo           - The gamer info to be populated.
    static bool GetLocalGamerInfoFromScGamerHandle(const char* scGamerHandleStr,
                                                    rlGamerInfo* gamerInfo);

    //PURPOSE
    //  Retrieves the gamer info of the local gamer who has a linked SC
    //  account matching rockstarId.
    //  rockstarId  - Rockstar ID
    //  gamerInfo   - The gamer info to be populated.
    static bool GetLocalGamerInfoFromRockstarId(const RockstarId rockstarId,
                                                rlGamerInfo* gamerInfo);

    //PURPOSE
    //  Returns the age of the local gamer at the given index.
    //NOTES
    //  Correct age will be returned only after the player signs in to the
    //  online service (XBL, PSN, SC).
    //  On Xbox a title needs a special exemption to have access to the GetAge() API.
    //  If the player is not signed online, or the XBL exemption has not been granted,
    //  age will be negative.
    static int GetAge(const int localGamerIndex);

    //PURPOSE
    //  Returns the age group of the local gamer at the given index.
    //  See the rlAgeGroup enumeration.
    //NOTES
    //  The function is supported only on Xbox.  On other platforms
    //  RL_AGEGROUP_INVALID will be returned.
    //
    //  Which group a specific age falls into varies by region.
    //
    //  Be sure to check for RL_AGEGROUP_PENDING which indicates the
    //  age group is being retrieved.
    static rlAgeGroup GetAgeGroup(const int localGamerIndex);

    //PURPOSE
    //  Returns true if the local gamer is a guest.
    //PARAMS
    //  localGamerIndex     - Index of local gamer
    static bool IsGuest(const int localGamerIndex);

    //PURPOSE
    //  Returns the local index of the gamer, or -1 if the gamer is
    //  not signed in locally.
    static int GetLocalIndex(const rlGamerHandle& gh);

    //PURPOSE
    //  Returns true if the gamer is signed in locally.
    static bool IsLocal(const rlGamerHandle& gh);

#if __RGSC_DLL
	//PURPOSE
	//  Retrieves the raw friends list for the local gamer.
	//PARAMS
	//  localGamerIndex     - Index of local gamer
	//  friends             - OUT: Friends array.
	//  numFriends          - IN: Length of the friends array
	//                        OUT: Number of friends retrieved
	static bool GetFriendsArray(const int localGamerIndex,
								rlFriend** friends,
								unsigned* numFriends);

	

	//PURPOSE
	//  Informs rlPresence that the friend list has changed.
	static void SyncFriends();

	//PURPOSE
	//  Tells rlPresence to update a friend's online status.
	static void SetFriendIsOnline(const int localGamerIndex,
                                    rlFriend* f,
                                    const bool isOnline,
                                    const bool isPlayingSameTitle);

	//PURPOSE
	//  Returns true if the initial friend sync after signing online
	//  has completed (even if it was unsuccessful).
	static bool IsInitialFriendSyncDone();

	//PURPOSE
	//  Sets the duplicate sign in flag.
	static void SetKickedByDuplicateSignIn(const int localGamerIndex);
#endif

#if RSG_NP
	//PURPOSE
	//  Sends an invitation to a player to become your friend.
	static bool AddFriend(const SceNpId& npId,
						  const char* msg,
						  netStatus* status);

	//PURPOSE
	//  Sends an invitation to a player to become your friend.
	static bool AddFriend(const SceNpOnlineId& npOnlineId,
						  const char* msg,
						  netStatus* status);
#endif

#if RSG_NP || __STEAM_BUILD
    //PURPOSE
    //  Sets the presence blob for the currently online local gamer.
    //PARAMS
    //  blob        - Serialized data, only understandable to the game.
    //  blobSize    - Size of data in bytes.  Pass zero to clear the blob.
    //NOTES
    //    will actually be sent to PSN.
    //  - You can clear the presence blob by specifying a zero dataSize.
    static bool SetBlob(const void* blob,
                        const unsigned blobSize);

    //PURPOSE
    //  Sets the status string that's advertised in a player's rich
    //  presence.
    //PARAMS
    //  statusStr   - The status string.  Pass NULL to clear the string.
    //NOTES
    //  - Because only one account can be online at a time on the NP,
    //    no gamer needs to be specified.  The string applies to all local gamers.
    //  - NP only allows presence to be updated once per minute.  
    //    SetStatusString() updates the locally-cached presence data, which
    //    is uploaded once per minute.  You can call SetStatusString() as often
    //    as you like, but only the last call in the one-minute delay window
    //    will actually be sent to PSN.
    static bool SetStatusString(const char* statusStr);
#elif RSG_DURANGO
	//PURPOSE
	//  Sets a gamer's presence data (typically a string indicating what
	//  the gamer is currently doing).
	//PARAMS
	//  gamer       - The gamer.
	//  presenceStr - Id of presence template.
	//  status      - Can be polled for completion/success.
	//NOTES
	//  This is an asynchronous function.  Poll the status object for
	//  completion.
	static bool SetStatusString(const rlGamerInfo& gamerInfo, const char * presenceStr, netStatus* status);

	//PURPOSE
	//  Trigger Online permissions Changed.
	static void TriggerOnlinePermissionsChanged(const int gamerIndex);

#else
    //PURPOSE
    //  Sets a gamer's presence data (typically a string indicating what
    //  the gamer is currently doing).
    //PARAMS
    //  gamer       - The gamer.
    //  presenceId  - Id of presence template.
    //  schema      - Presence data.
    //  status      - Can be polled for completion/success.
    //NOTES
    //  This is an asynchronous function.  Poll the status object for
    //  completion.
    static bool SetStatusString(const rlGamerInfo& gamerInfo,
                                const int presenceId,
                                const rlSchema& schema,
                                netStatus* status);
#endif

    //PURPOSE
    //  Gets a gamer's rich presence string.
    //PARAMS
    //  gamer       - The gamer.
    //  buf			- The string will be copied to this buffer
    //  sizeOfBuf	- The number of bytes in the buffer
    //  status      - Can be polled for completion/success.
    //NOTES
    //  This is an asynchronous function.  Poll the status object for
    //  completion.
	static bool GetStatusString(const rlGamerInfo& gamerInfo,
					             char* buf,
					             const unsigned sizeOfBuf,
					             netStatus* status);

    //PURPOSE
    //  Adds a delegate that will be called with event notifications.
    static void AddDelegate(Delegate* dlgt);

    //PURPOSE
    //  Removes a delegate.
    static void RemoveDelegate(Delegate* dlgt);

    //PURPOSE
    //  The game must call this to generate
	//  a PRESENCE_EVENT_INVITE_ACCEPTED event when when an invite
    //  is accepted. 
    //PARAMS
    //  gamerIdx    - Index of the local gamer accepting/joining.
    //  sessionInfo - Session info for the session to be joined.
    //  inviter     - Inviter's gamer handle.
	static void NotifyInviteAccepted(const int gamerIdx,
                                    const rlSessionInfo& sessionInfo,
                                    const rlGamerHandle inviter);

    //PURPOSE
    //  Sets the local copy of the attribute.
    //  The new value will be synchronized with ROS a short
    //  time after.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  name            - Attribute name
    //  value           - Attribute value
	//	bNeedsCommitIfDirty - specifies whether the value needs to be committed
	//						as soon as it's dirty.  Used to deffer updates to attributes
	//						so they only update with other ones.
    //RETURNS
    //  True for success.  The method will succeed as long as the named
    //  attribute already exists, or it can be created without exceeding 
    //  RL_SCPRESENCE_MAX_ATTRIBUTES.
    static bool SetIntAttribute(const int localGamerIndex,
                                const char* name,
                                const s64 value,
								const bool bNeedsCommitIfDirty = true);
    static bool SetDoubleAttribute(const int localGamerIndex,
                                    const char* name,
									const double value,
									const bool bNeedsCommitIfDirty = true);
    static bool SetStringAttribute(const int localGamerIndex,
                                    const char* name,
									const char* value,
									const bool bNeedsCommitIfDirty = true);

	//PURPOSE
	//  Clears an attribute
	//PARAMS
	//  localGamerIndex - Index of local gamer making the call.
	//  name            - Attribute name
	//  value           - Attribute value						
	static bool ClearAttribute(const int localGamerIndex, const char* name);

	//PURPOSE
	//  Returns whether this attribute has been set or not
	//PARAMS
	//  localGamerIndex - Index of local gamer making the call.
	//  name            - Attribute name
	//  value           - Attribute value
	//						
	static bool HasAttribute(const int localGamerIndex, const char* name);
	static bool IsAttributeSet(const int localGamerIndex, const char* name);

    //PURPOSE
    //  Retrieves the local copy of the attribute.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  name            - Attribute name
    //  value           - Will be populated with the attribute value
    //RETURNS
    //  True for success, i.e. if the attribute exists.
    static bool GetIntAttribute(const int localGamerIndex,
                                const char* name,
                                s64* value);
    static bool GetDoubleAttribute(const int localGamerIndex,
                                    const char* name,
                                    double* value);
    static bool GetStringAttribute(const int localGamerIndex,
                                    const char* name,
                                    char* value,
                                    const unsigned sizeofValue);
    template<int SIZE>
    static bool GetStringAttribute(const int localGamerIndex,
                                    const char* name,
                                    char (&value)[SIZE])
    {
        return GetStringAttribute(localGamerIndex, name, value, SIZE);
    }

    //PURPOSE
    //  Retrieves attributes for the gamer identified by the gamer handle.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  gamerHandle - Identifies the gamer for whom to retrieve attributes.
    //  attrs       - Array of attributes to retrieve.  Each item in the
    //                array must have its Name member set.
    //  numAttrs    - Number of attributes in the attrs array
    //  status      - Can be polled for completion
    //NOTES
    //  Upon completion attributes not retrieved will have their Type members
    //  set to INVALID.
    //
    //  This is an asynchronous operation.  Don't deallocate attrs
    //  while the operation is pending.
    static bool GetAttributesForGamer(const int localGamerIndex,
                                    const rlGamerHandle& gamerHandle,
                                    rlScPresenceAttribute* attrs,
                                    const unsigned numAttrs,
                                    netStatus* status);

	//PURPOSE
	//  Retrieves attributes for multiple gamers identified by gamer handles.
	//PARAMS
	//  localGamerIndex - Index of local gamer making the call.
	//  gamerHandles    - Identifies the gamers for whom to retrieve attributes.
	//  numGamerHandles - Number of gamer handles in the gamerHandles array
	//					  Must be <= RLSC_PRESENCE_GET_ATTRS_MAX_GAMERS_PER_REQUEST.
	//  attrs           - 2D Array of attributes to retrieve for each gamer handle.
	//					  (Array of N arrays of M attributes, where N = numGamerHandles
	//					  and M = numAttrs.)
	//                    Each item in the array must have its Name and Type set.
	//					  The set of attributes to retrieve must be the same
	//					  for all gamer handles.
	//  numAttrs        - Number of attributes in the attrs array
	//					  Must be <= RLSC_PRESENCE_GET_ATTRS_MAX_ATTRS_PER_GAMER.
	//  status          - Can be polled for completion
	//NOTES
	//  Upon completion attributes not retrieved will have their Type members
	//  set to INVALID.
	//
	//  It is possible to have no attributes retrieved for some or all of the 
	//  specified gamer handles and still have the task succeed. Always check
	//  the Type of each returned attribute is not INVALID before using them.
	//
	//  This is an asynchronous operation.  Don't deallocate attrs
	//  while the operation is pending.
	static bool GetAttributesForGamers(const int localGamerIndex,
										const rlGamerHandle* gamerHandles,
										const unsigned numGamerHandles,
										rlScPresenceAttribute* attrs[],
										const unsigned numAttrs,
										netStatus* status);

    //PURPOSE
    //  Subscribe to one or more message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  channels    - List of channels to subscribe to.
    //  numChannels - Number of channels in the list.
    //  status      - Can be polled for completion
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate status
    //  while the operation is pending.
    static bool Subscribe(const int localGamerIndex,
                            const char** channels,
                            const unsigned numChannels);
    static bool Subscribe(const int localGamerIndex,
                            const char* channel)
    {
        return Subscribe(localGamerIndex, &channel, 1);
    }

    //PURPOSE
    //  Unsubscribe from one or more message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  channels    - List of channels to unsubscribe from.
    //  numChannels - Number of channels in the list.
    //  status      - Can be polled for completion
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate status
    //  while the operation is pending.
    static bool Unsubscribe(const int localGamerIndex,
                            const char** channels,
                            const unsigned numChannels);
    static bool Unsubscribe(const int localGamerIndex,
                            const char* channel)
    {
        return Unsubscribe(localGamerIndex, &channel, 1);
    }

    //PURPOSE
    //  Unsubscribes from all subscribed message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    static bool UnsubscribeAll(const int localGamerIndex);

    //PURPOSE
    //  Publish a message to one or more message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  channels    - Optional.  List of channels to publish to.
    //  numChannels - Number of channels in the list.
    //  filterName  - Optional.  Name of filter that can be
    //                used to perform additional filtering on
    //                message recipients.
    //  paramNameValueCsv   - Filter parameters in CSV format
    //                        Parameter names alternate with values
    //                        Names must be prefixed with '@', as in @playerid.
    //  message     - The message.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate status
    //  while the operation is pending.
    //EXAMPLE:
    //  //Publish a message to friends who are in my crew.
    //  char filterParams[256];
    //  formatf(filterParams, "@crewid,%d", myCrewId);
    //  char channel[256];
    //  formatf(channel, "friend_%s", myGamerHandleStr);
    //  rlPresence::Publish(myGamerIndex,
    //                      &channel,
    //                      1,
    //                      "CrewmatesOnline",
    //                      filterParams,
    //                      message,
    //                      &m_Status);
    static bool Publish(const int localGamerIndex,
                        const char** channels,
                        const unsigned numChannels,
                        const char* filterName,
                        const char* paramNameValueCsv,
                        const char* message);
    static bool Publish(const int localGamerIndex,
                        const char* channel,
                        const char* filterName,
                        const char* paramNameValueCsv,
                        const char* message)
    {
        return Publish(localGamerIndex, &channel, 1, filterName, paramNameValueCsv, message);
    }

    template<int SIZE>
    static bool Publish(const int localGamerIndex,
                        const char* channel,
                        const char* filterName,
                        const char* paramNameValueCsv,
                        const char (&message)[SIZE])
    {
        return Publish(localGamerIndex, &channel, 1, filterName, paramNameValueCsv, (const char*)message);
    }

	//PURPOSE
	//  Variant of Publish that automatically serializes
	//  an instance of T into the message.
	template<typename T>
	static bool Publish(const int localGamerindex,
                        const char** channels,
                        const unsigned numChannels,
                        const char* filterName,
                        const char* paramNameValueCsv,
                        const T& t)
	{
		char msgBuf[1024];
		return t.Export(msgBuf)
			    && Publish(localGamerindex, channels, numChannels, filterName, paramNameValueCsv, msgBuf);
	}
	template<typename T>
	static bool Publish(const int localGamerindex,
                        const char* channel,
                        const char* filterName,
                        const char* paramNameValueCsv,
                        const T& t)
	{
        return Publish(localGamerindex, &channel, 1, filterName, paramNameValueCsv, t);
	}

	//PURPOSE
	//  Publishes a message to all friends who are currently online
	//  and in the same title. Uses the XB1/PS4 async friends readers
	//	for "unlimited" friends lists to first retrieve your list of friends.
	//PARAMS
	//  localGamerIndex - Index of local gamer making the call.
	//  message     - The message.
	static bool PublishToManyFriends(const int localGamerIndex, const char* message);

    //PURPOSE
    //  Publishes a message to all crew mates who are currently online
    //  and in the same title.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  message     - The message.
    static bool PublishToCrew(const int localGamerIndex,
                                const char* message);

    template<int SIZE>
    static bool PublishToCrew(const int localGamerIndex,
                                const char (&message)[SIZE])
    {
        return PublishToCrew(localGamerIndex, (const char*)message);
    }

	//PURPOSE
	//  Variant of PublishToCrew that automatically serializes
	//  an instance of T into the message.
	template<typename T>
	static bool PublishToCrew(const int localGamerindex,
                                const T& t)
	{
		char msgBuf[1024];
		return t.Export(msgBuf)
			    && PublishToCrew(localGamerindex, msgBuf);
	}

    //PURPOSE
    //  Posts a message to the recipients' message queues.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  recipients      - Message recipients
    //  numRecipients   - Number of recipients
    //  message         - The message
    static bool PostMessage(const int localGamerindex,
                            const rlGamerHandle* recipients,
                            const unsigned numRecipients,
                            const char* message,
                            const unsigned ttlSeconds);

    template<int SIZE>
    static bool PostMessage(const int localGamerindex,
                            const rlGamerHandle* recipients,
                            const unsigned numRecipients,
                            const char (&message)[SIZE],
                            const unsigned ttlSeconds)
    {
        return PostMessage(localGamerindex, recipients, numRecipients, (const char*)message, ttlSeconds);
    }

	//PURPOSE
	//  Variant of PostMessage that automatically serializes
	//  an instance of T into the message.
	template<typename T>
	static bool PostMessage(const int localGamerindex,
		const rlGamerHandle* recipients,
		const unsigned numRecipients,
		const T& t,
		const unsigned ttlSeconds)
	{
		char msgBuf[1024];
		return t.Export(msgBuf)
			    && PostMessage(localGamerindex, recipients, numRecipients, msgBuf, ttlSeconds);
	}

    //PURPOSE
    //  Retrieves messages from the message queue owned by the gamer
    //  identified at localGamerIndex.
    //PARAMS
    //  localGamerIndex     - Index of local gamer making the call.
    //  numMessages         - Number of messages to retrieve
    //  msgIter             - Message iterator.  Will be populated with messages
    //  status              - Can be polled for completion
    static bool GetMessages(const int localGamerindex,
                            const unsigned numMessages,
                            rlScPresenceMessageIterator* msgIter,
                            netStatus* status);

    //PURPOSE
    //  Runs a predefined query on the presence database and returns
    //  the results.
    //  Results are JSON representations of presence records.  Example:
    //      {"_id":"XBL 1234567890","gtag":"FurBuddy","crewid":776655}
    //PARAMS
    //  localGamerIndex     - Index of local gamer making the call.
    //  queryName           - Name of predefined query
    //  paramNameValueCsv   - Query parameters in CSV format
    //                        Parameter names alternate with values
    //                        Names must be prefixed with '@', as in @playerid.
    //  offset              - Offset into results at which to begin returning
    //                        results.
    //  count               - Number of results to return.
    //  recordsBuf          - Buffer to hold records returned
    //  sizeofRecordsBuf    - Size of recordsBuf.
    //  records             - Array of char pointers that will be populated with
    //                        records returned from the query.  It must be large
    //                        enough to contain "count" results.  Each string in
    //                        the results array points to a section of the
    //                        resultsBuf memory buffer.
    //  numRecordsRetrieved - Upon completion will contain the number of records
    //                        actually retrieved.
    //  numRecords          - Upon completion will contain the number of records
    //                        parsed.  If recordsBuf is too small numRecords will be
    //                        less than numRecordsRetrieved.
    //  status              - Can be polled for completion
    //NOTES
    //  Records are limited to RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE bytes each.
    //  Each record is returned as a null terminated string.  The recordsBuf
    //  parameter should be large enough to hold
    //  (count * RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE) chars.  
    //  This is an asynchronous operation.  Do not deallocate recordBuf,
    //  records, numRecords, or status while the operation is pending.
    //EXAMPLE:
    //  //Find a player's session token.
    //  char queryParams[256];
    //  formatf(queryParams, "@gamerhandle,'%s'", gamerHandlStr);
    //  rlPresence::Query(0,
    //                      "FindSessionTokenByGamerHandle",
    //                      queryParams,
    //                      0,  //offset
    //                      1,  //count
    //                      m_PresQueryBuf,
    //                      sizeof(m_QueryBuf),
    //                      m_QueryRecords,
    //                      &m_NumQueryRecordsRetrieved,
    //                      &m_NumQueryRecords,
    //                      &m_Status);
    //
    //  //When it completes parse the results which look like "{'gstok':123456789}"
    //  RsonReader rr(m_QueryRecords[0], strlen(m_QueryRecords[0]);
    //  s64 gstok;
    //  rr.ReadInt64("gstok", gstok);
    //  
    static bool Query(const int localGamerIndex,
                        const char* queryName,
                        const char* paramNameValueCsv,
                        const int offset,
                        const int count,
                        char* recordsBuf,
                        const unsigned sizeofRecordsBuf,
                        char** records,
                        unsigned* numRecordsRetrieved,
                        unsigned* numRecords,
                        netStatus* status);

    //PURPOSE
    //  Runs a predefined query on the presence database and returns
    //  the number of records that match the query.
    //  queryName           - Name of predefined query
    //  paramNameValueCsv   - Query parameters in CSV format
    //                        Parameter names alternate with values
    //                        Names must be prefixed with '@', as in @playerid.
    //  count               - Upon completion will contain the number of records
    //                        that match the query.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate count,
    //  or status while the operation is pending.
    static bool QueryCount(const int localGamerIndex,
                        const char* queryName,
                        const char* paramNameValueCsv,
                        unsigned* count,
                        netStatus* status);

    static void CancelQuery(netStatus* status);

	//PURPOSE
	// Get/Sets the acting user context for PS4 and XB1. 
	//PARAMS
	// localGamerIndex - local index for the acting user
	static int GetActingUserIndex() { return m_ActingUserIndex; }
	static void SetActingUserIndex(const int localGamerIndex);

	// PURPOSE
	//	Get/Sets the acting gamer info
	//	This is the main gamer info set by the game client to determine which gamer info is to be used 
	//	on behalf of all local users while accessing live services. 
	static rlGamerInfo* GetActingGamerInfoPtr();
	static const rlGamerInfo& GetActingGamerInfo();
	static bool IsActingGamerInfoValid();
	static void SetActingGamerInfo(const rlGamerInfo& gamerInfo);
	static void ClearActingGamerInfo();

	//PURPOSE
	// Access to presence message settings
	static void SetPresenceMessageDelayTimeMs(const unsigned delayTimeMs);
	static void SetPresenceMessageMaxToRetrieve(const unsigned maxToRetrieve); 

    //PURPOSE
    // Debug option to set the user/connection to offline in order to
    // nudge the game to disconnect from MP.
    static void FakeOfflineDueToUplink(bool fakeOffline);

private:

    rlPresence();
    rlPresence(const rlPresence&);
    rlPresence& operator=(const rlPresence&);

    //PURPOSE
    //  Presence info for a gamer.
    struct GamerPresence
    {
        friend class rlPresence;

        GamerPresence()
        {
            this->Clear();
        }

    private:

        void Clear();

        rlGamerInfo m_GamerInfo;

        rlAgeGroup m_AgeGroup;
        int m_Age;

#if SC_PRESENCE
        //When notified we have messages start a timer.
        //Don't retrieve messages until the timer expires.
        //This way we aggregate multiple message notifications
        //into a single GetMessages() operation.
        netTimeout m_RetrieveMessagesTimer;

        rlScPresenceMessageIterator m_MsgIter;
        netStatus m_GetMessagesStatus;
#endif

#if RSG_ORBIS
		int m_UserServiceId;
#endif

        //This is used to defer processing of signin changes.
        //We need to do this on Live platforms because of a bug
        //in the XDK.  We also need to do this if we detect an online
        //state before the gamer is fully signed in and signin
        //refresh needs to be retried.
        enum
        {
            SIGNIN_RETRY_INTERVAL   = 1500,
        };

        unsigned m_RetrySigninTimeout;

		bool m_HasNetwork				: 1;
        bool m_IsSignedIn               : 1;
        bool m_IsOnline                 : 1;
        bool m_RetrySignin              : 1;
        bool m_ProcessProfileChange     : 1;
		bool m_RetrievingMessages		: 1;
		unsigned m_SignInChangeFlags	: 2;
    };

	static void RefreshNetworkStatus(const int localGamerIndex);
    static void RefreshSigninState(const int gamerIdx);

#if SC_PRESENCE
	static void UpdateMessagesSync(const int gamerIdx);
#endif

    static void UpdateScPresence(const int localGamerIndex);

    static GamerPresence* GetGamerPresence(const rlGamerHandle& gh);

    //Listens for events from the relay server
    static void OnRelayEvent(const netRelayEvent& event);
	static void OnNatDetectorEvent(const netNatEvent& event);
	static void OnNetHardwareEvent(const netHardwareEvent& event);
	static void OnRosEvent(const rlRosEvent& evt);

    static void OnScMessage(const int localGamerIndex,
                            const rlScPresenceMessage& message,
                            const rlScPresenceMessageSender& sender);

    static bool NativeInit();
    static void NativeShutdown();
    static void NativeUpdate();

    static bool NativeSetStatusString(const rlGamerInfo& gamerInfo,
                                        const int presenceId,
                                        const rlSchema& schema,
                                        netStatus* status);

    static bool NativeGetStatusString(const rlGamerInfo& gamerInfo,
						               char* buf,
						               const unsigned sizeOfBuf,
						               netStatus* status);

    static bool PopulateGamerInfo(const int localGamerIndex,
                                rlGamerInfo* gamerInfo);

	static Delegator m_Delegator;

    //Registered with netRelay to listen for events.
	static netRelay::Delegate m_RelayDelegate[NET_RELAYEVENT_NUM_EVENTS];
	static netNatDetector::Delegate m_NatDetectorDelegate;
	static netHardware::Delegate m_NetHardwareDelegate;
	static rlRos::Delegate m_RosDelegate;

    static GamerPresence m_GamerPresences[RL_MAX_LOCAL_GAMERS];

	static rlFriendsManager::Delegate m_FriendDelegate;

#if RSG_NP
    static rlNpEventDelegator::Delegate m_NpDlgt;
    static void OnNpEvent(rlNp* np, const rlNpEvent* evt);
#elif RSG_PC
	static void OnPcEvent(rlPc* pc, const rlPcEvent* evt);
#elif RSG_DURANGO
	static void OnXblEvent(rlXbl* xbl, const rlXblEvent* evt);
#endif

#if __STEAM_BUILD
	static void OnSteamPresenceInvite(const char * sessionBlob);

	class rlSteamPresence
	{
	public:
		rlSteamPresence();
		void CheckForBootableInvites();

	protected:
		STEAM_CALLBACK(rlSteamPresence, OnSteamPresenceCb, GameRichPresenceJoinRequested_t, m_SteamCallbackHandle);
	};

	static rlSteamPresence sm_SteamPresence;
#endif
	
    static bool m_Initialized;
    static bool m_HaveNetwork;
	static int m_ActingUserIndex;
	static rlGamerInfo m_ActingGamerInfo;
	static unsigned m_PresenceMessageDelayTimeMs;
	static unsigned m_PresenceMessageMaxToRetrieve;
};

//PURPOSE
//  Convenience class for setting presence attributes.
class rlPresenceAttributeHelper
{
public:

	//Named attributes
    static bool SetGamerTag(const int localGamerIndex, const char* gamerTag);
    static bool SetScNickname(const int localGamerIndex, const char* scNickname);
    static bool SetPeerAddress(const int localGamerIndex, const rlPeerAddress& peerAddr);
    static bool SetCrewId(const int localGamerIndex, const s64 crewId);
    static bool SetIsGameHost(const int localGamerIndex, const bool isHost);
    static bool SetGameSessionToken(const int localGamerIndex, const rlSessionToken& token);
	static bool SetIsGameJoinable(const int localGamerIndex, const bool isJoinable);
	static bool GetIsGameJoinable(const int localGamerIndex, bool& isJoinable);
	static bool SetIsPartyHost(const int localGamerIndex, const bool isHost);
    static bool SetPartySessionToken(const int localGamerIndex, const rlSessionToken& token);
	static bool SetPartySessionId(const int localGamerIndex, const u64 sessionId);
	static bool SetIsPartyJoinable(const int localGamerIndex, const bool isJoinable);
	static bool GetIsPartyJoinable(const int localGamerIndex, bool& isJoinable);
	static bool SetHostedSessionMatchIds(const int localGamerIndex, const char* hostedMatchIds);
	static bool SetSessionType(const int localGamerIndex, const s32 sessionType);

	//General purpose
	static bool SetBool(const int localGamerIndex, const bool value, const char* name);
	static bool GetBool(const int localGamerIndex, bool& value, const char* name);
	static bool SetSessionToken(const int localGamerIndex, const rlSessionToken& token, const char* name);
	static bool SetSessionId(const int localGamerIndex, const u64 sessionId, const char* name);
	static bool SetSessionInfo(const int localGamerIndex, const rlSessionInfo& sessionInfo, const char* name);
};

}   //namespace rage

#endif  //RLINE_RLPRESENCE_H
