// 
// rline/presence/rlPresenceQuery.h
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RL_PRESENCEQUERY_H
#define RL_PRESENCEQUERY_H

#include "rline/rlgamerinfo.h"
#include "rline/rlsessioninfo.h"

// describes a gamer as returned from the presence server
namespace rage
{
	class rlSessionDetail;

	class rlSessionQueryData
	{
	public:

		rlSessionQueryData() { Clear(); }

		void Reset(const rlGamerHandle& hGamer, const rlSessionInfo& hSessionInfo);
		void Clear();

		rlGamerHandle m_GamerHandle;
		rlSessionInfo m_SessionInfo;
	};

	class rlGamerQueryData
	{
		friend class rlFindGamersTask;
	public:

		rlGamerQueryData() { Clear(); }

		void Reset(const rlGamerHandle& hGamer, const char* szGamerTag DURANGO_ONLY(, const char* szDisplayName));
		void Clear();

		rlGamerHandle m_GamerHandle;

#if RSG_DURANGO
		//PURPOSE
		//  Returns true if the display name has been set.
		bool HasDisplayName() const
		{
			return m_DisplayName[0] != '\0';
		}
#endif

		const char* GetName() const
		{
#if RSG_DURANGO
			// if we don't have the display name, this will fall back to returning GetName()
			if(HasDisplayName())
			{
				return m_DisplayName;
			}
#endif

			return m_GamerTag;
		}

	private:
#if RSG_DURANGO
		char m_GamerTag[RL_MAX_NAME_BUF_SIZE];
		char m_DisplayName[RL_MAX_DISPLAY_NAME_BUF_SIZE];
#else
		char m_GamerTag[RL_MAX_NAME_BUF_SIZE];
#endif
	};

	enum eGamerState
	{
		eGamerState_Invalid = -1,
		eGamerState_InTitle,
		eGamerState_InTitleMP,
		eGamerState_OffTitle,
	};

	class rlPresenceQuery
	{
	public:
		// PURPOSE: Max sessions to retrieve in a rlPresenceQuery
		static const unsigned MAX_SESSIONS_TO_FIND = 20;

		// PURPOSE
		//	Gets session info for indicated gamers (only info for gamers in a session will be returned)
		// Returns TRUE if the task is created successfully.
		static bool GetSessionByGamerHandle(const int nLocalGamerIndex, rlGamerHandle* pHandles, unsigned nGamers, rlSessionQueryData* pResults, 
			const unsigned nMaxResults, unsigned* nNumResults, netStatus* pStatus);

		// PURPOSE
		//	Finds sessions for indicated gamers (duplicate sessions will be removed)
		// Returns TRUE if the task is created successfully
		static bool FindGamerSessions(const int nLocalGamerIndex, const unsigned nChannelID, unsigned nQueryTimeoutMs, unsigned nQueryMaxAttempts,
			bool bRetryOnQueryFail, rlGamerHandle* pHandles, unsigned nGamers, rlSessionDetail* pResults, unsigned* pNumHits,
			const unsigned nMaxResults, const unsigned nNumResultsBeforeEarlyOut, unsigned* nNumResults, netStatus* pStatus);

		// PURPOSE
		//	General purpose query function for finding gamers through presence
		// Returns TRUE if the task is created successfully
		static bool FindGamers(const rlGamerInfo& localInfo, const bool bExcludeLocal, const char* szQueryName, const char* pParams,
			rlGamerQueryData* pResults, const unsigned nMaxResults, unsigned* nNumResults, netStatus* pStatus);

		// PURPOSE
		//	Gets the state of gamers (in title / off title, in multi)
		// Returns TRUE if the task is created successfully
		static bool GetGamerState(const int nLocalGamerIndex, rlGamerHandle* pHandles, unsigned nGamers, eGamerState* pResults, netStatus* pStatus);

		// PURPOSE
		//	Cancels a presence query
		static bool Cancel(netStatus* pStatus);

	private:

		rlPresenceQuery();
		~rlPresenceQuery();
	};

} // namespace rage

#endif  // RL_PRESENCEQUERY_H
