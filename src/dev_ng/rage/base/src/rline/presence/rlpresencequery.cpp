// 
// network/Live/PresenceQuery.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "rlpresencequery.h"

#include "diag/channel.h"
#include "diag/seh.h"
#include "data/rson.h"
#include "net/task.h"
#include "rline/rlpresence.h"
#include "rline/rlsessionmanager.h"
#include "rline/rltask.h"
#include "atl/string.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, presencequery)
#undef __rage_channel
#define __rage_channel rline_presencequery

///////////////////////////////////////////////////////////////////////////////
// rlSessionByGamerHandleTask
///////////////////////////////////////////////////////////////////////////////
class rlSessionByGamerHandleTask : public netTask
{
public:

	RL_TASK_USE_CHANNEL(rline_presencequery);
	RL_TASK_DECL(rlSessionByGamerHandleTask);

	 rlSessionByGamerHandleTask();
	~rlSessionByGamerHandleTask();

	bool Configure(const int nLocalGamerIndex,
				   rlGamerHandle* pHandles,
				   unsigned nGamers,
				   rlSessionQueryData* pResults,
                   const unsigned nMaxResults,
				   unsigned* nNumResults);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* resultCode);

private:

	bool ParseResults();

	enum State
	{
		STATE_FIND,
		STATE_FINDING,
	};

	State m_State;
	netStatus m_MyStatus; 

	//
	int m_nLocalGamerIndex;
	rlSessionQueryData* m_pSessions;
    unsigned m_MaxResults;
	unsigned* m_NumResults;

	// results from presence query
	char m_QName[64];
	atString m_QParams;
	char m_QRecordsBuf[RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE * rlPresenceQuery::MAX_SESSIONS_TO_FIND];
	char* m_QRecords[rlPresenceQuery::MAX_SESSIONS_TO_FIND];
	unsigned m_NumQRecordsRetrieved;
	unsigned m_NumQRecords;
};

rlSessionByGamerHandleTask::rlSessionByGamerHandleTask()
: m_State(STATE_FIND)
, m_NumQRecordsRetrieved(0)
, m_NumQRecords(0)
, m_pSessions(NULL)
, m_MaxResults(0)
, m_NumResults(NULL)
{
	
}

rlSessionByGamerHandleTask::~rlSessionByGamerHandleTask()
{

}

bool rlSessionByGamerHandleTask::Configure(const int nLocalGamerIndex,
										   rlGamerHandle* pHandles,
										   unsigned nGamers,
										   rlSessionQueryData* pResults,
                                           const unsigned nMaxResults,
										   unsigned* nNumResults)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(nLocalGamerIndex), catchall, netTaskError("Invalid local gamer index"));
		rverify(pHandles, catchall, netTaskError("pHandles was Null"));
		rverify(pResults, catchall, netTaskError("pResults was Null"));
		rverify(nNumResults, catchall, netTaskError("nNumResults was Null"));

		m_nLocalGamerIndex = nLocalGamerIndex;
		m_MaxResults = nMaxResults;
		m_NumResults = nNumResults;
		m_pSessions = pResults;

		// validate gamers
		if(!netTaskVerify(nGamers <= rlPresenceQuery::MAX_SESSIONS_TO_FIND))
		{
			netTaskDebug("Too many gamers requested (%d). Capping to maximum (%d)", nGamers, rlPresenceQuery::MAX_SESSIONS_TO_FIND);
			nGamers = rlPresenceQuery::MAX_SESSIONS_TO_FIND;
		}

		// initialise parameter
		m_QParams = "@ghandle,";

		int numValidGamerHandles = 0;

		for(unsigned int i = 0; i < nGamers; i++)
		{
			// skip invalid handles
			if(!netTaskVerify(pHandles[i].IsValidForRos()))
			{
#if !__NO_OUTPUT
				rlUserIdBuf idBuf;
				netTaskWarning("Could not get session for gamer handle %s, gamer handle was invalid for ROS.", pHandles[i].ToUserId(idBuf));
#endif
				continue; 
			}

			char szHandle[256];
			pHandles[i].ToString(szHandle);

			atString aHandle(szHandle);
#if RSG_NP
			aHandle.Replace("\"", "\\\"");
#endif

			if(numValidGamerHandles > 0)
			{
				m_QParams += "&";
			}

			numValidGamerHandles++;

			m_QParams += "\"";
			m_QParams += aHandle;
			m_QParams += "\"";
		}

		rverify(numValidGamerHandles > 0, catchall, netTaskError("None of the gamer handles were valid"));

		safecpy(m_QName, "SessionByGamerHandle");
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlSessionByGamerHandleTask::OnCancel()
{
	if(m_MyStatus.Pending())
	{
		rlPresence::CancelQuery(&m_MyStatus);
	}
}

netTaskStatus rlSessionByGamerHandleTask::OnUpdate(int* /* resultCode */)
{
	if(WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

	switch(m_State)
	{
	case STATE_FIND:
		{
			netTaskDebug("rlSessionByGamerHandleTask :: Finding on '%s' : '%s'...", m_QName, m_QParams.c_str());

			// make presence query
			bool bSuccess = rlPresence::Query(m_nLocalGamerIndex,
											  m_QName,
											  m_QParams.c_str(),
											  0,
											  rlPresenceQuery::MAX_SESSIONS_TO_FIND,
											  m_QRecordsBuf,
											  sizeof(m_QRecordsBuf),
											  m_QRecords,
											  &m_NumQRecordsRetrieved,
											  &m_NumQRecords,
											  &m_MyStatus);

			// if we failed, bail out
			if(!bSuccess)
				return NET_TASKSTATUS_FAILED;

			// advance state
			m_State = STATE_FINDING;
		}
		break;

	case STATE_FINDING:
		{
			// check if the presence query was successful
			if(m_MyStatus.Succeeded())
			{
				netTaskDebug("rlSessionByGamerHandleTask :: Retrieved %d results", m_NumQRecords);

				if(ParseResults())
					return NET_TASKSTATUS_SUCCEEDED;
				else
				{
					netTaskError("FindGamers :: Error parsing results");
					return NET_TASKSTATUS_FAILED;
				}
			}
			else if(!m_MyStatus.Pending())
				return NET_TASKSTATUS_FAILED;
		}
		break;
	}

	return NET_TASKSTATUS_PENDING;
}

//private:

bool rlSessionByGamerHandleTask::ParseResults()
{
	unsigned nNumResults = 0;

	for(unsigned i = 0; i < m_NumQRecords && (nNumResults < rlPresenceQuery::MAX_SESSIONS_TO_FIND) && (nNumResults < m_MaxResults); ++i)
	{
		RsonReader rr;
		
		if(!RsonReader::ValidateJson(m_QRecords[i], istrlen(m_QRecords[i])))
		{
			netTaskError("rlSessionByGamerHandleTask :: Error validating '%s'", m_QRecords[i]);
			continue;
		}

		if(!rr.Init(m_QRecords[i], RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE))
		{
			netTaskError("rlSessionByGamerHandleTask :: Error parsing '%s'", m_QRecords[i]);
			continue;
		}

		// read in user ID
		char szUserID[32];
		if(!rr.ReadString("_id", szUserID))
		{
			netTaskError("rlSessionByGamerHandleTask :: Error parsing '%s' - no '_id'", m_QRecords[i]);
			continue;
		}

		// convert to gamer handle and check validity
		if(!m_pSessions[nNumResults].m_GamerHandle.FromString(szUserID) || !m_pSessions[nNumResults].m_GamerHandle.IsValid())
		{
			netTaskError("rlSessionByGamerHandleTask :: Invalid GamerHandle '%s'", szUserID);
			continue;
		}

		// get session info
		char szInfoBuffer[rlSessionInfo::TO_STRING_BUFFER_SIZE];
		if(!rr.ReadString("gsinfo", szInfoBuffer))
		{
			netTaskError("rlSessionByGamerHandleTask :: Error parsing '%s' - no 'gsinfo'", m_QRecords[i]);
			continue;
		}

        // check we had data
        if(strlen(szInfoBuffer) == 0)
        {
            netTaskError("rlSessionByGamerHandleTask :: Empty session info when parsing '%s'. %s not in session.", m_QRecords[i], szUserID);
            continue;
        }

        // convert to session info and check validity
        if(!m_pSessions[nNumResults].m_SessionInfo.FromString(szInfoBuffer, NULL, true) || !m_pSessions[nNumResults].m_SessionInfo.IsValid())
        {
            netTaskError("rlSessionByGamerHandleTask :: Invalid session info when parsing '%s'", m_QRecords[i]);
            continue;
        }

		// increment valid results
		nNumResults++;
	}

	// copy out number of valid results
	*m_NumResults = nNumResults;

	return true; 
}

///////////////////////////////////////////////////////////////////////////////
// rlFindGamerSessionsTask
///////////////////////////////////////////////////////////////////////////////
class rlFindGamerSessionsTask : public netTask
{
public:

	RL_TASK_USE_CHANNEL(rline_presencequery);
	RL_TASK_DECL(rlFindGamerSessionsTask);

	 rlFindGamerSessionsTask();
	~rlFindGamerSessionsTask();

	bool Configure(const int nLocalGamerIndex,
				   const unsigned nChannelID,
                   unsigned nQueryTimeoutMs,
                   unsigned nQueryMaxAttempts,
                   bool bRetryOnQueryFail,
                   rlGamerHandle* pHandles,
				   unsigned nGamers,
				   rlSessionDetail* pResults,
                   unsigned* pNumHits,
                   const unsigned nMaxResults,
				   const unsigned nNumResultsBeforeEarlyOut,
				   unsigned* nNumResults);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* resultCode);

private:

	bool ParseResults();

	enum State
	{
		STATE_FIND,
		STATE_FINDING,
		STATE_QUERY_DETAIL,
		STATE_QUERYING_DETAIL
	};

	State m_State;
	netStatus m_MyStatus; 

	//
	int m_nLocalGamerIndex;
	unsigned m_nChannelID;
	rlSessionDetail* m_pSessions;
    unsigned* m_pNumHits;
    unsigned m_MaxResults;
	unsigned* m_NumResults;

    // for session query
    unsigned m_nQueryTimeoutMs;
    unsigned m_nQueryMaxAttempts;
	unsigned m_nNumResultsBeforeEarlyOut;
    bool m_bRetryOnQueryFail;
    unsigned m_nPresenceRetries; 
    unsigned m_nQueryRetries; 

	// results from presence query
	unsigned m_nNumSessionInfos; 
	rlSessionInfo m_SessionInfos[rlPresenceQuery::MAX_SESSIONS_TO_FIND];
	char m_QName[64];
	atString m_QParams;
	char m_QRecordsBuf[RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE * rlPresenceQuery::MAX_SESSIONS_TO_FIND];
	char* m_QRecords[rlPresenceQuery::MAX_SESSIONS_TO_FIND];
	unsigned m_NumQRecordsRetrieved;
	unsigned m_NumQRecords;
};

rlFindGamerSessionsTask::rlFindGamerSessionsTask()
: m_State(STATE_FIND)
, m_NumQRecordsRetrieved(0)
, m_NumQRecords(0)
, m_pSessions(NULL)
, m_pNumHits(NULL)
, m_MaxResults(0)
, m_NumResults(NULL)
, m_bRetryOnQueryFail(false)
, m_nPresenceRetries(0)
, m_nQueryRetries(0)
{

}

rlFindGamerSessionsTask::~rlFindGamerSessionsTask()
{

}

bool rlFindGamerSessionsTask::Configure(const int nLocalGamerIndex,
										const unsigned nChannelID,
                                        unsigned nQueryTimeoutMs,
                                        unsigned nQueryMaxAttempts,
                                        bool bRetryOnQueryFail,
                                        rlGamerHandle* pHandles,
									    unsigned nGamers,
									    rlSessionDetail* pResults,
                                        unsigned* pNumHits,
                                        const unsigned nMaxResults,
										const unsigned nNumResultsBeforeEarlyOut,
									    unsigned* nNumResults)
{
	m_nLocalGamerIndex = nLocalGamerIndex;
	m_nChannelID = nChannelID;
	m_MaxResults = nMaxResults;
	m_NumResults = nNumResults;
	m_pSessions = pResults;
    m_pNumHits = pNumHits;
    m_nQueryTimeoutMs = nQueryTimeoutMs;
    m_nQueryMaxAttempts = nQueryMaxAttempts;
	m_nNumResultsBeforeEarlyOut = nNumResultsBeforeEarlyOut;
    m_bRetryOnQueryFail = bRetryOnQueryFail;
    m_nPresenceRetries = 0;
    m_nQueryRetries = 0;

	// initialise parameter
	m_QParams = "@ghandle,";

	int numValidHandles = 0;

	for(unsigned int i = 0; i < nGamers; i++)
	{
		// skip invalid handles
        if(!rlVerify(pHandles[i].IsValidForRos()))
		{
			netTaskError("pHandles[%d] is not valid for Ros", i)
            continue;
		}


		char szHandle[256];
		pHandles[i].ToString(szHandle, 256);

		atString aHandle(szHandle);
#if RSG_NP
		aHandle.Replace("\"", "\\\"");
#endif

		if(numValidHandles > 0)
		{
			m_QParams += "&";
		}

		numValidHandles++;

		m_QParams += "\"";
		m_QParams += aHandle;
		m_QParams += "\"";
	}

	if (!rlVerify(numValidHandles > 0))
	{
		netTaskError("None of the %d gamerHandles were valid", nGamers);
		return false;
	}

    // reset the number of hits
    for(unsigned i = 0; i < nMaxResults; i++)
        m_pNumHits[i] = 0;

	safecpy(m_QName, "SessionByGamerHandle");
	return true;
}

void rlFindGamerSessionsTask::OnCancel()
{
	if(m_MyStatus.Pending())
	{
		if(m_State == STATE_FINDING)
		{
			rlPresence::CancelQuery(&m_MyStatus);
		}
		else if(m_State == STATE_QUERYING_DETAIL)
		{
			rlSessionManager::Cancel(&m_MyStatus);
		}
	}
}

netTaskStatus rlFindGamerSessionsTask::OnUpdate(int* /* resultCode */)
{
	if(WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

	switch(m_State)
	{
	case STATE_FIND:
		{
			netTaskDebug("rlFindGamerSessionsTask :: Finding on '%s' : '%s'...", m_QName, m_QParams.c_str());

			// make presence query
			bool bSuccess = rlPresence::Query(m_nLocalGamerIndex,
		                            		  m_QName,
		                            		  m_QParams.c_str(),
		                            		  0,
		                            		  rlPresenceQuery::MAX_SESSIONS_TO_FIND,
		                            		  m_QRecordsBuf,
		                            		  sizeof(m_QRecordsBuf),
		                            		  m_QRecords,
		                            		  &m_NumQRecordsRetrieved,
		                            		  &m_NumQRecords,
		                            		  &m_MyStatus);

			// if we failed, bail out
			if(!bSuccess)
				return NET_TASKSTATUS_FAILED;

			// advance state
			m_State = STATE_FINDING;
		}
		break;

	case STATE_FINDING:
		{
			// check if the presence query was successful
			if(m_MyStatus.Succeeded())
			{
				netTaskDebug("rlFindGamerSessionsTask :: Retrieved %d results", m_NumQRecords);

				if(ParseResults())
                {
                    m_State = STATE_QUERY_DETAIL;
                }
				else
				{
					netTaskError("rlFindGamerSessionsTask :: Error parsing results");
					return NET_TASKSTATUS_FAILED;
				}
			}
			else if(!m_MyStatus.Pending())
				return NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_QUERY_DETAIL:
		{
			if(m_nNumSessionInfos > 0)
			{
				netTaskDebug("FindGamerSessions :: Querying details for %d sessions...", m_nNumSessionInfos);

				if(rlSessionManager::QueryDetail(RL_NETMODE_ONLINE,
													m_nChannelID,
													m_nQueryTimeoutMs,
													m_nQueryMaxAttempts,
													m_nNumResultsBeforeEarlyOut,
													true,
													m_SessionInfos,
													m_nNumSessionInfos,
													m_pSessions,
													m_NumResults,
													&m_MyStatus))
				{
                    m_State = STATE_QUERYING_DETAIL;
				}
                else 
                {
                    bool bRetry = false;
                    if(m_bRetryOnQueryFail)
                    {
                        static const unsigned MAX_LOCAL_RETRIES = 3;
                        if((m_nNumSessionInfos == 1) && m_SessionInfos[0].GetHostPeerAddress().IsLocal() && (m_nPresenceRetries < MAX_LOCAL_RETRIES))
                        {
                            netTaskDebug("FindGamerSessions :: Querying local session. Waiting for migration. Retrying.");
                            m_nPresenceRetries++;
                            m_State = STATE_FIND;

                            // flag that we're retrying
                            bRetry = true;
                        }
                    }
                        
                    // not retrying, exit the state
                    if(!bRetry)
                    {
                        netTaskError("rlFindGamerSessionsTask :: Error querying session configs");
                        return NET_TASKSTATUS_FAILED;
                    }
				}
			}
			// no sessions to query
			else 
				return NET_TASKSTATUS_SUCCEEDED;
		}
		break;

	case STATE_QUERYING_DETAIL:
		{
			// config query does the work for us, we just need to poll the result
			if(m_MyStatus.Succeeded())
            {
                // if we have specified a retry on query fail, reset the flag (only do this once) and
                // move back to the find state
                if(m_NumResults) 
                {
					static const unsigned MAX_DETAIL_RETRIES = 1;
					if(*m_NumResults == 0 && m_bRetryOnQueryFail && m_nQueryRetries < MAX_DETAIL_RETRIES)
					{
						netTaskDebug("FindGamerSessions :: Query for %d sessions failed. Retrying.", m_nNumSessionInfos);
						m_nQueryRetries++;
						m_State = STATE_FIND;

						// still pending
						return NET_TASKSTATUS_PENDING;
					}
					// we need to shuffle m_pNumHits to match any sessions that were not successfully queried
					else if(*m_NumResults != m_nNumSessionInfos)
					{
						for(unsigned int i = 0; i < *m_NumResults; i++)
						{
							for(unsigned int j = 0; j < m_nNumSessionInfos; j++)
							{
								if(m_pSessions[i].m_SessionInfo == m_SessionInfos[j])
								{
									// copy higher value into lower
									m_pNumHits[i] = m_pNumHits[j];
									break;
								}
							}
						}
					}
                }

				// succeeded
				return NET_TASKSTATUS_SUCCEEDED;
            }
			else if(!m_MyStatus.Pending())
				return NET_TASKSTATUS_FAILED;
		}
		break;
	}

	return NET_TASKSTATUS_PENDING;
}

//private:

bool rlFindGamerSessionsTask::ParseResults()
{
	// randomize the records
	std::random_shuffle(&m_QRecords[0], &m_QRecords[m_NumQRecords]);

	unsigned nNumResults = 0;

	for(unsigned i = 0; i < m_NumQRecords && (nNumResults < rlPresenceQuery::MAX_SESSIONS_TO_FIND) && (nNumResults < m_MaxResults); ++i)
	{
		RsonReader rr;

		if(!RsonReader::ValidateJson(m_QRecords[i], istrlen(m_QRecords[i])))
		{
			netTaskError("rlFindGamerSessionsTask :: Error validating '%s'", m_QRecords[i]);
			continue;
		}

		if(!rr.Init(m_QRecords[i], RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE))
		{
			netTaskError("rlFindGamerSessionsTask :: Error parsing '%s'", m_QRecords[i]);
			continue;
		}

		// read in user ID
		char szUserID[32];
		if(!rr.ReadString("_id", szUserID))
		{
			netTaskError("rlFindGamerSessionsTask :: Error parsing '%s' - no '_id'", m_QRecords[i]);
			continue;
		}

		// convert to gamer handle and check validity
		rlGamerHandle hGamer;
		if(!hGamer.FromString(szUserID) || !hGamer.IsValid())
		{
			netTaskError("rlFindGamerSessionsTask :: Invalid GamerHandle '%s'", szUserID);
			continue;
		}

		// get session info
		char szInfoBuffer[rlSessionInfo::TO_STRING_BUFFER_SIZE];
		if(!rr.ReadString("gsinfo", szInfoBuffer))
		{
			netTaskError("rlFindGamerSessionsTask :: Error parsing '%s' - no 'gsinfo'", m_QRecords[i]);
			continue;
		}

		// check we had data
		if(strlen(szInfoBuffer) == 0)
		{
			netTaskDebug("rlFindGamerSessionsTask :: Empty session info when parsing '%s'.", szUserID);
			continue;
		}

		// convert to session info and check validity (fail silently, this can pull data socially and cross version)
		rlSessionInfo hInfoTemp;
		if(!hInfoTemp.FromString(szInfoBuffer, NULL, true) || !hInfoTemp.IsValid())
		{
            netTaskDebug("rlFindGamerSessionsTask :: Invalid session 0x%016" I64FMT "x when parsing '%s'.", hInfoTemp.GetToken().m_Value, szUserID);
            continue;
		}

		// do not try sessions where the local player is the host
		if(hInfoTemp.GetHostPeerAddress().IsLocal())
		{
			netTaskDebug("rlFindGamerSessionsTask :: Local session info 0x%016" I64FMT "x", hInfoTemp.GetToken().m_Value);
			continue;
		}

		// do not try sessions we are already a member
		if(rlSessionManager::IsInSession(hInfoTemp))
		{
			netTaskDebug("rlFindGamerSessionsTask :: Already in session 0x%016" I64FMT "x", hInfoTemp.GetToken().m_Value);
			continue;
		}

		// check for duplicates
		bool bAlreadyExists = false;
		for(unsigned j = 0; j < nNumResults; j++)
		{
			if(m_SessionInfos[j] == hInfoTemp)
			{
                netTaskDebug("rlFindGamerSessionsTask :: Duplicate session 0x%016" I64FMT "x when parsing '%s'.", hInfoTemp.GetToken().m_Value, szUserID);
                
                // increment number of times this session has been found
                m_pNumHits[j]++;
                bAlreadyExists = true;
				break;
			}
		}

		// increment valid results
		if(!bAlreadyExists)
		{
            netTaskDebug("rlFindGamerSessionsTask :: Added session 0x%016" I64FMT "x when parsing '%s'.", hInfoTemp.GetToken().m_Value, szUserID);
            
            m_SessionInfos[nNumResults] = hInfoTemp;
            m_pNumHits[nNumResults]++;
			nNumResults++;
		}
	}

	// copy out number of valid results
	m_nNumSessionInfos = nNumResults;

	return true; 
}

///////////////////////////////////////////////////////////////////////////////
// rlFindGamersTask
///////////////////////////////////////////////////////////////////////////////
class rlFindGamersTask : public netTask
{
public:

	RL_TASK_USE_CHANNEL(rline_presencequery);
	RL_TASK_DECL(rlFindGamersTask);

	 rlFindGamersTask();
	~rlFindGamersTask();

	bool Configure(const rlGamerInfo& localInfo,
				   const bool bExcludeLocal,
				   const char* szQueryName,
				   const char* pParams,
				   rlGamerQueryData* pResults,
				   const unsigned nMaxResults,
				   unsigned* nNumResults);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* resultCode);

private:

	bool ParseResults();

#if RSG_DURANGO
	bool GetDisplayNames();
#endif

	static const unsigned MAX_GAMERS_TO_FIND = RL_MAX_GAMERS_PER_SESSION;
	
	enum State
	{
		STATE_FIND,
		STATE_FINDING,
#if RSG_DURANGO
		STATE_GET_DISPLAY_NAMES,
		STATE_GETTING_DISPLAY_NAMES,
#endif
	};

	State m_State;
	
	//
	rlGamerInfo m_LocalInfo;
	bool m_bExcludeLocal;
	rlGamerQueryData* m_pGamers;
	unsigned m_MaxResults;
	unsigned* m_NumResults;
	netStatus m_MyStatus;
	
	// results from presence query
	char m_QName[64];
	char m_QParams[256];
	char m_QRecordsBuf[RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE * MAX_GAMERS_TO_FIND];
	char* m_QRecords[MAX_GAMERS_TO_FIND];
	unsigned m_NumQRecordsRetrieved;
	unsigned m_NumQRecords;
};

rlFindGamersTask::rlFindGamersTask()
	: m_State(STATE_FIND)
	, m_bExcludeLocal(false)
	, m_NumQRecordsRetrieved(0)
	, m_NumQRecords(0)
	, m_pGamers(NULL)
	, m_MaxResults(0)
	, m_NumResults(NULL)
{
	m_LocalInfo.Clear();
}

rlFindGamersTask::~rlFindGamersTask()
{

}

bool rlFindGamersTask::Configure(const rlGamerInfo& localInfo,
								 const bool bExcludeLocal,
								 const char* szQueryName,
								 const char* pParams,
								 rlGamerQueryData* pResults,
								 const unsigned nMaxResults,
								 unsigned* nNumResults)
{
	if(!rlVerifyf(strlen(szQueryName) < sizeof(m_QName), "Query name '%s' is too long - max is %" SIZETFMT "u", szQueryName, sizeof(m_QName)-1))
		return false;
	
	if(!rlVerifyf(strlen(pParams) < sizeof(m_QParams), "Query params '%s' is too long - max is %" SIZETFMT "u", pParams, sizeof(m_QParams)-1))
		return false;
	
	if(!rlVerifyf(localInfo.IsValid(), "Invalid local player info!"))
		return false;

	m_LocalInfo = localInfo;
	m_bExcludeLocal = bExcludeLocal;
	m_pGamers = pResults;
	m_MaxResults = nMaxResults;
	m_NumResults = nNumResults;
	safecpy(m_QName, szQueryName);
	safecpy(m_QParams, pParams);
	return true;
}

void rlFindGamersTask::OnCancel()
{
	if(m_MyStatus.Pending())
	{
		if(m_State == STATE_FINDING)
		{
			rlPresence::CancelQuery(&m_MyStatus);
		}
#if RSG_DURANGO
		else if(m_State == STATE_GETTING_DISPLAY_NAMES)
		{
			g_rlXbl.GetProfileManager()->CancelPlayerNameRequest(&m_MyStatus);
		}
#endif
	}
}

netTaskStatus rlFindGamersTask::OnUpdate(int* /* resultCode */)
{
	if(WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

	switch(m_State)
	{
	case STATE_FIND:
		{
			netTaskDebug("FindGamers :: Finding on '%s' : '%s'...", m_QName, m_QParams);

			// make presence query
			bool bSuccess = rlPresence::Query(m_LocalInfo.GetLocalIndex(),
											  m_QName,
											  m_QParams,
											  0,
											  MAX_GAMERS_TO_FIND,
											  m_QRecordsBuf,
											  sizeof(m_QRecordsBuf),
											  m_QRecords,
											  &m_NumQRecordsRetrieved,
											  &m_NumQRecords,
											  &m_MyStatus);

			// if we failed, bail out
			if(!bSuccess)
				return NET_TASKSTATUS_FAILED;

			// advance state
			m_State = STATE_FINDING;
		}
		break;

	case STATE_FINDING:
		{
			// check if the presence query was successful
			if(m_MyStatus.Succeeded())
			{
				netTaskDebug("FindGamers :: Retrieved %d results", m_NumQRecords);

				if(ParseResults())
				{
#if RSG_DURANGO
					if(*m_NumResults > 0)
					{
						m_State = STATE_GET_DISPLAY_NAMES;
					}
					else
					{
						return NET_TASKSTATUS_SUCCEEDED;
					}
#else
					return NET_TASKSTATUS_SUCCEEDED;
#endif
				}
				else
				{
					netTaskError("FindGamers :: Error parsing results");
					return NET_TASKSTATUS_FAILED;
				}
			}
			else if(!m_MyStatus.Pending())
			{
				return NET_TASKSTATUS_FAILED;
			}
		}
		break;

#if RSG_DURANGO
	case STATE_GET_DISPLAY_NAMES:
		if(GetDisplayNames())
		{
			m_State = STATE_GETTING_DISPLAY_NAMES;
		}
		else
		{
			// if we fail to get display names, we'll fall back to using gamertags
			return NET_TASKSTATUS_SUCCEEDED;
		}
		break;

	case STATE_GETTING_DISPLAY_NAMES:
		if(!m_MyStatus.Pending())
		{
			netTaskDebug("Getting display names %s", m_MyStatus.Succeeded() ? "Succeeded" : "Failed");

			// if we fail to get display names, we'll fall back to using gamertags
			return NET_TASKSTATUS_SUCCEEDED;
		}

		break;
#endif
	}

	return NET_TASKSTATUS_PENDING;
}

//private:
#if RSG_DURANGO
bool rlFindGamersTask::GetDisplayNames()
{
	return g_rlXbl.GetProfileManager()->GetPlayerNames(m_LocalInfo.GetLocalIndex(), &m_pGamers[0].m_GamerHandle, sizeof(m_pGamers[0]), *m_NumResults, &m_pGamers[0].m_DisplayName, sizeof(m_pGamers[0]), NULL, 0, &m_MyStatus);
}
#endif

bool rlFindGamersTask::ParseResults()
{
	unsigned nNumResults = 0;

	for(unsigned i = 0; i < m_NumQRecords && (nNumResults < MAX_GAMERS_TO_FIND) && (nNumResults < m_MaxResults); ++i)
	{
		RsonReader rr;

		if(!RsonReader::ValidateJson(m_QRecords[i], istrlen(m_QRecords[i])))
		{
			netTaskError("FindGamers :: Error validating '%s'", m_QRecords[i]);
			continue;
		}

		if(!rr.Init(m_QRecords[i], RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE))
		{
			netTaskError("FindGamers :: Error parsing '%s'", m_QRecords[i]);
			continue;
		}

		// read in user ID
		char szUserID[32];
		if(!rr.ReadString("_id", szUserID))
		{
			netTaskError("FindGamers :: Error parsing '%s' - no '_id'", m_QRecords[i]);
			continue;
		}

		// convert to gamer handle and check validity
		if(!m_pGamers[nNumResults].m_GamerHandle.FromString(szUserID) || !m_pGamers[nNumResults].m_GamerHandle.IsValid())
		{
			netTaskError("FindGamers :: Invalid GamerHandle '%s'", szUserID);
			continue;
		}

		// if we are skipping the local player...
		if(m_bExcludeLocal && m_LocalInfo.GetGamerHandle() == m_pGamers[nNumResults].m_GamerHandle)
			continue;

		// read in gamertag
		if(!rr.ReadString("gtag", m_pGamers[nNumResults].m_GamerTag, sizeof(m_pGamers[nNumResults].m_GamerTag)))
		{
			netTaskError("FindGamers :: Error parsing '%s' - no 'gtag'", m_QRecords[i]);
			continue;
		}

#if RSG_DURANGO
		// in case display name lookup fails, fallback to gamertag
		safecpy(m_pGamers[nNumResults].m_DisplayName, m_pGamers[nNumResults].m_GamerTag);
#endif

		// increment valid results
		nNumResults++;
	}

	// copy out number of valid results
	*m_NumResults = nNumResults;

	return true; 
}

///////////////////////////////////////////////////////////////////////////////
// rlGetGamerStateTask
///////////////////////////////////////////////////////////////////////////////
class rlGetGamerStateTask : public netTask
{
public:

	RL_TASK_USE_CHANNEL(rline_presencequery);
	RL_TASK_DECL(rlGetGamerStateTask);

	 rlGetGamerStateTask();
	~rlGetGamerStateTask();

	bool Configure(const int nLocalGamerIndex,
				   rlGamerHandle* pHandles,
				   unsigned nGamers,
				   eGamerState* pResults);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* resultCode);

private:

	bool ParseResults();

	static const unsigned MAX_GAMERS_TO_FIND = RL_MAX_GAMERS_PER_SESSION;
	static const short MAX_NAME_LENGTH = 64;
	static const short MAX_PARAMS_LENGTH = 1024;

	enum State
	{
		STATE_FIND,
		STATE_FINDING,
	};

	State m_State;
	netStatus m_MyStatus; 

	//
	int m_nLocalGamerIndex;
	eGamerState* m_pGamerStates;
	unsigned m_nGamers;
	rlGamerHandle m_hGamers[MAX_GAMERS_TO_FIND];
	
	// results from presence query
	char m_QName[MAX_NAME_LENGTH];
	atString m_QParams;
	char m_QRecordsBuf[RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE * MAX_GAMERS_TO_FIND];
	char* m_QRecords[MAX_GAMERS_TO_FIND];
	unsigned m_NumQRecordsRetrieved;
	unsigned m_NumQRecords;
};

rlGetGamerStateTask::rlGetGamerStateTask()
: m_State(STATE_FIND)
, m_NumQRecordsRetrieved(0)
, m_NumQRecords(0)
, m_pGamerStates(NULL)
, m_nGamers(0)
{

}

rlGetGamerStateTask::~rlGetGamerStateTask()
{

}

bool rlGetGamerStateTask::Configure(const int nLocalGamerIndex,
									rlGamerHandle* pHandles,
									unsigned nGamers,
									eGamerState* pResults)
{
	m_nLocalGamerIndex = nLocalGamerIndex;
	m_pGamerStates = pResults;
	
	// validate gamers
	if(!netVerify(nGamers <= MAX_GAMERS_TO_FIND))
	{
		netTaskDebug("Too many gamers requested (%d). Capping to maximum (%d)", nGamers, MAX_GAMERS_TO_FIND);
		nGamers = MAX_GAMERS_TO_FIND;
	}

	m_nGamers = nGamers;

	// reserve and set the gamer handle header identifier
	m_QParams.Reserve(MAX_PARAMS_LENGTH);
	m_QParams = "@ghandle,";

	// create string to contain gamer handle string
	char szHandle[RL_MAX_GAMER_HANDLE_CHARS];

#if RSG_NP
	atString aHandle;
	aHandle.Reserve(RL_MAX_GAMER_HANDLE_CHARS);
#endif

	int numValidHandles = 0;

	// add all gamers
	for(unsigned i = 0; i < nGamers; i++)
	{
		// flag as 'in title'
		m_pGamerStates[i] = eGamerState_OffTitle;
		m_hGamers[i] = pHandles[i];

		// skip invalid handles
        if(!netTaskVerify(pHandles[i].IsValidForRos()))
		{
			netTaskWarning("pHandles[%d] is not valid for Ros", i);
            continue;
		}

		// convert to string to send up
		if(pHandles[i].ToString(szHandle) == NULL)
		{
			netTaskDebug("Failed to convert gamer handle to string at [%d]", i);
			continue;
		}

#if RSG_NP
		// use atString to easily replace slashes and copy back in
		aHandle = szHandle;
		aHandle.Replace("\"", "\\\"");
		safecpy(szHandle, aHandle.c_str());
#endif

		netTaskDebug("Adding gamer [%d]: %s", i, szHandle);

		if(numValidHandles > 0)
		{
			m_QParams += "&";
		}

		numValidHandles++;

		m_QParams += "\"";
		m_QParams += szHandle;
		m_QParams += "\"";
	}

	if (!netTaskVerify(numValidHandles > 0))
	{
		netTaskError("None of the %d gamer handles were valid", nGamers);
		return false;
	}

	// copy in query name
	safecpy(m_QName, "SessionByGamerHandle");

	return true;
}

void rlGetGamerStateTask::OnCancel()
{
	if(m_MyStatus.Pending())
	{
		rlPresence::CancelQuery(&m_MyStatus);
	}
}

netTaskStatus rlGetGamerStateTask::OnUpdate(int* /* resultCode */)
{
	if(WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

	switch(m_State)
	{
	case STATE_FIND:
		{
			netTaskDebug("Calling %s with parameters: %s", m_QName, m_QParams.c_str());
			
			// make presence query
			bool bSuccess = rlPresence::Query(m_nLocalGamerIndex,
											  m_QName,
											  m_QParams.c_str(),
											  0,
											  MAX_GAMERS_TO_FIND,
											  m_QRecordsBuf,
											  sizeof(m_QRecordsBuf),
											  m_QRecords,
											  &m_NumQRecordsRetrieved,
											  &m_NumQRecords,
											  &m_MyStatus);

			// if we failed, bail out
			if(!bSuccess)
				return NET_TASKSTATUS_FAILED;

			// advance state
			m_State = STATE_FINDING;
		}
		break;

	case STATE_FINDING:
		{
			// check if the presence query was successful
			if(m_MyStatus.Succeeded())
			{
				netTaskDebug("rlGetGamerStateTask :: Retrieved %d results", m_NumQRecords);

				if(ParseResults())
					return NET_TASKSTATUS_SUCCEEDED;
				else
				{
					netTaskError("rlGetGamerStateTask :: Error parsing results");
					return NET_TASKSTATUS_FAILED;
				}
			}
			else if(!m_MyStatus.Pending())
				return NET_TASKSTATUS_FAILED;
		}
		break;
	}

	return NET_TASKSTATUS_PENDING;
}

//private:

bool rlGetGamerStateTask::ParseResults()
{
	for(unsigned i = 0; i < m_NumQRecords; ++i)
	{
		RsonReader rr;

		if(!RsonReader::ValidateJson(m_QRecords[i], istrlen(m_QRecords[i])))
		{
			netTaskError("rlGetGamerStateTask :: Error validating '%s'", m_QRecords[i]);
			continue;
		}

		if(!rr.Init(m_QRecords[i], RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE))
		{
			netTaskError("rlGetGamerStateTask :: Error parsing '%s'", m_QRecords[i]);
			continue;
		}

		// read in user ID
		char szUserID[32];
		if(!rr.ReadString("_id", szUserID))
		{
			netTaskError("rlGetGamerStateTask :: Error parsing '%s' - no '_id'", m_QRecords[i]);
			continue;
		}

		// convert to gamer handle and check validity
		rlGamerHandle hGamer;
		if(!hGamer.FromString(szUserID) || !hGamer.IsValid())
		{
			netTaskError("rlGetGamerStateTask :: Invalid GamerHandle '%s'", szUserID);
			continue;
		}

        // does this player have a valid session info
        bool bHasValidSessionInfo = false;

        char szInfoBuffer[rlSessionInfo::TO_STRING_BUFFER_SIZE];
        if(rr.ReadString("gsinfo", szInfoBuffer))
        {
            // check we had data
            if(strlen(szInfoBuffer) > 0)
            {
                rlSessionInfo hInfoTemp;
                if(hInfoTemp.FromString(szInfoBuffer, NULL, true) && hInfoTemp.IsValid())
                    bHasValidSessionInfo = true; 
				else
				{
					netTaskError("rlGetGamerStateTask :: Failed to import session info '%s' from '%s'", szInfoBuffer, m_QRecords[i]);
				}
            }
        }

		// we don't care if the session is valid or not here, treat this player as 'online'
		bool bFound = false;
		for(unsigned j = 0; j < m_nGamers; j++)
		{
			if(m_hGamers[j] == hGamer)
			{
                m_pGamerStates[j] = bHasValidSessionInfo ? eGamerState_InTitleMP : eGamerState_InTitle;
				bFound = true; 
				break;
			}
		}

		// we didn't find this gamer handle
		if(!bFound)
		{
			netTaskError("rlGetGamerStateTask :: Did not find gamer handle '%s'", szUserID);
		}
	}

	return true; 
}

void rlGamerQueryData::Reset(const rlGamerHandle& hGamer, const char* szGamerTag DURANGO_ONLY(, const char* szDisplayName))
{
	m_GamerHandle = hGamer;
	safecpy(m_GamerTag, szGamerTag);
#if RSG_DURANGO
	safecpy(m_DisplayName, szDisplayName);
#endif
}

void rlGamerQueryData::Clear()
{
	m_GamerHandle.Clear();
	m_GamerTag[0] = '\0';
#if RSG_DURANGO
	m_DisplayName[0] = '\0';
#endif
}

void rlSessionQueryData::Reset(const rlGamerHandle& hGamer, const rlSessionInfo& hSessionInfo)
{
	m_GamerHandle = hGamer;
	m_SessionInfo = hSessionInfo;
}

void rlSessionQueryData::Clear()
{
	m_GamerHandle.Clear();
	m_SessionInfo.Clear();
}

bool rlPresenceQuery::GetSessionByGamerHandle(const int nLocalGamerIndex,
											rlGamerHandle* pHandles,
											unsigned nGamers,
											rlSessionQueryData* pResults,
											const unsigned nMaxResults,
											unsigned* nNumResults,
											netStatus* pStatus)
{
	bool success = false;

	rlDebug("rlPresenceQuery::GetSessionByGamerHandle");

	rlSessionByGamerHandleTask* pTask = NULL;
	rtry
	{
		rverify(netTask::Create(&pTask, pStatus),
				catchall,
				rlError("Error creating rlSessionByGamerHandleTask"));

		rverify(pTask->Configure(nLocalGamerIndex,
								 pHandles,
								 nGamers,
								 pResults,
								 nMaxResults,
								 nNumResults),
				catchall,
				rlError("Error configuring rlSessionByGamerHandleTask"));

		rverify(netTask::Run(pTask),
				catchall,
				rlError("Error scheduling rlSessionByGamerHandleTask"));

		success = true;
	}
	rcatchall
	{
		netTask::Destroy(pTask);
	}

	return success;
}

bool rlPresenceQuery::FindGamerSessions(const int nLocalGamerIndex,
									  const unsigned nChannelID,
                                      unsigned nQueryTimeoutMs,
                                      unsigned nQueryMaxAttempts,
                                      bool bRetryOnQueryFail,
									  rlGamerHandle* pHandles,
									  unsigned nGamers,
									  rlSessionDetail* pResults,
                                      unsigned* pNumHits,
                                      const unsigned nMaxResults,
									  const unsigned nNumResultsBeforeEarlyOut,
									  unsigned* nNumResults,
									  netStatus* pStatus)
{
	bool success = false;

	rlDebug("rlPresenceQuery::FindGamerSessions");

	rlFindGamerSessionsTask* pTask = NULL;
	rtry
	{
		rverify(netTask::Create(&pTask, pStatus),
				catchall,
				rlError("Error creating rlFindGamerSessions"));

		rverify(pTask->Configure(nLocalGamerIndex,
								 nChannelID,
                                 nQueryTimeoutMs,
                                 nQueryMaxAttempts,
                                 bRetryOnQueryFail,
								 pHandles,
								 nGamers,
								 pResults,
                                 pNumHits,
                                 nMaxResults,
								 nNumResultsBeforeEarlyOut,
								 nNumResults),
				catchall,
				rlError("Error configuring rlFindGamerSessions"));

		rverify(netTask::Run(pTask),
				catchall,
				rlError("Error scheduling rlFindGamerSessions"));

		success = true;
	}
	rcatchall
	{
		netTask::Destroy(pTask);
	}

	return success;
}

bool rlPresenceQuery::FindGamers(const rlGamerInfo& localInfo,
							   const bool bExcludeLocal,
							   const char* szQueryName,
							   const char* pParams,
							   rlGamerQueryData* pResults,
							   const unsigned nMaxResults,
							   unsigned* nNumResults,
							   netStatus* pStatus)
{
	bool success = false;

	rlDebug("rlPresenceQuery::FindGamers %s(%s)...", szQueryName, pParams);

	rlFindGamersTask* pTask = NULL;
	rtry
	{
		rverify(netTask::Create(&pTask, pStatus),
				catchall,
				rlError("Error creating rlFindGamersTask"));

		rverify(pTask->Configure(localInfo,
								 bExcludeLocal,
								 szQueryName,
								 pParams,
								 pResults,
								 nMaxResults,
								 nNumResults),
				catchall,
				rlError("Error configuring rlFindGamersTask"));

		rverify(netTask::Run(pTask),
				catchall,
				rlError("Error scheduling rlFindGamersTask"));

		success = true;
	}
	rcatchall
	{
		netTask::Destroy(pTask);
	}

	return success;
}

bool rlPresenceQuery::GetGamerState(const int nLocalGamerIndex, 
								  rlGamerHandle* pHandles, 
								  unsigned nGamers, 
								  eGamerState* pResults, 
								  netStatus* pStatus)
{
	bool success = false;

	rlDebug("rlPresenceQuery::GetGamerState :: %u gamers", nGamers);

	rlGetGamerStateTask* pTask = NULL;
	rtry
	{
		rverify(netTask::Create(&pTask, pStatus),
				catchall,
				rlError("Error creating rlGetGamerStateTask"));

		rverify(pTask->Configure(nLocalGamerIndex,
								 pHandles,
								 nGamers,
								 pResults),
				catchall,
				rlError("Error configuring rlGetGamerStateTask"));

		rverify(netTask::Run(pTask),
				catchall,
				rlError("Error scheduling rlGetGamerStateTask"));

		success = true;
	}
	rcatchall
	{
		netTask::Destroy(pTask);
	}

	return success;
}

bool rlPresenceQuery::Cancel(netStatus* pStatus)
{
	if(netTask::HasTask(pStatus))
	{
		netTask::Cancel(pStatus);
		return true;
	}
	return false;
}

}   //namespace rage