// 
// rline/rlfriendsreader.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlfriendsreader.h"
#include "net/task.h"
#include "rldiag.h"
#include "rlpresence.h"
#include "diag/seh.h"
#include "system/nelem.h"
#include "system/timer.h"

#if RSG_DURANGO
#include "durango/rlxbl_interface.h"
#elif RSG_NP
#include "rlnp.h"
#elif __RGSC_DLL
#include "scfriends/rlscfriends.h"
#elif RSG_PC
#include "rlpc.h"
#endif

#if RSG_ORBIS
#include <sdk_version.h>
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, friendsreader);
#undef __rage_channel
#define __rage_channel rline_friendsreader

#if RSG_PC && !__RGSC_DLL

class FriendsReaderTask : public netTask
{
public:

	NET_TASK_DECL(FriendsReaderTask);
	NET_TASK_USE_CHANNEL(rline_friendsreader);

	FriendsReaderTask()
		: m_State(STATE_BEGIN_READ)
		, m_MaxFriends(0)
		, m_TotalFriends(NULL)
		, m_FirstFriendIndex(0)
		, m_NumFriends(0)
		, m_Friends(NULL)
		, m_FriendsBuffer(NULL)
		, m_FriendFlags(0)
		, m_AsyncStatus(NULL)
	{
	}

	~FriendsReaderTask()
	{
		if(m_FriendsBuffer)
		{
			RL_FREE(m_FriendsBuffer);
			m_FriendsBuffer = NULL;
		}

		if(m_AsyncStatus)
		{
			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
	}

	bool Configure(const int /*localGamerIndex*/,
		unsigned* totalFriends,
		const unsigned firstFriendIndex,
		unsigned* numFriends,
		unsigned maxFriends,
		rlFriend* friends,
		int friendFlags)
	{
		m_MaxFriends = maxFriends;
		m_FirstFriendIndex = firstFriendIndex;
		m_NumFriends = numFriends;
		m_TotalFriends = totalFriends;
		*m_NumFriends = 0;
		m_Friends = friends;
		m_FriendFlags = friendFlags;
		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);
		if(m_AsyncStatus)
		{
			m_AsyncStatus->Cancel();
			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_BEGIN_READ:
			if(WasCanceled())
			{
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else if(BeginRead())
			{
				m_State = STATE_READING;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;

		case STATE_READING:
			if(WasCanceled())
			{
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else if(!m_AsyncStatus->Pending())
			{
				if(m_AsyncStatus->Succeeded())
				{
					status = NET_TASKSTATUS_SUCCEEDED;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		}

		if(NET_TASKSTATUS_PENDING != status)
		{
			this->Complete(status);
		}

		return status;
	}

private:

	bool BeginRead()
	{
		bool success = false;

		rtry
		{
			rlDebug("Reading friends...");
			rverify(g_rlPc.IsInitialized(), catchall, );

			rgsc::IPlayerManagerLatestVersion::PlayerListType listType = rgsc::IPlayerManagerLatestVersion::LIST_TYPE_FRIENDS;
			if(m_FriendFlags & rlFriendsReader::FRIENDS_JOINABLE_SESSION_PRESENCE)
			{
				listType = rgsc::IPlayerManagerLatestVersion::LIST_TYPE_FRIENDS_WITH_PRESENCE;
			}

			u32 numBytesRequired = 0;
			void* handle = NULL;
			HRESULT hr = g_rlPc.GetPlayerManager()->CreatePlayerEnumerator(rgsc::IID_IPlayerListLatestVersion,
				0,
				listType,
				m_FirstFriendIndex,
				m_MaxFriends,
				&numBytesRequired,
				&handle);

			rverify(SUCCEEDED(hr), catchall, );

			m_FriendsBuffer = RL_ALLOCATE(rlFriendReader, numBytesRequired);
			rverify(m_FriendsBuffer, catchall, rlError("Error allocating buffer for player list"));

			hr = g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_AsyncStatus));
			rverify(SUCCEEDED(hr), catchall, );

			m_ListInterface = NULL;
			hr = g_rlPc.GetPlayerManager()->EnumeratePlayers(handle,
				m_FriendsBuffer,
				numBytesRequired,
				&m_ListInterface,
				m_AsyncStatus);
			rverify(SUCCEEDED(hr), catchall, );
			success = true;
		}
		rcatchall
		{

		}

		return success;
	}

	void Complete(const netTaskStatus status)
	{
		if(NET_TASKSTATUS_SUCCEEDED == status && !WasCanceled())
		{
			rgsc::IPlayerListLatestVersion* list = (rgsc::IPlayerListLatestVersion*)m_ListInterface;

			*m_NumFriends = list->GetNumPlayers();
			*m_TotalFriends = g_rlPc.GetPlayerManager()->GetTotalNumFriends();

			if(!rlVerify(list->GetNumPlayers() <= m_MaxFriends))
			{
				*m_NumFriends = m_MaxFriends;
				*m_TotalFriends = m_MaxFriends;
			}

			for(unsigned i = 0; i < *m_NumFriends; i++)
			{
				rgsc::IPlayerLatestVersion* rgscPlayer = list->GetPlayer(i);

				rlScFriend scFriend;
				scFriend.m_RockstarId = rgscPlayer->GetRockstarId();
				safecpy(scFriend.m_Nickname, rgscPlayer->GetName());
				rlAssertf(rgscPlayer->GetRelationship() == rgsc::IPlayerLatestVersion::RELATIONSHIP_FRIEND, "Expected relationship to be RELATIONSHIP_FRIEND but got %u", rgscPlayer->GetRelationship());
				scFriend.m_Relationship = RLSC_RELATIONSHIP_FRIEND;
				scFriend.m_IsOnline = rgscPlayer->IsOnline();
				scFriend.m_IsPlayingSameTitle = rgscPlayer->IsPlayingSameTitle();

				unsigned rgscFlags = 0;
				rgsc::IPlayerV2* v2 = NULL;
				if (SUCCEEDED(rgscPlayer->QueryInterface(rgsc::IID_PlayerV2, (void**)&v2)))
				{
					rgscFlags = rgscPlayer->GetFlags();
				}

				unsigned rlFlags = 0;

				if(m_FriendFlags & rlFriendsReader::FRIENDS_JOINABLE_SESSION_PRESENCE)
				{
					if(rgscFlags & rgsc::IPlayerLatestVersion::PLAYER_FLAG_IN_JOINABLE_SESSION)
					{
						rlFlags = rlFriend::FLAG_IS_IN_JOINABLE_SESSION;
					}
				}
				// If we didn't request updated joinable session presence, maintain the session presence
				else if (m_Friends[i].IsInSession())
				{
					rlFlags = rlFriend::FLAG_IS_IN_JOINABLE_SESSION;
				}

				rlDebug("Read friend %s (%"I64FMT"d), online: %d, same title: %d, relationship: %d, flags: %d, joinable: %d", scFriend.m_Nickname, 
						scFriend.m_RockstarId, scFriend.m_IsOnline, scFriend.m_IsPlayingSameTitle, scFriend.m_Relationship, rlFlags, 
						rlFlags & rgsc::IPlayerLatestVersion::PLAYER_FLAG_IN_JOINABLE_SESSION);

				m_Friends[i].Reset(scFriend, rlFlags);
			}
		}

		if(m_FriendsBuffer)
		{
			RL_FREE(m_FriendsBuffer);
			m_FriendsBuffer = NULL;
		}

		if(m_AsyncStatus)
		{
			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
	}

	enum State
	{
		STATE_BEGIN_READ,
		STATE_READING,
	};

	State m_State;
	rgsc::IPlayerList* m_ListInterface;
	rgsc::IAsyncStatusLatestVersion* m_AsyncStatus;
	netStatus m_MyStatus;
	unsigned m_MaxFriends;
	unsigned m_FirstFriendIndex;
	void* m_FriendsBuffer;
	unsigned* m_NumFriends;
	unsigned* m_TotalFriends;
	rlFriend* m_Friends;
	int m_FriendFlags;
};

#elif RSG_ORBIS
class rlNpFriendsReaderTask : public netTask
{
public:

    NET_TASK_DECL(rlNpFriendsReaderTask);
    NET_TASK_USE_CHANNEL(rline_friendsreader);

    rlNpFriendsReaderTask()
        : m_State(STATE_BEGIN_READ)
        , m_LocalGamerIndex(-1)
        , m_MaxFriends(0)
        , m_FirstFriendIndex(0)
        , m_NumFriends(0)
        , m_FriendsPtr(NULL)
        , m_NumFriendsPtr(NULL)
    {

    }

    bool Configure(const int localGamerIndex,
					unsigned * totalFriends,
                    const unsigned firstFriendIndex,
                    unsigned * numFriends,
                    rlFriend* friends,
					int maxFriends,
					int friendFlags)
    {
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index"));
			rverify(totalFriends, catchall, rlError("NULL total friends"));
			rverify(numFriends, catchall, rlError("NULL number of friends"));
			rverify(friends, catchall, rlError("NULL friends array"));

			m_LocalGamerIndex = localGamerIndex;
			m_TotalFriendsPtr = totalFriends,
			m_MaxFriends = maxFriends;
			m_FirstFriendIndex = firstFriendIndex;
			m_NumFriends = *numFriends = 0;
			m_FriendsPtr = friends;
			m_NumFriendsPtr = numFriends;
			m_FriendFlags = friendFlags;
			return true;
		}
		rcatchall
		{
			return false;
		}
    }

    virtual void OnCancel()
    {
		m_GetFriendsListWorkItem.Cancel();
        netTaskDebug("Canceled while in state %d", m_State);
    }

    virtual netTaskStatus OnUpdate(int* /*resultCode*/)
    {
		netTaskStatus status = NET_TASKSTATUS_PENDING;

        switch(m_State)
        {
        case STATE_BEGIN_READ:
            if(WasCanceled())
            {
                netTaskDebug("Canceled - bailing...");
                status = NET_TASKSTATUS_FAILED;
            }
            else if(BeginRead())
            {
                m_State = STATE_READING;
            }
            else
            {
                status = NET_TASKSTATUS_FAILED;
            }
            break;

        case STATE_READING:
			if (m_GetFriendsListWorkItem.Finished())
			{
				if (m_GetFriendsListWorkItem.Succeeded())
				{
					netTaskDebug("Get Friends List work item finished - success");
					status = NET_TASKSTATUS_SUCCEEDED;
				}
				else
				{
					netTaskDebug("Get Friends List work item finished - cancel or failure");
					status = NET_TASKSTATUS_FAILED;
				}
			}
		default:
			break;
        }

		if(NET_TASKSTATUS_PENDING != status)
		{
			this->Complete(status);
		}

        return status;
    }

private:

    bool BeginRead()
    {
        return g_rlNp.GetWebAPI().GetFriendsList(m_LocalGamerIndex, &m_GetFriendsListWorkItem, m_TotalFriendsPtr, m_FirstFriendIndex, 
													m_FriendsPtr, m_NumFriendsPtr, m_MaxFriends, m_FriendFlags);
    }

    void Complete(const netTaskStatus status)
    {
        if(NET_TASKSTATUS_SUCCEEDED == status && !WasCanceled())
        {
			m_GetFriendsListWorkItem.ProcessSuccess();
        }
    }

    enum State
    {
        STATE_BEGIN_READ,
        STATE_READING,
    };

    State m_State;
    int m_LocalGamerIndex;
    netStatus m_MyStatus;
    unsigned m_MaxFriends;
    unsigned m_FirstFriendIndex;
    unsigned m_NumFriends;
    rlFriend* m_FriendsPtr;
    unsigned* m_NumFriendsPtr;
	unsigned* m_TotalFriendsPtr;
	int m_FriendFlags;
	rlNpGetFriendsListWorkItem m_GetFriendsListWorkItem;
};

#endif 

///////////////////////////////////////////////////////////////////////////////
// rlFriendsReader
///////////////////////////////////////////////////////////////////////////////

rlFriendsReader::rlFriendsReader()
: m_Initialized(false)
{
}

rlFriendsReader::~rlFriendsReader()
{
    this->Shutdown();
}

bool
rlFriendsReader::Init(const int /*cpuAffinity*/)
{
    rlAssert(!m_Initialized);

    m_Initialized = true;

    return true;
}

void
rlFriendsReader::Shutdown()
{
	m_Initialized = false;
}
 
void
rlFriendsReader::Update()
{
}

bool
rlFriendsReader::Read(const int localGamerIndex,
#if RSG_NP || RSG_PC || RSG_DURANGO
#if __RGSC_DLL
					   const unsigned /*firstFriendIndex*/,
#else
					  const unsigned firstFriendIndex,
#endif
                       rlFriend friends[],
                       const unsigned maxFriends,
#else
                       const unsigned /*firstFriendIndex*/,
                       rlFriend /*friends*/[],
                       const unsigned /*maxFriends*/,
#endif
					   unsigned* numFriends,		// number of friends returned
					   unsigned* totalFriends,		// total number of friends
					   int friendFlags,
                       netStatus* status)
{
    rlAssert(m_Initialized);

    if(!rlVerify(rlPresence::IsOnline(localGamerIndex)))
    {
        return false;
    }

    bool success = false;

    *numFriends = 0;
	if (!rlVerify(totalFriends) || !rlVerify(friendFlags > 0))
	{
		return false;
	}

#if !__NO_OUTPUT
    char name[RL_MAX_NAME_BUF_SIZE];
    rlPresence::GetName(localGamerIndex, name, COUNTOF(name));
	rlDebug2("Reading friends for gamer:\"%s\", firstFriendIndex: %u, maxFriends: %u, friendFlags: %d...", name, firstFriendIndex, maxFriends, friendFlags);
#endif

#if RSG_DURANGO

	success = g_rlXbl.GetPlayerManager()->GetFriends(localGamerIndex,
														firstFriendIndex,
														friends,
														maxFriends,
														numFriends,
														totalFriends,
														friendFlags,
														status);

#elif RSG_ORBIS
    rlNpFriendsReaderTask* task;
    *numFriends = maxFriends;
	if(!netTask::Create(&task, status)
        || !task->Configure(localGamerIndex, totalFriends, firstFriendIndex, numFriends, friends, maxFriends, friendFlags)
        || !netTask::Run(task))
    {
        netTask::Destroy(task);
    }
    else
    {
        success = true;
    }
#elif __RGSC_DLL
    success = rlScFriends::GetFriends(localGamerIndex,
									  0,
									  maxFriends,
									  &friends[0].m_ScFriend, // see atSpanArray for why this works.
									  sizeof(friends[0]), 
									  numFriends,
									  totalFriends,
									  status);
#elif RSG_PC

	*numFriends = maxFriends;
	FriendsReaderTask* task;
	if(!netTask::Create(&task, status)
		|| !task->Configure(localGamerIndex, totalFriends, firstFriendIndex, numFriends, maxFriends, friends, friendFlags)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

#else
	success = false;
#endif  

    if(!success && status)
    {
        status->SetPending();
        status->SetFailed();
    }

    return success;
}

void
rlFriendsReader::Cancel(netStatus* status)
{
    netTask::Cancel(status);
}

}   //namespace rage
