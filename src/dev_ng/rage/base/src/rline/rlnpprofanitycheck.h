// 
// rline/rlNpProfanityCheck.h
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#ifndef RLINE_RLNPPROFANITYCHECK_H
#define RLINE_RLNPPROFANITYCHECK_H

#include "system/threadpool.h"
#include "rline/rlnpcommon.h"

#include <np.h>

namespace rage
{

	class rlNpProfanityCheck
	{
	public:

		//PURPOSE
		// Profanity text check.
		bool ProfanityCheck(const int localGamerIndex, const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord, netStatus* status);
		
	}; // rlNpProfanityCheck

} // namespace rage

#endif // RLINE_RLNPPROFANITYCHECK_H
#endif // RSG_ORBIS
