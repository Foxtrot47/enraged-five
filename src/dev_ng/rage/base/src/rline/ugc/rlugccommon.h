// 
// rline/rlugccommon.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUGCCOMMON_H
#define RLINE_RLUGCCOMMON_H

#include "rline/rl.h"
#include "rline/clan/rlclancommon.h"
#include "rline/rldiag.h"
#include "atl/array.h"
#include "data/growbuffer.h"
#include "data/rson.h"

#define RL_UGC_YOUTUBE 1

namespace rage
{

//PURPOSE
//  Buffer size limits.  All include the terminating null.
enum 
{    
    RLUGC_MAX_CATEGORY_CHARS        = 32,
    RLUGC_MAX_CLOUD_ABS_PATH_CHARS  = 128,
    RLUGC_MAX_CONTENT_NAME_CHARS    = 64,
    RLUGC_MAX_CONTENTID_CHARS       = 23,
    RLUGC_MAX_CONTENT_TYPE_CHARS    = 32
};

//PURPOSE
//  UGC categories. These are used to note content that is 
//  special in some way.
enum rlUgcCategory
{
    RLUGC_CATEGORY_UNKNOWN = -1,
    
    RLUGC_CATEGORY_NONE = 0,                        //No particular category (the most common case)
    RLUGC_CATEGORY_ROCKSTAR_CREATED,                //Created by R*
    RLUGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE,      //Created by R*, but pending approval
    RLUGC_CATEGORY_ROCKSTAR_VERIFIED,               //Created by user, but endorsed by R*
	RLUGC_CATEGORY_ROCKSTAR_VERIFIED_CANDIDATE,     //Created by user, pending endorsement by R*
	RLUGC_CATEGORY_ROCKSTAR_COMMUNITY,
	RLUGC_CATEGORY_ROCKSTAR_COMMUNITY_CANDIDATE,
    RLUGC_NUM_CATEGORIES,

    RLUGC_FIRST_CATEGORY = RLUGC_CATEGORY_NONE
};

//PURPOSE
//  Known UGC cloud absolute path formats.  These allow us to compose paths 
//  as require rather than needing to store them locally, which can take a lot of memory.
enum rlUgcCloudAbsPathFormat
{
    RLUGC_CLAPFORMAT_V0 = 0,    //Format: /ugc/onlineservice/userid/contenttype/contentid_fileid.extension (DEPRECATED)
    RLUGC_CLAPFORMAT_V1 = 1     //Format: /ugc/contenttype/contentid/fileid_fileversion{_language}.extension
};

//PURPOSE
//  Known UGC content types
//  *** DO NOT CHANGE THESE!!!
//  These IDs are defined on the backend and must not be changed!!
enum rlUgcContentType
{
    RLUGC_CONTENT_TYPE_UNKNOWN          = -1,
    RLUGC_CONTENT_TYPE_UNUSED           = 0,
    RLUGC_CONTENT_TYPE_GTA5MISSION      = 1,
    RLUGC_CONTENT_TYPE_GTA5PLAYLIST     = 2,
    RLUGC_CONTENT_TYPE_LIFEINVADERPOST  = 3,
    RLUGC_CONTENT_TYPE_GTA5PHOTO        = 4,
    RLUGC_CONTENT_TYPE_GTA5CHALLENGE    = 5,
    RLUGC_CONTENT_TYPE_EMBLEM           = 6,
    RLUGC_CONTENT_TYPE_GTA5VIDEO        = 7, //DEPRECATED. DO NOT USE.
    RLUGC_CONTENT_TYPE_GTA5YOUTUBE      = 8
};

//PURPOSE
//  String/enum converters.
rlUgcCategory rlUgcCategoryFromString(const char* category);
const char* rlUgcCategoryToString(rlUgcCategory category);
rlUgcContentType rlUgcContentTypeFromString(const char* contentType);
const char* rlUgcContentTypeToString(rlUgcContentType contentType);

//PURPOSE
//  Error codes returned by rlUgc methods.
enum rlUgcErrorCode
{
    RLUGC_ERROR_UNEXPECTED_RESULT = -1,              //Unhandled error code
    RLUGC_ERROR_NONE = 0,                            //Success    
    RLUGC_ERROR_AUTHENTICATIONFAILED_TICKET,
    RLUGC_ERROR_DOESNOTEXIST_CONTENT,
    RLUGC_ERROR_DOESNOTEXIST_PUBLISHER,
    RLUGC_ERROR_INVALIDARGUMENT_CLOUDABSPATH,
    RLUGC_ERROR_INVALIDARGUMENT_CONTENTID,
    RLUGC_ERROR_INVALIDARGUMENT_CONTENTNAME,
    RLUGC_ERROR_INVALIDARGUMENT_CONTENTTYPE,    
    RLUGC_ERROR_INVALIDARGUMENT_ONLINESERVICE,
    RLUGC_ERROR_INVALIDARGUMENT_TAGS,
    RLUGC_ERROR_INVALIDARGUMENT_TITLEINFO,
    RLUGC_ERROR_INVALIDARGUMENT_USERID,
    RLUGC_ERROR_NOTALLOWED_MAXCREATED,
    RLUGC_ERROR_NOTALLOWED_NOTCONTENTOWNER,
    RLUGC_ERROR_NOTALLOWED_NOTLATEST,
    RLUGC_ERROR_NOTALLOWED_PROFANE,
    RLUGC_ERROR_NOTALLOWED_RESERVEDWORD,
    RLUGC_ERROR_OUTOFRANGE_CONTENTTYPE,
    RLUGC_ERROR_NUM,
};

//PURPOSE
//  Flags for UGC CDN content requests
enum rlUgcCdnContentFlags
{
    CF_ValidateSignature = BIT0,
    CF_Decrypt = BIT1,
    CF_Decompress = BIT2,
    CF_DumpData = BIT3,
    CF_Default = CF_ValidateSignature | CF_Decrypt | CF_Decompress,
};

// Assumes a single flag is set, not the bitmask
rlUgcCdnContentFlags rlUgcContentFlagFromString(const char* contentType);
const char* rlUgcContentFlagToString(rlUgcCdnContentFlags contentType);

//PURPOSE
//  Metadata for a piece of content.
class rlUgcMetadata
{
public:
    rlUgcMetadata();

    void Clear();

    bool IsValid() const;

    //PURPOSE
    //  Composes the cloud absolute path to a file associated with this piece of content.
    //PARAMS
    //  fileId                  - File to compose path for.  Most content types have only one file of ID 0.
    //  cloudAbsPath            - Buffer to store composed path
    //  sizeofCloudAbsPath      - Size of buffer
    //RETURNS
    //  If the path cannot be programmatically created, this will return false.
    //  Otherwise, cloudAbsPath will contain the path to the file, and true will be returned.
    bool ComposeCloudAbsPath(const int fileId, 
                             char* cloudAbsPath, 
                             const unsigned sizeofCloudAbsPath) const;

    //Account ID of content creator.
    PlayerAccountId GetAccountId() const { return m_AccountId; }

    //Content type
    rlUgcContentType GetContentType() const { return m_ContentType; }

    //Unique content identifier.
    const char* GetContentId() const { return m_ContentId; }

    //Content category.  Most content does not have a category,
    //as this is used only to flag special content like R*-created.
    rlUgcCategory GetCategory() const { return m_Category; }

    //Display name for this piece of content. (ex. "My Awesome Race")
    const char* GetContentName() const { return m_ContentName; }

    //Posix time that the content was published, of 0 if N/A.
    s64 GetCreatedDate() const { return m_CreatedDate; }

    //JSON of content type-specific metadata.
    const char* GetData() const { return (m_Data.Length() > 0) ? (const char*)m_Data.GetBuffer() : 0; }

    //Size of content type-specific metadata. 
    unsigned GetDataSize() const { return m_Data.Length(); }

    //Content description.
    const char* GetDescription() const { return (const char*)m_Description.GetBuffer(); }

	//Content description length
	unsigned GetDescriptionLength() const { return m_Description.Length(); }

    //Returns number of files associated with this piece of content, or -1 if unknown.
    int GetNumFiles() const;

    //Returns the file ID of the file at the specified index.
    int GetFileId(const int index) const;

    //Returns version number of file at specified index.  File versions start at 0,
    //and are incremented whenever files are updated by rlUgc::UpdateContent.
    int GetFileVersion(const int index) const;

    //Language that the content's name, description, and any text in its files will be in.
    rlScLanguage GetLanguage() const { return m_Language; }

    //Rockstar ID of content creator.
    RockstarId GetRockstarId() const { return m_RockstarId; }

    //Unique root content identifier.  This is the contentid assigned to version 1 of
    //a piece of content.  All versions of that content will have the same root contentid.
    //NOTE: Depending on the query, the rootcontentid might not be returned and this will
    //      be an empty string.
    const char* GetRootContentId() const { return m_RootContentId; }

    //Posix time that the content was last updated, of 0 if N/A.
    s64 GetUpdatedDate() const { return m_UpdatedDate; }

    //Player account user ID of content creator.  
    const char* GetUserId() const { return m_UserId; }

    //Player account display name of content creator.  
    //NOTE: We use this nonstandard casing (Username instead of UserName)
    //      because windows #defines GetUserName to something else, and
    //      it messes up our compile.
    const char* GetUsername() const { return m_UserName; }

    //Version assigned this content when created
    int GetVersion() const { return m_Version; }

    //True if the content is currently published (i.e. visible to the world).
    bool IsPublished() const { return m_PublishedDate > 0; }

    //Returns posix time content was published, or 0 if unpublished or N/A
    s64 GetPublishedDate() const { return m_PublishedDate; }

    //True if the content was R* verified.  This piece of content is not actually
    //R* verified, but it had a copy created of it that is R* verified.
    bool IsVerified() const { return m_IsVerified; }

    //PURPOSE
    //  Composes the cloud absolute path to a file associated with this piece of content.
    //PARAMS
    //  contentType             - Content type
    //  contentId               - Content ID file is associated with
    //  fileId                  - File ID (defined by content type)
    //  language                - Language to get file in, if localized
    //  cloudAbsPath            - Buffer to store composed path
    //  sizeofCloudAbsPath      - Size of buffer
    //RETURNS
    //  If the path cannot be programmatically created, this will return false.
    //  Otherwise, cloudAbsPath will contain the path to the file, and true will be returned.
    static bool ComposeCloudAbsPath(const rlUgcContentType contentType,
                                    const char* contentId,
                                    const int fileId, 
                                    const int fileVersion,
                                    const rlScLanguage language,
                                    char* cloudAbsPath, 
                                    const unsigned sizeofCloudAbsPath);

    //Copy constructor.
    rlUgcMetadata(const rlUgcMetadata& other);

    //Assignment
    rlUgcMetadata& operator=(const rlUgcMetadata& other);

private:
    friend class rlUgcTaskBase;
    friend class rlUgcQueryContentTask;

    //HACK: This is based on fact that gta5mission has two files.  All other content types have 1 or 0.    
    //      Find a better way to do this that doesn't use too much mem but is expandable.
    static const unsigned RLUGC_MAX_FILES = 3;

    rlUgcContentType m_ContentType;
    char m_ContentId[RLUGC_MAX_CONTENTID_CHARS];

    PlayerAccountId m_AccountId;
    rlUgcCategory m_Category;
    char m_ContentName[RLUGC_MAX_CONTENT_NAME_CHARS];
    s64 m_CreatedDate; //Posix time
    datGrowBuffer m_Description;
    datGrowBuffer m_Data;
    rlScLanguage m_Language;
    s64 m_PublishedDate; //Posix time
    RockstarId m_RockstarId;
    char m_RootContentId[RLUGC_MAX_CONTENTID_CHARS];
    s64 m_UpdatedDate; //Posix time
    char m_UserId[RLROS_MAX_USERID_SIZE];
    char m_UserName[RLROS_MAX_USER_NAME_SIZE];
    int m_Version;
    int m_FileVersion[RLUGC_MAX_FILES];

    bool m_IsVerified : 1;
};

//PURPOSE
//  Parses the content specific data for content type gta5video
class rlUgcGta5VideoMetadataData
{
public:

    rlUgcGta5VideoMetadataData();

    void Clear();

    //PURPOSE
    //  Initialize from a UGC metadata object.
    //  Fails if the content type is not gta5video
    bool SetMetadata(const rlUgcMetadata&  md);

    //PURPOSE
    //  Returns the reference ID used identify the video during upload
    const char* GetStagingId() const;

    //PURPOSE
    //  Returns the target URL used to upload the video
    const char* GetStagingUrl() const;

    //PURPOSE
    //  Returns the auth token used during upload
    const char* GetStagingAuthToken() const;

    //PURPOSE
    //  Returns the name of the video in the staging location
    const char* GetStagingName() const;

    //PURPOSE
    //  Identifies the final encoded video after it's been published
    //  to the video distribution platform.
    const char* GetMediaId() const;

    //PURPOSE
    //  Returns the URL to be used to access the video.  This is
    //  a publically accessible URL.
    const char* GetUrl() const;

private:
    char m_StagingName[64];
    char m_StagingUrl[512];
    char m_StagingAuthToken[64];
    char m_StagingRefId[64];
    char m_MediaId[64];
    char m_FinalUrl[512];
};

//PURPOSE
//  Parses the content specific data for content type RLUGC_CONTENT_TYPE_GTA5YOUTUBE
struct rlUgcYoutubeMetadata
{
	rlUgcYoutubeMetadata()
	{
		szVideoID[0] = '\0';
		szDuration[0] = '\0';
		nDuration_s = 0;
		nDuration_ms = 0;
		nSize = 0;
		nQualityScore = 0;
		nTrackId = 0;
		nFilenameHash = 0;
		nModdedContent = false;
		szThumbnailDefaultUrl[0] = '\0';
		szThumbnailMediumUrl[0] = '\0';
		szThumbnailLargeUrl[0] = '\0';
	}

	static const unsigned MAX_ID_LENGTH = 64;
	static const unsigned MAX_DURATION_LENGTH = 32;
	static const unsigned MAX_THUMBNAIL_URL_LENGTH = 256;

	char szVideoID[MAX_ID_LENGTH];
	char szDuration[MAX_DURATION_LENGTH];
	int nDuration_s;
	u32 nDuration_ms;
	u64 nSize;
	u32 nQualityScore;
	u32 nTrackId;
	u32 nFilenameHash;
	bool nModdedContent;
	char szThumbnailDefaultUrl[MAX_THUMBNAIL_URL_LENGTH];
	char szThumbnailMediumUrl[MAX_THUMBNAIL_URL_LENGTH];
	char szThumbnailLargeUrl[MAX_THUMBNAIL_URL_LENGTH];
};

//PURPOSE
//  Parses the content specific data for content type gta5photo
class rlUgcGta5PhotoMetadataData
{
public:

	rlUgcGta5PhotoMetadataData();

	void Clear();

	//PURPOSE
	//  Initialize from a UGC metadata object.
	//  Fails if the content type is not gta5photo
	bool SetMetadata(const rlUgcMetadata&  md);

	//PURPOSE
	//  Returns the target URL used to upload the photo
	const char* GetStagingUrl() const;

	//PURPOSE
	//  Returns the auth token used during upload
	const char* GetStagingAuthToken() const;

	//PURPOSE
	//  Returns the directory name of the photo in the staging location
	const char* GetStagingDirName() const;

	//PURPOSE
	//  Returns the filename of the photo in the staging location
	const char* GetStagingFileName() const;

	//PURPOSE
	//  True if the new Akamai endpoint is used
	bool UsesAkamai() const;

private:
	char m_StagingDirName[128];
	char m_StagingFileName[128];
	char m_StagingUrl[512];
	char m_StagingAuthToken[512];
	bool m_Akamai;
};


//PURPOSE
//  A player record associated with a piece of content.  These are created
//  when a player rates content, or sets per-player data.  For example,
//  when a gta5mission is played, the player sets data that includes 
//  an updated "played" count.  This creates a record for them on the
//  backend (or updates it if it already existed).
class rlUgcPlayer
{
public:
    rlUgcPlayer();

    void Clear();

    bool IsValid() const;

    //Account ID of player.
    PlayerAccountId GetAccountId() const { return m_AccountId; }

    //JSON of content type-specific player data
    const char* GetData() const { return (const char*)m_Data.GetBuffer(); }
    unsigned GetDataSize() const { return m_Data.Length(); }

    //Player account user ID
    const char* GetUserId() const { return m_UserId; }

    //User name (display name, e.g. gamertag) of the player.
    //NOTE: We use this nonstandard casing (Username instead of UserName)
    //      because windows #defines GetUserName to something else, and
    //      it messes up our compile.
    const char* GetUsername() const { return m_UserName; }

    //Rating player gave this content.  This will only be valid if HasRating() returns true.
    float GetRating() const { return m_Rating; }

    //Rockstar ID of player.
    RockstarId GetRockstarId() const { return m_RockstarId; }

    //Returns true of the player has rated this content
    bool HasRating() const { return m_Rating >= 0.0f; }

    //True if the player has bookmarked this content
    bool IsBookmarked() const { return m_Bookmarked; }

	//Copy constructor.
	rlUgcPlayer(const rlUgcPlayer& other);

	//Assignment
	rlUgcPlayer& operator=(const rlUgcPlayer& other);
	
private:
    friend class rlUgcTaskBase;
    friend class rlUgcQueryContentTask;

    PlayerAccountId m_AccountId;
    char m_UserId[RLROS_MAX_USERID_SIZE];
    char m_UserName[RLROS_MAX_USER_NAME_SIZE];
    RockstarId m_RockstarId;

    datGrowBuffer m_Data;
    float m_Rating;
    bool m_Bookmarked;
};

class rlUgcRatings
{
public:
    rlUgcRatings();

    void Clear();

    bool IsDefaultValues() const;

    float GetAverage() const { return m_Average; }
    s64 GetUnique() const { return m_Unique; }
    float GetNegativePercent() const { return (m_Unique > 0) ? ((float)m_NegativeUnique)/m_Unique : 0.0f; }
    s64 GetNegativeUnique() const { return m_NegativeUnique; }
    float GetPositivePercent() const { return (m_Unique > 0) ? ((float)m_PositiveUnique)/m_Unique : 0.0f; }
    s64 GetPositiveUnique() const { return m_PositiveUnique; }

private:
    friend class rlUgcTaskBase;

    float m_Average;
    s64 m_Unique;
    s64 m_NegativeUnique;
    s64 m_PositiveUnique;
};

//PURPOSE
//  Describes a piece of content.  This includes metadata and aggregated stats.
//  The data and stats are specific to the content type (which its is assumed
//  the user knows); rlUgc is ignorant of their contents.
class rlUgcContentInfo
{
public:
    rlUgcContentInfo();

	rlUgcContentInfo(const rlUgcMetadata* metadata,
					 const rlUgcRatings* ratings,
					 const char* statsJson);

    void Clear();

    bool IsValid() const;

    //Content metadata
    const rlUgcMetadata& GetMetadata() const { return m_Metadata; }
    
    //Aggregated ratings for this piece of content
    const rlUgcRatings& GetRatings() const { return m_Ratings; }

    //JSON of aggregated stats for this content.  These are specific to the content type.
    const char* GetStats() const { return (const char*)m_Stats.GetBuffer(); }
	unsigned GetStatsSize() const { return m_Stats.Length(); }

	//Copy constructor.
	rlUgcContentInfo(const rlUgcContentInfo& other);

	// Assignment
	rlUgcContentInfo& operator=(const rlUgcContentInfo& other);

private:
    friend class rlUgcQueryContentResultBase;
    friend class rlUgcTaskBase;
    friend class rlUgcQueryContentTask;

    rlUgcMetadata m_Metadata;
    rlUgcRatings m_Ratings;
    datGrowBuffer m_Stats;
};

//PURPOSE
//  Describes a content creator.
class rlUgcContentCreatorInfo
{
public:
    rlUgcContentCreatorInfo();

    void Clear();

    bool IsValid() const;

	//Copy constructor.
	rlUgcContentCreatorInfo(const rlUgcContentCreatorInfo& other);

	// Assignment
	rlUgcContentCreatorInfo& operator=(const rlUgcContentCreatorInfo& other);

    PlayerAccountId m_AccountId;
    char m_UserId[RLROS_MAX_USERID_SIZE];
    char m_UserName[RLROS_MAX_USER_NAME_SIZE]; 
    RockstarId m_RockstarId;

    int m_NumCreated;
    int m_NumPublished;

    rlUgcRatings m_Ratings;
    datGrowBuffer m_Stats;
};

struct rlUgcUrlTemplate
{
    static const char* TEMPLATE_ROOT;
    static const char* TEMPLATE_NAME;
    static const char* TEMPLATE_URL;
    static const char* TEMPLATE_FLAGS;

    rlUgcUrlTemplate() : templateFlags(0) {}

    enum TokenType
    {
        Invalid = -1,
        ContentId,
        FileId,
        FileVersion,
        Language,
        Extension,
        Platform,
        RosEnvironment,
        Max
    };

    atString templateName;
    atString templateUrl;
    unsigned templateFlags;
    struct Token
    {
        int startIndex; // index of where the value for this token should be inserted
        int length;     // length of the token in chars, including the opening and closing braces
        TokenType tokenType;
    };
    typedef atArray<Token> TokenArray;
    TokenArray tokens;
};


class rlUgcTokenValues
{
public:

    static const unsigned MAX_TOKEN_VALUE_LENGTH = RLUGC_MAX_CONTENTID_CHARS;

    rlUgcTokenValues()
        : m_Language(RLSC_LANGUAGE_UNKNOWN)
        , m_FileId(-1)
        , m_FileVersion(-1)
    {
        m_ContentId[0] = '\0';
        m_FileExtension[0] = '\0';
        m_Platform[0] = '\0';

        for (int i = 0; i < (int)rlUgcUrlTemplate::TokenType::Max; i++)
        {
            m_TokenSet[i] = false;
        }

        // Always allow {env} to be overwritten with the games actual env.
        // Does nothign if {env} isn't part of the url
        m_TokenSet[rlUgcUrlTemplate::TokenType::RosEnvironment] = true;
    }

    void SetLanguage(const rlScLanguage language)
    {
        m_Language = language;
        m_TokenSet[rlUgcUrlTemplate::TokenType::Language] = true;
    }
    rlScLanguage GetLanguage() const { return m_TokenSet[rlUgcUrlTemplate::TokenType::Language] ? m_Language : RLSC_LANGUAGE_UNKNOWN; }

    void SetContentId(const char* contentId)
    {
        safecpy(m_ContentId, contentId);
        m_TokenSet[rlUgcUrlTemplate::TokenType::ContentId] = true;
    }
    const char* GetContentId() const { return m_TokenSet[rlUgcUrlTemplate::TokenType::ContentId] ? m_ContentId : nullptr; }

    void SetFileId(const int fileId)
    {
        m_FileId = fileId;
        m_TokenSet[rlUgcUrlTemplate::TokenType::FileId] = true;
    }
    int GetFileId() const { return m_TokenSet[rlUgcUrlTemplate::TokenType::FileId] ? m_FileId : -1; }

    void SetFileVersion(const int fileVersion)
    {
        m_FileVersion = fileVersion;
        m_TokenSet[rlUgcUrlTemplate::TokenType::FileVersion] = true;
    }
    int GetFileVersion() const { return m_TokenSet[rlUgcUrlTemplate::TokenType::FileVersion] ? m_FileVersion : -1; }

    void SetFileExtension(const char* fileExtension)
    {
        safecpy(m_FileExtension, fileExtension);
        m_TokenSet[rlUgcUrlTemplate::TokenType::Extension] = true;
    }
    const char* GetFileExtension() const { return m_TokenSet[rlUgcUrlTemplate::TokenType::Extension] ? m_FileExtension : nullptr; }

    void SetPlatform(const char* platform)
    {
        safecpy(m_Platform, platform);
        m_TokenSet[rlUgcUrlTemplate::TokenType::Platform] = true;
    }
    const char*  GetPlatform() const { return m_TokenSet[rlUgcUrlTemplate::TokenType::Platform] ? m_Platform : nullptr; }

    bool IsTokenValueSet(rlUgcUrlTemplate::TokenType tokenType) const
    {
        return m_TokenSet[tokenType];
    }

private:

    static const unsigned MAX_FILE_EXTENSION_LENGTH = 5;
    static const unsigned MAX_FILE_ID_LENGTH = 16;
    static const unsigned MAX_PLATFORM_LENGTH = 16;

    char m_ContentId[RLUGC_MAX_CONTENTID_CHARS];
    char m_FileExtension[MAX_FILE_EXTENSION_LENGTH];
    char m_Platform[MAX_PLATFORM_LENGTH];
    int m_FileId;
    int m_FileVersion;
    rlScLanguage m_Language;

    bool m_TokenSet[rlUgcUrlTemplate::TokenType::Max];
};

const char* rlUgcGetTokenStringFromType(const rlUgcUrlTemplate::TokenType tokenType);
rlUgcUrlTemplate::TokenType rlUgcGetTokenTypeFromString(const rlUgcUrlTemplate::TokenType tokenType, const int stringLength);

bool rlUgcCreateUrlTemplateFromJson(
    const RsonReader& templateJson,
    rlUgcUrlTemplate& result);

bool rlUgcCreateUrlTemplatesFromJson(
    const RsonReader& templateJsonArray,
    atArray<rlUgcUrlTemplate>& results);

const rlUgcUrlTemplate* rlUgcGetTemplateFromParams(
    const rlUgcUrlTemplate* const templates,
    const int numTemplates,
    const char* templateName);

bool rlUgcBuildUrlFromTemplate(const atString& urlTemplateString,
    const rlUgcUrlTemplate::TokenArray& urlTemplateTokens,
    const rlUgcTokenValues& tokenValues,
    char* urlBuffer,
    const int urlBufferSize);

RAGE_DECLARE_SUBCHANNEL(rline, ugc);

} //namespace rage

#endif  //RLINE_RLUGCCOMMON_H
