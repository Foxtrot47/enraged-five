// 
// rline/rlugctask.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlugctask.h"
#include "data/aes.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/device.h"
#include "parser/manager.h"
#include "system/alloca.h"
#include "system/param.h"
#include "system/endian.h"
#include "system/simpleallocator.h"
#include "rline/cloud/rlcloud.h"
#include "rline/rlgamerinfo.h"
#include "rline/rltitleid.h"
#include "rline/ros/rlros.h"
#include "rline/rlprofanitycheck.h"
#include "rline/rltelemetry.h"

namespace rage
{

PARAM(rlDumpUgc, "Dumps all UGC downloads to disk");

extern sysMemAllocator* g_rlAllocator;

extern const rlTitleId* g_rlTitleId;

#define ERROR_CASE(code, codeEx, resCode) \
    if(result.IsError(code, codeEx)) \
    { \
        resultCode = resCode; \
        return; \
    }

///////////////////////////////////////////////////////////////////////////////
//  rlUgcTaskBase
///////////////////////////////////////////////////////////////////////////////
rlUgcTaskBase::rlUgcTaskBase(sysMemAllocator* allocator, u8* bounceBuffer, const unsigned sizeofBounceBuffer)
: rlRosHttpTask(allocator, bounceBuffer, sizeofBounceBuffer)
{
}

bool 
rlUgcTaskBase::ReadMetadataXml(const parTreeNode* node, const rlUgcContentType contentType, rlUgcMetadata* metadata)
{
    rtry
    {
        rverify(node, catchall, );

        metadata->Clear();
        metadata->m_ContentType = contentType;

        //To support reading by either attribute or element, iterate through both
        parTreeNode::ChildNodeIterator elIter = node->BeginChildren();
        for( ; elIter != node->EndChildren(); ++elIter)
        {
            parTreeNode* curNode = *elIter;
            if (curNode != NULL && curNode->GetData() != NULL)
            {
                rcheck(ReadMetadataValue(curNode->GetElement().GetName(), curNode->GetData(), metadata), catchall, );
            }
        }

        const atArray<parAttribute>& attrs = ((parTreeNode*)node)->GetElement().GetAttributes();
        for(int i = 0; i < attrs.size(); i++)
        {
            rcheck(ReadMetadataValue(attrs[i].GetName(), attrs[i].GetStringValue(), metadata), catchall, );
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlUgcTaskBase::ReadPlayerXml(const parTreeNode* node, rlUgcPlayer* player)
{
    rtry
    {
        rverify(node, catchall, );

        player->Clear();

        //To support reading by either attribute or element, iterate through both
        parTreeNode::ChildNodeIterator elIter = node->BeginChildren();
        for( ; elIter != node->EndChildren(); ++elIter)
        {
            parTreeNode* curNode = *elIter;
            if (curNode != NULL && curNode->GetData() != NULL)
            {
                rcheck(ReadPlayerValue(curNode->GetElement().GetName(), curNode->GetData(), player), catchall, );
            }
        }

        const atArray<parAttribute>& attrs = ((parTreeNode*)node)->GetElement().GetAttributes();
        for(int i = 0; i < attrs.size(); i++)
        {
            rcheck(ReadPlayerValue(attrs[i].GetName(), attrs[i].GetStringValue(), player), catchall, );
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlUgcTaskBase::ReadRatingsXml(const parTreeNode* node, rlUgcRatings* ratings)
{
    rtry
    {
        rverify(node, catchall, );

        ratings->Clear();

        const atArray<parAttribute>& attrs = ((parTreeNode*)node)->GetElement().GetAttributes();
        for(int i = 0; i < attrs.size(); i++)
        {
            rcheck(ReadRatingsValue(attrs[i].GetName(), attrs[i].GetStringValue(), ratings), catchall, );
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlUgcTaskBase::ReadContentCreatorValue(const char* name, const char* val, rlUgcContentCreatorInfo* cc)
{
    if (rlVerify(name != NULL && val != NULL))
    {
        if (!stricmp("aid", name))
        {
            return (1 == sscanf(val, "%d", &cc->m_AccountId));
        }
        else if (!stricmp("numc", name))
        {
            return (1 == sscanf(val, "%d", &cc->m_NumCreated));
        }
        else if (!stricmp("nump", name))
        {
            return (1 == sscanf(val, "%d", &cc->m_NumPublished));
        }
        else if (!stricmp("rid", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &cc->m_RockstarId));
        }
        else if (!stricmp("uid", name))
        {
            safecpy(cc->m_UserId, val, sizeof(cc->m_UserId));
        }
        else if (!stricmp("uname", name))
        {
            safecpy(cc->m_UserName, val, sizeof(cc->m_UserName));
        }
    }

    return true;
}

bool 
rlUgcTaskBase::ReadMetadataValue(const char* name, const char* val, rlUgcMetadata* m)
{
    if (rlVerify(name != NULL && val != NULL))
    {
        if (!stricmp("a", name))
        {
            return (1 == sscanf(val, "%d", &m->m_AccountId));
        }
        else if (!stricmp("ca", name))
        {
            m->m_Category = rlUgcCategoryFromString(val);
        }
        else if (!stricmp("ci", name))
        {
            safecpy(m->m_ContentId, val, sizeof(m->m_ContentId));
        }
        else if (!stricmp("cd", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &m->m_CreatedDate));
        }
        else if (!stricmp("da", name))
        {
            m->m_Data.Clear();
            return (-1 != m->m_Data.Append(val, ustrlen(val) + 1));
        }
        else if (!stricmp("de", name))
        {
            m->m_Description.Clear();
            return (-1 != m->m_Description.Append(val, ustrlen(val) + 1));
        }
        else if (!stricmp("f0", name))
        {
            return (1 == sscanf(val, "%d", &m->m_FileVersion[0]));
        }
        else if (!stricmp("f1", name))
        {
            return (1 == sscanf(val, "%d", &m->m_FileVersion[1]));
        }
		else if (!stricmp("f2", name))
		{
			return (1 == sscanf(val, "%d", &m->m_FileVersion[2]));
		}
		else if (!stricmp("l", name))
        {
            m->m_Language = rlScLanguageFromString(val);
        }
        else if (!stricmp("n", name))
        {
            safecpy(m->m_ContentName, val, sizeof(m->m_ContentName));
        }
        else if (!stricmp("pd", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &m->m_PublishedDate));
        }
        else if (!stricmp("r", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &m->m_RockstarId));
        }
        else if (!stricmp("rci", name))
        {
            safecpy(m->m_RootContentId, val, sizeof(m->m_RootContentId));
        }
        else if (!stricmp("u", name))
        {
            safecpy(m->m_UserId, val, sizeof(m->m_UserId));
        }
        else if (!stricmp("ud", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &m->m_UpdatedDate));
        }
        else if (!stricmp("un", name))
        {
            safecpy(m->m_UserName, val, sizeof(m->m_UserName));
        }
        else if (!stricmp("v", name))
        {
            return (1 == sscanf(val, "%d", &m->m_Version));
        }
        else if (!stricmp("vci", name))
        {
            m->m_IsVerified = true;
        }
    }

    return true;
}

bool 
rlUgcTaskBase::ReadPlayerValue(const char* name, const char* val, rlUgcPlayer* p)
{
    if (rlVerify(name != NULL && val != NULL))
    {
        if (!stricmp("a", name))
        {
            return (1 == sscanf(val, "%d", &p->m_AccountId));
        }
        else if (!stricmp("b", name))
        {
            p->m_Bookmarked = (0 == stricmp("true", val));
        }
        else if (!stricmp("d", name))
        {
            p->m_Data.Clear();
            return (-1 != p->m_Data.Append(val, ustrlen(val) + 1));
        }
        else if (!stricmp("rt", name))
        {
            return (1 == sscanf(val, "%f", &p->m_Rating));
        }
        else if (!stricmp("r", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &p->m_RockstarId));
        }
        else if (!stricmp("u", name))
        {
            safecpy(p->m_UserId, val, sizeof(p->m_UserId));
        }
        else if (!stricmp("un", name))
        {
            safecpy(p->m_UserName, val, sizeof(p->m_UserName));
        }
    }

    return true;
}

bool 
rlUgcTaskBase::ReadRatingsValue(const char* name, const char* val, rlUgcRatings* r)
{
    if (rlVerify(name != NULL && val != NULL))
    {
        if (!stricmp("a", name))
        {
            return (1 == sscanf(val, "%f", &r->m_Average));
        }
        else if (!stricmp("u", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &r->m_Unique));
        }
        else if (!stricmp("n", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &r->m_NegativeUnique));
        }
        else if (!stricmp("p", name))
        {
            return (1 == sscanf(val, "%" I64FMT "d", &r->m_PositiveUnique));
        }
    }

    return true;
}

void
rlUgcTaskBase::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    ERROR_CASE("AuthenticationFailed", "Ticket", RLUGC_ERROR_AUTHENTICATIONFAILED_TICKET);
    ERROR_CASE("DoesNotExist", "Content", RLUGC_ERROR_DOESNOTEXIST_CONTENT);
    ERROR_CASE("DoesNotExist", "Publisher", RLUGC_ERROR_DOESNOTEXIST_PUBLISHER);
    ERROR_CASE("InvalidArgument", "CloudAbsPath", RLUGC_ERROR_INVALIDARGUMENT_CLOUDABSPATH);
    ERROR_CASE("InvalidArgument", "ContentId", RLUGC_ERROR_INVALIDARGUMENT_CONTENTID);
    ERROR_CASE("InvalidArgument", "ContentName", RLUGC_ERROR_INVALIDARGUMENT_CONTENTNAME);
    ERROR_CASE("InvalidArgument", "ContentType", RLUGC_ERROR_INVALIDARGUMENT_CONTENTTYPE);    
    ERROR_CASE("InvalidArgument", "OnlineService", RLUGC_ERROR_INVALIDARGUMENT_ONLINESERVICE);
    ERROR_CASE("InvalidArgument", "Tags", RLUGC_ERROR_INVALIDARGUMENT_TAGS);
    ERROR_CASE("InvalidArgument", "TitleInfo", RLUGC_ERROR_INVALIDARGUMENT_TITLEINFO);
    ERROR_CASE("InvalidArgument", "UserId", RLUGC_ERROR_INVALIDARGUMENT_USERID);
    ERROR_CASE("NotAllowed", "MaxCreated", RLUGC_ERROR_NOTALLOWED_MAXCREATED);
    ERROR_CASE("NotAllowed", "NotContentOwner", RLUGC_ERROR_NOTALLOWED_NOTCONTENTOWNER);
    ERROR_CASE("NotAllowed", "NotLatest", RLUGC_ERROR_NOTALLOWED_NOTLATEST);
    ERROR_CASE("NotAllowed", "Profane", RLUGC_ERROR_NOTALLOWED_PROFANE);
    ERROR_CASE("NotAllowed", "ReservedWord", RLUGC_ERROR_NOTALLOWED_RESERVEDWORD);
    ERROR_CASE("OutOfRange", "ContentType", RLUGC_ERROR_OUTOFRANGE_CONTENTTYPE);

#if __ASSERT
    rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
#endif
    resultCode = RLUGC_ERROR_UNEXPECTED_RESULT;
}

bool
rlUgcTaskBase::AppendFiles(const rlUgc::SourceFile* files, const unsigned numFiles)
{
    rtry
    {
        //Start an empty data parameter. This is custom binary data and needs
        //to be appended separately
        rverify(AddStringParameter("data", "", true),
                catchall,);

        rverify(files != NULL || numFiles == 0,
                catchall,);

        //Now append all of the content for the data parameter.
        //The format for each file is {fileSize}{fileId}{fileContent}
        //TODO: Consider using chunked transfer to avoid
        //using too much rline heap space, would require backend changes
        //as well
        for (unsigned k = 0; k < numFiles; k++)
        {
            const rlUgc::SourceFile& file = files[k];

            rverify(file.m_FileLength > 0, catchall, );
            rverify(file.m_FileContent != NULL, catchall, );

            //Ensure things are nbo
            const s32 fileLength = sysEndian::NtoB(file.m_FileLength);
            const s32 fileId = sysEndian::NtoB(file.m_FileId);

            rverify(AppendContent(&fileLength, sizeof(fileLength)), catchall, );
            rverify(AppendContent(&fileId, sizeof(fileId)), catchall, );
            rverify(AppendContent(file.m_FileContent, file.m_FileLength), catchall, );
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcCheckTextTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcCheckTextTask::Configure(const int localGamerIndex, 
                              const char* contentName,
                              const char* description,
							  const char* tagCsv,
							  char* profaneWord)
{
	if(	(!contentName && !description && !tagCsv) ||
		(contentName && strlen(contentName)==0 && description && strlen(description)==0 && tagCsv && strlen(tagCsv)==0)	)
	{
		rlTaskError("Cannot check for empty strings");
		return false;
	}
	if(!rlUgcTaskBase::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("languageCode", NULL, true)
        || !AddStringParameter("contentName", contentName, true)
        || !AddStringParameter("description", description, true)
        || !AddStringParameter("tagCsv", tagCsv, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

	if(profaneWord)
	{
		m_profaneWord=profaneWord;
	}

    return true;
}

void
rlUgcCheckTextTask::ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
	if(result.IsError("Profane", "ContentName") || result.IsError("Profane", "Description") || result.IsError("Profane", "Tags"))
	{
		resultCode = RLUGC_ERROR_NOTALLOWED_PROFANE;
		if(m_profaneWord)
		{
            if(!rlProfanityCheck::ExtractProfaneWord(node, m_profaneWord, MAX_PROFANE_WORD_LENGTH))
			{
				// Removed because certain UGC flows do not return the result (i.e. if using the word "exploit", it is
				// returned as an illegal UGC word rather than a profane word).
				// rlAssertf(0, "Could not extract profane word");
				rlTaskError("Could not extract profane word");
			}
		}
	}
	ERROR_CASE("Profane", "ContentName", RLUGC_ERROR_NOTALLOWED_PROFANE);
	ERROR_CASE("Profane", "Description", RLUGC_ERROR_NOTALLOWED_PROFANE);
	ERROR_CASE("Profane", "Tags", RLUGC_ERROR_NOTALLOWED_PROFANE);

    return rlUgcTaskBase::ProcessError(result, node, resultCode);
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcCopyContentTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcCopyContentTask::Configure(const int localGamerIndex, 
                                const rlUgcContentType contentType,
                                const char* contentId,
                                rlUgcMetadata* metadata)
{
    if(!rlUgcTaskBase::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    if (contentId == NULL || contentId[0] == '\0')
    {
        rlTaskError("contentId cannot be null or empty");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
        || !AddStringParameter("contentId", contentId, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_ContentType = contentType;
    m_Metadata = metadata;

    return true;
}

bool 
rlUgcCopyContentTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    if (m_Metadata)
    {
        return ReadMetadataXml(node->FindChildWithNameIgnoreNs("Result"), m_ContentType, m_Metadata);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcCreateContentTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcCreateContentTask::Configure(const int localGamerIndex, 
                                  const rlUgcContentType contentType,
                                  const char* contentName,
                                  const char* dataJson,
                                  const char* description,
                                  const rlUgc::SourceFileInfo* files,
                                  const unsigned numFiles,
                                  const rlScLanguage language,
                                  const char* tagCsv,
                                  const bool publish,
                                  rlUgcMetadata* metadata,
                                  const bool withFileData/* = false*/)
{
    rtry
    {
        m_WithFileData = withFileData;

        rverify(contentName && (strlen(contentName) <= RLUGC_MAX_CONTENT_NAME_CHARS),
                catchall,
                rlError("Invalid content name"));

        rverify(rlUgcTaskBase::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        //Allocate and write the params JSON
        RsonWriter w;
        w.InitNull(RSON_FORMAT_JSON);
        rcheck(WriteJson(contentName, dataJson, description, files, numFiles, language, &publish, tagCsv, w), 
                catchall, 
                rlTaskError("Failed to write JSON for len"));

        unsigned jsonLen = w.Length() + 1;
        char* json = Alloca(char, jsonLen);
        rcheck(json, catchall, rlTaskError("Failed to alloc %u bytes for JSON", jsonLen));

        w.Init(json, jsonLen, RSON_FORMAT_JSON);
        rcheck(WriteJson(contentName, dataJson, description, files, numFiles, language, &publish, tagCsv, w), 
                catchall, 
                rlTaskError("Failed to write JSON"));

        //Make the request
        if(!AddStringParameter("ticket", cred.GetTicket(), true)
            || !AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
            || !AddStringParameter("paramsJson", json, true))
        {
            rlTaskError("Failed to add params");
            return false;
        }

        m_ContentType = contentType;
        m_Metadata = metadata;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlUgcCreateContentTask::Configure(const int localGamerIndex, 
                                  const rlUgcContentType contentType,
                                  const char* contentName,
                                  const char* dataJson,
                                  const char* description,
                                  const rlUgc::SourceFile* files,
                                  const unsigned numFiles,
                                  const rlScLanguage language,
                                  const char* tagCsv,
                                  const bool publish,
                                  rlUgcMetadata* metadata)
{
    rtry
    {
        rverify(Configure(localGamerIndex,
                          contentType,
                          contentName,
                          dataJson,
                          description,
                          static_cast<rlUgc::SourceFileInfo*>(NULL),
                          0,
                          language,
                          tagCsv,
                          publish,
                          metadata,
                          true),
                catchall,);

        rverify(AppendFiles(files, numFiles), catchall, );

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlUgcCreateContentTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    if (m_Metadata)
    {
        return ReadMetadataXml(node->FindChildWithNameIgnoreNs("Result"), m_ContentType, m_Metadata);
    }
    return true;
}

bool
rlUgcCreateContentTask::WriteJson(const char* contentName,
                                  const char* dataJson,
                                  const char* description,
                                  const rlUgc::SourceFileInfo* files,
                                  const unsigned numFiles,
                                  const rlScLanguage language,
                                  const bool* publish,
                                  const char* tagCsv,
                                  RsonWriter& w) const
{
    rtry
    {
        rcheck(w.Begin(NULL, NULL), catchall, );

        if (contentName) rcheck(w.WriteString("ContentName", contentName), catchall, );
        if (dataJson) rcheck(w.WriteString("DataJson", dataJson), catchall, );
        if (description) rcheck(w.WriteString("Description", description), catchall, );
        if (publish) rcheck(w.WriteBool("Publish", *publish), catchall, );
        rcheck(w.WriteString("Language", rlScLanguageToString(language)), catchall, );
        if (tagCsv) rcheck(w.WriteString("TagCsv", tagCsv), catchall, );

        if (numFiles > 0) 
        {
            rcheck(w.BeginArray("Files", NULL), catchall, );

            for(unsigned i = 0; i < numFiles; i++)
            {
                rcheck(w.Begin(NULL, NULL), catchall, );
                rcheck(w.WriteInt("i", files[i].m_FileId), catchall, );
                rcheck(w.WriteString("p", files[i].m_CloudAbsPath), catchall, );
                rcheck(w.End(), catchall, );
            }

            rcheck(w.End(), catchall, );
        }

        rcheck(w.End(), catchall, );

        return true;
    }
    rcatchall
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcQueryContentTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcQueryContentTask::Configure(const int localGamerIndex, 
                                 const rlUgcContentType contentType,
                                 const char* queryName,
                                 const char* queryParams,
                                 const int offset,
                                 const int count,
                                 int* total,
                                 int* hash,
                                 const rlUgc::QueryContentDelegate& dlgt)
{
    rtry
    {
        m_InnerText.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);
        m_CurStats.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

        rcheck(rlUgcTaskBase::Configure(localGamerIndex, &m_RosSaxReader),
               catchall,
               rlTaskError("Failed to configure base class"));

        rverify(queryName != NULL, catchall, rlTaskError("queryName cannot be null"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rcheck(AddStringParameter("ticket", cred.GetTicket(), true)
               && AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
               && AddStringParameter("queryName", queryName, true)
               && AddStringParameter("queryParams", queryParams, true)
               && AddIntParameter("offset", offset)
               && AddIntParameter("count", count),
               catchall,
               rlTaskError("Failed to add params"));

        m_ContentType = contentType;
        m_Dlgt = dlgt;
        m_Total = total;
        m_Hash = hash;
        m_Index = 0;

        m_TempHash = 0;
        m_TempTotal = 0;
        m_State = STATE_EXPECT_RESULT;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

void
rlUgcQueryContentTask::ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
    rlUgcTaskBase::ProcessError(result, node, resultCode);
}

bool 
rlUgcQueryContentTask::SaxStartElement(const char* name)
{
    if (m_InnerText.Length() > 0) m_InnerText.Truncate(0);

    switch((int)m_State)
    {
    case STATE_EXPECT_RESULT:
        if (!stricmp("result", name))
        {
            m_State = STATE_READ_RESULT;
        }
        break;

    case STATE_READ_RESULT:
        if (!stricmp("r", name))
        {
            //Reset for reading this content record
            m_CurContentId[0] = '\0';
            m_CurMetadata.Clear();
            m_CurRatings.Clear();
            m_CurStats.Clear();
            m_CurNumPlayers = 0;
            m_CurPlayerIndex = 0;  

            m_State = STATE_READ_RECORD;
        }
        break;

    case STATE_READ_RECORD:
        if (!stricmp("m", name))
        {
            m_State = STATE_READ_RECORD_META;
        }
        else if (!stricmp("r", name))
        {
            m_State = STATE_READ_RECORD_RATINGS;
        }
        else if (!stricmp("p", name))
        {
            m_CurPlayer.Clear();
			m_State = STATE_READ_PLAYER;
        }
        break;
    }

    return true;
}

bool 
rlUgcQueryContentTask::SaxAttribute(const char* name, const char* val)
{
    rtry
    {
        switch((int)m_State)
        {
        case STATE_READ_RESULT:
            if (!stricmp("total", name))
            {
                rcheck(1 == sscanf(val, "%d", &m_TempTotal), catchall, );
            }
            else if (!stricmp("hash", name))
            {
                rcheck(1 == sscanf(val, "%d", &m_TempHash), catchall, );
            }
            break;

        case STATE_READ_RECORD:
            if (!stricmp("c", name))
            {
                safecpy(m_CurContentId, val, sizeof(m_CurContentId));
            }
            else if (!stricmp("p", name))
            {
                rcheck(1 == sscanf(val, "%d", &m_CurNumPlayers), catchall, );
            }
            break;

        case STATE_READ_RECORD_META:
            rcheck(ReadMetadataValue(name, val, &m_CurMetadata), catchall, );
            break;

        case STATE_READ_RECORD_RATINGS:
            rcheck(ReadRatingsValue(name, val, &m_CurRatings), catchall, );
            break;

        case STATE_READ_PLAYER:
            rcheck(ReadPlayerValue(name, val, &m_CurPlayer), catchall, );
            break;
        }

        return true;
    }
    rcatchall
    {
        rlTaskError("Failed to process attribute %s=%s", name, val);
        return false;
    }
}

bool 
rlUgcQueryContentTask::SaxCharacters(const char* ch, const int start, const int length)
{
    if (!m_InnerText.AppendOrFail(&ch[start], length))
    {
        rlTaskError("Failed to process characters %s (start=%d, length=%d)", ch, start, length);
        return false;
    }

    return true;
}

bool 
rlUgcQueryContentTask::SaxEndElement(const char* name)
{
    rtry
    {
        switch((int)m_State)
        {
        case STATE_READ_RESULT:
            if (!stricmp("result", name))
            {
                if (m_Hash != NULL) *m_Hash = m_TempHash;
                if (m_Total != NULL) *m_Total = m_TempTotal;
                m_State = STATE_DONE;
            }
            break;

        case STATE_READ_RECORD:
            if (!stricmp("r", name))
            {
                if (m_CurMetadata.IsValid())
                {
                    if (m_CurNumPlayers == 0) //Only call if we haven't already been called with a player
                    {
                        const char* statsJson = (m_CurStats.Length() > 0) ? (const char*)m_CurStats.GetBuffer() : NULL;   

                        m_CurMetadata.m_ContentType = m_ContentType; //Ensure the content type hasn't been cleared before calling dlgt

                        m_Dlgt.Invoke(m_Index,
                                      m_CurContentId,
                                      m_CurMetadata.IsValid() ? &m_CurMetadata : NULL,
                                      &m_CurRatings,
                                      statsJson,
                                      m_CurNumPlayers, 
                                      m_CurPlayerIndex, 
                                      NULL);
                    }

                    ++m_Index;
                }
                else
                {
                    rlTaskWarning("Invalid metadata; skipping");
                }

                m_State = STATE_READ_RESULT;
            }
            else if (!stricmp("s", name))
            {
                if (m_CurStats.Length() > 0) m_CurStats.Truncate(0);
                rcheck(m_CurStats.Append(m_InnerText.GetBuffer(), m_InnerText.Length()), catchall, );
            }
            else if (!stricmp("c", name))
            {
                m_State = STATE_READ_RECORD;
            }
            break;

        case STATE_READ_RECORD_META:            
            if (!stricmp("m", name))
            {
                safecpy(m_CurMetadata.m_ContentId, m_CurContentId, sizeof(m_CurMetadata.m_ContentId));
                m_State = STATE_READ_RECORD;
            }
            else if (m_InnerText.Length() > 0)
            {
                rcheck(ReadMetadataValue(name, (const char*)m_InnerText.GetBuffer(), &m_CurMetadata), catchall, );
            }
            break;

        case STATE_READ_RECORD_RATINGS:
            if (!stricmp("r", name))
            {
                m_State = STATE_READ_RECORD;
            }
            else if (m_InnerText.Length() > 0)
            {
                rcheck(ReadRatingsValue(name, (const char*)m_InnerText.GetBuffer(), &m_CurRatings), catchall, );
            }
            break;

        case STATE_READ_PLAYER:
            if (!stricmp("p", name))
            {
                if (m_CurMetadata.IsValid())
                {
                    if (rlVerify(m_CurPlayer.IsValid()))
                    {
                        const char* statsJson = (m_CurStats.Length() > 0) ? (const char*)m_CurStats.GetBuffer() : NULL;   

                        m_CurMetadata.m_ContentType = m_ContentType; //Ensure the content type hasn't been cleared before calling dlgt

                        m_Dlgt.Invoke(m_Index,
                                      m_CurContentId,
                                      m_CurMetadata.IsValid() ? &m_CurMetadata : NULL,
                                      &m_CurRatings,
                                      statsJson,
                                      m_CurNumPlayers, 
                                      m_CurPlayerIndex++,
                                      &m_CurPlayer);
                    }
                    else
                    {
                        rlTaskWarning("Invalid player; skipping");
                    }
                }
                else
                {
                    rlTaskWarning("Invalid metadata; skipping player");
                }

                m_State = STATE_READ_RECORD;
            }
            else if (m_InnerText.Length() > 0)
            {
				rcheck(ReadPlayerValue(name, (const char*)m_InnerText.GetBuffer(), &m_CurPlayer), catchall, );
            }
            break;
        }

        return true;
    }
    rcatchall
    {
        rlTaskError("Failed to process end element %s", name);
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcQueryContentCreatorsTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcQueryContentCreatorsTask::Configure(const int localGamerIndex, 
                                         const rlUgcContentType contentType,
                                         const char* queryName,
                                         const char* queryParams,
                                         const int offset,
                                         const int count,
                                         const rlUgc::QueryContentCreatorsDelegate& dlgt)
{
    rtry
    {
        m_InnerText.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

        rcheck(rlUgcTaskBase::Configure(localGamerIndex, &m_RosSaxReader),
               catchall,
               rlTaskError("Failed to configure base class"));
        
        rverify(queryName != NULL, catchall, rlTaskError("queryName cannot be null"));            

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rcheck(AddStringParameter("ticket", cred.GetTicket(), true)
               && AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
               && AddStringParameter("queryName", queryName, true)
               && AddIntParameter("offset", offset)
               && AddIntParameter("count", count)
               && AddStringParameter("queryParams", queryParams, true),
               catchall,
               rlTaskError("Failed to add params"));

        m_Dlgt = dlgt;

        m_State = STATE_EXPECT_RESULT;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

void
rlUgcQueryContentCreatorsTask::ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
    rlUgcTaskBase::ProcessError(result, node, resultCode);
}

bool 
rlUgcQueryContentCreatorsTask::SaxStartElement(const char* name)
{
    if (m_InnerText.Length() > 0) m_InnerText.Truncate(0);

    switch((int)m_State)
    {
    case STATE_EXPECT_RESULT:
        if (!stricmp("result", name))
        {
            m_State = STATE_READ_RESULT;
        }
        break;

    case STATE_READ_RESULT:
        if (!stricmp("c", name))
        {
            m_TempCreator.Clear();
            m_State = STATE_READ_CREATOR;
        }
        break;
    
    case STATE_READ_CREATOR:
        if (!stricmp("r", name))
        {
            m_State = STATE_READ_CREATOR_RATINGS;
        }
        break;
    }

    return true;
}

bool 
rlUgcQueryContentCreatorsTask::SaxAttribute(const char* name, const char* val)
{
    rtry
    {
        switch((int)m_State)
        {
        case STATE_READ_RESULT:
            if (!stricmp("total", name))
            {
                rcheck(1 == sscanf(val, "%d", &m_TotalVal), catchall, );
            }
            break;

        case STATE_READ_CREATOR:
            rcheck(ReadContentCreatorValue(name, val, &m_TempCreator), catchall, );
            break;

        case STATE_READ_CREATOR_RATINGS:
            rcheck(ReadRatingsValue(name, val, &m_TempCreator.m_Ratings), catchall, );
            break;
        }

        return true;
    }
    rcatchall
    {
        rlTaskError("Failed to process attribute %s=%s", name, val);
        return false;
    }
}

bool 
rlUgcQueryContentCreatorsTask::SaxCharacters(const char* ch, const int start, const int length)
{
    if (!m_InnerText.AppendOrFail(&ch[start], length))
    {
        rlTaskError("Failed to process characters %s (start=%d, length=%d)", ch, start, length);
        return false;
    }

    return true;
}

bool 
rlUgcQueryContentCreatorsTask::SaxEndElement(const char* name)
{
    rtry
    {
        switch((int)m_State)
        {
        case STATE_READ_RESULT:
            if (!stricmp("result", name))
            {
                m_State = STATE_DONE;
            }
            break;

        case STATE_READ_CREATOR:
            if (!stricmp("c", name))
            {
                if (m_TempCreator.IsValid())
                {
                    m_Dlgt.Invoke((unsigned)m_TotalVal, &m_TempCreator);
                }
                else
                {
                    rlTaskWarning("Invalid creator; skipping result");
                }

                m_State = STATE_READ_RESULT;
            }
            else if (!stricmp("s", name))
            {
                if (m_TempCreator.m_Stats.Length() > 0) m_TempCreator.m_Stats.Truncate(0);
                rcheck(m_TempCreator.m_Stats.Append(m_InnerText.GetBuffer(), m_InnerText.Length()), catchall, );
            }
            break;

        case STATE_READ_CREATOR_RATINGS:
            if (!stricmp("r", name))
            {
                m_State = STATE_READ_CREATOR;
            }
            else if (m_InnerText.Length() > 0)
            {
                rcheck(ReadRatingsValue(name, (const char*)m_InnerText.GetBuffer(), &m_TempCreator.m_Ratings), catchall, );
            }
            break;
        }

        return true;
    }
    rcatchall
    {
        rlTaskError("Failed to process end element %s", name);
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcPublishTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcPublishTask::Configure(const int localGamerIndex, 
                            const rlUgcContentType contentType,
                            const char* contentId,
                            const char* baseContentId,
                            const char* httpRequestHeaders)
{
	rlHttpTaskConfig config;
	config.m_HttpHeaders = httpRequestHeaders;
    if(!rlUgcTaskBase::Configure(localGamerIndex, &config))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    if (contentId == NULL || contentId[0] == '\0')
    {
        rlTaskError("contentId cannot be null or empty");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
        || !AddStringParameter("contentId", contentId, true)
        || !AddStringParameter("baseContentId", baseContentId, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcSetBookmarkedTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcSetBookmarkedTask::Configure(const int localGamerIndex, 
                                  const rlUgcContentType contentType,
                                  const char* contentId,
                                  const bool bookmarked)
{
    if(!rlUgcTaskBase::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    if (contentId == NULL || contentId[0] == '\0')
    {
        rlTaskError("contentId cannot be null or empty");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
        || !AddStringParameter("contentId", contentId, true)
        || !AddStringParameter("bookmarked", bookmarked ? "true" : "false", true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcSetDeletedTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcSetDeletedTask::Configure(const int localGamerIndex, 
                               const rlUgcContentType contentType,
                               const char* contentId,
                               const bool deleted)
{
    if(!rlUgcTaskBase::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    if (contentId == NULL || contentId[0] == '\0')
    {
        rlTaskError("contentId cannot be null or empty");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
        || !AddStringParameter("contentId", contentId, true)
        || !AddStringParameter("deleted", deleted ? "true" : "false", true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcSetPlayerDataTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcSetPlayerDataTask::Configure(const int localGamerIndex, 
                                  const rlUgcContentType contentType,
                                  const char* contentId,
                                  const char* dataJson,
                                  const float* rating)
{
    rtry
    {
        rverify(rlUgcTaskBase::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        rverify(contentId != NULL && contentId[0] != '\0', catchall, rlTaskError("contentId cannot be null or empty"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        RsonWriter w;
        w.InitNull(RSON_FORMAT_JSON);
        rcheck(WriteJson(dataJson, rating, w), catchall, rlTaskError("Failed to write JSON for len"));

        unsigned jsonLen = w.Length() + 1;
        char* json = Alloca(char, jsonLen);
        rcheck(json, catchall, rlTaskError("Failed to alloc %u bytes for JSON", jsonLen));

        w.Init(json, jsonLen, RSON_FORMAT_JSON);
        rcheck(WriteJson(dataJson, rating, w), catchall, rlTaskError("Failed to write JSON"));

        if(!AddStringParameter("ticket", cred.GetTicket(), true)
            || !AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
            || !AddStringParameter("contentId", contentId, true)
            || !AddStringParameter("paramsJson", json, true))
        {
            rlTaskError("Failed to add params");
            return false;
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlUgcSetPlayerDataTask::WriteJson(const char* dataJson,
                                  const float* rating,
                                  RsonWriter& w) const
{
    rtry
    {
        rcheck(w.Begin(NULL, NULL), catchall, );
        if (dataJson != NULL) rcheck(w.WriteString("DataJson", dataJson), catchall, );
        if (rating != NULL) rcheck(w.WriteFloat("Rating", *rating), catchall, );
        rcheck(w.End(), catchall, );

        return true;
    }
    rcatchall
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlUgcUpdateContentTask
///////////////////////////////////////////////////////////////////////////////
bool
rlUgcUpdateContentTask::Configure(const int localGamerIndex, 
                                  const rlUgcContentType contentType,
                                  const char* contentId,
                                  const char* contentName,
                                  const char* dataJson,
                                  const char* description,
                                  const rlUgc::SourceFileInfo* files,
                                  const unsigned numFiles,
                                  const char* tagCsv,
                                  rlUgcMetadata* metadata)
{
    rtry
    {
        rcheck(rlUgcTaskBase::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

        rverify(contentId != NULL && contentId[0] != '\0', catchall, rlTaskError("contentId cannot be null or empty"));

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        //Allocate and write the update JSON
        RsonWriter w;
        w.InitNull(RSON_FORMAT_JSON);
        rcheck(WriteJson(contentName, dataJson, description, files, numFiles, tagCsv, w), 
               catchall, 
               rlTaskError("Failed to write JSON for len"));

        unsigned jsonLen = w.Length() + 1;
        char* json = Alloca(char, jsonLen);
        rcheck(json, catchall, rlTaskError("Failed to alloc %u bytes for JSON", jsonLen));

        w.Init(json, jsonLen, RSON_FORMAT_JSON);
        rcheck(WriteJson(contentName, dataJson, description, files, numFiles,tagCsv, w), 
               catchall, 
               rlTaskError("Failed to write JSON"));

        //Make the request
        if(!AddStringParameter("ticket", cred.GetTicket(), true)
            || !AddStringParameter("contentType", rlUgcContentTypeToString(contentType), true)
            || !AddStringParameter("contentId", contentId, true)
            || !AddStringParameter("updateJson", json, true))
        {
            rlTaskError("Failed to add params");
            return false;
        }

        m_ContentType = contentType;
        m_Metadata = metadata;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlUgcUpdateContentTask::Configure(const int localGamerIndex, 
                                  const rlUgcContentType contentType,
                                  const char* contentId,
                                  const char* contentName,
                                  const char* dataJson,
                                  const char* description,
                                  const rlUgc::SourceFile* files,
                                  const unsigned numFiles,
                                  const char* tagCsv,
                                  rlUgcMetadata* metadata)
{
    rtry
    {
        rverify(Configure(localGamerIndex,
                          contentType,
                          contentId,
                          contentName,
                          dataJson,
                          description,
                          static_cast<rlUgc::SourceFileInfo*>(NULL),
                          0,
                          tagCsv,
                          metadata),
                catchall,);

        rverify(AppendFiles(files, numFiles), catchall, );

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlUgcUpdateContentTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
    if (m_Metadata)
    {
        return ReadMetadataXml(node->FindChildWithNameIgnoreNs("Result"), m_ContentType, m_Metadata);
    }
    return true;
}

bool
rlUgcUpdateContentTask::WriteJson(const char* contentName,
                                  const char* dataJson,
                                  const char* description,
                                  const rlUgc::SourceFileInfo* files,
                                  const unsigned numFiles,
                                  const char* tagCsv,
                                  RsonWriter& w) const
{
    rtry
    {
        rcheck(w.Begin(NULL, NULL), catchall, );

        if (contentName) rcheck(w.WriteString("ContentName", contentName), catchall, );
        if (dataJson) rcheck(w.WriteString("DataJson", dataJson), catchall, );
        if (description) rcheck(w.WriteString("Description", description), catchall, );
        if (tagCsv) rcheck(w.WriteString("TagCsv", tagCsv), catchall, );

        if (numFiles > 0) 
        {
            rcheck(w.BeginArray("Files", NULL), catchall, );

            for(unsigned i = 0; i < numFiles; i++)
            {
                rcheck(w.Begin(NULL, NULL), catchall, );
                rcheck(w.WriteInt("i", files[i].m_FileId), catchall, );
                rcheck(w.WriteString("p", files[i].m_CloudAbsPath), catchall, );
                rcheck(w.End(), catchall, );
            }

            rcheck(w.End(), catchall, );
        }

        rcheck(w.End(), catchall, );

        return true;
    }
    rcatchall
    {
        return false;
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcGetCdnContentTask
////////////////////////////////////////////////////////////////////////////////
rlUgcGetCdnContentTask::rlUgcGetCdnContentTask(sysMemAllocator* allocator)
    : m_Allocator(allocator)
    , m_GrowBuffer(nullptr)
    , m_LocalGamerIndex(RL_INVALID_GAMER_INDEX)
    , m_Flags(0)
    , m_State(State::State_Invalid)
    , m_UrlProvided(false)
{
    m_Url[0] = '\0';
}

rlUgcGetCdnContentTask::~rlUgcGetCdnContentTask()
{
    Reset();
}

void
rlUgcGetCdnContentTask::Reset()
{
    rlAssert(!this->IsPending());

    m_State = State::State_Invalid;
    m_HttpRequest.Clear();
    m_MyGrowBuffer.Clear();
}

bool
rlUgcGetCdnContentTask::Configure(
    const int localGamerIndex,
    datGrowBuffer* growBuffer,
    const char* url,
    const unsigned contentFlags,
    rlCloudFileInfo* fileInfo,
    u64 lastModifiedTime)
{
    bool success = false;

    rtry
    {
        // validate parameters
        rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
        rverifyall(m_State == State::State_Invalid || m_State == State::State_Finished);

        // copy in parameters 
        m_LocalGamerIndex = localGamerIndex;

        // copy in url if not null
        if (url != nullptr)
        {
            safecpy(m_Url, url);
            m_UrlProvided = true;
        }

        m_Flags = contentFlags;
        OUTPUT_ONLY(if (PARAM_rlDumpUgc.Get()) { m_Flags |= rlUgcCdnContentFlags::CF_DumpData; });

        m_LastModifiedTime = lastModifiedTime;
        rlTaskDebug("Configure :: m_LastModifiedTime: %" I64FMT "u", m_LastModifiedTime);
        m_FileInfo = fileInfo;
        rlTaskDebug("Configure :: m_FileInfo: %p", m_FileInfo);

        // reset state
        Reset();

        // initialise grow buffer with primary allocator if one wasn't supplied
        m_GrowBuffer = growBuffer;
        if (!m_GrowBuffer)
        {
            m_MyGrowBuffer.Reset();
            m_MyGrowBuffer.Init(m_Allocator, datGrowBuffer::NULL_TERMINATE);
            m_GrowBuffer = &m_MyGrowBuffer;
        }

        rlTaskDebug("Configure :: m_GrowBuffer Length: %u, Capacity: %u", m_GrowBuffer->Length(), m_GrowBuffer->GetCapacity());

        // advance state
        m_State = State::State_Get;

        success = true;
    }
        rcatchall
    {

    }
    return success;
}

void
rlUgcGetCdnContentTask::Start()
{
    rlTaskBase::Start();
    rlAssert(State::State_Get == m_State);
}

void
rlUgcGetCdnContentTask::Finish(const FinishType finishType, const int resultCode)
{
    if (m_FileInfo != nullptr)
    {
        rlTaskDebug("Finish :: Populating file info");
        PopulateFileInfo(m_FileInfo, m_HttpRequest, m_Filter);
        rlTaskDebug("Finish :: File info populated: Last Modified: %" I64FMT "u", m_FileInfo->m_LastModifiedPosixTime);
    }

    m_HttpRequest.Clear();
    m_State = State::State_Finished;

    rlTaskDebug("Finish :: m_GrowBuffer Length: %u, Capacity: %u", m_GrowBuffer->Length(), m_GrowBuffer->GetCapacity());

    rlTaskBase::Finish(finishType, resultCode);
}

void
rlUgcGetCdnContentTask::DoCancel()
{
    rlTaskDebug("Cancelling HTTP request");
    m_HttpRequest.Cancel();
}

void
rlUgcGetCdnContentTask::Update(const unsigned timeStep)
{
    rlTaskBase::Update(timeStep);

    if (WasCanceled())
    {
        this->Finish(FINISH_CANCELED);
        return;
    }

    switch (m_State)
    {
    case State::State_Get:
        if (this->Get())
        {
            rlTaskDebug("Getting");
            m_State = State::State_Getting;
        }
        else
        {
            m_State = State::State_Failed;
        }
        break;

    case State::State_Getting:

        // pump the request
        m_HttpRequest.Update();

        if (m_MyStatus.Pending() || m_MyStatus.Succeeded())
        {
            // read content whether we're still pending or we succeeded (to grab anything received this update)
            if (!ReadContent())
            {
                rlTaskError("ReadContent failed. m_GrowBuffer Length: %u, Capacity: %u", m_GrowBuffer->Length(), m_GrowBuffer->GetCapacity());

                // unable to write all content
                m_State = State::State_Failed;
                m_MyStatus.SetFailed(NET_HTTPSTATUS_OUT_OF_MEMORY);
                m_HttpRequest.Cancel();
            }
            else
            {
                // check if the status has succeeded
                if (m_MyStatus.Succeeded())
                {
                    if (m_HttpRequest.Succeeded())
                    {
                        rlTaskDebug("Retrieved %s", m_HttpRequest.GetUri());

                        if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_NOT_MODIFIED)
                        {
                            rlTaskDebug("HTTP request returned 304.");
                            this->Finish(FINISH_SUCCEEDED, NET_HTTPSTATUS_NOT_MODIFIED);
                        }
                        else if (ProcessData())
                        {
                            rlTaskDebug("Processed raw data");
                            GetSucceededAction action = OnGetSucceeded();
                            if (action == GetSucceededAction::Action_ProcessContent)
                            {
                                rlTaskDebug("GetSucceededAction::Action_ProcessContent - Processing content");
                                m_State = State::State_Processing;
                            }
                            else if (action == GetSucceededAction::Action_Failed)
                            {
                                rlTaskDebug("GetSucceededAction::Action_Failed - Bad data");
                                this->Finish(FINISH_FAILED);
                            }
                            else
                            {
                                rlTaskDebug("GetSucceededAction::Action_Complete");
                                this->Finish(FINISH_SUCCEEDED);
                            }
                        }
                        else
                        {
                            rlTaskDebug("Failed to process data");
                            this->Finish(FINISH_FAILED);
                        }
                    }
                    else
                    {
                        GetFailedAction action = OnGetFailed();
                        if (action == GetFailedAction::Action_Retry)
                        {
                            rlTaskDebug("GetFailedAction::Action_Retry - Retrying");
                            Reset();
                            m_State = State::State_Get;
                        }
                        else if (action == GetFailedAction::Action_Fail)
                        {
                            rlTaskDebug("GetFailedAction::Action_Fail - Fail");
                            this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
                        }
                    }
                }
            }
        }
        else
        {
            rlTaskError("Status failed");
            m_State = State::State_Failed;
        }
        break;

    case State::State_Processing:
    {
        int errorCode = 0;
        ProcessContentResult result = ProcessContent(timeStep, errorCode);
        if (result == ProcessContentResult::Process_Complete)
        {
            rlTaskDebug("Processing complete");
            this->Finish(FINISH_SUCCEEDED);
        }
        else if (result == ProcessContentResult::Process_Failed)
        {
            rlTaskDebug("Processing failed, Code: 0x%08x", errorCode);
            this->Finish(FINISH_FAILED, errorCode);
        }
    }
    break;

    case State::State_Failed:
    {
        rlTaskError("Failed to get %s, StatusCode: %d, ResultCode: %d",
            m_HttpRequest.GetUri(),
            m_HttpRequest.GetStatusCode(),
            m_MyStatus.GetResultCode());

        m_HttpRequest.Clear();
        this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
    break;

    case State::State_Invalid:
    default:
        rlAssert(false);
    }
}

bool
rlUgcGetCdnContentTask::ReadContent()
{
    unsigned amountReceived;
    u8 readBuffer[HTTP_READ_BUFFER_SIZE], writeBuffer[HTTP_READ_BUFFER_SIZE];

    m_HttpRequest.ReadBody(readBuffer, HTTP_READ_BUFFER_SIZE, writeBuffer, HTTP_READ_BUFFER_SIZE, &amountReceived);

    // if we received content
    if (amountReceived > 0)
    {
        // write to our grow buffer
        const unsigned nWritten = static_cast<unsigned>(m_GrowBuffer->Append(writeBuffer, amountReceived));

        rlTaskDebug("ReadContent :: Recieved %ub and wrote %ub into m_GrowBuffer", amountReceived, nWritten);

        // check that all content was written
        if (nWritten != amountReceived)
        {
            rlTaskError("ReadContent :: Failed to write %ub", amountReceived);
            return false;
        }
    }

    // content was read successfully or there was no content
    return true;
}

bool
rlUgcGetCdnContentTask::Get()
{
    bool success = false;

    rtry
    {
        // use global rline allocator if one wasn't provided
        m_HttpRequest.Init(m_Allocator ? m_Allocator : g_rlAllocator, rlRos::GetRosSslContext());

		// if we weren't provided with a URL, ask the task for it
		if (!m_UrlProvided)
		{
			GetUrl(m_Url, RL_MAX_URL_BUF_LENGTH);
		}
		rlTaskDebug("Url: %s", m_Url);

		// configure filter
		m_Filter.Configure(m_LocalGamerIndex,
						   m_Url,
						   RLROS_SECURITY_DEFAULT);

		// disable this for now
		m_HttpRequest.SetUrlEncode(false);

#if !__NO_OUTPUT
		// this is the string used in logging
		static const unsigned MAX_CONTEXT_STRING = 256;
		char szContextStr[MAX_CONTEXT_STRING];
		formatf(szContextStr, "%s[%u]", this->GetTaskName(), this->GetTaskId());
#else
        const char* szContextStr = nullptr;
#endif

        // initialise get
        rverify(m_HttpRequest.BeginGet(m_Url,
                                       nullptr, // proxyAddr
                                       HTTP_REQUEST_TIMEOUT_SECONDS,
                                       szContextStr,
                                       &m_Filter,
                                       &m_MyStatus),
                catchall,
                rlTaskError("Error beginning Get"));

        // add the last modified time from the cached version of the file; the server should return 304 if
        // we can use the cached version instead of downloading it.
        {
            char http_date[256];
            netHttpRequest::MakeDateString(m_LastModifiedTime, http_date);
            m_HttpRequest.AddRequestHeaderValue("If-Modified-Since", http_date);
        }

        rverify(m_HttpRequest.Commit(),
                catchall,
                rlTaskError("Error committing Get"));

        success = true;
    }
	rcatchall
    {

    }

    return success;
}

bool rlUgcGetCdnContentTask::ProcessData()
{
    // track success
    bool bSuccess = true;

    // grab data
    unsigned nData = m_GrowBuffer->Length();
    if (nData == 0)
    {
        rlTaskWarning("ProcessData: Buffer is empty!");
        return true;
    }

    u8* pData = reinterpret_cast<u8*>(m_GrowBuffer->GetBuffer());

    // track how we need to chop up our grow buffer to get the actual transformed payload
    unsigned nRemoveBytes = 0;

    // validate signature if required
    unsigned nPayloadSize = nData;
    if (bSuccess && (m_Flags & rlUgcCdnContentFlags::CF_ValidateSignature) != 0)
    {
        bSuccess = ValidateSignature(pData, nData, &nPayloadSize);
        rlTaskDebug("Validating Signature %s", bSuccess ? "Succeeded" : "Failed");

#if !__NO_OUTPUT
        if ((m_Flags & rlUgcCdnContentFlags::CF_DumpData) != 0)
        {
            char szLogNameString[RAGE_MAX_PATH];
            formatf(szLogNameString, "%s_%u_Signature.log", this->GetTaskName(), this->GetTaskId());
            WriteData(pData, nPayloadSize, szLogNameString);
        }
#endif  
    }

    // decrypt if required
    if (bSuccess && (m_Flags & rlUgcCdnContentFlags::CF_Decrypt) != 0)
    {
        bSuccess = Decrypt(pData, nPayloadSize);
        rlTaskDebug("Decrypt %s", bSuccess ? "Succeeded" : "Failed");

#if !__NO_OUTPUT
        if ((m_Flags & rlUgcCdnContentFlags::CF_DumpData) != 0)
        {
            char szLogNameString[RAGE_MAX_PATH];
            formatf(szLogNameString, "%s_%u_Decrypt.log", this->GetTaskName(), this->GetTaskId());
            WriteData(pData, nPayloadSize, szLogNameString);
        }
#endif
    }

    // decompress if required
    if (bSuccess && (m_Flags & rlUgcCdnContentFlags::CF_Decompress) != 0)
    {
        // allocate memory for uncompressed data
        static const unsigned MAX_DECOMPRESSED_BUFFER_SIZE = 2176 * 1024;
        static const unsigned DECOMPRESSED_BUFFER_ALIGN = 16;
        u8* pUncompressedBuffer = reinterpret_cast<u8*>(m_Allocator->TryAllocate(MAX_DECOMPRESSED_BUFFER_SIZE, DECOMPRESSED_BUFFER_ALIGN));
        if (pUncompressedBuffer == nullptr)
        {
            rlTaskError("Couldn't allocate decompress buffer of max size %u alignment %u", MAX_DECOMPRESSED_BUFFER_SIZE, DECOMPRESSED_BUFFER_ALIGN);
            return false;
        }
        else
        {
            // decompress
            unsigned nUncompressed = Decompress(pData, nPayloadSize, pUncompressedBuffer, MAX_DECOMPRESSED_BUFFER_SIZE);
            bSuccess &= (nUncompressed > 0);
            rlTaskDebug("Decompress %s", bSuccess ? "Succeeded" : "Failed");

            // if we succeeded, write the uncompressed data into our grow buffer
            if (bSuccess)
            {
                m_GrowBuffer->Clear();
                m_GrowBuffer->Append(pUncompressedBuffer, nUncompressed);

#if !__NO_OUTPUT
                if ((m_Flags & rlUgcCdnContentFlags::CF_DumpData) != 0)
                {
                    char szLogNameString[RAGE_MAX_PATH];
                    formatf(szLogNameString, "%s_%u_Decompress.log", this->GetTaskName(), this->GetTaskId());
                    WriteData(pUncompressedBuffer, nUncompressed, szLogNameString);
                }
#endif
            }

            // release uncompressed buffer
            m_Allocator->Free(pUncompressedBuffer);
        }
    }
    else
    {
        // trim and truncate the grow buffer to the actual data (we do this automatically when we decompress)
        m_GrowBuffer->Remove(0, nRemoveBytes);
        m_GrowBuffer->Truncate(nPayloadSize);
    }

    return bSuccess;
}

bool rlUgcGetCdnContentTask::ValidateSignature(const u8* pData, const unsigned nData, unsigned* pPayloadSize)
{
    static const unsigned SIZEOF_PAYLOAD_DATA_SIZE = 4;

    rtry
    {
        rverify(nData >= SIZEOF_PAYLOAD_DATA_SIZE, catchall, rlTaskError("Data size too small. Data: %u", nData));

        // payload is last 4 bytes in data
        unsigned nPayloadSize;
        memcpy(&nPayloadSize, pData + (nData - SIZEOF_PAYLOAD_DATA_SIZE), SIZEOF_PAYLOAD_DATA_SIZE);
        // signature size will be the total size, minus payload size, minus 4 bytes indicating payload size
        unsigned nSignatureSize = nData - nPayloadSize - SIZEOF_PAYLOAD_DATA_SIZE;
        rlTaskDebug("Datasize: %u, PayloadSize: %u, SignatureSize: %u", nData, nPayloadSize, nSignatureSize);

        rverify(nPayloadSize <= nData - SIZEOF_PAYLOAD_DATA_SIZE, catchall,
                rlTaskError("Payload size out of bound. Data: %u, PayloadSize: %u", nData, nPayloadSize));
        rverify(nSignatureSize > 0 && nSignatureSize <= RLROS_MAX_RSA_ENCRYPTED_SIZE, catchall, 
                rlTaskError("Signature size out of bound. Data: %u, PayloadSize: %u, SignatureSize: %u", nData, nPayloadSize, nSignatureSize));

        if (pPayloadSize)
        {
            // copy in if supplied
            *pPayloadSize = nPayloadSize;
        }

        u8* pSignatureBytes = Alloca(u8, nSignatureSize);
        memcpy(pSignatureBytes, pData + nPayloadSize, nSignatureSize);

        // compute SHA1 hash
        Sha1 sha1;
        u8 aDigest[Sha1::SHA1_DIGEST_LENGTH] = { 0 };
        sha1.Update(pData, nPayloadSize);
        sha1.Final(aDigest);

        // copy into signature structure
        rlRosContentSignature signature;
        memcpy(signature.m_ContentSignature, pSignatureBytes, RLROS_MAX_RSA_ENCRYPTED_SIZE);
        signature.m_Length = nSignatureSize;

        // build relative url
        char relUrl[RL_MAX_URL_BUF_LENGTH];
        GetRelativeUrl(m_Url, relUrl, RL_MAX_URL_BUF_LENGTH);

        return rlCloud::VerifyCloudSignature(signature, relUrl, aDigest, Sha1::SHA1_DIGEST_LENGTH);
    }
    rcatchall
    {
    }

    return false;
}

bool rlUgcGetCdnContentTask::Decrypt(u8* pData, const unsigned nData)
{
    rlTaskDebug("Decrypt: Decrypting %ub", nData);

    rtry
    {
        // decrypt
        rverify(AES::GetCloudAes() != nullptr, catchall, rlTaskError("GetCloudAes is null"));
        rverify(AES::GetCloudAes()->Decrypt(pData, nData), catchall, rlTaskError("Failed to decrypt"));
        
        rlTaskDebug("Decrypted %u bytes", nData);

        return true;
    }
    rcatchall
    {
        return false;
    }
}

unsigned rlUgcGetCdnContentTask::Decompress(const u8* pData, const unsigned nData, u8* pUncompressBuffer, const unsigned nUncompressMaxSize)
{
    rlTaskDebug("Decompress: Decompressing %ub", nData);

    // create 10k working buffer for zlib
    static const unsigned ZLIB_HEAP_SIZE = 10 * 1024;
    u8 zlibHeap[ZLIB_HEAP_SIZE];
    sysMemSimpleAllocator zlibAllocator(zlibHeap, COUNTOF(zlibHeap), sysMemSimpleAllocator::HEAP_NET);

    // create zlib stream and initialise decoding
    zlibStream zstrm;
    zstrm.BeginDecode(&zlibAllocator);

    // tracking variables
    unsigned totalConsumed = 0, totalProduced = 0;
    unsigned numConsumed, numProduced;

    // consume all bytes from submission into the zlib buffer.
    while (totalConsumed < nData && !zstrm.IsComplete() && !zstrm.HasError())
    {
        zstrm.Decode(&pData[totalConsumed],
            nData - totalConsumed,
            &pUncompressBuffer[totalProduced],
            nUncompressMaxSize - totalProduced,
            &numConsumed,
            &numProduced);

        rlTaskDebug("Decoding #1: Consumed: %u / %u, Produced: %u / %u", numConsumed, totalProduced, numProduced, totalProduced);

        totalConsumed += numConsumed;
        totalProduced += numProduced;
    }

    // produce any remaining bytes into the zlib buffer.
    while (!zstrm.IsComplete() && !zstrm.HasError())
    {
        zstrm.Decode(nullptr,
            0,
            &pUncompressBuffer[totalProduced],
            nUncompressMaxSize - totalProduced,
            &numConsumed,
            &numProduced);

        rlTaskDebug("Decoding #2: Consumed: %u / %u, Produced: %u / %u", numConsumed, totalProduced, numProduced, totalProduced);

        totalConsumed += numConsumed;
        totalProduced += numProduced;
    }

    rlTaskDebug("Decompress %s - Consumed: %u, Produced: %u, Error: 0x%0x", zstrm.HasError() ? "Failed" : "Succeeded", totalConsumed, totalProduced, zstrm.GetErrorCode());

    return (zstrm.IsComplete() && !zstrm.HasError()) ? totalProduced : 0;
}

const char*
rlUgcGetCdnContentTask::GetRelativeUrl(const char*, char*, const unsigned)
{
    rlTaskAssertf(0, "GetRelativeUrl called with no implementation");
    return nullptr;
}

const char*
rlUgcGetCdnContentTask::GetUrl(char*, const unsigned)
{
    rlTaskAssertf(0, "GetUrl called with no implementation");
    return nullptr;
}

#if !__NO_OUTPUT
void rlUgcGetCdnContentTask::WriteData(const u8* pData, const unsigned nData, const char* fileName)
{
    char fullpath[RAGE_MAX_PATH];
    formatf(fullpath, "x:/dumps/%s", fileName);

    fiStream* pStream(ASSET.Create(fullpath, ""));
    if (pStream)
    {
        pStream->Write(pData, nData);
        pStream->Close();
    }
}
#endif

bool
ParseTemplates(const char* buffer, const int bufferSize, atArray<rlUgcUrlTemplate>& results)
{
    RsonReader rr;
    if (!rlVerify(rr.Init(buffer, bufferSize))) return false;
    if (!rlVerify(rr.GetMember(rlUgcUrlTemplate::TEMPLATE_ROOT, &rr))) return false;
    if (!rlVerify(rlUgcCreateUrlTemplatesFromJson(rr, results))) return false;
    return true;
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcGetFeaturedContentDataTask
////////////////////////////////////////////////////////////////////////////////
rlUgcGetFeaturedContentDataTask::rlUgcGetFeaturedContentDataTask(sysMemAllocator* allocator)
    : rlUgcGetCdnContentTask(allocator)
    , m_bAllowLanguageRetryfor404(false)
{

}

bool
rlUgcGetFeaturedContentDataTask::Configure(
    const int localGamerIndex,
    datGrowBuffer* growBuffer,
    const rlUgcTokenValues& tokenValues,
    const rlUgcUrlTemplate* urlTemplate,
    rlCloudFileInfo* fileInfo,
    u64 lastModifiedTimeFromCache)
{
    bool success = false;

    rtry
    {
        const unsigned nFlags = urlTemplate->templateFlags;

        // validate parameters
        rverifyall(rlUgcGetCdnContentTask::Configure(localGamerIndex, growBuffer, nullptr, nFlags, fileInfo, lastModifiedTimeFromCache));
        rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
        rverifyall(urlTemplate != nullptr);

        // copy in parameters 
        m_TokenValues = tokenValues;
        m_UrlTemplate = urlTemplate;

        success = true;
    }
    rcatchall
    {

    }

    // configure base class
    return success;
}

void
rlUgcGetFeaturedContentDataTask::Start()
{
    if (m_TokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::Language) && (m_TokenValues.GetLanguage() != rlScLanguage::RLSC_LANGUAGE_ENGLISH))
    {
        rlTaskDebug("Attempting for %s. If not found, will retry with %s", rlScLanguageToString(m_TokenValues.GetLanguage()), rlScLanguageToString(rlScLanguage::RLSC_LANGUAGE_ENGLISH));
        m_bAllowLanguageRetryfor404 = true;
    }

    rlUgcGetCdnContentTask::Start();
}

rlUgcGetCdnContentTask::GetFailedAction
rlUgcGetFeaturedContentDataTask::OnGetFailed()
{
    // if we have requested a language other than English, fallback to English if we get a 404 / not found
    if ((m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_NOT_FOUND) &&
        m_bAllowLanguageRetryfor404 &&
        m_TokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::Language) &&
        (m_TokenValues.GetLanguage() != rlScLanguage::RLSC_LANGUAGE_ENGLISH))
    {
        rlTaskDebug("File not found for %s. Retrying with %s", rlScLanguageToString(m_TokenValues.GetLanguage()), rlScLanguageToString(rlScLanguage::RLSC_LANGUAGE_ENGLISH));

        m_bAllowLanguageRetryfor404 = false;
        m_TokenValues.SetLanguage(rlScLanguage::RLSC_LANGUAGE_ENGLISH);
        return GetFailedAction::Action_Retry;
    }

    return GetFailedAction::Action_Fail;
}

const char*
rlUgcGetFeaturedContentDataTask::GetRelativeUrl(const char* url, char* dst, const unsigned dstMaxLength)
{
    atString fullUrl(url);
    atString host, relativeUrl;

    dst[0] = '\0';

    // split after the service
    fullUrl.Split(host, relativeUrl, ".net");
    if (relativeUrl.length() > 0)
    {
        safecpy(dst, relativeUrl.c_str(), dstMaxLength);
    }

    return dst;
}

const char*
rlUgcGetFeaturedContentDataTask::GetUrl(char* dst, const unsigned dstMaxLength)
{
    rlTaskDebug("Using template with format: %s", m_UrlTemplate->templateUrl.c_str());

    rlTaskVerifyf(
        rlUgcBuildUrlFromTemplate(m_UrlTemplate->templateUrl, m_UrlTemplate->tokens, m_TokenValues, dst, dstMaxLength),
        "Error building URL from template");

    return dst;
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcGetUrlTemplatesTask
////////////////////////////////////////////////////////////////////////////////
rlUgcGetUrlTemplatesTask::rlUgcGetUrlTemplatesTask(sysMemAllocator* allocator)
    : rlUgcGetCdnContentTask(allocator)
    , m_ResultTemplates(nullptr)
{
}

bool
rlUgcGetUrlTemplatesTask::Configure(
    const int localGamerIndex,
    const char* url,
    const rlUgcCdnContentFlags flags,
    atArray<rlUgcUrlTemplate>* resultTemplates)
{
    rtry
    {
        // validate parameters
        rverifyall(rlUgcGetCdnContentTask::Configure(localGamerIndex, nullptr, url, (unsigned)flags));
        rverifyall(resultTemplates);

        // copy in parameters
        m_ResultTemplates = resultTemplates;
    }
    rcatchall
    {
        return false;
    }

        // configure base class
    return true;
}

rlUgcGetCdnContentTask::GetSucceededAction
rlUgcGetUrlTemplatesTask::OnGetSucceeded()
{
    const char* data = (const char*)m_GrowBuffer->GetBuffer();
    const int dataLength = m_GrowBuffer->Length();

#if !__NO_OUTPUT
    rlTaskDebug("JSON:");
    diagLoggedPrintLn(data, dataLength);
#endif

    if (ParseTemplates(data, dataLength, *m_ResultTemplates))
    {
        return GetSucceededAction::Action_Complete;
    }
    else
    {
        return GetSucceededAction::Action_Failed;
    }
}

const char*
rlUgcGetUrlTemplatesTask::GetRelativeUrl(const char* url, char* dst, const unsigned dstMaxLength)
{
    atString fullUrl(url);
    atString host, relativeUrl;

    dst[0] = '\0';

    // split after the service
    fullUrl.Split(host, relativeUrl, ".com");
    if (relativeUrl.length() > 0)
    {
        safecpy(dst, relativeUrl.c_str(), dstMaxLength);
    }

    return dst;
}

}; //namespace rage
