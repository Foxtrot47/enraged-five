// 
// rline/rlugccommon.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlugccommon.h"
#include "data/rson.h"
#include "diag/seh.h"
#include "net/status.h"
#include "string/stringbuilder.h"
#include "string/stringutil.h"
#include "system/memory.h"
#include "system/nelem.h"

namespace rage
{

extern sysMemAllocator* g_rlAllocator;
    
RAGE_DEFINE_SUBCHANNEL(rline, ugc);
#undef __rage_channel
#define __rage_channel rline_ugc

const char* rlUgcUrlTemplate::TEMPLATE_ROOT = "FileTemplates";
const char* rlUgcUrlTemplate::TEMPLATE_NAME = "Name";
const char* rlUgcUrlTemplate::TEMPLATE_URL = "UrlTemplate";
const char* rlUgcUrlTemplate::TEMPLATE_FLAGS = "Flags";

static const char* s_rlUgcCategoryStr[RLUGC_NUM_CATEGORIES] = 
{ 
    "",                 //RLUGC_CATEGORY_NONE
    "rstar",            //RLUGC_CATEGORY_ROCKSTAR_CREATED
    "rstarcan",         //RLUGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE
    "verif",            //RLUGC_CATEGORY_ROCKSTAR_VERIFIED
	"verifcan",         //RLUGC_CATEGORY_ROCKSTAR_VERIFIED_CANDIDATE
	"community",		//RLUGC_CATEGORY_ROCKSTAR_COMMUNITY
	"communitycan"      //RLUGC_CATEGORY_ROCKSTAR_COMMUNITY_CANDIDATE
};

rlUgcCategory
rlUgcCategoryFromString(const char* category)
{
    if (category == NULL) return RLUGC_CATEGORY_NONE;

    char cat[RLUGC_MAX_CATEGORY_CHARS];
    StringTrim(cat, category, sizeof(cat));

    for(int i = 0; i < RLUGC_NUM_CATEGORIES; i++)
    {
        if (!stricmp(cat, s_rlUgcCategoryStr[i])) 
        {
            return (rlUgcCategory)(RLUGC_FIRST_CATEGORY + i);
        }
    }
    
    rlError("Unknown or invalid category (%s)", category);
    return RLUGC_CATEGORY_UNKNOWN;
}

const char*
rlUgcCategoryToString(rlUgcCategory category)
{
    int index = (int)category - (int)RLUGC_FIRST_CATEGORY;

    if (index >= 0 && index < RLUGC_NUM_CATEGORIES)
    {
        return s_rlUgcCategoryStr[index];
    }

    rlError("Unknown or invalid category (%d)", (int)category);
    return "UNKNOWN";
}

rlUgcContentType
rlUgcContentTypeFromString(const char* contentType)
{
    if (contentType != NULL) 
    {
        char ct[RLUGC_MAX_CONTENT_TYPE_CHARS];
        StringTrim(ct, contentType, sizeof(ct));

#if __RGSC_DLL
		// keeping references to "GTA5" out of the Social Club DLL
		rlAssert(false);
#else
        //TODO: Need to eventually get script to change to new names, in case future titles reuse the old names
        if (!stricmp(ct, "gta5mission")) return RLUGC_CONTENT_TYPE_GTA5MISSION;
        if (!stricmp(ct, "gta5playlist") || !stricmp(ct, "playlist")) return RLUGC_CONTENT_TYPE_GTA5PLAYLIST;
        if (!stricmp(ct, "lifeinvaderpost")) return RLUGC_CONTENT_TYPE_LIFEINVADERPOST;
        if (!stricmp(ct, "gta5photo") || !stricmp(ct, "photo")) return RLUGC_CONTENT_TYPE_GTA5PHOTO;
        if (!stricmp(ct, "gta5challenge") || !stricmp(ct, "challenge")) return RLUGC_CONTENT_TYPE_GTA5CHALLENGE;
        if (!stricmp(ct, "gta5video")) return RLUGC_CONTENT_TYPE_GTA5VIDEO;
#if RL_UGC_YOUTUBE
        if (!stricmp(ct, "gta5youtube")) return RLUGC_CONTENT_TYPE_GTA5YOUTUBE;
#endif // RL_UGC_YOUTUBE
#endif
    }
    
    rlError("Unknown or invalid content type (%s)", contentType);
    return RLUGC_CONTENT_TYPE_UNKNOWN;
}

const char*
rlUgcContentTypeToString(rlUgcContentType contentType)
{
#if __RGSC_DLL
	// keeping references to "GTA5" out of the Social Club DLL
	contentType;
	rlAssert(false);
	return "UNKNOWN";
#else

    switch(contentType)
    {
    case RLUGC_CONTENT_TYPE_GTA5MISSION: return "gta5mission";
    case RLUGC_CONTENT_TYPE_GTA5PLAYLIST: return "gta5playlist";
    case RLUGC_CONTENT_TYPE_LIFEINVADERPOST: return "lifeinvaderpost";
    case RLUGC_CONTENT_TYPE_GTA5PHOTO: return "gta5photo";
    case RLUGC_CONTENT_TYPE_GTA5CHALLENGE: return "gta5challenge";
    case RLUGC_CONTENT_TYPE_GTA5VIDEO: return "gta5video";
#if RL_UGC_YOUTUBE
    case RLUGC_CONTENT_TYPE_GTA5YOUTUBE: return "gta5youtube";
#endif // RL_UGC_YOUTUBE

    default:
        rlError("Unknown or invalid content type (%d)", (int)contentType);
        return "UNKNOWN";
    }
#endif
}

rlUgcCdnContentFlags rlUgcContentFlagFromString(const char* contentType)
{
	// Do not modify the strings, can be passed through server-side

	if (StringNullOrEmpty(contentType)) { return (rlUgcCdnContentFlags)0; }

	if (stricmp(contentType, "ValidateSignature") == 0) { return CF_ValidateSignature; }
	if (stricmp(contentType, "Decrypt") == 0) { return CF_Decrypt; }
	if (stricmp(contentType, "Decompress") == 0) { return CF_Decompress; }
	if (stricmp(contentType, "DumpData") == 0) { return CF_DumpData; }

	return (rlUgcCdnContentFlags)0;
}

const char* rlUgcContentFlagToString(rlUgcCdnContentFlags contentType)
{
	switch (contentType)
	{
	case CF_ValidateSignature: return "ValidateSignature";
	case CF_Decrypt: return "Decrypt";
	case CF_Decompress: return "Decompress";
	case CF_DumpData: return "DumpData";
	default: break;
	}

	return "";
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcContentInfo
////////////////////////////////////////////////////////////////////////////////
rlUgcContentInfo::rlUgcContentInfo()
{
	m_Stats.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

	Clear();
}

rlUgcContentInfo::rlUgcContentInfo(const rlUgcMetadata* metadata,
								   const rlUgcRatings* ratings,
								   const char* statsJson)
{
	m_Stats.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

	m_Metadata = *metadata;
	m_Ratings = *ratings;

	if(statsJson)
	{
		m_Stats.Append(statsJson, static_cast<unsigned>(strlen(statsJson)));
	}
}

void 
rlUgcContentInfo::Clear() 
{
    m_Metadata.Clear();
    m_Ratings.Clear();
    m_Stats.Clear();
}

bool
rlUgcContentInfo::IsValid() const
{
    return m_Metadata.IsValid();
}

rlUgcContentInfo::rlUgcContentInfo(const rlUgcContentInfo& other)
{
	*this = other;
}

rlUgcContentInfo& 
rlUgcContentInfo::operator=(const rlUgcContentInfo& other)
{
	m_Metadata = other.m_Metadata;
	m_Ratings = other.m_Ratings;
	m_Stats.Append(other.GetStats(), other.GetStatsSize());
	return *this;
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcContentCreatorInfo
////////////////////////////////////////////////////////////////////////////////
rlUgcContentCreatorInfo::rlUgcContentCreatorInfo()
{
    m_Stats.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

    Clear();
}

void 
rlUgcContentCreatorInfo::Clear()
{
    m_AccountId = InvalidPlayerAccountId;
    m_UserId[0] = '\0';
    m_UserName[0] = '\0';
    m_RockstarId = InvalidRockstarId;

    m_NumCreated = 0;
    m_NumPublished = 0;

    m_Ratings.Clear();
    m_Stats.Clear();
}

bool
rlUgcContentCreatorInfo::IsValid() const
{
    return (m_AccountId != InvalidPlayerAccountId) && (ustrlen(m_UserId) > 0);
}

rlUgcContentCreatorInfo::rlUgcContentCreatorInfo(const rlUgcContentCreatorInfo& other)
{
	*this = other;
}

rlUgcContentCreatorInfo& 
rlUgcContentCreatorInfo::operator=(const rlUgcContentCreatorInfo& other)
{
	m_AccountId = other.m_AccountId;
	safecpy(m_UserId, other.m_UserId);
	safecpy(m_UserName, other.m_UserName);
	m_NumCreated = other.m_NumCreated;
	m_RockstarId = other.m_RockstarId;
	m_NumPublished = other.m_NumPublished;
	m_Ratings = other.m_Ratings;
	m_Stats.Append(other.m_Stats.GetBuffer(), other.m_Stats.Length());

	return *this;
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcMetadata
////////////////////////////////////////////////////////////////////////////////
rlUgcMetadata::rlUgcMetadata()
{
    m_Data.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);
    m_Description.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

    Clear();
}

void 
rlUgcMetadata::Clear()
{
    m_AccountId = InvalidPlayerAccountId;
    m_Category = RLUGC_CATEGORY_NONE;
    m_ContentId[0] = '\0';
    m_ContentName[0] = '\0';
    m_ContentType = RLUGC_CONTENT_TYPE_UNKNOWN;
    m_CreatedDate = 0;
    m_Data.Clear();
    m_Description.Clear();
    m_IsVerified = false;
    m_Language = RLSC_LANGUAGE_ENGLISH;
    m_PublishedDate = 0;
    m_RockstarId = InvalidRockstarId;
    m_RootContentId[0] = '\0';
    m_UpdatedDate = 0;
    m_UserId[0] = '\0';
    m_UserName[0] = '\0';
    m_Version = -1;

    sysMemSet(m_FileVersion, 0, sizeof(m_FileVersion));
}

bool 
rlUgcMetadata::ComposeCloudAbsPath(const int fileId, 
                                   char* cloudAbsPath, 
                                   const unsigned sizeofCloudAbsPath) const
{
    if (rlVerifyf(fileId < NELEM(m_FileVersion), "File ID (%d) out of range (0..%d)", fileId, NELEM(m_FileVersion) - 1))
    {
        return ComposeCloudAbsPath(m_ContentType,
                                   m_ContentId,
                                   fileId,
                                   m_FileVersion[fileId],
                                   m_Language,
                                   cloudAbsPath,
                                   sizeofCloudAbsPath);
    }

    return false;
}

bool
rlUgcMetadata::IsValid() const
{
    return ustrlen(m_ContentId) > 0;
}

rlUgcMetadata::rlUgcMetadata(const rlUgcMetadata& other)
{
    *this = other;
}

rlUgcMetadata& 
rlUgcMetadata::operator=(const rlUgcMetadata& other)
{
    m_ContentType = other.m_ContentType;
    safecpy(m_ContentId, other.m_ContentId);
    m_AccountId = other.m_AccountId;
    m_Category = other.m_Category;
    safecpy(m_ContentName, other.m_ContentName);
    m_CreatedDate = other.m_CreatedDate;
    m_Description.Clear();
    m_Description.Append(other.GetDescription(), other.m_Description.Length());
    m_Data.Clear();
    m_Data.Append(other.GetData(), other.GetDataSize());
    m_IsVerified = other.m_IsVerified;
    m_Language = other.m_Language;
    m_PublishedDate = other.m_PublishedDate;
    m_RockstarId = other.m_RockstarId;
    safecpy(m_RootContentId, other.m_RootContentId);
    m_UpdatedDate = other.m_UpdatedDate;
    safecpy(m_UserId, other.m_UserId);
    safecpy(m_UserName, other.m_UserName);
    m_Version = other.m_Version;

    for(int i = 0; i < RLUGC_MAX_FILES; ++i)
    {
        m_FileVersion[i] = other.m_FileVersion[i];
    }

    return *this;
}

int
rlUgcMetadata::GetNumFiles() const
{
    switch(m_ContentType)
    {
    case RLUGC_CONTENT_TYPE_GTA5CHALLENGE:  
    case RLUGC_CONTENT_TYPE_GTA5PLAYLIST: 
        return 0;

    case RLUGC_CONTENT_TYPE_GTA5PHOTO: 
    case RLUGC_CONTENT_TYPE_LIFEINVADERPOST: 
        return 1;

    case RLUGC_CONTENT_TYPE_GTA5MISSION: 
        return 2; //Content and photo

    default:
        rlError("rlUgcMetadata::GetNumFiles() not implemented for content type %d", m_ContentType);
        return -1;
    }
}

int 
rlUgcMetadata::GetFileId(const int index) const
{
    if (rlVerify((index >= 0) && (index < RLUGC_MAX_FILES)))
    {
        return index; //For the content types we know of right now, the index of all files is also their ID.
    }

    return -1;
}

int 
rlUgcMetadata::GetFileVersion(const int index) const
{
    if (rlVerify((index >= 0) && (index < RLUGC_MAX_FILES)))
    {
        return m_FileVersion[index];
    }

    return -1;
}

bool 
rlUgcMetadata::ComposeCloudAbsPath(const rlUgcContentType contentType,
                                   const char* contentId,
                                   const int fileId, 
                                   const int fileVersion,
                                   const rlScLanguage language,
                                   char* cloudAbsPath, 
                                   const unsigned sizeofCloudAbsPath)
{
    //Determine the extension by the content type and file ID
    const char* extension = NULL;
    bool localized = false;

    switch(contentType)
    {
    case RLUGC_CONTENT_TYPE_GTA5CHALLENGE: 
        if (fileId == 0) extension = "json";
        break;

    case RLUGC_CONTENT_TYPE_GTA5MISSION: 
        if (fileId == 0) 
        {
            extension = "json"; 
            localized = true;
        }
		else if (fileId == 1 || fileId == 2) 
		{
			extension = "jpg";    
		}
        break;

    case RLUGC_CONTENT_TYPE_GTA5PHOTO: 
		if (fileId == 0 || fileId == 1)
		{
			extension = "jpg"; 
		}
        break;

    case RLUGC_CONTENT_TYPE_GTA5PLAYLIST: 
        if (fileId == 0) extension = "json"; 
        break;

    default:
        break;
    }

    if (extension == NULL)
    {
        rlError("Could not determine extension of %s fileId %d", rlUgcContentTypeToString(contentType), fileId);
        return false;
    }

    //Compose the final path
    if (localized)
    {
        formatf(cloudAbsPath, sizeofCloudAbsPath, "/ugc/%s/%s/%d_%d_%s.%s",
                rlUgcContentTypeToString(contentType),
                contentId,
                fileId,
                fileVersion,
                rlScLanguageToString(language),
                extension);
    }
    else
    {
        formatf(cloudAbsPath, sizeofCloudAbsPath, "/ugc/%s/%s/%d_%d.%s",
            rlUgcContentTypeToString(contentType),
            contentId,
            fileId,
            fileVersion,
            extension);
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcGta5VideoMetadataData
////////////////////////////////////////////////////////////////////////////////
rlUgcGta5VideoMetadataData::rlUgcGta5VideoMetadataData()
{
    Clear();
}

void
rlUgcGta5VideoMetadataData::Clear()
{
    m_StagingName[0]
    = m_StagingUrl[0]
    = m_StagingAuthToken[0]
    = m_StagingRefId[0]
    = m_MediaId[0]
    = m_FinalUrl[0]
    = '\0';
}

bool
rlUgcGta5VideoMetadataData::SetMetadata(const rlUgcMetadata&  md)
{
    rtry
    {
        rverify(md.GetContentType() == RLUGC_CONTENT_TYPE_GTA5VIDEO,
                catchall,
                rlError("Content is '%s', expected '%s'",
                        rlUgcContentTypeToString(md.GetContentType()),
                        rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_GTA5VIDEO)));

        Clear();

        RsonReader json(md.GetData(), md.GetDataSize());
        rverify(json.GetMember("cdn", &json), catchall, rlError("No 'cdn' member in gta5video metadata"));
        rverify(json.GetValue("name", m_StagingName), catchall, rlError("No 'cdn.name' member in gta5video metadata"));
        rverify(json.GetValue("stageurl", m_StagingUrl), catchall, rlError("No 'cdn.stageurl' member in gta5video metadata"));
        rverify(json.GetValue("authtoken", m_StagingAuthToken), catchall, rlError("No 'cdn.authtoken' member in gta5video metadata"));
        rverify(json.GetValue("stageid", m_StagingRefId), catchall, rlError("No 'cdn.stageid' member in gta5video metadata"));

        //Media ID will only be present after the video has been ingested
        //by the video distribution platform.
        json.GetValue("id", m_MediaId);

        //Final URL will only be present after the video has been ingested
        //by the video distribution platform.
        json.GetValue("url", m_FinalUrl);

        return true;
    }
    rcatchall
    {
        Clear();
    }

    return false;
}

const char*
rlUgcGta5VideoMetadataData::GetStagingId() const
{
    return m_StagingRefId;
}

const char*
rlUgcGta5VideoMetadataData::GetStagingUrl() const
{
    return m_StagingUrl;
}

const char*
rlUgcGta5VideoMetadataData::GetStagingAuthToken() const
{
    return m_StagingAuthToken;
}

const char*
rlUgcGta5VideoMetadataData::GetStagingName() const
{
    return m_StagingName;
}

const char*
rlUgcGta5VideoMetadataData::GetMediaId() const
{
    return m_MediaId;
}

const char*
rlUgcGta5VideoMetadataData::GetUrl() const
{
    return m_FinalUrl;
}


////////////////////////////////////////////////////////////////////////////////
// rlUgcGta5PhotoMetadataData
////////////////////////////////////////////////////////////////////////////////
rlUgcGta5PhotoMetadataData::rlUgcGta5PhotoMetadataData()
{
	Clear();
}

void
rlUgcGta5PhotoMetadataData::Clear()
{
	m_StagingDirName[0]
	= m_StagingFileName[0]
	= m_StagingUrl[0]
	= m_StagingAuthToken[0]
	= '\0';
	m_Akamai = false;
}

bool
rlUgcGta5PhotoMetadataData::SetMetadata(const rlUgcMetadata&  md)
{
	rtry
	{
		rverify(md.GetContentType() == RLUGC_CONTENT_TYPE_GTA5PHOTO,
			catchall,
			rlError("Content is '%s', expected '%s'",
			rlUgcContentTypeToString(md.GetContentType()),
			rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_GTA5PHOTO)));

		Clear();

		RsonReader json(md.GetData(), md.GetDataSize());
		if (json.HasMember("cdndir"))
		{
			rlDebug2("akamai/cdn metadata found");
			rverify(json.GetValue("cdndir", m_StagingDirName), catchall, rlError("No 'cdndir' member in gta5photo metadata"));
			rverify(json.GetValue("cdnfn", m_StagingFileName), catchall, rlError("No 'cdnfn' member in gta5photo metadata"));
			rverify(json.GetValue("cdnurl", m_StagingUrl), catchall, rlError("No 'cdnurl' member in gta5photo metadata"));
			rverify(json.GetValue("cdnat", m_StagingAuthToken), catchall, rlError("No 'cdnat' member in gta5photo metadata"));
			m_Akamai = true;
		}
		else
		{
			rlDebug2("limelight metadata found");
			rverify(json.GetValue("lldir", m_StagingDirName), catchall, rlError("No 'lldir' member in gta5photo metadata"));
			rverify(json.GetValue("llfn", m_StagingFileName), catchall, rlError("No 'llfn' member in gta5photo metadata"));
			rverify(json.GetValue("llurl", m_StagingUrl), catchall, rlError("No 'llurl' member in gta5photo metadata"));
			rverify(json.GetValue("llat", m_StagingAuthToken), catchall, rlError("No 'llat' member in gta5photo metadata"));
		}

		return true;
	}
	rcatchall
	{
		Clear();
	}

	return false;
}

const char*
rlUgcGta5PhotoMetadataData::GetStagingUrl() const
{
	return m_StagingUrl;
}

const char*
rlUgcGta5PhotoMetadataData::GetStagingAuthToken() const
{
	return m_StagingAuthToken;
}

const char*
rlUgcGta5PhotoMetadataData::GetStagingDirName() const
{
	return m_StagingDirName;
}

const char*
rlUgcGta5PhotoMetadataData::GetStagingFileName() const
{
	return m_StagingFileName;
}

bool
rlUgcGta5PhotoMetadataData::UsesAkamai() const
{
	return m_Akamai;
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcPlayer
////////////////////////////////////////////////////////////////////////////////
rlUgcPlayer::rlUgcPlayer()
{
    m_Data.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);

    Clear();
}

void 
rlUgcPlayer::Clear()
{
    m_Data.Clear();
	m_AccountId = InvalidPlayerAccountId;
	m_RockstarId = InvalidRockstarId;
    m_Rating = -1.0f;
    m_UserId[0] = '\0';
    m_UserName[0] = '\0';
    m_Bookmarked = false;
}

bool
rlUgcPlayer::IsValid() const
{
    return (ustrlen(m_UserId) > 0);
}

rlUgcPlayer::rlUgcPlayer(const rlUgcPlayer& other)
{
	*this = other;
}

rlUgcPlayer& 
rlUgcPlayer::operator=(const rlUgcPlayer& other)
{
	m_AccountId = other.m_AccountId;
	safecpy(m_UserId, other.m_UserId);
	safecpy(m_UserName, other.m_UserName);
	m_RockstarId = other.m_RockstarId;
	m_Data.Append(other.GetData(), other.GetDataSize());
	m_Rating = other.m_Rating;
	m_Bookmarked = other.m_Bookmarked;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////
// rlUgcRatings
////////////////////////////////////////////////////////////////////////////////
rlUgcRatings::rlUgcRatings()
{
    Clear();
}

void 
rlUgcRatings::Clear()
{
    m_Average = -1.0f;
    m_Unique = 0;
    m_NegativeUnique = 0;
    m_PositiveUnique = 0;
}

bool
rlUgcRatings::IsDefaultValues() const
{
    return (m_Average < 0)
        && (m_Unique == 0)
        && (m_NegativeUnique == 0)
        && (m_NegativeUnique == 0)
        && (m_PositiveUnique == 0);
}

////////////////////////////////////////////////////////////////////////////////
// rlUgc templates
////////////////////////////////////////////////////////////////////////////////
RAGE_DEFINE_SUBCHANNEL(rline, ugctemplates)
#undef __rage_channel
#define __rage_channel rline_ugctemplates

const char* s_UgcTokenTypeStrings[] =
{
    "contentId",
    "fileId",
    "fileVersion",
    "lang",
    "ext",
    "platform",
    "env"
};
CompileTimeAssert(COUNTOF(s_UgcTokenTypeStrings) == (int)rlUgcUrlTemplate::TokenType::Max);

const char* rlUgcGetTokenStringFromType(const rlUgcUrlTemplate::TokenType tokenType)
{
    return ((tokenType > rlUgcUrlTemplate::TokenType::Invalid) && (tokenType < rlUgcUrlTemplate::TokenType::Max)) ? s_UgcTokenTypeStrings[tokenType] : "Invalid";
}

// Parses stuff inside the braces of a token {...} e.g. "lang" would return Language
rlUgcUrlTemplate::TokenType rlUgcGetTokenTypeFromString(const char* tokenString, const int stringLength)
{
    rlAssert(tokenString);
    rlAssert(stringLength > 0);

    for (int index = 0; index < (int)rlUgcUrlTemplate::TokenType::Max; ++index)
    {
        const char* validString = s_UgcTokenTypeStrings[index];

        if (strlen(validString) != stringLength)
            continue;

        if (strnicmp(validString, tokenString, stringLength) != 0)
            continue;

        return (rlUgcUrlTemplate::TokenType)index;
    }

    return rlUgcUrlTemplate::TokenType::Invalid;
}

bool rlUgcCreateUrlTemplateFromJson(const RsonReader& templateJson, rlUgcUrlTemplate& result)
{
    char nameString[64] = {0};
    if(!templateJson.ReadString(rlUgcUrlTemplate::TEMPLATE_NAME, nameString))
    {
        return false;
    }

    atString urlTemplate;
    if(!rlVerify(templateJson.ReadString(rlUgcUrlTemplate::TEMPLATE_URL, urlTemplate)))
    {
        return false;
    }

	// Optional flags
	{
		RsonReader flagsArray;
		RsonReader flagElement;
		unsigned flags = 0;
		bool exists = templateJson.GetMember("Flags", &flagsArray);

		for(exists = exists && flagsArray.GetFirstMember(&flagElement); exists; exists = flagElement.GetNextSibling(&flagElement))
		{
			char buffer[32] = {0};
			if (flagElement.AsString(buffer))
			{
				const unsigned flag = (unsigned)rlUgcContentFlagFromString(buffer);
				rlAssertf(flag != 0, "Flag %s not found", buffer);
				flags |= flag;
			}
		}

		result.templateFlags = flags;
	}

    rlUgcUrlTemplate::TokenArray tokens;

    // Find all the tokens in  the template.
    int tokenStart = urlTemplate.IndexOf('{');
    while(tokenStart > 0)
    {
        int tokenEnd = urlTemplate.IndexOf('}', tokenStart);

        if(tokenEnd <= 0)
        {
            return false;
        }

        // This parsing function doesn't expect the enclosing braces, so we omit them from the span we pass in.
        const rlUgcUrlTemplate::TokenType tokenType = rlUgcGetTokenTypeFromString(urlTemplate.c_str() + tokenStart + 1, tokenEnd - (tokenStart + 1));

        if(!rlVerify(tokenType != rlUgcUrlTemplate::TokenType::Invalid))
        {
            return false;
        }

        rlUgcUrlTemplate::Token token;
        token.startIndex = tokenStart;
        token.length = tokenEnd - tokenStart;
        token.tokenType = tokenType;

		tokens.PushAndGrow(token);

        tokenStart = urlTemplate.IndexOf('{', tokenEnd);
    }

    result.templateName = nameString;
    result.tokens = std::move(tokens);
    result.templateUrl = urlTemplate;

    rlDebug("rlUgcCreateUrlTemplateFromJson :: Created template. Name: %s, String: %s", result.templateName.c_str(), result.templateUrl.c_str());
    return true;
}

bool rlUgcCreateUrlTemplatesFromJson(const RsonReader& templateJsonArray, atArray<rlUgcUrlTemplate>& results)
{
#if RSG_GEN9
    if(!rlVerify(templateJsonArray.IsArray())) return false;
#endif

    atArray<rlUgcUrlTemplate> urlTemplates;

    {
        RsonReader arrayElement;
        bool exists = false;
        for(exists = templateJsonArray.GetFirstMember(&arrayElement); exists; exists = arrayElement.GetNextSibling(&arrayElement))
        {
            rlUgcUrlTemplate urlTemplate;
            if (rlUgcCreateUrlTemplateFromJson(arrayElement, urlTemplate))
            {
                urlTemplates.PushAndGrow(urlTemplate);
            }
            else
            {
                rlWarning("rlUgcCreateUrlTemplatesFromJson :: Discarding invalid template");
            }
        }
    }

    rlDebug("rlUgcCreateUrlTemplatesFromJson :: Created %d templates", urlTemplates.GetCount());

    results = std::move(urlTemplates);

    return true;
}

const rlUgcUrlTemplate* rlUgcGetTemplateFromParams(const rlUgcUrlTemplate* const templates, const int numTemplates, const char* templateName)
{
    for(int index = 0; index < numTemplates; index++)
    {
        const rlUgcUrlTemplate& urlTemplate = templates[index];
        if(urlTemplate.templateName != templateName) continue;

        return &urlTemplate;
    }

    return nullptr;
}

const char* rlUgcSelectValueForType(const rlUgcTokenValues& tokenValues, const rlUgcUrlTemplate::TokenType tokenType, char* dst, const unsigned dstMaxLength)
{
    dst[0] = '\0';

    if (!tokenValues.IsTokenValueSet(tokenType) &&
        tokenType != rlUgcUrlTemplate::TokenType::Language) // temporary
    {
        return dst;
    }

    switch (tokenType)
    {
    case rlUgcUrlTemplate::TokenType::ContentId:
        safecpy(dst, tokenValues.GetContentId(), dstMaxLength);
        break;
    case rlUgcUrlTemplate::TokenType::Language:
        // override for individual query data, mission data and mission images
        if (tokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::FileId) &&
            tokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::FileVersion) &&
            tokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::Language))
        {
            formatf_sized(dst, dstMaxLength, "%d_%d_%s", tokenValues.GetFileId(), tokenValues.GetFileVersion(), rlScLanguageToString(tokenValues.GetLanguage()));
        }
        else if (tokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::FileId) &&
            tokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::FileVersion))
        {
            formatf_sized(dst, dstMaxLength, "%d_%d", tokenValues.GetFileId(), tokenValues.GetFileVersion());
        }
        else if (tokenValues.IsTokenValueSet(rlUgcUrlTemplate::TokenType::Language))
        {
            safecpy(dst, rlScLanguageToString(tokenValues.GetLanguage()), dstMaxLength);
        }
        break;
    case rlUgcUrlTemplate::TokenType::Extension:
        safecpy(dst, tokenValues.GetFileExtension(), dstMaxLength);
        break;
    case rlUgcUrlTemplate::TokenType::FileId:
        formatf_sized(dst, dstMaxLength, "%d", tokenValues.GetFileId());
        break;
    case rlUgcUrlTemplate::TokenType::FileVersion:
        formatf_sized(dst, dstMaxLength, "%d", tokenValues.GetFileVersion());
        break;
    case rlUgcUrlTemplate::TokenType::Platform:
        formatf_sized(dst, dstMaxLength, "%s", tokenValues.GetPlatform());
        break;
    case rlUgcUrlTemplate::TokenType::RosEnvironment:
        formatf_sized(dst, dstMaxLength, "%s", rlRosGetEnvironmentAsString(rlRosGetEnvironment()));
        break;
    case rlUgcUrlTemplate::TokenType::Invalid:
    case rlUgcUrlTemplate::TokenType::Max:
        rlAssertf(false, "Invalid token type");
    }

    return dst;
}

bool rlUgcBuildUrlFromTemplate(const atString& urlTemplateString,
                               const rlUgcUrlTemplate::TokenArray& urlTemplateTokens,
                               const rlUgcTokenValues& tokenValues,
                               char* urlBuffer,
                               const int urlBufferSize)
{
    atStringBuilder sb(urlBuffer, urlBufferSize);

    int copyStartIndex = 0;

    for (int tokenIndex = 0; tokenIndex < urlTemplateTokens.size(); tokenIndex++)
    {
        const rlUgcUrlTemplate::Token& token = urlTemplateTokens[tokenIndex];

        char tokenString[rlUgcTokenValues::MAX_TOKEN_VALUE_LENGTH];
        const char* tokenValue = rlUgcSelectValueForType(tokenValues, token.tokenType, tokenString, rlUgcTokenValues::MAX_TOKEN_VALUE_LENGTH);

        if (tokenValue == nullptr)
        {
            // require value for token n of type thing, but none given
            rlError("BuildUrlFromTemplate :: Token value not valid for required token: %s", rlUgcGetTokenStringFromType(token.tokenType));
            return false;
        }

        sb.Append(
            urlTemplateString.c_str() + copyStartIndex,
            token.startIndex - copyStartIndex);

        sb.Append(tokenValue);

        copyStartIndex = token.startIndex + token.length + 1;
    }

    sb.Append(
        urlTemplateString.c_str() + copyStartIndex,
        urlTemplateString.length() - copyStartIndex);

    return true;
}

}   //namespace rage
