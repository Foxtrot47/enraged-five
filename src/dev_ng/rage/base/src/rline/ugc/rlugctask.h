// 
// rline/rlugctask.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUGCTASK_H
#define RLINE_RLUGCTASK_H

#include "rlugc.h"
#include "rline/rlhttptask.h"
#include "rline/ros/rlroshttptask.h"
#include "data/growbuffer.h"

namespace rage
{

class parTreeNode;

///////////////////////////////////////////////////////////////////////////////
//  rlUgcTaskBase
///////////////////////////////////////////////////////////////////////////////
class rlUgcTaskBase : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcTaskBase);

    rlUgcTaskBase(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0);
    virtual ~rlUgcTaskBase() {}

protected:
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    //Helpers for reading structs from XML.  These are only used for non-streaming tasks.
    //bool ReadContentXml(const parTreeNode* node, rlUgcContentInfo* content);
    bool ReadMetadataXml(const parTreeNode* node, const rlUgcContentType contentType, rlUgcMetadata* metadata);
    bool ReadPlayerXml(const parTreeNode* node, rlUgcPlayer* player);
    bool ReadRatingsXml(const parTreeNode* node, rlUgcRatings* ratings);

    //Helpers for reading struct values.  These can be used for both streaming and non-streaming tasks.
    bool ReadContentCreatorValue(const char* name, const char* val, rlUgcContentCreatorInfo* cc);
    bool ReadMetadataValue(const char* name, const char* val, rlUgcMetadata* m);
    bool ReadPlayerValue(const char* name, const char* val, rlUgcPlayer* p);
    bool ReadRatingsValue(const char* name, const char* val, rlUgcRatings* r);

    //Helper for appending file content to a binary data parameter
    bool AppendFiles(const rlUgc::SourceFile* files, const unsigned numFiles);
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcCheckTextTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcCheckTextTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcCheckTextTask);

	rlUgcCheckTextTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer), m_profaneWord(NULL) {}
	virtual ~rlUgcCheckTextTask() {}

    bool Configure(const int localGamerIndex, 
                   const char* contentName,
                   const char* description,
                   const char* tagCsv, 
				   char* profaneWord);

	static const unsigned MAX_PROFANE_WORD_LENGTH = 64;

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/CheckText"; }
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	char* m_profaneWord;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcCopyContentTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcCopyContentTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcCopyContentTask);

	rlUgcCopyContentTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
	virtual ~rlUgcCopyContentTask() {}

    bool Configure(const int localGamerIndex, 
                   const rlUgcContentType contentType,
                   const char* contentId,
                   rlUgcMetadata* metadata);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/CopyContent"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlUgcContentType m_ContentType;
    rlUgcMetadata* m_Metadata;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcCreateContentTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcCreateContentTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcCreateContentTask);

	rlUgcCreateContentTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer)
		, m_WithFileData(false)
	{}

    virtual ~rlUgcCreateContentTask() {}

    bool Configure(const int localGamerIndex, 
                   const rlUgcContentType contentType,
                   const char* contentName,
                   const char* dataJson,
                   const char* description,
                   const rlUgc::SourceFileInfo* files,
                   const unsigned numFiles,
                   const rlScLanguage language,
                   const char* tagCsv,
                   const bool publish,
                   rlUgcMetadata* metadata,
                   const bool withFileData = false);

    bool Configure(const int localGamerIndex, 
                   const rlUgcContentType contentType,
                   const char* contentName,
                   const char* dataJson,
                   const char* description,
                   const rlUgc::SourceFile* files,
                   const unsigned numFiles,
                   const rlScLanguage language,
                   const char* tagCsv,
                   const bool publish,
                   rlUgcMetadata* metadata);

protected:
    virtual const char* GetServiceMethod() const { return m_WithFileData ? "ugc.asmx/CreateContent" : "ugc.asmx/CreateContent2"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    bool WriteJson(const char* contentName,
                   const char* dataJson,
                   const char* description,
                   const rlUgc::SourceFileInfo* files,
                   const unsigned numFiles,
                   const rlScLanguage language,
                   const bool* publish,
                   const char* tagCsv,
                   RsonWriter& w) const;

    rlUgcContentType m_ContentType;
    rlUgcMetadata* m_Metadata;
    bool m_WithFileData;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcQueryContentTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcQueryContentTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcQueryContentTask);

	rlUgcQueryContentTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
    virtual ~rlUgcQueryContentTask() {}

    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* queryName,
                   const char* queryParams,
                   const int offset,
                   const int count,
                   int* total,
                   int* hash,
                   const rlUgc::QueryContentDelegate& dlgt);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/QueryContent"; }
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    virtual bool SaxStartElement(const char* name);
    virtual bool SaxAttribute(const char* name, const char* val);
    virtual bool SaxCharacters(const char* ch, const int start, const int length);
    virtual bool SaxEndElement(const char* name);

private:
    rlRosSaxReader m_RosSaxReader;

    enum State
    {
        STATE_EXPECT_RESULT,                //Looking for <Result>
        STATE_READ_RESULT,                  //Reading <Result>
        STATE_READ_RECORD,                  //Reading <Result><r>
        STATE_READ_RECORD_META,             //Reading <Result><r><m> 
        STATE_READ_RECORD_RATINGS,          //Reading <Result><r>r>
        STATE_READ_PLAYER,                  //Reading <Result><r><p>
        STATE_DONE                          //No more data is expected nor handled
    };
    State m_State;

    int* m_Total;
    int* m_Hash;
    rlUgc::QueryContentDelegate m_Dlgt;

    int m_TempTotal;
    int m_TempHash;

    int m_Index;
    datGrowBuffer m_InnerText;

    char m_CurContentId[RLUGC_MAX_CONTENTID_CHARS];
    rlUgcContentType m_ContentType;
    rlUgcMetadata m_CurMetadata;
    rlUgcRatings m_CurRatings;
    datGrowBuffer m_CurStats;
    unsigned m_CurNumPlayers;
    unsigned m_CurPlayerIndex;
    rlUgcPlayer m_CurPlayer;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcQueryContentCreatorsTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcQueryContentCreatorsTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcQueryContentCreatorsTask);

	rlUgcQueryContentCreatorsTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
    virtual ~rlUgcQueryContentCreatorsTask() {}

    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* queryName,
                   const char* queryParams,
                   const int offset,
                   const int count,
                   const rlUgc::QueryContentCreatorsDelegate& dlgt);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/QueryContentCreators"; }
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    virtual bool SaxStartElement(const char* name);
    virtual bool SaxAttribute(const char* name, const char* val);
    virtual bool SaxCharacters(const char* ch, const int start, const int length);
    virtual bool SaxEndElement(const char* name);

private:
    rlRosSaxReader m_RosSaxReader;

    enum State
    {
        STATE_EXPECT_RESULT,                //Looking for <Result>
        STATE_READ_RESULT,                  //Reading <Result>
        STATE_READ_CREATOR,                 //Reading <Result><c>
        STATE_READ_CREATOR_RATINGS,         //Reading <Result><c><r>
        STATE_DONE                          //No more data is expected nor handled
    };
    State m_State;
  
    rlUgc::QueryContentCreatorsDelegate m_Dlgt;

    datGrowBuffer m_InnerText;
    rlUgcContentCreatorInfo m_TempCreator;
    int m_TotalVal;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcPublishTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcPublishTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcPublishTask);

	rlUgcPublishTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
    virtual ~rlUgcPublishTask() {}

	// For info about the httpRequestHeaders parameter, see netHttpRequest::AddRequestHeaders.
    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* contentId,
                   const char* baseContentId,
                   const char* httpRequestHeaders);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/Publish"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcSetBookmarkedTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcSetBookmarkedTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcSetBookmarkedTask);

	rlUgcSetBookmarkedTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
    virtual ~rlUgcSetBookmarkedTask() {}

    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* contentId,
                   const bool bookmarked);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/SetBookmarked"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcSetDeletedTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcSetDeletedTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcSetDeletedTask);

	rlUgcSetDeletedTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
    virtual ~rlUgcSetDeletedTask() {}

    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* contentId,
                   const bool deleted);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/SetDeleted"; }
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcSetPlayerDataTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcSetPlayerDataTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcSetPlayerDataTask);

	rlUgcSetPlayerDataTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
    virtual ~rlUgcSetPlayerDataTask() {}

    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* contentId,
                   const char* dataJson,
                   const float* rating);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/SetPlayerData"; }

private:
    bool WriteJson(const char* dataJson,
                   const float* rating,
                   RsonWriter& w) const;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcUpdateContentTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcUpdateContentTask : public rlUgcTaskBase
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlUgcUpdateContentTask);

	rlUgcUpdateContentTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0)
		: rlUgcTaskBase(allocator, bounceBuffer, sizeofBounceBuffer) {}
    virtual ~rlUgcUpdateContentTask() {}

    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* contentId,
                   const char* contentName,
                   const char* dataJson,
                   const char* description,
                   const rlUgc::SourceFileInfo* files,
                   const unsigned numFiles,
                   const char* tagCsv,
                   rlUgcMetadata* metadata);

    bool Configure(const int localGamerIndex,
                   const rlUgcContentType contentType,
                   const char* contentId,
                   const char* contentName,
                   const char* dataJson,
                   const char* description,
                   const rlUgc::SourceFile* files,
                   const unsigned numFiles,
                   const char* tagCsv,
                   rlUgcMetadata* metadata);

protected:
    virtual const char* GetServiceMethod() const { return "ugc.asmx/UpdateContent"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    bool WriteJson(const char* contentName,
                   const char* dataJson,
                   const char* description,
                   const rlUgc::SourceFileInfo* files,
                   const unsigned numFiles,
                   const char* tagCsv,
                   RsonWriter& w) const;

    rlUgcContentType m_ContentType;
    rlUgcMetadata* m_Metadata;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcGetCdnContent - Retrieve featured / R* content
///////////////////////////////////////////////////////////////////////////////
class rlUgcGetCdnContentTask : public rlTaskBase
{
public:

    RL_TASK_USE_CHANNEL(rline_ugc);
    RL_TASK_DECL(rlUgcGetCdnContentTask);

    rlUgcGetCdnContentTask(sysMemAllocator* allocator);
    virtual ~rlUgcGetCdnContentTask();

    bool Configure(const int localGamerIndex,
        datGrowBuffer* pGrowBuffer,
        const char* url,
        const unsigned contentFlags,
        rlCloudFileInfo* fileInfo = nullptr,
        u64 modifiedTime = 0);

    virtual void Start();
    virtual void Update(const unsigned timeStep);
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual bool IsCancelable() const { return true; }
    virtual void DoCancel();

protected:

    virtual const char* GetUrl(char* dst, const unsigned dstMaxLength);
    virtual const char* GetRelativeUrl(const char* url, char* dst, const unsigned dstMaxLength);

    enum GetSucceededAction
    {
        Action_Complete,
        Action_Failed,
        Action_ProcessContent,
    };
    virtual GetSucceededAction OnGetSucceeded() { return GetSucceededAction::Action_Complete; }

    enum GetFailedAction
    {
        Action_Fail,
        Action_Retry,
    };
    virtual GetFailedAction OnGetFailed() { return GetFailedAction::Action_Fail; }

    enum ProcessContentResult
    {
        Process_Pending,
        Process_Complete,
        Process_Failed,
    };
    virtual ProcessContentResult ProcessContent(const unsigned /*timeStep*/, int& /*errorCode*/) { return ProcessContentResult::Process_Complete; }

    netHttpRequest m_HttpRequest;
    datGrowBuffer m_MyGrowBuffer;
    datGrowBuffer* m_GrowBuffer;

private:

    rlUgcGetCdnContentTask(const rlUgcGetCdnContentTask&);
    rlUgcGetCdnContentTask& operator=(const rlUgcGetCdnContentTask&);

    void Reset();

    bool Get();
    bool ReadContent();

    bool ProcessData();
    bool ValidateSignature(const u8* pData, const unsigned nData, unsigned* pPayloadSize);
    bool Decrypt(u8* pData, const unsigned nData);
    unsigned Decompress(const u8* pData, const unsigned nData, u8* pUncompressBuffer, const unsigned nUncompressMaxSize);

#if !__NO_OUTPUT
    void WriteData(const u8* pData, const unsigned nData, const char* fileName);
#endif

    enum Constants
    {
        HTTP_REQUEST_TIMEOUT_SECONDS = 30,
        HTTP_READ_BUFFER_SIZE = 2048,
    };

    enum State
    {
        State_Invalid = -1,
        State_Get,
        State_Getting,
        State_Processing,
        State_Failed,
        State_Finished,
    };

    State m_State;
    rlRosHttpFilter m_Filter;

    int m_LocalGamerIndex;
    unsigned m_Flags;
    char m_Url[RL_MAX_URL_BUF_LENGTH];
    bool m_UrlProvided;
    rlCloudFileInfo* m_FileInfo;
    u64 m_LastModifiedTime;

    sysMemAllocator* m_Allocator;

    netStatus m_MyStatus;
};


///////////////////////////////////////////////////////////////////////////////
//  rlUgcGetFeaturedContentDataTask - Retrieve featured / R* content
///////////////////////////////////////////////////////////////////////////////
class rlUgcGetFeaturedContentDataTask : public rlUgcGetCdnContentTask
{
public:

    RL_TASK_USE_CHANNEL(rline_ugc);
    RL_TASK_DECL(rlUgcGetFeaturedContentDataTask);

    rlUgcGetFeaturedContentDataTask(sysMemAllocator* allocator);

    bool Configure(const int localGamerIndex,
        datGrowBuffer* growBuffer,
        const rlUgcTokenValues& tokenValues,
        const rlUgcUrlTemplate* urlTemplate,
        rlCloudFileInfo* fileInfo,
        u64 modifiedTimeFromCache);

    virtual void Start();

protected:

    virtual GetFailedAction OnGetFailed();

    virtual const char* GetUrl(char* dst, const unsigned dstMaxLength);
    virtual const char* GetRelativeUrl(const char* url, char* dst, const unsigned dstMaxLength);

private:

    rlUgcGetFeaturedContentDataTask(const rlUgcGetFeaturedContentDataTask&);
    rlUgcGetFeaturedContentDataTask& operator=(const rlUgcGetFeaturedContentDataTask&);

    rlUgcTokenValues m_TokenValues;
    const rlUgcUrlTemplate* m_UrlTemplate;
    bool m_bAllowLanguageRetryfor404;
};

///////////////////////////////////////////////////////////////////////////////
//  rlUgcGetUrlTemplatesTask
///////////////////////////////////////////////////////////////////////////////
class rlUgcGetUrlTemplatesTask : public rlUgcGetCdnContentTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ugc);
    RL_TASK_DECL(rlUgcGetUrlTemplatesTask);

    rlUgcGetUrlTemplatesTask(sysMemAllocator* allocator);

    bool Configure(
        const int localGamerIndex,
        const char* url,
        const rlUgcCdnContentFlags flags,
        atArray<rlUgcUrlTemplate>* resultTemplates);

protected:

    virtual const char* GetRelativeUrl(const char* url, char* dst, const unsigned dstMaxLength);
    virtual GetSucceededAction OnGetSucceeded();

private:
    rlUgcGetUrlTemplatesTask(const rlUgcGetUrlTemplatesTask&);
    rlUgcGetUrlTemplatesTask& operator=(const rlUgcGetUrlTemplatesTask&);

    atArray<rlUgcUrlTemplate>* m_ResultTemplates;
};

} //namespace rage

#endif  //RLINE_RLUGCTASK_H
