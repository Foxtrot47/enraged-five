// 
// rline/rlugc.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlugc.h"
#include "rlugctask.h"
#include "diag/seh.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////
// rlUgc
////////////////////////////////////////////////////////////////////////i////////
void
rlUgc::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//Convenience macro to create and queue Social Club tasks
#define RLUGC_CREATETASK(T, allocator, ...)									                    \
    bool success = false;																	\
    T* task = NULL;																			\
    rtry																					\
    {																						\
        task = rlGetTaskManager()->CreateTask<T>(allocator);								            \
        rverify(task,catchall,);															\
        rverify(rlTaskBase::Configure(task, __VA_ARGS__), catchall,);		                \
        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);			            \
        success = true;																		\
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
            if(task->IsPending())															\
            {																				\
                task->Finish(rlTaskBase::FINISH_FAILED,										\
                RLSC_ERROR_UNEXPECTED_RESULT);						                        \
            }																				\
            else																			\
            {																				\
                status->ForceFailed();														\
            }																				\
            rlGetTaskManager()->DestroyTask(task);								            \
        }																					\
    }																						\
    return success;

bool
rlUgc::CheckText(const int localGamerIndex, 
                 const char* contentName,
                 const char* description,
				 const char* tagCsv,
				 char* profaneWord,
                 sysMemAllocator* allocator,
				 netStatus* status)
{
    RLUGC_CREATETASK(rlUgcCheckTextTask,
        allocator,
        localGamerIndex,
        contentName,
        description,
        tagCsv,
		profaneWord,
        status);
}

bool
rlUgc::CopyContent(const int localGamerIndex, 
                   const rlUgcContentType contentType,
                   const char* contentId,
				   rlUgcMetadata* metadata,
				   sysMemAllocator* allocator,
                   netStatus* status)
{
    RLUGC_CREATETASK(rlUgcCopyContentTask,
		allocator,
		localGamerIndex,
        contentType,
        contentId,
        metadata,
		status);
}

bool
rlUgc::CreateContent(const int localGamerIndex, 
                     const rlUgcContentType contentType,
                     const char* contentName,
                     const char* dataJson,
                     const char* description,
                     const SourceFileInfo* files,
                     const unsigned numFiles,
                     const rlScLanguage language,
                     const char* tagCsv,
                     const bool publish,
					 rlUgcMetadata* metadata,
					 sysMemAllocator* allocator,
                     netStatus* status)
{
    RLUGC_CREATETASK(rlUgcCreateContentTask,
		allocator,
		localGamerIndex,
        contentType,
        contentName,
        dataJson,
        description,
        files,
        numFiles,
        language,
        tagCsv,
        publish,
        metadata,
		status);
}

bool
rlUgc::CreateContent(const int localGamerIndex, 
                     const rlUgcContentType contentType,
                     const char* contentName,
                     const char* dataJson,
                     const char* description,
                     const SourceFile* files,
                     const unsigned numFiles,
                     const rlScLanguage language,
                     const char* tagCsv,
                     const bool publish,
					 rlUgcMetadata* metadata,
					 sysMemAllocator* allocator,
                     netStatus* status)
{
    RLUGC_CREATETASK(rlUgcCreateContentTask,
		allocator,
		localGamerIndex,
        contentType,
        contentName,
        dataJson,
        description,
        files,
        numFiles,
        language,
        tagCsv,
        publish,
        metadata,
		status);
}

bool
rlUgc::QueryContent(const int localGamerIndex, 
                    const rlUgcContentType contentType,
                    const char* queryName,
                    const char* queryParamsJson,
                    const int offset,
                    const int count,
                    int* total,
                    int* hash,
					const QueryContentDelegate& dlgt,
					sysMemAllocator* allocator,
                    netStatus* status)
{
    RLUGC_CREATETASK(rlUgcQueryContentTask,
                                      allocator,						
                                      localGamerIndex,
                                      contentType,
                                      queryName,
                                      queryParamsJson,
                                      offset,
                                      count,
                                      total,
                                      hash,
                                      dlgt,
									  status);
}

bool 
rlUgc::QueryContentCreators(const int localGamerIndex, 
                            const rlUgcContentType contentType,
                            const char* queryName,
                            const char* queryParamsJson,
                            const int offset,
                            const int count,
							const QueryContentCreatorsDelegate& dlgt,
							sysMemAllocator* allocator,
                            netStatus* status)
{
    RLUGC_CREATETASK(rlUgcQueryContentCreatorsTask,
		allocator,						
		localGamerIndex,
        contentType,
        queryName,
        queryParamsJson,
        offset,
        count,
        dlgt,
		status);
}

bool 
rlUgc::Publish(const int localGamerIndex,
               const rlUgcContentType contentType,
               const char* contentId,
			   const char* baseContentId,
			   const char* httpRequestHeaders,
			   sysMemAllocator* allocator,
               netStatus* status)
{
    RLUGC_CREATETASK(rlUgcPublishTask,
		allocator,						
		localGamerIndex,
        contentType,
        contentId,
        baseContentId,
		httpRequestHeaders,
		status);
}

bool
rlUgc::SetBookmarked(const int localGamerIndex,
                     const rlUgcContentType contentType,
                     const char* contentId,
					 const bool bookmarked,
					 sysMemAllocator* allocator,
                     netStatus* status)
{
    RLUGC_CREATETASK(rlUgcSetBookmarkedTask,
		allocator,						
		localGamerIndex,
        contentType,
        contentId,
        bookmarked,
		status);
}

bool
rlUgc::SetDeleted(const int localGamerIndex,
                  const rlUgcContentType contentType,
                  const char* contentId,
				  const bool deleted,
				  sysMemAllocator* allocator,
                  netStatus* status)
{
    RLUGC_CREATETASK(rlUgcSetDeletedTask,
		allocator,						
		localGamerIndex,
        contentType,
        contentId,
        deleted,
		status);
}

bool
rlUgc::SetPlayerData(const int localGamerIndex,
                     const rlUgcContentType contentType,
                     const char* contentId,
                     const char* dataJson,
					 const float* rating,
					 sysMemAllocator* allocator,
                     netStatus* status)
{
    RLUGC_CREATETASK(rlUgcSetPlayerDataTask,
		allocator,						
		localGamerIndex,
        contentType,
        contentId,
        dataJson,
        rating,
		status);
}

bool
rlUgc::UpdateContent(const int localGamerIndex,
                     const rlUgcContentType contentType,
                     const char* contentId,
                     const char* contentName,
                     const char* dataJson,
                     const char* description,
                     const SourceFileInfo* files,
                     const unsigned numFiles,
                     const char* tagCsv,
					 rlUgcMetadata* metadata,
					 sysMemAllocator* allocator,
                     netStatus* status)
{
    RLUGC_CREATETASK(rlUgcUpdateContentTask,
		allocator,						
		localGamerIndex,
        contentType,
        contentId,
        contentName,
        dataJson,
        description,
        files,
        numFiles,
        tagCsv,
        metadata,
		status);
}

bool
rlUgc::UpdateContent(const int localGamerIndex,
                     const rlUgcContentType contentType,
                     const char* contentId,
                     const char* contentName,
                     const char* dataJson,
                     const char* description,
                     const SourceFile* files,
                     const unsigned numFiles,
                     const char* tagCsv,
					 rlUgcMetadata* metadata,
					 sysMemAllocator* allocator,
                     netStatus* status)
{
    RLUGC_CREATETASK(rlUgcUpdateContentTask,
		allocator,						
		localGamerIndex,
        contentType,
        contentId,
        contentName,
        dataJson,
        description,
        files,
        numFiles,
        tagCsv,
        metadata,
		status);
}

bool
rlUgc::GetFeaturedContentData(
    const int localGamerIndex,
    const rlUgcTokenValues& tokenValues,
    const rlUgcUrlTemplate* urlTemplate,
    sysMemAllocator* allocator,
    datGrowBuffer* growBuffer,
    netStatus* status)
{
	return GetFeaturedContentData(
		localGamerIndex,
		tokenValues,
		urlTemplate,
		nullptr /* fileInfo */,
		0 /* lastModifiedTimeFromCache */,
		allocator,
		growBuffer,
		status);
}

bool
rlUgc::GetFeaturedContentData(
	const int localGamerIndex,
	const rlUgcTokenValues& tokenValues,
	const rlUgcUrlTemplate* urlTemplate,
	rlCloudFileInfo* fileInfo,
	u64 lastModifiedTimeFromCache,
	sysMemAllocator* allocator,
	datGrowBuffer* growBuffer,
	netStatus* status)
{
	RLUGC_CREATETASK(
		rlUgcGetFeaturedContentDataTask,
		allocator,
		localGamerIndex,
		growBuffer,
		tokenValues,
		urlTemplate,
		fileInfo,
		lastModifiedTimeFromCache,
		status);
}

bool 
rlUgc::GetUrlTemplates(
	const int localGamerIndex,
	const char* url,
	const rlUgcCdnContentFlags flags,
	sysMemAllocator* allocator,
	atArray<rlUgcUrlTemplate>& resultTemplates,
	netStatus& validStatus)
{
	netStatus* status = &validStatus;

	RLUGC_CREATETASK(
		rlUgcGetUrlTemplatesTask,
		allocator,
		localGamerIndex,
		url,
		flags,
		&resultTemplates,
		status);
}

}   //namespace rage
