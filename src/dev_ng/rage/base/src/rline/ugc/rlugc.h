// 
// rline/rlsocialclub.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUGC_H
#define RLINE_RLUGC_H

#include "rlugccommon.h"
#include "rline/ros/rlroscommon.h"
#include "atl/delegate.h"

namespace rage
{

class rlCloudFileInfo;

//PURPOSE
//  API for User Generated Content.
class rlUgc
{
public:
    //PURPOSE
    //  Describes a source file to import into UGC. This gets passed to CreateContent or UpdateContent.
    struct SourceFileInfo
    {
        int m_FileId;
        char m_CloudAbsPath[RLUGC_MAX_CLOUD_ABS_PATH_CHARS];

        SourceFileInfo() 
        : m_FileId(0)
        {
            m_CloudAbsPath[0] = '\0';
        }
    };

    //PURPOSE
    //  Describes a source file and it's content (as opposed to cloud path). This gets passed to CreateContent or UpdateContent.
    struct SourceFile
    {
        s32 m_FileId;
        const u8* m_FileContent;
        s32 m_FileLength;

        SourceFile()
            : m_FileId(0)
            , m_FileContent(NULL)
            , m_FileLength(0)
        {}

        SourceFile(const s32 fileId,
                   const u8* fileContent,
                   const s32 fileLength)
        : m_FileId(fileId)
        , m_FileContent(fileContent)
        , m_FileLength(fileLength)
        {}
    };

    //PURPOSE
    //  Cancels the task that the specified status object is monitoring.
    static void Cancel(netStatus* status);

    //PURPOSE
    //  Verifies one or more pieces of UGC text is legal.
    //PARAMS
    //  contentName     - (OPTIONAL) Content name
    //  description     - (OPTIONAL) Content description
    //  tagCsv          - (OPTIONAL) Comma separated list of tags
    static bool CheckText(const int localGamerIndex, 
                          const char* contentName,
                          const char* description,
						  const char* tagCsv,
						  char* profaneWord,
                          sysMemAllocator* allocator,
                          netStatus* status
						  );

    //PURPOSE
    //  Copies an existing piece of content.  The copy will initially be the
    //  same as the source.  The copy will have a copy of the source's content
    //  (UGC cloud file), so that changes to the source file won't affect the copy.
    //PARAMS
    //  contentType         - Type of content being published
    //  contentId           - ID of content to copy
    //  metadata            - (OPTIONAL) If non-null, will contain the copy's metadata
    static bool CopyContent(const int localGamerIndex, 
                            const rlUgcContentType contentType,
                            const char* contentId,
							rlUgcMetadata* metadata,
							sysMemAllocator* allocator,
                            netStatus* status);

    //PURPOSE
    //  Creates a new piece of content.  This copies the specified cloud file into
    //  the UGC cloud, which is read-only and can only be directly modified via
    //  rlUgc::SetContent.
    //PARAMS
    //  contentType         - Type of content being published
    //  contentName         - Display name for content (ex. "My Awesome Mission")
    //  dataJson            - Content type-specific data in JSON format.
    //                        This is typically metadata necessary for searching and
    //                        previewing the content.
    //  description         - Display description for content
    //  files               - (Optional) Source files to import into UGC.
    //                        Note that these files will be deleted after the task completes.
    //  numFiles            - Number of source files
    //  language            - Language that name, description, and any text in data JSON or files is in
    //  tagCsv              - String of comma-separated tags, useful when searching.
    //                        Tags may be changed later via SetContentTags.
    //  publish             - True if the content should be published upon creation
    //  metadata            - (OPTIONAL) If non-null, will contain the created content metadata
    static bool CreateContent(const int localGamerIndex, 
                              const rlUgcContentType contentType,
                              const char* contentName,
                              const char* dataJson,
                              const char* description,
                              const SourceFileInfo* files,
                              const unsigned numFiles,
                              const rlScLanguage language,
                              const char* tagCsv,
                              const bool publish,
							  rlUgcMetadata* metadata,
							  sysMemAllocator* allocator,
                              netStatus* status);

    //PURPOSE
    //  An overload of CreateContent that takes the actual file contents instead of cloud paths
    static bool CreateContent(const int localGamerIndex,
                              const rlUgcContentType contentType,
                              const char* contentName,
                              const char* dataJson,
                              const char* description,
                              const SourceFile* files,
                              const unsigned numFiles,
                              const rlScLanguage language,
                              const char* tagCsv,
                              const bool publish,
							  rlUgcMetadata* metadata,
							  sysMemAllocator* allocator,
                              netStatus* status);

    //PURPOSE
    //  Runs a named query to get a page of matching content.  The sort direction
    //  and query parameters are inherent to the query being run, so ask RAGE networking
    //  for the list of current queries.
    //PARAMS
    //  contentType         - Type of content being queried
    //  queryName           - Name of query being run.  You should assume this is case-sensitive.
    //  offset              - Zero-based offset into matches to begin returning results from.
    //  count               - Max number of results to return
    //  queryParamsJson     - JSON string of parameters required by the query.
    //                        See documentation on the query for details.
    //  dlgt                - Delegate that gets called as each piece of content and player is read.
    //                        This will get called at least once per piece of content, and up to
    //                        numPlayers times.  Ex. Content with zero players will call back once,
    //                        while content with six players will call back six times.  The totalContent
    //                        content, and numPlayers values will be the same in each callback for the same content.
    typedef atDelegate<void (const int index,                   //Index into all matches returned
                             const char* contentId,             //Current content being read
                             const rlUgcMetadata* metadata,     //Metadata for this content, or NULL if hash passed by client matched server
                             const rlUgcRatings* ratings,       //Ratings for this content, or NULL if none calculated yet
                             const char* statsJson,             //Stats JSON for this content, or NULL if none (some content types don't have stats)
                             const unsigned numPlayers,         //Number of players returned for this piece of content
                             const unsigned playerIndex,        //Index of current player being returned
                             const rlUgcPlayer* player)>        //Current player being read, or NULL if no players
            QueryContentDelegate;

    static bool QueryContent(const int localGamerIndex, 
                             const rlUgcContentType contentType,
                             const char* queryName,
                             const char* queryParamsJson,
                             const int offset,
                             const int count,
                             int* total,
                             int* hash,
							 const QueryContentDelegate& dlgt,
							 sysMemAllocator* allocator,
                             netStatus* status);

    //PURPOSE
    //  Runs a named query to get a page of content creators.  The sort direction
    //  and query parameters are inherent to the query being run, so ask RAGE networking
    //  for the list of current queries.
    //PARAMS
    //  contentType         - Type of content being queried
    //  queryName           - Name of query being run.  You should assume this is case-sensitive.
    //  offset              - Zero-based offset into matches to begin returning results from.
    //  count               - Max number of results to return
    //  dlgt                - Delegate that gets called as each creator is read.
    typedef atDelegate<void (const unsigned totalCreators, const rlUgcContentCreatorInfo*)> QueryContentCreatorsDelegate;

    static bool QueryContentCreators(const int localGamerIndex, 
                                     const rlUgcContentType contentType,
                                     const char* queryName,
                                     const char* queryParamsJson,
                                     const int offset,
                                     const int count,
									 const QueryContentCreatorsDelegate& dlgt,
									 sysMemAllocator* allocator,
                                     netStatus* status);

    //PURPOSE
    //  Publishes a piece of unpublished content created by the local gamer.
    //  Published content is visible in searches, while unpublished content is not.
    //PARAMs
    //  contentType         - Type of content being published
    //  contentId           - Content being published.  It must be currently unpublished.
    //  baseContentId       - (Optional) ContentId in a chain of currently published content.  It can either
    //                        be the latest version, or an earlier version.  The content being published
    //                        will become the new latest version.
	//  httpRequestHeaders  - Additional headers to be added to the request. 
	//                        See netHttpRequest::AddRequestHeaders for format rules.
    static bool Publish(const int localGamerIndex,
                        const rlUgcContentType contentType,
                        const char* contentId,
						const char* baseContentId,
						const char* httpRequestHeaders,
						sysMemAllocator* allocator,
                        netStatus* status);

    //PURPOSE
    //  Bookmarks or unbookmarks a piece of content.
    //PARAMs
    //  contentType         - Type of content being published/unpublished
    //  contentId           - Content being published/unpublished
    //  bookmarked          - True if content should be bookmarked, false if it should not.
    //                        If the content is already in this state, there is no change and
    //                        the call will report success.
    static bool SetBookmarked(const int localGamerIndex,
                              const rlUgcContentType contentType,
                              const char* contentId,
							  const bool bookmarked,
							  sysMemAllocator* allocator,
                              netStatus* status);

    //PURPOSE
    //  Deletes a piece of content created by the local gamer.
    //PARAMs
    //  contentType         - Type of content being published/unpublished
    //  contentId           - Content being published/unpublished
    //  deleted             - True if content should be deleted, false if it should not.
    //                        If the content is already in this state, there is no change and
    //                        the call will report success.
    static bool SetDeleted(const int localGamerIndex,
                           const rlUgcContentType contentType,
                           const char* contentId,
						   const bool deleted,
						   sysMemAllocator* allocator,
                           netStatus* status);

    //PURPOSE
    //  Sets the data for the local gamer associated with a particular
    //  piece of content.  What this data is depends on the content type.
    //  For example, a gta5mission tracks downloads, times played, and so on,
    //  while a screenshot may track different stats.  The backend expects
    //  the data to be in the correct format for the content type; if it differs,
    //  then it won't be included in the aggregated stats.
    //PARAMS
    //  contentType         - Type of content
    //  contentId           - Unique ID of the piece of content
    //  dataJson            - (Optional) JSON string describing the player data.  If specified, this completely
    //                        overwrites the player's existing data for this content.  If NULL, data is not updated.
    //  rating              - (Optional) Rating to assign the content, from 0.0 to 1.0.  If NULL, rating is not updated.
    static bool SetPlayerData(const int localGamerIndex,
                              const rlUgcContentType contentType,
                              const char* contentId,
                              const char* dataJson,
							  const float* rating,
							  sysMemAllocator* allocator,
                              netStatus* status);

    //PURPOSE
    //  Modifies an existing piece of content created by the local gamer.
    //PARAMS
    //  contentType         - Type of content being modified
    //  contentId           - Content being modified
    //  contentName         - If non-null, this updates the display name of the
    //                        content, which also updates the display name of all versions of 
    //                        the content.  If null, the name is not changed.
    //  dataJson            - If non-null, this updates the data JSON stored in the
    //                        content metadata.  If null, the data JSON is not changed.
    //                        If non-null but empty, the data JSON is cleared.
    //  description         - If non-null, this updates the description display text.
    //                        If null, the description is not changed.  
    //                        If non-null but empty, the description is cleared.
    //  files               - (Optional) Source files to import into UGC.
    //                        Note that these files will be deleted after the task completes.
    //  numFiles            - Number of source files
    //  tagCsv              - If non-null, this updates the tags for the content.
    //                        If null, the tags are not changed.  
    //                        If non-null but empty, the tags are cleared.
    //  metadata            - (OPTIONAL) If non-null, will contain updated content metadata
    static bool UpdateContent(const int localGamerIndex,
                              const rlUgcContentType contentType,
                              const char* contentId,
                              const char* contentName,
                              const char* dataJson,
                              const char* description,
                              const SourceFileInfo* files,
                              const unsigned numFiles,
                              const char* tagCsv,
							  rlUgcMetadata* metadata,
							  sysMemAllocator* allocator,
                              netStatus* status);

    //PURPOSE
    //  An overload of UpdateContent that takes the actual files instead of cloud paths
    static bool UpdateContent(const int localGamerIndex,
                              const rlUgcContentType contentType,
                              const char* contentId,
                              const char* contentName,
                              const char* dataJson,
                              const char* description,
                              const SourceFile* files,
                              const unsigned numFiles,
                              const char* tagCsv,
							  rlUgcMetadata* metadata,
							  sysMemAllocator* allocator,
                              netStatus* status);

    //PURPOSE
    //  Queries featured content
    static bool GetFeaturedContentData(
        const int localGamerIndex,
        const rlUgcTokenValues& tokenValues,
        const rlUgcUrlTemplate* urlTemplate,
        sysMemAllocator* allocator,
        datGrowBuffer* growBuffer,
        netStatus* status);

	//PURPOSE
	//  Queries featured content and populates fileInfo.
	static bool GetFeaturedContentData(
		const int localGamerIndex,
		const rlUgcTokenValues& tokenValues,
		const rlUgcUrlTemplate* urlTemplate,
		rlCloudFileInfo* fileInfo,
		u64 lastModifiedTimeFromCache,
		sysMemAllocator* allocator,
		datGrowBuffer* growBuffer,
		netStatus* status);

    //PURPOSE
    //  Retrieves UGC URL templates
    static bool GetUrlTemplates(
		const int localGamerIndex,
		const char* url,
		const rlUgcCdnContentFlags flags,
		sysMemAllocator* allocator,
		atArray<rlUgcUrlTemplate>& resultTemplates,
		netStatus& status);

private:
    rlUgc();
};

} //namespace rage

#endif  //RLINE_RLSOCIALCLUB_H
