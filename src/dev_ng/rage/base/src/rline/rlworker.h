// 
// rline/rlworker.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLWORKER_H
#define RLINE_RLWORKER_H

#include "system/criticalsection.h"
#include "system/ipc.h"

namespace rage
{

//PURPOSE
//  This class provides a generic interface for implementing background
//  worker threads.
//  A worker thread sleeps until it has something to do.  Call Wakeup()
//  to wake a worker thread.
class rlWorker
{
public:
    rlWorker();

    virtual ~rlWorker();

    //PURPOSE
    //  Starts the worker thread and gives it a name.
    //PARAMS
    //  name        - Name of worker.
    //  stackSize   - Size of worker stack.
	//  fastStart	- Calls with fastStart = false
    virtual bool Start(const char* name,
                        const unsigned stackSize,
						const int cpuAffinity) {return Start(name, stackSize, cpuAffinity, false);}
	//PURPOSE
	//  Starts the worker thread and gives it a name. Passes a parameter on whether or not to wait on the thread starting before returning
	//PARAMS
	//  name        - Name of worker.
	//  stackSize   - Size of worker stack.
	//  fastStart	- If true, doesn't wait for the worker to signal that it has started before returning
	bool Start(const char* name,
					const unsigned stackSize,
					const int cpuAffinity,
					bool fastStart);

    //PURPOSE
    //  Stops the worker thread.  This will block until the worker
    //  thread completes its current processing.
    virtual bool Stop();

    //PURPOSE
    //  Wakes the worker thread when there is work to do.
    //  Wakeup() can be called multiple times - each call is queued.
    void Wakeup();

    //PURPOSE
    //  Returns true if the worker thread is running (i.e. Start() has been
    //  called).
    bool IsRunning() const;

    //PURPOSE
    //  Returns true if the worker thread is currently executing Perform().
    bool IsPerforming() const;

protected:

    //PURPOSE
    //  Called when the worker is awakened.
    //  Sub-classes must implement this function.
    virtual void Perform() = 0;

private:

    static void Process(void* p);

    mutable sysCriticalSectionToken m_CsPerforming;

    sysIpcSema m_WakeupSema;
    sysIpcSema m_WaitSema;
    sysIpcThreadId m_ThreadHandle;
    bool m_Stop         : 1;
    bool m_Performing   : 1;
};

}   //namespace rage

#endif  //RLINE_RLWORKER_H
