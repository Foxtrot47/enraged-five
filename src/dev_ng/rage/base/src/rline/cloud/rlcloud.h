// 
// rline/rlcoud.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLCLOUD_H
#define RLINE_RLCLOUD_H

#include "rline/rlpresence.h"
#include "rline/rltask.h"
#include "rline/socialclub/rlsocialclubcommon.h"
#include "rline/ugc/rlugccommon.h"
#include "atl/delegate.h"
#include "file/device.h"
#include "net/status.h"
#include "net/http.h"
#include "data/sha1.h"

namespace rage
{

class rlGamerHandle;
class rlCloud;
class rlRosHttpFilter;
class atStringBuilder;

//PURPOSE
//  Identifies the Cloud namespace from which to operate.
enum rlCloudNamespace
{
    RL_CLOUD_NAMESPACE_INVALID  = -1,
    RL_CLOUD_NAMESPACE_MEMBERS,
    RL_CLOUD_NAMESPACE_CREWS,
	RL_CLOUD_NAMESPACE_UGC,
	RL_CLOUD_NAMESPACE_TITLES,
    RL_CLOUD_NAMESPACE_GLOBAL
};

//PURPOSE
//  Identifies the online service section of Cloud from
//  which to operate.
//  Example:
//      members/sc/2737264672/my/files/are/here
//      members/xbl/63745723743/my/files/are/here
enum rlCloudOnlineService
{
    RL_CLOUD_ONLINE_SERVICE_INVALID  = -1,
    RL_CLOUD_ONLINE_SERVICE_NATIVE, //"xbl" on xbox, "np" on ps
    RL_CLOUD_ONLINE_SERVICE_SC      //"sc" (Social Club)
};

//PURPOSE
// Client side error codes for cloud requests
enum 
{
	RL_CLOUD_ERROR_OUT_OF_MEMORY = -100,
};

static const unsigned MAX_FILEPATH_LENGTH = 256;

//Identifies a member within a Cloud namespace.
//For the "members" namespace it will contain either a Rockstar ID,
//a XUID, or a NP Online Id.
//For the "clans" namespace it will contain a clan ID.
//Not used in the "titles" namespace
class rlCloudMemberId
{
public:

    rlCloudMemberId();

    //PURPOSE
    //  Constructs a Cloud member ID from, e.g., a Rockstar ID,
    //  a clan ID, a player account ID, etc.
    explicit rlCloudMemberId(const u64 memberId);

    //PURPOSE
    //  Constructs a platform-specific Cloud member ID from
    //   a gamer handle.
    explicit rlCloudMemberId(const rlGamerHandle& gamerHandle);

    //PURPOSE
    //  Clears to the initial state.  Invalidates the instance.
    void Clear();

    //PURPOSE
    //  Resets the member ID from a Rockstar ID, clan ID, etc.
    void Reset(const u64 memberId);

    //PURPOSE
    //  Resets the member ID from a gamer handle.
    void Reset(const rlGamerHandle& gamerHandle);

    //PURPOSE
    //  Returns the string representation of the member ID.
    const char* ToString() const;

    //PURPOSE
    //  Returns true if this represents a valid member ID.
    bool IsValid() const;

private:

    char m_IdString[32];
};

//PURPOSE
//  Used to watch for updates to items stored in Cloud.
//  Presently a cloud watcher can only watch for updates to
//  files in the "members" namespace, and only those items
//  owned by the player identified by the local gamer index.
//
//  See rlCloud::WatchMemberItem() below.
//
//  Callbacks should be declared like:
//      void OnModified(const char* pathSpec, const char* fullPath);
//
//  pathSpec    - The path spec passed to rlCloud::WatchMemberItem().
//  fullPath    - The full cloud path of the file that was modified.
class rlCloudWatcher : public atDelegate<void (const char*, const char*)>
{
    friend class rlCloud;
public:

    //PURPOSE
    //  Constructs a rlCloudWatcher from a free function or a static member function.
    //EXAMPLE
    //
    //  class Foo
    //  {
    //  public:
    //      static void OnModified(const char* pathSpec, const char* fullPath);
    //  };
    //
    //  rlCloudWatcher cw(&Foo::OnModified);
    template<typename T>
    explicit rlCloudWatcher(T callback)
        : atDelegate<void(const char*, const char*)>(callback)
        , m_IsWatching(false)
    {
        this->Clear();
    }

    //PURPOSE
    //  Constructs a rlCloudWatcher from an object instance and a member function.
    //EXAMPLE
    //
    //  class Bar
    //  {
    //  public:
    //      void OnModified(const char* pathSpec, const char* fullPath);
    //  };
    //
    //  Bar* bar = new Bar();
    //  rlCloudWatcher cw(bar, &Bar::OnModified);
    //
    template<typename T, typename U>
    rlCloudWatcher(T target, U memberFunc)
        : atDelegate<void(const char*, const char*)>(target, memberFunc)
        , m_IsWatching(false)
    {
        this->Clear();
    }

    rlCloudWatcher();

	bool IsWatching() const { return m_IsWatching; }

private:

    void Clear();

    int m_LocalGamerIndex;

    rlCloudOnlineService m_OnlineSvc;

    char m_PathSpec[256];

    inlist_node<rlCloudWatcher> m_ListNode;

    bool m_IsWatching : 1;

	rlClanId m_clanId;
};

//PURPOSE
//  Contains useful info about cloud files.
class rlCloudFileInfo
{
public:

    rlCloudFileInfo()
    {
        Clear();
    }

    void Clear()
    {
        m_LastModifiedPosixTime = 0;
        m_ContentLength = 0;
        m_Location[0] = '\0';
        m_ETag[0] = '\0';
        m_HaveSignature = false;
        m_Signature.Clear();
        m_HaveHash = false;
		m_Encrypted = false;
    }

    //"Last-Modified" header
    u64 m_LastModifiedPosixTime;

    //"Content-Length" header
    unsigned m_ContentLength;

    //"Location" header.
    char m_Location[384];

    //"ETag" header
    char m_ETag[64];

    // Whether or not we have a signature for this file
    bool m_HaveSignature;
    // The signature, if any, for this file
    rlRosContentSignature m_Signature;
    // Whether or not we have a sha1 hash for this file
    bool m_HaveHash;
    // The hash, if any, for this file
    u8 m_Sha1Hash[Sha1::SHA1_DIGEST_LENGTH];

	// Whether or not this file is encrypted
	bool m_Encrypted; 
};

void PopulateFileInfo(rlCloudFileInfo *info, const netHttpRequest& request, const rlRosHttpFilter& filter);

//PURPOSE
//  This class implements the Cloud service.
//  Cloud allows players to access data stored in a global repository.
class rlCloud
{
public:

    enum PostType
    {
        POST_REPLACE,   //Replace file
        POST_APPEND     //Append new content to file
    };

    static const rlCloudMemberId INVALID_MEMBER_ID;

    //This controls how quickly we can download cloud files
    static const unsigned s_FileReadSize = 2048;

    //PURPOSE
    //  Init/Update/Shutdown
    static bool Init();
    static bool IsInitialized();
    static void Shutdown();
    static void Update();

    //PURPOSE
    //  Retrieves a file from the "members" namespace.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is requesting the file.
    //  targetMemberId      - Member ID of user who owns the file requested.
    //                        Pass an invalid rlCloudMemberId if the file is
    //                        owned by the gamer identified by localGamerIndex.
    //  onlineSvc           - Online service for the request.
    //                        If RL_CLOUD_ONLINE_SERVICE_SC the targetMemberId
    //                        must be a valid Rockstar ID.
    //  filePath            - Path of the file to retrieve
    //  ifModifiedSincePosixTime - Retrieve file only if newer.  Represented
    //                             as number of seconds since Jan 1, 1970 00:00:00
    //  httpOptions         - HTTP options to be specified with the request
    //  responseDevice      - fiDevice to which response data will be written
    //  responseHandle      - fiHandle to which response data will be written
    //  fileInfo            - Optional.  If present will be populated with file info
    //                        from the HTTP response headers.
    //  status              - Can be monitored for completion
    //                        On failure status->GetResultCode() will return
    //                        the HTTP status code, e.g. 404.
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the responseDevice, responseHandle, or status
    //  objects until the operation is complete.
    //
    //  If the file is not returned because it's not newer than ifModifiedSince,
    //  then nothing will have been written to the response handle, and netStatus
    //  will report success with a status code of 304 (HTTP 304 means "not modified").
    //EXAMPLES
    //  //Retrieve a SC file for the local player at index 0.
    //  rlCloud::GetMemberFile(0, NULL, RL_CLOUD_ONLINE_SERVICE_SC, "mp3/mpsaves/save1.json", fid, fih, &myStatus);
    //
    //  //Retrieve a XBL file (when called from xbox) for the local player at index 0.
    //  rlCloud::GetMemberFile(0, NULL, ONLINE_SERVICE_NATIVE, "mp3/mpsaves/save1.json", fid, fih, &myStatus);
    //
    //  //Retrieve a file for the remote player with NP ID BloodSplr (on ps), on behalf
    //  //of the local player at index 0.
    //  rlCloudId memberId = {"BloodSplr"};
    //  rlCloud::GetMemberFile(0, &memberId, ONLINE_SERVICE_NATIVE, "gta5/profile/images/avatar.jpeg", fid, fih, &myStatus);
    static bool GetMemberFile(const int localGamerIndex,
                                const rlCloudMemberId& targetMemberId,
                                const rlCloudOnlineService onlineSvc,
                                const char* filePath,
                                const rlRosSecurityFlags securityFlags,
                                const u64 ifModifiedSince,
                                const netHttpOptions httpOptions,
                                const fiDevice* responseDevice,
                                const fiHandle responseHandle,
                                rlCloudFileInfo* fileInfo,
                                sysMemAllocator* allocator,
                                netStatus* status);


    //PURPOSE
    //  Uploads or appends a file to the "members" namespace.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is uploading the file.
    //  onlineSvc           - Online service for the request.
    //                        If RL_CLOUD_ONLINE_SERVICE_SC the gamer at
    //                        localGamerIndex must have a valid Rockstar ID.
    //  dstFilePath         - Destination path of the file
    //  data                - Data to upload
    //  sizeofData          - Size of data to upload
    //  postType            - Replace (create) or Append.  In either case if
    //                        the file doesn't exist it will be created.
    //  fileInfo            - Optional.  If present will be populated with file info
    //                        from the HTTP response headers.
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the data or status objects until the operation
    //  is complete.
    //EXAMPLES
    //  //Upload a SC file for the local player at index 0.
    //  //If the file exists it is replaced.
    //  rlCloud::PostMemberFile(0, RL_CLOUD_ONLINE_SERVICE_SC, "mp3/mpsaves/save1.json", saveData, saveDataLen, rlCloud::POST_REPLACE, &myStatus);
    //
    //  //Append to a XBL file (when called from xbox) for the local player at index 0.
    //  //If the file doesn't exist it is created.
    //  rlCloud::PostMemberFile(0, ONLINE_SERVICE_NATIVE, "mp3/logs/log1.txt", saveData, saveDataLen, rlCloud::POST_APPEND, &myStatus);
    static bool PostMemberFile(const int localGamerIndex,
                                const rlCloudOnlineService onlineSvc,
                                const char* dstFilePath,
                                const void* data,
                                const unsigned sizeofData,
                                const PostType postType,
                                rlCloudFileInfo* fileInfo,
                                sysMemAllocator* allocator,
                                netStatus* status);

    //PURPOSE
    //  Same as above but uses a file stream for the source data.
    //NOTES
    //  Do not deallocate the responseDevice, responseHandle, or status
    //  objects until the operation is complete.
    static bool PostMemberFile(const int localGamerIndex,
                                const rlCloudOnlineService onlineSvc,
                                const char* dstFilePath,
                                const fiDevice* srcDevice,
                                const fiHandle srcHandle,
                                const PostType postType,
                                rlCloudFileInfo* fileInfo,
                                sysMemAllocator* allocator,
                                netStatus* status);

    //PURPOSE
    //  Register a callback to watch for updates to an item in the
    //  "members" namespace.
    //  If an item owned by the gamer at localGamerIndex, within the online
    //  service section specified by onlineSvc, and matching pathSpec
    //  is updated the watcher callback will be called.
    //  This is useful when, for example, an item can be modified from a
    //  mobile device and we want to watch for updates from a console.
    //PARAMS
    //  localGamerIndex     - Local index of gamer owning the item to watch.
    //  onlineSvc           - Online service for the request.
    //                        If RL_CLOUD_ONLINE_SERVICE_SC the gamer at
    //                        localGamerIndex must have a valid Rockstar ID.
    //  pathSpec            - Path spec of item to watch.
    //                        The path is relative to the member folder.
    //                        E.g. if the full path is:
    //                              "/members/xbl/2535285329554211/gta5/foo.txt"
    //                        then the pathSpec would be:
    //                              "gta5/foo.txt"
    //                        Folder separators must be "/", not "\".
    //  watcher             - Callback that is called in response to updates.
    //                        Callbacks should be declared like:
    //                              void OnModified(const char* pathSpec, const char* fullPath);
    //
    //                        pathSpec    - The path spec passed to rlCloud::WatchMemberItem().
    //                        fullPath    - The full cloud path of the file that was modified.
    //NOTES
    //  Folder separators in the path spec must be "/", not "\".
    //EXAMPLE
    //  static void OnUgcUpdate(const char* pathSpec, const char* fullPath)
    //  {
    //      printf("File modified: %s", path);
    //  }
    //  static rlCloudWatcher watcher(&OnUgcUpate);
    //  rlCloud::WatchMemberItem(0, RL_CLOUD_ONLINE_SERVICE_SC, "gta5/ugc/slot1.json", &watcher);
    static bool WatchMemberItem(const int localGamerIndex,
                                const rlCloudOnlineService onlineSvc,
                                const char* pathSpec,
                                rlCloudWatcher* watcher);

    //PURPOSE
    //  Unregister a file watcher callback.
    static bool UnwatchMemberItem(rlCloudWatcher* watcher);


	//PURPOSE
	// Similar to member watcher, but register to watch for updates from an item in the 
	//	"crews" namespace.
	//PARAMS
	//						SAME AS ABOVE
	//	clanId				- clan id of the clan to watch for
	//NOTES
	//	Only file events for the player's primary crew will be received.
	static bool WatchCrewItem(const int localGamerIndex,
								rlClanId clanId,
								const rlCloudOnlineService onlineSvc,
								const char* pathSpec,
								rlCloudWatcher* watcher);

	//PURPOSE
	//  Unregister a crew file watcher callback.
	static bool UnwatchCrewItem(rlCloudWatcher* watcher);

    //PURPOSE
    //  Deletes a file from the members namespace.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is uploading the file.
    //                        The gamer must have a valid Rockstar ID.
    //  onlineSvc           - Online service for the request.
    //                        If RL_CLOUD_ONLINE_SERVICE_SC the gamer at
    //                        localGamerIndex must have a valid Rockstar ID.
    //  filePath            - Path of file to delete
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the status object until the operation
    //  is complete.
    //EXAMPLES
    //  //Delete a SC file for the local player at index 0.
    //  rlCloud::DeleteMemberFile(0, RL_CLOUD_ONLINE_SERVICE_SC, "mp3/mpsaves/save1.json", &myStatus);
    static bool DeleteMemberFile(const int localGamerIndex,
                                const rlCloudOnlineService onlineSvc,
                                const char* filePath,
                                sysMemAllocator* allocator,
                                netStatus* status);

    //PURPOSE
    //  Retrieves a file from the "titles" namespace.
    //PARAMS
    //  filePath            - Path of the file to retrieve
    //  ifModifiedSincePosixTime - Retrieve file only if newer.  Represented
    //                             as number of seconds since Jan 1, 1970 00:00:00
    //  httpOptions         - HTTP options to be specified with the request
    //  responseDevice      - fiDevice to which response data will be written
    //  responseHandle      - fiHandle to which response data will be written
    //  fileInfo            - Optional.  If present will be populated with file info.
    //  status              - Can be monitored for completion
    //                        On failure status->GetResultCode() will return
    //                        the HTTP status code, e.g. 404.
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the responseDevice, responseHandle, or status
    //  objects until the operation is complete.
    //
    //  If the file is not returned because it's not newer than ifModifiedSince,
    //  then nothing will have been written to the response handle, and netStatus
    //  will report success with a status code of 304 (HTTP 304 means "not modified").
    //EXAMPLES
    //  //Retrieve a file for the local player at index 0.
    //  rlCloud::GetTitleFile("mp/playlists.xml", fid, fih, &myStatus);
    static bool GetTitleFile(const char* filePath,
                            const rlRosSecurityFlags securityFlags,
                            const u64 ifModifiedSince,
                            const netHttpOptions httpOptions,
                            const fiDevice* responseDevice,
                            const fiHandle responseHandle,
                            rlCloudFileInfo* fileInfo,
                            sysMemAllocator* allocator,
                            netStatus* status);

    //PURPOSE
    //  Performs a head request against a file from the "titles" namespace
    //PARAMS
    //  filePath            - Path of the file to retrieve
    //  fileInfo            - Optional.  If present will be populated with file info.
    //  status              - Can be monitored for completion
    //                        On failure status->GetResultCode() will return
    //                        the HTTP status code, e.g. 404.
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocator the status object or fileInfo until the operation is complete.
    //
    //EXAMPLES
    //  rlCloud::GetTitleFile("mp/playlists.xml", RLROS_SECURITY_DEFAULT, NULL, NULL, &myStatus);
    static bool HeadTitleFile(const char* filePath,
                              const rlRosSecurityFlags securityFlags,
                              rlCloudFileInfo* fileInfo,
                              sysMemAllocator* allocator,
                              netStatus* status);

    //PURPOSE
    //  Retrieves a file from the "global" namespace.
    //PARAMS
    //  filePath            - Path of the file to retrieve
    //  ifModifiedSincePosixTime - Retrieve file only if newer.  Represented
    //                             as number of seconds since Jan 1, 1970 00:00:00
    //  httpOptions         - HTTP options to be specified with the request
    //  responseDevice      - fiDevice to which response data will be written
    //  responseHandle      - fiHandle to which response data will be written
    //  fileInfo            - Optional.  If present will be populated with file info.
    //  status              - Can be monitored for completion
    //                        On failure status->GetResultCode() will return
    //                        the HTTP status code, e.g. 404.
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the responseDevice, responseHandle, or status
    //  objects until the operation is complete.
    //
    //  If the file is not returned because it's not newer than ifModifiedSince,
    //  then nothing will have been written to the response handle, and netStatus
    //  will report success with a status code of 304 (HTTP 304 means "not modified").
    //EXAMPLES
    //  //Retrieve a file for the local player at index 0.
    //  rlCloud::GetGlobalFile("mp/playlists.xml", fid, fih, &myStatus);
    static bool GetGlobalFile(const char* filePath,
                            const rlRosSecurityFlags securityFlags,
                            const u64 ifModifiedSince,
                            const netHttpOptions httpOptions,
                            const fiDevice* responseDevice,
                            const fiHandle responseHandle,
                            rlCloudFileInfo* fileInfo,
                            sysMemAllocator* allocator,
                            netStatus* status);

    //PURPOSE
    // Retrieves a file from the "crews" namespace
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is requesting the file.
    //  targetCrewId        - ID of crew from which to read the file
    //  onlineSvc           - Online service for the request.
    //  filePath            - Path of the file to retrieve
    //  ifModifiedSincePosixTime - Retrieve file only if newer.  Represented
    //                             as number of seconds since Jan 1, 1970 00:00:00
    //  httpOptions         - HTTP options to be specified with the request
    //  responseDevice      - fiDevice to which response data will be written
    //  responseHandle      - fiHandle to which response data will be written
    //  fileInfo            - Optional.  If present will be populated with file info.
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the responseDevice, responseHandle, or status
    //  objects until the operation is complete.
    //
    //  If the file is not returned because it's not newer than ifModifiedSince,
    //  then nothing will have been written to the response handle, and netStatus
    //  will report success with a status code of 304 (HTTP 304 means "not modified").
    //EXAMPLES
    //  Retrieve a file from a crew for the local player at index 0.
    //  rlCloud::GetCrewFile(0, crewCloudId, RL_CLOUD_ONLINE_SERVICE_SC,  "publish/emblem/emblem_256.dds", fid, fih, &myStatus);
    static bool GetCrewFile(const int localGamerIndex,
                            const rlCloudMemberId& targetCrewId,
                            const rlCloudOnlineService onlineSvc,
                            const char* filePath,
                            const rlRosSecurityFlags securityFlags,
                            const u64 ifModifiedSince,
                            const netHttpOptions httpOptions,
                            const fiDevice* responseDevice,
                            const fiHandle responseHandle,
                            rlCloudFileInfo* fileInfo,
                            sysMemAllocator* allocator,
                            netStatus* status);

	//PURPOSE
	// Retrieves a file from the "ugc" namespace
	//PARAMS
	//  localGamerIndex     - Local index of gamer who is requesting the file.
    //  contentType         - Content type
    //  contentId           - Content ID file is associated with
    //  fileId              - File ID (defined by content type)
    //  fileVersion         - File version
    //  language            - Language to get file in, if localized
    //  ifModifiedSincePosixTime - Retrieve file only if newer.  Represented
	//                             as number of seconds since Jan 1, 1970 00:00:00
    //  httpOptions         - HTTP options to be specified with the request
	//  responseDevice      - fiDevice to which response data will be written
	//  responseHandle      - fiHandle to which response data will be written
	//  fileInfo            - Optional.  If present will be populated with file info.
	//  status              - Can be monitored for completion
	//NOTES
	//  This is an asynchronous operation that is complete when
	//  status->Pending() returns false.
	//  Do not deallocate the responseDevice, responseHandle, or status
	//  objects until the operation is complete.
	//
	//  If the file is not returned because it's not newer than ifModifiedSince,
	//  then nothing will have been written to the response handle, and netStatus
	//  will report success with a status code of 304 (HTTP 304 means "not modified").
	//EXAMPLES
	//  Retrieve a file from a crew for the local player at index 0.
	//  rlCloud::GetUgcFile(0, targetMemberId, RL_CLOUD_ONLINE_SERVICE_NP, "gta5mission/819482389.json", fid, fih, &myStatus);
    static bool GetUgcFile(const int localGamerIndex,
                           const rlUgcContentType contentType,
                           const char* contentId,
                           const int fileId, 
                           const int fileVersion,
                           const rlScLanguage language,
                           const rlRosSecurityFlags cloudSecurityFlags,
                           const u64 ifModifiedSince,
                           const netHttpOptions httpOptions,
                           const fiDevice* responseDevice,
                           const fiHandle responseHandle,
                           rlCloudFileInfo* fileInfo,
                           sysMemAllocator* allocator,
                           netStatus* status);

    //PURPOSE
    //  Cancels the operation associated with the status object.
    static void Cancel(netStatus* status);

    //PURPOSE
    //  Build an absolute cloud path
    static bool CreateUrl(atStringBuilder* url,
                          const rlCloudNamespace ns,
                          const rlCloudOnlineService onlineSvc,
                          const rlCloudMemberId& memberId,
                          const char* filePath,
                          atStringBuilder* absoluteCloudPathBuilder);

    //PURPOSE
    //  Build an absolute cloud path
    static bool CreateCompleteUrl(atStringBuilder* url, 
                                  const char* cloudAbsPath, 
                                  atStringBuilder* normalizedCloudAbsPathBuilder);

    //PURPOSE
    //  Handler for verifying a cloud signature.
    static bool VerifyCloudSignature(const rlRosContentSignature& signature, 
                                     const char* absoluteCloudPath,
                                     const u8* hash,
                                     const unsigned hashLen);

    //PURPOSE
    //  Determines whether or not the given signature is for a non-existant file (i.e. a 404)
    static bool Verify404CloudSignature(const rlRosContentSignature& signature,
                                        const char* absoluteCloudPath);

#if !__FINAL
	//PURPOSE
	//  Allows setting of simulated fail rate for cloud operations
	static void SetSimulatedFailPct(const char* failRate);
#endif

private:

    static void OnPresenceEvent(const rlPresenceEvent* e);

    typedef inlist<rlCloudWatcher, &rlCloudWatcher::m_ListNode> CloudWatcherList;
    static CloudWatcherList sm_CloudWatcherList;
    static rlPresence::Delegate sm_PresenceEventDlgt;
};

//PURPOSE
//  Implements a fiDevice for reading and writing to Cloud.
//
//  Cloud read/write operations are asynchronous.  A Cloud operation must
//  be monitored by periodically calling fiDeviceCloud::Pending().
//  When Pending() returns false check the status by calling Succeeded(),
//  then call Close().
//
//  **********
//  Close() MUST be called at the end of an operation in order to free
//  the fiHandle
//
//  DO NOT attempt to use a fiHandle after calling Close().
//  **********

//  How to read a file:
//
//  Call GetFile() with the appropriate paramaters.
//  *Don't* call Size() to get the size of the file.  Retrieving a file
//  is asynchronous.
//  *Don't* call Seek(). Cloud files do not support seeking
//  Call Read() to read data as it becomes available.  Read() can return
//  zero if no data is available.  This doesn't mean the operation is complete,
//  as more data might become available later.
//  Call Pending() until it returns false to check if the operation is complete.
//  Call Succeeded() to determine the success of the operation.
//  Call Close() to free the fiHandle.
//
//  How to write a file:
//
//  Call CreateFile() or AppendFile() with the appropriate paramaters.
//  Call Write() to write data.
//  Call Commit() to signal the end of writing.  Alternatively, call Close().
//  Call Pending() until it returns false.
//  Call Succeeded() to determine the success of the operation.
//  If Commit() was called then call Close() to free the fiHandle.
//
//  If Close() is called instead of Commit() then Succeeded() will have
//  undefined results.  It is assumed that calling Close() indicates there
//  is no more interest in the file and the fiHandle should be freed at the
//  first opportunity.
//
class fiDeviceCloud: public fiDevice
{
public:

    fiDeviceCloud() :
        m_localGamerIndex(0)
        , m_ns(RL_CLOUD_NAMESPACE_MEMBERS)
        , m_onlineSvc(RL_CLOUD_ONLINE_SERVICE_SC)
        , m_FileInfo(NULL)
        , m_Allocator(NULL)
        {}

    //PURPOSE
    //  Setup parameters used for accessing files in the cloud
    //PARAMS
    //  localGamerIndex - Index of local gamer who's requesting the the file.
    //  ns              - Cloud namespace.
    //  onlineSvc       - Online service ID.
    //  fileInfo        - Optional.  Will be populated with the fields
    //                    from the HTTP response.  Can be NULL.
    void SetAccessParameters(const int localGamerIndex
                             ,const rlCloudNamespace ns
                             ,const rlCloudOnlineService onlineSvc
                             ,const rlCloudMemberId& memberId = rlCloudMemberId()
                             , rlCloudFileInfo* fileInfo = NULL);

    void SetAccessParameters(const int localGamerIndex
                             ,const rlCloudNamespace ns
                             ,const rlCloudOnlineService onlineSvc
                             ,sysMemAllocator* allocator
                             ,const rlCloudMemberId& memberId = rlCloudMemberId()
                             , rlCloudFileInfo* fileInfo = NULL);

    /*//PURPOSE
    //  Opens an existing file for reading.
    //PARAMS
    //  path            - Path to the file, including file name and extension.
    //NOTES
    //  For the RL_CLOUD_NAMESPACE_TITLES namespace pass RL_CLOUD_ONLINE_SERVICE_INVALID
    //  for onlineSvc.
    fiHandle GetFile(const int localGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const char* path) const;

    //PURPOSE
    //  Creates a file for writing.
    //PARAMS
    //  path            - Path to the file, including file name and extension.
    //  location        - Optional.  Will be populated with the "Location"
    //                    header of the response.  The URL in the Location
    //                    header should be used for future GETs or for posting
    //                    to external web sites.
    //                    This buffer must be large enough to hold the entire
    //                    URL.
    //                    Can be NULL.
    //  sizeofLocation  - Size of the location buffer.
    //NOTES
    //  Files cannot be created/appended in the RL_CLOUD_NAMESPACE_TITLES namespace.
    fiHandle CreateFile(const int localGamerIndex,
                        const rlCloudNamespace ns,
                        const rlCloudOnlineService onlineSvc,
                        const char* path,
                        char* location,
                        const unsigned sizeofLocation) const;

    //PURPOSE
    //  Opens a file for appending.
    //  If the file doesn't exist it's created.
    //PARAMS
    //  path            - Path to the file, including file name and extension.
    //  location        - Optional.  Will be populated with the "Location"
    //                    header of the response.  The URL in the Location
    //                    header should be used for future GETs or for posting
    //                    to external web sites.
    //                    This buffer must be large enough to hold the entire
    //                    URL.
    //                    Can be NULL.
    //  sizeofLocation  - Size of the location buffer.
    //NOTES
    //  Files cannot be created/appended in the RL_CLOUD_NAMESPACE_TITLES namespace.
    fiHandle AppendFile(const int localGamerIndex,
                        const rlCloudNamespace ns,
                        const rlCloudOnlineService onlineSvc,
                        const char* path,
                        char* location,
                        const unsigned sizeofLocation) const;*/

    //PURPOSE
    //  Used when writing to a file.  Indicates there
    //  will be no further writes and all data should be flushed
    //  to Cloud.
    //NOTES
    //  Call Pending() to check the status of the operation.
    //  If Pending() returns false check Succeeded() to determine
    //  the success of the operation.
    //  Call Close() to free the handle.
    int Commit(fiHandle handle) const;

    //PURPOSE
    //  Returns true if the file operation is complete.
    bool Pending(fiHandle handle) const;

    //PURPOSE
    //  Returns true if the operation completed and was successful.
    bool Succeeded(fiHandle handle) const;

    //PURPOSE
    //  Returns true if the content size matches the content header size
    bool IsValid(fiHandle handle) const;

    //PURPOSE
    //  Returns content size indicated by content header
    unsigned GetContentLength(fiHandle handle) const; 

    virtual fiHandle Open(const char* filePath,bool readOnly) const;
    //Not Implemented
    virtual fiHandle OpenBulk(const char *filePath,u64 &outBias) const;
    virtual fiHandle Create(const char *filePath) const;
    virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
    virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
    virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
    virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
    virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
    //Calling Close() when writing to a file is equivalent to calling
    //Commit(), then Close().
    virtual int Close(fiHandle handle) const;
    virtual int CloseBulk(fiHandle handle) const;
    virtual int Size(fiHandle handle) const;
    virtual u64 Size64(fiHandle handle) const;
    //Not Implemented
    virtual u64 GetFileTime(const char* filePath) const;
    //Not Implemented
    virtual bool SetFileTime(const char* filePath,u64 timestamp) const;
    //Not Implemented
    virtual u32 GetAttributes(const char*) const;
    //Not Implemented
    virtual u64 GetFileSize(const char* filePath) const;
    virtual bool IsCloud() const { return true; }
    virtual const char *GetDebugName() const;

	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return NETWORK; }

    // Singleton accessor
    static fiDeviceCloud& GetInstance();

private:
    int m_localGamerIndex;
    rlCloudNamespace m_ns;
    rlCloudOnlineService m_onlineSvc;
    rlCloudMemberId m_memberId;
    rlCloudFileInfo* m_FileInfo;
    sysMemAllocator* m_Allocator;
};

} //namespace rage

#endif  //RLINE_RLCLOUD_H
