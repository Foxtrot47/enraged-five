// 
// rline/rlcloud.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "rline/cloud/rlcloud.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/rltitleid.h"
#include "data/base64.h"
#include "data/sha1.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/http.h"
#include "parsercore/utils.h"
#include "string/stringutil.h"
#include "system/nelem.h"

#include <stdio.h>
#include <time.h>

namespace rage
{

#if !__FINAL
PARAM(nocloudsometimes, "Debug setting to cause cloud to intermittently fail.  -nocloudsometimes=<failure percentage>");
//Used for simulating failures
static bool s_rlCloudRngInitialized = false;
static netRandom s_rlCloudRng;
#endif

RAGE_DEFINE_SUBCHANNEL(rline, cloud)
#undef __rage_channel
#define __rage_channel rline_cloud

extern const rlTitleId* g_rlTitleId;

static bool s_RlCloudInitialized;

extern sysMemAllocator* g_rlAllocator;

static const unsigned HTTP_REQUEST_TIMEOUT_SECONDS  = 30;
static const unsigned MAX_FILENAME_LENGTH   = 64;

static const char* NATIVE_ONLINE_SERVICE_STRING_SC  = "sc";

#if RSG_DURANGO
static const char* NATIVE_ONLINE_SERVICE_STRING = "xbl";
#elif RSG_ORBIS
static const char* NATIVE_ONLINE_SERVICE_STRING = "np";
#else
static const char* NATIVE_ONLINE_SERVICE_STRING = NATIVE_ONLINE_SERVICE_STRING_SC;
#endif

const rlCloudMemberId rlCloud::INVALID_MEMBER_ID;

rlCloud::CloudWatcherList rlCloud::sm_CloudWatcherList;

rlPresence::Delegate rlCloud::sm_PresenceEventDlgt(&rlCloud::OnPresenceEvent);

//////////////////////////////////////////////////////////////////////////
//  fiCloudHandle
//////////////////////////////////////////////////////////////////////////
class fiCloudHandle
{
public:

    enum State
    {
        STATE_GETTING,
        STATE_POSTING,
        STATE_POST_COMMIT,
        STATE_POST_COMMITTING,
        STATE_SUCCESS,
        STATE_ERROR,
        STATE_CLOSED,
    };

    fiCloudHandle();

    bool Get(const int localGamerIndex,
             const char* cloudAbsPath,
             const rlRosSecurityFlags cloudSecurityFlags,
             const u64 ifModifiedSince,
             const netHttpOptions httpOptions,
             rlCloudFileInfo* fileInfo,
             sysMemAllocator* allocator);

    bool Get(const int localGamerIndex,
            const rlCloudNamespace ns,
            const rlCloudOnlineService onlineSvc,
            const rlCloudMemberId& targetMemberId,
            const char* path,
            const rlRosSecurityFlags cloudSecurityFlags,
            const u64 ifModifiedSince,
            const netHttpOptions httpOptions,
            rlCloudFileInfo* fileInfo,
            sysMemAllocator* allocator);
    
    bool Post(const int localGamerIndex,
            const rlCloudNamespace ns,
            const rlCloudOnlineService onlineSvc,
            const char* path,
            const rlCloud::PostType postType,
            rlCloudFileInfo* fileInfo,
            sysMemAllocator* allocator);

    int Read(void* outBuffer,int bufferSize);

    int Write(const void* buffer, int bufferSize);

    int Commit();

    int Close();

    void Cancel();

    bool Getting() const;

    bool Posting() const;

    bool Pending() const;

    bool Succeeded() const;

    const char* GetUri() const;

    unsigned GetStatusCode() const;

    void Update();

    void PopulateFileInfo();

    State m_State;
    netHttpRequest m_HttpRequest;
    char m_MultipartBoundary[32];
    netStatus m_Status;

    rlRosHttpFilter m_Filter;

    rlCloudFileInfo* m_FileInfo;

    //Our cached request path so that we can verify file signatures
    //(which requires knowing the request path)
    char m_AbsoluteCloudPath[MAX_FILEPATH_LENGTH + 1];

#if !__FINAL
    //Used when -nocloudsometimes is present.
    //Represents the percentage of time we should simulate failures.
    int m_SimulateFailPct;
#endif

    //True if Close() was called prior to completing a POST request.
    //It indicates the caller is no longer interested in the result.
    bool m_CloseAfterCommit : 1;


private:
    //Verifies a digital signature. Mainly this is responsible for obtaining the resource identifier
    //for the file, which is the absolute cloud path.
    bool VerifySignature(const rlRosContentSignature& signature, const u8* hash, const unsigned hashLen);
};

static const int CLOUD_HANDLE_POOL_SIZE     = 4;
static fiCloudHandle s_CloudHandles[CLOUD_HANDLE_POOL_SIZE];

//////////////////////////////////////////////////////////////////////////
// rlCloudMemberId
//////////////////////////////////////////////////////////////////////////

rlCloudMemberId::rlCloudMemberId()
{
    m_IdString[0] = '\0';
}

rlCloudMemberId::rlCloudMemberId(const u64 memberId)
{
    this->Reset(memberId);
}

rlCloudMemberId::rlCloudMemberId(const rlGamerHandle& gamerHandle)
{
    this->Reset(gamerHandle);
}

void
rlCloudMemberId::Clear()
{
    m_IdString[0] = '\0';
}

void
rlCloudMemberId::Reset(const u64 memberId)
{
    this->Clear();
    if(rlVerify(memberId > 0))
    {
        formatf(m_IdString, "%" I64FMT "u", memberId);
    }
}

void
rlCloudMemberId::Reset(const rlGamerHandle& gamerHandle)
{
    if(rlVerify(gamerHandle.IsValid()))
    {
#if RSG_DURANGO
        //XBL uses the XUID in decimal format.
        formatf(m_IdString, "%" I64FMT "u", gamerHandle.GetXuid());
#elif RSG_ORBIS 
        if(rlGamerHandle::IsUsingAccountIdAsKey())
        {
            formatf(m_IdString, "%" I64FMT "u", gamerHandle.GetNpAccountId());
        }
        else
        {
            formatf(m_IdString, "%s", gamerHandle.GetNpOnlineId().data);
        }
#elif RSG_PC
		//PC uses the RockstarId.
		formatf(m_IdString, "%" I64FMT "d", gamerHandle.GetRockstarId());
#else
        rlAssert(false);
#endif
    }
}

const char*
rlCloudMemberId::ToString() const
{
    return IsValid() ? m_IdString : "";
}

bool
rlCloudMemberId::IsValid() const
{
    return '\0' != m_IdString[0];
}

//////////////////////////////////////////////////////////////////////////
//  rlCloud Tasks
//////////////////////////////////////////////////////////////////////////

//Determines the local gamer's Cloud member ID based on the online
//service being used.
static bool GetMemberId(const int localGamerIndex,
                        const rlCloudOnlineService onlineSvc,
                        rlCloudMemberId* memberId)
{
    bool success = false;

    rtry
    {
        rverify(RL_CLOUD_ONLINE_SERVICE_SC == onlineSvc
                || RL_CLOUD_ONLINE_SERVICE_NATIVE == onlineSvc, catchall,);

        if(RL_CLOUD_ONLINE_SERVICE_SC == onlineSvc
            || (RL_CLOUD_ONLINE_SERVICE_NATIVE == onlineSvc && RLROS_SC_PLATFORM))
        {
            //For the "sc" service the member ID is the Rockstar ID.

            const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);

            rverify(creds.IsValid(),
                    catchall,
                    rlError("Gamer at index:%d doesn't have valid ROS credentials",
                                localGamerIndex));

            rcheck(creds.GetRockstarId() != InvalidRockstarId,
                    catchall,
                    rlError("Gamer at index:%d is not linked to a Social Club account",
                            localGamerIndex));

            memberId->Reset(creds.GetRockstarId());

            success = true;
        }
        else
        {
            //For the "xbl"/"np"/"..." services the member ID is the service-specific
            //user ID.

            rlGamerHandle gh;
            rverify(rlPresence::GetGamerHandle(localGamerIndex, &gh),
                    catchall,
                    rlError("Error retrieving gamer handle"));

            memberId->Reset(gh);

            success = true;
        }
    }
    rcatchall
    {
    }

    return success;
}

static const char* GetNamespaceString(const rlCloudNamespace ns)
{
    switch((int) ns)
    {
    case RL_CLOUD_NAMESPACE_MEMBERS:
        return "members";
    case RL_CLOUD_NAMESPACE_CREWS:
        return "crews";
	case RL_CLOUD_NAMESPACE_UGC:
		return "ugc";
    case RL_CLOUD_NAMESPACE_TITLES:
        return "titles";
    case RL_CLOUD_NAMESPACE_GLOBAL:
        return "global";
    }

    return NULL;
}

static const char* GetOnlineServiceString(const rlCloudOnlineService onlineSvc)
{
    rlAssert(RL_CLOUD_ONLINE_SERVICE_SC == onlineSvc
            || RL_CLOUD_ONLINE_SERVICE_NATIVE == onlineSvc);

    if(RL_CLOUD_ONLINE_SERVICE_SC == onlineSvc)
    {
        return NATIVE_ONLINE_SERVICE_STRING_SC;
    }
    else if(RL_CLOUD_ONLINE_SERVICE_NATIVE == onlineSvc)
    {
        return NATIVE_ONLINE_SERVICE_STRING;
    }
    else
    {
        return NULL;
    }
}

//Hit SCS for all cloud requests.  SCS will redirect to CDN where necessary.

bool rlCloud::CreateUrl(atStringBuilder* url,
                               const rlCloudNamespace ns,
                               const rlCloudOnlineService onlineSvc,
                               const rlCloudMemberId& memberId,
                               const char * filePath,
                               atStringBuilder* absoluteCloudPathBuilder)
{
    bool success = false;

    url->Clear();
    char buf[MAX_FILEPATH_LENGTH+1];

	rtry
	{
		//String template for "members" namespace:
		//"/cloud/<version>/cloudservices/members/<online service>/<member ID>/<filename>".
		//
		//String template for "crews" namespace:
		//"/cloud/<version>/cloudservices/crews/<online service>/<crew_id>/<filename>".
		//
		//String template for "ugc" namespace:
		//"/cloud/<version>/cloudservices/ugc/<online service>/<member_ID>/<filename>".
		//
		//String template for "titles" namespace:
		//"/cloud/<version>/cloudservices/titles/<title name>/<platform name>/<filename>".
		if (url->AppendOrFail("/cloud/")
			&& url->AppendOrFail(formatf(buf, "%d/", g_rlTitleId->m_RosTitleId.GetScVersion()))
			&& url->AppendOrFail("cloudservices/")
			&& url->AppendOrFail(formatf(buf, "%s/", GetNamespaceString(ns))))
		{
			bool pathIsGood = false;
			if(RL_CLOUD_NAMESPACE_MEMBERS == ns)
			{
				pathIsGood = rlVerify(memberId.IsValid())
					&&url->AppendOrFail(formatf(buf, "%s/", GetOnlineServiceString(onlineSvc)))
					&&url->AppendOrFail(formatf(buf, "%s/", memberId.ToString()));
			}
			else if(RL_CLOUD_NAMESPACE_CREWS == ns)
			{
				pathIsGood = rlVerify(memberId.IsValid()) 
					&& url->AppendOrFail(formatf(buf, "%s/", GetOnlineServiceString(onlineSvc))) 
					&& url->AppendOrFail(formatf(buf, "%s/", memberId.ToString()));
			}
			else if(RL_CLOUD_NAMESPACE_UGC == ns)
			{
				pathIsGood = rlVerify(memberId.IsValid())
					&& url->AppendOrFail(formatf(buf, "%s/", GetOnlineServiceString(onlineSvc)))
					&& url->AppendOrFail(formatf(buf, "%s/", memberId.ToString()));
			}
			else if(RL_CLOUD_NAMESPACE_TITLES == ns)
			{
				pathIsGood = url->AppendOrFail(formatf(buf, "%s/", g_rlTitleId->m_RosTitleId.GetTitleName()))
					&& url->AppendOrFail(formatf(buf, "%s/", g_rlTitleId->m_RosTitleId.GetPlatformName()));
			}
			else if(RL_CLOUD_NAMESPACE_GLOBAL == ns)
			{
				// NOOP
				pathIsGood = true;
			}

			rverify(pathIsGood, catchall, rlError("CreateUrl: Path was not valid"));

			char buf[MAX_FILEPATH_LENGTH+1];

			//Trim spaces
			StringTrim(buf, filePath, sizeof(buf));

			//Replace \ with /
			std::replace(buf, &buf[strlen(buf)], '\\', '/');

			//Trim trailing slashes
			StringRTrim(buf, buf, '/', sizeof(buf));

			rverify(strlen(buf) > 0, catchall, rlError("An empty filePath has been specified [%s]", filePath));

			rverify(url->AppendOrFail(buf), catchall, rlError("Error appending file path to URL"));

			const char *path = url->ToString();

            //Compute our cloud absolute path, which is everything after our /cloudservices
            if (absoluteCloudPathBuilder)
            {
                absoluteCloudPathBuilder->Clear();

                const char* servicesSeparator = "/cloudservices";
                const char* foundServicesSeparator = strstr(path, servicesSeparator);
                rverify(foundServicesSeparator != NULL, catchall,);

                const char* absoluteCloudPath = (foundServicesSeparator + strlen(servicesSeparator));
                rverify(absoluteCloudPathBuilder->AppendOrFail(absoluteCloudPath), catchall, rlError("Error building abs cloud path"));
            }

            char domainNameBuf[128];
			const char* domainName = rlRos::GetServiceHost(path, domainNameBuf);

			if (!domainName)
			{
				domainName = g_rlTitleId->m_RosTitleId.GetDomainName();
			}

			rlAssert(domainName);

            //Use https as appropriate based on ssl service configuration
            formatf(buf, rlRos::IsSslService(path) ? "https://%s" : "http://%s", domainName);

			rverify(url->PrependOrFail(buf), catchall, rlError("Error prepending domain Name to file"));
			
			success = true;
		}
	}
	rcatchall
	{
		success = false;
	}

    return success;
}

bool rlCloud::CreateCompleteUrl(atStringBuilder* url, const char* cloudAbsPath, atStringBuilder* normalizedCloudAbsPathBuilder)
{
    rtry
    {
        rverify((url != NULL) && (cloudAbsPath != NULL), 
                catchall, 
                rlError("Invalid arguments"));

		char path[MAX_FILEPATH_LENGTH+1];

		//Trim spaces
		StringTrim(path, cloudAbsPath, sizeof(path));

		//Replace \ with /
		std::replace(path, &path[strlen(path)], '\\', '/');

		//Trim trailing slashes
		StringRTrim(path, path, '/', sizeof(path));

        //Compute our normalized cloud absolute path
        if (normalizedCloudAbsPathBuilder)
        {
            normalizedCloudAbsPathBuilder->Clear();
            rverify(normalizedCloudAbsPathBuilder->AppendOrFail(path), catchall, rlError("Failed to build cloud abs path"));
        }

        char domainNameBuf[128];
		const char* domainName = rlRos::GetServiceHost(path, domainNameBuf);
		if (!domainName)
		{
			domainName = g_rlTitleId->m_RosTitleId.GetDomainName();
		}

		rlAssert(domainName);

        url->Clear();

		char buf[MAX_FILEPATH_LENGTH+1];
        //Create the prefix
        //Use https as appropriate based on ssl service configuration
        rcheck(url->AppendOrFail(rlRos::IsSslService(path) ? "https://" : "http://")
            && url->AppendOrFail(formatf(buf, "%s/", domainName))
            && url->AppendOrFail("cloud/")
            && url->AppendOrFail(formatf(buf, "%d/", g_rlTitleId->m_RosTitleId.GetScVersion()))
            && url->AppendOrFail("cloudservices"),
               catchall,
               rlError("Failed to build prefix"));

		return url->AppendOrFail(path);
    }
    rcatchall
    {
        return false;
    }
}

class rlCloudGetFileTask : public rlTaskBase
{
public:

	RL_TASK_USE_CHANNEL(rline_cloud);
	RL_TASK_DECL(rlCloudGetFileTask);

    rlCloudGetFileTask()
        : m_fiDevice(NULL)
        , m_fiHandle(fiHandleInvalid)
    {
    }

    ~rlCloudGetFileTask()
    {
    }

    bool Configure(const int localGamerIndex,
                   const char* cloudAbsPath,
                   const rlRosSecurityFlags cloudSecurityFlags,
                   const u64 ifModifiedSince,
                   const netHttpOptions httpOptions,
                   const fiDevice* responseDevice, //Dest IO device
                   const fiHandle responseHandle,  //Dest IO file handle
                   rlCloudFileInfo* fileInfo,
                   sysMemAllocator* allocator)
    {
        if(m_fiCloudHandle.Get(localGamerIndex,
                               cloudAbsPath,
                               cloudSecurityFlags,
                               ifModifiedSince,
                               httpOptions,
                               fileInfo,
                               allocator))
        {
            m_fiDevice = responseDevice;
            m_fiHandle = responseHandle;
            return true;
        }

        return false;
    }

    bool Configure(const int localGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const rlCloudMemberId& targetMemberId,
                    const char* filePath,
                    const rlRosSecurityFlags cloudSecurityFlags,
                    const u64 ifModifiedSince,
                    const netHttpOptions httpOptions,
                    const fiDevice* responseDevice, //Dest IO device
                    const fiHandle responseHandle,  //Dest IO file handle
                    rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator)
    {
        if(m_fiCloudHandle.Get(localGamerIndex,
                                ns,
                                onlineSvc,
                                targetMemberId,
                                filePath,
                                cloudSecurityFlags,
                                ifModifiedSince,
                                httpOptions,
                                fileInfo,
                                allocator))
        {
            m_fiDevice = responseDevice;
            m_fiHandle = responseHandle;
            return true;
        }

        return false;
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel()
    {
        m_fiCloudHandle.Cancel();
    }

    virtual void Update(const unsigned /*timeStepMs*/)
    {
        m_fiCloudHandle.Update();

        if(!WasCanceled()
            && (m_fiCloudHandle.Pending() || m_fiCloudHandle.Succeeded()))
        {
            // Note: This previous code was always reading into m_BounceBuf. As such, it broke
            // if we ever failed to write the entire output to m_fiDevice, since it instead
            // should have been reading into &m_BounceBuf[m_BounceBufLen] in order to not overwrite
            // previously buffered data.
            // As an optimization, if we enforce that assumption anyways, we don't
            // need the bounce buffer and can instead just read into a temporary buffer
            // on the stack, saving some memory. The task will fail if the output
            // device can't buffer the entire response
            u8 buffer[rlCloud::s_FileReadSize];
            const int len = m_fiCloudHandle.Read(buffer, sizeof(buffer));
            //const int len = m_fiCloudHandle.Read(m_BounceBuf, sizeof(m_BounceBuf));
            if(len > 0)
            {
                const int numConsumed =
                    m_fiDevice->Write(m_fiHandle, buffer, len);
                // If we fail to write all of the output, mark the task as having failed
                // since we don't currently support buffering it
                if(numConsumed != len)
                {
                    rlTaskError("Failed to write all of read block to output device");
                    this->Finish(FINISH_FAILED, RL_CLOUD_ERROR_OUT_OF_MEMORY);

					// populate file info, we may have the header which we can use to determine how much memory
					// is required should we want to retry the request
					m_fiCloudHandle.PopulateFileInfo();
					m_fiCloudHandle.Close();
                }
            }
            else if(m_fiCloudHandle.Succeeded())
            {
                rlTaskDebug("Retrieved %s, status code:%d",
                            m_fiCloudHandle.GetUri(),
                            m_fiCloudHandle.GetStatusCode());

                this->Finish(FINISH_SUCCEEDED, m_fiCloudHandle.GetStatusCode());

                m_fiCloudHandle.Close();
            }
        }
        else
        {
            rlTaskError("Failed to retrieve %s, status code:%d",
                        m_fiCloudHandle.GetUri(),
                        m_fiCloudHandle.GetStatusCode());
            this->Finish(FINISH_FAILED, m_fiCloudHandle.GetStatusCode());

            m_fiCloudHandle.Close();
        }
    }

private:

    fiCloudHandle m_fiCloudHandle;
    const fiDevice* m_fiDevice;
    fiHandle m_fiHandle;
};

void PopulateFileInfo(rlCloudFileInfo *info, const netHttpRequest& request, const rlRosHttpFilter& filter)
{
    //Get the file info (last modified date, content length, etc.);
    if(info)
    {
        info->Clear();

		// Calculate the buffer size needed
		unsigned maxBufSize = rage::Max((unsigned)sizeof(info->m_Location), (unsigned)sizeof(info->m_ETag));
		
		// Create a buffer
        char* buf = (char*)Alloca(char, maxBufSize);
		unsigned bufLen = maxBufSize;

		if (buf)
		{
			//Ignore certain headers if we get a 404
			if (request.GetStatusCode() != NET_HTTPSTATUS_NOT_FOUND)
			{
				//Content-Length
				info->m_ContentLength = request.GetResponseContentLength();

				//Last-Modified
				if(request.GetResponseHeaderValue("Last-Modified", buf, &bufLen))
				{
					info->m_LastModifiedPosixTime = netHttpRequest::ParseDateString(buf);
				}

				//Location.
				bufLen = maxBufSize; // reset buffer size as input/output param to GetResponseHeaderValue
				sysMemSet(buf, 0, bufLen);
				if(request.GetResponseHeaderValue("Location", buf, &bufLen))
				{
					rlAssertf(bufLen < sizeof(info->m_Location),
						"Location '%s' is too long", buf);
					safecpy(info->m_Location, buf);
				}

				//ETag
				bufLen = maxBufSize; // reset buffer size as input/output param to GetResponseHeaderValue
				sysMemSet(buf, 0, bufLen);
				if(request.GetResponseHeaderValue("ETag", buf, &bufLen))
				{
					rlAssertf(bufLen < sizeof(info->m_ETag),
						"ETag '%s' is too long", buf);
					safecpy(info->m_ETag, buf);
				}
			}

			//We can still get a content hash/signature with a 404

			//Signature
			info->m_HaveSignature = filter.GetContentSignature(&info->m_Signature);

			//Hash
			info->m_HaveHash = filter.GetContentHash(info->m_Sha1Hash);
		}
    }
}

class rlCloudHeadFileTask : public rlTaskBase
{
public:

	RL_TASK_USE_CHANNEL(rline_cloud);
	RL_TASK_DECL(rlCloudHeadFileTask);

    rlCloudHeadFileTask()
    : m_FileInfo(NULL)
    {
    }

    ~rlCloudHeadFileTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const rlCloudMemberId& targetMemberId,
                    const char* filePath,
                    const rlRosSecurityFlags cloudSecurityFlags,
                    rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator)
    {
        rtry
        {
            //Just construct the absolute cloud path and configure based on that
            char completeUrlBuf[MAX_FILEPATH_LENGTH+1];
            char absoluteCloudPathBuf[MAX_FILEPATH_LENGTH+1];
            atStringBuilder completeUrl(completeUrlBuf, sizeof(completeUrlBuf));
            atStringBuilder absoluteCloudPath(absoluteCloudPathBuf, sizeof(absoluteCloudPathBuf));
            rverify(rlCloud::CreateUrl(&completeUrl, ns, onlineSvc, targetMemberId, filePath, &absoluteCloudPath),
                    catchall,
                    rlError("Error building URL"));

            rverify(Configure(localGamerIndex,
                              absoluteCloudPath.ToString(),
                              cloudSecurityFlags,
                              fileInfo,
                              allocator),
                    catchall, );

            return true;
        }
        rcatchall
        {
            return false;
        }
    }

    bool Configure(const int localGamerIndex,
                   const char* cloudAbsPath,
                   const rlRosSecurityFlags cloudSecurityFlags,
                   rlCloudFileInfo* fileInfo,
                   sysMemAllocator* allocator)
    {
        rtry
        {
            rverify(cloudAbsPath, catchall, );
            rverify(strlen(cloudAbsPath) <= MAX_FILEPATH_LENGTH, catchall, );

            //Make sure our credentials are valid
            const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
            rverify(creds.IsValid(), 
                    catchall,
                    rlError("Invalid credentials"));

            //Create the complete url from our absolute cloud path
            char urlBuf[MAX_FILEPATH_LENGTH+1];
            atStringBuilder url(urlBuf, sizeof(urlBuf));
            rverify(rlCloud::CreateCompleteUrl(&url, cloudAbsPath, NULL), catchall, rlError("Error building URL"));

            m_HttpRequest.Init(allocator ? allocator : g_rlAllocator, rlRos::GetRosSslContext());

            rverify(m_Filter.Configure(localGamerIndex, url.ToString(), cloudSecurityFlags), catchall, );

            rverify(m_HttpRequest.BeginHead(url.ToString(), 
                                            NULL, //proxyAddr
                                            HTTP_REQUEST_TIMEOUT_SECONDS,
                                            this->GetTaskName(),
                                            &m_Filter,
                                            &m_MyStatus),
                    catchall, );

            //Add the ROS ticket to the HTTP headers, not the query string,
            //as adding it to the query string makes the URL unique and busts the cache.
            //TODO: We should encrypt our ticket with our session key or something
            m_HttpRequest.AddRequestHeaderValue("Scs-Ticket", creds.GetTicket());

            m_FileInfo = fileInfo;
            if (m_FileInfo)
            {
                m_FileInfo->Clear();
            }

            rverify(m_HttpRequest.Commit(),
                    catchall,
                    rlError("Failed to head %s", m_HttpRequest.GetUri()));

            return true;
        }
        rcatchall
        {
            return false;
        }
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel()
    {
        rlGetTaskManager()->CancelTask(&m_MyStatus);
        m_HttpRequest.Cancel();
    }

    virtual void Update(const unsigned /*timeStepMs*/)
    {
        m_HttpRequest.Update();

        if (m_MyStatus.Succeeded())
        {
            if (m_HttpRequest.Succeeded())
            {
                rlTaskDebug("Finished HEAD %s, status code:%d",
                            m_HttpRequest.GetUri(),
                            m_HttpRequest.GetStatusCode());
                this->Finish(FINISH_SUCCEEDED, m_HttpRequest.GetStatusCode());

                PopulateFileInfo(m_FileInfo, m_HttpRequest, m_Filter);
            }
            else
            {
                rlTaskError("Failed to head %s, status code:%d",
                            m_HttpRequest.GetUri(),
                            m_HttpRequest.GetStatusCode());
                this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());

                //If we got a 404, still populate our file info with what we can
                if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_NOT_FOUND)
                {
                    PopulateFileInfo(m_FileInfo, m_HttpRequest, m_Filter);
                }
            }
        }
        else if (!m_MyStatus.Pending())
        {
            rlTaskError("Failed to head %s, status code:%d",
                        m_HttpRequest.GetUri(),
                        m_HttpRequest.GetStatusCode());
            this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
        }
    }

private:

    rlRosHttpFilter m_Filter;
    netHttpRequest m_HttpRequest;
    netStatus m_MyStatus;
    rlCloudFileInfo* m_FileInfo;
};

class rlCloudPostFileTask : public rlTaskBase
{
public:

	RL_TASK_USE_CHANNEL(rline_cloud);
	RL_TASK_DECL(rlCloudPostFileTask);

    rlCloudPostFileTask()
        : m_SrcDevice(NULL)
        , m_SrcHandle(fiHandleInvalid)
    {
    }

    bool Configure(const int localGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const char* filePath,
                    const void* data,
                    const unsigned sizeofData,
                    const rlCloud::PostType postType,
                    rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator)
    {
        bool success = false;
        rtry
        {
            rcheck(m_fiCloudHandle.Post(localGamerIndex,
                                        ns,
                                        onlineSvc,
                                        filePath,
                                        postType,
                                        fileInfo,
                                        allocator),
                    catchall,);

            rcheck((int)sizeofData == m_fiCloudHandle.Write(data, (int)sizeofData),
                    catchall,);

            rcheck(0 == m_fiCloudHandle.Commit(), catchall,);

            success = true;
        }
        rcatchall
        {
            m_fiCloudHandle.Cancel();
        }

        return success;
    }

    bool Configure(const int localGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const char* filePath,
                    const fiDevice* srcDevice,
                    const fiHandle srcHandle,
                    const rlCloud::PostType postType,
                    rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator)
    {
        if(m_fiCloudHandle.Post(localGamerIndex,
                                ns,
                                onlineSvc,
                                filePath,
                                postType,
                                fileInfo,
                                allocator))
        {
            m_SrcDevice = srcDevice;
            m_SrcHandle = srcHandle;
            return true;
        }

        return false;
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel()
    {
        m_fiCloudHandle.Cancel();
    }

    virtual void Update(const unsigned /*timeStepMs*/)
    {
        m_fiCloudHandle.Update();

        if(m_fiCloudHandle.Pending())
        {
            if(m_SrcDevice)
            {
                // Note: This previous code was always reading into m_BounceBuf. As such, it broke
                // if we ever failed to write the entire output to m_fiDevice, since it instead
                // should have been reading into &m_BounceBuf[m_BounceBufLen] in order to not overwrite
                // previously buffered data.
                // As an optimization, if we enforce that assumption anyways, we don't
                // need the bounce buffer and can instead just read into a temporary buffer
                // on the stack, saving some memory. The task will fail if the output
                // device can't buffer the entire response
                u8 buffer[rlCloud::s_FileReadSize];
                const int len = m_SrcDevice->Read(m_SrcHandle, buffer, sizeof(buffer));
                //const int len = m_SrcDevice->Read(m_SrcHandle, m_BounceBuf, sizeof(m_BounceBuf));
                if(len > 0)
                {
                    const int numConsumed =
                        m_fiCloudHandle.Write(buffer, len);
                    if(!rlVerify(numConsumed == len))
                    {
                        rlTaskError("Error writing to Cloud");
                        m_fiCloudHandle.Cancel();
                    }
                }
                else if(0 == len)
                {
                    m_fiCloudHandle.Commit();
                }
                else
                {
                    rlTaskError("Error reading from source stream");
                    m_fiCloudHandle.Cancel();
                }
            }
        }
        else
        {
            if(m_fiCloudHandle.Succeeded())
            {
                rlTaskDebug("Posted %s, status code:%d",
                            m_fiCloudHandle.GetUri(),
                            m_fiCloudHandle.GetStatusCode());

                this->Finish(FINISH_SUCCEEDED, m_fiCloudHandle.GetStatusCode());
            }
            else
            {
                rlTaskError("Failed to post %s, status code:%d",
                            m_fiCloudHandle.GetUri(),
                            m_fiCloudHandle.GetStatusCode());
                this->Finish(FINISH_FAILED, m_fiCloudHandle.GetStatusCode());
            }

            m_fiCloudHandle.Close();
            m_SrcDevice = NULL;
            m_SrcHandle = fiHandleInvalid;
        }
    }

private:

    fiCloudHandle m_fiCloudHandle;
    const fiDevice* m_SrcDevice;
    fiHandle m_SrcHandle;
};

class rlCloudDeleteFileTask : public rlTaskBase
{
public:

	RL_TASK_USE_CHANNEL(rline_cloud);
	RL_TASK_DECL(rlCloudDeleteFileTask);

    rlCloudDeleteFileTask()
        : m_State(STATE_DELETE)
    {
    }

    ~rlCloudDeleteFileTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const char* filePath,
                    sysMemAllocator* allocator)
    {
        bool success = false;
        rtry
        {
            rverify(filePath, catchall,);
            rverify(strlen(filePath) <= MAX_FILEPATH_LENGTH,
                    catchall,
                    rlTaskError("Path %s is too long - max length is %d characters",
                                filePath,
                                MAX_FILEPATH_LENGTH));

            rlCloudMemberId memberId;
            rverify(GetMemberId(localGamerIndex, onlineSvc, &memberId),
                    catchall,
                    rlTaskError("Error validating namespace and online service"));

            rverify(strlen(filePath) <= MAX_FILEPATH_LENGTH,
                    catchall,
                    rlTaskError("Path %s is too long - max length is %d characters",
                                filePath,
                                MAX_FILEPATH_LENGTH));

            atStringBuilder url;
            rverify(rlCloud::CreateUrl(&url, ns, onlineSvc, memberId, filePath, NULL),
                    catchall,
                    rlTaskError("Error building URL"));

            const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);

            rverify(creds.IsValid(),
                    catchall,
                    rlTaskError("Gamer at index:%d doesn't have valid ROS credentials",
                                localGamerIndex));

            m_HttpRequest.Init(allocator?allocator:g_rlAllocator, rlRos::GetRosSslContext());

            rverify(m_Filter.Configure(localGamerIndex, url.ToString()), catchall, rlError("Failed to configure HTTP filter"));

            rverify(m_HttpRequest.BeginDelete(url.ToString(),
                                            NULL,   //proxyAddr
                                            HTTP_REQUEST_TIMEOUT_SECONDS,
                                            NULL,
                                            this->GetTaskName(),
                                            &m_Filter,
                                            &m_MyStatus),
                    catchall,
                    rlTaskError("Error beginning DELETE request"));

            //Add the ROS ticket to the HTTP headers, not the query string,
            //as adding it to the query string makes the URL unique and busts the cache.
            //TODO: We should encrypt our ticket with our session key or something
            m_HttpRequest.AddRequestHeaderValue("Scs-Ticket", creds.GetTicket());

            m_State = STATE_DELETE;

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel()
    {
        //Cancel any subtask we're waiting for.
        rlGetTaskManager()->CancelTask(&m_MyStatus);
        //Cancel any HTTP request we're waiting for.
        m_HttpRequest.Cancel();
    }

    virtual void Update(const unsigned /*timeStepMs*/)
    {
        m_HttpRequest.Update();

        switch(m_State)
        {
        case STATE_DELETE:
            if(this->WasCanceled())
            {
                this->Finish(FINISH_CANCELED);
            }
            else if(m_HttpRequest.Commit())
            {
                m_State = STATE_DELETING;
            }
            else
            {
                rlTaskError("Failed to delete %s", m_HttpRequest.GetUri());
                this->Finish(FINISH_FAILED);
            }
            break;

        case STATE_DELETING:
            if(m_MyStatus.Succeeded())
            {
                if(m_HttpRequest.Succeeded())
                {
                    rlTaskDebug("Deleted %s, status code:%d",
                                m_HttpRequest.GetUri(),
                                m_HttpRequest.GetStatusCode());
                    this->Finish(FINISH_SUCCEEDED, m_HttpRequest.GetStatusCode());
                }
                else
                {
                    rlTaskError("Failed to delete %s, status code:%d",
                                m_HttpRequest.GetUri(),
                                m_HttpRequest.GetStatusCode());
                    this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
                }
            }
            else if(!m_MyStatus.Pending())
            {
                rlTaskError("Failed to delete %s, status code:%d",
                            m_HttpRequest.GetUri(),
                            m_HttpRequest.GetStatusCode());
                this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
            }
            break;
        }
    }

private:

    enum State
    {
        STATE_DELETE,
        STATE_DELETING
    };

    rlRosHttpFilter m_Filter;
    netHttpRequest m_HttpRequest;

    netStatus m_MyStatus;

    State m_State;
};

//////////////////////////////////////////////////////////////////////////
//  rlCloudWatcher
//////////////////////////////////////////////////////////////////////////

rlCloudWatcher::rlCloudWatcher()
: m_IsWatching(false)
{
    this->Clear();
}

//private:

void
rlCloudWatcher::Clear()
{
    if(rlVerify(!m_IsWatching))
    {
        m_LocalGamerIndex = -1;
        m_OnlineSvc = RL_CLOUD_ONLINE_SERVICE_INVALID;
        m_PathSpec[0] = '\0';
		m_clanId = RL_INVALID_CLAN_ID;
    }
}

//////////////////////////////////////////////////////////////////////////
//  rlCloud
//////////////////////////////////////////////////////////////////////////

bool
rlCloud::Init()
{
	if(rlVerify(!s_RlCloudInitialized))
    {
        s_RlCloudInitialized = true;;
    }

	return s_RlCloudInitialized;
}

bool 
rlCloud::IsInitialized()
{
	return s_RlCloudInitialized;
}

void 
rlCloud::Shutdown()
{
	if(s_RlCloudInitialized)
	{
        rlPresence::RemoveDelegate(&sm_PresenceEventDlgt);

        while(!sm_CloudWatcherList.empty())
        {
            rlCloud::UnwatchMemberItem(sm_CloudWatcherList.front());
        }

        s_RlCloudInitialized = false;
	}
}

void 
rlCloud::Update()
{
    if(IsInitialized())
    {
        //Wait until rlPresence is initialized before registering
        //the delegate.
        if(!sm_PresenceEventDlgt.IsRegistered() && rlPresence::IsInitialized())
        {
            rlPresence::AddDelegate(&sm_PresenceEventDlgt);
        }

        for(int i = 0; i < COUNTOF(s_CloudHandles); ++i)
        {
            if(s_CloudHandles[i].m_Status.Pending())
            {
                s_CloudHandles[i].Update();
            }
        }
    }
}

bool
rlCloud::GetMemberFile(const int localGamerIndex,
                        const rlCloudMemberId& targetMemberId,
                        const rlCloudOnlineService onlineSvc,
                        const char* filePath,
                        const rlRosSecurityFlags cloudSecurityFlags,
                        const u64 ifModifiedSince,
                        const netHttpOptions httpOptions,
                        const fiDevice* responseDevice,
                        const fiHandle responseHandle,
                        rlCloudFileInfo* fileInfo,
                        sysMemAllocator* allocator,
                        netStatus* status)
{
    bool success = false;

    rlCloudGetFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

		// check we have read privileges
		rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_READ),
				catchall,
				rlError("Local gamer %d doesn't have cloud read privileges", localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        RL_CLOUD_NAMESPACE_MEMBERS,
                                        onlineSvc,
                                        targetMemberId,
                                        filePath,
                                        cloudSecurityFlags,
                                        ifModifiedSince,
                                        httpOptions,
                                        responseDevice,
                                        responseHandle,
                                        fileInfo,
                                        allocator,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlCloud::PostMemberFile(const int localGamerIndex,
                        const rlCloudOnlineService onlineSvc,
                        const char* dstFilePath,
                        const void* data,
                        const unsigned sizeofData,
                        const PostType postType,
                        rlCloudFileInfo* fileInfo,
                        sysMemAllocator* allocator,
                        netStatus* status)
{
    bool success = false;

    rlCloudPostFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_WRITE),
                catchall,
                rlError("Local gamer %d doesn't have cloud write privileges",
                        localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        RL_CLOUD_NAMESPACE_MEMBERS,
                                        onlineSvc,
                                        dstFilePath,
                                        data,
                                        sizeofData,
                                        postType,
                                        fileInfo,
                                        allocator,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlCloud::PostMemberFile(const int localGamerIndex,
                        const rlCloudOnlineService onlineSvc,
                        const char* dstFilePath,
                        const fiDevice* srcDevice,
                        const fiHandle srcHandle,
                        const PostType postType,
                        rlCloudFileInfo* fileInfo,
                        sysMemAllocator* allocator,
                        netStatus* status)
{
    bool success = false;

    rlCloudPostFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_WRITE),
                catchall,
                rlError("Local gamer %d doesn't have cloud write privileges",
                        localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        RL_CLOUD_NAMESPACE_MEMBERS,
                                        onlineSvc,
                                        dstFilePath,
                                        srcDevice,
                                        srcHandle,
                                        postType,
                                        fileInfo,
                                        allocator,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlCloud::WatchMemberItem(const int localGamerIndex,
                        const rlCloudOnlineService onlineSvc,
                        const char* path,
                        rlCloudWatcher* watcher)
{
    bool success = false;

    rlDebug("Watching member item '%s'", path);

    rtry
    {
        rverify(IsInitialized(), catchall, rlError("Not initialized"));
        rverify(!watcher->m_IsWatching,
                catchall,
                rlError("That watcher is already watching %s", watcher->m_PathSpec));
        rverify(path && strlen(path), catchall, rlError("Invalid path"));
        rverify(strlen(path) < COUNTOF(watcher->m_PathSpec),
                catchall,
                rlError("the path: %s is too long", path));
        rverify(onlineSvc == RL_CLOUD_ONLINE_SERVICE_NATIVE
                || onlineSvc == RL_CLOUD_ONLINE_SERVICE_SC,
                catchall,
                rlError("Invalid online service type: %d", onlineSvc));

        safecpy(watcher->m_PathSpec, path);

        //Change all "\" to "/".
        char* p = watcher->m_PathSpec;
        for(; *p; ++p)
        {
            if('\\' == *p)
            {
                *p = '/';
            }
        }
        watcher->m_LocalGamerIndex = localGamerIndex;
        watcher->m_OnlineSvc = onlineSvc;

        sm_CloudWatcherList.push_back(watcher);
        watcher->m_IsWatching = true;
        return true;
    }
    rcatchall
    {
    }

    return success;
}


bool 
rlCloud::WatchCrewItem( const int localGamerIndex, rlClanId clanId, const rlCloudOnlineService onlineSvc, const char* pathSpec, rlCloudWatcher* watcher )
{
	//Add a normal watcher
	bool success =  false;
	
	rlDebug("Watching crew item '%s' for crew %" I64FMT "d", pathSpec, clanId);

	rtry
	{
		rverify(clanId != RL_INVALID_CLAN_ID, 
				catchall, 
				rlError("Invalid clan passed for crew file watcher"));

		
		rcheck(WatchMemberItem(localGamerIndex, onlineSvc, pathSpec, watcher), 
				catchall, 
				rlError("Failed to intialized wather for crew file watcher"));
		
		watcher->m_clanId = clanId;

		success = true;
		
	}
	rcatchall
	{
	}

	return success;
}


bool
rlCloud::UnwatchMemberItem(rlCloudWatcher* watcher)
{
    if(rlVerifyf(IsInitialized(), "Not initialized")
        && rlVerifyf(watcher->m_IsWatching, "That watcher isn't watching anything"))
    {
        rlDebug("Unwatching member item '%s'", watcher->m_PathSpec);

        sm_CloudWatcherList.erase(watcher);
        watcher->m_IsWatching = false;
        watcher->Clear();
        return true;
    }

    return false;
}

bool rlCloud::UnwatchCrewItem( rlCloudWatcher* watcher )
{
	return UnwatchMemberItem(watcher);
}


bool
rlCloud::DeleteMemberFile(const int localGamerIndex,
                            const rlCloudOnlineService onlineSvc,
                            const char* filePath,
                            sysMemAllocator* allocator,
                            netStatus* status)
{
    bool success = false;

    rlCloudDeleteFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_WRITE),
                catchall,
                rlError("Local gamer %d doesn't have cloud write privileges",
                        localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        RL_CLOUD_NAMESPACE_MEMBERS,
                                        onlineSvc,
                                        filePath,
                                        allocator,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlCloud::GetTitleFile(const char* filePath,
                        const rlRosSecurityFlags cloudSecurityFlags,
                        const u64 ifModifiedSince,
                        const netHttpOptions httpOptions,
                        const fiDevice* responseDevice,
                        const fiHandle responseHandle,
                        rlCloudFileInfo* fileInfo,
                        sysMemAllocator* allocator,
                        netStatus* status)
{
    bool success = false;

    rlCloudGetFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

		// get local gamer index with credentials
		int localGamerIndex;
		rlRos::GetFirstValidCredentials(&localGamerIndex);

		// check we have read privileges
		rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_READ),
				catchall,
				rlError("Local gamer %d doesn't have cloud read privileges", localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

        rverify(rlTaskBase::Configure(task,
                                        -1,
                                        RL_CLOUD_NAMESPACE_TITLES,
                                        RL_CLOUD_ONLINE_SERVICE_INVALID,
                                        INVALID_MEMBER_ID,
                                        filePath,
                                        cloudSecurityFlags,
                                        ifModifiedSince,
                                        httpOptions,
                                        responseDevice,
                                        responseHandle,
                                        fileInfo,
                                        allocator,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}


bool
rlCloud::HeadTitleFile(const char* filePath,
                       const rlRosSecurityFlags securityFlags,
                       rlCloudFileInfo* fileInfo,
                       sysMemAllocator* allocator,
                       netStatus* status)
{
    rlCloudHeadFileTask* task = NULL;

    rtry
    {
        rverify(IsInitialized(), catchall,);

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

        //Title files can be retrieved with any valid local gamer
        int localGamerIndex;
        rlRos::GetFirstValidCredentials(&localGamerIndex);

        rverify(rlTaskBase::Configure(task,
                                      localGamerIndex,
                                      RL_CLOUD_NAMESPACE_TITLES,
                                      RL_CLOUD_ONLINE_SERVICE_INVALID,
                                      INVALID_MEMBER_ID,
                                      filePath,
                                      securityFlags,
                                      fileInfo,
                                      allocator,
                                      status),
                catchall, );

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        return true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        return false;
    }
}


bool
rlCloud::GetGlobalFile(const char* filePath,
                        const rlRosSecurityFlags cloudSecurityFlags,
                        const u64 ifModifiedSince,
                        const netHttpOptions httpOptions,
                        const fiDevice* responseDevice,
                        const fiHandle responseHandle,
                        rlCloudFileInfo* fileInfo,
                        sysMemAllocator* allocator,
                        netStatus* status)
{
    bool success = false;

    rlCloudGetFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

		// get local gamer index with credentials
		int localGamerIndex;
		rlRos::GetFirstValidCredentials(&localGamerIndex);

		// check we have read privileges
		rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_READ),
				catchall,
				rlError("Local gamer %d doesn't have cloud read privileges", localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

        rverify(rlTaskBase::Configure(task,
                                        -1,
                                        RL_CLOUD_NAMESPACE_GLOBAL,
                                        RL_CLOUD_ONLINE_SERVICE_INVALID,
                                        INVALID_MEMBER_ID,
                                        filePath,
                                        cloudSecurityFlags,
                                        ifModifiedSince,
                                        httpOptions,
                                        responseDevice,
                                        responseHandle,
                                        fileInfo,
                                        allocator,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}


bool
rlCloud::GetCrewFile(const int localGamerIndex, 
                    const rlCloudMemberId& targetCrewId, 
                    const rlCloudOnlineService onlineSvc,
                    const char* filePath, 
                    const rlRosSecurityFlags cloudSecurityFlags,
                    const u64 ifModifiedSince,
                    const netHttpOptions httpOptions,
                    const fiDevice* responseDevice, 
                    const fiHandle responseHandle, 
                    rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator,
                    netStatus *status)
{
	bool success = false;

	rlCloudGetFileTask* task = NULL;
	rtry
	{
		rverify(IsInitialized(), catchall,);
		rverify(targetCrewId.IsValid(), catchall,);

		// check we have read privileges
		rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_READ),
				catchall,
				rlError("Local gamer %d doesn't have cloud read privileges", localGamerIndex));

        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

		rverify(rlTaskBase::Configure(task,
			                            localGamerIndex,
			                            RL_CLOUD_NAMESPACE_CREWS,
			                            onlineSvc,
			                            targetCrewId,
			                            filePath,
                                        cloudSecurityFlags,
                                        ifModifiedSince,
                                        httpOptions,
			                            responseDevice,
			                            responseHandle,
                                        fileInfo,
                                        allocator,
			                            status),catchall,);

		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}

bool
rlCloud::GetUgcFile(const int localGamerIndex, 
                    const rlUgcContentType contentType,
                    const char* contentId,
                    const int fileId, 
                    const int fileVersion,
                    const rlScLanguage language,
                    const rlRosSecurityFlags cloudSecurityFlags,
					const u64 ifModifiedSince,
                    const netHttpOptions httpOptions,
					const fiDevice* responseDevice, 
					const fiHandle responseHandle, 
					rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator,
					netStatus *status)
{
	rlCloudGetFileTask* task = NULL;
	rtry
	{
        char cloudAbsPath[RLUGC_MAX_CLOUD_ABS_PATH_CHARS];

		// check we have read privileges
		rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_READ),
				catchall,
				rlError("Local gamer %d doesn't have cloud read privileges", localGamerIndex));

        rverify(rlUgcMetadata::ComposeCloudAbsPath(contentType,
                                                   contentId,
                                                   fileId, 
                                                   fileVersion,
                                                   language,
                                                   cloudAbsPath, 
                                                   sizeof(cloudAbsPath)),
                catchall,);


		rverify(IsInitialized(), catchall,);

		rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

		rverify(rlTaskBase::Configure(task,
                                    localGamerIndex,
                                    cloudAbsPath,
                                    cloudSecurityFlags,
                                    ifModifiedSince,
                                    httpOptions,
                                    responseDevice,
                                    responseHandle,
                                    fileInfo,
                                    allocator,
                                      status), 
                catchall,);

		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        return true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

        return false;
	}
}

void
rlCloud::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

bool
rlCloud::Verify404CloudSignature(const rlRosContentSignature& signature,
                                 const char* absoluteCloudPath)
{
    //A 404 won't have a hash (as opposed to a hash of nothing)
    return VerifyCloudSignature(signature, absoluteCloudPath, NULL, 0);
}

bool
rlCloud::VerifyCloudSignature(const rlRosContentSignature& signature, 
                              const char* absoluteCloudPath, 
                              const u8* hash,
                              const unsigned hashLen)
{
    rtry
    {
        //Normalize our path for consistency with the server's calculation
        char normalizedPath[MAX_FILEPATH_LENGTH + 1];
        rverify(strlen(absoluteCloudPath) <= MAX_FILEPATH_LENGTH,
                catchall,
                rlError("Abs cloud path is too long"));
        safecpy(normalizedPath, absoluteCloudPath);

        char* begin = normalizedPath;
        char* end = &normalizedPath[strlen(normalizedPath)];
        std::replace(begin, end, '\\', '/');
    
        for (char* it = begin; it != end; ++it)
        {
            (*it) = (char)tolower(*it);
        }

        rlDebug1("Verifying signature for: %s", normalizedPath);

        //Cloud signatures use the absolute cloud path as the resource and the RLROS_SIGNATURE_CLOUD signature type
        return rlRosHttpFilter::VerifySignature(signature,
                                                RLROS_SIGNATURE_CLOUD,
                                                (const u8*)normalizedPath,
                                                (unsigned)strlen(normalizedPath),
                                                hash,
                                                hashLen);
    }
    rcatchall
    {
        return false;
    }
}

#if !__FINAL
void 
rlCloud::SetSimulatedFailPct(const char* failRate)
{
	PARAM_nocloudsometimes.Set(failRate);
}
#endif

//private:

void
rlCloud::OnPresenceEvent(const rlPresenceEvent* e)
{
    if(PRESENCE_EVENT_SC_MESSAGE == e->GetId())
    {
        const rlPresenceEventScMessage& scMsg =(const rlPresenceEventScMessage&) *e;
		
		bool bEventReceived = false;
		rlClanId clanId = RL_INVALID_CLAN_ID;
		rlScPresenceMessageCloudFileChanged cfcMsg;

		//Check for a publish message with a cloud changed message in it.
        //This occurs, for example, when a crew file is changed.  All
        //crew members receive a message via the presence pub/sub system.
        //The message contains the rlScPresenceMessageCloudFileChanged
        //notification.

		if (scMsg.m_Message.IsA<rlScPresenceMessagePublish>())
		{
			rlScPresenceMessagePublish pubMsg;
			if (rlVerify(pubMsg.Import(scMsg.m_Message)))
			{
				//The content of the publish message will have the file changed message.
				//Pick off the first name and make sure it's teh file change message
				//NOTE the CheckName is a the raw way you do an IsA check.
				RsonReader fileChangeMsgJson(pubMsg.m_Message);
				if (fileChangeMsgJson.GetFirstMember(&fileChangeMsgJson) 
					&& fileChangeMsgJson.CheckName(rlScPresenceMessageCloudFileChanged::MSG_ID()))
				{
					if(rlVerify(cfcMsg.Import(fileChangeMsgJson)))
					{
						//Parse the channel.
						if(rlVerify(sscanf(pubMsg.m_Channel, "crew.%" I64FMT "d", &clanId) == 1))
						{
							bEventReceived = true;
							rlDebug1("Watcher received PUBLISH (%s) ros.cloud.file.changed for %s", pubMsg.m_Channel, cfcMsg.m_Path);
						}
						else
						{
							rlError("Unhandled channel [%s] for ros.cloud.file.changed", pubMsg.m_Channel);
						}
					}
				}

			}
		}
		else if(scMsg.m_Message.IsA<rlScPresenceMessageCloudFileChanged>())
        {
            if(rlVerify(cfcMsg.Import(scMsg.m_Message)))
            {
				rlDebug1("Watcher received ros.cloud.file.changed for %s", cfcMsg.m_Path);
				bEventReceived = true;
			}
		}
          
		//If we received a valid event, do stuff.
		if (bEventReceived)
		{
            int localGamerIndex = -1;
            rlCloudMemberId memberId;
            rlCloudOnlineService olt = RL_CLOUD_ONLINE_SERVICE_INVALID;

            //Iterate over the watcher list and if any are watching
            //this file call the watcher's callback function.
            CloudWatcherList::const_iterator it = sm_CloudWatcherList.begin();
            CloudWatcherList::const_iterator stop = sm_CloudWatcherList.end();
            for(; stop != it; ++it)
            {
                const rlCloudWatcher* watcher = *it;

                //Make sure the gamer who owns this watcher is online.
                if(!rlPresence::IsOnline(watcher->m_LocalGamerIndex))
                {
                    continue;
                }

                //Check if the full path in the event ends with the the
                //partial path in the watcher.  Case insensitive.
                if(!StringEndsWithI(cfcMsg.m_Path, watcher->m_PathSpec))
                {
                    continue;
                }

				//If we got a clan file notification and this watching isn't for a clan, bail
				bool bThisWatcherIsForClan = watcher->m_clanId != RL_INVALID_CLAN_ID;
				bool bWatchingForClan = clanId != RL_INVALID_CLAN_ID;

				//Move on if this watcher doesn't apply to this event (clan to member or member to clan)
				//or
				//If we're watching for clan, but this isn't the clan we're looking for, move along, move along (-Obi Wan)
				if ((bThisWatcherIsForClan != bWatchingForClan) || 
					(bThisWatcherIsForClan && watcher->m_clanId != clanId))
                {
                    continue;
                }

                //Synthesize the full path of the file we're watching.
                char fullPath[(sizeof(watcher->m_PathSpec) * 3) / 2];
                const char* onlineSvcStr;

                if(RL_CLOUD_ONLINE_SERVICE_SC == watcher->m_OnlineSvc
                    || (RL_CLOUD_ONLINE_SERVICE_NATIVE == watcher->m_OnlineSvc && RLROS_SC_PLATFORM))
                {
                    onlineSvcStr = NATIVE_ONLINE_SERVICE_STRING_SC;
                }
                else
                {
                    onlineSvcStr = NATIVE_ONLINE_SERVICE_STRING;
                }

				if (clanId != RL_INVALID_CLAN_ID)
				{
					rlCloudMemberId crewMemberId(clanId);

					formatf(fullPath, "/crews/%s/%s/%s",
						onlineSvcStr,
						crewMemberId.ToString(),
						watcher->m_PathSpec);
				}
				else//Member
				{
					//Get the gamer's member ID.  If we already have it from the
					//last loop iteration then re-use it.
					if(watcher->m_LocalGamerIndex != localGamerIndex || watcher->m_OnlineSvc != olt)
					{
						if(!GetMemberId(watcher->m_LocalGamerIndex, watcher->m_OnlineSvc, &memberId))
						{
							continue;
						}

						localGamerIndex = watcher->m_LocalGamerIndex;
						olt = watcher->m_OnlineSvc;
					}

                    formatf(fullPath, "/members/%s/%s/%s",
                            onlineSvcStr,
                            memberId.ToString(),
                            watcher->m_PathSpec);
				}

                //If a full match then invoke the delegate.  Case insensitive.
                if(StringEndsWithI(cfcMsg.m_Path, fullPath))
                {
                    rlDebug2("Watched file '%s' changed", watcher->m_PathSpec);
                    watcher->Invoke(watcher->m_PathSpec, fullPath);
                }
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
//  fiCloudHandle
//////////////////////////////////////////////////////////////////////////

RAGE_DEFINE_SUBCHANNEL(rline_cloud, device)
#undef __rage_channel
#define __rage_channel rline_cloud_device

fiCloudHandle::fiCloudHandle()
    : m_State(STATE_CLOSED)
    , m_FileInfo(NULL)
    , m_CloseAfterCommit(false)
{
    m_MultipartBoundary[0] = '\0';
    m_AbsoluteCloudPath[0] = '\0';

#if !__FINAL
    m_SimulateFailPct = 0;
#endif
}

bool
fiCloudHandle::Get(const int inLocalGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const rlCloudMemberId& targetMemberId,
                    const char* path,
                    const rlRosSecurityFlags cloudSecurityFlags,
                    const u64 ifModifiedSince,
                    const netHttpOptions httpOptions,
                    rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator)
{
    bool success = false;

    rlDebug("Reading '%s' file '%s'...",
            GetNamespaceString(ns),
            path);

    rtry
    {
		rverify(STATE_CLOSED == m_State,catchall,);
        rlAssert(!m_CloseAfterCommit);
        rverify(path, catchall,);
        rverify(strlen(path) <= MAX_FILEPATH_LENGTH,
                catchall,
                rlError("Path %s is too long - max length is %d characters",
                        path,
                        MAX_FILEPATH_LENGTH));
        //We don't require a valid gamer index for the "titles" namespace or the "global" namespace
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(inLocalGamerIndex)
                    || RL_CLOUD_NAMESPACE_TITLES == ns || RL_CLOUD_NAMESPACE_GLOBAL == ns,
                catchall,
                rlError("Invalid index for local gamer: %d", inLocalGamerIndex));

        m_CloseAfterCommit = false;

        //Always use valid credentials as they're necessary for secure transmission
        int localGamerIndex = inLocalGamerIndex;
        const rlRosCredentials& cred = RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) ? rlRos::GetCredentials(localGamerIndex) : rlRos::GetFirstValidCredentials(&localGamerIndex);
        rverify(cred.IsValid(),
                catchall,
                rlError("Unable to obtain valid ROS credentials"));

        rlCloudMemberId memberId;

        if(RL_CLOUD_NAMESPACE_MEMBERS == ns)
        {
            if(targetMemberId.IsValid())
            {
                //We're reading someone else's data.
                memberId = targetMemberId;
            }
            else
            {
                rverify(GetMemberId(localGamerIndex, onlineSvc, &memberId),
                        catchall,
                        rlError("Error validating namespace and online service"));
            }
        }
		else if(RL_CLOUD_NAMESPACE_CREWS == ns)
		{
			 rverify(targetMemberId.IsValid(),
					catchall,
					rlError("Invalid crew id"));

			 memberId = targetMemberId;
		}
        else if (RL_CLOUD_NAMESPACE_TITLES == ns)
        {
            rlAssertf(RL_CLOUD_ONLINE_SERVICE_INVALID == onlineSvc,
                        "For the 'titles' namespace pass RL_CLOUD_ONLINE_SERVICE_INVALID");
            rlAssert(!targetMemberId.IsValid());
        }
        else 
        {
            rlAssertf(RL_CLOUD_ONLINE_SERVICE_INVALID == onlineSvc,
                        "For the 'global' namespace pass RL_CLOUD_ONLINE_SERVICE_INVALID");
            rlAssert(!targetMemberId.IsValid());

            rverify(RL_CLOUD_NAMESPACE_GLOBAL == ns,
                    catchall,
                    rlError("Unrecognized namespace %d", ns));
        }

        atStringBuilder url;
        atStringBuilder absCloudPathBuilder(m_AbsoluteCloudPath, sizeof(m_AbsoluteCloudPath));

        if (StringStartsWithI(path, "http://") || StringStartsWithI(path, "https://"))
        {
            rverify(url.Append(path),
                catchall,
                rlError("Error building URL"));
        }
        else
        {
            rverify(rlCloud::CreateUrl(&url, ns, onlineSvc, memberId, path, &absCloudPathBuilder),
                catchall,
                rlError("Error building URL"));
        }

		m_HttpRequest.Init(allocator?allocator:g_rlAllocator, rlRos::GetRosSslContext());
        m_HttpRequest.SetOptions(m_HttpRequest.GetOptions() | static_cast<unsigned>(httpOptions));

        rverify(m_Filter.Configure(localGamerIndex, url.ToString(), cloudSecurityFlags), catchall, rlError("Failed to configure HTTP filter"));

        //Install our delegate that knows how to verify the digital signature if needed
        rlRosHttpFilter::VerifySignatureDelegate dlgt;
        dlgt.Bind(this, &fiCloudHandle::VerifySignature);
        m_Filter.SetVerifySignatureDelegate(dlgt);

        //GET with no response device, we'll poll the response ourselves
        rverify(m_HttpRequest.BeginGet(url.ToString(),
                                        NULL,   //proxyAddr
                                        HTTP_REQUEST_TIMEOUT_SECONDS,
                                        "fiCloudHandle",
                                        &m_Filter,
                                        &m_Status),
                catchall,
                rlError("Error beginning GET request"));

        //Add the ROS ticket to the HTTP headers, not the query string,
        //as adding it to the query string makes the URL unique and busts the cache.
        //TODO: We should encrypt our ticket with our session key or something
        m_HttpRequest.AddRequestHeaderValue("Scs-Ticket", cred.GetTicket());

        //if(ifModifiedSince > 0)
        {
            char http_date[256];
            netHttpRequest::MakeDateString(ifModifiedSince, http_date);
            m_HttpRequest.AddRequestHeaderValue("If-Modified-Since", http_date);
        }

        rverify(m_HttpRequest.Commit(),
                catchall,
                rlError("Failed to retrieve %s", m_HttpRequest.GetUri()));

        m_FileInfo = fileInfo;
        if(m_FileInfo)
        {
            m_FileInfo->Clear();
        }

        m_State = STATE_GETTING;

#if !__FINAL
        if(!PARAM_nocloudsometimes.Get(m_SimulateFailPct))
        {
            m_SimulateFailPct = 0;
        }
#endif

        success = true;
    }
    rcatchall
    {
        m_State = STATE_ERROR;
    }

    return success;
}

bool
fiCloudHandle::Get(const int inLocalGamerIndex,
                   const char* cloudAbsPath,
                   const rlRosSecurityFlags cloudSecurityFlags,
                   const u64 ifModifiedSince,
                   const netHttpOptions httpOptions,
                   rlCloudFileInfo* fileInfo,
                   sysMemAllocator* allocator)
{
    bool success = false;

    rlDebug("Reading file '%s'...", cloudAbsPath);

    rtry
    {
        rverify(STATE_CLOSED == m_State,catchall,);
        rlAssert(!m_CloseAfterCommit);
        rverify(cloudAbsPath, catchall,);
        rverify(strlen(cloudAbsPath) <= MAX_FILEPATH_LENGTH,
                catchall,
                rlError("Path %s is too long - max length is %d characters",
                        cloudAbsPath,
                        MAX_FILEPATH_LENGTH));

        m_CloseAfterCommit = false;

        int localGamerIndex = inLocalGamerIndex;
        const rlRosCredentials& cred = RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) ? rlRos::GetCredentials(localGamerIndex) : rlRos::GetFirstValidCredentials(&localGamerIndex);
        rverify(cred.IsValid(),
                catchall,
                rlError("Unable to obtain valid ROS credentials"));

        atStringBuilder url;
        atStringBuilder absCloudPathBuilder(m_AbsoluteCloudPath, sizeof(m_AbsoluteCloudPath));
        rverify(rlCloud::CreateCompleteUrl(&url, cloudAbsPath, &absCloudPathBuilder), catchall, rlError("Error building URL"));

		m_HttpRequest.Init(allocator?allocator:g_rlAllocator, rlRos::GetRosSslContext());
        m_HttpRequest.SetOptions(m_HttpRequest.GetOptions() | static_cast<unsigned>(httpOptions));

        rverify(m_Filter.Configure(localGamerIndex, url.ToString(), cloudSecurityFlags), catchall, rlError("Failed to configure HTTP filter"));

        //Install our delegate that knows how to verify the digital signature if needed
        rlRosHttpFilter::VerifySignatureDelegate dlgt;
        dlgt.Bind(this, &fiCloudHandle::VerifySignature);
        m_Filter.SetVerifySignatureDelegate(dlgt);

        //GET with no response device, we'll poll the response ourselves
        rverify(m_HttpRequest.BeginGet(url.ToString(),
                                       NULL,   //proxyAddr
                                       HTTP_REQUEST_TIMEOUT_SECONDS,
                                       "fiCloudHandle",
                                       &m_Filter,
                                       &m_Status),
                catchall,
                rlError("Error beginning GET request"));

        //Add the ROS ticket to the HTTP headers, not the query string,
        //as adding it to the query string makes the URL unique and busts the cache.
        //TODO: We should encrypt our ticket with our session key or something
        m_HttpRequest.AddRequestHeaderValue("Scs-Ticket", cred.GetTicket());

        //if(ifModifiedSince > 0)
        {
            char http_date[256];
            netHttpRequest::MakeDateString(ifModifiedSince, http_date);
            m_HttpRequest.AddRequestHeaderValue("If-Modified-Since", http_date);
        }

        rverify(m_HttpRequest.Commit(),
                catchall,
                rlError("Failed to retrieve %s", m_HttpRequest.GetUri()));

        m_FileInfo = fileInfo;
        if(m_FileInfo)
        {
            m_FileInfo->Clear();
        }

        m_State = STATE_GETTING;

#if !__FINAL
        if(!PARAM_nocloudsometimes.Get(m_SimulateFailPct))
        {
            m_SimulateFailPct = 0;
        }
#endif

        success = true;
    }
    rcatchall
    {
        m_State = STATE_ERROR;
    }

    return success;
}

bool
fiCloudHandle::Post(const int localGamerIndex,
                    const rlCloudNamespace ns,
                    const rlCloudOnlineService onlineSvc,
                    const char* path,
                    const rlCloud::PostType postType,
                    rlCloudFileInfo* fileInfo,
                    sysMemAllocator* allocator)
{
    bool success = false;

    rlDebug("Writing '%s' file '%s'...",
            GetNamespaceString(ns),
            path);

    rtry
    {
        rverify(STATE_CLOSED == m_State,catchall,);
        rlAssert(!m_CloseAfterCommit);
        rverify(path, catchall,);
        rverify(strlen(path) <= MAX_FILEPATH_LENGTH,
                catchall,
                rlError("Path %s is too long - max length is %d characters",
                            path,
                            MAX_FILEPATH_LENGTH));

        m_CloseAfterCommit = false;

        rlCloudMemberId memberId;
        rverify(GetMemberId(localGamerIndex, onlineSvc, &memberId),
                catchall,
                rlError("Error validating namespace and online service"));

        //Pass in the full path when creating the URL even though we'll be
        //truncating the file part from the path.  This is so host name
        //remapping will work when CreateUrl() calls rlRos::GetServiceHost().
        atStringBuilder url;
        rverify(rlCloud::CreateUrl(&url, ns, onlineSvc, memberId, path, NULL),
                catchall,
                rlError("Error building URL"));

        //Extract the file name from the path
        char fname[MAX_FILENAME_LENGTH+1] = {""};
        const char* urlStr = url.ToString();
        const char* eop = strrchr(urlStr, '/');
        if(eop)
        {
            safecpy(fname, eop+1);
            url.Truncate((int)(eop-urlStr));
        }

        char buf[MAX_FILEPATH_LENGTH+1];

        const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);

        rverify(creds.IsValid(),
                catchall,
                rlError("Gamer at index:%d doesn't have valid ROS credentials",
                        localGamerIndex));

        if(rlCloud::POST_APPEND == postType)
        {
            rverify(url.AppendOrFail("?append=1"),
                    catchall,
                    rlError("Error building URL"));
        }

        m_HttpRequest.Init(allocator?allocator:g_rlAllocator, rlRos::GetRosSslContext());

        rverify(m_Filter.Configure(localGamerIndex, url.ToString()), catchall, rlError("Failed to configure HTTP filter"));

        rverify(m_HttpRequest.BeginPost(url.ToString(),
                                        NULL,   //proxyAddr
                                        HTTP_REQUEST_TIMEOUT_SECONDS,
                                        NULL,
                                        "fiCloudHandle",
                                        &m_Filter,
                                        &m_Status),
                catchall,
                rlError("Error beginning POST request"));

        //Add the ROS ticket to the HTTP headers, not the query string,
        //as adding it to the query string makes the URL unique and busts the cache.
        //TODO: We should encrypt our ticket with our session key or something
        m_HttpRequest.AddRequestHeaderValue("Scs-Ticket", creds.GetTicket());

        formatf(m_MultipartBoundary, "--------------------%" I64FMT "x", rlGetPosixTime());

        formatf(buf, "multipart/form-data; boundary=%s", m_MultipartBoundary);

        rverify(m_HttpRequest.AddRequestHeaderValue("Content-Type", buf),catchall,);

        rverify(m_HttpRequest.AppendContent("--", 2),catchall,);
        rverify(m_HttpRequest.AppendContent(m_MultipartBoundary, ustrlen(m_MultipartBoundary)),catchall,);
        rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);
		formatf(buf, "Content-Disposition: form-data; name=\"fileField\"; filename=\"%s\"\r\n",
			fname);
        rverify(m_HttpRequest.AppendContent(buf, ustrlen(buf)),catchall,);
        const char* str = "Content-Transfer-Encoding: binary\r\n\r\n";
        rverify(m_HttpRequest.AppendContent(str, ustrlen(str)),catchall,);

        m_FileInfo = fileInfo;
        if(m_FileInfo)
        {
            m_FileInfo->Clear();
        }

        m_State = STATE_POSTING;

#if !__FINAL
		if(!PARAM_nocloudsometimes.Get(m_SimulateFailPct))
		{
			m_SimulateFailPct = 0;
		}
#endif

        success = true;
    }
    rcatchall
    {
        m_State = STATE_ERROR;
    }

    return success;
}

int
fiCloudHandle::Read(void* outBuffer,int bufferSize)
{
	if(STATE_ERROR == m_State)
	{
		return -1;
	}
	else if(STATE_GETTING == m_State)
    {
        if (bufferSize > 0)
        {
            unsigned amountReceived;
            u8 readBuffer[rlCloud::s_FileReadSize];
            m_HttpRequest.ReadBody(readBuffer, Min(bufferSize, COUNTOF(readBuffer)), static_cast<u8*>(outBuffer), bufferSize, &amountReceived);

            return amountReceived;
        }
	}

    return 0;
}

int
fiCloudHandle::Write(const void*buffer, int bufferSize)
{
    if(STATE_POSTING == m_State)
    {
        return m_HttpRequest.AppendContent(buffer, bufferSize) ? bufferSize : -1;
    }

    return -1;
}

int
fiCloudHandle::Commit()
{
    if(rlVerify(Posting()))
    {
        if(STATE_POSTING == m_State)
        {
            m_State = STATE_POST_COMMIT;
        }

        return 0;
    }

    return -1;
}

int
fiCloudHandle::Close()
{
    if(Posting())
    {
        this->Commit();
        m_CloseAfterCommit = true;

        return 0;
    }
    else if(Getting())
    {
        m_HttpRequest.Cancel();
		m_Filter.Clear();
        m_State = STATE_CLOSED;
        return 0;
    }
    else if(STATE_CLOSED != m_State)
    {
        rlAssert(!Pending());
        rlAssert(!m_HttpRequest.Pending());
        m_State = STATE_CLOSED;
		m_HttpRequest.Clear();
		m_Filter.Clear();
        return 0;
    }

    return -1;
}

void
fiCloudHandle::Cancel()
{
    if(Posting() || Getting())
    {
        m_HttpRequest.Cancel();
        m_State = STATE_ERROR;
    }
}

bool
fiCloudHandle::Getting() const
{
    return STATE_GETTING == m_State;
}

bool
fiCloudHandle::Posting() const
{
    return STATE_POSTING == m_State
            || STATE_POST_COMMIT == m_State
            || STATE_POST_COMMITTING == m_State;
}

bool
fiCloudHandle::Pending() const
{
    return Getting() || Posting();
}

bool
fiCloudHandle::Succeeded() const
{
    return fiCloudHandle::STATE_SUCCESS == m_State;
}

const char*
fiCloudHandle::GetUri() const
{
    return m_HttpRequest.GetUri();
}

unsigned
fiCloudHandle::GetStatusCode() const
{
    return m_HttpRequest.GetStatusCode();
}

void
fiCloudHandle::Update()
{
#if !__FINAL
    if(m_SimulateFailPct)
    {
        //Make sure we don't initialize the RNG until the PARAM
        //system is initialized because the user could have specified
        //via command line param a fixed seed for all RNGs.
        if(!s_rlCloudRngInitialized)
        {
            s_rlCloudRng.Reset((int)time(NULL));
            s_rlCloudRngInitialized = true;
        }

        rlDebug("Simulating failure %d%% of the time...", m_SimulateFailPct);
        if(s_rlCloudRng.GetRanged(1, 100) <= m_SimulateFailPct)
        {
            rlDebug("  Failure simulated!");
            this->Cancel();
        }
        else
        {
            rlDebug("  Failure averted!");
        }

        m_SimulateFailPct = 0;
    }
#endif

    if(!Pending())
    {
        return;
    }

    m_HttpRequest.Update();

    switch(m_State)
    {
    case STATE_GETTING:
        if(m_Status.Succeeded())
        {
            if(m_HttpRequest.Succeeded())
            {
                rlDebug("Retrieved %s, status code:%d",
                        m_HttpRequest.GetUri(),
                        m_HttpRequest.GetStatusCode());

                PopulateFileInfo();

                m_State = STATE_SUCCESS;
            }
            else
            {
                rlError("Failed to retrieve %s, status code:%d",
                        m_HttpRequest.GetUri(),
                        m_HttpRequest.GetStatusCode());

                //If we got a 404, still populate our file info with what we can
                if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_NOT_FOUND)
                {
                    PopulateFileInfo();
                }

                m_State = STATE_ERROR;
            }
        }
        else if(!m_Status.Pending())
        {
            rlError("Failed to retrieve %s", m_HttpRequest.GetUri());
            m_State = STATE_ERROR;
        }
        break;
    case STATE_POSTING:
        if(!m_Status.Pending())
        {
            rlError("HTTP request %s, Failed to post %s",
                        m_Status.Canceled() ? "CANCELED" : "FAILED",
                        m_HttpRequest.GetUri());
            m_State = STATE_ERROR;
        }
        break;
    case STATE_POST_COMMIT:
        if(!m_Status.Pending())
        {
            rlError("HTTP request %s, Failed to post %s",
                        m_Status.Canceled() ? "CANCELED" : "FAILED",
                        m_HttpRequest.GetUri());
            m_State = STATE_ERROR;
        }
        else
        {
            char endOfContent[256];
            formatf(endOfContent, "\r\n--%s--\r\n", m_MultipartBoundary);
            if(m_HttpRequest.AppendContent(endOfContent, ustrlen(endOfContent))
                && rlVerify(m_HttpRequest.Commit()))
            {
                m_State = STATE_POST_COMMITTING;
            }
            else
            {
                //Probably out of memory.  We'll try again next frame
                //until we eventually time out.
            }
        }
        break;
    case STATE_POST_COMMITTING:
        if(m_Status.Succeeded())
        {
            if(m_HttpRequest.Succeeded())
            {
                rlDebug("Posted %s", m_HttpRequest.GetUri());
                if(m_CloseAfterCommit)
                {
                    m_CloseAfterCommit = false;
                    m_State = STATE_CLOSED;
                }
                else
                {
                    PopulateFileInfo();

                    m_State = STATE_SUCCESS;
                }
            }
            else
            {
                rlError("Failed to post %s, status code:%d",
                            m_HttpRequest.GetUri(),
                            m_HttpRequest.GetStatusCode());
                m_State = STATE_ERROR;
            }
        }
        else if(!m_Status.Pending())
        {
            rlError("HTTP request %s, Failed to post %s",
                        m_Status.Canceled() ? "CANCELED" : "FAILED",
                        m_HttpRequest.GetUri());
            m_State = STATE_ERROR;
        }
        break;
    case STATE_SUCCESS:
    case STATE_ERROR:
    case STATE_CLOSED:
        break;
    }
}

void
fiCloudHandle::PopulateFileInfo()
{
    //Get the file info (last modified date, content length, etc.);
    rage::PopulateFileInfo(m_FileInfo, m_HttpRequest, m_Filter);
}

bool
fiCloudHandle::VerifySignature(const rlRosContentSignature& signature, const u8* hash, const unsigned hashLen)
{
    return rlCloud::VerifyCloudSignature(signature, m_AbsoluteCloudPath, hash, hashLen);
}

//////////////////////////////////////////////////////////////////////////
//  fiDeviceCloud
//////////////////////////////////////////////////////////////////////////

static fiCloudHandle* AllocCloudHandle()
{
    for(int i = 0; i < COUNTOF(s_CloudHandles); ++i)
    {
        if(fiCloudHandle::STATE_CLOSED == s_CloudHandles[i].m_State)
        {
            return &s_CloudHandles[i];
        }
    }

    return NULL;
}

static bool IsValidCloudHandle(const fiCloudHandle* h)
{
    return h >= &s_CloudHandles[0] && h < &s_CloudHandles[COUNTOF(s_CloudHandles)];
}

void
fiDeviceCloud::SetAccessParameters(const int localGamerIndex,
						 const rlCloudNamespace ns,
						 const rlCloudOnlineService onlineSvc,
						 const rlCloudMemberId& memberId,
                         rlCloudFileInfo* fileInfo) 
{
    SetAccessParameters(localGamerIndex,
                        ns,
                        onlineSvc,
                        NULL,
                        memberId,
                        fileInfo);
}

void
fiDeviceCloud::SetAccessParameters(const int localGamerIndex,
						         const rlCloudNamespace ns,
						         const rlCloudOnlineService onlineSvc,
                                 sysMemAllocator* allocator,
						         const rlCloudMemberId& memberId,
                                 rlCloudFileInfo* fileInfo) 
{
	m_localGamerIndex = localGamerIndex;
	m_ns = ns;
	m_onlineSvc = onlineSvc;
	m_memberId = memberId;
	m_FileInfo = fileInfo;
    m_Allocator = allocator;
}

/*fiHandle
fiDeviceCloud::GetFile(const char* path) const
{
    fiCloudHandle* cloudHandle = NULL;
    
    if(rlVerifyf(rlCloud::IsInitialized(), "rlCloud is not initialized"))
    {
        cloudHandle = AllocCloudHandle();

        if(cloudHandle)
        {
            if(cloudHandle->Get(m_localGamerIndex,
                                m_ns,
                                m_onlineSvc,
                                m_memberId,
                                path))
            {
                return (fiHandle) cloudHandle;
            }                            
        }
        else
        {
            rlWarning("fiCloudHandle pool is exhausted");
        }
    }

    return fiHandleInvalid;
}

fiHandle
fiDeviceCloud::CreateFile(const char* path) const
{
    rlDebug("Creating member file '%s'...", path);

    fiCloudHandle* cloudHandle = NULL;
    
    if(rlVerifyf(rlCloud::IsInitialized(), "rlCloud is not initialized"))
    {
        cloudHandle = AllocCloudHandle();

        if(cloudHandle)
        {
            if(cloudHandle->Post(m_localGamerIndex,
                                m_ns,
                                m_onlineSvc,
                                path,
                                rlCloud::POST_REPLACE,
                                m_location,
                                m_sizeofLocation))
            {
                return (fiHandle) cloudHandle;
            }
        }
        else
        {
            rlWarning("fiCloudHandle pool is exhausted");
        }
    }

    return fiHandleInvalid;
}

fiHandle
fiDeviceCloud::AppendFile(const char* path) const
{
    rlDebug("Appending to member file '%s'...", path);

    fiCloudHandle* cloudHandle = NULL;
    
    if(rlVerifyf(rlCloud::IsInitialized(), "rlCloud is not initialized"))
    {
        cloudHandle = AllocCloudHandle();

        if(cloudHandle)
        {
            if(cloudHandle->Post(m_localGamerIndex,
                                m_ns,
                                m_onlineSvc,
                                path,
                                rlCloud::POST_APPEND,
                                m_location,
                                m_sizeofLocation))
            {
                return (fiHandle) cloudHandle;
            }
        }
        else
        {
            rlWarning("fiCloudHandle pool is exhausted");
        }
    }

    return fiHandleInvalid;
}*/

int
fiDeviceCloud::Commit(fiHandle handle) const
{
	fiCloudHandle* cloudHandle = (fiCloudHandle*) handle;
	if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
	{
		return cloudHandle->Commit();
	}

	return false;
}

bool
fiDeviceCloud::Pending(fiHandle handle) const
{
    const fiCloudHandle* cloudHandle = (const fiCloudHandle*) handle;
    if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
    {
        return cloudHandle->Pending();
    }

    return false;
}

bool
fiDeviceCloud::Succeeded(fiHandle handle) const
{
    const fiCloudHandle* cloudHandle = (const fiCloudHandle*) handle;
    if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
    {
        return cloudHandle->Succeeded();
    }

    return false;
}

bool
fiDeviceCloud::IsValid(fiHandle handle) const
{
	fiCloudHandle* cloudHandle = (fiCloudHandle*) handle;
	if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
	{
		switch((int)cloudHandle->m_HttpRequest.GetHttpVerb())
		{
		case NET_HTTP_VERB_GET:
			return cloudHandle->m_HttpRequest.GetResponseContentLength() == static_cast<unsigned>(Size(handle));
		case NET_HTTP_VERB_POST:
			return true; 
		}
	}

	return false;
}

unsigned
fiDeviceCloud::GetContentLength(fiHandle handle) const
{
	fiCloudHandle* cloudHandle = (fiCloudHandle*) handle;
	if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
	{
		switch((int)cloudHandle->m_HttpRequest.GetHttpVerb())
		{
		case NET_HTTP_VERB_GET:
			return cloudHandle->m_HttpRequest.GetResponseContentLength();
		case NET_HTTP_VERB_POST:
			return cloudHandle->m_HttpRequest.GetOutContentBytesSent();
		}
	}

	return 0;
}

fiHandle
fiDeviceCloud::Open(const char* filePath, bool readOnly) const
{
	fiCloudHandle* cloudHandle = NULL;
	rlDebug("Opening member file '%s'...", filePath);

	if(rlVerifyf(rlCloud::IsInitialized(), "rlCloud is not initialized"))
	{
		cloudHandle = AllocCloudHandle();

		if(cloudHandle)
		{
			if(readOnly)
			{
				if(cloudHandle->Get(m_localGamerIndex,
					                m_ns,
					                m_onlineSvc,
					                m_memberId,
					                filePath,
                                    RLROS_SECURITY_DEFAULT,
                                    0,
                                    NET_HTTP_OPTIONS_NONE,
				                    m_FileInfo,
                                    m_Allocator))
				{
					return (fiHandle) cloudHandle;
				}                            
			}
			else
			{
				if(cloudHandle->Post(m_localGamerIndex,
										m_ns,
										m_onlineSvc,
										filePath,
										rlCloud::POST_APPEND,
										m_FileInfo,
                                        m_Allocator))
				{
					return (fiHandle) cloudHandle;
				}
			}
		}
		else
		{
			rlWarning("fiCloudHandle pool is exhausted");
		}
	}

    return fiHandleInvalid;
}


fiHandle
fiDeviceCloud::OpenBulk(const char *filename,u64& /*outBias*/) const
{
    return this->Open(filename, true);
}


int
fiDeviceCloud::ReadBulk(fiHandle /*handle*/,u64 /*offset*/,void* /*outBuffer*/,int /*bufferSize*/) const
{
	return 0;
}


fiHandle
fiDeviceCloud::Create(const char* filename) const
{
	rlDebug("Creating member file '%s'...", filename);

	fiCloudHandle* cloudHandle = NULL;

	if(rlVerifyf(rlCloud::IsInitialized(), "rlCloud is not initialized"))
	{
		cloudHandle = AllocCloudHandle();

		if(cloudHandle)
		{
			if(cloudHandle->Post(m_localGamerIndex,
									m_ns,
									m_onlineSvc,
									filename,
									rlCloud::POST_REPLACE,
									m_FileInfo,
                                    m_Allocator))
			{
				return (fiHandle) cloudHandle;
			}
		}
		else
		{
			rlWarning("fiCloudHandle pool is exhausted");
		}
	}

    return fiHandleInvalid;
}

int
fiDeviceCloud::Read(fiHandle _handle,void* outBuffer,int bufferSize) const
{
    fiCloudHandle* cloudHandle = (fiCloudHandle*) _handle;
    return cloudHandle->Read(outBuffer, bufferSize);
}

int
fiDeviceCloud::Write(fiHandle _handle,const void*buffer,int bufferSize) const
{
    fiCloudHandle* cloudHandle = (fiCloudHandle*) _handle;
    if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
    {
        return cloudHandle->Write(buffer, bufferSize);
    }

    return -1;
}

int
fiDeviceCloud::Close(fiHandle _handle) const
{
    fiCloudHandle* cloudHandle = (fiCloudHandle*) _handle;
    if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
    {
        return cloudHandle->Close();
    }

    return -1;
}

int
fiDeviceCloud::CloseBulk(fiHandle _handle) const
{
    return this->Close(_handle);
}

int
fiDeviceCloud::Size(fiHandle handle) const
{
    return (int) this->Size64(handle);
}

u64
fiDeviceCloud::Size64(fiHandle handle) const
{
	fiCloudHandle* cloudHandle = (fiCloudHandle*) handle;
	
	if(rlVerifyf(IsValidCloudHandle(cloudHandle), "Invalid fiCloudHandle"))
    {
        switch((int)cloudHandle->m_HttpRequest.GetHttpVerb())
        {
        case NET_HTTP_VERB_GET:
            //For reads, file size isn't supported!
            rlAssert(false);
            return 0;
        case NET_HTTP_VERB_POST:
            return cloudHandle->m_HttpRequest.GetOutContentBytesSent();
        }
    }

    return 0;
}

int
fiDeviceCloud::Seek(fiHandle /*handle*/, int /*offset*/,fiSeekWhence /*whence*/) const
{
    //Cloud does not support seeking!
	rlAssert(false);
	return -1;
}

u64
fiDeviceCloud::Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const
{
	return Seek(handle,(int)offset,whence);
}

u64
fiDeviceCloud::GetFileTime(const char *) const
{
	return 0;
}

bool
fiDeviceCloud::SetFileTime(const char *,u64) const
{
	return false;
}

u64
fiDeviceCloud::GetFileSize(const char* /*filename*/) const
{
    return 0;
}

u32
fiDeviceCloud::GetAttributes(const char*) const
{
	return FILE_ATTRIBUTE_INVALID;
}

const char*
fiDeviceCloud::GetDebugName() const
{
    return "fiDeviceCloud";
}

static fiDeviceCloud s_DeviceCloud;

fiDeviceCloud& fiDeviceCloud::GetInstance()
{
	return s_DeviceCloud;
}

}
