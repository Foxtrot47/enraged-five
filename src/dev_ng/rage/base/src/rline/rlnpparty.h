// 
// rline/rlNpParty.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#ifndef RLINE_RLNPPARTY_H
#define RLINE_RLNPPARTY_H

#include "atl/array.h"
#include "rlgamerinfo.h"
#include "system/criticalsection.h"

#include <np.h>
#include <np/np_party.h>

namespace rage
{

struct rlNpPartyMemberInfo
{
	rlGamerHandle m_GamerHandle;
	SceNpPartyMemberInfo m_SceNpPartyMemberInfo;
	SceNpPartyMemberVoiceState m_VoiceState;
	bool m_IsInSession;

	bool IsTalking()
	{
		return m_VoiceState == SCE_NP_PARTY_MEMBER_VOICE_STATE_TALKING;
	}
};


class rlNpParty
{
public:
	rlNpParty();

    static void InitLibs();

	bool Init();
	void Update();
	void Shutdown();

 	//NP Party Event Handlers
	static void PartyRoomEventHandler(SceNpPartyRoomEventType eventType,const void* data,	void* userdata);
	static void PartyVoiceEventHandler(const SceNpPartyMemberVoiceInfo* memberVoiceInfo,void* userdata);
	static void PartyBinaryMessageEventHandler(SceNpPartyBinaryMessageEvent msgEvent,const void* data, void* userdata);

	// PURPOSE
	//	Sends game invites to all members of the system party
	//  Returns true if the game invites sent correctly.
	//  NOTE: Not implemented
	bool SendGameInvites();

	// PURPOSE
	//	Returns true if any local user is in a system party
	bool IsInParty();

	// PURPOSE
	//	Returns the number of party members, and fills the members array
	int GetPartyMembers(rlNpPartyMemberInfo* members);

	// PURPOSE
	//	Returns the size of the system party
	int GetPartySize() { return m_PartyMembers.GetCount(); }

	// PURPOSE
	//	Shows the party invitation list dialog
	bool ShowInvitationList(const int localGamerIndex);

	// PURPOSE
	//	Creates a platform party for the user at the given local index
	bool CreateParty(const int localGamerIndex);

private:
	void UpdatePartyMemberVoiceState(SceNpPartyRoomMemberId memberId, SceNpPartyMemberVoiceState state);
	int RetrieveAllPartyInfo();
	int SetPartyMemberList(SceNpPartyMemberList *pPartyMemberList);
	void AddPartyMemberToMemberList(SceNpPartyMemberInfo* pInfo);
	void RemovePartyMemberFromMemberList(const SceNpPartyRoomMemberId MemberId);
	int ClearPartyMemberList();

	bool					m_bInitialized;
	atFixedArray<rlNpPartyMemberInfo, SCE_NP_PARTY_MEMBER_NUM_MAX> m_PartyMembers;
	SceNpPartyState			m_PartyState;

	sysCriticalSectionToken m_PartyInfoCs;

}; // rlNPParty

} // namespace rage

#endif // RLINE_RLNPPARTY_H
#endif // RSG_ORBIS