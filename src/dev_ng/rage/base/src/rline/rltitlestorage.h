// 
// rline/rltitlestorage.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLTITLESTORAGE_H
#define RLTITLESTORAGE_H

#include "rlgamerinfo.h"
#include "rlworker.h"
#include "file/file_config.h"
#include "system/criticalsection.h"

#if __USE_TUS

namespace rage
{

class netStatus;

class rlTitleStorage
{
public:
    rlTitleStorage();
    ~rlTitleStorage();

    bool Init(const int cpuAffinity);
    void Shutdown();

    //PURPOSE
    //  Begins the download of a file in title storage.  The caller should
    //  monitor the status variable to know when the download has finished.
    //PARAMS
    //  gamerInfo       - Info for local gamer doing download
    //  filename        - Name of file
    //  buf             - Buffer to hold downloaded file
    //  bufSize         - Size of buffer
    //  timeoutMs       - Max time to wait before giving up on download
    //  downloadSize    - Actual size of downloaded file
    //  status          - Status variable to monitor success/failure of download
    //RETURNS
    //  True if download was successfully started, false otherwise.
    //NOTES
    //  Filename is ignored on NP (but must be supplied), as the NP backend 
    //  can only store a single file.
    //
    //  If the file could not be found, status will report success but 
    //  downloadSize will be zero.
    //
    bool DownloadFile(const rlGamerInfo& gamerInfo,
                      const char* filename,              
                      char* buf,
                      const unsigned bufSize,
                      const unsigned timeoutMs,
                      unsigned *downloadSize,
                      netStatus* status);

    //PURPOSE
    //  Returns true if a download is in progress.
    bool Pending() const;

private:    
    struct WorkerInfo
    {
        WorkerInfo();

        void Clear();

        rlGamerInfo m_GamerInfo;
        const char* m_Filename;
        char* m_Buf;
        unsigned m_BufSize;
        unsigned m_TimeoutMs;
        unsigned *m_DownloadSize;
        netStatus* m_Status;
    };

    WorkerInfo m_WorkerInfo;

    struct Worker : public rlWorker
    {
        Worker();

        bool Start(rlTitleStorage* ts, const int cpuAffinity);
        bool Stop();

    private:
        //Make Start() un-callable
        bool Start(const char* /*name*/,
                   const unsigned /*stackSize*/,
                   const int /*cpuAffinity*/)
        {
            return rlVerify(false);
        }

        virtual void Perform();
        bool NativePerform();

        rlTitleStorage* m_Ts;
    };

    Worker m_Worker;

    sysCriticalSectionToken m_Cs;

    bool m_Initialized : 1;
};

}   //namespace rage

#endif //__USE_TUS

#endif //RLTITLESTORAGE_H
