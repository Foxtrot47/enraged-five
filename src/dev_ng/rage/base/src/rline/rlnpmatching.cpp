// 
// rline/rlnpmatching.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

//TODO: Test this with busy/unavailable servers
 
#if RSG_NP

#include "rlnpmatching.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/task.h"
#include "rlgamerinfo.h"
#include "rlnp.h"
#include "rlnptitleid.h"
#include "rlsessionfinder.h"
#include "rltask.h"

#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

#include <np.h>
#include <time.h>

#if RSG_ORBIS
#include <sdk_version.h>
# if SCE_ORBIS_SDK_VERSION >= 0x00920020u
#include <libsysmodule.h>
#endif
#pragma comment(lib,"libSceNpMatching2_stub_weak.a")
#endif

#ifdef __SNC__
#pragma diag_suppress 552
#endif

namespace rage
{

static const u64 RL_NP_INVALID_ROOM_ID      = u64(-1);

static const SceNpMatching2ServerId RL_NP_INVALID_SERVER_ID = SceNpMatching2ServerId(-1);

static const SceNpMatching2WorldId RL_NP_INVALID_WORLD_ID   = SceNpMatching2WorldId(-1);

CompileTimeAssert(RL_NP_MAX_ROOM_MEMBERS == SCE_NP_MATCHING2_ROOM_MAX_SLOT);

RAGE_DEFINE_SUBCHANNEL(rline_np, matching)
#undef __rage_channel
#define __rage_channel rline_np_matching

CompileTimeAssert(RL_MAX_MATCHING_ATTRS == SCE_NP_MATCHING2_ROOM_SEARCHABLE_INT_ATTR_EXTERNAL_NUM);

CompileTimeAssert(RL_MAX_GAMERS_PER_SESSION <= SCE_NP_MATCHING2_ROOM_MAX_SLOT);

static SceNpMatching2SessionPassword RLNP_ROOM_PASSWORD = {{"FONZKUL"}};

static void MakeGameModeAndType(const unsigned gameMode,
                                const rlGameType gameType,
                                u16* gameModeAndType);

static void ExtractGameModeAndType(unsigned* gameMode,
                                rlGameType* gameType,
                                const u16 gameModeAndType);

//Determines how we handle nil matching attributes when creating/finding
//rooms.  Nil attributes are prohibited when creating a room, but permitted
//when finding rooms.
enum NilAttrHandling
{
    NIL_ATTRS_PROHIBITED,
    NIL_ATTRS_PERMITTED
};

//------------------------------------------------------------------------------
//Static functions
//------------------------------------------------------------------------------

//Populate NP integer and binary matching attributes.
static int PopulateMatchingAttrs(const rlMatchingAttributes& attrs,
                                SceNpMatching2IntAttr (&intAttrs)[RL_MAX_MATCHING_ATTRS],
                                SceNpMatching2BinAttr* binAttr,
                                u8 (&binAttrData)[SCE_NP_MATCHING2_ROOM_SEARCHABLE_BIN_ATTR_EXTERNAL_MAX_SIZE],
                                const NilAttrHandling nilAttrHandling);

#if __ASSERT
/*static void ShowQueueStats(const unsigned ctxId)
{
    SceNpMatching2CbQueueInfo qi = {0};
    int err = sceNpMatching2GetCbQueueInfo(ctxId, &qi);
    if(!rlVerify(err >= 0))
    {
        rlNpError("Error calling sceNpMatching2CreateJoinRoom", err);
    }
    else
    {
        rlDebug3("Request queue         cap:%d cur:%d max:%d",
                qi.requestCbQueueLen,
                qi.curRequestCbQueueLen,
                qi.maxRequestCbQueueLen);

        rlDebug3("Session event queue   cap:%d cur:%d max:%d",
                qi.sessionEventCbQueueLen,
                qi.curSessionEventCbQueueLen,
                qi.maxSessionEventCbQueueLen);

        rlDebug3("Session message queue cap:%d cur:%d max:%d",
                qi.sessionMsgCbQueueLen,
                qi.curSessionMsgCbQueueLen,
                qi.maxSessionMsgCbQueueLen);
    }
}*/

static void CheckMemInfo()
{
    SceNpMatching2MemoryInfo memInfo;

    if(0 == sceNpMatching2GetMemoryInfo(&memInfo))
    {
        const float memFracUsed =
            ((float) memInfo.curMemUsage) / memInfo.totalMemSize;

        rlAssertf(memFracUsed < .9f,
                "NP Matching is using too much memory (%" SIZETFMT "dKb/%" SIZETFMT "dKb)",
                (memInfo.curMemUsage/1024),
                (memInfo.totalMemSize/1024));
    }
}
#endif  //__ASSERT

#if RSG_ORBIS
// We need to make copies of structures that Orbis sends to our callbacks because they
// go out of scope as soon as the callback function returns.
// Only the info we actually use gets copied. If you need other data, then add code to copy it here.

class rlNpMatching2GetWorldInfoListResponse : public rlNpMatching2EventData
{
public:
	rlNpMatching2GetWorldInfoListResponse(const SceNpMatching2GetWorldInfoListResponse& e)
	{
		m_WorldNum = e.worldNum;
		m_WorldId = e.world->worldId;
	}

	uint32_t m_WorldNum;
	SceNpMatching2WorldId m_WorldId;
};

class rlNpMatching2SearchRoomResponse : public rlNpMatching2EventData
{
	void Copy(const SceNpMatching2RoomDataExternal* srcRoomData, SceNpMatching2RoomDataExternal* dstRoomData)
	{
		*dstRoomData = *srcRoomData;

		if(srcRoomData->owner)
		{
			dstRoomData->owner = rage_new SceNpId;
			*(dstRoomData->owner) = *(srcRoomData->owner);
		}

		dstRoomData->roomGroup = NULL;

		//Note: uncomment this if you need room group info
		//if(srcRoomData->roomGroupNum > 0)
		//{
		//	dstRoomData->roomGroup = rage_new SceNpMatching2RoomGroupInfo[dstRoomData->roomGroupNum];
		//	for(uint32_t i = 0; i < dstRoomData->roomGroupNum; ++i)
		//	{
		//		*(dstRoomData->roomGroup[i]) = *(srcRoomData->roomGroup[i]);
		//	}
		//}

		if(srcRoomData->roomSearchableIntAttrExternalNum > 0)
		{
			dstRoomData->roomSearchableIntAttrExternal = rage_new SceNpMatching2IntAttr[srcRoomData->roomSearchableIntAttrExternalNum];
			for(uint32_t i = 0; i < srcRoomData->roomSearchableIntAttrExternalNum; ++i)
			{
				dstRoomData->roomSearchableIntAttrExternal[i] = srcRoomData->roomSearchableIntAttrExternal[i];
			}
		}

		if(srcRoomData->roomSearchableBinAttrExternalNum > 0)
		{
			dstRoomData->roomSearchableBinAttrExternal = rage_new SceNpMatching2BinAttr[srcRoomData->roomSearchableBinAttrExternalNum];
			for(uint32_t i = 0; i < srcRoomData->roomSearchableBinAttrExternalNum; ++i)
			{
				dstRoomData->roomSearchableBinAttrExternal[i] = srcRoomData->roomSearchableBinAttrExternal[i];
				if(srcRoomData->roomSearchableBinAttrExternal[i].ptr)
				{
					dstRoomData->roomSearchableBinAttrExternal[i].ptr = rage_new u8[srcRoomData->roomSearchableBinAttrExternal[i].size];
					memcpy((void*)dstRoomData->roomSearchableBinAttrExternal[i].ptr, srcRoomData->roomSearchableBinAttrExternal[i].ptr, srcRoomData->roomSearchableBinAttrExternal[i].size);
				}
			}
		}

		if(srcRoomData->roomBinAttrExternalNum > 0)
		{
			dstRoomData->roomBinAttrExternal = rage_new SceNpMatching2BinAttr[srcRoomData->roomBinAttrExternalNum];
			for(uint32_t i = 0; i < srcRoomData->roomBinAttrExternalNum; ++i)
			{
				dstRoomData->roomBinAttrExternal[i] = srcRoomData->roomBinAttrExternal[i];
				if(srcRoomData->roomBinAttrExternal[i].ptr)
				{
					dstRoomData->roomBinAttrExternal[i].ptr = rage_new u8[srcRoomData->roomBinAttrExternal[i].size];
					memcpy((void*)dstRoomData->roomBinAttrExternal[i].ptr, srcRoomData->roomBinAttrExternal[i].ptr, srcRoomData->roomBinAttrExternal[i].size);
				}
			}
		}
	}

	template<typename T>
	T* CopyLinkedList(const T* head)
	{
		T* copy = NULL;

		if(head)
		{
			copy = rage_new T;
			T* dst = copy;
			const T* src = head;
			Copy(src, dst);

			for(src = src->next; src; dst = dst->next, src = src->next)
			{
				dst->next = rage_new T;
				Copy(src, dst->next);
			}
			dst->next = NULL;
		}

		return copy;
	}

public:

	rlNpMatching2SearchRoomResponse(const SceNpMatching2SearchRoomResponse& e)
	{
		m_Resp.range = e.range;
		m_Resp.roomDataExternal = CopyLinkedList(e.roomDataExternal);
	}

	~rlNpMatching2SearchRoomResponse()
	{
		SceNpMatching2RoomDataExternal* roomData = m_Resp.roomDataExternal;

		while(roomData) 
		{
			if(roomData->owner)
			{
				delete roomData->owner;
				roomData->owner = NULL;
			}

			if(roomData->roomGroup)
			{
				delete [] roomData->roomGroup;
				roomData->roomGroup = NULL;
			}

			if(roomData->roomSearchableIntAttrExternal)
			{
				delete [] roomData->roomSearchableIntAttrExternal;
				roomData->roomSearchableIntAttrExternal = NULL;
			}

			if(roomData->roomSearchableBinAttrExternal)
			{
				for(uint32_t i = 0; i < roomData->roomSearchableBinAttrExternalNum; ++i)
				{
					if(roomData->roomSearchableBinAttrExternal[i].ptr)
					{
						delete [] ((u8*)(roomData->roomSearchableBinAttrExternal[i].ptr));
						roomData->roomSearchableBinAttrExternal[i].ptr = NULL;
					}
				}

				delete [] roomData->roomSearchableBinAttrExternal;
				roomData->roomSearchableBinAttrExternal = NULL;
			}

			if(roomData->roomBinAttrExternal)
			{
				for(uint32_t i = 0; i < roomData->roomBinAttrExternalNum; ++i)
				{
					if(roomData->roomSearchableBinAttrExternal[i].ptr)
					{
						delete [] ((u8*)(roomData->roomBinAttrExternal[i].ptr));
						roomData->roomBinAttrExternal[i].ptr = NULL;
					}
				}

				delete [] roomData->roomBinAttrExternal;
				roomData->roomBinAttrExternal = NULL;
			}

			SceNpMatching2RoomDataExternal* next = roomData->next; 
			delete roomData;
			roomData = next;
		}
	}

	SceNpMatching2SearchRoomResponse m_Resp;
};

class rlNpMatching2CreateJoinRoomResponse : public rlNpMatching2EventData
{
public:
	rlNpMatching2CreateJoinRoomResponse(const SceNpMatching2CreateJoinRoomResponse& e)
	{
		m_RoomId = e.roomDataInternal->roomId;

	}

	SceNpMatching2RoomId m_RoomId;
};

class rlNpMatching2JoinRoomResponse : public rlNpMatching2EventData
{
public:
	rlNpMatching2JoinRoomResponse(const SceNpMatching2JoinRoomResponse& e)
	{
		m_Members.m_NumMembers = e.memberList.membersNum;

		const SceNpMatching2RoomMemberDataInternal* member = e.memberList.members;

		for(int i = 0; i < (int) m_Members.m_NumMembers; ++i)
		{
			m_Members.m_Members[i].Reset(member->npId);
			member = member->next;
		}
	}

	rlNpRoomMembers m_Members;
};

class rlNpMatching2RoomMemberUpdateInfo : public rlNpMatching2EventData
{
public:
	rlNpMatching2RoomMemberUpdateInfo(const SceNpMatching2RoomMemberUpdateInfo& e)
	{
		m_NpId = e.roomMemberDataInternal->npId;
	}

	SceNpId m_NpId;
};

rlNpMatching2EventData* 
rlNpMatching::NpEvent::CopySceEventData(const unsigned eventId,
										const void* eventData)
{	
	void* ret = NULL;

	if(eventData == NULL)
	{
		return NULL;
	}

	switch(eventId)
	{
		case SCE_NP_MATCHING2_REQUEST_EVENT_GET_WORLD_INFO_LIST:
		{
			const SceNpMatching2GetWorldInfoListResponse* resp =
			(SceNpMatching2GetWorldInfoListResponse*) eventData;

			ret = rage_new rlNpMatching2GetWorldInfoListResponse(*resp);
			rlAssert(ret);
		}
		break;
		case SCE_NP_MATCHING2_REQUEST_EVENT_SEARCH_ROOM:
		{
			const SceNpMatching2SearchRoomResponse* resp =
                (SceNpMatching2SearchRoomResponse*) eventData;

			ret = rage_new rlNpMatching2SearchRoomResponse(*resp);
			rlAssert(ret);
		}
		break;
		case SCE_NP_MATCHING2_REQUEST_EVENT_CREATE_JOIN_ROOM:
		{
			const SceNpMatching2CreateJoinRoomResponse* resp =
			(SceNpMatching2CreateJoinRoomResponse*) eventData;

			ret = rage_new rlNpMatching2CreateJoinRoomResponse(*resp);
			rlAssert(ret);
		}
		break;
		case SCE_NP_MATCHING2_REQUEST_EVENT_JOIN_ROOM:
		{
			const SceNpMatching2JoinRoomResponse* resp =
            (SceNpMatching2JoinRoomResponse*) eventData;

			ret = rage_new rlNpMatching2JoinRoomResponse(*resp);
			rlAssert(ret);
		}
		break;
		case SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_LEFT:
		case SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_JOINED:
		{
			const SceNpMatching2RoomMemberUpdateInfo* info =
			(SceNpMatching2RoomMemberUpdateInfo*) eventData;

			ret = rage_new rlNpMatching2RoomMemberUpdateInfo(*info);
			rlAssert(ret);
		}
		break;
	}

	return (rlNpMatching2EventData*)ret;
}
#endif

rlNpMatching::NpEvent*
rlNpMatching::NpEvent::CreateNpEvent(const unsigned ctxId,
                                    const unsigned eventId,
#if RSG_PS3
                                    const unsigned eventKey,
                                    const unsigned sizeofEventData,
                                    const int errorCode
#elif RSG_ORBIS
									const int errorCode,
									const void* data
#endif
									)
{
    NpEvent* npe = NULL;

    rtry
    {
#if RSG_PS3
        //If -1u == eventKey then the size of event data should be zero.
        rlAssert(-1u != eventKey || sizeofEventData == 0);
        const size_t totalSize = sizeofEventData + sizeof(NpEvent);
        npe = (NpEvent*) RL_ALLOCATE(rlNpMatching, totalSize);
#elif RSG_ORBIS
        const size_t totalSize = sizeof(NpEvent);
        npe = (NpEvent*) RL_ALLOCATE(rlNpMatching, totalSize);
#endif
        rverify(npe,
                catchall,
                rlError("Error allocating NpEvent"));

        new (npe) NpEvent();

        npe->m_CtxId = ctxId;
        npe->m_EventId = eventId;

#if RSG_PS3
        npe->m_ErrorCode = errorCode;
        npe->m_SizeofEventData = sizeofEventData;

		if(sizeofEventData > 0)
        {
            npe->m_EventData = &npe[1];

            int err = sceNpMatching2GetEventData(ctxId, eventKey, npe->m_EventData, sizeofEventData);

            rcheck(err >= 0,
                    catchall,
                    rlNpError("sceNpMatching2GetEventData failed", err));
        }
#elif RSG_ORBIS
		npe->m_ErrorCode = errorCode;
		npe->m_EventData = CopySceEventData(eventId, data);
#endif
    }
    rcatchall
    {
        if(npe)
        {
            npe->~NpEvent();
            RL_FREE(npe);
            npe = NULL;
        }
    }

#if RSG_PS3
    if(sizeofEventData > 0)
    {
        //Clear the data.
        int err = sceNpMatching2ClearEventData(ctxId, eventKey);

        if(err < 0
            //Ignore errors indicating that we already consumed the event data.
            && (int) SCE_NP_MATCHING2_ERROR_EVENT_DATA_NOT_FOUND != err)
        {
            rlNpError("sceNpMatching2ClearEventData failed", err);
        }
    }
#endif

    return npe;
}

void
rlNpMatching::NpEvent::DestroyEvent(NpEvent* npe)
{
    if(npe)
    {
        npe->~NpEvent();
        RL_FREE(npe);
    }
}

//Populate a session search result from a room's matching attributes.
void
rlNpMatching::PopulateSearchResult(rlSessionInfo* result,
                                    const SceNpMatching2RoomDataExternal* roomData,
                                    const rlMatchingFilter& filter)
{
    rlAssert(filter.IsValid());

    rlMatchingAttributes attrs;
    unsigned numAttrs;

    const unsigned* attrIds =
        filter.GetSessionAttrFieldIds(&numAttrs);

    rlAssert(numAttrs == roomData->roomSearchableIntAttrExternalNum);

    if(numAttrs > 0)
    {
        attrs.Reset(attrIds, numAttrs);
    }

    const SceNpMatching2IntAttr* intAttrs =
        roomData->roomSearchableIntAttrExternal;

    const SceNpMatching2BinAttr* binAttrs =
        roomData->roomSearchableBinAttrExternal;

    unsigned gameMode;
    rlGameType gameType;

    ExtractGameModeAndType(&gameMode,
                            &gameType,
                            *(const u16*) binAttrs[0].ptr);

    attrs.SetGameMode(gameMode);
    attrs.SetGameType(gameType);

    for(int j = 0; j < (int) numAttrs; ++j)
    {
        attrs.SetValueByIndex(j, intAttrs[j].num);
    }

    //Determine the number of pub/priv slots
    SceNpMatching2RoomPasswordSlotMask pwSlotMask =
        roomData->passwordSlotMask;

    unsigned maxPubSlots = roomData->maxSlot;
    unsigned maxPrivSlots = 0;

    while(pwSlotMask)
    {
        if(pwSlotMask & 0x01)
        {
            rlAssert(maxPubSlots > 0);
            --maxPubSlots;
            ++maxPrivSlots;
        }

        pwSlotMask >>= 1;
    }

    rlPeerAddress hostPeerAddr;
    rlNpRoomInfo roomInfo;

    hostPeerAddr.SetNpId(NP_SWITCH(roomData->owner->npId,*roomData->owner));
    roomInfo.InitPublic((u64) roomData->roomId);

    result->Init(roomInfo, hostPeerAddr);
}

//Called with matching 2 context events.
void
rlNpMatching::NpMatching2ContextCallback(SceNpMatching2ContextId ctxId,
                                        SceNpMatching2Event event,
                                        SceNpMatching2EventCause eventCause,
                                        int errorCode,
                                        void* arg)
{
    rlNpMatching* npm = (rlNpMatching*) arg;
    rlAssert(npm);
    rlAssert(npm->m_Initialized);

    bool queueIt = false;

#define M2_EVENT_CASE(x) case x: rlDebug("context event("#x")"); queueIt = true; break;

    switch(event)
    {
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_CONTEXT_EVENT_StartOver,SCE_NP_MATCHING2_CONTEXT_EVENT_START_OVER));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_CONTEXT_EVENT_Start,SCE_NP_MATCHING2_CONTEXT_EVENT_STARTED));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_CONTEXT_EVENT_Stop,SCE_NP_MATCHING2_CONTEXT_EVENT_STOPPED));
        default:
            rlError("Unknown NP matching2 event:0x%08x", event);
            break;
    }

#undef M2_EVENT_CASE

    if(queueIt)
    {
        NpEvent* npe = NpEvent::CreateContextEvent(ctxId, event, eventCause, errorCode);
        if(!rlVerify(npe) || !rlVerify(npm->QueueEvent(npe)))
        {
            NpEvent::DestroyEvent(npe);
        }
    }
}

//Called with matching 2 room events.
void
rlNpMatching::NpMatching2RoomEventCallback(SceNpMatching2ContextId ctxId,
                                        SceNpMatching2RoomId roomId,
                                        SceNpMatching2Event event,
#if RSG_PS3
                                        SceNpMatching2EventKey eventKey,
                                        int errorCode,
                                        size_t sizeofEventData,
#elif RSG_ORBIS
										const void* data,
#endif
										void* arg)
{
    rlNpMatching* npm = (rlNpMatching*) arg;
    rlAssert(npm);
    rlAssert(npm->m_Initialized);

#define M2_EVENT_CASE(x) case x: rlDebug("room event("#x")"); break;

    switch(event)
    {
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_MemberJoined,SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_JOINED));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_MemberLeft,SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_LEFT));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_Kickedout,SCE_NP_MATCHING2_ROOM_EVENT_KICKEDOUT));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_RoomDestroyed,SCE_NP_MATCHING2_ROOM_EVENT_ROOM_DESTROYED));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_RoomOwnerChanged,SCE_NP_MATCHING2_ROOM_EVENT_ROOM_OWNER_CHANGED));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_UpdatedRoomDataInternal,SCE_NP_MATCHING2_ROOM_EVENT_UPDATED_ROOM_DATA_INTERNAL));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_UpdatedRoomMemberDataInternal,SCE_NP_MATCHING2_ROOM_EVENT_UPDATED_ROOM_MEMBER_DATA_INTERNAL));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_UpdatedSignalingOptParam,SCE_NP_MATCHING2_ROOM_EVENT_UPDATED_SIGNALING_OPT_PARAM));
        default:
            rlError("Unknown room event:0x%08x", event);
            break;
    }

#undef M2_EVENT_CASE

#if RSG_PS3
	NpEvent* npe = NpEvent::CreateRoomEvent(ctxId, event, eventKey, sizeofEventData, roomId, errorCode);
#elif RSG_ORBIS
	NpEvent* npe = NpEvent::CreateRoomEvent(ctxId, event, roomId, 0, data);
#endif

    if(!rlVerify(npe) || !rlVerify(npm->QueueEvent(npe)))
    {
        NpEvent::DestroyEvent(npe);
    }
}

//Called when room messages are received.
void
rlNpMatching::NpMatching2RoomMessageCallback(SceNpMatching2ContextId ctxId,
                                        SceNpMatching2RoomId roomId,
                                        SceNpMatching2RoomMemberId srcMemberId,
                                        SceNpMatching2Event event,
#if RSG_PS3
                                        SceNpMatching2EventKey eventKey,
                                        int errorCode,
                                        size_t sizeofEventData,
#elif RSG_ORBIS
										const void* data,
#endif
                                        void* arg)
{
    rlNpMatching* npm = (rlNpMatching*) arg;
    rlAssert(npm);
    rlAssert(npm->m_Initialized);

#define M2_EVENT_CASE(x) case x: rlDebug("room message("#x")"); break;

    switch(event)
    {
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_MSG_EVENT_ChatMessage,SCE_NP_MATCHING2_ROOM_MSG_EVENT_CHAT_MESSAGE));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_ROOM_MSG_EVENT_Message,SCE_NP_MATCHING2_ROOM_MSG_EVENT_MESSAGE));
        default:
            rlError("Unknown room message:0x%08x", event);
            break;
    }

#undef M2_EVENT_CASE

#if RSG_PS3
    NpEvent* npe = NpEvent::CreateRoomMessage(ctxId, event, eventKey, sizeofEventData, roomId, srcMemberId, errorCode);
#elif RSG_ORBIS
    NpEvent* npe = NpEvent::CreateRoomMessage(ctxId, event, roomId, srcMemberId, 0, data);
#endif

    if(!rlVerify(npe) || !rlVerify(npm->QueueEvent(npe)))
    {
        NpEvent::DestroyEvent(npe);
    }
}

//Called with matching 2 lobby events.
void
rlNpMatching::NpMatching2LobbyEventCallback(SceNpMatching2ContextId ctxId,
                                        SceNpMatching2LobbyId lobbyId,
                                        SceNpMatching2Event event,
#if RSG_PS3
                                        SceNpMatching2EventKey eventKey,
                                        int errorCode,
                                        size_t sizeofEventData,
#elif RSG_ORBIS
										const void* data,
#endif
										void* arg)
{
    rlNpMatching* npm = (rlNpMatching*) arg;
    rlAssert(npm);
    rlAssert(npm->m_Initialized);

#define M2_EVENT_CASE(x) case x: rlDebug("lobby event("#x")"); break;

    switch(event)
    {
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_LOBBY_EVENT_MemberJoined,SCE_NP_MATCHING2_LOBBY_EVENT_MEMBER_JOINED));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_LOBBY_EVENT_MemberLeft,SCE_NP_MATCHING2_LOBBY_EVENT_MEMBER_LEFT));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_LOBBY_EVENT_LobbyDestroyed,SCE_NP_MATCHING2_LOBBY_EVENT_LOBBY_DESTROYED));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_LOBBY_EVENT_UpdatedLobbyMemberDataInternal,SCE_NP_MATCHING2_LOBBY_EVENT_UPDATED_LOBBY_MEMBER_DATA_INTERNAL));
        default:
            rlError("Unknown lobby event:0x%08x", event);
            break;
    }

#undef M2_EVENT_CASE

#if RSG_PS3
	NpEvent* npe = NpEvent::CreateLobbyEvent(ctxId, event, eventKey, sizeofEventData, lobbyId, errorCode);
#elif RSG_ORBIS
	NpEvent* npe = NpEvent::CreateLobbyEvent(ctxId, event, lobbyId, 0, data);
#endif

    if(!rlVerify(npe) || !rlVerify(npm->QueueEvent(npe)))
    {
        NpEvent::DestroyEvent(npe);
    }
}

//Called lobby messages are received.
void
rlNpMatching::NpMatching2LobbyMessageCallback(SceNpMatching2ContextId ctxId,
                                        SceNpMatching2LobbyId lobbyId,
                                        SceNpMatching2LobbyMemberId srcMemberId,
                                        SceNpMatching2Event event,
#if RSG_PS3
                                        SceNpMatching2EventKey eventKey,
                                        int errorCode,
                                        size_t sizeofEventData,
#elif RSG_ORBIS
										const void* data,
#endif
										void* arg)
{
    rlNpMatching* npm = (rlNpMatching*) arg;
    rlAssert(npm);
    rlAssert(npm->m_Initialized);

#define M2_EVENT_CASE(x) case x: rlDebug("lobby message("#x")"); break;

    switch(event)
    {
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_LOBBY_MSG_EVENT_ChatMessage,SCE_NP_MATCHING2_LOBBY_MSG_EVENT_CHAT_MESSAGE));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_LOBBY_MSG_EVENT_Invitation,SCE_NP_MATCHING2_LOBBY_MSG_EVENT_INVITATION));
        default:
            rlError("Unknown lobby message:0x%08x", event);
            break;
    }

#undef M2_EVENT_CASE

#if RSG_PS3
    NpEvent* npe = NpEvent::CreateLobbyMessage(ctxId, event, eventKey, sizeofEventData, lobbyId, srcMemberId, errorCode);
#elif RSG_ORBIS
	NpEvent* npe = NpEvent::CreateLobbyMessage(ctxId, event, lobbyId, srcMemberId, 0, data);
#endif
	
    if(!rlVerify(npe) || !rlVerify(npm->QueueEvent(npe)))
    {
        NpEvent::DestroyEvent(npe);
    }
}

//Called when signaling events are received.
void
rlNpMatching::NpMatching2SignalingCallback(SceNpMatching2ContextId ctxId,
                                            SceNpMatching2RoomId roomId,
                                            SceNpMatching2RoomMemberId peerMemberId,
                                            SceNpMatching2Event event,
                                            int errorCode,
                                            void* arg)
{
    rlNpMatching* npm = (rlNpMatching*) arg;
    rlAssert(npm);
    rlAssert(npm->m_Initialized);

#define M2_EVENT_CASE(x) case x: rlDebug("signaling event("#x") for peer %d", peerMemberId); break;

    switch(event)
    {
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_SIGNALING_EVENT_Dead, SCE_NP_MATCHING2_SIGNALING_EVENT_DEAD));
        M2_EVENT_CASE(NP_SWITCH(SCE_NP_MATCHING2_SIGNALING_EVENT_Established, SCE_NP_MATCHING2_SIGNALING_EVENT_ESTABLISHED));
        default:
            rlError("Unknown signaling event:0x%08x", event);
            break;
    }

#undef M2_EVENT_CASE

    NpEvent* npe = NpEvent::CreateSignalingEvent(ctxId, event, roomId, peerMemberId, errorCode);
    if(!rlVerify(npe) || !rlVerify(npm->QueueEvent(npe)))
    {
        NpEvent::DestroyEvent(npe);
    }
}

//Timeout value for NP matching 2 requests.
static const unsigned RL_NP_REQUEST_TIMEOUT_USEC = 10 * 1000 * 1000;    //10 seconds

//////////////////////////////////////////////////////////////////////////
//  rlNpMatchingTask
//////////////////////////////////////////////////////////////////////////
class rlNpMatchingTask : public rlTaskBase
{
    friend class rlNpMatching;

public:

    RL_TASK_USE_CHANNEL(rline_np_matching);

protected:

    rlNpMatchingTask()
        : m_ReqId(-1u)
    {
    }

    ~rlNpMatchingTask()
    {
        rlAssert(-1u == m_ReqId);
        rlAssert(!m_ReqStatus.Pending());
    }

    bool Configure()
    {
        rlAssert(-1u == m_ReqId);

        return true;
    }

    virtual void DoCancel()
    {
        if(-1u != m_ReqId)
        {
            rlAssert(m_ReqStatus.Pending());
			// TODO: NS - there doesn't seem to be a way to abort a request on Orbis
#if RSG_PS3
            sceNpMatching2AbortRequest(g_rlNp.GetMatching().GetMatchingContextId(),
                                        m_ReqId);
#endif
            m_ReqId = -1u;
            m_ReqStatus.SetCanceled();
        }
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlAssert(-1u == m_ReqId);
        rlAssert(!m_ReqStatus.Pending());

        this->rlTaskBase::Finish(finishType, resultCode);
    }

    virtual bool HandleNpEvent(rlNpMatching::NpEvent* ASSERT_ONLY(npe))
    {
        rlAssert(0 == npe->m_SizeofEventData);
        return true;
    }

    //Called back from NP system with responses to our requests.
    static void RequestCallback(SceNpMatching2ContextId ctxId,
                                SceNpMatching2RequestId reqId,
                                SceNpMatching2Event eventId,
#if RSG_PS3
                                SceNpMatching2EventKey eventKey,
#endif
                                int errorCode,
#if RSG_PS3
                                size_t sizeofEventData,
#elif RSG_ORBIS
								const void* data,
#endif
                                void* arg)
    {
        //Queue *all* events, even if we don't recognize them, because
        //they might have associated data that needs to be cleared from
        //the internal NP event queue.  That will be done in ProcessEvents().

        // const unsigned taskId = (const unsigned) arg;
		const unsigned taskId = (const unsigned)((const uintptr_t) arg);

		rlNpMatching::NpEvent* npe =
#if RSG_PS3
            rlNpMatching::NpEvent::CreateMatching2ResponseEvent(ctxId,
                                                                eventId,
                                                                eventKey,
                                                                sizeofEventData,
                                                                reqId,
                                                                taskId,
                                                                errorCode);
#elif RSG_ORBIS
            rlNpMatching::NpEvent::CreateMatching2ResponseEvent(ctxId,
                                                                eventId,
                                                                reqId,
                                                                taskId,
																errorCode,
                                                                data);
#endif

        if(!rlVerify(npe)
            || !rlVerify(g_rlNp.GetMatching().QueueEvent(npe)))
        {
            rlNpMatching::NpEvent::DestroyEvent(npe);
        }

#define M2_HANDLE_EVENT(x)\
    case x: rlDebug("received request callback event("#x")"); break;

        switch(eventId)
        {
#if RSG_PS3
        M2_HANDLE_EVENT(SCE_NP_MATCHING2_REQUEST_EVENT_GetServerInfo);
#endif
        M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetWorldInfoList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_WORLD_INFO_LIST));
        M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SearchRoom, SCE_NP_MATCHING2_REQUEST_EVENT_SEARCH_ROOM));
        M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_CreateJoinRoom, SCE_NP_MATCHING2_REQUEST_EVENT_CREATE_JOIN_ROOM));
        M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_JoinRoom, SCE_NP_MATCHING2_REQUEST_EVENT_JOIN_ROOM));
        M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_LeaveRoom, SCE_NP_MATCHING2_REQUEST_EVENT_LEAVE_ROOM));
        M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetRoomDataExternalList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_ROOM_DATA_EXTERNAL_LIST));
        M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SetRoomDataExternal, SCE_NP_MATCHING2_REQUEST_EVENT_SET_ROOM_DATA_EXTERNAL));
		M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SetRoomDataInternal, SCE_NP_MATCHING2_REQUEST_EVENT_SET_ROOM_DATA_INTERNAL));
		M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetRoomMemberDataExternalList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_ROOM_MEMBER_DATA_EXTERNAL_LIST));
        default:
            rlDebug("Unhandled event:0x%08x", eventId);
            break;
        }
#undef M2_HANDLE_EVENT
    }

    u32 m_ReqId;    //Id of the currently pending NP request.
    netStatus m_ReqStatus;
};

//////////////////////////////////////////////////////////////////////////
//  NpGetRandomWorldIdTask
//////////////////////////////////////////////////////////////////////////

class NpGetRandomWorldIdTask : public rlNpMatchingTask
{
public:
    RL_TASK_DECL(NpGetRandomWorldIdTask);

    NpGetRandomWorldIdTask()
        : m_State(STATE_GET_SERVER_ID_LIST)
        , m_ServerIds(NULL)
        , m_NumServers(0)
        , m_NumServersToCheck(0)
        , m_CurServerId(RL_NP_INVALID_SERVER_ID)
        , m_WorldId(RL_NP_INVALID_WORLD_ID)
        , m_WorldIdPtr(NULL)
        , m_ServerAvailable(false)
    {
    }

    ~NpGetRandomWorldIdTask()
    {
        rlAssert(!m_ServerIds);

        if(m_ServerIds)
        {
            RL_DELETE_ARRAY(m_ServerIds, m_NumServers);
        }
    }

    bool Configure(SceNpMatching2WorldId* worldIdPtr)
    {
        rlAssert(!m_ServerIds);
        rlAssert(!m_NumServers);
        rlAssert(!m_NumServersToCheck);
        rlAssert(RL_NP_INVALID_SERVER_ID == m_CurServerId);
        rlAssert(RL_NP_INVALID_WORLD_ID == m_WorldId);
        rlAssert(!m_WorldIdPtr);

        m_WorldIdPtr = worldIdPtr;
        *worldIdPtr = -1u;

        return true;
    }

    void Start()
    {
        rlTaskDebug("Retrieving random NP world id...");

        rlAssert(STATE_GET_SERVER_ID_LIST == m_State);

        this->rlNpMatchingTask::Start();

        if(!rlVerify(g_rlNp.GetMatching().Started()))
        {
            rlTaskError("NP matching context not started");
            this->Finish(FINISH_FAILED);
        }
    }

    virtual void DoCancel()
    {
        this->rlNpMatchingTask::DoCancel();

        m_WorldIdPtr = NULL;
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlTaskDebug("Retrieving random NP world id %s", FinishString(finishType));

        if(!WasCanceled())
        {
            if(FinishSucceeded(finishType))
            {
                rlTaskDebug("World id:0x%08x", m_WorldId);
                rlAssert(-1u != m_WorldId);
                *m_WorldIdPtr = m_WorldId;
            }
            else
            {
                *m_WorldIdPtr = -1u;
            }
        }

        if(m_ServerIds)
        {
            RL_DELETE_ARRAY(m_ServerIds, m_NumServers);
        }

        m_ServerIds = NULL;
        m_NumServers = m_NumServersToCheck = 0;
        m_WorldIdPtr = NULL;

        this->rlNpMatchingTask::Finish(finishType, resultCode);
    }

    void Update(const unsigned timeStep)
    {
        this->rlNpMatchingTask::Update(timeStep);

		if(WasCanceled())
		{
            this->Finish(FINISH_CANCELED);
            return;
		}

        switch(m_State)
        {
        case STATE_GET_SERVER_ID_LIST:
            //Retrieve the local cache of server ids.
            rlTaskDebug("Retrieving server id list");
            if(!this->GetServerIdList())
            {
                rlTaskDebug("Failed to get server id list");
                this->Finish(FINISH_FAILED);
            }
            else if(0 == m_NumServers)
            {
                rlTaskDebug("No matching servers");
                this->Finish(FINISH_FAILED);
            }
            else
            {
                rlTaskDebug("Checking %d matching servers for availability",
                            m_NumServers);

                rlAssert(m_NumServersToCheck == m_NumServers);

                m_State = STATE_REQUEST_SERVER_INFO;
            }
            break;

        case STATE_REQUEST_SERVER_INFO:
            if(m_NumServersToCheck > 0)
            {
                int svrIdx = -1;

#if !__FINAL
                svrIdx = g_rlNp.GetMatching().GetMatchingServerIndex();
                if(svrIdx >= (int) m_NumServersToCheck)
                {
                    svrIdx = int(m_NumServersToCheck - 1);
                }

                if(svrIdx >= 0)
                {
                    rlTaskDebug("Forcing use of matching server at index:%d", svrIdx);
                }
#endif
                if(svrIdx < 0)
                {
                    static netRandom s_Rng(time(NULL));

                    //Choose a random server
                    svrIdx = s_Rng.GetRanged(0, m_NumServersToCheck - 1);
                }

                m_CurServerId = m_ServerIds[svrIdx];
                --m_NumServersToCheck;
                m_ServerIds[svrIdx] = m_ServerIds[m_NumServersToCheck];

                rlTaskDebug("Requesting server availability for server:0x%08x",
                            m_CurServerId);

                m_ServerAvailable = false;
                if(this->RequestServerInfo())
                {
                    m_State = STATE_REQUESTING_SERVER_INFO;
                }
                else
                {
                    rlTaskDebug("Failed to request server availability");
                    m_State = STATE_REQUEST_SERVER_INFO;
                }
            }
            else
            {
                rlTaskDebug("None of the %d servers are available",
                            m_NumServers);

                this->Finish(FINISH_FAILED);
            }
            break;

        case STATE_REQUESTING_SERVER_INFO:
            if(m_ReqStatus.Succeeded() && m_ServerAvailable)
            {
                m_State = STATE_REQUEST_WORLD_INFO;
            }
            else if(!m_ReqStatus.Pending())
            {
                m_State = STATE_REQUEST_SERVER_INFO;
            }
            break;

        case STATE_REQUEST_WORLD_INFO:
            rlTaskDebug("Requesting world info for server:0x%08x",
                        m_CurServerId);

            m_WorldId = -1u;
            if(this->RequestWorldInfo())
            {
                m_State = STATE_REQUESTING_WORLD_INFO;
            }
            else
            {
                rlTaskDebug("Failed to request world info");
                m_State = STATE_REQUEST_SERVER_INFO;
            }
            break;

        case STATE_REQUESTING_WORLD_INFO:
            if(m_ReqStatus.Succeeded() && rlVerify(-1u != m_WorldId))
            {
                this->Finish(FINISH_SUCCEEDED);
            }
            else if(!m_ReqStatus.Pending())
            {
                m_State = STATE_REQUEST_SERVER_INFO;
            }
            break;
		case STATE_NEXT_SERVER:
			break;
        }
    }

    private:

    virtual bool HandleNpEvent(rlNpMatching::NpEvent* npEvent)
    {
        switch((int)m_State)
        {
        case STATE_REQUESTING_SERVER_INFO:
            rlAssert(m_ReqStatus.Pending());
            return this->HandleServerInfoResponse(npEvent);
        case STATE_REQUESTING_WORLD_INFO:
            rlAssert(m_ReqStatus.Pending());
            return this->HandleWorldInfoResponse(npEvent);
        default:
            rlAssert(false);
        }

        return false;
    }

#if RSG_ORBIS
	// Orbis SDK 0.91 doesn't have APIs to support multiple servers
	// but the docs say it's possible to have more than one server per game,
	// so they may add these APIs later. This function mimics the PS3 API.
	int sceNpMatching2GetServerIdListLocal(SceNpMatching2ContextId ctxId,
										   SceNpMatching2ServerId *serverId,
										   uint32_t serverIdNum)
	{
		int ret = 1;

		rtry
		{
			// if serverId = NULL, we still want to return the number of servers
			rcheck(serverId != NULL, catchall, );

			rverify(serverIdNum > 0, catchall, );

			SceNpMatching2ServerId theServerId;
			ret = sceNpMatching2GetServerId(ctxId, &theServerId);
			if(ret == 0)
			{
				serverId[0] = theServerId;
				ret = 1;
			}
		}
		rcatchall
		{

		}

		return ret;
	}
#endif

    //Retrieve the local cache of server ids.
    bool GetServerIdList()
    {
        rlAssert(!m_ServerIds);
        rlAssert(0 == m_NumServers);

        bool success = false;

        rtry
        {
            const int numServers =
                sceNpMatching2GetServerIdListLocal(g_rlNp.GetMatching().GetMatchingContextId(), NULL, 0);

            rcheck(numServers >= 0,
                    catchall,
                    rlNpError("Error calling sceNpMatching2GetServerIdListLocal",
                                numServers));

            rcheck(numServers > 0,
                    catchall,
                    rlTaskDebug("No servers available"));

            m_NumServers = numServers;

            m_ServerIds = RL_NEW_ARRAY(rlNpMatching, SceNpMatching2ServerId, m_NumServers);

            rverify(m_ServerIds,
                    catchall,
                    rlTaskError("Error allocating server ids array"));

            const int err =
                sceNpMatching2GetServerIdListLocal(g_rlNp.GetMatching().GetMatchingContextId(),
                                                    m_ServerIds,
                                                    m_NumServers);

            rcheck(err == m_NumServers,
                    catchall,
                    rlNpError("Error calling sceNpMatching2GetServerIdListLocal", err));

            success = true;
        }
        rcatchall
        {
            if(m_ServerIds)
            {
                RL_DELETE_ARRAY(m_ServerIds, m_NumServers);
                m_ServerIds = NULL;
                m_NumServers = 0;
            }
        }

        m_NumServersToCheck = m_NumServers;

        return success;
    }

    //Request information about a server.
    bool RequestServerInfo()
    {
        bool success = false;

        rlAssert(-1u == m_ReqId);

        m_ReqStatus.SetPending();

#if RSG_PS3
        SceNpMatching2RequestOptParam optParam = {0};
        optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
        optParam.cbFuncArg = (void*) this->GetTaskId();
        optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

        SceNpMatching2GetServerInfoRequest serverInfoRqst = {0};

        serverInfoRqst.serverId = m_CurServerId;
        rlAssert(serverInfoRqst.serverId != SceNpMatching2ServerId(-1u));

        int err = sceNpMatching2GetServerInfo(g_rlNp.GetMatching().GetMatchingContextId(),
                                            &serverInfoRqst,
                                            &optParam,
                                            &m_ReqId);

        if(err >= 0)
        {
            success = true;
        }
        else
        {
            rlNpError("Error calling sceNpMatching2GetServerInfo", err);
            m_ReqStatus.SetFailed();
        }
#elif RSG_ORBIS
		// TODO: NS - Orbis SDK v0.91 doesn't seem to provide an API to get the server's status
		success = true;
		m_ReqStatus.SetSucceeded();
		m_ServerAvailable = true;
#endif

        return success;
    }

    //Request information about a server.
    bool RequestWorldInfo()
    {
        bool success = false;

        rlAssert(-1u == m_ReqId);

        m_ReqStatus.SetPending();

        SceNpMatching2GetWorldInfoListRequest worldListRqst = {0};
        worldListRqst.serverId = m_CurServerId;

        SceNpMatching2RequestOptParam optParam = {0};
        optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
        optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
        optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

        int err = sceNpMatching2GetWorldInfoList(g_rlNp.GetMatching().GetMatchingContextId(),
                                                &worldListRqst,
                                                &optParam,
                                                &m_ReqId);

        if(err >= 0)
        {
            success = true;
        }
        else
        {
            rlNpError("Error calling sceNpMatching2GetWorldInfoList", err);
            m_ReqStatus.SetFailed();
        }

        return success;
    }

    //Check the response to the server info request and return true
    //if the server is available.
    bool HandleServerInfoResponse(rlNpMatching::NpEvent* npe)
    {
        bool success = false;

#if RSG_PS3
		// TODO: NS - Orbis SDK v0.91 doesn't seem to provide an API to get the server's status

        m_ServerAvailable = false;

        rtry
        {
            rverify(npe,catchall,);

            rcheck(npe->m_ErrorCode >= 0,
                    catchall,
                    rlNpError("GetServerInfo", npe->m_ErrorCode));

            const SceNpMatching2GetServerInfoResponse* resp
                = (SceNpMatching2GetServerInfoResponse*)npe->m_EventData;

            rcheck(SCE_NP_MATCHING2_SERVER_STATUS_UNAVAILABLE != resp->server.status,
                    catchall,
                    rlTaskDebug("Matching server:0x%08x is unavailable",
                                resp->server.serverId));

            rcheck(SCE_NP_MATCHING2_SERVER_STATUS_BUSY != resp->server.status,
                    catchall,
                    rlTaskDebug("Matching server:0x%08x is busy",
                                resp->server.serverId));

            rcheck(SCE_NP_MATCHING2_SERVER_STATUS_MAINTENANCE != resp->server.status,
                    catchall,
                    rlTaskDebug("Matching server:0x%08x is under maintenance",
                                resp->server.serverId));

            rcheck(SCE_NP_MATCHING2_SERVER_STATUS_AVAILABLE == resp->server.status,
                    catchall,
                    rlTaskError("Unknown server status:%d", resp->server.status));

            rlTaskDebug("Matching server:0x%08x is available",
                        resp->server.serverId);

            success = m_ServerAvailable = true;
        }
        rcatchall
        {
        }
#endif

        return success;
    }

    //Request information about a server.
    bool HandleWorldInfoResponse(rlNpMatching::NpEvent* npe)
    {
        bool success = false;

        rlAssert(RL_NP_INVALID_WORLD_ID == m_WorldId);

        rtry
        {
            rverify(npe,catchall,);

            rcheck(npe->m_ErrorCode >= 0,
                    catchall,
                    rlNpError("GetWorldInfo", npe->m_ErrorCode));

#if RSG_PS3
            const SceNpMatching2GetWorldInfoListResponse* resp =
                (SceNpMatching2GetWorldInfoListResponse*) npe->m_EventData;

            //We only support one world per server.
            rlAssertf(1 == resp->worldNum,
                    "Expected only one world per server, got %d",
                    resp->worldNum);

            rlTaskDebug("Retrieved world:0x%08x.", resp->world->worldId);

            m_WorldId = resp->world->worldId;
#elif RSG_ORBIS
            const rlNpMatching2GetWorldInfoListResponse* resp =
                (rlNpMatching2GetWorldInfoListResponse*) npe->m_EventData;

            //We only support one world per server.
            rlAssertf(1 == resp->m_WorldNum,
                    "Expected only one world per server, got %d",
                    resp->m_WorldNum);

            rlTaskDebug("Retrieved world:0x%08x.", resp->m_WorldId);

            m_WorldId = resp->m_WorldId;
#endif

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    enum State
    {
        STATE_GET_SERVER_ID_LIST,
        STATE_REQUEST_SERVER_INFO,
        STATE_REQUESTING_SERVER_INFO,
        STATE_REQUEST_WORLD_INFO,
        STATE_REQUESTING_WORLD_INFO,
        STATE_NEXT_SERVER
    };

    State m_State;
    SceNpMatching2ServerId* m_ServerIds;
    unsigned m_NumServers;
    unsigned m_NumServersToCheck;
    SceNpMatching2ServerId m_CurServerId;
    SceNpMatching2WorldId m_WorldId;
    SceNpMatching2WorldId* m_WorldIdPtr;

    bool m_ServerAvailable;
};

//////////////////////////////////////////////////////////////////////////
//  NpFindRoomsTask
//////////////////////////////////////////////////////////////////////////
class NpFindRoomsTask : public rlNpMatchingTask
{
public:
    RL_TASK_DECL(NpFindRoomsTask);

    NpFindRoomsTask()
        : m_State(STATE_WAITING_FOR_CONTEXT_START)
        , m_Results(NULL)
        , m_MaxResults(0)
        , m_NumResults(NULL)
        , m_GetWorldIdTask(NULL)
        , m_WorldId(RL_NP_INVALID_WORLD_ID)
    {
    }

    ~NpFindRoomsTask()
    {
        rlAssert(!m_GetWorldIdTask);
        rlAssert(!m_MyStatus.Pending());
    }

    bool Configure(const rlMatchingFilter& filter,
                    rlSessionInfo* results,
                    const unsigned maxResults,
                    unsigned* numResults)
    {
        rlAssert(!m_Results);
        rlAssert(!m_GetWorldIdTask);
        rlAssert(RL_NP_INVALID_WORLD_ID == m_WorldId);
        rlAssert(results);
        rlAssert(maxResults);
        rlAssert(numResults);
        rlAssert(filter.IsValid());

        m_Filter = filter;
        m_Results = results;
        m_MaxResults = maxResults;
        m_NumResults = numResults;

        return true;
    }

    void Start()
    {
        rlTaskDebug("Finding rooms...");

        rlAssert(STATE_WAITING_FOR_CONTEXT_START == m_State);

        this->rlNpMatchingTask::Start();
    }

    virtual void DoCancel()
    {
        this->rlNpMatchingTask::DoCancel();

        m_Results = NULL;

        if(m_GetWorldIdTask)
        {
            rlGetTaskManager()->CancelTask(m_GetWorldIdTask);
            m_GetWorldIdTask = NULL;
        }
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlTaskDebug("Finding rooms %s", FinishString(finishType));

        rlAssert(!m_GetWorldIdTask);
        rlAssert(!m_MyStatus.Pending());

        if(!WasCanceled() && FinishSucceeded(finishType))
        {
            rlAssert(m_Results);
            rlTaskDebug("Found %d rooms", *m_NumResults);
        }

        m_Filter.Clear();
        m_Results = NULL;

        this->rlNpMatchingTask::Finish(finishType, resultCode);
    }

    void Update(const unsigned timeStep)
    {
        this->rlNpMatchingTask::Update(timeStep);

		if(WasCanceled())
		{
            this->Finish(FINISH_CANCELED);
            return;
		}

        switch(m_State)
        {
        case STATE_WAITING_FOR_CONTEXT_START:
            if(g_rlNp.GetMatching().Started())
            {
                m_State = STATE_FIND_AVAILABLE_WORLD;
            }
            else if(!g_rlNp.GetMatching().Starting())
            {
                rlTaskError("Matching context failed to start");
                this->Finish(FINISH_FAILED);
            }
            break;
        case STATE_FIND_AVAILABLE_WORLD:
            //Find an available world.
            rlTaskDebug("Finding an available world for matchmaking...");

            rlAssert(!m_GetWorldIdTask);

            m_GetWorldIdTask =
                rlGetTaskManager()->CreateTask<NpGetRandomWorldIdTask>();

            if(!rlVerify(m_GetWorldIdTask))
            {
                rlTaskError("Failed to allocate NpGetRandomWorldIdTask");
                this->Finish(FINISH_FAILED);
            }
            else if(!rlTaskBase::Configure(m_GetWorldIdTask, &m_WorldId, &m_MyStatus))
            {
                rlTaskError("Failed to configure NpGetRandomWorldIdTask");
                rlGetTaskManager()->DestroyTask(m_GetWorldIdTask);
                this->Finish(FINISH_FAILED);
            }
            else if(!rlGetTaskManager()->AddParallelTask(m_GetWorldIdTask))
            {
                rlTaskError("Failed to start NpGetRandomWorldIdTask");
                rlGetTaskManager()->DestroyTask(m_GetWorldIdTask);
                this->Finish(FINISH_FAILED);
            }
            else
            {
                m_State = STATE_FINDING_AVAILABLE_WORLD;
            }
            break;
        case STATE_FINDING_AVAILABLE_WORLD:
            if(!m_MyStatus.Pending())
            {
                m_GetWorldIdTask = NULL;

                if(m_MyStatus.Succeeded())
                {
                    rlTaskDebug("Found available world:0x%08x", m_WorldId);
                    m_State = STATE_FIND_ROOMS;
                }
                else
                {
                    rlTaskDebug("Failed to find an available world");
                    this->Finish(FINISH_FAILED);
                }
            }
            break;
        case STATE_FIND_ROOMS:
            if(this->RequestFindRooms())
            {
                m_State = STATE_FINDING_ROOMS;
            }
            else
            {
                this->Finish(FINISH_FAILED);
            }
            break;
        case STATE_FINDING_ROOMS:
            if(m_ReqStatus.Succeeded())
            {
                this->Finish(FINISH_SUCCEEDED);
            }
            else if(!m_ReqStatus.Pending())
            {
                this->Finish(FINISH_FAILED);
            }
            break;
        }
    }

    private:

    virtual bool HandleNpEvent(rlNpMatching::NpEvent* npEvent)
    {
        switch((int)m_State)
        {
        case STATE_FINDING_ROOMS:
            rlAssert(m_ReqStatus.Pending());
            return this->HandleFindRoomsResponse(npEvent);
        default:
            rlAssert(false);
        }

        return false;
    }

    bool RequestFindRooms()
    {
        rlAssert(-1u == m_ReqId);

        bool success = false;

        m_ReqStatus.SetPending();

        rtry
        {
            rcheck(!WasCanceled(),catchall,);

            rverify(m_Filter.GetConditionCount() <= RL_MAX_MATCHING_CONDITIONS,
                    catchall,
                    rlTaskError("%d search conditions exceeds max of %d",
                                m_Filter.GetConditionCount(),
                                RL_MAX_MATCHING_CONDITIONS));

            rverify(m_Filter.GetGameType() != RL_GAMETYPE_INVALID,
                    catchall,
                    rlTaskError("Invalid game type in matching filter"));

            rverify(m_Filter.GetGameMode() != RL_INVALID_GAMEMODE,
                    catchall,
                    rlTaskError("Invalid game mode in matching filter"));

            rverify(-1u != m_WorldId,
                    catchall,
                    rlTaskError("Invalid world id"));

            rlTaskDebug("Finding rooms on world id:0x%08x...", m_WorldId);

            int err;

            //Populate the filter conditions

            SceNpMatching2IntSearchFilter intConditions[RL_MAX_MATCHING_CONDITIONS];
            sysMemSet(intConditions, 0, sizeof(intConditions));

            //Binary condition will contain game mode and game type.
            SceNpMatching2BinSearchFilter binCondition = {0};

            u16 gameModeAndType = 0;
            MakeGameModeAndType(m_Filter.GetGameMode(),
                                m_Filter.GetGameType(),
                                &gameModeAndType);
            binCondition.attr.id = SCE_NP_MATCHING2_ROOM_SEARCHABLE_BIN_ATTR_EXTERNAL_1_ID;
            binCondition.attr.ptr = &gameModeAndType;
            binCondition.attr.size = sizeof(gameModeAndType);
            binCondition.searchOperator = SCE_NP_MATCHING2_OPERATOR_EQ;

            SceNpMatching2IntSearchFilter* curIntCond = intConditions;

            for(int i = 0; i < (int) m_Filter.GetConditionCount(); ++i)
            {
                const u32* filterVal = m_Filter.GetValue(i);

                if(filterVal)
                {
                    const int fldIdx = m_Filter.GetSessionAttrIndexForCondition(i);
                    rlAssert(fldIdx >= 0);

                    const unsigned op = m_Filter.GetOperator(i);

                    curIntCond->attr.num = *filterVal;
                    curIntCond->attr.id =
                        fldIdx + SCE_NP_MATCHING2_ROOM_SEARCHABLE_INT_ATTR_EXTERNAL_1_ID;

                    switch(op)
                    {
                    case '==':
                        curIntCond->searchOperator = SCE_NP_MATCHING2_OPERATOR_EQ;
                        break;
                    case '!=':
                        curIntCond->searchOperator = SCE_NP_MATCHING2_OPERATOR_NE;
                        break;
                    case '>':
                        curIntCond->searchOperator = SCE_NP_MATCHING2_OPERATOR_GT;
                        break;
                    case '>=':
                        curIntCond->searchOperator = SCE_NP_MATCHING2_OPERATOR_GE;
                        break;
                    case '<':
                        curIntCond->searchOperator = SCE_NP_MATCHING2_OPERATOR_LT;
                        break;
                    case '<=':
                        curIntCond->searchOperator = SCE_NP_MATCHING2_OPERATOR_LE;
                        break;

                    default:
                        rlAssert(false);
                        break;
                    }

                    ++curIntCond;
                }
            }

            //Specify which attrs will be returned from the search.
            SceNpMatching2AttributeId returnAttrs[RL_MAX_MATCHING_ATTRS + 1] = {0};
            int numReturnAttrs = 0;

            returnAttrs[numReturnAttrs] = SCE_NP_MATCHING2_ROOM_SEARCHABLE_BIN_ATTR_EXTERNAL_1_ID;
            ++numReturnAttrs;

            for(int i = 0; i < (int) m_Filter.GetSessionAttrCount(); ++i, ++numReturnAttrs)
            {
                returnAttrs[numReturnAttrs] =
                    i + SCE_NP_MATCHING2_ROOM_SEARCHABLE_INT_ATTR_EXTERNAL_1_ID;
            }

            SceNpMatching2SearchRoomRequest searchRoomRqst = {0};

            searchRoomRqst.worldId = m_WorldId;
            searchRoomRqst.rangeFilter.startIndex = SCE_NP_MATCHING2_RANGE_FILTER_START_INDEX_MIN;
            searchRoomRqst.rangeFilter.max = m_MaxResults;
            if(searchRoomRqst.rangeFilter.max > SCE_NP_MATCHING2_RANGE_FILTER_MAX)
            {
                searchRoomRqst.rangeFilter.max = SCE_NP_MATCHING2_RANGE_FILTER_MAX;
            }

#if RSG_ORBIS
			// TODO: NS - Orbis fails if there are any int conditions
            searchRoomRqst.intFilter = NULL;
            searchRoomRqst.intFilterNum = 0;
#else
            searchRoomRqst.intFilter = intConditions;
            searchRoomRqst.intFilterNum = (curIntCond - intConditions);
#endif
            searchRoomRqst.binFilter = &binCondition;
            searchRoomRqst.binFilterNum = 1;
            searchRoomRqst.attrId = returnAttrs;
            searchRoomRqst.attrIdNum = numReturnAttrs;
            searchRoomRqst.option =
                SCE_NP_MATCHING2_SEARCH_ROOM_OPTION_WITH_NPID   //Get the NP ID of the owner
                | SCE_NP_MATCHING2_SEARCH_ROOM_OPTION_NAT_TYPE_FILTER   //Filter bad NATs
                | SCE_NP_MATCHING2_SEARCH_ROOM_OPTION_RANDOM;   //Obtain random list of rooms

            SceNpMatching2RequestOptParam optParam = {0};
            optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
            optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
            optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

            rcheck(0 <= (err = sceNpMatching2SearchRoom(g_rlNp.GetMatching().GetMatchingContextId(),
                                                        &searchRoomRqst,
                                                        &optParam,
                                                        &m_ReqId)),
                    catchall,
                    rlNpError("Error calling sceNpMatching2SearchRoom", err));

            success = true;
        }
        rcatchall
        {
            m_ReqStatus.SetFailed();
        }

        return success;
    }

    bool HandleFindRoomsResponse(rlNpMatching::NpEvent* npe)
    {
        bool success = false;

        rtry
        {
            rverify(npe,catchall,);

            rcheck(npe->m_ErrorCode >= 0,
                    catchall,
                    rlNpError("FindRooms", npe->m_ErrorCode));

            rlTaskDebug("sceNpMatching2SearchRoom succeeded");

#if RSG_PS3
			const SceNpMatching2SearchRoomResponse* resp =
                (SceNpMatching2SearchRoomResponse*) npe->m_EventData;
            const SceNpMatching2RoomDataExternal* roomData = resp->roomDataExternal;

            //Before touching mem we don't own make sure we weren't canceled.
            rcheck(!WasCanceled(), catchall,);

            *m_NumResults = 0;

			int resultCount = (int)resp->range.size;
#elif RSG_ORBIS
			const rlNpMatching2SearchRoomResponse* rlResp =
				(rlNpMatching2SearchRoomResponse*) npe->m_EventData;

			//Before touching mem we don't own make sure we weren't canceled.
			rcheck(!WasCanceled(), catchall,);

			const SceNpMatching2SearchRoomResponse* resp = &rlResp->m_Resp;
			const SceNpMatching2RoomDataExternal* roomData = resp->roomDataExternal;

			*m_NumResults = 0;

			int resultCount = (int)resp->range.resultCount;
#endif

            for(int i = 0; i < resultCount && *m_NumResults < m_MaxResults; ++i, roomData = roomData->next)
            {
                rlSessionInfo* sinfo = &m_Results[*m_NumResults];

                rlNpMatching::PopulateSearchResult(sinfo, roomData, m_Filter);

                if(sinfo->GetHostPeerAddress().IsLocal())
                {
                    rlTaskDebug("Found locally hosted session, discarding");
                    continue;
                }

                ++*m_NumResults;
            }

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    enum State
    {
        STATE_WAITING_FOR_CONTEXT_START,
        STATE_FIND_AVAILABLE_WORLD,
        STATE_FINDING_AVAILABLE_WORLD,
        STATE_FIND_ROOMS,
        STATE_FINDING_ROOMS
    };

    State m_State;
    rlMatchingFilter m_Filter;
    rlSessionInfo* m_Results;
    unsigned m_MaxResults;
    unsigned* m_NumResults;
    NpGetRandomWorldIdTask* m_GetWorldIdTask;
    SceNpMatching2WorldId m_WorldId;
    netStatus m_MyStatus;
};

//////////////////////////////////////////////////////////////////////////
//  NpCreateRoomTask
//////////////////////////////////////////////////////////////////////////
class NpCreateRoomTask : public rlNpMatchingTask
{
public:
    RL_TASK_DECL(NpCreateRoomTask);

    NpCreateRoomTask()
        : m_State(STATE_WAITING_FOR_CONTEXT_START)
        , m_MaxPubSlots(0)
        , m_MaxPrivSlots(0)
        , m_RoomInfoPtr(NULL)
        , m_GetWorldIdTask(NULL)
        , m_WorldId(RL_NP_INVALID_WORLD_ID)
    {
    }

    ~NpCreateRoomTask()
    {
        rlAssert(!m_GetWorldIdTask);
        rlAssert(!m_MyStatus.Pending());
    }

    bool Configure(const rlGamerInfo& owner,
                    const unsigned maxPubSlots,
                    const unsigned maxPrivSlots,
                    const rlMatchingAttributes& attrs,
                    rlNpRoomInfo* roomInfoPtr)
    {
        rlAssert(!m_MaxPubSlots);
        rlAssert(!m_MaxPrivSlots);
        rlAssert(!m_RoomInfoPtr);
        rlAssert(roomInfoPtr);
        rlAssert(!m_GetWorldIdTask);
        rlAssert(RL_NP_INVALID_WORLD_ID == m_WorldId);
        rlAssert(attrs.IsValid());

        m_Owner = owner;
        m_MaxPubSlots = maxPubSlots;
        m_MaxPrivSlots = maxPrivSlots;
        m_Attrs = attrs;
        m_RoomInfoPtr = roomInfoPtr;

        //At this point we must have public slots, otherwise we shouldn't
        //be advertising the session.
        return rlVerify(maxPubSlots > 0)
                && rlVerify(maxPubSlots + maxPrivSlots <= SCE_NP_MATCHING2_ROOM_MAX_SLOT);
    }

    void Start()
    {
        rlTaskDebug("Creating room...");

        rlAssert(STATE_WAITING_FOR_CONTEXT_START == m_State);

        this->rlNpMatchingTask::Start();
    }

    virtual void DoCancel()
    {
        this->rlNpMatchingTask::DoCancel();

        m_RoomInfoPtr = NULL;

        if(m_GetWorldIdTask)
        {
            rlGetTaskManager()->CancelTask(m_GetWorldIdTask);
            m_GetWorldIdTask = NULL;
        }
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlTaskDebug("Creating room %s", FinishString(finishType));

        rlAssert(!m_GetWorldIdTask);
        rlAssert(!m_MyStatus.Pending());

        m_Attrs.Clear();
        m_RoomInfoPtr = NULL;

        this->rlNpMatchingTask::Finish(finishType, resultCode);
    }

    void Update(const unsigned timeStep)
    {
        this->rlNpMatchingTask::Update(timeStep);

		if(WasCanceled())
		{
			this->Finish(FINISH_CANCELED);
            return;
		}

        switch(m_State)
        {
        case STATE_WAITING_FOR_CONTEXT_START:
            if(g_rlNp.GetMatching().Started())
            {
                m_State = STATE_FIND_AVAILABLE_WORLD;
            }
            else if(!g_rlNp.GetMatching().Starting())
            {
                rlTaskError("Matching context failed to start");
                this->Finish(FINISH_FAILED);
            }
            break;
        case STATE_FIND_AVAILABLE_WORLD:
            //Find an available world.
            rlTaskDebug("Finding an available world for matchmaking...");

            rlAssert(!m_GetWorldIdTask);

            m_GetWorldIdTask =
                rlGetTaskManager()->CreateTask<NpGetRandomWorldIdTask>();

            if(!rlVerify(m_GetWorldIdTask))
            {
                rlTaskError("Failed to allocate NpGetRandomWorldIdTask");
                this->Finish(FINISH_FAILED);
            }
            else if(!rlTaskBase::Configure(m_GetWorldIdTask, &m_WorldId, &m_MyStatus))
            {
                rlTaskError("Failed to configure NpGetRandomWorldIdTask");
                rlGetTaskManager()->DestroyTask(m_GetWorldIdTask);
                this->Finish(FINISH_FAILED);
            }
            else if(!rlGetTaskManager()->AddParallelTask(m_GetWorldIdTask))
            {
                rlTaskError("Failed to start NpGetRandomWorldIdTask");
                rlGetTaskManager()->DestroyTask(m_GetWorldIdTask);
                this->Finish(FINISH_FAILED);
            }
            else
            {
                m_State = STATE_FINDING_AVAILABLE_WORLD;
            }
            break;
        case STATE_FINDING_AVAILABLE_WORLD:
            if(!m_MyStatus.Pending())
            {
                m_GetWorldIdTask = NULL;

                if(m_MyStatus.Succeeded())
                {
                    rlTaskDebug("Found available world:0x%08x", m_WorldId);
                    m_State = STATE_CREATE_ROOM;
                }
                else
                {
                    rlTaskDebug("Failed to find an available world");
                    this->Finish(FINISH_FAILED);
                }
            }
            break;
        case STATE_CREATE_ROOM:
            if(this->RequestCreateRoom())
            {
                m_State = STATE_CREATING_ROOM;
            }
            else
            {
                this->Finish(FINISH_FAILED);
            }
            break;
        case STATE_CREATING_ROOM:
            if(m_ReqStatus.Succeeded())
            {
                this->Finish(FINISH_SUCCEEDED);
            }
            else if(!m_ReqStatus.Pending())
            {
                this->Finish(FINISH_FAILED);
            }
            break;
        }
    }

    private:

    virtual bool HandleNpEvent(rlNpMatching::NpEvent* npEvent)
    {
        switch((int)m_State)
        {
        case STATE_CREATING_ROOM:
            rlAssert(m_ReqStatus.Pending());
            return this->HandleCreateRoomResponse(npEvent);
        default:
            rlAssert(false);
        }

        return false;
    }

    bool RequestCreateRoom()
    {
        bool success = false;

        m_ReqStatus.SetPending();

        rtry
        {
            rcheck(!WasCanceled(),catchall,);

            m_RoomInfoPtr->Clear();

            rcheck(m_Owner.IsLocal(),
                    catchall,
                    rlTaskError("Can't create a room - gamer not local"));

            rcheck(m_Owner.IsOnline(),
                    catchall,
                    rlTaskError("Can't create a room - gamer not online"));

            rverify(m_Attrs.GetCount() <= RL_MAX_MATCHING_ATTRS,
                    catchall,
                    rlTaskError("Too many matching attributes"));

#if !__NO_OUTPUT
            rlTaskDebug("Gamer:\"%s\" creating room...", m_Owner.GetName());

            rlTaskDebug("  Attributes:");
            for(int i = 0; i < m_Attrs.GetCount(); ++i)
            {
                const u32* val = m_Attrs.GetValueByIndex(i);
                if(val)
                {
                    rlTaskDebug("    0x%08x:    0x%08x", m_Attrs.GetAttrId(i), *val);
                }
                else
                {
                    rlTaskDebug("    0x%08x:    nil", m_Attrs.GetAttrId(i));
                }
            }
#endif  //__NO_OUTPUT

            int err;

            rverify(m_WorldId != -1u,
                    catchall,
                    rlTaskError("No matchmaking servers available"));

            rlTaskDebug("  World Id:    0x%08x", m_WorldId);

            SceNpMatching2IntAttr intAttrs[RL_MAX_MATCHING_ATTRS];
            sysMemSet(intAttrs, 0, sizeof(intAttrs));

            //Binary attribute will contain game mode and game type.
            SceNpMatching2BinAttr binAttr = {0};
            u8 binAttrData[SCE_NP_MATCHING2_ROOM_SEARCHABLE_BIN_ATTR_EXTERNAL_MAX_SIZE];

            const int numIntAttrs =
                PopulateMatchingAttrs(m_Attrs, intAttrs, &binAttr, binAttrData, NIL_ATTRS_PROHIBITED);

            rverify((unsigned) numIntAttrs == m_Attrs.GetCount(),
                    catchall,
                    rlTaskError("Error populating matching attributes"));

            SceNpMatching2RoomPasswordSlotMask pwSlotMask = 0;

            //The room owner joins in slot one, which if we have public
            //slots should be a public slot.  Therefore, if we have
            //private slots make the *last* slots private.
            int slotIdx = m_MaxPrivSlots + m_MaxPubSlots;

            for(int i = 0; i < (int) m_MaxPrivSlots; ++i, --slotIdx)
            {
                SCE_NP_MATCHING2_ADD_SLOTNUM_TO_ROOM_PASSWORD_SLOT_MASK(pwSlotMask, slotIdx);
            }

            //SceNpMatching2SignalingOptParam signalingParam = {0};
            //signalingParam.type = SCE_NP_MATCHING2_SIGNALING_TYPE_MESH;

            SceNpMatching2CreateJoinRoomRequest createRoomRqst = {0};

            createRoomRqst.worldId = m_WorldId;
            createRoomRqst.maxSlot = m_MaxPubSlots + m_MaxPrivSlots;
            createRoomRqst.flagAttr = SCE_NP_MATCHING2_ROOM_FLAG_ATTR_OWNER_AUTO_GRANT;
            createRoomRqst.roomSearchableIntAttrExternal = intAttrs;
            createRoomRqst.roomSearchableIntAttrExternalNum = numIntAttrs;
            createRoomRqst.roomSearchableBinAttrExternal = &binAttr;
            createRoomRqst.roomSearchableBinAttrExternalNum = 1;
            createRoomRqst.passwordSlotMask = &pwSlotMask;
            createRoomRqst.roomPassword = &RLNP_ROOM_PASSWORD;
            //createRoomRqst.sigOptParam = &signalingParam;

            // Setup the blocked user list
            SceNpId blockedUser[SCE_NP_MATCHING2_ROOM_BLOCKED_USER_MAX] = {0};

            createRoomRqst.blockedUser = blockedUser;
            createRoomRqst.blockedUserNum = 0;

#if RSG_PS3
			// TODO: NS - no block list support in Orbis SDK v0.91?
            uint32_t count = 0;
            if( sceNpBasicGetBlockListEntryCount(&count) >= 0 )
            {
                for(int i = 0; i < count && i < SCE_NP_MATCHING2_ROOM_BLOCKED_USER_MAX; i++) 
                {   
                    if(rlVerify(sceNpBasicGetBlockListEntry(i, &blockedUser[createRoomRqst.blockedUserNum]) >= 0))
                    {
                        ++createRoomRqst.blockedUserNum;
                    }
                    else
                    {
                        rlError(0, "RequestCreateRoom - failed to retrieve block list entry %d", i);
                    }
                }                                              
            }
#endif

            SceNpMatching2RequestOptParam optParam = {0};
            optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
            optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
            optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

            rcheck(0 <= (err = sceNpMatching2CreateJoinRoom(g_rlNp.GetMatching().GetMatchingContextId(),
                                                            &createRoomRqst,
                                                            &optParam,
                                                            &m_ReqId)),
                    catchall,
                    rlNpError("Error calling sceNpMatching2CreateJoinRoom", err));

            success = true;
        }
        rcatchall
        {
            m_ReqStatus.SetFailed();
        }

        return success;
    }

    bool HandleCreateRoomResponse(rlNpMatching::NpEvent* npe)
    {
        bool success = false;

        rtry
        {
            rverify(npe,catchall,);

            rcheck(npe->m_ErrorCode >= 0,
                    catchall,
                    rlNpError("CreateRoom", npe->m_ErrorCode));

            rlTaskDebug("sceNpMatching2CreateJoinRoom succeeded");

#if RSG_PS3
            const SceNpMatching2CreateJoinRoomResponse* resp =
                (SceNpMatching2CreateJoinRoomResponse*) npe->m_EventData;

            //Before touching mem we don't own make sure we weren't canceled.
            rcheck(!WasCanceled(), catchall,);

            m_RoomInfoPtr->InitPublic(resp->roomDataInternal->roomId);
#elif RSG_ORBIS
            const rlNpMatching2CreateJoinRoomResponse* resp =
                (rlNpMatching2CreateJoinRoomResponse*) npe->m_EventData;

            //Before touching mem we don't own make sure we weren't canceled.
            rcheck(!WasCanceled(), catchall,);

            m_RoomInfoPtr->InitPublic(resp->m_RoomId);
#endif

            rlTaskDebug("  Room id: 0x%016" I64FMT "x", m_RoomInfoPtr->GetRoomId());

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    enum State
    {
        STATE_WAITING_FOR_CONTEXT_START,
        STATE_FIND_AVAILABLE_WORLD,
        STATE_FINDING_AVAILABLE_WORLD,
        STATE_CREATE_ROOM,
        STATE_CREATING_ROOM
    };

    State m_State;
    rlGamerInfo m_Owner;
    unsigned m_MaxPubSlots;
    unsigned m_MaxPrivSlots;
    rlMatchingAttributes m_Attrs;
    rlNpRoomInfo* m_RoomInfoPtr;
    NpGetRandomWorldIdTask* m_GetWorldIdTask;
    SceNpMatching2WorldId m_WorldId;
    netStatus m_MyStatus;
};

//////////////////////////////////////////////////////////////////////////
//  NpJoinRoomTask
//////////////////////////////////////////////////////////////////////////
class NpJoinRoomTask : public rlNpMatchingTask
{
public:
    RL_TASK_DECL(NpJoinRoomTask);

    NpJoinRoomTask()
        : m_SlotType(RL_SLOT_INVALID)
        , m_Members(NULL)
    {
    }

    bool Configure(const rlNpRoomInfo& roomInfo,
                    const rlSlotType slotType,
                    rlNpRoomMembers* members)
    {
        rlAssert(RL_SLOT_INVALID == m_SlotType);
        rlAssert(!m_Members);

        m_RoomInfo = roomInfo;
        m_SlotType = slotType;
        m_Members = members;

        return rlVerify(roomInfo.IsValid())
                && rlVerify(RL_SLOT_INVALID != slotType);
    }

    void Start()
    {
        rlTaskDebug("Joining room:0x%016" I64FMT "x...", m_RoomInfo.GetRoomId());

        this->rlNpMatchingTask::Start();

        if(!rlVerify(g_rlNp.GetMatching().Started()))
        {
            rlTaskError("NP matching context not started");
            this->Finish(FINISH_FAILED);
        }
        else if(!this->RequestJoinRoom())
        {
            rlTaskError("Failed to request to join the room");
            this->Finish(FINISH_FAILED);
        }
    }

    virtual void DoCancel()
    {
        this->rlNpMatchingTask::DoCancel();

        m_Members = NULL;
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlTaskDebug("Joining room:0x%016" I64FMT "x %s",
                    m_RoomInfo.GetRoomId(),
                    FinishString(finishType));

        m_Members = NULL;

        this->rlNpMatchingTask::Finish(finishType, resultCode);
    }

    void Update(const unsigned timeStep)
    {
        this->rlNpMatchingTask::Update(timeStep);

		if(WasCanceled())
		{
            this->Finish(FINISH_CANCELED);
            return;
		}

        if(m_ReqStatus.Succeeded())
        {
            this->Finish(FINISH_SUCCEEDED);
        }
        else if(!m_ReqStatus.Pending())
        {
            this->Finish(FINISH_FAILED);
        }
    }

    private:

    virtual bool HandleNpEvent(rlNpMatching::NpEvent* npEvent)
    {
        rlAssert(m_ReqStatus.Pending());
        return this->HandleJoinRoomResponse(npEvent);
    }

    bool RequestJoinRoom()
    {
        bool success = false;

        m_ReqStatus.SetPending();

        rtry
        {
            rcheck(!WasCanceled(),catchall,);

            SceNpMatching2RequestOptParam optParam = {0};
            optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
            optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
            optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

            SceNpMatching2JoinRoomRequest joinRoomRqst = {0};

            joinRoomRqst.roomId = m_RoomInfo.GetRoomId();
            if(RL_SLOT_PRIVATE == m_SlotType)
            {
                joinRoomRqst.roomPassword = &RLNP_ROOM_PASSWORD;
            }

            int err = sceNpMatching2JoinRoom(g_rlNp.GetMatching().GetMatchingContextId(),
                                            &joinRoomRqst,
                                            &optParam,
                                            &m_ReqId);

            rcheck(err >= 0,
                    catchall,
                    rlNpError("Error calling sceNpMatching2JoinRoom", err));

            success = true;
        }
        rcatchall
        {
            m_ReqStatus.SetFailed();
        }

        return success;
    }

    bool HandleJoinRoomResponse(rlNpMatching::NpEvent* npe)
    {
        bool success = false;

        rtry
        {
            rverify(npe,catchall,);

            rcheck(npe->m_ErrorCode >= 0,
                    catchall,
                    rlNpError("JoinRoom", npe->m_ErrorCode));

            rlTaskDebug("sceNpMatching2JoinRoom succeeded");

#if RSG_PS3
            const SceNpMatching2JoinRoomResponse* resp =
                (SceNpMatching2JoinRoomResponse*) npe->m_EventData;

            //Before touching mem we don't own make sure we weren't canceled.
            rcheck(!WasCanceled(), catchall,);

            if(m_Members)
            {
                const SceNpMatching2RoomMemberDataInternal* member =
                    resp->roomDataInternal->memberList.members;

                m_Members->m_NumMembers = resp->roomDataInternal->memberList.membersNum;

                for(int i = 0; i < (int) m_Members->m_NumMembers; ++i)
                {
                    m_Members->m_Members[i].Reset(member->userInfo.npId);
                    member = member->next;
                }
            }
#elif RSG_ORBIS
            const rlNpMatching2JoinRoomResponse* resp =
                (rlNpMatching2JoinRoomResponse*) npe->m_EventData;

            //Before touching mem we don't own make sure we weren't canceled.
            rcheck(!WasCanceled(), catchall,);

            if(m_Members)
            {
				m_Members->m_NumMembers = resp->m_Members.m_NumMembers;

				for(int i = 0; i < (int) m_Members->m_NumMembers; ++i)
                {
					m_Members->m_Members[i].Reset(resp->m_Members.m_Members[i].AsSceNpId());
                }
            }
#endif

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    rlNpRoomInfo m_RoomInfo;
    rlSlotType m_SlotType;
    rlNpRoomMembers* m_Members;
};

//////////////////////////////////////////////////////////////////////////
//  NpLeaveRoomTask
//////////////////////////////////////////////////////////////////////////
class NpLeaveRoomTask : public rlNpMatchingTask
{
public:
    RL_TASK_DECL(NpLeaveRoomTask);

    NpLeaveRoomTask()
		: m_State(STATE_LEAVING_ROOM)
		, m_bDetached(false)
		, m_bStartedContext(false)
	{
    }

    bool Configure(const rlNpRoomInfo& roomInfo, bool bDetached)
    {
        m_RoomInfo = roomInfo;
		m_bDetached = bDetached;
        return rlVerify(roomInfo.IsValid());
    }

    void Start()
    {
        rlTaskDebug("Leaving room:0x%016" I64FMT "x...", m_RoomInfo.GetRoomId());

        this->rlNpMatchingTask::Start();

        if(!g_rlNp.GetMatching().Started())
        {
			if(m_bDetached)
			{
				rlTaskError("Running detached - Starting Matching Context ");
				if(g_rlNp.GetMatching().Start())
				{
					m_bStartedContext = true;
					m_State = STATE_STARTING_CONTEXT;
				}
				else
				{
					rlTaskError("Running detached - Failed to start Matching Context");
					this->Finish(FINISH_FAILED);
				}
			}
			else
			{
				rlTaskError("NP matching context not started");
				this->Finish(FINISH_FAILED);
			}
        }
        else if(!this->RequestLeaveRoom())
        {
            rlTaskError("Failed to request to leave the room");
            this->Finish(FINISH_FAILED);
        }
    }

    virtual void DoCancel()
    {
        this->rlNpMatchingTask::DoCancel();
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlTaskDebug("Leaving room:0x%016" I64FMT "x %s",
                    m_RoomInfo.GetRoomId(),
                    FinishString(finishType));

		if(m_bStartedContext)
		{
			g_rlNp.GetMatching().Stop();
		}

        this->rlNpMatchingTask::Finish(finishType, resultCode);
    }

    void Update(const unsigned timeStep)
    {
        this->rlNpMatchingTask::Update(timeStep);

		if(WasCanceled())
		{
            this->Finish(FINISH_CANCELED);
            return;
		}

		switch(m_State)
		{
		case STATE_STARTING_CONTEXT:
			if(g_rlNp.GetMatching().Started())
			{
				if(!this->RequestLeaveRoom())
				{
					rlTaskError("Failed to request to leave the room");
					this->Finish(FINISH_FAILED);
				}
				else
				{
					rlTaskDebug("Started Matching context. Leaving Room");
					m_State = STATE_LEAVING_ROOM;
				}
			}
			break;

		case STATE_LEAVING_ROOM:
			{
				if(m_ReqStatus.Succeeded())
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else if(!m_ReqStatus.Pending())
				{
					this->Finish(FINISH_FAILED);
				}
			}
		}
    }

    private:

    bool RequestLeaveRoom()
    {
        bool success = false;

        m_ReqStatus.SetPending();

        rtry
        {
            rcheck(!WasCanceled(),catchall,);

            SceNpMatching2RequestOptParam optParam = {0};
            optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
            optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
            optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

            SceNpMatching2LeaveRoomRequest leaveRoomRqst = {0};

            leaveRoomRqst.roomId = m_RoomInfo.GetRoomId();

            int err = sceNpMatching2LeaveRoom(g_rlNp.GetMatching().GetMatchingContextId(),
                                            &leaveRoomRqst,
                                            &optParam,
                                            &m_ReqId);

            rcheck(err >= 0,
                    catchall,
                    rlNpError("Error calling sceNpMatching2LeaveRoom", err));

            success = true;
        }
        rcatchall
        {
            m_ReqStatus.SetFailed();
        }

        return success;
    }

	enum State
	{
		STATE_STARTING_CONTEXT,
		STATE_LEAVING_ROOM,
	};

    rlNpRoomInfo m_RoomInfo;
	State m_State;
	bool m_bDetached;
	bool m_bStartedContext;
};

//////////////////////////////////////////////////////////////////////////
//  NpSetExtRoomDataTask
//////////////////////////////////////////////////////////////////////////
class NpSetExtRoomDataTask : public rlNpMatchingTask
{
public:
    RL_TASK_DECL(NpSetExtRoomDataTask);

    NpSetExtRoomDataTask()
    {
    }

    bool Configure(const rlNpRoomInfo& roomInfo,
                    const rlMatchingAttributes& newAttrs)
    {
        m_RoomInfo = roomInfo;
        m_NewAttrs = newAttrs;

        return rlVerify(roomInfo.IsValid())
                && rlVerify(newAttrs.IsValid());
    }

    void Start()
    {
        rlTaskDebug("Changing attributes for room:0x%016" I64FMT "x...",
                    m_RoomInfo.GetRoomId());

        this->rlNpMatchingTask::Start();

        if(!rlVerify(g_rlNp.GetMatching().Started()))
        {
            rlTaskError("NP matching context not started");
            this->Finish(FINISH_FAILED);
        }
        else if(!this->RequestChangeRoomAttrs())
        {
            rlTaskError("Failed to request changing room attributes");
            this->Finish(FINISH_FAILED);
        }
    }

    virtual void DoCancel()
    {
        this->rlNpMatchingTask::DoCancel();
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlTaskDebug("Changing attributes for room:0x%016" I64FMT "x %s",
                    m_RoomInfo.GetRoomId(),
                    FinishString(finishType));

        this->rlNpMatchingTask::Finish(finishType, resultCode);
    }

    void Update(const unsigned timeStep)
    {
        this->rlNpMatchingTask::Update(timeStep);

		if(WasCanceled())
		{
            this->Finish(FINISH_CANCELED);
            return;
		}

        if(m_ReqStatus.Succeeded())
        {
            this->Finish(FINISH_SUCCEEDED);
        }
        else if(!m_ReqStatus.Pending())
        {
            this->Finish(FINISH_FAILED);
        }
    }

    private:

    bool RequestChangeRoomAttrs()
    {
        bool success = false;

        m_ReqStatus.SetPending();

        rtry
        {
            rcheck(!WasCanceled(),catchall,);

            SceNpMatching2RequestOptParam optParam = {0};
            optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
            optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
            optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

            SceNpMatching2IntAttr intAttrs[RL_MAX_MATCHING_ATTRS] = {0};

            //Binary attribute will contain game mode and game type.
            SceNpMatching2BinAttr binAttr = {0};
            u8 binAttrData[SCE_NP_MATCHING2_ROOM_SEARCHABLE_BIN_ATTR_EXTERNAL_MAX_SIZE];

            const int numIntAttrs =
                PopulateMatchingAttrs(m_NewAttrs,
                                        intAttrs,
                                        &binAttr,
                                        binAttrData,
                                        NIL_ATTRS_PERMITTED);

            rverify((unsigned) numIntAttrs == m_NewAttrs.GetCount(),
                    catchall,
                    rlTaskError("Error populating matching attributes"));

            SceNpMatching2SetRoomDataExternalRequest setRoomDataRqst = {0};

            setRoomDataRqst.roomId = m_RoomInfo.GetRoomId();
            setRoomDataRqst.roomSearchableIntAttrExternal = intAttrs;
            setRoomDataRqst.roomSearchableIntAttrExternalNum = numIntAttrs;
            setRoomDataRqst.roomSearchableBinAttrExternal = &binAttr;
            setRoomDataRqst.roomSearchableBinAttrExternalNum = 1;

            int err = sceNpMatching2SetRoomDataExternal(g_rlNp.GetMatching().GetMatchingContextId(),
                                                        &setRoomDataRqst,
                                                        &optParam,
                                                        &m_ReqId);

            rcheck(err >= 0,
                    catchall,
                    rlNpError("Error calling sceNpMatching2SetRoomDataExternal", err));

            success = true;
        }
        rcatchall
        {
            m_ReqStatus.SetFailed();
        }

        return success;
    }

    rlNpRoomInfo m_RoomInfo;
    rlMatchingAttributes m_NewAttrs;
};

//////////////////////////////////////////////////////////////////////////
//  NpSetRoomVisibleTask
//////////////////////////////////////////////////////////////////////////
class NpSetRoomVisibleTask : public rlNpMatchingTask
{
public:
    RL_TASK_DECL(NpSetRoomVisibleTask);

    NpSetRoomVisibleTask()
        : m_Visible(false)
    {
    }

    bool Configure(const rlNpRoomInfo& roomInfo,
                    const bool visible)
    {
        m_RoomInfo = roomInfo;
        m_Visible = visible;

        return rlVerify(roomInfo.IsValid());
    }

    void Start()
    {
        rlTaskDebug("Setting visibility to \"%s\" for room:0x%016" I64FMT "x...",
                    m_Visible ? "true" : "false",
                    m_RoomInfo.GetRoomId());

        this->rlNpMatchingTask::Start();

        if(!rlVerify(g_rlNp.GetMatching().Started()))
        {
            rlTaskError("NP matching context not started");
            this->Finish(FINISH_FAILED);
        }
        else if(!this->RequestSetRoomVisibility())
        {
            rlTaskError("Failed to request room visibility change");
            this->Finish(FINISH_FAILED);
        }
    }

    virtual void DoCancel()
    {
        this->rlNpMatchingTask::DoCancel();
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        rlTaskDebug("Setting visibility to \"%s\" for room:0x%016" I64FMT "x %s",
                    m_Visible ? "true" : "false",
                    m_RoomInfo.GetRoomId(),
                    FinishString(finishType));

        this->rlNpMatchingTask::Finish(finishType, resultCode);
    }

    void Update(const unsigned timeStep)
    {
        this->rlNpMatchingTask::Update(timeStep);

		if(WasCanceled())
		{
            this->Finish(FINISH_CANCELED);
            return;
		}

        if(m_ReqStatus.Succeeded())
        {
            this->Finish(FINISH_SUCCEEDED);
        }
        else if(!m_ReqStatus.Pending())
        {
            this->Finish(FINISH_FAILED);
        }
    }

    private:

    bool RequestSetRoomVisibility()
    {
        bool success = false;

        m_ReqStatus.SetPending();

        rtry
        {
            rcheck(!WasCanceled(),catchall,);

            SceNpMatching2RequestOptParam optParam = {0};
            optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
            optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
            optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

            SceNpMatching2SetRoomDataInternalRequest setRoomDataRqst = {0};

            setRoomDataRqst.roomId = m_RoomInfo.GetRoomId();
            setRoomDataRqst.flagFilter = SCE_NP_MATCHING2_ROOM_FLAG_ATTR_HIDDEN;
            setRoomDataRqst.flagAttr =
                m_Visible ? 0 : SCE_NP_MATCHING2_ROOM_FLAG_ATTR_HIDDEN;

            int err = sceNpMatching2SetRoomDataInternal(g_rlNp.GetMatching().GetMatchingContextId(),
                                                        &setRoomDataRqst,
                                                        &optParam,
                                                        &m_ReqId);

            rcheck(err >= 0,
                    catchall,
                    rlNpError("Error calling sceNpMatching2SetRoomDataInternal", err));

            success = true;
        }
        rcatchall
        {
            m_ReqStatus.SetFailed();
        }

        return success;
    }

    rlNpRoomInfo m_RoomInfo;
    bool m_Visible;
};

//////////////////////////////////////////////////////////////////////////
//  NpGetSessionInfoFromRoomTask
//////////////////////////////////////////////////////////////////////////
class NpGetSessionInfoFromRoomTask : public rlNpMatchingTask
{
public:
	RL_TASK_DECL(NpGetSessionInfoFromRoomTask);

	static const int MAX_SESSION_INFOS_TO_FIND = RL_FIND_SESSIONS_MAX_SESSIONS_TO_FIND;

	NpGetSessionInfoFromRoomTask()
		: m_NumSessionTokens(0)
		, m_SessionInfos(NULL)
	{
	}

	bool Configure(
		const rlSessionToken* sessionTokens,
		int numSessionTokens,
		rlSessionInfo* sessionInfos
		);

	void Start()
	{
		rlAssert(!m_RequestCount);
		rlAssert(!m_MadeRequests);

#if !__NO_OUTPUT
		rlTaskDebug("Getting session info for %d rooms...", m_NumSessionTokens);
		for (int i = 0; i < m_NumSessionTokens; ++i)
		{
			rlTaskDebug("  Room 0x%016" I64FMT "x...", m_SessionTokens[i].m_Value);
		}
#endif

		this->rlNpMatchingTask::Start();

		if(!rlVerify(g_rlNp.GetMatching().Started()))
		{
			rlTaskError("NP matching context not started");
			this->Finish(FINISH_FAILED);
		}
		else if(!this->RequestGetSessionMemberList())
		{
			rlTaskError("Failed to request room member list");
			this->Finish(FINISH_FAILED);
		}
	}

	virtual void DoCancel()
	{
		this->rlNpMatchingTask::DoCancel();

		m_NumSessionTokens = 0;
		m_SessionInfos = NULL;
	}

	virtual bool IsCancelable() const
	{
		return true;
	}

	virtual void Finish(const FinishType finishType, const int resultCode = 0)
	{
		m_NumSessionTokens = 0;
		m_SessionInfos = NULL;

		this->rlNpMatchingTask::Finish(finishType, resultCode);
	}

	void Update(const unsigned timeStep)
	{
		this->rlNpMatchingTask::Update(timeStep);

		if((m_RequestCount == 0 && m_MadeRequests) || WasCanceled())
		{
			// In either the finish or cancel case, we should clear out the session infos
			// that we did not succeed in getting info for
			for (int i = 0; i < m_NumSessionTokens; ++i)
			{
				if (!m_RequestStatus[i].Succeeded())
				{
					if (WasCanceled())
					{
						// It didn't succeed and it didn't fail, it cancelled
						if (!m_RequestStatus[i].Failed())
						{
							m_RequestStatus[i].SetCanceled();
						}
					}
					else
					{
						// It failed, and we shouldn't be running this code unless we went through a codepath that marked it as failed.
						// If this assert trips, we should figure out how m_RequestCount got to zero with this not having succeeded or
						// failed
						rlAssert(m_RequestStatus[i].Failed());
					}

					m_SessionInfos[i].Clear();
				}
			}
			this->Finish(WasCanceled() ? FINISH_CANCELED : FINISH_SUCCEEDED);
		}
	}

private:

	virtual bool HandleNpEvent(rlNpMatching::NpEvent* npEvent)
	{
		int requestIndex = -1;

		rlAssert(m_NumSessionTokens <= MAX_SESSION_INFOS_TO_FIND);

		for (int i = 0; i < m_NumSessionTokens; ++i)
		{
			if (m_RequestId[i] == npEvent->m_ReqId)
			{
				requestIndex = i;
				break;
			}
		}

		if (rlVerifyf(requestIndex >= 0, "Received npEvent where ReqId does not match one of our requests"))
		{
			rlAssert(requestIndex < MAX_SESSION_INFOS_TO_FIND);

			this->HandleGetSessionMemberListResponse(npEvent, requestIndex);
		}

		if (!m_RequestCount)
		{
			m_ReqStatus.SetSucceeded();
		}

		return true;
	}

	bool RequestGetSessionMemberList()
	{
		bool success = false;

		m_ReqStatus.SetPending();

		rlAssert(m_NumSessionTokens <= MAX_SESSION_INFOS_TO_FIND);
		for (int i = 0; i < m_NumSessionTokens; ++i)
		{
			m_RequestStatus[i].SetPending();

			rtry
			{
				rcheck(!WasCanceled(),catchall,);

				SceNpMatching2RequestOptParam optParam = {0};
				optParam.cbFunc = &rlNpMatchingTask::RequestCallback;
				optParam.cbFuncArg = (void*) (size_t) this->GetTaskId();
				optParam.timeout = RL_NP_REQUEST_TIMEOUT_USEC;

				SceNpMatching2GetRoomMemberDataExternalListRequest getRoomMemberDataExternalListRqst = {0};

				getRoomMemberDataExternalListRqst.roomId = m_SessionTokens[i].m_Value;

				int err = sceNpMatching2GetRoomMemberDataExternalList(g_rlNp.GetMatching().GetMatchingContextId(),
					&getRoomMemberDataExternalListRqst,
					&optParam,
					&m_RequestId[i]);

				rcheck(err >= 0,
					catchall,
					rlNpError("Error calling sceNpMatching2GetRoomMemberDataExternalList", err));

				++m_RequestCount;

				success = true;
			}
			rcatchall
			{
				m_RequestStatus[i].SetFailed();
			}
		}

		m_MadeRequests = true;
		return success;
	}

	bool HandleGetSessionMemberListResponse(rlNpMatching::NpEvent* npe, int requestIndex)
	{
		bool success = false;

		rlAssert(requestIndex > -1);
		rlAssert(requestIndex < MAX_SESSION_INFOS_TO_FIND);
		rlAssert(m_RequestStatus[requestIndex].Pending());

		--m_RequestCount;
		rlAssert(m_RequestCount >= 0);

		rtry
		{
			rverify(npe,catchall,);

			rcheck(npe->m_ErrorCode >= 0,
				catchall,
				rlNpError("GetRoomMemberDataExternalList", npe->m_ErrorCode));

			rlTaskDebug("sceNpMatching2GetRoomMemberDataExternalList succeeded");

			m_RequestStatus[requestIndex].SetSucceeded();

#if RSG_PS3
			const SceNpMatching2GetRoomMemberDataExternalListResponse* resp =
				(SceNpMatching2GetRoomMemberDataExternalListResponse*) npe->m_EventData;

			//Before touching mem we don't own make sure we weren't canceled.
			rcheck(!WasCanceled(), catchall,);

			SceNpMatching2RoomMemberDataExternal *memberData = resp->roomMemberDataExternal;
			bool foundSessionOwner = false;
			for (int i = 0; i < (int)resp->roomMemberDataExternalNum; ++i)
			{
				if (memberData->role == SCE_NP_MATCHING2_ROLE_OWNER)
				{
					foundSessionOwner = true;
					break;
				}
				
				memberData = memberData->next;
			}

			if (foundSessionOwner)
			{
				SceNpId npId = memberData->userInfo.npId;

				rlNpRoomInfo roomInfo;
				roomInfo.InitPublic(m_SessionTokens[requestIndex].m_Value);
				
				rlPeerAddress address;
				address.SetNpId(npId);

				m_SessionInfos[requestIndex].Init(roomInfo, address);
			}
			else
			{
				m_SessionInfos[requestIndex].Clear();
			}

#elif RSG_ORBIS
			rlAssert(0);
#endif

			success = true;
		}
		rcatchall
		{
		}

		return success;
	}

	int m_NumSessionTokens;
	rlSessionToken m_SessionTokens[MAX_SESSION_INFOS_TO_FIND];
	rlSessionInfo *m_SessionInfos;

	netStatus m_RequestStatus[MAX_SESSION_INFOS_TO_FIND];
	u32 m_RequestId[MAX_SESSION_INFOS_TO_FIND];
	
	int m_RequestCount;
	bool m_MadeRequests;
};

bool NpGetSessionInfoFromRoomTask::Configure( const rlSessionToken* sessionTokens, int numSessionTokens, rlSessionInfo* sessionInfos )
{
	m_NumSessionTokens = numSessionTokens;

	for (int i = 0; i < m_NumSessionTokens; ++i)
	{
		m_SessionTokens[i] = sessionTokens[i];
	}

	m_SessionInfos = sessionInfos;
	
	m_RequestCount = 0;
	m_MadeRequests = false;

	if (rlVerifyf(m_NumSessionTokens <= MAX_SESSION_INFOS_TO_FIND, "NpGetSessionInfoFromRoomTask supports up to %d queries, but was asked to find %d queries", MAX_SESSION_INFOS_TO_FIND, numSessionTokens))
	{
		return true;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////
//  rlNpMatchingEvent
//////////////////////////////////////////////////////////////////////////
rlNpMatchingEvent::rlNpMatchingEvent(const rlNpMatchingEventId eventId)
: m_EventId(eventId)
{
}

rlNpMatchingEventId
rlNpMatchingEvent::GetId() const
{
    return m_EventId;
}

const rlNpMatchingEventGamerJoined*
rlNpMatchingEvent::AsGamerJoined() const
{
    return (RL_NP_MATCHINGEVENT_GAMER_JOINED == m_EventId) ? (const rlNpMatchingEventGamerJoined*) this : NULL;
}

const rlNpMatchingEventGamerLeft*
rlNpMatchingEvent::AsGamerLeft() const
{
    return (RL_NP_MATCHINGEVENT_GAMER_LEFT == m_EventId) ? (const rlNpMatchingEventGamerLeft*) this : NULL;
}

//////////////////////////////////////////////////////////////////////////
//  rlNpMatchingEventGamerJoined
//////////////////////////////////////////////////////////////////////////
rlNpMatchingEventGamerJoined::rlNpMatchingEventGamerJoined()
: rlNpMatchingEvent(RL_NP_MATCHINGEVENT_GAMER_JOINED)
{
}

void
rlNpMatchingEventGamerJoined::Reset(const rlNpRoomInfo& roomInfo, const rlSceNpId& npId)
{
    m_RoomInfo = roomInfo;
    m_NpId = npId;
}

//////////////////////////////////////////////////////////////////////////
//  rlNpMatchingEventGamerLeft
//////////////////////////////////////////////////////////////////////////
rlNpMatchingEventGamerLeft::rlNpMatchingEventGamerLeft()
: rlNpMatchingEvent(RL_NP_MATCHINGEVENT_GAMER_LEFT)
{
}

void
rlNpMatchingEventGamerLeft::Reset(const rlNpRoomInfo& roomInfo, const rlSceNpId& npId)
{
    m_RoomInfo = roomInfo;
    m_NpId = npId;
}

//////////////////////////////////////////////////////////////////////////
//  rlNpRoomInfo
//////////////////////////////////////////////////////////////////////////
rlNpRoomInfo::rlNpRoomInfo()
{
    this->Clear();
}

void
rlNpRoomInfo::Clear()
{
    m_RoomId = RL_NP_INVALID_ROOM_ID;
    m_IsPrivate = false;
}

bool
rlNpRoomInfo::IsValid() const
{
    return RL_NP_INVALID_ROOM_ID != m_RoomId;
}

bool
rlNpRoomInfo::IsPrivate() const
{
    return m_IsPrivate && this->IsValid();
}

u64
rlNpRoomInfo::GetRoomId() const
{
    return m_RoomId;
}

bool
rlNpRoomInfo::Export(void* buf,
                    const unsigned sizeofBuf,
                    unsigned* size) const
{
    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    const bool success = rlVerify(this->IsValid())
                        && bb.SerUns(m_RoomId, 64)
                        && bb.SerBool(m_IsPrivate);

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
rlNpRoomInfo::Import(const void* buf,
                    const unsigned sizeofBuf,
                    unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    const bool success = bb.SerUns(m_RoomId, 64)
                        && bb.SerBool(m_IsPrivate);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

bool
rlNpRoomInfo::operator==(const rlNpRoomInfo& that) const
{
    return this->IsValid()
            && m_RoomId == that.m_RoomId
            && m_IsPrivate == that.m_IsPrivate;
}

bool
rlNpRoomInfo::operator!=(const rlNpRoomInfo& that) const
{
    return !(*this == that);
}

bool
rlNpRoomInfo::operator<(const rlNpRoomInfo& that) const
{
    //Private rooms always compare as less than public rooms.
    return (m_IsPrivate != that.m_IsPrivate)
            ? m_IsPrivate
            : m_RoomId < that.m_RoomId;
}

//private:

bool
rlNpRoomInfo::InitPublic(const u64 roomId)
{
    m_IsPrivate = false;
    m_RoomId = roomId;

    return true;
}

bool
rlNpRoomInfo::InitPrivate()
{
    m_IsPrivate = true;
    return rlCreateUUID(&m_RoomId);
}

//////////////////////////////////////////////////////////////////////////
//  rlNpMatching
//////////////////////////////////////////////////////////////////////////
rlNpMatching::rlNpMatching()
: m_MatchingCtxId(-1u)
, m_State(STATE_IDLE)
, m_StartCount(0)
, m_Initialized(false)
{
#if !__FINAL
    m_MatchingServerIndex = -1;
#endif
}

rlNpMatching::~rlNpMatching()
{
    this->Shutdown();
}

bool 
rlNpMatching::IsInitialized() const
{
    return m_Initialized;
}

unsigned
rlNpMatching::GetMatchingContextId() const
{
    return m_MatchingCtxId;
}

void
rlNpMatching::Cancel(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

void
rlNpMatching::AddDelegate(EventDelegate* dlgt)
{
    if(rlVerify(m_Initialized))
    {
        m_EventDelegator.AddDelegate(dlgt);
    }
}

void
rlNpMatching::RemoveDelegate(EventDelegate* dlgt)
{
    m_EventDelegator.RemoveDelegate(dlgt);
}

#if !__FINAL
void
rlNpMatching::SetMatchingServerIndex(const int index)
{
    m_MatchingServerIndex = index;
}

int
rlNpMatching::GetMatchingServerIndex() const
{
    return m_MatchingServerIndex;
}
#endif  //!__FINAL

bool
rlNpMatching::Init()
{
    rlDebug("rlNpMatching::Init()");

    if(rlVerify(!m_Initialized))
    {
        rlAssert(STATE_IDLE == m_State);
        rlAssert(0 == m_StartCount);

#if !__FINAL
        m_MatchingServerIndex = -1;
#endif

		m_Initialized = true;

#if RSG_ORBIS && (SCE_ORBIS_SDK_VERSION >= 0x00920020u)
		m_Initialized = m_Initialized && rlVerify(sceSysmoduleLoadModule(SCE_SYSMODULE_NP_MATCHING2) == SCE_OK);
#endif

    }
    else
    {
        rlError("Already initialized");
    }

    return m_Initialized;
}

void 
rlNpMatching::Shutdown()
{
    rlDebug("rlNpMatching::Shutdown()");

    if(m_Initialized)
    {
        rlAssert(0 == m_StartCount);

        this->Reset();

        m_EventDelegator.Clear();

        rlAssert(m_EventQueue.empty());
        while(!m_EventQueue.empty())
        {
            NpEvent* npe = m_EventQueue.front();
            m_EventQueue.pop_front();
            NpEvent::DestroyEvent(npe);
        }

        rlAssert(STATE_IDLE == m_State);

        m_Initialized = false;
    }
}

bool
rlNpMatching::Start()
{
    rlAssert(m_StartCount >= 0);

    if(!g_rlNp.IsOnline(g_rlNp.GetPrimaryUserServiceIndex()))
    {
        rlError("Can't start NP matching - not online");
    }
    else if(rlVerify(m_Initialized))
    {
        //It's possible for the start count to be greater than zero
        //but still be in STATE_IDLE.  That will occur if the user signed
        //out (or some other event caused SCE_NP_MATCHING2_CONTEXT_EVENT_StartOver
        //to occur) and Reset() is called.
        if(STATE_IDLE == m_State)
        {
            SceNpMatching2ContextId matchCtxId = 0;

            rtry
            {
                int err;
                const SceNpId* npId = g_rlNp.GetNpId(g_rlNp.GetPrimaryUserServiceIndex());

                rcheck(NULL != npId,
                        catchall,
                       rlError("Failed to get NP ID"));

                rlDebug("Initializing Matching 2...");
#if RSG_PS3
                rcheck(0 <= (err = sceNpMatching2Init2(0, 0, NULL)),
                        catchall,
                        rlNpError("Error calling sceNpMatching2Init2()", err));
#elif RSG_ORBIS
				// TODO: NS - workaround for Orbis failing to initialize Np Matching after the 3rd attempt
				static bool sceNpMatching2InitializedAlready = false;
				if(sceNpMatching2InitializedAlready == false)
				{
					sceNpMatching2InitializedAlready = true;
					// TODO: NS - these initialization values taken from SDK sample
					SceNpMatching2InitializeParameter initParam;
					memset(&initParam, 0, sizeof(initParam));		
					initParam.poolSize = 1 * 1024 * 1024;
					initParam.threadPriority = 500;
					initParam.cpuAffinityMask = SCE_KERNEL_CPUMASK_USER_ALL;		
					initParam.threadStackSize = 32 * 1024;
					initParam.size = sizeof(initParam); 
					rcheck(0 <= (err = sceNpMatching2Initialize(&initParam)),
							catchall,
							rlNpError("Error calling sceNpMatching2Initialize()", err));
				}
#endif

                rlDebug("Creating a Matching 2 context...");
#if RSG_PS3
                rcheck(0 <= (err = sceNpMatching2CreateContext(npId,
                                                                g_rlNp.GetTitleId().GetCommunicationId(),
                                                                g_rlNp.GetTitleId().GetCommunicationPassphrase(),
                                                                &matchCtxId,
                                                                SCE_NP_MATCHING2_CONTEXT_OPTION_USE_ONLINENAME)),
                        Matching2Term,
                        rlNpError("sceNpMatching2CreateContext failed", err));
#elif RSG_ORBIS
				SceNpMatching2CreateContextParam createContextParam;
				memset(&createContextParam, 0, sizeof(createContextParam));
				createContextParam.npId = npId;
				createContextParam.commId = g_rlNp.GetTitleId().GetCommunicationId();
				createContextParam.passPhrase = g_rlNp.GetTitleId().GetCommunicationPassphrase();
				createContextParam.size = sizeof(createContextParam);

                rcheck(0 <= (err = sceNpMatching2CreateContext(&createContextParam, 
															   &matchCtxId)),
                        Matching2Term,
                        rlNpError("sceNpMatching2CreateContext failed", err));
#endif
								   
                rcheck(0 <= (err = sceNpMatching2RegisterContextCallback(
#if RSG_PS3
																		matchCtxId,
#endif
                                                                        &rlNpMatching::NpMatching2ContextCallback,
                                                                        this)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2RegisterContextCallback failed", err));

                rcheck(0 <= (err = sceNpMatching2RegisterRoomEventCallback(matchCtxId,
                                                                            &rlNpMatching::NpMatching2RoomEventCallback,
                                                                            this)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2RegisterRoomEventCallback failed", err));

                rcheck(0 <= (err = sceNpMatching2RegisterRoomMessageCallback(matchCtxId,
                                                                            &rlNpMatching::NpMatching2RoomMessageCallback,
                                                                            this)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2RegisterRoomMessageCallback failed", err));

                rcheck(0 <= (err = sceNpMatching2RegisterLobbyEventCallback(matchCtxId,
                                                                            &rlNpMatching::NpMatching2LobbyEventCallback,
                                                                            this)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2RegisterLobbyEventCallback failed", err));

                rcheck(0 <= (err = sceNpMatching2RegisterLobbyMessageCallback(matchCtxId,
                                                                            &rlNpMatching::NpMatching2LobbyMessageCallback,
                                                                            this)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2RegisterLobbyMessageCallback failed", err));

                rcheck(0 <= (err = sceNpMatching2RegisterSignalingCallback(matchCtxId,
                                                                            &rlNpMatching::NpMatching2SignalingCallback,
                                                                            this)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2RegisterSignalingCallback failed", err));

                rlDebug("Starting the Matching 2 context...");

#if RSG_PS3
                rcheck(0 <= (err = sceNpMatching2ContextStartAsync(matchCtxId, 0)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2ContextStartAsync failed", err));
#elif RSG_ORBIS
				// this function is async on Orbis
                rcheck(0 <= (err = sceNpMatching2ContextStart(matchCtxId, 0)),
                        Matching2DestroyContext,
                        rlNpError("sceNpMatching2ContextStart failed", err));
#endif

                m_MatchingCtxId = matchCtxId;
                m_State = STATE_STARTING_MATCHING_CONTEXT;
            }
            rcatch(Matching2DestroyContext)
            {
                sceNpMatching2DestroyContext(matchCtxId);
                rthrow(Matching2Term,);
            }
            rcatch(Matching2Term)
            {
#if RSG_PS3
                sceNpMatching2Term2();
#elif RSG_ORBIS
				// TODO: NS - workaround for Orbis failing to initialize Np Matching after the 3rd attempt
				//sceNpMatching2Terminate();
#endif
                rthrow(catchall,)
            }
            rcatchall
            {
                rlAssert(STATE_IDLE == m_State);
            }
        }
    }

    const bool success = (this->Started() || this->Starting());

    if(success)
    {
        ++m_StartCount;
    }

    return success;
}

bool
rlNpMatching::Stop()
{
    rlAssert(m_StartCount > 0);

    --m_StartCount;

    if(0 == m_StartCount)
    {
        rlDebug("Stopping the Matching 2 context...");
        this->Reset();
    }

    return true;
}

bool
rlNpMatching::Starting() const
{
    return STATE_STARTING_MATCHING_CONTEXT == m_State;
}

bool
rlNpMatching::Started() const
{
    return STATE_HAVE_MATCHING_CONTEXT == m_State;
}

void
rlNpMatching::Update()
{
    if(m_Initialized)
    {
        switch(m_State)
        {
        case STATE_IDLE:
            break;
        case STATE_STARTING_MATCHING_CONTEXT:
            break;
        case STATE_HAVE_MATCHING_CONTEXT:
#if __ASSERT
            CheckMemInfo();
#endif  //__ASSERT
            break;
        case STATE_ERROR_STARTING_MATCHING_CONTEXT:
            rlError("There was an error starting the Matching 2 context");

            sceNpMatching2DestroyContext(m_MatchingCtxId);

#if RSG_PS3
			sceNpMatching2Term2();
#elif RSG_ORBIS
			// TODO: NS - workaround for Orbis failing to initialize Np Matching after the 3rd attempt
			//sceNpMatching2Terminate();
#endif

            m_State = STATE_IDLE;
            break;
        }

        this->ProcessEvents();
    }
}

//Combine game mode and game type into a single u16
//that will be used as the binary search attribute
//for matchmaking, leaving 8 integer attributes available
//for the game to use.
static void MakeGameModeAndType(const unsigned gameMode,
                                const rlGameType gameType,
                                u16* gameModeAndType)
{
    rlAssert(gameMode <= 0xFF);

    *gameModeAndType = u16(gameMode & 0xFF);
    *gameModeAndType <<= 8;
    *gameModeAndType |= u16(gameType & 0xFF);
}

//Extract the game mode and game type from a u16.
static void ExtractGameModeAndType(unsigned* gameMode,
                                rlGameType* gameType,
                                const u16 gameModeAndType)
{
    *gameMode = (gameModeAndType >> 8) & 0xFF;
    *gameType = rlGameType(gameModeAndType & 0xFF);
}

//Populate NP integer and binary matching attributes.
static int PopulateMatchingAttrs(const rlMatchingAttributes& attrs,
                                SceNpMatching2IntAttr (&intAttrs)[RL_MAX_MATCHING_ATTRS],
                                SceNpMatching2BinAttr* binAttr,
                                u8 (&binAttrData)[SCE_NP_MATCHING2_ROOM_SEARCHABLE_BIN_ATTR_EXTERNAL_MAX_SIZE],
                                const NilAttrHandling nilAttrHandling)
{
    int numIntAttrs = 0;

    rtry
    {
        sysMemSet(intAttrs, 0, sizeof(intAttrs));
        sysMemSet(binAttr, 0, sizeof(*binAttr));

        rverify(attrs.IsValid(), catchall,);

        u16 gameModeAndType = 0;
        MakeGameModeAndType(attrs.GetGameMode(),
                            attrs.GetGameType(),
                            &gameModeAndType);
        *(u16*) binAttrData = gameModeAndType;
        binAttr->id = SCE_NP_MATCHING2_ROOM_SEARCHABLE_BIN_ATTR_EXTERNAL_1_ID;
        binAttr->ptr = binAttrData;
        binAttr->size = sizeof(gameModeAndType);

        for(int i = 0; i < (int) attrs.GetCount(); ++i)
        {
            const u32* val = attrs.GetValueByIndex(i);

            if(!val)
            {
                rverify(nilAttrHandling == NIL_ATTRS_PERMITTED,
                        catchall,
                        rlError("Session attribute %d is nil", i));

                continue;
            }

            intAttrs[numIntAttrs].id =
                i + SCE_NP_MATCHING2_ROOM_SEARCHABLE_INT_ATTR_EXTERNAL_1_ID;
            intAttrs[numIntAttrs].num = *val;

            ++numIntAttrs;
        }
    }
    rcatchall
    {
        numIntAttrs = -1;
    }

    return numIntAttrs;
}

bool
rlNpMatching::CreateRoom(const rlGamerInfo& owner,
                        const unsigned maxPubSlots,
                        const unsigned maxPrivSlots,
                        const rlMatchingAttributes& attrs,
                        rlNpRoomInfo* roomInfo,
                        netStatus* status)
{
    bool success = false;

    rlDebug("Creating room...");

    NpCreateRoomTask* task = NULL;

    roomInfo->Clear();

    rtry
    {
        //Prior to calling CreateRoom() the caller must call Start().
        //The start process might not have finished when CreateRoom()
        //is called.  The same requirements apply to FindRooms().
        //For all other matching functions the start process must
        //have already completed (Started() returns true).
        rverify(this->Started() || this->Starting(),
                catchall,
                rlError("Matching service is not available"));

        rcheck(owner.IsLocal(),
                catchall,
                rlError("Can't create a room - gamer not local"));

        rcheck(owner.IsOnline(),
                catchall,
                rlError("Can't create a room - gamer not online"));

        rverify(attrs.IsValid(),
                catchall,
                rlError("Invalid matching attributes"));

        rverify(attrs.GetCount() <= RL_MAX_MATCHING_ATTRS,
                catchall,
                rlError("Too many matching attributes"));

        rverify(maxPubSlots + maxPrivSlots > 0,
                catchall,
                rlError("Invalid number of slots: %d pub, %d priv", maxPubSlots, maxPrivSlots));

        if(0 == maxPubSlots)
        {
            rlDebug("Room is private (has no public slots) and will not be advertised");

            rverify(roomInfo->InitPrivate(),
                    catchall,
                    rlError("Error initializing room info for private room"));

            if(status){status->SetPending(); status->SetSucceeded();}
        }
        else
        {
            task = rlGetTaskManager()->CreateTask<NpCreateRoomTask>();

            rverify(task,
                    catchall,
                    rlError("Error allocating NpCreateRoomTask"));

            rverify(rlTaskBase::Configure(task,
                                    owner,
                                    maxPubSlots,
                                    maxPrivSlots,
                                    attrs,
                                    roomInfo,
                                    status),
                    catchall,
                    rlError("Error configuring NpCreateRoomTask"));

            rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);
        }

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlNpMatching::JoinRoom(const rlNpRoomInfo& roomInfo,
                        const rlSlotType slotType,
                        rlNpRoomMembers* members,
                        netStatus* status)
{
    rlAssert(roomInfo.IsValid());

    bool success = false;

    rlDebug("Joining room:0x%016" I64FMT "x...", roomInfo.GetRoomId());

    NpJoinRoomTask* task = NULL;

    rtry
    {
        //Prior to calling JoinRoom() the start process must have already
        //completed.
        rverify(this->Started(),
                catchall,
                rlError("Matching service is not available"));

        if(roomInfo.IsPrivate())
        {
            rlDebug("Room is private - performing fake join");

            rverify(!members,
                    catchall,
                    rlError("Can't retrive member info for private rooms"));

            if(status){status->SetPending(); status->SetSucceeded();}
        }
        else
        {
            task = rlGetTaskManager()->CreateTask<NpJoinRoomTask>();

            rverify(task,
                    catchall,
                    rlError("Error allocating NpJoinRoomTask"));

            rverify(rlTaskBase::Configure(task, roomInfo, slotType, members, status),
                    catchall,
                    rlError("Error configuring NpJoinRoomTask"));

            rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);
        }

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlNpMatching::LeaveRoom(const rlNpRoomInfo& roomInfo, netStatus* status)
{
    rlAssert(roomInfo.IsValid());

    bool success = false;

    rlDebug("Leaving room:0x%016" I64FMT "x...", roomInfo.GetRoomId());

    NpLeaveRoomTask* task = NULL;

    rtry
    {
        //Prior to calling LeaveRoom() the start process must have already
        //completed.  Note that we might be calling LeaveRoom() after
        //successfully joining, but then shutting down the network due
        //to sign-out or bad network conditions.  In that case Started()
        //will return false.
        rcheck(this->Started(),
                catchall,
                rlError("Matching service is not available"));

        if(roomInfo.IsPrivate())
        {
            rlDebug("Room is private - performing fake leave");

            if(status){status->SetPending(); status->SetSucceeded();}
        }
        else
        {
			bool bDetached = false;
			if(status != NULL)															
			{	
				task = rlGetTaskManager()->CreateTask<NpLeaveRoomTask>();
			}															
			else														
			{															
				rlFireAndForgetTask<NpLeaveRoomTask>* fafTask = NULL;				
				rlGetTaskManager()->CreateTask(&fafTask);
				task = fafTask;	
				status = &fafTask->m_Status;
				bDetached = true;
			}															
            
            rverify(task,
                    catchall,
                    rlError("Error allocating NpLeaveRoomTask"));

            rverify(rlTaskBase::Configure(task, roomInfo, bDetached, status),
                    catchall,
                    rlError("Error configuring NpLeaveRoomTask"));

            rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);
        }

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlNpMatching::FindRooms(const rlMatchingFilter& filter,
                        rlSessionInfo* results,
                        const unsigned maxResults,
                        unsigned* numResults,
                        netStatus* status)
{
    bool success = false;

    rlDebug("Searching for rooms...");

    NpFindRoomsTask* task = NULL;

    rtry
    {
        //Prior to calling FindRooms() the caller must call Start().
        //The start process might not have finished when FindRooms()
        //is called.  The same requirements apply to CreateRoom().
        //For all other matching functions the start process must
        //have already completed (Started() returns true).
        rverify(this->Started() || this->Starting(),
                catchall,
                rlError("Matching service is not available"));

        rverify(filter.IsValid(),
                catchall,
                rlError("Invalid matching filter"));

        rverify(filter.GetConditionCount() <= RL_MAX_MATCHING_CONDITIONS,
                catchall,
                rlError("%d search conditions exceeds max of %d",
                            filter.GetConditionCount(),
                            RL_MAX_MATCHING_CONDITIONS));

        rverify(filter.GetGameType() != RL_GAMETYPE_INVALID,
                catchall,
                rlError("Invalid game type in matching filter"));

        rverify(filter.GetGameMode() != RL_INVALID_GAMEMODE,
                catchall,
                rlError("Invalid game mode in matching filter"));

        task = rlGetTaskManager()->CreateTask<NpFindRoomsTask>();

        rverify(task,
                catchall,
                rlError("Error allocating NpFindRoomsTask"));

        rverify(rlTaskBase::Configure(task, filter, results, maxResults, numResults, status),
                catchall,
                rlError("Error configuring NpFindRoomsTask"));

        rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlNpMatching::ChangeRoomAttributes(const rlNpRoomInfo& roomInfo,
                                    const rlMatchingAttributes& newAttrs,
                                    netStatus* status)
{
    rlAssert(roomInfo.IsValid());

    bool success = false;

    rlDebug("Changing attributes on room:0x%016" I64FMT "x...",
                roomInfo.GetRoomId());

    NpSetExtRoomDataTask* task = NULL;

    rtry
    {
        //Prior to calling ChangeRoomAttributes() the start process must have
        //already completed.
        rverify(this->Started(),
                catchall,
                rlError("Matching service is not available"));

        rverify(!roomInfo.IsPrivate(),
                catchall,
                rlError("Can't change attributes of a private room"));

        rverify(newAttrs.IsValid(),
                catchall,
                rlError("Invalid matching attributes"));

        rverify(newAttrs.GetCount() <= RL_MAX_MATCHING_ATTRS,
                catchall,
                rlError("Too many attributes"));

        rverify(newAttrs.GetCount() > 0,
                catchall,
                rlError("No attributes to change"));

        task = rlGetTaskManager()->CreateTask<NpSetExtRoomDataTask>();

        rverify(task,
                catchall,
                rlError("Error allocating NpSetExtRoomDataTask"));

        rverify(rlTaskBase::Configure(task, roomInfo, newAttrs, status),
                catchall,
                rlError("Error configuring NpSetExtRoomDataTask"));

        rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

bool
rlNpMatching::SetRoomVisible(const rlNpRoomInfo& roomInfo,
                            const bool visible,
                            netStatus* status)
{
    rlAssert(roomInfo.IsValid());

    bool success = false;

    rlDebug("Setting visibility to %s on room:0x%016" I64FMT "x...",
                visible ? "true" : "false",
                roomInfo.GetRoomId());

    NpSetRoomVisibleTask* task = NULL;

    rtry
    {
        //Prior to calling SetRoomVisible() the start process must have
        //already completed.
        rverify(this->Started(),
                catchall,
                rlError("Matching service is not available"));

        if(roomInfo.IsPrivate())
        {
            rlDebug("Room is private - visibility can't be changed");

            if(status){status->SetPending(); status->SetSucceeded();}
        }
        else
        {
            task = rlGetTaskManager()->CreateTask<NpSetRoomVisibleTask>();

            rverify(task,
                    catchall,
                    rlError("Error allocating NpSetRoomVisibleTask"));

            rverify(rlTaskBase::Configure(task, roomInfo, visible, status),
                    catchall,
                    rlError("Error configuring NpSetRoomVisibleTask"));

            rverify(rlGetTaskManager()->AppendSerialTask(task),catchall,);
        }

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        if(status){status->SetPending(); status->SetFailed();}
    }

    return success;
}


bool
rlNpMatching::GetSessionInfoFromRoom(const rlSessionToken* sessionTokens,
									 int numSessionTokens,
									 rlSessionInfo* sessionInfos,
									 netStatus* status)
{
#if __ASSERT
	for (int i = 0; i < numSessionTokens; ++i)
	{
		rlAssert(sessionTokens[i].IsValid());
	}
#endif

	bool success = false;

#if !__NO_OUTPUT
	for (int i = 0; i < numSessionTokens; ++i)
	{
		rlDebug("Getting session info from room:0x%016" I64FMT "x...",
			sessionTokens[i].m_Value);
	}
#endif

	NpGetSessionInfoFromRoomTask* task = NULL;

	rtry
	{
		rverify(this->Started(),
			catchall,
			rlError("Matching service is not available"));

		task = rlGetTaskManager()->CreateTask<NpGetSessionInfoFromRoomTask>();

		rverify(task,
			catchall,
			rlError("Error allocating NpGetSessionInfoFromRoomTask"));

		rverify(rlTaskBase::Configure(task, sessionTokens, numSessionTokens, sessionInfos, status),
			catchall,
			rlError("Error configuring NpGetSessionInfoFromRoomTask"));

		rverify(rlGetTaskManager()->AddParallelTask(task),catchall,);

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		if(status){status->SetPending(); status->SetFailed();}
	}

	return success;
}

void
rlNpMatching::Reset()
{
    rlDebug("Resetting rlNpMatching...");

    //Clear the event queue
    this->ProcessEvents();

    if(this->HaveMatchingContext())
    {
        int err;

        if(0 > (err = sceNpMatching2ContextStop((SceNpMatching2ContextId) m_MatchingCtxId)))
        {
            rlNpError("Error calling sceNpMatching2ContextStop", err);
        }

        if(0 > (err = sceNpMatching2DestroyContext((SceNpMatching2ContextId) m_MatchingCtxId)))
        {
            rlNpError("Error calling sceNpMatching2DestroyContext", err);
        }

        m_MatchingCtxId = -1u;

#if RSG_PS3
		sceNpMatching2Term2();
#elif RSG_ORBIS
		// TODO: NS - workaround for Orbis failing to initialize Np Matching after the 3rd attempt
// 		if(0 > (err = sceNpMatching2Terminate()))
// 		{
// 			rlNpError("Error calling sceNpMatching2Terminate", err);
// 		}
#endif

        m_State = STATE_IDLE;

        ///DO NOT set m_StartCount to zero here.  Even after a reset
        //we still need to keep track of how many callers have called Start().
    }
}

void
rlNpMatching::ProcessEvents()
{
    rlAssert(m_Initialized);

    //Process events that were queued in the various NP callbacks.

/*#if __ASSERT
    bool showQueueStats = false;

    if(this->HaveMatchingContext())
    {
        SceNpMatching2CbQueueInfo qi = {0};
        int err = sceNpMatching2GetCbQueueInfo(m_MatchingCtxId, &qi);
        if(!rlVerify(err >= 0))
        {
            rlNpError("Error calling sceNpMatching2GetCbQueueInfo", err);
        }
        else
        {
            showQueueStats = (qi.curRequestCbQueueLen > 0)
                            || (qi.curSessionEventCbQueueLen > 0)
                            || (qi.curSessionMsgCbQueueLen > 0);
        }
    }
#endif  //__ASSERT*/

    for(NpEvent* npe = this->NextEvent(); npe; npe = this->NextEvent())
    {
        if(npe->m_ErrorCode < 0)
        {
            rlNpError("NP event error", npe->m_ErrorCode);
        }

        if(NpEvent::EVENT_CONTEXT == npe->m_Type)
        {
            if(NP_SWITCH(SCE_NP_MATCHING2_CONTEXT_EVENT_StartOver,SCE_NP_MATCHING2_CONTEXT_EVENT_START_OVER) == npe->m_EventId)
            {
                rlDebug("Received SCE_NP_MATCHING2_CONTEXT_EVENT_StartOver");
#if !__NO_OUTPUT
#define EVENT_CAUSE_CASE(x) case x: rlDebug("  Cause: "#x")"); break;
                switch(npe->m_EventCause)
                {
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_LEAVE_ACTION);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_KICKOUT_ACTION);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_GRANT_OWNER_ACTION);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_SERVER_OPERATION);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_MEMBER_DISAPPEARED);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_SERVER_INTERNAL);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_CONNECTION_ERROR);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_NP_SIGNED_OUT);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_SYSTEM_ERROR);
                    EVENT_CAUSE_CASE(SCE_NP_MATCHING2_EVENT_CAUSE_CONTEXT_ERROR);
                    default:
                        rlError("  Unknown cause:0x%08x", npe->m_EventCause);
                        break;
                }
#undef EVENT_CAUSE_CASE
#endif  //__NO_OUTPUT

                this->Reset();
            }
            else if(NP_SWITCH(SCE_NP_MATCHING2_CONTEXT_EVENT_Start,SCE_NP_MATCHING2_CONTEXT_EVENT_STARTED) == npe->m_EventId)
            {
                rlAssert(STATE_STARTING_MATCHING_CONTEXT == m_State);

                if(npe->m_ErrorCode < 0)
                {
                    rlNpError("sceNpMatching2ContextStartAsync failed", npe->m_ErrorCode);

                    m_State = STATE_ERROR_STARTING_MATCHING_CONTEXT;
                }
                else
                {
                    rlDebug("Starting Matching 2 context succeeded");
                    m_State = STATE_HAVE_MATCHING_CONTEXT;
                }
            }
            else if(NP_SWITCH(SCE_NP_MATCHING2_CONTEXT_EVENT_Stop,SCE_NP_MATCHING2_CONTEXT_EVENT_STOPPED) == npe->m_EventId)
            {
                rlAssert(STATE_IDLE == m_State);
            }
        }
        else if(NpEvent::EVENT_ROOM == npe->m_Type)
        {
            switch(npe->m_EventId)
            {
            case NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_MemberJoined,SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_JOINED):
            case NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_MemberLeft,SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_LEFT):
                this->HandleMemberJoinedOrLeft(npe);
                break;
            }
        }
        else if(NpEvent::EVENT_ROOM_MESSAGE == npe->m_Type)
        {
        }
        else if(NpEvent::EVENT_LOBBY == npe->m_Type)
        {
        }
        else if(NpEvent::EVENT_LOBBY_MESSAGE == npe->m_Type)
        {
        }
        else if(NpEvent::EVENT_SIGNALING == npe->m_Type)
        {
        }
        else if(NpEvent::EVENT_MATCHING2_RESPONSE == npe->m_Type)
        {
            rlNpMatchingTask* task =
                (rlNpMatchingTask*) rlGetTaskManager()->FindTask(npe->m_TaskId);

            if(task)
            {
                task->m_ReqId = -1u;

#define M2_HANDLE_EVENT(x)\
    case x:\
        rlTaskDebugPtr(task, "handling request callback event("#x")");\
        task->HandleNpEvent(npe)?task->m_ReqStatus.SetSucceeded():task->m_ReqStatus.SetFailed();\
        break;

                switch(npe->m_EventId)
                {
#if RSG_PS3
                M2_HANDLE_EVENT(SCE_NP_MATCHING2_REQUEST_EVENT_GetServerInfo);
#endif
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetWorldInfoList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_WORLD_INFO_LIST));
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SearchRoom, SCE_NP_MATCHING2_REQUEST_EVENT_SEARCH_ROOM));
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_CreateJoinRoom, SCE_NP_MATCHING2_REQUEST_EVENT_CREATE_JOIN_ROOM));
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_JoinRoom, SCE_NP_MATCHING2_REQUEST_EVENT_JOIN_ROOM));
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_LeaveRoom, SCE_NP_MATCHING2_REQUEST_EVENT_LEAVE_ROOM));
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetRoomDataExternalList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_ROOM_DATA_EXTERNAL_LIST));
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SetRoomDataExternal, SCE_NP_MATCHING2_REQUEST_EVENT_SET_ROOM_DATA_EXTERNAL));
                M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SetRoomDataInternal, SCE_NP_MATCHING2_REQUEST_EVENT_SET_ROOM_DATA_INTERNAL));

				case NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetRoomMemberDataExternalList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_ROOM_MEMBER_DATA_EXTERNAL_LIST):
					// This task makes multiple requests internally, so we have to handle it manually here
					rlTaskDebugPtr(task, "handling request callback event(SCE_NP_MATCHING2_REQUEST_EVENT_GetRoomMemberDataExternalList)");
					task->HandleNpEvent(npe);
					break;

                default:
                    rlTaskErrorPtr(task, "Unhandled event:0x%08x", npe->m_EventId);
                    task->m_ReqStatus.SetFailed();
                    break;
                }
#undef M2_HANDLE_EVENT
            }
            else
            {
#define M2_HANDLE_EVENT(x)\
    case x: rlError("No task with id %d found for request callback event("#x")", npe->m_TaskId); break;
                switch(npe->m_EventId)
                {
#if RSG_PS3
                    M2_HANDLE_EVENT(SCE_NP_MATCHING2_REQUEST_EVENT_GetServerInfo);
#endif
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetWorldInfoList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_WORLD_INFO_LIST));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SearchRoom, SCE_NP_MATCHING2_REQUEST_EVENT_SEARCH_ROOM));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_CreateJoinRoom, SCE_NP_MATCHING2_REQUEST_EVENT_CREATE_JOIN_ROOM));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_JoinRoom, SCE_NP_MATCHING2_REQUEST_EVENT_JOIN_ROOM));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_LeaveRoom, SCE_NP_MATCHING2_REQUEST_EVENT_LEAVE_ROOM));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetRoomDataExternalList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_ROOM_DATA_EXTERNAL_LIST));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SetRoomDataExternal, SCE_NP_MATCHING2_REQUEST_EVENT_SET_ROOM_DATA_EXTERNAL));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_SetRoomDataInternal, SCE_NP_MATCHING2_REQUEST_EVENT_SET_ROOM_DATA_INTERNAL));
                    M2_HANDLE_EVENT(NP_SWITCH(SCE_NP_MATCHING2_REQUEST_EVENT_GetRoomMemberDataExternalList, SCE_NP_MATCHING2_REQUEST_EVENT_GET_ROOM_MEMBER_DATA_EXTERNAL_LIST));
                default:
                    break;
                }
#undef M2_HANDLE_EVENT
            }
        }

        NpEvent::DestroyEvent(npe);
    }

/*#if __ASSERT

    //Show the queue usage so we can spot potential problems
    //with not draining the queue.

    if(showQueueStats)
    {
        ShowQueueStats(m_MatchingCtxId);
    }
#endif//__ASSERT*/
}

bool
rlNpMatching::HaveMatchingContext() const
{
    return STATE_HAVE_MATCHING_CONTEXT == m_State;
}

void
rlNpMatching::HandleMemberJoinedOrLeft(const NpEvent* e)
{
    rtry
    {
        rverify(e->m_EventData, catchall, rlError("No event data"));

        rlNpMatchingEvent* npme = NULL;
        rlNpMatchingEventGamerJoined gje;
        rlNpMatchingEventGamerLeft gle;
        rlSceNpId npid;

#if RSG_PS3
		const SceNpMatching2RoomMemberUpdateInfo* info =
			(const SceNpMatching2RoomMemberUpdateInfo*) e->m_EventData;

		npid.Reset(info->roomMemberDataInternal->userInfo.npId);
#elif RSG_ORBIS
		const rlNpMatching2RoomMemberUpdateInfo* info =
			(const rlNpMatching2RoomMemberUpdateInfo*) e->m_EventData;

		npid.Reset(info->m_NpId);
#endif

        rlNpRoomInfo roomInfo;
        roomInfo.InitPublic(e->m_RoomId);

        if(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_MemberJoined,SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_JOINED) == e->m_EventId)
        {
            gje.Reset(roomInfo, npid);
            npme = &gje;
        }
        else if(rlVerify(NP_SWITCH(SCE_NP_MATCHING2_ROOM_EVENT_MemberLeft,SCE_NP_MATCHING2_ROOM_EVENT_MEMBER_LEFT) == e->m_EventId))
        {
            gle.Reset(roomInfo, npid);
            npme = &gle;
        }

        if(npme)
        {
            m_EventDelegator.Dispatch(npme);
        }
    }
    rcatchall
    {
    }
}

bool
rlNpMatching::QueueEvent(NpEvent* event)
{
    SYS_CS_SYNC(m_EqCs);
    m_EventQueue.push_back(event);
    return true;
}

rlNpMatching::NpEvent*
rlNpMatching::NextEvent()
{
    NpEvent* npe = NULL;

    SYS_CS_SYNC(m_EqCs);

    if(!m_EventQueue.empty())
    {
        npe = m_EventQueue.front();
        m_EventQueue.pop_front();
    }

    return npe;
}

}   //namespace rage

#endif	// RSG_NP
