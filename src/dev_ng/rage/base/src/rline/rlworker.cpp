// 
// rline/rlworker.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlworker.h"
#include "rldiag.h"

namespace rage
{

rlWorker::rlWorker()
: m_WakeupSema(0)
, m_WaitSema(0)
, m_ThreadHandle(sysIpcThreadIdInvalid)
, m_Stop(true)
, m_Performing(false)
{
}

rlWorker::~rlWorker()
{
    this->Stop();
}

bool
rlWorker::Start(const char* name,
                const unsigned stackSize,
                const int cpuAffinity,
				bool fastStart)
{
    bool success = false;

    rlAssert(!m_WakeupSema && !m_WaitSema);
    rlAssert(sysIpcThreadIdInvalid == m_ThreadHandle);

    m_WakeupSema = sysIpcCreateSema(0);
    m_WaitSema = sysIpcCreateSema(0);

    if(rlVerify(m_WakeupSema && m_WaitSema))
    {
        rlAssert(m_Stop);
        m_Stop = false;

        m_ThreadHandle =
            sysIpcCreateThread(&rlWorker::Process,
                                this,
                                stackSize,
								PRIO_NORMAL,
                                name,
                                cpuAffinity);

        if(rlVerify(sysIpcThreadIdInvalid != m_ThreadHandle))
        {
			if(!fastStart)
			{
				//Wait for the thread to start.
				sysIpcWaitSema(m_WaitSema);
			}

            success = true;
        }
    }

    if(!success)
    {
        if(m_WakeupSema)
        {
            sysIpcDeleteSema(m_WakeupSema);
            m_WakeupSema = 0;
        }

        if(m_WaitSema)
        {
            sysIpcDeleteSema(m_WaitSema);
            m_WaitSema = 0;
        }
    }

    return success;
}

bool
rlWorker::Stop()
{
    if(sysIpcThreadIdInvalid != m_ThreadHandle)
    {
        rlAssert(!m_Stop);
        rlAssert(m_WakeupSema && m_WaitSema);

        m_Stop = true;
        sysIpcSignalSema(m_WakeupSema);
        //Wait for the thread to end.
        sysIpcWaitSema(m_WaitSema);

        sysIpcWaitThreadExit(m_ThreadHandle);
        m_ThreadHandle = sysIpcThreadIdInvalid;

        sysIpcDeleteSema(m_WakeupSema);
        sysIpcDeleteSema(m_WaitSema);
        m_WakeupSema = m_WaitSema = 0;
    }
    else
    {
        rlAssert(m_Stop);
        rlAssert(!m_WakeupSema && !m_WaitSema);
    }

    return true;
}

void
rlWorker::Wakeup()
{
    rlAssert(m_WakeupSema);

    sysIpcSignalSema(m_WakeupSema);
}

bool
rlWorker::IsRunning() const
{
    return sysIpcThreadIdInvalid != m_ThreadHandle;
}

bool 
rlWorker::IsPerforming() const
{
    SYS_CS_SYNC(m_CsPerforming);
    return m_Performing;
}

//private:

void
rlWorker::Process(void* p)
{
    rlWorker* worker = (rlWorker*) p;

    //Signal the calling thread that we've started.
    sysIpcSignalSema(worker->m_WaitSema);

    while(!worker->m_Stop)
    {
        sysIpcWaitSema(worker->m_WakeupSema);

        if(worker->m_Stop)
        {
            break;
        }

        {
            SYS_CS_SYNC(worker->m_CsPerforming);
            worker->m_Performing = true;
        }

		//@@: location RLWORKER_PROCESS_PERFORM_WORKER
        worker->Perform();

        {
            SYS_CS_SYNC(worker->m_CsPerforming);
            worker->m_Performing = false;
        }
    }

    //Signal the calling thread that we've ended.
    sysIpcSignalSema(worker->m_WaitSema);
}

}   //namespace rage
