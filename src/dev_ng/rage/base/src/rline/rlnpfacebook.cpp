// 
// rline/rlnpfacebook.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include "rlnpfacebook.h"

#if RL_FACEBOOK_ENABLED

#include "diag/seh.h"
#include "rline/rlnp.h"
#include "rline/facebook/rlfacebookcommon.h"

#include <np.h>

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rline_np, facebook)
	#undef __rage_channel
	#define __rage_channel rline_np

	// Threadpool for dealing with Facebook permissions pop up.
	extern sysThreadPool netThreadPool;

	npBasicFbGetLongAccessTokenTask::npBasicFbGetLongAccessTokenTask()
		: m_State(STATE_GET_ACCESS_TOKEN)
		, m_RequestId(0)
	{
	}

	npBasicFbGetLongAccessTokenTask::~npBasicFbGetLongAccessTokenTask()
	{
	}

	bool npBasicFbGetLongAccessTokenTask::Configure(const int userId, const u64 fbAppId, const char* permissions, u32 options, char* token, u64* expiration)
	{
		m_UserId = userId;
		m_FbAppId = fbAppId;
		safecpy(m_Permissions, permissions);
		m_Options = options;
		m_Expiration = expiration;
		m_Token = token;

		// 0 = show UI, 1 = silent
		if (m_Options)
		{
			m_State = STATE_GET_ACCESS_TOKEN;
		}
		else
		{
			m_State = STATE_GET_UI_ACCESS_TOKEN;
		}

		return true;
	}

	void npBasicFbGetLongAccessTokenTask::OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);
		if (STATE_GETTING_ACCESS_TOKEN)
		{
			netThreadPool.CancelWork(m_WorkItem.GetId());
		}
	}

	netTaskStatus npBasicFbGetLongAccessTokenTask::OnUpdate(int* resultCode)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_GET_ACCESS_TOKEN:
			if(WasCanceled())
			{
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				netTaskDebug("Getting fb access token");

				int ret;

				ret = sceNpSnsFacebookCreateRequest();
				if (ret < SCE_OK) 
				{
					rlNpError("sceNpSnsFacebookCreateRequest failed to create request", ret);
					*resultCode = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
					status = NET_TASKSTATUS_FAILED;
					return status;
				}

				m_RequestId = ret;

				SceNpSnsFacebookAccessTokenParam param;
				memset(&param, 0, sizeof(param));
				param.size = sizeof(param);
				param.userId = m_UserId;
				param.fbAppId = m_FbAppId;
				strncpy(param.permissions, m_Permissions, sizeof(param.permissions) - 1);

				memset(&m_FacebookAccessToken, 0, sizeof(m_FacebookAccessToken));

				m_WorkItem.Configure(m_RequestId, param);
				if (!netThreadPool.QueueWork(&m_WorkItem))
				{
					*resultCode = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
					status = NET_TASKSTATUS_FAILED;
				}
				else
				{
					m_State = STATE_GETTING_ACCESS_TOKEN;
				}
			}
			break;

		case STATE_GETTING_ACCESS_TOKEN:
			if(WasCanceled() && !m_WorkItem.Pending())
			{
				netTaskDebug("Canceled - bailing...");
				*resultCode = RL_FB_GET_TOKEN_FAILED_USER_CANCELLED;
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				// wait for work to be done
				if (m_WorkItem.WasCanceled() && !m_WorkItem.Pending())
				{
					*resultCode = RL_FB_GET_TOKEN_FAILED_USER_CANCELLED;
					status = NET_TASKSTATUS_FAILED;
				}
				else if (m_WorkItem.Finished())
				{
					s32 ret = m_WorkItem.GetReturnCode();
					if (ret == SCE_NP_SNS_FACEBOOK_ERROR_ABORTED)
					{
						*resultCode = RL_FB_GET_TOKEN_FAILED_USER_CANCELLED;
						status = NET_TASKSTATUS_FAILED;
					}
					else if (ret == SCE_NP_SNS_FACEBOOK_ERROR_ACCOUNT_NOT_BOUND)
					{
						*resultCode = RL_FB_GET_TOKEN_FAILED_NOT_CONFIGURED;
						status = NET_TASKSTATUS_FAILED;
					}
					else if(ret < SCE_OK)
					{
						rlError("FacebookAccessToken failed. ret = 0x%x\n", ret);
						*resultCode = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
						status = NET_TASKSTATUS_FAILED;
					}
					else
					{
						char* tokenResult = m_WorkItem.GetToken();
						safecpy(m_Token, tokenResult, SCE_NP_SNS_FACEBOOK_ACCESS_TOKEN_LENGTH_MAX);
						rlAssertf(strlen(m_Token) == strlen(tokenResult), "Length of the buffer allocated must be big enough to contain the resulting token.");

						*m_Expiration = m_WorkItem.GetTokenExpiration();

						netTaskDebug("GetFacebookAccessToken succeeded: %s", tokenResult);

						*resultCode = RL_FB_GET_TOKEN_SUCCESS;
						status = NET_TASKSTATUS_SUCCEEDED;
					}
				}
			}
			break;
		case STATE_GET_UI_ACCESS_TOKEN:
			if (g_rlNp.GetCommonDialog().ShowFacebookPermissionDialog(m_UserId, m_FbAppId, m_Permissions))
			{
				m_State = STATE_GETTING_UI_ACCESS_TOKEN;
			}
			else
			{
				*resultCode = RL_FB_GET_TOKEN_FAILED_UNKNOWN;
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_GETTING_UI_ACCESS_TOKEN:
			{
				if (WasCanceled())
				{
					status = NET_TASKSTATUS_FAILED;
				}
				else if (g_rlNp.GetCommonDialog().GetStatus() == SCE_COMMON_DIALOG_STATUS_FINISHED)
				{
					SceNpSnsFacebookDialogResult* result = g_rlNp.GetCommonDialog().GetFacebookResult();
					if (result->result == SCE_COMMON_DIALOG_RESULT_OK)
					{
						safecpy(m_Token, result->accessTokenResult.accessToken, SCE_NP_SNS_FACEBOOK_ACCESS_TOKEN_LENGTH_MAX);
						*m_Expiration = result->accessTokenResult.expiration;
						*resultCode = RL_FB_GET_TOKEN_SUCCESS;
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else if (result->result == SCE_COMMON_DIALOG_RESULT_USER_CANCELED)
					{
						*resultCode = RL_FB_GET_TOKEN_FAILED_USER_CANCELLED;
						status = NET_TASKSTATUS_FAILED;
					}
					else
					{
						*resultCode = result->result;
						status = NET_TASKSTATUS_FAILED;
					}
				}
			}
			break;
		}

		return status;
	}

	void npBasicFbGetLongAccessTokenTask::OnCleanup()
	{
		netTask::OnCleanup();
	}

	bool npBasicFbGetLongAccessTokenWorkItem::Configure(int requestId, SceNpSnsFacebookAccessTokenParam tokenParam)
	{
		m_RequestId = requestId;
		m_TokenParam = tokenParam;
		m_Token[0] = '\0';
		m_Expiration = 0;
		return true;
	}

	void npBasicFbGetLongAccessTokenWorkItem::DoWork()
	{
		SceNpSnsFacebookAccessTokenResult result;
		sysMemSet(&result, 0x00, sizeof(result));

		if (WasCanceled())
			return;

		m_ReturnCode = sceNpSnsFacebookGetAccessToken(m_RequestId, &m_TokenParam, &result);
		sceNpSnsFacebookDeleteRequest (m_RequestId);

		if (WasCanceled())
			return;

		if (m_ReturnCode < SCE_OK)
		{
			rlError("sceNpSnsFacebookGetAccessToken : 0x%x\n", m_ReturnCode);
		}

		safecpy(m_Token, result.accessToken, SCE_NP_SNS_FACEBOOK_ACCESS_TOKEN_LENGTH_MAX);
		m_Expiration = result.expiration;
	}

} // namespace rage

#endif // RL_FACEBOOK_ENABLED
#endif // RSG_ORIBS