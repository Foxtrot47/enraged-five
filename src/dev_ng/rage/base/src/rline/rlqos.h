// 
// rline/rlqos.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLQOS_H
#define RLINE_RLQOS_H

#include "rlnotifier.h"
#include "rlsession.h"
#include "net/message.h"
#include "net/transaction.h"
#include "net/tunneler.h"

namespace rage
{

class netConnectionManager;

struct rlMsgQosProbeRequest
{
    NET_MESSAGE_DECL(rlMsgQosProbeRequest, RL_MSG_QOS_PROBE_REQUEST);

    void Reset(const rlSessionInfo& sessionInfo)
    {
        m_SessionInfo = sessionInfo;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUser(msg.m_SessionInfo);
    }

    rlSessionInfo m_SessionInfo;
};

struct rlMsgQosProbeResponse
{
    NET_MESSAGE_DECL(rlMsgQosProbeResponse, RL_MSG_QOS_PROBE_RESPONSE);

    rlMsgQosProbeResponse()
    : m_UserDataSize(0) 
    {
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUns(msg.m_UserDataSize, datBitsNeeded<RL_MAX_QOS_USER_DATA_SIZE>::COUNT) &&
               rlVerifyf(msg.m_UserDataSize <= RL_MAX_QOS_USER_DATA_SIZE, "m_UserDataSize[%u] overflow", msg.m_UserDataSize) &&
               (msg.m_UserDataSize ? bb.SerBytes(msg.m_UserData, msg.m_UserDataSize) : true);
    }

    u8 m_UserData[RL_MAX_QOS_USER_DATA_SIZE];
    u16 m_UserDataSize;
};

//PURPOSE
//  This class is used to query quality of service (QOS) information from
//  a peer.
//  For a peer to respond it must have registered the session identified
//  by the session info passed to QueryPeer().
//
//  QueryPeer() is asynchronous and returns immediately.  Register a notification
//  listener (using AddNotifyHandler()) to receive QOS notifications.
//  Periodically call Update() until the rlQOS object notifies its listeners
//  that the operation either succeeded or failed.
class rlQos
{
public:

    enum NotifyCode
    {
        NOTIFY_INVALID      = -1,

        //The current operation was cancelled.
        NOTIFY_CANCELLED,

        //The QOS query succeeded.  The notification parameter will
        //be a pointer to a QosResult structure.
        NOTIFY_QOS_QUERY_SUCCEEDED,
        //The QOS query failed.  The notification parameter will be NULL.
        NOTIFY_QOS_QUERY_FAILED,
    };

    //PURPOSE
    //  A pointer to a QosResult instance is passed as the notification
    //  parameter with the NOTIFY_QOS_QUERY_SUCCEEDED notification.
    struct QosResult
    {        
        unsigned m_MinRtTimeMS;     //Minimum roundtrip time in milliseconds        
        unsigned m_MedRtTimeMS;     //Median roundtrip time in milliseconds        
        unsigned m_UpBitsPerSec;    //Average upstream bandwidth per second        
        unsigned m_DnBitsPerSec;    //Average downstream bandwidth per second

        u8 m_UserData[RL_MAX_QOS_USER_DATA_SIZE];
        u16 m_UserDataSize;

        QosResult()
        : m_MinRtTimeMS(0)
        , m_MedRtTimeMS(0)
        , m_UpBitsPerSec(0)
        , m_DnBitsPerSec(0)
        , m_UserDataSize(0)
        {
        }
    };

    rlQos();
    ~rlQos();

    //PURPOSE
    //  Begin an asynchronous QOS query with a session host.
    //PARAMS
    //  sessionInfo - Info for the session which the query will reference.
    //  peerAddress - Address of peer to query.
    //  netMode     - NETMODE_LAN or NETMODE_ONLINE.
    //  cxnMgr      - Connection manager used for network transport
    //  channelId   - Id of channel on which to send probes.
    //NOTES
    //  This method is asynchronous and returns immediately.  When the operation
    //  completes the proper notification will be dispatched.
    bool QueryPeer(const rlSessionInfo& sessionInfo,
                    const netPeerAddress& peerAddr,
                    const rlNetworkMode netMode,
                    netConnectionManager* cxnMgr,
                    const unsigned channelId);

    //PURPOSE
    //  Call periodically (once per frame) to update pending QOS
    //  queries.
    void Update();

    //PURPOSE
    //  Returns true if an operation is pending.
    bool IsPending() const;

    //PURPOSE
    //  Cancels any pending operations and resets this instance.
    void Cancel();

    //PURPOSE
    //  Implement the rlNotifier interface.
    RL_NOTIFIER(rlQos, QosResult);

private:
    bool SendProbe();
    void ReportResults();
    void Finish(const bool success);

private:
    enum { NUM_PROBES = 4 };

    enum State
    {
        STATE_INVALID = -1,
        STATE_OPEN_TUNNEL,
        STATE_OPENING_TUNNEL,
        STATE_PROBING
    };

    State           m_State;
    rlSessionInfo   m_SessionInfo;
    netPeerAddress  m_PeerAddr;
    rlNetworkMode   m_NetMode;

    void OnResponse(netTransactor* transactor,
                    netResponseHandler* handler,
                    const netResponse* resp);

    enum eProbeResult
    {
        PROBE_RESULT_UNKNOWN,
        PROBE_RESULT_PENDING,
        PROBE_RESULT_ANSWERED,
        PROBE_RESULT_TIMED_OUT,
        PROBE_RESULT_ERROR
    };
    
    struct ProbeInfo
    {
        netResponseHandler  m_RespHandler;
        unsigned            m_SendTime;
        unsigned            m_Rtt;
        eProbeResult        m_Result;
        u16                 m_UserDataSize;
        u8                  m_UserData[RL_MAX_QOS_USER_DATA_SIZE];

        ProbeInfo() { Reset(); }
        void Reset();
    };

    netConnectionManager* m_CxnMgr;
    unsigned m_ChannelId;
    netTransactor m_Transactor;

    netTunnelRequest    m_TunnelRqst;
    ProbeInfo           m_Probes[NUM_PROBES];
    int                 m_NumProbesSent;
    netTimeStep         m_TimeStep;
    netTimeout          m_Timeout;

    netStatus m_Status;
};

}   //namespace rage

#endif  //RLINE_RLQOS_H
