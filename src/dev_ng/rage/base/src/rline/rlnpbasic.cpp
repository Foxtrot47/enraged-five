// 
// rline/rlnpbasic.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
 
#if RSG_NP
#include "rlnpbasic.h"
#include "diag/seh.h"
#include "net/task.h"

#include "parsercore/utils.h"

#include "rl.h"
#include "rlnp.h"
#include "rlnptitleid.h"
#include "rline/rltask.h"

#include "rlfriend.h"
#include "rlsessioninfo.h"

#include "rline/rlnpfacebook.h"
#include "rline/rlpresence.h"

#include "string/string.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/threadpool.h"
#include "system/timer.h"

#include <np.h>

#include "rline/ros/rlros.h"
#include "rline/rlnpwebapi.h"
#include "rline/rlnpingamemessagemgr.h"

namespace rage
{

PARAM(rlnpuseoldinvites, "If present use the old NP invite technique (not the gui)");
PARAM(rlnpjointarget, "Fake a bootable invite event from a PSN Account ID i.e. -rlnpjointarget=58592752597");
PARAM(rlnpjoinsession, "Fake a bootable invite to a session id i.e. -rlnpjoinsession=ABCDE-FGHIJ-KLMNO");
PARAM(rlnpjoinsessiontarget, "Sets the target for an fake bootable invite/session -rlnpjoinsessiontarget=58592752597");
PARAM(netDisableWebApiInvites, "Disables web api invites");
PARAM(rlnpinvitedelayms, "Sets a delay in milliseconds");

RAGE_DEFINE_SUBCHANNEL(rline_np, basic)
#undef __rage_channel
#define __rage_channel rline_np_basic

CompileTimeAssert(RL_PRESENCE_MAX_BUF_SIZE <= SCE_NP_BASIC_MAX_PRESENCE_SIZE);

// Threadpool for dealing with Facebook permissions pop up.
extern sysThreadPool netThreadPool;

//------------------------------------------------------------------------------
//class rlNpBasic
//------------------------------------------------------------------------------
rlNpBasic::rlNpBasic()
: m_PresenceBlobSize(0)
, m_Initialized(false)
, m_PresenceDirty(false)
,m_WebApiInvitesDirty(false)
,m_RefreshingWebApiInvites(false)
,m_WebApiInviteAccepted(false)
,m_WebApiJoinSession(false)
,m_NumWebApiInvitations(0)
,m_PresenceBlobLastWriteTime(0)
,m_PresenceStatusLastWriteTime(0)
,m_GettingWebApiJoinSessionDetails(false)
,m_GettingWebApiJoinTargetDetails(false)
{
    m_PresenceStatusStr[0] = '\0';
	m_WebApiAcceptedInvitationId[0] = '\0';
	sysMemSet(&m_InvitationDetails, 0, sizeof(m_InvitationDetails));
}

rlNpBasic::~rlNpBasic()
{
    Shutdown();
}

bool
rlNpBasic::Init()
{
    rlDebug("rlNpBasic::Init()");

    rtry
    {
        rcheck(!m_Initialized, 
               catchall, 
               rlError("Already initialized"));

        m_Initialized = true;
        
        m_NpDlgt.Bind(this, &rlNpBasic::OnNpEvent);
        g_rlNp.AddDelegate(&m_NpDlgt);
    }
    rcatchall
    {
        Shutdown();
    }

    return m_Initialized;
}

void 
rlNpBasic::Shutdown()
{
    rlDebug("rlNpBasic::Shutdown()");

    if(m_Initialized)
    {
        g_rlNp.RemoveDelegate(&m_NpDlgt);

		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			Reset(i);
		}

        m_Initialized = false;
    }
}

void
rlNpBasic::Update(const int localGamerIndex)
{
    SYS_CS_SYNC(m_Cs);

    rlAssert(m_Initialized);
    
    if(g_rlNp.IsLoggedIn(localGamerIndex))
    {
        UpdateSetPresence(localGamerIndex);
    }

	UpdateWebApiInvites(localGamerIndex);
}

bool 
rlNpBasic::IsInitialized() const
{
    return m_Initialized;
}

bool 
rlNpBasic::IsPresenceInitialized() const 
{ 
	return true;
}

void
rlNpBasic::Reset(const int /*localGamerIndex*/)
{
    SYS_CS_SYNC(m_Cs);

    rlDebug("Reset()");

    //Presence
    m_PresenceBlobSize = 0;
    m_PresenceStatusStr[0] = '\0';
    m_PresenceDirty = false;
	m_PresenceBlobLastWriteTime = 0;
	m_PresenceStatusLastWriteTime = 0;
}

void 
rlNpBasic::OnNpEvent(rlNp* /*np*/, const rlNpEvent* evt)
{
    if(RLNP_EVENT_ONLINE_STATUS_CHANGED == evt->GetId())
    {
        rlNpEventOnlineStatusChanged* msg = (rlNpEventOnlineStatusChanged*)evt;

        if(!msg->IsSignedOnline())
        {
            rlDebug("NP went offline for index %d, resetting", msg->m_LocalGamerIndex);
            Reset(msg->m_LocalGamerIndex);
        }
        else
        {
			// If the main gamer logs in.
			if (msg->m_LocalGamerIndex == rlPresence::GetActingUserIndex())
			{
				// clear the previous block list, download a new one.
				ClearBlocklist();
				RefreshBlockList(msg->m_LocalGamerIndex);
				SetInviteListDirty();
			}
        }
    }
}

void rlNpBasic::HandleInGameMessage(int32_t /*libCtxId*/, const SceNpPeerAddress* /*pTo*/, const SceNpPeerAddress *pFrom, const SceNpInGameMessageData *pMessage, void * /*pUserArg*/)
{
	const u8* p = (const u8*)pMessage->data;
	const u8 msgId = *p++;

	switch(msgId)
	{
	case MSG_GENERAL:
		if(rlVerify(pFrom))
		{
			rlSceNpOnlineId onlineId; 
			onlineId.Reset(pFrom->onlineId);

			rlNpEventMsgReceived msgEvent(&onlineId, p, pMessage->dataSize - (p-(u8*)pMessage->data));
			g_rlNp.DispatchEvent(&msgEvent);
		}
		break;
	default:
		break;
	}
}

void rlNpBasic::AddPlayerToBlocklist(const rlGamerHandle& player)
{
	// Early out if a player is already on the block list.
	if (IsPlayerOnBlockList(player))
		return;

	// Add to the list.
	if (rlVerify(!m_UserBlockList.IsFull()))
	{
		m_UserBlockList.Append() = player;
	}
}  

eNpSendMsgResult rlNpBasic::SendMsg(const int localGamerIndex, const rlGamerHandle& recipient, const void* data, const unsigned size)
{
    rlAssert(size <= SCE_NP_IN_GAME_MESSAGE_DATA_SIZE_MAX);
	rlAssert(m_Initialized);

	if (g_rlNp.GetInGameMsgMgr().SendMessage(localGamerIndex, recipient, data, size) == SCE_OK)
	{
		return RLNP_SENDMSG_SUCCESS;
	}
	else
	{
		return RLNP_SENDMSG_ERROR;
	}
}

eNpSendMsgResult rlNpBasic::SendGuiMsg(const int localGamerIndex, const rlGamerHandle* recipients, const unsigned numRecipients, const char* subject, const char* message)
{
	rlAssert(m_Initialized);
	rlAssert((strlen(subject) + strlen(message) + 1) < SCE_NP_IN_GAME_MESSAGE_DATA_SIZE_MAX);

	u8 buf[SCE_NP_IN_GAME_MESSAGE_DATA_SIZE_MAX];
	sysMemSet(buf, 0, sizeof(buf));

	unsigned numBytes = 0;
	buf[numBytes++] = MSG_GENERAL;

	sysMemCpy(buf, subject, strlen(subject));
	numBytes += strlen(subject);

	sysMemCpy(buf, message, strlen(message));
	numBytes += strlen(message);

	if (g_rlNp.GetInGameMsgMgr().SendMessage(localGamerIndex, recipients, numRecipients, buf, numBytes) == SCE_OK)
	{
		return RLNP_SENDMSG_SUCCESS;
	}
	else
	{
		return RLNP_SENDMSG_ERROR;
	}
}

eNpSendMsgResult rlNpBasic::SendMsgToFriend(const int localGamerIndex, const rlFriend& f, const void* data, const unsigned size)
{
	rlGamerHandle hGamer;
	f.GetGamerHandle(&hGamer);

	if (g_rlNp.GetInGameMsgMgr().SendMessage(localGamerIndex, hGamer, data, size) == SCE_OK)
	{
		return RLNP_SENDMSG_SUCCESS;
	}
	else
	{
		return RLNP_SENDMSG_ERROR;
	}
}

// rate-limited
eNpSendMsgResult rlNpBasic::SendGeneralMsg(const int localGamerIndex, const rlGamerHandle& recipient, const void* data, const unsigned size)
{
	rlAssert(m_Initialized);

	u8 buf[SCE_NP_IN_GAME_MESSAGE_DATA_SIZE_MAX];
	sysMemSet(buf, 0, sizeof(buf));

	unsigned numBytes = 0;

	buf[numBytes] = MSG_GENERAL;
	numBytes++;

	sysMemCpy(&buf[numBytes], &size, sizeof(unsigned));
	numBytes += sizeof(unsigned);

	sysMemCpy(&buf[numBytes], data, size);
	numBytes += size;

	rlDebug("Sending general msg (%u bytes)", numBytes);

	if (g_rlNp.GetInGameMsgMgr().SendMessage(localGamerIndex, recipient, buf, numBytes) == SCE_OK)
	{
		return RLNP_SENDMSG_SUCCESS;
	}
	else
	{
		return RLNP_SENDMSG_ERROR;
	}
}

eNpSendMsgResult rlNpBasic::SendInviteMsg(
	const int localGamerIndex, 
	const rlSceNpSessionId* sessionId, 
	const rlGamerHandle* aInvitees,
	const unsigned nInvitees, 
	const rlSessionInfo& sessionInfo, 
	const char* salutation)
{
	rlAssert(sessionInfo.IsValid());
	rlAssert(m_Initialized);

	eNpSendMsgResult result = RLNP_SENDMSG_ERROR;

	rlNpWebApiPostInvitationParam param;
	sysMemSet(&param, 0, sizeof(param));
	param.m_SessionInfo = &sessionInfo;
	param.m_SessionId = sessionId;

    bool bUseAccountIds = (nInvitees > 0) && (aInvitees[0].GetNpAccountId() != RL_INVALID_NP_ACCOUNT_ID);
    if(bUseAccountIds)
    {
        rlSceNpAccountId accountIds[RL_MAX_GAMERS_PER_SESSION];
        for(unsigned i = 0; i < nInvitees; ++i)
        {
            accountIds[i] = aInvitees[i].GetNpAccountId();
        }
        param.m_AccountIds = accountIds;
        param.m_NumAccountIds = nInvitees;
    }
    else
    {
        rlSceNpOnlineId onlineIds[RL_MAX_GAMERS_PER_SESSION];
        for(unsigned i = 0; i < nInvitees; ++i)
        {
            onlineIds[i] = aInvitees[i].GetNpOnlineId();
        }
        param.m_OnlineIds = onlineIds;
        param.m_NumOnlineIds = nInvitees;
    }

	param.m_Message = salutation;

	if (g_rlNp.GetWebAPI().PostInvitation(localGamerIndex, &param))
	{
		result = RLNP_SENDMSG_SUCCESS;
	}
	else
	{
		result = RLNP_SENDMSG_ERROR;
	}

	return result;
}

bool rlNpBasic::ShowAcceptMultiplayerInviteUi(const int localGamerIndex) const
{
	int userId = g_rlNp.GetUserServiceId(localGamerIndex);
	return g_rlNp.GetCommonDialog().ReceiveInvitation(userId);
}

bool rlNpBasic::AddFriend(const SceNpId& UNUSED_PARAM(id), const char* UNUSED_PARAM(msg), netStatus* UNUSED_PARAM(status))
{
	SYS_CS_SYNC(m_Cs);
	rlAssertf(false, "Not supported on this platform.");
	return false;
}

bool 
rlNpBasic::AddFriend(const SceNpOnlineId& UNUSED_PARAM(id),
					 const char* UNUSED_PARAM(msg),
					 netStatus* UNUSED_PARAM(status))
{
	SYS_CS_SYNC(m_Cs);
	rlAssertf(false, "Not supported on this platform.");
	return false;
}

void
rlNpBasic::UpdateSetPresence(const int ORBIS_ONLY(localGamerIndex))
{
	SYS_CS_SYNC(m_Cs);

#if RSG_ORBIS
	if (m_PresenceDirty)
	{
		u32 currentTime = sysTimer::GetSystemMsTime();
		if ((currentTime - m_PresenceStatusLastWriteTime) > PRESENCE_STATUS_RATE_LIMIT_MS)
		{
			bool bSuccess = false;

			if (m_PresenceStatusStr[0] == '\0')
			{
				bSuccess = g_rlNp.GetWebAPI().DeleteGameStatus(localGamerIndex);
			}
			else
			{
				bSuccess = g_rlNp.GetWebAPI().PutGameStatus(localGamerIndex, m_PresenceStatusStr);
			}

			m_PresenceStatusLastWriteTime = currentTime;
			m_PresenceDirty = false;

			rlDebug("UpdateSetPresence - < rlNpEventSetPresenceData > : '%s'", m_PresenceStatusStr);

			rlNpEventSetPresenceData e(bSuccess, m_PresenceBlob, m_PresenceBlobSize, m_PresenceStatusStr);
			g_rlNp.DispatchEvent(&e);
		}
	}

	if (m_PresenceBlobDirty)
	{
		u32 currentTime = sysTimer::GetSystemMsTime();
		if ((currentTime - m_PresenceBlobLastWriteTime) > PRESENCE_BLOB_RATE_LIMIT_MS)
		{
			bool bSuccess = false;

			if (m_PresenceBlobSize == 0)
			{
				bSuccess =g_rlNp.GetWebAPI().DeleteGameData(localGamerIndex);
			}
			else
			{
				bSuccess = g_rlNp.GetWebAPI().PutGameDataBlob(localGamerIndex, m_PresenceBlob, m_PresenceBlobSize);
			}

			m_PresenceBlobLastWriteTime = currentTime;
			m_PresenceBlobDirty = false;

			rlDebug("UpdateSetPresence - < rlNpEventSetPresenceData > : '%s'", m_PresenceStatusStr);

			rlNpEventSetPresenceData e(bSuccess, m_PresenceBlob, m_PresenceBlobSize, m_PresenceStatusStr);
			g_rlNp.DispatchEvent(&e);
		}
	}
#else
	#pragma message("TODO: [PROSPERO] UpdateSetPresence : Presence vs. PlayerSession")
#endif // RSG_ORBIS

#if RSG_PROSPERO
	// Update block list retrieval
	if (m_BlocklistStatus.Finished())
	{
		if (m_BlocklistStatus.Succeeded())
		{
			rlDebug1("Block list request succeeded");

			rlNpEventBlocklistRetrieved e;
			g_rlNp.DispatchEvent(&e);
		}

		m_BlocklistStatus.Reset();
	}
#endif
}

#if RL_FACEBOOK_ENABLED
bool rlNpBasic::GetFacebookAccessToken(const u64 fbAppId, const char* permissions, u32 options, char* token, u64* expiration, netStatus* status)
{
	SYS_CS_SYNC(m_Cs);

	rlAssert(m_Initialized);
	rlAssert(fbAppId > 0 && token && expiration && status && !status->Pending());
	rlDebug("rlNpBasic::GetFacebookAccessToken(%s, %d)", permissions, options);

	npBasicFbGetLongAccessTokenTask* task;

	rtry
	{
		int userId = g_rlNp.GetUserServiceId(rlPresence::GetActingUserIndex());
		rverify(netTask::Create(&task, status), catchall, rlError("Error allocating npBasicFbGetLongAccessTokenTask"));
		rverify(task->Configure(userId, fbAppId, permissions, options, token, expiration),  catchall, rlError("Error configuring npBasicFbGetLongAccessTokenTask"));
		rverify(netTask::Run(task), catchall, rlError("Error scheduling npBasicFbGetLongAccessTokenTask"));
	}
	rcatchall
	{
		netTask::Destroy(task);
		return false;
	}

	return true;
}
#endif // RL_FACEBOOK_ENABLED

bool
rlNpBasic::SetPresenceBlob(const void* blob, const unsigned size)
{
    SYS_CS_SYNC(m_Cs);
    rlAssert(m_Initialized);

    if(size > sizeof(m_PresenceBlob))
    {
        rlAssertf(false, "SetPresenceBlob: Blob is too big(size=%d, max=%" SIZETFMT "d)", size, sizeof(m_PresenceBlob));
        return false;
    }
	else if (size == 0)
	{
		rlDebug("SetPresenceBlob :: Clearing presence blob");
	}

    if((m_PresenceBlobSize != size) ||
       (size && blob && memcmp(m_PresenceBlob, blob, size)))
    {
        m_PresenceBlobSize = size;
        if(size)
        {
            rlAssert(blob);
            sysMemCpy(m_PresenceBlob, blob, size);
        }

		m_PresenceBlobDirty = true;
    }

    return true;
}

bool
rlNpBasic::SetPresenceStatusString(const char* statusStr)
{
    SYS_CS_SYNC(m_Cs);

    if(rlVerify(m_Initialized) && rlVerify(statusStr))
    {
        if(strlen(statusStr) >= sizeof(m_PresenceStatusStr))
        {
            rlDebug("SetPresenceStatusString: string is too long(length=%" SIZETFMT "d, max=%" SIZETFMT "d)", strlen(statusStr), sizeof(m_PresenceStatusStr)-1);
            return false;
        }

        if(0 != strcmp(m_PresenceStatusStr, statusStr))
        {
			safecpy(m_PresenceStatusStr, statusStr);
			rlDebug("SetPresenceStatusString - m_PresenceStatusStr: '%s'", m_PresenceStatusStr);
            m_PresenceDirty = true;
        }

        return true;
    }

    return false;
}

void rlNpBasic::ClearBlocklist()
{
	m_UserBlockList.Reset();
}

void rlNpBasic::RefreshBlockList(int localGamerIndex)
{
	ClearBlocklist();
	g_rlNp.GetWebAPI().GetBlockList(localGamerIndex);
}

bool
rlNpBasic::IsPlayerOnBlockList(const rlGamerHandle& player) const
{
	for (int i=0; i < m_UserBlockList.GetCount(); ++i)
	{
		if (m_UserBlockList[i] == player)
		{
			return true;
		}
	}

	return false;
}

void rlNpBasic::HandleWebApiInviteAccepted(char * invitationId)
{
	rlDebug("HandleWebApiInviteAccepted, InvitationId: %s", invitationId);
	m_WebApiInviteAccepted = true;
	safecpy(m_WebApiAcceptedInvitationId, invitationId);
	SetInviteListDirty();
}

void rlNpBasic::HandleWebApiJoinSession(const rlSceNpSessionId& sessionId, const rlSceNpAccountId& target, const rlSceNpOnlineId& onlineId, const rlNpGameIntentMemberType memberType)
{
	rlDebug("HandleWebApiJoinSession, SessionId: %s, Target: %" I64FMT "u, MemberType: %d", sessionId.data, target, memberType);

	m_WebApiJoinSession = true;
	m_WebApiJoinSessionId = sessionId;
	m_WebApiJoinTargetId = target;
	m_WebApiJoinTargetOnlineId = onlineId;
	m_WebApiJoinMemberType = memberType;

	// retrieve the player invite list - all game intent joins arrive without context for whether it's an invite or a join
	// so we need to check the invite list to know whether we can consume private slots or not
	SetInviteListDirty();
}

void rlNpBasic::HandleWebApiJoinTarget(const rlSceNpAccountId target, const rlSceNpOnlineId& onlineId)
{
	rlDebug("HandleWebApiJoinTarget, Target: %" I64FMT "u", target);
	m_WebApiJoinTarget = true;
	m_WebApiJoinTargetId = target; 
	m_WebApiJoinTargetOnlineId = onlineId;
	m_WebApiJoinMemberType = rlNpGameIntentMemberType::Undefined;
}

void rlNpBasic::SetInviteListDirty()
{
	if(PARAM_netDisableWebApiInvites.Get())
	{
		rlDebug("SetInviteListDirty :: Disabled");
		return;
	}

	if(!m_WebApiInvitesDirty)
	{
		rlDebug("SetInviteListDirty");
		m_WebApiInvitesDirty = true;
	}
}

bool rlNpBasic::IsInviteBeingProcessed() const
{
	return m_WebApiJoinSession || m_GettingWebApiJoinSessionDetails || m_WebApiJoinTarget || m_GettingWebApiJoinTargetDetails;
}

void rlNpBasic::UpdateWebApiInvites(const int localGamerIndex)
{
#if !__FINAL && !__PROFILE
	const char* npJoinTarget = nullptr;
	static bool bLaunchedTargetEvent = false;
	if (!bLaunchedTargetEvent && PARAM_rlnpjointarget.Get(npJoinTarget))
	{
		// Wait for online
		if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && g_rlNp.IsOnline(localGamerIndex) && rlRos::IsOnline(localGamerIndex))
		{
			rlDebug("UpdateWebApiInvites :: CommandLine :: Processing -rlnpjointarget (%s)", npJoinTarget);

			rlSceNpAccountId accountId = (rlSceNpAccountId)parUtils::StringToInt64(npJoinTarget);

			// create a fake online ID using the account id as hex.
			rlSceNpOnlineId onlineId;
			formatf(onlineId.data, "%" I64FMT "X", accountId);

			HandleWebApiJoinTarget(accountId, onlineId);
			bLaunchedTargetEvent = true;
		}
	}

	static bool bLaunchedSessionEvent = false;
	const char * sessioninviter;
	const char * sessionid;
	if (!bLaunchedSessionEvent && PARAM_rlnpjoinsession.Get(sessionid) && PARAM_rlnpjoinsessiontarget.Get(sessioninviter))
	{
		// Wait for online
		if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && g_rlNp.IsOnline(localGamerIndex))
		{
			rlDebug("UpdateWebApiInvites :: CommandLine :: Processing -rlnpjoinsession (%s) and target (%s)", sessionid, sessioninviter);
			
			// create session id from command line
			rlSceNpSessionId session;
			safecpy(session.data, sessionid);

			// parse account ID from command line
			rlSceNpAccountId accountId = (rlSceNpAccountId)parUtils::StringToInt64(sessioninviter);

			// create a fake online ID using the account id as hex.
			rlSceNpOnlineId onlineId;
			formatf(onlineId.data, "%" I64FMT "X", accountId);

			HandleWebApiJoinSession(session, accountId, onlineId, rlNpGameIntentMemberType::Player);
			bLaunchedSessionEvent = true;
		}
	}
#endif

	// If WebAPI invites are dirty, fire off a call to get the invitations list
	if (m_WebApiInvitesDirty && RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		if(g_rlNp.GetNpAuth().HasNpResult(localGamerIndex) && !g_rlNp.GetNpAuth().IsNpAvailable(localGamerIndex))
		{
			rlDebug("UpdateWebApiInvites :: GetInvitations :: NP Unavailable");
			m_WebApiInvitesDirty = false;

			// fire event
			rlNpEventInviteRejectedNpUnavailable e;
			g_rlNp.DispatchEvent(&e);
		}
		else if(g_rlNp.IsOnline(localGamerIndex))
		{
			// Wait for the current invite task to finish
			if (!m_WebApiInviteStatus.Pending())
			{
				rlDebug("UpdateWebApiInvites :: GetInvitations :: Retrieving invites...");
				
				// Fire off request
				g_rlNp.GetWebAPI().GetInvitations(localGamerIndex, &m_InvitationDetails[0], RL_MAX_WEB_API_INVITES, &m_NumWebApiInvitations, &m_WebApiInviteStatus);
				m_RefreshingWebApiInvites = true;
				m_WebApiInvitesDirty = false;
			}
		}
	}

	if (m_RefreshingWebApiInvites)
	{
		// Check GetInviteList success/failure
		if (!m_WebApiInviteStatus.Pending())
		{
			rlDebug("UpdateWebApiInvites :: GetInvitations :: %s", m_WebApiInviteStatus.Succeeded() ? "Succeeded" : "Failed");
			
			if (m_WebApiInviteStatus.Succeeded())
			{
				// We don't want to rely on invitation sorting order from Sony
				for (int i = 0; i < RL_MAX_WEB_API_INVITES; i++)
				{
					// Check invite for validity
					if (m_InvitationDetails[i].invitationId[0] != '\0')
					{
						rlDebug("\tInvite [%d]: From: %" I64FMT "u [%s], InvitationId: %s, NpSessionId: %s, Token: 0x%016" I64FMT "x, Received: %s, Expired: %s",
							i,
							m_InvitationDetails[i].accountId,
							m_InvitationDetails[i].onlineId.data,
							m_InvitationDetails[i].invitationId,
							m_InvitationDetails[i].session.sessionId.IsValid() ? m_InvitationDetails[i].session.sessionId.data : "Invalid",
							m_InvitationDetails[i].session.info.GetToken().m_Value,
							m_InvitationDetails[i].receivedDate,
							m_InvitationDetails[i].expired ? "True" : "False");

						// skip expired invites
						if(m_InvitationDetails[i].expired)
							continue; 

						rlNpEventInviteReceived e(m_InvitationDetails[i].accountId, &m_InvitationDetails[i].onlineId, &m_InvitationDetails[i].session.info, m_InvitationDetails[i].message);
						g_rlNp.DispatchEvent(&e);
					}
				}
			}

			m_WebApiInviteStatus.Reset();
			m_RefreshingWebApiInvites = false;
		}
	}

	// Invite accepted by system UI, wait for online
	if (m_WebApiInviteAccepted && g_rlNp.IsOnline(localGamerIndex))
	{
		// Wait for web API invites to no longer be dirty
		if (!m_WebApiInvitesDirty && !m_RefreshingWebApiInvites)
		{
			bool bFound = false;

			for (int i = 0; i < RL_MAX_WEB_API_INVITES; i++)
			{
				if (!stricmp(m_WebApiAcceptedInvitationId, m_InvitationDetails[i].invitationId))
				{
					rlDebug("UpdateWebApiInvites :: InviteAccepted :: Index: %d, From: %" I64FMT "u [%s], InvitationId: %s, NpSessionId: %s, SessionToken: Token: 0x%016" I64FMT "x, Received: %s",
						i,
						m_InvitationDetails[i].accountId,
						m_InvitationDetails[i].onlineId.data,
						m_InvitationDetails[i].invitationId,
						m_InvitationDetails[i].session.sessionId.data,
						m_InvitationDetails[i].session.info.GetToken().m_Value,
						m_InvitationDetails[i].receivedDate);

					rlNpEventInviteAccepted e(m_InvitationDetails[i].accountId, &m_InvitationDetails[i].onlineId, &m_InvitationDetails[i].session.info, m_InvitationDetails[i].message);
					g_rlNp.DispatchEvent(&e);
					bFound = true;
					break;
				}
			}

			rlAssertf(bFound, "UpdateWebApiInvites :: InviteAccepted :: Invite not found with InvitationId: %s", m_WebApiAcceptedInvitationId);
			m_WebApiInviteAccepted = false;
		}
	}

	if(m_WebApiJoinSession && RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
	{
#if RSG_OUTPUT
		static unsigned delaytime = 0;
		int delay = 0;

		if (delaytime == 0 && PARAM_rlnpinvitedelayms.Get(delay) && delay > 0)
		{
			delaytime = sysTimer::GetSystemMsTime() + delay;
		}

		if (sysTimer::GetSystemMsTime() < delaytime)
		{
		}
		else
#endif
		if(g_rlNp.GetNpAuth().HasNpResult(localGamerIndex) && !g_rlNp.GetNpAuth().IsNpAvailable(localGamerIndex))
		{
			rlDebug("UpdateWebApiInvites :: JoinSession :: NP Unavailable");
			m_WebApiJoinSession = false;

			// fire event
			rlNpEventInviteRejectedNpUnavailable e;
			g_rlNp.DispatchEvent(&e);
		}
		else if(g_rlNp.IsOnline(localGamerIndex) && !m_WebApiInvitesDirty && !m_RefreshingWebApiInvites)
		{
			// Wait for the current join session task to finish
			if (!m_WebApiJoinSessionStatus.Pending())
			{
				rlDebug("UpdateWebApiInvites :: JoinSession :: Retrieving session details...");

				// Fire off request
				g_rlNp.GetWebAPI().GetChangeableSessionData(localGamerIndex, m_WebApiJoinSessionId, &m_WebApiJoinSessionInfo, &m_WebApiJoinSessionStatus);
				m_GettingWebApiJoinSessionDetails = true;
				m_WebApiJoinSession = false;
			}
		}
	}

	if (m_GettingWebApiJoinSessionDetails)
	{
		// Check GetInviteList success/failure
		if (!m_WebApiJoinSessionStatus.Pending())
		{
			if (m_WebApiJoinSessionStatus.Succeeded())
			{
				rlDebug("UpdateWebApiInvites :: JoinSession :: %s", m_WebApiJoinSessionStatus.Succeeded() ? "Succeeded" : "Failed");
				
				if (m_WebApiJoinSessionInfo.IsValid())
				{
					rlDebug("UpdateWebApiInvites :: JoinSession :: Target: %" I64FMT "u [%s], SessionToken: Token: 0x%016" I64FMT "x",
						m_WebApiJoinTargetId,
						m_WebApiJoinTargetOnlineId.data,
						m_WebApiJoinSessionInfo.GetToken().m_Value);

					rlNpEventJoinSession e(&m_WebApiJoinSessionInfo, m_WebApiJoinTargetId, &m_WebApiJoinTargetOnlineId, m_WebApiJoinMemberType);
					g_rlNp.DispatchEvent(&e);
				}
				else
				{
					rlError("UpdateWebApiInvites :: JoinSession :: Invalid session info!");
				}			
			}

			m_WebApiJoinSessionStatus.Reset();
			m_GettingWebApiJoinSessionDetails = false;
		}
	}

	if(m_WebApiJoinTarget && RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && g_rlNp.IsOnline(localGamerIndex))
	{
		// Wait for the current join session task to finish
		if (!m_WebApiJoinTargetStatus.Pending())
		{
			rlGamerHandle gh;
			gh.ResetNp(g_rlNp.GetEnvironment(), m_WebApiJoinTargetId, &m_WebApiJoinTargetOnlineId);

			rlDebug("UpdateWebApiInvites :: JoinTarget :: Target: %" I64FMT "u", m_WebApiJoinTargetId);

			rlPresenceQuery::GetSessionByGamerHandle(localGamerIndex, &gh, 1, &m_WebApiJoinTargetQuery, 1, &m_WebApiJoinTargetNumResults, &m_WebApiJoinTargetStatus);
			m_GettingWebApiJoinTargetDetails = true;
			m_WebApiJoinTarget = false;
		}
	}

	if (m_GettingWebApiJoinTargetDetails)
	{
		// Check GetInviteList success/failure
		if (!m_WebApiJoinTargetStatus.Pending())
		{
			if (m_WebApiJoinTargetStatus.Succeeded())
			{
				if (m_WebApiJoinTargetNumResults > 0)
				{
					rlDebug("UpdateWebApiInvites :: JoinTarget :: Target: %" I64FMT "u [%s], SessionToken: Token: 0x%016" I64FMT "x",
						m_WebApiJoinTargetId,
						m_WebApiJoinTargetOnlineId.data,
						m_WebApiJoinTargetQuery.m_SessionInfo.GetToken().m_Value);

					rlNpEventJoinSession e(&m_WebApiJoinTargetQuery.m_SessionInfo, m_WebApiJoinTargetId, &m_WebApiJoinTargetOnlineId, rlNpGameIntentMemberType::Player);
					g_rlNp.DispatchEvent(&e);
				}
				else
				{
					rlError("UpdateWebApiInvites :: JoinTarget :: No result!");
				}			
			}

			m_WebApiJoinTargetNumResults = 0;
			m_WebApiJoinTargetQuery.Clear();
			m_WebApiJoinTargetStatus.Reset();
			m_GettingWebApiJoinTargetDetails = false;
		}
	}
}

}   //namespace rage

#endif  //RSG_NP
