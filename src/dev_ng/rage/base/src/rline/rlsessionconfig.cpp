// 
// rline/rlsessionconfig.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsessionconfig.h"

#include "data/bitbuffer.h"
#include "rldiag.h"

namespace rage
{

rlSessionConfig::rlSessionConfig()
{
    this->Clear();
}

void
rlSessionConfig::Clear()
{
    m_NetworkMode = RL_NETMODE_INVALID;
    m_MaxPublicSlots = RL_MAX_GAMERS_PER_SESSION;
    m_MaxPrivateSlots = 0;
    m_Attrs.Clear();
    m_CreateFlags = 0;
    m_PresenceFlags = 0;
    m_SessionId = 0;
    m_ArbCookie = 0;
}

void
rlSessionConfig::Reset(const rlNetworkMode netMode,
                       const unsigned numPublicSlots,
                       const unsigned numPrivateSlots,
                       const rlMatchingAttributes& attrs,
                       const unsigned createFlags,
                       const unsigned presenceFlags,
                       const u64 sessionId,
                       const u64 arbCookie)
{
    //Only online games can be invitable.
    rlAssert(RL_NETMODE_ONLINE == netMode
            ||!(presenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE));
	//Only online games can have join via presence.
	rlAssert(RL_NETMODE_ONLINE == netMode
            ||!(presenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE));
    rlAssert(attrs.IsValid());

    this->Clear();

    m_NetworkMode = netMode;
    m_MaxPublicSlots = numPublicSlots;
    m_MaxPrivateSlots = numPrivateSlots;
    m_Attrs = attrs;
    m_CreateFlags = createFlags;
    m_PresenceFlags = presenceFlags;
    m_SessionId = sessionId;
    m_ArbCookie = arbCookie;
}

bool
rlSessionConfig::Export(void* buf,
                        const unsigned sizeofBuf,
                        unsigned* size) const
{
    unsigned numBytes;

    if(m_Attrs.Export(buf, sizeofBuf, &numBytes))
    {
        datBitBuffer bb;
        bb.SetReadWriteBytes(&((u8*)buf)[numBytes], sizeofBuf - numBytes);

		// remove local flags
		unsigned nCreateFlags = m_CreateFlags & ~RL_SESSION_CREATE_FLAG_LOCAL_MASK;

		if(bb.WriteUns(m_NetworkMode, datBitsNeeded<RL_NETMODE_NUM_MODES>::COUNT)
            && bb.WriteUns(m_MaxPublicSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
            && bb.WriteUns(m_MaxPrivateSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
            && bb.WriteUns(nCreateFlags, RL_SESSION_CREATE_FLAG_NUM_FLAGS)
            && bb.WriteUns(m_PresenceFlags, RL_SESSION_PRESENCE_FLAG_NUM_FLAGS)
            && bb.WriteUns(m_SessionId, 64)
            && bb.WriteUns(m_ArbCookie, 64))
        {
            numBytes += bb.GetNumBytesWritten();
        }
        else
        {
            numBytes = 0;
        }
    }

    if(size){*size = numBytes;}
    return numBytes > 0;
}

bool
rlSessionConfig::Import(const void* buf,
                        const unsigned sizeofBuf,
                        unsigned* size)
{
    unsigned numBytes;

    if(m_Attrs.Import(buf, sizeofBuf, &numBytes))
    {
        datBitBuffer bb;
        bb.SetReadOnlyBytes(&((const u8*)buf)[numBytes], sizeofBuf - numBytes);

        if(bb.ReadUns(m_NetworkMode, datBitsNeeded<RL_NETMODE_NUM_MODES>::COUNT)
            && bb.ReadUns(m_MaxPublicSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
            && bb.ReadUns(m_MaxPrivateSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
            && bb.ReadUns(m_CreateFlags, RL_SESSION_CREATE_FLAG_NUM_FLAGS)
            && bb.ReadUns(m_PresenceFlags, RL_SESSION_PRESENCE_FLAG_NUM_FLAGS)
            && bb.ReadUns(m_SessionId, 64)
            && bb.ReadUns(m_ArbCookie, 64))
        {
            numBytes += bb.GetNumBytesRead();
        }
        else
        {
            numBytes = 0;
        }
    }

    if(size){*size = numBytes;}
    return numBytes > 0;
}

bool rlSessionConfig::IsPresenceEnabled() const
{
	return (m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED) == 0;
}

bool rlSessionConfig::IsMatchmakingEnabled() const
{
	return (m_CreateFlags & RL_SESSION_CREATE_FLAG_MATCHMAKING_DISABLED) == 0;
}

bool rlSessionConfig::IsMatchmakingAdvertisingEnabled() const
{
	return (m_CreateFlags & RL_SESSION_CREATE_FLAG_MATCHMAKING_ADVERTISE_DISABLED) == 0;
}

}   //namespace rage
