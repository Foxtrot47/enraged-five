// 
// rline/rlnptitleid.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNPTITLEID_H
#define RLINE_RLNPTITLEID_H

#if RSG_NP

#include <np.h>

namespace rage
{

class rlNpTitleId
{
public:
    //PARAMS
	//  titleId				- Optional NP titleId for NP Web API on Orbis
	//  titleSecret			- Optional NP titleSecret for NP Web API on Orbis
    //  commId              - Optional id for communications including:
    //                        Friends list
    //                        Presence
    //                        Score Ranking
    //                        Signaling (P2P communication)
    //  commSig             - Communication signature (needed for trophies)
    //  commPassphrase      - Optional authentication string for NP scoring,
    //                        matching 2, title user storage.
    //  serviceId           - Optional id used for 3rd party authentication
    //                        of NP users.
    //                        Usually of the form ABCDEF-GHIJKLMNO_00
    //                        (19 characters).
    //NOTES
    //  The each of the ids and the pass phrase must be requested from
    //  Sony by using their title registration web form.
    rlNpTitleId(
				const SceNpCommunicationId* commId,
                const SceNpCommunicationSignature* commSig,
                const SceNpCommunicationPassphrase* commPassphrase,
#if RSG_ORBIS
				const SceNpTitleId* titleId,
				const SceNpTitleSecret* titleSecret
#else
				const char* ticketingServiceId,
                const char* serviceId
#endif
				);

    //PURPOSE
    //  Returns the communication id.
    const SceNpCommunicationId* GetCommunicationId() const;

    //PURPOSE
    //  Returns the communication signature.
    const SceNpCommunicationSignature* GetCommunicationSignature() const;

    //PURPOSE
    //  Returns the NP communication anti-cheat passphrase.
    const SceNpCommunicationPassphrase* GetCommunicationPassphrase() const;

#if RSG_ORBIS
	//PURPOSE
	//  Returns the SCE NP title id.
	const SceNpTitleId* GetSceNpTitleId() const;

	//PURPOSE
	//  Returns the SCE NP title secret.
	const SceNpTitleSecret* GetSceNpTitleSecret() const;
#else
    //PURPOSE
    //  Service ID used to obtain NP tickets, which allows third parties (like SCS) to authenticate the user.
    //  This is specified separately from the "normal" service ID because in practice one region
    //  (typically the US) does ticketing for all regions, so you must specify that region's service ID
    //  as the ticketing service ID.
    const char* GetTicketingServiceId() const;

    //PURPOSE
    //  Regional service ID used for non-ticketing purposes.  This can be the same as the
    //  ticketing service ID, but doesn't have to be (and probably won't outside of the US region).
    const char* GetServiceId() const;
#endif

private:
    SceNpCommunicationId m_CommId;
    SceNpCommunicationSignature m_CommSig;
    SceNpCommunicationPassphrase m_CommPassphrase;

#if RSG_ORBIS
	SceNpTitleId m_SceNpTitleId;
	SceNpTitleSecret m_SceNpTitleSecret;

	bool m_HaveTitleId              : 1;
	bool m_HaveTitleSecret          : 1;
	bool m_HaveCommId               : 1;
	bool m_HaveCommSig              : 1;
	bool m_HaveCommPassphrase       : 1;
#else
    char m_TicketingServiceId[32];
    char m_ServiceId[32];

	bool m_HaveCommId               : 1;
	bool m_HaveCommSig              : 1;
	bool m_HaveCommPassphrase       : 1;
	bool m_HaveTicketingServiceId   : 1;
	bool m_HaveServiceId            : 1;
#endif

};

}

#endif //RSG_NP

#endif  //RLINE_RLNPTITLEID_H
