// 
// rline/rlprofanitycheck.cpp
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#include "rlprofanitycheck.h"

#include "rline/rldiag.h"

#if RSG_DURANGO
#include "durango/rlxbl_interface.h"
#include "durango/private/rlxblprofanitycheck.h"
#include "rline/rlxbl.h"
#endif  //RSG_DURANGO

#if RSG_ORBIS
#include "rlnpprofanitycheck.h"
#endif  //RSG_ORBIS

#include "net/task.h"
#include "rlhttptask.h"
#include "socialclub/rlsocialclub.h"
#include "ugc/rlugc.h"
#include "parser/treenode.h"

namespace rage
{

	RAGE_DEFINE_SUBCHANNEL(rline, profanitycheck);
#undef __rage_channel
#define __rage_channel rline_profanitycheck


	///////////////////////////////////////////////////////////////////////////////
	//  rlProfanityCheck
	///////////////////////////////////////////////////////////////////////////////

	class ComposedProfanityCheckTask : public netTask
	{

	public:


		NET_TASK_DECL(ComposedProfanityCheckTask);
		NET_TASK_USE_CHANNEL(rline_profanitycheck);

		ComposedProfanityCheckTask() 
			: m_requestState(RS_NONE)
		    , m_ugcCheck(false)
		{;}

		bool Configure(const int localGamerIndex
			,const char* text
			,char* profaneWord
            ,unsigned profaneWordBufferSize
            ,bool* passedProfanityCheck
			,unsigned* numProfaneWord
			,bool ugcCheck)
		{
			m_localGamerIndex = localGamerIndex;
			safecpy(m_testedString, text);
			m_profaneWord = profaneWord;
			m_profaneWordBufferSize = profaneWordBufferSize;
            m_passedProfanityCheck = passedProfanityCheck;
			m_numProfaneWord = numProfaneWord;
			m_ugcCheck = ugcCheck;

#if RSG_DURANGO
			m_requestState = RS_FIRST_PARTY_CHECK;
			return g_rlXbl.GetProfanityCheckManager()->ProfanityCheck(m_localGamerIndex, m_testedString, m_profaneWord, m_profaneWordBufferSize, m_numProfaneWord, &m_internalStatus);
#else
			m_requestState = RS_SCS_CHECK;

            if(m_ugcCheck)
            {
                return rlUgc::CheckText(m_localGamerIndex, NULL, m_testedString, NULL, m_profaneWord, NULL, &m_internalStatus);  // check online profanity filter
            }
            else
            {
                return rlSocialClub::CheckText(m_localGamerIndex, m_testedString, RLSC_LANGUAGE_UNKNOWN, m_profaneWord, &m_internalStatus);  // check online profanity filter
            }
#endif //RSG_DURANGO
		}

		virtual netTaskStatus OnUpdate(int* UNUSED_PARAM(resultCode))
		{
			netTaskStatus status = NET_TASKSTATUS_PENDING;

			if(!m_internalStatus.Pending())
			{
				if(m_internalStatus.Failed())
				{
					if(m_requestState == RS_SCS_CHECK) // Social Club failed profanity check
                    {
                        *m_passedProfanityCheck = false;
                        status = NET_TASKSTATUS_SUCCEEDED;
                        if(strlen(m_profaneWord) != 0) // Setting the profane string if there's one available
                        {
                            *m_numProfaneWord = 1; 
                            rlDebug("rlProfanityCheck succeeded with profane word found by SCS, testedString='%s'    profaneWord='%s'.", m_testedString, m_profaneWord);
                        }
                        else
                        {
                            *m_numProfaneWord = 0; 
                            rlDebug("rlProfanityCheck succeeded with profane word found by SCS, testedString='%s' but not profane word returned.", m_testedString);
                        }
						return status;
					}
					rlError("rlProfanityCheck failed due to task failure, testedString='%s'.", m_testedString);
					status = NET_TASKSTATUS_FAILED;
				}

                if(m_internalStatus.Canceled())
                {
                    rlError("rlProfanityCheck failed due to task cancel");
                    *m_passedProfanityCheck = false;
                    status = NET_TASKSTATUS_FAILED;
                }

				if(m_internalStatus.Succeeded() && m_requestState == RS_FIRST_PARTY_CHECK)
				{
					if(*m_numProfaneWord > 0)
					{
						// First party profanity check found a profane string, stop here.
                        *m_passedProfanityCheck = false;
						status = NET_TASKSTATUS_SUCCEEDED;
						rlDebug("rlProfanityCheck succeeded with profane word found by 1st party, testedString='%s'    profaneWord='%s'.", m_testedString, m_profaneWord);
						return status;
					}
					m_internalStatus.Reset();
					bool result = true;
					
					if(m_ugcCheck)
					{
						result = rlUgc::CheckText(m_localGamerIndex, NULL, m_testedString, NULL, m_profaneWord, NULL, &m_internalStatus);  // check online profanity filter
					}
					else
					{
						result = rlSocialClub::CheckText(m_localGamerIndex, m_testedString, RLSC_LANGUAGE_UNKNOWN, m_profaneWord, &m_internalStatus);  // check online profanity filter
					}

					if(result)
					{
						m_requestState = RS_SCS_CHECK;
					}
					else
					{
						rlError("rlProfanityCheck failed due to SCS failure, testedString='%s'.", m_testedString);
						status = NET_TASKSTATUS_FAILED;
					}

				}
				else if(m_internalStatus.Succeeded() && m_requestState == RS_SCS_CHECK)
				{
					rlDebug("rlProfanityCheck succeeded, no profane words in testedString='%s'.", m_testedString);
                    *m_passedProfanityCheck = true;
					status = NET_TASKSTATUS_SUCCEEDED;
				}
			}

			return status;
		}

		virtual void OnCancel()
		{
			if(m_requestState == RS_SCS_CHECK)
			{
				rlSocialClub::Cancel(&m_internalStatus);
			}
			else m_internalStatus.SetCanceled();
		}

	private:

		enum RequestState
		{
			RS_NONE,
			RS_FIRST_PARTY_CHECK,
			RS_SCS_CHECK,
			RS_FINISHED
		};

		RequestState m_requestState;
		netStatus m_internalStatus;

		int        m_localGamerIndex;
		char       m_testedString[1024];
		char*      m_profaneWord;
		unsigned   m_profaneWordBufferSize;
        bool*      m_passedProfanityCheck;
		unsigned*  m_numProfaneWord;
		bool	   m_ugcCheck;
	};



	bool
		rlProfanityCheck::ProfanityCheck(const int localGamerIndex
		,const char* text
		,char* profaneWord
        ,unsigned profaneWordBufferSize
        ,bool* passedProfanityCheck
		,unsigned* numProfaneWord
		,bool ugcCheck
		,netStatus* status)
	{
		bool success = false;
		ComposedProfanityCheckTask* task;
		if(!netTask::Create(&task, status)
			|| !task->Configure(localGamerIndex, text, profaneWord, profaneWordBufferSize, passedProfanityCheck, numProfaneWord, ugcCheck)
			|| !netTask::Run(task))
		{
			netTask::Destroy(task);
		}
		else
		{
			rlDebug("rlProfanityCheck started, testedString='%s'.", text);
			success = true;
		}

		if(!success && status)
		{
			status->SetPending();
			status->SetFailed();
		}

		return success;
	}



    bool rlProfanityCheck::ExtractProfaneWord(const parTreeNode* node, char* profaneWordBuffer, int profaneWordBufferLength)
    {
        if(parTreeNode* nodeResult = node->FindChildWithNameIgnoreNs("Result"))
        {
            if(parTreeNode* nodeMatches = nodeResult->FindChildWithNameIgnoreNs("Matches"))
            {
                if(parTreeNode* nodeMatch = nodeMatches->FindChildWithNameIgnoreNs("Match"))
                {
                    const char* word = rlHttpTaskHelper::ReadString(nodeMatch, NULL, "word");
                    if(word)
                    {
                        safecpy(profaneWordBuffer, word, profaneWordBufferLength);
                        return true;
                    }
                }
            }
            else
            {
                // In some special cases, the server will return a Profane code but not a Matches list
                // Still succeed in this case, but no profane word will be available
                int count = 0;
                if(rlHttpTaskHelper::ReadInt(count, nodeResult, "Count", NULL))
                {
                    return count == 0;
                }
                else 
                {
                    netError("Node 'Count' not found");
                }
            }
        }
        else 
        {
            netError("Node 'Result' not found");
        }
        return false;
    }

}   //namespace rage

//eof

