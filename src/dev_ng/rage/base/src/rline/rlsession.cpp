// 
// rline/rlsession.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
 
//FIXME (KB) - Test the case where a guest fails arbitration registration
//             and they must be removed from the session.

//FIXME (KB) - Handle CREATE_INVITES_DISABLED and SetInvitable() on
//             non-xbox platforms.

//FIXME (KB) - When hosting do an automatic join

#include "rlsession.h"
#include "rlpresence.h"
#include "rlsessionfinder.h"
#include "rltask.h"
#include "ros/rlros.h"
#include "scmatchmaking/rlscmatchmaking.h"
#include "data/bitbuffer.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/nethardware.h"
#include "net/packet.h"
#include "net/task.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "system/interlocked.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "system/memory.h"
#include "system/memops.h"

#include "rltitleid.h"

#if RSG_DURANGO
#include "system/xtl.h"
#include "durango/rlxbl_interface.h"
#elif RSG_ORBIS
#include <np.h>
#include <libnetctl.h>
#include "rlnp.h"
#endif

#if RSG_ORBIS
#define orbis_switch(orbis,others) orbis
#else
#define orbis_switch(orbis,others) others
#endif

namespace rage
{

extern const char* netSocketErrorString( const int errorCode );

RAGE_DEFINE_SUBCHANNEL(rline, session)
#undef __rage_channel
#define __rage_channel rline_session

#if !__NO_OUTPUT
u64
rlSession::GetSessionId(const rlSession* s)
{
    return s->IsActivated() ? s->GetId() : 0;
}

u64
rlSession::GetSessionId(const rlSession::Cmd* cmd)
{
    return cmd->m_Session->IsActivated() ? cmd->m_Session->GetId() : 0;
}

u64
rlSession::GetSessionId(const void*)
{
    return 0;
}

const rlSessionToken
rlSession::GetSessionToken(const rlSession* s)
{
    return s->IsActivated()
            ? s->GetSessionInfo().GetToken()
            : rlSessionToken::INVALID_SESSION_TOKEN;
}

const rlSessionToken
rlSession::GetSessionToken(const rlSession::Cmd* cmd)
{
    return cmd->m_Session->IsActivated()
            ? cmd->m_Session->GetSessionInfo().GetToken()
            : rlSessionToken::INVALID_SESSION_TOKEN;
}

const rlSessionToken
rlSession::GetSessionToken(const void*)
{
    return rlSessionToken::INVALID_SESSION_TOKEN;
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "0x%016" I64FMT "x/0x%016" I64FMT "x: " fmt, rlSession::GetSessionId(this), rlSession::GetSessionToken(this).m_Value

#endif  //__NO_OUTPUT

extern const rlTitleId* g_rlTitleId;
extern netSocket* g_rlSkt;

static inline bool MakeSessionId(u64* sessionId)
{
    return rlCreateUUID(sessionId);
}

#if RSG_PC || RSG_NP
static inline bool MakeSessionToken(rlSessionToken* token)
{
    return rlCreateUUID(&token->m_Value);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//  rlSession::Data
///////////////////////////////////////////////////////////////////////////////
rlSession::Data::Data()
{
#if RSG_DURANGO
    m_hSession = 0;
#endif

    this->Clear();
}

void
rlSession::Data::Clear()
{
    m_State = STATE_DORMANT;
    m_LocalOwner.Clear();
    m_SessionInfo.Clear();
    m_Config.Clear();
	m_MatchId.Clear();
	m_HostPeerId = 0;
#if RSG_DURANGO
    rlAssert(!m_hSession);
#endif
}

bool
rlSession::Data::Reset(const rlGamerInfo& localOwner,
						const u64 hostPeerId,
                        const rlSessionInfo* sessionInfo,
                        const rlNetworkMode netMode,
                        const int numPublicSlots,
                        const int numPrivateSlots,
                        const rlMatchingAttributes& attrs,
                        const unsigned createFlags,
                        const unsigned presenceFlags,
                        const u64 sessionId,
                        const u64 arbCookie)
{
    this->Clear();

    rlAssert(numPublicSlots >= 0 &&
            numPrivateSlots >= 0 &&
            numPublicSlots + numPrivateSlots <= RL_MAX_GAMERS_PER_SESSION);

    rlAssert(attrs.IsValid());

    bool success = false;

    rtry
    {
        const int ownerIndex = localOwner.GetLocalIndex();

        rverify(ownerIndex >= 0 && ownerIndex < RL_MAX_LOCAL_GAMERS,
                 catchall,
                 rlError("Invalid gamer index for owner"));

        const bool isHost = !sessionInfo;

        if(isHost)
        {
            rlAssert(!sessionId);
            rlAssert(!arbCookie);
        }
        else
        {
            rlAssert(localOwner.GetPeerInfo().GetPeerAddress().GetPeerId() != sessionInfo->GetHostPeerAddress().GetPeerId());
            rlAssert(sessionId);

            m_SessionInfo = *sessionInfo;
			m_HostPeerId = hostPeerId;
        }

        m_LocalOwner = localOwner;

        //When we host a session we need to create a unique session id.
        //You might think that on xbox we can use the sessionID member
        //of XSESSION_INFO, but we can't because it changes during a host
        //migration.  We need a session id that is unique and persists for
        //the life of the session.

        u64 mySessionId;
        if(isHost)
        {
            rverify(MakeSessionId(&mySessionId),
                    catchall,
                    rlError("Error creating session ID"));
        }
        else
        {
            mySessionId = sessionId;
        }

        m_Config.Reset(netMode,
                        numPublicSlots,
                        numPrivateSlots,
                        attrs,
                        createFlags,
                        presenceFlags,
                        mySessionId,
                        arbCookie);

		//Log out details of the new config
		OUTPUT_ONLY(LogDetails());

        success = true;
    }
    rcatchall
    {
        this->Clear();
    }

    return success;
}

bool
rlSession::Data::Validate() const
{
    const bool isValid =
        rlVerify(m_LocalOwner.IsValid())
        && rlVerify(m_SessionInfo.IsValid()
                        || RL_NETMODE_OFFLINE == m_Config.m_NetworkMode)
        && rlVerify(RL_NETMODE_INVALID != m_Config.m_NetworkMode)
        && rlVerify(m_Config.m_MaxPublicSlots <= RL_MAX_GAMERS_PER_SESSION)
        && rlVerify(m_Config.m_MaxPrivateSlots <= RL_MAX_GAMERS_PER_SESSION)
        && rlVerify(m_Config.m_MaxPublicSlots + m_Config.m_MaxPrivateSlots > 0)
        && rlVerify(m_Config.m_MaxPublicSlots + m_Config.m_MaxPrivateSlots <= RL_MAX_GAMERS_PER_SESSION)
        && rlVerify(m_Config.m_Attrs.GetCount() <= RL_MAX_MATCHING_ATTRS)
        && rlVerify(m_Config.m_Attrs.GetGameMode() != RL_INVALID_GAME_MODE)
        && rlVerify(m_Config.m_Attrs.GetSessionPurpose() != RL_INVALID_SESSION_TYPE)
#if RSG_DURANGO
		&& rlVerify(m_hSession)
#endif
        && rlVerify(m_Config.m_SessionId != 0);

    return isValid;
}

#if !__NO_OUTPUT
void
rlSession::Data::LogDetails() const
{
	rlDebug("Config - Local Owner: %d:%s", m_LocalOwner.GetLocalIndex(), m_LocalOwner.GetDisplayName());
	rlDebug("Config - Host Peer ID: 0x%016" I64FMT "x", m_HostPeerId);
	rlDebug("Config - Session Token: 0x%016" I64FMT "x", m_SessionInfo.IsValid() ? m_SessionInfo.GetToken().m_Value : 0);
	rlDebug("Config - Network Mode: %d", m_Config.m_NetworkMode);
	rlDebug("Config - Max Public Slots: %u", m_Config.m_MaxPublicSlots);
	rlDebug("Config - Max Private Slots: %u", m_Config.m_MaxPrivateSlots);
	rlDebug("Config - Attribute Count: %u", m_Config.m_Attrs.GetCount());
	rlDebug("Config - Game Mode: %u", m_Config.m_Attrs.GetGameMode());
    rlDebug("Config - Session Type: %u", m_Config.m_Attrs.GetSessionPurpose());
	rlDebug("Config - Session ID: 0x%016" I64FMT "x", m_Config.m_SessionId);
	rlDebug("Config - CreateFlags: %u", m_Config.m_CreateFlags);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//  rlSession::Cmd
///////////////////////////////////////////////////////////////////////////////

rlSession::Cmd::Cmd(const CmdId id, rlSession* session, netStatus* status)
: m_Id(id)
, m_Session(session)
, m_Status(status ? status : &m_TmpStatus)
{
}

rlSession::Cmd::~Cmd()
{
    rlAssert(!m_MyStatus.Pending());
    rlAssert(!m_Status->Pending());
}

bool rlSession::Cmd::Cancel()
{
	OnCancel();

	if(m_MyStatus.Pending())
	{
		rlDebug("%s :: Cancel", GetCmdName());
		
		// task status is handed off to all sorts of places - instead of trying to pick up
		// all of these, just take the heavy handed approach of checking this status in each
		// of the major task pools
		// eventually, we should just have one task pool (with rlTask and netTask deriving from that
		// to define any differences). And these commands should be removed and replaced with tasks

#if RSG_DURANGO
		g_rlXbl.CancelTask(&m_MyStatus);
#endif

		// rlGetTaskManager
		if(m_MyStatus.Pending() && rlGetTaskManager()->FindTask(&m_MyStatus))
		{
			rlDebug("%s :: Cancelling rlTask", GetCmdName());
			rlGetTaskManager()->CancelTask(&m_MyStatus);
		}

		// netTask
		if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
		{
			rlDebug("%s :: Cancelling netTask", GetCmdName());
			netTask::Cancel(&m_MyStatus);
		}

		// still here - some commands use the netStatus object to poll other async operations, note the error
		// with an rlAssert (for now) and log. 
		if(!rlVerify(!m_MyStatus.Pending()))
		{
			rlError("%s :: Status still pending, forcing cancel", GetCmdName());
			m_MyStatus.SetCanceled();
		}
	}

	// we can only cancel a running task
	if(m_Status && m_Status->Pending())
	{
		m_Status->SetCanceled();
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdUpdateScAdvertisement
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdUpdateScAdvertisement::CmdUpdateScAdvertisement(rlSession* session,
															  netStatus* status)
: Cmd(CMD_UPDATE_SC_ADVERTISEMENT, session, status)
, m_State(STATE_INVALID)
{
}

void
rlSession::CmdUpdateScAdvertisement::Start()
{
	rlAssert(m_Session->IsHost() && m_Session->IsOnline());
	
	if(!m_Status->Pending())
	{
		m_Status->SetPending();
	}
	
	bool advertise = (m_Session->m_Data.m_MatchId.IsValid() == false) &&
				     (m_Session->GetEmptySlots(RL_SLOT_PUBLIC) > 0) && 
				     m_Session->IsMatchmakingEnabled() &&
					 m_Session->IsMatchmakingAdvertisingEnabled();

	bool unadvertise = (m_Session->m_Data.m_MatchId.IsValid()) &&
					   (m_Session->GetEmptySlots(RL_SLOT_PUBLIC) == 0) && 
					   m_Session->IsMatchmakingEnabled();

	bool update = (m_Session->m_Data.m_MatchId.IsValid()) &&
				  (m_Session->GetEmptySlots(RL_SLOT_PUBLIC) > 0) && 
				  m_Session->IsMatchmakingEnabled();

	if(advertise)
	{
		rlDebug("%s :: Advertising. Slots: %u / %u", GetCmdName(), m_Session->GetMaxSlots() - m_Session->GetEmptySlots(RL_SLOT_PUBLIC), m_Session->GetMaxSlots());
			
		if (rlScMatchmaking::Advertise(m_Session->GetOwner().GetLocalIndex(),
										m_Session->GetMaxSlots(),
										m_Session->GetEmptySlots(RL_SLOT_PUBLIC),
										m_Session->m_Data.m_Config.m_Attrs,
										m_Session->m_Data.m_Config.m_SessionId,
										m_Session->m_Data.m_SessionInfo,
										&m_Session->m_Data.m_MatchId,
										&m_MyStatus))
		{
			m_State = STATE_ADVERTISING_SOCIAL_CLUB_SESSION;
		}
		else
		{
			m_Status->SetFailed();
		}
	}
	else if(update)
	{
		rlDebug("%s :: Updating - %d/%d free public slots, %d/%d free private slots, %d reserved", 
				GetCmdName(),
				m_Session->GetEmptySlots(RL_SLOT_PUBLIC), 
				m_Session->GetMaxSlots(RL_SLOT_PUBLIC), 
				m_Session->GetEmptySlots(RL_SLOT_PRIVATE), 
				m_Session->GetMaxSlots(RL_SLOT_PRIVATE), 
				m_Session->GetNumReservedSlots());

		int numAvailableSlots = rage::Max((int)m_Session->GetEmptySlots(RL_SLOT_PUBLIC) - (int)m_Session->GetNumReservedSlots(), 0);
		if(rlScMatchmaking::Update(m_Session->GetOwner().GetLocalIndex(),
									m_Session->m_Data.m_MatchId,
									m_Session->GetMaxSlots(),
									numAvailableSlots,
									m_Session->m_Data.m_SessionInfo,
									m_Session->m_Data.m_Config.m_Attrs,
									&m_MyStatus))
		{
			m_State = STATE_UPDATING_SOCIAL_CLUB_SESSION;
		}
		else
		{
			m_Status->SetFailed();
		}
	}
	else if(unadvertise)
	{
		rlDebug("%s :: Unadvertising", GetCmdName());
		
		if (rlScMatchmaking::Unadvertise(m_Session->GetOwner().GetLocalIndex(),
										m_Session->m_Data.m_MatchId,
										&m_MyStatus))
		{
			m_State = STATE_UNADVERTISING_SOCIAL_CLUB_SESSION;
		}
		else
		{
			m_Status->SetFailed();
		}
	}
	else
	{
		// nothing to do
		m_Status->SetSucceeded();
	}
}

void
rlSession::CmdUpdateScAdvertisement::Update()
{
    switch(m_State)
    {
	case STATE_ADVERTISING_SOCIAL_CLUB_SESSION:
		if (!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: Advertising - Succeeded", GetCmdName());
				rlAssert(m_Session->m_Data.m_MatchId.IsValid());
				m_Status->SetSucceeded();
			}
			else
			{
				rlError("%s :: Advertising - Failed", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_UPDATING_SOCIAL_CLUB_SESSION:
		if (!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: Updating - Succeeded", GetCmdName());
				m_Status->SetSucceeded();
			}
			else
			{
				rlError("%s :: Updating - Failed", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_UNADVERTISING_SOCIAL_CLUB_SESSION:
		if (!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: Unadvertising - Succeeded", GetCmdName());
				m_Session->m_Data.m_MatchId.Clear();
				m_Status->SetSucceeded();
			}
			else
			{
				rlError("%s :: Unadvertising - Failed", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_INVALID:
		break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdDestroy
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdDestroy::CmdDestroy(
	rlSession* session,
	netStatus* status)
: Cmd(CMD_DESTROY, session, status)
, m_ClearSessionWhenDone(true)
, m_State(STATE_UNADVERTISE_SOCIAL_CLUB_SESSION)
{
}

void
rlSession::CmdDestroy::Start()
{
	m_State = STATE_UNADVERTISE_SOCIAL_CLUB_SESSION;
}

void
rlSession::CmdDestroy::Update()
{
    switch(m_State)
    {
	case STATE_UNADVERTISE_SOCIAL_CLUB_SESSION:
		{
			if(m_Session->CanUnadvertiseScMatch())
			{
				if(rlScMatchmaking::Unadvertise(m_Session->GetOwner().GetLocalIndex(),
												m_Session->m_Data.m_MatchId,
												&m_MyStatus))
				{
					rlDebug("%s :: Unadvertise :: Succeeed - State -> Unadvertising...", GetCmdName());
					m_State = STATE_UNADVERTISING_SOCIAL_CLUB_SESSION;
				}
				else
				{
					rlError("%s :: Unadvertise :: Failed - State -> Destroy...", GetCmdName());
					m_State = STATE_DESTROY;
				}
			}
			else
			{
				rlDebug("%s :: Unadvertise :: Not Needed - State -> Destroy...", GetCmdName());
				m_State = STATE_DESTROY;
			}
		}
		break;

	case STATE_UNADVERTISING_SOCIAL_CLUB_SESSION:
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: Unadvertising :: Succeeded - State -> Destroy...", GetCmdName());
				m_Session->m_Data.m_MatchId.Clear();
			}
			else
			{
				rlError("%s :: Unadvertising :: Failed - State -> Destroy...", GetCmdName());
			}

			m_State = STATE_DESTROY;
		}
		break;

    case STATE_DESTROY:
#if RSG_DURANGO
        if(m_Session->m_Data.m_hSession)
        {
			if(g_rlXbl.GetSessionManager()->DestroySession(m_Session->m_Data.m_LocalOwner.GetLocalIndex(),
				m_Session->m_Data.m_hSession,
				&m_MyStatus))
			{

				rlDebug("%s :: Destroy :: DestroySession Succeeded - State -> Destroying...", GetCmdName());
				m_State = STATE_DESTROYING;
			}
			else
			{
				rlError("%s :: Destroy :: DestroySession Failed - State -> Finished...", GetCmdName());
				m_State = STATE_FINISHED;
			}
		}
		else
		{
			rlDebug("%s :: Destroy :: Not Needed - State -> Finished...", GetCmdName());
			m_State = STATE_FINISHED;
		}
#elif RL_NP_UDS
        if(m_Session->m_Data.m_SessionInfo.m_WepApiMatchId.IsValid())
        {
			// todo - only do this when the match is being abandoned before we finish
            if(g_rlNp.GetWebAPI().UpdateMatchStatus(m_Session->m_Data.m_LocalOwner.GetLocalIndex(), m_Session->m_Data.m_SessionInfo.m_WepApiMatchId, rlNpMatchStatus::Canceled, &m_MyStatus))
            {
				rlDebug("%s :: Destroy :: UpdateMatchStatus Succeeded - State -> Destroying...", GetCmdName());
				m_State = STATE_DESTROYING;
            }
            else
            {
				rlError("%s :: Destroy :: UpdateMatchStatus Failed - State -> Finished...", GetCmdName());
				m_State = STATE_FINISHED;
            }
        }
		else
		{
			rlDebug("%s :: Destroy :: Not Needed - State -> Destroy...", GetCmdName());
			m_State = STATE_FINISHED;
		}
#else
        m_State = STATE_FINISHED;
#endif
        break;

    case STATE_DESTROYING:
        if(!m_MyStatus.Pending())
        {
#if RL_NP_UDS
			rlDebug("%s :: Destroying :: %s - State -> Finished", m_MyStatus.Succeeded() ? "Succeeded" : "Failed", GetCmdName());
			m_Session->m_Data.m_SessionInfo.m_WepApiMatchId.Clear();
#endif
            m_State = STATE_FINISHED;
        }
        break;

    case STATE_FINISHED:
#if RSG_DURANGO
        m_Session->m_Data.m_hSession = NULL;
		m_Session->m_Data.m_SessionInfo.Clear();
		m_Session->m_Data.m_State = STATE_DORMANT;
#elif RSG_PC || RSG_NP
		m_Session->m_Data.m_SessionInfo.Clear();
		m_Session->m_Data.m_State = STATE_DORMANT;
#endif

        if(m_ClearSessionWhenDone)
        {
			rlDebug("%s :: Clearing Session...", GetCmdName());
			m_Session->Clear();
        }

        // destroy should always succeed.
        m_Status->SetSucceeded();
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdHost
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdHost::CmdHost(rlSession* session,
                            const rlGamerInfo& localOwner,
                            const rlNetworkMode netMode,
                            const int numPublicSlots,
                            const int numPrivateSlots,
                            const rlMatchingAttributes& attrs,
                            const unsigned createFlags,
                            netStatus* status)
    : Cmd(CMD_HOST_NET, session, status)
    , m_State(STATE_CREATING)
    , m_CmdDestroy(session, &m_MyStatus)
{
    rlAssert(attrs.IsValid());

    unsigned presenceFlags = 0;
	unsigned createFlagsNoPresence = createFlags;

    if(!(createFlags & RL_SESSION_CREATE_FLAG_INVITES_DISABLED)
        && !(createFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED)
        && RL_NETMODE_ONLINE == netMode)
    {
        presenceFlags |= RL_SESSION_PRESENCE_FLAG_INVITABLE;
		createFlagsNoPresence &= ~RL_SESSION_CREATE_FLAG_INVITES_DISABLED;
		rlDebug("%s :: Invites are enabled.", GetCmdName());
    }

	if(!(createFlags & RL_SESSION_CREATE_FLAG_JOIN_VIA_PRESENCE_DISABLED)
		&& !(createFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED)
		&& RL_NETMODE_ONLINE == netMode)
	{
		presenceFlags |= RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE;
		createFlagsNoPresence &= ~RL_SESSION_CREATE_FLAG_JOIN_VIA_PRESENCE_DISABLED;
		rlDebug("%s :: JoinViaPresence enabled.", GetCmdName());
	}

    rlVerify(m_Data.Reset(localOwner,
								0,
                                NULL,
                                netMode,
                                numPublicSlots,
                                numPrivateSlots,
                                attrs,
                                createFlagsNoPresence,
                                presenceFlags,
                                0,
                                0));
}

void 
rlSession::CmdHost::OnCancel()
{
	if(m_State == STATE_DESTROYING && m_MyStatus.Pending())
	{
		rlDebug("%s :: OnCancel :: Cancelling CmdDestroy", GetCmdName());
		m_CmdDestroy.Cancel();
	}
}

void
rlSession::CmdHost::Update()
{
    switch(m_State)
    {
    case STATE_CREATING:
        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
				rlDebug("%s :: Creating :: Succeeded - State -> SetVisibility", GetCmdName());
				
				rlPeerInfo localPeerInfo;
				rlPeerInfo::GetLocalPeerInfo(&localPeerInfo, m_Data.m_LocalOwner.GetLocalIndex());

                m_Session->m_Data = m_Data;
				m_Session->m_Data.m_HostPeerId = localPeerInfo.GetPeerId();
				m_Session->m_Data.m_State = STATE_IDLE;

                rlAssert(m_Session->m_Data.Validate());

				// log out details of the new config
				OUTPUT_ONLY(m_Session->m_Data.LogDetails());

                m_State = STATE_SET_VISIBILITY;
            }
            else
            {
				rlError("%s :: Creating :: Failed - State -> Destroy", GetCmdName());
				m_State = STATE_DESTROY;
            }
        }
        break;

    case STATE_SET_VISIBILITY:
        m_State = STATE_SETTING_VISIBILITY;
        break;

    case STATE_SETTING_VISIBILITY:
        if(!m_MyStatus.Pending())
        {
			if(m_MyStatus.Succeeded())
			{
#if RL_NP_UDS
				rlDebug("%s :: SettingVisibility :: Succeeded - State -> CreateMatchActivity", GetCmdName());
				m_State = STATE_CREATE_MATCH_ACTIVITY;
#elif RSG_NP
				rlDebug("%s :: SettingVisibility :: Succeeded - State -> PostSession", GetCmdName());
				m_State = STATE_POST_SESSION;
#else
				rlDebug("%s :: SettingVisibility :: Succeeded - State -> AdvertiseSocialClubSession", GetCmdName());
				m_State = STATE_ADVERTISE_SOCIAL_CLUB_SESSION;
#endif
			}
			else
			{
				rlError("%s :: SettingVisibility :: Failed - State -> Destroy", GetCmdName());
				m_State = STATE_DESTROY;
			}
        }
        break;

#if RL_NP_UDS
    case STATE_CREATE_MATCH_ACTIVITY:
		if((m_Data.m_Config.m_CreateFlags & RL_SESSION_CREATE_UDS_MATCH_ENABLED) != 0)
		{
			const rlPresenceActivityId& activityId = m_Session->m_Data.m_PlatformAttrs.GetWepApiActivityId();
			if(activityId.IsNotNull())
			{
				int localGamerIndex = m_Session->GetOwner().GetLocalIndex();

				rlGamerInfo ginfo;
				rlNpMatchCreateParams params;
				params.m_ActivityId = activityId;
				params.m_ZoneId = m_Session->m_Data.m_PlatformAttrs.GetWepApiZoneId();

				if(rlPresence::GetGamerInfo(localGamerIndex, &ginfo))
				{
					params.m_Players.Push(rlNpMatchPlayerJoin(ginfo));
				}

				if(g_rlNp.GetWebAPI().CreateMatch(localGamerIndex, params, &m_Session->m_Data.m_SessionInfo.m_WepApiMatchId, &m_MyStatus))
				{
					rlDebug("%s :: CreateMatchActivity :: CreateMatch Succeeded - State -> CreatingMatchActivity", GetCmdName());
					m_State = STATE_CREATING_MATCH_ACTIVITY;
				}
				else
				{
					rlDebug("%s :: CreateMatchActivity :: CreateMatch Failed - State -> PostSession", GetCmdName());
					m_State = STATE_POST_SESSION;
				}
			}
			else
			{
				rlDebug("%s :: CreateMatchActivity :: Invalid Activity - State -> PostSession", GetCmdName());
				m_State = STATE_POST_SESSION;
			}
		}
		else
		{
			rlDebug("%s :: CreateMatchActivity :: Not Needed - State -> PostSession", GetCmdName());
			m_State = STATE_POST_SESSION;
		}
        break;

    case STATE_CREATING_MATCH_ACTIVITY:
        {
            if(!m_MyStatus.Pending())
            {
				rlDebug("%s :: CreatingMatchActivity :: %s - State -> PostSession", m_MyStatus.Succeeded() ? "Succeeded" : "Failed", GetCmdName());
				rlAssertf(m_MyStatus.Succeeded(), "%s :: Failed to create match. This is likely due to the backend activity either not a MP activity or the wrong id is being passed i.", GetCmdName());
                m_State = STATE_POST_SESSION;
            }
        }
        break;
#endif

#if RSG_NP
	case STATE_POST_SESSION:
		// only post for presence enabled sessions
		if((m_Data.m_Config.m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED) == 0)
		{
#if RSG_PROSPERO
			rlPeerAddress peerAddr;
			rlVerify(rlPeerAddress::GetLocalPeerAddress(m_Session->GetOwner().GetLocalIndex(), &peerAddr));
			rlVerify(peerAddr.IsValid());
			int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
			if(g_rlNp.GetWebAPI().PostSession(localGamerIndex, g_rlNp.GetDefaultSessionImage(), m_Session, &m_MyStatus))
			{

			rlSessionInfo sessionInfo;
			sessionInfo.Init(m_Data.m_Config.m_SessionId, m_Data.m_SessionInfo.m_SessionToken, peerAddr);

			unsigned dataLength = 0;
			char dataBuf[rlSessionInfo::TO_STRING_BUFFER_SIZE];
			rlVerify(sessionInfo.ToString(dataBuf, &dataLength));

			rlNpWebApiPostSessionParams params = m_Data.m_PlatformAttrs.GetPlayerSessionParams();
			params.SetSessionId(&m_Session->m_Data.m_SessionInfo.m_WebApiSessionId);
			params.SetChangeableData(dataBuf);

			int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
			if(g_rlNp.GetWebAPI().CreatePlayerSession(localGamerIndex, params, &m_MyStatus))
			{
				rlDebug("%s :: PostSession :: CreatePlayerSession Succeeded - State -> PostingSession", GetCmdName());
				m_State = STATE_POSTING_SESSION;
			}
			else
			{
				rlError("%s :: PostSession :: CreatePlayerSession Failed - State -> Destroy", GetCmdName());
				m_State = STATE_DESTROY;
			}
#elif RSG_ORBIS
			int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
			if (g_rlNp.GetWebAPI().PostSession(localGamerIndex, g_rlNp.GetDefaultSessionImage(), m_Session, &m_MyStatus))
			{
				rlDebug("%s :: PostSession :: PostSession Succeeded - State -> PostingSession", GetCmdName());
				m_State = STATE_POSTING_SESSION;
			}
			else
			{
				rlError("%s :: PostSession :: PostSession Failed - State -> Destroy", GetCmdName());
				m_State = STATE_DESTROY;
			}
#endif
		}
		else
		{
			rlDebug("%s :: PostSession :: Not Needed - State -> AdvertiseSocialClubSession", GetCmdName());
			m_State = STATE_ADVERTISE_SOCIAL_CLUB_SESSION;
		}
		break;

	case STATE_POSTING_SESSION:
	{
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: PostingSession :: Succeeded - State -> AdvertiseSocialClubSession", GetCmdName());
				m_State = STATE_ADVERTISE_SOCIAL_CLUB_SESSION;
			}
			else
			{
				rlError("%s :: PostingSession :: Failed - State -> AdvertiseSocialClubSession", GetCmdName());
				m_State = STATE_DESTROY;
			}
		}
	}
	break;
#endif

	case STATE_ADVERTISE_SOCIAL_CLUB_SESSION:
		if(m_Session->CanAdvertiseScMatch())
		{
			int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
			if (rlScMatchmaking::Advertise(localGamerIndex,
				m_Session->GetMaxSlots(),
				m_Session->GetEmptySlots(RL_SLOT_PUBLIC),
				m_Session->m_Data.m_Config.m_Attrs,
				m_Session->m_Data.m_Config.m_SessionId,
				m_Session->m_Data.m_SessionInfo,
				&m_Session->m_Data.m_MatchId,
				&m_MyStatus))
			{
				rlDebug("%s :: AdvertiseSocialClubSession :: Advertise Succeeded - State -> AdvertisingSocialClubSession", GetCmdName());
				m_State = STATE_ADVERTISING_SOCIAL_CLUB_SESSION;
			}
			else
			{
				rlError("%s :: AdvertiseSocialClubSession :: Advertise Failed - State -> Destroy", GetCmdName());
				m_State = STATE_DESTROY;
			}
		}
		else
		{
			rlDebug("%s :: AdvertiseSocialClubSession :: NotNeeded - Finished", GetCmdName());
			m_Status->SetSucceeded();
		}
		break;

	case STATE_ADVERTISING_SOCIAL_CLUB_SESSION:
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: AdvertisingSocialClubSession :: Succeeded - Finished", GetCmdName());
				m_Status->SetSucceeded();
			}
			else
			{
				rlError("%s :: AdvertisingSocialClubSession :: State -> Destroy", GetCmdName());
				m_State = STATE_DESTROY;
			}
		}
		break;

    case STATE_DESTROY:
		rlDebug("%s :: Destroy :: State -> Destroying...", GetCmdName());
		m_CmdDestroy.m_ClearSessionWhenDone = false;
        m_CmdDestroy.m_Status->SetPending();
        m_State = STATE_DESTROYING;
        break;

    case STATE_DESTROYING:
        m_CmdDestroy.Update();
        if(!m_MyStatus.Pending())
        {
			rlDebug("%s :: Destroying :: %s", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_Status->SetFailed();
        }
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdHostOffline
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdHostOffline::CmdHostOffline(rlSession* session,
                                            const rlGamerInfo& localOwner,
                                            const int numSlots,
                                            const rlMatchingAttributes& attrs,
                                            netStatus* status)
: CmdHost(session,
            localOwner,
            RL_NETMODE_OFFLINE,
            numSlots,
            0,   
            attrs,
            0,
            status)
{
    rlAssert(attrs.IsValid());
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdActivate
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdActivate::CmdActivate(rlSession* session,
                                    const rlGamerInfo& localOwner,
									const u64 hostPeerId,
                                    const rlSessionInfo& sessionInfo,
                                    const rlNetworkMode netMode,
                                    const unsigned createFlags,
                                    const unsigned presenceFlags,
                                    const unsigned maxPublicSlots,
                                    const unsigned maxPrivateSlots,
                                    const rlSlotType slotType,
                                    netStatus* status)
	: Cmd(CMD_ACTIVATE, session, status)
	, m_LocalOwner(localOwner)
	, m_HostPeerId(hostPeerId)
	, m_SessionInfo(sessionInfo)
	, m_NetMode(netMode)
	, m_CreateFlags(createFlags)
	, m_PresenceFlags(presenceFlags)
	, m_MaxPublicSlots(maxPublicSlots)
	, m_MaxPrivateSlots(maxPrivateSlots)
	, m_SlotType(slotType)
	, m_State(STATE_ACTIVATING)
	, m_CmdDestroy(session, &m_MyStatus)
{
#if RSG_DURANGO
    m_hSession = NULL;
#endif
}

void 
rlSession::CmdActivate::OnCancel()
{
	if(m_State == STATE_DESTROYING && m_MyStatus.Pending())
	{
		rlDebug("%s :: OnCancel :: Cancelling CmdDestroy", GetCmdName());
		m_CmdDestroy.Cancel();
	}
}

void
rlSession::CmdActivate::Update()
{
    switch(m_State)
    {
    case STATE_ACTIVATING:
        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                m_Session->m_Data.m_LocalOwner = m_LocalOwner;
				m_Session->m_Data.m_HostPeerId = m_HostPeerId;
				m_Session->m_Data.m_SessionInfo = m_SessionInfo;
                m_Session->m_Data.m_Config.m_NetworkMode = m_NetMode;
                m_Session->m_Data.m_Config.m_CreateFlags = m_CreateFlags;
                m_Session->m_Data.m_Config.m_PresenceFlags = m_PresenceFlags;
                m_Session->m_Data.m_Config.m_MaxPublicSlots = m_MaxPublicSlots;
                m_Session->m_Data.m_Config.m_MaxPrivateSlots = m_MaxPrivateSlots;
                m_Session->m_Data.m_State = STATE_ACTIVATED;
#if RSG_DURANGO
				m_Session->m_Data.m_hSession = m_hSession;
				rlDebug("%s :: SessionToken: 0x%016" I64FMT "x", GetCmdName(), m_SessionInfo.GetToken().GetValue());
#endif

                rlDebug("%s :: Activating :: Succeeded - Finished", GetCmdName());

                m_Status->SetSucceeded();
            }
            else
            {
				rlDebug("%s :: Activating :: Failed - State -> Destroy", GetCmdName());
				m_State = STATE_DESTROY;
            }
        }
        break;

	case STATE_DESTROY:
		rlDebug("%s :: Destroy - State -> Destroying...", GetCmdName());
        m_CmdDestroy.m_ClearSessionWhenDone = false;
        m_CmdDestroy.m_Status->SetPending();
        m_State = STATE_DESTROYING;
        break;

    case STATE_DESTROYING:
        m_CmdDestroy.Update();
        if(!m_MyStatus.Pending())
        {
			rlDebug("%s :: Destroying :: %s", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_Status->SetFailed();
        }
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdEstablish
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdEstablish::CmdEstablish(rlSession* session,
                                        netStatus* status)
: Cmd(CMD_ESTABLISH, session, status)
{
}

void
rlSession::CmdEstablish::Update()
{
    if(!m_MyStatus.Pending())
    {
        if(m_MyStatus.Succeeded())
        {
            m_Session->m_Data = m_Data;
            m_Session->m_Data.m_State = STATE_IDLE;

            rlAssert(m_Data.Validate());
        }

        rlDebug("%s :: %s", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
        m_Status->SetStatus(m_MyStatus.GetStatus());
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdJoin
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdJoin::CmdJoin(rlSession* session, netStatus* status)
: Cmd(CMD_JOIN, session, status)
, m_NumGamers(0)
, m_CmdUpdateScAdvertisement(session, &m_MyStatus)
, m_State(STATE_ADDING_GAMERS)
{
    m_PriorNumEmptyPubSlots = session->GetEmptySlots(RL_SLOT_PUBLIC);
}

void 
rlSession::CmdJoin::OnCancel()
{
	// cancel dependent tasks
	if(m_State == STATE_UPDATING_SOCIAL_CLUB_SESSION && m_MyStatus.Pending())
	{
		rlDebug("%s :: OnCancel :: Cancelling CmdUpdateScAdvertisement", GetCmdName());
		m_CmdUpdateScAdvertisement.Cancel();
	}
}

void
rlSession::CmdJoin::Update()
{
    switch(m_State)
    {
    case STATE_ADDING_GAMERS:
        if(!m_MyStatus.Pending())
        {
			if(m_MyStatus.Succeeded())
			{
				if(m_Session->IsHost()
					&& m_Session->GetEmptySlots(RL_SLOT_PUBLIC) == 0
					&& m_PriorNumEmptyPubSlots > 0)
				{
					rlDebug("%s :: AddingGamers :: Succeeded - State -> ChangingSessionVisibility...", GetCmdName());
					m_State = STATE_CHANGING_SESSION_VISIBILITY;
				}
				else
				{
#if RSG_ORBIS
					rlDebug("%s :: AddingGamers :: Succeeded - State -> PostMember...", GetCmdName());
					m_State = STATE_POST_MEMBER;
#elif RL_NP_UDS
					rlDebug("%s :: AddingGamers :: Succeeded - State -> JoinMatchActivity...", GetCmdName());
					m_State = STATE_JOIN_MATCH_ACTIVITY;
#else
					rlDebug("%s :: AddingGamers :: Succeeded - State -> UpdateGamerList...", GetCmdName());
					m_State = STATE_UPDATE_GAMER_LIST;
#endif
				}
			}
			else
			{
				rlError("%s :: AddingGamers :: Failed - Finished", GetCmdName());
				m_Status->SetStatus(m_MyStatus.GetStatus());
			}
        }
        break;

    case STATE_CHANGING_SESSION_VISIBILITY:
        if(m_MyStatus.Succeeded())
        {
#if RSG_ORBIS
			rlDebug("%s :: ChangingSessionVisibility :: Succeeded - State -> PostMember...", GetCmdName());
			m_State = STATE_POST_MEMBER;
#elif RL_NP_UDS
			rlDebug("%s :: ChangingSessionVisibility :: Succeeded - State -> JoinMatchActivity...", GetCmdName());
			m_State = STATE_JOIN_MATCH_ACTIVITY;
#else
			rlDebug("%s :: ChangingSessionVisibility :: Succeeded - State -> UpdateGamerList...", GetCmdName());
			m_State = STATE_UPDATE_GAMER_LIST;
#endif
		}
        else if(!m_MyStatus.Pending())
        {
			rlError("%s :: ChangingSessionVisibility :: Failed - Finished", GetCmdName());
			m_Status->SetStatus(m_MyStatus.GetStatus());
        }
        break;

#if RL_NP_UDS
    case STATE_JOIN_MATCH_ACTIVITY:
        {
            if(m_Session->m_Data.m_SessionInfo.m_WepApiMatchId.IsValid())
            {
                rlNpMatchJoiningPlayers params;
                for(int i = 0; i < (int)m_NumGamers; ++i)
                {
                    const rlGamerHandle& gamerHandle = m_GamerHandles[i];
                    if(gamerHandle.IsLocal())
                    {
                        rlGamerInfo ginfo;

                        int localGamerIndex = m_GamerHandles[i].GetLocalIndex();
                        if(rlVerifyf(rlPresence::GetGamerInfo(localGamerIndex, &ginfo), "%s :: JoinMatchActivity :: Failed to get gamer info", GetCmdName()))
                        {
							rlDebug("%s :: JoinMatchActivity :: Adding %s", GetCmdName(), ginfo.GetName());
							params.Push(rlNpMatchPlayerJoin(ginfo));
                        }
                    }
                }

                if(params.GetCount())
                {
                    int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
                    if(g_rlNp.GetWebAPI().JoinMatch(localGamerIndex, m_Session->m_Data.m_SessionInfo.m_WepApiMatchId, params, &m_MyStatus))
                    {
						rlDebug("%s :: JoinMatchActivity :: JoinMatch Succeeded - State -> JoiningMatchActivity", GetCmdName());
						m_State = STATE_JOINING_MATCH_ACTIVITY;
                    }
                    else
                    {
						rlError("%s :: JoinMatchActivity :: JoinMatch Failed - Finished", GetCmdName());
						m_Status->SetFailed();
                    }
                }
                else
                {
					rlDebug("%s :: JoinMatchActivity :: No-one to Add - State -> UpdateGamerList", GetCmdName());
					m_State = STATE_UPDATE_GAMER_LIST;
                }
            }
            else
            {
				rlDebug("%s :: JoinMatchActivity :: Not in a match - State -> UpdateGamerList", GetCmdName());
				m_State = STATE_UPDATE_GAMER_LIST;
            }
        }
        break;

    case STATE_JOINING_MATCH_ACTIVITY:
        {
            if(m_MyStatus.Succeeded())
            {
				rlDebug("%s :: JoiningMatchActivity :: Succeeded - State -> UpdateGamerList...", GetCmdName());
				m_State = STATE_UPDATE_GAMER_LIST;
            }
            else if(!m_MyStatus.Pending())
            {
				rlError("%s :: JoinMatchActivity :: Failed - Finished", GetCmdName());
				m_Status->SetStatus(m_MyStatus.GetStatus());
            }
        }
        break;

#elif RSG_ORBIS
	case STATE_POST_MEMBER:
		{
			// Post to the PS4 web api for presence enabled sessions only
			if(!(m_Session->m_Data.m_Config.m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED))
			{
				bool bHasLocalGamers = false;
				for(int i = 0; i < (int)m_NumGamers; ++i)
				{
					const rlGamerHandle& gamerHandle = m_GamerHandles[i];
					if(gamerHandle.IsLocal())
					{
						bHasLocalGamers = true;
						break;
					}
				}

				if(bHasLocalGamers)
				{
					int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
					if(g_rlNp.GetWebAPI().PostMembers(localGamerIndex, m_Session->GetWebApiSessionId(), m_GamerHandles, m_NumGamers, &m_MyStatus))
					{
						rlDebug("%s :: PostMember :: Succeeded - State -> PostingMember...", GetCmdName());
						m_State = STATE_POSTING_MEMBER;
					}
					else
					{
						rlDebug("%s :: PostMember :: Failed - Finished", GetCmdName());
						m_Status->SetFailed();
					}
				}
				else
				{
					rlDebug("%s :: PostMember :: No local gamers - State -> UpdateGamerList...", GetCmdName());
					m_State = STATE_UPDATE_GAMER_LIST;
				}
			}
			else
			{
				rlDebug("%s :: PostMember :: Not Needed - State -> UpdateGamerList...", GetCmdName());
				m_State = STATE_UPDATE_GAMER_LIST;
			}
		}
		break;
	case STATE_POSTING_MEMBER:
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: PostingMember :: Succeeded - State -> UpdateGamerList...", GetCmdName());
				m_State = STATE_UPDATE_GAMER_LIST;
			}
			else if(!m_MyStatus.Pending())
			{
				rlDebug("%s :: PostingMember :: Failed - Finished", GetCmdName());
				m_Status->SetStatus(m_MyStatus.GetStatus());
			}
		}
		break;
#endif

    case STATE_UPDATE_GAMER_LIST:
        
		// add the gamers to our local data structures.
        for(int i = 0; i < (int) m_NumGamers; ++i)
        {
            const rlGamerHandle& gamerHandle = m_GamerHandles[i];

            OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);
            rlDebug("%s :: UpdateGamerList :: %s (%s) joined, SlotType: %s",
				GetCmdName(),
				gamerHandle.ToString(ghBuf),
				gamerHandle.IsLocal() ? "Local" : "Remote",
				(RL_SLOT_PUBLIC == m_SlotTypes[i]) ? "Public" : "Private");

            m_Session->InsertGamer(gamerHandle,  m_SlotTypes[i]);
        }

		if(m_Session->IsHost() && m_Session->IsOnline())
		{
			rlDebug("%s :: UpdateGamerList :: State -> UpdatingSocialClubSession...", GetCmdName());
			m_CmdUpdateScAdvertisement.Start();
			m_State = STATE_UPDATING_SOCIAL_CLUB_SESSION;
		}
		else
		{
			rlDebug("%s :: UpdateGamerList :: Finished", GetCmdName());
			m_Status->SetStatus(m_MyStatus.GetStatus());
		}
        break;

	case STATE_UPDATING_SOCIAL_CLUB_SESSION:

		m_CmdUpdateScAdvertisement.Update();
		if(!m_MyStatus.Pending())
		{
			rlDebug("%s :: UpdatingSocialClubSession :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_Status->SetStatus(m_MyStatus.GetStatus());
		}
		break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdLeave
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdLeave::CmdLeave(rlSession* session, netStatus* status)
: Cmd(CMD_LEAVE, session, status)
, m_NumGamers(0)
, m_CmdUpdateScAdvertisement(session, &m_MyStatus)
, m_HasLocalGamers(false)
, m_UnadvertisedSession(false)
, m_State(STATE_UNADVERTISING_SOCIAL_CLUB_SESSION)
{

}

void 
rlSession::CmdLeave::OnCancel()
{
	// cancel dependent tasks
	if(m_State == STATE_UPDATING_SOCIAL_CLUB_SESSION && m_MyStatus.Pending())
	{
		rlDebug("%s :: OnCancel :: Cancelling CmdUpdateScAdvertisement", GetCmdName());
		m_CmdUpdateScAdvertisement.Cancel();
	}
}

void
rlSession::CmdLeave::Update()
{
    switch(m_State)
    {
	case STATE_UNADVERTISING_SOCIAL_CLUB_SESSION:
		{
			if(!m_MyStatus.Pending())
			{
				// clear our matchId if we unadvertised
				if(m_MyStatus.Succeeded() && m_UnadvertisedSession)
				{
					rlDebug("%s :: UnadvertisingSocialClubSession :: Clearing MatchId", GetCmdName());
					m_Session->m_Data.m_MatchId.Clear();
				}

				bool bShouldLeavePlatformService = false;
				bool bLeavingPlatformService = false;

#if RSG_XBL
				if(m_HasLocalGamers)
				{
					bShouldLeavePlatformService = true; 
					bLeavingPlatformService = g_rlXbl.GetSessionManager()->LeaveSession(m_Session->GetOwner().GetLocalIndex(),
																						m_Session->m_Data.m_hSession,
																						&m_MyStatus);
				}
#elif RSG_NP
				if(m_HasLocalGamers && !(m_Session->m_Data.m_Config.m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED))
				{
					bShouldLeavePlatformService = true;
#if RL_NP_CPPWEBAPI
					bLeavingPlatformService = g_rlNp.GetWebAPI().LeavePlayerSession(m_Session->GetOwner().GetLocalIndex(),
																					m_Session->GetWebApiSessionId(),
																					&m_MyStatus);
#else
					bLeavingPlatformService = g_rlNp.GetWebAPI().DeleteMembers(m_Session->GetOwner().GetLocalIndex(), 
																			   m_Session->GetWebApiSessionId(), 
																			   m_GamerHandles, 
																			   m_NumGamers, 
																			   &m_MyStatus);
#endif
				}
#endif
				if(bShouldLeavePlatformService)
				{
					if(bLeavingPlatformService)
					{
						rlDebug("%s :: UnadvertisingSocialClubSession :: Leaving Platform Service - State -> LeavingPlatformService...", GetCmdName());
						m_State = STATE_LEAVING_PLATFORM_SERVICE;
					}
					else
					{
						rlDebug("%s :: UnadvertisingSocialClubSession :: Failed to Leave Platform Service - State -> UpdateGamerList...", GetCmdName());
						m_State = STATE_UPDATE_GAMER_LIST;
					}
				}
				else
				{
					rlDebug("%s :: UnadvertisingSocialClubSession :: Not Needed to Leave Platform Service - State -> UpdateGamerList...", GetCmdName());
					m_State = STATE_UPDATE_GAMER_LIST;
				}
			}
		}
		break;

	case STATE_LEAVING_PLATFORM_SERVICE:
		{
			if(!m_MyStatus.Pending())
			{
#if RSG_SCE
				rlDebug("%s :: LeavingPlatformService :: Clearing WebApiSessionId", GetCmdName());
				m_Session->ClearWebApiSessionId();
#endif

#if RL_NP_UDS
				rlDebug("%s :: LeavingPlatformService :: %s - State -> LeaveMatchActivity", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
                m_State = STATE_LEAVE_MATCH_ACTIVITY;
#else
				// move on whether the platform call worked or not
				rlDebug("%s :: LeavingPlatformService :: %s - State -> UpdateGamerList", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
				m_State = STATE_UPDATE_GAMER_LIST;
#endif
			}
		}
		break;

#if RL_NP_UDS
    case STATE_LEAVE_MATCH_ACTIVITY:
        {
            if(m_Session->m_Data.m_SessionInfo.m_WepApiMatchId.IsValid())
            {
                rlNpMatchLeavingPlayers params;
                for(int i = 0; i < (int)m_NumGamers; ++i)
                {
                    const rlGamerHandle& gamerHandle = m_GamerHandles[i];
                    if(gamerHandle.IsLocal())
                    {
						// need to pass through whether this was a quit or we actually made it to the end
						rlDebug("%s :: LeaveMatchActivity :: Removing %s", GetCmdName(), gamerHandle.ToString());
						params.Push(rlNpMatchPlayerLeave(gamerHandle, rlNpMatchLeaveReason::Finished));
                    }
                }

                if(params.GetCount())
                {
                    int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
                    if(g_rlNp.GetWebAPI().LeaveMatch(localGamerIndex, m_Session->m_Data.m_SessionInfo.m_WepApiMatchId, params, &m_MyStatus))
                    {
						rlDebug("%s :: LeaveMatchActivity :: LeaveMatch Succeeded - State -> UpdateGamerList...", GetCmdName());
						m_State = STATE_LEAVING_MATCH_ACTIVITY;
                    }
                    else
                    {
						rlError("%s :: LeaveMatchActivity :: LeaveMatch Failed - Finished", GetCmdName());
						m_Status->SetFailed();
                    }
                }
                else
                {
					rlDebug("%s :: LeaveMatchActivity :: No Local Gamers - State -> UpdateGamerList...", GetCmdName());
					m_State = STATE_UPDATE_GAMER_LIST;
                }
            }
            else
            {
				rlDebug("%s :: LeaveMatchActivity :: Not in a Match - State -> UpdateGamerList...", GetCmdName());
				m_State = STATE_UPDATE_GAMER_LIST;
            }
        }
        break;

    case STATE_LEAVING_MATCH_ACTIVITY:
        {
            if(m_MyStatus.Succeeded())
            {
				rlDebug("%s :: LeaveMatchActivity :: Succeeded - State -> UpdateGamerList...", GetCmdName());
				m_State = STATE_UPDATE_GAMER_LIST;
            }
            else if(!m_MyStatus.Pending())
            {
				rlError("%s :: LeaveMatchActivity :: Failed - Finished...", GetCmdName());
				m_Status->SetStatus(m_MyStatus.GetStatus());
            }
        }
        break;
#endif

    case STATE_UPDATE_GAMER_LIST:
        
		// remove the gamers from our local data structures.
        for(int i = 0; i < (int) m_NumGamers; ++i)
        {
			const rlGamerHandle& gamerHandle = m_GamerHandles[i];
			
			OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);
			rlDebug("%s :: UpdateGamerList :: %s (%s) left",
				GetCmdName(),
				gamerHandle.ToString(ghBuf),
				gamerHandle.IsLocal() ? "Local" : "Remote");

			m_Session->RemoveGamer(gamerHandle);
        }

		// if the session was previously full, the host will have unadvertised the session, so we won't 
		// have a matchId any longer and CanUpdateScMatch() will return false. When a slot opens up 
		// after a player leaves a full session, we need to advertise the session once again, so check
		// CanAdvertiseScMatch() as well.
		if(m_Session->IsHost() && m_Session->IsOnline() && (m_Session->CanUpdateScMatch() || m_Session->CanAdvertiseScMatch()) && !m_HasLocalGamers)
		{
			rlDebug("%s :: UpdateGamerList :: %s - State -> UpdatingSocialClubSession...", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_CmdUpdateScAdvertisement.Start();
			m_State = STATE_UPDATING_SOCIAL_CLUB_SESSION;
		}
		else
		{
			rlDebug("%s :: UpdateGamerList :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_Status->SetStatus(m_MyStatus.GetStatus());
		}
        break;

	case STATE_UPDATING_SOCIAL_CLUB_SESSION:
		m_CmdUpdateScAdvertisement.Update();
		if(!m_MyStatus.Pending())
		{
			rlDebug("%s :: UpdatingSocialClubSession :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_Status->SetStatus(m_MyStatus.GetStatus());
		}
		break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdChangeAttrs
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdChangeAttrs::CmdChangeAttrs(rlSession* session,
                                            netStatus* status)
: Cmd(CMD_CHANGE_ATTRS, session, status)
, m_State(STATE_CHANGING_PLATFORM_SESSION_ATTRS)
{
}

void
rlSession::CmdChangeAttrs::Update()
{
	switch(m_State)
	{
	case STATE_CHANGING_PLATFORM_SESSION_ATTRS:
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				m_Session->m_Data.m_Config.m_Attrs = m_NewAttrs;
				if(m_Session->CanUpdateScMatch())
				{
					rlDebug("%s :: ChangingPlatformSessionAttrs :: Succeeded - State -> ChangeSocialClubSessionsAttrs...", GetCmdName());
					m_State = STATE_CHANGE_SOCIAL_CLUB_SESSION_ATTRS;
				}
				else
				{
					rlDebug("%s :: ChangingPlatformSessionAttrs :: Succeeded - Finished", GetCmdName());
					m_Status->SetStatus(m_MyStatus.GetStatus());
				}
			}
			else
			{
				rlError("%s :: ChangingPlatformSessionAttrs :: Failed - Finished", GetCmdName());
				m_Status->SetStatus(m_MyStatus.GetStatus());
			}
		}
		break;

	case STATE_CHANGE_SOCIAL_CLUB_SESSION_ATTRS:
		{
			rlAssert(m_Session->CanUpdateScMatch());

			int numAvailableSlots = rage::Max((int)m_Session->GetEmptySlots(RL_SLOT_PUBLIC) - (int)m_Session->GetNumReservedSlots(), 0);

			rlDebug("%s :: ChangingPlatformSessinAttrs :: Slots: %u / %u", GetCmdName(), m_Session->GetMaxSlots() - numAvailableSlots, m_Session->GetMaxSlots());
			
			if(rlScMatchmaking::Update(m_Session->GetOwner().GetLocalIndex(),
									   m_Session->m_Data.m_MatchId,
									   m_Session->GetMaxSlots(),
									   numAvailableSlots,
									   m_Session->m_Data.m_SessionInfo,
									   m_Session->m_Data.m_Config.m_Attrs,
									   &m_MyStatus))
			{
				rlDebug("%s :: ChangingPlatformSessionAttrs :: rlScMatchmaking::Update Succeeded - State -> ChangingSocialClubSessionsAttrs...", GetCmdName());
				m_State = STATE_CHANGING_SOCIAL_CLUB_SESSION_ATTRS;
			}
			else
			{
				rlError("%s :: ChangingPlatformSessionAttrs :: rlScMatchmaking::Update Failed - Finished", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_CHANGING_SOCIAL_CLUB_SESSION_ATTRS:
		if(!m_MyStatus.Pending())
		{
			rlDebug("%s :: ChangingSocialClubSessionsAttrs :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_Status->SetStatus(m_MyStatus.GetStatus());
		}
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdMigrate
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdMigrate::CmdMigrate(rlSession* session, netStatus* status)
: Cmd(CMD_MIGRATE, session, status)
, m_State(STATE_MIGRATING)
{
}

void
rlSession::CmdMigrate::Update()
{
    switch(m_State)
    {
	case STATE_MIGRATING:
        if(!m_MyStatus.Pending())
        {
			rlDebug("%s :: Migrating :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			
			if(m_MyStatus.Succeeded())
            {
				m_Session->m_Data.m_SessionInfo = m_NewSessionInfo;
				m_Session->m_Data.m_HostPeerId = m_NewHostPeerInfo.GetPeerId();
            }

#if RSG_ORBIS
			// Post to the PS4 web api for presence enabled sessions only
			if(m_Session->m_Data.m_Config.IsPresenceEnabled())
			{
				rlDebug("%s :: Migrating :: State -> PostSession", GetCmdName());
				m_State = STATE_POST_SESSION;
			}
			else
#endif // RSG_ORBIS
			{
				if(m_Session->CanAdvertiseScMatch())
				{
					rlDebug("%s :: Migrating :: State -> AdvertiseSocialClubSession", GetCmdName());
					m_State = STATE_ADVERTISE_SOCIAL_CLUB_SESSION;
				}
				else if(!m_Session->IsHost() && m_Session->CanUnadvertiseScMatch())
				{
					rlDebug("%s :: Migrating :: State -> UnadvertiseSocialClubSession", GetCmdName());
					m_State = STATE_UNADVERTISE_SOCIAL_CLUB_SESSION;
				}
				else
				{
					rlDebug("%s :: Migrating :: Finished", GetCmdName());
					m_Status->SetStatus(m_MyStatus.GetStatus());
				}
			}
        }
        break;

#if RSG_ORBIS
	case STATE_POST_SESSION:
		{
			if(g_rlNp.GetWebAPI().PostSession(m_Session->GetOwner().GetLocalIndex(), g_rlNp.GetDefaultSessionImage(), m_Session, &m_MyStatus))
			{
				rlDebug("%s :: Migrating :: PostSession Succeeded - State -> PostingSession", GetCmdName());
				m_State = STATE_POSTING_SESSION;
			}
			else
			{
				rlError("%s :: Migrating :: PostSession Failed - Finished", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_POSTING_SESSION:
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				if(m_Session->CanAdvertiseScMatch())
				{
					rlDebug("%s :: PostingSession :: Succeeded - State -> AdvertiseSocialClubSession", GetCmdName());
					m_State = STATE_ADVERTISE_SOCIAL_CLUB_SESSION;
				}
				else if(!m_Session->IsHost() && m_Session->CanUnadvertiseScMatch())
				{
					rlDebug("%s :: PostingSession :: Succeeded - State -> UnadvertiseSocialClubSession", GetCmdName());
					m_State = STATE_UNADVERTISE_SOCIAL_CLUB_SESSION;
				}
				else
				{
					rlDebug("%s :: PostingSession :: Succeeded - Finished", GetCmdName());
					m_Status->SetStatus(m_MyStatus.GetStatus());
				}
			}
			else
			{
				if(!m_Session->IsHost() && m_Session->CanUnadvertiseScMatch())
				{
					rlDebug("%s :: PostingSession :: Failed - State -> UnadvertiseSocialClubSession", GetCmdName());
					m_State = STATE_UNADVERTISE_SOCIAL_CLUB_SESSION;
				}
				else
				{
					rlError("%s :: PostingSession :: Failed - Finished", GetCmdName());
					m_Status->SetFailed();
				}
			}
		}
		break;
#else
		// on PS5, we use a presence / player session so this isn't strictly necessary (we would never need to migrate that)
	#pragma message("TODO: [PROSPERO] Add support to the new RL_NP_CPPWEBAPI session interface.")
#endif

	case STATE_ADVERTISE_SOCIAL_CLUB_SESSION:
		{
			rlAssert(m_Session->CanAdvertiseScMatch());

			rlDebug("%s :: AdvertiseSocialClubSession :: Slots: %u / %u", GetCmdName(), m_Session->GetMaxSlots() - m_Session->GetEmptySlots(RL_SLOT_PUBLIC), m_Session->GetMaxSlots());
			
			int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
			if(rlScMatchmaking::Advertise(localGamerIndex,
										  m_Session->GetMaxSlots(),
										  m_Session->GetEmptySlots(RL_SLOT_PUBLIC),
										  m_Session->m_Data.m_Config.m_Attrs,
										  m_Session->m_Data.m_Config.m_SessionId,
										  m_Session->m_Data.m_SessionInfo,
										  &m_Session->m_Data.m_MatchId,
										  &m_MyStatus))
			{
				rlDebug("%s :: AdvertiseSocialClubSession :: Advertise Succeeded - State -> AdvertisingSocialClubSession", GetCmdName());
				m_State = STATE_ADVERTISING_SOCIAL_CLUB_SESSION;
			}
			else
			{
				rlError("%s :: AdvertiseSocialClubSession :: Advertise Failed - Finished", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_ADVERTISING_SOCIAL_CLUB_SESSION:
		if(!m_MyStatus.Pending())
		{
			rlDebug("%s :: AdvertisingSocialClubSession :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			m_Status->SetStatus(m_MyStatus.GetStatus());
		}
		break;
	case STATE_UNADVERTISE_SOCIAL_CLUB_SESSION:
		{
			rlAssert(m_Session->CanUnadvertiseScMatch());

			if(rlScMatchmaking::Unadvertise(m_Session->GetOwner().GetLocalIndex(),
											m_Session->m_Data.m_MatchId,
											&m_MyStatus))
			{
				rlDebug("%s :: UnadvertiseSocialClubSession :: Unadvertise Succeeded - State -> UnadvertisingSocialClubSession", GetCmdName());
				m_State = STATE_UNADVERTISING_SOCIAL_CLUB_SESSION;
			}
			else
			{
				rlError("%s :: UnadvertiseSocialClubSession :: Unadvertise Failed - Finished", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_UNADVERTISING_SOCIAL_CLUB_SESSION:
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlDebug("%s :: UnadvertisingSocialClubSession :: Succeeded, Clearing MatchId - Finished", GetCmdName());
				m_Session->m_Data.m_MatchId.Clear();
				m_Status->SetSucceeded();
			}
			else
			{
				rlError("%s :: UnadvertisingSocialClubSession :: Failed - Finished", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdModifyPresenceFlags
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdModifyPresenceFlags::CmdModifyPresenceFlags(rlSession* session,
                                                          netStatus* status)
: Cmd(CMD_MODIFY_PRESENCE_FLAGS, session, status)
, m_PresenceFlags(0)
{
}

void
rlSession::CmdModifyPresenceFlags::Update()
{
    if(!m_MyStatus.Pending())
    {
		if(m_MyStatus.Succeeded())
		{
			rlDebug("%s :: Modified Presence flags. Now: 0x%x, Prev: 0x%x",
				GetCmdName(),
				m_Session->m_Data.m_Config.m_PresenceFlags,
				m_PresenceFlags);

			m_Session->m_Data.m_Config.m_PresenceFlags = m_PresenceFlags;
		}
#if !__NO_OUTPUT
		else
		{
			rlDebug("%s :: Failed to modify Presence flags", GetCmdName());
		}
#endif

        m_Status->SetStatus(m_MyStatus.GetStatus());
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdSendInvites
///////////////////////////////////////////////////////////////////////////////
#if RSG_PC
CompileTimeAssert(RL_MAX_GAMERS_PER_SESSION <= RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS);
#endif

rlSession::CmdSendInvites::CmdSendInvites(rlSession* session,
                                            const rlGamerHandle* gamerHandles,
                                            const unsigned numGamers,
                                            const char* subject,
                                            const char* salutation,
                                            netStatus* status)
: Cmd(CMD_SEND_INVITES, session, status)
, m_NumGamers(numGamers)
{
    rlAssert(salutation);
    rlAssert(numGamers > 0);
    rlAssert(numGamers < RL_MAX_GAMERS_PER_SESSION);
    safecpy(m_Subject, subject, RL_MAX_INVITE_SUBJECT_CHARS);
    safecpy(m_Salutation, salutation, RL_MAX_INVITE_SALUTATION_CHARS);

	for (unsigned i = 0; i < numGamers; ++i)
    {
		rlDebug("%s :: Sending invite to %s", GetCmdName(), gamerHandles[i].ToString());
		m_GamerHandles[i] = gamerHandles[i];
    }

#if RSG_NP
    m_State = STATE_SEND_INVITES;
#elif RSG_DURANGO
	m_State = STATE_CHECK_PARTY;
#endif
}

void
rlSession::CmdSendInvites::Update()
{
#if RSG_NP
	switch(m_State)
	{
	case STATE_SEND_INVITES:
		{
			const eNpSendMsgResult result = g_rlNp.GetBasic().SendInviteMsg(m_Session->GetOwner().GetLocalIndex(), 
				&m_Session->GetWebApiSessionId(), 
				m_GamerHandles,
				m_NumGamers,
				m_Session->GetSessionInfo(),
				m_Salutation);

			m_MyStatus.SetPending();
			m_MyStatus.SetStatus((result == RLNP_SENDMSG_SUCCESS) ? netStatus::NET_STATUS_SUCCEEDED : netStatus::NET_STATUS_FAILED);
            m_Status->SetStatus(m_MyStatus.Succeeded() ? netStatus::NET_STATUS_SUCCEEDED : netStatus::NET_STATUS_FAILED);

			rlDebug("%s :: SendInvites :: SendInviteMsg %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
		}
		break;
	}
#elif RSG_GDK
	switch (m_State)
	{ 
		case STATE_SEND_INVITES:
		{
			bool success = g_rlXbl.GetSessionManager()->SendInvites(m_Session->GetSessionInfo().GetSessionHandle(),
																	m_GamerHandles, 
																	m_NumGamers, 
																	m_Session->GetOwner().GetLocalIndex(), 
																	&m_MyStatus);
			if(success)
			{
				rlDebug("%s :: SendInvites :: SendInvites Succeeded - State -> SendingInvites", GetCmdName()); 
				m_State = STATE_SENDING_INVITES;
			}
			else
			{
				rlError("%s :: SendInvites :: SendInvites Failed - Finished", GetCmdName());
				m_Status->SetStatus(netStatus::NET_STATUS_FAILED);
			}
		}
		break;

		case STATE_SENDING_INVITES:
		{
			if(!m_MyStatus.Pending())
			{
				rlDebug("%s :: SendingInvites :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
				m_Status->SetStatus(m_MyStatus.Succeeded() ? netStatus::NET_STATUS_SUCCEEDED : netStatus::NET_STATUS_FAILED);
			}
		}
		break;

		case STATE_CHECK_PARTY:
		case STATE_CREATING_PARTY:
		{
			rlAssertf(false, "%s :: Unhandled state", GetCmdName());
		}
		break;
	}
#elif RSG_XBL
	switch(m_State)
	{
	case STATE_CHECK_PARTY:
		if(!g_rlXbl.GetPartyManager()->IsInParty())
		{
			int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
			if(g_rlXbl.GetPartyManager()->AddLocalUsersToParty(localGamerIndex, &m_MyStatus))
			{
				rlDebug("%s :: CheckParty :: AddLocalUsersToParty Succeeded - State -> CreatingParty", GetCmdName());
				m_State = STATE_CREATING_PARTY;
			}
			else
			{
				rlError("%s :: CheckParty :: AddLocalUsersToParty Failed - Finished", GetCmdName());
				m_Status->SetFailed();
			}
		}
		else
		{
			rlDebug("%s :: CheckParty :: In Party - State -> SendInvites", GetCmdName());
			m_State = STATE_SEND_INVITES;
		}
		break;

	case STATE_CREATING_PARTY:
		{
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					rlDebug("%s :: CreatingParty :: Succeeded - State -> SendInvites", GetCmdName());
					m_State = STATE_SEND_INVITES;
				}
				else
				{
					rlError("%s :: CreatingParty :: Failed - Finished", GetCmdName());
					m_Status->SetFailed();
				}
			}
		}
		break;

	case STATE_SEND_INVITES:
		{
			int localGamerIndex = m_Session->GetOwner().GetLocalIndex();
			if(g_rlXbl.GetPartyManager()->InviteToParty(localGamerIndex, m_GamerHandles, m_NumGamers, &m_MyStatus))
			{
				rlDebug("%s :: SendInvites :: InviteToParty Succeeded - State -> SendingInvites", GetCmdName());
				m_State = STATE_SENDING_INVITES;
			}
			else
			{
				rlError("%s :: SendInvites :: InviteToParty Failed - Finished", GetCmdName());
				m_Status->SetFailed();
			}
		}
		break;

	case STATE_SENDING_INVITES:
		{
			if(!m_MyStatus.Pending())
			{
				rlDebug("%s :: SendingInvites :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
				m_Status->SetStatus(m_MyStatus.Succeeded() ? netStatus::NET_STATUS_SUCCEEDED : netStatus::NET_STATUS_FAILED);
			}
		}
		break;
	}
#else
	if(!m_MyStatus.Pending())
	{
		rlDebug("%s :: SendInvites :: %s - Finished", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
		m_Status->SetStatus(m_MyStatus.Succeeded() ? netStatus::NET_STATUS_SUCCEEDED : netStatus::NET_STATUS_FAILED);
	}
#endif
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession::CmdSendPartyInvites
///////////////////////////////////////////////////////////////////////////////

rlSession::CmdSendPartyInvites::CmdSendPartyInvites(rlSession* session,
                                          const int localInviterIndex,
                                          netStatus* status)
                                          : Cmd(CMD_SEND_PARTY_INVITES, session, status)
{
    m_LocalInviterIndex = localInviterIndex;
}

void
rlSession::CmdSendPartyInvites::Update()
{
    if(!m_MyStatus.Pending())
    {
        rlDebug("%s :: %s", GetCmdName(), m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
        m_Status->SetStatus(m_MyStatus.GetStatus());
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlSession
///////////////////////////////////////////////////////////////////////////////

rlSession::rlSession()
	: m_MemberCount(0)
    , m_PubSlotCount(0)
    , m_PrivSlotCount(0)
	, m_NumReservedSlots(0)
    , m_Cmd(NULL)
    , m_QosUserDataSize(0)
	, m_SessionUserDataSize(0)
	, m_AnswerLanQueriesEnabled(true)
    , m_Initialized(false)
{
    this->Clear();
    rlSessionManager::RegisterSession(this);
}

rlSession::~rlSession()
{
    this->Shutdown(false);
    rlSessionManager::UnregisterSession(this);
}

bool
rlSession::Init()
{
    bool success = false;

    rtry
    {
        rverify(!m_Initialized, catchall,);

        rlAssert(!m_Cmd);

		this->Clear();

        success = m_Initialized = true;
    }
    rcatchall
    {

	}

    return success;
}

void
rlSession::Shutdown(bool isSudden)
{
    if(m_Initialized)
    {
        if(m_Cmd)
        {
			// if this is a sudden termination, cancel in-flight command
			if(isSudden && m_Cmd->CanCancel())
			{
				m_Cmd->Cancel();
			}

			rlDelete(m_Cmd);
			m_Cmd = NULL;
        }

		this->Clear();

        m_Initialized = false;
    }
}

void
rlSession::Update()
{
    if(m_Initialized)
    {
        if(m_Cmd)
        {
            m_Cmd->Update();

            if(!m_Cmd->m_Status->Pending())
            {
				rlDebug("Update :: Cmd Finished - %s, Success: %s", m_Cmd->GetCmdName(), m_Cmd->m_Status->Succeeded() ? "True" : "False");
                rlDelete(m_Cmd);
                m_Cmd = NULL;
            }
        }
    }
}

bool
rlSession::Host(const int localGamerIndex,
                const rlNetworkMode netMode,
                const int numPublicSlots,
                const int numPrivateSlots,
                const rlMatchingAttributes& attrs,
                const unsigned createFlags,
                netStatus* status)
{
    rlAssert(m_Initialized);
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
    rlAssert(RL_NETMODE_LAN == netMode || RL_NETMODE_ONLINE == netMode);
    rlAssert(attrs.IsValid());
    rlAssert(numPublicSlots >= 0);
    rlAssert(numPrivateSlots >= 0);
    rlAssert(numPublicSlots + numPrivateSlots > 0);
	rlAssert(numPublicSlots + numPrivateSlots <= RL_MAX_GAMERS_PER_SESSION);

    bool success = false;

    CmdHost* cmd = NULL;

    rtry
    {
        rverify(rlRos::HasPrivilege(localGamerIndex,
                                    RLROS_PRIVILEGEID_MULTIPLAYER),
                catchall,
                rlError("Local gamer %d doesn't have permission to host a session",
                        localGamerIndex));

        rcheck(rlGamerInfo::HasMultiplayerPrivileges(localGamerIndex),
                catchall,
                rlError("Local gamer %d doesn't have permission to host a session",
                        localGamerIndex));

        rlGamerInfo ginfo;
        rverify(rlPresence::GetGamerInfo(localGamerIndex, &ginfo),catchall, rlError("Failed to get gamer info"));

#if !__NO_OUTPUT
        rlDebug("Gamer:\"%s\" - hosting session...", ginfo.GetName());

        rlDebug("Matchmaking Attributes:");
        for(int i = 0; i < (int) attrs.GetCount(); ++i)
        {
            const u32* val = attrs.GetValueByIndex(i);
            if(val)
            {
                rlDebug("  0x%08x:       0x%08x", attrs.GetAttrId(i), *val);
            }
            else
            {
                rlDebug("  0x%08x:       nil", attrs.GetAttrId(i));
            }
        }

        rlDebug("  Game Mode:    0x%08x", attrs.GetGameMode());
        rlDebug("  Session Purpose: 0x%08x", attrs.GetSessionPurpose());
#endif  //__NO_OUTPUT

        rverify(this->IsDormant(), catchall, rlError("Session is not dormant!"));

        rverify(!this->IsPending(), catchall, rlError("Session has pending operation!"));

        rverify(ginfo.IsSignedIn(), catchall, rlError("Local gamer is not signed in!"));

        //Make sure there are no nil attributes on non-private sessions
        if(numPublicSlots > 0)
        {
            for(int i = 0; i < (int) attrs.GetCount(); ++i)
            {
                rverify(attrs.GetValueByIndex(i),
                        catchall,
                        rlError("Session attribute %d is nil", i));
            }
        }

        m_Data.Clear();

        cmd = RL_ALLOCATE_T(rlSession, CmdHost);

        rverify(cmd, catchall, rlError("Error allocating CmdHost"));

        new(cmd) CmdHost(this,
                            ginfo,
                            netMode,
                            numPublicSlots,
                            numPrivateSlots,
                            attrs,
                            createFlags,
                            status);

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::Activate(const int localGamerIndex,
                    const u64 hostPeerId,
					const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const unsigned createFlags,
                    const unsigned presenceFlags,
                    const unsigned maxPublicSlots,
                    const unsigned maxPrivateSlots,
                    const rlSlotType slotType,
                    netStatus* status)
{
    rlAssert(m_Initialized);
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
    rlAssert(RL_NETMODE_LAN == netMode || RL_NETMODE_ONLINE == netMode);
    rlAssertf(RL_NETMODE_ONLINE == netMode || !(presenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE)
            , "Only ONLINE sessions can be invitable");
	rlAssertf(RL_NETMODE_ONLINE == netMode || !(presenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE)
            , "Only ONLINE sessions can have join via presence");

    bool success = false;

    CmdActivate* cmd = NULL;

    rtry
    {
        rlGamerInfo ginfo;
        rverify(rlPresence::GetGamerInfo(localGamerIndex, &ginfo),catchall,);

        rlDebug("Gamer:\"%s\" - activating session...", ginfo.GetName());

        rverify(this->IsDormant(), catchall,);

        rverify(!this->IsPending(), catchall,);

        rverify(ginfo.IsSignedIn(), catchall,);

        m_Data.Clear();

        cmd = RL_ALLOCATE_T(rlSession, CmdActivate);

        rverify(cmd, catchall, rlError("Error allocating CmdActivate"));

        new(cmd) CmdActivate(this,
                            ginfo,
							hostPeerId,
                            sessionInfo,
                            netMode,
                            createFlags,
                            presenceFlags,
                            maxPublicSlots,
                            maxPrivateSlots,
                            slotType,
                            status);

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::Establish(const rlSessionConfig& config, netStatus* status)
{
    rlAssert(m_Initialized);

    bool success = false;

    CmdEstablish* cmd = NULL;

    rlDebug("Gamer:\"%s\" - establishing session...",
            this->GetOwner().GetName());

    rtry
    {
        rverify(!this->IsPending(), catchall,);
        rverify(this->IsActivated(), catchall,);
        rverify(!this->IsEstablished(), catchall,);

        rverify(config.m_NetworkMode == m_Data.m_Config.m_NetworkMode,
                catchall,
                rlError("Network mode does not match network mode used in Activate()"));

        rverify(RL_NETMODE_ONLINE == config.m_NetworkMode
                || RL_NETMODE_LAN == config.m_NetworkMode,
                catchall,);

        rverify(RL_NETMODE_ONLINE == config.m_NetworkMode
                || !(config.m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE),
                catchall,
                rlError("Only ONLINE sessions can be invitable"));
		
		rverify(RL_NETMODE_ONLINE == config.m_NetworkMode
                || !(config.m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE),
                catchall,
                rlError("Only ONLINE sessions can have join via presence"));

        //Disallow nil attributes.
        const rlMatchingAttributes& attrs = config.m_Attrs;

        for(int i = 0; i < (int) attrs.GetCount(); ++i)
        {
            rverify(attrs.GetValueByIndex(i),
                    catchall,
                    rlError("Session attribute %d is nil", i));
        }

        cmd = RL_ALLOCATE_T(rlSession, CmdEstablish);

        rverify(cmd, catchall, rlError("Error allocating CmdEstablish"));

        new(cmd) CmdEstablish(this, status);

        rverify(cmd->m_Data.Reset(m_Data.m_LocalOwner,
								m_Data.m_HostPeerId,
                                &m_Data.m_SessionInfo,
                                config.m_NetworkMode,
                                config.m_MaxPublicSlots,
                                config.m_MaxPrivateSlots,
                                config.m_Attrs,
                                config.m_CreateFlags,
                                config.m_PresenceFlags,
                                config.m_SessionId,
                                config.m_ArbCookie),
                catchall,
                rlError("Error configuring session"));

#if RSG_DURANGO
        cmd->m_Data.m_hSession = m_Data.m_hSession;
#endif

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::HostOffline(const int localGamerIndex,
                        const int numSlots,
                        const rlMatchingAttributes& attrs,
                        netStatus* status)
{
    rlAssert(m_Initialized);
    rlAssert(attrs.IsValid());

    bool success = false;

    CmdHostOffline* cmd = NULL;

    rtry
    {
        rlGamerInfo ginfo;
        rverify(rlPresence::GetGamerInfo(localGamerIndex, &ginfo),catchall,);

        rlDebug("Gamer:\"%s\" - creating offline session...", ginfo.GetName());

        rverify(this->IsDormant(), catchall,);

        rverify(!this->IsPending(), catchall,);

        m_Data.Clear();

        cmd = RL_ALLOCATE_T(rlSession, CmdHostOffline);

        rverify(cmd, catchall, rlError("Error allocating CmdHostOffline"));

        new(cmd) CmdHostOffline(this,
                                ginfo,
                                numSlots,
                                attrs,
                                status);

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::Destroy(netStatus* status)
{
    bool success = false;

    CmdDestroy* cmd = NULL;

    rtry
    {
        rlDebug("Destroying session...");

        if(this->IsDormant())
        {
            rlDebug("Session is dormant - no need to destroy");
        }
        else
        {
            rverify(this->IsActivated(), catchall,);

            rverify(!this->IsPending(), catchall,);

            m_Data.m_State = STATE_DESTROYING;

            cmd = RL_ALLOCATE_T(rlSession, CmdDestroy);

            rverify(cmd, catchall, rlError("Error allocating CmdDestroy"));

            new(cmd) CmdDestroy(this, status);

            rcheck(this->DoCmd(cmd), catchall,);
        }

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::Join(const rlGamerHandle* gamerHandles,
                const rlSlotType* slotTypes,
                const unsigned numGamers,
                netStatus* status)
{
    bool success = false;

    CmdJoin* cmd = 0;

    rtry
    {
        rverify(this->IsEstablished(), catchall,);

        rverify(!this->IsPending(), catchall,);

        rverify(numGamers <= RL_MAX_GAMERS_PER_SESSION, catchall,);

        rverify(numGamers > 0, catchall,);

        rverify(rlRos::HasPrivilege(this->GetOwner().GetLocalIndex(),
                                        RLROS_PRIVILEGEID_MULTIPLAYER),
                catchall,
                rlError("Local gamer %d doesn't have permission to join a session",
                        this->GetOwner().GetLocalIndex()));

        rverify(rlGamerInfo::HasMultiplayerPrivileges(this->GetOwner().GetLocalIndex()),
                catchall,
                rlError("Local gamer %d doesn't have permission to join a session",
                        this->GetOwner().GetLocalIndex()));

        cmd = RL_ALLOCATE_T(rlSession, CmdJoin);

        rverify(cmd, catchall, rlError("Error allocating CmdJoin"));

        new(cmd) CmdJoin(this, status);

        cmd->m_NumGamers = 0;

        unsigned numPriv = 0;

        for(int i = 0; i < (int) numGamers; ++i)
        {
#if !__NO_OUTPUT
			char ghBuf[RL_MAX_GAMER_HANDLE_CHARS];
			gamerHandles[i].ToString(ghBuf);
#endif
			rlDebug("\"%s\" requesting to join in %s slot", ghBuf, RL_SLOT_PRIVATE == slotTypes[i] ? "PRIVATE" : "PUBLIC");

            if(this->IsMember(gamerHandles[i]))
            {
                rlWarning("\"%s\" is already a member of the session", ghBuf);
                continue;
            }

            cmd->m_GamerHandles[cmd->m_NumGamers] = gamerHandles[i];
            cmd->m_SlotTypes[cmd->m_NumGamers] = slotTypes[i];

            if(RL_SLOT_PRIVATE == slotTypes[i])
            {
                ++numPriv;
            }

            ++cmd->m_NumGamers;
        }

        rverify(cmd->m_NumGamers > 0,
                catchall,
                rlError("No valid joiners"));

        const unsigned numPub = cmd->m_NumGamers - numPriv;

        rverify(this->GetEmptySlots(RL_SLOT_PUBLIC) >= numPub,
                catchall,
                rlError("Not enough public slots - Requested: %u, Empty: %u, Used: %u, Max: %u", numPub, this->GetEmptySlots(RL_SLOT_PUBLIC), m_PubSlotCount, m_Data.m_Config.m_MaxPublicSlots));

        rverify(this->GetEmptySlots(RL_SLOT_PRIVATE) >= numPriv,
                catchall,
                rlError("Not enough private slots - Requested: %u, Empty: %u, Used: %u, Max: %u", numPriv, this->GetEmptySlots(RL_SLOT_PRIVATE), m_PrivSlotCount, m_Data.m_Config.m_MaxPrivateSlots));

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::Leave(const rlGamerHandle* gamerHandles,
                const unsigned numGamers,
                netStatus* status)
{
    bool success = false;

    CmdLeave* cmd = 0;

    rtry
    {
        rverify(this->IsEstablished(), catchall,);

        rverify(!this->IsPending(), catchall,);

        rverify(numGamers <= RL_MAX_GAMERS_PER_SESSION, catchall,);

        rverify(numGamers > 0, catchall,);

        cmd = RL_ALLOCATE_T(rlSession, CmdLeave);

        rverify(cmd, catchall, rlError("Error allocating CmdLeave"));

        new(cmd) CmdLeave(this, status);

        cmd->m_NumGamers = 0;

        for(int i = 0; i < (int) numGamers; ++i)
        {
            OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);

            if(!this->IsMember(gamerHandles[i]))
            {
                rlWarning("\"%s\" is not a member of the session",
                            gamerHandles[i].ToString(ghBuf));
                continue;
            }

            cmd->m_GamerHandles[cmd->m_NumGamers] = gamerHandles[i];
			cmd->m_SlotTypes[cmd->m_NumGamers] = this->GetSlotType(gamerHandles[i]);
			rlAssert(cmd->m_SlotTypes[cmd->m_NumGamers] != RL_SLOT_INVALID);

            ++cmd->m_NumGamers;

            rlDebug("%s gamer:\"%s\" is leaving session...",
                        gamerHandles[i].IsLocal() ? "Local" : "Remote",
                        gamerHandles[i].ToString(ghBuf));
        }

        rverify(cmd->m_NumGamers > 0,
                catchall,
                rlError("No valid leavers"));

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool 
rlSession::EnableMatchmakingAdvertise(netStatus* status)
{
	bool success = false;

	rlDebug("Enabling matchmaking advertising...");

	if(m_Data.m_Config.IsMatchmakingAdvertisingEnabled())
	{
		//Already enabled, nothing to do here
		if(status)
		{
			status->SetPending();
			status->SetSucceeded();
		}
		rlDebug("Matchmaking advertising already enabled...");
		return true;
	}

	//Remove the flag (for hosts and clients)
	m_Data.m_Config.m_CreateFlags &= ~RL_SESSION_CREATE_FLAG_MATCHMAKING_ADVERTISE_DISABLED;
	
	//We might need to advertise now
	CmdUpdateScAdvertisement* cmd = 0;

	rtry
	{
		// Only for hosts
		rcheck(this->IsHost(), catchall, rlError("Not host"));

		rcheck(this->IsEstablished(), catchall, rlError("Session not established!"));

		rverify(!this->IsPending(), catchall, rlError("Pending command!"));

		cmd = RL_ALLOCATE_T(rlSession, CmdUpdateScAdvertisement);

		rverify(cmd, catchall, rlError("Error allocating CmdUpdateScAdvertisement"));

		new(cmd) CmdUpdateScAdvertisement(this, status);

		rcheck(this->DoCmd(cmd), catchall, rlError("Error starting CmdUpdateScAdvertisement"));
	}
	rcatchall
	{
		if(cmd)
		{
			cmd->m_Status->SetFailed();
			rlDelete(cmd);
		}
		else if(status)
		{
			status->ForceFailed();
		}
	}

	return success;
}

bool
rlSession::ChangeAttributes(const rlMatchingAttributes& newAttrs,
                            netStatus* status)
{
    bool success = false;

    rlDebug("Changing attributes on session...");

    CmdChangeAttrs* cmd = 0;

    rtry
    {
        rverify(this->IsEstablished(),
                catchall,
                rlError("Cannot change attributes - not established"));

        rverify(!this->IsPending(), catchall,);

        rverify(newAttrs.IsValid(),
                catchall,
                rlError("Invalid matching attributes"));

        rverify(newAttrs.GetCount() > 0
                || newAttrs.GetGameMode() != m_Data.m_Config.m_Attrs.GetGameMode()
                || newAttrs.GetSessionPurpose() != m_Data.m_Config.m_Attrs.GetSessionPurpose(),
                catchall,
                rlError("No new attributes specified"));

        rverify(newAttrs.GetCount() <= m_Data.m_Config.m_Attrs.GetCount(),
                catchall,
                rlError("Too many new attributes"));

        //Replace existing attributes with new attributes.

        rlMatchingAttributes attrs = m_Data.m_Config.m_Attrs;
        unsigned numDiffer = 0;

        if(RL_INVALID_GAME_MODE != newAttrs.GetGameMode()
            && attrs.GetGameMode() != newAttrs.GetGameMode())
        {
            attrs.SetGameMode(newAttrs.GetGameMode());
            ++numDiffer;
        }

        if(RL_INVALID_SESSION_TYPE != newAttrs.GetSessionPurpose()
           && attrs.GetSessionPurpose() != newAttrs.GetSessionPurpose())
        {
            attrs.SetSessionPurpose(newAttrs.GetSessionPurpose());
            ++numDiffer;
        }

        for(int i = 0; i < (int) newAttrs.GetCount(); ++i)
        {
			const u32 attrId = newAttrs.GetAttrId(i);
            const u32* val = newAttrs.GetValueByIndex(i);

            if(val && *val != *attrs.GetValueById(attrId))
            {
                attrs.SetValueById(attrId, *val);
                ++numDiffer;
            }
        }

        if(numDiffer > 0)
        {
            cmd = RL_ALLOCATE_T(rlSession, CmdChangeAttrs);

            rverify(cmd, catchall, rlError("Error allocating CmdChangeAttrs"));

            new(cmd) CmdChangeAttrs(this, status);

            cmd->m_NewAttrs = attrs;

            rcheck(this->DoCmd(cmd), catchall,);
        }
        else
        {
            rlWarning("No attributes differ");

            if(status)
            {
                status->SetPending();
                status->SetSucceeded();
            }
        }

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::MigrateHost(const rlPeerInfo& newHostPeerInfo,
                        const rlSessionInfo* sessionInfo,
                        netStatus* status)
{
    rlAssert(this->IsNetwork());
    //Make sure if we're not the new host that we have a non-NULL
    //sessionInfo.
    rlAssert(!sessionInfo || newHostPeerInfo != this->GetOwner().GetPeerInfo());

    bool success = false;

    rlDebug("Migrating...");

    const bool iamhost = newHostPeerInfo == this->GetOwner().GetPeerInfo();

    rlDebug("%s will become the new host",
             iamhost ? "I" : "A remote peer");

    CmdMigrate* cmd = 0;

    rtry
    {
        rverify(this->IsEstablished(), catchall,);

        rverify(!this->IsPending(), catchall,);

        cmd = RL_ALLOCATE_T(rlSession, CmdMigrate);

        rverify(cmd, catchall, rlError("Error allocating CmdMigrate"));

        new(cmd) CmdMigrate(this, status);

        rverify((!iamhost && sessionInfo) || (iamhost && !sessionInfo),
                catchall,
                rlError("sessionInfo must be null if I am the host, and vice versa"));

        cmd->m_NewHostPeerInfo = newHostPeerInfo;
        cmd->m_NewSessionInfo =
            sessionInfo ? *sessionInfo : m_Data.m_SessionInfo;

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::ModifyPresenceFlags(unsigned presenceFlags, netStatus* status)
{
    bool success = false;

    rlDebug("%s invites. Currently %s", (presenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE) ? "Enabling" : "Disabling",
        (m_Data.m_Config.m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE) ? "Enabled" : "Disabled");
	rlDebug("%s join via presence. Currently %s", (presenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE) ? "Enabling" : "Disabling",
        (m_Data.m_Config.m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE) ? "Enabled" : "Disabled");

    rlAssert(this->IsNetwork());

    CmdModifyPresenceFlags* cmd = 0;

    rtry
    {
        rverify(this->IsEstablished(), catchall,);
        rverify(!this->IsPending(), catchall,);
        rverify(this->IsOnline(),
                catchall,
                rlError("Can't change presence flags on a non-online session"));
        rverify(!(m_Data.m_Config.m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED),
                catchall,
                rlError("Can't change presence flags on a session created with the RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED flag"));

        if(presenceFlags != m_Data.m_Config.m_PresenceFlags)
        {
            cmd = RL_ALLOCATE_T(rlSession, CmdModifyPresenceFlags);

            rverify(cmd,
                    catchall,
                    rlError("Error allocating CmdModifyPresenceFlags"));

            new(cmd) CmdModifyPresenceFlags(this, status);

            cmd->m_PresenceFlags = presenceFlags;

            rcheck(this->DoCmd(cmd), catchall,);
        }
        else if(status)
        {
            status->SetPending();
            status->SetSucceeded();
        }

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool
rlSession::UpdateAvailableSlots(netStatus* status)
{
	bool success = false;
	
	rtry
	{
		rlDebug("Updating matchmaking slots: %d/%d free public slots, %d/%d free private slots, %d reserved", 
				GetEmptySlots(RL_SLOT_PUBLIC), 
				GetMaxSlots(RL_SLOT_PUBLIC), 
				GetEmptySlots(RL_SLOT_PRIVATE), 
				GetMaxSlots(RL_SLOT_PRIVATE), 
				GetNumReservedSlots());

		rverify(m_Data.m_MatchId.IsValid(), catchall, );

		int numAvailableSlots = rage::Max((int)GetEmptySlots(RL_SLOT_PUBLIC) - (int)GetNumReservedSlots(), 0);
		success = rlScMatchmaking::Update(GetOwner().GetLocalIndex(),
											m_Data.m_MatchId,
											GetMaxSlots(),
											numAvailableSlots,
											m_Data.m_SessionInfo,
											m_Data.m_Config.m_Attrs,
											status);
	}
	rcatchall
	{
		
	}

	return success;
}

bool 
rlSession::Cancel(netStatus* status)
{
    if(!m_Cmd || 
       !m_Cmd->m_Status || 
        m_Cmd->m_Status != status)
    {
        return false;
    }

    if(m_Cmd->CanCancel())
    {
        m_Cmd->Cancel();

        rlDelete(m_Cmd);
        m_Cmd = NULL;

        return true; 
    }

    return false;
}

unsigned 
rlSession::GetPresenceFlags() const
{
	rlAssert(this->IsEstablished());
	return m_Data.m_Config.m_PresenceFlags;
}

bool
rlSession::HasScMatchId() const
{
	return m_Data.m_MatchId.IsValid();
}

const rlScMatchmakingMatchId& 
rlSession::GetScMatchId()
{
	return m_Data.m_MatchId;
}

bool
rlSession::IsInvitable() const
{
    return this->IsEstablished()
            && this->IsOnline()
            && (0 != (m_Data.m_Config.m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE));
}

bool
rlSession::IsJoinableViaPresence() const
{
	return this->IsEstablished()
		&& this->IsOnline()
		&& (0 != (m_Data.m_Config.m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE));
}

bool
rlSession::SendInvites(const rlGamerHandle* gamerHandles,
                        const unsigned numGamers,
                        const char* subject,
                        const char* salutation,
                        netStatus* status)
{
    if(!subject)
    {
        subject = "";
    }
    if(!salutation)
    {
        salutation = "";
    }

    rlAssert(strlen(subject) < RL_MAX_INVITE_SUBJECT_CHARS);
    rlAssert(strlen(salutation) < RL_MAX_INVITE_SALUTATION_CHARS);

    bool success = false;

    rlDebug("Sending invites to:%d gamers...", numGamers);

    CmdSendInvites* cmd = 0;

    rtry
    {
        rverify(this->IsEstablished(), catchall,);
        rverify(!this->IsPending(), catchall,);
        rverify(this->IsInvitable(), catchall,);
        rverify(numGamers > 0, catchall,);
        rverify(numGamers < RL_MAX_GAMERS_PER_SESSION, catchall,);

        rlGamerHandle tmpHandles[RL_MAX_GAMERS_PER_SESSION];
        unsigned tmpCount = 0;

        //Make sure we're not inviting someone who is already in the session.
        for(int i = 0; i < (int) numGamers; ++i)
        {
            bool haveit = false;
            for(int j = 0; j < (int) m_MemberCount; ++j)
            {
                if(gamerHandles[i] == m_Slots[j].m_hGamer)
                {
                    haveit = true;
                    break;
                }
            }

            if(!haveit)
            {
                tmpHandles[tmpCount++] = gamerHandles[i];
            }
        }

        if(tmpCount > 0)
        {
            cmd = RL_ALLOCATE_T(rlSession, CmdSendInvites);

            rverify(cmd, catchall, rlError("Error allocating CmdSendInvites"));

            new(cmd) CmdSendInvites(this, tmpHandles, tmpCount, subject, salutation, status);

            rcheck(this->DoCmd(cmd), catchall,);
        }
        else
        {
            rlDebug("Not sending invites - all gamers are already in the session");
        }

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

bool 
rlSession::SendPartyInvites(const int localInviterIndex,
                            netStatus* status)
{
    bool success = false;

    rlDebug("Sending invites to platform party members...");

    CmdSendPartyInvites* cmd = 0;

    rtry
    {
        rverify(this->IsEstablished(), catchall,);
        rverify(!this->IsPending(), catchall,);
        rverify(this->IsInvitable(), catchall,);
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localInviterIndex), catchall,);

        cmd = RL_ALLOCATE_T(rlSession, CmdSendPartyInvites);

        rverify(cmd, catchall, rlError("Error allocating CmdSendPartyInvites"));

        new(cmd) CmdSendPartyInvites(this, localInviterIndex, status);

        rcheck(this->DoCmd(cmd), catchall,);

        success = true;
    }
    rcatchall
    {
        if(cmd)
        {
            cmd->m_Status->SetFailed();
            rlDelete(cmd);
        }
        else if(status)
        {
            status->ForceFailed();
        }
    }

    return success;
}

void
rlSession::SetAnswerLanQueriesEnabled(const bool enabled)
{
    rlDebug("Answering LAN queries: %s", enabled ? "ENABLED" : "DISABLED");

    m_AnswerLanQueriesEnabled = enabled;
}

bool
rlSession::IsAnswerLanQueriesEnabled() const
{
    return this->IsHost() && m_AnswerLanQueriesEnabled;
}

const rlSessionConfig&
rlSession::GetConfig() const
{
    rlAssert(this->IsEstablished());
    return m_Data.m_Config;
}

const rlMatchingAttributes&
rlSession::GetAttributes() const
{
    return m_Data.m_Config.m_Attrs;
}

unsigned
rlSession::GetCreateFlags() const
{
    rlAssert(this->IsEstablished());
    return m_Data.m_Config.m_CreateFlags;
}

const u64
rlSession::GetHostPeerId() const
{
	rlAssert(this->IsActivated());
	return m_Data.m_HostPeerId;
}

const rlPeerAddress&
rlSession::GetHostPeerAddress() const
{
    rlAssert(this->IsActivated());
    return m_Data.m_SessionInfo.GetHostPeerAddress();
}

const rlGamerInfo&
rlSession::GetOwner() const
{
    rlAssert(this->IsActivated());
    return m_Data.m_LocalOwner;
}

u64
rlSession::GetId() const
{
    return m_Data.m_Config.m_SessionId;
}

const void*
rlSession::GetSessionHandle() const
{
#if RSG_DURANGO
    return m_Data.m_hSession;
#else
    return NULL;
#endif
}

unsigned
rlSession::GetMaxSlots(const rlSlotType slotType) const
{
    rlAssert(RL_SLOT_PUBLIC == slotType || RL_SLOT_PRIVATE == slotType);
    return RL_SLOT_PUBLIC == slotType ? m_Data.m_Config.m_MaxPublicSlots : m_Data.m_Config.m_MaxPrivateSlots;
}

unsigned
rlSession::GetMaxSlots() const
{
    return this->GetMaxSlots(RL_SLOT_PUBLIC) + this->GetMaxSlots(RL_SLOT_PRIVATE);
}

unsigned
rlSession::GetFilledSlots(const rlSlotType slotType) const
{
    rlAssert(this->IsEstablished());
    rlAssert(RL_SLOT_PUBLIC == slotType || RL_SLOT_PRIVATE == slotType);
    return RL_SLOT_PUBLIC == slotType ? m_PubSlotCount : m_PrivSlotCount;
}

unsigned
rlSession::GetEmptySlots(const rlSlotType slotType) const
{
    rlAssert(this->IsEstablished());
    rlAssert(RL_SLOT_PUBLIC == slotType || RL_SLOT_PRIVATE == slotType);
    return RL_SLOT_PUBLIC == slotType
            ? m_Data.m_Config.m_MaxPublicSlots - m_PubSlotCount
            : m_Data.m_Config.m_MaxPrivateSlots - m_PrivSlotCount;
}

unsigned
rlSession::GetNumReservedSlots() const
{
	rlAssert(this->IsEstablished());
	return m_NumReservedSlots;
}

void rlSession::SetNumReservedSlots(unsigned numReservedSlots)
{
	rlAssert(this->IsEstablished());
	m_NumReservedSlots = numReservedSlots;
}

unsigned
rlSession::GetLocalGamerCount() const
{
    rlAssert(this->IsEstablished());

    unsigned count = 0;
    for(int i = 0; i < (int)m_MemberCount; ++i)
    {
        if(m_Slots[i].m_hGamer.IsLocal())
        {
            ++count;
        }
    }

    return count;
}

unsigned
rlSession::GetMembers(rlGamerHandle* gamerHandles, const unsigned maxGamers) const
{
    rlAssert(this->IsEstablished());

    for(int i = 0; i < (int)m_MemberCount && i < (int)maxGamers; ++i)
    {
        gamerHandles[i] = m_Slots[i].m_hGamer;
    }

    return m_MemberCount;
}

bool
rlSession::IsMember(const rlGamerHandle& gamerHandle) const
{
    rlAssert(this->IsEstablished());
    return this->GetSlotIndex(gamerHandle) >= 0;
}

bool
rlSession::IsInPublicSlot(const rlGamerHandle& gamerHandle) const
{
    rlAssert(this->IsEstablished());
    return RL_SLOT_PUBLIC == this->GetSlotType(gamerHandle);
}

bool
rlSession::IsInPrivateSlot(const rlGamerHandle& gamerHandle) const
{
    rlAssert(this->IsEstablished());
    return RL_SLOT_PRIVATE == this->GetSlotType(gamerHandle);
}

rlSlotType
rlSession::GetSlotType(const rlGamerHandle& gamerHandle) const
{
    rlAssert(this->IsEstablished());
    const int idx = this->GetSlotIndex(gamerHandle);
    return idx >= 0 ? m_Slots[idx].m_SlotType : RL_SLOT_INVALID;
}

const rlSessionInfo&
rlSession::GetSessionInfo() const
{
    rlAssert(this->IsActivated());
    return m_Data.m_SessionInfo;
}

rlNetworkMode
rlSession::GetNetworkMode() const
{
    return this->IsEstablished()
        ? m_Data.m_Config.m_NetworkMode
        : RL_NETMODE_INVALID;
}

bool
rlSession::IsOnline() const
{
    return RL_NETMODE_ONLINE == GetNetworkMode();
}

bool
rlSession::IsLan() const
{
    return RL_NETMODE_LAN == GetNetworkMode();
}

bool
rlSession::IsNetwork() const
{
    return IsOnline() || IsLan();
}

bool
rlSession::IsOffline() const
{
    return RL_NETMODE_OFFLINE == GetNetworkMode();
}

bool
rlSession::IsDormant() const
{
    return STATE_DORMANT == m_Data.m_State;
}

bool
rlSession::IsActivated() const
{
    return m_Data.m_State >= STATE_ACTIVATED
            && m_Data.m_State <= STATE_DESTROYING;
}

bool
rlSession::IsEstablished() const
{
    return m_Data.m_State >= STATE_IDLE
            && m_Data.m_State <= STATE_DESTROYING;
}

bool
rlSession::IsPending() const
{
    return NULL != m_Cmd;
}

bool
rlSession::IsIdle() const
{
    return STATE_IDLE == m_Data.m_State;
}

bool
rlSession::IsHost() const
{
    rlAssert(this->IsActivated());
    return this->IsOffline()
            || (m_Data.m_LocalOwner.GetPeerInfo().GetPeerAddress().GetPeerId() == m_Data.m_SessionInfo.GetHostPeerAddress().GetPeerId());
}

bool
rlSession::SetQosUserData(const u8* data,
                          const u16 size)
{
    if(!rlVerify(IsActivated() && IsHost()))
    {
        rlError("Can only set QoS data on active hosted session");
        return false;
    }

    if(data && size)
    {
        rlAssert(size <= RL_MAX_QOS_USER_DATA_SIZE);
        m_QosUserDataSize = size;

        if(m_QosUserDataSize)
        {
            sysMemCpy(m_QosUserData, data, m_QosUserDataSize);
        }
    }
    else
    {
        m_QosUserDataSize = 0;
    }

    bool success = true;

    rlDebug("%s setting Qos userdata (%u bytes)", success ? "succeeded" : "failed", size);

    return success;
}

u16 
rlSession::GetQosUserDataSize() const
{
    return ((IsActivated() && IsHost()) ? m_QosUserDataSize : 0);
}

const u8* 
rlSession::GetQosUserData()
{
    return ((IsActivated() && IsHost()) ? m_QosUserData : 0);
}

bool
rlSession::SetSessionUserData(const u8* data,
						      const u16 size)
{
	if(!rlVerify(IsActivated()))
	{
		rlError("Can only set session data on active session");
		return false;
	}

	if(data && size)
	{
		rlAssert(size <= RL_MAX_SESSION_USER_DATA_SIZE);
		m_SessionUserDataSize = size;

		if(m_SessionUserDataSize)
		{
			sysMemCpy(m_SessionUserData, data, m_SessionUserDataSize);
		}
	}
	else
	{
		m_SessionUserDataSize = 0;
	}

	return true;
}

u16 
rlSession::GetSessionUserDataSize() const
{
	return ((IsActivated()) ? m_SessionUserDataSize : 0);
}

const u8* 
rlSession::GetSessionUserData() const
{
	return ((IsActivated()) ? m_SessionUserData : 0);
}

#if RSG_NP
const rlSceNpSessionId& rlSession::GetWebApiSessionId() const
{
	return m_Data.m_SessionInfo.GetWebApiSessionId();
}

void rlSession::SetWebApiSessionId(const rlSceNpSessionId& sessionId)
{
	m_Data.m_SessionInfo.SetWebApiSessionId(sessionId);
}

void rlSession::ClearWebApiSessionId()
{
	m_Data.m_SessionInfo.ClearWebApiSessionId();
}

#if RSG_PROSPERO
void rlSession::SetWebApiMatchStatus(rlNpMatchStatus matchStatus, netStatus* status)
{
    rlAssertf(IsEstablished(), "SetWebApiMatchStatus :: Is being called before the session is established");
    if(IsEstablished() && IsHost() && m_Data.m_SessionInfo.m_WepApiMatchId.IsValid())
    {
        int localGamerIndex = GetOwner().GetLocalIndex();

        if (!g_rlNp.GetWebAPI().UpdateMatchStatus(localGamerIndex, m_Data.m_SessionInfo.m_WepApiMatchId, matchStatus, status))
        {
            rlWarning("SetWebApiMatchStatus :: Failed to update match status");
        }
    }
}
#endif // RSG_PROSPERO
#endif // RSG_NP

//private:

void
rlSession::Clear()
{
	rlDebug("Clear");

    rlAssert(!this->IsPending()
            || (m_Cmd && CMD_DESTROY == m_Cmd->m_Id)
            //Clear() can be called when Establish or Host fail and the
            //session is subsequently destroyed.
            || (m_Cmd && CMD_ESTABLISH == m_Cmd->m_Id)
            || (m_Cmd && CMD_HOST_NET == m_Cmd->m_Id)
            || (m_Cmd && CMD_HOST_OFFLINE == m_Cmd->m_Id));

	// if we're the host and still have a valid match id at this point, create a fire-and-forget task to unadvertise it
	if(m_Data.m_MatchId.IsValid())
	{
		rlDebug("Clear :: Unadvertising on matchmaking service");
		rlScMatchmaking::Unadvertise(GetOwner().GetLocalIndex(), m_Data.m_MatchId, NULL);
		m_Data.m_MatchId.Clear(); // we can get multiple rlSession::Clear() calls on the same session, don't fire multiple unadvertise tasks for the same session
	}

#if RSG_DURANGO
	if(m_Data.m_hSession)
	{		
		//Pass no status parameter to allow this task to run detached from the session
		rlGamerHandle gh = this->GetOwner().GetGamerHandle();
		if(gh.IsValid() && IsMember(gh))
		{
			// LeaveSession also destroys the local session object
			rlDebug("Clear :: Leaving platform session");
			g_rlXbl.GetSessionManager()->LeaveSession(m_Data.m_LocalOwner.GetLocalIndex(), m_Data.m_hSession, NULL);
		}
		else
		{
			rlDebug("Clear :: Destroying local session object");
			g_rlXbl.GetSessionManager()->DestroySession(m_Data.m_LocalOwner.GetLocalIndex(), m_Data.m_hSession, NULL);
		}

		m_Data.m_hSession = NULL;
	}
#elif RSG_NP

#if RL_NP_CPPWEBAPI
    if (m_Data.m_SessionInfo.m_WepApiMatchId.IsValid())
    {
        rlGamerHandle gh = this->GetOwner().GetGamerHandle();
        if (gh.IsValid() && IsMember(gh))
        {
            int localGamerIndex = this->GetOwner().GetLocalIndex();
            if (g_rlNp.IsOnline(localGamerIndex))
            {
                rlSceNpAccountId accountId = g_rlNp.GetNpAccountId(localGamerIndex);
                if (rlVerify(accountId != RL_INVALID_NP_ACCOUNT_ID))
                {
                    rlNpMatchLeavingPlayers players;
                    players.Push(rlNpMatchPlayerLeave(gh, rlNpMatchLeaveReason::Quit));

					rlDebug("Clear :: Leaving platform match");
					g_rlNp.GetWebAPI().LeaveMatch(localGamerIndex, m_Data.m_SessionInfo.m_WepApiMatchId, players, nullptr);
                }
            }
        }

        m_Data.m_SessionInfo.m_WepApiMatchId.Clear();
    }
#endif

	if (GetWebApiSessionId().IsValid())
	{
		rlGamerHandle gh = this->GetOwner().GetGamerHandle();
		if(gh.IsValid() && IsMember(gh))
		{
			int localGamerIndex = this->GetOwner().GetLocalIndex();
            if(g_rlNp.IsOnline(localGamerIndex))
            {
                rlSceNpAccountId accountId = g_rlNp.GetNpAccountId(localGamerIndex);
                if(rlVerify(accountId != RL_INVALID_NP_ACCOUNT_ID))
                {
					rlDebug("Clear :: Leaving platform session");
#if RL_NP_CPPWEBAPI
					g_rlNp.GetWebAPI().LeavePlayerSession(localGamerIndex, GetWebApiSessionId(), nullptr);
#else
                    g_rlNp.GetWebAPI().DeleteMember(localGamerIndex, GetWebApiSessionId(), accountId, nullptr);
#endif
                }
            }
		}

        // we can get multiple clear calls, clear the web api session id
        ClearWebApiSessionId();
	}
#endif

	while(m_MemberCount > 0)
	{
		this->RemoveGamer(m_Slots[0].m_hGamer);
	}

	m_PubSlotCount = m_PrivSlotCount = 0; 
	m_NumReservedSlots = 0;

    m_Data.Clear();

    m_SessionUserDataSize = 0;

    m_AnswerLanQueriesEnabled = true;
}

#if !__NO_OUTPUT
const char* 
rlSession::Cmd::GetCmdName() const
{
	static const char* s_Names[] =
	{
		"CmdHost",
		"CmdHostOffline",
		"CmdActivate",
		"CmdEstablish",
		"CmdDestroy",
		"CmdJoin",
		"CmdLeave",
		"CmdChangeAttrs",
		"CmdMigrate",
		"CmdModifyPresenceFlags",
		"CmdSendInvites",
		"CmdSendPartyInvites",
		"CmdUpdateScAdvertisement",
	};

	CompileTimeAssert(COUNTOF(s_Names) == CMD_NUM);

	if(m_Id == CMD_INVALID || m_Id == CMD_NUM)
		return "CMD_INVALID";

	return s_Names[m_Id];
}
#endif

bool
rlSession::DoCmd(Cmd* cmd)
{
    bool success = false;

    if(rlVerify(!m_Cmd))
    {
        if(!cmd->m_Status->Pending())
        {
            cmd->m_Status->SetPending();
        }

        m_Cmd = cmd;

		switch(cmd->m_Id)
        {
            case CMD_HOST_NET:
            case CMD_HOST_OFFLINE:
                success = this->CommonHost((CmdHost*) cmd); break;
            case CMD_ACTIVATE:
                success = this->CommonActivate((CmdActivate*) cmd); break;
            case CMD_ESTABLISH:
                success = this->CommonEstablish((CmdEstablish*) cmd); break;
            case CMD_DESTROY:
                success = this->CommonDestroy((CmdDestroy*) cmd); break;
            case CMD_JOIN:
                success = this->CommonJoin((CmdJoin*) cmd); break;
            case CMD_LEAVE:
                success = this->CommonLeave((CmdLeave*) cmd); break;
            case CMD_CHANGE_ATTRS:
                success = this->CommonChangeAttrs((CmdChangeAttrs*) cmd); break;
            case CMD_SEND_INVITES:
                success = this->CommonSendInvites((CmdSendInvites*) cmd); break;
            case CMD_SEND_PARTY_INVITES:
                success = this->CommonSendPartyInvites((CmdSendPartyInvites*) cmd); break;
            case CMD_MIGRATE:
                success = this->CommonMigrate((CmdMigrate*) cmd); break;
            case CMD_MODIFY_PRESENCE_FLAGS:
                success = this->CommonModifyPresenceFlags((CmdModifyPresenceFlags*) cmd); break;
			case CMD_UPDATE_SC_ADVERTISEMENT:
				success = this->CommonUpdateScAdvertisement((CmdUpdateScAdvertisement*) cmd); break;
			default:
                rlAssert(false); break;
        }

		rlDebug("DoCmd :: %s, Success: %s", cmd->GetCmdName(), success ? "True" : "False");

		if(!success)
		{
			m_Cmd = NULL;
		}
		else if(!m_Cmd->m_Status->Pending())
		{
			rlDebug("DoCmd :: Cmd Finished - %s - Succeeded: %s", cmd->GetCmdName(), m_Cmd->m_Status->Succeeded() ? "True" : "False");
			rlDelete(m_Cmd);
			m_Cmd = NULL;
		}
    }

    return success;
}

bool
rlSession::CommonHost(CmdHost* cmd)
{
    rlAssert(this->IsDormant());

    m_Data.Clear();

    return this->NativeHost(cmd);
}

bool
rlSession::CommonActivate(CmdActivate* cmd)
{
    rlAssert(this->IsDormant());

    m_Data.Clear();

    return this->NativeActivate(cmd);
}

bool
rlSession::CommonEstablish(CmdEstablish* cmd)
{
    rlAssert(this->IsActivated());

    bool success = false;

    rtry
    {
        //Disallow nil attributes.
        const rlMatchingAttributes& attrs = cmd->m_Data.m_Config.m_Attrs;

        for(int i = 0; i < (int) attrs.GetCount(); ++i)
        {
            rverify(attrs.GetValueByIndex(i),
                    catchall,
                    rlError("Session attribute %d is nil", i));
        }

        rcheck(this->NativeEstablish(cmd),
                catchall,
                rlError("Error establishing session"));

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool
rlSession::CommonDestroy(CmdDestroy* cmd)
{
    return this->NativeDestroy(cmd);
}

bool
rlSession::CommonJoin(CmdJoin* cmd)
{
    rlAssert(this->IsEstablished());

    return this->NativeJoin(cmd);
}

bool
rlSession::CommonLeave(CmdLeave* cmd)
{
    rlAssert(this->IsEstablished());

	// work out whether we have local gamers
	for(int i = 0; i < (int) cmd->m_NumGamers; ++i)
	{
		if(cmd->m_GamerHandles[i].IsLocal())
		{
			cmd->m_HasLocalGamers = true;
			break;
		}
	}

	// if we're hosting and if this contains the local gamer, 
	// immediately unadvertise the session from social club matchmaking
	if(cmd->m_HasLocalGamers && this->IsHost() && this->CanUnadvertiseScMatch())
	{
		cmd->m_UnadvertisedSession = true; 
		return rlScMatchmaking::Unadvertise(this->GetOwner().GetLocalIndex(),
											this->m_Data.m_MatchId,
											&cmd->m_MyStatus);
	}
	
	// still here - no social club step required, move on
	cmd->m_MyStatus.SetPending();
	cmd->m_MyStatus.SetSucceeded();

    return this->NativeLeave(cmd);
}

bool
rlSession::CommonChangeAttrs(CmdChangeAttrs* cmd)
{
    rlAssert(this->IsEstablished());

    return this->NativeChangeAttrs(cmd);
}

bool
rlSession::CommonMigrate(CmdMigrate* cmd)
{
    rlAssert(this->IsEstablished());
    rlAssert(this->IsNetwork());

    return this->NativeMigrate(cmd);
}

bool
rlSession::CommonModifyPresenceFlags(CmdModifyPresenceFlags* cmd)
{
    rlAssert(this->IsEstablished());
    rlAssert(this->IsOnline());

    return this->NativeModifyPresenceFlags(cmd);
}

bool
rlSession::CommonSendInvites(CmdSendInvites* cmd)
{
    rlAssert(this->IsEstablished());
    rlAssert(this->IsOnline());

    return this->NativeSendInvites(cmd);
}

bool
rlSession::CommonSendPartyInvites(CmdSendPartyInvites* cmd)
{
    rlAssert(this->IsEstablished());
    rlAssert(this->IsOnline());

    return this->NativeSendPartyInvites(cmd);
}

bool
rlSession::CommonUpdateScAdvertisement(CmdUpdateScAdvertisement* cmd)
{
	rlAssert(this->IsEstablished());
	rlAssert(this->IsOnline());

	cmd->Start();

	return true; 
}

#if RSG_DURANGO

bool
rlSession::NativeHost(CmdHost* cmd)
{
    return g_rlXbl.GetSessionManager()->HostSession(cmd->m_Data.m_LocalOwner.GetLocalIndex(),
													cmd->m_Data.m_Config.m_NetworkMode,
													cmd->m_Data.m_Config.m_MaxPublicSlots,
													cmd->m_Data.m_Config.m_MaxPrivateSlots,
													cmd->m_Data.m_Config.m_Attrs,
													cmd->m_Data.m_Config.m_CreateFlags,
													cmd->m_Data.m_Config.m_PresenceFlags,
													&cmd->m_Data.m_hSession,
													&cmd->m_Data.m_SessionInfo,
													&cmd->m_MyStatus);
}

bool
rlSession::NativeActivate(CmdActivate* cmd)
{
    return g_rlXbl.GetSessionManager()->CreateSession(cmd->m_LocalOwner.GetLocalIndex(),
													  cmd->m_SessionInfo,
													  cmd->m_NetMode,
													  cmd->m_CreateFlags,
													  cmd->m_PresenceFlags,
													  cmd->m_MaxPublicSlots,
													  cmd->m_MaxPrivateSlots,
													  &cmd->m_hSession,
													  &cmd->m_MyStatus);
}

bool
rlSession::NativeEstablish(CmdEstablish* cmd)
{
    return g_rlXbl.GetSessionManager()->ChangeSessionAttributes(cmd->m_Data.m_LocalOwner.GetLocalIndex(),
																cmd->m_Data.m_hSession,
																cmd->m_Data.m_Config.m_NetworkMode,
																cmd->m_Data.m_Config.m_MaxPublicSlots,
																cmd->m_Data.m_Config.m_MaxPrivateSlots,
																cmd->m_Data.m_Config.m_Attrs,
																cmd->m_Data.m_Config.m_CreateFlags,
																cmd->m_Data.m_Config.m_PresenceFlags,
																&cmd->m_MyStatus);
}

bool
rlSession::NativeDestroy(CmdDestroy* UNUSED_PARAM(cmd))
{
    return true;
}

bool
rlSession::NativeJoin(CmdJoin* cmd)
{
	rlGamerHandle localGamerHandle;

	for(int i = 0; i < (int) cmd->m_NumGamers; ++i)
	{
		const rlGamerHandle& gamerHandle = cmd->m_GamerHandles[i];

		if(this->GetOwner().GetGamerHandle() == gamerHandle)
		{
			rlAssert(!localGamerHandle.IsValid());
			localGamerHandle = gamerHandle;
			break;
		}
	}

	return g_rlXbl.GetSessionManager()->JoinSession(localGamerHandle,
													cmd->m_Session->GetConfig().m_CreateFlags,
													m_Data.m_hSession,
													cmd->m_GamerHandles,
													cmd->m_SlotTypes,
													cmd->m_NumGamers,
													&cmd->m_MyStatus);
}

bool
rlSession::NativeLeave(CmdLeave* /*cmd*/)
{
	return true; 
}

bool
rlSession::NativeChangeAttrs(CmdChangeAttrs* cmd)
{
    return g_rlXbl.GetSessionManager()->ChangeSessionAttributes(m_Data.m_LocalOwner.GetLocalIndex(),
																m_Data.m_hSession,
																m_Data.m_Config.m_NetworkMode,
																m_Data.m_Config.m_MaxPublicSlots,
																m_Data.m_Config.m_MaxPrivateSlots,
																cmd->m_NewAttrs,
																m_Data.m_Config.m_CreateFlags,
																m_Data.m_Config.m_PresenceFlags,
																&cmd->m_MyStatus);
}

bool
rlSession::NativeMigrate(CmdMigrate* cmd)
{
    bool success = false;

    const bool isHostLocal =
        cmd->m_NewHostPeerInfo == this->GetOwner().GetPeerInfo();
    const int gamerIdx =
        isHostLocal ? this->GetOwner().GetLocalIndex() : -1;

    if(rlVerify(this->IsOnline()))
    {
        success = g_rlXbl.GetSessionManager()->MigrateHost(
			&m_Data.m_hSession,
			this->GetOwner().GetLocalIndex(),
			gamerIdx,
			m_Data.m_Config.m_CreateFlags,
			m_Data.m_SessionInfo,
			&cmd->m_NewSessionInfo,
			&cmd->m_MyStatus);
    }

    return success;
}

bool
rlSession::NativeModifyPresenceFlags(CmdModifyPresenceFlags* cmd)
{
    cmd->m_MyStatus.SetPending();
    cmd->m_MyStatus.SetSucceeded();

    return true;
}

bool
rlSession::NativeSendInvites(CmdSendInvites* UNUSED_PARAM(cmd))
{
	return true;
}

// makes sure our invite maximum does not exceed the post message limit
CompileTimeAssert(RL_MAX_GAMERS_PER_SESSION <= RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS);

bool
rlSession::NativeSendPartyInvites(CmdSendPartyInvites* cmd)
{
	rlGamerHandle gamerHandles[RL_MAX_GAMERS_PER_SESSION];
	memset(gamerHandles, 0, sizeof(gamerHandles));

	unsigned numGamerHandles = 0;
	int partySize = g_rlXbl.GetPartyManager()->GetPartySize();
	for(int i = 0; i < partySize; i++)
	{
		// exit if we fill our list
		if(numGamerHandles >= RL_MAX_GAMERS_PER_SESSION)
			break;

		rlGamerHandle gamerHandle = g_rlXbl.GetPartyManager()->GetPartyMember(i)->m_GamerHandle;
		if(!gamerHandle.IsValid())
			continue; 

		gamerHandles[numGamerHandles] = gamerHandle;
		numGamerHandles++;
	}

	rlScPresenceMessageMpInvite invite;
	rlPresence::GetGamerHandle(this->GetOwner().GetLocalIndex(), &invite.m_InviterGamerHandle);
	rlPresence::GetName(this->GetOwner().GetLocalIndex(), invite.m_InviterName);
	invite.m_SessionInfo = this->GetSessionInfo();

	bool bSuccess = rlPresence::PostMessage(m_Data.m_LocalOwner.GetLocalIndex(), gamerHandles, numGamerHandles, invite, 0);

	if (bSuccess)
	{
		cmd->m_MyStatus.SetPending();
		cmd->m_MyStatus.SetSucceeded();
	}
	else
	{
		cmd->m_MyStatus.SetPending();
		cmd->m_MyStatus.SetFailed();
	}

	return bSuccess;
}

#elif RSG_PC || RSG_NP

bool
rlSession::NativeHost(CmdHost* cmd)
{
	bool success = false;

	rlAssert(CmdHost::STATE_CREATING == cmd->m_State);

	rtry
	{
		cmd->m_MyStatus.SetPending();

		rlPeerAddress peerAddr;
		rlVerify(rlPeerAddress::GetLocalPeerAddress(cmd->m_Data.m_LocalOwner.GetLocalIndex(), &peerAddr));
		rverify(peerAddr.IsValid(),
				catchall,
				rlError("NativeHost :: Invalid peer address"));

        rlSessionToken token;
        rverify(MakeSessionToken(&token),
                catchall,
                rlError("NativeHost :: Error creating session token"));
		cmd->m_Data.m_SessionInfo.Init(cmd->m_Data.m_Config.m_SessionId, token, peerAddr);
		cmd->m_MyStatus.SetSucceeded();

		success = true;
	}
	rcatchall
	{
		cmd->m_MyStatus.SetFailed();
		this->Clear();
	}

	return success;
}

bool
rlSession::NativeActivate(CmdActivate* cmd)
{
	cmd->m_MyStatus.SetPending();
	cmd->m_MyStatus.SetSucceeded();

	return true;
}

bool
rlSession::NativeEstablish(CmdEstablish* cmd)
{
	cmd->m_MyStatus.SetPending();
	cmd->m_MyStatus.SetSucceeded();

	return true;
}

bool
rlSession::NativeDestroy(CmdDestroy* /*cmd*/)
{
	return true;
}

bool
rlSession::NativeJoin(CmdJoin* cmd)
{
	cmd->m_MyStatus.SetPending();
	cmd->m_MyStatus.SetSucceeded();

	return true;
}

bool
rlSession::NativeLeave(CmdLeave* /*cmd*/)
{
	return true;
}

bool
rlSession::NativeChangeAttrs(CmdChangeAttrs* cmd)
{
	cmd->m_MyStatus.SetPending();
	cmd->m_MyStatus.SetSucceeded();

	return true;
}

bool
rlSession::NativeSendInvites(CmdSendInvites* cmd)
{
#if RSG_PC
	rlScPresenceMessageMpInvite invite;
	rlPresence::GetGamerHandle(this->GetOwner().GetLocalIndex(), &invite.m_InviterGamerHandle);
	rlPresence::GetName(this->GetOwner().GetLocalIndex(), invite.m_InviterName);
	invite.m_SessionInfo = this->GetSessionInfo();
	bool success = rlPresence::PostMessage(m_Data.m_LocalOwner.GetLocalIndex(),
										   cmd->m_GamerHandles,
										   cmd->m_NumGamers,
										   invite,
										   0);

	// do not wait for the post message to succeed (this would hold up session flow)
	cmd->m_MyStatus.SetPending();
	cmd->m_MyStatus.SetStatus(success ?  netStatus::NET_STATUS_SUCCEEDED : netStatus::NET_STATUS_FAILED);

	return success;
#elif RSG_NP
	return true;
#endif
}

bool
rlSession::NativeSendPartyInvites(CmdSendPartyInvites* /*cmd*/)
{
	// TODO: NS - implement?
	return rlVerify(false);
}

bool
rlSession::NativeMigrate(CmdMigrate* cmd)
{
	bool success = false;

	rlAssert(CmdMigrate::STATE_MIGRATING == cmd->m_State);

	if(this->GetOwner().GetPeerInfo() == cmd->m_NewHostPeerInfo)
	{
		if((RL_NETMODE_LAN == GetConfig().m_NetworkMode) ||
		   (RL_NETMODE_ONLINE == GetConfig().m_NetworkMode))
		{
			rlPeerAddress peerAddr;
			rlVerify(rlPeerAddress::GetLocalPeerAddress(this->GetOwner().GetLocalIndex(), &peerAddr));
			rlAssert(peerAddr.IsValid());

            rlSessionToken newToken;
            rlVerifyf(MakeSessionToken(&newToken),
                    "Error creating session token");

			cmd->m_NewSessionInfo.Init(this->GetId(), newToken, peerAddr);

			cmd->m_MyStatus.SetPending();
			cmd->m_MyStatus.SetSucceeded();
		}
		else
		{
			rlAssert(false);
		}
	}
	else
	{
		//We're not the new host, which means the new host should
		//have provided us with his session info.
		rlAssert(cmd->m_NewSessionInfo.IsValid());

#if RSG_PC
		rlDebug("New host's public address is:" NET_ADDR_FMT "",
			NET_ADDR_FOR_PRINTF(cmd->m_NewSessionInfo.GetHostPeerAddress().GetPublicAddress()));
#endif

		cmd->m_MyStatus.SetPending();
		cmd->m_MyStatus.SetSucceeded();
	}

	success = true;

	return success;
}

bool
rlSession::NativeModifyPresenceFlags(CmdModifyPresenceFlags* cmd)
{
	cmd->m_MyStatus.SetPending();
	cmd->m_MyStatus.SetSucceeded();
	
	// this gets taken care of in snSession::Update().
	// we send the invitable flag to the Social Club DLL based
	// on the higher level snSession's invitable state.

	return true;
}

#else

bool
rlSession::NativeHost(CmdHost* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeActivate(CmdActivate* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeEstablish(CmdEstablish* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeDestroy(CmdDestroy* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeJoin(CmdJoin* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeLeave(CmdLeave* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeChangeAttrs(CmdChangeAttrs* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeMigrate(CmdMigrate* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeModifyPresenceFlags(CmdModifyPresenceFlags* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeSendInvites(CmdSendInvites* /*cmd*/)
{
    return rlVerify(false);
}

bool
rlSession::NativeSendPartyInvites(CmdSendPartyInvites* /*cmd*/)
{
    return rlVerify(false);
}

#endif //LIVE

void
rlSession::InsertGamer(const rlGamerHandle& gamerHandle,
                       const rlSlotType slotType)
{
    if(rlVerify(this->IsEstablished()))
    {
        rlAssert(!this->IsMember(gamerHandle));
        rlAssert(m_MemberCount < RL_MAX_GAMERS_PER_SESSION);

        m_Slots[m_MemberCount].Reset(gamerHandle, slotType);
        if(RL_SLOT_PUBLIC == slotType)
        {
            ++m_PubSlotCount;
            rlAssert(m_PubSlotCount <= m_Data.m_Config.m_MaxPublicSlots);
        }
        else
        {
            ++m_PrivSlotCount;
            rlAssert(m_PrivSlotCount <= m_Data.m_Config.m_MaxPrivateSlots);
        }

        ++m_MemberCount;

        rlAssert(m_MemberCount <=
                (m_Data.m_Config.m_MaxPublicSlots + m_Data.m_Config.m_MaxPrivateSlots));
    }
}

void
rlSession::RemoveGamer(const rlGamerHandle& gamerHandle)
{
    const int idx = this->GetSlotIndex(gamerHandle);
    if(!rlVerify(idx >= 0))
	{
		return;
	}

    if(RL_SLOT_PUBLIC == m_Slots[idx].m_SlotType)
    {
        --m_PubSlotCount;
        // rlAssert(m_PubSlotCount >= 0);
    }
    else
    {
        --m_PrivSlotCount;
        // rlAssert(m_PrivSlotCount >= 0);
    }

    --m_MemberCount;
 //    rlAssert(m_MemberCount >= 0);

    m_Slots[idx] = m_Slots[m_MemberCount];
    m_Slots[m_MemberCount].Clear();
}

int
rlSession::GetSlotIndex(const rlGamerHandle& gamerHandle) const
{
    int index = -1;
    for(unsigned i = 0; i < m_MemberCount; ++i)
    {
        if(gamerHandle == m_Slots[i].m_hGamer)
        {
            index = (int) i;
            break;
        }
    }

    return index;
}

rlSession::Slot*
rlSession::GetSlot(const rlGamerHandle& gamerHandle)
{
    const int slotIdx = this->GetSlotIndex(gamerHandle);
    return slotIdx >= 0 ? &m_Slots[slotIdx] : NULL;
}

const rlSession::Slot*
rlSession::GetSlot(const rlGamerHandle& gamerHandle) const
{
    const int slotIdx = this->GetSlotIndex(gamerHandle);
    return slotIdx >= 0 ? &m_Slots[slotIdx] : NULL;
}

bool rlSession::CanAdvertiseScMatch() const
{
	return IsHost() && IsOnline() && (GetEmptySlots(RL_SLOT_PUBLIC) > 0) && IsMatchmakingEnabled() && IsMatchmakingAdvertisingEnabled() && !HasScMatchId();
}

bool rlSession::CanUpdateScMatch() const
{
	return IsHost() && IsOnline() && HasScMatchId() && IsMatchmakingEnabled();
}

bool rlSession::CanUnadvertiseScMatch() const
{
	return IsOnline() && HasScMatchId();
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

}   //namespace rage
