// 
// rline/rluserstorage.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "rline/userstorage/rluserstorage.h"
#include "rline/ros/rlros.h"
#include "rline/rltitleid.h"
#include "data/base64.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/http.h"
#include "string/stringutil.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, userstorage)
#undef __rage_channel
#define __rage_channel rline_userstorage

extern const rlTitleId* g_rlTitleId;

static bool s_Initialized;

extern sysMemAllocator* g_rlAllocator;

static const unsigned HTTP_REQUEST_TIMEOUT_SECONDS  = 30;
static const unsigned MAX_FILEPATH_LENGTH   = 256;
static const unsigned MAX_FILENAME_LENGTH   = 64;

class rlUserStorageGetFileTask : public rlTaskBase
{
public:

	RL_TASK_USE_CHANNEL(rline_userstorage);
	RL_TASK_DECL(rlUserStorageGetFileTask);

    rlUserStorageGetFileTask()
        : m_State(STATE_GET)
    {
    }

    ~rlUserStorageGetFileTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const RockstarId targetRockstarId,
                    const char* filePath,
                    datGrowBuffer* growBuffer)
    {
        return this->CommonConfigure(localGamerIndex,
                                    targetRockstarId,
                                    filePath,
                                    growBuffer,
                                    NULL,   //responseDevice
                                    NULL    //responseHandle
                                    );
    }

    bool Configure(const int localGamerIndex,
                    const RockstarId targetRockstarId,
                    const char* filePath,
                    const fiDevice* responseDevice, //Dest IO device
                    const fiHandle responseHandle   //Dest IO file handle
                    )
    {
        return this->CommonConfigure(localGamerIndex,
                                    targetRockstarId,
                                    filePath,
                                    NULL,   //growBuffer
                                    responseDevice,
                                    responseHandle);
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel()
    {
        //Cancel any subtask we're waiting for.
        rlGetTaskManager()->CancelTask(&m_MyStatus);
        //Cancel any HTTP request we're waiting for.
        m_HttpRequest.Cancel();
    }

    virtual void Update(const unsigned /*timeStepMs*/)
    {
        switch(m_State)
        {
        case STATE_GET:
            if(this->WasCanceled())
            {
                this->Finish(FINISH_CANCELED);
            }
            else if(m_HttpRequest.Commit())
            {
                m_State = STATE_GETTING;
            }
            else
            {
                rlTaskError("Failed to retrieve %s", m_HttpRequest.GetUri());
                this->Finish(FINISH_FAILED);
            }
            break;

        case STATE_GETTING:
            m_HttpRequest.Update();

            if(m_MyStatus.Pending())
            {
                if(m_HttpRequest.GetResponseHeader())
                {
                    char buf[128];
                    unsigned len = sizeof(buf);
                    if(m_HttpRequest.GetResponseHeaderValue("Content-Type", buf, &len))
                    {
                        for(int i = 0; i < (int) len; ++i){buf[i] = (char)tolower(buf[i]);}
                        if(strstr(buf, "application/json"))
                        {
                            //This is a directory listing
                            m_HttpRequest.Cancel();
                            rlTaskError("\"%s\" is a directory", m_HttpRequest.GetUri());
                            this->Finish(FINISH_FAILED);
                        }
                    }
                }
            }
            else if(m_MyStatus.Succeeded())
            {
                if(m_HttpRequest.Succeeded())
                {
                    rlTaskDebug("Retrieved %s", m_HttpRequest.GetUri());
                    this->Finish(FINISH_SUCCEEDED);
                }
                else
                {
                    rlTaskError("Failed to retrieve %s, status code:%d",
                                m_HttpRequest.GetUri(),
                                m_HttpRequest.GetStatusCode());
                    this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
                }
            }
            else
            {
                rlTaskError("Failed to retrieve %s", m_HttpRequest.GetUri());
                this->Finish(FINISH_FAILED);
            }
            break;
        }
    }

private:

    bool CommonConfigure(const int localGamerIndex,
                        const RockstarId targetRockstarId,
                        const char* filePath,
                        datGrowBuffer* growBuffer,
                        const fiDevice* responseDevice, //Dest IO device
                        const fiHandle responseHandle   //Dest IO file handle
                        )
    {
        bool success = false;
        rtry
        {
            rverify(strlen(filePath) <= MAX_FILEPATH_LENGTH,
                    catchall,
                    rlTaskError("File path %s is too long - max length is %d characters",
                                filePath,
                                MAX_FILEPATH_LENGTH));

            rverify(growBuffer || (responseDevice && fiHandleInvalid != responseHandle),catchall,);

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

            rverify(cred.IsValid(),
                    catchall,
                    rlTaskError("Gamer at index:%d doesn't have valid ROS credentials",
                                localGamerIndex));

            rcheck(cred.GetRockstarId() != InvalidRockstarId,
                    catchall,
                    rlTaskError("Gamer at index:%d is not linked to a Social Club account",
                                localGamerIndex));

            const RockstarId rockstarId =
                (InvalidRockstarId == targetRockstarId) ? cred.GetRockstarId() : targetRockstarId;

            atStringBuilder url;
            //String template is "http://%s/socialclub/%d/userstorage/%"I64FMT"d/%s?ticket=%s".
            char buf[MAX_FILEPATH_LENGTH+1];
            const char* domainName = g_rlTitleId->m_RosTitleId.GetDomainName();
            rverify(url.AppendOrFail("http://")
                    && url.AppendOrFail(domainName)
                    && url.AppendOrFail("/socialclub/")
                    && url.AppendOrFail(formatf(buf, "%d/userstorage/", g_rlTitleId->m_RosTitleId.GetScVersion()))
                    && url.AppendOrFail(formatf(buf, "%"I64FMT"d/", rockstarId)),
                    catchall,
                    rlTaskError("Error building URL"));

            //Trim spaces
            StringTrim(buf, filePath, sizeof(buf));

            //Replace \ with /
            std::replace(buf, &buf[strlen(buf)], '\\', '/');

            //Trim trailing slashes
            StringRTrim(buf, buf, '/', sizeof(buf));

            rverify(url.AppendOrFail(buf),
                    catchall,
                    rlTaskError("Error building URL"));

            const char* rosTicket = cred.GetTicket();
            rverify(url.AppendOrFail("?ticket=")
                    && url.AppendOrFail(rosTicket),
                    catchall,
                    rlTaskError("Error building URL"));

            m_HttpRequest.Init(g_rlAllocator);

            if(growBuffer)
            {
                rverify(m_HttpRequest.BeginGet(url.ToString(),
                                                NULL,   //proxyAddr
                                                HTTP_REQUEST_TIMEOUT_SECONDS,
                                                growBuffer,
                                                this->GetTaskName(),
                                                &m_MyStatus),
                        catchall,
                        rlTaskError("Error beginning GET request"));
            }
            else
            {
                rverify(m_HttpRequest.BeginGet(url.ToString(),
                                                NULL,   //proxyAddr
                                                HTTP_REQUEST_TIMEOUT_SECONDS,
                                                responseDevice,
                                                responseHandle,
                                                this->GetTaskName(),
                                                &m_MyStatus),
                        catchall,
                        rlTaskError("Error beginning GET request"));
            }

            m_State = STATE_GET;

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    enum State
    {
        STATE_GET,
        STATE_GETTING
    };

    netHttpRequest m_HttpRequest;

    netStatus m_MyStatus;

    State m_State;
};

class rlUserStoragePostFileTask : public rlTaskBase
{
public:

	RL_TASK_USE_CHANNEL(rline_userstorage);
	RL_TASK_DECL(rlUserStoragePostFileTask);

    rlUserStoragePostFileTask()
        : m_SrcDevice(NULL)
        , m_SrcHandle(fiHandleInvalid)
        , m_State(STATE_STREAMING)
        , m_OwnSrcHandle(false)
    {
        m_MultipartBoundary[0] = '\0';
        m_ResponseGb.Init(m_ResponseBuf, sizeof(m_ResponseBuf), datGrowBuffer::NULL_TERMINATE);
    }

    ~rlUserStoragePostFileTask()
    {
        this->ReleaseResources();
    }

    bool Configure(const int localGamerIndex,
                    const char* filePath,
                    const void* data,
                    const unsigned sizeofData,
                    const rlUserStorage::PostType postType)
    {
        bool success = false;
        rtry
        {
            char fname[32];
            fiDevice::MakeMemoryFileName(fname, sizeof(fname), data, sizeofData, false, NULL);
            const fiDevice* srcDevice = &fiDeviceMemory::GetInstance();
            rverify(srcDevice, catchall, rlError("Error getting memory device"));
            fiHandle srcHandle = srcDevice->Open(fname, true);
            rverify(fiHandleInvalid != srcHandle,
                    catchall,
                    rlError("Error opening memory file"));

            m_OwnSrcHandle = true;

            rcheck(this->CommonConfigure(localGamerIndex,
                                        filePath,
                                        srcDevice,
                                        srcHandle,
                                        postType),catchall,);

            success = true;
        }
        rcatchall
        {
            this->ReleaseResources();
        }

        return success;
    }

    bool Configure(const int localGamerIndex,
                    const char* filePath,
                    const fiDevice* srcDevice,
                    const fiHandle srcHandle,
                    const rlUserStorage::PostType postType)
    {
        m_OwnSrcHandle = false;

        if(!this->CommonConfigure(localGamerIndex,
                                    filePath,
                                    srcDevice,
                                    srcHandle,
                                    postType))
        {
            this->ReleaseResources();
            return false;
        }

        return true;
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel()
    {
        //Cancel any subtask we're waiting for.
        rlGetTaskManager()->CancelTask(&m_MyStatus);
        //Cancel any HTTP request we're waiting for.
        m_HttpRequest.Cancel();
    }

    virtual void Finish(const FinishType finishType, const int resultCode)
    {
        this->ReleaseResources();

        rlTaskBase::Finish(finishType, resultCode);
    }

    virtual void Update(const unsigned /*timeStepMs*/)
    {
        switch(m_State)
        {
        case STATE_STREAMING:
            if(this->WasCanceled())
            {
                this->Finish(FINISH_CANCELED, 0);
            }
            else
            {
                char buf[1024];
                int len = m_SrcDevice->Read(m_SrcHandle, buf, sizeof(buf));
                if(len > 0)
                {
                    if(!rlVerify(m_HttpRequest.AppendContent(buf, len)))
                    {
                        rlTaskError("Failed to post %s", m_HttpRequest.GetUri());
                        this->Finish(FINISH_FAILED, 0);
                    }
                }
                else
                {
                    if(rlVerify(m_HttpRequest.AppendContent("\r\n--", 4))
                        && rlVerify(m_HttpRequest.AppendContent(m_MultipartBoundary, strlen(m_MultipartBoundary)))
                        && rlVerify(m_HttpRequest.AppendContent("--\r\n", 4))
                        && rlVerify(m_HttpRequest.Commit()))
                    {
                        m_State = STATE_POSTING;
                    }
                    else
                    {
                        rlTaskError("Failed to post %s", m_HttpRequest.GetUri());
                        this->Finish(FINISH_FAILED, 0);
                    }
                }
            }
            break;
        case STATE_POSTING:
            m_HttpRequest.Update();

            //We don't care about the response.
            m_ResponseGb.Clear();

            if(m_MyStatus.Succeeded())
            {
                if(m_HttpRequest.Succeeded())
                {
                    rlTaskDebug("Posted %s", m_HttpRequest.GetUri());
                    this->Finish(FINISH_SUCCEEDED, m_HttpRequest.GetStatusCode());
                }
                else
                {
                    rlTaskError("Failed to post %s, status code:%d",
                                m_HttpRequest.GetUri(),
                                m_HttpRequest.GetStatusCode());
                    this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
                }
            }
            else if(!m_MyStatus.Pending())
            {
                rlTaskError("Failed to post %s", m_HttpRequest.GetUri());
                this->Finish(FINISH_FAILED, 0);
            }
            break;
        }
    }

private:

    bool CommonConfigure(const int localGamerIndex,
                        const char* filePath,
                        const fiDevice* srcDevice,
                        const fiHandle srcHandle,
                        const rlUserStorage::PostType postType)
    {
        bool success = false;
        rtry
        {
            rverify(srcDevice, catchall,);
            rverify(fiHandleInvalid != srcHandle, catchall,);

            rverify(strlen(filePath) <= MAX_FILEPATH_LENGTH,
                    catchall,
                    rlTaskError("File path %s is too long - max length is %d characters",
                                filePath,
                                MAX_FILEPATH_LENGTH));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

            rverify(cred.IsValid(),
                    catchall,
                    rlTaskError("Gamer at index:%d doesn't have valid ROS credentials",
                                localGamerIndex));

            rcheck(cred.GetRockstarId() != InvalidRockstarId,
                    catchall,
                    rlTaskError("Gamer at index:%d is not linked to a Social Club account",
                                localGamerIndex));

            const RockstarId rockstarId = cred.GetRockstarId();

            atStringBuilder url;
            //String template is "http://%s/socialclub/%d/userstorage/%"I64FMT"d/%s?ticket=%s".
            char buf[MAX_FILEPATH_LENGTH+1];
            const char* domainName = g_rlTitleId->m_RosTitleId.GetDomainName();
            rverify(url.AppendOrFail("http://")
                    && url.AppendOrFail(domainName)
                    && url.AppendOrFail("/socialclub/")
                    && url.AppendOrFail(formatf(buf, "%d/userstorage/", g_rlTitleId->m_RosTitleId.GetScVersion()))
                    && url.AppendOrFail(formatf(buf, "%"I64FMT"d/", rockstarId)),
                    catchall,
                    rlTaskError("Error building URL"));

            //Trim spaces
            StringTrim(buf, filePath, sizeof(buf));

            //Replace \ with /
            std::replace(buf, &buf[strlen(buf)], '\\', '/');

            //Get the file name.
            char fname[MAX_FILENAME_LENGTH+1];
            char* tmp = strrchr(buf, '/');

            if(tmp)
            {
                *tmp++ = '\0';
            }
            else
            {
                tmp = buf;
            }

            rverify('\0' != tmp[0],
                    catchall,
                    rlTaskError("Invalid file path: %s", filePath));

            rverify(strlen(tmp) <= MAX_FILENAME_LENGTH,
                    catchall,
                    rlTaskError("Filename %s is too long - max length is %d characters",
                                tmp,
                                MAX_FILENAME_LENGTH));

            safecpy(fname, tmp);

            //Trim trailing slashes from directory path
            StringRTrim(buf, buf, '/', sizeof(buf));

            if('\0' != buf[0])
            {
                rverify(url.AppendOrFail(buf),
                        catchall,
                        rlTaskError("Error building URL"));
            }

            const char* rosTicket = cred.GetTicket();
            rverify(url.AppendOrFail("?ticket=")
                    && url.AppendOrFail(rosTicket),
                    catchall,
                    rlTaskError("Error building URL"));

            if(rlUserStorage::POST_APPEND == postType)
            {
                rverify(url.AppendOrFail("&append=1"),
                        catchall,
                        rlTaskError("Error building URL"));
            }

            m_HttpRequest.Init(g_rlAllocator);

            rverify(m_HttpRequest.BeginPost(url.ToString(),
                                            NULL,   //proxyAddr
                                            HTTP_REQUEST_TIMEOUT_SECONDS,
                                            &m_ResponseGb,
                                            this->GetTaskName(),
                                            0, //Filter
                                            &m_MyStatus),
                    catchall,
                    rlTaskError("Error beginning POST request"));

            formatf(m_MultipartBoundary, "--------------------%"I64FMT"x", rlGetPosixTime());

            formatf(buf, "multipart/form-data; boundary=%s", m_MultipartBoundary);

            rverify(m_HttpRequest.AddRequestHeaderValue("Content-Type", buf),catchall,);

            rverify(m_HttpRequest.AppendContent("--", 2),catchall,);
            rverify(m_HttpRequest.AppendContent(m_MultipartBoundary, strlen(m_MultipartBoundary)),catchall,);
            rverify(m_HttpRequest.AppendContent("\r\n", 2),catchall,);
            formatf(buf, "Content-Disposition: form-data; name=\"fileField\"; filename=\"%s\"\r\n",
                    fname);
            rverify(m_HttpRequest.AppendContent(buf, strlen(buf)),catchall,);
            const char* str = "Content-Transfer-Encoding: binary\r\n\r\n";
            rverify(m_HttpRequest.AppendContent(str, strlen(str)),catchall,);

            m_SrcDevice = srcDevice;
            m_SrcHandle = srcHandle;

            m_State = STATE_STREAMING;

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    void ReleaseResources()
    {
        if(fiHandleInvalid != m_SrcHandle && m_OwnSrcHandle)
        {
            m_SrcDevice->Close(m_SrcHandle);
        }

        m_SrcHandle = fiHandleInvalid;
        m_SrcDevice = NULL;
        m_OwnSrcHandle = false;
    }

    enum State
    {
        STATE_STREAMING,
        STATE_POSTING
    };

    netHttpRequest m_HttpRequest;

    u8 m_ResponseBuf[256];
    datGrowBuffer m_ResponseGb;

    char m_MultipartBoundary[32];

    const fiDevice* m_SrcDevice;
    fiHandle m_SrcHandle;

    netStatus m_MyStatus;

    State m_State;

    bool m_OwnSrcHandle : 1;
};

class rlUserStorageDeleteFileTask : public rlTaskBase
{
public:

	RL_TASK_USE_CHANNEL(rline_userstorage);
	RL_TASK_DECL(rlUserStorageDeleteFileTask);

    rlUserStorageDeleteFileTask()
        : m_State(STATE_DELETE)
    {
    }

    ~rlUserStorageDeleteFileTask()
    {
    }

    bool Configure(const int localGamerIndex,
                    const char* filePath)
    {
        bool success = false;
        rtry
        {
            rverify(strlen(filePath) <= MAX_FILEPATH_LENGTH,
                    catchall,
                    rlTaskError("File path %s is too long - max length is %d characters",
                                filePath,
                                MAX_FILEPATH_LENGTH));

            const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

            rverify(cred.IsValid(),
                    catchall,
                    rlTaskError("Gamer at index:%d doesn't have valid ROS credentials",
                                localGamerIndex));

            rcheck(cred.GetRockstarId() != InvalidRockstarId,
                    catchall,
                    rlTaskError("Gamer at index:%d is not linked to a Social Club account",
                                localGamerIndex));

            const RockstarId rockstarId = cred.GetRockstarId();

            atStringBuilder url;
            //String template is "http://%s/socialclub/%d/userstorage/%"I64FMT"d/%s?ticket=%s".
            char buf[MAX_FILEPATH_LENGTH+1];
            const char* domainName = g_rlTitleId->m_RosTitleId.GetDomainName();
            rverify(url.AppendOrFail("http://")
                    && url.AppendOrFail(domainName)
                    && url.AppendOrFail("/socialclub/")
                    && url.AppendOrFail(formatf(buf, "%d/userstorage/", g_rlTitleId->m_RosTitleId.GetScVersion()))
                    && url.AppendOrFail(formatf(buf, "%"I64FMT"d/", rockstarId)),
                    catchall,
                    rlTaskError("Error building URL"));

            //Trim spaces
            StringTrim(buf, filePath, sizeof(buf));

            //Replace \ with /
            std::replace(buf, &buf[strlen(buf)], '\\', '/');

            //Trim trailing slashes
            StringRTrim(buf, buf, '/', sizeof(buf));

            rverify(url.AppendOrFail(buf),
                    catchall,
                    rlTaskError("Error building URL"));

            const char* rosTicket = cred.GetTicket();
            rverify(url.AppendOrFail("?ticket=")
                    && url.AppendOrFail(rosTicket),
                    catchall,
                    rlTaskError("Error building URL"));

            m_HttpRequest.Init(g_rlAllocator);

            rverify(m_HttpRequest.BeginDelete(url.ToString(),
                                            NULL,   //proxyAddr
                                            HTTP_REQUEST_TIMEOUT_SECONDS,
                                            NULL,
                                            this->GetTaskName(),
                                            &m_MyStatus),
                    catchall,
                    rlTaskError("Error beginning DELETE request"));

            m_State = STATE_DELETE;

            success = true;
        }
        rcatchall
        {
        }

        return success;
    }

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel()
    {
        //Cancel any subtask we're waiting for.
        rlGetTaskManager()->CancelTask(&m_MyStatus);
        //Cancel any HTTP request we're waiting for.
        m_HttpRequest.Cancel();
    }

    virtual void Update(const unsigned /*timeStepMs*/)
    {
        switch(m_State)
        {
        case STATE_DELETE:
            if(this->WasCanceled())
            {
                this->Finish(FINISH_CANCELED);
            }
            else if(m_HttpRequest.Commit())
            {
                m_State = STATE_DELETING;
            }
            else
            {
                rlTaskError("Failed to delete %s", m_HttpRequest.GetUri());
                this->Finish(FINISH_FAILED);
            }
            break;

        case STATE_DELETING:
            m_HttpRequest.Update();

            if(m_MyStatus.Succeeded())
            {
                if(m_HttpRequest.Succeeded())
                {
                    rlTaskDebug("Deleted %s", m_HttpRequest.GetUri());
                    this->Finish(FINISH_SUCCEEDED);
                }
                else
                {
                    rlTaskError("Failed to delete %s, status code:%d",
                                m_HttpRequest.GetUri(),
                                m_HttpRequest.GetStatusCode());
                    this->Finish(FINISH_FAILED, m_HttpRequest.GetStatusCode());
                }
            }
            else if(!m_MyStatus.Pending())
            {
                rlTaskError("Failed to delete %s", m_HttpRequest.GetUri());
                this->Finish(FINISH_FAILED);
            }
            break;
        }
    }

private:

    enum State
    {
        STATE_DELETE,
        STATE_DELETING
    };

    netHttpRequest m_HttpRequest;

    netStatus m_MyStatus;

    State m_State;
};

//////////////////////////////////////////////////////////////////////////
//  rlUserStorage
//////////////////////////////////////////////////////////////////////////
bool
rlUserStorage::Init()
{
	if(rlVerify(!s_Initialized))
    {
        s_Initialized = true;;
    }

	return s_Initialized;
}

bool 
rlUserStorage::IsInitialized()
{
	return s_Initialized;
}

void 
rlUserStorage::Shutdown()
{
	if(s_Initialized)
	{
        s_Initialized = false;
	}
}

void 
rlUserStorage::Update()
{
}

bool
rlUserStorage::GetFile(const int localGamerIndex,
                        const RockstarId targetRockstarId,
                        const char* filePath,
                        datGrowBuffer* growBuffer,
                        netStatus* status)
{
    bool success = false;

    rlUserStorageGetFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        task = rlGetTaskManager()->CreateTask<rlUserStorageGetFileTask>();
        rverify(task,catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        targetRockstarId,
                                        filePath,
                                        growBuffer,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlUserStorage::GetFile(const int localGamerIndex,
                        const RockstarId targetRockstarId,
                        const char* filePath,
                        const fiDevice* responseDevice,
                        const fiHandle responseHandle,
                        netStatus* status)
{
    bool success = false;

    rlUserStorageGetFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        task = rlGetTaskManager()->CreateTask<rlUserStorageGetFileTask>();
        rverify(task,catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        targetRockstarId,
                                        filePath,
                                        responseDevice,
                                        responseHandle,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlUserStorage::PostFile(const int localGamerIndex,
                        const char* dstFilePath,
                        const void* data,
                        const unsigned sizeofData,
                        const PostType postType,
                        netStatus* status)
{
    bool success = false;

    rlUserStoragePostFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        task = rlGetTaskManager()->CreateTask<rlUserStoragePostFileTask>();
        rverify(task,catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        dstFilePath,
                                        data,
                                        sizeofData,
                                        postType,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlUserStorage::PostFile(const int localGamerIndex,
                        const char* dstFilePath,
                        const fiDevice* srcDevice,
                        const fiHandle srcHandle,
                        const PostType postType,
                        netStatus* status)
{
    bool success = false;

    rlUserStoragePostFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        task = rlGetTaskManager()->CreateTask<rlUserStoragePostFileTask>();
        rverify(task,catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        dstFilePath,
                                        srcDevice,
                                        srcHandle,
                                        postType,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

bool
rlUserStorage::DeleteFile(const int localGamerIndex,
                        const char* filePath,
                        netStatus* status)
{
    bool success = false;

    rlUserStorageDeleteFileTask* task = NULL;
    rtry
    {
        rverify(IsInitialized(), catchall,);

        task = rlGetTaskManager()->CreateTask<rlUserStorageDeleteFileTask>();
        rverify(task,catchall,);

        rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        filePath,
                                        status),catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

void
rlUserStorage::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

}
