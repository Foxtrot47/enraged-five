// 
// rline/rluserstorage.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUSERSTORAGE_H
#define RLINE_RLUSERSTORAGE_H

#include "rline/rltask.h"
#include "file/handle.h"
#include "net/status.h"

//To fix jacked up Windows headers where DeleteFile is a #define
#ifdef DeleteFile
#undef DeleteFile
#endif

namespace rage
{

class fiDevice;
class datGrowBuffer;

//PURPOSE
//  This class implements the User Storage service.
//  User Storage allows players to access title-wide files stored
//  in a global repository.
class rlUserStorage
{
public:

    enum PostType
    {
        POST_REPLACE,
        POST_APPEND
    };

	//PURPOSE
	//  Init/Update/Shutdown
	static bool Init();
	static bool IsInitialized();
	static void Shutdown();
	static void Update();

    //PURPOSE
    //  Retrieves a file from User Storage.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is requesting the file.
    //                        The gamer must have a valid Rockstar ID.
    //  targetRockstarId    - Rockstar ID of user who owns the file requested.
    //                        Pass InvalidRockstarId if the file is owned by
    //                        the gamer identified by localGamerIndex.
    //  filePath            - Path of the file to retrieve
    //  growBuffer          - datGrowBuffer to which file data will be written
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the growBuffer or status objects until the operation
    //  is complete.
    //EXAMPLES
    //  //Retrieve a file for the local player at index 0.
    //  rlUserStorage::GetFile(0, InvalidRockstarId, "mpsaves/save1.json", &gb, &myStatus);
    //
    //  //Retrieve a file for the remote player with Rockstar ID 36512, on behalf
    //  //of the local player at index 0.
    //  rlUserStorage::GetFile(0, 36512, "profile/images/avatar.jpeg", &gb, &myStatus);
    static bool GetFile(const int localGamerIndex,
                        const RockstarId targetRockstarId,
                        const char* filePath,
                        datGrowBuffer* growBuffer,
                        netStatus* status);

    //PURPOSE
    //  Retrieves a file from User Storage.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is requesting the file.
    //                        The gamer must have a valid Rockstar ID.
    //  targetRockstarId    - Rockstar ID of user who owns the file requested.
    //                        Pass InvalidRockstarId if the file is owned by
    //                        the gamer identified by localGamerIndex.
    //  filePath            - Path of the file to retrieve
    //  responseDevice      - IO device to which file data will be written
    //  responseHandle      - IO handle to which file data will be written
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the growBuffer or status objects until the operation
    //  is complete.
    static bool GetFile(const int localGamerIndex,
                        const RockstarId targetRockstarId,
                        const char* filePath,
                        const fiDevice* responseDevice,
                        const fiHandle responseHandle,
                        netStatus* status);

    //PURPOSE
    //  Uploads or appends a file to User Storage.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is uploading the file.
    //                        The gamer must have a valid Rockstar ID.
    //  dstFilePath         - Destination path of the file
    //  data                - Data to upload
    //  sizeofData          - Size of data to upload
    //  postType            - Replace (create) or Append.  In either case if
    //                        the file doesn't exist it will be created.
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the data or status objects until the operation
    //  is complete.
    //EXAMPLES
    //  //Upload a file for the local player at index 0.
    //  //If the file exists it is replaced.
    //  rlUserStorage::PostFile(0, "mpsaves/save1.json", saveData, saveDataLen, rlUserStorage::POST_REPLACE, &myStatus);
    //
    //  //Append to a file for the local player at index 0.
    //  //If the file doesn't exist it is created.
    //  rlUserStorage::PostFile(0, "logs/log1.txt", saveData, saveDataLen, rlUserStorage::POST_APPEND, &myStatus);
    static bool PostFile(const int localGamerIndex,
                        const char* dstFilePath,
                        const void* data,
                        const unsigned sizeofData,
                        const PostType postType,
                        netStatus* status);

    //PURPOSE
    //  Uploads or appends a file to User Storage.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is uploading the file.
    //                        The gamer must have a valid Rockstar ID.
    //  dstFilePath         - Destination path of the file
    //  responseDevice      - IO device from which file data is read
    //  responseHandle      - IO handle from which file data is read
    //  postType            - Replace (create) or Append.  In either case if
    //                        the file doesn't exist it will be created.
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the device, handle, or status objects until the
    //  operation is complete.
    //EXAMPLES
    //  //Upload a file for the local player at index 0.
    //  //If the file exists it is replaced.
    //  rlUserStorage::PostFile(0, "mpsaves/save1.json", device, handle, rlUserStorage::POST_REPLACE, &myStatus);
    //
    //  //Append to a file for the local player at index 0.
    //  //If the file doesn't exist it is created.
    //  rlUserStorage::PostFile(0, "logs/log1.txt", device, handle, rlUserStorage::POST_APPEND, &myStatus);
    static bool PostFile(const int localGamerIndex,
                        const char* dstFilePath,
                        const fiDevice* srcDevice,
                        const fiHandle srcHandle,
                        const PostType postType,
                        netStatus* status);

    //PURPOSE
    //  Deletes a file from User Storage.
    //PARAMS
    //  localGamerIndex     - Local index of gamer who is uploading the file.
    //                        The gamer must have a valid Rockstar ID.
    //  filePath            - Path of file to delete
    //  status              - Can be monitored for completion
    //NOTES
    //  This is an asynchronous operation that is complete when
    //  status->Pending() returns false.
    //  Do not deallocate the status object until the operation
    //  is complete.
    //EXAMPLES
    //  //Delete a file for the local player at index 0.
    //  rlUserStorage::DeleteFile(0, "mpsaves/save1.json", &myStatus);
    static bool DeleteFile(const int localGamerIndex,
                            const char* filePath,
                            netStatus* status);

    //PURPOSE
    //  Cancels the operation associated with the status object.
    static void Cancel(netStatus* status);
};


} //namespace rage

#endif  //RLINE_RLUSERSTORAGE_H
