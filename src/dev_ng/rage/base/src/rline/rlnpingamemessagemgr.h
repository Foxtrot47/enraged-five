// 
// rline/rlNpInGameMessage.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#ifndef RLINE_RLNPINGAMEMESSAGE_H
#define RLINE_RLNPINGAMEMESSAGE_H

#include "system/threadpool.h"
#include "rline/rlnpcommon.h"
#include "rline/rlgamerinfo.h"

#include <np.h>

namespace rage
{
	class PrepareMessageWorkItem : public sysThreadPool::WorkItem
	{
	public:
		PrepareMessageWorkItem();
		void Configure(int libCtxId);
		void DoWork();

	private:
		int m_LibCtxId;
	};

	class rlNpInGameMessageMgr
	{
	public:
		rlNpInGameMessageMgr();

		bool Init();
		void Shutdown();

		int SendMessage(const int localGamerIndex, const rlGamerHandle& dest, const void * data, const unsigned size);
		int SendMessage(const int localGamerIndex, const rlGamerHandle* recipients, const unsigned numRecipients, const void * data, const unsigned size);

	private:
		rlNpEventDelegator::Delegate m_NpDlgt;
		void OnNpEvent(rlNp* np, const rlNpEvent* evt);

		bool m_bInitialized;
		int m_LibCtxId;
		PrepareMessageWorkItem m_PrepareWorkItem;

	}; // rlNpInGameMessage

} // namespace rage

#endif // RLINE_RLNPINGAMEMESSAGE_H
#endif // RSG_ORBIS
