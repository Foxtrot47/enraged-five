// 
// rline/rlsystemui.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSYSTEMUI_H
#define RLINE_RLSYSTEMUI_H

#include "diag/channel.h"
#include "rl.h"
#include "net/status.h"

namespace rage
{

/*
    rlSystemUi provides a platform-independent interface for presenting
	system UIs.  Examples include the sign-in UI, player review UI, etc.

    rlSystemUi assumes that system UIs are modal (i.e. they capture all
    user input), that they are displayed on top of the game view,
    and that only one system UI can be displayed at a time.

    NOTES
    
    On some platforms (Xbox) it's not possible to determine
    which UI is currently showing, only that *some* UI is showing.

    On some platforms (PS3) system UIs require memory to be allocated
    from the system.  Each UI differs in the amount of memory required.
    For example, the sign-in UI requires 8MB.  This memory is used only
    for the duration that the UI is displayed, but it must be available
    when showing the UI.
*/

class rlGamerHandle;

struct rlSystemBrowserConfig
{
	rlSystemBrowserConfig()
		: m_DialogMode(DM_DEFAULT)
		, m_ScreenMode(SM_DEFAULT)
		, m_Url(nullptr)
		, m_CallbackUrl(nullptr)
		, m_Width(0)
		, m_Height(0)
		, m_PosX(0)
		, m_PosY(0)
		, m_HeaderWidth(0)
		, m_HeaderX(0)
		, m_HeaderY(0)
		, m_Parts(PARTS_NONE)
		, m_Controls(CTRL_NONE)
        , m_ViewOptions(VIEW_OPTION_NONE)
		, m_Animate(true)
		, m_SafeRegion(false)
		, m_Flags(FLAGS_NONE)
#if RL_NP_DEEP_LINK_MANUAL_CLOSE
		, m_DeepLinkCloseTime(0)
#endif
	{
	}

	// Mode the browser will run in (SCE only)
	// Enable custom mode to use Parts/Controls and custom properties.
	enum DialogMode
	{
		DM_DEFAULT,
		DM_CUSTOM
	};

	enum ScreenMode
	{
		SM_DEFAULT,
		SM_FULLSCREEN,
		SM_EXTERNAL_BROWSER		// Mobile only at the moment, the OS browser will be used instead of the embedded one
	};

	// Components of the Browser to Display (SCE Only)
	enum Parts
	{
		PARTS_NONE		= 0,
		PARTS_TITLE		= BIT0, // Title will display
		PARTS_ADDRESS	= BIT1, // Address bar will display
		PARTS_FOOTER	= BIT2, // Footer guide will display
		PARTS_BG		= BIT3,
		PARTS_WAIT		= BIT4
	};

	// Controls the Browser will use (SCE Only)
	enum Controls
	{
		CTRL_NONE		= 0,
		CTRL_EXIT		= BIT0,
		CTRL_RELOAD		= BIT1,
		CTRL_BACK		= BIT2,
		CTRL_FORWARD	= BIT3,
		CTRL_ZOOM		= BIT4,
		CTRL_EXIT_UNTIL_COMPLETE = BIT(5)	// Exit (enabled until loading completes for the main frame of the page)
	};

    // View options the Browser will use (SCE Only)
    enum ViewOption
    {
        VIEW_OPTION_NONE        = 0,
        VIEW_OPTION_TRANSPARENT = BIT0,
        VIEW_OPTION_NO_CURSOR   = BIT1,
    };

    enum Flags
    {
        FLAGS_NONE = 0,
        FLAG_DEEP_LINK_URL = BIT0,
        FLAG_PLATFORM_SUBSCRIPTION_CHECK = BIT1,
    };

	void SetFullScreen()
	{
		m_DialogMode = rlSystemBrowserConfig::DM_CUSTOM;
		m_ScreenMode = rlSystemBrowserConfig::SM_FULLSCREEN;
		m_Parts = rlSystemBrowserConfig::PARTS_TITLE | rlSystemBrowserConfig::PARTS_ADDRESS | rlSystemBrowserConfig::PARTS_FOOTER | rlSystemBrowserConfig::PARTS_BG | rlSystemBrowserConfig::PARTS_WAIT;
		m_Controls = rlSystemBrowserConfig::CTRL_EXIT | rlSystemBrowserConfig::CTRL_RELOAD | rlSystemBrowserConfig::CTRL_BACK | rlSystemBrowserConfig::CTRL_FORWARD;
	}

	void DisableExit()
	{
		// clear the exit flag
		m_Controls &= ~rlSystemBrowserConfig::CTRL_EXIT;

		// add the 'exit until complete' flag' - browser can only be closed until its loaded, then requires browser action.
		m_Controls |= rlSystemBrowserConfig::CTRL_EXIT_UNTIL_COMPLETE;
	}

	void SetUseSafeRegion(bool bUseSafeRegion)
	{
		m_SafeRegion = bUseSafeRegion;
	}

	void SetHeaderEnabled(bool bEnabled)
	{
		if(bEnabled)
		{
			m_Parts |= rlSystemBrowserConfig::PARTS_TITLE;
			m_Parts |= rlSystemBrowserConfig::PARTS_ADDRESS;
		}
		else
		{
			m_Parts &= ~rlSystemBrowserConfig::PARTS_TITLE;
			m_Parts &= ~rlSystemBrowserConfig::PARTS_ADDRESS;
		}
	}

	void SetFooterEnabled(bool bEnabled)
	{
		if(bEnabled)
		{
			m_Parts |= rlSystemBrowserConfig::PARTS_FOOTER;
		}
		else
		{
			m_Parts &= ~rlSystemBrowserConfig::PARTS_FOOTER;
		}
	}

	void SetExternalBrowser()
	{
		m_ScreenMode = SM_EXTERNAL_BROWSER;
	}

	// Default or Custom dialog mode (SCE only)
	DialogMode m_DialogMode;
	ScreenMode m_ScreenMode;

	// Url to display in the system web browser
	const char* m_Url;

	// If this url is loaded, the browser will exit (SCE only)
	const char* m_CallbackUrl;

	// Custom window properties (SCE only)
	u16 m_Width;		// Width of the window (max 1920)
	u16 m_Height;		// Height of the window (max 1080)
	u16 m_PosX;			// Offset position X
	u16 m_PosY;			// Offset position Y
	u16 m_HeaderWidth;	// Header width (max 1920)
	u16 m_HeaderX;		// Header position X
	u16 m_HeaderY;		// Header position Y
	u32 m_Parts;		// Parts enum
	u32 m_Controls;		// Controls enum
    u32 m_ViewOptions;  // View Options enum
	bool m_Animate;		// Disable animation
	bool m_SafeRegion;  // Resize browser to fit safe region
    u32 m_Flags;        // Flags enum

#if RL_NP_DEEP_LINK_MANUAL_CLOSE
	u32 m_DeepLinkCloseTime; // Time at which we close the browser
#endif
};

class rlSystemUi
{
public:
	enum eMarketplaceEntryPoints
	{
		UI_ENTRYPOINT_CONTENTLIST = 0,	  // UI to a list of content offers.
		UI_ENTRYPOINT_CONTENTITEM ,       // UI to a specific content offer.
		UI_ENTRYPOINT_MEMBERSHIPLIST,     // UI to a list of membership offers.
		UI_ENTRYPOINT_MEMBERSHIPITEM      // UI to a specific membership offer.
	};

    enum eSignInFlags
    {
        SIGNIN_NONE        = 0,
        SIGNIN_FORCE       = BIT0,
        SIGNIN_ONLINE_ONLY = BIT1,
    };

	enum eDownloadableItemsUI
	{
		FREE_DOWNLODABLE_ITEMS_UI = 0,  // Show the free downloadable items UI. You must only specify free items or consumable
										// items that were previously purchased. Use XMarketplaceDoesContentIdMatch to 
										// determine what items has the user downloaded.

		BUYABLE_DOWNLODABLE_ITEMS_UI	// Allows you to present both paid and free items to the player for purchase. 
										// The text on this blade reads "You have chosen one or more items for download", 
										// plus it lists the offer name for each of the specified items. If a consumable 
										// is selected, the user will be charged for the item, even if it was already purchased. 
	};



public:

    rlSystemUi();

    ~rlSystemUi();

    //PURPOSE
    //  Initialize the system UI instance.
    bool Init();

    //PURPOSE
    //  Shutdown the system UI instance.
    void Shutdown();

    //PURPOSE
    //  Call this on a regular interval (at least every 100 ms)
    //  to poll for system UI events.
    void Update();

	//PURPOSE
	//  Returns true if a system UI is showing.
	//NOTES
	//  It's not possible to determine which UI is showing.
	bool IsUiShowing() const;

    //PURPOSE
    //  Show the sign-in UI.
    //NOTES
    //  On Xbox the sign-in UI can be used to sign in both online and offline
    //  gamers.
    //  On PS3 the sign-in UI can be used only to sign in online gamers.
    bool ShowSigninUi(const int localGamerIndex = 0, unsigned flags = SIGNIN_NONE);

    //PURPOSE
    //  Shows the gamer profile UI (gamer card).
    //PARAMS
    //  localGamerIndex     - Index of local gamer making the request.
    //  target              - Gamer handle of gamer who's profile will be shown.
    bool ShowGamerProfileUi(const int localGamerIndex,
                            const rlGamerHandle& target);

    //PURPOSE
    //  Shows the gamer feedback UI.
    //PARAMS
    //  localGamerIndex     - Index of local gamer making the request.
    //  target              - Gamer handle of gamer on whom feedback will
    //                        be given.
    bool ShowGamerFeedbackUi(const int localGamerIndex,
                            const rlGamerHandle& target);

	//PURPOSE
	//  Shows the message compose UI
	//PARAMS
	//  localGamerIndex    - Index of the currently signed-in user who is sending the message
	//  recipients			- Gamer handles of gamers who message will be sent to
	//  numRecipients       - Number of recipients
	//  subject             - Default message subject
	//  salutation			- Default message content
	bool ShowMessageComposeUi(const int localGamerIndex,
                              const rlGamerHandle* recipients,
                              const unsigned numRecipients,
                              const char* subject,
                              const char* salutation);

	//PURPOSE
	//  Displays the marketplace user interface.
	//PARAMS
	//  localGamerIndex     - Index of the currently signed-in user for whom to display content.
	//  iOfferType           - Starting location in the marketplace UI. Use one of the values from eMarketplaceEntryPoints.
	//  iOfferId             - Number that specifies the offering ID, if the entry point is specific to an offer. Default value 
	//                           of (0) when using a list of content offers for the offer type.
	//  iContentCategories   - Bit filter to display only certain types of items, as defined by the title. 
	//                           (-1) to specify content from all categories. Corresponds to the "content category" when adding a content 
	//							 offer to a project.
	bool ShowMarketplaceUi(const int localGamerIndex, 
							const int iOfferType, 
							const unsigned long iOfferId = 0, 
							const int iContentCategories = -1);

	//PURPOSE
	//  Displays the marketplace user interface for downloadable items.
	//PARAMS
	//  localGamerIndex     - Index of the currently signed-in user for whom to display content.
	//  iUItype              - The entry point from which the UI is downloaded. Must be either values from eDownloadableItemsUI.
	//  pOfferId             - An array of Offer IDs that can be downloaded or purchased. Maximum number is XMARKETPLACE_MAX_OFFERIDS.
	//  iOfferIdSize         - Size of the array of offers IDs.
	//NOTES
	//  Array size has to be minimum of 1.
	bool ShowMarketplaceDownloadItemsUi(const int localGamerIndex, 
										const int iUItype, 
										const unsigned long long* pOfferId,
										const int iOfferIdSize);

	//PURPOSE
	// Displays a dialog indicating that the marketplace is empty (Orbis only)
	bool ShowMarketplaceEmptyUi(const int localGamerIndex);

    //PURPOSE
    //  Displays the community sessions UI, with the party page selected.
    bool ShowPartySessionsUi(const int localGamerIndex);

	//PURPOSE
	//  Displays the send party invite UI
	bool ShowSendInviteUi(const int localGamerIndex);

	//PURPOSE
	// Shows the UI to upgrade your account (i.e. PS+, XBL Gold)
	bool ShowAccountUpgradeUI(const int localGamerIndex);

	//PURPOSE
	// Shows the system web browser to the specified url
	//	callbackUrl: optional, SCE only - will close the browser when a URL beginning with this path is reached
	bool ShowWebBrowser(const int localGamerIndex, const char * url, const char* callbackUrl = nullptr);
	bool ShowWebBrowser(const int localGamerIndex, const rlSystemBrowserConfig& config);

	//PURPOSE
	// Shows the application help menu using the given gamer index
	bool ShowAppHelpMenu(const int localGamerIndex);

	//PURPOSE
	// Shows the system UI for adding friends
	bool ShowAddFriendUI(const int localGamerIndex, const rlGamerHandle& gamerHandle, const char* szMessage);

	//PURPOSE
	// Returns whether or not the platform allows system messaging
	bool CanSendMessages();

	//PURPOSE
	// Returns whether or not the platform allows direct adding of friends
	bool CanAddFriendUI();

	//PURPOSE
	// Shows the system UI for adding friends
	bool ShowFriendSearchUi(const int localGamerIndex);

private:

    void FinishUi();

    void TerminateUi();

    bool NativeInit();

    void NativeShutdown();

    void NativeUpdate();

    bool NativeShowSigninUi(const int localGamerIndex,
							unsigned flags);

    bool NativeShowGamerProfileUi(const int localGamerIndex,
                                const rlGamerHandle& target);

	bool NativeShowMessageComposeUi(const int localGamerIndex,
                                    const rlGamerHandle* recipients,
                                    const unsigned numRecipients,
                                    const char* subject,
                                    const char* salutation);

    bool NativeShowGamerFeedbackUi(const int localGamerIndex,
                                const rlGamerHandle& target);

	bool NativeShowMarketplaceUi(const int localGamerIndex, 
								const int iOfferType, 
								const unsigned long iOfferId, 
								const int iContentCategories);

	bool NativeShowMarketplaceDownloadItemsUi(const int localGamerIndex, 
											  const int iUItype, 
											  const unsigned long long* pOfferId,
											  const int iOfferIdSize);

	bool NativeShowMarketplaceEmptyUi(const int localGamerIndex);

    bool NativeShowPartySessionsUi(const int localGamerIndex);
	
	bool NativeShowSendInviteUi(const int localGamerIndex);

	bool NativeShowAccountUpgradeUI(const int localGamerIndex);

	bool NativeShowAddFriendUI(const int localGamerIndex, const rlGamerHandle& gamerHandle, const char* szMessage);

	bool NativeShowWebBrowser(const int localGamerIndex, const rlSystemBrowserConfig& config);

	bool NativeShowAppHelpMenu(const int localGamerIndex);

    void NativeFinishUi();

    void NativeTerminateUi();

	bool NativeShowFriendSearchUi(const int localGamerIndex);

    bool m_Initialized      : 1;
};

extern rlSystemUi g_SystemUi;

}   //namespace rage

#endif  //RLINE_RLSYSTEMUI_H
