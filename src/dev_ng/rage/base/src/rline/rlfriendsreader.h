// 
// rline/rlfriendsreader.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLFRIENDSREADER_H
#define RLINE_RLFRIENDSREADER_H

#include "rlworker.h"
#include "net/status.h"
#include "system/criticalsection.h"

namespace rage
{

class rlFriend;
class rlGamerInfo;

//PURPOSE
//  Asynchronously reads an array of friends for a specific gamer.
//  The gamer requesting the read must be a local gamer.
//
//  To read friends for a gamer, allocate an array of rlFriend
//  and call Read(), which will return immediately.
//
//  The operation is complete when GetStatus() returns STATUS_READ_SUCCEEDED.
//
//  After calling Read() the caller must not de-allocate the rlFriend
//  array until IsPending() returns false.
//
//  Update() should be called at least once every 100 ms.
class rlFriendsReader
{
public:
    rlFriendsReader();
    ~rlFriendsReader();

    //PARAMS
    //  cpuAffinity     - CPU on which the worker thread will run.
    bool Init(const int cpuAffinity);

    void Shutdown();

    void Update();

	enum FriendReaderType
	{
		FRIENDS_ALL = 0x01,
		FRIENDS_ONLINE = 0x02,			// Online friends only
		FRIENDS_ONLINE_IN_TITLE = 0x04,	// Online friends playing our title
		FRIENDS_FAVORITES = 0x08,		// Favorite friends only (XB1)
		FRIENDS_TWOWAY = 0x10,			// Two-Way friends only (XB1)
		FRIENDS_PRESORT_ID = 0x20,		// Sort by Online Ids (PS4)
		FRIENDS_PRESORT_ONLINE = 0x40,	// Sort by Online Status (PS4)
		FRIENDS_JOINABLE_SESSION_PRESENCE = 0x80,		// Retrieves the 'IsInSession()' flag (PC)
	};

    //PURPOSE
    //  Initiates an asynchronous read of a friends list.
    //  The call returns immediately but the read is not complete until
    //  GetStatus() returns STATUS_READ_SUCCEEDED.
    //  The caller must not de-allocate the rlFriend array until
    //  IsPending() returns false.
    //PARAMS
    //  localGamerIndex     - Index of local gamer requesting the read.
    //  firstFriendIndex    - Index of first friend to read
    //  friends             - Array of rlFriend.
    //  maxFriends          - Length of rlFriend array.
    //  numFriends          - On completion contains number of friends read.
	//	totalFriends		- The total size of a user's friends list
	//  friendFlags			- Friends filter for online, context, etc 
    //  status              - Status object that can be polled for completion.
    //NOTES
    //  This is an asynchronous operation and is not complete until the
    //  status object is no longer in a Pending state.
    //  The memory referenced by friends, numFriends, and status
    //  must not be deallocated until the operation is complete.
    //  An operation cannot be initiated if rlFriendsReader::Pending()
    //  return true.
    bool Read(const int localGamerIndex,
                const unsigned firstFriendIndex,
                rlFriend* friends,
                const unsigned maxFriends,
                unsigned* numFriends,		// number of friends returned
				unsigned* totalFriends,		// total number of friends
				int friendFlags,
                netStatus* status);

    //PURPOSE
    //  Cancels a pending operation
    void Cancel(netStatus* status);

private:

    bool m_Initialized  : 1;

};

}   //namespace rage

#endif  //RLINE_RLFRIENDSREADER_H
