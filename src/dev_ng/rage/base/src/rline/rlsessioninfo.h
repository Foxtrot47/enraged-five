// 
// rline/rlsessioninfo.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSESSIONINFO
#define RLINE_RLSESSIONINFO

#include "rl.h"
#include "rlpeeraddress.h"

#if RSG_DURANGO
#include "rline/durango/rlxblsessions_interface.h" // for rlXblSessionHandle
#endif 

namespace rage
{

class rlSession;

//PURPOSE
//  Platform session identifier.
//  Uniquely identifies a host/session combination.
//  After a host migration the session token will change.
class rlSessionToken
{
public:

    static const rlSessionToken INVALID_SESSION_TOKEN;

    rlSessionToken()
        : m_Value(0)
    {
    }

    bool IsValid() const
    {
        return 0 != m_Value;
    }

	void Clear()
	{
		m_Value = 0;
	}

	u64 GetValue() const
	{
		return m_Value;
	}

    bool operator==(const rlSessionToken& that) const
    {
        return m_Value == that.m_Value;
    }

    bool operator!=(const rlSessionToken& that) const
    {
        return m_Value != that.m_Value;
    }

    //For use in sorting/searching operations
    bool operator<(const rlSessionToken& that) const
    {
        return m_Value < that.m_Value;
    }

    u64 m_Value;
};

class rlSessionInfo
{
    friend class rlSession;

public:

	static const rlSessionInfo INVALID_SESSION_INFO;

    rlSessionInfo();

    //PURPOSE
    //  Clears to the initial state.
    void Clear();

    //PURPOSE
    //  Returns true for valid instances.
    bool IsValid() const;

	//PURPOSE
	//  Returns true if cleared / reset
	bool IsClear() const;

    //PURPOSE
    //  Returns the session token.
    //RETURNS
    //  Unique identifier.
    //NOTES
    //  The session token uniquely identifies a host/session combination.
    //  If the host migrates the session token will change.  The session ID,
    //  however, will remain unchanged.
    //  It can be used to search for a session when all that is available is
    //  a rlSessionInfo, e.g. when an invitation is received.  This assumes
    //  that the session host advertises the current session token in
    //  matchmaking attributes.
    const rlSessionToken GetToken() const;

#if RSG_PC
	const u64 GetSessionId() const;
#endif

    //PURPOSE
    //  Returns the peer address of the host.
    const rlPeerAddress& GetHostPeerAddress() const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0,
				bool failSilently = false);

#if RSG_ORBIS
	//PURPOSE
	//  Exports the session info for NP presence.
	//  Same as Export() but omits some data from the
	//  netPeerAddress to get under the 128 byte limit.
    bool ExportForNpPresence(void* buf,
							 const unsigned sizeofBuf,
							 unsigned* size = 0) const;

	//PURPOSE
	//  Imports a session info that was exported from 
	//  ExportForNpPresence().
    bool ImportFromNpPresence(const void* buf,
							  const unsigned sizeofBuf,
							  unsigned* size = 0,
							  bool failSilently = false);
#endif

	//PURPOSE
	//  Clears the data that we don't advertise to any publicly queryable
	//  location including presence and matchmaking.
	void ClearNonAdvertisableData();

#if RSG_DURANGO
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = rlXblSessionHandle::MAX_EXPORTED_SIZE_IN_BYTES +
													   sizeof(rlSessionToken) +
													   rlPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES;
#elif RSG_PC
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = rlPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES + 
													   sizeof(u64) +
													   sizeof(rlSessionToken);
#elif RSG_ORBIS
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = rlPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES + 
													   sizeof(rlSessionToken) +
													   rlSceNpSessionId::MAX_EXPORTED_SIZE_IN_BYTES;

	static const unsigned NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES = rlPeerAddress::NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES + 
																	   sizeof(rlSessionToken) +
																	   rlSceNpSessionId::MAX_EXPORTED_SIZE_IN_BYTES;

	static const unsigned NP_PRESENCE_DATA_TO_STRING_BUFFER_SIZE = DAT_BASE64_MAX_ENCODED_SIZE(NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES);
#endif

	static const unsigned TO_STRING_BUFFER_SIZE = DAT_BASE64_MAX_ENCODED_SIZE(MAX_EXPORTED_SIZE_IN_BYTES);

	//PURPOSE
	//  Used to convert the session info to a string
	const char* ToString(char* buf, const unsigned sizeofBuf, unsigned* size = 0) const;
    template<int SIZE>
	const char* ToString(char (&buf)[SIZE], unsigned* size = 0) const
    {
        CompileTimeAssert(SIZE >= TO_STRING_BUFFER_SIZE);
        return ToString(buf, SIZE, size);
    }

	//PURPOSE
	//  Used to convert a string into a session info structure
	bool FromString(const char* buf, unsigned* size = 0, bool failSilently = false);

    bool operator==(const rlSessionInfo& that) const;
    bool operator!=(const rlSessionInfo& that) const;

#if RSG_DURANGO
	//PURPOSE
	//  Used internally to initialize the instance. Users should never call this.
	void Init(const rlXblSessionHandle& sessionHandle,
			  const rlSessionToken& sessionToken,
			  const rlPeerAddress& hostPeerAddr);

	const rlXblSessionHandle& GetSessionHandle() const;
#elif RSG_PC || RSG_NP
	//PURPOSE
	//  Used internally to initialize the instance. Users should never call this.
	void Init(const u64 sessionId, const rlSessionToken& sessionToken, const rlPeerAddress& hostPeerAddr);
#endif

#if RSG_ORBIS
	// PURPOSE
	//	Set, Get and Clear the PSN Web Api Session Id
	const rlSceNpSessionId& GetWebApiSessionId() const;
	void SetWebApiSessionId(const rlSceNpSessionId& sessionId);
	void ClearWebApiSessionId();
#endif

private:

#if RSG_DURANGO
	rlXblSessionHandle m_SessionHandle;
#elif RSG_PC
	u64 m_SessionId;
#elif RSG_ORBIS
	rlSceNpSessionId m_WebApiSessionId;
#endif

#if RL_NP_CPPWEBAPI
    rlNpMatchId m_WepApiMatchId;
#endif

#if RSG_XBOX || RSG_NP || RSG_PC
	rlSessionToken m_SessionToken;
	rlPeerAddress m_HostPeerAddr;
#endif
};

}   //namespace rage

#endif  //RLINE_RLSESSIONINFO
