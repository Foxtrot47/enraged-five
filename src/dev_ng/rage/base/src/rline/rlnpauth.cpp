// 
// rline/rlNpWebBrowser.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include <sdk_version.h>
#include "rlnp.h"
#include "rlnpauth.h"
#include "rline/rlpresence.h"
#include "rline/rlnpfaulttolerance.h"
#include "diag/seh.h"
#include "system/timer.h"
#include <libhttp.h>

#define MIN_AUTH_RETRY_INTERVAL_SECONDS 10
#define MIN_PERM_RETRY_INTERVAL_SECONDS 10
#define	AUTH_CODE_SCOPE	"psn:s2s"

namespace rage
{
#if RSG_PROSPERO
	CompileTimeAssert(rlRealtimeMultiplayerProperty::Property_None == SCE_NP_REALTIME_MULTIPLAY_PROPERTY_NONE);
	CompileTimeAssert(rlRealtimeMultiplayerProperty::Property_CrossPlatform == SCE_NP_REALTIME_MULTIPLAY_PROPERTY_CROSS_PLATFORM_PLAY);
	CompileTimeAssert(rlRealtimeMultiplayerProperty::Property_InEngineSpectating == SCE_NP_REALTIME_MULTIPLAY_PROPERTY_IN_ENGINE_SPECTATING);
#endif

	PARAM(npForceAllowPermissions, "Forces all NP permissions to return true");
	PARAM(npAutoAllowPermissions, "Automatically set all NP permissions to true (without issuing checks)");
	PARAM(npDisablePermissionRetry, "Disables permission retry");
	PARAM(npMaxAvailabilityWait, "Overrides the maximum amount of time (milliseconds) to wait for the NP availability check request to finish before aborting it");
	PARAM(rlNpPlusAuthorised, "Overrides result from the NP call to check plus / premium");
	
	RAGE_DEFINE_SUBCHANNEL(rline, npauth);
	#undef __rage_channel
	#define __rage_channel rline_npauth

#if !__NO_OUTPUT
    const char* GetNpUnavailabilityCodeAsString(const int resultCode)
    {
#define SCERR_CASE(a) case a: return #a;
        switch(resultCode)
        {
            SCERR_CASE(SCE_NP_ERROR_AGE_RESTRICTION)
            SCERR_CASE(SCE_NP_ERROR_LATEST_SYSTEM_SOFTWARE_EXIST)
            SCERR_CASE(SCE_NP_ERROR_LATEST_SYSTEM_SOFTWARE_EXIST_FOR_TITLE)
            SCERR_CASE(SCE_NP_ERROR_LATEST_PATCH_PKG_EXIST)
            SCERR_CASE(SCE_NP_ERROR_LATEST_PATCH_PKG_DOWNLOADED)
            SCERR_CASE(SCE_NP_ERROR_LOGOUT)
            SCERR_CASE(SCE_NP_ERROR_NOT_SIGNED_UP)
            SCERR_CASE(SCE_NP_ERROR_SIGNED_OUT)
            SCERR_CASE(SCE_NP_ERROR_USER_NOT_FOUND)
        }
        return "Unknown Error";
    }

    const char* rlNpAuth::GetNpUnavailabilityReasonAsString(const NpUnavailabilityReason reason)
    {
#define REASON_CASE(a) case a: return #a;
        switch(reason)
        {
            REASON_CASE(NP_REASON_INVALID)
            REASON_CASE(NP_REASON_OTHER)
            REASON_CASE(NP_REASON_SYSTEM_UPDATE)
            REASON_CASE(NP_REASON_GAME_UPDATE)
            REASON_CASE(NP_REASON_SIGNED_OUT)
            REASON_CASE(NP_REASON_AGE)
            REASON_CASE(NP_REASON_CONNECTION)
        }
        return "Unknown Reason";
    }
#endif

	unsigned rlNpAuth::rlScePermissionsRequest::ms_RetryBackoff[rlNpAuth::rlScePermissionsRequest::NUM_RETRY_ATTEMPTS] = { 1000, 2000, 5000, 10000, 20000, 50000 };	

	rlNpAuth::rlNpAuth() 
		: m_bHasRetrievedIssuerId(false)
		, m_bRefreshingAuthCode(false)
		, m_bCallbacksRegistered(false)
	{
		for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			m_PermissionsRequest[i].Clear();
			m_PermissionsResult[i].Clear();
			m_bHasValidPermissions[i] = false;
			m_bSubAccount[i] = false;
		}
	}

	rlNpAuth::~rlNpAuth()
	{
		Shutdown();
	}

	bool rlNpAuth::Init()
	{
		m_AuthStatus.Reset();
		sysMemSet(&asyncParam, 0, sizeof(asyncParam));
		sysMemSet(&authParam, 0, sizeof(authParam));
		sysMemSet(&resultAuthCode, 0, sizeof(resultAuthCode));
		m_ReqId = -1;
		m_IssuerId = -1;
		
		m_AuthRetryTimeout.Clear();
		m_AuthRetryTimeout.ForceTimeout();
		return true;
	}

	void rlNpAuth::Shutdown()
	{

	}

	void rlNpAuth::Update()
	{
		m_AuthRetryTimeout.Update();
		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			m_PermissionsRequest[i].m_PermissionCooldown.Update();

			// check if we should retry the parental controls permissions
			if(m_PermissionsRequest[i].m_ParentalControlRetryTimestamp > 0 && 
				(sysTimer::GetSystemMsTime() - m_PermissionsRequest[i].m_ParentalControlRetryTimestamp) > rlScePermissionsRequest::ms_RetryBackoff[m_PermissionsRequest[i].m_ParentalControlRetryCount])
			{
				// reset timestamp, increment retry counter and re-issue request
				rlDebug1("Update :: Reissue parental control request for index %d", i);
				m_PermissionsRequest[i].m_ParentalControlRetryTimestamp = 0;
				m_PermissionsRequest[i].m_ParentalControlRetryCount++;
				StartParentalControlRequest(i);
				m_PermissionsRequest[i].m_bRefreshing = true;
			}

			// check if we should retry the plus permissions
			if(m_PermissionsRequest[i].m_PlusRetryTimestamp > 0 && 
				(sysTimer::GetSystemMsTime() - m_PermissionsRequest[i].m_PlusRetryTimestamp) > rlScePermissionsRequest::ms_RetryBackoff[m_PermissionsRequest[i].m_PlusRetryCount])
			{
				// reset timestamp, increment retry counter and re-issue request
				rlDebug1("Update :: Reissue plus request for index %d", i);
				m_PermissionsRequest[i].m_PlusRetryTimestamp = 0;
				m_PermissionsRequest[i].m_PlusRetryCount++;
				StartPlusRequest(i);
				m_PermissionsRequest[i].m_bRefreshing = true;
			}

			if(IsRefreshingPermissions(i))
			{
				WaitPermissions(i);
			}

			if(IsRefreshingAuthCode())
			{
				WaitForAuthCode(i);
			}

			if(IsRefreshingNpAvailability(i))
			{
				WaitNpAvailability(i);
			}
		}
	}

	void rlNpAuth::Reset(const int localGamerIndex)
	{
        rlDebug1("Reset :: Index: %d", localGamerIndex);
        m_PermissionsResult[localGamerIndex].Clear();
		m_PermissionsRequest[localGamerIndex].Clear();
		m_PermissionsRequest[localGamerIndex].m_PermissionCooldown.ForceTimeout();
		m_bSubAccount[localGamerIndex] = false;
		m_NpAvailable[localGamerIndex].Clear();
	}

	bool rlNpAuth::RefreshAuthCode(const int localGamerIndex)
	{
		m_AuthRetryTimeout.InitSeconds(MIN_AUTH_RETRY_INTERVAL_SECONDS);
		
		m_ReqId = GetAuthCode(localGamerIndex, resultAuthCode, &m_IssuerId, &m_AuthStatus);
		if (m_ReqId >= 0)
		{
            rlDebug1("GetAuthCode :: Index: %d", localGamerIndex);
			m_bRefreshingAuthCode = true;
			return true;
		}

		return false;
	}

	int rlNpAuth::GetAuthCode(const int localGamerIndex, rlSceNpAuthorizationCode& authCode, int* issuerId, netStatus* status)
	{
		int reqId = -1;
		memset(&authCode, 0, sizeof(authCode));

		SceNpAuthCreateAsyncRequestParameter asyncParam = {0};
		asyncParam.size = sizeof(asyncParam);
		asyncParam.threadPriority = SCE_KERNEL_PRIO_FIFO_DEFAULT;
		asyncParam.cpuAffinityMask = SCE_KERNEL_CPUMASK_USER_ALL;

		int ret = sceNpAuthCreateAsyncRequest(&asyncParam);
		if(ret < 0) 
		{
			rlAssertf(false, "sceNpAuthCreateAsyncRequest failed. ret = 0x%08x", ret);
			return ret;
		}

		reqId = ret;

		SceNpAuthGetAuthorizationCodeParameter authParam = {0};
		authParam.size = sizeof(authParam);
		authParam.pOnlineId = &g_rlNp.GetNpOnlineId(localGamerIndex).AsSceNpOnlineId();
		authParam.pClientId = &g_rlNp.GetClientId()->AsSceNpClientId();
		authParam.pScope = AUTH_CODE_SCOPE;
		ret = sceNpAuthGetAuthorizationCode(reqId, &authParam, (SceNpAuthorizationCode*)&authCode, issuerId);
		FAULT_TOLERANCE_FAILURE(ATSTRINGHASH("NpAuthGetCode", 0xB75CD16),ret);
		if (ret < 0)
		{
			rlAssertf(false, "sceNpAuthGetAuthorizationCode failed, ret = 0x%08x", ret);
			sceNpAuthDeleteRequest(reqId);
			return ret;
		}

		status->SetPending();

		return reqId;
	}

	int rlNpAuth::UpdateAuthCodeRequest(const int /*localGamerIndex*/, int reqId, netStatus* status)
	{
		rlAssert(reqId >= 0);
		rlAssert(status->Pending());

		int result = 0;
		int	ret = sceNpAuthPollAsync(reqId, &result);
		FAULT_TOLERANCE_FAILURE(ATSTRINGHASH("NpAuthPoll", 0xFFFC6BB6),ret);
		if(ret < 0) 
		{
			rlAssertf(false, "sceNpAuthPollAsync() failed. ret = 0x%08x", ret);
			status->SetFailed();
			sceNpAuthDeleteRequest(reqId);
			return ret;
		}

		if(ret == SCE_NP_AUTH_POLL_ASYNC_RET_RUNNING) 
		{
			return ret;
		}

		sceNpAuthDeleteRequest(reqId);

		if(ret == SCE_NP_AUTH_POLL_ASYNC_RET_FINISHED) 
		{
			if(result < 0) 
			{
				rlError("sceNpAuthPollAsync() failed. ret = 0x%08x", result);
				status->SetFailed();
			}
			else
			{
				status->SetSucceeded();
			}
		} 
		else 
		{
			rlAssertf(false, "sceNpAuthPollAsync() failed. ret = 0x%08x", ret);
			status->SetFailed();
		}

		return ret;
	}

	void rlNpAuth::CancelAuthCodeRequest(const int OUTPUT_ONLY(localGamerIndex), int reqId, netStatus* status)
	{
        rlDebug1("CancelAuthCodeRequest :: Index: %d, ReqId: %d", localGamerIndex, reqId);
        
        rlAssert(reqId >= 0);
		rlAssert(status->Pending());

		status->SetCanceled();
		int	ret = sceNpAuthAbortRequest(reqId);
		rlAssertf(ret >= 0, "CancelAuthCodeRequest :: sceNpAuthAbortRequest() failed. ret = 0x%08x", ret);

		ret = sceNpAuthDeleteRequest(reqId);
		rlAssertf(ret >= 0, "CancelAuthCodeRequest :: sceNpAuthDeleteRequest() failed. ret = 0x%08x", ret);
	}

	void rlNpAuth::WaitForAuthCode(const int localGamerIndex)
	{
		UpdateAuthCodeRequest(localGamerIndex, m_ReqId, &m_AuthStatus);
		if (!m_AuthStatus.Pending())
		{
            rlDebug1("WaitForAuthCode :: Completed - Index: %d, Success: %s", localGamerIndex, m_AuthStatus.Succeeded() ? "True" : "False");
            if(m_AuthStatus.Succeeded())
			{
				m_bHasRetrievedIssuerId = true;
			}
			m_bRefreshingAuthCode = false;
		}
	}

	bool rlNpAuth::RequestPermissions(const int localGamerIndex)
	{
		bool success = StartPermissionsRequest(localGamerIndex);

		// clear any previous result if permissions request fails to start
		if(!success)
		{
			m_PermissionsResult[localGamerIndex].Clear();
		}

		return success;
	}

	bool rlNpAuth::StartPermissionsRequest(const int localGamerIndex)
	{
		rlDebug1("StartPermissionsRequest :: Index: %d", localGamerIndex);

		rlAssertf(m_PermissionsRequest[localGamerIndex].m_ParamId == -1, "Async param request still active!");
		rlAssertf(m_PermissionsRequest[localGamerIndex].m_PlusId  == -1, "Async plus request still active!");

		m_PermissionsRequest[localGamerIndex].Clear();
		m_PermissionsRequest[localGamerIndex].m_PermissionCooldown.InitSeconds(MIN_PERM_RETRY_INTERVAL_SECONDS);

		// automatically assume valid permissions
		if (PARAM_npAutoAllowPermissions.Get())
		{
			// mark that we have valid permissions - this only needs to be completed once
			m_bHasValidPermissions[localGamerIndex] = true; 

			m_PermissionsRequest[localGamerIndex].m_ParentalControlInfo.contentRestriction = false;
			m_PermissionsRequest[localGamerIndex].m_ParentalControlInfo.chatRestriction = false;
			m_PermissionsRequest[localGamerIndex].m_ParentalControlInfo.ugcRestriction = false;
			m_PermissionsRequest[localGamerIndex].m_Age = 35;
			m_PermissionsRequest[localGamerIndex].m_PlusResult.authorized = true;  
			m_PermissionsRequest[localGamerIndex].m_bRefreshing = false;
			m_PermissionsRequest[localGamerIndex].m_PermissionCooldown.ForceTimeout();
			m_PermissionsRequest[localGamerIndex].m_PlusInfoReceived = true;
			m_PermissionsRequest[localGamerIndex].m_ParentalControlInfoReceived = true;

			m_PermissionsResult[localGamerIndex].Set(m_PermissionsRequest[localGamerIndex]);

			rlDebug("StartPermissionsRequest :: Automatically Assigned Auth for for index %d. Age: %d. Restrictions: content: %s, chat: %s, ugc: %s, PS plus: %s ", 
				localGamerIndex, 
				m_PermissionsResult[localGamerIndex].m_Age,
				m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.contentRestriction ? "BLOCKED (PENDING CheckNpAvailability)" : "ALLOWED",
				m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.chatRestriction ? "BLOCKED" : "ALLOWED",
				m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.ugcRestriction ? "BLOCKED" : "ALLOWED",
				m_PermissionsResult[localGamerIndex].m_PlusResult.authorized ? "AUTHORIZED" : "NOT AUTHORIZED");

			rlNpEventPlayStationPlusUpdate e(localGamerIndex, false);
			g_rlNp.DispatchEvent(&e);

			return true; 
		}

		if(!StartParentalControlRequest(localGamerIndex))
			return false;

		if(!StartPlusRequest(localGamerIndex))
			return false;

		// set pending flags
		m_PermissionsRequest[localGamerIndex].m_bRefreshing = true;

		return true;
	}

	bool rlNpAuth::StartParentalControlRequest(const int localGamerIndex)
	{
		// Parental Controls and Age
		SceNpCreateAsyncRequestParameter param = { 0 };
		param.size = sizeof(param);
		m_PermissionsRequest[localGamerIndex].m_ParamId = sceNpCreateAsyncRequest(&param);
		if (m_PermissionsRequest[localGamerIndex].m_ParamId < SCE_OK)
		{
			rlError("sceNpCreateAsyncRequest() failed, ret = 0x%08x", m_PermissionsRequest[localGamerIndex].m_ParamId);
			return false;
		}

		rlDebug1("StartParentalControlRequest :: Index: %d, RequestId: %d", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_ParamId);

		int ret = sceNpGetParentalControlInfo(m_PermissionsRequest[localGamerIndex].m_ParamId, &g_rlNp.GetNpOnlineId(localGamerIndex).AsSceNpOnlineId(), &m_PermissionsRequest[localGamerIndex].m_Age, &m_PermissionsRequest[localGamerIndex].m_ParentalControlInfo);
		FAULT_TOLERANCE_FAILURE(ATSTRINGHASH("NpPermissions", 0xBD8A8925),ret);
		if (ret != SCE_OK)
		{
			rlError("sceNpGetParentalControlInfo() failed, ret = 0x%08x", ret);
			if (m_PermissionsRequest[localGamerIndex].m_ParamId >= 0)
			{
				sceNpDeleteRequest(m_PermissionsRequest[localGamerIndex].m_ParamId);
				m_PermissionsRequest[localGamerIndex].m_ParamId = -1;
			}
			return false;
		}

		return true; 
	}

	bool rlNpAuth::StartPlusRequest(const int localGamerIndex)
	{
		// PlayStation Plus
		SceNpCreateAsyncRequestParameter plusParam = { 0 };
		plusParam.size = sizeof(plusParam);
		m_PermissionsRequest[localGamerIndex].m_PlusId = sceNpCreateAsyncRequest(&plusParam);
		if (m_PermissionsRequest[localGamerIndex].m_PlusId < SCE_OK)
		{
			rlError("sceNpCreateAsyncRequest() failed, ret = 0x%08x", m_PermissionsRequest[localGamerIndex].m_PlusId);
			return false;
		}

        rlDebug1("StartPlusRequest :: Index: %d, RequestId: %d", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_PlusId);

		SceNpCheckPlusParameter checkParam = { 0 };
		checkParam.size = sizeof(checkParam);
		checkParam.features = SCE_NP_PLUS_FEATURE_REALTIME_MULTIPLAY; 
		checkParam.userId = g_rlNp.GetUserServiceId(localGamerIndex);
		sysMemSet(&m_PermissionsRequest[localGamerIndex].m_PlusResult, 0, sizeof(m_PermissionsRequest[localGamerIndex].m_PlusResult));

		int ret = sceNpCheckPlus(m_PermissionsRequest[localGamerIndex].m_PlusId, &checkParam, &m_PermissionsRequest[localGamerIndex].m_PlusResult);
		FAULT_TOLERANCE_FAILURE(ATSTRINGHASH("NpCheckPlus", 0x654983BB),ret);
		if (ret != SCE_OK)
		{
			rlError("sceNpCheckPlus() failed, ret = 0x%08x", ret);
			if (m_PermissionsRequest[localGamerIndex].m_ParamId >= 0)
			{
				sceNpDeleteRequest(m_PermissionsRequest[localGamerIndex].m_ParamId);
				m_PermissionsRequest[localGamerIndex].m_ParamId = -1;
			}

			if (m_PermissionsRequest[localGamerIndex].m_PlusId >= 0)
			{
				sceNpDeleteRequest(m_PermissionsRequest[localGamerIndex].m_PlusId);
				m_PermissionsRequest[localGamerIndex].m_PlusId = -1;
			}
			return false;
		}

		return true; 
	}

	void rlNpAuth::WaitPermissions(const int localGamerIndex)
	{
		// Waiting for Parental Control Permissions
		if (!m_PermissionsRequest[localGamerIndex].m_ParentalControlInfoReceived && m_PermissionsRequest[localGamerIndex].m_ParamId >= 0)
		{
			int result = SCE_OK;
			int ret = sceNpPollAsync(m_PermissionsRequest[localGamerIndex].m_ParamId, &result);
			FAULT_TOLERANCE_FAILURE(ATSTRINGHASH("NpPollPermissions", 0xA35BCB2E),ret);
			// negative values for errors
			if (!rlVerify(ret >= 0))
			{
				rlError("WaitPermissions :: Poll failed for Parental Control Request, Index: %d, RequestId: %d, Error: 0x%08x", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_ParamId, ret);
				CancelPermissions(localGamerIndex);
				return;
			}

			if(ret == SCE_NP_POLL_ASYNC_RET_RUNNING) 
			{
				 // do nothing
			}
			else
			{
				if (!rlVerify(ret == SCE_NP_POLL_ASYNC_RET_FINISHED))
				{
					rlError("WaitPermissions :: Parental Control Request Failed - Index: %d, RequestId: %d, Error: 0x%08x", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_ParamId, ret);
					CancelPermissions(localGamerIndex);
					return;
				}
				else if(result != SCE_OK)
				{
					rlError("WaitPermissions :: Parental Control Request Failed - Index: %d, RequestId: %d, Result: 0x%08x", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_ParamId, ret);

                    bool bExemptCodes = (result == SCE_HTTP_ERROR_BAD_RESPONSE);
					if(!bExemptCodes && !PARAM_npDisablePermissionRetry.Get() && (m_PermissionsRequest[localGamerIndex].m_ParentalControlRetryCount < rlScePermissionsRequest::NUM_RETRY_ATTEMPTS))
					{
						rlDebug1("WaitPermissions :: Retrying Parental Control Request in %ums [%u of %u]", 
								 rlScePermissionsRequest::ms_RetryBackoff[m_PermissionsRequest[localGamerIndex].m_ParentalControlRetryCount], 
								 m_PermissionsRequest[localGamerIndex].m_ParentalControlRetryCount + 1, 
								 rlScePermissionsRequest::NUM_RETRY_ATTEMPTS);

						m_PermissionsRequest[localGamerIndex].m_ParentalControlRetryTimestamp = sysTimer::GetSystemMsTime();
					}
					else
					{
						m_PermissionsRequest[localGamerIndex].m_ParentalControlInfoReceived = true;
					}
				}
				else
				{
					rlDebug1("WaitPermissions :: Parental Control Request Completed - Index: %d, RequestId: %d", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_ParamId);
					m_PermissionsRequest[localGamerIndex].m_ParentalControlInfoReceived = true;
				}

				sceNpDeleteRequest(m_PermissionsRequest[localGamerIndex].m_ParamId);
				m_PermissionsRequest[localGamerIndex].m_ParamId = -1;
			}
		}

		// Waiting for PlayStation Plus Check
		if (!m_PermissionsRequest[localGamerIndex].m_PlusInfoReceived && m_PermissionsRequest[localGamerIndex].m_PlusId >= 0)
		{
			int result = SCE_OK;
			int ret = sceNpPollAsync(m_PermissionsRequest[localGamerIndex].m_PlusId, &result);
			FAULT_TOLERANCE_FAILURE(ATSTRINGHASH("NpPollCheckPlus", 0xF915A75B),ret);
			
			// negative values for errors
			if (!rlVerify(ret >= 0))
			{
				rlError("WaitPermissions :: Poll failed for Plus Request, Index: %d, RequestId: %d, Error: 0x%08x", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_PlusId, ret);
                CancelPermissions(localGamerIndex);
				return;
			}

			if(ret == SCE_NP_POLL_ASYNC_RET_RUNNING) 
			{
				// do nothing
			}
			else
			{
				if (!rlVerify(ret == SCE_NP_POLL_ASYNC_RET_FINISHED))
				{
                    rlError("WaitPermissions :: Plus Request Failed - Index: %d, RequestId: %d, Error: 0x%08x", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_PlusId, ret);
                    CancelPermissions(localGamerIndex);
					return;
				}
				else if(result != SCE_OK)
				{
                    rlError("WaitPermissions :: Plus Request Failed - Index: %d, RequestId: %d, Result: 0x%08x", localGamerIndex, m_PermissionsRequest[localGamerIndex].m_PlusId, ret);
                    
                    bool bExemptCodes = (result == SCE_HTTP_ERROR_BAD_RESPONSE);
                    if(!bExemptCodes && !PARAM_npDisablePermissionRetry.Get() && (m_PermissionsRequest[localGamerIndex].m_PlusRetryCount < rlScePermissionsRequest::NUM_RETRY_ATTEMPTS))
					{
                        rlDebug1("WaitPermissions :: Retrying Plus Request in %ums [%u of %u]", 
                                 rlScePermissionsRequest::ms_RetryBackoff[m_PermissionsRequest[localGamerIndex].m_PlusRetryCount], 
								 m_PermissionsRequest[localGamerIndex].m_PlusRetryCount + 1, 
								 rlScePermissionsRequest::NUM_RETRY_ATTEMPTS);

						m_PermissionsRequest[localGamerIndex].m_PlusRetryTimestamp = sysTimer::GetSystemMsTime();
					}
					else
					{
						m_PermissionsRequest[localGamerIndex].m_PlusInfoReceived = true;
					}
				}
				else 
				{
                    rlDebug1("WaitPermissions :: Plus Request Completed - Index: %d, RequestId: %d, Authorised: %s", 
						localGamerIndex,
						m_PermissionsRequest[localGamerIndex].m_PlusId,
						m_PermissionsRequest[localGamerIndex].m_PlusResult.authorized ? "True" : "False");

                    m_PermissionsRequest[localGamerIndex].m_PlusInfoReceived = true;
				}

#if RSG_OUTPUT
				int value;
				if(PARAM_rlNpPlusAuthorised.Get(value))
				{
					rlDebug1("WaitPermissions :: Ignoring Plus Result via command line: %s", value == 0 ? "False" : "True");
					m_PermissionsRequest[localGamerIndex].m_PlusResult.authorized = (value != 0);
				}
#endif

				sceNpDeleteRequest(m_PermissionsRequest[localGamerIndex].m_PlusId);
				m_PermissionsRequest[localGamerIndex].m_PlusId = -1;
			}
		}

		if (HasRetrievedPermissions(localGamerIndex))
		{
			// mark that we have valid permissions - this only needs to be completed once
			m_bHasValidPermissions[localGamerIndex] = true; 

			if (PARAM_npForceAllowPermissions.Get())
			{
				m_PermissionsRequest[localGamerIndex].m_ParentalControlInfo.contentRestriction = false;
				m_PermissionsRequest[localGamerIndex].m_ParentalControlInfo.chatRestriction = false;
				m_PermissionsRequest[localGamerIndex].m_ParentalControlInfo.ugcRestriction = false;
				m_PermissionsRequest[localGamerIndex].m_Age = 35;
				m_PermissionsRequest[localGamerIndex].m_PlusResult.authorized = true;  
			}

			// set results and clear request
			m_PermissionsResult[localGamerIndex].Set(m_PermissionsRequest[localGamerIndex]);
			m_PermissionsRequest[localGamerIndex].m_PermissionCooldown.ForceTimeout();
			m_PermissionsRequest[localGamerIndex].m_bRefreshing = false;

			rlDebug("WaitPermissions :: Completed, Index: %d, Age: %d. Restrictions: Content: %s, Chat: %s, UGC: %s, Plus: %s ", 
				localGamerIndex, 
				m_PermissionsResult[localGamerIndex].m_Age,
				m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.contentRestriction ? "BLOCKED (PENDING CheckNpAvailability)" : "ALLOWED",
				m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.chatRestriction ? "BLOCKED" : "ALLOWED",
				m_PermissionsResult[localGamerIndex].m_ParentalControlInfo.ugcRestriction ? "BLOCKED" : "ALLOWED",
				m_PermissionsResult[localGamerIndex].m_PlusResult.authorized ? "AUTHORIZED" : "NOT AUTHORIZED");

			rlNpEventPlayStationPlusUpdate e(localGamerIndex, false);
			g_rlNp.DispatchEvent(&e);
		}
	}

	void rlNpAuth::CancelPermissions(const int localGamerIndex)
	{
		if(m_PermissionsRequest[localGamerIndex].m_PlusId != -1)
		{
			sceNpDeleteRequest(m_PermissionsRequest[localGamerIndex].m_PlusId);
		}
		if(m_PermissionsRequest[localGamerIndex].m_ParamId != -1)
		{
			sceNpDeleteRequest(m_PermissionsRequest[localGamerIndex].m_ParamId);
		}
		m_PermissionsRequest[localGamerIndex].m_PlusId = -1;
		m_PermissionsRequest[localGamerIndex].m_ParamId = -1;
		m_PermissionsRequest[localGamerIndex].m_ParentalControlInfoReceived = false;
		m_PermissionsRequest[localGamerIndex].m_PlusInfoReceived = false;
		m_PermissionsRequest[localGamerIndex].m_bRefreshing = false;
		m_PermissionsResult[localGamerIndex].Clear();
	}

	bool rlNpAuth::HasRetrievedPermissions(const int localGamerIndex)
	{
		return m_PermissionsRequest[localGamerIndex].m_PlusInfoReceived && m_PermissionsRequest[localGamerIndex].m_ParentalControlInfoReceived;
	}

	// Notify PS Premium Feature
	bool rlNpAuth::NotifyPremiumFeature(const int localGamerIndex, const unsigned PROSPERO_ONLY(properties))
	{
		if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
			return false;

		if(!IsNpAvailable(localGamerIndex))
			return false;

        if(!IsPlusAuthorized(localGamerIndex))
            return false;

#if RSG_ORBIS
		SceNpNotifyPlusFeatureParameter param = { 0 };
		param.features = SCE_NP_PLUS_FEATURE_REALTIME_MULTIPLAY;
		param.userId = g_rlNp.GetUserServiceId(localGamerIndex);
		param.size = sizeof(SceNpNotifyPlusFeatureParameter);

		int ret = sceNpNotifyPlusFeature(&param);
#else
		SceNpNotifyPremiumFeatureParameter param = { 0 };
		param.userId = g_rlNp.GetUserServiceId(localGamerIndex);
		param.features = SCE_NP_PREMIUM_FEATURE_REALTIME_MULTIPLAY;
		param.properties = properties;
		param.size = sizeof(SceNpNotifyPremiumFeatureParameter);

		int ret = sceNpNotifyPremiumFeature(&param);
#endif
		if(ret != SCE_OK)
		{
			rlDebug("NotifyPremiumFeature :: Failed, Error: 0x%08x", ret);
			return false;
		}

		return true;
	}

	bool rlNpAuth::RegisterPlusCallbacks()
	{
		// callback already registered
		if (m_bCallbacksRegistered)
		{
			return true;
		}

        int ret = sceNpRegisterPlusEventCallback(NpPlusEventCallback, &g_rlNp);
        if(ret != SCE_OK)
        {
            rlDebug("RegisterPlusCallbacks :: Failed, Error: 0x%08x", ret);
            return false;
        }

        rlDebug("RegisterPlusCallbacks :: Registered");
        m_bCallbacksRegistered = true;
        
        return true;
	}

	void rlNpAuth::UnregisterPlusCallbacks()
	{
        if(m_bCallbacksRegistered)
        {
            OUTPUT_ONLY(int ret =) sceNpUnregisterPlusEventCallback();
            rlDebug("UnregisterPlusCallbacks :: Unregistered, Code: 0x%08x", ret);
            m_bCallbacksRegistered = false;
        }
	}

	void rlNpAuth::NpPlusEventCallback(SceUserServiceUserId userId, SceNpPlusEventType evtId, void *userdata)
	{
		rlNp* myNp = (rlNp*)userdata;
		rlAssert(myNp);

		int localUserIndex = myNp->GetUserServiceIndex(userId);
		if (RL_IS_VALID_LOCAL_GAMER_INDEX(localUserIndex))
		{
			// Previous states include a privileges permission refresh
			if (myNp->IsOnline(localUserIndex))
			{
				rlDebug("NpPlusEventCallback :: Index: %d, EventId: %d", localUserIndex, evtId);

				if (SCE_NP_PLUS_EVENT_RECHECK_NEEDED == evtId && myNp->CanRequestPermissions(localUserIndex))
				{
                    // we only want to check for a positive change in PS+ membership status
                    // Sony have confirmed that we don't need to consider when a player loses PS+ when in title
                    if(!myNp->GetNpAuth().IsPlusAuthorized(localUserIndex))
                    {
						rlDebug("NpPlusEventCallback :: SCE_NP_PLUS_EVENT_RECHECK_NEEDED - No subscription, checking for positive change.");
						myNp->GetNpAuth().RequestPermissions(localUserIndex);
					}
				}
			}
		}
	}

	bool rlNpAuth::CanCheckNPAvailability(const int localGamerIndex)
	{
		if (m_NpAvailable[localGamerIndex].m_Available)
			return false;

		if (m_NpAvailable[localGamerIndex].m_RequestId != rlSceNpAvailable::INVALID_REQUEST)
			return false;

		if (m_NpAvailable[localGamerIndex].m_InfoReceived)
			return false;

		return true;
	}

	bool rlNpAuth::CheckNPAvailability(const int localGamerIndex)
	{
		rtry
		{
			rverify(m_NpAvailable[localGamerIndex].m_RequestId == rlSceNpAvailable::INVALID_REQUEST
					,catchall
					,rlError("CheckNPAvailability :: Async param request still active: 0x%08x", m_NpAvailable[localGamerIndex].m_RequestId));

			rverify(!m_NpAvailable[localGamerIndex].m_Available
					,catchall
					,rlError("CheckNPAvailability :: Async param request still active: 0x%08x", m_NpAvailable[localGamerIndex].m_RequestId));

			rverify(!m_NpAvailable[localGamerIndex].m_InfoReceived
					,catchall
					,rlError("CheckNPAvailability :: Async param request still active: 0x%08x", m_NpAvailable[localGamerIndex].m_RequestId));

			m_NpAvailable[localGamerIndex].Clear();

			SceNpCreateAsyncRequestParameter param = { 0 };
			param.size = sizeof(param);

			m_NpAvailable[localGamerIndex].m_RequestId = sceNpCreateAsyncRequest(&param);
			rverify(m_NpAvailable[localGamerIndex].m_RequestId > SCE_OK
				,catchall
				,rlError("CheckNPAvailability :: sceNpCreateAsyncRequest() failed, ret = 0x%08x", m_NpAvailable[localGamerIndex].m_RequestId));
		
			const int ret = sceNpCheckNpAvailability(m_NpAvailable[localGamerIndex].m_RequestId, &g_rlNp.GetNpOnlineId(localGamerIndex).AsSceNpOnlineId(), nullptr);
			rverify(ret == SCE_OK
					,catchall
					,rlError("CheckNPAvailability :: sceNpCheckNpAvailabilityA() failed, ret = 0x%08x", ret));

			m_NpAvailable[localGamerIndex].m_Refreshing = true;
			m_NpAvailable[localGamerIndex].m_RequestTimestamp = sysTimer::GetSystemMsTime();

			rlDebug("CheckNPAvailability :: Index: %d, RequestId: %d", localGamerIndex, m_NpAvailable[localGamerIndex].m_RequestId);

			return true;
		}
		rcatchall
		{
			//Only try once
			m_NpAvailable[localGamerIndex].m_InfoReceived = true;

			if (m_NpAvailable[localGamerIndex].m_RequestId >= 0)
			{
				rlError("Failed to Start sceNpCheckNpAvailabilityA");
				sceNpDeleteRequest(m_NpAvailable[localGamerIndex].m_RequestId);
				m_NpAvailable[localGamerIndex].m_RequestId = rlSceNpAvailable::INVALID_REQUEST;
			}
		}

		return false;
	}

	void rlNpAuth::WaitNpAvailability(const int localGamerIndex)
	{
		if (!m_NpAvailable[localGamerIndex].m_InfoReceived && m_NpAvailable[localGamerIndex].m_Refreshing)
		{
			int result = 0;
			const int ret = sceNpPollAsync(m_NpAvailable[localGamerIndex].m_RequestId, &result);

			if(ret == SCE_NP_POLL_ASYNC_RET_RUNNING) 
			{
				rlAssert(m_NpAvailable[localGamerIndex].m_RequestTimestamp > 0);
				
				unsigned maxWaitTime = 10000;
				PARAM_npMaxAvailabilityWait.Get(maxWaitTime);

				const unsigned waitTime = sysTimer::GetSystemMsTime() - m_NpAvailable[localGamerIndex].m_RequestTimestamp;
				if (waitTime > maxWaitTime)
				{
					rlDebug("WaitNpAvailability :: Index: %d, RequestId: %d, Running for %ums, Aborting...", localGamerIndex, m_NpAvailable[localGamerIndex].m_RequestId, waitTime);
					
					const int deleteRet = sceNpDeleteRequest(m_NpAvailable[localGamerIndex].m_RequestId);

					if (!rlVerify(deleteRet == SCE_OK))
					{
						rlDebug("WaitNpAvailability :: Failed to delete request, Error: 0x%08x", deleteRet);
					}

					m_NpAvailable[localGamerIndex].m_RequestId = rlSceNpAvailable::INVALID_REQUEST;
					m_NpAvailable[localGamerIndex].m_Refreshing = false;
					m_NpAvailable[localGamerIndex].m_InfoReceived = false;
					m_NpAvailable[localGamerIndex].m_ResultCode = 0;
					m_NpAvailable[localGamerIndex].m_Available = false;
					m_NpAvailable[localGamerIndex].m_RequestTimestamp = 0;
				}
			}
			else
			{
				rlDebug("WaitNpAvailability :: Finished - Index: %d, RequestId: %d, Result: 0x%08x", localGamerIndex, m_NpAvailable[localGamerIndex].m_RequestId, result);

#if __ASSERT
				if(ret != SCE_NP_POLL_ASYNC_RET_FINISHED) 
				{
					rlAssertf(false, "WaitNpAvailability :: Error: 0x%08x", ret);
				}
#endif // __ASSERT

				sceNpDeleteRequest(m_NpAvailable[localGamerIndex].m_RequestId);
				m_NpAvailable[localGamerIndex].m_RequestId    = rlSceNpAvailable::INVALID_REQUEST;
				m_NpAvailable[localGamerIndex].m_Refreshing   = false;
				m_NpAvailable[localGamerIndex].m_InfoReceived = true;
				m_NpAvailable[localGamerIndex].m_ResultCode   = result;
				m_NpAvailable[localGamerIndex].m_Available    = (result == SCE_OK);
				m_NpAvailable[localGamerIndex].m_RequestTimestamp = 0;

#if !__NO_OUTPUT
                // log unavailable strings
                if(result != SCE_OK)
                {
                    rlDebug("WaitNpAvailability :: Unavailable - Code: %s, Reason: %s", 
                            GetNpUnavailabilityCodeAsString(result),
                            GetNpUnavailabilityReasonAsString(GetNpUnavailabilityReason(localGamerIndex)));
                }
#endif
			}
		}
	}

	bool rlNpAuth::IsNpAvailable(const int localGamerIndex)
	{
#if RSG_BANK
		if (rlNp::sm_bAuthUnavailable)
		{
			return false;
		}
#endif

		if (rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
		{
			return m_NpAvailable[localGamerIndex].m_Available;
		}
		
		return false;
	}

	bool rlNpAuth::HasNpResult(const int localGamerIndex)
	{
		if (rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
		{
			return m_NpAvailable[localGamerIndex].m_InfoReceived;	
		}

		return false;
	}

	rlNpAuth::NpUnavailabilityReason rlNpAuth::GetNpUnavailabilityReason(const int localGamerIndex)
	{
		// Return NP_REASON_CONNECTION if could not connect to NP, but otherwise use the true availability reason
		NpUnavailabilityReason reason = NP_REASON_INVALID;		
		if (rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
		{
			if (IsNpAvailable(localGamerIndex))
			{
				rlAssertf(false, "GetNpUnavailabilityReason called when Np is available");
				return NP_REASON_INVALID;
			}

			if (!g_rlNp.IsLoggedIn(localGamerIndex))
			{
				return NP_REASON_SIGNED_OUT;
			}
			switch(m_NpAvailable[localGamerIndex].m_ResultCode)
			{
			case SCE_NP_ERROR_AGE_RESTRICTION:
				reason = NP_REASON_AGE;
				break;
			case SCE_NP_ERROR_LATEST_SYSTEM_SOFTWARE_EXIST:
			case SCE_NP_ERROR_LATEST_SYSTEM_SOFTWARE_EXIST_FOR_TITLE:
				reason = NP_REASON_SYSTEM_UPDATE;
				break;
			case SCE_NP_ERROR_LATEST_PATCH_PKG_EXIST:
			case SCE_NP_ERROR_LATEST_PATCH_PKG_DOWNLOADED:
				reason = NP_REASON_GAME_UPDATE;
				break;
			case SCE_NP_ERROR_LOGOUT:
			case SCE_NP_ERROR_NOT_SIGNED_UP:
			case SCE_NP_ERROR_SIGNED_OUT:
			case SCE_NP_ERROR_USER_NOT_FOUND:
				reason = NP_REASON_SIGNED_OUT;
				break;
			default:
				reason = NP_REASON_OTHER;
				break;
			}
		}

		return reason;
	}

	bool rlNpAuth::IsNpUnavailabilityReasonValidForRetry(const int localGamerIndex)
	{
		NpUnavailabilityReason reason = GetNpUnavailabilityReason(localGamerIndex);
		switch(reason)
		{
		case NP_REASON_GAME_UPDATE:
		case NP_REASON_AGE:
		case NP_REASON_SYSTEM_UPDATE:
			return false;
		default:
			return true;
		}
	}

} // namespace rage

#endif // RSG_ORBIS
