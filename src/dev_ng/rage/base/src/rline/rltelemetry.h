// 
// rline/rltelemetry.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLTELEMETRY_H
#define RLINE_RLTELEMETRY_H

#include "rl.h"
#include "ros/rlroscommon.h"
#include "atl/array.h"
#include "atl/binmap.h"
#include "atl/delegate.h"
#include "atl/inlist.h"
#include "data/rson.h"
#include "data/growbuffer.h"
#include "file/stream.h"
#include "net/connectionmanager.h"
#include "net/natdetector.h"
#include "net/natpcp.h"
#include "net/natupnp.h"
#include "zlib/zlib.h"
#include "string/stringhash.h"

//Default size of the small work buffers used to process the telemetry metrics.
//This basically has to be the size of the largest EXPORTED metric
#define RL_TELEMETRY_DEFAULT_WORK_BUFFER_SIZE		(1024)

// If 0 no telemetry client config will be used
#define RL_USE_TELEMETRY_CLIENT_CONFIG 1

// If 0 the client config only acts as on/off toggle
#define RL_USE_TELEMETRY_CLIENT_CONFIG_FILE (RL_USE_TELEMETRY_CLIENT_CONFIG && 1)

namespace rage
{

class rlTelemetry;
class rlMetricList;
class rlRosTelemetrySubmissionTask;
class rlRosTelemetryImmediateSubmissionTask;

/*PURPOSE
    "Telemetry" is sending metrics to a remote host for collection and
    processing.

    Metrics classes support collecting metrics data and serializing
    them to a RSON string, which can then be saved to persistent storage
    such as a file, a remote server, or a database.

    Games wishing to collect and record runtime metrics data must
    first define subclasses of rlMetric for each type of metric they
    wish to collect.
    
    rlMetric subclasses must implement a function called Write()
    with the following signature:

        bool Write(RsonWriter* rw);

    Within the Write() implementation subclasses will write (serialize)
    metrics data to RSON format using an instance of RsonWriter.

    Game code must then be "instrumented" with calls to
    rlTelemetry::Write() to record metrics data.

    Example:

    class PlayerCoords : public rlMetric
    {
        //This macro must be included in rlMetric subclass
        //declarations.  The argument is the name of the metric.
        //The name will be serialized to the RSON string along with
        //the time the metric was recorded and the metric data.

        RL_DECLARE_METRIC(COORDS);

    public:

        PlayerCoords()
        {
            //Player coordinates are saved when the
            //metric is instantiated.

            const Vector3 coords = CGameWorld::FindLocalPlayerCoors();
            m_X = (int) coords.x;
            m_Y = (int) coords.y;
        }

        bool Write(RsonWriter* rw) const
        {
            //Player coordinates are written in RSON format when
            //the metric is serialized, which occurs sometime
            //after it's instantiated.

            //Sub-classes need not serialize the metric name or
            //the time it was recorded.  These are handled by the
            //rlTemetry class.

            return rw->WriteInt("x", m_X)
                    && rw->WriteInt("y", m_Y);
        }

    private:

        int m_X;
        int m_Y;
    };

    //RecordPlayerCoords is an example of a function that would be
    //implemented by the game to simplify recording a metric.
    //Game code would then be sprinkled with calls to RecordPlayerCoords().

    void RecordPlayerCoords()
    {
        //Instantiate the metric.
        PlayerCoords coords;

        //Record the metric.
        //For each metric recorded the telemetry manager records the
        //local system time.  When the metric is later serialized
        //to RSON format the time will be included in the RSON string.
        rlTelemetry::Write(coords);
    }

    When the metrics buffer fills up rlTelemetry::Flush() will be called,
    which flushes the buffer and writes the resulting RSON string to
    persistent storage.

    If Flush() is called too frequently warning messages will be generated
    and submissions will be throttled, causing loss of data.  If this starts
    to occur either reduce the number of metrics being recorded or increase the
    size of the internal telemetry buffer.

    See the bottom of this file for the standard metrics that all titles
    should be submitting.

    TELEMETRY IS LOSSY!!

    Because metrics data are buffered and only sent periodically it's possible
    to lose data if the machine is powered down before a buffer has been sent.

    When using the stream feature, however, (see rlTelemetryStream below) metrics
    are not buffered and loss is minimized.  It is recommended that rlTelemetryStream
    only be used during development and debugging.
*/

#if !__FINAL

//PURPOSE
//  Control and metrics for telemetry bandwidth.
class rlTelemetryBandwidth
{
public:
	//Represents a type of metric record.
	struct sRecord
	{
	public:
		u32 m_time;
		u32 m_produced;
		u32 m_consumed;
		u32 m_bytesSent;

		sRecord();

		void  Clear( ) 
		{
			m_time = 0;
			m_produced = 0;
			m_consumed = 0;
			m_bytesSent = 0;
		}
	};

public:
	enum eRecordTypes {NORMAL_METRICS, DEBUG_METRICS, IMMEDIATE_METRICS};
	static const u32 MAX_NUM_RECORDS = 3;
	atFixedArray< sRecord, MAX_NUM_RECORDS > m_records;
	atFixedArray< sRecord, MAX_NUM_RECORDS > m_totalHistory;

	rlTelemetryBandwidth( );
	~rlTelemetryBandwidth( );

	void AddRecord(const eRecordTypes record, const u32 produced, const u32 consumed);
	void SetRecordBytesSent(const eRecordTypes record, const u32 bytes);

	const char* GetRecordName(const eRecordTypes record) const;
};

//PURPOSE
//  Used to set the debug behavior of a metric or metric group (forced on/off or normal)
enum class rlMetricDebugUsage : u8
{
    DEFAULT,  // nothing to do
    FORCE_ON, // all metrics in this group are forced on
    FORCE_OFF // all metrics in this group are forced off
};

#endif //!__FINAL

#define TELEMETRY_EVENT_COMMON_DECL( name )\
	static unsigned EVENT_ID() { return name::GetAutoId(); }\
	virtual unsigned GetId() const { return name::GetAutoId(); }

#define TELEMETRY_EVENT_DECL( name, id )\
	AUTOID_DECL_ID( name, rage::rlTelemetryEvent, id )\
	TELEMETRY_EVENT_COMMON_DECL( name )

enum rlTelemetryEvents
{
	TELEMETRY_EVENT_FLUSH_END = 0,
	TELEMETRY_EVENT_DEFERRED_FLUSH,
	TELEMETRY_EVENT_CONFIG_CHANGED
};

//PURPOSE
//  Base class for all telemetry events
class rlTelemetryEvent
{
public:
	AUTOID_DECL_ROOT(rlTelemetryEvent);
	TELEMETRY_EVENT_COMMON_DECL(rlTelemetryEvent);

	rlTelemetryEvent() {}
	virtual ~rlTelemetryEvent() {}
};

//PURPOSE
//  Triggered to report a deferred flush
class rlTelemetryEventDeferredFlush : public rlTelemetryEvent
{
public:
	TELEMETRY_EVENT_DECL(rlTelemetryEventDeferredFlush, TELEMETRY_EVENT_DEFERRED_FLUSH);
};

//PURPOSE
//  Triggered to report the result of a flush
class rlTelemetryEventFlushEnd : public rlTelemetryEvent
{
public:
	TELEMETRY_EVENT_DECL(rlTelemetryEventFlushEnd, TELEMETRY_EVENT_FLUSH_END);

	rlTelemetryEventFlushEnd(const netStatus* status);

public:
	const netStatus* m_Status;
};

//PURPOSE
//  Triggered to report a new config, also triggered on a failed download
class rlTelemetryEventConfigChanged : public rlTelemetryEvent
{
public:
	TELEMETRY_EVENT_DECL(rlTelemetryEventConfigChanged, TELEMETRY_EVENT_CONFIG_CHANGED);

	rlTelemetryEventConfigChanged(const int version);

public:
	int m_Version;
};

//PURPOSE
// How and when to apply sampling. Only relevant if sampling is enabled for the metric
enum rlMetricSamplingMode
{
	// Sampling done internally in ShouldSendMetric, individually for each metric. This is the default setting
	METRIC_SAMPLE_EACH = 0,

	// Sampling done internally in ShouldSendMetric, but the outcome is done once per metric type, after downloading the config
	METRIC_SAMPLE_ONCE,

	// Sampling done with the probability value from the config, but manually by the calling code
	METRIC_SAMPLE_MANUAL
};

// PURPOSE
// Contains configuration information for a specific metric
class rlMetricConfig
{
public:
	rlMetricConfig()
		: m_Probability(1.0f)
		, m_SampledValue(0.0f)
		, m_IntervalMs(0)
		, m_IsDisabled(false)
		NOTFINAL_ONLY(, m_DebugUsage(rlMetricDebugUsage::DEFAULT))
	{}

	rlMetricConfig(const bool enabled, const float probability, const float sampledValue, const u32 intervalMs)
		: m_Probability(probability)
		, m_SampledValue(sampledValue)
		, m_IntervalMs(intervalMs)
		, m_IsDisabled(!enabled)
		NOTFINAL_ONLY(, m_DebugUsage(rlMetricDebugUsage::DEFAULT))
	{}

#if !RSG_FINAL
	rlMetricConfig(const rlMetricDebugUsage debugUsage)
		: m_Probability(1.0f)
		, m_SampledValue(0.0f)
		, m_IntervalMs(0)
		, m_IsDisabled(debugUsage != rlMetricDebugUsage::FORCE_ON)
		, m_DebugUsage(debugUsage)
	{}
#endif //!RSG_FINAL

	~rlMetricConfig()
	{}

	bool IsDisabled() const
	{
		// The debug option is ignored here. That's checked separately.
		return m_IsDisabled;
	}

	bool IsEnabled() const
	{
		return !IsDisabled();
	}

	float GetProbability() const
	{
		return m_Probability;
	}

	float GetSampledValue() const
	{
		return m_SampledValue;
	}

	u32 GetSendIntervalMs() const
	{
		return m_IntervalMs;
	}

	void SetEnabled(const bool enabled)
	{
		m_IsDisabled = !enabled;
	}

#if !RSG_FINAL
	rlMetricDebugUsage GetDebugUsage() const
	{
		return m_DebugUsage;
	}

	void SetDebugUsage(const rlMetricDebugUsage usage)
	{
		m_DebugUsage = usage;
	}
#endif //!RSG_FINAL

	// Exclude m_SampledValue on the check, that's intentional
	bool operator==(const rlMetricConfig& other) const { return m_IsDisabled == other.m_IsDisabled && m_Probability == other.m_Probability && m_IntervalMs == other.m_IntervalMs; }

private:
	float m_Probability; // The sampling probability (0.0 == 0%, 1.0 == 100%)
	float m_SampledValue; // the sampling value used only by METRIC_SAMPLE_ONCE metrics
	u32 m_IntervalMs;
	bool m_IsDisabled;
	NOTFINAL_ONLY(rlMetricDebugUsage m_DebugUsage);
};

// PURPOSE
// Contains configuration information for the metrics
class rlMetricsConfig
{
public:
	friend class rlTelemetryPolicies;

	rlMetricsConfig();
	~rlMetricsConfig();

	const rlMetricConfig* GetMetricConfig(atHashString hMetric) const;

	void Reset();

	bool operator==(const rlMetricsConfig& other) const;

private:
	atBinaryMap<rlMetricConfig, atHashString> m_MetricConfigs;
};

#define METRIC_DEFAULT_ON true
#define METRIC_DEFAULT_OFF false

//Everything is off for now, as it should be. Any exceptions will directly use METRIC_DEFAULT_ON
#define METRIC_DEFAULT_IMPORTANT METRIC_DEFAULT_OFF

//PURPOSE
//  Used to keep track of the recently download telemetry configs
//  This helps us avoiding to download configs in short interval in case there's a 
//  mismatch between GetTelemetryClientConfig and SubmitMetric
class rlTelemetryConfigVersionHistory
{
public:
	rlTelemetryConfigVersionHistory() {}

	//PURPOSE
	//  Add a new config version. Will overwrite the oldest one in case the list is full.
	void Insert(const int version);

	//PURPOSE
	//  Returns the download time of that version. If not found it will return 0
	u64 GetDownloadTime(const int version) const;

	//PURPSOE
	//  Returns true if Insert has been called within timeIntervalSeconds seconds for this version
	bool IsRecent(const int version, const u64 timeIntervalSeconds) const;

	//PURPOSE
	//  Compare two rlTelemetryConfigVersionHistory's
	bool operator==(const rlTelemetryConfigVersionHistory& other) const;

protected:
	static const unsigned MAX_VERSIONS = 4;

	struct rlTelemetryConfigVersion
	{
		rlTelemetryConfigVersion() : m_Version(0), m_DownloadPosixTime(0) {}

		int m_Version;
		u64 m_DownloadPosixTime;
	};

	rlTelemetryConfigVersion m_Versions[MAX_VERSIONS];
};

#if RL_USE_TELEMETRY_CLIENT_CONFIG
//PURPOSE
//  Manages the client config file. That file contains the info about which
//  metrics are enabled
class rlTelemetryClientConfig
{
public:
	rlTelemetryClientConfig();

	void Init();
	void Shutdown();

	bool IsInitialized() const;

	// Gets called when the telemetry file has changed on the server
	void TelemetryVersionChanged(const int newVersionNumber);

	// Updates the telemetry config which needs to be downloaded from the server
	void Update();

	// Attempts a new download if needed
	void Refresh();

protected:
	static const u32 TELEMETRY_CONFIG_MIN_RETRY_TIME_SEC = 10; // If there's a non fatal error we retry after a while. This is the min retry time.
	static const u32 TELEMETRY_CONFIG_MAX_RETRY_TIME_SEC = 15; // If there's a non fatal error we retry after a while. This is the max retry time.
	static const u32 TELEMETRY_CONFIG_MAX_RETRY_SCALE_RANGE = 200; // The range we scale the retry time with (that's 200% of the range of each iteration)
	static const u32 TELEMETRY_CONFIG_MAX_RETRY = 3; // How often we retry

	enum class TelemetryConfigState
	{
		cNone,            // Not initialized
		cInit,            // We're ready for our first download
		cDownloading,     // We're downloading the config
		cDownloadingFile, // We'Re downloading the cloud file referenced in the config
		cOutdated,        // This means we have a config but it's old. It can still be used while old.
		cReady,           // The config is valid and ready to use
		cFailed,          // There has been an error while downloading. We may retry
		cFailedNoRetry    // There has been an error while downloading but we won't start the retry timer as it's an error where it won't help. Note:we may still retry if the network state changes.
	};

	void Cancel();

	// Processes the current status of the download operation(s)
	void UpdateTelemetryClientConfigDownload();

	// Process the cloud file download
	void UpdateCloudFileDownload();

	// Starts the download of the config file
	void DownloadTelemetryClientConfig();

	// Sets the state and correlated flags
	void SetTelemetryClientConfigState(const TelemetryConfigState value);

	// Resets the retry timer to its default interval and stops it.
	void ResetRetryTimer();

	// Starts the retry timer if all conditions are met
	void CheckForRetry();

	// Returns true if there's no point in retrying the download
	static bool IsSevereClientConfigError(const int resultCode);

protected:
	datGrowBuffer m_ConfigFileBuffer;
	TelemetryConfigState m_TelemetryConfigState; // The status of the config download
	netStatus m_DownloadTelemetryConfigStatus; // The netStatus used during the download
	netStatus m_DownloadFileStatus; // The netStatus used during the download of the file
	netRetryTimer m_TelemetryConfigRetryTime; // The retry timer
	unsigned m_TelemetryConfigRetryCount; // The number of retries
	rlTelemetryConfigVersionHistory m_RequestedTelemetryVersions; // The telemetry re-download request versions.
	int m_ExpectedTelemetryVersion; // The telemetry version number we want to download after a retry.
	bool m_TelemetryDownloadTried; // True if we called DownloadTelemetryClientConfig and it failed or succeeded. Note: if it failed it may still succeed later on
};
#endif //RL_USE_TELEMETRY_CLIENT_CONFIG

//PURPOSE
//  Policies to control telemetry behavior.
class rlTelemetryPolicies
{
public:
    friend class rlRosGetTelemetryClientConfigTask;

    enum
    {
        //DON'T CHANGE THIS!!!
        MINIMUM_SUBMISSION_INTERVAL_SECONDS = 3 * 60,

        DEFAULT_SUBMISSION_INTERVAL_SECONDS = MINIMUM_SUBMISSION_INTERVAL_SECONDS,

        MINIMUM_SUBMSSION_TIMEOUT_SECONDS   = 20,
        DEFAULT_SUBMSSION_TIMEOUT_SECONDS   = MINIMUM_SUBMSSION_TIMEOUT_SECONDS,

		DEFAULT_LOG_CHANNEL					= 0,
		DEFAULT_RAGE_LOG_CHANNEL			= 10,

#if __FINAL
		DEFAULT_LOG_LEVEL                   = 0,
#else
		DEFAULT_LOG_LEVEL                   = 9,
#endif

		MAX_NUM_LOG_CHANNELS				= 11,

        CONFIG_PARSING_ERROR                = -2,
        MAX_METRIC_CONFIG_CLOUD_FILE_NAME   = 255,
        MIN_REDOWNLOAD_SAME_CONFIG_INTERVAL_SECONDS	= 900 // How long we wait before we re-download a config version which is in our history.

    };

	//Represents log level of a channel.
	struct rlTelemetryLogChannel
	{
#if !__FINAL
		rlTelemetryLogChannel() : m_Usage(rlMetricDebugUsage::DEFAULT) {}
		
		void Set(const rlMetricDebugUsage usage) { m_Usage = usage; }
		bool operator==(const rlTelemetryLogChannel& other) const { return m_Id == other.m_Id && m_Usage == other.m_Usage; }
		
		atHashString m_Id;
		rlMetricDebugUsage m_Usage;
#else
		bool operator==(const rlTelemetryLogChannel& other) const { return m_Id == other.m_Id; }
		atHashString m_Id;
#endif
	};

	rlTelemetryPolicies();
	~rlTelemetryPolicies() { Clear(); }

	//PURPOSE
	//  Resets the config to a clean state. Applies debug and default settings.
	void ResetMetricsToDefaults();

	//PURPOSE
	//  Turn telemetry off (with the exception of debug options)
	void TurnTelemetryOff();

	//PURPOSE
	//  Loads the options from a json string. Assumes it to be 0-terminated.
	bool LoadMetricsFromJson(const char* json, const unsigned jsonStringLength);

#if !RSG_FINAL
	//PURPOSE
	//  Loads the options from a json file
	bool LoadFromJsonFile(const char* path);
#endif

	OUTPUT_ONLY(void DumpSettings() const);

	const rlMetricConfig* GetMetricConfig(atHashString hMetric) const 
	{
		return m_MetricsConfig.GetMetricConfig(hMetric);
	}

	//PURPOSE
	//  The path of the file to download in case the metrics are not embedded in the Telemetry.asmx/GetTelemetryClientConfig response
	const char* GetCloudFilePath() const { return m_CloudFile; }

	//PURPOSE
	//  Global boolean. If false all metrics are off. If true the metrics-specific options apply.
	bool IsTelemetryEnabled() const { return m_IsTelemetryEnabled; }

	//PURPOSE
	//  Returns the currently downloaded config version. 0 is invalid.
	int GetTelemetryVersion() const { return m_TelemetryVersion; }

	//PURPOSE
	//  Returns the version history of the recently downloaded versions.
	const rlTelemetryConfigVersionHistory& GetVersionHistory() const { return m_ConfigVersionHistory; }

    bool operator==(const rlTelemetryPolicies& that) const;

    bool operator!=(const rlTelemetryPolicies& that) const
    {
        return !operator==(that);
    }
	void Shutdown()
	{
		Clear();
	}

protected:
	//PURPOSE
	//  Clears everything
	void Clear();

	//PURPOSE
	//  Clears the metrics settings. Does not apply defaults or debug settings
	void ClearMetrics();

	//PURPOSE
	//  Applies the defaults
	void ApplyDefaultMetrics();

#if !RSG_FINAL
	//PURPOSE
	//  Reads metrics on/off settings from the args
	void ApplyDebugMetrics();

	//PURPOSE
	//  goes through the debug list and turns the metrics on/off
	void ApplyDebugMetrics(const char** list, const int num, bool on);

	//PURPOSE
	//  goes through the debug list and turns the metrics on/off by the specified groups/channels
	void ApplyDebugMetricsChannel(const char** list, const int num, bool on);
#endif //!RSG_FINAL

public:
    //Maximum number of seconds that may elapse between submissions.
    //A value of zero indicates that no submissions will be made.
    //Even if zero metrics will still be written to registered streams.
    //The actual submission interval might be shorter if the metrics
    //buffers fill up too quickly.
    unsigned m_SubmissionIntervalSeconds;

    //Timeout in seconds when submitting to the metrics server.
    unsigned m_SubmissionTimeoutSeconds;

	rlTelemetryLogChannel m_Channels[MAX_NUM_LOG_CHANNELS];

	// Uest to track when ALL is used in the -nettelemetryforce arg
	NOTFINAL_ONLY(rlMetricDebugUsage m_AllDebugUsage;)

	//PURPOSE
	//	Called to produce a game specific chunk of info in the telemetry submission header.
	//  If bound, a "game" object is included in the header, created by this callback function.
	//PARAMS
	//	writer		- RsonWriter being use to write the current set of telemetry data.
	atDelegate<bool (RsonWriter*)> GameHeaderInfoCallback;

private:
	// Holds config info for metrics retrieved using metric id
	rlMetricsConfig m_MetricsConfig;

	// These are the default settings used when the metric is not part of the server-returned config file
	rlMetricsConfig m_MetricConfigDefaults;

	// The config version history. Keeps track of the recently downloaded versions.
	rlTelemetryConfigVersionHistory m_ConfigVersionHistory;

	// The file from which we retrieve the data
	char m_CloudFile[MAX_METRIC_CONFIG_CLOUD_FILE_NAME];

	// The currently downloaded telemetry version
	int m_TelemetryVersion;

	// If false no metric will be sent to the server
	bool m_IsTelemetryEnabled;
};

//PURPOSE
//  By default the internal metrics buffer is periodically flushed and sent
//  to a server which records the submission in a database.  Optionally,
//  instances of fiStream can be registered with rlTelemetry that can be used to
//  record metrics elsewhere.  For example, one stream could record metrics to
//  a file, while another could record them to a different server.
//
//  For example, the following opens a connection to a server:
//
//      rlTelemetryStream telStrm;
//      telStrm.Stream = fiStream::Open("tcpip:4444:10.0.19.203");
//
//  Note that metrics written to instances of fiStream are not buffered by
//  rlTelemetry.  They are written to the stream when rlTelemetry::Write()
//  is called.  By contrast metrics sent to the default telemetry server
//  are buffered and only sent periodically.
//
//  It is recommended that rlTelemetryStream only be used during development
//  and debugging.
class rlTelemetryStream
{
    friend class rlTelemetry;

public:

    rlTelemetryStream();

    ~rlTelemetryStream();

    //A stream created by the application for writing metrics
    //to alternate storage.
    fiStream* Stream;

private:

    inlist_node<rlTelemetryStream> m_ListLink;
};

typedef u8 rlTelemetryChannel;

//Combined with variadic macros this helps simulate default macro arguments.
//We need the DUMMY argument so we can use the ## (token pasting) with
//__VA_ARGS__ to handle empty macro argument lists.  This seems to be an
//issue only on the MS x86 compiler.
template<int DUMMY,  rlTelemetryChannel LogChannel = rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, int LogLevel = rlTelemetryPolicies::DEFAULT_LOG_LEVEL, bool DefaultOn = METRIC_DEFAULT_OFF, rlMetricSamplingMode SamplingMode = METRIC_SAMPLE_EACH >
struct rlMetricDeclHelper
{
	static const int LOG_CHANNEL = LogChannel;
    static const int LOG_LEVEL  = LogLevel;
    static const bool DEFAULT_TO_ON = DefaultOn;
	static const rlMetricSamplingMode SAMPLING_MODE = SamplingMode;
};

//PURPOSE:
//  Must be included in all rlMetric subclass declarations.
//PARAMS
//  name        - Name of the metric.  Should be unique and compact.
//  channel		- Optional channel.  partitions metrics to different channels.
//  loglevel    - Optional log level.  Metric will be recorded when the
//                global telemetry log level is greater than or equal to
//                this value.  The default value is zero.
//NOTES
//  Channel ID and log level are optional, but in order to
//  specify log level channel ID must also be specified.
#define RL_DECLARE_METRIC(name, ...)\
    friend class rage::rlTelemetry;\
	public:\
	virtual int GetMetricLogChannel() const override {return rlMetricDeclHelper<0, ##__VA_ARGS__>::LOG_CHANNEL;}\
    virtual int GetMetricLogLevel() const override {return rlMetricDeclHelper<0, ##__VA_ARGS__>::LOG_LEVEL;}\
    virtual bool IsDisabledByDefault() const override {return !rlMetricDeclHelper<0, ##__VA_ARGS__>::DEFAULT_TO_ON;}\
	virtual rlMetricSamplingMode GetSamplingMode() const override {return rlMetricDeclHelper<0, ##__VA_ARGS__>::SAMPLING_MODE;}\
    virtual const char* GetMetricName() const {return #name;}\
	virtual size_t GetMetricSize() const {return sizeof(*this);}\
	virtual u32 GetMetricId() const { static u32 s_Id = atStringHash(#name); return s_Id; }\
	static u32 MetricId() { static u32 s_Id = atStringHash(#name); return s_Id; }\
	static int LogChannel() { return rlMetricDeclHelper<0, ##__VA_ARGS__>::LOG_CHANNEL; }\
	static rlMetricSamplingMode SamplingMode() { return rlMetricDeclHelper<0, ##__VA_ARGS__>::SAMPLING_MODE; }

//PURPOSE
//  Base class for all metrics.
//  Subclasses must provide an implementation for Write().
//  The signature for Write() is:
//
//      bool Write(RsonWriter* rw);
//
//  The Write() method serializes a metric instance to an RsonWriter.
/*

    Example:

    class PlayerCoords : public rlMetric
    {
        //This macro must be included in rlMetric subclass
        //declarations.  The arguments are the name of the metric,
        //the channel ID, and the log level.
        //Channel ID and log level are optional, but in order to
        //specify log level channel ID must also be specified.
        //The name will be serialized to the RSON string along with
        //the time the metric was recorded and the metric data.

        RL_DECLARE_METRIC(COORDS, 2, 0);

    public:

        PlayerCoords()
        {
            //Player coordinates are saved when the
            //metric is instantiated.

            const Vector3 coords = CGameWorld::FindLocalPlayerCoors();
            m_X = (int) coords.x;
            m_Y = (int) coords.y;
        }

        bool Write(RsonWriter* rw) const
        {
            //Player coordinates are written in RSON format when
            //the metric is serialized, which occurs sometime
            //after it's instantiated.

            //The name of the metric and the time it was recorded
            //are implicitly serialized.

            return rw->WriteInt("x", m_X)
                    && rw->WriteInt("y", m_Y);
        }

    private:

        int m_X;
        int m_Y;
    };
*/
class rlMetric
{
    friend class rlTelemetry;
    friend class rlMetricList;
    friend class rlRosTelemetrySubmissionTask;
    friend class rlRosTelemetryImmediateSubmissionTask;
public:

    rlMetric();

    virtual ~rlMetric();

    //PURPOSE
    //  Returns the log channel ID for this metric
	virtual int GetMetricLogChannel() const;

    //PURPOSE
    //  Returns the log level for this metric
    virtual int GetMetricLogLevel() const;

	//PURPOSE
	//  How and when to apply sampling
	virtual rlMetricSamplingMode GetSamplingMode() const;

    //PURPOSE
    //  Returns the name of the metric.
    //  Implemented in the RL_DECLARE_METRIC macro.
    virtual const char* GetMetricName() const;

    //PURPOSE
    //  Should be implemented by subclasses to serialize metric data.
    virtual bool Write(RsonWriter* rw) const;

    //PURPOSE
    //  Returns the time the metric was recorded.
    u64 GetMetricTime() const;

	rlMetric* GetNext() { return m_Next; }
	const rlMetric* GetConstNext() const { return m_Next; }

	//PURPOSE
	//  Returns the size of the metric subclass.
	//  Implemented in the RL_DECLARE_METRIC macro.
	virtual size_t GetMetricSize() const;

	void SetTime(u64 posix) { m_PosixTime = posix; }

	//PURPOSE
	//  Returns the id of the metric subclass.
	//  Implemented in the RL_DECLARE_METRIC macro.
	virtual u32 GetMetricId() const;
	
	//PURPOSE
	//  Returns the id of the metric subclass.
	//  Implemented in the RL_DECLARE_METRIC macro.
	static u32 MetricId();

	//PURPOSE
	//  Returns true if the metric should not be recorded and 
	//  false otherwise
	bool IsDisabled() const;

	// PURPOSE
	//	Returns true if the sampling value is valid for this metric. The SampleProbability 
	//  comes from an rlMetricConfig, and defaults to 1.0f (send always). If there is
	//  no rlMetricConfig set up for this metric, true is returned by default.
	bool ShouldIncludeInSample() const;

	//PURPOSE
	//  Returns interval in milliseconds at which the metric will send for metrics not 
	// triggered by a specific event
	u32 GetSendIntervalMs() const;

	// PURPOSE
	//	Returns a metric config for this metric if one is specified in the 
	//  current policies metric config
	const rlMetricConfig* GetMetricConfig() const;

protected:
	//PURPOSE
	//  Returns true if the metric should not be recorded by default
	//  and false otherwise
	virtual bool IsDisabledByDefault() const;

	static bool IsDisabled(const bool disabledByDefault, const int logChannel, const rlMetricConfig* metricConfig);
	static bool ShouldIncludeInSample(const rlMetricSamplingMode samplingMode, const int logChannel, const rlMetricConfig* metricConfig);

	//PURPOSE
	//  Used to know wether the metric has been forced on or off
	//PARAMS
	//  result - If GetDebugDisabledFlag returns false then "result" will maintain the same value as it had.
	//           If GetDebugDisabledFlag returns true then "result" will tell you if the metric is forced off or on
	NOTFINAL_ONLY(static bool GetDebugDisabledFlag(const rlMetricConfig* config, int logChannel, bool& result));

private:

	//Time in seconds since 1/1/1970 UTC at which metric was recorded.
    u64 m_PosixTime;

    //Next record on the GamerSlot queue.
    rlMetric* m_Next;
};

class rlMetricList
{
public:

    rlMetricList();

    ~rlMetricList();

    void Clear();

    void Append(rlMetric* metric);

    rlMetric* Pop();

    const rlMetric* GetFirst() const;

    unsigned GetCount() const;

private:

    rlMetric* m_Head;
    rlMetric* m_Tail;

    unsigned m_Count;
};

//Metrics submitted from a non-main thread get queued up to be written on the main thread.
class rlQueuedMetric
{
public:
	rlQueuedMetric(const int localGamerIndex, const rlMetric* metric)
		: m_LocalGamerIndex(localGamerIndex)
		, m_Metric(metric)
	{
	}

	inlist_node<rlQueuedMetric> m_ListLink;
	const rlMetric* m_Metric;
	const int m_LocalGamerIndex;
};

//List of telemetry metrics queued up to be written on the main thread.
class rlQueuedMetricList : public inlist<rlQueuedMetric, &rlQueuedMetric::m_ListLink>
{
public:
	static const unsigned MAX_QUEUED_TELEMETRY_METRICS = 64;
};

//Struct for specifying the memory to use when submitting telemetry metrics as a separate submission.
struct rlTelemetrySubmissionMemoryPolicy
{
	rlTelemetrySubmissionMemoryPolicy() 
		: m_pExportWorkBuffer(NULL)
		, m_ExportWorkBufferLen(0)
		, m_pCompressionWorkBuffer(NULL)
		, m_CompressionWorkBufferLen(0)
	{

	}

	~rlTelemetrySubmissionMemoryPolicy()
	{
		if(m_pCompressionWorkBuffer != NULL || m_pExportWorkBuffer != NULL)
		{
			Quitf(ERR_DEFAULT,"Dangling pointers for rlTelemetrySubmissionMemoryPolicy");
		}
	}

	rlTelemetrySubmissionMemoryPolicy(unsigned size)
	{
		m_pExportWorkBuffer = (char*)RL_ALLOCATE(rlTelemetrySubmissionMemoryPolicy, size);
		m_ExportWorkBufferLen = size;
		m_pCompressionWorkBuffer = (char*)RL_ALLOCATE(rlTelemetrySubmissionMemoryPolicy, size);
		m_CompressionWorkBufferLen = size;
	}

	void ReleaseAndClear()
	{
		RL_FREE(m_pExportWorkBuffer);
		RL_FREE(m_pCompressionWorkBuffer);
		m_pCompressionWorkBuffer = m_pExportWorkBuffer = NULL;
		m_ExportWorkBufferLen = m_CompressionWorkBufferLen = 0;
	}

	char* m_pExportWorkBuffer;
	unsigned m_ExportWorkBufferLen;
	char* m_pCompressionWorkBuffer;
	unsigned m_CompressionWorkBufferLen;
};

class rlTelemetry
{
    friend class rlRosTelemetrySubmissionTask;
    friend class rlRosTelemetryImmediateSubmissionTask;
    friend class rlTelemetryClientConfig;
public:

	//PURPOSE
	// Delegator/delegate for telemetry events
	typedef atDelegator< void (const rlTelemetryEvent&) > Delegator;
	typedef Delegator::Delegate Delegate;

	static void Init(const bool useClientConfig);
	static void Shutdown();

    //PURPOSE
    //  Registers an instance of rlTelemetryStream.
    //NOTES
    //  This should only be used during development and debugging.
    //
    //  The rlTelemetryStream must not be destroyed as long as it
    //  is registered.
    static bool AddStream(rlTelemetryStream* stream);

    //PURPOSE
    //  Removes an instance of rlTelemetryStream.
    //NOTES
    //  This should only be used during development and debugging.
    static bool RemoveStream(rlTelemetryStream* stream);

	//PURPOSE
	//  Add or remove a delegate that will be called with event notifications.
	static void AddDelegate(Delegate* dlgt);
	static void RemoveDelegate(Delegate* dlgt);

    //PURPOSE
    //  Sets telemetry policies
    static void SetPolicies(const rlTelemetryPolicies& policies);

    //PURPOSE
    //  Returns current policies
    static const rlTelemetryPolicies& GetPolicies();

    //PURPOSE
    //  Set the flush interval
    static void SetSubmissionInterval(const unsigned intervalSec);


#if RL_USE_TELEMETRY_CLIENT_CONFIG
    //PURPOSE
    //  Gets the telemetry config from the server
    static bool RetrieveTelemetryClientConfig(const int localGamerIndex, netStatus* status);

    //PURPOSE
    //  Applies the config from an external json-string. Assumes it to be 0-terminated.
    static bool ApplyTelemetryClientConfig(const char* json, const unsigned jsonStringLength);
#endif

	//PURPOSE
	//  Get metric config for given metric
	static bool GetMetricConfig(atHashString hMetric, rlMetricConfig& metricConfig);

	//PURPOSE
	//  In the absence of a telemetry config on GTA V, this function
	//  decides whether to send probability-based telemetry, based
	//  on a random number generated once per call. 
	//  For example, if a metric has a send probability of 0.1, the 
	//  the metric is sent if the random roll is < 0.1.
	static bool ShouldSendTelemetry(const float probability);

	//PURPOSE
	//  True when the metric passes the disabled and sampling check
	//  Only ever call this once per metric, otherwise you'll invalidate
	//  the sampling logic.
	static bool ShouldSendMetric(const rlMetric& metric OUTPUT_ONLY(, const char* context));

	//PURPOSE
	// Must be called only for metrics set to METRIC_SAMPLE_ONCE or METRIC_SAMPLE_MANUAL. For METRIC_SAMPLE_EACH-metrics it would apply the sampling twice
	static bool ShouldSendMetric(const atHashString metricId, const int logChannel, const rlMetricSamplingMode samplingMode OUTPUT_ONLY(, const char* context));

	//PURPOSE
    //  Appends a metric instance to the buffer.  If the buffer
    //  fills up Flush() will be called.
    //PARAMS
    //  localGamerIndex - Index of the local gamer to whom the metric belongs
    //  metric          - The metric
    //RETURNS
    //  True on success.
    //NOTES
    //  If the log level of the channel to which the metric is assigned is
    //  less than the log level for the metric the metric will not be appended.
    static bool Write(const int localGamerIndex, const rlMetric& metric);

	//PURPOSE
    //  Appends a metric instance to the immediate buffer.  If the buffer
    //  fills up FlushImmediate() will be called.
    //  The immediate buffer is flushed a few seconds after writing to it,
    //  giving near real time response.
    //PARAMS
    //  localGamerIndex - Index of the local gamer to whom the metric belongs
    //  metric          - The metric
    //RETURNS
    //  True on success.
    //NOTES
    //  If the log level of the channel to which the metric is assigned is
    //  less than the log level for the metric the metric will not be appended.
    static bool WriteImmediate(const int localGamerIndex, const rlMetric& metric, const bool deferredTransaction=false);

    //PURPOSE
    //  Updates the telemetry system.  Should be called at least once per frame.
    static void UpdateClass();

    //PURPOSE
    //  Flushes the current telemetry buffer and writes it to the default
    //  telemetry server.
    //NOTES
    //  This should normally not be called by an application as it could
    //  result in overloading the server.
	static bool Flush();
	static void TryForceFlushImmediate();

    //PURPOSE
    //  Cancel all flushes that are in progress.
    static void CancelFlushes();

	//PURPOSE
	//  Return true if any metric exists to be flushed.
	static bool HasAnyMetrics();

	//PURPOSE
	//  Return true if flush is in progress
    //NOTES
    //  This doesn't consider that multiple gamers might or might not have
    //  flushes occurring.  If any single gamer has a flush in progress
    //  this will return true.
	static bool FlushInProgress();

	//PURPOSE
	//  Return true if all flush slots are in use
    //NOTES
    //  This doesn't consider that multiple gamers might or might not have
    //  flushes occurring.  If any single gamer has all flush slots in
    //  use this will return true.
	static bool AreAllPending();

	//PURPOSE
	//  Return the current telemetry buffer available size.
    static unsigned GetAvailableSize();

    //PURPOSE
    //  Current interval between automatic flushes
    static unsigned GetSubmissionIntervalSeconds();

    //PURPOSE
    //  If false no metrics will be sent
    static bool IsTelemetryEnabled();

	//PURPOSE:
	// Used to submit a list of rlMetrics to the server outside of the normal rlTelemetry buffering.  This is good for when we have large debug 
	//		metrics that we cant to submit as a separate submission so they won't interfere with the normal game telemetry
	//PARAMS:
	//	localGamerIndex - gamer index of the active local gamer
	//	pMetricList - the head of the LINKED LIST of rlMetric (note: rlMetric is set up for being a linked list)
	//	pMemoryPolicy - a struct that allows us to pass in the memory used to do the work.
	//NOTES:
	//	If you have a normal C++ array of rlMetric (or derived types), you can make them a linked list but just iterating and 
	//			updating their m_pNext pointers
	//	Regarding the size of the memory to pass in using pMemoryPolicy... the Export buffer needs to be big enough to hold the largest EXPORTED size of the
	//			metrics in your list.  The compressed buffer can be less than or equal to your export buffer size.  
	static bool	SubmitMetricList(const int localGamerIndex, const rlMetricList* pMetricList, const rlTelemetrySubmissionMemoryPolicy* pMemoryPolicy );

	//PURPOSE:
	// Used to Block Deferred Flush buffer.
	static void SetDeferredFlushBlocked(const bool value);

	//PURPOSE:
	// Check if the Deferred Flush buffer is currently blocked.
	static bool GetDeferredFlushBlocked();

	//PURPOSE:
	// When true metrics won't be immediately added to the list but queued instead
	// for later inspection.
	static void QueueMetrics(const bool queue);

	//PURPOSE:
	// Returns true while metrics are queued for a later flush
	static bool AreMetricsQueued() { return sm_QueueMetrics; }

#if __DEV
    //Submits a random telemetry event.  Used for testing.
    static void TestSubmitMetric(const int gamerIndex);
    static void TestSubmitImmediateMetric(const int gamerIndex);
#endif

#if !__FINAL
	static void AddBandwidthRecord(const rlTelemetryBandwidth::eRecordTypes record, const u32 produced, const u32 consumed) {sm_TelemetryBandwidth.AddRecord(record, produced, consumed);}
	static void SetAddBandwidthRecordBytesSent(const rlTelemetryBandwidth::eRecordTypes record, const u32 bytes) {sm_TelemetryBandwidth.SetRecordBytesSent(record, bytes);}
	static rlTelemetryBandwidth& GetBandwidthRecords() {return sm_TelemetryBandwidth;}
#endif // !__FINAL

    //Print out the current setting for the metric data
    OUTPUT_ONLY(static void DumpSettings());

	//PURPOSE
	//  Writes metric data to all registered streams.
	static void DoWriteToStreams(const int localGamerIndex,
									const u64 curTime, 
									rlGamerHandle& gamerHandle, 
									const rlMetric& metric);

	static void WriteLineToStream(const char* text);

	//PURPOSE
	//  Attempts to download the telemetry config again if needed
	static void RefreshTelemetryClientConfig();

private:

    typedef inlist<rlTelemetryStream, &rlTelemetryStream::m_ListLink> StreamList;

    //PURPOSE
    //  One slot per local gamer.  Holds metrics specific to that gamer.
    class GamerSlot
    {
    public:
        rlGamerHandle m_GamerHandle;
        rlRosCredentials m_Cred;    //Used for submitting metrics to ROS.
        rlMetricList m_Metrics;
        s64 m_CurSessionId;
        unsigned m_RtSequenceNumber;    //To prevent replays of real time submissions
                                        //** INCREMENTED ONLY WHEN CALLING FlushImmediate() **
        char m_GamerName[RL_MAX_NAME_BUF_SIZE];

        GamerSlot()
            : m_CurSessionId(0)
            , m_RtSequenceNumber(0)
        {
            this->Clear();
        }

        void Clear()
        {
            m_GamerHandle.Clear();
            m_Cred.Clear();
            m_Metrics.Clear();
            m_GamerName[0] = '\0';

            /*** DO NOT RESET m_CurSessionId or m_RtSequenceNumber ***/
        }
    };

    //PURPOSE
    //  Flushes the immediate buffer.
	static bool FlushImmediate();

	//PURPOSE
	//  Flushes the deferred buffer.
	static bool FlushDeferred();

    static bool WriteHeader(const u64 baseTime,
                            const GamerSlot* slot,
                            RsonWriter* rw);


    //PURPOSE
    //  Writes metric data to all registered streams and then queues the
    //  data for later submission to the ROS metrics server.
    static bool DoWrite(const int localGamerIndex,
                        const rlMetric& metric);

	//PURPOSE
	//  Writes metric data to the "immediate" buffer.  Adding metrics
	//  to the immediate buffer causes them to be submitted in near real time.
	static bool DoWriteImmediate(const int localGamerIndex,
								 const rlMetric& metric);

	//PURPOSE
	//  Writes metric data to the "deferred" buffer.  Adding metrics
	//  to the deferred buffer causes them to be submitted in near real time when its un-bloked.
	static bool DoWriteDeferred(const int localGamerIndex,
										const rlMetric& metric);

    //PURPOSE
    //  Writes metric data to a RSON string.
    static bool Export(RsonWriter* rw,
                        const char* name,
                        const u64 metricTime,
                        const rlMetric& metric);

	//PURPOSE
	//  Writes telemetry metrics that have been queued to send on the main thread.
	static void WriteQueuedTelemetryMetrics();

	//PURPOSE
	//  Clears the list of queued telemetry metrics.
	static void ClearQueuedTelemetryMetrics();

    static GamerSlot* GetSlot(const rlGamerHandle& gamerHandle);

	static GamerSlot* GetImmediateSlot(const rlGamerHandle& gamerHandle);
	static GamerSlot* GetDeferredSlot(const rlGamerHandle& gamerHandle);

    static void ClearSlots();
	static void ClearImmediateSlots();
	static void ClearDeferredSlots();
    static StreamList sm_Streams;

    static GamerSlot sm_GamerSlots[RL_MAX_LOCAL_GAMERS];
	static rlQueuedMetricList sm_QueuedMetrics;
	static sysCriticalSectionToken sm_QueuedMetricsCs;
#if RL_USE_TELEMETRY_CLIENT_CONFIG
	static rlTelemetryClientConfig sm_TelemetryClientConfig;
#endif

	// While true all metrics are queued till it becomes false or till
	// sm_QueuedMetrics has filled up
	static bool sm_QueueMetrics;

	//Events delegator
	static Delegator sm_Delegator;

	static GamerSlot sm_ImmediateGamerSlots[RL_MAX_LOCAL_GAMERS];
	static GamerSlot sm_DeferredGamerSlots[RL_MAX_LOCAL_GAMERS];

#if !__FINAL
	static rlTelemetryBandwidth sm_TelemetryBandwidth;
#endif
};

//PURPOSE
//  Convenience interface for encoding/decoding zlib streams.
class zlibStream
{
public:

    zlibStream();

    ~zlibStream();

    //PURPOSE
    //  Releases resources and sets the stream to initial values.
    void Clear();

    //PURPOSE
    //  Begins encoding.
    //PARAMS
    //  allocator   - Used to allocate memory for zlib.
    //NOTES
    //  If memory allocation fails using this allocator the global
    //  allocator will be tried.
    bool BeginEncode(sysMemAllocator* allocator);

    //PURPOSE
    //  Begins decoding.
    //PARAMS
	//  allocator   - Used to allocate memory for zlib.
    //  windowbits  - determines the window size. For instance 16+MAX_WBITS indicates a gzip header file.
    //NOTES
    //  If memory allocation fails using this allocator the global
    //  allocator will be tried.
    bool BeginDecode(sysMemAllocator* allocator, int windowbits = 0);

    //PURPOSE
    //  Encodes data into the current stream.
    //PARAMS
    //  src     - Source buffer.
    //  srcLen  - Number of bytes in the source buffer.
    //  dst     - Destination buffer.
    //  dstLen  - Number of bytes in the destination buffer.
    //  numConsumed - On return contains the number of source bytes consumed.
    //  numProduced - On return contains the number of destination bytes produced.
    //NOTES
    //  When the entire content buffer has been encoded continue calling
    //  Encode() with NULL src and zero srcLen until IsComplete() returns
    //  true, or HasError() returns true.
    void Encode(const void* src,
                const unsigned srcLen,
                void* dst,
                const unsigned dstLen,
                unsigned* numConsumed,
                unsigned* numProduced);

    //PURPOSE
    //  Decodes data from the current stream.
    //PARAMS
    //  src     - Source buffer.
    //  srcLen  - Number of bytes in the source buffer.
    //  dst     - Destination buffer.
    //  dstLen  - Number of bytes in the destination buffer.
    //  numConsumed - On return contains the number of source bytes consumed.
    //  numProduced - On return contains the number of destination bytes produced.
    //NOTES
    //  When the entire content buffer has been decoded continue calling
    //  Decode() with NULL src and zero srcLen until IsComplete() returns
    //  true, or HasError() returns true.
    void Decode(const void* src,
                const unsigned srcLen,
                void* dst,
                const unsigned dstLen,
                unsigned* numConsumed,
                unsigned* numProduced);

    //PURPOSE
    //  Returns the total number of bytes consumed by the current
    //  encode/decode operation.
    unsigned GetNumConsumed() const;

    //PURPOSE
    //  Returns the total number of bytes produced by the current
    //  encode/decode operation.
    unsigned GetNumProduced() const;

    //PURPOSE
    //  Returns true if currently encoding.
    bool IsEncoding() const;

    //PURPOSE
    //  Returns true if currently decoding.
    bool IsDecoding() const;

    //PURPOSE
    //  Returns true if the operation (encoding/decoding) completed
    //  successfully.
    bool IsComplete() const;

    //PURPOSE
    //  Returns true if the operation (encoding/decoding) caused an error.
    bool HasError() const;

    //PURPOSE
    //  Returns the zlib error code.
    int GetErrorCode() const;

private:

    enum StreamType
    {
        STREAMTYPE_INVALID  =   -1,
        STREAMTYPE_ENCODE,
        STREAMTYPE_DECODE
    };

    enum State
    {
        STATE_ERROR     = -1,
        STATE_INITIAL,
        STATE_ENCODING,
        STATE_DECODING,
        STATE_COMPLETE,
    };

    State m_State;
    sysMemAllocator* m_Allocator;
    StreamType m_StreamType;
    unsigned m_NumConsumed;
    unsigned m_NumProduced;
    int m_ZlibErrCode;
    z_stream m_ZlibStrm;
};

//Standard metrics that should be used for every title.
//Titles can subclass these to add additional info.
namespace rlStandardMetrics
{

//Submitted when a NAT-traversal completes on PC.
class NatTraversal : public rlMetric
{
	RL_DECLARE_METRIC(NAT, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3, METRIC_DEFAULT_OFF, METRIC_SAMPLE_MANUAL);

public:

	NatTraversal(bool succeeded,
				 unsigned failReason,
				 bool canceled,
				 unsigned numConcurrentSessions,
				 unsigned peakConcurrentSessions,
				 unsigned totalSessions,
				 unsigned localCandidateType,
				 unsigned remoteCandidateType,
				 char (&lpriv)[netSocketAddress::MAX_STRING_BUF_SIZE],
				 char (&lpub)[netSocketAddress::MAX_STRING_BUF_SIZE],
				 char (&lrly)[netSocketAddress::MAX_STRING_BUF_SIZE],
				 char (&rpriv)[netSocketAddress::MAX_STRING_BUF_SIZE],
				 char (&rpub)[netSocketAddress::MAX_STRING_BUF_SIZE],
				 char (&rrly)[netSocketAddress::MAX_STRING_BUF_SIZE],
				 unsigned elapsedTime,
				 bool connectingToSessionHost,
				 bool hasControllingRole,
				 bool hadRoleConflict,
				 bool relayAssisted,
				 unsigned directOfferStatus,
				 unsigned indirectOfferStatus,
				 unsigned symmetricSocketStatus,
				 unsigned numOffersSent,
				 unsigned numOffersReceived,
				 unsigned numAnswersSent,
				 unsigned numAnswersReceived,
				 unsigned numPingsSent,
				 unsigned numPingsReceived,
				 unsigned numPongsSent,
				 unsigned numPongsReceived,
				 u64 gameSessionToken,
				 u64 gameSessionId,
				 int localNatType,
				 int remoteNatType,
				 int unmatchedPublicIpState,
				 int relayRouteCheckState)
	: m_ElapsedTime(elapsedTime)
	, m_LocalCandidateType(localCandidateType)
	, m_RemoteCandidateType(remoteCandidateType)
	, m_Succeeded(succeeded)
	, m_FailReason(failReason)
	, m_Canceled(canceled)
	, m_NumConcurrentSessions(numConcurrentSessions)
	, m_PeakConcurrentSessions(peakConcurrentSessions)
	, m_TotalSessions(totalSessions)
	, m_RelayAssisted(relayAssisted)
	, m_ConnectingToSessionHost(connectingToSessionHost)
	, m_HadRoleConflict(hadRoleConflict)
	, m_HasControllingRole(hasControllingRole)
	, m_DirectOfferStatus(directOfferStatus)
	, m_IndirectOfferStatus(indirectOfferStatus)
	, m_SymmetricSocketStatus(symmetricSocketStatus)
	, m_NumOffersSent(numOffersSent)
	, m_NumOffersReceived(numOffersReceived)
	, m_NumAnswersSent(numAnswersSent)
	, m_NumAnswersReceived(numAnswersReceived)
	, m_NumPingsSent(numPingsSent)
	, m_NumPingsReceived(numPingsReceived)
	, m_NumPongsSent(numPongsSent)
	, m_NumPongsReceived(numPongsReceived)
	, m_GameSessionToken(gameSessionToken)
	, m_GameSessionId(gameSessionId)
	, m_LocalNatType(localNatType)
	, m_RemoteNatType(remoteNatType)
	, m_UnmatchedPublicIpState(unmatchedPublicIpState)
	, m_RelayRouteCheckState(relayRouteCheckState)
	{
		safecpy(m_Lpriv, lpriv);
		safecpy(m_Lpub, lpub);
		safecpy(m_Lrly, lrly);
		safecpy(m_Rpriv, rpriv);
		safecpy(m_Rpub, rpub);
		safecpy(m_Rrly, rrly);
	}

	bool Write(RsonWriter* rw) const
	{
		bool success = rw->WriteBool("S", m_Succeeded)
						&& rw->WriteUns("F", m_FailReason)
						&& rw->WriteUns("T", m_ElapsedTime)
						&& rw->WriteBool("R", m_RelayAssisted)
						&& rw->WriteBool("H", m_ConnectingToSessionHost)
						&& rw->WriteBool("C", m_HasControllingRole)
						&& rw->WriteBool("RC", m_HadRoleConflict)
						&& rw->WriteUns("D", m_DirectOfferStatus)
						&& rw->WriteUns("I", m_IndirectOfferStatus)
						&& rw->WriteUns("Y", m_SymmetricSocketStatus)
						&& rw->WriteUns("NOS", m_NumOffersSent)
						&& rw->WriteUns("NOR", m_NumOffersReceived)
						&& rw->WriteUns("NAS", m_NumAnswersSent)
						&& rw->WriteUns("NAR", m_NumAnswersReceived)
						&& rw->WriteUns("NPIS", m_NumPingsSent)
						&& rw->WriteUns("NPIR", m_NumPingsReceived)
						&& rw->WriteUns("NPOS", m_NumPongsSent)
						&& rw->WriteUns("NPOR", m_NumPongsReceived)
						// Vertica only supports signed 64-bit ints
						&& rw->WriteInt64("STOK", static_cast<s64>(m_GameSessionToken))
						&& rw->WriteInt64("SID", static_cast<s64>(m_GameSessionId))
						&& rw->WriteString("LPRIV", m_Lpriv)
						&& rw->WriteString("LPUB", m_Lpub)
						&& rw->WriteString("LRLY", m_Lrly)
						&& rw->WriteString("RPRIV", m_Rpriv)
						&& rw->WriteString("RPUB", m_Rpub)
						&& rw->WriteString("RRLY", m_Rrly)
						&& rw->WriteBool("CAN", m_Canceled)
						&& rw->WriteUns("NCS", m_NumConcurrentSessions)
						&& rw->WriteUns("NPS", m_PeakConcurrentSessions)
						&& rw->WriteUns("NTS", m_TotalSessions)
						&& rw->WriteUns("LN", m_LocalNatType)
						&& rw->WriteUns("RN", m_RemoteNatType)
						&& rw->WriteInt("U", m_UnmatchedPublicIpState)
						&& rw->WriteInt("RR", m_RelayRouteCheckState);

		if(success && m_Succeeded)
		{
			success = rw->WriteUns("LCT", m_LocalCandidateType)
						&& rw->WriteUns("RCT", (m_RemoteCandidateType));
		}

		return success;
	}

	char m_Lpriv[netSocketAddress::MAX_STRING_BUF_SIZE];
	char m_Lpub[netSocketAddress::MAX_STRING_BUF_SIZE];
	char m_Lrly[netSocketAddress::MAX_STRING_BUF_SIZE];

	char m_Rpriv[netSocketAddress::MAX_STRING_BUF_SIZE];
	char m_Rpub[netSocketAddress::MAX_STRING_BUF_SIZE];
	char m_Rrly[netSocketAddress::MAX_STRING_BUF_SIZE];

	unsigned m_ElapsedTime;
	unsigned m_LocalCandidateType;
	unsigned m_RemoteCandidateType;
	unsigned m_FailReason;
	unsigned m_NumConcurrentSessions;
	unsigned m_PeakConcurrentSessions;
	unsigned m_TotalSessions;
	unsigned m_DirectOfferStatus;
	unsigned m_IndirectOfferStatus;
	unsigned m_SymmetricSocketStatus;
	unsigned m_NumOffersSent;
	unsigned m_NumOffersReceived;
	unsigned m_NumAnswersSent;
	unsigned m_NumAnswersReceived;
	unsigned m_NumPingsSent;
	unsigned m_NumPingsReceived;
	unsigned m_NumPongsSent;
	unsigned m_NumPongsReceived;
	u64 m_GameSessionToken;
	u64 m_GameSessionId;
	int m_LocalNatType;
	int m_RemoteNatType;
	int m_UnmatchedPublicIpState;
	int m_RelayRouteCheckState;

	bool m_Succeeded : 1;
	bool m_RelayAssisted : 1;
	bool m_ConnectingToSessionHost : 1;
	bool m_HasControllingRole : 1;
	bool m_HadRoleConflict : 1;
	bool m_Canceled : 1;
};

//Submitted when a tunnel request completes
class TunnelerResult : public rlMetric
{
	RL_DECLARE_METRIC(TUNNELER, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);

public:

	TunnelerResult(int result,
				   bool relayAttempted,
				   bool relayAssisted,
				   bool initiator,
				   int keyExchangeResultCode, 
				   int directResultCode,
				   int relayResultCode,	
				   int tunnelReason,
				   u64 uuid,
				   unsigned elapsedTime,
				   char (&rip)[netSocketAddress::MAX_STRING_BUF_SIZE],
				   char (&rrly)[netSocketAddress::MAX_STRING_BUF_SIZE],
				   char(&lpub)[netSocketAddress::MAX_STRING_BUF_SIZE],
				   char(&lrly)[netSocketAddress::MAX_STRING_BUF_SIZE],
				   int presenceQueryState)
		: m_Result(result)
		, m_RelayAttempted(relayAttempted)
		, m_RelayAssisted(relayAssisted)
		, m_Initiator(initiator)
		, m_KeyExchangeResultCode(keyExchangeResultCode)
		, m_DirectResultCode(directResultCode)
		, m_RelayResultCode(relayResultCode)
		, m_TunnelReason(tunnelReason)
		, m_Uuid(uuid)
		, m_ElapsedTime(elapsedTime)
		, m_PresenceQueryState(presenceQueryState)
	{
		safecpy(m_Rip, rip);
		safecpy(m_Rrly, rrly);
		safecpy(m_Lpub, lpub);
		safecpy(m_Lrly, lrly);
	}

	bool Write(RsonWriter* rw) const
	{
		bool success = rw->WriteInt("RES", m_Result)
					&& rw->WriteUns("T", m_ElapsedTime)
					&& rw->WriteBool("I", m_Initiator)
					&& rw->WriteInt("C", m_KeyExchangeResultCode)
					&& rw->WriteInt("DR", m_DirectResultCode)
					&& rw->WriteInt("RR", m_RelayResultCode)
					&& rw->WriteInt("TR", m_TunnelReason)
					// Vertica only supports signed 64-bit ints
					&& rw->WriteInt64("UUID", static_cast<s64>(m_Uuid))
					&& rw->WriteBool("RA", m_RelayAttempted)
					&& rw->WriteBool("R", m_RelayAssisted)
					&& rw->WriteString("RIP", m_Rip)
					&& rw->WriteString("RRLY", m_Rrly)
					&& rw->WriteString("LPUB", m_Lpub)
					&& rw->WriteString("LRLY", m_Lrly)
					&& rw->WriteInt("PQ", m_PresenceQueryState);

		return success;
	}

	int m_Result;
	bool m_RelayAttempted : 1;
	bool m_RelayAssisted : 1;
	bool m_Initiator : 1;
	int m_KeyExchangeResultCode;
	int m_DirectResultCode;
	int m_RelayResultCode;	
	int m_TunnelReason;
	u64 m_Uuid;
	unsigned m_ElapsedTime;
	int m_PresenceQueryState;
	char m_Rip[netSocketAddress::MAX_STRING_BUF_SIZE];
	char m_Rrly[netSocketAddress::MAX_STRING_BUF_SIZE];
	char m_Lpub[netSocketAddress::MAX_STRING_BUF_SIZE];
	char m_Lrly[netSocketAddress::MAX_STRING_BUF_SIZE];
};

//Submitted when NAT-Detection completes. Contains info about the NAT.
class NatInfoMetric : public rlMetric
{
	RL_DECLARE_METRIC(NAT_INFO, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);

public:

	NatInfoMetric(const bool succeeded,
				 const netNatInfo& natInfo,
				 const netNatPcpInfo& natPcpInfo,
				 const netNatUpNpInfo& natUpNpInfo,
				 const int consoleNatType,
				 const int errorCode,
				 const u32 mtu,
				 const netHardwareReachability device)
	: m_Succeeded(succeeded)
	, m_ConsoleNatType(consoleNatType)
	, m_ErrorCode(errorCode)
	, m_Mtu(mtu)
	, m_Device(device)
	{
		m_NatInfo = natInfo;
		m_NatPcpInfo = natPcpInfo;
		m_NatUpNpInfo = natUpNpInfo;
	}

	bool Write(RsonWriter* rw) const
	{
		bool success = rw->WriteBool("S", m_Succeeded)
						&& rw->WriteBool("D", m_NatInfo.IsNatDetected())
						&& rw->WriteUns("NT", m_NatInfo.GetNatType())
						&& rw->WriteUns("NTC", m_ConsoleNatType)
						&& rw->WriteUns("PMM", m_NatInfo.GetPortMappingMethod())
						&& rw->WriteUns("FM", m_NatInfo.GetFilteringMode())
						&& rw->WriteUns("PAS", m_NatInfo.GetPortAllocationStrategy())
						&& rw->WriteUns("PI", m_NatInfo.GetPortIncrement())
						&& rw->WriteUns("UPNP", m_NatUpNpInfo.GetState())
						&& rw->WriteUns("UTS", m_NatInfo.GetUdpTimeoutState())
						&& rw->WriteUns("UTT", m_NatInfo.GetUdpTimeoutSec())
						&& rw->WriteUns("URM", m_NatUpNpInfo.GetReserveMethod())
						&& rw->WriteBool("UMN", m_NatUpNpInfo.GetExternalIpAddress().IsV4()
												&& m_NatUpNpInfo.GetExternalIpAddress().ToV4().IsRfc1918())
						&& rw->WriteBool("UAP", (m_NatUpNpInfo.GetState() == netNatUpNpInfo::NAT_UPNP_SUCCEEDED)
												&& (m_NatUpNpInfo.GetRequestedPort() != m_NatUpNpInfo.GetReservedPort()))
						&& rw->WriteUns("PCP", m_NatPcpInfo.GetState())
						&& rw->WriteUns("PRM", m_NatPcpInfo.GetReserveMethod())
						&& rw->WriteBool("PAP", (m_NatPcpInfo.GetState() == netNatPcpInfo::NAT_PCP_SUCCEEDED)
												&& (m_NatPcpInfo.GetRequestedAddress() != m_NatPcpInfo.GetReservedAddress()))
						&& rw->WriteString("UDF", m_NatUpNpInfo.GetDeviceFriendlyName())
						&& rw->WriteString("UDM", m_NatUpNpInfo.GetDeviceManufacturer())
						&& rw->WriteString("UDD", m_NatUpNpInfo.GetDeviceModelName())
						&& rw->WriteString("UDN", m_NatUpNpInfo.GetDeviceModelNumber())
						&& rw->WriteInt("ERR", m_ErrorCode)
						&& rw->WriteUns("MTU", m_Mtu)
						&& rw->WriteInt("DEV", static_cast<int>(m_Device));

		return success;
	}

	netNatInfo m_NatInfo;
	netNatPcpInfo m_NatPcpInfo;
	netNatUpNpInfo m_NatUpNpInfo;
	int m_ConsoleNatType;
	int m_ErrorCode;
	u32 m_Mtu;
	netHardwareReachability m_Device;
	bool m_Succeeded : 1;
};

#if NET_CXNMGR_COLLECT_STATS
//Submitted when a p2p endpoint is destroyed.
class P2pConnectionReport : public rlMetric
{
	RL_DECLARE_METRIC(P2PCXN, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);

public:

	P2pConnectionReport(netIpAddress& lpub,
						netIpAddress& lrly,
						netIpAddress& rpub,
						netIpAddress& rrly,
						const netEndpoint::Statistics& stats)
	: m_Lpub(lpub)
	, m_Lrly(lrly)
	, m_Rpub(rpub)
	, m_Rrly(rrly)
	, m_Stats(stats)
	{

	}

	bool Write(RsonWriter* rw) const
	{
		unsigned bytesSentPeerRelay = m_Stats.m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS] +
									  m_Stats.m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_3_HOPS] +
									  m_Stats.m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS];

		unsigned packetsSentPeerRelay = m_Stats.m_PacketsSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS] +
										m_Stats.m_PacketsSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_3_HOPS] +
										m_Stats.m_PacketsSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS];

		// Vertica only supports signed 64-bit ints
		bool success = rw->WriteInt64("ST", static_cast<s64>(m_Stats.m_CxnStartTime))
			&& rw->WriteInt64("ET", static_cast<s64>(m_Stats.m_CxnEndTime))
			&& rw->WriteUns("CD", m_Stats.m_CxnDuration)
			&& rw->WriteUns("OM", m_Stats.m_NumOutOfMemoryEvents)
			&& rw->WriteUns("FA", m_Stats.m_NumFailedAllocs)
			&& rw->WriteUns("TO", m_Stats.m_NumTimedOutEvents)
			&& rw->WriteUns("TR", m_Stats.m_TimedOutReason)
			&& rw->WriteUns("SE", m_Stats.m_NumSendErrors)
			&& rw->WriteUns("RT", m_Stats.m_Rtt.Get())
			&& rw->WriteBool("VR", m_Stats.m_RouteType == netEndpoint::Statistics::ROUTE_TYPE_RELAY_SERVER)
			&& rw->WriteUns("BD", m_Stats.m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_DIRECT])
			&& rw->WriteUns("BR", m_Stats.m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_RELAY_SERVER])
			&& rw->WriteUns("PD", m_Stats.m_PacketsSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_DIRECT])
			&& rw->WriteUns("PR", m_Stats.m_PacketsSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_RELAY_SERVER])
			&& rw->WriteUns("FS", m_Stats.m_FramesSent)
			&& rw->WriteUns("FR", m_Stats.m_FramesSentReliably)
			&& rw->WriteUns("FE", m_Stats.m_FramesResent)
			&& rw->WriteUns("DD", m_Stats.m_FramesDroppedDuplicate)
			&& rw->WriteUns("DL", m_Stats.m_FramesDroppedLateUnreliable)
			&& rw->WriteUns("DW", m_Stats.m_FramesDroppedWndOverflow)
			&& rw->WriteUns("RI", m_Stats.m_FramesRcvdInOrder)
			&& rw->WriteUns("RO", m_Stats.m_FramesRcvdOutOfOrder)
			&& rw->WriteUns("BU", m_Stats.m_BytesUncompressed)
			&& rw->WriteUns("BC", m_Stats.m_BytesCompressed)
			&& rw->WriteUser("LP", m_Lpub)
			&& rw->WriteUser("LR", m_Lrly)
			&& rw->WriteUser("RP", m_Rpub)
			&& rw->WriteUser("RR", m_Rrly)
			&& rw->WriteBool("VP", (m_Stats.m_RouteType == netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS) ||
									(m_Stats.m_RouteType == netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_3_HOPS) ||
									(m_Stats.m_RouteType == netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS))
			&& rw->WriteUns("BP", bytesSentPeerRelay)
			&& rw->WriteUns("PP", packetsSentPeerRelay)
			&& rw->WriteInt("RY", (int)m_Stats.m_RouteType)
			&& rw->WriteUns("EF", m_Stats.m_NumFailedEncryptions)
			&& rw->WriteUns("NE", m_Stats.m_NumNoEncryptionKey)
			&& rw->WriteUns("DF", m_Stats.m_NumFailedDecryptions)
			&& rw->WriteUns("DR", m_Stats.m_NumDirectRouteReassignments)
			&& rw->WriteUns("MR", m_Stats.m_MaxDeltaReceiveTime);

		return success;
	}

	netIpAddress m_Lpub;
	netIpAddress m_Lrly;
	netIpAddress m_Rpub;
	netIpAddress m_Rrly;

	const netEndpoint::Statistics m_Stats;
};
#endif


}   //namespace rlStandardMetrics

}   //namespace rage

#endif  //RLINE_RLTELEMETRY_H
