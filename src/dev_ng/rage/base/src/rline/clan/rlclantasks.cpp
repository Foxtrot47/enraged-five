// 
// rline/rlclantasks.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlclantasks.h"
#include "rlclancommon.h"
#include "rlclan.h"
#include "rlclanclient.h"
#include "rline/rlpresence.h"
#include "rline/cloud/rlcloud.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "system/timer.h"
#include "string/string.h"
#include "system/nelem.h"
#include "vector/color32.h"  //for clan color support

namespace rage
{

//////////////////////////////////////////////////////////////////////////
// ConvertRosResultToClanError
//////////////////////////////////////////////////////////////////////////
rlClanRosBaseTask::rlClanRosBaseTask()
{
}

rlClanRosBaseTask::~rlClanRosBaseTask()
{
}

int
rlClanRosBaseTask::ConvertRosResultToClanError(const rlRosResult& result)
{
    rtry
    {
        if (result.IsError("AlreadyExists", "ClanNameExists")) return RL_CLAN_ERR_NAME_EXISTS;
        if (result.IsError("AlreadyExists", "ClanTagExists")) return RL_CLAN_ERR_TAG_EXISTS;
        if (result.IsError("AlreadyExists", "AlreadyInClan")) return RL_CLAN_ERR_ALREADY_IN_CLAN;
        if (result.IsError("AlreadyExists", "ClanInviteExists")) return RL_CLAN_ERR_INVITE_EXISTS;
        if (result.IsError("AlreadyExists", "ClanRankOrderExists")) return RL_CLAN_ERR_RANK_ORDER_EXISTS;
        if (result.IsError("AlreadyExists", "ClanRankNameExists")) return RL_CLAN_ERR_RANK_NAME_EXISTS;
		if (result.IsError("AuthenticationFailed", "Ticket")) return RL_CLAN_ERR_INVALID_TICKET;
		if (result.IsError("AuthenticationFailed", "DupLogin")) return RL_CLAN_ERR_DUPLICATE_LOGIN;
        if (result.IsError("OutOfRange", "ClanPermissionError")) return RL_CLAN_ERR_PERMISSION_ERROR;
        if (result.IsError("OutOfRange", "ClanInviteMaxSentCountExceeded")) return RL_CLAN_ERR_INVITE_MAX_SENT_COUNT_EXCEEDED;
        if (result.IsError("OutOfRange", "ClanMaxJoinCountExceeded")) return RL_CLAN_ERR_CLAN_MAX_JOIN_COUNT_EXCEEDED;
        if (result.IsError("OutOfRange", "ClanMaxMemberCountExceeded")) return RL_CLAN_ERR_CLAN_MAX_MEMBER_COUNT_EXCEEDED;
        if (result.IsError("OutOfRange", "ClanRankChangeNotAllowed")) return RL_CLAN_ERR_RANK_CHANGE_NOT_ALLOWED;
        if (result.IsError("OutOfRange", "ClanNameLength")) return RL_CLAN_ERR_CLAN_NAME_LENGTH_INVALID;
        if (result.IsError("OutOfRange", "ClanPlayerBanned")) return RL_CLAN_ERR_PLAYER_BANNED;
        if (result.IsError("DoesNotExist", "ClankRankoDoesNotExist")) return RL_CLAN_ERR_RANK_NOT_FOUND;
        if (result.IsError("DoesNotExist", "ClanPlayerNotFound")) return RL_CLAN_ERR_PLAYER_NOT_FOUND;
        if (result.IsError("DoesNotExist", "ClanInviteDoesNotExist")) return RL_CLAN_ERR_INVITE_DOES_NOT_EXIST;
        if (result.IsError("DoesNotExist", "ClanRequiresInvite")) return RL_CLAN_ERR_INVITE_PRIVATE;
        if (result.IsError("DoesNotExist", "ClanNotFound")) return RL_CLAN_ERR_NOT_FOUND;
        if (result.IsError("InvalidState", "ClanInviteRankOrderInvalid")) return RL_CLAN_ERR_RANK_INVALID;
        if (result.IsError("InvalidState", "ClanInviteRankNotEmpty")) return RL_CLAN_ERR_RANK_NOT_EMPTY;
        if (result.IsError("InvalidState", "ClanInviteRankChangeNotAllowed")) return RL_CLAN_ERR_RANK_CHANGE_NOT_ALLOWED;
        if (result.IsError("DoesNotExist", "RockstarAccount")) return RL_CLAN_ERR_ROCKSTAR_ID_DOES_NOT_EXIST;
        if (result.IsError("DoesNotExist", "Nickname")) return RL_CLAN_ERR_SC_NICKNAME_DOES_NOT_EXIST;
        if (result.IsError("DoesNotExist", "PlayerAccount")) return RL_CLAN_ERR_PLAYER_ACCOUNT_DOES_NOT_EXIST;
        if (result.IsError("NotAllowed", "ClanPermissionError")) return RL_CLAN_ERR_CLAN_PERMISSION_ERROR;
        if (result.IsError("NotAllowed", "Privilege")) return RL_CLAN_ERR_NO_PRIVILEGE;
        if (result.IsError("NotAllowed", "Rate")) return RL_CLAN_ERR_RATE_LIMIT_EXCEEDED;
        if (result.IsError("SqlException", nullptr)) return RL_CLAN_ERR_SQL_EXCEPTION;

        // TODO improve the alert text in the abort-retry-ignore dialog.
        rverify(false, catchall, 
            rlTaskError("Unknown error code: %s.%s", 
                result.m_Code ? result.m_Code : "", 
                result.m_CodeEx ? result.m_CodeEx : ""));
    }
    rcatchall
    {
    }

    return RL_CLAN_ERR_UNKNOWN;
}

int
rlClanRosBaseTask::ProcessExtractClanInfo(const rage::parTreeNode* desc, rlClanDesc& clanInfo)
{
    rtry
    {
        rlClanId clanId;
        rverify(rlHttpTaskHelper::ReadInt64(clanId, desc, NULL, "Id"), catchall, );
        clanInfo.m_Id = (rlClanId)clanId;

        const char* clanName = rlHttpTaskHelper::ReadString(desc, NULL, "Name");
        rcheck(clanName, catchall, rlTaskError("Failed to find <Clan.Name> attribute"));
        safecpy(clanInfo.m_ClanName, clanName, RL_CLAN_NAME_MAX_CHARS);

        const char* clanTag = rlHttpTaskHelper::ReadString(desc, NULL, "Tag");
        rcheck(clanTag, catchall, rlTaskError("Failed to find <Clan.Tag> attribute"));
        safecpy(clanInfo.m_ClanTag, clanTag, RL_CLAN_TAG_MAX_CHARS);

        int memberCount;
        rverify(rlHttpTaskHelper::ReadInt(memberCount, desc, NULL, "MemberCount"), catchall, );
        clanInfo.m_MemberCount = memberCount;

        int createdTimePosix;
        rverify(rlHttpTaskHelper::ReadInt(createdTimePosix, desc, NULL, "CreatedTimePosix"), catchall, );
        clanInfo.m_CreatedTimePosix = createdTimePosix;

        int isSystemClan;
        rverify(rlHttpTaskHelper::ReadInt(isSystemClan, desc, NULL, "IsSystemClan"), catchall, );
        clanInfo.m_IsSystemClan = (0 != isSystemClan);

        int isOpenClan;
        rverify(rlHttpTaskHelper::ReadInt(isOpenClan, desc, NULL, "IsOpenClan"), catchall, );
        clanInfo.m_IsOpenClan = (0 != isOpenClan);

        const char* clanMotto = rlHttpTaskHelper::ReadString(desc, NULL, "Motto");
        if (clanMotto == NULL)
        {
            clanInfo.m_ClanMotto[0] = 0;
        }
        else
        {
            safecpy(clanInfo.m_ClanMotto, clanMotto, RL_CLAN_MOTTO_MAX_CHARS);
        }

		//Parse the crew colors.  It will be a csv of #RRGGBB strings.
		//NOTE the default color is set in the rlClanDesc constructor.
		const char* clanColor = rlHttpTaskHelper::ReadString(desc, NULL, "Colors");
		if (clanColor != NULL)
		{
			//Yes, we're only grabbing the first color in the list.
			char colorString[16];
			unsigned stringSize = sizeof(colorString);
			if (parMemberArray::ReadCsvDatum(colorString, &stringSize, clanColor))
			{
				int r = 0,g = 0,b = 0;
				if(sscanf(colorString, "#%2x%2x%2x",&r,&g,&b) == 3)
				{
					Color32 colorHelper(r,g,b);
					clanInfo.m_clanColor = colorHelper.GetColor();
				}
				else
				{
					rlTaskError("Color is in invalid format [%s]", colorString);
				}
			}
			else
			{
				rlTaskError("ColorCSV is in invalid format [%s]", clanColor);
			}
		}

        return RL_CLAN_SUCCESS;
    }
    rcatchall
    {
    }

    return RL_CLAN_ERR_PARSER_FAILED;
}

int
rlClanRosBaseTask::ProcessExtractClanLeader(const rage::parTreeNode* desc, rlClanLeader& leader)
{
    rtry
    {
        rlClanId clanId;
        rverify(rlHttpTaskHelper::ReadInt64(clanId, desc, NULL, "ClanId"), catchall, );
        leader.m_ClanId = (rlClanId)clanId;

        return ProcessExtractPlayerInfo(desc, leader.m_MemberInfo);
    }
    rcatchall
    {
    }

    return RL_CLAN_ERR_PARSER_FAILED;
}

int
rlClanRosBaseTask::ProcessExtractRankInfo(const rage::parTreeNode* desc, rlClanRank& rankInfo)
{
    rtry
    {
        rlClanRankId rankId;
        rverify(rlHttpTaskHelper::ReadInt64(rankId, desc, NULL, "Id"), catchall, );
        rankInfo.m_Id = rankId;

        const char* rankName = rlHttpTaskHelper::ReadString(desc, NULL, "Name");
        rcheck(rankName, catchall, rlTaskError("Failed to find <Rank.Name> attribute"));
        safecpy(rankInfo.m_RankName, rankName, RL_CLAN_RANK_NAME_MAX_CHARS);

        int rankOrder;
        rverify(rlHttpTaskHelper::ReadInt(rankOrder, desc, NULL, "RankOrder"), catchall, );
        rankInfo.m_RankOrder = rankOrder;

        s64 systemFlags; 
        rverify(rlHttpTaskHelper::ReadInt64(systemFlags, desc, NULL, "SystemFlags"), catchall, );
        rankInfo.m_SystemFlags = systemFlags;

        return RL_CLAN_SUCCESS;
    }
    rcatchall
    {
    }

    return RL_CLAN_ERR_PARSER_FAILED;
}


int
rlClanRosBaseTask::ProcessExtractPlayerInfo(const rage::parTreeNode* desc, rlRosPlayerInfo& playerInfo)
{
    rtry
    {
        const char* gamerHandle = rlHttpTaskHelper::ReadString(desc, NULL, "GamerHandle");
        if (gamerHandle)
        {
            playerInfo.m_GamerHandle.FromString(gamerHandle);
        }
        else
        {
            playerInfo.m_GamerHandle.Clear();
        }

        const char* gamertag = rlHttpTaskHelper::ReadString(desc, NULL, "Gamertag");
        if (gamertag)
        {
            safecpy(playerInfo.m_Gamertag, gamertag, RL_MAX_NAME_BUF_SIZE);
        }
        else
        {
            playerInfo.m_Gamertag[0] = '\0';
        }

        const char* nickname = rlHttpTaskHelper::ReadString(desc, NULL, "Nickname");
        if (nickname)
        {
            safecpy(playerInfo.m_Nickname, nickname, RL_MAX_NAME_BUF_SIZE);
        }
        else
        {
            playerInfo.m_Nickname[0] = '\0';
        }

        RockstarId rockstarId;
        rverify(rlHttpTaskHelper::ReadInt64(rockstarId, desc, NULL, "RockstarId"), catchall, );
        playerInfo.m_RockstarId = rockstarId;

        PlayerAccountId playerAccountId;
        rverify(rlHttpTaskHelper::ReadInt(playerAccountId, desc, NULL, "PlayerAccountId"), catchall, );
        playerInfo.m_PlayerAccountId = playerAccountId;

        return RL_CLAN_SUCCESS;
    }
    rcatchall
    {
    }

    return RL_CLAN_ERR_PARSER_FAILED;
}

int
rlClanRosBaseTask::ProcessMembershipRecord(const parTreeNode* record,
                                            rlClanMembershipData* membership)
{
    rtry
    {
        rlClanMemberId memberId;
        rverify(rlHttpTaskHelper::ReadInt64(memberId, record, NULL, "Id"),
                catchall,
                rlTaskError("Failed to read 'Id' attribute"));

        bool isPrimary;
        rverify(rlHttpTaskHelper::ReadBool(isPrimary, record, NULL, "IsPrimary"),
                catchall,
                rlTaskError("Failed to read 'IsPrimary' attribute"));

        const parTreeNode* rankNode = record->FindChildWithName("Rank");
        rcheck(rankNode, catchall, rlTaskError("Failed to find <Rank> element"));

        const parTreeNode* clanDescNode = record->FindChildWithName("Clan");
        rcheck(clanDescNode, catchall, rlTaskError("Failed to find <Clan> element"));

        rlClanRank rank;
        int resultCode = ProcessExtractRankInfo(rankNode, rank);
        rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

        rlClanDesc clanDesc;
        resultCode = ProcessExtractClanInfo(clanDescNode, clanDesc);
        rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

        membership->Init(memberId, clanDesc, rank, isPrimary);

        return RL_CLAN_SUCCESS;
    }
    rcatchall
    {
    }

    return RL_CLAN_ERR_PARSER_FAILED;
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosCreateTask
//////////////////////////////////////////////////////////////////////////
rlClanRosCreateTask::rlClanRosCreateTask()
{
}

rlClanRosCreateTask::~rlClanRosCreateTask()
{
}

bool 
rlClanRosCreateTask::Configure(const int localGamerIndex,
                               const char* clanName,
                               const char* clanTag,
                               bool isOpenClan)
{
    bool success = false;

    rtry
    {
        rverify(StringLength(clanName) < RL_CLAN_NAME_MAX_CHARS,
                catchall,
                rlTaskError("Clan name is too long"));
        rverify(StringLength(clanTag) < RL_CLAN_TAG_MAX_CHARS,
                catchall,
                rlTaskError("Clan tag is too long"));
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("clanName", clanName, true), catchall, );
        rverify(AddStringParameter("clanTag", clanTag, true), catchall, );
        rverify(AddStringParameter("isOpenClan", isOpenClan ? "true" : "false", true), catchall, );
		rverify(AddStringParameter("clanMotto", clanName, true), catchall, );
		rverify(AddStringParameter("clanColors", "Yellow", true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosCreateTask::GetServiceMethod() const
{
    return "Clans.asmx/Create";
}

bool 
rlClanRosCreateTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* record = node->FindChildWithName("Member");
        rcheck(record, catchall, rlTaskError("Failed to find <Member> element"));

        rlClanMembershipData membership;

        resultCode = ProcessMembershipRecord(record, &membership);

        rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

        rverify(membership.IsValid(), catchall,);

        const int localGamerIndex = this->GetLocalGamerIndex();

        if(!rlClan::HasMembership(localGamerIndex, membership.m_Clan.m_Id))
        {
            //The ClanJoined event, and possibly the PrimaryClanChanged event,
            //will be dispatched in AddMembership()

            rverify(rlClan::AddMembership(localGamerIndex, membership, true),
                    catchall,
                    rlTaskError("Error adding clan for gamer: %d", localGamerIndex));
        }

        return true;
    }
    rcatchall
    {
    }

    resultCode = RL_CLAN_ERR_PARSER_FAILED;
    return false;
}

void 
rlClanRosCreateTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosInviteTask
//////////////////////////////////////////////////////////////////////////
rlClanRosInviteTask::rlClanRosInviteTask()
{
}

rlClanRosInviteTask::~rlClanRosInviteTask()
{
}

bool 
rlClanRosInviteTask::Configure(const int localGamerIndex,
                               const rlGamerHandle& gamer,
                               rlClanId clanId,
                               int rankOrder,
                               bool expireExtraInvitations)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        char gamerName[RL_MAX_GAMER_HANDLE_CHARS];
        rverify(gamer.ToString(gamerName), catchall, );

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("targetGamerHandle", gamerName, true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddIntParameter("requestRankOrder", rankOrder), catchall, );
        rverify(AddStringParameter("expireExtraInvitations", expireExtraInvitations ? "true" : "false", true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosInviteTask::GetServiceMethod() const
{
    return "Clans.asmx/Invite";
}

bool 
rlClanRosInviteTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosInviteTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosDeleteInviteTask
//////////////////////////////////////////////////////////////////////////
rlClanRosDeleteInviteTask::rlClanRosDeleteInviteTask()
{
}

rlClanRosDeleteInviteTask::~rlClanRosDeleteInviteTask()
{
}

bool 
rlClanRosDeleteInviteTask::Configure(const int localGamerIndex,
                                     rlClanInviteId inviteId)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("id", inviteId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosDeleteInviteTask::GetServiceMethod() const
{
    return "Clans.asmx/DeleteInvite";
}

bool 
rlClanRosDeleteInviteTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosDeleteInviteTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosJoinTask
//////////////////////////////////////////////////////////////////////////
rlClanRosJoinTask::rlClanRosJoinTask()
{
}

rlClanRosJoinTask::~rlClanRosJoinTask()
{
}

bool 
rlClanRosJoinTask::Configure(const int localGamerIndex,
                             rlClanId clanId)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosJoinTask::GetServiceMethod() const
{
    return "Clans.asmx/Join";
}

bool 
rlClanRosJoinTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* record = node->FindChildWithName("Member");
        rcheck(record, catchall, rlTaskError("Failed to find <Member> element"));

        rlClanMembershipData membership;

        resultCode = ProcessMembershipRecord(record, &membership);

        rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

        rverify(membership.IsValid(), catchall,);

        const int localGamerIndex = this->GetLocalGamerIndex();

        if(!rlClan::HasMembership(localGamerIndex, membership.m_Clan.m_Id))
        {
            //The ClanJoined event, and possibly the PrimaryClanChanged event,
            //will be dispatched in AddMembership()

            rverify(rlClan::AddMembership(localGamerIndex, membership, true),
                    catchall,
                    rlTaskError("Error adding clan for gamer: %d", localGamerIndex));
        }

        return true;
    }
    rcatchall
    {
    }

    resultCode = RL_CLAN_ERR_PARSER_FAILED;
    return false;
}

void 
rlClanRosJoinTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosLeaveTask
//////////////////////////////////////////////////////////////////////////
rlClanRosLeaveTask::rlClanRosLeaveTask()
: m_ClanId(RL_INVALID_CLAN_ID)
{
}

rlClanRosLeaveTask::~rlClanRosLeaveTask()
{
}

bool 
rlClanRosLeaveTask::Configure(const int localGamerIndex,
                              rlClanId clanId)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        m_ClanId = clanId;

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosLeaveTask::GetServiceMethod() const
{
    return "Clans.asmx/Leave";
}

bool 
rlClanRosLeaveTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    rlClanDesc newPrimaryClan;

    rlAssert(node);
    
    rtry
    {
        const parTreeNode* member = node->FindChildWithName("Member");
        const parTreeNode* clanDesc = NULL;
        if (NULL != member)
        {
            clanDesc = member->FindChildWithName("Clan");
        }

        if (NULL == clanDesc)
        {
            newPrimaryClan.Clear();
        }
        else
        {
            resultCode = ProcessExtractClanInfo(clanDesc, newPrimaryClan);
            rverify(RL_CLAN_SUCCESS == resultCode, catchall, );
        }

        rlClan::RemoveMembership(this->GetLocalGamerIndex(), m_ClanId, newPrimaryClan.m_Id);

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosLeaveTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosSetPrimaryTask
//////////////////////////////////////////////////////////////////////////
rlClanRosSetPrimaryTask::rlClanRosSetPrimaryTask()
{
}

rlClanRosSetPrimaryTask::~rlClanRosSetPrimaryTask()
{
}

bool 
rlClanRosSetPrimaryTask::Configure(const int localGamerIndex,
                              rlClanId clanId)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", (u64)clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosSetPrimaryTask::GetServiceMethod() const
{
    return "Clans.asmx/SetPrimaryClan";
}

bool 
rlClanRosSetPrimaryTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    rlClanDesc primaryClan;

    rlAssert(node);
    
    rtry
    {
        const parTreeNode* member = node->FindChildWithName("Member");
        const parTreeNode* clanDesc = NULL;
        if (NULL != member)
        {
            clanDesc = member->FindChildWithName("Clan");
        }

        if (NULL == clanDesc)
        {
            primaryClan.Clear();
        }
        else
        {
            resultCode = ProcessExtractClanInfo(clanDesc, primaryClan);
            rverify(RL_CLAN_SUCCESS == resultCode, catchall, );
        }

        const rlClanId oldPrimaryClanId = rlClan::GetPrimaryClan(GetLocalGamerIndex()).m_Id;
        if(oldPrimaryClanId != primaryClan.m_Id)
        {
            rverify(rlClan::UpdatePrimaryClan(GetLocalGamerIndex(),
                                            oldPrimaryClanId,
                                            primaryClan.m_Id), catchall, );
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosSetPrimaryTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosKickTask
//////////////////////////////////////////////////////////////////////////
rlClanRosKickTask::rlClanRosKickTask()
{
}

rlClanRosKickTask::~rlClanRosKickTask()
{
}

bool 
rlClanRosKickTask::Configure(const int localGamerIndex,
                             const rlGamerHandle& gamer,
                             rlClanId clanId)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        char gamerName[RL_MAX_GAMER_HANDLE_CHARS];
        rverify(gamer.ToString(gamerName), catchall, );

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("targetGamerHandle", gamerName, true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosKickTask::GetServiceMethod() const
{
    return "Clans.asmx/Kick";
}

bool 
rlClanRosKickTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosKickTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosRankCreateTask
//////////////////////////////////////////////////////////////////////////
rlClanRosRankCreateTask::rlClanRosRankCreateTask()
{
}

rlClanRosRankCreateTask::~rlClanRosRankCreateTask()
{
}

bool 
rlClanRosRankCreateTask::Configure(const int localGamerIndex,
                                   rlClanId clanId,
                                   const char* rankName,
                                   s64 initialSystemFlags)
{
    bool success = false;

    rtry
    {
        rverify(StringLength(rankName) < RL_CLAN_RANK_NAME_MAX_CHARS,
                catchall,
                rlTaskError("Rank name is too long"));

        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddStringParameter("rankName", rankName, true), catchall, );
        rverify(AddIntParameter("initialSystemFlags", initialSystemFlags), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosRankCreateTask::GetServiceMethod() const
{
    return "Clans.asmx/RankCreate";
}

bool 
rlClanRosRankCreateTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosRankCreateTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosRankUpdateTask
//////////////////////////////////////////////////////////////////////////
rlClanRosRankUpdateTask::rlClanRosRankUpdateTask()
{
}

rlClanRosRankUpdateTask::~rlClanRosRankUpdateTask()
{
}

bool 
rlClanRosRankUpdateTask::Configure(const int localGamerIndex,
                                   rlClanId clanId,
                                   rlClanRankId rankId,
                                   int adjustRankOrder,
                                   const char* rankName,
                                   s64 updateSystemFlags)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddIntParameter("rankId", rankId), catchall, );
        rverify(AddStringParameter("rankName", rankName, true), catchall, );
        rverify(AddIntParameter("adjustRankOrder", adjustRankOrder), catchall, );
        if (updateSystemFlags < 0)
        {
            rverify(AddIntParameter("updateSystemFlags", -1), catchall, );
        }
        else
        {
            rverify(AddIntParameter("updateSystemFlags", updateSystemFlags), catchall, );
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosRankUpdateTask::GetServiceMethod() const
{
    return "Clans.asmx/RankUpdate";
}

bool 
rlClanRosRankUpdateTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosRankUpdateTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosDeleteWallMessageTask
//////////////////////////////////////////////////////////////////////////
rlClanRosDeleteWallMessageTask::rlClanRosDeleteWallMessageTask()
{
}

rlClanRosDeleteWallMessageTask::~rlClanRosDeleteWallMessageTask()
{
}

bool 
rlClanRosDeleteWallMessageTask::Configure(const int localGamerIndex,
                                   rlClanId clanId,
                                   rlClanWallMessageId msgId)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddIntParameter("msgId", msgId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosDeleteWallMessageTask::GetServiceMethod() const
{
    return "Clans.asmx/DeleteWallMessage";
}

bool 
rlClanRosDeleteWallMessageTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosDeleteWallMessageTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosWriteWallMessageTask
//////////////////////////////////////////////////////////////////////////
rlClanRosWriteWallMessageTask::rlClanRosWriteWallMessageTask()
{
}

rlClanRosWriteWallMessageTask::~rlClanRosWriteWallMessageTask()
{
}

bool 
rlClanRosWriteWallMessageTask::Configure(const int localGamerIndex,
                                   rlClanId clanId,
                                   const char* message)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddStringParameter("message", message, true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosWriteWallMessageTask::GetServiceMethod() const
{
    return "Clans.asmx/WriteWallMessage";
}

bool 
rlClanRosWriteWallMessageTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosWriteWallMessageTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosRankDeleteTask
//////////////////////////////////////////////////////////////////////////
rlClanRosRankDeleteTask::rlClanRosRankDeleteTask()
{
}

rlClanRosRankDeleteTask::~rlClanRosRankDeleteTask()
{
}

bool 
rlClanRosRankDeleteTask::Configure(const int localGamerIndex,
                                   rlClanId clanId,
                                   rlClanRankId rankId)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddIntParameter("rankId", rankId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosRankDeleteTask::GetServiceMethod() const
{
    return "Clans.asmx/RankDelete";
}

bool 
rlClanRosRankDeleteTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosRankDeleteTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosMemberUpdateRankIdTask
//////////////////////////////////////////////////////////////////////////
rlClanRosMemberUpdateRankIdTask::rlClanRosMemberUpdateRankIdTask()
{
}

rlClanRosMemberUpdateRankIdTask::~rlClanRosMemberUpdateRankIdTask()
{
}

bool 
rlClanRosMemberUpdateRankIdTask::Configure(const int localGamerIndex,
                                           const rlGamerHandle& targetGamerHandle,
                                           rlClanId clanId,
                                           bool promote)
{
    bool success = false;

    rtry
    {
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        char gamerName[RL_MAX_GAMER_HANDLE_CHARS];
        rverify(targetGamerHandle.ToString(gamerName), catchall, );

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("targetGamerHandle", gamerName, true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddStringParameter("promote", promote ? "true" : "false", true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosMemberUpdateRankIdTask::GetServiceMethod() const
{
    return "Clans.asmx/MemberUpdateRankId";
}

bool 
rlClanRosMemberUpdateRankIdTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosMemberUpdateRankIdTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetInvitesTaskBase
//////////////////////////////////////////////////////////////////////////
rlClanRosGetInvitesTaskBase::rlClanRosGetInvitesTaskBase()
{
}

rlClanRosGetInvitesTaskBase::~rlClanRosGetInvitesTaskBase()
{
}

bool 
rlClanRosGetInvitesTaskBase::Configure(const int localGamerIndex,
                                       int pageIndex,
                                       rlClanInvite* resultInvites,
                                       unsigned int maxInvitesCount,
                                       unsigned int* resultInvitesCount,
                                       unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(resultInvites, catchall, );
        rverify(resultInvitesCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultInvites = resultInvites;
        m_MaxInvitesCount = maxInvitesCount;
        m_ResultInvitesCount = resultInvitesCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageSize", maxInvitesCount), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool 
rlClanRosGetInvitesTaskBase::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    *m_ResultInvitesCount = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Invites");
        rcheck(results, catchall, rlTaskError("Failed to find <Invites> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxInvitesCount; ++index)
        {
            m_ResultInvites[index].Clear();
        }

        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxInvitesCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Invite") != 0) continue;

                const char* message = rlHttpTaskHelper::ReadString(record, NULL, "Message");
                if (message)
                {
                    safecpy(m_ResultInvites[index].m_Message, message, RL_CLAN_INVITE_MESSAGE_MAX_CHARS);
                }

                const parTreeNode* inviterDesc = record->FindChildWithName("Inviter");
                rcheck(inviterDesc, catchall, rlTaskError("Failed to find <Inviter> element"));

                resultCode = ProcessExtractPlayerInfo(inviterDesc, m_ResultInvites[index].m_Inviter);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                const parTreeNode* inviteeDesc = record->FindChildWithName("Invitee");
                rcheck(inviteeDesc, catchall, rlTaskError("Failed to find <Invitee> element"));

                resultCode = ProcessExtractPlayerInfo(inviteeDesc, m_ResultInvites[index].m_Invitee);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                rlClanInviteId inviteId;
                rverify(rlHttpTaskHelper::ReadInt64(inviteId, record, NULL, "Id"), catchall, );
                m_ResultInvites[index].m_Id = (rlClanInviteId)inviteId;

                const parTreeNode* clanDesc = record->FindChildWithName("Clan");
                rcheck(clanDesc, catchall, rlTaskError("Failed to find <Clan> element"));

                resultCode = ProcessExtractClanInfo(clanDesc, m_ResultInvites[index].m_Clan);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultInvitesCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetInvitesTaskBase::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultInvitesCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetInvitesTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetInvitesTask::rlClanRosGetInvitesTask()
{
}

rlClanRosGetInvitesTask::~rlClanRosGetInvitesTask()
{
}

const char* 
rlClanRosGetInvitesTask::GetServiceMethod() const
{
    return "Clans.asmx/GetInvites";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetSentInvitesTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetSentInvitesTask::rlClanRosGetSentInvitesTask()
{
}

rlClanRosGetSentInvitesTask::~rlClanRosGetSentInvitesTask()
{
}

const char* 
rlClanRosGetSentInvitesTask::GetServiceMethod() const
{
    return "Clans.asmx/GetSentInvites";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetMembersTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetMembersTask::rlClanRosGetMembersTask()
{
}

rlClanRosGetMembersTask::~rlClanRosGetMembersTask()
{
}

bool 
rlClanRosGetMembersTask::Configure(const int localGamerIndex, 
                                   int pageIndex,
                                   rlClanId clanId, 
                                   const char* clanName, 
                                   rlClanMember* resultGamers,
                                   unsigned int maxGamersCount,
                                   unsigned int* resultGamersCount,
                                   unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(resultGamers, catchall, );
        rverify(resultGamersCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultGamers = resultGamers;
        m_MaxGamersCount = maxGamersCount;
        m_ResultGamersCount = resultGamersCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );
        rverify(AddIntParameter("pageSize", maxGamersCount), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddStringParameter("clanName", clanName, true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetMembersTask::GetServiceMethod() const
{
    return "Clans.asmx/GetMembers";
}

bool 
rlClanRosGetMembersTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    *m_ResultGamersCount = 0;

    rlAssert(node);
    
    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Members");
        rcheck(results, catchall, rlTaskError("Failed to find <Members> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "Count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxGamersCount; ++index)
        {
            m_ResultGamers[index].Clear();
        }

        *m_ResultGamersCount = 0;
        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxGamersCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Member") != 0) continue;

                const parTreeNode* detailDesc = record->FindChildWithName("Detail");
                rcheck(detailDesc, catchall, rlTaskError("Failed to find <Detail> element"));

                resultCode = ProcessExtractPlayerInfo(detailDesc, m_ResultGamers[index].m_MemberInfo);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                resultCode = ProcessMembershipRecord(record, &m_ResultGamers[index].m_MemberClanInfo);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultGamersCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetMembersTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultGamersCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetMembersTitleOnlyTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetMembersTitleOnlyTask::rlClanRosGetMembersTitleOnlyTask()
{
}

rlClanRosGetMembersTitleOnlyTask::~rlClanRosGetMembersTitleOnlyTask()
{
}

const char* 
rlClanRosGetMembersTitleOnlyTask::GetServiceMethod() const
{
    return "Clans.asmx/GetMembersTitleOnly";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetPrimaryClansTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetPrimaryClansTask::rlClanRosGetPrimaryClansTask()
{
}

rlClanRosGetPrimaryClansTask::~rlClanRosGetPrimaryClansTask()
{
}

bool 
rlClanRosGetPrimaryClansTask::Configure(const int localGamerIndex, 
                                        const rlGamerHandle* gamers,
                                        const unsigned numGamers,
                                        rlClanMember* resultGamers,
                                        unsigned int* resultGamersCount,
                                        unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(resultGamers, catchall, );
        rverify(gamers, catchall, );
        rverify(resultGamersCount, catchall, );
        rverify(numGamers <= RL_CLAN_MAX_GAMER_HANDLE_COUNT, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultGamers = resultGamers;
        m_MaxGamersCount = numGamers;
        m_ResultGamersCount = resultGamersCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

        char gamerHandle[RL_MAX_GAMER_HANDLE_CHARS];
        rverify(BeginListParameter("gamerHandlesCsv"), catchall, );
        for (unsigned int i = 0; i < numGamers; ++i)
        {
            rverify(gamers[i].ToString(gamerHandle), catchall, );
            rverify(AddListParamStringValue(gamerHandle), catchall, );
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetPrimaryClansTask::GetServiceMethod() const
{
    return "Clans.asmx/GetPrimaryClans";
}

bool 
rlClanRosGetPrimaryClansTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    *m_ResultGamersCount = 0;

    rlAssert(node);
    
    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Members");
        rcheck(results, catchall, rlTaskError("Failed to find <Members> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "Count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxGamersCount; ++index)
        {
            m_ResultGamers[index].Clear();
        }

        *m_ResultGamersCount = 0;
        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxGamersCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Member") != 0) continue;

                const parTreeNode* detailDesc = record->FindChildWithName("Detail");
                rcheck(detailDesc, catchall, rlTaskError("Failed to find <Detail> element"));

                const char* gamerHandle = rlHttpTaskHelper::ReadString(detailDesc, NULL, "GamerHandle");
                rcheck(gamerHandle, catchall, rlTaskError("Failed to find <Member.GamerHandle> attribute"));
                m_ResultGamers[index].m_MemberInfo.m_GamerHandle.FromString(gamerHandle);

                resultCode = ProcessMembershipRecord(record, &m_ResultGamers[index].m_MemberClanInfo);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultGamersCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetPrimaryClansTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultGamersCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetMyPrimaryClanTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetMyPrimaryClanTask::rlClanRosGetMyPrimaryClanTask()
: m_LocalGamerIndex(-1)
, m_ClanDesc(NULL)
, m_NumClansRetrieved(0)
{
}

rlClanRosGetMyPrimaryClanTask::~rlClanRosGetMyPrimaryClanTask()
{
}

bool
rlClanRosGetMyPrimaryClanTask::Configure(const int localGamerIndex,
                                        rlClanDesc* clanDesc)
{
    m_LocalGamerIndex = localGamerIndex;
    m_ClanDesc = clanDesc;

    return true;
}

void
rlClanRosGetMyPrimaryClanTask::DoCancel()
{
    rlGetTaskManager()->CancelTask(&m_MyStatus);
}

void
rlClanRosGetMyPrimaryClanTask::Start()
{
    rlTaskBase::Start();

    rlClanRosGetPrimaryClansTask* task = NULL;
    rtry
    {
        rlGamerHandle gamerHandle;
        rcheck(rlPresence::GetGamerHandle(m_LocalGamerIndex, &gamerHandle),
                catchall,
                rlTaskError("Error getting gamer handle"));

        OUTPUT_ONLY(char ghStr[RL_MAX_GAMER_HANDLE_CHARS]);
        rlTaskDebug("Retrieving primary clan for gamer: %s...",
                    gamerHandle.ToString(ghStr));

        rcheck(rlGetTaskManager()->CreateTask(&task), catchall,
                rlTaskError("Failed to allocate task"));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, &gamerHandle, 1, &m_ClanMember, 
                                     &m_NumClansRetrieved, &m_NumClansRetrieved, &m_MyStatus), catchall, 
            rlTaskError("Failed to configure task"));
        rcheck(rlGetTaskManager()->AddParallelTask(task), catchall,
            rlTaskError("Failed to queue task"));
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
        this->Finish(FINISH_FAILED);
    }
}

void
rlClanRosGetMyPrimaryClanTask::Finish(const FinishType finishType)
{
    if(FINISH_SUCCEEDED == finishType)
    {
        *m_ClanDesc = m_ClanMember.m_MemberClanInfo.m_Clan;
    }

    rlTaskBase::Finish(finishType);
}

void
rlClanRosGetMyPrimaryClanTask::Update(const unsigned timeStep)
{
    rlTaskBase::Update(timeStep);

    if(this->WasCanceled())
    {
        if(m_MyStatus.Pending())
        {
            rlGetTaskManager()->CancelTask(&m_MyStatus);
        }
        this->Finish(FINISH_FAILED);
    }
    else if(m_MyStatus.Succeeded())
    {
        this->Finish(FINISH_SUCCEEDED);
    }
    else if(m_MyStatus.Failed())
    {
        this->Finish(FINISH_FAILED);
    }
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetAllTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetAllTask::rlClanRosGetAllTask()
{
}

rlClanRosGetAllTask::~rlClanRosGetAllTask()
{
}

bool 
rlClanRosGetAllTask::Configure(const int localGamerIndex,
                               int pageIndex,
                               const char* searchTerm,
                               const rlClanOptionalBool isSystemClan,
                               const rlClanOptionalBool isOpenClan,
                               rlClanSortMode sortMode,
                               rlClanDesc* resultClans,
                               unsigned int maxClansCount,
                               unsigned int* resultClansCount,
                               unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(resultClans, catchall, );
        rverify(resultClansCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultClans = resultClans;
        m_MaxClansCount = maxClansCount;
        m_ResultClansCount = resultClansCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );
        rverify(AddIntParameter("pageSize", maxClansCount), catchall, );
        rverify(AddIntParameter("isSystemClan", (int)isSystemClan), catchall, );
        rverify(AddIntParameter("isOpenClan", (int)isOpenClan), catchall, );
        rverify(AddStringParameter("search", searchTerm, true), catchall, );
        rverify(AddIntParameter("sortMode", (int)sortMode), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetAllTask::GetServiceMethod() const
{
    return "Clans.asmx/GetAll";
}

bool 
rlClanRosGetAllTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    *m_ResultClansCount = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Clans");
        rcheck(results, catchall, rlTaskError("Failed to find <Clans> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            if (rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"))
            {
                *m_ResultTotalCount = maxCount;
            }
            else
            {
                *m_ResultTotalCount = count;
            }
        }

        unsigned int index;
        for (index = 0; index < m_MaxClansCount; ++index)
        {
            m_ResultClans[index].Clear();
        }

        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxClansCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Clan") != 0) continue;

                resultCode = ProcessExtractClanInfo(record, m_ResultClans[index]);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultClansCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetAllTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultClansCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetDescTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetDescTask::rlClanRosGetDescTask()
{
}

rlClanRosGetDescTask::~rlClanRosGetDescTask()
{
}

bool 
rlClanRosGetDescTask::Configure(const int localGamerIndex,
                                const rlClanId clanId,
                                rlClanDesc* resultClanDesc)
{
    bool success = false;

    rtry
    {
        rverify(resultClanDesc, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_Result = resultClanDesc;
        
        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetDescTask::GetServiceMethod() const
{
    return "Clans.asmx/GetDesc";
}

bool 
rlClanRosGetDescTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Clans");
        rcheck(results, catchall, rlTaskError("Failed to find <Clans> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

        m_Result->Clear();

        if (0 != count)
        {
            rverify(1 == count, catchall, );

            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            for( ;iter != results->EndChildren(); ++iter)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(0 == stricmp(record->GetElement().GetName(), "Clan"))
                {
                    break;
                }
            }

            rverify(iter != results->EndChildren(), catchall, );

            parTreeNode* record = *iter;
            rlAssert(record);

            rverify(0 == stricmp(record->GetElement().GetName(), "Clan"), catchall, );

            resultCode = ProcessExtractClanInfo(record, *m_Result);
            rverify(RL_CLAN_SUCCESS == resultCode, catchall, );
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetDescTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetDescsTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetDescsTask::rlClanRosGetDescsTask()
: m_ClanDescs(NULL)
, m_ClansCount(0)
{
}

rlClanRosGetDescsTask::~rlClanRosGetDescsTask()
{
}

bool
rlClanRosGetDescsTask::Configure(const int localGamerIndex,
                                 const rlClanId* clans,
                                 unsigned int clansCount,
                                 rlClanDesc* clanDescs)
{
    bool success = false;

    rtry
    {
        rverify(clanDescs, catchall, );
        rverify(clans, catchall, );
        rverify(clansCount > 0, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ClanDescs = clanDescs;
        m_ClansCount = clansCount;
        
        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

        rverify(BeginListParameter("clanIdsCsv"), catchall, );
        for (unsigned int i = 0; i < clansCount; ++i)
        {
            
            rverify(AddListParamIntValue(clans[i]), catchall, );
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetDescsTask::GetServiceMethod() const
{
    return "Clans.asmx/GetDescs";
}

bool 
rlClanRosGetDescsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Clans");
        rcheck(results, catchall, rlTaskError("Failed to find <Clans> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

        unsigned int index;
        for (index = 0; index < m_ClansCount; ++index)
        {
            m_ClanDescs[index].Clear();
        }

        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_ClansCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Clan") != 0) continue;

                resultCode = ProcessExtractClanInfo(record, m_ClanDescs[index]);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}


void 
rlClanRosGetDescsTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetLeadersForClansTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetLeadersForClansTask::rlClanRosGetLeadersForClansTask()
: m_ResultLeaders(NULL)
, m_ClansCount(0)
, m_ResultCount(NULL)
{
}

rlClanRosGetLeadersForClansTask::~rlClanRosGetLeadersForClansTask()
{
}

bool
rlClanRosGetLeadersForClansTask::Configure(const int localGamerIndex,
                                 const rlClanId* clans,
                                 unsigned int clansCount,
                                 rlClanLeader* resultLeaders,
                                 unsigned int* resultCount)
{
    bool success = false;

    rtry
    {
        rverify(resultLeaders, catchall, );
        rverify(resultCount, catchall, );
        rverify(clans, catchall, );
        rverify(clansCount > 0, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultLeaders = resultLeaders;
        m_ClansCount = clansCount;
        m_ResultCount = resultCount;
        
        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

        rverify(BeginListParameter("clanIdsCsv"), catchall, );
        for (unsigned int i = 0; i < clansCount; ++i)
        {
            
            rverify(AddListParamIntValue(clans[i]), catchall, );
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetLeadersForClansTask::GetServiceMethod() const
{
    return "Clans.asmx/GetLeadersForClans";
}

bool 
rlClanRosGetLeadersForClansTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Result");
        rcheck(results, catchall, rlTaskError("Failed to find <Result> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "Count"), catchall, );

        unsigned int index;
        for (index = 0; index < m_ClansCount; ++index)
        {
            m_ResultLeaders[index].Clear();
        }

        *m_ResultCount = 0;
        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            
            index = 0;
            for( ;iter != results->EndChildren() && index < m_ClansCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Leader") != 0) continue;

                resultCode = ProcessExtractClanLeader(record, m_ResultLeaders[*m_ResultCount]);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );
                (*m_ResultCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        *m_ResultCount = 0;
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}


void 
rlClanRosGetLeadersForClansTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetRanksTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetRanksTask::rlClanRosGetRanksTask()
{
}

rlClanRosGetRanksTask::~rlClanRosGetRanksTask()
{
}

bool 
rlClanRosGetRanksTask::Configure(const int localGamerIndex,
                                 int pageIndex,
                                 rlClanId clanId,
                                 rlClanRank* resultRanks,
                                 unsigned int maxRanksCount,
                                 unsigned int* resultRanksCount,
                                 unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(resultRanks, catchall, );
        rverify(resultRanksCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultRanks = resultRanks;
        m_MaxRanksCount = maxRanksCount;
        m_ResultRanksCount = resultRanksCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddIntParameter("pageSize", maxRanksCount), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetRanksTask::GetServiceMethod() const
{
    return "Clans.asmx/GetRanks";
}

bool 
rlClanRosGetRanksTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    *m_ResultRanksCount = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Ranks");
        rcheck(results, catchall, rlTaskError("Failed to find <Ranks> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxRanksCount; ++index)
        {
            m_ResultRanks[index].Clear();
        }

        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxRanksCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Rank") != 0) continue;

                resultCode = ProcessExtractRankInfo(record, m_ResultRanks[index]);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultRanksCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetRanksTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultRanksCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetMembershipTaskBase
//////////////////////////////////////////////////////////////////////////
rlClanRosGetMembershipTaskBase::rlClanRosGetMembershipTaskBase()
{
}

rlClanRosGetMembershipTaskBase::~rlClanRosGetMembershipTaskBase()
{
}

bool 
rlClanRosGetMembershipTaskBase::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Memberships");
        rcheck(results, catchall, rlTaskError("Failed to find <Memberships> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "Count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxMembershipsCount; ++index)
        {
            m_ResultMemberships[index].Clear();
        }

        *m_ResultMembershipsCount = 0;
        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxMembershipsCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Membership") != 0) continue;

                resultCode = ProcessMembershipRecord(record, &m_ResultMemberships[index]);

                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultMembershipsCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        *m_ResultMembershipsCount = 0;
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetMembershipTaskBase::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultMembershipsCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetMembershipForTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetMembershipForTask::rlClanRosGetMembershipForTask()
{
}

rlClanRosGetMembershipForTask::~rlClanRosGetMembershipForTask()
{
}

bool 
rlClanRosGetMembershipForTask::Configure(const int localGamerIndex,
                                int pageIndex,
                                const rlGamerHandle& targetGamerHandle,
                                rlClanMembershipData* memberships,
                                unsigned int maxMembershipCount,
                                unsigned int* resultMembershipsCount,
                                unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
		rverify(targetGamerHandle.IsValidForRos(), catchall, );
        rverify(memberships, catchall, );
        rverify(resultMembershipsCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        char gamerName[RL_MAX_GAMER_HANDLE_CHARS];
        rverify(targetGamerHandle.ToString(gamerName), catchall, );

        m_ResultMemberships = memberships;
        m_MaxMembershipsCount = maxMembershipCount;
        m_ResultMembershipsCount = resultMembershipsCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rcheck(cred.IsValid(),
                catchall,
                rlTaskError("Invalid ROS credentials"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );
        rverify(AddIntParameter("pageSize", maxMembershipCount), catchall, );
        rverify(AddStringParameter("targetGamerHandle", gamerName, true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetMembershipForTask::GetServiceMethod() const
{
    return "Clans.asmx/GetMembershipFor";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetMineTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetMineTask::rlClanRosGetMineTask()
: m_State(STATE_CHECKING_CACHED_DATA)
, m_Client(NULL)
, m_Memberships(NULL)
, m_MaxMemberships(0)
, m_NumMemberships(NULL)
, m_TotalMemberships(NULL)
{
}

rlClanRosGetMineTask::~rlClanRosGetMineTask()
{
}

bool 
rlClanRosGetMineTask::Configure(rlClanClient* client,
                                rlClanMembershipData* memberships,
                                unsigned int maxMembershipCount,
                                unsigned int* resultMembershipsCount,
                                unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(memberships, catchall, );
        rverify(resultMembershipsCount, catchall, );

        m_Client = client;
        m_Memberships = memberships;
        m_MaxMemberships = maxMembershipCount;
        m_NumMemberships = resultMembershipsCount;
        m_TotalMemberships = resultTotalCount;

        m_State = STATE_CHECKING_CACHED_DATA;

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

void
rlClanRosGetMineTask::Update(const unsigned timeStep)
{
    rlTaskBase::Update(timeStep);

    if(this->WasCanceled())
    {
        this->Finish(FINISH_CANCELED);
        return;
    }

    switch(m_State)
    {
    case STATE_CHECKING_CACHED_DATA:
        //If a rlClanRosGetMineTask or rlClanRosRefreshMineTask was queued
        //in front of us then it will have refreshed the clan data.
        //Otherwise queue a refresh of our own and wait for it to complete.
        if(m_Client->m_HaveSynchedMemberships)
        {
            m_Client->CopyMembershipsTo(m_Memberships,
                                        m_MaxMemberships,
                                        m_NumMemberships,
                                        m_TotalMemberships);
            this->Finish(FINISH_SUCCEEDED);
        }
        //Queue the task in parallel so it completes while we wait.
        else if(m_Client->RefreshMine(rlClanClient::REFRESH_QUEUE_PARALLEL, &m_MyStatus))
        {
            m_State = STATE_REFRESHING_CACHED_DATA;
        }
        else
        {
            rlTaskError("Error refreshing cached membership data");
            this->Finish(FINISH_FAILED);
        }
        break;
    case STATE_REFRESHING_CACHED_DATA:
        if(m_MyStatus.Succeeded())
        {
            m_Client->CopyMembershipsTo(m_Memberships,
                                        m_MaxMemberships,
                                        m_NumMemberships,
                                        m_TotalMemberships);
            this->Finish(FINISH_SUCCEEDED);
        }
        else if(!m_MyStatus.Pending())
        {
            this->Finish(FINISH_FAILED);
        }
        break;
    }
}

void
rlClanRosGetMineTask::DoCancel()
{
    if(m_MyStatus.Pending())
    {
        rlAssert(STATE_REFRESHING_CACHED_DATA == m_State);
        m_Client->CancelTask(&m_MyStatus);
    }
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosRefreshMineTask
//////////////////////////////////////////////////////////////////////////
rlClanRosRefreshMineTask::rlClanRosRefreshMineTask()
: m_NumMemberships(0)
, m_TotalMemberships(0)
, m_Client(NULL)
{
}

rlClanRosRefreshMineTask::~rlClanRosRefreshMineTask()
{
}

bool 
rlClanRosRefreshMineTask::Configure(rlClanClient* client)
{
    bool success = false;

    rtry
    {
        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(client->GetLocalGamerIndex());

        rcheck(cred.IsValid(),
                catchall,
                rlTaskError("Invalid ROS credentials"));

        rcheck(cred.GetRockstarId() != InvalidRockstarId,
                catchall,
                rlTaskError("Invalid rockstar ID"));

        rverify(rlClanRosGetMembershipTaskBase::Configure(client->GetLocalGamerIndex()), catchall, );
        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageSize", COUNTOF(m_MemberShips)), catchall, );
        rverify(AddIntParameter("pageIndex", 0), catchall, );

        m_ResultMemberships = m_MemberShips;
        m_MaxMembershipsCount = COUNTOF(m_MemberShips);
        m_ResultMembershipsCount = &m_NumMemberships;
        m_ResultTotalCount = &m_TotalMemberships;

        m_Client = client;

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosRefreshMineTask::GetServiceMethod() const
{
    return "Clans.asmx/GetMine";
}

bool
rlClanRosRefreshMineTask::ProcessSuccess(const rlRosResult& result,
                                        const parTreeNode* node,
                                        int& resultCode)
{
    //While this request was in flight we could have been unlinked from
    //SC.  Are we still a member of SC?
    if(!m_Client->CheckCredentials())
    {
        return true;
    }

    if(rlClanRosGetMembershipTaskBase::ProcessSuccess(result, node, resultCode))
    {
        const rlClanId oldPrimaryId = m_Client->GetPrimaryClan().m_Id;

        //For each clan in the returned list check if we should
        //generate a "Joined" event or a "PrimaryChanged" event.
        //These events are triggered if the returned list differs
        //from our cached list.
        bool isNewlyJoined[COUNTOF(m_Client->m_MemberShips)] = {0};

        if(m_Client->m_NumMemberships > 0)
        {
            for(int i = 0; i < (int)m_NumMemberships; ++i)
            {
                isNewlyJoined[i] =
                    !m_Client->HasMembership(m_MemberShips[i].m_Clan.m_Id);
            }
        }

		const int localGamerIndex = m_Client->GetLocalGamerIndex();
		rlClanId newPrimaryClanId = RL_INVALID_CLAN_ID;

        if(m_NumMemberships > 0)
        {
			//For our current list of clans check for clan removal
			//The ClanLeft event will be dispatched in RemoveMembership()
			for(int i=0; i<m_Client->GetNumMemberships(); ++i)
			{
				rlClanId clanId = m_Client->GetClanId(i);

				bool found = false;
				for(int j=0; j<(int)m_NumMemberships && !found; ++j)
				{
					if (clanId == m_MemberShips[j].m_Clan.m_Id)
					{
						found = true;
					}
				}

				if (!found)
				{
					rlTaskDebug("Gamer: %d - Remove Membership %" I64FMT "d", GetLocalGamerIndex(), clanId);

					//Remove clan from our list to keep the local list close to up to date.
					//The ClanLeft event will be dispatched in RemoveMembership()
					rlClan::RemoveMembership(localGamerIndex, clanId, oldPrimaryId);
				}
			}

            for(int i = 0; i < (int)m_NumMemberships; ++i)
            {
				//The ClanJoined event, and possibly the PrimaryClanChanged event,
				//will be dispatched in AddMembership()
                rlClan::AddMembership(localGamerIndex, m_MemberShips[i], isNewlyJoined[i]);

				// Cache the new primary clan ID
				if (m_MemberShips[i].m_IsPrimary)
				{
					newPrimaryClanId = m_MemberShips[i].m_Clan.m_Id;
				}
            }
        }
        else
        {
            rlTaskDebug("Gamer: %d has no clan memberships", GetLocalGamerIndex());
            if(RL_INVALID_CLAN_ID != oldPrimaryId)
            {
                //We thought we had a primary clan, but the server thinks
                //otherwise.
                rlClan::UpdatePrimaryClan(localGamerIndex, oldPrimaryId, RL_INVALID_CLAN_ID);
            }
        }
		
		// If no clans are marked as primary in the client (i.e. a leave was triggered via ScGlobalServices),
		//	but the server knows of a primary clan, update our client with the new primary clan id.
		//	This is placed before the 'm_HaveSyncedMemberships' 
		if (!rlClan::HasPrimaryClan(localGamerIndex) && newPrimaryClanId != RL_INVALID_CLAN_ID)
		{
			rlClan::UpdatePrimaryClan(localGamerIndex, RL_INVALID_CLAN_ID, newPrimaryClanId);
		}

		//Mark that we're received the initialization from the server.
		m_Client->m_HaveSynchedMemberships = true;

        rlAssert(m_TotalMemberships == m_NumMemberships);

        //Send an event indicating we've synchronized with the server.
		rlClanEventSynchedMemberships e(GetLocalGamerIndex());
		rlClan::DispatchEvent(e);

        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetWallMessagesTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetWallMessagesTask::rlClanRosGetWallMessagesTask()
{
}

rlClanRosGetWallMessagesTask::~rlClanRosGetWallMessagesTask()
{
}

bool 
rlClanRosGetWallMessagesTask::Configure(const int localGamerIndex,
                                        int pageIndex,
                                        rlClanId clanId,
                                        rlClanWallMessage* messages,
                                        unsigned int maxMessageCount,
                                        unsigned int* resultMessageCount,
                                        unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(messages, catchall, );
        rverify(resultMessageCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultMessages = messages;
        m_MaxMessageCount = maxMessageCount;
        m_ResultMessageCount = resultMessageCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageSize", maxMessageCount), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetWallMessagesTask::GetServiceMethod() const
{
    return "Clans.asmx/GetWallMessages";
}

bool 
rlClanRosGetWallMessagesTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Messages");
        rcheck(results, catchall, rlTaskError("Failed to find <Messages> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "Count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxMessageCount; ++index)
        {
            m_ResultMessages[index].Clear();
        }

        *m_ResultMessageCount = 0;
        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxMessageCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Message") != 0) continue;

                rlClanWallMessageId messageId;
                rverify(rlHttpTaskHelper::ReadInt64(messageId, record, NULL, "Id"), catchall, );
                m_ResultMessages[index].m_Id = messageId;


                //rverify(rlHttpTaskHelper::ReadString(rankOrder, rank, NULL, "CreatedDate"), catchall, );

                const parTreeNode* messageNode = record->FindChildWithName("Message");
                rcheck(messageNode, catchall, rlTaskError("Failed to find <Messages.Message.Message> element"));
                const char* message = rlHttpTaskHelper::ReadString(messageNode, "Message", NULL);
                rcheck(message, catchall, rlTaskError("Failed to extract <Messages/Message/Message> data"));
                safecpy(m_ResultMessages[index].m_Message, message, RL_CLAN_WALL_MSG_MAX_CHARS);

                const parTreeNode* writer = record->FindChildWithName("Writer");
                rcheck(writer, catchall, rlTaskError("Failed to find <Messages/Message/Writer> element"));

                resultCode = ProcessExtractPlayerInfo(writer, m_ResultMessages[index].m_Writer);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultMessageCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        *m_ResultMessageCount = 0;
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetWallMessagesTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultMessageCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetMetadataForClanTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetMetadataForClanTask::rlClanRosGetMetadataForClanTask()
{
}

rlClanRosGetMetadataForClanTask::~rlClanRosGetMetadataForClanTask()
{
}

bool 
rlClanRosGetMetadataForClanTask::Configure(const int localGamerIndex,
                                        int pageIndex,
                                        rlClanId clanId,
                                        rlClanMetadataEnum* enums,
                                        unsigned int maxEnumCount,
                                        unsigned int* resultEnumCount,
                                        unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rverify(enums, catchall, );
        rverify(resultEnumCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultEnums = enums;
        m_MaxEnumCount = maxEnumCount;
        m_ResultEnumCount = resultEnumCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageSize", maxEnumCount), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetMetadataForClanTask::GetServiceMethod() const
{
    return "Clans.asmx/GetMetadataForClan";
}

bool 
rlClanRosGetMetadataForClanTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("MetadataEnums");
        rcheck(results, catchall, rlTaskError("Failed to find <MetadataEnums> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "Count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxEnumCount; ++index)
        {
            m_ResultEnums[index].Clear();
        }

        *m_ResultEnumCount = 0;
        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxEnumCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "MetadataEnum") != 0) continue;

                rlClanEnumId enumId;
                rverify(rlHttpTaskHelper::ReadInt(enumId, record, NULL, "Id"), catchall, );
                m_ResultEnums[index].m_Id = enumId;


                //rverify(rlHttpTaskHelper::ReadString(rankOrder, rank, NULL, "CreatedDate"), catchall, );

                const char* enumName = rlHttpTaskHelper::ReadString(record, NULL, "EnumName");
                rcheck(enumName, catchall, rlTaskError("Failed to extract <MetadataEnums/MetadataEnum.EnumName> data"));
                safecpy(m_ResultEnums[index].m_EnumName, enumName, RL_CLAN_ENUM_MAX_CHARS);

                const char* setName = rlHttpTaskHelper::ReadString(record, NULL, "SetName");
                rcheck(setName, catchall, rlTaskError("Failed to extract <MetadataEnums/MetadataEnums.SetName> data"));
                safecpy(m_ResultEnums[index].m_SetName, setName, RL_CLAN_ENUM_MAX_CHARS);

                (*m_ResultEnumCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        *m_ResultEnumCount = 0;
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetMetadataForClanTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultEnumCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}
//////////////////////////////////////////////////////////////////////////
// rlClanRosJoinRequestTask 
//////////////////////////////////////////////////////////////////////////
rlClanRosJoinRequestTask::rlClanRosJoinRequestTask()
{
}

rlClanRosJoinRequestTask::~rlClanRosJoinRequestTask()
{
}

bool 
rlClanRosJoinRequestTask::Configure(const int localGamerIndex, rlClanId clanId)
{
    bool success = false;

    rtry
    {
        rcheck(RL_INVALID_CLAN_ID != clanId, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosJoinRequestTask::GetServiceMethod() const
{
    return "Clans.asmx/JoinRequest";
}

bool 
rlClanRosJoinRequestTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}

void 
rlClanRosJoinRequestTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = ConvertRosResultToClanError(result);
}
//////////////////////////////////////////////////////////////////////////
// rlClanRosGetSentJoinRequestsTask 
//////////////////////////////////////////////////////////////////////////
rlClanRosGetSentJoinRequestsTask::rlClanRosGetSentJoinRequestsTask()
    : m_ResultRequests(NULL)
    , m_MaxRequestsCount(0)
    , m_ResultRequestsCount(NULL)
    , m_ResultTotalCount(NULL)
{
}

rlClanRosGetSentJoinRequestsTask::~rlClanRosGetSentJoinRequestsTask()
{
}

bool 
rlClanRosGetSentJoinRequestsTask::Configure(const int localGamerIndex,
        int pageIndex,
        rlClanJoinRequestSent* requests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rcheck(requests, catchall, );
        rcheck(resultRequestsCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultRequests = requests;
        m_MaxRequestsCount = maxRequestsCount;
        m_ResultRequestsCount = resultRequestsCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );
        rverify(AddIntParameter("pageSize", maxRequestsCount), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetSentJoinRequestsTask::GetServiceMethod() const
{
    return "Clans.asmx/GetSentJoinRequests";
}

bool 
rlClanRosGetSentJoinRequestsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    *m_ResultRequestsCount = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("JoinRequests");
        rcheck(results, catchall, rlTaskError("Failed to find <JoinRequests> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxRequestsCount; ++index)
        {
            m_ResultRequests[index].Clear();
        }

        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxRequestsCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "JoinRequest") != 0) continue;

                rlClanRequestId requestId;
                rverify(rlHttpTaskHelper::ReadInt64(requestId, record, NULL, "Id"), catchall, );
                m_ResultRequests[index].m_RequestId = (rlClanRequestId)requestId;

                const parTreeNode* clanDesc = record->FindChildWithName("Clan");
                rcheck(clanDesc, catchall, rlTaskError("Failed to find <Clan> element"));

                resultCode = ProcessExtractClanInfo(clanDesc, m_ResultRequests[index].m_Clan);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultRequestsCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }

}

void 
rlClanRosGetSentJoinRequestsTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = ConvertRosResultToClanError(result);
}
//////////////////////////////////////////////////////////////////////////
// rlClanRosGetRecievedJoinRequestsTask 
//////////////////////////////////////////////////////////////////////////
rlClanRosGetRecievedJoinRequestsTask::rlClanRosGetRecievedJoinRequestsTask()
    : m_ResultRequests(NULL)
    , m_MaxRequestsCount(0)
    , m_ResultRequestsCount(NULL)
    , m_ResultTotalCount(NULL)
{
}

rlClanRosGetRecievedJoinRequestsTask::~rlClanRosGetRecievedJoinRequestsTask()
{
}

bool 
rlClanRosGetRecievedJoinRequestsTask::Configure(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanJoinRequestRecieved* requests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount)
{
    bool success = false;

    rtry
    {
        rcheck(RL_INVALID_CLAN_ID != clanId, catchall, );
        rcheck(requests, catchall, );
        rcheck(resultRequestsCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultRequests = requests;
        m_MaxRequestsCount = maxRequestsCount;
        m_ResultRequestsCount = resultRequestsCount;
        m_ResultTotalCount = resultTotalCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageIndex", pageIndex), catchall, );
        rverify(AddIntParameter("pageSize", maxRequestsCount), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetRecievedJoinRequestsTask::GetServiceMethod() const
{
    return "Clans.asmx/GetRecievedJoinRequests";
}

bool 
rlClanRosGetRecievedJoinRequestsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = RL_CLAN_SUCCESS;
    *m_ResultRequestsCount = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("JoinRequests");
        rcheck(results, catchall, rlTaskError("Failed to find <JoinRequests> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

        if (NULL != m_ResultTotalCount)
        {
            int maxCount;
            rverify(rlHttpTaskHelper::ReadInt(maxCount, results, NULL, "MaxCount"), catchall, );
            *m_ResultTotalCount = maxCount;
        }

        unsigned int index;
        for (index = 0; index < m_MaxRequestsCount; ++index)
        {
            m_ResultRequests[index].Clear();
        }

        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxRequestsCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "JoinRequest") != 0) continue;

                rlClanRequestId requestId;
                rverify(rlHttpTaskHelper::ReadInt64(requestId, record, NULL, "Id"), catchall, );
                m_ResultRequests[index].m_RequestId = (rlClanRequestId)requestId;

                const parTreeNode* inviterDesc = record->FindChildWithName("Player");
                rcheck(inviterDesc, catchall, rlTaskError("Failed to find <Player> element"));

                resultCode = ProcessExtractPlayerInfo(inviterDesc, m_ResultRequests[index].m_Player);
                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultRequestsCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }

}

void 
rlClanRosGetRecievedJoinRequestsTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = ConvertRosResultToClanError(result);
}
//////////////////////////////////////////////////////////////////////////
// rlClanRosDeleteJoinRequestTask 
//////////////////////////////////////////////////////////////////////////
rlClanRosDeleteJoinRequestTask::rlClanRosDeleteJoinRequestTask()
{
}

rlClanRosDeleteJoinRequestTask::~rlClanRosDeleteJoinRequestTask()
{
}

bool 
rlClanRosDeleteJoinRequestTask::Configure(const int localGamerIndex,
        rlClanRequestId requestId)
{
    bool success = false;

    rtry
    {
        rcheck(RL_INVALID_REQUEST_ID != requestId, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("id", requestId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosDeleteJoinRequestTask::GetServiceMethod() const
{
    return "Clans.asmx/DeleteJoinRequest";
}

bool 
rlClanRosDeleteJoinRequestTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = 0;
    return true;
}
void 
rlClanRosDeleteJoinRequestTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    resultCode = ConvertRosResultToClanError(result);
}
//////////////////////////////////////////////////////////////////////////
// rlClanRosGetClanEmblemTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetClanEmblemTask::rlClanRosGetClanEmblemTask()
: m_State(STATE_NONE)
// , m_fiHandle(fiHandleInvalid)
// , m_fiDevice(NULL)
{
	m_MyStatus.Reset();	
}

rlClanRosGetClanEmblemTask::~rlClanRosGetClanEmblemTask()
{

}

const char* rlClanRosGetClanEmblemTask::GetCrewEmblemCloudPath( rlClanEmblemSize emblemSize, char* emblemPath, const unsigned int strLen )
{
	static const char* emblemSizeStrs[] = { "32", "64", "128", "256", "1024" };
	CompileTimeAssert(COUNTOF(emblemSizeStrs) == RL_CLAN_EMBLEM_NUM_SIZES);	

	return formatf(emblemPath, strLen, "publish/emblem/emblem_%s.dds", emblemSizeStrs[emblemSize] );
}

bool
rlClanRosGetClanEmblemTask::Configure( const int localGamerIndex, rlClanId clanId, rlClanEmblemSize emblemSize, const u64 ifModifiedSincePosixTime,const fiDevice* responseDevice, const fiHandle responseHandle )
{
	bool success = false;
	rtry
	{
		rverify(!this->IsPending(),catchall,);

		rlCloudMemberId crewCloudId(clanId);
		rverify(crewCloudId.IsValid(),catchall,);

		
		char emblemPath[64];
		GetCrewEmblemCloudPath(emblemSize, emblemPath, sizeof(emblemPath));
				
		rlTaskDebug2("Requesting emblem for clan %" I64FMT "d from %s", clanId, emblemPath);

		rverify(rlCloud::GetCrewFile(	localGamerIndex, 
										crewCloudId, 
										RL_CLOUD_ONLINE_SERVICE_SC, 
										emblemPath,
                                        RLROS_SECURITY_DEFAULT, 
                                        ifModifiedSincePosixTime,
                                        NET_HTTP_OPTIONS_NONE,
										responseDevice, 
										responseHandle,
                                        NULL,   //rlCloudFileInfo
                                        NULL,   //allocator
										&m_MyStatus),
				catchall,
				rlTaskError("Error requested clan emblem"));

		m_State = STATE_RECEIVING;

		success = true;

	}
	rcatchall
	{

	}

	return success;
}

void
rlClanRosGetClanEmblemTask::DoCancel()
{
	rlCloud::Cancel(&m_MyStatus);
}

void
rlClanRosGetClanEmblemTask::Start()
{
	rlTaskDebug2("Start");

	rlTaskBase::Start();
}

void
rlClanRosGetClanEmblemTask::Update( const unsigned timeStepMs )
{
	if(!this->IsActive())
	{
		return;
	}

	rlTaskBase::Update(timeStepMs);

	if(WasCanceled())
	{
        if(m_MyStatus.Pending())
        {
            rlGetTaskManager()->CancelTask(&m_MyStatus);
        }
		this->Finish(FINISH_FAILED, -1);
	}
	else
	{
		switch(m_State)
		{
		case STATE_NONE:
			break;
		case STATE_RECEIVING:
			if(m_MyStatus.Succeeded())
			{
				this->Finish(FINISH_SUCCEEDED, m_MyStatus.GetResultCode());
				m_State = STATE_DONE;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_ERROR;
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;
		case STATE_DONE:
			break;
		case STATE_ERROR:
			break;

		}
	}
}

void
rlClanRosGetClanEmblemTask::Finish( const FinishType finishType, const int resultCode /*= 0*/ )
{
	rlTaskDebug2("Finish");
	if(m_MyStatus.Pending())
	{
		rlCloud::Cancel(&m_MyStatus);
	}

	rlTaskBase::Finish(finishType, resultCode);
}



//////////////////////////////////////////////////////////////////////////
// rlClanRosGetFeudTaskBase
//////////////////////////////////////////////////////////////////////////
rlClanRosGetFeudTaskBase::rlClanRosGetFeudTaskBase()
{
}

rlClanRosGetFeudTaskBase::~rlClanRosGetFeudTaskBase()
{
}

const char*
rlClanRosGetFeudTaskBase::GetSinceString(rlClanFeudSinceCode since)
{
    static const char *feudStrings[] =
    { "lastday", "lastweek", "last2weeks", "lastmonth", "last3months", "last6months", "alltime" };

    if (since < RL_CLAN_FEUD_SINCE_LAST_DAY) return NULL;
    if (since >= RL_CLAN_FEUD_SINCE_NUM_CODES) return NULL;

    return feudStrings[(int)since];
}

int 
rlClanRosGetFeudTaskBase::ProcessFeuder(const parTreeNode* record, rlClanFeuder& feuder)
{
    rtry
    {
        rlClanId clanId;
        rverify(rlHttpTaskHelper::ReadInt64(clanId, record, NULL, "Id"), catchall, );
        feuder.m_ClanId = (rlClanId)clanId;

        const char* clanName = rlHttpTaskHelper::ReadString(record, NULL, "Name");
        rcheck(clanName, catchall, rlTaskError("Failed to find <Clan.Name> attribute"));
        safecpy(feuder.m_ClanName, clanName, RL_CLAN_NAME_MAX_CHARS);

        const char* clanTag = rlHttpTaskHelper::ReadString(record, NULL, "Tag");
        rcheck(clanTag, catchall, rlTaskError("Failed to find <Clan.Tag> attribute"));
        safecpy(feuder.m_ClanTag, clanTag, RL_CLAN_TAG_MAX_CHARS);

        int firstFeudPosix;
        rverify(rlHttpTaskHelper::ReadInt(firstFeudPosix, record, NULL, "FirstFeudPosix"), catchall, );
        feuder.m_FirstFeudPosix = firstFeudPosix;

        int lastFeudPosix;
        rverify(rlHttpTaskHelper::ReadInt(lastFeudPosix, record, NULL, "LastFeudPosix"), catchall, );
        feuder.m_LastFeudPosix = lastFeudPosix;

        int totalFeuds;
        rverify(rlHttpTaskHelper::ReadInt(totalFeuds, record, NULL, "TotalFeuds"), catchall, );
        feuder.m_TotalFeuds = totalFeuds;

        int wins;
        rverify(rlHttpTaskHelper::ReadInt(wins, record, NULL, "Wins"), catchall, );
        feuder.m_Wins = wins;

        int loses;
        rverify(rlHttpTaskHelper::ReadInt(loses, record, NULL, "Losses"), catchall, );
        feuder.m_Loses = loses;

        int kills;
        rverify(rlHttpTaskHelper::ReadInt(kills, record, NULL, "Kills"), catchall, );
        feuder.m_Kills = kills;

        int deaths;
        rverify(rlHttpTaskHelper::ReadInt(deaths, record, NULL, "Deaths"), catchall, );
        feuder.m_Deaths = deaths;

        int lastSessionId;
        rverify(rlHttpTaskHelper::ReadInt(lastSessionId, record, NULL, "LastSessionId"), catchall, );
        feuder.m_LastSessionId = lastSessionId;

        int lastMatchId;
        rverify(rlHttpTaskHelper::ReadInt(lastMatchId, record, NULL, "LastMatchId"), catchall, );
        feuder.m_LastMatchId = lastMatchId;

        return RL_CLAN_SUCCESS;
    }
    rcatchall
    {
    }

    return RL_CLAN_ERR_PARSER_FAILED;
}

bool 
rlClanRosGetFeudTaskBase::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* results = node->FindChildWithName("Result");
        rcheck(results, catchall, rlTaskError("Failed to find <Result> element"));

        int count;
        rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "Count"), catchall, );

        unsigned int index;
        for (index = 0; index < m_MaxFeuderCount; ++index)
        {
            m_ResultFeuders[index].Clear();
        }

        *m_ResultFeuderCount = 0;
        if (0 != count)
        {
            parTreeNode::ChildNodeIterator iter = results->BeginChildren();

            index = 0;
            for( ;iter != results->EndChildren() && index < m_MaxFeuderCount; ++iter, ++index)
            {
                parTreeNode* record = *iter;
                rlAssert(record);

                if(stricmp(record->GetElement().GetName(), "Feuder") != 0) continue;

                resultCode = ProcessFeuder(record, m_ResultFeuders[index]);

                rverify(RL_CLAN_SUCCESS == resultCode, catchall, );

                (*m_ResultFeuderCount)++;
            }
        }

        return true;
    }
    rcatchall
    {
        *m_ResultFeuderCount = 0;
        if (RL_CLAN_SUCCESS == resultCode)
        {
            resultCode = RL_CLAN_ERR_PARSER_FAILED;
        }
        return false;
    }
}

void 
rlClanRosGetFeudTaskBase::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
    *m_ResultFeuderCount = 0;
    resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetFeudStatsTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetFeudStatsTask::rlClanRosGetFeudStatsTask()
{
}

rlClanRosGetFeudStatsTask::~rlClanRosGetFeudStatsTask()
{
}

bool 
rlClanRosGetFeudStatsTask::Configure(const int localGamerIndex,
        const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount)
{
    bool success = false;

    rtry
    {
        rverify(resultFeuders, catchall, );
        rverify(resultFeuderCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultFeuders = resultFeuders;
        m_MaxFeuderCount = maxFeuderCount;
        m_ResultFeuderCount = resultFeuderCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rcheck(cred.IsValid(),
                catchall,
                rlTaskError("Invalid ROS credentials"));

        rcheck(cred.GetRockstarId() != InvalidRockstarId,
                catchall,
                rlTaskError("Invalid rockstar ID"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddStringParameter("since", GetSinceString(since), true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetFeudStatsTask::GetServiceMethod() const
{
    return "Clans.asmx/GetFeudStats";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetTopRivalsTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetTopRivalsTask::rlClanRosGetTopRivalsTask()
{
}

rlClanRosGetTopRivalsTask::~rlClanRosGetTopRivalsTask()
{
}

bool 
rlClanRosGetTopRivalsTask::Configure(const int localGamerIndex,
        const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount)
{
    bool success = false;

    rtry
    {
        rverify(resultFeuders, catchall, );
        rverify(resultFeuderCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultFeuders = resultFeuders;
        m_MaxFeuderCount = maxFeuderCount;
        m_ResultFeuderCount = resultFeuderCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rcheck(cred.IsValid(),
                catchall,
                rlTaskError("Invalid ROS credentials"));

        rcheck(cred.GetRockstarId() != InvalidRockstarId,
                catchall,
                rlTaskError("Invalid rockstar ID"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("clanId", clanId), catchall, );
        rverify(AddStringParameter("since", GetSinceString(since), true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetTopRivalsTask::GetServiceMethod() const
{
    return "Clans.asmx/GetTopRivals";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetTopFeudersTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetTopFeudersTask::rlClanRosGetTopFeudersTask()
{
}

rlClanRosGetTopFeudersTask::~rlClanRosGetTopFeudersTask()
{
}

bool 
rlClanRosGetTopFeudersTask::Configure(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount)
{
    bool success = false;

    rtry
    {
        rverify(resultFeuders, catchall, );
        rverify(resultFeuderCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultFeuders = resultFeuders;
        m_MaxFeuderCount = maxFeuderCount;
        m_ResultFeuderCount = resultFeuderCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rcheck(cred.IsValid(),
                catchall,
                rlTaskError("Invalid ROS credentials"));

        rcheck(cred.GetRockstarId() != InvalidRockstarId,
                catchall,
                rlTaskError("Invalid rockstar ID"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("since", GetSinceString(since), true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetTopFeudersTask::GetServiceMethod() const
{
    return "Clans.asmx/GetTopFeuders";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetTopFeudWinnersTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetTopFeudWinnersTask::rlClanRosGetTopFeudWinnersTask()
{
}

rlClanRosGetTopFeudWinnersTask::~rlClanRosGetTopFeudWinnersTask()
{
}

bool 
rlClanRosGetTopFeudWinnersTask::Configure(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount)
{
    bool success = false;

    rtry
    {
        rverify(resultFeuders, catchall, );
        rverify(resultFeuderCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultFeuders = resultFeuders;
        m_MaxFeuderCount = maxFeuderCount;
        m_ResultFeuderCount = resultFeuderCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rcheck(cred.IsValid(),
                catchall,
                rlTaskError("Invalid ROS credentials"));

        rcheck(cred.GetRockstarId() != InvalidRockstarId,
                catchall,
                rlTaskError("Invalid rockstar ID"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("since", GetSinceString(since), true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetTopFeudWinnersTask::GetServiceMethod() const
{
    return "Clans.asmx/GetTopFeudWinners";
}

//////////////////////////////////////////////////////////////////////////
// rlClanRosGetTopFeudLosersTask
//////////////////////////////////////////////////////////////////////////
rlClanRosGetTopFeudLosersTask::rlClanRosGetTopFeudLosersTask()
{
}

rlClanRosGetTopFeudLosersTask::~rlClanRosGetTopFeudLosersTask()
{
}

bool 
rlClanRosGetTopFeudLosersTask::Configure(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount)
{
    bool success = false;

    rtry
    {
        rverify(resultFeuders, catchall, );
        rverify(resultFeuderCount, catchall, );
        rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

        m_ResultFeuders = resultFeuders;
        m_MaxFeuderCount = maxFeuderCount;
        m_ResultFeuderCount = resultFeuderCount;

        // Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

        rcheck(cred.IsValid(),
                catchall,
                rlTaskError("Invalid ROS credentials"));

        rcheck(cred.GetRockstarId() != InvalidRockstarId,
                catchall,
                rlTaskError("Invalid rockstar ID"));

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("since", GetSinceString(since), true), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

const char* 
rlClanRosGetTopFeudLosersTask::GetServiceMethod() const
{
    return "Clans.asmx/GetTopFeudLosers";
}

}   //namespace rage
