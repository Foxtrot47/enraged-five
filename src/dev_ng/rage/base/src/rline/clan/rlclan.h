// 
// rline/rlclan.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLCLAN_H
#define RLINE_RLCLAN_H

#include "file/handle.h"
#include "rlclancommon.h"
#include "rline\rltask.h"
#include "atl\delegate.h"
#include "file\handle.h"
#include "net\status.h"

namespace rage
{
class fiDevice;

//PURPOSE
//  "Clans" are groups of players which are tracked by the ROS backend.  Clans are
//   similar to friends but provide some unique differences:
//   - They are title specific.
//   - A clan can have hundred of members.
//   - A player can be in many clans.
//   - ROS clans provide players with some organizational tools.
//
//USAGE
//  A clan is set of players referred to as members.  Each member of a clan will
//  have a clan specific rank.  The rank defines the permissions any associated
//  members have.  The permissions define which operations can be performed.
//  
//  A clan can be a system clan, a open clan, both, or neither.  A open clan is a
//  clan that can be joined by any player without an invite. A system clan is a
//  clan which was created by the system.  A title can use these to help foster
//  a community by automatically placing players into open system clans.
//  
//  Clan functionality can be broken into 3 basic groups: member management, 
//  rank management, and data retrieval.  Member management provides players 
//  with tools to manipulate who is a member of a clan and the existence of the 
//  clan.  Rank management allows players to manipulate the clan specific ranks 
//  provided to various members of the clan.  Data retrieval are the set of 
//  methods for retrieving any clan data.  Members must have the proper 
//  permission to access any methods other than the data retrieval methods.
//  
//
//  To integrated clans into your title, do the following:
//
//  1. During initializatio, call rlClan::Init().
//  2. Call rlClan::Update() each frame.
//  3. Call any methods which seem appropriate.
//  4. Call rlClan::Shutdown() when shutting down networking.
//
//
//  The suggested startup process is:
//
//  1. call GetMine()
//  1.a. If no clans are found:
//  1.a.1. GetOpenClans()
//  1.a.2. Join a random open clan.
//  1.a.3. Use this clan as the active clan.
//  1.b. If any clans found:
//  1.b.1. Provide a mechanism for the user to distinguish one of these clans
//         as their active clan.
//  2. Use the active clan for all dealings that require a clan.  (displaying
//     the tag/logo in matches, etc.)
//  3. call GetInvites() 
//  3.a. Notify the user of any invitations
//  4. call GetSentInvites()
//  4.a. Notify the user of any remaining unprocessed invitations
//
class rlClan
{
    typedef atDelegator<void (const rlClanEvent&)> Delegator;

    //Listens for presence events.
    //Friended by rlClan so it can force cache refreshes.
    friend class rlClanPresenceEventListener;

public:
    //PURPOSE
    //  Delegate for handling events.
    //  See rlclancommon.h for event types.
    typedef Delegator::Delegate Delegate;

    //PURPOSE
    //  Creates the name of message channel on which clan presence
    //  messages are published.
    static const char* CreateMessageChannelName(char* channel,
                                                const unsigned maxLen,
                                                const rlClanId clanId);
    template<int SIZE>
    static const char* CreateMessageChannelName(char (&channel)[SIZE],
                                                const rlClanId clanId)
    {
        return CreateMessageChannelName(channel, SIZE, clanId);
    }


    //PURPOSE
    //  Add or remove a delegate that will be called with event notifications.
    static void AddDelegate(Delegate* dlgt);
    static void RemoveDelegate(Delegate* dlgt);

    //PURPOSE
    //  Init/Update/Shutdown
    static bool Init();
    static bool IsInitialized();
    static void Shutdown();
    static void Update();

    //PURPOSE
    //  Cancels a pending operation.
    static void Cancel(netStatus* status);

    //PURPOSE
    //  Returns true if the service is ready for use and 
    //  has gathered the local gamer's clan information.
    static bool IsServiceReady(const int localGamerIndex);

	//PURPOSE
	// Returns true if the service is ready AND the gamer has
	// access to higher level clan functionality, for which a 
	// SC account is required.
	static bool IsFullServiceReady(const int localGamerIndex);

    static bool HasPrimaryClan(const int localGamerIndex);

    static const rlClanDesc& GetPrimaryClan(const int localGamerIndex);

    static const rlClanMembershipData& GetPrimaryMembership(const int localGamerIndex);

    //PURPOSE
    //  Requests a list of clan memberships for the local gamer.
    //  Results will be stored in the given results objects.
    //PARAMS
    //  resultMemberships       - A caller provided array for storing results.
    //  maxMembershipsCount     - The maximum size of the result array.
    //  resultMembershipsCount  - The number of results which were returned.
    //  resultTotalCount        - Optional.  Total number of results available
    static bool GetMine(const int localGamerIndex,
        rlClanMembershipData* resultMemberships,
        unsigned int maxMembershipsCount,
        unsigned int* resultMembershipsCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests that a clan be created with the local gamer as the clan leader.
    //PARAMS
    //  clanName - The desired clan name for the new clan.  Uniqueness is enforced.
    //  clanTag - The desired 4 or less character tag for the new clan.
    static bool Create(const int localGamerIndex, 
        const char* clanName,
        const char* clanTag,
        bool isOpenClan,
        netStatus* status);

    //PURPOSE
    //  Requests that the local gamer be removed from the specified clan.  
    //  If the leaving gamer was the clan leader then promotes the most 
    //  senior high ranking clan member to clan leader.
    static bool Leave(const int localGamerIndex, 
        rlClanId clanId,
        netStatus* status);

    //PURPOSE
    //  Marks the requested clan as the primary clan for the local gamer.
    static bool SetPrimaryClan(const int localGamerIndex, 
        rlClanId clanId,
        netStatus* status);

    //PURPOSE
    //  Requests that an invitation be sent to the identified gamer
    //  if the local gamer has permission.  Also allows a rank be specified
    //  for the invited gamer.
    //PARAMS
    //  clanId - The clan to invite into.
    //  rankOrder - The desired rank order of the new clan member.
    //  expireExtraInvitations - If the maximum sent invitations limit is reached and this is true then extra invitations will be removed(expired).
    //  gamer - The gamer being invited.
    static bool Invite(const int localGamerIndex, 
        rlClanId clanId,
        int rankOrder,
        bool expireExtraInvitations,
        const rlGamerHandle& gamer,
        netStatus* status);

    //PURPOSE
    //  Requests that the identified invitation be deleted.
    static bool DeleteInvite(const int localGamerIndex, 
        rlClanInviteId inviteId,
        netStatus* status);

    //PURPOSE
    //  Requests that the local gamer be joined to the specified clan
    //  if they have the right.  They have the right to join if the clan
    //  is an open clan or if they have been sent an invitation.
    static bool Join(const int localGamerIndex, 
        rlClanId clanId,
        netStatus* status);

    //PURPOSE
    //  Requests that the specified gamer be removed from the specified clan.  
    //  The local gamer must have kick permission.   If the leaving gamer was 
    //  the clan leader then promotes the most senior high ranking clan member 
    //  to clan leader.
    static bool Kick(const int localGamerIndex, 
        rlClanId clanId,
        const rlGamerHandle& gamer,
        netStatus* status);

    //PURPOSE
    //  Requests that a rank is created for the specified clan if the local
    //  gamer has permission. The rank will be created with the given name
    //  and initial flags. The title flags have no semantics on the server 
    //  side and can be changed as desired.
    static bool RankCreate(const int localGamerIndex, 
        rlClanId clanId,
        const char* rankName,
        s64 initialSystemFlags,
        netStatus* status);

    //PURPOSE
    //  Requests that the specified rank be deleted if the local gamer
    //  has permission.  A rank cannot be deleted if there are any members
    //  with that rank.
    static bool RankDelete(const int localGamerIndex, 
        rlClanId clanId,
        rlClanRankId rankId,
        netStatus* status);

    //PURPOSE
    //  Requests that aspects of the specified rank be modified if the 
    //  local gamer has permission.
    //PARAMS
    //  clanId - The clan which owns the specified rank.
    //  rankId - The rank to update.
    //  adjustRankOrder - The direction to move this rank.  A value of 0 means no change.  
    //          A negative number switches the rank order with the rank less than it.  
    //          A positive number switches the rank order with the rank greater than it.
    //  rankName - A new name to give the rank.  A value of 0 means no change.
    //  updateSystemFlags - The new value for the system flags. A  negative value means no change.
    //NOTE
    //  A Rank Order of 0 is the leader rank.  Ranks with higher rank orders are considered
    //  worse than the leader rank. It is impossible to make a change that will result in 
    //  the leader rank (rank 0) having system flags other than 0x7FFFFFFFFFFFFFFF.  
    //  This will assure the leader always has full permission.
    static bool RankUpdate(const int localGamerIndex, 
        rlClanId clanId,
        rlClanRankId rankId,
        int adjustRankOrder,
        const char* rankName,
        s64 updateSystemFlags,
        netStatus* status);

    //PURPOSE
    //  Requests that a specific wall message be deleted if the local
    //  gamer has permission or was the writer.
    //PARAMS
    //  clanId - The clan which owns the specified message.
    //  msgId - The message to delete.
    static bool DeleteWallMessage(const int localGamerIndex,
        rlClanId clanId,
        rlClanWallMessageId msgId,
        netStatus* status);

    //PURPOSE
    //  Adds a specific message to the clans wall if the local
    //  gamer has permission.
    //PARAMS
    //  clanId - The clan which will own the new message.
    //  message - The text of the message.
    static bool WriteWallMessage(const int localGamerIndex,
        rlClanId clanId,
        const char* message,
        netStatus* status);

    //PURPOSE
    //  Requests that a specified gamer be promoted/demoted within the given clan.
    //  The local gamer must have permission to promote/demote.  Promote will decrease
    //  the rank order of the specified gamer.  Demote will increase the rank order of
    //  the specified gamer.
    //PARAMS
    //  gamer - The gamer to adjust the rank of.
    //  clanId - The clan association for which the rank should be adjusted.
    //  promote - True for promotion. False for demotion.
    static bool MemberUpdateRankId(const int localGamerIndex,
        const rlGamerHandle& gamer,
        rlClanId clanId,
        bool promote,
        netStatus* status);

    //PURPOSE
    //  Requests a paged list of invitations received by the local gamer.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  resultInvites - A caller provided array for storing results.
    //  maxInvitesCount - The maximum size of the result array.
    //  resultInvitesCount - The number of results which were returned.
    static bool GetInvites(const int localGamerIndex, 
        int pageIndex,
        rlClanInvite* resultInvites,
        unsigned int maxInvitesCount,
        unsigned int* resultInvitesCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests a paged list of invitations sent the local gamer.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  resultInvites - A caller provided array for storing results.
    //  maxInvitesCount - The maximum size of the result array.
    //  resultInvitesCount - The number of results which were returned.
    static bool GetSentInvites(const int localGamerIndex, 
        int pageIndex,
        rlClanInvite* resultInvites,
        unsigned int maxInvitesCount,
        unsigned int* resultInvitesCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests a paged list of all clans.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  resultClans - A caller provided array for storing results.
    //  maxClansCount - The maximum size of the result array.
    //  resultClansCount - The number of results which were returned.
    static bool GetAll(const int localGamerIndex, 
        int pageIndex,
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests a paged list of all clans that have the provided string in thier name.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanName - The name string to search for clan memberships for.
    //  resultClans - A caller provided array for storing results.
    //  maxClansCount - The maximum size of the result array.
    //  resultClansCount - The number of results which were returned.
    static bool GetAllByClanName(const int localGamerIndex, 
        int pageIndex,
        const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PARAMS
    //  Requests a paged list of all clans which are system clans.
    //  pageIndex - The page ID of the results to retrieve.
    //  resultClans - A caller provided array for storing results.
    //  maxClansCount - The maximum size of the result array.
    //  resultClansCount - The number of results which were returned.
    static bool GetSystemClans(const int localGamerIndex, 
        int pageIndex,
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PARAMS
    //  Requests a paged list of all clans which are currently flagged open clans.
    //  pageIndex - The page ID of the results to retrieve.
    //  resultClans - A caller provided array for storing results.
    //  maxClansCount - The maximum size of the result array.
    //  resultClansCount - The number of results which were returned.
    static bool GetOpenClans(const int localGamerIndex, 
		int pageIndex,
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests the description (name, tag) for a provided clan id.
    static bool GetDesc(const int localGamerIndex, 
        const rlClanId clan,
        rlClanDesc* resultClanDesc,
        netStatus* status);

    //PURPOSE
    //  Requests the description (name, tag) for a provided set of clan ids.
    static bool GetDescs(const int localGamerIndex, 
        const rlClanId* clans,
        unsigned int clansCount,
        rlClanDesc* resultClans,
        netStatus* status);

    //PURPOSE
    //  Requests the leaders for the provided set of clan ids.
    //  NOTE: Not all clans have leaders.
    //PARAMS
    //  clans - The set of clan IDs to lookup.
    //  clansCount - The number of clans to lookup.
    //  resultLeaders - A caller provided array for storing results.
    //  resultCount - The number of results which were returned.
    static bool GetLeadersForClans(const int localGamerIndex, 
        const rlClanId* clans,
        unsigned int clansCount,
        rlClanLeader* resultLeaders,
        unsigned int* resultCount,
        netStatus* status);

    //PURPOSE
    //  Requests the list of clan member structs for the primary clans
    //  of the requested gamer list.
    //PARAMS
    //  gamers - The gamer list to request clans for.
    //  numGamers - The number of gmaers to lookup.
    //  resultGamers - A caller provided array for storing results.
    //  resultGamersCount - The number of results which were returned.
    //NOTE
    //  The clan that is returned follows these rules:
    //  * If the player has a set primary clan then that is returned.
    //  * Else if the player is a member of a private clan then the 
    //    first private clan the player is a member of is returned.
    //  * Else the first clan the player is a member of is returned.
    static bool GetPrimaryClans(const int localGamerIndex,
        const rlGamerHandle* gamer,
        unsigned int numGamers,
        rlClanMember* resultGamers,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests the list of clan members for a provided clan id 
    //  and returns only players that have played this title.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanId - The clan ID to search for clan memberships for.
    //  resultGamers - A caller provided array for storing results.
    //  maxGamersCount - The maximum size of the result array.
    //  resultGamersCount - The number of results which were returned.
    static bool GetMembersTitleOnly(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanMember* resultGamers,
        unsigned int maxGamersCount,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests the list of clan members for a provided clan id.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanId - The clan ID to search for clan memberships for.
    //  resultGamers - A caller provided array for storing results.
    //  maxGamersCount - The maximum size of the result array.
    //  resultGamersCount - The number of results which were returned.
    static bool GetMembersByClanId(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanMember* resultGamers,
        unsigned int maxGamersCount,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests the list of clan members for a provided clan name.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanName - The name string to search for clan memberships for.
    //  resultGamers - A caller provided array for storing results.
    //  maxGamersCount - The maximum size of the result array.
    //  resultGamersCount - The number of results which were returned.
    static bool GetMembersByClanName(const int localGamerIndex,
        int pageIndex,
        const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],
        rlClanMember* resultGamers,
        unsigned int maxGamersCount,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests the list of ranks for a specific clan.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanId - The id of the clan to get ranks for.
    //  resultRanks - A caller provided array for storing results.
    //  maxRanksCount - The maximum size of the result array.
    //  resultRanksCount - The number of results which were returned.
    static bool GetRanks(const int localGamerIndex, 
        int pageIndex,
        rlClanId clanId,
        rlClanRank* resultRanks,
        unsigned int maxRanksCount,
        unsigned int* resultRanksCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests the list of wall messages for a specific clan.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanId - The id of the clan to get messages for.
    //  resultMessages - A caller provided array for storing results.
    //  maxMessageCount - The maximum size of the result array.
    //  resultMessageCount - The number of results which were returned.
    //NOTE
    //  The most recent messages will be returned first.  This means the 'id' values
    //  will be in descending order not ascending order like all other get calls.
    static bool GetWallMessages(const int localgamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanWallMessage* resultMessages,
        unsigned int maxMessageCount,
        unsigned int* resultMessageCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests the list of metadata enums for a specific clan.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanId - The id of the clan to get messages for.
    //  resultEnums - A caller provided array for storing results.
    //  maxEnumCount - The maximum size of the result array.
    //  resultEnumCount - The number of results which were returned.
    static bool GetMetadataForClan(const int localgamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanMetadataEnum* resultEnums,
        unsigned int maxEnumCount,
        unsigned int* resultEnumCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Requests a paged list of clan memberships for the specified gamer.
    //  Results will be stored in the given results objects.
    //PARAMS
    //  pageIndex               - The page ID of the results to retrieve.
    //  gamer                   - The gamer to lookup.
    //  resultMemberships       - A caller provided array for storing results.
    //  maxMembershipsCount     - The maximum size of the result array.
    //  resultMembershipsCount  - The number of results which were returned.
    //  resultTotalCount        - Total number of results available.
    static bool GetMembershipFor(const int localGamerIndex,
        int pageIndex,
        const rlGamerHandle& gamer,
        rlClanMembershipData* resultMemberships,
        unsigned int maxMembershipsCount,
        unsigned int* resultMembershipsCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Creates a Join Request for the local gamer to the provided clan.
    //PARAMS
    //  clanId - The id of the clan to request to join.
    static bool JoinRequest(const int localGamerIndex,
        rlClanId clanId,
        netStatus* status);

    //PURPOSE
    //  Retrieves the sent requests associated for the local gamer.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  resultRequests - A caller provided array for storing results.
    //  maxRequestsCount - The maximum size of the result array.
    //  resultRequestsCount - The number of results which were returned.
    static bool GetSentJoinRequests(const int localGamerIndex,
        int pageIndex,
        rlClanJoinRequestSent* resultRequests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Retrieves the requests associated with the specified clan.
    //PARAMS
    //  pageIndex - The page ID of the results to retrieve.
    //  clanId - The id of the clan to get requests for.
    //  resultRequests - A caller provided array for storing results.
    //  maxRequestsCount - The maximum size of the result array.
    //  resultRequestsCount - The number of results which were returned.
    static bool GetRecievedJoinRequests(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanJoinRequestRecieved* resultRequests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    //PURPOSE
    //  Deletes the identified join request.
    //PARAMS
    //  requestId - The id of the request to delete.
    static bool DeleteJoinRequest(const int localGamerIndex,
        rlClanRequestId requestId,
        netStatus* status);

    //PURPOSE
    //  Retrieves the feuds associated with the specified clan.
    //PARAMS
    //  clanId            - The clan to lookup.
    //  since             - Specifies the time span to search.
    //  resultFeuders     - A caller provided array for storing results.
    //  maxFeuderCount    - The size of the provided array.
    //  resultFeuderCount - The number of results which were returned.
    static bool GetFeudStats(const int localGamerIndex,
        const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    //PURPOSE
    //  Retrieves the top rivals (feuded with most) associated with the specified clan.
    //PARAMS
    //  clanId            - The clan to lookup.
    //  since             - Specifies the time span to search.
    //  resultFeuders     - A caller provided array for storing results.
    //  maxFeuderCount    - The size of the provided array.
    //  resultFeuderCount - The number of results which were returned.
    static bool GetTopRivals(const int localGamerIndex,
        const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    //PURPOSE
    //  Retrieves the top feuders (most total feuds) amongst all clans.
    //PARAMS
    //  since             - Specifies the time span to search.
    //  resultFeuders     - A caller provided array for storing results.
    //  maxFeuderCount    - The size of the provided array.
    //  resultFeuderCount - The number of results which were returned.
    static bool GetTopFeuders(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    //PURPOSE
    //  Retrieves the top feud winners (most total wins) amongst all clans.
    //PARAMS
    //  since             - Specifies the time span to search.
    //  resultFeuders     - A caller provided array for storing results.
    //  maxFeuderCount    - The size of the provided array.
    //  resultFeuderCount - The number of results which were returned.
    static bool GetTopFeudWinners(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    //PURPOSE
    //  Retrieves the top feud losers (most total loses) amongst all clans.
    //PARAMS
    //  since             - Specifies the time span to search.
    //  resultFeuders     - A caller provided array for storing results.
    //  maxFeuderCount    - The size of the provided array.
    //  resultFeuderCount - The number of results which were returned.
    static bool GetTopFeudLosers(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    //PURPOSE
    //	Request the emblem for the given clan.  The requested dds file will be 
    //	dumped into the given fiDevice under the given handle.
    //PARAMS
    //	clanId - The clan to download the emblem for 
    //	emblemSize - Emblem size to request (32, 64, 256)
    //  ifModifiedSincePosixTime - Retrieve file only if newer.  Represented
    //                             as number of seconds since Jan 1, 1970 00:00:00
    //  responseDevice      - fiDevice to which response data will be written
    //  responseHandle      - fiHandle to which response data will be written
    //  status              - Can be monitored for completion
    //NOTES
    //  If the file is not returned because it's not newer than ifModifiedSince,
    //  then nothing will have been written to the response handle, and netStatus
    //  will report success with a status code of 304 (HTTP 304 means "not modified").
    static bool GetEmblemFor(const int localGamerIndex,
        rlClanId clanId,
        rlClanEmblemSize emblemSize,
        const u64 ifModifiedSincePosixTime,
		const fiDevice* responseDevice,
		const fiHandle responseHandle,
		netStatus* status);

	//PURPOSE
	//  Helper function to getting the appropriate path to download the crew emblem
	//PARAMS
	//	emblemSize - Emblem size to request (32, 64, 256)
	//	outPath - char array to output the path
	//  strLen - length of the outPath
	//RETURN
	//	char* to the outPath given, for convenience.
	static const char* GetCrewEmblemCloudPath(rlClanEmblemSize emblemSize, char* outPath, const unsigned int strLen);
	
/* Existing ROS WebMethods not yet implemented in RAGE code:
    GetClansWithScFriends

    GetSentInvitesByClan

    DescUpdateMotto

    BanPlayer
    GetBannedPlayers
    DeleteBanPlayerRecord

*/
private:
    friend class rlClanClient;
    friend class rlClanRosRefreshMineTask;
    friend class rlClanRosSetPrimaryTask;
    friend class rlClanRosCreateTask;
    friend class rlClanRosJoinTask;
    friend class rlClanRosLeaveTask;

    static Delegator sm_Delegator;

    static bool m_Initialized;

    static bool RefreshMine(const int localGamerIndex);
    static bool HasMembership(const int localGamerIndex,
                            const rlClanId clanId);
    static bool AddMembership(const int localGamerIndex,
                                const rlClanMembershipData& membership,
                                const bool isNewlyJoined);
    static void RemoveMembership(const int localGamerIndex,
                                const rlClanId removedClanId,
                                const rlClanId newPrimaryClanId);
    static bool UpdatePrimaryClan(const int localGamerIndex,
                                    const rlClanId oldPrimaryClanId,
                                    const rlClanId newPrimaryClanId);

	static bool UpdateCachedClanRank(const int localGamerIndex, const rlClanId clanId, int rankOrder, const char* rankName);

    static void DispatchEvent(const rlClanEvent& e);

    rlClan();
};

} //namespace rage

#endif  //RLINE_RLCLAN_H
