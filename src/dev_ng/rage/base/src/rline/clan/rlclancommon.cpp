// 
// rline/rlclancommon.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlclancommon.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "string/string.h"
#include "string/stringutil.h"
#include "system/nelem.h"
#include "system/memops.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, clan)
#undef __rage_channel
#define __rage_channel rline_clan

AUTOID_IMPL(rlClanEvent);
AUTOID_IMPL(rlClanEventSynchedMemberships);
AUTOID_IMPL(rlClanEventPrimaryClanChanged);
AUTOID_IMPL(rlClanEventJoined);
AUTOID_IMPL(rlClanEventKicked);
AUTOID_IMPL(rlClanEventLeft);
AUTOID_IMPL(rlClanEventInviteRecieved);
AUTOID_IMPL(rlClanEventFriendJoined);
AUTOID_IMPL(rlClanEventFriendFounded);
AUTOID_IMPL(rlClanEventMetadataChanged);
AUTOID_IMPL(rlClanEventMemberRankChange);
AUTOID_IMPL(rlClanEventNotifyJoinRequest);
AUTOID_IMPL(rlClanEventNotifyDescChanged);

const rlClanDesc RL_INVALID_CLAN_DESC;
const rlClanMembershipData RL_INVALID_CLAN_MEMBERSHIP_DATA;

rlClanDesc::rlClanDesc() : m_Id(RL_INVALID_CLAN_ID)
{
    sysMemSet(m_ClanName, 0, COUNTOF(m_ClanName));
    sysMemSet(m_ClanTag, 0, COUNTOF(m_ClanTag));
    sysMemSet(m_ClanMotto, 0, COUNTOF(m_ClanMotto));
	m_clanColor = RL_CLAN_DEFAULT_CLAN_COLOR;

}

void 
rlClanDesc::Clear() 
{
    m_Id = RL_INVALID_CLAN_ID;
    sysMemSet(m_ClanName, 0, COUNTOF(m_ClanName));
    sysMemSet(m_ClanTag, 0, COUNTOF(m_ClanTag));
    sysMemSet(m_ClanMotto, 0, COUNTOF(m_ClanMotto));
	m_clanColor = RL_CLAN_DEFAULT_CLAN_COLOR;
}

bool
rlClanDesc::IsRockstarClan() const
{
    // turns out the only distinguishable characteristic of a Rockstar clan
    // is that they have 'Rockstar' in the name.
    return stristr(m_ClanName,"Rockstar") != NULL;
}

rlClanInvite::rlClanInvite() : m_Id(RL_INVALID_INVITE_ID)
{
    m_Invitee.Clear();
    m_Inviter.Clear();
    m_Clan.Clear();
    sysMemSet(m_Message, 0, COUNTOF(m_Message));
}

void 
rlClanInvite::Clear()
{
    m_Id = RL_INVALID_INVITE_ID;
    m_Invitee.Clear();
    m_Inviter.Clear();
    m_Clan.Clear();
    sysMemSet(m_Message, 0, COUNTOF(m_Message));
}

rlClanRank::rlClanRank() : m_Id(RL_INVALID_RANK_ID)
{

}

void 
rlClanRank::Clear()
{
    m_Id = RL_INVALID_RANK_ID;
}

rlClanMember::rlClanMember()
{
    m_MemberInfo.Clear();
    m_MemberClanInfo.Clear();
}

void 
rlClanMember::Clear()
{
    m_MemberInfo.Clear();
    m_MemberClanInfo.Clear();
}

rlClanLeader::rlClanLeader()
    : m_ClanId(RL_INVALID_CLAN_ID)
{
    m_MemberInfo.Clear();
}

void 
rlClanLeader::Clear()
{
    m_MemberInfo.Clear();
    m_ClanId = RL_INVALID_CLAN_ID;
}

rlClanMembershipData::rlClanMembershipData()
{
    Clear();
}

void
rlClanMembershipData::Init(const rlClanMemberId memberId,
                            const rlClanDesc& clanDesc,
                            const rlClanRank& rank,
                            const bool isPrimary)
{
    m_Id = memberId;
    m_Clan = clanDesc;
    m_Rank = rank;
    m_IsPrimary = isPrimary;
}

void 
rlClanMembershipData::Clear()
{
    m_Id = RL_INVALID_MEMBER_ID;
    m_Clan.Clear();
    m_Rank.Clear();
    m_IsPrimary = false;
}

rlClanJoinRequestRecieved::rlClanJoinRequestRecieved()
    : m_RequestId(RL_INVALID_REQUEST_ID)
{
    m_Player.Clear();
}

void 
rlClanJoinRequestRecieved::Clear()
{
    m_Player.Clear();
    m_RequestId = RL_INVALID_REQUEST_ID;
}

rlClanJoinRequestSent::rlClanJoinRequestSent()
    : m_RequestId(RL_INVALID_REQUEST_ID)
{
    m_Clan.Clear();
}

void 
rlClanJoinRequestSent::Clear()
{
    m_Clan.Clear();
    m_RequestId = RL_INVALID_REQUEST_ID;
}

rlClanWallMessage::rlClanWallMessage() : m_Id(RL_INVALID_MESSAGE_ID)
{
    m_Writer.Clear();
    m_Message[0] = '\0';
}

void 
rlClanWallMessage::Clear()
{
    m_Id = RL_INVALID_MESSAGE_ID;
    m_Writer.Clear();
    m_Message[0] = '\0';
}

rlClanMetadataEnum::rlClanMetadataEnum() : m_Id(RL_INVALID_ENUM_ID)
{
    m_EnumName[0] = '\0';
    m_SetName[0] = '\0';
}

void 
rlClanMetadataEnum::Clear()
{
    m_Id = RL_INVALID_ENUM_ID;
    m_EnumName[0] = '\0';
    m_SetName[0] = '\0';
}

rlClanFeuder::rlClanFeuder() 
: m_ClanId(RL_INVALID_CLAN_ID)
, m_FirstFeudPosix(0)
, m_LastFeudPosix(0)
, m_TotalFeuds(0)
, m_Wins(0)
, m_Loses(0)
, m_LastSessionId(0)
, m_LastMatchId(0)
{
    m_ClanName[0] = '\0';
    m_ClanTag[0] = '\0';
}

void 
rlClanFeuder::Clear()
{
    m_ClanId = RL_INVALID_CLAN_ID;
    m_FirstFeudPosix = 0;
    m_LastFeudPosix = 0;
    m_TotalFeuds = 0;
    m_Wins = 0;
    m_Loses = 0;
    m_LastSessionId = 0;
    m_LastMatchId = 0;
    m_ClanName[0] = '\0';
    m_ClanTag[0] = '\0';
}

};
