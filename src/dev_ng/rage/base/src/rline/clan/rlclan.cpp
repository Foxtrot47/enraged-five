// 
// rline/rlclan.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlclan.h"
#include "rlclanclient.h"
#include "rlclancommon.h"
#include "rline/ros/rlros.h"
#include "rline/rlpresence.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "system/timer.h"

namespace rage
{

//Override output macros.
#undef __rage_channel
#define __rage_channel rline_clan

extern sysMemAllocator* g_rlAllocator;

static rlClanClient m_Clients[RL_MAX_LOCAL_GAMERS];
rlClan::Delegator rlClan::sm_Delegator;
bool rlClan::m_Initialized;

//Listens for presence events.
//Friended by rlClan so it can force cache refreshes.
class rlClanPresenceEventListener
{
private:
	//PUROSE: Extract the gamer handle from the channel specification on a publish message
	// Example:		"channel":"friend.<gamer_handle>"
	static bool GetGamerhandleFromChannel(const char* channel, const char* channelPrefix, rlGamerHandle& out_gh)
	{
		//Make sure the prefix is there
		u32 prefixLen = ustrlen(channelPrefix);
		if (prefixLen > 0 && !(strlen(channel) > prefixLen && strncmp(channelPrefix, channel, prefixLen ) == 0))
		{
			return false;
		}
		
		//Look for the dot and make sure there is some stuff after it.
		const char* theDot = strchr(channel, '.');
		if (theDot == NULL || strlen(theDot+1) == 0)
		{
			rlError("Improperly formed channel string '%s'", channel);
			return false;
		}

		//Move past the dot to get the gamer handle
		theDot++;

		//Parse the leftover as a gamer handle
		return rlVerifyf(out_gh.FromUserId(theDot), "Unable to parse string as gamer handle: '%s'", theDot);
	}

public:
    static void OnPresenceEvent(const rlPresenceEvent* evt)
    {
        if(PRESENCE_EVENT_SC_MESSAGE == evt->GetId())
        {
            const int localGamerIndex = evt->m_GamerInfo.GetLocalIndex();
            const rlScPresenceMessage& msgBuf = evt->m_ScMessage->m_Message;
            if(msgBuf.IsA<rlScPresenceMessageClanInviteReceived>())
            {
                rlScPresenceMessageClanInviteReceived msg;
                if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
                {
                    rlDebug("Received clan invite for gamer: %d", localGamerIndex);

                    rlClanEventInviteRecieved e(localGamerIndex, msg.m_ClanId, msg.m_ClanName, msg.m_ClanTag, msg.m_RankName, msg.m_Message);
                    rlClan::DispatchEvent(e);
                }
            }
            else if(msgBuf.IsA<rlScPresenceMessageClanJoined>())
            {
                rlScPresenceMessageClanJoined msg;
                if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
                {
                    rlDebug("Gamer: %d joined clan: %" I64FMT "d", localGamerIndex, msg.m_ClanId);
                }

                if(!rlClan::HasMembership(localGamerIndex, msg.m_ClanId))
                {
                    //This join event was triggered by a join that occurred on the
                    //web site.  We need to refresh the cache.
                    rlClan::RefreshMine(localGamerIndex);
                }
            }
            else if(msgBuf.IsA<rlScPresenceMessageClanLeft>()
                    || msgBuf.IsA<rlScPresenceMessageClanKicked>())
            {
                rlClanId clanId = RL_INVALID_CLAN_ID;

                if(msgBuf.IsA<rlScPresenceMessageClanLeft>())
                {
                    rlScPresenceMessageClanLeft msg;
                    if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
                    {
                        rlDebug("Gamer: %d left clan: %" I64FMT "d", localGamerIndex, msg.m_ClanId);

                        clanId = msg.m_ClanId;
                    }
                }
                else
                {
                    rlScPresenceMessageClanKicked msg;
                    if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
                    {
                        rlDebug("Gamer: %d was kicked from clan: %" I64FMT "d", localGamerIndex, msg.m_ClanId);

                        clanId = msg.m_ClanId;

                        rlClanEventKicked e(localGamerIndex, clanId);
                        rlClan::DispatchEvent(e);
                    }
                }

                if(RL_INVALID_CLAN_ID != clanId
                    && rlClan::HasMembership(localGamerIndex, clanId))
                {
                    //We don't know about this leave/kick which implies it occurred
                    //outside of our local context, ie the local player didn't initiate
                    //the action locally.

                    const rlClanId currentPrimary =
                        rlClan::GetPrimaryClan(localGamerIndex).m_Id;                  

                    if(currentPrimary == clanId)
                    {
						//Remove our primary clan from our list to keep the local list close to up to date
						//The ClanLeft event will be dispatched in RemoveMembership()
						rlClan::RemoveMembership(localGamerIndex, clanId, RL_INVALID_CLAN_ID);

                        //Left or kicked from our primary clan and now we don't know what
                        //our primary clan is.
                        //We need to refresh the clan cache, which will remove the crew and all that.
                        rlClan::RefreshMine(localGamerIndex);
                    }
					else
                    {
						//No change in primary clan so no cache refresh necessary.
						//The ClanLeft event will be dispatched in RemoveMembership()
                        rlClan::RemoveMembership(localGamerIndex, clanId, currentPrimary);
                    }
                }
            }
            else if(msgBuf.IsA<rlScPresenceMessageClanPrimaryChanged>())
            {
                rlScPresenceMessageClanPrimaryChanged msg;
                if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
                {
                    rlDebug("Gamer: %d primary clan is now: %" I64FMT "d", localGamerIndex, msg.m_ClanId);

                    const rlClanId oldPrimaryClanId = rlClan::GetPrimaryClan(localGamerIndex).m_Id;
                    if(msg.m_ClanId != oldPrimaryClanId)
                    {
						if(rlClan::HasMembership(localGamerIndex, msg.m_ClanId))
						{
							//The PrimaryClanChanged event will be dispatched in UpdatePrimaryClan()
							rlClan::UpdatePrimaryClan(localGamerIndex, oldPrimaryClanId, msg.m_ClanId);
						}
						else
						{
							//This changed event was triggered short after a join that occurred on the
							//web site. Game is probably already refreshing the cache, but let's do it if not
							rlClan::RefreshMine(localGamerIndex);
						}
                    }
                }
            }
			else if (msgBuf.IsA<rlScPresenceMessageClanMemberPromoted>())
			{ 
				rlScPresenceMessageClanMemberPromoted msg;
				if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
				{
					rlClanEventMemberRankChange e(localGamerIndex, msg.m_ClanId, msg.m_rankOrder, msg.m_rankName, true);
					rlClan::DispatchEvent(e);
					rlClan::UpdateCachedClanRank(localGamerIndex, msg.m_ClanId, msg.m_rankOrder, msg.m_rankName);
				}
			}
			else if (msgBuf.IsA<rlScPresenceMessageClanMemberDemoted>())
			{ 
				rlScPresenceMessageClanMemberDemoted msg;
				if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
				{
					rlClanEventMemberRankChange e(localGamerIndex, msg.m_ClanId, msg.m_rankOrder, msg.m_rankName, false);
					rlClan::DispatchEvent(e);
					rlClan::UpdateCachedClanRank(localGamerIndex, msg.m_ClanId, msg.m_rankOrder, msg.m_rankName);
				}
			}
			//////////////// IS THIS CASE POSSIBLE ? Seems we are packaging these messages inside of presence publish messages
			else if(msgBuf.IsA<rlScPresenceMessageClanNotify>())
			{
				rlScPresenceMessageClanNotify msg;
				if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
				{
					const u32 typeHash = atStringHash(msg.m_Type);
					if(typeHash == ATSTRINGHASH("joinrequest", 0x8F407E48))
					{
						rlClanEventNotifyJoinRequest e(localGamerIndex);
						rlClan::DispatchEvent(e);
					}
					else if(typeHash == ATSTRINGHASH("descchanged", 0xE2F22B0C))
					{
						rlClanEventNotifyDescChanged e(localGamerIndex, msg.m_ClanId);
						rlClan::DispatchEvent(e);
					}
					else
					{
						rlError("Unhandled type found in rlScPresenceMessageClanNotify ('ros.clan.notify') string: %s", msg.m_Type);
					}
				}
			}
			//Check to see if we got an event notification about a friend
			else if (msgBuf.IsA<rlScPresenceMessagePublish>())
			{
				//Pick off the msg chunk to see if this is a clan msg.
				rlScPresenceMessagePublish msg;
				if (rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
				{
					//Need to get the chunk inside the curly braces
					//msg.m_Message.m_Contents looks like {"ros.clan.joined":8936}
                    const unsigned bufSize = ustrlen(msg.m_Message);
                    char* tmpBuf = Alloca(char, bufSize);
                    const rlScPresenceMessage pubMsgBuf(tmpBuf, 0);
					safecpy(tmpBuf, msg.m_Message+1, bufSize);
					char* endBrace = strrchr(tmpBuf, '}');
					if (endBrace)
					{
						*endBrace = '\0';
					}

					if (pubMsgBuf.IsA<rlScPresenceMessageClanJoined>())
					{
						//Means we got a clan joined message for a friend
						//"ros.publish":{"channel":"friend.<gamer_handle>","msg":{"ros.clan.joined":<clan ID>}}
						rlScPresenceMessageClanJoined clanJoined;
						if(rlVerifyf(clanJoined.Import(pubMsgBuf), "Error importing published friend clan join message '%s'", msg.m_Message ))
						{
							//Pick off the gamer handle
							rlGamerHandle gh;
							if(rlVerifyf( GetGamerhandleFromChannel(msg.m_Channel, "friend", gh), "Failed to get gamer handle from channel '%s'", msg.m_Channel ))
							{
								rlDebug("Received ros.clan.joined from friend %s", msg.m_Channel);

								//Now set up the friend joined crew message
								rlClanEventFriendJoined e(localGamerIndex, gh, clanJoined.m_ClanId);
								rlClan::DispatchEvent(e);
							}		
						}
					}
					else if (pubMsgBuf.IsA<rlScPresenceMessageClanFounded>())
					{
						rlScPresenceMessageClanFounded clanFounded;
						if(rlVerifyf(clanFounded.Import(pubMsgBuf), "Error importing published friend clan join message '%s'", msg.m_Message ))
						{
							//Pick off the gamer handle
							rlGamerHandle gh;
							if(rlVerifyf( GetGamerhandleFromChannel(msg.m_Channel, "friend", gh), "Failed to get gamer handle from channel '%s'", msg.m_Channel ))
							{
								rlDebug("Received ros.clan.founded from friend %s", msg.m_Channel);
								
								//Now set up the friend joined crew message
								rlClanEventFriendFounded e(localGamerIndex, gh, clanFounded.m_ClanId);
								rlClan::DispatchEvent(e);
							}		
						}
					}
					else if (pubMsgBuf.IsA<rlScPresenceMessageClanMetadataChanged>())
					{
						rlScPresenceMessageClanMetadataChanged mdChanged;
						if(rlVerifyf(mdChanged.Import(pubMsgBuf), "Error importing clan metadata changed message '%s'", msg.m_Message ))
						{
							const rlClanEventMetadataChanged e(localGamerIndex, mdChanged.m_ClanId);
							rlClan::DispatchEvent(e);
						}
					}
					else if(pubMsgBuf.IsA<rlScPresenceMessageClanNotify>())
					{
						rlScPresenceMessageClanNotify msgClanNotify;
						if(rlVerifyf(msgClanNotify.Import(pubMsgBuf), "Error importing published clan notify '%s'", msg.m_Message))
						{
							const u32 typeHash = atStringHash(msgClanNotify.m_Type);
							if(typeHash == ATSTRINGHASH("joinrequest", 0x8F407E48))
							{
								rlClanEventNotifyJoinRequest e(localGamerIndex);
								rlClan::DispatchEvent(e);
							}
							else if(typeHash == ATSTRINGHASH("descchanged", 0xE2F22B0C))
							{
								rlClanEventNotifyDescChanged e(localGamerIndex, msgClanNotify.m_ClanId);
								rlClan::DispatchEvent(e);
							}
							else
							{
								rlError("Unhandled type found in rlScPresenceMessageClanNotify ('ros.clan.notify') string: %s", msgClanNotify.m_Type);
							}
						}
					}
				}
			}
        }
    }
};

class rlClanRosEventListener
{
public:

    static void OnRosEvent(const rlRosEvent& event)
    {
        if(RLROS_EVENT_LINK_CHANGED == event.GetId())
        {
            //Update clan memberships

            const rlRosEventLinkChanged& lc =
                static_cast<const rlRosEventLinkChanged&>(event);

            if(RL_IS_VALID_LOCAL_GAMER_INDEX(lc.m_LocalGamerIndex))
            {
                if(RL_INVALID_ROCKSTAR_ID == lc.m_RockstarId)
                {
                    //We were unlinked - update clan related presence.
                    const rlClanId oldPrimary = rlClan::GetPrimaryClan(lc.m_LocalGamerIndex).m_Id;
                    m_Clients[lc.m_LocalGamerIndex].UpdatePresence(oldPrimary, RL_INVALID_CLAN_ID);
                }
            }
        }
    }
};

static rlPresence::Delegate s_RlClanPresenceDlgt(&rlClanPresenceEventListener::OnPresenceEvent);

static rlRos::Delegate s_RlClanRosDlgt(&rlClanRosEventListener::OnRosEvent);

const char*
rlClan::CreateMessageChannelName(char* channel,
                                const unsigned maxLen,
                                const rlClanId clanId)
{
    return formatf(channel, maxLen, "crew.%" I64FMT "d", clanId);
}

void 
rlClan::AddDelegate(Delegate* dlgt)
{
    sm_Delegator.AddDelegate(dlgt);
}

void 
rlClan::RemoveDelegate(Delegate* dlgt)
{
    sm_Delegator.RemoveDelegate(dlgt);
}

bool 
rlClan::Init()
{
    rlAssert(!m_Initialized);    

    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        if(!m_Clients[i].Init(i))
        {
            rlError("Failed to init client %d", i);
            return false;
        }
    }

    m_Initialized = true;

    return m_Initialized;
}

bool 
rlClan::IsInitialized()
{
    return m_Initialized;
}

void 
rlClan::Shutdown()
{
    if(m_Initialized)
    {
        sm_Delegator.Clear();

        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            m_Clients[i].Shutdown();
        }

        if(s_RlClanPresenceDlgt.IsRegistered())
        {
            rlPresence::RemoveDelegate(&s_RlClanPresenceDlgt);
        }

        if(s_RlClanRosDlgt.IsRegistered())
        {
            rlRos::RemoveDelegate(&s_RlClanRosDlgt);
        }

        m_Initialized = false;
    }
}

void 
rlClan::Update()
{
    //Make sure our presence event listener is registered.
    //We can't do this in Init() b/c we don't know if rlPresence
    //has been initialized yet.
    if(!s_RlClanPresenceDlgt.IsRegistered() && rlPresence::IsInitialized())
    {
        rlPresence::AddDelegate(&s_RlClanPresenceDlgt);
    }

    if(!s_RlClanRosDlgt.IsRegistered() && rlRos::IsInitialized())
    {
        rlRos::AddDelegate(&s_RlClanRosDlgt);
    }

    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        m_Clients[i].Update();
    }
}

void
rlClan::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

bool 
rlClan::IsServiceReady(int localGamerIndex)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].IsServiceReady()
        : false;
}


bool 
rlClan::IsFullServiceReady( const int localGamerIndex )
{
	return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		? m_Clients[localGamerIndex].IsFullServiceAvailable()
		: false;
}


bool
rlClan::HasPrimaryClan(const int localGamerIndex)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].HavePrimaryClan()
        : false;
}

const rlClanDesc&
rlClan::GetPrimaryClan(const int localGamerIndex)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetPrimaryClan()
        : RL_INVALID_CLAN_DESC;
}

const rlClanMembershipData& 
rlClan::GetPrimaryMembership(const int localGamerIndex)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetPrimaryMembership()
        : RL_INVALID_CLAN_MEMBERSHIP_DATA;
}

bool
rlClan::GetMine(const int localGamerIndex,
                rlClanMembershipData* resultMemberships,
                unsigned int maxMembershipsCount,
                unsigned int* resultMembershipsCount,
                unsigned int* resultTotalCount,
                netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetMine(resultMemberships, maxMembershipsCount, resultMembershipsCount, resultTotalCount, status)
        : false;
}

bool
rlClan::Create(const int localGamerIndex,
               const char* clanName,
               const char* clanTag,
               bool isOpenClan,
               netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].Create(clanName, clanTag, isOpenClan, status)
        : false;
}

bool
rlClan::Leave(const int localGamerIndex,
              rlClanId clanId,
              netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].Leave(clanId, status)
        : false;
}

bool
rlClan::SetPrimaryClan(const int localGamerIndex,
                       rlClanId clanId,
                       netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].SetPrimaryClan(clanId, status)
        : false;
}

bool
rlClan::Invite(const int localGamerIndex,
               rlClanId clanId,
               int rankOrder,
               bool expireExtraInvitations,
               const rlGamerHandle& gamer,
               netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].Invite(gamer, clanId, rankOrder, expireExtraInvitations, status)
        : false;
}

bool
rlClan::DeleteInvite(const int localGamerIndex,
                     rlClanInviteId inviteId,
                     netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].DeleteInvite(inviteId, status)
        : false;
}

bool
rlClan::Join(const int localGamerIndex,
             rlClanId clan,
             netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].Join(clan, status)
        : false;
}

bool
rlClan::Kick(const int localGamerIndex,
             rlClanId clan,
             const rlGamerHandle& gamer,
             netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].Kick(gamer, clan, status)
        : false;
}
bool
rlClan::MemberUpdateRankId(const int localGamerIndex,
                           const rlGamerHandle& gamer,
                           rlClanId clanId,
                           bool promote,
                           netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].MemberUpdateRankId(gamer, clanId, promote, status)
        : false;
}

bool
rlClan::RankCreate(const int localGamerIndex, 
        rlClanId clanId,
        const char* rankName,
        s64 initialSystemFlags,
        netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].RankCreate(clanId, rankName, initialSystemFlags, status)
        : false;
}

bool
rlClan::RankDelete(const int localGamerIndex, 
        rlClanId clanId,
        rlClanRankId rankId,
        netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].RankDelete(clanId, rankId, status)
        : false;
}

bool
rlClan::RankUpdate(const int localGamerIndex, 
        rlClanId clanId,
        rlClanRankId rankId,
        int adjustRankOrder,
        const char* rankName,
        s64 updateSystemFlags,
        netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].RankUpdate(clanId, rankId, adjustRankOrder, rankName, updateSystemFlags, status)
        : false;
}

bool
rlClan::DeleteWallMessage(const int localGamerIndex,
                          rlClanId clanId,
                          rlClanWallMessageId msgId,
                          netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].DeleteWallMessage(clanId, msgId, status)
        : false;
}

bool
rlClan::WriteWallMessage(const int localGamerIndex,
                          rlClanId clanId,
                          const char* message,
                          netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].WriteWallMessage(clanId, message, status)
        : false;
}

bool
rlClan::GetInvites(const int localGamerIndex,
                   int pageIndex,
                   rlClanInvite* resultInvites,
                   unsigned int maxInvitesCount,
                   unsigned int* resultInvitesCount,
                   unsigned int* resultTotalCount,
                   netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetInvites(pageIndex, 
        resultInvites, maxInvitesCount, resultInvitesCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetSentInvites(const int localGamerIndex,
                       int pageIndex,
                       rlClanInvite* resultInvites,
                       unsigned int maxInvitesCount,
                       unsigned int* resultInvitesCount,
                       unsigned int* resultTotalCount,
                       netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetSentInvites(pageIndex, 
        resultInvites, maxInvitesCount, resultInvitesCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetAll(const int localGamerIndex,
               int pageIndex,
               rlClanDesc* resultClans,
               unsigned int maxClansCount,
               unsigned int* resultClansCount,
               unsigned int* resultTotalCount,
               netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetAll(pageIndex, 
        resultClans, maxClansCount, resultClansCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetAllByClanName(const int localGamerIndex,
               int pageIndex,
               const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],
               rlClanDesc* resultClans,
               unsigned int maxClansCount,
               unsigned int* resultClansCount,
               unsigned int* resultTotalCount,
               netStatus* status)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_Clients[localGamerIndex].GetAllByClanName(pageIndex, 
        clanName, resultClans, maxClansCount, resultClansCount, resultTotalCount, status);
}

bool
rlClan::GetSystemClans(const int localGamerIndex,
                       int pageIndex,
                       rlClanDesc* resultClans,
                       unsigned int maxClansCount,
                       unsigned int* resultClansCount,
                       unsigned int* resultTotalCount,
                       netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetSystemClans(pageIndex, 
        resultClans, maxClansCount, resultClansCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetOpenClans(const int localGamerIndex,
                     int pageIndex,
                     rlClanDesc* resultClans,
                     unsigned int maxClansCount,
                     unsigned int* resultClansCount,
                     unsigned int* resultTotalCount,
                     netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetOpenClans(pageIndex,
        resultClans, maxClansCount, resultClansCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetDesc(const int localGamerIndex,
                const rlClanId clan,
                rlClanDesc* resultClanDesc,
                netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetDesc(clan, resultClanDesc, status)
        : false;
}

bool
rlClan::GetDescs(const int localGamerIndex,
                 const rlClanId* clans,
                 unsigned int clansCount,
                 rlClanDesc* resultClanDescs,
                 netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetDescs(clans, clansCount, resultClanDescs, status)
        : false;
}

bool
rlClan::GetLeadersForClans(const int localGamerIndex,
                           const rlClanId* clans,
                           unsigned int clansCount,
                           rlClanLeader* resultLeaders,
                           unsigned int* resultCount,
                           netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetLeadersForClans(clans, clansCount, resultLeaders, resultCount, status)
        : false;
}

bool
rlClan::GetPrimaryClans(const int localGamerIndex,
                        const rlGamerHandle* gamers,
                        unsigned int numGamers,
                        rlClanMember* resultGamers,
                        unsigned int* resultGamersCount,
                        unsigned int* resultTotalCount,
                        netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetPrimaryClans(gamers, 
        numGamers, resultGamers, resultGamersCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetMembersTitleOnly(const int localGamerIndex,
                           int pageIndex,
                           rlClanId clan,
                           rlClanMember* resultGamers,
                           unsigned int maxGamersCount,
                           unsigned int* resultGamersCount,
                           unsigned int* resultTotalCount,
                           netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetMembersTitleOnly(pageIndex, 
        clan, resultGamers, maxGamersCount, resultGamersCount, resultTotalCount, status)
        : false;
}
bool
rlClan::GetMembersByClanId(const int localGamerIndex,
                           int pageIndex,
                           rlClanId clan,
                           rlClanMember* resultGamers,
                           unsigned int maxGamersCount,
                           unsigned int* resultGamersCount,
                           unsigned int* resultTotalCount,
                           netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetMembersByClanId(pageIndex, 
        clan, resultGamers, maxGamersCount, resultGamersCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetMembersByClanName(const int localGamerIndex,
                             int pageIndex,
                             const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],
                             rlClanMember* resultGamers,
                             unsigned int maxGamersCount,
                             unsigned int* resultGamersCount,
                             unsigned int* resultTotalCount,
                             netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetMembersByClanName(pageIndex, 
        clanName, resultGamers, maxGamersCount, resultGamersCount, resultTotalCount, status)
        : false;
}

bool 
rlClan::GetRanks(const int localGamerIndex, 
                 int pageIndex,
                 rlClanId clanId,
                 rlClanRank* resultRanks,
                 unsigned int maxRanksCount,
                 unsigned int* resultRanksCount,
                 unsigned int* resultTotalCount,
                 netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetRanks(pageIndex, 
        clanId, resultRanks, maxRanksCount, resultRanksCount, resultTotalCount, status)
        : false;
}

bool 
rlClan::GetWallMessages(const int localGamerIndex,
                        int pageIndex,
                        rlClanId clanId,
                        rlClanWallMessage* resultMessages,
                        unsigned int maxMessageCount,
                        unsigned int* resultMessageCount,
                        unsigned int* resultTotalCount,
                        netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetWallMessages(pageIndex, 
        clanId, resultMessages, maxMessageCount, resultMessageCount, resultTotalCount, status)
        : false;
}

bool 
rlClan::GetMetadataForClan(const int localGamerIndex,
                        int pageIndex,
                        rlClanId clanId,
                        rlClanMetadataEnum* resultEnums,
                        unsigned int maxEnumCount,
                        unsigned int* resultEnumCount,
                        unsigned int* resultTotalCount,
                        netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetMetadataForClan(pageIndex, 
        clanId, resultEnums, maxEnumCount, resultEnumCount, resultTotalCount, status)
        : false;
}

bool
rlClan::GetMembershipFor(const int localGamerIndex,
                         int pageIndex,
                         const rlGamerHandle& gamer,
                         rlClanMembershipData* resultMemberships,
                         unsigned int maxMembershipsCount,
                         unsigned int* resultMembershipsCount,
                         unsigned int* resultTotalCount,
                         netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetMembershipFor(pageIndex, 
        gamer, resultMemberships, maxMembershipsCount, resultMembershipsCount, resultTotalCount, status)
        : false;
}

bool 
rlClan::JoinRequest(const int localGamerIndex,
                    rlClanId clanId,
                    netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].JoinRequest(clanId, status)
        : false;
}

bool 
rlClan::GetSentJoinRequests(const int localGamerIndex,
                            int pageIndex,
                            rlClanJoinRequestSent* resultRequests,
                            unsigned int maxRequestsCount,
                            unsigned int* resultRequestsCount,
                            unsigned int* resultTotalCount,
                            netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetSentJoinRequests(pageIndex, resultRequests, maxRequestsCount, resultRequestsCount, resultTotalCount, status)
        : false;
}

bool 
rlClan::GetRecievedJoinRequests(const int localGamerIndex,
                                int pageIndex,
                                rlClanId clanId,
                                rlClanJoinRequestRecieved* resultRequests,
                                unsigned int maxRequestsCount,
                                unsigned int* resultRequestsCount,
                                unsigned int* resultTotalCount,
                                netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetRecievedJoinRequests(pageIndex, clanId, resultRequests, maxRequestsCount, resultRequestsCount, resultTotalCount, status)
        : false;
}

bool 
rlClan::DeleteJoinRequest(const int localGamerIndex,
                          rlClanRequestId requestId,
                          netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].DeleteJoinRequest(requestId, status)
        : false;
}

bool
rlClan::GetFeudStats(const int localGamerIndex,
                     const rlClanId clanId,
                     const rlClanFeudSinceCode since,
                     const unsigned int maxFeuderCount,
                     rlClanFeuder* resultFeuders,
                     unsigned int* resultFeuderCount,
                     netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetFeudStats(clanId, since, maxFeuderCount, 
        resultFeuders, resultFeuderCount, status)
        : false;
}

bool
rlClan::GetTopRivals(const int localGamerIndex,
                     const rlClanId clanId,
                     const rlClanFeudSinceCode since,
                     const unsigned int maxFeuderCount,
                     rlClanFeuder* resultFeuders,
                     unsigned int* resultFeuderCount,
                     netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetTopRivals(clanId, since, maxFeuderCount, 
        resultFeuders, resultFeuderCount, status)
        : false;
}

bool
rlClan::GetTopFeuders(const int localGamerIndex,
                     const rlClanFeudSinceCode since,
                     const unsigned int maxFeuderCount,
                     rlClanFeuder* resultFeuders,
                     unsigned int* resultFeuderCount,
                     netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetTopFeuders(since, maxFeuderCount, 
        resultFeuders, resultFeuderCount, status)
        : false;
}

bool
rlClan::GetTopFeudWinners(const int localGamerIndex,
                     const rlClanFeudSinceCode since,
                     const unsigned int maxFeuderCount,
                     rlClanFeuder* resultFeuders,
                     unsigned int* resultFeuderCount,
                     netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetTopFeudWinners(since, maxFeuderCount, 
        resultFeuders, resultFeuderCount, status)
        : false;
}

bool
rlClan::GetTopFeudLosers(const int localGamerIndex,
                     const rlClanFeudSinceCode since,
                     const unsigned int maxFeuderCount,
                     rlClanFeuder* resultFeuders,
                     unsigned int* resultFeuderCount,
                     netStatus* status)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetTopFeudLosers(since, maxFeuderCount, 
        resultFeuders, resultFeuderCount, status)
        : false;
}

bool
rlClan::GetEmblemFor(const int localGamerIndex, 
                     rlClanId clanId, 
                     rlClanEmblemSize emblemSize, 
                     const u64 ifModifiedSincePosixTime,
                     const fiDevice* responseDevice, 
                     const fiHandle responseHandle,
                     netStatus* status )
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].GetEmblemFor(clanId,emblemSize,ifModifiedSincePosixTime,responseDevice, responseHandle,status)
        : false;
}


const char* 
rlClan::GetCrewEmblemCloudPath( rlClanEmblemSize emblemSize, 
								char* outPath, 
								const unsigned int strLen )
{
	return rlClanRosGetClanEmblemTask::GetCrewEmblemCloudPath(emblemSize, outPath, strLen);
}


bool
rlClan::HasMembership(const int localGamerIndex, const rlClanId clanId)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].HasMembership(clanId)
        : false;
}

bool
rlClan::AddMembership(const int localGamerIndex,
                        const rlClanMembershipData& membership,
                        const bool isNewlyJoined)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].AddMembership(membership, isNewlyJoined)
        : false;
}

void
rlClan::RemoveMembership(const int localGamerIndex,
                        const rlClanId removedClanId,
                        const rlClanId newPrimaryClanId)
{
    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        m_Clients[localGamerIndex].RemoveMembership(removedClanId, newPrimaryClanId);
    }
}

bool
rlClan::RefreshMine(const int localGamerIndex)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].RefreshMine()
        : false;
}

bool 
rlClan::UpdatePrimaryClan(const int localGamerIndex,
                        const rlClanId oldPrimaryClanId,
                        const rlClanId newPrimaryClanId)
{
    return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].UpdatePrimaryClan(oldPrimaryClanId, newPrimaryClanId)
        : false;
}

bool 
rlClan::UpdateCachedClanRank(const int localGamerIndex, const rlClanId clanId, int rankOrder, const char* rankName)
{
	return rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        ? m_Clients[localGamerIndex].UpdateCachedClanRank(clanId, rankOrder, rankName)
        : false;
}

void 
rlClan::DispatchEvent(const rlClanEvent& e)
{
    rlDebug("Dispatching '%s'", e.GetName());
    sm_Delegator.Dispatch(e);
}

};
