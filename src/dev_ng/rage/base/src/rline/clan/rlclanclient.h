// 
// rline/rlclanclient.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLCLANCLIENT_H
#define RLINE_RLCLANCLIENT_H

#include "rlclancommon.h"
#include "rlclantasks.h"
#include "rline\rltask.h"
#include "atl\bitset.h"
#include "net\status.h"

namespace rage
{

class rlClanRosRefreshMineTask;
class rlClanRosGetMineTask;

//PURPOSE
//  Helper class for rlClan that manages the state of a local gamer.
//  This should not be referenced by title code.
class rlClanClient
{
    friend class rlClanRosRefreshMineTask;
    friend class rlClanRosGetMineTask;
public:
    rlClanClient();
    ~rlClanClient();

    bool Init(const int localGamerIndex);
    void Shutdown();
    void Update();

    void Restart(bool forceNow);

    int GetLocalGamerIndex() const { return (!m_Initialized) ? -1 : m_LocalGamerIndex; }

    bool IsServiceReady() const;

	bool IsFullServiceAvailable() const;

    bool HavePrimaryClan() const;

    const rlClanDesc& GetPrimaryClan() const;

    const rlClanMembershipData& GetPrimaryMembership() const;

    bool UpdatePrimaryClan(const rlClanId oldPrimaryClanId,
                            const rlClanId newPrimaryClanId);

	bool UpdateCachedClanRank(const rlClanId clanId, int rankOrder, const char* rankName);

    //PURPOSE
    //  Sets the crew ID presence attribute and subscribes to
    //  the crew message channel.
    void UpdatePresence(const rlClanId oldPrimaryClanId,
                        const rlClanId newPrimaryClanId);

    enum RefreshQueueType
    {
        REFRESH_QUEUE_SERIAL,
        REFRESH_QUEUE_PARALLEL
    };
    bool RefreshMine(const RefreshQueueType refreshQueueType,
                    netStatus* status);

    bool RefreshMine();

    bool HasMembership(const rlClanId clanId) const;

	rlClanId GetClanId(const int index) const;

	int GetNumMemberships() const { return m_NumMemberships; }

    //PURPOSE
    //  Called when a clan is joined, or when the cache is being refreshed.
    bool AddMembership(const rlClanMembershipData& membership,
                        const bool isNewlyJoined);

    //PURPOSE
    //  Called when a clan is left.
    void RemoveMembership(const rlClanId removedClanId,
                        const rlClanId newPrimaryClanId);

    bool GetMine(rlClanMembershipData* resultMembership,
        unsigned int maxMembershipCount,
        unsigned int* resultMembershipCount,
        unsigned int* resultTotalCount,
        netStatus* status);
    
    bool Create(const char* clanName,
        const char* clanTag,
        bool isOpenClan,
        netStatus* status);

    bool Invite(const rlGamerHandle& gamer,
        rlClanId clanId,
        int rankOrder,
        bool expireExtraInvitations,
        netStatus* status);

    bool DeleteInvite(rlClanInviteId inviteId,
        netStatus* status);

    bool Join(rlClanId clanId,
        netStatus* status);

    bool Leave(rlClanId clanId,
        netStatus* status);

    bool SetPrimaryClan(rlClanId clanId,
        netStatus* status);

    bool Kick(const rlGamerHandle& gamer,
        rlClanId clanId,
        netStatus* status);

    bool RankCreate(rlClanId clanId, 
        const char* rankName,
        s64 initialSystemFlags,
        netStatus* status);

    bool RankDelete(rlClanId clanId, 
        rlClanRankId rankId,
        netStatus* status);

    bool RankUpdate(rlClanId clanId, 
        rlClanRankId rankId,
        int adjustRankOrder,
        const char* rankName,
        s64 updateSystemFlags,
        netStatus* status);

    bool DeleteWallMessage(rlClanId clanId,
        rlClanWallMessageId msgId,
        netStatus* status);

    bool WriteWallMessage(rlClanId clanId,
        const char* message,
        netStatus* status);

    bool MemberUpdateRankId(const rlGamerHandle& gamer,
        rlClanId clanId,
        bool promote,
        netStatus* status);

    bool GetInvites(int pageIndex,
        rlClanInvite* resultInvites,        
        unsigned int maxInvitesCount,
        unsigned int* resultInvitesCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetSentInvites(int pageIndex,
        rlClanInvite* resultInvites,        
        unsigned int maxInvitesCount,
        unsigned int* resultInvitesCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetAll(int pageIndex,
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetAllByClanName(int pageIndex,
        const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetSystemClans(int pageIndex,
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetOpenClans(int pageIndex,
		rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetDesc(const rlClanId clanId,
        rlClanDesc* resultClanDesc,
        netStatus* status);

    bool GetDescs(const rlClanId* clans,
        unsigned int clansCount,
        rlClanDesc* resultClanDescs,
        netStatus* status);

    bool GetLeadersForClans(const rlClanId* clans,
        unsigned int clansCount,
        rlClanLeader* resultLeaders,
        unsigned int* resultCount,
        netStatus* status);

    bool GetPrimaryClans(
        const rlGamerHandle* gamers,
        const unsigned numGamers,
        rlClanMember* resultGamers,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetMembersTitleOnly(int pageIndex,
        rlClanId clanId,
        rlClanMember* resultGamers,
        unsigned int maxGamersCount,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetMembersByClanId(int pageIndex,
        rlClanId clanId,
        rlClanMember* resultGamers,
        unsigned int maxGamersCount,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetMembersByClanName(int pageIndex,
        const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],
        rlClanMember* resultGamers,
        unsigned int maxGamersCount,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetRanks(int pageIndex,
        rlClanId clanId,
        rlClanRank* resultRanks,
        unsigned int maxRanksCount,
        unsigned int* resultRanksCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetWallMessages(int pageIndex,
        rlClanId clanId,
        rlClanWallMessage* resultMessages,
        unsigned int maxMessageCount,
        unsigned int* resultMessageCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetMetadataForClan(int pageIndex,
        rlClanId clanId,
        rlClanMetadataEnum* resultEnums,
        unsigned int maxEnumCount,
        unsigned int* resultEnumCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetMembershipFor(int pageIndex,
        const rlGamerHandle& gamer,
        rlClanMembershipData* resultMembership,
        unsigned int maxMembershipCount,
        unsigned int* resultMembershipCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool JoinRequest(rlClanId clanId,
        netStatus* status);

    bool GetSentJoinRequests(int pageIndex,
        rlClanJoinRequestSent* resultRequests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool GetRecievedJoinRequests(int pageIndex,
        rlClanId clanId,
        rlClanJoinRequestRecieved* resultRequests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount,
        netStatus* status);

    bool DeleteJoinRequest(rlClanRequestId requestId,
        netStatus* status);

    bool GetFeudStats(const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    bool GetTopRivals(const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    bool GetTopFeuders(const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    bool GetTopFeudWinners(const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    bool GetTopFeudLosers(const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount,
        netStatus* status);

    bool GetEmblemFor(rlClanId clanId,
        rlClanEmblemSize emblemSize,
        const u64 ifModifiedSincePosixTime,
        const fiDevice* responseDevice,
        const fiHandle responseHandle,
        netStatus* status);
    
private:
    enum eState
    {
        STATE_INVALID = -1,
        STATE_WAIT_FOR_CREDS,
        STATE_RUNNING_AS_SC_MEMBER,
        STATE_RUNNING
    };

    template<class T>
    bool CreateTask(T** task)
    {
        if(rlVerify(m_Initialized))
        {
            return rlGetTaskManager()->CreateTask(task);
        }

        return false;
    }

    void DestroyTask(rlTaskBase* task);

    bool QueueTask(rlTaskBase* task);

    bool AddParallelTask(rlTaskBase* task);

    void CancelTask(const netStatus* status);

    void CancelAllTasks();

	//Check for valid credentials.  Some operations don't require a valid rockstar ID (i.e. SC membership, so allow that check to).
    bool CheckCredentials(rlCheckCredentialsFlags requiredCredsFlag = RL_CREDS_SOCIALCLUB_MEMBER) const;

    void CopyMembershipsTo(rlClanMembershipData* memberships,
                            const unsigned maxMemberships,
                            unsigned* numMemberships,
                            unsigned* totalMemberships) const;

    eState m_State;
    int m_LocalGamerIndex;
    rlGamerHandle m_MyGamerHandle;

    netStatus m_RefreshMineStatus;

    rlClanMembershipData m_MemberShips[RL_CLAN_MAX_MEMBERSHIPS];
    unsigned m_NumMemberships;

    bool m_Initialized  : 1;

    //True if we've retrieved our clan membership data.
    //In the case we're in zero clans, m_NumMemberships will be zero,
    //but m_HaveSynchedMemberships will be true.
    bool m_HaveSynchedMemberships   : 1;
};

} //namespace rage

#endif  //RLINE_RLCLANCLIENT_H
