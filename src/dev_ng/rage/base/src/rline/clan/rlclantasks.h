// 
// rline/rlclantasks.h
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLCLANTASKS_H
#define RLINE_RLCLANTASKS_H

#include "rlclancommon.h"
#include "atl/bitset.h"
#include "rline/ros/rlroshttptask.h"

namespace rage
{

class rlClanClient;

class rlClanRosBaseTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlClanRosBaseTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosBaseTask();
    virtual ~rlClanRosBaseTask();

protected:
    int ConvertRosResultToClanError(const rlRosResult& result);
    int ProcessExtractRankInfo(const rage::parTreeNode* desc, rlClanRank& rankInfo);
    int ProcessExtractClanLeader(const rage::parTreeNode* desc, rlClanLeader& clanInfo);
    int ProcessExtractClanInfo(const rage::parTreeNode* desc, rlClanDesc& clanInfo);
    int ProcessExtractPlayerInfo(const rage::parTreeNode* desc, rlRosPlayerInfo& playerInfo);
    int ProcessMembershipRecord(const parTreeNode* record, rlClanMembershipData* membership);

private:
};

class rlClanRosCreateTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosCreateTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosCreateTask();
    virtual ~rlClanRosCreateTask();

    bool Configure(const int localGamerIndex,
        const char* clanName,
        const char* clanTag,
        bool isOpenClan);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
};

class rlClanRosInviteTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosInviteTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosInviteTask();
    virtual ~rlClanRosInviteTask();

    bool Configure(const int localGamerIndex,
        const rlGamerHandle& gamer,
        rlClanId clanId,
        int rankOrder,
        bool expireExtraInvitations);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
};

class rlClanRosDeleteInviteTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosDeleteInviteTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosDeleteInviteTask();
    virtual ~rlClanRosDeleteInviteTask();

    bool Configure(const int localGamerIndex,
        rlClanInviteId id);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosJoinTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosJoinTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosJoinTask();
    virtual ~rlClanRosJoinTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
};

class rlClanRosLeaveTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosLeaveTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosLeaveTask();
    virtual ~rlClanRosLeaveTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:

    rlClanId m_ClanId;
};

class rlClanRosSetPrimaryTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosSetPrimaryTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosSetPrimaryTask();
    virtual ~rlClanRosSetPrimaryTask();

    bool Configure(const int localGamerIndex,
                   rlClanId clanId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
};

class rlClanRosKickTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosKickTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosKickTask();
    virtual ~rlClanRosKickTask();

    bool Configure(const int localGamerIndex,
        const rlGamerHandle& gamer,
        rlClanId clanId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosRankCreateTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosRankCreateTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosRankCreateTask();
    virtual ~rlClanRosRankCreateTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId, 
        const char* rankName,
        s64 initialSystemFlags);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosRankDeleteTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosRankDeleteTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosRankDeleteTask();
    virtual ~rlClanRosRankDeleteTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId, 
        rlClanRankId rankId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosRankUpdateTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosRankUpdateTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosRankUpdateTask();
    virtual ~rlClanRosRankUpdateTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId, 
        rlClanRankId rankId,
        int adjustRankOrder,
        const char* rankName,
        s64 updateSystemFlags);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosDeleteWallMessageTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosDeleteWallMessageTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosDeleteWallMessageTask();
    virtual ~rlClanRosDeleteWallMessageTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId, 
        rlClanWallMessageId msgId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosWriteWallMessageTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosWriteWallMessageTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosWriteWallMessageTask();
    virtual ~rlClanRosWriteWallMessageTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId, 
        const char* message);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosMemberUpdateRankIdTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosMemberUpdateRankIdTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosMemberUpdateRankIdTask();
    virtual ~rlClanRosMemberUpdateRankIdTask();

    bool Configure(const int localGamerIndex,
        const rlGamerHandle& targetGamerHandle,
        rlClanId clanId, 
        bool promote);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosGetInvitesTaskBase : public rlClanRosBaseTask
{
public:
    rlClanRosGetInvitesTaskBase();
    virtual ~rlClanRosGetInvitesTaskBase();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        rlClanInvite* resultInvites,
        unsigned int maxInvitesCount,
        unsigned int* resultInvitesCount,
        unsigned int* resultTotalCount);

protected:
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

protected:
    rlClanInvite* m_ResultInvites;
    unsigned int m_MaxInvitesCount;
    unsigned int* m_ResultInvitesCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetInvitesTask : public rlClanRosGetInvitesTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetInvitesTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetInvitesTask();
    virtual ~rlClanRosGetInvitesTask();

protected:
    virtual const char* GetServiceMethod() const;
};

class rlClanRosGetSentInvitesTask : public rlClanRosGetInvitesTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetSentInvitesTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetSentInvitesTask();
    virtual ~rlClanRosGetSentInvitesTask();

protected:
    virtual const char* GetServiceMethod() const;
};

class rlClanRosGetMembersTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetMembersTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetMembersTask();
    virtual ~rlClanRosGetMembersTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        const char* clanName,
        rlClanMember* resultGamers,
        unsigned int maxGamersCount,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanMember* m_ResultGamers;
    unsigned int m_MaxGamersCount;
    unsigned int* m_ResultGamersCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetMembersTitleOnlyTask : public rlClanRosGetMembersTask
{
public:
    RL_TASK_DECL(rlClanRosGetMembersTitleOnlyTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetMembersTitleOnlyTask();
    virtual ~rlClanRosGetMembersTitleOnlyTask();

protected:
    virtual const char* GetServiceMethod() const;
};

class rlClanRosGetPrimaryClansTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetPrimaryClansTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetPrimaryClansTask();
    virtual ~rlClanRosGetPrimaryClansTask();

    bool Configure(const int localGamerIndex,
        const rlGamerHandle* gamers,
        const unsigned numGamers,
        rlClanMember* resultGamers,
        unsigned int* resultGamersCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanMember* m_ResultGamers;
    unsigned int m_MaxGamersCount;
    unsigned int* m_ResultGamersCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetMyPrimaryClanTask : public rlTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetMyPrimaryClanTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetMyPrimaryClanTask();
    virtual ~rlClanRosGetMyPrimaryClanTask();

    bool Configure(const int localGamerIndex,
                    rlClanDesc* clanDesc);

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void DoCancel();

    virtual void Start();

    virtual void Finish(const FinishType finishType);

    virtual void Update(const unsigned timeStep);

private:
    int m_LocalGamerIndex;
    rlClanDesc* m_ClanDesc;
    rlClanMember m_ClanMember;
    unsigned m_NumClansRetrieved;
    netStatus m_MyStatus;
};

class rlClanRosGetAllTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetAllTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetAllTask();
    virtual ~rlClanRosGetAllTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        const char* searchTerm,
        const rlClanOptionalBool isSystemClan,
        const rlClanOptionalBool isOpenClan,
        rlClanSortMode sortMode,
        rlClanDesc* resultClans,
        unsigned int maxClansCount,
        unsigned int* resultClansCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanDesc* m_ResultClans;
    unsigned int m_MaxClansCount;
    unsigned int* m_ResultClansCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetDescTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetDescTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetDescTask();
    virtual ~rlClanRosGetDescTask();

    bool Configure(const int localGamerIndex,
        const rlClanId clanId,
        rlClanDesc* resultClanDesc);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanDesc* m_Result;
};

class rlClanRosGetDescsTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetDescsTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetDescsTask();
    virtual ~rlClanRosGetDescsTask();

    bool Configure(const int localGamerIndex,
                   const rlClanId* clans,
                   unsigned int clansCount,
                   rlClanDesc* clanDescs);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanDesc* m_ClanDescs;
    unsigned int m_ClansCount;
};

class rlClanRosGetLeadersForClansTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetLeadersForClansTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetLeadersForClansTask();
    virtual ~rlClanRosGetLeadersForClansTask();

    bool Configure(const int localGamerIndex,
                   const rlClanId* clans,
                   unsigned int clansCount,
                   rlClanLeader* resultLeaders,
                   unsigned int* resultCoult);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanLeader* m_ResultLeaders;
    unsigned int m_ClansCount;
    unsigned int* m_ResultCount;
};

class rlClanRosGetRanksTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetRanksTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetRanksTask();
    virtual ~rlClanRosGetRanksTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId, 
        rlClanRank* resultRanks,
        unsigned int maxRanksCount,
        unsigned int* resultRanksCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanRank* m_ResultRanks;
    unsigned int m_MaxRanksCount;
    unsigned int* m_ResultRanksCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetMembershipTaskBase : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetMembershipTaskBase);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetMembershipTaskBase();
    virtual ~rlClanRosGetMembershipTaskBase();

protected:
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

protected:
    rlClanMembershipData* m_ResultMemberships;
    unsigned int m_MaxMembershipsCount;
    unsigned int* m_ResultMembershipsCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetMembershipForTask : public rlClanRosGetMembershipTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetMembershipForTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetMembershipForTask();
    virtual ~rlClanRosGetMembershipForTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        const rlGamerHandle& targetGamerHandle,
        rlClanMembershipData* memberships,
        unsigned int maxMembershipCount,
        unsigned int* resultMembershipsCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
};


class rlClanRosGetMineTask : public rlTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetMineTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetMineTask();
    virtual ~rlClanRosGetMineTask();

    bool Configure(rlClanClient* client,
                    rlClanMembershipData* memberships,
                    unsigned int maxMembershipCount,
                    unsigned int* resultMembershipsCount,
                    unsigned int* resultTotalCount);

    virtual bool IsCancelable() const
    {
        return true;
    }

    virtual void Update(const unsigned timeStep);

private:

    virtual void DoCancel();

    enum State
    {
        STATE_CHECKING_CACHED_DATA,
        STATE_REFRESHING_CACHED_DATA
    };

    State m_State;

    rlClanClient* m_Client;
    rlClanMembershipData* m_Memberships;
    unsigned m_MaxMemberships;
    unsigned* m_NumMemberships;
    unsigned* m_TotalMemberships;

    netStatus m_MyStatus;
};


class rlClanRosRefreshMineTask : public rlClanRosGetMembershipTaskBase
{
public:
    RL_TASK_DECL(rlClanRosRefreshMineTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosRefreshMineTask();
    virtual ~rlClanRosRefreshMineTask();

    bool Configure(rlClanClient* client);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    rlClanMembershipData m_MemberShips[RL_CLAN_MAX_MEMBERSHIPS];
    unsigned m_NumMemberships;
    unsigned m_TotalMemberships;
    rlClanClient* m_Client;
};

class rlClanRosGetWallMessagesTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetWallMessagesTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetWallMessagesTask();
    virtual ~rlClanRosGetWallMessagesTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanWallMessage* messages,
        unsigned int maxMessageCount,
        unsigned int* resultMessageCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanWallMessage* m_ResultMessages;
    unsigned int m_MaxMessageCount;
    unsigned int* m_ResultMessageCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetMetadataForClanTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetMetadataForClanTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetMetadataForClanTask();
    virtual ~rlClanRosGetMetadataForClanTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanMetadataEnum* enums,
        unsigned int maxEnumCount,
        unsigned int* resultEnumCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanMetadataEnum* m_ResultEnums;
    unsigned int m_MaxEnumCount;
    unsigned int* m_ResultEnumCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosJoinRequestTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosJoinRequestTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosJoinRequestTask();
    virtual ~rlClanRosJoinRequestTask();

    bool Configure(const int localGamerIndex,
        rlClanId clanId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosGetSentJoinRequestsTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetSentJoinRequestsTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetSentJoinRequestsTask();
    virtual ~rlClanRosGetSentJoinRequestsTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        rlClanJoinRequestSent* requests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanJoinRequestSent* m_ResultRequests;
    unsigned int m_MaxRequestsCount;
    unsigned int* m_ResultRequestsCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosGetRecievedJoinRequestsTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetRecievedJoinRequestsTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetRecievedJoinRequestsTask();
    virtual ~rlClanRosGetRecievedJoinRequestsTask();

    bool Configure(const int localGamerIndex,
        int pageIndex,
        rlClanId clanId,
        rlClanJoinRequestRecieved* requests,
        unsigned int maxRequestsCount,
        unsigned int* resultRequestsCount,
        unsigned int* resultTotalCount);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    rlClanJoinRequestRecieved* m_ResultRequests;
    unsigned int m_MaxRequestsCount;
    unsigned int* m_ResultRequestsCount;
    unsigned int* m_ResultTotalCount;
};

class rlClanRosDeleteJoinRequestTask : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosDeleteJoinRequestTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosDeleteJoinRequestTask();
    virtual ~rlClanRosDeleteJoinRequestTask();

    bool Configure(const int localGamerIndex,
        rlClanRequestId requestId);

protected:
    virtual const char* GetServiceMethod() const;
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlClanRosGetClanEmblemTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlClanRosGetClanEmblemTask);
	RL_TASK_USE_CHANNEL(rline_clan);

	rlClanRosGetClanEmblemTask();
	virtual ~rlClanRosGetClanEmblemTask();

	bool Configure(const int localGamerIndex,
		rlClanId clanId,
		rlClanEmblemSize emblemSize,
        const u64 ifModifiedSincePosixTime,
		const fiDevice* responseDevice,
		const fiHandle responseHandle );

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	virtual void Start();
	virtual void Update(const unsigned timeStepMs);
	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	static const char* GetCrewEmblemCloudPath(rlClanEmblemSize emblemSize, char* outPath, const unsigned int strLen);
private:

	enum State
	{
		STATE_NONE,
		STATE_RECEIVING,
		STATE_DONE,
		STATE_ERROR
	};
	
	State m_State;
	netStatus m_MyStatus;

	// fiHandle m_fiHandle;
	// const fiDevice* m_fiDevice;

};

class rlClanRosGetFeudTaskBase : public rlClanRosBaseTask
{
public:
    RL_TASK_DECL(rlClanRosGetFeudTaskBase);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetFeudTaskBase();
    virtual ~rlClanRosGetFeudTaskBase();

protected:
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

protected:
    rlClanFeuder* m_ResultFeuders;
    unsigned int m_MaxFeuderCount;
    unsigned int* m_ResultFeuderCount;

    const char* GetSinceString(rlClanFeudSinceCode since);

    int ProcessFeuder(const parTreeNode* record, rlClanFeuder& feuder);
};

class rlClanRosGetFeudStatsTask: public rlClanRosGetFeudTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetFeudStatsTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetFeudStatsTask();
    virtual ~rlClanRosGetFeudStatsTask();

    bool Configure(const int localGamerIndex,
        const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount);

protected:
    virtual const char* GetServiceMethod() const;
};

class rlClanRosGetTopRivalsTask: public rlClanRosGetFeudTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetTopRivalsTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetTopRivalsTask();
    virtual ~rlClanRosGetTopRivalsTask();

    bool Configure(const int localGamerIndex,
        const rlClanId clanId,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount);

protected:
    virtual const char* GetServiceMethod() const;
};

class rlClanRosGetTopFeudersTask: public rlClanRosGetFeudTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetTopFeudersTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetTopFeudersTask();
    virtual ~rlClanRosGetTopFeudersTask();

    bool Configure(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount);

protected:
    virtual const char* GetServiceMethod() const;
};

class rlClanRosGetTopFeudWinnersTask: public rlClanRosGetFeudTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetTopFeudWinnersTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetTopFeudWinnersTask();
    virtual ~rlClanRosGetTopFeudWinnersTask();

    bool Configure(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount);

protected:
    virtual const char* GetServiceMethod() const;
};

class rlClanRosGetTopFeudLosersTask: public rlClanRosGetFeudTaskBase
{
public:
    RL_TASK_DECL(rlClanRosGetTopFeudLosersTask);
    RL_TASK_USE_CHANNEL(rline_clan);

    rlClanRosGetTopFeudLosersTask();
    virtual ~rlClanRosGetTopFeudLosersTask();

    bool Configure(const int localGamerIndex,
        const rlClanFeudSinceCode since,
        const unsigned int maxFeuderCount,
        rlClanFeuder* resultFeuders,
        unsigned int* resultFeuderCount);

protected:
    virtual const char* GetServiceMethod() const;
};

};

#endif //RLINE_RLCLANTASKS_H
