// 
// rline/rlclanclient.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlclan.h"
#include "rlclanclient.h"
#include "rlclancommon.h"
#include "rlclantasks.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "system/nelem.h"

namespace rage
{

//Override output macros.
#undef __rage_channel
#define __rage_channel rline_clan

//------------------------------------------------------------------------------
// rlClanClient
//------------------------------------------------------------------------------
rlClanClient::rlClanClient()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_NumMemberships(0)
, m_Initialized(false)
, m_HaveSynchedMemberships(false)
{
    // NOOP
}

rlClanClient::~rlClanClient()
{
    Shutdown();
}

bool 
rlClanClient::Init(const int localGamerIndex)
{
    if(rlVerify(!m_Initialized))
    {
        m_LocalGamerIndex = localGamerIndex;
        m_Initialized = true;
        Restart(true);
        return true;
    }

    return false;
}

void 
rlClanClient::Shutdown()
{
    if(m_Initialized)
    {
        this->CancelAllTasks();
        m_State = STATE_INVALID;
        m_NumMemberships = 0;
        m_Initialized = false;
        m_HaveSynchedMemberships = false;
    }
}

void 
rlClanClient::Restart(bool /*forceNow*/)
{
    if(rlVerify(m_Initialized))
    {
        this->CancelAllTasks();
        m_State = STATE_WAIT_FOR_CREDS;
        m_NumMemberships = 0;
        m_HaveSynchedMemberships = false;
    }
}

void 
rlClanClient::Update()
{
    const rlRosCredentials& creds = rlRos::GetCredentials(m_LocalGamerIndex);

    if(m_RefreshMineStatus.Failed())
    {
        rlError("rlClanClient[%d]::Update: RefreshMine failed", m_LocalGamerIndex);
        m_RefreshMineStatus.Reset();
    }

    switch (m_State)
    {
    case STATE_WAIT_FOR_CREDS:
        if(creds.IsValid())
        {
            rlDebug("rlClanClient[%d]::Update: Got creds", m_LocalGamerIndex);
            rlVerify(rlPresence::GetGamerHandle(m_LocalGamerIndex, &m_MyGamerHandle));
            if(creds.IsScMember() && creds.HasPrivilege(RLROS_PRIVILEGEID_CLAN))
            {
                //Populate our cache
                RefreshMine();
                m_State = STATE_RUNNING_AS_SC_MEMBER;
            }
            else
            {
				rlDebug("rlClanClient[%d]::Update: Setting to STATE_RUNNING, is SC Member: %s, HasPrivilege: %s", m_LocalGamerIndex,
							creds.IsScMember() ? "true" : "false", creds.HasPrivilege(RLROS_PRIVILEGEID_CLAN) ? "true" : "false");

                m_State = STATE_RUNNING;
            }
        }
        break;
    case STATE_RUNNING_AS_SC_MEMBER:
        if(!creds.IsValid() || !creds.IsScMember() || !creds.HasPrivilege(RLROS_PRIVILEGEID_CLAN))
        {
            rlDebug("rlClanClient[%d]::Update: Online(%s), Linked(%s), Privileges(%s), restarting", m_LocalGamerIndex,
				creds.IsValid() ? "true" : "false", creds.IsScMember() ? "true" : "false", creds.HasPrivilege(RLROS_PRIVILEGEID_CLAN) ? "true" : "false");

            Restart(true);
        }
        break;
    case STATE_RUNNING:
        if(!creds.IsValid())
        {
            rlDebug("rlClanClient[%d]::Update: Offline, restarting", m_LocalGamerIndex);
            Restart(true);
        }
        else if(creds.IsScMember() && creds.HasPrivilege(RLROS_PRIVILEGEID_CLAN))
        {
            rlDebug("rlClanClient[%d]::Update: Now linked to SC with clan privileges, restarting", m_LocalGamerIndex);
            Restart(true);
        }
        break;
    default:
        rlError("rlClanClient[%d]::Update: Unhandled state %d", m_LocalGamerIndex, m_State);
    }
}

bool
rlClanClient::HavePrimaryClan() const
{
    return GetPrimaryClan().IsValid();
}

const rlClanDesc&
rlClanClient::GetPrimaryClan() const
{
    if(m_NumMemberships)
    {
        for(int i = 0; i < (int)m_NumMemberships; ++i)
        {
            if(m_MemberShips[i].m_IsPrimary)
            {
                return m_MemberShips[i].m_Clan;
            }
        }
    }

    return RL_INVALID_CLAN_DESC;
}

const rlClanMembershipData& 
rlClanClient::GetPrimaryMembership() const
{
    if(m_NumMemberships)
    {
        for(int i = 0; i < (int)m_NumMemberships; ++i)
        {
            if(m_MemberShips[i].m_IsPrimary)
            {
                return m_MemberShips[i];
            }
        }
    }

    return RL_INVALID_CLAN_MEMBERSHIP_DATA;
}

bool
rlClanClient::UpdatePrimaryClan(const rlClanId oldPrimaryClanId,
                                const rlClanId newPrimaryClanId)
{
    //Don't try to optimize this function by checking if clanId
    //is already our primary.  Reason is that in some cases we call
    //AddMembership() with a clan with m_IsPrimary true, then later call
    //UpdatePrimaryClan().

    //One of the two must be valid
    if(!rlVerifyf(RL_INVALID_CLAN_ID != oldPrimaryClanId
                    || RL_INVALID_CLAN_ID != newPrimaryClanId,
                    "Invalid clan IDs"))
    {
        return false;
    }

    if(RL_INVALID_CLAN_ID != oldPrimaryClanId)
    {
        OUTPUT_ONLY(bool foundIt = false;)

        for(int i = 0; i < (int)m_NumMemberships; ++i)
        {
            if(oldPrimaryClanId == m_MemberShips[i].m_Clan.m_Id)
            {
                rlAssert(!foundIt);
                OUTPUT_ONLY(foundIt = true);

                m_MemberShips[i].m_IsPrimary = false;
            }
        }
    }

    if(RL_INVALID_CLAN_ID != newPrimaryClanId)
    {
        if(rlVerifyf(HasMembership(newPrimaryClanId),
                    "Can't set primary clan - gamer: %d is not a member of clan: %" I64FMT "d",
                    m_LocalGamerIndex,
                    newPrimaryClanId))
        {
            rlDebug("Updating primary clan to %" I64FMT "d (from %" I64FMT "d) for gamer: %d...",
                    newPrimaryClanId,
					oldPrimaryClanId,
                    m_LocalGamerIndex);
        
            OUTPUT_ONLY(bool foundIt = false;)

            for(int i = 0; i < (int)m_NumMemberships; ++i)
            {
                if(newPrimaryClanId == m_MemberShips[i].m_Clan.m_Id)
                {
                    rlAssert(!foundIt);
                    OUTPUT_ONLY(foundIt = true);

                    m_MemberShips[i].m_IsPrimary = true;

                    rlDebug("  Clan ID:     %" I64FMT "d", m_MemberShips[i].m_Clan.m_Id);
                    rlDebug("  Clan Name:   '%s'", m_MemberShips[i].m_Clan.m_ClanName);
                    rlDebug("  Clan Tag:    '%s'", m_MemberShips[i].m_Clan.m_ClanTag);
                    rlDebug("  Clan Motto:  '%s'", m_MemberShips[i].m_Clan.m_ClanMotto);
                    rlDebug("  Is Primary:  %s", m_MemberShips[i].m_IsPrimary?"true":"false");
                }
            }
        }
    }

	//However, we only want to send this primary changed event if the primary is changing in a valid context
	if(oldPrimaryClanId != newPrimaryClanId && m_HaveSynchedMemberships)
	{
		rlClanEventPrimaryClanChanged e(m_LocalGamerIndex);
		rlClan::DispatchEvent(e);
	}

    //Set the crew ID presence attribute and subscribe to
    //the crew message channel.
    UpdatePresence(oldPrimaryClanId, newPrimaryClanId);

    return true;
}

bool 
rlClanClient::UpdateCachedClanRank(const rlClanId clanId, int rankOrder, const char* rankName)
{
	for(unsigned i = 0; i < m_NumMemberships; ++i)
    {
        if(m_MemberShips[i].m_Clan.m_Id == clanId)
        {
			rlDebug("rlClanClient[%d]::UpdateCachedClanRank: Updating clan %" I64FMT "d rank to order %d name %s", m_LocalGamerIndex, clanId, rankOrder, rankName);

            m_MemberShips[i].m_Rank.m_RankOrder = rankOrder;
			safecpy(m_MemberShips[i].m_Rank.m_RankName, rankName, RL_CLAN_RANK_NAME_MAX_CHARS);
			return true;
        }
    }

	rlError("rlClanClient[%d]::UpdateCachedClanRank: Failed to locally update clan %" I64FMT "d rank (order %d name %s)", m_LocalGamerIndex, clanId, rankOrder, rankName);

	return false;
}

void
rlClanClient::UpdatePresence(const rlClanId /*oldPrimaryClanId*/,
                            const rlClanId newPrimaryClanId)
{
    //Set the crew ID presence attribute
    rlVerifyf(rlPresenceAttributeHelper::SetCrewId(m_LocalGamerIndex, newPrimaryClanId),
            "Error setting crew ID presence attribute");
}

bool
rlClanClient::RefreshMine(const RefreshQueueType refreshQueueType,
                          netStatus* status)
{
    rlClanRosRefreshMineTask* task = NULL;

    rlDebug("Refreshing clan membership cache for gamer: %d...", m_LocalGamerIndex);

    rtry
    {
        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::RefreshMine: Failed to allocate task", m_LocalGamerIndex));

        rcheck(rlTaskBase::Configure(task, this, status), catchall,
                rlError("rlClanClient[%d]::RefreshMine: Failed to configure task", m_LocalGamerIndex));

        if(REFRESH_QUEUE_SERIAL == refreshQueueType)
        {
            rcheck(this->QueueTask(task), catchall, 
                    rlError("rlClanClient[%d]::RefreshMine: Failed to add serial task", m_LocalGamerIndex));
        }
        else
        {
            rcheck(this->AddParallelTask(task), catchall, 
                    rlError("rlClanClient[%d]::RefreshMine: Failed to add parallel task", m_LocalGamerIndex));
        }

        return true;
    }
    rcatchall
    {
        if(task)
        {
            this->DestroyTask(task);
        }
    }

    status->ForceFailed();
    return false;
}

bool
rlClanClient::RefreshMine()
{
    if(!m_RefreshMineStatus.Pending())
    {
        return RefreshMine(REFRESH_QUEUE_SERIAL, &m_RefreshMineStatus);
    }

    return true;
}

bool
rlClanClient::HasMembership(const rlClanId clanId) const
{
	for(int i = 0; i < (int)m_NumMemberships; ++i)
	{
		if(clanId == m_MemberShips[i].m_Clan.m_Id)
		{
			return true;
		}
	}

	return false;
}

rlClanId
rlClanClient::GetClanId(const int index) const
{
	if (rlVerify(index>=0) && rlVerify(index<(int)m_NumMemberships))
	{
		return m_MemberShips[index].m_Clan.m_Id;
	}

	return RL_INVALID_CLAN_ID;
}

bool
rlClanClient::AddMembership(const rlClanMembershipData& membership,
                            const bool isNewlyJoined)
{
    rlDebug("Adding clan %" I64FMT "d to memberships for gamer: %d...",
            membership.m_Clan.m_Id, m_LocalGamerIndex);

    if(rlVerifyf(m_NumMemberships < COUNTOF(m_MemberShips),
                "Too many clan memberships!!!")
        && rlVerifyf(!isNewlyJoined || !HasMembership(membership.m_Clan.m_Id),
                    "Already a member of clan %" I64FMT "d!!!", membership.m_Clan.m_Id))
    {
       
        if(!HasMembership(membership.m_Clan.m_Id))
        {
            rlDebug("  ID:          %" I64FMT "d", membership.m_Clan.m_Id);
            rlDebug("    Name:      '%s'", membership.m_Clan.m_ClanName);
            rlDebug("    Tag:       '%s'", membership.m_Clan.m_ClanTag);
            rlDebug("    Motto:     '%s'", membership.m_Clan.m_ClanMotto);
            rlDebug("    Primary:   %s", membership.m_IsPrimary?"true":"false");

			const rlClanId oldPrimaryClanId = HavePrimaryClan() ? GetPrimaryClan().m_Id : RL_INVALID_CLAN_ID;

            m_MemberShips[m_NumMemberships] = membership;
            ++m_NumMemberships;

            if(isNewlyJoined)
            {
                rlClanEventJoined e(m_LocalGamerIndex, membership.m_Clan);
                rlClan::DispatchEvent(e);
            }

            if(membership.m_IsPrimary)
            {
                UpdatePrimaryClan(oldPrimaryClanId, membership.m_Clan.m_Id);
            }
        }
        return true;
    }

    return false;
}

void
rlClanClient::RemoveMembership(const rlClanId removedClanId,
                                const rlClanId newPrimaryClanId)
{
    rlDebug("Removing clan %" I64FMT "d from memberships for gamer: %d...",
            removedClanId, m_LocalGamerIndex);

    for(int i = 0; i < (int)m_NumMemberships; ++i)
    {
        if(removedClanId == m_MemberShips[i].m_Clan.m_Id)
        {
            rlDebug("  ID:          %" I64FMT "d", m_MemberShips[i].m_Clan.m_Id);
            rlDebug("    Name:      '%s'", m_MemberShips[i].m_Clan.m_ClanName);
            rlDebug("    Tag:       '%s'", m_MemberShips[i].m_Clan.m_ClanTag);
            rlDebug("    Motto:     '%s'", m_MemberShips[i].m_Clan.m_ClanMotto);
            rlDebug("    Primary:   %s", m_MemberShips[i].m_IsPrimary?"true":"false");

            const bool isPrimary = m_MemberShips[i].m_IsPrimary;

            --m_NumMemberships;
            m_MemberShips[i] = m_MemberShips[m_NumMemberships];

            rlClanEventLeft e(m_LocalGamerIndex, removedClanId);
            rlClan::DispatchEvent(e);

            if(isPrimary)
            {
                //Notify the app our primary clan has changed.
                UpdatePrimaryClan(removedClanId, newPrimaryClanId);
            }
            else
            {
                rlAssert(newPrimaryClanId == this->GetPrimaryClan().m_Id);
            }

            break;
        }
    }
}

bool
rlClanClient::GetMine(rlClanMembershipData* resultMembership,
                        unsigned int maxMembershipCount,
                        unsigned int* resultMembershipCount,
                        unsigned int* resultTotalCount,
                        netStatus* status)
{
    rlClanRosGetMineTask* task = NULL;

    rlDebug("Retrieving clan membership for gamer: %d...", m_LocalGamerIndex);

    rtry
    {
        rverify(NULL != resultMembership, catchall, rlError("Invalid membership array"));
        rverify(NULL != resultMembershipCount, catchall, rlError("Invalid membership count pointer"));
        rcheck(CheckCredentials(), catchall, );

		//If a NULL netstatus is passed in, it's asking to retrieve the cached memberships.
		//If they haven't been retrieved yet, just bail as false.
		if (status == NULL && !m_HaveSynchedMemberships)
		{
			rlDebug("NULL netstatus and no cached memberships");
			return false;
		}
        
		if(m_HaveSynchedMemberships)
        {
            rlDebug("Using cached clan data...");
            CopyMembershipsTo(resultMembership,
                                maxMembershipCount,
                                resultMembershipCount,
                                resultTotalCount);

			if(status)
			{
				status->ForceSucceeded();
			}

            return true;
        }
        else
        {
            //If there is already another rlClanRosRefreshMineTask in
            //flight then it will retrieve the data from the servers.
            //When it completes then this task will copy the data from
            //the newly refreshed cache.
            rcheck(this->CreateTask(&task), catchall, 
                    rlError("rlClanClient[%d]::GetMine: Failed to allocate task", m_LocalGamerIndex));
            rcheck(rlTaskBase::Configure(task,
                                        this,
                                        resultMembership,
                                        maxMembershipCount,
                                        resultMembershipCount,
                                        resultTotalCount,
                                        status), catchall,
                    rlError("rlClanClient[%d]::GetMine: Failed to configure task", m_LocalGamerIndex));
            rcheck(this->QueueTask(task), catchall, 
                    rlError("rlClanClient[%d]::GetMine: Failed to add task", m_LocalGamerIndex));

            return true;
        }
    }
    rcatchall
    {
        if(task)
        {
            this->DestroyTask(task);
        }
    }

	if(status)
	{
		status->ForceFailed();
	}

    return false;
}

bool 
rlClanClient::Create(const char* clanName,
                     const char* clanTag,
                     bool isOpenClan,
                     netStatus* status)
{
    rlDebug("Gamer: %d creating clan '%s'...", m_LocalGamerIndex, clanName);

    rlClanRosCreateTask* task = NULL;
    rtry
    {
        rverify(NULL != clanName, catchall, rlError("Invalid clan name"));
        rverify(NULL != clanTag, catchall, rlError("Invalid clan tag"));

        rcheck(CheckCredentials(), catchall, );

        rverify(StringLength(clanName) < RL_CLAN_NAME_MAX_CHARS,
                catchall,
                rlError("Clan name is too long"));
        rverify(StringLength(clanTag) < RL_CLAN_TAG_MAX_CHARS,
                catchall,
                rlError("Clan tag is too long"));

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::Create: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanName, clanTag, isOpenClan, status), catchall, 
            rlError("rlClanClient[%d]::Create: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::Create: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::Leave(rlClanId clanId, netStatus* status)
{
    rlDebug("Gamer: %d leaving clan %" I64FMT "d...",
            m_LocalGamerIndex, clanId);

    rlClanRosLeaveTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));

        rcheck(CheckCredentials(), catchall, );

        rverify(HasMembership(clanId),
                catchall,
                status->ForceFailed(RL_CLAN_ERR_PLAYER_NOT_FOUND); rlError("Not a member of clan %" I64FMT "d", clanId));

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::Leave: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, status), catchall, 
            rlError("rlClanClient[%d]::Leave: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::Leave: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

	if( !status->Failed() )
		status->ForceFailed();
    return false;
}

bool 
rlClanClient::SetPrimaryClan(rlClanId clanId, netStatus* status)
{
    rlDebug("Setting primary clan for gamer: %d to %" I64FMT "d...",
            m_LocalGamerIndex, clanId);

    rlClanRosSetPrimaryTask* task = NULL;
    rtry
    {
        rcheck(CheckCredentials(), catchall, );

        rverify(HasMembership(clanId),
                catchall,
                rlError("Can't set primary clan - gamer: %d is not a member of clan: %" I64FMT "d",
                        m_LocalGamerIndex,
                        clanId));

        if(clanId == GetPrimaryClan().m_Id)
        {
            rlDebug("Clan %" I64FMT "d is already the primary", clanId);
            return true;
        }

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::SetPrimary: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, status), catchall, 
            rlError("rlClanClient[%d]::SetPrimary: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::SetPrimary: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::Invite(const rlGamerHandle& gamer,
                     rlClanId clanId, 
                     int rankOrder,
                     bool expireExtraInvitations,
                     netStatus* status)
{
    rlClanRosInviteTask* task = NULL;
    rtry
    {
        rcheck(gamer.IsValid(), catchall, rlError("Invalid gamer"));
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::Invite: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, gamer, clanId, rankOrder, expireExtraInvitations, status), catchall, 
            rlError("rlClanClient[%d]::Invite: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::Invite: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::DeleteInvite(rlClanInviteId inviteId,
                           netStatus* status)
{
    rlClanRosDeleteInviteTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_INVITE_ID != inviteId, catchall, rlError("Invalid invite id"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::DeleteInvite: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, inviteId, status), catchall, 
            rlError("rlClanClient[%d]::DeleteInvite: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::DeleteInvite: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::Join(rlClanId clanId,
                   netStatus* status)
{
    rlDebug("Gamer: %d joining clan %" I64FMT "d...",
            m_LocalGamerIndex, clanId);

    rlClanRosJoinTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));

        rcheck(CheckCredentials(), catchall, );

        rverify(!HasMembership(clanId),
                catchall,
                status->ForceFailed(RL_CLAN_ERR_ALREADY_IN_CLAN); rlError("Already a member of clan %" I64FMT "d", clanId));

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::Join: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, status), catchall, 
            rlError("rlClanClient[%d]::Join: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::Join: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

	// preferably we'd use the throws above to check this, but this'll do.
	if( !status->Failed() )
		status->ForceFailed();

    return false;
}

bool 
rlClanClient::Kick(const rlGamerHandle& gamer,
                   rlClanId clanId, 
                   netStatus* status)
{
    rlClanRosKickTask* task = NULL;
    rtry
    {
        rcheck(gamer.IsValid(), catchall, rlError("Invalid gamer"));
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::Kick: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, gamer, clanId, status), catchall, 
            rlError("rlClanClient[%d]::Kick: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::Kick: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::RankCreate(rlClanId clanId, 
        const char* rankName,
        s64 initialSystemFlags,
        netStatus* status)
{
    rlClanRosRankCreateTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));
        rverify(NULL != rankName, catchall, rlError("Invalid rank name"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::RankCreate: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, rankName, initialSystemFlags, status), catchall, 
            rlError("rlClanClient[%d]::RankCreate: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::RankCreate: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::RankDelete(rlClanId clanId, 
        rlClanRankId rankId,
        netStatus* status)
{
    rlClanRosRankDeleteTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));
        rverify(RL_INVALID_RANK_ID != rankId, catchall, rlError("Invalid rank id"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::RankDelete: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, rankId, status), catchall, 
            rlError("rlClanClient[%d]::RankDelete: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::RankDelete: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::RankUpdate(rlClanId clanId, 
        rlClanRankId rankId,
        int adjustRankOrder,
        const char* rankName,
        s64 updateSystemFlags,
        netStatus* status)
{
    rlClanRosRankUpdateTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));
        rverify(RL_INVALID_RANK_ID != rankId, catchall, rlError("Invalid rank id"));
        rverify(NULL != rankName, catchall, rlError("Invalid rank name"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::RankUpdate: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, rankId, 
                                     adjustRankOrder, rankName, updateSystemFlags, status), catchall, 
            rlError("rlClanClient[%d]::RankUpdate: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::RankUpdate: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::DeleteWallMessage(rlClanId clanId, 
        rlClanWallMessageId msgId,
        netStatus* status)
{
    rlClanRosDeleteWallMessageTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::DeleteWallMessage: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, msgId, status), catchall, 
            rlError("rlClanClient[%d]::DeleteWallMessage: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::DeleteWallMessage: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::WriteWallMessage(rlClanId clanId, 
        const char* message,
        netStatus* status)
{
    rlClanRosWriteWallMessageTask* task = NULL;
    rtry
    {
        rverify(NULL != message, catchall, rlError("Invalid message"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::WriteWallMessage: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, message, status), catchall, 
            rlError("rlClanClient[%d]::WriteWallMessage: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::WriteWallMessage: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::MemberUpdateRankId(const rlGamerHandle& gamer,
                                 rlClanId clanId,
                                 bool promote,
                                 netStatus* status)
{
    rlClanRosMemberUpdateRankIdTask* task = NULL;
    rtry
    {
        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall, 
                rlError("rlClanClient[%d]::MemberUpdateRankId: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, gamer, clanId, promote, status), catchall, 
            rlError("rlClanClient[%d]::MemberUpdateRankId: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::MemberUpdateRankId: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetInvites(int pageIndex,
                         rlClanInvite* resultInvites,
                         unsigned int maxInvitesCount,
                         unsigned int* resultInvitesCount,
                         unsigned int* resultTotalCount,
                         netStatus* status)
{
    rlClanRosGetInvitesTask* task = NULL;
    rtry
    {
        rverify(NULL != resultInvites, catchall, rlError("Invalid invite array"));
        rverify(NULL != resultInvitesCount, catchall, rlError("Invalid invite count pointer"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetInvites: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, 
            resultInvites, maxInvitesCount, resultInvitesCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetInvites: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetInvites: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetSentInvites(int pageIndex,
                             rlClanInvite* resultInvites,
                             unsigned int maxInvitesCount,
                             unsigned int* resultInvitesCount,
                             unsigned int* resultTotalCount,
                             netStatus* status)
{
    rlClanRosGetSentInvitesTask* task = NULL;
    rtry
    {
        rverify(NULL != resultInvites, catchall, rlError("Invalid invite array"));
        rverify(NULL != resultInvitesCount, catchall, rlError("Invalid invite count pointer"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetSentInvites: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, 
            resultInvites, maxInvitesCount, resultInvitesCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetSentInvites: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetSentInvites: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetAll(int pageIndex,
                     rlClanDesc* resultClans,
                     unsigned int maxClansCount,
                     unsigned int* resultClansCount,
                     unsigned int* resultTotalCount,
                     netStatus* status)
{
    rlClanRosGetAllTask* task = NULL;
    rtry
    {
        rverify(NULL != resultClans, catchall, rlError("Invalid clan array"));
        rverify(NULL != resultClansCount, catchall, rlError("Invalid clan count pointer"));

		rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetAll: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, (char*)NULL, RL_CLAN_OMITTED, RL_CLAN_OMITTED, RL_CLAN_SORT_NONE,
            resultClans, maxClansCount, resultClansCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetAll: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetAll: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetAllByClanName(int pageIndex,
                               const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],
                               rlClanDesc* resultClans,
                               unsigned int maxClansCount,
                               unsigned int* resultClansCount,
                               unsigned int* resultTotalCount,
                               netStatus* status)
{
    rlClanRosGetAllTask* task = NULL;
    rtry
    {
        rverify(NULL != resultClans, catchall, rlError("Invalid clan array"));
        rverify(NULL != resultClansCount, catchall, rlError("Invalid clan count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetAllByClanName: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, clanName, RL_CLAN_OMITTED, RL_CLAN_OMITTED, RL_CLAN_SORT_NONE,
            resultClans, maxClansCount, resultClansCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetAllByClanName: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetAllByClanName: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetSystemClans(int pageIndex,
                             rlClanDesc* resultClans,
                             unsigned int maxClansCount,
                             unsigned int* resultClansCount,
                             unsigned int* resultTotalCount,
                             netStatus* status)
{
    rlClanRosGetAllTask* task = NULL;
    rtry
    {
        rverify(NULL != resultClans, catchall, rlError("Invalid clan array"));
        rverify(NULL != resultClansCount, catchall, rlError("Invalid clan count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetSystemClans: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, (char*)NULL, RL_CLAN_TRUE, RL_CLAN_OMITTED, RL_CLAN_SORT_NONE,
            resultClans, maxClansCount, resultClansCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetSystemClans: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetSystemClans: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetOpenClans(int pageIndex,
                           rlClanDesc* resultClans,
                           unsigned int maxClansCount,
                           unsigned int* resultClansCount,
                           unsigned int* resultTotalCount,
                           netStatus* status)
{
    rlClanRosGetAllTask* task = NULL;
    rtry
    {
        rverify(NULL != resultClans, catchall, rlError("Invalid clan array"));
        rverify(NULL != resultClansCount, catchall, rlError("Invalid clan count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, );  //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetOpenClans: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, (char*)NULL, RL_CLAN_TRUE, RL_CLAN_TRUE, RL_CLAN_SORT_BY_COUNT,
            resultClans, maxClansCount, resultClansCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetOpenClans: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetOpenClans: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetDesc(const rlClanId clanId,
                      rlClanDesc* resultClanDesc,
                      netStatus* status)
{
    rlClanRosGetDescTask* task = NULL;
    rtry
    {
        rverify(NULL != resultClanDesc, catchall, rlError("Invalid clan array"));

		rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetDesc: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, resultClanDesc, status), catchall, 
            rlError("rlClanClient[%d]::GetDesc: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetDesc: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetDescs(const rlClanId* clans,
                       unsigned int clansCount,
                       rlClanDesc* resultClanDescs,
                       netStatus* status)
{
    rlClanRosGetDescsTask* task = NULL;
    rtry
    {
        rverify(NULL != resultClanDescs, catchall, rlError("Invalid clan array"));

        rverify(NULL != clans,
                catchall,
                rlError("Invalid clan ID array"));

        rverify(clansCount > 0,
                catchall,
                rlError("No clan descriptions requested"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetDescs: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clans, clansCount, resultClanDescs, status), catchall, 
            rlError("rlClanClient[%d]::GetDescs: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetDescs: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetLeadersForClans(const rlClanId* clans,
                                 unsigned int clansCount,
                                 rlClanLeader* resultLeaders,
                                 unsigned int* resultCount,
                                 netStatus* status)
{
    rlClanRosGetLeadersForClansTask* task = NULL;
    rtry
    {
        rverify(NULL != clans,
                catchall,
                rlError("Invalid clan ID array"));

        rverify(clansCount > 0,
                catchall,
                rlError("No clan descriptions requested"));

       rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetLeadersForClans: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clans, clansCount, resultLeaders, resultCount, status), catchall, 
            rlError("rlClanClient[%d]::GetLeadersForClans: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetLeadersForClans: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetPrimaryClans(const rlGamerHandle* gamers,
                              const unsigned numGamers,
                              rlClanMember* resultGamers,
                              unsigned int* resultGamersCount,
                              unsigned int* resultTotalCount,
                              netStatus* status)
{
    rlClanRosGetPrimaryClansTask* task = NULL;
    rtry
    {
        rverify(NULL != gamers, catchall, rlError("Invalid gamers array"));
        rverify(NULL != resultGamers, catchall, rlError("Invalid gameres result array"));
        rverify(NULL != resultGamersCount, catchall, rlError("Invalid gamers count pointer"));

       rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetPrimaryClans: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, gamers, numGamers, resultGamers, 
                                     resultGamersCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetPrimaryClans: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetPrimaryClans: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetMembersTitleOnly(int pageIndex,
                                 rlClanId clanId,
                                 rlClanMember* resultGamers,
                                 unsigned int maxGamersCount,
                                 unsigned int* resultGamersCount,
                                 unsigned int* resultTotalCount,
                                 netStatus* status)
{
    rlClanRosGetMembersTitleOnlyTask* task = NULL;
    rtry
    {
        rverify(NULL != resultGamers, catchall, rlError("Invalid gameres result array"));
        rverify(NULL != resultGamersCount, catchall, rlError("Invalid gamers count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetMembersTitleOnly: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, clanId, (char*)NULL,
                                     resultGamers, maxGamersCount, resultGamersCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetMembersTitleOnly: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetMembersTitleOnly: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetMembersByClanId(int pageIndex,
                                 rlClanId clanId,
                                 rlClanMember* resultGamers,
                                 unsigned int maxGamersCount,
                                 unsigned int* resultGamersCount,
                                 unsigned int* resultTotalCount,
                                 netStatus* status)
{
    rlClanRosGetMembersTask* task = NULL;
    rtry
    {
        rverify(NULL != resultGamers, catchall, rlError("Invalid gameres result array"));
        rverify(NULL != resultGamersCount, catchall, rlError("Invalid gamers count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetMembersByClanId: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, clanId, (char*)NULL,
                                     resultGamers, maxGamersCount, resultGamersCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetMembersByClanId: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetMembersByClanId: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetMembersByClanName(int pageIndex,
                                   const char (&clanName)[RL_CLAN_NAME_MAX_CHARS],                          
                                   rlClanMember* resultGamers,                         
                                   unsigned int maxGamersCount,
                                   unsigned int* resultGamersCount,
                                   unsigned int* resultTotalCount,
                                   netStatus* status)
{
    rlClanRosGetMembersTask* task = NULL;
    rtry
    {
        rverify(NULL != resultGamers, catchall, rlError("Invalid gameres result array"));
        rverify(NULL != resultGamersCount, catchall, rlError("Invalid gamers count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetMembersByClanName: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, 0, clanName,
                                     resultGamers, maxGamersCount, resultGamersCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetMembersByClanName: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetMembersByClanName: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetRanks(int pageIndex,
                       rlClanId clanId,
                       rlClanRank* resultRanks,
                       unsigned int maxRanksCount,
                       unsigned int* resultRanksCount,
                       unsigned int* resultTotalCount,
                       netStatus* status)
{
    rlClanRosGetRanksTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));
        rverify(NULL != resultRanks, catchall, rlError("Invalid rank array"));
        rverify(NULL != resultRanksCount, catchall, rlError("Invalid rank count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetRanks: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, clanId, 
                                     resultRanks, maxRanksCount, resultRanksCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetRanks: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetRanks: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetWallMessages(int pageIndex,
                              rlClanId clanId,
                              rlClanWallMessage* resultWallMessages,
                              unsigned int maxWallMessageCount,
                              unsigned int* resultWallMessageCount,
                              unsigned int* resultTotalCount,
                              netStatus* status)
{
    rlClanRosGetWallMessagesTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));
        rverify(NULL != resultWallMessages, catchall, rlError("Invalid message array"));
        rverify(NULL != resultWallMessageCount, catchall, rlError("Invalid message count pointer"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetWallMessages: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, clanId, 
                                     resultWallMessages, maxWallMessageCount, resultWallMessageCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetWallMessages: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetWallMessages: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetMetadataForClan(int pageIndex,
                              rlClanId clanId,
                              rlClanMetadataEnum* resultEnums,
                              unsigned int maxEnumCount,
                              unsigned int* resultEnumCount,
                              unsigned int* resultTotalCount,
                              netStatus* status)
{
    rlClanRosGetMetadataForClanTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));
        rverify(NULL != resultEnums, catchall, rlError("Invalid enum array"));
        rverify(NULL != resultEnumCount, catchall, rlError("Invalid enum count pointer"));

       rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetMetadataForClan: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, clanId, 
                                     resultEnums, maxEnumCount, resultEnumCount, resultTotalCount, status), catchall, 
            rlError("rlClanClient[%d]::GetMetadataForClan: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetMetadataForClan: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
        return false;
    }
}

bool
rlClanClient::GetMembershipFor(int pageIndex,
                               const rlGamerHandle& gamer,
                               rlClanMembershipData* resultMembership,
                               unsigned int maxMembershipCount,
                               unsigned int* resultMembershipCount,
                               unsigned int* resultTotalCount,
                               netStatus* status)
{
    rlClanRosGetMembershipForTask* task = NULL;
    rtry
    {
        rcheck(gamer.IsValid(), catchall, rlError("Invalid gamer"));
        rverify(NULL != resultMembership, catchall, rlError("Invalid membership array"));
        rverify(NULL != resultMembershipCount, catchall, rlError("Invalid membership count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetMembershipFor: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, gamer,
                                     resultMembership, maxMembershipCount, resultMembershipCount, resultTotalCount, status), catchall,
            rlError("rlClanClient[%d]::GetMembershipFor: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetMembershipFor: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::JoinRequest(rlClanId clanId,
                          netStatus* status)
{
    rlClanRosJoinRequestTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan id"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::JoinRequest: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, 
                                     status), catchall,
            rlError("rlClanClient[%d]::JoinRequest: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::JoinRequest: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetSentJoinRequests(int pageIndex,
                                  rlClanJoinRequestSent* resultRequests,
                                  unsigned int maxRequestsCount,
                                  unsigned int* resultRequestsCount,
                                  unsigned int* resultTotalCount,
                                  netStatus* status)
{
    rlClanRosGetSentJoinRequestsTask* task = NULL;
    rtry
    {
        rverify(NULL != resultRequests, catchall, rlError("Invalid requests array"));
        rverify(NULL != resultRequestsCount, catchall, rlError("Invalid requests count pointer"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetSentJoinRequests: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex,
                                     resultRequests, maxRequestsCount, resultRequestsCount, resultTotalCount, status), catchall,
            rlError("rlClanClient[%d]::GetSentJoinRequests: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetSentJoinRequests: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetRecievedJoinRequests(int pageIndex,
                                      rlClanId clanId,
                                      rlClanJoinRequestRecieved* resultRequests,
                                      unsigned int maxRequestsCount,
                                      unsigned int* resultRequestsCount,
                                      unsigned int* resultTotalCount,
                                      netStatus* status)
{
    rlClanRosGetRecievedJoinRequestsTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan"));
        rverify(NULL != resultRequests, catchall, rlError("Invalid requests array"));
        rverify(NULL != resultRequestsCount, catchall, rlError("Invalid requests count pointer"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetRecievedJoinRequests: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, pageIndex, clanId,
                                     resultRequests, maxRequestsCount, resultRequestsCount, resultTotalCount, status), catchall,
            rlError("rlClanClient[%d]::GetRecievedJoinRequests: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetRecievedJoinRequests: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::DeleteJoinRequest(rlClanRequestId requestId,
                                netStatus* status)
{
    rlClanRosDeleteJoinRequestTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_REQUEST_ID != requestId, catchall, rlError("Invalid request id"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::DeleteJoinRequest: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, requestId,
                                     status), catchall,
            rlError("rlClanClient[%d]::DeleteJoinRequest: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::DeleteJoinRequest: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetFeudStats(const rlClanId clanId,
                  const rlClanFeudSinceCode since,
                  const unsigned int maxFeuderCount,
                  rlClanFeuder* resultFeuders,
                  unsigned int* resultFeuderCount,
                  netStatus* status)
{
    rlClanRosGetFeudStatsTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan"));
        rverify(RL_CLAN_FEUD_SINCE_LAST_DAY <= since && since < RL_CLAN_FEUD_SINCE_NUM_CODES, catchall, rlError("Invalid since"));
        rverify(NULL != resultFeuders, catchall, rlError("Invalid feuders array"));
        rverify(NULL != resultFeuderCount, catchall, rlError("Invalid feuders count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, since, maxFeuderCount, 
            resultFeuders, resultFeuderCount, status), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetFeudStats: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetTopRivals(const rlClanId clanId,
                           const rlClanFeudSinceCode since,
                           const unsigned int maxFeuderCount,
                           rlClanFeuder* resultFeuders,
                           unsigned int* resultFeuderCount,
                           netStatus* status)
{
    rlClanRosGetTopRivalsTask* task = NULL;
    rtry
    {
        rverify(RL_INVALID_CLAN_ID != clanId, catchall, rlError("Invalid clan"));
        rverify(RL_CLAN_FEUD_SINCE_LAST_DAY <= since && since < RL_CLAN_FEUD_SINCE_NUM_CODES, catchall, rlError("Invalid since"));
        rverify(NULL != resultFeuders, catchall, rlError("Invalid feuders array"));
        rverify(NULL != resultFeuderCount, catchall, rlError("Invalid feuders count pointer"));

        rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, since, maxFeuderCount, 
            resultFeuders, resultFeuderCount, status), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetFeudStats: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetTopFeuders(const rlClanFeudSinceCode since,
                            const unsigned int maxFeuderCount,
                            rlClanFeuder* resultFeuders,
                            unsigned int* resultFeuderCount,
                            netStatus* status)
{
    rlClanRosGetTopFeudersTask* task = NULL;
    rtry
    {
        rverify(RL_CLAN_FEUD_SINCE_LAST_DAY <= since && since < RL_CLAN_FEUD_SINCE_NUM_CODES, catchall, rlError("Invalid since"));
        rverify(NULL != resultFeuders, catchall, rlError("Invalid feuders array"));
        rverify(NULL != resultFeuderCount, catchall, rlError("Invalid feuders count pointer"));

       rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, since, maxFeuderCount, 
            resultFeuders, resultFeuderCount, status), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetFeudStats: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetTopFeudWinners(const rlClanFeudSinceCode since,
                                const unsigned int maxFeuderCount,
                                rlClanFeuder* resultFeuders,
                                unsigned int* resultFeuderCount,
                                netStatus* status)
{
    rlClanRosGetTopFeudWinnersTask* task = NULL;
    rtry
    {
        rverify(RL_CLAN_FEUD_SINCE_LAST_DAY <= since && since < RL_CLAN_FEUD_SINCE_NUM_CODES, catchall, rlError("Invalid since"));
        rverify(NULL != resultFeuders, catchall, rlError("Invalid feuders array"));
        rverify(NULL != resultFeuderCount, catchall, rlError("Invalid feuders count pointer"));

       rcheck(CheckCredentials(RL_CREDS_ROS), catchall, ); //SC membership not required.

        rcheck(this->CreateTask(&task), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, since, maxFeuderCount, 
            resultFeuders, resultFeuderCount, status), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetFeudStats: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool 
rlClanClient::GetTopFeudLosers(const rlClanFeudSinceCode since,
                               const unsigned int maxFeuderCount,
                               rlClanFeuder* resultFeuders,
                               unsigned int* resultFeuderCount,
                               netStatus* status)
{
    rlClanRosGetTopFeudLosersTask* task = NULL;
    rtry
    {
        rverify(RL_CLAN_FEUD_SINCE_LAST_DAY <= since && since < RL_CLAN_FEUD_SINCE_NUM_CODES, catchall, rlError("Invalid since"));
        rverify(NULL != resultFeuders, catchall, rlError("Invalid feuders array"));
        rverify(NULL != resultFeuderCount, catchall, rlError("Invalid feuders count pointer"));

        rcheck(CheckCredentials(), catchall, );

        rcheck(this->CreateTask(&task), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, since, maxFeuderCount, 
            resultFeuders, resultFeuderCount, status), catchall,
            rlError("rlClanClient[%d]::GetFeudStats: Failed to configure task", m_LocalGamerIndex));
        rcheck(this->QueueTask(task), catchall, 
            rlError("rlClanClient[%d]::GetFeudStats: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

bool
rlClanClient::GetEmblemFor(rlClanId clanId,
                           rlClanEmblemSize emblemSize, 
                           const u64 ifModifiedSincePosixTime,
                           const fiDevice* responseDevice, 
                           const fiHandle responseHandle, 
                           netStatus* status )
{
    rlClanRosGetClanEmblemTask* task = NULL;
    rtry
    {
        rverify(clanId != RL_INVALID_CLAN_ID, catchall, rlError("Invalid clan"));

        const rlRosCredentials& creds = rlRos::GetCredentials(m_LocalGamerIndex);
        rcheck(creds.IsValid(), catchall, 
            rlError("Gamer at index:%d doesn't have valid ROS credentials", m_LocalGamerIndex));

        rcheck(this->CreateTask(&task), catchall,
                rlError("rlClanClient[%d]::GetEmblemFor: Failed to allocate task", m_LocalGamerIndex));
        
        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, clanId, emblemSize, ifModifiedSincePosixTime, responseDevice, responseHandle, status), catchall,
            rlError("rlClanClient[%d]::GetEmblemFor: Failed to configure task", m_LocalGamerIndex));
        
        rcheck(this->AddParallelTask(task), catchall, 
            rlError("rlClanClient[%d]::GetEmblemFor: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        this->DestroyTask(task);
    }

    status->ForceFailed();
    return false;
}

//private:

void
rlClanClient::DestroyTask(rlTaskBase* task)
{
    if(task)
    {
        rlGetTaskManager()->DestroyTask(task);
    }
}

bool
rlClanClient::QueueTask(rlTaskBase* task)
{
    if(rlVerify(m_Initialized))
    {
        return rlGetTaskManager()->AppendSerialTask((size_t)this, task);
    }

    return false;
}

bool
rlClanClient::AddParallelTask(rlTaskBase* task)
{
    if(rlVerify(m_Initialized))
    {
        return rlGetTaskManager()->AddParallelTask(task);
    }

    return false;
}

void
rlClanClient::CancelTask(const netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

void
rlClanClient::CancelAllTasks()
{
    rlGetTaskManager()->CancelAll((size_t)this);
}

bool
rlClanClient::CheckCredentials( rlCheckCredentialsFlags requiredCredsFlag /*= RL_CREDS_SOCIALCLUB_MEMBER*/ ) const
{
    rtry
    {
        rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex), catchall, rlError("Invalid gamer index."));

        rlRosCredentials cred = rlRos::GetCredentials(m_LocalGamerIndex);


        rcheck(cred.IsValid(), catchall, rlError("Credentials are invalid"));

		if ((requiredCredsFlag & RL_CREDS_SOCIALCLUB_MEMBER) == RL_CREDS_SOCIALCLUB_MEMBER)
		{
			rcheck(cred.GetRockstarId() != InvalidRockstarId, catchall, rlError("Invalid rockstar ID (Account not linked)"));
		}
    
        return true;
    }
    rcatchall
    {

    }
    return false;
}

bool 
rlClanClient::IsServiceReady() const
{
    return (STATE_RUNNING_AS_SC_MEMBER == m_State
            || STATE_RUNNING == m_State);
}

bool 
rlClanClient::IsFullServiceAvailable() const
{
    return (STATE_RUNNING_AS_SC_MEMBER == m_State);
}

void
rlClanClient::CopyMembershipsTo(rlClanMembershipData* memberships,
                                const unsigned maxMemberships,
                                unsigned* numMemberships,
                                unsigned* totalMemberships) const
{
    const int count =
        (m_NumMemberships < maxMemberships)
            ? m_NumMemberships
            : maxMemberships;

    *numMemberships = count;
    if(totalMemberships)
    {
        *totalMemberships = m_NumMemberships;
    }

    for(int i = 0; i < count; ++i)
    {
        memberships[i] = m_MemberShips[i];
    }
}

};
