// 
// rline/rlclancommon.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLCLANCOMMON_H
#define RLINE_RLCLANCOMMON_H

#include "rline/rl.h"
#include "rline/ros/rlroscommon.h"

namespace rage
{

enum
{
    RL_INVALID_CLAN_ID = 0,
    RL_INVALID_RANK_ID = 0,
    RL_INVALID_INVITE_ID = 0,
    RL_INVALID_MEMBER_ID = 0,
    RL_INVALID_REQUEST_ID = 0,
    RL_INVALID_MESSAGE_ID = 0,
    RL_INVALID_ENUM_ID = 0,
    RL_CLAN_MAX_INVITES = 20,
    // ClanName = 24 characters + 1 null terminator.
    RL_CLAN_NAME_MAX_CHARS = 25,
    // ClanTag = 4 characters + 1 null terminator.
    RL_CLAN_TAG_MAX_CHARS = 5,
    // ClanMott = 64 characters + 1 null terminator.
    RL_CLAN_MOTTO_MAX_CHARS = 65,
    // ClanRank = 24 characters + 1 null terminator.
    RL_CLAN_RANK_NAME_MAX_CHARS = 25,
    // ClanInviteMessage = 140 characters + 1 null terminator.
    RL_CLAN_INVITE_MESSAGE_MAX_CHARS = 141,
    // ClanWallMessage = 1024 characters + 1 null terminator.
    RL_CLAN_WALL_MSG_MAX_CHARS = 1025,
    RL_CLAN_MAX_GAMER_HANDLE_COUNT = 100,
    // ClanMetadataEnumName,ClanMetadataSetName = 50 characters + 1 null terminator.
    RL_CLAN_ENUM_MAX_CHARS = 51,

	//Default color is SC Yellow (249,157,39), smashed in to match Color32 ARGB format
	RL_CLAN_DEFAULT_CLAN_COLOR = 0x00F99D27 
};

enum rlClanErrorCodes
{
    RL_CLAN_SUCCESS = 0,
    RL_CLAN_ERR_NAME_EXISTS,
    RL_CLAN_ERR_TAG_EXISTS,
    RL_CLAN_ERR_ALREADY_IN_CLAN,
    RL_CLAN_ERR_PLAYER_NOT_FOUND,
    RL_CLAN_ERR_PERMISSION_ERROR,
    RL_CLAN_ERR_INVITE_EXISTS,
    RL_CLAN_ERR_INVITE_MAX_SENT_COUNT_EXCEEDED,
    RL_CLAN_ERR_INVITE_DOES_NOT_EXIST,
    RL_CLAN_ERR_INVITE_PRIVATE,
    RL_CLAN_ERR_NOT_FOUND,
    RL_CLAN_ERR_RANK_ORDER_EXISTS,
    RL_CLAN_ERR_RANK_NAME_EXISTS,
    RL_CLAN_ERR_RANK_NOT_FOUND,
    RL_CLAN_ERR_RANK_INVALID,
    RL_CLAN_ERR_RANK_NOT_EMPTY,
    RL_CLAN_ERR_RANK_CHANGE_NOT_ALLOWED,
    RL_CLAN_ERR_ROCKSTAR_ID_DOES_NOT_EXIST,
    RL_CLAN_ERR_SC_NICKNAME_DOES_NOT_EXIST,
    RL_CLAN_ERR_PLAYER_ACCOUNT_DOES_NOT_EXIST,
    RL_CLAN_ERR_PARSER_FAILED,
    RL_CLAN_ERR_CLAN_NAME_LENGTH_INVALID,
    RL_CLAN_ERR_CLAN_MAX_JOIN_COUNT_EXCEEDED,
    RL_CLAN_ERR_CLAN_MAX_MEMBER_COUNT_EXCEEDED,
    RL_CLAN_ERR_RATE_LIMIT_EXCEEDED,
    RL_CLAN_ERR_NO_PRIVILEGE,
	RL_CLAN_ERR_CLAN_PERMISSION_ERROR,
	RL_CLAN_ERR_INVALID_TICKET,
	RL_CLAN_ERR_DUPLICATE_LOGIN,
    RL_CLAN_ERR_SQL_EXCEPTION,
    RL_CLAN_ERR_PLAYER_BANNED,
    RL_CLAN_ERR_UNKNOWN
};

// ***DO NOT CHANGE this enum without verifying against SystemPermissionFlags in ClanSystem
enum rlClanPermissionFlags 
{
	// Set by the system. Provides no special access.
    RL_CLAN_PERMISSION_SYSTEM_SET = 0X01,
	// Allows the member to disband the entire crew.  Very destructive.
    RL_CLAN_PERMISSION_DISBAND = 0x02,
	// Allows the member to kick any user of a lower rank (higher number).
    RL_CLAN_PERMISSION_KICK = 0x04,
	// Allows the member to invite other members.  Also allows them to accept join requests.
    RL_CLAN_PERMISSION_INVITE = 0x08,
	// Allows the member to promote any user to any lower rank (higher number).
    RL_CLAN_PERMISSION_PROMOTE = 0x10,
	// Allows the member to demote any user from a lower rank (higher number).
    RL_CLAN_PERMISSION_DEMOTE = 0x20,
	// Allows the member to define the rank permissions.
    RL_CLAN_PERMISSION_RANKMANAGER = 0x40,
	// Allows the member to add records to the wall system.  The member will always be able to remove these records.
    RL_CLAN_PERMISSION_WRITEONWALL = 0x100,
	// Allows the member to delete any Emblem or record  from the wall system.
    RL_CLAN_PERMISSION_DELETEFROMWALL = 0x200,
	// Allows the member access to write within the 'cloud'.
    RL_CLAN_PERMISSION_WRITEINCLOUDE = 0x400,
	// Allows the member update the Crew desc record. Includes AllowOpen and ChangeMotto
    RL_CLAN_PERMISSION_CREWEDIT = 0x800
    // RESERVED_RL_CLAN_PERMISSION_NOFLAGS = 0x8000000000000000
};

enum rlClanOptionalBool
{
    RL_CLAN_OMITTED = -1,
    RL_CLAN_FALSE = 0,
    RL_CLAN_TRUE = 1,
};

enum rlClanSortMode
{
    RL_CLAN_SORT_NONE = 0,
    RL_CLAN_SORT_BY_COUNT = 1,
};

enum rlClanEmblemSize
{
    RL_CLAN_EMBLEM_SIZE_32,
    RL_CLAN_EMBLEM_SIZE_64,
    RL_CLAN_EMBLEM_SIZE_128,
    RL_CLAN_EMBLEM_SIZE_256,
    RL_CLAN_EMBLEM_SIZE_1024,
    
    RL_CLAN_EMBLEM_NUM_SIZES
};

//  ***DO NOT CHANGE the order of this enum.
//  See rlClanRosGetFeudTaskBase::GetSinceString for an array of
//  strings that depends on it.
enum rlClanFeudSinceCode
{
    RL_CLAN_FEUD_SINCE_LAST_DAY = 0,
    RL_CLAN_FEUD_SINCE_LAST_WEEK,
    RL_CLAN_FEUD_SINCE_LAST_2_WEEKS,
    RL_CLAN_FEUD_SINCE_LAST_MONTH,
    RL_CLAN_FEUD_SINCE_LAST_3_MONTHS,
    RL_CLAN_FEUD_SINCE_LAST_6_MONTHS,
    RL_CLAN_FEUD_SINCE_ALL_TIME,

    RL_CLAN_FEUD_SINCE_NUM_CODES
};
//Maximum number of clans in which a player can be a member.
#ifndef RL_CLAN_MAX_MEMBERSHIPS
#define RL_CLAN_MAX_MEMBERSHIPS     8
#endif


typedef s64 rlClanInviteId;
typedef s64 rlClanId;
typedef s64 rlClanRankId;
typedef s64 rlClanMemberId;
typedef s64 rlClanRequestId;
typedef s64 rlClanWallMessageId;
typedef s32 rlClanEnumId;

RAGE_DECLARE_SUBCHANNEL(rline, clan)

class rlClanDesc
{
public:
    rlClanId m_Id;
	u32 m_clanColor; // 0xaarrggbb to be compatible with rage::Color32
	int m_MemberCount;
	int m_CreatedTimePosix;
	bool m_IsSystemClan;
	bool m_IsOpenClan;

    char m_ClanName[RL_CLAN_NAME_MAX_CHARS];
    char m_ClanTag[RL_CLAN_TAG_MAX_CHARS];
    char m_ClanMotto[RL_CLAN_MOTTO_MAX_CHARS];
    
public:
    rlClanDesc();

    //Invalidates the gamer the clan description.
    void Clear();

    //Returns TRUE if the clan description is valid.
    bool IsValid() const { return m_Id != RL_INVALID_CLAN_ID; }

    // Determines if this clan is a rockstar clan
    bool IsRockstarClan() const;
};

class rlClanInvite
{
public:
    rlClanInviteId m_Id;
    rlRosPlayerInfo m_Invitee;
    rlRosPlayerInfo m_Inviter;
    rlClanDesc m_Clan;
    char m_Message[RL_CLAN_INVITE_MESSAGE_MAX_CHARS];

public:
    rlClanInvite();

    //Invalidates the gamer handle and
    // the clan description.
    void Clear();

    //Returns TRUE if the invite is valid.
    bool IsValid() const { return (m_Id != RL_INVALID_INVITE_ID && m_Inviter.IsValid() && m_Invitee.IsValid() && m_Clan.IsValid()); }
};

class rlClanRank
{
public:
    rlClanId m_Id;
    char m_RankName[RL_CLAN_RANK_NAME_MAX_CHARS];
    int m_RankOrder;
    u64 m_SystemFlags;

public:
    rlClanRank();

    //Invalidates the gamer the clan description.
    void Clear();

    //Returns TRUE if the clan description is valid.
    bool IsValid() const { return m_Id != RL_INVALID_RANK_ID; }
};

class rlClanMembershipData
{
public:
    rlClanMemberId m_Id;
    rlClanDesc m_Clan;
    rlClanRank m_Rank;
    bool m_IsPrimary;

public:
    rlClanMembershipData();

    void Init(const rlClanMemberId memberId,
                const rlClanDesc& clanDesc,
                const rlClanRank& rank,
                const bool isPrimary);

    //Invalidates the gamer handle,
    // the clan description and the Role id.
    void Clear();

    //Returns TRUE if the Clan member is valid.
    bool IsValid() const { return( m_Id != RL_INVALID_MEMBER_ID && m_Clan.IsValid() && m_Rank.IsValid()); }
};

class rlClanMember
{
public:
    rlRosPlayerInfo m_MemberInfo;
    rlClanMembershipData m_MemberClanInfo;

public:
    rlClanMember();

    //Invalidates the gamer handle,
    // the clan description and the Role id.
    void Clear();

    //Returns TRUE if the Clan member is valid.
    bool IsValid() const { return( m_MemberClanInfo.IsValid() && m_MemberInfo.IsValid()); }
};

class rlClanLeader
{
public:
    rlRosPlayerInfo m_MemberInfo;
    rlClanId m_ClanId;

public:
    rlClanLeader();

    //Invalidates the gamer handle,
    // the clan description and the Role id.
    void Clear();

    //Returns TRUE if the Clan member is valid.
    bool IsValid() const { return(m_ClanId != RL_INVALID_CLAN_ID && m_MemberInfo.IsValid()); }
};

class rlClanJoinRequestRecieved
{
public:
    rlClanRequestId m_RequestId;
    rlRosPlayerInfo m_Player;
public:
    rlClanJoinRequestRecieved();

    //Invalidates the gamer handle and request id;
    // the clan description and the Role id.
    void Clear();

    bool IsValid() const { return( m_RequestId != RL_INVALID_REQUEST_ID && m_Player.IsValid()); }
};

class rlClanJoinRequestSent
{
public:
    rlClanRequestId m_RequestId;
    rlClanDesc m_Clan;
public:
    rlClanJoinRequestSent();

    void Clear();

    bool IsValid() const { return( m_RequestId != RL_INVALID_REQUEST_ID && m_Clan.IsValid()); }
};

class rlClanWallMessage
{
public:
    rlClanWallMessageId m_Id;
    rlRosPlayerInfo m_Writer;
    char m_Message[RL_CLAN_WALL_MSG_MAX_CHARS];

public:
    rlClanWallMessage();

    //Invalidates the writer and message.
    void Clear();

    //Returns TRUE if the Wall message is valid.
    bool IsValid() const { return( m_Id != RL_INVALID_MESSAGE_ID && m_Writer.IsValid()); }
};

class rlClanMetadataEnum
{
public:
    rlClanEnumId m_Id;
    char m_EnumName[RL_CLAN_ENUM_MAX_CHARS];
    char m_SetName[RL_CLAN_ENUM_MAX_CHARS];

public:
    rlClanMetadataEnum();

    //Invalidates the data.
    void Clear();

    //Returns TRUE if the Wall message is valid.
    bool IsValid() const { return( m_Id != RL_INVALID_ENUM_ID); }
};

class rlClanFeuder
{
public:
    rlClanId m_ClanId;
    char m_ClanName[RL_CLAN_NAME_MAX_CHARS];
    char m_ClanTag[RL_CLAN_TAG_MAX_CHARS];
    // The posix time of the all time first feud of this crew (or between the rivals).
    int m_FirstFeudPosix;
    // The posix time of the all time last feud of this crew (or between the rivals).
    int m_LastFeudPosix;
    int m_TotalFeuds;
    int m_Wins;
    int m_Loses;
    int m_Kills;
    int m_Deaths;
    unsigned long m_LastSessionId;
    unsigned long m_LastMatchId;

public:
    rlClanFeuder();

    //Invalidates the date.
    void Clear();

    //Returns TRUE if the Wall message is valid.
    bool IsValid() const { return( m_ClanId != RL_INVALID_CLAN_ID); }
};

//PURPOSE
//  Representation of an invalid clan description
extern const rlClanDesc RL_INVALID_CLAN_DESC;

//PURPOSE
//  Representation of an invalid clan membership data
extern const rlClanMembershipData RL_INVALID_CLAN_MEMBERSHIP_DATA;

//PURPOSE
//  Events generated by rlClan.
enum
{
    RLCLAN_EVENT_SYNCHED_MEMBERSHIPS,
    RLCLAN_EVENT_PRIMARY_CLAN_CHANGED,
    RLCLAN_EVENT_JOINED,
    RLCLAN_EVENT_LEFT,
    RLCLAN_EVENT_KICKED,
    RLCLAN_EVENT_INVITE_RECEIVED,
	RLCLAN_EVENT_FRIEND_JOINED,
	RLCLAN_EVENT_FRIEND_FOUNDED,
	RLCLAN_EVENT_METADATA_CHANGED,
	RLCLAN_EVENT_MEMBER_RANK_CHANGED,
	RLCLAN_EVENT_NOTIFY_JOIN_REQUEST,
	RLCLAN_EVENT_NOTIFY_DESC_CHANGED
};

#define RLCLAN_EVENT_COMMON_DECL( name )\
    static unsigned EVENT_ID() { return name::GetAutoId(); }\
    virtual unsigned GetId() const { return name::GetAutoId(); }\
    OUTPUT_ONLY(virtual const char* GetName() const { return #name; })

#define RLCLAN_EVENT_DECL( name, id )\
    AUTOID_DECL_ID( name, rage::rlClanEvent, id )\
    RLCLAN_EVENT_COMMON_DECL( name )

//PURPOSE
//  Base class for all Clan event classes.
class rlClanEvent
{
public:
    AUTOID_DECL_ROOT(rlClanEvent);
    RLCLAN_EVENT_COMMON_DECL(rlClanEvent);

    rlClanEvent() {}
    virtual ~rlClanEvent() {}
};

//PURPOSE
//  Dispatched when the player's membership data has been synched with the server.
class rlClanEventSynchedMemberships : public rlClanEvent
{
public:
    RLCLAN_EVENT_DECL(rlClanEventSynchedMemberships, RLCLAN_EVENT_SYNCHED_MEMBERSHIPS);

    explicit rlClanEventSynchedMemberships(const int localGamerIndex)
    {
		m_LocalGamerIndex = localGamerIndex;
    }

    int m_LocalGamerIndex;
};

//PURPOSE
//  Dispatched when the player's primary clan changes.
class rlClanEventPrimaryClanChanged : public rlClanEvent
{
public:
    RLCLAN_EVENT_DECL(rlClanEventPrimaryClanChanged, RLCLAN_EVENT_PRIMARY_CLAN_CHANGED);

    explicit rlClanEventPrimaryClanChanged(const int localGamerIndex)
    {
		m_LocalGamerIndex = localGamerIndex;
    }

    int m_LocalGamerIndex;
};

//PURPOSE
//  Dispatched when a clan is joined.
//  A rlClanEventPrimaryClanChanged could follow if the player
//  wasn't in a clan prior.
class rlClanEventJoined : public rlClanEvent
{
public:
    RLCLAN_EVENT_DECL(rlClanEventJoined, RLCLAN_EVENT_JOINED);

    rlClanEventJoined(const int localGamerIndex,
                      const rlClanDesc clanDesc)
    {
        m_LocalGamerIndex = localGamerIndex;
        m_ClanDesc = clanDesc;
    }

    int m_LocalGamerIndex;
    rlClanDesc m_ClanDesc;
};

//PURPOSE
//  Dispatched when a player is kicked from a clan.
//  A rlClanEventLeft event will follow.
//  A rlClanEventPrimaryClanChanged could follow if the player
//  was kicked from the primary clan.
class rlClanEventKicked : public rlClanEvent
{
public:
    RLCLAN_EVENT_DECL(rlClanEventKicked, RLCLAN_EVENT_KICKED);

     rlClanEventKicked(const int localGamerIndex,
                        const rlClanId clanId)
    {
        m_LocalGamerIndex = localGamerIndex;
        m_ClanId = clanId;
    }

    int m_LocalGamerIndex;
    rlClanId m_ClanId;
};

//PURPOSE
//  Dispatched when a player is leaves a clan.
//  A rlClanEventPrimaryClanChanged could follow if the player
//  was left the primary clan.
class rlClanEventLeft : public rlClanEvent
{
public:
    RLCLAN_EVENT_DECL(rlClanEventLeft, RLCLAN_EVENT_LEFT);

     rlClanEventLeft(const int localGamerIndex,
                    const rlClanId clanId)
    {
        m_LocalGamerIndex = localGamerIndex;
        m_ClanId = clanId;
    }

    int m_LocalGamerIndex;
    rlClanId m_ClanId;
};

//PURPOSE
//  Dispatched when a clan invite is received.
class rlClanEventInviteRecieved : public rlClanEvent
{
public:
    RLCLAN_EVENT_DECL(rlClanEventInviteRecieved, RLCLAN_EVENT_INVITE_RECEIVED);

    explicit rlClanEventInviteRecieved(const int localGamerIndex, const rlClanId clanId, const char* clanName, const char* clanTag, const char* rankName, const char* message)
    {
        m_LocalGamerIndex = localGamerIndex;
		m_ClanId = clanId;
		safecpy(m_ClanName, clanName);
		safecpy(m_ClanTag, clanTag);
		safecpy(m_RankName, rankName);
		safecpy(m_Message, message);
    }

    int m_LocalGamerIndex;
	rlClanId m_ClanId;
	char m_ClanName[RL_CLAN_NAME_MAX_CHARS];
	char m_ClanTag[RL_CLAN_TAG_MAX_CHARS];
	char m_RankName[RL_CLAN_RANK_NAME_MAX_CHARS];
	char m_Message[RL_CLAN_INVITE_MESSAGE_MAX_CHARS];
};

//PURPOSE
//  Dispatched when a friend joins a clan.
class rlClanEventFriendJoined : public rlClanEvent
{
public:
	RLCLAN_EVENT_DECL(rlClanEventFriendJoined, RLCLAN_EVENT_FRIEND_JOINED);

	rlClanEventFriendJoined(const int localGamerIndex, 
							const rlGamerHandle& gh,
							const rlClanId clanId )
	{
		m_LocalGamerIndex = localGamerIndex;
		m_ClanId = clanId;
		m_friendGamerHandle = gh;
	}

	int m_LocalGamerIndex;
	rlGamerHandle m_friendGamerHandle;
	rlClanId m_ClanId;
};

//PURPOSE
//  Dispatched when a friend founds a clan.
class rlClanEventFriendFounded : public rlClanEvent
{
public:
	RLCLAN_EVENT_DECL(rlClanEventFriendFounded, RLCLAN_EVENT_FRIEND_FOUNDED);

	rlClanEventFriendFounded(const int localGamerIndex, 
		const rlGamerHandle& gh,
		const rlClanId clanId )
	{
		m_LocalGamerIndex = localGamerIndex;
		m_ClanId = clanId;
		m_friendGamerHandle = gh;
	}

	int m_LocalGamerIndex;
	rlGamerHandle m_friendGamerHandle;
	rlClanId m_ClanId;
};

//PURPOSE
//  Dispatched when the clan metadata changes.
class rlClanEventMetadataChanged : public rlClanEvent
{
public:
	RLCLAN_EVENT_DECL(rlClanEventMetadataChanged, RLCLAN_EVENT_METADATA_CHANGED);

	rlClanEventMetadataChanged(const int localGamerIndex,
		                        const rlClanId clanId)
	{
		m_LocalGamerIndex = localGamerIndex;
		m_ClanId = clanId;
	}

	int m_LocalGamerIndex;
	rlClanId m_ClanId;
};

class rlClanEventMemberRankChange : public rlClanEvent
{
public:
	RLCLAN_EVENT_DECL(rlClanEventMemberRankChange, RLCLAN_EVENT_MEMBER_RANK_CHANGED);

	rlClanEventMemberRankChange(  const int localGamerIndex
									, const rlClanId clanId
									, const int rankOrder
									, const char* rankName
									, bool bPromotion)
	{
		m_LocalGamerIndex = localGamerIndex;
		m_ClanId = clanId;
		m_rankOrder = rankOrder;
		safecpy(m_rankName, rankName);
		m_bPromotion = bPromotion;
	}

	int m_LocalGamerIndex;
	rlClanId m_ClanId;
	int m_rankOrder;
	char m_rankName[RL_CLAN_RANK_NAME_MAX_CHARS];
	bool m_bPromotion;
};

class rlClanEventNotifyJoinRequest : public rlClanEvent
{
public:
	RLCLAN_EVENT_DECL(rlClanEventNotifyJoinRequest, RLCLAN_EVENT_NOTIFY_JOIN_REQUEST);

	rlClanEventNotifyJoinRequest(const int localGamerIndex)
	{
		m_LocalGamerIndex = localGamerIndex;
	}

	int m_LocalGamerIndex;
};

class rlClanEventNotifyDescChanged : public rlClanEvent
{
public:
	RLCLAN_EVENT_DECL(rlClanEventNotifyDescChanged, RLCLAN_EVENT_NOTIFY_DESC_CHANGED);

	rlClanEventNotifyDescChanged(const int localGamerIndex , const rlClanId clanId)
	{
		m_LocalGamerIndex = localGamerIndex;
		m_ClanId = clanId;
	}

	int m_LocalGamerIndex;
	rlClanId m_ClanId;
};

} //namespace rage

#endif  //RLINE_RLCLANCOMMON_H
