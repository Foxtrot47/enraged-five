// 
// rline/rlWebApiWorkItem.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include <sdk_version.h>
#include "rlnpwebapiworkitem.h"

#include "data/base64.h"
#include "diag/seh.h"
#include "file/asset.h"
#include "net/http.h"
#include "rline/rl.h"
#include "rline/rlnp.h"
#include "rline/rlnpcommon.h"
#include "rline/rlsession.h"
#include "rline/rldiag.h"
#include "rline/rlfriendsmanager.h"
#include "rline/rlpresence.h"
#include "rline/rltitleid.h"
#include "rline/rlgamerinfo.h"
#include "string/string.h"
#include "string/stringutil.h"
#include "system/timer.h"
#include "rline/rltitleid.h"
#include "diag/channel.h"

#include <invitation_dialog.h>

// The following are from the PS4 SDK documentation:
// https://ps4.scedev.net/resources/documents/WebAPI/1/PSN_WebAPI-Overview/0002.html
#define API_GROUP_ACTIVITYFEED			"sdk:activityFeed"
#define API_GROUP_COMMERCE				"sdk:commerce"
#define API_GROUP_ENTITLEMENT			"sdk:entitlement"
#define API_GROUP_EVENTS_CLIENT			"sdk:eventsClient"
#define API_GROUP_IDENTITY_MAPPER		"sdk:identityMapper"
#define API_GROUP_GAME_CUSTOM_DATA		"sdk:gameCustomData"
#define API_GROUP_SCORE_RANKING			"sdk:scoreRanking"
#define API_GROUP_SCORE_RANKING_MGMT	"sdk:scoreRankingManagement"
#define API_GROUP_SESSION_INVITATION	"sdk:sessionInvitation"
#define API_GROUP_TROPHY				"sdk:trophy"
#define API_GROUP_TSS					"sdk:tss"
#define API_GROUP_TUS					"sdk:tus"
#define API_GROUP_TUS_MGMT				"sdk:tusManagement"
#define API_GROUP_USER_PROFILE			"sdk:userProfile"
#define API_GROUP_WORD_FILTER			"sdk:wordFilter"

// APIs for presence callbacks
#define NP_GAME_PRESENCE_SERVICE_NAME		"inGamePresence"
#define NP_PUSH_EVENT_INVITATION_RECEIVED	"np:service:invitation"
#define NP_PUSH_EVENT_PRESENCE_STATUS		"np:service:presence:gameStatus"

// APIs that we need to support an onlineId version
#define API_GROUP_SESSION_INVITATION_ONLINE_ID	"sessionInvitation"
#define API_GROUP_USER_PROFILE_ONLINE_ID		"userProfile"

#define WEBAPI_VERSION					"v1"
#define WEBAPI_COMMERCE_VERSION			"v1"
#define WEBAPI_IDENTITY_MAPPER_VERSION	"v2"

#define INVITATION_MAX_LENGTH			2048
#define WEBAPI_READ_BUF_LEN				256
#define WEBAPI_NP_ID_BUF_LEN			128
#define WEBAPI_ONLINE_STATUS_BUF_LEN	128
#define WEBAPI_SESSION_BUF_LEN			512
#define WEBAPI_ACTFEED_BUF_LEN			4096
#define WEBAPI_ACTFEED_CAPTION_BUF_LEN	4096
#define WEBAPI_ACTFEED_ACTIONS_BUF_LEN	2048
#define WEBAPI_ACTFEED_CATEGORY_BUF_LEN	1024
#define WEBAPI_ACTFEED_SECTION_BUF_LEN	512

// account ID
// caption
// condensed caption
// ACTIVITY_API_IN_GAME_POST_CUSTOM_CAPTION
// ACTIVITY_API_IN_GAME_POST_ACTIONS
// subType
// ACTIVITY_API_IN_GAME_POST_IMAGES
// online ID
// titleID
#define ACTIVITY_API_IN_GAME_POST_BODY_EMPTY		\
	"{"\
	"\"source\": {\"accountId\": \"%" I64FMT "u\", \"type\": \"ONLINE_ID\" }, " \
	"%s, " \
	"%s, " \
	"%s " \
	"%s" \
	"\"storyType\": \"IN_GAME_POST\", " \
	"\"subType\": %d, " \
	"\"targets\": [ " \
		"%s" \
		"{\"accountId\": \"%" I64FMT "u\", \"type\": \"ONLINE_ID\" }," \
		"{\"meta\": \"%s\", \"type\": \"TITLE_ID\" }" \
	"]" \
	"}"

#define ACTIVITY_API_IN_GAME_POST_CUSTOM_CAPTION \
	"\"storyComment\": \"%s\", " 

// ACTIVITY_API_IN_GAME_POST_IMAGES_SMALL
// ACTIVITY_API_IN_GAME_POST_IMAGES_LARGE
// ACTIVITY_API_IN_GAME_POST_IMAGES_VIDEO
// ACTIVITY_API_IN_GAME_POST_IMAGES_THUMBNAIL	
#define ACTIVITY_API_IN_GAME_POST_IMAGES	"%s %s %s %s"

// image URL
#define ACTIVITY_API_IN_GAME_POST_IMAGES_SMALL		\
		"{\"meta\": \"%s\", \"type\": \"SMALL_IMAGE_URL\", \"aspectRatio\": \"%s\" },"

// image URL
#define ACTIVITY_API_IN_GAME_POST_IMAGES_LARGE		\
		"{\"meta\": \"%s\", \"type\": \"LARGE_IMAGE_URL\" },"

// image URL
#define ACTIVITY_API_IN_GAME_POST_IMAGES_THUMBNAIL		\
	"{\"meta\": \"%s\", \"type\": \"THUMBNAIL_IMAGE_URL\" },"

// image URL
#define ACTIVITY_API_IN_GAME_POST_IMAGES_VIDEO		\
		"{\"meta\": \"%s\", \"type\": \"VIDEO_URL\" },"

// ACTIVITY_API_IN_GAME_POST_ACTIONS_URL
// comma needed?
// ACTIVITY_API_IN_GAME_POST_ACTIONS_STARTGAME
// comma needed?
// ACTIVITY_API_IN_GAME_POST_ACTIONS_STORE
#define ACTIVITY_API_IN_GAME_POST_ACTIONS		\
	"\"actions\": " \
	"[ " \
	"%s" \
	"%s" \
	"%s" \
	"%s" \
	"%s" \
	"], "

// uri url
// captions
#define ACTIVITY_API_IN_GAME_POST_ACTIONS_URL		\
	"{ " \
	"\"type\": \"url\", " \
	" \"uri\": \"%s\", " \
	" \"platform\": \"PS4\", " \
	"%s" \
	"%s" \
	"}"

// command line
// captions
#define ACTIVITY_API_IN_GAME_POST_ACTIONS_STARTGAME		\
	"{ " \
	"\"type\": \"startgame\", " \
	" \"startArguments\": \"%s\", " \
	" \"platform\": \"PS4\", " \
	"%s" \
	"%s" \
	"}" 

// service label is 0 ... need to keep an eye on that staying the same. no nice code way
// product category label ...the code for it
// ACTIVITY_API_IN_GAME_POST_ACTION_IMAGE_URL
// ACTIVITY_API_IN_GAME_POST_BUTTON_CAPTIONS
#define ACTIVITY_API_IN_GAME_POST_ACTIONS_STORE 	\
	"{ " \
	"\"type\": \"store\", " \
	"\"serviceLabel\": \"0\", " \
	" \"productCategoryLabel\": \"%s\", " \
	" \"platform\": \"PS4\", " \
	"%s" \
	"%s" \
	"}"

// imageurl for thumbnail 68x68
#define ACTIVITY_API_IN_GAME_POST_ACTION_IMAGE_URL	\
	" \"imageUrl\": \"%s\", "

// buttons captions
// CAPTION TYPE and then 
// ...in sysLanguage order
#define ACTIVITY_API_IN_GAME_POST_BUTTON_CAPTIONS		\
	" \"%s\": {" \
	" \"en\": \"%s\"" \
	", \"en-GB\": \"%s\"" \
	", \"fr\": \"%s\"" \
	", \"de\": \"%s\"" \
	", \"it\": \"%s\"" \
	", \"es\": \"%s\"" \
	", \"pt-BR\": \"%s\"" \
	", \"pl\": \"%s\"" \
	", \"ru\": \"%s\"" \
	", \"ko\": \"%s\"" \
	", \"zh-TW\": \"%s\"" \
	", \"ja\": \"%s\"" \
	", \"es-MX\": \"%s\"" \
	", \"zh-SW\": \"%s\"" \
	" }"

// user account ID
// ACTIVITY_API_PLAYED_WITH_GAME_MODE
// ACTIVITY_API_PLAYED_WITH_PLAYER ...multiple
// title ID
#define ACTIVITY_API_PLAYED_WITH		\
	"{"\
	"\"source\": {\"accountId\": \"%" I64FMT "u\", \"type\": \"ONLINE_ID\" }, " \
	"\"storyType\": \"PLAYED_WITH\", " \
	"\"targets\": [ " \
	"%s" \
	"%s" \
	"{\"meta\": \"%s\", \"type\": \"TITLE_ID\" }" \
	"]" \
	"}"

// game mode description
#define ACTIVITY_API_PLAYED_WITH_GAME_MODE	\
	"{\"meta\": \"%s\", \"type\": \"PLAYED_DESCRIPTION\" },"

// player account ID
#define ACTIVITY_API_PLAYED_WITH_PLAYER	\
	"{\"accountId\": \"%" I64FMT "u\", \"type\": \"ONLINE_ID\" },"

// From the Sony Samples
// TODO JRM - clean this up a bit.
#define WEBAPI_CONTENT_TYPE_MULTIPART				"multipart/mixed; boundary=\"XghzCbW9WUZ2hfvG6r6gCfsttPue5MVa\""
#define MULTIPART_BOUNDARY							"--XghzCbW9WUZ2hfvG6r6gCfsttPue5MVa"
#define MULTIPART_BOUNDARY_END						"\r\n--XghzCbW9WUZ2hfvG6r6gCfsttPue5MVa--"
#define CONTENT_TYPE_JSON							"Content-Type:application/json; charset=utf-8"
#define CONTENT_TYPE_IMAGE							"Content-Type:image/jpeg"
#define CONTENT_TYPE_STREAM							"Content-Type:application/octet-stream"
#define CONTENT_DESCRIPT_SES						"Content-Description:session-request"
#define CONTENT_DESCRIPT_INV						"Content-Description:invitation-request"
#define CONTENT_DESCRIPT_IMG						"Content-Description:session-image"
#define CONTENT_DESCRIPT_INV_DATA					"Content-Description:invitation-data"
#define CONTENT_DESCRIPT_SESSION_DATA				"Content-Description:session-data"
#define CONTENT_DESCRIPT_CHANGEABLE_DATA			"Content-Description:changeable-session-data"
#define CONTENT_DISPOSITION_ATTACH					"Content-Disposition:attachment"

PARAM(webapioutput, "Output the full response from the PSN Web API");
PARAM(webapicapture, "Capture responses from PSN Web API to output folder e.g. -webapicapture=C:\\WebApiOutput\\");

namespace rage
{

extern sysMemAllocator* g_rlAllocator;

RAGE_DEFINE_SUBCHANNEL(rline, npwebapiworkitem)
#undef __rage_channel
#define __rage_channel rline_npwebapiworkitem

XPARAM(nethttpdump);

/****************************************
   rlNpWebApiRegisterCallbacksWorkItem
****************************************/
int rlNpWebApiRegisterCallbacksWorkItem::sm_GameTitleFilterIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApiRegisterCallbacksWorkItem::sm_GameTitleCallbackIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApiRegisterCallbacksWorkItem::sm_GameStatusFilterIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApiRegisterCallbacksWorkItem::sm_GameStatusCallbackIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApiRegisterCallbacksWorkItem::sm_GameDataFilterIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApiRegisterCallbacksWorkItem::sm_GameDataCallbackIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApiRegisterCallbacksWorkItem::sm_InvitationFilterIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApiRegisterCallbacksWorkItem::sm_InvitationCallbackIds[RL_MAX_LOCAL_GAMERS] = {0};

rlNpWebApiRegisterCallbacksWorkItem::rlNpWebApiRegisterCallbacksWorkItem()
{
	m_LibCtxId = 0;
	m_UserCtxId = 0;
	m_LocalGamerIndex = 0;
	m_bUnRegister = false;
	
	static bool bInitialized = false;
	if (!bInitialized)
	{
		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			sm_GameTitleFilterIds[i] = -1;
			sm_GameTitleCallbackIds[i] = -1;

			sm_GameStatusFilterIds[i] = -1;
			sm_GameStatusCallbackIds[i] = -1;

			sm_GameDataFilterIds[i] = -1;
			sm_GameDataCallbackIds[i] = -1;

			sm_InvitationFilterIds[i] = -1;
			sm_InvitationCallbackIds[i] = -1;
		}

		bInitialized = true;
	}
}

void rlNpWebApiRegisterCallbacksWorkItem::Configure(const int localGamerIndex, int libCtxId, int userCtxId, bool bUnregister)
{
	m_LocalGamerIndex = localGamerIndex;
	m_LibCtxId = libCtxId;
	m_UserCtxId = userCtxId;
	m_bUnRegister = bUnregister;
}

void rlNpWebApiRegisterCallbacksWorkItem::DoWork()
{
	m_Status.SetPending();
	rtry
	{
		if (m_bUnRegister)
		{
			UnregisterPresenceGameTitleInfoCB();
			//UnregisterPresenceGameStatusCB();
			UnregisterPresenceGameDataCB();
			UnregisterGameInviteCB();
		}
		else
		{
			rverify(RegisterPresenceGameTitleInfoCB(), catchall, );
			//rverify(RegisterPresenceGameStatusCB(), catchall, );
			rverify(RegisterPresenceGameDataCB(), catchall, );
			rverify(RegisterGameInviteCB(), catchall, );
		}

		m_Status.SetSucceeded();
	}
	rcatchall
	{
		rlDebug3("rlNpWebApiRegisterCallbacksWorkItem - failed to register callbacks");
		m_Status.SetFailed();
	}
};

bool rlNpWebApiRegisterCallbacksWorkItem::RegisterPresenceGameTitleInfoCB()
{
	rtry
	{
		// Filter already registered, early out
		if (sm_GameTitleFilterIds[m_LocalGamerIndex] >= 0)
			return true;

		int handleId = 0;

		// Create handle
		int ret = sceNpWebApiCreateHandle(m_LibCtxId);
		handleId = ret;

		// Data types for service Push events to receive
		SceNpWebApiPushEventDataType dataType;
		memset(&dataType, 0, sizeof(dataType));
		snprintf(dataType.val, SCE_NP_WEBAPI_PUSH_EVENT_DATA_TYPE_LEN_MAX,"np:service:presence:gameTitleInfo");

		// Create the service push event filter
		sceNpWebApiCreateServicePushEventFilter( m_LibCtxId, handleId, NP_GAME_PRESENCE_SERVICE_NAME, 0, &dataType, 1);

		rverify(ret >= 0, catchall, );
		sm_GameTitleFilterIds[m_LocalGamerIndex] = ret;

		// Register the server push event callback
		ret = sceNpWebApiRegisterServicePushEventCallback(m_UserCtxId, sm_GameTitleFilterIds[m_LocalGamerIndex], &rlNpWebApi::GameTitleInfoCB, (void*)&m_LocalGamerIndex);

		rverify(ret >= 0, catchall, );
		sm_GameTitleCallbackIds[m_LocalGamerIndex] = -1;

		rlDebug3("Registered presence game title info callback");
		return true;
	}
	rcatchall
	{
		if (sm_GameTitleFilterIds[m_LocalGamerIndex] >=0)
		{
			sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_GameTitleFilterIds[m_LocalGamerIndex]);
		}

		sm_GameTitleFilterIds[m_LocalGamerIndex] = -1;
		sm_GameTitleCallbackIds[m_LocalGamerIndex] = -1;
		rlError("Could not register presence game title info callback");
		return false;
	}
}

bool rlNpWebApiRegisterCallbacksWorkItem::RegisterPresenceGameStatusCB()
{
	rtry
	{
		// Filter already registered, early out
		if (sm_GameStatusFilterIds[m_LocalGamerIndex] != -1)
			return true;

		int handleId = 0;

		// Create handle
		int ret = sceNpWebApiCreateHandle(m_LibCtxId);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiCreateHandle() failed. ret = 0x%x\n", ret));
		handleId = ret;

		// Data types for service Push events to receive
		SceNpWebApiPushEventDataType dataType;
		memset(&dataType, 0, sizeof(dataType));
		snprintf(dataType.val, SCE_NP_WEBAPI_PUSH_EVENT_DATA_TYPE_LEN_MAX, NP_PUSH_EVENT_PRESENCE_STATUS);

		// Create the push event filter and verify its creation
		ret = sceNpWebApiCreateServicePushEventFilter( m_LibCtxId, handleId, NP_GAME_PRESENCE_SERVICE_NAME, 0, &dataType, 1);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiCreateServicePushEventFilter() failed. ret = 0x%x\n", ret));
		sm_GameStatusFilterIds[m_LocalGamerIndex] = ret;

		// Register push events and verify the results
		ret = sceNpWebApiRegisterServicePushEventCallback(m_UserCtxId, sm_GameStatusFilterIds[m_LocalGamerIndex], &rlNpWebApi::GameStatusCB, &m_LocalGamerIndex);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiRegisterServicePushEventCallback() failed. ret = 0x%x\n", ret));
		sm_GameStatusCallbackIds[m_LocalGamerIndex] = ret;

		rlDebug3("registered game status callbacks");
		return true;
	}
	rcatchall
	{
		if (sm_GameStatusFilterIds[m_LocalGamerIndex] >=0)
		{
			sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_GameStatusFilterIds[m_LocalGamerIndex]);
		}

		sm_GameStatusFilterIds[m_LocalGamerIndex] = -1;
		sm_GameStatusCallbackIds[m_LocalGamerIndex] = -1;
		rlError("Could not register presence game status callback");
		return false;
	}
}

bool rlNpWebApiRegisterCallbacksWorkItem::RegisterPresenceGameDataCB()
{
	rtry
	{
		// Filter already registered, early out
		if (sm_GameDataFilterIds[m_LocalGamerIndex] != -1)
			return true;

		int handleId = 0;

		// Create handle
		int ret = sceNpWebApiCreateHandle(m_LibCtxId);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiCreateHandle() failed. ret = 0x%x\n", ret));
		handleId = ret;

		// Data types for service Push events to receive
		SceNpWebApiPushEventDataType dataType;
		memset(&dataType, 0, sizeof(dataType));
		snprintf(dataType.val, SCE_NP_WEBAPI_PUSH_EVENT_DATA_TYPE_LEN_MAX,"np:service:presence:gameData");

		// Create the push event filter and verify results
		ret = sceNpWebApiCreateServicePushEventFilter( m_LibCtxId, handleId, NP_GAME_PRESENCE_SERVICE_NAME, 0, &dataType, 1);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiCreateServicePushEventFilter() failed. ret = 0x%x\n", ret));
		sm_GameDataFilterIds[m_LocalGamerIndex] = ret;

		// Register the push event callbacks and verify results
		ret = sceNpWebApiRegisterServicePushEventCallback(m_UserCtxId, sm_GameDataFilterIds[m_LocalGamerIndex], &rlNpWebApi::GameDataCB, &m_LocalGamerIndex);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiRegisterServicePushEventCallback() failed. ret = 0x%x\n", ret));
		sm_GameDataCallbackIds[m_LocalGamerIndex] = ret;

		rlDebug3("Registered presence game data callback");
		return true;
	}
	rcatchall
	{
		if (sm_GameDataFilterIds[m_LocalGamerIndex] >=0)
		{
			sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_GameDataFilterIds[m_LocalGamerIndex]);
		}

		sm_GameDataFilterIds[m_LocalGamerIndex] = -1;
		sm_GameDataCallbackIds[m_LocalGamerIndex] = -1;
		rlError("Could not register presence game title data callback");
		return false;
	}
}

bool rlNpWebApiRegisterCallbacksWorkItem::RegisterGameInviteCB()
{
	rtry
	{
		// Filter already registered, early out
		if (sm_InvitationFilterIds[m_LocalGamerIndex] != -1)
			return true;

		int handleId = 0;

		// Create handle
		int ret = sceNpWebApiCreateHandle(m_LibCtxId);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiCreateHandle() failed. ret = 0x%x\n", ret));
		handleId = ret;

		// Data types for service Push events to receive
		SceNpWebApiPushEventDataType dataType;
		memset(&dataType, 0, sizeof(dataType));
		snprintf(dataType.val, SCE_NP_WEBAPI_PUSH_EVENT_DATA_TYPE_LEN_MAX, NP_PUSH_EVENT_INVITATION_RECEIVED);

		// Create the service push event filter and verify the results
		ret = sceNpWebApiCreateServicePushEventFilter( m_LibCtxId, handleId, API_GROUP_SESSION_INVITATION_ONLINE_ID, 0, &dataType, 1);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiCreateServicePushEventFilter() failed. ret = 0x%x\n", ret));
		sm_InvitationFilterIds[m_LocalGamerIndex] = ret;

		// Register the push event callback and verify the results
		ret = sceNpWebApiRegisterServicePushEventCallback(m_UserCtxId, sm_InvitationFilterIds[m_LocalGamerIndex], &rlNpWebApi::InivitationReceivedCB, &m_LocalGamerIndex);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiRegisterServicePushEventCallback() failed. ret = 0x%x\n", ret));
		sm_InvitationCallbackIds[m_LocalGamerIndex] = ret;

		rlDebug3("Registered presence game data callback");
		return true;
	}
	rcatchall
	{
		if (sm_GameDataFilterIds[m_LocalGamerIndex] >=0)
		{
			sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_GameDataFilterIds[m_LocalGamerIndex]);
		}

		sm_GameDataFilterIds[m_LocalGamerIndex] = -1;
		sm_GameDataCallbackIds[m_LocalGamerIndex] = -1;
		rlError("Could not register presence game title data callback");
		return false;
	}
}

void rlNpWebApiRegisterCallbacksWorkItem::UnregisterPresenceGameTitleInfoCB()
{
	sceNpWebApiUnregisterServicePushEventCallback(m_LibCtxId, sm_GameTitleCallbackIds[m_LocalGamerIndex]);
	sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_GameTitleFilterIds[m_LocalGamerIndex]);
}

void rlNpWebApiRegisterCallbacksWorkItem::UnregisterPresenceGameStatusCB()
{
	sceNpWebApiUnregisterServicePushEventCallback(m_LibCtxId, sm_GameStatusFilterIds[m_LocalGamerIndex]);
	sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_GameStatusCallbackIds[m_LocalGamerIndex]);
}

void rlNpWebApiRegisterCallbacksWorkItem::UnregisterPresenceGameDataCB()
{
	sceNpWebApiUnregisterServicePushEventCallback(m_LibCtxId, sm_GameDataFilterIds[m_LocalGamerIndex]);
	sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_GameDataCallbackIds[m_LocalGamerIndex]);
}

void rlNpWebApiRegisterCallbacksWorkItem::UnregisterGameInviteCB()
{
	sceNpWebApiUnregisterServicePushEventCallback(m_LibCtxId, sm_InvitationFilterIds[m_LocalGamerIndex]);
	sceNpWebApiDeleteServicePushEventFilter(m_LibCtxId, sm_InvitationCallbackIds[m_LocalGamerIndex]);
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "[%s] " fmt, GetWorkItemName()

/****************************************
		rlNpWebApiWorkItem
****************************************/
rlNpWebApiWorkItem::rlNpWebApiWorkItem()
	: m_ReadSize(0)
	, m_LocalGamerIndex(0)
	, m_WebApiUserCtxId(0)
	, m_WebApiRequestId(0)
	, m_HttpMethod(SCE_NP_WEBAPI_HTTP_METHOD_GET)
	, m_bAddContent(false)
#if RSG_BANK
	, m_FaultToleranceRule(NULL)
#endif // RSG_BANK
{
	sysMemSet(m_ApiPath, 0, sizeof(m_ApiPath));
	sysMemSet(&m_Content, 0, sizeof(SceNpWebApiContentParameter));

	m_ReadBuffer.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE, 2048);
}

rlNpWebApiWorkItem::~rlNpWebApiWorkItem()
{
	if (m_WebApiRequestId != 0)
	{
		sceNpWebApiDeleteRequest(m_WebApiRequestId);
	}
}

bool rlNpWebApiWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId)
{
	rtry
	{
		int ret = 0;
		rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
		m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;

		m_HttpStatusCode = 0;
		m_WebApiRequestId = 0;
		m_WebApiUserCtxId = webApiUserCtxId;
		m_LocalGamerIndex = localGamerIndex;

		SetupApiPath(accountId);

        rlDebug("Configure :: [%" I64FMT "d] WebAPI: %s, %s, %s", m_WebApiRequestId, GetMethod(), GetApiGroup(), m_ApiPath);

		ret = sceNpWebApiCreateRequest(m_WebApiUserCtxId, GetApiGroup(), m_ApiPath, m_HttpMethod, m_bAddContent ? &m_Content : NULL, &m_WebApiRequestId);
		rverify(ret >= 0, catchall, rlError("sceNpWebApiCreateRequest() failed. ret = 0x%x\n", ret));
		m_Status.SetPending();

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlNpWebApiWorkItem::DoWork()
{
	rtry
	{
		int ret = 0;

		const char * body = GetRequestBody();

#if !__NO_OUTPUT
		if (body != NULL)
		{
			if (m_bAddContent)
			{
				rlDebug3("[%" I64FMT "d] WebAPI:\r\n%s %s\r\nContent-Type: %s\r\nContent-Length: %d\r\n\r\n%s\r\n", 
					m_WebApiRequestId, GetMethod(), m_ApiPath, m_Content.pContentType, (int)m_Content.contentLength, body);
			}
			else
			{
				rlDebug3("[%" I64FMT "d] WebAPI:\r\n%s %s\r\n\r\n%s\r\n", m_WebApiRequestId, GetMethod(), m_ApiPath, body);
			}
		}
		else
		{
			rlDebug3("[%" I64FMT "d] WebAPI: %s %s", m_WebApiRequestId, GetMethod(), m_ApiPath);
		}
#endif

#if RSG_BANK
		m_FaultToleranceRule = (rlNpWebApiFaultToleranceRule*)g_rlNp.GetFaultTolerance().GetRule(GetIdentifierName(), m_ApiPath);
		if (m_FaultToleranceRule)
		{
			m_HttpStatusCode = m_FaultToleranceRule->m_ResponseCode;
		}
		else
#endif
		{
			size_t length = GetRequestLength();
			ret = sceNpWebApiSendRequest(m_WebApiRequestId, (void*)body, length);
			rcheck(ret >= 0, catchall, rlError("[%" I64FMT "d] sceNpWebApiSendRequest failed with code: 0x%x, length:%" SIZETFMT "d, body:%s\n", m_WebApiRequestId, ret, length, body));

			ret = sceNpWebApiGetHttpStatusCode(m_WebApiRequestId, &m_HttpStatusCode);
			rverify(ret >= 0, catchall, rlError("[%" I64FMT "d] sceNpWebApiGetHttpStatusCode failed with code: 0x%x\n", m_WebApiRequestId, ret));
		}
	
		if (m_HttpStatusCode >= NET_HTTPSTATUS_OK && m_HttpStatusCode < NET_HTTPSTATUS_MULTIPLE_CHOICES)
		{
			rlDebug3("[%" I64FMT "d] Succeeded with code: %d", m_WebApiRequestId, m_HttpStatusCode);
			if (m_HttpStatusCode != NET_HTTPSTATUS_NO_CONTENT)
			{
				ReadResponseData();
			}
			OnComplete();
		}
		else if (m_HttpStatusCode >= NET_HTTPSTATUS_BAD_REQUEST)
		{
			rlDebug3("[%" I64FMT "d] Failed with code: %d", m_WebApiRequestId, m_HttpStatusCode);
			ReadResponseData();
			OnFailure();
		}
		else
		{
			OnFailure();
		}
	
		sceNpWebApiDeleteRequest(m_WebApiRequestId);
		m_WebApiRequestId = 0;
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}

		sceNpWebApiDeleteRequest(m_WebApiRequestId);	
		m_WebApiRequestId = 0;
	}
}

bool rlNpWebApiWorkItem::ReadResponseData()
{
#if __BANK
	// If a fault tolerance rule has been set for this work item, apply its delay and read the response
	if (m_FaultToleranceRule)
	{
		m_FaultToleranceRule->ApplyDelay();
		return m_FaultToleranceRule->GetResponse(&m_ReadBuffer, &m_ReadSize);
	}
#endif

	int32_t ret = 0;	
	size_t readSize = 0;

	char buf[WEBAPI_READ_BUF_LEN] = {0};
	do 
	{
		ret = sceNpWebApiReadData(m_WebApiRequestId, buf, sizeof(buf));
		if (ret < 0) 
		{
			rlError("[%" I64FMT "d] sceNpWebApiReadData failed with code: 0x%x\n", m_WebApiRequestId, ret);
			return false;
		}

		rlDebug3("[%" I64FMT "d] Incoming response (length: %d): %s", m_WebApiRequestId, ret, buf);
		
		readSize += ret;
		m_ReadBuffer.Append(buf, ret);
	} while (ret > 0);	

	m_ReadSize = readSize;
	rlDebug3("[%" I64FMT "d] Response length: %d", m_WebApiRequestId, (int)readSize);
	CaptureOutput();

	return true;
}

void rlNpWebApiWorkItem::CaptureOutput()
{
	rlDebug3("[%" I64FMT "d] Response:", m_WebApiRequestId);

#if !__NO_OUTPUT
	if (Channel_rline_npwebapiworkitem.TtyLevel >= DIAG_SEVERITY_DEBUG3 || PARAM_nethttpdump.Get())
	{
		diagLoggedPrintLn(GetBuffer(), m_ReadBuffer.Length());
	}
#endif

	const char* outputPath;
	if (PARAM_webapicapture.Get(outputPath))
	{
		char path[RAGE_MAX_PATH] = {0};
		safecpy(path, outputPath);
		if (!StringEndsWith(path, "\\"))
		{
			safecat(path, "\\");
		}
		safecatf(path, "%u_%s.txt", sysTimer::GetSystemMsTime(), GetIdentifierName());

		const fiDevice *pDevice = fiDevice::GetDevice(path, true);
		fiHandle hFile = fiHandleInvalid;
		if (pDevice)
		{
			hFile = pDevice->Open(path, true);
			if (!fiIsValidHandle(hFile))
			{
				hFile = pDevice->Create(path);
			}

			if (fiIsValidHandle(hFile))
			{
				pDevice->Write(hFile, GetBuffer(), m_ReadSize);
				pDevice->Close(hFile);
			}
		}
	}
}

const char * rlNpWebApiWorkItem::GetIdentifierName()
{
	switch(m_Identifier)
	{
	case DELETE_GAME_DATA:
		return "DELETE_GAME_DATA";
		break;
	case DELETE_GAME_STATUS:
		return "DELETE_GAME_STATUS";
		break;
	case DELETE_MEMBER:
		return "DELETE_MEMBER";
		break;
	case DELETE_SESSION:
		return "DELETE_SESSION";
		break;
	case GET_BLOCKLIST:
		return "GET_BLOCKLIST";
		break;
	case GET_CATALOG:
		return "GET_CATALOG";
		break;
	case GET_FRIENDSLIST:
		return "GET_FRIENDSLIST";
		break;
	case GET_INVITATION_DATA:
		return "GET_INVITATION_DATA";
		break;
	case GET_INVITATIONLIST:
		return "GET_INVITATIONLIST";
		break;
	case GET_INVITATION:
		return "GET_INVITATION";
		break;
	case GET_PROFILE:
		return "GET_PROFILE";
		break;
	case GET_PROFILES:
		return "GET_PROFILES";
		break;
	case GET_PRESENCE:
		return "GET_PRESENCE";
		break;
	case GET_SESSION_DATA:
		return "GET_SESSION_DATA";
		break;
	case GET_CHANGEABLE_SESSION_DATA:
		return "GET_CHANGEABLE_SESSION_DATA";
		break;
	case GET_SESSONLIST:
		return "GET_SESSONLIST";
		break;
	case GET_SESSION:
		return "GET_SESSION";
		break;
	case POST_INVITATION:
		return "POST_INVITATION";
		break;
	case POST_MEMBER:
		return "POST_MEMBER";
		break;
	case POST_SESSION:
		return "POST_SESSION";
		break;
	case PUT_GAME_DATA:
		return "PUT_GAME_DATA";
		break;
	case PUT_GAME_DATABLOB:
		return "PUT_GAME_DATABLOB";
		break;
	case PUT_GAME_STATUS:
		return "PUT_GAME_STATUS";
		break;
	case PUT_INVITATION:
		return "PUT_INVITATION";
		break;
	case PUT_SESSION_IMAGE:
		return "PUT_SESSION_IMAGE";
		break;
	case PUT_SESSION:
		return "PUT_SESSION";
		break;
	case PUT_CHANGEABLE_SESSION_DATA:
		return "PUT_CHANGEABLE_SESSION_DATA";
		break;
	default:
		break;
	}

	return "";
}

#if !__NO_OUTPUT
const char* rlNpWebApiWorkItem::GetMethod()
{
	switch(m_HttpMethod)
	{
	case SceNpWebApiHttpMethod::SCE_NP_WEBAPI_HTTP_METHOD_DELETE:
		return "DELETE";
	case SceNpWebApiHttpMethod::SCE_NP_WEBAPI_HTTP_METHOD_GET:
		return "GET";
	case SceNpWebApiHttpMethod::SCE_NP_WEBAPI_HTTP_METHOD_POST:
		return "POST";
	case SceNpWebApiHttpMethod::SCE_NP_WEBAPI_HTTP_METHOD_PUT:
		return "PUT";
	}

	return "";
}
#endif // !__NO_OUTPUT

/****************************************
		rlNpGetProfileWorkItem
****************************************/
bool rlNpGetProfileWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId)
{
	m_Identifier = GET_PROFILE;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpGetProfileWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpGetProfileWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	formatf(m_ApiPath, "/v080/users/%" I64FMT "u/profile", accountId);
}

/****************************************
		rlNpGetProfilesWorkItem
****************************************/
bool rlNpGetProfilesWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlGamerHandle* gamerHandles, int startIndex,
										int numGamerHandles, rlDisplayName* displayNames, rlGamertag* gamerTags, int filters, unsigned flags)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
		rverify(gamerHandles, catchall, );
		rverify(numGamerHandles > 0, catchall, );
		rverify(numGamerHandles <= MAX_GETPROFILES_ONLINEIDS, catchall, ); // getprofiles supports x2 batches of MAX online ids
		rverify(startIndex >= 0, catchall, );
		rverify(startIndex < numGamerHandles, catchall, );
		rverify(displayNames || gamerTags, catchall, );

		m_Identifier = GET_PROFILES;

		m_NumGamerHandles = numGamerHandles;
		m_Filter = filters;
		m_GamerTags = gamerTags;
		m_DisplayNames = displayNames;

		m_Flags = flags;
		m_StartIndex = startIndex;

#if !RL_NP_CPPWEBAPI
		// Assume all gamer handles are setup the same way
		m_UseAccountIds = (m_NumGamerHandles > 0) && (gamerHandles[0].GetNpAccountId() != RL_INVALID_NP_ACCOUNT_ID);
#endif

        // Iterate through gamer handles, copy gamer handle and clear gamer tag / display name
        m_NumValidHandles = 0;
		for (int i = m_StartIndex; i < m_NumGamerHandles; i++)
		{
			if ((m_Flags & Flags::FLAG_FRIENDS_ONLY) != 0)
			{
				if (rlFriendsManager::IsFriendsWith(localGamerIndex, gamerHandles[i]))
				{
					m_GamerHandles[i] = gamerHandles[i];
					m_NumValidHandles++;
				}
				else
				{
					m_GamerHandles[i].Clear();
				}
			}
			else
			{
				m_GamerHandles[i] = gamerHandles[i];
				m_NumValidHandles++;
			}
			
			if (m_GamerTags)
			{
				sysMemSet(m_GamerTags[i], 0, sizeof(rlGamertag));
			}
			
			if (m_DisplayNames)
			{
				sysMemSet(m_DisplayNames[i], 0, sizeof(rlDisplayName));
			}
		}

		if (m_NumValidHandles > 0)
		{
            return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		else
		{
			rlDebug1("Not valid handles...this task will do nothing");
			m_Status.SetPending();
			return true;
		}
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpGetProfilesWorkItem::GetApiGroup()
{
    return m_UseAccountIds ? API_GROUP_USER_PROFILE : API_GROUP_USER_PROFILE_ONLINE_ID;
}

void rlNpGetProfilesWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
    if(!m_UseAccountIds)
    {
        formatf(m_ApiPath, "/v1/profiles?onlineId=");
    }
    else
    {
        formatf(m_ApiPath, "/v1/profiles?accountIds=");
    }

	for (int i = m_StartIndex; i < m_NumGamerHandles; i++)
	{
		if (!m_GamerHandles[i].IsValid())
		{
			continue;
		}

        if(!m_UseAccountIds)
        {
            safecat(m_ApiPath, m_GamerHandles[i].GetNpOnlineId().data);
        }
        else
        {
            safecatf(m_ApiPath, "%" I64FMT "u", m_GamerHandles[i].GetNpAccountId());
        }

		if (i != m_NumGamerHandles - 1)
		{
			safecat(m_ApiPath, ",");
		}
	}

	// add &field=
	safecat(m_ApiPath, "&fields=");

	bool bTrimComma = false;
	if (m_Filter & PF_DEFAULT)
	{
		safecat(m_ApiPath, "default");
	}
	else
	{
		if (m_Filter & PF_USER)
		{
            if(!m_UseAccountIds)
            {
                safecat(m_ApiPath, "onlineId,");
            }
            else
            {
                safecat(m_ApiPath, "user,");
            }
			bTrimComma = true;
		}
		if (m_Filter & PF_REGION)
		{
			safecat(m_ApiPath, "region,");
			bTrimComma = true;
		}
		if (m_Filter & PF_NPID)
		{
			safecat(m_ApiPath, "npId,");
			bTrimComma = true;
		}
		if (m_Filter & PF_AVATAR)
		{
			safecat(m_ApiPath, "avatarUrl,");
			bTrimComma = true;
		}
		if (m_Filter & PF_ABOUTME)
		{
			safecat(m_ApiPath, "aboutMe,");
			bTrimComma = true;
		}
		if (m_Filter & PF_LANGUAGES)
		{
			safecat(m_ApiPath, "languagesUsed,");
			bTrimComma = true;
		}
        // no need for personal details if we're not flagged to use them
		if (((m_Flags & Flags::FLAG_ALLOW_REAL_NAMES) != 0) && m_Filter & PF_PERSONAL)
		{
			safecat(m_ApiPath, "personalDetail,");
			bTrimComma = true;
		}

		// trim trailing comma
		if (bTrimComma)
		{
			int commaPos = strlen(m_ApiPath);
			m_ApiPath[commaPos-1] = 0;
		}
	}
}

void rlNpGetProfilesWorkItem::DoWork()
{
	if (m_NumValidHandles > 0)
	{
		rlNpWebApiWorkItem::DoWork();
	}
	else
	{
		 OnComplete();
	}
}

bool rlNpGetProfilesWorkItem::ProcessSuccess()
{
	rtry
	{
		// do not process if no valid handles
		if (m_NumValidHandles == 0)
		{
			return true;
		}

		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		RsonReader rrList;
		rverify(rr.GetMember("profiles", &rrList), catchall, );

		RsonReader rrSize;
		rverify(rr.GetMember("size", &rrSize), catchall, );

		u32 numResults = 0;
		rverify(rrSize.AsUns(numResults), catchall, );
		rcheck(numResults > 0, catchall, rlError("No results from GET profiles query"));

		//Get the gamer list
		RsonReader rrProfile;
		rverify(rrList.GetFirstMember(&rrProfile),  catchall, rlError("Failed to read profile from: \"%s\"", GetBuffer()));

		bool done = false;

		do
		{
			rlSceNpAccountId accountId = RL_INVALID_NP_ACCOUNT_ID;
			rlSceNpOnlineId onlineId;

			RsonReader rrUser;
			RsonReader* rrOnlineId = nullptr;
			if (m_UseAccountIds)
			{
				if (rrProfile.GetMember("user", &rrUser))
				{
					RsonReader rrAccountId;
					if (rrUser.GetMember("accountId", &rrAccountId))
					{
						rlVerify(rrAccountId.AsUns64(accountId));
					}

					// get online ID from the 'user' object in the new API
					rrOnlineId = &rrUser;
				}
			}
			else
			{
				// get online ID from the 'profile' object in the old API
				rrOnlineId = &rrProfile;
			}

			// Get the online Id
			if (rrOnlineId)
			{
				rlVerify(rrOnlineId->GetValue("onlineId", onlineId.data, COUNTOF(onlineId.data) + 1)); // data is 16 characters, but a has a trailing terminator in the struct
			}

			// Create a gamer handle based on what we parsed
			rlGamerHandle gh;
			gh.ResetNp(g_rlNp.GetEnvironment(), accountId, &onlineId);

			// find the matching gamer
			for (int i = m_StartIndex; i < m_NumGamerHandles; i++)
			{
				// Skip invalid gamer handles
				if (!m_GamerHandles[i].IsValid())
					continue;

				// We didn't match, skip this idx.
				if (gh != m_GamerHandles[i])
					continue;

				// Read personal detail
				if (((m_Flags & Flags::FLAG_ALLOW_REAL_NAMES) != 0) && rrProfile.HasMember("personalDetail"))
				{
					if (m_DisplayNames != nullptr)
					{
						RsonReader rrPersonalDetail;
						rrProfile.GetMember("personalDetail", &rrPersonalDetail);
						char firstName[49] = { 0 }, middleName[49] = { 0 }, lastName[49] = { 0 }; // 16*3 + 1
						wchar_t firstNameW[17] = { 0 }, middleNameW[17] = { 0 }, lastNameW[17] = { 0 }; // 16*3 + 1

						// Extract first and last name
						rrPersonalDetail.GetValue("firstName", firstName);
						Utf8ToWide((char16*)firstNameW, firstName, COUNTOF(firstNameW));

						rrPersonalDetail.GetValue("lastName", lastName);
						Utf8ToWide((char16*)lastNameW, lastName, COUNTOF(lastNameW));

						// middle name is option
						RsonReader rrMiddle;
						rrPersonalDetail.GetMember("middleName", &rrMiddle);
						if (rrMiddle.GetValue(middleName))
						{
							rrMiddle.GetValue(middleName);
							Utf8ToWide((char16*)middleNameW, middleName, COUNTOF(middleNameW));

							wchar_t displayNameFull[RL_MAX_DISPLAY_NAME_BUF_SIZE];
							formatf(displayNameFull, RL_MAX_DISPLAY_NAME_BUF_SIZE, L"%s %s %s", firstName, middleName, lastName);
							rlGamerInfo::DisplayNameToUtf8(m_DisplayNames[i], displayNameFull);
						}
						else
						{
							wchar_t displayNameFull[RL_MAX_DISPLAY_NAME_BUF_SIZE];
							formatf(displayNameFull, RL_MAX_DISPLAY_NAME_BUF_SIZE, L"%s %s", firstName, lastName);
							rlGamerInfo::DisplayNameToUtf8(m_DisplayNames[i], displayNameFull);
						}
					}
				}

				// Extract gamer tag
				if (m_GamerTags)
				{
					rverify(rrOnlineId->GetValue("onlineId", m_GamerTags[i], RL_MAX_NAME_BUF_SIZE), catchall, rlError("Profiles response invalid"));
				}

				// If the personalDetail yielded no display name - use the onlineId
				if (m_DisplayNames != NULL && StringNullOrEmpty(m_DisplayNames[i]))
				{
					rverify(rrOnlineId->GetValue("onlineId", m_DisplayNames[i], RL_MAX_NAME_BUF_SIZE), catchall, rlError("Profiles response invalid"));
				}
			}

		} while (done != rrProfile.GetNextSibling(&rrProfile));

#if !__NO_OUTPUT
		for (int i = m_StartIndex; i < m_NumGamerHandles; i++)
		{
			rlDebug3("rlNpGetProfilesWorkItem :: Processed user %s (display name: %s)", m_GamerTags ? m_GamerTags[i] : "", m_DisplayNames ? m_DisplayNames[i] : "");
		}
#endif
		return true;
	}
	rcatchall
	{
		return false;
	}
}

/****************************************
		rlNpGetFriendsListWorkItem
****************************************/
const char* rlNpGetFriendsListWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpGetFriendsListWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/friendList?friendStatus=friend&presenceType=platform&presenceDetail=true", WEBAPI_VERSION, accountId);

	// specify an offset for the result set to allow paging of friends
	if (m_FirstFriendIndex > 0)
	{
		safecatf(m_ApiPath, "&offset=%d", m_FirstFriendIndex);
	}

	// specify the limit of the result set
	if (m_MaxFriends > 0)
	{
		safecatf(m_ApiPath, "&limit=%d", m_MaxFriends);
	}

	if (m_FriendFlags & rlFriendsReader::FRIENDS_PRESORT_ID && m_FriendFlags & rlFriendsReader::FRIENDS_PRESORT_ONLINE)
	{
		safecatf(m_ApiPath, "&sort=onlineStatus%%2BonlineId");
	}
	else if (m_FriendFlags & rlFriendsReader::FRIENDS_PRESORT_ID)
	{
		safecatf(m_ApiPath, "&sort=onlineId");
	}
	else if (m_FriendFlags & rlFriendsReader::FRIENDS_PRESORT_ONLINE)
	{
		safecatf(m_ApiPath, "&sort=onlineStatus");
	}

	// filter by online or incontext friends only
	if (m_FriendFlags & rlFriendsReader::FRIENDS_ONLINE)
	{
		safecat(m_ApiPath, "&filter=online");
	}
	else if (m_FriendFlags & rlFriendsReader::FRIENDS_ONLINE_IN_TITLE)
	{
		safecat(m_ApiPath, "&filter=incontext");
	}
}

bool rlNpGetFriendsListWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, unsigned * totalFriendsPtr, int startIndex,
										   rlFriend* friends, unsigned* numFriends, int maxFriends, int friendFlags)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid local gamer index"));
		rverify(totalFriendsPtr, catchall, rlError("NULL total friends pointer"));
		rverify(numFriends, catchall, rlError("NULL num friends pointer"));
		rverify(friends, catchall, rlError("NULL friends list pointer"));
		rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, rlError("Invalid account ID"));

		m_Identifier = GET_FRIENDSLIST;
		m_FirstFriendIndex = startIndex;
		m_MaxFriends = maxFriends;
		m_FriendFlags = friendFlags;
		m_TotalFriendsPtr = totalFriendsPtr;
		m_FriendsPtr = friends;
		m_NumFriends = numFriends;

		rlAssertf(m_MaxFriends >= 0 && m_MaxFriends <= 500, "friendList api requires 1-500 friends limit");

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

rlNpGetFriendsListWorkItem::rlNpGetFriendsListWorkItem()
{
	m_FirstFriendIndex = 0;
	m_MaxFriends = rlFriendsPage::MAX_FRIEND_PAGE_SIZE;
	m_TotalFriendsPtr = NULL;
	m_NumFriends = NULL;
	m_FriendsPtr = NULL;
}

bool rlNpGetFriendsListWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		// Check total first to determine result size
		RsonReader rrTotal;
		int total = 0;
		rverify(rr.GetMember("totalResults", &rrTotal), catchall, );
		rverify(rrTotal.AsInt(total), catchall, );

		if (m_TotalFriendsPtr)
		{
			*m_TotalFriendsPtr = total;
		}

		// no users on friend list
		if (total == 0)
		{
			return true;
		}

        rverify(rr.GetFirstMember(&rr),  catchall, rlError("Failed to read friend list from message: \"%s\"", GetBuffer()));

		rverify(rr.CheckName("friendList"), catchall, );

        //Get the gamer list
		RsonReader rrFriend;
        rverify(rr.GetFirstMember(&rrFriend),  catchall, rlError("Failed to read friend list from message: \"%s\"", GetBuffer()));

		bool done = false;
		*m_NumFriends = 0;

		for(int i = 0; i < m_MaxFriends && !done; ++i, done = !rrFriend.GetNextSibling(&rrFriend))
		{
			rlSceNpOnlineId onlineId;
			bool isPS4 = false;

			// Read in the NpId field
			RsonReader rrNpId;
			rverify(rrFriend.GetMember("npId", &rrNpId), catchall, rlError("NpGetFriendsList response invalid - could not parse NpId"));

			char npid[WEBAPI_NP_ID_BUF_LEN];
			rverify(rrNpId.AsString(npid), catchall, rlError("Friends list response invalid"));

			// Convert response to NpId
			SceNpId npId;
			rverify(sceNpWebApiUtilityParseNpId(npid, &npId) == SCE_OK, catchall, rlError("NpGetFriendsList response invalid - could not parse NpId"));
			onlineId.FromString(npId.handle.data);

			RsonReader rrUser;
			rverify(rrFriend.GetMember("user", &rrUser), catchall, rlError("NpGetFriendsList response invalid - could not parse NpId"));

			// Read in the NpId field
			RsonReader rrAccountId;
			rverify(rrUser.GetMember("accountId", &rrAccountId), catchall, rlError("NpGetFriendsList response invalid - could not parse NpId"));

			rlSceNpAccountId accountId;
			rverify(rrAccountId.AsUns64(accountId), catchall, rlError("Friends list response invalid"));

			// Extract the presence and online status
			RsonReader rrPresence;
			rverify(rrFriend.GetMember("presence", &rrPresence), catchall, rlError("NpGetFriendsList response invalid - could not retrieve presence"));

			RsonReader rrOnlineStatus;
			rverify(rrPresence.GetMember("onlineStatus", &rrOnlineStatus), catchall, rlError("NpGetFriendsList response invalid - could not retrieve online status"));

			char onlineStatus[WEBAPI_ONLINE_STATUS_BUF_LEN] = {0};
			rverify(rrOnlineStatus.AsString(onlineStatus), catchall, rlError("NpGetFriendsList response invalid - could not retrieve online status"));
				
			// Map the presence string to the RlNp presence
			bool bIsOnline = false;
			bool bIsInSameTitle = false;

			bIsOnline = (strcmp(onlineStatus, "online") == 0) ? true : false;

			char gameDataBuf[RL_PRESENCE_MAX_BUF_SIZE] = {0};
			int gameDataLen = 0;
			if (rrPresence.HasMember("platformInfoList"))
			{
				RsonReader rrPlatformInfo;
				rverify(rrPresence.GetMember("platformInfoList", &rrPlatformInfo), catchall, );

				RsonReader rrCurrentContext;
				bool moreContexts = rrPlatformInfo.GetFirstMember(&rrCurrentContext);
				while (moreContexts)
				{
					// We've requested the 'presenceType=platform' which should contain the platform info.
					// Check that we have the platform member
					if (rrCurrentContext.HasMember("platform"))
					{
						RsonReader rrPlatform;
						rrCurrentContext.GetMember("platform", &rrPlatform);
						if (rrPlatform.GetValueLength() > 0)
						{
							char platform[16] = {0};
							rverify(rrPlatform.AsString(platform), catchall, );

							// Verify that we're on the PS4 platform
							if (stricmp(platform, "PS4") == 0)
							{
								// Set the 'isPS4' flag to true and continue reading title information
								isPS4 = true;
								if (rrCurrentContext.HasMember("gameTitleInfo"))
								{
									RsonReader rrGameTitleInfo;
									rrCurrentContext.GetMember("gameTitleInfo", &rrGameTitleInfo);

									// Extract the Titlte ID and verify that it matches our current title ID
									if (rrGameTitleInfo.HasMember("npTitleId"))
									{
										RsonReader rrTitleId;
										rrGameTitleInfo.GetMember("npTitleId", &rrTitleId);

										char titleId[16] = {0};
										rverify(rrTitleId.AsString(titleId), catchall, );

										// The titleId check alone is not enough because it fails between EU, US, JPN builds.
										// From the docs:
										// gameStatus ... User's status regarding gameplay with the current platform. Up to 191 bytes in UTF-8.
										// Only exists when gameStatus is set and the user is playing a game that uses the same NP Communication ID as the current user
										if (stricmp(titleId, g_rlNp.GetTitleId().GetSceNpTitleId()->id) == 0 || rrCurrentContext.HasMember("gameStatus"))
										{
											bIsInSameTitle = true;

											if (rrCurrentContext.HasMember("gameData"))
											{
												RsonReader gameData;
												rrCurrentContext.GetMember("gameData", &gameData);

												// Extract the game data
												if (gameData.GetValueLength() > 0)
												{
													// Read as binary data
													rverify(gameData.AsBinary(gameDataBuf, RL_PRESENCE_MAX_BUF_SIZE, &gameDataLen), catchall, );
#if !__NO_OUTPUT
													// Import the session info and output to the logs.
													rlSessionInfo sessionInfo;
													if (sessionInfo.ImportFromNpPresence(gameDataBuf, gameDataLen))
													{
														rlDebug3("NpGetFriendsList: Friend %" I64FMT "u contained sc session id: %s. Host Peer Address: %s", accountId, sessionInfo.GetWebApiSessionId().data, sessionInfo.GetHostPeerAddress().ToString());
													}
#endif
												}
												else
												{
													// Otherwise, read as string
													rverify(gameData.AsString(gameDataBuf), catchall, );
													gameDataLen = strlen(gameDataBuf);
												}
											}
										}
									}
								}
							}
						}
					}

					// Get the next context
					moreContexts = rrCurrentContext.GetNextSibling(&rrCurrentContext);
				}
			}

			rlDebug3("rlNpGetFriendsListWorkItem::ProcessSuccess: Read friend %" I64FMT "u, Online: %d, Same Title: %d", accountId, bIsOnline, bIsInSameTitle);

			// Add friends directly to the friends list using the given flags and NpId
			m_FriendsPtr[*m_NumFriends].Reset(accountId, onlineId, bIsOnline, bIsInSameTitle, gameDataBuf, gameDataLen);

			// Increment the friend count
			(*m_NumFriends)++;
		}
			
		rlDebug3("Friends list work item completed successfully");
		return true;
	}
	rcatchall
	{
		rlDebug3("Friends list work item failed");
		return false;
	}
}

/****************************************
		rlNpDeleteGameDataWorkItem
****************************************/
bool rlNpDeleteGameDataWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId)
{
	m_Identifier = DELETE_GAME_DATA;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpDeleteGameDataWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpDeleteGameDataWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_DELETE;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/presence/gameData", WEBAPI_VERSION, accountId);
}

/****************************************
		rlNpDeleteGameStatusWorkItem
****************************************/
bool rlNpDeleteGameStatusWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId)
{
	m_Identifier = DELETE_GAME_STATUS;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpDeleteGameStatusWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpDeleteGameStatusWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_DELETE;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/presence/gameStatus", WEBAPI_VERSION, accountId);
}

/****************************************
		rlNpGetPresenceWorkItem
****************************************/
bool rlNpGetPresenceWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpAccountId targetAccountId, const rlSceNpOnlineId& targetOnlineId, bool bIsLocalUserPresence)
{
	m_bIsLocalUserPresence = bIsLocalUserPresence;
	m_Identifier = GET_PRESENCE;
	m_TargetAccountId = targetAccountId;
	m_TargetOnlineId = targetOnlineId;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpGetPresenceWorkItem::GetApiGroup()
{
	if (m_TargetAccountId != RL_INVALID_NP_ACCOUNT_ID)
	{
		return API_GROUP_USER_PROFILE;
	}
	else
	{
		return API_GROUP_USER_PROFILE_ONLINE_ID;
	}
}

void rlNpGetPresenceWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	if(m_TargetAccountId != RL_INVALID_NP_ACCOUNT_ID)
	{
		formatf(m_ApiPath, "/%s/users/%" I64FMT "u/presence?type=platform&presenceDetail=true", WEBAPI_VERSION, m_TargetAccountId);
	}
	else
	{
		formatf(m_ApiPath, sizeof(m_ApiPath), "/%s/users/%s/presence?type=platform&presenceDetail=true", WEBAPI_VERSION, m_TargetOnlineId.data);
	}
}

bool rlNpGetPresenceWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);
		
		rlSceNpOnlineId onlineId;
		bool isPS4 = false;

		// Read in the NpId field
		RsonReader rrNpId;
		rverify(rr.GetMember("npId", &rrNpId), catchall, rlError("NpGetPresence response invalid - could not parse NpId"));

		char npid[WEBAPI_NP_ID_BUF_LEN] = { 0 };
		rverify(rrNpId.AsString(npid), catchall, rlError("Friends list response invalid"));

		// Convert response to NpId
		SceNpId npId;
		rverify(sceNpWebApiUtilityParseNpId(npid, &npId) == SCE_OK, catchall, rlError("NpGetPresence response invalid - could not parse NpId"));
		onlineId.FromString(npId.handle.data);

		// Extract the presence and online status
		RsonReader rrPresence;
		rverify(rr.GetMember("presence", &rrPresence), catchall, rlError("NpGetPresence response invalid - could not retrieve presence"));

		RsonReader rrOnlineStatus;
		rverify(rrPresence.GetMember("onlineStatus", &rrOnlineStatus), catchall, rlError("NpGetPresence response invalid - could not retrieve online status"));

		// Read in the NpId field
		RsonReader rrAccountId;
		rr.GetMember("accountId", &rrAccountId);

		rlSceNpAccountId accountId = RL_INVALID_NP_ACCOUNT_ID;
		if(rrAccountId.HasData())
		{
			rrAccountId.AsUns64(accountId);
		}
		else
		{
			accountId = m_TargetAccountId;
		}

		char onlineStatus[WEBAPI_ONLINE_STATUS_BUF_LEN] = { 0 };
		rverify(rrOnlineStatus.AsString(onlineStatus), catchall, rlError("NpGetPresence response invalid - could not retrieve online status"));

		// Map the presence string to the RlNp presence
		bool bIsOnline = false;
		bool bIsInSameTitle = false;
		if (strcmp(onlineStatus, "online") == 0)
		{
			bIsOnline = true;
		}

		char gameDataBuf[RL_PRESENCE_MAX_BUF_SIZE] = {0};
		int gameDataLen = 0;

		if (rrPresence.HasMember("platformInfoList"))
		{
			RsonReader rrPlatformInfo;
			rverify(rrPresence.GetMember("platformInfoList", &rrPlatformInfo), catchall, );

			RsonReader rrCurrentContext;
			bool moreContexts = rrPlatformInfo.GetFirstMember(&rrCurrentContext);
			while (moreContexts)
			{
				// We've requested the 'presenceType=platform' which should contain the platform info.
				// Check that we have the platform member
				if (rrCurrentContext.HasMember("platform"))
				{
					RsonReader rrPlatform;
					rrCurrentContext.GetMember("platform", &rrPlatform);
					if (rrPlatform.GetValueLength() > 0)
					{
						char platform[16] = {0};
						rverify(rrPlatform.AsString(platform), catchall, );

						// Verify that we're on the PS4 platform
						if (stricmp(platform, "PS4") == 0)
						{
							// Set the 'isPS4' flag to true and continue reading title information
							isPS4 = true;
							if (rrCurrentContext.HasMember("gameTitleInfo"))
							{
								RsonReader rrGameTitleInfo;
								rrCurrentContext.GetMember("gameTitleInfo", &rrGameTitleInfo);

								// Extract the Title ID and verify that it matches our current title ID
								if (rrGameTitleInfo.HasMember("npTitleId"))
								{
									RsonReader rrTitleId;
									rrGameTitleInfo.GetMember("npTitleId", &rrTitleId);

									char titleId[16] = {0};
									rverify(rrTitleId.AsString(titleId), catchall, );

									// The titleId check alone is not enough because it fails between EU, US, JPN builds.
									// From the docs:
									// gameStatus ... User's status regarding gameplay with the current platform. Up to 191 bytes in UTF-8.
									// Only exists when gameStatus is set and the user is playing a game that uses the same NP Communication ID as the current user
									if (stricmp(titleId, g_rlNp.GetTitleId().GetSceNpTitleId()->id) == 0 || rrCurrentContext.HasMember("gameStatus"))
									{
										bIsInSameTitle = true;

										if (rrCurrentContext.HasMember("gameData"))
										{
											RsonReader gameData;
											rrCurrentContext.GetMember("gameData", &gameData);

											// Extract the game data
											if (gameData.GetValueLength() > 0)
											{
												// Read as binary data
												rverify(gameData.AsBinary(gameDataBuf, RL_PRESENCE_MAX_BUF_SIZE, &gameDataLen), catchall, );
#if !__NO_OUTPUT
												// Import the session info blob and output to the logs.
												rlSceNpSessionId sessionId;
												if (sessionId.Import(gameDataBuf, gameDataLen))
												{
													rlDebug3("NpGetFriendsList: Friend %" I64FMT "u contained session info: %s", accountId, sessionId.data);
												}
#endif
											}
											else
											{
												// Otherwise, read as string
												rverify(gameData.AsString(gameDataBuf), catchall, );
												gameDataLen = strlen(gameDataBuf);
											}
										}
									}
								}
							}
						}
					}
				}

				// Get the next context
				moreContexts = rrCurrentContext.GetNextSibling(&rrCurrentContext);
			}
		}

		// Update friends presence
		if (!m_bIsLocalUserPresence)
		{
			// Has presence blob
			if (gameDataLen > 0)
			{
				rlFriendsManager::UpdateFriendPresence(m_LocalGamerIndex, accountId, onlineId, bIsOnline, bIsInSameTitle, gameDataBuf, gameDataLen, true);
			}
			else
			{
				rlFriendsManager::UpdateFriendPresence(m_LocalGamerIndex, accountId, onlineId, bIsOnline, bIsInSameTitle, NULL, 0, true);
			}
		}
			
		rlDebug3("Get Presence work item completed successfully");
		return true;
	}
	rcatchall
	{
		rlDebug3("Get Presence work item failed");
		return false;
	}
}

/****************************************
		rlNpPutGameDataWorkItem
****************************************/
bool rlNpPutGameDataBlobWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, void* blob, int blobLength)
{
	rtry
	{
		m_Identifier = PUT_GAME_DATABLOB;

		rcheck(blobLength <= RL_PRESENCE_MAX_BUF_SIZE, catchall, );

        rw.Init(rsonBuf, sizeof(rsonBuf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		if (blobLength == 0)
		{
			rcheck(rw.WriteString("gameData", ""), catchall, );
		}
		else
		{
			rcheck(rw.WriteBinary("gameData", blob, blobLength), catchall, );
		}
		rcheck(rw.End(), catchall, );

		m_bAddContent = true;
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;
		m_Content.contentLength = rw.Length();

		rlDebug3("Sending game data blob: %s", rw.ToString());

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);;
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpPutGameDataBlobWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpPutGameDataBlobWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/presence/gameData?notificationWithData=true", WEBAPI_VERSION, accountId);
}

/****************************************
		rlNpPutGameStatusWorkItem
****************************************/
bool rlNpPutGameStatusWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * gameStatus)
{
	rtry
	{
		m_Identifier = PUT_GAME_STATUS;
        rw.Init(rsonBuf, sizeof(rsonBuf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.WriteString("gameStatus", gameStatus), catchall, );
		rcheck(rw.End(), catchall, );

		m_bAddContent = true;
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;
		m_Content.contentLength = rw.Length();

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpPutGameStatusWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpPutGameStatusWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;

	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/presence/gameStatus?notificationWithData=true", WEBAPI_VERSION, accountId);
}


/****************************************
		rlNpPutGamePresenceWorkItem
****************************************/
bool rlNpPutGamePresenceWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * gameStatus, const void* gameData, int gameDataLength)
{
	rtry
	{
		m_Identifier = PUT_GAME_STATUS;
        rw.Init(rsonBuf, sizeof(rsonBuf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.WriteString("gameStatus", gameStatus), catchall, );
		if (gameDataLength == 0)
		{
			rcheck(rw.WriteString("gameData", ""), catchall, );
		}
		else
		{
			rcheck(rw.WriteBinary("gameData", gameData, gameDataLength), catchall, );
		}
		rcheck(rw.End(), catchall, );

		m_bAddContent = true;
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;
		m_Content.contentLength = rw.Length();

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpPutGamePresenceWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpPutGamePresenceWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/presence/inGamePresence?notificationWithData=true", WEBAPI_VERSION, accountId);
}

/****************************************
		rlNpGetBlocklistWorkItem
****************************************/
bool rlNpGetBlocklistWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId)
{
	m_Identifier = GET_BLOCKLIST;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpGetBlocklistWorkItem::GetApiGroup()
{
	return API_GROUP_USER_PROFILE;
}

void rlNpGetBlocklistWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/blockingUsers", WEBAPI_VERSION, accountId);
}

bool rlNpGetBlocklistWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		// Check total first to determine result size
		RsonReader rrTotal;
		int total = 0;
		rverify(rr.GetMember("totalResults", &rrTotal), catchall, );
		rverify(rrTotal.AsInt(total), catchall, );

		// no users on block list
		if (total == 0)
		{
			return true;
		}

        rverify(rr.GetFirstMember(&rr),  catchall, rlError("Failed to read block list from message: \"%s\"", GetBuffer()));
		rverify(rr.CheckName("blockingUsers"), catchall, );

        //Get the block list
		RsonReader rrBlockList;
        rverify(rr.GetFirstMember(&rrBlockList),  catchall, rlError("Failed to block friend list from message: \"%s\"", GetBuffer()));
		rverify(rr.CheckName("blockingUsers"), catchall, );

		// Clear blocklist before ingesting a new one.
		g_rlNp.GetBasic().ClearBlocklist();

		bool done = false;
		for(int i = 0; i < RL_MAX_BLOCK_LIST && !done; ++i, done = !rrBlockList.GetNextSibling(&rrBlockList))
		{
			RsonReader rrUser;
			rverify(rrBlockList.GetMember("user", &rrUser), catchall, rlError("rlNpGetBlocklistWorkItem response invalid - could not parse User"));

			// Read in the AccountId field
			RsonReader rrAccountId;
			rverify(rrUser.GetMember("accountId", &rrAccountId), catchall, rlError("rlNpGetBlocklistWorkItem response invalid - could not parse AccountId"));

			rlSceNpAccountId accountId = RL_INVALID_NP_ACCOUNT_ID;
			rverify(rrAccountId.AsUns64(accountId), catchall, rlError("Block list response invalid"));
			rverifyall(accountId != RL_INVALID_NP_ACCOUNT_ID);

			rlSceNpOnlineId onlineId;
			RsonReader rrOnlineId;
			rverify(rrUser.GetMember("onlineId", &rrOnlineId), catchall, rlError("rlNpGetBlocklistWorkItem response invalid - could not parse OnlineId"));
			rverifyall(rrOnlineId.AsString(onlineId.data, COUNTOF(onlineId.data) + 1)); // data is 16 characters, but a has a trailing terminator in the struct

			rlGamerHandle gh;
			gh.ResetNp(g_rlNp.GetEnvironment(), accountId, &onlineId);
			g_rlNp.GetBasic().AddPlayerToBlocklist(gh);
		}

		rlDebug3("Get block list work item completed successfully");
		return true;
	}
	rcatchall
	{
		rlDebug1("Get block list work item failed.");
		return false;
	}
}

/****************************************
		rlNpGetInvitationWorkItem
****************************************/
bool rlNpGetInvitationWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* invite)
{
	m_Identifier = GET_INVITATION;
	m_InvitationPtr = invite;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetInvitationWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);
		
		RsonReader rrReceivedDate;
		rverify(rr.GetMember("receivedDate", &rrReceivedDate), catchall, );
		rverify(rrReceivedDate.AsString(m_InvitationPtr->receivedDate, sizeof(m_InvitationPtr->receivedDate)), catchall, rlError("Could not read receivedDate from invitation response"));

		RsonReader rrExpired;
		rverify(rr.GetMember("expired", &rrExpired), catchall, rlError("Could not read expired status invitation response"));
		rverify(rrExpired.AsBool(m_InvitationPtr->expired), catchall, rlError("Could not read expired status from invitation response"));

		RsonReader rrUsed;
		rverify(rr.GetMember("usedFlag", &rrUsed), catchall, rlError("Could not read used status from invitation response"));
		rverify(rrUsed.AsBool(m_InvitationPtr->used), catchall, rlError("Could not read used status from invitation response"));

		if (rr.HasMember("message"))
		{
			rverify(rr.GetValue("message", m_InvitationPtr->message, sizeof(m_InvitationPtr->message)), catchall, rlError("Could not read message from invitation response"));
		}
		
		RsonReader rrInviter;
		rverify(rr.GetMember("fromUser", &rrInviter), catchall, rlError("Could not read inviter from invitation response"));

		char onlineIdBuf[RL_MAX_NAME_BUF_SIZE];
		rverify(rrInviter.GetValue("onlineId", onlineIdBuf, sizeof(onlineIdBuf)), catchall, rlError("Could not read inviter from invitation response"));
		m_InvitationPtr->onlineId.FromString(onlineIdBuf);

		RsonReader rrAccountId;
		rverify(rrInviter.GetMember("accountId", &rrAccountId), catchall, rlError("Could not read inviter from invitation response"));
		rverify(rrAccountId.AsUns64(m_InvitationPtr->accountId), catchall, rlError("Could not read inviter from invitation response"));

		if (!m_InvitationPtr->expired && rr.HasMember("session"))
		{
			RsonReader rrSession;
			rverify(rr.GetMember("session", &rrSession), catchall, rlError("Could not read Session from invitation response"));

			rverify(rrSession.GetValue("sessionId", m_InvitationPtr->session.sessionId.data, sizeof(m_InvitationPtr->session.sessionId)), catchall, 
				rlError("Could not read sessionId from invitation session"));

			rverify(rrSession.GetValue("sessionType", m_InvitationPtr->session.sessionType, sizeof(m_InvitationPtr->session.sessionType)), catchall, 
				rlError("Could not read sessionType from invitation session"));

			/* not included in response
			rverify(rrSession.GetValue("npTitleType", m_InvitationPtr->session.npTitleType, sizeof(m_InvitationPtr->session.npTitleType)), catchall, 
				rlError("Could not read npTitleType from invitation session"));
			*/

			rverify(rrSession.GetValue("sessionPrivacy", m_InvitationPtr->session.sessionPrivacy, sizeof(m_InvitationPtr->session.sessionPrivacy)), catchall, 
				rlError("Could not read sessionPrivacy from invitation session"));

			RsonReader rrMaxUser;
			rverify(rrSession.GetMember("sessionMaxUser", &rrMaxUser), catchall, rlError("Could not read sessionMaxUser from invitation session"));
			rverify(rrMaxUser.AsInt(m_InvitationPtr->session.sessionMaxUser), catchall, rlError("Could not read sessionMaxUser from invitation session"));

			rverify(rrSession.GetValue("sessionName", m_InvitationPtr->session.sessionName, sizeof(m_InvitationPtr->session.sessionName)), catchall, 
				rlError("Could not read sessionName from invitation session"));

			rverify(rrSession.GetValue("sessionStatus", m_InvitationPtr->session.sessionStatus, sizeof(m_InvitationPtr->session.sessionStatus)), catchall, 
				rlError("Could not read sessionStatus from invitation session"));

			RsonReader rrSessionCreator;
			rverify(rrSession.GetMember("sessionCreator", &rrSessionCreator), catchall, rlError("Could not read sessionCreator from invitation session"));
			rverify(rrSessionCreator.ReadUns64("accountId", m_InvitationPtr->session.sessionCreator), catchall, rlError("Could not parse sessionCreator from invitation session"));
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpGetInvitationWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpGetInvitationWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/me/invitations/%s?fields=@default,session", WEBAPI_VERSION, m_InvitationPtr->invitationId);
}

/****************************************
		rlNpGetInvitationDataWorkItem
****************************************/
bool rlNpGetInvitationDataWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* invitation)
{
	m_Identifier = GET_INVITATION_DATA;
	m_InvitationPtr = invitation;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetInvitationDataWorkItem::ProcessSuccess()
{
	u8 dst[WEBAPI_SESSION_BUF_LEN] = {0};
	unsigned int dstLen;
	datBase64::Decode(GetBuffer(), WEBAPI_SESSION_BUF_LEN, &dst[0], &dstLen);

	unsigned numInfoBytes = 0;                       
    if(!m_InvitationPtr->session.info.Import(dst, dstLen, &numInfoBytes))
	{
		rlError("rlNpGetInvitationDataWorkItem :: Failed to import session data");
		return false;
	}

	return true;
}

const char* rlNpGetInvitationDataWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpGetInvitationDataWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/me/invitations/%s/invitationData", WEBAPI_VERSION, m_InvitationPtr->invitationId);
}

#if RL_NP_LEGACY
/****************************************
		rlNpGetInvitationListWorkItem
****************************************/
bool rlNpGetInvitationListWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, int maxNumInvitations)
{
	m_Identifier = GET_INVITATIONLIST;
	m_MaxInvitations = maxNumInvitations;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);	
}

bool rlNpGetInvitationListWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);
		
		// Check total first to determine result size
		RsonReader rrTotal;
		int total = 0;
		rverify(rr.GetMember("totalResults", &rrTotal), catchall, );
		rverify(rrTotal.AsInt(total), catchall, );

		// no invitations
		if (total == 0)
		{
			return true;
		}

		if (total > m_MaxInvitations)
		{
			rlWarning("GetInvitationList returned %d invitations, clamping to %d", total, m_MaxInvitations);
			total = m_MaxInvitations;
		}

		m_NumInvitations = 0;

		RsonReader rrList;
		rverify(rr.GetMember("invitations", &rrList), catchall, rlError("rlNpGetInvitationListWorkItem response invalid - could not get invitations"));

		//Get the inviation list
		RsonReader rrInvitation;
        rverify(rrList.GetFirstMember(&rrInvitation),  catchall, rlError("Failed to read invitation list from message: \"%s\"", GetBuffer()));

		bool done = false;
		for(int i = 0; i < m_MaxInvitations && !done; ++i, done = !rrInvitation.GetNextSibling(&rrInvitation))
		{
			rverify(rrInvitation.GetValue("invitationId", m_Invitations[i].invitationId, sizeof(m_Invitations[i].invitationId)),
				catchall, rlError("Failed to read invitationId from message"));
		
			RsonReader rrUsedFlag;
			rverify(rrInvitation.GetMember("usedFlag", &rrUsedFlag), catchall, rlError("Could not read usedFlag from invitation info"));
			rverify(rrUsedFlag.AsBool(m_Invitations[i].used), catchall, rlError("Could not read usedFlag from invitation info"));

			m_NumInvitations++;
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlNpGetInvitationListWorkItem::GetInvitations(rlNpWebApiInvitationDetail* invitations, int * numInvitations)
{
	for (int i = 0; i < m_NumInvitations && i < m_MaxInvitations; i++)
	{
		invitations[i] = m_Invitations[i];
	}

	*numInvitations = m_NumInvitations;
}

const char* rlNpGetInvitationListWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpGetInvitationListWorkItem::SetupApiPath(rlSceNpAccountId /*accountId*/)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/me/invitations", WEBAPI_VERSION);
}
#else
void rlNpGetInvitationListWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<GetUsersAccountIdPlayerSessionsInvitationsResponseBody>> transaction;

	rtry
	{
		char buffer[24];
		formatf(buffer, "%" I64FMT "u", m_AccountId);

		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		PlayerSessionsApi::ParameterToGetPlayerSessionInvitations param;
		ret = param.initialize(m_LibCtx, buffer);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToGetPlayerSessionInvitations init failed. ret = 0x%x", ret));

		Common::Vector<Common::String> fields(m_LibCtx);
		ret = AddStringToVector(m_LibCtx, "invitationId", fields);
		rverify(ret == SCE_OK, catchall, );
		ret = AddStringToVector(m_LibCtx, "sessionId", fields);
		rverify(ret == SCE_OK, catchall, );
		ret = AddStringToVector(m_LibCtx, "receivedTimestamp", fields);
		rverify(ret == SCE_OK, catchall, );
		ret = AddStringToVector(m_LibCtx, "from", fields);
		rverify(ret == SCE_OK, catchall, );

		Common::IntrusivePtr<Common::Vector<Common::String>> fieldsPtr(&fields, m_LibCtx);
		param.setfields(fieldsPtr);

		ret = PlayerSessionsApi::getPlayerSessionInvitations(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("getPlayerSessionInvitations failed. ret = 0x%x", ret));

		Common::IntrusivePtr<GetUsersAccountIdPlayerSessionsInvitationsResponseBody> respPtr;
		ret = transaction.getResponse(respPtr);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		Common::IntrusivePtr<Common::Vector<Common::IntrusivePtr<UsersPlayerSessionsInvitationForRead>>> invitations = respPtr->getInvitations();

		m_NumInvitations = 0;

		if (invitations)
		{
			OUTPUT_ONLY(int processIndex = 0);

			for (const Common::IntrusivePtr<UsersPlayerSessionsInvitationForRead>& invitation : *invitations)
			{
				rlDebug("Invitation [%d] :: Id: %s, From: %" I64FMT "u [%s], SessionId: %s, Received: %s, Invalid: %s",
					processIndex,
					invitation->getInvitationId().c_str(),
					invitation->getFrom()->getAccountId(),
					invitation->getFrom()->getOnlineId().data,
					invitation->getSessionId().c_str(),
					invitation->getReceivedTimestamp().c_str(),
					invitation->getInvitationInvalid() ? "True" : "False");

				if (invitation->getInvitationInvalid())
				{
					rlDebug("\t[%d] Discarding Invalid Invite", m_NumInvitations);
					continue;
				}
				
				if (m_NumInvitations >= m_MaxInvitations)
				{
					rlError("\t[%d] Discarding Invite due to Maximum Limit (Max: %d)", processIndex, m_MaxInvitations);
					continue;
				}

				rlNpWebApiInvitationDetail& inv = m_Invitations[m_NumInvitations];

				if (invitation->fromIsSet())
				{
					inv.accountId = invitation->getFrom()->getAccountId();
					inv.onlineId.Reset(invitation->getFrom()->getOnlineId());
				}
				else
				{
					inv.accountId = RL_INVALID_NP_ACCOUNT_ID;
					inv.onlineId = rlSceNpOnlineId::RL_INVALID_ONLINE_ID;
				}

				safecpy(inv.invitationId, invitation->getInvitationId().c_str());
				safecpy(inv.receivedDate, invitation->getReceivedTimestamp().c_str());
				inv.receivedTimestamp = atol(inv.receivedDate);
				inv.expired = false; // this is used to invalidate invites after processing session data
				inv.used = false;
				inv.message[0] = 0;
				inv.session.sessionId.Reset(invitation->getSessionId().c_str());

				// increment invites (check array bounds at top so that we can log the incoming invites we miss)
				++m_NumInvitations;
				OUTPUT_ONLY(++processIndex);
			}

			rlDebug("Added %u invites (of %u total)", m_NumInvitations, processIndex);

			// while we're at it we get the session details of the invite
			if(m_NumInvitations > 0)
			{
				rlSceNpSessionId* sessionIds = (rlSceNpSessionId*)g_rlAllocator->RAGE_LOG_ALLOCATE(sizeof(rlSceNpSessionId) * m_NumInvitations, 0);
				rlNpWebApiSessionDetail* sessionDetails = (rlNpWebApiSessionDetail*)g_rlAllocator->RAGE_LOG_ALLOCATE(sizeof(rlNpWebApiSessionDetail) * m_NumInvitations, 0);

				rlDebug("Allocated %u for sessionIds, %u for sessionDetails",
					static_cast<unsigned>(sizeof(rlSceNpSessionId)) * m_NumInvitations,
					static_cast<unsigned>(sizeof(rlNpWebApiSessionDetail)) * m_NumInvitations);

				// copy all of the session Ids into our request array
				// duplicates are handled within the task
				for (unsigned i = 0; i < m_NumInvitations; ++i)
				{
					sessionIds[i] = m_Invitations[i].session.sessionId;
					memset(&sessionDetails[i], 0, sizeof(rlNpWebApiSessionDetail));
				}

				// it's ok for some of these to fail
				const bool failOnMissing = false;

				rlNpGetPlayerSessionWorkItem sessionsItem;
				rverify(sessionsItem.Configure(m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, sessionIds, sessionDetails, m_NumInvitations, failOnMissing), catchall, );

				// execute the work now
				sessionsItem.DoWork();
				rverify(sessionsItem.Succeeded(), catchall, );

				rlDebug("Processing session results...");

				for (unsigned i = 0; i < m_NumInvitations; ++i)
				{
					rlNpWebApiInvitationDetail& inv = m_Invitations[i];
					const rlNpWebApiSessionDetail& details = sessionDetails[i];

					if(!details.sessionId.IsValid())
					{
						rlDebug("\t[%d] Failed Look-up - SessionId: %s", i, inv.session.sessionId.data);
						inv.expired = true;
					}
					else if(!rlVerify(details.sessionId == inv.session.sessionId))
					{
						rlDebug("\t[%d] Invalid Assignment - Assigned: %s, SessionId: %s", i, details.sessionId.data, inv.session.sessionId.data);
						inv.expired = true;
					}
					else if(!details.info.IsValid())
					{
						rlDebug("\t[%d] Invalid Token - SessionId: %s", i, inv.session.sessionId.data);
						inv.expired = true;
					}
					else
					{
						rlDebug("\t[%d] Success - SessionId: %s, Token: 0x%016" I64FMT "x", i, inv.session.sessionId.data, details.info.GetToken().m_Value);
					}

					// copy the details (do this even if expired - let the requesting code handle that)
					inv.session = details;
				}

				// free temporary arrays
				if (rlVerify(g_rlAllocator->IsValidPointer(sessionIds)))
					g_rlAllocator->Free(sessionIds);
				if (rlVerify(g_rlAllocator->IsValidPointer(sessionDetails)))
					g_rlAllocator->Free(sessionDetails);
			}
		}

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}
#endif

#if RL_NP_LEGACY
/****************************************
		rlNpPutInvitationWorkItem
****************************************/
bool rlNpPutInvitationWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * invitationId, bool used)
{
	rtry
	{
		m_Identifier = PUT_INVITATION;
		safecpy(m_InvitationId, invitationId);
		rw.Init(rsonBuf, sizeof(rsonBuf), RSON_FORMAT_JSON);

		rverify(rw.Begin(NULL, NULL), catchall, rlError("Could not begin rson writer for PutInvitation"));
		rverify(rw.WriteBool("usedFlag", used), catchall, rlError("Could not add used flag for PutInvitation"));
		rverify(rw.End(), catchall, rlError("Could not end rson writer for PutInvitation"));

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
	
	return true;
}

const char* rlNpPutInvitationWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpPutInvitationWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;
	formatf(m_ApiPath, "/%s/users/me/invitations/%s", WEBAPI_VERSION, m_InvitationId);
}

/****************************************
		rlNpPostInvitationWorkItem
****************************************/
rlNpPostInvitationWorkItem::rlNpPostInvitationWorkItem()
{
	m_Body = NULL;
}

rlNpPostInvitationWorkItem::~rlNpPostInvitationWorkItem()
{
	if (m_Body != NULL)
	{
		delete[] m_Body;
		m_Body = NULL;
	}
}

bool rlNpPostInvitationWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiPostInvitationParam* param)
{
	rtry
	{
		m_Identifier = POST_INVITATION;
		m_SessionId = *param->m_SessionId;

		memset(&m_Content, 0, sizeof(m_Content));
		m_Content.pContentType = WEBAPI_CONTENT_TYPE_MULTIPART;

		char rsonBuf[INVITATION_MAX_LENGTH];
		RsonWriter rw;
		rw.Init(rsonBuf, INVITATION_MAX_LENGTH, RSON_FORMAT_JSON);

		rverify(rw.Begin(NULL, NULL), catchall, rlError("Could not initialize rsonWriter"));
		rw.BeginArray("to", NULL);

        if(param->m_NumAccountIds > 0)
        {
            rlDebug("Posting %u account Ids", param->m_NumAccountIds);
            for (unsigned i = 0; i < param->m_NumAccountIds; i++)
            {
                rw.WriteUns64(NULL, param->m_AccountIds[i]);
            }
            m_UseAccountIds = true; 
        }
        else
        {
            rlDebug("Posting %u online Ids", param->m_NumOnlineIds);
            for (int i = 0; i < param->m_NumOnlineIds; i++)
            {
                rw.WriteString(NULL, param->m_OnlineIds[i].data);
            }
            m_UseAccountIds = false;
        }

		rw.End(); // "to" array
        
        rw.WriteString("message", param->m_Message);

		rw.End(); // end

		int contentLength = rw.Length();

		char invite_list[2048];
		formatf(invite_list, sizeof(invite_list),"%s\r\n%s\r\n%s\r\n%s%d\r\n\r\n%s\r\n",
			MULTIPART_BOUNDARY,
			CONTENT_TYPE_JSON,
			CONTENT_DESCRIPT_INV,
			"Content-Length:",
			contentLength,
			rsonBuf);

		u32 sessionSize;
		char sessionBuf[rlSessionInfo::TO_STRING_BUFFER_SIZE] = {0};
		rverify(param->m_SessionInfo->ToString(sessionBuf, rlSessionInfo::TO_STRING_BUFFER_SIZE, &sessionSize) != NULL, catchall, rlError("Failed to export session info"));

		char session_data[WEBAPI_SESSION_BUF_LEN] = {0};
		formatf(session_data, sizeof(session_data),"\r\n%s\r\n%s\r\n%s\r\n%s\r\n%s%d\r\n\r\n%s",
			MULTIPART_BOUNDARY,
			CONTENT_TYPE_STREAM,
			CONTENT_DISPOSITION_ATTACH,
			CONTENT_DESCRIPT_INV_DATA,
			"Content-Length:",
			(int)strlen(sessionBuf),
			sessionBuf
			);

		int lengthRequired = strlen(invite_list) + strlen(session_data) + strlen(MULTIPART_BOUNDARY_END) + 1; // null terminator

		m_Body = rage_new char[lengthRequired];
		memset(m_Body, 0, lengthRequired);

		safecat(m_Body, invite_list, lengthRequired);
		safecat(m_Body, session_data, lengthRequired);
		safecat(m_Body, MULTIPART_BOUNDARY_END, lengthRequired);

		m_Content.contentLength = strlen(m_Body);
		m_bAddContent = true;
		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpPostInvitationWorkItem::GetApiGroup()
{
    if(m_UseAccountIds)
    {
        return API_GROUP_SESSION_INVITATION;
    }
    else
    {
        return API_GROUP_SESSION_INVITATION_ONLINE_ID;
    }
}

void rlNpPostInvitationWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_POST;
	formatf(m_ApiPath, "/%s/sessions/%s/invitations", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpDeleteMemberWorkItem
****************************************/
bool rlNpDeleteMemberWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId, rlSceNpAccountId targetId)
{
	m_Identifier = DELETE_MEMBER;
	m_SessionId.Reset(sessionId.AsSceNpSessionId());
	m_TargetId = targetId;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpDeleteMemberWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpDeleteMemberWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_DELETE;
	formatf(m_ApiPath, "/%s/sessions/%s/members/%" I64FMT "u", WEBAPI_VERSION, m_SessionId.data, m_TargetId);
}

/****************************************
		rlNpDeleteSessionWorkItem
****************************************/
bool rlNpDeleteSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId)
{
	m_Identifier = DELETE_SESSION;
	m_SessionId.Reset(sessionId.AsSceNpSessionId());
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpDeleteSessionWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpDeleteSessionWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_DELETE;
	formatf(m_ApiPath, "/%s/sessions/%s/", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpGetSessionWorkItem
****************************************/
bool rlNpGetSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId)
{
	m_Identifier = GET_SESSION;
	m_SessionId.Reset(sessionId.AsSceNpSessionId());
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetSessionWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rrSession(GetBuffer(), m_ReadSize);

		m_Session.sessionId.Reset(m_SessionId.AsSceNpSessionId());

		rverify(rrSession.GetValue("sessionPrivacy", m_Session.sessionPrivacy, sizeof(m_Session.sessionPrivacy)), catchall, 
			rlError("Could not read sessionPrivacy from session info"));

		rverify(rrSession.GetValue("sessionName", m_Session.sessionName, sizeof(m_Session.sessionName)), catchall, 
			rlError("Could not read sessionName from  session info"));

		rverify(rrSession.GetValue("sessionStatus", m_Session.sessionStatus, sizeof(m_Session.sessionStatus)), catchall, 
			rlError("Could not read sessionStatus from session info"));

		RsonReader rrSessionCreator;
		rverify(rrSession.GetMember("sessionCreator", &rrSessionCreator), catchall, rlError("Could not read sessionCreator from session info"));
		rverify(rrSessionCreator.ReadUns64("accountId", m_Session.sessionCreator), catchall, rlError("Could not parse sessionCreator from session info"));

		RsonReader rrMaxUser;
		rverify(rrSession.GetMember("sessionMaxUser", &rrMaxUser), catchall, rlError("Could not read sessionMaxUser from session info"));
		rverify(rrMaxUser.AsInt(m_Session.sessionMaxUser), catchall, rlError("Could not read sessionMaxUser from session info"));

		RsonReader rrJoinable;
		rverify(rrSession.GetMember("joinableFlag", &rrMaxUser), catchall, rlError("Could not read joinableFlag from session info"));
		rverify(rrMaxUser.AsBool(m_Session.joinableFlag), catchall, rlError("Could not read joinableFlag from session info"));
		return true;
	}
	rcatchall
	{
		rlDebug3("Post session work item failed");
		return false;
	}
}

const char* rlNpGetSessionWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpGetSessionWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/sessions/%s/", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpGetSessionDataWorkItem
****************************************/
bool rlNpGetSessionDataWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId)
{
	m_Identifier = GET_SESSION_DATA;
	m_SessionId.Reset(sessionId.AsSceNpSessionId());
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetSessionDataWorkItem::ProcessSuccess()
{                   
    if(!m_SessionInfo.FromString(GetBuffer()))
	{
		rlError("rlNpGetSessionDataWorkItem :: Failed to import session data from string");
		return false;
	}

	rlDebug("rlNpGetSessionDataWorkItem::ProcessSuccess, Token: 0x%016" I64FMT "x", m_SessionInfo.GetToken().m_Value);
	return true;
}

bool rlNpGetSessionDataWorkItem::GetSessionData(rlSessionInfo* sessionInfo)
{
	rtry
	{
		rverify(sessionInfo, catchall, );
		*sessionInfo = m_SessionInfo;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpGetSessionDataWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpGetSessionDataWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/sessions/%s/sessionData", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpGetSessionListWorkItem
****************************************/
bool rlNpGetSessionListWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, int maxNumSessions)
{
	m_Identifier = GET_SESSONLIST;
	m_MaxSessions = maxNumSessions;
	m_NumSessions = 0;

	for (int i = 0; i < NP_WEB_API_MAX_SESSIONS; i++)
	{
		memset(&m_Sessions[i], 0, sizeof(m_Sessions[i]));
	}

	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);	
}

bool rlNpGetSessionListWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);
		RsonReader rrSessions;
		rverify(rr.GetMember("sessions", &rrSessions), catchall, rlError("Could not read session list"));

		// Check total first to determine result size
		RsonReader rrTotal;
		int total = 0;
		rverify(rr.GetMember("totalResults", &rrTotal), catchall, );
		rverify(rrTotal.AsInt(total), catchall, );

		// no sessions
		if (total == 0)
		{
			return true;
		}

		if (total > NP_WEB_API_MAX_SESSIONS)
		{
			rlWarning("GetSessionList returned %d sessions, clamping to %d", total, NP_WEB_API_MAX_SESSIONS);
			total = NP_WEB_API_MAX_SESSIONS;
		}

		rverify(rr.GetFirstMember(&rr),  catchall, rlError("Failed to read session list from message: \"%s\"", GetBuffer()));
		rverify(rr.CheckName("sessions"), catchall, );

		//Get the inviation list
		RsonReader rrSession;
        rverify(rr.GetFirstMember(&rrSession),  catchall, rlError("Failed to read session list from message: \"%s\"", GetBuffer()));

		bool done = false;
		for(int i = 0; i < NP_WEB_API_MAX_SESSIONS && !done; ++i, done = !rrSession.GetNextSibling(&rrSession))
		{
			rverify(rrSession.GetValue("sessionId", m_Sessions[i].sessionId.data, sizeof(m_Sessions[i].sessionId.data)),
				catchall, rlError("Failed to read invitationId from message"));
			m_NumSessions++;
		}

		return true;
	}
	rcatchall
	{
		rlDebug3("Get Session List Work Item failed");
		return false;
	}
}

void rlNpGetSessionListWorkItem::GetSessions(rlNpWebApiSessionDetail* sessions, int * numSessions)
{
	for (int i = 0; i < m_NumSessions && i < m_MaxSessions; i++)
	{
		sessions[i] = m_Sessions[i];
	}

	*numSessions = m_NumSessions;
}

const char* rlNpGetSessionListWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpGetSessionListWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/sessions", WEBAPI_VERSION, accountId);
}

/****************************************
		rlNpPostMemberWorkItem
****************************************/
bool rlNpPostMemberWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId)
{
	m_Identifier = POST_MEMBER;
	m_SessionId.Reset(sessionId.AsSceNpSessionId());
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpPostMemberWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpPostMemberWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_POST;
	formatf(m_ApiPath, "/%s/sessions/%s/members", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpPostSessionWorkItem
****************************************/
bool rlNpPostSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char* sessionImagePath, rlSession* session)
{
	rtry
	{
		rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
		rverify(sessionImagePath, catchall, );
		rverify(session, catchall, );

		m_SessionPtr = session;

		m_Identifier = POST_SESSION;
		fiStream* pStream = ASSET.Open(sessionImagePath, "", false, true);
		rverify(pStream, catchall, rlError("Could not load session image path: %s", sessionImagePath));

		int imgSize = pStream->Size();
		rverify(imgSize < RL_MAX_SESSION_IMAGE_BUF_SIZE, catchall, rlError("Session image too large!"));
		rverify(imgSize >= 0, catchall, rlError("No session image size!"));

		uint8_t img_buf[ RL_MAX_SESSION_IMAGE_BUF_SIZE ] = {0}; // 160 KB
		pStream->Read(img_buf, imgSize);
		pStream->Close();

		rverify(Configure(localGamerIndex, accountId, webApiUserCtxId, img_buf, imgSize, session), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlNpPostSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, uint8_t* imageData, int imageSize, rlSession* session)
{
	rtry
	{
		rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
		rverify(imageData, catchall, );
		rverify(session, catchall, );

		m_SessionPtr = session;

		m_Identifier = POST_SESSION;
		memset(&m_SessionId, 0, sizeof(m_SessionId));

		memset(&m_Content, 0, sizeof(m_Content));
		m_Content.pContentType = WEBAPI_CONTENT_TYPE_MULTIPART;
		
		char session_cont[2048] = {0};
		formatf(session_cont, "{\"sessionType\":\"owner-migration\",\"sessionMaxUser\":%d,\"sessionName\":\"\",\"sessionStatus\":\"\",\"availablePlatforms\":[\"PS4\"]}", session->GetMaxSlots());
		int contentLength = strlen(session_cont);

		char session_req[2048] = {0};
		formatf(session_req, sizeof(session_req),"%s\r\n%s\r\n%s\r\n%s%d\r\n\r\n%s\r\n",
			MULTIPART_BOUNDARY,
			CONTENT_TYPE_JSON,
			CONTENT_DESCRIPT_SES,
			"Content-Length:",
			contentLength,
			session_cont);

		char session_image[WEBAPI_SESSION_BUF_LEN] = {0};
		formatf(session_image, "%s\r\n%s\r\n%s\r\n%s\r\n%s%d\r\n\r\n",
			MULTIPART_BOUNDARY,
			CONTENT_TYPE_IMAGE,
			CONTENT_DISPOSITION_ATTACH,
			CONTENT_DESCRIPT_IMG,
			"Content-Length:",
			imageSize
			);

		unsigned dataLength = 0;
		char dataBuf[rlSessionInfo::TO_STRING_BUFFER_SIZE];
		rverify(session->GetSessionInfo().ToString(dataBuf, &dataLength), catchall, rlError("Failed to export changeable data"));
		rverify(dataLength > 0, catchall, rlError("Failed to write changeable data"));

		char session_changeable[WEBAPI_SESSION_BUF_LEN] = {0};
		formatf(session_changeable, sizeof(session_changeable),"\r\n%s\r\n%s\r\n%s\r\n%s\r\n%s%d\r\n\r\n%s",
			MULTIPART_BOUNDARY,
			CONTENT_TYPE_STREAM,
			CONTENT_DISPOSITION_ATTACH,
			CONTENT_DESCRIPT_CHANGEABLE_DATA,
			"Content-Length:",
			istrlen(dataBuf),
			dataBuf
			);

		int lengthRequired = strlen(session_req) + strlen(session_image) + imageSize + strlen(session_changeable) + strlen(MULTIPART_BOUNDARY_END) + 1; // +1 null terminator
		m_Length = 0;

		m_Body = rage_new char[lengthRequired];
		memset(m_Body, 0, lengthRequired);

		memcpy(&m_Body[0], session_req, strlen(session_req));
		m_Length = strlen(session_req);

		memcpy(&m_Body[m_Length], session_image, strlen(session_image));
		m_Length = m_Length + strlen(session_image);

		memcpy(&m_Body[m_Length], imageData, imageSize);
		m_Length = m_Length + imageSize;

		memcpy(&m_Body[m_Length], session_changeable, strlen(session_changeable));
		m_Length = m_Length + strlen(session_changeable);

		memcpy(&m_Body[m_Length], MULTIPART_BOUNDARY_END, strlen(MULTIPART_BOUNDARY_END));
		m_Length = m_Length + strlen(MULTIPART_BOUNDARY_END);

		m_Content.contentLength = m_Length;

		m_bAddContent = true;

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

bool rlNpPostSessionWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);
		char sessionId[SCE_NP_SESSION_ID_MAX_SIZE] = {0};
		rverify(rr.GetValue("sessionId", sessionId, sizeof(m_SessionId)), catchall, rlError("Could not parse sessionId from PostSession response"));
		safecpy(m_SessionId.data, sessionId);
		m_SessionPtr->SetWebApiSessionId(m_SessionId);
		return true;
	}
	rcatchall
	{
		rlDebug3("Post session work item failed");
		return false;
	}
}

const char* rlNpPostSessionWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpPostSessionWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_POST;
	formatf(m_ApiPath, "/%s/sessions", WEBAPI_VERSION);
}

rlNpPostSessionWorkItem::rlNpPostSessionWorkItem()
	: m_Body(NULL)
	, m_SessionPtr(NULL)
{
}

rlNpPostSessionWorkItem::~rlNpPostSessionWorkItem()
{
	if (m_Body != NULL)
	{
		delete[] m_Body;
		m_Body = NULL;
	}
}

/****************************************
		rlNpPutSessionWorkItem
****************************************/
rlNpPutSessionWorkItem::rlNpPutSessionWorkItem()
{
}

rlNpPutSessionWorkItem::~rlNpPutSessionWorkItem()
{
}

bool rlNpPutSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId, const char* sessionName, const char* sessionStatus)
{
	rtry
	{
		m_Identifier = PUT_SESSION;
		rw.Init(rsonBuf, sizeof(rsonBuf), RSON_FORMAT_JSON);

		rverify(rw.Begin(NULL, NULL), catchall, rlError("Could not initialize rsonWriter"));
		rverify(rw.WriteString("sessionName", sessionName), catchall, rlError("Could not write sessionName"));
		rverify(rw.WriteString("sessionStatus", sessionStatus), catchall, rlError("Could not write sessionStatus"));
		rverify(rw.End(), catchall, );

		m_SessionId = sessionId;

		m_bAddContent = true;
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;
		m_Content.contentLength = rw.Length();

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpPutSessionWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpPutSessionWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;
	formatf(m_ApiPath, "/%s/sessions/%s", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpPutSessionWorkItem
****************************************/
bool rlNpPutChangeableDataWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSession* session)
{
	rtry
	{
		sysMemSet(&m_Body, 0, sizeof(m_Body));
		sysMemSet(&m_Content, 0, sizeof(m_Content));

		char dataBuf[rlSessionInfo::TO_STRING_BUFFER_SIZE];
		rverify(session->GetSessionInfo().ToString(dataBuf, &m_Length), catchall, rlError("Failed to write changeable data"));

		m_SessionId = session->GetWebApiSessionId();

		m_bAddContent = true;
		m_Content.pContentType = "application/octet-stream";
		m_Content.contentLength = m_Length;

		rverify(rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlNpPutChangeableDataWorkItem::ProcessSuccess()
{
	return true;
}

const char* rlNpPutChangeableDataWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpPutChangeableDataWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;
	formatf(m_ApiPath, "/%s/sessions/%s/changeableSessionData", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpGetChangeableDataWorkItem
****************************************/
bool rlNpGetChangeableDataWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId)
{
	m_Identifier = GET_SESSION_DATA;
	m_SessionId.Reset(sessionId.AsSceNpSessionId());
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetChangeableDataWorkItem::ProcessSuccess()
{             
	unsigned size = m_ReadBuffer.Length();
	if (!m_SessionInfo.FromString(GetBuffer(), &size, false))
	{
		rlError("rlNpGetChangeableDataWorkItem :: Failed to import changeable session data");
		return false;
	}

	rlDebug("rlNpGetChangeableDataWorkItem::ProcessSuccess, Token: 0x%016" I64FMT "x", m_SessionInfo.GetToken().m_Value);
	return true;
}

bool rlNpGetChangeableDataWorkItem::GetChangeableSessionData(rlSessionInfo* sessionInfo)
{
	rtry
	{
		rverify(sessionInfo, catchall, );
		*sessionInfo = m_SessionInfo;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpGetChangeableDataWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpGetChangeableDataWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/sessions/%s/changeableSessionData", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpPutSessionImageWorkItem
****************************************/
rlNpPutSessionImageWorkItem::rlNpPutSessionImageWorkItem()
	: m_SessionImage(NULL)
	, m_SessionImgBuf(NULL)
	, m_SessionImageLength(0)
{
	m_ImagePath[0] = '\0';
}

rlNpPutSessionImageWorkItem::~rlNpPutSessionImageWorkItem()
{
	if (m_SessionImgBuf)
	{
		delete[] m_SessionImgBuf;
		m_SessionImgBuf = NULL;
	}
}

bool rlNpPutSessionImageWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId,
											const u8* sessionImage, const u32 sessionImageLength)
{
	rtry
	{
		m_Identifier = PUT_SESSION_IMAGE;
		rverify(sessionId.IsValid(), catchall, );

		m_SessionId.Reset(sessionId.AsSceNpSessionId());
		m_SessionImage = sessionImage;
		m_SessionImageLength = sessionImageLength;

		m_bAddContent = true;

		m_Content.pContentType = CONTENT_TYPE_IMAGE;
		m_Content.contentLength = sessionImageLength;
		
		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}


bool rlNpPutSessionImageWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpSessionId& sessionId, const char* imagePath)
{
	rtry
	{
		m_Identifier = PUT_SESSION_IMAGE;
		rverify(sessionId.IsValid(), catchall, );

		m_SessionId.Reset(sessionId.AsSceNpSessionId());
		safecpy(m_ImagePath, imagePath);

		m_bAddContent = true;
		m_Content.pContentType = CONTENT_TYPE_IMAGE;

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpPutSessionImageWorkItem::GetRequestBody()
{
	rtry
	{
		// Check if we need to load the image from a path.
		// If we passed in the buffer, this will simply skip.
		rcheck(m_ImagePath[0] != '\0', catchall, );
		rverify(m_SessionImage == NULL, catchall, );

		fiStream* pStream = ASSET.Open(m_ImagePath, "", false, true);
		rverify(pStream, catchall, rlError("Could not load session image path: %s", m_ImagePath));

		int sessionImageLength = pStream->Size();
		rverify(sessionImageLength < RL_MAX_SESSION_IMAGE_BUF_SIZE, catchall, rlError("Session image too large!"));
		rverify(sessionImageLength >= 0, catchall, rlError("No session image size!"));

		m_SessionImgBuf = rage_new u8[sessionImageLength];
		rverify(m_SessionImgBuf, catchall, );
		rverify(pStream->Read((char*)m_SessionImgBuf, sessionImageLength), catchall, );

		m_SessionImage = m_SessionImgBuf;
		m_SessionImageLength = sessionImageLength;
	}
	rcatchall
	{

	}

	m_Content.contentLength = m_SessionImageLength;
	return (const char*)m_SessionImage; 
}

const char* rlNpPutSessionImageWorkItem::GetApiGroup()
{
	return API_GROUP_SESSION_INVITATION;
}

void rlNpPutSessionImageWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;
	formatf(m_ApiPath, "/%s/sessions/%s/sessionImage", WEBAPI_VERSION, m_SessionId.data);
}

/****************************************
		rlNpGetCatalogueWorkItem
****************************************/
bool rlNpGetCatalogWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetCatalogDetail *detail)
{
	m_Identifier = GET_CATALOG;

	rtry
	{
		rverify(detail, catchall, );
		m_Detail = detail;
	}
	rcatchall
	{
		rlError("Failed to configure message in m_Identifier = GET_CATALOG;"); 
		return false;
	}

	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetCatalogWorkItem::ProcessSuccess()
{
	//Wasteful as hell. I'll probably switch to just using the passed buffer.
	m_Detail->GetBuffer().Append( GetBuffer(), m_ReadBuffer.Length() );
	m_Detail->GetBuffer().Append( "\0", 1 );

	return true;
}

const char* rlNpGetCatalogWorkItem::GetApiGroup()
{
	return API_GROUP_COMMERCE;
}

void rlNpGetCatalogWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/me/container/%s", WEBAPI_COMMERCE_VERSION, "?size=128&serviceLabel=0");
}

rlNpGetCatalogDetail::rlNpGetCatalogDetail()
{
	m_DataBuffer.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE, 1024);
}

/****************************************
		rlNpGetProductInfoWorkItem
****************************************/
bool rlNpGetProductInfoWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetProductDetail *detail)
{
	rtry
	{
		rverify(detail, catchall, );
		m_Identifier = GET_PRODUCT_INFO;
		m_Detail = detail;
	}
	rcatchall
	{
		return false;
	}

	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetProductInfoWorkItem::ProcessSuccess()
{
	rtry
	{
		rcheck(m_ReadBuffer.GetBuffer(), catchall, rlError("rlNpGetProductInfoWorkItem: Read buffer with no length."));
		rcheck(m_ReadBuffer.Length(), catchall, rlError("rlNpGetProductInfoWorkItem: Read buffer with no buffer ptr."));

		m_Detail->GetBuffer().Clear();

		rverify(m_Detail->GetBuffer().AppendOrFail(m_ReadBuffer.GetBuffer(), m_ReadBuffer.Length()), catchall, rlError("rlNpGetProductInfoWorkItem: Error appending read buffer to detail buffer"));

		return true;
	}
	rcatchall
	{
		if (m_Detail->GetBuffer().GetBuffer())
		{
			m_Detail->GetBuffer().Clear();
		}

		return false;
	}	
}

const char* rlNpGetProductInfoWorkItem::GetApiGroup()
{
	return API_GROUP_COMMERCE;
}

void rlNpGetProductInfoWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;

	formatf(m_ApiPath, "/%s/users/me/container/", WEBAPI_COMMERCE_VERSION);
	for (int i = 0; i < m_Detail->GetNumRequestedProducts(); i++)
	{
		strncat(m_ApiPath,m_Detail->GetRequestedProductId(i),API_PATH_MAXIMUM_SIZE);
		strncat(m_ApiPath,":",API_PATH_MAXIMUM_SIZE);
	}
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

rlNpGetProductDetail::rlNpGetProductDetail() :
	m_NumRequestedProducts(0)
{
	m_DataBuffer.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE, 1024);
	Reset();
}

bool rlNpGetProductDetail::AddRequestedProduct( const char* productId )
{
	if ( m_NumRequestedProducts >= MAX_REQUESTABLE_PRODUCTS )
	{
		return false;
	}

	m_RequestedProducts[m_NumRequestedProducts] = productId;

	m_NumRequestedProducts++;

	return true;
}

void rlNpGetProductDetail::Reset()
{
	for (int i=0; i < MAX_REQUESTABLE_PRODUCTS; i++)
	{
		m_RequestedProducts[i].Reset();
	}

	m_NumRequestedProducts = 0;

	m_DataBuffer.Clear();
}

const char* rlNpGetProductDetail::GetRequestedProductId(int requestedProductArrayIndex)
{
	rtry
	{
		rverify(requestedProductArrayIndex >= 0 && requestedProductArrayIndex < m_NumRequestedProducts, catchall, );
		return m_RequestedProducts[requestedProductArrayIndex].c_str();
	}
	rcatchall
	{
		return NULL;
	}	
}

rlNpSetEntitlementsDetail::rlNpSetEntitlementsDetail() :
	m_CallSucceeded(false),
	m_NewUseLimit(-1)
{
	
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "[%s] " fmt, GetWorkItemName()

/****************************************
		rlNpSetEntitlementCountWorkItem
****************************************/
#if RL_NP_LEGACY
bool rlNpSetEntitlementCountWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		// Check total first to determine result size
		RsonReader rrUseCount;
		int useLimit = 0;
		rverify(rr.GetMember("use_limit", &rrUseCount), catchall, );
		rverify(rrUseCount.AsInt(useLimit), catchall, );

		m_Detail->SetCallSuccessFlag(true);
		m_Detail->SetNewUseLimit(useLimit);
	}
	rcatchall
	{
		m_Detail->SetCallSuccessFlag(false);
		return false;
	}

	return true;
}

const char* rlNpSetEntitlementCountWorkItem::GetApiGroup()
{
	return API_GROUP_ENTITLEMENT;
}

void rlNpSetEntitlementCountWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_PUT;

	//Add the entitlement to decrease and the amount to decrease by.
	formatf(m_ApiPath, "/%s/users/me/entitlements/%s", WEBAPI_COMMERCE_VERSION, m_EntitlementId.c_str());
}

bool rlNpSetEntitlementCountWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpSetEntitlementsDetail* detail, const char* entitlement, int amount)
{
	rtry
	{
		m_Identifier = PUT_ENTITLEMENT_COUNT;
		m_AmountToConsume = amount;

		m_EntitlementId = entitlement;

		rw.Init(rsonBuf, sizeof(rsonBuf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.WriteInt("use_count", amount), catchall, );
		rcheck(rw.End(), catchall, );

		m_bAddContent = true;
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;
		m_Content.contentLength = rw.Length();

		rverify(detail, catchall, );
		m_Detail = detail;
		m_Detail->SetCallSuccessFlag(false);
	}
	rcatchall
	{
		return false;
	}

	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}
#else
bool rlNpSetEntitlementCountWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpSetEntitlementsDetail* detail, const char* entitlement, int amount)
{
	rtry
	{
		m_AmountToConsume = amount;
		m_EntitlementId = entitlement;

		rverify(detail, catchall, );
		m_Detail = detail;
		m_Detail->SetCallSuccessFlag(false);

		m_Status.SetPending();
	}
	rcatchall
	{
		return false;
	}

	return rlNpWebApiWorkItemBase::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpSetEntitlementCountWorkItem::DoWork()
{
	// This will work for PSCON but not for PSVC see the documentation for more details as that now requires the use of a 
	// a game server to consume them.
	//
	// While managing consumable entitlements in a game server is more complex, it is ***required*** for virtual currency 
	// (PSVC) entitlements and is best practice for other consumable (PSCONS) entitlements.
	//
	// Docs   : https://p.siedev.net/resources/documents/WebAPI/1/Entitlements-Overview/0003.html#__document_toc_00000009
	// Errors : https://p.siedev.net/resources/documents/SDK/2.000/NpEntitlementAccess-Reference/0028.html

	/* Generate a transaction ID */
	SceNpEntitlementAccessTransactionId transactionId;
	memset(&transactionId, 0, sizeof(transactionId));
	int32_t ret = sceNpEntitlementAccessGenerateTransactionId(&transactionId);
	if (ret < SCE_OK)
	{
		rlError("rlNpSetEntitlementCountWorkItem :: Failed in sceNpEntitlementAccessGenerateTransactionId 0x%08x", ret);
		m_Status.SetFailed();
	}
	else
	{
		SceUserServiceUserId userId = g_rlNp.GetUserServiceId(m_LocalGamerIndex);
		SceNpServiceLabel serviceLabel = g_rlNp.GetNpEntitlementServiceLabel();
		SceNpUnifiedEntitlementLabel entitlementLabel;
		memset(&entitlementLabel, 0, sizeof(entitlementLabel));
		strncpy(entitlementLabel.data, m_EntitlementId, SCE_NP_UNIFIED_ENTITLEMENT_LABEL_SIZE);

		int32_t useCount = m_AmountToConsume;
		int64_t requestId;

		// Request
		ret = sceNpEntitlementAccessRequestConsumeUnifiedEntitlement(
			userId,					// User ID of the access-target user
			serviceLabel,			// NP service label of the target entitlement
			&entitlementLabel,		// Unified entitlement label of the target entitlement
			&transactionId,			// generated transaction ID
			useCount,				// Number of entitlement counts to consume
			&requestId				// The ID of the created list will be stored
		);

		if (ret < SCE_OK)
		{
			rlError("rlNpSetEntitlementCountWorkItem :: Failed sceNpEntitlementAccessRequestConsumeUnifiedEntitlement 0x%08x", ret);
			m_Status.SetFailed();
		}

		int32_t useLimit = -1;
		int32_t result = SCE_OK;

		if (ret == SCE_OK)
		{
			/* Obtain the result of a request to consume a consumable entitlement */
			while (1)
			{
				if (!WasCanceled())
				{
					ret = sceNpEntitlementAccessPollConsumeEntitlement(requestId, &result, &useLimit);
					if (ret == SCE_NP_ENTITLEMENT_ACCESS_POLL_ASYNC_RET_RUNNING)
					{
						sceKernelUsleep(100 * 1000);
						continue;
					}
					else
					{
						break;
					}
				}
				else
				{
					rlWarning("rlNpSetEntitlementCountWorkItem :: Request was canceled");
					ret = SCE_NP_ENTITLEMENT_ACCESS_ERROR_ABORTED;
					break;
				}
			}
		}

		if (ret < SCE_OK)
		{
			rlError("rlNpSetEntitlementCountWorkItem :: sceNpEntitlementAccessPollConsumeEntitlement failed. ret = 0x%08x", ret);
			if (result < SCE_OK) {
				rlError("rlNpSetEntitlementCountWorkItem :: sceNpEntitlementAccessPollConsumeEntitlement() failed. result = 0x%08x \n", result);
			}
			m_Status.SetFailed();
		}
		else if (ret == SCE_NP_ENTITLEMENT_ACCESS_ERROR_ABORTED)
		{
			rlError("rlNpSetEntitlementCountWorkItem :: sceNpEntitlementAccessPollConsumeEntitlement() failed. result = 0x%08x \n", ret);

			m_Status.SetCanceled();

			ret = sceNpEntitlementAccessAbortRequest(requestId);
			if (ret < SCE_OK) {
				rlError("rlNpSetEntitlementCountWorkItem :: sceNpEntitlementAccessAbortRequest() failed. ret = 0x%08x", ret);
			}
		}
		else if (ret == SCE_NP_ENTITLEMENT_ACCESS_POLL_RET_FINISHED)
		{
			if (result < SCE_OK)
			{
				rlAssertf(result != SCE_NP_ENTITLEMENT_ACCESS_SERVER_ERROR_MISSING_SCOPE_FOR_VC_CONSUMPTION, "rlNpSetEntitlementCountWorkItem : FAILED : In-game currency can only be consumed from a game server; it cannot be consumed through the system software.");
				rlError("rlNpSetEntitlementCountWorkItem :: sceNpEntitlementAccessPollConsumeEntitlement() failed. result = 0x%08x \n", result);
				m_Status.SetFailed();
			}
			else
			{
				m_Status.SetSucceeded();
			}
		}

		if (m_Status.Failed() || m_Status.Canceled())
		{
			m_Detail->SetCallSuccessFlag(false);
		}
		else if (m_Status.Succeeded())
		{
			m_Detail->SetCallSuccessFlag(true);
			m_Detail->SetNewUseLimit(useLimit);
		}

		ret = sceNpEntitlementAccessDeleteRequest(requestId);
		if (ret < SCE_OK) {
			rlError("rlNpSetEntitlementCountWorkItem :: sceNpEntitlementAccessDeleteRequest() failed. ret = 0x%08x", ret);
		}
	}
}
#endif

/****************************************
		rlNpGetEntitlementsWorkItem
****************************************/
bool rlNpGetEntitlementsWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetEntitlementsDetail *detail)
{
	m_Identifier = GET_ENTITLEMENTS;

	rtry
	{
		rverify(detail, catchall, );
		m_Detail = detail;
	}
	rcatchall
	{
		rlError("Failed to configure message in rlNpGetEntitlementsWorkItem::Configure"); 
		return false;
	}

	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

bool rlNpGetEntitlementsWorkItem::ProcessSuccess()
{
	rtry
	{
		rcheck(m_ReadBuffer.GetBuffer(), catchall, rlError("rlNpGetEntitlementsWorkItem: Read buffer with no length."));
		rcheck(m_ReadBuffer.Length(), catchall, rlError("rlNpGetEntitlementsWorkItem: Read buffer with no buffer ptr."));

		m_Detail->GetBuffer().Clear();
		rverify(m_Detail->GetBuffer().AppendOrFail(m_ReadBuffer.GetBuffer(), m_ReadBuffer.Length()), catchall, rlError("rlNpGetEntitlementsWorkItem: Error appending read buffer to detail buffer"));

		return true;
	}
	rcatchall
	{
		if (m_Detail->GetBuffer().GetBuffer())
		{
			m_Detail->GetBuffer().Clear();
		}

		return false;
	}	
}

const char* rlNpGetEntitlementsWorkItem::GetApiGroup()
{
	return API_GROUP_ENTITLEMENT;
}

void rlNpGetEntitlementsWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/me/entitlements%s", WEBAPI_COMMERCE_VERSION, "?entitlement_type=service&size=128&service_label=0");
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

/****************************************
	rlNpGetEntitlementsDetail
****************************************/
rlNpGetEntitlementsDetail::rlNpGetEntitlementsDetail()
{
	m_DataBuffer.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE, 1024);
}

void rlNpGetEntitlementsDetail::Reset()
{
	m_DataBuffer.Clear();
}

/****************************************
	rlNpPostActivityFeedStoryWorkItem
****************************************/

void rlNpActivityFeedStory::Reset()
{
	m_postTypeEnum = 0;
	m_hasCaptions = false;
	m_contentUsedFlag = 0;
	m_thumbnailUsedFlag = 0;
	m_noofSubStrings = 0;
	m_noofSubValues = 0;

#if __ASSERT
	m_noofMainThumbnailCalls = 0;
#endif
}

void rlNpActivityFeedStory::BuildAllCaptions()
{
	for (sysLanguage language = LANGUAGE_ENGLISH; language < MAX_LANGUAGES; ++language)
	{
		BuildCaption(language);
	}
}

void rlNpActivityFeedStory::BuildCaption(sysLanguage language)
{
	// string to work with and fill in
	char oldString[maxStringLength];
	formatf(oldString, maxStringLength, "%s", m_caption[language]);

	int currentSubString = 0;
	int currentSubValue = 0;
	
	int newIndex = 0;
	char* currentChar = &oldString[0];
	while (*currentChar != '\0' && newIndex < maxStringLength)
	{
		if (*currentChar == '~' && maxStringLength - newIndex > 3) // don't bother if there isn't enough room for a ~a~
		{
			char* testChar = currentChar+1;
			if ((*testChar >= 'a' && *testChar <= 'z' ) ||
				(*testChar == '1'))
			{
				bool isValue = (*testChar == '1');
				++testChar;
				if (*testChar == '~')
				{
					// if we're here, then we have a full tag to replace
					char* subStringChar = NULL;
					int maxSubLength = maxSubStringLength;
					if (isValue)
					{
						subStringChar = m_subValue[currentSubValue];
						++currentSubValue;
						maxSubLength = maxSubValueLength;
					}
					else
					{
						subStringChar = m_subString[currentSubString][language];
						++currentSubString;
					}

					int subIndex = 0;
					while (*subStringChar != '\0' && subIndex < maxSubLength && newIndex < maxStringLength )
					{
						m_caption[language][newIndex] = *subStringChar;
						++subStringChar;
						++subIndex;
						++newIndex;
					}

					currentChar = testChar+1;

					continue;
				}
			}
		}

		m_caption[language][newIndex] = currentChar[0];
		++currentChar;
		++newIndex;
	}

	if (newIndex >= maxStringLength)
	{
		rlAssertf(true, "rlNpActivityFeedStory::BuildCaption - caption built with substrings larger than maxStringLength - language: %d - string: %s", static_cast<int>(language), m_caption[language]);
		newIndex = maxStringLength-1; // make the string valid anyway.
	}

	m_caption[language][newIndex] = '\0';
}

const char* rlNpActivityFeedStory::GetTag(sysLanguage language, ContentType::Enum contentType) const
{
	if(contentType >= ContentType::URL)
	{
		return m_tag[contentType-ContentType::URL][language];
	}
	
	return NULL;
}

const char* rlNpActivityFeedStory::GetThumbnail(ContentType::Enum contentType) const
{
	if(contentType >= ContentType::URL)
	{
		if (m_thumbnailUsedFlag & 1 << contentType)
			return m_thumbnail[contentType-ContentType::URL];
	}

	return NULL;
}

const char* rlNpActivityFeedStory::GetImageAspectRatio() const
{
	return m_smallImageAspectRatio;
}


void rlNpActivityFeedStory::SetImageAspectRatio(const char* aspectRatio)
{
	formatf(m_smallImageAspectRatio, aspectRatioStringLength, "%s", aspectRatio);
}

bool rlNpActivityFeedStory::HasContent(ContentType::Enum contentType) const
{
	return (m_contentUsedFlag & (1 << contentType));
}

void rlNpActivityFeedStory::StartContent(unsigned int postType)
{
	rlAssertf(postType != 0, "rlNpActivityFeedStory::StartContent - can't use zero as a postType");
	Reset();
	m_postTypeEnum = postType;
}

void rlNpActivityFeedStory::AddCaptions(sysLanguage language, const char* caption, const char* condensedCaption)
{
#if __ASSERT
	if (language == LANGUAGE_ENGLISH)
	{
		rlAssertf(!m_hasCaptions, "rlNpActivityFeedStory::AddCaptions - already has captions");
	}
#endif
	m_hasCaptions = true;
	rlAssertf(strlen(caption) < maxStringLength, "rlNpActivityFeedStory::AddCaptions - caption must be less than %d bytes - language: %d string: %s", maxStringLength, static_cast<int>(language), caption);
	formatf(m_caption[language], maxStringLength, "%s", caption);
	rlAssertf(strlen(condensedCaption) < maxCondensedStringLength, "rlNpActivityFeedStory::AddCaptions - condensedCaption must be less than %d bytes. Can't be increased. - language: %d string: %s", maxCondensedStringLength, static_cast<int>(language), condensedCaption);
	formatf(m_condensedCaption[language], maxCondensedStringLength, "%s", condensedCaption);
}

void rlNpActivityFeedStory::AddContent(ContentType::Enum contentType, const char* string)
{
#if __ASSERT
	if (contentType == ContentType::thumbnail)
	{
		rlAssertf(m_noofMainThumbnailCalls==0, "rlNpActivityFeedStory::AddContent - already has content: contentType: %d - flagValue: 0x%08x - existing content string: %s", contentType, m_contentUsedFlag, m_content[contentType]);
		++m_noofMainThumbnailCalls;
	}
#endif

	// We allow the largeImage to be overwritten
	rlAssertf(contentType == rlNpActivityFeedStory::ContentType::largeImage || !HasContent(contentType),
		"rlNpActivityFeedStory::AddContent - already has content: contentType: %d - flagValue: 0x%08x - existing content string: %s", contentType, m_contentUsedFlag, m_content[contentType]);

	const bool isImg = contentType == ContentType::thumbnail || contentType == ContentType::largeImage || contentType == ContentType::smallImage;
	if (isImg && !StringStartsWithI(string, "http://") && !StringStartsWithI(string, "https://"))
	{
		char buf[RL_MAX_URL_BUF_LENGTH];

		// We have to use the cloud path here because the PS4 doasboard doesn't have a ticket
		if (g_rlTitleId->m_RosTitleId.GetEnvironmentName() == nullptr)
		{
			// If there's no ENV name we're probably offline and won't send the activity but just for safety we assume we're in prod
			formatf(buf, "https://prod.%s/%s", RLROS_CLOUD_BASE_DOMAIN_NAME, string);
		}
		else
		{
			formatf(buf, "https://%s/%s", g_rlTitleId->m_RosTitleId.GetCloudDomainName(), string);
		}

		rlAssertf(strlen(buf) < maxStringLength, "rlNpActivityFeedStory::AddContent - composed string must be less than %d bytes - %s", maxStringLength, buf);
		formatf(m_content[contentType], maxStringLength, "%s", buf);
	}
	else
	{
		rlAssertf(strlen(string) < maxStringLength, "rlNpActivityFeedStory::AddContent - string must be less than %d bytes - %s", maxStringLength, string);
		formatf(m_content[contentType], maxStringLength, "%s", string);
	}

	m_contentUsedFlag |= 1 << contentType;
}

void rlNpActivityFeedStory::AppendContent(ContentType::Enum contentType, const char* string)
{
	rlAssertf((HasContent(contentType)), "rlNpActivityFeedStory::AppendContent - does not yet have content to add to: contentType: %d - flagValue: 0x%08x - existing content string: %s", contentType, m_contentUsedFlag, m_content[contentType]);

	int currentLength = strlen(m_content[contentType]);
	rlAssertf(currentLength, "rlNpActivityFeedStory::AppendContent - String length is 0. Can't append to what isn't there.");

	if (contentType == ContentType::startAction && m_content[contentType][currentLength -1] != '=') // ensure gaps for command lines
	{
		rlAssertf(currentLength < maxStringLength, "rlNpActivityFeedStory::AppendContent - adding space before content failed, as you've already run out of space");

		m_content[contentType][currentLength] = ' ';
		++currentLength;
	}

	rlAssertf(strlen(string) + currentLength < maxStringLength, "rlNpActivityFeedStory::AppendContent - content string must be less than %d bytes. Append goes over limit - %s - %s", maxStringLength, m_content[contentType], string);
	formatf(&m_content[contentType][currentLength], maxStringLength-currentLength, "%s", string);
	
	m_contentUsedFlag |= 1 << contentType;
}

void rlNpActivityFeedStory::AddContentTag(sysLanguage language, ContentType::Enum contentType, const char* tag)
{
	if(contentType >= ContentType::URL)
	{
		rlAssertf(strlen(tag) < maxTagStringLength, "rlNpActivityFeedStory::AddContentTag - tag string must be less than %d bytes. Can't be increased. - language: %d - %s", maxTagStringLength, static_cast<int>(language), tag);
		formatf(m_tag[contentType-ContentType::URL][language], maxTagStringLength, "%s", tag);
	}
}


void rlNpActivityFeedStory::AddContentThumbnail(ContentType::Enum contentType, const char* thumbnail)
{
	if(contentType >= ContentType::URL)
	{
		rlAssertf(strlen(thumbnail) < maxStringLength, "rlNpActivityFeedStory::AddContentThumbnail - thumbnail URL must be less than %d bytes - %s", maxStringLength, thumbnail);
		formatf(m_thumbnail[contentType-ContentType::URL], maxStringLength, "%s", thumbnail);
		m_thumbnailUsedFlag |= 1 << contentType;
	}
}

void rlNpActivityFeedStory::AddCaptionSubString(sysLanguage language, const char* subString)
{
	if (m_noofSubStrings >= maxNoofSubStrings)
	{
		rlAssertf(true, "rlNpActivityFeedStory::AddCaptionSubString - hit max number of Sub Strings");
		return;
	}

	rlAssertf(strlen(subString) < maxSubStringLength, "rlNpActivityFeedStory::AddCaptionSubString - substring must be less than %d bytes - language: %d string: %s", maxSubStringLength, static_cast<int>(language), subString);

	formatf(m_subString[m_noofSubStrings][language], maxSubStringLength, "%s", subString);
}

void rlNpActivityFeedStory::ConfirmCaptionSubStringComplete()
{
	if (m_noofSubStrings >= maxNoofSubStrings)
	{
		rlAssertf(true, "rlNpActivityFeedStory::ConfirmCaptionSubStringComplete - hit max number of Sub Strings");
		return;
	}

	++m_noofSubStrings;
}

void rlNpActivityFeedStory::AddCaptionSubFloat(float subValue, int noofDecimalPlaces)
{
	if (m_noofSubValues >= maxNoofSubValues)
	{
		rlAssertf(true, "rlNpActivityFeedStory::AddCaptionSubFloat - hit max number of Sub Values");
		return;
	}

	formatf(m_subValue[m_noofSubValues], maxSubValueLength, "%.*f", noofDecimalPlaces, subValue);
	++m_noofSubValues;
}

void rlNpActivityFeedStory::AddCaptionSubInt(int subValue)
{
	if (m_noofSubValues >= maxNoofSubValues)
	{
		rlAssertf(true, "rlNpActivityFeedStory::AddCaptionSubInt - hit max number of Sub Values");
		return;
	}

	formatf(m_subValue[m_noofSubValues], maxSubValueLength, "%d", subValue);
	++m_noofSubValues;
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "[%s] " fmt, GetWorkItemName()

rlNpPostActivityFeedStoryWorkItem::rlNpPostActivityFeedStoryWorkItem()
{
	m_Body = NULL;
}

rlNpPostActivityFeedStoryWorkItem::~rlNpPostActivityFeedStoryWorkItem()
{
	if (m_Body != NULL)
	{
		delete[] m_Body;
		m_Body = NULL;
	}
}

void rlNpPostActivityFeedStoryWorkItem::BuildCaptionsString(char* o_data, int dataSize, bool isCondensed, rlNpActivityFeedStory *detail)
{
	const char* captionType = isCondensed ? "condensedCaptions" : "captions";

	// if a regular caption, check to see if we need to insert any sub strings
	if (!isCondensed && detail->HasSubStrings())
	{
		detail->BuildAllCaptions();
	}

	formatf(o_data, sizeof(char)*dataSize,ACTIVITY_API_IN_GAME_POST_BUTTON_CAPTIONS
		, captionType
		, detail->GetCaption(LANGUAGE_ENGLISH, isCondensed)
		, detail->GetCaption(LANGUAGE_ENGLISH, isCondensed) // -GB
		, detail->GetCaption(LANGUAGE_FRENCH, isCondensed)
		, detail->GetCaption(LANGUAGE_GERMAN, isCondensed)
		, detail->GetCaption(LANGUAGE_ITALIAN, isCondensed)
		, detail->GetCaption(LANGUAGE_SPANISH, isCondensed)
		, detail->GetCaption(LANGUAGE_PORTUGUESE, isCondensed)
		, detail->GetCaption(LANGUAGE_POLISH, isCondensed)
		, detail->GetCaption(LANGUAGE_RUSSIAN, isCondensed)
		, detail->GetCaption(LANGUAGE_KOREAN, isCondensed)
		, detail->GetCaption(LANGUAGE_CHINESE_TRADITIONAL, isCondensed)
		, detail->GetCaption(LANGUAGE_JAPANESE, isCondensed)
		, detail->GetCaption(LANGUAGE_MEXICAN, isCondensed)
		, detail->GetCaption(LANGUAGE_CHINESE_SIMPLIFIED, isCondensed)
	);
}

bool rlNpPostActivityFeedStoryWorkItem::BuildActionString(char* o_data, int dataSize, rlNpActivityFeedStory::ContentType::Enum contentType, rlNpActivityFeedStory *detail)
{
	if ( detail && detail->HasContent(contentType) )
	{
		const char* tag = detail->GetTag(LANGUAGE_ENGLISH, contentType);

		char buttonCaptions[WEBAPI_ACTFEED_CATEGORY_BUF_LEN] = {0};
		if (tag)
		{
			formatf(buttonCaptions, sizeof(buttonCaptions),ACTIVITY_API_IN_GAME_POST_BUTTON_CAPTIONS
				, "buttonCaptions"
				, detail->GetTag(LANGUAGE_ENGLISH, contentType)
				, detail->GetTag(LANGUAGE_ENGLISH, contentType) // -GB
				, detail->GetTag(LANGUAGE_FRENCH, contentType)
				, detail->GetTag(LANGUAGE_GERMAN, contentType)
				, detail->GetTag(LANGUAGE_ITALIAN, contentType)
				, detail->GetTag(LANGUAGE_SPANISH, contentType)
				, detail->GetTag(LANGUAGE_PORTUGUESE, contentType)
				, detail->GetTag(LANGUAGE_POLISH, contentType)
				, detail->GetTag(LANGUAGE_RUSSIAN, contentType)
				, detail->GetTag(LANGUAGE_KOREAN, contentType)
				, detail->GetTag(LANGUAGE_CHINESE_TRADITIONAL, contentType)
				, detail->GetTag(LANGUAGE_JAPANESE, contentType)
				, detail->GetTag(LANGUAGE_MEXICAN, contentType)
				, detail->GetTag(LANGUAGE_CHINESE_SIMPLIFIED, contentType)
				);
		}
		else
		{
			formatf(buttonCaptions," ");
		}

		const char* thumbnail = detail->GetThumbnail(contentType);

		char imageURL[WEBAPI_ACTFEED_SECTION_BUF_LEN] = {0};
		if (thumbnail)
		{
			formatf(imageURL, sizeof(imageURL),ACTIVITY_API_IN_GAME_POST_ACTION_IMAGE_URL, thumbnail);
		}
		else
		{
			formatf(imageURL, " ");
		}
		
		const char* templateString = rlNpActivityFeedStory::ContentType::URL == contentType ? ACTIVITY_API_IN_GAME_POST_ACTIONS_URL : rlNpActivityFeedStory::ContentType::startAction == contentType ? ACTIVITY_API_IN_GAME_POST_ACTIONS_STARTGAME : ACTIVITY_API_IN_GAME_POST_ACTIONS_STORE;

		// action type
		// content path
		// caption for button
		formatf(o_data, sizeof(char)*dataSize, templateString
			, detail->GetContent(contentType)
			, imageURL
			, buttonCaptions
			);

		return true;
	}

	return false;
}

bool rlNpPostActivityFeedStoryWorkItem::BuildImageString(char* o_data, int dataSize, rlNpActivityFeedStory::ContentType::Enum contentType, rlNpActivityFeedStory *detail)
{
	if ( detail && detail->HasContent(contentType) )
	{
		const char* templateString = rlNpActivityFeedStory::ContentType::smallImage == contentType ? ACTIVITY_API_IN_GAME_POST_IMAGES_SMALL : rlNpActivityFeedStory::ContentType::largeImage == contentType ? ACTIVITY_API_IN_GAME_POST_IMAGES_LARGE : rlNpActivityFeedStory::ContentType::thumbnail == contentType ? ACTIVITY_API_IN_GAME_POST_IMAGES_THUMBNAIL : ACTIVITY_API_IN_GAME_POST_IMAGES_VIDEO;

		const char* aspectRatioString = rlNpActivityFeedStory::ContentType::smallImage == contentType ? detail->GetImageAspectRatio() : " ";

		// action type
		// content path
		// caption for button
		formatf(o_data, sizeof(char)*dataSize, templateString
			, detail->GetContent(contentType)
			, aspectRatioString
			);

		return true;
	}

	return false;
}

bool rlNpPostActivityFeedStoryWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpActivityFeedStory *detail)
{
	rtry
	{
		rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
		rverify(detail, catchall, );

		m_Identifier = POST_ACTIVITYFEED_STORY;

		memset(&m_Content, 0, sizeof(m_Content));
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;

		char captionsData[WEBAPI_ACTFEED_CAPTION_BUF_LEN] = {0};
		BuildCaptionsString(captionsData, WEBAPI_ACTFEED_CAPTION_BUF_LEN, false, detail);

		char condensedCaptionsData[WEBAPI_ACTFEED_CAPTION_BUF_LEN] = {0};
		BuildCaptionsString(condensedCaptionsData, WEBAPI_ACTFEED_CAPTION_BUF_LEN, true, detail);

		char customCaptionData[WEBAPI_ACTFEED_SECTION_BUF_LEN] = {0};
		{
			if (detail->HasContent(rlNpActivityFeedStory::ContentType::customCaption))
			{
				const char* customCaptionString = detail->GetContent(rlNpActivityFeedStory::ContentType::customCaption);

				formatf(customCaptionData, sizeof(customCaptionData), ACTIVITY_API_IN_GAME_POST_CUSTOM_CAPTION, customCaptionString);
			}
			else
			{
				formatf(customCaptionData, " ");
			}
		}

		char actionsData[WEBAPI_ACTFEED_ACTIONS_BUF_LEN] = {0};
		{
			char urlString[WEBAPI_ACTFEED_CATEGORY_BUF_LEN] = {0};
			bool hasURL = BuildActionString(urlString, WEBAPI_ACTFEED_CATEGORY_BUF_LEN, rlNpActivityFeedStory::ContentType::URL, detail);

			char startString[WEBAPI_ACTFEED_CATEGORY_BUF_LEN] = {0};
			bool hasStart = BuildActionString(startString, WEBAPI_ACTFEED_CATEGORY_BUF_LEN, rlNpActivityFeedStory::ContentType::startAction, detail);;

			char storeString[WEBAPI_ACTFEED_CATEGORY_BUF_LEN] = {0};
			bool hasStore = BuildActionString(storeString, WEBAPI_ACTFEED_CATEGORY_BUF_LEN, rlNpActivityFeedStory::ContentType::store, detail);;

			if ( hasURL || hasStart || hasStore )
			{
				formatf(actionsData, sizeof(actionsData),ACTIVITY_API_IN_GAME_POST_ACTIONS
					, hasURL ? urlString : " "
					, (hasURL && (hasStart || hasStore)) ? "," : " "
					, hasStart ? startString : " "
					, ((hasURL || hasStart) && hasStore) ? "," : " "
					, hasStore ? storeString : " "
					);
			}
			else
			{
				formatf(actionsData, " ");
			}
		}

		// images
		char imageData[WEBAPI_ACTFEED_ACTIONS_BUF_LEN] = {0};
		{
			char smallImageString[WEBAPI_ACTFEED_SECTION_BUF_LEN] = {0};
			bool hasSmallImage = BuildImageString(smallImageString, WEBAPI_ACTFEED_SECTION_BUF_LEN, rlNpActivityFeedStory::ContentType::smallImage, detail);

			char largeImageString[WEBAPI_ACTFEED_SECTION_BUF_LEN] = {0};
			bool hasLargeImage = BuildImageString(largeImageString, WEBAPI_ACTFEED_SECTION_BUF_LEN, rlNpActivityFeedStory::ContentType::largeImage, detail);

			char videoString[WEBAPI_ACTFEED_SECTION_BUF_LEN] = {0};
			bool hasVideo = BuildImageString(videoString, WEBAPI_ACTFEED_SECTION_BUF_LEN, rlNpActivityFeedStory::ContentType::video, detail);

			char thumbString[WEBAPI_ACTFEED_SECTION_BUF_LEN] = {0};
			bool hasThumb = BuildImageString(thumbString, WEBAPI_ACTFEED_SECTION_BUF_LEN, rlNpActivityFeedStory::ContentType::thumbnail, detail);

			if ( hasThumb || hasSmallImage || hasLargeImage || hasVideo )
			{
				formatf(imageData, sizeof(imageData),ACTIVITY_API_IN_GAME_POST_IMAGES
					, hasSmallImage ? smallImageString : " "
					, hasLargeImage ? largeImageString : " "
					, hasVideo ? videoString : " "
					, hasThumb ? thumbString : " "
					);
			}
			else
			{
				formatf(imageData, " ");
			}
		}


		// create a running total of the length of the strings that will be compiled together later
		const unsigned int messageLength = strlen(captionsData) + 
			strlen(condensedCaptionsData) +
			strlen(customCaptionData) +
			strlen(actionsData) +
			strlen(imageData) +
			512; // more than enough for onlineID twice, TITLE_ID, PostType and the message itself;

		const char* TITLE_ID = g_rlTitleId->m_NpTitleId.GetSceNpTitleId()->id;

		// online ID
		// caption
		// condensed caption
		// ACTIVITY_API_IN_GAME_POST_ACTIONS
		// subType
		// ACTIVITY_API_IN_GAME_POST_IMAGES
		// online ID
		// titleID
		char* data = rage_new char[messageLength];
		formatf(data, sizeof(char)*messageLength,ACTIVITY_API_IN_GAME_POST_BODY_EMPTY
					, accountId
					, captionsData
					, condensedCaptionsData
					, customCaptionData
					, actionsData
					, detail->GetPostType()
					, imageData
					, accountId
					, TITLE_ID);

		int lengthRequired = strlen(data) + 1;

#if !__NO_OUTPUT
		const unsigned int lengthOfPrintString = 255;

		char printString[lengthOfPrintString+1];
		for (unsigned int offset = 0; offset <= lengthRequired; offset+=lengthOfPrintString)
		{
			formatf(printString, lengthOfPrintString+1, "%s", &data[offset]);
			rlDebug3("[ACTIVITY_FEED] %s", printString);
		}
#endif
		m_Length = 0;

		m_Body = rage_new char[lengthRequired];
		memset(m_Body, 0, lengthRequired);

		memcpy(&m_Body[0], data, strlen(data));
		m_Length = strlen(data);

		delete[] data;

		m_Content.contentLength = m_Length;

		m_bAddContent = true;

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpPostActivityFeedStoryWorkItem::GetApiGroup()
{
	return API_GROUP_ACTIVITYFEED;
}

void rlNpPostActivityFeedStoryWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_POST;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/feed", WEBAPI_VERSION, accountId);
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

/****************************************
	rlNpPostActivityFeedPlayedWithWorkItem

****************************************/

void rlNpActivityFeedPlayedWithDetails::Reset()
{
	m_noofPlayersInList = 0;
	m_playerUsed = 0;
}

void rlNpActivityFeedPlayedWithDetails::Start(const char* gameMode)
{
	Reset();

	formatf(m_gameMode, sizeof(m_gameMode), "%s", gameMode);
}

void rlNpActivityFeedPlayedWithDetails::AddPlayerToList(const rlGamerHandle& player)
{
	m_player[m_noofPlayersInList] = player;
	m_playerUsed |= 1 << m_noofPlayersInList;
	++m_noofPlayersInList;
}

void rlNpActivityFeedPlayedWithDetails::IgnoreLocalPlayer(const rlGamerHandle& player)
{
	for (int index = 0; index < maxNoofPlayers; ++index)
	{
		// don't list local player
		if (m_player[index] == player)
			m_playerUsed = m_playerUsed & ~index;
	}
}

const rlGamerHandle& rlNpActivityFeedPlayedWithDetails::GetPlayer(int index) const
{
	if (m_playerUsed & 1 << index)
		return m_player[index];

	return RL_INVALID_GAMER_HANDLE;
}

const char* rlNpActivityFeedPlayedWithDetails::GetGameMode() const
{
	return m_gameMode;
}

bool rlNpActivityFeedPlayedWithDetails::HasPlayer(int index) const
{
	return (m_playerUsed & 1 << index);
}

int rlNpActivityFeedPlayedWithDetails::GetNoofPlayers() const
{
	return m_noofPlayersInList;
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "[%s] " fmt, GetWorkItemName()

rlNpPostActivityFeedPlayedWithWorkItem::rlNpPostActivityFeedPlayedWithWorkItem()
{
	m_Body = NULL;
}

rlNpPostActivityFeedPlayedWithWorkItem::~rlNpPostActivityFeedPlayedWithWorkItem()
{
	if (m_Body != NULL)
	{
		delete[] m_Body;
		m_Body = NULL;
	}
}

bool rlNpPostActivityFeedPlayedWithWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpActivityFeedPlayedWithDetails* detail)
{
	rtry
	{
		rverify(accountId != RL_NETMODE_INVALID, catchall, );
		rverify(detail, catchall, );

		m_Identifier = POST_ACTIVITYFEED_STORY;

		memset(&m_Content, 0, sizeof(m_Content));
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;

		rlGamerHandle hLocalGamer;
		rlPresence::GetGamerHandle(localGamerIndex, &hLocalGamer);

		// we don't list the local player
		detail->IgnoreLocalPlayer(hLocalGamer);

		// build list of other players
		char playersData[WEBAPI_ACTFEED_ACTIONS_BUF_LEN] = {0};
		BuildPlayerString(playersData, sizeof(playersData),detail);

		// add current game mode
		char gameModeData[WEBAPI_ACTFEED_SECTION_BUF_LEN] = {0};
		formatf(gameModeData, sizeof(gameModeData),ACTIVITY_API_PLAYED_WITH_GAME_MODE
			, detail->GetGameMode() );

		const char* TITLE_ID = g_rlTitleId->m_NpTitleId.GetSceNpTitleId()->id;

		// user online ID
		// ACTIVITY_API_PLAYED_WITH_GAME_MODE
		// ACTIVITY_API_PLAYED_WITH_PLAYER ...multiple
		// title ID
		char data[WEBAPI_ACTFEED_BUF_LEN] = {0};
		formatf(data, sizeof(data),ACTIVITY_API_PLAYED_WITH
 			, accountId
			, gameModeData
 			, playersData
			, TITLE_ID);

		int lengthRequired = strlen(data) + 1;
		m_Length = 0;

		m_Body = rage_new char[lengthRequired];
		memset(m_Body, 0, lengthRequired);

		memcpy(&m_Body[0], data, strlen(data));
		m_Length = strlen(data);

		m_Content.contentLength = m_Length;

		m_bAddContent = true;

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

void rlNpPostActivityFeedPlayedWithWorkItem::BuildPlayerString(char* o_data, int dataSize, rlNpActivityFeedPlayedWithDetails* detail)
{
	const int noofPlayers = detail->GetNoofPlayers();

	int strLength = 0;
	int totalStringLength = 0;

	for (int index = 0; index < noofPlayers; ++index)
	{
		const rlGamerHandle& gamer = detail->GetPlayer(index);

		if (gamer.GetNpAccountId() != RL_INVALID_NP_ACCOUNT_ID)
		{
			int bufferRemaining = dataSize - totalStringLength;
			rlAssertf(bufferRemaining > 0, "rlNpPostActivityFeedPlayedWithWorkItem::BuildPlayerString - run out of player string buffer"); // very unlikely

			formatf(o_data, bufferRemaining, ACTIVITY_API_PLAYED_WITH_PLAYER, gamer.GetNpAccountId()); // appending
			strLength = strlen(o_data);
			o_data += strLength;
			totalStringLength += strLength;
		}
	}
}

const char* rlNpPostActivityFeedPlayedWithWorkItem::GetApiGroup()
{
	return API_GROUP_ACTIVITYFEED;
}

void rlNpPostActivityFeedPlayedWithWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_POST;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/feed", WEBAPI_VERSION, accountId);
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT     RAGE_LOG_FMT_DEFAULT

/****************************************
		rlNpGetActivityFeedVideoDataWorkItem
****************************************/
rlNpGetActivityFeedVideoDataDetails::rlNpGetActivityFeedVideoDataDetails()
	: m_noofRecordedVideos(0)
{
	Reset();

	// might have to stick a couple of zeros on the end to match 3339 format
	sceRtcParseRFC3339(&m_dateToCheckTo, "2014-01-01T09:50:00Z");
}

void rlNpGetActivityFeedVideoDataDetails::SetDateToCheckTo(const char* time)
{
	sceRtcParseRFC3339(&m_dateToCheckTo, time);
}

bool rlNpGetActivityFeedVideoDataDetails::IsNewerThanCheckDate(const char* time)
{
	SceRtcTick timePassedIn;
	sceRtcParseRFC3339(&timePassedIn, time);

	return (timePassedIn.tick > m_dateToCheckTo.tick);
}

void rlNpGetActivityFeedVideoDataDetails::Reset()
{
	m_noofRecordedVideos = 0;
}

bool rlNpGetActivityFeedVideoDataDetails::AddVideoID(const char* videoID)
{
	if (m_noofRecordedVideos >= maxNoofRecordedVideos)
		return false;

	safecpy(m_recordedVideos[m_noofRecordedVideos].m_videoId, videoID);

	return true;
}

bool rlNpGetActivityFeedVideoDataDetails::AddTitleID(const char* titleID)
{
	if (m_noofRecordedVideos >= maxNoofRecordedVideos)
		return false;

	if (m_recordedVideos[m_noofRecordedVideos].m_noofTitleIds >= maxNoofTitleIds)
		return false;

	int currentTitleId = m_recordedVideos[m_noofRecordedVideos].m_noofTitleIds;
	safecpy(m_recordedVideos[m_noofRecordedVideos].m_titleId[currentTitleId], titleID);

	++m_recordedVideos[m_noofRecordedVideos].m_noofTitleIds;

	return true;
}

bool rlNpGetActivityFeedVideoDataDetails::AddDate(const char* date)
{
	if (m_noofRecordedVideos >= maxNoofRecordedVideos)
		return false;

	safecpy(m_recordedVideos[m_noofRecordedVideos].m_date, date);

	return true;
}

bool rlNpGetActivityFeedVideoDataDetails::FinaliseEntry()
{
	++m_noofRecordedVideos;

	if (m_noofRecordedVideos >= maxNoofRecordedVideos)
		return false;

	m_recordedVideos[m_noofRecordedVideos].m_noofTitleIds = 0;

	return true;
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "[%s] " fmt, GetWorkItemName()

bool rlNpGetActivityFeedVideoDataWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetActivityFeedVideoDataDetails* details)
{
	m_Identifier = GET_ACTIVITYFEED_USER;
	m_Details = details;
	return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpGetActivityFeedVideoDataWorkItem::GetApiGroup()
{
	return API_GROUP_ACTIVITYFEED;
}

void rlNpGetActivityFeedVideoDataWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/users/%" I64FMT "u/feed/0?sharedMedia=true&maxStories=%d", WEBAPI_VERSION, accountId, rlNpGetActivityFeedVideoDataDetails::maxNoofStories);
}

bool rlNpGetActivityFeedVideoDataWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		RsonReader rrFeedList;
		rverify(rr.GetMember("feed", &rrFeedList), catchall, );

		RsonReader rrFeedPost;
		rverify(rrFeedList.GetFirstMember(&rrFeedPost),  catchall, );

		// go through each post getting youtube video data out of it
		const int maxNoofStories = rlNpGetActivityFeedVideoDataDetails::maxNoofStories;
		bool done = false;
		const int MAX_LENGTH_OF_STRING = 512;
		m_Details->Reset();
		for(int storyIndex = 0; storyIndex < maxNoofStories && !done; ++storyIndex, done = !rrFeedPost.GetNextSibling(&rrFeedPost))
		{
			if (rrFeedPost.HasMember("targets") && rrFeedPost.HasMember("date"))
			{
				// feed posts are done in order of newest first, so if this timestamp is old, we can just bail
				char dateString[MAX_LENGTH_OF_STRING];
				rrFeedPost.GetValue("date", dateString);
				if (!m_Details->IsNewerThanCheckDate(dateString))
				{
					return true;
				}
				
				// the important video data is stored in metadata in "targets"
				// it's a bit awkward to read through as it's in an array with lines formatted like
				// "meta" "the value", "type" "what it is" ...rather than standard "what it is": "the value" :/ 
				RsonReader rrTarget;
				rverify(rrFeedPost.GetMember("targets", &rrTarget), catchall, );

				const int maxNoofMeta = rlNpGetActivityFeedVideoDataDetails::maxNoofMeta;
				typedef char MetaTypeDef[MAX_LENGTH_OF_STRING];
				
				MetaTypeDef targetArray[maxNoofMeta];
				int numRead = rrTarget.AsArray(targetArray, maxNoofMeta);

				bool isYoutube = false;
				if (numRead)
				{
					char metaValueString[maxNoofMeta][MAX_LENGTH_OF_STRING];
					char typeValueString[maxNoofMeta][MAX_LENGTH_OF_STRING];

					// store off all the values, and check to see if this is a youtube post while we're at it
					// seems inefficient, but the order these are in are usually inefficient and not guaranteed either
					for (int index = 0; index < numRead; ++index)
					{
						int metaStringSize = strlen(targetArray[index]);
						RsonReader rrMeta(targetArray[index], metaStringSize);
						rrMeta.GetValue("meta", metaValueString[index], MAX_LENGTH_OF_STRING);
						rrMeta.GetValue("type", typeValueString[index], MAX_LENGTH_OF_STRING);

						if (strcmp(metaValueString[index], "youtube") == 0 && strcmp(typeValueString[index], "SN_TYPE") == 0)
						{
							isYoutube = true;
						}
					}

					// if it's a youtube post, save out important data to details
					if (isYoutube)
					{
						for (int index = 0; index < numRead; ++index)
						{
							if (strcmp(typeValueString[index], "VIDEO_ID") == 0)
							{
								m_Details->AddVideoID(metaValueString[index]);
							}
							// can be several title Ids
							else if (strcmp(typeValueString[index], "TITLE_ID") == 0)
							{
								m_Details->AddTitleID(typeValueString[index]);
							}
						}

						m_Details->FinaliseEntry();
					}
				}
			}
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

/****************************************
rlNpIdentityMapperGetAccountIdWorkItem
****************************************/
bool rlNpIdentityMapperGetAccountIdWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
													   const rlSceNpOnlineId& targetOnlineId, rlSceNpAccountId* targetAccountId)
{
	rtry
	{
		rverify(targetAccountId, catchall, );

		m_Identifier = GET_ACCOUNTID;
		m_TargetOnlineId = targetOnlineId;
		m_TargetAccountId = targetAccountId;

		*m_TargetAccountId = RL_INVALID_NP_ACCOUNT_ID;

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

bool rlNpIdentityMapperGetAccountIdWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		RsonReader rrAccountId;
		rverifyall(rr.GetMember("accountId", &rrAccountId));
		rverifyall(rrAccountId.AsUns64(*m_TargetAccountId));

		return true;
	}
	rcatchall
	{
		*m_TargetAccountId = RL_INVALID_NP_ACCOUNT_ID;
		return false;
	}
}

const char* rlNpIdentityMapperGetAccountIdWorkItem::GetApiGroup()
{
	return API_GROUP_IDENTITY_MAPPER;
}

void rlNpIdentityMapperGetAccountIdWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/accounts/map/onlineId2accountId/%s", WEBAPI_IDENTITY_MAPPER_VERSION, m_TargetOnlineId.data);
}

/****************************************
rlNpIdentityMapperGetAccountIdsWorkItem
****************************************/
bool rlNpIdentityMapperGetAccountIdsWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpOnlineId* targetOnlineIds, const unsigned numIds, rlSceNpAccountId* out_accountIds)
{
	rtry
	{
		rverifyall(targetOnlineIds);
		rverifyall(numIds > 0);
		rverifyall(numIds > NP_WEB_API_MAX_MAPPED_IDS);
		rverifyall(out_accountIds);

		m_Identifier = GET_ACCOUNTIDS;
		m_NumIds = numIds;
		m_TargetAccountIds = out_accountIds;

		for(unsigned i = 0; i < numIds; i++)
		{
			m_TargetAccountIds[i] = RL_INVALID_NP_ACCOUNT_ID;
		}

		m_ContentWriter.Init(m_ContentBuffer, MAPPER_MAX_LENGTH, RSON_FORMAT_JSON);
		m_ContentWriter.BeginArray(nullptr, nullptr);
		for (int i = 0; i < numIds; i++)
		{
			m_ContentWriter.WriteString(nullptr, targetOnlineIds[i].data);
		}
		m_ContentWriter.End();

		m_bAddContent = true;
		memset(&m_Content, 0, sizeof(m_Content));
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;
		m_Content.contentLength = m_ContentWriter.Length();

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

bool rlNpIdentityMapperGetAccountIdsWorkItem::ProcessSuccess()
{
	rtry
	{
		unsigned numRecords = 0; 

		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		// first list entry
		RsonReader idRecord;
		rverifyall(rr.GetFirstMember(&idRecord));

		do 
		{
			RsonReader rrAccountId;
			rverifyall(rr.GetMember("accountId", &rrAccountId));
			rverifyall(rrAccountId.AsUns64(m_TargetAccountIds[numRecords]));
			numRecords++;
		}
		while(idRecord.GetNextSibling(&idRecord));

		rverifyall(numRecords == m_NumIds);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpIdentityMapperGetAccountIdsWorkItem::GetApiGroup()
{
	return API_GROUP_IDENTITY_MAPPER;
}

void rlNpIdentityMapperGetAccountIdsWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_POST;
	formatf(m_ApiPath, "/%s/accounts/map/onlineId2accountId", WEBAPI_IDENTITY_MAPPER_VERSION);
}

/****************************************
rlNpIdentityMapperGetOnlineIdWorkItem
****************************************/
bool rlNpIdentityMapperGetOnlineIdWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
													  const rlSceNpAccountId targetAccountId, rlSceNpOnlineId* targetOnlineId)
{
	rtry
	{
		rverify(targetOnlineId, catchall, );

		m_Identifier = GET_ONLINEID;
		m_TargetAccountId = targetAccountId;
		m_TargetOnlineId = targetOnlineId;

		targetOnlineId->Clear();

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

bool rlNpIdentityMapperGetOnlineIdWorkItem::ProcessSuccess()
{
	rtry
	{
		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

        // use buffer with RL_MAX_NAME_BUF_SIZE to read (includes room for terminating null which AsString will set)
        char szOnlineId[RL_MAX_NAME_BUF_SIZE];

		RsonReader rrOnlineId;
		rverifyall(rr.GetMember("onlineId", &rrOnlineId));
		rverifyall(rrOnlineId.AsString(szOnlineId));

        unsigned onlineIdLength = static_cast<unsigned>(strlen(szOnlineId));
        rverifyall(onlineIdLength <= SCE_NP_ONLINEID_MAX_LENGTH);

        memcpy(m_TargetOnlineId->data, szOnlineId, onlineIdLength);

		return true;
	}
	rcatchall
	{
		m_TargetOnlineId->Clear();
		return false;
	}
}

const char* rlNpIdentityMapperGetOnlineIdWorkItem::GetApiGroup()
{
	return API_GROUP_IDENTITY_MAPPER;
}

void rlNpIdentityMapperGetOnlineIdWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/accounts/map/accountId2onlineId/%" I64FMT "u", WEBAPI_IDENTITY_MAPPER_VERSION, m_TargetAccountId);
}

/****************************************
rlNpIdentityMapperGetOnlineIdsWorkItem
****************************************/
bool rlNpIdentityMapperGetOnlineIdsWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpAccountId* targetAccountIds, const unsigned numIds, rlSceNpOnlineId* out_onlineIds)
{
	rtry
	{
		rverifyall(targetAccountIds);
		rverifyall(numIds > 0);
		rverifyall(numIds > NP_WEB_API_MAX_MAPPED_IDS);
		rverifyall(out_onlineIds);

		m_Identifier = GET_ONLINEIDS;
		m_NumIds = numIds;
		m_TargetOnlineIds = out_onlineIds;

		for(unsigned i = 0; i < numIds; i++)
		{
			m_TargetOnlineIds[i].Clear();
		}

		m_ContentWriter.Init(m_ContentBuffer, MAPPER_MAX_LENGTH, RSON_FORMAT_JSON);
		m_ContentWriter.BeginArray(nullptr, nullptr);
		for (int i = 0; i < numIds; i++)
		{
			m_ContentWriter.WriteUns64(nullptr, targetAccountIds[i]);
		}
		m_ContentWriter.End();

		m_bAddContent = true;
		memset(&m_Content, 0, sizeof(m_Content));
		m_Content.pContentType = SCE_NP_WEBAPI_CONTENT_TYPE_APPLICATION_JSON_UTF8;
		m_Content.contentLength = m_ContentWriter.Length();

		return rlNpWebApiWorkItem::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
		return false;
	}
}

bool rlNpIdentityMapperGetOnlineIdsWorkItem::ProcessSuccess()
{
	rtry
	{
		unsigned numRecords = 0; 

		rverify(m_ReadSize > 0, catchall, );
		RsonReader rr(GetBuffer(), m_ReadSize);

		// first list entry
		RsonReader idRecord;
		rverifyall(rr.GetFirstMember(&idRecord));

		do 
		{
			RsonReader rrOnlineId;
			rverifyall(rr.GetMember("onlineId", &rrOnlineId));
			rverifyall(rrOnlineId.AsString(m_TargetOnlineIds[numRecords].data));
			numRecords++;
		}
		while(idRecord.GetNextSibling(&idRecord));

		rverifyall(numRecords == m_NumIds);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* rlNpIdentityMapperGetOnlineIdsWorkItem::GetApiGroup()
{
	return API_GROUP_IDENTITY_MAPPER;
}

void rlNpIdentityMapperGetOnlineIdsWorkItem::SetupApiPath(rlSceNpAccountId UNUSED_PARAM(accountId))
{
	m_HttpMethod = SCE_NP_WEBAPI_HTTP_METHOD_GET;
	formatf(m_ApiPath, "/%s/accounts/map/accountId2onlineId", WEBAPI_IDENTITY_MAPPER_VERSION);
}
#endif //RL_NP_LEGACY

#if RL_NP_CPPWEBAPI
/****************************************
rlNpCheckProfanityWorkItem
****************************************/
bool rlNpCheckProfanityWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const sysLanguage language,
											const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord)
{
	rtry
	{
		rverify(text != nullptr, catchall, );
		rverify(profaneWord != nullptr, catchall, );
		rverify(numProfaneWord != nullptr, catchall, );
		rverify(profaneWordBufferSize > 0, catchall, );
		rverify(strlen(text) < PROFANITY_BUFFER_MAX_LEN, catchall, rlError("Text too long [%s]", text));

		safecpy(m_Text, text);
		m_ProfaneWord = profaneWord;
		m_ProfaneWordBufferSize = profaneWordBufferSize;
		m_NumProfaneWords = numProfaneWord;
		m_Language = language;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpCheckProfanityWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::ProfanityFilter::V2;

	Common::Transaction<Common::IntrusivePtr<FilterProfanityResponse>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		/*E Initialize the Input Parameters. */
		Common::IntrusivePtr<WebApiFilterRequest> pFilterRequest;
		ret = WebApiFilterRequestFactory::create(m_LibCtx, m_Text, &pFilterRequest);
		rverify(ret == SCE_OK, catchall, rlError("WebApiFilterRequestFactory create failed. ret = 0x%x", ret));

		const sysLanguage lang = m_Language == LANGUAGE_UNDEFINED ? g_SysService.GetSystemLanguage() : m_Language;
		const char* locale = GetNpLanguageCode(lang);
		rverify(locale != nullptr, catchall, );

		ProfanityApi::ParameterToFilterProfanity param;
		ret = param.initialize(m_LibCtx, locale, SCE_NP_DEFAULT_SERVICE_LABEL, pFilterRequest);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToFilterProfanity init failed. ret = 0x%x", ret));

		ret = ProfanityApi::filterProfanity(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("filterProfanity failed. ret = 0x%x", ret));

		Common::IntrusivePtr<FilterProfanityResponse> respPtr;
		ret = transaction.getResponse(respPtr);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));
		rverify(respPtr, catchall, rlError("The response is null"));

		const char* response = respPtr->getMessage().c_str();
		rlDebug1("Profanity response: %s", response);

		rverify(rlNpProfanityCheck::CompareResult(m_Text, response, m_ProfaneWord, m_ProfaneWordBufferSize, m_NumProfaneWords), catchall, );

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpCommunicationRestrictionWorkItem
****************************************/
bool rlNpCommunicationRestrictionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, bool* restricted)
{
	rtry
	{
		rverify(restricted != nullptr, catchall, );
		m_Restricted = restricted;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpCommunicationRestrictionWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::CommunicationRestrictionStatus::V3;

	Common::Transaction<Common::IntrusivePtr<CommunicationRestrictionStatusResponse>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		CommunicationRestrictionStatusApi::ParameterToGetCommunicationRestrictionStatus param;
		ret = param.initialize(m_LibCtx, m_AccountId);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToGetCommunicationRestrictionStatus init failed. ret = 0x%x", ret));

		ret = CommunicationRestrictionStatusApi::getCommunicationRestrictionStatus(m_WebApiUserCtxId, param, transaction);
		rcheck(ret == SCE_OK, catchall, rlError("getCommunicationRestrictionStatus failed. ret = 0x%x", ret));

		Common::IntrusivePtr<CommunicationRestrictionStatusResponse> respPtr;
		ret = transaction.getResponse(respPtr);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));
		rverify(respPtr, catchall, rlError("The response is null"));

		*m_Restricted = respPtr->getRestricted();

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpCreatePlayerSessionWorkItem
****************************************/
bool rlNpCreatePlayerSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, 
												const rlNpWebApiPostSessionParams& sessionParams, const SceNpWebApi2PushEventPushContextId& pushContextId)
{
	rtry
	{
		const char* name = sessionParams.GetSessionName(LANGUAGE_FIRST);
		rverify(sessionParams.GetSessionId() != nullptr, catchall, );
		rverify(name != nullptr && name[0] != 0, catchall, rlError("At least the default language must be set"));
		m_SessionParams = sessionParams;

		rverify(pushContextId.uuid[0] != 0, catchall, );
		m_PushContextId = pushContextId;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpCreatePlayerSessionWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<PostPlayerSessionsResponseBody>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::Vector< Common::IntrusivePtr<RequestCreatePlayerSessionPlayer> > requestCreatePlayerSessionPlayers(m_LibCtx);
		{
			Common::IntrusivePtr<PlayerSessionPushContext> playerSessionPushContextPtr;
			ret = PlayerSessionPushContextFactory::create(m_LibCtx, m_PushContextId.uuid, &playerSessionPushContextPtr);
			rverify(ret == SCE_OK, catchall, rlError("PlayerSessionPushContextFactory failed. ret = 0x%x", ret));

			Common::Vector< Common::IntrusivePtr<PlayerSessionPushContext> > playerSessionPushContexts(m_LibCtx);
			ret = playerSessionPushContexts.pushBack(playerSessionPushContextPtr);
			rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));

			Common::IntrusivePtr<RequestCreatePlayerSessionPlayer> requestCreatePlayerSessionPlayerPtr;

			ret = RequestCreatePlayerSessionPlayerFactory::create(
				m_LibCtx, "me", NP_WEB_API_PLATFORM, playerSessionPushContexts, &requestCreatePlayerSessionPlayerPtr);
			rverify(ret == SCE_OK, catchall, rlError("RequestCreatePlayerSessionPlayerFactory failed. ret = 0x%x", ret));

			ret = requestCreatePlayerSessionPlayerPtr->setPushContexts(playerSessionPushContexts);
			rverify(ret == SCE_OK, catchall, rlError("setPushContexts failed. ret = 0x%x", ret));

			//ret = requestCreatePlayerSessionPlayerPtr->setCustomData1(m_SessionParams.GetChangeableData(), istrlen(m_SessionParams.GetChangeableData()));
			//rverify(ret == SCE_OK, catchall, rlError("setCustomData1 failed. ret = 0x%x", ret));

			ret = requestCreatePlayerSessionPlayers.pushBack(requestCreatePlayerSessionPlayerPtr);
			rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));
		}

		Common::IntrusivePtr< RequestPlayerSessionMemberPlayer > requestPlayerSessionMemberPlayerPtr;

		ret = RequestPlayerSessionMemberPlayerFactory::create(m_LibCtx,
			requestCreatePlayerSessionPlayers, &requestPlayerSessionMemberPlayerPtr);
		rverify(ret == SCE_OK, catchall, );

		Common::Vector< Common::String > playerSessionSupportedPlatforms(m_LibCtx);
		ret = AddStringToVector(m_LibCtx, NP_WEB_API_PLATFORM_PROSPERO, playerSessionSupportedPlatforms);
		rverify(ret == SCE_OK, catchall, );
		ret = AddStringToVector(m_LibCtx, NP_WEB_API_PLATFORM_PS4, playerSessionSupportedPlatforms);
		rverify(ret == SCE_OK, catchall, );
		
		sce::Json::Object localizedText;

		for (int i = 0; i < MAX_LANGUAGES; ++i)
		{
			const char* locale = GetNpLanguageCode((sysLanguage)i);
			const char* sessionName = m_SessionParams.GetSessionName((sysLanguage)i);

			if (locale != nullptr && locale[0] != 0 && sessionName != nullptr && sessionName[0] != 0)
			{
				localizedText[locale] = sce::Json::String(sessionName);
			}
		}

		const char* defaultLanguage = GetNpLanguageCode(LANGUAGE_FIRST);

		Common::IntrusivePtr<LocalizedString> localizedStringPtr;
		ret = LocalizedStringFactory::create(m_LibCtx, defaultLanguage, localizedText, &localizedStringPtr);
		rverify(ret == SCE_OK, catchall, rlError("LocalizedStringFactory failed. ret = 0x%x", ret));

		Common::IntrusivePtr<RequestPlayerSession> requestPlayerSessionPtr;
		ret = RequestPlayerSessionFactory::create(m_LibCtx,
			m_SessionParams.GetMaxSlots(), requestPlayerSessionMemberPlayerPtr,
			playerSessionSupportedPlatforms, localizedStringPtr, &requestPlayerSessionPtr);
		rverify(ret == SCE_OK, catchall, rlError("RequestPlayerSessionFactory failed. ret = 0x%x", ret));

		requestPlayerSessionPtr->setMaxSpectators(0);

		// Users who can join Player Sessions without invitations
		JoinableUserType joinableUserType = JoinableUserType::ANYONE;
		requestPlayerSessionPtr->setJoinableUserType(joinableUserType);

		// Information about who can send invitations to a Player Session
		InvitableUserType invitableUserType = InvitableUserType::MEMBER;
		requestPlayerSessionPtr->setInvitableUserType(invitableUserType);

		ret = requestPlayerSessionPtr->setCustomData1(m_SessionParams.GetChangeableData(), istrlen(m_SessionParams.GetChangeableData()));
		rverify(ret == SCE_OK, catchall, rlError("setCustomData1 failed. ret = 0x%x", ret));

		Common::Vector< Common::String > leaderPrivileges(m_LibCtx);
		
		ret = AddStringToVector(m_LibCtx, "KICK", leaderPrivileges);
		rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));
		
		ret = AddStringToVector(m_LibCtx, "UPDATE_JOINABLE_USER_TYPE", leaderPrivileges);
		rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

		ret = requestPlayerSessionPtr->setLeaderPrivileges(leaderPrivileges);
		rverify(ret == SCE_OK, catchall, rlError("setLeaderPrivileges failed. ret = 0x%x", ret));

		Common::Vector< Common::IntrusivePtr<RequestPlayerSession> > requestPlayerSessions(m_LibCtx);
		ret = requestPlayerSessions.pushBack(requestPlayerSessionPtr);
		rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));

		Common::IntrusivePtr<PostPlayerSessionsRequestBody> postPlayerSessionsRequestBodyPtr;
		ret = PostPlayerSessionsRequestBodyFactory::create(m_LibCtx, requestPlayerSessions, &postPlayerSessionsRequestBodyPtr);
		rverify(ret == SCE_OK, catchall, rlError("PostPlayerSessionsRequestBodyFactory failed. ret = 0x%x", ret));

		PlayerSessionsApi::ParameterToCreatePlayerSessions param;
		ret = param.initialize(m_LibCtx, postPlayerSessionsRequestBodyPtr);
		rverify(ret == SCE_OK, catchall, rlError("initialize failed. ret = 0x%x", ret));

		ret = PlayerSessionsApi::createPlayerSessions(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("createPlayerSessions failed. ret = 0x%x", ret));

		Common::IntrusivePtr<PostPlayerSessionsResponseBody> respPtr;
		ret = transaction.getResponse(respPtr);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		Common::IntrusivePtr<Common::Vector<Common::IntrusivePtr<PlayerSession>>> sessions = respPtr->getPlayerSessions();
		rverify(sessions, catchall, );
		rverify(sessions->size() == 1, catchall, );

		Common::IntrusivePtr<PlayerSession> session = (*sessions)[0];
		m_SessionParams.GetSessionId()->Reset(session->getSessionId().c_str());

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpJoinPlayerSessionAsPlayerWorkItem
****************************************/
bool rlNpJoinPlayerSessionAsPlayerWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
														const rlSceNpSessionId& sessionId, const SceNpWebApi2PushEventPushContextId& pushContextId)
{
	rtry
	{
		rverify(sessionId.IsValid(), catchall,);
		m_SessionId = sessionId;

		rverify(pushContextId.uuid[0] != 0, catchall, );
		m_PushContextId = pushContextId;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
		rcatchall
	{
	}

	return false;
}

void rlNpJoinPlayerSessionAsPlayerWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<PostPlayerSessionsSessionIdMemberPlayersResponseBody>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::Vector< Common::IntrusivePtr<RequestPlayerSessionPlayer> > requestPlayerSessionPlayers(m_LibCtx);
		{
			Common::IntrusivePtr<PlayerSessionPushContext> playerSessionPushContextPtr;
			ret = PlayerSessionPushContextFactory::create(m_LibCtx, m_PushContextId.uuid, &playerSessionPushContextPtr);
			rverify(ret == SCE_OK, catchall, rlError("PlayerSessionPushContextFactory failed. ret = 0x%x", ret));

			Common::Vector< Common::IntrusivePtr<PlayerSessionPushContext> > playerSessionPushContexts(m_LibCtx);
			ret = playerSessionPushContexts.pushBack(playerSessionPushContextPtr);
			rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));

			Common::IntrusivePtr<RequestPlayerSessionPlayer> requestPlayerSessionPlayerPtr;

			ret = RequestPlayerSessionPlayerFactory::create(m_LibCtx, "me", NP_WEB_API_PLATFORM, &requestPlayerSessionPlayerPtr);
			rverify(ret == SCE_OK, catchall, rlError("RequestPlayerSessionPlayerFactory failed. ret = 0x%x", ret));

			ret = requestPlayerSessionPlayerPtr->setPushContexts(playerSessionPushContexts);
			rverify(ret == SCE_OK, catchall, rlError("setPushContexts failed. ret = 0x%x", ret));

			ret = requestPlayerSessionPlayers.pushBack(requestPlayerSessionPlayerPtr);
			rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));
		}
		
		Common::IntrusivePtr<PostPlayerSessionsSessionIdMemberPlayersRequestBody> requestBody;
		ret = PostPlayerSessionsSessionIdMemberPlayersRequestBodyFactory::create(m_LibCtx, requestPlayerSessionPlayers, &requestBody);
		rverify(ret == SCE_OK, catchall, rlError("PostPlayerSessionsSessionIdMemberPlayersRequestBodyFactory failed. ret = 0x%x", ret));

		PlayerSessionsApi::ParameterToJoinPlayerSessionAsPlayer param;
		ret = param.initialize(m_LibCtx, m_SessionId.GetData(), requestBody);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToJoinPlayerSessionAsPlayer failed. ret = 0x%x", ret));

		ret = PlayerSessionsApi::joinPlayerSessionAsPlayer(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("joinPlayerSessionAsPlayer failed. ret = 0x%x", ret));

		Common::IntrusivePtr<PostPlayerSessionsSessionIdMemberPlayersResponseBody> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpJoinPlayerSessionAsSpectatorWorkItem
****************************************/
bool rlNpJoinPlayerSessionAsSpectatorWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
														const rlSceNpSessionId& sessionId, const SceNpWebApi2PushEventPushContextId& pushContextId)
{
	rtry
	{
		rverify(sessionId.IsValid(), catchall,);
		m_SessionId = sessionId;

		rverify(pushContextId.uuid[0] != 0, catchall, );
		m_PushContextId = pushContextId;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
		rcatchall
	{
	}

	return false;
}

void rlNpJoinPlayerSessionAsSpectatorWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<PostPlayerSessionsSessionIdMemberSpectatorsResponseBody>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::Vector< Common::IntrusivePtr<RequestPlayerSessionSpectator> > requestPlayerSessionPlayers(m_LibCtx);
		{
			Common::IntrusivePtr<PlayerSessionPushContext> playerSessionPushContextPtr;
			ret = PlayerSessionPushContextFactory::create(m_LibCtx, m_PushContextId.uuid, &playerSessionPushContextPtr);
			rverify(ret == SCE_OK, catchall, rlError("PlayerSessionPushContextFactory failed. ret = 0x%x", ret));

			Common::Vector< Common::IntrusivePtr<PlayerSessionPushContext> > playerSessionPushContexts(m_LibCtx);
			ret = playerSessionPushContexts.pushBack(playerSessionPushContextPtr);
			rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));

			Common::IntrusivePtr<RequestPlayerSessionSpectator> requestPlayerSessionPlayerPtr;

			ret = RequestPlayerSessionSpectatorFactory::create(m_LibCtx, "me", NP_WEB_API_PLATFORM, &requestPlayerSessionPlayerPtr);
			rverify(ret == SCE_OK, catchall, rlError("RequestPlayerSessionPlayerFactory failed. ret = 0x%x", ret));

			ret = requestPlayerSessionPlayerPtr->setPushContexts(playerSessionPushContexts);
			rverify(ret == SCE_OK, catchall, rlError("setPushContexts failed. ret = 0x%x", ret));

			ret = requestPlayerSessionPlayers.pushBack(requestPlayerSessionPlayerPtr);
			rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));
		}
		
		Common::IntrusivePtr<PostPlayerSessionsSessionIdMemberSpectatorsRequestBody> requestBody;
		ret = PostPlayerSessionsSessionIdMemberSpectatorsRequestBodyFactory::create(m_LibCtx, requestPlayerSessionPlayers, &requestBody);
		rverify(ret == SCE_OK, catchall, rlError("PostPlayerSessionsSessionIdMemberPlayersRequestBodyFactory failed. ret = 0x%x", ret));

		PlayerSessionsApi::ParameterToJoinPlayerSessionAsSpectator param;
		ret = param.initialize(m_LibCtx, m_SessionId.GetData(), requestBody);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToJoinPlayerSessionAsSpectator failed. ret = 0x%x", ret));

		ret = PlayerSessionsApi::joinPlayerSessionAsSpectator(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("joinPlayerSessionAsSpectator failed. ret = 0x%x", ret));

		Common::IntrusivePtr<PostPlayerSessionsSessionIdMemberSpectatorsResponseBody> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpLeavePlayerSessionWorkItem
****************************************/
bool rlNpLeavePlayerSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId)
{
	rtry
	{
		rverify(sessionId.IsValid(), catchall, );
		m_SessionId = sessionId;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpLeavePlayerSessionWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::DefaultResponse> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		PlayerSessionsApi::ParameterToLeavePlayerSession param;
		ret = param.initialize(m_LibCtx, m_SessionId.GetData(), "me");
		rverify(ret == SCE_OK, catchall, rlError("ParameterToLeavePlayerSession failed. ret = 0x%x", ret));

		ret = PlayerSessionsApi::leavePlayerSession(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("leavePlayerSession failed. ret = 0x%x", ret));

		Common::DefaultResponse response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpGetPlayerSessionWorkItem
****************************************/
rlNpGetPlayerSessionWorkItem::rlNpGetPlayerSessionWorkItem()
	: m_SessionDetails(nullptr)
	, m_SessionInfo(nullptr)
	, m_NumSessions(0)
	, m_FailOnMissing(false)
{

}

bool rlNpGetPlayerSessionWorkItem::Configure(
	const int localGamerIndex, 
	rlSceNpAccountId accountId, 
	int webApiUserCtxId, 
	const rlSceNpSessionId* sessionIds,
	rlNpWebApiSessionDetail* outDetails, 
	const unsigned numSessions, 
	const bool failOnMissing)
{
	rtry
	{
		rverify(numSessions > 0, catchall, );
		rverify(numSessions <= MAX_REQUESTABLE_SESSIONS, catchall, rlError("Too many sessions: %u", numSessions));
		m_NumSessions = numSessions;

		for (unsigned i = 0; i < numSessions; ++i)
		{
			rverify(sessionIds[i].IsValid(), catchall, );
			m_SessionId[i] = sessionIds[i];
		}

		rverify(outDetails != nullptr, catchall, );
		m_SessionDetails = outDetails;

		m_FailOnMissing = failOnMissing;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

bool rlNpGetPlayerSessionWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, rlSessionInfo* outInfo)
{
	rtry
	{
		rverify(sessionId.IsValid(), catchall, );
		m_SessionId[0] = sessionId;
		m_NumSessions = 1;

		rverify(outInfo != nullptr, catchall, );
		m_SessionInfo = outInfo;

		m_FailOnMissing = true;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpGetPlayerSessionWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<GetPlayerSessionsResponseBody>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		char sessionIds[SESSION_ID_BUFFER_SIZE] = { 0 };
		char buffer[rlSceNpSessionId::MAX_DATA_BUF_SIZE];
		bool first = true;

		rlSceNpSessionId ids[MAX_REQUESTABLE_SESSIONS];
		unsigned uniqueIds = 0;

		// It's a small list so this should be fine
		for (unsigned i = 0; i < m_NumSessions; ++i)
		{
			bool found = false;

			for (unsigned j = 0; j < uniqueIds; ++j)
			{
				if (ids[j] == m_SessionId[i])
				{
					found = true;
					break;
				}
			}

			if (!found)
			{
				ids[uniqueIds] = m_SessionId[i];
				++uniqueIds;
			}
		}

		for (unsigned i = 0; i < uniqueIds; ++i)
		{
			formatf(buffer, "%s%s", first ? "" : ",", ids[i].data);
			safecat(sessionIds, buffer);

			first = false;
		}

		rlDebug("Requesting :: NumSessions: %u, NumUniqueIds: %u", m_NumSessions, uniqueIds);

		PlayerSessionsApi::ParameterToGetPlayerSessions param;
		ret = param.initialize(m_LibCtx, sessionIds);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToGetPlayerSessions failed. ret = 0x%x", ret));

		Common::Vector< Common::String > fields(m_LibCtx);
		ret = AddStringToVector(m_LibCtx, "sessionId", fields);
		rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

		ret = AddStringToVector(m_LibCtx, "customData1", fields);
		rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

		if (m_SessionDetails != nullptr)
		{
			ret = AddStringToVector(m_LibCtx, "sessionName", fields);
			rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			ret = AddStringToVector(m_LibCtx, "createdTimestamp", fields);
			rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			ret = AddStringToVector(m_LibCtx, "maxPlayers", fields);
			rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			ret = AddStringToVector(m_LibCtx, "maxSpectators", fields);
			rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			ret = AddStringToVector(m_LibCtx, "member", fields);
			rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			//ret = AddStringToVector(m_LibCtx, "member(players(customData1))", fields);
			//rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			//ret = AddStringToVector(m_LibCtx, "member(spectators(customData1))", fields);
			//rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			ret = AddStringToVector(m_LibCtx, "supportedPlatforms", fields);
			rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));

			ret = AddStringToVector(m_LibCtx, "leader", fields);
			rverify(ret == SCE_OK, catchall, rlError("AddStringToVector failed. ret = 0x%x", ret));
		}

		Common::IntrusivePtr< Common::Vector< Common::String > > fieldsPtr(&fields, m_LibCtx);
		param.setfields(fieldsPtr);

		ret = PlayerSessionsApi::getPlayerSessions(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("getPlayerSessions failed. ret = 0x%x", ret));

		Common::IntrusivePtr<GetPlayerSessionsResponseBody> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		Common::IntrusivePtr<Common::Vector<Common::IntrusivePtr<PlayerSessionForRead>>> sessions = response->getPlayerSessions();
		rverify(sessions, catchall,);

		const unsigned sessionsCount = static_cast<unsigned>(sessions->size());
		rverify(sessionsCount <= m_NumSessions, catchall,);

		if (m_FailOnMissing)
		{
			// This can mismatch if a session has just been deleted so don't assert
			rcheck(sessionsCount == uniqueIds, catchall, rlError("Received %u results, Requested: %u - Exiting...", sessionsCount, uniqueIds));
		}
		else
		{
			// just log it otherwise
			rlDebug("Received %u results, Requested: %u", sessionsCount, uniqueIds);
		}

		// loop round all results...
		for (unsigned i = 0; i < sessionsCount; ++i)
		{
			Common::IntrusivePtr<PlayerSessionForRead> session = (*sessions)[i];
			rverify(session, catchall, );
			rlSceNpSessionId sessionId;
			sessionId.Reset(session->getSessionId().c_str());

			rverify(session->customData1IsSet(), catchall, );
			const char* data = (const char*)(session->getCustomData1()->getBinary());
			unsigned dataSize = static_cast<unsigned>(session->getCustomData1()->size());

			// copy out the custom info 
			rlSessionInfo sessionInfo; 
			rverify(sessionInfo.FromString(data, &dataSize), catchall, );
			sessionInfo.SetWebApiSessionId(sessionId);

			// log results
			rlDebug("Session [%d] :: SessionId: %s, Token: 0x%016" I64FMT "x",
				i,
				sessionId.data,
				sessionInfo.GetToken().m_Value);

			// check if this result has a sessionId matching an entry in our requested list
			for (unsigned ss = 0; ss < m_NumSessions; ++ss)
			{
				if (m_SessionDetails != nullptr && m_SessionId[ss] == sessionId)
				{
					rlDebug2("\tAssigned to SessionIndex: %d", ss);
						
					rlNpWebApiSessionDetail& details = m_SessionDetails[ss];

					safecpy(details.sessionName, session->getSessionName().c_str());
					details.sessionMaxUser = session->getMaxPlayers();
					details.joinableFlag = !session->joinDisabledIsSet();
					details.info = sessionInfo;
					details.sessionId = sessionId;
				}

				if (m_SessionInfo != nullptr && m_SessionId[ss] == sessionId)
				{
					rlDebug2("\tAssigned to SessionIndex: %d", ss);
					m_SessionInfo[ss] = sessionInfo;
				}
			}
		}

#if !__NO_OUTPUT
		// log missing session Ids and invalid session Info
		if(m_SessionDetails != nullptr) for (unsigned i = 0; i < m_NumSessions; ++i)
		{
			if(!m_SessionDetails[i].sessionId.IsValid())
			{
				rlDebug("RequestedSession [%d] :: Not found - SessionId: %s", i, m_SessionId[i].data);
			}
			else if(!m_SessionDetails[i].info.IsValid())
			{
				rlDebug("RequestedSession [%d] :: Invalid Token - SessionId: %s", i, m_SessionDetails[i].sessionId.data);
			}
		}
#endif

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpGetFriendsPlayerSessionWorkItem
****************************************/
rlNpGetFriendsPlayerSessionsWorkItem::rlNpGetFriendsPlayerSessionsWorkItem()
{
}

bool rlNpGetFriendsPlayerSessionsWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlFriend* friends, const unsigned numFriends)
{
	rtry
	{
		rverifyall(friends != nullptr);
		rverifyall(numFriends > 0);

		m_Friends = friends;
		m_NumFriends = numFriends;
		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpGetFriendsPlayerSessionsWorkItem::DoWork()
{
	// Example: "18446744073709551615,18446744073709551614,18446744073709551613"
	static const unsigned ACCOUNTIDS_BUFFER_SIZE = ((NP_WEB_API_MAX_ACCOUNT_ID_CHARS + 1) * NP_WEB_API_MAX_FRIENDS_SESSIONS) + 2;

	unsigned count = 0;
	char buffer[ACCOUNTIDS_BUFFER_SIZE] = { 0 };
	char accountId[NP_WEB_API_MAX_ACCOUNT_ID_BYTES];

	for (unsigned i = 0; i < m_NumFriends && count < NP_WEB_API_MAX_FRIENDS_SESSIONS; ++i)
	{
		rlSceNpAccountId npAccountId = m_Friends[i].GetReference().m_Id.m_SceNpAccountId;

		// If the user isn't in the same title we don't need the session info
		if (npAccountId != RL_INVALID_NP_ACCOUNT_ID && m_Friends[i].IsInSameTitle())
		{
			formatf(accountId, "%" I64FMT "u", npAccountId);

			if (count > 0)
			{
				safecat(buffer, ",");
			}

			safecat(buffer, accountId);
			++count;
		}
		else
		{
			m_Friends[i].SetIsInSession(false);
		}
	}

	// Early out if there's nothing to do
	if (count == 0)
	{
		rlWarning("rlNpGetFriendsPlayerSessionsWorkItem skipped, no accounts to process");
		OnComplete();
		return;
	}
	
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<GetFriendsPlayerSessionsResponseBody>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		PlayerSessionsApi::ParameterToGetFriendsPlayerSessions param;
		ret = param.initialize(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToGetFriendsPlayerSessions failed. ret = 0x%x", ret));

		ret = param.setxPSNSESSIONMANAGERACCOUNTIDS(buffer);
		rverify(ret == SCE_OK, catchall, rlError("setxPSNSESSIONMANAGERACCOUNTIDS failed. ret = 0x%x", ret));

		ret = PlayerSessionsApi::getFriendsPlayerSessions(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("getFriendsPlayerSessions failed. ret = 0x%x", ret));

		Common::IntrusivePtr<GetFriendsPlayerSessionsResponseBody> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		Common::IntrusivePtr<Common::Vector<Common::IntrusivePtr<Friend>>> friends = response->getFriends();
		rverify(friends, catchall, );

		for (unsigned i = 0; i < m_NumFriends; ++i)
		{
			rlFriend& rlfriend = m_Friends[i];
			const SceNpAccountId accountid = rlfriend.GetReference().m_Id.m_SceNpAccountId;

			Common::IntrusivePtr<Friend> npfriend;

			for (Common::IntrusivePtr<Friend>& fri : *friends)
			{
				if (fri->accountIdIsSet() && fri->getAccountId() == accountid)
				{
					npfriend = fri;
				}
			}

			if (!npfriend || !npfriend->playerSessionsIsSet() || npfriend->getPlayerSessions()->empty())
			{
				rlfriend.SetIsInSession(false);
				continue;
			}

			Common::IntrusivePtr<Common::Vector<Common::IntrusivePtr<FriendJoinedPlayerSession>>> sessions = npfriend->getPlayerSessions();
#if RSG_OUTPUT
			if (sessions->size() > 1)
			{
				rlWarning("More than one player session found. [%u]", (unsigned)sessions->size());
			}
#endif

			Common::IntrusivePtr<FriendJoinedPlayerSession> ps = (*sessions)[0];
			const bool joinable = ps->playerJoinableStatusIsSet() && (ps->getPlayerJoinableStatus() == PlayerJoinableStatus::JOINABLE);

			// Nothing to set right now aside from the boolean. We may need to add joinable flags depending on the UI needs.
			rlfriend.SetIsInSession(joinable);
		}

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpGetJoinedPlayerSessionsWorkItem
****************************************/
rlNpGetJoinedPlayerSessionsWorkItem::rlNpGetJoinedPlayerSessionsWorkItem()
	: m_SessionIds(nullptr)
	, m_NumSessions(0)
{

}

bool rlNpGetJoinedPlayerSessionsWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId,
													rlSceNpSessionId* sessionIds, const unsigned numSessions)
{
	rtry
	{
		rverify(numSessions > 0, catchall, );
		rverify(numSessions <= MAX_REQUESTABLE_SESSIONS, catchall, rlError("Too many sessions: %u", numSessions));
		m_NumSessions = numSessions;

		rverify(sessionIds != nullptr, catchall, );
		m_SessionIds = sessionIds;

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpGetJoinedPlayerSessionsWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<GetUsersAccountIdPlayerSessionsResponseBody>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		char accountId[NP_WEB_API_MAX_ACCOUNT_ID_BYTES];
		formatf(accountId, "%" I64FMT "u", m_AccountId);

		PlayerSessionsApi::ParameterToGetJoinedPlayerSessionsByUser param;
		ret = param.initialize(m_LibCtx, accountId);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToGetJoinedPlayerSessionsByUser failed. ret = 0x%x", ret));
		
		ret = param.setplatformFilter(NP_WEB_API_PLATFORM_PROSPERO);
		rverify(ret == SCE_OK, catchall, rlError("setplatformFilter failed. ret = 0x%x", ret));

		ret = PlayerSessionsApi::getJoinedPlayerSessionsByUser(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("getJoinedPlayerSessionsByUser failed. ret = 0x%x", ret));

		Common::IntrusivePtr<GetUsersAccountIdPlayerSessionsResponseBody> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		Common::IntrusivePtr<Common::Vector<Common::IntrusivePtr<JoinedPlayerSessionWithPlatform>>> sessions = response->getPlayerSessions();
		rverify(sessions, catchall, );

		const unsigned sessionsCount = static_cast<unsigned>(sessions->size());
		const unsigned num = rage::Min(sessionsCount, m_NumSessions);

		for (unsigned i = 0; i < num; ++i)
		{
			Common::IntrusivePtr<JoinedPlayerSessionWithPlatform> session = (*sessions)[i];
			rverify(session, catchall, );

			rlSceNpSessionId sessionId;
			sessionId.Reset(session->getSessionId().c_str());

			m_SessionIds[i] = sessionId;
		}

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpSendPlayerSessionInvitationsWorkItem
****************************************/
bool rlNpSendPlayerSessionInvitationsWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiPostInvitationParam* param)
{
	rtry
	{
		rverify(param != nullptr, catchall,);
		m_InviteParam = *param;

		rverify(m_InviteParam.m_SessionId.IsValid(), catchall, );
		rverify(m_InviteParam.m_SessionInfo.IsValid(), catchall, );

		return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
	}
	rcatchall
	{
	}

	return false;
}

void rlNpSendPlayerSessionInvitationsWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::SessionManager::V1;

	Common::Transaction<Common::IntrusivePtr<PostPlayerSessionsSessionIdInvitationsResponseBody>> transaction;

	rtry
	{
		int ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::Vector< Common::IntrusivePtr<RequestPlayerSessionInvitation> > invitations(m_LibCtx);

		for (unsigned i = 0; i < m_InviteParam.m_NumAccountIds; ++i)
		{
			Common::IntrusivePtr<To> to;
			ret = ToFactory::create(m_LibCtx, m_InviteParam.m_AccountIds[i], &to);
			rverify(ret == SCE_OK, catchall, rlError("ToFactory failed. ret = 0x%x", ret));

			Common::IntrusivePtr<RequestPlayerSessionInvitation> requestPlayerSessionInvitation;
			ret = RequestPlayerSessionInvitationFactory::create(m_LibCtx, to, &requestPlayerSessionInvitation);
			rverify(ret == SCE_OK, catchall, rlError("RequestPlayerSessionInvitationFactory failed. ret = 0x%x", ret));

			ret = invitations.pushBack(requestPlayerSessionInvitation);
			rverify(ret == SCE_OK, catchall, rlError("pushBack failed. ret = 0x%x", ret));
		}

		Common::IntrusivePtr<PostPlayerSessionsSessionIdInvitationsRequestBody> postPlayerSessionsSessionIdInvitationsRequestBody;
		ret = PostPlayerSessionsSessionIdInvitationsRequestBodyFactory::create(m_LibCtx, &postPlayerSessionsSessionIdInvitationsRequestBody);
		rverify(ret == SCE_OK, catchall, rlError("PostPlayerSessionsSessionIdInvitationsRequestBodyFactory failed. ret = 0x%x", ret));

		ret = postPlayerSessionsSessionIdInvitationsRequestBody->setInvitations(invitations);
		rverify(ret == SCE_OK, catchall, rlError("PostPlayerSessionsSessionIdInvitationsRequestBody::setInvitations failed. ret = 0x%x", ret));

		PlayerSessionsApi::ParameterToSendPlayerSessionInvitations param;
		ret = param.initialize(m_LibCtx, m_InviteParam.m_SessionId.data, postPlayerSessionsSessionIdInvitationsRequestBody);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToSendPlayerSessionInvitations failed. ret = 0x%x", ret));

		ret = PlayerSessionsApi::sendPlayerSessionInvitations(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("sendPlayerSessionInvitations failed. ret = 0x%x", ret));

		Common::IntrusivePtr<PostPlayerSessionsSessionIdInvitationsResponseBody> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed();
		}
	}

	transaction.finish();
}

/****************************************
rlNpCreateMatchWorkItem
****************************************/
rlNpCreateMatchWorkItem::rlNpCreateMatchWorkItem()
    : m_MatchId(nullptr)
{

}

bool rlNpCreateMatchWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchCreateParams& param, rlNpMatchId* matchId)
{
	m_Param = param;
	m_MatchId = matchId;
	return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpCreateMatchWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::Matches::V1;

	Common::Transaction<Common::IntrusivePtr<CreateMatchResponse>> transaction;

	int ret = -1;

	rtry
	{
		ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::IntrusivePtr<CreateMatchRequest> request;
		ret = CreateMatchRequestFactory::create(m_LibCtx, rlPresenceActivityIdToString(m_Param.m_ActivityId).c_str(), &request);
		rverify(ret == SCE_OK, catchall, rlError("CreateMatchRequestFactory failed. ret = 0x%x", ret));

		const int32_t expirationTime = Min<u32>(Max<u32>(m_Param.m_expirationTime, 60), 2678400);
		request->setExpirationTime(expirationTime);

		if (m_Param.m_ZoneId.IsNotNull())
		{
			ret = request->setZoneId(rlPresenceActivityIdToString(m_Param.m_ZoneId).c_str());
			rverify(ret == SCE_OK, catchall, rlError("setZoneId failed. ret = 0x%x", ret));
		}

		// A match can be created without any players in it
		if (!m_Param.m_Players.IsEmpty() || !m_Param.m_Teams.IsEmpty())
		{
			Common::IntrusivePtr<RequestInGameRoster> inGameRoster;
			ret = RequestInGameRosterFactory::create(m_LibCtx, &inGameRoster);
			rverify(ret == SCE_OK, catchall, rlError("RequestInGameRosterFactory failed. ret = 0x%x", ret));

			rcheck(MatchFillRoster(m_LibCtx, m_Param.m_Players, m_Param.m_Teams, inGameRoster), catchall, rlError("FillRoster failed"));

			ret = request->setInGameRoster(inGameRoster);
			rverify(ret == SCE_OK, catchall, rlError("setInGameRoster failed. ret = 0x%x", ret));
		}

		MatchApi::ParameterToCreateMatch param;
		ret = param.initialize(m_LibCtx, request);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToCreateMatch failed. ret = 0x%x", ret));

		ret = MatchApi::createMatch(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("createMatch failed. ret = 0x%x", ret));

		Common::IntrusivePtr<CreateMatchResponse> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		rlNpMatchId guid;
		sysGuidUtil::GuidFromString(guid, response->getMatchId().c_str());

		if (m_MatchId != nullptr)
		{
			*m_MatchId = guid;
		}

		// A bit ugly but avoids storing the match id for the review dialog
		g_rlNp.GetWebAPI().SetMostRecentMatchId(m_LocalGamerIndex, guid);

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed(ret);
		}
	}

	transaction.finish();
}

/****************************************
rlNpUpdateMatchDetailWorkItem
****************************************/
bool rlNpUpdateMatchDetailWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchUpdateParams& param)
{
	m_Param = param;

	return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpUpdateMatchDetailWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::Matches::V1;

	Common::Transaction<Common::DefaultResponse> transaction;
	int ret = -1;

	rtry
	{
		ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::IntrusivePtr<UpdateMatchDetailRequest> request;
		ret = UpdateMatchDetailRequestFactory::create(m_LibCtx, &request);
		rverify(ret == SCE_OK, catchall, rlError("UpdateMatchDetailRequestFactory failed. ret = 0x%x", ret));

		if (!m_Param.m_Players.IsEmpty() || !m_Param.m_Teams.IsEmpty())
		{
			Common::IntrusivePtr<RequestInGameRoster> inGameRoster;
			ret = RequestInGameRosterFactory::create(m_LibCtx, &inGameRoster);
			rverify(ret == SCE_OK, catchall, rlError("RequestInGameRosterFactory failed. ret = 0x%x", ret));

			rcheck(MatchFillRoster(m_LibCtx, m_Param.m_Players, m_Param.m_Teams, inGameRoster), catchall, rlError("FillRoster failed"));

			ret = request->setInGameRoster(inGameRoster);
			rverify(ret == SCE_OK, catchall, rlError("setInGameRoster failed. ret = 0x%x", ret));
		}

		if (m_Param.m_ZoneId.IsNotNull())
		{
			ret = request->setZoneId(rlPresenceActivityIdToString(m_Param.m_ZoneId).c_str());
			rverify(ret == SCE_OK, catchall, rlError("setZoneId failed. ret = 0x%x", ret));
		}

		MatchApi::ParameterToUpdateMatchDetail param;
		param.initialize(m_LibCtx, sysGuidUtil::GuidToFixedString(m_Param.m_MatchId, sysGuidUtil::kGuidStringFormatNonDecorated).c_str(), request);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToUpdateMatchDetail failed. ret = 0x%x", ret));

		ret = MatchApi::updateMatchDetail(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("updateMatchDetail failed. ret = 0x%x", ret));

		Common::DefaultResponse response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed(ret);
		}
	}

	transaction.finish();
}

/****************************************
rlNpUpdateMatchStatusWorkItem
****************************************/
bool rlNpUpdateMatchStatusWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId, rlNpMatchStatus matchStatus)
{
	m_MatchId = matchId;
	m_MatchStatus = matchStatus;

	return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpUpdateMatchStatusWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::Matches::V1;

	Common::Transaction<Common::DefaultResponse> transaction;
	int ret = -1;

	rtry
	{
		ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		UpdateStatus status = UpdateStatus::_NOT_SET;

		switch (m_MatchStatus)
		{
		case rlNpMatchStatus::Playing: status = UpdateStatus::PLAYING; break;
		case rlNpMatchStatus::OnHold: status = UpdateStatus::ONHOLD; break;
		case rlNpMatchStatus::Canceled: status = UpdateStatus::CANCELLED; break;
		}

		Common::IntrusivePtr<UpdateMatchStatusRequest> request;
		ret = UpdateMatchStatusRequestFactory::create(m_LibCtx, status, &request);
		rverify(ret == SCE_OK, catchall, rlError("UpdateMatchStatusRequestFactory failed. ret = 0x%x", ret));

		MatchApi::ParameterToUpdateMatchStatus param;
		param.initialize(m_LibCtx, sysGuidUtil::GuidToFixedString(m_MatchId, sysGuidUtil::kGuidStringFormatNonDecorated).c_str(), request);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToUpdateMatchStatus failed. ret = 0x%x", ret));

		ret = MatchApi::updateMatchStatus(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("updateMatchStatus failed. ret = 0x%x", ret));

		Common::DefaultResponse response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed(ret);
		}
	}

	transaction.finish();
}

/****************************************
rlNpJoinMatchWorkItem
****************************************/
bool rlNpJoinMatchWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId, const rlNpMatchJoiningPlayers& joining)
{
	m_MatchId = matchId;
	m_Players = joining;

	return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpJoinMatchWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::Matches::V1;

	Common::Transaction<Common::DefaultResponse> transaction;
	int ret = -1;

	rtry
	{
		ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::Vector<Common::IntrusivePtr<AddedPlayer>> players(m_LibCtx);
		for (const rlNpMatchPlayerJoin & player : m_Players)
		{
			const PlayerType playerType = player.m_GamerInfo.GetGamerHandle().IsNativeGamerHandle() ? PlayerType::PSN_PLAYER : PlayerType::NON_PSN_PLAYER;

			Common::IntrusivePtr<AddedPlayer> addedPlayer;
			ret = AddedPlayerFactory::create(m_LibCtx, rlNpMatchFormatPlayerId(player.m_GamerInfo.GetGamerHandle()).c_str(), playerType, &addedPlayer);

			ret = addedPlayer->setPlayerName(player.m_GamerInfo.GetDisplayName());
			rverify(ret == SCE_OK, catchall, rlError("setPlayerName failed. ret = 0x%x", ret));

			if (player.m_GamerInfo.GetGamerHandle().IsNativeGamerHandle())
			{
				ret = addedPlayer->setAccountId(rlNpMatchFormatAccountId(player.m_GamerInfo.GetGamerHandle()).c_str());
				rverify(ret == SCE_OK, catchall, rlError("setAccountId failed. ret = 0x%x", ret));
			}

			if (player.HasTeam())
			{
				ret = addedPlayer->setTeamId(rlNpMatchFormatTeamId(player.m_TeamId).c_str());
				rverify(ret == SCE_OK, catchall, rlError("setTeamId failed. ret = 0x%x", ret));
			}

			ret = players.pushBack(addedPlayer);
			rverify(ret == SCE_OK, catchall, rlError("players pushBack failed. ret = 0x%x", ret));
		}

		Common::IntrusivePtr<JoinMatchRequest> request;
		ret = JoinMatchRequestFactory::create(m_LibCtx, players, &request);
		rverify(ret == SCE_OK, catchall, rlError("JoinMatchRequestFactory failed. ret = 0x%x", ret));

		MatchApi::ParameterToJoinMatch param;
		param.initialize(m_LibCtx, sysGuidUtil::GuidToFixedString(m_MatchId, sysGuidUtil::kGuidStringFormatNonDecorated).c_str(), request);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToJoinMatch failed. ret = 0x%x", ret));

		ret = MatchApi::joinMatch(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("joinMatch failed. ret = 0x%x", ret));

		Common::DefaultResponse response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		// A bit ugly but avoids storing the match id for the review dialog
		g_rlNp.GetWebAPI().SetMostRecentMatchId(m_LocalGamerIndex, m_MatchId);

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed(ret);
		}
	}

	transaction.finish();
}

/****************************************
rlNpLeaveMatchWorkItem
****************************************/
bool rlNpLeaveMatchWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId, const rlNpMatchLeavingPlayers& leaving)
{
	m_MatchId = matchId;
	m_Players = leaving;

	return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpLeaveMatchWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::Matches::V1;

	Common::Transaction<Common::DefaultResponse> transaction;
	int ret = -1;

	rtry
	{
		ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::Vector<Common::IntrusivePtr<RemovedPlayer>> players(m_LibCtx);
		for (const rlNpMatchPlayerLeave & pl : m_Players)
		{
			Common::IntrusivePtr<RemovedPlayer> removedPlayer;
			ret = RemovedPlayerFactory::create(m_LibCtx, rlNpMatchFormatPlayerId(pl.m_GamerHandle).c_str(), &removedPlayer);
			rverify(ret == SCE_OK, catchall, rlError("RemovedPlayerFactory failed. ret = 0x%x", ret));

			switch (pl.m_LeaveReason)
			{
			case rlNpMatchLeaveReason::Finished: removedPlayer->setReason(LeaveReason::FINISHED); break;
			case rlNpMatchLeaveReason::Disconnected: removedPlayer->setReason(LeaveReason::DISCONNECTED); break;
			case rlNpMatchLeaveReason::Quit: removedPlayer->setReason(LeaveReason::QUIT); break;
			default: break;
			}

			ret = players.pushBack(removedPlayer);
		}

		Common::IntrusivePtr<LeaveMatchRequest> request;
		ret = LeaveMatchRequestFactory::create(m_LibCtx, players, &request);
		rverify(ret == SCE_OK, catchall, rlError("LeaveMatchRequestFactory failed. ret = 0x%x", ret));

		MatchApi::ParameterToLeaveMatch param;
		param.initialize(m_LibCtx, sysGuidUtil::GuidToFixedString(m_MatchId, sysGuidUtil::kGuidStringFormatNonDecorated).c_str(), request);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToLeaveMatch failed. ret = 0x%x", ret));

		ret = MatchApi::leaveMatch(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("leaveMatch failed. ret = 0x%x", ret));

		Common::DefaultResponse response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed(ret);
		}
	}

	transaction.finish();
}

/****************************************
rlNpReportMatchResultWorkItem
****************************************/
bool rlNpReportMatchResultWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchResultParams& result)
{
	m_Result = result;

	return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpReportMatchResultWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::Matches::V1;

	Common::Transaction<Common::DefaultResponse> transaction;
	int ret = -1;

	rtry
	{
		ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::IntrusivePtr<RequestMatchResults> requestMatchResults;
		ret = RequestMatchResultsFactory::create(m_LibCtx, ResultsVersion::_1, &requestMatchResults);
		rverify(ret == SCE_OK, catchall, rlError("RequestMatchResultsFactory failed. ret = 0x%x", ret));

		if (m_Result.m_CoopResult != rlNpMatchCoopResult::Undefined)
		{
			switch (m_Result.m_CoopResult)
			{
			case rlNpMatchCoopResult::Success: requestMatchResults->setCooperativeResult(RequestCooperativeResult::SUCCESS); break;
			case rlNpMatchCoopResult::Unfinished: requestMatchResults->setCooperativeResult(RequestCooperativeResult::UNFINISHED); break;
			case rlNpMatchCoopResult::Failed: requestMatchResults->setCooperativeResult(RequestCooperativeResult::FAILED); break;
			default: rlError("Unexpected result"); break;
			}
		}
		else
		{
			Common::IntrusivePtr<RequestCompetitiveResult> requestCompetitiveResult;
			ret = RequestCompetitiveResultFactory::create(m_LibCtx, &requestCompetitiveResult);
			rverify(ret == SCE_OK, catchall, rlError("RequestCompetitiveResultFactory failed. ret = 0x%x", ret));
			
			if (!m_Result.m_TeamResults.IsEmpty()) // It was a team match
			{
				Common::Vector<Common::IntrusivePtr<RequestTeamResults>> requestTeamResults(m_LibCtx);

				for (const rlNpMatchTeamResult& teamResult : m_Result.m_TeamResults)
				{
					char teamIdStr[64];
					formatf(teamIdStr, "%d", teamResult.m_TeamId);
					Common::IntrusivePtr<RequestTeamResults> requestTeamResult;
					ret = RequestTeamResultsFactory::create(m_LibCtx, teamIdStr, teamResult.m_Rank, &requestTeamResult);
					rverify(ret == SCE_OK, catchall, rlError("RequestTeamResultsFactory failed. ret = 0x%x", ret));

					if (teamResult.HasScore())
					{
						requestTeamResult->setScore(teamResult.m_Score);
					}

					Common::Vector<Common::IntrusivePtr<RequestTeamMemberResult>> requestTeamMemberResults(m_LibCtx);

					for (const rlNpMatchPlayerResult& playerResult : m_Result.m_PlayerResults)
					{
						if (playerResult.m_TeamId != teamResult.m_TeamId)
						{
							continue;
						}

						Common::IntrusivePtr<RequestTeamMemberResult> requestTeamMemberResult;
						ret = RequestTeamMemberResultFactory::create(m_LibCtx, rlNpMatchFormatPlayerId(playerResult.m_GamerHandle).c_str(), playerResult.m_Score, &requestTeamMemberResult);
						rverify(ret == SCE_OK, catchall, rlError("RequestTeamMemberResultFactory failed. ret = 0x%x", ret));

						ret = requestTeamMemberResults.pushBack(requestTeamMemberResult);
						rverify(ret == SCE_OK, catchall, rlError("requestTeamMemberResults pushBack failed. ret = 0x%x", ret));
					}

					if (!requestTeamMemberResults.empty())
					{
						ret = requestTeamResults.pushBack(requestTeamResult);
						rverify(ret == SCE_OK, catchall, rlError("requestTeamResults pushBack failed. ret = 0x%x", ret));
					}
				}

				ret = requestCompetitiveResult->setTeamResults(requestTeamResults);
				rverify(ret == SCE_OK, catchall, rlError("setTeamResults failed. ret = 0x%x", ret));
			}
			else if (!m_Result.m_PlayerResults.IsEmpty()) // It was a non-team match
			{
				Common::Vector<Common::IntrusivePtr<RequestPlayerResults>> requestPlayerResults(m_LibCtx);

				for (const rlNpMatchPlayerResult& playerResult : m_Result.m_PlayerResults)
				{
					Common::IntrusivePtr<RequestPlayerResults> requestPlayerResult;
					ret = RequestPlayerResultsFactory::create(m_LibCtx, rlNpMatchFormatPlayerId(playerResult.m_GamerHandle).c_str(), playerResult.m_Rank, &requestPlayerResult);
					rverify(ret == SCE_OK, catchall, rlError("RequestPlayerResultsFactory failed. ret = 0x%x", ret));

					if (playerResult.HasScore())
					{
						requestPlayerResult->setScore(playerResult.m_Score);
					}

					ret = requestPlayerResults.pushBack(requestPlayerResult);
					rverify(ret == SCE_OK, catchall, rlError("requestPlayerResults pushBack failed. ret = 0x%x", ret));
				}

				ret = requestCompetitiveResult->setPlayerResults(requestPlayerResults);
				rverify(ret == SCE_OK, catchall, rlError("setPlayerResults failed. ret = 0x%x", ret));
			}

			ret = requestMatchResults->setCompetitiveResult(requestCompetitiveResult);
			rverify(ret == SCE_OK, catchall, rlError("setCompetitiveResult failed. ret = 0x%x", ret));
		}

		Common::IntrusivePtr<ReportResultsRequest> request;
		ret = ReportResultsRequestFactory::create(m_LibCtx, requestMatchResults, &request);
		rverify(ret == SCE_OK, catchall, rlError("ReportResultsRequestFactory failed. ret = 0x%x", ret));

		MatchApi::ParameterToReportResults param;
		param.initialize(m_LibCtx, sysGuidUtil::GuidToFixedString(m_Result.m_MatchId, sysGuidUtil::kGuidStringFormatNonDecorated).c_str(), request);
		rverify(ret == SCE_OK, catchall, rlError("ParameterToReportResults failed. ret = 0x%x", ret));

		ret = MatchApi::reportResults(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("reportResults failed. ret = 0x%x", ret));

		Common::DefaultResponse response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed(ret);
		}
	}

	transaction.finish();
}

/****************************************
rlNpGetMatchDetailWorkItem
****************************************/
bool rlNpGetMatchDetailWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlNpMatchId& matchId)
{
	m_MatchId = matchId;

	return rlNpCppWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

void rlNpGetMatchDetailWorkItem::DoWork()
{
	using namespace sce::Np::CppWebApi;
	using namespace sce::Np::CppWebApi::Matches::V1;

	Common::Transaction<Common::IntrusivePtr<GetMatchDetailResponse>> transaction;
	int ret = -1;

	rtry
	{
		ret = transaction.start(m_LibCtx);
		rverify(ret == SCE_OK, catchall, rlError("transaction.start() failed. ret = 0x%x", ret));

		Common::Vector<Common::IntrusivePtr<RemovedPlayer>> players(m_LibCtx);

		MatchApi::ParameterToGetMatchDetail param;
		param.initialize(m_LibCtx, sysGuidUtil::GuidToFixedString(m_MatchId, sysGuidUtil::kGuidStringFormatNonDecorated).c_str());
		rverify(ret == SCE_OK, catchall, rlError("ParameterToGetMatchDetail failed. ret = 0x%x", ret));

		ret = MatchApi::getMatchDetail(m_WebApiUserCtxId, param, transaction);
		rverify(ret == SCE_OK, catchall, rlError("leaveMatch failed. ret = 0x%x", ret));

		Common::IntrusivePtr<GetMatchDetailResponse> response;
		ret = transaction.getResponse(response);
		rverify(ret == SCE_OK, catchall, rlError("getResponse failed. ret = 0x%x", ret));

		//TODO_ANDI: fill in if we ever need this for anything other than debugging

		OnComplete();
	}
	rcatchall
	{
		if (m_Status.Pending())
		{
			m_Status.SetFailed(ret);
		}
	}

	transaction.finish();
}

#endif //RL_NP_CPPWEBAPI

#if RL_NP_UDS
/****************************************
rlNpGetPlayerActiveActivitiesWorkItem
****************************************/
bool rlNpGetPlayerActiveActivitiesWorkItem::Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId)
{
	return rlNpWebApiWorkItem::Init(localGamerIndex, accountId, webApiUserCtxId);
}

const char* rlNpGetPlayerActiveActivitiesWorkItem::GetApiGroup()
{
	return API_GROUP_ACTIVITIES;
}

void rlNpGetPlayerActiveActivitiesWorkItem::SetupApiPath(rlSceNpAccountId accountId)
{
	// The limit is currently needed due to a Sony-server bug. By the time anyone reads this that'll probably be fixed.
	formatf(m_ApiPath, "/v1/users/activities?accountIds=%" I64FMT "u&limit=10", accountId);
}

bool rlNpGetPlayerActiveActivitiesWorkItem::ProcessSuccess()
{
	// Right now this task is only used for debugging to print out active activities.
#if !__NO_OUTPUT
	rtry
	{
		rcheck(m_ReadSize > 0, catchall,);
		RsonReader rr(GetBuffer(), m_ReadSize);

		RsonReader user;
		rr.GetMember("users", &user);

		char idbuffer[64] = { 0 };
		char namebuffer[128] = { 0 };

		bool hasMore = user.GetFirstMember(&user);
		while (hasMore)
		{
			user.ReadString("accountId", idbuffer);
			rlDebug1("User %s activities:", idbuffer);

			RsonReader activity;
			user.GetMember("activities", &activity);

			bool hasActivity = activity.GetFirstMember(&activity);
			while (hasActivity)
			{
				activity.ReadString("activityId", idbuffer);
				activity.ReadString("name", namebuffer);

				rlDebug1("activity[%s]: %s", idbuffer, namebuffer);

				hasActivity = activity.GetNextSibling(&activity);
			}

			hasMore = user.GetNextSibling(&user);
		}
	}
	rcatchall
	{
	}
#endif //!__NO_OUTPUT

	return true;
}

#endif //RL_NP_UDS

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

} // namespace rage

#endif // RSG_ORBIS
