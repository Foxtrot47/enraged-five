// 
// rline/rlnpfacebook.h
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#ifndef RLINE_RLNPFACEBOOK_H
#define RLINE_RLNPFACEBOOK_H

#include "rlnp.h"
#include "rline/facebook/rlfacebookcommon.h"

#if RL_FACEBOOK_ENABLED

#include "net/task.h"
#include "system/threadpool.h"

#include <np/np_sns_facebook.h>

namespace rage
{
	RAGE_DECLARE_SUBCHANNEL(rline_np, facebook)

	class npBasicFbGetLongAccessTokenWorkItem : public sysThreadPool::WorkItem
	{
	public:

		bool Configure(int requestId, SceNpSnsFacebookAccessTokenParam tokenParam);
		virtual void DoWork();

		s32 GetReturnCode() const {	return m_ReturnCode; }
		char* GetToken() { return m_Token; }
		u64 GetTokenExpiration() const { return m_Expiration; }

	private:

		SceNpSnsFacebookAccessTokenParam m_TokenParam;
		u64 m_Expiration;
		s32 m_ReturnCode;
		int m_RequestId;
		char m_Token[SCE_NP_SNS_FACEBOOK_ACCESS_TOKEN_LENGTH_MAX];
	};

	class npBasicFbGetLongAccessTokenTask : public netTask
	{
		friend netTask;

	public:

		NET_TASK_DECL(npBasicFbGetLongAccessTokenTask);
		NET_TASK_USE_CHANNEL(rline_np);

		npBasicFbGetLongAccessTokenTask();
		~npBasicFbGetLongAccessTokenTask();

		bool Configure(const int userId, const u64 fbAppId, const char* permissions, u32 options, char* token, u64* expiration);

		virtual void OnCancel();

		virtual netTaskStatus OnUpdate(int* resultCode);

		virtual void OnCleanup();

	private:

		enum State
		{
			STATE_GET_ACCESS_TOKEN,
			STATE_GETTING_ACCESS_TOKEN,
			STATE_GET_UI_ACCESS_TOKEN,
			STATE_GETTING_UI_ACCESS_TOKEN
		};

		u64 m_FbAppId;
		int m_UserId;
		char* m_Token;
		u32 m_Options;
		State m_State;
		int m_RequestId;
		u64* m_Expiration;
		char m_Permissions[SCE_NP_SNS_FACEBOOK_PERMISSIONS_LENGTH_MAX+1];

		SceNpSnsFacebookAccessTokenResult m_FacebookAccessToken;
		npBasicFbGetLongAccessTokenWorkItem m_WorkItem;
	};

} // namespace rage

#endif // RL_FACEBOOK_ENABLED
#endif // RLINE_RLNPFACEBOOK_H
#endif // RSG_ORBIS