// 
// rline/rlxhttp.cpp 
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "file/file_config.h"
#if __LIVE

#include "rlxhttp.h"
#include "data/growbuffer.h"
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "rline/rl.h"
#include "net/netaddress.h"
#include "net/status.h"
#include "net/task.h"
#include "net/time.h"
#include "string/string.h"
#include "system/xtl.h"
#include <xhttp.h>
#include <xauth.h>
#pragma comment(lib,"xhttp.lib")
#pragma comment(lib,"xauth.lib")

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, xhttp)
#undef __rage_channel
#define __rage_channel rline_xhttp

#define CRLF "\r\n"

PARAM(rlxhttpallowrejectedcert, "If present allow XHTTP to use non-authorized SSL certificates");

static bool s_rlXhttpIsInitialized = false;
static bool s_rlXAuthInitialized = false;
static bool s_rlXhttpInitialized = false;

static unsigned s_rlXhttpNextRequestId = 1;

static rlXhttpRequest s_rlXhttpRequests;

enum rlXhttpState
{
    RL_XHTTP_STATE_RETRY,
    RL_XHTTP_STATE_OFFLINE,
    RL_XHTTP_STATE_INIT_XAUTH,
    RL_XHTTP_STATE_INIT_XHTTP,
    RL_XHTTP_STATE_ONLINE,
    RL_XHTTP_STATE_SHUTDOWN,
};

static rlXhttpState s_rlXhttpState = RL_XHTTP_STATE_OFFLINE;

static netRetryTimer s_rlXhttpRetryTimer;

static void XHttpStatusCallback(HINTERNET hInternet,
                                DWORD_PTR dwContext,
                                DWORD dwInternetStatus,
                                PVOID lpvStatusInformation,
                                DWORD dwStatusInformationLength);

//////////////////////////////////////////////////////////////////////////
//  rlXhttpRequestData
//////////////////////////////////////////////////////////////////////////
rlXhttpRequestData::rlXhttpRequestData()
    : m_WriteStatus(NULL)
    , m_ReadStatus(NULL)
{
    Clear();
}

void
rlXhttpRequestData::Clear()
{
    ClearReadData();

    m_WriteBuf = NULL;
    m_WriteBufLen = 0;
    m_NumBytesWritten = 0;
    m_WriteState = WS_NONE;
    m_ReadState = RS_NONE;
    rlAssert(!m_WriteStatus || !m_WriteStatus->Pending());
    m_WriteStatus = NULL;
    m_RequestId = 0;
}

void
rlXhttpRequestData::ClearReadData()
{
    m_ReadBuf = NULL;
    m_ReadBufLen = 0;
    m_NumBytesRead = NULL;
    rlAssert(!m_ReadStatus || !m_ReadStatus->Pending());
    m_ReadStatus = NULL;
}

//////////////////////////////////////////////////////////////////////////
//  rlXhttpRequest
//////////////////////////////////////////////////////////////////////////
rlXhttpRequest::rlXhttpRequest()
    : m_SendStatus(NULL)
    , m_hSession(NULL)
    , m_hConnection(NULL)
    , m_hRequest(NULL)
{
    m_Next = m_Prev = this;
}

rlXhttpRequest::~rlXhttpRequest()
{
    rlAssertf(!Pending(), "[%u]: Operation still pending", m_Data.m_RequestId);
    rlAssertf(!m_Data.m_WriteStatus, "[%u]: Write operation still pending", m_Data.m_RequestId);
    rlAssertf(!m_Data.m_ReadStatus, "[%u]: Read operation still pending", m_Data.m_RequestId);
    rlAssertf(!m_SendStatus, "[%u]: Send operation still pending", m_Data.m_RequestId);
    rlAssert(!m_hSession);
    rlAssert(!m_hConnection);
    rlAssert(!m_hRequest);
    rlAssert(m_Next == this);
    rlAssert(m_Prev == this);

    Close();
}

bool
rlXhttpRequest::Connect(const char* verb,
                        const char* hostname,
                        const char* path,
                        const char* userAgent,
                        const netSocketAddress* proxyAddr,
                        const unsigned flags)
{
    rtry
    {
        rverify(!Pending(),
                catchall,
                rlError("[%u]: Attempt to use a request object that's already in use", m_Data.m_RequestId));

        for(rlXhttpRequest* r = s_rlXhttpRequests.m_Next; r != &s_rlXhttpRequests; r = r->m_Next)
        {
            //Don't use rverify here because failure will jump to catchall and
            //cause the already pending request to be closed.  
            if(!rlVerifyf(this != r, "[%u]: Attempt to use a request object that's already in use", m_Data.m_RequestId))
            {
                return false;
            }
        }

        rverify(!m_hSession,
                catchall,
                rlError("[%u]: Already connected", m_Data.m_RequestId));

        rverify(rlXhttpRequestData::WS_NONE == m_Data.m_WriteState,
                catchall,
                rlError("Invalid write state: %d, expected WS_NONE (%d)",
                        m_Data.m_WriteState, rlXhttpRequestData::WS_NONE));

        rverify(rlXhttpRequestData::RS_NONE == m_Data.m_ReadState,
                catchall,
                rlError("Invalid read state: %d, expected RS_NONE (%d)",
                        m_Data.m_ReadState, rlXhttpRequestData::RS_NONE));

        Close();

        m_Data.m_RequestId = s_rlXhttpNextRequestId++;

        char proxyAddrStr[128];

        rlDebug2("[%u]: '%s' request to '%s%s', proxy '%s'",
                m_Data.m_RequestId,
                verb,
                hostname,
                path,
                proxyAddr ? proxyAddr->Format(proxyAddrStr, true) : "none");

        m_hSession = XHttpOpen(userAgent,
                                proxyAddr ? XHTTP_ACCESS_TYPE_NAMED_PROXY : XHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                                proxyAddr ? proxyAddr->Format(proxyAddrStr, true) : XHTTP_NO_PROXY_NAME,
                                XHTTP_NO_PROXY_BYPASS,
                                XHTTP_FLAG_ASYNC);

        rverify(NULL != m_hSession,
                catchall,
                rlError("[%u]: Error calling XHttpOpen (%08x)", m_Data.m_RequestId, GetLastError()));

        XHttpSetStatusCallback(m_hSession,
                                XHttpStatusCallback,
                                XHTTP_CALLBACK_FLAG_ALL_NOTIFICATIONS,
                                NULL);

        if(RL_XHTTP_CONNECT_SSL == (flags & RL_XHTTP_CONNECT_SSL))
        {
            m_hConnection = XHttpConnect(m_hSession, hostname, INTERNET_DEFAULT_PORT, XHTTP_FLAG_SECURE);
        }
        else
        {
            m_hConnection = XHttpConnect(m_hSession, hostname, INTERNET_DEFAULT_PORT, 0);
        }

        rverify(NULL != m_hConnection,
                catchall,
                rlError("[%u]: Error calling XHttpConnect (%08x)", m_Data.m_RequestId, GetLastError()));

        // DWORD seems like a reasonable guess for what type an untyped windows API function takes
        // TODO: Should figure out if we want to lower this. Default is probably 64KB
        /*
        const DWORD recvBufferSize = 8192;
        rverify(XHttpSetOption(m_hConnection,
                               XHTTP_OPTION_READ_BUFFER_SIZE,
                               &recvBufferSize,
                               sizeof(recvBufferSize)),
                catchall,
                rlError("[%u]: Error calling XHttpSetOption with XHTTP_OPTION_READ_BUFFER_SIZE (%08x)", m_Data.m_RequestId, GetLastError()));
        */


        //XHTTP requires that that SSL certificates are issued from a
        //recognized authority.  This can be overridden.
        if(RL_XHTTP_CONNECT_SSL == (flags & RL_XHTTP_CONNECT_SSL)
            && PARAM_rlxhttpallowrejectedcert.Get())
        {
            const DWORD setting = SECURITY_FLAG_ALLOW_LIVE_REJECTED_CERT;
            rverify(XHttpSetOption(m_hConnection,
                                    XHTTP_OPTION_SECURITY_FEATURE,
                                    &setting,
                                    sizeof(setting)),
                    catchall,
                    rlError("[%u]: Error calling XHttpSetOption (%08x)", m_Data.m_RequestId, GetLastError()));
        }

        m_hRequest = XHttpOpenRequest(m_hConnection,
                                        verb,
                                        path,
                                        NULL,
                                        XHTTP_NO_REFERRER,
                                        NULL,
                                        0);

        rverify(NULL != m_hRequest,
                catchall,
                rlError("[%u]: Error calling XHttpOpenRequest (%08x)", m_Data.m_RequestId, GetLastError()));

        XHttpSetStatusCallback(m_hRequest,
                                XHttpStatusCallback,
                                XHTTP_CALLBACK_FLAG_ALL_NOTIFICATIONS,
                                NULL);

        m_Data.m_WriteState = rlXhttpRequestData::WS_CONNECTED;

        return true;
    }
    rcatchall
    {
        Close();
    }

    return false;
}

bool
rlXhttpRequest::SendRequest(const char* headers,
                            const unsigned length,
                            const char* content,
                            const unsigned contentLength,
                            const unsigned totalLength,
                            netStatus* status)
{
    status->SetPending();

    rtry
    {
        for(rlXhttpRequest* r = s_rlXhttpRequests.m_Next; r != &s_rlXhttpRequests; r = r->m_Next)
        {
            //Don't use rverify here because failure will jump to catchall and
            //cause the already pending request to be closed.  
            if(!rlVerifyf(this != r, "Attempt to use a request object that's already in use"))
            {
                return false;
            }
        }

        rlDebug2("[%u]: Sending request", m_Data.m_RequestId);

        rverify(rlXhttpRequestData::WS_CONNECTED == m_Data.m_WriteState,
                catchall,
                rlError("Invalid write state: %d, expected WS_CONNECTED (%d)",
                        m_Data.m_WriteState, rlXhttpRequestData::WS_CONNECTED));

        rverify(rlXhttpRequestData::RS_NONE == m_Data.m_ReadState,
                catchall,
                rlError("Invalid read state: %d, expected RS_NONE (%d)",
                        m_Data.m_ReadState, rlXhttpRequestData::RS_NONE));

        rverify(!m_SendStatus,
                catchall,
                rlError("[%u]: A send request is already in progress", m_Data.m_RequestId));

        rverify(m_hRequest,
                catchall,
                rlError("Uninitialized HTTP request"));

        rverify(XHttpSendRequest(m_hRequest,
                                headers ? headers : XHTTP_NO_ADDITIONAL_HEADERS,
                                headers ? length : 0,
                                content ? content : XHTTP_NO_REQUEST_DATA,
                                content ? contentLength : 0,
                                totalLength ? totalLength : XHTTP_IGNORE_REQUEST_TOTAL_LENGTH,
                                (DWORD_PTR)&m_Data),
                catchall,
                rlError("[%u]: Error calling XHttpSendRequest (%08x)", m_Data.m_RequestId, GetLastError()));

        m_Data.m_WriteState = rlXhttpRequestData::WS_SENDING_REQUEST;
        m_Data.m_ReadState = rlXhttpRequestData::RS_NONE;
        m_SendStatus = status;
        Link(&s_rlXhttpRequests);
        return true;
    }
    rcatchall
    {
        Close();
    }

    status->SetFailed();

    return false;
}

bool
rlXhttpRequest::WriteData(const u8* data, const unsigned dataLen, netStatus* status)
{
    status->SetPending();

    rtry
    {
        rverify(m_hRequest,
                catchall,
                rlError("[%u]: Invalid request object", m_Data.m_RequestId));
        rverify(!m_Data.m_WriteStatus,
                catchall,
                rlError("[%u]: A write operation is already in progress", m_Data.m_RequestId));
        rverify(dataLen > 0 && data != NULL,
                catchall,
                rlError("[%u]: Invalid data", m_Data.m_RequestId));
        rverify(m_Data.m_WriteState == rlXhttpRequestData::WS_WRITE_DATA,
                catchall,
                rlError("[%u]: Invalid write state: %d, exptected WS_WRITE_DATA (%d)", m_Data.m_RequestId, m_Data.m_WriteState, rlXhttpRequestData::WS_WRITE_DATA));

        bool found = false;
        for(rlXhttpRequest* r = s_rlXhttpRequests.m_Next; r != &s_rlXhttpRequests; r = r->m_Next)
        {
            if(this == r)
            {
                found = true;
                break;
            }
        }

        rverify(found,
                catchall,
                rlError("Attempt to write using an inactive request"));

        m_Data.m_WriteBuf = data;
        m_Data.m_WriteBufLen = dataLen;
        m_Data.m_NumBytesWritten = 0;

        m_Data.m_WriteStatus = status;

        m_Data.m_WriteState = rlXhttpRequestData::WS_WRITING_DATA;
        if(!XHttpWriteData(m_hRequest, m_Data.m_WriteBuf, m_Data.m_WriteBufLen, NULL))
        {
            rlError("[%u]: Error calling XHttpWriteData (%08x)", m_Data.m_RequestId, GetLastError());
            m_Data.m_WriteState = rlXhttpRequestData::WS_ERROR;
        }

        return true;
    }
    rcatchall
    {
        Close();
    }

    status->SetFailed();
    return false;
}

bool
rlXhttpRequest::ReadData(u8* data, const unsigned maxLen, unsigned* numRead, netStatus* status)
{
    status->SetPending();

    rtry
    {
        rverify(m_hRequest,
                catchall,
                rlError("[%u]: Invalid request object", m_Data.m_RequestId));
        rverify(!m_Data.m_ReadStatus,
                catchall,
                rlError("[%u]: A read operation is already in progress", m_Data.m_RequestId));
        rverify(maxLen > 0 && data != NULL,
                catchall,
                rlError("[%u]: Invalid data", m_Data.m_RequestId));
        rverify(m_Data.m_ReadState == rlXhttpRequestData::RS_READ_DATA,
                catchall,
                rlError("[%u]: Invalid read state: %d, expected RS_READ_DATA (%d)", m_Data.m_RequestId, m_Data.m_ReadState, rlXhttpRequestData::RS_READ_DATA));

        bool found = false;
        for(rlXhttpRequest* r = s_rlXhttpRequests.m_Next; r != &s_rlXhttpRequests; r = r->m_Next)
        {
            if(this == r)
            {
                found = true;
                break;
            }
        }

        rverify(found,
                catchall,
                rlError("Attempt to read using an inactive request"));

        m_Data.m_ReadBuf = data;
        m_Data.m_ReadBufLen = maxLen;
        m_Data.m_NumBytesRead = numRead;
        *m_Data.m_NumBytesRead = 0;

        m_Data.m_ReadStatus = status;

        m_Data.m_ReadState = rlXhttpRequestData::RS_READING_DATA;
        if(!XHttpReadData(m_hRequest, m_Data.m_ReadBuf, m_Data.m_ReadBufLen, NULL))
        {
            rlError("[%u]: Error calling XHttpReadData (%08x)", m_Data.m_RequestId, GetLastError());
            m_Data.m_ReadState = rlXhttpRequestData::RS_ERROR;
        }

        return true;
    }
    rcatchall
    {
        Close();
    }

    status->SetFailed();
    return false;
}

bool
rlXhttpRequest::HaveHeaders() const
{
    return m_Data.m_ReadState > rlXhttpRequestData::RS_WAITING_FOR_HEADERS;
}

unsigned
rlXhttpRequest::GetHttpStatusCode() const
{
    rtry
    {
        rverify(m_hRequest,
                catchall,
                rlError("[%u]: Invalid request object", m_Data.m_RequestId));

        rverify(HaveHeaders(),
                catchall,
                rlError("Can't read HTTP status code - headers not yet received"));

        DWORD statusCode = 0;
        DWORD sizeofStatusCode = sizeof(statusCode);

        rcheck(XHttpQueryHeaders(m_hRequest,
                                XHTTP_QUERY_STATUS_CODE | XHTTP_QUERY_FLAG_NUMBER,
                                NULL,
                                &statusCode,
                                &sizeofStatusCode,
                                XHTTP_NO_HEADER_INDEX),
            catchall,
            rlError("[%u]: Error calling XHttpQueryHeaders (%08x)", m_Data.m_RequestId, GetLastError()));

        return statusCode;
    }
    rcatchall
    {
    }

    return 0;
}

bool
rlXhttpRequest::GetResponseHeaderLength(unsigned* headerLen) const
{
    rtry
    {
        rverify(m_hRequest,
                catchall,
                rlError("[%u]: Invalid request object", m_Data.m_RequestId));

        DWORD dwValLen;
        rcheck(!XHttpQueryHeaders(m_hRequest,
                                XHTTP_QUERY_RAW_HEADERS_CRLF,
                                XHTTP_HEADER_NAME_BY_INDEX,
                                XHTTP_NO_OUTPUT_BUFFER,
                                &dwValLen,
                                XHTTP_NO_HEADER_INDEX),
                catchall,
                rlError("Expected XHttpQueryHeaders() to return false"));

        const DWORD dwErr = GetLastError();

        rcheck(ERROR_INSUFFICIENT_BUFFER == dwErr,
                catchall,
                rlError("[%u]: Error calling XHttpQueryHeaders (%08x)", m_Data.m_RequestId, dwErr));

        *headerLen = dwValLen;
        return true;
    }
    rcatchall
    {
    }

    return false;
}

bool
rlXhttpRequest::GetResponseHeader(char* headerBuf, unsigned* headerLen) const
{
    rtry
    {
        rverify(m_hRequest,
                catchall,
                rlError("[%u]: Invalid request object", m_Data.m_RequestId));

        rverify(HaveHeaders(),
                catchall,
                rlError("Can't read header - headers not yet received"));

        DWORD dwValLen = *headerLen;
        rcheck(XHttpQueryHeaders(m_hRequest,
                                XHTTP_QUERY_RAW_HEADERS_CRLF,
                                XHTTP_HEADER_NAME_BY_INDEX,
                                headerBuf,
                                &dwValLen,
                                XHTTP_NO_HEADER_INDEX),
            catchall,
            rlError("[%u]: Error calling XHttpQueryHeaders (%08x)", m_Data.m_RequestId, GetLastError()));

        *headerLen = dwValLen;
        return true;
    }
    rcatchall
    {
    }

    return false;
}

bool
rlXhttpRequest::GetResponseHeaderValue(const char* fieldName,
                                        char* value,
                                        unsigned* valLen) const
{
    rtry
    {
        rverify(m_hRequest,
                catchall,
                rlError("[%u]: Invalid request object", m_Data.m_RequestId));

        rverify(HaveHeaders(),
                catchall,
                rlError("Can't read value for header '%s' - headers not yet received",
                        fieldName));

        DWORD dwValLen = *valLen;
        rcheck(XHttpQueryHeaders(m_hRequest,
                                XHTTP_QUERY_CUSTOM,
                                fieldName,
                                value,
                                &dwValLen,
                                XHTTP_NO_HEADER_INDEX),
                catchall,
                rlError("[%u]: Error calling XHttpQueryHeaders (%08x)", m_Data.m_RequestId, GetLastError()));

        *valLen = dwValLen;
        return true;
    }
    rcatchall
    {
    }

    return false;
}

void
rlXhttpRequest::FinishSuccess()
{
    if(rlVerify(Pending()))
    {
        rlDebug2("[%u]: Succeeded", m_Data.m_RequestId);

        CloseHandles();

        //CloseHandles() might have called our callback function, which
        //in turn could have caused our status objects to be set to
        //a non-pending state.  So we could have non-null, non-pending
        //status objects.

        if(m_SendStatus)
        {
            if(m_SendStatus->Pending())
            {
                m_SendStatus->SetSucceeded();
            }
            m_SendStatus = NULL;
        }

        if(m_Data.m_WriteStatus)
        {
            if(m_Data.m_WriteStatus->Pending())
            {
                m_Data.m_WriteStatus->SetSucceeded();
            }
            m_Data.m_WriteStatus = NULL;
        }

        if(m_Data.m_ReadStatus)
        {
            if(m_Data.m_ReadStatus->Pending())
            {
                m_Data.m_ReadStatus->SetSucceeded();
            }
            m_Data.m_ReadStatus = NULL;
        }

        Close();
    }
}

void
rlXhttpRequest::FinishFail()
{
    if(rlVerify(Pending()))
    {
        rlDebug2("[%u]: Failed", m_Data.m_RequestId);

        CloseHandles();

        //CloseHandles() might have called our callback function, which
        //in turn could have caused our status objects to be set to
        //a non-pending state.  So we could have non-null, non-pending
        //status objects.

        if(m_SendStatus)
        {
            if(m_SendStatus->Pending())
            {
                m_SendStatus->SetFailed();
            }
            m_SendStatus = NULL;
        }

        if(m_Data.m_WriteStatus)
        {
            if(m_Data.m_WriteStatus->Pending())
            {
                m_Data.m_WriteStatus->SetFailed();
            }
            m_Data.m_WriteStatus = NULL;
        }

        if(m_Data.m_ReadStatus)
        {
            if(m_Data.m_ReadStatus->Pending())
            {
                m_Data.m_ReadStatus->SetFailed();
            }
            m_Data.m_ReadStatus = NULL;
        }

        Close();
    }
}

void
rlXhttpRequest::Cancel()
{
    if(Pending())
    {
        rlDebug2("[%u]: Canceled", m_Data.m_RequestId);

        CloseHandles();

        //CloseHandles() might have called our callback function, which
        //in turn could have caused our status objects to be set to
        //a non-pending state.  So we could have non-null, non-pending
        //status objects.

        if(m_SendStatus)
        {
            if(m_SendStatus->Pending())
            {
                m_SendStatus->SetCanceled();
            }
            m_SendStatus = NULL;
        }

        if(m_Data.m_WriteStatus)
        {
            if(m_Data.m_WriteStatus->Pending())
            {
                m_Data.m_WriteStatus->SetCanceled();
            }
            m_Data.m_WriteStatus = NULL;
        }

        if(m_Data.m_ReadStatus)
        {
            if(m_Data.m_ReadStatus->Pending())
            {
                m_Data.m_ReadStatus->SetCanceled();
            }
            m_Data.m_ReadStatus = NULL;
        }

        Close();
    }
}

bool
rlXhttpRequest::Pending() const
{
    return NULL != m_hRequest;
}

//private:

void
rlXhttpRequest::Update()
{
    rlAssert(Pending());

    if(m_hSession)
    {
        XHttpDoWork(m_hSession, 0);
    }

    switch(m_Data.m_WriteState)
    {
    case rlXhttpRequestData::WS_NONE:
    case rlXhttpRequestData::WS_CONNECTED:
        break;
    case rlXhttpRequestData::WS_SENDING_REQUEST:
        break;
    case rlXhttpRequestData::WS_SENT_REQUEST:
        rlAssert(m_SendStatus);
        m_SendStatus->SetSucceeded();
        m_SendStatus = NULL;
        m_Data.m_WriteState = rlXhttpRequestData::WS_WRITE_DATA;
        m_Data.m_ReadState = rlXhttpRequestData::RS_WAITING_FOR_HEADERS;
        if(!XHttpReceiveResponse(m_hRequest, NULL))
        {
            rlError("[%u]: Error calling XHttpReceiveResponse (%08x)", m_Data.m_RequestId, GetLastError());
            m_Data.m_WriteState = rlXhttpRequestData::WS_ERROR;
        }

        break;
    case rlXhttpRequestData::WS_WRITE_DATA:
        // If a previous write finished but didn't write all of the data,
        // resubmit it
        if(m_Data.m_WriteBuf)
        {
            if(m_Data.m_NumBytesWritten < m_Data.m_WriteBufLen)
            {
                const void* src = &m_Data.m_WriteBuf[m_Data.m_NumBytesWritten];
                const unsigned numToWrite = m_Data.m_WriteBufLen - m_Data.m_NumBytesWritten;
                m_Data.m_WriteState = rlXhttpRequestData::WS_WRITING_DATA;
                if(!XHttpWriteData(m_hRequest, src, numToWrite, NULL))
                {
                    rlError("[%u]: Error calling XHttpWriteData (%08x)", m_Data.m_RequestId, GetLastError());
                    m_Data.m_WriteState = rlXhttpRequestData::WS_ERROR;
                }
            }
            else
            {
                m_Data.m_WriteBuf = NULL;
                m_Data.m_NumBytesWritten = m_Data.m_WriteBufLen = 0;
                m_Data.m_WriteStatus->SetSucceeded();
                m_Data.m_WriteStatus = NULL;
            }
        }
        break;
    case rlXhttpRequestData::WS_WRITING_DATA:
        break;
    case rlXhttpRequestData::WS_ERROR:
        break;
    }

    switch(m_Data.m_ReadState)
    {
    case rlXhttpRequestData::RS_NONE:
        break;
    case rlXhttpRequestData::RS_WAITING_FOR_HEADERS:
        break;
    case rlXhttpRequestData::RS_READ_DATA:        
        break;
    case rlXhttpRequestData::RS_READING_DATA:
        break;
    case rlXhttpRequestData::RS_ERROR:
        break;
    }

    if(rlXhttpRequestData::WS_ERROR == m_Data.m_WriteState
        || rlXhttpRequestData::RS_ERROR == m_Data.m_ReadState)
    {
        FinishFail();
    }
}

void
rlXhttpRequest::CloseHandles()
{
    if(m_hRequest)
    {
        XHttpCloseHandle(m_hRequest);
        m_hRequest = NULL;
    }

    if(m_hConnection)
    {
        XHttpCloseHandle(m_hConnection);
        m_hConnection = NULL;
    }

    if(m_hSession)
    {
        XHttpCloseHandle(m_hSession);
        m_hSession = NULL;
    }
}

void
rlXhttpRequest::Close()
{
    rlAssert(!m_SendStatus);
    rlAssert(!m_Data.m_WriteStatus);
    rlAssert(!m_Data.m_ReadStatus);

    CloseHandles();

    m_Data.Clear();
    Unlink();
}

void
rlXhttpRequest::Link(rlXhttpRequest* next)
{
    if(rlVerifyf(this == m_Next, "Already linked"))
    {
        m_Prev = next->m_Prev;
        m_Next = next;
        m_Prev->m_Next = m_Next->m_Prev = this;
    }
}

void
rlXhttpRequest::Unlink()
{
    m_Next->m_Prev = m_Prev;
    m_Prev->m_Next = m_Next;
    m_Next = m_Prev = this;
}

//////////////////////////////////////////////////////////////////////////
//  rlXhttp
//////////////////////////////////////////////////////////////////////////
bool
rlXhttp::Init()
{
    if(rlVerifyf(!s_rlXhttpIsInitialized, "Already initialized"))
    {
        s_rlXAuthInitialized = s_rlXhttpInitialized = false;
        s_rlXhttpRetryTimer.InitSeconds(5, 10);

        s_rlXhttpIsInitialized = true;

        return true;
    }

    return false;
}

void
rlXhttp::Shutdown()
{
    if(s_rlXhttpIsInitialized)
    {
        s_rlXhttpState = RL_XHTTP_STATE_SHUTDOWN;
        Update();
		XAuthShutdown();
        s_rlXhttpIsInitialized = false;
    }
}

void
rlXhttp::Update()
{
    if(!s_rlXhttpIsInitialized)
    {
        return;
    }

    rlXhttpRequest* rqst = s_rlXhttpRequests.m_Next;
    rlXhttpRequest* next = rqst->m_Next;
    for(; rqst != &s_rlXhttpRequests; rqst = next, next = next->m_Next)
    {
        rlAssert(rqst->Pending());
        rqst->Update();
    }

    switch(s_rlXhttpState)
    {
    case RL_XHTTP_STATE_RETRY:
        s_rlXhttpRetryTimer.Update();
        if(s_rlXhttpRetryTimer.IsTimeToRetry())
        {
            s_rlXhttpState = RL_XHTTP_STATE_OFFLINE;
        }
        break;
    case RL_XHTTP_STATE_OFFLINE:
        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
        {
            if(eXUserSigninState_SignedInToLive == XUserGetSigninState(i))
            {
                s_rlXhttpState = RL_XHTTP_STATE_INIT_XAUTH;
                break;
            }
        }
        break;
    case RL_XHTTP_STATE_INIT_XAUTH:
        rlDebug("Initializing XAuth...");
        {
            XAUTH_SETTINGS xas = {0};
            xas.SizeOfStruct = sizeof(xas);
            const HRESULT result = XAuthStartup(&xas);
            if(S_OK == result)
            {
                s_rlXAuthInitialized = true;
                s_rlXhttpState = RL_XHTTP_STATE_INIT_XHTTP;
            }
            else
            {
                rlError("Error calling XAuthStartup (%08x)", result);
                s_rlXhttpState = RL_XHTTP_STATE_SHUTDOWN;
            }
        }
        break;
    case RL_XHTTP_STATE_INIT_XHTTP:
        rlDebug("Initializing XHttp...");

        if(XHttpStartup(0, NULL))
        {
            rlDebug("Online!");
            s_rlXhttpState = RL_XHTTP_STATE_ONLINE;
        }
        else
        {
            OUTPUT_ONLY(const DWORD err = GetLastError());
            rlError("Error calling XAuthStartup (%08x)", err);
            s_rlXhttpState = RL_XHTTP_STATE_SHUTDOWN;
        }
        break;
    case RL_XHTTP_STATE_ONLINE:
        s_rlXhttpState = RL_XHTTP_STATE_SHUTDOWN;
        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
        {
            if(eXUserSigninState_SignedInToLive == XUserGetSigninState(i))
            {
                s_rlXhttpState = RL_XHTTP_STATE_ONLINE;
                break;
            }
        }
        break;
    case RL_XHTTP_STATE_SHUTDOWN:
        rlDebug("Going off line");
        while(&s_rlXhttpRequests != s_rlXhttpRequests.m_Next)
        {
            rlXhttpRequest* rqst = s_rlXhttpRequests.m_Next;
            rqst->Unlink();
            rqst->Cancel();
        }

        if(s_rlXhttpInitialized)
        {
            XHttpShutdown();
            s_rlXhttpInitialized = false;
        }

        if(s_rlXAuthInitialized)
        {
            XAuthShutdown();
            s_rlXAuthInitialized = false;
        }

        s_rlXhttpRetryTimer.Restart();
        s_rlXhttpState = RL_XHTTP_STATE_RETRY;
        rlDebug("Retrying in %d milliseconds",
                s_rlXhttpRetryTimer.GetMillisecondsUntilRetry());
        break;
    }
}

bool
rlXhttp::IsOnline()
{
    return (RL_XHTTP_STATE_ONLINE == s_rlXhttpState);
}

class GetAuthTokenTask : public netTask
{
public:

	NET_TASK_DECL(GetAuthTokenTask);
	NET_TASK_USE_CHANNEL(rline_xhttp);

    GetAuthTokenTask()
		: m_LocalGamerIndex(-1)
		, m_Uri(NULL)
		, m_AuthToken(NULL)
		, m_MyAuthToken(NULL)
		, m_State(STATE_GET_AUTH_TOKEN)
    {
    }

	bool Configure(const int localGamerIndex,
				   const char* uri,
				   rlRelyingPartyToken** authToken)
    {
		bool success = false;

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(uri, catchall, );
			rverify(authToken, catchall, );

			m_LocalGamerIndex = localGamerIndex,
			m_Uri = uri,
			m_AuthToken = authToken;

			success = true;
		}
		rcatchall
		{

		}

        return success;
    }

    virtual void OnCancel()
    {
        netTaskDebug("Canceled while in state %d", m_State);
		m_XovStatus.Cancel();
    }

    virtual netTaskStatus OnUpdate(int* /*resultCode*/)
    {
		netTaskStatus status = NET_TASKSTATUS_PENDING;

        switch(m_State)
        {
        case STATE_GET_AUTH_TOKEN:
            if(WasCanceled())
            {
                netTaskDebug("Canceled - bailing...");
                status = NET_TASKSTATUS_FAILED;
            }
            else if(GetAuthToken())
            {
                m_State = STATE_GETTING_AUTH_TOKEN;
            }
            else
            {
                status = NET_TASKSTATUS_FAILED;
            }
            break;

        case STATE_GETTING_AUTH_TOKEN:
            m_XovStatus.Update();
            if(!m_XovStatus.Pending())
            {
                if(m_MyStatus.Succeeded())
                {
                    status = NET_TASKSTATUS_SUCCEEDED;
                }
                else
                {
                    status = NET_TASKSTATUS_FAILED;
                }
            }
            break;
        }

		if(NET_TASKSTATUS_PENDING != status)
		{
			this->Complete(status);
		}
		
        return status;
    }

private:

    bool GetAuthToken()
    {
		netTaskDebug("Getting STS token for gamer index %d", m_LocalGamerIndex);

		// we've disabled HTTPS requests on Xbox 360 to reduce server load.
		// Pretend we're going to make an HTTPS request so that XAuthGetToken
		// still works, then we'll send the token over HTTP.
		static const char httpScheme[] = "http://" ;
		bool isHttpScheme = strnicmp(m_Uri, httpScheme, sizeof(httpScheme) - 1) == 0;

		if(isHttpScheme)
		{
			unsigned bufSize = strlen(m_Uri) + 1 + 1; // +1 for "https" vs "http", +1 for null terminator
			char* finalUri = Alloca(char, bufSize);
			if(finalUri == NULL)
			{
				netTaskError("Error allocating %u bytes for uri %s", bufSize, m_Uri);
				return false;
			}

			safecpy(finalUri, "https://", bufSize);
			safecat(finalUri, &m_Uri[sizeof(httpScheme) - 1], bufSize);

			m_Uri = finalUri;
		}

		m_XovStatus.Begin("XAuthGetToken", &m_MyStatus);
		HRESULT h = XAuthGetToken(m_LocalGamerIndex,
								  m_Uri,
								  strlen(m_Uri),
								  (RELYING_PARTY_TOKEN**)&m_MyAuthToken,
								  m_XovStatus.ToXov());

		if(h == S_OK)
		{
			m_MyStatus.ForceSucceeded();
			return true;
		}
		
		if(h != HRESULT_FROM_WIN32(ERROR_IO_PENDING))
		{
			m_MyStatus.SetFailed();
			OUTPUT_ONLY(const DWORD err = GetLastError());
			netTaskError("Error calling XAuthGetToken (%08x)", err);
			return false;
		}

		return true;
    }

	void Complete(const netTaskStatus status)
	{
		if(NET_TASKSTATUS_SUCCEEDED == status && !WasCanceled())
		{
			*m_AuthToken = m_MyAuthToken;
		}
		else
		{
			if(m_MyAuthToken)
			{
				rlXhttp::FreeAuthToken(m_MyAuthToken);
			}
		}
	}
	
    enum State
    {
        STATE_GET_AUTH_TOKEN,
        STATE_GETTING_AUTH_TOKEN,
	};

	int m_LocalGamerIndex;
	const char* m_Uri;
	rlRelyingPartyToken** m_AuthToken;
	rlRelyingPartyToken* m_MyAuthToken;

	rlXovStatus m_XovStatus;

    State m_State;
	netStatus m_MyStatus;
};

bool 
rlXhttp::GetAuthToken(const int localGamerIndex,
					  const char* uri,
					  rlRelyingPartyToken** authToken,
					  netStatus* status)
{
	bool success = false;

	GetAuthTokenTask* task;
	
	if(!netTask::Create(&task, status)
		|| !task->Configure(localGamerIndex, uri, authToken)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

	return success;
}

void 
rlXhttp::FreeAuthToken(rlRelyingPartyToken* authToken)
{
	if(authToken)
	{
		XAuthFreeToken((RELYING_PARTY_TOKEN*)authToken);
	}
}

static void XHttpStatusCallback(HINTERNET /*hInternet*/,
                                DWORD_PTR dwContext,
                                DWORD dwInternetStatus,
                                PVOID lpvStatusInformation,
                                DWORD dwStatusInformationLength)
{
    rlXhttpRequestData* rqstData = (rlXhttpRequestData*)dwContext;

    switch(dwInternetStatus)
    {
    case XHTTP_CALLBACK_STATUS_HANDLE_CLOSING:
        break;
    case XHTTP_CALLBACK_STATUS_REDIRECT:
        //FIXME handle redirects.
        break;
    case XHTTP_CALLBACK_STATUS_INTERMEDIATE_RESPONSE:
        //FIXME handle intermediate response.
        break;
    case XHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE:
        rlDebug3("[%u]: Sent request", rqstData->m_RequestId);
        rlAssert(0 == dwStatusInformationLength);
        rqstData->m_WriteState = rlXhttpRequestData::WS_SENT_REQUEST;
        break;
    case XHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE:
        rlDebug3("[%u]: Headers available", rqstData->m_RequestId);
        rlAssert(0 == dwStatusInformationLength);
        rqstData->m_ReadState = rlXhttpRequestData::RS_READ_DATA;
        break;
    case XHTTP_CALLBACK_STATUS_READ_COMPLETE:
        rlDebug3("[%u]: Read complete (%u bytes)", rqstData->m_RequestId, dwStatusInformationLength);
        *rqstData->m_NumBytesRead = dwStatusInformationLength;
        rqstData->m_ReadStatus->SetSucceeded();
        rqstData->ClearReadData();
        rqstData->m_ReadState = rlXhttpRequestData::RS_READ_DATA;
        break;
    case XHTTP_CALLBACK_STATUS_WRITE_COMPLETE:
        rlDebug3("[%u]: Write complete (%u bytes)", rqstData->m_RequestId, *(DWORD*)lpvStatusInformation);
        rqstData->m_NumBytesWritten += *(DWORD*)lpvStatusInformation;
        rqstData->m_WriteState = rlXhttpRequestData::WS_WRITE_DATA;
        // It's possible not all of the bytes we wanted to were written. If they are, end the request,
        // otherwise leave it intact and it'll get resubmitted in the next update since XHttp doesn't
        // support reentrant calls
        if (rqstData->m_NumBytesWritten >= rqstData->m_WriteBufLen)
        {
            rqstData->m_WriteBuf = NULL;
            rqstData->m_NumBytesWritten = rqstData->m_WriteBufLen = 0;
            rqstData->m_WriteStatus->SetSucceeded();
            rqstData->m_WriteStatus = NULL;
        }
        break;
    case XHTTP_CALLBACK_STATUS_REQUEST_ERROR:
        {
#define RL_XHTTP_HANDLE_ERROR(err) case err: rlError("[%u]: Error sending request: %s(%u)", (rqstData?rqstData->m_RequestId:0), #err, err); break;

            XHTTP_ASYNC_RESULT* ar = (XHTTP_ASYNC_RESULT*)lpvStatusInformation;
            switch(ar->dwError)
            {
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_INVALID_LIBRARY_VERSION);
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_URL_NOT_ALLOWED);
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_NOT_ENOUGH_BUFFER_SPACE);
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_NULL_SETTINGS_PASSED);
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_INVALID_CONFIG_FLAGS_PASSED);
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_USER_NOT_SIGNED_INTO_XBOX_LIVE);
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_NOT_ALLOWED);
                RL_XHTTP_HANDLE_ERROR(XONLINE_E_XAUTH_NOT_STARTED);
            default:
                rlError("[%u]: Error sending request: (%u)", (rqstData?rqstData->m_RequestId:0), ar->dwError);
                break;

            }
#undef RL_XHTTP_HANDLE_ERROR

            if(rqstData)
            {
                rqstData->m_WriteState = rlXhttpRequestData::WS_ERROR;
                rqstData->m_ReadState = rlXhttpRequestData::RS_ERROR;
            }
        }
        break;
    }
}

}   //namespace rage

#endif  //__LIVE
