// 
// rline/rlscpresence.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCPRESENCE_H
#define RLINE_RLSCPRESENCE_H

#include "file/file_config.h"
#include "rline/rl.h"
#include "data/rson.h"
#include "net/net.h"
#include "rline/rlgamerhandle.h"

#if defined(PostMessage)
// windows headers conflict
#undef PostMessage
#endif

/*
    rlScPresence is the entry point to the ROS presence service.
*/

namespace rage
{

class parTree;
class parTreeNode;
class netStatus;
class rlGamerHandle;
class rlPresence;

//PURPOSE
//  Types of attributes we support.
enum rlScPresenceAttributeType
{
    RLSC_PRESTYPE_INVALID,
    RLSC_PRESTYPE_S64,
    RLSC_PRESTYPE_DOUBLE,
    RLSC_PRESTYPE_STRING,
};

//PURPOSE
//  The scope of an attribute name
enum rlScAttributeScope
{
    RLSC_SCOPE_INVALID          = 0x00,
    RLSC_ATTRSCOPE_RESERVED     = 0x01,     //Can't be set by titles
    RLSC_ATTRSCOPE_CROSS_TITLE  = 0x02,     //Used across titles
    RLSC_ATTRSCOPE_TITLE        = 0x04      //Title specific
};

//Maximum number of attributes per player
#if !defined(RLSC_PRESENCE_MAX_ATTRIBUTES)
#define RLSC_PRESENCE_MAX_ATTRIBUTES        60
#endif

//Maximum size of attribute name
#if !defined(RLSC_PRESENCE_ATTR_NAME_MAX_SIZE)
//Includes terminating null
#define RLSC_PRESENCE_ATTR_NAME_MAX_SIZE    64
#endif

//Maximum size of string attribute value
#if !defined(RLSC_PRESENCE_STRING_MAX_SIZE)
//Includes terminating null
#define RLSC_PRESENCE_STRING_MAX_SIZE       256
#endif

//Maximum number of results returned by a query
#define RLSC_PRESENCE_QUERY_MAX_RESULTS     100
//Maximum size of each record returned by a query, including
//the terminating null character.
#define RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE 300

//Maximum number of channels that can be subscribed
#if !defined(RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS)
#define RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS   256
#endif

//Maximum size of channel name
#if !defined(RLSC_PRESENCE_CHANNEL_NAME_MAX_SIZE)
//Includes terminating null
#define RLSC_PRESENCE_CHANNEL_NAME_MAX_SIZE    64
#endif

#if !defined(RLSC_PRESENCE_MESSAGE_MAX_SIZE)
//Includes terminating null
#define RLSC_PRESENCE_MESSAGE_MAX_SIZE 1024
#endif

// If we should use SC Presence to look for duplicate signins
#define RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS (__RGSC_DLL && 1)

// Max sizes For rlScPresence::GetAttributesForGamers()
#define RLSC_PRESENCE_GET_ATTRS_MAX_GAMERS_PER_REQUEST (100)
#define RLSC_PRESENCE_GET_ATTRS_MAX_ATTRS_PER_GAMER (32)

#define RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS (32)

class rlScAttributeId
{
public:
	static const int NUM_SCATTR_TYPES = 31;

    const char* Name;
    unsigned Scope;

    //Reserved - can't be set by clients
    static const rlScAttributeId PrimaryId;
    static const rlScAttributeId PlayerAccountId;
    static const rlScAttributeId RockstarId;
    static const rlScAttributeId OnlineStatus;
    static const rlScAttributeId LastSeen;
    static const rlScAttributeId Publishers;
    static const rlScAttributeId Subscribers;
    static const rlScAttributeId Titles;
    static const rlScAttributeId PresenceServerName;

    //Not reserved - can be set by clients.
    static const rlScAttributeId GamerTag;
    static const rlScAttributeId ScNickname;
    static const rlScAttributeId RelayIp;
    static const rlScAttributeId RelayPort;
    static const rlScAttributeId MappedIp;
    static const rlScAttributeId MappedPort;
    static const rlScAttributeId PeerAddress;
    static const rlScAttributeId CrewId;
    static const rlScAttributeId IsGameHost;
    static const rlScAttributeId GameSessionToken;
	static const rlScAttributeId GameSessionId;
	static const rlScAttributeId GameSessionInfo;
	static const rlScAttributeId IsGameJoinable;
	static const rlScAttributeId IsPartyHost;
    static const rlScAttributeId PartySessionToken;
	static const rlScAttributeId PartySessionId;
    static const rlScAttributeId PartySessionInfo;
	static const rlScAttributeId IsPartyJoinable;
	static const rlScAttributeId HostedMatchIds;
	static const rlScAttributeId SignInTime;
	static const rlScAttributeId BuildVersion;
	static const rlScAttributeId RichPresence;
	static const rlScAttributeId SessionType;

    static const rlScAttributeId* const AllIds[NUM_SCATTR_TYPES];

    static unsigned GetScope(const char* attrId);
};

//PURPOSE
//  Name and value of a presence attribute.
class rlScPresenceAttribute
{
public:

    //Attribute name
    char Name[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE];

    //Attribute value
    union
    {
        s64 IntValue;
        double DoubleValue;
        char StringValue[RLSC_PRESENCE_STRING_MAX_SIZE];
    };

    //Attribute type
    rlScPresenceAttributeType Type;

	//Whether this is currently set or not
	bool m_IsSet;

    rlScPresenceAttribute();

    //PURPOSE
    //  Generic constructor
    template<typename T>
    rlScPresenceAttribute(const char* name,
                            const T value)
    {
        this->Reset(name, value);
    }

    //PURPOSE
    //  Clears the attribute to its initial values, i.e.
    //  empty name, zero value, INVALID type.
    void Clear();

    //PURPOSE
    //  Returns true if the attribute contains a valid value.
    bool IsValid() const;

    //PURPOSE
    //  Resets the name, value, and type of the attribute
    void Reset(const char* name, const s64 value);
    void Reset(const char* name, const double value);
    //Empty string values are ok.
    void Reset(const char* name, const char* value);

    //PURPOSE
    //  Sets the value of the attribute.  The type must have
    //  already been set appropriately and cannot be changed with
    //  this method.  Call Reset() to change the type.
    bool SetValue(const s64 value);
    bool SetValue(const double value);
    //Empty string values are ok.
    bool SetValue(const char* value);

	//PURPOSE
	//  Clears the value of an attribute
	bool ClearValue();

	//PURPOSE
	//  Checks if a value is set
	bool IsSet() const;

    //PURPOSE
    //  Gets the value of the attribute.  The type must have
    //  already been set appropriately and must match the
    //  type of the value parameter.
    bool GetValue(s64* value) const;
    bool GetValue(double* value) const;
    bool GetValue(char* value, const unsigned sizeofValue) const;
    template<int SIZE>
    bool GetValue(char (&value)[SIZE]) const
    {
        return GetValue(value, SIZE);
    }

    bool operator==(const rlScPresenceAttribute& that) const;
    bool operator!=(const rlScPresenceAttribute& that) const
    {
        return !operator==(that);
    }

    bool operator==(const s64 val) const;
    bool operator==(const double val) const;
    bool operator==(const char* val) const;
    bool operator!=(const s64 val) const
    {
        return !operator==(val);
    }
    bool operator!=(const double val) const
    {
        return !operator==(val);
    }
    bool operator!=(const char* val) const
    {
        return !operator==(val);
    }

    //PURPOSE
    //  Helper function to find an attribute by name.
    //  Returns the index of the attribute, or -1 if not found.
    //  Names are compared without regard to case.
    static int FindAttr(const char* name,
                        const rlScPresenceAttribute* attrs,
                        const unsigned numAttrs);
};

class rlScPresenceMessageSender
{
public:
    // message source
    enum Source
    {
        SENDER_UNKNOWN,
        SENDER_SERVER,
        SENDER_PLAYER,
    };
	
#if !__NO_OUTPUT
	static const char* GetSourceName(const Source source)
	{
		switch(source)
		{
		case SENDER_UNKNOWN: return "UNKNOWN"; break;
		case SENDER_SERVER: return "SERVER"; break;
		case SENDER_PLAYER: return "PLAYER"; break;
		}

		netAssertf(false, "Unknown Source: %d", (int)source);
		return "**UNKNOWN SOURCE**";
	}
#endif

    rlScPresenceMessageSender()
    : m_Source(SENDER_UNKNOWN)
    {

    }

    rlScPresenceMessageSender(const rlGamerHandle sender,
                              const Source source)
    : m_GamerHandle(sender)
	, m_Source(source)
    {
        
    }

    //PURPOSE
    //  Returns true if the sender info was provided by the server, false otherwise.
    bool IsAuthenticated() const
    {
        return m_Source != SENDER_UNKNOWN;
    }

    //Gamer handle of the sender if the message is sent by a player.
    rlGamerHandle m_GamerHandle;

    //The source of the message.
    Source m_Source;
};

//PURPOSE
//  Basic container for message contents.
//  A simple char buffer and a timestamp.
class rlScPresenceMessage
{
public:

	rlScPresenceMessage()
        : m_PosixTimeStamp(0)
        , m_Contents(NULL)
    {

    }

	rlScPresenceMessage(const char* contents,
						const u64 posixTimeStamp)
        : m_PosixTimeStamp(posixTimeStamp)
        , m_Contents(contents)
    {
    }

	//PURPOSE
	//  Returns true if the rson reader contains an instance of T
	//EXAMPLE
	//  IsA<lScPresenceMessageMpInvite>(rr);
	template<typename T>
	static bool IsA(const RsonReader& rr)
	{
		RsonReader firstMember;
		return rr.HasName() ? rr.CheckName(T::MSG_ID()) :
			   rr.GetFirstMember(&firstMember) && firstMember.CheckName(T::MSG_ID());
	}

    //PURPOSE
    //  Returns true if the message buffer contains an instance of T
    //EXAMPLE
    //  msgBuf.IsA<lScPresenceMessageMpInvite>();
    template<typename T>
    bool IsA() const
    {
        RsonReader rr(m_Contents, (unsigned)strlen(m_Contents));
		return rlScPresenceMessage::IsA<T>(rr);
    }

    //Time at which message was created.
    //I.e. the time it was put in the queue on the presence server.
    u64 m_PosixTimeStamp;

    //Message contents.
    const char* const m_Contents;
};

//PURPOSE
//  Used to iterate over messages retrieved with rlScPresence::GetMessages()
class rlScPresenceMessageIterator
{
    friend class rlScPresence;
    friend class rlScGetMessagesTask;

public:
    rlScPresenceMessageIterator();

    //PURPOSE
    //  Retrieves the next message in the iteration
    //RETURNS
    //  True if a message was retrieved, false if at the end
    //  of the iteration.
    //NOTES
    //  At the end of the iteration RelaseResources() will be called
    //  to free resources.
    bool NextMessage(const char** message, u64* posixTimeStamp, rlGamerHandle* senderGamerHandle, rlScPresenceMessageSender::Source* source);

    //PURPOSE
    //  Returns the number of messages retrieved by GetMessages()
    unsigned GetNumMessagesRetrieved() const;

    //PURPOSE
    //  Returns the total messages left in the queue after calling
    //  GetMessages().
    unsigned GetTotalMessages() const;

    //PURPOSE
    //  Releases memory used by the iterator.
    //  Be sure to call this when finished with the iterator.
    void ReleaseResources();

    //PURPOSE
    //  Returns true when resources have been released
    bool IsReleased() const;

private:

    enum State
    {
        STATE_NONE,
        STATE_INITIALIZED   //Set when INIT_PARSER is called
    };

    bool Begin();

    static bool NextMessage(const parTreeNode* rootNode,
                            const parTreeNode** msgNode,
                            const char** message,
                            u64* posixTimeStamp,
							rlGamerHandle* senderGamerHandle,
							rlScPresenceMessageSender::Source* source);

    parTree* m_ParserTree;
    const parTreeNode* m_MsgNode;
    unsigned m_NumMessagesRetrieved;
    unsigned m_TotalMessages;

    int m_MessageIndex;

    State m_State;
};

#if SC_PRESENCE
//PURPOSE
//  rlScPresence interface.
//  Attributes for local gamers are stored locally and synchronized
//  with ROS at a regular interval.
//  The server does not store attributes between sign-ins.
//  When a player signs in assume that all attributes need to be set.
class rlScPresence
{
public:

	//PURPOSE
	//  Init/Update/Shutdown
	static bool Init();
	static bool IsInitialized();
	static void Shutdown();
	static void Update();

	static void SetAllowReadingSenderInfo(const bool readSenderInfo);

private:
	// the functions below are called by rlPresence
	friend class rlPresence;

    //PURPOSE
    //  Signs the gamer in locally.
    //  When the presence server becomes available the online state will be
    //  updated.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    static bool SignIn(const int localGamerIndex);

    //PURPOSE
    //  Signs the gamer out of the online presence system.
    //  Still signed in locally.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
	//	bDuplicateSignOut - true if the user was kicked due to duplicate sign out
    static bool SignOffline(const int localGamerIndex, bool bDuplicateSignOut);

    //PURPOSE
    //  Signs the gamer out of the presence system completely
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
	//	bDuplicateSignOut - true if the user was kicked due to duplicate sign out
    static bool SignOut(const int localGamerIndex, bool bDuplicateSignOut);

    //PURPOSE
    //  Returns true if the local gamer is signed in locally
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    static bool IsSignedInLocally(const int localGamerIndex);

    //PURPOSE
    //  Returns true if the local gamer is signed in online to the presence system
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    static bool IsSignedOnline(const int localGamerIndex);

	//PURPOSE
	//  Returns true if the local gamer is signed into the
	//  presence system and the service is ready for use.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
	static bool IsServiceReady(int localGamerIndex);

    //PURPOSE
    //  Clears the local copy of attributes for the gamer at the given
    //  index.  Cleared attributes are not synchronized to ROS.
    //  This is typically called when a player signs out.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    static void ClearAttributes(const int localGamerIndex);

    //PURPOSE
    //  Sets the local copy of the attribute.
    //  The new value will be synchronized with ROS a short
    //  time after.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  name            - Attribute name
    //  value           - Attribute value
	//  bNeedsCommitIfDiry - specify if this attribute needs to be committed as soon 
	//							as it's dirty.  
	//						
    //RETURNS
    //  True for success.  The method will succeed as long as the named
    //  attribute already exists, or it can be created without exceeding 
    //  RL_SCPRESENCE_MAX_ATTRIBUTES.
    static bool SetIntAttribute(const int localGamerIndex,
                                const char* name,
                                const s64 value,
								const bool bNeedsCommitIfDirty);
    static bool SetDoubleAttribute(const int localGamerIndex,
                                    const char* name,
									const double value,
									const bool bNeedsCommitIfDirty);
    static bool SetStringAttribute(const int localGamerIndex,
                                    const char* name,
									const char* value,
									const bool bNeedsCommitIfDirty);

	//PURPOSE
	//  Clears an attribute
	//PARAMS
	//  localGamerIndex - Index of local gamer making the call.
	//  name            - Attribute name
	//  value           - Attribute value						
	static bool ClearAttribute(const int localGamerIndex, const char* name);

	//PURPOSE
	//  Returns whether this attribute has been set or not
	//PARAMS
	//  localGamerIndex - Index of local gamer making the call.
	//  name            - Attribute name
	//  value           - Attribute value
	//						
	static bool HasAttribute(const int localGamerIndex, const char* name);
	static bool IsAttributeSet(const int localGamerIndex, const char* name);

    //PURPOSE
    //  Forces a refresh of all attributes to the server
    static void ForceRefresh(const int localGamerIndex);

    //PURPOSE
    //  Retrieves the local copy of the attribute.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  name            - Attribute name
    //  value           - Will be populated with the attribute value
    //RETURNS
    //  True for success, i.e. if the attribute exists.
    static bool GetIntAttribute(const int localGamerIndex,
                                const char* name,
                                s64* value);
    static bool GetDoubleAttribute(const int localGamerIndex,
                                    const char* name,
                                    double* value);
    static bool GetStringAttribute(const int localGamerIndex,
                                    const char* name,
                                    char* value,
                                    const unsigned sizeofValue);
    template<int SIZE>
    static bool GetStringAttribute(const int localGamerIndex,
                                    const char* name,
                                    char (&value)[SIZE])
    {
        return GetStringAttribute(localGamerIndex, name, value, SIZE);
    }

    //PURPOSE
    //  Retrieves attributes for the gamer identified by the gamer handle.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  gamerHandle - Identifies the gamer for whom to retrieve attributes.
    //  attrs       - Array of attributes to retrieve.  Each item in the
    //                array must have its Name member set.
    //  numAttrs    - Number of attributes in the attrs array
    //  status      - Can be polled for completion
    //NOTES
    //  Upon completion attributes not retrieved will have their Type members
    //  set to INVALID.
    //
    //  This is an asynchronous operation.  Don't deallocate attrs
    //  while the operation is pending.
    static bool GetAttributesForGamer(const int localGamerIndex,
                                    const rlGamerHandle& gamerHandle,
                                    rlScPresenceAttribute* attrs,
                                    const unsigned numAttrs,
                                    netStatus* status);

    //PURPOSE
    //  Retrieves attributes for multiple gamers identified by gamer handles.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  gamerHandles    - Identifies the gamers for whom to retrieve attributes.
	//  numGamerHandles - Number of gamer handles in the gamerHandles array
	//					  Must be <= RLSC_PRESENCE_GET_ATTRS_MAX_GAMERS_PER_REQUEST.
    //  attrs           - 2D Array of attributes to retrieve for each gamer handle.
	//					  (Array of N arrays of M attributes, where N = numGamerHandles
	//					  and M = numAttrs.)
    //                    Each item in the array must have its Name and Type set.
	//					  The set of attributes to retrieve must be the same
	//					  for all gamer handles.
    //  numAttrs        - Number of attributes in the attrs array
	//					  Must be <= RLSC_PRESENCE_GET_ATTRS_MAX_ATTRS_PER_GAMER.
    //  status          - Can be polled for completion
    //NOTES
    //  Upon completion attributes not retrieved will have their Type members
    //  set to INVALID.
	//
	//  It is possible to have no attributes retrieved for some or all of the 
	//  specified gamer handles and still have the task succeed. Always check
	//  the Type of each returned attribute is not INVALID before using them.
    //
    //  This is an asynchronous operation.  Don't deallocate attrs
    //  while the operation is pending.
    static bool GetAttributesForGamers(const int localGamerIndex,
										const rlGamerHandle* gamerHandles,
										const unsigned numGamerHandles,
										rlScPresenceAttribute* attrs[],
										const unsigned numAttrs,
										netStatus* status);

    //PURPOSE
    //  Subscribe to one or more message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  channels    - List of channels to subscribe to.
    //  numChannels - Number of channels in the list.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate status
    //  while the operation is pending.
    static bool Subscribe(const int localGamerIndex,
                            const char** channels,
                            const unsigned numChannels);

    //PURPOSE
    //  Unsubscribe from one or more message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  channels    - List of channels to unsubscribe from.
    //  numChannels - Number of channels in the list.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate status
    //  while the operation is pending.
    static bool Unsubscribe(const int localGamerIndex,
                            const char** channels,
                            const unsigned numChannels);

    //PURPOSE
    //  Unsubscribes from all subscribed message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    static bool UnsubscribeAll(const int localGamerIndex);

    //PURPOSE
    //  Publish a message to one or more message channels.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  channels    - Optional.  List of channels to publish to.
    //  numChannels - Number of channels in the list.
    //  filterName  - Optional.  Name of filter that can be
    //                used to perform additional filtering on
    //                message recipients.
    //  paramNameValueCsv   - Filter parameters in CSV format
    //                        Parameter names alternate with values
    //                        Names must be prefixed with '@', as in @playerid.
    //  message     - The message.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate status
    //  while the operation is pending.
    //EXAMPLE:
    //  //Publish a message to friends who are in my crew.
    //  char filterParams[256];
    //  formatf(filterParams, "@crewid,%d", myCrewId);
    //  char channel[256];
    //  formatf(channel, "friend_%s", myGamerHandleStr);
    //  rlPresence::Publish(myGamerIndex,
    //                      &channel,
    //                      1,
    //                      "CrewmatesOnline",
    //                      filterParams,
    //                      message);
    static bool Publish(const int localGamerIndex,
                        const char** channels,
                        const unsigned numChannels,
                        const char* filterName,
                        const char* paramNameValueCsv,
                        const char* message);

    //PURPOSE
    //  Posts a message to the recipients' message queues.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //  recipients      - Message recipients
    //  numRecipients   - Number of recipients
    //  message         - The message
    //  status          - Optional.  Can be polled for completion
    static bool PostMessage(const int localGamerindex,
                            const rlGamerHandle* recipients,
                            const unsigned numRecipients,
                            const char* message,
                            const unsigned ttlSeconds);

    //PURPOSE
    //  Variant of PostMessage that automatically serializes
    //  an instance of T into the message.
    template<typename T>
    static bool PostMessage(const int localGamerindex,
                            const rlGamerHandle* recipients,
                            const unsigned numRecipients,
                            const T& t,
                            const unsigned ttlSeconds)
    {
        u8 msgBuf[1024];
        return t.Export(msgBuf)
                && PostMessage(localGamerindex, recipients, numRecipients, msgBuf, ttlSeconds);
    }

    //PURPOSE
    //  Retrieves messages from the message queue owned by the gamer
    //  identified at localGamerIndex.
    //PARAMS
    //  localGamerIndex     - Index of local gamer making the call.
    //  iter                - Upon completion will be initialized as a message iterator
    //  status              - Can be polled for completion
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate iter
    //  or status until the operation completes.
    //
    //  Be sure to call ReleaseResources() on the iterator when finished.
    static bool GetMessages(const int localGamerIndex,
                            const unsigned numMessages,
                            rlScPresenceMessageIterator* iter,
                            netStatus* status);

    //PURPOSE
    //  Runs a predefined query on the presence database and returns
    //  the results.
    //  Results are JSON representations of presence records.  Example:
    //      {"_id":"XBL 1234567890","gtag":"FurBuddy","crewid":776655}
    //PARAMS
    //  localGamerIndex     - Index of local gamer making the call.
    //  queryName           - Name of predefined query
    //  paramNameValueCsv   - Query parameters in CSV format
    //                        Parameter names alternate with values.
    //                        Names must be prefixed with '@', as in @playerid.
    //                          Example: @param1,value1,@param2,value2
    //                        To pass a list of values separate each value by '&'.
    //                          Example: @listParam,value1&value2&value3
    //  offset              - Offset into results at which to begin returning
    //                        results.
    //  count               - Number of results to return.
    //  recordsBuf          - Buffer to hold records returned
    //  sizeofRecordsBuf    - Size of recordsBuf.
    //  records             - Array of char pointers that will be populated with
    //                        records returned from the query.  It must be large
    //                        enough to contain "count" results.  Each string in
    //                        the results array points to a section of the
    //                        resultsBuf memory buffer.
    //  numRecordsRetrieved - Upon completion will contain the number of records
    //                        actually retrieved.
    //  numRecords          - Upon completion will contain the number of records
    //                        parsed.  If recordsBuf is too small numRecords will be
    //                        less than numRecordsRetrieved.
    //  status              - Can be polled for completion
    //NOTES
    //  Records are limited to RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE bytes each.
    //  Each record is returned as a null terminated string.  The recordsBuf
    //  parameter should be large enough to hold
    //  (count * RLSC_PRESENCE_QUERY_MAX_RECORD_SIZE) chars.  
    //  This is an asynchronous operation.  Do not deallocate recordBuf,
    //  records, numRecords, or status while the operation is pending.
    //EXAMPLE:
    //  //Find a player's session token.
    //  char queryParams[256];
    //  formatf(queryParams, "@gamerhandle,'%s'", gamerHandlStr);
    //  rlPresence::Query("FindSessionTokenByGamerHandle",
    //                      queryParams,
    //                      0,  //offset
    //                      1,  //count
    //                      m_PresQueryBuf,
    //                      sizeof(m_QueryBuf),
    //                      m_QueryRecords,
    //                      &m_NumQueryRecordsRetrieved,
    //                      &m_NumQueryRecords,
    //                      &m_Status);
    //
    //  //When it completes parse the results which look like "{'gstok':123456789}"
    //  RsonReader rr(m_QueryRecords[0], strlen(m_QueryRecords[0]);
    //  s64 gstok;
    //  rr.ReadInt64("gstok", gstok);
    //  
    static bool Query(const int localGamerindex,
                        const char* queryName,
                        const char* paramNameValueCsv,
                        const int offset,
                        const int count,
                        char* recordsBuf,
                        const unsigned sizeofRecordsBuf,
                        char** records,
                        unsigned* numRecordsRetrieved,
                        unsigned* numRecords,
                        netStatus* status);

    //PURPOSE
    //  Runs a predefined query on the presence database and returns
    //  the number of records that match the query.
    //  queryName           - Name of predefined query
    //  paramNameValueCsv   - Query parameters in CSV format
    //                        Parameter names alternate with values
    //                        Names must be prefixed with '@', as in @playerid.
    //  count               - Upon completion will contain the number of records
    //                        that match the query.
    //PARAMS
    //  localGamerIndex - Index of local gamer making the call.
    //NOTES
    //  This is an asynchronous operation.  Do not deallocate count,
    //  or status while the operation is pending.
    static bool QueryCount(const int localGamerindex,
                        const char* queryName,
                        const char* paramNameValueCsv,
                        unsigned* count,
                        netStatus* status);

    //PURPOSE
    //  Cancels the asynchronous operation associated with the status object.
    static void Cancel(netStatus* status);

private:

    static bool CommitState(const int localGamerIndex);

    static bool CommitAttributes(const int localGamerIndex);

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
	static void DuplicateSigninInit();
	static void DuplicateSigninCheck(const int localGamerIndex);
	static void DuplicateSigninRecheck(const int localGamerIndex);
	static void DuplicateSigninReset(const int localGamerIndex);
	static void DuplicateSigninUpdate(const int localGamerIndex);

	static netStatus sm_DuplicateSigninCheckStatus[RL_MAX_LOCAL_GAMERS];
	static rlScPresenceAttribute sm_SigninTimeAttrs[RL_MAX_LOCAL_GAMERS];
	static bool sm_SecondDuplicateCheck[RL_MAX_LOCAL_GAMERS];
#endif

};
#endif

} //namespace rage

#endif  //RLINE_RLSCPRESENCE_H
