// 
// rline/rlscpresencetasks.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscpresencetasks.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "rlscpresence.h"
#include "rlscpresencemessage.h"
#include "rline/ros/rlros.h"
#include "rline/rltitleid.h"
#include "system/nelem.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, scpresence)
#undef __rage_channel
#define __rage_channel rline_scpresence

extern const rlTitleId* g_rlTitleId;

///////////////////////////////////////////////////////////////////////////////
// rlScSignOutTask
///////////////////////////////////////////////////////////////////////////////

rlScSignOutTask::rlScSignOutTask()
{
}

rlScSignOutTask::~rlScSignOutTask()
{
}

bool
rlScSignOutTask::Configure(const int localGamerIndex)
{
    if(!rlTaskVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        rlTaskError("Invalid gamer index: %d", localGamerIndex);
        return false;
    }

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    return true;
}

bool 
rlScSignOutTask::ProcessSuccess(const rlRosResult& /*result*/,
                                const parTreeNode* OUTPUT_ONLY(node),
                                int& /*resultCode*/)
{
    rlTaskAssert(node);
    return true;
}

const char*
rlScSignOutTask::GetServiceMethod() const
{
    return "Presence.asmx/SignOut";
}

///////////////////////////////////////////////////////////////////////////////
// rlScCommitPresenceTask
///////////////////////////////////////////////////////////////////////////////
rlScCommitPresenceTask::rlScCommitPresenceTask()
: m_CommitType(COMMIT_INVALID)
{
}

rlScCommitPresenceTask::~rlScCommitPresenceTask()
{
}

bool
rlScCommitPresenceTask::Configure(
	const int localGamerIndex,
	const rlScPresenceAttribute* attrs,
	const unsigned numAttrs,
	const CommitType commitType)
{
    rlTaskDebug2("%s presence for gamer: %d with %d attributes...",
                (COMMIT_MODIFY == commitType) ? "Modifying" : "Replacing",
                localGamerIndex,
                numAttrs);

    if(!rlTaskVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        rlTaskError("Invalid gamer index: %d", localGamerIndex);
        return false;
    }

	if(!rlTaskVerify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE)))
	{
		rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex);
		return false;
	}

    if(!rlTaskVerify(numAttrs > 0))
    {
        rlTaskError("No attributes to commit");
        return false;
    }

    //Set the commit type before calling the base class's Configure
    //so when the base class calls GetServiceMethod() it will return
    //the right thing.
    m_CommitType = commitType;

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    if(!rlTaskVerify(BeginListParameter("typeNameValueCsv")))
    {
        rlTaskError("Failed to add \"typeNameValueCsv\" parameter");
        return false;
    }

    for(int i = 0; i < (int)numAttrs; ++i)
    {
        //We send up doubles as ints so precision isn't lost when
        //converting to strings.

        const rlScPresenceAttribute& attr = attrs[i];

		const char* typeStr = nullptr;

		if(attr.IsSet())
		{
			if(RLSC_PRESTYPE_STRING == attr.Type)
			{
				typeStr = "s";
			}
			else if(RLSC_PRESTYPE_DOUBLE == attr.Type)
			{
				//Doubles are sent up as ints to preserve precision
				if(attr.IntValue < INT_MAX && attr.IntValue > INT_MIN)
				{
					typeStr = "f";
				}
				else
				{
					typeStr = "D";
				}
			}
			else if(RLSC_PRESTYPE_S64 == attr.Type)
			{
				if(attr.IntValue < INT_MAX && attr.IntValue > INT_MIN)
				{
					typeStr = "i";
				}
				else
				{
					typeStr = "I";
				}
			}
		}

		// if this wasn't set, this is either a clear or an invalid type - in both cases, use a string type
		// an invalid type generates an assert below when we commit the value
		if(typeStr == nullptr)
		{
			// clearing an attribute requires a blank string and string type (using a blank string for a number type will generate a format error)
			typeStr = "s";
		}

        if(!rlTaskVerify(AddListParamStringValue(typeStr)))
        {
            rlTaskError("Failed to add type for attribute: \"%s\"", attr.Name);
            return false;
        }

        const unsigned attrScope = rlScAttributeId::GetScope(attr.Name);
        if(!rlTaskVerify(!(attrScope & RLSC_ATTRSCOPE_RESERVED)))
        {
            rlTaskError("\"%s\" is reserved", attr.Name);
            return false;
        }

        if(!rlTaskVerify(AddListParamStringValue(attr.Name)))
        {
            rlTaskError("Failed to add name for attribute: \"%s\"", attr.Name);
            return false;
        }
		
		if(attr.IsSet())
		{
			switch(attr.Type)
			{
				//Doubles are sent up as ints to preserve precision
			case RLSC_PRESTYPE_S64:
			case RLSC_PRESTYPE_DOUBLE:
				if(!rlTaskVerify(AddListParamIntValue(attr.IntValue)))
				{
					rlTaskError("Failed to add value for attribute: \"%s\"", attr.Name);
					return false;
				}
				break;
			case RLSC_PRESTYPE_STRING:
				if(!rlTaskVerify(AddListParamStringValue(attr.StringValue)))
				{
					rlTaskError("Failed to add value for attribute: \"%s\"", attr.Name);
					return false;
				}
				break;
			default:
				rlTaskAssertf(0, "Invalid attribute type (%d) for attribute: %s", attr.Type, attr.Name);
				return false;
			}
		}
		else
		{
			if(!rlTaskVerify(AddListParamStringValue("")))
			{
				rlTaskError("Failed to add clear for attribute: \"%s\"", attr.Name);
				return false;
			}

			rlTaskDebug1("Attribute %s is marked to be cleared, sending empty value", attr.Name);
		}
    }

    return true;
}

bool 
rlScCommitPresenceTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* OUTPUT_ONLY(node),
                                    int& /*resultCode*/)
{
    rlTaskAssert(node);
    return true;
}

const char*
rlScCommitPresenceTask::GetServiceMethod() const
{
    rlTaskAssert(COMMIT_MODIFY == m_CommitType || COMMIT_REPLACE == m_CommitType);

    return (COMMIT_MODIFY == m_CommitType)
            ? "Presence.asmx/SetAttributes"
            : "Presence.asmx/ReplaceAttributes";
}

///////////////////////////////////////////////////////////////////////////////
// rlScGetPresenceTask
///////////////////////////////////////////////////////////////////////////////
rlScGetPresenceTask::rlScGetPresenceTask()
: m_Attrs(NULL)
, m_NumRequested(0)
{
}

rlScGetPresenceTask::~rlScGetPresenceTask()
{
}

bool
rlScGetPresenceTask::Configure(const int localGamerIndex,
                                const rlGamerHandle& gamerHandle,
                                rlScPresenceAttribute* attrs,
                                const unsigned numAttrs)
{
    if(!rlTaskVerify(gamerHandle.IsValidForRos()))
    {
        rlTaskError("Invalid gamer handle");
        return false;
    }

	if(!rlTaskVerify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE)))
	{
		rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex);
		return false;
	}

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
    if(!rlTaskVerify(gamerHandle.ToString(ghStr)))
    {
        rlTaskError("Error retrieving gamer handle string");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("gamerHandle", ghStr, true)))
    {
        rlTaskError("Failed to add gamerHandle parameter");
        return false;
    }

    if(!rlTaskVerify(BeginListParameter("namesCsv")))
    {
        rlTaskError("Failed to add \"namesCsv\" parameter");
        return false;
    }

    m_Attrs = attrs;
    m_NumRequested = numAttrs;

    for(int i = 0; i < (int)m_NumRequested; ++i)
    {
        const int len = istrlen(attrs[i].Name);
        if(0 == len)
        {
            rlTaskError("Empty attribute name");
            return false;
        }
        else if(len > COUNTOF(attrs[i].Name))
        {
            rlTaskError("Attribute name \"%s\" is too long", attrs[i].Name);
            return false;
        }
        else if(RLSC_PRESTYPE_S64 != attrs[i].Type
                && RLSC_PRESTYPE_DOUBLE != attrs[i].Type
				&& RLSC_PRESTYPE_STRING != attrs[i].Type)
        {
            rlTaskError("Invalid attribute type: %d on \"%s\"",
                        attrs[i].Type,
                        attrs[i].Name);
            return false;
        }

        OUTPUT_ONLY(char ghStr[RL_MAX_GAMER_HANDLE_CHARS]);
        rlTaskDebug2("Requesting presence attribute \"%s\" for %s",
                    attrs[i].Name,
                    gamerHandle.ToString(ghStr));

        if(!rlTaskVerify(AddListParamStringValue(attrs[i].Name)))
        {
            rlTaskError("Failed to add request parameters");
            return false;
        }
    }

    return true;
}

bool
rlScGetPresenceTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* node,
                                    int& /*resultCode*/)
{
    rlTaskAssert(node);

    rtry
    {
        const parTreeNode* nvCsvNode = node->FindChildWithName("NameValueCsv");
        rcheck(nvCsvNode, catchall, 
                rlTaskError("Failed to find <NameValueCsv> element"));

        const parTreeNode* countNode = node->FindChildWithName("Count");
        rcheck(countNode, catchall, 
                rlTaskError("Failed to find <Count> element"));

        unsigned count;
        const char* countStr = countNode->GetData();
        rverify(1 == sscanf(countStr, "%u", &count),
                catchall,
                rlTaskError("Failed to parse <Count> element: %s", countStr));

        rlTaskDebug2("Retrieved %d presence attributes", count);

        rverify(count <= m_NumRequested,
                catchall,
                rlTaskError("Number of attributes returned (%u) is more than requested (%u)",
                            count,
                            m_NumRequested));

        const char* csv = nvCsvNode->GetData();
        bool done = false;

        //Attributes that were retrieved will have their types set
        //back to the correct values.  Those not retrieved will have
        //type INVALID.

        //Copy the expected types to a temp array, and set
        //the original types to INVALID.
        rlScPresenceAttributeType* attrTypes =
            Alloca(rlScPresenceAttributeType, m_NumRequested);

        for(int i = 0; i < (int)m_NumRequested; ++i)
        {
            attrTypes[i] = m_Attrs[i].Type;
            m_Attrs[i].Type = RLSC_PRESTYPE_INVALID;
        }

        for(int i = 0; i < (int)count && !done; ++i)
        {
            //Parse the name and value.
            //The format of the string is:
            //  name,value,name,value,...
			char nameStr[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE], valueStr[RLSC_PRESENCE_STRING_MAX_SIZE];
            unsigned bufLen;
            bufLen = sizeof(nameStr);

            //Get the name.
            done = parMemberArray::ReadCsvDatum(nameStr, &bufLen, csv);
            rverify(!done,
                    catchall,
                    rlTaskError("Response is corrupted"));

            //Find the attribute with that name.
            const int attrIdx =
                rlScPresenceAttribute::FindAttr(nameStr, m_Attrs, m_NumRequested);

            rverify(attrIdx >= 0,
                    catchall,
                    rlTaskError("Server returned \"%s\" which wasn't requested", nameStr));

            //Get the value.
            bufLen = sizeof(valueStr);
            done = parMemberArray::ReadCsvDatum(valueStr, &bufLen, csv);

            if(RLSC_PRESTYPE_S64 == attrTypes[attrIdx])
            {
                //Parse the value
                rverify(1 == sscanf(valueStr, "%" I64FMT "d", &m_Attrs[attrIdx].IntValue),
                        catchall,
                        rlTaskError("Failed to parse integer attribute value: %s(%s)",
                                    m_Attrs[attrIdx].Name,
                                    valueStr));

                rlTaskDebug2("%s = %" I64FMT "d",
                            m_Attrs[attrIdx].Name, m_Attrs[attrIdx].IntValue);

                //Restore the attribute type.
                m_Attrs[attrIdx].Type = attrTypes[attrIdx];
            }
            else if(RLSC_PRESTYPE_DOUBLE == attrTypes[attrIdx])
            {
                //See rlScCommitPresenceTask for how we store doubles as their
                //integer representation so precision isn't lost when converting
                //to strings.
                rverify(1 == sscanf(valueStr, "%" I64FMT "d", &m_Attrs[attrIdx].IntValue),
                        catchall,
                        rlTaskError("Failed to parse double attribute value: %s(%s)",
                                    m_Attrs[attrIdx].Name,
                                    valueStr));

                rlTaskDebug2("%s = %g",
                            m_Attrs[attrIdx].Name, m_Attrs[attrIdx].DoubleValue);

                //Restore the attribute type.
                m_Attrs[attrIdx].Type = attrTypes[attrIdx];
            }
			else if(RLSC_PRESTYPE_STRING == attrTypes[attrIdx])
			{
				safecpy(m_Attrs[attrIdx].StringValue, valueStr);
				rlTaskDebug2("%s = %s",
					m_Attrs[attrIdx].Name, m_Attrs[attrIdx].StringValue);

				//Restore the attribute type.
				m_Attrs[attrIdx].Type = attrTypes[attrIdx];
			}
			else
            {
                rthrow(catchall,
                        rlTaskError("Invalid attribute type: %d on \"%s\"",
                                    attrTypes[attrIdx],
                                    m_Attrs[attrIdx].Name));
            }
        }
    }
    rcatchall
    {
        return false;
    }

    return true;
}

const char*
rlScGetPresenceTask::GetServiceMethod() const
{
    return "Presence.asmx/GetAttributes";
}

///////////////////////////////////////////////////////////////////////////////
// rlScGetPresenceForGamersTask
///////////////////////////////////////////////////////////////////////////////
rlScGetPresenceForGamersTask::rlScGetPresenceForGamersTask()
: m_ResultCount(0)
, m_NumGamerHandles(0)
, m_CurCount(0)
, m_CurGamerHandle(0)
, m_HaveCurGamerHandle(false)
, m_HaveCurAttrCount(false)
, m_NumAttrsRequested(0)
, m_GamerHandles(NULL)
, m_Attrs(NULL)
{
	for(unsigned i = 0; i < COUNTOF(m_AttrTypes); ++i)
	{
		m_AttrTypes[i] = (u8)rlScPresenceAttributeType::RLSC_PRESTYPE_INVALID;
	}
}

rlScGetPresenceForGamersTask::~rlScGetPresenceForGamersTask()
{

}

bool
rlScGetPresenceForGamersTask::Configure(const int localGamerIndex,
										const rlGamerHandle* gamerHandles,
										const unsigned numGamerHandles,
										rlScPresenceAttribute** attrs,
										const unsigned numAttrs)
{
	rtry
	{
		rverify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE), catchall, rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex));
		rverify((numGamerHandles > 0) && gamerHandles, catchall, rlTaskError("No gamer handles specified"));
		rverify(numGamerHandles <= RLSC_PRESENCE_GET_ATTRS_MAX_GAMERS_PER_REQUEST, catchall, rlTaskError("Too many gamer handles specified"));
		rverify((numAttrs > 0) && attrs, catchall, rlTaskError("No attributes requested"));
		rverify(numAttrs <= RLSC_PRESENCE_GET_ATTRS_MAX_ATTRS_PER_GAMER, catchall, rlTaskError("Too many attributes specified"));
		rcheck(rlRosHttpTask::Configure(localGamerIndex, &m_RosSaxReader), catchall, rlTaskError("Failed to configure base class"));

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("Credentials are invalid"));

		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, rlTaskError("Failed to add \"ticket\" parameter"));

		rverify(BeginListParameter("gamerHandleCsv"), catchall, rlTaskError("Failed to add \"gamerHandleCsv\" parameter"));

		m_NumGamerHandles = numGamerHandles;
		m_GamerHandles = gamerHandles;
		m_Attrs = attrs;
		m_NumAttrsRequested = numAttrs;

		for(unsigned i = 0; i < m_NumGamerHandles; ++i)
		{
            // validate that the handle is valid at all
			rverify(gamerHandles[i].IsValid(), catchall, rlTaskError("Invalid gamer handle"));

            // only send if the gamer is valid for ros
            if(gamerHandles[i].IsValidForRos())
            {
                char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
                rverify(gamerHandles[i].ToString(ghStr), catchall, rlTaskError("Error retrieving gamer handle string"));
                rverify(AddListParamStringValue(ghStr), catchall, rlTaskError("Failed to add gamer handle"));
            }
		}

		rverify(BeginListParameter("namesCsv"), catchall, rlTaskError("Failed to add \"namesCsv\" parameter"));

		rlScPresenceAttribute* a = m_Attrs[0];

		for(unsigned i = 0; i < m_NumAttrsRequested; ++i)
		{
			const int len = istrlen(a[i].Name);

			rverify(len > 0, catchall, rlTaskError("Empty attribute name"));
			rverify(len < COUNTOF(a[i].Name), catchall, rlTaskError("Attribute name \"%s\" is too long", a[i].Name));
			rverify(RLSC_PRESTYPE_S64 == a[i].Type
				|| RLSC_PRESTYPE_DOUBLE == a[i].Type
				|| RLSC_PRESTYPE_STRING == a[i].Type, catchall, rlTaskError("Invalid attribute type: %d on \"%s\"", a[i].Type, a[i].Name));

			rverify(AddListParamStringValue(a[i].Name), catchall, rlTaskError("Failed to add request parameters"));
			
			// as a memory optimization, the original attribute types are stored in an array of u8
			rverify((unsigned)a[i].Type < (sizeof(m_AttrTypes[i]) << 3), catchall, rlTaskError("Empty attribute name"));

			m_AttrTypes[i] = (u8)a[i].Type;
		}

		// clear all the result attribute types to invalid. Any attributes not returned
		// by the service will be returned with an invalid type.
		for(unsigned i = 0; i < m_NumGamerHandles; ++i)
		{
			for(unsigned j = 0; j < m_NumAttrsRequested; ++j)
			{
				rverify(strcmp(m_Attrs[i][j].Name, a[j].Name) == 0, catchall, rlTaskError("The set of attributes to retrieve must be the same for all gamer handles"));
				rverify(m_Attrs[i][j].Type == (rlScPresenceAttributeType)m_AttrTypes[j], catchall, rlTaskError("The set of attributes to retrieve must be the same for all gamer handles"));

				m_Attrs[i][j].Type = RLSC_PRESTYPE_INVALID;
			}
		}

		m_State = STATE_EXPECT_RESULT;
		m_ResultCount = 0;
		m_CurCount = 0;
		m_CurGamerHandle = 0;

		m_HaveCurGamerHandle = false;
		m_HaveCurAttrCount = false;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool 
rlScGetPresenceForGamersTask::SaxStartElement(const char* name)
{
    switch((int)m_State)
    {
    case STATE_EXPECT_RESULT:
        if(!stricmp("result", name))
        {
            m_State = STATE_READ_RESULT_ATTRIBUTES;
        }
        break;
    
    case STATE_EXPECT_ATTRIBUTES_LIST:
        if(!stricmp("a", name))
        {
            m_State = STATE_READ_ATTRIBUTES_FOR_GAMER;
        }
        break;
    }
    return true;
}

bool 
rlScGetPresenceForGamersTask::SaxAttribute(const char* name, const char* val)
{
	/*
		<Result Count="3">
			<a gh="SC 2923723" c="2" csv="gsjoin,1,gsinfo,34d912d6-95ab-4cff-8d79-607375c6f8e0">
			<a gh="SC 3111565456" c="3" csv="gsjoin,1,gsinfo,34d912d6-95ab-4cff-8d79-607375c6f8e0,psjoin,0">
			<a gh="SC 456123489" c="0" csv=""> --> or omitted altogether
		</Result>
	*/

    switch((int)m_State)
    {
    case STATE_READ_RESULT_ATTRIBUTES:
        if(!stricmp("count", name))
        {
            if(sscanf(val, "%d", &m_ResultCount) != 1)
            {
                rlTaskError("Failed to parse count from %s", val);
                return false;
            }

			m_State = STATE_EXPECT_ATTRIBUTES_LIST;
        }
        break;

    case STATE_READ_ATTRIBUTES_FOR_GAMER:
        {
            // gamer handle
            if(!stricmp("gh", name))
            {
				rlGamerHandle gh;
				if(!gh.FromString(val))
				{
					rlTaskError("Failed to parse gamerhandle from %s", val);
					return false;
				}

				bool found = false;
				for(unsigned i = 0; i < m_NumGamerHandles; ++i)
				{
					if(m_GamerHandles[i] == gh)
					{
						m_CurGamerHandle = i;
						m_HaveCurGamerHandle = true;
						found = true;
						break;
					}
				}

				if(!found)
				{
					rlTaskError("Server returned attibutes for gamerhandle \"%s\" which wasn't requested", val);
					return false;
				}
            }
			// count of attributes for this gamer handle
			else if(!stricmp("c", name))
			{
				if(sscanf(val, "%d", &m_CurCount) != 1)
				{
					rlTaskError("Failed to parse count from %s", val);
					return false;
				}

				m_HaveCurAttrCount = true;
			}
            // attribute name-values pairs for this gamer handle
            else if(!stricmp("csv", name))
            {
				rtry
				{
					rverify(m_HaveCurGamerHandle && m_HaveCurAttrCount, catchall, rlError("Gamer handle and count xml attributes must come before attribute list"));

					const char* csv = val;
					bool done = false;

					rlScPresenceAttribute* attrs = &m_Attrs[m_CurGamerHandle][0];

#if !__NO_OUTPUT
					char buf[RL_MAX_GAMER_HANDLE_CHARS] = {0};
					rlTaskDebug2("Attributes for %s", m_GamerHandles[m_CurGamerHandle].ToString(buf));
#endif
					for(unsigned i = 0; i < m_CurCount && !done; ++i)
					{
						//Parse the name and value.
						//The format of the string is:
						//  name,value,name,value,...
						char nameStr[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE];
						char valueStr[RLSC_PRESENCE_STRING_MAX_SIZE];
						unsigned bufLen = sizeof(nameStr);

						//Get the name.
						done = parMemberArray::ReadCsvDatum(nameStr, &bufLen, csv);
						rverify(!done,
								catchall,
								rlTaskError("Response is corrupted"));

						//Find the attribute with that name.
						const int attrIdx =
							rlScPresenceAttribute::FindAttr(nameStr, attrs, m_NumAttrsRequested);

						rverify(attrIdx >= 0,
								catchall,
								rlTaskError("Server returned \"%s\" which wasn't requested", nameStr));

						//Get the value.
						bufLen = sizeof(valueStr);
						done = parMemberArray::ReadCsvDatum(valueStr, &bufLen, csv);

						if(RLSC_PRESTYPE_S64 == m_AttrTypes[attrIdx])
						{
							//Parse the value
							rverify(1 == sscanf(valueStr, "%" I64FMT "d", &attrs[attrIdx].IntValue),
									catchall,
									rlTaskError("Failed to parse integer attribute value: %s(%s)",
												attrs[attrIdx].Name,
												valueStr));

							rlTaskDebug2("    %s = %" I64FMT "d",
										attrs[attrIdx].Name, attrs[attrIdx].IntValue);

							//Restore the attribute type.
							attrs[attrIdx].Type = (rlScPresenceAttributeType)m_AttrTypes[attrIdx];
						}
						else if(RLSC_PRESTYPE_DOUBLE == (rlScPresenceAttributeType)m_AttrTypes[attrIdx])
						{
							//See rlScCommitPresenceTask for how we store doubles as their
							//integer representation so precision isn't lost when converting
							//to strings.
							rverify(1 == sscanf(valueStr, "%" I64FMT "d", &attrs[attrIdx].IntValue),
									catchall,
									rlTaskError("Failed to parse double attribute value: %s(%s)",
												attrs[attrIdx].Name,
												valueStr));

							rlTaskDebug2("    %s = %g", attrs[attrIdx].Name, attrs[attrIdx].DoubleValue);

							//Restore the attribute type.
							attrs[attrIdx].Type = (rlScPresenceAttributeType)m_AttrTypes[attrIdx];
						}
						else if(RLSC_PRESTYPE_STRING == (rlScPresenceAttributeType)m_AttrTypes[attrIdx])
						{
							safecpy(attrs[attrIdx].StringValue, valueStr);
							rlTaskDebug2("    %s = %s", attrs[attrIdx].Name, attrs[attrIdx].StringValue);

							//Restore the attribute type.
							attrs[attrIdx].Type = (rlScPresenceAttributeType)m_AttrTypes[attrIdx];
						}
						else
						{
							rthrow(catchall,
									rlTaskError("Invalid attribute type: %d on \"%s\"",
												m_AttrTypes[attrIdx],
												attrs[attrIdx].Name));
						}
					}
				}
				rcatchall
				{
					rlTaskError("Failed to parse attributes from %s", val);
					return false;
				}
            }
        }
        break;
    }
    return true;
}

bool 
rlScGetPresenceForGamersTask::SaxEndElement(const char* name)
{
	m_HaveCurGamerHandle = false;
	m_HaveCurAttrCount = false;

    switch((int)m_State)
    {
    case STATE_READ_RESULT_ATTRIBUTES:
        if(!stricmp("result", name))
        {
            rlTaskError("<Result> ended before expected attributes were read");
            return false;
        }
        break;

    case STATE_READ_ATTRIBUTES_FOR_GAMER:
        if(!stricmp("a", name))
        {
            m_State = STATE_EXPECT_ATTRIBUTES_LIST;
        }
        break;

#if !__NO_OUTPUT
    case STATE_EXPECT_ATTRIBUTES_LIST:
        //If we found the end of the <Result> element, we are done reading
		break;
#endif
    }
    return true;
}

const char*
rlScGetPresenceForGamersTask::GetServiceMethod() const
{
    return "Presence.asmx/GetAttributesForGamers";
}

///////////////////////////////////////////////////////////////////////////////
// rlScSubscribeUnsubscribeTask
///////////////////////////////////////////////////////////////////////////////
rlScSubscribeUnsubscribeTask::rlScSubscribeUnsubscribeTask()
{
}

rlScSubscribeUnsubscribeTask::~rlScSubscribeUnsubscribeTask()
{
}

bool
rlScSubscribeUnsubscribeTask::Configure(const int localGamerIndex,
                                        const rlScSubscribeUnsubscribeTask::Op op,
                                        const char** channels,
                                        const unsigned numChannels)
{
    if(!rlTaskVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        rlTaskError("Invalid gamer index: %d", localGamerIndex);
        return false;
    }

	if(!rlTaskVerify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE)))
	{
		rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex);
		return false;
	}

    m_Op = op;

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    if(!rlTaskVerify(BeginListParameter("channels")))
    {
        rlTaskError("Failed to add \"channels\" parameter");
        return false;
    }

    for(int i = 0; i < (int)numChannels; ++i)
    {
        if(!rlTaskVerify(AddListParamStringValue(channels[i])))
        {
            rlTaskError("Failed to add channel parameter '%s'", channels[i]);
            return false;
        }
    }

    return true;
}

bool 
rlScSubscribeUnsubscribeTask::ProcessSuccess(const rlRosResult& /*result*/,
                                            const parTreeNode* OUTPUT_ONLY(node),
                                            int& /*resultCode*/)
{
    rlTaskAssert(node);
    return true;
}

const char*
rlScSubscribeUnsubscribeTask::GetServiceMethod() const
{
    if(OP_SUBSCRIBE == m_Op)
    {
        return "Presence.asmx/Subscribe";
    }
    else if(OP_UNSUBSCRIBE == m_Op)
    {
        return "Presence.asmx/Unsubscribe";
    }
    else
    {
        rlTaskError("Unknown operation: %d", m_Op);
        rlTaskAssert(false);
        return "Presence.asmx/UnknownOp";
    }
}

///////////////////////////////////////////////////////////////////////////////
// rlScPublishTask
///////////////////////////////////////////////////////////////////////////////
rlScPublishTask::rlScPublishTask()
{
}

rlScPublishTask::~rlScPublishTask()
{
}

bool
rlScPublishTask::Configure(const int localGamerIndex,
                            const char** channels,
                            const unsigned numChannels,
                            const char* filterName,
                            const char* paramNameValueCsv,
                            const char* message)
{
    if(!rlTaskVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        rlTaskError("Invalid gamer index: %d", localGamerIndex);
        return false;
    }

	if(!rlTaskVerify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE)))
	{
		rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex);
		return false;
	}

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    if(!rlTaskVerify(BeginListParameter("channels")))
    {
        rlTaskError("Failed to add \"channels\" parameter");
        return false;
    }

    for(int i = 0; i < (int)numChannels; ++i)
    {
        if(!rlTaskVerify(AddListParamStringValue(channels[i])))
        {
            rlTaskError("Failed to add channel parameter '%s'", channels[i]);
            return false;
        }
    }

    if(!rlTaskVerify(AddStringParameter("filterName", filterName, true)))
    {
        rlTaskError("Failed to add \"filterName\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("paramNameValueCsv", paramNameValueCsv, true)))
    {
        rlTaskError("Failed to add \"paramNameValueCsv\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("message", message, true)))
    {
        rlTaskError("Failed to add \"message\" parameter");
        return false;
    }

    return true;
}

bool 
rlScPublishTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* OUTPUT_ONLY(node),
                                    int& /*resultCode*/)
{
    rlTaskAssert(node);
    return true;
}

const char*
rlScPublishTask::GetServiceMethod() const
{
    return "Presence.asmx/Publish";
}

///////////////////////////////////////////////////////////////////////////////
// rlScPostMessageTask
///////////////////////////////////////////////////////////////////////////////
rlScPostMessageTask::rlScPostMessageTask()
{
}

rlScPostMessageTask::~rlScPostMessageTask()
{
}

bool
rlScPostMessageTask::Configure(
    const int localGamerIndex,
    const rlGamerHandle* recipients,
    const unsigned numRecipients,
    const char* message,
    const unsigned ttlSeconds)
{
    if(!rlTaskVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        rlTaskError("Invalid gamer index: %d", localGamerIndex);
        return false;
    }

	if(!rlTaskVerify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE)))
	{
		rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex);
		return false;
	}

	if(!rlTaskVerify(message != NULL && istrlen(message) > 0))
	{
		rlTaskError("Message Is Empty");
		return false;
	}

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    if(!rlTaskVerify(BeginListParameter("recipientsCsv")))
    {
        rlTaskError("Failed to add \"recipientsCsv\" parameter");
        return false;
    }

	// cap the number of recipients we send to at RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS
    // throw an assert for now to root out any systems that do this
	int numRecipientsToSendTo = numRecipients;
	if(!rlTaskVerify(numRecipientsToSendTo <= RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS))
	{
		rlTaskDebug1("Too many recipients: %d, Capping at: %d", numRecipients, numRecipientsToSendTo);
		numRecipientsToSendTo = RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS;
	}

    for(int i = 0; i < (int)numRecipientsToSendTo; ++i)
    {
        if(!rlTaskVerify(recipients[i].IsValidForRos()))
        {
            rlTaskError("Invalid recipient");
            continue;
        }

        char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
        recipients[i].ToString(ghStr);

        if(!rlTaskVerify(AddListParamStringValue(ghStr)))
        {
            rlTaskError("Failed to add recipient parameter");
            return false;
        }
    }

    if(!rlTaskVerify(AddStringParameter("message", message, true)))
    {
        rlTaskError("Failed to add \"message\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddUnsParameter("ttlSeconds", ttlSeconds)))
    {
        rlTaskError("Failed to add \"ttlSeconds\" parameter");
        return false;
    }

    return true;
}

bool 
rlScPostMessageTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* OUTPUT_ONLY(node),
                                    int& /*resultCode*/)
{
    rlTaskAssert(node);
    return true;
}

const char*
rlScPostMessageTask::GetServiceMethod() const
{
    return "Presence.asmx/MultiPostMessage";
}

///////////////////////////////////////////////////////////////////////////////
// rlScGetMessagesTask
///////////////////////////////////////////////////////////////////////////////
rlScGetMessagesTask::rlScGetMessagesTask()
: m_MessageIter(NULL)
, m_MaxMessages(0)
{
}

rlScGetMessagesTask::~rlScGetMessagesTask()
{
}

bool
rlScGetMessagesTask::Configure(const int localGamerIndex,
                                const unsigned numMessages,
                                rlScPresenceMessageIterator* iter)
{
    if(!rlTaskVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        rlTaskError("Invalid gamer index: %d", localGamerIndex);
        return false;
    }

	if(!rlTaskVerify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE)))
	{
		rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex);
		return false;
	}

    if(0 == numMessages)
    {
        rlTaskError("No messages requested");
        return false;
    }

    if(!rlTaskVerify(iter->Begin()))
    {
        rlTaskError("Error initializing message iterator");
    }

    if(!rlRosHttpTask::Configure(localGamerIndex, &iter->m_ParserTree))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddUnsParameter("maxMessages", numMessages)))
    {
        rlTaskError("Failed to add \"maxMessages\" parameter");
        return false;
    }

    m_MessageIter = iter;
    m_MaxMessages = numMessages;
    iter->m_NumMessagesRetrieved = iter->m_TotalMessages = 0;

    return true;
}

bool 
rlScGetMessagesTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* node,
                                    int& /*resultCode*/)
{
    rlTaskAssert(node);

    rtry
    {
        m_MessageIter->m_NumMessagesRetrieved = 0;

        const parTreeNode* messagesNode = node->FindChildWithName("Messages");
        rcheck(messagesNode, catchall, 
                rlTaskError("Failed to find <Messages> element"));

        const parAttribute* attr;
        const parElement* el = &messagesNode->GetElement();

        //Total messages in queue
        attr = el->FindAttribute("Total");
        rcheck(attr, catchall, rlTaskError("Failed to find \"Total\" attribute"));
        const int msgTotal = attr->FindIntValue();

        //Number of messages
        attr = el->FindAttribute("Count");
        rcheck(attr, catchall, rlTaskError("Failed to find \"Count\" attribute"));
        const int msgCount = attr->FindIntValue();

        rlTaskDebug2("Received %d messages, %d messages remain", msgCount, msgTotal);

        rverify((unsigned)msgCount <= m_MaxMessages,
                catchall,
                rlTaskError("Message count returned: %d exceeds max requested: %d",
                            msgCount, m_MaxMessages));

        m_MessageIter->m_TotalMessages = msgTotal;
        m_MessageIter->m_NumMessagesRetrieved = msgCount;
    }
    rcatchall
    {
        return false;
    }

    return true;
}

const char*
rlScGetMessagesTask::GetServiceMethod() const
{
    return "Presence.asmx/GetMessages";
}

///////////////////////////////////////////////////////////////////////////////
// rlScQueryPresenceTask
///////////////////////////////////////////////////////////////////////////////
rlScQueryPresenceTask::rlScQueryPresenceTask()
: m_RecordsBuf(NULL)
, m_SizeofRecordsBuf(0)
, m_Records(NULL)
, m_NumRecords(NULL)
, m_MaxRecords(0)
{
}

rlScQueryPresenceTask::~rlScQueryPresenceTask()
{
}

bool
rlScQueryPresenceTask::Configure(const int localGamerIndex,
                                const char* queryName,
                                const char* paramNameValueCsv,
                                const int offset,
                                const int count,
                                char* recordsBuf,
                                const unsigned sizeofRecordsBuf,
                                char** records,
                                unsigned* numRecordsRetrieved,
                                unsigned* numRecords)
{
    if(offset < 0)
    {
        rlTaskError("Invalid offset: %d", offset);
        return false;
    }

    //If count is zero we're only interested in getting the total
    //number of matching records, not the actual records.
    if(count > 0 && (!recordsBuf || !sizeofRecordsBuf || !records))
    {
        rlTaskError("Insufficient buffer sizes");
        return false;
    }

	if(!rlTaskVerify(rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE)))
	{
		rlTaskError("No privilege to read/write presence for gamer index %d", localGamerIndex);
		return false;
	}

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("ticket", cred.GetTicket(), true)))
    {
        rlTaskError("Failed to add \"ticket\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("queryName", queryName, true)))
    {
        rlTaskError("Failed to add \"queryName\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddStringParameter("paramNameValueCsv", paramNameValueCsv, true)))
    {
        rlTaskError("Failed to add \"paramNameValueCsv\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddIntParameter("offset", offset)))
    {
        rlTaskError("Failed to add \"offset\" parameter");
        return false;
    }

    if(!rlTaskVerify(AddIntParameter("count", count)))
    {
        rlTaskError("Failed to add \"count\" parameter");
        return false;
    }

	if(!rlTaskVerify(AddIntParameter("maxQueryBufferSize", sizeofRecordsBuf)))
	{
		rlTaskError("Failed to add \"maxQueryBufferSize\" parameter");
		return false;
	}

    m_RecordsBuf = recordsBuf;
    m_SizeofRecordsBuf = sizeofRecordsBuf;
    m_Records = records;
    m_NumRecordsRetrieved = numRecordsRetrieved;
    m_NumRecords = numRecords;
    m_MaxRecords = count;

    *m_NumRecordsRetrieved = *m_NumRecords = 0;
    if(m_Records)
    {
        sysMemSet(m_Records, 0, sizeof(m_Records[0])*m_MaxRecords);
    }

    return true;
}

bool 
rlScQueryPresenceTask::ProcessSuccess(const rlRosResult& /*result*/,
                                    const parTreeNode* node,
                                    int& /*resultCode*/)
{
    rlTaskAssert(node);

    rtry
    {
        *m_NumRecordsRetrieved = *m_NumRecords = 0;
        if(m_Records)
        {
            sysMemSet(m_Records, 0, sizeof(m_Records[0])*m_MaxRecords);
        }

        const parTreeNode* resultsNode = node->FindChildWithName("Results");
        rcheck(resultsNode, catchall, 
                rlTaskError("Failed to find <Results> element"));

        const parAttribute* attr;
        const parElement* el = &resultsNode->GetElement();

        //Number of results
        attr = el->FindAttribute("Count");
        rcheck(attr, catchall, rlTaskError("Failed to find \"Count\" attribute"));
        const int resultCount = attr->FindIntValue();

        *m_NumRecordsRetrieved = resultCount;

        if(0 == m_MaxRecords)
        {
            rlTaskDebug2("%d matching records", resultCount);

            //We're only interested in the total number of matching records.
            *m_NumRecords = resultCount;
        }
        else
        {
            rlTaskDebug2("Received %d results", resultCount);

            rverify((unsigned)resultCount <= m_MaxRecords,
                    catchall,
                    rlTaskError("Result count returned: %d exceeds max requested: %u",
                                resultCount, m_MaxRecords));

            const parTreeNode* recordNode = resultsNode->GetChild();
            const char* eob = m_RecordsBuf + m_SizeofRecordsBuf;
            char* p = m_RecordsBuf;

            int i;
            for(i = 0; i < resultCount && recordNode; ++i, ++*m_NumRecords, recordNode = recordNode->GetSibling())
            {
                const char* nodeData = recordNode->GetData();
                const size_t dataLen = strlen(nodeData);

                if(dataLen >= (size_t)(eob-p))
                {
                    rlWarning("Not enough buffer space to hold entire result set");
                    break;
                }

                m_Records[*m_NumRecords] = p;
                safecpy(p, nodeData, eob-p);
                p += dataLen + 1;   //Account for terminating null

                rlTaskDebug2("  %2d: %s", *m_NumRecords, nodeData);
            }
        }
    }
    rcatchall
    {
        *m_NumRecordsRetrieved = *m_NumRecords = 0;
        return false;
    }

    return true;
}

const char*
rlScQueryPresenceTask::GetServiceMethod() const
{
    return "Presence.asmx/QueryWithMaxRecordLength";
}

}   //namespace rage
