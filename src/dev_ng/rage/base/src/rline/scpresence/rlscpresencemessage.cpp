// 
// rline/rlscpresencemessage.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscpresencemessage.h"
#include "rline/rldiag.h"
#include "rline/ros/rlros.h"
#include "rline/rlpresence.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, scpresence)
#undef __rage_channel
#define __rage_channel rline_scpresence

template<>
bool
RsonWriter::WriteValue(const char* name, const rlGamerHandle& value)
{
    char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
    return this->WriteString(name, value.ToString(ghStr));
}

template<>
bool
RsonWriter::WriteValue(const char* name, const rlSessionInfo& value)
{
    char siStr[rlSessionInfo::TO_STRING_BUFFER_SIZE];
    return this->WriteString(name, value.ToString(siStr));
}

template<>
bool
RsonReader::AsValue(rlGamerHandle& value) const
{
    char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
    return this->AsString(ghStr) && value.FromString(ghStr);
}

template<>
bool
RsonReader::AsValue(rlSessionInfo& value) const
{
    char siStr[rlSessionInfo::TO_STRING_BUFFER_SIZE];
    return this->AsString(siStr) && value.FromString(siStr);
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageSocialClubMembershipChanged
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageSocialClubMembershipChanged::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageSocialClubMembershipChanged::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageSocialClubMembershipChanged>(rr), "%s: Invalid", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageFriendListChanged
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageFriendListChanged::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageFriendListChanged::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageFriendListChanged>(rr), "%s: Invalid", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageFriendInviteReceived
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageFriendInviteReceived::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageFriendInviteReceived::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageFriendInviteReceived>(rr), "%s: Invalid", MSG_ID())
	        && rlVerifyf(rr.ReadValue("gh", m_hInvitee), "%s: Missing invitee gamer handle", MSG_ID())
            && rlVerifyf(rr.ReadString("nickname", m_InviteeName), "%s: Missing invitee name", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageFriendInviteAccepted
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageFriendInviteAccepted::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageFriendInviteAccepted::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageFriendInviteAccepted>(rr), "%s: Invalid", MSG_ID())
	        && rlVerifyf(rr.ReadValue("gh", m_hInvitee), "%s: Missing invitee gamer handle", MSG_ID())
            && rlVerifyf(rr.ReadString("nickname", m_InviteeName), "%s: Missing invitee name", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageFriendInviteCanceled
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageFriendInviteCanceled::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageFriendInviteCanceled::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageFriendInviteCanceled>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.ReadValue("ee", m_InviteeId), "%s: Missing invitee rockstar ID", MSG_ID())
            && rlVerifyf(rr.ReadValue("er", m_InviterId), "%s: Missing inviter rockstar ID", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageMpInvite
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageMpInvite::Export(RsonWriter& rw) const
{
    return rlVerifyf(rw.Begin(MSG_ID(), NULL), "%s: Error writing invite", MSG_ID())
            && rlVerifyf(rw.WriteValue<const rlGamerHandle&>("h", m_InviterGamerHandle), "%s: Error writing gamer handle", MSG_ID())
            && rlVerifyf(rw.WriteValue("n", m_InviterName), "%s: Error writing inviter name", MSG_ID())
            && rlVerifyf(rw.WriteValue<const rlSessionInfo&>("s", m_SessionInfo), "%s: Error writing session info", MSG_ID())
            && rlVerifyf(rw.End(), "%s: Error writing invite", MSG_ID());
}

#if __RGSC_DLL
bool
rlScPresenceMessageMpInvite::Export(RsonWriter& rw, const char* sessionInfoAsString) const
{
    return rlVerifyf(rw.Begin(MSG_ID(), NULL), "%s: Error writing invite", MSG_ID())
            && rlVerifyf(rw.WriteValue<const rlGamerHandle&>("h", m_InviterGamerHandle), "%s: Error writing gamer handle", MSG_ID())
            && rlVerifyf(rw.WriteValue("n", m_InviterName), "%s: Error writing inviter name", MSG_ID())
            && rlVerifyf(rw.WriteString("s", sessionInfoAsString), "%s: Error writing session info", MSG_ID())
            && rlVerifyf(rw.End(), "%s: Error writing invite", MSG_ID());
}
#endif

bool
rlScPresenceMessageMpInvite::Import(const RsonReader& rr, const rlScPresenceMessageSender& sender)
{
	bool success = 
		rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageMpInvite>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.ReadValue("h", m_InviterGamerHandle), "%s: Missing inviter gamer handle", MSG_ID())
            && rlVerifyf(rr.ReadString("n", m_InviterName), "%s: Missing inviter name", MSG_ID())
            && rlVerifyf(rr.ReadValue("s", m_SessionInfo), "%s: Missing session info", MSG_ID());

	if(success)
	{
		if((sender.m_Source == rlScPresenceMessageSender::SENDER_PLAYER) &&
			sender.m_GamerHandle.IsValid())
		{
			if(m_InviterGamerHandle != sender.m_GamerHandle)
			{
				rlError("rlScPresenceMessageMpInvite::Import :: inviter '%s' does not match authenticated sender '%s'",
					m_InviterGamerHandle.ToString(), sender.m_GamerHandle.ToString());

				m_InviterGamerHandle.Clear();
				success = false;
			}
		}
	}

	return success;
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageMpSendInvite
//////////////////////////////////////////////////////////////////////////
rlScPresenceMessageMpSendInvite::rlScPresenceMessageMpSendInvite()
{
    m_InviteeUserId[0] = '\0';
}

bool
rlScPresenceMessageMpSendInvite::Export(RsonWriter& rw) const
{
    return rlVerifyf(rw.Begin(MSG_ID(), NULL), "%s: Error writing invite", MSG_ID())
            && (!m_InviteeGamerHandle.IsValid()
                || rlVerifyf(rw.WriteValue<const rlGamerHandle&>("h", m_InviteeGamerHandle), "%s: Error writing gamer handle", MSG_ID()))
            && (m_InviteeUserId[0] == '\0'
                || rlVerifyf(rw.WriteValue("uid", m_InviteeUserId), "%s: Error writing user ID", MSG_ID()))
            && rlVerifyf(rw.WriteValue<const rlSessionInfo&>("s", m_SessionInfo), "%s: Error writing session info", MSG_ID())
            && rlVerifyf(rw.End(), "%s: Error writing invite", MSG_ID());
}

bool
rlScPresenceMessageMpSendInvite::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageMpSendInvite>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.HasMember("h") || rr.HasMember("uid"),
                           "%s: Neither 'h' nor 'uid' are present", MSG_ID())
            && (!rr.HasMember("h")
                || rlVerifyf(rr.ReadValue("h", m_InviteeGamerHandle), "%s: Error reading invitee gamer handle", MSG_ID()))
            && (!rr.HasMember("uid")
                || (rlVerifyf(rr.ReadValue("uid", m_InviteeUserId), "%s: Error reading invitee user ID", MSG_ID())
                    && m_InviteeGamerHandle.FromUserId(m_InviteeUserId)))
            && rlVerifyf(rr.ReadValue("s", m_SessionInfo), "%s: Missing session info", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageMpPartyInvite
//////////////////////////////////////////////////////////////////////////
bool rlScPresenceMessageMpPartyInvite::Export(RsonWriter& rw) const
{
	return rlVerifyf(rw.Begin(MSG_ID(), NULL), "%s: Error writing invite", MSG_ID())
		&& rlVerifyf(rw.WriteValue<const rlGamerHandle&>("h", m_InviterGamerHandle), "%s: Error writing gamer handle", MSG_ID())
		&& rlVerifyf(rw.WriteValue("n", m_InviterName), "%s: Error writing inviter name", MSG_ID())
		&& rlVerifyf(rw.WriteValue<const rlSessionInfo&>("s", m_SessionInfo), "%s: Error writing session info", MSG_ID())
		&& rlVerifyf(rw.WriteValue("sa", m_Salutation), "%s: Error writing salutation", MSG_ID())
		&& rlVerifyf(rw.End(), "%s: Error writing invite", MSG_ID());
}

#if __RGSC_DLL
bool rlScPresenceMessageMpPartyInvite::Export(RsonWriter& rw, const char* sessionInfoAsString) const
{
	return rlVerifyf(rw.Begin(MSG_ID(), NULL), "%s: Error writing invite", MSG_ID())
		&& rlVerifyf(rw.WriteValue<const rlGamerHandle&>("h", m_InviterGamerHandle), "%s: Error writing gamer handle", MSG_ID())
		&& rlVerifyf(rw.WriteValue("n", m_InviterName), "%s: Error writing inviter name", MSG_ID())
		&& rlVerifyf(rw.WriteString("s", sessionInfoAsString), "%s: Error writing session info", MSG_ID())
		&& rlVerifyf(rw.WriteValue("sa", m_Salutation), "%s: Error writing salutation", MSG_ID())
		&& rlVerifyf(rw.End(), "%s: Error writing invite", MSG_ID());
}
#endif

bool rlScPresenceMessageMpPartyInvite::Import(const RsonReader& rr, const rlScPresenceMessageSender& sender)
{
	bool success = 
		rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageMpPartyInvite>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadValue("h", m_InviterGamerHandle), "%s: Missing inviter gamer handle", MSG_ID())
		&& rlVerifyf(rr.ReadString("n", m_InviterName), "%s: Missing inviter name", MSG_ID())
		&& rlVerifyf(rr.ReadValue("s", m_SessionInfo), "%s: Missing session info", MSG_ID())
		&& rlVerifyf(rr.ReadString("sa", m_Salutation), "%s: Missing salutation", MSG_ID());

	if(success)
	{
		if((sender.m_Source == rlScPresenceMessageSender::SENDER_PLAYER) &&
			sender.m_GamerHandle.IsValid())
		{
			if(m_InviterGamerHandle != sender.m_GamerHandle)
			{
				rlError("rlScPresenceMessageMpPartyInvite::Import :: inviter '%s' does not match authenticated sender '%s'",
					m_InviterGamerHandle.ToString(), sender.m_GamerHandle.ToString());

				m_InviterGamerHandle.Clear();
				success = false;
			}
		}
	}
	
	return success;
}

//////////////////////////////////////////////////////////////////////////
//  rlSPresencecMessageGamerOnline
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageGamerOnline::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageGamerOnline::Import(const RsonReader& rr)
{
    return rlVerifyf(rr.AsValue(MSG_ID(), m_GamerHandle), "%s: Invalid", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageGamerOffline
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageGamerOffline::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageGamerOffline::Import(const RsonReader& rr)
{
    return rlVerifyf(rr.AsValue(MSG_ID(), m_GamerHandle), "%s: Invalid", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageRichPresenceChanged
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageRichPresenceChanged::Export(RsonWriter& rw) const
{
	return rlVerifyf(rw.Begin(MSG_ID(), NULL), "%s: Error writing rich presence msg", MSG_ID())
		&& rlVerifyf(rw.WriteValue<const rlGamerHandle&>("h", m_GamerHandle), "%s: Error writing gamer handle", MSG_ID())
		&& rlVerifyf(rw.WriteValue("p", m_PresenceMsg), "%s: Error writing rich presence message", MSG_ID())
		&& rlVerifyf(rw.End(), "%s: Error writing rich presence msg", MSG_ID());
}

bool
rlScPresenceMessageRichPresenceChanged::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageRichPresenceChanged>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadValue("h", m_GamerHandle), "%s: Missing gamer handle", MSG_ID())
		&& rlVerifyf(rr.ReadValue("p", m_PresenceMsg), "%s: Missing rich presence message", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageRefreshAttributes
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageRefreshAttributes::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageRefreshAttributes::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageRefreshAttributes>(rr), "%s: Invalid", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageClanInvites
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageClanInviteReceived::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageClanInviteReceived::Import(const RsonReader& rr)
{
    bool bSuccess = rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanInviteReceived>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.ReadValue("ci", m_ClanId), "%s: Missing clan ID", MSG_ID())
            && rlVerifyf(rr.ReadString("cn", m_ClanName), "%s: Missing clan name", MSG_ID())
            && rlVerifyf(rr.ReadString("ct", m_ClanTag), "%s: Missing clan tag", MSG_ID())
            && rlVerifyf(rr.ReadString("rn", m_RankName), "%s: Missing rank name", MSG_ID());

	m_Message[0] = '\0';
	if (bSuccess)
	{
		rr.ReadString("msg", m_Message);
	}

	return bSuccess; 
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageClanJoined
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageClanJoined::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageClanJoined::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanJoined>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.AsValue(m_ClanId), "%s: Failed to parse clan ID", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageClanLeft
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageClanLeft::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageClanLeft::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanLeft>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.AsValue(m_ClanId), "%s: Failed to parse clan ID", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageClanKicked
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageClanKicked::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageClanKicked::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanKicked>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.AsValue(m_ClanId), "%s: Failed to parse clan ID", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageClanPrimaryChanged
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageClanPrimaryChanged::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageClanPrimaryChanged::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanPrimaryChanged>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.AsValue(m_ClanId), "%s: Failed to parse clan ID", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//	rlSCPresenceClanFounded
//////////////////////////////////////////////////////////////////////////
bool 
rlScPresenceMessageClanFounded::Export( RsonWriter& ) const
{
	//Sent only from the server
	return rlVerify(false);
}

bool 
rlScPresenceMessageClanFounded::Import( const RsonReader& rr )
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanFounded>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.AsValue(m_ClanId), "%s: Failed to parse clan ID", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//	rlSCPresenceClanFounded
//////////////////////////////////////////////////////////////////////////
bool 
rlScPresenceMessageClanMetadataChanged::Export( RsonWriter& ) const
{
	//Sent only from the server
	return rlVerify(false);
}

bool 
rlScPresenceMessageClanMetadataChanged::Import( const RsonReader& rr )
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanMetadataChanged>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadValue("clanid", m_ClanId), "%s: Failed to parse clan ID", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageCloudFileChanged
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageCloudFileChanged::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageCloudFileChanged::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageCloudFileChanged>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.AsValue(m_Path), "%s: Failed to parse path", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessagePublish
//////////////////////////////////////////////////////////////////////////
rlScPresenceMessagePublish::rlScPresenceMessagePublish()
{
    m_Message[0] = m_Channel[0] = '\0';
}


rlScPresenceMessagePublish::rlScPresenceMessagePublish(const char* channel,
                                                        const char* msg)
{
    safecpy(m_Channel, channel);
    rlAssertf(strlen(m_Channel) == strlen(channel),
            "Channel name '%s' is too long, max length is %" SIZETFMT "d",
            channel,
            sizeof(m_Channel)-1);

    safecpy(m_Message, msg);
}

bool
rlScPresenceMessagePublish::Export(RsonWriter& rw) const
{
    return rlVerifyf(RsonReader::ValidateJson(m_Message, istrlen(m_Message)), "%s: Invalid JSON: %s", MSG_ID(), m_Message)
            && rlVerifyf(rw.Begin(MSG_ID(), NULL), "%s: Error writing publish message", MSG_ID())
            && rlVerifyf(rw.WriteString("channel", m_Channel), "%s: Failed to write channel", MSG_ID())
            //Use WriteRawString so we don't escape or modify the message contents in any way.
            && rlVerifyf(rw.WriteRawString("msg", m_Message), "%s: Failed to write message", MSG_ID())
            && rlVerifyf(rw.End(), "%s: Error writing publish message", MSG_ID());
}

bool
rlScPresenceMessagePublish::Import(const RsonReader& rr)
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessagePublish>(rr), "%s: Invalid", MSG_ID())
            && rlVerifyf(rr.ReadString("channel", m_Channel), "%s: Failed to read channel", MSG_ID())
            //Use ReadRawString so we don't try to un-escape the message contents as they're read.
            && rlVerifyf(rr.ReadRawString("msg", m_Message), "%s: Failed to read message", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceInboxNewMessage
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceInboxNewMessage::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceInboxNewMessage::Import(const RsonReader& rr)
{
    char recipient[128];

    if(!rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceInboxNewMessage>(rr), "%s: Invalid", MSG_ID())
        || !rlVerifyf(rr.ReadValue("recip", recipient), "%s: Error reading recipient gamer handle", MSG_ID()))
    {
        return false;
    }

	//Check for tags 
	// "tags":["t1","t2","t3"]
	m_numTags = 0;
	RsonReader tags;
	if (rr.GetMember("tags", &tags))
	{
		m_numTags = tags.AsArray(m_tags, MAX_NUM_TAGS);
	}

    if(rlGamerHandle::IsNativeGamerHandle(recipient))
    {
        return m_Recipient.FromString(recipient);
    }
    else if(rlGamerHandle::IsSc(recipient))
    {
        return rlPresence::GetLocalGamerHandleFromScGamerHandle(recipient, &m_Recipient);
    }

    return false;
}

bool rlScPresenceInboxNewMessage::IsTagPresent( const char* tag ) const
{
	for (unsigned i = 0; i < m_numTags; ++i)
	{
		if (strcmp(m_tags[i], tag) == 0)
		{
			return true;
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////
//
//	rlScPresenceMessageClanMemberPromoted
//	rlScPresenceMessageClanMemberDemoted
//
//////////////////////////////////////////////////////////////////////////
bool rlScPresenceMessageClanMemberPromoted::Export( RsonWriter& ) const
{
	//Sent only from the server
	return rlVerify(false);
}

bool rlScPresenceMessageClanMemberPromoted::Import( const RsonReader& rr )
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanMemberPromoted>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadValue("clanid", m_ClanId), "%s: Failed to parse clan ID", MSG_ID())
		&& rlVerifyf(rr.ReadInt("rankorder", m_rankOrder), "%s: Failed to parse rankorder", MSG_ID())
		&& rlVerifyf(rr.ReadString("rankname", m_rankName), "%s: Failed to parse rank Name", MSG_ID());
}

bool rlScPresenceMessageClanMemberDemoted::Export( RsonWriter& ) const
{
	//Sent only from the server
	return rlVerify(false);
}

bool rlScPresenceMessageClanMemberDemoted::Import( const RsonReader& rr )
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanMemberDemoted>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadValue("clanid", m_ClanId), "%s: Failed to parse clan ID", MSG_ID())
		&& rlVerifyf(rr.ReadInt("rankorder", m_rankOrder), "%s: Failed to parse rankorder", MSG_ID())
		&& rlVerifyf(rr.ReadString("rankname", m_rankName), "%s: Failed to parse rank Name", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//	rlScPresenceMessageClanNotify
//////////////////////////////////////////////////////////////////////////
bool rlScPresenceMessageClanNotify::Export( RsonWriter& ) const
{
	//Sent only from the server
	return rlVerify(false);
}

bool rlScPresenceMessageClanNotify::Import( const RsonReader& rr )
{
	return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageClanNotify>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadValue("clanid", m_ClanId), "%s: Failed to parse clan ID", MSG_ID())
		&& rlVerifyf(rr.ReadString("type", m_Type), "%s: Failed to parse type", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//	rlScPresenceRenewTicket
//////////////////////////////////////////////////////////////////////////

bool
rlScPresenceRenewTicket::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceRenewTicket::Import(const RsonReader& rr)
{
    return rlVerifyf(rr.CheckName(MSG_ID()), "%s: Invalid", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//	rlScPresenceInvalidateTicket
//////////////////////////////////////////////////////////////////////////

bool
rlScPresenceInvalidateTicket::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceInvalidateTicket::Import(const RsonReader& rr)
{
    return rlVerifyf(rr.CheckName(MSG_ID()), "%s: Invalid", MSG_ID());
}

//////////////////////////////////////////////////////////////////////////
//   rlScPresenceAccountLinkResult
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceAccountLinkResult::Export(RsonWriter& /*rw*/) const
{
	// Sent only from the server
	return rlVerify(false);
}

bool
rlScPresenceAccountLinkResult::Import(const RsonReader& rr)
{
	bool success = rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceAccountLinkResult>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadBool("s", m_Success), "%s: Failed to parse success", MSG_ID())
		&& rlVerifyf(rr.ReadString("n", m_Network), "%s: Failed to parse network", MSG_ID())
		&& rlVerifyf(rr.ReadString("r", m_Route), "%s: Failed to parse route", MSG_ID());

	// Optional Param:
	rr.ReadString("e", m_ErrorId);
	return success;
}

//////////////////////////////////////////////////////////////////////////
//   rlScPresenceAccountUnlinkResult
//////////////////////////////////////////////////////////////////////////
rlScPresenceAccountUnlinkResult::rlScPresenceAccountUnlinkResult()
	: m_Success(false)
{
	sysMemSet(m_Network, 0, sizeof(m_Network));
	sysMemSet(m_Route, 0, sizeof(m_Route));
	sysMemSet(m_ErrorId, 0, sizeof(m_ErrorId));
}

bool
rlScPresenceAccountUnlinkResult::Export(RsonWriter& /*rw*/) const
{
	// Sent only from the server
	return rlVerify(false);
}

bool
rlScPresenceAccountUnlinkResult::Import(const RsonReader& rr)
{
	bool success = rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceAccountUnlinkResult>(rr), "%s: Invalid", MSG_ID())
		&& rlVerifyf(rr.ReadBool("s", m_Success), "%s: Failed to parse success", MSG_ID())
		&& rlVerifyf(rr.ReadString("n", m_Network), "%s: Failed to parse network", MSG_ID())
		&& rlVerifyf(rr.ReadString("r", m_Route), "%s: Failed to parse route", MSG_ID());

	// Optional Param:
	rr.ReadString("e", m_ErrorId);
	return success;
}

//////////////////////////////////////////////////////////////////////////
//	rlScPresenceMessageEntitlementsChanged
//////////////////////////////////////////////////////////////////////////
bool
rlScPresenceMessageEntitlementsChanged::Export(RsonWriter& /*rw*/) const
{
    //Sent only from the server
    return rlVerify(false);
}

bool
rlScPresenceMessageEntitlementsChanged::Import(const RsonReader& rr)
{
    return rlVerifyf(rlScPresenceMessage::IsA<rlScPresenceMessageEntitlementsChanged>(rr), "%s: Invalid", MSG_ID());
}

} //namespace rage
