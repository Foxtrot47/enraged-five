// 
// rline/rlscpresencetasks.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCPRESENCETASKS_H
#define RLINE_RLSCPRESENCETASKS_H

#include "rline/ros/rlroshttptask.h"
#include "rline/scpresence/rlscpresence.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, scpresence)

class rlScPresenceAttribute;
class rlScPresenceMessageIterator;

///////////////////////////////////////////////////////////////////////////////
// rlScSignOutTask
///////////////////////////////////////////////////////////////////////////////
class rlScSignOutTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScSignOutTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    rlScSignOutTask();

    virtual ~rlScSignOutTask();

    bool Configure(const int localGamerIndex);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;
};

///////////////////////////////////////////////////////////////////////////////
// rlScCommitPresenceTask
///////////////////////////////////////////////////////////////////////////////
class rlScCommitPresenceTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScCommitPresenceTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    enum CommitType
    {
        COMMIT_INVALID  = -1,
        COMMIT_MODIFY,
        COMMIT_REPLACE
    };

    rlScCommitPresenceTask();

    virtual ~rlScCommitPresenceTask();

    bool Configure(const int localGamerIndex,
                    const rlScPresenceAttribute* attrs,
                    const unsigned numAttrs,
                    const CommitType commitType);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;

private:

    CommitType m_CommitType;
};

///////////////////////////////////////////////////////////////////////////////
// rlScGetPresenceTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetPresenceTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScGetPresenceTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    rlScGetPresenceTask();

    virtual ~rlScGetPresenceTask();

    bool Configure(const int localGamerIndex,
                    const rlGamerHandle& gamerHandle,
                    rlScPresenceAttribute* attrs,
                    const unsigned numAttrs);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;

    rlScPresenceAttribute* m_Attrs;
    unsigned m_NumRequested;
};

///////////////////////////////////////////////////////////////////////////////
// rlScGetPresenceForGamersTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetPresenceForGamersTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScGetPresenceForGamersTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

	rlScGetPresenceForGamersTask();

    virtual ~rlScGetPresenceForGamersTask();

    bool Configure(const int localGamerIndex,
					const rlGamerHandle* gamerHandles,
					const unsigned numGamerHandles,
					rlScPresenceAttribute** attrs,
					const unsigned numAttrs);

protected:
    virtual const char* GetServiceMethod() const;

	virtual bool SaxStartElement(const char* name);
	virtual bool SaxAttribute(const char* name, const char* val);
	virtual bool SaxEndElement(const char* name);

	rlRosSaxReader m_RosSaxReader;

	enum State
	{
		STATE_EXPECT_RESULT,            //Looking for <Result> element
		STATE_READ_RESULT_ATTRIBUTES,   //Need to read Count attribute
		STATE_EXPECT_ATTRIBUTES_LIST,   //Looking for <A> element for a single gamer
		STATE_READ_ATTRIBUTES_FOR_GAMER, //Reading attributes of <A> for a single gamer
	};
	State m_State;

	unsigned m_ResultCount;
	unsigned m_NumGamerHandles;
	unsigned m_CurCount;
	unsigned m_CurGamerHandle;
	bool m_HaveCurGamerHandle;
	bool m_HaveCurAttrCount;
	unsigned m_NumAttrsRequested;

	const rlGamerHandle* m_GamerHandles;
	rlScPresenceAttribute** m_Attrs;

	u8 m_AttrTypes[RLSC_PRESENCE_GET_ATTRS_MAX_GAMERS_PER_REQUEST];
};

///////////////////////////////////////////////////////////////////////////////
// rlScSubscribeUnsubscribeTask
///////////////////////////////////////////////////////////////////////////////
class rlScSubscribeUnsubscribeTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScSubscribeUnsubscribeTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    enum Op
    {
        OP_SUBSCRIBE,
        OP_UNSUBSCRIBE
    };

    rlScSubscribeUnsubscribeTask();

    virtual ~rlScSubscribeUnsubscribeTask();

    bool Configure(const int localGamerIndex,
                    const Op op,
                    const char** channels,
                    const unsigned numChannels);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;

private:

    Op m_Op;
};

///////////////////////////////////////////////////////////////////////////////
// rlScPublishTask
///////////////////////////////////////////////////////////////////////////////
class rlScPublishTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScPublishTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    rlScPublishTask();

    virtual ~rlScPublishTask();

    bool Configure(const int localGamerIndex,
                    const char** channels,
                    const unsigned numChannels,
                    const char* filterName,
                    const char* paramNameValueCsv,
                    const char* message);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;
};

///////////////////////////////////////////////////////////////////////////////
// rlScPostMessageTask
///////////////////////////////////////////////////////////////////////////////
class rlScPostMessageTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScPostMessageTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    rlScPostMessageTask();

    virtual ~rlScPostMessageTask();

    bool Configure(const int localGamerIndex,
                    const rlGamerHandle* recipients,
                    const unsigned numRecipients,
                    const char* message,
                    const unsigned ttlSeconds);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;
};

///////////////////////////////////////////////////////////////////////////////
// rlScGetMessagesTask
///////////////////////////////////////////////////////////////////////////////
class rlScGetMessagesTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScGetMessagesTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    rlScGetMessagesTask();

    virtual ~rlScGetMessagesTask();

    bool Configure(const int localGamerIndex,
                    const unsigned numMessages,
                    rlScPresenceMessageIterator* iter);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;

private:

    rlScPresenceMessageIterator* m_MessageIter;

    unsigned m_MaxMessages;
};

///////////////////////////////////////////////////////////////////////////////
// rlScQueryPresenceTask
///////////////////////////////////////////////////////////////////////////////
class rlScQueryPresenceTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScQueryPresenceTask);
    RL_TASK_USE_CHANNEL(rline_scpresence);

    rlScQueryPresenceTask();

    virtual ~rlScQueryPresenceTask();

    //Pass zero for count to retrieve just the total number of
    //matching records, with no actual records returned.
    bool Configure(const int localGamerIndex,
                    const char* queryName,
                    const char* paramNameValueCsv,
                    const int offset,
                    const int count,
                    char* recordsBuf,
                    const unsigned sizeofRecordsBuf,
                    char** records,
                    unsigned* numRecordsRetrieved,
                    unsigned* numRecords);

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

    virtual const char* GetServiceMethod() const;

private:

    char* m_RecordsBuf;
    unsigned m_SizeofRecordsBuf;
    char** m_Records;
    unsigned* m_NumRecordsRetrieved;
    unsigned* m_NumRecords;
    unsigned m_MaxRecords;
};

} //namespace rage

#endif  //RLINE_RLSCPRESENCETASKS_H
