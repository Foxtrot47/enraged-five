// 
// rline/rlscpresencemessage.h 
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCPRESENCEMESSAGE_H
#define RLINE_RLSCPRESENCEMESSAGE_H

#include "rline/rlgamerinfo.h"
#include "rline/rlsessioninfo.h"
#include "rline/clan/rlclancommon.h"
#include "rline/scpresence/rlscpresence.h"
#include "data/rson.h"

/*
    Messages sent to/from the presence system

    Messages are typically encoded in JSON format and have
    an associated reader/writer (see the bottom of this file).
*/

namespace rage
{

//////////////////////////////////////////////////////////////////////////
//  Message serialization
//
//  Each message type must implement Export()/Import() functions that
//  take a RsonWriter/RsonReader, respectively.
//
//  For convenience the RLPRESENCE_MESSAGE_DECL macro adds Export()/Import()
//  functions that take char buffers.
//
//  For example, to create a ClanLeft message and serialize it to a
//  char buffer:
//
//  rlScPresenceMessageClanLeft clanLeftMsg.
//  clanLeftMsg.m_ClanId = 12345;
//
//  char msgBuf[1024];
//  clanLeftMsg.Export(msgBuf);
//
//  Similarly, to deserialize:
//
//  clanLeftMsg.Import(msgBuf);
//  
//////////////////////////////////////////////////////////////////////////

#define RLPRESENCE_MESSAGE_DECL(id)\
	static const char* MSG_ID() { return id; }\
    const char* Name() const {return MSG_ID();}\
    bool Export(char* dst, const unsigned maxLen) const\
    {\
        RsonWriter rw(dst, maxLen, RSON_FORMAT_JSON);\
        return this->Export(rw);\
    }\
    template<int SIZE>\
    bool Export(char (&dst)[SIZE]) const\
    {\
        RsonWriter rw(dst, SIZE, RSON_FORMAT_JSON);\
        return this->Export(rw);\
    }\
    bool Import(const char* src)\
    {\
        RsonReader rr(src, (unsigned)strlen(src));\
		RsonReader firstMember;\
		if(!rr.HasName())\
		{\
			rr.GetFirstMember(&rr);\
		}\
        return this->Import(rr);\
    }\
    bool Import(const rlScPresenceMessage& src)\
    {\
        RsonReader rr(src.m_Contents, (unsigned)strlen(src.m_Contents));\
		RsonReader firstMember;\
		if(!rr.HasName())\
		{\
			rr.GetFirstMember(&rr);\
		}\
        return this->Import(rr);\
    }

#define RLPRESENCE_MESSAGE_WITH_SENDER_DECL(id)\
	static const char* MSG_ID() { return id; }\
    const char* Name() const {return MSG_ID();}\
    bool Export(char* dst, const unsigned maxLen) const\
    {\
        RsonWriter rw(dst, maxLen, RSON_FORMAT_JSON);\
        return this->Export(rw);\
    }\
    template<int SIZE>\
    bool Export(char (&dst)[SIZE]) const\
    {\
        RsonWriter rw(dst, SIZE, RSON_FORMAT_JSON);\
        return this->Export(rw);\
    }\
	bool Import(const rlScPresenceMessage& src, const rlScPresenceMessageSender& sender)\
    {\
        RsonReader rr(src.m_Contents, (unsigned)strlen(src.m_Contents));\
		RsonReader firstMember;\
		if(!rr.HasName())\
		{\
			rr.GetFirstMember(&rr);\
		}\
        return this->Import(rr, sender);\
    }

class rlScPresenceMessageSocialClubMembershipChanged
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.sc.membership.changed");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);
};

class rlScPresenceMessageFriendListChanged
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.friends.changed");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);
};

//PURPOSE
//  Sent from the server when someone sends us a friend invite.
//
// "ros.friends.invite.received":{"gh":"<sender gamer handle>","nickname":"<sender name>","avatarurl":"<sender avatarUrl>"}"
class rlScPresenceMessageFriendInviteReceived
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.friends.invite.received");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

	rlGamerHandle m_hInvitee;
	char m_InviteeName[RL_MAX_NAME_BUF_SIZE];
};

//PURPOSE
//  Sent from the server when someone accepts our friend invite.
//
// "ros.friends.invite.accepted":{"gh":"<sender gamer handle>","nickname":"<sender name>","avatarurl":"<sender avatarUrl>"}"
class rlScPresenceMessageFriendInviteAccepted
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.friends.invite.accepted");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

	rlGamerHandle m_hInvitee;
	char m_InviteeName[RL_MAX_NAME_BUF_SIZE];
};

class rlScPresenceMessageFriendInviteCanceled
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.friends.invite.canceled");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    RockstarId m_InviteeId;
    RockstarId m_InviterId;
};

//PURPOSE
//  Represents a multiplayer session invite
//
//  "ros.mp.invite":{"h:":"<inviter gamer handle>","n":"<inviter name>","s":"<session info>"}"
class rlScPresenceMessageMpInvite
{
public:

    RLPRESENCE_MESSAGE_WITH_SENDER_DECL("ros.mp.invite");

    bool Export(RsonWriter& rw) const;

#if __RGSC_DLL
	// special version of Export() that takes the already encoded session info
	// struct as a string. This allows the DLL to preserve backward compatibility.
	bool Export(RsonWriter& rw, const char* sessionInfoAsString) const;
#endif

	bool Import(const RsonReader& rr, const rlScPresenceMessageSender& sender);

    rlGamerHandle m_InviterGamerHandle;
    char m_InviterName[RL_MAX_NAME_BUF_SIZE];
    rlSessionInfo m_SessionInfo;
};

//PURPOSE
//  Sent to a session host as a command to invite another player into the session.
//
//  "ros.mp.sendinvite":
//  {
//      "h":"<gamer handle to invite>",
//      "uid":<user ID to invite",
//      "s":"<target session info>"
//  }
//
//  On PS "uid" must be the invitee's NP Online ID
//  On XBox "uid" must be the decimal representation of the invitee's XUID.
//  On PC "uid" must be the invitee's rockstar ID.
//  One of "h" or "uid" must be present.
//
class rlScPresenceMessageMpSendInvite
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.mp.sendinvite");

    rlScPresenceMessageMpSendInvite();

    bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

    rlGamerHandle m_InviteeGamerHandle;
    rlSessionInfo m_SessionInfo;
    char m_InviteeUserId[32];
};

//PURPOSE
//  Represents a multiplayer party invite
//
//  "ros.mp.partyinvite":{"h:":"<inviter gamer handle>","n":"<inviter name>","s":"<session info>","sa";"salutation"}"
class rlScPresenceMessageMpPartyInvite
{
public:

	RLPRESENCE_MESSAGE_WITH_SENDER_DECL("ros.mp.partyinvite");

	bool Export(RsonWriter& rw) const;

#if __RGSC_DLL
	// special version of Export() that takes the already encoded session info
	// struct as a string. This allows the DLL to preserve backward compatibility.
	bool Export(RsonWriter& rw, const char* sessionInfoAsString) const;
#endif

	bool Import(const RsonReader& rr, const rlScPresenceMessageSender& sender);

	rlGamerHandle m_InviterGamerHandle;
	char m_InviterName[RL_MAX_NAME_BUF_SIZE];
	char m_Salutation[RL_MAX_INVITE_SALUTATION_CHARS];
	rlSessionInfo m_SessionInfo;
};

//PURPOSE
//  Represents a notification that a gamer came online
//
//  "ros.gamer.online":"<gamer handle>"
class rlScPresenceMessageGamerOnline
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.gamer.online");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    rlGamerHandle m_GamerHandle;
};

//PURPOSE
//  Represents a notification that a gamer went offline
//
//  "ros.gamer.offline":"<gamer handle>"
class rlScPresenceMessageGamerOffline
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.gamer.offline");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    rlGamerHandle m_GamerHandle;
};

//PURPOSE
//	Sent when the rich presence of a user we're subscribed to has changed.
//	Must be enclosed with braces.
//	{ "ros.gamer.richpresence" : { "h" : "<gamer handle>", "p" : "rich presence string" } }
class rlScPresenceMessageRichPresenceChanged
{
public:

	RLPRESENCE_MESSAGE_DECL("ros.gamer.richpresence");
		
	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	rlGamerHandle m_GamerHandle;
	char m_PresenceMsg[RLSC_PRESENCE_STRING_MAX_SIZE];
};

//PURPOSE
//  Sent to us when we need to do a full refresh of our presence attributes.
//
//  "ros.attrs.refresh":1
class rlScPresenceMessageRefreshAttributes
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.attrs.refresh");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);
};

//PURPOSE
//  Sent to us when we've received a clan invite.
//
//  ros.clan.invite:{"ci":<clan id>,"cn":"<clan name>", "ct":"<clan tag>", "rn":"<rank name>", "msg":"<message >"}
class rlScPresenceMessageClanInviteReceived
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.clan.invite");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    rlClanId m_ClanId;
    char m_ClanName[RL_CLAN_NAME_MAX_CHARS];
    char m_ClanTag[RL_CLAN_TAG_MAX_CHARS];
    char m_RankName[RL_CLAN_RANK_NAME_MAX_CHARS];
    char m_Message[RL_CLAN_INVITE_MESSAGE_MAX_CHARS];
};

//PURPOSE
//  Sent to us when we've joined a clan.
//
//  ros.clan.joined:<clan id>
class rlScPresenceMessageClanJoined
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.clan.joined");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    rlClanId m_ClanId;
};

//PURPOSE
//  Sent to us when we've left a clan.
//
//  ros.clan.left:<clan id>
class rlScPresenceMessageClanLeft
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.clan.left");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    rlClanId m_ClanId;
};

//PURPOSE
//  Sent to us when we've been kicked from a clan.
//
//  ros.clan.kicked:<clan id>
class rlScPresenceMessageClanKicked
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.clan.kicked");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    rlClanId m_ClanId;
};

//PURPOSE
//  Sent to us when our primary clan changes.
//
//  ros.clan.primary.changed:<clan id>
class rlScPresenceMessageClanPrimaryChanged
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.clan.primary.changed");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    rlClanId m_ClanId;
};

//PURPOSE
//  Sent to us when a clan is founded (typically by a friend)
//
//  ros.clan.founed:<clan id>
class rlScPresenceMessageClanFounded
{
public:
	RLPRESENCE_MESSAGE_DECL("ros.clan.founded");

	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	rlClanId m_ClanId;
};

//PURPOSE
//  Sent to us when a clan's metadata changes
//
//  ros.clan.metadata.changed:{"clanid":<clan id>}
class rlScPresenceMessageClanMetadataChanged
{
public:
	RLPRESENCE_MESSAGE_DECL("ros.clan.metadata.changed");

	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	rlClanId m_ClanId;
};

//PUPROSE
//	Sent to us when the local player is promoted or demoted within a clan
//
//	ros.clan.member.promoted:{"clanid":<clan ID>, "rankorder":<int>, "rankname":<string>}
//	ros.clan.member.demoted:{"clanid":<clan ID>, "rankorder":<int>, "rankname":<string>}
class rlScPresenceMessageClanMemberPromoted
{
public:
	RLPRESENCE_MESSAGE_DECL("ros.clan.member.promoted");

	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	rlClanId m_ClanId;
	int m_rankOrder;
	char m_rankName[RL_CLAN_RANK_NAME_MAX_CHARS];
};

class rlScPresenceMessageClanMemberDemoted
{
public:
	RLPRESENCE_MESSAGE_DECL("ros.clan.member.demoted");

	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	rlClanId m_ClanId;
	int m_rankOrder;
	char m_rankName[RL_CLAN_RANK_NAME_MAX_CHARS];
};

//PURPOSE
//  Notify players about generic clan events (e.g. that other player has requested to join your clan)
//
// "ros.clan.notify":{"type":"joinrequest","clanid":<clan ID>}
class rlScPresenceMessageClanNotify
{
public:
	
	RLPRESENCE_MESSAGE_DECL("ros.clan.notify");

	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	enum { NOTIFY_TYPE_NAME_MAX_SIZE	 = 32 };

	rlClanId m_ClanId;
	char m_Type[NOTIFY_TYPE_NAME_MAX_SIZE];
};

//PURPOSE
//  Sent to us when a cloud file changes.
//
//  ros.cloud.file.changed:<path>
class rlScPresenceMessageCloudFileChanged
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.cloud.file.changed");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    char m_Path[384];
};

//PURPOSE
//  Sent to us when a message is published to a channel to which we're subscribed.
//
//  "ros.publish":{"channel":<channel>,"msg":<msg>}
class rlScPresenceMessagePublish
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.publish");

    rlScPresenceMessagePublish();

    //msg must be a valid JSON document enclosed in {}
    rlScPresenceMessagePublish(const char* channel,
                                const char* msg);

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

    char m_Message[RLSC_PRESENCE_MESSAGE_MAX_SIZE];
    char m_Channel[RLSC_PRESENCE_CHANNEL_NAME_MAX_SIZE];
};

//PURPOSE
//  Sent to us when a message arrives in our inbox.  Must query the inbox system
//  to retrieve the message
//
//  "ros.inbox.new":{"recip":"<gamerhandle>"}
class rlScPresenceInboxNewMessage
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.inbox.new");

    rlScPresenceInboxNewMessage()
	{ 
		m_Recipient.Clear();
		m_numTags = 0;
		for (int i = 0; i < MAX_NUM_TAGS; i++)
		{
			m_tags[i][0] = '\0';
		}
	}
	
	enum { MAX_NUM_TAGS = 8, MAX_TAG_LENGTH = 32};

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);

	unsigned GetNumTags() const { return m_numTags; }
	bool	 IsTagPresent(const char* tag) const;

    rlGamerHandle m_Recipient;
	
	typedef char InboxMsgTag[MAX_TAG_LENGTH];
	InboxMsgTag m_tags[MAX_NUM_TAGS];
	unsigned m_numTags;
};

//PURPOSE
//  Sent when we should renew our ticket because it has changed (i.e. privileges)
//
//  ros.ticket.renew
class rlScPresenceRenewTicket
{
public:
    RLPRESENCE_MESSAGE_DECL("ros.ticket.renew");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);
};

//PURPOSE
//  Sent when we should invalidate our ticket because it has changed (i.e. privileges)
//  Since the player won't have a ticket until it is renewed, anything requiring a ticket
//  will fail until it is rewed
//
//  ros.ticket.invalidate
class rlScPresenceInvalidateTicket
{
public:
    RLPRESENCE_MESSAGE_DECL("ros.ticket.invalidate");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);
};

//PURPOSE
//	Sent when a user attempts to link their social club account to other social
//	services like Youtube, Google+, Twitch, Facebook, etc
//
// ros.account.link
class rlScPresenceAccountLinkResult
{
public:
	RLPRESENCE_MESSAGE_DECL("ros.account.link");

	rlScPresenceAccountLinkResult()
	{
		m_Success = false;
		sysMemSet(m_Network, 0, sizeof(m_Network));
		sysMemSet(m_Route, 0, sizeof(m_Route));
		sysMemSet(m_ErrorId, 0, sizeof(m_ErrorId));
	}

	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	bool m_Success;
	char m_Network[256];
	char m_Route[256]; 
	char m_ErrorId[64];
};

//PURPOSE
//	Sent when a user has unlinked their account.
//
// ros.account.unlink
class rlScPresenceAccountUnlinkResult
{
public:
	RLPRESENCE_MESSAGE_DECL("ros.account.unlink");

	rlScPresenceAccountUnlinkResult();
	bool Export(RsonWriter& rw) const;
	bool Import(const RsonReader& rr);

	bool m_Success;
	char m_Network[256];
	char m_Route[256];
	char m_ErrorId[64];
};

//PURPOSE
//  Sent to us when our entitlements have changed.
//
//  "ros.entitlement.changed":1
class rlScPresenceMessageEntitlementsChanged
{
public:

    RLPRESENCE_MESSAGE_DECL("ros.entitlement.changed");

    bool Export(RsonWriter& rw) const;
    bool Import(const RsonReader& rr);
};

} //namespace rage

#endif  //RLINE_RLSCPRESENCEMESSAGE_H
