// 
// rline/rlscpresence.cpp
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscpresence.h"
#include "atl/bitset.h"
#include "rlscpresencemessage.h"
#include "rlscpresencetasks.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/nethardware.h"
#include "net/relay.h"
#include "parser/manager.h"
#include "system/nelem.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, scpresence)
#undef __rage_channel
#define __rage_channel rline_scpresence

//Default amount of time to delay committing presence attributes after
//an attribute changes.  This gives time to coalesce a number of changes
//into a single presence update.
#define RL_SC_PRESENCE_DEFAULT_COMMIT_DELAY_SECS 5

//Minimum interval between doing a complete replace of all of our
//attributes.  Helps avoid spamming the server in poor conditions.
#define RL_SC_PRESENCE_MINIMUM_REPLACE_ATTRS_INTERVAL_SECS  30

PARAM(rlscpresencecommitdelay, "Sets the presence commit delay in seconds.");

//Amount of time to delay committing presence attributes when an attribute changes.
static unsigned s_CommitDelaySec    = RL_SC_PRESENCE_DEFAULT_COMMIT_DELAY_SECS;

static bool s_RlScPresenceInitialized = false;
static bool s_RlScPresenceReadSenderInfo = true;

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
netStatus rlScPresence::sm_DuplicateSigninCheckStatus[RL_MAX_LOCAL_GAMERS];
rlScPresenceAttribute rlScPresence::sm_SigninTimeAttrs[RL_MAX_LOCAL_GAMERS];
bool rlScPresence::sm_SecondDuplicateCheck[RL_MAX_LOCAL_GAMERS];
#endif

const rlScAttributeId rlScAttributeId::PrimaryId        = {"_id", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::PlayerAccountId  = {"pid", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::RockstarId       = {"rid", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::OnlineStatus     = {"onln", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::LastSeen         = {"ts", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::Publishers       = {"pubs", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::Subscribers      = {"subs", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::Titles           = {"titles", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::PresenceServerName   = {"pressrvr", RLSC_ATTRSCOPE_RESERVED|RLSC_ATTRSCOPE_CROSS_TITLE};

const rlScAttributeId rlScAttributeId::GamerTag         = {"gtag", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::ScNickname       = {"scnick", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::RelayIp          = {"rlyip", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::RelayPort        = {"rlyport", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::MappedIp         = {"mapip", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::MappedPort       = {"mapport", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::PeerAddress      = {"peeraddr", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::CrewId           = {"crewid", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::IsGameHost       = {"gshost", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::GameSessionToken = {"gstok", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::GameSessionId	= {"gsid", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::GameSessionInfo  = {"gsinfo", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::IsGameJoinable   = {"gsjoin", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::IsPartyHost      = {"pshost", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::PartySessionToken= {"pstok", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::PartySessionId   = {"psid", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::PartySessionInfo = {"psinfo", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::IsPartyJoinable  = {"psjoin", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::HostedMatchIds   = {"matchids", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::SignInTime		= {"sitime", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::BuildVersion		= {"buildver", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::RichPresence		= {"richp", RLSC_ATTRSCOPE_CROSS_TITLE};
const rlScAttributeId rlScAttributeId::SessionType		= {"gstype", RLSC_ATTRSCOPE_CROSS_TITLE};

const rlScAttributeId* const rlScAttributeId::AllIds[NUM_SCATTR_TYPES] =
{
    &rlScAttributeId::PrimaryId,
    &rlScAttributeId::PlayerAccountId,
    &rlScAttributeId::RockstarId,
    &rlScAttributeId::OnlineStatus,
    &rlScAttributeId::LastSeen,
    &rlScAttributeId::Publishers,
    &rlScAttributeId::Subscribers,
    &rlScAttributeId::Titles,

    &rlScAttributeId::GamerTag,
    &rlScAttributeId::ScNickname,
    &rlScAttributeId::RelayIp,
    &rlScAttributeId::RelayPort,
    &rlScAttributeId::MappedIp,
    &rlScAttributeId::MappedPort,
    &rlScAttributeId::PeerAddress,
    &rlScAttributeId::CrewId,
    &rlScAttributeId::IsGameHost,
    &rlScAttributeId::GameSessionToken,
	&rlScAttributeId::GameSessionId,
	&rlScAttributeId::GameSessionInfo,
	&rlScAttributeId::IsGameJoinable,
	&rlScAttributeId::IsPartyHost,
    &rlScAttributeId::PartySessionToken,
	&rlScAttributeId::PartySessionId,
	&rlScAttributeId::PartySessionInfo,
	&rlScAttributeId::IsPartyJoinable,
	&rlScAttributeId::HostedMatchIds,
	&rlScAttributeId::SignInTime,
	&rlScAttributeId::BuildVersion,
	&rlScAttributeId::RichPresence,
	&rlScAttributeId::SessionType
};

unsigned
rlScAttributeId::GetScope(const char* attrId)
{
    for(int i = 0; i < COUNTOF(AllIds); ++i)
    {
        if(!stricmp(attrId, AllIds[i]->Name))
        {
            return AllIds[i]->Scope;
        }
    }

    return RLSC_ATTRSCOPE_TITLE;
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceAttribute
//////////////////////////////////////////////////////////////////////////
rlScPresenceAttribute::rlScPresenceAttribute()
{
    Clear();
}

void
rlScPresenceAttribute::Clear()
{
    Type = RLSC_PRESTYPE_INVALID;
    IntValue = 0;
	Name[0] = '\0';
	m_IsSet = false;
}

bool
rlScPresenceAttribute::IsValid() const
{
    return (RLSC_PRESTYPE_S64 == Type
            || RLSC_PRESTYPE_DOUBLE == Type
            || RLSC_PRESTYPE_STRING == Type);
}

void
rlScPresenceAttribute::Reset(const char* name, const s64 value)
{
    Clear();
	Type = RLSC_PRESTYPE_S64;
	m_IsSet = true;
    safecpy(Name, name);
    IntValue = value;
}

void
rlScPresenceAttribute::Reset(const char* name, const double value)
{
    Clear();
	Type = RLSC_PRESTYPE_DOUBLE;
	m_IsSet = true;
    safecpy(Name, name);
    DoubleValue = value;
}

void
rlScPresenceAttribute::Reset(const char* name, const char* value)
{
    Clear();

    //Empty string values are ok.

    if(rlVerifyf(strlen(value) < RLSC_PRESENCE_STRING_MAX_SIZE,
                "String attribute %s=\"%s\" is too long (%" SIZETFMT "u) - max size is %d",
                name,
                value,
                strlen(value),
                RLSC_PRESENCE_STRING_MAX_SIZE))
    {
		Type = RLSC_PRESTYPE_STRING;
		m_IsSet = true;
        safecpy(Name, name);
        safecpy(StringValue, value);
    }
}

bool
rlScPresenceAttribute::SetValue(const s64 value)
{
    if(rlVerifyf(RLSC_PRESTYPE_INVALID != Type,
                "Attribute type is unknown - use Reset()")
        && rlVerifyf(RLSC_PRESTYPE_S64 == Type,
                    "Type mismatch on attribute:%s", Name))
    {
		IntValue = value;
		m_IsSet = true;
        return true;
    }

    return false;
}

bool
rlScPresenceAttribute::SetValue(const double value)
{
    if(rlVerifyf(RLSC_PRESTYPE_INVALID != Type,
                "Attribute type is unknown - use Reset()")
        && rlVerifyf(RLSC_PRESTYPE_DOUBLE == Type,
                "Type mismatch on attribute:%s", Name))
    {
		DoubleValue = value;
		m_IsSet = true;
        return true;
    }

    return false;
}

bool
rlScPresenceAttribute::SetValue(const char* value)
{
    //Empty string values are ok.

    if(rlVerifyf(RLSC_PRESTYPE_INVALID != Type,
                "Attribute type is unknown - use Reset()")
        && rlVerifyf(RLSC_PRESTYPE_STRING == Type,
                "Type mismatch on attribute:%s", Name)
        && rlVerifyf(strlen(value) < RLSC_PRESENCE_STRING_MAX_SIZE,
                    "String attribute %s=\"%s\" is too long (%" SIZETFMT "u) - max size is %d",
                    Name,
                    value,
                    strlen(value),
                    RLSC_PRESENCE_STRING_MAX_SIZE))
    {
        safecpy(StringValue, value);
		m_IsSet = true;
        return true;
    }

    return false;
}

bool 
rlScPresenceAttribute::ClearValue()
{
	// clear the value but not the name / type
	// mark as unset
	IntValue = 0;
	m_IsSet = false;

	return true;
}

bool
rlScPresenceAttribute::IsSet() const
{
	return m_IsSet;
}

bool
rlScPresenceAttribute::GetValue(s64* value) const
{
    if(rlVerifyf(RLSC_PRESTYPE_INVALID != Type,
                "Attribute type is unknown")
        && rlVerifyf(RLSC_PRESTYPE_S64 == Type,
                    "Type mismatch on attribute:%s", Name))
    {
        *value = IntValue;
        return true;
    }

    return false;
}

bool
rlScPresenceAttribute::GetValue(double* value) const
{
    if(rlVerifyf(RLSC_PRESTYPE_INVALID != Type,
                "Attribute type is unknown")
        && rlVerifyf(RLSC_PRESTYPE_DOUBLE == Type,
                    "Type mismatch on attribute:%s", Name))
    {
        *value = DoubleValue;
        return true;
    }

    return false;
}

bool
rlScPresenceAttribute::GetValue(char* value, const unsigned sizeofValue) const
{
    if(rlVerifyf(RLSC_PRESTYPE_INVALID != Type,
                "Attribute type is unknown")
        && rlVerifyf(RLSC_PRESTYPE_STRING == Type,
                    "Type mismatch on attribute:%s", Name))
    {
        safecpy(value, StringValue, sizeofValue);
        return true;
    }

    return false;
}

bool
rlScPresenceAttribute::operator==(const rlScPresenceAttribute& that) const
{
	if(!m_IsSet)
	{
		return false;
	}

    if(Type == that.Type)
    {
        switch(Type)
        {
        case RLSC_PRESTYPE_S64:
            return IntValue == that.IntValue;
        case RLSC_PRESTYPE_DOUBLE:
            return DoubleValue == that.DoubleValue;
        case RLSC_PRESTYPE_STRING:
            return !strcmp(StringValue, that.StringValue);
        case RLSC_PRESTYPE_INVALID:
            return false;
        }
    }

    return false;
}

bool
rlScPresenceAttribute::operator==(const s64 val) const
{
	return m_IsSet &&
		RLSC_PRESTYPE_S64 == Type &&
		IntValue == val;
}

bool
rlScPresenceAttribute::operator==(const double val) const
{
	return m_IsSet &&
		RLSC_PRESTYPE_DOUBLE == Type &&
		DoubleValue == val;
}

bool
rlScPresenceAttribute::operator==(const char* val) const
{
    return m_IsSet &&
		RLSC_PRESTYPE_STRING == Type &&
		!strcmp(StringValue, val);
}

int
rlScPresenceAttribute::FindAttr(const char* name,
                                const rlScPresenceAttribute* attrs,
                                const unsigned numAttrs)
{
    for(int i = 0; i < (int)numAttrs; ++i)
    {
        if(0 == stricmp(name, attrs[i].Name))
        {
            return i;
        }
    }

    return -1;
}

//////////////////////////////////////////////////////////////////////////
//  rlScPresenceMessageIterator
//////////////////////////////////////////////////////////////////////////
rlScPresenceMessageIterator::rlScPresenceMessageIterator()
    : m_ParserTree(NULL)
    , m_MsgNode(NULL)
    , m_NumMessagesRetrieved(0)
    , m_TotalMessages(0)
    , m_MessageIndex(0)
    , m_State(STATE_NONE)
{
}

bool
rlScPresenceMessageIterator::NextMessage(const char** message, u64* posixTimeStamp, rlGamerHandle* senderGamerHandle, rlScPresenceMessageSender::Source* source)
{
    if(m_ParserTree)
    {
        if(NextMessage(m_ParserTree->GetRoot(), &m_MsgNode, message, posixTimeStamp, senderGamerHandle, source))
        {
            rlDebug2("  Message %d: source: %s, msg: '%s'",
				m_MessageIndex,
				(*source == rlScPresenceMessageSender::Source::SENDER_PLAYER) ?
					senderGamerHandle->ToString() :
					rlScPresenceMessageSender::GetSourceName(*source),
				*message);

            ++m_MessageIndex;
            return true;
        }
        else
        {
            ReleaseResources();
        }
    }

    return false;
}

unsigned
rlScPresenceMessageIterator::GetNumMessagesRetrieved() const
{
    return m_NumMessagesRetrieved;
}

unsigned
rlScPresenceMessageIterator::GetTotalMessages() const
{
    return m_TotalMessages;
}

void
rlScPresenceMessageIterator::ReleaseResources()
{
    if(m_State > STATE_NONE)
    {
        if(m_ParserTree)
        {
            delete m_ParserTree;
            m_ParserTree = NULL;
        }

        SHUTDOWN_PARSER;

        m_MsgNode = NULL;
        m_NumMessagesRetrieved = 0;
        m_TotalMessages = 0;
        m_MessageIndex = 0;
        m_State = STATE_NONE;
    }
}

bool
rlScPresenceMessageIterator::IsReleased() const
{
    return STATE_NONE == m_State;
}

//private:

bool
rlScPresenceMessageIterator::Begin()
{
    rlAssertf(IsReleased(), "Iterator has not been released - need to call rlScPresenceMessageIterator::ReleaseResources()");

    ReleaseResources();

    INIT_PARSER;
    m_State = STATE_INITIALIZED;
    return true;
}

bool
rlScPresenceMessageIterator::NextMessage(const parTreeNode* rootNode,
                                        const parTreeNode** msgNode,
                                        const char** message,
                                        u64* posixTimeStamp, 
										rlGamerHandle* senderGamerHandle,
										rlScPresenceMessageSender::Source* source)
{
    rtry
    {
		*source = rlScPresenceMessageSender::Source::SENDER_UNKNOWN;
		senderGamerHandle->Clear();

        if(NULL == *msgNode)
        {
            const parTreeNode* messagesNode = rootNode->FindChildWithName("Messages");
            rcheck(messagesNode, catchall, 
                    rlError("Failed to find <Messages> element"));

            *msgNode = messagesNode->GetChild();
        }
        else
        {
            *msgNode = (*msgNode)->GetSibling();
        }

        if(*msgNode)
        {
            const parElement* el = &(*msgNode)->GetElement();

            //Timestamp
            const parAttribute* attr = el->FindAttribute("t");
            rverify(attr, catchall, rlError("Failed to find 't' message attribute"));
            const char* tsStr = attr->GetStringValue();
            rverify(1 == sscanf(tsStr, "%" I64FMT "u", posixTimeStamp),
                    catchall,
                    rlError("Failed to parse timestamp: %s", tsStr));

            //Message
            *message = (*msgNode)->GetData();
            rverify(*message, catchall, rlError("No message data"));

			if(s_RlScPresenceReadSenderInfo)
			{
				//Server flag
				const parAttribute* svc = el->FindAttribute("svc");
				if(svc)
				{
					const char* svcStr = svc->GetStringValue();
					if(svcStr && (stricmp(svcStr, "true") == 0))
					{
						*source = rlScPresenceMessageSender::Source::SENDER_SERVER;
					}
				}

				if(*source == rlScPresenceMessageSender::Source::SENDER_UNKNOWN)
				{
					// sender's authenticated gamer handle
					const parAttribute* sgh = el->FindAttribute("sgh");
					if(sgh)
					{
						const char* ghStr = sgh->GetStringValue();
						if(ghStr)
						{
							if(!senderGamerHandle->FromString(ghStr) || !senderGamerHandle->IsValid())
							{
								senderGamerHandle->Clear();
							}

							if(senderGamerHandle->IsValid())
							{
								*source = rlScPresenceMessageSender::Source::SENDER_PLAYER;
							}
						}					
					}
				}
			}
			
            return true;
        }
    }
    rcatchall
    {
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
//  rlScSubscription
//////////////////////////////////////////////////////////////////////////
class rlScSubscription
{
public:

    rlScSubscription()
    {
        m_Channel[0] = '\0';
    }

    bool Init(const char* channel)
    {
        if(rlVerifyf(strlen(channel) < RLSC_PRESENCE_CHANNEL_NAME_MAX_SIZE,
                    "Channel name '%s' is too long", channel))
        {
            safecpy(m_Channel, channel);
            return true;
        }

        return false;
    }

    char m_Channel[RLSC_PRESENCE_CHANNEL_NAME_MAX_SIZE];
};

#if SC_PRESENCE

//////////////////////////////////////////////////////////////////////////
//  rlScPlayerPresence
//////////////////////////////////////////////////////////////////////////
class rlScPlayerPresence
{
public:

    rlScPlayerPresence()
    {
        Clear();
    }

    void Clear()
    {
        if(m_CommitAttrsStatus.Pending())
        {
            rlGetTaskManager()->CancelTask(&m_CommitAttrsStatus);
        }

        if(m_CommitSubscribeStatus.Pending())
        {
            rlGetTaskManager()->CancelTask(&m_CommitSubscribeStatus);
        }

        if(m_CommitUnsubscribeStatus.Pending())
        {
            rlGetTaskManager()->CancelTask(&m_CommitUnsubscribeStatus);
        }

        for(int i = 0; i < RLSC_PRESENCE_MAX_ATTRIBUTES; ++i)
        {
            m_Attributes[i].Clear();
            m_CommittedAttributes[i].Clear();
        }

        AttrSetAllDirty(false);
        SubSetAllDirty(false);
        m_StopWatch.Reset();
        m_NumAttributes = 0;
        m_NumCommittedAttributes = 0;
        m_NumSubscriptions = 0;
        m_LastRefreshAttrsPosixTime = 0;
		m_SigninTime = 0;
		m_DuplicateSigninCheckTime = 0;
        m_SignedInLocally = false;
        m_SignedOnline = false;
		m_HasReplacedAttrs = false;
    }

    template<typename T>
    int AddAttr(const char* name, const T value)
    {
        if(rlVerifyf(m_NumAttributes < RLSC_PRESENCE_MAX_ATTRIBUTES,
                    "Attribute \"%s\" exceeded max of %d",
                    name,
                    RLSC_PRESENCE_MAX_ATTRIBUTES))
        {
			rlDebug3("  Adding attribute %s(%d)", name, m_NumAttributes);
            m_Attributes[m_NumAttributes].Reset(name, value);
            ++m_NumAttributes;
            AttrSetDirty(m_NumAttributes-1, true, true);
            return m_NumAttributes-1;
        }

        return -1;
    }

    template<typename T>
    bool SetValue(const int index, const T value, const bool bNeedsCommit)
    {
        if(rlVerifyf(index >= 0 && index < m_NumAttributes, "Invalid attribute index: %d", index)
            && rlVerifyf(m_Attributes[index].IsValid(), "Invalid attribute: %d", index))
        {
            if(!m_Attributes[index].IsSet() || m_Attributes[index] != value)
            {
                if(rlVerify(m_Attributes[index].SetValue(value)))
                {
                    if(index >= m_NumCommittedAttributes
                        || m_Attributes[index] != m_CommittedAttributes[index])
                    {
                        AttrSetDirty(index, true, bNeedsCommit);
                    }
                    else
                    {
                        //Value is the same as the already committed value
                        AttrSetDirty(index, false, false);
                    }

                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        return false;
    }

	bool ClearValue(const int index)
	{
		if(rlVerifyf(index >= 0 && index < m_NumAttributes, "Invalid attribute index: %d", index))
		{
			// only needed if it's currently set to something
			if(m_Attributes[index].IsSet())
			{
				// retain the attribute but clear out the value and flag it as dirty (which will clear it on the server)
				if(m_Attributes[index].ClearValue())
				{
					AttrSetDirty(index, true, true);
					return true;
				}
			}
		}

		return false;
	}

    template<typename T>
    bool GetValue(const int index, T* value) const
    {
        if(rlVerifyf(index >= 0 && index < m_NumAttributes, "Invalid attribute index: %d", index) &&
			m_Attributes[index].IsSet() &&
            rlVerify(m_Attributes[index].GetValue(value)))
        {
            return true;
        }

        return false;
    }

    bool GetValue(const int index, char* value, const unsigned sizeofValue) const
    {
		if(rlVerifyf(index >= 0 && index < m_NumAttributes, "Invalid attribute index: %d", index) &&
			m_Attributes[index].IsSet() &&
            rlVerify(m_Attributes[index].GetValue(value, sizeofValue)))
        {
            return true;
        }

        return false;
    }

    template<int SIZE>
    bool GetValue(const int index, char (&value)[SIZE]) const
    {
        return GetValue(index, value, SIZE);
    }

    void AttrSetDirty(const int index, const bool dirty, const bool bNeedsCommit)
    {
        if(rlVerifyf(index >= 0 && index < m_NumAttributes,
                    "Invalid attribute index: %d", index))
        {
            if(dirty)
            {
                m_DirtyAttrs.Set(index);

				if (bNeedsCommit)
				{
					//Only start the stopwatch if we have something that is 
					//dirty AND needs to be committed
					if(!m_StopWatch.IsRunning())
					{
						m_StopWatch.Restart();
					}
				}
            }
            else
            {
                m_DirtyAttrs.Clear(index);
            }
        }
    }

    void AttrSetAllDirty(const bool dirty)
    {
        //We should call SetAllDirty(true) only when we're signed in.
        rlAssert(!dirty || m_SignedInLocally);

        for(int i = 0; i < m_NumAttributes; ++i)
        {
            AttrSetDirty(i, dirty, dirty);
        }
    }

    bool AttrIsDirty(const int index) const
    {
        if(rlVerifyf(index >= 0 && index < m_NumAttributes,
                    "Invalid attribute index: %d", index))
        {
            return m_DirtyAttrs.IsSet(index);
        }

        return false;
    }

    bool AttrAreAnyDirty() const
    {
        return m_DirtyAttrs.AreAnySet();
    }

    bool Subscribe(const char* channel)
    {
        if(IsSubscribed(channel))
        {
            return true;
        }
        else
        {
            for(int i = 0; i < m_NumSubscriptions; ++i)
            {
                //Do we have this channel in our list, but marked "unsubscribed"?
                if(0 == strcmp(channel, m_Subscriptions[i].m_Channel)
                    && !SubIsSubscribed(i))
                {
                    SubSetSubscribed(i, true);
                    SubSetDirty(i, true);
                    return true;
                }
            }

            //Add a new subscription
            if(rlVerifyf(m_NumSubscriptions < RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS,
                        "Cannot subscribe to '%s', too many subscriptions", channel)
                && m_Subscriptions[m_NumSubscriptions].Init(channel))
            {
                ++m_NumSubscriptions;
                SubSetSubscribed(m_NumSubscriptions-1, true);
                SubSetDirty(m_NumSubscriptions-1, true);

                return true;
            }
        }

        return false;
    }

    bool Unsubscribe(const char* channel)
    {
        //Unsubscribing doesn't remove the subscription from
        //the list, it just marks it as unsubscribed.
        //When we next commit subscriptions to the backend server
        //we'll "collect the garbage" and removed unsubscribed
        //channels from the list.
        for(int i = 0; i < m_NumSubscriptions; ++i)
        {
            if(0 == strcmp(channel, m_Subscriptions[i].m_Channel)
                && SubIsSubscribed(i))
            {
                SubSetSubscribed(i, false);
                SubSetDirty(i, true);
                return true;
            }
        }

        return true;
    }

    bool UnsubscribeAll()
    {
        for(int i = 0; i < m_NumSubscriptions; ++i)
        {
            if(SubIsSubscribed(i))
            {
                SubSetSubscribed(i, false);
                SubSetDirty(i, true);
            }
        }

        return true;
    }

    bool IsSubscribed(const char* channel)
    {
        for(int i = 0; i < m_NumSubscriptions; ++i)
        {
            if(0 == strcmp(channel, m_Subscriptions[i].m_Channel))
            {
                return SubIsSubscribed(i);
            }
        }

        return false;
    }

    void SubSetDirty(const int index, const bool dirty)
    {
        if(rlVerifyf(index >= 0 && index < m_NumSubscriptions,
                    "Invalid subscription index: %d", index))
        {
            if(dirty)
            {
                if(!m_StopWatch.IsRunning())
                {
                    m_StopWatch.Restart();
                }

                m_DirtySubs.Set(index);
            }
            else
            {
                m_DirtySubs.Clear(index);
            }
        }
    }

    void SubSetAllDirty(const bool dirty)
    {
        //We should call SetAllDirty(true) only when we're signed in.
        rlAssert(!dirty || m_SignedInLocally);

        for(int i = 0; i < m_NumSubscriptions; ++i)
        {
            SubSetDirty(i, dirty);
        }
    }

    bool SubIsDirty(const int index) const
    {
        if(rlVerifyf(index >= 0 && index < m_NumSubscriptions,
                    "Invalid subscription index: %d", index))
        {
            return m_DirtySubs.IsSet(index);
        }

        return false;
    }

    bool SubAreAnyDirty() const
    {
        return m_DirtySubs.AreAnySet();
    }

    void SubSetSubscribed(const int index, const bool subscribed)
    {
        if(SubIsSubscribed(index) != subscribed)
        {
            if(rlVerifyf(index >= 0 && index < m_NumSubscriptions,
                        "Invalid subscription index: %d", index))
            {
                m_Subscribed.Set(index, subscribed);
            }
        }
    }

    bool SubIsSubscribed(const int index) const
    {
        if(rlVerifyf(index >= 0 && index < m_NumSubscriptions,
                    "Invalid subscription index: %d", index))
        {
            return m_Subscribed.IsSet(index);
        }

        return false;
    }

    void CollectGarbage()
    {
        for(int i = 0; i < m_NumSubscriptions; ++i)
        {
            //Remove subscriptions that have been marked as
            //unsubscribed and have been synched to the server.
            if(!SubIsSubscribed(i) && !SubIsDirty(i))
            {
                m_Subscriptions[i] = m_Subscriptions[m_NumSubscriptions-1];

                SubSetSubscribed(i, SubIsSubscribed(m_NumSubscriptions-1));
                SubSetDirty(i, SubIsDirty(m_NumSubscriptions-1));

                SubSetSubscribed(m_NumSubscriptions-1, false);
                SubSetDirty(m_NumSubscriptions-1, false);

                --m_NumSubscriptions;
                --i;
            }
        }
    }

    bool IsDirty() const
    {
        return m_StopWatch.IsRunning();
    }

    bool NeedToCommit() const
    {
        if(m_CommitAttrsStatus.Pending())
        {
            //Commit already pending
            return false;
        }
        else if(m_CommitSubscribeStatus.Pending())
        {
            //Commit already pending
            return false;
        }
        else if(m_CommitUnsubscribeStatus.Pending())
        {
            //Commit already pending
            return false;
        }
        else if(IsDirty() && m_StopWatch.GetElapsedMilliseconds() >= (s_CommitDelaySec*1000))
        {
            //Have dirty attributes and the throttle timer
            //has expired.
            return true;
        }

        return false;
    }

    bool NeedToSignOnline() const
    {
        return m_SignedInLocally && !m_SignedOnline;
    }

	bool NeedsToReplaceAttrs() const
	{
		return m_HasReplacedAttrs == false;
	}
	
	void SetSignInTime(u64 time) { m_SigninTime = time; }
	void SetDuplicateSigninCheckTime(u64 time) { m_DuplicateSigninCheckTime = time; }

    u64 m_LastRefreshAttrsPosixTime;   //Last time we did a complete refresh of attributes
    netStopWatch m_StopWatch;
    int m_NumAttributes;
    int m_NumSubscriptions;

    int m_NumCommittedAttributes;

    rlScPresenceAttribute m_Attributes[RLSC_PRESENCE_MAX_ATTRIBUTES];
    rlScSubscription m_Subscriptions[RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS];

    rlScPresenceAttribute m_CommittedAttributes[RLSC_PRESENCE_MAX_ATTRIBUTES];

	u64 m_SigninTime;
	u64 m_DuplicateSigninCheckTime;

    atFixedBitSet<RLSC_PRESENCE_MAX_ATTRIBUTES, u32> m_DirtyAttrs;
    atFixedBitSet<RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS, u32> m_Subscribed;
    atFixedBitSet<RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS, u32> m_DirtySubs;
    netStatus m_CommitAttrsStatus;
    netStatus m_CommitSubscribeStatus;
    netStatus m_CommitUnsubscribeStatus;
    bool m_SignedInLocally  : 1;
    bool m_SignedOnline     : 1;
	bool m_HasReplacedAttrs : 1;
};

static rlScPlayerPresence s_PlayerPres[RL_MAX_LOCAL_GAMERS];

//////////////////////////////////////////////////////////////////////////
//  rlScPresence
//////////////////////////////////////////////////////////////////////////
bool
rlScPresence::Init()
{
    rlDebug("Initializing...");
    if(rlVerifyf(!s_RlScPresenceInitialized, "Already initialized"))
    {
        if(!PARAM_rlscpresencecommitdelay.Get(s_CommitDelaySec))
        {
            s_CommitDelaySec = RL_SC_PRESENCE_DEFAULT_COMMIT_DELAY_SECS;
        }

        rlDebug("Commit interval is %u seconds", s_CommitDelaySec);

        s_RlScPresenceInitialized = true;

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
		DuplicateSigninInit();
#endif

        rlDebug("Initialized");
    }

    return s_RlScPresenceInitialized;
}

bool
rlScPresence::IsInitialized()
{
    return s_RlScPresenceInitialized;
}

void rlScPresence::SetAllowReadingSenderInfo(const bool readSenderInfo)
{
	if(s_RlScPresenceReadSenderInfo != readSenderInfo)
	{
		rlDebug("SetAllowReadingSenderInfo :: %s", readSenderInfo ? "true" : "false");
		s_RlScPresenceReadSenderInfo = readSenderInfo;
	}
}

void
rlScPresence::Shutdown()
{
    if(s_RlScPresenceInitialized)
    {
        rlDebug("Shutting down...");

        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
        {
            s_PlayerPres[i].Clear();
        }

        s_RlScPresenceInitialized = false;

        rlDebug("Shut down");
    }
}

ASSERT_ONLY(static netStopWatch s_TimeSinceLastUpdate(true));

void
rlScPresence::Update()
{
    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
#if __ASSERT
        const unsigned msSinceLastUpdate =
            s_TimeSinceLastUpdate.GetElapsedMilliseconds();
        s_TimeSinceLastUpdate.Restart();
#endif

        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
        {
            if(rlRos::IsOnline(i) && rlRos::HasPrivilege(i, RLROS_PRIVILEGEID_PRESENCE_WRITE))
            {
#if __ASSERT
                //If we're stuck in an assert then the presence
                //system will sign us out.  If that happens we need
                //to refresh all of our presence attributes.
                if(s_PlayerPres[i].m_SignedOnline
                    && msSinceLastUpdate > netRelay::GetPresencePingIntervalMs()*2)
                {
                    s_PlayerPres[i].AttrSetAllDirty(true);
                }
#endif

                if(s_PlayerPres[i].NeedToCommit())
                {
                    CommitState(i);
                    //FIXME(KB) handle failures and add a retry
                    s_PlayerPres[i].AttrSetAllDirty(false);
                    s_PlayerPres[i].SubSetAllDirty(false);
                    s_PlayerPres[i].CollectGarbage();
                    s_PlayerPres[i].m_SignedOnline = true;
                }

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
				DuplicateSigninUpdate(i);
#endif
            }
        }
    }
}

bool
rlScPresence::SignIn(const int localGamerIndex)
{
    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
        && rlVerifyf(!s_PlayerPres[localGamerIndex].m_SignedInLocally,
                    "Gamer: %d is already signed in", localGamerIndex))
    {
        rlDebug("Gamer: %d signed in locally", localGamerIndex);

        s_PlayerPres[localGamerIndex].Clear();
        s_PlayerPres[localGamerIndex].m_SignedInLocally = true;

        s_PlayerPres[localGamerIndex].m_StopWatch.Restart();

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
		DuplicateSigninCheck(localGamerIndex);
#endif

        return true;
    }

    return false;
}

bool
rlScPresence::SignOffline(const int localGamerIndex, bool bDuplicateSignOut)
{
    bool success = false;

	rlDebug("Gamer: %d signing offline... (duplicate: %s)", localGamerIndex, bDuplicateSignOut ? "true" : "false");

    rlFireAndForgetTask<rlScSignOutTask>* task = NULL;

    rtry
    {
        rverify(s_RlScPresenceInitialized, catchall, rlError("Not initialized"));

        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        if(!s_PlayerPres[localGamerIndex].m_SignedOnline)
        {
            rlWarning("Gamer: %d is already offline", localGamerIndex);
        }
        else
        {
			s_PlayerPres[localGamerIndex].m_SignedOnline = false;
			s_PlayerPres[localGamerIndex].m_HasReplacedAttrs = false;

            // once the player signs offline we can no longer
            // access the network, so no sense trying to sign out of presence
			// also, don't bother signing out of ScPresence when duplicate signin is detected,
			// we're just clearing the duplicate signin person
            if(netHardware::IsAvailable() && rlRos::GetCredentials(localGamerIndex).IsValid() && !bDuplicateSignOut)
            {
                rverify(rlGetTaskManager()->CreateTask(&task),
                        catchall,
                        rlError("Error creating task"));

                rverify(rlTaskBase::Configure(task, localGamerIndex, &task->m_Status),
                        catchall,
                        rlError("Error configuring task"));

                rverify(rlGetTaskManager()->AppendSerialTask((size_t)&s_PlayerPres[localGamerIndex], task),
                        catchall,
                        rlError("Error queuing task"));
            }
        }

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
	DuplicateSigninReset(localGamerIndex);
#endif

    return success;
}

bool
rlScPresence::SignOut(const int localGamerIndex, bool bDuplicateSignOut)
{
    bool success = false;

	rlDebug("Gamer: %d signing out completely... (duplicate: %s)", localGamerIndex, bDuplicateSignOut ? "true" : "false");

    rtry
    {
        rverify(s_RlScPresenceInitialized, catchall, rlError("Not initialized"));

        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        if(s_PlayerPres[localGamerIndex].m_SignedOnline)
        {
            rlScPresence::SignOffline(localGamerIndex, bDuplicateSignOut);
        }

        s_PlayerPres[localGamerIndex].Clear();

        success = true;
    }
    rcatchall
    {
    }

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
	DuplicateSigninReset(localGamerIndex);
#endif

    return success;
}

bool
rlScPresence::IsSignedInLocally(const int localGamerIndex)
{
	if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
        //If not signed online then can't be signed in locally.
        rlAssert(!s_PlayerPres[localGamerIndex].m_SignedOnline
                || s_PlayerPres[localGamerIndex].m_SignedInLocally);

		return s_PlayerPres[localGamerIndex].m_SignedInLocally;
	}

	return false;
}

bool
rlScPresence::IsSignedOnline(const int localGamerIndex)
{
	if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
        //If not signed in locally can't be signed online
        rlAssert(s_PlayerPres[localGamerIndex].m_SignedInLocally
                || !s_PlayerPres[localGamerIndex].m_SignedOnline);

		return s_PlayerPres[localGamerIndex].m_SignedOnline;
	}

	return false;
}

bool
rlScPresence::IsServiceReady(int localGamerIndex)
{
	if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
		return IsSignedOnline(localGamerIndex);
	}

	return false;
}

void
rlScPresence::ClearAttributes(const int localGamerIndex)
{
    rlDebug2("Clearing attributes for local gamer:%d", localGamerIndex);

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    "Invalid gamer index: %d", localGamerIndex))
        {
            s_PlayerPres[localGamerIndex].Clear();
        }
    }
}

#if !__NO_OUTPUT
template<int SIZE>
static inline const char* AttrValToString(const s64 value, char (&buf)[SIZE])
{
    return formatf(buf, "%" I64FMT "d", value);
}
template<int SIZE>
static inline const char* AttrValToString(const double value, char (&buf)[SIZE])
{
    return formatf(buf, "%g", value);
}
template<int SIZE>
static inline const char* AttrValToString(const char* value, char (&buf)[SIZE])
{
    return formatf(buf, "%s", value);
}
#endif

template<typename T>
static bool SetAttributeT(const int localGamerIndex,
                        const char* name,
                        const T value,
						const bool bNeedsCommitIfDirty)
{
    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    "Invalid gamer index: %d", localGamerIndex)
            && rlVerifyf(s_PlayerPres[localGamerIndex].m_SignedInLocally,
                        "Gamer: %d isn't signed in",
                        localGamerIndex))
        {
            rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];
            int index =
                rlScPresenceAttribute::FindAttr(name, player->m_Attributes, player->m_NumAttributes);

            bool success;

            if(index >= 0)
            {
                success = player->SetValue(index, value, bNeedsCommitIfDirty);
            }
            else
            {
                index = player->AddAttr(name, value);
                success = (index >= 0);
            }

			// this should always be valid after a set
			rlAssert(index >= 0 && player->m_Attributes[index].IsValid());

            OUTPUT_ONLY(char buf[256]);
            rlDebug2("  %s(%d)=%s %s %s",
                    name,
                    index,
                    AttrValToString(value, buf),
                    success ? "succeeded" : "failed",
					bNeedsCommitIfDirty ? "" : "no commit");
            if(success)
            {
                rlDebug2("  Total attributes = %d", player->m_NumAttributes);
            }

            return success;
        }
    }

    return false;
}

bool
rlScPresence::SetIntAttribute(const int localGamerIndex,
                            const char* name,
                            const s64 value,
							const bool bNeedsCommitIfDirty)
{
    s64 oldVal;
    if(GetIntAttribute(localGamerIndex, name, &oldVal) && oldVal == value)
    {
        return true;
    }

    rlDebug2("Setting attribute: gamer(%d), %s=%" I64FMT "d",
            localGamerIndex,
            name,
            value);

    return SetAttributeT(localGamerIndex, name, value, bNeedsCommitIfDirty);
}

bool
rlScPresence::SetDoubleAttribute(const int localGamerIndex,
                                const char* name,
								const double value,
								const bool bNeedsCommitIfDirty)
{
    double oldVal;
    if(GetDoubleAttribute(localGamerIndex, name, &oldVal) && oldVal == value)
    {
        return true;
    }

    rlDebug2("Setting attribute: gamer(%d), %s=%g",
            localGamerIndex,
            name,
            value);

    return SetAttributeT(localGamerIndex, name, value, bNeedsCommitIfDirty);
}

bool
rlScPresence::SetStringAttribute(const int localGamerIndex,
                                const char* name,
								const char* value,
								const bool bNeedsCommitIfDirty)
{
    char oldVal[RLSC_PRESENCE_STRING_MAX_SIZE];
    if(GetStringAttribute(localGamerIndex, name, oldVal) && !strcmp(oldVal, value))
    {
        return true;
    }

    rlDebug2("Setting attribute: gamer(%d), %s=%s",
            localGamerIndex,
            name,
            value);

    return rlVerifyf(value, "Invalid value for attribute: %s", name)
            && SetAttributeT(localGamerIndex, name, value, bNeedsCommitIfDirty);
}

bool
rlScPresence::ClearAttribute(const int localGamerIndex, const char* name)
{
	if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
		"Invalid gamer index: %d", localGamerIndex))
	{
		rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];

		const int index = rlScPresenceAttribute::FindAttr(name, player->m_Attributes, player->m_NumAttributes);
		if(index >= 0)
		{
			return player->ClearValue(index);
		}
		else
		{
			rlWarning("Tried to clear attribute %s for gamer index %d but was not found", name, localGamerIndex);
		}
	}
	return false;
}

bool
rlScPresence::HasAttribute(const int localGamerIndex, const char* name)
{
	if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
		"Invalid gamer index: %d", localGamerIndex))
	{
		rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];
		return rlScPresenceAttribute::FindAttr(name, player->m_Attributes, player->m_NumAttributes) > 0;
	}
	return false;
}

bool
rlScPresence::IsAttributeSet(const int localGamerIndex, const char* name)
{
	if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
		"Invalid gamer index: %d", localGamerIndex))
	{
		rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];
		const int index = rlScPresenceAttribute::FindAttr(name, player->m_Attributes, player->m_NumAttributes);
		if (index >= 0)
		{
			return player->m_Attributes[index].IsValid();
		}
	}
	return false;
}

void
rlScPresence::ForceRefresh(const int localGamerIndex)
{
    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    "Invalid gamer index: %d", localGamerIndex))
        {
            rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];
            const u64 curPosixTime = rlGetPosixTime();
            if(0 == player->m_LastRefreshAttrsPosixTime
                || (curPosixTime - player->m_LastRefreshAttrsPosixTime) >= RL_SC_PRESENCE_MINIMUM_REPLACE_ATTRS_INTERVAL_SECS)
            {
                rlDebug("Replacing all presence attributes for gamer %d...", localGamerIndex);
                player->AttrSetAllDirty(true);
                player->SubSetAllDirty(true);
                player->m_LastRefreshAttrsPosixTime = curPosixTime | 0x01;
                player->m_NumCommittedAttributes = 0;

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS
				// Do not mark the signin time as dirty, this could have been set by another user signing in.
				// We want a solid read of this value to determine if we should neuter ourselves
				int index = rlScPresenceAttribute::FindAttr(rlScAttributeId::SignInTime.Name, player->m_Attributes, player->m_NumAttributes);
				if (index >= 0)
				{
					rlDebug("ForceRefresh %s as not dirty", rlScAttributeId::SignInTime.Name);
					player->SetValue(index, (s64)0, false);
					player->AttrSetDirty(index, false, false);
				}
#endif
            }
            else
            {
                rlDebug("Request to replace all presence attributes for gamer %d came too soon - ignoring.", localGamerIndex);
            }
        }
    }
}

template<typename T>
static bool GetAttributeT(const int localGamerIndex,
                            const char* name,
                            T* value)
{
    *value = 0;
    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    "Invalid gamer index: %d", localGamerIndex))
        {
            rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];
            const int index =
                rlScPresenceAttribute::FindAttr(name, player->m_Attributes, player->m_NumAttributes);
            if(index >= 0)
            {
                return player->GetValue(index, value);
            }
        }
    }

    return false;
}

static bool GetAttributeT(const int localGamerIndex,
						  const char* name,
						  char* value,
						  const unsigned sizeofValue)
{
	*value = 0;
	if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
	{
		if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
			"Invalid gamer index: %d", localGamerIndex))
		{
			rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];
			const int index =
				rlScPresenceAttribute::FindAttr(name, player->m_Attributes, player->m_NumAttributes);
			if(index >= 0)
			{
				return player->GetValue(index, value, sizeofValue);
			}
		}
	}

	return false;
}

bool
rlScPresence::GetIntAttribute(const int localGamerIndex,
                            const char* name,
                            s64* value)
{
    return GetAttributeT(localGamerIndex, name, value);
}

bool
rlScPresence::GetDoubleAttribute(const int localGamerIndex,
                            const char* name,
                            double* value)
{
    return GetAttributeT(localGamerIndex, name, value);
}

bool
rlScPresence::GetStringAttribute(const int localGamerIndex,
								 const char* name,
								 char* value,
								 const unsigned sizeofValue)
{
	return GetAttributeT(localGamerIndex, name, value, sizeofValue);
}

bool
rlScPresence::GetAttributesForGamer(const int localGamerIndex,
                                    const rlGamerHandle& gamerHandle,
                                    rlScPresenceAttribute* attrs,
                                    const unsigned numAttrs,
                                    netStatus* status)
{
    bool success = false;

    OUTPUT_ONLY(char ghStr[RL_MAX_GAMER_HANDLE_CHARS]);
    rlDebug2("Retrieving presence attributes for %s...",
            gamerHandle.ToString(ghStr));

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        rlScGetPresenceTask* task = NULL;

        rtry
        {
            rverify(gamerHandle.IsValidForRos(),
                    catchall,
                    rlError("Invalid gamer handle"));

            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error creating task"));

            rverify(rlTaskBase::Configure(task, localGamerIndex, gamerHandle, attrs, numAttrs, status),
                    catchall,
                    rlError("Error configuring task"));

            rverify(rlGetTaskManager()->AddParallelTask(task),
                    catchall,
                    rlError("Error queuing task"));

            success = true;
        }
        rcatchall
        {
            if(task)
            {
                rlGetTaskManager()->DestroyTask(task);
            }
        }
    }

    return success;
}

bool
rlScPresence::GetAttributesForGamers(const int localGamerIndex,
									const rlGamerHandle* gamerHandles,
									const unsigned numGamerHandles,
									rlScPresenceAttribute* attrs[],
									const unsigned numAttrs,
									netStatus* status)
{
    bool success = false;

    rlDebug2("Retrieving %u presence attributes for %u gamers...", numAttrs, numGamerHandles);

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        rlScGetPresenceForGamersTask* task = NULL;

        rtry
        {
            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error creating task"));

            rverify(rlTaskBase::Configure(task, localGamerIndex, gamerHandles, numGamerHandles, attrs, numAttrs, status),
                    catchall,
                    rlError("Error configuring task"));

            rverify(rlGetTaskManager()->AddParallelTask(task),
                    catchall,
                    rlError("Error queuing task"));

            success = true;
        }
        rcatchall
        {
            if(task)
            {
                rlGetTaskManager()->DestroyTask(task);
            }
        }
    }

    return success;
}

bool
rlScPresence::Subscribe(const int localGamerIndex,
                         const char** channels,
                         const unsigned numChannels)
{
#if !__NO_OUTPUT
    rlDebug("Local gamer %d subscribing to:", localGamerIndex);
    for(int i = 0; i < (int)numChannels; ++i)
    {
        rlDebug("  %s", channels[i]);
    }
#endif

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    "Invalid gamer index: %d", localGamerIndex)
            && rlVerifyf(s_PlayerPres[localGamerIndex].m_SignedInLocally,
                        "Gamer: %d isn't signed in",
                        localGamerIndex))
        {
            rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];

            for(int i = 0; i < (int)numChannels; ++i)
            {
                rlVerifyf(player->Subscribe(channels[i]),
                    "Failed to subscribe to '%s'", channels[i]);
            }

            return true;
        }
    }

    return false;
}

bool
rlScPresence::Unsubscribe(const int localGamerIndex,
                            const char** channels,
                            const unsigned numChannels)
{
#if !__NO_OUTPUT
    rlDebug("Local gamer %d unsubscribing from:", localGamerIndex);
    for(int i = 0; i < (int)numChannels; ++i)
    {
        rlDebug("  %s", channels[i]);
    }
#endif

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    "Invalid gamer index: %d", localGamerIndex)
            && rlVerifyf(s_PlayerPres[localGamerIndex].m_SignedInLocally,
                        "Gamer: %d isn't signed in",
                        localGamerIndex))
        {
            rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];

            for(int i = 0; i < (int)numChannels; ++i)
            {
                rlVerifyf(player->Unsubscribe(channels[i]),
                    "Failed to subscribe to '%s'", channels[i]);
            }

            return true;
        }
    }

    return false;
}

bool
rlScPresence::UnsubscribeAll(const int localGamerIndex)
{
    rlDebug("Local gamer %d unsubscribing from all channels", localGamerIndex);

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        if(rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    "Invalid gamer index: %d", localGamerIndex)
            && rlVerifyf(s_PlayerPres[localGamerIndex].m_SignedInLocally,
                        "Gamer: %d isn't signed in",
                        localGamerIndex))
        {
            rlScPlayerPresence* player = &s_PlayerPres[localGamerIndex];

            return player->UnsubscribeAll();
        }
    }

    return false;
}

bool
rlScPresence::Publish(const int localGamerIndex,
                        const char** channels,
                        const unsigned numChannels,
                        const char* filterName,
                        const char* paramNameValueCsv,
                        const char* message)
{
    bool success = false;

#if !__NO_OUTPUT
    rlDebug("Local gamer %d publishing '%s' to:",
            localGamerIndex,
            message);
    for(int i = 0; i < (int)numChannels; ++i)
    {
        rlDebug("  %s", channels[i]);
    }
#endif

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        rlFireAndForgetTask<rlScPublishTask>* task = NULL;

        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    catchall,
                    rlError("Invalid gamer index: %d", localGamerIndex));

            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error creating task"));

            rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        channels,
                                        numChannels,
                                        filterName,
                                        paramNameValueCsv,
                                        message,
                                        &task->m_Status),
                    catchall,
                    rlError("Error configuring task"));

            rverify(rlGetTaskManager()->AddParallelTask(task),
                    catchall,
                    rlError("Error queuing task"));

            success = true;
        }
        rcatchall
        {
            if(task)
            {
                rlGetTaskManager()->DestroyTask(task);
            }
        }
    }

    return success;
}

bool
rlScPresence::PostMessage(
    const int localGamerIndex,
    const rlGamerHandle* recipients,
    const unsigned numRecipients,
    const char* message,
    const unsigned ttlSeconds)
{
    bool success = false;

#if !__NO_OUTPUT
    rlDebug("Posting '%s' to: %d recipients from local gamer: %d...",
            message,
            numRecipients,
            localGamerIndex);
#endif  //__NO_OUTPUT

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        rlFireAndForgetTask<rlScPostMessageTask>* task = NULL;

        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    catchall,
                    rlError("Invalid gamer index: %d", localGamerIndex));

			rverify(message != NULL && istrlen(message) > 0, 
				catchall, 
				rlError("Message Is Empty"));

            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error creating task"));

            rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        recipients,
                                        numRecipients,
                                        message,
                                        ttlSeconds,
                                        &task->m_Status),
                    catchall,
                    rlError("Error configuring task"));

            rverify(rlGetTaskManager()->AddParallelTask(task),
                    catchall,
                    rlError("Error queuing task"));

            success = true;
        }
        rcatchall
        {
            if(task)
            {
                rlGetTaskManager()->DestroyTask(task);
            }
        }
    }

    return success;
}

bool
rlScPresence::GetMessages(const int localGamerIndex,
                            const unsigned numMessages,
                            rlScPresenceMessageIterator* iter,
                            netStatus* status)
{
    bool success = false;

    rlDebug2("Retrieving messages for local gamer: %d...",
            localGamerIndex);

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        rlScGetMessagesTask* task = NULL;

        rtry
        {
            rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                    catchall,
                    rlError("Invalid gamer index: %d", localGamerIndex));

            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error creating task"));

            rverify(rlTaskBase::Configure(task, localGamerIndex, numMessages, iter, status),
                    catchall,
                    rlError("Error configuring task"));

            rverify(rlGetTaskManager()->AppendSerialTask((size_t)&s_PlayerPres[localGamerIndex], task),
                    catchall,
                    rlError("Error queuing task"));

            success = true;
        }
        rcatchall
        {
            if(task)
            {
                rlGetTaskManager()->DestroyTask(task);
            }
        }
    }

    return success;
}

bool
rlScPresence::Query(const int localGamerIndex,
                    const char* queryName,
                    const char* paramNameValueCsv,
                    const int offset,
                    const int count,
                    char* recordsBuf,
                    const unsigned sizeofRecordsBuf,
                    char** records,
                    unsigned* numRecordsRetrieved,
                    unsigned* numRecords,
                    netStatus* status)
{
    bool success = false;

    rlDebug2("Executing query \"%s\" with \"%s\"...",
            queryName, paramNameValueCsv);

    if(rlVerifyf(s_RlScPresenceInitialized, "Not initialized"))
    {
        rlScQueryPresenceTask* task = NULL;

        rtry
        {
            rverify(offset >= 0,
                    catchall,
                    rlError("Invalid offset: %d", offset));

            rverify(0 == count || (recordsBuf && sizeofRecordsBuf && records),
                    catchall,
                    rlError("Insufficient buffers"));

            if(count > RLSC_PRESENCE_QUERY_MAX_RESULTS)
            {
                rlWarning("%d records requested which exceeds maximum of %d records",
                            count, RLSC_PRESENCE_QUERY_MAX_RESULTS);
            }

            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error creating task"));

            rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        queryName,
                                        paramNameValueCsv,
                                        offset,
                                        count <= RLSC_PRESENCE_QUERY_MAX_RESULTS ? count : RLSC_PRESENCE_QUERY_MAX_RESULTS,
                                        recordsBuf,
                                        sizeofRecordsBuf,
                                        records,
                                        numRecordsRetrieved,
                                        numRecords,
                                        status),
                    catchall,
                    rlError("Error configuring task"));

            rverify(rlGetTaskManager()->AddParallelTask(task),
                    catchall,
                    rlError("Error queuing task"));

            success = true;
        }
        rcatchall
        {
            if(task)
            {
                rlGetTaskManager()->DestroyTask(task);
            }
        }
    }

    return success;
}

//Dummy value to pass to Query().
static unsigned s_NumRecordsRetrieved;

bool
rlScPresence::QueryCount(const int localGamerIndex,
                        const char* queryName,
                        const char* paramNameValueCsv,
                        unsigned* count,
                        netStatus* status)
{
    rlDebug2("Querying count of \"%s\" with \"%s\"...",
            queryName, paramNameValueCsv);

    return Query(localGamerIndex,
                queryName,
                paramNameValueCsv,
                0,
                0,
                NULL,
                0,
                NULL,
                &s_NumRecordsRetrieved,
                count,
                status);
}

void
rlScPresence::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//private

static bool CommitSubscriptions(const int localGamerIndex,
                                rlScSubscribeUnsubscribeTask::Op op);

bool
rlScPresence::CommitState(const int localGamerIndex)
{
    bool success = false;

    rlDebug2("Committing state for gamer: %d...", localGamerIndex);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        rlScPlayerPresence* playerPres = &s_PlayerPres[localGamerIndex];

        if(playerPres->NeedToSignOnline())
        {
            rlDebug("Gamer: %d signing in to presence - committing all attributes/subscriptions...",
                    localGamerIndex);

            //Make sure all attributes/subscriptions go up when we first sign online.
            playerPres->AttrSetAllDirty(true);
            playerPres->SubSetAllDirty(true);
        }

        if(playerPres->AttrAreAnyDirty())
        {
            CommitAttributes(localGamerIndex);
        }

        if(playerPres->SubAreAnyDirty())
        {
            CommitSubscriptions(localGamerIndex, rlScSubscribeUnsubscribeTask::OP_SUBSCRIBE);
            CommitSubscriptions(localGamerIndex, rlScSubscribeUnsubscribeTask::OP_UNSUBSCRIBE);
        }

        playerPres->m_StopWatch.Reset();

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool
rlScPresence::CommitAttributes(const int localGamerIndex)
{
    bool success = false;

    rlScCommitPresenceTask* task = NULL;

    rlDebug2("Committing attributes for gamer: %d...", localGamerIndex);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        rlScPlayerPresence* playerPres = &s_PlayerPres[localGamerIndex];

        if(!playerPres->m_CommitAttrsStatus.Pending())
        {
            rlScCommitPresenceTask::CommitType commitType;
            if(playerPres->NeedToSignOnline() || playerPres->NeedsToReplaceAttrs())
            {
                //When first signing online, replace all presence attributes.
                //This covers the case when we reboot and sign in again before
                //the presence system has a chance to log us out.
				//Note that NeedToSignOnline() can return false before we have
				//replaced attributes (e.g. if no attrs were set before signing
				//online), which caused stale attributes to be left intact.
				//Added a separate check for NeedsToReplaceAttrs() to fix this.
                commitType = rlScCommitPresenceTask::COMMIT_REPLACE;
            }
            else
            {
                commitType = rlScCommitPresenceTask::COMMIT_MODIFY;
            }

            //Only commit dirty attributes.
            rlScPresenceAttribute dirtyAttrs[RLSC_PRESENCE_MAX_ATTRIBUTES];
            unsigned numDirty = 0;

            for(int i = 0; i < (int)playerPres->m_NumAttributes; ++i)
            {
                if(!playerPres->AttrIsDirty(i))
                {
                    continue;
                }

                dirtyAttrs[numDirty++] = playerPres->m_Attributes[i];
            }

            rlAssertf(numDirty > 0,
                        "No dirty attributes - why are we trying to commit them?");

            rverify(rlGetTaskManager()->CreateTask(&task),
                    catchall,
                    rlError("Error creating task"));

            rverify(rlTaskBase::Configure(task,
                                        localGamerIndex,
                                        dirtyAttrs,
                                        numDirty,
                                        commitType,
                                        &playerPres->m_CommitAttrsStatus),
                    catchall,
                    rlError("Error configuring task"));

            rverify(rlGetTaskManager()->AppendSerialTask((size_t)&s_PlayerPres[localGamerIndex], task),
                    catchall,
                    rlError("Error queuing task"));

            if(rlScCommitPresenceTask::COMMIT_REPLACE == commitType)
            {
                playerPres->m_LastRefreshAttrsPosixTime = rlGetPosixTime() | 0x01;
				playerPres->m_HasReplacedAttrs = true;
            }

            for(int i = 0; i < playerPres->m_NumAttributes; ++i)
            {
                //Keep track of the attributes that have been committed.
                playerPres->m_CommittedAttributes[i] = playerPres->m_Attributes[i];
            }

            playerPres->m_NumCommittedAttributes = playerPres->m_NumAttributes;

            success = true;
        }
        else
        {
            rlDebug2("Commit operation already pending");
        }
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

static
bool CommitSubscriptions(const int localGamerIndex,
                          rlScSubscribeUnsubscribeTask::Op op)
{
    bool success = false;

    rlScSubscribeUnsubscribeTask* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                catchall,
                rlError("Invalid gamer index: %d", localGamerIndex));

        rlScPlayerPresence* playerPres = &s_PlayerPres[localGamerIndex];

        if((rlScSubscribeUnsubscribeTask::OP_SUBSCRIBE == op
                && !playerPres->m_CommitSubscribeStatus.Pending())
            || (rlScSubscribeUnsubscribeTask::OP_UNSUBSCRIBE == op
                && !playerPres->m_CommitUnsubscribeStatus.Pending()))
        {
            //Only commit dirty subscriptions.
            const char* dirtySubs[RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS];
            char dirtySubsBuf[RLSC_PRESENCE_MAX_SUBSCRIBED_CHANNELS][RLSC_PRESENCE_CHANNEL_NAME_MAX_SIZE];
            int numDirty = 0;
            for(int i = 0; i < playerPres->m_NumSubscriptions; ++i)
            {
                if(!playerPres->SubIsDirty(i))
                {
                    continue;
                }

                if(rlScSubscribeUnsubscribeTask::OP_SUBSCRIBE == op
                    && !playerPres->SubIsSubscribed(i))
                {
                    //We're currently committing "subscribes" but this channel
                    //isn't subscribed.
                    continue;
                }

                if(rlScSubscribeUnsubscribeTask::OP_UNSUBSCRIBE == op
                    && playerPres->SubIsSubscribed(i))
                {
                    //We're currently committing "unsubscribes" but this channel
                    //is subscribed.
                    continue;
                }

                safecpy(dirtySubsBuf[numDirty], playerPres->m_Subscriptions[i].m_Channel);
                dirtySubs[numDirty] = dirtySubsBuf[numDirty];
                ++numDirty;
            }

            if(numDirty > 0)
            {
                rlDebug2("Committing %s for gamer: %d...",
                    (rlScSubscribeUnsubscribeTask::OP_SUBSCRIBE == op) ? "subscriptions" : "unsubscriptions",
                    localGamerIndex);

                rverify(rlGetTaskManager()->CreateTask(&task),
                        catchall,
                        rlError("Error creating task"));

                //Use a different status object depending on if we're
                //committing subscribes or unsubscribes
                netStatus* status = rlScSubscribeUnsubscribeTask::OP_SUBSCRIBE == op
                                    ? &playerPres->m_CommitSubscribeStatus
                                    : &playerPres->m_CommitUnsubscribeStatus;

                rverify(rlTaskBase::Configure(task,
                                            localGamerIndex,
                                            op,
                                            dirtySubs,
                                            numDirty,
                                            status),
                        catchall,
                        rlError("Error configuring task"));

                rverify(rlGetTaskManager()->AppendSerialTask((size_t)&s_PlayerPres[localGamerIndex], task),
                        catchall,
                        rlError("Error queuing task"));
            }

            success = true;
        }
        else
        {
            rlDebug2("Commit operation already pending");
        }
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}
#endif

#if RLSC_PRESENCE_DUPLICATE_SIGNIN_CHECKS

void
rlScPresence::DuplicateSigninInit()
{
	if (!GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return;
	}

	// Iterate through RL_MAX_LOCAL_GAMERS. 
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		// set the attribute back to zero, reset the status flag and the duplicate recheck flag
		sm_SigninTimeAttrs[i].Reset(rlScAttributeId::SignInTime.Name, (s64)0);
		sm_DuplicateSigninCheckStatus[i].Reset();
		sm_SecondDuplicateCheck[i] = false;
	}
}

void
rlScPresence::DuplicateSigninCheck(const int localGamerIndex)
{
	if (!GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return;
	}

	static const int DUPLICATE_CHECK_SEC = (15);

	// grab the current posix time
	u64 curTime = rlGetPosixTime();
	u64 sysTime = sysTimer::GetSystemMsTime();

	// add extra precision of the system MS time. Since the posix time is in seconds, 
	// two users can sign in at the same second. Its extremely unlikely they logged in
	// the same second with the same system MS time. We don't want false positives.
	u64 extraPrecisionTime = (curTime * 1000) + (sysTime % 1000);

	// Set the signin time as this extra precision time
	s_PlayerPres[localGamerIndex].SetSignInTime(extraPrecisionTime);
	rlDebug("DuplicateSigninCheck: posix time: %"I64FMT"u, system time: %"I64FMT"u, precision time: %"I64FMT"u", curTime, sysTime, extraPrecisionTime);

	// Set the duplicate signin check time as the current time + the check interval (15 seconds)
	s_PlayerPres[localGamerIndex].SetDuplicateSigninCheckTime(curTime + DUPLICATE_CHECK_SEC);

	// actually set the int attribute
	rlPresence::SetIntAttribute(localGamerIndex, rlScAttributeId::SignInTime.Name, extraPrecisionTime, true);
}

void
rlScPresence::DuplicateSigninRecheck(const int localGamerIndex)
{
	if (!GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return;
	}

	// change the recheck to 5 minutes
	static const int DUPLICATE_RECHECK_SEC = (300);

	// We only trigger a secondary recheck once per signin.
	if (sm_SecondDuplicateCheck[localGamerIndex] == false)
	{
		// Set the next check time
		u64 nextCheckTime = rlGetPosixTime() + DUPLICATE_RECHECK_SEC;
		rlDebug("DuplicateSigninRecheck - Queuing secondary check in %d seconds at %"I64FMT"u", DUPLICATE_RECHECK_SEC, nextCheckTime);
		s_PlayerPres[localGamerIndex].SetDuplicateSigninCheckTime(nextCheckTime);

		// set the second duplicate check flag so we don't try again
		sm_SecondDuplicateCheck[localGamerIndex] = true;
	}		
	else
	{
		// If the duplicate recheck flag is true, simply set the recheck time to 0
		s_PlayerPres[localGamerIndex].SetDuplicateSigninCheckTime(0);
	}
}

void 
rlScPresence::DuplicateSigninReset(const int localGamerIndex)
{
	if (!GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return;
	}

	// Reset the signin time and the duplicate check time to zero
	s_PlayerPres[localGamerIndex].SetSignInTime(0);
	s_PlayerPres[localGamerIndex].SetDuplicateSigninCheckTime(0);

	// If a task is in progress, cancel the task
	if (sm_DuplicateSigninCheckStatus[localGamerIndex].Pending())
	{
		rlGetTaskManager()->CancelTask(&(sm_DuplicateSigninCheckStatus[localGamerIndex]));
	}

	// reset the attribute back to 0
	sm_SigninTimeAttrs[localGamerIndex].Reset(rlScAttributeId::SignInTime.Name, (s64)0);

	// reset the secondary duplicate check flag so that the next signin will trigger a secondary recheck
	sm_SecondDuplicateCheck[localGamerIndex] = false;
}

void
rlScPresence::DuplicateSigninUpdate(const int localGamerIndex)
{
	if (!GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return;
	}

	// If the presence attributes are still dirty, extend the check by 15 seconds
	static const int DUPLICATE_EXTEND_TIME_SEC = (15);

	// Get the current posix time
	u64 posixTime = rlGetPosixTime();

	// Is the posix time more recent than the duplicate signin time? (requires duplicate signin time to be > 0)
	if (posixTime > s_PlayerPres[localGamerIndex].m_DuplicateSigninCheckTime && 
		s_PlayerPres[localGamerIndex].m_DuplicateSigninCheckTime != 0)
	{
		// The current player index has dirty attrs, dont recheck yet
		if (s_PlayerPres[localGamerIndex].IsDirty())
		{
			rlDebug("DuplicateSigninUpdate - extending recheck time by %d seconds", DUPLICATE_EXTEND_TIME_SEC);
			s_PlayerPres[localGamerIndex].SetDuplicateSigninCheckTime(posixTime + DUPLICATE_EXTEND_TIME_SEC);
			return;
		}

		// If we have a gamer info at this index ( should be true if the previous checks are true ), get their attributes
		rlGamerInfo gi;
		if (rlPresence::GetGamerInfo(localGamerIndex, &gi))
		{
			rlPresence::GetAttributesForGamer(0, gi.GetGamerHandle(), &sm_SigninTimeAttrs[localGamerIndex], 1, &sm_DuplicateSigninCheckStatus[localGamerIndex]);

			// Set the duplicate signin check time to 0 so we don't try to check again
			s_PlayerPres[localGamerIndex].m_DuplicateSigninCheckTime = 0;
		}
	}

	// If the duplicate signin check is finished
	if (!sm_DuplicateSigninCheckStatus[localGamerIndex].Pending())
	{
		// Success processing
		if (sm_DuplicateSigninCheckStatus[localGamerIndex].Succeeded())
		{
			// If the attrs are not valid (I.e. Zero), it means the server didn't get our attr. 
			if (!sm_SigninTimeAttrs[localGamerIndex].IsValid())
			{
				rlDebug("DuplicateSigninUpdate - presence attr not read - potentially cleared by duplicate signin");
			}
			else
			{
				// Read the value (defaulted as a s64, must be casted to u64 to match the posix time above)
				s64 val;
				sm_SigninTimeAttrs[localGamerIndex].GetValue(&val);

				if ((u64)val == s_PlayerPres[localGamerIndex].m_SigninTime)
				{
					// Check if the value matches what we've cached as the signin time
					rlDebug("DuplicateSigninUpdate - presence attr %"I64FMT"u matched %"I64FMT"u", (u64)val, s_PlayerPres[localGamerIndex].m_SigninTime);

					// Check for the secondary duplicate signin recheck (if needed)
					DuplicateSigninRecheck(localGamerIndex);
				}
				else if ((u64)val > s_PlayerPres[localGamerIndex].m_SigninTime)
				{
					// the attr is newer than the one we wrote. This means someone else has logged into our account, we missed the 
					// duplicate signout message, and now two of us are logged in. Kick us for duplicate logins
					rlDebug("DuplicateSigninUpdate - presence attr %"I64FMT"u newer than %"I64FMT"u", (u64)val, s_PlayerPres[localGamerIndex].m_SigninTime);

					// Trigger the duplicate 
					GetRgscConcreteInstance()->_GetProfileManager()->DuplicateSignInKick(localGamerIndex);
				}
				else
				{
					// the attr is older (or zero). This would indicate some sort of write failure, not worth kicking for but could be better. Do a recheck.
					rlDebug("DuplicateSigninUpdate - presence attr %"I64FMT"u older than %"I64FMT"u", (u64)val, s_PlayerPres[localGamerIndex].m_SigninTime);
					DuplicateSigninRecheck(localGamerIndex);
				}
			}
		}

		// Clear the signintime attr and reset the net status
		sm_SigninTimeAttrs[localGamerIndex].Reset(rlScAttributeId::SignInTime.Name, (s64)0);
		sm_DuplicateSigninCheckStatus[localGamerIndex].Reset();
	}
}

#endif

}
