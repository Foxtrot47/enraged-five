// 
// rline/rlgamerhandle.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLGAMERHANDLE
#define RLINE_RLGAMERHANDLE

#include "net\net.h"

namespace rage
{

//PURPOSE
//  Defines for which platforms can read/write handles for a particular service.
//  This gives us the option of supporting multiple services on a platform. 
//  If a platform can't, any import of the service's handle will fail.
#define RLGAMERHANDLE_XBL       RSG_XBL || RSG_DURANGO
#define RLGAMERHANDLE_NP        RSG_NP
#define RLGAMERHANDLE_SC		RSG_PC

//PURPOSE
//  rlGamerHandle provides a container for platform-specific gamer
//  identification data.  This is the data that is typically required
//  by platform-specific APIs to identify a gamer to "backend" services.
//
//  rlGamerHandle should *not* be used to uniquely identify a gamer.  Use
//  rlGamerId for this.  The reason is that on Xbox the data in rlGamerHandle
//  can be different for the same gamer depending on whether he is online or
//  offline.
class rlGamerHandle
{
    friend class rlPresence;

public:

    rlGamerHandle();

#if RLGAMERHANDLE_SC
	static const unsigned MAX_EXPORTED_SIZE_IN_BITS_SC =	(sizeof(u8) << 3) +									// m_Service
															(sizeof(RockstarId) << 3) +							// m_RockstarId
															(sizeof(u8) << 3);									// m_BotIndex

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES_SC = ((MAX_EXPORTED_SIZE_IN_BITS_SC + 7) & ~7) >> 3;		// round up to nearest multiple of 8 then divide by 8
#else
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES_SC = 0;
#endif

#if RLGAMERHANDLE_NP
	static const unsigned MAX_EXPORTED_SIZE_IN_BITS_NP =	(sizeof(u8) << 3) +									// m_Service
															8 /*RLNP_ENV_NUM_BITS*/ +							// m_NpEnv
															(rlSceNpOnlineId::MAX_DATA_BUF_SIZE << 3) +			// m_OnlineIdData
															(sizeof(rlSceNpAccountId) << 3) +					// m_AccountId (u64)
															(sizeof(u8) << 3);									// m_BotIndex

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES_NP = ((MAX_EXPORTED_SIZE_IN_BITS_NP + 7) & ~7) >> 3;		// round up to nearest multiple of 8 then divide by 8
#else
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES_NP = 0;
#endif

#if	RLGAMERHANDLE_XBL
	static const unsigned MAX_EXPORTED_SIZE_IN_BITS_XBL =	(sizeof(u8) << 3) +									// m_Service
															(sizeof(u64) << 3) +								// m_Xuid
															(sizeof(u8) << 3);									// m_BotIndex

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES_XBL = ((MAX_EXPORTED_SIZE_IN_BITS_XBL + 7) & ~7) >> 3;	// round up to nearest multiple of 8 then divide by 8
#else
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES_XBL = 0;
#endif

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = MAX(MAX_EXPORTED_SIZE_IN_BYTES_XBL, 
													   MAX(MAX_EXPORTED_SIZE_IN_BYTES_SC, 
														   MAX_EXPORTED_SIZE_IN_BYTES_NP));

    //PURPOSE
    //  Sets / gets whether we're using the account Id as a key or not
    //  PSN is transitioning from Online ID (string: 16 characters) to AccoundID (u64).
    //	On V, we're on an older SDK so can't dispense with online Id fully because it's required
    //  for some platform API functions. Once SCS enable account Id as the primary key in the backend
    //  this should be enabled on the client
    static bool IsUsingAccountIdAsKey();
    static void SetUsingAccountIdAsKey(const bool useAccountIdAsKey);

    //PURPOSE
    //  Invalidates the gamer handle.
    void Clear();

    //PURPOSE
    //  Returns true if the gamer handle is valid.
    bool IsValid() const;
    bool IsValidForRos() const;
	OUTPUT_ONLY( void OutputValidity() const; )

    //PURPOSE
    //  Returns true if the gamer handle identifies a local gamer.
    bool IsLocal() const;

    //PURPOSE
    //  Returns the local index if the gamer handle identifies a local gamer.
    //  Otherwise returns -1.
    int GetLocalIndex() const;

#if HACK_GTA4
    //PURPOSE
    // Sets the bot index, required for network bots (players will be always have this set to 0, bots non-zero)
    //PARAMS
    // botIndex - The bot index of the gamer represented by this handle 
    void SetBotIndex(u8 botIndex);

	//PURPOSE
	// Network Bots will always have their bot index set to non-zero.
	bool IsBot() const { return (m_BotIndex > 0); }
#endif // #HACK_GTA4

    //PURPOSE
    //  Returns the handle encoded in string form.
	const char* ToString() const;
    const char* ToString(char* buf, const int bufSize) const;
    template<int SIZE>
    const char* ToString(char (&buf)[SIZE]) const
    {
        CompileTimeAssert(SIZE >= RL_MAX_GAMER_HANDLE_CHARS);
        return ToString(buf, SIZE);
    }

    //PURPOSE
    //  Initializes the handle from a string.
    bool FromString(const char* str);

	//PURPOSE
	//  Returns the user ID encoded in string form.
	//  On XBL the user ID is the string representation of a u64 *in DECIMAL format*.
	//  On NP the user ID is the sceNpOnlineId
	//  On SC the user ID is the string representation of the Rockstar ID.
	const char* ToUserId(char* buf, const int bufSize) const;
    template<int SIZE>
	const char* ToUserId(char (&buf)[SIZE]) const
    {
        return ToUserId(buf, SIZE);
    }

    //PURPOSE
    //  Initializes the handle from a user ID.
    //  On XBL the user ID is the string representation of a u64 *in DECIMAL format*.
    //  On NP the user ID is the sceNpOnlineId
    //  On SC the user ID is the string representation of the Rockstar ID.
    bool FromUserId(const char* str);

	//PURPOSE
	//  Exports data in a platform/endian independent format.
	//PARAMS
	//  buf         - Destination buffer.
	//  sizeofBuf   - Size of destination buffer.
	//  size        - Set to number of bytes exported.
	//RETURNS
	//  True on success.
	bool Export(void* buf,
		const unsigned sizeofBuf,
		unsigned* size = nullptr) const;

	//PURPOSE
	//  Imports data exported with Export().
	//  buf         - Source buffer.
	//  sizeofBuf   - Size of source buffer.
	//  size        - Set to number of bytes imported.
	//RETURNS
	//  True on success.
	bool Import(const void* buf,
		const unsigned sizeofBuf,
		unsigned* size = nullptr);

	//PURPOSE
	//  Checks whether this the same as that
	//  allowInvalid - Whether we return true when both handles are invalid
	bool Equals(const rlGamerHandle& that, const bool allowInvalid) const;

    bool operator==(const rlGamerHandle& that) const;
    bool operator!=(const rlGamerHandle& that) const;

    //Service within which this handle is valid
    enum eService
    {
        SERVICE_INVALID = 0,
        SERVICE_FIRST,        
        SERVICE_XBL = SERVICE_FIRST,
        SERVICE_NP,
		SERVICE_SC,
		SERVICE_LAST    = SERVICE_SC,
        NUM_SERVICES
    };

#if RLGAMERHANDLE_XBL
    static const eService NATIVE_SERVICE = SERVICE_XBL;
#elif RLGAMERHANDLE_NP
    static const eService NATIVE_SERVICE = SERVICE_NP;
#else
    static const eService NATIVE_SERVICE = SERVICE_SC;
#endif

    int GetService() const { return m_Service; }
	   
	bool IsNativeGamerHandle() const { return m_Service == NATIVE_SERVICE; }
	bool IsSupportedGamerHandle() const { return (RLGAMERHANDLE_XBL && (m_Service == SERVICE_XBL)) ||
												 (RLGAMERHANDLE_NP && (m_Service == SERVICE_NP)) ||
												 (RLGAMERHANDLE_SC && (m_Service == SERVICE_SC)); }

	bool IsXbl() const { return m_Service == SERVICE_XBL; }
	bool IsNp() const { return m_Service == SERVICE_NP; }
	bool IsSc() const { return m_Service == SERVICE_SC; }

    static int GetServiceFromString(const char* ghstr);

    static bool IsNativeGamerHandle(const char* ghstr)
    {
        return GetServiceFromString(ghstr) == NATIVE_SERVICE;
    }

    static bool IsSc(const char* ghstr)
    {
        return GetServiceFromString(ghstr) == SERVICE_SC;
    }

    static RockstarId GetRockstarId(const char* ghstr);

    //Reset the handle for different services.
#if RLGAMERHANDLE_XBL    
    void ResetXbl(const u64 xuid);
    u64 GetXuid() const;
#endif

#if RLGAMERHANDLE_NP
	void ResetNp(const int npEnv, const rlSceNpAccountId accountId, const rlSceNpOnlineId* onlineId);
	const rlSceNpAccountId GetNpAccountId() const;
	rlSceNpOnlineId GetNpOnlineId() const;
#endif // RLGAMERHANDLE_NP

#if RLGAMERHANDLE_SC
	void ResetSc(const RockstarId profileId);
	RockstarId GetRockstarId() const;
#endif

	u64 ToUniqueId() const;
	void FromUniqueId(const int service, const u64 uniqueId);

	static const char* ToString(const rlGamerHandle& hGamer);
	static void SetAllowExtraValidationOnImport(const bool allowExtraValidationOnImport); 

private:

    static bool s_IsUsingAccountIdAsKey;
	static bool sm_AllowExtraValidationOnImport;

    //Handle data for each service this platform can read/write.  Note that RSG_PC 
    //needs to be the most inclusive because our servers need to work with handles 
    //given to them by consoles.
    union Data
    {
#if RLGAMERHANDLE_XBL
        struct XblData
        {
            u64 m_Xuid;
        } m_XblData;
#endif

#if RLGAMERHANDLE_NP
		struct NpData
		{
			int m_NpEnv;
			rlSceNpAccountId m_AccountId;
			char m_OnlineIdData[rlSceNpOnlineId::MAX_DATA_BUF_SIZE+1]; // null term
		} m_NpData;
#endif

#if RLGAMERHANDLE_SC
		struct ScData
		{
			RockstarId m_RockstarId;
		} m_ScData;
#endif

	} m_Data;

    u8 m_Service;

#if HACK_GTA4
#define INVALID_BOT_INDEX (0)
#define MAX_BOT_INDEX (31)
    u8 m_BotIndex;
#endif // HACK_GTA4
};

//Represents an invalid gamer handle
extern const rlGamerHandle RL_INVALID_GAMER_HANDLE;

}   //namespace rage

#endif  //RLINE_RLGAMERHANDLE
