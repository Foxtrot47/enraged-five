// 
// rline/rlyoutubetasks.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// Youtube Includes
#include "rlyoutubetasks.h"

// Rage Includes
#include "data/rson.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "rline/rldiag.h"
#include "rline/rlsystemui.h"
#include "rline/ros/rlros.h"
#include "rline/scauth/rlscauth.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/rltitleid.h"
#include "system/timer.h"

#if RSG_BANK
#include "bank/bkmgr.h"
#endif

#if RSG_PC
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(error: 4668)
#endif

#if !__FINAL
#include "math/random.h"
#endif

namespace rage
{

#if !__FINAL
	mthRandom g_debugRand;
#endif

RAGE_DEFINE_SUBCHANNEL(rline, youtubetasks);
#undef __rage_channel
#define __rage_channel rline_youtubetasks

#define YOUTUBE_HTTP_TIMEOUT_SECS 30

extern sysMemAllocator* g_rlAllocator;

#define ERROR_CASE(code, codeEx, resCode) \
	if(result.IsError(code, codeEx)) \
{ \
	resultCode = resCode; \
	return; \
}

#define WARNING_CASE(code, codeEx) \
	if(result.IsError(code, codeEx)) \
{ \
	resultCode = 0; \
	return; \
}

#if !__FINAL
PARAM(ytNoQuotaResume, "Forces a quota error for Video Resumable");
PARAM(ytNoQuotaUpload, "Forces a quota error for Video Uploads");
PARAM(ytNoQuotaVideo, "Forces a quota error for requesting Video Info");
PARAM(ytNoQuotaChannel, "Forces a quota error for requesting Channel Info");


PARAM(ytNoQuotaDailyResume, "Forces a daily limit error for Video Resumable");
PARAM(ytNoQuotaDailyUpload, "Forces a daily limit error for Video Uploads");
PARAM(ytNoQuotaDailyVideo, "Forces a daily limit error for Video Info");
PARAM(ytNoQuotaDailyChannel, "Forces a daily limit error for Channel Info");

static const char * quotaExceededMsg = "{															\
										\"error\": {												\
												\"errors\": [										\
												{													\
													\"domain\": \"youtube.quota\",					\
														\"reason\": \"quotaExceeded\",				\
														\"message\": \"Quota Exceeded\",			\
														\"locationType\": \"\",						\
														\"location\": \"\"							\
												}													\
												],													\
													\"code\": 403,									\
													\"message\": \"Quota Exceeded\"					\
													\"extendedHelp\": \"\"							\
											}														\
										}";

static const char * quotaDailyMsg = "{																\
										\"error\": {												\
												\"errors\": [										\
												{													\
													\"domain\": \"youtube.quota\",					\
														\"reason\": \"dailyLimitExceeded\",			\
														\"message\": \"Daily Limit Exceeded\",		\
														\"locationType\": \"\",						\
														\"location\": \"\"							\
												}													\
												],													\
													\"code\": 403,									\
													\"message\": \"Daily Limit Exceeded\"			\
													\"extendedHelp\": \"\"							\
											}														\
									   }";

#endif

/////////////////////////////////////////////////////
// rlYoutubeDataApiTask
/////////////////////////////////////////////////////
rlYoutubeDataApiTask::rlYoutubeDataApiTask()
	: m_UploadPtr(NULL)
{

}
bool rlYoutubeDataApiTask::Configure()
{
	rtry
	{
		// We control our own content type
		m_Config.m_AddDefaultContentTypeHeader = false;
		m_Config.m_HttpVerb = GetVerb();

		rverify(rlHttpTask::Configure(&m_Config), catchall, );	

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlYoutubeDataApiTask::Finish(const FinishType finishType, const int resultCode)
{
	int newResultCode = resultCode;

	// YouTube sends us failures as part of their response. We need to extract the 4XX errors and handle them.
	if (finishType == FINISH_FAILED)
	{
		const char* response = (const char*)m_GrowBuffer.GetBuffer();
		if (response)
		{
			ProcessResponse(response, newResultCode);
		}
	}


	rlHttpTask::Finish(finishType, newResultCode);
}

bool rlYoutubeDataApiTask::ProcessYoutubeErrors(RsonReader& rr)
{
	rtry
	{
		rcheck(m_UploadPtr != NULL, catchall, );
		if (rr.HasMember("error"))
		{
			RsonReader rrError;
			rverify(rr.GetMember("error", &rrError), catchall, );

			if (rrError.HasMember("code"))
			{
				RsonReader rrCode;
				rverify(rrError.GetMember("code", &rrCode), catchall, );
				rverify(rrCode.AsInt(m_UploadPtr->m_ErrCode), catchall, );
			}

			if (rrError.HasMember("message"))
			{
				RsonReader rrMessage;
				rverify(rrError.GetMember("message", &rrMessage), catchall, );
				rverify(rrMessage.AsAtString(m_UploadPtr->m_ErrResponse), catchall, );
			}

			if (rrError.HasMember("errors"))
			{
				RsonReader rrErrors;
				rverify(rrError.GetMember("errors", &rrErrors), catchall, );

				RsonReader rrErrorMsg;
				bool moreErrors = rrErrors.GetFirstMember(&rrErrorMsg);

				while(moreErrors)
				{
					if (rrErrorMsg.HasMember("reason"))
					{
						RsonReader rrReason;
						rverify(rrErrorMsg.GetMember("reason", &rrReason), catchall, );
						rverify(rrReason.AsAtString(m_UploadPtr->m_ErrReason), catchall, );
					}

					moreErrors = rrErrorMsg.GetNextSibling(&rrErrorMsg);
				}
			}

			return false;
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

/////////////////////////////////////////////////////
// rlRosGetYoutubeAccessTokenTask
/////////////////////////////////////////////////////
rlRosGetYoutubeAccessTokenTask::rlRosGetYoutubeAccessTokenTask()
	: m_UploadPtr(NULL)
{

}

rlRosGetYoutubeAccessTokenTask::~rlRosGetYoutubeAccessTokenTask()
{

}

bool rlRosGetYoutubeAccessTokenTask::Configure(const int localGamerIndex, rlYoutubeUpload* upload)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(upload, catchall, );
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("ROS credentials are invalid"));

		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

		m_UploadPtr = upload;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlRosGetYoutubeAccessTokenTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rlAssert(node);

	rtry
	{
		const parTreeNode* result = node->FindChildWithName("Result");
		rverify(result, catchall, );

		const char * tokenPtr = rlHttpTaskHelper::ReadString(result, "Token", NULL);
		rverify(tokenPtr, catchall, );

		u64 expirationTime = 0;
		rverify(rlHttpTaskHelper::ReadUInt64(expirationTime, result, "ExpirationPosixTime", NULL), catchall, );

		const char * linkUrl = rlHttpTaskHelper::ReadString(result, "LinkUrlBase", NULL);
		if (linkUrl != NULL)
		{
			rlDebug3("Received updated link URL: %s", linkUrl);
			rlYoutube::SetLinkUrl(linkUrl);
		}

		m_UploadPtr->TokenType = "Bearer";
		m_UploadPtr->AccessToken = tokenPtr;
		m_UploadPtr->ExpirationTime = expirationTime;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlRosGetYoutubeAccessTokenTask::ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
	rtry
	{
		const parTreeNode* resultNode = node->FindChildWithName("Result");
		rverify(resultNode, catchall, );

		const char * linkUrl = rlHttpTaskHelper::ReadString(resultNode, "LinkUrlBase", NULL);
		if (linkUrl != NULL)
		{
			rlDebug3("Received updated link URL: %s", linkUrl);
			rlYoutube::SetLinkUrl(linkUrl);
		}

		ERROR_CASE("AuthenticationFailed", "Ticket", RL_YT_ERROR_AUTHENTICATIONFAILED_TICKET);
		ERROR_CASE("DoesNotExist", "PlayerAccount", RL_YT_ERROR_NOT_LINKED);
		ERROR_CASE("NotAllowed", "GoogleAPI", RL_YT_ERROR_NOT_LINKED);
	}
	rcatchall
	{

	}

	rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	resultCode = RL_YT_ERROR_UNEXPECTED_RESULT;
}

const char* rlRosGetYoutubeAccessTokenTask::GetServiceMethod() const
{
	return "socialclub.asmx/GetYoutubeAccessToken";
}

//////////////////////////////////////////////////////
// rlYoutubeGetResumableUploadUrlTask
//////////////////////////////////////////////////////
rlYoutubeGetResumableUploadUrlTask::rlYoutubeGetResumableUploadUrlTask()
{

}

bool rlYoutubeGetResumableUploadUrlTask::Configure(rlYoutubeUpload* upload)
{
	rtry
	{
		rverify(upload, catchall, );
		m_UploadPtr = upload;

		// Assert if the channel should not be uploading videos.
		rlTaskAssertf(upload->ChannelInfo.ChannelId.length() > 0, "Channel ID was not found in channel info.");
		rlTaskAssertf(upload->ChannelInfo.IsLinked, "Channel has not been linked to Youtube or Google+ - this request will likely fail.");

		atString authBuf = m_UploadPtr->TokenType;
		authBuf += " ";
		authBuf += m_UploadPtr->AccessToken;

		char fileLenBuf[256];
		formatf(fileLenBuf, "%" I64FMT "u", m_UploadPtr->UploadSize);

		rverify(rlYoutubeDataApiTask::Configure(), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("authorization", authBuf.c_str()), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("Content-Type", "application/json; charset=utf-8"),catchall,);
		rverify(m_HttpRequest.AddRequestHeaderValue("X-Upload-Content-Length", fileLenBuf), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("X-Upload-Content-Type", "video/*"), catchall, );

		rverify(m_HttpRequest.AddQueryParam("uploadType", "resumable", istrlen("resumable")), catchall, );
		rverify(m_HttpRequest.AddQueryParam("part", "snippet,status,contentDetails", istrlen("snippet,status,contentDetails")), catchall, );

		RsonWriter rw;
		rverify(m_UploadPtr->VideoInfo.GenerateJSON(&rw), catchall, );
		rverify(AppendContent(rw.ToString(), rw.Length()), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// Receives the HTTP response
bool rlYoutubeGetResumableUploadUrlTask::ProcessResponse(const char* response, int& /*resultCode*/)
{
	rtry
	{
#if !__FINAL
		if (PARAM_ytNoQuotaDailyResume.Get())
		{
			response = quotaDailyMsg;
		}
		else
		{
			int odds = -1;
			if (PARAM_ytNoQuotaResume.Get(odds))
			{
				if (odds >= 100 || g_debugRand.GetRanged(0,100) < odds)
					response = quotaExceededMsg;
			}
		}


#endif

		// No response is expected
		if (response != NULL)
		{
			RsonReader rr(response, istrlen(response));
			rcheck(ProcessYoutubeErrors(rr), catchall, );
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlYoutubeGetResumableUploadUrlTask::Finish(const FinishType finishType, const int resultCode )
{
	if (finishType == FINISH_SUCCEEDED)
	{
		char urlBuf[1024];
		unsigned len = sizeof(urlBuf);
		if (rlVerify(m_HttpRequest.GetResponseHeaderValue("Location", urlBuf, &len)))
		{
			rlAssert(len > 0);

			// Copy urlBuf to Upload Url
			m_UploadPtr->UploadUrl = urlBuf;
		}
	}

	rlYoutubeDataApiTask::Finish(finishType, resultCode);
}

const char* rlYoutubeGetResumableUploadUrlTask::GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const
{
	return "www.googleapis.com";
}

bool rlYoutubeGetResumableUploadUrlTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	formatf(svcPath, maxLen, "upload/youtube/v3/videos");
	return true;
}

netHttpVerb rlYoutubeGetResumableUploadUrlTask::GetVerb()
{
	return NET_HTTP_VERB_POST;
}

//////////////////////////////////////////////////////
// rlYoutubeUploadChunkTask
//////////////////////////////////////////////////////
rlYoutubeUploadChunkTask::rlYoutubeUploadChunkTask()
	: m_UploadData(NULL)
	, m_ChunkSize(0)
{

}

bool rlYoutubeUploadChunkTask::Configure(rlYoutubeUpload* upload, u8* uploadData, u64 offset, unsigned chunkSize, bool OUTPUT_ONLY(resumedUpdate))
{
	rtry
	{
		rverify(upload, catchall, );
		rverify(uploadData, catchall, );
		rverify(upload->UploadSize > 0, catchall, );
		rverify(chunkSize <= upload->UploadSize, catchall, );
		rverify(upload->BytesUploaded + chunkSize <= upload->UploadSize, catchall, );

#if !__NO_OUTPUT
		// Assert if the channel should not be uploading videos.
		// When we resume an upload we don't retrieve the ChannelInfo data
		// but we assume Youtube won't tell us to resume if the user isn't allowed.
		if (!resumedUpdate)
		{
			rlTaskAssertf(upload->ChannelInfo.ChannelId.length() > 0, "Channel ID was not found in channel info.");
			rlTaskAssertf(upload->ChannelInfo.IsLinked, "Channel has not been linked to Youtube or Google+ - this request will likely fail.");
		}
#endif //!__NO_OUTPUT

		m_UploadPtr = upload;
		m_UploadData = uploadData;
		m_ChunkSize = chunkSize;

		atString authBuf = m_UploadPtr->TokenType;
		authBuf += " ";
		authBuf += m_UploadPtr->AccessToken;

#if !__NO_OUTPUT
		u64 posixTime = rlGetPosixTime();
		if (posixTime > upload->ExpirationTime)
		{
			rlTaskWarning("Based on POSIX time this token is expired. This task will probably fail.");
		}
		else
		{
			u64 secTilExpiry = upload->ExpirationTime - posixTime;
			rlTaskDebug3("Access token expires in %" I64FMT "u seconds", secTilExpiry);
		}
#endif

		char contentRangeBuf[256];

		// The Content-Range includes the starting and end range.
		// Thus, for uploading a chunk of 100,000 bytes, the starting byte is 0 and the ending byte is 99,999 (0-99,999)
		// On the next iteration, 'BytesUploaded' is 100,000, thus start bytes is 100,000 and end bytes is 199,999 (100000-199999)
		// In each instance, the ending byte is the starting byte + the chunk size - 1.
		u64 startingByte = upload->BytesUploaded;
		u64 endingByte = startingByte + m_ChunkSize - 1;
		formatf(contentRangeBuf, "bytes %" I64FMT "u-%" I64FMT "u/%" I64FMT "u", startingByte, endingByte, upload->UploadSize);

		rverify(rlYoutubeDataApiTask::Configure(), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("authorization", authBuf.c_str()), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("Content-Type", "video/*"),catchall,);
		rverify(m_HttpRequest.AddRequestHeaderValue("Content-Range", contentRangeBuf),catchall,);

		u8* data = m_UploadData + offset;
		rverify(m_HttpRequest.AppendContentZeroCopy(data, chunkSize), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// Receives the HTTP response
bool rlYoutubeUploadChunkTask::ProcessResponse(const char* response, int& resultCode)
{
	rtry
	{
#if !__FINAL
		// this command line test has to be done before everything else to work,
		// but the ProcessYoutubeErrors is actually done later in the final else
		if (PARAM_ytNoQuotaDailyUpload.Get())
		{
			response = quotaDailyMsg;

			// No response is expected
			if (response != NULL)
			{
				RsonReader rr(response, istrlen(response));
				if (!ProcessYoutubeErrors(rr))
				{
					return false;
				}
			}
		}
		else
		{
			int odds = -1;
			if (PARAM_ytNoQuotaUpload.Get(odds))
			{
				if (odds >= 100 || g_debugRand.GetRanged(0,100) < odds)

					response = quotaExceededMsg;


				// No response is expected
				if (response != NULL)
				{
					RsonReader rr(response, istrlen(response));
					if (!ProcessYoutubeErrors(rr))
					{
						return false;
					}
				}
			}
		}

#endif


		// A 308 "Resume Incomplete" means the chunk was successful, and the next chunk can be uploaded
		if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_RESUME_INCOMPLETE)
		{
			// a NULL response is expected from the 308
			rverify(response == NULL, catchall, );
			resultCode = UPLOAD_SUCCESS_CONTINUE;
		}
		// A 200 or 201 "OK" or "Created" is expected when completing a video in a single chunk or uploading the last chunk
		else if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_OK ||
				 m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_CREATED)
		{
			m_UploadPtr->m_Response.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);
			rverify(-1 != m_UploadPtr->m_Response.Append(response, ustrlen(response) + 1), catchall, );

			if (response != NULL)
			{
				RsonReader rr(response, istrlen(response));
				if (rr.HasMember("id"))
				{
					RsonReader rrId;
					rverify(rr.GetMember("id", &rrId), catchall,);
					rverify(rrId.AsAtString(m_UploadPtr->VideoInfo.VideoId), catchall,);
				}
			}

			resultCode = UPLOAD_SUCCESS_COMPLETE;
		}
		// The following 5XX errors should result in a failed upload that can be resumed. We must query the status of the upload to 
		// determine ho many bytes were processed, and then resume the upload from that point.
		else if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_INTERNAL_SERVER_ERROR ||
				 m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_BAD_GATEWAY ||
				 m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_SERVICE_UNAVAILABLE ||
				 m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_GATEWAY_TIMEOUT)
		{
			resultCode = UPLOAD_FAILED_RETRY;
		}
		// otherwise, the upload has permanently failed
		else
		{
			resultCode = UPLOAD_FAILED_PERMANENTLY;

			// No response is expected
			if (response != NULL)
			{
				RsonReader rr(response, istrlen(response));
				if (!ProcessYoutubeErrors(rr))
				{
					return false;
				}
			}
		}

		return true;
	}
	rcatchall
	{
		resultCode = -1;
		return false;
	}
}

void rlYoutubeUploadChunkTask::Finish(const FinishType finishType, const int resultCode )
{
	// As long as the upload has not permanently failed, try to retrieve the byte range that has been uploaded.
	if (finishType == FINISH_SUCCEEDED && resultCode != UPLOAD_FAILED_PERMANENTLY)
	{
		char bytesRangeBuf[256];
		unsigned bytesRangeBufLen = sizeof(bytesRangeBuf);
		if (m_HttpRequest.GetResponseHeaderValue("Range", bytesRangeBuf, &bytesRangeBufLen))
		{
			u64 bytesUploaded = 0;
			if (bytesRangeBufLen > 0 &&								// We read in a byte range
				!strncmp(bytesRangeBuf,"bytes=0-",8) &&				// The byte Range has the correct format
				strlen(bytesRangeBuf) >= 9 &&						// something exists at the end of the byte range
				sscanf_s(bytesRangeBuf + 8, "%" I64FMT "u", &bytesUploaded))	// Read in the byte range
			{
				// end range is zero-indexed, so the total number of bytes uploaded is the ending byte + 1
				m_UploadPtr->BytesUploaded = bytesUploaded + 1;
			}
		}
	}
	
	rlYoutubeDataApiTask::Finish(finishType, resultCode);
}


const char* rlYoutubeUploadChunkTask::BuildUrl(char* url, const unsigned sizeofUrl) const
{
	formatf(url,sizeofUrl, "%s", m_UploadPtr->UploadUrl.c_str());
	return url;
}

netHttpVerb rlYoutubeUploadChunkTask::GetVerb()
{
	return NET_HTTP_VERB_PUT;
}

//////////////////////////////////////////////////////
// rlYoutubeGetUploadStatusTask
//////////////////////////////////////////////////////
rlYoutubeGetUploadStatusTask::rlYoutubeGetUploadStatusTask()
{

}

bool rlYoutubeGetUploadStatusTask::Configure(rlYoutubeUpload* upload)
{
	rtry
	{
		rverify(upload, catchall, );
		rverify(upload->UploadSize > 0, catchall, );
		m_UploadPtr = upload;

		atString authBuf = m_UploadPtr->TokenType;
		authBuf += " ";
		authBuf += m_UploadPtr->AccessToken;

		char contentRangeBuf[256];
		formatf(contentRangeBuf, "bytes */%" I64FMT "u", upload->UploadSize);

		rverify(rlYoutubeDataApiTask::Configure(), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("authorization", authBuf.c_str()), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("Content-Range", contentRangeBuf),catchall,);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// Receives the HTTP response
bool rlYoutubeGetUploadStatusTask::ProcessResponse(const char* response, int& resultCode)
{
	// No response is normally expected, but if we stop while youtube video is processing rather than uploading
	// the game could call this again making there be a response 
	// ... deal with the special case of NET_HTTPSTATUS_OK with resume, and the game will do what it needs with the complete video
	if(response)
	{
		if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_OK)
			resultCode = UPLOAD_STATUS_COMPLETE;
	}
	// Querying the status of the upload, it can be resumed
	else if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_RESUME_INCOMPLETE)
	{
		resultCode = UPLOAD_STATUS_CONTINUE;
	}
	// Querying the status of the upload, it has been completed
	else if (m_HttpRequest.GetStatusCode() == NET_HTTPSTATUS_CREATED)
	{
		resultCode = UPLOAD_STATUS_COMPLETE;
	}
	// Querying the status of an upload, it has failed.
	else
	{
		resultCode = UPLOAD_STATUS_FAILED;
	}

	return true;
	
}

void rlYoutubeGetUploadStatusTask::Finish(const FinishType finishType, const int resultCode )
{
	if (finishType == FINISH_SUCCEEDED)
	{
		char bytesRangeBuf[256];
		unsigned bytesRangeBufLen = sizeof(bytesRangeBuf);
		if (m_HttpRequest.GetResponseHeaderValue("Range", bytesRangeBuf, &bytesRangeBufLen))
		{
			// format: bytes=0-n , i.e. bytes=0-420582508
			u64 bytesUploaded = 0;
			if (bytesRangeBufLen > 0 &&								// We read in a byte range
				!strncmp(bytesRangeBuf,"bytes=0-",8) &&				// The byte Range has the correct format
				strlen(bytesRangeBuf) >= 9 &&						// something exists at the end of the byte range
				sscanf_s(bytesRangeBuf + 8, "%" I64FMT "u", &bytesUploaded) == 1)	// Read in the byte range
			{
				m_UploadPtr->BytesUploaded = bytesUploaded;
			}
		}
	}

	rlYoutubeDataApiTask::Finish(finishType, resultCode);
}

const char* rlYoutubeGetUploadStatusTask::BuildUrl(char* url, const unsigned sizeofUrl) const
{
	formatf(url,sizeofUrl, "%s", m_UploadPtr->UploadUrl.c_str());
	return url;
}

netHttpVerb rlYoutubeGetUploadStatusTask::GetVerb()
{
	return NET_HTTP_VERB_PUT;
}

//////////////////////////////////////////////////////
// rlYoutubeUploadTask
//////////////////////////////////////////////////////
unsigned rlYoutubeUploadTask::UPLOAD_WAIT_TIMER[RETRY_TIMERS] = { 0, 1000, 5000, 10000 };	

rlYoutubeUploadTask::rlYoutubeUploadTask()
	: m_LastUploadTime(0)
	, m_BackoffTimer(0)
	, m_UploadData(NULL)
	, m_State(STATE_WAIT_UPLOAD)
	, m_UploadPtr(NULL)
{

}

bool rlYoutubeUploadTask::Configure(rlYoutubeUpload* upload, u8* data)
{
	rtry
	{
		rverify(upload, catchall, );
		rverify(data, catchall, );
		rverify(upload->UploadSize > 0, catchall, );

		// Assert if the channel should not be uploading videos.
		netTaskAssertf(upload->ChannelInfo.ChannelId.length() > 0, "Channel ID was not found in channel info.");
		netTaskAssertf(upload->ChannelInfo.IsLinked, "Channel has not been linked to Youtube or Google+ - this request will likely fail.");

		m_UploadPtr = upload;
		m_UploadData = data;

		return true;
	}
	rcatchall
	{

	}
	return false;
}

void rlYoutubeUploadTask::OnCancel()
{
	if (m_UploadStatus.Pending() && rlGetTaskManager())
	{
		rlGetTaskManager()->CancelTask(&m_UploadStatus);
	}
}

netTaskStatus rlYoutubeUploadTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	if (WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

	switch(m_State)
	{
	case STATE_WAIT_UPLOAD:
		if (sysTimer::GetSystemMsTime() - m_LastUploadTime > UPLOAD_WAIT_TIMER[m_BackoffTimer])
		{
			m_State = STATE_UPLOAD;
		}
		break;
	case STATE_UPLOAD:
		{
			// Calculate the chunkSize and offset for the given upload
			u64 offset = m_UploadPtr->BytesUploaded;
			unsigned chunkSize = m_UploadPtr->GetNextChunkSize();

			// Upload the next chunk
			if (rlYoutube::UploadChunk(m_UploadPtr, m_UploadData, offset, chunkSize, false, &m_UploadStatus))
			{
				m_State = STATE_UPLOADING;
			}
			else
			{
				netTaskError("Could not queue chunk for upload");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		break;
	case STATE_UPLOADING:
		if (!m_UploadStatus.Pending())
		{
			if (m_UploadStatus.Succeeded())
			{
				int result = m_UploadStatus.GetResultCode();
				switch(result)
				{
				case rlYoutubeUploadChunkTask::UPLOAD_SUCCESS_COMPLETE:
					// The upload was successful and the upload is now complete
					status = NET_TASKSTATUS_SUCCEEDED;
					break;
				case rlYoutubeUploadChunkTask::UPLOAD_SUCCESS_CONTINUE:
					// The upload was successful and we can upload the next chunk
					m_State = STATE_UPLOAD;
					m_BackoffTimer = 0; // reset backoff timer
					break;
				case rlYoutubeUploadChunkTask::UPLOAD_FAILED_RETRY:
					// The upload has failed, but we can retry. Increment the backoff timer and get
					// the status of the upload to know where to resume from.
					m_State = STATE_GET_STATUS;
					if (m_BackoffTimer < RETRY_TIMERS-1)
					{
						m_BackoffTimer++;
					}
					else
					{
						netTaskError("Upload failed - backoff timers exhausted");
						status = NET_TASKSTATUS_FAILED;
					}
					break;
				case rlYoutubeUploadChunkTask::UPLOAD_FAILED_PERMANENTLY:
				default:
					netTaskError("Upload failed permanently");
					status = NET_TASKSTATUS_FAILED;
					break;
				}
			}
			else
			{
				netTaskError("Upload failed");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		break;
	case STATE_GET_STATUS:
		if (rlYoutube::GetUploadStatus(m_UploadPtr, &m_UploadStatus))
		{
			m_State = STATE_GETTING_STATUS;
		}
		else
		{
			netTaskError("GetUploadStatus failed");
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_GETTING_STATUS:
		if (!m_UploadStatus.Pending())
		{
			if (m_UploadStatus.Succeeded())
			{
				int result = m_UploadStatus.GetResultCode();
				// The status request determined we can continue
				if (result == rlYoutubeGetUploadStatusTask::UPLOAD_STATUS_CONTINUE)
				{
					m_State = STATE_WAIT_UPLOAD;
				}
				// The status request determined that the upload has completed
				else if (result == rlYoutubeGetUploadStatusTask::UPLOAD_STATUS_COMPLETE)
				{
					status = NET_TASKSTATUS_SUCCEEDED;
				}
				else
				{
					// the upload failed...
					netTaskError("Upload failed");
					status = NET_TASKSTATUS_FAILED;
				}
			}
			else
			{

			}
		}
		break;
	}

	return status;
}

//////////////////////////////////////////////////////
// rlYoutubeShowAccountLinkUiTask
//////////////////////////////////////////////////////
rlYoutubeShowAccountLinkUiTask::rlYoutubeShowAccountLinkUiTask()
	: m_State(STATE_GET_SCAUTH_TOKEN)
	, m_LocalGamerIndex(-1)
{

}

bool rlYoutubeShowAccountLinkUiTask::Configure(const int localGamerIndex, const char * title, const char * route)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(title, catchall, );
		rverify(route, catchall, );

		m_LocalGamerIndex = localGamerIndex;
		m_Title = title;
		m_Route = route;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlYoutubeShowAccountLinkUiTask::OnCancel()
{
	if (m_MyStatus.Pending() && rlGetTaskManager())
	{
		rlGetTaskManager()->CancelTask(&m_MyStatus);
	}
}

bool rlYoutubeShowAccountLinkUiTask::GetScAuthToken()
{
	return rlSocialClub::GetScAuthToken(m_LocalGamerIndex, m_TokenBuf, RLSC_MAX_SCAUTHTOKEN_CHARS, &m_MyStatus);
}

bool rlYoutubeShowAccountLinkUiTask::ShowWebBrowser()
{
	rtry
	{
		const int MAX_SC_AUTH_ENCODED_SIZE = 4 * RLSC_MAX_SCAUTHTOKEN_CHARS;
		const int MAX_LINK_URL_BUF_SIZE = RL_MAX_URL_BUF_LENGTH + // base url
										  MAX_SC_AUTH_ENCODED_SIZE + // max encoded ticket size
										  128 + // extra buffer for &lang={language}
										  16; // extra buffer for ?ScAuthToken=
		// url encode token
		char tokenBuf[MAX_SC_AUTH_ENCODED_SIZE] = {0};
		unsigned dstLen = MAX_SC_AUTH_ENCODED_SIZE, numConsumed = 0;
		rverify(netHttpRequest::UrlEncode(&tokenBuf[0], &dstLen, m_TokenBuf, (int)strlen(m_TokenBuf), &numConsumed, NULL), catchall, );

		// construct URL
		const char* linkUrl = rlYoutube::GetLinkUrl();
		rverify(linkUrl && linkUrl[0] != '\0', catchall, );

		// base url + scauthtoken
		char ytUrl[MAX_LINK_URL_BUF_SIZE] = {0};
		formatf(ytUrl, "%s?ScAuthToken=%s", linkUrl, tokenBuf);

		// client id
		char cidBuf[RLROS_MAX_TITLE_NAME_SIZE + RLROS_MAX_PLATFORM_NAME_SIZE + 8] = { 0 };
		rverifyall(rlScAuth::FormatClientId(cidBuf, sizeof(cidBuf)));
		safecatf(ytUrl, "&cid=%s", cidBuf);

		// language
		safecatf(ytUrl, "&lang=%s", rlScAuth::GetLanguageStr());

		// build callback url
		char cbUrl[RL_MAX_URL_BUF_LENGTH];
		rverify(rlScAuth::BuildCallbackUrl(cbUrl), catchall, );

		// setup browser config
		rlSystemBrowserConfig config;
		config.m_Url = ytUrl;
		config.m_CallbackUrl = cbUrl;
		config.SetFullScreen();
		config.SetUseSafeRegion(true);

		// show web browser
		rverify(g_SystemUi.ShowWebBrowser(m_LocalGamerIndex, config), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

netTaskStatus rlYoutubeShowAccountLinkUiTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	if (WasCanceled())
	{
		return NET_TASKSTATUS_FAILED;
	}

	switch(m_State)
	{
	case STATE_GET_SCAUTH_TOKEN:
		if (GetScAuthToken())
		{
			m_State = STATE_GETTING_SCAUTH_TOKEN;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_GETTING_SCAUTH_TOKEN:
		if (!m_MyStatus.Pending())
		{
			if (m_MyStatus.Succeeded())
			{
				if (ShowWebBrowser())
				{
					status = NET_TASKSTATUS_SUCCEEDED;
				}
				else
				{
					netTaskError("Failed to show web browser.");
					status = NET_TASKSTATUS_FAILED;
				}
			}
			else
			{
				netTaskError("Failed to retrieve sc auth token.");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		break;
	}

	return status;
}


//////////////////////////////////////////////////////
// rlYoutubeGetChannelInfoTask
//////////////////////////////////////////////////////
rlYoutubeGetChannelInfoTask::rlYoutubeGetChannelInfoTask()
{

}

bool rlYoutubeGetChannelInfoTask::Configure(rlYoutubeUpload* upload)
{
	rtry
	{
		rverify(upload, catchall, );
		m_UploadPtr = upload;

		atString authBuf = m_UploadPtr->TokenType;
		authBuf += " ";
		authBuf += m_UploadPtr->AccessToken;

		char fileLenBuf[256];
		formatf(fileLenBuf, "%" I64FMT "u", m_UploadPtr->UploadSize);

		rverify(rlYoutubeDataApiTask::Configure(), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("authorization", authBuf.c_str()), catchall, );

		rverify(m_HttpRequest.AddQueryParam("part", "snippet,status,auditDetails", istrlen("snippet,status,auditDetails")), catchall, );
		rverify(m_HttpRequest.AddQueryParam("mine", "true", istrlen("true")), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// Receives the HTTP response
bool rlYoutubeGetChannelInfoTask::ProcessResponse(const char* response, int& /*resultCode*/)
{
	rtry
	{
#if !__FINAL
		if (PARAM_ytNoQuotaDailyChannel.Get())
		{
			response = quotaDailyMsg;
		}
		else
		{
			int odds = -1;
			if (PARAM_ytNoQuotaChannel.Get(odds))
			{
				if (odds >= 100 || g_debugRand.GetRanged(0,100) < odds)
					response = quotaExceededMsg;
			}
		}

#endif

		if (response != NULL)
		{
			RsonReader rr(response, istrlen(response));
			if (!ProcessYoutubeErrors(rr))
			{
				return false;
			}

			if (rr.HasMember("items"))
			{
				RsonReader rrItems;
				rverify(rr.GetMember("items", &rrItems), catchall, );

				RsonReader rrItem;
				rverify(rrItems.GetFirstMember(&rrItem), catchall, );
				if (rrItem.HasMember("id"))
				{
					RsonReader rrId;
					rverify(rrItem.GetMember("id", &rrId), catchall,);
					rverify(rrId.AsAtString(m_UploadPtr->ChannelInfo.ChannelId), catchall,);
				}

				if (rrItem.HasMember("snippet"))
				{
					RsonReader rrSnippet;
					rverify(rrItem.GetMember("snippet", &rrSnippet), catchall, );

					RsonReader rrTitle;
					rverify(rrSnippet.GetMember("title", &rrTitle), catchall, );
					rverify(rrTitle.AsAtString(m_UploadPtr->ChannelInfo.ChannelTitle), catchall, );
				}

				if (rrItem.HasMember("status"))
				{
					RsonReader rrStatus;
					rverify(rrItem.GetMember("status", &rrStatus), catchall,);

					if (rrStatus.HasMember("privacyStatus"))
					{
						RsonReader rrPrivacyStatus;
						rverify(rrStatus.GetMember("privacyStatus", &rrPrivacyStatus), catchall,);
						atString privStatus;
						rverify(rrPrivacyStatus.AsAtString(privStatus),catchall,);
						if (!stricmp(privStatus.c_str(), "public"))
						{
							m_UploadPtr->ChannelInfo.DefaultPrivacy = rlYoutubeVideoInfo::PRIVATE;
						}
						else if (!stricmp(privStatus.c_str(), "unlisted"))
						{
							m_UploadPtr->ChannelInfo.DefaultPrivacy = rlYoutubeVideoInfo::UNLISTED;
						}
						else if ((!stricmp(privStatus.c_str(), "private")))
						{
							m_UploadPtr->ChannelInfo.DefaultPrivacy = rlYoutubeVideoInfo::PRIVATE;
						}
						else
						{
							rlError("Unknown privacy status: %s", privStatus.c_str());
							m_UploadPtr->ChannelInfo.DefaultPrivacy = rlYoutubeVideoInfo::PRIVATE;
						}
					}

					if (rrStatus.HasMember("longUploadsStatus"))
					{
						RsonReader rrLongUploadStatus;
						rverify(rrStatus.GetMember("longUploadsStatus", &rrLongUploadStatus), catchall,);

						atString longUploadStatus;
						rverify(rrLongUploadStatus.AsAtString(longUploadStatus),catchall,);
						if (!stricmp(longUploadStatus.c_str(), "allowed"))
						{
							m_UploadPtr->ChannelInfo.UploadStatus = rlYoutubeChannelInfo::ALLOWED;
						}
						else if (!stricmp(longUploadStatus.c_str(), "eligible"))
						{
							m_UploadPtr->ChannelInfo.UploadStatus = rlYoutubeChannelInfo::ELIGIBLE;
						}
						else if ((!stricmp(longUploadStatus.c_str(), "disallowed")))
						{
							m_UploadPtr->ChannelInfo.UploadStatus = rlYoutubeChannelInfo::DISALLOWED;
						}
						else
						{
							rlError("Unknown upload status: %s", longUploadStatus.c_str());
							m_UploadPtr->ChannelInfo.UploadStatus = rlYoutubeChannelInfo::DISALLOWED;
						}
					}

					if (rrStatus.HasMember("isLinked"))
					{
						RsonReader rrIsLinked;
						rverify(rrStatus.GetMember("isLinked", &rrIsLinked), catchall,);
						rverify(rrIsLinked.AsBool(m_UploadPtr->ChannelInfo.IsLinked), catchall, );

						if (!m_UploadPtr->ChannelInfo.IsLinked)
						{
							rlWarning("Channel is not linked to either YouTube or Google+ and thus cannot upload videos.");
						}
					}
				}

				if (rrItem.HasMember("auditDetails"))
				{
					RsonReader rrAudit;
					rverify(rrItem.GetMember("auditDetails", &rrAudit), catchall, );

					if (rrAudit.HasMember("overallGoodStanding"))
					{
						RsonReader rrGoodStanding;
						rverify(rrAudit.GetMember("overallGoodStanding", &rrGoodStanding), catchall,);
						rrGoodStanding.AsBool(m_UploadPtr->ChannelInfo.IsGoodStanding);
					}
					else if (rrAudit.HasMember("communityGuidelinesGoodStanding") && rrAudit.HasMember("copyrightStrikesGoodStanding") && rrAudit.HasMember("contentIdClaimsGoodStanding"))
					{
						bool goodA = false;
						rrAudit.ReadBool("communityGuidelinesGoodStanding", goodA);

						bool goodB = false;
						rrAudit.ReadBool("copyrightStrikesGoodStanding", goodB);

						bool goodC = false;
						rrAudit.ReadBool("contentIdClaimsGoodStanding", goodC);

						m_UploadPtr->ChannelInfo.IsGoodStanding = (goodA && goodB && goodC);
					}
				}

				rlDebug1("Channel <%s>: Default Privacy Status: %s, Is Linked: %d, Long Upload Status: %s, Good Standing: %s", m_UploadPtr->ChannelInfo.ChannelTitle.c_str(),  
					m_UploadPtr->ChannelInfo.DefaultPrivacyStr(), m_UploadPtr->ChannelInfo.IsLinked,  m_UploadPtr->ChannelInfo.UploadStatusStr(), m_UploadPtr->ChannelInfo.IsGoodStanding ? "true" : "false");
			}
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlYoutubeGetChannelInfoTask::Finish(const FinishType finishType, const int resultCode )
{
	rlYoutubeDataApiTask::Finish(finishType, resultCode);
}

const char* rlYoutubeGetChannelInfoTask::GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const
{
	return "www.googleapis.com";
}

bool rlYoutubeGetChannelInfoTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	formatf(svcPath, maxLen, "youtube/v3/channels");
	return true;
}

netHttpVerb rlYoutubeGetChannelInfoTask::GetVerb()
{
	return NET_HTTP_VERB_GET;
}

//////////////////////////////////////////////////////
// rlYoutubeGetVideoInfoTask
//////////////////////////////////////////////////////
rlYoutubeGetVideoInfoTask::rlYoutubeGetVideoInfoTask()
{

}

bool rlYoutubeGetVideoInfoTask::Configure(rlYoutubeUpload* upload)
{
	rtry
	{
		rverify(upload, catchall, );
		rverify(upload->VideoInfo.VideoId.length() != 0, catchall, );

		m_UploadPtr = upload;

		atString authBuf = m_UploadPtr->TokenType;
		authBuf += " ";
		authBuf += m_UploadPtr->AccessToken;

#if !__NO_OUTPUT
		u64 posixTime = rlGetPosixTime();
		if (posixTime > upload->ExpirationTime)
		{
			rlTaskWarning("Based on POSIX time this token is expired. This task will probably fail.");
		}
		else
		{
			u64 secTilExpiry = upload->ExpirationTime - posixTime;
			rlTaskDebug3("Access token expires in %" I64FMT "u seconds", secTilExpiry);
		}
#endif

		char fileLenBuf[256];
		formatf(fileLenBuf, "%" I64FMT "u", m_UploadPtr->UploadSize);

		rverify(rlYoutubeDataApiTask::Configure(), catchall, );
		rverify(m_HttpRequest.AddRequestHeaderValue("authorization", authBuf.c_str()), catchall, );

		rverify(m_HttpRequest.AddQueryParam("part", "snippet,status", istrlen("snippet,status")), catchall, );
		rverify(m_HttpRequest.AddQueryParam("id", upload->VideoInfo.VideoId.c_str(), upload->VideoInfo.VideoId.length()), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// Receives the HTTP response
bool rlYoutubeGetVideoInfoTask::ProcessResponse(const char* response, int& resultCode)
{
	rtry
	{
		resultCode = SUCCESS;

#if !__FINAL
		if (PARAM_ytNoQuotaDailyVideo.Get())
		{
			response = quotaDailyMsg;
		}
		else
		{
			int odds = -1;
			if (PARAM_ytNoQuotaVideo.Get(odds))
			{
				if (odds >= 100 || g_debugRand.GetRanged(0,100) < odds)
					response = quotaExceededMsg;
			}
		}

#endif

		if (response != NULL)
		{
			RsonReader rr(response, istrlen(response));
			if (!ProcessYoutubeErrors(rr))
			{
				return false;
			}

			if (rr.HasMember("items"))
			{
				RsonReader rrItems;
				rverify(rr.GetMember("items", &rrItems), catchall,);

				// can legitimately fail here, if the video has been deleted from youtube before publishing it
				RsonReader rrItem;
				if (!rrItems.GetFirstMember(&rrItem))
				{
					resultCode = NO_ITEMS_FOUND;
					return false;
				}

				if (rrItem.HasMember("id"))
				{
					RsonReader rrId;
					rverify(rrItem.GetMember("id", &rrId), catchall,);
					rverify(rrId.AsAtString(m_UploadPtr->VideoInfo.VideoId), catchall,);
				}

				if (rrItem.HasMember("snippet"))
				{
					RsonReader rrSnippet;
					rverify(rrItem.GetMember("snippet", &rrSnippet), catchall,);

					RsonReader rrTitle;
					rverify(rrSnippet.GetMember("title", &rrTitle), catchall, );
					rverify(rrTitle.AsString(m_UploadPtr->VideoInfo.Title), catchall, );

					RsonReader rrDescription;
					rverify(rrSnippet.GetMember("description", &rrDescription), catchall, );
					rverify(rrDescription.AsString(m_UploadPtr->VideoInfo.Description), catchall, );

					RsonReader rrThumbnails;
					rverify(rrSnippet.GetMember("thumbnails", &rrThumbnails), catchall, );

					RsonReader rrThumbDefault;
					rverify(rrThumbnails.GetMember("default", &rrThumbDefault), catchall, );
					RsonReader rrUrl;
					rverify(rrThumbDefault.GetMember("url", &rrUrl), catchall, );
					rverify(rrUrl.AsAtString(m_UploadPtr->VideoInfo.ThumbDefault), catchall, );

					RsonReader rrThumbMedium;
					rverify(rrThumbnails.GetMember("medium", &rrThumbMedium), catchall, );
					rrUrl.Clear();
					rverify(rrThumbMedium.GetMember("url", &rrUrl), catchall, );
					rverify(rrUrl.AsAtString(m_UploadPtr->VideoInfo.ThumbMedium), catchall, );

					RsonReader rrThumbHigh;
					rverify(rrThumbnails.GetMember("high", &rrThumbHigh), catchall, );
					rrUrl.Clear();
					rverify(rrThumbHigh.GetMember("url", &rrUrl), catchall, );
					rverify(rrUrl.AsAtString(m_UploadPtr->VideoInfo.ThumbHigh), catchall, );
				}

				if (rrItem.HasMember("status"))
				{
					RsonReader rrStatus;
					rverify(rrItem.GetMember("status", &rrStatus), catchall, );

					RsonReader rrUploadStatus;
					rverify(rrStatus.GetMember("uploadStatus", &rrUploadStatus), catchall, );
					rverify(rrUploadStatus.AsAtString(m_UploadPtr->VideoInfo.VideoStatus.UploadStatus), catchall, );

					// For once, failure is OPTIONAL !
					RsonReader rrFailureReason;
					if (rrStatus.HasMember("failureReason"))
					{
						rverify(rrStatus.GetMember("failureReason", &rrFailureReason), catchall, );
						rverify(rrFailureReason.AsAtString(m_UploadPtr->VideoInfo.VideoStatus.FailureReason), catchall, );
					}

					// For once, rejection is OPTIONAL !
					RsonReader rrRejectionReason;
					if (rrStatus.HasMember("rejectionReason"))
					{
						rverify(rrStatus.GetMember("rejectionReason", &rrRejectionReason), catchall, );
						rverify(rrRejectionReason.AsAtString(m_UploadPtr->VideoInfo.VideoStatus.RejectionReason), catchall, );
					}

					RsonReader rrPrivacyStatus;
					if (rrStatus.HasMember("privacyStatus"))
					{
						rverify(rrStatus.GetMember("privacyStatus", &rrPrivacyStatus), catchall, );
						rverify(rrPrivacyStatus.AsAtString(m_UploadPtr->VideoInfo.VideoStatus.PrivacyStatus), catchall, );
					}
				}
			}
		}

		return true;
	}
	rcatchall
	{
		resultCode = UNEXPECTED_RESULT;
		return false;
	}
}

void rlYoutubeGetVideoInfoTask::Finish(const FinishType finishType, const int resultCode )
{
	if (finishType == FINISH_SUCCEEDED)
	{
		rlDebug("Successfully read video info:\n Title:%s\n Description:%s\n Default Thumb:%s\n Medium Thumb:%s\n High Thumb:%s\n", m_UploadPtr->VideoInfo.Title, 
			m_UploadPtr->VideoInfo.Description, m_UploadPtr->VideoInfo.ThumbDefault.c_str(), m_UploadPtr->VideoInfo.ThumbMedium.c_str(), m_UploadPtr->VideoInfo.ThumbHigh.c_str());
	}
	else if (resultCode == NO_ITEMS_FOUND)
	{
		rlDebug("No items found in GetVideoInfo response. video has probably been deleted on youtube since upload started.");
	}

	rlYoutubeDataApiTask::Finish(finishType, resultCode);
}

const char* rlYoutubeGetVideoInfoTask::GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const
{
	return "www.googleapis.com";
}

bool rlYoutubeGetVideoInfoTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	formatf(svcPath, maxLen, "youtube/v3/videos");
	return true;
}

netHttpVerb rlYoutubeGetVideoInfoTask::GetVerb()
{
	return NET_HTTP_VERB_GET;
}

}   //namespace rage

