// 
// rline/rlyoutube.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// Youtube Includes
#include "rlyoutube.h"
#include "rlyoutubetasks.h"

// RAGE Includes
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "rline/rlsystemui.h"
#include "rline/rltitleid.h"
#include "rline/ros/rlros.h"
#include "system/memory.h"

#if !RSG_FINAL
PARAM(youtubeLinkUrl, "Override the servers link URL with our own.");
#endif // !RSG_FINAL

namespace rage
{
	extern const rlTitleId* g_rlTitleId;

	char rlYoutube::sm_LinkUrl[RL_MAX_URL_BUF_LENGTH] = {0};

	//////////////////////////////////////////////////////////////
	// rlYoutubeVideoInfo
	//////////////////////////////////////////////////////////////
	rlYoutubeVideoInfo::rlYoutubeVideoInfo()
	{
		Privacy = PRIVATE;
		sysMemSet(Title, 0, sizeof(Title));
		sysMemSet(Description, 0, sizeof(Description));
		sysMemSet(Tags, 0, sizeof(Tags));
		sysMemSet(RsonBuf, 0, sizeof(RsonBuf));
		Embeddable = false;
		CategoryId = 20; // default to "Gaming"
		QualityScore = 0;
		ModdedContent = false;
	}

	const char * rlYoutubeVideoInfo::GetPrivacyString()
	{
		switch(Privacy)
		{
		case PUBLIC:
			return "public";
		case PRIVATE:
			return "private";
		case UNLISTED:
			return "unlisted";
		}

		return "";
	}

	bool rlYoutubeVideoInfo::AddTag(const char* tag)
	{
		rtry
		{
			// rlVerify that adding the tag will not overflow the buffer
			rverify(strlen(Tags) + strlen(tag) + 1 < sizeof(Tags), catchall, );

			// Empty tag, simply add the new tag
			if (Tags[0] == '\0')
			{
				safecat(Tags, tag);
			}
			// Otherwise, comma-delimit the tag
			else
			{
				safecatf(Tags, ",%s", tag);
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlYoutubeVideoInfo::GenerateJSON(RsonWriter* rw)
	{
		/* Example
			{
				"snippet": 
				{
					"title": "Summer vacation in California",
					"description": "Had a great time surfing in Santa Cruz",
					"tags": ["surfing", "Santa Cruz"],
					"categoryId": "22"
				},
				"status": 
				{
					"privacyStatus": "private"
				}
			}
		*/

		rtry
		{
			rw->Init(RsonBuf, RSON_FORMAT_JSON);
			rcheck(rw->Begin(NULL, NULL), catchall, );

			rcheck(rw->Begin("snippet", NULL), catchall,);
			rcheck(rw->WriteString("title", Title), catchall, );
			rcheck(rw->WriteString("description", Description), catchall, );
				rcheck(rw->BeginArray("tags", NULL), catchall, );
					rw->WriteString(NULL, Tags);
				rcheck(rw->End(), catchall, );
			rcheck(rw->WriteInt("categoryId", CategoryId), catchall, );
			rcheck(rw->End(), catchall, );

			rcheck(rw->Begin("status", NULL), catchall,);
			rcheck(rw->WriteString("privacyStatus", GetPrivacyString()), catchall, );
			rcheck(rw->WriteBool("embeddable", Embeddable), catchall, );
			rcheck(rw->WriteString("license", "youtube"), catchall, );
			rcheck(rw->End(), catchall, );

			rcheck(rw->End(), catchall,);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	//////////////////////////////////////////////////////////////
	// rlYoutube
	//////////////////////////////////////////////////////////////
	bool rlYoutube::GetOAuthToken(const int localGamerIndex, rlYoutubeUpload* upload, netStatus* status)
	{
		rlRosGetYoutubeAccessTokenTask* task = NULL;
		bool success = false;

		rtry
		{
			task = rlGetTaskManager()->CreateTask<rlRosGetYoutubeAccessTokenTask>();
			rverify(task,catchall,);
			rverify(rlTaskBase::Configure(task, localGamerIndex, upload, status), catchall,);
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
			success = true;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
		return success;
	}

	//////////////////////////////////////////////////////////////
	// rlYoutube
	//////////////////////////////////////////////////////////////
#if ENABLE_YOUTUBE_DEVICE_TASKS
	bool rlYoutube::GetOAuthDeviceToken(const int localGamerIndex, rlYoutubeUpload* upload, netStatus* status)
	{
		rlYoutubeAuthenticateDeviceTask* task = NULL;

		rtry												
		{										
			rverify(netTask::Create(&task, status), catchall, );																								
			rverify(task->Configure(localGamerIndex, upload), catchall, );	
			rverify(netTask::Run(task), catchall, );			
			return true;										
		}													
		rcatchall											
		{													
			if(task != NULL)									
			{													
				netTask::Destroy(task);								
			}		
			return false;										
		}
	}
#endif // ENABLE_YOUTUBE_DEVICE_TASKS

	bool rlYoutube::GetResumableUploadUrl(rlYoutubeUpload* upload, netStatus* status)
	{
		rlYoutubeGetResumableUploadUrlTask* task = NULL;
		bool success = false;

		rtry
		{
			task = rlGetTaskManager()->CreateTask<rlYoutubeGetResumableUploadUrlTask>();
			rverify(task,catchall,);
			rverify(rlTaskBase::Configure(task, upload, status), catchall,);
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
			success = true;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
		return success;
	}

	bool rlYoutube::Upload(rlYoutubeUpload* upload, u8* data, netStatus* status)
	{
		rlYoutubeUploadTask* task = NULL;

		rtry												
		{										
			rverify(netTask::Create(&task, status), catchall, );																								
			rverify(task->Configure(upload, data), catchall, );	
			rverify(netTask::Run(task), catchall, );			
			return true;										
		}													
		rcatchall											
		{													
			if(task != NULL)									
			{													
				netTask::Destroy(task);								
			}		
			return false;										
		}
	}

	bool rlYoutube::UploadChunk(rlYoutubeUpload* upload, u8* data, u64 offset, unsigned chunkSize, bool resumedUpdate, netStatus* status)
	{
		bool success = false;
		rlYoutubeUploadChunkTask* task = NULL;

		rtry
		{
			task = rlGetTaskManager()->CreateTask<rlYoutubeUploadChunkTask>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, upload, data, offset, chunkSize, resumedUpdate, status), catchall,);
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
			success = true;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
		return success;
	}

	bool rlYoutube::GetUploadStatus(rlYoutubeUpload* upload, netStatus* status)
	{
		bool success = false;
		rlYoutubeGetUploadStatusTask* task = NULL;

		rtry
		{
			task = rlGetTaskManager()->CreateTask<rlYoutubeGetUploadStatusTask>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, upload, status), catchall,);
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
			success = true;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
		return success;
	}

	bool rlYoutube::ShowAccountLinkUi(const int localGamerIndex, const char * title, const char * route, netStatus* status)
	{
		rlYoutubeShowAccountLinkUiTask* task = NULL;

		rtry												
		{		
			rcheck(status, catchall, );
			rverify(netTask::Create(&task, status), catchall, );																								
			rverify(task->Configure(localGamerIndex, title, route), catchall, );	
			rverify(netTask::Run(task), catchall, );			
			return true;										
		}													
		rcatchall											
		{													
			if(task != NULL)									
			{													
				netTask::Destroy(task);								
			}		
			return false;										
		}
	}

	bool rlYoutube::GetChannelInfo(rlYoutubeUpload* upload, netStatus* status)
	{
		bool success = false;
		rlYoutubeGetChannelInfoTask* task = NULL;

		rtry
		{
			task = rlGetTaskManager()->CreateTask<rlYoutubeGetChannelInfoTask>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, upload, status), catchall,);
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
			success = true;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
		return success;
	}

	bool rlYoutube::GetVideoInfo(rlYoutubeUpload* upload, netStatus* status)
	{
		bool success = false;
		rlYoutubeGetVideoInfoTask* task = NULL;

		rtry
		{
			task = rlGetTaskManager()->CreateTask<rlYoutubeGetVideoInfoTask>();
			rverify(task,catchall,);

			rverify(rlTaskBase::Configure(task, upload, status), catchall,);
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
			success = true;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
		return success;
	}

	const char* rlYoutube::GetLinkUrl()
	{
#if !RSG_FINAL
		static const char* youtubeLinkUrl;
		if (PARAM_youtubeLinkUrl.Get(youtubeLinkUrl))
		{
			return youtubeLinkUrl;
		}
#endif // !RSG_FINAL

		return sm_LinkUrl;
	}

	void rlYoutube::SetLinkUrl(const char* linkUrl)
	{
		safecpy(sm_LinkUrl, linkUrl);
	}

}   //namespace rage

