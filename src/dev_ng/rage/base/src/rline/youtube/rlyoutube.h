// 
// rline/rlyoutube.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_YOUTUBE_H
#define RLINE_YOUTUBE_H

#include "atl/string.h"
#include "data/growbuffer.h"
#include "data/rson.h"
#include "net/status.h"
#include "rline/rl.h"
#include "rline/rldiag.h"
#include "system/memory.h"

namespace rage
{
	#define ENABLE_YOUTUBE_DEVICE_TASKS 0

	// PURPOSE
	//	Encapulates a videos upload status, including reasons for failure/rejection
	// https://developers.google.com/youtube/v3/docs/videos#status
	class rlYoutubeVideoStatus
	{
	public:
		rlYoutubeVideoStatus()
		{
			UploadStatus = "";
			FailureReason = "";
			RejectionReason = "";
			PrivacyStatus = "";
		}

		// Possible Values: deleted, failed, processed, rejected, uploaded
		atString UploadStatus;

		// Possible Values: codec, conversion, emptyFile, invalidFile, tooSmall, uploadAborted
		atString FailureReason;

		// Possible Values: claim, copyright, duplicate, inappropriate, length, termsOfUse, trademark, uploaderAccountClosed, uploadedAccountSuspended
		atString RejectionReason;

		// Possible Values: private, public, unlisted
		atString PrivacyStatus;
	};

	// PURPOSE
	//  Encapsulates all the details of a Youtube Video Upload, including its
	//	title, description, tag, visibility and embeddable status.
	class rlYoutubeVideoInfo
	{
	public:
		rlYoutubeVideoInfo();

		enum VideoPrivacy
		{
			PUBLIC,
			UNLISTED,
			PRIVATE
		};

		// PURPOSE
		//	Returns the string representation of the privacy setting
		const char * GetPrivacyString();

		// PURPOSE
		//	Adds a tag to the video upload, returning TRUE if successful.
		bool AddTag(const char* tag);

		// PURPOSE
		//	Initializes an RsonWriter with the video uploads internal buffer,
		//	returning TRUE of the JSON object is created successfully
		bool GenerateJSON(RsonWriter* rw);

		static const int MAX_PRIVACY_LENGTH = 8;		// length of "unlisted"
		static const int MAX_TITLE_LEN = 100;			// youtube title limit of 100 chars
		static const int MAX_DESCRIPTION_LEN = 4850;	// youtube desc limit of 4850 chars
		static const int MAX_TAG_LEN = 500;				// youtube tag limit of 500 chars
		static const int EMBEDDABLE_LEN = 5;			// length of "false"
		static const int CATAGORY_ID = 3;				// max length of categoryId
		static const int FORMAT_BUF_LEN = 256;			// slush for all the {}[] brackets and names

		// Calculate the required RSON buffer size
		static const int RSON_BUF_SIZE = MAX_TITLE_LEN + MAX_DESCRIPTION_LEN + MAX_TAG_LEN + 
										EMBEDDABLE_LEN + CATAGORY_ID + MAX_PRIVACY_LENGTH + FORMAT_BUF_LEN;

		// Members
		atString VideoId;
		char RsonBuf[RSON_BUF_SIZE];
		char Title[MAX_TITLE_LEN];
		char Description[MAX_DESCRIPTION_LEN];
		atString ThumbDefault;
		atString ThumbMedium;
		atString ThumbHigh;
		char Tags[MAX_TAG_LEN];
		bool Embeddable;
		int CategoryId;
		VideoPrivacy Privacy;
		rlYoutubeVideoStatus VideoStatus;
		u32 QualityScore;
		u32 TrackId;
		u32 FilenameHash;
		u32 DurationMs;
		bool ModdedContent;
	};

	struct rlYoutubeChannelInfo
	{
		rlYoutubeChannelInfo()
		{
			ChannelId = "";
			DefaultPrivacy = rlYoutubeVideoInfo::PRIVATE;
			IsLinked = false;
			IsGoodStanding = false;
			UploadStatus = DISALLOWED;
		}

		enum LongUploadStatus
		{
			ALLOWED,	//  This channel can upload videos that are more than 15 minutes long.
			DISALLOWED, //  This channel is not able or eligible to upload videos that are more than 15 minutes long.
			ELIGIBLE    //  This channel is eligible to upload videos that are more than 15 minutes long. 
						//	  However, the channel owner must first enable the ability to upload longer videos at https://www.youtube.com/verify.
		};

		const char* UploadStatusStr()
		{
			switch(UploadStatus)
			{
			case ALLOWED:
				return "ALLOWED";
			case DISALLOWED:
				return "DISALLOWED";
			case ELIGIBLE:
				return "ELIGIBLE";
			default:;
				return "UNKNOWN";
			}
		}

		const char* DefaultPrivacyStr()
		{
			switch(DefaultPrivacy)
			{
			case rlYoutubeVideoInfo::PUBLIC:
				return "PUBLIC";
			case rlYoutubeVideoInfo::PRIVATE:
				return "PRIVATE";
			case rlYoutubeVideoInfo::UNLISTED:
				return "UNLISTED";
			default:
				return "UNKNOWN";
			}
		}

		atString ChannelTitle;
		atString ChannelId;
		rlYoutubeVideoInfo::VideoPrivacy DefaultPrivacy;
		bool IsLinked;
		bool IsGoodStanding;
		LongUploadStatus UploadStatus;
	};

	// PURPOSE
	//	Represents a Youtube Upload object. Contains the OAuth information, such as a device code,
	//	verification URL and access token. When uploading, set the Upload size
	struct rlYoutubeUpload
	{
		static const int CHUNK_SIZE = 1024 * 1024; // Default Chunk Size is 1 MB

		rlYoutubeUpload()
		{
			UploadSize = 0;
			BytesUploaded = 0;
			ExpiresInSecs = 0;
			IntervalSecs = 0;
			ExpirationTime = 0;
			m_ErrCode = -1;
		}

		~rlYoutubeUpload()
		{
			m_Response.Clear();
		}

		// Youtube Max Lengths not well documented, use atStrings for safety and forwards compatibility
		atString DeviceCode;		// The unique code to identify the device for OAuth2 verification
		atString UserCode;			// The user code used to register the device using a web browser
		atString VerificationUrl;	// The Verification URL for OAuth2 activation
		atString UploadUrl;			// The URL to send uploaded chunks
		atString AccessToken;		// The OAuth2 Access token used to authorize upload APIs
		atString TokenType;			// The token type to be used with upload APIs (Bearer)
		atString RefreshToken;		// The Token used for refreshing OAuth2 tokens.
		unsigned ExpiresInSecs;		// How long the UserCode or AccessToken are valid for
		unsigned IntervalSecs;		// The polling interval for the OAuth device
		u64      ExpirationTime;	// The POSIX time that the AccessToken expires

		// Contains information regarding the video upload, such as Title, Description, etc.
		rlYoutubeVideoInfo VideoInfo;

		// Contains information about the user's channel, such as ID, DefaultPrivacy and Long Upload Status
		rlYoutubeChannelInfo ChannelInfo;

		// The upload size of the video and how many bytes have been uploaded
		u64 UploadSize;
		u64 BytesUploaded;

		// Json response from Youtube
		datGrowBuffer m_Response;

		// Errors
		int m_ErrCode;
		atString m_ErrResponse;
		atString m_ErrReason;

		// PURPOSE
		//	Returns the size of the next chunk, either the default CHUNK_SIZE
		//	or the remaining bytes in an upload
		unsigned GetNextChunkSize()
		{
			unsigned chunkSize;
			rlAssert(BytesUploaded < UploadSize);
			if (BytesUploaded + CHUNK_SIZE > UploadSize)
			{
				u64 delta = UploadSize - BytesUploaded;
				chunkSize = (unsigned)delta;
				rlAssert(delta == chunkSize);
			}
			else
			{
				chunkSize = CHUNK_SIZE;
			}

			return chunkSize;
		}
	};

	class rlYoutube
	{
	public:

#if ENABLE_YOUTUBE_DEVICE_TASKS
		// PURPOSE
		//	Returns an OAuthToken using the embedded device method. The system requests a code from
		//	Google and receives a Verification URL. Navigating to the verification URL will allow the
		//	user to enter the device code. This task will poll until the device is authenticated or rejected.
		// NOTES:
		//	For BANK/PC builds, this will actually load up a Web Browser with the device code in the clipboard.
		static bool GetOAuthDeviceToken(const int localGamerIndex, rlYoutubeUpload* upload, netStatus* status);
#endif // ENABLE_YOUTUBE_DEVICE_TASKS

		// PURPOSE
		//	Returns an OAuthToken using linked account credentials. We send the SCS ticket as part of the call,
		//  and the token is returned.
		static bool GetOAuthToken(const int localGamerIndex, rlYoutubeUpload* upload, netStatus* status);

		// PURPOSE
		//	Starts a resumable upload by requesting an upload Url. This requires the rlYoutubeUpload object
		//  to have its rlYoutubeVideoInfo set, such as a Title, Description and Tags. 
		static bool GetResumableUploadUrl(rlYoutubeUpload* upload, netStatus* status);

		// PURPOSE
		//	Uploads a full video. This is all-or-nothing, it cannot be paused or resumed. It handles all Youtube responses,
		//  2XX,3XX,4XX,5XX and completely uploads the given buffer. Useful for smaller file size videos.
		// PARAMS
		//	upload - the object representing the youtube upload
		//	data   - a pointer to the upload buffer.
		static bool Upload(rlYoutubeUpload* upload, u8* data, netStatus* status);

		// PURPOSE
		//	Uploads a chunk of a video with the given data buffer using the given offset and chunk size.
		// PARAMS
		//  offset: Use offset = 0 if you are streaming in chunks. If the entire upload data is in memory, use upload.BytesUploaded
		//	chunksize: Use rlYoutubeUpload::GetNextChunkSize() to calculate the chunkSize in most cases.
		// NOTES:
		//	status result codes:
		//	  308 - Resume Incomplete, continue uploading the next chunk
		//	  200/201 - The video was completely uploaded
		//	  4XX - unrecoverable error, cancel the upload
		//    5XX - retry the upload with an incremental backoff
		static bool UploadChunk(rlYoutubeUpload* upload, u8* data, u64 offset, unsigned chunkSize, bool resumedUpdate, netStatus* status);

		// PURPOSE
		//	Retrieves the upload status, returning the uploaded byte range. Use this if you
		//	receive a 5XX error from the youtube API and need to retry or resume a download.
		static bool GetUploadStatus(rlYoutubeUpload* upload, netStatus* status);

		// PURPOSE
		//	Shows the account linking UI
		static bool ShowAccountLinkUi(const int localGamerIndex, const char * title, const char * route, netStatus* status = NULL);

		//PURPOSE
		//	Retrieves the channel info for the logged in user.
		static bool GetChannelInfo(rlYoutubeUpload* upload, netStatus* status);

		//PURPOSE
		//	Retrieves the video info for the video ID specified in the youtube upload object's 
		//	Fills out the results in the same rlYoutube object.
		static bool GetVideoInfo(rlYoutubeUpload* upload, netStatus* status);

		// PURPOSE
		//	Sets or Retrieves the Youtube account link URL
		static const char* GetLinkUrl();
		static void SetLinkUrl(const char* linkUrl);

private:
		static char sm_LinkUrl[RL_MAX_URL_BUF_LENGTH];
	};
}

#endif // RLINE_YOUTUBE_H
