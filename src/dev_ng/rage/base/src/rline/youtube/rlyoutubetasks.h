// 
// rline/rlyoutubetasks.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_YOUTUBETASKS_H
#define RLINE_YOUTUBETASKS_H

#include "rlyoutube.h"

#include "atl/string.h"
#include "net/http.h"
#include "net/status.h"
#include "net/task.h"
#include "net/tcp.h"
#include "rline/rlhttptask.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/socialclub/rlsocialclubcommon.h"

namespace rage
{
	RAGE_DECLARE_SUBCHANNEL(rline, youtubetasks)

	class rlYoutubeDataApiTask : public rlHttpTask
	{
	public:
		RL_TASK_USE_CHANNEL(rline_youtubetasks);
		RL_TASK_DECL(rlYoutubeDataApiTask);

		rlYoutubeDataApiTask();

		// PURPOSE
		// Basic configuration common to all Youtube Data API tasks
		bool Configure();

	protected:

		//Set to true if request should be sent via HTTPS.
		virtual bool UseHttps() const {return true;}
        //We don't have youtube CA pinned, so must use
        //insecure SSL ctx
        virtual SSL_CTX* GetSslCtx() const {return netTcp::GetInsecureSslCtx();}

		virtual netHttpVerb GetVerb() = 0;
		virtual void Finish(const FinishType finishType, const int resultCode /* = 0 */);
		virtual bool ProcessYoutubeErrors(RsonReader& rr);

		rlHttpTaskConfig m_Config;

		rlYoutubeUpload* m_UploadPtr;
	};

	class rlRosGetYoutubeAccessTokenTask : public rlRosHttpTask
	{
	public:
		RL_TASK_USE_CHANNEL(rline_youtubetasks);
		RL_TASK_DECL(rlRosGetYoutubeAccessTokenTask);

		rlRosGetYoutubeAccessTokenTask();

		virtual ~rlRosGetYoutubeAccessTokenTask();

		bool Configure(int localGamerIndex, rlYoutubeUpload* upload);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode);

		virtual const char* GetServiceMethod() const;

		enum rlYoutubeAccessErrorCodes
		{
			RL_YT_ERROR_UNEXPECTED_RESULT = -1,          
			RL_YT_ERROR_NONE = 0,
			RL_YT_ERROR_AUTHENTICATIONFAILED_TICKET,
			RL_YT_ERROR_NOT_LINKED
		};

		rlYoutubeUpload* m_UploadPtr;
	};

	class rlYoutubeGetResumableUploadUrlTask : public rlYoutubeDataApiTask
	{
	public:
		RL_TASK_USE_CHANNEL(rline_youtubetasks);
		RL_TASK_DECL(rlYoutubeGetResumableUploadUrlTask);

		rlYoutubeGetResumableUploadUrlTask();
		bool Configure(rlYoutubeUpload* upload);

	protected:
		
		// Receives the HTTP response
		virtual bool ProcessResponse(const char* response, int& resultCode);

		virtual void Finish(const FinishType finishType, const int resultCode /* = 0 */);

		virtual const char* GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const;

		virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const;

		// Http Verb
		virtual netHttpVerb GetVerb();
	};

	class rlYoutubeGetUploadStatusTask : public rlYoutubeDataApiTask
	{
	public:
		RL_TASK_USE_CHANNEL(rline_youtubetasks);
		RL_TASK_DECL(rlYoutubeGetUploadStatusTask);

		rlYoutubeGetUploadStatusTask();
		bool Configure(rlYoutubeUpload* upload);

		enum UploadResult
		{
			UPLOAD_STATUS_CONTINUE,
			UPLOAD_STATUS_COMPLETE,
			UPLOAD_STATUS_FAILED
		};

	protected:

		// Receives the HTTP response
		virtual bool ProcessResponse(const char* response, int& resultCode); 

		virtual void Finish(const FinishType finishType, const int resultCode /* = 0 */);

		virtual const char* GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const { return ""; }

		virtual bool GetServicePath(char* /*svcPath*/, const unsigned /*maxLen*/) const { return false; }

		virtual const char* BuildUrl(char* url, const unsigned sizeofUrl) const;

		// Http Verb
		virtual netHttpVerb GetVerb();
	};

	class rlYoutubeUploadChunkTask : public rlYoutubeDataApiTask
	{
	public:
		RL_TASK_USE_CHANNEL(rline_youtubetasks);
		RL_TASK_DECL(rlYoutubeUploadChunkTask);

		rlYoutubeUploadChunkTask();
		bool Configure(rlYoutubeUpload* upload, u8* uploadData, u64 offset, unsigned chunkSize, bool resumedUpdate);

		enum UploadResult
		{
			UPLOAD_SUCCESS_COMPLETE,
			UPLOAD_SUCCESS_CONTINUE,
			UPLOAD_FAILED_RETRY,
			UPLOAD_FAILED_PERMANENTLY
		};

	protected:

		// Receives the HTTP response
		virtual bool ProcessResponse(const char* response, int& resultCode); 

		virtual void Finish(const FinishType finishType, const int resultCode /* = 0 */);

		virtual const char* BuildUrl(char* url, const unsigned sizeofUrl) const;

		virtual const char* GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const { return ""; }

		virtual bool GetServicePath(char* /*svcPath*/, const unsigned /*maxLen*/) const { return false; }

		// Http Verb
		virtual netHttpVerb GetVerb();

		u8* m_UploadData;
		unsigned m_ChunkSize;
	};

	class rlYoutubeUploadTask : public netTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_youtubetasks);
		NET_TASK_DECL(rlYoutubeUploadTask);

		rlYoutubeUploadTask();

		virtual bool Configure(rlYoutubeUpload* upload, u8* data);
		virtual netTaskStatus OnUpdate(int* resultCode);
		virtual void OnCancel();

	private:
		static const int RETRY_TIMERS = 4;
		static unsigned UPLOAD_WAIT_TIMER[RETRY_TIMERS];

		enum State
		{
			STATE_WAIT_UPLOAD,
			STATE_UPLOAD,
			STATE_UPLOADING,
			STATE_GET_STATUS,
			STATE_GETTING_STATUS
		};
		
		State m_State;
		u8* m_UploadData;
		netStatus m_UploadStatus;
		unsigned m_LastUploadTime;
		unsigned m_BackoffTimer;

		rlYoutubeUpload* m_UploadPtr;
	};

	class rlYoutubeShowAccountLinkUiTask : public netTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_youtubetasks);
		NET_TASK_DECL(rlYoutubeShowAccountLinkUiTask);

		rlYoutubeShowAccountLinkUiTask();

		virtual bool Configure(const int localGamerIndex, const char * title, const char * route);
		virtual netTaskStatus OnUpdate(int* resultCode);
		virtual void OnCancel();

	private:

		bool GetScAuthToken();
		bool ShowWebBrowser();

		enum State
		{
			STATE_GET_SCAUTH_TOKEN,
			STATE_GETTING_SCAUTH_TOKEN
		};

		State m_State;
		netStatus m_MyStatus;
		int m_LocalGamerIndex;

		atString m_Title;
		atString m_Route;
		char m_TokenBuf[RLSC_MAX_SCAUTHTOKEN_CHARS];
	};

	class rlYoutubeGetChannelInfoTask : public rlYoutubeDataApiTask
	{
	public:
		RL_TASK_USE_CHANNEL(rline_youtubetasks);
		RL_TASK_DECL(rlYoutubeGetChannelInfoTask);

		rlYoutubeGetChannelInfoTask();
		bool Configure(rlYoutubeUpload* upload);

	protected:

		// Receives the HTTP response
		virtual bool ProcessResponse(const char* response, int& resultCode);

		virtual void Finish(const FinishType finishType, const int resultCode /* = 0 */);

		virtual const char* GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const;

		virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const;

		// Http Verb
		virtual netHttpVerb GetVerb();
	};

	class rlYoutubeGetVideoInfoTask : public rlYoutubeDataApiTask
	{
	public:
		RL_TASK_USE_CHANNEL(rline_youtubetasks);
		RL_TASK_DECL(rlYoutubeGetVideoInfoTask);

		enum GetVideoInfoResult
		{
			UNEXPECTED_RESULT = -1,          
			SUCCESS = 0,
			NO_ITEMS_FOUND
		};

		rlYoutubeGetVideoInfoTask();
		bool Configure(rlYoutubeUpload* upload);

	protected:

		// Receives the HTTP response
		virtual bool ProcessResponse(const char* response, int& resultCode);

		virtual void Finish(const FinishType finishType, const int resultCode /* = 0 */);

		virtual const char* GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const;

		virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const;

		// Http Verb
		virtual netHttpVerb GetVerb();
	};

}

#endif // RLINE_YOUTUBETASKS_H
