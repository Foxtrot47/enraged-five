// 
// rline/scfriends/rlscfriends.cpp
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscfriends.h"
#include "rlscfriendstasks.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"

namespace rage
{

//Override output macros.
#undef __rage_channel
#define __rage_channel rline_scfriends

RAGE_DEFINE_SUBCHANNEL(rline, scfriends)

//////////////////////////////////////////////////////////////////////////
// rlScPlayer
//////////////////////////////////////////////////////////////////////////
rlScPlayer::rlScPlayer()
{
     Clear();
}

void 
rlScPlayer::Clear()
{
    sysMemSet(m_Nickname, 0, sizeof(m_Nickname));
    m_RockstarId = InvalidRockstarId;
    m_Relationship = RLSC_RELATIONSHIP_NONE;

    m_IsOnline = false;
    m_IsPlayingSameTitle = false;

    m_PrimaryClanId = RL_INVALID_CLAN_ID;
    sysMemSet(m_PrimaryClanName, 0, sizeof(m_PrimaryClanName));
    sysMemSet(m_PrimaryClanTag, 0, sizeof(m_PrimaryClanTag));

	sysMemSet(m_RichPresence, 0, sizeof(m_RichPresence));

    m_MutualFriendCount = 0;
}

//////////////////////////////////////////////////////////////////////////
// rlScFriends
//////////////////////////////////////////////////////////////////////////
static bool sm_Initialized = false;

bool 
rlScFriends::Init()
{
    rlAssert(!sm_Initialized);    

    sm_Initialized = true;

    return sm_Initialized;
}

bool 
rlScFriends::IsInitialized()
{
    return sm_Initialized;
}

void 
rlScFriends::Shutdown()
{
    if(sm_Initialized)
    {
        sm_Initialized = false;
    }
}

void 
rlScFriends::Update()
{
}

void
rlScFriends::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

//Convenience macro to create and queue Social Club tasks
#define RLSCFRIENDS_CREATETASK(T, status, localGamerIndex, ...) 	                        \
    bool success = false;																	\
    rlTaskBase* task = NULL;                                                                \
    rtry																					\
    {																						\
        rcheck(rlRos::IsOnline(localGamerIndex),                                            \
               catchall,                                                                    \
               rlDebug("Local gamer %d is not signed into ROS", localGamerIndex));          \
        if (status)                                                                         \
        {                                                                                   \
            T* nonfafTask = rlGetTaskManager()->CreateTask<T>();		                    \
            rverify(nonfafTask,catchall,);													\
            task = nonfafTask;                                                              \
            rverify(rlTaskBase::Configure(nonfafTask, localGamerIndex, __VA_ARGS__, status),\
                    catchall, rlError("Failed to configure task"));                         \
        }                                                                                   \
        else                                                                                \
        {                                                                                   \
            rlFireAndForgetTask<T>* fafTask = NULL;											\
            rverify(rlGetTaskManager()->CreateTask(&fafTask), catchall, );  				\
            task = fafTask;                                                                 \
            rverify(rlTaskBase::Configure(fafTask, localGamerIndex, __VA_ARGS__, &fafTask->m_Status), \
                    catchall, rlError("Failed to configure fire-and-forget task"));         \
        }                                                                                   \
        rverify(rlGetTaskManager()->AppendSerialTask(task), catchall, );       			    \
        success = true;	                                                                    \
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
            rlGetTaskManager()->DestroyTask(task);								            \
        }																					\
    }																						\
    return success;

bool 
rlScFriends::AcceptInvite(const int localGamerIndex, 
                          const RockstarId rockstarId,
                          netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsAcceptInviteTask,
        status, 
        localGamerIndex, 
        rockstarId);
}

bool 
rlScFriends::BlockPlayer(const int localGamerIndex, 
                         const RockstarId rockstarId,
                         netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsBlockPlayerTask,
        status, 
        localGamerIndex, 
        rockstarId);
}

bool 
rlScFriends::CancelInvite(const int localGamerIndex, 
                          const RockstarId rockstarId,
                          netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsCancelInviteTask,
        status, 
        localGamerIndex, 
        rockstarId);
}

bool 
rlScFriends::CountAll(const int localGamerIndex, 
                      int* numBlocked,
                      int* numFriends,
                      int* numInvitesReceived,
                      int* numInvitesSent,
                      netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsCountAllTask,
        status, 
        localGamerIndex, 
        numBlocked,
        numFriends,
        numInvitesReceived,
        numInvitesSent);
}

bool 
rlScFriends::DeclineInvite(const int localGamerIndex, 
                           const RockstarId rockstarId,
                           netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsDeclineInviteTask,
        status, 
        localGamerIndex, 
        rockstarId);
}

bool 
rlScFriends::GetBlocked(const int localGamerIndex, 
                        unsigned int pageIndex,
                        unsigned int pageSize,
                        rlScBlockedPlayer* results,
                        const unsigned sizeofResult,
                        unsigned int* resultCount,
                        unsigned int* resultTotal,
                        netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsGetBlockedPlayersTask,
        status, 
        localGamerIndex, 
        pageIndex,
        pageSize,
        results,
        sizeofResult,
        resultCount,
        resultTotal);
}

bool 
rlScFriends::GetFriends(const int localGamerIndex,
                        unsigned int pageIndex,
                        unsigned int pageSize,
                        rlScFriend* results,
                        const unsigned sizeofResult,
                        unsigned int* resultCount,
                        unsigned int* resultTotal,
                        netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsGetFriendsTask,
        status, 
        localGamerIndex, 
        pageIndex,
        pageSize,
        results,
        sizeofResult,
        resultCount,
        resultTotal);
}

bool 
rlScFriends::GetFriendsAndInvitesSent(const int localGamerIndex,
                                      unsigned int pageIndex,
                                      unsigned int pageSize,
                                      rlScFriend* results,
                                      const unsigned sizeofResult,
                                      unsigned int* resultCount,
                                      unsigned int* resultTotal,
                                      netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsGetFriendsAndInvitesSentTask,
        status, 
        localGamerIndex, 
        pageIndex,
        pageSize,
        results,
        sizeofResult,
        resultCount,
        resultTotal);
}

bool 
rlScFriends::GetInvitesReceived(const int localGamerIndex, 
                                unsigned int pageIndex,
                                unsigned int pageSize,
                                rlScFriendInvite* results,
                                const unsigned sizeofResult,
                                unsigned int* resultCount,
                                unsigned int* resultTotal,
                                netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsGetInvitesReceivedTask,
        status, 
        localGamerIndex, 
        pageIndex,
        pageSize,
        results,
        sizeofResult,
        resultCount,
        resultTotal);
}

bool 
rlScFriends::GetInvitesSent(const int localGamerIndex, 
                            unsigned int pageIndex,
                            unsigned int pageSize,
                            rlScFriendInvite* results,
                            const unsigned sizeofResult,
                            unsigned int* resultCount,
                            unsigned int* resultTotal,
                            netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsGetInvitesSentTask,
        status, 
        localGamerIndex, 
        pageIndex,
        pageSize,
        results,
        sizeofResult,
        resultCount,
        resultTotal);
}

bool 
rlScFriends::RemoveFriend(const int localGamerIndex, 
                          const RockstarId rockstarId,
                          netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsRemoveFriendTask,
        status, 
        localGamerIndex, 
        rockstarId);
}

bool 
rlScFriends::SendInvite(const int localGamerIndex, 
                        const RockstarId rockstarId,
                        netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsSendInviteTask,
        status, 
        localGamerIndex, 
        rockstarId);
}

bool 
rlScFriends::UnblockPlayer(const int localGamerIndex, 
                           const RockstarId rockstarId,
                           netStatus* status)
{
    RLSCFRIENDS_CREATETASK(rlScFriendsUnblockPlayerTask,
        status, 
        localGamerIndex, 
        rockstarId);
}

#if !__NO_OUTPUT

const char* 
rlScFriends::RelationshipToString(const rlScRelationship relationship)
{
    switch(relationship)
    {
    case RLSC_RELATIONSHIP_INVALID:         return "INVALID";
    case RLSC_RELATIONSHIP_NONE:            return "None";
    case RLSC_RELATIONSHIP_BLOCKED_BY_ME:   return "BlockedByMe";
    case RLSC_RELATIONSHIP_BLOCKED_BY_THEM: return "BlockedByThem";
    case RLSC_RELATIONSHIP_BLOCKED_BY_BOTH: return "BlockedByBoth";
    case RLSC_RELATIONSHIP_INVITED_BY_ME:   return "InvitedByMe";
    case RLSC_RELATIONSHIP_INVITED_BY_THEM: return "InvitedByThem";
    case RLSC_RELATIONSHIP_FRIEND:          return "Friend";
    default:
        return NULL;
    }
}

#endif

};
