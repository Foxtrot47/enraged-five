// 
// rline/scfriends/rlscfriends.h 
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCFRIEND_H
#define RLINE_RLSCFRIEND_H

#include "rline/rl.h"
#include "rline/ros/rlroscommon.h"
#include "rline/clan/rlclancommon.h" //For rlScPlayer
#include "rline/scpresence/rlscpresence.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, scfriends)

//PURPOSE
//  Possible relationship states between Rockstar (SC) accounts.
enum rlScRelationship
{
	// Invalid relationship type.
	RLSC_RELATIONSHIP_INVALID = 0x00,

	// They are neither friends, inviting, nor blocking each other.
	RLSC_RELATIONSHIP_NONE = 0x01,

	// I am blocking them
	RLSC_RELATIONSHIP_BLOCKED_BY_ME = 0x02,

	// They are blocking me
	RLSC_RELATIONSHIP_BLOCKED_BY_THEM = 0x04,

	// Both sides are blocking the other
	RLSC_RELATIONSHIP_BLOCKED_BY_BOTH = RLSC_RELATIONSHIP_BLOCKED_BY_ME | RLSC_RELATIONSHIP_BLOCKED_BY_THEM,

	// I am inviting them
	RLSC_RELATIONSHIP_INVITED_BY_ME = 0x08,

	// They are inviting me
	RLSC_RELATIONSHIP_INVITED_BY_THEM = 0x10,

	// We are friends
	RLSC_RELATIONSHIP_FRIEND = 0x20,
};

//PURPOSE
//  Information retrieved from one of the rlScFriend::GetXXX methods.
class rlScPlayer
{
public:
	char m_Nickname[RLSC_MAX_NICKNAME_CHARS];
	RockstarId m_RockstarId;
	rlScRelationship m_Relationship;
	bool m_IsOnline : 1;
	bool m_IsPlayingSameTitle : 1;

    rlClanId m_PrimaryClanId; //RL_INVALID_CLAN_ID if unknown, or not in a clan
    char m_PrimaryClanName[RL_CLAN_NAME_MAX_CHARS];
    char m_PrimaryClanTag[RL_CLAN_TAG_MAX_CHARS];

    int m_MutualFriendCount;

	char m_RichPresence[RLSC_PRESENCE_STRING_MAX_SIZE];

public:
	rlScPlayer();

	// invalidates the player
	void Clear();

	//Returns TRUE if the invite is valid.
	bool IsValid() const { return (m_RockstarId != InvalidRockstarId); }
};

// The Social Club returns the same info for friends, invites, and blocked players.
// Keeping the names separate in case that changes.
typedef rlScPlayer rlScFriend;
typedef rlScPlayer rlScBlockedPlayer;
typedef rlScPlayer rlScFriendInvite;

class netStatus;

//PURPOSE
//  Manages Social Club friends which are tracked by the ROS backend.
//
//USAGE
//  A player's friend list is set of players who have accepted each others friend invites.
//  Friendships are symmetrical. Player A is a friend of player B implies player B is also
//  a friend of player A.
//  
//  To integrated friends into your title, do the following:
//
//  1. During initialization, call rlScFriends::Init().
//  2. Call rlScFriends::Update() each frame.
//  3. Call any methods which seem appropriate.
//  4. Call rlScFriends::Shutdown() when shutting down networking.
//
class rlScFriends
{
public:
    //PURPOSE
    //  Init/Update/Shutdown
    static bool Init();
    static bool IsInitialized();
    static void Shutdown();
    static void Update();

    //PURPOSE
    //  Cancels a pending operation.
    static void Cancel(netStatus* status);

    //PURPOSE
    //  Accepts a pending invite that was received by the local gamer.
    //PARAMS
    //  gamer - The gamer who sent the invitation.
    static bool AcceptInvite(const int localGamerIndex, 
                             const RockstarId rockstarId,
                             netStatus* status);

    //PURPOSE
    //  Block communication (text, voice, invites) to the local gamer from a specified player.
    //PARAMS
    //  gamer - The gamer to block.
    static bool BlockPlayer(const int localGamerIndex, 
                            const RockstarId rockstarId,
                            netStatus* status);

    //PURPOSE
    //  Cancels a pending invite that was sent by the local gamer.
    //PARAMS
    //  gamer - The gamer who was previously invited.
    static bool CancelInvite(const int localGamerIndex, 
                             const RockstarId rockstarId,
                             netStatus* status);

    //PURPOSE
    //  Returns number of other SC accounts local gamer has blocked,
    //  invited, been invited by, or is friends with.
    //PARAMS
    //  localGamerIndex - Local gamer to get counts for
    static bool CountAll(const int localGamerIndex, 
                         int* numBlocked,
                         int* numFriends,
                         int* numInvitesReceived,
                         int* numInvitesSent,
                         netStatus* status);

    //PURPOSE
    //  Declines a pending invite that was received by the local gamer.
    //PARAMS
    //  gamer - The gamer who sent the invitation.
    static bool DeclineInvite(const int localGamerIndex, 
                              const RockstarId rockstarId,
                              netStatus* status);

    //PURPOSE
    //  Retrieve list of players that are blocked by the local gamer.
    //PARAMS
    //  pageIndex			- Index of page to retrieve, starting at 0.
    //  pageSize			- Max number of results to return.
    //  results				- A caller provided array for storing results.
    //  sizeofResult            - Size of each item in the results array.
    //                        Used when the array is of items containing
    //                        an embedded rlScFriend.
    //  resultCount			- The number of results which were returned.
    //  resultTotal	- Total number of results available
    static bool GetBlocked(const int localGamerIndex, 
                           unsigned int pageIndex,
                           unsigned int pageSize,
                           rlScBlockedPlayer* results,
                           const unsigned sizeofResult,
                           unsigned int* resultCount,
                           unsigned int* resultTotal,
                           netStatus* status);

    //PURPOSE
    //  Requests a paged list of friends of the local gamer.
    //  Results will be stored in the given results objects.
    //PARAMS
    //  pageIndex			- Index of page to retrieve, starting at 0.
    //  pageSize			- Max number of results to return.
    //  results				- A caller provided array for storing results.
    //  sizeofResult         - Size of each item in the results array.
    //                        Used when the array is of items containing
    //                        an embedded rlScFriend.
    //  resultCount			- The number of results which were returned.
    //  resultTotal	        - Total number of results available
    static bool GetFriends(const int localGamerIndex,
                           unsigned int pageIndex,
                           unsigned int pageSize,
                           rlScFriend* results,
                           const unsigned sizeofResult,
                           unsigned* resultCount,
                           unsigned* resultTotal,
                           netStatus* status);

    //PURPOSE
    //  Retrieve a paged list that combines friends and invites sent.
    //  Results will be stored in the given results objects.
    //PARAMS
    //  pageIndex			- Index of page to retrieve, starting at 0.
    //  pageSize			- Max number of results to return.
    //  results				- A caller provided array for storing results.
    //  sizeofResult            - Size of each item in the results array.
    //                        Used when the array is of items containing
    //                        an embedded rlScFriend.
    //  resultCount			- The number of results which were returned.
    //  resultTotal	        - Total number of results available
    static bool GetFriendsAndInvitesSent(const int localGamerIndex,
                                         unsigned int pageIndex,
                                         unsigned int pageSize,
                                         rlScFriend* results,
                                         const unsigned sizeofResult,
                                         unsigned int* resultCount,
                                         unsigned int* resultTotal,
                                         netStatus* status);

    //PURPOSE
    //  Retrieve a paged list of pending invites sent to the local gamer.
    //PARAMS
    //  pageIndex			- Index of page to retrieve, starting at 0.
    //  pageSize			- Max number of results to return.
    //  results				- A caller provided array for storing results.
    //  resultCount			- The number of results which were returned.
    //  resultTotal	        - Total number of results available
    static bool GetInvitesReceived(const int localGamerIndex, 
                                   unsigned int pageIndex,
                                   unsigned int pageSize,
                                   rlScFriendInvite* results,
                                   const unsigned sizeofResult,
                                   unsigned int* resultCount,
                                   unsigned int* resultTotal,
                                   netStatus* status);

    //PURPOSE
    //  Retrieve a paged list of pending invites sent by the local gamer.
    //PARAMS
    //  pageIndex			- Index of page to retrieve, starting at 0.
    //  pageSize			- Max number of results to return.
    //  results				- A caller provided array for storing results.
    //  resultCount			- The number of results which were returned.
    //  resultTotal	        - Total number of results available
    static bool GetInvitesSent(const int localGamerIndex, 
                               unsigned int pageIndex,
                               unsigned int pageSize,
                               rlScFriendInvite* results,
                               const unsigned sizeofResult,
                               unsigned int* resultCount,
                               unsigned int* resultTotal,
                               netStatus* status);

    //PURPOSE
    //  Removes a friend from the local gamer's friend list.
    //  Since friendships are symmetrical relationships,
    //  the local gamer will also be removed from the other
    //  gamer's friend list.
    //PARAMS
    //  gamer - The gamer to remove.
    static bool RemoveFriend(const int localGamerIndex, 
                             const RockstarId rockstarId,
                             netStatus* status);

    //PURPOSE
    //  Sends a friend invitation to the specified invitee.
    //PARAMS
    //  gamer - The gamer being invited.
    static bool SendInvite(const int localGamerIndex, 
                           const RockstarId rockstarId,
                           netStatus* status);

    //PURPOSE
    //  Unblocks communication (text, voice, invites) to the local gamer from a specified player.
    //PARAMS
    //  gamer - The gamer to unblock.
    static bool UnblockPlayer(const int localGamerIndex, 
                              const RockstarId rockstarId,
                              netStatus* status);

#if !__NO_OUTPUT
    //PURPOSE
    //  Returns string name of a relationship, or NULL if unknown.
    static const char* RelationshipToString(const rlScRelationship relationship);
#endif

private:
    rlScFriends();
};

} //namespace rage

#endif  //RLINE_RLSCFRIEND_H
