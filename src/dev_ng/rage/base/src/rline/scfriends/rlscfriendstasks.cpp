// 
// rline/scfriends/rlscfriendstasks.cpp
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlscfriendstasks.h"
#include "rlscfriends.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "rline/ros/rlros.h"
#include "string/string.h"

namespace rage
{

//////////////////////////////////////////////////////////////////////////
// rlScFriendsBaseTask
//////////////////////////////////////////////////////////////////////////
rlScFriendsBaseTask::rlScFriendsBaseTask()
{
}

rlScFriendsBaseTask::~rlScFriendsBaseTask()
{
}

int
rlScFriendsBaseTask::ConvertRosResultToError(const rlRosResult& result)
{
    rtry
    {
		if (result.IsError("AuthenticationFailed", "Ticket")) return RL_SC_FRIENDS_ERR_AUTHENTICATION_FAILED;
		if (result.IsError("AuthenticationFailed", "DupLogin")) return RL_SC_FRIENDS_ERR_DUPLICATE_LOGIN;
        if (result.IsError("DoesNotExist", "FriendInviteDoesNotExist")) return RL_SC_FRIENDS_ERR_INVITE_DOES_NOT_EXIST;
        if (result.IsError("NotAllowed", "FriendLimitReached_Inviter")) return RL_SC_FRIENDS_ERR_INVITER_FRIEND_LIMIT_REACHED;
        if (result.IsError("NotAllowed", "FriendLimitReached_Invitee")) return RL_SC_FRIENDS_ERR_INVITEE_FRIEND_LIMIT_REACHED;
        if (result.IsError("NotAllowed", "FriendInviteBlocked")) return RL_SC_FRIENDS_ERR_FRIEND_INVITE_BLOCKED;
        if (result.IsError("AlreadyExist", "AlreadyFriends")) return RL_SC_FRIENDS_ERR_ALREADY_FRIENDS;
		if (result.IsError("AlreadyExists", "AlreadyFriends")) return RL_SC_FRIENDS_ERR_ALREADY_FRIENDS;
        if (result.IsError("DoesNotExist", "InvalidRockstarId_Blocker")) return RL_SC_FRIENDS_ERR_INVALID_BLOCKER_ID;
        if (result.IsError("DoesNotExist", "InvalidRockstarId_Blockee")) return RL_SC_FRIENDS_ERR_INVALID_BLOCKEE_ID;
		if (result.IsError("Redundant", "AlreadyBlocked")) return RL_SC_FRIENDS_ERR_ALREADY_BLOCKED;
		if (result.IsError("NotAllowed", "FriendLimitReached_Blocker")) return RL_SC_FRIENDS_ERR_BLOCKER_FRIEND_LIMIT_REACHED;
		if (result.IsError("DoesNotExist", "FriendInviteDoesNotExist")) return RL_SC_FRIENDS_ERR_FRIEND_INVITE_DOES_NOT_EXIST;
		if (result.IsError("DoesNotExist", "InvalidRockstarId_Inviter")) return RL_SC_FRIENDS_ERR_INVALID_INVITER_ID;
		if (result.IsError("DoesNotExist", "InvalidRockstarId_Invitee")) return RL_SC_FRIENDS_ERR_INVALID_INVITEE_ID;
		if (result.IsError("NotAllowed", "PendingSentLimitReached")) return RL_SC_FRIENDS_ERR_PENDING_SENT_LIMIT_REACHED;
		if (result.IsError("NotAllowed", "PendingReceivedLimitReached")) return RL_SC_FRIENDS_ERR_PENDING_RECEIVED_LIMIT_REACHED;
		if (result.IsError("AlreadyExist", "FriendInviteExists")) return RL_SC_FRIENDS_ERR_FRIEND_INVITE_EXISTS;
		if (result.IsError("AlreadyExists", "FriendInviteExists")) return RL_SC_FRIENDS_ERR_FRIEND_INVITE_EXISTS;
		if (result.IsError("DoesNotExist", "FriendDoesNotExist")) return RL_SC_FRIENDS_ERR_FRIEND_DOES_NOT_EXIST;
		if (result.IsError("DoesNotExist", "BlockDoesNotExist")) return RL_SC_FRIENDS_ERR_BLOCK_DOES_NOT_EXIST;
		if (result.IsError("DoesNotExist", NULL)) return RL_SC_FRIENDS_ERR_DOES_NOT_EXIST;
		if (result.IsError("NotAvailable", NULL)) return RL_SC_FRIENDS_ERR_NOT_AVAILABLE;
		if (result.IsError("SystemError", NULL)) return RL_SC_FRIENDS_ERR_SYSTEM_ERROR;
		if (result.IsError("InvalidArgument", NULL)) return RL_SC_FRIENDS_ERR_INVALID_ARGUMENT;
		if (result.IsError("SqlException", NULL)) return RL_SC_FRIENDS_ERR_SQL_EXCEPTION;

		// TODO improve the alert text in the abort-retry-ignore dialog.
        rverify(false, catchall, 
            rlError("Unknown error code: %s.%s", 
                result.m_Code ? result.m_Code : "", 
                result.m_CodeEx ? result.m_CodeEx : ""));
    }
    rcatchall
    {
    }

    return RL_SC_FRIENDS_ERR_UNKNOWN;
}

//////////////////////////////////////////////////////////////////////////
// rlScFriendsPlayerListBaseTask
//////////////////////////////////////////////////////////////////////////
rlScFriendsPlayerListBaseTask::rlScFriendsPlayerListBaseTask()
{
}

rlScFriendsPlayerListBaseTask::~rlScFriendsPlayerListBaseTask()
{
}

bool 
rlScFriendsPlayerListBaseTask::Configure(const int localGamerIndex,
                                         unsigned int pageIndex,
                                         unsigned int pageSize,
                                         rlScPlayer* results,
                                         const unsigned sizeofResult,
                                         unsigned int* resultCount,
                                         unsigned int* resultTotal)
{
    rtry
    {
        rverify(pageSize > 0, catchall, );
        rverify(results, catchall, );
        rverify(resultCount, catchall, );
        rverify(resultTotal, catchall, );
        rverify(sizeofResult >= sizeof(rlScPlayer), catchall,);
        rverify(rlScFriendsBaseTask::Configure(localGamerIndex, &m_RosSaxReader), catchall, );

        m_PageIndex = pageIndex;
        m_PageSize = pageSize;
        m_Results.Init(results, sizeofResult, pageSize);
        m_ResultCount = resultCount;
        m_ResultTotal = resultTotal;

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("pageIndex", m_PageIndex), catchall, );
        rverify(AddIntParameter("pageSize", m_PageSize), catchall, );

        m_State = STATE_EXPECT_RESULT;
        m_NumRead = 0;
        m_HaveCount = false;
        m_HaveTotal = false;
        
        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlScFriendsPlayerListBaseTask::SaxStartElement(const char* name)
{
    switch((int)m_State)
    {
    case STATE_EXPECT_RESULT:
        if (!stricmp("result", name))
        {
            m_State = STATE_READ_RESULT_ATTRIBUTES;
        }
        break;
    
    case STATE_EXPECT_FRIEND:
        if (!stricmp("f", name))
        {
            m_Results[m_NumRead].Clear();
            m_State = STATE_READ_FRIEND_ATTRIBUTES;
        }
        break;
    }
    return true;
}

bool 
rlScFriendsPlayerListBaseTask::SaxAttribute(const char* name, const char* val)
{
    switch((int)m_State)
    {
    case STATE_READ_RESULT_ATTRIBUTES:
        if (!stricmp("count", name))
        {
            if (sscanf(val, "%d", m_ResultCount) != 1)
            {
                rlTaskError("Failed to parse count from %s", val);
                return false;
            }
            m_HaveCount = true;
        }
        else if (!stricmp("total", name))
        {
            if (sscanf(val, "%d", m_ResultTotal) != 1)
            {
                rlTaskError("Failed to parse total from %s", val);
                return false;
            }
            m_HaveTotal = true;
        }

        if (m_HaveCount && m_HaveTotal)
        {
            m_State = STATE_EXPECT_FRIEND;
        }
        break;

    case STATE_READ_FRIEND_ATTRIBUTES:
        {
            rlScPlayer& player = m_Results[m_NumRead];

            //RockstarId
            if (!stricmp("rid", name))
            {
                if (1 != sscanf(val, "%" I64FMT "d", &player.m_RockstarId))
                {
                    rlTaskError("Failed to parse RockstarID from %s", val);
                    return false;
                }
            }
            
            //Nickname
            else if (!stricmp("n", name))
            {
                safecpy(player.m_Nickname, val);
            }
            
            //IsOnline
            else if (!stricmp("o", name))
            {
                player.m_IsOnline = !stricmp("true", val);
            }
                        
            //IsPlayingSameTitle
            else if (!stricmp("t", name))
            {
                player.m_IsPlayingSameTitle = !stricmp("true", val);
            }
            
            //Relationship
            else if (!stricmp("rel", name))
            {
                if(stricmp(val, "None") == 0)
                {
                    player.m_Relationship = RLSC_RELATIONSHIP_NONE;
                }
                else if(stricmp(val, "BlockedByMe") == 0)
                {
                    player.m_Relationship = RLSC_RELATIONSHIP_BLOCKED_BY_ME;
                }
                else if(stricmp(val, "BlockedByThem") == 0)
                {
                    player.m_Relationship = RLSC_RELATIONSHIP_BLOCKED_BY_THEM;
                }
                else if(stricmp(val, "BlockedByBoth") == 0)
                {
                    player.m_Relationship = RLSC_RELATIONSHIP_BLOCKED_BY_BOTH;
                }
                else if(stricmp(val, "InvitedByMe") == 0)
                {
                    player.m_Relationship = RLSC_RELATIONSHIP_INVITED_BY_ME;
                }
                else if(stricmp(val, "InvitedByThem") == 0)
                {
                    player.m_Relationship = RLSC_RELATIONSHIP_INVITED_BY_THEM;
                }
                else if(stricmp(val, "Friend") == 0)
                {
                    player.m_Relationship = RLSC_RELATIONSHIP_FRIEND;
                }
                else
                {
                    //Alert on unknown values, but allow to proceed
                    rlTaskError("Unknown relationship type: %s", val);
                }
            }
            
            //Mutual friend count
            else if (!stricmp("mfc", name))
            {
                if (1 != sscanf(val, "%d", &player.m_MutualFriendCount))
                {
                    rlTaskError("Failed to parse mutual friend count from %s", val);
                    return false;
                }
            }
            
            //Primary clan ID
            else if (!stricmp("pci", name))
            {
                if (1 != sscanf(val, "%" I64FMT "d", &player.m_PrimaryClanId))
                {
                    rlTaskError("Failed to parse primary clan ID from %s", val);
                    return false;
                }
            }
            
            //Primary clan name
            else if (!stricmp("pcn", name))
            {
                safecpy(player.m_PrimaryClanName, val);
            }
            
            //Primary clan tag
            else if (!stricmp("pct", name))
            {
                safecpy(player.m_PrimaryClanTag, val);
            }
        }
        break;
    }
    return true;
}

bool 
rlScFriendsPlayerListBaseTask::SaxEndElement(const char* name)
{
    switch((int)m_State)
    {
    case STATE_READ_RESULT_ATTRIBUTES:
        if (!stricmp("result", name))
        {
            rlTaskError("<Result> ended before expected attributes were read");
            return false;
        }
        break;

    case STATE_READ_FRIEND_ATTRIBUTES:
        if (!stricmp("f", name))
        {
#if !__NO_OUTPUT
            //Print the friend we read
            const rlScPlayer& player = m_Results[m_NumRead];

            rlTaskDebug3("[%d]:", m_NumRead);
            rlTaskDebug3("\tRockstarId           = %" I64FMT "d", player.m_RockstarId);
            rlTaskDebug3("\tRelationship         = %s", rlScFriends::RelationshipToString(player.m_Relationship));
            rlTaskDebug3("\tIsOnline             = %s", player.m_IsOnline ? "TRUE" : "FALSE");
            rlTaskDebug3("\tIsPlayingSameTitle   = %s", player.m_IsPlayingSameTitle ? "TRUE" : "FALSE");
            if (strlen(player.m_Nickname)) rlTaskDebug3("\tNickname             = %s", player.m_Nickname);
            if (player.m_PrimaryClanId) rlTaskDebug3("\tPrimaryClanId        = %" I64FMT "d", player.m_PrimaryClanId);
            if (player.m_PrimaryClanId) rlTaskDebug3("\tPrimaryClanName      = %s", player.m_PrimaryClanName);
            if (player.m_PrimaryClanId) rlTaskDebug3("\tPrimaryClanTag       = %s", player.m_PrimaryClanTag);
            rlTaskDebug3("\tMutualFriendCount    = %d", player.m_MutualFriendCount);
#endif

            //Go onto the next friend
            ++m_NumRead;
            m_State = STATE_EXPECT_FRIEND;
        }
        break;

#if !__NO_OUTPUT
    case STATE_EXPECT_FRIEND:
        //If we found the end of the <Result> element, we are done reading
        if (!stricmp("result", name))
        {
            //Print the number of friends we read
            rlTaskDebug("Read %d records", m_NumRead);
        }
#endif
    }
    return true;
}

void 
rlScFriendsPlayerListBaseTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
    *m_ResultCount = 0;
    if(NULL != m_ResultTotal)
    {
        *m_ResultTotal = 0;
    }

	resultCode = ConvertRosResultToError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlScFriendsSimpleEventBaseTask
//////////////////////////////////////////////////////////////////////////
rlScFriendsSimpleEventBaseTask::rlScFriendsSimpleEventBaseTask()
{
}

rlScFriendsSimpleEventBaseTask::~rlScFriendsSimpleEventBaseTask()
{
}

bool 
rlScFriendsSimpleEventBaseTask::Configure(const int localGamerIndex,
                                          const RockstarId rockstarId)
{
    bool success = false;

    rtry
    {
        rverify(rlScFriendsBaseTask::Configure(localGamerIndex), catchall, );

        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddIntParameter("rockstarId", rockstarId), catchall, );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool 
rlScFriendsSimpleEventBaseTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& /*resultCode*/)
{
    return true;
}

void 
rlScFriendsSimpleEventBaseTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode )
{
	resultCode = ConvertRosResultToError(result);
}

///////////////////////////////////////////////////////////////////////////////
//  rlScFriendsCountAllTask
///////////////////////////////////////////////////////////////////////////////
bool
rlScFriendsCountAllTask::Configure(const int localGamerIndex, 
                                   int* numBlocked,
                                   int* numFriends,
                                   int* numInvitesReceived,
                                   int* numInvitesSent)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_NumBlocked = numBlocked;
    m_NumFriends = numFriends;
    m_NumInvitesReceived = numInvitesReceived;
    m_NumInvitesSent = numInvitesSent;

    return true;
}

bool 
rlScFriendsCountAllTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                        const parTreeNode* node, 
                                        int& resultCode)
{
    resultCode = 0;

    rlAssert(node);

    rtry
    {
        const parTreeNode* result = node->FindChildWithName("Result");
        rverify(result, catchall, );

        if (m_NumBlocked) 
        {
            rverify(rlHttpTaskHelper::ReadInt(*m_NumBlocked, result, NULL, "b"), catchall, );
            rlTaskDebug3("NumBlocked = %d", *m_NumBlocked);
        }

        if (m_NumFriends)
        {
            rverify(rlHttpTaskHelper::ReadInt(*m_NumFriends, result, NULL, "f"), catchall, );
            rlTaskDebug3("NumFriends = %d", *m_NumFriends);
        }

        if (m_NumInvitesReceived)
        {
            rverify(rlHttpTaskHelper::ReadInt(*m_NumInvitesReceived, result, NULL, "ir"), catchall, );
            rlTaskDebug3("NumInvitesReceived = %d", *m_NumInvitesReceived);
        }

        if (m_NumInvitesSent)
        {
            rverify(rlHttpTaskHelper::ReadInt(*m_NumInvitesSent, result, NULL, "is"), catchall, );
            rlTaskDebug3("NumInvitesSent = %d", *m_NumInvitesSent);
        }

        return true;
    }
    rcatchall
    {
        resultCode = -1;
        return false;
    }
}

void
rlScFriendsCountAllTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
	resultCode = rlScFriendsBaseTask::ConvertRosResultToError(result);
}

}   //namespace rage
