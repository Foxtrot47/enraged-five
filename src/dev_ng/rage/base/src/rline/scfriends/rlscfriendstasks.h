// 
// rline/scfriends/rlscfriendstasks.h
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSCFRIENDSTASKS_H
#define RLINE_RLSCFRIENDSTASKS_H

#include "rlscfriends.h"
#include "atl/array.h"
#include "rline/ros/rlroshttptask.h"

namespace rage
{

class rlScPlayer;

enum rlScFriendsErrorCodes
{
	// don't change the values, only add before UNKONWN. The SCUI uses these values.
	RL_SC_FRIENDS_SUCCESS = 0,
	RL_SC_FRIENDS_ERR_AUTHENTICATION_FAILED = 1,
	RL_SC_FRIENDS_ERR_DOES_NOT_EXIST = 2,
	RL_SC_FRIENDS_ERR_NOT_AVAILABLE = 3,
	RL_SC_FRIENDS_ERR_SYSTEM_ERROR = 4,
	RL_SC_FRIENDS_ERR_INVALID_ARGUMENT = 5,
	RL_SC_FRIENDS_ERR_INVITE_DOES_NOT_EXIST = 6,
	RL_SC_FRIENDS_ERR_INVITER_FRIEND_LIMIT_REACHED = 7,
	RL_SC_FRIENDS_ERR_INVITEE_FRIEND_LIMIT_REACHED = 8,
	RL_SC_FRIENDS_ERR_FRIEND_INVITE_BLOCKED = 9,
	RL_SC_FRIENDS_ERR_ALREADY_FRIENDS = 10,
	RL_SC_FRIENDS_ERR_INVALID_BLOCKER_ID = 11,
	RL_SC_FRIENDS_ERR_INVALID_BLOCKEE_ID = 12,
	RL_SC_FRIENDS_ERR_ALREADY_BLOCKED = 13,
	RL_SC_FRIENDS_ERR_BLOCKER_FRIEND_LIMIT_REACHED = 14,
	RL_SC_FRIENDS_ERR_FRIEND_INVITE_DOES_NOT_EXIST = 15,
	RL_SC_FRIENDS_ERR_INVALID_INVITER_ID = 16,
	RL_SC_FRIENDS_ERR_INVALID_INVITEE_ID = 17,
	RL_SC_FRIENDS_ERR_PENDING_SENT_LIMIT_REACHED = 18,
	RL_SC_FRIENDS_ERR_PENDING_RECEIVED_LIMIT_REACHED = 19,
	RL_SC_FRIENDS_ERR_FRIEND_INVITE_EXISTS = 20,
	RL_SC_FRIENDS_ERR_FRIEND_DOES_NOT_EXIST = 21,
	RL_SC_FRIENDS_ERR_BLOCK_DOES_NOT_EXIST = 22,
	RL_SC_FRIENDS_ERR_DUPLICATE_LOGIN = 23,
	RL_SC_FRIENDS_ERR_SQL_EXCEPTION = 24,

	RL_SC_FRIENDS_ERR_UNKNOWN = 999
};

class rlScFriendsBaseTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScFriendsBaseTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

    rlScFriendsBaseTask();
    virtual ~rlScFriendsBaseTask();

	static int ConvertRosResultToError(const rlRosResult& result);

protected:
	virtual const char* GetServiceMethod() const = 0;
};

class rlScFriendsPlayerListBaseTask : public rlScFriendsBaseTask
{
public:
	RL_TASK_DECL(rlScFriendsPlayerListBaseTask);
	RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsPlayerListBaseTask();
	virtual ~rlScFriendsPlayerListBaseTask();

	bool Configure(const int localGamerIndex,
                   unsigned int pageIndex,
                   unsigned int pageSize,
                   rlScPlayer* results,
                   const unsigned resultSpan,
                   unsigned int* resultCount,
                   unsigned int* resultTotal);

protected:
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual bool SaxStartElement(const char* name);
    virtual bool SaxAttribute(const char* name, const char* val);
    virtual bool SaxEndElement(const char* name);

    virtual rlScRelationship GetDefaultRelationshipType() = 0;

private:
    unsigned int m_PageIndex;
    unsigned int m_PageSize;
    atSpanArray<rlScPlayer> m_Results;
    unsigned int* m_ResultCount;
    unsigned int* m_ResultTotal;

    rlRosSaxReader m_RosSaxReader;

    enum State
    {
        STATE_EXPECT_RESULT,            //Looking for <Result> element
        STATE_READ_RESULT_ATTRIBUTES,   //Need to read Count and Total attributes
        STATE_EXPECT_FRIEND,            //Looking for <f> element
        STATE_READ_FRIEND_ATTRIBUTES,   //Reading attributes of <f>
    };
    State m_State;
    
    unsigned m_NumRead;     //Num results read so far
    bool m_HaveCount : 1;   //True if we read the <Result> Count attribute
    bool m_HaveTotal : 1;   //True if we read the <Result> Total attribute
};

class rlScFriendsSimpleEventBaseTask : public rlScFriendsBaseTask
{
public:
	RL_TASK_DECL(rlScFriendsSimpleEventBaseTask);
	RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsSimpleEventBaseTask();
	virtual ~rlScFriendsSimpleEventBaseTask();

	bool Configure(const int localGamerIndex,
				   const RockstarId rockstarId);

protected:
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

class rlScFriendsSendInviteTask : public rlScFriendsSimpleEventBaseTask
{
public:
    RL_TASK_DECL(rlScFriendsSendInviteTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsSendInviteTask() {}
	virtual ~rlScFriendsSendInviteTask() {}

protected:
	virtual const char* GetServiceMethod() const {return "Friends.asmx/InviteByRockstarId";}
};

class rlScFriendsCancelInviteTask : public rlScFriendsSimpleEventBaseTask
{
public:
    RL_TASK_DECL(rlScFriendsCancelInviteTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

    rlScFriendsCancelInviteTask() {}
    virtual ~rlScFriendsCancelInviteTask() {}

protected:
    virtual const char* GetServiceMethod() const {return "Friends.asmx/CancelInvite";}
};

class rlScFriendsCountAllTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlScFriendsCountAllTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

    rlScFriendsCountAllTask() {}
    virtual ~rlScFriendsCountAllTask() {}

    bool Configure(const int localGamerIndex, 
                   int* numBlocked,
                   int* numFriends,
                   int* numInvitesReceived,
                   int* numInvitesSent);

protected:
    virtual const char* GetServiceMethod() const {return "Friends.asmx/CountAll";}
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    int* m_NumBlocked;
    int* m_NumFriends;
    int* m_NumInvitesReceived;
    int* m_NumInvitesSent;
};

class rlScFriendsDeclineInviteTask : public rlScFriendsSimpleEventBaseTask
{
public:
    RL_TASK_DECL(rlScFriendsDeclineInviteTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

    rlScFriendsDeclineInviteTask() {}
    virtual ~rlScFriendsDeclineInviteTask() {}

protected:
    virtual const char* GetServiceMethod() const {return "Friends.asmx/DeclineInvite";}
};

class rlScFriendsAcceptInviteTask : public rlScFriendsSimpleEventBaseTask
{
public:
    RL_TASK_DECL(rlScFriendsAcceptInviteTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

    rlScFriendsAcceptInviteTask() {}
    virtual ~rlScFriendsAcceptInviteTask() {}

protected:
    virtual const char* GetServiceMethod() const {return "Friends.asmx/AcceptInvite";}
};

class rlScFriendsRemoveFriendTask : public rlScFriendsSimpleEventBaseTask
{
public:
    RL_TASK_DECL(rlScFriendsRemoveFriendTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

    rlScFriendsRemoveFriendTask() {}
    virtual ~rlScFriendsRemoveFriendTask() {}

protected:
    virtual const char* GetServiceMethod() const {return "Friends.asmx/RemoveFriend";}
};

class rlScFriendsGetInvitesSentTask : public rlScFriendsPlayerListBaseTask
{
public:
	RL_TASK_DECL(rlScFriendsGetInvitesSentTask);
	RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsGetInvitesSentTask() {}
	virtual ~rlScFriendsGetInvitesSentTask() {}

protected:
	virtual const char* GetServiceMethod() const {return "Friends.asmx/GetInvitesSent";}
	virtual rlScRelationship GetDefaultRelationshipType() {return RLSC_RELATIONSHIP_INVITED_BY_ME;}
};

class rlScFriendsGetInvitesReceivedTask : public rlScFriendsPlayerListBaseTask
{
public:
	RL_TASK_DECL(rlScFriendsGetInvitesReceivedTask);
	RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsGetInvitesReceivedTask() {}
	virtual ~rlScFriendsGetInvitesReceivedTask() {}

protected:
	virtual const char* GetServiceMethod() const {return "Friends.asmx/GetInvitesReceived";}
	virtual rlScRelationship GetDefaultRelationshipType() {return RLSC_RELATIONSHIP_INVITED_BY_THEM;}
};

class rlScFriendsGetFriendsTask : public rlScFriendsPlayerListBaseTask
{
public:
	RL_TASK_DECL(rlScFriendsGetFriendsTask);
	RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsGetFriendsTask() {}
	virtual ~rlScFriendsGetFriendsTask() {}

protected:
	virtual const char* GetServiceMethod() const {return "Friends.asmx/GetFriends";}
	virtual rlScRelationship GetDefaultRelationshipType() {return RLSC_RELATIONSHIP_FRIEND;}
};

class rlScFriendsGetFriendsAndInvitesSentTask : public rlScFriendsPlayerListBaseTask
{
public:
	RL_TASK_DECL(rlScFriendsGetFriendsAndInvitesSentTask);
	RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsGetFriendsAndInvitesSentTask() {}
	virtual ~rlScFriendsGetFriendsAndInvitesSentTask() {}

protected:
	virtual const char* GetServiceMethod() const {return "Friends.asmx/GetFriendsAndInvitesSent";}
	virtual rlScRelationship GetDefaultRelationshipType() {return RLSC_RELATIONSHIP_INVALID;}
};

class rlScFriendsBlockPlayerTask : public rlScFriendsSimpleEventBaseTask
{
public:
    RL_TASK_DECL(rlScFriendsBlockPlayerTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsBlockPlayerTask() {}
	virtual ~rlScFriendsBlockPlayerTask() {}

protected:
    virtual const char* GetServiceMethod() const {return "Friends.asmx/Block";}
};

class rlScFriendsUnblockPlayerTask : public rlScFriendsSimpleEventBaseTask
{
public:
    RL_TASK_DECL(rlScFriendsUnblockPlayerTask);
    RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsUnblockPlayerTask() {}
	virtual ~rlScFriendsUnblockPlayerTask() {}

protected:
    virtual const char* GetServiceMethod() const {return "Friends.asmx/Unblock";}
};

class rlScFriendsGetBlockedPlayersTask : public rlScFriendsPlayerListBaseTask
{
public:
	RL_TASK_DECL(rlScFriendsGetBlockedPlayersTask);
	RL_TASK_USE_CHANNEL(rline_scfriends);

	rlScFriendsGetBlockedPlayersTask() {}
	virtual ~rlScFriendsGetBlockedPlayersTask() {}

protected:
	virtual const char* GetServiceMethod() const {return "Friends.asmx/GetBlocked";}
	virtual rlScRelationship GetDefaultRelationshipType() {return RLSC_RELATIONSHIP_BLOCKED_BY_ME;}
};

};

#endif //RLINE_RLSCFRIENDSTASKS_H
