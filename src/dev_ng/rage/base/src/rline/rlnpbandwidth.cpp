// 
// rline/rlnpbandwidth.cpp
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "rlnpbandwidth.h"

#if RL_NP_BANDWIDTH

#include "bank/bank.h"
#include "rline/rlpresence.h"
#include "system/threadpool.h"

// sdk includes
#include <libsysmodule.h>

#pragma comment(lib, "libSceNpUtility_stub_weak.a")

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, npbandwidth);
#undef __rage_channel
#define __rage_channel rline_npbandwidth

const unsigned rlNpBandwidth::DEFAULT_TEST_TIMEOUT_SEC = 20;
#define RL_NP_INVALID_BANDWIDTH_CTX -1

extern sysThreadPool netThreadPool;

/****************************************
rlNpShutdownBandwidthWorkItem
****************************************/
rlNpShutdownBandwidthWorkItem::rlNpShutdownBandwidthWorkItem()
	: m_PendingResult(nullptr)
	, m_Ctx(nullptr)
	, m_Success(false)
{

}
void rlNpShutdownBandwidthWorkItem::Configure(netBandwidthTestResult* result, int* contextPtr)
{
	m_PendingResult = result;
	m_Ctx = contextPtr;
	m_Success = false;
}

void rlNpShutdownBandwidthWorkItem::DoWork()
{
	rtry
	{
		SceNpBandwidthTestResult result;
		int ret = sceNpBandwidthTestShutdown(*m_Ctx, &result);

		if (m_PendingResult)
		{
			m_PendingResult->m_ResultCode = ret;
		}

		// Only do this now, if the task gets canceled we need it for a blocking shutdown.
		(*m_Ctx) = RL_NP_INVALID_BANDWIDTH_CTX;

		rcheck(ret == SCE_OK, catchall, rlError("sceNpBandwidthTestShutdown failed. ret = 0x%x", ret));
		rcheck(result.result == SCE_OK, catchall, rlError("SceNpBandwidthTestResult failed. ret = 0x%x", result.result));

		rlDebug1("Bandwidth download Bps: %f upload Bps: %f result[0x%x]", result.downloadBps, result.uploadBps, result.result);

		if (m_PendingResult)
		{
			m_PendingResult->m_DownloadBps = (int)result.downloadBps;
			m_PendingResult->m_UploadBps = (int)result.uploadBps;
		}

		m_Success = true;
	}
	rcatchall
	{
	}
}

/****************************************
rlNpBandwidth
****************************************/
rlNpBandwidth::rlNpBandwidth()
    : m_State(rlNpBandwidthState::None)
    , m_ExternalNetStatus(nullptr)
    , m_TestCtx(RL_NP_INVALID_BANDWIDTH_CTX)
{
}

rlNpBandwidth::~rlNpBandwidth()
{
}

void
rlNpBandwidth::InitLibs()
{
    // SCE_SYSMODULE_NP_UTILITY can theoretically be loaded elsewhere
    const bool isLoaded = (sceSysmoduleIsLoaded(SCE_SYSMODULE_NP_UTILITY) == SCE_OK);

    if (!isLoaded && sceSysmoduleLoadModule(SCE_SYSMODULE_NP_UTILITY) != SCE_OK)
    {
        Quitf("Failed to load prx SCE_SYSMODULE_NP_UTILITY");
    }
}

bool
rlNpBandwidth::Init()
{
    return true;
}

void
rlNpBandwidth::Shutdown()
{
	m_ShutdownWorkItem.Cancel();
    AbortBandwidthTestBlocking();
}

void
rlNpBandwidth::Update()
{
    switch (m_State)
    {
    case rlNpBandwidthState::None: break;

    case rlNpBandwidthState::Testing:
        CheckStatus(m_State); break;
    case rlNpBandwidthState::ReadingResult:
        CheckResultStatus(m_State); break;

    case rlNpBandwidthState::Failed: break;
    }
}

void
rlNpBandwidth::AbortBandwidthTest()
{
    if (m_TestCtx >= 0)
    {
        rlDebug2("AbortBandwidthTest m_TestCtx[%d]", m_TestCtx);
        sceNpBandwidthTestAbort(m_TestCtx);

        if (m_ExternalNetStatus)
        {
            m_ExternalNetStatus->SetCanceled();
        }
    }
}

void
rlNpBandwidth::AbortBandwidthTestBlocking()
{
    if (m_TestCtx >= 0)
    {
        rlDebug2("AbortBandwidthTestBlocking m_TestCtx[%d]", m_TestCtx);
        sceNpBandwidthTestAbort(m_TestCtx);
        SceNpBandwidthTestResult result;
        sceNpBandwidthTestShutdown(m_TestCtx, &result);
        m_TestCtx = RL_NP_INVALID_BANDWIDTH_CTX;
    }
}

bool
rlNpBandwidth::HasValidResult() const
{
    return m_Result.IsValid();
}

bool
rlNpBandwidth::IsTestActive() const
{
    return (m_State != rlNpBandwidthState::Failed && m_State != rlNpBandwidthState::None) || m_TestCtx != RL_NP_INVALID_BANDWIDTH_CTX;
}

const netBandwidthTestResult&
rlNpBandwidth::GetTestResult() const
{
    return m_Result;
}

void
rlNpBandwidth::ClearTestResult()
{
    m_Result = netBandwidthTestResult();
}

bool
rlNpBandwidth::TestBandwidth(netStatus* status)
{
    rtry
    {
        rcheck(m_State == rlNpBandwidthState::None || m_State == rlNpBandwidthState::Failed, catchall, rlWarning("TestBandwidth skipped. m_State[%d]", (int)m_State));
        m_PendingResult = netBandwidthTestResult(ATSTRINGHASH("SceBandwidthTest", 0x51B58CC), rlGetPosixTime());

        rcheckall(Start());

        if (status)
        {
            m_ExternalNetStatus = status;
            m_ExternalNetStatus->SetPending();
        }

        return true;
    }
    rcatchall
    {
    }

    return false;
}

void
rlNpBandwidth::InitParam(SceNpBandwidthTestInitParam& param)
{
    memset(&param, 0x00, sizeof(param));
    param.size = sizeof(param);
    param.threadPriority = SCE_KERNEL_PRIO_FIFO_DEFAULT;
    param.cpuAffinityMask = 0;
}

bool
rlNpBandwidth::Start()
{
    rtry
    {
        SceNpBandwidthTestInitParam param;
        InitParam(param);

        int ret = sceNpBandwidthTestInitStart(&param);
        m_PendingResult.m_ResultCode = ret;

        rverify(ret >= 0, catchall, rlError("sceNpBandwidthTestInitStart failed. ret = 0x%x", ret));

        m_TestCtx = ret;
        m_State = rlNpBandwidthState::Testing;
        rlDebug2("Start m_TestCtx[%d]", m_TestCtx);

        return true;
    }
    rcatchall
    {
    }

    return false;
}

void
rlNpBandwidth::CheckStatus(rlNpBandwidthState state)
{
    rtry
    {
        int status = 0;
        int ret = sceNpBandwidthTestGetStatus(m_TestCtx, &status);
        m_PendingResult.m_ResultCode = ret;

        rcheck(ret == SCE_OK, catchall, rlError("sceNpBandwidthTestGetStatus failed. ret = 0x%x", ret));
        rverify(status != SCE_NP_BANDWIDTH_TEST_STATUS_NONE, catchall, rlError("Should be running or finished. m_TestCtx[%d]", m_TestCtx));

        if (status == SCE_NP_BANDWIDTH_TEST_STATUS_RUNNING)
        {
            return;
        }

        rlDebug2("CheckStatus m_TestCtx[%d] status[%d]", m_TestCtx, (int)status);
        rcheck(RetrieveResult(state), catchall, rlError("RetrieveResult failed. m_TestCtx[%d]", m_TestCtx));
        m_State = rlNpBandwidthState::ReadingResult;

        return;
    }
    rcatchall
    {
    }

    ShutdownBandwidthTest();
    SetFailed();
}

void
rlNpBandwidth::CheckResultStatus(rlNpBandwidthState state)
{
    if (m_ShutdownWorkItem.Pending())
    {
        return;
    }

    if (!m_ShutdownWorkItem.Succeeded())
    {
        rlDebug1("Bandwidth task failed");
        SetFailed();
        return;
    }

	//m_ShutdownWorkItem.Reset();

    if (state == rlNpBandwidthState::ReadingResult)
    {
        rlDebug1("Bandwidth check completed");
        m_Result = m_PendingResult;
        SetSucceeded();
    }
    else
    {
        rlError("Unexpected state %d", (int)m_State);
        ShutdownBandwidthTest();
        SetFailed();
    }
}

bool
rlNpBandwidth::RetrieveResult(rlNpBandwidthState state)
{
	m_ShutdownWorkItem.Configure(&m_PendingResult, &m_TestCtx);

	if (!netThreadPool.QueueWork(&m_ShutdownWorkItem))
	{
		rlError("netThreadPool failed to queue ShutdownWorkItem");
		return false;
	}

	return true;
}

void
rlNpBandwidth::SetFailed()
{
    if (m_ExternalNetStatus)
    {
        m_ExternalNetStatus->SetFailed();
        m_ExternalNetStatus = nullptr;
    }

    m_State = rlNpBandwidthState::Failed;
}

void
rlNpBandwidth::SetSucceeded()
{
    if (m_ExternalNetStatus)
    {
        m_ExternalNetStatus->SetSucceeded();
        m_ExternalNetStatus = nullptr;
    }

    m_State = rlNpBandwidthState::None;
}

void
rlNpBandwidth::ShutdownBandwidthTest()
{
    if (m_TestCtx < 0)
    {
        return;
    }

	// sceNpBandwidthTestShutdown must not be called from a time-critical thread. So we do it on a worker thread.
	// It should be fine to start new check while this is ending.
	m_ShutdownWorkItem.Configure(nullptr, &m_TestCtx);

	if (!netThreadPool.QueueWork(&m_ShutdownWorkItem))
	{
		rlError("netThreadPool failed to queue ShutdownBandwidthTest WorkItem");
		AbortBandwidthTestBlocking();
	}
}

#if RSG_BANK
void
rlNpBandwidth::BANK_TestBandwidth()
{
    TestBandwidth(nullptr);
}

void
rlNpBandwidth::AddWidgets(bkBank *pBank)
{
    if (!pBank)
    {
        return;
    }

    pBank->PushGroup("Bandwidth");
    pBank->AddButton("Test Bandwidth", datCallback(MFA(rlNpBandwidth::BANK_TestBandwidth), (datBase*)this));
    pBank->AddText("Download Bps", &m_Result.m_DownloadBps, true);
    pBank->AddText("Upload Bps", &m_Result.m_UploadBps, true);
    pBank->PopGroup();
}
#endif //RSG_BANK

} // namespace rage

#endif // RL_NP_BANDWIDTH
