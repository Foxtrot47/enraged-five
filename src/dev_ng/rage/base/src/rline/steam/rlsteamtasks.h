// 
// rline/rlsteamtasks.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_STEAMTASKS_H
#define RLINE_STEAMTASKS_H

#include "file/file_config.h"

#if RSG_PC
#if __STEAM_BUILD
#include "atl/string.h"
#include "net/http.h"
#include "net/status.h"
#include "net/task.h"
#include "rline/rlhttptask.h"
#include "rline/ros/rlroshttptask.h"
#include "rline/socialclub/rlsocialclubcommon.h"
#include "../../../../suite/src/rgsc/rgsc/rgsc/public_interface/rgsc_common.h"
#include "rlsteam.h"

#pragma warning(disable: 4265)
#include "../../3rdParty/Steam/public/steam/steam_api.h"
#pragma warning(error: 4265)
// 20 seconds, in milliseconds
#define STEAM_TIMEOUT_VALUE 20000

#define ERR_STEAM_TIMEOUT	0x6DEEEF01
#define ERR_STEAM_OFFLINE	0xF5CF7537
#define ERR_STEAM_NOTOWNED	0x5DCB1AB7 

namespace rage
{
	RAGE_DECLARE_SUBCHANNEL(rline, steamtasks)

	class rlSteamVerifyOwnershipTask : public netTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_steamtasks);
		NET_TASK_DECL(rlSteamVerifyOwnershipTask);

		rlSteamVerifyOwnershipTask();
		
		virtual bool Configure(const int localGamerIndex, rlSteam::SteamFailureType* failureType, int* failureCode);
		virtual netTaskStatus OnUpdate(int* resultCode);
		virtual void OnCancel();

	private:
#if STEAM_VERIFY_WITH_CALLBACKS
		virtual bool GetAuthSessionTicket(const int localGamerIndex, rlSteam::SteamFailureType* failureType, int* failureCode, netStatus* status);
		virtual bool BeginAuthSession(const int localGamerIndex, rlSteam::SteamFailureType* failureType, int* failureCode, netStatus* status);
#endif
		enum State
		{
#if STEAM_VERIFY_WITH_CALLBACKS			
			STATE_BEGIN_AUTH_SESSION,
			STATE_GET_AUTH_SESSION_TICKET,
			STATE_GETTING_AUTH_SESSION_TICKET,
			STATE_GETTING_BEGIN_AUTH_SESSION,
			STATE_OFFLINE_CHECK,
#endif
			STATE_INIT,
			STATE_SUCCEEDED,
			STATE_FAILED
		};
		// Store our local gamer index
		int m_localGamerIndex;
		// Localize the steam ID. I don't think this is horrendously necessary
		// but it facilitates
		CSteamID m_steamId;
#if STEAM_VERIFY_WITH_CALLBACKS
		// Authentication Session Ticket. Passed around via pointers
		u8 m_authSessionTicket[rgsc::RGSC_STEAM_TICKET_BUF_SIZE];
		// Length of above parameter
		uint32 m_authSessionTicketLength;
		// Failure type
		rlSteam::SteamFailureType* m_failureType;
		// Failure Code
		int* m_failureCode;

		// Retry for a cancelled ticket
		bool m_Retry;
#endif

		State m_State;
		netStatus m_MyStatus;
	};
#if STEAM_VERIFY_WITH_CALLBACKS
	class rlSteamGetAuthSessionTicketTask : public netTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_steamtasks);
		NET_TASK_DECL(rlSteamGetAuthSessionTicketTask);

		rlSteamGetAuthSessionTicketTask();

		virtual bool Configure(const int localGamerIndex, u8* authSessionTicket, uint32* authSessionTicketLength, rlSteam::SteamFailureType* failureType, int* failureCode);
		virtual netTaskStatus OnUpdate(int* resultCode);
		virtual void OnCancel();

	private:
		enum State
		{
			STATE_REQUEST,
			STATE_PENDING_RESPONSE,
			STATE_RECIEVED_RESPONSE,
			STATE_FAILED,
			STATE_SUCCEEDED
		};

		// HAuthTicket
		HAuthTicket m_hAuthTicket;
		// Ticket from parent task, provided by pointer
		u8* m_authSessionTicket;
		// Length of above parameter
		uint32* m_authSessionTicketLength;
		// Failure type
		rlSteam::SteamFailureType* m_failureType;
		// Failure Code
		int* m_failureCode;
		// Counter for timeout
		unsigned m_timer;
		State m_State;
		netStatus m_MyStatus;
	};


	class rlSteamBeginAuthSessionTask : public netTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_steamtasks);
		NET_TASK_DECL(rlSteamBeginAuthSessionTask);

		rlSteamBeginAuthSessionTask();

		virtual bool Configure(const int localGamerIndex, u8* authSessionTicket, int* authSessionTicketLength, rlSteam::SteamFailureType* failureType, int* failureCode);
		virtual netTaskStatus OnUpdate(int* resultCode);
		virtual void OnCancel();

	private:
		enum State
		{
			STATE_REQUEST,
			STATE_PENDING_RESPONSE,
			STATE_RECIEVED_RESPONSE,
			STATE_FAILED,
			STATE_SUCCEEDED
		};

		// I really want to make this a uint32 for consistency, but the function to
		// steam specifically calls for an integer, and I'm not really the 
		// biggest fan of casting.

		// Length of Ticket
		int* m_authSessionTicketLength;
		// Session ticket
		u8* m_authSessionTicket;
		// Timer for timeouts
		unsigned m_timer;
#endif
		// Failure type
		rlSteam::SteamFailureType* m_failureType;
		// Failure Code
		int* m_failureCode;
#if STEAM_VERIFY_WITH_CALLBACKS
		State m_State;

		netStatus m_MyStatus;
	};
#endif

}

#endif // __STEAM_BUILD
#endif // RSG_PC
#endif // RLINE_STEAMTASKS_H
