// 
// rline/rlysteamtasks.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// Rage Includes
#include "data/rson.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "rline/rldiag.h"
#include "rline/rlsystemui.h"
#include "rline/ros/rlros.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/rltitleid.h"
#include "system/timer.h"
#include "rline/steam/rlsteam.h"
#include "rlsteamtasks.h"

#if RSG_BANK
#include "bank/bkmgr.h"
#endif

#if RSG_PC
#if __STEAM_BUILD
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(error: 4668)

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, steamtasks);
#undef __rage_channel
#define __rage_channel rline_steamtasks

extern sysMemAllocator* g_rlAllocator;

#define ERROR_CASE(code, codeEx, resCode) \
	if(result.IsError(code, codeEx)) \
{ \
	resultCode = resCode; \
	return; \
}

#define WARNING_CASE(code, codeEx) \
	if(result.IsError(code, codeEx)) \
{ \
	resultCode = 0; \
	return; \
}

#if STEAM_VERIFY_WITH_CALLBACKS
// Creating static members of our Steam Callbacks
static GetAuthSessionTicketCallback		sm_authSessionTicketCallback;
static BeginAuthSessionCallback			sm_authSessionCallback;
#endif


//////////////////////////////////////////////////////
// rlSteamVerifyOwnershipTask
//////////////////////////////////////////////////////
rlSteamVerifyOwnershipTask::rlSteamVerifyOwnershipTask()
	: m_State(STATE_INIT)
#if STEAM_VERIFY_WITH_CALLBACKS
	, m_Retry(true)
#endif
{

}

bool rlSteamVerifyOwnershipTask::Configure(const int localGamerIndex, rlSteam::SteamFailureType* failureType, int* failureCode)
{
	rtry
	{
		// Fail of no failure type address
		rverify(failureType, catchall, netTaskError("No failure type."));
		// Set failure type
		m_failureType = failureType;
		// Fail of no failure codeaddress
		rverify(failureCode, catchall, netTaskError("No failure code."));
		// Set failure code
		m_failureCode = failureCode;

		// Fail if no steam user found
		rverify(SteamUser(), catchall, netTaskError("SteamUser() failed"));
		// Fetch the common SteamID and reset the memory, accordingly
		m_steamId = SteamUser()->GetSteamID();
		// Set the memory of the ticket

#if STEAM_VERIFY_WITH_CALLBACKS 
		sysMemSet(m_authSessionTicket, 0, sizeof(m_authSessionTicket));
		// Set the length of the ticket to 0
		m_authSessionTicketLength = 0;
#endif
		// Check the Gamer Index
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		m_localGamerIndex = localGamerIndex;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlSteamVerifyOwnershipTask::OnCancel()
{
	// m_MyStatus monitors rlSteamVerifyOwnershipTask which is a netTask
	if (m_MyStatus.Pending())
	{
		netTask::Cancel(&m_MyStatus);
	}
}



netTaskStatus rlSteamVerifyOwnershipTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	
	switch(m_State)
	{
#if STEAM_VERIFY_WITH_CALLBACKS
	case STATE_INIT:
		// Determine whether we're doing an online or an off-line check
		if(SteamUser()->BLoggedOn())
		{
			// First indicate that we want to call GetAuthSessionTicket
			m_State = STATE_GET_AUTH_SESSION_TICKET;
		}
		else
		{
			m_State = STATE_OFFLINE_CHECK;
		}
		break;
	case STATE_OFFLINE_CHECK:
		{
			// In the event that we're offline, first lets determine if we're subscribed to the general content
			if(!SteamApps()->BIsSubscribed() || !SteamApps()->BIsSubscribedApp(g_rlTitleId->m_RosTitleId.GetSteamAppId()))
			{
				*m_failureType = rlSteam::SteamFailureType::STEAM_FAILURE_OFFLINE;
				*m_failureCode = ERR_STEAM_OFFLINE;
				netTaskError("Steam is Offline and BIsSubscribed returned false");
				m_State = STATE_FAILED;
			}
			else
			{
				m_State = STATE_SUCCEEDED;
			}
		}
		break;
	case STATE_GET_AUTH_SESSION_TICKET:
		// Make the transition call to start the task to Steam's
		// API of GetAuthSessionTicket
		GetAuthSessionTicket(m_localGamerIndex, m_failureType, m_failureCode, &m_MyStatus);
		m_State = STATE_GETTING_AUTH_SESSION_TICKET;
		break;
	case STATE_GETTING_AUTH_SESSION_TICKET:
		// Wait for the response to come back. 
		// I have to check against .None() because this frame
		// was executing faster than the status was being set 
		// to pending. Seems weird, but nto something I'm worrying
		// myself too much about
		if(!m_MyStatus.Pending() && !m_MyStatus.None())
		{
			// Check for success
			if(m_MyStatus.Succeeded())
			{
				// Continue on to verify the session itself
				m_State = STATE_BEGIN_AUTH_SESSION;
			}
			else
			{
				netTaskError("Failed to retrieve Steam Authorization Session Ticket.");
				m_State = STATE_FAILED;
			}
		}
		break;
	case STATE_BEGIN_AUTH_SESSION:
		// Make the transition call to the task hat calls
		// Steam's BeginAuthSession function
		BeginAuthSession(m_localGamerIndex,m_failureType, m_failureCode, &m_MyStatus);
		m_State = STATE_GETTING_BEGIN_AUTH_SESSION;
		break;
	case STATE_GETTING_BEGIN_AUTH_SESSION:
		// Similar to above
		if(!m_MyStatus.Pending() && !m_MyStatus.None())
		{
			if(m_MyStatus.Succeeded())
			{
				// Hooray!
				m_State = STATE_SUCCEEDED;
			}
			else
			{
				if (m_Retry)
				{
					netTaskDebug("Auth Session failed, retrying once.");
					m_State = STATE_GET_AUTH_SESSION_TICKET;
					m_Retry = false;
				}
				else
				{
					// Womp womp
					netTaskError("Failed to authorize Steam Content.");
					m_State = STATE_FAILED;
				}
			}
		}
		break;
#else
	case STATE_INIT:
		{
			// In the event that we're offline, first lets determine if we're subscribed to the general content
			if(!SteamApps()->BIsSubscribed() || !SteamApps()->BIsSubscribedApp(g_rlTitleId->m_RosTitleId.GetSteamAppId()))
			{
				*m_failureType = rlSteam::SteamFailureType::STEAM_FAILURE_NOT_SUBSCRIBED;
				*m_failureCode = ERR_STEAM_NOTOWNED;
				netTaskError("User is not authorized to play the game on Steam.");
				m_State = STATE_FAILED;
			}
			else
			{
				m_State = STATE_SUCCEEDED;
			}
		}
		break;
#endif
	case STATE_SUCCEEDED:
		{
			status = NET_TASKSTATUS_SUCCEEDED;
			break;
		}	
	case STATE_FAILED:
		{
			status = NET_TASKSTATUS_FAILED;
			break;
		}
	}

	return status;
}

// Transparent function that creates  tasks and calls it
#if STEAM_VERIFY_WITH_CALLBACKS
bool rlSteamVerifyOwnershipTask::GetAuthSessionTicket(const int localGamerIndex, rlSteam::SteamFailureType* failureType, int* failureCode, netStatus* status)
{
	rlSteamGetAuthSessionTicketTask * task = NULL;

	rtry												
	{	
		rverify(netTask::Create(&task, status), catchall, );																								
		rverify(task->Configure(localGamerIndex, m_authSessionTicket, &m_authSessionTicketLength, failureType, failureCode), catchall, );	
		rverify(netTask::Run(task), catchall, );			
		return true;										
	}													
	rcatchall											
	{													
		if(task != NULL)									
		{													
			netTask::Destroy(task);								
		}		
		return false;										
	}

}

bool rlSteamVerifyOwnershipTask::BeginAuthSession(const int localGamerIndex, rlSteam::SteamFailureType* failureType, int* failureCode, netStatus* status)
{
	rlSteamBeginAuthSessionTask* task = NULL;

	rtry												
	{										
		rverify(netTask::Create(&task, status), catchall, );			
		rverify(task->Configure(localGamerIndex, m_authSessionTicket, (int*)&m_authSessionTicketLength, failureType, failureCode), catchall, );	
		rverify(netTask::Run(task), catchall, );			
		return true;										
	}													
	rcatchall											
	{													
		if(task != NULL)									
		{													
			netTask::Destroy(task);								
		}		
		return false;										
	}

}


//////////////////////////////////////////////////////
// rlSteamGetAuthSessionTicketTask
//////////////////////////////////////////////////////
rlSteamGetAuthSessionTicketTask::rlSteamGetAuthSessionTicketTask()
	: m_State(STATE_REQUEST)
{

}

bool rlSteamGetAuthSessionTicketTask::Configure(const int localGamerIndex, u8* authSessionTicket, uint32* authSessionTicketLength, rlSteam::SteamFailureType* failureType, int* failureCode)
{
	rtry
	{
		// Fail of no failure type address
		rverify(failureType, catchall, netTaskError("No failure type."));
		// Set failure type
		m_failureType = failureType;
		// Fail of no failure code address
		rverify(failureCode, catchall, netTaskError("No failure code."));
		// Set failure code
		m_failureCode = failureCode;


		// rlVerify that our ticket is a valid address
		rverify(authSessionTicket, catchall, netTaskError("No auth session token."))
		// Set the pointer correctly - I decided to just pass around
		// a memory pointer so I don't need to worry about allocating / cleaning
		// memory
		m_authSessionTicket = authSessionTicket;
		rverify(authSessionTicketLength, catchall,)
		// Set the length
		m_authSessionTicketLength = authSessionTicketLength;
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlSteamGetAuthSessionTicketTask::OnCancel()
{
	if (m_MyStatus.Pending())
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus rlSteamGetAuthSessionTicketTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;
	
	switch(m_State)
	{
	case STATE_REQUEST:
		{
			// Issue the request to Valve
			m_hAuthTicket = SteamUser()->GetAuthSessionTicket(m_authSessionTicket, rgsc::RGSC_STEAM_TICKET_BUF_SIZE, m_authSessionTicketLength);
			// Set our initial timer
			m_timer = sysTimer::GetSystemMsTime();
			// Indicate that we're going to wait for the response
			m_State = STATE_PENDING_RESPONSE;
			break;
		}
	case STATE_PENDING_RESPONSE:
		{
			// Now that we're waiting for the response, we need to grind
			// until our call back indicates that it's gotten a valid result
			// The interesting thing here is that this callback actually fires
			// well before one would expect, on the first iteration of 
			// SteamAPI_RunCallbacks() in a much earlier setup call
			// So by the time it gets here, it's already called this callback
			// and those values have been set already
			// So why still have it? In case that doesn't happen, I need to handle
			// myself gracefully
			if(!sm_authSessionTicketCallback.IsPending())
			{
				m_State = STATE_RECIEVED_RESPONSE;
			}
			else
			{
				// We should fail out if we don't get a response in 20s
				if(sysTimer::HasElapsedIntervalMs(m_timer, STEAM_TIMEOUT_VALUE))
				{
					*m_failureCode = ERR_STEAM_TIMEOUT;
					*m_failureType = rlSteam::STEAM_FAILURE_GET_AUTH_SESSION;
					m_State = STATE_FAILED;
					break;
				}
			}
			break;
		}
	case STATE_RECIEVED_RESPONSE:
		{
			netTaskDebug("GetAuthSessionTicket Response: %s(%d)", sm_authSessionTicketCallback.GetResultStr(), sm_authSessionTicketCallback.GetResult());

			// We've gotten a response back, check for validity
			if( sm_authSessionTicketCallback.GetResult() == EResult::k_EResultOK)
			{
				m_State = STATE_SUCCEEDED;
			}
			else
			{
				*m_failureCode = sm_authSessionTicketCallback.GetResult();
				*m_failureType = rlSteam::STEAM_FAILURE_GET_AUTH_SESSION;
				m_State = STATE_FAILED;
			}
			break;

		}
	case STATE_SUCCEEDED:
		{
			status = NET_TASKSTATUS_SUCCEEDED;
			break;
		}
	case STATE_FAILED:
		{
			status = NET_TASKSTATUS_FAILED;
			break;
		}

	}
	return status;
}


//////////////////////////////////////////////////////
// rlSteamBeginAuthSessionTask
//////////////////////////////////////////////////////
rlSteamBeginAuthSessionTask::rlSteamBeginAuthSessionTask()
	: m_State(STATE_REQUEST)
{

}

bool rlSteamBeginAuthSessionTask::Configure(const int localGamerIndex, u8* authSessionTicket, int* authSessionTicketLength, rlSteam::SteamFailureType* failureType, int* failureCode)
{
	rtry
	{
		// Fail of no failure type address
		rverify(failureType, catchall, netTaskError("NULL failureType."));
		// Set failure type
		m_failureType = failureType;
		// Fail of no failure code address
		rverify(failureCode, catchall, netTaskError("NULL failureCode."));
		// Set failure code
		m_failureCode = failureCode;

		// rlVerify that the ticket is at a valid address
		rverify(authSessionTicket, catchall, netTaskError("NULL authSessionTicket."))
		m_authSessionTicket = authSessionTicket;
		// Assign and handle the length appropriately
		rverify(authSessionTicketLength, catchall,)
		m_authSessionTicketLength = authSessionTicketLength;
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		return true;
	}
	rcatchall
	{
		netTaskError("Configure failed.");
		return false;
	}
}

void rlSteamBeginAuthSessionTask::OnCancel()
{
	if (m_MyStatus.Pending())
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus rlSteamBeginAuthSessionTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_REQUEST:
		{
			// Create the request to BeginAuthSession
			EBeginAuthSessionResult authSessionResult = SteamUser()->BeginAuthSession(m_authSessionTicket, *m_authSessionTicketLength, SteamUser()->GetSteamID());
			netTaskDebug3("BeginAuthSession Result: %s (%d)",  BeginAuthSessionCallback::BeginResultToStr(authSessionResult), authSessionResult);

			// Check the immediate result
			if(authSessionResult != k_EBeginAuthSessionResultOK)
			{
				netTaskError("Failed: STATE_REQUEST");
				m_State = STATE_FAILED;
				*m_failureCode = authSessionResult;
				*m_failureType = rlSteam::STEAM_FAILURE_BEGIN_AUTH_SESSION_RESULT;
			}
			else
			{
				m_timer = sysTimer::GetSystemMsTime();
				m_State = STATE_PENDING_RESPONSE;
			}
			break;

		}
	case STATE_PENDING_RESPONSE:
		{
			// Now we're waiting for a response from the callback
			if(!sm_authSessionCallback.IsPending())
			{
				m_State = STATE_RECIEVED_RESPONSE;
			}
			else
			{
				// We should fail out if we don't get a response in 20s
				if(sysTimer::HasElapsedIntervalMs(m_timer, STEAM_TIMEOUT_VALUE))
				{
					m_State = STATE_FAILED;
					*m_failureCode = ERR_STEAM_TIMEOUT;
					*m_failureType = rlSteam::STEAM_FAILURE_BEGIN_AUTH_SESSION_RESPONSE;
					netTaskError("STATE_PENDING_RESPONSE timeout");
					break;
				}
			}

			break;
		}
	case STATE_RECIEVED_RESPONSE:
		{
			netTaskDebug3("BeginAuthSession Response: %s (%d)", sm_authSessionCallback.GetResultStr(), sm_authSessionCallback.GetResult());

			// Now that we've received a response, lets check the result
			if(sm_authSessionCallback.GetResult() == EAuthSessionResponse::k_EAuthSessionResponseOK)
			{
				m_State = STATE_SUCCEEDED;
			}
			else
			{
				netTaskError("Failed: STEAM_FAILURE_BEGIN_AUTH_SESSION_RESPONSE");
				*m_failureCode = sm_authSessionCallback.GetResult();
				*m_failureType = rlSteam::STEAM_FAILURE_BEGIN_AUTH_SESSION_RESPONSE;
				m_State = STATE_FAILED;
			}
			break;
		}
	case STATE_FAILED:
		{
			status = NET_TASKSTATUS_FAILED;
			break;
		}
	case STATE_SUCCEEDED:
		{
			status = NET_TASKSTATUS_SUCCEEDED;
			break;
		}
	}

	return status;
}
#endif

}   //namespace rage

#endif // __STEAM_BUILD
#endif // RSG_PC
