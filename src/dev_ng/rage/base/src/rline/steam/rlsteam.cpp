// 
// rline/rlyoutube.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// Youtube Includes
#include "rlsteam.h"
#include "rlsteamtasks.h"

#if RSG_PC
#if __STEAM_BUILD
// RAGE Includes
#include "diag/seh.h"
#include "rline/rlsystemui.h"
#include "rline/rltitleid.h"
#include "rline/ros/rlros.h"
#include "system/memory.h"

namespace rage
{
	extern const rlTitleId* g_rlTitleId;

	//////////////////////////////////////////////////////////////
	// rlSteam
	//////////////////////////////////////////////////////////////
	bool rlSteam::VerifyOwnership(const int localGamerIndex, netStatus* status, rlSteam::SteamFailureType *failureType, int *failureCode)
	{
		rlSteamVerifyOwnershipTask* task = NULL;

		rtry												
		{										
			rverify(netTask::Create(&task, status), catchall, );																								
			rverify(task->Configure(localGamerIndex, failureType, failureCode), catchall, );	
			rverify(netTask::Run(task), catchall, );			
			return true;										
		}													
		rcatchall											
		{													
			if(task != NULL)									
			{													
				netTask::Destroy(task);								
			}		
			return false;										
		}
	}

#if STEAM_VERIFY_WITH_CALLBACKS
	/************************************************************************/
	/* 
	When creating a ticket for use by the AuthenticateUserTicket Web API, the calling
	application should wait for the callback GetAuthSessionTicketResponse_t generated
	by the API call before attempting to use the ticket to ensure that the ticket has
	been communicated to the server. If this callback does not come in a timely 
	fashion ( 10 - 20 seconds ), your client is not connected to Steam, and the
	AuthenticeUserTicket will fail because it can not authenticate the user.
	*/
	/************************************************************************/
	void GetAuthSessionTicketCallback::OnAuthSessionTicketResponse( GetAuthSessionTicketResponse_t *pParam )
	{
		if(pParam == NULL)
		{
			m_result = EResult::k_EResultBadResponse;
			m_pending = false;
		}
		else
		{
			m_result = pParam->m_eResult;
			m_pending = false;
			Displayf("GetAuthSessionTicketCallback::OnAuthSessionTicketResponse : %s (%d)", ResultToStr(m_result), m_result);
		}
	}

	// This function gets called whenever we get a response from the validation of the actual ticket itself
	// This will tell us if we're the owner of the game or not. Also,  As of now, I'm under the operating
	// assumption that we're not going to support sharing / borrowing of the game
	void BeginAuthSessionCallback::OnAuthSessionResponse( ValidateAuthTicketResponse_t *pParam )
	{
		if(pParam == NULL)
		{
			m_result = EAuthSessionResponse::k_EAuthSessionResponseAuthTicketInvalid;
		}
		else
		{
			m_result = pParam->m_eAuthSessionResponse;
			m_pending = false;
		}

		rlDebug("BeginAuthSessionCallback::OnAuthSessionResponse: %s %d", ResultToStr(m_result), m_result);
	}
#endif

#if !__NO_OUTPUT && STEAM_VERIFY_WITH_CALLBACKS
	
	#define ResToStr(x) case x: return #x; break;

	const char * GetAuthSessionTicketCallback::ResultToStr(EResult result)
	{
		switch (result)
		{
			ResToStr(k_EResultOK);					
			ResToStr(k_EResultFail);
			ResToStr(k_EResultNoConnection);
			ResToStr(k_EResultInvalidPassword);
			ResToStr(k_EResultLoggedInElsewhere);
			ResToStr(k_EResultInvalidProtocolVer);
			ResToStr(k_EResultInvalidParam);
			ResToStr(k_EResultFileNotFound);
			ResToStr(k_EResultBusy);
			ResToStr(k_EResultInvalidState);
			ResToStr(k_EResultInvalidName);
			ResToStr(k_EResultInvalidEmail);
			ResToStr(k_EResultDuplicateName);
			ResToStr(k_EResultAccessDenied);
			ResToStr(k_EResultTimeout);
			ResToStr(k_EResultBanned);
			ResToStr(k_EResultAccountNotFound);
			ResToStr(k_EResultInvalidSteamID);
			ResToStr(k_EResultServiceUnavailable);
			ResToStr(k_EResultNotLoggedOn);
			ResToStr(k_EResultPending);
			ResToStr(k_EResultEncryptionFailure);
			ResToStr(k_EResultInsufficientPrivilege);
			ResToStr(k_EResultLimitExceeded);
			ResToStr(k_EResultRevoked);
			ResToStr(k_EResultExpired);
			ResToStr(k_EResultAlreadyRedeemed);
			ResToStr(k_EResultDuplicateRequest);
			ResToStr(k_EResultAlreadyOwned);
			ResToStr(k_EResultIPNotFound);
			ResToStr(k_EResultPersistFailed);
			ResToStr(k_EResultLockingFailed);
			ResToStr(k_EResultLogonSessionReplaced);
			ResToStr(k_EResultConnectFailed);
			ResToStr(k_EResultHandshakeFailed);
			ResToStr(k_EResultIOFailure);
			ResToStr(k_EResultRemoteDisconnect);
			ResToStr(k_EResultShoppingCartNotFound);
			ResToStr(k_EResultBlocked);
			ResToStr(k_EResultIgnored);
			ResToStr(k_EResultNoMatch);
			ResToStr(k_EResultAccountDisabled);
			ResToStr(k_EResultServiceReadOnly);
			ResToStr(k_EResultAccountNotFeatured);
			ResToStr(k_EResultAdministratorOK);
			ResToStr(k_EResultContentVersion);
			ResToStr(k_EResultTryAnotherCM);
			ResToStr(k_EResultPasswordRequiredToKickSession);
			ResToStr(k_EResultAlreadyLoggedInElsewhere);
			ResToStr(k_EResultSuspended);
			ResToStr(k_EResultCancelled);
			ResToStr(k_EResultDataCorruption);
			ResToStr(k_EResultDiskFull);
			ResToStr(k_EResultRemoteCallFailed);
			ResToStr(k_EResultPasswordUnset);
			ResToStr(k_EResultExternalAccountUnlinked);
			ResToStr(k_EResultPSNTicketInvalid);
			ResToStr(k_EResultExternalAccountAlreadyLinked);
			ResToStr(k_EResultRemoteFileConflict);
			ResToStr(k_EResultIllegalPassword);
			ResToStr(k_EResultSameAsPreviousValue);
			ResToStr(k_EResultAccountLogonDenied);
			ResToStr(k_EResultCannotUseOldPassword);
			ResToStr(k_EResultInvalidLoginAuthCode);
			ResToStr(k_EResultAccountLogonDeniedNoMail);
			ResToStr(k_EResultHardwareNotCapableOfIPT);
			ResToStr(k_EResultIPTInitError);
			ResToStr(k_EResultParentalControlRestricted);
			ResToStr(k_EResultFacebookQueryError);
			ResToStr(k_EResultExpiredLoginAuthCode);
			ResToStr(k_EResultIPLoginRestrictionFailed);
			ResToStr(k_EResultAccountLockedDown);
			ResToStr(k_EResultAccountLogonDeniedVerifiedEmailRequired);
			ResToStr(k_EResultNoMatchingURL);
			ResToStr(k_EResultBadResponse);
			ResToStr(k_EResultRequirePasswordReEntry);
			ResToStr(k_EResultValueOutOfRange);
			ResToStr(k_EResultUnexpectedError);
			ResToStr(k_EResultDisabled);
			ResToStr(k_EResultInvalidCEGSubmission);
			ResToStr(k_EResultRestrictedDevice);
			ResToStr(k_EResultRegionLocked);
		}

		return "unknown";
	}

	const char * BeginAuthSessionCallback::ResultToStr(EAuthSessionResponse result)
	{
		switch (result)
		{
			ResToStr(k_EAuthSessionResponseOK);
			ResToStr(k_EAuthSessionResponseUserNotConnectedToSteam);
			ResToStr(k_EAuthSessionResponseNoLicenseOrExpired);
			ResToStr(k_EAuthSessionResponseVACBanned);
			ResToStr(k_EAuthSessionResponseLoggedInElseWhere);
			ResToStr(k_EAuthSessionResponseVACCheckTimedOut);
			ResToStr(k_EAuthSessionResponseAuthTicketCanceled);
			ResToStr(k_EAuthSessionResponseAuthTicketInvalidAlreadyUsed);
			ResToStr(k_EAuthSessionResponseAuthTicketInvalid);
		}

		return "unknown";
	}


	const char* BeginAuthSessionCallback::BeginResultToStr(EBeginAuthSessionResult result)
	{
		switch (result)
		{
			ResToStr(k_EBeginAuthSessionResultOK);	
			ResToStr(k_EBeginAuthSessionResultInvalidTicket);
			ResToStr(k_EBeginAuthSessionResultDuplicateRequest);
			ResToStr(k_EBeginAuthSessionResultInvalidVersion);
			ResToStr(k_EBeginAuthSessionResultGameMismatch);
			ResToStr(k_EBeginAuthSessionResultExpiredTicket);
		}

		return "unknown";
	}

	#undef ResToStr

#endif

}   //namespace rage
#endif // RSG_PC
#endif // __STEAM_BUILD

