// 
// rline/rlyoutube.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_STEAM_H
#define RLINE_STEAM_H

#include "file/file_config.h"

#if RSG_PC
#if __STEAM_BUILD
#include "atl/string.h"
#include "data/growbuffer.h"
#include "data/rson.h"
#include "net/status.h"
#include "system/memory.h"

#define STEAM_VERIFY_WITH_CALLBACKS 0
#pragma warning(disable: 4265)
#include "../../3rdParty/Steam/public/steam/steam_api.h"
#include "../../3rdParty/Steam/public/steam/ISteamGameServer.h"
#include "../../3rdParty/Steam/public/steam/steam_gameserver.h"
#pragma warning(error: 4265)
#pragma comment(lib, "steam_api64.lib")
#include "data/certificateverify.h"
#include "diag/seh.h"

namespace rage
{


	class rlSteam
	{

	public:

		enum SteamFailureType 
		{
			STEAM_FAILURE_NONE,
#if STEAM_VERIFY_WITH_CALLBACKS
			STEAM_FAILURE_GET_AUTH_SESSION,
			STEAM_FAILURE_BEGIN_AUTH_SESSION_RESULT,
			STEAM_FAILURE_BEGIN_AUTH_SESSION_RESPONSE,
			STEAM_FAILURE_OFFLINE
#else
			STEAM_FAILURE_NOT_SUBSCRIBED
#endif
		};

		// Simple static call to kick off the verification of ownership
		static bool VerifyOwnership(const int localGamerIndex, netStatus* status, rlSteam::SteamFailureType *failureType, int *failureCode);
		
	};

/**
First thing we want to do here is declare the classes that will act as the callbacks for functions
*/
class GetAuthSessionTicketCallback
{
	// This is how to establish a steam-call back when their API's are accessed
	// Utilising the Macro STEAM_CALLBACK
	//		@parm	AuthSessionTicketCallback		Name of the class to access the callback from
	//		@parm	OnValidateAuthTicketResponse	Function to call
	//		@parm	GetAuthSessionTicketResponse_t	Parameter to pass to the function
	//		@parm	m_response						Name of variable that stores CCallback information
	STEAM_CALLBACK( GetAuthSessionTicketCallback, OnAuthSessionTicketResponse, GetAuthSessionTicketResponse_t , m_response);
	bool m_pending;
	EResult m_result; 
public:
	// Creating the constructor - but since I don't have any fancy parameters, there isn't much to it.
	// Default pending as to true - since it'll only be called once as a static member, we can set it to false
	// Once we get a response back from the server, and idle until then
	GetAuthSessionTicketCallback()
		:m_response(this, &GetAuthSessionTicketCallback::OnAuthSessionTicketResponse)
		, m_pending(true) 
		, m_result(k_EResultOK)
	{
	}
	bool IsPending(){return m_pending;}
	EResult GetResult(){return m_result;}

#if !__NO_OUTPUT
	const char* GetResultStr() { return ResultToStr(m_result); }
	static const char* ResultToStr(EResult result);
#endif
};

class BeginAuthSessionCallback
{
	// See above GetAuthSessionTicketCallback for similar description
	STEAM_CALLBACK( BeginAuthSessionCallback, OnAuthSessionResponse, ValidateAuthTicketResponse_t, m_response);
	bool m_pending;
	EAuthSessionResponse m_result;
public:
	BeginAuthSessionCallback()
		: m_response(this, &BeginAuthSessionCallback::OnAuthSessionResponse)
		, m_pending(true)
		, m_result(k_EAuthSessionResponseOK)
	{
	}
	bool IsPending(){return m_pending;}
	EAuthSessionResponse GetResult(){return m_result;}
#if !__NO_OUTPUT
	const char* GetResultStr() { return ResultToStr(m_result); }
	static const char* ResultToStr(EAuthSessionResponse result);
	static const char* BeginResultToStr(EBeginAuthSessionResult result);
#endif
};


}

#endif // __STEAM_BUILD

#endif // RSG_PC
#endif // RLINE_STEAM_H
