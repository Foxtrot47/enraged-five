// 
// rline/rlHttpTask.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLHTTPTASK_H
#define RLINE_RLHTTPTASK_H

#include "float.h"
#include "rltask.h"
#include "net/http.h"
#include "net/status.h"

namespace rage
{

class parTreeNode;
class datSaxReader;

RAGE_DECLARE_SUBCHANNEL(rline, http);

//PURPOSE
//  Helper functions useful for rlHttpTasks, especially when processing responses.
class rlHttpTaskHelper
{
public:
    //PURPOSE
    //  Helpers for getting the value of an element or attribute.  Given a node, you can return
    //  the value of data, the data of a child element, or an attribute of either the node or
    //  a child element.
    //PARAMS
    //  node        - Node to either get data from, or parent of child element to get data from.
    //  element     - (Optional) Name of child element.  If null, data is retrieved from node instead.
    //  attribute   - (Optional) Name of attribute.  If null, data is retrieved from node data.
    //RETURNS
    //  True or nonnull if value was successfully read.
    static const char* ReadString(const parTreeNode* node, const char* element, const char* attribute);
    static bool ReadBool(bool &val, const parTreeNode* node, const char* element, const char* attribute);
    static bool ReadFloat(float &val, const parTreeNode* node, const char* element, const char* attribute);
    static bool ReadInt(int &val, const parTreeNode* node, const char* element, const char* attribute);
    static bool ReadInt64(s64 &val, const parTreeNode* node, const char* element, const char* attribute);
    static bool ReadUInt64(u64 &val, const parTreeNode* node, const char* element, const char* attribute);
    static bool ReadUns(unsigned &val, const parTreeNode* node, const char* element, const char* attribute);
    
    static bool ReadBinaryLen(const parTreeNode* node, const char* element, const char* attribute, unsigned* len);
    static bool ReadBinary(const parTreeNode* node, const char* element, const char* attribute, u8* buf, const unsigned bufMaxLen, unsigned* bufLen);
};

class rlHttpTaskConfig
{
public:
    rlHttpTaskConfig()
        : m_Filter(NULL)
        , m_SaxReader(NULL)
        , m_HttpHeaders(NULL)
		, m_AddDefaultContentTypeHeader(true)
		, m_HttpVerb(NET_HTTP_VERB_POST)
		, m_ResponseDevice(NULL)
		, m_ResponseHandle(fiHandleInvalid)
		, m_IgnoreProxy(false)
    {
    }

    //If non-null outbound and inbound content will be passed
    //through the filter .
    netHttpFilter* m_Filter;

	// A device and handle to write the response to.
	fiDevice* m_ResponseDevice;
	fiHandle m_ResponseHandle;

    //If non-null the SAX parser wil be used to process inbound XML streams.
    //ProcessResponse() *will not* be called.  It is the sub-class's
    //responsibility to call Finish() when parsing is complete.
    datSaxReader* m_SaxReader;

    //If non-null the headers will be added to the HTTP request headers
    const char* m_HttpHeaders;

	//If true, the default content type will be added to the HTTP request headers
	bool m_AddDefaultContentTypeHeader;

	//The http verb to use (GET, POST, etc.)
	netHttpVerb m_HttpVerb;

	//If true, the HTTP request won't be sent to the proxy if one is set.
	bool m_IgnoreProxy;
};

//PURPOSE
//  Abstract base class for http tasks.
class rlHttpTask : public rlTaskBase
{
public:

	static const int MAX_MULTIPART_BOUNDARY_LENGTH = 32;
	static const int MAX_MULTIPART_BOUNDARY_BUF_SIZE = MAX_MULTIPART_BOUNDARY_LENGTH + 1;

    RL_TASK_DECL(rlHttpTask);
    RL_TASK_USE_CHANNEL(rline_http);

    rlHttpTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0);
    virtual ~rlHttpTask();

    bool Configure();

    bool Configure(netHttpFilter* filter);

    //PURPOSE
    //  Uses the SAX parser to process inbound XML streams.
    //  ProcessResponse() *will not* be called.  It is the sub-class's
    //  responsibility to call Finish() when parsing is complete.
    bool Configure(datSaxReader* saxReader, netHttpFilter* filter = NULL);

    //PURPOSE
    //  Wrap up all the possible parameters inside of rlHttpTaskConfig.
    //  FIXME(KB) Someday get rid of the other Configure() variants.
    bool Configure(const rlHttpTaskConfig* config);

    virtual bool IsCancelable() const {return true;}
    virtual void DoCancel();

    virtual void Start();
    virtual void Update(const unsigned timeStep);
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

protected:
    //Derived classes must implement these methods.

    //Return true to use HTTPS, false to use HTTP
    virtual bool UseHttps() const = 0;

    //If UseHttps is true, must return the SSL_CTX to use for the ssl connection
    virtual SSL_CTX* GetSslCtx() const = 0;

    //Returns the host name part of the URL to be used.
    //E.g. http://{hostname}/path/to/resource?arg1=0&arg2=1
    virtual const char* GetUrlHostName(char* /*hostnameBuf*/, const unsigned /*sizeofBuf*/) const = 0;

    template<int SIZE>
    const char* GetUrlHostName(char (&hostnameBuf)[SIZE]) const
    {
        return GetUrlHostName(hostnameBuf, SIZE);
    }

    //Returns the path part of the URL to be used.
    //E.g. http://example.com/{path to resource}?arg1=0&arg2=1
	virtual bool GetServicePath(char* /*svcPath*/, const unsigned /*maxLen*/) const  = 0;

    virtual bool ProcessResponse(const char* response, int& resultCode) = 0;

    virtual const char* BuildUrl(char* url, const unsigned sizeofUrl) const;

    template<int SIZE>
    const char* BuildUrl(char (&url)[SIZE]) const
    {
        return BuildUrl(url, SIZE);
    }

    //Append text to the user agent string
    bool AppendUserAgentString(const char* userAgentString);

	// Helpers for multi-part form data
	//	If boundary is left null, the posix time will be used in the request
	bool StartMultipartFormData(const char* boundary = NULL);
	bool AddMultipartStringParameter(const char* name, const char* value, const unsigned minLen = 0, const unsigned maxLen = UINT_MAX);
	bool AddMultipartIntParameter(const char* name, const s64 value, const s64 minVal = (-s64(9223372036854775807)-1), const s64 maxVal = s64(9223372036854775807));
	bool AddMultipartUnsParameter(const char* name, const u64 value);
	bool AddMultipartDoubleParameter(const char* name, const double value);
	bool AddMultipartBinaryParameter(const char* name, const char* filename, const u8* data, const unsigned size);
	bool EndMultipartFormData();

    //Helpers for adding request parameters.
    bool AddStringParameter(const char* name, const char* value, const bool required, const unsigned minLen = 0, const unsigned maxLen = UINT_MAX);
    bool AddIntParameter(const char* name, const s64 value, const s64 minVal = (-s64(9223372036854775807)-1), const s64 maxVal = s64(9223372036854775807));
    bool AddUnsParameter(const char* name, const u64 value);
	bool AddDoubleParameter(const char* name, const double value, const double minVal = -1.7976931348623158e+308, const double maxVal = 1.7976931348623158e+308);
    bool AddBinaryParameter(const char* name, const void* data, const unsigned size);
    //URL-encodes the string and appends it to the end of the HTTP request.
    bool AppendContentUrlEncoded(const void* data, const unsigned len);
    //Appends raw data to the end of the HTTP request.
    bool AppendContent(const void* data, const unsigned len);

    //Adds a comma separated list of values.
    //Call BeginListParameter to begin the list, passing the name of the list.
    //For each value in the list call AddListParam*().
    //The list is ended when another named parameter is added (e.g. AddStringParameter())
    //or when the request is Committed.
    bool BeginListParameter(const char* name);
    bool AddListParamStringValue(const char* value);
    bool AddListParamIntValue(const s64 value, const s64 minVal = (-s64(9223372036854775807-1)), const s64 maxVal = s64(9223372036854775807));
    bool AddListParamUnsValue(const u64 value);
    bool AddListParamDoubleValue(const double value, const double minVal = -1.7976931348623158e+308, const double maxVal = 1.7976931348623158e+308);

	bool AddRequestHeaderValue(const char* name, const char* value);

	bool AddQueryParam(const char * name, const char * value, unsigned int length);

	u32 GetResponseContentLength() const { return m_HttpRequest.GetResponseContentLength(); }


protected:
	void Reset();

private:
    rlHttpTask(const rlHttpTask& rhs);
    rlHttpTask& operator=(const rlHttpTask& rhs);

    enum State
    {
        STATE_RECEIVING,
        STATE_SAX_PROCESSING_RESPONSE,
        STATE_SAX_DONE
    };

protected:
	bool SendRequest();

	netHttpRequest m_HttpRequest;

	char m_MultipartBoundary[MAX_MULTIPART_BOUNDARY_BUF_SIZE];

    //Buffer used to receive HTTP response
    datGrowBuffer m_GrowBuffer;

    datSaxReader* m_SaxReader;

    unsigned m_StartTime;
	unsigned m_TimeOutSecs;
    netStatus m_MyStatus;

    State m_State;

    //True if adding a list parameter
    bool m_AddingListParam  : 1;
	bool m_bHasResponseDevice : 1;
};

//PURPOSE
//  Download a file from an http server.
class rlHttpDownloadTask : public rlTaskBase
{
public:
    RL_TASK_DECL(rlHttpDownloadTask);
    RL_TASK_USE_CHANNEL(rline_http);

	typedef void (*ProgressCallback)(const unsigned int numBytesDownloaded, const unsigned int totalBytes, void* userData);

	// specifies what to do if the local path/filename already exists
	enum OverwriteOptions
	{
		RL_HTTP_OVERWRITE_NEVER,		// The file will not be re-downloaded if it already exists.
										// Fastest and uses no server bandwidth.
										// Local resource may differ from server version.

		RL_HTTP_OVERWRITE_ALWAYS,		// The file will be re-downloaded and overwritten.
										// Slowest and uses the most server bandwidth.
										// Local resource will be the same as the server version.

		// Not yet supported.
		//RL_HTTP_OVERWRITE_IF_CHANGED,	// The file will only be re-downloaded if the supplied entity
										// tag (ETag) differs from the one on the server.
										// Uses some server bandwidth (headers), but best method to
										// use for caching files that may be updated on the server.
										// Also requires the caller to store ETag string.
		RL_HTTP_RESUME
	};

    rlHttpDownloadTask();
    virtual ~rlHttpDownloadTask();

    //PURPOSE
    // Configures the download task.
    //PARAMS
    //  downloadUrl     - Location of file to download
    //  localPath       - Local path to store file
    //  overwrite       - Overwrite behavior
    //  callback        - Callback for progress events
    //  callbackData    - User data passed to callback
	bool Configure(const char* downloadUrl, 
                   const char* localPath, 
                   OverwriteOptions overwrite, 
                   rlHttpDownloadTask::ProgressCallback callback, 
                   void* callbackData);

    virtual bool IsCancelable() const {return true;}
    virtual void DoCancel();

    virtual void Start();
    virtual void Update(const unsigned timeStep);
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    //PURPOSE
    //  Helper that creates a download task and queues it in parallel
    //  on the global rlGetTaskManager().
    //PARAMS
    //  downloadUrl - URI of resource, including http://
    //  localPath - path to where the file should be downloaded on the local disk.
    //				This can be a full path including a file name (i.e. C:\Dir\file.txt)
    //				If the path ends in a '/' or '\' (i.e. C:\Dir\) then the filename
    //				will be the same as the remote filename.
    //	overwrite - specifies what to do if the local file already exists. See rlhttptask.h.
    //  status	  - poll for completion
    static bool DownloadFile(const char* downloadUrl,
                             const char* localPath, 
                             rlHttpDownloadTask::OverwriteOptions overwrite,
                             netStatus* status);

    //PURPOSE
    //  Helper that creates a download task and queues it in parallel
    //  on the global rlGetTaskManager().
    //PARAMS
    //  downloadUrl - URI of resource, including http://
    //  localPath - path to where the file should be downloaded on the local disk.
    //				This can be a full path including a file name (i.e. C:\Dir\file.txt)
    //				If the path ends in a '/' or '\' (i.e. C:\Dir\) then the filename
    //				will be the same as the remote filename.
    //	overwrite - specifies what to do if the local file already exists. See rlhttptask.h.
    //  progressCallback - callback to receive number of bytes downloaded and total number of bytes
    //  userData - this pointer will be passed to the callback unmodified
    //  status	  - poll for completion
    static bool DownloadFile(const char* downloadUrl,
                             const char* localPath,
                             rlHttpDownloadTask::OverwriteOptions overwrite,
                             rlHttpDownloadTask::ProgressCallback progressCallback,
                             void* callbackData,
                             netStatus* status);

private:
    rlHttpDownloadTask(const rlHttpDownloadTask& rhs);
    rlHttpDownloadTask& operator=(const rlHttpDownloadTask& rhs);

    void Reset();
	bool SetupPaths(const char* downloadUrl, const char* localPath);
	bool SendRequest();
	bool SendFileSizeRequest();
	bool DoesFileExist(bool deleteFile = true);
	bool ResumeFile();
	bool _CreateFile();
	void CloseFile();
	void RemoveFile();
	bool RemoteFileHasChanged();

    enum State
    {
        STATE_INVALID       = -1,
		STATE_GET_SERVER_FILE_SIZE,
		STATE_GET_SERVER_FILE_SIZE_RESPONSE,
		STATE_CHECK_FOR_EXISTING_FILE,
		STATE_GET_HEADER,
		STATE_GETTING_HEADER,
		STATE_DOWNLOAD,
        STATE_DOWNLOADING,
		STATE_FAILED,
		STATE_FINISHED
	};

    State m_State;
    netHttpRequest m_HttpRequest;
	bool m_HttpRequestInited;
	void* m_DownloadBuffer;

	unsigned m_TimeoutSecs;
    unsigned m_StartTime;

	OverwriteOptions m_Overwrite;

	fiHandle m_fiHandle;
	const fiDevice* m_fiDevice;

	char m_RemotePath[512];
	const char* m_RemoteFileName;

	char m_LocalPath[RAGE_MAX_PATH];
	const char* m_LocalFileName;

	ProgressCallback m_ProgressCallback;
	void* m_ProgressCallbackData;

	bool m_HaveResponseHeader;

	// Resumable Download Support
	unsigned int m_CurrentFileSize;
	unsigned int m_ContentFileSize;

	fiHandle m_fiResponseHandle;
	const fiDevice* m_fiResponseDevice;
	void* m_DeviceMemoryBuffer;
	char m_DeviceMemoryFilename[64];

    netStatus m_MyStatus;
};

//PURPOSE
//  Checks the file size of a given file.
class rlHttpFileSizeTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlHttpFileSizeTask);
	RL_TASK_USE_CHANNEL(rline_http);

	rlHttpFileSizeTask();
	virtual ~rlHttpFileSizeTask();

	//PURPOSE
	// Configures the file size task.
	//PARAMS
	//  urlPath			- Location of file to check file size
	//  contentLength	- Pointer to store the content length
	bool Configure(const char* urlPath, unsigned int* contentLength);

	virtual void Start();
	virtual void Update(const unsigned timeStep);
	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void DoCancel();

private:
	rlHttpFileSizeTask(const rlHttpFileSizeTask& rhs);
	rlHttpFileSizeTask& operator=(const rlHttpFileSizeTask& rhs);

	void Reset();
	bool SetupPaths(const char* downloadUrl);
	bool SendFileSizeRequest();

	enum State
	{
		STATE_INVALID       = -1,
		STATE_GET_SERVER_FILE_SIZE,
		STATE_GET_SERVER_FILE_SIZE_RESPONSE,
		STATE_FAILED,
		STATE_FINISHED
	};

	State m_State;
	netHttpRequest m_HttpRequest;
	bool m_HttpRequestInited;

	unsigned m_TimeoutSecs;
	unsigned m_StartTime;

	unsigned int* m_ContentLength;

	char m_RemotePath[512];
	const char* m_RemoteFileName;

	bool m_HaveResponseHeader;

	netStatus m_MyStatus;
};

//PURPOSE
//  Sample code demonstrating how to upload UGC video
//  The workflow is:
//
//  1. Call rlUgc::CreateContent() to create the content record.
//  2. Upon complestion CreateContent will populate a rlUgcMetadata instance.
//  3. Pass the rlUgcMetadata instance to rlUgcGta5VideoMetadataData.SetMetadata()
//      to parse the video-specific data.
//  4. Load the video data into a memory buffer
//  5. Create and run an instance of rlHttpLlnwUploadVideoTask.  See below for an example.
//  6. When rlHttpLlnwUploadVideoTask completes call rlUgc::Publish().
//  7. After a short delay the video will be available on the CDN video platform
//      in multiple formats.
//  8. Call rlUgc::QueryContent() to retrieve the final metadata.
//  9. Upon complestion QueryContent will populate a rlUgcMetadata instance.
//  10. Pass the rlUgcMetadata instance to rlUgcGta5VideoMetadataData.SetMetadata()
//      to parse the video-specific data.
//
//  After step 10 the rlUgcGta5VideoMetadataData will contain the public URL
//  for accessing the video.
//
//  This class can handle uploading an entire video at once, or uploading in
//  pieces.  Pausable uploads will use the "upload by pieces" functionality.
//  In this case the caller must keep track of which pieces have been uploaded.
//
//  Note that "piece" numbers start with 1, not zero.
//
class rlHttpLlnwUploadVideoTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlHttpLlnwUploadVideoTask);
	RL_TASK_USE_CHANNEL(rline_http);

	rlHttpLlnwUploadVideoTask();
	virtual ~rlHttpLlnwUploadVideoTask();

    //PURPOSE
    //  Configure task to upload video content.
    //
    //  The parameters can be obtained from the response to rlUgc::CreateContent().
    //  Pass the resulting rlUgcMetadata to rlUgcGta5VideoMetadataData.SetMetadata()
    //  to parse out the staging url, staging ref ID, etc.
    //
    //  The piece parameter represents which piece/chunk of the video is being
    //  uploaded.  If srcData contains the entire video content then pass zero
    //  for piece.  Otherwise pass the piece number, starting with 1.
    //PARAMS
	//  name        Name of content being uploaded
    //  url         URL to use for uploading
    //  authToken   Auth token
    //  refId       Reference ID for file being uploaded
    //  piece       The piece/chunk number represented by the source data.
    //              Pass zero if srcData represents an entire video buffer.
    //  srcData     Data to upload
    //  srcDataLength   Length of data to upload
	bool Configure(const char* name,
                    const char* url,
                    const char* authToken,
                    const char* refId,
                    const int piece,
                    const void* srcData,
                    const unsigned srcDataLength);

    //PURPOSE
    //  Configure task to upload video content.
    //  This version attempts to obtain the auth token directly
    //  from the cdn provider.  We leave this in just for demonstation
    //  but don't actually use it because that would require embedding
    //  the user name and password in the code.
    //
    //  The parameters can be obtained from the response to rlUgc::CreateContent().
    //  Pass the resulting rlUgcMetadata to rlUgcGta5VideoMetadataData.SetMetadata()
    //  to parse out the staging url, staging ref ID, etc.
    //
    //  The piece parameter represents which piece/chunk of the video is being
    //  uploaded.  If srcData contains the entire video content then pass zero
    //  for piece.  Otherwise pass the piece number, starting with 1.
    //PARAMS
	//  name        Name of content being uploaded
    //  url         URL to use for uploading
    //  username    Username used to obtain an auth token
    //  password    Password used to obtain an auth token
    //  refId       Reference ID for file being uploaded
    //  piece       The piece/chunk number represented by the source data.
    //              Pass zero if srcData represents an entire video buffer.
    //  srcData     Data to upload
    //  srcDataLength   Length of data to upload
	bool Configure(const char* name,
                    const char* url,
                    const char* username,
                    const char* password,
                    const char* refId,
                    const int piece,
                    const void* srcData,
                    const unsigned srcDataLength);

	virtual void Start();
	virtual void Update(const unsigned timeStep);
	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void DoCancel();

private:
	rlHttpLlnwUploadVideoTask(const rlHttpLlnwUploadVideoTask&);
	rlHttpLlnwUploadVideoTask& operator=(const rlHttpLlnwUploadVideoTask&);

	void Reset();
    //Retrieves the auth token from the cdn provider
    bool GetAuthToken();
    //Uploads a piece of the video
    bool UploadPiece();

	enum State
	{
		STATE_INVALID       = -1,
        STATE_GET_AUTH_TOKEN,
        STATE_GETTING_AUTH_TOKEN,
		STATE_UPLOAD_PIECE,
		STATE_UPLOADING_PIECE,
        STATE_FAILED,
		STATE_FINISHED
	};

	State m_State;
	netHttpRequest m_HttpRequest;

    char m_StagingName[64];
    char m_StagingUrl[512];
    char m_StagingAuthToken[64];
    char m_StagingUserName[64];
    char m_StagingPassword[64];
    char m_StagingRefId[64];
    const u8* m_SrcData;
    unsigned m_SrcDataLen;

    //To demonstrate uploading by "pieces" we divide
    //the source data int several pieces and upload each
    //one in sequence.

    static const unsigned PIECE_SIZE = 1024*1024;

    int m_NumPieces;
    int m_NumPiecesUploaded;

    //The current piece that's being uploaded.
    int m_CurrentPiece;

    //Piece number that was explicitly requested.
    //If non-zero then the video buffer we have represents
    //only a chunk of the entire content.
    //Otherwise we have the entire video and we'll take
    //care of dividing it up into pieces.
    int m_RequestedPiece;

	netStatus m_MyStatus;
};


//PURPOSE
//  Sample code demonstrating how to upload UGC photo
//  The workflow is:
//
//  1. Call rlUgc::CreateContent() to create the content record.
//  2. Upon completion CreateContent will populate a rlUgcMetadata instance.
//  3. Pass the rlUgcMetadata instance to rlUgcGta5PhotoMetadataData.SetMetadata()
//      to parse the photo-specific data.
//  4. Load the photo data into a memory buffer
//  5. Create and run an instance of rlHttpCdnUploadPhotoTask.  See below for an example.
//  6. When rlHttpCdnUploadPhotoTask completes call rlUgc::Publish().
//
class rlHttpCdnUploadPhotoTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlHttpCdnUploadPhotoTask);
	RL_TASK_USE_CHANNEL(rline_http);

	rlHttpCdnUploadPhotoTask();
	virtual ~rlHttpCdnUploadPhotoTask();

	//PURPOSE
	//  Configure task to upload photo content.
	//
	//  The parameters can be obtained from the response to rlUgc::CreateContent().
	//  Pass the resulting rlUgcMetadata to rlUgcGta5PhotoMetadataData.SetMetadata()
	//  to parse out the staging url, etc.
	//
	//PARAMS
	//  name        Name of content being uploaded
	//  url         URL to use for uploading
	//  authToken   Auth token
	//  dirName     Name of directory to save photo to
	//  fileName    Filename to use when saving the photo
	//  srcData     Data to upload
	//  srcDataLength   Length of data to upload
	bool Configure(
		const char* name,
		const char* url,
		const char* authToken,
		const char* dirName,
		const char* fileName,
		const void* srcData,
		const unsigned srcDataLength,
		const char* responseHeaderValueToGet,
		char* responseHeaderValueBuffer,
		const unsigned responseHeaderValueBufferSize,
		const bool akamai);

	virtual void Start();
	virtual void Update(const unsigned timeStep);
	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void DoCancel();

private:
	rlHttpCdnUploadPhotoTask(const rlHttpCdnUploadPhotoTask&);
	rlHttpCdnUploadPhotoTask& operator=(const rlHttpCdnUploadPhotoTask&);

	void Reset();

	//Uploads the photo
	bool Upload();

	enum State
	{
		STATE_INVALID       = -1,
		STATE_UPLOAD,
		STATE_UPLOADING,
		STATE_FAILED,
		STATE_FINISHED
	};

	State m_State;
	netHttpRequest m_HttpRequest;

	char m_StagingName[64];
	char m_StagingUrl[512];
	char m_StagingAuthToken[512];
	char m_StagingDirName[128];
	char m_StagingFileName[128];

	char m_RealUrl[1024];

	const u8* m_SrcData;
	unsigned m_SrcDataLen;

	const char* m_ResponseHeaderValueToGet;
	char* m_ResponseHeaderValueBuffer;
	unsigned m_ResponseHeaderValueBufferSize;
	bool m_Akamai;

	netStatus m_MyStatus;
};

//PURPOSE
//  Can be used to specify a bigger bounce buffer. A bigger bounce buffer improves the download speed 
//  The download speed is limited to the size of the bounce buffer (per frame).
//  From a certain point on you won't notice any improvement as other factors come into play.
//  Currently about 80KB are a good value but that may change in future.
//
//  NOTE: There may be cases where it's better to keep the buffer externally. 
//  For example if you create multiple tasks at once but they are processed sequentially.
//PARAMS
//  T                  - The type of the class this will derive from. It will usually be rlHttpTask or rlRosHttpTask
//  BOUNCE_BUFFER_SIZE - The size in bytes of the buffer.
#define DEFAULT_LARGE_BOUNCE_BUFFER_SIZE (80 * 1024)

template <class T, unsigned BOUNCE_BUFFER_SIZE = DEFAULT_LARGE_BOUNCE_BUFFER_SIZE>
class rlHttpTaskWithBounceBuffer : public T
{
public:
	rlHttpTaskWithBounceBuffer(sysMemAllocator* allocator = NULL)
		: T(allocator, m_BounceBuffer, BOUNCE_BUFFER_SIZE)
	{}

private:
	u8 m_BounceBuffer[BOUNCE_BUFFER_SIZE];
};

} //namespace rage

#endif  //RLINE_RLHTTPTASK_H
