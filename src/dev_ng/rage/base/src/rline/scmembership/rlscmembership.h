// 
// rline/scmembership/rlscmembership.h
// 
// Copyright (C) Rockstar Games. All Rights Reserved. 
// 

#ifndef RLSCMEMBERSHIP_H
#define RLSCMEMBERSHIP_H

#include "rline/scmembership/rlscmembershipcommon.h"

#if RL_SC_MEMBERSHIP_ENABLED

#include "atl/delegate.h"
#include "net/status.h"

namespace rage
{

class rlScMembership
{
	typedef atDelegator<void(const rlScMembershipEvent*)> Delegator;

public:

	//PURPOSE
	//  Delegate for handling events.
	//  See rlscmembershipcommon.h for event types.
	typedef Delegator::Delegate Delegate;

	// PURPOSE:
	//	Initializes the rlScMembership interface
	static bool Init();

	// PURPOSE:
	//	Returns true if the rlScMembership interface has been initialized
	static bool IsInitialized();

	// PURPOSE:
	//	Shuts down the rlScMembership interface and clears membership data
	static void Shutdown();

	// PURPOSE:
	//	Updates the rlScMembership interface. Should be called once per frame.
	static void Update();

	// PURPOSE:
	//	Request membership data for the indicated gamer index
	static bool RequestMembershipStatus(const int localGamerIndex);

	// PURPOSE:
	//	Returns the data source for the indicated gamer index
	static rlScMembershipDataSource GetMembershipDataSource(const int localGamerIndex);

	// PURPOSE:
	//	Returns the result of our service request for the indicated gamer index
	static rlScMembershipRequestResult GetMembershipRequestResult(const int localGamerIndex);

	// PURPOSE:
	//	Returns the task state for the indicated gamer index
	static rlScMembershipTelemetryState GetMembershipTelemetryState(const int localGamerIndex);

	// PURPOSE:
	//	Returns whether we have valid membership data for the indicated gamer index
	static bool HasMembershipData(const int localGamerIndex);

	// PURPOSE:
	//	Request membership data for the indicated gamer index
	static const rlScMembershipInfo& GetMembershipInfo(const int localGamerIndex);

	// PURPOSE:
	//	Direct check for membership
	static bool HasMembership(const int localGamerIndex);

	// PURPOSE:
	//	Checks whether membership can expire (meaning, can we have membership and then lose it
	//  mid-session if it expires)
	static bool CanMembershipExpire();

	// PURPOSE:
	//	Returns the subscriptionId for membership
	static const char* GetSubscriptionId();

	// PURPOSE:
	//	Requests and consumes subscription benefits from ROS
	static bool EvaluateSubscriptionBenefits(const int localGamerIndex);

	//PURPOSE
	//  Add or remove a delegate that will be called with event notifications.
	static void AddDelegate(Delegate* dlgt);
	static void RemoveDelegate(Delegate* dlgt);

	//PURPOSE
	//  Accessors to behaviour toggles
	static void SetAllowMembershipsToExpire(const bool allow);
	static void SetUseMembershipServiceTasks(const bool useServiceTasks);
	static void SetEnableEvaluateBenefits(const bool enableEvaluateBenefits);

#if RL_ALLOW_DUMMY_MEMBERSHIP_DATA
	static void SetDummyMembershipData(const bool hasMembership, const u64 membershipStart, const u64 membershipEnd);
#endif

private:

	// PURPOSE:
	//	Task request state
	enum TaskState
 	{
		State_Idle,
		State_Requested,
		State_Active,
	};

	// PURPOSE:
	//	Contains membership information and request state
	struct MembershipData
	{
		MembershipData()
			: m_LocalGamerIndex(-1)
			, m_HasValidData(false)
			, m_DataSource(rlScMembershipDataSource::Source_Invalid)
			, m_RequestResult(rlScMembershipRequestResult::Result_TaskNotStarted)
			, m_TaskState(TaskState::State_Idle)
			, m_LastRequestTimestamp(0)
		{

		}

		void Init(const int localGamerIndex);
		void Reset();

		int m_LocalGamerIndex;
		rlScMembershipInfo m_Info;
		bool m_HasValidData;
		rlScMembershipDataSource m_DataSource;
		rlScMembershipRequestResult m_RequestResult;

		// task data
		TaskState m_TaskState;
		rlScMembershipInfo m_TaskInfo;
		netStatus m_RequestStatus;
		unsigned m_LastRequestTimestamp;
	};

	struct SubscriptionBenefitsData
	{
		SubscriptionBenefitsData()
			: m_LocalGamerIndex(-1)
			, m_TaskState(TaskState::State_Idle)
		{

		}

		void Init(const int localGamerIndex);
		void Reset();

		rlScSubscriptionBenefits m_SubscriptionBenefits;

		int m_LocalGamerIndex;
		TaskState m_TaskState;
		netStatus m_RequestStatus;
	};

	static void DispatchEvent(const rlScMembershipEvent* e);

	static void SetMembershipInfo(const int localGamerIndex, const rlScMembershipInfo& info, const rlScMembershipDataSource dataSource);
	static bool ShouldUseMembershipServiceTasks();
	
	static bool DoMembershipRequest(MembershipData& data);
	static bool TryCreateRequestMembershipStatusTask(MembershipData& data);
	static bool TryCreateDummyRequestMembershipStatusTask(MembershipData& data);

	static bool IsEvaulateBenefitsEnabled();
	static bool DoSubscriptionBenefitsRequest(SubscriptionBenefitsData& data);
	static bool TryCreateGetSubscriptionBenefitsTask(SubscriptionBenefitsData& data);

#if __BANK
	static void InitWidgets(); 
	static void Bank_RequestMembership();
	static void Bank_ApplyMembershipInfo();
	static void Bank_EvaluateSubscriptionBenefits();
#endif

	static MembershipData sm_MembershipData[NUM_LOCAL_MEMBERSHIPS];
	static SubscriptionBenefitsData sm_BenefitsData[NUM_LOCAL_MEMBERSHIPS];
	static bool sm_IsInitialised;
	static Delegator sm_Delegator;

	// tunables
	static bool sm_AllowMembershipsToExpire;
	static bool sm_UseMembershipServiceTasks;
	static bool sm_EvaluateBenefitsEnabled;
};

}

#endif
#endif