// 
// rline/scmembership/rlscmembership.h
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "rlscmembership.h"

#if RL_SC_MEMBERSHIP_ENABLED
#include "rlscmembershiptasks.h"
#include "diag/channel.h"
#include "rline/rldiag.h"
#include "system/param.h"
#include "system/timer.h"

#if __BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#endif

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, membership)
RAGE_DEFINE_SUBCHANNEL(rline, membership)
#undef __rage_channel
#define __rage_channel rline_membership

PARAM(netEnableScMembershipServerTasks, "Enables membership ROS tasks to request data from the server");
PARAM(netEnableEvaluateBenefits, "Enables evaluation of membership benefits");
PARAM(netHasScMembership, "If present, specifies whether we have SC membership or not");
PARAM(netScMembershipStartPosix, "If present, specifies start of SC membership");
PARAM(netScMembershipExpiryPosix, "If present, specifies expiry of SC membership");
PARAM(netScMembershipAllowExpire, "If present, determines whether SC membership can expire mid-session");
PARAM(netScAddUnconsumedStipend, "If present, adds an unconsumed stipend benefit");
PARAM(netScIgnoreFirstServiceResult, "If present, adds an unconsumed stipend benefit");

#if RL_ALLOW_DUMMY_MEMBERSHIP_DATA
static bool s_UseDebugInfo = false;
static rlScMembershipInfo s_DebugInfo;
static rlScMembershipDataSource s_DebugInfoSource;
static bool s_HasFirstServiceResult = false;
#endif

#if __BANK
static bool s_BankHasServerData = false;
static const unsigned MAX_BANK_TEXT = 32;
static char s_BankServerHasMembership[MAX_BANK_TEXT];
static char s_BankServerMembershipStart[MAX_BANK_TEXT];
static char s_BankServerMembershipExpiry[MAX_BANK_TEXT];
#endif

rlScMembershipInfo rlScMembershipInfo::INVALID;

void rlScMembership::MembershipData::Init(const int localGamerIndex)
{
	Reset();
	m_LocalGamerIndex = localGamerIndex;
}

void rlScMembership::MembershipData::Reset()
{
	// cancel pending membership request
	if(m_RequestStatus.Pending())
	{
		RLSCMEMBERSHIP_CANCEL_TASK(&m_RequestStatus);
	}

	// clear membership state
	m_LocalGamerIndex = -1;
	m_Info.Clear();
	m_HasValidData = false;
	m_RequestResult = rlScMembershipRequestResult::Result_TaskNotStarted;
	m_DataSource = rlScMembershipDataSource::Source_Invalid;

	m_TaskState = TaskState::State_Idle;
	m_LastRequestTimestamp = 0;
	m_TaskInfo.Clear();
}

void rlScMembership::SubscriptionBenefitsData::Init(const int localGamerIndex)
{
	Reset();
	m_LocalGamerIndex = localGamerIndex;
}

void rlScMembership::SubscriptionBenefitsData::Reset()
{
	// cancel pending membership request
	if(m_RequestStatus.Pending())
	{
		RLSCMEMBERSHIP_CANCEL_TASK(&m_RequestStatus);
	}

	m_LocalGamerIndex = -1;
	m_SubscriptionBenefits.m_Benefits.Reset();
	m_TaskState = TaskState::State_Idle;
}

bool rlScMembership::sm_IsInitialised = false;
rlScMembership::MembershipData rlScMembership::sm_MembershipData[NUM_LOCAL_MEMBERSHIPS];
rlScMembership::SubscriptionBenefitsData rlScMembership::sm_BenefitsData[NUM_LOCAL_MEMBERSHIPS];
rlScMembership::Delegator rlScMembership::sm_Delegator;
bool rlScMembership::sm_AllowMembershipsToExpire = true;
bool rlScMembership::sm_UseMembershipServiceTasks = true;
bool rlScMembership::sm_EvaluateBenefitsEnabled = true;

bool rlScMembership::Init()
{
	// initialise membership data
	for(int i = 0; i < NUM_LOCAL_MEMBERSHIPS; i++)
	{
		sm_MembershipData[i].Init(i);
		sm_BenefitsData[i].Init(i);
	}

#if __BANK
	// initialise bank widgets to edit membership data
	InitWidgets();
#endif

	sm_IsInitialised = true;

	return true;
}

bool rlScMembership::IsInitialized()
{
	return sm_IsInitialised;
}

void rlScMembership::Shutdown()
{
	if(!sm_IsInitialised)
		return;

	sm_IsInitialised = false;

	// reset membership data
	for(int i = 0; i < NUM_LOCAL_MEMBERSHIPS; i++)
	{
		sm_MembershipData[i].Reset();
		sm_BenefitsData[i].Reset();
	}
}

void rlScMembership::Update()
{
#if !__FINAL
	static bool s_CheckedCommandLine = false;
	if(!s_CheckedCommandLine)
	{
		int hasMembership = 0;
		if(PARAM_netHasScMembership.Get(hasMembership))
		{
			s_UseDebugInfo = true; 
			s_DebugInfoSource = rlScMembershipDataSource::Source_CommandLine;

			int membershipStart = 0;
			PARAM_netScMembershipStartPosix.Get(membershipStart);

			int membershipExpiry = 0;
			PARAM_netScMembershipExpiryPosix.Get(membershipExpiry);

			s_DebugInfo.Apply((hasMembership == 1), static_cast<u64>(membershipStart), static_cast<u64>(membershipExpiry));

			rlDebug("Update :: Applying Command Line - HasMembership: %s, Start: %u, End: %u",
				s_DebugInfo.HasMembership() ? "True" : "False",
				static_cast<int>(s_DebugInfo.GetStartPosix()),
				static_cast<int>(s_DebugInfo.GetExpirePosix()));
		}
		s_CheckedCommandLine = true; 
	}
#endif

	// update requests
	for(int i = 0; i < NUM_LOCAL_MEMBERSHIPS; i++)
	{
		switch(sm_MembershipData[i].m_TaskState)
		{
		case TaskState::State_Idle:
			break;

		case TaskState::State_Requested:
			DoMembershipRequest(sm_MembershipData[i]);
			break;

		case TaskState::State_Active:
			{
				// check if the task has completed...
				if(!sm_MembershipData[i].m_RequestStatus.Pending())
				{
					rlDebug("Update :: Request Complete - %s - HasMembership: %s, Start: %u, End: %u",
						sm_MembershipData[i].m_RequestStatus.Succeeded() ? "Succeeded" : "Failed",
						sm_MembershipData[i].m_TaskInfo.HasMembership() ? "True" : "False",
						static_cast<int>(sm_MembershipData[i].m_TaskInfo.GetStartPosix()),
						static_cast<int>(sm_MembershipData[i].m_TaskInfo.GetExpirePosix()));

					// update our info
#if RL_ALLOW_DUMMY_MEMBERSHIP_DATA
					if(s_UseDebugInfo)
					{
						rlDebug("Update :: Request Complete - Applying Debug Info");
						SetMembershipInfo(i, s_DebugInfo, s_DebugInfoSource);
					}
					else if(PARAM_netScIgnoreFirstServiceResult.Get() && !s_HasFirstServiceResult)
					{
						// just set the data to false for now
						rlScMembershipInfo debugInfo; 
						debugInfo.Apply(false, 0, 0);

						SetMembershipInfo(i, debugInfo, rlScMembershipDataSource::Source_IgnoreFirstResult);
						s_HasFirstServiceResult = true; 
					}
					else
#endif
					{
						rlDebug("Update :: Request Complete - Applying Task Info");
						const rlScMembershipDataSource dataSource = ShouldUseMembershipServiceTasks() ? rlScMembershipDataSource::Source_ServiceTask : rlScMembershipDataSource::Source_DummyTask;
						SetMembershipInfo(i, sm_MembershipData[i].m_TaskInfo, dataSource);
					}

					// clear the task info regardless
					sm_MembershipData[i].m_TaskInfo.Clear();

#if __BANK
					// update bank text
					s_BankHasServerData = sm_MembershipData[i].m_RequestStatus.Succeeded();
					if(s_BankHasServerData)
					{
						formatf(s_BankServerHasMembership, "%s", sm_MembershipData[i].m_Info.HasMembership() ? "True" : "False");
						formatf(s_BankServerMembershipStart, "%u", static_cast<unsigned>(sm_MembershipData[i].m_Info.GetStartPosix()));
						formatf(s_BankServerMembershipExpiry, "%u", static_cast<unsigned>(sm_MembershipData[i].m_Info.GetExpirePosix()));
					}
#endif
					// update the task state
					sm_MembershipData[i].m_RequestResult = sm_MembershipData[i].m_RequestStatus.Succeeded() ? rlScMembershipRequestResult::Result_TaskSucceeded : rlScMembershipRequestResult::Result_TaskFailed;
					
					// update the time-stamp (todo: use this for interval back-off in the event of failure)
					sm_MembershipData[i].m_LastRequestTimestamp = sysTimer::GetSystemMsTime();

					// reset task state
					sm_MembershipData[i].m_TaskState = TaskState::State_Idle;
				}
			}
			break;
		}

		// update subscription benefits
		switch(sm_BenefitsData[i].m_TaskState)
		{
		case TaskState::State_Idle:
			break;

		case TaskState::State_Requested:
			DoSubscriptionBenefitsRequest(sm_BenefitsData[i]);
			break;

		case TaskState::State_Active:
			// check if the task has completed...
			if(!sm_BenefitsData[i].m_RequestStatus.Pending())
			{
				rlDebug("Update :: Evaluation Complete");

#if !__FINAL
				// add debug stipends to claim
				int numStipends = 0;
				if(PARAM_netScAddUnconsumedStipend.Get(numStipends))
				{
					for(int s = 0; s < numStipends; s++)
						sm_BenefitsData[i].m_SubscriptionBenefits.AddBenefit(rlScSubscriptionBenefits::Benefit("1", "GTA5_GTAPLUS_MONTHLYSTIPEND"));
				}
#endif

				// if we have any benefits, send them up
				if(sm_BenefitsData[i].m_SubscriptionBenefits.GetCount() > 0)
				{
					rlDebug("Update :: Dispatching %d Benefits", sm_BenefitsData[i].m_SubscriptionBenefits.GetCount());
					
					rlScUnconsumedSubscriptionBenefitsEvent e(i, sm_BenefitsData[i].m_SubscriptionBenefits);
					DispatchEvent(&e);
				}

				// reset data
				sm_BenefitsData[i].m_SubscriptionBenefits.Reset();
				sm_BenefitsData[i].m_TaskState = TaskState::State_Idle;
			}
			break;
		};

		// check for memberships expiring...
		if(CanMembershipExpire() && sm_MembershipData[i].m_Info.CheckAndUpdateExpiry())
		{
			rlDebug("Update :: Membership Expired");
			SetMembershipInfo(i, sm_MembershipData[i].m_Info, rlScMembershipDataSource::Source_Expired);
		}
	}
}

void rlScMembership::SetMembershipInfo(const int localGamerIndex, const rlScMembershipInfo& info, const rlScMembershipDataSource dataSource)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return;

	// we have received information for the first time
	if(!sm_MembershipData[localGamerIndex].m_HasValidData)
	{
		rlDebug("SetMembershipInfo :: StatusReceived - HasMembership: %s, Start: %u, End: %u, Source: %s",
			info.HasMembership() ? "True" : "False",
			static_cast<unsigned>(info.GetStartPosix()),
			static_cast<unsigned>(info.GetExpirePosix()),
			rlScGetMembershipDataSourceAsString(dataSource));
		
		rlScMembershipStatusReceivedEvent e(localGamerIndex, info);
		DispatchEvent(&e);
		
		sm_MembershipData[localGamerIndex].m_HasValidData = true;
		sm_MembershipData[localGamerIndex].m_Info = info;
		sm_MembershipData[localGamerIndex].m_DataSource = dataSource;
	}
	else if(sm_MembershipData[localGamerIndex].m_Info != info)
	{
		rlDebug("SetMembershipInfo :: StatusChanged - HasMembership: %s -> %s, Start: %u -> %u, End: %u -> %u, Source: %s",
			sm_MembershipData[localGamerIndex].m_Info.HasMembership() ? "True" : "False",
			info.HasMembership() ? "True" : "False",
			static_cast<unsigned>(sm_MembershipData[localGamerIndex].m_Info.GetStartPosix()),
			static_cast<unsigned>(info.GetStartPosix()),
			static_cast<unsigned>(sm_MembershipData[localGamerIndex].m_Info.GetExpirePosix()),
			static_cast<unsigned>(info.GetExpirePosix()),
			rlScGetMembershipDataSourceAsString(dataSource));

		if(!info.HasMembership() && !CanMembershipExpire())
		{
			rlDebug("SetMembershipInfo :: Ignoring expired membership change");
			return;
		}

		rlScMembershipStatusChangedEvent e(localGamerIndex, sm_MembershipData[localGamerIndex].m_Info, info);
		DispatchEvent(&e);
		
		// capture the new information (after logging)
		sm_MembershipData[localGamerIndex].m_Info = info;
		sm_MembershipData[localGamerIndex].m_DataSource = dataSource;
	}
}

bool rlScMembership::EvaluateSubscriptionBenefits(const int localGamerIndex)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return false;

	// if we have already requested membership information or we have a request in flight, accept that data
	if(sm_BenefitsData[localGamerIndex].m_TaskState != TaskState::State_Idle)
		return true;

	rlDebug("EvaluateSubscriptionBenefits :: Index: %d", localGamerIndex);

	// flag request as pending
	sm_BenefitsData[localGamerIndex].m_TaskState = TaskState::State_Requested;

	return true;
}

bool rlScMembership::RequestMembershipStatus(const int localGamerIndex)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return false;

	// if we have already requested membership information or we have a request in flight, accept that data
	if(sm_MembershipData[localGamerIndex].m_TaskState != TaskState::State_Idle)
		return true;

	rlDebug("RequestMembershipStatus :: Index: %d", localGamerIndex);

	// flag request as pending
	sm_MembershipData[localGamerIndex].m_TaskState = TaskState::State_Requested;

	return true;
}

rlScMembershipDataSource rlScMembership::GetMembershipDataSource(const int localGamerIndex)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return rlScMembershipDataSource::Source_Invalid;

	return sm_MembershipData[localGamerIndex].m_DataSource;
}

rlScMembershipRequestResult rlScMembership::GetMembershipRequestResult(const int localGamerIndex)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return rlScMembershipRequestResult::Result_TaskNotStarted;

	return sm_MembershipData[localGamerIndex].m_RequestResult;
}

rlScMembershipTelemetryState rlScMembership::GetMembershipTelemetryState(const int localGamerIndex)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return rlScMembershipTelemetryState::State_Unknown_NotRequested;

	// do not factor in debug membership for telemetry reporting
	switch (sm_MembershipData[localGamerIndex].m_RequestResult)
	{
	case rlScMembershipRequestResult::Result_TaskNotStarted:
		return rlScMembershipTelemetryState::State_Unknown_NotRequested;
	case rlScMembershipRequestResult::Result_TaskFailed:
		return rlScMembershipTelemetryState::State_Unknown_RequestFailed;
	case rlScMembershipRequestResult::Result_TaskSucceeded:
		return HasMembership(localGamerIndex) ? rlScMembershipTelemetryState::State_Known_Member : rlScMembershipTelemetryState::State_Known_NotMember;
	}

	return rlScMembershipTelemetryState::State_Unknown_NotRequested;
}

bool rlScMembership::HasMembershipData(const int localGamerIndex)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return false;

	return sm_MembershipData[localGamerIndex].m_HasValidData;
}

const rlScMembershipInfo& rlScMembership::GetMembershipInfo(const int localGamerIndex)
{
	if(!RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex))
		return rlScMembershipInfo::INVALID;

	// return debug information if using that
#if RL_ALLOW_DUMMY_MEMBERSHIP_DATA
	if(s_UseDebugInfo)
		return s_DebugInfo;
#endif

	return sm_MembershipData[localGamerIndex].m_Info;
}

bool rlScMembership::HasMembership(const int localGamerIndex)
{
	return GetMembershipInfo(localGamerIndex).HasMembership();
}

bool rlScMembership::CanMembershipExpire()
{
#if !__FINAL
	int allowMembershipExpiry;
	if(PARAM_netScMembershipAllowExpire.Get(allowMembershipExpiry))
		return allowMembershipExpiry > 0;
#endif

	return sm_AllowMembershipsToExpire;
}

const char* rlScMembership::GetSubscriptionId()
{
	// possibly supply this from the game / application
#if RSG_PROSPERO
	return "GTAOPL";
#elif RSG_SCARLETT
	return "CFQ7TTC0HX8W";
#else
	return nullptr;
#endif
}

bool rlScMembership::ShouldUseMembershipServiceTasks()
{
#if !__FINAL
	int enableServiceTasks;
	if(PARAM_netEnableScMembershipServerTasks.Get(enableServiceTasks))
		return enableServiceTasks > 0;
#endif

	return sm_UseMembershipServiceTasks;
}

bool rlScMembership::IsEvaulateBenefitsEnabled()
{
#if !__FINAL
	int enableEvaluateBenefits;
	if(PARAM_netEnableEvaluateBenefits.Get(enableEvaluateBenefits))
		return enableEvaluateBenefits > 0;
#endif

	return sm_EvaluateBenefitsEnabled;
}

void rlScMembership::DispatchEvent(const rlScMembershipEvent* e)
{
	rlDebug("DispatchEvent :: %s", e->GetName());
	sm_Delegator.Dispatch(e);
}

void rlScMembership::AddDelegate(Delegate* dlgt)
{
	sm_Delegator.AddDelegate(dlgt);
}

void rlScMembership::RemoveDelegate(Delegate* dlgt)
{
	sm_Delegator.RemoveDelegate(dlgt);
}

void rlScMembership::SetAllowMembershipsToExpire(const bool allow)
{
	if(sm_AllowMembershipsToExpire != allow)
	{
		rlDebug("SetAllowMembershipsToExpire :: %s -> %s", sm_AllowMembershipsToExpire ? "True" : "False", allow ? "True" : "False");
		sm_AllowMembershipsToExpire = allow;
	}
}

void rlScMembership::SetUseMembershipServiceTasks(const bool useServiceTasks)
{
	if(sm_UseMembershipServiceTasks != useServiceTasks)
	{
		rlDebug("SetUseMembershipServiceTasks :: %s -> %s", sm_UseMembershipServiceTasks ? "True" : "False", useServiceTasks ? "True" : "False");
		sm_UseMembershipServiceTasks = useServiceTasks;
	}
}

void rlScMembership::SetEnableEvaluateBenefits(const bool enableEvaluateBenefits)
{
	if(sm_EvaluateBenefitsEnabled != enableEvaluateBenefits)
	{
		rlDebug("SetEnableEvaluateBenefits :: %s -> %s", sm_EvaluateBenefitsEnabled ? "True" : "False", enableEvaluateBenefits ? "True" : "False");
		sm_EvaluateBenefitsEnabled = enableEvaluateBenefits;
	}
}

#if RL_ALLOW_DUMMY_MEMBERSHIP_DATA
void rlScMembership::SetDummyMembershipData(const bool hasMembership, const u64 membershipStart, const u64 membershipEnd)
{
	rlDebug("SetDummyMembershipData :: HasMembership: %s, Start: %u, End: %u",
		hasMembership ? "True" : "False",
		static_cast<int>(membershipStart),
		static_cast<int>(membershipEnd));

	s_DebugInfo.Apply(hasMembership, membershipStart, membershipEnd);

	s_DebugInfoSource = rlScMembershipDataSource::Source_Tunable;
	s_UseDebugInfo = true;
}
#endif

bool rlScMembership::TryCreateDummyRequestMembershipStatusTask(MembershipData& data)
{
#if RL_MEMBERSHIP_ENABLE_DUMMY_TASK
	// this needs to be in a separate function for a separate rtry / rcatch block
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlRequestScMembershipDummyTask, &data.m_RequestStatus, data.m_LocalGamerIndex, &data.m_TaskInfo);
#else
	(void)localGamerIndex;
	(void)info;
	(void)status;
	return false;
#endif
}

bool rlScMembership::TryCreateRequestMembershipStatusTask(MembershipData& data)
{
#if RL_MEMBERSHIP_ENABLE_SERVICE_TASK
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlScRequestMembershipStatusTask, &data.m_RequestStatus, data.m_LocalGamerIndex, &data.m_TaskInfo, rlScMembership::GetSubscriptionId());
#else
	(void)localGamerIndex;
	(void)info;
	(void)status;
	return false;
#endif
}

bool rlScMembership::DoMembershipRequest(MembershipData& data)
{
	rtry
	{
		rcheckall(data.m_TaskState == TaskState::State_Requested);

		// we should prevent setting the request flag if a request is in flight
		rverify(!data.m_RequestStatus.Pending(), error, );

		// verify index is valid
		const int localGamerIndex = data.m_LocalGamerIndex;
		rverify(RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex), error, );

		// wait until we have R* credentials
		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		rcheckall(creds.IsValid());

		// use the actual tasks if this command line is enabled (allows us to switch to these without 
		// requiring a new build)
		if(ShouldUseMembershipServiceTasks())
		{
			rverify(TryCreateRequestMembershipStatusTask(data), error, );
		}
		else
		{
			rverify(TryCreateDummyRequestMembershipStatusTask(data), error, );
		}

		rlDebug("DoMembershipRequest :: Issued Request - Index: %d", data.m_LocalGamerIndex);

		// advance task state
		data.m_TaskState = TaskState::State_Active;

		return true;
	}
	rcatch(banned)
	{
		data.m_TaskInfo.Clear();
		data.m_Info.Clear();
		data.m_RequestResult = rlScMembershipRequestResult::Result_TaskFailed;
		data.m_HasValidData = false;
		data.m_LastRequestTimestamp = sysTimer::GetSystemMsTime();
		data.m_TaskState = TaskState::State_Idle;
	}
	rcatch(error)
	{
		// if we hit an error, reset the task state so that we don't continually spam this
		data.m_TaskState = TaskState::State_Idle;
	}
	rcatchall
	{
		
	}

	return false;
}

bool rlScMembership::TryCreateGetSubscriptionBenefitsTask(SubscriptionBenefitsData& data)
{
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlEvaluateAndConsumeSubscriptionBenefitsTask, &data.m_RequestStatus, data.m_LocalGamerIndex, &data.m_SubscriptionBenefits);
}

bool rlScMembership::DoSubscriptionBenefitsRequest(SubscriptionBenefitsData& data)
{
	rtry
	{
		// check if enabled
		rcheck(IsEvaulateBenefitsEnabled(), error, rlDebug("DoSubscriptionBenefitsRequest :: Disabled"));
		
		rcheckall(data.m_TaskState == TaskState::State_Requested);

		// we should prevent setting the request flag if a request is in flight
		rverify(!data.m_RequestStatus.Pending(), error, );

		// verify index is valid
		const int localGamerIndex = data.m_LocalGamerIndex;
		rverify(RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex), error, );

		// wait until we have R* credentials
		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		rcheckall(creds.IsValid());

		// check we're not banned
		rcheck(!rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_BANNED), banned, );

		// create and start task
		rcheck(TryCreateGetSubscriptionBenefitsTask(data), error, );
		
		rlDebug("DoSubscriptionBenefitsRequest :: Issued Request - Index: %d", data.m_LocalGamerIndex);

		// advance task state
		data.m_TaskState = TaskState::State_Active;

		return true;
	}
	rcatch(banned)
	{
		data.m_TaskState = TaskState::State_Idle;
	}
	rcatch(error)
	{
		// if we hit an error, reset the task state so that we don't continually spam this
		data.m_TaskState = TaskState::State_Idle;
	}
	rcatchall
	{
		
	}

	return false;
}

#if __BANK

static bool s_BankHasMembership = false;
static int s_BankMembershipStart = 0;
static int s_BankMembershipExpiry = 0;

void rlScMembership::Bank_EvaluateSubscriptionBenefits()
{
	const int gamerIndex = rlPresence::GetActingUserIndex();
	rlDebug("Bank_EvaluateSubscriptionBenefits :: GamerIndex: %d", gamerIndex);

	if(RL_IS_VALID_MEMBERSHIP_INDEX(gamerIndex))
		rlScMembership::EvaluateSubscriptionBenefits(gamerIndex);
}

void rlScMembership::Bank_RequestMembership()
{
	const int gamerIndex = rlPresence::GetActingUserIndex();
	rlDebug("Bank_RequestMembership :: GamerIndex: %d", gamerIndex);

	if(RL_IS_VALID_MEMBERSHIP_INDEX(gamerIndex))
		rlScMembership::RequestMembershipStatus(gamerIndex);
}

void rlScMembership::Bank_ApplyMembershipInfo()
{
	s_DebugInfo.Apply(s_BankHasMembership, static_cast<u64>(s_BankMembershipStart), static_cast<u64>(s_BankMembershipExpiry));

	s_DebugInfoSource = rlScMembershipDataSource::Source_Bank;
	s_UseDebugInfo = true;

	// if we already completed our membership request, set the membership information now
	if(GetMembershipRequestResult(0) != rlScMembershipRequestResult::Result_TaskNotStarted)
		SetMembershipInfo(0, s_DebugInfo, s_DebugInfoSource);

	rlDebug("Bank_ApplyMembershipInfo :: HasMembership: %s, Start: %u, End: %u",
		s_DebugInfo.HasMembership() ? "True" : "False",
		static_cast<int>(s_DebugInfo.GetStartPosix()),
		static_cast<int>(s_DebugInfo.GetExpirePosix()));
}

void rlScMembership::InitWidgets()
{
	if(!bkManager::IsEnabled())
		return;

	bkBank* pBank = BANKMGR.FindBank("Network");

	if(!pBank)
		pBank = &BANKMGR.CreateBank("Network");

	// setup to indicate that we have no data yet
	safecpy(s_BankServerHasMembership, "-");
	safecpy(s_BankServerMembershipStart, "-");
	safecpy(s_BankServerMembershipExpiry, "-");

	pBank->PushGroup("SC Membership", false);

	pBank->AddButton("Request Membership", datCallback(Bank_ApplyMembershipInfo));
	pBank->AddButton("Evaluate Subscription Benefits", datCallback(Bank_EvaluateSubscriptionBenefits));

	pBank->AddSeparator();

	// just show the data for the first membership entry
	pBank->AddText("Has Server Data", &s_BankHasServerData, false);
	pBank->AddText("Has Membership", s_BankServerHasMembership, MAX_BANK_TEXT);
	pBank->AddText("Membership Start", s_BankServerMembershipStart, MAX_BANK_TEXT);
	pBank->AddText("Membership Expiry", s_BankServerMembershipExpiry, MAX_BANK_TEXT);

	pBank->AddSeparator();

	pBank->AddToggle("Has Membership", &s_BankHasMembership, NullCallback);
	pBank->AddText("Membership Start", &s_BankMembershipStart, true);
	pBank->AddText("Membership Expiry", &s_BankMembershipExpiry, true);
	pBank->AddButton("Apply Bank Fields", datCallback(Bank_ApplyMembershipInfo));

	pBank->PopGroup();
}

#endif

}
#endif

