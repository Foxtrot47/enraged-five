// 
// rline/scmembership/rlscmembershiptasks.cpp
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "rlscmembership.h"

#if RL_SC_MEMBERSHIP_ENABLED
#include "diag/channel.h"
#include "system/param.h"

#if RSG_DURANGO
#include "net/durango/xblhttp.h"
#endif

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, membership)
#undef __rage_channel
#define __rage_channel rline_membership

// macro for handling errors in the response
#define ERROR_CASE(code, codeEx, resCode)	\
	if(result.IsError(code, codeEx))		\
	{										\
		resultCode = resCode;				\
		return;								\
	}

// error codes reported by the server
enum rlScMembershipErrorCode
{
	RL_MEMBERSHIP_ERROR_UNEXPECTED_RESULT = -1,			//Unhandled error code
	RL_MEMBERSHIP_ERROR_NONE = 0,						//Success
	RL_MEMBERSHIP_ERROR_AUTHENTICATIONFAILED_TICKET,	//Invalid ROS Ticket
};

#if RSG_SCARLETT
//////////////////////////////////////////////////////////////////////////
// rlRequestStorePurchaseAccessTokenTask
//////////////////////////////////////////////////////////////////////////
bool rlGetXblStoreIdTask::rlRequestStorePurchaseAccessTokenTask::Configure(int localGamerIndex, rlXblPurchaseAccessToken* purchaseAccessToken)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverifyall(purchaseAccessToken != nullptr);

		m_PurchaseAccessToken = purchaseAccessToken;

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlGetXblStoreIdTask::rlRequestStorePurchaseAccessTokenTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		const char* token = rlHttpTaskHelper::ReadString(node, "Token", nullptr);
		rverify(token, catchall, rlTaskError("Failed to read <Token>"));
		safecpy(m_PurchaseAccessToken->m_Token, token);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlGetXblStoreIdTask::rlRequestStorePurchaseAccessTokenTask::rlRequestStorePurchaseAccessTokenTask::GetServiceMethod() const
{
	return "entitlements.asmx/GetMSStorePurchaseAccessToken";
}

//////////////////////////////////////////////////////////////////////////
// rlGetXblStoreIdTask
//////////////////////////////////////////////////////////////////////////
rlGetXblStoreIdTask::rlGetXblStoreIdTask()
{

}

rlGetXblStoreIdTask::~rlGetXblStoreIdTask()
{

}

bool rlGetXblStoreIdTask::Configure(const int localGamerIndex, rlXblCommerceStoreId* storeId)
{
	rtry
	{
		rverify(RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex), catchall,);
		m_LocalGamerIndex = localGamerIndex;

		rverify(storeId != nullptr, catchall,);
		m_StoreId = storeId;

		m_RequestState = STATE_GET_PURCHASE_ACCESS_TOKEN;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlGetXblStoreIdTask::IsCancelable() const
{
	return true;
}

bool rlGetXblStoreIdTask::CreatePurchaseAccessTokenTask()
{
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlRequestStorePurchaseAccessTokenTask, &m_MyStatus, m_LocalGamerIndex, &m_PurchaseAccessToken);
}

void rlGetXblStoreIdTask::Update(const unsigned timeStep)
{
	rlTaskBase::Update(timeStep);

	if(IsFinished())
		return;

	if(this->WasCanceled())
	{
		// Wait for cancel to finish.
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}
		return;
	}

	switch(m_RequestState)
	{
	case STATE_GET_PURCHASE_ACCESS_TOKEN:
		{
			if(CreatePurchaseAccessTokenTask())
			{
				rlTaskDebug("GetPurchaseAccessToken :: CreatePurchaseAccessTokenTask succeeded...");
				m_RequestState = STATE_GETTING_PURCHASE_ACCESS_TOKEN;
			}
			else
			{
				rlTaskError("GetPurchaseAccessToken :: CreatePurchaseAccessTokenTask failed!");
				this->Finish(FINISH_FAILED, -1);
			}
		}
		break;

	case STATE_GETTING_PURCHASE_ACCESS_TOKEN:
		{
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					rlTaskDebug("GettingPurchaseAccessToken :: Succeeded");
					
					static const char* s_PublisherId = "Publisher";
					if(g_rlXbl.GetPlatformCommerceManager()->GetUserPurchaseId(
						m_LocalGamerIndex,
						m_PurchaseAccessToken.m_Token,
						s_PublisherId,
						m_StoreId,
						&m_MyStatus))
					{
						rlTaskDebug("GettingPurchaseAccessToken :: Requesting StoreId...");
						m_RequestState = STATE_GETTING_STORE_ID;
					}
					else
					{
						rlTaskError("GettingPurchaseAccessToken :: GetUserPurchaseId failed!");
						this->Finish(FINISH_FAILED, -1);
					}
				}
				else if(m_MyStatus.Failed())
				{
					rlTaskError("GettingPurchaseAccessToken :: Failed!");
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
			}
		}
		break;

	case STATE_GETTING_STORE_ID:
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlTaskDebug("GettingStoreId :: Succeeded! Task finished");
				this->Finish(FINISH_SUCCEEDED);
			}
			else if(m_MyStatus.Failed())
			{
				rlTaskError("GettingStoreId :: Failed!");
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
		}
		break;
	}
}

void rlGetXblStoreIdTask::DoCancel()
{
	if(m_MyStatus.Pending())
	{
		switch(m_RequestState)
		{
		case STATE_GET_PURCHASE_ACCESS_TOKEN:
			break;
		case STATE_GETTING_PURCHASE_ACCESS_TOKEN:
			rlGetTaskManager()->CancelTask(&m_MyStatus);
		case STATE_GETTING_STORE_ID:
			g_rlXbl.CancelTask(&m_MyStatus);
			break;
		}
	}
}
#endif

//////////////////////////////////////////////////////////////////////////
// rlScRequestMembershipStatusTask
//////////////////////////////////////////////////////////////////////////
rlScRequestMembershipStatusTask::rlScRequestMembershipStatusTask()
{

}

rlScRequestMembershipStatusTask::~rlScRequestMembershipStatusTask()
{

}

bool rlScRequestMembershipStatusTask::Configure(const int localGamerIndex, rlScMembershipInfo* info, const char* subscriptionId)
{
	rtry
	{
		rverify(RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex), catchall,);
		m_LocalGamerIndex = localGamerIndex;

		rverify(info != nullptr, catchall,);
		m_Info = info;
		m_SubscriptionId = subscriptionId;

		m_RequestState = STATE_GET_REQUEST_PARAMETERS;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlScRequestMembershipStatusTask::IsCancelable() const
{
	return true;
}

bool rlScRequestMembershipStatusTask::GetRequestParameters()
{
#if RSG_SCARLETT
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlGetXblStoreIdTask, &m_MyStatus, m_LocalGamerIndex, &m_StoreId);
#elif RSG_PROSPERO
	m_AuthRequestId = g_rlNp.GetNpAuth().GetAuthCode(m_LocalGamerIndex, m_AuthCode, &m_IssuerId, &m_MyStatus);
	return m_AuthRequestId >= 0;
#elif
	return false;
#endif
}

bool rlScRequestMembershipStatusTask::CreateGetSubscriptionDataTask()
{
#if RSG_SCARLETT
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlScGetSubscriptionDataTask, &m_MyStatus, m_LocalGamerIndex, m_Info, m_SubscriptionId, m_StoreId.m_StoreId);
#elif RSG_PROSPERO
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlScGetSubscriptionDataTask, &m_MyStatus, m_LocalGamerIndex, m_Info, m_SubscriptionId, &m_AuthCode, m_IssuerId);
#elif
	return false;
#endif
}

void rlScRequestMembershipStatusTask::Update(const unsigned timeStep)
{
	rlTaskBase::Update(timeStep);

	if(IsFinished())
		return;

	if(this->WasCanceled())
	{
		// Wait for cancel to finish.
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}
		return;
	}

	switch(m_RequestState)
	{
	case STATE_GET_REQUEST_PARAMETERS:
		{
			if(GetRequestParameters())
			{
				rlTaskDebug("GetRequestParameters :: GetRequestParameters succeeded...");
				m_RequestState = STATE_GETTING_REQUEST_PARAMETERS;
			}
			else
			{
				rlTaskError("GetRequestParameters :: GetRequestParameters failed!");
				this->Finish(FINISH_FAILED, -1);
			}
		}
		break;

	case STATE_GETTING_REQUEST_PARAMETERS:
#if RSG_PROSPERO
		g_rlNp.GetNpAuth().UpdateAuthCodeRequest(m_LocalGamerIndex, m_AuthRequestId, &m_MyStatus);
#endif

		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlTaskDebug("GettingRequestParameters :: Succeeded...");
				
				if(CreateGetSubscriptionDataTask())
				{
					rlTaskDebug("GettingRequestParameters :: Requesting Subscription Data...");
					m_RequestState = STATE_GETTING_SUBSCRIPTION_DATA;
				}
				else
				{
					rlTaskError("GettingRequestParameters :: CreateGetSubscriptionDataTask Failed!");
					this->Finish(FINISH_FAILED, -1);
				}
			}
			else if(m_MyStatus.Failed())
			{
				rlTaskError("GettingRequestParameters :: Failed!");
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
		}
		break;

	case STATE_GETTING_SUBSCRIPTION_DATA:
		{
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					rlTaskDebug("GettingSubscriptionData :: Succeeded...");
					this->Finish(FINISH_SUCCEEDED);
				}
				else if(m_MyStatus.Failed())
				{
					rlTaskError("GettingSubscriptionData :: Failed!");
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
			}
		}
		break;
	}
}

void rlScRequestMembershipStatusTask::DoCancel()
{
	if(m_MyStatus.Pending())
	{
		switch(m_RequestState)
		{
		case STATE_GET_REQUEST_PARAMETERS:
			break;
		case STATE_GETTING_REQUEST_PARAMETERS:
#if RSG_PROSPERO
			g_rlNp.GetNpAuth().CancelAuthCodeRequest(
				m_LocalGamerIndex,
				m_AuthRequestId,
				&m_MyStatus);
#endif
		case STATE_GETTING_SUBSCRIPTION_DATA:
			RLSCMEMBERSHIP_CANCEL_TASK(&m_MyStatus);
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// rlRequestScMembershipXblInnerTask
//////////////////////////////////////////////////////////////////////////
bool rlScRequestMembershipStatusTask::rlScGetSubscriptionDataTask::Configure(
	const int localGamerIndex
	, rlScMembershipInfo* info
	, const char* subscriptionId
#if RSG_SCARLETT
	, const char* storeId
#elif RSG_PROSPERO
	, rlSceNpAuthorizationCode* authCode
	, const int issuerId
#endif
)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverifyall(info != nullptr);

		m_Info = info;

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddStringParameter("subscriptionId", subscriptionId, true), catchall, );

		// add platform specific parameters
#if RSG_SCARLETT
		rverify(AddStringParameter("storeId", storeId, true), catchall, );
#elif RSG_PROSPERO
		rverify(AddStringParameter("authCodeV3", authCode->code, true), catchall, );
		rverify(AddIntParameter("issuerId", issuerId, true), catchall, );
		rverify(AddStringParameter("sku", sysAppContent::GetBuildLocaleCode(), true), catchall, );
#endif
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlScRequestMembershipStatusTask::rlScGetSubscriptionDataTask::GetServiceMethod() const
{
#if RSG_SCARLETT
	return "entitlements.asmx/GetSubscriptionDataXbl";
#elif RSG_PROSPERO
	return "entitlements.asmx/GetSubscriptionDataPSN";
#endif
}

bool rlScRequestMembershipStatusTask::rlScGetSubscriptionDataTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		// clear the info
		m_Info->Clear();

		// read the membership active status
		bool hasMembership = false;
		rcheck(rlHttpTaskHelper::ReadBool(hasMembership, node, "Active", nullptr), catchall, rlTaskError("Failed to read <Active>"));

		// we only parse error on these if we have membership
		const char* membershipStart = rlHttpTaskHelper::ReadString(node, "SubStartTime", nullptr);
		rcheck(!hasMembership || membershipStart, catchall, rlTaskError("Failed to read <SubStartTime>"));
		const u64 startPosix = membershipStart ? netHttpRequest::ParseDateStringRFC3339(membershipStart) : 0;

		const char* membershipEnd = rlHttpTaskHelper::ReadString(node, "SubEndTime", nullptr);
		rcheck(!hasMembership || membershipEnd, catchall, rlTaskError("Failed to read <SubEndTime>"));
		const u64 expirePosix = membershipEnd ? netHttpRequest::ParseDateStringRFC3339(membershipEnd) : 0;

		m_Info->Apply(hasMembership, startPosix, expirePosix);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlScRequestMembershipStatusTask::rlScGetSubscriptionDataTask::ProcessError(const rlRosResult& result, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
{
	ERROR_CASE("AuthenticationFailed", "Ticket", rlScMembershipErrorCode::RL_MEMBERSHIP_ERROR_AUTHENTICATIONFAILED_TICKET);

	rlTaskWarning("ProcessError :: Unhandled error: Code: %s, CodeEx: %s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[null]");
	resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
}

#if RL_MEMBERSHIP_ENABLE_DUMMY_TASK
//////////////////////////////////////////////////////////////////////////
// rlRequestScMembershipDummyTask
//////////////////////////////////////////////////////////////////////////
bool rlRequestScMembershipDummyTask::Configure(int localGamerIndex, rlScMembershipInfo* info)
{
	m_LocalGamerIndex = localGamerIndex;
	m_Info = info;

	return true;
}

void rlRequestScMembershipDummyTask::Update(const unsigned timeStep)
{
	rlTaskBase::Update(timeStep);

	if(IsFinished())
	{
		return;
	}

	if(this->WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
	}

	m_Info->Clear();
	this->Finish(FINISH_SUCCEEDED, -1);
}
#endif // RL_MEMBERSHIP_ENABLE_DUMMY_TASK

//////////////////////////////////////////////////////////////////////////
// rlEvaluateAndConsumeSubscriptionBenefitsTask
//////////////////////////////////////////////////////////////////////////
rlEvaluateAndConsumeSubscriptionBenefitsTask::rlEvaluateAndConsumeSubscriptionBenefitsTask()
{

}

rlEvaluateAndConsumeSubscriptionBenefitsTask::~rlEvaluateAndConsumeSubscriptionBenefitsTask()
{

}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::Configure(int localGamerIndex, rlScSubscriptionBenefits* benefits)
{
	rtry
	{
		rverifyall(RL_IS_VALID_MEMBERSHIP_INDEX(localGamerIndex));
		rverifyall(benefits != nullptr);

		m_LocalGamerIndex = localGamerIndex;
		m_SubscriptionBenefits = benefits;

		m_RequestState = STATE_GET_REQUEST_PARAMETERS;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::IsCancelable() const
{
	return true;
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::GetRequestParameters()
{
#if RSG_SCARLETT
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlGetXblStoreIdTask, &m_MyStatus, m_LocalGamerIndex, &m_StoreId);
#elif RSG_PROSPERO
	m_AuthRequestId = g_rlNp.GetNpAuth().GetAuthCode(m_LocalGamerIndex, m_AuthCode, &m_IssuerId, &m_MyStatus);
	return m_AuthRequestId >= 0;
#elif
	return false;
#endif
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::CreateEvaluateBenefitsTask()
{
#if RSG_SCARLETT
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlScEvaluateBenefitsTask, &m_MyStatus, m_LocalGamerIndex, m_StoreId.m_StoreId);
#elif RSG_PROSPERO
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlScEvaluateBenefitsTask, &m_MyStatus, m_LocalGamerIndex, &m_AuthCode, m_IssuerId);
#elif
	return false;
#endif
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::CreateGetUnconsumedBenefitsTask()
{
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlScGetUnconsumedBenefitsTask, &m_MyStatus, m_LocalGamerIndex, &m_UnconsumedBenefits);
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::CreateConsumeBenefitsTask()
{
	RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(rlScConsumeBenefitsTask, &m_MyStatus, m_LocalGamerIndex, m_UnconsumedBenefits, m_SubscriptionBenefits);
}

void rlEvaluateAndConsumeSubscriptionBenefitsTask::Update(const unsigned timeStep)
{
	rlTaskBase::Update(timeStep);

	if(IsFinished())
		return;

	if(this->WasCanceled())
	{
		// Wait for cancel to finish.
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}
		return;
	}

	switch(m_RequestState)
	{
	case STATE_GET_REQUEST_PARAMETERS:
		{
			if(GetRequestParameters())
			{
				rlTaskDebug("GetRequestParameters :: GetRequestParameters succeeded...");
				m_RequestState = STATE_GETTING_REQUEST_PARAMETERS;
			}
			else
			{
				rlTaskError("GetRequestParameters :: GetRequestParameters failed!");
				this->Finish(FINISH_FAILED, -1);
			}
		}
		break;

	case STATE_GETTING_REQUEST_PARAMETERS:
#if RSG_PROSPERO
		g_rlNp.GetNpAuth().UpdateAuthCodeRequest(m_LocalGamerIndex, m_AuthRequestId, &m_MyStatus);
#endif

		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlTaskDebug("GettingRequestParameters :: Succeeded...");
				
				if(CreateEvaluateBenefitsTask())
				{
					rlTaskDebug("GettingRequestParameters :: Evaluating Subscription Benefits...");
					m_RequestState = STATE_EVALUATING_BENEFITS;
				}
				else
				{
					rlTaskError("GettingRequestParameters :: CreateEvaluateBenefitsTask Failed!");
					this->Finish(FINISH_FAILED, -1);
				}
			}
			else if(m_MyStatus.Failed())
			{
				rlTaskError("GettingRequestParameters :: Failed!");
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
		}
		break;

	case STATE_EVALUATING_BENEFITS:
		{
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					rlTaskDebug("EvaluatingBenefits :: Succeeded...");

					if(CreateGetUnconsumedBenefitsTask())
					{
						rlTaskDebug("EvaluatingBenefits :: Requesting Unconsumed Benefits...");
						m_RequestState = STATE_GETTING_UNCONSUMED_BENEFITS;
					}
					else
					{
						rlTaskError("EvaluatingBenefits :: CreateGetUnconsumedBenefitsTask Failed!");
						this->Finish(FINISH_FAILED, -1);
					}
				}
				else if(m_MyStatus.Failed())
				{
					rlTaskError("EvaluatingBenefits :: Failed!");
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
			}
		}
		break;

	case STATE_GETTING_UNCONSUMED_BENEFITS:
	{
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				rlTaskDebug("GettingUnconsumedBenefits :: Succeeded...");

				// are there any benefits to consume....
				if(m_UnconsumedBenefits.m_Benefits.GetCount() > 0)
				{
					if(CreateConsumeBenefitsTask())
					{
						rlTaskDebug("GettingUnconsumedBenefits :: Consuming Benefits...");
						m_RequestState = STATE_CONSUMING_BENEFITS;
					}
					else
					{
						rlTaskError("GettingUnconsumedBenefits :: CreateConsumeBenefitsTask Failed!");
						this->Finish(FINISH_FAILED, -1);
					}
				}
				else
				{
					rlTaskDebug("GettingUnconsumedBenefits :: No Unconsumed Benefits");
					this->Finish(FINISH_SUCCEEDED);
				}
			}
			else if(m_MyStatus.Failed())
			{
				rlTaskError("GettingUnconsumedBenefits :: Failed!");
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
		}
	}
	break;

	case STATE_CONSUMING_BENEFITS:
		{
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					rlTaskDebug("ConsumingBenefits :: Succeeded...");
					this->Finish(FINISH_SUCCEEDED);
				}
				else if(m_MyStatus.Failed())
				{
					rlTaskError("ConsumingBenefits :: Failed!");
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
			}
		}
		break;
	}
}

void rlEvaluateAndConsumeSubscriptionBenefitsTask::DoCancel()
{
	if(m_MyStatus.Pending())
	{
		switch(m_RequestState)
		{
		case STATE_GET_REQUEST_PARAMETERS:
			break;
		case STATE_GETTING_REQUEST_PARAMETERS:
#if RSG_PROSPERO
			g_rlNp.GetNpAuth().CancelAuthCodeRequest(
				m_LocalGamerIndex,
				m_AuthRequestId,
				&m_MyStatus);
#endif
		case STATE_EVALUATING_BENEFITS:
		case STATE_GETTING_UNCONSUMED_BENEFITS:
		case STATE_CONSUMING_BENEFITS:
			RLSCMEMBERSHIP_CANCEL_TASK(&m_MyStatus);
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// rlScEvaluateBenefitsTask
//////////////////////////////////////////////////////////////////////////
bool rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScEvaluateBenefitsTask::Configure(
	const int localGamerIndex
#if RSG_SCARLETT
	, const char* storeId
#elif RSG_PROSPERO
	, rlSceNpAuthorizationCode* authCode
	, const int issuerId
#endif
)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

	// add platform specific parameters
#if RSG_SCARLETT
		rverify(AddStringParameter("storeId", storeId, true), catchall,);
#elif RSG_PROSPERO
		rverify(AddStringParameter("authCodeV3", authCode->code, true), catchall, );
		rverify(AddIntParameter("issuerId", issuerId, true), catchall, );
		rverify(AddStringParameter("sku", sysAppContent::GetBuildLocaleCode(), true), catchall, );
#endif
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScEvaluateBenefitsTask::GetServiceMethod() const
{
#if RSG_SCARLETT
	return "entitlements.asmx/EvaluateSubscriptionBenefitsXbl";
#elif RSG_PROSPERO
	return "entitlements.asmx/EvaluateSubscriptionBenefitsPSN";
#endif
}

//////////////////////////////////////////////////////////////////////////
// rlScGetUnconsumedBenefitsTask
//////////////////////////////////////////////////////////////////////////
bool rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScGetUnconsumedBenefitsTask::Configure(const int localGamerIndex, UnconsumedBenefits* unconsumedBenefits)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverify(unconsumedBenefits != nullptr, catchall, );

		m_UnconsumedBenefits = unconsumedBenefits;

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall,);

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScGetUnconsumedBenefitsTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		// clear any existing data
		m_UnconsumedBenefits->m_Benefits.Reset();

		rverifyall(node != nullptr);

		const parTreeNode* results = node->FindChildWithName("UnconsumedBenefitInstances");
		rcheck(results != nullptr, catchall, rlTaskError("Failed to find <UnconsumedBenefitInstances> element"));

		parTreeNode::ChildNodeIterator iter = results->BeginChildren();
		for(int index = 0; iter != results->EndChildren(); ++iter, ++index)
		{
			parTreeNode* record = *iter;
			rcheck(record != nullptr, catchall, rlTaskError("Invalid <UnconsumedBenefitInstance> at %d", index));

			const char* benefitInstanceId = rlHttpTaskHelper::ReadString(record, "BenefitInstanceId", nullptr);
			rcheck(benefitInstanceId, catchall, rlTaskError("Failed to read <BenefitInstanceId> at %d", index));
			
			const char* benefitCode = rlHttpTaskHelper::ReadString(record, "BenefitCode", nullptr);
			rcheck(benefitCode, catchall, rlTaskError("Failed to read <BenefitCode> at %d", index));

			rlTaskDebug("Added UnconsumedBenefit :: Id: %s, Code: %s", benefitInstanceId, benefitCode);
			UnconsumedBenefit benefit(benefitInstanceId, benefitCode);
			m_UnconsumedBenefits->m_Benefits.PushAndGrow(benefit);
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScGetUnconsumedBenefitsTask::GetServiceMethod() const
{
	return "entitlements.asmx/GetUnconsumedBenefitInstances";
}

//////////////////////////////////////////////////////////////////////////
// rlScConsumeBenefitsTask
//////////////////////////////////////////////////////////////////////////
bool rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScConsumeBenefitsTask::Configure(const int localGamerIndex, const UnconsumedBenefits& unconsumedBenefits, rlScSubscriptionBenefits* subscriptionBenefits)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverifyall(unconsumedBenefits.m_Benefits.GetCount() > 0);
		rverifyall(subscriptionBenefits != nullptr);

		m_SubscriptionBenefits = subscriptionBenefits;
		m_UnconsumedBenefits = unconsumedBenefits;

		rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall,);

		// build instances string
		atString instanceIds; 
		
		int instanceNum = unconsumedBenefits.m_Benefits.GetCount();
		for(int i = 0; i < instanceNum; i++)
		{
			if(i > 0)
				instanceIds += ",";

			const UnconsumedBenefit& unconsumedBenefit = unconsumedBenefits.m_Benefits[i];
			instanceIds += unconsumedBenefit.m_BenefitInstanceId;
		}

		rverify(AddStringParameter("unconsumedBenefitInstancesIdsCsv", instanceIds.c_str(), true), catchall, );
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScConsumeBenefitsTask::FindAndClaimUnconsumedBenefit(const char* benefitInstanceId, const char* benefitCode)
{
	int benefitsNum = m_UnconsumedBenefits.m_Benefits.GetCount();
	for(int i = 0; i < benefitsNum; i++)
	{
		if(strcmp(benefitInstanceId, m_UnconsumedBenefits.m_Benefits[i].m_BenefitInstanceId) != 0)
			continue;

		// benefit code not always supplied in response
		if(benefitCode != nullptr && strcmp(benefitCode, m_UnconsumedBenefits.m_Benefits[i].m_BenefitCode) != 0)
			continue;

		if(!rlTaskVerifyf(!m_UnconsumedBenefits.m_Benefits[i].m_ConsumeClaimed, "FindAndClaimUnconsumedBenefit :: Already Claimed! Id: %s, Code: %s", benefitInstanceId, benefitCode))
			continue;

		m_UnconsumedBenefits.m_Benefits[i].m_ConsumeClaimed = true;
		return true;
	}
	return false;
}

bool rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScConsumeBenefitsTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
{
	rtry
	{
		// clear any existing data
		m_SubscriptionBenefits->m_Benefits.Reset();

		rverifyall(node != nullptr);

		const parTreeNode* results = node->FindChildWithName("BenefitInstanceConsumptionResults");
		rcheck(results != nullptr, catchall, rlTaskError("Failed to find <BenefitInstanceConsumptionResults> element"));

		parTreeNode::ChildNodeIterator iter = results->BeginChildren();
		for(int index = 0; iter != results->EndChildren(); ++iter, ++index)
		{
			parTreeNode* record = *iter;
			rverify(record != nullptr, catchall, rlTaskError("Invalid <BenefitInstanceConsumption> at %d", index));

			const char* benefitInstanceId = rlHttpTaskHelper::ReadString(record, "BenefitInstanceId", nullptr);
			rverify(benefitInstanceId, catchall, rlTaskError("Failed to read <BenefitInstanceId> at %d", index));

			const char* benefitConsumed = rlHttpTaskHelper::ReadString(record, "Consumed", nullptr);
			rverify(benefitConsumed, catchall, rlTaskError("Failed to read <Consumed> at %d", index));

			const char* consumedString = "true";
			const bool isConsumed = strncmp(benefitConsumed, consumedString, strlen(consumedString)) == 0;

			// not always present if not consumed
			const char* benefitCode = rlHttpTaskHelper::ReadString(record, "BenefitCode", nullptr);
			rverify(benefitCode || !isConsumed, catchall, rlTaskError("Failed to read <BenefitCode> at %d", index));

			if(isConsumed)
			{
				// verify that we actually tried to claim this result - this ensures that we can't just replace the 
				// server response with spoofed claimed benefits in order to gain cash
				if(FindAndClaimUnconsumedBenefit(benefitInstanceId, benefitCode))
				{
					rlTaskDebug("BenefitInstanceConsumption Added :: Id: %s, Code: %s", benefitInstanceId, benefitCode);
					m_SubscriptionBenefits->AddBenefit(rlScSubscriptionBenefits::Benefit(benefitInstanceId, benefitCode));
				}
				else
				{
					rlTaskDebug("BenefitInstanceConsumption Ignored :: Not Found in Unconsumed Claims - Id: %s, Code: %s", benefitInstanceId, benefitCode);
				}
			}
#if RSG_OUTPUT
			else
			{
				const char* failureCode = rlHttpTaskHelper::ReadString(record, "FailureCode", nullptr);
				const char* failureReason = rlHttpTaskHelper::ReadString(record, "FailureReason", nullptr);
				rlTaskDebug("BenefitInstanceConsumption Failed :: Id: %s, Code: %s, FailureCode: %s, FailureReason: %s", benefitInstanceId, benefitCode ? benefitCode : "Not Supplied", failureCode, failureReason);
			}
#endif
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char* rlEvaluateAndConsumeSubscriptionBenefitsTask::rlScConsumeBenefitsTask::GetServiceMethod() const
{
	return "entitlements.asmx/ConsumeBenefitInstances";
}

}
#endif

