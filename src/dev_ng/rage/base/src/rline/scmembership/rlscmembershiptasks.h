// 
// rline/scmembership/rlscmembershiptasks.h
// 
// Copyright (C) Rockstar Games. All Rights Reserved. 
// 

#ifndef RLSCMEMBERSHIPTASKS_H
#define RLSCMEMBERSHIPTASKS_H

#include "rlscmembershipcommon.h"

#if RL_SC_MEMBERSHIP_ENABLED

#include "rline/rltask.h"
#include "rline/ros/rlroshttptask.h"

#if RSG_GDK
#include "rline/durango/rlxblcommerce_interface.h"
#endif

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, membership);

// macros for task creation / cancellation
#define RLSCMEMBERSHIP_CREATE_TASK_WITH_PARAMS(T, status, localGamerIndex, ...) 	        \
	bool success = false;																	\
	rlTaskBase* task = NULL;                                                                \
	rtry																					\
	{																						\
		rcheck(rlRos::IsOnline(localGamerIndex),                                            \
				catchall,                                                                   \
				rlDebug("Local gamer %d is not signed into ROS", localGamerIndex));         \
		T* newTask = rlGetTaskManager()->CreateTask<T>();									\
		rverify(newTask,catchall,);															\
		task = newTask;																		\
		rverify(rlTaskBase::Configure(newTask, localGamerIndex, __VA_ARGS__, status),		\
					catchall, rlError("Failed to configure task"));                         \
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall, );       				\
		success = true;	                                                                    \
	}																						\
	rcatchall																				\
	{																						\
		if(task)																			\
		{																					\
			rlGetTaskManager()->DestroyTask(task);								            \
		}																					\
	}																						\
	return success;		

#define RLSCMEMBERSHIP_CANCEL_TASK(status) 													\
	rlGetTaskManager()->CancelTask(status);		

//////////////////////////////////////////////////////////////////////////
// rlScMembershipBaseTask
//////////////////////////////////////////////////////////////////////////
class rlScMembershipBaseTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlScMembershipBaseTask);
	RL_TASK_USE_CHANNEL(rline_membership);

	rlScMembershipBaseTask() : rlRosHttpTask() {}
	virtual ~rlScMembershipBaseTask() {}

protected:

	virtual bool UseHttps() const override
	{
		return true;
	}

private:
};

#if RSG_SCARLETT
//////////////////////////////////////////////////////////////////////////
// rlGetXblStoreIdTask
//////////////////////////////////////////////////////////////////////////
class rlGetXblStoreIdTask: public rlTaskBase
{
public:
	RL_TASK_DECL(rlGetXblStoreIdTask);
	RL_TASK_USE_CHANNEL(rline_membership);

	rlGetXblStoreIdTask();
	virtual ~rlGetXblStoreIdTask();

	bool Configure(int localGamerIndex, rlXblCommerceStoreId* storeId);

	virtual bool IsCancelable() const override;

protected:
	virtual void Update(const unsigned timeStep) override;
	virtual void DoCancel() override;

private:

	class rlRequestStorePurchaseAccessTokenTask : public rlScMembershipBaseTask
	{
	public:
		RL_TASK_DECL(rlRequestStorePurchaseAccessTokenTask);
		RL_TASK_USE_CHANNEL(rline_membership);

		rlRequestStorePurchaseAccessTokenTask() : rlScMembershipBaseTask() {}
		virtual ~rlRequestStorePurchaseAccessTokenTask() override {}

		bool Configure(const int localGamerIndex, rlXblPurchaseAccessToken* purchaseAccessToken);

	protected:

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual const char* GetServiceMethod() const override;

	private:

		rlXblPurchaseAccessToken* m_PurchaseAccessToken;
	};

	bool CreatePurchaseAccessTokenTask();

	// track state of requesting purchase access token and store Id
	enum RequestState
	{
		STATE_GET_PURCHASE_ACCESS_TOKEN,
		STATE_GETTING_PURCHASE_ACCESS_TOKEN,
		STATE_GETTING_STORE_ID,
	};

	rlXblPurchaseAccessToken m_PurchaseAccessToken;
	rlXblCommerceStoreId* m_StoreId;

	RequestState m_RequestState;
	int m_LocalGamerIndex;

	netStatus m_MyStatus;
};
#endif

//////////////////////////////////////////////////////////////////////////
// rlScRequestMembershipStatusTask
//////////////////////////////////////////////////////////////////////////
class rlScRequestMembershipStatusTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlScRequestMembershipStatusTask);
	RL_TASK_USE_CHANNEL(rline_membership);

	rlScRequestMembershipStatusTask();
	virtual ~rlScRequestMembershipStatusTask();

	bool Configure(int localGamerIndex, rlScMembershipInfo* info, const char* subscriptionId);

	virtual bool IsCancelable() const override;

protected:
	virtual void Update(const unsigned timeStep) override;
	virtual void DoCancel() override;

private:

	class rlScGetSubscriptionDataTask : public rlScMembershipBaseTask
	{
	public:
		RL_TASK_DECL(rlScGetSubscriptionDataTask);
		RL_TASK_USE_CHANNEL(rline_membership);

		rlScGetSubscriptionDataTask() : rlScMembershipBaseTask() {}
		virtual ~rlScGetSubscriptionDataTask() {}

#if RSG_SCARLETT
		bool Configure(const int localGamerIndex, rlScMembershipInfo* info, const char* subscriptionId, const char* storeId);
#elif RSG_PROSPERO
		bool Configure(const int localGamerIndex, rlScMembershipInfo* info, const char* subscriptionId, rlSceNpAuthorizationCode* authCode, const int issuerId);
#endif

	protected:

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual const char* GetServiceMethod() const override;

	private:

		rlScMembershipInfo* m_Info;
	};

	bool GetRequestParameters();
	bool CreateGetSubscriptionDataTask();

	enum RequestState
	{
		STATE_GET_REQUEST_PARAMETERS,
		STATE_GETTING_REQUEST_PARAMETERS,
		STATE_GETTING_SUBSCRIPTION_DATA,
	};

#if RSG_SCARLETT
	rlXblCommerceStoreId m_StoreId;
#elif RSG_PROSPERO
	int m_AuthRequestId;
	rlSceNpAuthorizationCode m_AuthCode;
	int m_IssuerId;
#endif

	// for the inner task
	rlScMembershipInfo* m_Info;
	const char* m_SubscriptionId;

	RequestState m_RequestState;
	int m_LocalGamerIndex;

	netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
//  rlRequestMembershipStatusDummyTask
///////////////////////////////////////////////////////////////////////////////
#if RL_MEMBERSHIP_ENABLE_DUMMY_TASK
class rlRequestScMembershipDummyTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlRequestScMembershipDummyTask);

	rlRequestScMembershipDummyTask() {}
	virtual ~rlRequestScMembershipDummyTask() {}

	bool Configure(int localGamerIndex, rlScMembershipInfo* info);

protected:
	virtual void Update(const unsigned timeStep) override;

private:
	int m_LocalGamerIndex;
	rlScMembershipInfo* m_Info;
};
#endif

//////////////////////////////////////////////////////////////////////////
// rlEvaluateAndConsumeSubscriptionBenefitsTask
//////////////////////////////////////////////////////////////////////////
class rlEvaluateAndConsumeSubscriptionBenefitsTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlEvaluateAndConsumeSubscriptionBenefitsTask);
	RL_TASK_USE_CHANNEL(rline_membership);

	rlEvaluateAndConsumeSubscriptionBenefitsTask();
	virtual ~rlEvaluateAndConsumeSubscriptionBenefitsTask();

	bool Configure(int localGamerIndex, rlScSubscriptionBenefits* benefits);

	virtual bool IsCancelable() const override;

protected:
	virtual void Update(const unsigned timeStep) override;
	virtual void DoCancel() override;

private:

	struct UnconsumedBenefit
	{
		UnconsumedBenefit()
		{
			m_BenefitInstanceId[0] = '\0';
			m_BenefitCode[0] = '\0';
			m_ConsumeClaimed = false;
		}

		UnconsumedBenefit(const char* instanceId, const char* code)
		{
			safecpy(m_BenefitInstanceId, instanceId);
			safecpy(m_BenefitCode, code);
			m_ConsumeClaimed = false;
		}

		char m_BenefitInstanceId[MAX_BENEFIT_ID_LENGTH];
		char m_BenefitCode[MAX_BENEFIT_CODE_LENGTH];
		bool m_ConsumeClaimed;
	};

	struct UnconsumedBenefits
	{
		// can ask SCS if there's a limit here 
		atArray<UnconsumedBenefit> m_Benefits;
	};

	class rlScEvaluateBenefitsTask : public rlScMembershipBaseTask
	{
	public:
		RL_TASK_DECL(rlScEvaluateBenefitsTask);
		RL_TASK_USE_CHANNEL(rline_membership);

		rlScEvaluateBenefitsTask() : rlScMembershipBaseTask() {}
		virtual ~rlScEvaluateBenefitsTask() override {}

#if RSG_SCARLETT
		bool Configure(const int localGamerIndex, const char* storeId);
#elif RSG_PROSPERO
		bool Configure(const int localGamerIndex, rlSceNpAuthorizationCode* authCode, const int issuerId);
#endif

	protected:

		virtual const char* GetServiceMethod() const override;
	};

	class rlScGetUnconsumedBenefitsTask : public rlScMembershipBaseTask
	{
	public:
		RL_TASK_DECL(rlScGetUnconsumedBenefitsTask);
		RL_TASK_USE_CHANNEL(rline_membership);

		rlScGetUnconsumedBenefitsTask() : rlScMembershipBaseTask() {}
		virtual ~rlScGetUnconsumedBenefitsTask() override {}

		bool Configure(const int localGamerIndex, UnconsumedBenefits* unconsumedBenefits);

	protected:

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual const char* GetServiceMethod() const override;

		UnconsumedBenefits* m_UnconsumedBenefits; 
	};

	class rlScConsumeBenefitsTask : public rlScMembershipBaseTask
	{
	public:
		RL_TASK_DECL(rlScConsumeBenefitsTask);
		RL_TASK_USE_CHANNEL(rline_membership);

		rlScConsumeBenefitsTask() : rlScMembershipBaseTask() {}
		virtual ~rlScConsumeBenefitsTask() override {}

		bool Configure(const int localGamerIndex, const UnconsumedBenefits& unconsumedBenefits, rlScSubscriptionBenefits* subscriptionBenefits);

	protected:

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
		virtual const char* GetServiceMethod() const override;

		bool FindAndClaimUnconsumedBenefit(const char* benefitInstanceId, const char* benefitCode);

		UnconsumedBenefits m_UnconsumedBenefits;
		rlScSubscriptionBenefits* m_SubscriptionBenefits;
	};

	bool GetRequestParameters();
	bool CreateEvaluateBenefitsTask();
	bool CreateGetUnconsumedBenefitsTask();
	bool CreateConsumeBenefitsTask();

	enum RequestState
	{
		STATE_GET_REQUEST_PARAMETERS,
		STATE_GETTING_REQUEST_PARAMETERS,
		STATE_EVALUATING_BENEFITS,
		STATE_GETTING_UNCONSUMED_BENEFITS,
		STATE_CONSUMING_BENEFITS,
	};

#if RSG_SCARLETT
	rlXblCommerceStoreId m_StoreId;
#elif RSG_PROSPERO
	int m_AuthRequestId;
	rlSceNpAuthorizationCode m_AuthCode;
	int m_IssuerId;
#endif

	UnconsumedBenefits m_UnconsumedBenefits;
	rlScSubscriptionBenefits* m_SubscriptionBenefits;
	
	RequestState m_RequestState;
	int m_LocalGamerIndex;

	netStatus m_MyStatus;
};

}

#endif
#endif