// 
// rline/scmembership/rlscmembershipcommon.cpp
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "rlscmembershipcommon.h"

#if RL_SC_MEMBERSHIP_ENABLED

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, membership)
#undef __rage_channel
#define __rage_channel rline_membership

#if RSG_OUTPUT
const char* rlScGetMembershipDataSourceAsString(const rlScMembershipDataSource dataSource)
{
	static const char* s_Strings[] =
	{
		"Source_ServiceTask",		// Source_ServiceTask
		"Source_DummyTask",			// Source_DummyTask
		"Source_Expired",   		// Source_Expired
		"Source_Tunable",			// Source_Tunable
		"Source_CommandLine",		// Source_CommandLine
		"Source_IgnoreFirstResult", // Source_IgnoreFirstResult
		"Source_Bank",   			// Source_Bank
	};
	CompileTimeAssert(COUNTOF(s_Strings) == rlScMembershipDataSource::Source_Max);

	return (dataSource >= rlScMembershipDataSource::Source_ServiceTask && dataSource < rlScMembershipDataSource::Source_Max) ? s_Strings[dataSource] : "Source_Invalid";
}
#endif

AUTOID_IMPL(rlScMembershipEvent);
AUTOID_IMPL(rlScMembershipStatusReceivedEvent);
AUTOID_IMPL(rlScMembershipStatusChangedEvent);
AUTOID_IMPL(rlScUnconsumedSubscriptionBenefitsEvent);

/////////////////////////////////////////////////////////////////////////////////
// rlScMembershipStatusReceivedEvent
/////////////////////////////////////////////////////////////////////////////////
rlScMembershipStatusReceivedEvent::rlScMembershipStatusReceivedEvent(const int localGamerIndex, const rlScMembershipInfo& info)
{
	m_LocalGamerIndex = localGamerIndex;
	m_Info = info;
}

/////////////////////////////////////////////////////////////////////////////////
// rlScMembershipStatusChangedEvent
/////////////////////////////////////////////////////////////////////////////////
rlScMembershipStatusChangedEvent::rlScMembershipStatusChangedEvent(const int localGamerIndex, const rlScMembershipInfo& prevInfo, const rlScMembershipInfo& info)
{
	m_LocalGamerIndex = localGamerIndex;
	m_PrevInfo = prevInfo;
	m_Info = info;
}

/////////////////////////////////////////////////////////////////////////////////
// rlScUnconsumedSubscriptionBenefitsEvent
/////////////////////////////////////////////////////////////////////////////////
rlScUnconsumedSubscriptionBenefitsEvent::rlScUnconsumedSubscriptionBenefitsEvent(const int localGamerIndex, const rlScSubscriptionBenefits& benefits)
{
	m_LocalGamerIndex = localGamerIndex;
	m_Benefits = benefits;
}

/////////////////////////////////////////////////////////////////////////////////
// rlScMembershipInfo
/////////////////////////////////////////////////////////////////////////////////
void rlScMembershipInfo::Apply(const bool hasMembership, const u64 startPosix, const u64 endPosix)
{
	m_HasMembership = hasMembership;
	m_MembershipStartPosix = startPosix;
	m_MembershipExpirePosix = endPosix;

	if(CheckAndUpdateExpiry())
	{
		rlDebug("rlScMembershipInfo::Apply :: Already expired!");
	}

	rlDebug("rlScMembershipInfo::Apply :: HasMembership: %s, Start: %u, End: %u",
		m_HasMembership ? "True" : "False",
		static_cast<unsigned>(m_MembershipStartPosix),
		static_cast<unsigned>(m_MembershipExpirePosix));
}

void rlScMembershipInfo::Apply(const bool hasMembership)
{
	m_HasMembership = hasMembership;
	m_MembershipStartPosix = 0;
	m_MembershipExpirePosix = 0;

	rlDebug("rlScMembershipInfo::Apply :: HasMembership: %s, Start: %u, End: %u",
		m_HasMembership ? "True" : "False",
		static_cast<unsigned>(m_MembershipStartPosix),
		static_cast<unsigned>(m_MembershipExpirePosix));
}

bool rlScMembershipInfo::CheckAndUpdateExpiry()
{
	if(m_HasMembership && HasExpired())
	{
		rlDebug("rlScMembershipInfo::CheckAndUpdateExpiry :: Expired!");

		Expire();
		return true; 
	}

	return false;
}

void rlScMembershipInfo::Expire()
{
	// leave the start / expiry values
	m_HasMembership = false;
}

bool rlScMembershipInfo::HasExpired() const
{
	return (m_MembershipExpirePosix > 0) && (rlGetPosixTime() > m_MembershipExpirePosix);
}

}
#endif

