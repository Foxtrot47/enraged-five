// 
// rline/rlnptitleuserstorage.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNPTITLEUSERSTORAGE_H
#define RLINE_RLNPTITLEUSERSTORAGE_H

#if RSG_NP

//FIXME (KB) - remove <np.h>
#include <np.h>
#include "file/file_config.h"
#include "rline/rlnp.h"

#if __USE_TUS

//FIXME (KB) - this class needs a little work.  For example, it
//should handle multiple simultaneous transactions.  It should
//have only one instance of the title context, rather than a
//title context per transaction.  It should support canceling
//individual transactions.

namespace rage
{
class netStatus;

class rlNpTitleUserStorage
{
public:
    rlNpTitleUserStorage();
    ~rlNpTitleUserStorage();

    bool Init(rlNp* np);
    void Update();
    void Shutdown();

    bool IsInitialized() const;

    bool IsPending() const;

    void Drain();

    //Uploads file (stored in memory).
    bool Upload(int slot,
        const void* buf,
        const unsigned bufSize,
        const unsigned timeoutMs,
        netStatus* status);

    //Downloads specified file to memory buffer.
    bool Download(int slot,
        char* buf,
        const unsigned bufSize,
        const unsigned timeoutMs,
        netStatus* status);

    //Delete multiple TUS data of 1 user (asynchronous).
    bool Delete(int slot, const unsigned timeoutMs, netStatus* status);

    //Cancel current operation
    void Cancel();

    //Returns the result of the last transaction
    unsigned GetTransactionResult() { return m_Transaction.m_TransResult; }

private:

    rlNp* m_Np;

    //NP event handler
    rlNpEventDelegator::Delegate m_NpDlgt;
    void OnNpEvent(rlNp* np, const rlNpEvent* evt);

    struct Transaction
    {
        Transaction();
        ~Transaction();

        void Reset();
        bool IsPending() const;
        void Update();
        void Cancel();

        int m_TransResult;
        int m_TransCtxId;
        SceNpTusDataStatus m_TusStatus;
        netStatus* m_Status;
    };

    Transaction m_Transaction;
    int m_TitleCtxId;

    bool m_Initialized      : 1;
};

}

#endif //__USE_TUS
#endif //RSG_NP

#endif  //RLINE_RLNPTITLEUSERSTORAGE_H
