// 
// rline/rlnotifier.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNOTIFIER_H
#define RLINE_RLNOTIFIER_H

#include "atl/atfunctor.h"
#include "atl/atsafecontainer.h"
#include "rline/rldiag.h"

namespace rage
{

template< typename T, typename NTYPE >
class rlNotifier
{
public:

    //PURPOSE
    //  Functor for notify callbacks.
    //
    //  void Callback( T* source, T::NotifyCode code, const void* arg );
    //
    class NotifyHandler : public atFunctor3< void,
                                             T*,
                                             const typename T::NotifyCode,
                                             NTYPE* >
    {
        friend class rlNotifier< T, NTYPE >;

    public:

        NotifyHandler()
            : m_Notifier( 0 )
        {
        }

        virtual ~NotifyHandler()
        {
            if( m_Notifier )
            {
                m_Notifier->RemoveNotifyHandler( this );
            }
        }

        bool IsRegistered() const
        {
            return ( 0 != m_Notifier );
        }

    private:

        DLinkSimple< NotifyHandler > m_Link;
        rlNotifier< T, NTYPE >* m_Notifier;
    };

    rlNotifier()
    {
    }

    //PURPOSE
    //  Adds a functor callback to the collection of callbacks that are
    //  called whenever a notification event occurs.
    void AddNotifyHandler( NotifyHandler* handler )
    {
        rlAssert( !handler->m_Notifier );
        m_NotifyHandlers.AddToTail( handler );
        handler->m_Notifier = this;
    }

    //PURPOSE
    //  Removes a functor callback.
    void RemoveNotifyHandler( NotifyHandler* handler )
    {
        rlAssert( this == handler->m_Notifier );
        m_NotifyHandlers.Remove( handler );
        handler->m_Notifier = 0;
    }

    void Notify( T* source,
                 const typename T::NotifyCode code )
    {
        this->Notify( source, code, 0 );
    }

    void Notify( T* source,
                 const typename T::NotifyCode code,
                 NTYPE* arg )
    {
        typename HandlerList::Iterator it;

        m_NotifyHandlers.BeginIteration( &it );

        for( ; it.IsValid(); it.Next() )
        {
            ( *it.Item() )( source, code, arg );
        }

        m_NotifyHandlers.EndIteration( &it );
    }

private:

    typedef atSafeDLListSimple< NotifyHandler, &NotifyHandler::m_Link > HandlerList;
    HandlerList m_NotifyHandlers;
};

#define RL_NOTIFIER( classname, notifytype )\
    private:\
    typedef ::rage::rlNotifier< classname, notifytype > Notifier;\
    Notifier m_Notifier;\
    protected:\
    void Notify( const NotifyCode code ) { m_Notifier.Notify( this, code ); }\
    void Notify( const NotifyCode code, notifytype* arg ) { m_Notifier.Notify( this, code, arg ); }\
    public:\
    typedef Notifier::NotifyHandler NotifyHandler;\
    void AddNotifyHandler( Notifier::NotifyHandler* handler ) { m_Notifier.AddNotifyHandler( handler ); }\
    void RemoveNotifyHandler( Notifier::NotifyHandler* handler ) { m_Notifier.RemoveNotifyHandler( handler ); }

}   //namespace rage

#endif  //RLINE_RLNOTIFIER_H
