// 
// rline/rlpersistentstorage.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPERSISTENTSTORAGE_H
#define RLINE_RLPERSISTENTSTORAGE_H

#include "rlgamerinfo.h"
#include "rlworker.h"
#include "atl/inlist.h"
#include "net/status.h"
#include "system/criticalsection.h"

namespace rage
{

//PURPOSE
//  Allows async upload and download of large amounts of data.
//
class rlPersistentStorage
{
public:
    rlPersistentStorage();
    ~rlPersistentStorage();

    bool Init();
    void Shutdown();

    struct FileInfo
    {
        int         m_RecordId;
        int         m_FileId;
        unsigned    m_FileSize;

        FileInfo(): m_RecordId(-1), m_FileId(-1), m_FileSize(0) {}
    };

    //Retrieves FileInfo on all files uploaded for the specified gamer.
    bool Query(const rlGamerInfo& gamerInfo,
               const char* tableId,
               unsigned* numFileInfos,      //Num files gamer owns in this table
               unsigned* numFileInfosLimit, //Maximum num files gamer can own in this table
               FileInfo* fileInfos,         //Array of FileInfos to store results
               const unsigned maxFileInfos, //Size of fileInfos array passed in
               netStatus* status);

    //Uploads file (stored in memory).
    bool Upload(const rlGamerInfo& gamerInfo,
                const char* tableId,
                const void* buf,
                const unsigned bufSize,
                FileInfo* fileInfo,
                netStatus* status);

    //Downloads specified file to memory buffer.
    bool Download(const rlGamerInfo& gamerInfo,
                  const FileInfo& fileInfo,
                  void* buf,
                  const unsigned bufSize,
                  unsigned* numBytesDownloaded,
                  netStatus* status);

    //Deletes specified file.
    bool Delete(const rlGamerInfo& gamerInfo,
                const char* tableId,
                const FileInfo& fileInfo,
                netStatus* status);

private:

    //PURPOSE
    //  Base Command class used to queue commands for async processing.
    struct Command
    {
        enum CommandId
        {
            CMD_QUERY,
            CMD_UPLOAD,
            CMD_DOWNLOAD,
            CMD_DELETE,
        };

        Command(const CommandId cmdId,
                netStatus* status);

        virtual ~Command() {}

        CommandId m_CmdId;
        netStatus* m_Status;
        inlist_node<Command> m_ListLink;
    };

    struct QueryCmd : public Command
    {
        QueryCmd(const rlGamerInfo& gamerInfo,
                 const char* tableId,
                 unsigned* numFileInfos,    
                 unsigned* numFileInfosLimit, 
                 FileInfo* fileInfos,       
                 const unsigned maxFileInfos,
                 netStatus* status);

        rlGamerInfo     m_GamerInfo;
        const char*     m_TableId;
        unsigned*       m_NumFileInfos;
        unsigned*       m_NumFileInfosLimit;
        FileInfo*       m_FileInfos;
        unsigned        m_MaxFileInfos;
    };

    struct UploadCmd : public Command
    {
        UploadCmd(const rlGamerInfo& gamerInfo,
                  const char* tableId,
                  const void* buf,
                  const unsigned bufSize,
                  FileInfo* fileInfo,
                  netStatus* status);

        rlGamerInfo     m_GamerInfo;
        const char*     m_TableId;
        const void*     m_Buf;
        unsigned        m_BufSize;
        FileInfo*       m_FileInfo;
    };

    struct DownloadCmd : public Command
    {
        DownloadCmd(const rlGamerInfo& gamerInfo,
                    const FileInfo& fileInfo,
                    void* buf,
                    const unsigned bufSize,
                    unsigned* numBytesDownloaded,
                    netStatus* status);

        rlGamerInfo     m_GamerInfo;
        FileInfo        m_FileInfo;
        void*           m_Buf;
        unsigned        m_BufSize;
        unsigned*       m_NumBytesDownloaded;
    };

    struct DeleteCmd : public Command
    {
        DeleteCmd(const rlGamerInfo& gamerInfo,
                  const char* tableId,
                  const FileInfo& fileInfo,
                  netStatus* status);

        rlGamerInfo     m_GamerInfo;
        const char*     m_TableId;
        FileInfo        m_FileInfo;
    };

    //Platform-specific implementations of commands.
    bool NativeQuery(const QueryCmd* cmd);
    bool NativeUpload(const UploadCmd* cmd);
    bool NativeDownload(const DownloadCmd* cmd);
    bool NativeDelete(const DeleteCmd* cmd);

    //Asynchronous worker thread.
    struct Worker : public rlWorker
    {
        Worker();

        bool Start(rlPersistentStorage* owner, const char* name);
        bool Stop();

    private:
        //Make start un-callable
        void Start(const char* name);
        virtual void Perform();

        rlPersistentStorage* m_Owner;
    };

    Worker m_Worker;

    //Queue of commands.
    typedef inlist<Command, &Command::m_ListLink> CommandQ;
    CommandQ m_CmdQ;

    //Critical section protecting the command queue.
    sysSpinCriticalSection m_CsCmdQ;

    bool m_Initialized  : 1;
};

}   //namespace rage

#endif  //RLINE_rlPersistentStorage_H
