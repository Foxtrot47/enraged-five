// 
// rline/rlNpWebApi.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS

#include <sdk_version.h>
#include "rlnpwebapi.h"

#include "diag/seh.h"
#include "file/limits.h"
#include "rline/rl.h"
#include "rline/rlnp.h"
#include "rline/rlnpfaulttolerance.h"
#include "rline/rltitleid.h"
#include "rline/rlfriendsmanager.h"
#include "rline/rlSession.h"
#include "rline/rltask.h"
#include "string/stringutil.h"
#include "string/unicode.h"
#include "system/memops.h"
#include "system/param.h"
#include "system/threadpool.h"
#include "rline/rlnpfaulttolerance.h"

#include <np.h>
#include <libhttp.h>
#include <libssl.h>
#pragma comment(lib, "libSceNpWebApi_stub_weak.a")
#pragma comment(lib, "libSceSsl_stub_weak.a")
#pragma comment(lib, "libSceHttp_stub_weak.a")

#define NP_WEBAPI_NET_HEAP_NAME "NpWebApi"
#define NP_WEBAPI_NET_HEAP_SIZE	(16 * 1024)
#define NP_WEBAPI_SSL_HEAP_SIZE	(512 * 1024)
#define NP_WEBAPI_HTTP_HEAP_SIZE (48 * 1024)
#define NP_WEBAPI_HEAP_SIZE (48 * 1024)

#define NP_API_GROUP_USERPROFILE "userProfile"

#define NP_PUSH_EVENT_FRIEND_UPDATE "np:service:friendlist:friend"
#define NP_PUSH_EVENT_PRESENCE_ONLINE_STATUS "np:service:presence:onlineStatus"
#define NP_PUSH_EVENT_PRESENCE_GAME_TITLE_INFO "np:service:presence:gameTitleInfo"
#define NP_PUSH_EVENT_PRESENCE_GAME_STATUS "np:service:presence:gameStatus"
#define NP_PUSH_EVENT_PRESENCE_GAME_DATA "np:service:presence:gameData"
#define NP_PUSH_EVENT_BLOCKLIST_UPDATE "np:service:blocklist"

#if !__NO_OUTPUT
PARAM(rlWebApiCheckHeap, "Runs high watermark checks on the WebAPI heap every frame");
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, npwebapi);
#undef __rage_channel
#define __rage_channel rline_npwebapi

extern const rlTitleId* g_rlTitleId;
extern sysThreadPool netThreadPool;

int rlNpWebApi::sm_FriendUpdateFilterIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApi::sm_FriendUpdateCallbackIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApi::sm_BlocklistUpdateFilterIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApi::sm_BlocklistUpdateCallbackIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApi::sm_PresenceFilterIds[RL_MAX_LOCAL_GAMERS] = {0};
int rlNpWebApi::sm_PresenceCallbackIds[RL_MAX_LOCAL_GAMERS] = {0};
sysCriticalSectionToken rlNpWebApi::sm_TaskCs;
atFixedArray<rlTaskBase*, rlNpWebApi::MAX_QUEUED_TASKS> rlNpWebApi::sm_QueuedTask;

////////////////////////////////////////////////////////////////
// RLNP_WEBAPI_CREATEREQUEST - Configures a work item and queue it up
////////////////////////////////////////////////////////////////
#define RLNP_WEBAPI_CREATEREQUEST(workItem, localGamerIndex, ...)							\
if (!workItem.Configure(localGamerIndex, __VA_ARGS__))										\
{																							\
	rlError("%s->Configure() failed", workItem.GetWorkItemName());							\
	return false;																			\
}																							\
\
if (!netThreadPool.QueueWork(&workItem))													\
{																							\
	rlError("netThreadPool failed to queue work item");										\
	return false;																			\
}																							\
return true;

////////////////////////////////////////////////////////////////
// RLNP_CCP_WEBAPI_WORK_ITEM - Configures a work item and queue it up
////////////////////////////////////////////////////////////////
#define RLNP_CCP_WEBAPI_WORK_ITEM(WorkItemType)												\
virtual rlNpWebApiWorkItemBase* GetWorkItem() { return &m_WorkItem; }						\
virtual bool CreateRequest()																\
{																							\
	if (!netThreadPool.QueueWork(&m_WorkItem))												\
	{																						\
		rlError("netThreadPool failed to queue %s", #WorkItemType);							\
		return false;																		\
	}																						\
	return true;																			\
}																							\
WorkItemType m_WorkItem

////////////////////////////////////////////////////////////////
// RLNP_WEBAPI_CREATE_TASK - Create a WebAPI netTask with a given status, or Fire and Forget if NULL
////////////////////////////////////////////////////////////////
#define RLNP_WEBAPI_CREATE_TASK(T, status, localGamerIndex, ...)							\
if (!Prepare(localGamerIndex))																\
{																							\
	return false;																			\
}																							\
\
if (status == NULL)																			\
{																							\
	netFireAndForgetTask<T>* task;															\
	if(!netTask::Create(&task)																\
	|| !task->Configure(localGamerIndex, __VA_ARGS__)										\
	|| !netTask::Run(task))																	\
	{																						\
		netTask::Destroy(task);																\
		return false;																		\
	}																						\
	else																					\
	{																						\
		return true;																		\
	}																						\
}																							\
else																						\
{																							\
	T* task;																				\
	rtry																					\
	{																						\
		rverify(netTask::Create(&task, status), catchall, );								\
		rverify(task->Configure(localGamerIndex, __VA_ARGS__), catchall, );					\
		rverify(netTask::Run(task), catchall, );											\
		return true;																		\
	}																						\
	rcatchall																				\
	{																						\
		netTask::Destroy(task);																\
		return false;																		\
	}																						\
}

class rlNpWebApiNetTask : public netTask
{
public:
	NET_TASK_DECL(rlNpWebApiNetTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	rlNpWebApiNetTask()
		: m_State(STATE_BEGIN_REQUEST)
		, m_LocalGamerIndex(RL_INVALID_GAMER_INDEX)
		, m_AccountId(RL_INVALID_NP_ACCOUNT_ID)
#if RSG_BANK
		, m_TaskHash(0)
#endif
	{
	}

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId)
	{
		rlAssert(accountId != RL_INVALID_NP_ACCOUNT_ID);
		m_LocalGamerIndex = localGamerIndex;
		m_AccountId = accountId;
		m_WebApiUserCtxId = webApiUserCtxId;
		return true;
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		FAULT_TOLERANCE_CANCEL(GetTaskHash(), GetTaskId());
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_BEGIN_REQUEST:
			if(WasCanceled())
			{
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else if(CreateRequest())
			{
				m_State = STATE_WAIT_REQUEST;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;

		case STATE_WAIT_REQUEST:
			if (GetWorkItem() && GetWorkItem()->Finished())
			{
				if (WasCanceled())
				{
					ProcessCancel();
					status = NET_TASKSTATUS_FAILED;
				}
				else if (GetWorkItem()->Succeeded())
				{
					netTaskDebug("%s succeeded", GetWorkItem()->GetWorkItemName());
					if (ProcessSuccess())
					{
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				else if (GetWorkItem()->Failed())
				{
					netTaskDebug("%s failed", GetWorkItem()->GetWorkItemName());
					ProcessFailure();
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		}

		return status;
	}

protected:
	virtual bool CreateRequest()
	{
		netTaskDebug("CreateRequest...");
		
		if (!GetWorkItem()->Configure(m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId))
		{
			netTaskError("%s->Configure() failed", GetWorkItem()->GetWorkItemName());
			return false;
		}

		if (!netThreadPool.QueueWork(GetWorkItem()))
		{
			netTaskError("netThreadPool failed to queue WorkItem");
			return false;
		}

		return true;
	}

	virtual rlNpWebApiWorkItem* GetWorkItem()
	{
		rlError("GetWorkItem not defined for rlNPWebApiWorkItem");
		return NULL;
	}

	virtual void OnCancel()
	{
        netTaskDebug("OnCancel :: State: %d", m_State);

		if (GetWorkItem() && GetWorkItem()->Pending())
		{
			netTaskDebug("OnCancel :: Cancelling work item");
			netThreadPool.CancelWork(GetWorkItem()->GetId());

			// this cancels the status
			GetWorkItem()->OnCanceled();
		}
	}

	virtual bool ProcessSuccess() 
	{
		if (GetWorkItem())
		{
			netTaskDebug("ProcessSuccess :: Processing success on work item...");
			return GetWorkItem()->ProcessSuccess();
		}

		return false;
	};

	virtual void ProcessFailure() {};
	virtual void ProcessCancel() {};
	virtual u32 GetTaskHash() { return 0; }

protected:
	enum State
	{
		STATE_BEGIN_REQUEST,
		STATE_WAIT_REQUEST
	};

	State m_State;

	int m_LocalGamerIndex;
	rlSceNpAccountId m_AccountId;
	int m_WebApiUserCtxId;

#if RSG_BANK
	int m_TaskHash;
#endif
};

class rlNpGetProfileTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetProfileTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getProfileWorkItem; }
	rlNpGetProfileWorkItem getProfileWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetProfile", 0x91AA9A78); }
#endif
};

class rlNpDeleteGameDataTask : public rlNpWebApiNetTask 
{
	NET_TASK_DECL(rlNpDeleteGameDataTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &deleteGameDataWorkItem; }
	rlNpDeleteGameDataWorkItem deleteGameDataWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpDeleteGameData", 0x6E46A17A); }
#endif
};

class rlNpDeleteGameStatusTask : public rlNpWebApiNetTask 
{
public:
	NET_TASK_DECL(rlNpDeleteGameStatusTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &deleteGameStatusWorkItem; }
	rlNpDeleteGameStatusWorkItem deleteGameStatusWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpDeleteGameStatus", 0xC31C772); }
#endif
};

class rlNpGetPresenceTask : public rlNpWebApiNetTask 
{
	NET_TASK_DECL(rlNpGetPresenceTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

public:
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpAccountId targetAccountId, rlSceNpOnlineId targetOnlineId, bool bIsLocalUserPresence)
	{
		m_bIsLocalUserPresence = bIsLocalUserPresence;
		m_TargetAccountId = targetAccountId;
		m_TargetOnlineId = targetOnlineId;

		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(getPresenceWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_TargetAccountId, m_TargetOnlineId, m_bIsLocalUserPresence);
	}

private:
	bool m_bIsLocalUserPresence;
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getPresenceWorkItem; }
	rlNpGetPresenceWorkItem getPresenceWorkItem;

	rlSceNpAccountId m_TargetAccountId;
	rlSceNpOnlineId m_TargetOnlineId;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetPresence", 0x48AF0365); }
#endif
};

class rlNpPutGameDataBlobTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPutGameDataBlobTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

public:
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const void* blob, int blobLength)
	{
		rtry
		{
			rverify(blobLength < RL_PRESENCE_MAX_BUF_SIZE, catchall, );

			m_BlobPtr = (void*)blob;
			m_BlobLength = blobLength;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(putGameDataBlobWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_BlobPtr, m_BlobLength);
	}

private:
	void* m_BlobPtr;
	int m_BlobLength;
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &putGameDataBlobWorkItem; }
	rlNpPutGameDataBlobWorkItem putGameDataBlobWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPutGameDataBlob", 0xC12894AD); }
#endif
};

class rlNpPutGameStatusTask : public rlNpWebApiNetTask 
{
public:
	NET_TASK_DECL(rlNpPutGameStatusTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

public:
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * gameStatus)
	{
		rtry
		{
			rverify(strlen(gameStatus) < SCE_NP_GAME_STATUS_MAX_BUF_SIZE, catchall, );
			safecpy(gameStatusBuf, gameStatus);
			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(putGameStatusWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, gameStatusBuf);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &putGameStatusWorkItem; }
	rlNpPutGameStatusWorkItem putGameStatusWorkItem;
	char gameStatusBuf[SCE_NP_GAME_STATUS_MAX_BUF_SIZE];

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPutGameStatus", 0x68264E13); }
#endif
};

class rlNpPutGamePresenceTask : public rlNpWebApiNetTask 
{
public:
	NET_TASK_DECL(rlNpPutGamePresenceTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

public:
	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char * gameStatus, const void* gameData, int gameDataLength)
	{
		rtry
		{
			rverify(gameDataLength < RL_PRESENCE_MAX_BUF_SIZE, catchall, );
			rverify(strlen(gameStatus) < SCE_NP_GAME_STATUS_MAX_BUF_SIZE, catchall, );

			safecpy(gameStatusBuf, gameStatus);
			m_BlobPtr = (void*)gameData;
			m_BlobLength = gameDataLength;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(putGamePresenceWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, gameStatusBuf, m_BlobPtr, m_BlobLength);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &putGamePresenceWorkItem; }
	rlNpPutGamePresenceWorkItem putGamePresenceWorkItem;

	void* m_BlobPtr;
	int m_BlobLength;
	char gameStatusBuf[SCE_NP_GAME_STATUS_MAX_BUF_SIZE];

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPutGamePresence", 0x7CF85378); }
#endif
};

class rlNpGetBlocklistTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetBlocklistTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getBlocklistWorkItem; }
	rlNpGetBlocklistWorkItem getBlocklistWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetBlocklist", 0x4514B777); }
#endif
};

class rlNpPostSessionTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPostSessionTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const char* sessionImagePath, rlSession* session)
	{
		m_ImagePtr = NULL;
		safecpy(m_ImagePath, sessionImagePath);
		m_SessionPtr = session;

		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, uint8_t* imagePtr, int imageLen, rlSession* session)
	{
		m_ImagePtr = NULL;
		m_SessionPtr = session;

		sysMemSet(&m_ImagePath, 0, sizeof(m_ImagePath));
		m_ImagePtr = imagePtr;
		m_ImageLen = imageLen;

		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		if (m_ImagePtr)
		{
			RLNP_WEBAPI_CREATEREQUEST(postSessionWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_ImagePtr, m_ImageLen, m_SessionPtr);
		}
		else
		{
			RLNP_WEBAPI_CREATEREQUEST(postSessionWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_ImagePath, m_SessionPtr);
		}
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &postSessionWorkItem; }
	rlNpPostSessionWorkItem postSessionWorkItem;

	rlSession* m_SessionPtr;
	char m_ImagePath[RAGE_MAX_PATH];
	uint8_t* m_ImagePtr;
	int m_ImageLen;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPostSession", 0xDC0A27A3); }
#endif
};

class rlNpGetProfilesTask : public netTask
{
public:
	NET_TASK_DECL(rlNpGetProfilesTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlGamerHandle* handles, unsigned numHandles, rlDisplayName* displayNames, rlGamertag* gamertags, unsigned flags)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(handles, catchall, );
			rverify(numHandles > 0, catchall, );
			rverify(displayNames || gamertags, catchall, );
			rverify(numHandles < rlNpGetProfilesWorkItem::MAX_GETPROFILES_ONLINEIDS, catchall, rlError("Only 100 (rlNpGetProfilesWorkItem::MAX_GETPROFILES_ONLINEIDS) handles supported."));

			m_LocalGamerIndex = localGamerIndex;
			m_AccountId = accountId;
			m_WebApiUserCtxId = webApiUserCtxId;

			m_GamerHandles = handles;
			m_NumGamerHandles = numHandles;
			m_DisplayNames = displayNames;
			m_GamerTags = gamertags;
			m_Flags = flags;

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);

		if (getProfilesWorkItem.Pending())
		{
			netThreadPool.CancelWork(getProfilesWorkItem.GetId());
			getProfilesWorkItem.OnCanceled();
		}

		if (getAdditionalProfilesWorkItem.Pending())
		{
			netThreadPool.CancelWork(getAdditionalProfilesWorkItem.GetId());
			getAdditionalProfilesWorkItem.OnCanceled();
		}
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_GET_PROFILES:
			if (WasCanceled())
			{
				status = NET_TASKSTATUS_FAILED;
			}
			else if (GetProfiles() && GetAdditionalProfiles())
			{
				m_State = STATE_GETTING_PROFILES;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_GETTING_PROFILES:
			{
				// Wait for subtasks to finish
				if (getProfilesWorkItem.Finished() && !getAdditionalProfilesWorkItem.Pending())
				{
					if (WasCanceled())
					{
						status = NET_TASKSTATUS_FAILED;
					}
					else if (getProfilesWorkItem.Succeeded())
					{
						getProfilesWorkItem.ProcessSuccess();
						if (getAdditionalProfilesWorkItem.Succeeded())
						{
							getAdditionalProfilesWorkItem.ProcessSuccess();
							status = NET_TASKSTATUS_SUCCEEDED;
						}
						else if (getAdditionalProfilesWorkItem.Failed())
						{
							status = NET_TASKSTATUS_FAILED;
						}
						else
						{
							status = NET_TASKSTATUS_SUCCEEDED;
						}
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}

					m_State = STATE_FINISHED;
				}
			}
			break;
		default:
			break;
		}

		return status;
	}

	// Sony only allows 50 at a time, whereas Microsoft allows 100. To keep upper level code nice and neat, we do two batches of 50
	// down here so that there can remain a common interface
	virtual bool GetProfiles()
	{
		int filter = rlNpGetProfilesWorkItem::PF_USER | rlNpGetProfilesWorkItem::PF_PERSONAL;
		int numHandles = m_NumGamerHandles;
		if (numHandles > rlNpGetProfilesWorkItem::MAX_GETPROFILES_ONLINEIDS_BATCH)
		{
			numHandles = rlNpGetProfilesWorkItem::MAX_GETPROFILES_ONLINEIDS_BATCH;
		}

		RLNP_WEBAPI_CREATEREQUEST(getProfilesWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_GamerHandles, 0,
			numHandles, m_DisplayNames, m_GamerTags, filter, m_Flags);
	}

	// This is called only if we're requesting more than the first 50 (max) handles
	virtual bool GetAdditionalProfiles()
	{
		if (m_NumGamerHandles > rlNpGetProfilesWorkItem::MAX_GETPROFILES_ONLINEIDS_BATCH)
		{
			int filter = rlNpGetProfilesWorkItem::PF_USER | rlNpGetProfilesWorkItem::PF_PERSONAL;
			RLNP_WEBAPI_CREATEREQUEST(getAdditionalProfilesWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_GamerHandles, rlNpGetProfilesWorkItem::MAX_GETPROFILES_ONLINEIDS_BATCH,
				m_NumGamerHandles, m_DisplayNames, m_GamerTags, filter, m_Flags);
		}

		return true;
	}

private:

	enum State
	{
		STATE_GET_PROFILES,
		STATE_GETTING_PROFILES,
		STATE_FINISHED
	};

	State m_State;

	rlNpGetProfilesWorkItem getProfilesWorkItem;
	rlNpGetProfilesWorkItem getAdditionalProfilesWorkItem;

	int m_LocalGamerIndex;
	rlSceNpAccountId m_AccountId;
	int m_WebApiUserCtxId;

	unsigned m_NumGamerHandles;
	const rlGamerHandle* m_GamerHandles;
	rlDisplayName* m_DisplayNames;
	rlGamertag* m_GamerTags;
	unsigned m_Flags;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetProfiles", 0x8F4CF537); }
#endif
};

class rlNpGetSessionsListTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetSessionsListTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiSessionDetail* sessions, int maxNumSessions, int * numSessions)
	{
		m_SessionDetailsPtr = sessions;
		m_MaxSessions = maxNumSessions;
		m_NumSessionsPtr = numSessions;

		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(getSessionsWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_MaxSessions);
	}

protected:
	virtual bool ProcessSuccess()
	{
		rtry
		{
			rverify(getSessionsWorkItem.ProcessSuccess(), catchall, );
			getSessionsWorkItem.GetSessions(m_SessionDetailsPtr, m_NumSessionsPtr);
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getSessionsWorkItem; }
	rlNpGetSessionListWorkItem getSessionsWorkItem;

	rlNpWebApiSessionDetail* m_SessionDetailsPtr;
	int m_MaxSessions;
	int* m_NumSessionsPtr;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetSessionList", 0xA9CF464E); }
#endif
};

class rlNpDeleteSessionTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpDeleteSessionTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& session)
	{
		m_SessionId = session;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(deleteSessionWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &deleteSessionWorkItem; }
	rlSceNpSessionId m_SessionId;
	rlNpDeleteSessionWorkItem deleteSessionWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpDeleteSession", 0x34291C); }
#endif
};

class rlNpGetSessionDataTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetSessionDataTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& session, rlSessionInfo* outInfo)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(outInfo, catchall, );

			m_SessionId = session;
			m_SessionInfoPtr = outInfo;

			rverify(rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId), catchall, );
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(getSessionDataWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId);
	}

protected:

	virtual bool ProcessSuccess()
	{
		rtry
		{
			rverify(getSessionDataWorkItem.ProcessSuccess(), catchall, );
			rverify(getSessionDataWorkItem.GetSessionData(m_SessionInfoPtr), catchall, );
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getSessionDataWorkItem; }
	rlSceNpSessionId m_SessionId;
	rlSessionInfo* m_SessionInfoPtr;
	rlNpGetSessionDataWorkItem getSessionDataWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetSessionDataTask", 0xC1519E6B); }
#endif
};


class rlNpPutChangeableDataTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPutChangeableDataTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSession* session)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(session, catchall, );
			m_SessionPtr = session;
			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{	
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(putChangeableDataWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionPtr);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &putChangeableDataWorkItem; }
	rlNpPutChangeableDataWorkItem putChangeableDataWorkItem;
	rlSession* m_SessionPtr;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPutChangeableData", 0x8B9B36C); }
#endif
};

class rlNpGetChangeableSessionDataTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetChangeableSessionDataTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& session, rlSessionInfo* outInfo)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(outInfo, catchall, );

			m_SessionId = session;
			m_SessionInfoPtr = outInfo;

			rverify(rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId), catchall, );
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(getChangeableDataWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId);
	}

protected:

	virtual bool ProcessSuccess()
	{
		rtry
		{
			rverify(getChangeableDataWorkItem.ProcessSuccess(), catchall,);
			rverify(getChangeableDataWorkItem.GetChangeableSessionData(m_SessionInfoPtr), catchall,);
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getChangeableDataWorkItem; }
	rlSceNpSessionId m_SessionId;
	rlSessionInfo* m_SessionInfoPtr;
	rlNpGetChangeableDataWorkItem getChangeableDataWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetChangeableSessionData", 0x369B39C4); }
#endif
};

class rlNpPutSessionTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPutSessionTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, const char* sessionName, const char* sessionStatus)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(sessionId.IsValid(), catchall, );
			rverify(strlen(sessionName) <= RL_MAX_SESSION_NAME_LENGTH, catchall, );
			rverify(strlen(sessionStatus) <= RL_MAX_SESSION_STATUS_LENGTH, catchall, );

			m_SessionId = sessionId;
			safecpy(m_SessionName, sessionName);
			safecpy(m_SessionStatus, sessionStatus);

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{	
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(putSessionWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId, m_SessionName, m_SessionStatus);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &putSessionWorkItem; }
	rlNpPutSessionWorkItem putSessionWorkItem;

	char m_SessionName[RL_MAX_SESSION_NAME_BUF_SIZE];
	char m_SessionStatus[RL_MAX_SESSION_STATUS_BUF_SIZE];

	rlSceNpSessionId m_SessionId;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPutSession", 0xB6FA1746); }
#endif
};

class rlNpPutSessionImageTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPutSessionImageTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	rlNpPutSessionImageTask()
		: m_SessionImage(NULL)
		, m_SessionImageLength(0)
	{
		m_SessionId.Clear();
		m_ImagePath[0] = '\0';
	}

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, const u8* sessionImage, const u32 imageLength)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(sessionId.IsValid(), catchall, );
			rverify(sessionImage, catchall, );
			rverify(imageLength <= RL_MAX_SESSION_IMAGE_BUF_SIZE, catchall, );

			m_SessionId = sessionId;
			m_SessionImage = sessionImage;
			m_SessionImageLength = imageLength;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{	
			return false;
		}
	}

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, const char* imagePath)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(sessionId.IsValid(), catchall, );
			rverify(strlen(imagePath) <= RAGE_MAX_PATH, catchall, );

			safecpy(m_ImagePath, imagePath);

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{	
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		if (m_SessionImage != NULL)
		{
			RLNP_WEBAPI_CREATEREQUEST(putSessionImageWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId, m_SessionImage, m_SessionImageLength);
		}
		else
		{
			RLNP_WEBAPI_CREATEREQUEST(putSessionImageWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId, m_ImagePath);
		}
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &putSessionImageWorkItem; }
	rlNpPutSessionImageWorkItem putSessionImageWorkItem;

	const u8* m_SessionImage;
	char m_ImagePath[RAGE_MAX_PATH];
	u32 m_SessionImageLength;
	rlSceNpSessionId m_SessionId;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPutSessionImage", 0xBBE2A3DD); }
#endif
};


class rlNpPostMemberTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPostMemberTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId)
	{
		m_SessionId.Reset(sessionId.AsSceNpSessionId());
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(postMemberWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &postMemberWorkItem; }
	rlSceNpSessionId m_SessionId;
	rlNpPostMemberWorkItem postMemberWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPostMember", 0x97318EE0); }
#endif
};

class rlNpDeleteMemberTask : public rlNpWebApiNetTask
{

public:
	NET_TASK_DECL(rlNpDeleteMemberTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, rlSceNpAccountId targetId)
	{
		rtry
		{
			rverify(accountId != RL_INVALID_NP_ACCOUNT_ID, catchall, );
			rverify(targetId, catchall, );

			m_SessionId.Reset(sessionId.AsSceNpSessionId());
			m_TargetId = targetId;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(deleteMemberWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_SessionId, m_TargetId);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &deleteMemberWorkItem; }
	rlSceNpSessionId m_SessionId;
	rlSceNpAccountId m_TargetId;
	rlNpDeleteMemberWorkItem deleteMemberWorkItem;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpDeleteMember", 0x5E1E5FDB); }
#endif
};

class rlNpDeleteMembersTask : public netTask
{
public:

	NET_TASK_DECL(rlNpDeleteMembersTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, rlGamerHandle* gamerHandles, int numGamers)
	{
		sysMemSet(&m_DeleteMembers, 0, sizeof(m_DeleteMembers));

		m_LocalGamerIndex = localGamerIndex;
		m_AccountId = accountId;
		m_WebApiUserCtxId = webApiUserCtxId;

		m_SessionId.Reset(sessionId.AsSceNpSessionId());
		m_State = STATE_DELETE_MEMBERS;

		m_NumGamers = 0;
		for (int i = 0; i < numGamers && m_NumGamers < RL_MAX_LOCAL_GAMERS; i++)
		{
			if (gamerHandles[i].IsLocal())
			{
				m_DeleteMembers[m_NumGamers].gamerHandle = gamerHandles[i];
				m_NumGamers++;
			}
		}

		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);

		for (int i = 0; i < m_NumGamers; i++)
		{
			if (m_DeleteMembers[i].status.Pending())
			{
				netTask::Cancel(&m_DeleteMembers[i].status);
			}
		}
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_DELETE_MEMBERS:
			for (int i = 0; i < m_NumGamers; i++)
			{
				rlSceNpAccountId accountId = m_DeleteMembers[i].gamerHandle.GetNpAccountId();
				rlAssert(accountId != RL_INVALID_NP_ACCOUNT_ID);

				if (!g_rlNp.GetWebAPI().DeleteMember(m_LocalGamerIndex, m_SessionId, accountId, &m_DeleteMembers[i].status))
				{
					m_DeleteMembers[i].status.SetFailed();
				}
			}

			m_State = STATE_DELETING_MEMBERS;
			break;
		case STATE_DELETING_MEMBERS:
			{
				bool bAllFinished = true;
				for (int i = 0; i < m_NumGamers; i++)
				{
					if (m_DeleteMembers[i].status.Pending())
					{
						bAllFinished = false;
					}
				}

				if (bAllFinished)
				{
					m_State = STATE_FINISHED;
					status = NET_TASKSTATUS_SUCCEEDED;
				}
			}
			break;
		default:
			break;
		}

		return status;
	}

private:

	enum State
	{
		STATE_DELETE_MEMBERS,
		STATE_DELETING_MEMBERS,
		STATE_FINISHED
	};

	State m_State;

	struct DeleteMemberTemplate
	{
		netStatus status;
		rlGamerHandle gamerHandle;
	};

	DeleteMemberTemplate m_DeleteMembers[RL_MAX_LOCAL_GAMERS];
	int m_NumGamers;

	int m_LocalGamerIndex;
	rlSceNpAccountId m_AccountId;
	int m_WebApiUserCtxId;

	rlSceNpSessionId m_SessionId;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpDeleteMembers", 0xFA281E9F); }
#endif

};


class rlNpPostMembersTask : public netTask
{
public:
	NET_TASK_DECL(rlNpPostMembersTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpSessionId& sessionId, rlGamerHandle* gamerHandles, int numGamers)
	{
		sysMemSet(&m_PostMembers, 0, sizeof(m_PostMembers));

		m_LocalGamerIndex = localGamerIndex;
		m_AccountId = accountId;
		m_WebApiUserCtxId = webApiUserCtxId;

		m_SessionId.Reset(sessionId.AsSceNpSessionId());
		m_State = STATE_POST_MEMBERS;

		m_NumGamers = 0;
		for (int i = 0; i < numGamers && m_NumGamers < RL_MAX_LOCAL_GAMERS; i++)
		{
			if (gamerHandles[i].IsLocal())
			{
				m_PostMembers[m_NumGamers].gamerHandle = gamerHandles[i];
				m_NumGamers++;
			}
		}

		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);

		for (int i = 0; i < m_NumGamers; i++)
		{
			if (m_PostMembers[i].status.Pending())
			{
				netTask::Cancel(&m_PostMembers[i].status);
			}
		}
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_POST_MEMBERS:
			for (int i = 0; i < m_NumGamers; i++)
			{
				if (!g_rlNp.GetWebAPI().PostMember(m_PostMembers[i].gamerHandle.GetLocalIndex(), m_SessionId, &m_PostMembers[i].status))
				{
					m_PostMembers[i].status.SetFailed();
				}
			}

			m_State = STATE_POSTING_MEMBERS;
			break;
		case STATE_POSTING_MEMBERS:
			{
				bool bAllFinished = true;
				for (int i = 0; i < m_NumGamers; i++)
				{
					if (m_PostMembers[i].status.Pending())
					{
						bAllFinished = false;
					}
				}

				if (bAllFinished)
				{
					m_State = STATE_FINISHED;
					status = NET_TASKSTATUS_SUCCEEDED;
				}
			}
			break;
		default:
			break;
		}

		return status;
	}

private:

	enum State
	{
		STATE_POST_MEMBERS,
		STATE_POSTING_MEMBERS,
		STATE_FINISHED
	};

	State m_State;

	struct PostMemberTemplate
	{
		netStatus status;
		rlGamerHandle gamerHandle;
	};

	PostMemberTemplate m_PostMembers[RL_MAX_LOCAL_GAMERS];
	int m_NumGamers;

	int m_LocalGamerIndex;
	rlSceNpAccountId m_AccountId;
	int m_WebApiUserCtxId;

	rlSceNpSessionId m_SessionId;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPostMembers", 0x6CFE1DF3); }
#endif
};

class rlNpGetInvitationsTask : public netTask
{
public:
	NET_TASK_DECL(rlNpGetInvitationsTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* invites, int maxNumInvites, int * numInvites)
	{
		// clear out the web api invitation details
		for (int i = 0; i < maxNumInvites; i++)
		{
			memset(&invites[i], 0, sizeof(rlNpWebApiInvitationDetail));
		}

		m_LocalGamerIndex = localGamerIndex;
		m_AccountId = accountId;
		m_WebApiUserCtxId = webApiUserCtxId;

		m_MaxNumInvites = maxNumInvites;
		m_InvitationDetailsPtr = invites;
		m_NumInvitesPtr = numInvites;
		m_State = STATE_GET_LIST;
		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);

		if (m_GetListStatus.Pending())
		{
			netTask::Cancel(&m_GetListStatus);
		}

		for (int i = 0; i < m_MaxNumInvites; i++)
		{
			if (!StringNullOrEmpty(m_InvitationDetailsPtr[i].invitationId))
			{
				if (m_InvitationDetailsPtr[i].m_DataStatus.Pending())
				{
					netTask::Cancel(&m_InvitationDetailsPtr[i].m_DataStatus);
				}
				if (m_InvitationDetailsPtr[i].m_DetailStatus.Pending())
				{
					netTask::Cancel(&m_InvitationDetailsPtr[i].m_DetailStatus);
				}
			}
		}
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		// early out if cancelled
		if (WasCanceled())
		{
			netTaskDebug("Canceled - bailing...");
			return NET_TASKSTATUS_FAILED;
		}

		netTaskStatus status = NET_TASKSTATUS_PENDING;
		bool success = true;

		switch(m_State)
		{
		case STATE_GET_LIST:
			if (g_rlNp.GetWebAPI().GetInvitationList(m_LocalGamerIndex, m_InvitationDetailsPtr, m_MaxNumInvites, m_NumInvitesPtr, &m_GetListStatus))
			{
				m_State = STATE_GETTING_LIST;
			}
			break;
		case STATE_GETTING_LIST:
			{
				if (m_GetListStatus.Succeeded())
				{
					m_State = STATE_GET_DETAILS;
				}
				else if (m_GetListStatus.Canceled() || m_GetListStatus.Failed())
				{
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		case STATE_GET_DETAILS:
			for (int i = 0; i < m_MaxNumInvites; i++)
			{
				// Get details for the valid invitations
				if (m_InvitationDetailsPtr[i].invitationId[0] != '\0' &&
					!g_rlNp.GetWebAPI().GetInvitation(m_LocalGamerIndex, &m_InvitationDetailsPtr[i]))
				{
					success = false;
					break;
				}
			}

			if (success)
			{
				m_State = STATE_GETTING_DETAILS;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}

			break;
		case STATE_GETTING_DETAILS:
			{
				success = true;
				bool bReady = true;
				for (int i = 0; i < m_MaxNumInvites; i++)
				{
					if (m_InvitationDetailsPtr[i].invitationId[0] != '\0')
					{
						if (m_InvitationDetailsPtr[i].m_DetailStatus.Failed() ||
							m_InvitationDetailsPtr[i].m_DetailStatus.Canceled())
						{
							success = false;
						}
						else if (m_InvitationDetailsPtr[i].m_DetailStatus.Pending())
						{
							bReady = false;
						}
					}
				}

				if (!success)
				{
					m_State = STATE_CLEAR_EXPIRED_INVITES_FAILED;
				}
				else if (bReady)
				{
					m_State = STATE_CLEAR_EXPIRED_INVITES;
				}
			}
			break;
		case STATE_CLEAR_EXPIRED_INVITES_FAILED:
			{
				for (int i = 0; i < m_MaxNumInvites; i++)
				{
					if (m_InvitationDetailsPtr[i].expired)
					{
						memset(&m_InvitationDetailsPtr[i], 0, sizeof(m_InvitationDetailsPtr[i]));
						(*m_NumInvitesPtr)--;
					}
				}

				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_CLEAR_EXPIRED_INVITES:
			{
				for (int i = 0; i < m_MaxNumInvites; i++)
				{
					if (m_InvitationDetailsPtr[i].expired)
					{
						memset(&m_InvitationDetailsPtr[i], 0, sizeof(m_InvitationDetailsPtr[i]));
						(*m_NumInvitesPtr)--;
					}
				}

				m_State = STATE_GET_DATA;
			}
			break;
		case STATE_GET_DATA:
			for (int i = 0; i < m_MaxNumInvites; i++)
			{
				if (m_InvitationDetailsPtr[i].invitationId[0] != '\0' && !m_InvitationDetailsPtr[i].expired &&
					!g_rlNp.GetWebAPI().GetInvitationData(m_LocalGamerIndex, &m_InvitationDetailsPtr[i]))
				{
					success = false;
					break;
				}
			}

			if (success)
			{
				m_State = STATE_GETTING_DATA;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_GETTING_DATA:
			{
				success = true;
				bool bReady = true;
				for (int i = 0; i < m_MaxNumInvites; i++)
				{
					if (m_InvitationDetailsPtr[i].invitationId[0] != '\0' && !m_InvitationDetailsPtr[i].expired)
					{
						if (m_InvitationDetailsPtr[i].m_DataStatus.Failed() || m_InvitationDetailsPtr[i].m_DataStatus.Canceled())
						{
							success = false;
						}
						else if (m_InvitationDetailsPtr[i].m_DataStatus.Pending())
						{
							bReady = false;
						}
					}
				}

				if (!success)
				{
					status = NET_TASKSTATUS_FAILED;
				}
				else if (bReady)
				{
					m_State = STATE_FINISHED;
					status = NET_TASKSTATUS_SUCCEEDED;
				}
			}
			break;
		default:
			break;
		}

		return status;
	}

private:
	enum State
	{
		STATE_GET_LIST,
		STATE_GETTING_LIST,
		STATE_GET_DETAILS,
		STATE_GETTING_DETAILS,
		STATE_CLEAR_EXPIRED_INVITES_FAILED,
		STATE_CLEAR_EXPIRED_INVITES,
		STATE_GET_DATA,
		STATE_GETTING_DATA,
		STATE_FINISHED
	};

	State m_State;

	int m_LocalGamerIndex;
	rlSceNpAccountId m_AccountId;
	int m_WebApiUserCtxId;

	// Status Objects
	netStatus m_GetListStatus;

	rlNpWebApiInvitationDetail* m_InvitationDetailsPtr;
	int m_MaxNumInvites;
	int* m_NumInvitesPtr; 

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetInvitations", 0x28F8E89B); }
#endif
};

class rlNpGetInvitationsListTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetInvitationsListTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* details, int maxNumInvites, int * numInvites)
	{
		m_InvitationDetailsPtr = details;
		m_MaxInvites = maxNumInvites;
		m_NumInvitesPtr = numInvites;

		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(getInvitationsWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_MaxInvites);
	}

protected:
	virtual bool ProcessSuccess()
	{
		rtry
		{
			rverify(getInvitationsWorkItem.ProcessSuccess(), catchall, );
			getInvitationsWorkItem.GetInvitations(m_InvitationDetailsPtr, m_NumInvitesPtr);
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getInvitationsWorkItem; }
	rlNpGetInvitationListWorkItem getInvitationsWorkItem;

	rlNpWebApiInvitationDetail* m_InvitationDetailsPtr;
	int m_MaxInvites;
	int* m_NumInvitesPtr; 

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetInvitationsList", 0xE3A95362); }
#endif
};

class rlNpGetInvitationTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetInvitationTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* detail)
	{
		m_InvitationDetailPtr = detail;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(getInvitationWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_InvitationDetailPtr);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getInvitationWorkItem; }
	rlNpGetInvitationWorkItem getInvitationWorkItem;

	rlNpWebApiInvitationDetail* m_InvitationDetailPtr;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetInvitation", 0x6C17EA03); }
#endif
};

class rlNpGetInvitationDataTask : public rlNpWebApiNetTask
{

public:
	NET_TASK_DECL(rlNpGetInvitationDataTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* detail)
	{
		m_InvitationDetailPtr = detail;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		if (!getInvitationDataWorkItem.Configure(m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_InvitationDetailPtr))
			return false;

		if (!netThreadPool.QueueWork(&getInvitationDataWorkItem))
		{
			rlError("netThreadPool failed to queue getInvitationDataWorkItem");
			return false;
		}
		return true;
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &getInvitationDataWorkItem; }
	rlNpGetInvitationDataWorkItem getInvitationDataWorkItem;

	rlNpWebApiInvitationDetail* m_InvitationDetailPtr;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetInvitationData", 0x158A6031); }
#endif
};

class rlNpClearInvitationTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpClearInvitationTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiInvitationDetail* detail)
	{
		m_InvitationDetailPtr = detail;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		if (!putInvitationWorkItem.Configure(m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_InvitationDetailPtr->invitationId, true))
			return false;

		if (!netThreadPool.QueueWork(&putInvitationWorkItem))
		{
			rlError("netThreadPool failed to queue putInvitationWorkItem");
			return false;
		}
		return true;
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &putInvitationWorkItem; }
	rlNpPutInvitationWorkItem putInvitationWorkItem;

	rlNpWebApiInvitationDetail* m_InvitationDetailPtr;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpClearInvitation", 0x6914B8E7); }
#endif
};

class rlNpPostInvitationTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPostInvitationTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpWebApiPostInvitationParam* invite)
	{
		if (invite == nullptr)
			return false;

		// Make a copy of the invitation params
		m_SessionInfo = *invite->m_SessionInfo;
		m_SessionId = *invite->m_SessionId;
		safecpy(m_MessageBuf, invite->m_Message);

        m_NumAccountIds = 0;
        m_NumOnlineIds = 0;

		if(invite->m_NumAccountIds > 0)
		{
			m_NumAccountIds = invite->m_NumAccountIds;
			for (int i = 0; i < m_NumAccountIds; i++)
			{
				m_AccountIds[i] = invite->m_AccountIds[i];
			}
		}
		else
		{
			m_NumOnlineIds = invite->m_NumOnlineIds;
			for (int i = 0; i < m_NumOnlineIds; i++)
			{
				m_OnlineIds[i] = invite->m_OnlineIds[i];
			}
		}

		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		// Create new invitation params for the work item.
		rlNpWebApiPostInvitationParam param;
		sysMemSet(&param, 0, sizeof(param));
		param.m_SessionInfo = &m_SessionInfo;
		param.m_SessionId = &m_SessionId;
		param.m_AccountIds = m_AccountIds;
		param.m_NumAccountIds = m_NumAccountIds;
		param.m_Message = m_MessageBuf;
		param.m_OnlineIds = m_OnlineIds;
		param.m_NumOnlineIds = m_NumOnlineIds;

		RLNP_WEBAPI_CREATEREQUEST(postInvitationWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, &param);
	}

private:

	virtual rlNpWebApiWorkItem* GetWorkItem() { return &postInvitationWorkItem; }
	rlNpPostInvitationWorkItem postInvitationWorkItem;

	rlSessionInfo m_SessionInfo;
	rlSceNpSessionId m_SessionId;
	rlSceNpAccountId m_AccountIds[RL_MAX_GAMERS_PER_SESSION];
	unsigned m_NumAccountIds;
	rlSceNpOnlineId m_OnlineIds[RL_MAX_GAMERS_PER_SESSION];
	unsigned m_NumOnlineIds;
	char m_MessageBuf[SCE_INVITATION_DIALOG_MAX_USER_MSG_SIZE];

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPostInvitation", 0xE8380DF3); }
#endif
};

class rlNpGetCatalogTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetCatalogTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetCatalogDetail* detail)
	{
		mp_CatalogRequestDetails = detail;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_GetCatalogWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, mp_CatalogRequestDetails);
	}

private:
	virtual rlNpGetCatalogWorkItem* GetWorkItem() { return &m_GetCatalogWorkItem; }
	rlNpGetCatalogWorkItem m_GetCatalogWorkItem;
	rlNpGetCatalogDetail *mp_CatalogRequestDetails;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetCatalog", 0x72AD9442); }
#endif
};

class rlNpGetProductInfoTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetProductInfoTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetProductDetail* detail)
	{
		mp_ProductRequestDetails = detail;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_GetProductInfoWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, mp_ProductRequestDetails);
	}

private:
	virtual rlNpWebApiWorkItem* GetWorkItem() { return &m_GetProductInfoWorkItem; }
	rlNpGetProductInfoWorkItem m_GetProductInfoWorkItem;
	rlNpGetProductDetail *mp_ProductRequestDetails;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetProductInfo", 0x6a2e8b58); }
#endif
};

class rlNpGetEntitlementsTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetEntitlementsTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetEntitlementsDetail* detail)
	{
		mp_GetEntitlementsDetails = detail;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_GetEntitlementsWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, mp_GetEntitlementsDetails);
	}

private:
	virtual rlNpGetEntitlementsWorkItem* GetWorkItem() { return &m_GetEntitlementsWorkItem; }
	rlNpGetEntitlementsWorkItem m_GetEntitlementsWorkItem;
	rlNpGetEntitlementsDetail *mp_GetEntitlementsDetails;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetEntitlements", 0x9dea8a52); }
#endif
};

class rlNpSetEntitlementLevelTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpSetEntitlementLevelTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpSetEntitlementsDetail* details, const char* entitlementId, int amountToConsume)
	{
		m_AmountToConsume = amountToConsume;
		m_Details = details;
		strncpy(m_EntitlementId, entitlementId, rlNpSetEntitlementCountWorkItem::MAX_ENTITLEMENT_ID_LEN);
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_SetEntitlementsCountWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_Details, m_EntitlementId, m_AmountToConsume);
	}

private:
	virtual rlNpSetEntitlementCountWorkItem* GetWorkItem() { return &m_SetEntitlementsCountWorkItem; }
	rlNpSetEntitlementCountWorkItem m_SetEntitlementsCountWorkItem;
	rlNpSetEntitlementsDetail* m_Details;

	int	m_AmountToConsume;
	char m_EntitlementId[rlNpSetEntitlementCountWorkItem::MAX_ENTITLEMENT_ID_LEN];

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpSetEntitlementLevel", 0x9A7C99C8); }
#endif
};


class rlNpPostActivityFeedStory : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPostActivityFeedStory);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpActivityFeedStory* details)
	{
		m_Details = details;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_PostActivityFeedStoryWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_Details);
	}

private:
	virtual rlNpPostActivityFeedStoryWorkItem* GetWorkItem() { return &m_PostActivityFeedStoryWorkItem; }
	rlNpPostActivityFeedStoryWorkItem m_PostActivityFeedStoryWorkItem;
	rlNpActivityFeedStory* m_Details;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpPostActivityFeedStory", 0x92cd160a); }
#endif
};

class rlNpPostActivityFeedPlayedWith : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpPostActivityFeedPlayedWith);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpActivityFeedPlayedWithDetails* details)
	{
		m_Details = details;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_PostActivityFeedPlayedWithWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_Details);
	}

private:
	virtual rlNpPostActivityFeedPlayedWithWorkItem* GetWorkItem() { return &m_PostActivityFeedPlayedWithWorkItem; }
	rlNpPostActivityFeedPlayedWithWorkItem m_PostActivityFeedPlayedWithWorkItem;
	rlNpActivityFeedPlayedWithDetails* m_Details;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpActivityFeedPlayedWithDetails", 0x87490b34); }
#endif
};


class rlNpGetActivityFeedVideoData : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpGetActivityFeedVideoData);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlNpGetActivityFeedVideoDataDetails* details)
	{
		m_Details = details;
		return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_GetActivityFeedVideoDataWorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_Details);
	}

private:
	virtual rlNpGetActivityFeedVideoDataWorkItem* GetWorkItem() { return &m_GetActivityFeedVideoDataWorkItem; }
	rlNpGetActivityFeedVideoDataWorkItem m_GetActivityFeedVideoDataWorkItem;
	rlNpGetActivityFeedVideoDataDetails* m_Details;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpGetActivityFeedVideoDataDetails", 0x6d4987d6); }
#endif
};

class rlNpIdentityMapperGetAccountIdTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpIdentityMapperGetAccountIdTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpOnlineId& targetOnlineId, rlSceNpAccountId* out_accountId)
	{
		rtry
		{
			rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
			rverifyall(out_accountId);

			m_TargetOnlineId = targetOnlineId;
			m_TargetAccountId = out_accountId;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_WorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_TargetOnlineId, m_TargetAccountId);
	}

private:
	virtual rlNpIdentityMapperGetAccountIdWorkItem* GetWorkItem() { return &m_WorkItem; }
	rlNpIdentityMapperGetAccountIdWorkItem m_WorkItem;

	rlSceNpOnlineId m_TargetOnlineId;
	rlSceNpAccountId* m_TargetAccountId;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpIdentityMapperGetAccountIdTask", 0x907AE568); }
#endif
};

class rlNpIdentityMapperGetAccountIdsTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpIdentityMapperGetAccountIdsTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpOnlineId* targetOnlineIds, const unsigned numIds, rlSceNpAccountId* out_accountIds)
	{
		rtry
		{
			rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
			rverifyall(targetOnlineIds != nullptr);
			rverifyall(numIds > 0);
			rverifyall(numIds <= NP_WEB_API_MAX_MAPPED_IDS);
			rverifyall(out_accountIds != nullptr);

			m_TargetOnlineIds = targetOnlineIds;
			m_NumIds = numIds;
			m_TargetAccountIds = out_accountIds;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_WorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_TargetOnlineIds, m_NumIds, m_TargetAccountIds);
	}

private:
	virtual rlNpIdentityMapperGetAccountIdsWorkItem* GetWorkItem() { return &m_WorkItem; }
	rlNpIdentityMapperGetAccountIdsWorkItem m_WorkItem;

	rlSceNpOnlineId* m_TargetOnlineIds;
	unsigned m_NumIds;
	rlSceNpAccountId* m_TargetAccountIds;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpIdentityMapperGetAccountIdTask", 0x907AE568); }
#endif
};

class rlNpIdentityMapperGetOnlineIdTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpIdentityMapperGetOnlineIdTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, const rlSceNpAccountId targetAccountId, rlSceNpOnlineId* out_onlineId)
	{
		rtry
		{
			rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
			rverifyall(out_onlineId);

			m_TargetAccountId = targetAccountId;
			m_TargetOnlineId = out_onlineId;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_WorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_TargetAccountId, m_TargetOnlineId);
	}

private:
	virtual rlNpIdentityMapperGetOnlineIdWorkItem* GetWorkItem() { return &m_WorkItem; }
	rlNpIdentityMapperGetOnlineIdWorkItem m_WorkItem;

	rlSceNpAccountId m_TargetAccountId;
	rlSceNpOnlineId* m_TargetOnlineId;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpIdentityMapperGetOnlineIdTask", 0x88A537A3); }
#endif
};

class rlNpIdentityMapperGetOnlineIdsTask : public rlNpWebApiNetTask
{
public:
	NET_TASK_DECL(rlNpIdentityMapperGetOnlineIdsTask);
	NET_TASK_USE_CHANNEL(rline_npwebapi);

	bool Configure(const int localGamerIndex, rlSceNpAccountId accountId, int webApiUserCtxId, rlSceNpAccountId* targetAccountIds, const unsigned numIds, rlSceNpOnlineId* out_onlineIds)
	{
		rtry
		{
			rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
			rverifyall(targetAccountIds != nullptr);
			rverifyall(numIds > 0);
			rverifyall(numIds <= NP_WEB_API_MAX_MAPPED_IDS);
			rverifyall(out_onlineIds != nullptr);

			m_TargetAccountIds = targetAccountIds;
			m_NumIds = numIds;
			m_TargetOnlineIds = out_onlineIds;

			return rlNpWebApiNetTask::Configure(localGamerIndex, accountId, webApiUserCtxId);
		}
		rcatchall
		{
			return false;
		}
	}

	virtual bool CreateRequest()
	{
		RLNP_WEBAPI_CREATEREQUEST(m_WorkItem, m_LocalGamerIndex, m_AccountId, m_WebApiUserCtxId, m_TargetAccountIds, m_NumIds, m_TargetOnlineIds);
	}

private:
	virtual rlNpIdentityMapperGetOnlineIdsWorkItem* GetWorkItem() { return &m_WorkItem; }
	rlNpIdentityMapperGetOnlineIdsWorkItem m_WorkItem;

	rlSceNpAccountId* m_TargetAccountIds;
	unsigned m_NumIds;
	rlSceNpOnlineId* m_TargetOnlineIds;

#if RSG_BANK
protected:
	virtual u32 GetTaskHash() { return ATSTRINGHASH("NpIdentityMapperGetOnlineIdTask", 0x88A537A3); }
#endif
};

/////////////////////////////////////////////////////////////////////
// rlNpWebApi
/////////////////////////////////////////////////////////////////////
rlNpWebApi::rlNpWebApi()
	: m_LibNetMemId(0)
	, m_LibSslCtxId(0)
	, m_LibHttpCtxId(0)
	, m_WebApiLibCtxId(0)
#if !__NO_OUTPUT
	, m_MaxSslPoolUsage(0)
	, m_MaxHttpPoolUsage(0)
#endif
{

}

rlNpWebApi::~rlNpWebApi()
{

}

bool rlNpWebApi::Init()
{
	m_LibNetMemId = 0;
	m_LibSslCtxId = 0;
	m_LibHttpCtxId = 0;
	m_WebApiLibCtxId = 0;

	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		m_WebApiUserCtxId[i] = -1;
		m_GamerIndices[i] = i;

		m_bCallbacksRegistered[i] = false;
		sm_FriendUpdateFilterIds[i] = -1;
		sm_FriendUpdateCallbackIds[i] = -1;
		sm_PresenceFilterIds[i] = -1;
		sm_PresenceCallbackIds[i] = -1;
		sm_BlocklistUpdateFilterIds[i] = -1;
		sm_BlocklistUpdateCallbackIds[i] = -1;
	}

	// Create the Net Pool
	int ret = sceNetPoolCreate(NP_WEBAPI_NET_HEAP_NAME, NP_WEBAPI_NET_HEAP_SIZE, 0);
	if (ret < 0)
	{
		rlError("sceNetPoolCreate() failed. ret = 0x%x\n", ret);
		sceNetTerm();
		return false;
	}
	m_LibNetMemId = ret;

	// Initialize Sce SSL Library
	ret = sceSslInit(NP_WEBAPI_SSL_HEAP_SIZE);
	if (ret < 0)
	{
		rlError("sceSslInit() failed. ret = 0x%x\n", ret);
		sceNetTerm();
		return false;
	}
	m_LibSslCtxId = ret;

	// Initialize Sce Http Library
	ret = sceHttpInit(m_LibNetMemId, m_LibSslCtxId, NP_WEBAPI_HTTP_HEAP_SIZE);
	if (ret < 0)
	{
		rlError("sceHttpInit() failed. ret = 0x%x\n", ret);
		ShutdownSceNet();
		return false;
	}

	m_LibHttpCtxId = ret;

	// Initialize the Web API
	ret = sceNpWebApiInitialize(m_LibHttpCtxId, NP_WEBAPI_HEAP_SIZE);
	if (ret < 0) 
	{
		rlAssertf(false, "sceNpWebApiInitialize() failed. ret = 0x%x\n", ret);
		return false;
	}

	m_WebApiLibCtxId = ret;

	return true;
}

void rlNpWebApi::Update()
{
#if !__NO_OUTPUT
	if (PARAM_rlWebApiCheckHeap.Get())
	{
		CheckWebApiHeap();
	}
#endif

	CheckPendingOperations();
}

void rlNpWebApi::Shutdown()
{
	int ret;

	ClearPendingOperations();

	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if (m_WebApiUserCtxId[i] >= 0)
		{
			ret = sceNpWebApiDeleteContext(m_WebApiUserCtxId[i]);
			rlAssertf(ret == SCE_OK, "sceNpWebApiDeleteContext() failed. ret = 0x%x\n", ret);
			m_WebApiUserCtxId[i] = -1;
		}
	}

	ret = sceNpWebApiTerminate(m_WebApiLibCtxId);
	rlAssertf(ret == SCE_OK, "sceNpWebApiTerminate() failed. ret = 0x%x\n", ret);
	m_WebApiLibCtxId = 0;

	ShutdownSceNet();
}

void rlNpWebApi::CheckPendingOperations()
{
	if (sm_QueuedTask.size())
	{
		SYS_CS_SYNC(sm_TaskCs);

		rlDebug1("Adding %u ParallelTasks", sm_QueuedTask.GetCount());

		for (int i = 0; i < sm_QueuedTask.GetCount(); ++i)
		{
			// We might be signed out from PSN at this stage but calling
			// AddParallelTask remains the safest option
			rlGetTaskManager()->AddParallelTask(sm_QueuedTask[i]);
		}

		sm_QueuedTask.clear();
	}
}

void rlNpWebApi::ClearPendingOperations()
{
	SYS_CS_SYNC(sm_TaskCs);

	rlDebug1("Clearing %u ParallelTasks", sm_QueuedTask.GetCount());

	for (int i = 0; i < sm_QueuedTask.GetCount(); ++i)
	{
		rlGetTaskManager()->DestroyTask(sm_QueuedTask[i]);
	}

	sm_QueuedTask.clear();
}

void rlNpWebApi::QueueTask(rlTaskBase* task)
{
	SYS_CS_SYNC(sm_TaskCs);

	rtry
	{
		rlDebug1("Queueing a ParallelTask. current %u", sm_QueuedTask.GetCount());

		rverify(!sm_QueuedTask.IsFull(), catchall, rlError("There's no space to add the task"));
		sm_QueuedTask.Push(task);
	}
	rcatchall
	{
		// This can assert on IsPending but it will still clear the task. We shouldn't really never hit this anyway.
		rlGetTaskManager()->DestroyTask(task);
	}
}

bool rlNpWebApi::RegisterPushEventCallbacks(int localGamerIndex, bool bUnRegister)
{
	rtry
	{
		Prepare(localGamerIndex);

		if (bUnRegister)
		{
			UnregisterFriendUpdateCB(localGamerIndex);
			UnregisterPresenceOnlineStatusCB(localGamerIndex);
			UnregisterBlocklistUpdateCB(localGamerIndex);
		}
		else
		{
			rverify(RegisterFriendUpdateCB(localGamerIndex), catchall, );
			rverify(RegisterPresenceOnlineStatusCB(localGamerIndex), catchall, );
			rverify(RegisterBlocklistUpdateCB(localGamerIndex), catchall, );
		}

		m_RegisterCallbacksWorkItem[localGamerIndex].Configure(localGamerIndex, m_WebApiLibCtxId, m_WebApiUserCtxId[localGamerIndex], bUnRegister);

		if (!netThreadPool.QueueWork(&m_RegisterCallbacksWorkItem[localGamerIndex]))
		{
			rlError("netThreadPool failed to queue rlNpWebApiRegisterCallbacksWorkItem");
			return false;
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlNpWebApi::RegisterFriendUpdateCB(const int localGamerIndex)
{
	if (sm_FriendUpdateFilterIds[localGamerIndex] >= 0)
		return true;

	SceNpWebApiPushEventDataType dataType;
	memset(&dataType, 0, sizeof(dataType));
	snprintf(dataType.val, SCE_NP_WEBAPI_PUSH_EVENT_DATA_TYPE_LEN_MAX, NP_PUSH_EVENT_FRIEND_UPDATE);

	int ret = sceNpWebApiCreatePushEventFilter(m_WebApiLibCtxId, &dataType, 1);
	if (ret < 0)
		return false;
	sm_FriendUpdateFilterIds[localGamerIndex] = ret;

	ret = sceNpWebApiRegisterPushEventCallback(m_WebApiUserCtxId[localGamerIndex], sm_FriendUpdateFilterIds[localGamerIndex], rlNpWebApi::FriendUpdateCB, &m_GamerIndices[localGamerIndex]);
	if (ret < 0)
	{
		sceNpWebApiDeletePushEventFilter(m_WebApiLibCtxId, sm_FriendUpdateFilterIds[localGamerIndex]);
		sm_FriendUpdateFilterIds[localGamerIndex] = -1;
		return false;
	}

	sm_FriendUpdateCallbackIds[localGamerIndex] = ret;

	return true;
}

bool rlNpWebApi::RegisterBlocklistUpdateCB(const int localGamerIndex)
{
	// NP_PUSH_EVENT_BLOCKLIST_UPDATE
	if (sm_BlocklistUpdateFilterIds[localGamerIndex] >= 0)
		return true;

	SceNpWebApiPushEventDataType dataType;
	memset(&dataType, 0, sizeof(dataType));
	snprintf(dataType.val, SCE_NP_WEBAPI_PUSH_EVENT_DATA_TYPE_LEN_MAX, NP_PUSH_EVENT_BLOCKLIST_UPDATE);

	int ret = sceNpWebApiCreatePushEventFilter(m_WebApiLibCtxId, &dataType, 1);
	if (ret < 0)
		return false;
	sm_BlocklistUpdateFilterIds[localGamerIndex] = ret;

	ret = sceNpWebApiRegisterPushEventCallback(m_WebApiUserCtxId[localGamerIndex], sm_BlocklistUpdateFilterIds[localGamerIndex], rlNpWebApi::BlocklistUpdateCB, &m_GamerIndices[localGamerIndex]);
	if (ret < 0)
	{
		sceNpWebApiDeletePushEventFilter(m_WebApiLibCtxId, sm_BlocklistUpdateFilterIds[localGamerIndex]);
		sm_BlocklistUpdateFilterIds[localGamerIndex] = -1;
		return false;
	}

	sm_BlocklistUpdateCallbackIds[localGamerIndex] = ret;

	return true;
}

bool rlNpWebApi::RegisterPresenceOnlineStatusCB(const int localGamerIndex)
{
	if (sm_PresenceFilterIds[localGamerIndex] >= 0)
		return true;

	SceNpWebApiPushEventDataType dataType;
	memset(&dataType, 0, sizeof(dataType));
	snprintf(dataType.val, SCE_NP_WEBAPI_PUSH_EVENT_DATA_TYPE_LEN_MAX, NP_PUSH_EVENT_PRESENCE_ONLINE_STATUS);

	int ret = sceNpWebApiCreatePushEventFilter(m_WebApiLibCtxId, &dataType, 1);
	if (ret < 0)
		return false;
	sm_PresenceFilterIds[localGamerIndex] = ret;

	ret = sceNpWebApiRegisterPushEventCallback(m_WebApiUserCtxId[localGamerIndex], sm_PresenceFilterIds[localGamerIndex], rlNpWebApi::PresenceUpdateCB, &m_GamerIndices[localGamerIndex]);
	if (ret < 0)
	{
		sceNpWebApiDeletePushEventFilter(m_WebApiLibCtxId, sm_PresenceFilterIds[localGamerIndex]);
		sm_PresenceFilterIds[localGamerIndex] = -1;
		return false;
	}

	sm_PresenceCallbackIds[localGamerIndex] = ret;

	return true;
}

void rlNpWebApi::UnregisterFriendUpdateCB(const int localGamerIndex)
{
	sceNpWebApiUnregisterServicePushEventCallback(m_WebApiLibCtxId, sm_FriendUpdateFilterIds[localGamerIndex]);
	sceNpWebApiDeletePushEventFilter(m_WebApiLibCtxId, sm_FriendUpdateCallbackIds[localGamerIndex]);
}

void rlNpWebApi::UnregisterBlocklistUpdateCB(const int localGamerIndex)
{
	sceNpWebApiUnregisterServicePushEventCallback(m_WebApiLibCtxId, sm_BlocklistUpdateFilterIds[localGamerIndex]);
	sceNpWebApiDeletePushEventFilter(m_WebApiLibCtxId, sm_BlocklistUpdateCallbackIds[localGamerIndex]);
}

void rlNpWebApi::UnregisterPresenceOnlineStatusCB(const int localGamerIndex)
{
	sceNpWebApiUnregisterServicePushEventCallback(m_WebApiLibCtxId, sm_PresenceCallbackIds[localGamerIndex]);
	sceNpWebApiDeletePushEventFilter(m_WebApiLibCtxId, sm_PresenceFilterIds[localGamerIndex]);
}


class rlNpGetAccountIdForFriendPresence : public rlTaskBase
{
public:

	RL_TASK_DECL(rlNpGetAccountIdForFriendPresence);

	enum ePurpose
	{
		UPDATE_PRESENCE,
		REFRESH_PRESENCE
	};

	rlNpGetAccountIdForFriendPresence()
	{
		m_LocalGamerIndex = RL_INVALID_GAMER_INDEX;
		m_AccountId = RL_INVALID_NP_ACCOUNT_ID;
		m_OnlineId = rlSceNpOnlineId::RL_INVALID_ONLINE_ID;
	}

	bool Configure(const int localGamerIndex, const rlSceNpOnlineId& onlineId, bool bIsOnline, bool bIsInSameTitle, const void *data, const unsigned dataSize, bool bRaiseEvent, ePurpose purpose)
	{
		if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
			return false;

		if (!onlineId.IsValid())
			return false;

		m_LocalGamerIndex = localGamerIndex;
		m_OnlineId = onlineId;
		m_IsOnline = bIsOnline;
		m_IsInSameTitle = bIsInSameTitle;
		m_RaiseEvent = bRaiseEvent;
		m_PresenceDataLength = dataSize;
		if (m_PresenceDataLength > 0)
		{
			sysMemCpy(m_PresenceData, data, m_PresenceDataLength);
		}
		m_Purpose = purpose;

		return true;
	}

	virtual void Start()
	{
		this->rlTaskBase::Start();

		// create a temporary gamer handle using environment, invalid ID and our target online id
		rlGamerHandle hTemp;
		hTemp.ResetNp(g_rlNp.GetEnvironment(), RL_INVALID_NP_ACCOUNT_ID, &m_OnlineId);
		
		// see if we can find a friend with this id.
		int friendIndex = rlFriendsManager::GetFriendIndex(m_LocalGamerIndex, hTemp);
		if (friendIndex != rlFriendsManager::INVALID_FRIEND_INDEX)
		{
			m_MapperStatus.SetPending();

			// retrieve the account ID from friends.
			rlGamerHandle hGamerFull;
			if (rlFriendsManager::GetGamerHandle(friendIndex, &hGamerFull))
			{
				m_AccountId = hGamerFull.GetNpAccountId();
				m_MapperStatus.SetSucceeded();
			}
			else
			{
				m_MapperStatus.SetFailed();
			}
		}
		else
		{
			g_rlNp.GetWebAPI().GetAccountId(m_LocalGamerIndex, m_OnlineId, &m_AccountId, &m_MapperStatus);
		}
	}

	virtual void Finish(const FinishType finishType, const int resultCode = 0)
	{
		this->rlTaskBase::Finish(finishType, resultCode);
		if (finishType == rlTaskBase::FINISH_SUCCEEDED)
		{
			rlGamerHandle gamer;
			gamer.ResetNp(g_rlNp.GetEnvironment(), m_AccountId, &m_OnlineId);

			if (m_Purpose == UPDATE_PRESENCE)
			{
				rlFriendsManager::UpdateFriendPresence(m_LocalGamerIndex, m_AccountId, m_OnlineId, m_IsOnline, m_IsInSameTitle, m_PresenceData, m_PresenceDataLength, m_RaiseEvent);
			}
			else if (m_Purpose == REFRESH_PRESENCE)
			{
				g_rlNp.GetWebAPI().GetPresence(m_LocalGamerIndex, gamer);
			}
		}
	}

	virtual void Update(const unsigned timeStep)
	{
		this->rlTaskBase::Update(timeStep);
		if (!m_MapperStatus.Pending())
		{
			Finish(m_MapperStatus.Succeeded() ? rlTaskBase::FINISH_SUCCEEDED : rlTaskBase::FINISH_FAILED);
		}
	}

private:

	int m_LocalGamerIndex;
	rlSceNpAccountId m_AccountId;
	rlSceNpOnlineId m_OnlineId;
	netStatus m_MapperStatus;

	bool m_IsOnline;
	bool m_IsInSameTitle;
	bool m_RaiseEvent;
	u8 m_PresenceData[RL_PRESENCE_MAX_BUF_SIZE];
	unsigned m_PresenceDataLength;

	ePurpose m_Purpose;
};

void rlNpWebApi::FriendUpdateCB(int32_t userCtxId, int32_t callbackId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom,
								const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg)
{
	int* gamerIndex = (int*)pUserArg;
	if (gamerIndex != NULL)
	{
		rlNpEventFriendListUpdate e(*gamerIndex);
		g_rlNp.DispatchEvent(&e);
	}
}

void rlNpWebApi::PresenceUpdateCB(int32_t userCtxId, int32_t callbackId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom,
								  const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg)
{
	int* gamerIndex = (int*)pUserArg;
	if (gamerIndex != NULL)
	{
		// Online status has been updated , but no information is given about the online/offline state - map to accountID and retrieve results
		rlFireAndForgetTask<rlNpGetAccountIdForFriendPresence>* pTask = NULL;
		rlGetTaskManager()->CreateTask(&pTask);
		if (pTask)
		{
			rlSceNpOnlineId onlineId;
			onlineId.Reset(pFrom->onlineId);

			rlTaskBase::Configure(pTask, *gamerIndex, onlineId, false, false, nullptr, 0, false, rlNpGetAccountIdForFriendPresence::REFRESH_PRESENCE, &pTask->m_Status);
			QueueTask(pTask);
		}
	}
}

void rlNpWebApi::BlocklistUpdateCB(int32_t userCtxId, int32_t callbackId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom,
								   const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg)
{
	int* gamerIndex = (int*)pUserArg;
	if (gamerIndex != NULL)
	{
		// Retrieve the new blocklist
		g_rlNp.GetWebAPI().GetBlockList(*gamerIndex);
	}
}

// Service Push Event Callbacks
void rlNpWebApi::GameTitleInfoCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
								 const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg)
{
	// Ignore callbacks sent to myself
	if (sceNpCmpOnlineId(&pFrom->onlineId, &pTo->onlineId) == 0)
	{
		return;
	}

	int* gamerIndex = (int*)pUserArg;
	if (gamerIndex != NULL)
	{
		// The user has either started or stopping playing title- map to accountID and retrieve results
		rlFireAndForgetTask<rlNpGetAccountIdForFriendPresence>* pTask = NULL;
		rlGetTaskManager()->CreateTask(&pTask);
		if (pTask)
		{
			rlSceNpOnlineId onlineId;
			onlineId.Reset(pFrom->onlineId);

			rlTaskBase::Configure(pTask, *gamerIndex, onlineId, false, false, nullptr, 0, false, rlNpGetAccountIdForFriendPresence::REFRESH_PRESENCE, &pTask->m_Status);
			QueueTask(pTask);
		}
	}
}

void rlNpWebApi::GameStatusCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
							  const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg)
{
	// Ignore callbacks sent to myself
	if (sceNpCmpOnlineId(&pFrom->onlineId, &pTo->onlineId) == 0)
	{
		return;
	}

	int* gamerIndex = (int*)pUserArg;
	if (gamerIndex != NULL)
	{
		// Game status (i.e. Rich Presence) has been updated for friend - map to accountID and retrieve results
		rlFireAndForgetTask<rlNpGetAccountIdForFriendPresence>* pTask = NULL;
		rlGetTaskManager()->CreateTask(&pTask);
		if (pTask)
		{
			rlSceNpOnlineId onlineId;
			onlineId.Reset(pFrom->onlineId);

			rlTaskBase::Configure(pTask, *gamerIndex, onlineId, false, false, nullptr, 0, false, rlNpGetAccountIdForFriendPresence::REFRESH_PRESENCE, &pTask->m_Status);
			QueueTask(pTask);
		}
	}
}

void rlNpWebApi::GameDataCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
							const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg)
{
	// Ignore callbacks sent to myself
	if (sceNpCmpOnlineId(&pFrom->onlineId, &pTo->onlineId) == 0)
	{
		return;
	}

	// We need a FROM user
	if (pFrom == NULL)
	{
		return;
	}

	int* gamerIndex = (int*)pUserArg;
	if (gamerIndex != NULL)
	{
		bool bIsOnline = true;
		bool bIsInSameTitle = false;

		rlSceNpOnlineId onlineId;
		onlineId.Reset(pFrom->onlineId);

		// No data means that they have data from another title
		if (dataLen == 0)
		{
			bIsInSameTitle = false;
			rlDebug1("Updated friends presence for %s, isOnline: %s, isInSameTitle: %s", pFrom->onlineId.data, bIsOnline ? "true" : "false", bIsInSameTitle ? "true" : "false");
			rlDebug1("Updated friends data for %s, len: 0, data: NULL", pFrom->onlineId.data);

            rlFireAndForgetTask<rlNpGetAccountIdForFriendPresence>* pTask = NULL;
            rlGetTaskManager()->CreateTask(&pTask);
            if(pTask)
            {
                rlTaskBase::Configure(pTask, *gamerIndex, onlineId, bIsOnline, bIsInSameTitle, nullptr, 0, false, rlNpGetAccountIdForFriendPresence::UPDATE_PRESENCE, &pTask->m_Status);
                QueueTask(pTask);
            }
		}
		else if (dataLen == 2)
		{
			bIsInSameTitle = true;
			rlDebug1("Updated friends presence for %s , isOnline: %s, isInSameTitle: %s", pFrom->onlineId.data, bIsOnline ? "true" : "false", bIsInSameTitle ? "true" : "false");
			rlDebug1("Updated friends data for %s, len: 2, data: NULL", pFrom->onlineId.data);

            rlFireAndForgetTask<rlNpGetAccountIdForFriendPresence>* pTask = NULL;
            rlGetTaskManager()->CreateTask(&pTask);
            if(pTask)
            {
                rlTaskBase::Configure(pTask, *gamerIndex, onlineId, bIsOnline, bIsInSameTitle, nullptr, 0, false, rlNpGetAccountIdForFriendPresence::UPDATE_PRESENCE, &pTask->m_Status);
                QueueTask(pTask);
            }
		}
		else
		{
			bIsInSameTitle = true;
			
			// PS4 docs: gameData = freely defined data for the in-game presence for the current platform.
			//	Binary data up to 128 bytes encoded in Base64 (up to 172 ASCII characters).

			// Larger buffer for escape characters
			char gameDataBuf[DAT_BASE64_MAX_ENCODED_SIZE(RL_PRESENCE_MAX_BUF_SIZE*2)] = {0};
			RsonReader rr(pData, dataLen);

			if (rr.HasMember("gameData"))
			{
				RsonReader gameData;
				rr.GetMember("gameData", &gameData);

				rtry
				{
					if (gameData.GetValueLength() > 0)
					{
						// Get as string to unescape the string
						rverify(gameData.AsString(gameDataBuf), catchall, );

						u8 dst[RL_PRESENCE_MAX_BUF_SIZE] = {0};
						unsigned int dstLen;
						datBase64::Decode(gameDataBuf, RL_PRESENCE_MAX_BUF_SIZE, &dst[0], &dstLen);
						rverify(dstLen < RL_PRESENCE_MAX_BUF_SIZE, catchall, );

						rlDebug1("Updated friends presence for %s , isOnline: %s, isInSameTitle: %s", pFrom->onlineId.data, bIsOnline ? "true" : "false", bIsInSameTitle ? "true" : "false");
						rlDebug1("Updated friends presence data for %s, dstLen: %d", pFrom->onlineId.data, dstLen);

                        rlFireAndForgetTask<rlNpGetAccountIdForFriendPresence>* pTask = NULL;
                        rlGetTaskManager()->CreateTask(&pTask);
                        if(pTask)
                        {
                            rlTaskBase::Configure(pTask, *gamerIndex, onlineId, bIsOnline, bIsInSameTitle, dst, dstLen, true, rlNpGetAccountIdForFriendPresence::UPDATE_PRESENCE, &pTask->m_Status);
                            QueueTask(pTask);
                        }
					}
				}
				rcatchall
				{

				}
			}
		}
	}
}

void rlNpWebApi::InivitationReceivedCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
									   const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg)
{
	int* gamerIndex = (int*)pUserArg;
	if (gamerIndex != NULL)
	{
		// Presence for a friend has been updated, retrieve the results
		g_rlNp.GetBasic().SetInviteListDirty();
	}
}

void rlNpWebApi::ShutdownSceNet()
{
	sceSslTerm(m_LibSslCtxId);
	sceNetPoolDestroy(m_LibNetMemId);
	m_LibSslCtxId = 0;
	m_LibNetMemId = 0;
}

bool rlNpWebApi::Prepare(int localGamerIndex)
{
	// Make sure a user Id exists for this user (i.e. they are logged in)
	int userServiceId = g_rlNp.GetUserServiceId(localGamerIndex);
	if (userServiceId == SCE_USER_SERVICE_USER_ID_INVALID)
		return false;

	SceNpOnlineId onlineId;
	int ret = sceNpGetOnlineId(userServiceId, &onlineId);
	if (ret < 0)
	{
		if (ret == SCE_NP_ERROR_SIGNED_OUT)
		{
			rlDebug3("rlNpWebApi, Prepare() returning false due to PSN not signed in.");
			return false;
		}
		else
		{
			rlError("Prepare() failed. ret = 0x%x\n", ret);
			return false;
		}
	}

	// We already have a user context
	if (m_WebApiUserCtxId[localGamerIndex] >= 0)
	{
		// We haven't changed users, context is still valid.
		if (m_UserServiceId[localGamerIndex] == userServiceId)
		{
			return true;
		}
		else
		{
			// context is invalid
			sceNpWebApiDeleteContext(m_WebApiUserCtxId[localGamerIndex]);
			m_WebApiUserCtxId[localGamerIndex] = -1;
		}
	}

	// Set the user service id for this index so we can compare to it later.
	m_UserServiceId[localGamerIndex] = userServiceId;

	// Create a context for this index
	ret = sceNpWebApiCreateContext(m_WebApiLibCtxId, &onlineId);
	if (ret < 0)
	{
		rlError("sceNpWebApiCreateContextA() failed. ret = 0x%x\n", ret);
		return false;
	}
	else
	{
		m_WebApiUserCtxId[localGamerIndex] = ret;
	}

	return true;
}

#if !__NO_OUTPUT
void rlNpWebApi::CheckWebApiHeap(const bool logDetails)
{
	if (m_LibHttpCtxId > 0)
	{
		SceHttpMemoryPoolStats stat;
		int ret = sceHttpGetMemoryPoolStats(m_LibHttpCtxId, &stat);

		if (ret == SCE_OK)
		{
			if (logDetails)
			{
				rlDisplay("NpWebApi (HTTP) Memory pool size: %lu", stat.poolSize);
				rlDisplay("NpWebApi (HTTP) Current memory usage: %lu (%.f percent of pool)", 
					stat.currentInuseSize, 
					stat.currentInuseSize * 100.0f / NP_WEBAPI_HTTP_HEAP_SIZE);
				rlDisplay("NpWebApi (HTTP) Peak memory usage: %lu (%.f percent of pool)", 
					stat.maxInuseSize,
					stat.maxInuseSize * 100.0f / NP_WEBAPI_HTTP_HEAP_SIZE);
			}

			if (stat.maxInuseSize > m_MaxHttpPoolUsage)
			{
				rlDebug3("New Peak HTTP Memory Pool Usage: %" SIZETFMT "d", stat.maxInuseSize);
				m_MaxHttpPoolUsage = stat.maxInuseSize;
			}

			rlAssertf((stat.currentInuseSize * 100.0f / NP_WEBAPI_HTTP_HEAP_SIZE) < 95, "NpWebAPI running low on memory");
		}
		else
		{
			rlAssertf(false, "sceHttpGetMemoryPoolStats() failed. ret = 0x%x\n", ret);
		}
	}

	if (m_LibSslCtxId > 0)
	{
		SceSslMemoryPoolStats stat;
		int ret = sceSslGetMemoryPoolStats(m_LibSslCtxId, &stat);
		if (ret == SCE_OK)
		{
			if (logDetails)
			{
				rlDisplay("NpWebApi (SSL) Memory pool size: %lu", stat.poolSize);
				rlDisplay("NpWebApi (SSL) Current memory usage: %lu (%.f percent of pool)", 
					stat.currentInuseSize, 
					stat.currentInuseSize * 100.0f / NP_WEBAPI_SSL_HEAP_SIZE);
				rlDisplay("NpWebApi (SSL) Peak memory usage: %lu (%.f percent of pool)", 
					stat.maxInuseSize,
					stat.maxInuseSize * 100.0f / NP_WEBAPI_SSL_HEAP_SIZE);
			}

			if (stat.maxInuseSize > m_MaxSslPoolUsage)
			{
				rlDebug3("New Peak SSL Memory Pool Usage: %" SIZETFMT "d", stat.maxInuseSize);
				m_MaxSslPoolUsage = stat.maxInuseSize;
			}

			rlAssertf((stat.currentInuseSize * 100.0f / NP_WEBAPI_SSL_HEAP_SIZE) < 95, "NpWebAPI SSL running low on memory");
		}
		else
		{
			rlAssertf(false, "sceSslGetMemoryPoolStats() failed. ret = 0x%x\n", ret);
		}
	}
}
#endif

bool rlNpWebApi::GetProfile(const int localGamerIndex)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetProfileTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex]);
}

bool rlNpWebApi::GetFriendsList(const int localGamerIndex, rlNpGetFriendsListWorkItem* workItem, unsigned* totalFriendsPtr, 
	int startIndex, rlFriend* friends, unsigned* numFriends, int maxFriends, int friendFlags)
{
	if (!Prepare(localGamerIndex))
	{
		return false;
	}

	if (!workItem->Configure(localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], totalFriendsPtr, startIndex, friends, numFriends, maxFriends, friendFlags))
	{
		return false;
	}

	if (!netThreadPool.QueueWork(workItem))
	{
		rlError("netThreadPool failed to queue rlNpGetFriendsListWorkItem");
		return false;
	}

	return true;
}

bool rlNpWebApi::DeleteGameData(const int localGamerIndex)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpDeleteGameDataTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex]);
}

bool rlNpWebApi::DeleteGameStatus(const int localGamerIndex)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpDeleteGameStatusTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex]);
}

bool rlNpWebApi::GetPresence(const int localGamerIndex, const rlGamerHandle& gamer)
{
	bool bIsLocalUserPresence = false;

    // all calls come from Sony callbacks where we only have online Ids in SDK 1.7.
	//	some account ID's will be valid if they match a friend.
	rlSceNpAccountId accountId = gamer.GetNpAccountId();
	rlSceNpOnlineId onlineId = gamer.GetNpOnlineId();

    // no online Id specified, retrieve the online Id of the local gamer index
    if(!onlineId.IsValid())
    {
        onlineId = g_rlNp.GetNpOnlineId(localGamerIndex);
		accountId = g_rlNp.GetNpAccountId(localGamerIndex);
        bIsLocalUserPresence = true;
    }
    // if we're retrieving our own presence
    else if(memcmp(onlineId.data, g_rlNp.GetNpOnlineId(localGamerIndex).data, sizeof(onlineId.data)) == 0)
    {
        // skip this when via callbacks
        return true;
    }

	RLNP_WEBAPI_CREATE_TASK(rlNpGetPresenceTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], accountId, onlineId, bIsLocalUserPresence);
}

bool rlNpWebApi::PutGameDataBlob(const int localGamerIndex, const void* blob, int blobSize)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPutGameDataBlobTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], blob, blobSize);
}

bool rlNpWebApi::PutGameStatus(const int localGamerIndex, const char* gameStatus)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPutGameStatusTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], gameStatus);
}

bool rlNpWebApi::PutGamePresence(const int localGamerIndex, const char* gameStatus, const void* gameData, int gameDataSize)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPutGamePresenceTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], gameStatus, gameData, gameDataSize);
}

bool rlNpWebApi::GetBlockList(const int localGamerIndex)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetBlocklistTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex]);
}

bool rlNpWebApi::GetPlayerNames(const int localGamerIndex, const rlGamerHandle* handles, unsigned numHandles, rlDisplayName* displayNames, rlGamertag* gamertags, unsigned flags, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetProfilesTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], handles, numHandles, displayNames, gamertags, flags);
}

bool rlNpWebApi::PostSession(const int localGamerIndex, const char* sessionImagePath, rlSession* session, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPostSessionTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], sessionImagePath, session);
}

bool rlNpWebApi::PostSession(const int localGamerIndex, uint8_t* img, int imgLen, rlSession* session, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPostSessionTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], img, imgLen, session);
}

bool rlNpWebApi::GetSessions(const int localGamerIndex, rlNpWebApiSessionDetail* sessions, int maxNumSessions, int * numSessions, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetSessionsListTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], sessions, maxNumSessions, numSessions);
}

bool rlNpWebApi::DeleteSession(const int localGamerIndex, const rlSceNpSessionId& session, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpDeleteSessionTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session);
}

bool rlNpWebApi::GetSessionData(const int localGamerIndex, const rlSceNpSessionId& session, rlSessionInfo* outInfo, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetSessionDataTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session, outInfo);
}

bool rlNpWebApi::PutChangeableSessionData(const int localGamerIndex, rlSession* session, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPutChangeableDataTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session);
}

bool rlNpWebApi::GetChangeableSessionData(const int localGamerIndex, const rlSceNpSessionId& session, rlSessionInfo* outInfo, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetChangeableSessionDataTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session, outInfo);
}

bool rlNpWebApi::PostMember(const int localGamerIndex, const rlSceNpSessionId& session, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPostMemberTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session);
}

bool rlNpWebApi::PostMembers(const int localGamerIndex, const rlSceNpSessionId& session, rlGamerHandle* gamerHandles, int numGamers, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPostMembersTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session, gamerHandles, numGamers);
}

// Specify NULL for target ID and the player at local gamer index will be specified by default
bool rlNpWebApi::DeleteMember(const int localGamerIndex, const rlSceNpSessionId& session, rlSceNpAccountId targetId, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpDeleteMemberTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session, targetId);
}

bool rlNpWebApi::DeleteMembers(const int localGamerIndex, const rlSceNpSessionId& session, rlGamerHandle* gamerHandles, int numGamers, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpDeleteMembersTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], session, gamerHandles, numGamers);
}

bool rlNpWebApi::GetInvitations(const int localGamerIndex, rlNpWebApiInvitationDetail* invites, int maxNumInvites, int* numInvitesPtr, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetInvitationsTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], invites, maxNumInvites, numInvitesPtr);
}

bool rlNpWebApi::GetInvitationList(const int localGamerIndex, rlNpWebApiInvitationDetail* invites, int maxNumInvites, int * numInvites, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetInvitationsListTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], invites, maxNumInvites, numInvites);
}

bool rlNpWebApi::GetInvitation(const int localGamerIndex, rlNpWebApiInvitationDetail* invite)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetInvitationTask, &invite->m_DetailStatus, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], invite);
}

bool rlNpWebApi::ClearInvitation(const int localGamerIndex, rlNpWebApiInvitationDetail* invite)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpClearInvitationTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], invite);
}

bool rlNpWebApi::PostInvitation(const int localGamerIndex, rlNpWebApiPostInvitationParam* invite)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPostInvitationTask, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], invite);
}

bool rlNpWebApi::GetInvitationData(const int localGamerIndex, rlNpWebApiInvitationDetail* invite)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetInvitationDataTask, &invite->m_DataStatus, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], invite);
}

bool rlNpWebApi::GetCatalog(const int localGamerIndex, rlNpGetCatalogDetail* detail, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetCatalogTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], detail);
}

bool rlNpWebApi::GetProductInfo(const int localGamerIndex, rlNpGetProductDetail* detail, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetProductInfoTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex],detail);
}

bool rlNpWebApi::GetEntitlements(const int localGamerIndex, rlNpGetEntitlementsDetail* detail, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetEntitlementsTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], detail);
}

bool rlNpWebApi::SetEntitlementCount(const int localGamerIndex, rlNpSetEntitlementsDetail* detail, const char* entitlement, int amount, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpSetEntitlementLevelTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], detail, entitlement, amount);
}

bool rlNpWebApi::PostActivityFeedStory(const int localGamerIndex, rlNpActivityFeedStory* detail)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPostActivityFeedStory, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], detail);
}

bool rlNpWebApi::PostActivityFeedOnlinePlayedWith(const int localGamerIndex, rlNpActivityFeedPlayedWithDetails* detail)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpPostActivityFeedPlayedWith, NULL, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], detail);
}

bool rlNpWebApi::GetActivityFeedVideoData(const int localGamerIndex, rlNpGetActivityFeedVideoDataDetails* detail, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpGetActivityFeedVideoData, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], detail);
}

bool rlNpWebApi::GetAccountId(const int localGamerIndex, const rlSceNpOnlineId& targetOnlineId, rlSceNpAccountId* out_accountId, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpIdentityMapperGetAccountIdTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], targetOnlineId, out_accountId);
}

bool rlNpWebApi::GetAccountIds(const int localGamerIndex, rlSceNpOnlineId* targetOnlineIds, const unsigned numIds, rlSceNpAccountId* out_accountIds, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpIdentityMapperGetAccountIdsTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], targetOnlineIds, numIds, out_accountIds);
}

bool rlNpWebApi::GetOnlineId(const int localGamerIndex, const rlSceNpAccountId targetAccountId, rlSceNpOnlineId* out_onlineId, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpIdentityMapperGetOnlineIdTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], targetAccountId, out_onlineId);
}

bool rlNpWebApi::GetOnlineIds(const int localGamerIndex, rlSceNpAccountId* targetAccountIds, const unsigned numIds, rlSceNpOnlineId* out_onlineIds, netStatus* status)
{
	RLNP_WEBAPI_CREATE_TASK(rlNpIdentityMapperGetOnlineIdsTask, status, localGamerIndex, g_rlNp.GetNpAccountId(localGamerIndex), m_WebApiUserCtxId[localGamerIndex], targetAccountIds, numIds, out_onlineIds);
}

bool rlNpWebApi::Cancel(netStatus* status)
{
	if(netTask::HasTask(status))
	{
		netTask::Cancel(status);
		return true;
	}
	return false;
}

} // namespace rage

#endif // RSG_ORBIS
