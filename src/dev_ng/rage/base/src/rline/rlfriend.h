// 
// rline/rlfriend.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLFRIEND_H
#define RLINE_RLFRIEND_H

#include "file/file_config.h"
#include "rline/rlgamerinfo.h"
#include "rline/rlsessioninfo.h"

#if RSG_PC
#include "scfriends/rlscfriends.h"
#elif RSG_DURANGO
#include "rline/durango/rlxblplayers_interface.h"
#endif

#define SESSION_PRESENCE_OVERRIDE 1

namespace rage
{
	struct rlXblFriendInfo;

	// PURPOSE
	//  - Simple wrapper for platform identifiers for friends (Xuid, Rockstar Id, Online Id) to keep the API consistent between platforms.
	//	- Similar to rlGamerHandle or rlGamerId,  but kept specifically trimmed due to the growing size of friends lists. 
	//		rlGamerHandle is 16 bytes on XB1, rlFriendId is 8 
	//		rlGamerHandle is 48 bytes on PS4, rlFriendIs is 24 (On PS4, we also hash their friend's name for easier QSort)
	struct rlFriendId
	{
#if RSG_PC
		u64 m_RockstarId;	
#elif RSG_ORBIS
		rlSceNpAccountId m_AccountId;
		rlSceNpOnlineId m_OnlineId;
		u32	m_HandleHash;
#elif RSG_DURANGO
		u64 m_Xuid;
#endif
		
		rlFriendId();
		rlFriendId(const rlGamerHandle& handle);

		void Clear();
		bool IsValid() const;

		bool operator==( const rlFriendId& that ) const;
		bool operator<(const rlFriendId& that) const;
	};

	//PURPOSE
	//	Contains the smallest reference to a friend and their online/session
	//	Major (memory intensive) details like Name, Presence, etc belong to the rlFriend instead.
	struct rlFriendsReference
	{
		rlFriendId m_Id;

		bool m_bIsOnline : 1;
		bool m_bIsInSameTitle : 1;
		bool m_bIsInSession : 1;
		bool m_bIsTwoWay : 1; // XB1 only

		rlFriendsReference()
		{
			Clear();
		}

		void Clear()
		{
			m_Id.Clear();

			m_bIsOnline = false;
			m_bIsInSameTitle = false;
			m_bIsInSession = false;
			m_bIsTwoWay = false;
		}

		bool IsValid() const
		{
			return m_Id.IsValid();
		}

		bool operator==( const rlFriendsReference& that ) const;
	};

#if RSG_DURANGO
	struct rlXblFriendInfo
	{
		char m_DisplayName[RL_MAX_DISPLAY_NAME_BUF_SIZE];
		char m_Nickname[RL_MAX_NAME_BUF_SIZE];
		bool m_bIsFavorite;

		rlXblFriendInfo()
		{
			Clear();
		}

		void Clear()
		{
			m_DisplayName[0] = '\0';
			m_Nickname[0] = '\0';
			m_bIsFavorite = false;
		}
	};

#endif

//PURPOSE
//  Contains details for a gamer on a friends list.  rlFriend instances are populated by rlFriendsReader.
class rlFriend
{
    friend class rlFriendsReader;
	friend class rlFriendsManager;

public:

    rlFriend();
    ~rlFriend();

	//PURPOSE
	// Reset a friend to an empty state
    void Clear();

    //PURPOSE
    //  Retrieves the gamer handle for the friend.
    rlGamerHandle* GetGamerHandle(rlGamerHandle* hGamer) const;

	//PURPOSE
	//  Returns the user ID of the friend encoded in string form.
	//  On XBL the user ID is the string representation of a u64 *in DECIMAL format*.
	//  On NP the user ID is the sceNpOnlineId
	//  On SC the user ID is the string representation of the Rockstar ID.
    const char* GetUserId(char* buf, const int bufSize) const
    {
        rlGamerHandle gh;
        return GetGamerHandle(&gh)->ToUserId(buf, bufSize);
    }
    template<int SIZE>
	const char* GetUserId(char (&buf)[SIZE]) const
    {
        return GetUserId(buf, SIZE);
    }

    //PURPOSE
    //  Returns the friend's name
    const char* GetName() const;

	//PURPOSE
	// Returns the user's UTF-8 display name
	const char* GetDisplayName() const;

	//PURPOSE
	// Returns true if the gamer handle is valid
	const bool IsValid() const;

    //PURPOSE
    //  Returns true if the friend is currently online.
    //NOTES
    //  This value is updated only when the friend data is refreshed
    //  by a call to rlFriendsReader::Read().
    bool IsOnline() const;

	//PURPOSE
	//  Set whether a user is online
	void SetIsOnline(bool bOnline);

    //PURPOSE
    //  Returns true if the friend is in the same title.
    bool IsInSameTitle() const;

	//PURPOSE
	//   Sets whether a user is in the same title
	void SetIsInSameTitle(bool bIsPlayingSameTitle);

	//PURPOSE
	//	Sets whether a user is online and in the same title
	void SetIsOnline(bool bIsOnline, bool bIsPlayingSameTitle);

	//PURPOSE
	// Set is in session
	void SetIsInSession(bool bInSession);

    //PURPOSE
    //  Returns true if the friend is a "pending" friend.
    //  Either he's been invited me and not accepted,
    //  or I've been invited by him and I've not accepted.
    bool IsPendingFriend() const;

    //PURPOSE
    //  Returns true if we've invited the friend but he hasn't yet
    //  accepted.
    bool IsPendingFriendInvitee() const;

    //PURPOSE
    //  Returns true if the friend invited us but we've not yet accepted.
    bool IsPendingFriendInviter() const;

#if !RSG_PC
	//PURPOSE
	// Returns the underlying rlFriendsReference of the friend.
	const rlFriendsReference& GetReference() const { return m_FriendReference; }
#endif

	//PURPOSE
	// Set whether the user is a favorite
	void SetFavorite(bool bFavorite);

	//PURPOSE
	// Returns true if the friend is favorited as well as followed
	bool IsFavorite() const;

	//PURPOSE
	//  Returns true if the friend is in a multiplayer session
	bool IsInSession() const;

	// PURPOSE
	// Returns true if the friend is playing a last gen console (currently PS4 only)
	bool IsPlayingLastGen() const;

#if RSG_NP
    //PURPOSE
    //  Initializes the instance.
    void Reset(rlSceNpAccountId accountId, const rlSceNpOnlineId& onlineId, bool bIsOnline, bool bIsInSameTitle, char* blob, const unsigned blobSize);

    //PURPOSE
    //  Returns the title-specific presence blob associated with this friend.
    //  Returns false if this friend could not be found, or the passed-in
    //  buffer was not large enough to hold the data.
    //NOTES
    //  - The buffer passed in must be at least RL_PRESENCE_MAX_BUF_SIZE bytes.
    //  - The data returned is the instantaneous presence blob state, so the 
    //    results may change between calls, even for the same rlFriend instance.
    bool GetPresenceBlob(const int localGamerIndex, void* blob, unsigned* blobSize, const unsigned maxBlobSize) const;

#elif RSG_PC
	enum Flags
	{
		FLAG_IS_IN_JOINABLE_SESSION = 0x01,
		FLAG_IS_IN_JOINABLE_PARTY  = 0x02,
	};

	void Reset(const rlScFriend& scFriend, unsigned flags);
	const rlSessionInfo& GetSessionInfo() const;
	RockstarId GetRockstarId() const;

#if __RGSC_DLL
	const rlScFriend& GetScFriend() const;

	void SetRichPresence(const char* richPresence);
	const char* GetRichPresence();
#endif
#elif RSG_DURANGO
	//PURPOSE
	// Reset the rlFriend from a given xblFriend
	void Reset(const u64 xuid, const rlXblFriendInfo& xblFriend, bool bIsOnline = false, bool bIsInSameTitle = false, bool bTwoWay = false, bool bIsInSession = false);

	//PURPOSE
	// Returns the user XUID
	u64 GetXuid() const;

	//PURPOSE
	//	Sets the user's UTF-8 display name
	void SetDisplayName(const char * displayName);

	//PURPOSE
	// Tests to see if this friend is using a Display Name
	bool HasDisplayName() const;

	//PURPOSE
	// On Durango, we pull in details and presence separately.
	void SetNickname(const char* nickname);

	//PURPOSE
	// Set is two-way friend
	void SetIsTwoWay(bool bTwoWay);

	//PURPOSE
	// Returns the rlXblFriendInfo
	rlXblFriendInfo GetXblFriendInfo() const;

	//PURPOSE
	// Sets the session info
	void SetSession(rlSessionInfo& session);
#endif

	bool operator==(const rlFriend& that) const;
	bool operator!=(const rlFriend& that) const;

private:

#if RSG_NP
	rlFriendsReference  m_FriendReference;
	u8                  m_PresenceBlob[RL_PRESENCE_MAX_BUF_SIZE];
	unsigned            m_PresenceBlobSize;
#elif RSG_PC
	rlScFriend			m_ScFriend;
	unsigned			m_Flags;
#elif RSG_DURANGO
	rlFriendsReference  m_FriendReference;
	rlXblFriendInfo		m_XblFriendInfo;
#endif

};

}   //namespace rage

#endif  //RLINE_RLFRIEND_H
