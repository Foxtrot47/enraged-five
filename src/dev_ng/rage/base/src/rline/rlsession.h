// 
// rline/rlsession.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSESSION_H
#define RLINE_RLSESSION_H

#include "rlmatchingattributes.h"
#include "scmatchmaking/rlscmatchmaking.h"
#include "rlgamerinfo.h"
#include "rlsessionconfig.h"
#include "rlsessioninfo.h"
#include "rlsessionmanager.h"
#include "net/status.h"
#include "net/netsocket.h"
#include "string/string.h"

namespace rage
{

class rlMatch;

#if RSG_DURANGO
struct rlXblSessionHandle;
#endif

//PURPOSE
//  This class represents a game session.
class rlSession
{
    friend class rlSessionManager;
    friend class rlMatch;

public:

    static const u64 INVALID_SESSION_ID = 0;

    rlSession();

    virtual ~rlSession();

    bool Init();

    void Shutdown(bool isSudden);

    //PURPOSE
    //  Process session operations.  Should be called at least once
    //  every 100 ms.
    void Update();

    //PURPOSE
    //  Creates a session as the host.
    //PARAMS
    //  localGamerIndex - Index of the local gamer that owns the session.
    //  netMode         - The network mode to use.
    //  maxPublicSlots  - Number of public slots in the session.
    //  maxPrivateSlots - Number of private slots in the session.
    //  attrs           - Advertised session attributes.
    //  flags           - Bit combination of CreateFlags enum.
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  Sessions with no public slots are considered private sessions and
    //  will not be advertised on matchmaking.  Private sessions thus
    //  require no server resources.  Consider making sessions private when
    //  possible.  For example, invite-only sessions, such as those used
    //  for parties, should be private.
    //
    //  Matching attributes cannot be nil (unset) for sessions with public
    //  slots.  Private sessions are permitted to have nil attributes, but
    //  they must still specify a valid game mode and game type.
    //
    //  When joining a LAN session on PS make sure the socket protocol
    //  used for network transport is NET_PROTO_UDP.  When joining an ONLINE
    //  session on PS make sure the socket protocol is NET_PROTO_UDPP2P.
    bool Host(const int localGamerIndex,
                const rlNetworkMode netMode,
                const int maxPublicSlots,
                const int maxPrivateSlots,
                const rlMatchingAttributes& attrs,
                const unsigned flags,
                netStatus* status);

    //PURPOSE
    //  Activates a local instance of a session hosted by a remote peer.
    //PARAMS
    //  localGamerIndex - Index of the local gamer that owns the session.
    //  sessionInfo     - Session info.
    //  netMode         - ONLINE or LAN.
    //  createFlags     - Bit combination of CreateFlags enum.
    //  presenceFlags   - Bit combination of PresenceFlags enum.
    //  slotType        - Slot type into which the local owner will join.
    //  maxPublicSlots  - Maximum public slots
    //  maxPrivateSlots - Maximum private slots
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  When joining a LAN session on PS make sure the socket protocol
    //  used for network transport is NET_PROTO_UDP.  When joining an ONLINE
    //  session on PS make sure the socket protocol is NET_PROTO_UDPP2P.
    bool Activate(const int localGamerIndex,
				const u64 hostPeerId,
                const rlSessionInfo& sessionInfo,
                const rlNetworkMode netMode,
                const unsigned createFlags,
                const unsigned presenceFlags,
                const unsigned maxPublicSlots,
                const unsigned maxPrivateSlots,
                const rlSlotType slotType,
                netStatus* status);

	// PURPOSE
	//	Establishes the session
    bool Establish(const rlSessionConfig& config,
                    netStatus* status);

    //PURPOSE
    //  Hosts an offline session.
    //PARAMS
    //  localGamerIndex - Index of the local gamer that owns the session.
    //  numSlots        - Number of slots in the session.
    //  attrs           - Session attributes.
    //  status          - Optional status object that can be polled for
    //                    completion.
    bool HostOffline(const int localGamerIndex,
                    const int numSlots,
                    const rlMatchingAttributes& attrs,
                    netStatus* status);

    //PURPOSE
    //  Destroys a game session.
    //PARAMS
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  This method will fail if a current game session does not exist
    //  or is not in the idle state.
    //  This method initiates an asynchronous process and returns immediately.
    //  When it completes the notify handler will be called with the appropriate
    //  code.
    //
    //  If this method fails the session will be in an undefined state until
    //  Clear() is called.
    bool Destroy(netStatus* status);

    //PURPOSE
    //  Joins the gamers to the session.
    //  This method will fail if a current game session does not exist.
    //  Call this method for all gamers (local or remote) that are
    //  participants in the session.
    //  Non-hosts should not call Join() until they have asked the host
    //  to join and the host has accepted.
    //  Hosts should call Join() for each gamer they accept and
    //  broadcast the join to all peers so they too can call Join()
    //  for that gamer on their local copies of the session.
    //PARAMS
    //  gamerHandles    - Gamers that are joining.
    //  slotTypes   - Slot types for the joiners.
    //  numGamers   - Number of joiners.
    //  status      - Optional status object that can be polled for
    //                completion.
    //NOTES
    //  This method will fail if a current game session does not exist.
    //  This method initiates an asynchronous process and returns immediately.
    //  When it completes the notify handler will be called with the appropriate
    //  code.
    bool Join(const rlGamerHandle* gamerHandles,
                const rlSlotType* slotTypes,
                const unsigned numGamers,
                netStatus* status);

    //PURPOSE
    //  Leaves the gamers from the session.
    //  This method will fail if a current game session does not exist.
    //  When a non-host wishes to leave a session it should first notify
    //  the host, then call Leave().  When a host receives a leave request
    //  it should call Leave() for that gamer and then broadcast the leave
    //  to all peers so they too can call Leave() for that gamer on their local
    //  copies of the session.
    //PARAMS
    //  gamerHandles    - Gamers that are leaving.
    //  numGamers   - Number of leavers.
    //  status      - Optional status object that can be polled for
    //                completion.
    //NOTES
    //  This method will fail if a current game session does not exist.
    //  This method initiates an asynchronous process and returns immediately.
    //  When it completes the notify handler will be called with the appropriate
    //  code.
    bool Leave(const rlGamerHandle* gamerHandles,
                const unsigned numGamers,
                netStatus* status);

	//PURPOSE
	//  Enables matchmaking advertising
	bool EnableMatchmakingAdvertise(netStatus* status);

    //PURPOSE
    //  Changes the attributes of the session.
    //NOTES
    //  If the host changes attributes it should communicate that change
    //  to all session members so they can change their local copies of
    //  the attributes.
    //
    //  This should only be called on the host, or in response to
    //  a command from the host.
    //
    //  Only attributes contained in newAttrs will be changed.  Attributes
    //  not contained in newAttrs will be left unchanged.
    //
    //  New attributes cannot be added.
    //
    bool ChangeAttributes(const rlMatchingAttributes& newAttrs,
                        netStatus* status);

    //PURPOSE
    //  Migrates the session to a new host.
    //PARAMS
    //  newHostPeerInfo - New host peer info.
    //  sessionInfo     - Session info for migrated session.
    //                    Ignored if we're the new host.
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  If newHost refers to the local peer then the local peer
    //  will become the new host.
    //  If newHost refers to a remote peer then sessionInfo must be non-NULL
    //  and must have been received from the new host after it successfully
    //  took ownership of the session.
    bool MigrateHost(const rlPeerInfo& newHostPeerInfo,
                    const rlSessionInfo* sessionInfo,
                    netStatus* status);

    //PURPOSE
    //  Enables/disables presence flags.  See the rlSessionPresenceFlags enum.
    bool ModifyPresenceFlags(const unsigned presenceFlags,
                        netStatus* status);

	//PURPOSE
	// Updates the number of available slots in the local session with matchmaking
	bool UpdateAvailableSlots(netStatus* status);

    //PURPOSE
    //  Cancels in flight command with given status
    bool Cancel(netStatus* status);

	//PURPOSEHASM
	//  Returns the session presence flags.
	unsigned GetPresenceFlags() const;

	//PURPOSE
	//  Returns true if the session has a valid SC matchId.
	bool HasScMatchId() const;

	//PURPOSE
	//  Returns SC matchId.
	const rlScMatchmakingMatchId& GetScMatchId();

    //PURPOSE
    //  Returns true if invites are enabled.
    bool IsInvitable() const;

	//PURPOSE
	//  Returns true if players can join via presence
	bool IsJoinableViaPresence() const;

    //PURPOSE
    //  Sends invites to remote gamers to join the session.
    //PARAMS
    //  gamerHandles    - Array of gamer handles representing
    //                    remote gamers.
    //  numGamers       - Length of gamer handle array.
    //  subject         - Subject line of invite message.  PS only.
    //  salutation      - String to display with invitation.
    //  status          - Optional status object that can be polled for
    //                    completion.
    bool SendInvites(const rlGamerHandle* gamerHandles,
                    const unsigned numGamers,
                    const char* subject,
                    const char* salutation,
                    netStatus* status);

    //PURPOSE
    //  Sends invites to members of your platform-service party.
    //NOTE
    //  This is NOT the same as a game-level party. It is only
    //  supported on LIVE platforms currently.
    bool SendPartyInvites(const int localInviterIndex,
                          netStatus* status);

    //PURPOSE
    //  Enable/disable answering of LAN queries when we're the host.
    //NOTES
    //  This is used to enable/disable answering of LAN queries when
    //  we wish to prevent join in progress.
    void SetAnswerLanQueriesEnabled(const bool enabled);

    //PURPOSE
    //  Returns true if answering LAN queries is enabled.
    bool IsAnswerLanQueriesEnabled() const;

    //PURPOSE
    //  Returns the session configuration.
    const rlSessionConfig& GetConfig() const;

    //PURPOSE
    //  Returns the session attributes.
    const rlMatchingAttributes& GetAttributes() const;

    //PURPOSE
    //  Returns the session creation flags.
    unsigned GetCreateFlags() const;

	//PURPOSE
	//  Returns the peer ID of the host.
	const u64 GetHostPeerId() const;

    //PURPOSE
    //  Returns the peer address of the host.
    const rlPeerAddress& GetHostPeerAddress() const;

    //PURPOSE
    //  Returns the owner of the local session instance.
    const rlGamerInfo& GetOwner() const;

    //PURPOSE
    //  Returns the unique id for this session.
    //  This ID *must not* be saved beyond the lifetime of
    //  the session, as session IDs can be reused.
    u64 GetId() const;

    //PURPOSE
    //  Returns the platform-specific session handle.
    const void* GetSessionHandle() const;

    //PURPOSE
    //  Returns the maximum number of slots of the given type.
    unsigned GetMaxSlots(const rlSlotType slotType) const;

    //PURPOSE
    //  Returns the maximum number of overall slots.
    unsigned GetMaxSlots() const;

    //PURPOSE
    //  Returns the number of filled slots of the given type.
    unsigned GetFilledSlots(const rlSlotType slotType) const;

    //PURPOSE
    //  Returns the number of empty slots of the given type.
    unsigned GetEmptySlots(const rlSlotType slotType) const;

	//PURPOSE
	//	Returns the number of reserved slots
	unsigned GetNumReservedSlots() const;

	//PURPOSE
	//	Sets the number of reserved slots
	void SetNumReservedSlots(unsigned numReservedSlots);

    //PURPOSE
    //  Returns the number of local gamers in the session.
    unsigned GetLocalGamerCount() const;

    //PURPOSE
    //  Retrieves session members.
    //RETURNS
    //  Number of members
    unsigned GetMembers(rlGamerHandle* gamerHandles,
                        const unsigned maxGamers) const;

    //PURPOSE
    //  Returns true if the gamer is a member of this session.
    bool IsMember(const rlGamerHandle& gamerHandle) const;

    //PURPOSE
    //  Returns true if the gamer is in a public slot.
    bool IsInPublicSlot(const rlGamerHandle& gamerHandle) const;

    //PURPOSE
    //  Returns true if the gamer is in a private slot.
    bool IsInPrivateSlot(const rlGamerHandle& gamerHandle) const;

    //PURPOSE
    //  Returns the slot type occupied by the given gamer.
    rlSlotType GetSlotType(const rlGamerHandle& gamerHandle) const;

    //PURPOSE
    //  Returns the session info.
    const rlSessionInfo& GetSessionInfo() const;

    //PURPOSE
    //  Returns the network mode.
    rlNetworkMode GetNetworkMode() const;

    //PURPOSE
    //  Returns true if this is an online session.
    bool IsOnline() const;

    //PURPOSE
    //  Returns true if this is a lan session
    bool IsLan() const;

    //PURPOSE
    //  Returns true if this is a network (online or lan) session.
    bool IsNetwork() const;

    //PURPOSE
    //  Returns true if this is an offline session.
    bool IsOffline() const;

    //PURPOSE
    //  Returns true if the session is dormant.
    bool IsDormant() const;

    //PURPOSE
    //  Returns true if the local instance of the session is activated.
    bool IsActivated() const;

    //PURPOSE
    //  Returns true if the session has been established.
    bool IsEstablished() const;

    //PURPOSE
    //  Returns true if an operation is pending.
    //NOTES
    //  While IsPending() return true, all modifying operations
    //  (Host(), Join(), Leave(), etc.) will fail.
    bool IsPending() const;

    //PURPOSE
    //  Returns true if the session is in a SESSION_IDLE* state.
    bool IsIdle() const;

    //PURPOSE
    //  Returns true if the session was created as host.
    bool IsHost() const;

    //PURPOSE
    //  Set app-specific data returned to QoS queries.  This can be set at any
    //  time.  To reset, call with a null data pointer.
    //PARAMS
    //  data        - Pointer to data
    //  size        - Size of user data (max is RL_MAX_QOS_USER_DATA_SIZE)
    //RETURNS
    //  True in success, false on failure.
    bool SetQosUserData(const u8* data,
                        const u16 size);

    //PURPOSE
    //  Access QoS data previously set with SetQosUserData. This is only
    //  valid after Host() has been succesfully called, and becomes invalid
    //  when the session is destroyed.
    u16 GetQosUserDataSize() const;
    const u8* GetQosUserData();

	//PURPOSE
	//  Set app-specific data returned to config queries.  This can be set at any
	//  time.  To reset, call with a null data pointer.
	//PARAMS
	//  data        - Pointer to data
	//  size        - Size of user data (max is RL_MAX_SESSION_USER_DATA_SIZE)
	//RETURNS
	//  True in success, false on failure.
	bool SetSessionUserData(const u8* data,
                            const u16 size);

	//PURPOSE
	//  Access QoS data previously set with SetSessionUserData. This is only
	//  valid after Host() has been succesfully called, and becomes invalid
	//  when the session is destroyed.
	u16 GetSessionUserDataSize() const;
	const u8* GetSessionUserData() const;

#if RSG_ORBIS
	// PURPOSE
	//	Set, Get and Clear the PSN Web Api Session Id
	const rlSceNpSessionId& GetWebApiSessionId() const;
	void SetWebApiSessionId(const rlSceNpSessionId& sessionId);
	void ClearWebApiSessionId();
#endif

private:

    enum State
    {
        STATE_DORMANT,
        STATE_ACTIVATED,
        STATE_IDLE,
        STATE_DESTROYING
    };

    struct Slot
    {
        Slot()
        {
            this->Clear();
        }

        void Clear()
        {
            m_hGamer.Clear();
            m_SlotType = RL_SLOT_INVALID;
        }

        void Reset(const rlGamerHandle& gamerHandle,
                    const rlSlotType slotType)
        {
            this->Clear();

            m_hGamer = gamerHandle;
            m_SlotType = slotType;
        }

        rlGamerHandle m_hGamer;
        rlSlotType m_SlotType;
    };

    struct Data
    {
        Data();

        void Clear();

        bool Reset(const rlGamerInfo& localOwner,
					const u64 hostPeerId,
                    const rlSessionInfo* sessionInfo,
                    const rlNetworkMode netMode,
                    const int numPublicSlots,
                    const int numPrivateSlots,
                    const rlMatchingAttributes& attrs,
                    const unsigned createFlags,
                    const unsigned presenceFlags,
                    const u64 sessionId,
                    const u64 arbCookie);

        bool Validate() const;

#if !__NO_OUTPUT
		void LogDetails() const;
#endif

        State           m_State;
        rlGamerInfo     m_LocalOwner;
		u64				m_HostPeerId;
        rlSessionInfo   m_SessionInfo;
        rlSessionConfig m_Config;
		rlScMatchmakingMatchId m_MatchId;

#if RSG_DURANGO
		rlXblSessionHandle* m_hSession;
#endif
    };

    void Clear();

    enum CmdId
    {
        CMD_INVALID         = -1,
        CMD_HOST_NET,
        CMD_HOST_OFFLINE,
        CMD_ACTIVATE,
        CMD_ESTABLISH,
        CMD_DESTROY,
        CMD_JOIN,
        CMD_LEAVE,
        CMD_CHANGE_ATTRS,
        CMD_MIGRATE,
        CMD_MODIFY_PRESENCE_FLAGS,
        CMD_SEND_INVITES,
        CMD_SEND_PARTY_INVITES,
		CMD_UPDATE_SC_ADVERTISEMENT,
		CMD_NUM,
    };


    struct Cmd
    {
        Cmd(const CmdId id, rlSession* session, netStatus* status);
        virtual ~Cmd();

#if RSG_OUTPUT
		const char* GetCmdName() const;
#endif

        virtual void Update() {}
		virtual bool CanCancel() { return false; }
		virtual bool Cancel();
		virtual void OnCancel() {} 

        CmdId m_Id;
        rlSession* m_Session;
        netStatus* m_Status;
        netStatus m_TmpStatus;
        netStatus m_MyStatus;
    };
    
	struct CmdUpdateScAdvertisement : public Cmd
	{
		CmdUpdateScAdvertisement(rlSession* session, netStatus* status);

		virtual void Start();
		virtual void Update();
		virtual bool CanCancel() { return true; }

		enum State
		{
			STATE_INVALID,
			STATE_ADVERTISING_SOCIAL_CLUB_SESSION,
			STATE_UPDATING_SOCIAL_CLUB_SESSION,
			STATE_UNADVERTISING_SOCIAL_CLUB_SESSION,
		};

		State m_State;
	};

    struct CmdDestroy : public Cmd
    {
        CmdDestroy(rlSession* session, netStatus* status);

		virtual void Start();
		virtual void Update();
		virtual bool CanCancel() { return true; }

        enum State
        {
			STATE_UNADVERTISE_SOCIAL_CLUB_SESSION,
			STATE_UNADVERTISING_SOCIAL_CLUB_SESSION,
            STATE_DESTROY,
            STATE_DESTROYING,
            STATE_FINISHED
        };

        State m_State;

        bool m_ClearSessionWhenDone : 1;
    };

    struct CmdHost : public Cmd
    {
        CmdHost(rlSession* session,
                const rlGamerInfo& localOwner,
                const rlNetworkMode netMode,
                const int numPublicSlots,
                const int numPrivateSlots,
                const rlMatchingAttributes& attrs,
                const unsigned createFlags,
                netStatus* status);

        virtual void Update();
		virtual void OnCancel();
		virtual bool CanCancel() { return true; }

        enum State
        {
            STATE_CREATING,
            STATE_SET_VISIBILITY,
            STATE_SETTING_VISIBILITY,
			STATE_ADVERTISE_SOCIAL_CLUB_SESSION,
			STATE_ADVERTISING_SOCIAL_CLUB_SESSION,
#if RL_NP_UDS
            STATE_CREATE_MATCH_ACTIVITY,
            STATE_CREATING_MATCH_ACTIVITY,
#endif
#if RSG_NP
            STATE_POST_SESSION,
			STATE_POSTING_SESSION,
#endif
            STATE_DESTROY,
            STATE_DESTROYING
        };

        Data m_Data;

        CmdDestroy m_CmdDestroy;

        State m_State;
	};

    struct CmdHostOffline : public CmdHost
    {
        CmdHostOffline(rlSession* session,
                        const rlGamerInfo& localOwner,
                        const int numSlots,
                        const rlMatchingAttributes& attrs,
                        netStatus* status);
    };

    struct CmdActivate : public Cmd
    {
        CmdActivate(rlSession* session,
                    const rlGamerInfo& localOwner,
					const u64 hostPeerId,
                    const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const unsigned createFlags,
                    const unsigned presenceFlags,
                    const unsigned maxPublicSlots,
                    const unsigned maxPrivateSlots,
                    const rlSlotType slotType,
                    netStatus* status);

        virtual void Update();
		virtual void OnCancel();
		virtual bool CanCancel() { return true; }

        enum State
        {
            STATE_ACTIVATING,
            STATE_DESTROY,
            STATE_DESTROYING
        };

        rlGamerInfo m_LocalOwner;
		u64 m_HostPeerId;
        rlSessionInfo m_SessionInfo;
        rlNetworkMode m_NetMode;
        unsigned m_CreateFlags;
        unsigned m_PresenceFlags;
        unsigned m_MaxPublicSlots;
        unsigned m_MaxPrivateSlots;
        rlSlotType m_SlotType;

#if RSG_DURANGO
		rlXblSessionHandle* m_hSession;
#endif

        State m_State;

        CmdDestroy m_CmdDestroy;
    };

    struct CmdEstablish : public Cmd
    {
        CmdEstablish(rlSession* session, netStatus* status);

        virtual void Update();
		virtual bool CanCancel() { return true; }

        Data m_Data;
    };

    struct CmdJoin : public Cmd
    {
        CmdJoin(rlSession* session, netStatus* status);

        virtual void Update();
		virtual void OnCancel();
		virtual bool CanCancel() { return true; }

        enum State
        {
            STATE_ADDING_GAMERS,
            STATE_CHANGING_SESSION_VISIBILITY,
            STATE_UPDATE_GAMER_LIST,
#if RL_NP_UDS
            STATE_JOIN_MATCH_ACTIVITY,
            STATE_JOINING_MATCH_ACTIVITY,
#elif RSG_ORBIS
			STATE_POST_MEMBER,
			STATE_POSTING_MEMBER,
#endif
			STATE_UPDATING_SOCIAL_CLUB_SESSION,
        };

		unsigned m_NumGamers;
        rlGamerHandle m_GamerHandles[RL_MAX_GAMERS_PER_SESSION];
        rlSlotType m_SlotTypes[RL_MAX_GAMERS_PER_SESSION];

        unsigned m_PriorNumEmptyPubSlots;

		CmdUpdateScAdvertisement m_CmdUpdateScAdvertisement;

        State m_State;
    };

    struct CmdLeave : public Cmd
    {
        CmdLeave(rlSession* session, netStatus* status);

        virtual void Update();
		virtual void OnCancel();
		virtual bool CanCancel() { return true; }

        enum State
        {
			STATE_UNADVERTISING_SOCIAL_CLUB_SESSION,
            STATE_LEAVING_PLATFORM_SERVICE,
#if RL_NP_UDS
            STATE_LEAVE_MATCH_ACTIVITY,
            STATE_LEAVING_MATCH_ACTIVITY,
#endif
            STATE_UPDATE_GAMER_LIST,
			STATE_UPDATING_SOCIAL_CLUB_SESSION,
        };

        unsigned m_NumGamers;
        rlGamerHandle m_GamerHandles[RL_MAX_GAMERS_PER_SESSION];
		rlSlotType m_SlotTypes[RL_MAX_GAMERS_PER_SESSION];
		bool m_HasLocalGamers;
		bool m_UnadvertisedSession;

		CmdUpdateScAdvertisement m_CmdUpdateScAdvertisement;

        State m_State;
    };

    struct CmdChangeAttrs : public Cmd
    {
        CmdChangeAttrs(rlSession* session, netStatus* status);

        virtual void Update();
		virtual bool CanCancel() { return true; }

		enum State
		{
			STATE_CHANGING_PLATFORM_SESSION_ATTRS,
			STATE_CHANGE_SOCIAL_CLUB_SESSION_ATTRS,
			STATE_CHANGING_SOCIAL_CLUB_SESSION_ATTRS,
		};

		State m_State;

        rlMatchingAttributes m_NewAttrs;
    };

    struct CmdMigrate : public Cmd
    {
        CmdMigrate(rlSession* session, netStatus* status);

        virtual void Update();
		virtual bool CanCancel() { return true; }

        enum State
        {
            STATE_MIGRATING,
			STATE_ADVERTISE_SOCIAL_CLUB_SESSION,
			STATE_ADVERTISING_SOCIAL_CLUB_SESSION,
			STATE_UNADVERTISE_SOCIAL_CLUB_SESSION,
			STATE_UNADVERTISING_SOCIAL_CLUB_SESSION,
#if RSG_ORBIS
			STATE_POST_SESSION,
			STATE_POSTING_SESSION
#endif
		};

        rlPeerInfo m_NewHostPeerInfo;
        rlSessionInfo m_NewSessionInfo;
        State m_State;
    };

    struct CmdModifyPresenceFlags : public Cmd
    {
        CmdModifyPresenceFlags(rlSession* session, netStatus* status);

        virtual void Update();
		virtual bool CanCancel() { return true; }

        unsigned m_PresenceFlags;
    };

    struct CmdSendInvites : public Cmd
    {
        CmdSendInvites(rlSession* session,
                        const rlGamerHandle* gamerHandles,
                        const unsigned numGamers,
                        const char* subject,
                        const char* salutation,
                        netStatus* status);

        virtual void Update();
		virtual bool CanCancel() { return true; }

#if RSG_NP
		enum State
		{
			STATE_SEND_INVITES,
		};
		State m_State;
#elif RSG_DURANGO
		enum State
		{
			STATE_CHECK_PARTY,
			STATE_CREATING_PARTY,
			STATE_SEND_INVITES,
			STATE_SENDING_INVITES,
		};
		State m_State;
#endif

        unsigned m_NumGamers;
        rlGamerHandle m_GamerHandles[RL_MAX_GAMERS_PER_SESSION];
        char m_Subject[RL_MAX_INVITE_SUBJECT_CHARS];
        char m_Salutation[RL_MAX_INVITE_SALUTATION_CHARS];
    };

    struct CmdSendPartyInvites : public Cmd
    {
        CmdSendPartyInvites(rlSession* session, 
                            const int localInviterIndex,
                            netStatus* status);

        virtual void Update();
		virtual bool CanCancel() { return true; }

        int m_LocalInviterIndex;
    };

    bool DoCmd(Cmd* cmd);

    //Platform-independent functions called from worker thread.
    bool CommonHost(CmdHost* cmd);
    bool CommonActivate(CmdActivate* cmd);
    bool CommonEstablish(CmdEstablish* cmd);
    bool CommonDestroy(CmdDestroy* cmd);
    bool CommonJoin(CmdJoin* cmd);
    bool CommonLeave(CmdLeave* cmd);
    bool CommonChangeAttrs(CmdChangeAttrs* cmd);
    bool CommonMigrate(CmdMigrate* cmd);
    bool CommonModifyPresenceFlags(CmdModifyPresenceFlags* cmd);
    bool CommonSendInvites(CmdSendInvites* cmd);
    bool CommonSendPartyInvites(CmdSendPartyInvites* cmd);
	bool CommonUpdateScAdvertisement(CmdUpdateScAdvertisement* cmd);

    //Platform-specific functions called from platform-independent functions.
    bool NativeHost(CmdHost* cmd);
    bool NativeActivate(CmdActivate* cmd);
    bool NativeEstablish(CmdEstablish* cmd);
    bool NativeDestroy(CmdDestroy* cmd);
    bool NativeJoin(CmdJoin* cmd);
    bool NativeLeave(CmdLeave* cmd);
    bool NativeChangeAttrs(CmdChangeAttrs* cmd);
    bool NativeMigrate(CmdMigrate* cmd);
    bool NativeModifyPresenceFlags(CmdModifyPresenceFlags* cmd);
    bool NativeSendInvites(CmdSendInvites* cmd);
    bool NativeSendPartyInvites(CmdSendPartyInvites* cmd);

    //PURPOSE
    //  Inserts a gamer into the next available slot.
    void InsertGamer(const rlGamerHandle& gamerHandle,
                     const rlSlotType slotType);

    //PURPOSE
    //  Removes the gamer from the slot he occupies and clears the slot.
    void RemoveGamer(const rlGamerHandle& gamerHandle);

    int GetSlotIndex(const rlGamerHandle& gamerHandle) const;

    Slot* GetSlot(const rlGamerHandle& gamerHandle);
    const Slot* GetSlot(const rlGamerHandle& gamerHandle) const;

	// PURPOSE
	//	Returns TRUE if the session is matchmaking or presence enabled
	bool IsPresenceEnabled() const { return m_Data.m_Config.IsPresenceEnabled(); }
	bool IsMatchmakingEnabled() const { return m_Data.m_Config.IsMatchmakingEnabled(); }
	bool IsMatchmakingAdvertisingEnabled() const { return m_Data.m_Config.IsMatchmakingAdvertisingEnabled(); }

	// PURPOSE 
	//	Returns TRUE if social club matchmaking action can be performed
	bool CanAdvertiseScMatch() const;
	bool CanUpdateScMatch() const;
	bool CanUnadvertiseScMatch() const;

    Data m_Data;

    Slot m_Slots[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_MemberCount;
    unsigned m_PubSlotCount;
    unsigned m_PrivSlotCount;
	unsigned m_NumReservedSlots;

    Cmd* m_Cmd;

    rlSessionManagerLink m_SesMgrLink;

    //QoS user data (optional app-specific data that can be returned to QoS queries)
    u16 m_QosUserDataSize;
    u8 m_QosUserData[RL_MAX_QOS_USER_DATA_SIZE];

	//Session user data (optional app-specific data that can be returned to config queries)
	u16 m_SessionUserDataSize;
	u8 m_SessionUserData[RL_MAX_SESSION_USER_DATA_SIZE];

    bool m_AnswerLanQueriesEnabled : 1;

    bool m_Initialized : 1;

#if !__NO_OUTPUT
    //Helper functions used to retrieve session ids for debug spew.
    static u64 GetSessionId(const rlSession* s);
    static u64 GetSessionId(const Cmd* cmd);
    static u64 GetSessionId(const void*);
    static const rlSessionToken GetSessionToken(const rlSession* s);
    static const rlSessionToken GetSessionToken(const Cmd* cmd);
    static const rlSessionToken GetSessionToken(const void*);
#endif  //!__NO_OUTPUT

};

}   //namespace rage

#endif  //RLINE_RLSESSION_H
