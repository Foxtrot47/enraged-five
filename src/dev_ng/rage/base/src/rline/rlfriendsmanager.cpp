// 
// rline/rlfriendsmanager.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "rlfriendsmanager.h"

#include "diag/seh.h"
#include "rline/rldiag.h"
#include "rline/rlnp.h"
#include "rline/rlfriendmanagertasks.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "string/stringhash.h"

#if RSG_NP
#include "rline/rlnpcommon.h"
#elif RSG_DURANGO
#elif RSG_PC
#endif

#if __RGSC_DLL
#include "../../../suite/src/rgsc/rgsc/rgsc/rgsc.h"
#endif

namespace rage
{
	extern sysMemAllocator* g_rlAllocator;

	//Override output macros.
	#undef __rage_channel
	#define __rage_channel rline_rlfriendsmanager
	RAGE_DEFINE_SUBCHANNEL(rline, rlfriendsmanager)

	bool rlFriendsManager::sm_bInitialized;
	rlFriendsManager::ActivationState rlFriendsManager::sm_ActivateState;
	rlFriendsReader rlFriendsManager::sm_FriendsReader;
	rlFriendsPage rlFriendsManager::sm_DefaultFriends;

	atArray<rlFriendsReference*> rlFriendsManager::sm_FriendReferencesById;

	netStatus	rlFriendsManager::sm_ReadStatus;
	netStatus	rlFriendsManager::sm_ActivationStatus;
	netStatus	rlFriendsManager::sm_XblFriendCountStatus;
	netStatus	rlFriendsManager::sm_XblPresenceSessionStatus;

	unsigned	rlFriendsManager::sm_FriendsReadResultCount = 0;
	unsigned	rlFriendsManager::sm_NumFriends = 0;
	bool		rlFriendsManager::sm_bSortFriends = false;
	int			rlFriendsManager::sm_ActiveGamerIndex = -1;

	int			rlFriendsManager::ACTIVATION_FAILURE_RETRY_TIMER[RETRY_TIMERS] = { 1, 30, 60, 600 };	
	int			rlFriendsManager::sm_CurrentActivationTimer = 0;
	netRetryTimer rlFriendsManager::sm_ActivationRetryTimer;

	unsigned	rlFriendsManager::sm_LastFriendsRefresh = 0;
	bool		rlFriendsManager::sm_NeedToRefreshFriends = false;
	bool		rlFriendsManager::sm_CheckForListChanges = false;
	bool		rlFriendsManager::sm_RefreshPresence = false;
	int			rlFriendsManager::sm_RefreshPresenceNumFriends = 0;
	int			rlFriendsManager::sm_RefreshPresenceStartIndex = 0;
	int			rlFriendsManager::sm_NumFriendsToRefresh = 0;
	u32			rlFriendsManager::sm_TimeBetweenRefreshes = DEFAULT_TIME_BETWEEN_REFRESH;

	rlFriendsManager::Delegator	rlFriendsManager::sm_Delegator;

#if RSG_NP
	rlNpEventDelegator::Delegate rlFriendsManager::sm_NpDlgt;
#elif RSG_DURANGO
	atArray<rlFriendsReference*> rlFriendsManager::sm_FriendReferencesByStatus;
	rlXblEventDelegator::Delegate rlFriendsManager::sm_XblDlgt;
	
	//////////////////////////////////////////////////////////////////////////
	// rlFriendsReference sorting
	//////////////////////////////////////////////////////////////////////////
	static int SortFriendsByStatus(rlFriendsReference* const* lhs, rlFriendsReference* const* rhs)
	{
		int result = 0;
		if((*lhs)->m_bIsInSession != (*rhs)->m_bIsInSession)
		{
			result = (*lhs)->m_bIsInSession ? -1 : 1;
		}
		else if((*lhs)->m_bIsInSameTitle != (*rhs)->m_bIsInSameTitle)
		{
			result = (*lhs)->m_bIsInSameTitle ? -1 : 1;
		}
		else if((*lhs)->m_bIsOnline != (*rhs)->m_bIsOnline)
		{
			result = (*lhs)->m_bIsOnline ? -1 : 1;
		}
		else if((*lhs)->m_bIsTwoWay != (*rhs)->m_bIsTwoWay)
		{
			result = (*lhs)->m_bIsTwoWay ? -1 : 1;
		}

		return result;
	}
#endif

	///////////////////////////////////////////////////////////////////////////////
	//  rlFriend Sorting
	///////////////////////////////////////////////////////////////////////////////
	static int SortFriends(const void* p0, const void* p1)
	{
		const rlFriend* f0 = (const rlFriend*) p0;
		const rlFriend* f1 = (const rlFriend*) p1;

		int result = 0;
		if(f0->IsInSession() != f1->IsInSession())
		{
			result = f0->IsInSession() ? -1 : 1;
		}
		else if(f0->IsInSameTitle() != f1->IsInSameTitle())
		{
			result = f0->IsInSameTitle() ? -1 : 1;
		}
		else if(f0->IsOnline() != f1->IsOnline())
		{
			result = f0->IsOnline() ? -1 : 1;
		}
#if RSG_DURANGO
		else if(f0->IsFavorite() != f1->IsFavorite())
		{
			result = f0->IsFavorite() ? -1 : 1;
		}
#endif
		else
		{
			//Case insensitive sort
			result = stricmp(f0->GetName(), f1->GetName());
		}

		return result;
	}

	//////////////////////////////////////////////////////////////////////////
	//	rlFriendPage
	//////////////////////////////////////////////////////////////////////////
	rlFriendsPage::rlFriendsPage()
		: m_Friends(NULL)
		, m_MaxFriends(0)
		, m_NumFriends(0)
		, m_StartIndex(0)
	{

	}

	rlFriendsPage::~rlFriendsPage()
	{
		Shutdown();
	}

	void rlFriendsPage::Shutdown()
	{
		// Clear the friends array, resets the starting index and num friends
		ClearFriendsArray();

		// If the friends list contains a friends page, free the memory
		if (m_Friends)
		{
			RL_DELETE_ARRAY(m_Friends, m_MaxFriends);
			m_Friends = NULL;
		}
	}

	bool rlFriendsPage::Init(const int maxFriends)
	{
		rtry
		{
			// Initialization checks: do not re-init, validate size
			rverify(m_Friends == NULL, catchall, rlError("rlFriendsPage already initialized"));
			rverify(maxFriends <= MAX_FRIEND_PAGE_SIZE, catchall, rlError("Too many friends requested."));

			m_MaxFriends = maxFriends;
			m_Friends = (rlFriend*)RL_NEW_ARRAY(rlFriendsPage, rlFriend, m_MaxFriends);

			// Clear out the friends array
			ClearFriendsArray();

			return true;
		}
		rcatchall
		{
			// initialization error, free friends memory
			if (m_Friends)
			{
				RL_DELETE_ARRAY(m_Friends, m_NumFriends);
			}

			return false;
		}
	}

	void rlFriendsPage::ClearFriendsArray()
	{
		// Note: does not free memory, simply clears the friends
		for(unsigned i = 0; i < m_NumFriends; i++)
		{
			m_Friends[i].Clear();
		}

		m_NumFriends = 0;
		m_StartIndex = 0;
	}

	bool rlFriendsPage::Contains(rlFriendsPage* page)
	{
		// Friends page must have a friends array to contain another page
		if (m_Friends == NULL)
		{
			return false;
		}

		// Start index is within our range, and request does not exceed our list size
		if (page->m_StartIndex >= m_StartIndex && 
			(page->m_StartIndex + page->m_MaxFriends) <= (m_StartIndex + m_MaxFriends))
		{
			return true;
		}

		return false;
	}

	rlFriend* rlFriendsPage::GetFriend(const rlGamerHandle& gamerHandle)
	{
		if (m_Friends == NULL)
		{
			return NULL;
		}

		// Iterate through friends array looking for a matching gamer handle
		for (unsigned  i = 0; i < m_NumFriends; i++)
		{
			rlGamerHandle h;
			if (m_Friends[i].GetGamerHandle(&h) != NULL)
			{
				if (h == gamerHandle)
				{
					return &m_Friends[i];
				}
			}
		}

		return NULL;
	}

	int rlFriendsPage::GetFriendIndex(const char* friendName, const unsigned startIndex, unsigned count)
	{
		int index = rlFriendsManager::INVALID_FRIEND_INDEX;

		// Iterate friends array, using string comparison to find a friend.
		for(unsigned i = startIndex; 0 < count; ++i, --count)
		{
			if(0 == strcmp(friendName, m_Friends[i].GetName()))
			{
				index = (int) i;
				break;
			}
		}

		return index;
	}
	
	void rlFriendsPage::FillFriendArray(atArray<rlGamerHandle>& out_FilledArray)
	{
		rlAssert(out_FilledArray.empty());

		out_FilledArray.ResizeGrow(m_NumFriends);
		for(unsigned int i = 0; i < m_NumFriends; ++i)
		{
			m_Friends[i].GetGamerHandle( &out_FilledArray[i] );
		}
	}

	// PURPOSE
	//	Handle a friend is fully updated
	bool rlFriendsPage::OnFriendUpdated(const rlFriend& f)
	{
		for(unsigned int i = 0; i < m_NumFriends; ++i)
		{
			// gamer handle comparison
			if (m_Friends[i] == f)
			{
#if !__NO_OUTPUT
				rlUserIdBuf idBuf;
				rlDebug1("rlFriendsPage::OnFriendUpdated - updating %s (index: %d)", f.GetUserId(idBuf), i);
#endif

				// set all properties including presence blob
				m_Friends[i] = f;
				return true;
			}
		}

		rlDebug1("rlFriendsPage::OnFriendUpdated - Event fired for friend not in immediate list.");
		return false;
	}

	void rlFriendsPage::OnFriendStatusChange(const rlFriend& f)
	{
		for(unsigned int i = 0; i < m_NumFriends; ++i)
		{
			if (m_Friends[i] == f)
			{
#if !__NO_OUTPUT
				rlUserIdBuf idBuf;
				rlDebug1("rlFriendsPage::OnFriendStatusChange - updating %s IsOnline to %d", f.GetUserId(idBuf), f.IsOnline());
#endif
				m_Friends[i].SetIsOnline(f.IsOnline());
				return;
			}
		}

		rlDebug1("rlFriendsPage::OnFriendStatusChange - Event fired for friend not in immediate list.");
	}

	void rlFriendsPage::OnFriendTitleChange(const rlFriend& f)
	{
		for(unsigned int i = 0; i < m_NumFriends; ++i)
		{
			if (m_Friends[i] == f)
			{
#if !__NO_OUTPUT
				rlUserIdBuf idBuf;
				rlDebug1("rlFriendsPage::OnFriendTitleChange - updating %s IsInSameTitle to %d", f.GetUserId(idBuf), f.IsInSameTitle());
#endif
				m_Friends[i].SetIsInSameTitle(f.IsInSameTitle());
				return;
			}
		}

		rlDebug1("rlFriendsPage::OnFriendTitleChange - Event fired for friend not in immediate list.");
	}

	void rlFriendsPage::OnFriendSessionChange(const rlFriend& f)
	{
		for(unsigned int i = 0; i < m_NumFriends; ++i)
		{
			if (m_Friends[i] == f)
			{
#if !__NO_OUTPUT
				rlUserIdBuf idBuf;
				rlDebug3("rlFriendsPage::OnFriendSessionChange - %s updating IsInSession to %d", f.GetUserId(idBuf), f.IsInSession());
#endif
				m_Friends[i].SetIsInSession(f.IsInSession());
				return;
			}
		}

		rlDebug3("rlFriendsPage::OnFriendSessionChange - Event fired for friend not in immediate list.");
	}

	void rlFriendsPage::Sort()
	{
		qsort(&m_Friends[0], m_NumFriends, sizeof(m_Friends[0]), SortFriends);
	}

	//////////////////////////////////////////////////////////////////////////
	// rlFriendsManager
	//////////////////////////////////////////////////////////////////////////
	bool rlFriendsManager::InitClass()
	{
		rtry
		{
			rlDebug3("Size of gamerHandle: %" SIZETFMT "d", sizeof(rlGamerHandle));
			rlDebug3("Size of friendId: %" SIZETFMT "d", sizeof(rlFriendId));

			rverify(!IsInitialized(), catchall, rlError("rlFriendsManager is already initialized."));
			rverify(sm_FriendsReader.Init(0), catchall, rlError("rlFriendsManager could not initialize its reader."));
			rverify(sm_DefaultFriends.Init(), catchall, rlError("rlFriendsManager could not initialize the default friends page."));

			// Clear the internal friends manager details
			Clear();

			sm_bInitialized = true;
			sm_ActivateState = STATE_IDLE;
			sm_RefreshPresence = false;

#if RSG_DURANGO
			g_rlXbl.AddDelegate(&sm_XblDlgt);
			sm_XblDlgt.Bind(&rlFriendsManager::OnXblEvent);
#elif RSG_NP
			g_rlNp.AddDelegate(&sm_NpDlgt);
			sm_NpDlgt.Bind(&rlFriendsManager::OnNpEvent);
#endif

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	// PURPOSE
	//	Does a platform/title analysis to determine if subscriptions to
	//  presence should be done.
	bool rlFriendsManager::ShouldSubscribeToPresence()
	{
#if __RGSC_DLL
		// For the DLL - if we're handling presence for the title, subscribe to friends.
		// Otherwise, let the game do it.
		if (GetRgscConcreteInstance()->IsPresenceSupported())
			return true;
		else
			return false;
#elif RSG_ORBIS
		// For PS4 - we do want to subscribe to presence for friends.
		return true;
#elif RSG_DURANGO
		// For XB1 - we do want to subscribe to presence for friends.
		return true;
#elif RSG_PC
		// For PC on GTAV - we get our friends presence via the DLL.
		return false;
#else
		return false;
#endif
	}

	//PURPOSE
	//	Performs a binary search for a friend ID
	//	Returns the friend index, or -1 if not found.
	int rlFriendsManager::BinarySearchFriends(rlFriendId id)
	{
		int low = 0;
		int high = sm_FriendReferencesById.GetCount()-1;
		while (low <= high) 
		{
			int mid = (low + high) >> 1;
			if (id == sm_FriendReferencesById[mid]->m_Id)
			{
				return mid;
			}
			else if (id < sm_FriendReferencesById[mid]->m_Id)
			{
				high = mid-1;
			}
			else
			{
				low = mid+1;
			}
		}
		return INVALID_FRIEND_INDEX;
	}

	void rlFriendsManager::SortFriends()
	{
#if RSG_DURANGO
		sm_FriendReferencesByStatus.QSort(0, -1, &SortFriendsByStatus);
		sm_FriendReferencesById.QSort(0, -1, &rlFriendsManagerTask::SortFriendsById);
#elif RSG_ORBIS
		sm_FriendReferencesById.QSort(0, -1, &rlFriendsManagerTask::SortFriendsById);
#elif __RGSC_DLL
		sm_DefaultFriends.Sort();
#endif
	}

	void rlFriendsManager::Clear()
	{
		for (int i = 0; i < sm_FriendReferencesById.GetCount(); i++)
		{
			RL_DELETE(sm_FriendReferencesById[i]);
		}

		sm_FriendReferencesById.Reset(true);

#if RSG_DURANGO
		sm_FriendReferencesByStatus.Reset(true);
#endif

		// Friend numbers
		sm_FriendsReadResultCount = 0;
		sm_NumFriends = 0;
		sm_bSortFriends = false;
		
		CancelTaskIfNecessary(&sm_ActivationStatus);
		CancelTaskIfNecessary(&sm_ReadStatus);
		CancelTaskIfNecessary(&sm_XblPresenceSessionStatus);
		CancelTaskIfNecessary(&sm_XblFriendCountStatus);
	}

	void rlFriendsManager::Update()
	{
		if (IsInitialized())
		{
			sm_FriendsReader.Update();
			sm_ActivationRetryTimer.Update();

			if (sm_ActivateState == STATE_ACTIVATING && !sm_ActivationStatus.Pending())
			{
				if (sm_ActivationStatus.Succeeded())
				{
					sm_CurrentActivationTimer = 0;
					sm_ActivationStatus.Reset(); 

#if RSG_DURANGO || RSG_ORBIS || __RGSC_DLL
					// Sort friends immediately
					SortFriends();
#endif

					// Update Friend SC Subscriptions due to presence read
					UpdateScSubscriptions();

					// Set activated and throw activation event
					sm_ActivateState = STATE_ACTIVATED;
					rlFriendsManagerReadyEvent e(sm_ActiveGamerIndex);
					DispatchEvent(&e);
				}
				else
				{
					sm_ActivateState = STATE_ACTIVATION_FAILED;
					sm_ActivationStatus.Reset();

					// Backoff retries: retry after 1, 30, 60, 600 seconds etc
					sm_ActivationRetryTimer.InitSeconds(ACTIVATION_FAILURE_RETRY_TIMER[sm_CurrentActivationTimer]);
					if (sm_CurrentActivationTimer < RETRY_TIMERS-1)
					{
						sm_CurrentActivationTimer++;
					}

					rlDebug3("rlFriendsManager failed to activate, trying again in %d seconds", sm_ActivationRetryTimer.GetCurrentRetryIntervalSeconds());
				}
			}
			else if (sm_ActivateState == STATE_ACTIVATION_FAILED && sm_ActivationRetryTimer.IsTimeToRetry())
			{
				if (RL_IS_VALID_LOCAL_GAMER_INDEX(sm_ActiveGamerIndex) && rlPresence::IsOnline(sm_ActiveGamerIndex))
				{
					Activate(sm_ActiveGamerIndex);
				}
			}

			// We're activated, wait for any current reads (or system syncs) to complete
			if (IsActivated() && !IsReading())
			{
				if (sm_NeedToRefreshFriends)
				{
					rlDebug3("Refreshing friends due to sm_NeedToRefreshFriends");
					int iFriendFlags = rlFriendsReader::FRIENDS_ALL | rlFriendsReader::FRIENDS_PRESORT_ONLINE;
					sm_FriendsReader.Read(sm_ActiveGamerIndex, sm_DefaultFriends.m_StartIndex, &sm_DefaultFriends.m_Friends[0], sm_DefaultFriends.m_MaxFriends, 
											&sm_DefaultFriends.m_NumFriends, &sm_FriendsReadResultCount, iFriendFlags, &sm_ReadStatus);
					sm_NeedToRefreshFriends = false;
				}
				else if (sm_RefreshPresence)
				{
					rlDebug3("Refreshing friends due to sm_RefreshPresence. sm_RefreshPresenceStartIndex: %d, sm_RefreshPresenceNumFriends: %d, "
						"sm_DefaultFriends.m_StartIndex: %d, sm_DefaultFriends.m_NumFriends: %u, sm_DefaultFriends.m_MaxFriends: %u",
						sm_RefreshPresenceStartIndex, sm_RefreshPresenceNumFriends, sm_DefaultFriends.m_StartIndex, sm_DefaultFriends.m_NumFriends, sm_DefaultFriends.m_MaxFriends);

					int iFriendFlags = rlFriendsReader::FRIENDS_ALL | rlFriendsReader::FRIENDS_PRESORT_ONLINE | rlFriendsReader::FRIENDS_JOINABLE_SESSION_PRESENCE;

					static unsigned presenceFriendCount; // throwaway since we're not interested in the results
					static unsigned presenceFriendsRefreshed; // throwaway since we're not interested in the results

					// If the friends refresh resulted in us losing a friend, our queued refresh num would be greater than our actually number of friends
					if (sm_RefreshPresenceNumFriends > (int)sm_DefaultFriends.m_MaxFriends)
						sm_RefreshPresenceNumFriends = (int)sm_DefaultFriends.m_MaxFriends;

					sm_FriendsReader.Read(sm_ActiveGamerIndex, sm_RefreshPresenceStartIndex, &sm_DefaultFriends.m_Friends[sm_RefreshPresenceStartIndex], sm_RefreshPresenceNumFriends,
										&presenceFriendsRefreshed, &presenceFriendCount, iFriendFlags, &sm_ReadStatus);
					sm_RefreshPresence = false;
				}
				else if (sm_bSortFriends)
				{
#if RSG_DURANGO  || RSG_ORBIS || __RGSC_DLL
					SortFriends();
#endif

#if RSG_DURANGO
					UpdateScSubscriptions();
#endif
					sm_bSortFriends = false;
				}
				else if (sm_ReadStatus.Succeeded())
				{
					if (sm_CheckForListChanges)
					{
#if RSG_ORBIS
						// On PS4 SDK 1.7, our push notifications from Sony don't include who was
						// removed from a friends list. If we get out of sync, we need to reactivate.
						if (IsOutofSync())
						{
							Reactivate();
						}
						else
#endif
						{
							for (unsigned int i = 0; i < sm_DefaultFriends.m_NumFriends; i++)
							{
								rlGamerHandle h;
								sm_DefaultFriends.m_Friends[i].GetGamerHandle(&h);
								bool bHasReference = IsFriendsWith(sm_ActiveGamerIndex, h);
								if (!bHasReference)
								{
									AddFriendReferenceById(rlFriendId(h), true);
								}
							}
						}

						sm_CheckForListChanges = false;
					}

#if __RGSC_DLL
					// Friends reads come back from SCS unsorted. Sort immediately before firing event.
					SortFriends();
#endif

					// Update Friend SC Subscriptions due to friends read
					UpdateScSubscriptions();

					rlFriendEventListChanged e;
					DispatchEvent(&e);
					sm_ReadStatus.Reset();
				}
				else if (sm_XblPresenceSessionStatus.Succeeded())
				{
					// Update Friend SC Subscriptions due to presence read
					UpdateScSubscriptions();
					sm_XblPresenceSessionStatus.Reset();
				}
			}
		}
	}

	bool rlFriendsManager::ShutdownClass()
	{
		if (IsInitialized())
		{
			Clear();

			sm_DefaultFriends.Shutdown();
			sm_FriendsReader.Shutdown();

			sm_bInitialized = false;
			sm_ActivateState = STATE_IDLE;
		}
		
		return true;
	}

	bool rlFriendsManager::Activate(const int localGamerIndex)
	{
		rtry
		{
			rverify(sm_bInitialized, catchall, rlError("rlFriendsManager must be initialized to activate"));
			rverify(sm_ActivateState == STATE_IDLE || sm_ActivateState == STATE_ACTIVATION_FAILED, catchall, rlError("rlFriendsManager must be in an inactivate state to activate"));

			sm_ActiveGamerIndex = localGamerIndex;
			sm_DefaultFriends.ClearFriendsArray();

			if (ReadAndSubscribeToFriends(true))
			{
				sm_ActivateState = STATE_ACTIVATING;
				return true;
			}
			else
			{
				sm_ActivateState = STATE_ACTIVATION_FAILED;
				return false;
			}
		}
		rcatchall
		{
			return false;
		}
	}

	//PURPOSE
	//	Refreshes the friend manager. Calls clear() internally, and re-reads the friends list. Sets state to ACTIVATING
	bool rlFriendsManager::Reactivate()
	{
		rtry
		{
			rverify(sm_bInitialized, catchall, rlError("rlFriendsManager must be initialized to Reactivate"));
			rcheck(sm_ActivateState == STATE_ACTIVATED, catchall, rlError("rlFriendsManager must be in an activated state to Reactivate"));
			rcheck(!IsReading(), catchall, );

			// Our friends array differs from our friends list in memory
			if (IsOutofSync())
			{
				rlDebug1("Friends list is out of sync, reactivate.");

				sm_DefaultFriends.ClearFriendsArray();
				Clear(); // Clears pending reads

				if (ReadAndSubscribeToFriends(false))
				{
					sm_ActivateState = STATE_ACTIVATING;
				}
				else
				{
					sm_ActivateState = STATE_ACTIVATION_FAILED;
					return false;
				}
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlFriendsManager::IsReading() 
	{ 
		rtry
		{
			rcheck(!sm_ReadStatus.Pending(), catchall, );
			rcheck(!sm_ActivationStatus.Pending(), catchall, );
			rcheck(!sm_XblPresenceSessionStatus.Pending(), catchall, );
			rcheck(!sm_XblFriendCountStatus.Pending(), catchall, );

#if !__RGSC_DLL
			// PC - Wait for the DLL to finish its sync before reading friends
			rcheck(!IsSyncingFriends(), catchall, );
#endif

			return false;
		}
		rcatchall
		{
			return true;		
		}
	}

	bool rlFriendsManager::IsSyncingFriends()
	{
#if __RGSC_DLL
		return sm_NeedToRefreshFriends || IsReading();
#elif RSG_PC
		return g_rlPc.GetPlayerManager() != NULL && g_rlPc.GetPlayerManager()->IsSyncingFriends();
#else
		return false;
#endif
	}

	bool rlFriendsManager::IsOutofSync()
	{
		return sm_FriendsReadResultCount != GetTotalNumFriends(sm_ActiveGamerIndex);
	}

	//PURPOSE
	//	Deactivates the friend reader. Calls Clear() internally. Must be called before attempting to activate a new gamer index.
	bool rlFriendsManager::Deactivate()
	{
		rtry
		{
			rverify(IsInitialized(), catchall, );

			// Only unregister callbacks if we successfully activated previously
			if (IsActivated())
			{
				UnsubscribeToFriends();
			}

			// Clear friends array
			sm_DefaultFriends.ClearFriendsArray();

			// clear the friends references
			Clear();

			// reset state
			sm_ActivateState = STATE_IDLE;

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlFriendsManager::ReadAndSubscribeToFriends(bool ORBIS_ONLY(bSubscribe))
	{
		rtry
		{
#if RSG_DURANGO
			rlFriendsMgrInitializationTask* task;
			rverify(netTask::Create(&task, &sm_ActivationStatus), catchall, );
			rverify(task->Configure(sm_ActiveGamerIndex, &sm_FriendReferencesByStatus, &sm_FriendReferencesById, &sm_FriendsReadResultCount), catchall, );
			rverify(netTask::Run(task), catchall, );
#elif RSG_ORBIS
			rlFriendsMgrInitializationTask* task;
			rverify(netTask::Create(&task, &sm_ActivationStatus), catchall, );
			rverify(task->Configure(sm_ActiveGamerIndex, bSubscribe, &sm_FriendReferencesById, &sm_DefaultFriends), catchall, );
			rverify(netTask::Run(task), catchall, );
#elif RSG_PC
			rverify(sm_FriendsReader.Read(sm_ActiveGamerIndex, sm_DefaultFriends.m_StartIndex, &sm_DefaultFriends.m_Friends[0], sm_DefaultFriends.m_MaxFriends, 
					&sm_DefaultFriends.m_NumFriends, &sm_FriendsReadResultCount, rlFriendsReader::FRIENDS_ALL, &sm_ActivationStatus), catchall, );
#else
			rlAssertf(false, "Not supported on this platform.");
#endif
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlFriendsManager::UnsubscribeToFriends()
	{
#if RSG_DURANGO
		return false; // Unsubscription handled in RTA disconnect
#elif RSG_ORBIS
		return g_rlNp.GetWebAPI().RegisterPushEventCallbacks(sm_ActiveGamerIndex, false);
#else
		return false;
#endif
	}

	// Subscribe/Unsubscribe from SC
	bool rlFriendsManager::AddRemoveScSubscription(rlGamerHandle* gh, bool bSubscribe)
	{
		if (!ShouldSubscribeToPresence())
			return true;

		char friendChannel[256];
		rlUserIdBuf userIdBuf;
		if (rlVerify(gh->ToUserId(userIdBuf) != NULL))
		{
			formatf(friendChannel, "friend.%s", userIdBuf);
			if (bSubscribe)
			{
				return rlPresence::Subscribe(sm_ActiveGamerIndex, friendChannel);
			}
			else
			{
				return rlPresence::Unsubscribe(sm_ActiveGamerIndex, friendChannel);
			}
		}

		return false;
	}

	void rlFriendsManager::OnFriendRichPresenceChanged(const rlGamerHandle& RGSC_DLL_ONLY(gh), const char* RGSC_DLL_ONLY(presenceString))
	{
#if __RGSC_DLL
		int friendIndex = GetFriendIndex(sm_ActiveGamerIndex, gh);
		if (friendIndex != INVALID_FRIEND_INDEX)
		{
			sm_DefaultFriends.m_Friends[friendIndex].SetRichPresence(presenceString);
		}
#endif
	}

	//PURPOSE
	// Update Friend SC Subscriptions (different than XB1/PSN subscriptions)
	void rlFriendsManager::UpdateScSubscriptions()
	{
		rtry
		{
			rcheck(ShouldSubscribeToPresence(), catchall, );

			rlDebug3("Updating SC Friend subscriptions");
			unsigned numFriends = GetTotalNumFriends(sm_ActiveGamerIndex);
			for (unsigned i = 0; i < numFriends; i++)
			{
				rlGamerHandle gh;
				rcheck(GetGamerHandle(i, &gh), catchall, );

#if RSG_DURANGO
				if (!IsPendingFriend(sm_ActiveGamerIndex, gh))
#endif
				{
					rverify(AddRemoveScSubscription(&gh, IsFriendOnline(i)), catchall, );
				}
			}
		}
		rcatchall
		{

		}
	}

	unsigned rlFriendsManager::GetTotalNumFriends(const int localGamerIndex)
	{
		rtry
		{
			rcheck(localGamerIndex == sm_ActiveGamerIndex, catchall, );
			rcheck(IsInitialized(), catchall, );

#if RSG_DURANGO
			return sm_FriendReferencesByStatus.GetCount();
#elif RSG_PC
			return sm_DefaultFriends.m_NumFriends;
#elif RSG_ORBIS
			return sm_FriendReferencesById.GetCount();
#endif
		}
		rcatchall
		{
			return 0;
		}
	}

	bool rlFriendsManager::GetFriends(const int localGamerIndex, rlFriendsPage* page, int friendFlags, netStatus* status)
	{
#if RSG_DURANGO
		(void)friendFlags; // not needed on durango
		
		// No friends to retrieve
		if (sm_FriendReferencesByStatus.GetCount() == 0)
		{
			page->m_NumFriends = 0;
			status->SetPending();
			status->SetSucceeded();
			return true;
		}

		// Validate the incoming page
		if (rlVerify(page->m_StartIndex >= 0 && page->m_StartIndex < sm_FriendReferencesByStatus.GetCount()))
		{
			int endIndex = rage::Min(page->m_StartIndex + (int)page->m_MaxFriends, sm_FriendReferencesByStatus.GetCount());
			int counter = 0;
			for (int i = page->m_StartIndex; i < endIndex; i++)
			{
				rlXblFriendInfo xblFriendInfo;
				page->m_Friends[counter].Reset(sm_FriendReferencesByStatus[i]->m_Id.m_Xuid, xblFriendInfo, sm_FriendReferencesByStatus[i]->m_bIsOnline, 
												sm_FriendReferencesByStatus[i]->m_bIsInSameTitle, sm_FriendReferencesByStatus[i]->m_bIsTwoWay, sm_FriendReferencesByStatus[i]->m_bIsInSession);
				counter++;
			}

			page->m_NumFriends = counter;
			g_rlXbl.GetPlayerManager()->GetFriendInfo(localGamerIndex, page->m_Friends, page->m_NumFriends, status);
		}
#else
		if (IsActivated() && sm_DefaultFriends.Contains(page))
		{
			// the parent page could contain more than we're requesting
			int offset = page->m_StartIndex - sm_DefaultFriends.m_StartIndex;

			// return the number of requested friends, capped to the max friends we can request
			int numFriends = rage::Min(page->m_MaxFriends, sm_DefaultFriends.m_NumFriends - offset);

			// copy the friends over
			for (int i = 0; i < numFriends; i++)
			{
				page->m_Friends[i] = sm_DefaultFriends.m_Friends[i + offset];
			}

			// set the number of friends and return immediate success
			page->m_NumFriends = numFriends;
			status->SetPending();
			status->SetSucceeded();
		}
		else
		{
			sm_FriendsReader.Read(localGamerIndex, page->m_StartIndex, page->m_Friends, page->m_MaxFriends, &page->m_NumFriends, &sm_FriendsReadResultCount, friendFlags, status);
		}
#endif

		return true;
	}

	bool rlFriendsManager::IsFriendsWith(const int localGamerIndex, const rlGamerHandle& gamer)
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rcheck(localGamerIndex == sm_ActiveGamerIndex, catchall, );
			rverify(gamer.IsValid(), catchall, );

			return GetFriendIndex(localGamerIndex, gamer) != INVALID_FRIEND_INDEX;
		}
		rcatchall
		{
		}

		return false;
	}

	bool rlFriendsManager::IsPendingFriend(const int localGamerIndex, const rlGamerHandle& DURANGO_ONLY(gamer))
	{
		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rcheck(localGamerIndex == sm_ActiveGamerIndex, catchall, );

#if RSG_DURANGO
			int index = BinarySearchFriends(rlFriendId(gamer));
			if (index >= 0)
			{
				return !sm_FriendReferencesById[index]->m_bIsTwoWay;
			}
			else
			{
				return false;
			}
#else
			rlAssertf(false, "Not supported on this platform");
#endif
		}
		rcatchall
		{
		}

		return false;
	}

	//PURPOSE
	//	Returns true if the manager is currently sorting friends
	bool rlFriendsManager::IsSortingFriends()
	{
		return sm_bSortFriends;
	}

	//PURPOSE
	//	Returns the friend's index in the IdReferences array, or INVALID_FRIEND_INDEX if not found
	int rlFriendsManager::GetFriendIndex(const int localGamerIndex, const rlGamerHandle& gamer)
	{
		if (localGamerIndex == sm_ActiveGamerIndex)
		{
#if RSG_ORBIS || RSG_DURANGO
			return BinarySearchFriends(rlFriendId(gamer));
#elif RSG_PC
			for (unsigned int i = 0; i < sm_DefaultFriends.m_NumFriends; i++)
			{
				if (sm_DefaultFriends.m_Friends[i].GetRockstarId() == gamer.GetRockstarId())
				{
					return i;
				}
			}
#endif
		}

		return INVALID_FRIEND_INDEX;
	}

	//PURPOSE
	//	Returns the gamer handle of a friend at the given index
	rlGamerHandle* rlFriendsManager::GetGamerHandle(const int friendIndex, rlGamerHandle* hGamer)
	{
		hGamer->Clear();

#if RSG_DURANGO
		if (rlVerify(friendIndex < sm_FriendReferencesByStatus.GetCount()))
		{
			hGamer->ResetXbl(sm_FriendReferencesByStatus[friendIndex]->m_Id.m_Xuid);
		}
#elif RSG_PC
		if (rlVerify(friendIndex < (int)sm_DefaultFriends.m_NumFriends))
		{
			hGamer->ResetSc(sm_DefaultFriends.m_Friends[friendIndex].GetRockstarId());
		}
#elif RSG_ORBIS
		if (rlVerify(friendIndex < sm_FriendReferencesById.GetCount()))
		{
			hGamer->ResetNp(g_rlNp.GetEnvironment(),
							sm_FriendReferencesById[friendIndex]->m_Id.m_AccountId,
							&sm_FriendReferencesById[friendIndex]->m_Id.m_OnlineId);
		}
#endif

		return hGamer;
	}

	bool rlFriendsManager::AddFriendReferenceById(rlFriendId id, bool IsTwoWay)
	{
		if (!IsActivated())
		{
			return false;
		}

		rlFriendsReference* ref;
		ref = RL_ALLOCATE_T(rlFriendsManager, rlFriendsReference);
		if (ref)
		{
			ref->Clear();
			ref->m_Id = id;
			ref->m_bIsTwoWay = IsTwoWay;

			// Add status ref
#if RSG_DURANGO
			sm_FriendReferencesByStatus.PushAndGrow(ref);
#endif

			// Add Id ref
			sm_FriendReferencesById.PushAndGrow(ref);

#if RSG_DURANGO || RSG_ORBIS || __RGSC_DLL
			// Sort friends immediately
			SortFriends();
#endif

			// Flag for a refresh
			sm_NeedToRefreshFriends = true;

#if RSG_DURANGO
			// Subscribe
			g_rlXbl.GetRealtimeActivityManager()->SubscribeToFriendsPresence(sm_ActiveGamerIndex, ref, 1);
#endif

			return true;
		}
		else
		{
			rlError("Failed to allocated Friend Reference when adding to array.");
			return false;
		}
	}

	bool rlFriendsManager::RemoveFriendReferenceByIndex(int index)
	{
		rlFriendsReference* ref = NULL;

		rtry
		{
#if RSG_DURANGO
			// Durango's sorted friends list is by status
			rverify(index < sm_FriendReferencesByStatus.GetCount(), catchall, );
			// cache the reference pointer
			ref = sm_FriendReferencesByStatus[index];
			// remove from the id sorted list
			sm_FriendReferencesByStatus.Delete(index);
#else
			rverify(index < sm_FriendReferencesById.GetCount(), catchall, );

			// cache the reference pointer
			ref = sm_FriendReferencesById[index];

			// remove from the id sorted list
			sm_FriendReferencesById.Delete(index);
#endif
		
#if RSG_DURANGO
			// Find it in the id-sorted array, and remove
			int numIdRefs = sm_FriendReferencesById.GetCount();
			for (int i = 0; i < numIdRefs; i++)
			{
				if (sm_FriendReferencesById[i]->m_Id.m_Xuid == ref->m_Id.m_Xuid)
				{
					sm_FriendReferencesById.Delete(i);
					break;
				}
			}
#endif

			rverify(ref, catchall, );

#if RSG_DURANGO
			// Unsubscribe
			g_rlXbl.GetRealtimeActivityManager()->UnsubscribeToFriendsPresence(sm_ActiveGamerIndex, ref, 1);
#endif

			RL_DELETE(ref);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlFriendsManager::IsFriendOnline(const int friendIndex)
	{
#if RSG_DURANGO
		if (rlVerify(friendIndex >= 0 && friendIndex < sm_FriendReferencesByStatus.GetCount()))
		{
			return sm_FriendReferencesByStatus[friendIndex]->m_bIsOnline;
		}
#elif RSG_PC
		if (rlVerify(friendIndex >= 0 && friendIndex < (int)sm_DefaultFriends.m_NumFriends))
		{
			return sm_DefaultFriends.m_Friends[friendIndex].IsOnline();
		}
#elif RSG_ORBIS
		if (rlVerify(friendIndex >= 0 && friendIndex < sm_FriendReferencesById.GetCount()))
		{
			return sm_FriendReferencesById[friendIndex]->m_bIsOnline;
		}
#endif

		return false;
	}

	bool rlFriendsManager::IsFriendInSameTitle(const int friendIndex)
	{
#if RSG_DURANGO
		if (rlVerify(friendIndex >= 0 && friendIndex < sm_FriendReferencesByStatus.GetCount()))
		{
			return sm_FriendReferencesByStatus[friendIndex]->m_bIsInSameTitle;
		}
#elif RSG_PC
		if (rlVerify(friendIndex >= 0 && friendIndex < (int)sm_DefaultFriends.m_NumFriends))
		{
			return sm_DefaultFriends.m_Friends[friendIndex].IsInSameTitle();
		}
#elif RSG_ORBIS
		if (rlVerify(friendIndex >= 0 && friendIndex < sm_FriendReferencesById.GetCount()))
		{
			return sm_FriendReferencesById[friendIndex]->m_bIsInSameTitle;
		}
#endif

		return false;
	}

	bool rlFriendsManager::IsFriendInSession(const int friendIndex)
	{
#if RSG_DURANGO
		if (rlVerify(friendIndex >= 0 && friendIndex < sm_FriendReferencesByStatus.GetCount()))
		{
			return sm_FriendReferencesByStatus[friendIndex]->m_bIsInSession;
		}
#elif RSG_PC
		if (rlVerify(friendIndex >= 0 && friendIndex < (int)sm_DefaultFriends.m_NumFriends))
		{
			return sm_DefaultFriends.m_Friends[friendIndex].IsInSession();
		}
#elif RSG_ORBIS
		if (rlVerify(friendIndex >= 0 && friendIndex < sm_FriendReferencesById.GetCount()))
		{
			return sm_FriendReferencesById[friendIndex]->m_bIsInSession;
		}
#endif

		return false;
	}

	bool rlFriendsManager::CanQueueFriendsRefresh(const int localGamerIndex)
	{
		if (!IsActivated())
		{
			return false;
		}

		// requires a valid local index
		if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		{
			return false;
		}

		// requires local index is online
		if (!rlPresence::IsOnline(localGamerIndex))
		{
			return false;
		}

		rlRosCredentials tCredentials = rlRos::GetCredentials(localGamerIndex);
		if (!tCredentials.IsValid())
		{
			return false;
		}

		// Make sure that the request tasks have finished.
		if (sm_ReadStatus.Pending() || sm_ActivationStatus.Pending() || sm_XblFriendCountStatus.Pending() || sm_XblPresenceSessionStatus.Pending())
		{
			return false;
		}

		unsigned currentTime = sysTimer::GetSystemMsTime();
		unsigned timeSpan = currentTime - sm_LastFriendsRefresh;
		if (timeSpan > sm_TimeBetweenRefreshes)
		{
			return true;
		}

		return false;
	}

	void rlFriendsManager::ForceAllowFriendsRefresh()
	{
		sm_LastFriendsRefresh = 0;
	}

	//PURPOSE
	//	Refreshes data on a subset of friends
	bool rlFriendsManager::RequestRefreshFriendsPage(const int localGamerIndex, rlFriendsPage* page, int startIndex, int numFriends, bool bRefreshPresence)
	{
		rtry
		{
			rlDebug3("RequestRefreshFriendsPage for %d friends starting from index %d. page->m_StartIndex: %d, page->m_NumFriends: %u.", numFriends, startIndex, page ? page->m_StartIndex : 0, page ? page->m_NumFriends : 0);

			// Check that we're even allowed to refresh this page
			rcheck(CanQueueFriendsRefresh(localGamerIndex), catchall, );

			// Make sure that a valid page was passed
			rverify(page, catchall, );

#if RSG_DURANGO
			// Always check for list changes on Durango since we don't get events
			g_rlXbl.GetPlayerManager()->CheckForListChanges(localGamerIndex, &sm_XblFriendCountStatus);
#endif
			
			// If no friends, bail early (nothing to refresh)
			rcheck(numFriends > 0, catchall, );
			rcheck(page->m_NumFriends > 0, catchall, );

			// Validate the indices are acceptable for the given page
			rverify(startIndex >= 0 && startIndex < (int)(page->m_StartIndex + page->m_NumFriends), catchall, );
			rverify(startIndex + numFriends <= (int)(page->m_StartIndex + page->m_MaxFriends), catchall, );

			// If presence only, make sure we are not requesting too many slots for presence
			rverify(!bRefreshPresence || numFriends <= MAX_PRESENCE_PAGE_SIZE, catchall, );

#if RSG_DURANGO

			// Request online/title status
			g_rlXbl.GetPlayerManager()->UpdateFriends(localGamerIndex, page, startIndex - page->m_StartIndex, numFriends, &sm_XblPresenceSessionStatus);

			sm_bSortFriends = true;
#elif RSG_PC
			// Request the SC DLL to update your friends list first
			if (g_rlPc.GetPlayerManager())
			{
				g_rlPc.GetPlayerManager()->RequestFriendSync();
			}

			// Queue a session refresh when the SC DLL has completed its request
			sm_RefreshPresence = bRefreshPresence;
			sm_RefreshPresenceStartIndex = startIndex;
			sm_RefreshPresenceNumFriends = numFriends;
#else
			sm_NeedToRefreshFriends = true;
#endif
			sm_LastFriendsRefresh = sysTimer::GetSystemMsTime();
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlFriendsManager::RequestFriendSync()
	{
		rlDebug3("Requesting friend sync");

#if RSG_PC
		// Only PC needs to sync friends between the DLL and the game.
		sm_NeedToRefreshFriends = true;
		sm_NumFriendsToRefresh = sm_DefaultFriends.m_MaxFriends;
#endif
		return true;
	}

	bool rlFriendsManager::CheckListForChanges(const int DURANGO_ONLY(localGamerIndex))
	{
#if RSG_DURANGO
		// No need to refresh if we're already doing a pending read
		if (!IsReading())
		{
			return g_rlXbl.GetPlayerManager()->CheckForListChanges(localGamerIndex, &sm_XblFriendCountStatus);
		}
#endif

		return true;
	}

	bool rlFriendsManager::CanPresort()
	{
#if RSG_DURANGO
		return false;
#elif RSG_ORBIS
		return true;
#elif RSG_PC
		return false;
#else
		rlAssertf(false, "Not supported on this platform.");
#endif
	}

	void rlFriendsManager::DispatchEvent(const rlFriendEvent* e)
	{
		rlDebug("Dispatching '%s'", e->GetName());
		sm_Delegator.Dispatch(e);
	}

	void rlFriendsManager::AddDelegate(Delegate* dlgt)
	{
		sm_Delegator.AddDelegate(dlgt);
	}

	void rlFriendsManager::RemoveDelegate(Delegate* dlgt)
	{
		sm_Delegator.RemoveDelegate(dlgt);
	}

	void rlFriendsManager::CancelRead(netStatus* status)
	{
		if (status->Pending())
		{
			sm_FriendsReader.Cancel(status);
		}
	}

	void rlFriendsManager::CancelTaskIfNecessary(netStatus* status)
	{
		if (!status->Pending())
			return;

		if (netTask::HasTask(status))
		{
			netTask::Cancel(status);
		}
		else if (rlGetTaskManager() && rlGetTaskManager()->FindTask(status) != NULL)
		{
			rlGetTaskManager()->CancelTask(status);
		}
	}

	bool rlFriendsManager::LookupFriendsName(const int localGamerIndex, const rlGamerHandle* gamerHandles, const unsigned numGamerhandles, rlDisplayName* displayNames, rlGamertag* gamertags, netStatus* status)
	{
#if RSG_DURANGO
		return g_rlXbl.GetProfileManager()->GetPlayerNames(localGamerIndex, gamerHandles, numGamerhandles, displayNames, gamertags, status);
#elif RSG_ORBIS
		return g_rlNp.GetWebAPI().GetPlayerNames(localGamerIndex, gamerHandles, numGamerhandles, displayNames, gamertags, rlNpGetProfilesWorkItem::FLAG_FRIENDS_ONLY | rlNpGetProfilesWorkItem::FLAG_ALLOW_REAL_NAMES, status);
#elif RSG_PC
		(void)localGamerIndex;
		(void)status;
		(void)displayNames;
		for(u32 i=0; i<numGamerhandles; i++)
		{
			rlFriend* myFriend = rlFriendsManager::GetDefaultFriendPage()->GetFriend(gamerHandles[i]);
			if(rlVerifyf(myFriend, "Friend '%" I64FMT "u' is not present in sm_DefaultFriends", gamerHandles[i].GetRockstarId()))
			{
				strcpy_s((char*) &gamertags[i], RL_MAX_NAME_BUF_SIZE, myFriend->GetName());
			}
			else
			{
				status->ForceFailed();
				return false;
			}
		}
		status->ForceSucceeded();
		return true;
#else
		rlAssertf(false, "Not supported on this platform.");
#endif
	}

#if RSG_NP

	void rlFriendsManager::OnNpEvent(rlNp* np, const rlNpEvent* evt)
	{
		const unsigned evtId = evt->GetId();

		if(RLNP_EVENT_FRIEND_LIST_UPDATE == evtId)
		{
			sm_NeedToRefreshFriends = true;
			sm_CheckForListChanges = true;
		}
	}

	void rlFriendsManager::UpdateFriendPresence(const int localGamerIndex, const rlSceNpAccountId accountId, const rlSceNpOnlineId& onlineId, bool bIsOnline, 
												bool bIsInSameTitle, const void *data, const unsigned dataSize, bool bRaiseEvent)
	{
		if (localGamerIndex == sm_ActiveGamerIndex)
		{
			rlFriend f;
			f.Reset(accountId, onlineId, bIsOnline, bIsInSameTitle, (char*)data, dataSize);

			bool bFound = false;
			for (int i = 0; i < sm_DefaultFriends.m_NumFriends; i++)
			{
				// gamer handle comparison
				if (sm_DefaultFriends.m_Friends[i] == f)
				{
					// preserve account id if the new friend didn't have one.
					if (f.GetReference().m_Id.m_AccountId == RL_INVALID_NP_ACCOUNT_ID)
					{
						rlSceNpAccountId accountId = sm_DefaultFriends.m_Friends[i].m_FriendReference.m_Id.m_AccountId;
						sm_DefaultFriends.m_Friends[i] = f;
						sm_DefaultFriends.m_Friends[i].m_FriendReference.m_Id.m_AccountId = accountId;
					}
					else
					{
						// includes online, same title, etc
						sm_DefaultFriends.m_Friends[i] = f;
					}

					bFound = true;
					break;
				}
			}

			// Online friend not in the default page, we need to request the new presorted list from NP
			if (bIsOnline && !bFound)
			{
				sm_NeedToRefreshFriends = true;
			}

			// Is this a new friend?
			rlGamerHandle gh;
			f.GetGamerHandle(&gh);

			int friendIndex = GetFriendIndex(localGamerIndex, gh);
			if (friendIndex == INVALID_FRIEND_INDEX)
			{
				if (rlVerify(gh.GetNpAccountId() != RL_INVALID_NP_ACCOUNT_ID))
				{
					rlFriendId id(gh);
					AddFriendReferenceById(id);
				}
			}
			else
			{
				// update online ID - this may have changed
				if (rlVerify(gh.GetNpOnlineId().IsValid()))
				{
					sm_FriendReferencesById[friendIndex]->m_Id.m_OnlineId = gh.GetNpOnlineId();
				}
			}

			// Update SC subscription
			AddRemoveScSubscription(&gh, bIsOnline);

			rlFriendEventUpdated e(f);
			sm_Delegator.Dispatch(&e); 

			if (bRaiseEvent)
			{
				// This callback includes data for online status, session status and title status. All 3 must be invoked and updated.
				rlFriendEventSessionChanged eSessionEvent(f);
				sm_Delegator.Dispatch(&eSessionEvent); 

				rlFriendEventTitleChanged eTitleEvent(f);
				sm_Delegator.Dispatch(&eTitleEvent); 
			}
		}
	}

#elif RSG_DURANGO

	void rlFriendsManager::OnXblEvent(rlXbl* /* xbl */, const rlXblEvent* evt)
	{
		const unsigned evtId = evt->GetId();

		if (RLXBL_FRIEND_EVENT_LIST_CHANGED == evtId)
		{
			rlXblFriendListChangedEvent* msg = (rlXblFriendListChangedEvent*)evt;
			if (msg)
			{
				rlFriendEventListChanged e;
				sm_Delegator.Dispatch(&e); 
			}
		}
		else if (RLXBL_FRIEND_EVENT_ONLINE_STATUS_CHANGED == evtId)
		{
			rlXblFriendOnlineStatusChangedEvent* msg = (rlXblFriendOnlineStatusChangedEvent*)evt;
			if (msg)
			{
				rlFriend f;
				rlXblFriendInfo xblFriendInfo;
				xblFriendInfo.Clear();

				f.Reset(msg->m_Xuid, xblFriendInfo, msg->m_bIsOnline);

				// If inside our friend's page (cache)
				for (unsigned int i = 0; i < sm_DefaultFriends.m_NumFriends; i++)
				{
					if (sm_DefaultFriends.m_Friends[i].GetXuid() == msg->m_Xuid)
					{
						// includes online, same title, etc
						sm_DefaultFriends.m_Friends[i].SetIsOnline(msg->m_bIsOnline);

						// Copy more information into the friend event
						f = sm_DefaultFriends.m_Friends[i];
					}
				}

				// Find the friend within the giant list of references, update its online status and set the sort flag
				rlFriendId friendId;
				friendId.m_Xuid = msg->m_Xuid;
				int index = BinarySearchFriends(friendId);
				if (index >= 0)
				{
					sm_FriendReferencesById[index]->m_bIsOnline = msg->m_bIsOnline;
					sm_bSortFriends = true;
				}

				// notify our delegate
				rlFriendEventStatusChanged e(f);
				sm_Delegator.Dispatch(&e);
			}
		}
		else if (RLXBL_FRIEND_EVENT_TITLE_STATUS_CHANGED == evtId)
		{
			rlXblFriendTitleStatusChangedEvent* msg = (rlXblFriendTitleStatusChangedEvent*)evt;
			if (msg)
			{
				rlFriend f;
				rlXblFriendInfo xblFriendInfo;
				xblFriendInfo.Clear();

				rlDebug3("rlXblFriendTitleStatusChangedEvent for %" I64FMT "d, in same title: %d", msg->m_Xuid, msg->m_bIsInSameTitle);

				// implicitly online if in the same title
				f.Reset(msg->m_Xuid, xblFriendInfo, true, msg->m_bIsInSameTitle);

				// If inside our friend's page (cache)
				for (unsigned i = 0; i < sm_DefaultFriends.m_NumFriends; i++)
				{
					if (sm_DefaultFriends.m_Friends[i].GetXuid() == msg->m_Xuid)
					{
						// includes online, same title, etc
						sm_DefaultFriends.m_Friends[i].SetIsInSameTitle(msg->m_bIsInSameTitle);

						// Copy more information into the friend event
						f = sm_DefaultFriends.m_Friends[i];
					}
				}

				// Find the friend within the giant list of references, update its title status and set the sort flag
				rlFriendId friendId;
				friendId.m_Xuid = msg->m_Xuid;
				int index = BinarySearchFriends(friendId);
				if (index >= 0)
				{
					sm_FriendReferencesById[index]->m_bIsInSameTitle = msg->m_bIsInSameTitle;

					// If the user is starting the same title, they are implicitly online
					if (msg->m_bIsInSameTitle)
					{
						sm_FriendReferencesById[index]->m_bIsOnline = true;
					}

					// Set the online flag in the friend object
					f.SetIsOnline(sm_FriendReferencesById[index]->m_bIsOnline);

					// Queue a friend sort
					sm_bSortFriends = true;
				}

				// notify our delegate
				rlFriendEventTitleChanged e(f);
				sm_Delegator.Dispatch(&e);
			}
		}
		else if (RLXBL_FRIEND_EVENT_SESSION_CHANGED == evtId)
		{
			rlXblFriendSessionStatusChangedEvent* msg = (rlXblFriendSessionStatusChangedEvent*)evt;
			if (msg)
			{
				rlFriend f;
				rlXblFriendInfo xblFriendInfo;
				xblFriendInfo.Clear();

				f.Reset(msg->m_Xuid, xblFriendInfo);
				f.SetIsInSession(msg->m_bIsInSession);

				// If inside our friend's page (cache)
				for (unsigned i = 0; i < sm_DefaultFriends.m_NumFriends; i++)
				{
					if (sm_DefaultFriends.m_Friends[i].GetXuid() == msg->m_Xuid)
					{
						// includes online, same title, etc
						sm_DefaultFriends.m_Friends[i].SetIsInSession(msg->m_bIsInSession);

						// Copy more information into the friend event
						f = sm_DefaultFriends.m_Friends[i];
					}
				}

				// Find the friend within the giant list of references, update its session status and set the sort flag
				rlFriendId friendId;
				friendId.m_Xuid = msg->m_Xuid;
				int index = BinarySearchFriends(friendId);
				if (index >= 0)
				{
					sm_FriendReferencesById[index]->m_bIsInSession = msg->m_bIsInSession;
					sm_bSortFriends = true;
				}

				// notify our delegate
				rlFriendEventSessionChanged e(f);
				sm_Delegator.Dispatch(&e);
			}
		}
	}

#endif

}   //namespace rage
