// 
// rline/rltelemetry.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rltelemetry.h"
#include "rlpresence.h"
#include "diag/seh.h"
#include "file/stream.h"
#include "parser/manager.h"
#include "parser/memberarray.h"
#include "rline/cloud/rlcloud.h"
#include "ros/rlros.h"
#include "ros/rlroshttptask.h"
#include "string/stringhash.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/simpleallocator.h"
#include "system/threadtype.h"

#include <time.h>

#if !__FINAL
#include "system/timer.h"
#endif

//Used to size the buffer used to export/process the metric to a submission format
//NOTE: this size needs to be at least as large as the largest message.
#define RL_TELEMETRY_BOUNCE_BUF_SIZE    2048

//Size of buffer used to store metrics in binary form prior to submission.
#define RL_METRICS_BUFFER_SIZE          2048

//Flush interval for "immediate" metrics.  Obviously it should be low.
#define RL_TELEMETRY_FLUSH_INTERVAL_IMMEDIATE_SEC   5

PARAM(netTelemetryDump, "[telemetry] If present, dumps all telemetry to logging");
PARAM(netTelemetryDisableDumpOnExportFail, "[telemetry] Disables dumping of metrics which export fails on");
PARAM(netTelemetryForceOn, "[telemetry] Forces telemetry metrics to be enabled even if the config file says otherwise", "", "All", "");
PARAM(netTelemetryForceOff, "[telemetry] Forces telemetry metrics to be disabled even if the config file says otherwise", "", "All", "");
PARAM(netTelemetryGroupForceOn, "[telemetry] Forces telemetry metrics of a certain group to be enabled even if the config file says otherwise", "", "All", "");
PARAM(netTelemetryGroupForceOff, "[telemetry] Forces telemetry metrics of a certain group to be disabled even if the config file says otherwise", "", "All", "");
PARAM(netTelemetryForceOnKeepsProbability, "[telemetry] When set ForceOn maintains the probability. By default ForceOn assumes the probability will be 1.0", "Disabled", "All", "");
PARAM(netTelemetryForceProbability, "[telemetry] Enforces a specific probability value for all metrics", "Disabled", "All", "");
PARAM(netTelemetryConfig, "[telemetry] Loads the telemetry from a file", "", "All", "");
PARAM(netTelemetrySubmissionIntervalSec, "[telemetry] Overwrite the default submission interval", "Disabled", "All", "");
// Legacy macros
PARAM(metricallowlogleveldevoff, "[telemetry] Use this command to turn on metrics on channel LOGLEVEL_DEBUG_NEVER");
PARAM(telemetrychanneldevcaptureloglevel, "[telemetry] Setup telemetry channel dev capture log level.");

namespace rage
{

//Override output macros.
RAGE_DEFINE_SUBCHANNEL(rline,telemetry)
#undef __rage_channel
#define __rage_channel rline_telemetry

AUTOID_IMPL(rlTelemetryEvent);
AUTOID_IMPL(rlTelemetryEventDeferredFlush);
AUTOID_IMPL(rlTelemetryEventFlushEnd);
AUTOID_IMPL(rlTelemetryEventConfigChanged);

//Included in the submission header.
static const unsigned TELEMETRY_VERSION     = 2;
static netRandom s_TelemetryRng((int)time(NULL));

NOTFINAL_ONLY(float s_ForceProbability = -1.0f;)

//Standard header.
class rlTelemetryHeader : public rlMetric
{
    RL_DECLARE_METRIC(HEADER);

public:

    explicit rlTelemetryHeader(const u64 baseTime, const unsigned sequenceNumber)
        : m_BaseTime(baseTime)
        , m_SequenceNumber(sequenceNumber)
    {
    }

    bool Write(RsonWriter* rw) const
    {
        bool success =
            rw->WriteUns64("t", rlGetPosixTime())           //time of submission
                && rw->WriteUns64("bt", m_BaseTime)         //base time
                && rw->WriteUns("seq", m_SequenceNumber)    //sequence number
                && rw->WriteUns("ver", TELEMETRY_VERSION);  //version

        if(success)
        {
		    const rlTelemetryPolicies& policies = rlTelemetry::GetPolicies();
		    if(policies.GameHeaderInfoCallback.IsBound() && !rw->HasError())
		    {
			    int cursor;
			    success = false;
			    if (rw->Begin("game", &cursor))					//game
			    {
                    const int gameHeaderBegin = rw->GetCursor();
				    success = policies.GameHeaderInfoCallback(rw);
                    if(rw->GetCursor() == gameHeaderBegin)
                    {
                        //The game didn't write anything.
                        rw->Undo(cursor);
                    }
                    else
                    {
				        rw->End();
                    }
			    }
            }
        }

        return success;
    }

private:

    u64 m_BaseTime;
    unsigned m_SequenceNumber;
};

//////////////////////////////////////////////////////////////////////////
//  rlRosTelemetrySubmissionTask
//////////////////////////////////////////////////////////////////////////
class rlRosTelemetrySubmissionTask : public rlRosHttpTask
{
#if !__FINAL
private:
	rlTelemetryBandwidth::eRecordTypes  m_recordType;
#endif

public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosTelemetrySubmissionTask);

	rlRosTelemetrySubmissionTask() { NOTFINAL_ONLY( m_recordType=rlTelemetryBandwidth::NORMAL_METRICS; ) };
	virtual ~rlRosTelemetrySubmissionTask() {};

	bool Configure(const rlTelemetry::GamerSlot* slot);
	bool Configure(const int localGamerIndex,
			const rlMetricList* metricList,
			const rlTelemetrySubmissionMemoryPolicy* pMemPolicy);

#if !__FINAL
	bool Configure(const int localGamerIndex,
		const rlMetricList* metricList,
		const rlTelemetrySubmissionMemoryPolicy* pMemPolicy,
		const rlTelemetryBandwidth::eRecordTypes recordtype);
#endif //!__FINAL

protected:
    virtual const char* GetServiceMethod() const override
    {
        return "Telemetry.asmx/SubmitCompressed";
    }

	virtual bool  ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;

private:

	bool CompressAndAppend(zlibStream* zstrm, 
							const char* srcBuf, const unsigned srcBufLen, 
							char* pCompressBuff, const unsigned compressedBufLen)
	{
		unsigned totalConsumed = 0;
		bool appendSuccessful = true;
		while(totalConsumed < srcBufLen && !zstrm->HasError() && appendSuccessful)
		{
			unsigned numConsumed, numProduced;

			zstrm->Encode(&srcBuf[totalConsumed]
							,srcBufLen - totalConsumed
							,pCompressBuff
							,compressedBufLen
							,&numConsumed
							,&numProduced);

			if(numProduced)
			{
				rlAssert(numProduced < compressedBufLen);
				appendSuccessful = AppendContent(pCompressBuff, numProduced);
				
				if(!appendSuccessful)
				{
					rlTaskError("Error appending compressed telemetry, compressedBufLen='%d', numProduced='%d'.", compressedBufLen, numProduced);
				}
			}

			totalConsumed += numConsumed;
		}

		if(zstrm->HasError())
		{
			rlTaskError("Error compressing telemetry: %d", zstrm->GetErrorCode());
		}

		return appendSuccessful && !zstrm->HasError();
	}

    bool Flush(zlibStream* zstrm)
    {
		bool bAppendSuccess = true;
        if(zstrm && zstrm->GetNumConsumed())
        {
            while(!zstrm->IsComplete() && !zstrm->HasError() && bAppendSuccess)
            {
                char compressedBuf[RL_TELEMETRY_DEFAULT_WORK_BUFFER_SIZE];
                unsigned numConsumed, numProduced;
                zstrm->Encode(NULL,
                                0,
                                compressedBuf,
                                sizeof(compressedBuf),
                                &numConsumed,
                                &numProduced);

                if(numProduced)
                {
                    bAppendSuccess = AppendContent(compressedBuf, numProduced);
                }
            }

            if(zstrm->HasError() || !bAppendSuccess)
            {
				rlTaskError("Error compressing telemetry: ZError[%d] - appendsuccess[%s] ", zstrm->GetErrorCode(), bAppendSuccess ? "true" : "false");
            }
            else
            {
				NOTFINAL_ONLY( rlTelemetry::AddBandwidthRecord(m_recordType, zstrm->GetNumProduced(), zstrm->GetNumConsumed()); )

                rlDebug3("Compression ratio: %d/%d = %0.2f%% (%0.2f to 1)",
                        zstrm->GetNumProduced(), zstrm->GetNumConsumed(),
                        100*(1.0-((double)zstrm->GetNumProduced()/zstrm->GetNumConsumed())),
                        (double)zstrm->GetNumConsumed()/zstrm->GetNumProduced());
            }

			
        }
		
		return bAppendSuccess && !zstrm->HasError();
    }
};

#define RL_TELEMETRY_NEW_WAY 0

#if !__FINAL
bool
rlRosTelemetrySubmissionTask::Configure(const int localGamerIndex,
										const rlMetricList* metricList,
										const rlTelemetrySubmissionMemoryPolicy* pMemPolicy,
										const rlTelemetryBandwidth::eRecordTypes recordtype)
{
	if (Configure(localGamerIndex, metricList, pMemPolicy))
	{
		m_recordType = recordtype;
		return true;
	}

	return false;
}
#endif

bool
rlRosTelemetrySubmissionTask::Configure(const int localGamerIndex,
                                        const rlMetricList* metricList,
                                        const rlTelemetrySubmissionMemoryPolicy* memPolicy)
{
	bool success = false;

	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	u8* zlibHeap = 0;

	rtry
	{
		rcheck(rlPresence::IsSignedIn(localGamerIndex),
			catchall, 
			rlTaskError("Gamer is not signed in"));

		rcheck(rlPresence::IsOnline(localGamerIndex),
			catchall, 
			rlTaskError("Gamer is offline"));

		rverify(metricList,
			catchall,
			rlTaskError("No metrics to submit"));

		rverify((memPolicy != NULL 
				&& memPolicy->m_pExportWorkBuffer != NULL 
				&& memPolicy->m_ExportWorkBufferLen > 0 
				&& memPolicy->m_pCompressionWorkBuffer != NULL 
				&& memPolicy->m_CompressionWorkBufferLen > 0), 
				catchall, 
				rlTaskError("Invalid rlTelemetrySubmissionMemoryPolicy"));

		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		
		rverify(creds.IsValid(),
			catchall,
			rlTaskError("Credentials are invalid"));

		rverify(AddStringParameter("ticket", creds.GetTicket(), true),
			catchall,
			rlTaskError("Failed to add ticket param"));

		//Start an empty "data=" parameter.  We'll fill it out by calling
		//AppendContent below.
		rverify(AddStringParameter("data", "", true),
			catchall,
			rlTaskError("Failed to add data param"));

		//Smash the metric into the given buffer.
		RsonWriter rw;
		rw.Init(memPolicy->m_pExportWorkBuffer, memPolicy->m_ExportWorkBufferLen, RSON_FORMAT_JSON);

		//Open the outer container.  It's an array of RSON objects.
		rverify(rw.BeginArray(NULL, NULL),
			catchall,
			rlTaskError("RSON buffer too small"));

		rlTaskDebug3("metricList->GetCount() = %u", metricList->GetCount());

		//Time from the first metric.  All metric timestamps
		//will be sent as offsets from this in order to save
		//bandwidth.
        const rlMetric* pMetricIter = metricList->GetFirst();
		const u64 baseTime = pMetricIter->GetMetricTime();

#if RL_TELEMETRY_NEW_WAY
        rlTelemetryHeader header(baseTime, 0);
        header.m_PosixTime = baseTime;

		rverify(rlTelemetry::Export(&rw, NULL, header.GetMetricTime() - baseTime, header),
                catchall,
				rlTaskError("Unable to export header '%s' into buffer of size %d", header.GetMetricName(), memPolicy->m_ExportWorkBufferLen));
#else
		rlTelemetry::WriteHeader(baseTime, NULL, &rw);
#endif

		rverify(!rw.HasError(),
			catchall,
			rlTaskError("RSON buffer too small to write header"));

		const size_t ZLIB_HEAP_SIZE = 10 * 1024; //Zlib needs a crap ton of memory to do it's work.  .
		zlibHeap = (u8*)RL_ALLOCATE(rlRosTelemetrySubmissionTask, ZLIB_HEAP_SIZE);
		rverify(zlibHeap, 
			catchall, 
			rlTaskError("Failed to allocate %" SIZETFMT "d for tmp buffer", ZLIB_HEAP_SIZE));

		sysMemSimpleAllocator allocator(zlibHeap, ZLIB_HEAP_SIZE, 7);
		
#if RAGE_TRACKING
		allocator.SetTallied(false);
#endif // RAGE_TRACKING

		zlibStream zstrm;
		zstrm.BeginEncode(&allocator);

		rlTaskDebug3("BeginEncode.");

		rverify(CompressAndAppend(&zstrm, memPolicy->m_pExportWorkBuffer, rw.Length(), memPolicy->m_pCompressionWorkBuffer, memPolicy->m_CompressionWorkBufferLen),
			catchall,
			rlTaskError("CompressAndAppend failed finish compressing '%d'.", rw.Length()));

		rw.ResetToContinue();

		rlTaskDebug3("Start exporting each metric to text and encode it into the zlib stream.");

		//Start exporting each metric to text and encode it into the zlib stream.  
		//If we run out of room, we fail.
		while (pMetricIter)
		{
			//Export into the buffer
			//Times are offsets from base time in order to save space.
			if(rlVerifyf(rlTelemetry::Export(&rw, NULL, pMetricIter->GetMetricTime() - baseTime, *pMetricIter),  
					"Unable to export metric '%s' into buffer of size %d.", 
					pMetricIter->GetMetricName(), memPolicy->m_ExportWorkBufferLen))
			{
				rlTaskDebug3("Exported %s with size %d", pMetricIter->GetMetricName(), rw.Length());
				
				rverify(CompressAndAppend(&zstrm, memPolicy->m_pExportWorkBuffer, rw.Length(), memPolicy->m_pCompressionWorkBuffer, memPolicy->m_CompressionWorkBufferLen),
					catchall,
					rlTaskError("CompressAndAppend failed on metric '%s' of exported size %d", pMetricIter->GetMetricName(), rw.Length()));
			}
#if !__NO_OUTPUT
			else
			{
				// If Export fails, dump out the metric JSON so we can look at it.
				if(!PARAM_netTelemetryDisableDumpOnExportFail.Get())
				{
					char metricBuf[2048] = {0};
					RsonWriter debugRw(metricBuf, RSON_FORMAT_JSON);
					pMetricIter->Write(&debugRw);
					rlDebug1("Export failed, dumping metric '%s' (Length: %u):", pMetricIter->GetMetricName(), debugRw.Length());
					diagLoggedPrintLn(debugRw.ToString(), debugRw.Length());
				}
			}

			// Dump out what we wrote.
			if(PARAM_netTelemetryDump.Get())
			{
				diagLoggedPrintLn(rw.ToString(), rw.Length());
			}
#endif //!__NO_OUTPUT

			//Clear out pass-thru json maker, but retain the json object info
			rw.ResetToContinue();

			//move on to the next one
			pMetricIter = pMetricIter->m_Next;
		}

		//Close the outer container.
        if(!rw.End())
        {
			rverify(CompressAndAppend(&zstrm, memPolicy->m_pExportWorkBuffer, rw.Length(), memPolicy->m_pCompressionWorkBuffer, memPolicy->m_CompressionWorkBufferLen), 
				catchall,
				rlTaskError("CompressAndAppend failed finish compressing '%d'", rw.Length()));

            //Try again
	    	rw.ResetToContinue();
            rw.End();
        }

		rverify(CompressAndAppend(&zstrm, memPolicy->m_pExportWorkBuffer, rw.Length(), memPolicy->m_pCompressionWorkBuffer, memPolicy->m_CompressionWorkBufferLen), 
			catchall,
			rlTaskError("CompressAndAppend failed finish compressing '%d'", rw.Length()));

		//Call Flush to finish it off.
		rverify(Flush(&zstrm), 
			catchall, 
			rlTaskError("Failed to Flush(zlibStream)"));

		success = true;
	}
	rcatchall
	{
	}

	if (zlibHeap)
	{
		RL_FREE(zlibHeap);
		zlibHeap = 0;
	}

	return success;
}

bool 
rlRosTelemetrySubmissionTask::Configure(const rlTelemetry::GamerSlot* slot)
{
	int localGamerIndex = slot->m_GamerHandle.GetLocalIndex();
	const rlMetricList* metricList = &slot->m_Metrics;

	rlTelemetrySubmissionMemoryPolicy memPolicy(RL_TELEMETRY_DEFAULT_WORK_BUFFER_SIZE);

	bool success = Configure(localGamerIndex, metricList, &memPolicy);

	memPolicy.ReleaseAndClear();

	return success;
}

bool 
rlRosTelemetrySubmissionTask::ProcessSuccess(const rlRosResult&, const parTreeNode* node, int& /*resultCode*/)
{
    rlAssert(node);

	NOTFINAL_ONLY( rlTelemetry::SetAddBandwidthRecordBytesSent(m_recordType, m_HttpRequest.GetOutContentBytesSent()); )

    unsigned secsUntilNextWrite = 0;
	int currentVersion = 0;

    if(rlHttpTaskHelper::ReadUns(secsUntilNextWrite, node, "SecsUntilNextWrite", NULL))
    {
        if (rlTelemetry::GetPolicies().m_SubmissionIntervalSeconds != secsUntilNextWrite NOTFINAL_ONLY(&& !PARAM_netTelemetrySubmissionIntervalSec.Get()))
        {
            rlTelemetry::SetSubmissionInterval(secsUntilNextWrite);
        }
    }
    else
    {
        rlTaskError("Failed to read 'SecsUntilNextWrite' attribute");
    }

    // This parameter is optional.
    rlHttpTaskHelper::ReadInt(currentVersion, node, "CurrentVersion", NULL);
    rlDebug2("rlRosTelemetrySubmissionTask - success. Local version[%d] Server version[%d]", rlTelemetry::GetPolicies().GetTelemetryVersion(), currentVersion);

    if (currentVersion != 0 && currentVersion != rlTelemetry::GetPolicies().GetTelemetryVersion())
    {
#if RL_USE_TELEMETRY_CLIENT_CONFIG_FILE
        rlTelemetry::sm_TelemetryClientConfig.TelemetryVersionChanged(currentVersion);
#endif
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////
//  rlRosTelemetryImmediateSubmissionTask
//////////////////////////////////////////////////////////////////////////
class rlRosTelemetryImmediateSubmissionTask : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosTelemetryImmediateSubmissionTask);

    rlRosTelemetryImmediateSubmissionTask() {};
	virtual ~rlRosTelemetryImmediateSubmissionTask() {};

	bool Configure(const rlTelemetry::GamerSlot* slot);

    virtual const char* GetServiceMethod() const override
    {
        return "Telemetry.asmx/SubmitRealTime";
    }

    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
};

#undef RL_TELEMETRY_NEW_WAY
#define RL_TELEMETRY_NEW_WAY 1

bool
rlRosTelemetryImmediateSubmissionTask::Configure(const rlTelemetry::GamerSlot* slot)
{
	int localGamerIndex = slot->m_GamerHandle.GetLocalIndex();
	const rlMetricList* metricList = &slot->m_Metrics;
    const unsigned sequenceNumber = slot->m_RtSequenceNumber;

	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	rtry
	{
		rcheck(rlPresence::IsSignedIn(localGamerIndex),
			catchall, 
			rlTaskError("Gamer is not signed in"));

		rcheck(rlPresence::IsOnline(localGamerIndex),
			catchall, 
			rlTaskError("Gamer is offline"));

		rverify(metricList,
			catchall,
			rlTaskError("No metrics to submit"));

		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		
		rverify(creds.IsValid(),
			catchall,
			rlTaskError("Credentials are invalid"));

		rverify(AddStringParameter("ticket", creds.GetTicket(), true),
			catchall,
			rlTaskError("Failed to add 'ticket' param"));

		//Start an empty "events=" parameter.  We'll fill it out by calling
		//AppendContentUrlEncoded below.
        //FIXME(KB) - remove this after 2013-08-22
		rverify(AddStringParameter("events", "", true),
			catchall,
			rlTaskError("Failed to add 'events' param"));

#if RL_TELEMETRY_NEW_WAY
		//Start an empty "data=" parameter.  We'll fill it out by calling
		//AppendContent below.
		rverify(AddStringParameter("data", "", true),
			catchall,
			rlTaskError("Failed to add 'data' param"));
#endif

		char workBuf[2048];
		RsonWriter rw(workBuf, RSON_FORMAT_JSON);

		//Open the outer container.  It's an array of RSON objects.
		rverify(rw.BeginArray(NULL, NULL),
			catchall,
			rlTaskError("RSON buffer too small"));

        const rlMetric* pMetricIter = metricList->GetFirst();

		//Time from the first metric.  All metric timestamps
		//will be sent as offsets from this in order to save
		//bandwidth.
		const u64 baseTime = pMetricIter->GetMetricTime();

        rlTelemetryHeader header(baseTime, sequenceNumber);
        header.m_PosixTime = baseTime;

		rverify(rlTelemetry::Export(&rw, NULL, header.GetMetricTime() - baseTime, header),
                catchall,
				rlTaskError("Unable to export header '%s' into buffer of size %d", header.GetMetricName(), RL_TELEMETRY_DEFAULT_WORK_BUFFER_SIZE));

		rverify(!rw.HasError(),
			catchall,
			rlTaskError("RSON buffer too small to write header"));

        //Export the metrics
		while (pMetricIter)
		{
			//Export into the buffer
            for(int i = 0; i < 2; ++i)
            {
    			if(!rlTelemetry::Export(&rw, NULL, pMetricIter->GetMetricTime() - baseTime, *pMetricIter))
                {
                    //Buffer filled up - send it then try
                    //again to export the metric.

                    rverify(0 == i,
                            catchall,
                            rlTaskError("Failed to export metric '%s' after two attempts",
                                        pMetricIter->GetMetricName()));
#if RL_TELEMETRY_NEW_WAY
                    rverify(AppendContent(workBuf, rw.Length()),
                            catchall,
                            rlTaskError("Error sending telemetry buffer"));
#else
                    rverify(AppendContentUrlEncoded(workBuf, rw.Length()),
                            catchall,
                            rlTaskError("Error sending telemetry buffer"));
#endif

        			//Try again
	        		rw.ResetToContinue();
                    continue;
                }

				rlTaskDebug3("Exported %s with size %d", pMetricIter->GetMetricName(), rw.Length());

                break;
			}

			//move on to the next one
			pMetricIter = pMetricIter->m_Next;
		}

		//Close the outer container.
		if(!rw.End())
		{
            //Buffer filled up - send it
#if RL_TELEMETRY_NEW_WAY
            rverify(AppendContent(workBuf, rw.Length()),
                    catchall,
                    rlTaskError("Error sending telemetry buffer"));
#else
            rverify(AppendContentUrlEncoded(workBuf, rw.Length()),
                    catchall,
                    rlTaskError("Error sending telemetry buffer"));
#endif

            //Try again
	    	rw.ResetToContinue();
            rw.End();
		}

#if RL_TELEMETRY_NEW_WAY
        rverify(AppendContent(workBuf, rw.Length()),
                catchall,
                rlTaskError("Error sending telemetry buffer"));
#else
        rverify(AppendContentUrlEncoded(workBuf, rw.Length()),
                catchall,
                rlTaskError("Error sending telemetry buffer"));
#endif

		NOTFINAL_ONLY( rlTelemetry::AddBandwidthRecord(rlTelemetryBandwidth::IMMEDIATE_METRICS, rw.Length(), rw.Length()); )

        return true;
	}
	rcatchall
	{
	}

	return false;
}

bool 
rlRosTelemetryImmediateSubmissionTask::ProcessSuccess(const rlRosResult&, const parTreeNode* NET_ASSERTS_ONLY(node), int& /*resultCode*/)
{
    rlAssert(node);

	NOTFINAL_ONLY( rlTelemetry::SetAddBandwidthRecordBytesSent(rlTelemetryBandwidth::IMMEDIATE_METRICS, m_HttpRequest.GetOutContentBytesSent()); )

    return true;
}

//////////////////////////////////////////////////////////////////////////
//  rlRosTelemetryGetClientConfigTask
//////////////////////////////////////////////////////////////////////////
class rlRosGetTelemetryClientConfigTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlRosGetTelemetryClientConfigTask);
    RL_TASK_USE_CHANNEL(rline_ros);

    rlRosGetTelemetryClientConfigTask()
        : m_Config(nullptr)
    {
    }

    virtual ~rlRosGetTelemetryClientConfigTask() {}

    bool Configure(const int localGamerIndex, rlTelemetryPolicies* config);

    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    virtual const char* GetServiceMethod() const
    {
        return "Telemetry.asmx/GetTelemetryClientConfig";
    }

private:
	rlTelemetryPolicies* m_Config;
};

bool
rlRosGetTelemetryClientConfigTask::Configure(const int localGamerIndex, rlTelemetryPolicies* config)
{
    rtry
    {
        rverify(config,
            catchall,
            rlTaskError("config is null"));

        rverify(rlRosHttpTask::Configure(localGamerIndex),
            catchall,
            rlTaskError("Failed to configure rlRosHttpTask"));

        const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);

        rverify(creds.IsValid(),
            catchall,
            rlTaskError("Credentials are invalid"));

        rverify(AddStringParameter("ticket", creds.GetTicket(), true),
            catchall,
            rlTaskError("Failed to add ticket param"));

        m_Config = config;
    }
    rcatchall
    {
        return false;
    }

    return true;
}

bool
rlRosGetTelemetryClientConfigTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& resultCode)
{
    rtry
    {
        rverify(m_Config, catchall, rlTaskError("m_MetricsConfig is null"));

        rcheck(node, catchall, rlTaskError("Failed to find <Response> element"));

        const parElement* el = &node->GetElement();
        rcheck(el, catchall, rlTaskError("GetElement is null"));

        const parAttribute* attr = el->FindAttribute("IsTelemetryEnabled");
        rcheck(attr, catchall, rlTaskError("Failed to find 'IsTelemetryEnabled' attribute"));

        const bool telemetryEnabled = attr->FindBoolValue();

        const parAttribute* pathAttr = el->FindAttribute("Path");
        const char* pathStr = pathAttr ? pathAttr->GetStringValue() : nullptr;

        const parTreeNode* rawDataNode = node->FindChildWithName("RawConfig");
        const char* rawConfigStr = rawDataNode ? rawDataNode->GetData() : nullptr;

        const bool hasPath = pathStr && pathStr[0] != 0;
        const bool hasRaw = rawConfigStr && rawConfigStr[0] != 0;

        // If path and rawConfig are empty it also means telemetry is disabled
        if (!telemetryEnabled || (!hasPath && !hasRaw))
        {
            m_Config->TurnTelemetryOff(); // This will turn all telemetry off except for eventual debug options
        }
        else
        {
            // If we have a raw config we apply it for the time being
            if (hasRaw)
            {
                const unsigned len = static_cast<unsigned>(strlen(rawConfigStr));
                rcheck(m_Config->LoadMetricsFromJson(rawConfigStr, len), catchall, rlTaskError("LoadMetricsFromJson failed"));
            }

            // If we have a path we download that
            if (hasPath)
            {
                rcheck(pathStr && static_cast<unsigned>(strlen(pathStr)) < rlTelemetryPolicies::MAX_METRIC_CONFIG_CLOUD_FILE_NAME, catchall, rlTaskError("The path is too long"));
                safecpy(m_Config->m_CloudFile, pathStr); // We need a second step to download the config
            }
        }

    }
    rcatchall
    {
        resultCode = rlTelemetryPolicies::CONFIG_PARSING_ERROR;

        m_Config->ResetMetricsToDefaults();

        return false;
    }

    return true;
}

#if !__FINAL

//////////////////////////////////////////////////////////////////////////
//  rlTelemetryBandwidth
//////////////////////////////////////////////////////////////////////////

rlTelemetryBandwidth::sRecord::sRecord() 
: m_time(0)
, m_produced(0)
, m_consumed(0) 
, m_bytesSent(0)
{;}

rlTelemetryBandwidth::rlTelemetryBandwidth( )
{
	m_records.Reset();
	m_records.Resize(MAX_NUM_RECORDS);

	m_totalHistory.Reset();
	m_totalHistory.Resize(MAX_NUM_RECORDS);
}

rlTelemetryBandwidth::~rlTelemetryBandwidth( )
{
	m_records.clear();
	m_totalHistory.clear();
}

void rlTelemetryBandwidth::AddRecord(const eRecordTypes record, const u32 produced, const u32 consumed)
{
	m_records[record].m_produced = produced;
	m_records[record].m_consumed = consumed;

	m_totalHistory[record].m_produced += produced;
	m_totalHistory[record].m_consumed += consumed;
}

void rlTelemetryBandwidth::SetRecordBytesSent(const eRecordTypes record, const u32 bytes)
{
	m_records[record].m_time      = sysTimer::GetSystemMsTime();
	m_records[record].m_bytesSent = bytes;

	m_totalHistory[record].m_bytesSent += bytes;
}

const char* rlTelemetryBandwidth::GetRecordName(const eRecordTypes record) const 
{
	switch(record)
	{
	case NORMAL_METRICS: return "NORMAL";
	case DEBUG_METRICS: return "DEBUG";
	case IMMEDIATE_METRICS: return "IMMEDIATE";
	}

	return "UNKNOWN";
}

#endif //!__FINAL

//////////////////////////////////////////////////////////////////////////
//  rlTelemetrEvents
//////////////////////////////////////////////////////////////////////////
rlTelemetryEventFlushEnd::rlTelemetryEventFlushEnd(const netStatus* status)
	: m_Status(status)
{

}

rlTelemetryEventConfigChanged::rlTelemetryEventConfigChanged(const int version)
	: m_Version(version)
{

}

//////////////////////////////////////////////////////////////////////////
//  rlMetricConfigs
//////////////////////////////////////////////////////////////////////////

rlMetricsConfig::rlMetricsConfig()
{

}

rlMetricsConfig::~rlMetricsConfig()
{
	Reset();
}

const rlMetricConfig*
rlMetricsConfig::GetMetricConfig(atHashString hMetric) const
{
	return m_MetricConfigs.SafeGet(hMetric);
}

void
rlMetricsConfig::Reset()
{
	m_MetricConfigs.Reset();
}

bool
rlMetricsConfig::operator==(const rlMetricsConfig& other) const
{
	if (m_MetricConfigs.GetCount() != other.m_MetricConfigs.GetCount())
	{
		return false;
	}

	atBinaryMap<rlMetricConfig, atHashString>::ConstIterator iterMe = m_MetricConfigs.begin();
	atBinaryMap<rlMetricConfig, atHashString>::ConstIterator iterOther = other.m_MetricConfigs.begin();

	while (iterMe != m_MetricConfigs.end() && iterOther != other.m_MetricConfigs.end())
	{
		if ((iterMe.GetKey() != iterOther.GetKey()) || !(*iterMe == *iterOther))
		{
			return false;
		}

		++iterMe;
		++iterOther;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////
//  rlTelemetryPolicies
//////////////////////////////////////////////////////////////////////////
rlTelemetryPolicies::rlTelemetryPolicies()
{
	Clear();
}

void
rlTelemetryPolicies::ResetMetricsToDefaults()
{
	ClearMetrics();
	ApplyDefaultMetrics();
	NOTFINAL_ONLY(ApplyDebugMetrics());
}

void
rlTelemetryPolicies::TurnTelemetryOff()
{
	ClearMetrics();
	m_IsTelemetryEnabled = false;
	NOTFINAL_ONLY(ApplyDebugMetrics());
}

bool
rlTelemetryPolicies::LoadMetricsFromJson(const char* json, const unsigned jsonStringLength)
{
	char tempBuf[MAX_METRIC_CONFIG_CLOUD_FILE_NAME]; // we retain the cloud path for debugging purpose
	safecpy(tempBuf, m_CloudFile);

	ClearMetrics(); // clear out the current options
	ApplyDefaultMetrics(); // Apply the defaults now so the real settings can overwrite them if needed.

	safecpy(m_CloudFile, tempBuf);

	RsonReader reader;

	bool success = false;

	char platform[64] = { 0 };
	char title[64] = { 0 };

	bool telemetryEnabled = false;

	atBinaryMap<rlMetricConfig, atHashString>& cfgs = m_MetricsConfig.m_MetricConfigs;

	rtry
	{
		rverify(reader.Init(json, jsonStringLength), catchall, rlError("Invalid json [%s]", json));

		RsonReader telemetryConfig;
		rverify(reader.GetMember("TelemetryConfig", &telemetryConfig), catchall, rlError("TelemetryConfig not found"));

		rverify(telemetryConfig.ReadBool("IsTelemetryEnabled", telemetryEnabled), catchall, rlError("IsTelemetryEnabled not found"));

		if (!telemetryEnabled)
		{
			TurnTelemetryOff(); // This will turn all telemetry off except for eventual debug options
			return true;
		}

		RsonReader targetEnvironment;
		rverify(reader.GetMember("TargetEnvironment", &targetEnvironment), catchall, rlError("'TargetEnvironment' not found"));

		rverify(targetEnvironment.ReadString("Platform", platform), catchall, rlError("'platform' not found in TargetEnvironment"));
		rverify(targetEnvironment.ReadString("Title", title), catchall, rlError("'title' not found in TargetEnvironment"));
		rverify(targetEnvironment.ReadInt("Version", m_TelemetryVersion), catchall, rlError("'version' not found in TargetEnvironment"));

		// For now I ignore platform and title as those values don't always return consistent data (example: rdr2app vs. bobapp)

		RsonReader iter;
		reader.GetMember("Metrics", &iter);

		bool hasMore = iter.GetFirstMember(&iter);
		while (hasMore)
		{
			bool enabled = false;
			rverify(iter.ReadBool("enabled", enabled), catchall, rlError("'enabled' not found in metric"));

			char metricName[128] = { 0 };
			rverify(iter.ReadString("name", metricName), catchall, rlError("'name' not found in metric"));

			u32 intervalMs = 0;
			if (iter.ReadUns("intervalMs", intervalMs))
			{
				rlDebug("Setting %s telemetry send interval to %u ms", metricName, intervalMs);
			}
			else
			{
				intervalMs = 0;
			}

			// sampling probability defaults to 1.0 (always send if enabled) unless overridden by the config
			float probability = 1.0f;
			if (iter.ReadFloat("probability", probability))
			{
				rlDebug("Setting %s telemetry send probability to %.4f %s", metricName, probability,
					NOTFINAL_ONLY(s_ForceProbability >= 0.0f ? "BUT it will be ignored due to netTelemetryForceProbability" : ) "");
			}
			else
			{
				probability = 1.0f;
			}

			const atHashString key(metricName);
			const rlMetricConfig config(enabled, probability, s_TelemetryRng.GetFloat(), intervalMs);

			if (cfgs.Has(key))
			{
				int idx = cfgs.GetIndex(key);
				if (idx >= 0)
				{
					cfgs.GetItem(idx)->SetEnabled(enabled);
				}
			}
			else
			{
				cfgs.Insert(key, config);
			}

			hasMore = iter.GetNextSibling(&iter);
		}

		cfgs.FinishInsertion();

		m_ConfigVersionHistory.Insert(m_TelemetryVersion);
		m_IsTelemetryEnabled = telemetryEnabled;

		success = true;

		// Done at the end as this can change m_IsTelemetryEnabled and debug wins over all other settings
		NOTFINAL_ONLY(ApplyDebugMetrics());
	}
	rcatchall
	{
		ResetMetricsToDefaults();
	}

	return success;
}

#if !RSG_FINAL
bool
rlTelemetryPolicies::LoadFromJsonFile(const char* path)
{
	bool success = false;
	char* buf = nullptr;
	rtry
	{
		fiStream* fileStream = ASSET.Open(path, NULL);
		rverify(fileStream, catchall,);

		const int len = fileStream->Size();
		rverify(len > 0, catchall,);

		buf = rage_new char[len + 2];
		rverify(buf, catchall,);

		const int num = fileStream->Read(buf, len);
		rverify(num == len, catchall,);

		buf[len] = 0;

		rcheck(LoadMetricsFromJson(buf, static_cast<unsigned>(len)), catchall,);

		success = true;
	}
	rcatchall
	{
	}

	if (buf)
	{
		delete[] buf;
	}

	return success;
}
#endif //!RSG_FINAL

#if RSG_OUTPUT
void
rlTelemetryPolicies::DumpSettings() const
{
	static const char* DEBUG_USAGE_STRING[] = {" default ", "force on ", "force off"};
    rlDisplay("#### Telemetry settings START ####");

    rlDisplay("Version[%d]   IsEnabled[%s]   SubmissionInterval[%u]   SubmissionTimeout[%u]   CloudFile[%s]  " NOTFINAL_ONLY("DebugUsage[%s]")
        , m_TelemetryVersion, IsTelemetryEnabled() ? "yes" : "no", m_SubmissionIntervalSeconds, m_SubmissionTimeoutSeconds, m_CloudFile NOTFINAL_ONLY(, DEBUG_USAGE_STRING[(unsigned)m_AllDebugUsage]));

    int num = 1;
    for (atBinaryMap<rlMetricConfig, atHashString>::ConstIterator iter = m_MetricsConfig.m_MetricConfigs.begin(); iter != m_MetricsConfig.m_MetricConfigs.end(); ++iter)
    {
        rlDisplay("config %03d: enabled%s  prob[%.02f]" NOTFINAL_ONLY("  debug[%s]") "  id[%s]", num,
            (*iter).IsEnabled() ? "[yes]" : "[no] ", (*iter).GetProbability() NOTFINAL_ONLY(, DEBUG_USAGE_STRING[(unsigned)(*iter).GetDebugUsage()]), iter.GetKey().TryGetCStr());
        ++num;
    }

#if !__FINAL
	for (int i = 0; i < rlTelemetryPolicies::MAX_NUM_LOG_CHANNELS; ++i)
	{
		rlDisplay("%s (idx.%d) = %s", m_Channels[i].m_Id.TryGetCStr(), i, DEBUG_USAGE_STRING[(unsigned)m_Channels[i].m_Usage]);
	}
#endif

    rlDisplay("####  Telemetry settings END  ####");

	{
		char buffer[256];
		rlTelemetry::WriteLineToStream("------------ SETTINGS CHANGED ------------\n");
		formatf(buffer, "Version[%d]   IsEnabled[%s]   SubmissionInterval[%u]   SubmissionTimeout[%u]   CloudFile[%s]  " NOTFINAL_ONLY("DebugUsage[%s]")
			, m_TelemetryVersion, IsTelemetryEnabled() ? "yes" : "no", m_SubmissionIntervalSeconds, m_SubmissionTimeoutSeconds, m_CloudFile NOTFINAL_ONLY(, DEBUG_USAGE_STRING[(unsigned)m_AllDebugUsage]));
		rlTelemetry::WriteLineToStream(buffer);

		num = 1;
		for (atBinaryMap<rlMetricConfig, atHashString>::ConstIterator iter = m_MetricsConfig.m_MetricConfigs.begin(); iter != m_MetricsConfig.m_MetricConfigs.end(); ++iter)
		{
			formatf(buffer, "config %03d: enabled%s  prob[%.02f]" NOTFINAL_ONLY("  debug[%s]") "  id[%s]", num,
				(*iter).IsEnabled() ? "[yes]" : "[no] ", (*iter).GetProbability() NOTFINAL_ONLY(, DEBUG_USAGE_STRING[(unsigned)(*iter).GetDebugUsage()]), iter.GetKey().TryGetCStr());
			rlTelemetry::WriteLineToStream(buffer);
			++num;
		}

		rlTelemetry::WriteLineToStream("");

#if !__FINAL
		for (int i = 0; i < rlTelemetryPolicies::MAX_NUM_LOG_CHANNELS; ++i)
		{
			formatf(buffer, "%s = %s", m_Channels[i].m_Id.TryGetCStr(), DEBUG_USAGE_STRING[(unsigned)m_Channels[i].m_Usage]);
			rlTelemetry::WriteLineToStream(buffer);
		}
#endif

		rlTelemetry::WriteLineToStream("------------------------------------------\n");
	}
}
#endif // RSG_OUTPUT

bool
rlTelemetryPolicies::operator==(const rlTelemetryPolicies& that) const
{
	for (int i = 0; i < MAX_NUM_LOG_CHANNELS; ++i)
	{
		if (!(m_Channels[i] == that.m_Channels[i]))
		{
			return false;
		}
	}

	return m_SubmissionIntervalSeconds == that.m_SubmissionIntervalSeconds
		&& m_SubmissionTimeoutSeconds == that.m_SubmissionTimeoutSeconds
		&& m_TelemetryVersion == that.m_TelemetryVersion
		&& m_IsTelemetryEnabled == that.m_IsTelemetryEnabled
		&& m_ConfigVersionHistory == that.m_ConfigVersionHistory
		&& m_MetricsConfig == that.m_MetricsConfig
		&& m_MetricConfigDefaults == that.m_MetricConfigDefaults
		&& stricmp(m_CloudFile, that.m_CloudFile) == 0;
}

void
rlTelemetryPolicies::Clear()
{
	ClearMetrics();
	m_MetricConfigDefaults.Reset();

	CompileTimeAssert(DEFAULT_SUBMISSION_INTERVAL_SECONDS >= MINIMUM_SUBMISSION_INTERVAL_SECONDS);
	CompileTimeAssert(DEFAULT_SUBMSSION_TIMEOUT_SECONDS >= MINIMUM_SUBMSSION_TIMEOUT_SECONDS);

	m_SubmissionIntervalSeconds = DEFAULT_SUBMISSION_INTERVAL_SECONDS;
	m_SubmissionTimeoutSeconds = DEFAULT_SUBMSSION_TIMEOUT_SECONDS;

	NOTFINAL_ONLY(m_AllDebugUsage = rlMetricDebugUsage::DEFAULT);
	NOTFINAL_ONLY(PARAM_netTelemetrySubmissionIntervalSec.Get(m_SubmissionIntervalSeconds));

	for (int i = 0; i < MAX_NUM_LOG_CHANNELS; ++i)
		m_Channels[i] = rlTelemetryLogChannel();

	m_ConfigVersionHistory = rlTelemetryConfigVersionHistory();
}

void
rlTelemetryPolicies::ClearMetrics()
{
	m_MetricsConfig.Reset();
	// Don't clear the m_MetricConfigsDefaults. Those are loaded once at start-up and then kept as they are.
	m_CloudFile[0] = 0;
	m_TelemetryVersion = 0;

	// m_IsTelemetryEnabled defaults to true but the individual metrics by default are off.
	// This way if we haven't got any telemetry config yet all metrics will be dropped except for those which have been explicitly set to default to on.
	m_IsTelemetryEnabled = true;
}

void
rlTelemetryPolicies::ApplyDefaultMetrics()
{
	rlAssertf(m_MetricsConfig.m_MetricConfigs.GetCount() == 0, "This should only be called on an empty list");
	rlAssert(m_MetricConfigDefaults.m_MetricConfigs.IsSorted());

	m_MetricsConfig.m_MetricConfigs = m_MetricConfigDefaults.m_MetricConfigs;
}

#if !RSG_FINAL
void
rlTelemetryPolicies::ApplyDebugMetrics()
{
	atBinaryMap<rlMetricConfig, atHashString>& cfgs = m_MetricsConfig.m_MetricConfigs;

	static const unsigned MAX_OVERRIDES = 32;
	const char* list[MAX_OVERRIDES] = { 0 };

	char buffer[MAX_OVERRIDES * 64] = { 0 };

	const int numOff = PARAM_netTelemetryForceOff.GetArray(list, MAX_OVERRIDES, buffer, static_cast<int>(sizeof(buffer)));
	ApplyDebugMetrics(list, numOff, false);

	const int numOn = PARAM_netTelemetryForceOn.GetArray(list, MAX_OVERRIDES, buffer, static_cast<int>(sizeof(buffer)));
	ApplyDebugMetrics(list, numOn, true);

	cfgs.FinishInsertion();

	const int numGroupOff = PARAM_netTelemetryGroupForceOff.GetArray(list, MAX_OVERRIDES, buffer, static_cast<int>(sizeof(buffer)));
	ApplyDebugMetricsChannel(list, numGroupOff, false);

	const int numGroupOn = PARAM_netTelemetryGroupForceOn.GetArray(list, MAX_OVERRIDES, buffer, static_cast<int>(sizeof(buffer)));
	ApplyDebugMetricsChannel(list, numGroupOn, true);

	// A bit hacky but will do. Keeps the old args working.
	{
		if (PARAM_telemetrychanneldevcaptureloglevel.Get())
		{
			m_Channels[9 /* TELEMETRY_CHANNEL_DEV_CAPTURE */].Set(rlMetricDebugUsage::FORCE_ON);
		}

		if (PARAM_metricallowlogleveldevoff.Get())
		{
			m_Channels[8 /* TELEMETRY_CHANNEL_DEV_DEBUG */].Set(rlMetricDebugUsage::FORCE_ON);
			m_Channels[9 /* TELEMETRY_CHANNEL_DEV_CAPTURE */].Set(rlMetricDebugUsage::FORCE_ON);
		}
	}

	if (numOn > 0)
	{
		// If we have any metric turned on by the debug option then we enable telemetry. Other metrics which are on will be on too therefor.
		m_IsTelemetryEnabled = true;
	}
}

void
rlTelemetryPolicies::ApplyDebugMetrics(const char** list, const int num, bool on)
{
	const atHashString ALL_METRICS = ATSTRINGHASH("ALL", 0x45835226);

	atBinaryMap<rlMetricConfig, atHashString>& cfgs = m_MetricsConfig.m_MetricConfigs;

	const rlMetricDebugUsage usage = on ? rlMetricDebugUsage::FORCE_ON : rlMetricDebugUsage::FORCE_OFF;

	for (int i = 0; i < num; ++i)
	{
		const atHashString key(list[i]);

		if (key == ALL_METRICS)
		{
			// We don't know what ALL means because we only know the metric names reported in the config file.
			m_AllDebugUsage = usage;
	
			atBinaryMap<rlMetricConfig, atHashString>::Iterator iter = cfgs.begin();
	
			for (; iter != cfgs.end(); ++iter)
			{
				// Little weird thing how QA uses the arg. They force off some metrics and then force on ALL
				// Technically it's wrong to do that but it should be fine to allow that.
				if (iter->GetDebugUsage() == rlMetricDebugUsage::DEFAULT)
				{
					iter->SetDebugUsage(usage);
				}
			}
		}
		else if (cfgs.Has(key))
		{
			int idx = cfgs.GetIndex(key);
			if (idx >= 0)
			{
				cfgs.GetItem(idx)->SetDebugUsage(usage);
			}
		}
		else
		{
			cfgs.Insert(key, rlMetricConfig(usage));
		}
	}
}

void rlTelemetryPolicies::ApplyDebugMetricsChannel(const char** list, const int num, bool on)
{
	const atHashString ALL_GROUPS = ATSTRINGHASH("ALL", 0x45835226);

	for (int i = 0; i < num; ++i)
	{
		const atHashString channelId(list[i]);

		for (int j = 0; j < MAX_NUM_LOG_CHANNELS; ++j)
		{
			if (m_Channels[j].m_Id == channelId)
			{
				m_Channels[j].Set(on ? rlMetricDebugUsage::FORCE_ON : rlMetricDebugUsage::FORCE_OFF);
				break;
			}

			if (channelId == ALL_GROUPS)
			{
				m_Channels[j].Set(on ? rlMetricDebugUsage::FORCE_ON : rlMetricDebugUsage::FORCE_OFF);
			}
		}
	}
}
#endif //!RSG_FINAL

//////////////////////////////////////////////////////////////////////////
//  rlTelemetryConfigHistory
//////////////////////////////////////////////////////////////////////////
void
rlTelemetryConfigVersionHistory::Insert(const int version)
{
	if (!rageVerify(version != 0))
	{
		return;
	}

	int oldestIdx = -1;
	u64 oldestTime = 0;

	for (unsigned i = 0; i < MAX_VERSIONS; ++i)
	{
		if (m_Versions[i].m_Version == version)
		{
			m_Versions[i].m_DownloadPosixTime = rlGetPosixTime();
			return;
		}

		if (oldestIdx == -1 || (oldestTime > m_Versions[i].m_DownloadPosixTime))
		{
			oldestIdx = i;
			oldestTime = m_Versions[i].m_DownloadPosixTime;
		}
	}

	if (oldestIdx >= 0)
	{
		m_Versions[oldestIdx].m_Version = version;
		m_Versions[oldestIdx].m_DownloadPosixTime = rlGetPosixTime();
	}
}

u64
rlTelemetryConfigVersionHistory::GetDownloadTime(const int version) const
{
	for (unsigned i = 0; i < MAX_VERSIONS; ++i)
	{
		if (m_Versions[i].m_Version == version)
		{
			return m_Versions[i].m_DownloadPosixTime;
		}
	}

	return 0;
}

bool
rlTelemetryConfigVersionHistory::IsRecent(const int version, const u64 timeIntervalSeconds) const
{
	const u64 downloadTime = GetDownloadTime(version);
	const u64 currentTime = rlGetPosixTime();

	return (downloadTime > currentTime || ((currentTime - downloadTime) < timeIntervalSeconds));
}

bool
rlTelemetryConfigVersionHistory::operator==(const rlTelemetryConfigVersionHistory& other) const
{
	for (unsigned i = 0; i < MAX_VERSIONS; ++i)
	{
		// No need to check all possible permutations
		if (m_Versions[i].m_DownloadPosixTime != other.m_Versions[i].m_DownloadPosixTime
			|| m_Versions[i].m_Version != other.m_Versions[i].m_Version)
		{
			return false;
		}
	}

	return true;
}

#if RL_USE_TELEMETRY_CLIENT_CONFIG
//////////////////////////////////////////////////////////////////////////
//  rlTelemetryClientConfig
//////////////////////////////////////////////////////////////////////////
rlTelemetryClientConfig::rlTelemetryClientConfig()
	: m_TelemetryConfigRetryCount(0)
	, m_ExpectedTelemetryVersion(0)
	, m_TelemetryDownloadTried(false)
	, m_TelemetryConfigState(TelemetryConfigState::cNone)
{

}

void
rlTelemetryClientConfig::Init()
{
    if (rlVerify(!IsInitialized()))
    {
        m_ConfigFileBuffer.Init(nullptr, datGrowBuffer::NULL_TERMINATE);
        m_TelemetryConfigState = TelemetryConfigState::cInit;
    }
}

void
rlTelemetryClientConfig::Shutdown()
{
    Cancel();
    ResetRetryTimer();

    m_ConfigFileBuffer.Clear();

    m_RequestedTelemetryVersions = rlTelemetryConfigVersionHistory();
    m_TelemetryConfigState = TelemetryConfigState::cNone;
    m_ExpectedTelemetryVersion = 0;
    m_TelemetryConfigRetryCount = 0;
    m_TelemetryDownloadTried = false;
}

bool
rlTelemetryClientConfig::IsInitialized() const
{
    return m_TelemetryConfigState != TelemetryConfigState::cNone;
}

void
rlTelemetryClientConfig::Cancel()
{
	if (m_DownloadTelemetryConfigStatus.Pending())
	{
		rlGetTaskManager()->CancelTask(&m_DownloadTelemetryConfigStatus);
	}
	m_DownloadTelemetryConfigStatus.Reset();

	if (m_DownloadFileStatus.Pending())
	{
		rlCloud::Cancel(&m_DownloadFileStatus);
	}
	m_DownloadFileStatus.Reset();
}

void
rlTelemetryClientConfig::TelemetryVersionChanged(const int newVersionNumber)
{
	// If we're in any other state we just ignore this event
	if (m_TelemetryConfigState != TelemetryConfigState::cReady)
	{
		rlDebug1("rlTelemetryClientConfig - TelemetryVersionChanged will be skipped for %u. We're not ready. state[%u]",
			newVersionNumber, static_cast<unsigned>(m_TelemetryConfigState));
		return;
	}

	// We check the config version history to reduce the chance of continuously swapping between different configs.
	if (rlTelemetry::GetPolicies().GetVersionHistory().IsRecent(newVersionNumber, rlTelemetryPolicies::MIN_REDOWNLOAD_SAME_CONFIG_INTERVAL_SECONDS))
	{
		rlWarning("rlTelemetryClientConfig - TelemetryVersionChanged will be skipped for %u. Less than %u seconds have passed since the previous download of that version",
			newVersionNumber, static_cast<unsigned>(rlTelemetryPolicies::MIN_REDOWNLOAD_SAME_CONFIG_INTERVAL_SECONDS));
		return;
	}

	// This additional check is to avoid that we try to download the same file over and over in case 
	// Version (from GetTelemetryClientConfig) and CurrentVersion (from SubmitMetric) mismatch. the requested version may not be the one we actually download.
	if (m_RequestedTelemetryVersions.IsRecent(newVersionNumber, rlTelemetryPolicies::MIN_REDOWNLOAD_SAME_CONFIG_INTERVAL_SECONDS))
	{
		rlWarning("rlTelemetryClientConfig - TelemetryVersionChanged will be skipped for %u. The version is part of the recent requests.", newVersionNumber);
		return;
	}

	rlDebug1("rlTelemetryClientConfig - TelemetryVersionChanged %d", newVersionNumber);

	m_ExpectedTelemetryVersion = newVersionNumber;
	m_RequestedTelemetryVersions.Insert(newVersionNumber);

	SetTelemetryClientConfigState(TelemetryConfigState::cOutdated);
}

void
rlTelemetryClientConfig::Update()
{
	m_TelemetryConfigRetryTime.Update();

	switch (m_TelemetryConfigState)
	{
	case TelemetryConfigState::cNone:
	case TelemetryConfigState::cFailedNoRetry:
	case TelemetryConfigState::cReady:
		{
			return;
		}
	case TelemetryConfigState::cInit:
	case TelemetryConfigState::cOutdated:
		{
			DownloadTelemetryClientConfig();
		} break;
	case TelemetryConfigState::cDownloading:
		{
			UpdateTelemetryClientConfigDownload();
		} break;
	case TelemetryConfigState::cDownloadingFile:
		{
			UpdateCloudFileDownload();
		} break;
	case TelemetryConfigState::cFailed:
	{
		if (!m_TelemetryConfigRetryTime.IsStopped() && m_TelemetryConfigRetryTime.IsTimeToRetry())
		{
			++m_TelemetryConfigRetryCount;
			DownloadTelemetryClientConfig();
		}
	} break;
	}
}

void
rlTelemetryClientConfig::Refresh()
{
	if (IsInitialized() && m_TelemetryConfigState != TelemetryConfigState::cDownloading && m_TelemetryConfigState != TelemetryConfigState::cReady)
	{
		rlDebug1("rlTelemetryClientConfig :: Refresh - Requesting telemetry client config");

		SetTelemetryClientConfigState(TelemetryConfigState::cInit);
		DownloadTelemetryClientConfig();
	}
}

void
rlTelemetryClientConfig::UpdateTelemetryClientConfigDownload()
{
	if (m_DownloadTelemetryConfigStatus.Pending())
	{
		return;
	}

	rtry
	{
		rcheck(m_DownloadTelemetryConfigStatus.Succeeded(), catchall, rlError("m_DownloadTelemetryConfigStatus [%s] with [%d]",
		m_DownloadTelemetryConfigStatus.c_str(), m_DownloadTelemetryConfigStatus.GetResultCode()));

#if RL_USE_TELEMETRY_CLIENT_CONFIG_FILE
		const char* path = rlTelemetry::GetPolicies().GetCloudFilePath();

		// If there's a path we need to download the file from the cloud
		if (path && path[0] != 0)
		{
			rlRosSecurityFlags flags = static_cast<rlRosSecurityFlags>(RLROS_SECURITY_DEFAULT | RLROS_SECURITY_REQUIRE_CONTENT_SIG);
			u64 modifiedTime = 0;

			rcheck(rlCloud::GetTitleFile(path, flags, modifiedTime, NET_HTTP_DUMP, m_ConfigFileBuffer.GetFiDevice(), m_ConfigFileBuffer.GetFiHandle(), nullptr, nullptr, &m_DownloadFileStatus),
				catchall, rlError("GetTitleFile failed for %s", path));

			SetTelemetryClientConfigState(TelemetryConfigState::cDownloadingFile);
		}
		else
#endif
		{
			SetTelemetryClientConfigState(TelemetryConfigState::cReady);
		}
	}
	rcatchall
	{
		rlWarning("TelemetryClientConfig download failed [%d]", m_DownloadTelemetryConfigStatus.GetResultCode());

		if (IsSevereClientConfigError(m_DownloadTelemetryConfigStatus.GetResultCode()))
		{
			SetTelemetryClientConfigState(TelemetryConfigState::cFailedNoRetry);
		}
		else
		{
			SetTelemetryClientConfigState(TelemetryConfigState::cFailed);

			CheckForRetry();
		}
	}
}

void
rlTelemetryClientConfig::UpdateCloudFileDownload()
{
	if (m_DownloadFileStatus.Pending())
	{
		// We're downloading from the cloud
		return;
	}

	if (!m_DownloadFileStatus.Succeeded())
	{
		rlDebug1("rlTelemetryClientConfig - Telemetry download from cloud failed (%d)", m_DownloadFileStatus.GetResultCode());
		SetTelemetryClientConfigState(IsSevereClientConfigError(m_DownloadFileStatus.GetResultCode()) ? TelemetryConfigState::cFailedNoRetry : TelemetryConfigState::cFailed);

		CheckForRetry();
	}
	else if (!rlTelemetry::ApplyTelemetryClientConfig((const char*)m_ConfigFileBuffer.GetBuffer(), m_ConfigFileBuffer.Length()))
	{
		rlAssertf(false, "rlTelemetryClientConfig - Telemetry download from cloud succeeded but the data is invalid");
		SetTelemetryClientConfigState(TelemetryConfigState::cFailedNoRetry);
	}
	else
	{
		rlDebug1("rlTelemetryClientConfig - Telemetry successfully downloaded from cloud");
		SetTelemetryClientConfigState(TelemetryConfigState::cReady);
	}

	m_ConfigFileBuffer.Clear();
}

bool
rlTelemetryClientConfig::IsSevereClientConfigError(const int resultCode)
{
	return resultCode == rlTelemetryPolicies::CONFIG_PARSING_ERROR || (resultCode >= 400 && resultCode < 500);
}

void
rlTelemetryClientConfig::DownloadTelemetryClientConfig()
{
	const int localGamerIndex = rlPresence::GetActingUserIndex();
	bool bValidCreds = RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && rlRos::GetCredentials(localGamerIndex).IsValid();

	if (!bValidCreds || !rlPresence::IsOnline(localGamerIndex))
	{
		return;
	}

	rtry
	{
		Cancel(); // Cancel any pending net status

		rverify(m_TelemetryConfigState == TelemetryConfigState::cInit || m_TelemetryConfigState == TelemetryConfigState::cOutdated
		|| m_TelemetryConfigState == TelemetryConfigState::cFailed, catchall, rlError("DownloadTelemetryClientConfig - invalid state [%d]", static_cast<int>(m_TelemetryConfigState)));

		rcheck(rlTelemetry::RetrieveTelemetryClientConfig(localGamerIndex, &m_DownloadTelemetryConfigStatus), catchall, rlError("RetrieveTelemetryClientConfig failed"));

		SetTelemetryClientConfigState(TelemetryConfigState::cDownloading);
	}
	rcatchall
	{
		SetTelemetryClientConfigState(TelemetryConfigState::cFailedNoRetry);
	}
}

void
rlTelemetryClientConfig::SetTelemetryClientConfigState(const TelemetryConfigState value)
{
	m_TelemetryConfigState = value;

	if (value == TelemetryConfigState::cReady)
	{
		ResetRetryTimer();
		m_TelemetryConfigRetryCount = 0;

#if RSG_OUTPUT
		if (m_ExpectedTelemetryVersion != 0 && m_ExpectedTelemetryVersion != rlTelemetry::GetPolicies().GetTelemetryVersion())
		{
			rlWarning("The expected version[%d] and version[%d] don't match. This can happen if the file just changed but realistically you shouldn't hit this.",
				m_ExpectedTelemetryVersion, rlTelemetry::GetPolicies().GetTelemetryVersion());
		}

		if (Channel_rline_telemetry.TtyLevel >= DIAG_SEVERITY_DISPLAY || Channel_rline_telemetry.FileLevel >= DIAG_SEVERITY_DISPLAY)
		{
			rlTelemetry::DumpSettings();
		}

#endif //RSG_OUTPUT
	}

	if (value != TelemetryConfigState::cDownloading && value != TelemetryConfigState::cDownloadingFile && value != TelemetryConfigState::cInit)
	{
		m_TelemetryDownloadTried = true; // Once we're beyond downloading it stays true
		rlTelemetry::QueueMetrics(false);
	
	}

	if (value == TelemetryConfigState::cReady || value == TelemetryConfigState::cFailed || value == TelemetryConfigState::cFailedNoRetry)
	{
		rlTelemetry::sm_Delegator.Dispatch(rlTelemetryEventConfigChanged(rlTelemetry::GetPolicies().GetTelemetryVersion()));
	}
}

void
rlTelemetryClientConfig::CheckForRetry()
{
	if (m_TelemetryConfigState == TelemetryConfigState::cFailed && m_TelemetryConfigRetryCount < TELEMETRY_CONFIG_MAX_RETRY)
	{
		if (m_TelemetryConfigRetryTime.IsStopped())
		{
			m_TelemetryConfigRetryTime.Start();
		}
		else
		{
			m_TelemetryConfigRetryTime.ScaleRange(TELEMETRY_CONFIG_MAX_RETRY_SCALE_RANGE);
			m_TelemetryConfigRetryTime.Restart();
		}
	}
	else
	{
		// It's a 'fatal' error or we're done retrying so we reset the timer.
		ResetRetryTimer();
	}
}

void
rlTelemetryClientConfig::ResetRetryTimer()
{
	m_TelemetryConfigRetryTime.Shutdown();
	m_TelemetryConfigRetryTime.ResetIntervalMilliseconds(TELEMETRY_CONFIG_MIN_RETRY_TIME_SEC, TELEMETRY_CONFIG_MAX_RETRY_TIME_SEC);
	m_TelemetryConfigRetryTime.Stop();
}
#endif //RL_USE_TELEMETRY_CLIENT_CONFIG

//////////////////////////////////////////////////////////////////////////
//  rlTelemetryStream
//////////////////////////////////////////////////////////////////////////
rlTelemetryStream::rlTelemetryStream()
: Stream(NULL)
{
}

rlTelemetryStream::~rlTelemetryStream()
{
	if(Stream)
	{
	    rlTelemetry::RemoveStream(this);
	}
}
//////////////////////////////////////////////////////////////////////////
//  rlMetric
//////////////////////////////////////////////////////////////////////////
rlMetric::rlMetric()
    : m_PosixTime(rlGetPosixTime())
    , m_Next(NULL)
{
}

rlMetric::~rlMetric()
{
    rlAssert(!m_Next);
}

int
rlMetric::GetMetricLogChannel() const
{
    return 0;
}

int
rlMetric::GetMetricLogLevel() const
{
    return 0;
}

rlMetricSamplingMode
rlMetric::GetSamplingMode() const
{
	return METRIC_SAMPLE_EACH;
}

const char*
rlMetric::GetMetricName() const
{
    rlAssert(false);
    return "UNKNOWN";
}

bool
rlMetric::Write(RsonWriter* /*rw*/) const
{
    return true;
}

u64
rlMetric::GetMetricTime() const
{
    return m_PosixTime;
}

//private:

size_t
rlMetric::GetMetricSize() const
{
    return sizeof(*this);
}

u32 
rlMetric::GetMetricId() const
{
	rlAssert(false);
	return 0;
}

u32
rlMetric::MetricId()
{
	rlAssert(false);
	return 0;
}

bool 
rlMetric::IsDisabled() const
{
	const bool disabled = IsDisabledByDefault();
	rlMetricConfig metricConfig;
	const rlMetricConfig* cfg = rlTelemetry::GetMetricConfig(GetMetricId(), metricConfig) ? &metricConfig : nullptr;
	return IsDisabled(disabled, GetMetricLogChannel(), cfg);
}

bool
rlMetric::IsDisabled(const bool disabledByDefault, const int NOTFINAL_ONLY(logChannel), const rlMetricConfig* metricConfig)
{
	bool disabled = disabledByDefault;

	if (metricConfig)
	{
		disabled = metricConfig->IsDisabled();
		NOTFINAL_ONLY(GetDebugDisabledFlag(metricConfig, logChannel, disabled));
	}
	else
	{
		NOTFINAL_ONLY(GetDebugDisabledFlag(nullptr, logChannel, disabled));
	}

	return disabled;
}

bool
rlMetric::ShouldIncludeInSample() const
{
	rlMetricConfig metricConfig;
	const rlMetricConfig* cfg = rlTelemetry::GetMetricConfig(GetMetricId(), metricConfig) ? &metricConfig : nullptr;
	return ShouldIncludeInSample(GetSamplingMode(), GetMetricLogChannel(), cfg);
}

bool
rlMetric::ShouldIncludeInSample(const rlMetricSamplingMode samplingMode, const int NOTFINAL_ONLY(logChannel), const rlMetricConfig* metricConfig)
{
	// If no config is set up for this metric, we'll default to true (always included).
	if (metricConfig == nullptr)
	{
		return true;
	}

	const float samplingValue = (samplingMode == METRIC_SAMPLE_ONCE) ? metricConfig->GetSampledValue() : s_TelemetryRng.GetFloat();
	const float probability = NOTFINAL_ONLY(s_ForceProbability >= 0.0f ? s_ForceProbability :) metricConfig->GetProbability();

	// The range of valid values for samplingValue is between zero and the rlMetricConfig::GetProbability()
	// If this samplingValue is out of range (>= probability), the metric is culled and false is returned.
	// A probability of zero is always culled no matter what sampling value is passed in (including zero).
	if (samplingValue >= probability)
	{
#if !RSG_FINAL
		bool disabled = false;

		if (PARAM_netTelemetryForceOnKeepsProbability.Get() == false && GetDebugDisabledFlag(metricConfig, logChannel, disabled) && !disabled)
		{
			rlDebug3("Metric culling skipped due to the forceOn debug arg. Sampling value %.4f. Range (0.0 - %.4f).", samplingValue, probability);
			return true;
		}
#endif //!RSG_FINAL

		rlDebug3("Metric will be culled, sampling value %.4f is outside of valid range (0.0 - %.4f).", samplingValue, probability);
		return false;
	}

	return true;
}

u32
rlMetric::GetSendIntervalMs() const
{
	u32 intervalMs = 0;

    rlMetricConfig metricConfig;

	// If no config is set up for this metric, we'll default to 0 (no interval set).
	if (rlTelemetry::GetMetricConfig(GetMetricId(), metricConfig))
	{
		intervalMs = metricConfig.GetSendIntervalMs();
	}

	return intervalMs;
}

const rlMetricConfig*
rlMetric::GetMetricConfig() const
{
	return rlTelemetry::GetPolicies().GetMetricConfig(GetMetricId());
}

bool
rlMetric::IsDisabledByDefault() const
{
	return true;
}

#if !RSG_FINAL
bool
rlMetric::GetDebugDisabledFlag(const rlMetricConfig* config, int logChannel, bool& result)
{
	const rlMetricDebugUsage metricUsage = config ? config->GetDebugUsage() : rlTelemetry::GetPolicies().m_AllDebugUsage;
	const rlMetricDebugUsage usage = metricUsage == rlMetricDebugUsage::DEFAULT ? rlTelemetry::GetPolicies().m_Channels[logChannel].m_Usage : metricUsage;

	if (usage == rlMetricDebugUsage::FORCE_ON || usage == rlMetricDebugUsage::FORCE_OFF)
	{
		result = (metricUsage == rlMetricDebugUsage::FORCE_OFF);
		return true;
	}

	return false;
}
#endif

//////////////////////////////////////////////////////////////////////////
//  rlMetricList
//////////////////////////////////////////////////////////////////////////
rlMetricList::rlMetricList()
    : m_Head(NULL)
    , m_Tail(NULL)
    , m_Count(0)
{
}

rlMetricList::~rlMetricList()
{
    Clear();
}

void
rlMetricList::Clear()
{
    while(Pop())
    {
    }
}

void
rlMetricList::Append(rlMetric* metric)
{
    rageAssert(!metric->m_Next);

    if(!m_Head)
    {
        m_Head = m_Tail = metric;
    }
    else
    {
        m_Tail->m_Next = metric;
        m_Tail = metric;
    }

    ++m_Count;
}

rlMetric*
rlMetricList::Pop()
{
    rlMetric* m = m_Head;
    if(m)
    {
        m_Head = m->m_Next;
        if(!m_Head)
        {
            m_Tail = NULL;
        }

        m->m_Next = NULL;
        --m_Count;
    }

    return m;
}

const rlMetric*
rlMetricList::GetFirst() const
{
    return m_Head;
}

unsigned
rlMetricList::GetCount() const
{
    return m_Count;
}

//////////////////////////////////////////////////////////////////////////
//  rlTelemetry
//////////////////////////////////////////////////////////////////////////
//Buffer where metrics are stored in binary form prior to submission.
class rlTelemetryStreamMetricsBuffer
{
public:

    rlTelemetryStreamMetricsBuffer()
    {
        Reset();
    }

	void Reset()
	{
		m_Avail = sizeof(m_MemoryBuf);
	}

	bool IsEmpty() const { return (m_Avail == sizeof(m_MemoryBuf)); }
	unsigned GetAvailableSize() const { return m_Avail; }
	rlMetric* GetNewMetric(unsigned size) 
	{
		if (size > m_Avail)
		{
			return NULL;
		}

		const unsigned used = sizeof(m_MemoryBuf) - m_Avail;
		rlMetric* newRec = (rlMetric*) &m_MemoryBuf[used];
		m_Avail -= (unsigned) size;
		return newRec;
	}

	u8 m_MemoryBuf[RL_METRICS_BUFFER_SIZE];
	unsigned m_Avail;
};

static rlTelemetryStreamMetricsBuffer s_MetricsMemoryBuf;
static rlTelemetryStreamMetricsBuffer s_ImmediateMetricsMemoryBuf;
static rlTelemetryStreamMetricsBuffer s_DeferredMetricsMemoryBuf;

static rlTelemetryPolicies s_Policies;

static netTimeout s_FlushTimer;

static netTimeout s_ImmediateFlushTimer;

static netTimeout s_DeferredFlushTimer;
static bool       s_DeferredFlushBlocked = true;

rlTelemetry::StreamList rlTelemetry::sm_Streams;
rlTelemetry::GamerSlot rlTelemetry::sm_GamerSlots[RL_MAX_LOCAL_GAMERS];
rlTelemetry::GamerSlot rlTelemetry::sm_ImmediateGamerSlots[RL_MAX_LOCAL_GAMERS];
rlTelemetry::GamerSlot rlTelemetry::sm_DeferredGamerSlots[RL_MAX_LOCAL_GAMERS];
rlQueuedMetricList rlTelemetry::sm_QueuedMetrics;
sysCriticalSectionToken rlTelemetry::sm_QueuedMetricsCs;
rlTelemetry::Delegator rlTelemetry::sm_Delegator;
bool rlTelemetry::sm_QueueMetrics = false;
#if RL_USE_TELEMETRY_CLIENT_CONFIG
rlTelemetryClientConfig rlTelemetry::sm_TelemetryClientConfig;
#endif

#if !__FINAL
rlTelemetryBandwidth rlTelemetry::sm_TelemetryBandwidth;

//Count number of metrics added until Flush.
static u32 s_DeferredMetricCount = 0;
#endif

//Allow two submissions to be in flight for each gamer.
//This is to handle situations where telemetry has been flushed,
//then another flush must quickly occur due to a change in an
//important variable like the session ID.
static const int MAX_SUBMISSIONS_PER_GAMER  = 2;

netStatus s_SubmissionStatus[RL_MAX_LOCAL_GAMERS][MAX_SUBMISSIONS_PER_GAMER];

void 
rlTelemetry::Init(const bool useClientConfig)
{
	s_TelemetryRng.SetRandomSeed();
	s_Policies.ResetMetricsToDefaults();

#if RL_USE_TELEMETRY_CLIENT_CONFIG
    if (useClientConfig)
    {
        sm_TelemetryClientConfig.Init();
    }
#endif

#if !RSG_FINAL
    float probability = -1.0f;
    if (PARAM_netTelemetryForceProbability.Get(probability) && probability >= 0.0f)
    {
        s_ForceProbability = probability;
        rlDisplay("Setting ALL telemetry send probability to %.4f.", probability);
    }

    unsigned subInterval = 0;
    if (PARAM_netTelemetrySubmissionIntervalSec.Get(subInterval))
    {
        s_Policies.m_SubmissionIntervalSeconds = subInterval;
        rlDisplay("Submission interval overwritten to %u seconds", s_Policies.m_SubmissionIntervalSeconds);
    }
#endif //!RSG_FINAL
}

void 
rlTelemetry::Shutdown()
{
	ClearQueuedTelemetryMetrics();
	sm_Delegator.Clear();
	s_Policies.Shutdown();
#if RL_USE_TELEMETRY_CLIENT_CONFIG
	sm_TelemetryClientConfig.Shutdown();
#endif
	sm_QueueMetrics = false;
}

bool
rlTelemetry::AddStream(rlTelemetryStream* stream)
{
    if(rlVerify(stream->Stream))
    {
        sm_Streams.push_back(stream);
        return true;
    }

    return false;
}

bool
rlTelemetry::RemoveStream(rlTelemetryStream* stream)
{
    sm_Streams.erase(stream);

    return true;
}


void
rlTelemetry::AddDelegate(Delegate* dlgt)
{
	sm_Delegator.AddDelegate(dlgt);
}

void
rlTelemetry::RemoveDelegate(Delegate* dlgt)
{
	sm_Delegator.RemoveDelegate(dlgt);
}

void
rlTelemetry::SetSubmissionInterval(const unsigned intervalSec)
{
    SYS_CS_SYNC(sm_QueuedMetricsCs);

    rlDebug1("rlTelemetry :: SetSubmissionInterval - %u sec", intervalSec);
    s_Policies.m_SubmissionIntervalSeconds = intervalSec;

    if (!IsTelemetryEnabled())
    {
        ClearSlots();
    }
}

void
rlTelemetry::SetPolicies(const rlTelemetryPolicies& policies)
{
#if RSG_OUTPUT
	if(policies != s_Policies && policies.GetTelemetryVersion() != 0 &&
		(Channel_rline_telemetry.TtyLevel >= DIAG_SEVERITY_DISPLAY || Channel_rline_telemetry.FileLevel >= DIAG_SEVERITY_DISPLAY))
	{
		policies.DumpSettings();
	}
#endif

    s_Policies = policies;

    if(0 == policies.m_SubmissionIntervalSeconds || !policies.IsTelemetryEnabled())
    {
        //Clear the queues.
        ClearSlots();
    }
}

const rlTelemetryPolicies&
rlTelemetry::GetPolicies()
{
    return s_Policies;
}

bool 
rlTelemetry::ShouldSendTelemetry(const float probability)
{
	return (probability > 0.0f)
			&& rlVerify(probability <= 1.0f)
			&& (s_TelemetryRng.GetFloat() < probability);
}

#if RL_USE_TELEMETRY_CLIENT_CONFIG
bool
rlTelemetry::RetrieveTelemetryClientConfig(const int localGamerIndex, netStatus* status)
{
    bool success = false;
    rlRosGetTelemetryClientConfigTask* task = nullptr;

    rlDebug2("RetrieveTelemetryClientConfig - Local gamer [%d]", localGamerIndex);

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index: %d", localGamerIndex));
        rverify(status, catchall, );

#if !__FINAL
        const char* path = nullptr;
        if (PARAM_netTelemetryConfig.Get(path))
        {
            rcheck(s_Policies.LoadFromJsonFile(path), catchall, rlError("Failed to load from: %s", path));
            status->ForceSucceeded();
            return true;
        }
#endif //!__FINAL

        rverify(rlGetTaskManager()->CreateTask(&task), catchall, rlError("Error creating task"));

        rverify(rlTaskBase::Configure(task,
            localGamerIndex,
            &s_Policies,
            status),
            catchall,
            rlError("Error configuring task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

        success = true;
    }
    rcatchall
    {
        if (task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }

        s_Policies.ResetMetricsToDefaults();
    }

    return success;
}

bool
rlTelemetry::ApplyTelemetryClientConfig(const char* json, const unsigned jsonStringLength)
{
    return s_Policies.LoadMetricsFromJson(json, jsonStringLength);
}
#endif

bool
rlTelemetry::GetMetricConfig(atHashString hMetric, rlMetricConfig& metricConfig)
{
    // SYS_CS_SYNC(sm_TelemetryCs); is currently not needed because
    // We sync main and ragenet thread when applying the telemetry config
    // Should that no longer work please re-enable it. The rest of the code
    // should be fine to go but it might have changed since this was written.

    const rlMetricConfig* cfg = s_Policies.GetMetricConfig(hMetric);

    if (cfg)
    {
        metricConfig = *cfg;
        return true;
    }

    return false;
}

bool
rlTelemetry::ShouldSendMetric(const rlMetric& metric OUTPUT_ONLY(, const char* context))
{
	rtry
	{
		// Check if the metric is disabled, or should be removed due to sampling
		rcheck(GetPolicies().IsTelemetryEnabled(),
			catchall,
			rlDebug3("[%s] Discarding metric %s. Telemetry is disabled.", context, metric.GetMetricName()));

		rcheck(!metric.IsDisabled(),
			catchall,
			rlDebug3("[%s] Discarding metric %s. Metric is disabled.", context, metric.GetMetricName()));

		// It it's manually sampled. The calling code has to do the sampling
		rcheck(metric.GetSamplingMode() == METRIC_SAMPLE_MANUAL || metric.ShouldIncludeInSample(),
			catchall,
			rlDebug3("[%s] Discarding metric %s. Metric is Shouldnt be Included In Sample.", context, metric.GetMetricName()));

		return true;
	}
	rcatchall
	{
	}

	return false;
}

bool
rlTelemetry::ShouldSendMetric(const atHashString metricId, const int logChannel, const rlMetricSamplingMode samplingMode OUTPUT_ONLY(, const char* context))
{
	rlAssertf(samplingMode != METRIC_SAMPLE_EACH, "Do not call this version of ShouldSendMetric");

	rtry
	{
		rlMetricConfig metricConfig;
		const rlMetricConfig* cfg = rlTelemetry::GetMetricConfig(metricId, metricConfig) ? &metricConfig : nullptr;

		// Check if the metric is disabled, or should be removed due to sampling
		rcheck(GetPolicies().IsTelemetryEnabled(),
			catchall,
			rlDebug3("[%s] ShouldSendMetric - No for metric %s. Telemetry is disabled.", context, metricId.TryGetCStr()));

		rcheck(!rlMetric::IsDisabled(false, logChannel, cfg),
			catchall,
			rlDebug3("[%s] ShouldSendMetric - No for metric %s. Metric is disabled.", context, metricId.TryGetCStr()));

		rcheck(rlMetric::ShouldIncludeInSample(samplingMode, logChannel, cfg),
			catchall,
			rlDebug3("[%s] ShouldSendMetric - No for metric %s. Metric is Shouldnt be Included In Sample.", context, metricId.TryGetCStr()));

		return true;
	}
	rcatchall
	{
	}

	return false;
}

bool
rlTelemetry::Write(const int localGamerIndex, const rlMetric& metric)
{
	if (ShouldSendMetric(metric OUTPUT_ONLY(, "Write")))
	{
		return DoWrite(localGamerIndex, metric);
	}

	return true;
}

bool
rlTelemetry::WriteImmediate(const int localGamerIndex, const rlMetric& metric, const bool deferredTransaction)
{
	// rlTelemetry::Write() isn't thread safe. If we're not on the main thread,
	// we'll queue up the metric to be sent on the main thread.
	if(!sysThreadType::IsUpdateThread())
	{
		SYS_CS_SYNC(sm_QueuedMetricsCs);

		if(sm_QueuedMetrics.size() < rlQueuedMetricList::MAX_QUEUED_TELEMETRY_METRICS)
		{
			const unsigned metricSize = (unsigned)metric.GetMetricSize();

			rlDebug("Queuing metric '%s' (%u bytes) to be sent on the main thread.", metric.GetMetricName(), metricSize);

			const unsigned size = sizeof(rlQueuedMetric) + metricSize;
			rlQueuedMetric* queuedMetric = (rlQueuedMetric*)RL_ALLOCATE(rlQueuedMetric, size);
			if(queuedMetric)
			{
				// copy the rlMetric into the buffer immediately following the rlQueuedMetric container
				rlMetric* metricCpy = (rlMetric*)&queuedMetric[1];
				sysMemCpy((void*)metricCpy, (void*)&metric, metricSize);
				new(queuedMetric) rlQueuedMetric(localGamerIndex, metricCpy);
				sm_QueuedMetrics.push_back(queuedMetric);
			}
			else
			{
				rlDebug("Discarding metric %s. Failed to allocate %u bytes.", metric.GetMetricName(), size);
				return false;
			}
		}
		else
		{
			rlDebug("Discarding metric %s. Queue is at capacity.", metric.GetMetricName());
			return false;
		}
		return true;
	}

	if (ShouldSendMetric(metric OUTPUT_ONLY(, "WriteImmediate")))
	{
		if (deferredTransaction)
		{
			NOTFINAL_ONLY( s_DeferredMetricCount += 1; )
			//Record the metric.
			return DoWriteDeferred(localGamerIndex, metric);
		}
		else
		{
			//Record the metric.
			return DoWriteImmediate(localGamerIndex, metric);
		}
	}

	return true;
}

void
rlTelemetry::UpdateClass()
{
	WriteQueuedTelemetryMetrics();

	for(int i=0; i<COUNTOF(sm_GamerSlots); ++i)
	{
		for(int j = 0; j < MAX_SUBMISSIONS_PER_GAMER; ++j)
		{
			//Not pending and not none
			if (!s_SubmissionStatus[i][j].Pending() && !s_SubmissionStatus[i][j].None())
			{
				sm_Delegator.Dispatch(rlTelemetryEventFlushEnd(&s_SubmissionStatus[i][j]));
				s_SubmissionStatus[i][j].Reset();
			}
		}
	}

    s_FlushTimer.Update();

    if(s_FlushTimer.IsTimedOut())
    {
        //Disable the timer.
        s_FlushTimer.Clear();

        Flush();
    }

	s_ImmediateFlushTimer.Update();
	if(s_ImmediateFlushTimer.IsTimedOut())
	{
		//Disable the timer.
		s_ImmediateFlushTimer.Clear();

		FlushImmediate();
	}

	s_DeferredFlushTimer.Update();
	if(s_DeferredFlushTimer.IsTimedOut() && !s_DeferredFlushBlocked)
	{
		//Disable the timer.
		s_DeferredFlushTimer.Clear();

		FlushDeferred();
	}

#if RL_USE_TELEMETRY_CLIENT_CONFIG
	sm_TelemetryClientConfig.Update();
#endif
}

void
rlTelemetry::SetDeferredFlushBlocked(const bool value)
{
	//There is no point in allowing an Immediate Flush if there are no cash transactions pending to be flushed.
	if (s_DeferredFlushBlocked && !value && s_DeferredMetricsMemoryBuf.IsEmpty())
		return;

	s_DeferredFlushBlocked = value;
}

bool
rlTelemetry::GetDeferredFlushBlocked()
{
	return s_DeferredFlushBlocked;
}

bool rlTelemetry::SubmitMetricList(const int localGamerIndex, const rlMetricList* pMetricList, const rlTelemetrySubmissionMemoryPolicy* pMemoryPolicy )
{
	bool success = false;
	rlFireAndForgetTask<rlRosTelemetrySubmissionTask>* task = NULL;
	rtry
	{
		rverify(rlGetTaskManager()->CreateTask(&task),catchall,rlError("Failed to created rlRosTelemetrySubmissionTask"));
		rverify(task,catchall,);
		rverify(rlTaskBase::Configure(task,
			localGamerIndex,
			pMetricList,
			pMemoryPolicy, 
			NOTFINAL_ONLY(rlTelemetryBandwidth::DEBUG_METRICS,)
			&task->m_Status), catchall,rlError("Failed to configure rlRosTelemetrySubmissionTask"));

		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);	
		}
	}

	return success;
}

void
rlTelemetry::QueueMetrics(const bool queue)
{
	rlDisplay("Queueing Telemetry Metrics: %s", queue ? "true" : "false");
	
	sm_QueueMetrics = queue;
}

bool
rlTelemetry::HasAnyMetrics()
{
	bool result = false;

	for(int i = 0; i < COUNTOF(sm_GamerSlots) && !result; ++i)
	{
		GamerSlot* slot = &sm_GamerSlots[i];
		if(slot->m_Metrics.GetFirst())
		{
			result = true;
			//No metrics in this slot
			continue;
		}
	}

	return result;
}

void
rlTelemetry::TryForceFlushImmediate()
{
	s_ImmediateFlushTimer.Clear();
	OUTPUT_ONLY(const bool result = )FlushImmediate();
	rlDebug1("TryForceFlushImmediate() - result='%s'.", result?"true":"false");
}

bool
rlTelemetry::Flush()
{
    //NOTE - make sure to NOT RETURN from this function
    //until ClearSlots() has been called.

    //Disable the timer.
    s_FlushTimer.Clear();

    //A value of zero disables submissions.
    if(s_Policies.m_SubmissionIntervalSeconds > 0)
    {
        for(int i = 0; i < COUNTOF(sm_GamerSlots); ++i)
        {
            GamerSlot* slot = &sm_GamerSlots[i];

            if(!slot->m_Metrics.GetFirst())
            {
                //No metrics in this slot
                continue;
            }

            if(!slot->m_Cred.IsValid()
                || slot->m_GamerHandle.GetLocalIndex() < 0)
            {
                //Credentials are invalid for this slot
                //or the gamer has signed out and we don't know about it yet.
                continue;
			}

			const rlRosCredentials& creds = rlRos::GetCredentials(slot->m_GamerHandle.GetLocalIndex());
			if(!creds.IsValid())
			{
				//Credentials are not valid anymore - lets clear them for now.
				slot->m_Cred.Clear();
				continue;
			}

            rlRosTelemetrySubmissionTask* task = NULL;
            rtry
            {
                rlDebug3("Flushing compressed telemetry for gamer: %d...", i);

                netStatus* status = NULL;
                for(int j = 0; j < MAX_SUBMISSIONS_PER_GAMER && NULL == status; ++j)
                {
                    if(s_SubmissionStatus[i][j].None())
                    {
                        status = &s_SubmissionStatus[i][j];
                    }
                    else
                    {
                        rlDebug1("Already a submission in flight - using backup netStatus object, status='%d'.", s_SubmissionStatus[i][j].GetStatus());
                    }
                }

                rverify(status && !status->Pending(),
                        catchall,
                        rlError("There are already too many submissions pending for gamer: %d", i));

                rverify(rlGetTaskManager()->CreateTask(&task),
                        catchall,
                        rlError("Error creating submission task"));

                rcheck(rlTaskBase::Configure(task, slot, status),
                        catchall,
                        rlError("Error configuring submission task"));

                const size_t queueId = (size_t)slot;
                rverify(rlGetTaskManager()->AppendSerialTask(queueId, task),
                        catchall,
                        rlError("Error queuing task"));
            }
            rcatchall
            {
                if(task)
                {
                    rlGetTaskManager()->DestroyTask(task);
                }
            }
        }
    }

    ClearSlots();

	return true;
}

void
rlTelemetry::CancelFlushes()
{
    for(int i = 0; i < COUNTOF(sm_GamerSlots); ++i)
    {
        const size_t queueId = (size_t) &sm_GamerSlots[i];
        rlGetTaskManager()->CancelAll(queueId);
    }

	for(int i = 0; i < COUNTOF(sm_ImmediateGamerSlots); ++i)
	{
		const size_t queueId = (size_t) &sm_ImmediateGamerSlots[i];
		rlGetTaskManager()->CancelAll(queueId);
	}

	for(int i = 0; i < COUNTOF(sm_DeferredGamerSlots); ++i)
	{
		const size_t queueId = (size_t) &sm_DeferredGamerSlots[i];
		rlGetTaskManager()->CancelAll(queueId);
	}
}

bool 
rlTelemetry::FlushInProgress()
{
	for(int i=0; i<COUNTOF(sm_GamerSlots); ++i)
	{
        const size_t queueId = (size_t) &sm_GamerSlots[i];
        if(rlGetTaskManager()->HasPendingTask(queueId))
        {
		    //Already a submission in flight
			return true;
		}
    }

	return false;
}

bool 
rlTelemetry::AreAllPending()
{
	bool result = false;
	for(int i=0; i<COUNTOF(sm_GamerSlots) && !result; ++i)
	{
        result = true;
        for(int j = 0; j < MAX_SUBMISSIONS_PER_GAMER && result; ++j)
        {
    		result = result && s_SubmissionStatus[i][j].Pending();
		}
    }

	return result;
}

unsigned rlTelemetry::GetAvailableSize()
{
	return s_MetricsMemoryBuf.GetAvailableSize();
}

unsigned rlTelemetry::GetSubmissionIntervalSeconds()
{
	return s_Policies.m_SubmissionIntervalSeconds;
}

bool rlTelemetry::IsTelemetryEnabled()
{
	return s_Policies.IsTelemetryEnabled() && s_Policies.m_SubmissionIntervalSeconds > 0;
}

#if __DEV

//For testing purposes

#define MAKE_METRIC(name, memname)\
class name##Metric : public rlMetric{\
    RL_DECLARE_METRIC(name);\
public:\
    bool Write(RsonWriter* rw) const\
    {\
    return rw->WriteInt(#memname, 1);\
    }\
    static void WriteMe(const int gamerIndex)\
    {\
        name##Metric m;\
        rlTelemetry::Write(gamerIndex, m);\
    }\
    static void WriteMeImmediate(const int gamerIndex)\
    {\
        name##Metric m;\
        rlTelemetry::WriteImmediate(gamerIndex, m);\
    }\
};

MAKE_METRIC(Foo,foo);
MAKE_METRIC(Bar,bar);
MAKE_METRIC(Abc,abc);
MAKE_METRIC(Bcd,bcd);
MAKE_METRIC(Cde,cde);
MAKE_METRIC(Def,def);
MAKE_METRIC(Efg,efg);
MAKE_METRIC(Fgh,fgh);
MAKE_METRIC(Ghi,ghi);
MAKE_METRIC(Hij,hij);
MAKE_METRIC(Ijk,ijk);
MAKE_METRIC(Jkl,jkl);
MAKE_METRIC(Klm,klm);
MAKE_METRIC(Lmn,lmn);
MAKE_METRIC(Mno,mno);
MAKE_METRIC(Nop,nop);

typedef void (*MetricWriter)(const int);

struct MetricWriters
{
    MetricWriter Write;
    MetricWriter WriteImmedate;
};

static const MetricWriters s_MetricWriters[] =
{
    {
        &FooMetric::WriteMe,
        &FooMetric::WriteMeImmediate
    },
    {
        &BarMetric::WriteMe,
        &BarMetric::WriteMeImmediate,
    },
    {
        &AbcMetric::WriteMe,
        &AbcMetric::WriteMeImmediate,
    },
    {
        &BcdMetric::WriteMe,
        &BcdMetric::WriteMeImmediate,
    },
    {
        &CdeMetric::WriteMe,
        &CdeMetric::WriteMeImmediate,
    },
    {
        &DefMetric::WriteMe,
        &DefMetric::WriteMeImmediate,
    },
    {
        &EfgMetric::WriteMe,
        &EfgMetric::WriteMeImmediate,
    },
    {
        &FghMetric::WriteMe,
        &FghMetric::WriteMeImmediate,
    },
    {
        &GhiMetric::WriteMe,
        &GhiMetric::WriteMeImmediate,
    },
    {
        &HijMetric::WriteMe,
        &HijMetric::WriteMeImmediate,
    },
    {
        &IjkMetric::WriteMe,
        &IjkMetric::WriteMeImmediate,
    },
    {
        &JklMetric::WriteMe,
        &JklMetric::WriteMeImmediate,
    },
    {
        &KlmMetric::WriteMe,
        &KlmMetric::WriteMeImmediate,
    },
    {
        &LmnMetric::WriteMe,
        &LmnMetric::WriteMeImmediate,
    },
    {
        &MnoMetric::WriteMe,
        &MnoMetric::WriteMeImmediate,
    },
    {
        &NopMetric::WriteMe,
        &NopMetric::WriteMeImmediate,
    }
};

void rlTelemetry::TestSubmitMetric(const int gamerIndex)
{
    MetricWriters writers = s_MetricWriters[s_TelemetryRng.GetInt()%COUNTOF(s_MetricWriters)];

    writers.Write(gamerIndex);
}

void rlTelemetry::TestSubmitImmediateMetric(const int gamerIndex)
{
    MetricWriters writers = s_MetricWriters[s_TelemetryRng.GetInt()%COUNTOF(s_MetricWriters)];

    writers.WriteImmedate(gamerIndex);
}

#endif  //__DEV

#if !__NO_OUTPUT
void
rlTelemetry::DumpSettings()
{
    s_Policies.DumpSettings();
}
#endif // !__NO_OUTPUT

//private:

bool
rlTelemetry::FlushImmediate()
{
	//NOTE - make sure to NOT RETURN from this function
	//until ClearImmediateSlots() has been called.

	//Disable the timer.
	s_ImmediateFlushTimer.Clear();

	for(int i = 0; i < COUNTOF(sm_ImmediateGamerSlots); ++i)
	{
		GamerSlot* slot = &sm_ImmediateGamerSlots[i];

		if(!slot->m_Metrics.GetFirst())
		{
			//No metrics in this slot
			continue;
		}

		if(!slot->m_Cred.IsValid()
			|| slot->m_GamerHandle.GetLocalIndex() < 0)
		{
			//Credentials are invalid for this slot
			//or the gamer has signed out and we don't know about it yet.
			continue;
		}

		//Reset the sequence number when we get a new session ID.
		if(slot->m_Cred.GetSessionId() != slot->m_CurSessionId)
		{
			slot->m_CurSessionId = slot->m_Cred.GetSessionId();
			slot->m_RtSequenceNumber = 0;
		}

		rlFireAndForgetTask<rlRosTelemetryImmediateSubmissionTask>* task = NULL;
		rtry
		{
			rlDebug3("Flushing immediate telemetry for gamer: %d...", i);

			rverify(rlGetTaskManager()->CreateTask(&task),
				catchall,
				rlError("Error creating submission task"));

			rcheck(rlTaskBase::Configure(task, slot, &task->m_Status),
				catchall,
				rlError("Error configuring submission task"));

			const size_t queueId = (size_t)slot;
			rverify(rlGetTaskManager()->AppendSerialTask(queueId, task),
				catchall,
				rlError("Error queuing task"));

			++slot->m_RtSequenceNumber;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
	}

	ClearImmediateSlots();

	return true;
}


bool
rlTelemetry::FlushDeferred()
{
	//NOTE - make sure to NOT RETURN from this function
	//until ClearDeferredSlots() has been called.

#if !__FINAL
	rlDebug3("Flushing deferred telemetry, number of metrics=%d...", s_DeferredMetricCount);
	s_DeferredMetricCount = 0;
#endif // !__FINAL

	//Callback for game
	sm_Delegator.Dispatch(rlTelemetryEventDeferredFlush());

	//Disable the timer.
	s_DeferredFlushTimer.Clear();

	for(int i = 0; i < COUNTOF(sm_DeferredGamerSlots); ++i)
	{
		GamerSlot* slot = &sm_DeferredGamerSlots[i];

		if(!slot->m_Metrics.GetFirst())
		{
			//No metrics in this slot
			continue;
		}

		if(!slot->m_Cred.IsValid()
			|| slot->m_GamerHandle.GetLocalIndex() < 0)
		{
			//Credentials are invalid for this slot
			//or the gamer has signed out and we don't know about it yet.
			continue;
		}

		//Reset the sequence number when we get a new session ID.
		if(slot->m_Cred.GetSessionId() != slot->m_CurSessionId)
		{
			slot->m_CurSessionId = slot->m_Cred.GetSessionId();
			slot->m_RtSequenceNumber = 0;
		}

		rlFireAndForgetTask<rlRosTelemetryImmediateSubmissionTask>* task = NULL;
		rtry
		{
			rlDebug3("Flushing deferred telemetry for gamer: %d...", i);

			rverify(rlGetTaskManager()->CreateTask(&task),
				catchall,
				rlError("Error creating submission task"));

			rcheck(rlTaskBase::Configure(task, slot, &task->m_Status),
				catchall,
				rlError("Error configuring submission task"));

			const size_t queueId = (size_t)slot;
			rverify(rlGetTaskManager()->AppendSerialTask(queueId, task),
				catchall,
				rlError("Error queuing task"));

			++slot->m_RtSequenceNumber;
		}
		rcatchall
		{
			if(task)
			{
				rlGetTaskManager()->DestroyTask(task);
			}
		}
	}

	ClearDeferredSlots();

	//Turn Gate on for Immediate flushing.
	s_DeferredFlushBlocked = true;

	return true;
}

bool
rlTelemetry::WriteHeader(const u64 baseTime, const GamerSlot* /*slot*/, RsonWriter* rw)
{
    if(rw->Begin(NULL, NULL))
    {
        rw->WriteUns64("t", rlGetPosixTime());      //time of submission
        rw->WriteUns64("bt", baseTime);             //base time
        rw->WriteUns("v", TELEMETRY_VERSION);       //version
	
		//Call the game specific header callback.
		const rlTelemetryPolicies& policies = rlTelemetry::GetPolicies();
		if(policies.GameHeaderInfoCallback.IsBound() && !rw->HasError())
		{
			int cursor;
			bool success = false;
			if (rw->Begin("game", &cursor))					//game
			{
                const int gameHeaderBegin = rw->GetCursor();
				success = policies.GameHeaderInfoCallback(rw);
                if(rw->GetCursor() == gameHeaderBegin)
                {
                    //The game didn't write anything.
                    rw->Undo(cursor);
                }
                else
                {
				    rw->End();
                }
			}

			//If we failed or there was an error, back it out.
			if(!success || rw->HasError())
			{
				rlError("There was an error writing with the GameHeaderInfoCallback");
				rw->Undo(cursor);
			}
		}

      rw->End();
    }

	//////////////////////////////////////////////////////////////////////////
	//Write the Metric Header to registered streams
	if (!rw->HasError() && !sm_Streams.empty())
	{
		StreamList::iterator it = sm_Streams.begin();
		StreamList::const_iterator stop = sm_Streams.end();

		for(; stop != it; ++it)
		{
			if(rlVerify(it->Stream))
			{
				it->Stream->Write(rw->ToString(), rw->Length());
				it->Stream->Write("]\n", 2);
				it->Stream->Flush();
			}
		}
	}

    return !rw->HasError();
}

bool
rlTelemetry::Export(RsonWriter* rw,
                    const char* name,
                    const u64 metricTime,
                    const rlMetric& metric)
{
    bool success = false;

    int cursor;
    if(rw->Begin(name, &cursor)) //metric
    {
        rw->WriteString("m", metric.GetMetricName());   //metric name
        rw->WriteUns64("t", metricTime);				//time
        rw->WriteUns64("qt", metric.GetMetricTime());
        int valueCursor;
        bool wroteIt = false;

        if(rw->Begin("v", &valueCursor)) //value
        {
            const unsigned len = rw->Length();
            wroteIt = metric.Write(rw);

            if(!wroteIt)
            {
                //The metric failed to write itself.
                rw->Undo(valueCursor);

				rlDebug1("rlTelemetry::Export: Metric %s (id: %u) failed to write itself. Metric size: %d - Space left in the buffer: %d", 
							 metric.GetMetricName(), metric.GetMetricId(), (int)metric.GetMetricSize(), rw->Available());
            }
            else if(!rw->HasError() && rw->Length() == len)
            {
                //Metric didn't have a value item.
                //Remove empty value items so we don't get "value={}"
                //in the submission.
                rw->Undo(valueCursor);

				rlDebug1("rlTelemetry::Export: Metric %s (id: %u) didn't have a value item. Metric size: %d", 
					         metric.GetMetricName(), metric.GetMetricId(), (int)metric.GetMetricSize());
            }
            else
            {
				rlDebug1("rlTelemetry::Export: wrote Metric %s (id: %u). Metric size: %d - Space left in the buffer: %d",
					metric.GetMetricName(), metric.GetMetricId(), (int)metric.GetMetricSize(), rw->Available());
				rw->End(); //value
            }
        }

		rw->End(); //metric

        success = wroteIt && !rw->HasError();
    }

    if(!success)
    {
        rw->Undo(cursor);
    }

    return success;
}

void
rlTelemetry::DoWriteToStreams(const int localGamerIndex, const u64 curTime, rlGamerHandle& gamerHandle, const rlMetric& metric)
{
	if(!sm_Streams.empty())
	{
		char rsonBuf[RL_TELEMETRY_BOUNCE_BUF_SIZE];
		RsonWriter rw;

		rw.Init(rsonBuf, sizeof(rsonBuf), RSON_FORMAT_JSON);

		if(rw.Begin(NULL, NULL))
		{
			//Write the gamer info

			if(gamerHandle.IsValid())
			{
				char ghBuf[RL_MAX_GAMER_HANDLE_CHARS];
				rlVerify(gamerHandle.ToString(ghBuf));
				rw.WriteString("handle", ghBuf);
			}
			else
			{
				rw.WriteString("handle", "NONE");
			}

			char gamerName[RL_MAX_NAME_BUF_SIZE];
			if(rlPresence::GetName(localGamerIndex, gamerName))
			{
				rw.WriteString("name", gamerName);
			}
			else
			{
				rw.WriteString("name", "NONE");
			}

			//Write the metric
			Export(&rw, "metric", curTime, metric);

			rw.End();
		}

		if(rlVerify(!rw.HasError()))
		{
			StreamList::iterator it = sm_Streams.begin();
			StreamList::const_iterator stop = sm_Streams.end();

			for(; stop != it; ++it)
			{
				if(rlVerify(it->Stream))
				{
					it->Stream->Write(rw.ToString(), rw.Length());
					it->Stream->Write("\n", 1);
					it->Stream->Flush();
				}
			}
		}
	}
}

void
rlTelemetry::WriteLineToStream(const char* text)
{
	StreamList::iterator it = sm_Streams.begin();
	StreamList::const_iterator stop = sm_Streams.end();

	for(; stop != it; ++it)
	{
		if(rlVerify(it->Stream))
		{
			it->Stream->Write(text, istrlen(text));
			it->Stream->Write("\n", 1);
			it->Stream->Flush();
		}
	}
}

bool
rlTelemetry::DoWrite(const int localGamerIndex, const rlMetric& metric)
{
    if(0 == s_Policies.m_SubmissionIntervalSeconds && sm_Streams.empty())
    {
        //Nothing to do.
        return true;
    }

    rlGamerHandle gamerHandle;

    if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        rlDebug3("Gamer index: %d is invalid", localGamerIndex);
        return false;
    }
    else if(!rlPresence::GetGamerHandle(localGamerIndex, &gamerHandle))
    {
        rlDebug3("Gamer index: %d has no signed in gamer", localGamerIndex);
        return false;
    }

    const u64 curTime = rlGetPosixTime();

	//Write the metric to registered streams
	rlTelemetry::DoWriteToStreams(localGamerIndex, curTime, gamerHandle, metric);

    if(0 == s_Policies.m_SubmissionIntervalSeconds)
    {
        //If submission interval is zero then don't submit to ROS.
        return true;
    }

    if(!rlPresence::IsOnline(localGamerIndex))
    {
        //If not online don't submit to ROS.
        return true;
    }

    //Add the metric to the appropriate queue
    const size_t size = metric.GetMetricSize();

    GamerSlot* slot = GetSlot(gamerHandle);

    if(!slot || s_MetricsMemoryBuf.GetAvailableSize() < size)
    {
        Flush();
        slot = GetSlot(gamerHandle);
    }
		
	//Get a block of memory for a new metric
	rlMetric* pNewRec = s_MetricsMemoryBuf.GetNewMetric((unsigned)size);

    if(!rlVerify(pNewRec))
    {
        return false;
    }

    if(!slot->m_Cred.IsValid())
    {
        slot->m_Cred = rlRos::GetCredentials(localGamerIndex);
    }
		
	//Slam the memory for the metric
	sysMemCpy((void*)pNewRec, (void*)&metric, size);

    slot->m_Metrics.Append(pNewRec);

    if(!s_FlushTimer.IsRunning())
    {
        s_FlushTimer.InitSeconds(s_Policies.m_SubmissionIntervalSeconds);
    }

    return true;
}

bool
rlTelemetry::DoWriteImmediate(const int localGamerIndex, const rlMetric& metric)
{
	rlGamerHandle gamerHandle;

	if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		rlDebug3("Gamer index: %d is invalid", localGamerIndex);
		return false;
	}
	else if(!rlPresence::GetGamerHandle(localGamerIndex, &gamerHandle))
	{
		rlDebug3("Gamer index: %d has no signed in gamer", localGamerIndex);
		return false;
	}

	if(!rlPresence::IsOnline(localGamerIndex))
	{
		//Nothing to do.
		return true;
	}

	const u64 curTime = rlGetPosixTime();

	//Write the metric to registered streams
	rlTelemetry::DoWriteToStreams(localGamerIndex, curTime, gamerHandle, metric);

	//Add the metric to the appropriate queue
	const size_t size = metric.GetMetricSize();

	GamerSlot* slot = GetImmediateSlot(gamerHandle);

	if(!slot || s_ImmediateMetricsMemoryBuf.GetAvailableSize() < size)
	{
		FlushImmediate();
		slot = GetImmediateSlot(gamerHandle);
	}

	//Get a block of memory for a new metric
	rlMetric* pNewRec = s_ImmediateMetricsMemoryBuf.GetNewMetric((unsigned)size);

	if(!rlVerify(pNewRec))
	{
		return false;
	}

	if(!slot->m_Cred.IsValid())
	{
		slot->m_Cred = rlRos::GetCredentials(localGamerIndex);
	}

	//Slam the memory for the metric
	sysMemCpy((void*)pNewRec, (void*)&metric, size);

	slot->m_Metrics.Append(pNewRec);

	if(!s_ImmediateFlushTimer.IsRunning())
	{
		s_ImmediateFlushTimer.InitSeconds(RL_TELEMETRY_FLUSH_INTERVAL_IMMEDIATE_SEC);
	}

	return true;
}

bool
rlTelemetry::DoWriteDeferred(const int localGamerIndex, const rlMetric& metric)
{
	rlGamerHandle gamerHandle;

	if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		rlDebug3("Gamer index: %d is invalid", localGamerIndex);
		return false;
	}
	else if(!rlPresence::GetGamerHandle(localGamerIndex, &gamerHandle))
	{
		rlDebug3("Gamer index: %d has no signed in gamer", localGamerIndex);
		return false;
	}

	if(!rlPresence::IsOnline(localGamerIndex))
	{
		//Nothing to do.
		return true;
	}

	const u64 curTime = rlGetPosixTime();

	//Write the metric to registered streams
	rlTelemetry::DoWriteToStreams(localGamerIndex, curTime, gamerHandle, metric);

	//Add the metric to the appropriate queue
	const size_t size = metric.GetMetricSize();

	GamerSlot* slot = GetDeferredSlot(gamerHandle);

	if(!slot || s_DeferredMetricsMemoryBuf.GetAvailableSize() < size)
	{
		FlushDeferred();
		slot = GetDeferredSlot(gamerHandle);
	}

	//Get a block of memory for a new metric
	rlMetric* pNewRec = s_DeferredMetricsMemoryBuf.GetNewMetric((unsigned)size);

	if(!rlVerify(pNewRec))
	{
		return false;
	}

	if(!slot->m_Cred.IsValid())
	{
		slot->m_Cred = rlRos::GetCredentials(localGamerIndex);
	}

	//Slam the memory for the metric
	sysMemCpy((void*)pNewRec, (void*)&metric, size);

	slot->m_Metrics.Append(pNewRec);

	if(!s_DeferredFlushTimer.IsRunning())
	{
		s_DeferredFlushTimer.InitSeconds(RL_TELEMETRY_FLUSH_INTERVAL_IMMEDIATE_SEC);
	}

	return true;
}

rlTelemetry::GamerSlot*
rlTelemetry::GetSlot(const rlGamerHandle& gamerHandle)
{
    GamerSlot* slot = NULL;

    for(int i = 0; i < COUNTOF(sm_GamerSlots); ++i)
    {
        GamerSlot* tmp = &sm_GamerSlots[i];
        if(gamerHandle == tmp->m_GamerHandle)
        {
            slot = tmp;
            break;
        }
        else if(!slot && !tmp->m_GamerHandle.IsValid())
        {
            slot = tmp;
            //Don't break here
        }
    }

    if(slot && !slot->m_GamerHandle.IsValid())
    {
        slot->m_GamerHandle = gamerHandle;

        //Get the gamer's name.
        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
        {
            rlGamerHandle tmp;
            if(rlPresence::GetGamerHandle(i, &tmp)
                && tmp == gamerHandle)
            {
                rlPresence::GetName(i, slot->m_GamerName, COUNTOF(slot->m_GamerName));
                break;
            }
        }
    }

    return slot;
}

rlTelemetry::GamerSlot*
rlTelemetry::GetImmediateSlot(const rlGamerHandle& gamerHandle)
{
	GamerSlot* slot = NULL;

	for(int i = 0; i < COUNTOF(sm_ImmediateGamerSlots); ++i)
	{
		GamerSlot* tmp = &sm_ImmediateGamerSlots[i];
		if(gamerHandle == tmp->m_GamerHandle)
		{
			slot = tmp;
			break;
		}
		else if(!slot && !tmp->m_GamerHandle.IsValid())
		{
			slot = tmp;
			//Don't break here
		}
	}

	if(slot && !slot->m_GamerHandle.IsValid())
	{
		slot->m_GamerHandle = gamerHandle;

		//Get the gamer's name.
		for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
		{
			rlGamerHandle tmp;
			if(rlPresence::GetGamerHandle(i, &tmp)
				&& tmp == gamerHandle)
			{
				rlPresence::GetName(i, slot->m_GamerName, COUNTOF(slot->m_GamerName));
				break;
			}
		}
	}

	return slot;
}


rlTelemetry::GamerSlot*
rlTelemetry::GetDeferredSlot(const rlGamerHandle& gamerHandle)
{
	GamerSlot* slot = NULL;

	for(int i = 0; i < COUNTOF(sm_DeferredGamerSlots); ++i)
	{
		GamerSlot* tmp = &sm_DeferredGamerSlots[i];
		if(gamerHandle == tmp->m_GamerHandle)
		{
			slot = tmp;
			break;
		}
		else if(!slot && !tmp->m_GamerHandle.IsValid())
		{
			slot = tmp;
			//Don't break here
		}
	}

	if(slot && !slot->m_GamerHandle.IsValid())
	{
		slot->m_GamerHandle = gamerHandle;

		//Get the gamer's name.
		for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
		{
			rlGamerHandle tmp;
			if(rlPresence::GetGamerHandle(i, &tmp)
				&& tmp == gamerHandle)
			{
				rlPresence::GetName(i, slot->m_GamerName, COUNTOF(slot->m_GamerName));
				break;
			}
		}
	}

	return slot;
}

void
rlTelemetry::ClearSlots()
{
    for(int i = 0; i < COUNTOF(sm_GamerSlots); ++i)
    {
        sm_GamerSlots[i].Clear();
    }

    s_MetricsMemoryBuf.Reset();
}

void
rlTelemetry::ClearImmediateSlots()
{
	for(int i = 0; i < COUNTOF(sm_ImmediateGamerSlots); ++i)
	{
		sm_ImmediateGamerSlots[i].Clear();
	}

	s_ImmediateMetricsMemoryBuf.Reset();
}

void
rlTelemetry::ClearDeferredSlots()
{
	for(int i = 0; i < COUNTOF(sm_DeferredGamerSlots); ++i)
	{
		sm_DeferredGamerSlots[i].Clear();
	}

	s_DeferredMetricsMemoryBuf.Reset();
}

void
rlTelemetry::WriteQueuedTelemetryMetrics()
{
	if(!rlVerify(sysThreadType::IsUpdateThread()))
	{
		return;
	}

	SYS_CS_SYNC(sm_QueuedMetricsCs);

	while(!sm_QueuedMetrics.empty())
	{
		rlQueuedMetric* queuedMetric = sm_QueuedMetrics.front();
		sm_QueuedMetrics.pop_front();

		rlTelemetry::Write(queuedMetric->m_LocalGamerIndex, *queuedMetric->m_Metric);

		queuedMetric->~rlQueuedMetric();
		RL_FREE(queuedMetric);
	}
}

void
rlTelemetry::ClearQueuedTelemetryMetrics()
{
	SYS_CS_SYNC(sm_QueuedMetricsCs);

	while(!sm_QueuedMetrics.empty())
	{
		rlQueuedMetric* queuedMetric = sm_QueuedMetrics.front();
		sm_QueuedMetrics.pop_front();

		queuedMetric->~rlQueuedMetric();
		RL_FREE(queuedMetric);
	}
}

#if 0
bool
rlTelemetry::WriteStandardHeader(RsonWriter* rw,
                                const rlGamerInfo& gamerInfo,
                                const char* version,
                                const char* sku,
                                const u64 sessionId,
                                const char* sessionType)
{
    static const char PLATFORM_STR[] =
    {
#if RSG_DURANGO
        "durango",
#elif RSG_SCARLETT
        "xboxsx",
#elif RSG_ORBIS
        "orbis",
#elif RSG_PROSPERO
        "prospero",
#elif RSG_PC
        "win",
#else
        "unk",
#endif
    };

    u8 macBytes[6];
    if(!netSocketAddress::GetLocalMacAddress(macBytes))
    {
        sysMemSet(macBytes, 0, sizeof(macBytes));
    }

    u64 mac = 0;
    for(int i = 0; i < 6; ++i)
    {
        mac = (mac << 8) | macBytes[i];
    }

    time_t ltime;
    time(&ltime);

    rw->WriteString("ver", version);
    rw->WriteString("plat", PLATFORM_STR);
    rw->WriteString("sku", sku);
    rw->WriteHex64("mac", mac);

    rw->WriteUns64("tim", ltime);

    if(rw->Begin("usr"))    //list of users
    {
        if(gamerInfo.IsValid()
            && rlVerifyf(gamerInfo.IsLocal()), "Attempted to write telemetry header for non-local gamer")
        {
            char userid[256] = {""};
            char onlineName[RL_MAX_NAME_BUF_SIZE] = {""};
            char offlineName[RL_MAX_NAME_BUF_SIZE] = {""};

            //We only handle one user for now, but we enclose the
            //user info in a list in case we handle more than one user
            //in the future.
            if(rw->Begin()) //Individual user
            {
                if(gamerInfo.IsSignedIn())
                {
                    gamerInfo.GetGamerHandle().ToString(userid, COUNTOF(userid));

                    if(gamerInfo.IsOnline())
                    {
                        safecpy(onlineName, gamerInfo.GetName(), COUNTOF(onlineName));
#if RSG_NP
                        rlPresence::GetOfflineName(gamerInfo.GetLocalIndex(),
                                                    offlineName,
                                                    COUNTOF(offlineName));
#endif  //RSG_NP
                    }
                    else
                    {
                        safecpy(offlineName, gamerInfo.GetName(), COUNTOF(offlineName));
                    }
                }
                else
                {
                    safecpy(offlineName, "NONE", COUNTOF(offlineName));
                }

                if(rw->Begin("n"))  //name
                {
                    if('\0' == offlineName[0])
                    {
                        rlAssert(strlen(onlineName));
                        //Global name
                        rw->WriteString("g", onlineName);
                    }
                    else if('\0' == onlineName[0])
                    {
                        rlAssert(strlen(offlineName));
                        //Local name
                        rw->WriteString("l", offlineName);
                    }
                    else
                    {
                        //Global name
                        rw->WriteString("g", onlineName);
                        //Local name
                        rw->WriteString("l", offlineName);
                    }

                    rw->End();  //name
                }

                if('\0' != userid[0])
                {
                    rw->WriteString("id", userid);
                }

                rw->End();  //Individual user
            }
        }

        rw->End();  //list of users
    }

    if(rw->Begin("ses"))
    {
        rw->WriteUns64("id", sessionId);
        rw->WriteString("t", sessionType);
        rw->End();
    }

    return !rw->HasError();
}

#endif  //0

//Negative numbers for window bits imply raw deflate format (RFC 1951),
//which is what's understood by .NET's System.IO.Compression.DeflateStream.
//From the zlib docs:
//     windowBits can also be -8..-15 for raw deflate. In this case, -windowBits
//   determines the window size. deflate() will then generate raw deflate data
//   with no zlib header or trailer, and will not compute an adler32 check value.
#define __DEFLATE_WINDOW_BITS   -9  /* window size of 512 bytes */
#define __DEFLATE_MEM_LEVEL     1

#define __ABS_DEFLATE_WINDOW_BITS \
	(((__DEFLATE_WINDOW_BITS) < 0) ? -(__DEFLATE_WINDOW_BITS) : (__DEFLATE_WINDOW_BITS))

#define __DEFLATE_MEM_REQUIRED\
	((1 << (__ABS_DEFLATE_WINDOW_BITS+2)) +  (1 << (__DEFLATE_MEM_LEVEL+9)))

#define __INFLATE_MEM_REQUIRED\
	(1 << __ABS_DEFLATE_WINDOW_BITS+2)

#if !__NO_OUTPUT
static const char* ZlibErrorString(const int errCode)
{
	switch(errCode)
	{
	case Z_ERRNO:
		return "Z_ERRNO";
	case Z_STREAM_ERROR:
		return "Z_STREAM_ERROR";
	case Z_DATA_ERROR:
		return "Z_DATA_ERROR";
	case Z_MEM_ERROR:
		return "Z_MEM_ERROR";
	case Z_BUF_ERROR:
		return "Z_BUF_ERROR";
	case Z_VERSION_ERROR:
		return "Z_VERSION_ERROR";
	}

	return "UNKNOWN";
}
#endif  //__NO_OUTPUT

//////////////////////////////////////////////////////////////////////////
//  zlibStream
//////////////////////////////////////////////////////////////////////////
RAGE_DEFINE_CHANNEL(zlib)
#undef __rage_channel
#define __rage_channel zlib

static void* zlibStreamAlloc(void* opaque, unsigned items, unsigned size)
{
	sysMemAllocator* allocator = (sysMemAllocator*)opaque;
	void* mem = NULL;
	if(rageVerify(allocator))
	{
		mem = allocator->TryAllocate(items*size, 0);

	}

	return mem;
}

static void zlibStreamFree(void* opaque, void* ptr)
{
	sysMemAllocator* allocator = (sysMemAllocator*)opaque;
	if(rageVerify(allocator && allocator->IsValidPointer(ptr)))
	{
		allocator->Free(ptr);
	}
}

zlibStream::zlibStream()
	: m_State(STATE_INITIAL)
	, m_Allocator(NULL)
	, m_StreamType(STREAMTYPE_INVALID)
	, m_NumConsumed(0)
	, m_NumProduced(0)
	, m_ZlibErrCode(Z_OK)
{
	sysMemSet(&m_ZlibStrm, 0, sizeof(m_ZlibStrm));

	this->Clear();
}

zlibStream::~zlibStream()
{
	this->Clear();
}

void
zlibStream::Clear()
{
	if(STREAMTYPE_ENCODE == m_StreamType)
	{
		ASSERT_ONLY(const int zErr =) deflateEnd(&m_ZlibStrm);
		rageAssert(Z_OK == zErr || Z_DATA_ERROR == zErr);
	}
	else if(STREAMTYPE_DECODE == m_StreamType)
	{
		(void)rageVerify(inflateEnd(&m_ZlibStrm) == Z_OK);
	}

	m_ZlibStrm.avail_in = m_ZlibStrm.avail_out = 0;
	m_ZlibStrm.next_in = m_ZlibStrm.next_out = NULL;
	m_ZlibStrm.zalloc = zlibStreamAlloc;
	m_ZlibStrm.zfree = zlibStreamFree;
	m_ZlibStrm.opaque = NULL;

	m_Allocator = NULL;

	m_NumConsumed = m_NumProduced = 0;
	m_StreamType = STREAMTYPE_INVALID;
	m_State = STATE_INITIAL;
	m_ZlibErrCode = Z_OK;
}

bool
zlibStream::BeginEncode(sysMemAllocator* allocator)
{
	this->Clear();

	m_ZlibStrm.opaque = allocator;

	int err = deflateInit2(&m_ZlibStrm, 9, Z_DEFLATED, __DEFLATE_WINDOW_BITS,__DEFLATE_MEM_LEVEL,Z_DEFAULT_STRATEGY);

	bool success = rageVerifyf(Z_OK == err, "Error code %s", ZlibErrorString(err));

	if(success)
	{
		m_StreamType = STREAMTYPE_ENCODE;
		m_State = STATE_ENCODING;
	}

	return success;
}

bool
zlibStream::BeginDecode(sysMemAllocator* allocator, int windowbits)
{
	bool success = false;

	this->Clear();

	m_ZlibStrm.opaque = allocator;

	if (0 == windowbits)
		windowbits = __DEFLATE_WINDOW_BITS;

	success = rageVerify(Z_OK == inflateInit2(&m_ZlibStrm, windowbits));

	if(success)
	{
		m_StreamType = STREAMTYPE_DECODE;
		m_State = STATE_DECODING;
	}

	return success;
}

void
zlibStream::Encode(const void* src,
	const unsigned srcLen,
	void* dst,
	const unsigned dstLen,
	unsigned* numConsumed,
	unsigned* numProduced)
{
	rageAssert(this->IsEncoding());
	rageAssert(src || 0 == srcLen);
	rageAssert(dst || STATE_ENCODING == m_State);
	rageAssert(dstLen > 0 || STATE_ENCODING == m_State);

	*numConsumed = *numProduced = 0;

	m_ZlibStrm.avail_in = srcLen;
	m_ZlibStrm.next_in = (Bytef*) src;
	m_ZlibStrm.avail_out = dstLen;
	m_ZlibStrm.next_out = (Bytef*) dst;

	int zflush = Z_NO_FLUSH;

	if(0 == srcLen)
	{
		//Zero srcLen implies the caller has no more data to encode.
		zflush = Z_FINISH;
	}

	m_ZlibErrCode = deflate(&m_ZlibStrm, zflush);

	if(Z_STREAM_END == m_ZlibErrCode)
	{
		m_State = STATE_COMPLETE;
	}
	else if(m_ZlibErrCode != Z_OK)
	{
		rageErrorf("deflate error '%s,%d', msg='%s'.", ZlibErrorString(m_ZlibErrCode), m_ZlibErrCode, m_ZlibStrm.msg ? m_ZlibStrm.msg : "NULL");
		m_State = STATE_ERROR;
	}

	*numConsumed = srcLen - m_ZlibStrm.avail_in;
	*numProduced = dstLen - m_ZlibStrm.avail_out;

	m_NumConsumed += *numConsumed;
	m_NumProduced += *numProduced;
}

void
zlibStream::Decode(const void* src
					,const unsigned srcLen
					,void* dst
					,const unsigned dstLen
					,unsigned* numConsumed
					,unsigned* numProduced)
{
	rageAssert(this->IsDecoding());
	rageAssert(src || 0 == srcLen);
	rageAssert(dst || STATE_DECODING == m_State);
	rageAssert(dstLen > 0 || STATE_DECODING == m_State);

	*numConsumed = *numProduced = 0;

	m_ZlibStrm.avail_in = srcLen;
	m_ZlibStrm.next_in = (Bytef*) src;
	m_ZlibStrm.avail_out = dstLen;
	m_ZlibStrm.next_out = (Bytef*) dst;

	m_ZlibErrCode = inflate(&m_ZlibStrm, Z_SYNC_FLUSH);

	if(Z_STREAM_END == m_ZlibErrCode)
	{
		m_State = STATE_COMPLETE;
	}
	else if(m_ZlibErrCode != Z_OK)
	{
		rageErrorf("inflate error '%s','%d', msg='%s'.", ZlibErrorString(m_ZlibErrCode), m_ZlibErrCode, m_ZlibStrm.msg ? m_ZlibStrm.msg : "NULL");
		m_State = STATE_ERROR;
	}

	*numConsumed = srcLen - m_ZlibStrm.avail_in;
	*numProduced = dstLen - m_ZlibStrm.avail_out;

	m_NumConsumed += *numConsumed;
	m_NumProduced += *numProduced;
}

unsigned
zlibStream::GetNumConsumed() const
{
	return m_NumConsumed;
}

unsigned
zlibStream::GetNumProduced() const
{
	return m_NumProduced;
}

bool
zlibStream::IsEncoding() const
{
	return STATE_ENCODING == m_State;
}

bool
zlibStream::IsDecoding() const
{
	return STATE_DECODING == m_State;
}

bool
zlibStream::IsComplete() const
{
	return STATE_COMPLETE == m_State;
}

bool
zlibStream::HasError() const
{
	return STATE_ERROR == m_State;
}

int
zlibStream::GetErrorCode() const
{
	return m_ZlibErrCode;
}

}; //namespace rage
