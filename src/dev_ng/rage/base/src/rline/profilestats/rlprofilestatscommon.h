// 
// rline/rlprofilestatscommon.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPROFILESTATSCOMMON_H
#define RLINE_RLPROFILESTATSCOMMON_H

#include "rline/rl.h"
#include "rline/rldiag.h"
#include "rline/rlgamerinfo.h"
#include "atl/array.h"
#include "data/autoid.h"
#include "net/status.h"
#include "system/criticalsection.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, profilestats)

//PURPOSE
//  Current availability of rlProfileStats::Flush().
enum rlProfileStatsFlushStatus
{
    RL_PROFILESTATS_FLUSH_READY = 0,            //There are dirty stats, and we're ready to write them
    RL_PROFILESTATS_FLUSH_INVALID_CREDENTIALS,  //Gamer does not have valid ROS credentials yet
    RL_PROFILESTATS_FLUSH_TOO_SOON_SINCE_LAST,  //Too soon since the last flush was done
    RL_PROFILESTATS_FLUSH_DISABLED,             //Writes have been disabled completely
};

//PURPOSE
//  Flags that control how rlProfileStats::Flush() behaves.
enum rlProfileStatsFlushFlags
{
    RL_PROFILESTATS_FLUSH_NONE = 0,                    // Default
    RL_PROFILESTATS_FLUSH_FORCE = (1 << 0),            // Ignore flush throttling
};

//PURPOSE
//  Valid types for a profile stat.
//IMPORTANT
//  These values MUST match the backend.  Do not change them.
enum rlProfileStatsType
{
    RL_PROFILESTATS_TYPE_INVALID    = -1,
    RL_PROFILESTATS_TYPE_FLOAT      = 3,
    RL_PROFILESTATS_TYPE_INT32      = 4,
    RL_PROFILESTATS_TYPE_INT64      = 0
};

//PURPOSE
//  Priority levels for rlProfileStats::SetDirty().
enum rlProfileStatsPriorityLevel
{
    RL_PROFILESTATS_MIN_PRIORITY = 0,
    RL_PROFILESTATS_MAX_PRIORITY = 15
};

//PURPOSE
//  Formats of how profile stats are returned by the backend
enum rlProfileStatsFormat
{
    RL_PROFILESTATS_FORMAT_INVALID = -1,
    RL_PROFILESTATS_FORMAT_BASE64VALUES = 0,
    RL_PROFILESTATS_FORMAT_TEXTVALUES = 1,
    RL_PROFILESTATS_FORMAT_BASE64IDTYPEVALUE = 2,
    RL_PROFILESTATS_FORMAT_BASE64TYPEVALUE = 3
};

enum rlProfileStatsSubmissionType
{
    RL_PROFILESTATS_SUBMISSION_TYPE_SORTED = 0,
    RL_PROFILESTATS_SUBMISSION_TYPE_SORTED_COMPRESSED = 1
};

typedef u8 rlProfileStatsPriority;

//PURPOSE
//  Stores a value that can be of any type supported by profile stats.
class rlProfileStatsValue
{
public:
    rlProfileStatsValue();

    rlProfileStatsType GetType() const { return m_Type; }
    bool IsValid() const { return m_Type != RL_PROFILESTATS_TYPE_INVALID; }
    void Clear() { m_Type = RL_PROFILESTATS_TYPE_INVALID; }

    //Sets the value to specified type and value.
    void SetFloat(const float val);
    void SetInt32(const s32 val);
    void SetInt64(const s64 val);

    //Returns the value as specified type.  This will assert if value is not that type.
    float GetFloat() const;
    s32 GetInt32() const;
    s64 GetInt64() const;

    //Returns string representation of value.
    const char* ToString(char* buf, const unsigned bufSize) const;
    template<int SIZE>
    const char* ToString(char (&buf)[SIZE]) const
    {
        return ToString(buf, SIZE);
    }

    //Returns byte size of serialized value and (optionally) type.
    static bool GetSerializedSize(const rlProfileStatsType type,
                                  unsigned* size, 
                                  const bool includeType = true);

    //Deserializes type and value.
    bool Deserialize(const u8* buf, 
                     const unsigned bufSize,
                     const unsigned offset,
                     unsigned* numBytes);

    //Deserializes value only, since type is specified.
    bool Deserialize(const u8* buf, 
                     const unsigned bufSize,
                     const unsigned offset, 
                     const rlProfileStatsType type,
                     unsigned* numBytes);

    //Serializes value and (optionall) type.
    bool Serialize(u8* buf, 
                   const unsigned bufSize,
                   const unsigned offset, 
                   unsigned* numBytes, 
                   const bool includeType = true) const;

    //Operator overrides.
    bool operator==(const rlProfileStatsValue& that) const;
    bool operator!=(const rlProfileStatsValue& that) const;

private:
    union ValueUnion
    {
        float   m_Float;
        s32     m_Int32;
        s64     m_Int64;
    };

    ValueUnion m_Union;
    rlProfileStatsType m_Type;
};

//PURPOSE
//  A base class used to define a record within rlProfileStatsReadResults.
class rlProfileStatsRecordBase
{
public:
    rlProfileStatsRecordBase() {}
    virtual ~rlProfileStatsRecordBase() {}

    const rlGamerHandle& GetGamerHandle() const { return m_GamerHandle; }   
    const rlProfileStatsValue& GetValue(const unsigned column) const;

protected:
	virtual const rlProfileStatsValue* GetValues() const = 0;
	virtual unsigned GetNumValues() const = 0;

private:
    friend class rlProfileStatsReadResults;

    rlGamerHandle m_GamerHandle;
};

//PURPOSE
//  A concrete class used to define a record within a rlProfileStatsReadResults. 
//  The T_NUM_COLUMNS defines the max number of columns the record can contain.
template <unsigned T_NUM_COLUMNS>
class rlProfileStatsFixedRecord : public rlProfileStatsRecordBase
{
public:
    rlProfileStatsFixedRecord() {}
    virtual ~rlProfileStatsFixedRecord() {}

protected:
	virtual const rlProfileStatsValue* GetValues() const { return m_Values; }
    virtual unsigned GetNumValues() const override { return T_NUM_COLUMNS; }

private:
    rlProfileStatsValue m_Values[T_NUM_COLUMNS];
};

//PURPOSE
//  Stores the results of a rlProfileStats::ReadStatsByGamer() call.
class rlProfileStatsReadResults
{
public:
    rlProfileStatsReadResults();

    //PURPOSE
    //  Clears the results.  Reset must be called again before the results may be used.
    void Clear();

    //PURPOSE
    //  Resets/initializes the results for reading.
    //PARAMS
    //  statIds         - IDs of stats to read
    //  numStatIds      - Number of elements in statIds array
    //  maxRows         - Maximum number of rows in the results
    //  records         - Array of records, of at least maxRows length.
    //                    Each record must have at least numStatIds columns.
    //  recordSpan      - Byte size of each record (sizeof(records type))
    bool Reset(const int* statIds,
               const unsigned numStatIds,
               const unsigned maxRows,
               rlProfileStatsRecordBase* records,
               const unsigned recordSpan);

    //Basic accessors
    int GetStatId(const unsigned column) const { return m_StatIds[column]; }
    int GetStatColumn(const int statId) const;
    unsigned GetNumStatIds() const { return m_NumStatIds; }
    unsigned GetMaxRows() const { return m_MaxRows; }
	const int* GetRequestedStatIds() { return m_StatIds; }

    //Result accessors
    unsigned GetNumRows() const { return m_NumRows; }
    const rlGamerHandle* GetGamerHandle(const unsigned row) const;
    const int FindGamerHandle(const rlGamerHandle& gamerHandle);
    const rlProfileStatsValue* GetValue(const unsigned row, const unsigned column) const ;   

	bool SetNumRows(const unsigned num);
	bool SetGamerHandle(const unsigned row, const rlGamerHandle& gamerHandle);

private:
    friend class rlProfileStatsReadStatsByGamerTask;

    const int* m_StatIds;
    unsigned m_NumStatIds;
    unsigned m_MaxRows;
    unsigned m_NumRows;
    atSpanArray<rlProfileStatsRecordBase> m_Records;
};

//PURPOSE
//  IDs of events generated by rlProfileStats.
//  See the event classes below for descriptions.
enum rlProfileStatsEventType
{
    RL_PROFILESTATS_EVENT_GET_LOCAL_VALUE = 0,
    RL_PROFILESTATS_EVENT_SYNCHRONIZE_STAT,
    RL_PROFILESTATS_EVENT_SYNCHRONIZE_GROUP,
    RL_PROFILESTATS_EVENT_FLUSH_STAT,
};

//PURPOSE
//  Helper macros for rlProfileStatsEvents.
#define RL_PROFILESTATS_EVENT_COMMON_DECL( name )\
    static unsigned EVENT_ID() { return name::GetAutoId(); }\
    virtual unsigned GetId() const { return name::GetAutoId(); }

#define RL_PROFILESTATS_EVENT_DECL( name, id )\
    AUTOID_DECL_ID( name, rage::rlProfileStatsEvent, id )\
    RL_PROFILESTATS_EVENT_COMMON_DECL( name )

//PURPOSE
//  Base class for events generated by rlProfileStats.
class rlProfileStatsEvent
{
public:
    AUTOID_DECL_ROOT(rlProfileStatsEvent);
    RL_PROFILESTATS_EVENT_COMMON_DECL(rlProfileStatsEvent);

    rlProfileStatsEvent() {}
    virtual ~rlProfileStatsEvent() {}
};

//PURPOSE
//  Event generated when rlProfileStats needs the current local value of a stat.
//  The client code should call one of the SetXXX() methods on the value.
//  If the stat is unknown to the client, doing nothing or calling Clear() is allowed.
class rlProfileStatsGetLocalValueEvent : public rlProfileStatsEvent
{
public:
    RL_PROFILESTATS_EVENT_DECL(rlProfileStatsGetLocalValueEvent, 
                               RL_PROFILESTATS_EVENT_GET_LOCAL_VALUE);

    rlProfileStatsGetLocalValueEvent(const int localGamerIndex, 
                                     const int statId,
                                     rlProfileStatsValue* val)
    : m_LocalGamerIndex(localGamerIndex)
    , m_StatId(statId)
    , m_Value(val)
    {
    }

    int m_LocalGamerIndex; 
    int m_StatId;
    rlProfileStatsValue* m_Value;

};

//PURPOSE
//  Event generated during synchronization for any stat, regardless
//  of whether or not the local value differs. Clients are expected 
//  to track dirty stats for client-authoritative stats so that the
//  client publishes them to the backend during the next flush.
//  For server-authritative stats, clients are expected to store
//  the new value.
class rlProfileStatsSynchronizeStatEvent : public rlProfileStatsEvent
{
public:
    RL_PROFILESTATS_EVENT_DECL(rlProfileStatsSynchronizeStatEvent,
                               RL_PROFILESTATS_EVENT_SYNCHRONIZE_STAT);

    rlProfileStatsSynchronizeStatEvent(const int localGamerIndex, 
                                       const int statId,
                                       const rlProfileStatsValue* val)
    : m_LocalGamerIndex(localGamerIndex)
    , m_StatId(statId)
    , m_Value(val)
    {
    }

    int m_LocalGamerIndex; 
    int m_StatId;
    const rlProfileStatsValue* m_Value;
};

//PURPOSE
//  Event generated during synchronization for groups of stats.
//  When triggered, it means that local client has already read
//  up-to-date stats in the specified group. Stats in the group
//  should still be considered dirty, but don't need to be
//  synched with profile stats again.
class rlProfileStatsSynchronizeGroupEvent : public rlProfileStatsEvent
{
public:
    RL_PROFILESTATS_EVENT_DECL(rlProfileStatsSynchronizeGroupEvent,
                               RL_PROFILESTATS_EVENT_SYNCHRONIZE_GROUP);

    rlProfileStatsSynchronizeGroupEvent(const int localGamerIndex,
                                        const unsigned groupId)
    : m_LocalGamerIndex(localGamerIndex)
    , m_GroupId(groupId)
    {}

    const int m_LocalGamerIndex;
    const unsigned m_GroupId;
};

//PURPOSE
//  Event generated during stats Flush to indicate that a stat has
//  actually been flushed. Clients are expected to internally
//  manage this stat and not return this stat in future flushes
//  unless the stat is modified again after this event is triggered
//  (i.e. when it becomes dirty again).
class rlProfileStatsFlushStatEvent : public rlProfileStatsEvent
{
public:
    RL_PROFILESTATS_EVENT_DECL(rlProfileStatsFlushStatEvent,
                               RL_PROFILESTATS_EVENT_FLUSH_STAT);

    rlProfileStatsFlushStatEvent(const int localGamerIndex, const int statId)
    : m_LocalGamerIndex(localGamerIndex)
    , m_StatId(statId)
    {
    }

    const int m_LocalGamerIndex;
    const int m_StatId;
};

//PURPOSE
//  Structure that defines a dirty stat to be published, including its
//  flush priority, value, and id.
struct rlProfileDirtyStat
{
    rlProfileDirtyStat() 
    : m_StatId(0)
    , m_StatValue()
    , m_StatPriority(RL_PROFILESTATS_MIN_PRIORITY)
    {}

    rlProfileDirtyStat(const int statId, const rlProfileStatsValue statValue, const rlProfileStatsPriority statPriority)
    {
        Reset(statId, statValue, statPriority);
    }

    int GetStatId() const { return m_StatId; }
    rlProfileStatsPriority GetPriority() const { return m_StatPriority; }
    rlProfileStatsValue GetValue() const { return m_StatValue; }

    void Clear()
    {
        m_StatId = 0;
        m_StatValue.Clear();
        m_StatPriority = RL_PROFILESTATS_MIN_PRIORITY;
    }

    void Reset(const int statId, const rlProfileStatsValue statValue, const rlProfileStatsPriority statPriority)
    {
        rlAssert(statPriority <= RL_PROFILESTATS_MAX_PRIORITY);
        m_StatId = statId;
        m_StatValue = statValue;
        m_StatPriority = statPriority;
    }

    rlProfileDirtyStat& operator=(const rlProfileDirtyStat& rhs)
    {
        Reset(rhs.m_StatId, rhs.m_StatValue, rhs.m_StatPriority);

        return *this;
    }

private:
    int m_StatId;
    rlProfileStatsValue m_StatValue;
    rlProfileStatsPriority m_StatPriority;
};

//PURPOSE
//  Interface for providing "dirty" stats that need to be published
//  in order of their priority during a stats flush
class rlProfileStatsDirtyIterator
{
public:
    rlProfileStatsDirtyIterator()
    : m_FlushedCount(0)
    {}

    virtual ~rlProfileStatsDirtyIterator() {}

    // Fills in the current stat that this iterator is at and returns true,
    // or returns false if this iterator doesn't have one (i.e. AtEnd() is true)
    virtual bool GetCurrentStat(rlProfileDirtyStat &stat) const = 0;

    // Advances this iterator to the next dirty stat
    virtual void Next() = 0;

    // Returns true if this iterator has been advanced past the last element
    virtual bool AtEnd() const = 0;

    unsigned GetFlushedCount() const { return m_FlushedCount; }

private:
    friend class rlProfileStatsWriter;

    unsigned m_FlushedCount;
};

//PURPOSE
//  Helper class that manages writing out dirty stats
class rlProfileStatsWriter
{
public:
    // Serializes as many dirty stats as possible.
    // Less efficient than SerializeByType, as each stat has its type serialized separately
    // Format is: [{Id, Type, Value}]
    static bool SerializeByPriority(rlProfileStatsDirtyIterator &iter,
                                    const int localGamerIndex,
                                    u8 *buf,
                                    const unsigned bufSize,
                                    const unsigned offset,
                                    unsigned *bytesWritten,
                                    unsigned *statsWritten);

    // Serializes as many dirty stats as possible, ordered by their type
    // This is more efficient than SerializeByPriority as a stat's type doesn't have to be serialized
    // per stat
    // Format is: {i64 count} [{Id, Value}] {i32 count} [{Id, Value}] {float count} [{Id, Value}]
    // Note that tmpIntBuf and tmpFloatBuf must be at least as large as submissionBuf (they don't need
    // to be any larger).
    static bool SerializeByType(rlProfileStatsDirtyIterator &iter,
                                const int localGamerIndex,
                                u8 *submissionBuf,
                                u8 *tmpIntBuf,
                                u8 *tmpFloatBuf,
                                const unsigned bufSize,
                                const unsigned offset,
                                unsigned *bytesWritten,
                                unsigned *statsWritten);
};

//PURPOSE
//  Defines a profile stats group, including the group id and the version stat ids for the group
struct rlProfileStatsGroupDefinition
{
    unsigned m_GroupId;
    //This is the version stat of the profile stats save
    int m_GroupVersionStatId;
    //This is the verison stat of the other save we may
    //already have loaded the same values from (e.g. a
    //savegame).
    int m_AuthGroupVersionStatId;
    //The value of the local profile stats version
    rlProfileStatsValue m_LocalGroupVersion;
    //The value of the local auth stats version
    rlProfileStatsValue m_LocalAuthGroupVersion;

    rlProfileStatsGroupDefinition()
    {}

    rlProfileStatsGroupDefinition(const unsigned groupId, 
                                  const int groupVersionStatId, 
                                  const int authGroupVersionStatId,
                                  const rlProfileStatsValue& localGroupVersion,
                                  const rlProfileStatsValue& localAuthGroupVersion)
    : m_GroupId(groupId)
    , m_GroupVersionStatId(groupVersionStatId)
    , m_AuthGroupVersionStatId(authGroupVersionStatId)
    , m_LocalGroupVersion(localGroupVersion)
    , m_LocalAuthGroupVersion(localAuthGroupVersion)
    {}
};

//An arbitrary limit on the number of groups we can sync at once to avoid dynamic allocs
const static int RL_PROFILESTATS_MAX_SYNC_GROUPS = 10;

} //namespace rage

#endif  //RLINE_RLPROFILESTATSCOMMON_H
