// 
// rline/rlprofilestatsclient.h 
// 
// Copyright (C) 2010-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPROFILESTATSCLIENT_H
#define RLINE_RLPROFILESTATSCLIENT_H

#include "rlprofilestatscommon.h"
#include "rlprofilestatstasks.h"
#include "rline\rltask.h"
#include "net\status.h"

namespace rage
{

//PURPOSE
//  Helper class for rlProfileStats that manages the state of a local gamer.  T
//  This should not be referenced by title code.
class rlProfileStatsClient
{
public:
    rlProfileStatsClient();
    ~rlProfileStatsClient();

    bool Init(const int localGamerIndex,
              const unsigned maxSubmissionSize,
              const bool submitCompressed);
    void Shutdown();
    void Update(const unsigned timeStep);

    //PURPOSE
    //  Writes dirty stats to the backend.
    bool Flush(rlProfileStatsDirtyIterator &flushIt, netStatus* status, const unsigned flags = RL_PROFILESTATS_FLUSH_NONE);

    //PURPOSE
    //  Returns current status in regards to flushing stats.  
    //  Any result other than RL_PROFILESTATS_FLUSH_READY indicates a
    //  call to Flush() is guaranteed to fail.
    rlProfileStatsFlushStatus GetFlushStatus() const;

    //PURPOSE
    //  Returns the local gamer index of this client.
    int GetLocalGamerIndex() const;

    //PURPOSE
    //  Requests the given set of stats for the given set of gamers.
    //  Results will be stored in the given results object.
    bool ReadStatsByGamer(const rlGamerHandle* gamers,
                          const unsigned numGamers,
                          rlProfileStatsReadResults* results, 
                          netStatus* status);

    //PURPOSE
    //  Reads all the stats from the backend for specified local gamer,
    //  compares them with current local values, and calls back to title code
    //  when they differ.  The title should then either call SetDirty() to
    //  note that the local value needs to be written to the backend, or
    //  simply store the value if it is server-authoritative.
    bool Synchronize(netStatus* status);

    //PURPOSE
    //  Similar to synchronize, but only synchronizes stats in the specified
    //  groups.
    bool SynchronizeGroups(const atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                           netStatus* status);

    //PURPOSE
    //  Similar to synchronize groups, but only synchronizes if there's a version
    //  mismatch
    bool ConditionalSynchronizeGroups(const atFixedArray<rlProfileStatsGroupDefinition, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                                      netStatus* status);

    //PURPOSE
    //  Resets a group of stats for a particular gamer. The groups are defined
    //  via the Profile Stats CSV (the "group" column). 
    bool ResetStatsByGroup(const unsigned groupId, netStatus* status);

protected:
    //PURPOSE
    //  Sets the minimum time until we can perform our next Flush().
    //  A delay of zero is interpreted as "disable", and the client will
    //  not flush again for this boot of the game.
    void SetMsUntilNextFlush(const unsigned ms);

private:
    friend class rlProfileStats;
    friend class rlProfileStatsFlushTask;

    int m_LocalGamerIndex;
    unsigned m_MaxSubmissionSize;
    bool m_SubmitCompressed;

    unsigned m_MsUntilNextFlush;
    bool m_FlushEnabled;

    // The default delay between flushes, used until the backend tells us
    // what the delay should be
    const static unsigned ms_DefaultFlushDelaySecs = 60 * 5;
};

} //namespace rage

#endif  //RLINE_RLPROFILESTATSCLIENT_H
