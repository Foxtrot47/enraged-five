// 
// rline/rlprofilestats.h 
// 
// Copyright (C) 2010-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPROFILESTATS_H
#define RLINE_RLPROFILESTATS_H

#include "rlprofilestatscommon.h"
#include "rline\rltask.h"
#include "atl\delegate.h"
#include "net\status.h"

namespace rage
{

//PURPOSE
//  "Profile stats" are locally tracked stats whose latest value is periodically
//  persisted to the ROS backend.  From there, they may be queried and used
//  for things like Social Club viewing, in-game comparisons with other players
//  (ala RDR's Comparisons page), designer tuning, or surveying user hardware
//  and settings (like display mode, NAT type, etc).
//
//  Profile stats differ from other types of stats:
//  - Unlike leaderboard stats, they are not associated with a match and
//    the ONLY aggregation they support is "last".
//  - Unlike telemetry, which is a stream of events, profile stats only 
//    store the latest value reported.
//
//  Note that profile stats are only periodically persisted to ROS;
//  because a title may track hundreds or thousands of profile stats, 
//  writing every stat change as they occur is simply not feasible.
//  The rate at which stats may be written is throttled internally
//  by rlProfileStats based on real-time feedback from ROS, and cannot
//  be changed on the client.  The rate for your title will be determined 
//  by RAGE based on sales estimates and available hardware, and transparently
//  enforced.  Typically this rate is one submit per gamer every ten minutes,
//  where a submission may contain up to about a hundred changed stats.
//
//  This also means that it is normal for the backend stats to be slightly
//  out of date with the locally stored values, so make sure to factor that
//  into your plans.
//
//USAGE
//  Each title defines the stats they want to track in a .csv file, which they 
//  then upload to the ROS backend via the Title Manager web tool. Once the stat 
//  definitions are uploaded, rlProfileStats can be used to read and write 
//  stats from in-game.
//
//  To integrate profile stats into your title, do the following:
//
//  1. Contact RAGE networking to do any necessary ROS setup to allow your
//     title to use profile stats.
//  
//  2. Define your profile stats, and upload the definitions to ROS via Title Manager.
//  
//  3. During initialization, call rlProfileStats::Init().
//  
//  4. Call rlProfileStats::Update() each frame.
//  
//  5. After you initialize all your local stat values (usually when loading a
//     save game), call rlProfileStats::Synchronize().  This lets rlProfileStats
//     know that it needs to do a comparison of all the player's stats on the
//     backend with their new local values, so it knows which ones are the same
//     and which differ.  This allows it to avoid sending unchanged stats later on.
//  
//  6. At appropriate times (ex. when player saves their game), call rlProfileStats::Flush()
//     to persist the current value of dirty stats to the backend. You must provide an
//     implementation of the rlProfileStatsDirtyIterator interface which allows
//     rlProfileStats to iterate over dirty stats that need to be published. The game
//     is responsible for managing what stats are dirty and need to be published. The
//     rlProfileStatsFlushStatEvent can be used to know when a stat has been flushed
//     and should no longer be considered dirty.
//  
//  7. Call rlProfileStats::Shutdown() when shutting down networking. 
//    

class rlProfileStats
{
public:
    static int MAX_NUM_GAMERS_FOR_STATS_READ; //-1 is disabled

    //PURPOSE
    //  Delegator that calls back when an event has occurred the game may be interested in.
    typedef atDelegate<void (const class rlProfileStatsEvent*)> EventDelegate;

    //PURPOSE
    //  Init/Update/Shutdown
    static bool Init(const EventDelegate& eventDlgt, 
                     const unsigned maxSubmissionSize,
                     const bool submitCompressed = true);
    static bool IsInitialized();
    static void Shutdown();
    static void Update();

    //PURPOSE
    //  Cancels a pending operation.
    static void Cancel(netStatus* status);

    //PURPOSE
    //  Submits the current value of dirty stats to the backend.  Because this is
    //  an expensive operation, submissions are throttled at a rate set by the 
    //  backend at runtime.  This means that if Flush is called too soon after the 
    //  previous call, nothing will be sent. This behavior can be changed by
    //  specifying the RL_PROFILESTATS_FLUSH_FORCE flag.
    //  Also, only so many stats may be submitted per flush, so it is not 
    //  guaranteed that every currently dirty stat will have been flushed.
    //IMPORTANT!
    //  It may be several minutes before the submitted stats show up in a read call.
    static bool Flush(rlProfileStatsDirtyIterator &flushIt, const int localGamerIndex, netStatus* status, const unsigned flags = RL_PROFILESTATS_FLUSH_NONE);

    //PURPOSE
    //  Returns current flush status.  Any result other than 
    //  RL_PROFILESTATS_FLUSH_READY indicates a call to Flush() is guaranteed 
    //  to fail.  It is harmless to call Flush() without calling this first.
    //NOTE
    //  This method is provided primarily as a debugging aid.  It is neither
    //  necessary nor more efficient to call this in production code.
    static rlProfileStatsFlushStatus GetFlushStatus(const int localGamerIndex);

    //PURPOSE
    //  Requests the given set of stats for the given set of gamers.
    //  Results will be stored in the given results object.
    //IMPORTANT!
    //  This is not a free operation; calling this more than once every
    //  few minutes may lead to the backend being overwhelmed on launch week.
    //  If you have a need to call it frequently, notify rage networking
    //  immediately so we can adjust our load tests, and suggest alternative
    //  approaches if it's determined to be too much load.  Do NOT wait until the
    //  client code is locked to notify us; there likely won't be anything we can 
    //  do, and you run the risk of this function being unusable on launch week.
    static bool ReadStatsByGamer(const int localGamerIndex, 
                                 const rlGamerHandle* gamers,
                                 const unsigned numGamers,
                                 rlProfileStatsReadResults* results, 
                                 netStatus* status);
    static void SetMaxNumGamersForStatsRead(const unsigned numGamers);

    //PURPOSE
    //  Reads all the stats from the backend for specified local gamer,
    //  compares them with current local values, and calls back to title code
    //  when they differ.  The title should then either call SetDirty() to
    //  note that the local value needs to be written to the backend, or
    //  simply store the value if it is server-authoritative.
    //IMPORTANT!
    //  This is not a free operation; calling this more than once every
    //  few minutes may lead to the backend being overwhelmed on launch week.
    //  If you have a need to call it frequently, notify rage networking
    //  immediately so we can adjust our load tests, and suggest alternative
    //  approaches if it's determined to be too much load.  Do NOT wait until the
    //  client code is locked to notify us; there likely won't be anything we can 
    //  do, and you run the risk of this function being unusable on launch week.
    static bool Synchronize(const int localGamerIndex, netStatus* status);

    //PURPOSE
    //  Similar to synchronize, but only synchronizes stats in the specified
    //  groups. Useful for speeding up sync time, for example, when entering
    //  MP by only syncing MP stats for the current character.
    static bool SynchronizeGroups(const int localGamerIndex,
                                  const atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                                  netStatus* status);

    //PURPOSE
    //  Similar to synchronize groups, but uses a versioning stat to skip the sync
    //  if the backend version matches the local version.
    //  Special care must be taken when using this to ensure that "version" stats
    //  are never flushed before stats in the same version group have been,
    //  otherwise the backend will think it has the latest version when it doesn't.
    //  Also note that this should really only be used if all stats in the specified
    //  groups have been loaded from some other source already.
    static bool ConditionalSynchronizeGroups(const int localGamerIndex, 
                                             const atFixedArray<rlProfileStatsGroupDefinition, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                                             netStatus* status);



    //PURPOSE 
    //  Reset a group of stats for specified local gamer. The group of stats specified 
    //  are reset to their default values. 
    static bool ResetStatsByGroup(const int localGamerIndex, 
                                  const unsigned group,
                                  netStatus* status);

protected:
    //PURPOSE
    //  Sets the minimum time until we can perform our next Flush().
    //  A delay of zero is interpreted as "disable", and the client will
    //  not flush again for this boot of the game.
    static void SetMsUntilNextFlush(const int localGamerIndex, const unsigned ms);

private:
    friend class rlProfileStatsClient;
    friend class rlProfileStatsFlushTask;
    friend class rlProfileStatsSynchronizeTask;
    friend class rlProfileStatsReadStatsByGamerTask;
    friend class rlProfileStatsWriteStatsTask;
    friend class rlProfileStatsWriter;
    friend class rlProfileStatsConditionalSynchronizeGroupsTask;

    //PURPOSE
    //  Don't allow creating instances.  Users only call static methods.
    rlProfileStats();

    static EventDelegate m_EventDelegate;
    static rlTaskManager m_TaskMgr;   

    //Config settings read from the backend at runtime.
    struct BackendConfig
    {
        BackendConfig()
        : m_WriteStatsMaxSubmissionBinarySize(0)
        {
        }

        int m_WriteStatsMaxSubmissionBinarySize;
    };

    static BackendConfig m_BackendConfig;

    static bool m_Initialized;
};

} //namespace rage

#endif  //RLINE_RLPROFILESTATS_H
