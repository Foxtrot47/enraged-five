// 
// rline/rlprofilestatstasks.cpp
// 
// Copyright (C) 2010-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "rlprofilestatstasks.h"
#include "rlprofilestatsclient.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "parser/manager.h"
#include "system/alloca.h"
#include "system/endian.h"
#include "system/param.h"
#include "system/timer.h"
#include "system/simpleallocator.h"
#include "system/nelem.h"

// included for zlipStream
#include "rline/rltelemetry.h"
#include "rline/rlpresence.h"

namespace rage
{

PARAM(rlProfileStatsFlushErrorDelaySecs, "Delay range after an attempt to write stats fails");

extern sysMemAllocator* g_rlAllocator;

//rlProfileStatsStreamParser

rlProfileStatsStreamParser::rlProfileStatsStreamParser(const rlProfileStatsFormat format)
: m_Format(format)
{
    ResetStream();
}

void 
rlProfileStatsStreamParser::ResetStream()
{
    m_Base64Ctx.Clear();
    m_NumLeftoverBytes = 0;
    m_HaveStatId = false;
    m_HaveStatType = false;
}

bool
rlProfileStatsStreamParser::ParseStream(class Receiver& receiver, const char* ch, const int length)
{
    unsigned totalConsumed = 0;
    unsigned numConsumed = 0;
    do 
    {
        static const unsigned decodedBufferSize = 256;
        u8 decodedBuffer[decodedBufferSize];
        unsigned decodedBufferCount = 0;

        //First, copy over anything to our decoded buffer from
        //our leftovers
        CompileTimeAssert(COUNTOF(decodedBuffer) >= COUNTOF(m_LeftoverBuffer));
        sysMemCpy(decodedBuffer, m_LeftoverBuffer, m_NumLeftoverBytes);
        decodedBufferCount += m_NumLeftoverBytes;
        m_NumLeftoverBytes = 0;

        NET_ASSERTS_ONLY(const unsigned numBytes = decodedBufferCount;)

        //Try to decode what we've received so far
        numConsumed = datBase64::Decode(&m_Base64Ctx,
                                        ch,
                                        totalConsumed,
                                        length,
                                        COUNTOF(decodedBuffer),
                                        decodedBuffer,
                                        &decodedBufferCount);

        if(!numConsumed)
        {
            rlAssert(numBytes == decodedBufferCount);
            break;
        }

        totalConsumed += numConsumed;

        //Deserialize as much as we can from the decoded bytes
        unsigned valuesPos = 0;
        while (valuesPos < decodedBufferCount)
        {
            if (!m_HaveStatId)
            {
                if (m_Format == RL_PROFILESTATS_FORMAT_BASE64IDTYPEVALUE)
                {
                    //Read stat ID
                    if((valuesPos + sizeof(m_CurStatId)) > decodedBufferCount)
                    {
                        break; //Haven't read enough data yet
                    }

                    sysMemCpy(&m_CurStatId, &decodedBuffer[valuesPos], sizeof(m_CurStatId));
                    m_CurStatId = sysEndian::BtoN(m_CurStatId);
                    valuesPos += sizeof(m_CurStatId);
                    m_HaveStatId = true;
                }
                else
                {
                    m_HaveStatId = receiver.GetNextStatId(m_CurStatId);
                    rlAssert(m_HaveStatId);
                    if (!m_HaveStatId)
                    {
                        return false;
                    }
                }
            }

            if (m_HaveStatId && !m_HaveStatType)
            {
                if (m_Format == RL_PROFILESTATS_FORMAT_BASE64IDTYPEVALUE
                    || m_Format == RL_PROFILESTATS_FORMAT_BASE64TYPEVALUE)
                {
                    //Read stat type
                    if((valuesPos + sizeof(u8)) > decodedBufferCount)
                    {
                        break; //Haven't read enough data yet
                    }

                    m_CurStatType = (rlProfileStatsType)decodedBuffer[valuesPos++];
                    m_HaveStatType = true;
                }
                else
                {
                    m_HaveStatType = receiver.GetNextStatType(m_CurStatType);
                    rlAssert(m_HaveStatType);
                    if (!m_HaveStatType)
                    {
                        return false;
                    }
                }
            }

            if (m_HaveStatId && m_HaveStatType)
            {
                //Read stat value
                unsigned valueSize;
                if(!rlProfileStatsValue::GetSerializedSize(m_CurStatType, &valueSize, false))
                {
                    rlError("Cannot determine size for type: %d (id=%d)", m_CurStatType, m_CurStatId);
                    return false;
                }

                if((valuesPos + valueSize) > decodedBufferCount)
                {                            
                    break; //Haven't read enough data yet
                }

                rlProfileStatsValue statVal;
                if(!statVal.Deserialize(decodedBuffer, COUNTOF(decodedBuffer), valuesPos, m_CurStatType, &valueSize))
                {
                    rlError("Failed to deserialize value (id=%d, type=%d)",
                            m_CurStatId,
                            m_CurStatType);
                    return false;
                }

#if !__NO_OUTPUT
                /*
                char buf[64];
                rlTaskDebug3("\tid=%d  type=%d  val=%s", 
                                m_CurStatId,
                                m_CurStatType,
                                statVal.ToString(buf, sizeof(buf)));
                */
#endif

                // Let task-specific implementation handle processing this stat value
                receiver.ReceiveStat(m_CurStatId, m_CurStatType, statVal);

                //Clear flags
                m_HaveStatId = false;
                m_HaveStatType = false;

                valuesPos += valueSize;
            }
        }

        rlAssert(valuesPos <= decodedBufferCount);

        const unsigned leftover = decodedBufferCount - valuesPos;
        rlAssert(m_NumLeftoverBytes == 0);
        rlAssert(leftover < COUNTOF(m_LeftoverBuffer));

        if(leftover)
        {
            sysMemCpy(m_LeftoverBuffer, &decodedBuffer[valuesPos], leftover);
            m_NumLeftoverBytes += leftover;
        }
    } 
    while(totalConsumed < (unsigned)length);

    return true;
}

//------------------------------------------------------------------------------
// rlProfileStatsFlushTask
//------------------------------------------------------------------------------
rlProfileStatsFlushTask::rlProfileStatsFlushTask()
{
}

rlProfileStatsFlushTask::~rlProfileStatsFlushTask()
{
}

bool 
rlProfileStatsFlushTask::Configure(rlProfileStatsClient* ctx, rlProfileStatsDirtyIterator *flushIt)
{
    rlAssert(flushIt != NULL);
    if (ctx->m_SubmitCompressed)
    {
        return ConfigureCompressed(ctx, *flushIt);
    }
    else
    {
        return ConfigureUncompressed(ctx, *flushIt);
    }
}

bool 
rlProfileStatsFlushTask::ConfigureCompressed(rlProfileStatsClient* ctx, rlProfileStatsDirtyIterator &flushIt)
{
    bool success = false;
    static const int ZLIB_HEAP_SIZE = 10 * 1024;
    // An 8KB uncompressed submission size should allow us to flush ~1K stats in a single go
    // Because this is compressed, the actual submission size may be less than this, but
    // it won't contain more stats than the uncompressed amount we can fit in this buffer
    static const int MAX_SUBMISSION_BUFFER_SIZE = 8 * 1024;
    // Worst case is an expansion of 5 bytes per 16KB ... using 6 to allow for some fudge factor.
    // Also needs 1 byte for the mode.
    static const int MAX_COMPRESSED_BUFFER_SIZE = 1 + MAX_SUBMISSION_BUFFER_SIZE + 6 *(1 + MAX_SUBMISSION_BUFFER_SIZE / 16384);

    // We use statically allocated buffers for submission
    // Note that compressedBuffer/zlibHeap currently need to be at least as large as the submission buffer
    // due to them being used as temporary buffers by SerializeByType.
    // The submission size used may be limited by ctx->m_MaxSubmissionSize and rlProfileStats::m_BackendConfig.m_WriteStatsMaxSubmissionBinarySize
    // The actual submission size may (hopefully not) become a bit larger once compressed
    u8 submission[MAX_SUBMISSION_BUFFER_SIZE];
    u8 compressedBuffer[MAX_COMPRESSED_BUFFER_SIZE];
    u8 zlibHeap[ZLIB_HEAP_SIZE];
    unsigned submissionSize;

    rtry
    {
        rcheck(!flushIt.AtEnd(), catchall, rlTaskError("No dirty stats to flush"));

        //Determine our max submission size.  It will be the lesser of what the
        //title code specified, what the backend tells us and the buffer size deemed "optimum"
        unsigned maxSubmissionSize = Min((unsigned)MAX_SUBMISSION_BUFFER_SIZE, 
                                          ctx->m_MaxSubmissionSize,
                                         (unsigned)rlProfileStats::m_BackendConfig.m_WriteStatsMaxSubmissionBinarySize);

        unsigned numSubmitted = 0;        
        unsigned bytesWritten = 0;

        // SerializeByType expects the two temporary buffers passed to it be at least
        // as large as the submission buffer
        CompileTimeAssert(COUNTOF(compressedBuffer) >= COUNTOF(submission));
        CompileTimeAssert(COUNTOF(zlibHeap) >= COUNTOF(submission));
        rverify(rlProfileStatsWriter::SerializeByType(flushIt,
                                                      ctx->GetLocalGamerIndex(),
                                                      submission, 
                                                      compressedBuffer, // only used as a temp buffer
                                                      zlibHeap,  // only used as a temp buffer
                                                      maxSubmissionSize, 
                                                      0, 
                                                      &bytesWritten, 
                                                      &numSubmitted),
                catchall,
                rlTaskError("Failed to serialize queue; should be impossible"));
        submissionSize = bytesWritten;

        // 'zlibHeap' now belongs to 'zstrm' instatiated below.
        sysMemSimpleAllocator allocator(zlibHeap, COUNTOF(zlibHeap), 7);
#if RAGE_TRACKING
        allocator.SetTallied(false);
#endif // RAGE_TRACKING

        zlibStream zstrm;
        zstrm.BeginEncode(&allocator);

        // 'compressedBuffer' will now be used to store the compressed submission.
        compressedBuffer[0] = (u8)RL_PROFILESTATS_SUBMISSION_TYPE_SORTED_COMPRESSED;
        unsigned totalConsumed = 0;
        unsigned totalProduced = 1; // skip the mode byte.
        unsigned numConsumed, numProduced;
        // Consume all bytes from submission into the zlib buffer.
        while(totalConsumed < submissionSize && !zstrm.HasError())
        {
            zstrm.Encode(&submission[totalConsumed],
                submissionSize - totalConsumed,
                &compressedBuffer[totalProduced],
                COUNTOF(compressedBuffer) - totalProduced,
                &numConsumed,
                &numProduced);

            totalConsumed += numConsumed;
            totalProduced += numProduced;
        }
        rverify(!zstrm.HasError(),
                catchall,
                rlTaskError("Error compressing profile stats: %d", zstrm.GetErrorCode()));

        // Produce any remaining bytes into the zlib buffer.
        while(!zstrm.IsComplete() && !zstrm.HasError())
        {
            zstrm.Encode(NULL,
                0,
                &compressedBuffer[totalProduced],
                COUNTOF(compressedBuffer) - totalProduced,
                &numConsumed,
                &numProduced);
            totalConsumed += numConsumed;
            totalProduced += numProduced;
        }
        rverify(!zstrm.HasError(),
                catchall,
                rlTaskError("Error compressing profile stats: %d", zstrm.GetErrorCode()));

        //Configure WriteStats task to do that actual submission
        rlTaskDebug("Submitting %u stats", numSubmitted);
        rlTaskDebug2("Profile stats compression ratio: %u/%u = %f", 
            totalProduced, totalConsumed, (float)totalProduced/(float)totalConsumed );
        rcheck(rlTaskBase::Configure(&m_WriteStatsTask,
                                     ctx->GetLocalGamerIndex(),
                                     compressedBuffer,
                                     totalProduced,
                                     &m_NumWritten,
                                     &m_SecsUntilNextWrite,
                                     &m_MaxSubmissionBinarySize,
                                     &m_MyStatus),
                                     catchall,
                                     rlTaskError("Failed to configure WriteStats task"));

        success = true;
    }
    rcatchall
    {
        success = false;
    }

    return success;
}

bool 
rlProfileStatsFlushTask::ConfigureUncompressed(rlProfileStatsClient* ctx, rlProfileStatsDirtyIterator &flushIt)
{
    u8* submission = 0;
    char* submissionB64 = 0;
    bool success = false;

    rtry
    {
        rcheck(!flushIt.AtEnd(), catchall, rlTaskError("No dirty stats to flush"));

        //Determine our max submission size.  It will be the lesser of what the
        //title code specified and what the backend tells us.
        unsigned maxSubmissionSize = Min(ctx->m_MaxSubmissionSize, 
                                         (unsigned)rlProfileStats::m_BackendConfig.m_WriteStatsMaxSubmissionBinarySize);

        //Allocate buffers.
        submission = (u8*)RL_ALLOCATE(rlProfileStatsFlushTask, maxSubmissionSize);
        rverify(submission, 
                catchall, 
                rlTaskError("Failed to allocate %d for submission buffer", maxSubmissionSize));
               
        rlAssert(!submissionB64);
        unsigned maxBase64SubmissionSize = datBase64::GetMaxEncodedSize(maxSubmissionSize);        
        submissionB64 = (char*)RL_ALLOCATE(rlProfileStatsFlushTask, maxBase64SubmissionSize);
        rverify(submissionB64, 
                catchall, 
                rlTaskError("Failed to allocate %d for b64 submission buffer", maxBase64SubmissionSize));

        unsigned numSubmitted = 0;        
        unsigned bytesWritten = 0;

        rverify(rlProfileStatsWriter::SerializeByPriority(flushIt, ctx->GetLocalGamerIndex(), submission, maxSubmissionSize, 0, &bytesWritten, &numSubmitted),
                catchall,
                rlTaskError("Failed to serialize queue; should be impossible"));

        //Convert to base64
        unsigned charsUsed;
        rcheck(datBase64::Encode(submission, bytesWritten, submissionB64, maxBase64SubmissionSize, &charsUsed), catchall, );

        //Configure WriteStats task to do that actual submission
        rlTaskDebug("Submitting %u stats", numSubmitted);
        rcheck(rlTaskBase::Configure(&m_WriteStatsTask,
                                     ctx->GetLocalGamerIndex(),
                                     submissionB64,
                                     &m_NumWritten,
                                     &m_SecsUntilNextWrite,
                                     &m_MaxSubmissionBinarySize,
                                     &m_MyStatus),
                                     catchall,
                                     rlTaskError("Failed to configure WriteStats task"));

        success = true;
    }
    rcatchall
    {
        success = false;
    }

    if (submission)
    {
        RL_FREE(submission);
        submission = 0;
    }

    if (submissionB64)
    {
        RL_FREE(submissionB64);
        submissionB64 = 0;
    }

    return success;
}

void
rlProfileStatsFlushTask::DoCancel()
{
    rlTask<rlProfileStatsClient>::DoCancel();

    SetRandomDelayUntilNextFlush();

    m_WriteStatsTask.Cancel();
}

void 
rlProfileStatsFlushTask::Start()
{
    rlTask<rlProfileStatsClient>::Start();

    m_WriteStatsTask.Start();
}

void
rlProfileStatsFlushTask::Update(const unsigned timestep)
{
	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
		return;
	}

    if(IsActive())
    {
        rlTask<rlProfileStatsClient>::Update(timestep);

		m_WriteStatsTask.Update(timestep);

        if(!m_MyStatus.Pending())
        {
            m_MyStatus.Succeeded() ? Finish(FINISH_SUCCEEDED) : Finish(FINISH_FAILED);
        }
    }
}

void 
rlProfileStatsFlushTask::Finish(const FinishType finishType, const int resultCode)
{
    if(finishType == FINISH_SUCCEEDED)
    {
        //On a success, use the flush delay the server returned
        m_Ctx->SetMsUntilNextFlush(m_SecsUntilNextWrite * 1000);

        //If the max submission size changed, update it
        if(m_MaxSubmissionBinarySize != rlProfileStats::m_BackendConfig.m_WriteStatsMaxSubmissionBinarySize)
        {
            rlTaskDebug("Config.m_WriteStatsMaxStats changed from %d to %d",
                        rlProfileStats::m_BackendConfig.m_WriteStatsMaxSubmissionBinarySize,
                        m_MaxSubmissionBinarySize);

            rlProfileStats::m_BackendConfig.m_WriteStatsMaxSubmissionBinarySize = m_MaxSubmissionBinarySize;
        }
    }
    else
    {
        //On any failure, pick a random delay until next flush
        SetRandomDelayUntilNextFlush();
    }

    rlTask<rlProfileStatsClient>::Finish(finishType, resultCode);
}

void
rlProfileStatsFlushTask::SetRandomDelayUntilNextFlush()
{
    if (!WasCanceled())
    {
        unsigned delayRangeSecs = rlProfileStatsClient::ms_DefaultFlushDelaySecs;
        PARAM_rlProfileStatsFlushErrorDelaySecs.Get(delayRangeSecs);

        if(delayRangeSecs)
        {
            m_Ctx->SetMsUntilNextFlush(((unsigned)(rand() % delayRangeSecs) * 1000) + 100);
        }
        else
        {
            rlTaskError("Flush error delay set to zero; this disables flushes for this boot");
            m_Ctx->SetMsUntilNextFlush(0);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
// rlProfileStatsReadStatsByGamerTask
//////////////////////////////////////////////////////////////////////////
rlProfileStatsReadStatsByGamerTask::rlProfileStatsReadStatsByGamerTask()
: m_StreamParser(RL_PROFILESTATS_FORMAT_BASE64TYPEVALUE)
{
}

rlProfileStatsReadStatsByGamerTask::~rlProfileStatsReadStatsByGamerTask()
{
}

bool 
rlProfileStatsReadStatsByGamerTask::Configure(const int localGamerIndex,
                                              const rlGamerHandle* gamers,
                                              const unsigned numGamers,
                                              rlProfileStatsReadResults* results)
{
    bool success = false;

    rtry
    {
        rverify(results, catchall, );
        rverify(gamers, catchall, );
        rverify(numGamers <= results->GetMaxRows(), 
                catchall, 
                rlTaskError("Results has fewer max rows (%d) than there are gamerhandles (%d)",
                            results->GetMaxRows(), numGamers));

        rverify(rlRosHttpTask::Configure(localGamerIndex, &m_RosSaxReader), catchall, );

        unsigned i;
        unsigned pos;

        //Build CSV for gamer handles
        const unsigned handlesBufSize = numGamers * (RL_MAX_GAMER_HANDLE_CHARS + 1);
        char* gamerHandles = Alloca(char, handlesBufSize);
        rverify(gamerHandles, 
                catchall, 
                rlTaskError("Could not alloca %d bytes for %d gamerhandles", handlesBufSize, numGamers));

        pos = 0;
        for (i = 0; i < numGamers; ++i)
        {
            if (0 != i)
            {
                while (gamerHandles[pos] && pos + 2 < handlesBufSize) pos++;
                gamerHandles[pos++] = ',';
                gamerHandles[pos] = '\0';
            }
            rverify(gamers[i].ToString(gamerHandles + pos, handlesBufSize - pos), catchall, );
			rverify(gamers[i].IsValidForRos(), catchall, rlTaskError("gamers[%d] is invalid for ROS", i));
        }

        //Build binary for statIds
        const unsigned statIdsBufSize = results->GetNumStatIds() * sizeof(s32);
        u8* statIdsBuf = Alloca(u8, statIdsBufSize);
        rverify(statIdsBuf, catchall, );

        pos = 0;
        for (i = 0; i < results->GetNumStatIds(); ++i)
        {
            s32 id = sysEndian::NtoB(results->GetStatId(i));
            rcheck(pos + sizeof(id) <= statIdsBufSize, catchall, );
            sysMemCpy(statIdsBuf + pos, &id, sizeof(id));
            pos += sizeof(id);
        }
        rlAssert(pos == statIdsBufSize);

        //Get credentials
        const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
        if(!cred.IsValid())
        {
            rlTaskError("Credentials are invalid");
            return false;
        }

        rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
        rverify(AddStringParameter("gamerHandles", gamerHandles, true), catchall, );
        rverify(AddBinaryParameter("statIds", statIdsBuf, statIdsBufSize), catchall, );

        m_Results = results;
        m_Results->SetNumRows(0);
        m_State = STATE_EXPECT_RESULTS;

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool 
rlProfileStatsReadStatsByGamerTask::SaxStartElement(const char* name)
{
    switch((int)m_State)
    {
    case STATE_EXPECT_RESULTS:
        if (!stricmp("results", name))
        {
            m_State = STATE_EXPECT_RECORD;
        }
        else if (!stricmp("InvalidIds", name))
        {
            m_State = STATE_READ_INVALID_IDS;
            m_MissingStatIdsText.Init(g_rlAllocator, datGrowBuffer::NULL_TERMINATE);
        }
        break;
    case STATE_EXPECT_RECORD:
        if (!stricmp("r", name))
        {
            m_State = STATE_READ_GAMERHANDLE;
        }
        break;
    }
    return true;
}

bool 
rlProfileStatsReadStatsByGamerTask::SaxAttribute(const char* name, const char* val)
{
    switch((int)m_State)
    {
    case STATE_READ_GAMERHANDLE:
        if (!stricmp("gh", name))
        {
            rlGamerHandle gamerHandle;
            if (!gamerHandle.FromString(val))
            {
                rlTaskError("Unable to build gamer handle from string: %s", val);
                return false;
            }

#if !__NO_OUTPUT
            rlTaskDebug3("[%d] GamerHandle = %s", m_Results->GetNumRows(), val);
#endif

            if (!m_Results->SetGamerHandle(m_Results->GetNumRows(), gamerHandle))
            {
                return false;
            }

            m_CurStatIndex = 0;
            m_State = STATE_READ_VALUES;
            m_StreamParser.ResetStream();
        }
        break;
    }
    return true;
}

bool 
rlProfileStatsReadStatsByGamerTask::SaxCharacters(const char* ch, const int start, const int length)
{
    switch((int)m_State)
    {
    case STATE_READ_VALUES:
        {
            if (!m_StreamParser.ParseStream(*this, &ch[start], length - start))
            {
                return false;
            }
        }
        break;
    case STATE_READ_INVALID_IDS:
        {
            if (!m_MissingStatIdsText.AppendOrFail(&ch[start], length))
            {
                rlTaskError("Failed to process characters %s (start=%d, length=%d)", ch, start, length);
                return false;
            }
        }
        break;
    }
    return true;
}

bool 
rlProfileStatsReadStatsByGamerTask::SaxEndElement(const char* name)
{
    switch((int)m_State)
    {
    case STATE_READ_VALUES:
        if (!stricmp("r", name))
        {
            //Move onto the next record
            m_Results->SetNumRows(m_Results->GetNumRows() + 1);
            m_State = STATE_EXPECT_RECORD;
        }
        break;
    case STATE_READ_INVALID_IDS:
        {
            //Parse our invalid ids into our array
            const char* statIdsText = (const char* )m_MissingStatIdsText.GetBuffer();

            unsigned len = ustrlen(statIdsText);
            unsigned startIndex = 0;
            int statId;

            for (unsigned k = 0; k < len; k++)
            {
                if (statIdsText[k] == ',' || k == len - 1)
                {
                    if (sscanf(&statIdsText[startIndex], "%d", &statId))
                    {
                        //Add this stat to our list of invalid stats
                        m_MissingStatIds.PushAndGrow(statId);
                        //Advance to the next stat
                        startIndex = k + 1;
                    }
                    else
                    {
                        rlTaskError("Failed to parse stat id: %s", &statIdsText[startIndex]);
                        return false;
                    }
                }
            }

            //Clear our buffer since we no longer need it
            m_MissingStatIdsText.Clear();

            m_State = STATE_EXPECT_RESULTS;
        }
        break;
    
#if !__NO_OUTPUT
    case STATE_EXPECT_RECORD:
        if (!stricmp("results", name))
        {
            //Done reading, so print totals
            rlTaskDebug("Read %d stats for %d gamers", m_Results->GetNumStatIds(), m_Results->GetNumRows());
        }
#endif
    }
    return true;
}

void 
rlProfileStatsReadStatsByGamerTask::ProcessError(const rlRosResult& /*result*/, const parTreeNode* /*node*/, int& /*resultCode*/ )
{
    m_Results->Clear();
}

bool
rlProfileStatsReadStatsByGamerTask::GetNextStatId(int& out_statId)
{
    //Not exactly efficient, but to support missing stat ids we need to advance our
    //current stat index past any that are missing since they won't be in the response
    do
    {
        out_statId = m_Results->GetStatId(m_CurStatIndex);

        //See if we found a valid stat id
        if (m_MissingStatIds.Find(out_statId) == -1)
        {
            return true;
        }

        rlTaskWarning("Stat id %d missing from response, skipping", out_statId);
    }
    while (++m_CurStatIndex < m_Results->GetNumStatIds());

    return false;
}

void
rlProfileStatsReadStatsByGamerTask::ReceiveStat(const int /*statId*/, const rlProfileStatsType /*statType*/, const rlProfileStatsValue& statValue)
{
    //Copy the received stat into our result set
    rlProfileStatsValue* resultVal = const_cast<rlProfileStatsValue*>(m_Results->GetValue(m_Results->GetNumRows(), m_CurStatIndex));
    if (resultVal)
    {
        *resultVal = statValue;

        ++m_CurStatIndex;
    }
    else
    {
        rlAssert(false);
    }
}

void 
rlProfileStatsReadStatsByGamerTask::DoCancel()
{
	rlRosHttpTask::DoCancel();

	if (m_Results)
		m_Results->Clear();

	m_StreamParser.ResetStream();
}

//////////////////////////////////////////////////////////////////////////
// rlProfileStatsSynchronizeTask
//////////////////////////////////////////////////////////////////////////
rlProfileStatsSynchronizeTask::rlProfileStatsSynchronizeTask()
: m_LocalGamerIndex(-1)
, m_State(STATE_EXPECT_VALUES)
, m_StreamParser(RL_PROFILESTATS_FORMAT_BASE64IDTYPEVALUE)
, m_NumReceivedStats(0)
{
}

bool 
rlProfileStatsSynchronizeTask::Configure(const int localGamerIndex)
{
    if(!rlRosHttpTask::Configure(localGamerIndex, &m_RosSaxReader))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Invalid ROS credentials");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_LocalGamerIndex = localGamerIndex;

    return true;
}

bool 
rlProfileStatsSynchronizeTask::SaxStartElement(const char* name)
{
    switch((int)m_State)
    {
    case STATE_EXPECT_VALUES:
        if(0 == stricmp("values", name))
        {
            m_StreamParser.ResetStream();
            m_State = STATE_READ_VALUES;
        }
        break;
    }

    return true;
}

bool 
rlProfileStatsSynchronizeTask::SaxCharacters(const char* ch, const int start, const int length)
{
    switch((int)m_State)
    {
    case STATE_READ_VALUES:
        {
            if (!m_StreamParser.ParseStream(*this, &ch[start], length - start))
            {
                return false;
            }
        }
        break;
    }

    return true;
}

void
rlProfileStatsSynchronizeTask::ReceiveStat(const int statId, const rlProfileStatsType /*statType*/, const rlProfileStatsValue& statValue)
{
	//Count the number of stats Received
	m_NumReceivedStats += 1;

    //Notify caller of the stat value, so they can compare it to the local value
    //or store the value of server-authoritative stats.
    rlProfileStatsSynchronizeStatEvent e(m_LocalGamerIndex, statId, &statValue);
    rlProfileStats::m_EventDelegate.Invoke(&e);
}

void 
rlProfileStatsSynchronizeTask::DoCancel()
{
	rlRosHttpTask::DoCancel();
	m_StreamParser.ResetStream();
}

//////////////////////////////////////////////////////////////////////////
// rlProfileStatsWriteStatsTask
//////////////////////////////////////////////////////////////////////////
rlProfileStatsWriteStatsTask::rlProfileStatsWriteStatsTask()
: m_NumWritten(NULL)  
, m_SecsUntilNextWrite(NULL)
, m_MaxSubmissionBinarySize(NULL)
{
}

bool 
rlProfileStatsWriteStatsTask::Configure(const int localGamerIndex,
                                        const char* submission,
                                        int* numWritten,
                                        int* secsUntilNextWrite,
                                        int* maxSubmissionSize)
{
    rlAssert(numWritten && secsUntilNextWrite && maxSubmissionSize);

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!cred.HasPrivilege(RLROS_PRIVILEGEID_PROFILE_STATS_WRITE))
    {
        rlTaskError("User does not have ProfileStatsWrite privilege");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("submission", submission, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_NumWritten = numWritten;
    m_SecsUntilNextWrite = secsUntilNextWrite;
    m_MaxSubmissionBinarySize = maxSubmissionSize;

    return true;
}

bool 
rlProfileStatsWriteStatsTask::Configure(const int localGamerIndex,
                                        const u8* submissionBuf,
                                        const int submissionSize,
                                        int* numWritten,
                                        int* secsUntilNextWrite,
                                        int* maxSubmissionSize)
{
    rlAssert(numWritten && secsUntilNextWrite && maxSubmissionSize);

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!cred.HasPrivilege(RLROS_PRIVILEGEID_PROFILE_STATS_WRITE))
    {
        rlTaskError("User does not have ProfileStatsWrite privilege");
        return false;
    }

    //Add the "ticket".
    //Send an empty "submission".
    //Start an empty "data" parameter.
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddStringParameter("submission", "", true)
        || !AddStringParameter("data", "", true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    // The 'submissionBuf' buffer will be the 'data' parameter value.
    if (!AppendContent((char*)submissionBuf, submissionSize))
    {
        rlTaskError("Failed to append content");
        return false;
    }

    m_NumWritten = numWritten;
    m_SecsUntilNextWrite = secsUntilNextWrite;
    m_MaxSubmissionBinarySize = maxSubmissionSize;

    return true;
}

bool 
rlProfileStatsWriteStatsTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                             const parTreeNode* node, 
                                             int& /*resultCode*/)
{
    rlAssert(node);

    if(!rlHttpTaskHelper::ReadInt(*m_NumWritten, node, "NumWritten", NULL))
    {
        rlTaskError("Failed to read <NumWritten>");
        return false;
    }

    if(!rlHttpTaskHelper::ReadInt(*m_SecsUntilNextWrite, node, "SecsUntilNextWrite", NULL))
    {
        rlTaskError("Failed to read <SecsUntilNextWrite>");
        return false;
    }

    if(!rlHttpTaskHelper::ReadInt(*m_MaxSubmissionBinarySize, node, "MaxSubmissionBinarySize", NULL))
    {
        rlTaskError("Failed to read <MaxSubmissionBinarySize>");
        return false;
    }

#if !__NO_OUTPUT
    parTreeNode::ChildNodeIterator iter = node->BeginChildren();
    while(iter != node->EndChildren())
    {
        parTreeNode* childNode = *iter;

        if(stricmp(childNode->GetElement().GetName(), "FailedToWrite") != 0)
        {
            ++iter;
            continue;
        }

        rlTaskWarning("FailedToWrite: %s", rlHttpTaskHelper::ReadString(childNode, NULL, NULL));

        ++iter;
    }
#endif

    return true;
}


//////////////////////////////////////////////////////////////////////////
// rlProfileStatsResetByGroupTask
//////////////////////////////////////////////////////////////////////////
rlProfileStatsResetByGroupTask::rlProfileStatsResetByGroupTask()
{
}

bool 
rlProfileStatsResetByGroupTask::Configure(const int localGamerIndex,
                                          const unsigned int groupId)
{
    if(groupId < 1)
    {
        return false;
    }
        
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("Credentials are invalid");
        return false;
    }

    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddIntParameter("groupId", groupId, 0))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_State = STATE_EXPECT_RESULT;

    return true;
}

bool 
rlProfileStatsResetByGroupTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                               const parTreeNode* node, 
                                               int& /*resultCode*/)
{
    rlAssert(node);

    int resetCount = 0;
    if(!rlHttpTaskHelper::ReadInt(resetCount, node, "Count", NULL))
    {
            rlTaskError("Failed to read <Count>");
            return false;
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////
// rlProfileStatsSynchronizeGroupsTask
//////////////////////////////////////////////////////////////////////////

rlProfileStatsSynchronizeGroupsTask::rlProfileStatsSynchronizeGroupsTask()
: rlProfileStatsSynchronizeTask()
{}

bool 
rlProfileStatsSynchronizeGroupsTask::Configure(const int localGamerIndex,
                                               const atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS>* groupsPtr)
{
    bool success = false;

    rtry
    {
        const atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS> &groups = *groupsPtr;

        rverify(rlProfileStatsSynchronizeTask::Configure(localGamerIndex),
                catchall,
                );

        //Build binary for groupIds
        u8 groupIdsBuf[RL_PROFILESTATS_MAX_SYNC_GROUPS * sizeof(int)];

        unsigned pos = 0;
        for (unsigned k = 0; k < (unsigned)groups.GetCount(); k++)
        {
            const int id = sysEndian::NtoB(groups[k]);
            rlAssert(pos + sizeof(id) <= COUNTOF(groupIdsBuf));
            sysMemCpy(&groupIdsBuf[pos], &id, sizeof(id));
            pos += sizeof(id);
        }
        rlAssert(pos <= COUNTOF(groupIdsBuf));

        rverify(AddBinaryParameter("groupIds", groupIdsBuf, pos),
                catchall,
                );

        success = true;
    }
    rcatchall
    {
    }

    return success;
}


bool 
rlProfileStatsSynchronizeGroupsTask::ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode)
{
	bool resultvalue = rlRosHttpTask::ProcessSuccess(result, node, resultCode);

	if(0 == m_NumReceivedStats)
	{
		resultvalue = false;
		resultCode = -1;
	}

	return resultvalue;
}


//////////////////////////////////////////////////////////////////////////
// rlProfileStatsConditionalSynchronizeGroupsTask
//////////////////////////////////////////////////////////////////////////

rlProfileStatsConditionalSynchronizeGroupsTask::rlProfileStatsConditionalSynchronizeGroupsTask()
: m_State(STATE_NONE)
, m_ReadVersionTask(NULL)
, m_SyncGroupsTask(NULL)
{
}

rlProfileStatsConditionalSynchronizeGroupsTask::~rlProfileStatsConditionalSynchronizeGroupsTask()
{
    if (m_ReadVersionTask)
    {
        rlProfileStats::m_TaskMgr.DestroyTask(m_ReadVersionTask);
        m_ReadVersionTask = NULL;
    }

    if (m_SyncGroupsTask)
    {
        rlProfileStats::m_TaskMgr.DestroyTask(m_SyncGroupsTask);
        m_SyncGroupsTask = NULL;
    }
}

bool 
rlProfileStatsConditionalSynchronizeGroupsTask::Configure(rlProfileStatsClient* /*client*/,
                                                          const atFixedArray<rlProfileStatsGroupDefinition, RL_PROFILESTATS_MAX_SYNC_GROUPS>* groups)
{
    rtry
    {
        rverify(groups,
                catchall,);

        m_Groups = *groups;

        //Build the list of group version stats to write
        for (int k = 0; k < m_Groups.GetCount(); k++)
        {
            rlTaskDebug("Conditionally syncing group %d", m_Groups[k].m_GroupId);
            //Don't add duplicate version stats, groups can technically share the same version
            //if they want
            if (m_StatIds.Find(m_Groups[k].m_GroupVersionStatId) == -1)
            {
                m_StatIds.Push(m_Groups[k].m_GroupVersionStatId);
            }

            if (m_StatIds.Find(m_Groups[k].m_AuthGroupVersionStatId) == -1)
            {
                m_StatIds.Push(m_Groups[k].m_AuthGroupVersionStatId);
            }
        }
        
        return true;
    }
    rcatchall
    {
    }

    return false;
}

void 
rlProfileStatsConditionalSynchronizeGroupsTask::Start()
{
    rtry
    {
        rlTask<rlProfileStatsClient>::Start();

        rverify(m_State == STATE_NONE,
                catchall,);
        rverify(m_ReadVersionTask == NULL,
                catchall,);

        //Create our tasks to read the group version stats stored on the backend
        m_ReadVersionTask = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsReadStatsByGamerTask>();

        rverify(m_ReadVersionTask,
                catchall,);
        
        rverify(m_ReadResults.Reset(const_cast<const int*>(m_StatIds.GetElements()),
                                    m_StatIds.GetCount(),
                                    1,
                                    &m_ReadRecord,
                                    sizeof(m_ReadRecord)),
                catchall,);

        //Get the gamer handle for this local gamer to read stats for
        rlGamerHandle gamer;
        rverify(rlPresence::GetGamerHandle(m_Ctx->GetLocalGamerIndex(), &gamer),
                catchall,);

        rverify(rlTaskBase::Configure(m_ReadVersionTask,
                                      m_Ctx->GetLocalGamerIndex(),
                                      &gamer,
                                      1,
                                      &m_ReadResults,
                                      &m_MyStatus),
                catchall,);

        m_ReadVersionTask->Start();

        m_State = STATE_READING_VERSION;
                                             
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void 
rlProfileStatsConditionalSynchronizeGroupsTask::Update(const unsigned timestep)
{
	if(WasCanceled())
	{
		this->Finish(FINISH_CANCELED);
		return;
	}

    rtry
    {
        rlTask<rlProfileStatsClient>::Update(timestep);

        switch(m_State)
        {
        case STATE_NONE:
        case STATE_DONE:
            break;
        case STATE_READING_VERSION:
            {
                rverify(m_ReadVersionTask,
                        catchall,);

                m_ReadVersionTask->Update(timestep);

                if (!m_MyStatus.Pending())
                {
                    rlProfileStats::m_TaskMgr.DestroyTask(m_ReadVersionTask);
                    m_ReadVersionTask = NULL;

                    rcheck(m_MyStatus.Succeeded(),
                           catchall,);

                    m_MyStatus.Reset();

                    // Check if the version we read matches our local version...
                    atFixedBitSet<RL_PROFILESTATS_MAX_SYNC_GROUPS> groupsMismatch(false);

                    for (unsigned k = 0; k < (unsigned)m_Groups.GetCount(); k++)
                    {
                        rtry
                        {
                            // No rows will be returned if this gamer doesn't have stats yet (as opposed to defaults)
                            rcheck(m_ReadResults.GetNumRows() == 1,
                                    failversioncheck,);

                            const int statColumn = m_ReadResults.GetStatColumn(m_Groups[k].m_GroupVersionStatId);
                            rverify(statColumn != -1,
                                    failversioncheck,);

                            const int authStatColumn = m_ReadResults.GetStatColumn(m_Groups[k].m_AuthGroupVersionStatId);
                            rverify(authStatColumn != -1,
                                    failversioncheck,);

                            const rlProfileStatsValue* statValue = m_ReadResults.GetValue(0/*row*/, statColumn);
                            rverify(statValue != NULL && statValue->IsValid(),
                                    failversioncheck,);

                            const rlProfileStatsValue* authStatValue = m_ReadResults.GetValue(0/*row*/, authStatColumn);
                            rverify(authStatValue != NULL && authStatValue->IsValid(),
                                    failversioncheck,);

                            // And lastly, see if our version/auth versions match expected
                            if (m_Groups[k].m_LocalGroupVersion.IsValid() 
                                && m_Groups[k].m_LocalAuthGroupVersion.IsValid() 
                                && m_Groups[k].m_LocalGroupVersion == *statValue 
                                && m_Groups[k].m_LocalAuthGroupVersion == *authStatValue)
                            {
                                OUTPUT_ONLY(char valBuf[32];)
                                OUTPUT_ONLY(char authValBuf[32];)
                                rlTaskDebug("Group %d: Local version (Profile: %s, Auth: %s) matches remote version", m_Groups[k].m_GroupId, statValue->ToString(valBuf), authStatValue->ToString(authValBuf));

                                // Versions match, tell client code to consider the entire group synched
                                rlProfileStatsSynchronizeGroupEvent synchGroupEvent(m_Ctx->GetLocalGamerIndex(), m_Groups[k].m_GroupId);
                                rlProfileStats::m_EventDelegate.Invoke(&synchGroupEvent);
                            }
                            else
                            {
                                OUTPUT_ONLY(char localValBuf[32];)
                                OUTPUT_ONLY(char remoteValBuf[32];)
                                OUTPUT_ONLY(char localAuthValBuf[32];)
                                OUTPUT_ONLY(char remoteAuthValBuf[32];)
                                rlTaskDebug("Group %d: Local version (Profile: %s, Auth: %s) mismatches remote version (Profile %s, Auth: %s)",
                                            m_Groups[k].m_GroupId,
                                            m_Groups[k].m_LocalGroupVersion.IsValid() ? m_Groups[k].m_LocalGroupVersion.ToString(localValBuf) : "?",
                                            m_Groups[k].m_LocalAuthGroupVersion.IsValid() ? m_Groups[k].m_LocalAuthGroupVersion.ToString(localAuthValBuf) : "?",
                                            statValue->ToString(remoteValBuf),
                                            authStatValue->ToString(remoteAuthValBuf));

                                groupsMismatch.Set(k, true);
                            }
                        }
                        rcatch(failversioncheck)
                        {
                            groupsMismatch.Set(k, true);
                        }
                    }

                    // Read any groups that don't match our local version
                    if (groupsMismatch.AreAnySet())
                    {
                        m_SyncGroupsTask = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsSynchronizeGroupsTask>();

                        rverify(m_SyncGroupsTask,
                                catchall,);

                        atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS> syncGroups;
                        for (unsigned k = 0; k < (unsigned)m_Groups.GetCount(); k++)
                        {
                            if (groupsMismatch.IsSet(k))
                            {
                                rlTaskDebug("Syncing group %d", m_Groups[k].m_GroupId);
                                syncGroups.Push(m_Groups[k].m_GroupId);
                            }
                        }

                        rverify(rlTaskBase::Configure(m_SyncGroupsTask,
                                                      m_Ctx->GetLocalGamerIndex(),
                                                      &syncGroups,
                                                      &m_MyStatus),
                                catchall,);

                        m_SyncGroupsTask->Start();

                        m_State = STATE_SYNCING_GROUPS;
                    }
                    else
                    {
                        rlTaskDebug("No groups need to be synced");
                        this->Finish(FINISH_SUCCEEDED);
                        m_State = STATE_DONE;
                    }
                }
            }
            break;
        case STATE_SYNCING_GROUPS:
            {
                rverify(m_SyncGroupsTask,
                        catchall,);

                m_SyncGroupsTask->Update(timestep);

                if (!m_MyStatus.Pending())
                {
                    rlProfileStats::m_TaskMgr.DestroyTask(m_SyncGroupsTask);
                    m_SyncGroupsTask = NULL;

                    rcheck(m_MyStatus.Succeeded(),
                           catchall,);

                    m_MyStatus.Reset();

                    this->Finish(FINISH_SUCCEEDED);
                    m_State = STATE_DONE;
                }
            }
            break;
        }
    }
    rcatchall
    {
        if (m_MyStatus.Failed())
        {
            this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        else
        {
            this->Finish(FINISH_FAILED);
        }

        m_State = STATE_DONE;
    }
}

void
rlProfileStatsConditionalSynchronizeGroupsTask::DoCancel()
{
	rlTaskDebug("Cancel - state='%d'.", m_State);

    rlTask<rlProfileStatsClient>::DoCancel();

    switch(m_State)
    {
    case STATE_NONE:
    case STATE_DONE:
        break;
    case STATE_READING_VERSION:
        if (m_ReadVersionTask)
        {
			rlTaskDebug("m_ReadVersionTask->Cancel().");
            m_ReadVersionTask->Cancel();
            m_ReadVersionTask->Update(0); // Needed to set FINISH_CANCELED just like for the parent task
        }
        break;
    case STATE_SYNCING_GROUPS:
        if (m_SyncGroupsTask)
        {
			rlTaskDebug("m_SyncGroupsTask->Cancel().");
			m_SyncGroupsTask->Cancel();
            m_SyncGroupsTask->Update(0); // Needed to set FINISH_CANCELED just like for the parent task
        }
        break;
    }

    m_State = STATE_DONE;
}

}   //namespace rage
