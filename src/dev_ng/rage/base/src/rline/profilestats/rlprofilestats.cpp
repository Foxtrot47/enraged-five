// 
// rline/rlprofilestats.cpp
// 
// Copyright (C) 2010-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "rlprofilestats.h"
#include "rlprofilestatsclient.h"
#include "rlprofilestatstasks.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "system/timer.h"

namespace rage
{

//Override output macros.
#undef __rage_channel
#define __rage_channel rline_profilestats

extern sysMemAllocator* g_rlAllocator;

rlProfileStats::EventDelegate rlProfileStats::m_EventDelegate;
rlProfileStats::BackendConfig rlProfileStats::m_BackendConfig;
rlTaskManager rlProfileStats::m_TaskMgr;   
bool rlProfileStats::m_Initialized = false;

static netTimeStep s_RlProfileStatsTimestep;

static rlProfileStatsClient m_RlProfileStatsClients[RL_MAX_LOCAL_GAMERS];

int rlProfileStats::MAX_NUM_GAMERS_FOR_STATS_READ = -1;

//------------------------------------------------------------------------------
// rlProfileStats
//------------------------------------------------------------------------------
bool 
rlProfileStats::Init(const EventDelegate& eventDlgt, 
                     const unsigned maxSubmissionSize,
                     const bool submitCompressed)
{
    rlAssert(!m_Initialized);   
    rlAssert(eventDlgt.IsBound());
    rlAssert(maxSubmissionSize);

    rtry
    {
        rverify(m_TaskMgr.Init(g_rlAllocator), catchall, );

        m_EventDelegate = eventDlgt;

        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            rverify(m_RlProfileStatsClients[i].Init(i, maxSubmissionSize, submitCompressed), catchall, );
        }

        m_BackendConfig.m_WriteStatsMaxSubmissionBinarySize = maxSubmissionSize;

        m_Initialized = true;
    }
    rcatchall
    {
        Shutdown();
    }

    return m_Initialized;
}

bool 
rlProfileStats::IsInitialized()
{
    return m_Initialized;
}

void 
rlProfileStats::Shutdown()
{
	rlDebug1("rlProfileStats::Shutdown()");

	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        m_RlProfileStatsClients[i].Shutdown();
    }

	rlDebug1("Task manager Shutdown");

	m_TaskMgr.Shutdown();
    
	rlDebug1("Task manager Shutdown - done");
	
	m_EventDelegate.Clear();
 
    m_Initialized = false;
}

void 
rlProfileStats::Update()
{
    s_RlProfileStatsTimestep.SetTime(sysTimer::GetSystemMsTime());
    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        m_RlProfileStatsClients[i].Update(s_RlProfileStatsTimestep.GetTimeStep());
    }

    m_TaskMgr.Update();
}

void
rlProfileStats::Cancel(netStatus* status)
{
    m_TaskMgr.CancelTask(status);
}

bool
rlProfileStats::Flush(rlProfileStatsDirtyIterator &flushIt, 
                      const int localGamerIndex,
                      netStatus* status,
                      const unsigned flags/* = RL_PROFILESTATS_FLUSH_NONE*/)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_RlProfileStatsClients[localGamerIndex].Flush(flushIt, status, flags);
}

rlProfileStatsFlushStatus
rlProfileStats::GetFlushStatus(const int localGamerIndex)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_RlProfileStatsClients[localGamerIndex].GetFlushStatus();
}

bool 
rlProfileStats::ReadStatsByGamer(const int localGamerIndex, 
                                 const rlGamerHandle* gamers,
                                 const unsigned numGamers,
                                 rlProfileStatsReadResults* results, 
                                 netStatus* status)
{
    rlDebug1("ReadStatsByGamer :: Reading stats for %u gamers. Local index: %u", numGamers, localGamerIndex);
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));

	const int numToRead = MAX_NUM_GAMERS_FOR_STATS_READ == -1 ? numGamers : rage::Min(MAX_NUM_GAMERS_FOR_STATS_READ, static_cast<int>(numGamers));
    return m_RlProfileStatsClients[localGamerIndex].ReadStatsByGamer(gamers, numToRead, results, status);
}

void rlProfileStats::SetMaxNumGamersForStatsRead(const unsigned numGamers)
{
	MAX_NUM_GAMERS_FOR_STATS_READ = numGamers;
}

void 
rlProfileStats::SetMsUntilNextFlush(const int localGamerIndex, const unsigned ms)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_RlProfileStatsClients[localGamerIndex].SetMsUntilNextFlush(ms);
}

bool 
rlProfileStats::Synchronize(const int localGamerIndex, netStatus* status)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_RlProfileStatsClients[localGamerIndex].Synchronize(status);
}

/*static*/
bool 
rlProfileStats::SynchronizeGroups(const int localGamerIndex, 
                                  const atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                                  netStatus* status)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_RlProfileStatsClients[localGamerIndex].SynchronizeGroups(groups, status);
}

/*static*/
bool 
rlProfileStats::ConditionalSynchronizeGroups(const int localGamerIndex, 
                                             const atFixedArray<rlProfileStatsGroupDefinition, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                                             netStatus* status)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_RlProfileStatsClients[localGamerIndex].ConditionalSynchronizeGroups(groups, status);
}

bool 
rlProfileStats::ResetStatsByGroup(const int localGamerIndex, 
                                  const unsigned groupId, 
                                  netStatus* status)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    return m_RlProfileStatsClients[localGamerIndex].ResetStatsByGroup(groupId, status);
}

};
