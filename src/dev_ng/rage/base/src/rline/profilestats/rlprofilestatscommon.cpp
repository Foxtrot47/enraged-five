// 
// rline/rlprofilestatscommon.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlprofilestatscommon.h"
#include "rlprofilestats.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "string/string.h"
#include "system/memops.h"
#include "system/endian.h"

#include <algorithm>

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, profilestats);
#undef __rage_channel
#define __rage_channel rline_profilestats

AUTOID_IMPL(rlProfileStatsEvent);
AUTOID_IMPL(rlProfileStatsGetLocalValueEvent);
AUTOID_IMPL(rlProfileStatsSynchronizeStatEvent);
AUTOID_IMPL(rlProfileStatsSynchronizeGroupEvent);
AUTOID_IMPL(rlProfileStatsFlushStatEvent);

//------------------------------------------------------------------------------
// rlProfileStatsValue
//------------------------------------------------------------------------------
rlProfileStatsValue::rlProfileStatsValue()
{
    Clear();
}

bool 
rlProfileStatsValue::operator==(const rlProfileStatsValue& that) const
{
    if(m_Type == RL_PROFILESTATS_TYPE_INVALID) return false;
    if(m_Type != that.m_Type) return false;

    switch(m_Type)
    {
    case RL_PROFILESTATS_TYPE_FLOAT: return m_Union.m_Float == that.m_Union.m_Float;
    case RL_PROFILESTATS_TYPE_INT32: return m_Union.m_Int32 == that.m_Union.m_Int32;
    case RL_PROFILESTATS_TYPE_INT64: return m_Union.m_Int64 == that.m_Union.m_Int64;
    default: return false;
    }
}

bool 
rlProfileStatsValue::operator!=(const rlProfileStatsValue& that) const
{
    return !(*this == that);
}

float 
rlProfileStatsValue::GetFloat() const
{
    rlAssert(m_Type == RL_PROFILESTATS_TYPE_FLOAT);
    return m_Union.m_Float;
}

s32 
rlProfileStatsValue::GetInt32() const
{
    rlAssert(m_Type == RL_PROFILESTATS_TYPE_INT32);
    return m_Union.m_Int32;
}

s64 
rlProfileStatsValue::GetInt64() const
{
    rlAssert(m_Type == RL_PROFILESTATS_TYPE_INT64);
    return m_Union.m_Int64;
}

void 
rlProfileStatsValue::SetFloat(const float val)
{
    m_Union.m_Float = val;
    m_Type = RL_PROFILESTATS_TYPE_FLOAT;
}

void 
rlProfileStatsValue::SetInt32(const s32 val)
{
    m_Union.m_Int32 = val;
    m_Type = RL_PROFILESTATS_TYPE_INT32;
}

void 
rlProfileStatsValue::SetInt64(const s64 val)
{
    m_Union.m_Int64 = val;
    m_Type = RL_PROFILESTATS_TYPE_INT64;
}

const char* 
rlProfileStatsValue::ToString(char* buf, const unsigned bufSize) const
{
    rlAssert(buf && bufSize);

    buf[0] = '\0';

    switch(m_Type)
    {
    case RL_PROFILESTATS_TYPE_INVALID:      safecpy(buf, "INVALID", bufSize); break;
    case RL_PROFILESTATS_TYPE_FLOAT:        formatf(buf, bufSize, "%f", GetFloat()); break;
    case RL_PROFILESTATS_TYPE_INT32:        formatf(buf, bufSize, "%d", GetInt32()); break;
    case RL_PROFILESTATS_TYPE_INT64:        formatf(buf, bufSize, "0x%" I64FMT "x", GetInt64()); break;
    default:                                rlError("Unhandled or invalid value type (%d)", m_Type);
    }

    return buf;
}

bool 
rlProfileStatsValue::GetSerializedSize(const rlProfileStatsType type,
                                       unsigned* size, 
                                       const bool includeType)
{
    unsigned total = 0;

    switch(type)
    {
    case RL_PROFILESTATS_TYPE_FLOAT: total = sizeof(float); break;
    case RL_PROFILESTATS_TYPE_INT32: total = sizeof(s32); break;
    case RL_PROFILESTATS_TYPE_INT64: total = sizeof(s64); break;
    default: rlError("Invalid type %d", type); return false;
    }

    if (includeType) ++total;

    *size = total;

    return true;
}

bool 
rlProfileStatsValue::Deserialize(const u8* buf, 
                                 const unsigned bufSize,
                                 const unsigned offset,
                                 unsigned* numBytes)
{
    //Read the type so we can read the value
    if (offset >= bufSize) return false;

    unsigned cur = offset;
    rlProfileStatsType type = (rlProfileStatsType)buf[cur++];

    //Read the value
    if(!Deserialize(buf, bufSize, cur, type, numBytes)) return false;

    cur += *numBytes;

    (*numBytes) = cur - offset;

    return true;
}

bool 
rlProfileStatsValue::Deserialize(const u8* buf, 
                                 const unsigned bufSize,
                                 const unsigned offset, 
                                 const rlProfileStatsType type,
                                 unsigned* numBytes)
{
    //Make sure there is enough space in the buffer for the value
    unsigned bytesRequired;
    if (!GetSerializedSize(type, &bytesRequired, false)) return false;
    if ((bytesRequired + offset) > bufSize) return false;

    //Read value by type
    unsigned cur = offset;

    switch(type)
    {
    case RL_PROFILESTATS_TYPE_FLOAT:
        {
            float val;
            sysMemCpy(&val, buf + cur, sizeof(val));
			sysEndian::BtoNMe(val);
            cur += sizeof(val);

            SetFloat(val);
        }
        break;
    case RL_PROFILESTATS_TYPE_INT32:
        {
            s32 val;
            sysMemCpy(&val, buf + cur, sizeof(val));
            cur += sizeof(val);

            SetInt32(sysEndian::BtoN(val));
        }
        break;
    case RL_PROFILESTATS_TYPE_INT64:
        {
            s64 val;
            sysMemCpy(&val, buf + cur, sizeof(val));
            cur += sizeof(val);

            SetInt64(sysEndian::BtoN(val));
        }
        break;
    default:
        rlError("Invalid type %d", m_Type);
        return false;
    }

    *numBytes = cur - offset;

    return true;
}

bool 
rlProfileStatsValue::Serialize(u8* buf, 
                               const unsigned bufSize,
                               const unsigned offset, 
                               unsigned* numBytes, 
                               const bool includeType) const
{
    //Make sure there is enough space in the buffer.
    unsigned bytesRequired;
    if (!GetSerializedSize(GetType(), &bytesRequired, includeType)) return false;
    if ((bytesRequired + offset) > bufSize) return false;

    //Serialize the (optional) type and value.
    unsigned cur = offset;

    if (includeType) buf[cur++] = (u8)m_Type;

    switch(m_Type)
    {
    case RL_PROFILESTATS_TYPE_FLOAT:
        {
            float val = GetFloat();
			sysEndian::NtoBMe(val);
            sysMemCpy(buf + cur, &val, sizeof(val));
            cur += sizeof(val);
        }
        break;
    case RL_PROFILESTATS_TYPE_INT32:
        {
            s32 val = sysEndian::NtoB(GetInt32());
            sysMemCpy(buf + cur, &val, sizeof(val));
            cur += sizeof(val);
        }
        break;
    case RL_PROFILESTATS_TYPE_INT64:
        {
            s64 val = sysEndian::NtoB(GetInt64());
            sysMemCpy(buf + cur, &val, sizeof(val));
            cur += sizeof(val);
        }
        break;
    default:
        rlError("Invalid type %d", m_Type);
        return false;
    }

    *numBytes = cur - offset;

    rlAssert(*numBytes == bytesRequired);

    return true;
}

//------------------------------------------------------------------------------
// rlProfileStatsRecordBase
//------------------------------------------------------------------------------

const rage::rlProfileStatsValue& rlProfileStatsRecordBase::GetValue(const unsigned column) const
{
	if (rlVerify(column >= 0 && column < GetNumValues()))
	{
		return GetValues()[column];
	}
	static rlProfileStatsValue s_invalidValue;
	return s_invalidValue;
}

//------------------------------------------------------------------------------
// rlProfileStatsReadResults
//------------------------------------------------------------------------------
rlProfileStatsReadResults::rlProfileStatsReadResults()
{
    Clear();
}

void 
rlProfileStatsReadResults::Clear()
{
    m_StatIds = 0;
    m_NumStatIds = 0;
    m_MaxRows = 0;
    m_NumRows = 0;
}

bool 
rlProfileStatsReadResults::Reset(const int* statIds,
                                 const unsigned numStatIds,
                                 const unsigned maxRows,
                                 rlProfileStatsRecordBase* records,
                                 const unsigned recordSpan)
{
    if (rlVerifyf(statIds
                  && numStatIds
                  && maxRows
                  && records
                  && (recordSpan >= sizeof(rlProfileStatsRecordBase)),
                  "Invalid arguments - statIds=%p, numStatIds=%d, maxRows=%d, records=%p, recordSpan=%d, sizeof(rlProfileStatsRecordBase)=%u",
				  statIds, numStatIds, maxRows, records, recordSpan, (unsigned)sizeof(rlProfileStatsRecordBase)))
    {
        m_StatIds = statIds;
        m_NumStatIds = numStatIds;
        m_MaxRows = maxRows;
        m_Records.Init(records, recordSpan, maxRows);
    
        return true;
    }

    return false;
}

int 
rlProfileStatsReadResults::GetStatColumn(const int statId) const 
{ 
    for(unsigned i = 0; i < m_NumStatIds; i++) 
    {
        if (m_StatIds[i] == statId) return i;
    }
    return -1;
}

const rlGamerHandle* 
rlProfileStatsReadResults::GetGamerHandle(const unsigned row) const
{
    if (rlVerifyf(row < m_MaxRows, "Row %d out of range (max=%d)", row, m_MaxRows))
    {
        return &m_Records[row].GetGamerHandle();
    }
    return 0;
}

const int rlProfileStatsReadResults::FindGamerHandle(const rlGamerHandle& gamerHandle)
{
    for (unsigned i = 0; i < m_Records.GetCount(); ++i)
    {
        if (gamerHandle == m_Records[i].GetGamerHandle())
        {
            return i;
        }
    }

    return -1;
}

bool 
rlProfileStatsReadResults::SetGamerHandle(const unsigned row, const rlGamerHandle& gamerHandle)
{ 
    if (rlVerifyf(row < m_MaxRows, "Row %d out of range (max=%d)", row, m_MaxRows)) 
    {
        m_Records[row].m_GamerHandle = gamerHandle;
        return true;
    }
    return false;
}

const rlProfileStatsValue* 
rlProfileStatsReadResults::GetValue(const unsigned row, const unsigned column) const 
{ 
    if (rlVerifyf(row < m_MaxRows, "Row %d out of range (max=%d)", row, m_MaxRows)
        && rlVerifyf(column < m_NumStatIds, "Column %d out of range (max=%d)", column, m_NumStatIds)) 
    {
        return &m_Records[row].GetValue(column);
    }
    return 0;
}

bool 
rlProfileStatsReadResults::SetNumRows(const unsigned num)
{
    if (rlVerifyf(num <= m_MaxRows, "Num %d out of range (max=%d)", num, m_MaxRows)) 
    {
        m_NumRows = num;
    }
    return (num == m_NumRows);
}

//------------------------------------------------------------------------------
// rlProfileStatsWriter
//------------------------------------------------------------------------------

/*static*/ 
bool 
rlProfileStatsWriter::SerializeByPriority(rlProfileStatsDirtyIterator &iter,
                                          const int localGamerIndex,
                                          u8 *buf,
                                          const unsigned bufSize,
                                          const unsigned offset, 
                                          unsigned* bytesWritten,
                                          unsigned* statsWritten)
{
    bool success = false;
    rtry
    {
        *bytesWritten = 0;
        *statsWritten = 0;

        int statId;
        rlProfileStatsType statType;
        unsigned statSize;

        unsigned currOffset = offset;
        // While we have room in our buffer and there are remaining dirty stats
        while (currOffset < bufSize && 
               !iter.AtEnd())
        {
            rlProfileDirtyStat dirtyStat;
            // Grab the next dirty stat id and advance
            rverify(iter.GetCurrentStat(dirtyStat),
                    catchall,
                    rlError("Failed to get the next dirty stat; should be impossible"));
            iter.Next();

            //Get the stat's current value from the title
            statId = dirtyStat.GetStatId();
            rlProfileStatsValue val = dirtyStat.GetValue();

            if(!val.IsValid())
            {
                rlDebug("Failed to get value for statid=%d, skipping", statId);
                continue;
            }

            // Check to see if we will have enough space for this stat.
            statType = val.GetType();
            rverify(rlProfileStatsValue::GetSerializedSize(statType, &statSize),
                    catchall,
                    rlError("Failed to Get the size of stat %d type %d; should be impossible", statId, statType));
            statSize += sizeof(statId) + sizeof(statType);
            if (currOffset + statSize > bufSize)
            {
                // The next value is going to cause us to go over the limit so stop.
                break;
            }

            // Remember the offset before we serialize data.
            unsigned prevOffset = currOffset;

            // serialize the statId.
            s32 id = sysEndian::NtoB(statId);
            sysMemCpy(buf + currOffset, &id, sizeof(id));
            currOffset += sizeof(id);

            // serialize the type and value.
            unsigned numBytes;
            rverify(val.Serialize(buf, bufSize, currOffset, &numBytes, true),
                catchall,
                rlError("Failed to serialize stat id %d; should be impossible", statId));
            currOffset += numBytes;

            // Count the stat and the bytes as written.
            *bytesWritten += currOffset - prevOffset;
            (*statsWritten)++;

            // Inform the iterator that this stat was flushed
            iter.m_FlushedCount++;

            // Inform the title that this stat was flushed
            rlProfileStatsFlushStatEvent flushEvt(localGamerIndex, statId);
            rlProfileStats::m_EventDelegate.Invoke(&flushEvt);

        }
        success = true;
    }
    rcatchall
    {
        success = false;
    }

    return success;
}

/*static*/ 
bool 
rlProfileStatsWriter::SerializeByType(rlProfileStatsDirtyIterator &iter,
                                      const int localGamerIndex,
                                      u8 *submissionBuf,
                                      u8* tmpIntBuf,
                                      u8* tmpFloatBuf,
                                      const unsigned bufSize,
                                      const unsigned offset, 
                                      unsigned* bytesWritten,
                                      unsigned* statsWritten)
{
    bool success = false;
    rtry
    {
        *bytesWritten = 0;
        *statsWritten = 0;

        int statId;
        rlProfileStatsType statType;
        unsigned statSize;
        // Remember the current offset to be used to store the type count header.
        unsigned typeHeaderOffset = offset;
        // Define the size of the type count header.
        const int typeHeaderSize = sizeof(s16) * 3;
        // Initialize the counts which will be stored in the type count header.
        short s64Count = 0;
        short s32Count = 0;
        short floatCount = 0;

        // Some variables used during serializeation.
        unsigned prevOffset;
        u8* currBuffer;
        unsigned* pCurrOffset;

        // Byte locations within the buffers.
        unsigned bigintBytes = offset + typeHeaderSize;
        unsigned intBytes = 0;
        unsigned floatBytes = 0;
        while (bigintBytes + intBytes + floatBytes < bufSize &&
               !iter.AtEnd())
        {
            rlProfileDirtyStat dirtyStat;
            rverify(iter.GetCurrentStat(dirtyStat),
                    catchall,
                    rlError("Failed to get the next stat; should be impossible"));
            iter.Next();

            //Get the stat's current value from the title
            statId = dirtyStat.GetStatId();
            rlProfileStatsValue val = dirtyStat.GetValue();

            if(!val.IsValid())
            {
                rlDebug("Failed to get value for statid=%d, skipping", statId);
                continue;
            }
            
            // Check to see if we will have enough space for this stat.
            statType = val.GetType();
            rverify(rlProfileStatsValue::GetSerializedSize(statType, &statSize),
                catchall,
                rlError("Failed to Get the size of stat %d type %d; should be impossible", statId, statType));
            statSize += sizeof(statId);
            // The bytes needed store the serialized data is defined by
            //   the sum of all bytes perviously used for all types
            //   and the bytes needed for the current stat.
            if (bigintBytes + floatBytes + intBytes + statSize > bufSize)
            {
                // The next value is going to cause us to go over the limit so stop.
                break;
            }

            // Count the stat within its type group.
            // Get the buffer to store the values within.
            // Get a pointer to the offset which will be used.
            switch (statType)
            {
            case RL_PROFILESTATS_TYPE_INT64:
                s64Count++;
                pCurrOffset = &bigintBytes;
                // Using the 'buf' as the bigint buffer since 
                // bigints are the first set of values to be serialized.
                currBuffer = submissionBuf;
                break;
            case RL_PROFILESTATS_TYPE_INT32:
                s32Count++;
                pCurrOffset = &intBytes;
                // Using 'tmpBuf' as the int buffer since
                // they will be at a yet to be determined position within the final buffer.
                currBuffer = tmpIntBuf;
                break;
            case RL_PROFILESTATS_TYPE_FLOAT:
                floatCount++;
                pCurrOffset = &floatBytes;
                // Using 'tmp2Buf' as the float buffer since
                // they will be at a yet to be determined position within the final buffer.
                currBuffer = tmpFloatBuf;
                break;
            default:
                rthrow(catchall,
                    rlError("Unexpected stat type; should be impossible"));
            }

            // Remember the position within the current buffer prior to serialization
            prevOffset = *pCurrOffset;

            // Serialize the ID into the desired buffer.
            s32 id = sysEndian::NtoB(statId);
            sysMemCpy(currBuffer + *pCurrOffset, &id, sizeof(id));
            *pCurrOffset += sizeof(id);

            // Serialize the value into the desired buffer.
            // NOTE: this will not contain the type.
            unsigned numBytes;
            rverify(val.Serialize(currBuffer, bufSize, *pCurrOffset, &numBytes, false),
                catchall,
                rlError("Failed to serialize stat id %d; should be impossible", statId));
            *pCurrOffset += numBytes;

            // Remember the number of bytes written to the associated buffer.
            *bytesWritten += *pCurrOffset - prevOffset;
            // Count this stat.
            (*statsWritten) ++;

            // Inform the iterator that this stat was flushed
            iter.m_FlushedCount++;

            // Inform the title that this stat was flushed
            rlProfileStatsFlushStatEvent flushEvt(localGamerIndex, statId);
            rlProfileStats::m_EventDelegate.Invoke(&flushEvt);
        }

        // Now copy the int values after the bigint values.
        if (intBytes > 0)
        {
            sysMemCpy(submissionBuf + bigintBytes, tmpIntBuf, intBytes);
        }
        // Now copy the float values after the int values.
        if (floatBytes > 0)
        {
            sysMemCpy(submissionBuf + bigintBytes + intBytes, tmpFloatBuf, floatBytes);
        }

        // Now set the typeCounts at the front of the buffer.
        unsigned currOffset = typeHeaderOffset;
        prevOffset = currOffset;
        s16 count;
        count = sysEndian::NtoB(s64Count);
        sysMemCpy(submissionBuf + currOffset, &count, sizeof(count));
        currOffset += sizeof(count);
        count = sysEndian::NtoB(s32Count);
        sysMemCpy(submissionBuf + currOffset, &count, sizeof(count));
        currOffset += sizeof(count);
        count = sysEndian::NtoB(floatCount);
        sysMemCpy(submissionBuf + currOffset, &count, sizeof(count));
        currOffset += sizeof(count);

        // Remeber the bytes written to the typeHeader.
        *bytesWritten += currOffset - prevOffset;

        success = true;
    }
    rcatchall
    {
        success = false;
    }

    return success;
}


};
