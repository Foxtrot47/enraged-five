// 
// rline/rlprofilestatsclient.cpp
// 
// Copyright (C) 2010-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "rlprofilestatsclient.h"
#include "rlprofilestatstasks.h"
#include "rline/ros/rlros.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "math/amath.h"

namespace rage
{

//Override output macros.
#undef __rage_channel
#define __rage_channel rline_profilestats

//------------------------------------------------------------------------------
// rlProfileStatsClient
//------------------------------------------------------------------------------
rlProfileStatsClient::rlProfileStatsClient()
: m_LocalGamerIndex(-1)
, m_MaxSubmissionSize(0)
, m_FlushEnabled(true)
{
}

rlProfileStatsClient::~rlProfileStatsClient()
{
    Shutdown();
}

bool
rlProfileStatsClient::Init(const int localGamerIndex, 
                           const unsigned maxSubmissionSize,
                           const bool submitCompressed)
{
    m_LocalGamerIndex = localGamerIndex;
    m_MaxSubmissionSize = maxSubmissionSize;
    m_FlushEnabled = true;
    m_SubmitCompressed = submitCompressed;

    return true;
}

void
rlProfileStatsClient::Shutdown()
{
    
}

void 
rlProfileStatsClient::Update(const unsigned timeStep)
{
    //Countdown the time until the gamer can flush again
    if(m_MsUntilNextFlush > timeStep)
    {
        m_MsUntilNextFlush -= timeStep;
    }
    else
    {
        m_MsUntilNextFlush = 0;
    }
}

bool
rlProfileStatsClient::Flush(rlProfileStatsDirtyIterator &flushIt, 
                            netStatus* status,
                            const unsigned flags/* = RL_PROFILESTATS_FLUSH_NONE*/)
{
    switch(GetFlushStatus())
    {
    case RL_PROFILESTATS_FLUSH_INVALID_CREDENTIALS:
        rlWarning("rlProfileStatsClient[%d]::Flush: Gamer does not have valid ROS credentials yet", m_LocalGamerIndex);
        return false;

    case RL_PROFILESTATS_FLUSH_DISABLED: 
        rlWarning("rlProfileStatsClient[%d]::Flush: Disabled by backend", m_LocalGamerIndex);
        return false;

    case RL_PROFILESTATS_FLUSH_TOO_SOON_SINCE_LAST:
        if (!(flags & RL_PROFILESTATS_FLUSH_FORCE))
        {
            rlWarning("rlProfileStatsClient[%d]::Flush: %0.2f seconds until allowed", m_LocalGamerIndex, m_MsUntilNextFlush/1000.0f);
            return false;
        }
        else
        {
            // Ignore backend throttling flush if it's being forced
            rlDebug3("rlProfileStatsClient[%d]::Flush: Forcing flush during RL_PROFILESTATS_FLUSH_TOO_SOON_SINCE_LAST", m_LocalGamerIndex);
            break;
        }
    
    case RL_PROFILESTATS_FLUSH_READY:
        break;
    }

    // If we're ready to flush, verify that we were actually given some stats to flush
    if (flushIt.AtEnd())
    {
        rlWarning("rlProfileStatsClient[%d]::Flush: No stats are dirty", m_LocalGamerIndex);
        return false;
    }

    rlProfileStatsFlushTask* task = NULL;

    rtry
    {
        // Before creating the task, set our delay until we can flush again.
        // This fixes two cases:
        // 1. It prevents callers from immediately flushing again if we fail to allocate the task for a serious reason, such as running out of memory
        // 2. It prevents callers from flushing again before the previous flush has finished. The delay until the next flush is allowed is only set
        //    once the flush task completes. So we have to have some way of delaying flushing while a flush is still outstanding, although we could
        //    just use a netStatus object to do that if we wanted...
        rlProfileStats::SetMsUntilNextFlush(m_LocalGamerIndex, rlProfileStatsClient::ms_DefaultFlushDelaySecs * 1000);
        task = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsFlushTask>();
        rcheck(task, catchall, rlError("rlProfileStatsClient[%d]::Flush: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, this, &flushIt, status), catchall, rlError("rlProfileStatsClient[%d]::Flush: Failed to configure task", m_LocalGamerIndex));
        rcheck(rlProfileStats::m_TaskMgr.AddParallelTask(task), catchall, rlError("rlProfileStatsClient[%d]::Flush: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        if(task) rlProfileStats::m_TaskMgr.DestroyTask(task);
        return false;
    }
}

rlProfileStatsFlushStatus
rlProfileStatsClient::GetFlushStatus() const
{
    if(!rlRos::IsOnline(m_LocalGamerIndex)) return RL_PROFILESTATS_FLUSH_INVALID_CREDENTIALS;
    if(!m_FlushEnabled) return RL_PROFILESTATS_FLUSH_DISABLED;
    if(m_MsUntilNextFlush) return RL_PROFILESTATS_FLUSH_TOO_SOON_SINCE_LAST;

    return RL_PROFILESTATS_FLUSH_READY;
}

int
rlProfileStatsClient::GetLocalGamerIndex() const
{
    return m_LocalGamerIndex;
}

bool 
rlProfileStatsClient::ReadStatsByGamer(const rlGamerHandle* gamers,
                                       const unsigned numGamers,
                                       rlProfileStatsReadResults* results, 
                                       netStatus* status)
{
    rlProfileStatsReadStatsByGamerTask* task = NULL;
    
    rtry
    {
        task = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsReadStatsByGamerTask>();
        
        rcheck(task, 
               catchall, 
               rlError("rlProfileStatsClient[%d]::GetStatsByGamerHandle: Failed to allocate task", m_LocalGamerIndex));

        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, gamers, numGamers, results, status), 
               catchall, 
               rlError("rlProfileStatsClient[%d]::GetStatsByGamerHandle: Failed to configure task", m_LocalGamerIndex));
        
        rcheck(rlProfileStats::m_TaskMgr.AddParallelTask(task), 
               catchall, 
               rlError("rlProfileStatsClient[%d]::GetStatsByGamerHandle: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        if(task) rlProfileStats::m_TaskMgr.DestroyTask(task);
        return false;
    }
}

void
rlProfileStatsClient::SetMsUntilNextFlush(const unsigned ms)
{
    m_FlushEnabled = (ms != 0);
    m_MsUntilNextFlush = ms;

    if(m_MsUntilNextFlush)
    {
        rlDebug("rlProfileStatsClient[%d]::SetMsUntilNextFlush: %0.2f secs until next flush allowed", m_LocalGamerIndex, m_MsUntilNextFlush/1000.0f);
    }
    else if(!m_FlushEnabled)
    {
        rlDebug("rlProfileStatsClient[%d]::SetMsUntilNextFlush: Flushes disabled", m_LocalGamerIndex);
    }
}

bool 
rlProfileStatsClient::Synchronize(netStatus* status)
{
    rlProfileStatsSynchronizeTask* task = NULL;

    rtry
    {
        task = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsSynchronizeTask>();
        rcheck(task, catchall, rlError("rlProfileStatsClient[%d]::Synchronize: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, GetLocalGamerIndex(), status), catchall, rlError("rlProfileStatsClient[%d]::Synchronize: Failed to configure task", m_LocalGamerIndex));
        rcheck(rlProfileStats::m_TaskMgr.AddParallelTask(task), catchall, rlError("rlProfileStatsClient[%d]::Synchronize: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        if(task) rlProfileStats::m_TaskMgr.DestroyTask(task);
        return false;
    }
}

bool
rlProfileStatsClient::SynchronizeGroups(const atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                                        netStatus* status)
{
    rlProfileStatsSynchronizeGroupsTask* task = NULL;

    rtry
    {
        task = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsSynchronizeGroupsTask>();
        rcheck(task, catchall, rlError("rlProfileStatsClient[%d]::SynchronizeGroups: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, GetLocalGamerIndex(), &groups, status), catchall, rlError("rlProfileStatsClient[%d]::SynchronizeGroups: Failed to configure task", m_LocalGamerIndex));
        rcheck(rlProfileStats::m_TaskMgr.AddParallelTask(task), catchall, rlError("rlProfileStatsClient[%d]::SynchronizeGroups: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        if(task) rlProfileStats::m_TaskMgr.DestroyTask(task);
        return false;
    }
}

bool
rlProfileStatsClient::ConditionalSynchronizeGroups(const atFixedArray<rlProfileStatsGroupDefinition, RL_PROFILESTATS_MAX_SYNC_GROUPS>& groups,
                                                   netStatus* status)
{
    rlProfileStatsConditionalSynchronizeGroupsTask* task = NULL;

    rtry
    {
        task = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsConditionalSynchronizeGroupsTask>();
        rcheck(task, catchall, rlError("rlProfileStatsClient[%d]::ConditionalSynchronizeGroups: Failed to allocate task", m_LocalGamerIndex));
        rcheck(rlTaskBase::Configure(task, this, &groups, status), catchall, rlError("rlProfileStatsClient[%d]::ConditionalSynchronizeGroups: Failed to configure task", m_LocalGamerIndex));
        rcheck(rlProfileStats::m_TaskMgr.AddParallelTask(task), catchall, rlError("rlProfileStatsClient[%d]::ConditionalSynchronizeGroups: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        if(task) rlProfileStats::m_TaskMgr.DestroyTask(task);
        return false;
    }
}

bool 
rlProfileStatsClient::ResetStatsByGroup(const unsigned int groupId,
                                        netStatus* status)
{
    rlProfileStatsResetByGroupTask* task = NULL;

    rtry
    {
        task = rlProfileStats::m_TaskMgr.CreateTask<rlProfileStatsResetByGroupTask>();

        rcheck(task, 
            catchall, 
            rlError("rlProfileStatsClient[%d]::ResetStatsByGroup: Failed to allocate task", m_LocalGamerIndex));

        rcheck(rlTaskBase::Configure(task, m_LocalGamerIndex, groupId, status), 
            catchall, 
            rlError("rlProfileStatsClient[%d]::ResetStatsByGroup: Failed to configure task", m_LocalGamerIndex));

        rcheck(rlProfileStats::m_TaskMgr.AddParallelTask(task), 
            catchall, 
            rlError("rlProfileStatsClient[%d]::ResetStatsByGroup: Failed to add task", m_LocalGamerIndex));

        return true;
    }
    rcatchall
    {
        if(task) rlProfileStats::m_TaskMgr.DestroyTask(task);
        return false;
    }
}

};
