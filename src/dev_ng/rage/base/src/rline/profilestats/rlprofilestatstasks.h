// 
// rline/rlprofilestatstasks.h
// 
// Copyright (C) 2010-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPROFILESTATSTASKS_H
#define RLINE_RLPROFILESTATSTASKS_H

#include "rlprofilestatscommon.h"
#include "rlprofilestats.h"
#include "atl/bitset.h"
#include "data/base64.h"
#include "rline/ros/rlroshttptask.h"

namespace rage
{

class rlProfileStats;
class rlProfileStatsClient;
class rlProfileStatsWriteStatsTask;

//------------------------------------------------------------------------------
// rlProfileStatsStreamParser
//------------------------------------------------------------------------------
//PURPOSE
//  Handles parsing a stream of encoded bytes into individual stats.
class rlProfileStatsStreamParser
{
public:
    //PURPOSE
    //  Interface for classes that which to use the parser to receive decoded
    //  profile stats.
    class Receiver
    {
    public:
        Receiver() {}
        virtual ~Receiver() {}

    protected:
        //If the specified rlProfileStatsFormat type doesn't include the stat id
        //in the response, subclasses must implement this and provide the next
        //stat id coming in the stream
        virtual bool GetNextStatId(int& /*out_statId*/) { return false; }
        //If the specified rlProfileStatsFormat type doesn't include the stat type
        //in the response, subclasses must implement this and provide the next
        //stat id coming in the strema
        virtual bool GetNextStatType(rlProfileStatsType& /*out_statType*/) { return false; }
        //Subclasses can override this to receive stats as they are parsed from the stream. Always called
        //in response to calling ParseStream
        virtual void ReceiveStat(const int statId, const rlProfileStatsType statType, const rlProfileStatsValue& statValue) = 0;

    private:
        friend class rlProfileStatsStreamParser;
    };

public:
    //Initializes the parser to parse a stream of the specified format
    rlProfileStatsStreamParser(const rlProfileStatsFormat format);
    //Resets parsing state (mainly clearing out leftover bytes)
    //Can be called when the caller expects the beginning of a new stream
    void ResetStream();
    //Parses the specified stream of bytes into stats, calling ReceiveStat for each on the specified
    //receiver
    bool ParseStream(class Receiver& receiver, const char* ch, const int length);

private:
    datBase64::DecodeContext m_Base64Ctx;

    //A small buffer used to buffer part of an imcomplete stat in
    //between parse calls
    //Must be at least as large as the largest of the largest
    //stat type, the size of a stat id, and the size of stat type
    u8 m_LeftoverBuffer[sizeof(s64)];
    unsigned m_NumLeftoverBytes;

    bool m_HaveStatId:1;
    bool m_HaveStatType:1;

    int m_CurStatId;
    rlProfileStatsType m_CurStatType;

    const rlProfileStatsFormat m_Format;
};

//------------------------------------------------------------------------------
// rlProfileStatsReadStatsByGamerTask
//------------------------------------------------------------------------------
class rlProfileStatsReadStatsByGamerTask : public rlRosHttpTask, public rlProfileStatsStreamParser::Receiver
{
public:
    RL_TASK_DECL(rlProfileStatsReadStatsByGamerTask);
    RL_TASK_USE_CHANNEL(rline_profilestats);

    rlProfileStatsReadStatsByGamerTask();
    virtual ~rlProfileStatsReadStatsByGamerTask();

    bool Configure(const int localGamerIndex,
                   const rlGamerHandle* gamers,
                   const unsigned numGamers,
                   rlProfileStatsReadResults* results);

protected:
    virtual const char* GetServiceMethod() const { return "ProfileStats.asmx/ReadStatsByGamer2"; }
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);
    virtual bool SaxStartElement(const char* name);
    virtual bool SaxAttribute(const char* name, const char* val);
    virtual bool SaxCharacters(const char* ch, const int start, const int length);
    virtual bool SaxEndElement(const char* name);

    virtual bool GetNextStatId(int& out_statId);
    virtual void ReceiveStat(const int statId, const rlProfileStatsType statType, const rlProfileStatsValue& statValue);

	virtual bool IsCancelable() const { return true; }
	virtual void DoCancel();

private:
    friend class rlProfileStatsConditionalSynchronizeGroupsTask;

    rlProfileStatsReadResults* m_Results;
    rlRosSaxReader m_RosSaxReader;

    enum State
    {
        STATE_EXPECT_RESULTS,
        STATE_EXPECT_RECORD,
        STATE_READ_GAMERHANDLE,
        STATE_READ_VALUES,
        STATE_READ_INVALID_IDS
    };
    State m_State;

    rlProfileStatsStreamParser m_StreamParser;
    unsigned m_CurStatIndex;

    //An array of stat ids that are missing. ReadStatsByGamer2 will
    //return a list of invalid stat ids, and we have to use that list
    //to know what stat each stat value corresponds to since we only
    //get a list of values back and not id/value pairs
    atArray<int> m_MissingStatIds;
    //Grow buffer used to parse the entire list of stat ids
    datGrowBuffer m_MissingStatIdsText;
};

//------------------------------------------------------------------------------
// rlProfileStatsSynchronizeTask
//------------------------------------------------------------------------------
class rlProfileStatsSynchronizeTask : public rlRosHttpTask, public rlProfileStatsStreamParser::Receiver
{
public:
    RL_TASK_USE_CHANNEL(rline_profilestats);
    RL_TASK_DECL(rlProfileStatsSynchronizeTask);

    rlProfileStatsSynchronizeTask();
    virtual ~rlProfileStatsSynchronizeTask() {};

    bool Configure(const int localGamerIndex);

protected:
    virtual const char* GetServiceMethod() const { return "ProfileStats.asmx/ReadAllStats"; }
    virtual bool SaxStartElement(const char* name);
    virtual bool SaxCharacters(const char* ch, const int start, const int length);

    virtual void ReceiveStat(const int statId, const rlProfileStatsType statType, const rlProfileStatsValue& statValue);

	virtual bool IsCancelable() const { return true; }
	virtual void DoCancel();

private:
    enum State
    {
        STATE_EXPECT_VALUES,
        STATE_READ_VALUES
    };

    State m_State;

    int m_LocalGamerIndex;
    
    rlProfileStatsStreamParser m_StreamParser;
    rlRosSaxReader m_RosSaxReader;

protected:
	unsigned m_NumReceivedStats;
};

//------------------------------------------------------------------------------
// rlProfileStatsWriteStatsTask
//------------------------------------------------------------------------------
class rlProfileStatsWriteStatsTask : public rlRosHttpTask
{
public:
    RL_TASK_DECL(rlProfileStatsWriteStatsTask);
    RL_TASK_USE_CHANNEL(rline_profilestats);

    rlProfileStatsWriteStatsTask();
    virtual ~rlProfileStatsWriteStatsTask() {};

    bool Configure(const int localGamerIndex,
                   const char* submission,
                   int* numWritten,
                   int* secsUntilNextWrite,
                   int* maxSubmissionBinarySize);

    // For binary submissions
    bool Configure(const int localGamerIndex,
                   const u8* submissionBuf,
                   const int submissionSize,
                   int* numWritten,
                   int* secsUntilNextWrite,
                   int* maxSubmissionBinarySize);

protected:
    virtual const char* GetServiceMethod() const { return "ProfileStats.asmx/WriteStats"; }
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

private:
    friend class rlProfileStatsFlushTask; //So it can call Cancel on this task

    int* m_NumWritten;
    int* m_SecsUntilNextWrite;
    int* m_MaxSubmissionBinarySize;
};

//------------------------------------------------------------------------------
// rlProfileStatsFlushTask
//------------------------------------------------------------------------------
class rlProfileStatsFlushTask : public rlTask<rlProfileStatsClient>
{
public:
    RL_TASK_DECL(rlProfileStatsFlushTask);
    RL_TASK_USE_CHANNEL(rline_profilestats);

    rlProfileStatsFlushTask();
    virtual ~rlProfileStatsFlushTask();

    bool Configure(rlProfileStatsClient* ctx, rlProfileStatsDirtyIterator *flushIt);

    virtual void Start();
    virtual void Update(const unsigned timestep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);
    virtual bool IsCancelable() const { return true; }
    virtual void DoCancel();

private:
    void SetRandomDelayUntilNextFlush();

    bool ConfigureUncompressed(rlProfileStatsClient* ctx, rlProfileStatsDirtyIterator &flushIt);
    bool ConfigureCompressed(rlProfileStatsClient* ctx, rlProfileStatsDirtyIterator &flushIt);

    rlProfileStatsWriteStatsTask m_WriteStatsTask;

    int m_NumWritten;
    int m_SecsUntilNextWrite;
    int m_MaxSubmissionBinarySize;

    netStatus m_MyStatus;
};


//------------------------------------------------------------------------------
// rlProfileStatsResetByGroupTask
//------------------------------------------------------------------------------
class rlProfileStatsResetByGroupTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_profilestats);
    RL_TASK_DECL(rlProfileStatsResetByGroupTask);

    rlProfileStatsResetByGroupTask();
    virtual ~rlProfileStatsResetByGroupTask() {};

    bool Configure(const int localGamerIndex,
                   const unsigned int group);

protected:
    virtual const char* GetServiceMethod() const { return "ProfileStats.asmx/ResetStats"; }
    virtual bool ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/);

private:
    enum State
    {
        STATE_EXPECT_RESULT,
    };

    State m_State;
    // int m_LocalGamerIndex;
};

//------------------------------------------------------------------------------
// rlProfileStatsSynchronizeGroupsTask
//------------------------------------------------------------------------------

//PURPOSE
//  Similar to Synchronize, but synchronizes only stats in the specified groups
//  instead of all stats
class rlProfileStatsSynchronizeGroupsTask : public rlProfileStatsSynchronizeTask
{
public:
    RL_TASK_USE_CHANNEL(rline_profilestats);
    RL_TASK_DECL(rlProfileStatsSynchronizeGroupsTask);

    rlProfileStatsSynchronizeGroupsTask();
    virtual ~rlProfileStatsSynchronizeGroupsTask() {}

    bool Configure(const int localGamerIndex,
                   const atFixedArray<unsigned, RL_PROFILESTATS_MAX_SYNC_GROUPS>* groups);

protected:
    virtual const char* GetServiceMethod() const { return "ProfileStats.asmx/ReadStatsByGroups"; }

	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& /*resultCode*/);

	virtual bool IsCancelable() const { return true; }

private:
    friend class rlProfileStatsConditionalSynchronizeGroupsTask;
};

//------------------------------------------------------------------------------
// rlProfileStatsConditionalSynchronizeGroupsTask
//------------------------------------------------------------------------------

//PURPOSE
//  Encompasses several tasks to conditionally synchronize stats in certain groups,
//  but only if their "version" stats mismatch with what is already on the backend.
//  It does this by first reading the version stats and comparing to the local values,
//  and then synchronizes only groups that mismatch
class rlProfileStatsConditionalSynchronizeGroupsTask : public rlTask<rlProfileStatsClient>
{
public:
    RL_TASK_USE_CHANNEL(rline_profilestats);
    RL_TASK_DECL(rlProfileStatsConditionalSynchronizeGroupsTask);

    rlProfileStatsConditionalSynchronizeGroupsTask();
    virtual ~rlProfileStatsConditionalSynchronizeGroupsTask();

    bool Configure(rlProfileStatsClient* client,
                   const atFixedArray<rlProfileStatsGroupDefinition, RL_PROFILESTATS_MAX_SYNC_GROUPS>* groups);

    virtual void Start();
    virtual void Update(const unsigned timestep);

protected:
	virtual bool IsCancelable() const { return true; }
    virtual void DoCancel();

protected:
    enum State
    {
        STATE_NONE,
        STATE_READING_VERSION,
        STATE_SYNCING_GROUPS,
        STATE_DONE
    };

    State m_State;

    //Fixed-sized storage for our groups to read
    atFixedArray<rlProfileStatsGroupDefinition, RL_PROFILESTATS_MAX_SYNC_GROUPS> m_Groups;
    //Version stats to read
    //Note this is twice the number of groups, one for the profile stats version, and one for
    //the version of some alternate source (e.g. savegame)
    atFixedArray<int, 2 * RL_PROFILESTATS_MAX_SYNC_GROUPS> m_StatIds;
    rlProfileStatsFixedRecord<2 * RL_PROFILESTATS_MAX_SYNC_GROUPS> m_ReadRecord;
    rlProfileStatsReadResults m_ReadResults;

    //Task used to read group versions
    rlProfileStatsReadStatsByGamerTask* m_ReadVersionTask;
    //Task used to sync groups that don't match our version
    rlProfileStatsSynchronizeGroupsTask* m_SyncGroupsTask;

    netStatus m_MyStatus;
};

} // namespace rage

#endif //RLINE_RLPROFILESTATSTASKS_H
