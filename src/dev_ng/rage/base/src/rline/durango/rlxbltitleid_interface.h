// 
// rlxbltitleid_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_TITLEID_H
#define RLXBL_TITLEID_H

#if RSG_DURANGO

namespace rage
{

//PURPOSE
//  Interface to Xbox One title Id information
class rlXblTitleId
{
public:

	//PURPOSE
	//  multiplayerSessionTemplateName is the session template name to use when creating
	//  multiplayer sessions on the MPSD (multiplayer session directory). It is
	//  configured via the XDP (Xbox Developer Portal).
	rlXblTitleId(const char* multiplayerSessionTemplateName);
	~rlXblTitleId() {};

	//PURPOSE
	//  Initializes the title configuration
	void Init();

	//PURPOSE
	//  Returns the Xbox Live Title Id (eg. 0xABCD1234).
	unsigned GetTitleId() const;

	//PURPOSE
	//  Returns the Xbox Live primary service config id.
	const wchar_t* GetPrimaryServiceConfigId() const;

	//PURPOSE
	//  Returns the sandbox id in which the console is currently running.
	const wchar_t* GetSandboxId() const;

	//PURPOSE
	//  Returns the environment the native online service is in.
	//  On Xbox, this is based off the sandboxId. Any sandboxId
	//  other than CERT and RETAIL is considered DEV.
	rlEnvironment GetNativeEnvironment() const;

	//PURPOSE
	//  Returns environment combined with any forced overrides at run or compile time
	rlEnvironment GetEnvironment() const;

	//PURPOSE
	//  Returns the session template name to use when creating
	//  multiplayer sessions on the MPSD (multiplayer session directory)
	const wchar_t* GetMultiplayerSessionTemplateName() const;

	// PURPOSE
	// Updates the titleID from the manifest if necessary
	static unsigned GetXboxLiveConfigurationTitleId();

private:
	 // buf sizes include the null terminator
	const static unsigned SCID_BUF_SIZE = 38;
	const static unsigned SANDBOX_ID_BUF_SIZE = 32;
	const static unsigned MP_SESSION_TEMPLATE_NAME_BUF_SIZE = 32;

	wchar_t m_PrimaryServiceConfigId[SCID_BUF_SIZE];
	wchar_t m_SandboxId[SANDBOX_ID_BUF_SIZE];
	wchar_t m_MultiplayerSessionTemplateName[MP_SESSION_TEMPLATE_NAME_BUF_SIZE];
	unsigned m_TitleId;
	rlEnvironment m_Env;
	rlEnvironment m_NativeEnv;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_TITLEID_H
