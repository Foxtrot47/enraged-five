// 
// rlxblsystemui_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_SYSTEMUI_INTERFACE_H
#define RLXBL_SYSTEMUI_INTERFACE_H
#if RSG_DURANGO

namespace rage
{

class rlGamerHandle;
class netStatus;

//PURPOSE
// Interface to the system UI on Durango
class ISystemUi
{
public:
	//PURPOSE
	// Returns whether the Durango UI is currently being displayed
	virtual bool IsUiShowing() = 0;

	//PURPOSE
	// Show the system UI for Signing In
	virtual bool ShowSigninUi(const int localGamerIndex) = 0;

	//PURPOSE
	// Show the system UI for Gamer Profiles
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the UI
	// gamerHandle - Gamer handle of the profile of which to display
	virtual bool ShowGamerProfileUi(const int localGamerIndex, const rlGamerHandle& gamerHandle) = 0;

	//PURPOSE
	// Shows the system UI for game invites
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the UI
	virtual bool ShowSendInvitesUi(const int localGamerIndex) = 0;

	//PURPOSE
	// Shows the system UI for parties
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the UI
	virtual bool ShowPartySessionsUi(const int localGamerIndex) = 0;

	//PURPOSE
	// Shows the system UI for web browser
	//PARAMS
	// url - the URL to load
	virtual bool ShowWebBrowser(const int localGamerIndex, const char * url) = 0;

	//PURPOSE
	// Shows the system UI for help menu
	//PARAMS
	// url - the URL to load
	virtual bool ShowAppHelpMenu(const int localGamerIndex) = 0;

	//PURPOSE
	// Shows the system UI for adding/removing friends
	virtual bool ShowAddRemoveFriend(const int localGamerIndex, u64 targetXuid) = 0;

	//PURPOSE
	// Shows the system UI for composing messages
	virtual bool ShowComposeMessage(const int localGamerIndex, const char * defaultText, const rlGamerHandle* recipients,  const unsigned numRecipients) = 0;
};

} // namespace rage

#endif // RSG_DURANGO
#endif // RLXBL_SYSTEMUI_INTERFACE_H