// 
// rlxblprofiles_interface.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PROFILES_INTERFACE_H
#define RLXBL_PROFILES_INTERFACE_H

#if RSG_DURANGO

namespace rage
{

//PURPOSE
//  Interface to Xbox Live profile info (display names and gamertags).
class IProfiles
{
public:
	//PURPOSE
	// Called when the main player signs out
	virtual void OnSignOut() = 0;

	//PURPOSE
	// Pass in a list of gamerhandles, get display names and gamertags back about each.
	// displayNames can be NULL if you don't want display names, or gamertags can be NULL if you don't want gamertags, but at least one needs to be non-NULL
	// Note:
	//	gamerhandles, displayNames, gamertags must remain valid until the status is no longer pending. These must not be allocated on the stack.
	virtual bool GetPlayerNames(const int localGamerIndex, const rlGamerHandle* gamerHandles, const unsigned numGamerhandles, rlDisplayName* displayNames, rlGamertag* gamertags, netStatus* status) = 0;

	//PURPOSE
	// Same as above but treats diplayNames and gamertags as SpanArrays. Use this version when the displayName or gamertag is part of another structure.
	// displayNameSpan is the number of bytes to skip to get to the next displayName. Must be at least sizeof(rlDisplayName)
	// gamertagSpan is the number of bytes to skip to get to the next gamertag. Must be at least sizeof(rlGamertag)
	virtual bool GetPlayerNames(const int localGamerIndex, const rlGamerHandle* gamerHandles, const u32 gamerHandleSpan, const unsigned numGamerhandles, rlDisplayName* displayNames, const u32 displayNameSpan, rlGamertag* gamertags, const u32 gamertagSpan, netStatus* status) = 0;

	//PURPOSE
	// Cancels a previous call to GetPlayerNames()
	virtual void CancelPlayerNameRequest(netStatus* status) = 0;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PROFILES_INTERFACE_H
