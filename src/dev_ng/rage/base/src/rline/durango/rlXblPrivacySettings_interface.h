// 
// rlXblPrivacySettings_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PRIVACY_SETTINGS_INTERFACE_H
#define RLXBL_PRIVACY_SETTINGS_INTERFACE_H

#if RSG_DURANGO

#include "rline/rlgamerinfo.h"
#include "atl/array.h"

namespace rage
{


//PURPOSE
// Return results for Privacy checks.
class PrivacySettingsResult
{
public:
	static const int MAX_DENY_REASON = 256;

	PrivacySettingsResult()
		: m_permission(0)
		, m_isAllowed(false)
	{
		m_denyReason[0] = '\0';
	}

	u32  m_permission;
	bool m_isAllowed;
	char m_denyReason[MAX_DENY_REASON]; //Describes the reason why permission was denied. 
};

//PURPOSE
// Return results for Privacy checks.
class PrivacySettingsResultMultiUser
{
public:
	PrivacySettingsResultMultiUser(rlGamerHandle& gamer) {m_gamer = gamer;}

public:
	rlGamerHandle  m_gamer;
	atArray < PrivacySettingsResult >  m_permissions;
};

//PURPOSE
// Manage privacy settings.
class IPrivacySettings
{
public:
	enum ePermissions
	{
		 PERM_BroadcastWithTwitch			= (1<<(0))  //Contains information about how audio buffers are broadcast. 
		,PERM_CommunicateUsingText			= (1<<(1))  //Indicates if the user is communicating with text. 
		,PERM_CommunicateUsingVideo			= (1<<(2))  //Indicates if the user is communicating with video. 
		,PERM_CommunicateUsingVoice			= (1<<(3))  //Indicates if the user is communicating with voice. 
		,PERM_PlayMultiplayer				= (1<<(4))  //Indicates if the user can play in a multiplayer session. 
		,PERM_ViewTargetExerciseInfo		= (1<<(5))  //Contains information about the view target. 
		,PERM_ViewTargetGameHistory			= (1<<(6))  //Contains historical information about the view target. 
		,PERM_ViewTargetMusicHistory		= (1<<(7))  //Contains historical music information about the view target. 
		,PERM_ViewTargetMusicStatus			= (1<<(8))  //Contains status information about music in the view target. 
		,PERM_ViewTargetPresence			= (1<<(9))  //Contains the presence string for the view target. 
		,PERM_ViewTargetProfile				= (1<<(10)) //Contains the profile for the view target. 
		,PERM_ViewTargetVideoHistory		= (1<<(11)) //Contains information about videos presented. 
		,PERM_ViewTargetVideoStatus			= (1<<(12)) //Contains the status of videos presented in the view target.
		,PERM_ViewTargetUserCreatedContent  = (1<<(13)) //Indicates if the user view content created by the target
	};

public:

	//PURPOSE
	// Returns information about multiple permissions for multiple target users.
	//PARAMS
	//  localGamerIndex - Index of local gamer creating the session.
	//  permissions - permissions to check.
	//  gamers - gamer handlers to check against.
	//  numGamers - number of gamers.
	//  status - pool result from request.
	static bool CheckPermissions(const int localGamerIndex, const u32 permissions, PrivacySettingsResultMultiUser* results, const u32 numresults, rlGamerHandle* gamers, const u32 numGamers, netStatus* status);

	//PURPOSE
	// Returns information about one permission for one target user.
	//PARAMS
	//  localGamerIndex - Index of local gamer creating the session.
	//  permission - permission to check.
	//  gamer - gamer handler to check against.
	//  status - pool result from request.
	static bool CheckPermission(const int localGamerIndex, const u32 permission, const rlGamerHandle& gamer, PrivacySettingsResult* result, netStatus* status);
};

} //namespace rage

#endif //RSG_DURANGO

#endif //RLXBL_PRIVACY_SETTINGS_INTERFACE_H
