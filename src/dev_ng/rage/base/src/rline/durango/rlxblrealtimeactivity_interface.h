// 
// rlxblrealtimeactivity_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_REALTIMEACTIVITY_INTERFACE_H
#define RLXBL_REALTIMEACTIVITY_INTERFACE_H

#if RSG_DURANGO

#include "atl/functor.h"

namespace rage
{

//PURPOSE
// Interface to the XB1 Real-Time Activity Service
class IRealTimeActivity
{
public:

	// PURPOSE
	// The number of subscriptions we can create at once or in total
	static const int MAX_SUBSCRIPTIONS_BATCH_SIZE = 16;
	static const int MAX_SUBSCRIPTIONS = 1000;

	//PURPOSE
	// Subscribes to the RTA stream for friends presence
	// Can queue up a large list of friend references
	virtual bool SubscribeToFriendsPresence(int localGamerIndex, const atArray<rlFriendsReference*>* friendReferences) = 0;
	virtual bool SubscribeToFriendsPresence(int localGamerIndex, const rlFriendsReference* friendReferences, const int numFriendReferences) = 0;

	// Remove subscription to the RTA stream for friends presence
	//	Unsubscribe to a large list of friends references
	virtual bool UnsubscribeToFriendsPresence(int localGamerIndex, const atArray<rlFriendsReference*>* friendReferences) = 0;
	virtual bool UnsubscribeToFriendsPresence(int localGamerIndex, const rlFriendsReference* friendReferences, const int numFriendReferences) = 0;

	//PURPOSE
	//  Initialize the realtime activity service
	virtual bool Init() = 0;

	//PURPOSE
	//  Shut down the realtime activity service
	virtual void Shutdown() = 0;

	//PURPOSE
	//  Update the the the realtime activity service.  Should be called once per frame.
	virtual void Update() = 0;

	// PURPOSE
	//  Disconnects the RealTimeActivity service from the local gamer index
	virtual void OnSignOut(const int localGamerIndex) = 0;

	// PURPOSE
	//	Event fired when a RealTimeActivity service web socket is closed
	virtual void OnWebSocketClosed(u64 xuid) = 0;
};

} // namespace rage


#endif // RSG_DURANGO
#endif // RLXBL_REALTIMEACTIVITY_INTERFACE_H