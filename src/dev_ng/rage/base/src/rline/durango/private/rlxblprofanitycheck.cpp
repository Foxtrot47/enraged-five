// 
// rlxblsystemui.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlxblprofanitycheck.h"

#include "atl/functor.h"
#include "diag/seh.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "net/durango/xbltask.h"
#include "rline/rlachievement.h"
#include "rline/rldiag.h"
#include "rline/rltitleid.h"
#include "rline/rltask.h"

using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

using namespace Microsoft::Xbox::Services;
using namespace Microsoft::Xbox::Services::System;

namespace rage
{
	
RAGE_DEFINE_SUBCHANNEL(rline, xblprofanitycheck);
#undef __rage_channel
#define __rage_channel rline_xblprofanitycheck

class XblProfanityCheckTask : public netXblTask
{
public:

	NET_TASK_DECL(XblProfanityCheckTask);
    NET_TASK_USE_CHANNEL(rline_xblprofanitycheck);

	XblProfanityCheckTask();

	bool Configure(const int localGamerIndex, const char* text,	char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord);

	static const int PROFANE_WORD_MAX_BUFFER_SIZE = 8192;

protected:
	virtual bool DoWork();
	virtual void ProcessSuccess();
	
	int m_LocalGamerIndex;

	Windows::Foundation::Collections::IVector<Platform::String^>^ m_strings;

	unsigned* m_numProfaneWord;
	char*     m_profaneWord;
	unsigned  m_profaneWordBufferSize;
};

XblProfanityCheckTask::XblProfanityCheckTask() : netXblTask()
{
	m_LocalGamerIndex = -1;
	m_strings = ref new Platform::Collections::Vector<Platform::String^>();
}

bool XblProfanityCheckTask::Configure(const int localGamerIndex, const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord)
{
	bool success = false;

	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, rlError("Invalid gamer index='%d'.", localGamerIndex));

		rverify(numProfaneWord, catchall, rlError("NULL numProfaneWord."));

		rverify(profaneWord, catchall, rlError("NULL profaneWord."));

		m_numProfaneWord = numProfaneWord;
		*m_numProfaneWord = 0;

		m_LocalGamerIndex = localGamerIndex;

		m_profaneWord = profaneWord;
		m_profaneWordBufferSize = profaneWordBufferSize;
		memset(m_profaneWord, 0, m_profaneWordBufferSize);

		// split the string in words (using space as delimiter)
		const char* charIndex=text;
		while(charIndex && *charIndex != '\0')
		{
			if(*charIndex == ' ')
			{
				charIndex++;
			}
			else
			{
				size_t copyUpTo = (strstr(charIndex, " ")) ? (size_t)(strstr(charIndex, " ")-charIndex) : strlen(charIndex);

				wchar_t wszWord[PROFANE_WORD_MAX_BUFFER_SIZE] = {0};
				String^ wordString = rlXblCommon::UTF8ToWinRtString(charIndex, wszWord, copyUpTo /*COUNTOF(wszStatName)*/); // check if that's ok to pass in the word length instead of the buffer size
				m_strings->Append(wordString);
				charIndex += copyUpTo;
			}
		}

		success = true;
	}
	rcatchall
	{
		rlError("XblProfanityCheckTask :: Failed to configure!");
	}

	return success;
}

bool XblProfanityCheckTask::DoWork()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			rlError("DoWork() :: context == nullptr!");
			return false;
		}

		success = m_XblStatus.BeginOp<IVectorView<VerifyStringResult^ >^ >("VerifyStringsAsync"
																			,context->StringService->VerifyStringsAsync(m_strings->GetView())
																			,DEFAULT_ASYNC_TIMEOUT
																			,this
																			,&m_MyStatus);
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

void XblProfanityCheckTask::ProcessSuccess()
{
	rlDebug1("Finish :: Task Successful!");

	try
	{
		IVectorView<VerifyStringResult^ >^ profaneResults = m_XblStatus.GetResults<IVectorView<VerifyStringResult^ >^ >();

		if(profaneResults == nullptr)
		{
			return;
		}

		bool profaneFound = false;
		for(unsigned i=0; i<profaneResults->Size && !profaneFound; i++)
		{
			VerifyStringResult^ res = profaneResults->GetAt(i);
			VerifyStringResultCode code = res->ResultCode;
			if(code != VerifyStringResultCode::Success)
			{
				//some word failed
				String^ profaneWord = m_strings->GetAt(i);
				rlXblCommon::WinRtStringToUTF8(profaneWord, m_profaneWord, m_profaneWordBufferSize);
				profaneFound = true;
				*m_numProfaneWord += 1;
			}
		}

		rlDebug1("Finish :: profaneWord='%s', numProfaneWord='%d'.", m_profaneWord, *m_numProfaneWord);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		rlError("Finish :: Failed - Exception - profaneWord='%s', numProfaneWord='%d'.", m_profaneWord, *m_numProfaneWord);
	}
}

bool rlXblProfanityCheck::ProfanityCheck(const int localGamerIndex, const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord, netStatus* status)
{
	CREATE_NET_XBLTASK(XblProfanityCheckTask, status, localGamerIndex, text, profaneWord, profaneWordBufferSize, numProfaneWord);
}

} // namespace rage

#endif // RSG_DURANGO
