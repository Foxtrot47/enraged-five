// 
// rlxblhttptask.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_HTTP_TASK_H
#define RLXBL_HTTP_TASK_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblhttptask.h is a private header. Do not #include it."
#endif

#include "rline/rlhttptask.h"
#include "data/rson.h"
#include "diag/channel.h"
#include "net/status.h"

namespace rage
{
	RAGE_DECLARE_SUBCHANNEL(rline, xbl);
	struct rlFriendsReference;

//PURPOSE
//  Abstract base class for tasks that call XBL web services.
class rlXblHttpTask : public rlHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_xbl);
    RL_TASK_DECL(rlXblHttpTask);

    rlXblHttpTask(u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0);
    virtual ~rlXblHttpTask();

    //PURPOSE
    //  Value to pass to Configure for tasks that don't require a local gamer.
    static const int NO_LOCAL_GAMER = -1;

    //PURPOSE
    //  Base class configure.  If the call is not in the
    //  context of a local gamer, then pass in NO_LOCAL_GAMER.
    //  The entire HTTP response will be passed to ProcessResponse().
    bool Configure(const int localGamerIndex);

	//PURPOSE
	//  Base class configure.  If the call is not in the
	//  context of a local gamer, then pass in NO_LOCAL_GAMER.
	//  The entire HTTP response will be passed to ProcessResponse().
	bool Configure(const int localGamerIndex, netHttpVerb verb);

    //PURPOSE
    //  Returns local gamer index we were configured for, or NO_LOCAL_GAMER if
    //  the call is outside the context of a local gamer.
    int GetLocalGamerIndex() const;

protected:
    //Set to true if request should be sent via HTTPS.
    virtual bool UseHttps() const {return true;}

	//These tasks use IXHR2 which uses its own internal trusted CAs. When IXHR2 is
	//disabled (only possible during developement), use the insecure context since
	//we don't have xboxlive CAs pinned.
	virtual SSL_CTX* GetSslCtx() const override {return netTcp::GetInsecureSslCtx();}

    //Returns the host name part of a URL (ex. profile.xboxlive.com). 
    //Derived tasks should not override this method.
    virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const = 0;

	// XBL web services require a contract version, and each
	// web service can require a different version number
	virtual u32 GetContractVersion() const = 0;
    
    // Receives the HTTP response
    virtual bool ProcessResponse(const char* response, int& resultCode) = 0;
    
    //Returns the path part of the URL to be used.
    //E.g. https://profile.xboxlive.com/{path to resource}?arg1=0&arg2=1
    virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const = 0;

private:
    rlXblHttpTask(const rlXblHttpTask& rhs);
    rlXblHttpTask& operator=(const rlXblHttpTask& rhs);

    int m_LocalGamerIndex;
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpProfileSettingsTask
///////////////////////////////////////////////////////////////////////////////
class rlXblHttpProfileSettingsTask : public rlXblHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_xbl);
    RL_TASK_DECL(rlXblHttpProfileSettingsTask);

	rlXblHttpProfileSettingsTask();
	virtual ~rlXblHttpProfileSettingsTask();

	enum ProfileSettings
	{
		GAMEDISPLAYNAME = 0x01, // NOTE: this is unicode, encoded as UTF-8
		DISPLAYPIC = 0x02,
		GAMERSCORE = 0x04,
		// note: never display the gamertag on screen, it violates XRs. Use GAMEDISPLAYNAME instead
		// which may be the gamertag or the player's real name depending on their settings
		GAMERTAG = 0x08,
		PUBLICGAMERPIC = 0x10,
	};

	// Pass in a list of xuids, get information back about each.
	// Specify a combination of ProfileSettings flags. Only request the settings you need.
	bool Configure(const int localGamerIndex, const u64* xuids, const unsigned numXuids, const unsigned settings);

protected:

	// Override any of the following methods as needed. You can get the full response and parse
	// it yourself, or just the parts you need. For example, if you only want to know the display
	// name of each user, you can just override the ProcessUserDisplayName method.
	virtual bool ProcessResponse(const char* response, int& resultCode);
	virtual bool ProcessUserList(const RsonReader &users, int& resultCode);
	virtual bool ProcessUser(u64 xuid, const RsonReader &user, int& resultCode);
	virtual bool ProcessUserSettingsList(u64 xuid, const RsonReader &settings, int& resultCode);
	virtual bool ProcessUserSetting(u64 xuid, const RsonReader &setting, int& resultCode);
	virtual bool ProcessUserDisplayName(u64 xuid, const char* displayName, int& resultCode);
	virtual bool ProcessUserDisplayPic(u64 xuid, const char* displayPic, int& resultCode);
	virtual bool ProcessUserGamerscore(u64 xuid, const char* gamerScore, int& resultCode);
	virtual bool ProcessUserGamertag(u64 xuid, const char* gamertag, int& resultCode);
	virtual bool ProcessUserPublicGamerpic(u64 xuid, const char* publicGamerPic, int& resultCode);
	virtual bool ProcessUserNames(u64 xuid, const char* displayName, const char* gamertag, int& resultCode);

	const char* GetSettingName(ProfileSettings setting);

private:
	const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	bool GetServicePath(char* svcPath, const unsigned maxLen) const;
	u32 GetContractVersion() const;
	rlDisplayName m_TempDisplayName;
	rlGamertag m_TempGamertag;
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpPresenceTask
///////////////////////////////////////////////////////////////////////////////
class rlXblHttpBatchPresenceTask : public rlXblHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(rlXblHttpBatchPresenceTask);

	rlXblHttpBatchPresenceTask();
	virtual ~rlXblHttpBatchPresenceTask();

	enum PresenceSettings
	{
		USER = 1,
		DEVICE,
		TITLE,
		ALL,
		MAX
	};

	// Pass in a list of xuids, get information back about each.
	// Specify a combination of ProfileSettings flags. Only request the settings you need.
	bool Configure(const int localGamerIndex, const u64* xuids, const unsigned numXuids, const unsigned titleId, bool bOnlineOnly, const PresenceSettings setting);

protected:

	// Override any of the following methods as needed. You can get the full response and parse
	// it yourself, or just the parts you need. For example, if you only want to know the display
	// name of each user, you can just override the ProcessUserDisplayName method.
	virtual bool ProcessResponse(const char* response, int& resultCode);
	virtual bool ProcessUser(u64 xuid, const RsonReader &user, int& resultCode);
	virtual bool ProcessUserState(u64 xuid, const char* state, bool bIsInSameTitle, bool bIsActive, int& resultCode) = 0;

private:
	const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	bool GetServicePath(char* svcPath, const unsigned maxLen) const;
	u32 GetContractVersion() const;
	const char* GetSettingName(PresenceSettings setting);
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpGetPresenceGroupTask
///////////////////////////////////////////////////////////////////////////////
class rlXblHttpGetPresenceGroupTask : public rlXblHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(rlXblHttpGetPresenceGroupTask);

	rlXblHttpGetPresenceGroupTask();
	virtual ~rlXblHttpGetPresenceGroupTask();

	enum PresenceMoniker
	{
		PEOPLE
	};

	enum PresenceSettings
	{
		USER = 1,
		DEVICE,
		TITLE,
		ALL,
		MAX
	};

	// Pass in a list of xuids, get information back about each.
	// Specify a combination of ProfileSettings flags. Only request the settings you need.
	bool Configure(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById,
					const PresenceMoniker moniker, const PresenceSettings setting);

protected:

	// Override any of the following methods as needed. You can get the full response and parse
	// it yourself, or just the parts you need. For example, if you only want to know the display
	// name of each user, you can just override the ProcessUserDisplayName method.
	virtual bool ProcessResponse(const char* response, int& resultCode);
	virtual bool ProcessUser(u64 xuid, const RsonReader &user);

private:
	const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	bool GetServicePath(char* svcPath, const unsigned maxLen) const;
	u32 GetContractVersion() const;

	static const char* GetSettingName(PresenceSettings setting);
	static const char* GetMoniker(PresenceMoniker moniker);

	u64 m_Xuid;
	PresenceMoniker m_Moniker;
	PresenceSettings m_PresenceSetting;

	atArray<rlFriendsReference*>* m_FriendRefsByStatus;
	atArray<rlFriendsReference*>* m_FriendRefsById;
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpUserStatsTask
///////////////////////////////////////////////////////////////////////////////
class rlXblHttpUserStatsTask : public rlXblHttpTask
{

public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(rlXblHttpUserStatsTask);

	rlXblHttpUserStatsTask();
	virtual ~rlXblHttpUserStatsTask();

	bool Configure(const int localGamerIndex, const u64 xuid, String^ scid, const char* statName, int statNameLength,  int* outInt);
	bool Configure(const int localGamerIndex, const u64 xuid, String^ scid, const char* statName,int statNameLength, double* outDouble);

private:
	virtual bool ProcessResponse(const char* response, int& resultCode);
	const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	bool GetServicePath(char* svcPath, const unsigned maxLen) const;
	u32 GetContractVersion() const;

	u64 m_Xuid;
	String^ m_Scid;

	int* m_OutInt;
	double* m_OutDouble;

	char m_QueryStat[RL_MAX_STAT_BUF_SIZE];
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttp
///////////////////////////////////////////////////////////////////////////////
class rlXblHttpSetPresenceTask : public rlXblHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(rlXblHttpSetPresenceTask);

	rlXblHttpSetPresenceTask();
	virtual ~rlXblHttpSetPresenceTask();

	bool Configure(const int localGamerIndex, const u64 xuid, String^ scid, String^ friendlyName);

private:
	virtual bool ProcessResponse(const char* response, int& resultCode);
	const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	bool GetServicePath(char* svcPath, const unsigned maxLen) const;
	u32 GetContractVersion() const;

	u64 m_Xuid;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_HTTP_TASK_H
