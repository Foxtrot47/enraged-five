// 
// rlxblsessions.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_SESSIONS_H
#define RLXBL_SESSIONS_H

#if RSG_DURANGO

#include "../rlxblsessions_interface.h"
#include "rline/presence/rlpresencequery.h"
#include "rline/rl.h"
#include "rline/rlgamerinfo.h"
#include "rline/rlmatchingattributes.h"
#include "rline/rlfriendsmanager.h"
#include "rline/rlsessioninfo.h"

#if !defined(__cplusplus_winrt)
#error "rlxblsessions.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblsessions_interface.h instead"
#endif

using namespace Microsoft::Xbox::Services::Multiplayer;

namespace rage
{

class rlXblSession;
class rlXblSessionManagementTask;

RAGE_DECLARE_SUBCHANNEL(rline_xbl, tasks)

//PURPOSE
//  Interface to Xbox Live multiplayer sessions and matchmaking on Durango.
class rlXblSessions : public ISessions
{
public:

    rlXblSessions();
    virtual ~rlXblSessions();

    //PURPOSE
    //  Initialize the matching service.
    bool Init();

    //PURPOSE
    //  Shut down the matching service.
    void Shutdown();

    //PURPOSE
    //  Update the matching service.  Should be called once per frame.
    void Update();

	//PURPOSE
	//  Sets timeout policies for all sessions - see defaults and descriptions above.
	void SetTimeoutPolicies(const unsigned asyncSessionTimeoutMs, const unsigned memberReservedTimeoutMs, const unsigned memberInactiveTimeoutMs, 
									const unsigned memberReadyTimeoutMs, const unsigned sessionEmptyTimeoutMs);

	//PURPOSE
	//  Session tunables
	void SetRunValidateHost(const bool bRunValidateHost);
	void SetConsiderInvalidHostAsInvalidSession(const bool bConsiderInvalidHostAsInvalidSession);

    //PURPOSE
    //  Creates a new session as the host.
    //PARAMS
    //  localGamerIndex - Index of local gamer creating the session.
    //  netMode         - RL_NETMODE_ONLINE or RL_NETMODE_LAN.
    //  maxPubSlots     - Maximum number of public slots.
    //  maxPrivSlots    - Maximum number of private slots.
    //  attrs           - Matchmaking attributes.
    //  createFlags     - Session creation flags.  See rlSession::CreateFlags.
    //  presenceFlags   - Session creation flags.  See rlSession::PresenceFlags.
	//  hSession        - On successful completion will contain the session handle.
    //  sessionInfo     - On successful completion will contain new session info.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Sessions with no public slots are considered private sessions and
    //  will not be advertised on matchmaking.  Private sessions thus
    //  require no server resources.  Consider making sessions private when
    //  possible.  For example, invite-only sessions, such as those used
    //  for parties, should be private.
    //
    //  Matching attributes cannot be nil (unset) for sessions with public
    //  slots.  Private sessions are permitted to have nil attributes, but
    //  they must still specify a valid game mode and game type.
    //
    //  This is an asynchronous operation.  The handle, sessionInfo,
    //  and status parameters must remain valid until the operation completes.
    //  They must not be stack variables.
    bool HostSession(
		const int localGamerIndex,
		const rlNetworkMode netMode,
		const unsigned maxPubSlots,
		const unsigned maxPrivSlots,
		const rlMatchingAttributes& attrs,
		const unsigned createFlags,
		const unsigned presenceFlags,
		rlXblSessionHandle** hSession,
		rlSessionInfo* sessionInfo,
		netStatus* status);

    //PURPOSE
    //  Creates a new session as a guest.
    //PARAMS
    //  localGamerIndex - Index of local gamer creating the session.
    //  sessionInfo     - Info for an existing session hosted by someone else.
    //  netMode         - RL_NETMODE_ONLINE or RL_NETMODE_LAN.
    //  createFlags     - Session creation flags.  See rlSession::CreateFlags.
    //  presenceFlags   - Session creation flags.  See rlSession::PresenceFlags.
    //  maxPublicSlots  - Maximum public slots
    //  maxPrivateSlots - Maximum private slots
    //  hSession        - On successful completion will contain the session handle.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  This is an asynchronous operation.  The handle and status parameters
    //  must remain valid until the operation completes.  They must not be stack
    //  variables.
	bool CreateSession(
		const int localGamerIndex,
		const rlSessionInfo& sessionInfo,
		const rlNetworkMode netMode,
		const unsigned createFlags,
		const unsigned presenceFlags,
		const unsigned maxPublicSlots,
		const unsigned maxPrivateSlots,
		rlXblSessionHandle** hSession,
		netStatus* status);

	//PURPOSE
	//  Destroys a session.
	//PARAMS
	//  hSession        - Handle of an existing session.
	//  localGamerIndex - Local owner of the session.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  This is an asynchronous operation.  The status parameter must remain
	//  valid until the operation completes.  It must not be a stack variable.
	bool DestroySession(
		const int localGamerIndex,
		const rlXblSessionHandle* hSession,
		netStatus* status);

	//PURPOSE
    //  Joins gamers to the local instance of a session.
    //PARAMS
	//  localGamerHandle - Handle to the local gamer (if exists)
	//  createFlags		- The sessions' created flags used to determine party eligibility
    //  hSession        - Handle of an existing session.
    //  joiners         - Array of gamers to join.
    //  slotTypes       - Slot types in which they wish to join.
    //  numJoiners      - Number of joiners.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Call this function to join local as well as remote gamers to the
    //  local instance of the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool JoinSession(
		const rlGamerHandle& localGamerHandle,
		const int createFlags,
		const rlXblSessionHandle* hSession,
		const rlGamerHandle* joiners,
		const rlSlotType* slotTypes,
		const unsigned numJoiners,
		netStatus* status);

    //PURPOSE
    //  Leaves the specified gamer from the specified session.
    //PARAMS
	//  localGamerIndex - Index of local gamer leaving the session.
    //  hSession        - Handle of an existing session.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Call this function to leave the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
	bool LeaveSession(
		const int localGamerIndex,
		const rlXblSessionHandle* hSession,
		netStatus* status);

    //PURPOSE
    //  Changes attributes on the local instance of the session.
    //PARAMS
    //  localGamerIndex - Index of local owner of the session (not necessarily the host).
    //  hSession        - Handle of an existing session.
    //  netMode         - Network mode used to create the session.
    //  newMaxPubSlots  - New maximum number of public slots.
    //  newMaxPrivSlots - New maximum number of private slots.
    //  newAttrs        - New attributes.
    //  createFlags     - Flags used to create the session.
    //  newPresenceFlags- New presence flags.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Attributes with nil values will not be changed on the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    bool ChangeSessionAttributes(
		const int localGamerIndex,
		const rlXblSessionHandle* hSession,
		const rlNetworkMode netMode,
		const unsigned newMaxPubSlots,
		const unsigned newMaxPrivSlots,
		const rlMatchingAttributes& newAttrs,
		const unsigned createFlags,
		const unsigned newPresenceFlags,
		netStatus* status);

    //PURPOSE
    //  Changes who is the session host.
    //PARAMS
    //  hSession        - Handle of an existing session.
    //  localHostIndex  - Index of local gamer that is to become the new host.
    //                    Must be -1 if the new host is remote.
    //  newSessionInfo  - Session info for the new session.  If a local
    //                    gamer is to be the new host then newSessionInfo
    //                    will be populated with info for the migrated session.
    //                    if a remote gamer is the new host then newSessionInfo
    //                    must contain session info received from that gamer.
    //  status          - Optional.  Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  The status and newSessionInfo
    //  parameters must remain valid until the operation completes.
    //  They must not be stack variables.
    bool MigrateHost(
		rlXblSessionHandle** hSession,
		const int localGamerIndex,
		const int localHostIndex,
		const int createFlags,
		const rlSessionInfo& sessionInfo,
		rlSessionInfo* newSessionInfo,
		netStatus* status);
	
	//PURPOSE
	//  Write custom properties for the session (temporary)
	//PARAMS
	//  hSession        - Handle of an existing session.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  This is an asynchronous operation.  The status and newSessionInfo
	//  parameters must remain valid until the operation completes.
	//  They must not be stack variables.
	bool WriteCustomProperties(
		const rlXblSessionHandle* hSession,
		const rlSessionToken& sessionToken,
		netStatus* status);

#if !__NO_OUTPUT
	//PURPOSE
	//  Retrieves the latest copy of the session from MPSD and logs out
	//	all pertinent details
	//PARAMS
	//  hSession        - Handle of an existing session.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  This is an asynchronous operation.  The status and newSessionInfo
	//  parameters must remain valid until the operation completes.
	//  They must not be stack variables.
	virtual bool LogSessionDetails(
		const rlXblSessionHandle* hSession,
		netStatus* status);
#endif

	//PURPOSE
	//  Retrieves the multiplayer session Id of the provided session
	//PARAMS
	//  hSession - Handle of an existing session.
	String^ GetMultiplayerCorrelationId(const rlXblSessionHandle* hSession);

	//PURPOSE
	//  Retrieves a list of the user's local sessions, and leaves them.
	//PARAMS
	//	localGamerIndex	- index of the local gamer
	virtual bool ClearLocalSessions(const int localGamerIndex, netStatus* status);

	//PURPOSE
	// Converts a XB1 MultiplayerSession^ into a rlSessionInfo*
	static bool ConvertToSessionInfo(MultiplayerSession^ session, rlSessionInfo* outResults, unsigned* outTotal);

	//PURPOSE
	// Retrieves the String^ XUID of the session host
	static String^ GetHostXuid(MultiplayerSession^ session);

#if !__NO_OUTPUT
	//PURPOSE
	// Logs session details
	static void LogSessionDetail(MultiplayerSession^ session, const char* headerName, bool bAllMembers);
#endif

	//PURPOSE
	// Determines if a player is in a MultiplayerSession^
	static bool IsPlayerInSession(Platform::String^ xboxUserId, Microsoft::Xbox::Services::Multiplayer::MultiplayerSession^ session);

	rlXblSession* AllocateSession();
	void DeleteSession(rlXblSession* session);
	rlXblSession* GetSessionByHandle(const rlXblSessionHandle* handle) const;

private:

	// From MS: Best practice for titles is expect to encounter orphaned sessions during startup and handle the case 
	// appropriately in code, either by removing the user from the session through Leave() or by reusing the session:
	bool m_bClearedSessions[RL_MAX_LOCAL_GAMERS];
	netStatus m_ClearSessionStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlXblClearUserSessionsTask
///////////////////////////////////////////////////////////////////////////////
class rlXblClearUserSessionsTask : public netTask
{
public:
	NET_TASK_DECL(rlXblClearUserSessionsTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblClearUserSessionsTask();

	bool Configure(const int localGamerIndex);
	virtual netTaskStatus OnUpdate(int* resultCode);
	virtual void OnCancel();

private:
	enum State
	{
		STATE_GET_SESSIONS,
		STATE_GETTING_SESSIONS,
		STATE_LEAVE_SESSIONS,
		STATE_LEAVING_SESSIONS
	};

	bool LeaveSessions();
	bool GetSessions();

	int m_LocalGamerIndex;
	int m_NumSessions;
	State m_State;
	netStatus m_GetStatus;
	IVectorView<MultiplayerSessionStates^>^ m_SessionsResponse;
	netStatus m_LeaveStatus[ISessions::MAX_SESSIONS_BATCH_SIZE];
};

///////////////////////////////////////////////////////////////////////////////
// rlXblCleanupSessionTask
///////////////////////////////////////////////////////////////////////////////
class rlXblCleanupSessionTask : public netTask
{
public:
	NET_TASK_DECL(rlXblCleanupSessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblCleanupSessionTask();

	bool Configure(const int localGamerIndex,  Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionReference);
	virtual netTaskStatus OnUpdate(int* resultCode);
	virtual void OnCancel();

private:

	bool GetSessionDetails();
	bool LeaveSession();

	enum State
	{
		STATE_GET_SESSION_DETAILS,
		STATE_GETTING_SESSION_DETAILS,
		STATE_LEAVE_SESSION,
		STATE_LEAVING_SESSION
	};

	int m_LocalGamerIndex;
	State m_State;
	netStatus m_DetailsStatus;
	netStatus m_LeaveStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_DetailsXblStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_LeaveXblStatus;
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ m_SessionReference;
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSession^ m_Session;
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblGetUserSessionTask
///////////////////////////////////////////////////////////////////////////////
class rlXblGetUserSessionTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblGetUserSessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblGetUserSessionTask();

	bool Configure(const int localGamerIndex, u64 xuid, IVectorView<MultiplayerSessionStates^>^* sessionsPtr);

protected:
	virtual void ProcessSuccess();

	virtual bool DoWork();

	int m_LocalGamerIndex;
	String^ m_XuidFilter;
	IVectorView<MultiplayerSessionStates^>^* m_SessionsResponsePtr;
};

/////////////////////////////////////////////////////////////////////////////
// rlXblGetFriendsSessionsTask
/////////////////////////////////////////////////////////////////////////////
class rlXblGetFriendsSessionsTask : public netTask
{
public:

	NET_TASK_DECL(rlXblGetFriendsSessionsTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblGetFriendsSessionsTask();

	netTaskStatus OnUpdate(int* /*resultCode*/);
	bool Configure(const int localGamerIndex, rlFriend* friends, unsigned numFriends);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();

	int m_LocalGamerIndex;
	int m_NumXuids;

	rlFriend* m_Friends;
	u64 m_Xuids[rlFriendsPage::MAX_FRIEND_PAGE_SIZE];
	netStatus m_MyStatus[rlFriendsPage::MAX_FRIEND_PAGE_SIZE];
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus[rlFriendsPage::MAX_FRIEND_PAGE_SIZE];

	enum State
	{
		STATE_GET_SESSIONS,
		STATE_WAIT
	};

	State m_State;
	static const int MAX_FRIEND_SESSIONS = 10;
};

/////////////////////////////////////////////////////////////////////////////
// rlXblGetFriendsPresenceSessionsTask
/////////////////////////////////////////////////////////////////////////////
class rlXblGetFriendsPresenceSessionsTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblGetFriendsPresenceSessionsTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblGetFriendsPresenceSessionsTask();

	bool Configure(const int localGamerIndex, rlFriend* friends, unsigned numFriends);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();

private:
	int m_LocalGamerIndex;

	rlFriend* m_Friends;
	unsigned m_NumFriends;

	rlSessionQueryData m_SessionQuery[rlPresenceQuery::MAX_SESSIONS_TO_FIND];
	unsigned m_NumResults;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_SESSIONS_H
