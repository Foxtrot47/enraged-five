#ifndef RLXBLCOMMERCE_H
#define RLXBLCOMMERCE_H

#if RSG_DURANGO
#include "../rlxblcommerce_interface.h"

#if !defined(__cplusplus_winrt)
#error "rlxblcommerce.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblcommerce_interface.h instead"
#endif

namespace rage
{

class rlXblCommerce : public ICommerce
{
public:
	virtual bool GetCatalog(const int localGamerIndex, const char* rootProductId, cCommercePlatformDetails* details, eCommercePlatformItemType itemType, netStatus* status);

	virtual bool GetCatalogDetails( const int localGamerIndex, cCommercePlatformDetails* details, int startIndex, int numToFetch, netStatus* status  );


	virtual bool GetInventoryItems( const int localGamerIndex, cCommercePlatformInventoryDetails* details, eCommercePlatformItemType itemType, netStatus* status );

	virtual bool ConsumeFromItem( const int localGamerIndex, const char* idToConsume, int numToConsume, netStatus* status );

	virtual bool DoProductPurchaseCheckout(const int localGamerIndex, const char* availabilityId, netStatus* status);

	virtual bool DoCodeRedemption( int localGamerIndex, netStatus* status );

#if RSG_GDK
	virtual bool GetUserPurchaseId(const int localGamerIndex, const char* serviceTicket, const char* publisherId, rlXblCommerceStoreId* storeId, netStatus* status);
#endif
};

}

#endif //RSG_DURANGO


#endif //RLXBLCOMMERCE_H