// 
// rlxblinternal.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "diag/seh.h"
#include "net/net.h"
#include "net/netfuncprofiler.h"
#include "rline/rldiag.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"
#include <winnls.h>

using namespace Microsoft::Xbox::Services;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage::Streams;
using namespace Windows::Xbox::Multiplayer;
using namespace Windows::Xbox::System;
using namespace Microsoft::Xbox::Services::Multiplayer;
using namespace Windows::Xbox::Networking;

namespace rage
{

#undef __rage_channel
#define __rage_channel rline_xbl

AUTOID_IMPL(rlXblEvent);
AUTOID_IMPL(rlXblPresenceEventSigninStatusChanged);
AUTOID_IMPL(rlXblPresenceEventNetworkStatusChanged);
AUTOID_IMPL(rlXblPartyEventsRosterChanged);
AUTOID_IMPL(rlXblPartyMatchStatusCancelledEvent);
AUTOID_IMPL(rlXblPartyMatchStatusExpiredEvent);
AUTOID_IMPL(rlXblPartyMatchStatusFoundEvent);
AUTOID_IMPL(rlXblPartyGameSessionReadyEvent);
AUTOID_IMPL(rlXblPartyGameSessionUnavailableEvent);
AUTOID_IMPL(rlXblPartyGameSessionInvalidEvent);
AUTOID_IMPL(rlXblPartyGameSessionRegisteredEvent);
AUTOID_IMPL(rlXblPartyMatchSessionRegisteredEvent);
AUTOID_IMPL(rlXblPartyGamePlayersAvailableEvent);
AUTOID_IMPL(rlXblRealTimeActivityConnectionChangedEvent);
AUTOID_IMPL(rlXblFriendListChangedEvent);
AUTOID_IMPL(rlXblFriendOnlineStatusChangedEvent);
AUTOID_IMPL(rlXblFriendTitleStatusChangedEvent);
AUTOID_IMPL(rlXblFriendSessionStatusChangedEvent);
AUTOID_IMPL(rlXblFriendStatisticChangedEvent);
AUTOID_IMPL(rlXblInviteAcceptedEvent);
AUTOID_IMPL(rlXblEventInviteUnavailableEvent);

///////////////////////////////////////////////////////////////////////////////
//  XblInternal
///////////////////////////////////////////////////////////////////////////////

static rlXblInternal* sm_Instance = NULL;

rlXblInternal* 
rlXblInternal::GetInstance()
{
	if(sm_Instance == NULL)
	{
		// TODO: NS - use the allocator passed in
		sm_Instance = RL_ALLOCATE_T(rlXblInternal, rlXblInternal);
		if(sm_Instance)
		{
			new(sm_Instance) rlXblInternal();
		}
	}

	return sm_Instance;
}

rlGameRegion
rlXblInternal::GetGameRegion()
{
	wchar_t localeName[LOCALE_NAME_MAX_LENGTH];

	int retVal = GetUserDefaultLocaleName( localeName, ARRAYSIZE( localeName ) );

	if ( retVal == 0 )
	{
		rlAssertf(0,"rlXblInternal::GetGameRegion() GetUserDefaultLocaleName failed with error code: 0x%x\n",  GetLastError() );
		return RL_GAME_REGION_INVALID;
	}
	else
	{
		Displayf("The Locale Name from GetUserDefaultLocaleName: %s\n",  localeName );
	}

	wchar_t countryCodeW[3];

	retVal = GetLocaleInfoEx(localeName, LOCALE_SISO3166CTRYNAME,countryCodeW, ARRAYSIZE( countryCodeW ) );

	if ( retVal == 0 )
	{
		rlAssertf(0,"rlXblInternal::GetGameRegion() GetLocaleInfoEx failed with error code: 0x%x\n",  GetLastError() );
		return RL_GAME_REGION_INVALID;
	}
	else
	{
		Displayf("The Locale Name from GetLocaleInfoEx: %s\n",  countryCodeW );
	}

	if( wcsstr( countryCodeW, L"CA" ) ||
		wcsstr( countryCodeW, L"MX" ) ||
		wcsstr( countryCodeW, L"US" ) )
	{
		return RL_GAME_REGION_AMERICA;
	}
	else if( wcsstr( countryCodeW, L"JP" ) )
	{
		return RL_GAME_REGION_JAPAN;
	}
		
	return RL_GAME_REGION_EUROPE;
}

rlXblInternal::rlXblInternal()
: m_Allocator(NULL)
, m_Initialized(false)
{

}

rlXblInternal::~rlXblInternal()
{

}

bool 
rlXblInternal::IsInitialized()
{
	return m_Initialized;
}

sysMemAllocator* 
rlXblInternal::GetAllocator()
{
	return m_Allocator;
}

bool 
rlXblInternal::Init(sysMemAllocator* allocator)
{
	bool success = false;

	rtry
	{
		rverify(m_Allocator == NULL, catchall, );
		rverify(allocator, catchall, );
		m_Allocator = allocator;

		try
		{
			// TODO: NS - this is temporary until the templates are automatically loaded
			auto secureDeviceAssociationTemplate = SecureDeviceAssociationTemplate::GetTemplateByName(
				ref new Platform::String(L"PeerTraffic")  // the name of a SecureDeviceAssociationTemplate in the application manifest
				);
		}
		catch (Platform::Exception^ ex)
		{
			RL_EXCEPTION(ex);
		}
		
		rverify(m_Presence.Init(), catchall, );
		rverify(m_Profiles.Init(), catchall, );
		rverify(m_Privileges.Init(), catchall, );
		rverify(m_Sessions.Init(), catchall, );
		rverify(m_SystemUi.Init(), catchall, );
		rverify(m_Achievements.Init(), catchall, );
		rverify(m_Party.Init(), catchall, );
		rverify(m_RealTimeActivity.Init(), catchall, )

		m_Initialized = true;
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void 
rlXblInternal::Shutdown()
{
	m_Sessions.Shutdown();
	m_Profiles.Shutdown();
	m_Presence.Shutdown();
	m_Privileges.Shutdown();
	m_SystemUi.Shutdown();
	m_Achievements.Shutdown();
	m_Party.Shutdown();
	m_RealTimeActivity.Shutdown();

	m_Delegator.Clear();

	if(sm_Instance != NULL)
	{
		sm_Instance->~rlXblInternal();

		// TODO: NS - use the allocator passed in
		rlFree(sm_Instance);
		sm_Instance = NULL;
	}
}

void 
rlXblInternal::Update()
{
	NPROFILE(m_Presence.Update();)
	NPROFILE(m_Profiles.Update();)
	NPROFILE(m_Privileges.Update();)
	NPROFILE(m_Sessions.Update();)
	NPROFILE(m_SystemUi.Update();)
	NPROFILE(m_Achievements.Update();)
	NPROFILE(m_Party.Update();)
	NPROFILE(m_RealTimeActivity.Update();)
}

IPresence* 
rlXblInternal::GetPresenceManager()
{
	return &m_Presence;
}

rlXblPresence* 
rlXblInternal::_GetPresenceManager()
{
	return &m_Presence;
}

IProfiles* 
rlXblInternal::GetProfilesManager()
{
	return &m_Profiles;
}

rlXblProfiles* 
rlXblInternal::_GetProfilesManager()
{
	return &m_Profiles;
}

IPrivileges* 
rlXblInternal::GetPrivilegesManager()
{
	return &m_Privileges;
}

rlXblPrivileges* 
rlXblInternal::_GetPrivilegesManager()
{
	return &m_Privileges;
}

IPlayers* 
rlXblInternal::GetPlayerManager()
{
	return &m_Players;
}

rlXblPlayers* 
rlXblInternal::_GetPlayerManager()
{
	return &m_Players;
}

ISessions* 
rlXblInternal::GetSessionManager()
{
	return &m_Sessions;
}

rlXblSessions* 
rlXblInternal::_GetSessionManager()
{
	return &m_Sessions;
}

ISystemUi*
rlXblInternal::GetSystemUi()
{
	return &m_SystemUi;
}

rlXblSystemUi*
rlXblInternal::_GetSystemUi()
{
	return &m_SystemUi;
}

IAchievements*
rlXblInternal::GetAchievementsManager()
{
	return &m_Achievements;
}

rlXblAchievements*
rlXblInternal::_GetAchievementsManager()
{
	return &m_Achievements;
}

IParty*
rlXblInternal::GetPartyManager()
{
	return &m_Party;
}

rlXblParty*
rlXblInternal::_GetPartyManager()
{
	return &m_Party;
}

IRealTimeActivity* 
rlXblInternal::GetRealtimeActivityManager()
{
	return &m_RealTimeActivity;
}

rlXblRealTimeActivity*
rlXblInternal::_GetRealtimeActivityManager()
{
	return &m_RealTimeActivity;
}

ICommerce* 
rlXblInternal::GetPlatformCommerceManager()
{
	return &m_PlatformCommerce;
}

rlXblCommerce* 
rlXblInternal::_GetPlatformCommerceManager()
{
	return &m_PlatformCommerce;	
}

IProfanityCheck* 
rlXblInternal::GetProfanityCheckManager()
{
	return &m_ProfanityCheck;
}

rlXblProfanityCheck* 
rlXblInternal::_GetProfanityCheckManager()
{
	return &m_ProfanityCheck;	
}

void
rlXblInternal::AddDelegate(rlXblEventDelegate* dlgt)
{
	SYS_CS_SYNC(m_Cs);
	m_Delegator.AddDelegate(dlgt);
}

void
rlXblInternal::RemoveDelegate(rlXblEventDelegate* dlgt)
{
	SYS_CS_SYNC(m_Cs);
	m_Delegator.RemoveDelegate(dlgt);
}

void
rlXblInternal::DispatchEvent(const rlXblEvent *e)
{
	m_Delegator.Dispatch(&g_rlXbl, e);
}

String^ rlXblInternal::GetGlobalReputationServiceConfigId()
{
	return L"7492baca-c1b4-440d-a391-b7ef364a8d40";
}

bool rlXblInternal::HasPendingInvite() const
{
#if RSG_GDK
	return m_Invites.HasPendingInvite();
#else
	return false; // XDK uses the horrendous party system on V
#endif
}

} // namespace rage

#endif  //RSG_DURANGO
