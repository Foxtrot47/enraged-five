// 
// rlxblprivileges.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

//#include "data/rson.h"
#include "rlxblinternal.h"
#include "rlxblprivileges.h"
#include "diag/seh.h"
#include "rline/rlprivileges.h"
#include "rline/rl.h"
#include "rline/rlpresence.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

extern __THREAD int RAGE_LOG_DISABLE;

namespace rage
{

#undef __rage_channel
#define __rage_channel rline_xbl


CompileTimeAssert( rlXblPrivileges::PRIV_RESULT_ABORTED          == (int)Store::PrivilegeCheckResult::Aborted          );
CompileTimeAssert( rlXblPrivileges::PRIV_RESULT_BANNED           == (int)Store::PrivilegeCheckResult::Banned           );
CompileTimeAssert( rlXblPrivileges::PRIV_RESULT_NOISSUE          == (int)Store::PrivilegeCheckResult::NoIssue          );
CompileTimeAssert( rlXblPrivileges::PRIV_RESULT_PURCHASEREQUIRED == (int)Store::PrivilegeCheckResult::PurchaseRequired );
CompileTimeAssert( rlXblPrivileges::PRIV_RESULT_RESTRICTED       == (int)Store::PrivilegeCheckResult::Restricted       );


///////////////////////////////////////////////////////////////////////////////
//  rlXblPrivileges
///////////////////////////////////////////////////////////////////////////////
class rlXblCheckPrivilegesTask : public netTask
{
public:

	NET_TASK_DECL(rlXblCheckPrivilegesTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	enum State
	{
		STATE_CHECK_PRIVILEGES,
		STATE_CHECKING_PRIVILEGES,
	};

	rlXblCheckPrivilegesTask();

	bool Configure(const int localGamerIndex,
					const int* privilegeIds,
					const int numPrivilegeIds,
					const bool attemptResolution,
					Store::PrivilegeCheckResult* privilegeCheckResult);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);

private:
	bool BeginCheckingPrivileges();
	bool PopulatePeerAddress(MultiplayerSession^ session);

	int m_LocalGamerIndex;
	Vector<uint32>^ m_PrivilegeIds;
	bool m_AttemptResolution;

	int m_State;
	netStatus m_MyStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
	Store::PrivilegeCheckResult* m_PrivilegeCheckResult;
};

rlXblCheckPrivilegesTask::rlXblCheckPrivilegesTask()
: m_LocalGamerIndex(-1)
, m_PrivilegeIds(nullptr)
, m_AttemptResolution(false)
, m_State(STATE_CHECK_PRIVILEGES)
, m_PrivilegeCheckResult(NULL)
{
}

bool
rlXblCheckPrivilegesTask::Configure(const int localGamerIndex,
									const int* privilegeIds,
									const int numPrivilegeIds,
									const bool attemptResolution,
									Store::PrivilegeCheckResult* privilegeCheckResult)
{
	++RAGE_LOG_DISABLE;	// Disable the asserts for the calls to new within Platform::Collections::Vector
	bool success = false;

	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(privilegeIds != NULL, catchall, );
		rverify(numPrivilegeIds > 0, catchall, );
		rverify(privilegeCheckResult != NULL, catchall, );

		m_LocalGamerIndex = localGamerIndex;
		m_AttemptResolution = attemptResolution;
		m_PrivilegeCheckResult = privilegeCheckResult;
		m_PrivilegeIds = ref new Platform::Collections::Vector<uint32>();

		for(int i = 0; i < numPrivilegeIds; ++i)
		{
			m_PrivilegeIds->Append(privilegeIds[i]);
		}

		success = true;
	}
	rcatchall
	{
	}

	--RAGE_LOG_DISABLE;
	return success;
}

void 
rlXblCheckPrivilegesTask::OnCancel()
{
	m_XblStatus.Cancel();
}

netTaskStatus 
rlXblCheckPrivilegesTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_CHECK_PRIVILEGES:
		if(WasCanceled())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		else if(!BeginCheckingPrivileges())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			m_State = STATE_CHECKING_PRIVILEGES;
		}
		break;

	case STATE_CHECKING_PRIVILEGES:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

void 
rlXblCheckPrivilegesTask::Complete(const netTaskStatus status)
{
	if((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled())
	{
		try
		{
			*m_PrivilegeCheckResult = m_XblStatus.GetResults<Store::PrivilegeCheckResult>();
		}
		catch (Platform::Exception^ ex)
		{
			NET_EXCEPTION(ex);
		}
	}
}

bool 
rlXblCheckPrivilegesTask::BeginCheckingPrivileges()
{
	bool success = false;

	try
	{
		User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
		if(actingUser == nullptr)
		{
			return false;
		}

		// XDK White Papers:
		// If you call CheckPrivilege for sponsored guests, please call with attemptResolution set to false. 
		// Sponsored guests cannot be �resolved�: they do not have any path to buy, be banned, etc.
		if (rlPresence::IsGuest(m_LocalGamerIndex))
		{
			m_AttemptResolution = false;
		}

		IVectorView<uint32>^ privilegeIds = m_PrivilegeIds->GetView();

		if (rlXblInternal::GetInstance()->_GetPresenceManager()->IsSignedIn(m_LocalGamerIndex))
		{
			success = m_XblStatus.BeginOp<Store::PrivilegeCheckResult>("CheckPrivilegesAsync", 
										Store::Product::CheckPrivilegesAsync(actingUser, privilegeIds, m_AttemptResolution, nullptr),
										DEFAULT_ASYNC_TIMEOUT,
										this,
										&m_MyStatus);
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION_SILENT(ex);
		success = false;
	}

	return success;
}

rlXblPrivileges::rlXblPrivileges()
: m_PrivilegeCheckResult(Store::PrivilegeCheckResult::NoIssue)
, m_CurrentResultNotNeeded(false)
{

}

rlXblPrivileges::~rlXblPrivileges()
{

}

bool 
rlXblPrivileges::Init()
{
	ResetPrivilegeCheck();
	return true;
}

void 
rlXblPrivileges::Shutdown()
{
}

void 
rlXblPrivileges::Update()
{
	if(m_CurrentResultNotNeeded && IsPrivilegeCheckResultReady())
	{
		rlDebug1("rlXblPrivileges::Update - m_CurrentResultNotNeeded && IsPrivilegeCheckResultReady() (m_Status: %u) - Resetting privilege check", m_Status.GetStatus());
		ResetPrivilegeCheck();
	}
}

bool
rlXblPrivileges::CheckPrivileges(const int localGamerIndex, const int privilegeTypeBitfield, const bool attemptResolution)
{
	bool success = false;

	int numPrivilegeIds = 0;
	int privilegeIds[rlPrivileges::PRIVILEGE_MAX];
	
	for(int i = 0; i < rlPrivileges::PRIVILEGE_MAX; ++i)
	{
		if(privilegeTypeBitfield & rlPrivileges::GetPrivilegeBit(static_cast<rlPrivileges::PrivilegeTypes>(i)))
		{
			privilegeIds[numPrivilegeIds] = GetPrivilegeCode(i);
			++numPrivilegeIds;
		}
	}

	rlAssert(numPrivilegeIds > 0);

	rlXblCheckPrivilegesTask* task = NULL;

	rtry
	{
		rverify(m_Status.GetStatus() == netStatus::NET_STATUS_NONE,
				catchall,
				rlError("Attempting to check for privileges while another check is still in progress"));
		rverify(netTask::Create(&task, &m_Status),
				catchall,
				rlError("Error allocating rlXblCheckPrivilegesTask"));
		rverify(task->Configure(localGamerIndex,
								privilegeIds,
								numPrivilegeIds,
								attemptResolution,
								&m_PrivilegeCheckResult),
				catchall,
				rlError("Error configuring rlXblCheckPrivilegesTask"));
		rverify(netTask::Run(task),
				catchall,
				rlError("Error scheduling rlXblCheckPrivilegesTask"));

		success = true;
	}
	rcatchall
	{
		netTask::Destroy(task);
	}

	return success;
}

void
rlXblPrivileges::ResetPrivilegeCheck()
{
	rlDebug1("rlXblPrivileges::ResetPrivilegeCheck - m_PrivilegeCheckResult: %d, m_Status: %u, m_CurrentResultNotNeeded: %s", 
		(int)m_PrivilegeCheckResult, m_Status.GetStatus(), m_CurrentResultNotNeeded ? "true" : "false");
	m_PrivilegeCheckResult = Store::PrivilegeCheckResult::NoIssue;
	m_Status.Reset();
	m_CurrentResultNotNeeded = false;
}

void
rlXblPrivileges::SetPrivilegeCheckResultNotNeeded()
{
	if(IsPrivilegeCheckInProgress())
	{
		rlDebug1("rlXblPrivileges::SetPrivilegeCheckResultNotNeeded - privilege check is in progress");
		if(IsPrivilegeCheckResultReady())
		{
			rlDebug1("rlXblPrivileges::SetPrivilegeCheckResultNotNeeded - check result ready, resetting privilege check");
			ResetPrivilegeCheck();
		}
		else
		{
			rlDebug1("rlXblPrivileges::SetPrivilegeCheckResultNotNeeded - check result not ready, setting m_CurrentResultNotNeeded to true");
			m_CurrentResultNotNeeded = true;
		}
	}
}

rlXblPrivileges::ePrivilegeCheckResult 
rlXblPrivileges::GetPrivilegeCheckResult() const
{
	rlAssert(IsPrivilegeCheckResultReady());
	return (rlXblPrivileges::ePrivilegeCheckResult)m_PrivilegeCheckResult;
}

bool 
rlXblPrivileges::HasPrivilege(const int localGamerIndex, const int privilegeId) const
{
	bool success = false;

	try
	{
		if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(localGamerIndex);
			if (actingUser != nullptr)
			{
				for (u32 i=0; i < actingUser->DisplayInfo->Privileges->Size && i < rlPrivileges::PRIVILEGE_MAX; i++)
				{
					uint32 value = actingUser->DisplayInfo->Privileges->GetAt(i);
					if (value == (u32)GetPrivilegeCode(privilegeId))
					{
						success = true;
						break;
					}
				}
			}
		}
	}
	catch(Platform::Exception^ ex)
	{
		rlError("Failed to check Privileges. Exception 0x%08x, Message: %ls", ex->HResult, ex->Message != nullptr ? ex->Message->Data() : L"NULL");
		success = false;
	}

	return success;
}

// Keep this synchronized with:
//  1)  rlprivileges.h: PrivilegeTypes
//	2)	rlprivileges.cpp: const int privilegeBits[rlPrivileges::PRIVILEGE_MAX]
//  3)  commands_network.sch: PRIVILEGE_TYPE_DURANGO_X bit flags
const int privilegeCodes[rlPrivileges::PRIVILEGE_MAX] = {
	(int)Store::KnownPrivileges::XPRIVILEGE_ADD_FRIEND,
	(int)Store::KnownPrivileges::XPRIVILEGE_CLOUD_GAMING_JOIN_SESSION,
	(int)Store::KnownPrivileges::XPRIVILEGE_CLOUD_GAMING_MANAGE_SESSION,
	(int)Store::KnownPrivileges::XPRIVILEGE_CLOUD_SAVED_GAMES,
	(int)Store::KnownPrivileges::XPRIVILEGE_COMMUNICATIONS,
	(int)Store::KnownPrivileges::XPRIVILEGE_COMMUNICATION_VOICE_INGAME,
	(int)Store::KnownPrivileges::XPRIVILEGE_COMMUNICATION_VOICE_SKYPE,
	(int)Store::KnownPrivileges::XPRIVILEGE_GAME_DVR,
	(int)Store::KnownPrivileges::XPRIVILEGE_MULTIPLAYER_PARTIES,
	(int)Store::KnownPrivileges::XPRIVILEGE_MULTIPLAYER_SESSIONS,
	(int)Store::KnownPrivileges::XPRIVILEGE_PREMIUM_CONTENT,
	(int)Store::KnownPrivileges::XPRIVILEGE_PREMIUM_VIDEO,
	(int)Store::KnownPrivileges::XPRIVILEGE_PROFILE_VIEWING,
	(int)193, //XPRIVILEGE_DOWNLOAD_FREE_CONTENT ...doesn't seem to be listed in docs
	(int)Store::KnownPrivileges::XPRIVILEGE_PURCHASE_CONTENT,
	(int)Store::KnownPrivileges::XPRIVILEGE_SHARE_KINECT_CONTENT,
	(int)Store::KnownPrivileges::XPRIVILEGE_SOCIAL_NETWORK_SHARING,
	(int)Store::KnownPrivileges::XPRIVILEGE_SUBSCRIPTION_CONTENT,
	(int)Store::KnownPrivileges::XPRIVILEGE_USER_CREATED_CONTENT,
	(int)Store::KnownPrivileges::XPRIVILEGE_VIDEO_COMMUNICATIONS,
	(int)Store::KnownPrivileges::XPRIVILEGE_VIEW_FRIENDS_LIST
};

int
rlXblPrivileges::GetPrivilegeCode(const int privilegeType) const
{
	if (rlVerify((int)privilegeType >= 0 && (int)privilegeType < (int)rlPrivileges::PRIVILEGE_MAX))
	{
		return privilegeCodes[(int)privilegeType];
	}

	return 0;
}

void 
rlXblPrivileges::OnSignOut()
{
	rlDebug1("rlXblPrivileges::OnSignOut");

	if (m_Status.Pending())
	{
		rlDebug1("rlXblPrivileges::OnSignOut - Cancelling privilege check");
		m_PrivilegeCheckResult = Store::PrivilegeCheckResult::Aborted;
		netTask::Cancel(&m_Status);
	}
}

} // namespace rage

#endif  //RSG_DURANGO
