// 
// rlxblpresence.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PRESENCE_H
#define RLXBL_PRESENCE_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblpresence.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblpresence_interface.h instead"
#endif

#include "../rlxblpresence_interface.h"
#include "atl/array.h"
#include "diag/channel.h"
#include "net/status.h"
#include "rline/rl.h"
#include "system/userlist.h"
#include "system/criticalsection.h"

using namespace Microsoft::Xbox::Services;
using namespace Windows::Xbox::System;
using namespace Windows::Foundation::Collections;

namespace rage
{

class LocalGamer;

//PURPOSE
//  Interface to Xbox Live on Durango.
class rlXblPresence : public IPresence
{
public:
    rlXblPresence();
    virtual ~rlXblPresence();

    //PURPOSE
    //  Initialize the class.
    bool Init();

    //PURPOSE
    //  Shut down the class.
    void Shutdown();

    //PURPOSE
    //  Update the class. Should be called once per frame.
    void Update();

	//PURPOSE
	//  Returns true if the specified local user is signed in locally.
	bool IsSignedIn(int localGamerIndex);

	//PURPOSE
	//  Returns true if the specified local user is signed into Live.
	bool IsOnline(int localGamerIndex);

	//PURPOSE
	//  Returns true if the console is connected to Xbox Live.
	//  Note: can return true even if no users are signed in.
	bool IsOnline();

	//PURPOSE
	//  Returns the name of the specified local user.
	const char* GetName(int localGamerIndex);

	//PURPOSE
	//  Returns the display name of the specified local user.
	const char* GetDisplayName(int localGamerIndex);

	//PURPOSE
	//  Returns the XUID of the specified local user, or 0 if they're not online.
	u64 GetXboxUserId(int localGamerIndex);

	//PURPOSE
	//  Returns TRUE if the xbox local gamer is valid and ready for use.
	bool HasValidXboxUser(int localGamerIndex);

	//PURPOSE
	// Returns the String^ representation of the specified local user, or "" if they're not online
	String^ GetUserIdString(int localGamerIndex);

	//PURPOSE
	//  Fills the user hash of the specified local user to 'hashBuffer'
	//	Returns TRUE if successful.
	bool GetXboxUserHash(int localGamerIndex, wchar_t* hashBuffer, unsigned maxLen);

	//PURPOSE
	// Set the rich presence string of the specified local user
	bool SetRichPresence(const rlGamerInfo& gamerInfo, const char* presenceString, netStatus* status);

	//PURPOSE
	//	Get the rich presence string of the targetXuid using the context of the local gamer index
	bool GetRichPresence(const int localGamerIndex, u64 targetXuid, netStatus* status);

	XboxLiveContext^ GetXboxLiveContext(int localGamerIndex);
	XboxLiveContext^ GetCurrentXboxLiveContext();

	//PURPOSE
	// Return the Xbox User^ object for the specified local user index
	User^ GetXboxUser(int localGamerIndex);

	//PURPOSE
	// Return the the local index for the specified Xbox User^ object
	int GetLocalIndex(User^ user);

	//PURPOSE
	// Return the the local index for the specified Xbox User^ object
	int GetLocalIndex(IUser^ user);

	//PURPOSE
	// Return a new VectorView of Xbox User^ objects for all local users. Expensive function, do not call every frame.
	IVectorView<User^>^ GetLocalXboxUsers();

	//PURPOSE
	// Returns true if a user is a guest/sponsored account
	virtual bool IsGuest(const int localGamerIndex);

private:

	typedef atFixedArray<LocalGamer*, RL_MAX_LOCAL_GAMERS> LocalGamerList;
	LocalGamerList* m_LocalGamers;
	bool m_WasOnline;
	unsigned int m_RefreshLocalGamers;

	sysUserList::Delegate m_Delegate;

	LocalGamer* GetLocalGamer(int localGamerIndex);

	void OnUserListUpdated(const sysUserInfo& userInfo);
	void OnUserSignedIn(int localGamerIndex, User^ user);
	void OnUserSignedOut(int localGamerIndex);

	void RegisterEventHandlers();
	void RefreshLocalGamers();
	void RefreshNetworkStatus();

public:

	// Network Status
	static bool GetXboxLiveAccess();
	bool m_RefreshNetworkStatus;

	// Xb1 Cnnection profile variables
	static bool sm_XboxLiveAccess;

	static sysCriticalSectionToken m_LocalGamersCS;
	static sysCriticalSectionToken m_NetworkStatusCS;

#if __BANK
	static void AddWidgets();

	bool m_GetRichPresence;
	u32 m_uRichPresenceWriteTime;
	netStatus m_GetRichPresenceStatus;
#endif
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PRESENCE_H
