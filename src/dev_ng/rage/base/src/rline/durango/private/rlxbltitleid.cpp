// 
// rlxblplayers.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO
#include "rlxblinternal.h"
#include "rline/durango/rlxbltitleid_interface.h"
#include "diag/seh.h"
#include "string/unicode.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#pragma warning(pop)

using namespace Windows::Xbox::Services;

namespace rage
{

#undef __rage_channel
#define __rage_channel rline_xbl

static unsigned s_XboxLiveConfigurationTitleId = 0;

rlXblTitleId::rlXblTitleId(const char* multiplayerSessionTemplateName)
	: m_Env(RL_ENV_UNKNOWN)
	, m_NativeEnv(RL_ENV_UNKNOWN)
	, m_TitleId(0)
{
#if RSG_XDK
	Utf8ToWide((char16*)m_MultiplayerSessionTemplateName, multiplayerSessionTemplateName, COUNTOF(m_MultiplayerSessionTemplateName));
#elif RSG_GDK
	strcpy_s(m_MultiplayerSessionTemplateName, multiplayerSessionTemplateName);
#endif
}

void rlXblTitleId::Init()
{
	// we cache this data since it's slow to access and doesn't change after startup

	String^ scid = XboxLiveConfiguration::PrimaryServiceConfigId;
	safecpy(m_PrimaryServiceConfigId, scid->Data());
	rlDebug("PrimaryServiceConfigId: %ls", m_PrimaryServiceConfigId);

	String^ sandboxId = XboxLiveConfiguration::SandboxId;
	safecpy(m_SandboxId, sandboxId->Data());
	m_TitleId = GetXboxLiveConfigurationTitleId();

	rlDebug("SandboxId: %ls", m_SandboxId);
	rlDebug("TitleId: %08X", m_TitleId);

	rlEnvironment env = rlEnvironment::RL_ENV_UNKNOWN;
	
	if(rlXblCommon::IsStringEqualI(sandboxId, L"RETAIL"))
	{
		env = rlEnvironment::RL_ENV_PROD;
	}
	else if(rlXblCommon::IsStringEqualI(sandboxId, L"CERT") || 
			rlXblCommon::IsStringEqualI(sandboxId, L"RKTR.99") ||
			rlXblCommon::IsStringEqualI(sandboxId, L"CERT.DEBUG"))
	{
		env = rlEnvironment::RL_ENV_CERT;
	}
	else
	{
		// any custom sandboxes are considered dev
		env = rlEnvironment::RL_ENV_DEV;

#if !__NO_OUTPUT
		// log and assert if we're not in RKTR.1 in case someone forgets to set it. Otherwise it causes all HTTP requests to fail.
		if(rlXblCommon::IsStringNEqualI(sandboxId, L"RKTR", 4) == false)
		{
			Errorf("Please set your sandbox ID to RKTR.1. Your current sandbox ID is set to %ls", sandboxId->Data());
		}
		rlAssertf(rlXblCommon::IsStringNEqualI(sandboxId, L"RKTR", 4), "Please set your sandbox ID to RKTR.1. Your current sandbox ID is set to %ls", sandboxId->Data());
#endif
	}

	rlDebug("SessionTemplate: %s", m_MultiplayerSessionTemplateName);

	// cache the native environment and then pick an environment based on this
	// the picked environment will be a combination of the native environment and any forced overrides
	m_NativeEnv = env;
	m_Env = rlCheckAndApplyForcedEnvironment(m_NativeEnv);

	rlDebug("Native Environment: %s", rlGetEnvironmentString(m_NativeEnv));
	rlDebug("Environment: %s", rlGetEnvironmentString(m_Env));
}

unsigned 
rlXblTitleId::GetTitleId() const
{
	return m_TitleId;
}

unsigned
rlXblTitleId::GetXboxLiveConfigurationTitleId()
{
	if (s_XboxLiveConfigurationTitleId == 0)
	{
#if RSG_XDK
		String^ titleId = XboxLiveConfiguration::TitleId;
		if(!rlVerify(swscanf_s(titleId->Data(), L"%u", &s_XboxLiveConfigurationTitleId) == 1))
		{
			s_XboxLiveConfigurationTitleId = 0;
		}
#else
		uint32_t titleID;
		ASSERT_ONLY(HRESULT hr =) XGameGetXboxTitleId(&titleID);
		Assertf(hr == S_OK, "Failed to retrieve Title ID");
		s_XboxLiveConfigurationTitleId = titleID;
#endif
	}

	return s_XboxLiveConfigurationTitleId;
}

const wchar_t* 
rlXblTitleId::GetPrimaryServiceConfigId() const
{
	return m_PrimaryServiceConfigId;
}

const wchar_t* 
rlXblTitleId::GetSandboxId() const
{
	return m_SandboxId;
}

rlEnvironment 
rlXblTitleId::GetNativeEnvironment() const
{
	return m_NativeEnv;
}

rlEnvironment
rlXblTitleId::GetEnvironment() const
{
	return m_Env;
}

const wchar_t* 
rlXblTitleId::GetMultiplayerSessionTemplateName() const
{
	return m_MultiplayerSessionTemplateName;
}

} // namespace rage

#endif  //RSG_DURANGO
