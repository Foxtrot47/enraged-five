// 
// rlxblparty.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PARTY_H
#define RLXBL_PARTY_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblparty.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblparty_interface.h instead"
#endif

#include "atl/array.h"
#include "diag/channel.h"
#include "net/durango/xbltask.h"
#include "rline/rl.h"
#include "rline/durango/rlxblparty_interface.h"
#include "rlxblcommon.h"
#include "rlxblhttptask.h"
#include "system/criticalsection.h"
#include "system/nelem.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <collection.h>
#pragma warning(pop)

using namespace Platform;
using namespace Platform::Collections;
using namespace Microsoft::Xbox::Services;
using namespace Windows::Xbox::Multiplayer;
using namespace Windows::Foundation::Collections;

// how many party events we can queue up at once
#define PARTY_EVENT_SIZE 16 

namespace rage
{

class PartyEvent
{
public:
	enum PartyEventType
	{
		PARTY_EVENT_INVALID = -1,
		PARTY_EVENT_SESSION_READY = 0,
		PARTY_EVENT_MATCH_FOUND,
		PARTY_EVENT_MATCH_EXPIRED,
		PARTY_EVENT_MATCH_CANCELLED,
		PARTY_EVENT_AVAILABLE_PLAYERS_CHANGED,
		PARTY_EVENT_MAX
	};

	PartyEvent(PartyEventType eventId, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef, IVectorView<GamePlayer^>^ availablePlayers) 
	{
		m_EventId = eventId;
		rlAssert(availablePlayers->Size <= RL_MAX_PARTY_SIZE);

		m_SessionHandle.Init(sessionRef->ServiceConfigurationId->Data(), sessionRef->SessionName->Data(),  sessionRef->SessionTemplateName->Data());

		int numAvailablePlayers = 0;
		for (unsigned i = 0; i < availablePlayers->Size && i < RL_MAX_PARTY_SIZE; i++)
		{
			Windows::Xbox::Multiplayer::GamePlayer^ player = availablePlayers->GetAt(i);
			m_AvailablePlayers[i].LastInvitedTime = player->LastInvitedTime.UniversalTime;
			swscanf_s(player->XboxUserId->Data(), L"%" LI64FMT L"u", &m_AvailablePlayers[i].XboxUserId);
			numAvailablePlayers++;
		}

		m_NumAvailablePlayers = numAvailablePlayers;
		m_Origin = ORIGIN_UNKNOWN;
	}

	PartyEvent(PartyEventType eventId, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef, GameSessionReadyOrigin origin) 
	{
		m_EventId = eventId;
		m_SessionHandle.Init(sessionRef->ServiceConfigurationId->Data(), 
			sessionRef->SessionName->Data(), 
			sessionRef->SessionTemplateName->Data());
		sysMemSet(&m_AvailablePlayers, 0, sizeof(m_AvailablePlayers));
		m_NumAvailablePlayers = 0;
		m_Origin = origin; 
	}

	PartyEvent(PartyEventType eventId) 
	{
		m_EventId = eventId;
		sysMemSet(&m_SessionHandle, 0, sizeof(m_SessionHandle));
		sysMemSet(&m_AvailablePlayers, 0, sizeof(m_AvailablePlayers));
		m_NumAvailablePlayers = 0;
		m_Origin = ORIGIN_UNKNOWN;
	}

	PartyEvent()
		: m_EventId(PARTY_EVENT_INVALID)
	{
		sysMemSet(&m_SessionHandle, 0, sizeof(m_SessionHandle));
		sysMemSet(&m_AvailablePlayers, 0, sizeof(m_AvailablePlayers));
		m_NumAvailablePlayers = 0;
		m_Origin = ORIGIN_UNKNOWN;
	}

#if !__NO_OUTPUT
	static const char * GetStateName(const PartyEvent& pe)
	{
		static const char *s_rlStateNames[] =
		{
			"PARTY_EVENT_INVALID",
			"PARTY_EVENT_SESSION_READY",
			"PARTY_EVENT_MATCH_FOUND",
			"PARTY_EVENT_MATCH_EXPIRED",
			"PARTY_EVENT_MATCH_CANCELLED",
			"PARTY_EVENT_AVAILABLE_PLAYERS_CHANGED",
		};
		CompileTimeAssert(COUNTOF(s_rlStateNames) == PartyEvent::PARTY_EVENT_MAX + 1); // increment 1 for the -1 starting index

		return s_rlStateNames[pe.m_EventId+1];
	}
#endif

	PartyEventType m_EventId;
	rlXblSessionHandle m_SessionHandle;
	rlXblGamePlayer m_AvailablePlayers[RL_MAX_PARTY_SIZE];
	int m_NumAvailablePlayers;
	GameSessionReadyOrigin m_Origin;
};

//PURPOSE
//  Interface to Xbox Live on XBox One.
class rlXblParty : public IParty
{
public:
    rlXblParty();
    virtual ~rlXblParty();

    //PURPOSE
    //  Initialize the class.
    bool Init();

    //PURPOSE
    //  Shut down the class.
    void Shutdown();

    //PURPOSE
    //  Update the class. Should be called once per frame.
    void Update();

    //PURPOSE
    // Clears the party and all members
    void ClearParty(const bool bFireRosterChangedEvent);

	//PURPOSE
	// Handle a Party State change event (join/leave)
	void OnRefreshPartyView();

	//PURPOSE
	// Handle a Party Roster event
	void OnRefreshPartyDetails();

    //PURPOSE
    //  Returns number and status of all LIVE party members.
    //
    //  This can be used to determine if a party exists on the local console.
    //  If no results are returned, then there is no party.  If results are 
    //  returned, at least one of the party members will be a local gamer.
    //
    //  Title code should only need to call this after getting a "party
    //  changed" presence event, and can cache results between changes.
    //RETURNS
    //  False on error, true on success.   
    //NOTE
    //  LIVE parties can have up to eight members, so be sure that maxResults
    //  is >= that limit to get all the members.
	// TODO: NS - verify that there is a maximum.
    bool GetPartyMembers(rlXblPartyMemberInfo* results, const unsigned maxResults, unsigned* numResults);

	//PURPOSE
	// Flag for party refresh 
	virtual void FlagRefreshParty();

	//PURPOSE
	// Add a user to your party
	// Pass NULL to status to create a fire and forget task
	virtual bool AddLocalUsersToParty(const int localGamerIndex, netStatus* status);

	//PURPOSE
	// Remove all local users to party
	// Pass NULL to status to create a fire and forget task
	virtual bool RemoveLocalUsersFromParty(const int localGamerIndex, netStatus* status);

	//PURPOSE
	// Sends a party invite to the target users
	virtual bool InviteToParty(const int localGamerIndex, const rlGamerHandle* targetUsers, int numUsers, netStatus* status);

	// PURPOSE
	// Switches the party title to the acting user's current title. 
	virtual bool SwitchPartyTitle(const int localGamerIndex, netStatus* status);

	//PURPOSE
	// Get the number of party members
	virtual int GetPartySize();

	//PURPOSE
	// Returns whether we are in a party or not
	virtual bool IsInParty() { return GetPartySize() > 0; }
	virtual bool IsInSoloParty() { return GetPartySize() == 1; }

	//PURPOSE
	// Suppress the game session ready notification
	virtual void SuppressGameSessionReadyNotification(bool bSuppress);
	
	//PURPOSE
	// Get a party member based on the index
	virtual rlXblPartyMemberInfo* GetPartyMember(int index);
	virtual bool IsPartyMember(const rlGamerHandle& hGamer);
	virtual bool IsPartyMember(const u64 xuid);

	// PURPOSE
	// Fills session slots with party members for XR-066
	virtual bool FillOpenSessionSlots(const int localGamerIndex, const rlSessionInfo& sessionInfo, int numSlots, rlGamerHandle* availablePlayers, u64* lastInviteTimes, int numAvailablePlayers, const rlGamerHandle& excludeGamer);

	//PURPOSE
	//  Party tunables
	virtual void SetLeavePartyOnInvalidGameSession(const bool bLeavePartyOnInvalidGameSession);

	//PURPOSE
	// Clear a party member from the specified index
	bool ClearPartyMember(int index, const bool bFireRosterChangedEvent);

	//PURPOSE
	// Add a party member to the specified index
	bool AddPartyMember(rlXblPartyMemberInfo* pMember);

	//PURPOSE
	// Determine if the party is in another title
	bool IsPartyInAnotherTitle();

	//PURPOSE
	// Party chat checks
	bool IsPartyChatActive() const;
	bool IsPartyChatSuppressed() const;

	//PURPOSE
	// Register a game session to the party
	virtual bool RegisterGameSessionToParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status);
	virtual bool DisassociateGameSessionFromParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status);
	
	//PURPOSE
	// Query game session
	virtual bool HasRegisteredGameSession();
	virtual rlXblSessionHandle* GetRegisteredGameSession(rlXblSessionHandle* sessionHandle);
	virtual bool IsRegisteredGameSession(const rlXblSessionHandle* sessionHandle);
	virtual bool ValidateGameSession(const int localGamerIndex); 

	// Private (to rlXblXXX) versions
	bool RegisterGameSession(const int localGamerIndex, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionReference, netStatus* status);
	bool DisassociateGameSession(const int localGamerIndex, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionReference, netStatus* status);
	bool IsRegisteringGameSession();

	//PURPOSE
	// Register a game session to the party
	bool RegisterMatchmakingSession(const int localGamerIndex, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionReference, netStatus* status);

	//PURPOSE
	// Returns the party view
	PartyView^ GetPartyView() { return m_PartyView; }

	//PURPOSE
	// Set the party view
	void SetPartyView(PartyView^ partyView);

	//PURPOSE
	// Check for whether we're currently refreshing our party
	bool IsRefreshingParty();
	bool ShouldRefreshParty();

private:

	void ProcessEvents();

	bool CanGetPartyGameSession();
	bool ShouldGetPartyGameSession();
	void GetGameSession(const rlXblSessionHandle& session);

	bool CanRefreshParty();
	void RefreshParty();

	bool CanFillPartySlots();
	bool ShouldFillPartySlots();
	void GetAvailablePlayers();

	void RegisterEventHandlers();
	void OnPartyStateChanged( Windows::Xbox::Multiplayer::PartyStateChangedEventArgs^ eventArgs );
	void OnPartyRosterChanged( Windows::Xbox::Multiplayer::PartyRosterChangedEventArgs^ eventArgs );
	void OnGamePlayersChanged( Windows::Xbox::Multiplayer::GamePlayersChangedEventArgs^ eventArgs );
	void OnPartyMatchStatusChanged( Windows::Xbox::Multiplayer::MatchStatusChangedEventArgs^ eventArgs );
	void OnGameSessionReady( Windows::Xbox::Multiplayer::GameSessionReadyEventArgs^ eventArgs );

	void UnregisterGamePlayersEventHandler();
	void RegisterGamePlayersChangedEventHandler();

	bool m_Initialized;

	// Wrapper for the PartyView
	typedef atArray<rlXblPartyMemberInfo*> PartyMemberList;
	PartyMemberList m_PartyMembers;
	bool m_bFireRosterChangedEvent;

	// Refresh Statuses
	netStatus m_RefreshPartyStatus;
	netStatus m_GetGameSessionStatus;

	// Xbox Parties
    Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ m_partyGameReadyRef;
	PartyView^ m_PartyView;
	bool m_bActingUserInParty;
	u64 m_ActingUser;

	// bools get/set by session events
	unsigned int m_RefreshParty;
	bool m_bGetGameSession;
	GameSessionReadyOrigin m_GameSessionReadyOrigin;
	bool m_bIsGamePlayerEventRegistered;
	rlXblSessionHandle m_GameSessionHandle;
	bool m_bIsRegisteringGameSession; 

	// Slot Filling
	netStatus m_FillSlotsStatus;
	bool m_bFillSlots;

	// Party/Session Events
	Windows::Foundation::EventRegistrationToken m_partyRosterChangedToken;
	Windows::Foundation::EventRegistrationToken m_partyStateChangedToken;
	Windows::Foundation::EventRegistrationToken m_partyGamePlayersChangedToken;
	Windows::Foundation::EventRegistrationToken m_partyMatchStatusChangedToken;
	Windows::Foundation::EventRegistrationToken m_partyGameSessionReadyToken;
	Windows::Foundation::EventRegistrationToken m_partyChatActiveChangedToken;
	Windows::Foundation::EventRegistrationToken m_partyChatSuppressedChangedToken;

	// Party chat tracking
	bool m_bIsPartyChatActive;
	bool m_bIsPartyChatSuppressed;

	// Event Queue
	rlXblEventQueue<PartyEvent, PARTY_EVENT_SIZE> m_EventQueue;
	void QueueEvent(const PartyEvent& e);

	mutable sysCriticalSectionToken m_Cs;

#if !__NO_OUTPUT
	bool m_LogPartyMembers;
#endif
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PARTY_H
