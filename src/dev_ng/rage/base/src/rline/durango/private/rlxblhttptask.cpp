// 
// rlxblhttptask.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "data/rson.h"
#include "diag/seh.h"
#include "net/durango/xblstatus.h"
#include "net/task.h"
#include "rline/rltitleid.h"
#include "rlxblinternal.h"
#include "rlxblhttptask.h"
#include "string/unicode.h"

#include "rline/durango/rlxblachievements_interface.h"

#include <xdk.h>
#include <stdio.h>

using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

namespace rage
{

#undef __rage_channel
#define __rage_channel rline_xbl

extern const rlTitleId* g_rlTitleId;
	
///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpTask
///////////////////////////////////////////////////////////////////////////////
rlXblHttpTask::rlXblHttpTask(u8* bounceBuffer, const unsigned sizeofBounceBuffer)
: rlHttpTask(NULL, bounceBuffer, sizeofBounceBuffer)
, m_LocalGamerIndex(NO_LOCAL_GAMER)
{

}

rlXblHttpTask::~rlXblHttpTask()
{

}

bool
rlXblHttpTask::Configure(const int localGamerIndex)
{
	return Configure(localGamerIndex, NET_HTTP_VERB_POST);
}

bool rlXblHttpTask::Configure(const int localGamerIndex, netHttpVerb verb)
{
	bool success = false;

	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) || (localGamerIndex == NO_LOCAL_GAMER),
			catchall,
			rlTaskError("Illegal local gamer index: %d", localGamerIndex))

			m_LocalGamerIndex = localGamerIndex;

		rlHttpTaskConfig config;
		config.m_AddDefaultContentTypeHeader = false;
		config.m_HttpVerb = verb;
		config.m_IgnoreProxy = true;

		rverify(rlHttpTask::Configure(&config), catchall, );		

		AddRequestHeaderValue("Content-Type", "application/json");

		// Add the magic header that tells the XDK to retrieve the token and signature and add it
		// to the request automatically.
		if(m_LocalGamerIndex != NO_LOCAL_GAMER)
		{
			wchar_t userHash[1024];
			rverify(g_rlXbl.GetPresenceManager()->GetXboxUserHash(m_LocalGamerIndex, userHash, COUNTOF(userHash)), catchall, );

			rverify(userHash, catchall, );

			unsigned hashLen = wcslen(userHash);
			rverify(hashLen > 0, catchall, );

			char* szUserHash = (char*)Alloca(char, hashLen + 1);
			WideToAscii(szUserHash, (char16*)userHash, hashLen + 1);

			m_HttpRequest.AddRequestHeaderValue(netXblHttp::GetAuthActorHeaderName(), szUserHash);
		}

		char contractVersion[16] = {0};
		formatf(contractVersion, "%u", GetContractVersion());
		AddRequestHeaderValue("x-xbl-contract-version", contractVersion);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

int 
rlXblHttpTask::GetLocalGamerIndex() const
{
    return m_LocalGamerIndex;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpProfileSettingsTask
///////////////////////////////////////////////////////////////////////////////

rlXblHttpProfileSettingsTask::rlXblHttpProfileSettingsTask()
{

}

rlXblHttpProfileSettingsTask::~rlXblHttpProfileSettingsTask()
{

}

const char* 
rlXblHttpProfileSettingsTask::GetSettingName(ProfileSettings setting)
{
	switch(setting)
	{
	case GAMEDISPLAYNAME:
		return "GameDisplayName";
		break;
	case DISPLAYPIC:
		return "DisplayPic";
		break;
	case GAMERSCORE:
		return "Gamerscore";
		break;
	case GAMERTAG:
		return "Gamertag";
		break;
	case PUBLICGAMERPIC:
		return "PublicGamerpic";
		break;
	}
	rlVerifyf(false, "Unknown setting: %d", (int)setting);
	return NULL;
}

const char* rlXblHttpProfileSettingsTask::GetUrlHostName(char* UNUSED_PARAM(hostnameBuf), const unsigned UNUSED_PARAM(sizeofBuf)) const
{
	return "profile.xboxlive.com";
}

bool rlXblHttpProfileSettingsTask::GetServicePath(char* svcPath, const unsigned maxLen ) const
{
	safecpy(svcPath, "users/batch/profile/settings", maxLen);
	return true;
}

u32 
rlXblHttpProfileSettingsTask::GetContractVersion() const
{
	return 2;
}

bool rlXblHttpProfileSettingsTask::Configure(const int localGamerIndex, const u64* xuids, const unsigned numXuids, const unsigned settings)
{
	bool success = false;

	rtry
	{
		rverify(xuids && (numXuids > 0), catchall, );
		rverify(numXuids <= 100, catchall, );
		rverify(settings != 0, catchall, );

		rverify(rlXblHttpTask::Configure(localGamerIndex), catchall, );

		RsonWriter rw;
		char buf[4096] = {0}; // allows for at least 100 xuids of max length
		rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.BeginArray("userIds", NULL), catchall, );

		for(unsigned i = 0; i < numXuids; ++i)
		{
			char szXuid[32] = {0};
			formatf(szXuid, "%" I64FMT "u", xuids[i]);
			rcheck(rw.WriteString(NULL, szXuid), catchall, );
		}

		rcheck(rw.End(), catchall, );

		rcheck(rw.BeginArray("settings", NULL), catchall, );

#define RL_XBL_HTTP_WRITE_SETTING_STRING(setting) \
		if(settings & setting) {rcheck(rw.WriteString(NULL, GetSettingName(setting)), catchall, );}

		RL_XBL_HTTP_WRITE_SETTING_STRING(GAMEDISPLAYNAME);
		RL_XBL_HTTP_WRITE_SETTING_STRING(DISPLAYPIC);
		RL_XBL_HTTP_WRITE_SETTING_STRING(GAMERSCORE);
		RL_XBL_HTTP_WRITE_SETTING_STRING(GAMERTAG);
		RL_XBL_HTTP_WRITE_SETTING_STRING(PUBLICGAMERPIC);
#undef RL_XBL_HTTP_WRITE_SETTING_STRING

		rcheck(rw.End(), catchall, );
		rcheck(rw.End(), catchall, );

		rverify(AppendContent(rw.ToString(), rw.Length()), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessResponse(const char* response, int& resultCode)
{
	bool success = false;

	rtry
	{
		/* Example response:
		  {
			  "profileUsers":[
				 {
					"id":"2533274791381930",
					"settings":[
					   {
						  "id":"GameDisplayName",
						  "value":"John Smith"
					   },
					   {
						  "id":"DisplayPic",
						  "value":"https:\/\/p.sfx.ms\/ic\/bluemanmxxl.png"
					   },
					   {
						  "id":"Gamerscore",
						  "value":"0"
					   },
					   {
						  "id":"Gamertag",
						  "value":"CracklierJewel9"
					   },
					   {
						  "id":"PublicGamerpic",
						  "value":"http:\/\/compass.xbox.com\/assets\/b0\/fd\/b0fd5d5e-c5f8-454b-ac74-de9982a6d157.jpg?n=017.jpg"
					   }
					]
				 }
			  ]
		   }
		*/

		resultCode = 0;
		rcheck(response, catchall, rlError("Null body in response when a body is expected"));

		unsigned len = strlen(response);
		RsonReader rr(response, len);

		RsonReader users;
		rverify(rr.GetMember("profileUsers", &users), catchall, rlTaskError("profileUsers not found in response: %s", response));

		rverify(ProcessUserList(users, resultCode), catchall, );

		success = true;
	}
	rcatchall
	{
		resultCode = -1;
	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserList(const RsonReader &users, int& resultCode)
{
	bool success = false;

	rtry
	{
		RsonReader user;
		bool moreUsers = users.GetFirstMember(&user);

		while(moreUsers)
		{
			char szXuid[32] = {0};
			if(user.ReadString("id", szXuid, sizeof(szXuid)))
			{
				u64 xuid = 0;
				if(rlVerify(sscanf_s(szXuid, "%" I64FMT "u", &xuid) == 1))
				{
					rverify(ProcessUser(xuid, user, resultCode), catchall, );
				}
			}

			moreUsers = user.GetNextSibling(&user);
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUser(u64 xuid, const RsonReader &user, int& resultCode)
{
	bool success = false;

	rtry
	{
		RsonReader settings;
		rverify(user.GetMember("settings", &settings), catchall, );
		rverify(ProcessUserSettingsList(xuid, settings, resultCode), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserSettingsList(u64 xuid, const RsonReader &settings, int& resultCode)
{
	bool success = false;

	rtry
	{
		m_TempDisplayName[0] = '\0';
		m_TempGamertag[0] = '\0';

		RsonReader setting;
		bool moreSettings = settings.GetFirstMember(&setting);
		while(moreSettings)
		{
			rverify(ProcessUserSetting(xuid, setting, resultCode), catchall, );
			moreSettings = setting.GetNextSibling(&setting);
		}

		rverify(ProcessUserNames(xuid, m_TempDisplayName, m_TempGamertag, resultCode), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserSetting(u64 xuid, const RsonReader &setting, int& resultCode)
{
	bool success = false;

	rtry
	{
		char settingName[256] = {0};
		char settingValue[512] = {0};

		rverify(setting.ReadString("id", settingName, sizeof(settingName)), catchall, );

		if(stricmp(settingName, GetSettingName(GAMEDISPLAYNAME)) == 0)
		{
			rverify(setting.ReadString("value", settingValue, sizeof(settingValue)), catchall, );
			
			// convert to wide string
			wchar_t wideDisplayName[RL_MAX_DISPLAY_NAME_BUF_SIZE] = {0};
			Utf8ToWide((char16*)wideDisplayName, settingValue, COUNTOF(wideDisplayName));

			// truncate the name and convert back to utf-8
			char displayName[RL_MAX_DISPLAY_NAME_BUF_SIZE] = {0};
			rlXblCommon::DisplayNameToUtf8(displayName, wideDisplayName);

			safecpy(m_TempDisplayName, displayName);

			rverify(ProcessUserDisplayName(xuid, displayName, resultCode), catchall, );
		}
		else if(stricmp(settingName, GetSettingName(DISPLAYPIC)) == 0)
		{
			rverify(setting.ReadString("value", settingValue, sizeof(settingValue)), catchall, );
			rverify(ProcessUserDisplayPic(xuid, settingValue, resultCode), catchall, );
		}
		else if(stricmp(settingName, GetSettingName(GAMERSCORE)) == 0)
		{
			rverify(setting.ReadString("value", settingValue, sizeof(settingValue)), catchall, );
			rverify(ProcessUserGamerscore(xuid, settingValue, resultCode), catchall, );
		}
		else if(stricmp(settingName, GetSettingName(GAMERTAG)) == 0)
		{
			rverify(setting.ReadString("value", settingValue, sizeof(settingValue)), catchall, );

			safecpy(m_TempGamertag, settingValue);

			rverify(ProcessUserGamertag(xuid, settingValue, resultCode), catchall, );
		}
		else if(stricmp(settingName, GetSettingName(PUBLICGAMERPIC)) == 0)
		{
			rverify(setting.ReadString("value", settingValue, sizeof(settingValue)), catchall, );
			rverify(ProcessUserPublicGamerpic(xuid, settingValue, resultCode), catchall, );
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserDisplayName(u64 xuid, const char* displayName, int& resultCode)
{
	bool success = false;

	rtry
	{
		resultCode = 0;
		rcheck(xuid != 0, catchall, );
		rcheck(displayName && displayName[0] != '\0', catchall, );
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserDisplayPic(u64 xuid, const char* displayPic, int& resultCode)
{
	bool success = false;

	rtry
	{
		resultCode = 0;
		rcheck(xuid != 0, catchall, );
		rcheck(displayPic && displayPic[0] != '\0', catchall, );
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserGamerscore(u64 xuid, const char* gamerScore, int& resultCode)
{
	bool success = false;

	rtry
	{
		resultCode = 0;
		rcheck(xuid != 0, catchall, );
		rcheck(gamerScore && gamerScore[0] != '\0', catchall, );
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserGamertag(u64 xuid, const char* gamertag, int& resultCode)
{
	bool success = false;

	rtry
	{
		resultCode = 0;
		rcheck(xuid != 0, catchall, );
		rcheck(gamertag && gamertag[0] != '\0', catchall, );
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserPublicGamerpic(u64 xuid, const char* publicGamerPic, int& resultCode)
{
	bool success = false;

	rtry
	{
		resultCode = 0;
		rcheck(xuid != 0, catchall, );
		rcheck(publicGamerPic && publicGamerPic[0] != '\0', catchall, );
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpProfileSettingsTask::ProcessUserNames(u64 xuid, const char* UNUSED_PARAM(displayName), const char* UNUSED_PARAM(gamertag), int& resultCode)
{
	bool success = false;

	rtry
	{
		resultCode = 0;
		rcheck(xuid != 0, catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpBatchPresenceTask
///////////////////////////////////////////////////////////////////////////////
rlXblHttpBatchPresenceTask::rlXblHttpBatchPresenceTask()
{

}

rlXblHttpBatchPresenceTask::~rlXblHttpBatchPresenceTask()
{

}

bool rlXblHttpBatchPresenceTask::Configure(const int localGamerIndex, const u64* xuids, const unsigned numXuids, const unsigned titleId, bool bOnlineOnly, PresenceSettings setting)
{
	bool success = false;

	rtry
	{
		rverify(setting < MAX, catchall, );
		rverify(rlXblHttpTask::Configure(localGamerIndex, NET_HTTP_VERB_POST), catchall, );
		
		RsonWriter rw;
		char buf[4096] = {0}; // allows for at least 100 xuids of max length
		rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.BeginArray("users", NULL), catchall, );

		for(unsigned i = 0; i < numXuids; ++i)
		{
			char szXuid[32] = {0};
			formatf(szXuid, "%" I64FMT "u", xuids[i]);
			rcheck(rw.WriteString(NULL, szXuid), catchall, );
		}

		rcheck(rw.End(), catchall, );

		if (titleId > 0)
		{
			rcheck(rw.BeginArray("titles", NULL), catchall, );
			rcheck(rw.WriteUns(NULL, titleId), catchall, );
			rcheck(rw.End(), catchall, );
		}

		if (bOnlineOnly)
		{
			rw.WriteBool("onlineOnly", true);
		}

		if (setting)
		{
			rcheck(rw.WriteString("level", GetSettingName(setting)), catchall, );
		}

		rcheck(rw.End(), catchall, );
		rverify(AppendContent(rw.ToString(), rw.Length()), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool rlXblHttpBatchPresenceTask::ProcessResponse(const char* response, int& resultCode)
{
	bool success = false;

	rtry
	{
		/* Example Response: 

		 [
		 {
			 xuid:"0123456789",
			 state:"online",
			 devices:
			 [
				 {
					 type:"D",
					 titles:
					 [
						 {
							 id:"12341234",
							 name:"Contoso 5",
							 lastModified:"2012-09-17T07:15:23.4930000",
							 placement:"full",
							 state:"active",
							 activity:
							 {
								 richPresence:"Playing on Nirvana"
							 },
						 }
					 ]
				 }
			 ]
		 },
		 ]

   
		*/

		resultCode = 0;
		rcheck(response, catchall, rlError("Null body in response when a body is expected"));

		unsigned len = strlen(response);
		RsonReader rr(response, len);

		RsonReader user;
		bool moreUsers = rr.GetFirstMember(&user);

		while(moreUsers)
		{
			char szXuid[32] = {0};
			if(user.ReadString("xuid", szXuid, sizeof(szXuid)))
			{
				u64 xuid = 0;
				if(rlVerify(sscanf_s(szXuid, "%" I64FMT "u", &xuid) == 1))
				{
					rverify(ProcessUser(xuid, user, resultCode), catchall, );
				}
			}

			moreUsers = user.GetNextSibling(&user);
		}

		success = true;
	}
	rcatchall
	{
		resultCode = -1;
	}

	return success;
}

bool rlXblHttpBatchPresenceTask::ProcessUser(u64 xuid, const RsonReader &user, int& resultCode)
{
	bool success = false;

	rtry
	{
		char stateBuf[64] = {0};

		bool bIsActive = false;
		bool bIsInSameTitle = false;

		user.GetValue("state", stateBuf);

		if (user.HasMember("devices"))
		{
			RsonReader rrDevices;
			user.GetMember("devices", &rrDevices);

			RsonReader rrDevice;
			bool bMoreDevices = rrDevices.GetFirstMember(&rrDevice);

			while(bMoreDevices)
			{
				if (rrDevice.HasMember("titles"))
				{
					RsonReader rrTitles;
					rrDevice.GetMember("titles", &rrTitles);

					RsonReader rrTitle;
					bool bMoreTitles = rrTitles.GetFirstMember(&rrTitle);
					while (bMoreTitles)
					{
						if (rrTitle.HasMember("id"))
						{
							char titleBuf[64];
							rverify(rrTitle.GetValue("id", titleBuf), catchall, );

							// read in the titleID from the title object
							unsigned userTitleId = (unsigned)atoi(titleBuf);

							// get our titleID from the service config
							unsigned titleId = g_rlTitleId->m_XblTitleId.GetTitleId();

							// We found our matching titleID
							if (titleId == userTitleId)
							{
								bIsInSameTitle = true;

								// we only care about the activity of our title
								if (rrTitle.HasMember("activity") && rrTitle.HasMember("state"))
								{
									char titleStateBuf[64] = {0};
									rverify(rrTitle.GetValue("state", titleStateBuf), catchall, );
									bIsActive = (strcmp(titleStateBuf, "Active") == 0);
								}
							}
						}
						
						bMoreTitles = rrTitle.GetNextSibling(&rrTitle);
					}
				}

				bMoreDevices = rrDevice.GetNextSibling(&rrDevice);
			}
		}

		rverify(ProcessUserState(xuid, stateBuf, bIsInSameTitle, bIsActive, resultCode), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}


#define USE_BETA_URL 0
const char* rlXblHttpBatchPresenceTask::GetUrlHostName(char* UNUSED_PARAM(hostnameBuf), const unsigned UNUSED_PARAM(sizeofBuf)) const
{
#if USE_BETA_URL
	return "presencebeta.xboxlive.com";
#else
	return "userpresence.xboxlive.com";
#endif
}

bool rlXblHttpBatchPresenceTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	safecpy(svcPath, "users/batch", maxLen);
	return true;
}

u32 rlXblHttpBatchPresenceTask::GetContractVersion() const
{
	return 1;
}

const char* rlXblHttpBatchPresenceTask::GetSettingName(PresenceSettings setting)
{
	switch(setting)
	{
		case USER:
			return "user";
		case DEVICE:
			return "device";
		case TITLE:
			return "title";
		case ALL:
			return "all";
		default:
			return "user";
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpGetPresenceGroupTask
///////////////////////////////////////////////////////////////////////////////
rlXblHttpGetPresenceGroupTask::rlXblHttpGetPresenceGroupTask()
{

}

rlXblHttpGetPresenceGroupTask::~rlXblHttpGetPresenceGroupTask()
{

}

bool rlXblHttpGetPresenceGroupTask::Configure(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, 
											  atArray<rlFriendsReference*>* friendRefsById, 
											  const PresenceMoniker moniker, const PresenceSettings setting)
{
	bool success = false;

	rtry
	{
		rverify(setting < MAX, catchall, );
		rverify(friendRefsById, catchall, );
		rverify(friendRefsByStatus, catchall, );

		m_Xuid = g_rlXbl.GetPresenceManager()->GetXboxUserId(localGamerIndex);
		m_Moniker = moniker;
		m_FriendRefsByStatus = friendRefsByStatus;
		m_FriendRefsById = friendRefsById;
		m_PresenceSetting = setting;

		rverify(rlXblHttpTask::Configure(localGamerIndex, NET_HTTP_VERB_GET), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}


bool rlXblHttpGetPresenceGroupTask::ProcessResponse(const char* response, int& resultCode)
{
	bool success = false;

	rtry
	{
		/* Example Response: 
		 [
		 {
			 xuid:"0123456789",
			 state:"online",
		 },
		 ]
		*/

		resultCode = 0;
		rcheck(response, catchall, rlError("Null body in response when a body is expected"));

		unsigned len = strlen(response);
		RsonReader rr(response, len);

		RsonReader user;
		bool moreUsers = rr.GetFirstMember(&user);

		while(moreUsers)
		{
			char szXuid[32] = {0};
			if(user.ReadString("xuid", szXuid, sizeof(szXuid)))
			{
				u64 xuid = 0;
				if(rlVerify(sscanf_s(szXuid, "%" I64FMT "u", &xuid) == 1))
				{
					rverify(ProcessUser(xuid, user), catchall, );
				}
			}

			moreUsers = user.GetNextSibling(&user);
		}

		success = true;
	}
	rcatchall
	{
		resultCode = -1;
	}

	return success;
}


bool rlXblHttpGetPresenceGroupTask::ProcessUser(u64 xuid, const RsonReader &user)
{
	bool success = false;

	rtry
	{
		char stateBuf[64] = {0};
		user.GetValue("state", stateBuf);

		bool bOnline = (stricmp(stateBuf, "online") == 0);

		rlGamerHandle gh;
		gh.ResetXbl(xuid);

		// our local index (presence)
		int localIndex = rlPresence::GetActingUserIndex();

		// Get the index of the friend we're updating
		int friendIndex = rlFriendsManager::GetFriendIndex(localIndex, gh);

		rcheck(friendIndex >= 0 && friendIndex < m_FriendRefsById->GetCount(), catchall, );

		// update the online status
		(*m_FriendRefsById)[friendIndex]->m_bIsOnline = bOnline;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

const char* rlXblHttpGetPresenceGroupTask::GetUrlHostName(char* UNUSED_PARAM(hostnameBuf), const unsigned UNUSED_PARAM(sizeofBuf)) const
{
	return "userpresence.xboxlive.com";
}

bool rlXblHttpGetPresenceGroupTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	formatf(svcPath, maxLen, "users/xuid(%" I64FMT "u)/groups/%s?level=%s", m_Xuid, GetMoniker(m_Moniker), GetSettingName(m_PresenceSetting));
	return true;
}

u32 rlXblHttpGetPresenceGroupTask::GetContractVersion() const
{
	return 1;
}

const char* rlXblHttpGetPresenceGroupTask::GetSettingName(PresenceSettings setting)
{
	switch(setting)
	{
	case USER:
		return "user";
	case DEVICE:
		return "device";
	case TITLE:
		return "title";
	case ALL:
		return "all";
	default:
		return "user";
	}
}

const char* rlXblHttpGetPresenceGroupTask::GetMoniker(PresenceMoniker moniker)
{
	switch(moniker)
	{
	case PEOPLE:
		return "People";
	default:
		return "People";
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpUserStatsTask
///////////////////////////////////////////////////////////////////////////////
rlXblHttpUserStatsTask::rlXblHttpUserStatsTask()
	: m_OutInt(NULL)
	, m_OutDouble(NULL)
	, m_Xuid(0)
	, m_Scid(nullptr)
{
	m_QueryStat[0] = '\0';
}

rlXblHttpUserStatsTask::~rlXblHttpUserStatsTask()
{

}

bool rlXblHttpUserStatsTask::Configure(const int localGamerIndex, u64 xuid, String^ scid, const char* statName, int statNameLength, int* outInt)
{
	bool success = false;

	rtry
	{
		m_Xuid = xuid;
		m_Scid = scid;
		m_OutInt = outInt;
		safecpy(m_QueryStat, statName);

		rverify(statNameLength < RL_MAX_STAT_BUF_SIZE, catchall,);
		rverify(rlXblHttpTask::Configure(localGamerIndex, NET_HTTP_VERB_GET), catchall, );

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

bool rlXblHttpUserStatsTask::Configure(const int localGamerIndex, u64 xuid, String^ scid, const char * statName, int statNameLength, double* outDouble)
{
	bool success = false;

	rtry
	{
		m_Xuid = xuid;
		m_Scid = scid;
		m_OutDouble = outDouble;

		rverify(statNameLength < RL_MAX_STAT_BUF_SIZE, catchall,);
		rverify(rlXblHttpTask::Configure(localGamerIndex, NET_HTTP_VERB_GET), catchall, );

		safecpy(m_QueryStat, statName);

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

bool rlXblHttpUserStatsTask::ProcessResponse(const char* response, int& resultCode)
{
	bool success = false;

	rtry
	{
		/* Example Response: 

		{
		"user": {
		"xuid": "123456789",
		"gamertag": "WarriorSaint",
		"stats": [
		{
		"statname": "Wins",
		"type": "Integer",
		"value": 40
		},
		{
		"statname": "Kills",
		"type": "Integer",
		"value": 700
		},
		{
		"statname": "KDRatio",
		"type": "Double",
		"value": 2.23
		},
		{
		"statname": "Headshots",
		"type": "Integer",
		"value": 173
		}
		],
		}
		}

		*/

		resultCode = 0;
		rcheck(response, catchall, rlError("Null body in response when a body is expected"));

		unsigned len = strlen(response);
		RsonReader rr(response, len);

		RsonReader rrStats;
		rverify(rr.GetMember("stats", &rrStats), catchall,);

		RsonReader rrStat;
		rverify(rrStats.GetFirstMember(&rrStat), catchall, );

		char statName[XB1_STAT_MAX_LENGTH];
		RsonReader rrStatName;
		rverify(rrStat.GetMember("statname", &rrStatName), catchall,);
		rrStatName.AsString(statName);

		char statType[XB1_STAT_MAX_LENGTH];
		RsonReader rrType;
		rverify(rrStat.GetMember("type", &rrType), catchall,);
		rrType.AsString(statType);

		RsonReader rrValue;
		rverify(rrStat.GetMember("value", &rrValue), catchall,);

		if (!strcmp(statType, "Integer"))
		{
			if (m_OutInt != NULL)
			{
				rverify(rrValue.AsInt(*m_OutInt), catchall,);
				rlDebug3("Read stat (%s) of type (%s), value: (%d)", statName, statType, *m_OutInt);
			}
		}
		else if (!strcmp(statType, "Double"))
		{
			if (m_OutDouble != NULL)
			{
				rverify(rrValue.AsDouble(*m_OutDouble), catchall,);
				rlDebug3("Read stat (%s) of type (%s), value: (%d)", statName, statType, *m_OutDouble);
			}
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

const char* rlXblHttpUserStatsTask::GetUrlHostName(char* UNUSED_PARAM(hostnameBuf), const unsigned UNUSED_PARAM(sizeofBuf)) const
{
	return "userstats.xboxlive.com";
}

bool rlXblHttpUserStatsTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	formatf(svcPath, maxLen, "users/xuid(%" I64FMT "u)/scids/%ls/stats/%s", m_Xuid, m_Scid->Data(), m_QueryStat);
	return true;
}

u32 rlXblHttpUserStatsTask::GetContractVersion() const
{
	return 1;
}


///////////////////////////////////////////////////////////////////////////////
//  rlXblHttpSetPresenceTask
///////////////////////////////////////////////////////////////////////////////
rlXblHttpSetPresenceTask::rlXblHttpSetPresenceTask()
	: m_Xuid(0)
{
}

rlXblHttpSetPresenceTask::~rlXblHttpSetPresenceTask()
{

}

bool rlXblHttpSetPresenceTask::Configure(const int localGamerIndex, u64 xuid, String^ scid, String^ friendlyName)
{
	bool success = false;

	rtry
	{
		m_Xuid = xuid;
		rverify(rlXblHttpTask::Configure(localGamerIndex, NET_HTTP_VERB_POST), catchall, );

		RsonWriter rw;
		char buf[4096] = {0}; // allows for at least 100 xuids of max length
		rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );

		// title
		char titleIdBuf[256] = {0};
		formatf(titleIdBuf, "%u", g_rlTitleId->m_XblTitleId.GetTitleId());

		rcheck(rw.WriteString("id", titleIdBuf), catchall, );
		rcheck(rw.WriteString("state", "active"), catchall, );
		
		rcheck(rw.Begin("activity", NULL), catchall, );
		rcheck(rw.Begin("richPresence", NULL), catchall, );
		
		char friendlyBuf[256] = {0};
		rlXblCommon::WinRtStringToUTF8(friendlyName, &friendlyBuf[0], sizeof(friendlyBuf));
		rcheck(rw.WriteString("id", friendlyBuf), catchall, );

		char scidBuf[256] = {0};
		rlXblCommon::WinRtStringToUTF8(scid, &scidBuf[0], sizeof(scidBuf));
		rcheck(rw.WriteString("scid", scidBuf), catchall, );

		rcheck(rw.End(), catchall, ); // richPresence
		rcheck(rw.End(), catchall, ); // activity

		rcheck(rw.End(), catchall, ); // null

		rverify(AppendContent(rw.ToString(), rw.Length()), catchall, );

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

bool rlXblHttpSetPresenceTask::ProcessResponse(const char* response, int& resultCode)
{
	bool success = false;

	rtry
	{
		/* Example Response: 

		{
			xuid:"12345",
			state:"online",
			devices:
			[
				{
					type:"D",
					titles:
					[
						{
							id:"12345",
							name:"Buckets are Awesome",
							lastModified:"2012-09-17T07:15:23.4930000",
							placement: "full",
							state:"active",
							activity:
							{
								richPresence:"Playing on map:Mountains"
							}
						}
					]
				}	
			]
		}

		*/

		resultCode = 0;
		rcheck(response, catchall, rlError("Null body in response when a body is expected"));

		success = true;
	}
	rcatchall
	{
		resultCode = false;
	}

	return success;
}

const char* rlXblHttpSetPresenceTask::GetUrlHostName(char* UNUSED_PARAM(hostnameBuf), const unsigned UNUSED_PARAM(sizeofBuf)) const
{
	return "userpresence.xboxlive.com";
}

bool rlXblHttpSetPresenceTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	formatf(svcPath, maxLen, "users/xuid(%" I64FMT "u)/devices/current/titles/current", m_Xuid);
	return true;
}

u32 rlXblHttpSetPresenceTask::GetContractVersion() const
{
	return 1;
}

} // namespace rage

#endif  //RSG_DURANGO
