// 
// rlxblplayers.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO
#include "rlxblinternal.h"
#include "rlxblplayers.h"

#include "net/durango/xblstatus.h"
#include "rline/durango/private/rlxblsessions.h"
#include "diag/seh.h"
#include "net/task.h"
#include "rline/rl.h"
#include "rline/rltitleid.h"
#include "rlxblhttptask.h"
#include "system/timer.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

using namespace Microsoft::Xbox::Services::Social;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

namespace rage
{
	unsigned s_FriendsListSize[RL_MAX_LOCAL_GAMERS] = {0};

#undef __rage_channel
#define __rage_channel rline_xbl

// Cancellation of pending tasks
#define XBLPLAYER_CANCEL_TASK(status) 	                        \
	if(status.Pending())										\
	{                                                           \
		rlGetTaskManager()->CancelTask(&status);				\
	}

// XBL player retrieval subtasks follow similar code paths. Use this
// macro for tasks with the GET INFO, GETTING_INFO, FINISHED states.
#define XBLPLAYER_UPDATE_TASK(state, xovstatus, status, workCallback, resultCallback)	\
	switch(state)													\
	{																\
	case STATE_GET_INFO:											\
		if(WasCanceled())											\
		{															\
			netTaskDebug("Canceled - bailing...");					\
			return false;											\
		}															\
		else if(workCallback())										\
		{															\
			state = STATE_GETTING_INFO;								\
		}															\
		else														\
		{															\
			state = STATE_ERROR;									\
			return false;											\
		}															\
		break;														\
																	\
	case STATE_GETTING_INFO:										\
		xovstatus.Update();											\
		if(!status.Pending())										\
		{															\
			if(status.Succeeded())									\
			{														\
				resultCallback();									\
				state = STATE_FINISHED;								\
			}														\
			else													\
			{														\
				state = STATE_ERROR;								\
				return false;										\
			}														\
		}															\
		break;														\
	default:														\
		break;														\
	}																\
	return true;

// Helper macro for creating the Friends Info and Presence subtasks
#define XBLPLAYER_CREATE_TASK(T, ...)										\
	bool success = false;													\
	T* task = NULL;															\
	rtry																	\
	{																		\
		task = rlGetTaskManager()->CreateTask<T>();							\
		rverify(task,catchall,);											\
		rverify(rlTaskBase::Configure(task, __VA_ARGS__),catchall,);		\
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);		\
		success = true;														\
	}																		\
	rcatchall																\
	{																		\
		if(task)															\
		{																	\
			rlGetTaskManager()->DestroyTask(task);							\
		}																	\
	}																		\
	return success;


	///////////////////////////////////////////////////////////////////////////////
	//  Friends Sorting 
	///////////////////////////////////////////////////////////////////////////////
	static int SortFriends(const void* p0, const void* p1)
	{
		const rlFriend* f0 = (const rlFriend*) p0;
		const rlFriend* f1 = (const rlFriend*) p1;

		int result = 0;
		if(f0->IsInSameTitle() != f1->IsInSameTitle())
		{
			result = f0->IsInSameTitle() ? -1 : 1;
		}
		else if(f0->IsOnline() != f1->IsOnline())
		{
			result = f0->IsOnline() ? -1 : 1;
		}
		else if(f0->IsFavorite() != f1->IsFavorite())
		{
			result = f0->IsFavorite() ? -1 : 1;
		}
		else
		{
			//Case insensitive sort
			result = stricmp(f0->GetName(), f1->GetName());
		}

		return result;
	}

///////////////////////////////////////////////////////////////////////////////
//  rlXblGetFriendInfoTask
///////////////////////////////////////////////////////////////////////////////
class rlXblGetFriendInfoTask : public rlXblHttpProfileSettingsTask
{
private:
	rlFriend* m_FriendsPtr;
	unsigned m_NumFriends;

public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(rlXblGetFriendInfoTask);

	rlXblGetFriendInfoTask() {}
	virtual ~rlXblGetFriendInfoTask() {}

	bool Configure(const int localGamerIndex, rlFriend* friendsPtr, unsigned numFriends)
	{
		bool success = false;
		m_FriendsPtr = friendsPtr;
		m_NumFriends = numFriends;

		rtry
		{
			rverify(numFriends > 0, catchall, );
			u64* xuids = (u64*)Alloca(u64, numFriends);
			for(unsigned i = 0; i < numFriends; i++)
			{
				xuids[i] = friendsPtr[i].GetXuid();
			}

			// The only information we need is the gamer display name
			rverify(rlXblHttpProfileSettingsTask::Configure(localGamerIndex,
				xuids,
				numFriends,
				rlXblHttpProfileSettingsTask::GAMERTAG | rlXblHttpProfileSettingsTask::GAMEDISPLAYNAME),
				catchall, );

			success = true;
		}
		rcatchall
		{
			success = false;
		}

		return success;
	}

	bool ProcessUserDisplayName(u64 xuid, const char* displayName, int& resultCode)
	{
		bool success = false;

		rtry
		{
			resultCode = 0;
			rverify(displayName && displayName[0] != '\0', catchall, );

			// find the user with this id
			rlFriend* pFriend = NULL;
			for(unsigned i = 0; i < m_NumFriends; ++i)
			{
				rlFriend* tempFriend = &m_FriendsPtr[i];
				if(tempFriend == NULL)
				{
					continue;
				}

				if(tempFriend->GetXuid() == xuid)
				{
					pFriend = tempFriend;
					break;
				}
			}

			// if we were able to find a friend with the given XUID, update its display name
			if(pFriend)
			{
				pFriend->SetDisplayName(displayName);
			}

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}

	bool ProcessUserGamertag(u64 xuid, const char* gamerTag, int& resultCode)
	{
		bool success = false;

		rtry
		{
			resultCode = 0;
			rverify(gamerTag && gamerTag[0] != '\0', catchall, );

			// find the user with this id
			rlFriend* pFriend = NULL;
			for(unsigned i = 0; i < m_NumFriends; ++i)
			{
				rlFriend* tempFriend = &m_FriendsPtr[i];
				if(tempFriend == NULL)
				{
					continue;
				}

				if(tempFriend->GetXuid() == xuid)
				{
					pFriend = tempFriend;
					break;
				}
			}

			// if we were able to find a friend with the given XUID, update its gamertag
			if(pFriend)
			{
				pFriend->SetNickname(gamerTag);
			}

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}
};

class rlXblUpdateFriendsPresenceAndSessionsTask : public netTask
{
public:
	NET_TASK_DECL(rlXblUpdateFriendsPresenceAndSessionsTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblUpdateFriendsPresenceAndSessionsTask()
		: m_State(STATE_GET_FRIENDS_PRESENCE)
		, m_Flags(0)
		, m_LocalGamerIndex(-1)
		, m_FriendsPage(NULL)
		, m_StartIndex(0)
		, m_NumFriends(0)
	{
	}

	bool Configure(const int localGamerIndex, rlFriendsPage* page, unsigned startIndex, unsigned numFriends, int flags)
	{
		bool success = false;

		rtry
		{
			rverify(page, catchall, );
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

			// If no friends, bail early (nothing to refresh)
			rcheck(numFriends > 0, catchall, );
			rcheck(page->m_NumFriends > 0, catchall, );

			// Validate the indices are acceptable for the given page
			rverify(startIndex >= 0 && startIndex < (int)page->m_NumFriends, catchall, );
			rverify(startIndex + numFriends <= (int)page->m_MaxFriends, catchall, );

			// tracking: url:bugstar:2330325 - [rlAssert] Error: INVALID_XUID != xuid
			if (numFriends > page->m_NumFriends)
			{
				netTaskWarning("Requesting more friends in the page than the page has info for (%d > %d)", numFriends, page->m_NumFriends);
			}

			netTaskDebug3("Updating Friends Presence+Sessions for %d friends, starting with index %d (flags: %d)", numFriends, startIndex, flags);

			m_NumFriends = numFriends;
			m_Flags = flags;
			m_FriendsPage = page;
			m_NumFriends = numFriends;
			m_StartIndex = startIndex;

			m_LocalGamerIndex = localGamerIndex;

			m_State = STATE_GET_FRIENDS_PRESENCE;

			success = true;
		}
		rcatchall
		{
			success = false;
		}

		return success;
	}

	bool GetFriendsPresence()
	{
		return g_rlXbl.GetPlayerManager()->UpdateFriendsPresence(m_LocalGamerIndex, m_FriendsPage->m_Friends, m_FriendsPage->m_NumFriends, &m_PresenceStatus);
	}

	bool GetFriendsSessions()
	{
		// CheckListForChanges could have detected a removed friend
		int numFriendsToUpdate = rage::Min((int)m_NumFriends, (int)m_FriendsPage->m_NumFriends);
		if (numFriendsToUpdate == 0)
		{
			netTaskDebug1("GetFriendsSessions :: no friends to update");
			m_SessionStatus.SetPending();
			m_SessionStatus.SetSucceeded();
			return true;
		}
		else
		{
			return g_rlXbl.GetPlayerManager()->GetFriendsPresenceSessions(m_LocalGamerIndex, &m_FriendsPage->m_Friends[m_StartIndex], numFriendsToUpdate, &m_SessionStatus);
		}
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		if (WasCanceled())
		{
			return NET_TASKSTATUS_FAILED;
		}

		switch(m_State)
		{
		case STATE_GET_FRIENDS_PRESENCE:
			{
				if (GetFriendsPresence())
				{
					m_State = STATE_GETTING_FRIENDS_PRESENCE;
				}
				else
				{
					netTaskError("Failed to get friends presence");
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		case STATE_GETTING_FRIENDS_PRESENCE:
			{
				if (!m_PresenceStatus.Pending())
				{
					if (m_PresenceStatus.Succeeded())
					{
						m_State = STATE_GET_FRIENDS_SESSIONS;
					}
					else
					{
						netTaskError("Failed to retrieve friends presence");
						status = NET_TASKSTATUS_FAILED;
					}
				}
			}
			break;
		case STATE_GET_FRIENDS_SESSIONS:
			{
				if (GetFriendsSessions())
				{
					m_State = STATE_GETTING_FRIENDS_SESSIONS;
				}
				else
				{
					netTaskError("Failed to get friends sessions");
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		case STATE_GETTING_FRIENDS_SESSIONS:
			{
				if (!m_SessionStatus.Pending())
				{
					if (m_SessionStatus.Succeeded())
					{
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						netTaskError("Failed to retrieve friends sessions");
						status = NET_TASKSTATUS_FAILED;
					}
				}
			}
			break;
		}

		return status;
	}

	virtual void OnCancel()
	{
		if (m_PresenceStatus.Pending() && rlGetTaskManager())
		{
			rlGetTaskManager()->CancelTask(&m_PresenceStatus);
		}

		if (m_SessionStatus.Pending())
		{
			netTask::Cancel(&m_SessionStatus);
		}
	}

private:
	
	enum State
	{
		STATE_GET_FRIENDS_PRESENCE,
		STATE_GETTING_FRIENDS_PRESENCE,
		STATE_GET_FRIENDS_SESSIONS,
		STATE_GETTING_FRIENDS_SESSIONS
	};

	State m_State;

	rlFriendsPage* m_FriendsPage;
	int m_Flags;
	int m_LocalGamerIndex;
	unsigned m_StartIndex;
	unsigned m_NumFriends;

	netStatus m_PresenceStatus;
	netStatus m_SessionStatus;
};

///////////////////////////////////////////////////////////////////////////////
//	rlXblUpdateFriendPresenceTask
///////////////////////////////////////////////////////////////////////////////
class rlXblUpdateFriendPresenceTask : public rlXblHttpBatchPresenceTask
{
private:
	rlFriend* m_FriendsPtr;
	unsigned m_NumFriends;
	int m_MaxFriends;
	int m_Flags;
	int m_LocalGamerIndex;

public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(rlXblUpdateFriendPresenceTask);

	rlXblUpdateFriendPresenceTask() {}
	virtual ~rlXblUpdateFriendPresenceTask() {}

	bool Configure(const int localGamerIndex, rlFriend* friendsPtr, unsigned numFriends, int flags)
	{
		bool success = false;

		rtry
		{
			rverify(friendsPtr, catchall, );
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			m_NumFriends = numFriends;
			m_Flags = flags;
			m_FriendsPtr = friendsPtr;
			m_LocalGamerIndex = localGamerIndex;

			u64* xuids = (u64*)Alloca(u64, m_NumFriends);
			for(unsigned i = 0; i < m_NumFriends; i++)
			{
				xuids[i] = friendsPtr[i].GetXuid();
				if (xuids[i] == 0)
				{
					rlTaskError("Invalid xuid passed to friends presence task (idx: %d)", i);
				}
			}

			// We need the title information to determine the title ID the user is in
			rlXblHttpBatchPresenceTask::PresenceSettings settings;
			settings = rlXblHttpBatchPresenceTask::PresenceSettings::TITLE;

			// Should we filter for online
			bool bFilterForOnline = (m_Flags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE) > 0 || 
				(m_Flags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE) > 0;

			// pass 0 for titleId will not filter title Id
			unsigned titleId = 0;
			if (m_Flags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE)
			{
				// get our titleID from the service config
				titleId = g_rlTitleId->m_XblTitleId.GetTitleId();
			}

			rverify(rlXblHttpBatchPresenceTask::Configure(localGamerIndex, xuids, m_NumFriends, titleId, bFilterForOnline, settings), catchall, );

			success = true;
		}
		rcatchall
		{
			success = false;
		}

		return success;
	}

	bool ProcessUserState(u64 xuid, const char* state, bool bIsInSameTitle, bool UNUSED_PARAM(bIsActive), int& resultCode)
	{
		bool success = false;

		rtry
		{
			resultCode = 0;
			rverify(state && state[0] != '\0', catchall, );

			// find the user with this id
			rlFriend* pFriend = NULL;
			int friendIndex = 0;
			for(unsigned i = 0; i < m_NumFriends; ++i)
			{
				rlFriend* tempFriend = &m_FriendsPtr[i];
				if(tempFriend == NULL)
				{
					continue;
				}

				if(tempFriend->GetXuid() == xuid)
				{
					pFriend = tempFriend;
					friendIndex = i;
					break;
				}
			}

			// Update our friend's online status
			if(pFriend)
			{
				// check the online status
				bool bIsOnline = (stricmp(state, "online") == 0);

				// set the online status, cache the previous status
				bool bWasOnline = pFriend->IsOnline();
				bool bWasInSameTitle = pFriend->IsInSameTitle();

				// update the online state
				pFriend->SetIsOnline(bIsOnline, bIsInSameTitle);

				// check for online changes
				if (bWasOnline != bIsOnline)
				{
					rlXblFriendOnlineStatusChangedEvent e(xuid, bIsOnline);
					rlXblInternal::GetInstance()->DispatchEvent(&e);
				}

				// check for title changes
				if (bWasInSameTitle != bIsInSameTitle)
				{
					rlXblFriendTitleStatusChangedEvent e(xuid, bIsInSameTitle);
					rlXblInternal::GetInstance()->DispatchEvent(&e);
				}

				rlTaskDebug3("ProcessUserState for %" I64FMT "u: Online (was:%d/is:%d) and Title (was:%d/is%d)", xuid, bWasOnline, bIsOnline, bWasInSameTitle, bIsInSameTitle);
			}

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblGetFriendPresenceTask
///////////////////////////////////////////////////////////////////////////////
class rlXblGetFriendPresenceTask : public rlXblHttpBatchPresenceTask
{
private:
	rlFriend* m_FriendsPtr;
	unsigned m_NumFriends;
	int m_Flags;

public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(rlXblGetFriendPresenceTask);

	rlXblGetFriendPresenceTask() {}
	virtual ~rlXblGetFriendPresenceTask() {}

	bool Configure(const int localGamerIndex, rlFriend* friendsPtr, unsigned numFriends, int flags)
	{
		bool success = false;

		rtry
		{
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rverify(friendsPtr, catchall,);
			rverify(numFriends, catchall,);

			m_FriendsPtr = friendsPtr;
			m_NumFriends = numFriends;
			m_Flags = flags;

			rverify(m_NumFriends > 0, catchall, );

			u64* xuids = (u64*)Alloca(u64, m_NumFriends);
			for(unsigned i = 0; i < m_NumFriends; i++)
			{
				xuids[i] = friendsPtr[i].GetXuid();
			}

			// We need the title information to determine the title ID the user is in
			rlXblHttpBatchPresenceTask::PresenceSettings settings;
			settings = rlXblHttpBatchPresenceTask::PresenceSettings::TITLE;

			// Should we filter for online
			bool bFilterForOnline = (m_Flags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE) > 0 || 
				(m_Flags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE) > 0;

			// pass 0 for titleId will not filter title Id
			unsigned titleId = 0;
			if (m_Flags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE)
			{
				// get our titleID from the service config
				titleId = g_rlTitleId->m_XblTitleId.GetTitleId();
			}

			rverify(rlXblHttpBatchPresenceTask::Configure(localGamerIndex, xuids, m_NumFriends, titleId, bFilterForOnline, settings), catchall, );

			success = true;
		}
		rcatchall
		{
			success = false;
		}

		return success;
	}

	bool ProcessUserState(u64 xuid, const char* state, bool bIsInSameTitle, bool /*bIsActive*/, int& resultCode)
	{
		bool success = false;

		rtry
		{
			resultCode = 0;
			rverify(state && state[0] != '\0', catchall, );

			// find the user with this id
			rlFriend* pFriend = NULL;
			int friendIndex = 0;
			for(unsigned i = 0; i < m_NumFriends; ++i)
			{
				rlFriend* tempFriend = &m_FriendsPtr[i];
				if(tempFriend == NULL)
				{
					continue;
				}

				if(tempFriend->GetXuid() == xuid)
				{
					pFriend = tempFriend;
					friendIndex = i;
					break;
				}
			}

			// Update our friend's online status
			if(pFriend)
			{
				// get online status
				bool bIsOnline = (stricmp(state, "online") == 0);

				// set the online status
				pFriend->SetIsInSameTitle(bIsInSameTitle);
				pFriend->SetIsOnline(bIsOnline);
			}

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}
};

class FriendsReaderTask : public netTask
{
public:

    NET_TASK_DECL(FriendsReaderTask);
    NET_TASK_USE_CHANNEL(rline_xbl);

    FriendsReaderTask()
        : m_LocalGamerIndex(-1)
        , m_MaxFriends(0)
        , m_FirstFriendIndex(0)
        , m_FriendsPtr(NULL)
        , m_NumFriendsPtr(NULL)
		, m_TotalFriends(NULL)
		, m_RelationState(STATE_NONE)
		, m_FriendInfoState(STATE_NONE)
		, m_PresenceState(STATE_NONE)
		, m_FriendCount(0)
    {
		
    }

    bool Configure(const int localGamerIndex,
					const unsigned firstFriendIndex,
					rlFriend friends[],
					const unsigned maxFriends,
					unsigned* numFriends,
					unsigned* totalFriends,
					int friendFlags)
    {
		rtry
		{
			rverify(numFriends, catchall, rlError("numFriends is NULL"));
			rverify(totalFriends, catchall, rlError("totalFriends is NULL"));
			rverify(maxFriends <= rlFriendsPage::MAX_FRIEND_PAGE_SIZE, catchall, rlError("FriendsReader can only read rlFriendsPage::MAX_FRIEND_PAGE_SIZE simultaneously"));

			m_LocalGamerIndex = localGamerIndex;
			m_FirstFriendIndex = firstFriendIndex;
			m_FriendsPtr = friends;
			m_MaxFriends = maxFriends;
			m_NumFriendsPtr = numFriends;
			*m_NumFriendsPtr = 0;
			m_FriendFlags = friendFlags;
			m_TotalFriends = totalFriends;
			
			// Set initial state
			m_RelationState = STATE_GET_INFO;
			m_FriendInfoState = STATE_NONE;
			m_PresenceState = STATE_NONE;
			
		}
		rcatchall
		{

		}

        return true;
    }

    virtual void OnCancel()
    {
        netTaskDebug("Canceled while in state Relation State %d, Info Stats %d, PresenceState %d", 
						m_RelationState, m_FriendInfoState, m_PresenceState);

		XBLPLAYER_CANCEL_TASK(m_RelationshipStatus);
		XBLPLAYER_CANCEL_TASK(m_FriendInfoStatus);
		XBLPLAYER_CANCEL_TASK(m_PresenceStatus);
    }

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		// Step 1: Getting a list of social contacts
		if (!UpdateGetRelationships())
		{
			status = NET_TASKSTATUS_FAILED;
		}

		// Step 2: From the social contact list, retrieve user presence
		if (!UpdateGetPresence())
		{
			status = NET_TASKSTATUS_FAILED;
		}

		// Step 3 (can be done in parallel if we are not filtering friends): retrieve friend's gamer name
		if (!UpdateGetFriendInfo())
		{
			status = NET_TASKSTATUS_FAILED;
		}

		if (m_PresenceState == STATE_FINISHED && m_FriendInfoState == STATE_FINISHED && m_RelationState == STATE_FINISHED)
		{
			status = NET_TASKSTATUS_SUCCEEDED;
		}

		if(NET_TASKSTATUS_PENDING != status)
		{
			// Cancel (if necessary) any outstanding tasks
			XBLPLAYER_CANCEL_TASK(m_PresenceStatus);
			XBLPLAYER_CANCEL_TASK(m_FriendInfoStatus);
			XBLPLAYER_CANCEL_TASK(m_RelationshipStatus);

			this->Complete(status);
		}

		return status;
	}

private:

	enum State
	{
		STATE_NONE,
		STATE_GET_INFO,
		STATE_GETTING_INFO,
		STATE_FINISHED,
		STATE_ERROR
	};

	bool UpdateGetRelationships()
	{
		XBLPLAYER_UPDATE_TASK(m_RelationState, m_RelationshipRequestStatus, m_RelationshipStatus, GetRelationships, ProcessRelationships);
	}

	bool UpdateGetFriendInfo()
	{
		XBLPLAYER_UPDATE_TASK(m_FriendInfoState, m_FriendInfoRequestStatus, m_FriendInfoStatus, GetFriendInfo, ProcessFriendInfo);
	}

	bool UpdateGetPresence()
	{
		XBLPLAYER_UPDATE_TASK(m_PresenceState, m_PresenceRequestStatus, m_PresenceStatus, GetPresence, ProcessPresence);
	}

	// Create an async request of the user's social relationships
	bool GetRelationships()
	{
		bool success = false;

		netTaskDebug("Reading friends for gamer: %d...", m_LocalGamerIndex);
		netTaskDebug("	First Friend Index: %d", m_FirstFriendIndex);
		netTaskDebug("	Max Friends: %d", m_MaxFriends);
		netTaskDebug("	FRIENDS_ALL: %d", m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ALL ? 1 : 0);
		netTaskDebug("	FRIENDS_ONLINE: %d", m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE ? 1 : 0);
		netTaskDebug("	FRIENDS_ONLINE_IN_TITLE: %d", m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE ? 1 : 0);
		netTaskDebug("	FRIENDS_FAVORITES: %d", m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_FAVORITES ? 1 : 0);
		netTaskDebug("	FRIENDS_TWOWAY: %d", m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_TWOWAY ? 1 : 0);
		netTaskDebug("	FRIENDS_PRESORT_ID: %d", m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_PRESORT_ID ? 1 : 0);
		netTaskDebug("	FRIENDS_PRESORT_ONLINE: %d", m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_PRESORT_ONLINE ? 1 : 0);

		try
		{
			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			if (context == nullptr)
			{
				return false;
			}

			// Can filter for favorites or all users
			SocialRelationship relationship;
			if (m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_FAVORITES)
			{
				relationship = SocialRelationship::Favorite;
			}
			else
			{
				relationship = SocialRelationship::All;
			}

			// Retrieve a list of the user's social contacts
			success = m_RelationshipRequestStatus.BeginOp<XboxSocialRelationshipResult^>("GetSocialRelationshipsAsync", 
				context->SocialService->GetSocialRelationshipsAsync(relationship, m_FirstFriendIndex, m_MaxFriends),
				DEFAULT_ASYNC_TIMEOUT,
				this,
				&m_RelationshipStatus);
		}
		catch (Platform::Exception^ e)
		{

		}

		return success;
	}

	// Create an async request for the user's friend details
	bool GetFriendInfo()
	{
		XBLPLAYER_CREATE_TASK(rlXblGetFriendInfoTask, m_LocalGamerIndex, m_XblFriends, m_FriendCount, &m_FriendInfoStatus);
	}

	// Create an async request for the user's presence
	bool GetPresence()
	{
		XBLPLAYER_CREATE_TASK(rlXblGetFriendPresenceTask, m_LocalGamerIndex, m_XblFriends, m_FriendCount, m_FriendFlags, &m_PresenceStatus);
	}

	bool ProcessRelationships()
	{
		try
		{

			XboxSocialRelationshipResult^ result = m_RelationshipRequestStatus.GetResults<XboxSocialRelationshipResult^>();
			if (!rlVerify(result != nullptr))
				return false;

			auto relationships = result->Items;
			if (!rlVerify(relationships != nullptr))
				return false;

			// return the total friends list size as the result size of the social contacts list
			*m_TotalFriends = relationships->Size;

			// cache the total friends list size so we can check for changes
			s_FriendsListSize[m_LocalGamerIndex] = relationships->Size;

			netTaskDebug("XboxSocialRelationshipResult: %d relationships...", *m_TotalFriends);

			unsigned friendCount = 0;
			for(XboxSocialRelationship^ socialContact : relationships)
			{
				if (friendCount > m_MaxFriends)
					break;

				u64 xuid = 0;
				// parse the user's XUID and filter favorites/2-way relationships
				if(socialContact != nullptr &&
					socialContact->XboxUserId != nullptr &&
					swscanf_s(socialContact->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1)
				{
					bool bIsFavorite = socialContact->IsFavorite;
					bool bIsTwoWay = socialContact->IsFollowingCaller;

					// Check if friend is favorite
					if (m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_FAVORITES && 
						!bIsFavorite)
					{	
						netTaskDebug3("Social relationship for %" I64FMT "u skipped because not FAVORITE.", xuid);
						continue;
					}

					// Two-Way friends
					if (m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_TWOWAY && 
						!bIsTwoWay)
					{
						netTaskDebug3("Social relationship for %" I64FMT "u skipped because not TWO-WAY.", xuid);
						continue;
					}

					rlXblFriendInfo xblFriendInfo;
					xblFriendInfo.m_bIsFavorite = bIsFavorite;

					rlFriend xblFriend;
					xblFriend.Reset(xuid, xblFriendInfo, false, false, bIsTwoWay);
					m_XblFriends[friendCount] = xblFriend;
					friendCount++;
				}
			}

			// Determine whether we're filtering for online friends only
			bool bCheckForOnline = m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE || 
				m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE;

			// Set the friend count to the number of social contacts successfully imported
			m_FriendCount = friendCount;

			// If we have friends, retrieve their details. Otherwise, we're done.
			if(m_FriendCount > 0)
			{
				m_PresenceState = STATE_GET_INFO;
				if (!bCheckForOnline)
				{
					m_FriendInfoState = STATE_GET_INFO;
				}
			}
			else
			{
				// no friends to search info for
				m_PresenceState = STATE_FINISHED;
				m_FriendInfoState = STATE_FINISHED;
			}

			return true;
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			return false;
		}
	}

	void ProcessFriendInfo()
	{
		m_FriendInfoState = STATE_FINISHED;
	}

	void ProcessPresence()
	{
		m_PresenceState = STATE_FINISHED;

		// Online filtering requires presence to be finished before getting friend info. This allows us
		// to filter online friends FIRST and then get their display names
		bool bCheckForOnline = m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE || 
			m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE;

		// If check for online is enabled, get friend info
		if (bCheckForOnline)
		{
			if (m_FriendCount == 0)
			{
				m_FriendInfoState = STATE_FINISHED;
			}
			else
			{
				m_FriendInfoState = STATE_GET_INFO;
			}
		}
	}

	void Complete(const netTaskStatus status)
	{
		if(NET_TASKSTATUS_SUCCEEDED == status && !WasCanceled())
		{
			// Iterate through all friends, and apply any filters that we need to apply.
			int friendCount = 0;
			for (unsigned i = 0; i < m_FriendCount; i++)
			{
				if (m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE)
				{
					// Filter offline friends
					if (m_XblFriends[i].IsOnline())
					{
						m_FriendsPtr[friendCount] = m_XblFriends[i];
						friendCount++;
					}
					else
					{
						netTaskDebug3("Social relationship for %s skipped because not ONLINE.", m_XblFriends[i].GetName());
					}
				}
				else if (m_FriendFlags & rlFriendsReader::FriendReaderType::FRIENDS_ONLINE_IN_TITLE)
				{
					// Filter offline friends or online friends out of context
					if (m_XblFriends[i].IsOnline() && m_XblFriends[i].IsInSameTitle())
					{
						m_FriendsPtr[friendCount] = m_XblFriends[i];
						friendCount++;
					}
					else
					{
						netTaskDebug3("Social relationship for %s skipped because not ONLINE IN SAME TITLE.", m_XblFriends[i].GetName());
					}
				}
				else
				{
					netTaskDebug("Friend Read Succeeded, Name=%s, IsOnline=%s, IsInSameTitle=%s, IsFavorite=%s,"
						, m_XblFriends[i].GetName()
						, m_XblFriends[i].IsOnline() ? "true":"false"
						, m_XblFriends[i].IsInSameTitle() ? "true":"false"
						, m_XblFriends[i].IsFavorite() ? "true":"false");

					// Otherwise, add all friends
					m_FriendsPtr[friendCount] = m_XblFriends[i];
					friendCount++;
				}
			}

			netTaskDebug("Read %d friends, returning %d", m_FriendCount, friendCount);
			*m_NumFriendsPtr = friendCount;
		}
	}

private:

    State m_RelationState;
	State m_PresenceState;
	State m_FriendInfoState;

    int m_LocalGamerIndex;
	
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_RelationshipRequestStatus;
	netStatus m_RelationshipStatus;

	netXblStatus<Windows::Foundation::IAsyncInfo^> m_FriendInfoRequestStatus;
	netStatus m_FriendInfoStatus;

	netXblStatus<Windows::Foundation::IAsyncInfo^> m_PresenceRequestStatus;
	netStatus m_PresenceStatus;

    unsigned m_MaxFriends;
    unsigned m_FirstFriendIndex;
    rlFriend* m_FriendsPtr;
    unsigned* m_NumFriendsPtr;
	unsigned* m_TotalFriends;
	unsigned m_FriendCount;

	rlFriend m_XblFriends[rlFriendsPage::MAX_FRIEND_PAGE_SIZE];

	int m_FriendFlags;
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblGetSocialRelationshipsTask
///////////////////////////////////////////////////////////////////////////////
class rlXblGetSocialRelationshipsTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblGetSocialRelationshipsTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblGetSocialRelationshipsTask();

	bool Configure(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();
	int m_LocalGamerIndex;

private:
	atArray<rlFriendsReference*>* m_FriendRefsByStatus;
	atArray<rlFriendsReference*>* m_FriendRefsById;
};

rlXblGetSocialRelationshipsTask::rlXblGetSocialRelationshipsTask()
	: m_LocalGamerIndex(-1)
	, m_FriendRefsByStatus(NULL)
	, m_FriendRefsById(NULL)
{

}

bool rlXblGetSocialRelationshipsTask::Configure(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById)
{
	if (rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)) && rlVerify(friendRefsByStatus) && rlVerify(friendRefsById))
	{
		m_LocalGamerIndex = localGamerIndex;
		m_FriendRefsByStatus = friendRefsByStatus;
		m_FriendRefsById = friendRefsById;
		return true;
	}

	return false;
}

bool rlXblGetSocialRelationshipsTask::DoWork()
{
	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		// Can filter for favorites or all users
		SocialRelationship relationship = SocialRelationship::All;

		// Retrieve a list of the user's social contacts
		return m_XblStatus.BeginOp<XboxSocialRelationshipResult^>("GetSocialRelationshipsAsync", context->SocialService->GetSocialRelationshipsAsync(relationship), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ e)
	{
		return false;
	}
}

void rlXblGetSocialRelationshipsTask::ProcessSuccess()
{
	try
	{

		XboxSocialRelationshipResult^ result = m_XblStatus.GetResults<XboxSocialRelationshipResult^>();
		if (!rlVerify(result != nullptr))
			return;

		auto relationships = result->Items;
		if (!rlVerify(relationships != nullptr))
			return;

		unsigned numFriends = relationships->Size;
		m_FriendRefsByStatus->Reset(true);
		m_FriendRefsByStatus->Reserve(numFriends);
		m_FriendRefsById->Reset(true);
		m_FriendRefsById->Reserve(numFriends);

		s_FriendsListSize[m_LocalGamerIndex] = numFriends;

		netTaskDebug("XboxSocialRelationshipResult: %d relationships...", numFriends);

		for(XboxSocialRelationship^ socialContact : relationships)
		{
			u64 xuid = 0;
			if(socialContact != nullptr &&
				socialContact->XboxUserId != nullptr &&
				swscanf_s(socialContact->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1)
			{
				rlFriendsReference* ref;
				ref = RL_ALLOCATE_T(rlXblGetSocialRelationshipsTask, rlFriendsReference);
				if (ref)
				{
					ref->Clear();
					ref->m_Id.m_Xuid = xuid;
					ref->m_bIsTwoWay = socialContact->IsFollowingCaller;
					m_FriendRefsByStatus->Push(ref);
					m_FriendRefsById->Push(ref);
				}
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblCheckFriendsListSizeTask
///////////////////////////////////////////////////////////////////////////////
class rlXblCheckFriendsListSizeTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblCheckFriendsListSizeTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblCheckFriendsListSizeTask();

	bool Configure(const int localGamerIndex);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();
	int m_LocalGamerIndex;
};


rlXblCheckFriendsListSizeTask::rlXblCheckFriendsListSizeTask()
	: m_LocalGamerIndex(-1)
{

}

bool rlXblCheckFriendsListSizeTask::Configure(const int localGamerIndex)
{
	if (rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
		m_LocalGamerIndex = localGamerIndex;
		return true;
	}

	return false;
}

void rlXblCheckFriendsListSizeTask::ProcessSuccess()
{
	try
	{

		XboxSocialRelationshipResult^ result = m_XblStatus.GetResults<XboxSocialRelationshipResult^>();
		if (!rlVerify(result != nullptr))
			return;

		auto relationships = result->Items;
		if (!rlVerify(relationships != nullptr))
			return;

		unsigned totalFriends = relationships->Size;

		netTaskDebug("XboxSocialRelationshipResult: %d relationships...", totalFriends);

		if (totalFriends > s_FriendsListSize[m_LocalGamerIndex])
		{
			rlDebug3("rlXblCheckFriendsListSizeTask completed: we've gained %d friend(s)", totalFriends - s_FriendsListSize[m_LocalGamerIndex]);
			for(XboxSocialRelationship^ socialContact : relationships)
			{
				u64 xuid = 0;
				// parse the user's XUID and filter favorites/2-way relationships
				if(socialContact != nullptr &&
					socialContact->XboxUserId != nullptr &&
					swscanf_s(socialContact->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1)
				{
					rlGamerHandle gh;
					gh.ResetXbl(xuid);

					if (!rlFriendsManager::IsFriendsWith(m_LocalGamerIndex, gh))
					{
						rlFriendId id(gh);
						netTaskDebug("Adding new friend: %" I64FMT "d", xuid);
						rlFriendsManager::AddFriendReferenceById(id);
					}
				}
			}

			rlXblFriendListChangedEvent e(m_LocalGamerIndex);
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else if (totalFriends < s_FriendsListSize[m_LocalGamerIndex])
		{
			rlDebug3("rlXblCheckFriendsListSizeTask completed: we've lost %d friend(s)", s_FriendsListSize[m_LocalGamerIndex] - totalFriends);
			unsigned totalNumFriends = rlFriendsManager::GetTotalNumFriends(m_LocalGamerIndex);
			for (int i = totalNumFriends-1; i >= 0; i--)
			{
				rlGamerHandle gh;
				rlFriendsManager::GetGamerHandle(i, &gh);

				bool bFound = false;

				if (gh.IsValid())
				{
					for(XboxSocialRelationship^ socialContact : relationships)
					{
						u64 xuid = 0;
						// parse the user's XUID and filter favorites/2-way relationships
						if(swscanf_s(socialContact->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1)
						{
							if (xuid == gh.GetXuid())
							{
								bFound = true;
								break;
							}
						}
					}

				}

				if (!bFound)
				{
					netTaskDebug("Removing friend (%" I64FMT "u) at index (%d)", gh.GetXuid(), i);
					rlFriendsManager::RemoveFriendReferenceByIndex(i);
				}
			}

			rlXblFriendListChangedEvent e(m_LocalGamerIndex);
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else
		{
			rlDebug3("rlXblCheckFriendsListSizeTask completed with no friends list changes");
		}

		s_FriendsListSize[m_LocalGamerIndex] = totalFriends;
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}
}

bool rlXblCheckFriendsListSizeTask::DoWork()
{
	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		// Can filter for favorites or all users
		SocialRelationship relationship = SocialRelationship::All;

		// Retrieve a list of the user's social contacts
		return m_XblStatus.BeginOp<XboxSocialRelationshipResult^>("GetSocialRelationshipsAsync", context->SocialService->GetSocialRelationshipsAsync(relationship), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ e)
	{
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblGetAvoidListTask
///////////////////////////////////////////////////////////////////////////////
class rlXblGetAvoidListTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblGetAvoidListTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblGetAvoidListTask();

	bool Configure(int localGamerIndex, PlayerList* playerList);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();

	int m_LocalGamerIndex;
	PlayerList* m_PlayerList;
};

rlXblGetAvoidListTask::rlXblGetAvoidListTask()
	: m_LocalGamerIndex(-1)
	, m_PlayerList(NULL)
{

}

bool rlXblGetAvoidListTask::Configure(int localGamerIndex, PlayerList* playerList)
{
	if (!rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
		return false;
	}

	if (!rlVerify(playerList))
	{
		return false;
	}

	m_LocalGamerIndex = localGamerIndex;
	m_PlayerList = playerList;

	return true;
}

void rlXblGetAvoidListTask::ProcessSuccess()
{
	try
	{
		m_PlayerList->m_Players.Reset();

		IVectorView<String^>^ playerList = m_XblStatus.GetResults<IVectorView<String^>^>();

		if (playerList != nullptr && playerList->Size > 0)
		{
			for (unsigned i = 0; i < playerList->Size && i < m_PlayerList->m_Limit; i++)
			{
				netTaskDebug("[%u] XUID %ls is in avoid list", i, playerList->GetAt(i)->Data());
				u64 xuid = 0;
				swscanf_s(playerList->GetAt(i)->Data(), L"%" LI64FMT L"u", &xuid);
				m_PlayerList->m_Players.PushAndGrow(xuid);
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}
}

bool rlXblGetAvoidListTask::DoWork()
{
	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}	

		// Retrieve a list of the user's social contact
		return m_XblStatus.BeginOp<IVectorView<String^>^>("GetAvoidListAsync", context->PrivacyService->GetAvoidListAsync(), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ e)
	{
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblGetMuteListTask
///////////////////////////////////////////////////////////////////////////////
class rlXblGetMuteListTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblGetMuteListTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	rlXblGetMuteListTask();

	bool Configure(int localGamerIndex, PlayerList* playerList);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();

	int m_LocalGamerIndex;
	PlayerList* m_PlayerList;
};

rlXblGetMuteListTask::rlXblGetMuteListTask()
	: m_LocalGamerIndex(-1)
	, m_PlayerList(NULL)
{

}

bool rlXblGetMuteListTask::Configure(int localGamerIndex, PlayerList* playerList)
{
	if (!rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
		return false;
	}

	if (!rlVerify(playerList))
	{
		return false;
	}

	m_LocalGamerIndex = localGamerIndex;
	m_PlayerList = playerList;

	return true;
}

void rlXblGetMuteListTask::ProcessSuccess()
{
	try
	{
		m_PlayerList->m_Players.Reset();

		IVectorView<String^>^ playerList = m_XblStatus.GetResults<IVectorView<String^>^>();

		if (playerList != nullptr && playerList->Size > 0)
		{
			for (unsigned i = 0; i < playerList->Size && i < m_PlayerList->m_Limit; i++)
			{
				netTaskDebug("[%u] XUID %ls is in mute list", i, playerList->GetAt(i)->Data());
				u64 xuid = 0;
				swscanf_s(playerList->GetAt(i)->Data(), L"%" LI64FMT L"u", &xuid);
				m_PlayerList->m_Players.PushAndGrow(xuid);
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}
}

bool rlXblGetMuteListTask::DoWork()
{
	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}	

		// Retrieve a list of the user's social contact
		return m_XblStatus.BeginOp<IVectorView<String^>^>("GetAvoidListAsync", context->PrivacyService->GetMuteListAsync(), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ e)
	{
		return false;
	}
}

//////////////////////////////////////////////////////////////////////
// rlXblPlayers
//////////////////////////////////////////////////////////////////////
bool rlXblPlayers::GetFriends(const int localGamerIndex,
					const unsigned firstFriendIndex,
					rlFriend friends[],
					const unsigned maxFriends,
					unsigned* numFriends,
					unsigned* totalFriends,	
					int friendFlags,
					netStatus* status)
{
 	bool success = false;

	FriendsReaderTask* task;
	if(!netTask::Create(&task, status)
		|| !task->Configure(localGamerIndex, firstFriendIndex, friends, maxFriends, numFriends, totalFriends, friendFlags)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

	return success;
}

bool rlXblPlayers::UpdateFriends(const int localGamerIndex, rlFriendsPage* page, unsigned startIndex, unsigned numFriends, netStatus* status)
{
	rlXblUpdateFriendsPresenceAndSessionsTask* task = NULL;

	rtry												
	{										
		rverify(netTask::Create(&task, status), catchall, );																								
		rverify(task->Configure(localGamerIndex, page, startIndex, numFriends, rlFriendsReader::FRIENDS_ALL), catchall, );	
		rverify(netTask::Run(task), catchall, );			
		return true;										
	}													
	rcatchall											
	{													
		if(task != NULL)									
		{													
			netTask::Destroy(task);								
		}		
		return false;										
	}
}

bool rlXblPlayers::UpdateFriendsPresence(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status)
{
	XBLPLAYER_CREATE_TASK(rlXblUpdateFriendPresenceTask, localGamerIndex, friends, numFriends, rlFriendsReader::FRIENDS_ALL, status);
}

bool rlXblPlayers::GetFriendInfo(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status)
{
	XBLPLAYER_CREATE_TASK(rlXblGetFriendInfoTask, localGamerIndex, friends, numFriends, status);
}

//PURPOSE
// Retrieves the list of social relationships.
bool rlXblPlayers::GetSocialRelationships(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, netStatus* status)
{
	CREATE_NET_XBLTASK(rlXblGetSocialRelationshipsTask, status, localGamerIndex, friendRefsByStatus, friendRefsById);
}

bool rlXblPlayers::GetPeoplePresence(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, netStatus* status)
{
	XBLPLAYER_CREATE_TASK(rlXblHttpGetPresenceGroupTask, localGamerIndex, friendRefsByStatus, friendRefsById, rlXblHttpGetPresenceGroupTask::PEOPLE, rlXblHttpGetPresenceGroupTask::ALL, status);
}

// PURPOSE
// Checks the size of the friend's list for changes
bool rlXblPlayers::CheckForListChanges(const int localGamerIndex, netStatus* status)
{
	CREATE_NET_XBLTASK(rlXblCheckFriendsListSizeTask, status, localGamerIndex);
}

// PURPOSE
// Gets the session status for a list of friends
bool rlXblPlayers::GetFriendsSessions(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status)
{
	CREATE_NET_XBLTASK(rlXblGetFriendsSessionsTask, status, localGamerIndex, friends, numFriends);
}

// PURPOSE
// Gets the session status for a list of friends
bool rlXblPlayers::GetFriendsPresenceSessions(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status)
{
	CREATE_NET_XBLTASK(rlXblGetFriendsPresenceSessionsTask, status, localGamerIndex, friends, numFriends);
}

// PURPOSE
// Gets the session status for a list of friends
bool rlXblPlayers::GetAvoidList(const int localGamerIndex, PlayerList* playerList, netStatus* status)
{
	CREATE_NET_XBLTASK(rlXblGetAvoidListTask, status, localGamerIndex, playerList);
}

// PURPOSE
// Gets the session status for a list of friends
bool rlXblPlayers::GetMuteList(const int localGamerIndex, PlayerList* playerList, netStatus* status)
{
	CREATE_NET_XBLTASK(rlXblGetMuteListTask, status, localGamerIndex, playerList);
}

} // namespace rage

#endif  //RSG_DURANGO
