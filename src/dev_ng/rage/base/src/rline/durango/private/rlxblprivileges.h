// 
// rlxblprivileges.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PRIVILEGES_H
#define RLXBL_PRIVILEGES_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblprivileges.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblprivileges_interface.h instead"
#endif

#include "../../rlprivileges.h"
#include "../rlxblprivileges_interface.h"
#include "net/status.h"

using namespace Windows::Xbox::ApplicationModel;

namespace rage
{
//PURPOSE
//  Interface to Xbox Live privileges on Durango.
class rlXblPrivileges : public IPrivileges
{
public:
	rlXblPrivileges();
	virtual ~rlXblPrivileges();

	//PURPOSE
	//  Initialize the class.
	bool Init();

	//PURPOSE
	//  Shut down the class.
	void Shutdown();

	//PURPOSE
	//  Update the class. Should be called once per frame.
	void Update();

	//PURPOSE
	// Begin an asynchronous check to see if the specified local user has the specified privileges.
	bool CheckPrivileges(const int localGamerIndex, const int privilegeTypeBitfield, const bool attemptResolution);

	//PURPOSE
	// Check if the current privilege check has a result.
	bool IsPrivilegeCheckResultReady() const { return	(m_Status.GetStatus() == netStatus::NET_STATUS_SUCCEEDED) ||
														(m_Status.GetStatus() == netStatus::NET_STATUS_FAILED) ||
														(m_Status.GetStatus() == netStatus::NET_STATUS_CANCELED); }

	//PURPOSE
	// Check if a privilege check is in progress.
	bool IsPrivilegeCheckInProgress() const { return m_Status.GetStatus() != netStatus::NET_STATUS_NONE; }

	//PURPOSE
	// Check if the current privilege check was successful.
	bool IsPrivilegeCheckSuccessful() const { return m_PrivilegeCheckResult == Store::PrivilegeCheckResult::NoIssue; }

	//PURPOSE
	// Set that the privilege check result is no longer needed. 
	void SetPrivilegeCheckResultNotNeeded();

	//PURPOSE
	// Returns the result of the Asynch call privilege check. 
	ePrivilegeCheckResult GetPrivilegeCheckResult() const;

	//PURPOSE
	// Return TRUE if a privilege is granted. This is a faster call and the result
	//  can only be used if the return value is TRUE.
	bool HasPrivilege(const int localGamerIndex, const int privilegeId) const;

	//PURPOSE
	// Cancels the current privilege task.
	void OnSignOut();

private:
	//PURPOSE
	//  Reset the current privilege check.
	void ResetPrivilegeCheck();

	//PURPOSE
	//  Return the corresponding XDK privilege code for a given privilege type.
	int rlXblPrivileges::GetPrivilegeCode(const int privilegeType) const;

	Store::PrivilegeCheckResult m_PrivilegeCheckResult;
	netStatus m_Status;
	bool m_CurrentResultNotNeeded;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PRIVILEGES_H
