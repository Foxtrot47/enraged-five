// 
// rlxblrealtimeactivity.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlxblrealtimeactivity.h"

#include "atl/functor.h"
#include "diag/seh.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "net/durango/xbltask.h"
#include "rline/rlachievement.h"
#include "rline/rldiag.h"
#include "rline/rltitleid.h"
#include "system/timer.h"

extern __THREAD int RAGE_LOG_DISABLE;

using namespace Windows::Foundation;
using namespace Microsoft::Xbox::Services::RealTimeActivity;

PARAM(noRTA, "Disables the Realtime Activity Service");

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, xblrealtimeactivity);
#undef __rage_channel
#define __rage_channel rline_xblrealtimeactivity

sysCriticalSectionToken rlXblRealTimeActivity::m_QueueCs;

////////////////////////////////////////
// rlXblRtaConnectionTask
////////////////////////////////////////
class rlXblRtaConnectionTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblRtaConnectionTask);
    NET_TASK_USE_CHANNEL(rline_xblrealtimeactivity);

	rlXblRtaConnectionTask();
	bool Configure(XboxLiveContext^ context);

protected:
	virtual bool DoWork();
	XboxLiveContext^ m_Context;
};

rlXblRtaConnectionTask::rlXblRtaConnectionTask()
	: m_Context(nullptr)
{

}

bool rlXblRtaConnectionTask::Configure(XboxLiveContext^ context)
{
	rtry
	{
		rverify(context != nullptr, catchall, );
		m_Context = context;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXblRtaConnectionTask::DoWork()
{
	try
	{
		rtry
		{
			rverify(m_Context->RealTimeActivityService != nullptr, catchall, );
			return m_XblStatus.BeginAction("ConnectAsync", m_Context->RealTimeActivityService->ConnectAsync(), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
		}
		rcatchall
		{

		}
	}
	catch(Platform::Exception^ ex)
	{
		rlError("Failed to Connect to Realtime Activity. Exception 0x%08x, Message: %ls", ex->HResult, ex->Message != nullptr ? ex->Message->Data() : L"NULL");
	}

	return false;
}

////////////////////////////////////////
// rlXblRtaSubscribeTask
////////////////////////////////////////
class rlXblRtaSubscribeTask : public netXblTask
{
public:

	NET_TASK_DECL(rlXblRtaSubscribeTask);
	NET_TASK_USE_CHANNEL(rline_xblrealtimeactivity);

	rlXblRtaSubscribeTask();

	bool Configure(const rlFriend* pFriend, rlXBlRtaConnection* m_Connection);
	bool Configure(const u64 xuid, rlXBlRtaConnection* m_Connection);

protected:

	virtual bool DoWork() = 0; 

	u64 m_Xuid;
	rlXBlRtaConnection* m_Connection;

	enum State
	{
		STATE_SUBSCRIBE,
		STATE_WAIT
	};

	State m_State;
};

rlXblRtaSubscribeTask::rlXblRtaSubscribeTask()
	: m_Xuid(0)
	, m_Connection(NULL)
	, m_State(STATE_SUBSCRIBE)
{
}

bool rlXblRtaSubscribeTask::Configure(const rlFriend* pFriend, rlXBlRtaConnection* connection)
{
	rtry
	{
		rverify(pFriend, catchall, );
		rverify(connection, catchall, );

		netTaskDebug("Subscribing to friend %" I64FMT "u", pFriend->GetXuid());

		m_Xuid = pFriend->GetXuid();
		m_Connection = connection;

		m_State = STATE_SUBSCRIBE;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXblRtaSubscribeTask::Configure(u64 xuid, rlXBlRtaConnection* connection)
{
	rtry
	{
		rverify(xuid, catchall, );
		rverify(connection, catchall, );

		netTaskDebug("Subscribing to xuid %" I64FMT "u", xuid);

		m_Xuid = xuid;
		m_Connection = connection;

		m_State = STATE_SUBSCRIBE;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

////////////////////////////////////////
// rlXblRtaSubscribeToDevicePresenceTask
////////////////////////////////////////
class rlXblRtaSubscribeToDevicePresenceTask : public rlXblRtaSubscribeTask
{
public:

	NET_TASK_DECL(rlXblRtaSubscribeToDevicePresenceTask);
	NET_TASK_USE_CHANNEL(rline_xblrealtimeactivity);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();
};


void rlXblRtaSubscribeToDevicePresenceTask::ProcessSuccess()
{
	try
	{
		RealTimeActivityDevicePresenceChangeSubscription^ sub = m_XblStatus.GetResults<RealTimeActivityDevicePresenceChangeSubscription^>();
		rlXblRtaDeviceSubscription* pSub = (rlXblRtaDeviceSubscription*)rlXBlRtaConnection::CreateSubscription(m_Xuid, SubscriptionType::SUB_DEVICE);
		if (pSub && sub != nullptr)
		{
			netTaskDebug3("Successfully subscribed to device presence for %" I64FMT "d", pSub->m_Xuid);
			pSub->deviceSubscription = sub;
			m_Connection->AddSubscription(pSub);
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}
}

bool rlXblRtaSubscribeToDevicePresenceTask::DoWork()
{
	try
	{
		rtry
		{
			rverify(m_Connection, catchall, );
			rverify(m_Connection->GetContext(), catchall, );

			String^ userId = rlXblCommon::XuidToWinRTString(m_Xuid);

			m_XblStatus.BeginOp<RealTimeActivityDevicePresenceChangeSubscription^>("SubscribeToDevicePresenceChangeAsync", 
				m_Connection->GetContext()->RealTimeActivityService->SubscribeToDevicePresenceChangeAsync(userId),
				DEFAULT_ASYNC_TIMEOUT,
				this,
				&m_MyStatus);

			return true;
		}
		rcatchall
		{
			return false;
		}	
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}
}

////////////////////////////////////////
// rlXblRtaUnsubscribeToDevicePresenceTask
////////////////////////////////////////
class rlXblRtaUnsubscribeToDevicePresenceTask : public rlXblRtaSubscribeTask
{
public:

	NET_TASK_DECL(rlXblRtaUnsubscribeToDevicePresenceTask);
	NET_TASK_USE_CHANNEL(rline_xblrealtimeactivity);

protected:
	virtual void ProcessSuccess();
	virtual void ProcessFailure();
	virtual bool DoWork();

};

void rlXblRtaUnsubscribeToDevicePresenceTask::ProcessSuccess()
{
	rlXblRtaSubscription* pSub = m_Connection->GetSubscription(m_Xuid, SUB_DEVICE);
	if (pSub)
	{
		netTaskDebug3("Successfully unsubscribed to device presence for %" I64FMT "d", pSub->m_Xuid);
		m_Connection->DeleteSubscription(pSub);
	}
}

void rlXblRtaUnsubscribeToDevicePresenceTask::ProcessFailure()
{
	rlXblRtaSubscription* pSub = m_Connection->GetSubscription(m_Xuid, SUB_DEVICE);
	if (pSub)
	{
		netTaskDebug3("Failed to unsubscribe to device presence for %" I64FMT "d, removing from list.", pSub->m_Xuid);
		m_Connection->DeleteSubscription(pSub);
	}
}

bool rlXblRtaUnsubscribeToDevicePresenceTask::DoWork()
{
	try
	{
		rtry
		{
			unsigned titleId = g_rlTitleId->m_XblTitleId.GetTitleId();
			rverify(titleId > 0, catchall, );

			rverify(m_Connection, catchall, );
			rverify(m_Connection->GetContext(), catchall, );

			rlXblRtaDeviceSubscription* pSub = (rlXblRtaDeviceSubscription*)m_Connection->GetSubscription(m_Xuid, SUB_DEVICE);
			if (pSub)
			{
				m_XblStatus.BeginAction("UnsubscribeFromDevicePresenceChangeAsync", 
					m_Connection->GetContext()->RealTimeActivityService->UnsubscribeFromDevicePresenceChangeAsync(pSub->deviceSubscription),
					DEFAULT_ASYNC_TIMEOUT,
					this,
					&m_MyStatus);
			}
			else
			{
				m_MyStatus.ForceSucceeded();
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}
	catch (...)
	{
		return false;
	}
}

////////////////////////////////////////
// rlXblRtaSubscribeToTitlePresenceTask
////////////////////////////////////////
class rlXblRtaSubscribeToTitlePresenceTask : public rlXblRtaSubscribeTask
{
public:

	NET_TASK_DECL(rlXblRtaSubscribeToTitlePresenceTask);
	NET_TASK_USE_CHANNEL(rline_xblrealtimeactivity);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();

};

void rlXblRtaSubscribeToTitlePresenceTask::ProcessSuccess()
{
	try
	{
		RealTimeActivityTitlePresenceChangeSubscription^ sub = m_XblStatus.GetResults<RealTimeActivityTitlePresenceChangeSubscription^>();
		rlXblRtaTitleSubscription* pSub = (rlXblRtaTitleSubscription*)rlXBlRtaConnection::CreateSubscription(m_Xuid, SubscriptionType::SUB_TITLE);
		if (pSub  && sub != nullptr)
		{
			netTaskDebug3("Successfully subscribed to title presence for %" I64FMT "d", pSub->m_Xuid);
			pSub->titleSubscription = sub;
			m_Connection->AddSubscription(pSub);
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}
	
}

bool rlXblRtaSubscribeToTitlePresenceTask::DoWork()
{
	try
	{
		rtry
		{
			unsigned titleId = g_rlTitleId->m_XblTitleId.GetTitleId();
			rverify(titleId > 0, catchall, );

			rverify(m_Connection, catchall, );
			rverify(m_Connection->GetContext(), catchall, );

			String^ userId = rlXblCommon::XuidToWinRTString(m_Xuid);

			m_XblStatus.BeginOp<RealTimeActivityTitlePresenceChangeSubscription^>("SubscribeToTitlePresenceChangeAsync", 
				m_Connection->GetContext()->RealTimeActivityService->SubscribeToTitlePresenceChangeAsync(userId, titleId),
				DEFAULT_ASYNC_TIMEOUT,
				this,
				&m_MyStatus);

			return true;
		}
		rcatchall
		{
			return false;
		}	
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}
}

////////////////////////////////////////
// rlXblRtaUnsubscribeToTitlePresenceTask
////////////////////////////////////////
class rlXblRtaUnsubscribeToTitlePresenceTask : public rlXblRtaSubscribeTask
{
public:

	NET_TASK_DECL(rlXblRtaUnsubscribeToTitlePresenceTask);
	NET_TASK_USE_CHANNEL(rline_xblrealtimeactivity);

protected:
	virtual void ProcessSuccess();
	virtual void ProcessFailure();
	virtual bool DoWork();

};

void rlXblRtaUnsubscribeToTitlePresenceTask::ProcessSuccess()
{
	rlXblRtaSubscription* pSub = m_Connection->GetSubscription(m_Xuid, SUB_TITLE);
	if (pSub)
	{
		netTaskDebug3("Successfully unsubscribed to title presence for %" I64FMT "d", pSub->m_Xuid);
		m_Connection->DeleteSubscription(pSub);
	}
}

void rlXblRtaUnsubscribeToTitlePresenceTask::ProcessFailure()
{
	rlXblRtaSubscription* pSub = m_Connection->GetSubscription(m_Xuid, SUB_TITLE);
	if (pSub)
	{
		netTaskDebug3("Failed to unsubscribe to title presence for %" I64FMT "d, removing from list.", pSub->m_Xuid);
		m_Connection->DeleteSubscription(pSub);
	}
}

bool rlXblRtaUnsubscribeToTitlePresenceTask::DoWork()
{
	try
	{
		rtry
		{
			unsigned titleId = g_rlTitleId->m_XblTitleId.GetTitleId();
			rverify(titleId > 0, catchall, );

			rverify(m_Connection, catchall, );
			rverify(m_Connection->GetContext(), catchall, );

			rlXblRtaTitleSubscription* pSub = (rlXblRtaTitleSubscription*)m_Connection->GetSubscription(m_Xuid, SUB_TITLE);
			if (pSub)
			{
				m_XblStatus.BeginAction("UnsubscribeFromTitlePresenceChangeAsync", 
					m_Connection->GetContext()->RealTimeActivityService->UnsubscribeFromTitlePresenceChangeAsync(pSub->titleSubscription),
					DEFAULT_ASYNC_TIMEOUT,
					this,
					&m_MyStatus);
			}
			else
			{
				m_MyStatus.ForceSucceeded();
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}
	catch (...)
	{
		return false;
	}
}

////////////////////////////////////////
// rlXBlRtaConnection
////////////////////////////////////////
rlXBlRtaConnection::rlXBlRtaConnection()
	: m_XboxLiveContext(nullptr)
	, m_State(STATE_IDLE)
	, m_Xuid(0)
	, m_uLastConnectTime(0)
	, m_SignedOut(false)
	, m_Connected(false)
	, m_CallbacksRegistered(false)
{
}

rlXBlRtaConnection::~rlXBlRtaConnection()
{
	Terminate();
}


bool rlXBlRtaConnection::Init(const int localGamerIndex)
{
	rtry
	{
		m_XboxLiveContext = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(localGamerIndex);
		m_Xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localGamerIndex);
		rverify(m_XboxLiveContext != nullptr, catchall, );
		rverify(m_Xuid > 0, catchall, );

		m_State = STATE_IDLE;

		rlDebug3("Creating RTA connection for gamer index %d (XUID: %" I64FMT "u)", localGamerIndex, m_Xuid);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXBlRtaConnection::CanDestroy()
{
	if (m_Subscriptions.GetCount() > 0)
	{
		return false;
	}

	if (m_PresenceSubscriptionQueue.GetCount() > 0)
	{
		return false;
	}

	if (m_PresenceUnsubscriptionQueue.GetCount() > 0)
	{
		return false;
	}

	if (m_TitleStatus.Pending() || m_DeviceStatus.Pending())
	{
		return false;
	}

	if (m_State != STATE_SHUTDOWN && m_State != STATE_IDLE)
	{
		return false;
	}

	return true;
}

bool rlXBlRtaConnection::CanConnect()
{
	rtry
	{
		rlGamerHandle gh;
		gh.ResetXbl(m_Xuid);

		int gamerIndex = rlPresence::GetLocalIndex(gh);

		rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(gamerIndex), catchall, );
		rcheck(rlPresence::IsOnline(gamerIndex), catchall, );
		rcheck((sysTimer::GetSystemMsTime() - m_uLastConnectTime) > RECONNECT_INTERVAL_MS, catchall, );

		rlDebug3("Gamer Index %d is online, connecting to RealTimeActivity service.", gamerIndex);
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlXBlRtaConnection::Update()
{
	switch(m_State)
	{
	case STATE_IDLE:
		{
			if (CanConnect())
			{
				m_State = STATE_CONNECT;
			}
		}
		break;
	case STATE_CONNECT:
		if (RegisterCallbacks() && Connect())
		{
			m_State = STATE_CONNECTING;
		}
		else
		{
			ClearSubscriptionQueue();
			m_State = STATE_SHUTDOWN;
		}
		break;
	
	case STATE_CONNECTING:
		if (!m_ConnectionStatus.Pending())
		{
			if (m_ConnectionStatus.Succeeded())
			{
				m_Connected = true;
				m_State = STATE_ACTIVE;
			}
			else
			{
				ClearSubscriptionQueue();
				m_State = STATE_SHUTDOWN;
			}
		}
		break;
	case STATE_ACTIVE:
		ProcessPresenceQueue();
		break;
	case STATE_SHUTDOWN:
		ProcessShutdown();
		break;
	}
}

bool rlXBlRtaConnection::ProcessPresenceQueue()
{
	if (m_DeviceStatus.Pending() || m_TitleStatus.Pending())
	{
		return false;
	}

	if (m_PresenceUnsubscriptionQueue.GetCount() > 0)
	{
		u64 xuid = m_PresenceUnsubscriptionQueue.Pop();
		if (!rlVerify(xuid > 0))
		{
			return false;
		}

		if (!UnsubscribeToDevicePresence(xuid))
		{
			rlError("Failed to unsubscribe to device presence");
			return false;
		}

		if (!UnsubscribeToTitlePresence(xuid))
		{
			rlError("Failed to unsubscribe to title presence");
			return false;
		}
	}
	else if (m_PresenceSubscriptionQueue.GetCount() > 0)
	{
		u64 xuid = m_PresenceSubscriptionQueue.Pop();
		if (!rlVerify(xuid > 0))
		{
			return false;
		}

		if (!SubscribeToDevicePresence(xuid))
		{
			rlError("Failed to subscribe to device presence");
			return false;
		}

		if (!SubscribeToTitlePresence(xuid))
		{
			rlError("Failed to subscribe to title presence");
			return false;
		}
	}

	return true;
}

bool rlXBlRtaConnection::ProcessShutdown()
{
	if (m_DeviceStatus.Pending() || m_TitleStatus.Pending())
	{
		return false;
	}

	if (m_PresenceUnsubscriptionQueue.GetCount() > 0)
	{
		u64 xuid = m_PresenceUnsubscriptionQueue.Pop();
		if (!rlVerify(xuid > 0))
		{
			return false;
		}

		if (!UnsubscribeToDevicePresence(xuid))
		{
			rlError("Failed to unsubscribe to device presence");
			return false;
		}

		if (!UnsubscribeToTitlePresence(xuid))
		{
			rlError("Failed to unsubscribe to title presence");
			return false;
		}
	}
	else
	{
		m_State = STATE_IDLE;
	}

	return true;
}

bool rlXBlRtaConnection::IsXuidQueuedForSubscription(u64 xuid)
{
	for (int i = 0; i < m_PresenceSubscriptionQueue.GetCount(); i++)
	{
		if (m_PresenceSubscriptionQueue[i] == xuid)
		{
			return true;
		}
	}

	return false;
}

bool rlXBlRtaConnection::IsXuidQueuedForRemoval(u64 xuid)
{
	for (int i = 0; i < m_PresenceUnsubscriptionQueue.GetCount(); i++)
	{
		if (m_PresenceUnsubscriptionQueue[i] == xuid)
		{
			return true;
		}
	}

	return false;
}

bool rlXBlRtaConnection::SubscribeToDevicePresence(u64 xuid)
{
	if (!HasSubscription(xuid, SUB_DEVICE))
	{
		CREATE_NET_XBLTASK(rlXblRtaSubscribeToDevicePresenceTask, &m_DeviceStatus, xuid, this);
	}

	rlDebug3("Already subscribed to %"I64FMT"u, skipping subscription to device", xuid);

	return true;
}

bool rlXBlRtaConnection::SubscribeToTitlePresence(u64 xuid)
{
	if (!HasSubscription(xuid, SUB_TITLE))
	{
		CREATE_NET_XBLTASK(rlXblRtaSubscribeToTitlePresenceTask, &m_TitleStatus, xuid, this);
	}

	rlDebug3("Already subscribed to %"I64FMT"u, skipping subscription to title", xuid);
	return true;
}

bool rlXBlRtaConnection::UnsubscribeToDevicePresence(u64 xuid)
{
	CREATE_NET_XBLTASK(rlXblRtaUnsubscribeToDevicePresenceTask, &m_DeviceStatus, xuid, this);
}

bool rlXBlRtaConnection::UnsubscribeToTitlePresence(u64 xuid)
{
	CREATE_NET_XBLTASK(rlXblRtaUnsubscribeToTitlePresenceTask, &m_TitleStatus, xuid, this);
}

void rlXBlRtaConnection::ClearSubscriptionQueue()
{
	rlDebug3("Clearing subscription queue, we had %d elements", m_PresenceSubscriptionQueue.GetCount());
	m_PresenceSubscriptionQueue.clear();
}

bool rlXBlRtaConnection::Connect()
{
	m_uLastConnectTime = sysTimer::GetSystemMsTime();
	CREATE_NET_XBLTASK(rlXblRtaConnectionTask, &m_ConnectionStatus, m_XboxLiveContext);
}

bool rlXBlRtaConnection::RegisterCallbacks()
{
	try
	{
		u64 xuid = m_Xuid;

		if (rlVerify(m_XboxLiveContext != nullptr))
		{
			m_OnDisconnectRegistration = m_XboxLiveContext->RealTimeActivityService->WebSocketClosed += 
				ref new EventHandler<Windows::Networking::Sockets::WebSocketClosedEventArgs^>( 
				[xuid] (Platform::Object^, Windows::Networking::Sockets::WebSocketClosedEventArgs^ args) 
			{
				rlXblInternal::GetInstance()->GetRealtimeActivityManager()->OnWebSocketClosed(xuid);
			});

			m_OnDevicePresenceChangeRegistration = m_XboxLiveContext->RealTimeActivityService->DevicePresenceChanged += 
				ref new Windows::Foundation::EventHandler<RealTimeActivityDevicePresenceChangeEventArgs^>( 
				[]( Platform::Object^, RealTimeActivityDevicePresenceChangeEventArgs^ args )
			{
				try
				{
					rlXBlRtaConnection::OnDevicePresenceChange(args);
				}
				catch (Platform::Exception^ ex)
				{
					NET_EXCEPTION(ex);
				}
			});

			m_OnTitlePresenceChangeRegistration = m_XboxLiveContext->RealTimeActivityService->TitlePresenceChanged += 
				ref new Windows::Foundation::EventHandler<RealTimeActivityTitlePresenceChangeEventArgs^>( 
				[]( Platform::Object^, RealTimeActivityTitlePresenceChangeEventArgs^ args )
			{
				try
				{
					rlXBlRtaConnection::OnTitlePresenceChange(args);
				}
				catch (Platform::Exception^ ex)
				{
					NET_EXCEPTION(ex);
				}
			});

			return true;
		}
	}
	catch (Platform::Exception^ ex)
	{
		rlError("RTA. Exception 0x%08x, Message: %ls", ex->HResult, ex->Message != nullptr ? ex->Message->Data() : L"NULL");
	}

	m_CallbacksRegistered = true;
	return false;
}

bool rlXBlRtaConnection::DeregisterCallbacks()
{
	try
	{
		rtry
		{
			// retrieve xbox live context
			rcheck(m_XboxLiveContext != nullptr, catchall, );
			rcheck(m_CallbacksRegistered, catchall, );

			m_XboxLiveContext->RealTimeActivityService->WebSocketClosed -= m_OnDisconnectRegistration;
			m_XboxLiveContext->RealTimeActivityService->DevicePresenceChanged -= m_OnDevicePresenceChangeRegistration;
			m_XboxLiveContext->RealTimeActivityService->TitlePresenceChanged -= m_OnTitlePresenceChangeRegistration;

			m_CallbacksRegistered = false;

			return true;
		}
		rcatchall
		{

		}
	}
	catch (Exception^ ex)
	{
		rlError("Failed to deregister from RTA. Exception 0x%08x, Message: %ls", ex->HResult, ex->Message != nullptr ? ex->Message->Data() : L"NULL");
	}

	return false;
}

void rlXBlRtaConnection::Terminate()
{
	rlDebug1("Trace: Terminate (state: %d)", m_State);

	if (m_State != STATE_IDLE)
	{
		Shutdown();
		m_PresenceUnsubscriptionQueue.clear();
	}
	
	// Deregister all callbacks on termination
	DeregisterCallbacks();

	// Disconnect if necessary
	if (m_Connected && !m_SignedOut)
	{
		Disconnect();
	}
}

void rlXBlRtaConnection::Shutdown()
{
	rlDebug1("Trace: Shutdown");
	if (m_State == STATE_SHUTDOWN)
	{
		return;
	}

	// If we're still connected, transfer all subscriptions to the disconnect queue.
	if (m_Connected)
	{
		int numSubscriptions = m_Subscriptions.GetCount();
		for (int i = numSubscriptions - 1; i >= 0; i--)
		{
			rlXblRtaSubscription* subscription = m_Subscriptions[i];
			if (!IsXuidQueuedForRemoval(subscription->m_Xuid))
			{
				rlDebug3("Removing subscription to %" I64FMT "u, queuing for unsubscription.", subscription->m_Xuid);
				m_PresenceUnsubscriptionQueue.PushAndGrow(subscription->m_Xuid);
			}
		}
	}

	// Cancel all tasks in progress
	CancelInFlightTasks();

	// clear all pending subscriptions
	ClearSubscriptionQueue();

	m_State = STATE_SHUTDOWN;
}

void rlXBlRtaConnection::OnSignOut()
{
	m_SignedOut = true;
}

void rlXBlRtaConnection::OnWebSocketClosed()
{
	m_Connected = false;

	// Cancel all tasks in progress
	CancelInFlightTasks();

	// The web socket closing removes all of our subscriptions inside the Microsoft Services DLL
	DeleteAllSubscriptions();
}

void rlXBlRtaConnection::CancelInFlightTasks()
{
	if (m_TitleStatus.Pending())
	{
		netTask::Cancel(&m_TitleStatus);
	}

	if (m_DeviceStatus.Pending())
	{
		netTask::Cancel(&m_DeviceStatus);
	}
}

void rlXBlRtaConnection::Disconnect()
{
	try
	{
		rlDebug1("Trace: Disconnect (connected: %d)", m_Connected);

		if (m_Connected)
		{
			m_XboxLiveContext->RealTimeActivityService->Disconnect();
			m_Connected = false;
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}
}

rlXblRtaSubscription* rlXBlRtaConnection::CreateSubscription(u64 xuid, SubscriptionType subType)
{
	rlXblRtaSubscription* subscription = NULL;

	rtry
	{
		switch(subType)
		{
		case SUB_DEVICE:
			{
				subscription = RL_NEW(rlXBlRtaConnection, rlXblRtaDeviceSubscription);
			}
			break;
		case SUB_TITLE:
			{
				subscription = RL_NEW(rlXBlRtaConnection, rlXblRtaTitleSubscription);
			}
			break;
		case SUB_STATISTIC:
			{
				subscription = RL_NEW(rlXBlRtaConnection, rlXblRtaStatisticSubscription);
			}
			break;
		}

		rverify(subscription, catchall, );
	}
	rcatchall
	{

	}

	if (subscription)
	{
		subscription->m_SubscriptionType = subType;
		subscription->m_Xuid = xuid;
	}

	return subscription;
}

bool rlXBlRtaConnection::AddSubscription(rlXblRtaSubscription* subscription)
{
	m_Subscriptions.PushAndGrow(subscription);
	return true;
}

bool rlXBlRtaConnection::HasSubscription(u64 xuid, SubscriptionType subType)
{
	// subscriptions
	for (int i = 0; i < m_Subscriptions.GetCount(); i++)
	{
		if (m_Subscriptions[i]->m_Xuid == xuid && m_Subscriptions[i]->m_SubscriptionType == subType)
		{
			return true;
		}
	}

	return false;
}

rlXblRtaSubscription* rlXBlRtaConnection::GetSubscription(u64 xuid, SubscriptionType subType)
{
	for (int i = 0; i < m_Subscriptions.GetCount(); i++)
	{
		if (m_Subscriptions[i]->m_Xuid == xuid && m_Subscriptions[i]->m_SubscriptionType == subType)
		{
			return m_Subscriptions[i];
		}
	}

	return NULL;
}

void rlXBlRtaConnection::DeleteSubscription(rlXblRtaSubscription* subscription)
{
	rlDebug3("Deleted subscription (type: %d) for %"I64FMT"u", subscription->m_SubscriptionType, subscription->m_Xuid);
	m_Subscriptions.DeleteMatches(subscription);
	RL_DELETE(subscription);
}

void rlXBlRtaConnection::DeleteAllSubscriptions()
{
	int count = m_Subscriptions.GetCount();
	for (int i = count - 1; i >= 0; i--)
	{
		rlXblRtaSubscription* sub = m_Subscriptions[i];
		DeleteSubscription(sub);
	}
}

void rlXBlRtaConnection::OnStatisticChange(RealTimeActivityStatisticChangeEventArgs^ args)
{
	u64 xuid = rlXblCommon::WinRtStringToXuid(args->Subscription->XboxUserId);

	char statName[RL_MAX_STAT_BUF_SIZE] = {0};
	rlXblCommon::WinRtStringToUTF8(args->Subscription->StatisticName, statName, sizeof(statName));

	rlXblFriendStatisticChangedEvent e(xuid, statName);
	rlXblInternal::GetInstance()->DispatchEvent(&e);
}

void rlXBlRtaConnection::OnDevicePresenceChange(RealTimeActivityDevicePresenceChangeEventArgs^ args)
{
	bool bIsLoggedIn = args->IsUserLoggedOnDevice && args->DeviceType == Presence::PresenceDeviceType::XboxOne;
	u64 xuid = rlXblCommon::WinRtStringToXuid(args->Subscription->XboxUserId);
	rlDebug3("OnDevicePresenceChange:: %d for %" I64FMT "d ", bIsLoggedIn, xuid);

	rlXblFriendOnlineStatusChangedEvent e(xuid, bIsLoggedIn);
	rlXblInternal::GetInstance()->DispatchEvent(&e);
}

void rlXBlRtaConnection::OnTitlePresenceChange(RealTimeActivityTitlePresenceChangeEventArgs^ args)
{
	RealTimeActivityTitlePresenceState state = args->TitlePresenceState;
	if (state == RealTimeActivityTitlePresenceState::Ended)
	{
		u64 xuid = rlXblCommon::WinRtStringToXuid(args->Subscription->XboxUserId);
		rlDebug3("OnTitlePresenceChange::RealTimeActivityTitlePresenceState::Ended for %" I64FMT "d ",xuid);

		rlXblFriendTitleStatusChangedEvent e(xuid, false);
		rlXblInternal::GetInstance()->DispatchEvent(&e);
	}
	else if (state == RealTimeActivityTitlePresenceState::Started)
	{
		u64 xuid = rlXblCommon::WinRtStringToXuid(args->Subscription->XboxUserId);
		rlDebug3("OnTitlePresenceChange::RealTimeActivityTitlePresenceState::Started for %" I64FMT "d",xuid);

		rlXblFriendTitleStatusChangedEvent e(xuid, true);
		rlXblInternal::GetInstance()->DispatchEvent(&e);
	}
}

////////////////////////////////////////
// rlXblRealTimeActivity
////////////////////////////////////////
rlXblRealTimeActivity::rlXblRealTimeActivity()
{

}

rlXblRealTimeActivity::~rlXblRealTimeActivity()
{

}

bool rlXblRealTimeActivity::Init()
{
	return true;
}

void rlXblRealTimeActivity::Shutdown()
{
	int numConnections = m_RtaConnections.GetCount();
	for (int i = numConnections - 1; i >= 0; i--)
	{
		m_RtaConnections[i]->Terminate();
		delete m_RtaConnections[i];
		m_RtaConnections.Delete(i);
	}
}

void rlXblRealTimeActivity::Update()
{
	int numConnections = m_RtaConnections.GetCount();
	for (int i = numConnections-1; i >= 0; i--)
	{
		m_RtaConnections[i]->Update();
		if (m_RtaConnections[i]->CanDestroy())
		{
			rlDebug3("Deleting RTA Connection for %" I64FMT "u", m_RtaConnections[i]->GetXuid());
			RL_DELETE(m_RtaConnections[i]);
			m_RtaConnections.Delete(i);
		}
	}
}

void rlXblRealTimeActivity::OnSignOut(const int localGamerIndex)
{
	rtry
	{
		rlDebug3("Trace: OnSignOut index %d", localGamerIndex);

		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localGamerIndex);
		rcheck(xuid > 0, catchall, );

		rlXBlRtaConnection* connection = FindRtaConnection(xuid);
		rcheck(connection, catchall, );
		connection->OnSignOut();
		connection->Shutdown();
	}
	rcatchall
	{

	}
}


rlXBlRtaConnection* rlXblRealTimeActivity::CreateRtaConnection(const int localGamerIndex)
{
	rlXBlRtaConnection* xblRtaConnection = NULL;

	rtry
	{
		// Create the connection object
		xblRtaConnection = RL_NEW(rlXblRealTimeActivity,rlXBlRtaConnection);

		// initialize it
		rverify(xblRtaConnection->Init(localGamerIndex), catchall, );

		// Add to list and return
		m_RtaConnections.PushAndGrow(xblRtaConnection);
		return xblRtaConnection;
	}
	rcatchall
	{
		// if initialization failed, we still want to clean up this object
		if (xblRtaConnection)
		{
			delete xblRtaConnection;
		}

		return NULL;
	}
}

rlXBlRtaConnection* rlXblRealTimeActivity::FindRtaConnection(u64 xuid)
{
	int numConnections = m_RtaConnections.GetCount();
	for (int i = 0; i < numConnections; i++)
	{
		if (m_RtaConnections[i]->GetXuid() == xuid)
		{
			return m_RtaConnections[i];
		}
	}

	return NULL;
}

rlXBlRtaConnection* rlXblRealTimeActivity::GetRtaConnection(const int localGamerIndex)
{
	int numConnections = m_RtaConnections.GetCount();
	u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localGamerIndex);
	for (int i = 0; i < numConnections; i++)
	{
		if (m_RtaConnections[i]->GetXuid() == xuid)
		{
			return m_RtaConnections[i];
		}
	}

	return CreateRtaConnection(localGamerIndex);
}


bool rlXblRealTimeActivity::SubscribeToFriendsPresence(int localGamerIndex, const atArray<rlFriendsReference*>* friendReferences)
{
	SYS_CS_SYNC(m_QueueCs);

	rtry
	{
		rlXBlRtaConnection* connection = GetRtaConnection(localGamerIndex);
		rverify(connection, catchall, );

		for (int i = 0; i < friendReferences->GetCount(); i++)
		{
			u64 xuid = (*friendReferences)[i]->m_Id.m_Xuid;
			if (!connection->IsXuidQueuedForSubscription(xuid))
			{
				rlDebug3("Queued %" I64FMT "u for presence for index %d", xuid, localGamerIndex);
				connection->m_PresenceSubscriptionQueue.PushAndGrow(xuid);
			}
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXblRealTimeActivity::SubscribeToFriendsPresence(int localGamerIndex, const rlFriendsReference* friendReferences, const int numFriendReferences)
{
	SYS_CS_SYNC(m_QueueCs);

	rtry
	{
		rlXBlRtaConnection* connection = GetRtaConnection(localGamerIndex);
		rverify(connection, catchall, );

		for (int i = 0; i < numFriendReferences; i++)
		{
			u64 xuid = friendReferences[i].m_Id.m_Xuid;
			if (!connection->IsXuidQueuedForSubscription(xuid))
			{
				rlDebug3("Queued %" I64FMT "u for presence for index %d", xuid, localGamerIndex);
				connection->m_PresenceSubscriptionQueue.PushAndGrow(xuid);
			}
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXblRealTimeActivity::UnsubscribeToFriendsPresence(int localGamerIndex, const atArray<rlFriendsReference*>* friendReferences)
{
	SYS_CS_SYNC(m_QueueCs);

	rtry
	{
		rlXBlRtaConnection* connection = GetRtaConnection(localGamerIndex);
		rverify(connection, catchall, );

		for (int i = 0; i < friendReferences->GetCount(); i++)
		{
			u64 xuid = (*friendReferences)[i]->m_Id.m_Xuid;
			rlDebug3("Queued %" I64FMT "u for removal of presence for index %d", xuid, localGamerIndex);
			if (!connection->IsXuidQueuedForRemoval(xuid))
			{
				connection->m_PresenceUnsubscriptionQueue.PushAndGrow(xuid);
			}
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXblRealTimeActivity::UnsubscribeToFriendsPresence(int localGamerIndex, const rlFriendsReference* friendReferences, const int numFriendReferences)
{
	SYS_CS_SYNC(m_QueueCs);

	rtry
	{
		rlXBlRtaConnection* connection = GetRtaConnection(localGamerIndex);
		rverify(connection, catchall, );

		for (int i = 0; i < numFriendReferences; i++)
		{
			u64 xuid = friendReferences[i].m_Id.m_Xuid;
			rlDebug3("Queued %" I64FMT "u for removal of presence for index %d", xuid, localGamerIndex);
			if (!connection->IsXuidQueuedForRemoval(xuid))
			{
				connection->m_PresenceUnsubscriptionQueue.PushAndGrow(xuid);
			}
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlXblRealTimeActivity::OnWebSocketClosed(u64 xuid)
{
	rtry
	{
		rlDebug1("Trace: OnWebSocketClosed xuid(%" I64FMT "u)", xuid);

		rlXBlRtaConnection* connection = FindRtaConnection(xuid);
		rcheck(connection, catchall, );
		connection->OnWebSocketClosed();
		connection->Shutdown();
	}
	rcatchall
	{

	}
}

} // namespace rage

#endif  //((RSG_DURANGO))
