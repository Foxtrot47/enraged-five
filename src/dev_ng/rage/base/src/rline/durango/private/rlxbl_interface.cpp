// 
// rlxbl_interface.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "../rlxbl_interface.h"
#include "rlxblinternal.h"
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "rline/rltitleid.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, xbl)

#undef __rage_channel
#define __rage_channel rline_xbl

//Global instance
rlXbl g_rlXbl;

extern const rlTitleId* g_rlTitleId;

///////////////////////////////////////////////////////////////////////////////
//  rlXbl
///////////////////////////////////////////////////////////////////////////////

rlGameRegion
rlXbl::GetGameRegion()
{
	return rlXblInternal::GetGameRegion();
}

rlEnvironment 
rlXbl::GetEnvironment()
{
	return g_rlTitleId->m_XblTitleId.GetEnvironment();
}

rlEnvironment
rlXbl::GetNativeEnvironment()
{
	return g_rlTitleId->m_XblTitleId.GetNativeEnvironment();
}

rlXbl::rlXbl()
: m_Initialized(false)
{

}

rlXbl::~rlXbl()
{

}

bool 
rlXbl::IsInitialized()
{
	return m_Initialized;
}

bool 
rlXbl::Init(sysMemAllocator* allocator)
{
	bool success = false;

	rtry
	{
		rverify(rlXblInternal::GetInstance() != NULL, catchall, );
		rverify(rlXblInternal::GetInstance()->Init(allocator), catchall, );
		m_Initialized = true;
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void 
rlXbl::Shutdown()
{
	rlXblInternal::GetInstance()->Shutdown();
}

void 
rlXbl::Update()
{
	rlXblInternal::GetInstance()->Update();
}

IPresence* 
rlXbl::GetPresenceManager()
{
	return rlXblInternal::GetInstance()->GetPresenceManager();
}

IProfiles* 
rlXbl::GetProfileManager()
{
	return rlXblInternal::GetInstance()->GetProfilesManager();
}

IPrivileges* 
rlXbl::GetPrivilegesManager()
{
	return rlXblInternal::GetInstance()->GetPrivilegesManager();
}

IPlayers* 
rlXbl::GetPlayerManager()
{
	return rlXblInternal::GetInstance()->GetPlayerManager();
}

ISessions* 
rlXbl::GetSessionManager()
{
	return rlXblInternal::GetInstance()->GetSessionManager();
}

ISystemUi*
rlXbl::GetSystemUi()
{
	return rlXblInternal::GetInstance()->GetSystemUi();
}

IAchievements*
rlXbl::GetAchivementManager()
{
	return rlXblInternal::GetInstance()->GetAchievementsManager();	
}

IParty* 
rlXbl::GetPartyManager()
{
	return rlXblInternal::GetInstance()->GetPartyManager();
}

IRealTimeActivity*
rlXbl::GetRealtimeActivityManager()
{
	return rlXblInternal::GetInstance()->GetRealtimeActivityManager();
}

ICommerce* 
rlXbl::GetPlatformCommerceManager()
{
	return rlXblInternal::GetInstance()->GetPlatformCommerceManager();
}

IProfanityCheck* 
rlXbl::GetProfanityCheckManager()
{
	return rlXblInternal::GetInstance()->GetProfanityCheckManager();
}

bool 
rlXbl::CancelTask(netStatus* status)
{
	if(status && status->Pending() && netTask::HasTask(status))
	{
		netTask::Cancel(status);
		return true;
	}
	return false;
}

void
rlXbl::AddDelegate(rlXblEventDelegate* dlgt)
{
	return rlXblInternal::GetInstance()->AddDelegate(dlgt);
}

void rlXbl::RemoveDelegate(rlXblEventDelegate* dlgt)
{
	return rlXblInternal::GetInstance()->RemoveDelegate(dlgt);
}

void rlXbl::DispatchEvent(const rlXblEvent *e)
{
	return rlXblInternal::GetInstance()->DispatchEvent(e);
}

bool rlXbl::HasPendingInvite()
{
	return rlXblInternal::GetInstance()->HasPendingInvite();
}

static bool s_bShouldCancelCommerceTasks = false;

void 
rlXbl::SetShouldCancelCommerceTasks(const bool bShouldCancelCommerceTasks)
{
	if(s_bShouldCancelCommerceTasks != bShouldCancelCommerceTasks)
	{
		rlDebug("SetShouldCancelCommerceTasks :: %s", bShouldCancelCommerceTasks ? "Enabled" : "Disabled");
		s_bShouldCancelCommerceTasks = bShouldCancelCommerceTasks;
	}
}

bool
rlXbl::ShouldCancelCommerceTasks()
{
	return s_bShouldCancelCommerceTasks;
}

#if RSG_GDK
void rlXbl::SetInviteResumeGracePeriodMs(const unsigned gracePeriodMs)
{
	rlXblInvites::SetResumeGracePeriodMs(gracePeriodMs);
}
#endif

} // namespace rage

#endif  //RSG_DURANGO
