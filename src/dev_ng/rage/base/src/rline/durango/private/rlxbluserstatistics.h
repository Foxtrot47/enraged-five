// 
// rlXblUserStatistics.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_USERSTATISTICS_H
#define RLXBL_USERSTATISTICS_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlXblUserStatistics.h is a private header. Do not #include it. Use rlxbluserstatistics_interface.h instead"
#endif

#include "../rlxbluserstatistics_interface.h"

#endif //RSG_DURANGO
#endif // RLXBL_USERSTATISTICS_H

