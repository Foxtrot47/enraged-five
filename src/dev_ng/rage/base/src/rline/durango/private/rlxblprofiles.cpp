﻿// 
// rlxblprofiles.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlxblprofiles.h"
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

namespace rage
{

/*
	This file implements a cache & batch system for display name and gamertag requests.
	Desirables:
	- Batch all requests over short intervals (i.e. 1-2 seconds) into one larger request.
	- Only allow one in-flight HTTP request at once so we don't flood the server or our max in-flight HTTP request
			queue with display name requests.
	- If we kick off an HTTP request but end up getting queued because there are too many in-flight HTTP requests,
			coalesce all pending requests again, just before committing the HTTP request to the server.
	- Requests that are made while a batch is in-flight can potentially be fulfilled by the previous in-flight
			batch (i.e. if new requests had duplicate xuids relative to the previous batch)
	- Request the minimum amount of info from the server (remove duplicates throughout all requests in a batch)
	- Cache results for a short period of time (i.e. 10 mins) so that multiple requests for the same info won't
			cause more server load and retrieval latency. Microsoft says we can't cache this info but we often
			see requests for the same info within a small amount of time. We should be able to get away with
			a short cache life.
*/

// if set to 1, a small number of player names will be cached for a short period of time.
// player name requests will be fulfilled from the cache whenever possible
#define CACHE_PLAYER_NAMES 1

RAGE_DEFINE_SUBCHANNEL(rline_xbl, profiles);
#undef __rage_channel
#define __rage_channel rline_xbl_profiles

#define XBLPROFILES_CREATE_RL_SUBTASK(T, ...)								\
	bool success = false;													\
	T* task = NULL;															\
	rtry																	\
	{																		\
		task = rlGetTaskManager()->CreateTask<T>();							\
		rverify(task,catchall,);											\
		rverify(rlTaskBase::Configure(task, __VA_ARGS__),catchall,);		\
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);		\
		success = true;														\
	}																		\
	rcatchall																\
	{																		\
		if(task)															\
		{																	\
			rlGetTaskManager()->DestroyTask(task);							\
		}																	\
	}																		\
	return success;

#if CACHE_PLAYER_NAMES
class CachedPlayerNameEntry
{
public:
	static const unsigned PLAYER_NAME_CACHE_TTL = 60 * 10000; // player names will be cached for 10 mins

	CachedPlayerNameEntry()
	{
		sysMemSet(m_DisplayName, 0, sizeof(m_DisplayName));
		sysMemSet(m_Gamertag, 0, sizeof(m_Gamertag));
		m_GamerHandle.Clear();
		m_Timestamp = 0;
	}

	unsigned GetTimeToLiveMs()
	{
		unsigned elapsed = sysTimer::GetSystemMsTime() - m_Timestamp;
		return (elapsed < PLAYER_NAME_CACHE_TTL) ? PLAYER_NAME_CACHE_TTL - elapsed : 0;
	}

	bool IsValid()
	{
		return (m_GamerHandle.IsValid()) && ((sysTimer::GetSystemMsTime() - m_Timestamp) < PLAYER_NAME_CACHE_TTL);
	}

	bool IsMatch(const rlGamerHandle& gh)
	{
		return IsValid() && (m_GamerHandle == gh);
	}

	void Reset(const rlGamerHandle& gh, const rlDisplayName& displayName, const rlGamertag& gamertag)
	{
		m_GamerHandle = gh;
		safecpy(m_DisplayName, displayName);
		safecpy(m_Gamertag, gamertag);
		m_Timestamp = sysTimer::GetSystemMsTime();
	}

	const rlDisplayName* GetDisplayName() {return &m_DisplayName;} 
	const rlGamertag* GetGamertag() {return &m_Gamertag;} 
	const rlGamerHandle& GetGamerHandle() {return m_GamerHandle;} 

private:
	rlDisplayName m_DisplayName;
	rlGamertag m_Gamertag;
	rlGamerHandle m_GamerHandle;
	unsigned m_Timestamp;
};

static const unsigned MAX_CACHED_PLAYER_NAME_ENTRIES = 64;
static CachedPlayerNameEntry s_CachedPlayerNameEntries[MAX_CACHED_PLAYER_NAME_ENTRIES];
static u32 s_MaxCachedEntries = 0;

static void CachePlayerNameEntry(const rlGamerHandle& gh, const rlDisplayName& displayName, const rlGamertag& gamertag)
{
	int cacheIndex = -1;
	for(unsigned i = 0; i < COUNTOF(s_CachedPlayerNameEntries); ++i)
	{
		if(s_CachedPlayerNameEntries[i].IsMatch(gh))
		{
			cacheIndex = i;
			break;
		}
	}

	if(cacheIndex < 0)
	{
		unsigned lowestTtl = 0xFFFFFFFF;
		for(unsigned i = 0; i < COUNTOF(s_CachedPlayerNameEntries); ++i)
		{
			if(s_CachedPlayerNameEntries[i].IsValid() == false)
			{
				cacheIndex = i;
				break;
			}

			unsigned ttl = s_CachedPlayerNameEntries[i].GetTimeToLiveMs();
			if(ttl < lowestTtl)
			{
				lowestTtl = ttl;
				cacheIndex = i;
			}
		}
	}

	if(rlVerify(cacheIndex >= 0))
	{
		s_CachedPlayerNameEntries[cacheIndex].Reset(gh, displayName, gamertag);
	}
	
#if !__NO_OUTPUT
	u32 numCachedEntries = 0;
	for(unsigned i = 0; i < COUNTOF(s_CachedPlayerNameEntries); ++i)
	{
		if(s_CachedPlayerNameEntries[i].IsValid())
		{
			numCachedEntries++;
			if(numCachedEntries > s_MaxCachedEntries)
			{
				s_MaxCachedEntries = numCachedEntries;
				rlDebug3("s_MaxCachedEntries: %u", s_MaxCachedEntries);
			}
		}
	}
#endif
}

static bool GetCachedPlayerNameEntry(const rlGamerHandle& gamerhandle, rlDisplayName& displayName, rlGamertag& gamertag)
{
	for(unsigned i = 0; i < COUNTOF(s_CachedPlayerNameEntries); ++i)
	{
		if(s_CachedPlayerNameEntries[i].IsMatch(gamerhandle))
		{
			safecpy(displayName, (const char*)s_CachedPlayerNameEntries[i].GetDisplayName());
			safecpy(gamertag, (const char*)s_CachedPlayerNameEntries[i].GetGamertag());
			return true;
		}
	}

	return false;
}
#endif

class rlXblPlayerNamesBatch;
///////////////////////////////////////////////////////////////////////////////
//  XblGetPlayerNamesTask Declaration
///////////////////////////////////////////////////////////////////////////////
class XblGetPlayerNamesTask : public rlXblHttpProfileSettingsTask
{
public:
	static const int TIMEOUT_SECS = 10;

	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(XblGetPlayerNamesTask);

	XblGetPlayerNamesTask();
	virtual ~XblGetPlayerNamesTask();

	bool Configure(int localGamerIndex, rlXblPlayerNamesBatch* batch);
	virtual void Start();
	virtual void Update(const unsigned timeStep);
	bool AppendContentToHttpRequest();
	bool CoalesceBatch();
	bool SendBatchRequest();
	bool ProcessUserNames(u64 xuid, const char* displayName, const char* gamertag, int& resultCode);

private:
	static const unsigned MAX_NAMES = 100;
	rlGamerHandle m_GamerHandles[MAX_NAMES];
	unsigned m_NumGamerHandles;
	unsigned m_NumNamesProcessed;
	rlXblPlayerNamesBatch* m_Batch;
	int m_Settings;

	enum RequestState
	{
		STATE_WAITING_FOR_READY,
		STATE_RECEIVING
	};

	RequestState m_RequestState;

	bool HaveGamerHandle(const rlGamerHandle& gh)
	{
		for(unsigned i = 0; i < m_NumGamerHandles; ++i)
		{
			if(gh == m_GamerHandles[i])
			{
				return true;
			}
		}

		return false;
	}

	int FindGamerHandleIndex(const rlGamerHandle& gh, unsigned hintIndex)
	{
		if(rlVerify(hintIndex < m_NumGamerHandles) && (m_GamerHandles[hintIndex] == gh))
		{
			return hintIndex;
		}

		for(unsigned i = 0; i < m_NumGamerHandles; ++i)
		{
			if(gh == m_GamerHandles[i])
			{
				return i;
			}
		}

		return -1;
	}
};

///////////////////////////////////////////////////////////////////////////////
//  rlXblPlayerNamesRequest
///////////////////////////////////////////////////////////////////////////////
class rlXblPlayerNamesRequest
{
	friend class rlXblPlayerNamesBatch;
	friend class XblGetPlayerNamesTask;
public:
	rlXblPlayerNamesRequest()
	{
		Clear();
	}

	~rlXblPlayerNamesRequest()
	{

	}

	void Clear()
	{
		m_Fulfilled = false;
		m_Timer = netStopWatch();
		m_LocalGamerIndex = -1;
		m_GamerHandles.Clear();
		m_DisplayNames.Clear();
		m_Gamertags.Clear();
		m_NumGamerHandles = 0;
		if(!rlVerify((m_Status == NULL) || (m_Status->Pending() == false)))
		{
			m_Status->SetFailed();
		}
		m_Status = NULL;
	}

	bool Init(const int localGamerIndex, const rlGamerHandle* gamerhandles, u32 gamerHandleSpan, const unsigned numGamerhandles, rlDisplayName* displayNames, u32 displayNameSpan, rlGamertag* gamertags, u32 gamertagSpan, netStatus* status)
	{
		bool success = false;
		
		rtry
		{
			Clear();

			rverify(gamerhandles, catchall, );
			rverify(numGamerhandles > 0, catchall, );
			rverify(status, catchall, );
			rverify(status->Pending() == false, catchall, );


#if !__NO_OUTPUT
			m_Id = GetNextRequestId();
#endif

			rlDebug3("Init request %u containing %u gamerhandles", GetRequestId(), numGamerhandles);

			m_Timer.Start();
			m_LocalGamerIndex = localGamerIndex;
			m_GamerHandles.Init(gamerhandles, gamerHandleSpan, numGamerhandles);

			m_DisplayNames.Clear();
			if(displayNames)
			{
				m_DisplayNames.Init(displayNames, displayNameSpan, numGamerhandles);
			}

			m_Gamertags.Clear();
			if(gamertags)
			{
				m_Gamertags.Init(gamertags, gamertagSpan, numGamerhandles);
			}
			m_NumGamerHandles = numGamerhandles;
			m_Status = status;

			m_Status->SetPending();

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}

	u32 GetNextRequestId()
	{
		static u32 s_NextId = 0;

		u32 id = sysInterlockedIncrement(&s_NextId);

		while(0 == id)
		{
			id = sysInterlockedIncrement(&s_NextId);
		}

		return id;
	}

#if !__NO_OUTPUT
	u32 GetRequestId()
	{
		return m_Id;
	}
#endif

	bool Pending()
	{
		return m_Status && m_Status->Pending();
	}

	void Cancel()
	{
		if(Pending())
		{
			rlDebug3("[%u] Canceling request", GetRequestId());
			m_Status->SetCanceled();
			m_Status = NULL;
		}
	}

	void SetFailed()
	{
		if(Pending())
		{
			rlDebug3("[%u] Setting request status to failed", GetRequestId());
			m_Status->SetFailed();
			m_Status = NULL;
		}
	}

	void SetSucceeded()
	{
		if(Pending())
		{
			rlDebug3("[%u] Setting request status to succeeded", GetRequestId());
			m_Status->SetSucceeded();
			m_Status = NULL;
		}
	}

	const u32 GetElapsedMilliseconds()
	{
		return m_Timer.GetElapsedMilliseconds();
	}

	bool ProcessUserNames(const rlGamerHandle gh, const char* displayName, const char* gamertag)
	{
		if(!Pending())
		{
			return true;
		}

		//rlDebug3("[%u] Name batch processing user names", GetRequestId());

		bool fulfilled = true;

		// this loop does two things:
		// 1. looks for the gamerhandle in our list and fulfills that entry
		// 2. checks to see if all of the gamerhandles in our list are fulfilled
		for(u32 i = 0; i < m_NumGamerHandles; ++i)
		{
			if(m_GamerHandles[i] == gh)
			{
				if(!m_DisplayNames.IsEmpty())
				{
					safecpy(m_DisplayNames[i], displayName, RL_MAX_DISPLAY_NAME_BUF_SIZE);
				}

				if(!m_Gamertags.IsEmpty())
				{
					safecpy(m_Gamertags[i], gamertag, RL_MAX_NAME_BUF_SIZE);
				}
			}

			if((!m_Gamertags.IsEmpty() && (m_Gamertags[i][0] == '\0')) || (!m_DisplayNames.IsEmpty() && (m_DisplayNames[i][0] == '\0')))
			{
				fulfilled = false;
			}
		}

		if(fulfilled)
		{
			rlDebug3("[%u] Request is fulfilled, setting status to succeeded", GetRequestId());

			m_Fulfilled = true;

			SetSucceeded();
		}

		return true;
	}

	bool IsFulfilled(const rlGamerHandle& gh)
	{
		for(u32 i = 0; i < m_NumGamerHandles; ++i)
		{
			if(m_GamerHandles[i] == gh)
			{
				if((!m_Gamertags.IsEmpty() && (m_Gamertags[i][0] == '\0')) || (!m_DisplayNames.IsEmpty() && (m_DisplayNames[i][0] == '\0')))
				{
					return false;
				}
			}
		}

		return true;
	}

	bool IsFulfilled()
	{
		return m_Fulfilled;
	}

	inlist_node<rlXblPlayerNamesRequest> m_ListLink;

private:
#if !__NO_OUTPUT
	u32 m_Id;
#endif

	int m_LocalGamerIndex;
	atSpanArray<const rlGamerHandle> m_GamerHandles;
	atSpanArray<rlDisplayName> m_DisplayNames;
	atSpanArray<rlGamertag> m_Gamertags;
	u32 m_NumGamerHandles;
	netStopWatch m_Timer;
	netStatus* m_Status;
	bool m_Fulfilled : 1;
};

typedef inlist<rlXblPlayerNamesRequest, &rlXblPlayerNamesRequest::m_ListLink> RequestList;

///////////////////////////////////////////////////////////////////////////////
//  rlXblPlayerNamesBatch
///////////////////////////////////////////////////////////////////////////////
class rlXblPlayerNamesBatch
{
	friend class XblGetPlayerNamesTask;

public:
	const static u32 MAX_REQUESTS = 100;
	const static u32 BATCH_TIME_MS = 1000;

	rlXblPlayerNamesBatch()
	{
		Clear();
	}

	~rlXblPlayerNamesBatch()
	{
		Clear();
	}

	void Clear()
	{
		CancelAll();
	}

	void Init()
	{
		rlDebug3("sizeof(rlXblPlayerNamesRequest): %" SIZETFMT "u", sizeof(rlXblPlayerNamesRequest));
		rlDebug3("sizeof(rlXblPlayerNamesBatch): %" SIZETFMT "u", sizeof(rlXblPlayerNamesBatch));
		rlDebug3("sizeof(s_CachedPlayerNameEntries): %" SIZETFMT "u", sizeof(s_CachedPlayerNameEntries));
		Clear();
	}

	void CancelAll()
	{
		RequestList::iterator it = m_ActiveRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_ActiveRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			if(rqst->Pending())
			{
				rqst->Cancel();
			}
		}

		it = m_PendingRequests.begin();
		next = it;
		stop = m_PendingRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			if(rqst->Pending())
			{
				rqst->Cancel();
			}
		}

		if(m_Status.Pending())
		{
			netTask::Cancel(&m_Status);
		}

		m_ActiveRequests.clear();
		m_PendingRequests.clear();
		m_FreeRequests.clear();

		for(unsigned i = 0; i < MAX_REQUESTS; ++i)
		{
			m_FreeRequests.push_back(&m_Requests[i]);
		}
	}

	bool ActivatePendingRequest(rlXblPlayerNamesRequest* rqst)
	{
		rlDebug3("Promoting pending request %u to active list", rqst->GetRequestId());
		rlAssert(IsRequestPending(rqst));
		m_PendingRequests.erase(rqst);
		m_ActiveRequests.push_back(rqst);
		return true;
	}

	bool SetRequestToPending(rlXblPlayerNamesRequest* rqst)
	{
		rlDebug3("Setting active request %u back to pending list", rqst->GetRequestId());
		rlAssert(IsRequestActive(rqst));
		m_ActiveRequests.erase(rqst);
		m_PendingRequests.push_back(rqst);
		return true;
	}
	
	void CancelRequest(netStatus* status)
	{		
		rlXblPlayerNamesRequest* rqst = FindPendingRequest(status);
		if(rqst == NULL)
		{
			rqst = FindActiveRequest(status);
		}

		if(rqst)
		{
			rqst->Cancel();
			FreeRequest(rqst);
		}
	}

	u32 IncNumTasksStarted()
	{
		static u32 s_NumTasksStarted = 0;

		u32 numTasks = sysInterlockedIncrement(&s_NumTasksStarted);

		while(0 == numTasks)
		{
			numTasks = sysInterlockedIncrement(&s_NumTasksStarted);
		}

		return numTasks;
	}

	bool StartTask(int localGamerIndex)
	{
		rlDebug3("Starting XblGetPlayerNamesTask #%u", IncNumTasksStarted());

		XBLPROFILES_CREATE_RL_SUBTASK(XblGetPlayerNamesTask, localGamerIndex, this, &m_Status);
	}

	void Update()
	{
		if(m_Status.None())
		{
			if(!m_PendingRequests.empty())
			{
				RequestList::iterator it = m_PendingRequests.begin();
				RequestList::iterator next = it;
				RequestList::const_iterator stop = m_PendingRequests.end();
				u32 maxWaitingTime = 0;
				int localGamerIndex = -1;
				for(++next; stop != it; it = next, ++next)
				{
					rlXblPlayerNamesRequest* rqst = *it;

					u32 elapsed = rqst->GetElapsedMilliseconds();
					//rlDebug3("Pending request %u has been waiting %u ms and has %u gamerhandless", rqst->GetRequestId(), elapsed, rqst->m_NumGamerHandles);

					if(elapsed > (20 * 1000))
					{
						rlError("Pending request %u hasn't been fulfilled for %u ms, failing it", rqst->GetRequestId(), elapsed);
						rqst->SetFailed();
						FreeRequest(rqst);
					}
					else if(elapsed > maxWaitingTime)
					{
						maxWaitingTime = elapsed;
					}

					// if multiple requests have different local gamer indexes, we 
					// just pick one of them and make the request
					localGamerIndex = rqst->m_LocalGamerIndex;
				}

				if((maxWaitingTime >= BATCH_TIME_MS))
				{
					rlDebug3("Starting player name batch request. Oldest request has been waiting %u ms", maxWaitingTime);
					StartTask(localGamerIndex);
				}
			}
		}
		else if(!m_Status.Pending())
		{
			if(m_Status.Succeeded())
			{
				// if there are still active requests, then we haven't fulfilled them, add them back to the pending 
				// list. This can happen if there were too many gamerhandles in all the pending lists combined and we
				// partially filled a request.

				RequestList::iterator it = m_ActiveRequests.begin();
				RequestList::iterator next = it;
				RequestList::const_iterator stop = m_ActiveRequests.end();
				for(++next; stop != it; it = next, ++next)
				{
					rlXblPlayerNamesRequest* rqst = *it;
					SetRequestToPending(rqst);
				}
			}
			else
			{
				// batch failed, fail all active requests
				RequestList::iterator it = m_ActiveRequests.begin();
				RequestList::iterator next = it;
				RequestList::const_iterator stop = m_ActiveRequests.end();
				for(++next; stop != it; it = next, ++next)
				{
					rlXblPlayerNamesRequest* rqst = *it;
					rqst->SetFailed();
					FreeRequest(rqst);
				}
			}

			m_Status.Reset();
		}
	}

	rlXblPlayerNamesRequest* FindPendingRequest(netStatus* status)
	{
		RequestList::iterator it = m_PendingRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_PendingRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			if(rqst->m_Status == status)
			{
				return rqst;
			}
		}

		return NULL;
	}

	bool IsRequestPending(rlXblPlayerNamesRequest* request)
	{
		RequestList::iterator it = m_PendingRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_PendingRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			if(rqst == request)
			{
				return true;
			}
		}

		return false;
	}

	rlXblPlayerNamesRequest* FindActiveRequest(netStatus* status)
	{
		RequestList::iterator it = m_ActiveRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_ActiveRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			if(rqst->m_Status == status)
			{
				return rqst;
			}
		}

		return NULL;
	}

	bool IsRequestActive(rlXblPlayerNamesRequest* request)
	{
		RequestList::iterator it = m_ActiveRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_ActiveRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			if(rqst == request)
			{
				return true;
			}
		}

		return false;
	}
	
	rlXblPlayerNamesRequest* AllocateRequest()
	{
		rtry
		{
			rverify(m_FreeRequests.empty() == false, catchall, );

			rlXblPlayerNamesRequest* request = m_FreeRequests.front();
			rverify(request, catchall, );
			m_FreeRequests.pop_front();
			m_PendingRequests.push_back(request);

			rlDebug3("Allocated player name request. Num Pending Requests: %" SIZETFMT "u, Num Active Requests: %" SIZETFMT "u, Num Free Requests: %" SIZETFMT "u", m_PendingRequests.size(), m_ActiveRequests.size(), m_FreeRequests.size());
			rlAssert((m_PendingRequests.size() + m_ActiveRequests.size() + m_FreeRequests.size()) == MAX_REQUESTS);

			return request;
		}
		rcatchall
		{
		}

		return NULL;
	}

	void FreeRequest(rlXblPlayerNamesRequest* request)
	{
		bool removed = false;

		if(IsRequestPending(request))
		{
			m_PendingRequests.erase(request);
			removed = true;
		}
		else if(IsRequestActive(request))
		{
			m_ActiveRequests.erase(request);
			removed = true;
		}

		if(removed)
		{
			m_FreeRequests.push_back(request);
			rlDebug3("Removed player name request %u. Num Pending Requests: %" SIZETFMT "u, Num Active Requests: %" SIZETFMT "u, Num Free Requests: %" SIZETFMT "u", request->GetRequestId(), m_PendingRequests.size(), m_ActiveRequests.size(), m_FreeRequests.size());
			rlAssert((m_PendingRequests.size() + m_ActiveRequests.size() + m_FreeRequests.size()) == MAX_REQUESTS);
			request->Clear();
		}
	}

	bool AddRequest(const int localGamerIndex, const rlGamerHandle* gamerhandles, u32 gamerHandleSpan, const unsigned numGamerhandles, rlDisplayName* displayNames, u32 displayNameSpan, rlGamertag* gamertags, u32 gamertagSpan, netStatus* status)
	{
		bool success = false;

		rtry			
		{
			rverify(FindActiveRequest(status) == false, catchall, rlError("Player name request with this status object already exists in active list"));
			rverify(FindPendingRequest(status) == false, catchall, rlError("Player name request with this status object already exists in pending list"));

			rlXblPlayerNamesRequest* request = AllocateRequest();
			rverify(request, catchall, );
			if(request->Init(localGamerIndex, gamerhandles, gamerHandleSpan, numGamerhandles, displayNames, displayNameSpan, gamertags, gamertagSpan, status))
			{
				success = true;
			}
			else
			{
				FreeRequest(request);
			}
		}
		rcatchall
		{

		}

		return success;
	}

	bool ProcessUserNames(const rlGamerHandle& gh, const char* displayName, const char* gamertag)
	{
		RequestList::iterator it = m_ActiveRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_ActiveRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			rqst->ProcessUserNames(gh, displayName, gamertag);
			if(rqst->IsFulfilled())
			{
				rlDebug3("Active request %d fulfilled after %u ms", rqst->GetRequestId(), rqst->GetElapsedMilliseconds());
				FreeRequest(rqst);
			}
		}

		// we may have fulfilled some pending requests as well if
		// they contain gamerhandles that an active request also had
		it = m_PendingRequests.begin();
		next = it;
		stop = m_PendingRequests.end();
		for(++next; stop != it; it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			rqst->ProcessUserNames(gh, displayName, gamertag);
			if(rqst->IsFulfilled())
			{
				rlDebug3("Pending request %u fulfilled by previous lookup after %u ms", rqst->GetRequestId(), rqst->GetElapsedMilliseconds());
				FreeRequest(rqst);
			}
		}

		return true;
	}


private:
	rlXblPlayerNamesRequest m_Requests[MAX_REQUESTS];
	RequestList m_ActiveRequests;
	RequestList m_PendingRequests;
	RequestList m_FreeRequests;

	netStopWatch m_Timer;
	netStatus m_Status;
};

///////////////////////////////////////////////////////////////////////////////
//  XblGetPlayerNamesTask Definition
///////////////////////////////////////////////////////////////////////////////

XblGetPlayerNamesTask::XblGetPlayerNamesTask() {}
XblGetPlayerNamesTask::~XblGetPlayerNamesTask() {}

bool XblGetPlayerNamesTask::Configure(int localGamerIndex, rlXblPlayerNamesBatch* batch)
{
	bool success = false;

	rtry
	{
		rverify(batch != NULL, catchall, );
		m_Batch = batch;
		m_Settings = 0;
		m_TimeOutSecs = TIMEOUT_SECS;

		// begin the HTTP request but don't commit it yet. This puts us into the HTTP queue.
		// Once we move out of the queue and become an in-flight HTTP request, we coalesce
		// all pending requests again, just before committing the HTTP request to the server.
		// This way, if there are too many in-flight HTTP requests, we effectively use the
		// time in the HTTP queue as part of our batching time.
		rverify(rlXblHttpTask::Configure(localGamerIndex), catchall, );

		RequestList::iterator it = m_Batch->m_PendingRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_Batch->m_PendingRequests.end();
		for(++next; (stop != it); it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			m_Batch->ActivatePendingRequest(rqst);
		}

		m_RequestState = STATE_WAITING_FOR_READY;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void XblGetPlayerNamesTask::Start()
{
	rlTaskDebug2("Start");

	rlTaskBase::Start();

	m_StartTime = sysTimer::GetSystemMsTime();
}

void XblGetPlayerNamesTask::Update(const unsigned timeStep)
{
	rlXblHttpProfileSettingsTask::Update(timeStep);

	// this task can be finished in the rlXblHttpProfileSettingsTask::Update 
	if(IsFinished())
	{
		return;
	}

	switch(m_RequestState)
	{
	case STATE_WAITING_FOR_READY:
		if(WasCanceled())
		{
			this->Finish(FINISH_CANCELED);
		}
		else
		{
			if(m_HttpRequest.CanRun())
			{
				// fill the request and send
				if(!this->SendBatchRequest())
				{
					this->Finish(FINISH_FAILED, -1);
				}
				else
				{
					m_RequestState = STATE_RECEIVING;
				}
			}
		}
		break;
	case STATE_RECEIVING:
		break;
	}
}

bool XblGetPlayerNamesTask::AppendContentToHttpRequest()
{
	bool success = false;

	rtry
	{
		// could legitimately have 0 gamer handles if all the pending requests were
		// canceled between configuring the task and starting it
		rcheck(m_NumGamerHandles > 0, catchall, );
		rverify(m_NumGamerHandles <= COUNTOF(m_GamerHandles), catchall, );
		rverify(m_Settings != 0, catchall, );

		RsonWriter rw;
		char buf[4096] = {0}; // allows for at least 100 xuids of max length
		rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.BeginArray("userIds", NULL), catchall, );

		for(unsigned i = 0; i < m_NumGamerHandles; ++i)
		{
			char szXuid[32] = {0};
			formatf(szXuid, "%" I64FMT "u", m_GamerHandles[i].GetXuid());
			rcheck(rw.WriteString(NULL, szXuid), catchall, );
		}

		rcheck(rw.End(), catchall, );

		rcheck(rw.BeginArray("settings", NULL), catchall, );

#define RL_XBL_HTTP_WRITE_SETTING_STRING(setting) \
		if(m_Settings & setting) {rcheck(rw.WriteString(NULL, GetSettingName(setting)), catchall, );}

		RL_XBL_HTTP_WRITE_SETTING_STRING(GAMEDISPLAYNAME);
		RL_XBL_HTTP_WRITE_SETTING_STRING(DISPLAYPIC);
		RL_XBL_HTTP_WRITE_SETTING_STRING(GAMERSCORE);
		RL_XBL_HTTP_WRITE_SETTING_STRING(GAMERTAG);
		RL_XBL_HTTP_WRITE_SETTING_STRING(PUBLICGAMERPIC);
#undef RL_XBL_HTTP_WRITE_SETTING_STRING

		rcheck(rw.End(), catchall, );
		rcheck(rw.End(), catchall, );

		rverify(AppendContent(rw.ToString(), rw.Length()), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool XblGetPlayerNamesTask::CoalesceBatch()
{
	bool success = false;

	rtry
	{
		// build a list of unique xuids from all pending requests
		m_NumGamerHandles = 0;

		RequestList::iterator it = m_Batch->m_PendingRequests.begin();
		RequestList::iterator next = it;
		RequestList::const_iterator stop = m_Batch->m_PendingRequests.end();
		for(++next; (stop != it); it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;
			m_Batch->ActivatePendingRequest(rqst);
		}

		rlTaskDebug2("Building list of unique xuids from %" SIZETFMT "u active requests", m_Batch->m_ActiveRequests.size());

		it = m_Batch->m_ActiveRequests.begin();
		next = it;
		stop = m_Batch->m_ActiveRequests.end();
		for(++next; (stop != it) && (m_NumGamerHandles < COUNTOF(m_GamerHandles)); it = next, ++next)
		{
			rlXblPlayerNamesRequest* rqst = *it;

			u32 numGamerHandles = rqst->m_NumGamerHandles;
			for(u32 i = 0; (i < numGamerHandles) && (m_NumGamerHandles < COUNTOF(m_GamerHandles)); ++i)
			{
				rlGamerHandle gh = rqst->m_GamerHandles[i];

				if(!rlVerify(gh.IsValid()))
				{
					continue;
				}

				// it's possible to have partially fulfilled a request already
				if(rqst->IsFulfilled(gh) == false)
				{
					if(!HaveGamerHandle(gh))
					{
						rlTaskDebug2("adding unique xuid %" I64FMT "u to list", gh.GetXuid());
						m_GamerHandles[m_NumGamerHandles] = gh;
						++m_NumGamerHandles;
					}
					else
					{
						rlTaskDebug2("not adding xuid %" I64FMT "u since it's already in our list (duplicate)", gh.GetXuid());
					}
				}
				else
				{
					rlTaskDebug2("not adding xuid %" I64FMT "u since it's already fulfilled", gh.GetXuid());
				}
			}
		}

		rlTaskDebug2("Coalesced batch contains %u unique xuids from %" SIZETFMT "u requests", m_NumGamerHandles, m_Batch->m_ActiveRequests.size());

		// for now, always request display name and gamertag
		m_Settings = rlXblHttpProfileSettingsTask::GAMEDISPLAYNAME | rlXblHttpProfileSettingsTask::GAMERTAG;

		success = true;
	}
	//rcatchall
	{

	}

	return success;
}

bool XblGetPlayerNamesTask::SendBatchRequest()
{
	bool success = false;

	rtry
	{
		m_NumNamesProcessed = 0;

		rverify(CoalesceBatch(), catchall, );
		rcheck(AppendContentToHttpRequest(), catchall, );
		rverify(SendRequest(), catchall, );
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool XblGetPlayerNamesTask::ProcessUserNames(u64 xuid, const char* displayName, const char* gamertag, int& resultCode)
{
	bool success = false;

	rtry
	{
		rlGamerHandle gh;
		gh.ResetXbl(xuid);
		rverify(gh.IsValid(), catchall, );
		resultCode = 0;

// 		if(displayName)
// 		{
// 			rlDebug3("Display Name for XUID: %" I64FMT "u is %s", gh.GetXuid(), displayName);
// 		}
// 
// 		if(gamertag)
// 		{
// 			rlDebug3("Gamertag for XUID: %" I64FMT "u is %s", gh.GetXuid(), gamertag);
// 		}

		rlAssert(FindGamerHandleIndex(gh, m_NumNamesProcessed) >= 0);

		CachePlayerNameEntry(gh, *(rlDisplayName*)displayName, *(rlGamertag*)gamertag);

		m_Batch->ProcessUserNames(gh, displayName, gamertag);

		m_NumNamesProcessed++;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblProfiles
///////////////////////////////////////////////////////////////////////////////
rlXblPlayerNamesBatch s_PlayerNamesBatch;

rlXblProfiles::rlXblProfiles()
: m_Initialized(false)
{

}

rlXblProfiles::~rlXblProfiles()
{

}

bool 
rlXblProfiles::Init()
{
	Clear();
	s_PlayerNamesBatch.Init();
	m_Initialized = true;
	return true;
}

void 
rlXblProfiles::Clear()
{
	s_PlayerNamesBatch.Clear();
	m_Initialized = false;
}

void 
rlXblProfiles::Shutdown()
{
	Clear();
}

void
rlXblProfiles::OnSignOut()
{
    s_PlayerNamesBatch.CancelAll();
}

void 
rlXblProfiles::Update()
{
	if(m_Initialized)
	{
		s_PlayerNamesBatch.Update();
	}
}

void 
rlXblProfiles::CancelPlayerNameRequest(netStatus* status)
{
	s_PlayerNamesBatch.CancelRequest(status);
}

bool 
rlXblProfiles::GetPlayerNames(const int localGamerIndex, const rlGamerHandle* gamerHandles, const u32 gamerHandleSpan, const unsigned numGamerhandles, rlDisplayName* displayNames, const u32 displayNameSpan, rlGamertag* gamertags, const u32 gamertagSpan, netStatus* status)
{
	bool success = false;

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(gamerHandles, catchall, );
		rverify(numGamerhandles > 0, catchall, );
		rverify(status, catchall, );
		rverify(status->Pending() == false, catchall, );
		rverify(displayNames || gamertags, catchall, rlError("Must request either displayNames or gamertags or both"));
		
		atSpanArray<const rlGamerHandle> spanArrayGamerHandles;
		spanArrayGamerHandles.Init(gamerHandles, gamerHandleSpan, numGamerhandles);

		atSpanArray<rlDisplayName> spanArrayDisplayNames;
		if(displayNames)
		{
			spanArrayDisplayNames.Init(displayNames, displayNameSpan, numGamerhandles);
		}

		atSpanArray<rlGamertag> spanArrayGamerTags;

		if(gamertags)
		{
			spanArrayGamerTags.Init(gamertags, gamertagSpan, numGamerhandles);
		}

		bool fulfilledFromCache = true;

		for(u32 i = 0; i < numGamerhandles; ++i)
		{
			rlDisplayName dn = {0};
			rlGamertag gt = {0};

			rlAssert(spanArrayGamerHandles[i].IsValid());

            const bool fromCache = GetCachedPlayerNameEntry(spanArrayGamerHandles[i], dn, gt);
			if(!fromCache)
			{
				fulfilledFromCache = false;
			}

#if !__NO_OUTPUT
            char szHandleString[RL_MAX_GAMER_HANDLE_CHARS];
            spanArrayGamerHandles[i].ToString(szHandleString, RL_MAX_GAMER_HANDLE_CHARS);
            rlDebug("GetPlayerNames :: Requesting %s, FromCache: %s", szHandleString, fromCache ? "True" : "False");
#endif

			// always copy, since it's either fulfilled from the cache or 
			// we want to clear the result otherwise we'll think
			// it's already fulfilled
			if(displayNames)
			{
				safecpy(spanArrayDisplayNames[i], (char*)dn, sizeof(rlDisplayName));
			}

			if(gamertags)
			{
				safecpy(spanArrayGamerTags[i], (char*)gt, sizeof(rlGamertag));
			}
		}

		if(fulfilledFromCache)
		{
			rlDebug("GetPlayerNames :: Request with %u handles fulfilled from cache", numGamerhandles);
			status->ForceSucceeded();
		}
		else
		{
			rverify(s_PlayerNamesBatch.AddRequest(localGamerIndex, gamerHandles, gamerHandleSpan, numGamerhandles, displayNames, displayNameSpan, gamertags, gamertagSpan, status), catchall, );
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
rlXblProfiles::GetPlayerNames(const int localGamerIndex, const rlGamerHandle* gamerHandles, const unsigned numGamerhandles, rlDisplayName* displayNames, rlGamertag* gamertags, netStatus* status)
{
	return GetPlayerNames(localGamerIndex, gamerHandles, sizeof(rlGamerHandle), numGamerhandles, displayNames, sizeof(rlDisplayName), gamertags, sizeof(rlGamertag), status);
}

} // namespace rage

#endif  //RSG_DURANGO
