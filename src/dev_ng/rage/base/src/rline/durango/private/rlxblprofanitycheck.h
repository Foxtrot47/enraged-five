// 
// rlxblprofanitycheck.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PROFANITYCHECK_H
#define RLXBL_PROFANITYCHECK_H

#if RSG_DURANGO

#include "rline/durango/rlXblProfanityCheck_interface.h"

namespace rage
{

class netStatus;

//PURPOSE
// Interface to the profanity check on Durango
class rlXblProfanityCheck : public IProfanityCheck
{
public:

	//PURPOSE
	// Check text profanity.
	virtual bool ProfanityCheck(const int localGamerIndex, const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* numProfaneWord, netStatus* status);

};

} // namespace rage

#endif // RSG_DURANGO
#endif // RLXBL_PROFANITYCHECK_H