// 
// rlxblsystemui.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_SYSTEMUI_H
#define RLXBL_SYSTEMUI_H

#if RSG_DURANGO
#include "../rlxblsystemui_interface.h"
#include "system/service.h"

#if !defined(__cplusplus_winrt)
#error "rlxblsystemui.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblsystemui_interface.h instead"
#endif

namespace rage
{

//PURPOSE
// Interface to the system UI on Durango
class rlXblSystemUi : public ISystemUi
{
public:
	rlXblSystemUi();
	virtual ~rlXblSystemUi();

	//PURPOSE
	// Returns whether the Durango UI is currently being displayed
	virtual bool IsUiShowing();

	//PURPOSE
	// Show the system UI for Signing In
	virtual bool ShowSigninUi(const int localGamerIndex);

	//PURPOSE
	// Show the system UI for Gamer Profiles
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the UI
	// gamerHandle - Gamer handle of the profile of which to display
	virtual bool ShowGamerProfileUi(const int localGamerIndex, const rlGamerHandle& gamerHandle);

	//PURPOSE
	// Shows the system UI for game invites
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the UI
	virtual bool ShowSendInvitesUi(const int localGamerIndex);

	//PURPOSE
	// Shows the system UI for parties
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the UI
	virtual bool ShowPartySessionsUi(const int localGamerIndex);

	//PURPOSE
	// Shows the system UI for web browser
	//PARAMS
	// url - the URL to load
	virtual bool ShowWebBrowser(const int localGamerIndex, const char * url);

	//PURPOSE
	// Shows the XboxOne app help menu
	//PARAMS
	// url - the URL to load
	virtual bool ShowAppHelpMenu(const int localGamerIndex);

	//PURPOSE
	// Shows the system UI for adding/removing friends
	virtual bool ShowAddRemoveFriend(const int localGamerIndex, u64 targetXuid);

	//PURPOSE
	// Shows the system UI for composing messages
	virtual bool ShowComposeMessage(const int localGamerIndex, const char * defaultText, const rlGamerHandle* recipients,  const unsigned numRecipients);

	//PURPOSE
	//  Initialize the system UI
	bool Init();

	//PURPOSE
	//  Shut down the system UI
	void Shutdown();

	//PURPOSE
	//  Update the the system UI.  Should be called once per frame.
	void Update();

private:
	//PURPOSE
	//  Callback when the application window input focus has changed.
	static void OnFocusChanged(sysServiceEvent *evt);

	static bool sm_HasFocus;
	static ServiceDelegate sm_Delegate;

	static netStatus sm_SystemUiStatus;
};


} // namespace rage

#endif // RSG_DURANGO
#endif // RLXBL_SYSTEMUI_H