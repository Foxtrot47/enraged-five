// 
// rlxblinternal.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_INTERNAL_H
#define RLXBL_INTERNAL_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblinternal.h is a private header. Do not #include it. Use rlxbl_interface.h instead"
#endif

#include "../rlxbl_interface.h"
#include "diag/channel.h"
#include "rline/rl.h"
#include "rlxblachievements.h"
#include "rlxblcommerce.h"
#include "rlxblparty.h"
#include "rlxblplayers.h"
#include "rlxblpresence.h"
#include "rlxblprivileges.h"
#include "rlxblprofiles.h"
#include "rlxblrealtimeactivity.h"
#include "rlxblsessions.h"
#include "rlxblsystemui.h"
#include "rlXblProfanityCheck.h"
#include "system/criticalsection.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, xbl)
RAGE_DECLARE_SUBCHANNEL(rline_xbl, tasks)

class sysMemAllocator;

//PURPOSE
//  Private interface to Xbox Live on Durango.
class rlXblInternal
{
public:
	static rlXblInternal* GetInstance();

	//PURPOSE
	//  Returns the region for which the game was built.
	static rlGameRegion GetGameRegion();

	//PURPOSE
    //  Initialize the class.
    bool Init(sysMemAllocator* allocator);

    //PURPOSE
    //  Shut down the class.
    void Shutdown();

    //PURPOSE
    //  Update the class.  Should be called once per frame.
    void Update();

	bool IsInitialized();
	sysMemAllocator* GetAllocator();

	//PURPOSE
	//  These functions return public interfaces.
	//AchievementManager* GetAchievementManager();
	IPlayers* GetPlayerManager();
	IPresence* GetPresenceManager();
	IProfiles* GetProfilesManager();
	IPrivileges* GetPrivilegesManager();
	ISessions* GetSessionManager();
	ISystemUi* GetSystemUi();
	IAchievements* GetAchievementsManager();
	IParty* GetPartyManager();
	IRealTimeActivity* GetRealtimeActivityManager();
	ICommerce* GetPlatformCommerceManager();
	IProfanityCheck* GetProfanityCheckManager();

	//PURPOSE
	//  These functions return full (public and private) interfaces.
	rlXblPlayers* _GetPlayerManager();
	rlXblPresence* _GetPresenceManager();
	rlXblProfiles* _GetProfilesManager();
	rlXblPrivileges* _GetPrivilegesManager();
	rlXblSessions* _GetSessionManager();
	rlXblSystemUi* _GetSystemUi();
	rlXblAchievements* _GetAchievementsManager();
	rlXblParty* _GetPartyManager();
	rlXblRealTimeActivity* _GetRealtimeActivityManager();
	rlXblCommerce* _GetPlatformCommerceManager();
	rlXblProfanityCheck* _GetProfanityCheckManager();

	//PURPOSE
	//  Add/remove a delegate that will be called with event notifications.
	void AddDelegate(rlXblEventDelegate* dlgt);
	void RemoveDelegate(rlXblEventDelegate* dlgt);
	void DispatchEvent(const rlXblEvent* e);

	//PURPOSE
	//  Utility functions
	static String^ GetGlobalReputationServiceConfigId(); 

	bool HasPendingInvite() const;

private:
	rlXblInternal();
	virtual ~rlXblInternal();

	sysMemAllocator* m_Allocator;
	bool m_Initialized;
	rlXblPresence m_Presence;
	rlXblProfiles m_Profiles;
	rlXblPrivileges m_Privileges;
	rlXblPlayers m_Players;
	rlXblSessions m_Sessions;
	rlXblSystemUi m_SystemUi;
	rlXblAchievements m_Achievements;
	rlXblParty m_Party;
	rlXblRealTimeActivity m_RealTimeActivity;
	rlXblCommerce m_PlatformCommerce;
	rlXblProfanityCheck m_ProfanityCheck;

	mutable sysCriticalSectionToken m_Cs;
	rlXblEventDelegator m_Delegator;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_INTERNAL_H
