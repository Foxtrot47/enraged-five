// 
// rlxblsessions.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlxblpresence.h"
#include "rlxblsessions.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "data/base64.h"
#include "diag/seh.h"
#include "net/task.h"
#include "rline/rldiag.h"
#include "rline/rlpresence.h"
#include "rline/rlsessionconfig.h"
#include "rline/rlprivileges.h"
#include "rline/ros/rlros.h"
#include "rline/rltitleid.h"
#include "string/unicode.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"
#include "system/xtl.h"
#include <xdk.h>
#include <collection.h>

#include <winerror.h>

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Storage::Streams;
using namespace Windows::Data::Json;
using namespace Microsoft::Xbox::Services::Matchmaking;
using namespace Microsoft::Xbox::Services::Multiplayer;
using namespace Windows::Xbox::ApplicationModel::Store;

#define TICKS_PER_SECOND 10000000i64
#define TICKS_PER_MILLISECOND 10000i64
#define SECONDS_PER_DAY 86400

namespace rage
{

static String^ m_ServiceConfigurationId = nullptr;
static String^ m_SessionTemplateName = nullptr;

RAGE_DEFINE_SUBCHANNEL(rline_xbl, session)
#undef __rage_channel
#define __rage_channel rline_xbl_session

extern const rlTitleId* g_rlTitleId;

PARAM(nosessioncleanup, "Does not attempt session cleanup on startup.");

// session tunable timeouts
static unsigned s_AsyncSessionTimeoutMs = ISessions::DEFAULT_ASYNC_SESSION_TIMEOUT_MS;
static unsigned s_MemberReservedTimeoutMs = ISessions::DEFAULT_MEMBER_RESERVED_TIMEOUT_MS;
static unsigned s_MemberInactiveTimeoutMs = ISessions::DEFAULT_MEMBER_INACTIVE_TIMEOUT_MS;
static unsigned s_MemberReadyTimeoutMs = ISessions::DEFAULT_MEMBER_READY_TIMEOUT_MS;
static unsigned s_SessionEmptyTimeoutMs = ISessions::DEFAULT_SESSION_EMPTY_TIMEOUT_MS;
static bool s_bRunValidateHost = true;
static bool s_bConsiderInvalidHostAsInvalidSession = true;

///////////////////////////////////////////////////////////////////////////////
//  rlXblSessionHandle
///////////////////////////////////////////////////////////////////////////////
rlXblSessionHandle::rlXblSessionHandle()
{
	sysMemSet(m_ServiceConfigurationId, 0, sizeof(m_ServiceConfigurationId));
	sysMemSet(m_SessionTemplateName, 0, sizeof(m_SessionTemplateName));
}

rlXblSessionHandle::rlXblSessionHandle(const wchar_t* serviceConfigurationId,
									   const wchar_t* sessionName,
									   const wchar_t* sessionTemplateName)
{
	Init(serviceConfigurationId, sessionName, sessionTemplateName);
}

void 
rlXblSessionHandle::Init(const wchar_t* serviceConfigurationId,
						 const wchar_t* sessionName,
						 const wchar_t* sessionTemplateName)
{
	safecpy(m_ServiceConfigurationId, serviceConfigurationId);
	safecpy(m_SessionTemplateName, sessionTemplateName);
	m_SessionName.FromWideString(sessionName);
}

void 
rlXblSessionHandle::Clear()
{
	m_SessionName.Clear();
	m_SessionTemplateName[0] = '\0';
	m_ServiceConfigurationId[0] = '\0';
}

bool
rlXblSessionHandle::operator==(const rlXblSessionHandle& that) const
{
	return m_SessionName == that.m_SessionName;
}

bool 
rlXblSessionHandle::operator!=(const rlXblSessionHandle& that) const
{
	return !(*this == that);
}

bool
rlXblSessionHandle::Import(const void* buf,
						   const unsigned sizeofBuf,
						   unsigned* size)
{
	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf); 

	char szAsciiScId[COUNTOF(m_ServiceConfigurationId)] = {0};
	char szAsciiTemplateName[COUNTOF(m_SessionTemplateName)] = {0};

	bool success = bb.ReadStr(szAsciiScId, sizeof(szAsciiScId))
				   && bb.ReadStr(szAsciiTemplateName, sizeof(szAsciiTemplateName))
				   && bb.SerUser(m_SessionName);

	if(success)
	{
		Utf8ToWide((char16*) m_ServiceConfigurationId, szAsciiScId, COUNTOF(m_ServiceConfigurationId));
		Utf8ToWide((char16*) m_SessionTemplateName, szAsciiTemplateName, COUNTOF(m_SessionTemplateName));
	}
	 
 	if(size){*size = success ? bb.GetNumBytesRead() : 0;}
 
 	return success;
}

bool 
rlXblSessionHandle::Export(void* buf,
						   const unsigned sizeofBuf,
						   unsigned* size) const
{
	// if you change the exported size, make sure
	// to change MAX_EXPORTED_SIZE_IN_BYTES

	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	char szAsciiScId[COUNTOF(m_ServiceConfigurationId)] = {0};
	char szAsciiTemplateName[COUNTOF(m_SessionTemplateName)] = {0};

	WideToAscii(szAsciiScId, (char16*)m_ServiceConfigurationId, sizeof(szAsciiScId));
	WideToAscii(szAsciiTemplateName, (char16*)m_SessionTemplateName, sizeof(szAsciiTemplateName));

	bool success = bb.WriteStr(szAsciiScId, sizeof(szAsciiScId))
				   && bb.WriteStr(szAsciiTemplateName, sizeof(szAsciiTemplateName))
				   && bb.SerUser(m_SessionName);

	rlAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool rlXblSessionHandle::IsValid() const
{
	return m_SessionName.IsValid() && m_SessionTemplateName[0] != 0 && m_ServiceConfigurationId[0] != 0;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblSessionName
///////////////////////////////////////////////////////////////////////////////
rlXblSessionName::rlXblSessionName()
{
	Clear();
}

void
rlXblSessionName::Clear()
{
	sysMemSet(m_GuidString, 0, sizeof(m_GuidString));
	sysMemSet(m_Guid, 0, sizeof(m_Guid));
}

void 
rlXblSessionName::SetWideString()
{
	GUID guid;
	sysMemCpy(&guid, m_Guid, sizeof(guid));

	formatf(m_GuidString,
			L"%08lx-%04hx-%04hx-%02x%02x-%02x%02x%02x%02x%02x%02x",
			guid.Data1, guid.Data2, guid.Data3, guid.Data4[0],
			guid.Data4[1], guid.Data4[2], guid.Data4[3], guid.Data4[4],
			guid.Data4[5], guid.Data4[6], guid.Data4[7]);
}

bool 
rlXblSessionName::CreateUniqueName(rlXblSessionName& sessionName)
{
	bool success = false;

	rtry
	{
		CompileTimeAssert(sizeof(sessionName.m_Guid) == sizeof(GUID));

		GUID guid;
		HRESULT hr = CoCreateGuid(&guid);
		rverify(SUCCEEDED(hr), catchall, );

		sysMemCpy(sessionName.m_Guid, &guid, sizeof(sessionName.m_Guid));

		sessionName.SetWideString();

		rlDebug3("uniqueSessionName: %ls", sessionName.m_GuidString);
	}
	rcatchall
	{

	}

	return success;
}

bool
rlXblSessionName::Import(const void* buf,
						 const unsigned sizeofBuf,
						 unsigned* size)
{
 	datBitBuffer bb;
 	bb.SetReadOnlyBytes(buf, sizeofBuf);
 
	bool success = bb.ReadBytes(m_Guid, sizeof(m_Guid));

	SetWideString();

 	if(size){*size = success ? bb.GetNumBytesRead() : 0;}
 
 	return success;
}

bool 
rlXblSessionName::operator==(const rlXblSessionName& that) const
{
	for(unsigned i = 0; i < sizeof(m_Guid); ++i)
	{
		if(m_Guid[i] != that.m_Guid[i])
		{
			return false;
		}
	}

	return true;
}

bool 
rlXblSessionName::operator!=(const rlXblSessionName& that) const
{
	return !(*this == that);
}

bool 
rlXblSessionName::Export(void* buf,
						 const unsigned sizeofBuf,
						 unsigned* size) const
{
	// if you change the exported size, make sure
	// to change MAX_EXPORTED_SIZE_IN_BYTES

	datBitBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	bool success = bb.WriteBytes(m_Guid, sizeof(m_Guid));

	rlAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool 
rlXblSessionName::FromWideString(const wchar_t* s)
{
	bool success = false;

	rtry
	{
		GUID guid;

		// Visual Studio doesn't support the %hhx length specifier for chars
		// so read in as unsigned shorts, then copy to unsigned chars
		unsigned short data[8] = {0};
		rverify(swscanf_s(s, L"%08lx-%04hx-%04hx-%02hx%02hx-%02hx%02hx%02hx%02hx%02hx%02hx", 
						  &guid.Data1, &guid.Data2, &guid.Data3, &data[0],
						  &data[1], &data[2], &data[3], &data[4],
						  &data[5], &data[6], &data[7]) == 11, catchall, );
				
		for(unsigned i = 0; i < 8; ++i)
		{
			guid.Data4[i] = data[i];
		}

		sysMemCpy(m_Guid, &guid, sizeof(m_Guid));
		safecpy(m_GuidString, s);
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

const wchar_t* 
rlXblSessionName::ToWideString() const
{
	return m_GuidString;
}

bool rlXblSessionName::IsValid() const
{
	for(unsigned i = 0; i < sizeof(m_Guid); ++i)
	{
		if(m_Guid[i] != 0)
		{
			return true;
		}
	}

	return false;
}

class rlXblSession
{
public:

	rlXblSession();

	bool Init(int localGamerIndex,
			  MultiplayerSession^ session);

	void Update();

	void Shutdown(netStatus* status);

	void Clear();

	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ GetSessionReference() const {return m_SessionReference;}

	void Join(const int localGamerIndex);

	void MigrateHost(const int localGamerIndex, const rlSessionToken& nToken);
	void ValidateHost(MultiplayerSession^ session);

	void ChangeAttributes(const rlMatchingAttributes& attrs);

	void SetCorrelationId(String^ correlationId);
	String^ GetCorrelationId() const {return m_CorrelationId;}

	bool IsHost() const { return m_IsHost; }
	bool HasJoined() const { return m_HasJoined; }

	int GetLocalGamerIndex() const {return m_LocalGamerIndex;}

	rlXblSessionHandle* GetXblSessionHandle() {return &m_Handle;}

	inlist_node<rlXblSession> m_ListLink;

private:
	bool m_IsHost;
	bool m_HasJoined;
	rlXblSessionHandle m_Handle;
	rlSessionToken m_Token;
	
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ m_SessionReference;
	int m_LocalGamerIndex;
	String^ m_CorrelationId;
};

rlXblSession::rlXblSession()
{
	Clear();
}

bool 
rlXblSession::Init(int localGamerIndex,
				   MultiplayerSession^ session)
{
	bool success = false;

	rtry
	{
		Clear();

		rverify(session != nullptr, catchall, rlError("Session pointer is null"));

		m_IsHost = false;
		m_HasJoined = false;

		m_LocalGamerIndex = localGamerIndex;

		m_SessionReference = session->SessionReference;

		m_Handle.Init(m_SessionReference->ServiceConfigurationId->Data(),
					  m_SessionReference->SessionName->Data(),
					  m_SessionReference->SessionTemplateName->Data());

		rlDebug("rlXblSession::Init - ServiceConfigurationId: %ls", m_SessionReference->ServiceConfigurationId->Data());
		rlDebug("rlXblSession::Init - SessionName: %ls", m_SessionReference->SessionName->Data());
		rlDebug("rlXblSession::Init - SessionTemplateName: %ls", m_SessionReference->SessionTemplateName->Data());

		success = true;
	}
	rcatchall
	{
		rlError("Failed to initialise session");
	}

	return success;
}

void 
rlXblSession::Clear()
{
	m_Handle = rlXblSessionHandle();
	m_IsHost = false;
	m_HasJoined = false;
	m_SessionReference = nullptr;
	m_LocalGamerIndex = -1;
	m_CorrelationId = nullptr;
}

void 
rlXblSession::Update()
{

}

void 
rlXblSession::Shutdown(netStatus* status)
{
	// shutting down the local session is no longer an async task but
	// keeping the interface the same in case we need to do something
	// async in the future.
	if(status)
	{
		status->SetPending();
		status->SetSucceeded();
	}

	rlXblInternal::GetInstance()->_GetSessionManager()->DeleteSession(this);
}

void 
rlXblSession::Join(const int localGamerIndex)
{
	m_HasJoined = localGamerIndex == m_LocalGamerIndex;
}

void 
rlXblSession::MigrateHost(const int localGamerIndex, const rlSessionToken& nToken)
{
	if(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && (localGamerIndex == m_LocalGamerIndex))
	{
		// local session owner is the new host
		m_IsHost = true;
		m_Token = nToken;
	}
	else
	{
		// remote gamer is the new host
		m_IsHost = false;
	}
}

void 
rlXblSession::ChangeAttributes(const rlMatchingAttributes& UNUSED_PARAM(attrs))
{

}

void
rlXblSession::SetCorrelationId(String^ correlationId)
{
	if(m_CorrelationId != correlationId)
	{
		netDebug("SetCorrelationId :: New: %ls, Old: %ls", correlationId->Data(), m_CorrelationId->Data());
		m_CorrelationId = correlationId;
	}
}

void 
rlXblSession::ValidateHost(MultiplayerSession^ session)
{
	if(!s_bRunValidateHost)
	{
		// do not run unless this is true
		return;
	}

	if(!m_IsHost)
	{
		// only do this if we're flagged as host
		return;
	}

	if(session == nullptr)
	{
		// invalid session, nothing to write to
		rlError("rlXblSession::ValidateHost - Invalid session");
		return;
	}

	// assume false
	bool bIsHostValid = false;

	JsonObject^ jsonSessionCustomProperties;
	bool success = JsonObject::TryParse(session->SessionProperties->SessionCustomPropertiesJson, &jsonSessionCustomProperties);

	if(success)
	{
		if(jsonSessionCustomProperties->HasKey(L"customProperties"))
		{
			String^ hostXuid = jsonSessionCustomProperties->GetNamedObject("customProperties")->GetNamedString(L"host");
			if(hostXuid != nullptr)
			{
				// retrieve local XUID
				u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(m_LocalGamerIndex);
				String^ winRtXuid = rlXblCommon::XuidToWinRTString(xuid);

				// check the session copy matches the correct XUID
				bIsHostValid = (hostXuid == winRtXuid);
			}

		}
	}

	if(!bIsHostValid)
	{
		rlDebug("rlXblSession::ValidateHost - Failed, writing correct properties");
		OUTPUT_ONLY(rlXblSessions::LogSessionDetail(session, "ValidateHost", true);)
		rlXblInternal::GetInstance()->_GetSessionManager()->WriteCustomProperties(&m_Handle, m_Token, NULL);
	}
}

#pragma region GetCloudSessionTask

#if RSG_GDK

///////////////////////////////////////////////////////////////////////////////
//  GetCloudSessionTask
///////////////////////////////////////////////////////////////////////////////
class GetCloudSessionTask : public SessionManagementTask
{
public:

	NET_TASK_DECL(GetCloudSessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,
		STATE_GETTING_SESSION,
	};

	GetCloudSessionTask();

	bool Configure(	const int localGamerIndex, 
					const std::string& sessionName, 
					const std::string& sessionTemplateName, 
					const std::string& sessionConfigurationId,
					rlSessionInfo* out_sessionInfo);

	virtual void OnCleanup() override;
	virtual netTaskStatus OnUpdate(int* /*resultCode*/) override;
	void Complete(const netTaskStatus status);

	bool GetSessionAsync();

	XBLMultiplayerSessionType m_CloudSession;		// internal session handle, returned from MPSD
	rlSessionInfo* m_SessionInfo;

	std::string	m_SessionName;
	std::string	m_SessionConfigurationId;
	std::string	m_SessionTemplateName;
	XblMultiplayerSessionReference m_SessionRef;	// Populated from input strings for retrieving session

	int m_State;
};

GetCloudSessionTask::GetCloudSessionTask()
	: m_State(STATE_GET_SESSION)
	, m_CloudSession(NULL)
	, m_SessionInfo(NULL)
{
}

void GetCloudSessionTask::OnCleanup()
{
	SessionManagementTask::OnCleanup();
	GDK_ONLY(SafeCloseSessionHandle(m_CloudSession));
}

bool
GetCloudSessionTask::Configure(	const int localGamerIndex,
							const std::string& sessionName,
							const std::string& sessionTemplateName,
							const std::string& sessionConfigurationId,
							rlSessionInfo* out_sessionInfo)
{
	bool success = false;

	rtry
	{
		rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);

		rverify(sessionName.length() > 0,			catchall, netTaskError("Invalid sessionName"));
		rverify(sessionTemplateName.length() > 0,	catchall, netTaskError("Invalid sessionTemplateName"));
		rverify(sessionConfigurationId.length() > 0,catchall, netTaskError("Invalid sessionConfigurationId"));
		rlAssert(out_sessionInfo != NULL);

		m_LocalGamerIndex			= localGamerIndex;
		m_SessionName				= sessionName;
		m_SessionTemplateName		= sessionTemplateName;
		m_SessionConfigurationId	= sessionConfigurationId;
		m_SessionInfo				= out_sessionInfo;

		SessionManagementTask::Configure(localGamerIndex);

		success = true;
	}
		rcatchall
	{

	}

	return success;
}

netTaskStatus
GetCloudSessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been canceled
	if (WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch (m_State)
	{

	case STATE_GET_SESSION:
		if (!this->GetSessionAsync())
		{
			netTaskError("GetSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Getting session...");
			m_State = STATE_GETTING_SESSION;
		}
		break;

	case STATE_GETTING_SESSION:
		m_XblStatus.Update();

		if (m_MyStatus.Succeeded())
		{
			netTaskDebug("Retrieved session");

			GDK_ONLY(SafeCloseSessionHandle(m_CloudSession));
			m_CloudSession = GetGetSessionAsyncResults();
			if (m_CloudSession != nullptr)
			{
				rlGamerInfo actingGamerInfo;
				rlPresence::GetGamerInfo(m_LocalGamerIndex, &actingGamerInfo);

				// Generate session info
				unsigned int numSessions;
				rlXblSessions::ConvertToSessionInfo(m_CloudSession, m_SessionInfo, &numSessions);

				status = NET_TASKSTATUS_SUCCEEDED;
			}
			else
			{
				netTaskError("GetSessionAsync succeeded but session is invalid");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if (!m_MyStatus.Pending())
		{
			netTaskError("Failed to retrieve session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if (NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool
GetCloudSessionTask::GetSessionAsync()
{
	bool success = false;

	XBLTry
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));
			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));
			rcheck(rlPresence::IsOnline(m_LocalGamerIndex), catchall, netTaskError("Local index %d not online", m_LocalGamerIndex));

			XBLContextType context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rverify(context, catchall, netTaskError("Invalid XboxLiveContext"));

			// Create a session reference with the supplied data
			m_SessionRef = XblMultiplayerSessionReferenceCreate(m_SessionConfigurationId.c_str(),
																m_SessionTemplateName.c_str(),
																m_SessionName.c_str());

			success = GetXblSessionFromSessionReferenceAsync(&m_SessionRef, m_LocalGamerIndex);
		}
		rcatchall
		{
		}
	}
	XBLCatchException(e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

void
GetCloudSessionTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall,);
	}
		rcatchall
	{

	}

	//Cleanup the update handle regardless of success
	GDK_ONLY(SafeCloseSessionHandle(m_CloudSession));
}

#pragma endregion GetCloudSessionTask

#pragma region SetMultiplayerActivityTask

///////////////////////////////////////////////////////////////////////////////
//  SetMultiplayerActivityTask
///////////////////////////////////////////////////////////////////////////////
class SetMultiplayerActivityTask : public SessionManagementTask
{
public:

	NET_TASK_DECL(SetMultiplayerActivityTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,
		STATE_GETTING_SESSION,
		STATE_SET_ACTIVITY,
		STATE_SETTING_ACTIVITY,
	};

	SetMultiplayerActivityTask();

	bool Configure(const int localGamerIndex, const rlXblSessionHandle* hSession);

	virtual void OnCleanup() override;
	virtual netTaskStatus OnUpdate(int* /*resultCode*/) override;
	void Complete(const netTaskStatus status);

	bool GetSessionAsync();
	bool SetMultiplayerActivity();

	const rlXblSessionHandle* m_Handle;

	rlXblSession* m_LocalSession;
	XBLMultiplayerSessionType m_CloudSession;

	int m_State;
	XblMultiplayerActivityInfo m_ActivityInfo;
};

SetMultiplayerActivityTask::SetMultiplayerActivityTask()
	: m_State(STATE_GET_SESSION)
	, m_CloudSession(NULL)
	, m_Handle(NULL)
{
}

void SetMultiplayerActivityTask::OnCleanup()
{
	SessionManagementTask::OnCleanup();
	GDK_ONLY(SafeCloseSessionHandle(m_CloudSession));
}

bool
SetMultiplayerActivityTask::Configure(const int localGamerIndex, const rlXblSessionHandle* hSession)
{
	bool success = false;

	rtry
	{
		rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
		rverify(hSession, catchall, netTaskError("Session handle is NULL"));
		m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(hSession);
		rverify(m_LocalSession, catchall, netTaskError("No local session matching handle"));

		m_Handle = hSession;
		m_LocalGamerIndex = localGamerIndex;

		SessionManagementTask::Configure(localGamerIndex);

		success = true;
	}
		rcatchall
	{

	}

	return success;
}

netTaskStatus
SetMultiplayerActivityTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if (WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch (m_State)
	{

	case STATE_GET_SESSION:
		if (!this->GetSessionAsync())
		{
			netTaskError("GetSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Getting session...");
			m_State = STATE_GETTING_SESSION;
		}
		break;

	case STATE_GETTING_SESSION:
		m_XblStatus.Update();

		if (m_MyStatus.Succeeded())
		{
			netTaskDebug("Retrieved session");

			GDK_ONLY(SafeCloseSessionHandle(m_CloudSession));
			m_CloudSession = GetGetSessionAsyncResults();
			if (m_CloudSession != nullptr)
			{
				m_State = STATE_SET_ACTIVITY;
			}
			else
			{
				netTaskError("GetSessionAsync succeeded but session is invalid");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if (!m_MyStatus.Pending())
		{
			netTaskError("Failed to retrieve session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_SET_ACTIVITY:
		if (!this->SetMultiplayerActivity())
		{
			netTaskError("SetActivity failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Setting Activity...");
			m_State = STATE_SETTING_ACTIVITY;
		}
		break;

	case STATE_SETTING_ACTIVITY:
		m_XblStatus.Update();

		if (m_MyStatus.Succeeded())
		{
			netTaskDebug("Activity has been set");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if (!m_MyStatus.Pending())
		{
			netTaskError("Failed to set activity with error 0x%08x", m_MyStatus.GetResultCode());
			OUTPUT_ONLY(rlXblSessions::LogSessionDetail(m_CloudSession, "SetMultiplayerActivityTask", true);)
				status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if (NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool
SetMultiplayerActivityTask::SetMultiplayerActivity()
{
	bool success = false;

	XBLTry
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));

			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));

			rlAssert(m_LocalGamerIndex >= 0 && m_LocalGamerIndex < RL_MAX_LOCAL_GAMERS);
			rverify(m_LocalSession, catchall, netTaskError("Invalid local session"));
			rcheck(rlPresence::IsOnline(m_LocalSession->GetLocalGamerIndex()), catchall, netTaskError("Local index %d not online", m_LocalSession->GetLocalGamerIndex()));

			XBLContextType context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalSession->GetLocalGamerIndex());
			rverify(context, catchall, netTaskError("Invalid XboxLiveContext"));

			//////////////////////////////////////////////////////////////////////////

			u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(m_LocalGamerIndex);
			int numPlayers = 0;
			int maxPlayers = 0;
			bool isClosed;		// Is session currently marked as closed
			XblMultiplayerActivityJoinRestriction activityRestriction;
			XblMultiplayerSessionRestriction sessionRestriction;

			// Attempt to read session properties
			success = ReadSessionPropertiesForActivity(m_CloudSession, sessionRestriction, maxPlayers, numPlayers, isClosed);
			if (!success)
			{
				return false;
			}
			// Try convert session restriction to activity join restriction
			success = ConvertJoinRestriction(sessionRestriction, activityRestriction);
			if (!success)
			{
				return false;
			}

			m_ActivityInfo.xuid = xuid;
			m_ActivityInfo.currentPlayers = numPlayers;
			m_ActivityInfo.maxPlayers = maxPlayers;

			// Set activity join to invite only if it's closed, otherwise use converted session restriction.
			// Not sure we need to set the restriction when the session is closed, but it makes sense to keep session & activity join restrictions aligned.
			m_ActivityInfo.joinRestriction = isClosed ? XblMultiplayerActivityJoinRestriction::InviteOnly : activityRestriction;

			// Construct local session reference handle
			const XblMultiplayerSessionReference* sessionReference = XblMultiplayerSessionSessionReference(m_CloudSession);
			rverify(sessionReference != nullptr, catchall, );

			// Pack session data into connection string (required for invitee to retrieve cloud session)
			std::string packedConnectionString;
			success = rlXblInviteData::PackSessionDataToString(sessionReference->SessionName, sessionReference->SessionTemplateName, sessionReference->Scid, packedConnectionString);
			if (!success)
			{
				return false;
			}

			// Set the connection string to the packed string, this will be available to the invitee/joiner.
			m_ActivityInfo.connectionString = packedConnectionString.c_str();

			// This is currently not used but cannot be empty
			m_ActivityInfo.groupId = "dummy";

			netTaskDebug("Players - %u / %u", m_ActivityInfo.currentPlayers, m_ActivityInfo.maxPlayers);
			netTaskDebug("Restriction - %u", static_cast<unsigned>(m_ActivityInfo.joinRestriction));
			netTaskDebug("ConnectionString - %s", m_ActivityInfo.connectionString);

			success = SetMultiplayerActivityAsync(&m_ActivityInfo, gs_AllowCrossPlatform, m_LocalSession->GetLocalGamerIndex());
		}
		rcatchall
		{
		}
	}
	XBLCatchException(e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool
SetMultiplayerActivityTask::GetSessionAsync()
{
	bool success = false;

	XBLTry
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));

			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));

			rverify(m_LocalSession, catchall, netTaskError("Invalid local session"));
			rcheck(rlPresence::IsOnline(m_LocalSession->GetLocalGamerIndex()), catchall, netTaskError("Local index %d not online", m_LocalSession->GetLocalGamerIndex()));

			XBLContextType context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalSession->GetLocalGamerIndex());
			rverify(context, catchall, netTaskError("Invalid XboxLiveContext"));

			//////////////////////////////////////////////////////////////////////////

			success = GetXblSessionFromSessionAsync(m_LocalSession, m_LocalSession->GetLocalGamerIndex());
		}
		rcatchall
		{
		}
	}
	XBLCatchException(e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

void
SetMultiplayerActivityTask::Complete(const netTaskStatus status)
{
	//Cleanup the update handle regardless of success
	GDK_ONLY(SafeCloseSessionHandle(m_CloudSession));

	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall,);
	}
		rcatchall
	{

	}
}

#pragma endregion SetMultiplayerActivityTask

#pragma region ClearMultiplayerActivityTask

///////////////////////////////////////////////////////////////////////////////
//  ClearMultiplayerActivityTask
///////////////////////////////////////////////////////////////////////////////
class ClearMultiplayerActivityTask : public SessionManagementTask
{
public:

	NET_TASK_DECL(ClearMultiplayerActivityTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_CLEAR_ACTIVITY,
		STATE_CLEARING_ACTIVITY,
	};

	ClearMultiplayerActivityTask();

	bool Configure(const int localGamerIndex);

	virtual void OnCleanup() override;
	virtual netTaskStatus OnUpdate(int* /*resultCode*/) override;
	void Complete(const netTaskStatus status);

	bool ClearMultiplayerActivity();

	int m_State;
};

ClearMultiplayerActivityTask::ClearMultiplayerActivityTask()
	: m_State(STATE_CLEAR_ACTIVITY)
{
}

void ClearMultiplayerActivityTask::OnCleanup()
{
	SessionManagementTask::OnCleanup();
}

bool
ClearMultiplayerActivityTask::Configure(const int localGamerIndex)
{
	bool success = false;

	rtry
	{
		rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));

		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, netTaskError("Invalid localGamerIndex: %d", localGamerIndex));
	
		m_LocalGamerIndex = localGamerIndex;

		SessionManagementTask::Configure(localGamerIndex);

		success = true;
	}
		rcatchall
	{
	}

	return success;
}

netTaskStatus
ClearMultiplayerActivityTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if (WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch (m_State)
	{
	case STATE_CLEAR_ACTIVITY:
		if (!this->ClearMultiplayerActivity())
		{
			netTaskError("Clear Activity failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Clearing Activity...");
			m_State = STATE_CLEARING_ACTIVITY;
		}
		break;

	case STATE_CLEARING_ACTIVITY:
		m_XblStatus.Update();

		if (m_MyStatus.Succeeded())
		{
			netTaskDebug("Activity has been cleared");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if (!m_MyStatus.Pending())
		{
			netTaskError("Failed to clear activity with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if (NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool
ClearMultiplayerActivityTask::ClearMultiplayerActivity()
{
	bool success = false;

	XBLTry
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));
			rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex), catchall, netTaskError("Invalid localGamerIndex: %d", m_LocalGamerIndex));
			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));

			// might have signed out since the task started
			rcheck(rlXblInternal::GetInstance()->_GetPresenceManager()->IsSignedIn(m_LocalGamerIndex), catchall, netTaskWarning("Not signed in"));
			
			XBLContextType context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rverify(context != nullptr, catchall, netTaskError("Invalid XboxLiveContext"));

			success = ClearMultiplayerActivityAsync(m_LocalGamerIndex);
		}
		rcatchall
		{
		}
	}
	XBLCatchException(e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

void
ClearMultiplayerActivityTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall,);
	}
	rcatchall
	{

	}
}

#pragma endregion ClearMultiplayerActivityTask

#endif // RSG_GDK

#pragma region CreateSessionTask

///////////////////////////////////////////////////////////////////////////////
//  CreateSessionTask
///////////////////////////////////////////////////////////////////////////////
class CreateSessionTask : public netTask
{
public:

	NET_TASK_DECL(CreateSessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,
		STATE_GETTING_SESSION,
		STATE_CREATE_SESSION,
		STATE_CREATING_SESSION,
		STATE_HANDLE_PERMISSIONS,
		STATE_HANDLING_PERMISSIONS,
		STATE_ALLOCATE_LOCAL_SESSION,
		STATE_ALLOCATING_LOCAL_SESSION,
	};

	CreateSessionTask();

	bool Configure(const int localGamerIndex,
				   const rlSessionInfo* sessionInfo,
				   const rlNetworkMode netMode,
				   const unsigned createFlags,
				   const unsigned presenceFlags,
				   const unsigned maxPublicSlots,
				   const unsigned maxPrivateSlots,
				   rlXblSessionHandle** hSession);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);
	
	bool GetSession();
	bool CreateSession();
	bool AllocateLocalSession();

	int m_LocalGamerIndex;
	rlNetworkMode m_NetMode;
	unsigned m_MaxPubSlots;
	unsigned m_MaxPrivSlots;
	unsigned m_CreateFlags;
	unsigned m_PresenceFlags;
	rlSessionInfo m_SessionInfo;
	
	bool m_AsHost;
	int m_State;
	String^ m_GameSessionName;
	MultiplayerSession^ m_Session;
	rlXblSessionHandle** m_Handle;
	rlXblSession* m_XblSession;
	rlSessionToken m_SessionToken;

	netStatus m_MyStatus;
	
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;

	bool m_bShownPrivelegesCheck;
};

CreateSessionTask::CreateSessionTask()
: m_NetMode(RL_NETMODE_INVALID)
, m_MaxPubSlots(0)
, m_MaxPrivSlots(0)
, m_CreateFlags(0)
, m_PresenceFlags(0)
, m_Handle(NULL)
, m_AsHost(false)
, m_State(STATE_CREATE_SESSION)
, m_bShownPrivelegesCheck(false)
{
}

bool
CreateSessionTask::Configure(const int localGamerIndex,
							 const rlSessionInfo* sessionInfo,
							 const rlNetworkMode netMode,
							 const unsigned createFlags,
							 const unsigned presenceFlags,
							 const unsigned maxPublicSlots,
							 const unsigned maxPrivateSlots,
							 rlXblSessionHandle** hSession)
{
	rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
	rlAssert(RL_NETMODE_LAN == netMode
		|| RL_NETMODE_OFFLINE == netMode
		|| RL_NETMODE_ONLINE == netMode);
	rlAssert(maxPublicSlots <= RL_MAX_GAMERS_PER_SESSION);
	rlAssert(maxPrivateSlots <= RL_MAX_GAMERS_PER_SESSION);
	rlAssert(maxPublicSlots + maxPrivateSlots <= RL_MAX_GAMERS_PER_SESSION);
	rlAssert(hSession);
	rlAssert(RL_NETMODE_ONLINE == netMode
		|| !(RL_SESSION_PRESENCE_FLAG_INVITABLE & presenceFlags));
	rlAssert(RL_NETMODE_ONLINE == netMode
		|| !(RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE & presenceFlags));

    m_LocalGamerIndex = localGamerIndex;
	m_NetMode = netMode;
	m_CreateFlags = createFlags;
	m_PresenceFlags = presenceFlags;
	m_MaxPubSlots = maxPublicSlots;
    m_MaxPrivSlots = maxPrivateSlots;
	m_Handle = hSession;

	if(sessionInfo)
	{
		m_SessionInfo = *sessionInfo;
	}
	else
	{
		m_SessionInfo.Clear();
	}

	m_AsHost = (NULL == sessionInfo);

	if(m_AsHost)
	{
		m_State = STATE_CREATE_SESSION;
	}
	else
	{
		m_State = STATE_GET_SESSION;
	}

    return true;
}

void 
CreateSessionTask::OnCancel()
{
	m_XblStatus.Cancel();
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
CreateSessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_GET_SESSION:
		if(!this->GetSession())
		{
			netTaskError("GetSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Getting session...");
			m_State = STATE_GETTING_SESSION;
		}
		break;

	case STATE_GETTING_SESSION:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Retrieved session");
			m_Session = m_XblStatus.GetResults<MultiplayerSession^>();

			if(m_Session != nullptr)
				m_State = STATE_ALLOCATE_LOCAL_SESSION;
			else
			{
				netTaskError("GetCurrentSessionAsync succeeded but session is invalid");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to get session with error code 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_CREATE_SESSION:
		if(!this->CreateSession())
		{
			netTaskError("CreateSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Creating session...");
			m_State = STATE_CREATING_SESSION;
		}
		break;

	case STATE_CREATING_SESSION:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			m_Session = m_XblStatus.GetResults<MultiplayerSession^>();
			if(m_Session == nullptr)
			{
				netTaskDebug("Creating session failed. Invalid session document!");
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				netTaskDebug("Created session");
				m_State = STATE_ALLOCATE_LOCAL_SESSION;
			}
		}
		else if(m_MyStatus.Failed())
		{
			s32 errCode = m_MyStatus.GetResultCode();
			if (errCode == HTTP_E_STATUS_FORBIDDEN && !m_bShownPrivelegesCheck)
			{
				netTaskDebug("Forbidden from creating session");
				m_bShownPrivelegesCheck = true;
				m_State = STATE_HANDLE_PERMISSIONS;
			}
			else
			{
				netTaskError("Failed to create session with error 0x%08x", m_MyStatus.GetResultCode());
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if(!m_MyStatus.Pending())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_HANDLE_PERMISSIONS:
		{
			// Wait until we can check privileges
			if (!rlPrivileges::IsPrivilegeCheckInProgress())
			{
				netTaskDebug("Checking privileges");
				rlPrivileges::CheckPrivileges(m_LocalGamerIndex, rlPrivileges::GetPrivilegeBit(static_cast<rlPrivileges::PrivilegeTypes>(rlPrivileges::PRIVILEGE_MULTIPLAYER_SESSIONS)), true);
				m_State = STATE_HANDLING_PERMISSIONS;
			}
		}
		break;
	case STATE_HANDLING_PERMISSIONS:
		{
			// Wait until our new privilege check is complete
			if (rlPrivileges::IsPrivilegeCheckResultReady())
			{
				rlXblPrivileges::ePrivilegeCheckResult result = g_rlXbl.GetPrivilegesManager()->GetPrivilegeCheckResult();

				// Update the privilege result
				rlPrivileges::UpdatePrivilege(m_LocalGamerIndex, rlPrivileges::PRIVILEGE_MULTIPLAYER_SESSIONS, result);
				if (result == rlXblPrivileges::PRIV_RESULT_NOISSUE)
				{
					netTaskDebug("Checked privileges - Success - Creating Session");
					m_State = STATE_CREATE_SESSION;
				}
				else
				{
					netTaskDebug("Checked privileges - Failed");
					status = NET_TASKSTATUS_FAILED;
				}

				// Free up the privileges for someone else
				rlPrivileges::SetPrivilegeCheckResultNotNeeded();
			}
		}
		break;

	case STATE_ALLOCATE_LOCAL_SESSION:
		{
			if(AllocateLocalSession())
			{
				netTaskDebug("Allocating local session...");
				m_State = STATE_ALLOCATING_LOCAL_SESSION;
			}
			else
			{
				netTaskError("AllocateLocalSession failed");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		break;

	case STATE_ALLOCATING_LOCAL_SESSION:
		{
			if(m_MyStatus.Succeeded())
			{
				netTaskDebug("Allocated local session");
				status = NET_TASKSTATUS_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool 
CreateSessionTask::GetSession()
{
    bool success = false;

    try
    {
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		const rlXblSessionHandle& handle = m_SessionInfo.GetSessionHandle();

		Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ ref = ref new Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference(ref new String(handle.m_ServiceConfigurationId),
																		ref new String(handle.m_SessionTemplateName),
																		ref new String(handle.m_SessionName.ToWideString()));

		success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
									context->MultiplayerService->GetCurrentSessionAsync(ref),
									s_AsyncSessionTimeoutMs,
									this,
									&m_MyStatus);
    }
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

    return success;
}

bool CreateSessionTask::CreateSession()
{
	bool success = false;

	try
	{
		rtry
		{
#if !__NO_OUTPUT
			const char* gamerName = rlXblInternal::GetInstance()->_GetPresenceManager()->GetName(m_LocalGamerIndex);
			rverify(gamerName, catchall, );

			netTaskDebug2("Gamer:\"%s\" creating session as %s:...", gamerName, m_AsHost ? "Host" : "Guest");

			netTaskDebug2("  Net mode:      %s",
				(RL_NETMODE_ONLINE == m_NetMode) ? "online" : (RL_NETMODE_LAN == m_NetMode) ? "LAN" : "offline");

			netTaskDebug2("  Presence:      %s",
				(m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED) ? "false" : "true");

			netTaskDebug2("  Invitable:     %s",
				(m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE) ? "true" : "false");

			netTaskDebug2("  JIP:           %s",
				(m_CreateFlags & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED) ? "false" : "true");

			netTaskDebug2("  Party:         %s",
				(m_CreateFlags & RL_SESSION_CREATE_FLAG_PARTY_SESSION_DISABLED) ? "false" : "true");

			//netTaskDebug2("  Stats:         %s",
			//            (xflags & XSESSION_CREATE_USES_STATS) ? "true" : "false");

			netTaskDebug2("  Slots:         %d(pub),%d(priv)", m_MaxPubSlots, m_MaxPrivSlots);

			//netTaskDebug2("  XFlags:        0x%08x", xflags);
#endif  //__NO_OUTPUT

			bool isSignedIn = rlXblInternal::GetInstance()->_GetPresenceManager()->IsSignedIn(m_LocalGamerIndex);

			//Make sure the gamer is signed in.
			rverify(isSignedIn, catchall, netTaskError("Cannot create session - gamer:\"%s\" is not signed in", gamerName));

			if(RL_NETMODE_ONLINE == m_NetMode)
			{
				rcheck(rlGamerInfo::HasMultiplayerPrivileges(m_LocalGamerIndex), catchall, netTaskError("Gamer:\"%s\" does not have multiplayer privileges", gamerName));
			}

			rlXblSessionName uniqueSessionName;
			rlXblSessionName::CreateUniqueName(uniqueSessionName);
#if RSG_XDK
			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rverify(context, catchall, );

			Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef = ref new Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference(m_ServiceConfigurationId,
				m_SessionTemplateName, ref new String(uniqueSessionName.ToWideString()));

			JsonObject^ sessionCustomConstants = ref new JsonObject();

			MultiplayerSession^ multiplayerSession = ref new MultiplayerSession(context,									// XboxLiveContext
																				sessionRef,									// MultiplayerSessionReference
																				0,											// Max Members in Session
																				false,										// clientMatchmakingCapable 
																				MultiplayerSessionVisibility::Any,			// session visibility
																				nullptr,									// initiator user Ids
																				sessionCustomConstants->Stringify());

			String^ winRtXuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetUserIdString(m_LocalGamerIndex);
			multiplayerSession->AddMemberReservation(winRtXuid, nullptr, true);

			// apply timeouts
			TimeSpan memberReservedTimeout = TimeSpan();	memberReservedTimeout.Duration = s_MemberReservedTimeoutMs * TICKS_PER_MILLISECOND;
			TimeSpan memberInactiveTimeout = TimeSpan();	memberInactiveTimeout.Duration = s_MemberInactiveTimeoutMs * TICKS_PER_MILLISECOND;
			TimeSpan memberReadyTimeout = TimeSpan();		memberReadyTimeout.Duration = s_MemberReadyTimeoutMs * TICKS_PER_MILLISECOND;
			TimeSpan sessionEmptyTimeout = TimeSpan();		sessionEmptyTimeout.Duration = s_SessionEmptyTimeoutMs * TICKS_PER_MILLISECOND;

			// setting the inactive timeout too low will fail session creation.
			if(memberInactiveTimeout.Duration < (1 * TICKS_PER_SECOND))
			{
				// 1 second works
				memberInactiveTimeout.Duration = 1 * TICKS_PER_SECOND;
			}
			multiplayerSession->SetTimeouts(memberReservedTimeout, memberInactiveTimeout, memberReadyTimeout, sessionEmptyTimeout);

			rcheck(m_XblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync",  
				   context->MultiplayerService->WriteSessionAsync(multiplayerSession, MultiplayerSessionWriteMode::CreateNew), s_AsyncSessionTimeoutMs, this, &m_MyStatus), catchall, 
				   netTaskError("Failed to begin WriteSessionAsync"));

			success = true;
#elif RSG_GDK

			m_UniqueSessionName = uniqueSessionName.ToString();

			m_SessionReference = XblMultiplayerSessionReferenceCreate(
				g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId(),
				g_rlTitleId->m_XblTitleId.GetMultiplayerSessionTemplateName(),
				m_UniqueSessionName.c_str());

			//Create the new session
			u64 localPlayerXuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(m_LocalGamerIndex);

			XblMultiplayerSessionInitArgs initArgs;
			initArgs.Visibility = XblMultiplayerSessionVisibility::Any;
			initArgs.CustomJson = nullptr; //optional
			initArgs.InitiatorXuids = nullptr; //optional
			initArgs.InitiatorXuidsCount = 0;
			initArgs.MaxMembersInSession = 0; //ignore

			m_AsyncSessionHandle = XblMultiplayerSessionCreateHandle(
				localPlayerXuid,
				&m_SessionReference,
				&initArgs);

			//Reserve local user
			HRESULT hr = XblMultiplayerSessionAddMemberReservation(
				m_AsyncSessionHandle,
				localPlayerXuid,
				nullptr, //memberCustomConstantsJson
				true); //initializeRequested

			//Configure constants
			hr = XblMultiplayerSessionConstantsSetTimeouts(m_AsyncSessionHandle,
				s_MemberReservedTimeoutMs,
				s_MemberInactiveTimeoutMs,
				s_MemberReadyTimeoutMs,
				s_SessionEmptyTimeoutMs);
			rverify(SUCCEEDED(hr), catchall, netTaskError("Failed to set timeouts on session - aborting create"));

			hr = WriteSessionCreateAsync(m_AsyncSessionHandle);
			rverify(SUCCEEDED(hr), catchall, netTaskError("Failed to start async session write - aborting create"));
			success = SUCCEEDED(hr);
#endif
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool 
CreateSessionTask::AllocateLocalSession()
{
	bool success = false;

	rtry
	{
		// not asynchronous, but like DestroyLocalSession, allow this to be changed easily
		rverify(!m_MyStatus.Pending(), catchall, netTaskError("Status is pending!"));
	
		rverify(m_Session != nullptr, catchall, netTaskError("Invalid MultiplayerSession!"));

		m_XblSession = rlXblInternal::GetInstance()->_GetSessionManager()->AllocateSession();
		rverify(m_XblSession, catchall, netTaskError("Failed to allocate session"));

		rverify(m_XblSession->Init(m_LocalGamerIndex, m_Session), catchall, netTaskError("Failed to initialise session"));
		*m_Handle = m_XblSession->GetXblSessionHandle();

		m_MyStatus.SetPending();
		m_MyStatus.SetSucceeded();

		success = true; 
	}
	rcatchall
	{
		if(!m_MyStatus.Pending())
		{
			m_MyStatus.SetPending();
			m_MyStatus.SetFailed();
		}
	}

	return success;
}

void 
CreateSessionTask::Complete(const netTaskStatus UNUSED_PARAM(status))
{

}

///////////////////////////////////////////////////////////////////////////////
//  ManagePartySessionTask
///////////////////////////////////////////////////////////////////////////////
class ManagePartySessionTask : public netTask
{
public:

	NET_TASK_DECL(ManagePartySessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_JOIN_PARTY,
		STATE_JOINING_PARTY,
		STATE_WAITING_FOR_PARTY_REFRESH,
		STATE_WAITING_TO_REGISTER_SESSION,
		STATE_REGISTER_SESSION_FOR_PARTY,
		STATE_REGISTERING_SESSION_FOR_PARTY
	};

	ManagePartySessionTask();

	bool Configure(const int localGamerIndex, const rlXblSession* pSession);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);

	bool NeedsToJoinParty();
	bool JoinParty();

	bool RegisterSession();
	bool ShouldRegisterSessionForParty();

	const rlXblSession* m_LocalSession;
	int m_State;

	int m_LocalGamerIndex;
	netStatus m_PartyStatus;
};

ManagePartySessionTask::ManagePartySessionTask()
	: m_LocalSession(NULL)
	, m_State(STATE_JOIN_PARTY)
{
}

bool ManagePartySessionTask::Configure(const int localGamerIndex, const rlXblSession* pSession)
{
	bool success = false;

	rtry
	{
		rverify(pSession != NULL, catchall, netTaskError("Invalid Session"));
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, netTaskError("Invalid gamer index of %d", localGamerIndex));

		m_LocalSession = pSession;
		m_LocalGamerIndex = localGamerIndex;

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

void ManagePartySessionTask::OnCancel()
{
	if(m_PartyStatus.Pending() && netTask::HasTask(&m_PartyStatus))
	{
		netTask::Cancel(&m_PartyStatus);
	}
}

netTaskStatus ManagePartySessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{

	case STATE_JOIN_PARTY:
		if (!NeedsToJoinParty())
		{
			// can use this state to catch an in flight refresh happening now
			m_State = STATE_WAITING_FOR_PARTY_REFRESH;
		}
		else if (JoinParty())
		{
			m_State = STATE_JOINING_PARTY;
		}
		else
		{
			netTaskError("Failed to join party of one");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		break;

	case STATE_JOINING_PARTY:
		if (m_PartyStatus.Succeeded())
		{
			netTaskDebug("Joined party");
			m_State = STATE_WAITING_FOR_PARTY_REFRESH;
		}
		else if (!m_PartyStatus.Pending())
		{
			netTaskError("Failed to join party");
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_WAITING_FOR_PARTY_REFRESH:
		if (!rlXblInternal::GetInstance()->_GetPartyManager()->IsRefreshingParty())
		{
			netTaskDebug("Party up to date");
			m_State = STATE_WAITING_TO_REGISTER_SESSION;
		}
		break;

	case STATE_WAITING_TO_REGISTER_SESSION:
		if (!rlXblInternal::GetInstance()->_GetPartyManager()->IsRegisteringGameSession())
		{
			netTaskDebug("Party not currently registering");
			m_State = STATE_REGISTER_SESSION_FOR_PARTY;
		}
		break;

	case STATE_REGISTER_SESSION_FOR_PARTY:
		if (!ShouldRegisterSessionForParty())
		{
			// Don't need to register session for party, complete successfully
			netTaskDebug("Game registration not required");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if (RegisterSession())
		{
			m_State = STATE_REGISTERING_SESSION_FOR_PARTY;
		}
		else
		{
			netTaskError("Failed to register game session for party");
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_REGISTERING_SESSION_FOR_PARTY:

		if (m_PartyStatus.Succeeded())
		{
			netTaskDebug("Registered for party");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if (!m_PartyStatus.Pending())
		{
			netTaskError("Failed to register for party");
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool ManagePartySessionTask::NeedsToJoinParty()
{
	// If we have no party view, we need to join a party of one
	if (rlXblInternal::GetInstance()->_GetPartyManager()->GetPartyView() == nullptr)
	{
		return true;
	}

	return false;
}

bool ManagePartySessionTask::JoinParty()
{
	bool success = false;

	try
	{
		success = rlXblInternal::GetInstance()->_GetPartyManager()->AddLocalUsersToParty(m_LocalGamerIndex, &m_PartyStatus);
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool ManagePartySessionTask::ShouldRegisterSessionForParty()
{
	try
	{

		rtry
		{
			PartyView^ partyView = rlXblInternal::GetInstance()->_GetPartyManager()->GetPartyView();

			// Check for the party view, if we're not in a party just return false
			rcheck(partyView, catchall, );

			// Only register the session for the party if null or changed
			if(partyView->GameSession == nullptr)
			{
				// If we're in a party, return if the session is party enabled
				netTaskDebug("ShouldRegisterSessionForParty :: Yes - No session currently assigned");
				return true;
			}
			else if(rlXbl::IsInviteActionPending())
			{
				// If we're in a party, return if the session is party enabled
				netTaskDebug("ShouldRegisterSessionForParty :: No - Invite Action Pending");
				return true;
			}
			else
			{
				rlXblSessionHandle thisHandle, partyHandle;
				rverify(rlXblCommon::ImportXblHandle(m_LocalSession->GetSessionReference(), &thisHandle), catchall, );
				rverify(rlXblCommon::ImportXblHandle(partyView->GameSession, &partyHandle), catchall, );

				// If the handle is different than the currently registered party handle
				if(thisHandle != partyHandle)
				{
					netTaskDebug("ShouldRegisterSessionForParty :: Yes - Different session currently assigned");
					return true;
				}
			}
		}
		rcatchall
		{

		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}

	return false;
}

bool ManagePartySessionTask::RegisterSession()
{
	bool success = false;

	try
	{
		success = rlXblInternal::GetInstance()->_GetPartyManager()->RegisterGameSession(m_LocalGamerIndex, m_LocalSession->GetSessionReference(), &m_PartyStatus);
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

void ManagePartySessionTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall, );
	}
	rcatchall
	{

	}
}

///////////////////////////////////////////////////////////////////////////////
//  JoinSessionTask
///////////////////////////////////////////////////////////////////////////////
class JoinSessionTask : public netTask
{
public:

	NET_TASK_DECL(JoinSessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,
		STATE_GETTING_SESSION,
		STATE_JOIN_LOCAL,
		STATE_JOINING_LOCAL,
		STATE_JOIN_REMOTE,
		STATE_JOINING_REMOTE,
	};

	JoinSessionTask();

	bool Configure(const rlGamerHandle& localGamer,
				   const int createFlags,
				   const rlXblSessionHandle* hSession,
				   const rlGamerHandle* joiners,
				   const rlSlotType* slotTypes,
				   const unsigned numJoiners,
				   const bool applyCorrelationId);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);

	bool GetSession();
	bool JoinLocalGamers();
	bool JoinRemoteGamers();
	bool ManagePartySession();

	const rlXblSessionHandle* m_hSession;
	int m_LocalGamerIndexes[RL_MAX_LOCAL_GAMERS];
	u64 m_RemoteGamerHandles[RL_MAX_GAMERS_PER_SESSION - 1];
	int m_LocalPrivSlots[RL_MAX_LOCAL_GAMERS];
	int m_RemotePrivSlots[RL_MAX_GAMERS_PER_SESSION - 1];
	unsigned m_NumLocalGamers;
	unsigned m_NumRemoteGamers;

	rlXblSession* m_LocalSession;
	int m_State;
	netStatus m_MyStatus;
	MultiplayerSession^ m_CloudSession;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
	bool m_ApplyCorrelationId;

	// parties
	int m_LocalGamerIndex;
	bool m_bPartySession;
};

JoinSessionTask::JoinSessionTask()
	: m_hSession(NULL)
	, m_NumLocalGamers(0)
	, m_NumRemoteGamers(0)
	, m_State(STATE_GET_SESSION)
	, m_bPartySession(false)
	, m_LocalSession(NULL)
	, m_ApplyCorrelationId(true)
{
}

bool
JoinSessionTask::Configure(const rlGamerHandle& localGamer,
						   const int createFlags,
						   const rlXblSessionHandle* hSession,
						   const rlGamerHandle* gamerHandles,
						   const rlSlotType* slotTypes,
						   const unsigned numGamers,
						   const bool applyCorrelationId)
{
	rlAssert(hSession);

	bool success = false;

	rtry
	{
		m_hSession = hSession;
		m_NumLocalGamers = m_NumRemoteGamers = 0;

		for(int i = 0; i < (int) numGamers; ++i)
		{
			const rlGamerHandle& gamerHandle = gamerHandles[i];

			if(gamerHandle.IsLocal())
			{
				rverify(m_NumLocalGamers < COUNTOF(m_LocalGamerIndexes), catchall,);

				m_LocalGamerIndexes[m_NumLocalGamers] = gamerHandle.GetLocalIndex();
				m_LocalPrivSlots[m_NumLocalGamers] = (RL_SLOT_PRIVATE == slotTypes[i]);
				++m_NumLocalGamers;
			}
			else
			{
				rverify(m_NumRemoteGamers < COUNTOF(m_RemoteGamerHandles), catchall,);

				m_RemoteGamerHandles[m_NumRemoteGamers] = gamerHandle.GetXuid();
				m_RemotePrivSlots[m_NumRemoteGamers] = (RL_SLOT_PRIVATE == slotTypes[i]);
				++m_NumRemoteGamers;
			}
		}

		if (localGamer.IsValid())
		{
			m_LocalGamerIndex = localGamer.GetLocalIndex();
			m_bPartySession = (createFlags & RL_SESSION_CREATE_FLAG_PARTY_SESSION_DISABLED) == 0;
		}
		else
		{
			m_LocalGamerIndex = 0;
			m_bPartySession = false;
		}

		m_ApplyCorrelationId = applyCorrelationId;

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

void 
JoinSessionTask::OnCancel()
{
	m_XblStatus.Cancel();
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
JoinSessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_GET_SESSION:

		m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(m_hSession);
		if(!m_LocalSession)
		{
			netTaskError("Local session does not exist!");
			status = NET_TASKSTATUS_FAILED;
		}
		else if(!this->GetSession())
		{
			netTaskError("GetSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Getting session...");
			m_State = STATE_GETTING_SESSION;
		}
		break;

	case STATE_GETTING_SESSION:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Retrieved session");
			m_CloudSession = m_XblStatus.GetResults<MultiplayerSession^>();

			if(m_CloudSession != nullptr)
			{
				m_LocalSession->ValidateHost(m_CloudSession);
				m_State = STATE_JOIN_LOCAL;
			}
			else
			{
				netTaskError("GetCurrentSessionAsync succeeded but session is invalid");
				status = NET_TASKSTATUS_FAILED;
			}

		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to retrieve session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_JOIN_LOCAL:
		if(m_NumLocalGamers > 0)
		{
			if(JoinLocalGamers())
			{
				netTaskDebug("Joining local gamers...");
				m_State = STATE_JOINING_LOCAL;
			}
			else
			{
				netTaskError("JoinLocalGamers failed");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else
		{
			netTaskDebug("No local gamers");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		break;

	case STATE_JOINING_LOCAL:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Joined local gamers");
			m_LocalSession->Join(m_LocalGamerIndexes[0]);

			if(m_ApplyCorrelationId)
			{
				netTaskDebug("Applying Correlation Id: %ls", m_CloudSession->MultiplayerCorrelationId->Data());
				m_LocalSession->SetCorrelationId(m_CloudSession->MultiplayerCorrelationId);
			}

			OUTPUT_ONLY(rlXblSessions::LogSessionDetail(m_CloudSession, "JoinSessionTask::Local - SUCCEEDED", true);)
			m_State = STATE_JOIN_REMOTE;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to join local gamers with error code 0x%08x", m_MyStatus.GetResultCode());
			OUTPUT_ONLY(rlXblSessions::LogSessionDetail(m_CloudSession, "JoinSessionTask::Local - FAILED", true);)
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_JOIN_REMOTE:
		if(m_NumRemoteGamers > 0)
		{
			if(JoinRemoteGamers())
			{
				netTaskDebug("Joining remote gamers...");
				m_State = STATE_JOINING_REMOTE;
			}
			else
			{
				netTaskError("JoinRemoteGamers failed");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else
		{
			netTaskDebug("No remote gamers");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		break;

	case STATE_JOINING_REMOTE:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Joined remote gamers");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to join remote gamers with error code 0x%08x", m_MyStatus.GetResultCode());
			OUTPUT_ONLY(rlXblSessions::LogSessionDetail(m_CloudSession, "JoinSessionTask::Remote", true);)
				status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

void 
JoinSessionTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall, );
		ManagePartySession();
	}
	rcatchall
	{

	}
}

bool 
JoinSessionTask::GetSession()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalSession->GetLocalGamerIndex());
		if (context == nullptr)
		{
			return false;
		}

		success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
			context->MultiplayerService->GetCurrentSessionAsync(m_LocalSession->GetSessionReference()),
			s_AsyncSessionTimeoutMs,
			this,
			&m_MyStatus);
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool
JoinSessionTask::JoinLocalGamers()
{
	bool success = false;

	rtry
	{
		rverify(m_NumLocalGamers > 0, catchall, );

		// TODO: NS
		rverify(m_NumLocalGamers == 1, catchall, netTaskError("JoinSessionTask only supports joining 1 local gamer for now"));

		netTaskDebug2("Joining %d local gamers...", m_NumLocalGamers);

		for(unsigned int i = 0; i < m_NumLocalGamers; ++i)
		{
			// set current user to be active and joined
			m_CloudSession->Join(nullptr);
			m_CloudSession->SetCurrentUserStatus(MultiplayerSessionMemberStatus::Active);

			netPeerAddress peerAddr;
			rverify(rlPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &peerAddr), catchall, );
			peerAddr.ClearNonAdvertisableData();
			char paBuf[rlPeerAddress::TO_STRING_BUFFER_SIZE] = {0};
			rverify(peerAddr.ToString(paBuf), catchall, );

			wchar_t widePaBuf[sizeof(paBuf)] = {0};
			String^ winRtPeerAddress = rlXblCommon::UTF8ToWinRtString(paBuf, widePaBuf, COUNTOF(widePaBuf));

			JsonObject^ memberCustomProperties = ref new JsonObject();
			memberCustomProperties->Insert("peeraddress", JsonValue::CreateStringValue(winRtPeerAddress));

			m_CloudSession->SetCurrentUserMemberCustomPropertyJson(L"customProperties", memberCustomProperties->Stringify());

			netTaskDebug("JoinLocal :: Properties: %ls", memberCustomProperties->Stringify()->Data());

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndexes[i]);
			rverify(context, catchall, );

			rverify(m_XblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync", 
				context->MultiplayerService->WriteSessionAsync(m_CloudSession,
				MultiplayerSessionWriteMode::UpdateExisting),
				s_AsyncSessionTimeoutMs,
				this,
				&m_MyStatus),
				catchall, );

			success = true;
		}
	}
	rcatchall
	{
	}

	return success;
}

bool
JoinSessionTask::JoinRemoteGamers()
{
	bool success = false;

	rtry
	{
		rcheck(true, catchall, );
		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool JoinSessionTask::ManagePartySession()
{
	if(m_bPartySession)
	{
		// create a fire and forget task to manage party joining / registration
		CREATE_NET_XBLTASK(ManagePartySessionTask, NULL, m_LocalGamerIndex, m_LocalSession);
	}
	return false;
}

///////////////////////////////////////////////////////////////////////////////
//  LeaveSessionTask
///////////////////////////////////////////////////////////////////////////////
class LeaveSessionTask : public netTask
{
public:

	NET_TASK_DECL(LeaveSessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,
		STATE_GETTING_SESSION,
		STATE_LEAVE_SESSION,
		STATE_LEAVING_SESSION,
		STATE_DESTROY_LOCAL_SESSION,
		STATE_DESTROYING_LOCAL_SESSION,
	};

	LeaveSessionTask();

	bool Configure(const int localGamerIndex,
				   const rlXblSessionHandle* hSession,
				   const bool bDestroySession);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);

	bool GetSession();
	bool LeaveSession();
	bool DestroyLocalSession();

	int m_LocalGamerIndex;
	const rlXblSessionHandle* m_hSession;
	bool m_bDestroySession;
	rlXblSession* m_LocalSession;
	MultiplayerSession^ m_CloudSession;
	int m_State;
	netStatus m_MyStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
};

LeaveSessionTask::LeaveSessionTask()
	: m_LocalGamerIndex(-1)
	, m_hSession(NULL)
	, m_bDestroySession(true)
	, m_State(STATE_GET_SESSION)
{
}

bool
LeaveSessionTask::Configure(const int localGamerIndex,
							const rlXblSessionHandle* hSession,
							const bool bDestroySession)
{
	bool success = false;

	rtry
	{
		rverify(localGamerIndex >= 0, catchall, );
		rverify(hSession != NULL, catchall, );

		m_LocalGamerIndex = localGamerIndex;
		m_hSession = hSession;
		m_bDestroySession = bDestroySession;

		m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(m_hSession);
		rverify(m_LocalSession, catchall, netTaskError("Attempting to leave an unknown session"));

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

void 
LeaveSessionTask::OnCancel()
{
	m_XblStatus.Cancel();
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
LeaveSessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	try
	{

		switch(m_State)
		{
		case STATE_GET_SESSION:
			if(!this->GetSession())
			{
				netTaskError("GetSession failed");
				m_State = STATE_DESTROY_LOCAL_SESSION;
			}
			else
			{
				netTaskDebug("Getting session...");
				m_State = STATE_GETTING_SESSION;
			}
			break;

		case STATE_GETTING_SESSION:
			m_XblStatus.Update();

			if(m_MyStatus.Succeeded())
			{
				netTaskDebug("Retrieved session");
				m_CloudSession = m_XblStatus.GetResults<MultiplayerSession^>();

				if(m_CloudSession != nullptr)
				{
					m_State = STATE_LEAVE_SESSION;
				}
				else
				{
					netTaskError("GetCurrentSessionAsync succeeded but session is invalid");
					m_State = STATE_DESTROY_LOCAL_SESSION;
				}
			}
			else if(!m_MyStatus.Pending())
			{
				netTaskError("Failed to leave session with error code 0x%08x", m_MyStatus.GetResultCode());
				m_State = STATE_DESTROY_LOCAL_SESSION;
			}
			break;

		case STATE_LEAVE_SESSION:
			if(LeaveSession())
			{
				netTaskDebug("Leaving session...");
				m_State = STATE_LEAVING_SESSION;
			}
			else
			{
				netTaskError("LeaveSession failed");
				m_State = STATE_DESTROY_LOCAL_SESSION;
			}
			break;

		case STATE_LEAVING_SESSION:
			m_XblStatus.Update();

			if(!m_MyStatus.Pending())
			{
				netTaskDebug("Leave session %s", m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
				m_State = STATE_DESTROY_LOCAL_SESSION;
			}
			break;

		case STATE_DESTROY_LOCAL_SESSION:
			if(!m_bDestroySession)
			{
				netTaskDebug("Flagged not to destroy local session");
				status = NET_TASKSTATUS_SUCCEEDED;
			}
			else if(DestroyLocalSession())
			{
				netTaskDebug("Destroying local session...");
				m_State = STATE_DESTROYING_LOCAL_SESSION;
			}
			else
			{
				netTaskError("DestroyLocalSession failed");
				status = NET_TASKSTATUS_FAILED;
			}
			break;

		case STATE_DESTROYING_LOCAL_SESSION:
			if(m_MyStatus.Succeeded())
			{
				netTaskDebug("Destroyed local session");
				status = NET_TASKSTATUS_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		status = NET_TASKSTATUS_FAILED;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

void 
LeaveSessionTask::Complete(const netTaskStatus status)
{
	if((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled())
	{

	}
}

bool 
LeaveSessionTask::GetSession()
{
	bool success = false;

	try
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));

			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));

			rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex), catchall, netTaskError("Invalid local host index of %d", m_LocalGamerIndex));
			rcheck(rlPresence::IsOnline(m_LocalGamerIndex), catchall, netTaskError("Local index %d not online", m_LocalGamerIndex));

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rverify(context, catchall, netTaskError("Invalid XboxLiveContext"));

			success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
																context->MultiplayerService->GetCurrentSessionAsync(m_LocalSession->GetSessionReference()),
																s_AsyncSessionTimeoutMs,
																this,
																&m_MyStatus);
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool 
LeaveSessionTask::LeaveSession()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		m_CloudSession->Leave();

		success = m_XblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync", 
			context->MultiplayerService->WriteSessionAsync(m_CloudSession,
			MultiplayerSessionWriteMode::UpdateExisting),
			s_AsyncSessionTimeoutMs,
			this,
			&m_MyStatus);
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool 
LeaveSessionTask::DestroyLocalSession()
{
	m_LocalSession->Shutdown(&m_MyStatus);
	return true;
}

///////////////////////////////////////////////////////////////////////////////
//  DestroySessionTask
///////////////////////////////////////////////////////////////////////////////
class DestroySessionTask : public netTask
{
public:

	NET_TASK_DECL(DestroySessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_DESTROY_LOCAL_SESSION,
		STATE_DESTROYING_LOCAL_SESSION,
	};

	DestroySessionTask();

	bool Configure(const int localGamerIndex,
		const rlXblSessionHandle* hSession);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);

	bool DestroyLocalSession();

	int m_LocalGamerIndex;
	const rlXblSessionHandle* m_hSession;
	rlXblSession* m_LocalSession;
	int m_State;
	netStatus m_MyStatus;
};

DestroySessionTask::DestroySessionTask()
	: m_LocalGamerIndex(-1)
	, m_hSession(NULL)
	, m_State(STATE_DESTROY_LOCAL_SESSION)
{
}

bool
DestroySessionTask::Configure(const int localGamerIndex,
							  const rlXblSessionHandle* hSession)
{
	bool success = false;

	rtry
	{
		// we only destroy the local session here. Xbox Live removes the 
		// session from the MPSD when there are no members joined. This task
		// assumes we already left the session.

		rverify(localGamerIndex >= 0, catchall, );
		rverify(hSession != NULL, catchall, );

		m_LocalGamerIndex = localGamerIndex;
		m_hSession = hSession;

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

void 
DestroySessionTask::OnCancel()
{
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
DestroySessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_DESTROY_LOCAL_SESSION:
		if(DestroyLocalSession())
		{
			netTaskDebug("Destroying local session...");
			m_State = STATE_DESTROYING_LOCAL_SESSION;
		}
		else
		{
			netTaskError("DestroyLocalSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_DESTROYING_LOCAL_SESSION:
		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Destroyed local session");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

void 
DestroySessionTask::Complete(const netTaskStatus status)
{
	if((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled())
	{

	}
}

bool 
DestroySessionTask::DestroyLocalSession()
{
	m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(m_hSession);

	// LeaveSession() can also destroy the session so it's possible for m_LocalSession to be NULL here.
	if(m_LocalSession)
	{
		m_LocalSession->Shutdown(&m_MyStatus);
	}
	else
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetSucceeded();
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
//  MigrateHostTask
///////////////////////////////////////////////////////////////////////////////
class MigrateHostTask : public netTask
{
public:

	NET_TASK_DECL(MigrateHostTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,			// new host only
		STATE_GETTING_SESSION,		// new host only
		STATE_LEAVE_SESSION,
		STATE_LEAVING_SESSION,
		STATE_CREATE_SESSION,
		STATE_CREATING_SESSION,
		STATE_MIGRATE_HOST,			// host only
		STATE_MIGRATING_HOST,		// host only
		STATE_JOIN_SESSION,
		STATE_JOINING_SESSION,
	};

	MigrateHostTask();

	bool Configure(rlXblSessionHandle** hSession,
				   const int localGamerIndex,
				   const int localHostIndex,
				   const int createFlags,
				   const rlSessionInfo& sessionInfo,
				   rlSessionInfo* newSessionInfo);
	
	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);
	
	bool GetSession();
	bool LeaveSession();
	bool CreateSession();
	bool GetMigratedSession();
	bool MigrateHost();
	bool JoinSession();

	rlXblSessionHandle** m_Handle;
	int m_LocalHostIndex;
	int m_CreateFlags;
	rlSessionInfo m_SessionInfo;
	rlSessionInfo* m_NewSessionInfo;

	rlXblSession* m_LocalSession;
	MultiplayerSession^ m_CloudSession;
	rlSessionToken m_SessionToken;

	int m_State;
	netStatus m_MyStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
};

MigrateHostTask::MigrateHostTask()
: m_LocalHostIndex(-1)
, m_CreateFlags(0)
, m_State(STATE_LEAVE_SESSION)
{
}

bool
MigrateHostTask::Configure(
	rlXblSessionHandle** hSession,
	const int localGamerIndex,
	const int localHostIndex,
	const int createFlags,
	const rlSessionInfo& sessionInfo,
	rlSessionInfo* newSessionInfo)
{
	bool success = false;

	rtry
	{
		rverify(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS, catchall, netTaskError("Invalid local gamer index"));
		rverify(hSession, catchall, netTaskError("Session handle is NULL"));
		m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(*hSession);
		rverify(m_LocalSession, catchall, netTaskError("No local session matching handle"));
		rverify(newSessionInfo, catchall, netTaskError("New session info is NULL"));

		// just copy, this can be invalid, indicating that we are hosting a new session
		m_CreateFlags = createFlags;
		m_SessionInfo = sessionInfo;

		// local host index is the index of the local gamer that we intend to host the session
		// if invalid, we are migrating to a remote host
        if(RL_IS_VALID_LOCAL_GAMER_INDEX(localHostIndex))
        {
			netTaskDebug("Migrating to local host");
			newSessionInfo->Clear();
            m_LocalHostIndex = localHostIndex;
        }
        else
        {
			netTaskDebug("Migrating to remote host");
			rverify(newSessionInfo->IsValid(), catchall, netTaskError("New session info is not valid"));
			rverify(newSessionInfo->GetHostPeerAddress().IsRemote(), catchall, netTaskError("Host peer address is not valid"));
			m_LocalHostIndex = -1;
        }
		
		if(m_SessionInfo.IsValid())
		{
			netTaskDebug("Migrating from existing session, CorrelationId: %ls", m_LocalSession->GetCorrelationId()->Data());
			m_State = STATE_LEAVE_SESSION;
		}
		else
		{
			netTaskDebug("Hosting new session");
			m_State = STATE_GET_SESSION;
		}

		m_Handle = hSession;
        m_NewSessionInfo = newSessionInfo;

		success = true;
	}
	rcatchall
	{

	}

    return success;
}

void 
MigrateHostTask::OnCancel()
{
	m_XblStatus.Cancel();
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
MigrateHostTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{

	case STATE_GET_SESSION:
		if(!this->GetSession())
		{
			netTaskError("GetSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Getting session...");
			m_State = STATE_GETTING_SESSION;
		}
		break;

	case STATE_GETTING_SESSION:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Retrieved session");
			m_CloudSession = m_XblStatus.GetResults<MultiplayerSession^>();
			if(m_CloudSession != nullptr)
				m_State = STATE_MIGRATE_HOST;
			else
			{
				netTaskError("GetCurrentSessionAsync succeeded but session is invalid");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to retrieve session with error 0x%08x", m_MyStatus.GetResultCode());
			netTaskError("\tServiceConfigurationId: %ls", m_LocalSession->GetSessionReference()->ServiceConfigurationId->Data());
			netTaskError("\tSessionName: %ls", m_LocalSession->GetSessionReference()->SessionName->Data());
			netTaskError("\tSessionTemplateName: %ls", m_LocalSession->GetSessionReference()->SessionTemplateName->Data());

			// instead of failing, try to create a new (split) session
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_LEAVE_SESSION:
		if(!this->LeaveSession())
		{
			netTaskError("LeaveSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Leaving session...");
			m_State = STATE_LEAVING_SESSION;
		}
		break;

	case STATE_LEAVING_SESSION:
		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Left session");
			m_State = STATE_CREATE_SESSION;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to leave session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_CREATE_SESSION:
		if(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalHostIndex))
		{
			if(!this->CreateSession())
			{
				netTaskError("CreateSession failed");
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				netTaskDebug("Creating session...");
				m_State = STATE_CREATING_SESSION;
			}
		}
		else
		{
			if(!this->GetMigratedSession())
			{
				netTaskError("GetMigratedSession failed");
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				netTaskDebug("Getting migrated session...");

				// note: we're sending the GetMigratedSession result to the CREATING_SESSION handler on purpose,
				// so that we end up creating a new local session based on the results of GetMigratedSession
				m_State = STATE_CREATING_SESSION;
			}
		}
		break;

	case STATE_CREATING_SESSION:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Created session");

			rtry
			{
				m_CloudSession = m_XblStatus.GetResults<MultiplayerSession^>();
				rverify(m_CloudSession != nullptr, catchall, netTaskError("Invalid MultiplayerSession!"));

				int localGamerIndex = m_LocalHostIndex;
				if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
				{
					// we are not the host, so use the acting user index to create the local session
					localGamerIndex = rlPresence::GetActingUserIndex();
				}

				rverify(m_LocalSession->Init(localGamerIndex, m_CloudSession), catchall, netTaskError("Failed to initialise session"));
				*m_Handle = m_LocalSession->GetXblSessionHandle();

				// migrate using the new session
				if(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalHostIndex))
				{
					m_State = STATE_MIGRATE_HOST;
				}
				else
				{
					m_State = STATE_JOIN_SESSION;
				}
			}
			rcatchall
			{
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to create session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_MIGRATE_HOST:
		if(!this->MigrateHost())
		{
			netTaskError("MigrateHost failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Migrating host...");
			m_State = STATE_MIGRATING_HOST;
		}
		break;

	case STATE_MIGRATING_HOST:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Migrated host");

			// if this is a new session, we'll need to join it
			if(m_SessionInfo.IsValid())
			{
				m_State = STATE_JOIN_SESSION;
			}
			else
			{
				status = NET_TASKSTATUS_SUCCEEDED;
			}
		}
		else if(!m_MyStatus.Pending())
		{
			OUTPUT_ONLY(rlXblSessions::LogSessionDetail(m_CloudSession, "MigrateHostTask", true);)
			netTaskError("Failed to migrate host with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_JOIN_SESSION:
		if(!this->JoinSession())
		{
			netTaskError("JoinSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Joining session...");
			m_State = STATE_JOINING_SESSION;
		}
		break;

	case STATE_JOINING_SESSION:
		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Joined session");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to join session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool 
MigrateHostTask::GetSession()
{
	bool success = false;

	try
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));

			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));

			rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalHostIndex), catchall, netTaskError("Invalid local host index of %d", m_LocalHostIndex));
			rcheck(rlPresence::IsOnline(m_LocalHostIndex), catchall, netTaskError("Local index %d not online", m_LocalHostIndex));

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalHostIndex);

			rverify(context, catchall, netTaskError("Invalid XboxLiveContext"));
			rverify(m_LocalSession, catchall, netTaskError("Invalid local session"));

			success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
																context->MultiplayerService->GetCurrentSessionAsync(m_LocalSession->GetSessionReference()),
																s_AsyncSessionTimeoutMs,
																this,
																&m_MyStatus);
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool
MigrateHostTask::MigrateHost()
{
    bool success = false;

	try
	{
		rtry
		{
			if(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalHostIndex))
			{
				// set a custom property: host <xuid> so joiners can determine the host
				u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(m_LocalHostIndex);
				String^ winRtXuid = rlXblCommon::XuidToWinRTString(xuid);

				// set a custom property: sessionToken so others can join the session
				// the session token changes when a host migration occurs, so it must be 
				// a property, not a constant.
				rverify(rlCreateUUID(&m_SessionToken.m_Value),
						catchall,
						netTaskError("Error creating session token"));

				char szToken[32] = {0};
				formatf(szToken, "%016" I64FMT "x", m_SessionToken.m_Value);
				wchar_t wszToken[32];
				Utf8ToWide((char16*) wszToken, szToken, COUNTOF(wszToken));
				String^ winRtToken = ref new String(wszToken);
				
				JsonObject^ sessionCustomProperties = ref new JsonObject();
				sessionCustomProperties->Insert("host", JsonValue::CreateStringValue(winRtXuid));
				sessionCustomProperties->Insert("sessionToken", JsonValue::CreateStringValue(winRtToken));
				m_CloudSession->SetSessionCustomPropertyJson(L"customProperties", sessionCustomProperties->Stringify());

				netTaskDebug("Custom session properties: %ls", sessionCustomProperties->Stringify()->Data());

				rlPeerAddress peerAddr;
				rcheck(rlPeerAddress::GetLocalPeerAddress(m_LocalHostIndex, &peerAddr), catchall, );
				peerAddr.ClearNonAdvertisableData();
				m_NewSessionInfo->Init(**m_Handle, m_SessionToken, peerAddr);

				XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalHostIndex);
				rcheck(context, catchall, );

				rverify(m_XblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync", 
											context->MultiplayerService->WriteSessionAsync(m_CloudSession,
																						   MultiplayerSessionWriteMode::SynchronizedUpdate),
											s_AsyncSessionTimeoutMs,
											this,
											&m_MyStatus),
						catchall, 
						netTaskError("Failed to begin WriteSessionAsync"));

				success = true;
			}
			else
			{ 	
				// someone else is becoming the host
				success = true;
			}
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

    return success;
}

bool
MigrateHostTask::CreateSession()
{
	bool success = false;

	try
	{
		rtry
		{
			bool isSignedIn = rlXblInternal::GetInstance()->_GetPresenceManager()->IsSignedIn(m_LocalHostIndex);

			rcheck(isSignedIn, catchall, );
			rcheck(rlGamerInfo::HasMultiplayerPrivileges(m_LocalHostIndex), catchall, );

			// new session
			rlXblSessionName uniqueSessionName;
			rlXblSessionName::CreateUniqueName(uniqueSessionName);

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalHostIndex);
			rverify(context, catchall, );

			Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef = ref new Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference(m_ServiceConfigurationId, m_SessionTemplateName, ref new String(uniqueSessionName.ToWideString()));

			JsonObject^ sessionCustomConstants = ref new JsonObject();

			MultiplayerSession^ multiplayerSession = ref new MultiplayerSession(context,									// XboxLiveContext
																				sessionRef,									// MultiplayerSessionReference
																				0,											// Max Members in Session
																				false,										// clientMatchmakingCapable 
																				MultiplayerSessionVisibility::Any,			// session visibility
																				nullptr,									// initiator user Ids
																				sessionCustomConstants->Stringify());

			String^ winRtXuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetUserIdString(m_LocalHostIndex);
			multiplayerSession->AddMemberReservation(winRtXuid, nullptr, true);

			// apply timeouts
			TimeSpan memberReservedTimeout = TimeSpan();	memberReservedTimeout.Duration = s_MemberReservedTimeoutMs * TICKS_PER_MILLISECOND;
			TimeSpan memberInactiveTimeout = TimeSpan();	memberInactiveTimeout.Duration = s_MemberInactiveTimeoutMs * TICKS_PER_MILLISECOND;
			TimeSpan memberReadyTimeout = TimeSpan();		memberReadyTimeout.Duration = s_MemberReadyTimeoutMs * TICKS_PER_MILLISECOND;
			TimeSpan sessionEmptyTimeout = TimeSpan();		sessionEmptyTimeout.Duration = s_SessionEmptyTimeoutMs * TICKS_PER_MILLISECOND;

			// setting the inactive timeout too low will fail session creation.
			if(memberInactiveTimeout.Duration < (1 * TICKS_PER_SECOND))
			{
				// 1 second works
				memberInactiveTimeout.Duration = 1 * TICKS_PER_SECOND;
			}
			multiplayerSession->SetTimeouts(memberReservedTimeout, memberInactiveTimeout, memberReadyTimeout, sessionEmptyTimeout);

			rcheck(m_XblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync",  
				context->MultiplayerService->WriteSessionAsync(multiplayerSession, MultiplayerSessionWriteMode::CreateNew), s_AsyncSessionTimeoutMs, this, &m_MyStatus), 
				catchall, 
				netTaskError("Failed to begin WriteSessionAsync"));

			success = true;
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool 
MigrateHostTask::GetMigratedSession()
{
	bool success = false;

	try
	{
		rtry
		{
			int localGamerIndex = rlPresence::GetActingUserIndex();
			rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
			rcheck(rlPresence::IsOnline(localGamerIndex), catchall, netTaskError("Local index %d not online", localGamerIndex));

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(localGamerIndex);
			rcheck(context != nullptr, catchall, );

			const rlXblSessionHandle& handle = m_NewSessionInfo->GetSessionHandle();

			Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ ref = ref new Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference(ref new String(handle.m_ServiceConfigurationId),
																																								   ref new String(handle.m_SessionTemplateName),
																																								   ref new String(handle.m_SessionName.ToWideString()));

			success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
																context->MultiplayerService->GetCurrentSessionAsync(ref),
																s_AsyncSessionTimeoutMs,
																this,
																&m_MyStatus);
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool
MigrateHostTask::LeaveSession()
{
	bool success = false;

	LeaveSessionTask* task = NULL;

	if(!netTask::Create(&task, &m_MyStatus)
		|| !task->Configure(rlPresence::GetActingUserIndex(), *m_Handle, false)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

	return success;
}

bool
MigrateHostTask::JoinSession()
{
	bool success = false;
	JoinSessionTask* task = NULL;

	rtry
	{
		rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(rlPresence::GetActingUserIndex()), catchall, );
		
		rlGamerInfo gamerInfo;
		rcheck(rlPresence::GetGamerInfo(rlPresence::GetActingUserIndex(), &gamerInfo), catchall, );

		rcheck(netTask::Create(&task, &m_MyStatus), catchall, );

		rlGamerHandle gamerHandles[1];
		gamerHandles[0] = gamerInfo.GetGamerHandle();

		rlSlotType slotTypes[1];
		slotTypes[0] = RL_SLOT_PUBLIC;
		
		rcheck(task->Configure(gamerInfo.GetGamerHandle(),
							   m_CreateFlags,
							   *m_Handle,
							   gamerHandles,
							   slotTypes,
							   1,
							   false), 
			   catchall, );

		rcheck(netTask::Run(task), catchall, );

		success = true;
	}
	rcatchall
	{
		netTask::Destroy(task);
	}

	return success;
}

void 
MigrateHostTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall, );
		m_LocalSession->MigrateHost(m_LocalHostIndex, m_SessionToken);

		// set our correlation Id
		if(m_CloudSession != nullptr)
		{
			m_LocalSession->SetCorrelationId(m_CloudSession->MultiplayerCorrelationId);
		}
	}
	rcatchall
	{

	}
}

///////////////////////////////////////////////////////////////////////////////
//  ChangeAttributesTask
///////////////////////////////////////////////////////////////////////////////
class ChangeAttributesTask : public netTask
{
public:

	NET_TASK_DECL(ChangeAttributesTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_CHANGE_ATTRIBUTES,
		STATE_CHANGING_ATTRIBUTES,
	};

	ChangeAttributesTask();

	bool Configure(const int localGamerIndex,
				   const rlXblSessionHandle* hSession,
				   const rlNetworkMode netMode,
				   const unsigned newMaxPubSlots,
				   const unsigned newMaxPrivSlots,
				   const rlMatchingAttributes& newAttrs,
				   const unsigned createFlags,
				   const unsigned newPresenceFlags);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);
	
	bool ChangeAttributes();

	int m_LocalGamerIndex;
	const rlXblSessionHandle* m_Handle;
	rlNetworkMode m_NetMode;
	unsigned m_MaxPubSlots;
	unsigned m_MaxPrivSlots;
	rlMatchingAttributes m_Attrs;
	unsigned m_CreateFlags;
	unsigned m_PresenceFlags;

	int m_State;
	rlXblSession* m_LocalSession;

	netStatus m_MyStatus;
};

ChangeAttributesTask::ChangeAttributesTask()
: m_NetMode(RL_NETMODE_INVALID)
, m_Handle(NULL)
, m_MaxPubSlots(0)
, m_MaxPrivSlots(0)
, m_CreateFlags(0)
, m_PresenceFlags(0)
, m_State(STATE_CHANGE_ATTRIBUTES)
{
}

bool
ChangeAttributesTask::Configure(const int localGamerIndex,
								const rlXblSessionHandle* hSession,
								const rlNetworkMode netMode,
								const unsigned newMaxPubSlots,
								const unsigned newMaxPrivSlots,
								const rlMatchingAttributes& newAttrs,
								const unsigned createFlags,
								const unsigned newPresenceFlags)
{
	bool success = false;

	rtry
	{
		rverify(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS, catchall, netTaskError("Invalid local gamer index: %d", localGamerIndex));
		rverify(hSession, catchall, netTaskError("Invalid session handle"));
		rverify(RL_NETMODE_ONLINE == netMode, catchall, netTaskError("Invalid net mode: %d", netMode));
		rverify(newMaxPubSlots <= RL_MAX_GAMERS_PER_SESSION, catchall, netTaskError("Too many public slots: %u", newMaxPubSlots));
		rverify(newMaxPrivSlots <= RL_MAX_GAMERS_PER_SESSION, catchall, netTaskError("Too many private slots: %u", newMaxPrivSlots));
		rverify(newMaxPubSlots + newMaxPrivSlots <= RL_MAX_GAMERS_PER_SESSION, catchall, netTaskError("Too many overall slots - Pub: %u, Priv: %u", newMaxPubSlots, newMaxPrivSlots));
		rverify(newAttrs.IsValid(), catchall, netTaskError("Invalid attributes"));

		m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(hSession);
		rcheck(m_LocalSession, catchall, netTaskError("Attempting to change attributes on an unknown session"));

		m_LocalGamerIndex = localGamerIndex;
		m_Handle = hSession;
		m_NetMode = netMode;
		m_MaxPubSlots = newMaxPubSlots;
		m_MaxPrivSlots = newMaxPrivSlots;
		m_Attrs = newAttrs;
		m_CreateFlags = createFlags;
		m_PresenceFlags = newPresenceFlags;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void 
ChangeAttributesTask::OnCancel()
{
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
ChangeAttributesTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_CHANGE_ATTRIBUTES:
		if(!this->ChangeAttributes())
		{
			netTaskError("ChangeAttributes failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Changing attributes...");
			m_State = STATE_CHANGING_ATTRIBUTES;
		}
		break;

	case STATE_CHANGING_ATTRIBUTES:
		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Changed attributes...");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to change attributes");
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	return status;
}

bool
ChangeAttributesTask::ChangeAttributes()
{
	m_LocalSession->ChangeAttributes(m_Attrs);

	m_MyStatus.SetPending();
	m_MyStatus.SetSucceeded();

	return true;
}

///////////////////////////////////////////////////////////////////////////////
//  HostSessionTask
///////////////////////////////////////////////////////////////////////////////
class HostSessionTask : public netTask
{
public:

	NET_TASK_DECL(HostSessionTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_CREATE_SESSION,
		STATE_CREATING_SESSION,
		STATE_MIGRATE_HOST,
		STATE_MIGRATING_HOST,
		STATE_CHANGE_SESSION_ATTRIBUTES,
		STATE_CHANGING_SESSION_ATTRIBUTES
	};

	HostSessionTask();

	bool Configure(const int localGamerIndex,
				   const rlNetworkMode netMode,
				   const unsigned maxPubSlots,
				   const unsigned maxPrivSlots,
				   const rlMatchingAttributes& attrs,
				   const unsigned createFlags,
				   const unsigned presenceFlags,
				   rlXblSessionHandle** hSession,
				   rlSessionInfo* sessionInfo);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);
	
	bool CreateSession();
	bool MigrateHost();
	bool ChangeSessionAttributes();

	int m_LocalGamerIndex;
	rlNetworkMode m_NetMode;
	unsigned m_MaxPubSlots;
	unsigned m_MaxPrivSlots;
	rlMatchingAttributes m_Attrs;
	unsigned m_CreateFlags;
	unsigned m_PresenceFlags;
	rlSessionInfo* m_SessionInfo;
	int m_State;
	String^ m_GameSessionName;
	MultiplayerSession^ m_Session;
	rlXblSessionHandle** m_Handle;
	rlXblSession* m_XblSession;
	rlSessionToken m_SessionToken;

	netStatus m_MyStatus;	
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
};

HostSessionTask::HostSessionTask()
: m_NetMode(RL_NETMODE_INVALID)
, m_MaxPubSlots(0)
, m_MaxPrivSlots(0)
, m_CreateFlags(0)
, m_PresenceFlags(0)
, m_State(STATE_CREATE_SESSION)
, m_Handle(NULL)
{
}

bool
HostSessionTask::Configure(const int localGamerIndex,
						   const rlNetworkMode netMode,
						   const unsigned maxPubSlots,
						   const unsigned maxPrivSlots,
						   const rlMatchingAttributes& attrs,
						   const unsigned createFlags,
						   const unsigned presenceFlags,
						   rlXblSessionHandle** hSession,
						   rlSessionInfo* sessionInfo)
{
    rlAssert(localGamerIndex >= 0 && localGamerIndex < RL_MAX_LOCAL_GAMERS);
	rlAssert(RL_NETMODE_ONLINE == netMode);
	rlAssert(maxPubSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(maxPubSlots + maxPrivSlots <= RL_MAX_GAMERS_PER_SESSION);
    rlAssert(RL_NETMODE_ONLINE == netMode
            || !(RL_SESSION_PRESENCE_FLAG_INVITABLE & presenceFlags));
	rlAssert(sessionInfo);

    m_LocalGamerIndex = localGamerIndex;
    m_NetMode = netMode;
    m_MaxPubSlots = maxPubSlots;
    m_MaxPrivSlots = maxPrivSlots;
	m_Attrs = attrs;
    m_CreateFlags = createFlags;
    m_PresenceFlags = presenceFlags;
    m_SessionInfo = sessionInfo;
	m_Handle = hSession;

    return true;
}

void 
HostSessionTask::OnCancel()
{
	m_XblStatus.Cancel();
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
HostSessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_CREATE_SESSION:
		if(!this->CreateSession())
		{
			netTaskError("CreateSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Creating session...");
			m_State = STATE_CREATING_SESSION;
		}
		break;

	case STATE_CREATING_SESSION:
		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Created session");
			m_State = STATE_MIGRATE_HOST;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to create session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_MIGRATE_HOST:
		if(!this->MigrateHost())
		{
			netTaskError("MigrateHost failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Migrating host...");
			m_State = STATE_MIGRATING_HOST;
		}
		break;

	case STATE_MIGRATING_HOST:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Migrated host");
			m_State = STATE_CHANGE_SESSION_ATTRIBUTES;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to migrate session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_CHANGE_SESSION_ATTRIBUTES:
		if(!this->ChangeSessionAttributes())
		{
			netTaskError("ChangeSessionAttributes failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Changing attributes...");
			m_State = STATE_CHANGING_SESSION_ATTRIBUTES;
		}
		break;

	case STATE_CHANGING_SESSION_ATTRIBUTES:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Changed attributes");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to change attributes with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool
HostSessionTask::CreateSession()
{
	bool success = false;

	CreateSessionTask* task;

	if(!netTask::Create(&task, &m_MyStatus)
		|| !task->Configure(m_LocalGamerIndex,
							NULL,
							m_NetMode,
							m_CreateFlags,
							m_PresenceFlags,
							m_MaxPubSlots,
							m_MaxPrivSlots,
							m_Handle)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

	return success;
}

bool
HostSessionTask::MigrateHost()
{
	bool success = false;

	MigrateHostTask* task;

	// when hosting, pass the local gamer index for the host index
	if(!netTask::Create(&task, &m_MyStatus)
		|| !task->Configure(m_Handle,
							m_LocalGamerIndex,
							m_LocalGamerIndex,
							m_CreateFlags,
							rlSessionInfo(),
							m_SessionInfo)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

	return success;
}

bool
HostSessionTask::ChangeSessionAttributes()
{
	bool success = false;

	ChangeAttributesTask* task;

	if(!netTask::Create(&task, &m_MyStatus)
		|| !task->Configure(m_LocalGamerIndex,
							*m_Handle,
							m_NetMode,
							m_MaxPubSlots,
							m_MaxPrivSlots,
							m_Attrs,
							m_CreateFlags,
							m_PresenceFlags)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

	return success;
}

void 
HostSessionTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall, );
	}
	rcatchall
	{

	}
}

///////////////////////////////////////////////////////////////////////////////
//  WriteCustomPropertiesTask
///////////////////////////////////////////////////////////////////////////////
class WriteCustomPropertiesTask : public netTask
{
public:

	NET_TASK_DECL(WriteCustomPropertiesTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,
		STATE_GETTING_SESSION,
		STATE_WRITE_PROPERTIES,
		STATE_WRITING_PROPERTIES,
	};

	WriteCustomPropertiesTask();

	bool Configure(const rlXblSessionHandle* hSession, const rlSessionToken& sessionToken);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);

	bool GetSession();
	bool WriteCustomProperties();

	const rlXblSessionHandle* m_Handle;
	rlSessionToken m_SessionToken;
	
	rlXblSession* m_LocalSession;
	MultiplayerSession^ m_CloudSession;

	int m_State;
	netStatus m_MyStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
};

WriteCustomPropertiesTask::WriteCustomPropertiesTask()
	: m_State(STATE_GET_SESSION)
{
}

bool
WriteCustomPropertiesTask::Configure(const rlXblSessionHandle* hSession, const rlSessionToken& sessionToken)
{
	bool success = false;

	rtry
	{
		rverify(hSession, catchall, netTaskError("Session handle is NULL"));
		m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(hSession);
		rverify(m_LocalSession, catchall, netTaskError("No local session matching handle"));
		rverify(m_LocalSession->IsHost(), catchall, netTaskError("Not session host"));
		rverify(sessionToken.IsValid(), catchall, netTaskError("Invalid Session Token"));

		m_Handle = hSession;
		m_SessionToken = sessionToken;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void 
WriteCustomPropertiesTask::OnCancel()
{
	m_XblStatus.Cancel();
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
WriteCustomPropertiesTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_GET_SESSION:
		if(!this->GetSession())
		{
			netTaskError("GetSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Getting session...");
			m_State = STATE_GETTING_SESSION;
		}
		break;

	case STATE_GETTING_SESSION:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Retrieved session");
			
			m_CloudSession = m_XblStatus.GetResults<MultiplayerSession^>();
			if(m_CloudSession != nullptr)
				m_State = STATE_WRITE_PROPERTIES;
			else
			{
				netTaskError("GetCurrentSessionAsync succeeded but session is invalid");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to retrieve session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;

	case STATE_WRITE_PROPERTIES:
		if(!this->WriteCustomProperties())
		{
			netTaskError("WriteCustomProperties failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Writing properties...");
			m_State = STATE_WRITING_PROPERTIES;
		}
		break;

	case STATE_WRITING_PROPERTIES:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Wrote properties");
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to write properties with error 0x%08x", m_MyStatus.GetResultCode());
			OUTPUT_ONLY(rlXblSessions::LogSessionDetail(m_CloudSession, "WriteCustomPropertiesTask", true);)
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool 
WriteCustomPropertiesTask::GetSession()
{
	bool success = false;

	try
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));

			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));

			rverify(m_LocalSession, catchall, netTaskError("Invalid local session"));
			rcheck(rlPresence::IsOnline(m_LocalSession->GetLocalGamerIndex()), catchall, netTaskError("Local index %d not online", m_LocalSession->GetLocalGamerIndex()));

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalSession->GetLocalGamerIndex());
			rverify(context, catchall, netTaskError("Invalid XboxLiveContext"));

			success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
									    context->MultiplayerService->GetCurrentSessionAsync(m_LocalSession->GetSessionReference()),
									    0,
										this,
										&m_MyStatus);
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool
WriteCustomPropertiesTask::WriteCustomProperties()
{
	bool success = false;

	try
	{
		rtry
		{
			int localGamerIndex = rlPresence::GetActingUserIndex();
			rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, netTaskError("Invalid acting user index: %d", localGamerIndex));
			rcheck(rlPresence::IsOnline(localGamerIndex), catchall, netTaskError("Local index %d not online", localGamerIndex));

			// set a custom property: host <xuid> so joiners can determine the host
			u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localGamerIndex);
			String^ winRtXuid = rlXblCommon::XuidToWinRTString(xuid);

			char szToken[32] = {0};
			formatf(szToken, "%016" I64FMT "x", m_SessionToken.m_Value);
			wchar_t wszToken[32];
			Utf8ToWide((char16*) wszToken, szToken, COUNTOF(wszToken));
			String^ winRtToken = ref new String(wszToken);

			JsonObject^ sessionCustomProperties = ref new JsonObject();
			sessionCustomProperties->Insert("host", JsonValue::CreateStringValue(winRtXuid));
			sessionCustomProperties->Insert("sessionToken", JsonValue::CreateStringValue(winRtToken));
			m_CloudSession->SetSessionCustomPropertyJson(L"customProperties", sessionCustomProperties->Stringify());

			netTaskDebug("Custom session properties: %ls", sessionCustomProperties->Stringify()->Data());

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(localGamerIndex);
			rcheck(context, catchall, );

			rverify(m_XblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync", 
					context->MultiplayerService->WriteSessionAsync(m_CloudSession,
																   MultiplayerSessionWriteMode::UpdateExisting),
					s_AsyncSessionTimeoutMs, 
					this,
					&m_MyStatus),
					catchall, 
					netTaskError("Failed to begin WriteSessionAsync"));

			success = true;
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

void 
WriteCustomPropertiesTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall, );
	}
	rcatchall
	{

	}
}

#if !__NO_OUTPUT
///////////////////////////////////////////////////////////////////////////////
//  LogSessionDetailsTask
///////////////////////////////////////////////////////////////////////////////
class LogSessionDetailsTask : public netTask
{
public:

	NET_TASK_DECL(LogSessionDetailsTask);
	NET_TASK_USE_CHANNEL(rline_xbl_session);

	enum State
	{
		STATE_GET_SESSION,
		STATE_GETTING_SESSION,
	};

	LogSessionDetailsTask();

	bool Configure(const rlXblSessionHandle* hSession);

	virtual void OnCancel();
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);
	void Complete(const netTaskStatus status);

	bool GetSession();
	bool WriteCustomProperties();

	const rlXblSessionHandle* m_Handle;

	rlXblSession* m_LocalSession;
	MultiplayerSession^ m_CloudSession;

	int m_State;
	netStatus m_MyStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
};

LogSessionDetailsTask::LogSessionDetailsTask()
	: m_State(STATE_GET_SESSION)
{
}

bool
LogSessionDetailsTask::Configure(const rlXblSessionHandle* hSession)
{
	bool success = false;

	rtry
	{
		rverify(hSession, catchall, netTaskError("Session handle is NULL"));
		m_LocalSession = rlXblInternal::GetInstance()->_GetSessionManager()->GetSessionByHandle(hSession);
		rverify(m_LocalSession, catchall, netTaskError("No local session matching handle"));
		rverify(m_LocalSession->IsHost(), catchall, netTaskError("Not session host"));
		
		m_Handle = hSession;
		
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void 
LogSessionDetailsTask::OnCancel()
{
	m_XblStatus.Cancel();
	if(m_MyStatus.Pending() && netTask::HasTask(&m_MyStatus))
	{
		netTask::Cancel(&m_MyStatus);
	}
}

netTaskStatus 
LogSessionDetailsTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_GET_SESSION:
		if(!this->GetSession())
		{
			netTaskError("GetSession failed");
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Getting session...");
			m_State = STATE_GETTING_SESSION;
		}
		break;

	case STATE_GETTING_SESSION:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			netTaskDebug("Retrieved session");
			
			m_CloudSession = m_XblStatus.GetResults<MultiplayerSession^>();
			if(m_CloudSession != nullptr)
			{
				rlXblSessions::LogSessionDetail(m_CloudSession, "LogSessionDetailsTask", true);
				status = NET_TASKSTATUS_SUCCEEDED;
			}
			else
			{
				netTaskError("GetCurrentSessionAsync succeeded but session is invalid");
				status = NET_TASKSTATUS_FAILED;
			}
		}
		else if(!m_MyStatus.Pending())
		{
			netTaskError("Failed to retrieve session with error 0x%08x", m_MyStatus.GetResultCode());
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}

bool 
LogSessionDetailsTask::GetSession()
{
	bool success = false;

	try
	{
		rtry
		{
			rcheck(rlXblInternal::GetInstance(), catchall, netTaskError("Invalid rlXblInternal instance"));

			rverify(rlXblInternal::GetInstance()->_GetPresenceManager(), catchall, netTaskError("Invalid presence manager"));

			rverify(m_LocalSession, catchall, netTaskError("Invalid local session"));
			rcheck(rlPresence::IsOnline(m_LocalSession->GetLocalGamerIndex()), catchall, netTaskError("Local index %d not online", m_LocalSession->GetLocalGamerIndex()));

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalSession->GetLocalGamerIndex());
			rverify(context, catchall, netTaskError("Invalid XboxLiveContext"));

			success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
				context->MultiplayerService->GetCurrentSessionAsync(m_LocalSession->GetSessionReference()),
				s_AsyncSessionTimeoutMs, 
				this,
				&m_MyStatus);
		}
		rcatchall
		{
		}
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

void 
LogSessionDetailsTask::Complete(const netTaskStatus status)
{
	rtry
	{
		rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall, );
	}
	rcatchall
	{

	}
}
#endif

///////////////////////////////////////////////////////////////////////////////
// rlXblClearUserSessionsTask
///////////////////////////////////////////////////////////////////////////////
rlXblClearUserSessionsTask::rlXblClearUserSessionsTask()
	: m_LocalGamerIndex(-1)
	, m_State(STATE_GET_SESSIONS)
	, m_SessionsResponse(nullptr)
{

}

bool rlXblClearUserSessionsTask::Configure(const int localGamerIndex)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		m_LocalGamerIndex = localGamerIndex;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlXblClearUserSessionsTask::OnCancel()
{
	CANCEL_NET_XBLTASK(m_GetStatus);
}

netTaskStatus rlXblClearUserSessionsTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_GET_SESSIONS:
		if(GetSessions())
		{
			m_State = STATE_GETTING_SESSIONS;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_GETTING_SESSIONS:
		if(m_GetStatus.Succeeded())
		{
			netTaskDebug3("%d sessions to leave.", m_SessionsResponse->Size);
			if (m_SessionsResponse != nullptr &&
				m_SessionsResponse->Size > 0)
			{
				m_State = STATE_LEAVE_SESSIONS;
			}
			else
			{
				status = NET_TASKSTATUS_SUCCEEDED;
			}
		}
		else if(!m_GetStatus.Pending())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_LEAVE_SESSIONS:
		if (LeaveSessions())
		{
			m_State = STATE_LEAVING_SESSIONS;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}

		break;
	case STATE_LEAVING_SESSIONS:
		bool taskPending = false;
		bool taskFailure = false;

		for (int i = 0; i < m_NumSessions; i++)
		{
			if(!m_LeaveStatus[i].Pending() && !m_LeaveStatus[i].Succeeded())
			{
				taskFailure = true;
			}
			else if (m_LeaveStatus[i].Pending())
			{
				taskPending = true;
			}
		}

		// Wait for tasks to finish
		if (!taskPending)
		{
			if (taskFailure)
			{
				status = NET_TASKSTATUS_FAILED;
			}
			else
			{
				status = NET_TASKSTATUS_SUCCEEDED;
			}
		}
		break;
	}

	return status;
}

bool rlXblClearUserSessionsTask::GetSessions()
{
	u64 xuid = g_rlXbl.GetPresenceManager()->GetXboxUserId(m_LocalGamerIndex);
	if (rlVerify(xuid > 0))
	{
		CREATE_NET_XBLTASK(rlXblGetUserSessionTask, &m_GetStatus, m_LocalGamerIndex, xuid, &m_SessionsResponse);
	}

	return false;
}

bool rlXblClearUserSessionsTask::LeaveSessions()
{
	bool bSuccess = true;

	try
	{
		m_NumSessions = m_SessionsResponse->Size;
		for (int i = 0; i < m_NumSessions && i < ISessions::MAX_SESSIONS_BATCH_SIZE; i++)
		{
			MultiplayerSessionStates^ state = m_SessionsResponse->GetAt(i);

			rlXblCleanupSessionTask* task;
			if (!netTask::Create(&task, &m_LeaveStatus[i]) ||
				!task->Configure(m_LocalGamerIndex, state->SessionReference) ||
				!netTask::Run(task))
			{
				netTask::Destroy(task);
				bSuccess = false;
			}
		}

	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
		bSuccess = false;
	}

	return bSuccess;
}

///////////////////////////////////////////////////////////////////////////////
// rlXblCleanupSessionTask
///////////////////////////////////////////////////////////////////////////////
rlXblCleanupSessionTask::rlXblCleanupSessionTask()
	: m_LocalGamerIndex(-1)
	, m_Session(nullptr)
	, m_SessionReference(nullptr)
{
}
bool rlXblCleanupSessionTask::Configure(const int localGamerIndex,  Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionReference)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(sessionReference != nullptr, catchall, );
		m_LocalGamerIndex = localGamerIndex;
		m_SessionReference = sessionReference;
		m_State = STATE_GET_SESSION_DETAILS;
		m_Session = nullptr;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

netTaskStatus rlXblCleanupSessionTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}

	netTaskStatus status = NET_TASKSTATUS_PENDING;

	try
	{
		switch(m_State)
		{
		case STATE_GET_SESSION_DETAILS:
			if(GetSessionDetails())
			{
				m_State = STATE_GETTING_SESSION_DETAILS;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_GETTING_SESSION_DETAILS:
			m_DetailsXblStatus.Update();
			if (m_DetailsStatus.Succeeded())
			{
				m_Session = m_DetailsXblStatus.GetResults<MultiplayerSession^>();
				if (m_Session == nullptr)
				{
					status = NET_TASKSTATUS_FAILED;
				}
				else
				{
					m_State = STATE_LEAVE_SESSION;
				}
			}
			else if (!m_DetailsStatus.Pending())
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_LEAVE_SESSION:
			if (LeaveSession())
			{
				m_State = STATE_LEAVING_SESSION;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_LEAVING_SESSION:
			m_LeaveXblStatus.Update();
			if (m_LeaveStatus.Succeeded())
			{
				status = NET_TASKSTATUS_SUCCEEDED;
			}
			else if (!m_LeaveStatus.Pending())
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION_SILENT(ex);
		status = NET_TASKSTATUS_FAILED;
	}


	return status;
}

void rlXblCleanupSessionTask::OnCancel()
{

}

bool rlXblCleanupSessionTask::GetSessionDetails()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		success = m_DetailsXblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
			context->MultiplayerService->GetCurrentSessionAsync(m_SessionReference),
			s_AsyncSessionTimeoutMs,
			this,
			&m_DetailsStatus);
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

bool rlXblCleanupSessionTask::LeaveSession()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		if (m_Session->Members->Size == 1)
		{
			m_Session->Leave();
		}

		success = m_LeaveXblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync", 
			context->MultiplayerService->WriteSessionAsync(m_Session,
			MultiplayerSessionWriteMode::UpdateExisting),
			s_AsyncSessionTimeoutMs,
			this,
			&m_LeaveStatus);
	}
	catch (Platform::Exception^ e)
	{
		TASK_EXCEPTION(e);
		success = false;
	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlXblGetUserSessionTask
///////////////////////////////////////////////////////////////////////////////
rlXblGetUserSessionTask::rlXblGetUserSessionTask() : netXblTask()
{
	m_SessionsResponsePtr = nullptr;
	m_LocalGamerIndex = 0;
}


bool rlXblGetUserSessionTask::Configure(const int localGamerIndex, u64 xuid, IVectorView<MultiplayerSessionStates^>^* sessionsPtr)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(sessionsPtr != nullptr, catchall, );

		m_LocalGamerIndex = localGamerIndex;
		m_XuidFilter = rlXblCommon::XuidToWinRTString(xuid);
		m_SessionsResponsePtr = sessionsPtr;

		return true;
	}
	rcatchall
	{
		return false;
	}
}


bool rlXblGetUserSessionTask::DoWork()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		String^ emptyFilter = ref new String(L"");
		MultiplayerSessionVisibility visiblity = MultiplayerSessionVisibility::Any;
		u32 contractFilter = 0;

		success = m_XblStatus.BeginOp<IVectorView<MultiplayerSessionStates^>^>("GetSessionsAsync", 
									context->MultiplayerService->GetSessionsAsync(m_ServiceConfigurationId, emptyFilter, m_XuidFilter, emptyFilter, visiblity, contractFilter, true, false, true, ISessions::MAX_SESSIONS_BATCH_SIZE),
									s_AsyncSessionTimeoutMs,
									this,
									&m_MyStatus); 
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}


void rlXblGetUserSessionTask::ProcessSuccess()
{
	*m_SessionsResponsePtr = m_XblStatus.GetResults<IVectorView<MultiplayerSessionStates^>^>();
}

///////////////////////////////////////////////////////////////////////////////
// rlXblGetFriendsSessionsTask
///////////////////////////////////////////////////////////////////////////////
rlXblGetFriendsSessionsTask::rlXblGetFriendsSessionsTask()
	: m_LocalGamerIndex(-1)
	, m_NumXuids(0)
	, m_State(STATE_GET_SESSIONS)
{
	for (int i = 0; i < ISessions::MAX_SESSIONS_BATCH_SIZE; i++)
	{
		m_Xuids[i] = 0;
	}
}

bool rlXblGetFriendsSessionsTask::Configure(const int localGamerIndex, rlFriend* friends, unsigned numFriends)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(friends, catchall, );
		rverify(numFriends > 0, catchall, );
		rverify(numFriends <= rlFriendsPage::MAX_FRIEND_PAGE_SIZE, catchall, );
		m_LocalGamerIndex = localGamerIndex;
		m_Friends = friends;

		for(unsigned i = 0; i < numFriends; i++)
		{
			if (friends[i].IsPendingFriend())
			{
				numFriends--;
			}
			else
			{
				m_Xuids[i] = friends[i].GetXuid();
			}
		}

		m_NumXuids = numFriends;
		m_State = STATE_GET_SESSIONS;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

netTaskStatus rlXblGetFriendsSessionsTask::OnUpdate(int* /*resultCode*/)
{
	// check if task has been cancelled
	if(WasCanceled())
	{
		netTaskDebug("Canceled - bailing...");
		return NET_TASKSTATUS_FAILED;
	}
	
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_GET_SESSIONS:
		if(DoWork())
		{
			m_State = STATE_WAIT;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_WAIT:
		{
			bool taskPending = false;
			bool taskFailure = false;

			for (int i = 0; i < m_NumXuids; i++)
			{
				m_XblStatus[i].Update();

				if(!m_MyStatus[i].Pending() && !m_MyStatus[i].Succeeded())
				{
					taskFailure = true;
				}
				else if (m_MyStatus[i].Pending())
				{
					taskPending = true;
				}
			}

			// Wait for tasks to finish
			if (!taskPending)
			{
				if (taskFailure)
				{
					status = NET_TASKSTATUS_FAILED;
				}
				else
				{
					status = NET_TASKSTATUS_SUCCEEDED;
					ProcessSuccess();
				}
			}
		}
		break;
	}

	return status;
}


void rlXblGetFriendsSessionsTask::ProcessSuccess()
{
	for (int i = 0; i < m_NumXuids; i++)
	{
		if (m_MyStatus[i].Succeeded())
		{
			try
			{
				IVectorView<MultiplayerSessionStates^>^ sessions = m_XblStatus[i].GetResults<IVectorView<MultiplayerSessionStates^>^>();

				bool bWasInSession = m_Friends[i].IsInSession();
				bool bIsInSession = (sessions->Size > 0);

				netTaskDebug3("Processed friends sessions for %s: is in session (%s), was in session (%s)", m_Friends[i].GetName(), bIsInSession ? "TRUE" : "FALSE", bWasInSession ? "TRUE" : "FALSE");

				// check for session changes
				if (bWasInSession != bIsInSession)
				{
					rlXblFriendSessionStatusChangedEvent e(m_LocalGamerIndex, m_Friends[i].GetXuid(), bIsInSession);
					rlXblInternal::GetInstance()->DispatchEvent(&e);
				}
			}
			catch (Platform::Exception^ ex)
			{
				NET_EXCEPTION(ex);
			}
		}
	}
}

bool rlXblGetFriendsSessionsTask::DoWork()
{
	try
	{
		rtry
		{
			unsigned titleId = g_rlTitleId->m_XblTitleId.GetTitleId();
			rverify(titleId > 0, catchall, );

			XboxLiveContext^ xboxLiveContext = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rverify(xboxLiveContext, catchall, );

			rlDebug3("Finding friend sessions for %d friends", m_NumXuids);

			for (int i = 0; i < m_NumXuids; i++)
			{
				String^ userId = rlXblCommon::XuidToWinRTString(m_Xuids[i]);
				String^ emptyFilter = ref new String(L"");
				MultiplayerSessionVisibility visiblity = MultiplayerSessionVisibility::Any;
				u32 contractFilter = 0;
				String^ xuidFilter = rlXblCommon::XuidToWinRTString(m_Xuids[i]);

				m_XblStatus[i].BeginOp<IVectorView<MultiplayerSessionStates^>^>("GetSessionsAsync", 
					xboxLiveContext->MultiplayerService->GetSessionsAsync(m_ServiceConfigurationId, emptyFilter, xuidFilter, emptyFilter, visiblity, contractFilter, true, true, true, MAX_FRIEND_SESSIONS),
					s_AsyncSessionTimeoutMs,
					this,
					&m_MyStatus[i]); 
			}

			return true;
		}
		rcatchall
		{
			return false;
		}	
	}
	catch(Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
// rlXblGetFriendsPresenceSessionsTask
///////////////////////////////////////////////////////////////////////////////
rlXblGetFriendsPresenceSessionsTask::rlXblGetFriendsPresenceSessionsTask()
	: m_Friends(NULL)
	, m_NumFriends(0)
	, m_NumResults(0)
{
	sysMemSet(&m_SessionQuery, 0, sizeof(m_SessionQuery));
}

bool rlXblGetFriendsPresenceSessionsTask::Configure(const int localGamerIndex, rlFriend* friends, unsigned numFriends)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(friends, catchall, );
		rverify(numFriends > 0, catchall, );

		m_Friends = friends;
		m_NumFriends = numFriends;
		m_LocalGamerIndex = localGamerIndex;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXblGetFriendsPresenceSessionsTask::DoWork()
{
	rtry
	{
		rlGamerHandle* gHandles =  (rlGamerHandle*)alloca(sizeof(rlGamerHandle) * m_NumFriends);
		rverify(gHandles, catchall, );

		rlRosCredentials tCredentials = rlRos::GetCredentials(m_LocalGamerIndex);
		rcheck(tCredentials.IsValid(), catchall, );

		unsigned numOnlineFriends = 0;

		// count online friends
		for (unsigned i = 0; i < m_NumFriends; i++)
		{
			// only search presence sessions for online friends
			if (m_Friends[i].IsOnline())
			{
				m_Friends[i].GetGamerHandle(&gHandles[numOnlineFriends]);
				rverify(gHandles[numOnlineFriends].IsValid(), catchall, "Invalid gamer handle on Friend Presence Sessions find");
				numOnlineFriends++;
			}
			else
			{
#if !__NO_OUTPUT
				rlUserIdBuf idBuf;
				netTaskDebug3("Skipping friend %s for presence sessions, user is offline", m_Friends[i].GetUserId(idBuf));
#endif
			}
		}

		netTaskDebug3("Retrieving friends presence sessions for %d of %d friends", numOnlineFriends, m_NumFriends);

		if (numOnlineFriends == 0)
		{
			m_MyStatus.SetPending();
			m_MyStatus.SetSucceeded();
		}
		else
		{
			rverify(rlPresenceQuery::GetSessionByGamerHandle(m_LocalGamerIndex, gHandles, numOnlineFriends, m_SessionQuery, rlPresenceQuery::MAX_SESSIONS_TO_FIND, &m_NumResults, &m_MyStatus), catchall, );
		}
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void rlXblGetFriendsPresenceSessionsTask::ProcessSuccess()
{
	for (unsigned i = 0; i < m_NumFriends; i++)
	{
		if (!m_Friends[i].IsValid())
		{
			continue;
		}

		bool bWasInSession = m_Friends[i].IsInSession();

		rlGamerHandle h;

		// Extract the gamer handle from the friend
		if (!m_Friends[i].GetGamerHandle(&h) || !h.IsValid())
		{
			continue;
		}

		bool bFoundSession = false;
		// Find its handle in the results;
		for (unsigned j = 0; j < m_NumResults; j++)
		{
			if (m_SessionQuery[j].m_GamerHandle == h)
			{
				m_Friends[i].SetSession(m_SessionQuery[j].m_SessionInfo);
				bFoundSession = true;
				break;
			}
		}

		netTaskDebug3("Processed friends presence sessions for %s: is in session (%s), was in session (%s)", m_Friends[i].GetName(), bFoundSession ? "TRUE" : "FALSE", bWasInSession ? "TRUE" : "FALSE");

		// check for session changes
		if (bWasInSession != bFoundSession)
		{
			rlXblFriendSessionStatusChangedEvent e(m_LocalGamerIndex, m_Friends[i].GetXuid(), bFoundSession);
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  Sessions
///////////////////////////////////////////////////////////////////////////////

static const unsigned MAX_ACTIVE_XBL_SESSIONS = 8;
static rlXblSession s_Sessions[MAX_ACTIVE_XBL_SESSIONS];
typedef inlist<rlXblSession, &rlXblSession::m_ListLink> SessionList;
static SessionList s_ActiveSessions;
static SessionList s_FreeSessions;

void 
rlXblSessions::SetTimeoutPolicies(const unsigned asyncSessionTimeoutMs, 
								  const unsigned memberReservedTimeoutMs,
								  const unsigned memberInactiveTimeoutMs,
								  const unsigned memberReadyTimeoutMs,
								  const unsigned sessionEmptyTimeoutMs)
{
	s_AsyncSessionTimeoutMs = asyncSessionTimeoutMs;
	s_MemberReservedTimeoutMs = memberReservedTimeoutMs;
	s_MemberInactiveTimeoutMs = memberInactiveTimeoutMs;
	s_MemberReadyTimeoutMs = memberReadyTimeoutMs;
	s_SessionEmptyTimeoutMs = sessionEmptyTimeoutMs;

	rlDebug1("Updating Timeout Policies:");
	rlDebug1(" Async Session Timeout: %u", s_AsyncSessionTimeoutMs);
	rlDebug1(" Member Reserved Timeout: %u", s_MemberReservedTimeoutMs);
	rlDebug1(" Member Inactive Timeout: %u", s_MemberInactiveTimeoutMs);
	rlDebug1(" Member Ready Timeout: %u", s_MemberReadyTimeoutMs);
	rlDebug1(" Session Empty Timeout: %u", s_SessionEmptyTimeoutMs);
}

void rlXblSessions::SetRunValidateHost(const bool bRunValidateHost)
{
	if(s_bRunValidateHost != bRunValidateHost)
	{
		rlDebug1("SetRunValidateHost :: Setting to %s", bRunValidateHost ? "True" : "False");
		s_bRunValidateHost = bRunValidateHost;
	}
}

void rlXblSessions::SetConsiderInvalidHostAsInvalidSession(const bool bConsiderInvalidHostAsInvalidSession)
{
	if(s_bConsiderInvalidHostAsInvalidSession != bConsiderInvalidHostAsInvalidSession)
	{
		rlDebug1("SetConsiderInvalidHostAsInvalidSession :: Setting to %s", bConsiderInvalidHostAsInvalidSession ? "True" : "False");
		s_bConsiderInvalidHostAsInvalidSession = bConsiderInvalidHostAsInvalidSession;
	}
}

rlXblSessions::rlXblSessions()
{
}

rlXblSessions::~rlXblSessions()
{
    this->Shutdown();
}

bool
rlXblSessions::Init()
{
	try
	{
		m_ServiceConfigurationId = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());
		m_SessionTemplateName = ref new String(g_rlTitleId->m_XblTitleId.GetMultiplayerSessionTemplateName());		
	}
	catch(Platform::Exception^ e)
	{
		RL_EXCEPTION(e);
		return rlVerify(false);
	}

	for(unsigned i = 0; i < MAX_ACTIVE_XBL_SESSIONS; ++i)
	{
		s_Sessions[i].Clear();
		s_FreeSessions.push_back(&s_Sessions[i]);
	}

	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if (PARAM_nosessioncleanup.Get())
		{
			m_bClearedSessions[i] = true;
		}
		else
		{
			m_bClearedSessions[i] = false;
		}
	}

    return true;
}

void
rlXblSessions::Shutdown()
{
	for(unsigned i = 0; i < MAX_ACTIVE_XBL_SESSIONS; ++i)
	{
		s_Sessions[i].Clear();
	}

	s_ActiveSessions.clear();
	s_FreeSessions.clear();
}

void
rlXblSessions::Update()
{
	SessionList::iterator it = s_ActiveSessions.begin();
	SessionList::const_iterator stop = s_ActiveSessions.end();
	for(; stop != it; ++it)
	{
		rlXblSession* session = *it;
	 	session->Update();
	}

	// From MS: Best practice for titles is expect to encounter orphaned sessions during startup and handle the case 
	// appropriately in code, either by removing the user from the session through Leave() or by reusing the session:
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if (!m_bClearedSessions[i] && rlPresence::IsSignedIn(i) && !m_ClearSessionStatus.Pending())
		{
			ClearLocalSessions(i, &m_ClearSessionStatus);
			m_bClearedSessions[i] = true;
			break;
		}
	}
}

rlXblSession* 
rlXblSessions::AllocateSession()
{
	rtry
	{
		rverify(s_FreeSessions.empty() == false, catchall, );

		rlXblSession* session = s_FreeSessions.front();
		rverify(session, catchall, );
		s_FreeSessions.pop_front();
		s_ActiveSessions.push_back(session);

		rlDebug("Allocated XBL session. Num Active Sessions: %" SIZETFMT "u, Num Free Sessions: %" SIZETFMT "u", s_ActiveSessions.size(), s_FreeSessions.size());
		rlAssert((s_ActiveSessions.size() + s_FreeSessions.size()) == MAX_ACTIVE_XBL_SESSIONS);

		return session;
	}
	rcatchall
	{
	}

	return NULL;
}

void  
rlXblSessions::DeleteSession(rlXblSession* session)
{
	s_ActiveSessions.erase(session);
	s_FreeSessions.push_back(session);
	rlAssert((s_ActiveSessions.size() + s_FreeSessions.size()) == MAX_ACTIVE_XBL_SESSIONS);

	rlDebug("Removed XBL session and added to free list. "
			 "Num Active Sessions: %" SIZETFMT "u, Num Free Sessions: %" SIZETFMT "u",
			 s_ActiveSessions.size(), s_FreeSessions.size());
}

rlXblSession*
rlXblSessions::GetSessionByHandle(const rlXblSessionHandle* handle) const
{
	for(SessionList::iterator it = s_ActiveSessions.begin(); it != s_ActiveSessions.end(); ++it)
	{
		rlXblSession* session = (*it);
		if(*session->GetXblSessionHandle() == *handle)
		{
			return session;
		}
	}	

	return NULL;
}

bool rlXblSessions::IsPlayerInSession(Platform::String^ xboxUserId, Microsoft::Xbox::Services::Multiplayer::MultiplayerSession^ session)
{
	if( session == nullptr )
	{
		rlWarning("IsPlayerInSession: invalid session.");
		return false;
	}

	try
	{
		for each (MultiplayerSessionMember^ member in session->Members)
		{
			try
			{
				if( rlXblCommon::IsStringEqualI(xboxUserId, member->XboxUserId ))
				{
					return true;
				}
			}
			catch (Platform::Exception^ ex)
			{
				NET_EXCEPTION(ex);
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}

	return false;
}

bool
rlXblSessions::HostSession(const int localGamerIndex,
						   const rlNetworkMode netMode,
						   const unsigned maxPublicSlots,
						   const unsigned maxPrivateSlots,
						   const rlMatchingAttributes& attrs,
						   const unsigned createFlags,
						   const unsigned presenceFlags,
						   rlXblSessionHandle** hSession,
						   rlSessionInfo* sessionInfo,
						   netStatus* status)
{
	CREATE_NET_XBLTASK(HostSessionTask, 
		status,
		localGamerIndex,
		netMode,
		maxPublicSlots,
		maxPrivateSlots,
		attrs,
		createFlags,
		presenceFlags,
		hSession,
		sessionInfo);
}

bool
rlXblSessions::CreateSession(const int localGamerIndex,
							 const rlSessionInfo& sessionInfo,
							 const rlNetworkMode netMode,
							 const unsigned createFlags,
							 const unsigned presenceFlags,
							 const unsigned maxPublicSlots,
							 const unsigned maxPrivateSlots,
							 rlXblSessionHandle** hSession,
							 netStatus* status)
{
	CREATE_NET_XBLTASK(CreateSessionTask, 
		status,
		localGamerIndex,
		&sessionInfo,
		netMode,
		createFlags,
		presenceFlags,
		maxPublicSlots,
		maxPrivateSlots,
		hSession);
}

bool
rlXblSessions::DestroySession(const int localGamerIndex,
							  const rlXblSessionHandle* hSession,
							  netStatus* status)
{
	CREATE_NET_XBLTASK(DestroySessionTask, 
					   status,
					   localGamerIndex,
					   hSession);
}

bool
rlXblSessions::JoinSession(const rlGamerHandle& localGamer,
						   const int createFlags,
						   const rlXblSessionHandle* hSession,
						   const rlGamerHandle* joiners,
						   const rlSlotType* slotTypes,
						   const unsigned numJoiners,
						   netStatus* status)
{
	CREATE_NET_XBLTASK(JoinSessionTask, 
		status,
		localGamer,
		createFlags,
		hSession,
		joiners,
		slotTypes,
		numJoiners,
		true);
}

bool
rlXblSessions::LeaveSession(const int localGamerIndex,
							const rlXblSessionHandle* hSession,
							netStatus* status)
{
	CREATE_NET_XBLTASK(LeaveSessionTask, 
		status,
		localGamerIndex,
		hSession,
		true);
}

bool
rlXblSessions::ChangeSessionAttributes(const int localGamerIndex,
									   const rlXblSessionHandle* hSession,
									   const rlNetworkMode netMode,
									   const unsigned newMaxPubSlots,
									   const unsigned newMaxPrivSlots,
									   const rlMatchingAttributes& newAttrs,
									   const unsigned createFlags,
									   const unsigned newPresenceFlags,
									   netStatus* status)
{
	CREATE_NET_XBLTASK(ChangeAttributesTask, 
		status,
		localGamerIndex,
		hSession,
		netMode,
		newMaxPubSlots,
		newMaxPrivSlots,
		newAttrs,
		createFlags,
		newPresenceFlags);
}

bool
rlXblSessions::MigrateHost(rlXblSessionHandle** hSession,
	const int localGamerIndex,
	const int localHostIndex,
	const int createFlags,
	const rlSessionInfo& sessionInfo,
	rlSessionInfo* newSessionInfo,
	netStatus* status)
{
	CREATE_NET_XBLTASK(MigrateHostTask,
		status,
		hSession,
		localGamerIndex,
		localHostIndex, 
		createFlags,
		sessionInfo,
		newSessionInfo);
}

bool 
rlXblSessions::WriteCustomProperties(const rlXblSessionHandle* hSession,
									 const rlSessionToken& sessionToken,
									 netStatus* status)
{
	CREATE_NET_XBLTASK(WriteCustomPropertiesTask, 
		status, 
		hSession, 
		sessionToken);
}

#if !__NO_OUTPUT
bool 
rlXblSessions::LogSessionDetails(const rlXblSessionHandle* hSession,
							    netStatus* status)
{
	CREATE_NET_XBLTASK(LogSessionDetailsTask, 
		status, 
		hSession);
}
#endif

String^ rlXblSessions::GetMultiplayerCorrelationId(const rlXblSessionHandle* hSession)
{
	rlXblSession* pSession = GetSessionByHandle(hSession);
	if(pSession)
	{
		return pSession->GetCorrelationId();
	}

	// from the XDK documentation - if a user is not in a session, record 0.
	return L"";
}

bool rlXblSessions::ClearLocalSessions(const int localGamerIndex, netStatus* status)
{
	CREATE_NET_XBLTASK(rlXblClearUserSessionsTask, 
		status, 
		localGamerIndex);
}

bool rlXblSessions::ConvertToSessionInfo(MultiplayerSession^ session, rlSessionInfo* outResults, unsigned* outTotal)
{
	try
	{
		rtry
		{
			rverify(outResults != NULL, catchall, rlError("ConvertToSessionInfo - null result set specified"));

			String^ hostXUID = rlXblSessions::GetHostXuid(session);
			rverify(hostXUID != nullptr, catchall, rlError("ConvertToSessionInfo - invalid host XUID"));

			JsonObject^ jsonSessionCustomProperties;
			bool success = JsonObject::TryParse(session->SessionProperties->SessionCustomPropertiesJson, &jsonSessionCustomProperties);
			
			// session document is in a bad state, invalid session
			String^ sessionToken = nullptr;
			if (success && jsonSessionCustomProperties->HasKey(L"customProperties"))
				sessionToken = jsonSessionCustomProperties->GetNamedObject("customProperties")->GetNamedString(L"sessionToken");

			bool bHostIsInSession = false;

			// now look for that member
			for(MultiplayerSessionMember^ member : session->Members)
			{
				JsonObject^ jsonMemberCustomConstants = JsonObject::Parse(member->MemberCustomConstantsJson);
				JsonObject^ jsonMemberCustomProperties = JsonObject::Parse(member->MemberCustomPropertiesJson);

				rcheck(jsonMemberCustomProperties->HasKey(L"customProperties"), catchall, rlError("ConvertToSessionInfo - No object named \"customProperties\" in MemberCustomPropertiesJson for %ls", member->Gamertag->Data()));

				String^ szXuid = member->XboxUserId;

				if(szXuid == hostXUID)
				{
					bHostIsInSession = true;

					u64 xuid = 0;
					rverify(swscanf_s(szXuid->Data(), L"%" LI64FMT L"u", &xuid) == 1, catchall, );
					
					String^ winRtPeerAddress = jsonMemberCustomProperties->GetNamedObject("customProperties")->GetNamedString("peeraddress");

					char szPeerAddress[rlPeerAddress::TO_STRING_BUFFER_SIZE];
					rverify(rlXblCommon::WinRtStringToUTF8(winRtPeerAddress, szPeerAddress, COUNTOF(szPeerAddress)), catchall, );

					rlPeerAddress peerAddr;
					rverify(peerAddr.FromString(szPeerAddress), catchall, );

					rlSessionInfo* info = &outResults[0];

					rlSessionToken token;

					if(sessionToken == nullptr)
					{
						token.m_Value = peerAddr.GetPeerId().GetU64();
					}
					else
					{
						rverify(swscanf_s(sessionToken->Data(), L"%016" LI64FMT L"x", &token.m_Value) == 1, catchall, rlError("ConvertToSessionInfo - Failed to read session token", sessionToken->Data()));
					}

					rlXblSessionHandle handle(session->SessionReference->ServiceConfigurationId->Data(),
											  session->SessionReference->SessionName->Data(),
											  session->SessionReference->SessionTemplateName->Data());

					info->Init(handle, token, peerAddr);
					*outTotal = 1;
					break;
				}
			}

			// session not valid - host not in member list (session exists as the child of failed host migration).
			if(!bHostIsInSession && s_bConsiderInvalidHostAsInvalidSession)
			{
				rlError("Session was not valid: host xuid %ls is not a session member.", hostXUID->Data());
				outResults[0].Clear();
				return false;
			}

			return true;
		}
		rcatchall
		{
			OUTPUT_ONLY(LogSessionDetail(session, "ConvertToSessionInfo - Error", false);)
			rlAssertf(false, "Failed to convert MultiplayerSession^ to rlSessionInfo");
			return false;
		}
	}
	catch (Platform::Exception^ e)
	{
		RL_EXCEPTION(e);
		return L"";
	}
}

String^ rlXblSessions::GetHostXuid(MultiplayerSession^ session)
{
	try
	{
		rtry
		{
			JsonObject^ jsonSessionCustomProperties;
			bool success = JsonObject::TryParse(session->SessionProperties->SessionCustomPropertiesJson, &jsonSessionCustomProperties);

			rcheck(success, catchall, rlError("Failed to parse SessionCustomPropertiesJson"));
			rverify(jsonSessionCustomProperties->HasKey(L"customProperties"), catchall, rlError("No object named \"customProperties\" in SessionCustomPropertiesJson"));

			// look for the host in the custom session properties
			String^ hostXuid = jsonSessionCustomProperties->GetNamedObject("customProperties")->GetNamedString(L"host");
			rverify(hostXuid != nullptr, catchall, );
		
			return hostXuid;
		}
		rcatchall
		{
			OUTPUT_ONLY(LogSessionDetail(session, "GetHostXUID - Error", false);)
		}

		// not found in member
		return L"";
	}
	catch (Platform::Exception^ e)
	{
		RL_EXCEPTION(e);
		return L"";
	}
}

#if !__NO_OUTPUT
void rlXblSessions::LogSessionDetail(MultiplayerSession^ session, const char* headerName, bool bAllMembers)
{
	rlDebug("%s - Logging session details...", headerName);

	if(session == nullptr)
	{
		rlError("\t\tInvalid session");
		return;
	}

	rlDebug("\t\tHost Device Token: %ls", (session->SessionProperties->HostDeviceToken != nullptr) ? session->SessionProperties->HostDeviceToken->Data() : L"Invalid");
	rlDebug("\t\tCorrelation ID: %ls", session->MultiplayerCorrelationId->Data());
	rlDebug("\t\tServiceConfigurationId: %ls", session->SessionReference->ServiceConfigurationId->Data());
	rlDebug("\t\tSessionName: %ls", session->SessionReference->SessionName->Data());
	rlDebug("\t\tSessionTemplateName: %ls", session->SessionReference->SessionTemplateName->Data());
	rlDebug("\t\tSessionCustomPropertiesJson: %ls", session->SessionProperties->SessionCustomPropertiesJson->Data());

	JsonObject^ jsonSessionCustomProperties;
	bool success = JsonObject::TryParse(session->SessionProperties->SessionCustomPropertiesJson, &jsonSessionCustomProperties);

	if(success)
	{
		if(jsonSessionCustomProperties->HasKey(L"customProperties"))
		{
			String^ hostXuid = jsonSessionCustomProperties->GetNamedObject("customProperties")->GetNamedString(L"host");
			if(hostXuid != nullptr)
				rlDebug("\t\t\tHost XUID: %ls", hostXuid->Data());

			String^ sessionToken = jsonSessionCustomProperties->GetNamedObject("customProperties")->GetNamedString(L"sessionToken");
			if(sessionToken != nullptr)
				rlDebug("\t\t\tSession Token: %ls", sessionToken->Data());
		}
	}

	rlDebug("\t\tNumber of Members: %d", session->Members->Size);
	if(!bAllMembers)
		return;

	int nCount = 0;

	for(MultiplayerSessionMember^ member : session->Members)
	{
		rlDebug("\t\t\tMember %d: %ls", nCount++, member->Gamertag->Data());
		
		rlDebug("\t\t\t\tMemberId: %d", member->MemberId);
		rlDebug("\t\t\t\tXboxUserId: %ls", member->XboxUserId->Data());
		rlDebug("\t\t\t\tState: %d", member->Status);
		rlDebug("\t\t\t\tDeviceToken: %ls", member->DeviceToken->Data());
		rlDebug("\t\t\t\tMemberCustomConstantsJson: %ls", member->MemberCustomConstantsJson->Data());
		rlDebug("\t\t\t\tMemberCustomPropertiesJson: %ls", member->MemberCustomPropertiesJson->Data());

		JsonObject^ jsonCustomMemberProperties;
		success = JsonObject::TryParse(member->MemberCustomPropertiesJson, &jsonCustomMemberProperties);

		if(success)
		{
			if(jsonCustomMemberProperties->HasKey(L"customProperties"))
			{
				String^ winRtPeerAddress = jsonCustomMemberProperties->GetNamedObject("customProperties")->GetNamedString("peeraddress");

				if(winRtPeerAddress != nullptr)
				{
					char szPeerAddress[rlPeerAddress::TO_STRING_BUFFER_SIZE];
					rlXblCommon::WinRtStringToUTF8(winRtPeerAddress, szPeerAddress, COUNTOF(szPeerAddress));

					rlPeerAddress peerAddr;
					peerAddr.FromString(szPeerAddress);

					rlDebug("\t\t\t\t\tPeer Id: %" I64FMT "u", peerAddr.GetPeerId());
				}
			}
		}
	}
}
#endif

} // namespace rage

#endif // RSG_DURANGO
