// 
// rlxblplayers.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PLAYERS_H
#define RLXBL_PLAYERS_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblplayers.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblplayers_interface.h instead"
#endif

#include "../rlxblplayers_interface.h"
#include "diag/channel.h"
#include "net/status.h"
#include "rline/rl.h"
#include "rline/rlfriend.h"

namespace rage
{

	//PURPOSE
//  Interface to Xbox Live social relationships (friends, blocked players, etc.) on XB1.
class rlXblPlayers : public IPlayers
{
public:
	// PURPOSE
	// Reads a list of friends with the given parameters
	bool GetFriends(const int localGamerIndex, const unsigned firstFriendIndex, rlFriend friends[], const unsigned maxFriends,
					unsigned* numFriends, unsigned* totalFriends, int friendFlags, netStatus* status);

	// PURPOSE
	// Updates a list of friend's presence and then queries the session status for all online friends
	bool UpdateFriends(const int localGamerIndex, rlFriendsPage* page, unsigned startIndex, unsigned numFriends, netStatus* status);

	// PURPOSE
	// Updates a list of friends presence without refreshing the entire list
	bool UpdateFriendsPresence(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status);

	//PURPOSE
	//	Retrieves friend info for an array of friends without refreshing the entire list
	bool GetFriendInfo(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status);

	//PURPOSE
	// Retrieves the list of social relationships.
	//	Fills two arrays: one sorted by status, the other sorted by Id.
	bool GetSocialRelationships(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, netStatus* status);

	//PURPOSE
	// Returns the presence of the People moniker
	//	Updates the in arrays: one sorted by status, the other sorted by Id
	bool GetPeoplePresence(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, netStatus* status);

	// PURPOSE
	// Checks the size of the friend's list for changes
	bool CheckForListChanges(const int localGamerIndex, netStatus* status);

	// PURPOSE
	// Gets the session status for a list of friends
	bool GetFriendsSessions(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status);

	// PURPOSE
	// Gets the presence session status for a list of friends
	bool GetFriendsPresenceSessions(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status);

	// PURPOSE
	// Gets the players avoid / block list
	bool GetAvoidList(const int localGamerIndex, PlayerList* playerList, netStatus* status);

	// PURPOSE
	// Gets the players mute list
	bool GetMuteList(const int localGamerIndex, PlayerList* playerList, netStatus* status);
	
private:
	int m_MaxFriends;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PLAYERS_H
