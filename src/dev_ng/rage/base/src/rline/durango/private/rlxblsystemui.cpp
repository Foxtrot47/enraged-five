// 
// rlxblsystemui.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlxblsystemui.h"

#include "diag/seh.h"
#include "input/pad_durango.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "net/task.h"
#include "rline/rldiag.h"
#include "string/unicode.h"
#include "system/nelem.h"

#if RSG_DURANGO
#include "system/userlist_durango.winrt.h"
#endif

#include <xdk.h>

using namespace Platform;
using namespace Windows::System;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Xbox::Input;
using namespace Windows::Xbox::UI;

#define MAX_BROWSER_URL_LENGTH 2000

extern __THREAD int RAGE_LOG_DISABLE;

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline_xbl, systemui)
#undef __rage_channel
#define __rage_channel rline_xbl

bool rlXblSystemUi::sm_HasFocus;
ServiceDelegate rlXblSystemUi::sm_Delegate;
netStatus rlXblSystemUi::sm_SystemUiStatus;

#define CREATE_SYSTEMUI_TASK(T, status, ...)																					\
if (IsUiShowing())																												\
{																																\
	rlAssertf(false, "Attempting to call system UI when another system UI screen is being displayed.");							\
	return false;																												\
}																																\
																																\
T* task;																														\
rtry																															\
{																																\
	rverify(netTask::Create(&task, status), catchall, );																		\
	rverify(task->Configure(__VA_ARGS__), catchall, );																			\
	rverify(netTask::Run(task), catchall, );																					\
	return true;																												\
}																																\
rcatchall																														\
{																																\
	netTask::Destroy(task);																										\
	return false;																												\
}

class SystemUiTask : public netTask
{
public:

	NET_TASK_DECL(SystemUiTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	SystemUiTask();

	bool Configure(const int localGamerIndex);

	virtual void OnCancel();
	virtual void Complete(const netTaskStatus status);
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);

protected:
	virtual bool DisplaySystemUi() = 0;

	enum State
	{
		STATE_SHOW_UI,
		STATE_SHOWING_UI
	};

	int m_LocalGamerIndex;
	State m_State;
	netStatus m_MyStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
};

SystemUiTask::SystemUiTask()
	: m_State(STATE_SHOW_UI)
	, m_LocalGamerIndex(-1)
{
}

bool SystemUiTask::Configure(const int localGamerIndex)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		m_LocalGamerIndex = localGamerIndex;
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void SystemUiTask::OnCancel()
{
	m_XblStatus.Cancel();
}

netTaskStatus SystemUiTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_SHOW_UI:
		if(WasCanceled())
		{
			netTaskDebug("Canceled - bailing...");
			status = NET_TASKSTATUS_FAILED;
		}
		else if(DisplaySystemUi())
		{
			m_State = STATE_SHOWING_UI;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_SHOWING_UI:
		m_XblStatus.Update();

		if(m_MyStatus.Succeeded())
		{
			status = NET_TASKSTATUS_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	}

	if(NET_TASKSTATUS_PENDING != status)
	{
		this->Complete(status);
	}

	return status;
}


void SystemUiTask::Complete(const netTaskStatus /* status */)
{
}

class ShowSigninUiTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowSigninUiTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	bool Configure(const int localGamerIndex);
	virtual void Complete(const netTaskStatus status);

protected:
	bool DisplaySystemUi();
};

bool ShowSigninUiTask::Configure(const int localGamerIndex)
{
	m_LocalGamerIndex = localGamerIndex;
	return true;
}

void ShowSigninUiTask::Complete(const netTaskStatus)
{
	sysUserList::GetInstance().Reset();
}

bool ShowSigninUiTask::DisplaySystemUi()
{
	bool success = false;
	
	try
	{
		IController^ controllerToAssociateWith = nullptr;
		// If a valid controller index is passed in, use it
		if (RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex) && 
			sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(m_LocalGamerIndex))
		{
			controllerToAssociateWith = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(m_LocalGamerIndex)->GetPlatformGamepad();
		}
		else
		{
			// Otherwise, get the controller with the latest input
			IController^ currentController = nullptr;
			IVectorView<IGamepad^>^ gamepads = Gamepad::Gamepads;

			for(u32 i = 0; i < gamepads->Size; ++i)
			{
				currentController = gamepads->GetAt(i);
				if(currentController->Id == durangoPads::GetLastUsedGamePadId())
				{
					controllerToAssociateWith = currentController;
					break;
				}
			}
		}

		success = m_XblStatus.BeginOp<AccountPickerResult^>("ShowAccountPickerAsync",
									SystemUI::ShowAccountPickerAsync(controllerToAssociateWith, AccountPickerOptions::None),
									NO_ASYNC_TIMEOUT,
									this,
									&m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

class ShowGamerProfileUiTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowGamerProfileUiTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	bool Configure(const int localGamerIndex, const rlGamerHandle& handle);
	virtual void Complete(const netTaskStatus status);

protected:
	bool DisplaySystemUi();

private:
	u64 m_GamerXUID;
};

bool ShowGamerProfileUiTask::Configure(const int localGamerIndex, const rlGamerHandle& handle)
{
	SystemUiTask::Configure(localGamerIndex);
	m_GamerXUID = handle.GetXuid();
	return true;
}


void ShowGamerProfileUiTask::Complete(const netTaskStatus status)
{
	if (status == NET_TASKSTATUS_SUCCEEDED)
	{
		rlFriendEventNeedsRecheck e;
		rlFriendsManager::DispatchEvent(&e);
	}
}


bool ShowGamerProfileUiTask::DisplaySystemUi()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context != nullptr)
		{
			String^ winRtXuid = rlXblCommon::XuidToWinRTString(m_GamerXUID);

			success = m_XblStatus.BeginAction("ShowProfileCardAsync", 
										SystemUI::ShowProfileCardAsync(context->User, winRtXuid), 
										NO_ASYNC_TIMEOUT,
										this,
										&m_MyStatus);
		}

	}
	catch (Platform::Exception^ e)
	{
		netTaskError("Exception %d , Msg: %ls",e->HResult,e->Message->Data());
		success = false;
	}

	return success;
}

class ShowSendInvitesUiTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowSendInvitesUiTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

protected:
	bool DisplaySystemUi();
};

bool ShowSendInvitesUiTask::DisplaySystemUi()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		success = m_XblStatus.BeginAction("ShowSendInvitesAsync", SystemUI::ShowSendInvitesAsync(context->User), NO_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

class ShowPartySessionsUiTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowPartySessionsUiTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

protected:
	bool DisplaySystemUi();
};

bool ShowPartySessionsUiTask::DisplaySystemUi()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		success = m_XblStatus.BeginAction("LaunchPartyAsync",
									SystemUI::LaunchPartyAsync(context->User),
									DEFAULT_ASYNC_TIMEOUT,
									this,
									&m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}


class ShowAppHelpUiTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowAppHelpUiTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

protected:
	bool DisplaySystemUi();
};

bool ShowAppHelpUiTask::DisplaySystemUi()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		success = m_XblStatus.BeginAction("Show",
									Windows::Xbox::ApplicationModel::Help::Show(context->User),
									NO_ASYNC_TIMEOUT,
									this,
									&m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

class ShowWebBrowserTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowWebBrowserTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	bool Configure(const int localGamerIndex, const char * url);

protected:
	bool DisplaySystemUi();
	String^ m_Url;
};

bool ShowWebBrowserTask::Configure(const int localGamerIndex, const char * url)
{
	try
	{
		rtry
		{
			rverify(strlen(url) < MAX_BROWSER_URL_LENGTH, catchall, );
			rverify(SystemUiTask::Configure(localGamerIndex), catchall, );

			wchar_t wsUrl[MAX_BROWSER_URL_LENGTH] = {0};
			m_Url = rlXblCommon::UTF8ToWinRtString(url, wsUrl, COUNTOF(wsUrl));

			return true;
		}
		rcatchall
		{
			return false;
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
		return false;
	}
}

bool ShowWebBrowserTask::DisplaySystemUi()
{
	bool success = false;

	try
	{
		rlDebug1("Opening external URL: %ls", m_Url->Data());
		Uri^ uri = ref new Windows::Foundation::Uri(m_Url);
#if _XDK_VER >= 10542
		success = m_XblStatus.BeginOp<bool>("LaunchUriAsync",  Windows::System::Launcher::LaunchUriAsync(uri), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
#else
		success = m_XblStatus.BeginOp<bool>("LaunchUriAsync", Launcher::LaunchUriAsync(uri), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
#endif

	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

class ShowAddRemoveFriendTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowAddRemoveFriendTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	bool Configure(const int localGamerIndex, u64 targetXuid);
	virtual void Complete(const netTaskStatus status);

protected:
	bool DisplaySystemUi();
	u64 m_TargetXuid;
};

bool ShowAddRemoveFriendTask::Configure(const int localGamerIndex, u64 targetXuid)
{
	rtry
	{
		rverify(SystemUiTask::Configure(localGamerIndex), catchall, );
		m_TargetXuid = targetXuid;
		netTaskDebug("Showing Add/Remove Friend UI for %" I64FMT "u", targetXuid);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void ShowAddRemoveFriendTask::Complete(const netTaskStatus status)
{
	if (status == NET_TASKSTATUS_SUCCEEDED)
	{
		rlFriendEventNeedsRecheck e;
		rlFriendsManager::DispatchEvent(&e);
	}
}

bool ShowAddRemoveFriendTask::DisplaySystemUi()
{
	bool success = false;

	try
	{
		User^ user = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
		if (user == nullptr)
		{
			return false;
		}

		String^ targetXuid = rlXblCommon::XuidToWinRTString(m_TargetXuid);
		success = m_XblStatus.BeginOp<AddRemoveFriendResult^>("ShowAddRemoveFriendAsync", SystemUI::ShowAddRemoveFriendAsync(user, targetXuid), NO_ASYNC_TIMEOUT, this, &m_MyStatus);
		rlAssertf(success, "ShowAddRemoveFriendAsync failed for: %ls", targetXuid->Data());
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

class ShowComposeMessageTask : public SystemUiTask
{
public:
	NET_TASK_DECL(ShowComposeMessageTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	bool Configure(const int localGamerIndex, const char * defaultText, const rlGamerHandle* recipients,  const unsigned numRecipients);

protected:
	bool DisplaySystemUi();

	String^ m_DefaultText;
	int m_NumXuids;

	static const int MAX_DEFAULT_MESSAGE_LENGTH = 250;
	static const int MAX_COMPOSE_MESSAGE_XUIDS = 32;
	u64 m_Xuids[MAX_COMPOSE_MESSAGE_XUIDS];
};


bool ShowComposeMessageTask::Configure(const int localGamerIndex, const char * defaultText, const rlGamerHandle* recipients,  const unsigned numRecipients)
{
	try
	{
		rtry
		{
			rverify(defaultText, catchall, );
			rverify(recipients, catchall, );
			rverify(numRecipients > 0 && numRecipients < MAX_COMPOSE_MESSAGE_XUIDS, catchall, );
			rverify(SystemUiTask::Configure(localGamerIndex), catchall, );

			m_NumXuids = 0;
			for (unsigned i = 0; i < numRecipients; i++)
			{
				if(recipients[i].IsValid())
				{
					netTaskDebug("Sending to %" I64FMT "u", recipients[i].GetXuid());			
					m_Xuids[m_NumXuids++] = recipients[i].GetXuid();
				}
				else
					netTaskWarning("Invalid gamer handle provided at index %d", i);			
			}

			wchar_t buf[MAX_DEFAULT_MESSAGE_LENGTH];
			m_DefaultText = rlXblCommon::UTF8ToWinRtString(defaultText, buf, MAX_DEFAULT_MESSAGE_LENGTH);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
		return false;
	}
}

bool ShowComposeMessageTask::DisplaySystemUi()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		++RAGE_LOG_DISABLE;

		IVector<Platform::String^>^ users = ref new Platform::Collections::Vector<Platform::String^>();
		for (int i = 0; i < m_NumXuids; i++)
		{
			String^ xuid = rlXblCommon::XuidToWinRTString(m_Xuids[i]);
			users->Append(xuid);
		}

		--RAGE_LOG_DISABLE;

		success = m_XblStatus.BeginAction("ShowComposeMessageAsync", SystemUI::ShowComposeMessageAsync(context->User, m_DefaultText, users), NO_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

rlXblSystemUi::rlXblSystemUi()
{

}

rlXblSystemUi::~rlXblSystemUi()
{
	this->Shutdown();
}

bool rlXblSystemUi::Init()
{
	sm_HasFocus = true;

	sm_Delegate.Bind(&rlXblSystemUi::OnFocusChanged);
	g_SysService.AddDelegate(&sm_Delegate);

	return true;
}

void rlXblSystemUi::Shutdown()
{
	g_SysService.RemoveDelegate(&sm_Delegate);
	sm_Delegate.Reset();
}

void rlXblSystemUi::Update()
{

}

bool rlXblSystemUi::IsUiShowing()
{
	return sm_SystemUiStatus.Pending() || !sm_HasFocus;
}

void rlXblSystemUi::OnFocusChanged(sysServiceEvent *evt)
{
	if(evt->GetType() == sysServiceEvent::FOCUS_GAINED)
	{
		sm_HasFocus = true;
	}
	else if(evt->GetType() == sysServiceEvent::FOCUS_LOST)
	{
		sm_HasFocus = false;
	}
}

bool rlXblSystemUi::ShowSigninUi(const int localGamerIndex)
{
	CREATE_SYSTEMUI_TASK(ShowSigninUiTask, &sm_SystemUiStatus, localGamerIndex);
}

bool rlXblSystemUi::ShowGamerProfileUi(const int localGamerIndex, const rlGamerHandle& gamerHandle)
{
	CREATE_SYSTEMUI_TASK(ShowGamerProfileUiTask, &sm_SystemUiStatus, localGamerIndex, gamerHandle);
}

bool rlXblSystemUi::ShowSendInvitesUi(const int localGamerIndex)
{
	CREATE_SYSTEMUI_TASK(ShowSendInvitesUiTask, &sm_SystemUiStatus, localGamerIndex);
}

bool rlXblSystemUi::ShowPartySessionsUi(const int localGamerIndex)
{
	CREATE_SYSTEMUI_TASK(ShowPartySessionsUiTask, &sm_SystemUiStatus, localGamerIndex);
}

bool rlXblSystemUi::ShowWebBrowser(const int localGamerIndex, const char * url)
{
	CREATE_SYSTEMUI_TASK(ShowWebBrowserTask, &sm_SystemUiStatus, localGamerIndex, url);
}

bool rlXblSystemUi::ShowAppHelpMenu(const int localGamerIndex)
{
	CREATE_SYSTEMUI_TASK(ShowAppHelpUiTask, &sm_SystemUiStatus, localGamerIndex);
}

bool rlXblSystemUi::ShowAddRemoveFriend(const int localGamerIndex, u64 targetXuid)
{
	CREATE_SYSTEMUI_TASK(ShowAddRemoveFriendTask, &sm_SystemUiStatus, localGamerIndex, targetXuid);
}

bool rlXblSystemUi::ShowComposeMessage(const int localGamerIndex, const char * defaultText, const rlGamerHandle* recipients,  const unsigned numRecipients)
{
	CREATE_SYSTEMUI_TASK(ShowComposeMessageTask, &sm_SystemUiStatus, localGamerIndex, defaultText, recipients, numRecipients);
}

} // namespace rage

#endif // RSG_DURANGO
