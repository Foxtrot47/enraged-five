// 
// rlxblrealtimeactivity.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_TASK_H
#define RLXBL_TASK_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblrealtimeactivity.h is a private header. Do not #include it in non-WinRT classes"
#endif

#include "../rlxblrealtimeactivity_interface.h"
#include "atl/inlist.h"
#include "atl/array.h"
#include "net/time.h"
#include "rline/rlpresence.h"
#include "rline/durango/private/rlxbluserstatistics.h"

using namespace Microsoft::Xbox::Services::RealTimeActivity;

namespace rage
{
	enum RtaConnectionState
	{
		RTA_DISCONNECTED,
		RTA_CONNECTED
	};

	enum SubscriptionType
	{
		SUB_DEVICE,
		SUB_TITLE,
		SUB_STATISTIC
	};

	struct rlXblRtaSubscription
	{
		SubscriptionType m_SubscriptionType;
		u64 m_Xuid;
	};

	struct rlXblRtaDeviceSubscription : rlXblRtaSubscription
	{
		RealTimeActivityDevicePresenceChangeSubscription^ deviceSubscription;
	};

	struct rlXblRtaTitleSubscription : rlXblRtaSubscription
	{
		RealTimeActivityTitlePresenceChangeSubscription^ titleSubscription;
	};

	struct rlXblRtaStatisticSubscription : rlXblRtaSubscription
	{
		RealTimeActivityStatisticChangeSubscription^ statisticSubscription;
	};

	class rlXBlRtaConnection
	{
		friend class rlXblRealTimeActivity;

	public:
		rlXBlRtaConnection();
		~rlXBlRtaConnection();

		static const int RECONNECT_INTERVAL_MS = 30000; // 30s

		bool Init(const int localGamerIndex);
		void Update();
		bool ProcessShutdown();
		void Terminate();
		bool CanDestroy();

		// callbacks
		bool RegisterCallbacks();
		bool DeregisterCallbacks();

		// Connections
		bool Connect();
		bool CanConnect();
		void Disconnect();
		void Shutdown();
		void OnSignOut();
		void OnWebSocketClosed();
		void CancelInFlightTasks();

		// Presence Queue
		bool ProcessPresenceQueue();
		bool IsXuidQueuedForSubscription(u64 xuid);
		bool IsXuidQueuedForRemoval(u64 xuid);
		bool SubscribeToDevicePresence(u64 xuid);
		bool SubscribeToTitlePresence(u64 xuid);
		bool UnsubscribeToDevicePresence(u64 xuid);
		bool UnsubscribeToTitlePresence(u64 xuid);
		void ClearSubscriptionQueue();

		// Subscriptions
		static rlXblRtaSubscription* CreateSubscription(u64 xuid, SubscriptionType subType);
		bool AddSubscription(rlXblRtaSubscription* subscription);
		bool HasSubscription(u64 xuid, SubscriptionType subType);
		void DeleteSubscription(rlXblRtaSubscription* subscription);
		void DeleteAllSubscriptions();
		rlXblRtaSubscription* GetSubscription(u64 xuid, SubscriptionType subType);

		XboxLiveContext^ GetContext() { return m_XboxLiveContext; }
		u64 GetXuid() { return m_Xuid; }

	protected:

		static void OnStatisticChange(RealTimeActivityStatisticChangeEventArgs^ args);
		static void OnDevicePresenceChange(RealTimeActivityDevicePresenceChangeEventArgs^ args);
		static void OnTitlePresenceChange(RealTimeActivityTitlePresenceChangeEventArgs^ args);

		Windows::Foundation::EventRegistrationToken m_OnDisconnectRegistration;
		Windows::Foundation::EventRegistrationToken m_OnDevicePresenceChangeRegistration;
		Windows::Foundation::EventRegistrationToken m_OnTitlePresenceChangeRegistration;

		enum State
		{
			STATE_IDLE,
			STATE_CONNECT,
			STATE_CONNECTING,
			STATE_ACTIVE,
			STATE_SHUTDOWN,
		};

		u64 m_Xuid;
		XboxLiveContext^ m_XboxLiveContext;
		State m_State;

		// Queues to un/subscribe to presence
		atArray<u64> m_PresenceSubscriptionQueue;
		atArray<u64> m_PresenceUnsubscriptionQueue;
		atArray<rlXblRtaSubscription*> m_Subscriptions;

		bool m_CallbacksRegistered;
		bool m_Connected;
		bool m_SignedOut;
		u32 m_uLastConnectTime;

		// Status Objects
		netStatus m_ConnectionStatus;
		netStatus m_TitleStatus;
		netStatus m_DeviceStatus;
	};

	//PURPOSE
	// Interface to the XBox One Real-Time Activity Service
	class rlXblRealTimeActivity : public IRealTimeActivity
	{
		friend class rlXblRtaConnectionTask;
		friend class rlXBlRtaConnection;

	public:

		static const int RECONNECT_INTERVAL = 60; // 60 seconds

		rlXblRealTimeActivity();
		virtual ~rlXblRealTimeActivity();

		//PURPOSE
		//  Initialize the realtime activity service
		virtual bool Init();

		//PURPOSE
		//  Shut down the realtime activity service
		virtual void Shutdown();

		//PURPOSE
		//  Update the the realtime activity service.  Should be called once per frame.
		virtual void Update();

		// PURPOSE
		//  Disconnects the RealTimeActivity service from the local gamer index
		virtual void OnSignOut(const int localGamerIndex);

		//PURPOSE
		// Subscribes to the RTA stream for friends presence
		// Can queue up a large list of friend references
		bool SubscribeToFriendsPresence(int localGamerIndex, const atArray<rlFriendsReference*>* friendReferences);
		bool SubscribeToFriendsPresence(int localGamerIndex, const rlFriendsReference* friendReferences, const int numFriendReferences);

		// Remove subscription to the RTA stream for friends presence
		//	Unsubscribe to a large list of friends references
		bool UnsubscribeToFriendsPresence(int localGamerIndex, const atArray<rlFriendsReference*>* friendReferences);
		bool UnsubscribeToFriendsPresence(int localGamerIndex, const rlFriendsReference* friendReferences, const int numFriendReferences);

		// PURPOSE
		//	Event fired when RTA web socket closes
		void OnWebSocketClosed(u64 xuid);

		// PURPOSE
		//	Finds a RealTimeActivity connection
		rlXBlRtaConnection* FindRtaConnection(u64 xuid);
	private:

		rlXBlRtaConnection* CreateRtaConnection(const int localGamerIndex);
		rlXBlRtaConnection* GetRtaConnection(const int localGamerIndex);
		
		static sysCriticalSectionToken m_QueueCs;

		// Array of rlXblRtaInternal objects
		atArray<rlXBlRtaConnection*> m_RtaConnections;
	};

}

#endif  // RSG_DURANGO

#endif  // RLXBL_TASK_H

