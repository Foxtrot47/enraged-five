// 
// rlXblUserStatistics.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlXblUserStatistics.h"

#include "diag/seh.h"
#include "net/task.h"
#include "net/durango/xblstatus.h"
#include "net/durango/xbltask.h"
#include "rline/rldiag.h"
#include "rline/rltitleid.h"
#include "string/unicode.h"
#include "rline/durango/private/rlxblhttptask.h"
#include "rline/durango/private/rlxblcommon.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

using namespace Microsoft::Xbox::Services::UserStatistics;
using namespace Microsoft::Xbox::Services::Multiplayer;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, user_statistics)
#undef __rage_channel
#define __rage_channel rline_user_statistics

extern const rlTitleId* g_rlTitleId;


///////////////////////////////////////////////////////////////////////////////
//  XblUserStatisticsTask
///////////////////////////////////////////////////////////////////////////////
class XblUserStatisticsTask : public netXblTask 
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(XblUserStatisticsTask);

	XblUserStatisticsTask() : m_LocalGamerIndex(-1), m_Result(0), m_MaxNumResults(0) {;}

	bool Configure(const int localGamerIndex, rlXblUserStatiticsInfo* statNames, const unsigned maxStatNames, eXblServiceConfigId serviceConfigID);

private:
	virtual bool DoWork();
	virtual void ProcessSuccess();
	virtual void ProcessFailure();

	bool SetResult(Statistic^ statistic);

private:
	int                      m_LocalGamerIndex;
	rlXblUserStatiticsInfo*  m_Result;
	u32                      m_MaxNumResults;
	Platform::Collections::Vector<String^>^ m_StatNames;
	eXblServiceConfigId		 m_ServiceConfigId;
};

bool XblUserStatisticsTask::Configure(const int localGamerIndex, rlXblUserStatiticsInfo* statNames, const unsigned maxStatNames, eXblServiceConfigId serviceConfigID)
{
	try
	{
		rtry
		{
			m_LocalGamerIndex = localGamerIndex;
			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(m_LocalGamerIndex), catchall, );

			rverify(statNames, catchall, );

			wchar_t wsStatName[rlXblUserStatiticsInfo::MAX_NAME_LENGTH] = {0};

			m_StatNames = ref new Platform::Collections::Vector<String^>();
			for (u32 i=0; i<maxStatNames; i++)
			{
				wsStatName[0] = '\0';
				m_StatNames->Append( rlXblCommon::UTF8ToWinRtString(statNames[i].m_Name, wsStatName, COUNTOF(wsStatName)) );
			}

			m_Result = statNames;
			m_MaxNumResults = maxStatNames;
			m_ServiceConfigId = serviceConfigID;

			netTaskDebug1("Start retrieve User statistics.");
			return true;
		}
		rcatchall
		{
			netTaskError("Failed Start retrieve User statistics.");
			return false;
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}
}

bool XblUserStatisticsTask::SetResult(Statistic^ statistic)
{
	try
	{

		if (rlVerify(statistic) && rlVerify(m_Result) && rlVerify(m_MaxNumResults > 0))
		{
			wchar_t wsStatName[rlXblUserStatiticsInfo::MAX_NAME_LENGTH] = {0};

			for (u32 i = 0; i < m_MaxNumResults; i++)
			{
				wsStatName[0] = '\0';
				if (rlXblCommon::IsStringEqual(rlXblCommon::UTF8ToWinRtString(m_Result[i].m_Name, wsStatName, COUNTOF(wsStatName)), statistic->StatisticName))
				{
					if (statistic->StatisticType == Windows::Foundation::PropertyType::String)
					{
						m_Result[i].m_Type = rlXblUserStatiticsInfo::TYPE_STRING;
						m_Result[i].m_Value.m_asString[0] = '\0';

						USES_CONVERSION;
						const char* str = WIDE_TO_UTF8((char16*)statistic->Value->Data());

						formatf(m_Result[i].m_Value.m_asString, rlXblUserStatiticsInfo::MAX_VALUE_LENGTH, str);

						return true;
					}
					else if (statistic->StatisticType == Windows::Foundation::PropertyType::Double)
					{
						m_Result[i].m_Type = rlXblUserStatiticsInfo::TYPE_DOUBLE;
						m_Result[i].m_Value.m_asDouble = 0.0f;

						USES_CONVERSION;
						const char* str = WIDE_TO_UTF8((char16*)statistic->Value->Data());

						double val = 0;
						if (str && (1 == sscanf(str, "%lf", &val)))
							m_Result[i].m_Value.m_asDouble = val;

						return true;
					}
					else if (rlVerify(statistic->StatisticType == Windows::Foundation::PropertyType::Int64))
					{
						m_Result[i].m_Type = rlXblUserStatiticsInfo::TYPE_S64;
						m_Result[i].m_Value.m_asS64 = 0;

						USES_CONVERSION;
						const char* str = WIDE_TO_UTF8((char16*)statistic->Value->Data());

						s64 val = 0;
						if (str && (1 == sscanf(str, "%" I64FMT "d", &val)))
							m_Result[i].m_Value.m_asS64 = val;

						return true;
					}

					return false;
				}
			}
		}

		return false;
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
		return false;
	}
}

void XblUserStatisticsTask::ProcessSuccess()
{
	netTaskDebug1("SUCCEEDED retrieve User statistics.");

	try
	{
		UserStatisticsResult^ result = m_XblStatus.GetResults< UserStatisticsResult^ >();
		if (rlVerify(result))
		{
			netTaskDebug1("Succeeded to retrieve User statistics.");
			netTaskDebug1("XboxUserId='%ls'", result->XboxUserId->Data());
			netTaskDebug1("Results='%u'", result->ServiceConfigurationStatistics->Size);

			if (result->ServiceConfigurationStatistics->Size > 0)
			{
				for (u32 i=0; i<result->ServiceConfigurationStatistics->Size; i++)
				{
					ServiceConfigurationStatistic^ serviceConfigStatistic = result->ServiceConfigurationStatistics->GetAt(i);
					netTaskDebug1("ServiceConfigurationId '%ls', Statistics Size='%u'", serviceConfigStatistic->ServiceConfigurationId->Data(), serviceConfigStatistic->Statistics->Size);

					if (serviceConfigStatistic->Statistics->Size > 0)
					{
						for (u32 j=0; j<serviceConfigStatistic->Statistics->Size; j++)
						{
							Statistic^ statistic = serviceConfigStatistic->Statistics->GetAt(j);

							if (SetResult(statistic))
							{
								netTaskDebug1("Statistic parsed: name='%ls', value='%ls', type='%d'", statistic->StatisticName->Data(), statistic->Value->Data(), statistic->StatisticType);
							}
							else
							{
								netTaskError("Failed to parse Statistic: name='%ls', value='%ls', type='%d'", statistic->StatisticName->Data(), statistic->Value->Data(), statistic->StatisticType);
							}
						}
					}
				}
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}

}

void XblUserStatisticsTask::ProcessFailure()
{
	netTaskError("FAILED retrieve User statistics.");
}

bool XblUserStatisticsTask::DoWork()
{
	bool success = true;

	rtry
	{
		try
		{
			XboxLiveContext^ xboxLiveContext = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rverify(xboxLiveContext, catchall, );

			String^ serviceConfigurationId;
			if(m_ServiceConfigId == SERVICE_ID_GLOBAL_REPUTATION)
				serviceConfigurationId = rlXblInternal::GetGlobalReputationServiceConfigId();
			else
				serviceConfigurationId = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());

			// A list of stats that are defined by the developer on the Xbox Development Portal.
			rverify(m_StatNames, catchall, );

			success = m_XblStatus.BeginOp<UserStatisticsResult^>("GetSingleUserStatisticsAsync"
										,xboxLiveContext->UserStatisticsService->GetSingleUserStatisticsAsync(xboxLiveContext->User->XboxUserId, serviceConfigurationId, m_StatNames->GetView())
										,DEFAULT_ASYNC_TIMEOUT
										,this
										,&m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}
	}
	rcatchall
	{
	}

	return success;
}



///////////////////////////////////////////////////////////////////////////////
//  XblMultipleUserStatisticsTask
///////////////////////////////////////////////////////////////////////////////
class XblMultipleUserStatisticsTask : public netXblTask 
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(XblMultipleUserStatisticsTask);

	XblMultipleUserStatisticsTask() : m_LocalGamerIndex(-1), m_Result(0), m_MaxNumResults(0) {;}

	bool Configure(const int localGamerIndex, rlXblMultiUserStatiticsInfo* players, const unsigned maxPlayers, eXblServiceConfigId serviceConfigId);

private:
	virtual bool DoWork();
	virtual void ProcessSuccess();
	virtual void ProcessFailure();

	bool SetResult(const u64 xuid, Statistic^ statistic);

private:
	int                           m_LocalGamerIndex;
	rlXblMultiUserStatiticsInfo*  m_Result;
	u32                           m_MaxNumResults;
	Platform::Collections::Vector<String^>^ m_StatNames;
	Platform::Collections::Vector<String^>^ m_PlayerNames;
	eXblServiceConfigId			  m_ServiceConfigId;
};

bool XblMultipleUserStatisticsTask::Configure(const int localGamerIndex, rlXblMultiUserStatiticsInfo* players, const unsigned maxPlayers, eXblServiceConfigId serviceConfigId)
{
	try
	{

		rtry
		{
			m_LocalGamerIndex = localGamerIndex;
			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(m_LocalGamerIndex), catchall, );

			rverify(players, catchall, );

			m_PlayerNames = ref new Platform::Collections::Vector<String^>();
			for (u32 i=0; i<maxPlayers; i++)
			{
				m_PlayerNames->Append(  rlXblCommon::XuidToWinRTString(players[i].m_gamer.GetXuid()) );
			}

			wchar_t wsUrl[rlXblUserStatiticsInfo::MAX_NAME_LENGTH] = {0};
			m_StatNames = ref new Platform::Collections::Vector<String^>();
			for (int i=0; i<players[0].m_stats.GetCount(); i++)
			{
				wsUrl[0] = '\0';
				m_StatNames->Append( rlXblCommon::UTF8ToWinRtString(players[0].m_stats[i].m_Name, wsUrl, COUNTOF(wsUrl)) );
			}

			m_Result = players;
			m_MaxNumResults = maxPlayers;
			m_ServiceConfigId = serviceConfigId;

			netTaskDebug1("Start retrieve User statistics.");
			return true;
		}
		rcatchall
		{
			netTaskError("Failed Start retrieve User statistics.");
			return false;
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}
}

bool XblMultipleUserStatisticsTask::SetResult(const u64 xuid, Statistic^ statistic)
{
	try
	{
		if (rlVerify(statistic) && rlVerify(m_Result) && rlVerify(m_MaxNumResults > 0))
		{
			wchar_t wsUrl[rlXblUserStatiticsInfo::MAX_NAME_LENGTH] = {0};

			for (u32 i = 0; i < m_MaxNumResults; i++)
			{
				if (xuid == m_Result[i].m_gamer.GetXuid())
				{
					rlXblMultiUserStatiticsInfo& info = m_Result[i];

					for (int j=0; j<info.m_stats.GetCount(); j++)
					{
						wsUrl[0] = '\0';

						if (rlXblCommon::IsStringEqual(rlXblCommon::UTF8ToWinRtString(info.m_stats[i].m_Name, wsUrl, COUNTOF(wsUrl)), statistic->StatisticName))
						{
							if (statistic->StatisticType == Windows::Foundation::PropertyType::String)
							{
								info.m_stats[i].m_Type = rlXblUserStatiticsInfo::TYPE_STRING;
								info.m_stats[i].m_Value.m_asString[0] = '\0';

								USES_CONVERSION;
								const char* str = WIDE_TO_UTF8((char16*)statistic->Value->Data());

								formatf(info.m_stats[i].m_Value.m_asString, rlXblUserStatiticsInfo::MAX_VALUE_LENGTH, str);

								return true;
							}
							else if (statistic->StatisticType == Windows::Foundation::PropertyType::Double)
							{
								info.m_stats[i].m_Type = rlXblUserStatiticsInfo::TYPE_DOUBLE;
								info.m_stats[i].m_Value.m_asDouble = 0.0f;

								USES_CONVERSION;
								const char* str = WIDE_TO_UTF8((char16*)statistic->Value->Data());

								double val = 0;
								if (str && (1 == sscanf(str, "%lf", &val)))
									info.m_stats[i].m_Value.m_asDouble = val;

								return true;
							}
							else if (rlVerify(statistic->StatisticType == Windows::Foundation::PropertyType::Int64))
							{
								info.m_stats[i].m_Type = rlXblUserStatiticsInfo::TYPE_S64;
								info.m_stats[i].m_Value.m_asS64 = 0;

								USES_CONVERSION;
								const char* str = WIDE_TO_UTF8((char16*)statistic->Value->Data());

								s64 val = 0;
								if (str && (1 == sscanf(str, "%" I64FMT "d", &val)))
									info.m_stats[i].m_Value.m_asS64 = val;

								return true;
							}

							break;
						}
					}

					break;
				}
			}
		}

		return false;
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}
}

void XblMultipleUserStatisticsTask::ProcessSuccess()
{
	netTaskDebug1("SUCCEEDED retrieve User statistics.");

	try
	{
		IVectorView< UserStatisticsResult^ >^ resultvector = m_XblStatus.GetResults< IVectorView< UserStatisticsResult^ >^ >();
		if (rlVerify(resultvector))
		{
			netTaskDebug1("Succeeded to retrieve User statistics.");

			for (u32 i=0; i<resultvector->Size; i++)
			{
				UserStatisticsResult^ result = resultvector->GetAt(i);
				if (rlVerify(result))
				{
					netTaskDebug1("XboxUserId='%ls'", result->XboxUserId->Data());
					netTaskDebug1("Results='%u'", result->ServiceConfigurationStatistics->Size);

					for (u32 j=0; j<result->ServiceConfigurationStatistics->Size; j++)
					{
						ServiceConfigurationStatistic^ serviceConfigStatistic = result->ServiceConfigurationStatistics->GetAt(j);
						netTaskDebug1("ServiceConfigurationId '%ls', Statistics Size='%u'", serviceConfigStatistic->ServiceConfigurationId->Data(), serviceConfigStatistic->Statistics->Size);

						for (u32 l=0; l<serviceConfigStatistic->Statistics->Size; l++)
						{
							Statistic^ statistic = serviceConfigStatistic->Statistics->GetAt(l);

							if (SetResult( rlXblCommon::WinRtStringToXuid(result->XboxUserId), statistic ))
							{
								netTaskDebug1("Statistic parsed: name='%ls', value='%ls', type='%d'", statistic->StatisticName->Data(), statistic->Value->Data(), statistic->StatisticType);
							}
							else
							{
								netTaskError("Failed to parse Statistic: name='%ls', value='%ls', type='%d'", statistic->StatisticName->Data(), statistic->Value->Data(), statistic->StatisticType);
							}
						}
					}
				}
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}
}

void XblMultipleUserStatisticsTask::ProcessFailure()
{
	netTaskError("FAILED retrieve User statistics.");
}

bool XblMultipleUserStatisticsTask::DoWork( )
{
	bool success = true;


	try
	{
		rtry
		{
			XboxLiveContext^ xboxLiveContext = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);

			String^ serviceConfigurationId;
			if(m_ServiceConfigId == SERVICE_ID_GLOBAL_REPUTATION)
				serviceConfigurationId = rlXblInternal::GetGlobalReputationServiceConfigId();
			else
				serviceConfigurationId = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());

			// A list of stats that are defined by the developer on the Xbox Development Portal.
			rverify(m_StatNames, catchall, );

			success = m_XblStatus.BeginOp<IVectorView<UserStatisticsResult^>^>("GetMultipleUserStatisticsAsync"
				,xboxLiveContext->UserStatisticsService->GetMultipleUserStatisticsAsync(m_PlayerNames->GetView(), serviceConfigurationId, m_StatNames->GetView())
				,DEFAULT_ASYNC_TIMEOUT
				,this
				,&m_MyStatus);
		}
		rcatchall
		{

		}

	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  IUserStatistics
///////////////////////////////////////////////////////////////////////////////

bool IUserStatistics::GetUserStatistics(const int localGamerIndex, rlXblUserStatiticsInfo* stats, const unsigned maxStats, eXblServiceConfigId serviceConfigId, netStatus* status)
{
	CREATE_NET_XBLTASK(XblUserStatisticsTask, status, localGamerIndex, stats, maxStats, serviceConfigId);
}

bool IUserStatistics::GetMultipleUserStatistics(const int localGamerIndex, rlXblMultiUserStatiticsInfo* players, const unsigned maxPlayers, eXblServiceConfigId serviceConfigId, netStatus* status)
{
	CREATE_NET_XBLTASK(XblMultipleUserStatisticsTask, status, localGamerIndex, players, maxPlayers, serviceConfigId);
}

} // namespace rage

#endif //RSG_DURANGO

