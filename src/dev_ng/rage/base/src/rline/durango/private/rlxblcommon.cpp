// 
// rlxblcommon.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "rline/durango/rlxbl_interface.h"
#include "rline/durango/rlxblparty_interface.h"
#include "rline/rldiag.h"
#include "rlxblcommon.h"
#include "string/string.h"
#include "string/unicode.h"
#include "system/param.h"
#include "system/xtl.h"

namespace rage
{

bool rlXblCommon::XuidToWidestring(u64 xuid, wchar_t* outBuf, int outBufLen)
{
	if (outBufLen > XBL_XUID_BUFFER_SIZE)
		return false;

	char szXuid[XBL_XUID_BUFFER_SIZE] = {0};
	formatf(szXuid, "%" I64FMT "u", xuid);

	Utf8ToWide((char16*) outBuf, szXuid, outBufLen);

	return true;
}

String^ rlXblCommon::XuidToWinRTString(u64 xuid)
{
	wchar_t wszId[XBL_XUID_BUFFER_SIZE];
	XuidToWidestring(xuid, wszId, XBL_XUID_BUFFER_SIZE);
	return ref new String(wszId);
}

bool rlXblCommon::WinRtStringToUTF8(String^ winRtString, char* outBuf, int outBufLen)
{
	const wchar_t *wideStr = winRtString->Data();
	int wideStrLen = wcslen( wideStr );

	int numConverted = 0;
	WideToUtf8(outBuf,(char16*)wideStr, wideStrLen, outBufLen, &numConverted);
	rlAssertf(numConverted == wideStrLen, "Failed to convert WinRtStringToUTF8 : could only convert %d of %d characters. Increase output buffer!", numConverted, wideStrLen);

	return numConverted == wideStrLen;
}

u64 rlXblCommon::WinRtStringToXuid(String^ xuidStr)
{
	u64 xuid = 0;
	if(swscanf_s(xuidStr->Data(), L"%" LI64FMT L"u", &xuid) == 1)
	{
		return xuid;
	}

	return 0;
}

String^ rlXblCommon::UTF8ToWinRtString(const char * inStr, const wchar_t * outBuf, const int outBufLen)
{
	Utf8ToWide((char16*)outBuf, inStr, outBufLen);
	return ref new String(outBuf);
}

bool rlXblCommon::IsStringEqual( Platform::String^ val1, Platform::String^ val2 )
{
	return ( wcscmp (val1->Data(), val2->Data()) == 0 );
}

bool rlXblCommon::IsStringEqualI( Platform::String^ val1, Platform::String^ val2 )
{
	return ( _wcsicmp(val1->Data(), val2->Data()) == 0 );
}

bool rlXblCommon::IsStringNEqual( Platform::String^ val1, Platform::String^ val2, const u32 num )
{
	return ( wcsncmp(val1->Data(), val2->Data(), num) == 0 );
}

bool rlXblCommon::IsStringNEqualI( Platform::String^ val1, Platform::String^ val2, const u32 num )
{
	return ( _wcsnicmp(val1->Data(), val2->Data(), num) == 0 );
}

char* rlXblCommon::DisplayNameToUtf8(char (&out)[RL_MAX_DISPLAY_NAME_BUF_SIZE], const wchar_t *in)
{
	wchar_t name[RL_MAX_DISPLAY_NAME_LENGTH + 1] = {0}; // + 1 for null terminator

	safecpy(name, in, RL_MAX_DISPLAY_NAME_LENGTH);

	size_t len = wcslen(in);
	if(len >= RL_MAX_DISPLAY_NAME_LENGTH)
	{
		len = RL_MAX_DISPLAY_NAME_LENGTH;
		safecat(name, L"\u2026"); // append an ellipsis
	}

	return WideToUtf8(out, (char16*)name, len);
}

Windows::Foundation::DateTime rlXblCommon::GetCurrentDateTime() 
{
	ULARGE_INTEGER uInt;
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);
	uInt.LowPart = ft.dwLowDateTime;
	uInt.HighPart = ft.dwHighDateTime;

	Windows::Foundation::DateTime time;
	time.UniversalTime = uInt.QuadPart;
	return time;
}

double rlXblCommon::GetDateTimeDiffInSeconds(Windows::Foundation::DateTime dt1, Windows::Foundation::DateTime dt2)
{
	const uint64 tickPerSecond = 10000000i64;
	uint64 deltaTime = dt2.UniversalTime - dt1.UniversalTime;
	return (double)deltaTime / (double)tickPerSecond;
}

double rlXblCommon::GetDateTimeDiffInSeconds(u64 t1, Windows::Foundation::DateTime dt2)
{
	const uint64 tickPerSecond = 10000000i64;
	uint64 deltaTime = dt2.UniversalTime - (t1 * tickPerSecond);
	return (double)deltaTime / (double)tickPerSecond;
}

bool rlXblCommon::ImportXblHandle(Windows::Xbox::Multiplayer::MultiplayerSessionReference^ sessionRef, rlXblSessionHandle* handle)
{
	rtry
	{
		rverify(handle, catchall, );
		rverify(sessionRef != nullptr, catchall, );

		handle->Init(sessionRef->ServiceConfigurationId->Data(),sessionRef->SessionName->Data(),sessionRef->SessionTemplateName->Data());

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlXblCommon::ImportXblHandle(Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef,  rlXblSessionHandle* handle)
{
	rtry
	{
		rverify(handle, catchall, );
		rverify(sessionRef != nullptr, catchall, );

		handle->Init(sessionRef->ServiceConfigurationId->Data(),sessionRef->SessionName->Data(),sessionRef->SessionTemplateName->Data());

		return true;
	}
	rcatchall
	{
		return false;
	}
}

} // namespace rage

#endif  // RSG_DURANGO
