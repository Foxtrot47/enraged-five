#if RSG_DURANGO

#include "rlxblcommerce.h"


#include "rlxblinternal.h"

#include "atl/functor.h"
#include "diag/seh.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "net/durango/xbltask.h"
#include "rline/rldiag.h"
#include "rline/rltitleid.h"
#include "rline/rltask.h"
#include "rlxblcommon.h"
#include "rlxblhttptask.h"
#include "system/timer.h"

using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Microsoft::Xbox::Services::Marketplace;

const int ITEM_STRING_BUFFER_SIZE = 256;
const int SIGNED_OFFER_BUFFER_SIZE = 8192;
const int ROOT_OFFER_ID_LENGTH = 128;

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rline_xbl, commerce)
	#undef __rage_channel
	#define __rage_channel rline_xbl_commerce

	class GetCatalogTask : public netXblTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl_commerce);
		NET_TASK_DECL(GetCatalogTask);

		GetCatalogTask();
		bool Configure(const int localGamerIndex, const char* rootProductId, cCommercePlatformDetails* details, eCommercePlatformItemType itemType );

		enum 
		{
			MAX_ITEMS_TO_FETCH_FROM_CATALOG_RESULT = 32
		};

	private:

		void ClearTask()
		{
			m_LocalGamerIndex  = -1;
			m_Details = 0;
			m_ItemType = COMMERCE_INVENTORY_ITEM_TYPE_INVALID;
		}

	protected:
		virtual bool DoWork();
		virtual void ProcessSuccess();
		virtual void ProcessFailure();
		int m_LocalGamerIndex;

		eCommercePlatformItemType m_ItemType;

		cCommercePlatformDetails* m_Details;

		atString m_RootProductId;
	};

	GetCatalogTask::GetCatalogTask()
		: m_LocalGamerIndex(-1)
		, m_Details(0)
		, m_ItemType(COMMERCE_INVENTORY_ITEM_TYPE_INVALID)
	{
	}

	bool GetCatalogTask::Configure( const int localGamerIndex,  const char* rootProductId, cCommercePlatformDetails* details,  eCommercePlatformItemType itemType )
	{
		m_LocalGamerIndex = localGamerIndex;
		m_Details = details;
		m_ItemType = itemType;
		m_RootProductId = rootProductId;

		return true;
	}

	//This is only here until we get a method to fetch the products own ID.
#define TITLE_PARENT_ID L"4f0a3089-ba2c-4f3d-9e38-102a41cbd885" // Parent Title's ProductID

	bool GetCatalogTask::DoWork()
	{
		bool success = false;

		try
		{
			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			if (context == nullptr)
			{
				return false;
			}

			String^ scid = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());

			MediaItemType typeToEnumerate;

			switch(m_ItemType)
			{
			case(COMMERCE_INVENTORY_ITEM_TYPE_CONSUMABLE):
				typeToEnumerate = MediaItemType::GameConsumable;
				break;
			case(COMMERCE_INVENTORY_ITEM_TYPE_DURABLE):
				typeToEnumerate = MediaItemType::GameContent;
				break;
			default:
				netTaskError("Invalid mediatype for Xbox enumeration. Defaulting to durable.");
				typeToEnumerate = MediaItemType::GameContent;
			}

			wchar_t wszRootProductId[ROOT_OFFER_ID_LENGTH];
			String^ rootProductId = rlXblCommon::UTF8ToWinRtString( m_RootProductId, wszRootProductId, COUNTOF(wszRootProductId));

			success = m_XblStatus.BeginOp<BrowseCatalogResult^>("BrowseCatalogAsync",
				context->CatalogService->BrowseCatalogAsync(rootProductId, 
				MediaItemType::Game,
				typeToEnumerate,
				CatalogSortOrder::DigitalReleaseDate,
				0,
				0),
				DEFAULT_ASYNC_TIMEOUT,
				this,
				&m_MyStatus);

			netTaskError("BrowseCatalogAsync finished with a value of %d",success);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	void GetCatalogTask::ProcessSuccess()
	{
		if (m_Details == NULL )
		{
			return;
		}

		try
		{
			BrowseCatalogResult^ catalogResults = m_XblStatus.GetResults<BrowseCatalogResult^>();

			// this can happen if we sign out or lose connection (cable pull) while task is in flight
			if(catalogResults == nullptr)
			{
				return;
			}

			for( unsigned int itemsFound = 0; itemsFound < catalogResults->Items->Size; ++itemsFound )
			{
				CatalogItem^ item = catalogResults->Items->GetAt( itemsFound );
				netTaskDebug("Item found name: [%s]",item->Name);
				char stringBuffer[ITEM_STRING_BUFFER_SIZE];
				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				cCommercePlatformItem platformItem;

				rlXblCommon::WinRtStringToUTF8(item->Name, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				platformItem.m_Name = stringBuffer;

				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				rlXblCommon::WinRtStringToUTF8(item->Id, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				platformItem.m_Id = stringBuffer;

				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				rlXblCommon::WinRtStringToUTF8(item->SandboxId, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				platformItem.m_SandboxId = stringBuffer;

				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				rlXblCommon::WinRtStringToUTF8(item->ProductId, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				platformItem.m_ProductId = stringBuffer;

				platformItem.m_IsConsumable = (item->MediaItemType  == MediaItemType::GameConsumable);

				platformItem.m_AvailablilityId.Reset();
				if ( item->AvailabilityContentIds->Size > 0 )
				{
					//We have an availablility, grab the first.
					ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
					rlXblCommon::WinRtStringToUTF8(item->AvailabilityContentIds->GetAt(0), stringBuffer, ITEM_STRING_BUFFER_SIZE);
					platformItem.m_AvailablilityId = stringBuffer;
				}

				platformItem.m_IsOwned = false;

				m_Details->GetItemArray().PushAndGrow(platformItem);
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
		}

		ClearTask();
	}

	void GetCatalogTask::ProcessFailure()
	{
		netTaskError("BrowseCatalogAsync failed.");
		netXblTask::ProcessFailure();
		ClearTask();
	}

	//
	// GetCatalogDetailsTask
	//

	class GetCatalogDetailsTask : public netXblTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl);
		NET_TASK_DECL(GetCatalogDetailsTask);

		GetCatalogDetailsTask();
		bool Configure(const int localGamerIndex, int startIndex, int numToFetch, cCommercePlatformDetails* details );

		enum 
		{
			MAX_ITEMS_TO_FETCH_FROM_CATALOG_RESULT = 32
		};

	private:

		void ClearTask()
		{
			m_StartIndex  = 0;
			m_NumToFetch = 0;
			m_Details = 0;
			m_LocalGamerIndex = -1;
		}

	protected:
		virtual bool DoWork();
		virtual void ProcessSuccess();
		virtual void ProcessFailure();
		int m_LocalGamerIndex;
		int m_StartIndex;
		int m_NumToFetch;

		cCommercePlatformDetails* m_Details;
	};

	GetCatalogDetailsTask::GetCatalogDetailsTask() 
		: m_StartIndex(0)
		, m_NumToFetch(0)
		, m_Details(0)
		, m_LocalGamerIndex(-1)
	{
	}

	bool GetCatalogDetailsTask::Configure( const int localGamerIndex, int startIndex, int numToFetch, cCommercePlatformDetails* details )
	{
		rlAssert(details);

		if (details == 0)
		{
			return false;
		}

		rlAssertf(numToFetch <= MAX_PRODUCT_DETAILS_TO_FETCH, "Maximum number of details per request supported by MS is %d", MAX_PRODUCT_DETAILS_TO_FETCH);
		if ( numToFetch > MAX_PRODUCT_DETAILS_TO_FETCH )
		{
			m_NumToFetch = MAX_PRODUCT_DETAILS_TO_FETCH;
			return false;
		}

		if ( numToFetch == 0 )
		{
			return false;
		}

		rlAssertf(startIndex < details->GetItemArray().GetCount(), "Requested details on a product we do not know about. Start index too high");
		if ( startIndex >= details->GetItemArray().GetCount() )
		{
			m_StartIndex = 0;
			return false;
		}

		rlAssertf(startIndex + numToFetch <= details->GetItemArray().GetCount(), "Requested details on a product we do not know about. Start index plus numToFetch too high");
		if ( startIndex + numToFetch >  details->GetItemArray().GetCount() )
		{
			m_StartIndex = 0;
			m_NumToFetch = 0;
			return false;
		}

		m_LocalGamerIndex = localGamerIndex;
		m_Details = details;
		m_StartIndex = startIndex;
		m_NumToFetch = numToFetch;

		return true;
	}

	bool GetCatalogDetailsTask::DoWork()
	{
		bool success = false;

		try
		{
			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			if (context == nullptr)
			{
				return false;
			}

			//Build a vector of product IDs to get details on.
			IVector< Platform::String^ >^ vProductIds = ref new Platform::Collections::Vector< Platform::String^ >();

			wchar_t wszStatName[ITEM_STRING_BUFFER_SIZE];
			for ( int i = m_StartIndex; i < m_StartIndex + m_NumToFetch; i++ )
			{
				String^ productId = rlXblCommon::UTF8ToWinRtString( m_Details->GetItemArray()[i].m_ProductId, wszStatName, COUNTOF(wszStatName));
				vProductIds->Append( productId );
			}

			success = m_XblStatus.BeginOp<IVectorView<CatalogItemDetails^>^>("GetCatalogItemDetailsAsync",
				context->CatalogService->GetCatalogItemDetailsAsync(vProductIds->GetView()),
				DEFAULT_ASYNC_TIMEOUT,
				this,
				&m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	void GetCatalogDetailsTask::ProcessSuccess()
	{
		if (m_Details == NULL )
		{
			return;
		}

		try
		{
			IVectorView<CatalogItemDetails^ >^ catalogDetailResults = m_XblStatus.GetResults<IVectorView<CatalogItemDetails^ >^ >();

			// this can happen if we sign out or lose connection (cable pull) while task is in flight
			if(catalogDetailResults == nullptr)
			{
				return;
			}

			for( unsigned int iItemsFound = 0; iItemsFound < catalogDetailResults->Size; ++iItemsFound )
			{
				//First of all, see if we have an existing record for this product.
				int foundIndex = -1;
				for ( int iRecords = 0; iRecords < m_Details->GetItemArray().GetCount(); iRecords++ )
				{
					wchar_t wszStatName[ITEM_STRING_BUFFER_SIZE];
					String^ recordId = rlXblCommon::UTF8ToWinRtString( m_Details->GetItemArray()[iRecords].m_Id.c_str(), wszStatName, COUNTOF(wszStatName));

					if ( catalogDetailResults->GetAt(iItemsFound)->Id == recordId )
					{
						foundIndex = iRecords;
						break;
					}
				}

				cCommercePlatformItem* pFoundRecord = NULL;
				if ( foundIndex != -1 )
				{
					pFoundRecord = &(m_Details->GetItemArray()[foundIndex]);
				}
				else
				{
					cCommercePlatformItem newRecord;
					m_Details->GetItemArray().PushAndGrow(newRecord);
					pFoundRecord = &(m_Details->GetItemArray()[m_Details->GetItemArray().GetCount() - 1]);
				}

				//Now we have a record to access, dump the fetched data into it.
				CatalogItemDetails^ item = catalogDetailResults->GetAt(iItemsFound);
				char stringBuffer[ITEM_STRING_BUFFER_SIZE];
				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);

				rlXblCommon::WinRtStringToUTF8(item->Name, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				pFoundRecord->m_Name = stringBuffer;

				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				rlXblCommon::WinRtStringToUTF8(item->Id, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				pFoundRecord->m_Id = stringBuffer;

				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				rlXblCommon::WinRtStringToUTF8(item->SandboxId, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				pFoundRecord->m_SandboxId = stringBuffer;

				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				rlXblCommon::WinRtStringToUTF8(item->ProductId, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				pFoundRecord->m_ProductId = stringBuffer;

				pFoundRecord->m_IsConsumable = (item->MediaItemType  == MediaItemType::GameConsumable);

				//This is where we differ from the non detailed version, in the collection of availabilities.

				if ( item->Availabilities->Size == 0 )
				{
					pFoundRecord->m_DisplayPrice.Clear();
					pFoundRecord->m_Price = 0;
					pFoundRecord->m_AvailablilityId.Clear();
				}
				else
				{
					//For now we support one availability per product.
					//More than one could be used for consumables (same id, differing amounts)
					//However past experience tells us that this could be problematic from an ops standpoint.
					//K.I.S.S principle applies.
					CatalogItemAvailability^ availability = item->Availabilities->GetAt(0);

					char signedOfferBuffer[SIGNED_OFFER_BUFFER_SIZE];
					ZeroMemory(signedOfferBuffer, SIGNED_OFFER_BUFFER_SIZE);
					rlXblCommon::WinRtStringToUTF8(item->Availabilities->GetAt(0)->SignedOffer, signedOfferBuffer, SIGNED_OFFER_BUFFER_SIZE);
					pFoundRecord->m_SignedOfferId = signedOfferBuffer;

					ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
					rlXblCommon::WinRtStringToUTF8(availability->DisplayPrice, stringBuffer, ITEM_STRING_BUFFER_SIZE);
					pFoundRecord->m_DisplayPrice = stringBuffer;

					pFoundRecord->m_Price = static_cast<float>(availability->Price);
				}
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
		}

		ClearTask();
	}

	void GetCatalogDetailsTask::ProcessFailure()
	{
		netTaskError("GetCatalogDetailsTask failed.");
		netXblTask::ProcessFailure();
		ClearTask();
	}

	struct ConsumptionTaskDetails
	{
		InventoryItem ^m_ItemToConsume; 
		bool wasItemFound;
	};

	class GetXblInventoryItemTask : public netXblTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl);
		NET_TASK_DECL(GetXblInventoryItemTask);

		GetXblInventoryItemTask();
		bool Configure(const int localGamerIndex, const char* itemIdentifier, ConsumptionTaskDetails *consumptionDetails);

	protected:
		virtual bool DoWork();
		virtual void ProcessSuccess();

		int m_LocalGamerIndex;
		atString m_ItemIdToFetch;

		ConsumptionTaskDetails *m_ConsumptionDetails;
	};

	GetXblInventoryItemTask::GetXblInventoryItemTask()
		: m_ConsumptionDetails(0)
		, m_LocalGamerIndex(-1)
	{

	}

	bool GetXblInventoryItemTask::Configure( const int localGamerIndex, const char* itemIdentifier, ConsumptionTaskDetails *consumptionDetails )
	{
		m_LocalGamerIndex = localGamerIndex;
		m_ItemIdToFetch = itemIdentifier;

		m_ConsumptionDetails = consumptionDetails;

		return true;
	}

	void GetXblInventoryItemTask::ProcessSuccess()
	{
		if (m_ConsumptionDetails == NULL )
		{
			return;
		}

		try
		{
			InventoryItemsResult^ inventoryResults = m_XblStatus.GetResults<InventoryItemsResult^>();

			// this can happen if we sign out or lose connection (cable pull) while task is in flight
			if(inventoryResults == nullptr)
			{
				return;
			}	

			for( unsigned int itemsFound = 0; itemsFound < inventoryResults->Items->Size; ++itemsFound )
			{
				InventoryItem^ item = inventoryResults->Items->GetAt( itemsFound );
				netTaskDebug("Inventory Item found ID: [%s]",item->ProductId);

				char stringBuffer[ITEM_STRING_BUFFER_SIZE];
				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				cCommercePlatformInventoryItem platformInventoryItem;

				rlXblCommon::WinRtStringToUTF8(item->ProductId, stringBuffer, ITEM_STRING_BUFFER_SIZE);

				if ( strcmp( stringBuffer, m_ItemIdToFetch ) == 0 )
				{
					m_ConsumptionDetails->m_ItemToConsume = item;
					m_ConsumptionDetails->wasItemFound = true;
				}

				// 			char stringBuffer[ITEM_STRING_BUFFER_SIZE];
				// 			ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				// 			cCommercePlatformInventoryItem platformInventoryItem;
				//	
				// 			rlXblCommon::WinRtStringToUTF8(item->ProductId, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				// 			platformInventoryItem.SetProductId(stringBuffer);
				// 
				// 			platformInventoryItem.SetConsumableBalance(item->ConsumableBalance);
				// 
				// 			platformInventoryItem.SetItemType(m_ItemType);
				// 
				// 			m_Details->GetInventoryItemArray().PushAndGrow(platformInventoryItem);
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
		}
	}

	bool GetXblInventoryItemTask::DoWork()
	{
		bool success = false;

		try
		{
			//This is horrible, and I need to talk to MS and see if there is a better solution.

			//Possibly not, because MS's approach to these things is just generally dreadful.

			MediaItemType typeToFetch;
			typeToFetch = MediaItemType::GameConsumable;

			if (!rlXblInternal::GetInstance()->_GetPresenceManager()->IsOnline(m_LocalGamerIndex))
			{
				return false;
			}

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			if ( context == nullptr )
			{
				return false;
			}

			String^ scid = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());

			success = m_XblStatus.BeginOp<InventoryItemsResult^>("GetInventoryItemsAsync",
				context->InventoryService->GetInventoryItemsAsync(typeToFetch),
				DEFAULT_ASYNC_TIMEOUT,
				this,
				&m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	//Util function to create a valid transacton ID for item consumption
	std::wstring CreateTransactionID()
	{
		GUID guid;
		HRESULT hr = CoCreateGuid(&guid);
		if (hr == S_OK)
		{
			WCHAR wsz_guidBuffer[39] = L"";
			if (0 < StringFromGUID2(guid, wsz_guidBuffer, _countof(wsz_guidBuffer)))
			{
				std::wstring guidString( wsz_guidBuffer );

				//Remove the beginning '{' and trailing '}'
				guidString.erase(37,1);
				guidString.erase(0,1);

				return guidString;
			}
		}
		return L"";
	}

	class ConsumeFromXblInventoryItemTask : public netXblTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl);
		NET_TASK_DECL(ConsumeFromXblInventoryItemTask);

		ConsumeFromXblInventoryItemTask();
		bool Configure(const int localGamerIndex, int numToConsume, ConsumptionTaskDetails *consumptionDetails );

	protected:

		static int m_TransactionStringSuffix;

		virtual bool DoWork();
		virtual void ProcessSuccess();
		virtual void ProcessFailure();

		int m_LocalGamerIndex;
		unsigned int m_NumToConsume;
		ConsumptionTaskDetails *m_ConsumptionDetails;
	};


	int ConsumeFromXblInventoryItemTask::m_TransactionStringSuffix = 0;

	ConsumeFromXblInventoryItemTask::ConsumeFromXblInventoryItemTask() 
	{

	}

	bool ConsumeFromXblInventoryItemTask::Configure( const int localGamerIndex, int numToConsume, ConsumptionTaskDetails *consumptionDetails )
	{
		m_LocalGamerIndex = localGamerIndex;
		m_NumToConsume = numToConsume;
		m_ConsumptionDetails = consumptionDetails;
		return true;
	}

	bool ConsumeFromXblInventoryItemTask::DoWork()
	{
		bool success = false;

		try
		{
			//This is horrible, and I need to talk to MS and see if there is a better solution.
			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);

			success = m_XblStatus.BeginOp<ConsumeInventoryItemResult^>("ConsumeInventoryItemAsync",
				context->InventoryService->ConsumeInventoryItemAsync(m_ConsumptionDetails->m_ItemToConsume,m_NumToConsume,ref new Platform::String(CreateTransactionID().c_str())),
				DEFAULT_ASYNC_TIMEOUT,
				this,
				&m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	void ConsumeFromXblInventoryItemTask::ProcessSuccess()
	{
		netTaskError("ConsumeFromXblInventoryItemTask succeeded");
	}

	void ConsumeFromXblInventoryItemTask::ProcessFailure()
	{
		netTaskError("ConsumeFromXblInventoryItemTask failed");
	}

	//
	// Consumption Task which holds the two Xbox subtasks.
	//

	class DoConsumptionTask : public netTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl);
		NET_TASK_DECL(DoConsumptionTask);

		bool Configure(const int localGamerIndex, const char* consumableIdentifier, int numberToConsume );

	protected:
		virtual void OnCancel();
		virtual netTaskStatus OnUpdate(int* resultCode);

	private:
		bool StartItemFetchStage();
		bool StartConsumptionStage();

		int m_LocalGamerIndex;

		atString m_ConsumableIdentifier;
		int m_NumberToConsume;

		enum eConsumptionStage
		{
			CONSUMPTION_INITIAL_STAGE = 0,
			CONSUMPTION_GETTING_INVENTORY_ITEM,
			CONSUMPTION_CONSUMING_INVENTORY_ITEM,
			CONSUMPTION_NUM_STAGES
		};

		eConsumptionStage m_CurrentConsumptionStage;

		netStatus m_GetInventoryItemStatus;
		netStatus m_ConsumeInventoryItemStatus;

		ConsumptionTaskDetails m_ConsumptionTaskDetails;
	};

	bool DoConsumptionTask::Configure( const int localGamerIndex, const char* consumableIdentifier, int numberToConsume )
	{
		m_LocalGamerIndex = localGamerIndex;
		m_ConsumableIdentifier = consumableIdentifier;
		m_NumberToConsume = numberToConsume;

		m_GetInventoryItemStatus.Reset();
		m_ConsumeInventoryItemStatus.Reset();

		m_CurrentConsumptionStage = CONSUMPTION_INITIAL_STAGE;

		m_ConsumptionTaskDetails.wasItemFound = false;

		return true;
	}

	void DoConsumptionTask::OnCancel()
	{
		if (m_GetInventoryItemStatus.Pending())
		{
			rlGetTaskManager()->CancelTask(&m_GetInventoryItemStatus);
		}

		if (m_ConsumeInventoryItemStatus.Pending())
		{
			rlGetTaskManager()->CancelTask(&m_ConsumeInventoryItemStatus);
		}
	}

	netTaskStatus DoConsumptionTask::OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch ( m_CurrentConsumptionStage )
		{
		case(CONSUMPTION_INITIAL_STAGE):
			StartItemFetchStage();
			break;
		case(CONSUMPTION_GETTING_INVENTORY_ITEM):
			if ( m_GetInventoryItemStatus.Failed() )
			{
				status = NET_TASKSTATUS_FAILED;
			}
			else if ( m_GetInventoryItemStatus.Succeeded() )
			{
				if ( m_ConsumptionTaskDetails.wasItemFound )
				{
					StartConsumptionStage();
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}

			}
			break;
		case(CONSUMPTION_CONSUMING_INVENTORY_ITEM):
			if ( m_ConsumeInventoryItemStatus.Failed() )
			{
				status = NET_TASKSTATUS_FAILED;
			}
			else if ( m_ConsumeInventoryItemStatus.Succeeded() )
			{
				status = NET_TASKSTATUS_SUCCEEDED;
			}
			break;
		default:
			break;
		}

		return status;
	}

	bool DoConsumptionTask::StartItemFetchStage()
	{
		m_CurrentConsumptionStage = CONSUMPTION_GETTING_INVENTORY_ITEM;
		CREATE_NET_XBLTASK(GetXblInventoryItemTask, &m_GetInventoryItemStatus, m_LocalGamerIndex, m_ConsumableIdentifier, &m_ConsumptionTaskDetails);
	}

	bool DoConsumptionTask::StartConsumptionStage()
	{
		m_CurrentConsumptionStage = CONSUMPTION_CONSUMING_INVENTORY_ITEM;
		CREATE_NET_XBLTASK(ConsumeFromXblInventoryItemTask, &m_ConsumeInventoryItemStatus, m_LocalGamerIndex, m_NumberToConsume, &m_ConsumptionTaskDetails);
	}

	//
	// CheckoutProductTask
	//

	class CheckoutProductTask : public netXblTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl_commerce);
		NET_TASK_DECL(CheckoutProductTask);

		CheckoutProductTask();
		bool Configure(const int localGamerIndex, const char* signedOfferId);

	protected:
		virtual netTaskStatus OnUpdate(int* resultCode);
		virtual bool DoWork();
		int m_LocalGamerIndex;
		atString m_SignedOfferId;
	private:
		int m_TimeoutStartTime;
	};

	CheckoutProductTask::CheckoutProductTask() :
		m_LocalGamerIndex(0),
		m_TimeoutStartTime(-1)
	{

	}

	netTaskStatus CheckoutProductTask::OnUpdate(int* resultCode)
	{
		if(rlXbl::ShouldCancelCommerceTasks())
		{
			// Allow 5 seconds without showing System UI before canceling checkout process
			// This alleviates a firmware issue that doesn't trigger a status change when exiting System UI 
			// (https://forums.xboxlive.com/questions/56224/showpurchaseasync-hangs-after-snap-and-guide-butto.html)
			if(g_rlXbl.GetSystemUi()->IsUiShowing())
			{
				m_TimeoutStartTime = -1;
			}
			else
			{
				if(m_TimeoutStartTime == -1)
				{
					m_TimeoutStartTime = sysTimer::GetSystemMsTime();
				}
				else if(sysTimer::HasElapsedIntervalMs(m_TimeoutStartTime, 5000))
				{
					m_XblStatus.Cancel();
				}
			}
		}

		return netXblTask::OnUpdate(resultCode);
	}

	bool CheckoutProductTask::Configure( const int localGamerIndex, const char* signedOfferId )
	{
		if (signedOfferId == NULL)
		{
			return false;
		}

		m_LocalGamerIndex = localGamerIndex;
		m_SignedOfferId = signedOfferId;

		return true;
	}

	bool CheckoutProductTask::DoWork()
	{
		bool success = false;

		try
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
			if (actingUser != nullptr)
			{
				wchar_t wszStatName[SIGNED_OFFER_BUFFER_SIZE];
				String^ signedOfferId = rlXblCommon::UTF8ToWinRtString( m_SignedOfferId, wszStatName, COUNTOF(wszStatName));

				success = m_XblStatus.BeginAction("ShowPurchaseAsync",
					Store::Product::ShowPurchaseAsync(actingUser, signedOfferId),
					NO_ASYNC_TIMEOUT,
					this,
					m_XblStatus.FLAGS_DONT_CANCEL_ASYNC_TASK,
					&m_MyStatus);
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	//
	// CodeRedemptionTask
	//

	class CodeRedemptionTask : public netXblTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl_commerce);
		NET_TASK_DECL(CodeRedemptionTask);

		CodeRedemptionTask();
		bool Configure(const int localGamerIndex);

	protected:

		virtual bool DoWork();
		int m_LocalGamerIndex;
	};

	CodeRedemptionTask::CodeRedemptionTask() :
		m_LocalGamerIndex(0)
	{

	}

	bool CodeRedemptionTask::Configure( const int localGamerIndex )
	{
		m_LocalGamerIndex = localGamerIndex;
		return true;
	}

	bool CodeRedemptionTask::DoWork()
	{
		bool success = false;

		try
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
			if (actingUser != nullptr)
			{
				String^ blankOfferId;

				success = m_XblStatus.BeginAction("ShowRedeemCodeAsync",
					Store::Product::ShowRedeemCodeAsync(actingUser, blankOfferId),
					NO_ASYNC_TIMEOUT,
					this,
					m_XblStatus.FLAGS_DONT_CANCEL_ASYNC_TASK,
					&m_MyStatus);
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

#if RSG_GDK
	//
	// GetUserPurchaseIdTask
	//

	class GetUserPurchaseIdTask : public StoreInteractionTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl_commerce);
		NET_TASK_DECL(GetUserPurchaseIdTask);

		GetUserPurchaseIdTask();
		bool Configure(const int localGamerIndex, const char* serviceTicket, const char* publisherId, rlXblCommerceStoreId* storeId);

	protected:

		virtual bool DoWork() override; 
		virtual void ProcessSuccess() override;

		int m_LocalGamerIndex;
		const char* m_ServiceTicket;
		const char* m_PublisherId;
		rlXblCommerceStoreId* m_StoreId;
	};

	GetUserPurchaseIdTask::GetUserPurchaseIdTask()
		: m_LocalGamerIndex(0)
		, m_ServiceTicket(nullptr)
	{

	}

	bool GetUserPurchaseIdTask::Configure(const int localGamerIndex, const char* serviceTicket, const char* publisherId, rlXblCommerceStoreId* storeId)
	{
		rtry
		{
			rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
			rverifyall(serviceTicket != nullptr);
			rverifyall(publisherId != nullptr);
			rverifyall(storeId != nullptr);

			m_LocalGamerIndex = localGamerIndex;
			m_ServiceTicket = serviceTicket;
			m_PublisherId = publisherId;
			m_StoreId = storeId;
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool GetUserPurchaseIdTask::DoWork()
	{
		bool success = false;

		XBLTry
		{
			XLocalUserType actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);

			// store context cleaned up on task cleanup
			HRESULT hr = CreateStoreContextForUser(actingUser);
			if(SUCCEEDED(hr))
			{
				default_xbltask_async_op(
					XStoreGetUserPurchaseIdAsync,
					GetStoreContext(),
					m_ServiceTicket,
					m_PublisherId);

				success = SUCCEEDED(hr);

				// cleanup early on fail
				if(!success)
				{
					CloseStoreContext();
				}				
			}
		}
		XBLCatchException(ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	void GetUserPurchaseIdTask::ProcessSuccess()
	{
		size_t size;
		HRESULT hr = XStoreGetUserPurchaseIdResultSize(
			m_XblStatus.GetAsyncBlock(),
			&size);

		if(FAILED(hr))
		{
			netTaskDebug("Failed retrieve the user purchase ID size: 0x%08x", hr);
			return;
		}

		char* result = new char[size];
		hr = XStoreGetUserPurchaseIdResult(
			m_XblStatus.GetAsyncBlock(),
			size,
			result);

		if(FAILED(hr))
		{
			netTaskDebug("Failed retrieve the user purchase ID result: 0x%08x", hr);
			delete[] result;
			return;
		}

		netTaskDebug("result: %s", result);

		// copy out the store Id
		safecpy(m_StoreId->m_StoreId, result);

		delete[] result;
	}
#endif

	//
	// GetInventoryItemsTask
	//
	class GetInventoryItemsTask : public netXblTask
	{
	public:
		NET_TASK_USE_CHANNEL(rline_xbl_commerce);
		NET_TASK_DECL(GetInventoryItemsTask);

		GetInventoryItemsTask();
		bool Configure(const int localGamerIndex, cCommercePlatformInventoryDetails* details, eCommercePlatformItemType itemType );

	protected:

		virtual void ProcessSuccess();
		virtual void ProcessFailure();

		virtual bool DoWork();
		int m_LocalGamerIndex;

		cCommercePlatformInventoryDetails *m_Details;
		eCommercePlatformItemType m_ItemType;
	};

	GetInventoryItemsTask::GetInventoryItemsTask() :
		m_LocalGamerIndex(-1),
		m_Details(NULL),
		m_ItemType(COMMERCE_INVENTORY_ITEM_TYPE_INVALID)
	{

	}

	bool GetInventoryItemsTask::Configure(const int localGamerIndex, cCommercePlatformInventoryDetails* details, eCommercePlatformItemType itemType  )
	{
		m_LocalGamerIndex = localGamerIndex;
		m_Details = details;
		m_ItemType = itemType;
		return true;
	}

	void GetInventoryItemsTask::ProcessSuccess()
	{
		if (m_Details == NULL )
		{
			return;
		}

		try
		{
			InventoryItemsResult^ inventoryResults = m_XblStatus.GetResults<InventoryItemsResult^>();

			// this can happen if we sign out or lose connection (cable pull) while task is in flight
			if(inventoryResults == nullptr)
			{
				return;
			}

			for( unsigned int itemsFound = 0; itemsFound < inventoryResults->Items->Size; ++itemsFound )
			{
				InventoryItem^ item = inventoryResults->Items->GetAt( itemsFound );
				netTaskDebug("Inventory Item found ID: [%s]",item->ProductId);
				char stringBuffer[ITEM_STRING_BUFFER_SIZE];
				ZeroMemory(stringBuffer, ITEM_STRING_BUFFER_SIZE);
				cCommercePlatformInventoryItem platformInventoryItem;

				rlXblCommon::WinRtStringToUTF8(item->ProductId, stringBuffer, ITEM_STRING_BUFFER_SIZE);
				platformInventoryItem.SetProductId(stringBuffer);

				platformInventoryItem.SetConsumableBalance(item->ConsumableBalance);

				platformInventoryItem.SetItemType(m_ItemType);

				platformInventoryItem.SetOwned(item->InventoryItemState == InventoryItemState::Enabled);

				m_Details->GetInventoryItemArray().PushAndGrow(platformInventoryItem);
			}	
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
		}
	}

	void GetInventoryItemsTask::ProcessFailure()
	{
		netTaskError("GetInventoryTask failed");
	}

	bool GetInventoryItemsTask::DoWork()
	{
		bool success = false;

		try
		{
			MediaItemType typeToFetch;

			switch(m_ItemType)
			{
			case(COMMERCE_INVENTORY_ITEM_TYPE_CONSUMABLE):
				typeToFetch = MediaItemType::GameConsumable;
				break;
			case(COMMERCE_INVENTORY_ITEM_TYPE_DURABLE):
				typeToFetch = MediaItemType::GameContent;
				break;
			default:
				rlAssertf(false,"Unknown or invalid inventory item type passed");
				typeToFetch = MediaItemType::GameContent;
			}

			if (!rlXblInternal::GetInstance()->_GetPresenceManager()->IsOnline(m_LocalGamerIndex))
			{
				return false;
			}

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);

			if ( context != nullptr )
			{
				success = m_XblStatus.BeginOp<InventoryItemsResult^>("GetInventoryItemsAsync",
					context->InventoryService->GetInventoryItemsAsync(typeToFetch),
					DEFAULT_ASYNC_TIMEOUT,
					this,
					&m_MyStatus);
			}
			else
			{
				return false;
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	//
	// Util functions which create the tasks above
	//

	bool rlXblCommerce::GetCatalog( const int localGamerIndex, const char* rootProductId, cCommercePlatformDetails* details, eCommercePlatformItemType itemType, netStatus* status )
	{
		CREATE_NET_XBLTASK(GetCatalogTask, status, localGamerIndex, rootProductId, details, itemType);
	}

	bool rlXblCommerce::GetCatalogDetails( const int localGamerIndex, cCommercePlatformDetails* details, int startIndex, int numToFetch, netStatus* status  )
	{
		CREATE_NET_XBLTASK(GetCatalogDetailsTask, status, localGamerIndex, startIndex, numToFetch, details);
	}

	bool rlXblCommerce::GetInventoryItems( const int localGamerIndex, cCommercePlatformInventoryDetails* details, eCommercePlatformItemType itemType, netStatus* status )
	{
		CREATE_NET_XBLTASK(GetInventoryItemsTask, status, localGamerIndex, details, itemType);
	}

	bool rlXblCommerce::ConsumeFromItem( const int localGamerIndex, const char* idToConsume, int numToConsume, netStatus* status )
	{
		bool success = false;
		DoConsumptionTask* task;
		if(!netTask::Create(&task, status)
			|| !task->Configure(localGamerIndex, idToConsume, numToConsume)
			|| !netTask::Run(task))
		{
			netTask::Destroy(task);
		}
		else
		{
			success = true;
		}

		return success;
	}

	bool rlXblCommerce::DoProductPurchaseCheckout( const int localGamerIndex, const char* availabilityId, netStatus* status )
	{
		CREATE_NET_XBLTASK(CheckoutProductTask, status, localGamerIndex, availabilityId);
	}

	bool rlXblCommerce::DoCodeRedemption( int localGamerIndex, netStatus* status )
	{
		CREATE_NET_XBLTASK(CodeRedemptionTask, status, localGamerIndex);
	}

#if RSG_GDK
	bool rlXblCommerce::GetUserPurchaseId(int localGamerIndex, const char* serviceTicket, const char* publisherId, rlXblCommerceStoreId* storeId, netStatus* status)
	{
		CREATE_NET_XBLTASK(GetUserPurchaseIdTask, status, localGamerIndex, serviceTicket, publisherId, storeId);
	}
#endif
}

#endif //RSG_DURANGO


