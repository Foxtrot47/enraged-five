// 
// rlxblcommon.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_COMMON_H
#define RLXBL_COMMON_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblcommon.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblparty_interface.h instead"
#endif

#include "atl/array.h"
#include "diag/seh.h"
#include "system/criticalsection.h"
#include "system/memops.h"
#include "rline/durango/rlxblsessions_interface.h"

using namespace Platform;
using namespace Windows::Foundation::Collections;

#define RL_EXCEPTION_SILENT(ex)									\
	rageErrorf("Exception 0x%08x, Msg: %ls",					\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");	

#define RL_EXCEPTION_EX_SILENT(loc, ex)							\
	rageErrorf("%s - Exception 0x%08x, Msg: %ls",				\
	loc,														\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");	

#if __ASSERT
#define RL_EXCEPTION(ex)										\
	rageAssertf(false, "Exception 0x%08x, Msg: %ls",			\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");			

#define RL_EXCEPTION_EX(loc, ex)								\
	rageAssertf(false, "%s - Exception 0x%08x, Msg: %ls",		\
	loc,														\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");		
#else
#define RL_EXCEPTION(ex)			RL_EXCEPTION_SILENT(ex)
#define RL_EXCEPTION_EX(loc, ex)	RL_EXCEPTION_EX_SILENT(loc, ex)
#endif

namespace rage
{

class rlXblCommon
{
public:
	// Xuid Conversions
	static bool XuidToWidestring(u64 xuid, wchar_t* outBuf, int outBufLen);
	static String^ XuidToWinRTString(u64 xuid);
	static bool WinRtStringToUTF8(String^ winRtString, char* outBuf, int outBufLen);
	static u64 WinRtStringToXuid(String^ xuidStr);

	// String conversions
	static String^ UTF8ToWinRtString(const char * inStr, const wchar_t * outBuf, const int outBufLen);

	// WinRT String Comparison
	static bool IsStringEqual( Platform::String^ val1, Platform::String^ val2 );
	static bool IsStringEqualI( Platform::String^ val1, Platform::String^ val2 );
	static bool IsStringNEqual( Platform::String^ val1, Platform::String^ val2, const u32 num );
	static bool IsStringNEqualI( Platform::String^ val1, Platform::String^ val2, const u32 num );

	// Display name
	// Note: if 'in' is >= RL_MAX_NAME_LENGTH chars, out will contain RL_MAX_NAME_LENGTH chars, including a trailing ellipsis as required by XRs
	static char* DisplayNameToUtf8(char (&out)[RL_MAX_DISPLAY_NAME_BUF_SIZE], const wchar_t *in);

	// Convert to MICROSOFT xbox services
	static Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ 
		ConvertToMicrosoftXboxServicesMultiplayerSessionReference (Windows::Xbox::Multiplayer::MultiplayerSessionReference^ sessionRef)
	{
		return ref new Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference( sessionRef->ServiceConfigurationId, sessionRef->SessionTemplateName, sessionRef->SessionName);
	}

	static Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ 
		ConvertToMicrosoftXboxServicesMultiplayerSessionReference (rlXblSessionHandle* sessionHandle)
	{
		String^ ServiceConfigurationId = ref new String(sessionHandle->m_ServiceConfigurationId);
		String^ SessionTemplateName = ref new String(sessionHandle->m_SessionTemplateName);
		String^ SessionName = ref new String(sessionHandle->m_SessionName.ToWideString());

		return ref new Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference( ServiceConfigurationId, SessionTemplateName, SessionName);
	}

	static Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ 
		ConvertToMicrosoftXboxServicesMultiplayerSessionReference (const rlXblSessionHandle& sessionHandle)
	{
		String^ ServiceConfigurationId = ref new String(sessionHandle.m_ServiceConfigurationId);
		String^ SessionTemplateName = ref new String(sessionHandle.m_SessionTemplateName);
		String^ SessionName = ref new String(sessionHandle.m_SessionName.ToWideString());

		return ref new Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference( ServiceConfigurationId, SessionTemplateName, SessionName);
	}

	// Convert to WINDOWS xbox services
	static Windows::Xbox::Multiplayer::MultiplayerSessionReference^
		rlXblCommon::ConvertToWindowsXboxServicesMultiplayerSessionReference (Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^  sessionRef)
	{
		return ref new Windows::Xbox::Multiplayer::MultiplayerSessionReference( sessionRef->SessionName, sessionRef->ServiceConfigurationId, sessionRef->SessionTemplateName);
	}
	
	static Windows::Xbox::Multiplayer::MultiplayerSessionReference^
		rlXblCommon::ConvertToWindowsXboxServicesMultiplayerSessionReference (rlXblSessionHandle* sessionHandle)
	{
		String^ ServiceConfigurationId = ref new String(sessionHandle->m_ServiceConfigurationId);
		String^ SessionTemplateName = ref new String(sessionHandle->m_SessionTemplateName);
		String^ SessionName = ref new String(sessionHandle->m_SessionName.ToWideString());

		return ref new Windows::Xbox::Multiplayer::MultiplayerSessionReference( SessionName, ServiceConfigurationId, SessionTemplateName);
	}

	static Windows::Xbox::Multiplayer::MultiplayerSessionReference^
		rlXblCommon::ConvertToWindowsXboxServicesMultiplayerSessionReference (const rlXblSessionHandle& sessionHandle)
	{
		String^ ServiceConfigurationId = ref new String(sessionHandle.m_ServiceConfigurationId);
		String^ SessionTemplateName = ref new String(sessionHandle.m_SessionTemplateName);
		String^ SessionName = ref new String(sessionHandle.m_SessionName.ToWideString());

		return ref new Windows::Xbox::Multiplayer::MultiplayerSessionReference( SessionName, ServiceConfigurationId, SessionTemplateName);
	}

	// Create rlXblHandles from SessionReferences
	static bool rlXblCommon::ImportXblHandle(Windows::Xbox::Multiplayer::MultiplayerSessionReference^ sessionRef, rlXblSessionHandle* handle);
	static bool rlXblCommon::ImportXblHandle(Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef,  rlXblSessionHandle* handle);

	// Date functions
	static Windows::Foundation::DateTime GetCurrentDateTime();
	static double GetDateTimeDiffInSeconds(u64 t1, Windows::Foundation::DateTime dt2);
	static double GetDateTimeDiffInSeconds(Windows::Foundation::DateTime dt1, Windows::Foundation::DateTime dt2);
};

public ref class rlXblStringVectorView sealed : public IVectorView<String^>
{
public:
	rlXblStringVectorView(const Array<String^>^ strings)
	{
		for (unsigned int i = 0; i < strings->Length; i++)
		{
			m_Array.PushAndGrow(strings[i]);
		}
	}

	// necessary for IIterable interface
	virtual IIterator<String^>^ First()
	{
		return nullptr;
	}

	virtual String^ GetAt(unsigned int index)
	{
		rlAssertf((int)index < m_Array.size(), "rlXblStringVectorView - Index out of range");
		return m_Array[index];
	}

	virtual unsigned int GetMany(unsigned int startIndex, WriteOnlyArray<String^, 1>^ items)
	{
		int count = 0;
		for (int i = startIndex ; i < m_Array.GetCount(); i++)
		{
			items->set(i, m_Array[i]);
			count++;
		}
		
		return count;
	}

	virtual bool IndexOf(String^ value, unsigned int* index)
	{
		for (int i = 0 ; i < m_Array.GetCount(); i++)
		{
			if (m_Array[i] == value)
			{
				*index = i;
				return true;
			}
		}

		*index = 0;
		return false;
	}

	virtual property unsigned int Size
	{
		virtual unsigned int get() = IVectorView<String^>::Size::get
		{
			return m_Array.GetCount();
		}
	}

private:
	atArray<String^> m_Array;
};

template<typename T, unsigned QUEUE_SIZE>
class rlXblEventQueue
{
	public:

	rlXblEventQueue()
	{
		this->Clear();
	}

	void Clear()
	{
		SYS_CS_SYNC(m_Cs);
		m_Count = m_Next = m_Free = 0;
	}

	bool QueueEvent(const T& event)
	{
		SYS_CS_SYNC(m_Cs);
		const bool success = rlVerify(m_Count < QUEUE_SIZE);
		if(success)
		{
			sysMemCpy(&m_Queue[m_Free], &event, sizeof(event));
			++m_Count;
			++m_Free;
			if(m_Free >= QUEUE_SIZE)
			{
				m_Free = 0;
			}
		}
		return success;
	}

	bool NextEvent(T* event)
	{
		SYS_CS_SYNC(m_Cs);
		rlAssert(m_Count >= 0);
		rlAssert(m_Count <= QUEUE_SIZE);
		bool success = false;
		if(m_Count > 0)
		{
			*event = m_Queue[m_Next];
			--m_Count;
			++m_Next;
			if(m_Next >= QUEUE_SIZE)
			{
				m_Next = 0;
			}
			success = true;
		}
		return success;
	};

	T* GetEvent(int index)
	{
		if (!netVerify(index < QUEUE_SIZE))
		{
			return NULL;
		}

		return &(m_Queue[index]);
	}

	unsigned GetNext() const
	{
		return (unsigned) m_Next;
	}
	unsigned GetCount() const
	{
		return (unsigned) m_Count;
	}

private:

	sysCriticalSectionToken m_Cs;
	T m_Queue[QUEUE_SIZE];
	int m_Count;
	int m_Next;
	int m_Free;
};

} // namespace rage

#endif  // RSG_DURANGO

#endif  //RLXBL_COMMON_H
