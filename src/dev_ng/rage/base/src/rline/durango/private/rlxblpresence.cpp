﻿// 
// rlxblpresence.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlxblpresence.h"
#include "avchat/voicechat_durango.h"
#include "data/rson.h"
#include "diag/seh.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "net/netfuncprofiler.h"
#include "net/nethardware.h"
#include "net/task.h"
#include "rline/rldiag.h"
#include "rline/rlpresence.h"
#include "rline/rltitleid.h"
#include "rlxblhttptask.h"
#include "string/unicode.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/userlist_durango.winrt.h"
#include "system/timer.h"

#include <wchar.h>

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Xbox::System;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Microsoft::Xbox::Services::Presence;
using namespace Windows::Networking::Connectivity;

#define MAX_RP_FRIENDLY_NAME_LEN 32

#if __BANK
#include "bank/bkmgr.h"
#include "bank/bank.h"
#endif

namespace rage
{

#undef __rage_channel
#define __rage_channel rline_xbl

extern const rlTitleId* g_rlTitleId;

bool rlXblPresence::sm_XboxLiveAccess = false;
sysCriticalSectionToken rlXblPresence::m_NetworkStatusCS;
sysCriticalSectionToken rlXblPresence::m_LocalGamersCS;

///////////////////////////////////////////////////////////////////////////////
//  LocalGamer
///////////////////////////////////////////////////////////////////////////////
class LocalGamer
{
public:
	LocalGamer()
	{
		Clear();
	}

	void Clear()
	{
		memset(m_DisplayName, 0, sizeof(m_DisplayName));
		memset(m_Nickname, 0, sizeof(m_Nickname));		
		m_XboxLiveContext = nullptr;
		m_User = nullptr;
		m_Xuid = 0;
		m_LocalGamerIndex = -1;
		m_IsGuest = false;
		m_IsSignedIn = false;
	}

	bool Init(int localGamerIndex,
			  const char* nickname,
			  const char* displayName,
			  XboxLiveContext^ context,
			  User^ user,
			  u64 xuid,
			  bool isSignedIn,
			  bool isNewUser,
			  bool isGuest)
	{
		m_LocalGamerIndex = localGamerIndex;
		safecpy(m_Nickname, nickname);
		safecpy(m_DisplayName, displayName);
		m_XboxLiveContext = context;
		m_User = user;
		m_Xuid = xuid;
		m_IsSignedIn = isSignedIn;
		m_IsNewUser = isNewUser;
		m_IsGuest = isGuest;
		return true;
	}

	bool IsValid()
	{
		return m_Xuid != 0;
	}

	char m_Nickname[RL_MAX_NAME_BUF_SIZE];
	char m_DisplayName[RL_MAX_DISPLAY_NAME_BUF_SIZE];

	XboxLiveContext^ m_XboxLiveContext;
	User^ m_User;
	u64 m_Xuid;
	int m_LocalGamerIndex;
	bool m_IsSignedIn : 1;
	bool m_IsNewUser : 1;
	bool m_IsGuest : 1;
};

#define XBLPRESENCE_CREATE_RL_SUBTASK(T, ...)								\
	bool success = false;													\
	T* task = NULL;															\
	rtry																	\
	{																		\
		task = rlGetTaskManager()->CreateTask<T>();							\
		rverify(task,catchall,);											\
		rverify(rlTaskBase::Configure(task, __VA_ARGS__),catchall,);		\
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);		\
		success = true;														\
	}																		\
	rcatchall																\
	{																		\
		if(task)															\
		{																	\
			rlGetTaskManager()->DestroyTask(task);							\
		}																	\
	}																		\
	return success;


///////////////////////////////////////////////////////////////////////////////
//  XblSetRichPresenceTask
///////////////////////////////////////////////////////////////////////////////

class XblSetRichPresenceTask : public netXblTask
{
public:

	NET_TASK_DECL(XblSetRichPresenceTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	XblSetRichPresenceTask();

	bool Configure(const int localGamerIndex, const char * statusString, const int statusLen);

private:

	virtual void ProcessSuccess();
	virtual bool DoWork();

	int m_LocalGamerIndex;

	State m_State;
	String^ m_PresenceId;
	String^ m_ConfigurationId;
	PresenceData^ m_RichPresenceData;
};

XblSetRichPresenceTask::XblSetRichPresenceTask()
	: m_PresenceId(nullptr)
	, m_ConfigurationId(nullptr)
	, m_RichPresenceData(nullptr)
	, m_LocalGamerIndex(0)
{
}

bool XblSetRichPresenceTask::Configure(const int localGamerIndex, const char * statusString, const int statusLen)
{
	rtry
	{
		rverify(statusLen < MAX_RP_FRIENDLY_NAME_LEN, catchall, );
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

		m_LocalGamerIndex = localGamerIndex;

		// Copy the status string to a widechar buffer and create a String^
		wchar_t wszPresence[MAX_RP_FRIENDLY_NAME_LEN] = {0};
		m_PresenceId = rlXblCommon::UTF8ToWinRtString(statusString, wszPresence, COUNTOF(wszPresence));
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool XblSetRichPresenceTask::DoWork()
{
	bool success = false;

	try
	{
		rtry
		{
			netTaskDebug("Setting rich presence: %ls", m_PresenceId->Data());

			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rcheck(context != nullptr && context->User != nullptr, catchall, );

			m_ConfigurationId = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());
			rverify(m_ConfigurationId != nullptr, catchall, );

			m_RichPresenceData = ref new PresenceData(m_ConfigurationId, m_PresenceId);
			rverify(m_RichPresenceData != nullptr, catchall, );

			rverify(m_XblStatus.BeginAction("SetPresenceAsync", 
				context->PresenceService->SetPresenceAsync(true, m_RichPresenceData), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus), 
				catchall, );

			success = true;
		}
		rcatchall
		{
			success = false;
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

void XblSetRichPresenceTask::ProcessSuccess()
{
	netTaskDebug("Presence wrote successfully.");
}

///////////////////////////////////////////////////////////////////////////////
//  XblGetRichPresenceTask
///////////////////////////////////////////////////////////////////////////////

class XblGetRichPresenceTask : public netXblTask
{
public:

	NET_TASK_DECL(XblGetRichPresenceTask);
	NET_TASK_USE_CHANNEL(rline_xbl);

	XblGetRichPresenceTask();
	bool Configure(const int localGamerIndex, const u64 targetXuid);

private:

	virtual void ProcessSuccess();
	virtual bool DoWork();

	int m_LocalGamerIndex;
	u64 m_TargetXuid;
};

XblGetRichPresenceTask::XblGetRichPresenceTask()
	: m_LocalGamerIndex(0)
	, m_TargetXuid(0)
{

}

bool XblGetRichPresenceTask::Configure(const int localGamerIndex, const u64 targetXuid)
{
	rtry
	{
		m_LocalGamerIndex = localGamerIndex;
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex), catchall, );

		m_TargetXuid = targetXuid;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool XblGetRichPresenceTask::DoWork()
{
	bool success = false;

	try
	{
		rtry
		{
			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			rcheck(context != nullptr && context->User != nullptr, catchall, );

			String^ xboxUserId = rlXblCommon::XuidToWinRTString(m_TargetXuid);
			rcheck(xboxUserId != nullptr, catchall, );

			netTaskDebug("Getting rich presence for: %ls", xboxUserId->Data());

			rverify(m_XblStatus.BeginOp<PresenceRecord^>("GetPresenceAsync", 
				context->PresenceService->GetPresenceAsync(xboxUserId),  DEFAULT_ASYNC_TIMEOUT,  this, &m_MyStatus),  
				catchall, );

			success = true;
		}
		rcatchall
		{
			success = false;
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}


void XblGetRichPresenceTask::ProcessSuccess()
{
	try
	{
		PresenceRecord^ record = m_XblStatus.GetResults<PresenceRecord^>();
		if (record == nullptr)
			return;

		netTaskDebug3("Presence Record was valid.");

		switch(record->UserState)
		{
		case Microsoft::Xbox::Services::Presence::UserPresenceState::Away:
			netTaskDebug3("User State: Away");
			break;
		case Microsoft::Xbox::Services::Presence::UserPresenceState::Online:
			netTaskDebug3("User State: Online");
			break;
		case Microsoft::Xbox::Services::Presence::UserPresenceState::Offline:
			netTaskDebug3("User State: Offline");
			break;
		case Microsoft::Xbox::Services::Presence::UserPresenceState::Unknown:
		default:
			netTaskDebug3("User State: Unknown");
			break;
		}

#if !__NO_OUTPUT
		if (record->PresenceTitleRecords != nullptr)
		{
			for each (PresenceTitleRecord^ titleRecord in record->PresenceTitleRecords)
			{
				u64 timeDelta = rlXblCommon::GetDateTimeDiffInSeconds(titleRecord->LastModifiedDate, rlXblCommon::GetCurrentDateTime());
				netTaskDebug3("=========================================");
				netTaskDebug3("Title Name: %ls", titleRecord->TitleName != nullptr ? titleRecord->TitleName->Data() : L"");
				netTaskDebug3("Presence: %ls", titleRecord->Presence != nullptr ? titleRecord->Presence->Data() : L"");
				netTaskDebug3("Last Modified Date: %" I64FMT "u (%" I64FMT "u seconds ago)", titleRecord->LastModifiedDate.UniversalTime, timeDelta);
				netTaskDebug3("Is Title Active: %s", titleRecord->IsTitleActive ? "yes" : "no");
				netTaskDebug3("Presence Device Type: %d", titleRecord->DeviceType);
				netTaskDebug3("Presence Title View State: %d", titleRecord->TitleViewState);
			}
			netTaskDebug3("=========================================");
		}
#endif
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}
}


///////////////////////////////////////////////////////////////////////////////
//  rlXblPresence
///////////////////////////////////////////////////////////////////////////////

rlXblPresence::rlXblPresence()
: m_LocalGamers(NULL)
, m_WasOnline(false)
, m_RefreshLocalGamers(1)
, m_RefreshNetworkStatus(true)
#if RSG_BANK
, m_GetRichPresence(false)
, m_uRichPresenceWriteTime(0)
#endif
{

}

rlXblPresence::~rlXblPresence()
{

}

bool 
rlXblPresence::Init()
{
	bool success = false;

	SYS_CS_SYNC(m_LocalGamersCS);

	rtry
	{
		m_LocalGamers = (LocalGamerList*)rlXblInternal::GetInstance()->GetAllocator()->Allocate(sizeof(*m_LocalGamers), 0);
		rverify(m_LocalGamers, catchall, );

		new (m_LocalGamers) LocalGamerList();

		for(int i = 0; i < m_LocalGamers->GetMaxCount(); ++i)
		{
			m_LocalGamers->Push(NULL);
		}

		RegisterEventHandlers();
		
		RefreshNetworkStatus();
		RefreshLocalGamers();
		
		BANK_ONLY(AddWidgets());

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void 
rlXblPresence::Shutdown()
{
	SYS_CS_SYNC(m_LocalGamersCS);

	u32 numGamers = m_LocalGamers->GetCount();
	for(u32 i = 0; i < numGamers; ++i)
	{
		LocalGamer* localGamer = (*m_LocalGamers)[i];
		if(localGamer)
		{
			Delete(localGamer, *rlXblInternal::GetInstance()->GetAllocator());
		}
	}

	m_LocalGamers->Reset();

	Delete(m_LocalGamers, *rlXblInternal::GetInstance()->GetAllocator());

	sysUserList::GetInstance().RemoveDelegate(&m_Delegate);
	m_Delegate.Reset();
}

#if __BANK
static void NetworkBank_Offline()
{
	rlDebug3("NetworkBank_Offline");
	sysTimer t;
	rlXblPresence::GetXboxLiveAccess();
	rlDebug3("GetXboxLiveAccess took %f ms", t.GetMsTime());
	rlXblPresence::sm_XboxLiveAccess = false;
}

static void NetworkBank_Online()
{
	rlDebug3("NetworkBank_Online");
	rlXblPresence::sm_XboxLiveAccess = true;
}

void rlXblPresence::AddWidgets()
{
	bkBank *pBank = BANKMGR.FindBank("Network");

	if(!pBank)
	{
		pBank = &BANKMGR.CreateBank("Network");
	}

	pBank->PushGroup("Xbl Presence", false);

	pBank->AddButton("Fake Offline Event", datCallback(NetworkBank_Offline));
	pBank->AddButton("Fake Online Event", datCallback(NetworkBank_Online));

	pBank->PopGroup();
}
#endif

void 
rlXblPresence::Update()
{
	NPROFILE(RefreshNetworkStatus());
	NPROFILE(RefreshLocalGamers());

#if RSG_BANK
	static const int POST_WRITE_READ_DELAY = (30 * 1000);
	if (m_GetRichPresence && sysTimer::HasElapsedIntervalMs(m_uRichPresenceWriteTime, POST_WRITE_READ_DELAY) && !m_GetRichPresenceStatus.Pending() && RL_IS_VALID_LOCAL_GAMER_INDEX(rlPresence::GetActingUserIndex()))
	{
		const int localGamerIndex = rlPresence::GetActingUserIndex();
		GetRichPresence(localGamerIndex, rlXblPresence::GetXboxUserId(localGamerIndex), &m_GetRichPresenceStatus);
		m_GetRichPresence = false;
		m_uRichPresenceWriteTime = 0;
	}
#endif
}

void 
rlXblPresence::OnUserSignedIn(int localGamerIndex, User^ user)
{
	SYS_CS_SYNC(m_LocalGamersCS);

	try
	{
		LocalGamer* localGamer = (*m_LocalGamers)[localGamerIndex];

		u64 xuid = 0;
		if(!rlVerify(swscanf_s(user->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1))
		{
			return;
		}

		localGamer = (LocalGamer*)rlXblInternal::GetInstance()->GetAllocator()->Allocate(sizeof(LocalGamer), 0);

		if(rlVerify(localGamer))
		{
			(*m_LocalGamers)[localGamerIndex] = localGamer;

			char nickname[RL_MAX_NAME_BUF_SIZE] = {0};
			WideToAscii(nickname, (char16*)user->DisplayInfo->Gamertag->Data(), sizeof(nickname));

			char displayName[RL_MAX_DISPLAY_NAME_BUF_SIZE] = {0};
			rlXblCommon::DisplayNameToUtf8(displayName, user->DisplayInfo->GameDisplayName->Data());

			XboxLiveContext^ xboxLiveContext = ref new XboxLiveContext(user);

			new (localGamer) LocalGamer();
			bool isNewUser = true;
			bool bIsGuest = user->IsGuest;

			localGamer->Init(localGamerIndex, nickname, displayName, xboxLiveContext, user, xuid, true, isNewUser, bIsGuest);

			rlXblPresenceEventSigninStatusChanged e(localGamer->m_LocalGamerIndex, true);
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

void 
rlXblPresence::OnUserSignedOut(int localGamerIndex)
{
	SYS_CS_SYNC(m_LocalGamersCS);

	try
	{
		LocalGamer* localGamer = (*m_LocalGamers)[localGamerIndex];

		// Terminate any real-time activity that is using this local gamer index
		g_rlXbl.GetRealtimeActivityManager()->OnSignOut(localGamerIndex);

		// delete the user before raising the event, since event handlers can
		// immediately check if the user is signed in
		int localGamerIndex = localGamer->m_LocalGamerIndex;
		Delete(localGamer, *rlXblInternal::GetInstance()->GetAllocator());
		(*m_LocalGamers)[localGamerIndex] = NULL;

		rlXblPresenceEventSigninStatusChanged e(localGamerIndex, false);
		rlXblInternal::GetInstance()->DispatchEvent(&e);
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

void 
rlXblPresence::OnUserListUpdated(const sysUserInfo& /*userInfo*/)
{
	sysInterlockedExchange((unsigned int*)&m_RefreshLocalGamers, 1);
}

void 
rlXblPresence::RegisterEventHandlers()
{
	// these events can get fired from another thread, so the handlers need to be thread safe

	m_Delegate.Bind(this, &rlXblPresence::OnUserListUpdated);
	sysUserList::GetInstance().AddDelegate(&m_Delegate);

	try
	{
		NetworkInformation::NetworkStatusChanged += ref new NetworkStatusChangedEventHandler([this] (Platform::Object^)
		{
			bool hasAccess = GetXboxLiveAccess();
			rlDebug("NetworkInformation::NetworkStatusChanged :: Access: %s", hasAccess ? "True" : "False");

			if(sm_XboxLiveAccess != hasAccess && hasAccess)
			{
				rlDebug("NetworkInformation::NetworkStatusChanged :: Come online, refreshing local secure device address");
			}

			sm_XboxLiveAccess = hasAccess;
		});
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}
}

void 
rlXblPresence::RefreshLocalGamers()
{
	if(g_rlXbl.IsInitialized() == false)
	{
		return;
	}

	bool refreshLocalGamers = sysInterlockedExchange(&m_RefreshLocalGamers, 0) != 0;

	if(refreshLocalGamers == false)
	{
		return;
	}

	SYS_CS_SYNC(m_LocalGamersCS);

	try
	{
		// Step 1: check our existing list, and see if anyone has been removed,
		// set that array entry to NULL for anyone removed
		for(unsigned i = 0; i < (unsigned)m_LocalGamers->GetCount(); ++i)
		{
			LocalGamer* localGamer = (*m_LocalGamers)[i];
			if(localGamer == NULL)
			{
				continue;
			}

			const sysUserInfo* user = sysUserList::GetInstance().GetUserInfo(i);
			bool gamerRemoved = (user == nullptr) || (user->IsSignedIn() == false) || (localGamer->m_Xuid != user->GetUserId());

			if(gamerRemoved)
			{
				OnUserSignedOut(i);
			}
		}

		// Step 2: add any new players
		for(u32 i = 0; i < (u32)sysUserList::GetInstance().GetMaxUsers(); ++i)
		{
			const sysUserInfo* user = sysUserList::GetInstance().GetUserInfo(i);

			if((user == NULL) || (user->IsSignedIn() == false))
			{
				continue;
			}

			bool newGamer = true;

			LocalGamer* localGamer = (*m_LocalGamers)[i];
			if(localGamer != NULL)
			{
				// we already have this user
				newGamer = false;
				continue;
			}

			if(newGamer)
			{
				const sysDurangoUserInfo* durangoUser = static_cast<const sysDurangoUserInfo*>(user);
				OnUserSignedIn(i, durangoUser->GetPlatformUser());
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

bool 
rlXblPresence::GetXboxLiveAccess()
{
	bool hasAccess = false;

	try
	{
		auto internetProfile = NetworkInformation::GetInternetConnectionProfile();

		// internetProfile is NULL when the ethernet cable is unplugged 
		// for some reason, so if it's NULL, consider us offline
		if(internetProfile != nullptr)
		{
			auto connectionLevel = internetProfile->GetNetworkConnectivityLevel();
			hasAccess = connectionLevel == Windows::Networking::Connectivity::NetworkConnectivityLevel::XboxLiveAccess;
			rlDebug3("Checking internet connection access (%d), connection level: %d", hasAccess, connectionLevel);
		}
		else
		{
			rlWarning("Cable not connected.");
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}

	return hasAccess;
}

void 
rlXblPresence::RefreshNetworkStatus()
{
	SYS_CS_SYNC(m_NetworkStatusCS);

	if(g_rlXbl.IsInitialized() == false)
	{
		return;
	}

	try
	{
		// If we need to refresh the network status.
		if (m_RefreshNetworkStatus)
		{
			rlDebug3("Reading Network Status");

			// Update the access flag
			sm_XboxLiveAccess = GetXboxLiveAccess();

			// clear flag
			m_RefreshNetworkStatus = false;
		}

		bool bIsOnline = sm_XboxLiveAccess;

		// check for a change in access
		if(bIsOnline && !m_WasOnline)
		{
			rlDebug3("XboxLiveAccess changed from %d to %d", m_WasOnline, bIsOnline);
			m_WasOnline = bIsOnline;
			rlXblPresenceEventNetworkStatusChanged e(rlXblPresenceEventNetworkStatusChanged::FLAG_CONNECTED_TO_XBL);
			NPROFILE(rlXblInternal::GetInstance()->DispatchEvent(&e));
		}
		else if(!bIsOnline && m_WasOnline)
		{
			rlDebug3("XboxLiveAccess changed from %d to %d", m_WasOnline, bIsOnline);
			m_WasOnline = bIsOnline;
			rlXblPresenceEventNetworkStatusChanged e(0);
			NPROFILE(rlXblInternal::GetInstance()->DispatchEvent(&e));
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

LocalGamer* 
rlXblPresence::GetLocalGamer(int localGamerIndex)
{
	SYS_CS_SYNC(m_LocalGamersCS);

	if((localGamerIndex >= 0) && (localGamerIndex < m_LocalGamers->GetCount()))
	{
		return (*m_LocalGamers)[localGamerIndex];
	}

	rlError("GetLocalGamer could not find gamer at index : %d", localGamerIndex);
	return NULL;
}

bool 
rlXblPresence::IsSignedIn(int localGamerIndex)
{
	bool isSignedIn = false;

	rtry
	{
		LocalGamer* localGamer = GetLocalGamer(localGamerIndex);
		rcheck(localGamer, catchall, );
		isSignedIn = localGamer->m_IsSignedIn;
	}
	rcatchall
	{

	}

	return isSignedIn;
}

bool 
rlXblPresence::IsOnline(int localGamerIndex)
{
	return m_WasOnline && IsSignedIn(localGamerIndex);
}

bool 
rlXblPresence::IsOnline()
{
	return m_WasOnline;
}

const char* 
rlXblPresence::GetName(int localGamerIndex)
{
	const char* name = NULL;

	rtry
	{
		LocalGamer* localGamer = GetLocalGamer(localGamerIndex);
		rverify(localGamer, catchall, rlError("Could not find LocalGamer* for GetName at index : %d", localGamerIndex));
		name = localGamer->m_Nickname;
	}
	rcatchall
	{

	}

	return name;
}

const char* 
rlXblPresence::GetDisplayName(int localGamerIndex)
{
	const char* displayName = NULL;

	rtry
	{
		LocalGamer* localGamer = GetLocalGamer(localGamerIndex);
		rverify(localGamer, catchall, rlError("Could not find LocalGamer* for DisplayName at index : %d", localGamerIndex));
		displayName = localGamer->m_DisplayName;
	}
	rcatchall
	{

	}

	return displayName;
}

u64 
rlXblPresence::GetXboxUserId(int localGamerIndex)
{
	u64 xuid = 0;

	rtry
	{
		LocalGamer* localGamer = GetLocalGamer(localGamerIndex);
		rverify(localGamer, catchall, rlError("Could not find LocalGamer* for XboxUserId at index : %d", localGamerIndex));
		xuid = localGamer->m_Xuid;
	}
	rcatchall
	{

	}

	return xuid;
}

bool
rlXblPresence::HasValidXboxUser(int localGamerIndex)
{
	if (GetLocalGamer(localGamerIndex))
	{
		return true;
	}

	return false;
}

String^ rlXblPresence::GetUserIdString(int localGamerIndex)
{
	u64 xuid = GetXboxUserId(localGamerIndex);
	return rlXblCommon::XuidToWinRTString(xuid);
}

bool
rlXblPresence::GetXboxUserHash(int localGamerIndex, wchar_t* hashBuffer, unsigned maxLen)
{
	rtry
	{
		LocalGamer* localGamer = GetLocalGamer(localGamerIndex);

		rverify(localGamer != nullptr, catchall, );
		rverify(localGamer->m_User != nullptr, catchall, );
		rverify(localGamer->m_User->XboxUserHash != nullptr, catchall, );

		String^ hashStr = localGamer->m_User->XboxUserHash;
		rverify(maxLen > hashStr->Length(), catchall, rlError("Buffer was too small"));

		safecpy(hashBuffer, hashStr->Data(), maxLen);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

//PURPOSE
// Set the rich presence string of the specified local user
bool rlXblPresence::SetRichPresence(const rlGamerInfo& gamerInfo, const char* presenceString, netStatus* status)
{
#if RSG_BANK
	// Queue up a rich presence read for logging on bank builds.
    // To help provide more info for MS for url:bugstar:2463832 - [XR-049] Rich Presence isn't updating correctly for GTA V and GTAO.
	m_GetRichPresence = true;
	m_uRichPresenceWriteTime = sysTimer::GetSystemMsTime();
#endif

	int presenceLength = strlen(presenceString);
	CREATE_NET_XBLTASK(XblSetRichPresenceTask, status, gamerInfo.GetLocalIndex(), presenceString, presenceLength);
}


bool rlXblPresence::GetRichPresence(const int localGamerIndex, u64 targetXuid, netStatus* status)
{
	CREATE_NET_XBLTASK(XblGetRichPresenceTask, status, localGamerIndex, targetXuid);
}


XboxLiveContext^ 
rlXblPresence::GetXboxLiveContext(int localGamerIndex)
{
	XboxLiveContext^ xboxLiveContext = nullptr;

	rtry
	{
		// if called from a task, this can fail if the task is queued and then we are signed offline
		LocalGamer* localGamer = GetLocalGamer(localGamerIndex);
		rcheck(localGamer, catchall, );
		xboxLiveContext = localGamer->m_XboxLiveContext;
	}
	rcatchall
	{

	}

	return xboxLiveContext;
}

XboxLiveContext^ rlXblPresence::GetCurrentXboxLiveContext()
{
	XboxLiveContext^ xboxLiveContext = nullptr;

	rtry
	{
		int actingUserIndex = rlPresence::GetActingUserIndex();
		LocalGamer* localGamer = GetLocalGamer(actingUserIndex);
		rverify(localGamer, catchall, );
		xboxLiveContext = localGamer->m_XboxLiveContext;
	}
	rcatchall
	{

	}

	return xboxLiveContext;
}

User^
rlXblPresence::GetXboxUser(int localGamerIndex)
{
	User^ user = nullptr;

	LocalGamer* localGamer = GetLocalGamer(localGamerIndex);
	if (localGamer)
	{
		user = localGamer->m_User;
	}

	return user;
}

int 
rlXblPresence::GetLocalIndex(User^ user)
{
	SYS_CS_SYNC(m_LocalGamersCS);

	try
	{
		for(unsigned i = 0; i < (unsigned)m_LocalGamers->GetCount(); ++i)
		{
			LocalGamer* localGamer = (*m_LocalGamers)[i];
			if(localGamer && localGamer->m_User && localGamer->m_User->XboxUserId == user->XboxUserId)
				return i;
		}
	}
	catch (Platform::Exception^ e)
	{
		RL_EXCEPTION_EX_SILENT("rlXblPresence::GetLocalIndex", e);
	}

	// not found
	return -1;
}

int 
rlXblPresence::GetLocalIndex(IUser^ user)
{
	SYS_CS_SYNC(m_LocalGamersCS);

	try
	{
		for(unsigned i = 0; i < (unsigned)m_LocalGamers->GetCount(); ++i)
		{
			LocalGamer* localGamer = (*m_LocalGamers)[i];
			if(localGamer && localGamer->m_User && localGamer->m_User->XboxUserId == user->XboxUserId)
				return i;
		}
	}
	catch (Platform::Exception^ e)
	{
		RL_EXCEPTION_EX_SILENT("rlXblPresence::GetLocalIndex", e);
	}

	// not found
	return -1;
}

bool rlXblPresence::IsGuest(const int localGamerIndex)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );

		LocalGamer* localGamer = GetLocalGamer(localGamerIndex);
		if (localGamer)
		{
			return localGamer->m_IsGuest;
		}
	}
	rcatchall
	{
		
	}

	return false;
}

IVectorView<User^>^ 
rlXblPresence::GetLocalXboxUsers()
{
	SYS_CS_SYNC(m_LocalGamersCS);

	Array<User^>^ users = ref new Array<User^>(m_LocalGamers->size());
	for (int i = 0; i < m_LocalGamers->size(); i++)
	{
		LocalGamer* localGamer = (*m_LocalGamers)[i];
		if (localGamer)
		{
			users->set(i, localGamer->m_User);
		}
	}
	VectorView<User^>^ userView = ref new VectorView<User^>(users);
	return userView;
}

} // namespace rage

#endif  //RSG_DURANGO
