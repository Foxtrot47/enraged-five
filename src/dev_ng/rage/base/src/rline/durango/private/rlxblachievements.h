// 
// rlxblachievements.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_ACHIEVEMENTS_H
#define RLXBL_ACHIEVEMENTS_H

#if RSG_DURANGO
#include "../rlxblachievements_interface.h"

#if !defined(__cplusplus_winrt)
#error "rlxblachievements.h is a private header. Do not #include it. Use rlxbl_interface.h or rlxblachievements_interface.h instead"
#endif

namespace rage
{

class rlAchievementInfo;
class XblGetAchievementsTask;

//PURPOSE
// Interface to the system UI on Durango
class rlXblAchievements : public IAchievements
{
public:
	rlXblAchievements();
	virtual ~rlXblAchievements();

	//PURPOSE
	// Gets the list of achievements
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the achievements
	virtual bool ReadAchievements(const int localGamerIndex, rlAchievementInfo* achievements, const int maxAchievements, int* numAchievements, netStatus* status);

	//PURPOSE
	// Sets the function that writes XB1 Events for achievements
	virtual void SetAchievementEventFunc(const IAchievements::AchievementEvent& functor);

	//PURPOSE
	// Writes an achievement using the achievement event functor
	virtual bool WriteAchievement(const int localGamerIndex, const int achId);

	//PURPOSE
	// Gets the user stat required for an achievement
	virtual bool ReadStatInt(const int localGamerIndex, const char * statName, const int statNameLength, int* outInt, netStatus* status);

	//PURPOSE
	// Gets the user stat required for an achievement
	virtual bool ReadStatDouble(const int localGamerIndex, const char * statName, const int statNameLength, double* outDouble, netStatus* status);

	//PURPOSE
	//  Initialize the system UI
	bool Init();

	//PURPOSE
	//  Shut down the system UI
	void Shutdown();

	//PURPOSE
	//  Update the the system UI.  Should be called once per frame.
	void Update();

private:
	IAchievements::AchievementEvent m_AchievementEventWriteFunctor;
};


} // namespace rage

#endif // RSG_DURANGO
#endif // RLXBL_ACHIEVEMENTS_H