// 
// rlxblparty.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#include "rlxblparty.h"

#if RSG_XBL

#include "rlxblinternal.h"
#include "rlxblhttptask.h"
#include "rlxblcommon.h"

#include "diag/seh.h"
#include "net/nethardware.h"
#include "rline/rldiag.h"
#include "rline/rlpresence.h"
#include "system/memory.h"
#include "system/nelem.h"

#if RSG_GDK
#include "system/userlist_gdk.h"
#elif RSG_XDK
#include "system/userlist_durango.winrt.h"
#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

using namespace Microsoft::Xbox::Services::Multiplayer;
using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Xbox::System;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Xbox::Multiplayer;
#endif

#define LAST_INVITED_TIME_TIMEOUT 180

extern __THREAD int RAGE_LOG_DISABLE;

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline_xbl, party)
#undef __rage_channel
#define __rage_channel rline_xbl_party

static bool s_bInviteAccepted = false;
static bool s_bInviteActionPending = false;
static bool s_bInviteNotifyPending = false;
static bool s_bLeavePartyOnInvalidGameSession = true; 
static u64 s_InvitedXUID = 0;

void rlXbl::FlagInviteAccepted(u64 xuid)
{
	rlDebug("FlagInviteAccepted :: XUID: %" I64FMT "u", xuid);

	rtry
	{
		rlGamerInfo gamerInfo;
		for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			rlGamerHandle hGamer;
			if(rlPresence::IsSignedIn(i) && rlPresence::GetGamerHandle(i, &hGamer) && hGamer.GetXuid() == xuid)
			{
				rlPresence::GetGamerInfo(i, &gamerInfo);
				rlDebug("FlagInviteAccepted - %s invited. XUID: %" I64FMT "u, Local Index: %d", gamerInfo.GetName(), xuid, gamerInfo.GetLocalIndex());
				break;
			}
		}

		// Invite went to an inactive gamer
		if (!gamerInfo.IsValid())
		{
			rlDebug("FlagInviteAccepted - Cannot find local XUID: %" I64FMT "u", xuid);
			int actingUserIndex = rlPresence::GetActingUserIndex();
			rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(actingUserIndex), catchall, rlError("Acting user index (%d) was invalid.", actingUserIndex));

			// Acting user
#if RSG_GDK
			const sysGdkUserInfo* actingUser = sysGdkUserList::GetPlatformInstance().GetPlatformUserInfo(actingUserIndex);
#else
			const sysDurangoUserInfo* actingUser = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(actingUserIndex);
#endif // RSG_GDK

			rverify(actingUser, catchall,);

			// We reset the presence of the currently active player about to become inactive.
			rlGamerInfo actingGamerInfo;
			rlPresence::GetGamerInfo(actingUserIndex, &actingGamerInfo);

			// Setting the presence to just a whitespace resets the rich presence - Durango only
			rlPresence::SetStatusString(actingGamerInfo, " ", nullptr);
			rlDebug("[rich_presence] FlagInviteAccepted - Resetting presence for acting user at index %d", actingUserIndex);

			// Invited User
#if RSG_GDK
			// NOTE: I am not fully sure if this is correct here. The "signed in" user list doesn't exist on GDK and is one of the main user assignment changes from XDK to GDK.
			//       In the GDK you only know about the users in the game, not all the users on the system.
			const sysGdkUserInfo* invitedUser = sysGdkUserList::GetPlatformInstance().GetPlatformUserInfo(xuid);
#else
			const sysDurangoUserInfo* invitedUser = sysDurangoUserList::GetPlatformInstance().GetSignedInPlatformUserInfo(xuid);
#endif // RSG_GDK

			rverify(invitedUser, catchall,);

			rlDebug("FlagInviteAccepted - Setting user %" I64FMT "d to index %d", xuid, actingUserIndex);

			// swap user to controller index
#if RSG_GDK
			sysGdkUserList::GetPlatformInstance().SetUserInfo(actingUserIndex,
				invitedUser->GetPlatformUser().GetId(),
				false);
#else
			sysDurangoUserList::GetPlatformInstance().SetUserInfo(actingUserIndex, 
				invitedUser->GetPlatformUser(), 
				actingUser->GetPlatformGamepad(),
				false);
#endif // RSG_GDK
		}
	}
	rcatchall
	{

	}

	// set invite accepted and the xuid
	s_bInviteAccepted = true;
	s_bInviteActionPending = true;
	s_bInviteNotifyPending = true;
	s_InvitedXUID = xuid;
}

void rlXbl::FlagInviteActioned()
{
	if(s_bInviteActionPending)
	{
        // do not clear an invite that we haven't told the game about yet - this command is called when we clear / cancel invites
        // due to sign out / bail but should only clear out invites that we've expressly told script about
		rlDebug("FlagInviteActioned :: NotifyPending: %s", s_bInviteNotifyPending ? "True" : "False");
		if(!s_bInviteNotifyPending)
		{
            rlDebug("FlagInviteActioned :: Clearing Action Flag");
            s_bInviteActionPending = false;
		}
	}
}

bool rlXbl::IsInviteActionPending()
{
	return s_bInviteActionPending;
}

void rlXbl::HandleSuspend()
{
    // Ideally, we'd switch to using a service delegate internally to the party system - too close to submission to
    // make that change though so just call through
    // wipe out our party so that new invites to the same party (during the suspend) are recognized correctly
    rlDebug("HandleSuspend");
#if RSG_XDK
    rlXblInternal::GetInstance()->GetPartyManager()->ClearParty(false);
#endif
}

#if !__NO_OUTPUT
const char* GetGameSessionReadyOriginAsString(GameSessionReadyOrigin origin)
{
    static const char* s_Strings[] =
    {
        "ORIGIN_UNKNOWN",
        "ORIGIN_FROM_BOOT",
        "ORIGIN_FROM_INVITE",
        "ORIGIN_FROM_MULTIPLAYER_JOIN",
        "ORIGIN_FROM_JVP",
    };
    CompileTimeAssert(COUNTOF(s_Strings) == GameSessionReadyOrigin::ORIGIN_MAX);

    return ((origin >= GameSessionReadyOrigin::ORIGIN_UNKNOWN) && (origin < GameSessionReadyOrigin::ORIGIN_MAX)) ? s_Strings[origin] : "ORIGIN_INVALID";
}
#endif

#if RSG_XDK
///////////////////////////////////////////////////////////////////////////////
//  GetPartyMemberInfoTask
///////////////////////////////////////////////////////////////////////////////
class GetPartyMemberInfoTask : public rlXblHttpProfileSettingsTask
{
private:
	rlXblParty* m_PartyPtr;
	PartyView^ m_PartyView;
	int m_PartySize;

public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(GetPartyMemberInfoTask);

	GetPartyMemberInfoTask() {}
	virtual ~GetPartyMemberInfoTask() {}

	bool Configure(const int localGamerIndex, rlXblParty* xblParty, PartyView^ partyView);
	bool ProcessUserDisplayName(u64 xuid, const char* displayName, int& resultCode);
	void Finish(const FinishType finishType, const int resultCode = 0);
};

///////////////////////////////////////////////////////////////////////////////
//  AddLocalUsersToPartyTask
///////////////////////////////////////////////////////////////////////////////
class AddLocalUsersToPartyTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(AddLocalUsersToPartyTask);

	AddLocalUsersToPartyTask();
	bool Configure(const int localGamerIndex);

protected:
	virtual bool DoWork();
	int m_LocalGamerIndex;
};

///////////////////////////////////////////////////////////////////////////////
//  RemoveLocalUsersFromPartyTask
///////////////////////////////////////////////////////////////////////////////
class RemoveLocalUsersFromPartyTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(RemoveLocalUsersFromPartyTask);

	RemoveLocalUsersFromPartyTask();
	bool Configure(const int localGamerIndex);
	
	static bool IsLeaving() { return s_bIsLeaving; }

protected:

	virtual bool DoWork();
	virtual void OnCleanup();

	static bool s_bIsLeaving;
	int m_LocalGamerIndex;
};

///////////////////////////////////////////////////////////////////////////////
//  InviteToPartyTask
///////////////////////////////////////////////////////////////////////////////
class InviteToPartyTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(InviteToPartyTask);

	InviteToPartyTask();
	bool Configure(const int localGamerIndex, const rlGamerHandle* targetUsers, int numUsers);

protected:
	virtual bool DoWork();
	int m_LocalGamerIndex;
	Array<String^>^ m_Invitees;
};

///////////////////////////////////////////////////////////////////////////////
//  RegisterGameSessionTask
///////////////////////////////////////////////////////////////////////////////
class RegisterGameSessionTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(RegisterGameSessionTask);

	RegisterGameSessionTask();
	bool Configure(const int localGamerIndex,  Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef);

	static bool IsRegistering() { return s_bIsRegistering; }

protected:
	
	virtual bool DoWork();
	virtual void OnCleanup();

	static bool s_bIsRegistering; 
	int m_LocalGamerIndex;
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ m_SessionRef;
};

///////////////////////////////////////////////////////////////////////////////
//  DisassociateGameSessionTask
///////////////////////////////////////////////////////////////////////////////
class DisassociateGameSessionTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(DisassociateGameSessionTask);

	DisassociateGameSessionTask();
	bool Configure(const int localGamerIndex,  Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef);

protected:
	virtual bool DoWork();
	int m_LocalGamerIndex;
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ m_SessionRef;
};

///////////////////////////////////////////////////////////////////////////////
//  RegisterMatchmakingSessionTask
///////////////////////////////////////////////////////////////////////////////
class RegisterMatchmakingSessionTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(RegisterMatchmakingSessionTask);

	RegisterMatchmakingSessionTask();
	bool Configure(const int localGamerIndex,  Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef);

protected:
	virtual bool DoWork();
	int m_LocalGamerIndex;
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ m_SessionRef;
};

///////////////////////////////////////////////////////////////////////////////
//  SwitchPartyTitleTask
///////////////////////////////////////////////////////////////////////////////
class SwitchPartyTitleTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(SwitchPartyTitleTask);

	SwitchPartyTitleTask();
	bool Configure(const int localGamerIndex);

protected:
	virtual bool DoWork();
	int m_LocalGamerIndex;
};

///////////////////////////////////////////////////////////////////////////////
//  GetGameSessionReadyTask
///////////////////////////////////////////////////////////////////////////////
class GetGameSessionReadyTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(GetGameSessionReadyTask);

	GetGameSessionReadyTask();
	bool Configure(const int localGamerIndex, const rlXblSessionHandle& sessionHandle, rlXblParty* xblParty, GameSessionReadyOrigin origin);

protected:
	virtual void ProcessSuccess();
	virtual void ProcessFailure();
	virtual bool DoWork();

	virtual void GetTarget(rlGamerInfo* pGamerInfo);

	int m_LocalGamerIndex;
	rlSessionInfo m_SessionInfo;
	unsigned m_NumSessions;
	rlXblSessionHandle m_SessionHandle;
	GameSessionReadyOrigin m_Origin;
	rlXblParty* m_PartyPtr;
};

///////////////////////////////////////////////////////////////////////////////
//  ValidateGameSessionTask
///////////////////////////////////////////////////////////////////////////////
class ValidateGameSessionTask : public netXblTask
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl_party);
	RL_TASK_DECL(ValidateGameSessionTask);

	ValidateGameSessionTask();
	bool Configure(const int localGamerIndex, rlXblSessionHandle sessionHandle, rlXblParty* xblParty);

protected:
	virtual void ProcessSuccess();
	virtual void ProcessFailure();
	virtual bool DoWork();

	int m_LocalGamerIndex;
	rlSessionInfo m_SessionInfo;
	unsigned m_NumSessions;
	rlXblSessionHandle m_SessionHandle;
	rlXblParty* m_PartyPtr;
};

///////////////////////////////////////////////////////////////////////////////
//  RefreshPartyTask
///////////////////////////////////////////////////////////////////////////////
class RefreshPartyTask : public netTask
{
public:

	NET_TASK_DECL(RefreshPartyTask);
	NET_TASK_USE_CHANNEL(rline_xbl_party);

	RefreshPartyTask()
		: m_State(STATE_GET_PARTY_VIEW)
		, m_PartyPtr(NULL)
		, m_LocalGamerIndex(-1)
	{
		m_PartyView = nullptr;
	}

	bool Configure(const int localGamerIndex, rlXblParty* xblParty)
	{
		m_LocalGamerIndex = localGamerIndex;
		m_PartyPtr = xblParty;
		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);
		m_XblStatus.Cancel();
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		try
		{
			switch(m_State)
			{
			case STATE_GET_PARTY_VIEW:
				if(WasCanceled())
				{
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if(GetPartyView())
				{
					netTaskDebug("Getting party view");
					m_State = STATE_GETTING_PARTY_VIEW;
				}
				else
				{
					// Try to join the party once
					if (m_ViewStatus.GetResultCode() == static_cast<int>(PartyErrorStatus::EmptyParty))
					{
						// We're not in a party
						netTaskDebug("Not in a party");
						RemoveStalePartyMembers();
						m_PartyView = nullptr;
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				break;

			case STATE_GETTING_PARTY_VIEW:
				m_XblStatus.Update();
				if(!m_ViewStatus.Pending())
				{
					if(m_ViewStatus.Succeeded())
					{
						m_PartyView = m_XblStatus.GetResults<PartyView^>();
						m_State = STATE_GET_PARTY_INFO;
					}
					else if (m_ViewStatus.GetResultCode() == static_cast<int>(PartyErrorStatus::EmptyParty))
					{
						// We're not in a party
						netTaskDebug("Not in a party");
						RemoveStalePartyMembers();
						m_PartyView = nullptr;
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				break;
			case STATE_GET_PARTY_INFO:
				if(WasCanceled())
				{
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if(GetPartyInfo())
				{
					netTaskDebug("Retrieving party info");
					m_State = STATE_GETTING_PARTY_INFO;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
				break;

			case STATE_GETTING_PARTY_INFO:
				if(!m_InfoStatus.Pending())
				{
					if(m_InfoStatus.Succeeded())
					{
						netTaskDebug("Retrieving party info succeeded");
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						// this failed but don't fail the party task
						netTaskDebug("Retrieving party info failed");
						status = NET_TASKSTATUS_SUCCEEDED;
					}
				}
				break;
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			status = NET_TASKSTATUS_FAILED;
		}

		if(NET_TASKSTATUS_PENDING != status)
		{
			this->Complete(status);
		}

		return status;
	}

private:

	bool GetPartyView()
	{
		bool success = true;

		try
		{
			success = m_XblStatus.BeginOp<PartyView^>("GetPartyViewAsync", Party::GetPartyViewAsync(), DEFAULT_ASYNC_TIMEOUT, this, &m_ViewStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);		
			success = false;
		}

		return success;
	}

	void RemoveStalePartyMembers()
	{
		try
		{
			if (!rlVerifyf(m_PartyPtr != NULL, "Party was invalid!"))
				return;

			if (m_PartyView == nullptr)
				return;

			if (!rlVerifyf(m_PartyView->Members != nullptr, "Party View Members were invalid!"))
				return;

			if (m_PartyView->Members->Size == 0)
				return;

			for (int i = 0; i < m_PartyPtr->GetPartySize(); i++)
			{
				rlXblPartyMemberInfo* pInfo = m_PartyPtr->GetPartyMember(i);
				bool bRemovedPartyMember = true;

				// Iterate through PartyView^ looking for my member
				if (pInfo != NULL && m_PartyView != nullptr)
				{
					try
					{
						for(PartyMember^ user : m_PartyView->Members)
						{
							u64 xuid = 0;
							if(rlVerify(user != nullptr) && 
								rlVerify(user->XboxUserId != nullptr) && 
								rlVerify(swscanf_s(user->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1))
							{
								if (xuid == pInfo->m_GamerHandle.GetXuid())
								{
									bRemovedPartyMember = false;
									break;
								}
							}
						}
					}
					catch (Platform::Exception^ ex)
					{
						bRemovedPartyMember = false;
						TASK_EXCEPTION(ex);
					}
				}

				if (bRemovedPartyMember)
				{
					m_PartyPtr->ClearPartyMember(i, true);
				}			
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
		}
	}

	bool GetPartyInfo()
	{
		try
		{
			// clear out members no longer with us
			RemoveStalePartyMembers();

			if (m_PartyView && m_PartyView->Members->Size > 0)
			{
				bool success = false;
				GetPartyMemberInfoTask* task = NULL;
				rtry
				{
					task = rlGetTaskManager()->CreateTask<GetPartyMemberInfoTask>();
					rverify(task,catchall,);
					rverify(rlTaskBase::Configure(task, m_LocalGamerIndex, m_PartyPtr, m_PartyView, &m_InfoStatus), catchall,);
					rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
					success = true;
				}
				rcatchall
				{
					if(task)
					{
						rlGetTaskManager()->DestroyTask(task);
					}
				}
				return success;
			}
			else
			{
				m_InfoStatus.SetPending();
				m_InfoStatus.SetSucceeded();
				return true;
			}
		}
		catch (Platform::Exception^ ex)
		{
			RL_EXCEPTION(ex);
			return false;
		}
	}

	void Complete(const netTaskStatus status)
	{
		if (status == NET_TASKSTATUS_SUCCEEDED && !WasCanceled())
		{
			m_PartyPtr->SetPartyView(m_PartyView);
		}
	}

	enum State
	{
		STATE_GET_PARTY_VIEW,
		STATE_GETTING_PARTY_VIEW,
		STATE_GET_PARTY_INFO,
		STATE_GETTING_PARTY_INFO,
	};

	State m_State;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
	
	netStatus m_ViewStatus;
	netStatus m_InfoStatus;
	netStatus m_SystemPartyStatus;

	int m_LocalGamerIndex;

	rlXblParty* m_PartyPtr;
	PartyView^ m_PartyView;
	bool* m_AnyOnline;
};

///////////////////////////////////////////////////////////////////////////////
//  FillOpenSlotsTask
///////////////////////////////////////////////////////////////////////////////
class FillOpenSlotsTask : public netTask
{
public:

	NET_TASK_DECL(FillOpenSlotsTask);
	NET_TASK_USE_CHANNEL(rline_xbl_party);

	FillOpenSlotsTask()
		: m_State(STATE_GET_GAME_SESSION)
		, m_LocalGamerIndex(-1)
	{
		sysMemSet(m_AvailablePlayers, 0, sizeof(m_AvailablePlayers));
	}

	bool Configure(const int localGamerIndex, const rlSessionInfo& sessionInfo, int numSlots, rlGamerHandle* availablePlayers, u64* lastInviteTime, int numAvailablePlayers, const rlGamerHandle& excludeGamer)
	{
		m_LocalGamerIndex = localGamerIndex;

		if (!sessionInfo.GetSessionHandle().IsValid())
			return false;

		m_SessionRef = rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(sessionInfo.GetSessionHandle());
		m_RemainingSlots = numSlots;

		if (numAvailablePlayers > 0)
		{
			for (int i = 0; i < numAvailablePlayers; i++)
			{
				m_AvailablePlayers[i].LastInvitedTime = lastInviteTime[i];
				m_AvailablePlayers[i].XboxUserId = availablePlayers[i].GetXuid();

				m_State = STATE_GET_GAME_SESSION;
			}

			m_NumAvailablePlayers = numAvailablePlayers;
		}
		else
		{
			m_State = STATE_GET_AVAILABLE_PLAYERS;
		}

		m_ExcludeGamer = excludeGamer;

		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);
		m_XblStatus.Cancel();
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		try
		{
			switch(m_State)
			{
			case STATE_GET_AVAILABLE_PLAYERS:
				if(WasCanceled())
				{
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if(GetAvailablePlayers())
				{
					m_State = STATE_GETTING_AVAILABLE_PLAYERS;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
				break;
			case STATE_GETTING_AVAILABLE_PLAYERS:
				m_XblStatus.Update();
				if(!m_MyStatus.Pending())
				{
					if(m_MyStatus.Succeeded())
					{
						IVectorView<GamePlayer^>^ availablePlayers = m_XblStatus.GetResults<IVectorView<GamePlayer^>^>();

						if (availablePlayers->Size > 0)
						{
							for (unsigned i = 0; i < availablePlayers->Size; i++)
							{
								Windows::Xbox::Multiplayer::GamePlayer^ player = availablePlayers->GetAt(i);
								m_AvailablePlayers[i].LastInvitedTime = player->LastInvitedTime.UniversalTime;
								swscanf_s(player->XboxUserId->Data(), L"%" LI64FMT L"u", &m_AvailablePlayers[i].XboxUserId);
							}

							m_NumAvailablePlayers = availablePlayers->Size;
							m_State = STATE_GET_GAME_SESSION;
						}
						else
						{
							status = NET_TASKSTATUS_SUCCEEDED;
						}
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				break;
			case STATE_GET_GAME_SESSION:
				if(WasCanceled())
				{
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if(GetGameSession())
				{
					m_State = STATE_GETTING_GAME_SESSION;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
				break;
			case STATE_GETTING_GAME_SESSION:
				m_XblStatus.Update();
				if(!m_MyStatus.Pending())
				{
					if(m_MyStatus.Succeeded())
					{
						m_Session = m_XblStatus.GetResults<MultiplayerSession^>();
						m_State = STATE_RESERVE_MEMBER_SLOTS;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				break;
			case STATE_RESERVE_MEMBER_SLOTS:
				if(WasCanceled())
				{
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if(ReserveMemberSlots())
				{
					m_State = STATE_RESERVING_MEMBER_SLOTS;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
				break;

			case STATE_RESERVING_MEMBER_SLOTS:
				m_XblStatus.Update();
				if(!m_MyStatus.Pending())
				{
					if(m_MyStatus.Succeeded())
					{
						m_State = STATE_PULL_MEMBERS;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				break;
			case STATE_PULL_MEMBERS:
				if(WasCanceled())
				{
					netTaskDebug("Canceled - bailing...");
					status = NET_TASKSTATUS_FAILED;
				}
				else if(PullPartyMembers())
				{
					m_State = STATE_PULLING_MEMBERS;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
				break;

			case STATE_PULLING_MEMBERS:
				if(!m_MyStatus.Pending())
				{
					if(m_MyStatus.Succeeded())
					{
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				break;
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			status = NET_TASKSTATUS_FAILED;
		}

		if(NET_TASKSTATUS_PENDING != status)
		{
			this->Complete(status);
		}

		return status;
	}

private:
	
	bool GetAvailablePlayers()
	{
		bool success = true;

		try
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
			success = m_XblStatus.BeginOp<IVectorView<GamePlayer^>^>("GetAvailableGamePlayersAsync", Party::GetAvailableGamePlayersAsync(actingUser), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	bool GetGameSession()
	{
		bool success = true;

		try
		{
			XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
			if (context == nullptr)
			{
				return false;
			}

			success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", context->MultiplayerService->GetCurrentSessionAsync(m_SessionRef), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	bool ReserveMemberSlots()
	{
		bool success = true;

		try
		{
			int remainingSlots = m_RemainingSlots;

			bool bNewMembersAdded = false;
			for (int i = 0; i < m_NumAvailablePlayers; i++)
			{
				if (m_ExcludeGamer.IsValid() && m_AvailablePlayers[i].XboxUserId == m_ExcludeGamer.GetXuid())
				{
					rlDebug3("Available party member %" I64FMT "u excluded from slot fill.", m_ExcludeGamer);
					continue;
				}

				// Use the timeout based on what you think your title
				if( remainingSlots <= 0 )
				{
					rlDebug3("All remaining slots have been filled; will stop adding more players in related party sessions");
					break;
				}

				if( rlXblCommon::GetDateTimeDiffInSeconds(m_AvailablePlayers[i].LastInvitedTime, rlXblCommon::GetCurrentDateTime()) < LAST_INVITED_TIME_TIMEOUT)
				{
					rlDebug3("Skipping join request as this player has been invited recently");
					continue;
				}

				String^ winRTXuid = rlXblCommon::XuidToWinRTString(m_AvailablePlayers[i].XboxUserId);
				if( rlXblSessions::IsPlayerInSession(winRTXuid, m_Session))
				{
					rlDebug3("Player is already in session; skipping join request");
					continue;
				}

				m_Session->AddMemberReservation( winRTXuid, nullptr, true );
				rlDebug3("Member reserved: %" I64FMT "u",m_AvailablePlayers[i].XboxUserId);

				bNewMembersAdded = true;
				remainingSlots--;
			}

			if (bNewMembersAdded)
			{
				XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
				if (context == nullptr)
				{
					return false;
				}

				m_XblStatus.BeginOp<MultiplayerSession^>("WriteSessionAsync",  
					context->MultiplayerService->WriteSessionAsync(m_Session, MultiplayerSessionWriteMode::UpdateExisting), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
			}
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	bool PullPartyMembers()
	{
		bool success = true;

		try
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
			Windows::Xbox::Multiplayer::MultiplayerSessionReference^ sessionRef = rlXblCommon::ConvertToWindowsXboxServicesMultiplayerSessionReference(m_SessionRef);
			success = m_XblStatus.BeginAction("PullReservedPlayersAsync", Party::PullReservedPlayersAsync(actingUser, sessionRef), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	void Complete(const netTaskStatus status)
	{
		if((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled())
		{

		}
	}

	enum State
	{
		STATE_GET_AVAILABLE_PLAYERS,
		STATE_GETTING_AVAILABLE_PLAYERS,
		STATE_GET_GAME_SESSION,
		STATE_GETTING_GAME_SESSION,
		STATE_RESERVE_MEMBER_SLOTS,
		STATE_RESERVING_MEMBER_SLOTS,
		STATE_PULL_MEMBERS,
		STATE_PULLING_MEMBERS,
	};

	State m_State;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
	netStatus m_MyStatus;

	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ m_SessionRef;
	MultiplayerSession^ m_Session;

	rlXblGamePlayer m_AvailablePlayers[RL_MAX_PARTY_SIZE];

	int m_LocalGamerIndex;
	int m_RemainingSlots;
	int m_NumAvailablePlayers;
	rlGamerHandle m_ExcludeGamer;
};

///////////////////////////////////////////////////////////////////////////////
//  GetAvailablePlayersTask
///////////////////////////////////////////////////////////////////////////////
class GetAvailablePlayersTask : public netTask
{
public:

	NET_TASK_DECL(GetAvailablePlayersTask);
	NET_TASK_USE_CHANNEL(rline_xbl_party);

	GetAvailablePlayersTask()
		: m_State(STATE_GET_AVAILABLE_PLAYERS)
		, m_LocalGamerIndex(-1)
	{
		m_AvailablePlayers = nullptr;
	}

	bool Configure(const int localGamerIndex, const rlXblSessionHandle& sessionHandle)
	{
		m_LocalGamerIndex = localGamerIndex;
		m_SessionHandle = sessionHandle;

		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);
		m_XblStatus.Cancel();
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		netTaskStatus status = NET_TASKSTATUS_PENDING;

		switch(m_State)
		{
		case STATE_GET_AVAILABLE_PLAYERS:
			if(WasCanceled())
			{
				netTaskDebug("Canceled - bailing...");
				status = NET_TASKSTATUS_FAILED;
			}
			else if(GetAvailablePlayers())
			{
				m_State = STATE_GETTING_AVAILABLE_PLAYERS;
			}
			else
			{
				status = NET_TASKSTATUS_FAILED;
			}
			break;
		case STATE_GETTING_AVAILABLE_PLAYERS:
			m_XblStatus.Update();
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					try
					{
						m_AvailablePlayers = m_XblStatus.GetResults<IVectorView<GamePlayer^>^>();
						status = NET_TASKSTATUS_SUCCEEDED;
					}
					catch (Platform::Exception^ ex)
					{
						TASK_EXCEPTION(ex);
						status = NET_TASKSTATUS_FAILED;
					}
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
			}
			break;
		}

		if(NET_TASKSTATUS_PENDING != status)
		{
			this->Complete(status);
		}

		return status;
	}

private:

	bool GetAvailablePlayers()
	{
		bool success = true;

		try
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
			success = m_XblStatus.BeginOp<IVectorView<GamePlayer^>^>("GetPartyDetailsAsync", Party::GetAvailableGamePlayersAsync(actingUser), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			success = false;
		}

		return success;
	}

	void Complete(const netTaskStatus status)
	{
		if((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled())
		{
			if (m_AvailablePlayers != nullptr && m_AvailablePlayers->Size > 0)
			{
				for (unsigned i = 0; i < m_AvailablePlayers->Size; i++)
				{
					Windows::Xbox::Multiplayer::GamePlayer^ player = m_AvailablePlayers->GetAt(i);
					m_GamePlayers[i].LastInvitedTime = player->LastInvitedTime.UniversalTime;
					swscanf_s(player->XboxUserId->Data(), L"%" LI64FMT L"u", &m_GamePlayers[i].XboxUserId);
				}

				rlXblPartyGamePlayersAvailableEvent e(m_SessionHandle, m_AvailablePlayers->Size, m_GamePlayers);
				rlXblInternal::GetInstance()->DispatchEvent(&e);
			}
		}
	}

	enum State
	{
		STATE_GET_AVAILABLE_PLAYERS,
		STATE_GETTING_AVAILABLE_PLAYERS,
	};

	State m_State;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
	netStatus m_MyStatus;

	IVectorView<GamePlayer^>^ m_AvailablePlayers;
	rlXblGamePlayer m_GamePlayers[RL_MAX_PARTY_SIZE];
	rlXblSessionHandle m_SessionHandle;

	int m_LocalGamerIndex;
};

bool GetPartyMemberInfoTask::Configure(const int localGamerIndex, rlXblParty* xblParty, PartyView^ partyView)
{
	bool success = false;
	m_PartyPtr = xblParty;
	m_PartyView = partyView;

	try
	{
		rtry
		{
			m_PartySize = partyView->Members->Size;
			if (m_PartySize > 0)
			{
				u64* xuids = (u64*)Alloca(u64, m_PartySize);
				int i = 0;

				for(PartyMember^ partyMember : partyView->Members)
				{
					u64 xuid = 0;
					if(swscanf_s(partyMember->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1)
					{
						xuids[i] = xuid;
						i++;
					}
				}

				rverify(rlXblHttpProfileSettingsTask::Configure(localGamerIndex, xuids, m_PartySize, rlXblHttpProfileSettingsTask::GAMEDISPLAYNAME), catchall, );
			}

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
		return false;
	}
}

bool GetPartyMemberInfoTask::ProcessUserDisplayName(u64 xuid, const char* displayName, int& resultCode)
{
	try
	{
		bool success = false;

		rtry
		{
			resultCode = 0;
			rverify(displayName && displayName[0] != '\0', catchall, );

			// find the user with this id
			rlXblPartyMemberInfo* pPartyMember = NULL;
			for(int i = 0; i < m_PartyPtr->GetPartySize(); ++i)
			{
				rlXblPartyMemberInfo* tempPartyMember = m_PartyPtr->GetPartyMember(i);
				if(tempPartyMember == NULL)
				{
					continue;
				}

				if(tempPartyMember->m_GamerHandle.GetXuid() == xuid)
				{
					pPartyMember = tempPartyMember;
					break;
				}
			}

			// exists
			if(pPartyMember)
			{
				pPartyMember->m_GamerHandle.ResetXbl(xuid);
				safecpy(pPartyMember->m_GamerName, displayName);
			}
			// new
			else
			{
				rlXblPartyMemberInfo* pMember = (rlXblPartyMemberInfo*)rlXblInternal::GetInstance()->GetAllocator()->Allocate(sizeof(rlXblPartyMemberInfo), 0);

				if(rlTaskVerify(pMember))
				{
					rlTaskDebug("'%" I64FMT "u' (%s) added to Xbox Live Party", xuid, displayName);

					new (pMember) rlXblPartyMemberInfo();
					sysMemSet(pMember, 0, sizeof(rlXblPartyMemberInfo));

					pMember->m_GamerHandle.ResetXbl(xuid);
					safecpy(pMember->m_GamerName, displayName);

					m_PartyPtr->AddPartyMember(pMember);
				}

				pPartyMember = pMember;
			}

			// Set the party join time
			for(PartyMember^ partyMember : m_PartyView->Members)
			{
				u64 pmXuid = 0;
				if(swscanf_s(partyMember->XboxUserId->Data(), L"%" LI64FMT L"u", &pmXuid) == 1)
				{
					if (pmXuid == xuid)
					{
						// keep previous join time to track players
						pPartyMember->m_PreviousPartyJoinTime = pPartyMember->m_PartyJoinTime;
						pPartyMember->m_PartyJoinTime = partyMember->JoinTime.UniversalTime;
					}
				}
			}

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
		return false;
	}
}

void GetPartyMemberInfoTask::Finish(const FinishType finishType, const int resultCode)
{
	try
	{
		// we need to add dummy representations of non-existing players
		if(!FinishSucceeded(finishType))
		{
			for(PartyMember^ partyMember : m_PartyView->Members)
			{
				u64 xuid = 0;
				if(swscanf_s(partyMember->XboxUserId->Data(), L"%" LI64FMT L"u", &xuid) == 1)
				{
					// find user with this id
					rlXblPartyMemberInfo* pPartyMember = NULL;
					for(int i = 0; i < m_PartyPtr->GetPartySize(); ++i)
					{
						rlXblPartyMemberInfo* tempPartyMember = m_PartyPtr->GetPartyMember(i);
						if(tempPartyMember == NULL)
						{
							continue;
						}

						if(tempPartyMember->m_GamerHandle.GetXuid() == xuid)
						{
							pPartyMember = tempPartyMember;
							break;
						}
					}

					// exists - don't do anything, the information we previously got is better or equally invalid
					// doesn't exist, make a dummy copy with empty display name (but valid gamer handle)
					if(!pPartyMember)
					{
						rlXblPartyMemberInfo* pMember = (rlXblPartyMemberInfo*)rlXblInternal::GetInstance()->GetAllocator()->Allocate(sizeof(rlXblPartyMemberInfo), 0);

						if(rlTaskVerify(pMember))
						{
							rlTaskDebug("'%" I64FMT "u' added to Xbox Live Party", xuid);

							new (pMember) rlXblPartyMemberInfo();
							sysMemSet(pMember, 0, sizeof(rlXblPartyMemberInfo));

							pMember->m_GamerHandle.ResetXbl(xuid);
							pMember->m_GamerName[0] = '\0';

							m_PartyPtr->AddPartyMember(pMember);
						}
					}
				}
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}

	// call up
	rlXblHttpProfileSettingsTask::Finish(finishType, resultCode);
}

// Add Local Users To Party Task
AddLocalUsersToPartyTask::AddLocalUsersToPartyTask()
{

}

bool AddLocalUsersToPartyTask::Configure(const int localGamerIndex)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	m_LocalGamerIndex = localGamerIndex;
	return true;
}

bool AddLocalUsersToPartyTask::DoWork()
{
	bool success = false;

	try
	{
		rtry
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
			rcheck(actingUser != nullptr, catchall, netTaskDebug("Invalid user"));
			rcheck(actingUser->IsSignedIn, catchall, netTaskDebug("User is signed out")); // Usually this is still true just after signing out but can't hurt checking

			const sysDurangoUserInfo* info = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(actingUser);
			rcheck(info, catchall, netTaskDebug("User not found in user list"));
			rcheck(info->IsSignedIn(), catchall, netTaskDebug("User info not signed in"));

			++RAGE_LOG_DISABLE;	// Disable the assert for the call to new within shared_ptr::Resetp
			VectorView<User^>^ userView = ref new VectorView<User^>();
			--RAGE_LOG_DISABLE; // Re-enable the assert

			success = m_XblStatus.BeginAction("AddLocalUsersAsync", Party::AddLocalUsersAsync(actingUser, userView), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
		}
		rcatchall
		{

		}
	}
	catch (Platform::Exception^ ex)
	{
		// This can happen during a sign out so for that case we change the assert to an error log
		if (ex->HResult == 0x80004003)
		{
			RL_EXCEPTION_EX_SILENT("AddLocalUsersToPartyTask::DoWork", ex);
		}
		else
		{
			RL_EXCEPTION_EX("AddLocalUsersToPartyTask::DoWork", ex);
		}
		success = false;
	}

	return success;
}


// RemoveLocalUsersFromPartyTask
bool RemoveLocalUsersFromPartyTask::s_bIsLeaving = false;

RemoveLocalUsersFromPartyTask::RemoveLocalUsersFromPartyTask()
{

}

bool RemoveLocalUsersFromPartyTask::Configure(const int localGamerIndex)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	s_bIsLeaving = true; 
	m_LocalGamerIndex = localGamerIndex;
	return true;
}

void RemoveLocalUsersFromPartyTask::OnCleanup()
{
	s_bIsLeaving = false;
}

bool RemoveLocalUsersFromPartyTask::DoWork()
{
	bool success = false;

	// if we're about to process an invite to a new party, the system will handle the leave for us
	// just finish up early
	if(rlXblInternal::GetInstance()->_GetPartyManager()->ShouldRefreshParty() && s_bInviteAccepted)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetSucceeded();
	}
	else
	{
		try
		{
			rtry
			{
				rcheck(RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex), catchall, netTaskDebug("Invalid acting user index: %d", m_LocalGamerIndex));

				User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
				rcheck(actingUser != nullptr, catchall, netTaskDebug("Invalid acting user, local index: %d", m_LocalGamerIndex));

				const u32 numUsersLoggedIn = sysDurangoUserList::GetPlatformInstance().GetNumberOfUsersLoggedInOnConsole();
				rcheck(numUsersLoggedIn > 0, catchall, netTaskError("numUsersLoggedIn = 0"));

				const s32 maxUsers = sysDurangoUserList::GetPlatformInstance().GetMaxUsers();

				Array<User^>^ users = ref new Array<User^>(maxUsers);
				rverifyall(users != nullptr);

				u32 numLocalUsersToRemove = 0;
				for(int i = 0; i < maxUsers; i++)
				{
					const sysDurangoUserInfo* user = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(i);
					if(user && rlXblInternal::GetInstance()->_GetPartyManager()->IsPartyMember(user->GetUserId()))
					{
						netTaskDebug("Removing party user - XUID: %" I64FMT "u", user->GetUserId());
						users->set(numLocalUsersToRemove, user->GetPlatformUser());
						++numLocalUsersToRemove;
					}
				}

				rcheck(numLocalUsersToRemove > 0, catchall, netTaskDebug("no local users to remove"));

				// copy the non-null entries to a new array to pass to the XDK
				Array<User^>^ localUsersToRemove = ref new Array<User^>(numLocalUsersToRemove);
				rverifyall(localUsersToRemove != nullptr);

				for(u32 i = 0; i < numLocalUsersToRemove; i++)
				{
					netTaskAssert(users[i] != nullptr);
					localUsersToRemove->set(i, users[i]);
				}

				++RAGE_LOG_DISABLE;	// Disable the assert for the call to new within shared_ptr::Resetp
				VectorView<User^>^ userView = ref new VectorView<User^>(localUsersToRemove);
				--RAGE_LOG_DISABLE; // Re-enable the assert

				rverifyall(userView != nullptr);

				success = m_XblStatus.BeginAction("RemoveLocalUsersAsync", Party::RemoveLocalUsersAsync(userView), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
			}
			rcatchall
			{

			}
		}
		catch (Platform::Exception^ ex)
		{
			RL_EXCEPTION_EX("RemoveLocalUsersFromPartyTask::DoWork", ex);
			success = false;
		}
	}

	return success;
}

// Invite To Party Task
InviteToPartyTask::InviteToPartyTask()
{
	m_LocalGamerIndex = -1;
	m_Invitees = nullptr;
}

bool InviteToPartyTask::Configure(const int localGamerIndex, const rlGamerHandle* targetUsers, int numUsers)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	bool success = false;

	try
	{
		m_Invitees = ref new Array<String^>(numUsers);

		for (int i = 0; i < numUsers; i++)
		{
			String^ winRtXuid = rlXblCommon::XuidToWinRTString(targetUsers[i].GetXuid());
			m_Invitees->set(i, winRtXuid);
		}

		m_LocalGamerIndex = localGamerIndex;
		success = true; 
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

bool InviteToPartyTask::DoWork()
{
	bool success = false;

	try
	{
		User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
		rlXblStringVectorView^ inviteeView = ref new rlXblStringVectorView(m_Invitees);
		success = m_XblStatus.BeginAction("InviteToPartyAsync", Party::InviteToPartyAsync(actingUser, inviteeView), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);	
		success = false;
	}

	return success;
}

// Register Game Session Task
bool RegisterGameSessionTask::s_bIsRegistering = false;

RegisterGameSessionTask::RegisterGameSessionTask()
{
	m_LocalGamerIndex = -1;
}

bool RegisterGameSessionTask::Configure(const int localGamerIndex, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	try
	{
		if (sessionRef == nullptr)
			return false;

		s_bIsRegistering = true; 
		m_LocalGamerIndex = localGamerIndex;
		m_SessionRef = sessionRef;

		// log details
		netTaskDebug("Session - ServiceConfigurationId: %ls", sessionRef->ServiceConfigurationId->Data());
		netTaskDebug("Session - SessionName: %ls", sessionRef->SessionName->Data());
		netTaskDebug("Session - SessionTemplateName: %ls", sessionRef->SessionTemplateName->Data());
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}

	return true;
}

void RegisterGameSessionTask::OnCleanup()
{
	s_bIsRegistering = false; 
}

bool RegisterGameSessionTask::DoWork()
{
	bool success = false;

	try
	{
		rtry
		{
			if(rlXbl::IsInviteActionPending())
			{
				netTaskDebug("DoWork :: Invite Action Pending. Exiting to prevent stomp");
				m_MyStatus.SetPending();
				m_MyStatus.SetSucceeded();
			}
			else
			{
				User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
				rcheck(actingUser != nullptr, catchall, netTaskDebug("Invalid user: %d", m_LocalGamerIndex));

				Windows::Xbox::Multiplayer::MultiplayerSessionReference^ sessionRef = rlXblCommon::ConvertToWindowsXboxServicesMultiplayerSessionReference(m_SessionRef);

				success = m_XblStatus.BeginAction("RegisterGameSessionAsync", Party::RegisterGameSessionAsync(actingUser, sessionRef), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
			}
		}
		rcatchall
		{

		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

// Disassociate Game Session Task
DisassociateGameSessionTask::DisassociateGameSessionTask()
{
	m_LocalGamerIndex = -1;
}

bool DisassociateGameSessionTask::Configure(const int localGamerIndex,  Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	try
	{
		if (sessionRef == nullptr)
			return false;

		m_LocalGamerIndex = localGamerIndex;
		m_SessionRef = sessionRef;

		// log details
		netTaskDebug("Session - ServiceConfigurationId: %ls", sessionRef->ServiceConfigurationId->Data());
		netTaskDebug("Session - SessionName: %ls", sessionRef->SessionName->Data());
		netTaskDebug("Session - SessionTemplateName: %ls", sessionRef->SessionTemplateName->Data());
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		return false;
	}

	return true;
}

bool DisassociateGameSessionTask::DoWork()
{
	bool success = false;

	try
	{
		rtry
		{
			User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
			rcheck(actingUser != nullptr, catchall, netTaskDebug("Invalid user: %d", m_LocalGamerIndex));

			Windows::Xbox::Multiplayer::MultiplayerSessionReference^ sessionRef = rlXblCommon::ConvertToWindowsXboxServicesMultiplayerSessionReference(m_SessionRef);

			success = m_XblStatus.BeginAction("DisassociateGameSessionAsync", Party::DisassociateGameSessionAsync(actingUser, sessionRef), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
		}
		rcatchall
		{

		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

// Register Matchmaking Session Task
RegisterMatchmakingSessionTask::RegisterMatchmakingSessionTask()
{
	m_LocalGamerIndex = -1;
}

bool RegisterMatchmakingSessionTask::Configure(const int localGamerIndex,  Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	m_LocalGamerIndex = localGamerIndex;
	return true;
}


bool RegisterMatchmakingSessionTask::DoWork()
{
	bool success = false;

	try
	{
		User^ actingUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(m_LocalGamerIndex);
		Windows::Xbox::Multiplayer::MultiplayerSessionReference^ sessionRef = rlXblCommon::ConvertToWindowsXboxServicesMultiplayerSessionReference(m_SessionRef);

		success = m_XblStatus.BeginAction("RegisterMatchSessionAsync", Party::RegisterMatchSessionAsync(actingUser, sessionRef), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

// Show Switch Party Title Task
SwitchPartyTitleTask::SwitchPartyTitleTask()
{
	m_LocalGamerIndex = -1;
}

bool SwitchPartyTitleTask::Configure(const int localGamerIndex)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	m_LocalGamerIndex = localGamerIndex;
	return true;
}

bool SwitchPartyTitleTask::DoWork()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		success = m_XblStatus.BeginAction("SwitchPartyTitleAsync Method", Party::SwitchPartyTitleAsync(context->User), DEFAULT_ASYNC_TIMEOUT, this, &m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

// GetGameSessionReadyTask
GetGameSessionReadyTask::GetGameSessionReadyTask()
{
	m_LocalGamerIndex = -1;
}

bool GetGameSessionReadyTask::Configure(const int localGamerIndex, const rlXblSessionHandle& sessionHandle, rlXblParty* xblParty, GameSessionReadyOrigin origin)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	m_LocalGamerIndex = localGamerIndex;
	m_SessionHandle = sessionHandle;
	m_PartyPtr = xblParty;
	m_Origin = origin;

	return true;
}

void GetGameSessionReadyTask::ProcessSuccess()
{
	try
	{
		MultiplayerSession^ session = m_XblStatus.GetResults<MultiplayerSession^>(); 
		if (session == nullptr)
		{
			netTaskDebug("Not processing. Session is NULL");
			return;
		}

		if(!m_PartyPtr->IsInParty())
		{
			netTaskDebug("Not processing. No longer in party");
			return;
		}

#if !__NO_OUTPUT
		rlXblSessions::LogSessionDetail(session, "GetGameSessionReadyTask::ProcessSuccess", true);
#endif

		if(rlXblSessions::ConvertToSessionInfo(session, &m_SessionInfo, &m_NumSessions))
		{
			rlGamerHandle partyPlayers[RL_MAX_PARTY_SIZE];
			int numPartyPlayers = 0;

			// pass in the members of our party who are in this session
			int partySize = m_PartyPtr->GetPartySize();
			rlAssert(partySize <= RL_MAX_PARTY_SIZE);
			for (int i = 0; i < partySize && i < RL_MAX_PARTY_SIZE; i++)
			{
				for each (MultiplayerSessionMember^ member in session->Members)
				{
					String^ winRtXuid = rlXblCommon::XuidToWinRTString(m_PartyPtr->GetPartyMember(i)->m_GamerHandle.GetXuid());
					if(rlXblCommon::IsStringEqualI(winRtXuid, member->XboxUserId))
					{
						netTaskDebug("'%s' in session and Xbox Live Party", m_PartyPtr->GetPartyMember(i)->m_GamerName);
						partyPlayers[numPartyPlayers].ResetXbl(m_PartyPtr->GetPartyMember(i)->m_GamerHandle.GetXuid());
						numPartyPlayers++;
					}
				}
			}

			// if this is sourced from an invite, work out who it's for
			rlGamerInfo gamerInfo;
			GetTarget(&gamerInfo);

			rlXblPartyGameSessionReadyEvent e(m_SessionInfo, gamerInfo, numPartyPlayers, partyPlayers, m_Origin);
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else
		{
			netTaskDebug("Not processing. Failed to convert session info!");

			// leave party (we won't be able to invite or be joined with an invalid session)
			if(s_bLeavePartyOnInvalidGameSession)
				m_PartyPtr->RemoveLocalUsersFromParty(rlPresence::GetActingUserIndex(), NULL);
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}

	if(s_bInviteNotifyPending)
	{
		netTaskDebug("Clearing invite notify flag");
		s_bInviteNotifyPending = false;
	}
}

void GetGameSessionReadyTask::ProcessFailure()
{
	if(m_MyStatus.GetResultCode() == HTTP_E_STATUS_FORBIDDEN)
	{
		rlGamerInfo gamerInfo;
		GetTarget(&gamerInfo);

		if(!rlGamerInfo::HasMultiplayerPrivileges(gamerInfo.GetLocalIndex()))
		{
			netTaskDebug("Cannot process due to invalid privileges for %s", gamerInfo.GetName());
			rlXblPartyGameSessionUnavailableEvent e(gamerInfo);
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
	}

	if(s_bInviteNotifyPending)
	{
		netTaskDebug("Clearing invite notify flag");
		s_bInviteNotifyPending = false;
	}
}

bool GetGameSessionReadyTask::DoWork()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef =
			rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(&m_SessionHandle);

		success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
			context->MultiplayerService->GetCurrentSessionAsync(sessionRef),
			DEFAULT_ASYNC_TIMEOUT, 
			this,
			&m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

void GetGameSessionReadyTask::GetTarget(rlGamerInfo* pGamerInfo)
{
	if(!pGamerInfo)
		return;

	// clear first
	pGamerInfo->Clear();

	if(m_Origin == ORIGIN_FROM_INVITE) for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		rlGamerHandle hGamer;
		if(rlPresence::IsSignedIn(i) && rlPresence::GetGamerHandle(i, &hGamer) && hGamer.GetXuid() == s_InvitedXUID)
		{
			rlPresence::GetGamerInfo(i, pGamerInfo);
			netTaskDebug("From Invite - %s invited. XUID: %" I64FMT "u, Local Index: %d", pGamerInfo->GetName(), s_InvitedXUID, pGamerInfo->GetLocalIndex());
			break;
		}

#if !__NO_OUTPUT
		if(!pGamerInfo->IsValid())
		{
			netTaskDebug("From Invite - Cannot find local XUID: %" I64FMT "u", s_InvitedXUID);
		}
#endif
	}

	// if a specific user has not been selected, use the active user
	if(!pGamerInfo->IsValid() && rlPresence::GetActingUserIndex() >= 0)
	{
		rlPresence::GetGamerInfo(rlPresence::GetActingUserIndex(), pGamerInfo);
		netTaskDebug("Using gamer: %s, index: %d", pGamerInfo->GetName(), pGamerInfo->GetLocalIndex());
	}
}

// ValidateGameSessionTask
ValidateGameSessionTask::ValidateGameSessionTask()
{
	m_LocalGamerIndex = -1;
}

bool ValidateGameSessionTask::Configure(const int localGamerIndex, rlXblSessionHandle sessionHandle, rlXblParty* xblParty)
{
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	m_LocalGamerIndex = localGamerIndex;
	m_SessionHandle = sessionHandle;
	m_PartyPtr = xblParty;

	return true;
}

void ValidateGameSessionTask::ProcessSuccess()
{
	int numPartyPlayers = 0;

	try
	{
		if(!m_PartyPtr->IsInParty())
		{
			netTaskDebug("Not processing. No longer in party");
			return;
		}
		
		MultiplayerSession^ session = m_XblStatus.GetResults<MultiplayerSession^>(); 
		if (session)
		{
	#if !__NO_OUTPUT
			rlXblSessions::LogSessionDetail(session, "ValidateGameSessionTask::ProcessSuccess", true);
	#endif

			if(rlXblSessions::ConvertToSessionInfo(session, &m_SessionInfo, &m_NumSessions))
			{
				rlGamerHandle partyPlayers[RL_MAX_PARTY_SIZE];
			
				// pass in the members of our party who are in this session
				int partySize = m_PartyPtr->GetPartySize();
				rlAssert(partySize <= RL_MAX_PARTY_SIZE);
				for (int i = 0; i < partySize && i < RL_MAX_PARTY_SIZE; i++)
				{
					for each (MultiplayerSessionMember^ member in session->Members)
					{
						String^ winRtXuid = rlXblCommon::XuidToWinRTString(m_PartyPtr->GetPartyMember(i)->m_GamerHandle.GetXuid());
						if(rlXblCommon::IsStringEqualI(winRtXuid, member->XboxUserId))
						{
							netTaskDebug("'%s' in session and Xbox Live Party", m_PartyPtr->GetPartyMember(i)->m_GamerName);
							partyPlayers[numPartyPlayers].ResetXbl(m_PartyPtr->GetPartyMember(i)->m_GamerHandle.GetXuid());
							numPartyPlayers++;
						}
					}
				}
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}

	if(numPartyPlayers == 0)
	{
		rlXblPartyGameSessionInvalidEvent e;
		rlXblInternal::GetInstance()->DispatchEvent(&e);
	}
}

void ValidateGameSessionTask::ProcessFailure()
{
	rlXblPartyGameSessionInvalidEvent e;
	rlXblInternal::GetInstance()->DispatchEvent(&e);
}

bool ValidateGameSessionTask::DoWork()
{
	bool success = false;

	try
	{
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef =
			rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(&m_SessionHandle);

		success = m_XblStatus.BeginOp<MultiplayerSession^>("GetCurrentSessionAsync", 
														   context->MultiplayerService->GetCurrentSessionAsync(sessionRef),
														   DEFAULT_ASYNC_TIMEOUT, 
														   this,
														   &m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}
#endif

#if RSG_GDK
#pragma warning(push)
#pragma warning(disable: 4100)

void rlXblParty::FlagRefreshParty() {}
bool rlXblParty::AddLocalUsersToParty(const int localGamerIndex, netStatus* status) { return false; }
bool rlXblParty::RemoveLocalUsersFromParty(const int localGamerIndex, netStatus* status) { return false; }
bool rlXblParty::InviteToParty(const int localGamerIndex, const rlGamerHandle* targetUsers, int numUsers, netStatus* status) { return false; }
int rlXblParty::GetPartySize() { return 0; }
bool rlXblParty::IsInParty() { return false; }
bool rlXblParty::IsInSoloParty() { return false; }
bool rlXblParty::IsPartyChatActive() const { return false; }
bool rlXblParty::IsPartyChatSuppressed() const { return false; }
void rlXblParty::SuppressGameSessionReadyNotification(bool bSuppress) {}
bool rlXblParty::RegisterGameSessionToParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status) { return false; }
bool rlXblParty::DisassociateGameSessionFromParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status) { return false; }
bool rlXblParty::IsRegisteringGameSession() { return false; }
bool rlXblParty::HasRegisteredGameSession() { return false; }
rlXblSessionHandle* rlXblParty::GetRegisteredGameSession(rlXblSessionHandle* sessionHandle) { return NULL; }
bool rlXblParty::IsRegisteredGameSession(const rlXblSessionHandle* sessionHandle) { return false; }
bool rlXblParty::ValidateGameSession(const int localGamerIndex) { return false; }
bool rlXblParty::SwitchPartyTitle(const int localGamerIndex, netStatus* status) { return false; }
rlXblPartyMemberInfo* rlXblParty::GetPartyMember(int index) { return NULL; }
bool rlXblParty::FillOpenSessionSlots(const int localGamerIndex, const rlSessionInfo& sessionInfo, int numSlots, rlGamerHandle* availablePlayers, u64* lastInviteTimes, int numAvailablePlayers, const rlGamerHandle& excludeGamer) { return false; }
void rlXblParty::SetLeavePartyOnInvalidGameSession(const bool bLeavePartyOnInvalidGameSession) {}
bool rlXblParty::Init() { return true; }
void rlXblParty::Shutdown() {};
void rlXblParty::Update() {};
void rlXblParty::ClearParty(const bool bFireRosterChangedEvent) { (void)bFireRosterChangedEvent; }
bool rlXblParty::IsRefreshingParty() { return false; }
bool rlXblParty::RegisterGameSession(const int localGamerIndex, XBLMultiplayerSessionReferenceType sessionReference, netStatus* status) { return false; }

#pragma warning(pop)
#else

rlXblParty::rlXblParty()
	: m_Initialized(false)
	, m_RefreshParty(0)
	, m_bGetGameSession(false)
	, m_GameSessionReadyOrigin(ORIGIN_UNKNOWN)
	, m_bFillSlots(false)
	, m_bIsGamePlayerEventRegistered(false)
	, m_bActingUserInParty(false)
	, m_bFireRosterChangedEvent(false)
	, m_bIsPartyChatActive(false)
	, m_bIsPartyChatSuppressed(false)
	OUTPUT_ONLY(, m_LogPartyMembers(false))
{
	m_RefreshPartyStatus.Reset();
	m_GetGameSessionStatus.Reset();
	m_PartyView = nullptr;
}

rlXblParty::~rlXblParty()
{

}

bool 
rlXblParty::Init()
{
	m_RefreshParty = 1;
	m_bGetGameSession = false;
	m_GameSessionReadyOrigin = ORIGIN_UNKNOWN;
	RegisterEventHandlers();
	m_Initialized = true;

	return m_Initialized;
}

void 
rlXblParty::Shutdown()
{
	m_PartyMembers.Reset(true);
}

void rlXblParty::Update()
{
	// Refresh PartyView Logic
	if (ShouldRefreshParty() && CanRefreshParty())
	{
		RefreshParty();
	}

	// Get Game Session Info from GameSessionReady event
	if (ShouldGetPartyGameSession() && CanGetPartyGameSession())
	{
		GetGameSession(m_GameSessionHandle);
	}

	// Auto-fill party slots
	if (ShouldFillPartySlots() && CanFillPartySlots())
	{
		GetAvailablePlayers();
	}

	// Check if a party roster event needs generated (only once we've fully refreshed the party)
	if (m_bFireRosterChangedEvent && !m_RefreshPartyStatus.Pending())
	{
		rlDebug("Firing roster changed event");
		rlXblPartyEventsRosterChanged e;
		rlXblInternal::GetInstance()->DispatchEvent(&e);
		m_bFireRosterChangedEvent = false;
	}
	
	ProcessEvents();
}

void rlXblParty::ProcessEvents()
{
	SYS_CS_SYNC(m_Cs);
	PartyEvent pe;

	// wait for session and party refreshing to complete before processing events
	while(m_EventQueue.NextEvent(&pe))
	{
#if !__NO_OUTPUT
		rlDebug("Handle Party Event: (%s)", PartyEvent::GetStateName(pe));
#endif

		if (pe.m_EventId == PartyEvent::PARTY_EVENT_SESSION_READY)
		{
			rlDebug("\tPARTY_EVENT_SESSION_READY: %d", pe.m_Origin);

			m_GameSessionHandle = pe.m_SessionHandle;
			m_GameSessionReadyOrigin = pe.m_Origin;
			m_bGetGameSession = true;
		}
		else if (pe.m_EventId == PartyEvent::PARTY_EVENT_MATCH_EXPIRED)
		{
			rlXblPartyMatchStatusExpiredEvent e;
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else if (pe.m_EventId == PartyEvent::PARTY_EVENT_MATCH_FOUND)
		{
			rlXblPartyMatchStatusFoundEvent e;
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else if (pe.m_EventId == PartyEvent::PARTY_EVENT_MATCH_CANCELLED)
		{
			rlXblPartyMatchStatusCancelledEvent e;
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else if (pe.m_EventId == PartyEvent::PARTY_EVENT_AVAILABLE_PLAYERS_CHANGED)
		{
			rlXblPartyGamePlayersAvailableEvent e(pe.m_SessionHandle, pe.m_NumAvailablePlayers, pe.m_AvailablePlayers);
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
	}
}

bool rlXblParty::CanRefreshParty()
{
	if (!m_Initialized)
	{
		return false;
	}

	if(!netHardware::IsAvailable())
	{
		return false;
	}

	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(rlPresence::GetActingUserIndex()))
	{
		return false;
	}

	if(m_RefreshPartyStatus.Pending() || m_GetGameSessionStatus.Pending())
	{
		return false;
	}

	// wait until we've processed any leave tasks
	if(RemoveLocalUsersFromPartyTask::IsLeaving())
	{
		return false;
	}

	if (!rlPresence::IsOnline(rlPresence::GetActingUserIndex()))
	{
		return false;
	}

	if (sysDurangoUserList::GetPlatformInstance().IsWaitingForLiveUserListUpdate())
	{
		return false;
	}

	return true;
}

bool rlXblParty::ShouldRefreshParty()
{
	return m_RefreshParty != 0;
}

bool rlXblParty::IsRefreshingParty()
{
	return ((m_RefreshParty != 0) || m_RefreshPartyStatus.Pending());
}

bool rlXblParty::CanFillPartySlots()
{
	if (!m_Initialized)
	{
		return false;
	}

	if(!netHardware::IsAvailable())
	{
		return false;
	}

	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(rlPresence::GetActingUserIndex()))
	{
		return false;
	}

	if(m_FillSlotsStatus.Pending())
	{
		return false;
	}

	if (!g_rlXbl.GetPresenceManager()->IsOnline())
	{
		return false;
	}

	if (!m_GameSessionHandle.IsValid())
	{
		return false;
	}

	return true;
}

bool rlXblParty::ShouldFillPartySlots()
{
	return m_bFillSlots;
}

void rlXblParty::GetAvailablePlayers()
{
	int localGamerIndex = rlPresence::GetActingUserIndex();

	GetAvailablePlayersTask* task;

	if(!netTask::Create(&task, &m_FillSlotsStatus)
		|| !task->Configure(localGamerIndex, m_GameSessionHandle)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}

	m_bFillSlots = false;
}

bool rlXblParty::CanGetPartyGameSession()
{
	if(!m_Initialized)
	{
		return false;
	}

	if(!netHardware::IsAvailable())
	{
		return false;
	}

	if(m_GetGameSessionStatus.Pending())
	{
		return false;
	}

	if(!g_rlXbl.GetPresenceManager()->IsOnline())
	{
		return false;
	}

	if(m_PartyView == nullptr)
	{
		// wait until we have our initial party view first
		return false;
	}

	return true;
}

bool rlXblParty::ShouldGetPartyGameSession()
{
	return m_bGetGameSession;
}

void rlXblParty::RefreshParty()
{
	int localGamerIndex = rlPresence::GetActingUserIndex();
	rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));

	RefreshPartyTask* task;
	if(!netTask::Create(&task, &m_RefreshPartyStatus)
		|| !task->Configure(localGamerIndex, this)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}

	m_RefreshParty = 0;
}

void rlXblParty::GetGameSession(const rlXblSessionHandle& session)
{
	int localGamerIndex = rlPresence::GetActingUserIndex();
	rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));

	GetGameSessionReadyTask* task;
	if(!netTask::Create(&task, &m_GetGameSessionStatus)
		|| !task->Configure(localGamerIndex, session, this, m_GameSessionReadyOrigin)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}

	m_bGetGameSession = false;
}

void rlXblParty::ClearParty(const bool bFireRosterChangedEvent)
{
    rlDebug("ClearParty");

    unsigned memberCount = (unsigned)m_PartyMembers.GetCount();
    for(unsigned i = 0; i < memberCount; ++i)
    {
        ClearPartyMember(i, bFireRosterChangedEvent);
        --i;
        --memberCount;
    }

    m_PartyView = nullptr;
    m_PartyMembers.Reset();
    OUTPUT_ONLY(m_LogPartyMembers = false;)
}

void rlXblParty::RegisterEventHandlers()
{
	try
	{
		// Listen to Party Roster Changed
		EventHandler<PartyRosterChangedEventArgs^>^ partyRosterChangedEvent = ref new EventHandler<PartyRosterChangedEventArgs^>(
			[this] (Platform::Object^, PartyRosterChangedEventArgs^ eventArgs)
		{
			OnPartyRosterChanged( eventArgs );
		});
		m_partyRosterChangedToken = Party::PartyRosterChanged += partyRosterChangedEvent;

		// Listen to Party::MatchStatusChanged
		EventHandler<MatchStatusChangedEventArgs^>^ partyMatchStatusChanged = ref new EventHandler<MatchStatusChangedEventArgs^>(
			[this] (Platform::Object^, MatchStatusChangedEventArgs^ eventArgs)
		{
			OnPartyMatchStatusChanged( eventArgs );
		});
		m_partyMatchStatusChangedToken = Party::MatchStatusChanged += partyMatchStatusChanged;

		// Listen to Party State Changed
		EventHandler<PartyStateChangedEventArgs^>^ partyStateChangedEvent = ref new EventHandler<PartyStateChangedEventArgs^>(
			[this] (Platform::Object^, PartyStateChangedEventArgs^ eventArgs)
		{
			OnPartyStateChanged( eventArgs );
		});
		m_partyStateChangedToken = Party::PartyStateChanged += partyStateChangedEvent;

		// Listen to Game Session Ready
		EventHandler<GameSessionReadyEventArgs^>^ gameSessionReadyEvent = ref new EventHandler<GameSessionReadyEventArgs^>(
			[this] (Platform::Object^, GameSessionReadyEventArgs^ eventArgs)
		{
			OnGameSessionReady(eventArgs);
		});
		m_partyGameSessionReadyToken = Party::GameSessionReady += gameSessionReadyEvent;

		// Listen to IsPartyChatActiveChanged to track changes
		m_bIsPartyChatActive = PartyChat::IsPartyChatActive;
		EventHandler<bool>^ partyChatActiveChanged = ref new EventHandler<bool>(
			[this] (Platform::Object^, bool bActive)
		{
			rlDebug("IsPartyChatActiveChanged :: State: %s", bActive ? "Active" : "Inactive");
			m_bIsPartyChatActive = bActive;
		});
		m_partyChatActiveChangedToken = PartyChat::IsPartyChatActiveChanged += partyChatActiveChanged;

		// Listen to IsPartyChatSuppressedChanged to track changes
		m_bIsPartyChatSuppressed = PartyChat::IsPartyChatSuppressed;
		EventHandler<bool>^ partyChatSuppressedChanged = ref new EventHandler<bool>(
			[this] (Platform::Object^, bool bActive)
		{
			rlDebug("IsPartyChatSuppressedChanged :: State: %s", bActive ? "Active" : "Inactive");
			m_bIsPartyChatSuppressed = bActive;
		});
		m_partyChatSuppressedChangedToken = PartyChat::IsPartyChatSuppressedChanged += partyChatSuppressedChanged;

		// log initial state
		rlDebug("RegisterEventHandlers :: PartyChat states - Active: %s, Suppressed: %s", m_bIsPartyChatActive ? "True" : "False", m_bIsPartyChatSuppressed ? "True" : "False");
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

// This event will fire when :
// - A new Match Session registered to party, or 
// - A new Game Session is registered to party (via RegisterGame call, or as a result of matchmaking), and 
// - Party Title ID changes (which will trigger change in IsPartyInAnotherTItle bool flag).
void rlXblParty::OnPartyStateChanged( PartyStateChangedEventArgs^ eventArgs )
{
	rlDebug("OnPartyStateChanged");
	sysInterlockedExchange((unsigned int*)&m_RefreshParty, 1);
}

// Triggered when member joins or leaves the user's party. 
void rlXblParty::OnPartyRosterChanged( PartyRosterChangedEventArgs^ eventArgs )
{
	rlDebug("OnPartyRosterChanged");
	sysInterlockedExchange((unsigned int*)&m_RefreshParty, 1);
	OUTPUT_ONLY(m_LogPartyMembers = true;)
}

// Triggered when the list of available players has changed. 
void rlXblParty::OnGamePlayersChanged( GamePlayersChangedEventArgs^ eventArgs )
{
	try
	{
		rlDebug("OnGamePlayersChanged");
		sysInterlockedExchange((unsigned int*)&m_RefreshParty, 1);

		Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef =
			rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(eventArgs->GameSession);

		if(sessionRef == nullptr)
		{
			rlDebug("OnGamePlayersChanged: invalid sessionRef.");
			return;
		}

		PartyEvent pe(PartyEvent::PARTY_EVENT_AVAILABLE_PLAYERS_CHANGED, sessionRef, eventArgs->AvailablePlayers);
		QueueEvent(pe);
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

// Triggered when the status of a match session changes to Found, Canceled, or Expired. 
void rlXblParty::OnPartyMatchStatusChanged( MatchStatusChangedEventArgs^ eventArgs )
{
	try
	{
		Windows::Xbox::Multiplayer::MatchStatus partyMatchStatus = eventArgs->MatchStatus;

		rlDebug("OnPartyMatchStatusChanged: Match status changed: %s", partyMatchStatus.ToString()->Data());
		switch (partyMatchStatus)
		{
		case Windows::Xbox::Multiplayer::MatchStatus::Canceled:
			{
				PartyEvent e(PartyEvent::PARTY_EVENT_MATCH_CANCELLED);
				QueueEvent(e);
			}
			break;
		case Windows::Xbox::Multiplayer::MatchStatus::Found:
			{
				PartyEvent e(PartyEvent::PARTY_EVENT_MATCH_FOUND);
				QueueEvent(e);
			}
			break;
		case Windows::Xbox::Multiplayer::MatchStatus::Expired:
			{
				PartyEvent e(PartyEvent::PARTY_EVENT_MATCH_EXPIRED);
				QueueEvent(e);
			}
			break;

		default:
			break;
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

// Triggered when a game session is ready to play. Once a GameSessionReady event is received, 
// the title should establish peer to peer connections to everyone in the game session. 
// The Secure Device Address of each player is stored in the Game Session.
void rlXblParty::OnGameSessionReady( GameSessionReadyEventArgs^ eventArgs )
{
	rlDebug("rlXblParty::OnGameSessionReady event triggered.\n");
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef =
		rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(eventArgs->GameSession);

	PartyEvent e(PartyEvent::PARTY_EVENT_SESSION_READY, sessionRef, ORIGIN_FROM_MULTIPLAYER_JOIN);
	QueueEvent(e);
}

void rlXblParty::RegisterGamePlayersChangedEventHandler()
{
	// Listen to Party::GamePlayersChanged
	// Only the HOST should register for this event to detect if any party members were added that should be pulled into the game.
	EventHandler<GamePlayersChangedEventArgs^>^ partyGamePlayersChanged = ref new EventHandler<GamePlayersChangedEventArgs^>(
		[this] (Platform::Object^, GamePlayersChangedEventArgs^ eventArgs)
	{
		OnGamePlayersChanged( eventArgs );
	});

	m_partyGamePlayersChangedToken = Party::GamePlayersChanged += partyGamePlayersChanged;
	m_bIsGamePlayerEventRegistered = true;

	rlDebug("GamePlayersChanged event handler registered");
}

void rlXblParty::UnregisterGamePlayersEventHandler()
{
	if(m_bIsGamePlayerEventRegistered)
	{
		Party::GamePlayersChanged -= m_partyGamePlayersChangedToken;
	}

	m_bIsGamePlayerEventRegistered = false;
	rlDebug("GamePlayersChanged event handler unregistered");
}

bool 
rlXblParty::GetPartyMembers(rlXblPartyMemberInfo* results,
							const unsigned maxResults,
							unsigned* numResults)
{
	bool success = false;

	rtry
	{
		rverify(numResults, catchall, );

		*numResults = m_PartyMembers.GetCount();
		rverify(maxResults >= *numResults, catchall, );
		rverify(results, catchall, );

		for(unsigned i = 0; i < (unsigned)m_PartyMembers.GetCount(); ++i)
		{
			rlXblPartyMemberInfo* partyMember = m_PartyMembers[i];
			if(rlVerify(partyMember))
			{
				results[i] = *partyMember;
			}
		}
	}
	rcatchall
	{

	}

	return success;
}

void rlXblParty::FlagRefreshParty()
{
	rlDebug("FlagRefreshParty");
	sysInterlockedExchange((unsigned int*)&m_RefreshParty, 1);
}

bool rlXblParty::AddLocalUsersToParty(const int localGamerIndex, netStatus* status)
{
	CREATE_NET_XBLTASK(AddLocalUsersToPartyTask, status, localGamerIndex);
}

bool rlXblParty::RemoveLocalUsersFromParty(const int localGamerIndex, netStatus* status)
{
	if(RemoveLocalUsersFromPartyTask::IsLeaving())
	{
		rlDebug("RemoveLocalUsersFromParty :: Already leaving party.");
		return false;
	}

	CREATE_NET_XBLTASK(RemoveLocalUsersFromPartyTask, status, localGamerIndex);
}

bool rlXblParty::InviteToParty(const int localGamerIndex, const rlGamerHandle* targetUsers, int numUsers, netStatus* status)
{
	CREATE_NET_XBLTASK(InviteToPartyTask, status, localGamerIndex, targetUsers, numUsers);
}

bool rlXblParty::RegisterGameSessionToParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status)
{
	if(!sessionHandle)
	{
		rlError("RegisterGameSessionToParty :: Invalid game session");
		return false;
	}

	if(rlXbl::IsInviteActionPending())
	{
		rlError("RegisterGameSessionToParty :: Invite Action Pending. Exiting to prevent stomp");
		return false;
	}

	// convert to session reference
	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef = rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(*sessionHandle);

	// call internal task prop function
	return RegisterGameSession(localGamerIndex, sessionRef, status);
}

bool rlXblParty::RegisterGameSession(const int localGamerIndex, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef, netStatus* status)
{
	if(IsRegisteringGameSession())
	{
		rlDebug("RegisterGameSession :: Already registering a session.");
		return false;
	}

	CREATE_NET_XBLTASK(RegisterGameSessionTask, status, localGamerIndex, sessionRef);
}

bool rlXblParty::DisassociateGameSessionFromParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status)
{
	try
	{
		// session reference
		Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef = nullptr;

		if(!sessionHandle)
		{
			// use the current game session
			if(m_PartyView != nullptr && m_PartyView->GameSession != nullptr)
			{
				sessionRef = rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(m_PartyView->GameSession);
			}
			else
			{
				rlDebug("DisassociateGameSessionFromParty :: No session associated with party.");
				return false;
			}
		}
		else
		{
			sessionRef = rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(*sessionHandle);
		}

		// call internal task prop function
		return DisassociateGameSession(localGamerIndex, sessionRef, status);
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
		return false;
	}
}

bool rlXblParty::HasRegisteredGameSession()
{
	try
	{
		return (m_PartyView != nullptr) && (m_PartyView->GameSession != nullptr);
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
		return false;
	}
}

rlXblSessionHandle* rlXblParty::GetRegisteredGameSession(rlXblSessionHandle* sessionHandle)
{
	if(!sessionHandle)
		return NULL;

	sessionHandle->Clear();

	try 
	{
		if(m_PartyView != nullptr && m_PartyView->GameSession != nullptr)
		{
			rlVerify(rlXblCommon::ImportXblHandle(m_PartyView->GameSession, sessionHandle));
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}

	return sessionHandle;
}

bool rlXblParty::IsRegisteredGameSession(const rlXblSessionHandle* sessionHandle)
{
	if(!sessionHandle)
		return false;

	try 
	{
		if(m_PartyView != nullptr && m_PartyView->GameSession != nullptr)
		{
			rlXblSessionHandle partyHandle;
			if(rlVerify(rlXblCommon::ImportXblHandle(m_PartyView->GameSession, &partyHandle)))
				return (partyHandle == *sessionHandle);
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}

	return false;
}


bool rlXblParty::ValidateGameSession(const int localGamerIndex)
{
	// treat no party as validated
	if(m_PartyView == nullptr)
		return true;

	// treat no game session as validated
	if(m_PartyView->GameSession == nullptr)
		return true; 

	Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef = rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(m_PartyView->GameSession);

	rlXblSessionHandle sessionHandle;
	sessionHandle.Init(sessionRef->ServiceConfigurationId->Data(), 
					   sessionRef->SessionName->Data(), 
					   sessionRef->SessionTemplateName->Data());

	CREATE_NET_XBLTASK(ValidateGameSessionTask, NULL, localGamerIndex, sessionHandle, this);
}

bool rlXblParty::DisassociateGameSession(const int localGamerIndex, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef, netStatus* status)
{
	CREATE_NET_XBLTASK(DisassociateGameSessionTask, status, localGamerIndex, sessionRef);
}

bool rlXblParty::IsRegisteringGameSession()
{
	return RegisterGameSessionTask::IsRegistering();
}

bool rlXblParty::RegisterMatchmakingSession(const int localGamerIndex, Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef, netStatus* status)
{
	CREATE_NET_XBLTASK(RegisterMatchmakingSessionTask, status, localGamerIndex, sessionRef);
}

bool rlXblParty::SwitchPartyTitle(const int localGamerIndex, netStatus* status)
{
	if (!IsPartyInAnotherTitle())
	{
		rlDebug("SwitchPartyTitle failed due to party already being in this title. Recheck the calling logic?");
		return false;
	}

	CREATE_NET_XBLTASK(SwitchPartyTitleTask, status, localGamerIndex);
}

void rlXblParty::SuppressGameSessionReadyNotification(bool bSuppress)
{
	try
	{
		if(PartyConfig::SuppressGameSessionReadyToast != bSuppress)
		{
			rlDebug("SuppressGameSessionReadyNotification :: Setting to %s", bSuppress ? "True" : "False");
			PartyConfig::SuppressGameSessionReadyToast = bSuppress;
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

int rlXblParty::GetPartySize()
{
	return m_PartyMembers.size();
}

rlXblPartyMemberInfo* rlXblParty::GetPartyMember(int index)
{
	return m_PartyMembers[index];
}

bool rlXblParty::IsPartyMember(const rlGamerHandle& hGamer)
{
	for (int i = 0; i < m_PartyMembers.size(); i++)
	{
		if(m_PartyMembers[i]->m_GamerHandle == hGamer)
			return true;
	}
	return false;
}

bool rlXblParty::IsPartyMember(const u64 xuid)
{
	for (int i = 0; i < m_PartyMembers.size(); i++)
	{
		if(m_PartyMembers[i]->m_GamerHandle.GetXuid() == xuid)
			return true;
	}
	return false;
}

bool rlXblParty::FillOpenSessionSlots(const int localGamerIndex, const rlSessionInfo& sessionInfo, int numSlots, rlGamerHandle* availablePlayers, u64* lastInviteTimes, int numAvailablePlayers, const rlGamerHandle& excludeGamer)
{
	if (m_FillSlotsStatus.Pending())
	{
		m_bFillSlots = true;
	}
	else
	{
		bool success = false;
		FillOpenSlotsTask* task;

		if(!netTask::Create(&task, &m_FillSlotsStatus)
			|| !task->Configure(localGamerIndex, sessionInfo, numSlots, availablePlayers, lastInviteTimes, numAvailablePlayers, excludeGamer)
			|| !netTask::Run(task))
		{
			netTask::Destroy(task);
		}
		else
		{
			success = true;
		}

		return success;
	}

	return true;
}

void rlXblParty::SetLeavePartyOnInvalidGameSession(const bool bLeavePartyOnInvalidGameSession)
{
	if(s_bLeavePartyOnInvalidGameSession != bLeavePartyOnInvalidGameSession)
	{
		rlDebug1("SetLeavePartyOnInvalidGameSession :: Setting to %s", bLeavePartyOnInvalidGameSession ? "True" : "False");
		s_bLeavePartyOnInvalidGameSession = bLeavePartyOnInvalidGameSession;
	}
}

bool rlXblParty::ClearPartyMember(int index, const bool bFireRosterChangedEvent)
{
	rlXblPartyMemberInfo* partyMember = m_PartyMembers[index];
	if(partyMember != NULL)
	{
		rlDebug("ClearPartyMember :: '%s' removed from Xbox Live Party", partyMember->m_GamerName);

		Delete(partyMember, *rlXblInternal::GetInstance()->GetAllocator());
		m_PartyMembers.Delete(index);
		m_bFireRosterChangedEvent = bFireRosterChangedEvent;

		return true;
	}

	return false;
}

bool rlXblParty::AddPartyMember(rlXblPartyMemberInfo* pMember)
{
	if (pMember != NULL)
	{
		m_PartyMembers.PushAndGrow(pMember);
		m_bFireRosterChangedEvent = true; 
		
		return true;
	}

	return false;
}

bool rlXblParty::IsPartyInAnotherTitle()
{
	try
	{
		if( m_PartyView == nullptr )
		{
			return false;
		}

		return m_PartyView->IsPartyInAnotherTitle;
	}
	catch(Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
		return false;
	}
}

bool rlXblParty::IsPartyChatActive() const
{
	return m_bIsPartyChatActive;
}

bool rlXblParty::IsPartyChatSuppressed() const
{
	return m_bIsPartyChatSuppressed;
}

void rlXblParty::SetPartyView(PartyView^ partyView)
{
	static bool s_bFirstRefresh = true;

	try
	{
		// if there's no party view, we're not in a party
		if(partyView == nullptr)
		{
			rlDebug("SetPartyView :: Not in a party");
			ClearParty(true);

			return;
		}

		bool bHasGameSession = (m_PartyView != nullptr) && m_PartyView->GameSession != nullptr;
		bool bHasMatchSession = (m_PartyView != nullptr) && m_PartyView->MatchSession != nullptr;
		bool bWasInDifferentTitle = IsPartyInAnotherTitle();
		bool bWasActingUserInParty = m_bActingUserInParty;
		bool bInitialPartyView = m_PartyView == nullptr;
		bool bChangedParties = false;
		bool bChangedActingUser = false;

		rlDebug("SetPartyView :: Members: %d, GameSession: Had: %s / Have: %s, MatchSession: Had: %s / Have: %s", partyView->Members->Size, bHasGameSession ? "True" : "False", partyView->GameSession ? "True" : "False", bHasMatchSession ? "True" : "False", partyView->MatchSession ? "True" : "False");

		Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ sessionRef = nullptr;
		Microsoft::Xbox::Services::Multiplayer::MultiplayerSessionReference^ currentRef = nullptr;

		if(bHasGameSession)
		{
			currentRef = rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(m_PartyView->GameSession);
		}

		// validate acting user is in party and use join time to track if we have joined a different party than before
		int localGamerIndex = rlPresence::GetActingUserIndex();
		if(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		{
			// reset tracking
			m_bActingUserInParty = false;

			// acting user XUID
			u64 nActingXUID = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localGamerIndex);

			// detect if the acting user has changed, we want to process game session in that case
			if(!bInitialPartyView && nActingXUID != m_ActingUser)
			{
				rlDebug("SetPartyView :: Changed acting user");
				bChangedActingUser = true;
				bWasActingUserInParty = false;
			}

			// update local copy of acting user
			m_ActingUser = nActingXUID;

			// check all party members
			for (int i = 0; i < m_PartyMembers.size(); i++)
			{
				rlXblPartyMemberInfo* pInfo = m_PartyMembers[i];
				if(nActingXUID == pInfo->m_GamerHandle.GetXuid())
				{
					// log change
					if(!bWasActingUserInParty)
						rlDebug("SetPartyView :: Acting user %s added to party", rlXblInternal::GetInstance()->_GetPresenceManager()->GetName(localGamerIndex));

					// active user has been found
					m_bActingUserInParty = true; 

					// if we have a valid previous time and it's less than the new time, we changed parties
					if(!bInitialPartyView && pInfo->m_PreviousPartyJoinTime > 0 && pInfo->m_PreviousPartyJoinTime < pInfo->m_PartyJoinTime)
					{
						rlDebug("SetPartyView :: Changed party");
						bChangedParties = true;  
					}
					break;
				}
			}
		}

		if(partyView->GameSession != nullptr)
		{
			sessionRef = rlXblCommon::ConvertToMicrosoftXboxServicesMultiplayerSessionReference(partyView->GameSession);

			// only generate event when game session has changed
			bool bFireGameSessionEvent = true; 
			if(bHasGameSession && !bChangedActingUser)
			{
				if(currentRef->SessionName == sessionRef->SessionName)
				{
					if(bChangedParties)
					{
						rlDebug("SetPartyView :: Game Session is the same in new party - New SessionName: %ls, Prev SessionName: %ls", sessionRef->SessionName->Data(), currentRef->SessionName->Data());
					}
					bFireGameSessionEvent = false; 
				}
			}

			if(bFireGameSessionEvent)
			{
				GameSessionReadyOrigin origin = ORIGIN_UNKNOWN;

				// flagged from application settings
				if(s_bInviteAccepted || s_bInviteActionPending)
					origin = ORIGIN_FROM_INVITE;
				// we always join a party during boot (or are already in one) so we can assume this is from boot
				else if(s_bFirstRefresh)
					origin = ORIGIN_FROM_BOOT;
				// if we changed parties assume invite, otherwise, someone in our existing party has joined multiplayer
				else if(bInitialPartyView || bChangedParties)
					origin = ORIGIN_FROM_JVP;
				else
					origin = ORIGIN_FROM_MULTIPLAYER_JOIN;

				// generate event
				PartyEvent e(PartyEvent::PARTY_EVENT_SESSION_READY, sessionRef, origin);
				QueueEvent(e);

				// log details
				rlDebug("SetPartyView :: Game Session - Origin: %s", GetGameSessionReadyOriginAsString(origin));
				rlDebug("SetPartyView :: Game Session - ServiceConfigurationId: %ls", sessionRef->ServiceConfigurationId->Data());
				rlDebug("SetPartyView :: Game Session - SessionName: %ls", sessionRef->SessionName->Data());
				rlDebug("SetPartyView :: Game Session - SessionTemplateName: %ls", sessionRef->SessionTemplateName->Data());
			}
		}

		// apply party view
		m_PartyView = partyView;

		if (bInitialPartyView)
		{
			if(m_PartyView->IsPartyInAnotherTitle)
			{
				rlDebug("SetPartyView :: Party In Another Title.");
			}
		}
		else
		{
			// A new game session has appeared in our party!
			if (bWasInDifferentTitle != m_PartyView->IsPartyInAnotherTitle)
			{
				rlDebug("SetPartyView :: Title Status Changed. Party is in %s title.", m_PartyView->IsPartyInAnotherTitle ? "Another" : "Same");
			}
		}

		// A new game session has appeared in our party!
		if (!bHasGameSession && m_PartyView->GameSession != nullptr)
		{
			rlDebug("SetPartyView :: Game Session Registered.");
			rlXblPartyGameSessionRegisteredEvent e;
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else if (bHasGameSession && m_PartyView->GameSession == nullptr)
		{
			rlDebug("SetPartyView :: Game Session Unregistered.");
		}

		// A new match session has appeared in our party!
		if (!bHasMatchSession && m_PartyView->MatchSession != nullptr)
		{
			rlDebug("SetPartyView :: Match Session Registered.");
			rlXblPartyMatchSessionRegisteredEvent e;
			rlXblInternal::GetInstance()->DispatchEvent(&e);
		}
		else if (bHasMatchSession && m_PartyView->MatchSession == nullptr)
		{
			rlDebug("SetPartyView :: Match Session Unregistered.");
		}

		u64 oldestJoinTime = (u64)~0;
		rlXblPartyMemberInfo* oldestPartyMember = NULL;
		u64 highestGuid = 0;

		// Iterate through party member list looking for the party member with the oldest join time
		for (int i = 0; i < m_PartyMembers.size(); i++)
		{
			rlXblPartyMemberInfo* pInfo = m_PartyMembers[i];

	#if !__NO_OUTPUT
			if(m_LogPartyMembers)
			{
				rlDebug("SetPartyView :: %s is a party member", pInfo->m_GamerName);
			}
	#endif

			if (pInfo->m_PartyJoinTime < oldestJoinTime)
			{
				oldestJoinTime = pInfo->m_PartyJoinTime;
				oldestPartyMember = pInfo;
				highestGuid = pInfo->m_GamerHandle.GetXuid();
			}
			else if (pInfo->m_PartyJoinTime == oldestJoinTime)
			{
				if (pInfo->m_GamerHandle.GetXuid() > highestGuid)
				{
					oldestJoinTime = pInfo->m_PartyJoinTime;
					oldestPartyMember = pInfo;
					highestGuid = pInfo->m_GamerHandle.GetXuid();
				}
			}
		}

		// if the oldest party member is local, register the GamePlayers event handler
		if (oldestPartyMember != NULL && oldestPartyMember->m_GamerHandle.IsLocal())
		{
			// Only add if we need to
			if (!m_bIsGamePlayerEventRegistered)
			{
				RegisterGamePlayersChangedEventHandler();
			}
		}
		else if (m_bIsGamePlayerEventRegistered)
		{
			// We're not the highest XUID party member any more, but we were before
			UnregisterGamePlayersEventHandler();
		}

		s_bInviteAccepted = false;
		s_bFirstRefresh = false;

		// reset logging parameter
		OUTPUT_ONLY(m_LogPartyMembers = false;)
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
}

void rlXblParty::QueueEvent(const PartyEvent& e)
{
	SYS_CS_SYNC(m_Cs);

#if !__NO_OUTPUT
	rlDebug3("Queuing party event of type %s", PartyEvent::GetStateName(e));
#endif

	int queueSize = m_EventQueue.GetCount();
	int queueStart = m_EventQueue.GetNext();
	int queueIndex = queueStart;

	// iterate through the queue length
	for (int i = 0; i < queueSize; i++)
	{
		PartyEvent* ev = m_EventQueue.GetEvent(queueIndex);
		if (ev && e.m_EventId == ev->m_EventId)
		{
			switch(e.m_EventId)
			{
			// We have no use for duplicate events of these types
			case PartyEvent::PARTY_EVENT_MATCH_FOUND:
			case PartyEvent::PARTY_EVENT_MATCH_EXPIRED:
			case PartyEvent::PARTY_EVENT_MATCH_CANCELLED:
			case PartyEvent::PARTY_EVENT_AVAILABLE_PLAYERS_CHANGED:
				return;
			// Session ready events could contain a different session
			case PartyEvent::PARTY_EVENT_SESSION_READY:
			default:
				break;
			}
		}

		// increment the queue index, rolling it over as needed. matches
		// the flow from inside the event queue if it was popping events.
		queueIndex++;
		if (queueIndex >= PARTY_EVENT_SIZE)
		{
			queueIndex = 0;
		}
	}

	m_EventQueue.QueueEvent(e);
}
#endif // RSG_GDK

} // namespace rage

#endif // RSG_XBL
