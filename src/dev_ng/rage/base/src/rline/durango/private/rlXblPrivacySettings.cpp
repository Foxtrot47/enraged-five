// 
// rlXblPrivacySettings.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlXblPrivacySettings.h"
#include "rline/rldiag.h"
#include "rlxblinternal.h"
#include "net/durango/xbltask.h"
#include "diag/seh.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

using namespace Microsoft::Xbox::Services::Privacy;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, privacy_settings)
#undef __rage_channel
#define __rage_channel rline_privacy_settings


bool PERMISSION_ID_IS_VALID(const int permission)
{
	switch (permission)
	{
	case IPrivacySettings::PERM_BroadcastWithTwitch				: return true;
	case IPrivacySettings::PERM_CommunicateUsingText			: return true;
	case IPrivacySettings::PERM_CommunicateUsingVideo			: return true;
	case IPrivacySettings::PERM_CommunicateUsingVoice			: return true;
	case IPrivacySettings::PERM_PlayMultiplayer					: return true;
	case IPrivacySettings::PERM_ViewTargetExerciseInfo			: return true;
	case IPrivacySettings::PERM_ViewTargetGameHistory			: return true;
	case IPrivacySettings::PERM_ViewTargetMusicHistory			: return true;
	case IPrivacySettings::PERM_ViewTargetMusicStatus			: return true;
	case IPrivacySettings::PERM_ViewTargetPresence				: return true;
	case IPrivacySettings::PERM_ViewTargetProfile				: return true;
	case IPrivacySettings::PERM_ViewTargetVideoHistory			: return true;
	case IPrivacySettings::PERM_ViewTargetVideoStatus			: return true;
	case IPrivacySettings::PERM_ViewTargetUserCreatedContent	: return true;
	}

	return false;
}
#define VERIFY_PERMISSION_ID(i) rlVerifyf(PERMISSION_ID_IS_VALID(i), "Invalid permission id: %d", i)


String^ PermissionToWinRTString(const u32 permission, const wchar_t* outBuf, const int outBufLen)
{
	const char* inStr = "CommunicateUsingText";

	switch (permission)
	{
	case IPrivacySettings::PERM_BroadcastWithTwitch    			: inStr = "BroadcastWithTwitch"; break;
	case IPrivacySettings::PERM_CommunicateUsingText   			: inStr = "CommunicateUsingText"; break;
	case IPrivacySettings::PERM_CommunicateUsingVideo  			: inStr = "CommunicateUsingVideo"; break;
	case IPrivacySettings::PERM_CommunicateUsingVoice  			: inStr = "CommunicateUsingVoice"; break;
	case IPrivacySettings::PERM_PlayMultiplayer        			: inStr = "PlayMultiplayer"; break;
	case IPrivacySettings::PERM_ViewTargetExerciseInfo 			: inStr = "ViewTargetExerciseInfo"; break;
	case IPrivacySettings::PERM_ViewTargetGameHistory  			: inStr = "ViewTargetGameHistory"; break;
	case IPrivacySettings::PERM_ViewTargetMusicHistory 			: inStr = "ViewTargetMusicHistory"; break;
	case IPrivacySettings::PERM_ViewTargetMusicStatus 			: inStr = "ViewTargetMusicStatus"; break;
	case IPrivacySettings::PERM_ViewTargetPresence				: inStr = "ViewTargetPresence"; break;
	case IPrivacySettings::PERM_ViewTargetProfile				: inStr = "ViewTargetProfile"; break;
	case IPrivacySettings::PERM_ViewTargetVideoHistory			: inStr = "ViewTargetVideoHistory"; break;
	case IPrivacySettings::PERM_ViewTargetVideoStatus			: inStr = "ViewTargetVideoStatus"; break;
	case IPrivacySettings::PERM_ViewTargetUserCreatedContent	: inStr = "ViewTargetUserCreatedContent"; break;
	}

	return rlXblCommon::UTF8ToWinRtString(inStr, outBuf, outBufLen);
}


u32 PermissionIdFromString(const char* permission)
{
	if (rlVerify(permission))
	{
		static const u32 PERMHASH_BroadcastWithTwitch			= 0xeb63d56f; //("BroadcastWithTwitch   ")
		static const u32 PERMHASH_CommunicateUsingText			= 0x71223aaa; //("CommunicateUsingText  ")
		static const u32 PERMHASH_CommunicateUsingVideo			= 0xdeebfe97; //("CommunicateUsingVideo ")
		static const u32 PERMHASH_CommunicateUsingVoice			= 0xcf9aa229; //("CommunicateUsingVoice ")
		static const u32 PERMHASH_PlayMultiplayer				= 0xc7fe4926; //("PlayMultiplayer       ")
		static const u32 PERMHASH_ViewTargetExerciseInfo		= 0x2e58cfb8; //("ViewTargetExerciseInfo")
		static const u32 PERMHASH_ViewTargetGameHistory			= 0x3c86f3c5; //("ViewTargetGameHistory ")
		static const u32 PERMHASH_ViewTargetMusicHistory		= 0xae496fbf; //("ViewTargetMusicHistory")
		static const u32 PERMHASH_ViewTargetMusicStatus			= 0x9f001df3; //("ViewTargetMusicStatus ")
		static const u32 PERMHASH_ViewTargetPresence			= 0xbec496e5; //("ViewTargetPresence    ")
		static const u32 PERMHASH_ViewTargetProfile				= 0x832f0e55; //("ViewTargetProfile     ")
		static const u32 PERMHASH_ViewTargetVideoHistory		= 0x0793810a; //("ViewTargetVideoHistory")
		static const u32 PERMHASH_ViewTargetVideoStatus			= 0x69d756d3; //("ViewTargetVideoStatus ")
		static const u32 PERMHASH_ViewTargetUserCreatedContent  = 0x7a61fe1a; //("ViewTargetUserCreatedContent ")

		const u32 permissionhash = atStringHash(permission);

		switch (permissionhash)
		{
		case PERMHASH_BroadcastWithTwitch:			return IPrivacySettings::PERM_BroadcastWithTwitch;
		case PERMHASH_CommunicateUsingText:			return IPrivacySettings::PERM_CommunicateUsingText;
		case PERMHASH_CommunicateUsingVideo:		return IPrivacySettings::PERM_CommunicateUsingVideo;
		case PERMHASH_CommunicateUsingVoice:		return IPrivacySettings::PERM_CommunicateUsingVoice;
		case PERMHASH_PlayMultiplayer:				return IPrivacySettings::PERM_PlayMultiplayer;
		case PERMHASH_ViewTargetExerciseInfo:		return IPrivacySettings::PERM_ViewTargetExerciseInfo;
		case PERMHASH_ViewTargetGameHistory:		return IPrivacySettings::PERM_ViewTargetGameHistory;
		case PERMHASH_ViewTargetMusicHistory:		return IPrivacySettings::PERM_ViewTargetMusicHistory;
		case PERMHASH_ViewTargetMusicStatus:		return IPrivacySettings::PERM_ViewTargetMusicStatus;
		case PERMHASH_ViewTargetPresence:			return IPrivacySettings::PERM_ViewTargetPresence;
		case PERMHASH_ViewTargetProfile:			return IPrivacySettings::PERM_ViewTargetProfile;
		case PERMHASH_ViewTargetVideoHistory:		return IPrivacySettings::PERM_ViewTargetVideoHistory;
		case PERMHASH_ViewTargetVideoStatus:		return IPrivacySettings::PERM_ViewTargetVideoStatus;
		case PERMHASH_ViewTargetUserCreatedContent: return IPrivacySettings::PERM_ViewTargetUserCreatedContent;
		}
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// XblCheckMultiplePermissions
///////////////////////////////////////////////////////////////////////////////

class XblCheckMultiplePermissions : public netXblTask 
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(XblCheckMultiplePermissions);

	XblCheckMultiplePermissions() : m_localGamerIndex(-1), m_results(NULL), m_numResults(0) {;}

	bool Configure(const int localGamerIndex, const u32 permissions, PrivacySettingsResultMultiUser* results, const u32 numResults, rlGamerHandle* gamers, const u32 numGamers);

private:
	virtual bool DoWork();
	virtual void ProcessSuccess();
	virtual void ProcessFailure();

	bool SetResult(const u64 xuid, IVectorView<PermissionCheckResult^ >^ items);

private:
	PrivacySettingsResultMultiUser*          m_results;
	u32                                      m_numResults;
	int                                      m_localGamerIndex;
	Platform::Collections::Vector<String^>^  m_permissionIds;
	Platform::Collections::Vector<String^>^  m_targetXboxUserIds;
};

bool XblCheckMultiplePermissions::Configure(const int localGamerIndex, const u32 permissions, PrivacySettingsResultMultiUser* results, const u32 numResults, rlGamerHandle* gamers, const u32 numGamers)
{
	rtry
	{
		rverify(results, catchall, );

		m_localGamerIndex = localGamerIndex;
		rverify(RL_VERIFY_LOCAL_GAMER_INDEX(m_localGamerIndex), catchall, );

		rverify(gamers, catchall, );
		rverify(0 < numGamers, catchall, );
		rverify(numGamers <= RL_MAX_GAMERS_PER_SESSION, catchall, );

		m_targetXboxUserIds = ref new Platform::Collections::Vector<String^>();
		for (u32 i=0; i<numGamers; i++)
			m_targetXboxUserIds->Append( rlXblCommon::XuidToWinRTString(gamers[i].GetXuid()) );

		wchar_t wsUrl[256] = {0};

		m_permissionIds = ref new Platform::Collections::Vector<String^>();

		u32 numPermissions = 0;
		if (IPrivacySettings::PERM_BroadcastWithTwitch & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_BroadcastWithTwitch, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_CommunicateUsingText & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_CommunicateUsingText, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_CommunicateUsingVideo & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_CommunicateUsingVideo, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_CommunicateUsingVoice & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_CommunicateUsingVoice, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_PlayMultiplayer & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_PlayMultiplayer, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetExerciseInfo & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetExerciseInfo, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetGameHistory & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetGameHistory, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetMusicHistory & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetMusicHistory, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetMusicStatus & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetMusicStatus, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetPresence & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetPresence, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetProfile & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetProfile, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetVideoHistory & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetVideoHistory, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetVideoStatus & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetVideoStatus, wsUrl, COUNTOF(wsUrl)) );
		}

		if (IPrivacySettings::PERM_ViewTargetUserCreatedContent & permissions)
		{
			++numPermissions;
			wsUrl[0] = '\0';
			m_permissionIds->Append( PermissionToWinRTString(IPrivacySettings::PERM_ViewTargetUserCreatedContent, wsUrl, COUNTOF(wsUrl)) );
		}

		rverify(numResults == numPermissions, catchall, );
		m_results = results;
		m_numResults = numResults;

		netTaskDebug1("Start XblCheckMultiplePermissions.");

		return true;
	}
	rcatchall
	{
		netTaskError("Failed to Start XblCheckMultiplePermissions.");
		return false;
	}
}

bool XblCheckMultiplePermissions::DoWork()
{
	bool success = false;

	try
	{
		rtry
		{
			rverify(m_results, catchall, );

			rverify(m_numResults, catchall, );

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(m_localGamerIndex), catchall, );

			XboxLiveContext^ xboxLiveContext = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_localGamerIndex);

			success = m_XblStatus.BeginOp<IVectorView< MultiplePermissionsCheckResult^>^>("CheckMultiplePermissionsWithMultipleTargetUsersAsync"
				,xboxLiveContext->PrivacyService->CheckMultiplePermissionsWithMultipleTargetUsersAsync(m_permissionIds->GetView(), m_targetXboxUserIds->GetView())
				,DEFAULT_ASYNC_TIMEOUT
				,this
				,&m_MyStatus);

			rverify(success, catchall, );
		}
		rcatchall
		{
			netTaskError("Failed to DoWork XblCheckMultiplePermissions.");
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}

	return success;
}

void XblCheckMultiplePermissions::ProcessSuccess()
{
	netTaskDebug1("SUCCEEDED XblCheckMultiplePermissions.");

	try
	{
		IVectorView< MultiplePermissionsCheckResult^ >^ resultvector = m_XblStatus.GetResults< IVectorView< MultiplePermissionsCheckResult^ >^ >();

		if (rlVerify(resultvector))
		{
			netTaskDebug1("Succeeded to retrieve User permissions.");

			for (u32 i=0; i<resultvector->Size; i++)
			{
				MultiplePermissionsCheckResult^ result = resultvector->GetAt(i);
				if (rlVerify(result))
				{
					netTaskDebug1("XboxUserId='%ls', number_permissions='%d'", result->XboxUserId->Data(), result->Items->Size);

					if (SetResult( rlXblCommon::WinRtStringToXuid(result->XboxUserId), result->Items ))
					{
						netTaskDebug1("Permissions parsed: name='%ls'", result->XboxUserId->Data());
					}
					else
					{
						netTaskError("Failed to parse Permissions: name='%ls'", result->XboxUserId->Data());
					}
				}
			}
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}
}

void XblCheckMultiplePermissions::ProcessFailure()
{
	netTaskError("FAILED XblCheckMultiplePermissions.");
}

bool XblCheckMultiplePermissions::SetResult(const u64 xuid, IVectorView<PermissionCheckResult^ >^ items)
{
	bool result = false;

	if (rlVerify(items) && rlVerify(m_results) && rlVerify(m_numResults > 0))
	{
		char stringBuffer[64];

		for (u32 i = 0; i < m_numResults; i++)
		{
			if (xuid == m_results[i].m_gamer.GetXuid())
			{
				netTaskDebug1("number_permissions='%d'", items->Size);

				for (u32 j=0; j<items->Size; j++)
				{
					PermissionCheckResult^ perm = items->GetAt(j);

					if (rlVerify(perm))
					{
						PrivacySettingsResult permresult;

						ZeroMemory(stringBuffer, sizeof(stringBuffer));
						rlVerify( rlXblCommon::WinRtStringToUTF8(perm->PermissionRequested, stringBuffer, sizeof(stringBuffer)) );

						//PermissionRequested Contains the permission requested. 
						permresult.m_permission = PermissionIdFromString( stringBuffer );

						//IsAllowed Indicates if the user is allowed the requested access. 
						permresult.m_isAllowed = perm->IsAllowed;

						//DenyReasons Contains the reason the requested permissions were denied. 
						//perm->DenyReasons;
						ZeroMemory(permresult.m_denyReason, sizeof(permresult.m_denyReason));

#if !__NO_OUTPUT
						netTaskDebug1("PermissionRequested='%ls', IsAllowed='%ls'", perm->PermissionRequested->Data(), perm->IsAllowed?"true":"false");

						for (u32 h=0; h<perm->DenyReasons->Size; h++)
						{
							PermissionDenyReason^ denyReason = perm->DenyReasons->GetAt(h);
							if (denyReason)
							{
								netTaskDebug1("PermissionDenyReason: Reason='%ls', RestrictedSetting='%ls'", denyReason->Reason->Data(), denyReason->RestrictedSetting->Data());
							}
						}
#endif // !__NO_OUTPUT

						m_results[i].m_permissions.PushAndGrow(permresult);
					}
				}

				result = true;

				break;
			}
		}
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////
// XblCheckPermission
///////////////////////////////////////////////////////////////////////////////

class XblCheckPermission : public netXblTask 
{
public:
	RL_TASK_USE_CHANNEL(rline_xbl);
	RL_TASK_DECL(XblCheckPermission);

	XblCheckPermission() : m_localGamerIndex(-1), m_result(NULL) {;}

	bool Configure(const int localGamerIndex, const u32 permission, const rlGamerHandle& gamer, PrivacySettingsResult* result);

private:
	virtual bool DoWork();
	virtual void ProcessSuccess();
	virtual void ProcessFailure();

private:
	PrivacySettingsResult*  m_result;
	int                     m_localGamerIndex;
	String^                 m_permissionId;
	String^                 m_targetXboxUserId;
};


bool XblCheckPermission::Configure(const int localGamerIndex, const u32 permission, const rlGamerHandle& gamer, PrivacySettingsResult* result)
{
	rtry
	{
		rverify(result, catchall, );
		m_result = result;

		rverify(VERIFY_PERMISSION_ID(permission), catchall, );

		m_localGamerIndex = localGamerIndex;
		rverify(RL_VERIFY_LOCAL_GAMER_INDEX(m_localGamerIndex), catchall, );

		rverify(gamer.IsValid(), catchall, );

		m_targetXboxUserId = rlXblCommon::XuidToWinRTString(gamer.GetXuid());

		wchar_t wsUrl[256] = {0};
		m_permissionId = PermissionToWinRTString(permission, wsUrl, COUNTOF(wsUrl));

		netTaskDebug1("Start XblCheckPermission.");

		return true;
	}
	rcatchall
	{
		netTaskError("Failed to Start XblCheckPermission.");
		return false;
	}
}

bool XblCheckPermission::DoWork()
{
	bool success = false;

	try
	{
		rtry
		{
			rverify(m_result, catchall, );

			rverify(RL_VERIFY_LOCAL_GAMER_INDEX(m_localGamerIndex), catchall, );

			XboxLiveContext^ xboxLiveContext = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_localGamerIndex);

			success = m_XblStatus.BeginOp<PermissionCheckResult^>("CheckPermissionWithTargetUserAsync"
				,xboxLiveContext->PrivacyService->CheckPermissionWithTargetUserAsync(m_permissionId, m_targetXboxUserId)
				,DEFAULT_ASYNC_TIMEOUT
				,this
				,&m_MyStatus);

			rverify(success, catchall, );
		}
		rcatchall
		{
			netTaskError("Failed to DoWork XblCheckPermission.");
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}

	return success;
}

void XblCheckPermission::ProcessSuccess()
{
	netTaskDebug1("SUCCEEDED XblCheckPermission.");

	try
	{
		PermissionCheckResult^ result = m_XblStatus.GetResults< PermissionCheckResult^ >();
		if (rlVerify(result))
		{
			netTaskDebug1("Succeeded to retrieve User permissions.");

			char stringBuffer[64];
			ZeroMemory(stringBuffer, sizeof(stringBuffer));
			rlVerify( rlXblCommon::WinRtStringToUTF8(result->PermissionRequested, stringBuffer, sizeof(stringBuffer)) );

			//PermissionRequested Contains the permission requested. 
			m_result->m_permission = PermissionIdFromString( stringBuffer );

			//IsAllowed Indicates if the user is allowed the requested access. 
			m_result->m_isAllowed = result->IsAllowed;

#if !__NO_OUTPUT
			netTaskDebug1("PermissionRequested='%ls', IsAllowed='%s'", result->PermissionRequested->Data(), result->IsAllowed?"true":"false");

			// Build a string containing all the deny reasons.
			atString denyReasonsStr;

			for (u32 h=0; h<result->DenyReasons->Size; h++)
			{
				PermissionDenyReason^ denyReason = result->DenyReasons->GetAt(h);
				if (denyReason)
				{
					// Separate reasons with a comma.
					if (h > 0) {
						denyReasonsStr += ", ";
					}

					// Append reason to string.
					denyReasonsStr += atString((const unsigned short*)denyReason->Reason->Data());

					netTaskDebug1("PermissionDenyReason: Reason='%ls', RestrictedSetting='%ls'", denyReason->Reason->Data(), denyReason->RestrictedSetting->Data());
				}
			}

			// Copy the contents of denyReasonsStr into the m_denyReason char buffer.
			ZeroMemory(m_result->m_denyReason, sizeof(m_result->m_denyReason));
			memcpy(m_result->m_denyReason, denyReasonsStr.c_str(), PrivacySettingsResult::MAX_DENY_REASON);

#endif // !__NO_OUTPUT
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}
}

void XblCheckPermission::ProcessFailure()
{
	netTaskError("FAILED XblCheckPermission.");
}

///////////////////////////////////////////////////////////////////////////////
//  IPrivacySettings
///////////////////////////////////////////////////////////////////////////////

bool IPrivacySettings::CheckPermissions(const int localGamerIndex, const u32 permissions, PrivacySettingsResultMultiUser* results, const u32 numresults, rlGamerHandle* gamers, const u32 numGamers, netStatus* status)
{
	CREATE_NET_XBLTASK(XblCheckMultiplePermissions, status, localGamerIndex, permissions, results, numresults, gamers, numGamers);
}

bool IPrivacySettings::CheckPermission(const int localGamerIndex, const u32 permission, const rlGamerHandle& gamer, PrivacySettingsResult* result, netStatus* status)
{
	CREATE_NET_XBLTASK(XblCheckPermission, status, localGamerIndex, permission, gamer, result);
}

} // namespace rage

#endif //RSG_DURANGO

