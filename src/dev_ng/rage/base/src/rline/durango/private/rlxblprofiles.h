// 
// rlxblprofiles.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_XBL_PROFILES_H
#define RLXBL_XBL_PROFILES_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlxblprofiles.h is a private header. Do not #include it."
#endif

#include "rlxblhttptask.h"
#include "diag/channel.h"
#include "net/status.h"

namespace rage
{

//PURPOSE
//  Interface to Xbox Live profile data (display names and gamertags).
class rlXblProfiles : public IProfiles
{
public:
    rlXblProfiles();
    virtual ~rlXblProfiles();

    //PURPOSE
    //  Initialize the class.
    bool Init();

    //PURPOSE
    //  Shut down the class.
    void Shutdown();

    //PURPOSE
    //  Called when the main player signs out
    void OnSignOut();

    //PURPOSE
    //  Update the class. Should be called once per frame.
    void Update();

	//PURPOSE
	// Pass in a list of gamerhandles, get display names and gamertags back about each.
	// displayNames can be NULL if you don't want display names, or gamertags can be NULL if you don't want gamertags, but at least one needs to be non-NULL
	// Note:
	//	gamerhandles, displayNames, gamertags must remain valid until the status is no longer pending. These must not be allocated on the stack.
	bool GetPlayerNames(const int localGamerIndex, const rlGamerHandle* gamerHandles, const unsigned numGamerhandles, rlDisplayName* displayNames, rlGamertag* gamertags, netStatus* status);
	
	//PURPOSE
	// Same as above but treats gamerHandles, diplayNames, and gamertags as SpanArrays. Use this version when the gamerHandle, displayName, or gamertag is part of another structure.
	// gamerHandleSpan is the number of bytes to skip to get to the next gamerHandle. Must be at least sizeof(rlGamerHandle)
	// displayNameSpan is the number of bytes to skip to get to the next displayName. Must be at least sizeof(rlDisplayName)
	// gamertagSpan is the number of bytes to skip to get to the next gamertag. Must be at least sizeof(rlGamertag)
	virtual bool GetPlayerNames(const int localGamerIndex, const rlGamerHandle* gamerHandles, const u32 gamerHandleSpan, const unsigned numGamerhandles, rlDisplayName* displayNames, const u32 displayNameSpan, rlGamertag* gamertags, const u32 gamertagSpan, netStatus* status);

	//PURPOSE
	//  Cancels a previous call to GetPlayerNames().
	void CancelPlayerNameRequest(netStatus* status);

private:
	void Clear();
	bool m_Initialized;
};


} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_XBL_PROFILES_H
