// 
// rlXblPrivacySettings.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PRIVACY_SETTINGS_H
#define RLXBL_PRIVACY_SETTINGS_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "rlXblPrivacySettings.h is a private header. Do not #include it. Use rlXblPrivacySettings_interface.h instead"
#endif

#include "../rlXblPrivacySettings_interface.h"

#endif //RSG_DURANGO
#endif // RLXBL_USERSTATISTICS_H

