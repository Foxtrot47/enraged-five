// 
// rlxblsystemui.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_DURANGO

#include "rlxblinternal.h"
#include "rlxblachievements.h"

#include "atl/functor.h"
#include "diag/seh.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "net/durango/xbltask.h"
#include "rline/rlachievement.h"
#include "rline/rldiag.h"
#include "rline/rltitleid.h"

using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Microsoft::Xbox::Services::Achievements;
using namespace Windows::Xbox::Achievements;

namespace rage
{

extern const rlTitleId* g_rlTitleId;

RAGE_DEFINE_SUBCHANNEL(rline, xblachievements);
#undef __rage_channel
#define __rage_channel rline_xblachievements

class XblGetAchievementsTask : public netXblTask
{
public:

	NET_TASK_DECL(XblGetAchievementsTask);
    NET_TASK_USE_CHANNEL(rline_xblachievements);

	XblGetAchievementsTask();

	bool Configure(const int localGamerIndex, rlAchievementInfo* achievements, const int maxAchievements, int* numAchievements);

protected:
	virtual void ProcessSuccess();
	virtual bool DoWork();

	int m_LocalGamerIndex;
	
	int m_MaxAchievements;
	rlAchievementInfo* m_AchievementsPtr;
	int* m_NumAchievementsPtr;

};

XblGetAchievementsTask::XblGetAchievementsTask()
{
	m_LocalGamerIndex = 0;
	m_AchievementsPtr = NULL;
	m_MaxAchievements = 0;
	m_NumAchievementsPtr = NULL;
}

bool XblGetAchievementsTask::Configure(const int localGamerIndex, rlAchievementInfo* achievements, const int maxAchievements, int* numAchievements)
{
	if (rlVerify(achievements) && rlVerify(numAchievements))
	{
		m_LocalGamerIndex = localGamerIndex;
		m_AchievementsPtr = achievements;
		m_MaxAchievements = maxAchievements;
		m_NumAchievementsPtr = numAchievements;
		return true;
	}

	return false;
}

void XblGetAchievementsTask::ProcessSuccess()
{
	try
	{

		AchievementsResult^ achievements = m_XblStatus.GetResults<AchievementsResult^>();
		*m_NumAchievementsPtr = 0;

		rageAssertf(achievements, "achievements is NULL.");
		int count = achievements ? achievements->Items->Size : 0;

		// NOTE: The XDP automatically assigns a self-incrementing ID for achievements that do not match the XLAST files.
		//	Since an achievement was deleted at one point, this is now out of sync. We'll need to eventually find a way
		//  to match up these achievements with our XLAST config using something like a name comparison.
		// Set the achievement Id to 1 to begin, and increment accordingly.
		int achId = 1;

		for (int i = 0; i < count && i < m_MaxAchievements; i++)
		{
			Achievement^ ach = achievements->Items->GetAt(i);

			char achievementName[256] = {0};
			char achievementDesc[256] = {0};
			rlXblCommon::WinRtStringToUTF8(ach->Name, &achievementName[0], sizeof(achievementName));
			rlXblCommon::WinRtStringToUTF8(ach->UnlockedDescription, &achievementDesc[0], sizeof(achievementDesc));

			bool bUnlocked = (ach->ProgressState == AchievementProgressState::Achieved);

#if !__NO_OUTPUT
			int xdpId = -1, idBuf;
			if(swscanf_s(ach->Id->Data(), L"%d", &idBuf) == 1)
			{
				xdpId = idBuf;
			}

			netTaskDebug3("[id:%d,Xid:%d] %s achievement: %s, %s, %s", achId, xdpId, bUnlocked ? "unlocked" : "locked", achievementName, ach->IsSecret ? "SECRET" : "NOT SECRET", achievementDesc);
#endif

			// Write out the progression
			AchievementProgression^ progression = ach->Progression;
			if (progression != nullptr && progression->Requirements != nullptr)
			{
				for(AchievementRequirement^ req : progression->Requirements)
				{
					if (req->TargetProgressValue != nullptr)
					{
						netTaskDebug3("  Progress: %ls of %ls", req->CurrentProgressValue != nullptr ? req->CurrentProgressValue->Data() : L"0", req->TargetProgressValue->Data());
					}
				}
			}

			int achPoints = 0;

			for (AchievementReward^ reward : ach->Rewards)
			{
				// We only particularly care for the points
				if (reward->RewardType == AchievementRewardType::Gamerscore)
				{
					String^ rewardData = reward->Data;
					int points;
					if(swscanf_s(rewardData->Data(), L"%d", &points) == 1)
					{
						achPoints = points;
					}
				}
			}

			m_AchievementsPtr[*m_NumAchievementsPtr].Reset(achId, achPoints, &achievementName[0], &achievementDesc[0], bUnlocked);
			++(*m_NumAchievementsPtr);
			achId++;
		}
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
	}
}

bool XblGetAchievementsTask::DoWork()
{
	bool success = false;

	try
	{
		unsigned titleId = g_rlTitleId->m_XblTitleId.GetTitleId();
		XboxLiveContext^ context = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxLiveContext(m_LocalGamerIndex);
		if (context == nullptr)
		{
			return false;
		}

		success = m_XblStatus.BeginOp<AchievementsResult^>("GetAchievementsForTitleIdAsync", 
													context->AchievementService->GetAchievementsForTitleIdAsync(context->User->XboxUserId, titleId, 
													AchievementType::All, false, AchievementOrderBy::Default, 0, m_MaxAchievements),
													DEFAULT_ASYNC_TIMEOUT,
													this,
													&m_MyStatus);
	}
	catch (Platform::Exception^ ex)
	{
		TASK_EXCEPTION(ex);
		success = false;
	}

	return success;
}

rlXblAchievements::rlXblAchievements()
{

}


rlXblAchievements::~rlXblAchievements()
{
	this->Shutdown();
}

bool rlXblAchievements::Init()
{
#if !__NO_OUTPUT
	try
	{
		AchievementSource^ pAchievementSource = AchievementNotifier::GetTitleIdFilteredSource();
		if (pAchievementSource != nullptr)
		{
			pAchievementSource->AchievementUnlocked += ref new EventHandler<AchievementUnlockedEventArgs^>(
				[this] (Platform::Object^, AchievementUnlockedEventArgs^ args)
			{
				try
				{
					rlDebug3("rlXblAchievements unlocked handler:ID: %ls, User: %ls, at time: %" I64FMT "d", args->AchievementId->Data(), args->User->XboxUserId->Data(), args->UnlockTime.UniversalTime);
				}
				catch (Platform::Exception^ ex)
				{
					RL_EXCEPTION(ex);
				}
			});
		}
	}
	catch (Platform::Exception^ ex)
	{
		RL_EXCEPTION(ex);
	}
#endif

	return true;
}

void rlXblAchievements::Shutdown()
{

}

void rlXblAchievements::Update()
{

}

bool rlXblAchievements::ReadAchievements(const int localGamerIndex, rlAchievementInfo* achievements, const int maxAchievements, int* numAchievements, netStatus* status)
{
	CREATE_NET_XBLTASK(XblGetAchievementsTask, status, localGamerIndex, achievements, maxAchievements, numAchievements);
}

//PURPOSE
// Sets the function that writes XB1 Events for achievements
void rlXblAchievements::SetAchievementEventFunc(const IAchievements::AchievementEvent& functor)
{
	m_AchievementEventWriteFunctor = functor;
}

//PURPOSE
// Writes an achievement using the achievement event functor
bool rlXblAchievements::WriteAchievement(const int localGamerIndex, const int achId)
{
	if (m_AchievementEventWriteFunctor)
	{
		m_AchievementEventWriteFunctor(achId, localGamerIndex, FULL_UNLOCK_PROGRESS);
	}
	else
	{
		rlAssertf(false, "Achievement Event Write Functor not set.");
		return false;
	}

	return true;
}

//PURPOSE
// Gets the user stat required for an achievement
bool rlXblAchievements::ReadStatInt(const int localGamerIndex, const char * statName, const int statNameLength, int* outInt, netStatus* status)
{
	bool success = false;

	rlXblHttpUserStatsTask* task;

	rtry
	{
		u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localGamerIndex);
		String^ scid = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());

		task = rlGetTaskManager()->CreateTask<rlXblHttpUserStatsTask>();
		rverify(task,catchall,);
		rverify(rlTaskBase::Configure(task, localGamerIndex, xuid, scid, statName, statNameLength, outInt, status), catchall,);
		rverify(rlGetTaskManager()->AppendSerialTask(task), catchall,);
		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}

//PURPOSE
// Gets the user stat required for an achievement
bool rlXblAchievements::ReadStatDouble(const int localGamerIndex, const char * statName, const int statNameLength, double* outDouble, netStatus* status)
{
	bool success = false;

	rlXblHttpUserStatsTask* task;

	rtry
	{
		u64 xuid = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localGamerIndex);
		String^ scid = ref new String(g_rlTitleId->m_XblTitleId.GetPrimaryServiceConfigId());

		task = rlGetTaskManager()->CreateTask<rlXblHttpUserStatsTask>();
		rverify(task,catchall,);
		rverify(rlTaskBase::Configure(task, localGamerIndex, xuid, scid, statName, statNameLength, outDouble, status), catchall,);
		rverify(rlGetTaskManager()->AppendSerialTask(task), catchall,);
		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}


} // namespace rage

#endif // RSG_DURANGO
