// 
// rlxbluserstatistics_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_USERSTATISTICS_INTERFACE_H
#define RLXBL_USERSTATISTICS_INTERFACE_H

#if RSG_DURANGO

#include "string/string.h"
#include "atl/array.h"
#include "rlxbl_interface.h"
#include "rline/rldiag.h"

namespace rage
{

class netStatus;
class rlGamerHandle;

//PURPOSE
//  Class that represents a user statistic.
class rlXblUserStatiticsInfo
{
public:
	static const int MAX_NAME_LENGTH = 256;

	static const int MAX_VALUE_LENGTH = 64;

	enum eTypes { TYPE_DOUBLE, TYPE_S64, TYPE_STRING };

public:

	rlXblUserStatiticsInfo( ) {m_Name[0] = '\0';}

	bool SetName(const char* name)
	{
		m_Name[0] = '\0';
		rlAssert(name);
		if (name)
		{
			formatf(m_Name, MAX_NAME_LENGTH, name);
			return true;
		}
		return false;
	}

	const char* GetValueAsString( char (&buff)[MAX_VALUE_LENGTH] ) const
	{
		buff[0] = '\0';

		if (IsString())
			formatf(buff, MAX_VALUE_LENGTH, "%s", GetString());
		else if (IsDouble( ))
			formatf(buff, MAX_VALUE_LENGTH, "%lf", GetDouble());
		else if (IsS64( ))
			formatf(buff, MAX_VALUE_LENGTH, "%" I64FMT "d", GetS64());

		return buff;
	}

	bool  IsString( ) const { return (m_Type == TYPE_STRING); }
	bool  IsDouble( ) const { return (m_Type == TYPE_DOUBLE); }
	bool     IsS64( ) const { return (m_Type == TYPE_S64); }

	const char* GetString( ) const { return m_Value. m_asString; }
	double      GetDouble( ) const { return m_Value. m_asDouble; }
	s64            GetS64( ) const { return m_Value. m_asS64; }


public:
	eTypes  m_Type;
	char    m_Name[MAX_NAME_LENGTH];
	union {
		double  m_asDouble;
		s64     m_asS64;
		char    m_asString[MAX_VALUE_LENGTH];
	} m_Value;
};

//PURPOSE
//  Class that represents a user statistic.
class rlXblMultiUserStatiticsInfo
{
public:
	static const int MAX_NUM_STATS = 10;

public:
	rlXblMultiUserStatiticsInfo(rlGamerHandle& gamer) {m_gamer = gamer;}

	void  AddStat(const char* name) 
	{
		rlAssert(name);
		rlAssert(m_stats.GetCount() < MAX_NUM_STATS);

		if (name && m_stats.GetCount() < MAX_NUM_STATS)
		{
			rlAssert(!Exists(name));
			rlXblUserStatiticsInfo stat;
			stat.SetName(name);
			m_stats.PushAndGrow(stat);
		}
	}

	bool  Exists(const char* name)
	{
		if (name)
		{
			for (int i=0; i<m_stats.GetCount(); i++)
			{
				if (stricmp(name, m_stats[i].m_Name) == 0)
				{
					return true;
				}
			}
		}

		return false;
	}

public:
	rlGamerHandle  m_gamer;
	atArray < rlXblUserStatiticsInfo >  m_stats;
};

//PURPOSE
//  Interface to the user statistics on Durango.
class IUserStatistics
{
public:

	//PURPOSE
	// Get statistics for the local user.
	//PARAMS
	//  localGamerIndex - Index of local gamer creating the session.
	//  stats - array with all stats being retrieved.
	//  maxStats - number of stats.
	//  status - pool result from request.
	static bool GetUserStatistics(const int localGamerIndex, rlXblUserStatiticsInfo* stats, const unsigned maxStats, eXblServiceConfigId serviceConfigId, netStatus* status);

	//PURPOSE
	// Get statistics for the multiple user's.
	//PARAMS
	//  localGamerIndex - Index of local gamer creating the session.
	//  players - array with all stats being retrieved.
	//  maxPlayers - number of players.
	//  status - pool result from request.
	static bool GetMultipleUserStatistics(const int localGamerIndex, rlXblMultiUserStatiticsInfo* players, const unsigned maxPlayers, eXblServiceConfigId serviceConfigId, netStatus* status);
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_USERSTATISTICS_INTERFACE_H
