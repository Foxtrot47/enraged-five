// 
// rlxbl_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_INTERFACE_H
#define RLXBL_INTERFACE_H

#if RSG_DURANGO

#include "atl/delegate.h" 
#include "diag/channel.h"
#include "fwnet/nettypes.h"
#include "data/autoid.h"
#include "rline/rl.h"
#include "rlxblcommerce_interface.h"
#include "rlxblachievements_interface.h"
#include "rlxblparty_interface.h"
#include "rlxblplayers_interface.h"
#include "rlxblpresence_interface.h"
#include "rlxblprivileges_interface.h"
#include "rlxblprofiles_interface.h"
#include "rlxblrealtimeactivity_interface.h"
#include "rlxblsessions_interface.h"
#include "rlxblsystemui_interface.h"
#include "rlXblProfanityCheck_interface.h"

#define XBL_XUID_BUFFER_SIZE 32

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(rline, xbl)
RAGE_DECLARE_SUBCHANNEL(rline_xbl, tasks)

class sysMemAllocator;

enum eXblServiceConfigId
{
	SERVICE_ID_TITLE,
	SERVICE_ID_GLOBAL_REPUTATION,
};

enum eXblEventType
{
	// Presence Events
    RLXBL_PRESENCE_EVENT_SIGNIN_STATUS_CHANGED = 0,
	RLXBL_PRESENCE_EVENT_NETWORK_STATUS_CHANGED = 1,
	// Party Events
	RLXBL_PARTY_EVENT_ROSTER_CHANGED = 100,
	RLXBL_PARTY_EVENT_MATCH_CANCELLED = 101,
	RLXBL_PARTY_EVENT_MATCH_EXPIRED = 102,
	RLXBL_PARTY_EVENT_MATCH_FOUND = 103,
	RLXBL_PARTY_EVENT_GAME_AVAILABLE_PLAYERS = 104,
	RLXBL_PARTY_EVENT_GAME_SESSION_READY = 105,
	RLXBL_PARTY_EVENT_GAME_REGISTERED = 106,
	RLXBL_PARTY_EVENT_MATCH_REGISTERED = 107,
	RLXBL_PARTY_EVENT_GAME_SESSION_UNAVAILABLE = 108,
	RLXBL_PARTY_EVENT_GAME_SESSION_INVALID = 109,
	// RTA Events
	RLXBL_RTA_EVENT_CONNECTION_CHANGED = 200,
	// Friend Events
	RLXBL_FRIEND_EVENT_LIST_CHANGED = 300,
	RLXBL_FRIEND_EVENT_ONLINE_STATUS_CHANGED = 301,
	RLXBL_FRIEND_EVENT_TITLE_STATUS_CHANGED = 302,
	RLXBL_FRIEND_EVENT_STATISTIC_CHANGED = 303,
	RLXBL_FRIEND_EVENT_SESSION_CHANGED = 304,
	//Invite Events
	RLXBL_INVITE_EVENT_ACCEPTED = 400,
	RLXBL_INVITE_EVENT_UNAVAILABLE = 401,
};

#define RLXBL_EVENT_COMMON_DECL(name)\
    static unsigned EVENT_ID() {return name::GetAutoId();}\
    virtual unsigned GetId() const {return name::GetAutoId();}

#define RLXBL_EVENT_DECL(name, id)\
    AUTOID_DECL_ID(name, rlXblEvent, id)\
    RLXBL_EVENT_COMMON_DECL(name)

class rlXblEvent
{
public:
	AUTOID_DECL_ROOT(rlXblEvent);
	RLXBL_EVENT_COMMON_DECL(rlXblEvent);

	rlXblEvent() {}
	virtual ~rlXblEvent() {}
};

typedef atDelegator<void (class rlXbl*, const class rlXblEvent*)> rlXblEventDelegator;
typedef rlXblEventDelegator::Delegate rlXblEventDelegate;

//PURPOSE
//  Dispatched when a gamer's sign-in status changes.
class rlXblPresenceEventSigninStatusChanged : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPresenceEventSigninStatusChanged, RLXBL_PRESENCE_EVENT_SIGNIN_STATUS_CHANGED);

	rlXblPresenceEventSigninStatusChanged(int localGamerIndex, const bool isSignedIn)
		: m_LocalGamerIndex(localGamerIndex)
		, m_IsSignedIn(isSignedIn)
	{
	}

	// returns whether the game is currently signed in
	bool IsSignedIn() const {return m_IsSignedIn;}
	int GetLocalGamerIndex() const {return m_LocalGamerIndex;}

private:
	int m_LocalGamerIndex;
	bool m_IsSignedIn : 1;
};

//PURPOSE
//  Dispatched when the network status has changed (access to XBL)
class rlXblPresenceEventNetworkStatusChanged : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPresenceEventNetworkStatusChanged, RLXBL_PRESENCE_EVENT_NETWORK_STATUS_CHANGED);

	enum Flags
	{
		FLAG_CONNECTED_TO_XBL		= 0x01,
	};

	rlXblPresenceEventNetworkStatusChanged(const unsigned flags)
		: m_Flags(flags)
	{
	}

	bool IsOnline() const
	{
		return (0 != (m_Flags & FLAG_CONNECTED_TO_XBL));
	}

private:
	unsigned m_Flags;
};

//PURPOSE
//  Dispatched when party roster changes
class rlXblPartyEventsRosterChanged : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyEventsRosterChanged, RLXBL_PARTY_EVENT_ROSTER_CHANGED);
	rlXblPartyEventsRosterChanged() {}
};

//PURPOSE
//  Dispatched when party match status is canceled
class rlXblPartyMatchStatusCancelledEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyMatchStatusCancelledEvent, RLXBL_PARTY_EVENT_MATCH_CANCELLED);
	rlXblPartyMatchStatusCancelledEvent() {}
};

//PURPOSE
//  Dispatched when party match status is expired
class rlXblPartyMatchStatusExpiredEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyMatchStatusExpiredEvent, RLXBL_PARTY_EVENT_MATCH_EXPIRED);
	rlXblPartyMatchStatusExpiredEvent() {}
};

//PURPOSE
//  Dispatched when party match status is canceled
class rlXblPartyMatchStatusFoundEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyMatchStatusFoundEvent, RLXBL_PARTY_EVENT_MATCH_FOUND);
	rlXblPartyMatchStatusFoundEvent() {}
};

//PURPOSE
//  Dispatched when party has registered a GAME session (can also be fired on boot when party has session)
class rlXblPartyGameSessionRegisteredEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyGameSessionRegisteredEvent, RLXBL_PARTY_EVENT_GAME_REGISTERED);
	rlXblPartyGameSessionRegisteredEvent() {}
};

//PURPOSE
//  Dispatched when party has registered a MATCHMAKING session (can also be fired on boot when party has session)
class rlXblPartyMatchSessionRegisteredEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyMatchSessionRegisteredEvent, RLXBL_PARTY_EVENT_MATCH_REGISTERED);
	rlXblPartyMatchSessionRegisteredEvent() {}
};

//PURPOSE
//  Dispatched when party game session is ready
class rlXblPartyGameSessionReadyEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyGameSessionReadyEvent, RLXBL_PARTY_EVENT_GAME_SESSION_READY);
	rlXblPartyGameSessionReadyEvent(rlSessionInfo& gameSession, const rlGamerInfo target, int numPartyPlayers, rlGamerHandle* players, GameSessionReadyOrigin origin)
		: m_GameSession(gameSession)
		, m_Target(target)
		, m_NumPartyPlayers(numPartyPlayers)
		, m_Origin(origin)
	{
		for (int i = 0; i < numPartyPlayers; i++)
		{
			m_PartyPlayers[i] = players[i];
		}	
	}
		
	rlSessionInfo& m_GameSession;
	rlGamerInfo m_Target;
	rlGamerHandle m_PartyPlayers[RL_MAX_PARTY_SIZE];
	int m_NumPartyPlayers;
	GameSessionReadyOrigin m_Origin; 
};

//PURPOSE
//  Dispatched when party game session is unavailable (due to lack of privilege)
class rlXblPartyGameSessionUnavailableEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyGameSessionUnavailableEvent, RLXBL_PARTY_EVENT_GAME_SESSION_UNAVAILABLE);
	rlXblPartyGameSessionUnavailableEvent(const rlGamerInfo target)
		: m_Target(target)
	{

	}

	rlGamerInfo m_Target;
};

//PURPOSE
//  Dispatched when party game session is unavailable (due to lack of privilege)
class rlXblPartyGameSessionInvalidEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyGameSessionInvalidEvent, RLXBL_PARTY_EVENT_GAME_SESSION_INVALID);
	rlXblPartyGameSessionInvalidEvent()
	{

	}
};

//PURPOSE
//  Dispatched when party has game players available
class rlXblPartyGamePlayersAvailableEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblPartyGamePlayersAvailableEvent, RLXBL_PARTY_EVENT_GAME_AVAILABLE_PLAYERS);
	rlXblPartyGamePlayersAvailableEvent(rlXblSessionHandle& sessionHandle, int numAvailablePlayers, rlXblGamePlayer* players)
	{
		m_SessionHandle = sessionHandle;
		m_NumAvailablePlayers = numAvailablePlayers;

		for (int i = 0; i < numAvailablePlayers; i++)
		{
			m_AvailablePlayers[i] = players[i];
		}	
	}

	rlXblSessionHandle m_SessionHandle;
	rlXblGamePlayer m_AvailablePlayers[RL_MAX_PARTY_SIZE];
	int m_NumAvailablePlayers;
};

//PURPOSE
// Dispatched when the realtime activity services is connected or disconnected
class rlXblRealTimeActivityConnectionChangedEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblRealTimeActivityConnectionChangedEvent, RLXBL_RTA_EVENT_CONNECTION_CHANGED);
	rlXblRealTimeActivityConnectionChangedEvent()
	{
	}
};

//PURPOSE
// Dispatched when the friends list is updated
class rlXblFriendListChangedEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblFriendListChangedEvent, RLXBL_FRIEND_EVENT_LIST_CHANGED);
	rlXblFriendListChangedEvent(const int localGamerIndex)
	{
		m_LocalGamerIndex = localGamerIndex;
	}

	int m_LocalGamerIndex;
};

//PURPOSE
// Dispatched when the realtime activity service receives a device presence update
class rlXblFriendOnlineStatusChangedEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblFriendOnlineStatusChangedEvent, RLXBL_FRIEND_EVENT_ONLINE_STATUS_CHANGED);
	rlXblFriendOnlineStatusChangedEvent(u64 xuid, bool bIsOnline)
	{
		m_Xuid = xuid;
		m_bIsOnline = bIsOnline;
	}

	u64 m_Xuid;
	bool m_bIsOnline;
};

//PURPOSE
// Dispatched when the friend's title has changed
class rlXblFriendTitleStatusChangedEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblFriendTitleStatusChangedEvent, RLXBL_FRIEND_EVENT_TITLE_STATUS_CHANGED);
	rlXblFriendTitleStatusChangedEvent(u64 xuid, bool bIsInSameTitle)
	{
		m_Xuid = xuid;
		m_bIsInSameTitle = bIsInSameTitle;
	}

	u64 m_Xuid;
	bool m_bIsInSameTitle;
};

//PURPOSE
// Dispatched when a friend's statistics have changed
class rlXblFriendStatisticChangedEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblFriendStatisticChangedEvent, RLXBL_FRIEND_EVENT_STATISTIC_CHANGED);
	rlXblFriendStatisticChangedEvent(u64 xuid, const char * statistic)
	{
		m_Xuid = xuid;
		safecpy(m_StatName, statistic);
	}

	u64 m_Xuid;
	char m_StatName[RL_MAX_STAT_BUF_SIZE];
};

//PURPOSE
// Dispatched when the friend's session status has changed
class rlXblFriendSessionStatusChangedEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblFriendSessionStatusChangedEvent, RLXBL_FRIEND_EVENT_SESSION_CHANGED);
	rlXblFriendSessionStatusChangedEvent(const int localGamerIndex, u64 xuid, bool bIsInSession)
	{
		m_LocalGamerIndex = localGamerIndex;
		m_Xuid = xuid;
		m_bIsInSession = bIsInSession;
	}

	int m_LocalGamerIndex;
	u64 m_Xuid;
	bool m_bIsInSession;
};

//PURPOSE
// Dispatched when the user has accepted a system invite or joined via system presence
class rlXblInviteAcceptedEvent : public rlXblEvent
{
public:
	RLXBL_EVENT_DECL(rlXblInviteAcceptedEvent, RLXBL_INVITE_EVENT_ACCEPTED);
	rlXblInviteAcceptedEvent(
		rlGamerInfo& receiver, 
		rlSessionInfo& sessionInfo,
		rlGamerHandle& inviter,
		const bool isJoin)
	{
		m_gamerInfo = receiver;
		m_sessionInfo = sessionInfo; 
		m_inviter = inviter;
		m_IsJoin = isJoin;
	}

	rlGamerInfo		m_gamerInfo;
	rlSessionInfo	m_sessionInfo;
	rlGamerHandle	m_inviter;
	bool			m_IsJoin; 
};

class rlXblEventInviteUnavailableEvent : public rlXblEvent
{
public:

	RLXBL_EVENT_DECL(rlXblEventInviteUnavailableEvent, RLXBL_INVITE_EVENT_UNAVAILABLE);

	rlXblEventInviteUnavailableEvent(const rlPresenceInviteUnavailableReason reason)
		: m_Reason(reason)
	{
	}

	rlPresenceInviteUnavailableReason m_Reason;
};

//PURPOSE
//  Interface to Xbox Live on Durango.
class rlXbl
{

public:
	//PURPOSE
	//  Returns the region for which the game was built.
	static rlGameRegion GetGameRegion();

	//PURPOSE
	//  Returns the environment the native online service is in.
	//  On Xbox, this is based off the sandboxId. Any sandboxId
	//  other than CERT and RETAIL is considered DEV.
	static rlEnvironment GetNativeEnvironment();
	static rlEnvironment GetEnvironment();

    rlXbl();
    ~rlXbl();

    //PURPOSE
    //  Initialize the class.
    bool Init(sysMemAllocator* allocator);

    //PURPOSE
    //  Shut down the class.
    void Shutdown();

    //PURPOSE
    //  Update the class. Should be called once per frame.
    void Update();

    //PURPOSE
    //  Check for initialized
    bool IsInitialized();

	//PURPOSE
	//  Indicate that an invite was received
	static void FlagInviteAccepted(u64 xuid); 

	//PURPOSE
	//  Used to indicate that the app has actioned an invite
	//  We can also check whether we have an invite action pending
	static void FlagInviteActioned();
	static bool IsInviteActionPending();

	//PURPOSE
	//  Returns whether we have a pending platform invite (either awaiting 
	//  processing or being processed)
	static bool HasPendingInvite();

    //PURPOSE
    //  Handle a suspend PLM
    static void HandleSuspend();

	//AchievementManager* GetAchievementManager();
	IPlayers* GetPlayerManager();
	IPresence* GetPresenceManager();
	IProfiles* GetProfileManager();
	IPrivileges* GetPrivilegesManager();
	ISessions* GetSessionManager();
	ISystemUi* GetSystemUi();
	IAchievements* GetAchivementManager();
	IParty* GetPartyManager();
	IRealTimeActivity* GetRealtimeActivityManager();
	ICommerce* GetPlatformCommerceManager();
	IProfanityCheck* GetProfanityCheckManager();

	//PURPOSE
	//  Cancels task with given status
	bool CancelTask(netStatus* status); 

	//PURPOSE
	//  Add/remove a delegate that will be called with event notifications.
	void AddDelegate(rlXblEventDelegate* dlgt);
	void RemoveDelegate(rlXblEventDelegate* dlgt);
	void DispatchEvent(const rlXblEvent* e);

	//PURPOSE
	//  Sets whether we cancel in flight commerce tasks that use UI that have 
	//  been running without a UI for a set time
	static void SetShouldCancelCommerceTasks(const bool bShouldCancelCommerceTasks);
	static bool ShouldCancelCommerceTasks();

#if RSG_GDK
	//PURPOSE
	//  Sets the grace period after a resume at which we attempt to process an invite
	static void SetInviteResumeGracePeriodMs(const unsigned gracePeriodMs);
#endif

private:
	bool m_Initialized;
};

//Global instance
extern rlXbl g_rlXbl;

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_INTERFACE_H
