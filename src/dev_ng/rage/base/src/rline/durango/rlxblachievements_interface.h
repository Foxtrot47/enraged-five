// 
// rlxblachievements_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_ACHIEVEMENTS_INTERFACE_H
#define RLXBL_ACHIEVEMENTS_INTERFACE_H

#if RSG_DURANGO

#include "atl/functor.h"

namespace rage
{

class rlGamerHandle;
class netStatus;
class rlAchievementInfo;

#define XB1_STAT_MAX_LENGTH 256

//PURPOSE
// Interface to the system UI on Durango
class IAchievements
{
public:

	static const int FULL_UNLOCK_PROGRESS = -1;

	//PURPOSE
	// Gets the list of achievements
	//PARAMS
	// localGamerIndex - Index of the local gamer requesting the achievements
	virtual bool ReadAchievements(const int localGamerIndex, rlAchievementInfo* achievements, const int maxAchievements, int* numAchievements, netStatus* status) = 0;

	//PURPOSE
	// Sets the function that writes XB1 Events for achievements
	// localGamerIndex - Index of the local gamer requesting the achievements
	typedef Functor3Ret<bool, int, int, int> AchievementEvent;
	virtual void SetAchievementEventFunc(const AchievementEvent& functor) = 0;

	//PURPOSE
	// Writes an achievement using the achievement event functor
	virtual bool WriteAchievement(const int localGamerIndex, const int achId) = 0;

	//PURPOSE
	// Gets the user stat required for an achievement
	virtual bool ReadStatInt(const int localGamerIndex, const char * statName, const int statNameLength, int* outInt, netStatus* status) = 0;

	//PURPOSE
	// Gets the user stat required for an achievement
	virtual bool ReadStatDouble(const int localGamerIndex, const char * statName, const int statNameLength, double* outDouble, netStatus* status) = 0;

};

} // namespace rage


#endif // RSG_DURANGO
#endif // RLXBL_ACHIEVEMENTS_INTERFACE_H