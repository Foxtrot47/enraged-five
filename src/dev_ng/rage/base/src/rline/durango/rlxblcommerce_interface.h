#ifndef XBL_COMMERCE_INTERFACE_H
#define XBL_COMMERCE_INTERFACE_H

#include "atl/array.h"
#include "atl/string.h"
#include "net/status.h"

namespace rage
{

const int MAX_PRODUCT_DETAILS_TO_FETCH = 10;

struct cCommercePlatformItem
{
	cCommercePlatformItem() :
		m_Price(-1.0f),
		m_IsConsumable(false),
		m_IsOwned(false)
	{

	}

	atString	m_Name;
	atString	m_Id;
	atString	m_ProductId;
	atString	m_AvailablilityId;
	atString	m_SignedOfferId;
	atString	m_DisplayPrice;
	atString	m_SandboxId;
	atString	m_CurrencyCode; 
	float		m_Price;
	bool		m_IsConsumable;
	bool		m_IsOwned;
};

class cCommercePlatformDetails
{
public:
	cCommercePlatformDetails()
	{
		m_ItemArray.Reserve(INITIAL_ITEM_ARRAY_SIZE);
	}

	~cCommercePlatformDetails()
	{
		m_ItemArray.Reset();
	}

	atArray<cCommercePlatformItem>& GetItemArray() {return m_ItemArray;}
	const atArray<cCommercePlatformItem>& GetItemArray() const {return m_ItemArray;}

	void Reset() { m_ItemArray.Reset(); m_ItemArray.Reserve(INITIAL_ITEM_ARRAY_SIZE); }

private:

	enum 
	{
		INITIAL_ITEM_ARRAY_SIZE = 32
	};

	atArray<cCommercePlatformItem> m_ItemArray;
};

enum eCommercePlatformItemType
{
	COMMERCE_INVENTORY_ITEM_TYPE_CONSUMABLE,
	COMMERCE_INVENTORY_ITEM_TYPE_DURABLE,
	COMMERCE_INVENTORY_ITEM_TYPE_INVALID
};

class cCommercePlatformInventoryItem
{
public:
	cCommercePlatformInventoryItem() :
		m_ConsumableBalance(0),
		m_ItemType(COMMERCE_INVENTORY_ITEM_TYPE_INVALID)
	{

	}

	void SetProductId( const char* newProductId ) { m_ProductId = newProductId; }
	const char* GetProductId() const { return m_ProductId.c_str(); }

	void SetConsumableBalance( unsigned int newConsumableBalance ) { m_ConsumableBalance = newConsumableBalance; }
	unsigned int GetConsumableBalance() const { return m_ConsumableBalance; }

	void SetItemType( eCommercePlatformItemType newItemType ) { m_ItemType = newItemType; }
	eCommercePlatformItemType GetItemType() const { return m_ItemType; }

    void SetOwned( bool ownerShip ) { m_IsOwned = ownerShip; }
    bool GetOwned() const { return m_IsOwned; }

private:
	atString	m_ProductId;
	u32			m_ConsumableBalance;
    bool        m_IsOwned;
	eCommercePlatformItemType	m_ItemType;
};

class cCommercePlatformInventoryDetails
{
public:
	cCommercePlatformInventoryDetails()
	{
		m_InventoryItemArray.Reserve(INITIAL_INVENT_ITEM_ARRAY_SIZE);
	}

	~cCommercePlatformInventoryDetails()
	{
		m_InventoryItemArray.Reset();
	}


	void Reset()
	{
		m_InventoryItemArray.Reset();
		m_InventoryItemArray.Reserve(INITIAL_INVENT_ITEM_ARRAY_SIZE);
	}

	atArray<cCommercePlatformInventoryItem>& GetInventoryItemArray() {return m_InventoryItemArray;}
	const atArray<cCommercePlatformInventoryItem>& GetInventoryItemArray() const {return m_InventoryItemArray;}

private:

	enum 
	{
		INITIAL_INVENT_ITEM_ARRAY_SIZE = 32
	};

	atArray<cCommercePlatformInventoryItem> m_InventoryItemArray;
};

class rlXblPurchaseAccessToken
{
public:
	static const unsigned MAX_TOKEN_LEN = 2048;
	char m_Token[MAX_TOKEN_LEN];
};

class rlXblCommerceStoreId
{
public:
	static const unsigned MAX_STORE_ID_LENGTH = 2048;
	char m_StoreId[MAX_STORE_ID_LENGTH];
};

class ICommerce
{
public:

	virtual bool GetCatalog(const int localGamerIndex, const char* rootProductId, cCommercePlatformDetails* details,  eCommercePlatformItemType itemType, netStatus* status) = 0;

	virtual bool GetCatalogDetails( const int localGamerIndex, cCommercePlatformDetails* details, int startIndex, int numToFetch, netStatus* status  ) = 0;

	virtual bool DoProductPurchaseCheckout(const int localGamerIndex, const char* availabilityId, netStatus* status) = 0;

	virtual bool DoCodeRedemption( int localGamerIndex, netStatus* status ) = 0;

	virtual bool ConsumeFromItem( const int localGamerIndex, const char* idToConsume, int numToConsume, netStatus* status ) = 0;

	virtual bool GetInventoryItems( const int localGamerIndex, cCommercePlatformInventoryDetails* details, eCommercePlatformItemType itemType, netStatus* status ) = 0;

#if RSG_GDK
	virtual bool GetUserPurchaseId(const int localGamerIndex, const char* serviceTicket, const char* publisherId, rlXblCommerceStoreId* storeId, netStatus* status) = 0;
#endif
};

}

#endif