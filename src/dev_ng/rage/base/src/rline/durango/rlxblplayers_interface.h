// 
// rlxblplayers_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PLAYERS_INTERFACE_H
#define RLXBL_PLAYERS_INTERFACE_H

#if RSG_DURANGO

#include "atl/array.h"
#include "rline/rl.h"
#include "rline/rlfriend.h"
#include "rline/rlfriendsreader.h"

namespace rage
{

// fwd declarations
class rlFriend;
class rlFriendsPage;
class netStatus;
class rlFriendsReader;
struct rlFriendsReference;
enum rlFriendsReader::FriendReaderType;

struct PlayerList
{
	static const unsigned DEFAULT_LIMIT = 1000;
	PlayerList() : m_Limit(DEFAULT_LIMIT) {}

	// list of XUIDs
	// we use an atArray so that we can allocate more elements as needed but 
	// have a configurable limit so that we have an upper boundary on this
	unsigned m_Limit; 
	atArray<u64> m_Players; 
};

//PURPOSE
//  Interface to Xbox Live social relationships (friends, blocked players, etc.) on Durango.
class IPlayers
{
public:
	// PURPOSE
	// Reads a list of friends with the given parameters
	virtual bool GetFriends(const int localGamerIndex, const unsigned firstFriendIndex, rlFriend* friends, const unsigned maxFriends,
							unsigned* numFriends, unsigned* totalFriends, int friendFlags, netStatus* status) = 0;

	// PURPOSE
	// Updates a list of friend's presence and then queries the session status for all online friends
    virtual bool UpdateFriends(const int localGamerIndex, rlFriendsPage* page, unsigned startIndex, unsigned numFriends, netStatus* status) = 0;

	// PURPOSE
	// Updates a list of friends presence without refreshing the entire list
	virtual bool UpdateFriendsPresence(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status) = 0;

	//PURPOSE
	//	Retrieves friend info for an array of friends without refreshing the entire list
	virtual bool GetFriendInfo(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status) = 0;

	//PURPOSE
	// Retrieves the list of social relationships.
	//	Fills two arrays: one sorted by status, the other sorted by Id.
	virtual bool GetSocialRelationships(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, netStatus* status) = 0;

	//PURPOSE
	// Returns the presence of the People moniker
	//	Updates the in arrays: one sorted by status, the other sorted by Id
	virtual bool GetPeoplePresence(const int localGamerIndex, atArray<rlFriendsReference*>* friendRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, netStatus* status) = 0;

	// PURPOSE
	// Checks the size of the friend's list for changes
	virtual bool CheckForListChanges(const int localGamerIndex, netStatus* status) = 0;

	// PURPOSE
	// Gets the session status for a list of friends
	virtual bool GetFriendsSessions(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status) = 0;

	// PURPOSE
	// Gets the presence session status for a list of friends
	virtual bool GetFriendsPresenceSessions(const int localGamerIndex, rlFriend* friends, unsigned numFriends, netStatus* status) = 0;

	// PURPOSE
	// Gets the players avoid / block list
	virtual bool GetAvoidList(const int localGamerIndex, PlayerList* playerList, netStatus* status) = 0;

	// PURPOSE
	// Gets the players mute list
	virtual bool GetMuteList(const int localGamerIndex, PlayerList* playerList, netStatus* status) = 0;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PLAYERS_INTERFACE_H
