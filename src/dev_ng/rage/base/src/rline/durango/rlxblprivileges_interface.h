// 
// rlxblprivileges_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PRIVILEGES_INTERFACE_H
#define RLXBL_PRIVILEGES_INTERFACE_H

#if RSG_DURANGO

namespace rage
{

//PURPOSE
//  Interface to Xbox Live privileges on Durango.
class IPrivileges
{
public:
	//PURPOSE
	// Indicates the result of a privilege check. 
	enum ePrivilegeCheckResult
	{
		PRIV_RESULT_ABORTED           = 2 //The check was aborted. 
		,PRIV_RESULT_BANNED           = 4 //The user is banned. 
		,PRIV_RESULT_NOISSUE          = 0 //There are no privilege issues with the user. 
		,PRIV_RESULT_PURCHASEREQUIRED = 1 //The user must purchase the title for access. 
		,PRIV_RESULT_RESTRICTED       = 8 //The user is restricted from access. 
		, PRIV_RESULT_DEFAULT = PRIV_RESULT_BANNED
	};

#if !__NO_OUTPUT
	//PURPOSE
	//  Retrieve friendly name for the possible privileges result checks.
	static const char* GetPrivilegeCheckResultName(const ePrivilegeCheckResult privResult)
	{
		switch (privResult)
		{
		case PRIV_RESULT_ABORTED: return "ABORTED";
		case PRIV_RESULT_BANNED: return "BANNED";
		case PRIV_RESULT_NOISSUE: return "NOISSUE";
		case PRIV_RESULT_PURCHASEREQUIRED: return "PURCHASEREQUIRED";
		case PRIV_RESULT_RESTRICTED: return "RESTRICTED";
		}

		return "INVALID";
	}
#endif // !__NO_OUTPUT

public:
	//PURPOSE
	// Begin an asynchronous check to see if the specified local user has the specified privileges.
	virtual bool CheckPrivileges(const int localGamerIndex, const int privilegeTypeBitfield, const bool attemptResolution) = 0;

	//PURPOSE
	// Check if the current privilege check has a result.
	virtual bool IsPrivilegeCheckResultReady() const = 0;

	//PURPOSE
	// Check if a privilege check is in progress.
	virtual bool IsPrivilegeCheckInProgress() const = 0;

	//PURPOSE
	// Check if the current privilege check was successful.
	virtual bool IsPrivilegeCheckSuccessful() const = 0;

	//PURPOSE
	// Set that the privilege check result is no longer needed. 
	virtual void SetPrivilegeCheckResultNotNeeded() = 0;

	//PURPOSE
	// Returns the result of the Asynch call privilege check. 
	virtual ePrivilegeCheckResult GetPrivilegeCheckResult() const = 0;

	//PURPOSE
	// Return TRUE if a privilege is granted. This is a faster call and the result
	//  can only be used if the return value is TRUE.
	virtual bool HasPrivilege(const int localGamerIndex, const int privilegeId) const = 0;

	//PURPOSE
	// Cancels the current privilege task.
	virtual void OnSignOut() = 0;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PRIVILEGES_INTERFACE_H
