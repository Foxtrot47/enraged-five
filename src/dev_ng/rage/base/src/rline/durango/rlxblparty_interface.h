// 
// rlxblparty_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PARTY_INTERFACE_H
#define RLXBL_PARTY_INTERFACE_H

#if RSG_DURANGO

#include "rline/rl.h"
#include "rline/rlgamerinfo.h"
#include "rline/rlsessioninfo.h"

namespace rage
{

// best guess at where the game session ready originates from
enum GameSessionReadyOrigin
{
	ORIGIN_UNKNOWN = 0,
	ORIGIN_FROM_BOOT,
	ORIGIN_FROM_INVITE,
	ORIGIN_FROM_MULTIPLAYER_JOIN,
	ORIGIN_FROM_JVP,
    ORIGIN_MAX,
};

//PURPOSE
//  Contains information about an Xbox Live party member
struct rlXblPartyMemberInfo
{
	rlXblPartyMemberInfo() 
	{
		m_GamerName[0] = '\0';
		m_PartyJoinTime = 0;
		m_PreviousPartyJoinTime = 0;
		m_TitleId = 0;
		m_IsInSession = false;
		m_IsTalking = false;
		m_IsInPartyVoice = false;
		m_Available = false;
	}

	rlGamerHandle m_GamerHandle;
	rlDisplayName m_GamerName;
	unsigned m_TitleId;
	rlSessionInfo m_SessionInfo;    //Only valid if m_IsInSession is true;
	u64 m_PreviousPartyJoinTime;
	u64 m_PartyJoinTime;

	bool m_IsInSession : 1;
	bool m_IsTalking : 1;
	bool m_IsInPartyVoice : 1;
	bool m_Available : 1;
};

struct rlXblGamePlayer
{
	u64 LastInvitedTime;
	u64 XboxUserId;

	// uint64 PartyJoinTime; (This is not used, but is part of the XDK GamePlayer^ object)
};

//PURPOSE
//  Interface to Xbox Live Party on Durango.
class IParty
{
public:

    //PURPOSE
    // Clears the party and all members
    virtual void ClearParty(const bool bFireRosterChangedEvent) = 0;

	//PURPOSE
	// Flag for party refresh 
	virtual void FlagRefreshParty() = 0;

	//PURPOSE
	// Add all local users to party
	// Pass NULL to status to create a fire and forget task
	virtual bool AddLocalUsersToParty(const int localGamerIndex, netStatus* status) = 0;

	//PURPOSE
	// Remove all local users to party
	// Pass NULL to status to create a fire and forget task
	virtual bool RemoveLocalUsersFromParty(const int localGamerIndex, netStatus* status = NULL) = 0;

	//PURPOSE
	// Sends a party invite to the target users
	virtual bool InviteToParty(const int localGamerIndex, const rlGamerHandle* targetUsers, int numUsers, netStatus* status) = 0;

	//PURPOSE
	// Get the number of party members
	virtual int GetPartySize() = 0;
	
	//PURPOSE
	// Returns whether we are in a party or not
	virtual bool IsInParty() = 0;
	virtual bool IsInSoloParty() = 0;

	//PURPOSE
	// Party chat checks
	virtual bool IsPartyChatActive() const = 0;
	virtual bool IsPartyChatSuppressed() const = 0;

	//PURPOSE
	// Suppress the game session ready notification
	virtual void SuppressGameSessionReadyNotification(bool bSuppress) = 0;

	//PURPOSE
	// Register game session for party
	virtual bool RegisterGameSessionToParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status) = 0;
	virtual bool DisassociateGameSessionFromParty(const int localGamerIndex, const rlXblSessionHandle* sessionHandle, netStatus* status) = 0;
	virtual bool IsRegisteringGameSession() = 0;

	//PURPOSE
	// Query game session
	virtual bool HasRegisteredGameSession() = 0;
	virtual rlXblSessionHandle* GetRegisteredGameSession(rlXblSessionHandle* sessionHandle) = 0;
	virtual bool IsRegisteredGameSession(const rlXblSessionHandle* sessionHandle) = 0;
	virtual bool ValidateGameSession(const int localGamerIndex) = 0; 

	// PURPOSE
	// Switches the party title to the acting user's current title. 
	virtual bool SwitchPartyTitle(const int localGamerIndex, netStatus* status) = 0;

	//PURPOSE
	// Get a party member based on the index
	virtual rlXblPartyMemberInfo* GetPartyMember(int index) = 0;

	// PURPOSE
	// Fills session slots with party members for XR-066
	virtual bool FillOpenSessionSlots(const int localGamerIndex, const rlSessionInfo& sessionInfo, int numSlots, rlGamerHandle* availablePlayers, u64* lastInviteTimes, int numAvailablePlayers, const rlGamerHandle& excludeGamer) = 0;

	//PURPOSE
	//  Party tunables
	virtual void SetLeavePartyOnInvalidGameSession(const bool bLeavePartyOnInvalidGameSession) = 0;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PARTY_INTERFACE_H
