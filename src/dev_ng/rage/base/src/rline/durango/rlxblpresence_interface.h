// 
// rlxblpresence_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PRESENCE_INTERFACE_H
#define RLXBL_PRESENCE_INTERFACE_H

#if RSG_DURANGO

namespace rage
{

//PURPOSE
//  Interface to Xbox Live local on Durango.
class IPresence
{
public:
	//PURPOSE
	//  Returns true if the specified local user is signed in locally.
	virtual bool IsSignedIn(int localGamerIndex) = 0;

	//PURPOSE
	//  Returns true if the specified local user is signed into Live.
	virtual bool IsOnline(int localGamerIndex) = 0;

	//PURPOSE
	//  Returns true if any local user is signed into Live.
	virtual bool IsOnline() = 0;

	//PURPOSE
	//  Returns the name of the specified local user.
	virtual const char* GetName(int localGamerIndex) = 0;

	//PURPOSE
	//  Returns the display name of the specified local user.
	virtual const char* GetDisplayName(int localGamerIndex) = 0;

	//PURPOSE
	//  Returns the XUID of the specified local user, or 0 if they're not online.
	virtual u64 GetXboxUserId(int localGamerIndex) = 0;

	//PURPOSE
	//  Returns TRUE if the xbox local gamer is valid and ready for use.
	virtual bool HasValidXboxUser(int localGamerIndex) = 0;

	//PURPOSE
	//  Fills the user hash of the specified local user to 'hashBuffer'
	//	Returns TRUE if successful.
	virtual bool GetXboxUserHash(int localGamerIndex, wchar_t* hashBuffer, unsigned maxLen) = 0;

	//PURPOSE
	// Set the rich presence string of the specified local user
	virtual bool SetRichPresence(const rlGamerInfo& gamerInfo, const char* presenceString, netStatus* status) = 0;

	//PURPOSE
	//	Get the rich presence string of the targetXuid using the context of the local gamer index
	virtual bool GetRichPresence(const int localGamerIndex, u64 targetXuid, netStatus* status) = 0;

	//PURPOSE
	// Returns true if a user is a guest/sponsored account
	virtual bool IsGuest(const int localGamerIndex) = 0;
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PRESENCE_INTERFACE_H
