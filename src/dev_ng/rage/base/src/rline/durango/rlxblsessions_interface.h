// 
// rlxblsessions_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_SESSIONS_INTERFACE_H
#define RLXBL_SESSIONS_INTERFACE_H

#if RSG_DURANGO

#include "rline/rlmatchingattributes.h"
#include "data/bitbuffer.h"

namespace rage
{

class rlSessionInfo;
class rlSessionToken;
class rlGamerHandle;
class netPeerAddress;

class rlXblSessionName
{
public:
	static bool CreateUniqueName(rlXblSessionName& rlGuid);
	rlXblSessionName();

	//PURPOSE
	//	Clears data
	void Clear();

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = 16;

	bool operator==(const rlXblSessionName& that) const;
	bool operator!=(const rlXblSessionName& that) const;

	//PURPOSE
	//  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
	//  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

	const wchar_t* ToWideString() const;
	bool FromWideString(const wchar_t* s);

	const u8* GetGuid() const { return m_Guid; }

	//PURPOSE
	//	Returns true if this object contains a valid session name
	bool IsValid() const;

private:
	void SetWideString();

	// example: 98e58b97-b3a7-4e89-9b47-5a696f992a38
	// making this 38 chars instead of 37 because of a bug in formatf
	wchar_t m_GuidString[38];
	u8 m_Guid[16];
};

struct rlXblSessionHandle
{
	rlXblSessionHandle();
	rlXblSessionHandle(const wchar_t* serviceConfigurationId,
					   const wchar_t* sessionName,
					   const wchar_t* sessionTemplateName);

	void Init(const wchar_t* serviceConfigurationId,
			  const wchar_t* sessionName,
			  const wchar_t* sessionTemplateName);

	void Clear();
	
	static const unsigned MAX_SCID_BUF_SIZE = 38;
	static const unsigned MAX_TEMPLATE_NAME_BUF_SIZE = 16;

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = datMaxBytesNeededForString<MAX_SCID_BUF_SIZE>::COUNT + // m_ServiceConfigurationId (Ascii) // we can have multiple SCID per title if there are different SKUs
													   rlXblSessionName::MAX_EXPORTED_SIZE_IN_BYTES +
													   datMaxBytesNeededForString<MAX_TEMPLATE_NAME_BUF_SIZE>::COUNT; // m_SessionTemplateName (Ascii)

	bool operator==(const rlXblSessionHandle& that) const;
	bool operator!=(const rlXblSessionHandle& that) const;

	//PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
	//  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

	//PURPOSE
	//	Has valid content
	bool IsValid() const;
	
	wchar_t m_ServiceConfigurationId[MAX_SCID_BUF_SIZE];
	rlXblSessionName m_SessionName;
	wchar_t m_SessionTemplateName[MAX_TEMPLATE_NAME_BUF_SIZE];
};

//PURPOSE
//  Interface to Xbox Live multiplayer sessions and matchmaking on Durango.
class ISessions
{
public:
	static const int MAX_SESSIONS_BATCH_SIZE = 16;

	enum
	{
		/*
		Each member in a session has a status, which is one of these values:

		Reserved: The user has been added to the session by someone else, but hasn't yet joined the session themselves. 
		Inactive: The user has joined the session but is not currently engaged in the game. 
		Ready: The user has joined the session and is trying to get into the game. This usually means the shell has joined the user on behalf of the title and the title is being launched. 
		Active: The user has joined the session and is currently engaged in the game. 

		Timeouts
		Sessions can be changed by timers and other external events. The timeouts object in MPSD provides a basic mechanism to manage session lifetime and members. 
		Zero is allowed and means to immediately time out.

		Active Member Decay
		Active members are automatically marked inactive when MPSD detects that the user is no longer engaged in the title.
		This can happen, for example, if the Presence times out the user record. This mechanism is just a backstop - titles are still required to proactively
		mark members as inactive (or remove them from the session) when they leave or switch away from the title, sign out, or otherwise become disengaged. 
		*/

		DEFAULT_ASYNC_SESSION_TIMEOUT_MS = (30 * 1000),	// the async create/join/leave operation timeouts
		DEFAULT_MEMBER_RESERVED_TIMEOUT_MS = (30 * 1000),
		DEFAULT_MEMBER_INACTIVE_TIMEOUT_MS = (60 * 2 * 1000),	// remove the member from the session after being inactive for too long (i.e. MS's presence system detects they've gone offline, etc.)
																// I don't know if they set a player to inactive just for entering constrained mode however, so allow 2 minutes before removing the player automatically.

		DEFAULT_MEMBER_READY_TIMEOUT_MS = (90 * 1000),
		DEFAULT_SESSION_EMPTY_TIMEOUT_MS = (0 * 1000),			// 0 means delete the session as soon as everyone has left
	};

    //PURPOSE
    //  Creates a new session as the host.
    //PARAMS
    //  localGamerIndex - Index of local gamer creating the session.
    //  netMode         - RL_NETMODE_ONLINE or RL_NETMODE_LAN.
    //  maxPubSlots     - Maximum number of public slots.
    //  maxPrivSlots    - Maximum number of private slots.
    //  attrs           - Matchmaking attributes.
    //  createFlags     - Session creation flags.  See rlSession::CreateFlags.
    //  presenceFlags   - Session creation flags.  See rlSession::PresenceFlags.
    //  hSession        - On successful completion will contain the session handle.
    //  sessionInfo     - On successful completion will contain new session info.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Sessions with no public slots are considered private sessions and
    //  will not be advertised on matchmaking.  Private sessions thus
    //  require no server resources.  Consider making sessions private when
    //  possible.  For example, invite-only sessions, such as those used
    //  for parties, should be private.
    //
    //  Matching attributes cannot be nil (unset) for sessions with public
    //  slots.  Private sessions are permitted to have nil attributes, but
    //  they must still specify a valid game mode and game type.
    //
    //  This is an asynchronous operation.  The handle, arbCookie, sessionInfo,
    //  and status parameters must remain valid until the operation completes.
    //  They must not be stack variables.
    virtual bool HostSession(const int localGamerIndex,
							 const rlNetworkMode netMode,
							 const unsigned maxPubSlots,
							 const unsigned maxPrivSlots,
							 const rlMatchingAttributes& attrs,
							 const unsigned createFlags,
							 const unsigned presenceFlags,
							 rlXblSessionHandle** hSession,
							 rlSessionInfo* sessionInfo,
							 netStatus* status) = 0;

	//PURPOSE
	//  Creates a new session as a guest.
	//PARAMS
	//  localGamerIndex - Index of local gamer creating the session.
	//  sessionInfo     - Info for an existing session hosted by someone else.
	//  netMode         - RL_NETMODE_ONLINE or RL_NETMODE_LAN.
	//  createFlags     - Session creation flags.  See rlSession::CreateFlags.
	//  presenceFlags   - Session creation flags.  See rlSession::PresenceFlags.
	//  maxPublicSlots  - Maximum public slots
	//  maxPrivateSlots - Maximum private slots
	//  hSession        - On successful completion will contain the session handle.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  This is an asynchronous operation.  The handle and status parameters
	//  must remain valid until the operation completes.  They must not be stack
	//  variables.
	virtual bool CreateSession(const int localGamerIndex,
							   const rlSessionInfo& sessionInfo,
							   const rlNetworkMode netMode,
							   const unsigned createFlags,
							   const unsigned presenceFlags,
							   const unsigned maxPublicSlots,
							   const unsigned maxPrivateSlots,
							   rlXblSessionHandle** hSession,
							   netStatus* status) = 0;


	//PURPOSE
	//  Destroys a session.
	//PARAMS
	//  hSession        - Handle of an existing session.
	//  localGamerIndex - Local owner of the session.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  This is an asynchronous operation.  The status parameter must remain
	//  valid until the operation completes.  It must not be a stack variable.
	virtual bool DestroySession(const int localGamerIndex,
								const rlXblSessionHandle* hSession,
								netStatus* status) = 0;

	//PURPOSE
    //  Joins gamers to the local instance of a session.
    //PARAMS
	//  localGamerHandle - Handle to the local joining gamer (if exists)
	//  createFlags		- The sessions' created flags used to determine party eligibility
    //  hSession        - Handle of an existing session.
    //  joiners         - Array of gamers to join.
    //  slotTypes       - Slot types in which they wish to join.
    //  numJoiners      - Number of joiners.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Call this function to join local as well as remote gamers to the
    //  local instance of the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    virtual bool JoinSession(const rlGamerHandle& localGamerHandle,
							 const int createFlags,
							 const rlXblSessionHandle* hSession,
							 const rlGamerHandle* joiners,
							 const rlSlotType* slotTypes,
							 const unsigned numJoiners,
							 netStatus* status) = 0;

    //PURPOSE
    //  Leaves the specified gamer from the specified session.
    //PARAMS
	//  localGamerIndex - Index of local gamer leaving the session.
    //  hSession        - Handle of an existing session.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Call this function to leave the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
	virtual bool LeaveSession(const int localGamerIndex,
							  const rlXblSessionHandle* hSession,
							  netStatus* status) = 0;

    //PURPOSE
    //  Changes attributes on the local instance of the session.
    //PARAMS
    //  localGamerIndex - Index of local owner of the session (not necessarily the host).
    //  hSession        - Handle of an existing session.
    //  netMode         - Network mode used to create the session.
    //  newMaxPubSlots  - New maximum number of public slots.
    //  newMaxPrivSlots - New maximum number of private slots.
    //  newAttrs        - New attributes.
    //  createFlags     - Flags used to create the session.
    //  newPresenceFlags- New presence flags.
    //  status          - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the operation.
    //NOTES
    //  Attributes with nil values will not be changed on the session.
    //
    //  This is an asynchronous operation.  The status parameter must remain
    //  valid until the operation completes.  It must not be a stack variable.
    virtual bool ChangeSessionAttributes(const int localGamerIndex,
										 const rlXblSessionHandle* hSession,
										 const rlNetworkMode netMode,
										 const unsigned newMaxPubSlots,
										 const unsigned newMaxPrivSlots,
										 const rlMatchingAttributes& newAttrs,
										 const unsigned createFlags,
										 const unsigned newPresenceFlags,
										 netStatus* status) = 0;

    //PURPOSE
    //  Changes who is the session host.
    //PARAMS
    //  hSession        - Handle of an existing session.
    //  localHostIndex  - Index of local gamer that is to become the new host.
    //                    Must be -1 if the new host is remote.
    //  newSessionInfo  - Session info for the new session.  If a local
    //                    gamer is to be the new host then newSessionInfo
    //                    will be populated with info for the migrated session.
    //                    if a remote gamer is the new host then newSessionInfo
    //                    must contain session info received from that gamer.
    //  status          - Optional.  Can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  The status and newSessionInfo
    //  parameters must remain valid until the operation completes.
    //  They must not be stack variables.
	virtual bool MigrateHost(
		rlXblSessionHandle** hSession,
		const int localGamerIndex,
		const int localHostIndex,
		const int createFlags,
		const rlSessionInfo& sessionInfo,
		rlSessionInfo* newSessionInfo,
		netStatus* status) = 0;

	//PURPOSE
	//  Write custom properties for the session (temporary)
	//PARAMS
	//  hSession        - Handle of an existing session.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  This is an asynchronous operation.  The status and newSessionInfo
	//  parameters must remain valid until the operation completes.
	//  They must not be stack variables.
	virtual bool WriteCustomProperties(const rlXblSessionHandle* hSession,
									   const rlSessionToken& sessionToken,
									   netStatus* status) = 0;

	//PURPOSE
	//  Sets timeout policies for all sessions - see defaults and descriptions in rlxblsessions.h
	virtual void SetTimeoutPolicies(const unsigned asyncSessionTimeoutMs, const unsigned memberReservedTimeoutMs, const unsigned memberInactiveTimeoutMs, 
									const unsigned memberReadyTimeoutMs, const unsigned sessionEmptyTimeoutMs) = 0;

	//PURPOSE
	//  Session tunables
	virtual void SetRunValidateHost(const bool bRunValidateHost) = 0;
	virtual void SetConsiderInvalidHostAsInvalidSession(const bool bConsiderInvalidHostAsInvalidSession) = 0;

#if !__NO_OUTPUT
	//PURPOSE
	//  Retrieves the latest copy of the session from MPSD and logs out
	//	all pertinent details
	//PARAMS
	//  hSession        - Handle of an existing session.
	//  status          - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the operation.
	//NOTES
	//  This is an asynchronous operation.  The status and newSessionInfo
	//  parameters must remain valid until the operation completes.
	//  They must not be stack variables.
	virtual bool LogSessionDetails(const rlXblSessionHandle* hSession,
								   netStatus* status) = 0;
#endif
};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_SESSIONS_INTERFACE_H
