// 
// rlXblProfanityCheck_interface.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_PROFANITYCHECK_INTERFACE_H
#define RLXBL_PROFANITYCHECK_INTERFACE_H

#if RSG_DURANGO

namespace rage
{

class netStatus;

//PURPOSE
//  Interface to the user statistics on Durango.
class IProfanityCheck
{
public:

	//PURPOSE
	// Check text profanity.
	virtual bool ProfanityCheck(const int localGamerIndex, const char* text, char* profaneWord, unsigned profaneWordBufferSize, unsigned* hasProfaneWord, netStatus* status) = 0;

};

} // namespace rage

#endif  //RSG_DURANGO

#endif  //RLXBL_PROFANITYCHECK_INTERFACE_H

