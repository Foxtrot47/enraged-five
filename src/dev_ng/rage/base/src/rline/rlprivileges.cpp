// 
// rline/rlprivileges.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "rlprivileges.h"
#include "rldiag.h"
#include "rline/rlpresence.h"
#include "diag/seh.h"
#include "net/nethardware.h"
#include "system/nelem.h"


namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, privileges);
#undef __rage_channel
#define __rage_channel rline_privileges

#define RL_IS_VALID_PRIVILEGE_ID(i) ((i) >= 0 && (i) < PRIVILEGE_MAX)
#define RL_VERIFY_PRIVILEGE_ID(i) rlVerifyf(RL_IS_VALID_PRIVILEGE_ID(i), "Invalid privilege id: %d", i)


//////////////////////////////////////////////////////////////////////////
//  rlPrivileges::PrivilegeWorker
//////////////////////////////////////////////////////////////////////////

#if RSG_DURANGO

rlPrivileges::PrivilegeWorker::PrivilegeWorker() 
	: m_Pending(-1)
	, m_Refresh(true)
{
	for (int i=0; i<PRIVILEGE_MAX; i++)
	{
		m_Value[i] = IPrivileges::PRIV_RESULT_DEFAULT;
		m_RefreshValue[i] = true;
	}
}

bool 
rlPrivileges::PrivilegeWorker::Update(const int actingUserIndex)
{
	if (!Refresh() && !IsPending())
		return false;

	//Must be a valid Gamer index.
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(actingUserIndex))
		return false;

	//Gamer must be signed in locally.
	rlGamerInfo gamerInfo;
	if(!rlPresence::GetGamerInfo(actingUserIndex, &gamerInfo) || !gamerInfo.IsSignedIn())
		return false;

	//We need a valid xbox user to check if the privilege is granted in HasPrivilege()
	if (!g_rlXbl.GetPresenceManager()->HasValidXboxUser(actingUserIndex))
		return false;

	IPrivileges* privMgr = g_rlXbl.GetPrivilegesManager();
	if (!rlVerify(privMgr))
		return false;

	//Get the value for the Asynch privilege check.
	if (privMgr->IsPrivilegeCheckResultReady() && rlVerify(IsPending()))
	{
		const int privId = m_Pending;
		m_Pending = -1;

		rlDebug1("Privileges Worker - End Asynch call for Gamer '%d', Privilege '%s'='%s'.", actingUserIndex,  rlPrivileges::GetPrivilegeName(privId), privMgr->GetPrivilegeCheckResultName(privMgr->GetPrivilegeCheckResult()));

		m_Value[privId] = privMgr->GetPrivilegeCheckResult();

		rlPrivileges::SetPrivilegeCheckResultNotNeeded();
	}

	//start Asynch privilege check.
	if (Refresh() && !privMgr->IsPrivilegeCheckInProgress())
	{
		m_Refresh = false;

		for (int i=0; i<rlPrivileges::PRIVILEGE_MAX; ++i)
		{
			if (m_RefreshValue[i] && privMgr->HasPrivilege(actingUserIndex, i))
			{
				m_RefreshValue[i] = false;
				m_Value[i]        = IPrivileges::PRIV_RESULT_NOISSUE;
			}
		}

		for (int i=0; i<rlPrivileges::PRIVILEGE_MAX; ++i)
		{
			if (m_RefreshValue[i])
			{
				rlDebug1("Privileges Worker - Start Asynch call for Gamer '%d', Privilege '%s'.", actingUserIndex,  rlPrivileges::GetPrivilegeName(i));

				m_Pending         = i;
				m_RefreshValue[i] = false;
				m_Refresh         = true;

				//Retrieve value Asynchronously for this privilege.
				rlPrivileges::CheckPrivileges(actingUserIndex, rlPrivileges::GetPrivilegeBit(static_cast<rlPrivileges::PrivilegeTypes>(i)), false);

				break;
			}
		}

		//Finally trigger the end of this worker - Lets make sure the game updates its records.
		if (!m_Refresh)
		{
#if !__NO_OUTPUT
			rlDebug1("Privileges Worker - Gamer %d Privileges Worker done:", actingUserIndex);
			for (int i=0; i<rlPrivileges::PRIVILEGE_MAX; ++i)
				rlDebug1("Privileges Worker - ...... Privilege '%s'='%s'.", rlPrivileges::GetPrivilegeName(i), IPrivileges::GetPrivilegeCheckResultName(m_Value[i]));
#endif // !__NO_OUTPUT

			rlPresence::TriggerOnlinePermissionsChanged(actingUserIndex);
		}
	}

	return true;
}

#endif // RSG_DURANGO


//////////////////////////////////////////////////////////////////////////
//  rlPrivileges
//////////////////////////////////////////////////////////////////////////
bool rlPrivileges::m_Initialized = false;

#if RSG_DURANGO
rlPrivileges::PrivilegeWorker  rlPrivileges::sm_Privileges[RL_MAX_LOCAL_GAMERS];
#endif //RSG_DURANGO

bool
rlPrivileges::Init()
{
	rlDebug("Initializing rlPrivileges...");

	bool success = false;

	if(rlVerifyf(!m_Initialized, "Already initialized")
		&& rlPrivileges::NativeInit())
	{
		m_Initialized = true;
		success = true;
		rlDebug("Initialized");
	}

	return success;
}

void
rlPrivileges::Shutdown()
{
	if(m_Initialized)
	{
		rlDebug("Shutting down rlPrivileges...");

		rlPrivileges::NativeShutdown();

		m_Initialized = false;

		rlDebug("Shut down");
	}
}

bool
rlPrivileges::IsInitialized()
{
	return m_Initialized;
}

void
rlPrivileges::Update()
{
	if(!rlVerifyf(m_Initialized, "Not initialized"))
	{
		return;
	}

	rlPrivileges::NativeUpdate();
}

#if RSG_DURANGO
// Keep this synchronized with:
//  1)  rlprivileges.h: PrivilegeTypes
//	2)	rlxblprivileges.cpp: const int privilegeCodes[rlPrivileges::PRIVILEGE_MAX]
//  3)  commands_network.sch: PRIVILEGE_TYPE_DURANGO_X bit flags
const int privilegeBits[rlPrivileges::PRIVILEGE_MAX] = {
	(1 << (int)rlPrivileges::PRIVILEGE_ADD_FRIEND),
	(1 << (int)rlPrivileges::PRIVILEGE_CLOUD_GAMING_JOIN_SESSION),
	(1 << (int)rlPrivileges::PRIVILEGE_CLOUD_GAMING_MANAGE_SESSION),
	(1 << (int)rlPrivileges::PRIVILEGE_CLOUD_SAVED_GAMES),
	(1 << (int)rlPrivileges::PRIVILEGE_COMMUNICATIONS),
	(1 << (int)rlPrivileges::PRIVILEGE_COMMUNICATION_VOICE_INGAME),
	(1 << (int)rlPrivileges::PRIVILEGE_COMMUNICATION_VOICE_SKYPE),
	(1 << (int)rlPrivileges::PRIVILEGE_GAME_DVR),
	(1 << (int)rlPrivileges::PRIVILEGE_MULTIPLAYER_PARTIES),
	(1 << (int)rlPrivileges::PRIVILEGE_MULTIPLAYER_SESSIONS),
	(1 << (int)rlPrivileges::PRIVILEGE_PREMIUM_CONTENT),
	(1 << (int)rlPrivileges::PRIVILEGE_PREMIUM_VIDEO),
	(1 << (int)rlPrivileges::PRIVILEGE_PROFILE_VIEWING),
	(1 << (int)rlPrivileges::PRIVILEGE_DOWNLOAD_FREE_CONTENT),
	(1 << (int)rlPrivileges::PRIVILEGE_PURCHASE_CONTENT),
	(1 << (int)rlPrivileges::PRIVILEGE_SHARE_KINECT_CONTENT),
	(1 << (int)rlPrivileges::PRIVILEGE_SOCIAL_NETWORK_SHARING),
	(1 << (int)rlPrivileges::PRIVILEGE_SUBSCRIPTION_CONTENT),
	(1 << (int)rlPrivileges::PRIVILEGE_USER_CREATED_CONTENT),
	(1 << (int)rlPrivileges::PRIVILEGE_VIDEO_COMMUNICATIONS),
	(1 << (int)rlPrivileges::PRIVILEGE_VIEW_FRIENDS_LIST)
};
#else
const int privilegeBits[rlPrivileges::PRIVILEGE_MAX] = { 0 };
#endif // RSG_DURANGO

#if RSG_DURANGO
int
rlPrivileges::GetPrivilegeBit(PrivilegeTypes privilegeType)
{
	rlAssert( privilegeType >= 0 && privilegeType < PRIVILEGE_MAX);
	return privilegeBits[static_cast<int>(privilegeType)];
}
#else
int
rlPrivileges::GetPrivilegeBit(PrivilegeTypes UNUSED_PARAM(privilegeType))
{
	rlAssertf(false, "rlPrivileges::GetPrivilegeBit() is not implemented for this platform.");
	return 0;
}
#endif //RSG_DURANGO

#if RSG_DURANGO
bool
rlPrivileges::CheckPrivileges(const int localGamerIndex, const int privilegeTypeBitfield, const bool attemptResolution)
{
	rlDebug1("rlPrivileges::CheckPrivileges - localGamerIndex: %d, privilegeTypeBitfield: 0x%08x, attemptResolution: %s", localGamerIndex, privilegeTypeBitfield, attemptResolution ? "true" : "false");
	if(!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
		return false;

	return g_rlXbl.GetPrivilegesManager()->CheckPrivileges(localGamerIndex, privilegeTypeBitfield, attemptResolution);
}
#else
bool
rlPrivileges::CheckPrivileges(const int UNUSED_PARAM(localGamerIndex), const int UNUSED_PARAM(privilegeTypeBitfield), const bool UNUSED_PARAM(attemptResolution))
{
	rlAssertf(false, "rlPrivileges::CheckPrivileges() is not implemented for this platform.");
	return false;
}
#endif //RSG_DURANGO

bool
rlPrivileges::IsPrivilegeCheckResultReady()
{
#if RSG_DURANGO
	return g_rlXbl.GetPrivilegesManager()->IsPrivilegeCheckResultReady();
#else
	rlAssertf(false, "rlPrivileges::IsPrivilegeCheckResultReady() is not implemented for this platform.");
	return false;
#endif //RSG_DURANGO
}

bool
rlPrivileges::IsPrivilegeCheckInProgress()
{
#if RSG_DURANGO
	return g_rlXbl.GetPrivilegesManager()->IsPrivilegeCheckInProgress();
#else
	rlAssertf(false, "rlPrivileges::IsPrivilegeCheckInProgress() is not implemented for this platform.");
	return false;
#endif //RSG_DURANGO
}

bool
rlPrivileges::IsPrivilegeCheckSuccessful()
{
#if RSG_DURANGO
	return g_rlXbl.GetPrivilegesManager()->IsPrivilegeCheckSuccessful();
#else
	rlAssertf(false, "rlPrivileges::IsPrivilegeCheckSuccessful() is not implemented for this platform.");
	return false;
#endif //RSG_DURANGO
}

void
rlPrivileges::SetPrivilegeCheckResultNotNeeded()
{
#if RSG_DURANGO
	g_rlXbl.GetPrivilegesManager()->SetPrivilegeCheckResultNotNeeded();
#else
	rlAssertf(false, "rlPrivileges::SetPrivilegeCheckResultNotNeeded() is not implemented for this platform.");
#endif //RSG_DURANGO
}

void
rlPrivileges::OnSignOut(const int XBL_ONLY(localGamerIndex))
{
#if RSG_XBL
	rlDebug1("rlPrivileges::OnSignOut - localGamerIndex: %d", localGamerIndex);

	g_rlXbl.GetPrivilegesManager()->OnSignOut();

	// reset all privileges for this user
	if(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		sm_Privileges[localGamerIndex].m_Refresh = true; 
		sm_Privileges[localGamerIndex].m_Pending = -1;

		for(int i = 0; i < PRIVILEGE_MAX; i++)
		{
			sm_Privileges[localGamerIndex].m_RefreshValue[i] = true;
			sm_Privileges[localGamerIndex].m_Value[i] = IPrivileges::ePrivilegeCheckResult::PRIV_RESULT_DEFAULT;
		}
	}	
#endif
}

#if RSG_DURANGO

void 
rlPrivileges::RefreshPrivileges(const int localGamerIndex)
{
	if(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		rlDebug1("rlPrivileges::RefreshPrivileges - localGamerIndex: %d", localGamerIndex);
		SetPrivilegeCheckResultNotNeeded();
		sm_Privileges[localGamerIndex].m_Refresh = true;
		for (int i=0; i<PRIVILEGE_MAX; i++)
		{
			sm_Privileges[localGamerIndex].m_RefreshValue[i] = true;
		}
	}
}

bool
rlPrivileges::IsRefreshPrivilegesPending(const int localGamerIndex)
{
	if(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return sm_Privileges[localGamerIndex].m_Refresh;
	}
	return false;
}

bool
rlPrivileges::HasPrivilege(const int localGamerIndex, const rlPrivileges::PrivilegeTypes privilegeId, const IPrivileges::ePrivilegeCheckResult privtype)
{
	if(RL_VERIFY_PRIVILEGE_ID(privilegeId))
	{
		return (sm_Privileges[localGamerIndex].m_Value[privilegeId] == privtype);
	}

	return false;
}

void 
rlPrivileges::UpdatePrivilege(const int localGamerIndex, const rlPrivileges::PrivilegeTypes privilegeId, const IPrivileges::ePrivilegeCheckResult privtype)
{
	if(RL_VERIFY_PRIVILEGE_ID(privilegeId))
	{
		rlDebug1("rlPrivileges::UpdatePrivilege - localGamerIndex: %d, privilegeId: %d, privtype: %d", localGamerIndex, (int)privilegeId, (int)privtype);
		sm_Privileges[localGamerIndex].m_Value[privilegeId] = privtype;
		sm_Privileges[localGamerIndex].m_Refresh = true;
	}
}

#endif //RSG_DURANGO

bool
rlPrivileges::NativeInit()
{
	return true;
}

void
rlPrivileges::NativeShutdown()
{

}

void
rlPrivileges::NativeUpdate()
{
#if RSG_DURANGO
	bool isAnyWorking = false;

	for (int i=0; i<RL_MAX_LOCAL_GAMERS && !isAnyWorking; i++)
	{
		if (sm_Privileges[i].IsPending())
		{
			sm_Privileges[i].Update(i);
			isAnyWorking = true;
			break;
		}
	}

	if (!isAnyWorking)
	{
		for (int i=0; i<RL_MAX_LOCAL_GAMERS; i++)
		{
			// Check if the privileges worker for the given gamer index requires a refresh
			if (sm_Privileges[i].Refresh())
			{
				// Update only returns true if work has been queued/done.
				// Returns false if work can not be done (i.e. no paired gamer at the given index)
				if (sm_Privileges[i].Update(i))
				{
					// Break if work has succeeded, since the XBL Privilege Manager
					// can only allow a single check (and result).
					break;
				}
			}
		}
	}
#endif //RSG_DURANGO
}

#if !__NO_OUTPUT
static const char* s_PrivilegeNames[] = {
#if RSG_DURANGO
	"ADD_FRIEND"
	,"CLOUD_GAMING_JOIN_SESSION"
	,"CLOUD_GAMING_MANAGE_SESSION"
	,"CLOUD_SAVED_GAMES"
	,"COMMUNICATIONS"
	,"COMMUNICATION_VOICE_INGAME"
	,"COMMUNICATION_VOICE_SKYPE"
	,"GAME_DVR"
	,"MULTIPLAYER_PARTIES"
	,"MULTIPLAYER_SESSIONS"
	,"PREMIUM_CONTENT"
	,"PREMIUM_VIDEO"
	,"PROFILE_VIEWING"
	,"DOWNLOAD_FREE_CONTENT"
	,"PURCHASE_CONTENT"
	,"SHARE_KINECT_CONTENT"
	,"SOCIAL_NETWORK_SHARING"
	,"SUBSCRIPTION_CONTENT"
	,"USER_CREATED_CONTENT"
	,"VIDEO_COMMUNICATIONS"
	,"VIEW_FRIENDS_LIST"
#else
	"NONE"
#endif //RSG_DURANGO
};
CompileTimeAssert(COUNTOF(s_PrivilegeNames) == rlPrivileges::PRIVILEGE_MAX);

const char* 
rlPrivileges::GetPrivilegeName(const int id)
{
	if (RL_VERIFY_PRIVILEGE_ID(id))
	{
		return s_PrivilegeNames[id];
	}

	return "INVALID";
}

#endif 

}   //namespace rage
