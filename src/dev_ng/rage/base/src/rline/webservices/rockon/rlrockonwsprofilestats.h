// 
// rline/rlrockonwsprofilestats.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONWSPROFILESTATS_H
#define RLINE_RLROCKONWSPROFILESTATS_H

#include "rline/rlrockonhttptask.h"

namespace rage
{

class netStatus;
class rlDbReadResults;

//PURPOSE
//  Proxy class for \services\profilestats.asmx
class rlRockonWsProfileStats
{
public:
    class ReadStatsTask : public rlRockonHttpTask
    {
    public:
        RL_TASK_USE_SUBCHANNEL(rline, rockon);
        RL_DECL_TASK(rlRockonWsProfileStats::ReadStatsTask);

        ReadStatsTask();
        virtual ~ReadStatsTask() {};

        bool Configure(
            const int localGamerIndex,
            const int titleId,
            const char* tableName,
            const char* gamerHandles,
            const char* statNames,
            rlDbReadResults* results,
            netStatus* status);

    protected:
        virtual const char* GetServicePath();
        virtual bool ProcessResult(const parTreeNode* node);

    private:
        rlDbReadResults* m_Results;

        char m_ServicePath[ROS_MAX_SERVICE_PATH];
    };

    class WriteStatsTask : public rlRockonHttpTask
    {
    public:
        RL_TASK_USE_SUBCHANNEL(rline, rockon);
        RL_DECL_TASK(rlRockonWsProfileStats::WriteStatsTask);

        WriteStatsTask();
        virtual ~WriteStatsTask() {};

        bool Configure(
            const int localGamerIndex,
            const int titleId,
            const char* tableName,
            const char* gamerHandle,
            const char* gamertag,
            const char* submission,
            int* secsUntilNextWrite,
            netStatus* status);

    protected:
        virtual const char* GetServicePath();
        virtual bool ProcessResult(const parTreeNode* node);

    private:
        int* m_SecsUntilNextWrite;

        char m_ServicePath[ROS_MAX_SERVICE_PATH];
    };
};

} //namespace rage

#endif
