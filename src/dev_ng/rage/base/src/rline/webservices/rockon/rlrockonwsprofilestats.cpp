// 
// rline/rlrockonprofilestats.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrockonwsprofilestats.h"
#include "rline/rldatabase.h"
#include "rline/rlrockonclient.h"
#include "rline/rlrockonhttptask.h"
#include "rline/rlrockonmanager.h"
#include "system/nelem.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"

using namespace rage;

//////////////////////////////////////////////////////////////////////////
// ReadStats
//////////////////////////////////////////////////////////////////////////
rlRockonWsProfileStats::ReadStatsTask::ReadStatsTask()
: m_Results(NULL)
{
    m_ServicePath[0] = '\0';
}

bool 
rlRockonWsProfileStats::ReadStatsTask::Configure(
    const int localGamerIndex,
    const int titleId,
    const char* tableName,
    const char* gamerHandles,
    const char* statNames,
    rlDbReadResults* results,
    netStatus* status)
{
    if(!rlRockonHttpTask::Configure(status))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    rlRockonCredentials cred = rlRockonManager::GetCredentials(localGamerIndex);

    if(!AddBinaryParameter("ticket", cred.m_Ticket, cred.m_TicketLen)
        || !AddBinaryParameter("authenticator", cred.m_Authenticator, cred.m_AuthenticatorLen)
        || !AddIntParameter("titleId", titleId)
        || !AddStringParameter("tableName", tableName, true)
        || !AddStringParameter("gamerHandles", gamerHandles, true)
        || !AddStringParameter("statNames", statNames, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_Results = results;

    return true;
}

const char*
rlRockonWsProfileStats::ReadStatsTask::GetServicePath()
{
    if('\0' == m_ServicePath[0])
    {
        ComposeServicePath(m_ServicePath,
                            COUNTOF(m_ServicePath),
                            "ProfileStats.asmx/ReadStats");
    }

    return m_ServicePath;
}

bool 
rlRockonWsProfileStats::ReadStatsTask::ProcessResult(const parTreeNode* node)
{
    rlAssert(node);

    rtry
    {
        const parTreeNode* records = node->FindChildWithName("Records");
        rcheck(records, catchall, rlTaskError("Failed to find <Records> element"));

        unsigned numRecords = 0;

        //TODO: Change to only store the service results here, and move the parsing
        //      into the db results to the main task.
        parTreeNode::ChildNodeIterator iter = records->BeginChildren();
        while((iter != records->EndChildren()) && (numRecords < m_Results->GetMaxRecords()))
        {
            parTreeNode* record = *iter;
            rlAssert(record);

            if(stricmp(record->GetElement().GetName(), "Record") != 0) continue;

            const char* gh = rlHttpTaskHelper::ReadString(record, NULL, "gh");
            rverify(gh, catchall, );

            const char* gt = rlHttpTaskHelper::ReadString(record, NULL, "gt");
            rverify(gt, catchall, );

            const char* csv = rlHttpTaskHelper::ReadString(record, NULL, "csv");
            rverify(csv, catchall, );

            rlTaskDebug2("Record:  gh=%s  gt=%s  csv=%s", gh, gt, csv);

            rverify(m_Results->SetAsciiString(numRecords, 0, gh), catchall, );
            rverify(m_Results->SetAsciiString(numRecords, 1, gt), catchall, );

            unsigned column = 2;
            const char* csvStart = csv;
            const char* csvEnd = csvStart;
            while(*csvEnd)
            {
                if(*csvEnd == ',')
                {
                    unsigned len = csvEnd - csvStart;
                    if(rlVerify(len))
                    {
                        char buf[128];                        
                        memcpy(buf, csvStart, len);
                        buf[len] = '\0';

                        switch(m_Results->GetFieldType(column))
                        {
                        case RLDB_VALUETYPE_FLOAT:
                            {
                                float val;
                                rcheck(1 == sscanf(buf, "%f", &val),
                                    catchall,
                                    rlTaskError("Failed to parse float from: %s", buf));
                                m_Results->SetFloat(numRecords, column, val);
                            }
                            break;
                        case RLDB_VALUETYPE_INT32:
                            {
                                s32 val;
                                rcheck(1 == sscanf(buf, "%d", &val),
                                    catchall,
                                    rlTaskError("Failed to parse int32 from: %s", buf));
                                m_Results->SetInt32(numRecords, column, val);
                            }
                            break;
                        case RLDB_VALUETYPE_INT64:
                            {
                                s64 val;
                                rcheck(1 == sscanf(buf, "%"I64FMT"d", &val),
                                    catchall,
                                    rlTaskError("Failed to parse int64 from: %s", buf));
                                m_Results->SetInt64(numRecords, column, val);
                            }
                            break;
                        default:
                            rlTaskError("Unhandled DB type: %u", m_Results->GetFieldType(column));
                        }
                    }

                    ++column;
                    csvStart = ++csvEnd;
                }  
                else
                {
                    ++csvEnd;
                }
            }

            //Process final column, if any.
            unsigned len = csvEnd - csvStart;
            if(len)
            {
                char buf[128];                        
                memcpy(buf, csvStart, len);
                buf[len] = '\0';

                switch(m_Results->GetFieldType(column))
                {
                case RLDB_VALUETYPE_FLOAT:
                    {
                        float val;
                        rcheck(1 == sscanf(buf, "%f", &val),
                            catchall,
                            rlTaskError("Failed to parse float from: %s", buf));
                        m_Results->SetFloat(numRecords, column, val);
                    }
                    break;
                case RLDB_VALUETYPE_INT32:
                    {
                        s32 val;
                        rcheck(1 == sscanf(buf, "%d", &val),
                            catchall,
                            rlTaskError("Failed to parse int32 from: %s", buf));
                        m_Results->SetInt32(numRecords, column, val);
                    }
                    break;
                case RLDB_VALUETYPE_INT64:
                    {
                        s64 val;
                        rcheck(1 == sscanf(buf, "%"I64FMT"d", &val),
                            catchall,
                            rlTaskError("Failed to parse int64 from: %s", buf));
                        m_Results->SetInt64(numRecords, column, val);
                    }
                    break;
                default:
                    rlTaskError("Unhandled DB type: %u", m_Results->GetFieldType(column));
                }
            }

            ++numRecords;

            ++iter;
        }

        m_Results->SetNumRecords(numRecords);

        return true;
    }
    rcatchall
    {
        return false;
    }
}

//bool 
//rlRockonWsProfileStats::ReadStatsTask::ProcessResult(parTreeNode* node) 
//{ 
//    parTreeNode* recordsNode = node->FindChildWithName("Records");
//    if(!rlVerify(recordsNode)) return false;
//
//    int numRecords = recordsNode->FindNumChildren();
//
//    for(int i = 0; i < numRecords; i++)
//    {
//        parTreeNode* recordNode = recordsNode->FindChildWithIndex(i);
//        if(!rlVerify(recordNode)) return false;
//        if(!rlVerify(!strcmp(recordNode->GetElement().GetName(), "Record"))) return false;
//
//        rlRockonWsProfileStats::Record& r = m_Records->m_Buf[i];
//
//        {
//            const parAttribute *attr = recordNode->GetElement().FindAttributeAnyCase("gh");
//            if(!rlVerify(attr)) return false;
//            
//            const char* val = attr->GetStringValue();
//            if(!rlVerify(val)) return false;
//
//            size_t valLen = strlen(val);
//            if(!rlVerify(valLen < r.m_gh.m_MaxLength)) return false;
//
//            safecpy(r.m_gh.m_Buf, val, r.m_gh.m_MaxLength);
//            *r.m_gh.m_Length = (unsigned)valLen;
//        }
//
//        {
//            const parAttribute *attr = recordNode->GetElement().FindAttributeAnyCase("gt");
//            if(!rlVerify(attr)) return false;
//
//            const char* val = attr->GetStringValue();
//            if(!rlVerify(val)) return false;
//
//            size_t valLen = strlen(val);
//            if(!rlVerify(valLen < r.m_gt.m_MaxLength)) return false;
//
//            safecpy(r.m_gt.m_Buf, val, r.m_gt.m_MaxLength);
//            *r.m_gt.m_Length = (unsigned)valLen;
//        }
//
//        {
//            const parAttribute *attr = recordNode->GetElement().FindAttributeAnyCase("csv");
//            if(!rlVerify(attr)) return false;
//
//            const char* val = attr->GetStringValue();
//            if(!rlVerify(val)) return false;
//
//            size_t valLen = strlen(val);
//            if(!rlVerify(valLen < r.m_csv.m_MaxLength)) return false;
//
//            safecpy(r.m_csv.m_Buf, val, r.m_csv.m_MaxLength);
//            *r.m_csv.m_Length = (unsigned)valLen;
//        }
//    }
//
//    return true; 
//}

//////////////////////////////////////////////////////////////////////////
// WriteStats
//////////////////////////////////////////////////////////////////////////
rlRockonWsProfileStats::WriteStatsTask::WriteStatsTask()
: m_SecsUntilNextWrite(NULL)
{
    m_ServicePath[0] = '\0';
}

bool 
rlRockonWsProfileStats::WriteStatsTask::Configure(
    const int localGamerIndex,
    const int titleId,
    const char* tableName,
    const char* gamerHandle,
    const char* gamertag,
    const char* submission,
    int* secsUntilNextWrite,
    netStatus* status)
{
    if(!rlRockonHttpTask::Configure(status))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    rlRockonCredentials cred = rlRockonManager::GetCredentials(localGamerIndex);

    if(!AddBinaryParameter("ticket", cred.m_Ticket, cred.m_TicketLen)
        || !AddBinaryParameter("authenticator", cred.m_Authenticator, cred.m_AuthenticatorLen)
        || !AddIntParameter("titleId", titleId)
        || !AddStringParameter("tableName", tableName, true)
        || !AddStringParameter("gamerHandle", gamerHandle, true)
        || !AddStringParameter("gamertag", gamertag, true)
        || !AddStringParameter("submission", submission, true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_SecsUntilNextWrite = secsUntilNextWrite;

    return true;
}

const char*
rlRockonWsProfileStats::WriteStatsTask::GetServicePath()
{
    if('\0' == m_ServicePath[0])
    {
        ComposeServicePath(m_ServicePath,
                            COUNTOF(m_ServicePath),
                            "ProfileStats.asmx/WriteStats");
    }

    return m_ServicePath;
}

bool 
rlRockonWsProfileStats::WriteStatsTask::ProcessResult(const parTreeNode* node)
{
    rlAssert(node);

    if(!rlHttpTaskHelper::ReadInt(*m_SecsUntilNextWrite, node, "SecsUntilNextWrite", NULL))
    {
        rlTaskError("Failed to read <SecsUntilNextWrite>");
        return false;
    }

    return true;
}
