// 
// rline/rltask.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rltask.h"

#include "diag/channel.h"
#include "net/status.h"
#include "net/net.h"
#include "net/netfuncprofiler.h"
#include "rldiag.h"
#include "system/memory.h"
#include "system/timer.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, task);
#undef __rage_channel
#define __rage_channel rline_task

#define rlTaskManagerDebug rageDebugf1

static volatile unsigned s_NextTaskId = 0;

PARAM(rlTaskAllowAddFailures, "Allow DoAddTask to fail");

//////////////////////////////////////////////////////////////////////////
// rlTaskBase
//////////////////////////////////////////////////////////////////////////
const char* 
rlTaskBase::FinishString(const FinishType finishType)
{
    switch(finishType)
    {
    case FINISH_SUCCEEDED:  return "SUCCEEDED"; /*NOTREACHED*/ break;
    case FINISH_CANCELED:   return "CANCELED"; /*NOTREACHED*/ break;
    case FINISH_FAILED:
    default:
        return "FAILED"; /*NOTREACHED*/ break;
    }
}

bool 
rlTaskBase::FinishSucceeded(const FinishType finishType)
{
    return (FINISH_SUCCEEDED == finishType);
}

rlTaskBase::rlTaskBase()
: m_Id(0)
, m_Status(NULL)
, m_State(STATE_INVALID)
, m_Flags(0)
, m_TimeoutTimerMs(0)
#if !__NO_OUTPUT
, m_ConfigureTime(0)
, m_StartTime(0)
#endif
, m_Owner(NULL)
, m_Creator(NULL)
, m_Next(NULL)
{
    m_Id = sysInterlockedIncrement(&s_NextTaskId);
}

rlTaskBase::~rlTaskBase()
{
    rlAssert(!this->IsActive());
    rlAssert(!m_Owner);
    rlAssert(!m_Status);

    if(m_Owner)
    {
        m_Owner->RemoveTask(this);
    }

    rlAssert(!m_Creator);
}

unsigned 
rlTaskBase::GetTaskId() const 
{ 
    return m_Id; 
}

const char*
rlTaskBase::GetTaskName() const
{
    return "UNNAMED_TASK";
}

void 
rlTaskBase::Start()
{
    if(rlVerify(STATE_CONFIGURED == m_State))
    {
#if !__NO_OUTPUT
		m_StartTime = sysTimer::GetSystemMsTime();
#endif
        rlTaskDebug2("Starting (waited %ums in queue)...", m_StartTime - m_ConfigureTime);

        m_State = STATE_STARTED;
    }
}

void 
rlTaskBase::Update(const unsigned timeStepMs)
{
    rlAssert(this->IsActive());

    if(m_TimeoutTimerMs > 0)
    {
        m_TimeoutTimerMs -= timeStepMs;
        if(0 == m_TimeoutTimerMs)
        {
            m_TimeoutTimerMs = -1;
        }
    }
}

bool 
rlTaskBase::IsStarted() const
{
    return m_State >= STATE_STARTED;
}

bool 
rlTaskBase::IsFinished() const
{
    return STATE_FINISHED == m_State;
}

bool 
rlTaskBase::IsActive() const
{
    return this->IsStarted() && !this->IsFinished();
}

bool 
rlTaskBase::IsPending() const
{
    return m_State >= STATE_CONFIGURED && m_State < STATE_FINISHED;
}

bool
rlTaskBase::WasCanceled() const
{
    return 0 != (FLAG_CANCELED & m_Flags);
}

bool
rlTaskBase::IsCancelable() const
{
    return false;
}

bool 
rlTaskBase::Configure(netStatus* status)
{
    const bool success = rlVerify(!this->IsPending());

    if(success)
    {
        m_Flags = 0;
        m_Status = status;
        m_State = STATE_CONFIGURED;

#if !__NO_OUTPUT
		m_ConfigureTime = sysTimer::GetSystemMsTime();
#endif

        if(m_Status)
        {
            m_Status->SetPending();
        }
    }
    else if(status)
    {
        status->SetPending(); 
        status->SetFailed();
    }

    return success;
}

//protected:

const diagChannel*
rlTaskBase::GetDiagChannel() const
{
#if !__NO_OUTPUT
    return &Channel_rline_task;
#else
    return NULL;
#endif
}

void
rlTaskBase::Finish(const FinishType finishType)
{
    this->Finish(finishType, 0);
}

void
rlTaskBase::Finish(const FinishType finishType, const int resultCode)
{
	if(FINISH_FAILED == finishType)
	{ 
		rlTaskError("Finish :: Task failed");
	}
	else if(FINISH_CANCELED == finishType)
	{
		rlTaskDebug("Finish :: Task canceled");
	}

	if(!rlVerify(!this->IsFinished()))
	{
		rlTaskError("Finish :: Task already finished!");
	}

	//Permit Finish() only if we're active, or, if we haven't yet
	//started, then if we're configured but failed or canceled.
	if(!rlVerify(this->IsActive()
		|| (this->IsPending() && FINISH_SUCCEEDED != finishType)))
	{
		rlTaskError("Finish :: Not active or pending!");
	}

#if !__NO_OUTPUT
	u32 queueTime = (m_StartTime > 0) ? (m_StartTime - m_ConfigureTime) : (sysTimer::GetSystemMsTime() - m_ConfigureTime);
	u32 executionTime = (m_StartTime > 0) ? (sysTimer::GetSystemMsTime() - m_StartTime) : 0;
	u32 totalTime = queueTime + executionTime;
#endif
    rlTaskDebug2("%s (%ums in queue, %ums executing, %ums total)", FinishString(finishType), queueTime, executionTime, totalTime);

    if(m_Status)
    {
        if(FINISH_SUCCEEDED == finishType)
        { 
            m_Status->SetSucceeded(resultCode);
        }
        else if(FINISH_CANCELED == finishType)
        {
            m_Status->SetCanceled();
        }
        else
        {
            m_Status->SetFailed(resultCode);
        }
    }

    if(m_Owner && m_Status)
    {
        m_Owner->m_TaskMapByStatus.erase(this);
    }

    m_Status = NULL;
    m_State = STATE_FINISHED;
}

//protected:

void
rlTaskBase::Cancel()
{
    if(!rlVerify(this->IsCancelable()))
	{
		rlTaskError("Cancel :: Task is not cancelable!"); 
	}

    if(!this->WasCanceled() && !(m_Flags & FLAG_CANCELING))
    {
        rlTaskDebug2("Canceling...");

        const unsigned oldFlags = m_Flags;
        m_Flags |= FLAG_CANCELING;

        if(m_Owner && m_Status)
        {
            m_Owner->m_TaskMapByStatus.erase(this);
        }

        this->DoCancel();

        m_Flags = oldFlags | FLAG_CANCELED;

        if(m_Status)
        {
            m_Status->SetCanceled();
            m_Status = NULL;
        }
    }
}

bool 
rlTaskBase::IsStatusCanceled() const
{
	if(m_Status)
	{
		return m_Status->Canceled();
	}

	return false;
}

void
rlTaskBase::DoCancel()
{
    
}

void
rlTaskBase::SetTimeoutMs(const unsigned timeoutMs)
{
    rlTaskDebug3("SetTimeoutMs: %u", timeoutMs);
    m_TimeoutTimerMs = (int) timeoutMs;
    rlAssert(m_TimeoutTimerMs >= 0);
}

unsigned
rlTaskBase::GetMsUntilTimeout() const
{
    return (m_TimeoutTimerMs >= 0) ? m_TimeoutTimerMs : 0;
}

unsigned
rlTaskBase::GetSecondsUntilTimeout() const
{
    return this->GetMsUntilTimeout() / 1000;
}

void
rlTaskBase::ClearTimeout()
{
    m_TimeoutTimerMs = 0;
}

bool
rlTaskBase::TimedOut() const
{
    return m_TimeoutTimerMs <= 0;
}

//////////////////////////////////////////////////////////////////////////
// rlTaskManager
//////////////////////////////////////////////////////////////////////////
rlTaskManager::rlTaskManager()
: m_Allocator(NULL)
, m_LastUpdateTime(0)
, m_Initialized(false)
{
}

rlTaskManager::~rlTaskManager()
{
    this->Shutdown();
}

bool 
rlTaskManager::Init(sysMemAllocator* allocator)
{
    bool success = false;

    if(rlVerify(!m_Initialized))
    {
		//@@: location RLTASKMANAGER_INIT_SET_VARS
        rlAssert(!m_Allocator);
        rlAssert(!m_LastUpdateTime);
        rlAssert(m_TaskMapById.empty());
        rlAssert(m_TaskMapByStatus.empty());
        rlAssert(m_TaskQ.empty());

        m_Allocator = allocator;
        m_Initialized = success = true;
    }
    
    return success;
}

void 
rlTaskManager::Shutdown()
{
    if(m_Initialized)
    {
        this->CancelAll();

        while(!m_TaskMapById.empty())
        {
			// UpdateTask will properly finish and remove the canceled tasks
            this->UpdateTask(m_TaskMapById.begin()->second, 0);
        }

        rlAssert(m_TaskMapById.empty());
        rlAssert(m_TaskMapByStatus.empty());
        rlAssert(m_TaskQ.empty());

        m_Allocator = NULL;
        m_LastUpdateTime = 0;
        m_Initialized = false;
    }
}

bool
rlTaskManager::IsInitialized() const
{
    return m_Initialized;
}

void 
rlTaskManager::Update()
{
    unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;
    unsigned deltaMs = 0;

    if(m_LastUpdateTime)
    {
        deltaMs = curTime - m_LastUpdateTime;
    }

    m_LastUpdateTime = curTime;    

    if(!m_TaskQ.empty())
    {
        TaskQueue::iterator it = m_TaskQ.begin();
        TaskQueue::iterator next = it;
        TaskQueue::const_iterator stop = m_TaskQ.end();

        size_t queueId = it->first + 1;

        for(++next; stop != it; it = next, ++next)
        {
            //Run the first task on each queue, or if queue zero
            //(the queue for parallel tasks) run all tasks.
            if(it->first != queueId || 0 == it->first)
            {
#if defined(NET_PROFILER) && NET_PROFILER
				char buf[255];
				formatf(buf, "[%u] %s", it->second->GetTaskId(), it->second->GetTaskName());

				bool pushed = false;
				if (this == rlGetTaskManager())
				{
					pushed = netFunctionProfiler::GetProfiler().PushProfiler(buf);
				}
#endif

                this->UpdateTask(it->second, deltaMs);
                queueId = it->first;

#if defined(NET_PROFILER) && NET_PROFILER
				if (this == rlGetTaskManager() && pushed)
				{
					netFunctionProfiler::GetProfiler().PopProfiler(buf);
				}
#endif
            }
        }
    }
}

void
rlTaskManager::UpdateTask(const netStatus* status)
{
	TaskMapByStatus::iterator it = m_TaskMapByStatus.find(status);
	rlTaskBase* task = (m_TaskMapByStatus.end() != it) ? it->second : NULL;

	if(task)
	{
		static const unsigned DELTA_MS = 33; 
		this->UpdateTask(it->second, DELTA_MS);
	}
}

bool 
rlTaskManager::AddParallelTask(rlTaskBase* task)
{
    //All tasks on queue zero are parallel tasks.
    return this->DoAddTask(0, task);
}

bool 
rlTaskManager::AppendSerialTask(rlTaskBase* task)
{
    //Queue one is the default queue if a queue is not specified.
    return this->DoAddTask(1, task);
}

bool
rlTaskManager::AppendSerialTask(const size_t queueId, rlTaskBase* task)
{
    bool success = false;

    if(rlVerifyf(0 != queueId, "0 is an invalid queue id")
        && rlVerifyf(1 != queueId, "1 is an invalid queue id"))
    {
        success = this->DoAddTask(queueId, task);
    }

    return success;
}

bool
rlTaskManager::HasPendingTask(const size_t queueId) const
{
    if(rlVerifyf(0 != queueId, "0 is an invalid queue id")
        && rlVerifyf(1 != queueId, "1 is an invalid queue id"))
    {
        TaskQueue::const_iterator it = m_TaskQ.find(queueId);

        return m_TaskQ.end() != it;
    }

    return false;
}

bool 
rlTaskManager::HasAnyPendingTasks()
{
	return !m_TaskQ.empty();
}

rlTaskBase*
rlTaskManager::FindTask(const unsigned taskId)
{
    TaskMapById::iterator it = m_TaskMapById.find(taskId);

    return (m_TaskMapById.end() != it) ? it->second : NULL;
}

rlTaskBase*
rlTaskManager::FindTask(const netStatus* status)
{
    TaskMapByStatus::iterator it = m_TaskMapByStatus.find(status);

    return (m_TaskMapByStatus.end() != it) ? it->second : NULL;
}

void
rlTaskManager::CancelTask(const unsigned taskId)
{
    rlTaskBase* task = this->FindTask(taskId);
	if(!rlVerify(task))
	{
		rlError("CancelTask :: Task not found for status!"); 
		return;
	}

	this->CancelTask(task);
}

void
rlTaskManager::CancelTask(const netStatus* status)
{
    if(!rlVerify(status))
	{
		rlError("CancelTask :: Status is NULL!"); 
		return;
	}

	if(!rlVerify(status->Pending()))
	{
		rlError("CancelTask :: Status is not pending!"); 
		return;
	}

	rlTaskBase* task = this->FindTask(status);
	if(!rlVerify(task))
	{
		rlError("CancelTask :: Task not found for status!"); 
		return;
	}

	this->CancelTask(task);

	if(!rlVerify(status->Canceled()))
	{
		rlError("CancelTask :: %s[%u] - Status not cancelled!", task->GetTaskName(), task->GetTaskId()); 
	}
}

void
rlTaskManager::CancelTask(rlTaskBase* task)
{
    if(!rlVerify(task))
	{
		rlError("CancelTask :: Task is NULL!"); 
		return;
	}

    if(rlVerify(this == task->m_Owner))
    {
        if(!task->WasCanceled())
        {
            if(rlVerify(task->IsPending()))
            {
                task->Cancel();

				if(!rlVerify(!task->m_Status))
				{
					rlError("CancelTask :: %s[%u] - Status not cleared!", task->GetTaskName(), task->GetTaskId()); 
				}

				if(!rlVerify(!task->m_MapNodeByStatus.m_left
						  && !task->m_MapNodeByStatus.m_right
						  && !task->m_MapNodeByStatus.m_parent))
				{
					rlError("CancelTask :: %s[%u] - Status Map invalid!", task->GetTaskName(), task->GetTaskId()); 
				}
            }
        }
    }
}

void
rlTaskManager::ShutdownCancel()
{
	TaskMapById::iterator it = m_TaskMapById.begin();
	TaskMapById::const_iterator stop = m_TaskMapById.end();

	for(; stop != it; ++it)
	{
		rlTaskBase* task = it->second;
		if(rlVerify(task) && task->GetShutdownBehaviour() == rlTaskBase::CANCEL_ON_SHUTDOWN)
		{
			this->CancelTask(task);
		}
	}
}

void
rlTaskManager::CancelAll(const size_t queueId)
{
    if(rlVerifyf(0 != queueId, "0 is an invalid queue id")
        && rlVerifyf(1 != queueId, "1 is an invalid queue id"))
    {
        TaskQueue::iterator it = m_TaskQ.find(queueId);
        TaskQueue::const_iterator stop = m_TaskQ.end();

        for(; stop != it && it->first == queueId; ++it)
        {
			rlTaskBase* task = it->second;
			if(task->IsCancelable())
			{
				this->CancelTask(task);
			}
        }
    }
}

void
rlTaskManager::CancelAll()
{
    TaskMapById::iterator it = m_TaskMapById.begin();
    TaskMapById::const_iterator stop = m_TaskMapById.end();

    for(; stop != it; ++it)
    {
		rlTaskBase* task = it->second;
		if(task->IsCancelable())
		{
			this->CancelTask(task);
		}
    }
}

void
rlTaskManager::DetachStatus(rlTaskBase* task)
{
	if(!rlVerify(task))
	{
		rlError("CancelTask :: Task is NULL!"); 
		return;
	}

    if(task->m_Status)
    {
        // This needs to have been dealt with by now
		if(!rlVerify(!task->m_Status->Pending()))
		{
			rlError("DetachStatus :: %s[%u] - Status is pending!", task->GetTaskName(), task->GetTaskId()); 
		}

        // Remove from the status map
        m_TaskMapByStatus.erase(task);
        task->m_Status = NULL;
	}
}

void 
rlTaskManager::DestroyTask(rlTaskBase* task)
{
    if(!rlVerify(task))
	{
		rlError("DestroyTask :: Task is NULL!"); 
		return;
	}

	if(!rlVerify(m_Initialized))
	{
		rlError("DestroyTask :: %s[%u] - Task Manager not initialised!", task->GetTaskName(), task->GetTaskId());
		return;
	}

	if(!rlVerify(!task->IsPending()))
	{
		rlError("DestroyTask :: %s[%u] - Task is pending!", task->GetTaskName(), task->GetTaskId()); 
	}

	rlTaskBase* otherTask = this->FindTask(task->GetTaskId());
	if(!rlVerify(!otherTask))
	{
		rlError("DestroyTask :: %s[%u] - Status is still tracked by %s[%u]", task->GetTaskName(), task->GetTaskId(), otherTask->GetTaskName(), otherTask->GetTaskId()); 
	}

	otherTask = this->FindTask(task->m_Status);
	if(!rlVerify(!otherTask))
	{
		rlError("DestroyTask :: %s[%u] - Status is still tracked by %s[%u]", task->GetTaskName(), task->GetTaskId(), otherTask->GetTaskName(), otherTask->GetTaskId()); 
	}

	if(!rlVerify(!task->m_Owner))
	{
		rlError("DestroyTask :: %s[%u] - Owner still valid!", task->GetTaskName(), task->GetTaskId()); 
	}

	if(rlVerify(this == task->m_Creator))
	{
		task->m_Creator = NULL;
		task->~rlTaskBase();
		m_Allocator->Free(task);
	}
}

//private:

bool 
rlTaskManager::DoAddTask(const size_t queueId, rlTaskBase* task)
{
    bool success = false;
	
	rtry
	{
		rverify(task, catchall, rlError("DoAddTask :: Task is NULL"));
		rverify(m_Initialized, catchall, rlError("DoAddTask :: %s[%u] - Not Initialised", task->GetTaskName(), task->GetTaskId()));
		rverify(!task->m_Owner, catchall, rlError("DoAddTask :: %s[%u] - Owner already assigned!", task->GetTaskName(), task->GetTaskId()));
		rverify(((!task->m_Creator) || (this == task->m_Creator)), catchall, rlError("DoAddTask :: %s[%u] - Creator already assigned!", task->GetTaskName(), task->GetTaskId()));
		rverify(task->m_Status, catchall, rlError("DoAddTask :: %s[%u] - Status is NULL!", task->GetTaskName(), task->GetTaskId()));
		rverify(!this->FindTask(task->GetTaskId()), catchall, rlError("DoAddTask :: %s[%u] - Task is already active!", task->GetTaskName(), task->GetTaskId()));
		rverify(!this->FindTask(task->m_Status), catchall, rlError("DoAddTask :: %s[%u] - Status already assigned!", task->GetTaskName(), task->GetTaskId()));
		rverify(!task->IsStarted(), catchall, rlError("DoAddTask :: %s[%u] - Task already started!", task->GetTaskName(), task->GetTaskId()));
		rverify(task->IsPending(), catchall, rlError("DoAddTask :: %s[%u] - Task is not pending!", task->GetTaskName(), task->GetTaskId()));

		if(!rlVerify((0 == task->m_Flags)))
		{
			rlError("DoAddTask :: %s[%u] - Task has non zero flags [%u]!", task->GetTaskName(), task->GetTaskId(), task->m_Flags);
		}
		
		task->m_Flags = 0;
		task->m_Owner = this;

		m_TaskMapById.insert(task->GetTaskId(), task);
		m_TaskMapByStatus.insert(task->m_Status, task);
		m_TaskQ.insert(queueId, task);

		success = true;
	}
	rcatchall
	{
#if !__FINAL
		if(!PARAM_rlTaskAllowAddFailures.Get())
		{
			Quitf("DoAddTask :: Failed to add task. Add logs and dump. Use -rlTaskAllowAddFailures to suppress fatal error");
		}
#endif
	}

    return success;
}

void
rlTaskManager::UpdateTask(rlTaskBase* task, const unsigned deltaMs)
{
    if(!rlVerify(task))
	{
		rlError("UpdateTask :: Task is NULL!");
		return;
	}

	if(!rlVerify(m_Initialized))
	{
		rlError("UpdateTask :: %s[%u] - Task Manager not initialised!", task->GetTaskName(), task->GetTaskId());
		return;
	}

	if(!rlVerify(this == task->m_Owner))
	{
		rlError("UpdateTask :: %s[%u] - Not owned by this task manager!", task->GetTaskName(), task->GetTaskId()); 
	}

    if(task->IsPending())
    {
        if(!task->IsStarted())
        {
            if(task->WasCanceled())
            {
                task->Finish(rlTaskBase::FINISH_CANCELED);
            }
            else
            {
                task->Start();
            }
        }

        if(task->IsActive())
        {
            task->Update(deltaMs);
        }
    }

    if(task->IsFinished())
    {
        this->RemoveTask(task);
    }
}

void
rlTaskManager::RemoveTask(rlTaskBase* task)
{
    if(!rlVerify(task))
	{
		rlError("RemoveTask :: Task is NULL!");
		return;
	}

	if(!rlVerify(m_Initialized))
	{
		rlError("RemoveTask :: %s[%u] - Task Manager not initialised!", task->GetTaskName(), task->GetTaskId());
		return;
	}

	if(!rlVerify(!task->IsPending() || task->WasCanceled()))
	{
		rlError("RemoveTask :: %s[%u] - Task is still pending!", task->GetTaskName(), task->GetTaskId()); 
	}

	if(!rlVerify(this == task->m_Owner))
	{
		rlError("RemoveTask :: %s[%u] - Not owned by this task manager!", task->GetTaskName(), task->GetTaskId()); 
		return;
	}

    m_TaskMapById.erase(task);
    m_TaskQ.erase(task);

    //m_Status will be NULL if the task was canceled.
    if(task->m_Status)
    {
        m_TaskMapByStatus.erase(task);
    }
    else
    {
		if(!rlVerify(!task->m_MapNodeByStatus.m_left
				  && !task->m_MapNodeByStatus.m_right
				  && !task->m_MapNodeByStatus.m_parent))
		{
			rlError("RemoveTask :: %s[%u] - Status Map invalid!", task->GetTaskName(), task->GetTaskId()); 
		}
    }

    task->m_Flags = 0;
    task->m_Owner = NULL;

    if(this == task->m_Creator)
    {
        this->DestroyTask(task);
    }
}

#if !__NO_OUTPUT
void
rlTaskManager::Dump()
{
	if(!rlVerify(m_Initialized))
	{
		rlError("Dump :: Task Manager not initialised!");
		return;
	}

	static const char* s_StateString[] = 
	{
		"STATE_INVALID",
		"STATE_CONFIGURED",
		"STATE_STARTED",
		"STATE_FINISHED",
	};

	static const char* s_StatusCodeString[] = 
	{
		"NET_STATUS_NONE",
		"NET_STATUS_PENDING",
		"NET_STATUS_FAILED",
		"NET_STATUS_SUCCEEDED",
		"NET_STATUS_CANCELED",
	};

	TaskMapById::iterator it = m_TaskMapById.begin();
	TaskMapById::const_iterator stop = m_TaskMapById.end();

	rlTaskManagerDebug("Dumping Task Queue:"); 
	for(; stop != it; ++it)
	{
#if !__NO_OUTPUT
		rlTaskBase* pTask = it->second;
#endif

		netStatus::StatusCode statusCode = netStatus::NET_STATUS_NONE;
		netStatus* pStatus = it->second->m_Status;
		if(pStatus)
		{
			statusCode = pStatus->GetStatus();
		}

		rlTaskManagerDebug("Found task: %s. State: %s, Flags: 0x%08x, Status: %s.", pTask->GetTaskName(), s_StateString[pTask->m_State], pTask->m_Flags, s_StatusCodeString[statusCode]);
	}
	rlTaskManagerDebug("Dump Complete"); 
}
#endif

}   //namespace rage
