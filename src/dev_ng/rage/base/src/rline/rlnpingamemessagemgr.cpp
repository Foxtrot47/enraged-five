// 
// rline/rlNpInGameMessage.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include "rlnpingamemessagemgr.h"

#include "diag/seh.h"
#include "rline/rlnp.h"

#include <np.h>
#define NP_IN_GAME_MESSAGE_POOL_SIZE ( 16 * 1024 )

namespace rage
{

extern sysThreadPool netThreadPool;

PrepareMessageWorkItem::PrepareMessageWorkItem()
{
	m_LibCtxId = 0;
}

// Callback function that is called when receiving an InGame message
void _inGameMessageEventCallback(int32_t libCtxId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom, const SceNpInGameMessageData *pMessage, void *pUserArg)
{
	rlDebug3("InGameMessage received from %s to %s", pFrom->onlineId.data, pTo->onlineId.data);
	g_rlNp.GetBasic().HandleInGameMessage(libCtxId, pTo, pFrom, pMessage, pUserArg);
}

void PrepareMessageWorkItem::Configure(int libCtxId)
{
	m_LibCtxId = libCtxId;
}

void PrepareMessageWorkItem::DoWork()
{
	int handleId = 0;

	rtry
	{
		int ret = sceNpInGameMessageCreateHandle(m_LibCtxId);
		rverify(ret >= 0, catchall, rlError("sceNpInGameMessageCreateHandle() failed. ret = 0x%x\n", ret));
		handleId = ret;
		 
		ret = sceNpInGameMessagePrepare( m_LibCtxId, handleId, NULL, _inGameMessageEventCallback, NULL);
		rcheck(ret == SCE_OK, catchall, rlError("sceNpInGameMessagePrepare() failed. ret = 0x%x\n", ret));

		// Destroy handle
		ret = sceNpInGameMessageDeleteHandle(m_LibCtxId, handleId);
		rverify(ret == SCE_OK, catchall, rlError("sceNpInGameMessageDeleteHandle() failed. ret = 0x%x\n", ret));
	}
	rcatchall
	{
		if (handleId != 0)
			sceNpInGameMessageDeleteHandle(m_LibCtxId, handleId);
	}
}

rlNpInGameMessageMgr::rlNpInGameMessageMgr()
	: m_LibCtxId(0)
	, m_bInitialized(false)
{

}

bool rlNpInGameMessageMgr::Init()
{
	if (m_bInitialized)
		return false;

	m_NpDlgt.Bind(this, &rlNpInGameMessageMgr::OnNpEvent);
	g_rlNp.AddDelegate(&m_NpDlgt);

	return true;
}

void rlNpInGameMessageMgr::OnNpEvent(rlNp* /*np*/, const rlNpEvent* evt)
{
	// Wait for a login before initializing the library, the same conditions that make the logins fail
	// will cause this API to fail, thus duplicating our levels of asserts. In-game messages cannot be used
	// until an online login has been made as well.
	if(RLNP_EVENT_ONLINE_STATUS_CHANGED == evt->GetId())
	{
		rlNpEventOnlineStatusChanged* msg = (rlNpEventOnlineStatusChanged*)evt;
		if (msg->IsSignedOnline() && !m_bInitialized)
		{
			int ret = sceNpInGameMessageInitialize(NP_IN_GAME_MESSAGE_POOL_SIZE, NULL);
			if (ret < SCE_OK)
			{
				rlError("sceNpInGameMessageInitialize() failed. ret = 0x%x\n", ret);
				return;
			}

			m_LibCtxId = ret;

			m_PrepareWorkItem.Configure(m_LibCtxId);
			if (!netThreadPool.QueueWork(&m_PrepareWorkItem))
			{
				rlError("netThreadPool failed to queue PrepareMessageWorkItem");
				return;
			}

			m_bInitialized = true;	
		}
	}
}

int rlNpInGameMessageMgr::SendMessage(const int localGamerIndex, const rlGamerHandle& dest, const void* data, const unsigned size)
{
	int ret = SCE_OK;

	if (size > SCE_NP_IN_GAME_MESSAGE_DATA_SIZE_MAX)
	{
		rlWarning("rlNpInGameMessageMgr::SendMessage called with size %d, Max Size is %d", size, SCE_NP_IN_GAME_MESSAGE_DATA_SIZE_MAX);
		return SCE_NP_IN_GAME_MESSAGE_ERROR_INVALID_ARGUMENT;
	}

	if (m_bInitialized)
	{
		SceNpPeerAddress to;
		memset(&to, 0, sizeof(to));
		to.onlineId = dest.GetNpOnlineId().AsSceNpOnlineId();
		to.platform = SCE_NP_PLATFORM_TYPE_ORBIS;

		SceNpPeerAddress from;
		memset(&from, 0, sizeof(from));
		from.onlineId = g_rlNp.GetNpOnlineId(localGamerIndex).AsSceNpOnlineId();
		from.platform = SCE_NP_PLATFORM_TYPE_ORBIS;

		SceNpInGameMessageData message;
		memset(&message, 0, sizeof(message));
		memcpy(message.data, data, size);
		message.dataSize = size;

		ret = sceNpInGameMessageSendData( m_LibCtxId, &to, &from, &message);
		if(ret < 0)
		{
			rlError("sceNpInGameMessageSendData() failed. ret = 0x%x\n", ret);
			return ret;
		}

		rlDebug3("InGameMessage sent from %s to %s",  from.onlineId.data, to.onlineId.data);
	}
	else
	{
		rlError("rlNpInGameMessageMgr::SendMessage called when not initialized.");
	}
	
	return ret;
}

int rlNpInGameMessageMgr::SendMessage(const int localGamerIndex, const rlGamerHandle* recipients, const unsigned numRecipients, const void * data, const unsigned size)
{
	int ret = SCE_OK;
	for (int i = 0; i < numRecipients; i++)
	{
		ret = SendMessage(localGamerIndex, recipients[i], data, size);
		if (ret != SCE_OK)
			break;
	}

	return ret;
}

void rlNpInGameMessageMgr::Shutdown()
{
	if (m_bInitialized)
	{
		sceNpInGameMessageTerminate(m_LibCtxId);
	}

	g_rlNp.RemoveDelegate(&m_NpDlgt);
}

} // namespace rage

#endif // RSG_ORBIS