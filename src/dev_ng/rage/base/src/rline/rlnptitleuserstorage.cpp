// 
// rline/rlnptitleuserstorage.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_NP

#include "rlnptitleuserstorage.h"
#include "diag/seh.h"
#include "net/status.h"
#include "rline/rlnp.h"
#include "rline/rlnptitleid.h"

#if __USE_TUS

#pragma comment(lib, "sysutil_np_tus_stub")

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline_np, tus)
#undef __rage_channel
#define __rage_channel rline_np_tus

//------------------------------------------------------------------------------
//struct rlNpTitleUserStorage::Transaction
//------------------------------------------------------------------------------
rlNpTitleUserStorage::Transaction::Transaction()
: m_TransCtxId(0)
, m_Status(0)
, m_TransResult(0)
{
}

rlNpTitleUserStorage::Transaction::~Transaction()
{
    Reset();
}

void 
rlNpTitleUserStorage::Transaction::Reset()
{
    if(m_Status)
    {
        if(m_Status->Pending())
        {
            Cancel();
            rlWarning("Reset() while download in progress");
        }

        m_Status = 0;
    }

    if(m_TransCtxId)
    {
        sceNpTusDestroyTransactionCtx(m_TransCtxId);
        m_TransCtxId = 0;
    }
}

bool 
rlNpTitleUserStorage::Transaction::IsPending() const
{
    return m_Status && m_Status->Pending();
}

void
rlNpTitleUserStorage::Transaction::Update()
{
    if(IsPending())
    {
        int err = sceNpTusPollAsync (m_TransCtxId, &m_TransResult);

        if(err != 1) //1 == in progress
        {
            if(err == 0)
            {
                if (m_TransResult >= 0)
                {
                    rlDebug("Title user storage transaction finished");
                    m_Status->SetSucceeded();
                }
                else
                {
                    rlError("Title user storage transaction failed (result=0x%08x)", m_TransResult);
                    m_Status->SetFailed();
                }
            }
            else if(err < 0)
            {
                rlError("Title user storage transaction failed (err=0x%08x)", err);
                m_Status->SetFailed();
            }

            Reset();
        }
    }
}

void 
rlNpTitleUserStorage::Transaction::Cancel()
{
     if(rlVerify(IsPending()))
     {
          sceNpTusAbortTransaction(m_TransCtxId);
          m_Status->SetCanceled();
     }
}

//------------------------------------------------------------------------------
//class rlNpTitleUserStorage
//------------------------------------------------------------------------------
rlNpTitleUserStorage::rlNpTitleUserStorage()
: m_Np(NULL)
, m_TitleCtxId(0)
, m_Initialized(false)
{
}

rlNpTitleUserStorage::~rlNpTitleUserStorage()
{
    Shutdown();
}

bool 
rlNpTitleUserStorage::IsInitialized() const
{
    return m_Initialized;
}

bool
rlNpTitleUserStorage::IsPending() const
{
    return m_Transaction.IsPending();
}

void
rlNpTitleUserStorage::Drain()
{
    while (m_Transaction.IsPending())
    {
        m_Transaction.Update();
        sysIpcSleep(1);
    }
}

bool
rlNpTitleUserStorage::Init(rlNp* np)
{
    rlAssert(np);

    bool success = false;

    rlDebug("rlNpTitleUserStorage::Init()");

    bool tusAlreadyInitialized = false;

    rtry
    {
        rverify(!m_Initialized,catchall,rlError("Already initialized"));

        int err = sceNpTusInit(0);

        rcheck(!err || (err == (int)SCE_NP_ERROR_ALREADY_INITIALIZED),
                catchall,
                rlError("Error calling sceNpTusInit (err=0x%08x)", err));


        if(err == (int)SCE_NP_ERROR_ALREADY_INITIALIZED)
        {
            tusAlreadyInitialized = true;
            rlDebug("NP title user storage was already initialized");
        }

        rcheck(np->GetNpId(),
            catchall,
            rlError("Could not get NP ID; possibly not logged in yet"));

        rlAssert(0 == m_TitleCtxId);
        m_TitleCtxId = sceNpTusCreateTitleCtx(rlNp::GetTitleId().GetCommunicationId(),
                                                rlNp::GetTitleId().GetCommunicationPassphrase(),
                                                np->GetNpId());

        rcheck(m_TitleCtxId > 0,
                TusTerm,
               rlError("Failed to create title user storage context ID"));

        m_Np = np;
        m_Np->AddDelegate(&m_NpDlgt);
        m_NpDlgt.Bind(this, &rlNpTitleUserStorage::OnNpEvent);

        success = true;

        m_Initialized = success;
    }
    rcatch(TusTerm)
    {
        if(!tusAlreadyInitialized)
        {
            sceNpTusTerm();
        }

        rthrow(catchall,);
    }
    rcatchall
    {
        m_TitleCtxId = 0;
    }

    return m_Initialized;
}

void 
rlNpTitleUserStorage::Shutdown()
{
    rlDebug("rlNpTitleUserStorage::Shutdown()");

    if(m_Initialized)
    {
        m_Transaction.Reset();

        if(m_TitleCtxId)
        {
            sceNpTusDestroyTitleCtx(m_TitleCtxId);
            m_TitleCtxId = 0;
        }

        if(m_Np)
        {
            m_Np->RemoveDelegate(&m_NpDlgt);
            m_Np = 0;
        }

        sceNpTusTerm();

        m_Initialized = false;
    }
}

void
rlNpTitleUserStorage::Update()
{
    rlAssert(m_Initialized);
    
    //Update any pending title storage download.
    m_Transaction.Update();
}

bool 
rlNpTitleUserStorage::Download(int slot,
                               char* buf,
                               const unsigned bufSize,
                               const unsigned timeoutMs,
                               netStatus* status)
{
    rlAssert(buf && bufSize && status && !status->Pending());

    rtry
    {
        rcheck(rlVerify(m_Initialized),
               catchall,
               rlError("NP title user storage not initialized yet; cannot access title use storage data"));

        rcheck(!m_Transaction.IsPending(),
               catchall,
               rlError("NP title user storage transaction already in progress"));

        rcheck(m_Np->GetNpId(),
               catchall,
               rlError("Could not get NP ID; possibly not logged in yet"));

        rcheck(m_TitleCtxId > 0,
                catchall,
                rlError("Title context id has not been initialized"));

        m_Transaction.m_TransCtxId = sceNpTusCreateTransactionCtx(m_TitleCtxId);
        rcheck(m_Transaction.m_TransCtxId > 0,
               catchall,
               rlError("Failed to create NP title user storage transaction context ID"));

        unsigned timeoutUs = timeoutMs * 1000; //Timeout is in microseconds.

        const unsigned MIN_TIMEOUT_US = 10 * 1000 * 1000;
        if(timeoutUs < MIN_TIMEOUT_US)
        {
            timeoutUs = MIN_TIMEOUT_US;
        }

        int err = sceNpTusSetTimeout(m_Transaction.m_TransCtxId, timeoutUs);
        rcheck(0 == err,
               catchall,
               rlError("sceNpTusSetTimeout failed (0x%08x)", err));

        err = sceNpTusGetDataAsync (m_Transaction.m_TransCtxId, m_Np->GetNpId(), 
            slot, &m_Transaction.m_TusStatus, sizeof(SceNpTusDataStatus),
            buf, bufSize, NULL);
        rcheck(0 == err,
               catchall,
               rlError("sceNpTusGetDataAsync failed (0x%08x)", err));

        m_Transaction.m_Status = status;
        m_Transaction.m_Status->SetPending();
        m_Transaction.m_TransResult = 0;
    }
    rcatchall
    {
        m_Transaction.Reset();
        status->ForceFailed();
        return false;
    }
    return true;
}

bool 
rlNpTitleUserStorage::Upload(int slot,
                             const void* buf,
                             const unsigned bufSize,
                             const unsigned timeoutMs,
                             netStatus* status)
{
    rlAssert(buf && bufSize && status && !status->Pending());

    rtry
    {
        rcheck(rlVerify(m_Initialized),
            catchall,
            rlError(("NP title user storage not initialized yet; cannot access title use storage data")));

        rcheck(!m_Transaction.IsPending(),
            catchall,
            rlError("NP title user storage transaction already in progress"));

        rcheck(m_Np->GetNpId(),
            catchall,
            rlError(("Could not get NP ID; possibly not logged in yet")));

        rcheck(m_TitleCtxId > 0,
                catchall,
                rlError("Title context id has not been initialized"));

        m_Transaction.m_TransCtxId = sceNpTusCreateTransactionCtx(m_TitleCtxId);
        rcheck(m_Transaction.m_TransCtxId > 0,
            catchall,
            rlError("Failed to create title user storage transaction context ID"));

        unsigned timeoutUs = timeoutMs * 1000; //Timeout is in microseconds.

        const unsigned MIN_TIMEOUT_US = 10 * 1000 * 1000;
        if(timeoutUs < MIN_TIMEOUT_US)
        {
            timeoutUs = MIN_TIMEOUT_US;
        }

        int err = sceNpTusSetTimeout (m_Transaction.m_TransCtxId, timeoutUs);
        rcheck(0 == err,
            catchall,
            rlError("sceNpTusSetTimeout failed (0x%08x)", err));

        err = sceNpTusSetDataAsync (m_Transaction.m_TransCtxId, m_Np->GetNpId(), 
            slot, bufSize, bufSize, buf, NULL, sizeof(SceNpTusDataStatus), NULL);
        rcheck(0 == err,
            catchall,
            rlError("sceNpTusGetDataAsync failed (0x%08x)", err));

        m_Transaction.m_Status = status;
        m_Transaction.m_Status->SetPending();    
        m_Transaction.m_TransResult = 0;
    }
    rcatchall
    {
        m_Transaction.Reset();
        status->ForceFailed();
        return false;
    }
    return true;
}


bool 
rlNpTitleUserStorage::Delete(int slot, const unsigned timeoutMs, netStatus* status)
{
    rlAssert(status && !status->Pending());

    rtry
    {
        rcheck(rlVerify(m_Initialized), catchall, rlError(("NP title user storage not initialized yet; cannot access title use storage data")));
        rcheck(!m_Transaction.IsPending(),  catchall, rlError("NP title user storage transaction already in progress"));
        rcheck(m_Np->GetNpId(),             catchall, rlError(("Could not get NP ID; possibly not logged in yet")));

        rcheck(m_TitleCtxId > 0,
                catchall,
                rlError("Title context id has not been initialized"));

        m_Transaction.m_TransCtxId = sceNpTusCreateTransactionCtx(m_TitleCtxId);
        rcheck(m_Transaction.m_TransCtxId > 0, catchall, rlError("Failed to create title user storage transaction context ID"));

        unsigned timeoutUs = timeoutMs * 1000; //Timeout is in microseconds.

        const unsigned MIN_TIMEOUT_US = 10 * 1000 * 1000;
        if(timeoutUs < MIN_TIMEOUT_US)
        {
            timeoutUs = MIN_TIMEOUT_US;
        }

        int err = sceNpTusSetTimeout (m_Transaction.m_TransCtxId, timeoutUs);
        rcheck(0 == err, catchall, rlError("sceNpTusSetTimeout failed (0x%08x)", err));

        err = sceNpTusDeleteMultiSlotDataAsync(m_Transaction.m_TransCtxId,  // Transaction context ID.
                                                          m_Np->GetNpId(),  // NP ID of the target user.
                                                                    &slot,  // Pointer to the array storing the target slot IDs.
                                                                        1,  // Valid number of elements for slotIdArray. Maximum is 51.
                                                                     NULL); // Option for future extension. Always specify NULL.
        rcheck(0 == err, catchall, rlError("sceNpTusDeleteMultiSlotDataAsync failed (0x%08x)", err));

        m_Transaction.m_Status = status;
        m_Transaction.m_Status->SetPending();    
        m_Transaction.m_TransResult = 0;
    }
    rcatchall
    {
        m_Transaction.Reset();
        status->ForceFailed();
        return false;
    }

    return true;
}

void 
rlNpTitleUserStorage::Cancel()
{
    m_Transaction.Cancel();
}


void 
rlNpTitleUserStorage::OnNpEvent(rlNp* /*np*/, const rlNpEvent* evt)
{
    if(RLNP_EVENT_ONLINE_STATUS_CHANGED == evt->GetId())
    {
        rlNpEventOnlineStatusChanged* msg = (rlNpEventOnlineStatusChanged*)evt;

        if(!msg->IsSignedOnline())
        {
            rlDebug("NP went offline, resetting");
            m_Transaction.Reset();
        }
    }
}

}   //namespace rage

#endif //__USE_TUS
#endif //RSG_NP
