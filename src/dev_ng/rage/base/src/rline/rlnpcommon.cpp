// 
// rline/rlnpcommon.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
 
#if RSG_NP

#include "rlnpcommon.h"

namespace rage
{

AUTOID_IMPL(rlNpEvent);
AUTOID_IMPL(rlNpEventOnlineStatusChanged);
AUTOID_IMPL(rlNpEventActingUserChanged);
AUTOID_IMPL(rlNpEventPresenceStatusChanged);
AUTOID_IMPL(rlNpEventFriendPresenceUpdate);
AUTOID_IMPL(rlNpEventFriendListUpdate);
AUTOID_IMPL(rlNpEventMsgReceived);
AUTOID_IMPL(rlNpEventInviteReceived);
AUTOID_IMPL(rlNpEventInviteAccepted);
AUTOID_IMPL(rlNpEventInviteAcceptedWhileOffline);
AUTOID_IMPL(rlNpEventJoinSession);
AUTOID_IMPL(rlNpEventSetPresenceData);
#if RSG_ORBIS
AUTOID_IMPL(rlNpEventNpUnavailable);
AUTOID_IMPL(rlNpEventUserServiceStatusChanged);
AUTOID_IMPL(rlNpEventPlayStationPlusInvalid);
AUTOID_IMPL(rlNpEventPlayStationPlusUpdate);
AUTOID_IMPL(rlNpEventInviteRejectedNpUnavailable);
AUTOID_IMPL(rlNpEventPlayTogetherHost);
#endif
AUTOID_IMPL(rlNpEventBlocklistUpdated);
AUTOID_IMPL(rlNpEventBlocklistRetrieved);

}

#endif
