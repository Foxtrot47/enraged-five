// 
// rline/rlnptrophy.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_NP

#include "rlnptrophy.h"
#include "rlnp.h"
#include "rlachievement.h"
#include "diag/seh.h"
#include "file/asset.h"

#include <np.h>
#include <np/np_trophy.h>

#ifdef __SNC__
#pragma diag_suppress 552
#endif

#include <libsysmodule.h>
#pragma comment(lib,"libSceNpTrophy_stub_weak.a")

SceNpServiceLabel g_ServiceLabel = 0;

#define ENABLE_PS4_TROPHIES RSG_ORBIS

namespace rage
{

	extern bool g_IsExiting;
	
RAGE_DEFINE_SUBCHANNEL(rline_np, trophy)
#undef __rage_channel
#define __rage_channel rline_np_trophy

bool rlNpTrophy::ms_bTrophyDataHasBeenReinstalled = false;

//------------------------------------------------------------------------------
//class rlNpTrophy
//------------------------------------------------------------------------------
rlNpTrophy::rlNpTrophy()
: m_Initialized(false)
, m_TrophyContextDiskSpaceBytes(-1)
, m_CommId(0)
{
#if RSG_ORBIS
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		m_TrophyContext[i] = SCE_NP_TROPHY_INVALID_CONTEXT;
		m_TrophyHandle[i] = SCE_NP_TROPHY_INVALID_HANDLE;
		m_TrophyWorker[i] = sysIpcThreadIdInvalid;
		m_TrophyRegisterContextResult[i] = -1;
		m_bDirtyContext[i] = false;
	}
#endif
}

rlNpTrophy::~rlNpTrophy()
{
    Shutdown();
}

bool 
rlNpTrophy::Init(const SceNpCommunicationId* commId,
                 const SceNpCommunicationSignature* commSig)
{
    rlDebug("rlNpTrophy::Init()");

#if ENABLE_PS4_TROPHIES
	rtry
	{
		// Check for Trophy File Existing, since it needs to be mapped correctly to prevent a PS4 system crash.
#if !__FINAL
		rcheck(ASSET.Exists("/app0/sce_sys/trophy/trophy00", "trp"), catchall, rlError("trophy pack not found on system at /app0/trophy/trophy000.trp, canceling init"));
#endif

        rverify(!m_Initialized, catchall, rlError("Already initialized"));

		int ret = sceSysmoduleLoadModule(SCE_SYSMODULE_NP_TROPHY);
		if (ret != SCE_OK ) 
		{
			rlError("sceSysmoduleLoadModule(SCE_SYSMODULE_NP_TROPHY) failed. ret = 0x%x\n", ret);
		}

		m_CommId = commId;
        m_Initialized = true;

		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			int userid = g_rlNp.GetUserServiceId(i);
			if (userid != SCE_USER_SERVICE_USER_ID_INVALID)
			{
				ret = sceNpTrophyCreateContext(&m_TrophyContext[i], userid, g_ServiceLabel, 0);
				rverify(ret == SCE_OK, catchall, rlError("sceNpTrophyCreateContext() failed. ret = 0x%x\n", ret));

				ret = sceNpTrophyCreateHandle(&m_TrophyHandle[i]);
				rverify(ret == SCE_OK, catchall, rlError("sceNpTrophyCreateHandle() failed. ret = 0x%x\n", ret));

				m_Closures[i].index = i;
				m_Closures[i].m_TrophyPtr = this;
				m_TrophyWorker[i] = sysIpcCreateThread(RegisterContextWorker, &m_Closures[i], sysIpcMinThreadStackSize, PRIO_BELOW_NORMAL, "[RAGE] TrophyRegister");
				rverify(m_TrophyWorker[i] != sysIpcThreadIdInvalid, catchall, rlError("Could not start trophy worker thread"));
			}
		}
	}
	rcatchall
	{
		Shutdown();
	}

	return m_Initialized;
#else
	m_Initialized = false;
	return m_Initialized;
#endif 
}

void 
rlNpTrophy::Shutdown()
{
    rlDebug("rlNpTrophy::Shutdown()");

	if (m_Initialized)
	{
		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			WaitForWorker(i);
			DestroyContextAndHandles(i);
		}

		int ret = sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_TROPHY);
		if (ret != SCE_OK ) 
		{
			rlError("sceSysmoduleUnloadModule(SCE_SYSMODULE_NP_TROPHY) failed. ret = 0x%x\n", ret);
		}

		m_Initialized = false;
	}
}

s64
rlNpTrophy::GetRequiredDiskSpace()
{
	return 0;
}

bool rlNpTrophy::ReadAchievements(rlAchievementInfo* achievements,
                             const int maxAchievements,
                             int* numAchievements,
							 int ORBIS_ONLY(gamerIndex))
{
    rlAssert(achievements && maxAchievements && numAchievements);
        
    rlDebug("Reading achievements...");

 	rtry
 	{
 		*numAchievements = 0;
 
 		rcheck(m_Initialized, catchall, rlError("rlNpTrophy not initialized"));
 		rcheck(CheckRegisterContextWorker(gamerIndex), catchall, rlError("Failed to register trophy context"));
		rverify(RefreshContextsIfNecessary(gamerIndex), catchall, rlError("Could not read achievements due to bad trophy contexts"));
 
#if ENABLE_PS4_TROPHIES
		SceNpTrophyGameDetails details;
		SceNpTrophyGameData data;        

		memset(&details, 0x00, sizeof(details));
		memset(&data, 0x00, sizeof(data));

		details.size = sizeof(details);
		data.size = sizeof(data);

		int ret = sceNpTrophyGetGameInfo(m_TrophyContext[gamerIndex], m_TrophyHandle[gamerIndex], &details, &data);
		rverify(ret == SCE_OK, catchall, rlError("sceNpTrophyGetGameInfo failed, ret = 0x%x\n", ret));

		rlDebug3("TROPHY: title [%s]",details.title);
		rlDebug3("TROPHY: description [%s]",details.description);
		rlDebug3("TROPHY: %u/%u in game, %u/%u platinum, %u/%u gold, %u/%u silver, %u/%u bronze",
			data.unlockedTrophies,details.numTrophies,
			data.unlockedPlatinum,details.numPlatinum,
			data.unlockedGold,details.numGold,
			data.unlockedSilver,details.numSilver,
			data.unlockedBronze,details.numBronze);

		SceNpTrophyFlagArray unlockedTrophies; 
		SCE_NP_TROPHY_FLAG_ZERO(&unlockedTrophies);

		uint32_t uNumAchievements;
		ret = sceNpTrophyGetTrophyUnlockState(m_TrophyContext[gamerIndex], m_TrophyHandle[gamerIndex], &m_UnlockedTrophies[gamerIndex], &uNumAchievements);
		rverify(ret == SCE_OK, catchall, rlError("sceNpTrophyGetTrophyUnlockState failed. ret = 0x%x\n", ret));
		for (unsigned int i = 0; i < uNumAchievements && (*numAchievements < maxAchievements); i++)
		{
			SceNpTrophyDetails trophyDetails = {0};
			SceNpTrophyData trophyData = {0};
			trophyDetails.size = sizeof(SceNpTrophyDetails);
			trophyData.size = sizeof(SceNpTrophyData);

			int ret = sceNpTrophyGetTrophyInfo(m_TrophyContext[gamerIndex], m_TrophyHandle[gamerIndex], i, &trophyDetails, &trophyData);
			if (ret == SCE_OK)
			{
				achievements[*numAchievements].Reset(i, 10, &trophyDetails.name[0], &trophyDetails.description[0], SCE_NP_TROPHY_FLAG_ISSET(i, &m_UnlockedTrophies[gamerIndex]));
				++(*numAchievements);
			}
		#if __BANK
			else
			{
				rlError("sceNpTrophyGetTrophyInfo failed getting gamer %d's trophy %d with error: 0x%X", gamerIndex, i, ret);
			}
		#endif
		}
#endif

		return true;
	}
 	rcatchall
	{
		return false;
	}
}

bool
rlNpTrophy::WriteAchievement(const int achievementId,
                             int *platinumAchievementId,
							 int ORBIS_ONLY(gamerIndex))
{
    rlAssert(platinumAchievementId);

    rlDebug("Writing achievement %d...", achievementId);

	rtry
	{
		RefreshContextsIfNecessary(gamerIndex);

		*platinumAchievementId = SCE_NP_TROPHY_INVALID_TROPHY_ID;

		rcheck(m_Initialized, catchall, rlError("rlNpTrophy not initialized"));
		rcheck(CheckRegisterContextWorker(gamerIndex), catchall, rlError("Failed to register trophy context"));

		int ret = sceNpTrophyUnlockTrophy(m_TrophyContext[gamerIndex], m_TrophyHandle[gamerIndex], achievementId, platinumAchievementId);
		rverify(ret == SCE_OK || SCE_NP_TROPHY_ERROR_TROPHY_ALREADY_UNLOCKED == ret, catchall, rlNpError("sceNpTrophyUnlockTrophy failed", ret));

		if(SCE_NP_TROPHY_ERROR_TROPHY_ALREADY_UNLOCKED == ret)
		{
			rlWarning("Trophy %d was already unlocked, this shouldn't happen unless you cheated with widgets.", achievementId);
		}

		if(SCE_NP_TROPHY_INVALID_TROPHY_ID != *platinumAchievementId)
		{
			rlDebug("Hey, you just unlocked all available trophies, you're awesome!");
		}

		rlDebug("Successfully wrote achievement %d (platinumAchievementId=%d)", achievementId, *platinumAchievementId);
	}
	rcatchall
	{
		return false;
	}

	return true;
}

#if RSG_ORBIS

void  rlNpTrophy::RegisterContextWorker(void* userdata)
{
    RegisterHandleClosure* closure = (RegisterHandleClosure*)userdata;
    rlAssert(closure);


	int ret = sceNpTrophyRegisterContext(closure->m_TrophyPtr->m_TrophyContext[closure->index],
										 closure->m_TrophyPtr->m_TrophyHandle[closure->index], 0);
    if(ret < SCE_OK)
    {
		rlError("sceNpTrophyRegisterContext() failed. ret = 0x%x\n", ret);
    }

    closure->m_TrophyPtr->m_TrophyRegisterContextResult[closure->index] = ret;
}

void rlNpTrophy::WaitForWorker(int index)
{
	if (m_TrophyWorker[index] != sysIpcThreadIdInvalid)
	{
		sysIpcWaitThreadExit(m_TrophyWorker[index]);
		m_TrophyWorker[index] = sysIpcThreadIdInvalid;
	}
}

bool rlNpTrophy::CheckRegisterContextWorker(int index)
{
	WaitForWorker(index);
	return (m_TrophyRegisterContextResult[index] == 0);
}

#endif

#if RSG_ORBIS

void rlNpTrophy::DestroyContextAndHandles(int index)
{
	rlAssert(m_TrophyWorker[index] == sysIpcThreadIdInvalid);
	m_TrophyRegisterContextResult[index] = -1;

	if(SCE_NP_TROPHY_INVALID_HANDLE != m_TrophyHandle[index])
	{
		sceNpTrophyAbortHandle(m_TrophyHandle[index]);
		sceNpTrophyDestroyHandle(m_TrophyHandle[index]);
		m_TrophyHandle[index] = SCE_NP_TROPHY_INVALID_HANDLE;
	}

	if(SCE_NP_TROPHY_INVALID_CONTEXT != m_TrophyContext[index])
	{
		sceNpTrophyDestroyContext(m_TrophyContext[index]);
		m_TrophyContext[index] = SCE_NP_TROPHY_INVALID_CONTEXT;
	}	

	m_bDirtyContext[index] = true;
}

bool rlNpTrophy::RefreshContextsIfNecessary(int index)
{
	rtry
	{
		if (m_bDirtyContext[index])
		{
			int ret = SCE_OK;
			WaitForWorker(index);
			DestroyContextAndHandles(index);

			int userid = g_rlNp.GetUserServiceId(index);
			if (userid != SCE_USER_SERVICE_USER_ID_INVALID)
			{
				ret = sceNpTrophyCreateContext(&m_TrophyContext[index], userid, g_ServiceLabel, 0);
				rverify(ret == SCE_OK, catchall, rlError("sceNpTrophyCreateContext() failed. ret = 0x%x\n", ret));

				ret = sceNpTrophyCreateHandle(&m_TrophyHandle[index]);
				rverify(ret == SCE_OK, catchall, rlError("sceNpTrophyCreateHandle() failed. ret = 0x%x\n", ret));

				ret = sceNpTrophyRegisterContext(m_TrophyContext[index], m_TrophyHandle[index], 0);
				rverify(ret == SCE_OK, catchall, rlError("sceNpTrophyRegisterContext() failed. ret = 0x%x\n", ret));
				m_TrophyRegisterContextResult[index] = ret;
			}

			SCE_NP_TROPHY_FLAG_ZERO(&m_UnlockedTrophies[index]);
			m_bDirtyContext[index] = false;
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void rlNpTrophy::OnUserLoginStatuschange(int localGamerIndex)
{
	m_bDirtyContext[localGamerIndex] = true;
}
#endif

}   //namespace rage

#endif	// RSG_NP
