// 
// rline/rlfriendevents.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlfriendevents.h"

namespace rage
{
	AUTOID_IMPL(rlFriendEvent);
	AUTOID_IMPL(rlFriendsManagerReadyEvent);
	AUTOID_IMPL(rlFriendEventListChanged);
	AUTOID_IMPL(rlFriendEventStatusChanged);
	AUTOID_IMPL(rlFriendEventUpdated);
	AUTOID_IMPL(rlFriendEventTitleChanged);
	AUTOID_IMPL(rlFriendEventSessionChanged);
	AUTOID_IMPL(rlFriendEventNeedsRecheck);

	/////////////////////////////////////////////////////////////////////////////////
	// rlFriendsManagerReadyEvent
	/////////////////////////////////////////////////////////////////////////////////
	rlFriendsManagerReadyEvent::rlFriendsManagerReadyEvent(const int localGamerIndex)
	{
		m_LocalGamerIndex = localGamerIndex;
	}

	/////////////////////////////////////////////////////////////////////////////////
	// rlFriendEventUpdated
	/////////////////////////////////////////////////////////////////////////////////
	rlFriendEventUpdated::rlFriendEventUpdated(const rlFriend& pFriend)
	{
		m_Friend = pFriend;
	}

	/////////////////////////////////////////////////////////////////////////////////
	// rlFriendEventStatusChanged
	/////////////////////////////////////////////////////////////////////////////////
	rlFriendEventStatusChanged::rlFriendEventStatusChanged(const rlFriend& pFriend)
	{
		m_Friend = pFriend;
	}

	/////////////////////////////////////////////////////////////////////////////////
	// rlFriendEventTitleChanged
	/////////////////////////////////////////////////////////////////////////////////
	rlFriendEventTitleChanged::rlFriendEventTitleChanged(const rlFriend& pFriend)
	{
		m_Friend = pFriend;
	}

	/////////////////////////////////////////////////////////////////////////////////
	// rlFriendEventSessionChanged
	/////////////////////////////////////////////////////////////////////////////////
	rlFriendEventSessionChanged::rlFriendEventSessionChanged(const rlFriend& pFriend)
	{
		m_Friend = pFriend;
	}


}   //namespace rage
