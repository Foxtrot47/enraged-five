// 
// rline/rlfriend.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlfriend.h"
#include "diag/seh.h"
#include "rltitleid.h"
#include "system/bit.h"
#include "string/string.h"
#include "string/stringhash.h"
#include <string.h>

#if RSG_ORBIS
#include "rlnp.h"
#include <np.h>
#elif RSG_DURANGO
#include "rline/durango/rlxblplayers_interface.h"
#endif

namespace rage
{

extern const rlTitleId* g_rlTitleId;

///////////////////////////////////////////////////////////////
// rlFriendId
///////////////////////////////////////////////////////////////
rlFriendId::rlFriendId()
{
	Clear();
}

rlFriendId::rlFriendId(const rlGamerHandle& handle)
{
	Clear();

#if RSG_PC
	m_RockstarId = handle.GetRockstarId();
#elif RSG_ORBIS
	m_AccountId = handle.GetNpAccountId();
	m_OnlineId.FromString(handle.GetNpOnlineId().data);
	m_HandleHash = atStringHash(m_OnlineId.data);
#elif RSG_DURANGO
	m_Xuid = handle.GetXuid();
#endif
}

void rlFriendId::Clear()
{
#if RSG_PC
	m_RockstarId = RL_INVALID_ROCKSTAR_ID;
#elif RSG_ORBIS
	m_AccountId = RL_INVALID_NP_ACCOUNT_ID;
	m_OnlineId.Clear();
	m_HandleHash = 0;
#elif RSG_DURANGO
	m_Xuid = 0;
#endif
}

bool rlFriendId::operator==( const rlFriendId& that ) const
{
#if RSG_PC
	return m_RockstarId == that.m_RockstarId;
#elif RSG_ORBIS
	return m_HandleHash == that.m_HandleHash;
#elif RSG_DURANGO
	return m_Xuid == that.m_Xuid;
#endif
}

bool rlFriendId::operator<(const rlFriendId& that) const 
{ 
#if RSG_PC
	return m_RockstarId < that.m_RockstarId;
#elif RSG_ORBIS
	return m_HandleHash < that.m_HandleHash;
#elif RSG_DURANGO
	return m_Xuid < that.m_Xuid;
#endif
}

bool rlFriendsReference::operator==( const rlFriendsReference& that ) const
{
	return m_Id == that.m_Id;
}

bool rlFriendId::IsValid() const
{
#if RSG_PC
	return m_RockstarId != InvalidRockstarId;	
#elif RSG_ORBIS
	return m_HandleHash > 0;
#elif RSG_DURANGO
	return m_Xuid > 0;
#endif
}

///////////////////////////////////////////////////////////////
// rlFriend
///////////////////////////////////////////////////////////////
rlFriend::rlFriend()
{
    this->Clear();
}

rlFriend::~rlFriend()
{
}

void rlFriend::Clear()
{

#if RSG_NP
	m_FriendReference.Clear();
	m_PresenceBlobSize = 0;
	sysMemSet(m_PresenceBlob, 0, sizeof(m_PresenceBlob));
#elif RSG_PC
	m_ScFriend.Clear();
	m_Flags = 0;
#elif RSG_DURANGO
	m_FriendReference.Clear();
	m_XblFriendInfo.Clear();
#endif
}

rlGamerHandle* rlFriend::GetGamerHandle(rlGamerHandle* hGamer) const
{
    hGamer->Clear();

#if RSG_ORBIS
    hGamer->ResetNp(g_rlNp.GetEnvironment(), m_FriendReference.m_Id.m_AccountId, &m_FriendReference.m_Id.m_OnlineId);
#elif RSG_PC
    hGamer->ResetSc(m_ScFriend.m_RockstarId);
#elif RSG_DURANGO
	hGamer->ResetXbl(m_FriendReference.m_Id.m_Xuid);
#endif
    return hGamer;
}

const char* rlFriend::GetName() const
{
#if RSG_ORBIS
    return m_FriendReference.m_Id.m_OnlineId.data;
#elif RSG_PC
    return m_ScFriend.m_Nickname;
#elif RSG_DURANGO
	return m_XblFriendInfo.m_Nickname;
#else
    return "";
#endif
}

const char* rlFriend::GetDisplayName() const
{
#if RSG_DURANGO
	return m_XblFriendInfo.m_DisplayName;
#else
	return GetName();
#endif
}

//PURPOSE
// Returns true if the gamer handle is valid
const bool rlFriend::IsValid() const
{
#if RSG_ORBIS
	return m_FriendReference.IsValid();
#elif RSG_PC
	return m_ScFriend.IsValid();
#elif RSG_DURANGO
	return m_FriendReference.IsValid();
#else
	return false;
#endif
}

bool rlFriend::IsOnline() const
{
#if RSG_PC
	return m_ScFriend.m_IsOnline;
#else
   return m_FriendReference.m_bIsOnline;
#endif
}

bool rlFriend::IsInSameTitle() const
{
#if RSG_PC
	return m_ScFriend.m_IsPlayingSameTitle;
#else
	return m_FriendReference.m_bIsInSameTitle;
#endif
}

bool rlFriend::IsInSession() const
{
#if RSG_PC
	return (m_Flags & FLAG_IS_IN_JOINABLE_SESSION) != 0;
#else
	return m_FriendReference.m_bIsInSession;
#endif
}

bool rlFriend::IsFavorite() const
{
#if RSG_DURANGO
	return m_XblFriendInfo.m_bIsFavorite;
#else
	rlAssert(false);
	return false;
#endif
}

void rlFriend::SetIsOnline(bool bOnline)
{
#if RSG_PC
	m_ScFriend.m_IsOnline = bOnline;
#else
	m_FriendReference.m_bIsOnline = bOnline;
#endif
}

void rlFriend::SetIsInSameTitle(bool bInSameTitle)
{
#if RSG_PC
	m_ScFriend.m_IsPlayingSameTitle = bInSameTitle;
#else
	m_FriendReference.m_bIsInSameTitle = bInSameTitle;
#endif
}

void rlFriend::SetIsOnline(bool bIsOnline, bool bIsPlayingSameTitle)
{
	SetIsOnline(bIsOnline);
	SetIsInSameTitle(bIsPlayingSameTitle);
}

void rlFriend::SetIsInSession(bool bInSession)
{
#if RSG_PC
	if (bInSession)
	{
		m_Flags |= FLAG_IS_IN_JOINABLE_SESSION;
	}
	else
	{
		m_Flags &= ~FLAG_IS_IN_JOINABLE_SESSION;
	}
#else
	m_FriendReference.m_bIsInSession = bInSession;
#endif
}

void rlFriend::SetFavorite(bool DURANGO_ONLY(bFavorite))
{
#if RSG_DURANGO
	m_XblFriendInfo.m_bIsFavorite = bFavorite;
#else
	rlAssert(false);
#endif
}

bool rlFriend::IsPendingFriend() const
{
    return IsPendingFriendInvitee() || IsPendingFriendInviter();
}

bool rlFriend::IsPendingFriendInvitee() const
{
    return false;
}

bool rlFriend::IsPendingFriendInviter() const
{
#if RSG_DURANGO
	return !m_FriendReference.m_bIsTwoWay;
#else
    return false;
#endif
}

bool rlFriend::IsPlayingLastGen() const
{
#if RSG_ORBIS
	return false;//!m_UserInfo.isPS4;
#else
	return false;
#endif
}

bool rlFriend::operator==(const rlFriend& that) const
{
	rlGamerHandle thisGamer;
	rlGamerHandle thatGamer;

	rtry
	{
		if (!IsValid() && !that.IsValid())
		{
			return true;
		}
		else if (!IsValid())
		{
			return false;
		}
		else if (!that.IsValid())
		{
			return false;
		}

		rverify(this->GetGamerHandle(&thisGamer), catchall, );
		rverify(that.GetGamerHandle(&thatGamer), catchall, );
		
		// Identical gamer handles
		if (thisGamer == thatGamer)
			return true;

#if RSG_ORBIS
		// Allow a loose mapping between a gamer with the same online id but a different account id.
		if (!stricmp(thisGamer.GetNpOnlineId().data, thatGamer.GetNpOnlineId().data))
			return true;
#endif
	}
	rcatchall
	{

	}

	return false;
}

bool rlFriend::operator!=(const rlFriend& that) const
{
    return !(*this == that);
}

#if RSG_NP

void rlFriend::Reset(rlSceNpAccountId accountId, const rlSceNpOnlineId& onlineId, bool bIsOnline, bool bIsInSameTitle, char* blob, const unsigned blobSize)
{
	Clear();
	m_FriendReference.m_bIsOnline = bIsOnline;
	m_FriendReference.m_bIsInSameTitle = bIsInSameTitle;

	m_PresenceBlobSize = blobSize;
	
	if (m_PresenceBlobSize > 0)
	{
		m_FriendReference.m_bIsInSession = true;
	}

	sysMemCpy(m_PresenceBlob, blob, blobSize);

	m_FriendReference.m_Id.m_AccountId = accountId;
	m_FriendReference.m_Id.m_OnlineId = onlineId;
	m_FriendReference.m_Id.m_HandleHash = atStringHash(onlineId.data);
}

bool rlFriend::GetPresenceBlob(const int localGamerIndex, void* blob, unsigned* blobSize, const unsigned maxBlobSize) const
{
	rtry
	{
		rverify(maxBlobSize >= m_PresenceBlobSize, catchall, );
		*blobSize = m_PresenceBlobSize;
		if (m_PresenceBlobSize > 0)
		{
			sysMemCpy(blob, m_PresenceBlob, m_PresenceBlobSize);
		}
		return true;
	}
	rcatchall
	{
		return false;
	}
}

#elif RSG_PC

void rlFriend::Reset(const rlScFriend& scFriend, unsigned flags)
{
	Clear();

    m_ScFriend = scFriend;
	m_Flags = flags;
}

RockstarId rlFriend::GetRockstarId() const
{
	return m_ScFriend.m_RockstarId;
}

#if __RGSC_DLL
const rlScFriend& rlFriend::GetScFriend() const
{
	return m_ScFriend;
}

void rlFriend::SetRichPresence(const char* richPresence)
{
	safecpy(m_ScFriend.m_RichPresence, richPresence);
}

const char* rlFriend::GetRichPresence()
{
	return m_ScFriend.m_RichPresence;
}

#endif

#elif RSG_DURANGO

void rlFriend::Reset(const u64 xuid, const rlXblFriendInfo& xblFriend, bool bIsOnline, bool bIsInSameTitle, bool bTwoWay, bool bIsInSession)
{
	Clear();
	m_FriendReference.m_Id.m_Xuid = xuid;
	m_FriendReference.m_bIsOnline = bIsOnline;
	m_FriendReference.m_bIsInSameTitle = bIsInSameTitle;
	m_FriendReference.m_bIsTwoWay = bTwoWay;
	m_FriendReference.m_bIsInSession = bIsInSession;
	m_XblFriendInfo = xblFriend;
}

u64 rlFriend::GetXuid() const
{
	return m_FriendReference.m_Id.m_Xuid;
}

void rlFriend::SetDisplayName(const char * displayName)
{
	safecpy(m_XblFriendInfo.m_DisplayName, displayName);
}

bool rlFriend::HasDisplayName() const
{
	return m_XblFriendInfo.m_DisplayName[0] != '\0';
}

void rlFriend::SetNickname(const char* username)
{
	safecpy(m_XblFriendInfo.m_Nickname, username);
}

//PURPOSE
// Set is two-way friend  (xb1 feature)
void rlFriend::SetIsTwoWay(bool bTwoWay)
{
	m_FriendReference.m_bIsTwoWay = bTwoWay;
}

//PURPOSE
// Returns the rlXblFriend
rlXblFriendInfo rlFriend::GetXblFriendInfo() const
{
	return m_XblFriendInfo;
}

//PURPOSE
// Sets the session info
void rlFriend::SetSession(rlSessionInfo& session)
{
	m_FriendReference.m_bIsInSession = session.IsValid();

	if (!m_FriendReference.m_bIsOnline && m_FriendReference.m_bIsInSession)
	{
#if SESSION_PRESENCE_OVERRIDE
		rlWarning("Presence server says %s is offline, but we think they are in a session. Forcing ONLINE and IN SAME TITLE.", GetName());
		m_FriendReference.m_bIsInSameTitle = true;
		m_FriendReference.m_bIsOnline = true;
#endif
	}
}

#endif

}   //namespace rage
