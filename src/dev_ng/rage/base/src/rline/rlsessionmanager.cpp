// 
// rline/rlsessionmanager.cpp
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "rlsessionmanager.h"

#include "diag/seh.h"
#include "net/connectionmanager.h"
#include "net/nethardware.h"
#include "net/task.h"
#include "net/transaction.h"
#include "rldiag.h"
#include "rlpresence.h"
#include "rlsession.h"
#include "rlsessionfinder.h"
#include "system/timer.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, sessionmanager)
#undef __rage_channel
#define __rage_channel rline_sessionmanager

#define QUERY_RESULT_RESPONSE_RECEIVED (0)
#define QUERY_RESULT_TIMED_OUT (1)

#define SESSION_DETAIL_REQUEST_VERSION (11)
#define SESSION_VERSION_BITS (8) //< bits needed to sync SESSION_DETAIL_REQUEST_VERSION - should have breathing room to cope with different versions

static unsigned s_rlSesMgrDiscriminator = 0;
static netRandom s_rlSesMgrRng;

//PURPOSE
//  Sent to a session host to retrieve session config
//NOTES
//  H->P:       NO
//  P->Host:    YES
//  P->H:       NO
struct rlSessionDetailRequest
{
    NET_MESSAGE_DECL(rlSessionDetailRequest, RL_SESSION_DETAIL_REQUEST);
	NET_MESSAGE_IMPORT_STUB_DECL();

	enum 
	{
		REQUEST_FLAG_DATA_MINE = BIT0,
		REQUEST_FLAG_NUM = 1,
	};

    void Reset(const rlSessionInfo& sessionInfo, unsigned flags)
    {
		m_Version = SESSION_DETAIL_REQUEST_VERSION;
		m_Discriminator = s_rlSesMgrDiscriminator;
		m_UniqueID = static_cast<u32>(s_rlSesMgrRng.GetInt());
		m_SessionInfo = sessionInfo;
		m_Flags = flags;
		m_GamerHandle.Clear();
		m_bHasGamerHandle = false;
		rlPresence::GetName(rlPresence::GetActingUserIndex(), m_FromGamerName);
    }

	void Reset(const rlSessionInfo& sessionInfo, unsigned flags, const rlGamerHandle& gamerHandle)
	{
		Reset(sessionInfo, flags);
		m_GamerHandle = gamerHandle;
		m_bHasGamerHandle = gamerHandle.IsValid();
	}

    NET_MESSAGE_SER(bb, msg)
    {
		return bb.SerUns(msg.m_Version, SESSION_VERSION_BITS) &&
			   bb.SerUns(msg.m_Discriminator, 32) &&
			   bb.SerUns(msg.m_UniqueID, 32) &&
			   bb.SerStr(msg.m_FromGamerName, RL_MAX_NAME_BUF_SIZE) &&
			   bb.SerUser(msg.m_SessionInfo) &&
			   bb.SerUns(msg.m_Flags, REQUEST_FLAG_NUM) &&
			   bb.SerBool(msg.m_bHasGamerHandle) && 
			   (msg.m_bHasGamerHandle ? bb.SerUser(msg.m_GamerHandle) : true);
    }

	NET_MESSAGE_IMPORT_STUB(bb, msg)
	{
		return bb.SerUns(msg.m_Version, SESSION_VERSION_BITS);
	}

	u32 m_Version;
	u32 m_Discriminator;
	u32 m_UniqueID;
	u32 m_Flags; 
	char m_FromGamerName[RL_MAX_NAME_BUF_SIZE];
	rlSessionInfo m_SessionInfo;
	rlGamerHandle m_GamerHandle;
	bool m_bHasGamerHandle;
};

//PURPOSE
//  Response to a session config request.
//NOTES
//  H->P:       YES
//  P->Host:    NO
//  P->H:       NO
struct rlSessionDetailResponse
{
    NET_MESSAGE_DECL(rlSessionDetailResponse, RL_SESSION_DETAIL_RESPONSE);

    rlSessionDetailResponse()
    {
        //Default to invalid, to pick up any rogue code paths
        m_Response = RESPONSE_INVALID_SESSION;
		m_ResponseParam = 0;
		m_UniqueID = 0;
    }

    enum 
    {
		RESPONSE_INVALID_DATA,
		RESPONSE_INVALID_VERSION,
		RESPONSE_INVALID_DISCRIMINATOR,
		RESPONSE_INVALID_SESSION,
		RESPONSE_NOT_HOSTING,
		RESPONSE_HAVE_DETAILS,
        RESPONSE_WAIT,
		RESPONSE_FORWARD,
		RESPONSE_MAX,
    };

	void SetUniqueID(const unsigned uniqueID)
	{
		m_UniqueID = uniqueID;
	}

    void Reset(const unsigned response, 
			   const unsigned responseParam)
	{
        rlAssert(response != RESPONSE_HAVE_DETAILS);
        rlAssert(response != RESPONSE_FORWARD);
        m_Response = response;
		m_ResponseParam = responseParam;
	}

	void Reset(const unsigned response, 
			   const rlSessionInfo& sessionInfo)
	{
		rlAssert(response == RESPONSE_FORWARD);
		m_Response = response;
		m_SessionInfo = sessionInfo;
	}

	void Reset(const unsigned response, 
			   const rlGamerHandle& hostHandle,
			   const char* hostName,
			   const rlPeerInfo& hostPeerInfo,
			   const rlSessionInfo& sessionInfo,
			   const rlSessionConfig& config,
			   const unsigned numFilledPubSlots,
			   const unsigned numFilledPrivSlots,
			   const u8* sessionUserData,
			   const u16 sessionUserDataSize,
			   const u8* sessionInfoMineData,
			   const u16 sessionInfoMineDataSize)
    {
        rlAssert(response == RESPONSE_HAVE_DETAILS);

		// these two peer addresses must be the same for the export size optimization below to work
		rlAssert(hostPeerInfo.GetPeerAddress().GetPeerId() == sessionInfo.GetHostPeerAddress().GetPeerId());

        m_Response = response;
		m_HostPeerInfo = hostPeerInfo;
		m_SessionInfo = sessionInfo;
		m_Config = config;
		m_HostHandle = hostHandle;
		safecpy(m_HostName, hostName);
        m_NumFilledPublicSlots = numFilledPubSlots;
        m_NumFilledPrivateSlots = numFilledPrivSlots;

		if(sessionUserData && sessionUserDataSize)
		{
			rlAssert(sessionUserDataSize <= RL_MAX_SESSION_USER_DATA_SIZE);
			m_SessionUserDataSize = sessionUserDataSize;

			if(m_SessionUserDataSize)
			{
				sysMemCpy(m_SessionUserData, sessionUserData, m_SessionUserDataSize);
			}
		}
		else
		{
			m_SessionUserDataSize = 0;
		}

		if(sessionInfoMineData && sessionInfoMineDataSize)
		{
			rlAssert(sessionInfoMineDataSize <= RL_MAX_SESSION_DATA_MINE_SIZE);
			m_SessionInfoMineDataSize = sessionInfoMineDataSize;

			if(m_SessionInfoMineDataSize)
			{
				sysMemCpy(m_SessionInfoMineData, sessionInfoMineData, m_SessionInfoMineDataSize);
			}
		}
		else
		{
			m_SessionInfoMineDataSize = 0;
		}
    }

    NET_MESSAGE_SER(bb, msg)
    {
        bool success = true;
        success &= bb.SerUns(msg.m_Response, datBitsNeeded<RESPONSE_MAX - 1>::COUNT);
		success &= bb.SerUns(msg.m_ResponseParam, 32);
		success &= bb.SerUns(msg.m_UniqueID, 32);

		if(success)
        {
			if(msg.m_Response == RESPONSE_HAVE_DETAILS)
			{
				u64 peerId = msg.m_HostPeerInfo.GetPeerId();

				return bb.SerUns(peerId, 64)
					&& bb.SerUser(msg.m_SessionInfo)
					&& rlVerify(msg.m_HostPeerInfo.Init(peerId, msg.m_SessionInfo.GetHostPeerAddress()))
					&& rlVerify(msg.m_SessionInfo.IsValid())
					&& bb.SerUser(msg.m_Config)
					&& bb.SerUser(msg.m_HostHandle)
					&& bb.SerStr(msg.m_HostName, RL_MAX_NAME_BUF_SIZE)
					&& bb.SerUns(msg.m_NumFilledPublicSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
					&& bb.SerUns(msg.m_NumFilledPrivateSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
					&& bb.SerUns(msg.m_SessionUserDataSize, datBitsNeeded<RL_MAX_SESSION_USER_DATA_SIZE>::COUNT)
					&& (msg.m_SessionUserDataSize <= RL_MAX_SESSION_USER_DATA_SIZE)
					&& (msg.m_SessionUserDataSize ? bb.SerBytes(msg.m_SessionUserData, msg.m_SessionUserDataSize) : true)
					&& bb.SerUns(msg.m_SessionInfoMineDataSize, datBitsNeeded<RL_MAX_SESSION_DATA_MINE_SIZE>::COUNT)
					&& (msg.m_SessionInfoMineDataSize <= RL_MAX_SESSION_DATA_MINE_SIZE)
					&& (msg.m_SessionInfoMineDataSize ? bb.SerBytes(msg.m_SessionInfoMineData, msg.m_SessionInfoMineDataSize) : true);
			}
			else if(msg.m_Response == RESPONSE_FORWARD)
			{
				success &= bb.SerUser(msg.m_SessionInfo)
						 && rlVerify(msg.m_SessionInfo.IsValid());
			}
        }

        return success;
    }

    unsigned m_Response;
	unsigned m_ResponseParam;
	unsigned m_UniqueID;

	// needs to be mutable since we modify it in the const Export() function added by the NET_MESSAGE_SER macro
    mutable rlPeerInfo m_HostPeerInfo;

    //The session info that the caller should attempt to join.
    //In the case of a party session on xbox it will be different from the
    //session that's being queried because we query the party's "presence"
    //session, but we join the party session.
    rlSessionInfo m_SessionInfo;
    rlSessionConfig m_Config;

	rlGamerHandle m_HostHandle;

	char m_HostName[RL_MAX_NAME_BUF_SIZE];

    unsigned m_NumFilledPublicSlots;
    unsigned m_NumFilledPrivateSlots;

	u16 m_SessionUserDataSize;
	u8 m_SessionUserData[RL_MAX_SESSION_USER_DATA_SIZE];

	u16 m_SessionInfoMineDataSize;
	u8 m_SessionInfoMineData[RL_MAX_SESSION_DATA_MINE_SIZE];
};

NET_MESSAGE_IMPL(rlSessionDetailRequest);
NET_MESSAGE_IMPL(rlSessionDetailResponse);

//////////////////////////////////////////////////////////////////////////
//  rlSessionQueryHostDetailTask
//////////////////////////////////////////////////////////////////////////
//PURPOSE
//  Queries the host for the session details.
class rlSessionQueryHostDetailTask : public netTask
{

public:

	NET_TASK_USE_CHANNEL(rline_sessionmanager);
	NET_TASK_DECL(rlSessionQueryHostDetailTask);

    rlSessionQueryHostDetailTask();
    ~rlSessionQueryHostDetailTask();

    bool Configure(netConnectionManager* cxnMgr,
					const unsigned flags, 
                    const unsigned channelId,
					const unsigned timeoutMs,
					const unsigned maxAttempts,
					const EndpointId hostEndpointId,
					const rlSessionInfo& sessionInfo,
					const rlGamerHandle& targetHandle,
					rlSessionDetail* detail);

    virtual netTaskStatus OnUpdate(int* resultCode);

    virtual void OnCancel();

    static void SetRetryPolicy(const unsigned timeoutMs, const unsigned maxAttempts);

private:

    bool SendQuery();

    void OnResponse(netTransactor* transactor,
                    netResponseHandler* handler,
                    const netResponse* resp);

    enum State
    {
        STATE_QUERYING,
        STATE_FAILED,
        STATE_SUCCEEDED
    };

    State m_State;

    netTransactor m_Transactor;
	unsigned m_Flags;
    unsigned m_ChannelId;
	rlSessionInfo m_SessionInfo;
	rlGamerHandle m_TargetHandle; 
    netResponseHandler m_RespHandler;
    unsigned m_TxId;
    unsigned m_QueryCount;
    EndpointId m_HostEndpointId;

	unsigned m_TimeoutMs;
	unsigned m_MaxAttempts;

    rlSessionDetail* m_Detail;

    static unsigned sm_TimeoutMs;
    static unsigned sm_MaxAttempts;
};
unsigned rlSessionQueryHostDetailTask::sm_TimeoutMs = rlSessionManager::HOST_QUERY_DEFAULT_TIMEOUT_MS;
unsigned rlSessionQueryHostDetailTask::sm_MaxAttempts = rlSessionManager::HOST_QUERY_DEFAULT_MAX_ATTEMPTS;

rlSessionQueryHostDetailTask::rlSessionQueryHostDetailTask()
: m_State(STATE_QUERYING)
, m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_TimeoutMs(0)
, m_MaxAttempts(0)
, m_TxId(NET_INVALID_TRANSACTION_ID)
, m_QueryCount(0)
, m_Detail(NULL)
{
	m_TargetHandle.Clear();
    m_RespHandler.Bind(this, &rlSessionQueryHostDetailTask::OnResponse);
}

rlSessionQueryHostDetailTask::~rlSessionQueryHostDetailTask()
{
}

bool
rlSessionQueryHostDetailTask::Configure(netConnectionManager* cxnMgr,
										const unsigned flags, 
										const unsigned channelId,
										const unsigned timeoutMs,
										const unsigned maxAttempts,
										const EndpointId hostEndpointId,
										const rlSessionInfo& sessionInfo,
										const rlGamerHandle& targetHandle,
                                        rlSessionDetail* detail)
{
    m_Transactor.Init(cxnMgr);
	m_Flags = flags; 
    m_ChannelId = channelId;

	m_TimeoutMs = timeoutMs;
	if(m_TimeoutMs == 0)
	{
		m_TimeoutMs = sm_TimeoutMs;
	}

	m_MaxAttempts = maxAttempts;
	if(m_MaxAttempts == 0)
	{
		m_MaxAttempts = sm_MaxAttempts;
	}

	m_HostEndpointId = hostEndpointId;
	m_SessionInfo = sessionInfo;
	m_TargetHandle = targetHandle;
    m_TxId = NET_INVALID_TRANSACTION_ID;
    m_QueryCount = 0;
    m_Detail = detail;
    m_Detail->Clear();
    return true;
}

netTaskStatus
rlSessionQueryHostDetailTask::OnUpdate(int* resultCode)
{
    if(WasCanceled())
    {
        return NET_TASKSTATUS_FAILED;
    }

    switch(m_State)
    {
    case STATE_QUERYING:
        m_Transactor.Update(netTask::GetTimestamp());
        if(!m_RespHandler.Pending())
        {
            if(m_QueryCount < m_MaxAttempts)
            {
                if(!this->SendQuery())
                {
                    return NET_TASKSTATUS_FAILED;
                }
            }
            else
            {
				*resultCode = QUERY_RESULT_TIMED_OUT;
                return NET_TASKSTATUS_FAILED;
            }
        }
        break;

    case STATE_FAILED:
        return NET_TASKSTATUS_FAILED;
    
    case STATE_SUCCEEDED:
        return NET_TASKSTATUS_SUCCEEDED;
    }

    return NET_TASKSTATUS_PENDING;
}

//private:

void
rlSessionQueryHostDetailTask::OnCancel()
{
    if(m_RespHandler.Pending())
    {
        m_Transactor.CancelRequest(&m_RespHandler);
    }
}

bool
rlSessionQueryHostDetailTask::SendQuery()
{
	//Request the session config from the host.
	rlSessionDetailRequest rqst;
	rqst.Reset(m_SessionInfo, m_Flags, m_TargetHandle);

	// logging
	netTaskDebug("Querying Host [%" I64FMT "d] - EndpointId: [%d], ID: 0x%08x, Token: 0x%016" I64FMT "x, Flags: 0x%x, Version: %d, Disc: 0x%08x",
				 rlGetPosixTime(), 
				 m_HostEndpointId, 
				 rqst.m_UniqueID, 
				 m_SessionInfo.GetToken().GetValue(), 
				 m_Flags,
				 rqst.m_Version, 
				 rqst.m_Discriminator);

    bool success;

    if(m_QueryCount)
    {
        success = m_Transactor.ResendRequestOutOfBand(m_HostEndpointId,
													 m_ChannelId,
													 m_TxId,
													 rqst,
													 m_TimeoutMs,
													 &m_RespHandler);
    }
    else
    {
        success = m_Transactor.SendRequestOutOfBand(m_HostEndpointId,
												   m_ChannelId,
												   &m_TxId,
												   rqst,
												   m_TimeoutMs,
												   &m_RespHandler);
    }

    ++m_QueryCount;

    return success;
}

void
rlSessionQueryHostDetailTask::OnResponse(netTransactor* /*transactor*/,
                                    netResponseHandler* /*handler*/,
                                    const netResponse* resp)

{
    if(resp->Complete() && !WasCanceled())
    {
        if(resp->Answered())
        {
            //Get the host's response
            rlSessionDetailResponse detailResp;

            if(!rlVerify(detailResp.Import(resp->m_Data, resp->m_SizeofData)))
            {
                netTaskError("Query Response [%" I64FMT "u] - Failed to import %s. Size: %d, Addr: [" NET_ADDR_FMT "]", rlGetPosixTime(), detailResp.GetMsgName(), resp->m_SizeofData, NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));
            }
            else
            {
#if !__NO_OUTPUT
                static const char* s_ResponseString[] =
                {
					"RESPONSE_INVALID_DATA",			// RESPONSE_INVALID_DATA
					"RESPONSE_INVALID_VERSION",			// RESPONSE_INVALID_VERSION
					"RESPONSE_INVALID_DISCRIMINATOR",	// RESPONSE_INVALID_DISCRIMINATOR
					"RESPONSE_INVALID_SESSION",			// RESPONSE_INVALID_SESSION
					"RESPONSE_NOT_HOSTING",				// RESPONSE_NOT_HOSTING
					"RESPONSE_HAVE_DETAILS",			// RESPONSE_HAVE_DETAILS
					"RESPONSE_WAIT",					// RESPONSE_WAIT
					"RESPONSE_FORWARD",					// RESPONSE_FORWARD
                };
                CompileTimeAssert(COUNTOF(s_ResponseString) == rlSessionDetailResponse::RESPONSE_MAX);
#endif

				// logging
				netTaskDebug("Query Response [%" I64FMT "u] - Addr: [" NET_ADDR_FMT "], Response: %s, Param: %d, Size: %d, ID: 0x%08x",
							 rlGetPosixTime(), 
							 NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr), 
							 s_ResponseString[detailResp.m_Response],
							 detailResp.m_ResponseParam,
                             resp->m_SizeofData,
							 detailResp.m_UniqueID);

                if(detailResp.m_Response == rlSessionDetailResponse::RESPONSE_HAVE_DETAILS)
                {
                    m_Detail->Reset(detailResp.m_HostPeerInfo,
                                    detailResp.m_SessionInfo,
                                    detailResp.m_Config,
									detailResp.m_HostHandle,
									detailResp.m_HostName,
                                    detailResp.m_NumFilledPublicSlots,
                                    detailResp.m_NumFilledPrivateSlots,
                                    detailResp.m_SessionUserData,
									detailResp.m_SessionUserDataSize,
									detailResp.m_SessionInfoMineData,
									detailResp.m_SessionInfoMineDataSize);

					// we may not get back what we asked for, log the token
					netTaskDebug("Query Response - Given back 0x%016" I64FMT "x", detailResp.m_SessionInfo.GetToken().m_Value);

                    // log the host gamer handle and name
					char buffer[RL_MAX_GAMER_HANDLE_CHARS];
					detailResp.m_HostHandle.ToString(buffer, RL_MAX_GAMER_HANDLE_CHARS);
					netTaskDebug("Query Response - Host name: %s, rlGamerHandle: %s", detailResp.m_HostName, buffer);
					
					m_State = STATE_SUCCEEDED;
                }
				else if(detailResp.m_Response == rlSessionDetailResponse::RESPONSE_FORWARD)
				{
                    netTaskDebug("Query Response - Forwarded to 0x%016" I64FMT "x", detailResp.m_SessionInfo.GetToken().m_Value);

                    //Set only the session info
                    m_Detail->Reset(detailResp.m_SessionInfo);

                    //Indicate failure - this should be interpreted as a forward request by the calling code
                    m_State = STATE_FAILED;
				}
				else if(detailResp.m_Response == rlSessionDetailResponse::RESPONSE_WAIT)
                {
                    //Double our max attempts. If the session host moves on from RESPONSE_WAIT,
                    //we'll fail below so the m_MaxAttempts increase only serves to assist in 
                    //joining the target gamer. 
                    m_MaxAttempts *= 2; 
                }
                else
                {
                    m_State = STATE_FAILED;
                }
            }
        }
        else
        {
            netTaskDebug("Query Response - Timed out or failed");
        }
    }
}

void
rlSessionQueryHostDetailTask::SetRetryPolicy(const unsigned timeoutMs, 
                                            const unsigned maxAttempts)
{
    rlAssert(timeoutMs > 0);
    rlAssert(maxAttempts > 0);

    sm_TimeoutMs = timeoutMs;
    sm_MaxAttempts = maxAttempts;
}

//////////////////////////////////////////////////////////////////////////
//  rlSessionQueryDetailTask
//////////////////////////////////////////////////////////////////////////
//PURPOSE
//  Query details of a session you don't belong to.
class rlSessionQueryDetailTask : public netTask
{
public:

	// tunables
	static unsigned sm_NumResultsBeforeEarlyOut;
	static unsigned sm_MaxWaitTimeSinceLastSuccessfulResult;

    // maximum number of sessions we can query at once
    static const int MAX_SESSIONS_TO_QUERY  = RL_FIND_SESSIONS_MAX_SESSIONS_TO_FIND;

	NET_TASK_USE_CHANNEL(rline_sessionmanager);
	NET_TASK_DECL(rlSessionQueryDetailTask);

    rlSessionQueryDetailTask();
    ~rlSessionQueryDetailTask();

    bool Configure(netConnectionManager* cxnMgr,
					const unsigned flags,
                    const unsigned channelId,
					const unsigned timeoutMs,
					const unsigned maxAttempts,
					const unsigned numResultsBeforeEarlyOut,
					const bool solicited,
					const rlNetworkMode netMode,
					const rlSessionInfo* sessionInfos,
                    const unsigned numSessions,
					const rlGamerHandle targetHandle,
                    rlSessionDetail* details,
                    unsigned* numResults);

    virtual netTaskStatus OnUpdate(int* resultCode);

private:

    virtual void OnCancel();

    virtual void OnCleanup();

	void UpdateQuery(const int queryIdx);
    bool OpenTunnel(const int queryIdx);
	bool QueryDetails(const int queryIdx);
    
	netConnectionManager* m_CxnMgr;
	unsigned m_Flags;
    unsigned m_ChannelId;
	unsigned m_TimeoutMs;
	unsigned m_MaxAttempts;
	unsigned m_NumResultsBeforeEarlyOut;
    rlNetworkMode m_NetMode;
	unsigned m_NumSessions;
	rlGamerHandle m_TargetHandle;
	bool m_bSolicited;
	unsigned m_LastResultTime;

	struct sQuery
	{
		enum
		{
			STATE_OPEN_TUNNEL,
			STATE_OPENING_TUNNEL,
			STATE_QUERYING_DETAILS,
			STATE_FINISHED
		};
		
		sQuery() 
		{ 
			m_State = STATE_OPEN_TUNNEL; 
		}

		int m_State;
		netTunnelRequest m_TunnelRqst;
		netStatus m_TunnelStatus;
		netStatus m_QueryStatus;
		rlSessionInfo m_SessionInfo;
		rlSessionDetail m_Detail;
	};
	sQuery m_Queries[MAX_SESSIONS_TO_QUERY];

    rlSessionDetail* m_Details;
    unsigned* m_NumResults;
};

unsigned rlSessionQueryDetailTask::sm_NumResultsBeforeEarlyOut = rlSessionManager::DEFAULT_NUM_QUERY_RESULTS_BEFORE_EARLY_OUT;
unsigned rlSessionQueryDetailTask::sm_MaxWaitTimeSinceLastSuccessfulResult = rlSessionManager::DEFAULT_MAX_WAIT_SINCE_LAST_RESULT;

rlSessionQueryDetailTask::rlSessionQueryDetailTask()
: m_CxnMgr(nullptr)
, m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_TimeoutMs(0)
, m_MaxAttempts(0)
, m_NumResultsBeforeEarlyOut(rlSessionManager::DEFAULT_NUM_QUERY_RESULTS_BEFORE_EARLY_OUT)
, m_NetMode(RL_NETMODE_INVALID)
, m_NumSessions(0)
, m_Details(nullptr)
, m_NumResults(nullptr)
, m_bSolicited(false)
, m_LastResultTime(0)
{
	m_TargetHandle.Clear();
}

rlSessionQueryDetailTask::~rlSessionQueryDetailTask()
{
}

bool
rlSessionQueryDetailTask::Configure(netConnectionManager* cxnMgr,
									const unsigned flags,
									const unsigned channelId,
									const unsigned timeoutMs,
									const unsigned maxAttempts,
									const unsigned numResultsBeforeEarlyOut,
									const bool solicited,
                                    const rlNetworkMode netMode,
									const rlSessionInfo* sessionInfos,
                                    const unsigned numSessions,
									const rlGamerHandle targetHandle,
                                    rlSessionDetail* details,
                                    unsigned* numResults)
{
    m_CxnMgr = cxnMgr;
	m_Flags = flags;
    m_ChannelId = channelId;
	m_TimeoutMs = timeoutMs;
	m_MaxAttempts = maxAttempts;
	m_NumResultsBeforeEarlyOut = numResultsBeforeEarlyOut;
	m_LastResultTime = 0;

	if(numResults == nullptr)
	{
		netTaskError("Invalid results parameter");
		return false;
	}

	if(m_NumResultsBeforeEarlyOut == 0)
	{
		m_NumResultsBeforeEarlyOut = sm_NumResultsBeforeEarlyOut;
	}

    if(numSessions > MAX_SESSIONS_TO_QUERY)
    {
        netTaskWarning("Request to query %d sessions, whereas the maximum is %d",
                        numSessions,
                        MAX_SESSIONS_TO_QUERY);
    }

    if(0 == numSessions)
    {
        netTaskError("No sessions to query");
        return false;
    }

    m_NumSessions = 0;
    for(int i = 0; i < (int)numSessions && i < MAX_SESSIONS_TO_QUERY; ++i)
    {
        m_Queries[i].m_SessionInfo = sessionInfos[i];
        ++m_NumSessions;
    }

	m_TargetHandle = targetHandle;
	m_bSolicited = solicited;
    m_NetMode = netMode;
	m_Details = details;
    m_NumResults = numResults;
	*m_NumResults = 0;

	netTaskDebug("Opening tunnels to %u hosts...", m_NumSessions);
	
    return true;
}

void
rlSessionQueryDetailTask::UpdateQuery(const int queryIdx)
{
	switch(m_Queries[queryIdx].m_State)
	{
	case sQuery::STATE_OPEN_TUNNEL:
		{
			if(OpenTunnel(queryIdx))
			{
				netTaskDebug("[%d] Opening tunnel to 0x%016" I64FMT "x", queryIdx, m_Queries[queryIdx].m_SessionInfo.GetToken().GetValue());
				m_Queries[queryIdx].m_State = sQuery::STATE_OPENING_TUNNEL;
			}
			else
			{
				netTaskError("[%d] Error opening tunnel to 0x%016" I64FMT "x", queryIdx, m_Queries[queryIdx].m_SessionInfo.GetToken().GetValue());
				m_Queries[queryIdx].m_State = sQuery::STATE_FINISHED;
			}
		}
		break;

	case sQuery::STATE_OPENING_TUNNEL:
		{
			if(m_Queries[queryIdx].m_TunnelStatus.Succeeded())
			{
				netTaskDebug("[%d] Opened tunnel", queryIdx);
				m_Queries[queryIdx].m_State = sQuery::STATE_QUERYING_DETAILS;
				QueryDetails(queryIdx);
			}
			else if(!m_Queries[queryIdx].m_TunnelStatus.Pending())
			{
				netTaskError("[%d] Failed to open tunnel", queryIdx);
				m_Queries[queryIdx].m_State = sQuery::STATE_FINISHED;
			}
		}
		break;

	case sQuery::STATE_QUERYING_DETAILS:
		{
			if(m_Queries[queryIdx].m_QueryStatus.Succeeded())
			{
				netTaskDebug("[%d] Retrieved details", queryIdx);

				// update last result time
				m_LastResultTime = sysTimer::GetSystemMsTime();
				
				// copy successful result into output buffer
				m_Details[*m_NumResults].Reset(m_Queries[queryIdx].m_Detail.m_HostPeerInfo,
											   m_Queries[queryIdx].m_Detail.m_SessionInfo,
											   m_Queries[queryIdx].m_Detail.m_SessionConfig, 
											   m_Queries[queryIdx].m_Detail.m_HostHandle,
											   m_Queries[queryIdx].m_Detail.m_HostName,
											   m_Queries[queryIdx].m_Detail.m_NumFilledPublicSlots,
											   m_Queries[queryIdx].m_Detail.m_NumFilledPrivateSlots,
											   m_Queries[queryIdx].m_Detail.m_SessionUserData,
											   m_Queries[queryIdx].m_Detail.m_SessionUserDataSize,
											   m_Queries[queryIdx].m_Detail.m_SessionInfoMineData,
											   m_Queries[queryIdx].m_Detail.m_SessionInfoMineDataSize);

				// increment results count
				++*m_NumResults;

				// finished
				m_Queries[queryIdx].m_State = sQuery::STATE_FINISHED;
			}
			else if(!m_Queries[queryIdx].m_QueryStatus.Pending())
			{
				//Particular case for forward requests - interpret a failure, with valid session info
				//As indication that we should re-issue the request with the given session info
				if(m_Queries[queryIdx].m_Detail.m_SessionInfo.IsValid())
				{
					netTaskDebug("[%d] Forwarded to 0x%016" I64FMT "x. Restarting.", queryIdx, m_Queries[queryIdx].m_Detail.m_SessionInfo.GetToken().m_Value);

					// update last result time (we got a response so this qualifies)
					m_LastResultTime = sysTimer::GetSystemMsTime();
					
					//Make sure to unregister any current keys
					OnCleanup();

					//And allow query to re-run
					m_Queries[queryIdx].m_QueryStatus.Reset(); 

					//Stash new session info and restart flow
					m_Queries[queryIdx].m_SessionInfo = m_Queries[queryIdx].m_Detail.m_SessionInfo;
					m_Queries[queryIdx].m_State = sQuery::STATE_OPEN_TUNNEL;
				}
				else
				{
					netTaskError("[%d] Error querying details", queryIdx);
					
					if((m_Queries[queryIdx].m_QueryStatus.GetResultCode() == QUERY_RESULT_TIMED_OUT) && m_Queries[queryIdx].m_TunnelRqst.GetNetAddress().IsRelayServerAddr())
					{
						//Config request unsuccessful. Remove the cached relay address.
						netTaskDebug("[%d] Query made via relay. Removing cached address", queryIdx);
						m_CxnMgr->RemoveCachedRelayAddress(m_Queries[queryIdx].m_TunnelRqst.GetNetAddress());
					}

					m_Queries[queryIdx].m_State = sQuery::STATE_FINISHED;
				}
			}
		}
		break;

	case sQuery::STATE_FINISHED:
		break;
	}
}

netTaskStatus
rlSessionQueryDetailTask::OnUpdate(int* /*resultCode*/)
{
    if(WasCanceled())
    {
        return NET_TASKSTATUS_FAILED;
    }

	bool allFinished = true; 
	for(int i = 0; i < (int)m_NumSessions; ++i)
	{
		// update queries and check state
		UpdateQuery(i);
		if(m_Queries[i].m_State != sQuery::STATE_FINISHED)
		{
			allFinished = false;
		}
	}

	if(allFinished)
	{
		netTaskDebug("All queries have completed. We have %u results.", *m_NumResults);
		return NET_TASKSTATUS_SUCCEEDED;
	}

	// check if we should early out of this task because we already have enough results
	if((m_NumResultsBeforeEarlyOut > 0) && ((*m_NumResults) >= m_NumResultsBeforeEarlyOut))
	{
		netTaskDebug("Exiting early - We have enough successful results (%u / %u)", *m_NumResults, m_NumSessions);

		// cancel our pending tunnels/queries
		OnCancel();
		return NET_TASKSTATUS_SUCCEEDED;
	}

	// if we have enabled cancelling for long running checks and we have at least half of our results back...
	if(sm_MaxWaitTimeSinceLastSuccessfulResult > 0 && *m_NumResults > 0 && (*m_NumResults >= (m_NumSessions / 2)))
	{
		// and we have enabled 
		const unsigned timeSinceLastResultMs = sysTimer::GetSystemMsTime() - m_LastResultTime;
		if(timeSinceLastResultMs > sm_MaxWaitTimeSinceLastSuccessfulResult)
		{
			netTaskDebug("Exiting early - %ums since last result. Retrieved %u / %u results", timeSinceLastResultMs, *m_NumResults, m_NumSessions);

			// cancel our pending tunnels/queries
			OnCancel();
			return NET_TASKSTATUS_SUCCEEDED;
		}
	}

	return NET_TASKSTATUS_PENDING;
}

//private:

void
rlSessionQueryDetailTask::OnCancel()
{
    for(int i = 0; i < (int)m_NumSessions; ++i)
    {
		if(m_CxnMgr)
		{
#if !__NO_OUTPUT
			if(m_Queries[i].m_TunnelRqst.Pending())
			{
				// only log when pending...
				netTaskDebug("[%d] Cancelling tunnel request", i);
			}
#endif

			m_CxnMgr->CancelTunnelRequest(&m_Queries[i].m_TunnelRqst);
		}

        if(m_Queries[i].m_QueryStatus.Pending())
        {
			netTaskDebug("[%d] Cancelling pending query", i);
			netTask::Cancel(&m_Queries[i].m_QueryStatus);
        }
    }
}

void rlSessionQueryDetailTask::OnCleanup()
{

}

bool
rlSessionQueryDetailTask::OpenTunnel(const int queryIdx)
{
	netTunnelType tunnelType;

    if(RL_NETMODE_ONLINE == m_NetMode)
    {
        tunnelType = NET_TUNNELTYPE_ONLINE;
    }
    else
    {
        rlAssert(RL_NETMODE_LAN == m_NetMode);
        tunnelType = NET_TUNNELTYPE_LAN;
    }

	const netTunnelDesc tunnelDesc(tunnelType,
									m_bSolicited ? NET_TUNNEL_REASON_DIRECT_QUERY : NET_TUNNEL_REASON_MATCHMAKING_QUERY,
									m_Queries[queryIdx].m_SessionInfo.GetToken().GetValue(),
									false);

	if(!m_CxnMgr->OpenTunnel(m_Queries[queryIdx].m_SessionInfo.GetHostPeerAddress(),
							 tunnelDesc,
							 &m_Queries[queryIdx].m_TunnelRqst,
							 &m_Queries[queryIdx].m_TunnelStatus))
	{
		netTaskError("Failed to open tunnel to session host");
		return false;
	}
    
	return true; 
}

bool
rlSessionQueryDetailTask::QueryDetails(const int queryIdx)
{
    rlSessionQueryHostDetailTask* task = NULL;
    rtry
    {
        rverify(netTask::Create(&task, &m_Queries[queryIdx].m_QueryStatus),
                                catchall,
                                netTaskError("Error creating rlSessionQueryHostDetailTask"));

        rverify(task->Configure(m_CxnMgr,
                                m_Flags,
                                m_ChannelId,
								m_TimeoutMs,
								m_MaxAttempts,
								m_Queries[queryIdx].m_TunnelRqst.GetEndpointId(),
                                m_Queries[queryIdx].m_SessionInfo,
								m_TargetHandle,
                                &m_Queries[queryIdx].m_Detail),
                                catchall,
                                netTaskError("Error configuring rlSessionQueryHostDetailTask"));

        rverify(netTask::Run(task),
                catchall,
                netTaskError("Error scheduling rlSessionQueryHostDetailTask"));

        return true;
    }
    rcatchall
    {
        netTask::Destroy(task);
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
//  rlSessionDetail
//////////////////////////////////////////////////////////////////////////
rlSessionDetail::rlSessionDetail()
{
    Clear();
}

void
rlSessionDetail::Reset(const rlPeerInfo& hostPeerInfo,
                        const rlSessionInfo& sessionInfo,
                        const rlSessionConfig& sessionConfig,
                        const rlGamerHandle& hostHandle,
						const char* hostName,
                        const unsigned numFilledPubSlots,
                        const unsigned numFilledPrivSlots,
						const u8* sessionUserData,
						const u16 sessionUserDataSize,
						const u8* sessionInfoMineData,
						const u16 sessionInfoMineDataSize)
{
    Clear();

	// these two peer addresses must be the same for the export size optimization below to work
	rlAssert(hostPeerInfo.GetPeerAddress().GetPeerId() == sessionInfo.GetHostPeerAddress().GetPeerId());

    m_HostPeerInfo = hostPeerInfo;
    m_SessionInfo = sessionInfo;
    m_SessionConfig = sessionConfig;
	m_HostHandle = hostHandle;
	safecpy(m_HostName, hostName);
    m_NumFilledPublicSlots = numFilledPubSlots;
    m_NumFilledPrivateSlots = numFilledPrivSlots;

	if(sessionUserData && sessionUserDataSize)
	{
		rlAssert(sessionUserDataSize <= RL_MAX_SESSION_USER_DATA_SIZE);
		m_SessionUserDataSize = sessionUserDataSize;

		if(m_SessionUserDataSize)
		{
			sysMemCpy(m_SessionUserData, sessionUserData, m_SessionUserDataSize);
		}
	}
	else
	{
		m_SessionUserDataSize = 0;
	}

	if(sessionInfoMineData && sessionInfoMineDataSize)
	{
		rlAssert(sessionInfoMineDataSize <= RL_MAX_SESSION_DATA_MINE_SIZE);
		m_SessionInfoMineDataSize = sessionInfoMineDataSize;

		if(m_SessionInfoMineDataSize)
		{
			sysMemCpy(m_SessionInfoMineData, sessionInfoMineData, m_SessionInfoMineDataSize);
		}
	}
	else
	{
		m_SessionInfoMineDataSize = 0;
	}
}

void 
rlSessionDetail::Reset(const rlSessionInfo& sessionInfo)
{
    Clear();
    m_SessionInfo = sessionInfo;
}

void
rlSessionDetail::Clear()
{
    m_HostPeerInfo.Clear();
    m_SessionConfig.Clear();
    m_SessionInfo.Clear();
	m_HostHandle.Clear();
	m_HostName[0] = '\0';
    m_NumFilledPublicSlots = 0;
    m_NumFilledPrivateSlots = 0;
	m_SessionUserDataSize = 0;
	m_SessionInfoMineDataSize = 0;
}

bool
rlSessionDetail::Export(void* buf,
                        const unsigned sizeofBuf,
                        unsigned* size) const
{
    bool success = false;

    unsigned numBytes = 0;
    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

	u64 peerId = m_HostPeerInfo.GetPeerId();

    if(bb.SerUser(m_HostHandle)
		&& bb.SerStr(m_HostName, sizeof(m_HostName))
        && bb.SerUns(peerId, 64)
        && bb.SerUser(m_SessionInfo)
        && bb.SerUser(m_SessionConfig)
        && bb.SerUns(m_NumFilledPublicSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
        && bb.SerUns(m_NumFilledPrivateSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
		&& bb.SerUns(m_SessionUserDataSize, datBitsNeeded<RL_MAX_SESSION_USER_DATA_SIZE>::COUNT)
		&& (m_SessionUserDataSize ? bb.SerBytes(m_SessionUserData, m_SessionUserDataSize) : true)
		&& bb.SerUns(m_SessionInfoMineDataSize, datBitsNeeded<RL_MAX_SESSION_DATA_MINE_SIZE>::COUNT)
		&& (m_SessionInfoMineDataSize ? bb.SerBytes(m_SessionInfoMineData, m_SessionInfoMineDataSize) : true))
    {
        numBytes = bb.GetNumBytesWritten();
        success = true;
    }

    if(size){*size = success ? numBytes : 0;}

    return success;
}

bool
rlSessionDetail::Import(const void* buf,
                        const unsigned sizeofBuf,
                        unsigned* size)
{
    bool success = false;

    unsigned numBytes = 0;
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	u64 peerId = RL_INVALID_PEER_ID;

    if(bb.SerUser(m_HostHandle)
        && bb.SerStr(m_HostName, sizeof(m_HostName))
		&& bb.SerUns(peerId, 64)
        && bb.SerUser(m_SessionInfo)
        && bb.SerUser(m_SessionConfig)
        && bb.SerUns(m_NumFilledPublicSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
		&& bb.SerUns(m_NumFilledPrivateSlots, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT)
		&& bb.SerUns(m_SessionUserDataSize, datBitsNeeded<RL_MAX_SESSION_USER_DATA_SIZE>::COUNT)
		&& rlVerifyf(m_SessionUserDataSize <= static_cast<unsigned>(RL_MAX_SESSION_USER_DATA_SIZE), "m_SessionUserDataSize[%u] overflow", m_SessionUserDataSize)
		&& (m_SessionUserDataSize ? bb.SerBytes(m_SessionUserData, m_SessionUserDataSize) : true)
		&& bb.SerUns(m_SessionInfoMineDataSize, datBitsNeeded<RL_MAX_SESSION_DATA_MINE_SIZE>::COUNT)
		&& rlVerifyf(m_SessionInfoMineDataSize <= static_cast<unsigned>(RL_MAX_SESSION_DATA_MINE_SIZE), "m_SessionInfoMineDataSize[%u] overflow", m_SessionInfoMineDataSize)
		&& (m_SessionInfoMineDataSize ? bb.SerBytes(m_SessionInfoMineData, m_SessionInfoMineDataSize) : true))
	{
		m_HostPeerInfo.Init(peerId, m_SessionInfo.GetHostPeerAddress());
        numBytes = bb.GetNumBytesRead();
        success = true;
    }

    if(size){*size = success ? numBytes : 0;}

    return success;
}

//////////////////////////////////////////////////////////////////////////
//  rlSessionManager
//////////////////////////////////////////////////////////////////////////
#if SUPPORT_LAN_MULTIPLAYER
static netSocket s_rlSesMgrLanSkt;
#endif
static unsigned s_rlSesMgrLanPort;

//Listen on all channels for queries
static netRequestHandler s_rlSesMgrRqstHandlers[NET_MAX_CHANNELS];
static netTransactor s_rlSesMgrTransactors[NET_MAX_CHANNELS];
static netTimeStep s_rlSesMgrTimeStep;
static bool s_rlSesMgrInitialized = false;
static bool s_rlSesMgrDataMineEnabled = true; 
rlSessionManager::QueryReceivedDelegate rlSessionManager::ms_rlSesMgrQueryDelegate; 
rlSessionManager::GetInfoMine rlSessionManager::ms_rlSesMgrGetInfoMineDelegate; 

static rlPresence::Delegate s_rlSesMgrPresenceDlgt;

static const unsigned short rlSesMgrDEFAULT_LAN_MATCHING_PORT   = 0x0F0B;

PARAM(lanmatchingport, "Port to use for matchmaking on a LAN");

#define GET_SESSION(link) \
        ((rlSession*)((size_t)(link) - OffsetOf(rlSession, m_SesMgrLink)))

//Encapsulate the head of the session list in a function
//so we can be assured it's initialized during the static
//initialization phase of program startup
static inline rlSessionManagerLink& rlSessionManagerGetHead()
{
    static rlSessionManagerLink s_Head;

    return s_Head;
}

bool
rlSessionManager::Init()
{
    rlDebug("Initializing...");

    if(!rlVerifyf(!IsInitialized(), "Already initialized"))
    {
        return true;
    }

    if(!PARAM_lanmatchingport.Get(s_rlSesMgrLanPort))
    {
        s_rlSesMgrLanPort = rlSesMgrDEFAULT_LAN_MATCHING_PORT;
    }

    for(int i = 0; i < NET_MAX_CHANNELS; ++i)
    {
        s_rlSesMgrRqstHandlers[i].Bind(&rlSessionManager::OnRequest);
    }

    //Listen for presence events
    s_rlSesMgrPresenceDlgt.Bind(&rlSessionManager::OnPresenceEvent);

    s_rlSesMgrTimeStep.Init(sysTimer::GetSystemMsTime());

	s_rlSesMgrRng.Reset(sysTimer::GetSystemMsTime());

    s_rlSesMgrInitialized = true;

    return s_rlSesMgrInitialized;
}

void
rlSessionManager::Shutdown()
{
    if(IsInitialized())
    {
        rlDebug("Shutting down...");

        rlPresence::RemoveDelegate(&s_rlSesMgrPresenceDlgt);
		
#if SUPPORT_LAN_MULTIPLAYER
        netHardware::DestroySocket(&s_rlSesMgrLanSkt);
#endif

        while(&rlSessionManagerGetHead() != rlSessionManagerGetHead().m_Next)
        {
            UnregisterSession(GET_SESSION(rlSessionManagerGetHead().m_Next));
        }

        for(int i = 0; i < NET_MAX_CHANNELS; ++i)
        {
            s_rlSesMgrTransactors[i].Shutdown();
        }

        s_rlSesMgrInitialized = false;
    }
}

bool
rlSessionManager::IsInitialized()
{
    return s_rlSesMgrInitialized;
}

void
rlSessionManager::Update()
{
    if(IsInitialized())
    {
        //When we finally get a valid g_CxnMgr
        //initialize the transactors that listen for queries
        if(g_CxnMgr && !s_rlSesMgrTransactors[0].IsInitialized())
        {
            for(int i = 0; i < NET_MAX_CHANNELS; ++i)
            {
                s_rlSesMgrTransactors[i].Init(g_CxnMgr);
                s_rlSesMgrTransactors[i].AddRequestHandler(&s_rlSesMgrRqstHandlers[i], i);
            }
        }

        s_rlSesMgrTimeStep.SetTime(sysTimer::GetSystemMsTime());

        for(int i = 0; i < NET_MAX_CHANNELS; ++i)
        {
            s_rlSesMgrTransactors[i].Update(s_rlSesMgrTimeStep.GetTimeStep());
        }

        if(!s_rlSesMgrPresenceDlgt.IsRegistered() && rlPresence::IsInitialized())
        {
            rlPresence::AddDelegate(&s_rlSesMgrPresenceDlgt);
        }
		
#if SUPPORT_LAN_MULTIPLAYER
		HandleLanQueries();
#endif
    }
}

bool
rlSessionManager::IsInSession(const rlSessionInfo& sessionInfo)
{
    return NULL != GetSession(sessionInfo);
}

bool
rlSessionManager::IsHostingSession(const rlSessionInfo& sessionInfo)
{
	rlSession* session = GetSession(sessionInfo);
	return (NULL != session) && (session->IsHost());
}

bool 
rlSessionManager::IsHostingSession(const rlSessionInfo& sessionInfo, const rlScMatchmakingMatchId& matchId)
{
	rlSession* session = GetSession(sessionInfo);
	return (NULL != session) && (session->IsHost()) && (session->HasScMatchId() && (session->GetScMatchId() == matchId));
}

bool 
rlSessionManager::GetHostedMatchIds(const int localGamerIndex, char* buf, const unsigned sizeOfBuf)
{
	bool success = false;
	buf[0] = '\0';

	rtry
	{
		rcheck(IsInitialized(), catchall, );
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall,);

		RsonWriter rw;
		rw.Init(buf, sizeOfBuf, RSON_FORMAT_JSON);
		rverify(rw.BeginArray(NULL, NULL), catchall, );

		rlSessionManagerLink* link = rlSessionManagerGetHead().m_Next;
		for(; &rlSessionManagerGetHead() != link; link = link->m_Next)
		{
			rlSession* session = GET_SESSION(link);
			if(session
				&& session->IsActivated()
				&& (session->GetOwner().GetLocalIndex() == localGamerIndex)
				&& session->m_Data.m_MatchId.IsValid()
				&& session->IsHost())
			{
				rverify(rw.WriteString(NULL, session->m_Data.m_MatchId.ToString()), catchall, );
			}
		}

		rverify(rw.End(), catchall, );

		success = true;
	}
	rcatchall
	{
		buf[0] = '\0';
	}

	return success;
}

void
rlSessionManager::SetLanMatchingPort(const unsigned short lanPort)
{
    rlDebug("LAN listen port to :%u", lanPort);

    if(rlVerifyf(IsInitialized(), "Not initialized"))
    {
#if RSG_NP
        //On NP, port must be >= 1024 and <= 49151,
        //49152 to 65535 are reserved by the system.

        if(!rlVerifyf(lanPort >= 1024 && lanPort <= 49151, "Port must be in the range 1024 <= port <= 49151"))
        {
            return;
        }
#endif

        s_rlSesMgrLanPort = lanPort;
    }
}

unsigned short
rlSessionManager::GetLanMatchingPort()
{

    if(rlVerifyf(IsInitialized(), "Not initialized"))
    {
        return (unsigned short)s_rlSesMgrLanPort;
    }

    return 0;
}

unsigned 
rlSessionManager::RemoveInvalidSessions(rlSessionInfo* sessionInfos, const unsigned numSessions)
{
	if(sessionInfos == NULL)
	{
		return 0;
	}

	unsigned validCount = 0;
	for(int i = 0; i < (int)numSessions; ++i)
	{
		const rlSessionInfo& info = sessionInfos[i];

		if(info.GetHostPeerAddress().IsLocal())
		{
			rlWarning("RemoveLocalSessions - Attempt to query detail for a local session!");
			continue;
		}

		if(rlSessionManager::IsInSession(info))
		{
			rlWarning("RemoveLocalSessions - Attempt to query detail for a session we're already in!");
			continue;
		}

		//Remove duplicates
		bool haveIt = false;
		for(int j = 0; j < (int)validCount; ++j)
		{
			if(info == sessionInfos[j])
			{
				haveIt = true;
				break;
			}
		}

		if(haveIt)
		{
			continue;
		}

		sessionInfos[validCount] = info;
		++validCount;
	}

	return validCount;
}

bool
rlSessionManager::QueryDetail(const rlNetworkMode netMode,
                                const unsigned channelId,
								unsigned timeoutMs,
								unsigned maxAttempts,
								unsigned numResultsBeforeEarlyOut,
								const bool solicited,
								const rlSessionInfo* sessionInfos,
                                const unsigned numSessions,
								const rlGamerHandle targetHandle,
								rlSessionDetail* details,
                                unsigned* numResults,
                                netStatus* status)
{
    bool success = false;

    rlSessionQueryDetailTask* task = NULL;

    rlDebug("Querying details for %u sessions...", numSessions);

    //Make sure we're not trying to query a local session
    rlSessionInfo tmpSessionInfos[rlSessionQueryDetailTask::MAX_SESSIONS_TO_QUERY];
    unsigned tmpNumSessions = 0;

    for(int i = 0; i < (int)numSessions; ++i)
    {
        const rlSessionInfo& info = sessionInfos[i];

        if(info.GetHostPeerAddress().IsLocal())
        {
            rlWarning("Attempt to query detail for a local session!");
            continue;
        }

        if(rlSessionManager::IsInSession(info))
        {
            rlWarning("Attempt to query detail for a session we're already in!");
            continue;
        }

        //Remove duplicates
        bool haveIt = false;
        for(int j = 0; j < (int)tmpNumSessions; ++j)
        {
            if(info == tmpSessionInfos[j])
            {
                haveIt = true;
                break;
            }
        }

        if(haveIt)
        {
            continue;
        }

        tmpSessionInfos[tmpNumSessions] = info;
        ++tmpNumSessions;
    }

    rtry
    {
        rverify(IsInitialized(), catchall, rlError("Not initialized"));

        rcheck(tmpNumSessions > 0,
                catchall,
                rlError("No sessions to query"));

        rverify(g_CxnMgr,
                catchall,
                rlError("Invalid connection manager"));

        rverify(g_CxnMgr->IsInitialized(),
                catchall,
                rlError("Connection manager not initialized"));

        rverify(channelId <= NET_MAX_CHANNEL_ID,
                catchall,
                rlError("Invalid channel ID: %u", channelId));

		if(targetHandle.IsValid())
		{
			rverify(tmpNumSessions == 1,
					catchall,
					rlError("Target handle provided for multi-session query"));
		}

        rverify(netTask::Create(&task, status), catchall,);

		unsigned flags = 0;
#if !__NO_OUTPUT
		// always request data mine in output builds
		if(s_rlSesMgrDataMineEnabled)
		{
			flags |= rlSessionDetailRequest::REQUEST_FLAG_DATA_MINE;
		}
#endif

        rverify(task->Configure(g_CxnMgr,
								flags,
                                channelId,
								timeoutMs,
								maxAttempts,
								numResultsBeforeEarlyOut,
								solicited,
                                netMode,
								tmpSessionInfos,
                                tmpNumSessions,
								targetHandle,
                                details,
                                numResults), catchall,);

        rverify(netTask::Run(task),catchall,rlError("Error scheduling task"));
        success = true;
    }
    rcatchall
    {
        netTask::Destroy(task);
    }

    if(!success){status->ForceFailed();}

    return success;
}

bool 
rlSessionManager::QueryDetail(const rlNetworkMode netMode,
							  const unsigned channelId,
							  unsigned timeoutMs,
							  unsigned maxAttempts,
							  unsigned numResultsBeforeEarlyOut,
							  const bool solicited,
							  const rlSessionInfo* sessionInfos,
							  const unsigned numSessions,
							  rlSessionDetail* details,
							  unsigned* numResults,
							  netStatus* status)
{
	return QueryDetail(netMode, channelId, timeoutMs, maxAttempts, numResultsBeforeEarlyOut, solicited, sessionInfos, numSessions, rlGamerHandle(), details, numResults, status);
}

void
rlSessionManager::Cancel(netStatus* status)
{
	if (!status->Canceled())
	{
		netTask::Cancel(status);
	}
}

void
rlSessionManager::SetDiscriminator(const unsigned discriminator)
{
	if(s_rlSesMgrDiscriminator != discriminator)
	{
		rlDebug("SetDiscriminator :: Discriminator is 0x%08x", discriminator);
		s_rlSesMgrDiscriminator = discriminator;
	}
}

void
rlSessionManager::SetRetryPolicy(const unsigned timeoutMs, 
								 const unsigned maxAttempts)
{
	rlSessionQueryHostDetailTask::SetRetryPolicy(timeoutMs, maxAttempts);
}

void
rlSessionManager::SetDataMineEnabled(const bool enable)
{
	if(s_rlSesMgrDataMineEnabled != enable)
	{
		rlDebug("SetDataMineEnabled :: %s", enable ? "Enabled" : "Disabled");
		s_rlSesMgrDataMineEnabled = enable;
	}
}

void rlSessionManager::SetDefaultNumEarlyOutResults(const unsigned numResults)
{
	if(rlSessionQueryDetailTask::sm_NumResultsBeforeEarlyOut != numResults)
	{
		rlDebug("SetDefaultNumEarlyOutResults :: %u -> %u", rlSessionQueryDetailTask::sm_NumResultsBeforeEarlyOut, numResults);
		rlSessionQueryDetailTask::sm_NumResultsBeforeEarlyOut = numResults;
	}
}

void rlSessionManager::SetDefaultMaxWaitTimeAfterResult(const unsigned maxWaitTime)
{
	if(rlSessionQueryDetailTask::sm_MaxWaitTimeSinceLastSuccessfulResult != maxWaitTime)
	{
		rlDebug("SetDefaultMaxWaitTimeAfterResult :: %u -> %u", rlSessionQueryDetailTask::sm_MaxWaitTimeSinceLastSuccessfulResult, maxWaitTime);
		rlSessionQueryDetailTask::sm_MaxWaitTimeSinceLastSuccessfulResult = maxWaitTime;
	}
}

//private:

bool
rlSessionManager::RegisterSession(rlSession* session)
{
    rlSessionManagerLink* link = &session->m_SesMgrLink;
    if(rlVerify(link == link->m_Next && link == link->m_Prev))
    {
        link->LinkBefore(&rlSessionManagerGetHead());
        return true;
    }

    return false;
}

void
rlSessionManager::UnregisterSession(rlSession* session)
{
    if(rlVerifyf(IsInitialized(), "Not initialized"))
    {
        rlSessionManagerLink* link = &session->m_SesMgrLink;
        if(rlVerify(link != link->m_Next && link != link->m_Prev))
        {
            link->Unlink();
        }
    }
}

rlSession*
rlSessionManager::GetSession(const rlSessionInfo& sessionInfo)
{
    if(rlVerifyf(IsInitialized(), "Not initialized"))
    {
        rlSessionManagerLink* link = rlSessionManagerGetHead().m_Next;
        for(; &rlSessionManagerGetHead() != link; link = link->m_Next)
        {
            rlSession* session = GET_SESSION(link);
            if(session->IsActivated()
                && session->GetSessionInfo() == sessionInfo)
            {
                return session;
                //break;
            }
        }
    }

    return NULL;
}

void
rlSessionManager::OnRequest(netTransactor* transactor,
                            netRequestHandler* /*handler*/,
                            const netRequest* rqst)
{
    rlAssert(transactor >= &s_rlSesMgrTransactors[0]
            && transactor < &s_rlSesMgrTransactors[NET_MAX_CHANNELS]);

    rtry
    {
        rverify(IsInitialized(), catchall, rlError("Not initialized"));

        unsigned msgId;
        rcheck(netMessage::GetId(&msgId,
                                 rqst->m_Data,
                                 rqst->m_SizeofData), catchall,);

        if(rlSessionDetailRequest::MSG_ID() == msgId)
        {
            rlSessionDetailRequest detailRqst;
			rlSessionDetailResponse detailResp;

			if(rlVerify(detailRqst.ImportStub(rqst->m_Data, rqst->m_SizeofData)))
			{
				if(detailRqst.m_Version != SESSION_DETAIL_REQUEST_VERSION)
				{
					rlDebug("OnRequest :: Invalid Version - Local: %d, Remote: %d", SESSION_DETAIL_REQUEST_VERSION, detailRqst.m_Version);
					detailResp.Reset(rlSessionDetailResponse::RESPONSE_INVALID_VERSION, SESSION_DETAIL_REQUEST_VERSION);
				}
				else if(rlVerify(detailRqst.Import(rqst->m_Data, rqst->m_SizeofData)))
				{
					rlDebug("OnRequest [%" I64FMT "d] :: Received Request - From: %s, Addr: %s, ID: 0x%08x, Token: 0x%016" I64FMT "x, Flags: 0x%x, Version: %d, Disc: 0x%08x", 
						rlGetPosixTime(), 
						detailRqst.m_FromGamerName, 
						NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr), 
						detailRqst.m_UniqueID, 
						detailRqst.m_SessionInfo.GetToken().GetValue(), 
						detailRqst.m_Flags,
						detailRqst.m_Version, 
						detailRqst.m_Discriminator);

					detailResp.SetUniqueID(detailRqst.m_UniqueID);

					if(detailRqst.m_Discriminator != s_rlSesMgrDiscriminator)
					{
						rlDebug("OnRequest :: Invalid Discriminator - Local: %d, Remote: %d", s_rlSesMgrDiscriminator, detailRqst.m_Discriminator);
						detailResp.Reset(rlSessionDetailResponse::RESPONSE_INVALID_DISCRIMINATOR, s_rlSesMgrDiscriminator);
					}
					else
					{
						const rlSession* session = GetSession(detailRqst.m_SessionInfo);
						if(NULL != session)
						{
							if(session->IsHost())
							{
								rlSessionInfo sessionInfo;
								const rlSession* dlgtSession = NULL;

								// call delegate, if bound
								if(ms_rlSesMgrQueryDelegate.IsBound())
								{
									dlgtSession = ms_rlSesMgrQueryDelegate(session, detailRqst.m_GamerHandle, &sessionInfo);
								}

								if(sessionInfo.IsValid())
								{
									// we have been given a session to forward this request to
									rlDebug("OnRequest :: Forwarding request to - 0x%016" I64FMT "x", sessionInfo.GetToken().m_Value);
									detailResp.Reset(rlSessionDetailResponse::RESPONSE_FORWARD, sessionInfo);
								}
								else
								{
									// if we were given a different session
									if(dlgtSession)
										session = dlgtSession;

									if(session)
									{
										rlDebug("OnRequest :: Giving information back for - 0x%016" I64FMT "x", session->GetSessionInfo().GetToken().m_Value);

										// get data mine data if requested
										rlSessionInfoMine infoMine;
										if((detailRqst.m_Flags & rlSessionDetailRequest::REQUEST_FLAG_DATA_MINE) != 0 && ms_rlSesMgrGetInfoMineDelegate.IsBound())
										{
											ms_rlSesMgrGetInfoMineDelegate(session, &infoMine);
											rlDebug("OnRequest :: Requested data mine. Size: %u", infoMine.m_SizeofData);
										}

										rlPeerInfo hostPeerInfo;
										hostPeerInfo.Init(session->GetHostPeerId(), session->GetHostPeerAddress());

										detailResp.Reset(rlSessionDetailResponse::RESPONSE_HAVE_DETAILS,
															session->GetOwner().GetGamerHandle(),
															session->GetOwner().GetName(),
															hostPeerInfo,
															session->GetSessionInfo(),
															session->GetConfig(),
															session->GetFilledSlots(RL_SLOT_PUBLIC),
															session->GetFilledSlots(RL_SLOT_PRIVATE),
															session->GetSessionUserData(),
															session->GetSessionUserDataSize(),
															infoMine.m_Data,
															infoMine.m_SizeofData);
									}
								}
							}
							else
							{
								rlDebug("OnRequest :: In session but not session host - 0x%016" I64FMT "x", session->GetSessionInfo().GetToken().m_Value);
								detailResp.Reset(rlSessionDetailResponse::RESPONSE_NOT_HOSTING, 0);
							}
						}
						else
						{
							rlDebug("OnRequest :: Not a member of requested session");
							detailResp.Reset(rlSessionDetailResponse::RESPONSE_INVALID_SESSION, 0);
						}
					}
				}
				else
				{
					rlError("OnRequest :: Failed to import %s from:" NET_ADDR_FMT "", detailRqst.GetMsgName(), NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));
					detailResp.Reset(rlSessionDetailResponse::RESPONSE_INVALID_DATA, 0);
				}
			}
			else
			{
				rlError("OnRequest :: Failed to import stubbed %s from:" NET_ADDR_FMT "", detailRqst.GetMsgName(), NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));
				detailResp.Reset(rlSessionDetailResponse::RESPONSE_INVALID_DATA, 0);
			}
			
			// send response
			transactor->SendResponse(rqst->m_TxInfo, detailResp);
		}

#if !__NO_OUTPUT
        else
        {
            const char* msgName = netMessage::GetName(rqst->m_Data, rqst->m_SizeofData);
            if(msgName)
            {
                rlDebug("Ignoring %s from:" NET_ADDR_FMT "",
                            msgName,
                            NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));
            }
        }
#endif  //!__NO_OUTPUT
    }
    rcatchall
    {
    }
}

void
rlSessionManager::OnPresenceEvent(const rlPresenceEvent* event)
{
    if(PRESENCE_EVENT_SC_MESSAGE == event->GetId())
    {
        rlPresenceEventScMessage* scMsg = 
            (rlPresenceEventScMessage*)event;

        if(scMsg->m_Message.IsA<rlScPresenceMessageMpSendInvite>())
        {
            //The server told us to invite someone into the our session.
		    rlScPresenceMessageMpSendInvite msg;
		    if(msg.Import(scMsg->m_Message))
		    {
                OUTPUT_ONLY(char ghStr[RL_MAX_GAMER_HANDLE_CHARS]);
                rlDebug("Received command to invite '%s' into our session",
                        msg.m_InviteeGamerHandle.ToString(ghStr));
                rlSession* session = GetSession(msg.m_SessionInfo);
                if(session)
                {
                    if(!session->SendInvites(&msg.m_InviteeGamerHandle, 1, "", "", NULL))
                    {
                        rlError("Failed to invite '%s'", ghStr);
                    }
                }
                else
                {
                    rlError("Can't invite '%s' - session doesn't exist", ghStr);
                }
            }
        }
    }
}

#if SUPPORT_LAN_MULTIPLAYER
void
rlSessionManager::HandleLanQueries()
{
    if(!s_rlSesMgrLanSkt.IsCreated())
    {
        if(netHardware::CreateSocket(&s_rlSesMgrLanSkt,
                                    GetLanMatchingPort(),
                                    NET_PROTO_UDP,
                                    NET_SOCKET_NONBLOCKING))
        {
        }
        else
        {
            rlError("Error creating LAN listener socket");
        }
    }

    if(s_rlSesMgrLanSkt.CanSendReceive())
    {
        u8 pktBuf[netPacket::MAX_BYTE_SIZEOF_PACKET];
        netSocketAddress sender;
        rlMsgSearchRequest rqst;

        int len = 0;

        //Prevent DOS by looping 10 times max.
        static const int MAX_LOOPS  = 10;

        int count = MAX_LOOPS;

        do 
        {
            netSocketError sktErr;

            len = s_rlSesMgrLanSkt.Receive(&sender, pktBuf, sizeof(pktBuf), &sktErr);

            if(0 == len)
            {
                break;
            }
            else if(len < 0)
            {
                rlError("Error receiving on LAN listen socket: %s",
                        netSocketErrorString(sktErr));
                break;
            }

            unsigned sizeofRqst;

            rlDebug("Received session search request from:" NET_ADDR_FMT "",
                        NET_ADDR_FOR_PRINTF(sender));

			// check the port as well - we can run multiple instances on the same PC for testing.
            if(sender.GetIp() == s_rlSesMgrLanSkt.GetAddress().GetIp()
                && sender.GetPort() == s_rlSesMgrLanSkt.GetAddress().GetPort())
            {
                rlDebug("Received LAN session search request from ourselves - ignoring.");
                continue;
            }
            //First 2 bytes are the size of the packet.
            else if(len < 2
                    || !rqst.Import(&((const u8*)pktBuf)[2], len - 2, &sizeofRqst)
                    || int(sizeofRqst + 2) != len)
            {
                rlDebug("Invalid search request - ignoring.");
                continue;
            }
            
            rlSessionManagerLink* link = rlSessionManagerGetHead().m_Next;
            for(; &rlSessionManagerGetHead() != link; link = link->m_Next)
            {
                const rlSession* session = GET_SESSION(link);
                if(!session->IsEstablished())
                {
                    //Not established - ignoring
                    continue;
                }
                else if(!session->IsLan())
                {
                    //Not a LAN session - ignoring.
                    continue;
                }
                else if(!session->IsHost())
                {
                    //Not the host - ignoring.
                    continue;
                }
                else if(!session->IsAnswerLanQueriesEnabled())
                {
                    //Not answering LAN queries - ignoring.
                    continue;
                }
                else if(session->GetEmptySlots(RL_SLOT_PUBLIC) == 0)
                {
                    //No empty slots - ignoring
                    continue;
                }

                const rlSessionConfig& cfg = session->GetConfig();
                const rlMatchingAttributes& myAttrs = cfg.m_Attrs;

                //Respond if the filter matches our attributes.
                if(!rqst.m_Filter.IsMatch(myAttrs))
                {
                    continue;
                }

                rlMsgSearchResponse resp;
                rlSessionDetail& detail = resp.m_Result;
                detail.Reset(session->GetOwner().GetPeerInfo(),
                                session->GetSessionInfo(),
                                session->GetConfig(),
								session->GetOwner().GetGamerHandle(),
                                session->GetOwner().GetName(),
                                session->GetFilledSlots(RL_SLOT_PUBLIC),
                                session->GetFilledSlots(RL_SLOT_PRIVATE),
								session->GetSessionUserData(),
								session->GetSessionUserDataSize(),
								NULL,
								0);

                resp.m_Nonce = rqst.m_Nonce;

                unsigned sizeofPkt;
                if(rlVerify(resp.Export(&pktBuf[2], sizeof(pktBuf)-2, &sizeofPkt)))
                {
                    rlDebug("Answering session search request from:" NET_ADDR_FMT "",
                                NET_ADDR_FOR_PRINTF(sender));

                    //First 2 bytes is packet size.
                    *(u16*)pktBuf = u16(sizeofPkt + 2);
                    s_rlSesMgrLanSkt.Broadcast(sender.GetPort(), pktBuf, sizeofPkt + 2);
                }
            }
        }
        while(len > 0 && --count > 0);
    }
}
#endif

}   //namespace rage
