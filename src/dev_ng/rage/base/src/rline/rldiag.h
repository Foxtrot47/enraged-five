// 
// rline/rldiag.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLDIAG_H
#define RLINE_RLDIAG_H

#include "diag/channel.h"
#include "rline/rl.h"
#include "system/param.h"
#include "system/stack.h"

namespace rage
{

RAGE_DECLARE_CHANNEL(rline)

#define rlDebug(fmt, ...)						RAGE_DEBUGF1(__rage_channel, fmt, ##__VA_ARGS__)
#define rlDebug1(fmt, ...)						RAGE_DEBUGF1(__rage_channel, fmt, ##__VA_ARGS__)
#define rlDebug2(fmt, ...)						RAGE_DEBUGF2(__rage_channel, fmt, ##__VA_ARGS__)
#define rlDebug3(fmt, ...)						RAGE_DEBUGF3(__rage_channel, fmt, ##__VA_ARGS__)
#define rlDisplay(fmt, ...)						RAGE_DISPLAYF(__rage_channel, fmt, ##__VA_ARGS__)
#define rlWarning(fmt, ...)						RAGE_WARNINGF(__rage_channel, fmt, ##__VA_ARGS__)
#define rlError(fmt, ...)						RAGE_ERRORF(__rage_channel, fmt, ##__VA_ARGS__)
#define rlCondLogf(cond, severity, fmt, ...)	RAGE_CONDLOGF(__rage_channel, cond, severity, fmt, ##__VA_ARGS__)

#if USE_NET_ASSERTS
#if __ASSERT
#define rlVerify(cond)							RAGE_VERIFY(__rage_channel, cond)
#define rlVerifyf(cond, fmt, ...)				RAGE_VERIFYF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#else
#define rlVerify(cond)							( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__rage_channel), #cond, __FILE__, __LINE__, "") )
#define rlVerifyf(cond, fmt, ...)				( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__rage_channel), #cond, __FILE__, __LINE__, fmt, ##__VA_ARGS__) )
#endif  // __ASSERT

#if __ASSERT
#define rlAssert(cond)							RAGE_ASSERT(__rage_channel, cond)
#define rlAssertf(cond, fmt, ...)				RAGE_ASSERTF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#else
#define rlAssert(cond)			do { static bool __FILE__l__LINE__ =false;  \
	if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); \
	RAGE_ERRORF(__rage_channel, "NetworkAssertf(%s) FAILED", #cond); __FILE__l__LINE__=true;  } } while(false)
#define rlAssertf(cond, fmt, ...)	do { static bool __FILE__l__LINE__ =false;  \
	if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); char formatted[1024]; formatf(formatted, fmt, ##__VA_ARGS__); \
	RAGE_ERRORF(__rage_channel, "NetworkAssertf(%s) FAILED: %s", #cond, formatted); __FILE__l__LINE__=true;  } } while(false)
#endif  // __ASSERT

#else
#define rlVerify(cond)							RAGE_VERIFY(__rage_channel, cond)
#define rlVerifyf(cond, fmt, ...)				RAGE_VERIFYF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#define rlAssert(cond) 							RAGE_ASSERT(__rage_channel, cond)
#define rlAssertf(cond, fmt, ...) 				RAGE_ASSERTF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#endif // USE_NET_ASSERTS

}   //namespace rage

#endif  //RLINE_RLDIAG_H

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __rage_channel
#define __rage_channel rline
