// 
// rline/rlNpWebApi.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include <sdk_version.h>
#if SCE_ORBIS_SDK_VERSION >= 0x00920020u
#ifndef RLINE_RLNPWEBAPI_H
#define RLINE_RLNPWEBAPI_H

#include <np.h>

#include "net/status.h"
#include "net/task.h"
#include "rline/rlnpcommon.h"
#include "rline/rlnpwebapiworkitem.h"
#include "rline/rldiag.h"
#include "system/ipc.h"

// C:\Program Files (x86)\SCE\ORBIS\documentation\pdf\en\WebAPI_Doc\User_Profile_WebAPI-Reference_e.pdf
#define SCE_NP_BASIC_MAX_PRESENCE_SIZE 128 
#define SCE_NP_BASIC_MAX_PRESENCE_BASE64_SIZE 172
#define SCE_NP_GAME_STATUS_MAX_BUF_SIZE 192

namespace rage
{

class rlTaskBase;

class rlNpWebApi
{

public:
    rlNpWebApi();
    virtual ~rlNpWebApi();

    bool Init();
	void Update();
	void Shutdown();

	// Callbacks
	bool RegisterPushEventCallbacks(const int localGamerIndex, bool bUnregister);

	// User Profile
	bool GetProfile(const int localGamerIndex);
	bool GetFriendsList(const int localGamerIndex, rlNpGetFriendsListWorkItem* workItem, unsigned* totalFriendsPtr, 
						int startIndex, rlFriend* friends, unsigned * numFriends, int maxFriends, int friendFlags);
	bool DeleteGameData(const int localGamerIndex);
	bool DeleteGameStatus(const int localGamerIndex);
	bool GetPresence(const int localGamerIndex, const rlGamerHandle& gamer);
	bool PutGameDataBlob(const int localGamerIndex, const void* blob, int blobSize);
	bool PutGameStatus(const int localGamerIndex, const char* gameStatus);
	bool PutGamePresence(const int localGamerIndex, const char* gameStatus, const void* gameData, int gameDataSize);
	bool GetBlockList(const int localGamerIndex);

	// Personal Details
	bool GetPlayerNames(const int localGamerIndex, const rlGamerHandle* handles, unsigned numHandles, rlDisplayName* displayNames, rlGamertag* gamertags, unsigned flags, netStatus* status);

	// Commerce
	bool GetCatalog(const int localGamerIndex, rlNpGetCatalogDetail* detail, netStatus* status);
	bool GetProductInfo(const int localGamerIndex, rlNpGetProductDetail* detail, netStatus* status);

	// Entitlements
	bool GetEntitlements(const int localGamerIndex, rlNpGetEntitlementsDetail* detail, netStatus* status);
	bool SetEntitlementCount(const int localGamerIndex, rlNpSetEntitlementsDetail* detail, const char* entitlement, int amount, netStatus* status);

	// Activity Feed
	bool PostActivityFeedStory(const int localGamerIndex, rlNpActivityFeedStory* detail);
	bool PostActivityFeedOnlinePlayedWith(const int localGamerIndex, rlNpActivityFeedPlayedWithDetails* detail);
	bool GetActivityFeedVideoData(const int localGamerIndex, rlNpGetActivityFeedVideoDataDetails* detail, netStatus* status);

	// S/I - Sessions
	bool PostSession(const int localGamerIndex, const char * sessionImagePath, rlSession* session, netStatus* status);
	bool PostSession(const int localGamerIndex, uint8_t* img, int imgLen, rlSession* session, netStatus* status);
	bool GetSessions(const int localGamerIndex, rlNpWebApiSessionDetail* sessions, int maxNumSessions, int * numSessions, netStatus* status);
	bool DeleteSession(const int localGamerIndex, const rlSceNpSessionId& session, netStatus* status);
	bool GetSessionData(const int localGamerIndex, const rlSceNpSessionId& session, rlSessionInfo* outInfo, netStatus* status);
	bool PutChangeableSessionData(const int localGamerIndex, rlSession* session, netStatus* status);
	bool GetChangeableSessionData(const int localGamerIndex, const rlSceNpSessionId& session, rlSessionInfo* outInfo, netStatus* status);

	// S/i - Members (Essentially WebAPI Join/Leave Notification)
	bool PostMember(const int localGamerIndex, const rlSceNpSessionId& session, netStatus* status);
	bool PostMembers(const int localGamerIndex, const rlSceNpSessionId& session, rlGamerHandle* gamerHandles, int numGamers, netStatus* status);
	bool DeleteMember(const int localGamerIndex, const rlSceNpSessionId& session, rlSceNpAccountId targetId, netStatus* status);
	bool DeleteMembers(const int localGamerIndex, const rlSceNpSessionId& session, rlGamerHandle* gamerHandles, int numGamers, netStatus* status);

	// S/I - Invitations
	bool GetInvitations(const int localGamerIndex, rlNpWebApiInvitationDetail* invites, int maxNumInvitations, int* numInvitations, netStatus* status);
	bool GetInvitationList(const int localGamerIndex, rlNpWebApiInvitationDetail* invites, int maxNumInvitations, int * numInvitations, netStatus* status);
	bool GetInvitation(const int localGamerIndex, rlNpWebApiInvitationDetail* invite);
	bool GetInvitationData(const int localGamerIndex, rlNpWebApiInvitationDetail* invite);
	bool ClearInvitation(const int localGamerIndex, rlNpWebApiInvitationDetail* invite);
	bool PostInvitation(const int localGamerIndex, rlNpWebApiPostInvitationParam* param);

	// Identity Mapper
	bool GetAccountId(const int localGamerIndex, const rlSceNpOnlineId& targetOnlineId, rlSceNpAccountId* out_accountId, netStatus* status);
	bool GetAccountIds(const int localGamerIndex, rlSceNpOnlineId* targetOnlineIds, const unsigned numIds, rlSceNpAccountId* out_accountIds, netStatus* status);
	bool GetOnlineId(const int localGamerIndex, const rlSceNpAccountId targetAccountId, rlSceNpOnlineId* out_onlineId, netStatus* status);
	bool GetOnlineIds(const int localGamerIndex, rlSceNpAccountId* targetAccountIds, const unsigned numIds, rlSceNpOnlineId* out_onlineIds, netStatus* status);
	
	// Task Cancellation
	bool Cancel(netStatus* status);

#if !__NO_OUTPUT
	void CheckWebApiHeap(const bool logDetails = false);
#endif

private:
	void ShutdownSceNet();
	bool Prepare(const int localGamerIndex);
	void CheckPendingOperations();
	void ClearPendingOperations();

	// NpWebAPI Push Event Callback Registration
	bool RegisterFriendUpdateCB(const int localGamerIndex);
	void UnregisterFriendUpdateCB(const int localGamerIndex);
	bool RegisterBlocklistUpdateCB(const int localGamerIndex);
	void UnregisterBlocklistUpdateCB(const int localGamerIndex);
	bool RegisterPresenceOnlineStatusCB(const int localGamerIndex);
	void UnregisterPresenceOnlineStatusCB(const int localGamerIndex);

	static void QueueTask(rlTaskBase* task);

public:
	// Push Event Callbacks
	static void FriendUpdateCB(int32_t userCtxId, int32_t callbackId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom,
		const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);
	static void PresenceUpdateCB(int32_t userCtxId, int32_t callbackId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom,
		const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);
	static void BlocklistUpdateCB(int32_t userCtxId, int32_t callbackId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom,
		const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);
	static void InivitationReceivedCB(int32_t userCtxId, int32_t callbackId, const SceNpPeerAddress *pTo, const SceNpPeerAddress *pFrom,
		const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);

	// Service Push Event Callbacks
	static void GameTitleInfoCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
		const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);
	static void GameStatusCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
		const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);
	static void GameDataCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
		const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);
	static void InivitationReceivedCB(int32_t userCtxId, int32_t callbackId, const char *pNpServiceName, SceNpServiceLabel pReserved, const SceNpPeerAddress *pTo,
		const SceNpPeerAddress *pFrom, const SceNpWebApiPushEventDataType *pDataType, const char *pData, size_t dataLen, void *pUserArg);

private:
	static const int MAX_QUEUED_TASKS = 32;

	enum InvitationState
	{
		STATE_ACCEPTED,
		STATE_GETTING_DETAILS,
	};

	int m_LibNetMemId;
	int m_LibSslCtxId;
	int m_LibHttpCtxId;
	int m_WebApiLibCtxId;
	int m_WebApiUserCtxId[RL_MAX_LOCAL_GAMERS];

	int m_UserServiceId[RL_MAX_LOCAL_GAMERS];
	int m_GamerIndices[RL_MAX_LOCAL_GAMERS];

	rlNpWebApiRegisterCallbacksWorkItem m_RegisterCallbacksWorkItem[RL_MAX_LOCAL_GAMERS];
	bool m_bCallbacksRegistered[RL_MAX_LOCAL_GAMERS];

	static sysCriticalSectionToken sm_TaskCs;
	static atFixedArray<rlTaskBase*, MAX_QUEUED_TASKS> sm_QueuedTask;

	// User Profile
	static int sm_FriendUpdateFilterIds[RL_MAX_LOCAL_GAMERS];
	static int sm_FriendUpdateCallbackIds[RL_MAX_LOCAL_GAMERS];
	static int sm_BlocklistUpdateFilterIds[RL_MAX_LOCAL_GAMERS];
	static int sm_BlocklistUpdateCallbackIds[RL_MAX_LOCAL_GAMERS];
	static int sm_PresenceFilterIds[RL_MAX_LOCAL_GAMERS];
	static int sm_PresenceCallbackIds[RL_MAX_LOCAL_GAMERS];

	// Profiling
#if !__NO_OUTPUT
	size_t m_MaxSslPoolUsage;
	size_t m_MaxHttpPoolUsage;
#endif
};

} // namespace rage

#endif // RLINE_RLNPWEBAPI_H
#endif // #if SCE_ORBIS_SDK_VERSION >= 0x00920020u
#endif // RSG_ORBIS