// 
// rline/rlpeeraddress.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPEERADDRESS
#define RLINE_RLPEERADDRESS

#include "net/peeraddress.h"
namespace rage
{
typedef netPeerAddress rlPeerAddress;
}   //namespace rage


#endif   //RLINE_RLPEERADDRESS
