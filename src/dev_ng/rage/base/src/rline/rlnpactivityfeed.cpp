// 
// rline/rlnpactivityfeed.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include <sdk_version.h>
#include "rlnpactivityfeed.h"

#include "rline/rlnp.h"
#include "rline/rlpresence.h"

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rline, rlnpactivityfeed);
	#undef __rage_channel
	#define __rage_channel rline_npactivityfeed


	rlNpActivityFeed::rlNpActivityFeed()
	{

	}

	rlNpActivityFeed::~rlNpActivityFeed()
	{
		Shutdown();
	}

	bool rlNpActivityFeed::Init()
	{
		return true;
	}

	void rlNpActivityFeed::Shutdown()
	{

	}

	// normally ...use condensed caption, no translation, as key
	void rlNpActivityFeed::Start(int key)
	{
		m_activityFeedStory.StartContent(key);
	}

	void rlNpActivityFeed::PostCaptions(sysLanguage language, const char* caption, const char* condensedCaption)
	{
		m_activityFeedStory.AddCaptions(language, caption, condensedCaption);
	}

	void rlNpActivityFeed::AddSubStringToCaption(sysLanguage language, const char* subString)
	{
		m_activityFeedStory.AddCaptionSubString(language, subString);
	}

	void rlNpActivityFeed::ConfirmCaptionSubStringComplete()
	{
		m_activityFeedStory.ConfirmCaptionSubStringComplete();
	}

	void rlNpActivityFeed::AddSubValueToCaptionFloat(float subValue, int noofDecimalPlaces)
	{
		m_activityFeedStory.AddCaptionSubFloat(subValue, noofDecimalPlaces);
	}

	void rlNpActivityFeed::AddSubValueToCaptionInt(int subValue)
	{
		m_activityFeedStory.AddCaptionSubInt(subValue);
	}

	void rlNpActivityFeed::PostSmallImageURL(const char* imageURL, const char* aspectRatio)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::smallImage, imageURL);
		m_activityFeedStory.SetImageAspectRatio(aspectRatio);
	}

	void rlNpActivityFeed::PostLargeImageURL(const char* imageURL)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::largeImage, imageURL);
	}

	void rlNpActivityFeed::PostThumbnailImageURL(const char* imageURL)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::thumbnail, imageURL);
	}

	void rlNpActivityFeed::PostVideoURL(const char* imageURL)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::video, imageURL);
	}

	void rlNpActivityFeed::PostCustomCaption(const char* customCaptionString)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::customCaption, customCaptionString);
	}

	void rlNpActivityFeed::PostActionURL(const char* urlString)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::URL, urlString);
	}

	void rlNpActivityFeed::PostActionStart(const char* commandLineString)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::startAction, commandLineString);
	}

	void rlNpActivityFeed::PostActionStore(const char* productCodeString)
	{
		m_activityFeedStory.AddContent(rlNpActivityFeedStory::ContentType::store, productCodeString);
	}

	void rlNpActivityFeed::AppendActionURL(const char* urlString)
	{
		m_activityFeedStory.AppendContent(rlNpActivityFeedStory::ContentType::URL, urlString);
	}

	void rlNpActivityFeed::AppendActionStart(const char* commandLineString)
	{
		m_activityFeedStory.AppendContent(rlNpActivityFeedStory::ContentType::startAction, commandLineString);
	}

	void rlNpActivityFeed::PostTagForActionURL(sysLanguage language, const char* labelString)
	{
		m_activityFeedStory.AddContentTag(language, rlNpActivityFeedStory::ContentType::URL, labelString);
	}

	void rlNpActivityFeed::PostTagForActionStart(sysLanguage language, const char* labelString)
	{
		m_activityFeedStory.AddContentTag(language, rlNpActivityFeedStory::ContentType::startAction, labelString);
	}
	
	void rlNpActivityFeed::PostTagForActionStore(sysLanguage language, const char* labelString)
	{
		m_activityFeedStory.AddContentTag(language, rlNpActivityFeedStory::ContentType::store, labelString);
	}

	void rlNpActivityFeed::PostThumbnailForActionURL(const char* thumbnail)
	{
		m_activityFeedStory.AddContentThumbnail(rlNpActivityFeedStory::ContentType::URL, thumbnail);
	}

	void rlNpActivityFeed::PostThumbnailForActionStart(const char* thumbnail)
	{
		m_activityFeedStory.AddContentThumbnail(rlNpActivityFeedStory::ContentType::startAction, thumbnail);
	}

	void rlNpActivityFeed::PostThumbnailForActionStore(const char* thumbnail)
	{
		m_activityFeedStory.AddContentThumbnail(rlNpActivityFeedStory::ContentType::store, thumbnail);
	}

	void rlNpActivityFeed::PostCurrentMessage()
	{
		if (!m_activityFeedStory.GetPostType() || !m_activityFeedStory.HasCaptions())
			return;

		int localGamerIndex = rlPresence::GetActingUserIndex();
		g_rlNp.GetWebAPI().PostActivityFeedStory(localGamerIndex, &m_activityFeedStory);
	}

	void rlNpActivityFeed::StartOnlinePlayedWith(const char* gameMode)
	{
		m_activityFeedOnlineDetails.Start(gameMode);
	}

	void rlNpActivityFeed::AddOnlinePlayedWith(const rlGamerHandle& player)
	{
		m_activityFeedOnlineDetails.AddPlayerToList(player);
	} 

	void rlNpActivityFeed::PostOnlinePlayedWith()
	{
		int userIndex = rlPresence::GetActingUserIndex();
		g_rlNp.GetWebAPI().PostActivityFeedOnlinePlayedWith(userIndex, &m_activityFeedOnlineDetails);
	}

	void rlNpActivityFeed::GetActivityFeedVideoData()
	{
		int userIndex = rlPresence::GetActingUserIndex();
		g_rlNp.GetWebAPI().GetActivityFeedVideoData(userIndex, &m_getActivityFeedDetails, &m_getActivityFeedVideoDataStatus);
	}

} // namespace rage

#endif // RSG_ORBIS
