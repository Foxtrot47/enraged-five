// 
// rline/rlnp.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNP_H
#define RLINE_RLNP_H

#if RSG_NP

#include "rlnpbandwidth.h"
#include "rlnpbasic.h"
#include "rlnpcommon.h"
#include "rlnptrophy.h"

#include "data/autoid.h"                 //For events
#include "data/bitbuffer.h"
#include "file/limits.h"
#include "net/netaddress.h"
#include "net/status.h"
#include "rldiag.h"
#include "system/criticalsection.h"
#include "system/memops.h"

#include <np.h>
#include "rlnpwebapi.h"
#include "rlnpparty.h"
#include "rlnpingamemessagemgr.h"
#include "rlnpcommondialog.h"
#include "rlnpauth.h"
#include "rlnpactivityfeed.h"
#include "system/service.h"

#if RSG_BANK
#include "rlnpfaulttolerance.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"
#endif // RSG_BANK

struct SceNpId;

namespace rage
{
RAGE_DECLARE_SUBCHANNEL(rline, np)

class rlNpTitleId;

//Account namespace and matchmaking environment, as determined by our ticket
//issuer ID.  Some environments can have multiple issuer IDs; for example, 
//"np" is anything greater than 255.
//
//The environment can be set on devkits under the System->Debug menu.
//Production hardware is always set to "np", and cannot be changed.
//Only Sony QA can use prod-qa.
//
//IMPORTANT: Do not change the these values after a game ships, as this
//           will make any persisted gamerhandles appear to be from
//           the wrong environment.  This can cause, for example, it to look
//           like there are duplicate leaderboard entries, as there will be
//           multiple records with the same gamername but slightly different
//           gamerhandles.
enum rlNpEnvironment
{
    RLNP_ENV_UNRECOGNIZED   = -2,   //We have a valid ticket, but don't recognize the issuer
    RLNP_ENV_UNKNOWN        = -1,   //Ticket hasn't been retrieved yet
    RLNP_ENV_SP_INT			= 0,                //"sp-int" environment
	RLNP_ENV_PROD_QA		= 1,               //"prod-qa" environment
	RLNP_ENV_NP				= 2,                    //"np" environment
	RLNP_ENV_PROD_QA_2		= 3,             //"prod-qa" environment_2
    RLNP_NUM_ENV,

    RLNP_ENV_NUM_BITS  = datBitsNeeded<RLNP_NUM_ENV>::COUNT + 1 //+1 for sign bit
};

enum rlNpIssuerId
{
	RLNP_ISSUER_UNKNOWN = -1,	// unknown
	RLNP_ISSUER_DEV = 1,		// SP-INT
	RLNP_ISSUER_CERT = 8,		// QA-PROD
	RLNP_ISSUER_PROD = 256		// PROD (255+)
};

// PURPOSE
//	SIE Product regions defined by Sony
enum rlNpSceRegion
{
	Region_Invalid = -1,    // Unknown
	Region_SIEE,            // Europe
	Region_SIEA,            // America
	Region_SIEJ,            // Japan
	Region_SIEAsia,         // Asia (excluding Japan)
	Region_Max,
};

// proxy of SceNpAuthorizationCode
struct rlNpAuthorizationCode
{
	char code[128 + 1]; // Compile time asserted in rlnp.cpp
	uint8_t padding[7];
};

struct rlNpManagerEvent
{
	enum EventId
	{
		RLNP_EVENT_INVALID = -1,
		// state
		RLNP_EVENT_STATE_UNKNOWN = 0,
		RLNP_EVENT_STATE_SIGNED_IN,
		RLNP_EVENT_STATE_SIGNED_OUT,
		// user service 
		RLNP_EVENT_USER_SERVICE_LOGIN,
		RLNP_EVENT_USER_SERVICE_LOGOUT,
		// presence
		RLNP_EVENT_PRESENCE,
	};

	rlNpManagerEvent()
		: m_EventId(RLNP_EVENT_INVALID)
		, m_Context(-1)
		, m_UserServiceId(-1)
	{
	}

	rlNpManagerEvent(const EventId eventId,
		const int context,
		const int userServiceId)
		: m_EventId(eventId)
		, m_Context(context)
		, m_UserServiceId(userServiceId)
	{
	}

	int m_EventId;
	int m_Context;
	int m_UserServiceId;
};

class rlNp
{
public:

    //Max size of string returned by GetSignInName()
    enum { MAX_SIGNIN_NAME_CHARS = 64 }; //CELL_SYSUTIL_SYSTEMPARAM_CURRENT_USERNAME_SIZE

	// PURPOSE
	//	Initialize PS4 libraries.
	static void InitLibs();

	//PURPOSE
    //  Convenience method for getting rlNpTitleId from titleid passed to rlInit.
    static const rlNpTitleId& GetTitleId();

    //PURPOSE
    //  Return the region the game was built to run in.
    static rlGameRegion GetGameRegion();

    //PURPOSE
    //  Decodes the region value from the product code.
    //NOTES
    //  The product code is in the format XXXXYYYYY (with letters for XXXX
    //  and numbers for YYYYY).  For example, "BLUS30418".
	static rlGameRegion GetGameRegionFromProductCode(const char* productCode);

	// Initialize delegates to system service
	static void InitServiceDelegate();

#if !__NO_OUTPUT
	const char* GetNpEnvironmentAsString(const rlNpEnvironment env);
#endif

     rlNp();
    ~rlNp();

    bool Init();
    void Update();
    void Shutdown();

    //PURPOSE
    //  Returns true if rlNp has been initialized.
    bool IsInitialized() const;

    //PURPOSE
    //  Subsystem access.
    rlNpBasic&      GetBasic() { return m_NpBasic; }
    rlNpTrophy&     GetTrophy() { return m_NpTrophy; }
    rlNpWebApi&     GetWebAPI() { return m_NpWebApi; }
	rlNpParty&		GetNpParty() { return m_NpParty; }
	rlNpInGameMessageMgr& GetInGameMsgMgr() { return m_NpInGameMsgMgr; }
	rlNpCommonDialog& GetCommonDialog() { return m_NpCommonDialog; }
	rlNpAuth&		GetNpAuth() { return m_NpAuth; }
	rlNpActivityFeed& GetNpActivityFeed() { return m_NpActivityFeed; }
	RL_NP_BANDWIDTH_ONLY(rlNpBandwidth& GetNpBandwidth() { return m_NpBandwidth; })
#if RSG_BANK
	rlNpFaultTolerance& GetFaultTolerance() { return m_NpFaultTolerance; }
#endif

	// PURPOSE
	// Fired when the connection to NP is lost
	void OnNpConnectionLost(const int localGamerIndex, const rlNpSignedOfflineReason reason);

	// PURPOSE
	// Fired when the acting user is changed
	void OnNpActingUserChange(const int localGamerIndex);

    //PURPOSE
    //  Returns true if the user is not logged in or logging into NP. 
    bool IsOffline(const int localGamerIndex) const;

    //PURPOSE
    //  Returns true if the user is logged in to NP.
    //NOTES
    //  The use still might be in the process of gaining access to all
    //  necessary NP services, so IsLoggedIn() does not imply IsOnline().
    bool IsLoggedIn(const int localGamerIndex) const;

    //PURPOSE
    //  Returns true if the user is logged in to NP and able to
    //  access all necessary NP services.  Implies IsLoggedIn() will return true.
    bool IsOnline(const int localGamerIndex) const;

	//PURPOSE
	//	Updates the Np State, returning false if the NP functionality returns an error
	bool UpdateNpState(const int localGamerIndex);

	//PURPOSE
	//	Updates the Np Id, returning false if the NP functionality returns an error
	bool UpdateNpId(const int localGamerIndex);

    //PURPOSE
    //  Returns sign-in name for user, or 0 if not signed in.
	// TODO: ORBIS: Need to properly handle the Np status of all 4 logged in users.
    const char* GetSignInName(int localGamerIndex);

	//PURPOSE
	//  Returns Online ID for user (NP equivalent to XBL gamertag).  
	//  Returns null if user is not online.
	const rlSceNpOnlineId& GetNpOnlineId(const int localGamerIndex) const;

	//PURPOSE
	//	Returns Account ID for the user (NP equivalent of XBL XUID)
	//	Returns RL_INVALID_NP_ACCOUNT_ID if the user is not online.
	const rlSceNpAccountId& GetNpAccountId(const int localGamerIndex) const;

	//PURPOSE
	// Returns the account Id for a given online Id
	const rlSceNpAccountId& GetNpAccountIdFromNpOnlineId(const rlSceNpOnlineId& onlineId);

    //PURPOSE
    //  Add/remove a delegate that will be called with event notifications.
    void AddDelegate(rlNpEventDelegate* dlgt);
    void RemoveDelegate(rlNpEventDelegate* dlgt);
    void DispatchEvent(const rlNpEvent* e);

    //PURPOSE
    //  Returns environment we are logged into, based on ticket issuer ID.
    //  This will be RLNP_ENV_UNKNOWN if we are not logged in.
    rlNpEnvironment GetNativeEnvironment() const;

	//PURPOSE
	//  Returns environment combined with any forced overrides at run or compule time
	rlNpEnvironment GetEnvironment() const;

	//PURPOSE
	//  Convert between ROS <--> NP environments
	static rlNpEnvironment GetNpEnvironmentFromRlEnvironment(const rlEnvironment rosEnv);
	static rlEnvironment GetRlEnvironmentFromNpEnvironment(const rlNpEnvironment npEnv);

    //PURPOSE
    //  Returns the same value returned from sceNpManagerGetChatRestrictionFlag().
    //  sceNpManagerGetChatRestrictionFlag() is costly so we cache the value.
    int GetChatRestrictionFlag(int localGamerIndex) const;

	// Returns the Orbis User Service Id
	int GetUserServiceId(int localGamerIndex);

	//PURPOSE
	// Returns age for the user at index 0-3
	// Returns SCE_OK if the Age could be retrieved
	const int GetAge(const int localGamerIndex, int* agePtr);

	//PURPOSE
	// Returns the chat restriction for the user at index 0-3
	// Returns SCE_OK if the restriction could be retrieved
	const int GetChatRestriction(const int localGamerIndex, bool* restriction);

	//PURPOSE
	// Returns the ugc restriction for the user at index 0-3
	// Returns SCE_OK if the restriction could be retrieved
	const int GetUgcRestriction(const int localGamerIndex, bool* restriction);

	//PURPOSE
	// Returns if the user is a PlayStation Plus subscriber, required for real-time multiplayer
	const bool HasPlayStationPlusSubscription(const int localGamerIndex);

	//PURPOSE
	// Access to whether PS+ is required to enter real-time multiplayer
	// This can be enabled or disabled externally
	void SetPlayStationPlusRequiredForRealtimeMultiplayer(const int localGamerIndex, const bool isRequired);
	const bool IsPlayStationPlusRequiredForRealtimeMultiplayer(const int localGamerIndex);

	//PURPOSE
	// Call to check whether the local player has permission to access multiplayer
	// Combination of IsPlayStationPlusRequiredForRealtimeMultiplayer() || HasPlayStationPlusSubscription()
	const bool CanAccessRealtimeMultiplayer(const int localGamerIndex);

	// PURPOSE
	//	Get or set the PS4 session/invitation preview image.
	//	Required for all PS4 system session invitations
	const char * GetDefaultSessionImage();
	void SetDefaultSessionImage(const char* imagePath);

	// PURPOSE
	//	Retrieve the Client ID
	rlSceNpClientId* GetClientId() { return &m_ClientId; }

	// PURPOSE
	// Returns the index 0-3 of the given userId
	int GetUserServiceIndex(s32 userId);

	// Returns true if we're in a valid state to request permissions
	bool CanRequestPermissions(const int localGamerIndex);

	// Checks if we have a permission request pending
	bool IsRequestPermissionsPending(const int localGamerIndex);

	// PURPOSE
	//  Set content restriction - R4109 - Use the sceNpCheckNpAvailability() results to confirm that 
	//  the target user has signed in to PSNSM  before using the network features.
	void SetContentRestriction(const unsigned minAgeRating);

	//PURPOSE
	//  Return TRUE if we have a result that means a call to IsNpAvailable is valid
	bool HasAvailabilityResult(const int localGamerIndex);

	//PURPOSE
	//  Return TRUE if NP is available. 
	//  R4109 - Must be checked in case the game uses non related PlayStationŽNetwork libraries.
	bool IsNpAvailable(const int localGamerIndex) { return m_NpAuth.IsNpAvailable(localGamerIndex); }

	// PURPOPSE
	// Resets the NpUnavailability recheck timer and NP auth, requires the NP_UNAVAILABLE state
	bool RecheckNpAvailability(const int localGamerIndex);

	// PURPOSE
	//	Callback when NP presence switches from offline to online
	void SetPresenceStatus(const int localGamerIndex, SceNpGamePresenceStatus status);

	// PURPOSE
	// Returns TRUE if connected to PSN/R* heartbeat 
	bool IsConnectedToPresence(const int localGamerIndex);

	// PURPOSE
	//	Returns TRUE if the state machine is pending online due to checking NP auth, availability etc.
	bool IsAnyPendingOnline();
	bool IsPendingOnline(const int localGamerIndex);

	// PURPOSE
	//	Gets the Np CountryCode for the local gamer index
	bool GetCountryCode(const int localGamerIndex, char(&countryCode)[3]);

#if !__NO_OUTPUT
	const char* GetSceRegionAsString(const rlNpSceRegion region);
#endif

	// PURPOSE
	//	Gets the product region for the given account
	rlNpSceRegion GetSceRegion(const int localGamerIndex);

	// PURPOSE
	//	Gets the date of birth for the local gamer index
	bool GetDateOfBirth(const int localGamerIndex, u16& out_year, u8& out_month, u8& out_day);

	// PURPOSE
	//	Queues an event for processing
	void QueueEvent(const rlNpManagerEvent& evt);

private:
    enum eState
    {
        STATE_INVALID  = -1,                    //rlNp::Init() has not been called yet
        STATE_FIRST = 0,

        STATE_WAIT_FOR_NP_ONLINE = STATE_FIRST, //Waiting for NP to reach online state

		STATE_CHECK_NP_AVAILABILITY,			// Check the NP availability for this account
		STATE_WAIT_NP_AVAILABILITY,				// Waiting for the NP availability call to return
		STATE_NP_UNAVAILABLE,					//CheckNP Availability has determined you cannot access NP functionality

        STATE_ONLINE,                           //Have a ticket, and are considered online

		STATE_REQUEST_AUTH,						// Need to request NpAuth
		STATE_WAIT_FOR_AUTH,					// Waiting for NpAuth code result
		STATE_REQUEST_AUTH_FAILED,				// NpAuth request failed,

		STATE_REQUEST_PERMISSIONS,				// Need to retrieve permissions (age, content, ugc)
		STATE_WAIT_PERMISSIONS,					// Waiting for permissions
		STATE_REQUEST_PERMISSIONS_FAILED,		// Permission request failed

        STATE_NUM_STATES
    };

	void AddWidgets();
    void Reset(const int localGamerIndex);                       //Resets all data (except delegates), sets state to STATE_INVALID
    void Restart(const int localGamerIndex);                     //Calls Reset() and sets state to STATE_FIRST
    void RetryState(const int localGamerIndex); //Restarts the current state on the next Update()
    void StartState(const int localGamerIndex, const int state);   //Starts (or restarts) a state  
    void UpdateState(const int localGamerIndex, const int state);
    
	// Event processing
	void ProcessEvents(const int localGamerIndex = -1);
	bool ProcessStateEvent(const rlNpManagerEvent& evt, const int localGamerIndex);
	void ProcessUserServiceEventCommon(const int userId, const int localGamerIndex);

	// Returns true if we're in a valid state to request an Auth code
	bool CanRequestAuthCode(const int localGamerIndex);

	// PURPOSE
	// Retrieves the current NP presence status (ONLINE or OFFLINE), returns SCE_OK or error
	int GetGamePresenceStatus(const int localGamerIndex);

	// Convert the auth issuer id to Np rlEnvironment
	rlNpEnvironment GetEnvironmentFromIssuerId(int issuerId);
	rlNpEnvironment GetEnvironmentFromNativeEnvironment(rlNpEnvironment env);

	// Checks all conditions needed for PlayTogether event to be fired
	void AddPendingPlayTogetherEvent(const rlNpEventPlayTogetherHost& evt);
	void CheckPendingPlayTogetherEvent();

	// Service Events
	static ServiceDelegate sm_ServiceDelegate;
    static void OnServiceEvent(sysServiceEvent* evt);

	// Handle specific service events
	bool ProcessGameIntentEvent();

	static const unsigned RL_NP_EVENTS_QUEUE_SIZE = 64;
	rlNpEventQueue<rlNpManagerEvent, RL_NP_EVENTS_QUEUE_SIZE> m_MgrEventQueue;

    int m_State[RL_MAX_LOCAL_GAMERS];
    int m_NextState[RL_MAX_LOCAL_GAMERS];

    rlNpEventDelegator m_Delegator;

	//Environment, derived from last valid ticket we've fetched.
	rlNpEnvironment m_NativeEnv;
    rlNpEnvironment m_Env;

    mutable sysCriticalSectionToken m_Cs;

    unsigned m_OnlineStartTime;

	char m_SignInNames[RL_MAX_LOCAL_GAMERS][MAX_SIGNIN_NAME_CHARS];
	rlSceNpOnlineId m_OnlineIds[RL_MAX_LOCAL_GAMERS];
	rlSceNpAccountId m_AccountIds[RL_MAX_LOCAL_GAMERS];

    int m_NpStatus[RL_MAX_LOCAL_GAMERS];		// This is an enum under orbis, but we store as an int to avoid forward declarations
	SceNpGamePresenceStatus m_PresenceStatus[RL_MAX_LOCAL_GAMERS];

	// The last time the Np state machine transitioned to offline
	static const int PRESENCE_HEARTBEAT_DISCONNECT_INTERVAL_MS = 300000; // 300000 = 5 minutes
	static const int NP_STATE_CHANGE_TIMER_MS = 15000; // 15s
	unsigned m_uLastPresenceChangeTime;
	unsigned m_uTimeOfLastPresenceDisconnect;
	unsigned m_uNumUnackedHeartbeatPings;
	netRetryAndBackoffTimer m_NpUnavailabilityBackoff[RL_MAX_LOCAL_GAMERS];

	bool m_bPendingPlayTogetherEvent; 
	rlNpEventPlayTogetherHost m_PendingPlayTogetherEvent;

	bool m_PlayStationPlusRequiredForRealtimeMultiplayer[RL_MAX_LOCAL_GAMERS];

    rlNpBasic				m_NpBasic;
    rlNpTrophy				m_NpTrophy;
	rlNpWebApi				m_NpWebApi;
	rlNpParty				m_NpParty;
	rlNpAuth				m_NpAuth;
	rlNpActivityFeed		m_NpActivityFeed;
	rlNpInGameMessageMgr	m_NpInGameMsgMgr;
	rlNpCommonDialog		m_NpCommonDialog;
#if RL_NP_BANDWIDTH
	rlNpBandwidth			m_NpBandwidth;
#endif

	char m_DefaultSessionImage[RAGE_MAX_PATH];
	rlSceNpClientId m_ClientId;

    bool m_Initialized      : 1;
    bool m_OwnNp            : 1;

#if __BANK
public:
	rlNpFaultTolerance		m_NpFaultTolerance;
	static bool				sm_bPresenceForcedOffline;
	static bool				sm_bAuthUnavailable;
#endif
};

//Global instance.  
extern rlNp g_rlNp;

}

#endif // RSG_NP

#endif  //RLINE_NP_H
