#ifndef __TFIT_ECC_FAST_IECDHGREENSSFASTP256GAME__
#define __TFIT_ECC_FAST_IECDHGREENSSFASTP256GAME__

#if (RSG_CPU_X64 && !__RGSC_DLL && RSG_PC)
#include "wbecc_common_api.h"

#ifdef __cplusplus
extern "C" { 
#endif

extern wbecc_key_pair_t TFIT_ecc_fast_iECDHgreenSSfastP256game;

#ifdef __cplusplus
}
#endif

#endif
#endif /*__TFIT_ECC_FAST_IECDHGREENSSFASTP256GAME__*/

