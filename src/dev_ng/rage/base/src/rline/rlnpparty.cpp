// 
// rline/rlNpParty.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#include "rlnpparty.h"

#include "diag/seh.h"
#include "rline/rlnp.h"
#include "rline/rlnptitleid.h"
#include "string/string.h"
#include "system/memops.h"

#include <libsysmodule.h>
#pragma comment(lib, "libSceNpParty_stub_weak.a")

#include <np.h>

namespace rage
{

	RAGE_DEFINE_SUBCHANNEL(rline, npparty);
#undef __rage_channel
#define __rage_channel rline_npparty

extern const rlTitleId* g_rlTitleId;

rlNpParty::rlNpParty()
	: m_bInitialized(false)
{
	m_PartyMembers.clear();
	memset(&m_PartyState, 0, sizeof(m_PartyState));
}

void rlNpParty::InitLibs()
{
	if (sceSysmoduleLoadModule(SCE_SYSMODULE_NP_PARTY) != SCE_OK)
	{
		Quitf("Failed to load prx SCE_SYSMODULE_NP_PARTY");
	}
}

bool rlNpParty::Init()
{
	rtry
	{
		SceNpPartyInitializeParam params;
		sceNpPartyInitializeParamInitialize(&params);
	
		int ret = sceNpPartyInitialize(&params);
		rverify(ret == SCE_OK, catchall, rlError("sceNpPartyInitialize() failed. ret = 0x%x\n", ret));

		SceNpPartyEventHandlers stPartyEventHandler;
		memset(&stPartyEventHandler,0,sizeof(SceNpPartyEventHandlers));

		stPartyEventHandler.roomEventHandler			= PartyRoomEventHandler;
		stPartyEventHandler.voiceEventHandler			= PartyVoiceEventHandler;
		stPartyEventHandler.binaryMessageEventHandler	= PartyBinaryMessageEventHandler;

		ret = sceNpPartyRegisterHandler(&stPartyEventHandler,this);
		rverify(ret == SCE_OK, catchall, rlError("sceNpPartyRegisterHandler() failed. ret = 0x%x\n", ret));

		ret = RetrieveAllPartyInfo();
		rverify(ret == SCE_OK, catchall, rlError("RetrieveAllPartyInfo() failed. ret = 0x%x\n", ret));

		m_bInitialized = true;
	}
	rcatchall
	{
		sceNpPartyTerminate();
		m_bInitialized = false;
		return false;
	}

	return true;
}

void rlNpParty::Update()
{
	if (m_bInitialized)
	{
		int ret = sceNpPartyCheckCallback();
		if( ret != SCE_OK )
		{	
			rlError("Error: sceNpPartyCheckCallback failed result:0x%x\n",ret);
		}
	}
}

void rlNpParty::Shutdown()
{
	if (m_bInitialized)
	{
		int ret = sceNpPartyTerminate();
		if (ret != SCE_OK)
		{
			rlError("sceNpPartyRegisterHandler() failed. ret = 0x%x\n", ret);
		}

		ret = sceSysmoduleUnloadModule( SCE_SYSMODULE_NP_PARTY );
		if( ret != SCE_OK )
		{
			rlError( "Error unloading party module: 0x%x\n", ret );
		}
	}
}

void rlNpParty::PartyRoomEventHandler(SceNpPartyRoomEventType eventType, const void* data, void* userdata)
{
	if (data == NULL || userdata == NULL)
		return;

	rlNpParty* npParty = (rlNpParty*)userdata;

	switch(eventType)
	{
	case SCE_NP_PARTY_ROOM_EVENT_JOINED:
		{
			SceNpPartyMemberList* pPartyMemberList = (SceNpPartyMemberList*)data;
			rlDebug3("This player has joined a party. Number of players:%d",pPartyMemberList->memberNum);
			npParty->SetPartyMemberList(pPartyMemberList);
			break;
		}
	case SCE_NP_PARTY_ROOM_EVENT_MEMBER_JOINED:
		{
			SceNpPartyMemberInfo *pNewPartyMemberInfo = (SceNpPartyMemberInfo*)data;
			rlDebug3("A player has joined the party.[%s]",pNewPartyMemberInfo->npId.handle.data);
			npParty->AddPartyMemberToMemberList(pNewPartyMemberInfo);
			break;
		}
	case SCE_NP_PARTY_ROOM_EVENT_MEMBER_LEFT:
		{
			SceNpPartyMemberInfo *pLeavingPartyMemberInfo = (SceNpPartyMemberInfo*)data;
			rlDebug3("A player has left the party.[%s]", pLeavingPartyMemberInfo->npId.handle.data);
			npParty->RemovePartyMemberFromMemberList(pLeavingPartyMemberInfo->memberId);
			break;
		}
	case SCE_NP_PARTY_ROOM_EVENT_LEFT:
		{
			OUTPUT_ONLY(SceNpPartyRoomLeftInfo* pLeftInfo = (SceNpPartyRoomLeftInfo *)data;)
			rlDebug3("This player has left a party. Reason[%d]", pLeftInfo->reason);
			npParty->ClearPartyMemberList();
			break;
		}
	case SCE_NP_PARTY_ROOM_EVENT_CREATE_RESPONSE:
		{
			SceNpPartyCreateResponseInfo *pCreateResponseInfo =  (SceNpPartyCreateResponseInfo*)data;
			switch( pCreateResponseInfo->status )
			{
			case SCE_NP_PARTY_ROOM_CREATE_RESPONSE_STATUS_OK: 
				rlDebug3("Party created successfully.");
				break;
			case SCE_NP_PARTY_ROOM_CREATE_RESPONSE_STATUS_CANCELLED: 
				rlDebug3("Party creation cancelled.");
				break;
			default:
				break;
			}
			break;
		}
	default:
		rlWarning("Unhandled party event.");
		break;
	}
}
void rlNpParty::PartyVoiceEventHandler(const SceNpPartyMemberVoiceInfo* memberVoiceInfo, void* userdata)
{
	if(memberVoiceInfo==NULL)
		return;

	rlNpParty* party = (rlNpParty*)userdata;
	party->UpdatePartyMemberVoiceState(memberVoiceInfo->memberId, memberVoiceInfo->memberVoiceState);
}

void rlNpParty::PartyBinaryMessageEventHandler(SceNpPartyBinaryMessageEvent msgEvent,const void* data, void* userdata)
{
	unsigned char binary_message[32];
	memset(&binary_message,0,sizeof(binary_message));

	switch(msgEvent)
	{
	case SCE_NP_PARTY_BINARY_MESSAGE_EVENT_READY:
		{
			SceNpPartyBinaryMessageReady *pReady = (SceNpPartyBinaryMessageReady*) data;
			
			if(pReady->result == SCE_OK)
			{
				//its safe to now call sceNpPartySendBinaryMessage
				rlDebug3("Received ready event for binary message, its safe to send binary messages");
			}
			else
			{
				rlDebug3("Received ready event for binary message, an error occurred: 0x%08x", pReady->result);
			}
			break;
		}
	case SCE_NP_PARTY_BINARY_MESSAGE_EVENT_DATA:
		{
			SceNpPartyBinaryMessageInfo *pBinaryData = (SceNpPartyBinaryMessageInfo*)data;
			if(pBinaryData->dataSize < sizeof(binary_message))
			{
				memcpy(binary_message,pBinaryData->data,pBinaryData->dataSize);
				binary_message[pBinaryData->dataSize] = '\0';
				rlDebug3("Binary Message received (%s) from member %d for local member:%d.\n", binary_message, pBinaryData->memberId, pBinaryData->destinationMemberId);
			}
			break;
		}
	default:
		rlWarning("Un-handled Binary Message Event:%d. Ignoring!\n",msgEvent);
		break;
	}
}

int rlNpParty::RetrieveAllPartyInfo()
{
	SYS_CS_SYNC(m_PartyInfoCs);

	int ret = sceNpPartyGetState(&m_PartyState);
	// If in party
	if(m_PartyState != SCE_NP_PARTY_STATE_NOT_IN_PARTY)
	{
		SceNpPartyMemberList memberList;
		ret = sceNpPartyGetMembers(&memberList);
		if (ret == SCE_OK)
		{
			m_PartyMembers.clear();

			for (int i = 0; i < memberList.memberNum; i++)
			{
				rlNpPartyMemberInfo info;
				ret = sceNpPartyGetMemberInfo(memberList.memberIds[i], &(info.m_SceNpPartyMemberInfo));
				if(ret!=SCE_OK)
				{
					rlError("sceNpPartyGetMemberInfo returned an error: 0x%x\n",ret);
				}

				rlSceNpOnlineId onlineId;
				onlineId.Reset(info.m_SceNpPartyMemberInfo.npId.handle);

				info.m_GamerHandle.ResetNp(g_rlNp.GetEnvironment(), RL_INVALID_NP_ACCOUNT_ID, &onlineId);
				m_PartyMembers.Push(info);
			}
		}
		else
		{
			rlError("sceNpPartyGetMembers() failed. ret = 0x%x\n", ret);
		}
	}

	return ret;
}

int rlNpParty::SetPartyMemberList(SceNpPartyMemberList *pPartyMemberList)
{
	SYS_CS_SYNC(m_PartyInfoCs);

	if(pPartyMemberList==NULL)
	{
		return SCE_NP_PARTY_ERROR_INVALID_ARGUMENT;
	}

	if(m_bInitialized)
	{
		m_PartyMembers.clear();
		int ret = sceNpPartyGetState(&m_PartyState);

		if (ret == SCE_OK)
		{
			for(int i = 0; i < pPartyMemberList->memberNum; i++)
			{
				rlNpPartyMemberInfo info;

				//for each party member, get their corresponding member info. The member info will help with handling voice events.
				int ret = sceNpPartyGetMemberInfo(pPartyMemberList->memberIds[i], &(info.m_SceNpPartyMemberInfo));
				if(ret != SCE_OK)
				{
					rlDebug3("sceNpPartyGetMemberInfo returned an error: 0x%x\n",ret);
				}

				rlSceNpOnlineId onlineId;
				onlineId.Reset(info.m_SceNpPartyMemberInfo.npId.handle);

				info.m_GamerHandle.ResetNp(g_rlNp.GetEnvironment(), RL_INVALID_NP_ACCOUNT_ID, &onlineId);
				rlDebug3("Player %s id:%d is in this party.\n", info.m_SceNpPartyMemberInfo.npId.handle.data, info.m_SceNpPartyMemberInfo.memberId);
				m_PartyMembers.Push(info);
			}

			return ret;
		}
		else
		{
			rlDebug3( "sceNpPartyGetState failed result:0x%x\n", ret );
		}
	}

	return SCE_NP_PARTY_ERROR_NOT_INITIALIZED;
}

void rlNpParty::AddPartyMemberToMemberList(SceNpPartyMemberInfo* pInfo)
{
	SYS_CS_SYNC(m_PartyInfoCs);

	rtry
	{
		rverify(pInfo, catchall, );
		rverify(pInfo->memberId > 0, catchall, rlError("Invalid Member Id: %u", pInfo->memberId));
		rverify(m_PartyMembers.GetCount() < m_PartyMembers.GetMaxCount(), catchall, );
		rverify(m_bInitialized, catchall, );

		for(int i = 0; i < m_PartyMembers.GetCount(); i++)
		{
			rverify(pInfo->memberId != m_PartyMembers[i].m_SceNpPartyMemberInfo.memberId, catchall, rlError("Adding member who is already in party list"));
		}

		rlNpPartyMemberInfo info;
		info.m_SceNpPartyMemberInfo = *pInfo;
		m_PartyMembers.Push(info);
	}
	rcatchall
	{

	}
}

void rlNpParty::RemovePartyMemberFromMemberList(const SceNpPartyRoomMemberId MemberId)
{
	SYS_CS_SYNC(m_PartyInfoCs);

	rtry
	{
		rverify(m_bInitialized, catchall, rlError("rlNpParty not initialized"));
		rverify(MemberId != 0, catchall, rlError("No member ID."));
		rverify(m_PartyMembers.GetCount(), catchall, rlError("Party member removed with party count of 0 -- something bad happened here."));

		for(int i = 0; i < m_PartyMembers.GetCount(); i++)
		{
			// found party member with matching npid
			if(m_PartyMembers[i].m_SceNpPartyMemberInfo.memberId == MemberId)
			{
				m_PartyMembers.Delete(i);
				break;
			}
		}
	}
	rcatchall
	{

	}
}

int rlNpParty::ClearPartyMemberList()
{
	SYS_CS_SYNC(m_PartyInfoCs);

	m_PartyMembers.clear();
	m_PartyState = SCE_NP_PARTY_STATE_NOT_IN_PARTY;

	return SCE_OK;
}

void rlNpParty::UpdatePartyMemberVoiceState(SceNpPartyRoomMemberId memberId, SceNpPartyMemberVoiceState state)
{
	SYS_CS_SYNC(m_PartyInfoCs);

	for(int i=0;i < m_PartyMembers.GetCount();i++)
	{
		// search for user by member id, update voice state
		if(m_PartyMembers[i].m_SceNpPartyMemberInfo.memberId == memberId)
		{
			m_PartyMembers[i].m_VoiceState = state;
			break;
		}
	}
}

bool rlNpParty::SendGameInvites()
{
	// TODO: Send binary messages to users that would be interpreted as an invite.
	//	sceNpPartySendBinaryMessage hook up an NpMatching2 call?
	return true;
}

bool rlNpParty::IsInParty()
{
	return m_PartyState != SCE_NP_PARTY_STATE_NOT_IN_PARTY;
}

int rlNpParty::GetPartyMembers(rlNpPartyMemberInfo* members)
{
	SYS_CS_SYNC(m_PartyInfoCs);

	for (int i = 0; i < m_PartyMembers.GetCount(); i++)
	{
		members[i] = m_PartyMembers[i];
	}

	return m_PartyMembers.GetCount();
}

bool rlNpParty::CreateParty(const int localGamerIndex)
{
	SYS_CS_SYNC(m_PartyInfoCs);

	int ret = SCE_OK;
	SceNpPartyCreateParams createParams; 
	memset( &createParams, 0, sizeof( createParams ) );
	createParams.privateParty = false;
	createParams.inviteParams.inviteeList = NULL;
             
	ret = sceNpPartyCreate( g_rlNp.GetUserServiceId(localGamerIndex), &createParams );
	if(ret != SCE_OK)
	{
		rlDebug3( "sceNpPartyCreate failed result:0x%x\n", ret );
		return false;
	}

	rlDebug3( "Attempting to create party. Waiting for callback");
	return true;
}

bool rlNpParty::ShowInvitationList(const int localGamerIndex)
{
	SYS_CS_SYNC(m_PartyInfoCs);

	rtry
	{
		SceNpPartyInviteListParams inviteListParams;
		memset( &inviteListParams, 0, sizeof( inviteListParams ) );

		bool bLocalUserFound = false;
		int localPartyIndex = 0;

		const SceNpOnlineId& onlineId = g_rlNp.GetNpOnlineId(localGamerIndex).AsSceNpOnlineId();
		for(int i = 0; i < m_PartyMembers.GetCount(); i++)
		{
			if (sceNpCmpOnlineId(&m_PartyMembers[i].m_SceNpPartyMemberInfo.npId.handle, &onlineId) == SCE_OK)
			{
				localPartyIndex = i;
				bLocalUserFound = true;
				break;
			}
		}

		rverify(bLocalUserFound, catchall, rlError("Local user not found in party"));

		int ret = sceNpPartyShowInvitationList( m_PartyMembers[localPartyIndex].m_SceNpPartyMemberInfo.memberId , &inviteListParams );
		rverify(ret == SCE_OK, catchall, rlError("sceNpPartyShowInvitationList failed result:0x%x\n", ret));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

} // namespace rage

#endif // RSG_ORIBS
