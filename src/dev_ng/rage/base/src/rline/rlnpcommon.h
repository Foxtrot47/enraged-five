// 
// rline/rlnpcommon.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNPCOMMON_H
#define RLINE_RLNPCOMMON_H

#if RSG_NP

#include "rl.h"
#include "rldiag.h"
#include "atl/delegate.h" 
#include "data/autoid.h"
#include "system/criticalsection.h"
#include "system/memops.h"

#include <user_service.h>

#define RL_MAX_WEB_API_INVITES (100)

namespace rage
{

class rlSessionInfo;
class rlFriend;

enum eNpLicenseArea
{   
    RLNP_LICENSE_AREA_INVALID = -1,

    RLNP_LICENSE_AREA_SCEJ,                 //Japan
    RLNP_LICENSE_AREA_SCEA,                 //Americas
    RLNP_LICENSE_AREA_SCEE,                 //Europe
    RLNP_LICENSE_AREA_SCEH,                 //Hong Kong
    RLNP_LICENSE_AREA_SCEK,                 //Korea
    RLNP_LICENSE_AREA_SCEC,                 //China
    RLNP_LICENSE_AREA_OTHER,                //Other

    RLNP_LICENSE_AREA_NUM_AREAS
};

enum eNpSendMsgResult
{
    RLNP_SENDMSG_SUCCESS = 0,               //Msg was sent
    RLNP_SENDMSG_UNKNOWN_RECIPIENT,         //Could not find player being sent to
    RLNP_SENDMSG_TOO_SOON,                  //It was too soon to send another message
    RLNP_SENDMSG_ERROR                      //Other error attempting to send message
};

enum rlNpOnlineStatus
{
	RLNP_STATUS_ONLINE,
	RLNP_STATUS_OFFLINE,
};

enum rlNpSignedOfflineReason
{
	RLNP_SIGNED_OFFLINE_INVALID = -1,
	RLNP_SIGNED_OFFLINE_STATE_SIGNED_OUT,
	RLNP_SIGNED_OFFLINE_STATE_UNKNOWN,
	RLNP_SIGNED_OFFLINE_UNREACHABLE,
	RLNP_SIGNED_OFFLINE_HARDWARE_LINK_DISCONNECTED,
	RLNP_SIGNED_OFFLINE_DUP_LOGIN,
#if __BANK
	RLNP_SIGNED_OFFLINE_DEBUG,
#endif
};

enum class rlNpGameIntentType
{
	Undefined,
	JoinSession,
	LaunchActivity,
	LaunchMultiplayerActivity,
};

enum class rlNpGameIntentMemberType
{
	Undefined,
	Player,
	Spectator
};

enum eNpEventType
{
    RLNP_EVENT_ONLINE_STATUS_CHANGED = 0,
	RLNP_EVENT_ACTING_USER_CHANGED,
	RLNP_EVENT_PRESENCE_STATUS_CHANGED,
	RLNP_EVENT_FRIEND_PRESENCE_UPDATE,
	RLNP_EVENT_FRIEND_LIST_UPDATE,
    RLNP_EVENT_MSG_RECEIVED,
    RLNP_EVENT_INVITE_RECEIVED,
    RLNP_EVENT_INVITE_ACCEPTED,
    RLNP_EVENT_INVITE_ACCEPTED_WHILE_OFFLINE,
	RLNP_EVENT_JOIN_SESSION,
    RLNP_EVENT_SET_PRESENCE_DATA,
#if RSG_ORBIS
	RLNP_EVENT_NP_UNAVAILABLE,
	RLNP_EVENT_USER_SERVICE_STATUS_CHANGED,
	RLNP_EVENT_PLAYSTATION_PLUS_INVALID,
	RLNP_EVENT_PLAYSTATION_PLUS_UPDATE,
	RLNP_EVENT_INVITE_REJECTED_NP_UNAVAILABLE,
	RLNP_EVENT_PLAY_TOGETHER_HOST,
#endif
	RLNP_EVENT_BLOCKLIST_UPDATED,
	RLNP_EVENT_BLOCKLIST_RETRIEVED,
};

#define RLNP_EVENT_COMMON_DECL( name )\
    static unsigned EVENT_ID() { return name::GetAutoId(); }\
    virtual unsigned GetId() const { return name::GetAutoId(); }

#define RLNP_EVENT_DECL( name, id )\
    AUTOID_DECL_ID( name, rage::rlNpEvent, id )\
    RLNP_EVENT_COMMON_DECL( name )

typedef atDelegator<void (class rlNp*, const class rlNpEvent*)> rlNpEventDelegator;
typedef rlNpEventDelegator::Delegate rlNpEventDelegate;

class rlNpEvent
{
public:
    AUTOID_DECL_ROOT(rlNpEvent);
    RLNP_EVENT_COMMON_DECL(rlNpEvent);

    rlNpEvent() {}
    virtual ~rlNpEvent() {}
};

class rlNpEventOnlineStatusChanged : public rlNpEvent
{
public:

	RLNP_EVENT_DECL(rlNpEventOnlineStatusChanged, RLNP_EVENT_ONLINE_STATUS_CHANGED);

	rlNpEventOnlineStatusChanged(int localGamerIndex, const rlNpOnlineStatus onlineStatus, const rlNpSignedOfflineReason reason)
		: m_LocalGamerIndex(localGamerIndex)
		, m_OnlineStatus(onlineStatus)
		, m_Reason(reason)
	{
	}

	bool IsSignedOnline() const
	{
		return (m_OnlineStatus == RLNP_STATUS_ONLINE);
	}

	bool IsOfflineDueToSignOut() const
	{
		return (m_Reason == RLNP_SIGNED_OFFLINE_STATE_SIGNED_OUT);
	}

	bool KickedByDuplicateLogin() const
	{
		return (m_Reason == RLNP_SIGNED_OFFLINE_DUP_LOGIN);
	}

	int m_LocalGamerIndex;
	rlNpOnlineStatus m_OnlineStatus;
	rlNpSignedOfflineReason m_Reason;
};

class rlNpEventActingUserChanged : public rlNpEvent
{
public:

	RLNP_EVENT_DECL(rlNpEventActingUserChanged, RLNP_EVENT_ACTING_USER_CHANGED);

	rlNpEventActingUserChanged(int localGamerIndex)
		: m_LocalGamerIndex(localGamerIndex)
	{
	}

	int m_LocalGamerIndex;
};

class rlNpEventPresenceStatusChanged : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventPresenceStatusChanged, RLNP_EVENT_PRESENCE_STATUS_CHANGED);

	enum Flags
	{
		FLAG_ONLINE = 0x01,
	};

	rlNpEventPresenceStatusChanged(int localGamerIndex, const unsigned flags)
		: m_LocalGamerIndex(localGamerIndex)
		, m_Flags(flags)
	{
	}

	bool IsOnline() const
	{
		return (0 != (m_Flags & FLAG_ONLINE));
	}

	int m_LocalGamerIndex;
	unsigned m_Flags;
};

class rlNpEventFriendPresenceUpdate : public rlNpEvent
{
public:
    RLNP_EVENT_DECL(rlNpEventFriendPresenceUpdate, RLNP_EVENT_FRIEND_PRESENCE_UPDATE);

    rlNpEventFriendPresenceUpdate(const int localGamerIndex, rlFriend* f)
    : m_Friend(f)
    {
    }

    rlFriend* m_Friend;
	int m_LocalGamerIndex;
};

class rlNpEventFriendListUpdate : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventFriendListUpdate, RLNP_EVENT_FRIEND_LIST_UPDATE);

	rlNpEventFriendListUpdate(const int localGamerIndex)
		: m_LocalGamerIndex(localGamerIndex)
	{
		
	}

	int m_LocalGamerIndex;
};

class rlNpEventMsgReceived : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventMsgReceived, RLNP_EVENT_MSG_RECEIVED);

	rlNpEventMsgReceived(const rlSceNpOnlineId* fromOnlineId, const void *data, const unsigned size)
		: m_FromOnlineId(fromOnlineId), m_Data(data) , m_Size(size)
	{
	}

	const rlSceNpOnlineId* m_FromOnlineId;
	const void *m_Data;
	unsigned m_Size;
};

class rlNpEventInviteReceived : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventInviteReceived, RLNP_EVENT_INVITE_RECEIVED);

	rlNpEventInviteReceived(
		const rlSceNpAccountId fromAccountId,
		const rlSceNpOnlineId* fromOnlineId,
		const rlSessionInfo* sessionInfo,
		const char* salutation)
		: m_FromAccountId(fromAccountId)
		, m_FromOnlineId(fromOnlineId)
		, m_SessionInfo(sessionInfo)
		, m_Salutation(salutation)
	{
	}

	const rlSceNpAccountId m_FromAccountId;
	const rlSceNpOnlineId* m_FromOnlineId;
	const rlSessionInfo* m_SessionInfo;
	const char* m_Salutation;
};

class rlNpEventInviteAccepted : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventInviteAccepted, RLNP_EVENT_INVITE_ACCEPTED);

	rlNpEventInviteAccepted(
		const rlSceNpAccountId from,
		const rlSceNpOnlineId* onlineId,
		const rlSessionInfo* sessionInfo,
		const char* salutation)
		: m_From(from)
		, m_OnlineId(onlineId)
		, m_SessionInfo(sessionInfo)
		, m_Salutation(salutation)
	{
	}

	const rlSceNpAccountId m_From;
	const rlSceNpOnlineId* m_OnlineId;
	const rlSessionInfo* m_SessionInfo;
	const char* m_Salutation;
};

class rlNpEventInviteAcceptedWhileOffline : public rlNpEvent
{
public:
    RLNP_EVENT_DECL(rlNpEventInviteAcceptedWhileOffline,
                    RLNP_EVENT_INVITE_ACCEPTED_WHILE_OFFLINE);

    rlNpEventInviteAcceptedWhileOffline()
    {
    }
};

class rlNpEventJoinSession : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventJoinSession, RLNP_EVENT_JOIN_SESSION);

	rlNpEventJoinSession(const rlSessionInfo* sessionInfo, const rlSceNpAccountId& target, const rlSceNpOnlineId* targetOnlineId, const rlNpGameIntentMemberType memberType)
		: m_SessionInfo(sessionInfo)
		, m_Target(target)
		, m_TargetOnlineId(targetOnlineId)
		, m_MemberType(memberType)
	{
	}

	const rlSessionInfo* m_SessionInfo;
	const rlSceNpAccountId m_Target;
	const rlSceNpOnlineId* m_TargetOnlineId;
	rlNpGameIntentMemberType m_MemberType;
};

class rlNpEventSetPresenceData : public rlNpEvent
{
public:
    RLNP_EVENT_DECL(rlNpEventSetPresenceData, RLNP_EVENT_SET_PRESENCE_DATA);

    rlNpEventSetPresenceData(const bool succeeded,
                             const void* presenceBlob,
                             const unsigned presenceDataSize,
                             const char* presenceStatusStr)
    : m_Succeeded(succeeded)
    , m_PresenceBlob(presenceBlob)
    , m_PresenceDataSize(presenceDataSize)
    , m_PresenceStatusStr(presenceStatusStr)
    {
    }
    
    bool m_Succeeded;
    const void* m_PresenceBlob;
    unsigned m_PresenceDataSize;
    const char* m_PresenceStatusStr;
};

#if RSG_ORBIS
class rlNpEventNpUnavailable : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventNpUnavailable, RLNP_EVENT_NP_UNAVAILABLE);

	rlNpEventNpUnavailable(int localGamerIndex)
		: m_LocalGamerIndex(localGamerIndex)
	{
	}

	int m_LocalGamerIndex;
};

class rlNpEventUserServiceStatusChanged : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventUserServiceStatusChanged, RLNP_EVENT_USER_SERVICE_STATUS_CHANGED);

	rlNpEventUserServiceStatusChanged(SceUserServiceUserId serviceId, int gamerIndex)
		: m_UserServiceId(serviceId)
		, m_LocalGamerIndex(gamerIndex)
	{
	}

	int m_LocalGamerIndex;
	SceUserServiceUserId m_UserServiceId;
};

class rlNpEventPlayStationPlusInvalid : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventPlayStationPlusInvalid, RLNP_EVENT_PLAYSTATION_PLUS_INVALID);

	rlNpEventPlayStationPlusInvalid(SceUserServiceUserId serviceId, int gamerIndex, SceNpPlusEventType eventType)
		: m_UserServiceId(serviceId)
		, m_LocalGamerIndex(gamerIndex)
		, m_EventType(eventType)
	{
	}

	int m_LocalGamerIndex;
	SceUserServiceUserId m_UserServiceId;
	SceNpPlusEventType m_EventType;
};

class rlNpEventPlayStationPlusUpdate : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventPlayStationPlusUpdate, RLNP_EVENT_PLAYSTATION_PLUS_UPDATE);

	rlNpEventPlayStationPlusUpdate(int gamerIndex, const bool hadActivePromotion)
		: m_LocalGamerIndex(gamerIndex)
        , m_HadActivePromotion(hadActivePromotion)
	{
	}

	int m_LocalGamerIndex;
    bool m_HadActivePromotion;
};

class rlNpEventInviteRejectedNpUnavailable : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventInviteRejectedNpUnavailable, RLNP_EVENT_INVITE_REJECTED_NP_UNAVAILABLE);

	rlNpEventInviteRejectedNpUnavailable()
	{
	}
};

class rlNpEventPlayTogetherHost : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventPlayTogetherHost, RLNP_EVENT_PLAY_TOGETHER_HOST);

	rlNpEventPlayTogetherHost()
		: m_UserId(0)
		, m_nInvitees(0)
	{

	}

	rlNpEventPlayTogetherHost(SceUserServiceUserId userId, unsigned nInvitees, rlSceNpOnlineId* invitees)
		: m_UserId(userId)
		, m_nInvitees(nInvitees)
	{
		if(m_nInvitees > RL_MAX_PLAY_TOGETHER_GROUP)
		{
			m_nInvitees = RL_MAX_PLAY_TOGETHER_GROUP;
		}
		for(unsigned i = 0; i < nInvitees; i++)
		{
			m_Invitees[i] = invitees[i];
		}
	}

	SceUserServiceUserId m_UserId;
	unsigned m_nInvitees;
	rlSceNpOnlineId m_Invitees[RL_MAX_PLAY_TOGETHER_GROUP];
};

class rlNpEventBlocklistUpdated : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventBlocklistUpdated, RLNP_EVENT_BLOCKLIST_UPDATED);

	rlNpEventBlocklistUpdated(const int localGamerIndex)
		: m_LocalGamerIndex(localGamerIndex)
	{
	}

	int m_LocalGamerIndex;
};

class rlNpEventBlocklistRetrieved : public rlNpEvent
{
public:
	RLNP_EVENT_DECL(rlNpEventBlocklistRetrieved, RLNP_EVENT_BLOCKLIST_RETRIEVED);

	rlNpEventBlocklistRetrieved() {}
};

#endif

template<typename T, unsigned QUEUE_SIZE>
class rlNpEventQueue
{
public:

    rlNpEventQueue()
    {
        this->Clear();
    }

    void Clear()
    {
        SYS_CS_SYNC(m_Cs);

        m_Count = m_Next = m_Free = 0;
    }

    bool QueueEvent(const T& event)
    {
        SYS_CS_SYNC(m_Cs);

        const bool success = rlVerify(m_Count < QUEUE_SIZE);
        if(success)
        {
            sysMemCpy(&m_Queue[m_Free], &event, sizeof(event));
            ++m_Count;
            ++m_Free;
            if(m_Free >= QUEUE_SIZE)
            {
                m_Free = 0;
            }
        }

        return success;
    }

    bool NextEvent(T* event)
    {
        SYS_CS_SYNC(m_Cs);

        rlAssert(m_Count >= 0);
        rlAssert(m_Count <= QUEUE_SIZE);

        bool success = false;
        if(m_Count > 0)
        {
            *event = m_Queue[m_Next];
            --m_Count;
            ++m_Next;
            if(m_Next >= QUEUE_SIZE)
            {
                m_Next = 0;
            }

            success = true;
        }

        return success;
    };

    unsigned GetCount() const
    {
        return (unsigned) m_Count;
    }

private:

    sysCriticalSectionToken m_Cs;
    T m_Queue[QUEUE_SIZE];
    int m_Count;
    int m_Next;
    int m_Free;
};

}

#endif // RSG_NP

#endif  //RLINE_NPCOMMON_H
