// 
// rline/rlnpactivityfeed.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS
#ifndef RLINE_RLNPACTIVITYFEED_H
#define RLINE_RLNPACTIVITYFEED_H

#include "rline/rldiag.h"
#include "rline/rl.h"
#include <np.h>

#include "rline/rlnpwebapi.h"

namespace rage
{
	class rlNpActivityFeed
	{
	public:
		rlNpActivityFeed();
		~rlNpActivityFeed();

		bool Init();
		void Shutdown();

		void Start(int key);
		void PostCaptions(sysLanguage language, const char* caption, const char* condensedCaption);

		void AddSubStringToCaption(sysLanguage language, const char* subString);
		void ConfirmCaptionSubStringComplete();
		void AddSubValueToCaptionFloat(float subValue, int noofDecimalPlaces);
		void AddSubValueToCaptionInt(int subValue);

		void PostSmallImageURL(const char* imageURL, const char* aspectRatio);
		void PostLargeImageURL(const char* imageURL);
		void PostThumbnailImageURL(const char* imageURL);
		void PostVideoURL(const char* imageURL);

		void PostCustomCaption(const char* customCaptionString);

		void PostActionURL(const char* urlString);
		void PostActionStart(const char* commandLineString);
		void PostActionStore(const char* productCodeString);

		void AppendActionURL(const char* urlString);
		void AppendActionStart(const char* commandLineString);

		void PostTagForActionURL(sysLanguage language, const char* labelString);
		void PostTagForActionStart(sysLanguage language, const char* labelString);
		void PostTagForActionStore(sysLanguage language, const char* labelString);

		void PostThumbnailForActionURL(const char* thumbnail);
		void PostThumbnailForActionStart(const char* thumbnail);
		void PostThumbnailForActionStore(const char* thumbnail);

		void PostCurrentMessage();

		void StartOnlinePlayedWith(const char* gameMode);
		void AddOnlinePlayedWith(const rlGamerHandle& player);
		void PostOnlinePlayedWith();

		void GetActivityFeedVideoData();

	private:
		rlNpActivityFeedStory m_activityFeedStory;
		rlNpActivityFeedPlayedWithDetails m_activityFeedOnlineDetails;
		rlNpGetActivityFeedVideoDataDetails m_getActivityFeedDetails;

		netStatus m_getActivityFeedVideoDataStatus;
	};

} // namespace rage

#endif // RLINE_RLNPACTIVITYFEED_H
#endif // RSG_ORBIS