// 
// rline/rlros.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROS_H
#define RLINE_RLROS_H

#include "rlroscommon.h"
#include "atl/delegate.h"
#include "diag/seh.h"

struct WOLFSSL_CTX;

namespace rage
{

class rlRosPresenceEventListener;

class rlRos
{
    typedef atDelegator<void (const rlRosEvent&)> Delegator;

    //Listens for presence events.
    //Friended by rlRos so it can force a ticket refresh.
    friend class rlRosPresenceEventListener;
public:

    //Maximum number of rlServiceHost entries we can have.
    static const unsigned MAX_SERVICE_HOSTS = 32;

    //Maximum number of rlSslService entries we can have
    static const unsigned MAX_SSL_SERVICES = 32;

    //PURPOSE
    //  Delegate for handling events.
    //  See rlroscommon.h for event types.
    typedef Delegator::Delegate Delegate;

    //PURPOSE
    //  Add or remove a delegate that will be called with event notifications.
    static void AddDelegate(Delegate* dlgt);
    static void RemoveDelegate(Delegate* dlgt);

    //PURPOSE
    //  Init/Update/Shutdown.  Users do not need to call these; they are
    //  called automatically by rlInit, rlUpdate, and rlShutdown.
    static bool InitClass();
    static void ShutdownClass();
    static void UpdateClass();

    static bool IsInitialized();

#if RL_FORCE_STAGE_ENVIRONMENT
	//PURPOSE
	//  Recreates the ROS ssl context - needed if we need to switch environment after initialization
	static void RecreateRosSslContext();
#endif

    //PURPOSE
    //  Returns the current login status for the specified local gamer.
    static rlRosLoginStatus GetLoginStatus(const int localGamerIndex);

    //PURPOSE
    //  Returns the time until the next login attempt.  This will only be valid
    //  when GetLoginStatus() == RLROS_LOGIN_STATUS_COUNTING_DOWN.  In all other
    //  states, -1 will be returned.
    static int GetMsUntilNextLoginAttempt(const int localGamerIndex);

    //PURPOSE
    //  Return true if the gamer is considered online to ROS.
    //  This is equivalent to (GetLoginStatus() == RLROS_LOGIN_STATUS_ONLINE).
    static bool IsOnline(const int localGamerIndex);

	//PURPOSE
	//  Return true if any local gamer is considered online to ROS.
	static bool IsAnyOnline();

    //PURPOSE
    //  Return true if the gamer is considered online to ROS and is a member of SC.
    static bool IsScMember(const int localGamerIndex);

    //PURPOSE
    //  Returns the current credentials for specified local gamer.
    //  Callers should check the credentials IsValid() before using.
    //  If the gamer is not online, the credentials are guaranteed
    //  to be invalid.
    static const rlRosCredentials& GetCredentials(const int localGamerIndex);

    //PURPOSE
    //  Returns the credentials of the first local gamer with valid credentials.
    //  If none of them are valid, then the credentials returned will not be valid.
    static const rlRosCredentials& GetFirstValidCredentials();

    //PURPOSE
    //  Returns the credentials of the first local gamer with valid credentials,
    //  and stores their local gamer index in the out parameter. If none of them
    //  are valid, then the credentials returned will not be valid and the local
    //  gamer index will be set to NO_LOCAL_GAMER
	static const rlRosCredentials& GetFirstValidCredentials(int* localGamerIndex);

#if RLROS_USE_SERVICE_ACCESS_TOKENS
	//PURPOSE
	//	Returns the service access info for specified local gamer.  
	//	If there isn't a valid token, an invalid info object is returned
	static const rlRosServiceAccessInfo& GetServiceAccessInfo(const int localGamerIndex);

	//PUPOSE
	// Requests the service access info for this gamer
	static bool RequestServiceAccessInfo(const int localGamerIndex);

	//PURPOSE
	//	Get the status for the request for this gamer index.
	static bool IsServiceAccessRequestPending(const int localGamerIndex, int& retryCount, int& retryTimeMs);

	//PURPOSE
	//	Invalidate and request a new token.
	NOTFINAL_ONLY(static void InvalidateOrRequestToken(const int localGamerIndex);)

	//PURPOSE
	//	Shutdown current service access request.
	static void ClearServiceAccessRequest( );
#endif

    //PURPOSE
    //  Sets or clears the ROS credentials for specified local gamer.
    //  This is provided for clients that directly call ROS web services
    //  (like from the blade UI) for login and account creation.
    //  For example, the blade UI may call the CreateAccount service 
    //  and return the response to game code, which then constructs an
    //  rlRosCredentials object which it passes to this method.
    //PARAMS
    //  localGamerIndex     Gamer to set/clear credentials for.
    //  cred                Credentials to set, or NULL to clear.
    //IMPORTANT!
    //  There is currently only one valid use of this from title code, 
    //  and that is to set the credentials obtained from the blade UI.  
    //  If you find another use case for this, let the ROS team know,
    //  or your title will likely fail ROS certification.
    static bool SetCredentials(const int localGamerIndex, const rlRosCredentials* cred);

	//PURPOSE
	//  Returns true if the gamer at the given index has the privilege.
	//PARAMS
	//  localGamerIndex - Index of local gamer for whom to check privileges
	//  privilegeId     - Id of privilege to check.
	//NOTES
	//  Forceinline to make it harder for hackers to patch the function (See
	// url:bugstar:1997858; this is exactly what they've done).
	__forceinline static bool HasPrivilege(const int localGamerIndex,
							const rlRosPrivilegeId privilegeId);

	//PURPOSE
	//  Returns the default privilege for a given privilege ID.
	//PARAMS
	//  privilegeId     - Id of privilege to check.
	//NOTES
	static bool GetDefaultPrivilege(const rlRosPrivilegeId privilegeId);

#if __BANK
	//PURPOSE
	//  Change the value of a privilege for a gamer.
	//PARAMS
	//  localGamerIndex - Index of local gamer for whom to check privileges
	//  privilegeId     - Id of privilege to check.
	//  value           - value to set.
	static bool SetPrivilege(const int localGamerIndex,
							 const rlRosPrivilegeId privilegeId,
							 const bool value);
#endif 

    //PURPOSE
    //  Returns the geolocation info for the local host.
    static bool GetGeoLocInfo(rlRosGeoLocInfo* geoLocInfo);

	//PURPOSE
	//  Returns the presence info for the local host.
	static bool GetPresenceInfo(rlRosPresenceInfo* presenceInfo);

    //PURPOSE
    //  Sets the global service hosts table.  We use this to map service
    //  URLs to host names.
    static void SetServiceHosts(rlRosServiceHost* serviceHosts,
                                const unsigned numServiceHosts);

    //PURPOSE
    //  Retrieves the service host mapping for the given URL.
    //PARAMS
    //  url         - URL to match.
    //  hostnameBuf - Destination buffer
    //  sizeofBuf   - Size of destination buffer
    //NOTES
    //  Matching is done from the end of the URL.
    //  For example, the URL "http://foo.bar.com/path/to/service/ends/here"
    //  will match a service host with a service like "ends/here".
    static const char* GetServiceHost(const char* url,
                                    char* hostnameBuf,
                                    const unsigned sizeofBuf);

    template<int SIZE>
    static const char* GetServiceHost(const char* url,
                                    char (&hostnameBuf)[SIZE])
    {
        return GetServiceHost(url, hostnameBuf, SIZE);
    }

    //PURPOSE
    //  Sets the global ssl service list. We use this to map
    //  service URLs that should be executed behind SSL for security
    //  reasons
    static void SetSslServices(rlRosSslService* sslServices, const unsigned numSslServices);

    //PURPOSE
    //  Determines whether or not a given URL should be forced
    //  to execute behind SSL using our currently configured
    //  SSL service list
    static bool IsSslService(const char* url);

    //PURPOSE
    //  Sets the global security flags that should be disabled.
    //  We mainly use this as a killswitch to disable these
    //  from create ticket if they're causing problems (create ticket
    //  doesn't use any).
    static void SetDisabledSecurityFlags(const rlRosSecurityFlags disabledSecurityFlags);

    //PURPOSE
    //  Removes any security flags from the supplied flags that
    //  have been disabled.
    static void RemoveDisabledSecurityFlags(rlRosSecurityFlags& securityFlags);

    //PURPOSE
    //  Returns the Ros-specific SSL context, which mainly overrides
    //  memory allocation and certificate verification
    static WOLFSSL_CTX* GetRosSslContext();

	//PURPOSE
	//  Returns the pinned Ros certificate authority to verify our
	//  Ros server certificates against. 
	static const char* GetRosCertificateAuthority();

#if __BANK
    //PURPOSE
    //  BANK only access to credentials for testing purposes. 
    static void Bank_RenewCredentials(const int localGamerIndex);
    static void Bank_InvalidateCredentials(const int localGamerIndex);
#endif

	//PURPOSE
	//  Dispatches an event to delegates.
	static void DispatchEvent(rlRosEvent& e);

	//Forces a ticket refresh
	static void RenewCredentials(const int localGamerIndex);

    //PURPOSE
    //  Central handling for common RosResult error codes returned from
    //  our endpoints (e.g. duplicate login, invalid auth ticket, etc...)
    static void ProcessCommonErrors(const int localGamerIndex, const rlRosResult& result);

#if RSG_ORBIS && !__FINAL
    //PURPOSE
    //  Whether we should use Np3 (instead of Np4)
    static bool UseCreateTicketNp3();
#endif

private:
    friend class rlRosClient;
    friend class rlRosCredentialsChangingTask;

    static void UpdateGeolocation(bool isAnyOnline, bool wentOffline);
	static void UpdatePresenceInfo(bool isAnyOnline, bool wentOffline);

    //PURPOSE
    //  Initializes our ssl context to be used with ros requests
    static bool InitRosSslContext();

	OUTPUT_ONLY(static void DumpWolfSslCapabilities();)

    static Delegator sm_Delegator;
};

__forceinline bool
rlRos::HasPrivilege(const int localGamerIndex,
					const rlRosPrivilegeId privilegeId)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
			catchall,
			rlError("Invalid local gamer index: %d", localGamerIndex));

		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);

		if(creds.IsValid())
		{
			return creds.HasPrivilege(privilegeId);
		}
		else
		{
			//Return default 'offline' value
			return GetDefaultPrivilege(privilegeId);
		}
	}
	rcatchall
	{
		return false;
	}
}

} //namespace rage

#endif
