// 
// rline/rlrosmanager.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlros.h"
#include "data/aes.h"
#include "diag/seh.h"
#include "rlroscommon.h"
#include "rlrosclient.h"
#include "rlroscredtasks.h"
#include "rlrosserviceaccesstokenstore.h"
#include "rline/rlpresence.h"
#include "rline/rltitleid.h"
#include "net/nethardware.h"
#include "net/http.h"
#include "net/net.h"
#include "net/resolver.h"
#include "net/netaddress.h"
#include "net/tcp.h"
#include "net/time.h"
#include "string/stringutil.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "system/memory.h"
#include "atl/array.h"
#include "atl/string.h"
#include "wolfssl/ssl.h"

#if !__NO_OUTPUT
#include "wolfssl/wolfcrypt/cpuid.h"
#endif

#if RSG_SCE // WolfSSL needs this (random.c)
#pragma comment(lib,"SceSysmodule_stub_weak")
#endif

// wolfSSL static libs:
// the debug and beta builds enable wolfSSL debugging defines
// so we can debug SSL issues. x64 Debug and Beta libs use
// different MSVC runtimes to match the game exe's runtime.
// Release libs disable the debugging defines.
// See "3rdparty\wolfssl\user_settings.h".
#if RSG_DEV
#if !__OPTIMIZED

// debug libs
#if RSG_DURANGO
#pragma comment(lib, "libwolfssld_durango.lib")
#elif RSG_ORBIS
#pragma comment(lib, "libwolfssld_orbis.a")
#elif RSG_PC
#if RSG_TOOL
#pragma comment(lib, "libwolfssld_pc_tool.lib")
#elif RSG_RSC
#pragma comment(lib, "libwolfssl_pc.lib")
#else
#pragma comment(lib, "libwolfssld_pc.lib")
#endif
#endif

#else

// beta libs
#if RSG_DURANGO
#pragma comment(lib, "libwolfsslb_durango.lib")
#elif RSG_ORBIS
#pragma comment(lib, "libwolfsslb_orbis.a")
#elif RSG_PC
#if RSG_TOOL
#pragma comment(lib, "libwolfsslb_pc_tool.lib")
#else
#pragma comment(lib, "libwolfsslb_pc.lib")
#endif
#endif

#endif
#else

// release libs
#if RSG_DURANGO
#pragma comment(lib, "libwolfssl_durango.lib")
#elif RSG_ORBIS
#pragma comment(lib, "libwolfssl_orbis.a")
#elif RSG_PC
#if RSG_TOOL
#pragma comment(lib, "libwolfssl_pc_tool.lib")
#else
#pragma comment(lib, "libwolfssl_pc.lib")
#endif
#endif

#endif

#if RSG_PC
#if !defined(STRICT)
#define STRICT 1
#endif
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(pop)
#endif

namespace rage
{

PARAM(rlservicehosts, "JSON array of host/endpoint pairs:  [{\"h\":\"dev.telemetry.rockstargames.com\",\"e\":\"*/telemetry.asmx/submitcompressed\"}]");
PARAM(rlsslservices, "Comma-delimited list of services that should use ssl: \"*/profilestats.asmx/*,\"*/leaderboards.asmx/*\"");
PARAM(rldisabledsecurityflags, "Security flags to disable (0 for none or 65535 to disable all");
PARAM(rlsslnocert, "If present, the server's certificate won't be verified, and SSLv23 will be used instead of tlsv1.2");
PARAM(rldisablessl, "If present, SSL will be disabled by default");
PARAM(rlenablesslbydefault, "If present, SSL will be enabled by default - temporary command line until all services are available via ssl");

#if RSG_ORBIS
PARAM(rlRosUseNp3, "Use rlRosCreateTicketNp3Task instead of rlRosCreateTicketNp4Task (will disable use of account Ids)");
#endif

#undef __rage_channel
#define __rage_channel rline_ros

extern const rlTitleId* g_rlTitleId;
extern sysMemAllocator* g_rlAllocator;

///////////////////////////////////////////////////////////////////////////////
// rlRos
///////////////////////////////////////////////////////////////////////////////
static rlRosClient s_Clients[RL_MAX_LOCAL_GAMERS];
static bool s_RlRosInitialized = false;
static netTimeStep s_TimeStep;
rlRos::Delegator rlRos::sm_Delegator;

static rlRosGeoLocInfo s_GeoLocInfo;
static netRetryTimer s_GeoLocRetryTimer;
static netStatus s_GeoLocStatus;
enum GeoLocState
{
    GEOLOCSTATE_WAIT_FOR_ONLINE,
    GEOLOCSTATE_GET_GEOLOC_INFO,
    GEOLOCSTATE_GETTING_GEOLOC_INFO,
    GEOLOCSTATE_HAVE_GEOLOC_INFO,
};

static GeoLocState s_GeoLocState = GEOLOCSTATE_WAIT_FOR_ONLINE;

static rlRosPresenceInfo s_PresenceInfo;
static netRetryTimer s_PresenceInfoRetryTimer;
static netStatus s_PresenceInfoStatus;
enum PresenceInfoState
{
	PRES_STATE_WAIT_FOR_ONLINE,
	PRES_STATE_GET_PRESENCE_INFO,
	PRES_STATE_GETTING_PRESENCE_INFO,
	PRES_STATE_HAVE_PRESENCE_INFO,
};

static PresenceInfoState s_PresenceInfoState = PRES_STATE_WAIT_FOR_ONLINE;

static const bool s_CredentialDefaults[] = 
{
    true,   // RLROS_PRIVILEGEID_NONE                
    true,   // RLROS_PRIVILEGEID_CREATE_TICKET       
    true,   // RLROS_PRIVILEGEID_PROFILE_STATS_WRITE 
    true,   // RLROS_PRIVILEGEID_MULTIPLAYER         
    true,   // RLROS_PRIVILEGEID_LEADERBOARD_WRITE   
    true,   // RLROS_PRIVILEGEID_CLOUD_STORAGE_READ  
    true,   // RLROS_PRIVILEGEID_CLOUD_STORAGE_WRITE 
    true,   // RLROS_PRIVILEGEID_BANNED              
    true,   // RLROS_PRIVILEGEID_CLAN                
    true,   // RLROS_PRIVILEGEID_PRIVATE_MESSAGING   
    true,   // RLROS_PRIVILEGEID_SOCIAL_CLUB         
    true,   // RLROS_PRIVILEGEID_PRESENCE_WRITE      
    false,  // RLROS_PRIVILEGEID_DEVELOPER           
    true,   // RLROS_PRIVILEGEID_HTTP_REQUEST_LOGGING
    true,   // RLROS_PRIVILEGEID_UGCWRITE				
    true,   // RLROS_PRIVILEGEID_PURCHASEVC			
    true,   // RLROS_PRIVILEGEID_TRANSFERVC			
    true,   // RLROS_PRIVILEGEID_CANBET              
    true,   // RLROS_PRIVILEGEID_CREATORS            
    true,   // RLROS_PRIVILEGEID_CLOUD_TUNABLES      
	false,  // RLROS_PRIVILEGEID_CHEATER_POOL   
	true,   // RLROS_PRIVILEGEID_COMMENTWRITE   
	true,   // RLROS_PRIVILEGEID_CANUSELOTTERY
	false,  // RLROS_PRIVILEGEID_ALLOW_MEMBER_REDIRECT
	false,  // RLROS_PRIVILEGEID_PLAYED_LAST_GEN
	false,  // RLROS_PRIVILEGEID_UNLOCK_SPECIAL_EDITION
	false,	// RLROS_PRIVILEGEID_CONTENT_CREATOR
	false,	// RLROS_PRIVILEGEID_CONDUCTOR
};
CompileTimeAssert(COUNTOF(s_CredentialDefaults) == RLROS_NUM_PRIVILEGEID);

//True if any local gamers are online
static bool s_IsAnyOnline = false;


#if RLROS_USE_SERVICE_ACCESS_TOKENS
static rlRosServiceAccessTokenStore s_ServiceTokenStore;
#endif



//Listens for presence events.
//Friended by rlRos so it can force a ticket refresh.
class rlRosPresenceEventListener
{
public:
    static void OnPresenceEvent(const rlPresenceEvent* evt)
    {
        if(PRESENCE_EVENT_SC_MESSAGE == evt->GetId())
        {
            const rlScPresenceMessage& msg = evt->m_ScMessage->m_Message;
            if(msg.IsA<rlScPresenceMessageSocialClubMembershipChanged>())
            {
                rlRos::RenewCredentials(evt->m_ScMessage->m_GamerInfo.GetLocalIndex());
            }
            else if(msg.IsA<rlScPresenceRenewTicket>())
            {
                rlDebug("Renewing ticket for gamer: %d", evt->m_ScMessage->m_GamerInfo.GetLocalIndex());
                rlRos::RenewCredentials(evt->m_ScMessage->m_GamerInfo.GetLocalIndex());
            }
            else if(msg.IsA<rlScPresenceInvalidateTicket>())
            {
                rlDebug("Invalidating ticket for gamer: %d", evt->m_ScMessage->m_GamerInfo.GetLocalIndex());
                rlRos::SetCredentials(evt->m_ScMessage->m_GamerInfo.GetLocalIndex(), NULL);
            }
        }
    }
};

static rlPresence::Delegate s_RlRosPresenceDlgt(&rlRosPresenceEventListener::OnPresenceEvent);

void 
rlRos::AddDelegate(Delegate* dlgt)
{
    sm_Delegator.AddDelegate(dlgt);
}

void 
rlRos::RemoveDelegate(Delegate* dlgt)
{
    sm_Delegator.RemoveDelegate(dlgt);
}

bool
rlRos::InitClass()
{
    if(rlVerifyf(!s_RlRosInitialized, "Not initialized"))
    {
        bool success = true;

        s_TimeStep.Init(sysTimer::GetSystemMsTime());

        for(int i = 0; i < COUNTOF(s_Clients); i++)
        {
            if(!s_Clients[i].Init(i))
            {
                rlError("Failed to init client for local gamer %d", i);
                success = false;
                break;
            }
        }
		
        s_GeoLocInfo.Clear();
        s_GeoLocRetryTimer.InitSeconds(60, 3*60);
        s_GeoLocRetryTimer.ForceRetry();
        s_GeoLocState = GEOLOCSTATE_WAIT_FOR_ONLINE;

		s_PresenceInfo.Clear();
		s_PresenceInfoRetryTimer.InitSeconds(60, 3*60);
		s_PresenceInfoRetryTimer.ForceRetry();
		s_PresenceInfoState = PRES_STATE_WAIT_FOR_ONLINE;

        if (!InitRosSslContext())
        {
            success = false;
        }

#if RLROS_USE_SERVICE_ACCESS_TOKENS
		if(!s_ServiceTokenStore.Init())
		{
			success = false;
		}
#endif

        s_IsAnyOnline = false;

        s_RlRosInitialized = true;

        if(!success)
        {
            rlRos::ShutdownClass();
        }
    }

    return s_RlRosInitialized;
}

void 
rlRos::ShutdownClass()
{
    if(s_RlRosInitialized)
    {
        sm_Delegator.Clear();

        for(int i = 0; i < COUNTOF(s_Clients); ++i)
        {
            s_Clients[i].Shutdown();
        }

        s_GeoLocInfo.Clear();
        s_GeoLocState = GEOLOCSTATE_WAIT_FOR_ONLINE;

		s_PresenceInfo.Clear();
		s_PresenceInfoState = PRES_STATE_WAIT_FOR_ONLINE;

        if(s_RlRosPresenceDlgt.IsRegistered() && rlPresence::IsInitialized())
        {
            rlPresence::RemoveDelegate(&s_RlRosPresenceDlgt);
        }

#if RLROS_USE_SERVICE_ACCESS_TOKENS
		s_ServiceTokenStore.Shutdown();
#endif

        s_IsAnyOnline = false;

        AES::ReleaseCloudAes();
        s_RlRosInitialized = false;
    }
}

void 
rlRos::UpdateClass()
{
    rlAssert(s_RlRosInitialized);

    s_TimeStep.SetTime(sysTimer::GetSystemMsTime());

    //Make sure our presence event listener is registered.
    //We can't do this in InitClass() b/c we don't know if rlPresence
    //has been initialized yet.
    if(!s_RlRosPresenceDlgt.IsRegistered() && rlPresence::IsInitialized())
    {
        rlPresence::AddDelegate(&s_RlRosPresenceDlgt);
    }

    for(int i = 0; i < COUNTOF(s_Clients); ++i)
    {
        s_Clients[i].Update(s_TimeStep.GetTimeStep());
    }

	const bool wasAnyOnline = s_IsAnyOnline;
	bool isAnyOnline = IsAnyOnline();
	const bool wentOffline = wasAnyOnline && !isAnyOnline;

	s_IsAnyOnline = isAnyOnline;
	
#if !__RGSC_DLL
	UpdateGeolocation(isAnyOnline, wentOffline);
#endif

#if SC_PRESENCE
	UpdatePresenceInfo(isAnyOnline, wentOffline);
#endif

#if RLROS_USE_SERVICE_ACCESS_TOKENS
	s_ServiceTokenStore.Update(s_TimeStep.GetTimeStep());
#endif

}

bool
rlRos::IsInitialized()
{
    return s_RlRosInitialized;
}

#if RL_FORCE_STAGE_ENVIRONMENT
// only needed for stage-environment force
void rlRos::RecreateRosSslContext()
{
	if(!s_RlRosInitialized)
	{
		return;
	}

	InitRosSslContext();
}
#endif

rlRosLoginStatus 
rlRos::GetLoginStatus(const int localGamerIndex)
{
	//@@: range RLROS_GETLOGINSTATUS {
	if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return s_Clients[localGamerIndex].GetLoginStatus();
	}
	return RLROS_LOGIN_STATUS_INVALID;
	//@@: } RLROS_GETLOGINSTATUS

}

int 
rlRos::GetMsUntilNextLoginAttempt(const int localGamerIndex)
{
    if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        return s_Clients[localGamerIndex].GetMsUntilNextLoginAttempt();
    }
    return -1;
}

bool
rlRos::IsOnline(const int localGamerIndex)
{
	//@@: range RLROS_ISONLINE {
	return GetLoginStatus(localGamerIndex) == RLROS_LOGIN_STATUS_ONLINE;
	//@@: } RLROS_ISONLINE

}

bool
rlRos::IsAnyOnline()
{
	for(int i = 0; i < COUNTOF(s_Clients); ++i)
	{
		if(rlRos::IsOnline(i))
		{
			return true;
		}
	}

	return false;
}

bool
rlRos::IsScMember(const int localGamerIndex)
{
    return IsOnline(localGamerIndex)
            && s_Clients[localGamerIndex].GetCredentials().IsScMember();
}

const rlRosCredentials&
rlRos::GetCredentials(const int localGamerIndex)
{
	//@@: location RLROS_GETCREDENTIALS
    (void)RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex);
    return s_Clients[localGamerIndex].GetCredentials();
}

const rlRosCredentials&
rlRos::GetFirstValidCredentials()
{
    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        const rlRosCredentials& cred = s_Clients[i].GetCredentials();
        if (cred.IsValid()) return cred;
    }

    return s_Clients[0].GetCredentials();
}

const rlRosCredentials& 
rlRos::GetFirstValidCredentials(int* localGamerIndex)
{
    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        const rlRosCredentials& cred = s_Clients[i].GetCredentials();
        if (cred.IsValid())
        {
            if (localGamerIndex)
            {
                *localGamerIndex = i;
            }
            return cred;
        }
    }

    if (localGamerIndex)
    {
        *localGamerIndex = -1;
    }
    return s_Clients[0].GetCredentials();
}

bool
rlRos::SetCredentials(const int localGamerIndex, const rlRosCredentials* cred)
{
    if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        return s_Clients[localGamerIndex].SetCredentials(cred);
    }
    return false;
}

bool
rlRos::GetDefaultPrivilege(const rlRosPrivilegeId privilegeId)
{
	return s_CredentialDefaults[privilegeId];
}

#if __BANK
bool
rlRos::SetPrivilege(const int localGamerIndex,
					const rlRosPrivilegeId privilegeId,
					const bool value)
{
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
			catchall,
			rlError("Invalid local gamer index: %d", localGamerIndex));

		rlRosCredentials& creds = const_cast< rlRosCredentials& >( rlRos::GetCredentials(localGamerIndex) );

		if(creds.IsValid())
		{
			return creds.SetPrivilege(privilegeId, value);
		}
	}
	rcatchall
	{
		return false;
	}

	return false;
}
#endif

bool
rlRos::GetGeoLocInfo(rlRosGeoLocInfo* geoLocInfo)
{
    if(GEOLOCSTATE_HAVE_GEOLOC_INFO == s_GeoLocState)
    {
        *geoLocInfo = s_GeoLocInfo;
        return true;
    }

    return false;
}

bool
rlRos::GetPresenceInfo(rlRosPresenceInfo* presenceInfo)
{
    if(PRES_STATE_HAVE_PRESENCE_INFO == s_PresenceInfoState)
    {
        *presenceInfo = s_PresenceInfo;
        return true;
    }

    return false;
}

static unsigned s_rlRosNumServiceHosts = 0;
static rlRosServiceHost m_rlRosServiceHosts[rlRos::MAX_SERVICE_HOSTS];

void
rlRos::SetServiceHosts(rlRosServiceHost* serviceHosts,
                        const unsigned numServiceHosts)
{
    rlAssert(numServiceHosts <= MAX_SERVICE_HOSTS);

    for(unsigned i = 0; i < MAX_SERVICE_HOSTS && i < numServiceHosts; ++i)
    {
        m_rlRosServiceHosts[i] = serviceHosts[i];
    }

    s_rlRosNumServiceHosts = Min(numServiceHosts, MAX_SERVICE_HOSTS);
}

const char*
rlRos::GetServiceHost(const char* url,
                    char* hostnameBuf,
                    const unsigned sizeofBuf)
{
    const char* cmdLine;
    RsonReader rr;
    if(PARAM_rlservicehosts.Get(cmdLine)
        && rlVerifyf(rr.Init(cmdLine, 0, (int)strlen(cmdLine)), "Invalid JSON: '%s'", cmdLine))
    {
        RsonReader rrhn, rrep;

        for(bool b = rr.GetFirstMember(&rr); b; b = rr.GetNextSibling(&rr))
        {
            if(rlVerifyf(rr.GetMember("e", &rrep), "No endpoint in '%s'", cmdLine)
                && rlVerifyf(rr.GetMember("h", &rrhn), "No hostname in '%s'", cmdLine)) 
            {
                char endpoint[512];
                if(rrep.AsString(endpoint)
                    && 0 == StringWildcardCompare(endpoint, url, true)
                    && rlVerify(rrhn.AsString(hostnameBuf, sizeofBuf)))
                {
                    return hostnameBuf;
                }
            }
        }
    }

    for(unsigned i = 0; i < s_rlRosNumServiceHosts; ++i)
    {
        if(0 == StringWildcardCompare(m_rlRosServiceHosts[i].Endpoint, url, true))
        {
            return safecpy(hostnameBuf, m_rlRosServiceHosts[i].Host, sizeofBuf);
        }
    }

    return NULL;
}

static unsigned s_rlRosNumSslServices = 0;
static rlRosSslService s_rlRosSslServices[rlRos::MAX_SSL_SERVICES];

void
rlRos::SetSslServices(rlRosSslService* sslServices, const unsigned numSslServices)
{
    rlAssert(numSslServices <= MAX_SSL_SERVICES);

    for (unsigned i = 0; i < MAX_SSL_SERVICES && i < numSslServices; ++i)
    {
        s_rlRosSslServices[i] = sslServices[i];
    }

    s_rlRosNumSslServices = Min(numSslServices, MAX_SSL_SERVICES);
}

bool
rlRos::IsSslService(const char* url)
{
    const char* cmdLine;
    if (PARAM_rlsslservices.Get(cmdLine))
    {
        atString sslServicesCsv(cmdLine);

        atArray<atString> sslServices;
        sslServicesCsv.Split(sslServices, ',');

        for (atArray<atString>::const_iterator it = sslServices.begin(); it != sslServices.end(); ++it)
        {
            if (0 == StringWildcardCompare(*it, url, true))
            {
                return true;
            }
        }
    }
    
    for (unsigned i = 0; i < s_rlRosNumSslServices; ++i)
    {
        if (0 == StringWildcardCompare(s_rlRosSslServices[i].Endpoint, url, true))
        {
            return true;
        }
    }

    return !PARAM_rldisablessl.Get() && PARAM_rlenablesslbydefault.Get();
}

static rlRosSecurityFlags s_rlRosDisabledSecurityFlags = RLROS_SECURITY_NONE;

void
rlRos::SetDisabledSecurityFlags(const rlRosSecurityFlags disabledSecurityFlags)
{
    s_rlRosDisabledSecurityFlags = disabledSecurityFlags;
}

void
rlRos::RemoveDisabledSecurityFlags(rlRosSecurityFlags& securityFlags)
{
    int cmdLineDisabledFlags;
    if (PARAM_rldisabledsecurityflags.Get(cmdLineDisabledFlags))
    {
        securityFlags = static_cast<rlRosSecurityFlags>(securityFlags & ~cmdLineDisabledFlags);
    }
    else
    {
        securityFlags = static_cast<rlRosSecurityFlags>(securityFlags & ~s_rlRosDisabledSecurityFlags);
    }
}

#if __BANK
void 
rlRos::Bank_RenewCredentials(const int localGamerIndex)
{
    if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        s_Clients[localGamerIndex].RenewCredentials();
    }
}

void 
rlRos::Bank_InvalidateCredentials(const int localGamerIndex)
{
    if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        s_Clients[localGamerIndex].SetCredentials(NULL);
    }
}
#endif

//private:

void
rlRos::UpdateGeolocation(bool isAnyOnline, bool wentOffline)
{
    s_GeoLocRetryTimer.Update();

    if(wentOffline)
    {
        if(s_GeoLocStatus.Pending())
        {
            rlGetTaskManager()->CancelTask(&s_GeoLocStatus);
        }

        s_GeoLocState = GEOLOCSTATE_WAIT_FOR_ONLINE;
        s_GeoLocInfo.Clear();
    }

    netIpAddress myPublicIp;

    switch(s_GeoLocState)
    {
    case GEOLOCSTATE_WAIT_FOR_ONLINE:
        if(isAnyOnline)
        {
            s_GeoLocRetryTimer.ForceRetry();
            s_GeoLocState = GEOLOCSTATE_GET_GEOLOC_INFO;
        }
        break;
    case GEOLOCSTATE_GET_GEOLOC_INFO:
        if(s_GeoLocRetryTimer.IsTimeToRetry()
            && netHardware::GetPublicIpAddress(&myPublicIp))
        {
            rlDebug("Fetching geolocation info...");

            s_GeoLocRetryTimer.Restart();

            rlRosGetGeolocationInfoTask* task = NULL;
            rtry
            {
                rverify(rlGetTaskManager()->CreateTask(&task),
                        catchall,
                        rlError("Error creating geoloc task"));

                rverify(rlTaskBase::Configure(task, myPublicIp, &s_GeoLocInfo, &s_GeoLocStatus),
                        catchall,
                        rlError("Error configuring geoloc task"));

                rverify(rlGetTaskManager()->AddParallelTask(task),
                        catchall,
                        rlError("Error scheduling geoloc task"));

                s_GeoLocState = GEOLOCSTATE_GETTING_GEOLOC_INFO;
            }
            rcatchall
            {
                if(task)
                {
                    rlGetTaskManager()->DestroyTask(task);
                }
            }
        }
        break;
    case GEOLOCSTATE_GETTING_GEOLOC_INFO:
        if(s_GeoLocStatus.Succeeded())
        {
            s_GeoLocState = GEOLOCSTATE_HAVE_GEOLOC_INFO;
            rlRosEventRetrievedGeoLocInfo e(s_GeoLocInfo);
            DispatchEvent(e);
        }
        else if(!s_GeoLocStatus.Pending())
        {
            s_GeoLocState = GEOLOCSTATE_GET_GEOLOC_INFO;
            rlDebug("Failed to fetch geoloc info, retrying in %d seconds",
                    s_GeoLocRetryTimer.GetSecondsUntilRetry());
        }
        break;
    case GEOLOCSTATE_HAVE_GEOLOC_INFO:
        break;
    }
}

void
rlRos::UpdatePresenceInfo(bool isAnyOnline, bool wentOffline)
{
#if __RGSC_DLL
	if (!GetRgscConcreteInstance()->IsPresenceSupported())
	{
		// don't connect to presence in the game launcher or on LA noire
		return;
	}
#endif

	if (isAnyOnline)
	{
		int localGamerIndex;
		GetFirstValidCredentials(&localGamerIndex);

		if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && !rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE))
		{
			isAnyOnline = false;
		}
	}
	

    s_PresenceInfoRetryTimer.Update();

    if(wentOffline)
    {
        if(s_PresenceInfoStatus.Pending())
        {
            rlGetTaskManager()->CancelTask(&s_PresenceInfoStatus);
        }

        s_PresenceInfoState = PRES_STATE_WAIT_FOR_ONLINE;
        s_PresenceInfo.Clear();
    }

    switch(s_PresenceInfoState)
    {
    case PRES_STATE_WAIT_FOR_ONLINE:
        if(isAnyOnline)
        {
            s_PresenceInfoRetryTimer.ForceRetry();
            s_PresenceInfoState = PRES_STATE_GET_PRESENCE_INFO;
        }
        break;
    case PRES_STATE_GET_PRESENCE_INFO:
        if(s_PresenceInfoRetryTimer.IsTimeToRetry())
        {
            rlDebug("Fetching presence server info...");

            s_PresenceInfoRetryTimer.Restart();

            rlRosGetPresenceServerInfoTask* task = NULL;
            rtry
            {
                rverify(rlGetTaskManager()->CreateTask(&task),
                        catchall,
                        rlError("Error creating presence server info task"));

                rverify(rlTaskBase::Configure(task, &s_PresenceInfo, &s_PresenceInfoStatus),
                        catchall,
                        rlError("Error configuring presence server info task"));

                rverify(rlGetTaskManager()->AddParallelTask(task),
                        catchall,
                        rlError("Error scheduling presence server info task"));

                s_PresenceInfoState = PRES_STATE_GETTING_PRESENCE_INFO;
            }
            rcatchall
            {
                if(task)
                {
                    rlGetTaskManager()->DestroyTask(task);
                }
            }
        }
        break;
    case PRES_STATE_GETTING_PRESENCE_INFO:
        if(s_PresenceInfoStatus.Succeeded())
        {
            s_PresenceInfoState = PRES_STATE_HAVE_PRESENCE_INFO;
            rlRosEventRetrievedPresenceServerInfo e(s_PresenceInfo);
            DispatchEvent(e);
        }
        else if(!s_PresenceInfoStatus.Pending())
        {
            s_PresenceInfoState = PRES_STATE_GET_PRESENCE_INFO;
            rlDebug("Failed to fetch presence server info, retrying in %d seconds",
                    s_PresenceInfoRetryTimer.GetSecondsUntilRetry());
        }
        break;
    case PRES_STATE_HAVE_PRESENCE_INFO:
        break;
    }
}

//private:

void 
rlRos::RenewCredentials(const int localGamerIndex)
{
    if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        return s_Clients[localGamerIndex].RenewCredentials();
    }
}

void 
rlRos::DispatchEvent(rlRosEvent& e)
{
    sm_Delegator.Dispatch(e);
}

void
rlRos::ProcessCommonErrors(const int localGamerIndex, const rlRosResult& result)
{
    //If the error was AuthenticationFailed.Ticket, then invalidate our ticket.
    if (result.IsError("AuthenticationFailed", "Ticket") && (localGamerIndex >= 0))
    {
        rlDebug("Invalidating ticket because of AuthenticationFailed.Ticket error");
        SetCredentials(localGamerIndex, NULL);
    }

    // If the error is AuthenticationFailed.DupLogin, then handle duplicate logins.
    //	1. RGSC_DLL : Fire the duplicate kick event, which is propogated to the game and forces a sign out.
    //	2.       PC : Notifies the DLL That we received a kickmessage, which then follows the flow of #1
    //	3. Consoles : Invalidate the ROS Ticket.
    if (result.IsError("AuthenticationFailed", "DupLogin"))
    {
#if __RGSC_DLL
        GetRgscConcreteInstance()->_GetProfileManager()->DuplicateSignInKick(localGamerIndex);
#elif RSG_PC
        if (g_rlPc.GetProfileManager())
        {
            g_rlPc.GetProfileManager()->OnDuplicateSignInMessageReceived(localGamerIndex);
        }
#else
        rlDebug("Invalidating ticket because of AuthenticationFailed.DupLogin error");
        SetCredentials(localGamerIndex, NULL);
#endif
    }
}

#if RSG_ORBIS && !__FINAL
bool
rlRos::UseCreateTicketNp3()
{
    return PARAM_rlRosUseNp3.Get(); 
}
#endif

#if RLROS_USE_SERVICE_ACCESS_TOKENS

const rlRosServiceAccessInfo& rlRos::GetServiceAccessInfo( const int localGamerIndex )
{
	(void)RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex);
	return s_ServiceTokenStore.GetAccessInfoForService(localGamerIndex);
}

bool rlRos::RequestServiceAccessInfo( const int localGamerIndex )
{
	if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return s_ServiceTokenStore.RequestAccessToken(localGamerIndex);
	}

	return false;
}

#if !__FINAL
void rlRos::InvalidateOrRequestToken(const int localGamerIndex)
{
	if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		s_ServiceTokenStore.InvalidateOrRequestToken(localGamerIndex);
	}
}
#endif // !__FINAL

bool rlRos::IsServiceAccessRequestPending( const int localGamerIndex, int& retryCount, int& retryTimeMs )
{
	(void)RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex);
	return s_ServiceTokenStore.IsRequestPending(localGamerIndex, retryCount, retryTimeMs);
}

void rlRos::ClearServiceAccessRequest()
{
	s_ServiceTokenStore.ClearAccessRequest();
}

#endif //RLROS_USE_SERVICE_ACCESS_TOKENS

static WOLFSSL_CTX* s_RosSslCtx = NULL;

// Our pinned CAs to verify our ros server certificates against. 
// For now this just includes the root CA that our ros.rockstargames.com
// is signed with
static const char s_RosCAs[] = 
// https://www.entrust.net/downloads/binary/entrust_2048_ca.cer
// https://www.entrust.net/downloads/binary/entrust_g2_ca.cer (new root)
// https://www.entrust.net/downloads/binary/entrust_ev_ca.cer (game server root)
"-----BEGIN CERTIFICATE-----\n"
"MIIEKjCCAxKgAwIBAgIEOGPe+DANBgkqhkiG9w0BAQUFADCBtDEUMBIGA1UEChML\n"
"RW50cnVzdC5uZXQxQDA+BgNVBAsUN3d3dy5lbnRydXN0Lm5ldC9DUFNfMjA0OCBp\n"
"bmNvcnAuIGJ5IHJlZi4gKGxpbWl0cyBsaWFiLikxJTAjBgNVBAsTHChjKSAxOTk5\n"
"IEVudHJ1c3QubmV0IExpbWl0ZWQxMzAxBgNVBAMTKkVudHJ1c3QubmV0IENlcnRp\n"
"ZmljYXRpb24gQXV0aG9yaXR5ICgyMDQ4KTAeFw05OTEyMjQxNzUwNTFaFw0yOTA3\n"
"MjQxNDE1MTJaMIG0MRQwEgYDVQQKEwtFbnRydXN0Lm5ldDFAMD4GA1UECxQ3d3d3\n"
"LmVudHJ1c3QubmV0L0NQU18yMDQ4IGluY29ycC4gYnkgcmVmLiAobGltaXRzIGxp\n"
"YWIuKTElMCMGA1UECxMcKGMpIDE5OTkgRW50cnVzdC5uZXQgTGltaXRlZDEzMDEG\n"
"A1UEAxMqRW50cnVzdC5uZXQgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkgKDIwNDgp\n"
"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArU1LqRKGsuqjIAcVFmQq\n"
"K0vRvwtKTY7tgHalZ7d4QMBzQshowNtTK91euHaYNZOLGp18EzoOH1u3Hs/lJBQe\n"
"sYGpjX24zGtLA/ECDNyrpUAkAH90lKGdCCmziAv1h3edVc3kw37XamSrhRSGlVuX\n"
"MlBvPci6Zgzj/L24ScF2iUkZ/cCovYmjZy/Gn7xxGWC4LeksyZB2ZnuU4q941mVT\n"
"XTzWnLLPKQP5L6RQstRIzgUyVYr9smRMDuSYB3Xbf9+5CFVghTAp+XtIpGmG4zU/\n"
"HoZdenoVve8AjhUiVBcAkCaTvA5JaJG/+EfTnZVCwQ5N328mz8MYIWJmQ3DW1cAH\n"
"4QIDAQABo0IwQDAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNV\n"
"HQ4EFgQUVeSB0RGAvtiJuQijMfmhJAkWuXAwDQYJKoZIhvcNAQEFBQADggEBADub\n"
"j1abMOdTmXx6eadNl9cZlZD7Bh/KM3xGY4+WZiT6QBshJ8rmcnPyT/4xmf3IDExo\n"
"U8aAghOY+rat2l098c5u9hURlIIM7j+VrxGrD9cv3h8Dj1csHsm7mhpElesYT6Yf\n"
"zX1XEC+bBAlahLVu2B064dae0Wx5XnkcFMXj0EyTO2U87d89vqbllRrDtRnDvV5b\n"
"u/8j72gZyxKTJ1wDLW8w0B62GqzeWvfRqqgnpv55gcR5mTNXuhKwqeBCbJPKVt7+\n"
"bYQLCIt+jerXmCHG8+c8eS9enNFMFY3h7CI3zJpDC5fcgJCNs2ebb0gIFVbPv/Er\n"
"fF6adulZkMV8gzURZVE=\n"
"-----END CERTIFICATE-----\n"

/*
Entrust Root Certificate Authority - G2
Cert: https://www.entrust.net/downloads/binary/entrust_g2_ca.cer
Info: https://www.entrust.com/get-support/ssl-certificate-support/root-certificate-downloads/

Valid Until: 12/7/2030
Serial Number: 4a 53 8c 28
Thumbprint: 8c f4 27 fd 79 0c 3a d1 66 06 8d e8 1e 57 ef bb 93 22 72 d4
Signing Algorithm: SHA256RSA
Key Size: 2048
Support EKU: SHA-256 SSL, Code Signing, S/MIME
Validation: OV, EV
*/
"-----BEGIN CERTIFICATE-----\n"
"MIIEPjCCAyagAwIBAgIESlOMKDANBgkqhkiG9w0BAQsFADCBvjELMAkGA1UEBhMC\n"
"VVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50\n"
"cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAyMDA5IEVudHJ1c3Qs\n"
"IEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEyMDAGA1UEAxMpRW50cnVz\n"
"dCBSb290IENlcnRpZmljYXRpb24gQXV0aG9yaXR5IC0gRzIwHhcNMDkwNzA3MTcy\n"
"NTU0WhcNMzAxMjA3MTc1NTU0WjCBvjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVu\n"
"dHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwt\n"
"dGVybXMxOTA3BgNVBAsTMChjKSAyMDA5IEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0\n"
"aG9yaXplZCB1c2Ugb25seTEyMDAGA1UEAxMpRW50cnVzdCBSb290IENlcnRpZmlj\n"
"YXRpb24gQXV0aG9yaXR5IC0gRzIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK\n"
"AoIBAQC6hLZy254Ma+KZ6TABp3bqMriVQRrJ2mFOWHLP/vaCeb9zYQYKpSfYs1/T\n"
"RU4cctZOMvJyig/3gxnQaoCAAEUesMfnmr8SVycco2gvCoe9amsOXmXzHHfV1IWN\n"
"cCG0szLni6LVhjkCsbjSR87kyUnEO6fe+1R9V77w6G7CebI6C1XiUJgWMhNcL3hW\n"
"wcKUs/Ja5CeanyTXxuzQmyWC48zCxEXFjJd6BmsqEZ+pCm5IO2/b1BEZQvePB7/1\n"
"U1+cPvQXLOZprE4yTGJ36rfo5bs0vBmLrpxR57d+tVOxMyLlbc9wPBr64ptntoP0\n"
"jaWvYkxN4FisZDQSA/i2jZRjJKRxAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAP\n"
"BgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBRqciZ60B7vfec7aVHUbI2fkBJmqzAN\n"
"BgkqhkiG9w0BAQsFAAOCAQEAeZ8dlsa2eT8ijYfThwMEYGprmi5ZiXMRrEPR9RP/\n"
"jTkrwPK9T3CMqS/qF8QLVJ7UG5aYMzyorWKiAHarWWluBh1+xLlEjZivEtRh2woZ\n"
"Rkfz6/djwUAFQKXSt/S1mja/qYh2iARVBCuch38aNzx+LaUa2NSJXsq9rD1s2G2v\n"
"1fN2D807iDginWyTmsQ9v4IbZT+mD12q/OWyFcq1rca8PdCE6OoGcrBNOTJ4vz4R\n"
"nAuknZoh8/CbCzB428Hch0P+vGOaysXCHMnHjf87ElgI5rY97HosTvuDls4MPGmH\n"
"VHOkc8KT/1EQrBVUAdj8BbGJoX90g5pJ19xOe4pIb4tF9g==\n"
"-----END CERTIFICATE-----\n"
"-----BEGIN CERTIFICATE-----\n"
"MIIEkTCCA3mgAwIBAgIERWtQVDANBgkqhkiG9w0BAQUFADCBsDELMAkGA1UEBhMC\n"
"VVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xOTA3BgNVBAsTMHd3dy5lbnRydXN0\n"
"Lm5ldC9DUFMgaXMgaW5jb3Jwb3JhdGVkIGJ5IHJlZmVyZW5jZTEfMB0GA1UECxMW\n"
"KGMpIDIwMDYgRW50cnVzdCwgSW5jLjEtMCsGA1UEAxMkRW50cnVzdCBSb290IENl\n"
"cnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTA2MTEyNzIwMjM0MloXDTI2MTEyNzIw\n"
"NTM0MlowgbAxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1FbnRydXN0LCBJbmMuMTkw\n"
"NwYDVQQLEzB3d3cuZW50cnVzdC5uZXQvQ1BTIGlzIGluY29ycG9yYXRlZCBieSBy\n"
"ZWZlcmVuY2UxHzAdBgNVBAsTFihjKSAyMDA2IEVudHJ1c3QsIEluYy4xLTArBgNV\n"
"BAMTJEVudHJ1c3QgUm9vdCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJ\n"
"KoZIhvcNAQEBBQADggEPADCCAQoCggEBALaVtkNC+sZtKm9I35RMOVcF7sN5EUFo\n"
"Nu3s/poBj6E4KPz3EEZmLk0eGrEaTsbRwJWIsMn/MYszA9u3g3s+IIRe7bJWKKf4\n"
"4LlAcTfFy0cOlypowCKVYhXbR9n10Cv/gkvJrT7eTNuQgFA/CYqEAOwwCj0Yzfv9\n"
"KlmaI5UXLEWeH25DeW0MXJj+SKfFI0dcXv1u5x609mhF0YaDW6KKjbHjKYD+JXGI\n"
"rb68j6xSlkuqUY3kEzEZ6E5Nn9uss2rVvDlUccp6en+Q3X0dgNmBu1kmwhH+5pPi\n"
"94DkZfs0Nw4pgHBNrziGLp5/V6+eF67rHMsoIV+2HNjnogQi+dPa2MsCAwEAAaOB\n"
"sDCBrTAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zArBgNVHRAEJDAi\n"
"gA8yMDA2MTEyNzIwMjM0MlqBDzIwMjYxMTI3MjA1MzQyWjAfBgNVHSMEGDAWgBRo\n"
"kORnpKZTgMeGZqTx90tD+4S9bTAdBgNVHQ4EFgQUaJDkZ6SmU4DHhmak8fdLQ/uE\n"
"vW0wHQYJKoZIhvZ9B0EABBAwDhsIVjcuMTo0LjADAgSQMA0GCSqGSIb3DQEBBQUA\n"
"A4IBAQCT1DCw1wMgKtD5Y+iRDAUgqV8ZyntyTtSx29CW+1RaGSwMCPeyvIWonX9t\n"
"O1KzKtvn1ISMY/YPyyYBkVBs9F8U4pN0wBOeMDpQ47RgxRzwIkSNcUesyBrJ6Zua\n"
"AGAT/3B+XxFNSRuzFVJ7yVTav52Vr2ua2J7p8eRDjeIRRDq/r72DQnNSi6q7pynP\n"
"9WQcCk3RvKqsnyrQ/39/2n3qse0wJcGE2jTSW3iDVuycNsMm4hH2Z0kdkquM++v/\n"
"eu6FSqdQgPCnXEqULl8FmTxSQeDNtGPPAUO6nIPcj2A781q0tHuu2guQOHXvgR1m\n"
"0vdXcDazv/wor3ElhVsT/h5/WrQ8\n"
"-----END CERTIFICATE-----\n"
// Issued by: VeriSign Class 3 Public Primary Certification Authority
// Serial Number: 70 ba e4 1d 10 d9 29 34 b6 38 ca 7b 03 cc ba bf
"-----BEGIN CERTIFICATE-----\n"
"MIICPDCCAaUCEHC65B0Q2Sk0tjjKewPMur8wDQYJKoZIhvcNAQECBQAwXzELMAkG\n"
"A1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMTcwNQYDVQQLEy5DbGFz\n"
"cyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTk2\n"
"MDEyOTAwMDAwMFoXDTI4MDgwMTIzNTk1OVowXzELMAkGA1UEBhMCVVMxFzAVBgNV\n"
"BAoTDlZlcmlTaWduLCBJbmMuMTcwNQYDVQQLEy5DbGFzcyAzIFB1YmxpYyBQcmlt\n"
"YXJ5IENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIGfMA0GCSqGSIb3DQEBAQUAA4GN\n"
"ADCBiQKBgQDJXFme8huKARS0EN8EQNvjV69qRUCPhAwL0TPZ2RHP7gJYHyX3KqhE\n"
"BarsAx94f56TuZoAqiN91qyFomNFx3InzPRMxnVx0jnvT0Lwdd8KkMaOIG+YD/is\n"
"I19wKTakyYbnsZogy1Olhec9vn2a/iRFM9x2Fe0PonFkTGUugWhFpwIDAQABMA0G\n"
"CSqGSIb3DQEBAgUAA4GBALtMEivPLCYATxQT3ab7/AoRhIzzKBxnki98tsX63/Do\n"
"lbwdj2wsqFHMc9ikwFPwTtYmwHYBV4GSXiHx0bH/59AhWM1pF+NEHJwZRDmJXNyc\n"
"AA9WjQKZ7aKQRUzkuxCkPfAyAw7xzvjoyVGM5mKf5p/AfbdynMk2OmufTqj/ZA1k\n"
"-----END CERTIFICATE-----\n"

/*
VeriSign Class 3 Primary CA - G5
Reason for adding: this is to hit akaimaized.net URLs for the SC feed.

Country = US
Organization = VeriSign, Inc.
Organizational Unit = VeriSign Trust Network
Organizational Unit = (c) 2006 VeriSign, Inc. - For authorized use only
Common Name = VeriSign Class 3 Public Primary Certification Authority - G5
Serial Number: 18 da d1 9e 26 7d e8 bb 4a 21 58 cd cc 6b 3b 4a
Operational Period: Tue, November 07, 2006 to Wed, July 16, 2036
Certificate SHA1 Fingerprint: 4e b6 d5 78 49 9b 1c cf 5f 58 1e ad 56 be 3d 9b 67 44 a5 e5
Key Size: RSA(2048Bits)
Signature Algorithm: sha1RSA
*/
"-----BEGIN CERTIFICATE-----\n"
"MIIE0zCCA7ugAwIBAgIQGNrRniZ96LtKIVjNzGs7SjANBgkqhkiG9w0BAQUFADCB\n"
"yjELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL\n"
"ExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJp\n"
"U2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxW\n"
"ZXJpU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0\n"
"aG9yaXR5IC0gRzUwHhcNMDYxMTA4MDAwMDAwWhcNMzYwNzE2MjM1OTU5WjCByjEL\n"
"MAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQLExZW\n"
"ZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJpU2ln\n"
"biwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxWZXJp\n"
"U2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9y\n"
"aXR5IC0gRzUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvJAgIKXo1\n"
"nmAMqudLO07cfLw8RRy7K+D+KQL5VwijZIUVJ/XxrcgxiV0i6CqqpkKzj/i5Vbex\n"
"t0uz/o9+B1fs70PbZmIVYc9gDaTY3vjgw2IIPVQT60nKWVSFJuUrjxuf6/WhkcIz\n"
"SdhDY2pSS9KP6HBRTdGJaXvHcPaz3BJ023tdS1bTlr8Vd6Gw9KIl8q8ckmcY5fQG\n"
"BO+QueQA5N06tRn/Arr0PO7gi+s3i+z016zy9vA9r911kTMZHRxAy3QkGSGT2RT+\n"
"rCpSx4/VBEnkjWNHiDxpg8v+R70rfk/Fla4OndTRQ8Bnc+MUCH7lP59zuDMKz10/\n"
"NIeWiu5T6CUVAgMBAAGjgbIwga8wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8E\n"
"BAMCAQYwbQYIKwYBBQUHAQwEYTBfoV2gWzBZMFcwVRYJaW1hZ2UvZ2lmMCEwHzAH\n"
"BgUrDgMCGgQUj+XTGoasjY5rw8+AatRIGCx7GS4wJRYjaHR0cDovL2xvZ28udmVy\n"
"aXNpZ24uY29tL3ZzbG9nby5naWYwHQYDVR0OBBYEFH/TZafC3ey78DAJ80M5+gKv\n"
"MzEzMA0GCSqGSIb3DQEBBQUAA4IBAQCTJEowX2LP2BqYLz3q3JktvXf2pXkiOOzE\n"
"p6B4Eq1iDkVwZMXnl2YtmAl+X6/WzChl8gGqCBpH3vn5fJJaCGkgDdk+bW48DW7Y\n"
"5gaRQBi5+MHt39tBquCWIMnNZBU4gcmU7qKEKQsTb47bDN0lAtukixlE0kF6BWlK\n"
"WE9gyn6CagsCqiUXObXbf+eEZSqVir2G3l6BFoMtEMze/aiCKm0oHw0LxOXnGiYZ\n"
"4fQRbxC1lfznQgUy286dUV4otp6F01vvpX1FQHKOtw5rDgb7MzVIcbidJ4vEZV8N\n"
"hnacRHr2lVz2XTIIM6RUthg/aFzyQkqFOFSDX9HoLPKsEdao7WNq\n"
"-----END CERTIFICATE-----\n"

/*
Entrust Root Certificate Authority - EC1
Cert: https://www.entrust.com/root-certificates/entrust_ec1_ca.cer
Info: https://www.entrust.com/get-support/ssl-certificate-support/root-certificate-downloads/
Reason for adding: requested by Dan Cooper since we might use SHA384 signed cert in the future.

Valid Until: 12/18/2037
Serial Number: 00 a6 8b 79 29 00 00 00 00 50 d0 91 f9
Thumbprint: 20 d8 06 40 df 9b 25 f5 12 25 3a 11 ea f7 59 8a eb 14 b5 47
Signing Algorithm: SHA384ECDSA
Key Size: ECC (384 Bits)
Support EKU: ECC SSL
*/
"-----BEGIN CERTIFICATE-----\n"
"MIIC+TCCAoCgAwIBAgINAKaLeSkAAAAAUNCR+TAKBggqhkjOPQQDAzCBvzELMAkG\n"
"A1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3\n"
"d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAyMDEyIEVu\n"
"dHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEzMDEGA1UEAxMq\n"
"RW50cnVzdCBSb290IENlcnRpZmljYXRpb24gQXV0aG9yaXR5IC0gRUMxMB4XDTEy\n"
"MTIxODE1MjUzNloXDTM3MTIxODE1NTUzNlowgb8xCzAJBgNVBAYTAlVTMRYwFAYD\n"
"VQQKEw1FbnRydXN0LCBJbmMuMSgwJgYDVQQLEx9TZWUgd3d3LmVudHJ1c3QubmV0\n"
"L2xlZ2FsLXRlcm1zMTkwNwYDVQQLEzAoYykgMjAxMiBFbnRydXN0LCBJbmMuIC0g\n"
"Zm9yIGF1dGhvcml6ZWQgdXNlIG9ubHkxMzAxBgNVBAMTKkVudHJ1c3QgUm9vdCBD\n"
"ZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEVDMTB2MBAGByqGSM49AgEGBSuBBAAi\n"
"A2IABIQTydC6bUF74mzQ61VfZgIaJPRbiWlH47jCffHyAsWfoPZb1YsGGYZPUxBt\n"
"ByQnoaD41UcZYUx9ypMn6nQM72+WCf5j7HBdNq1nd67JnXxVRDqiY1Ef9eNi1KlH\n"
"Bz7MIKNCMEAwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0O\n"
"BBYEFLdj5xrdjekIplWDpOBqUEFlEUJJMAoGCCqGSM49BAMDA2cAMGQCMGF52OVC\n"
"R98crlOZF7ZvHH3hvxGU0QOIdeSNiaSKd0bebWHvAvX7td/M/k7//qnmpwIwW5nX\n"
"hTcGtXsI/esni0qU+eH6p44mCOh8kmhtc9hvJqwhAriZtyZBWyVgrtBIGu4G\n"
"-----END CERTIFICATE-----\n"

/*
DigiCert Global Root CA
https://global-root-ca.chain-demos.digicert.com/info/index.html
Reason for adding: requested by Dan Cooper since we will eventually be moving everything to DigiCert.

Serial Number:
08:3B:E0:56:90:42:46:B1:A1:75:6A:C9:59:91:C7:4A

Subject:
CN=DigiCert Global Root CA
O=DigiCert Inc
OU=www.digicert.com
C=US

Validity period:
Not Before: Nov 10 00:00:00 2006 GMT
Not After : Nov 10 00:00:00 2031 GMT

Fingerprints:
MD5:	  79:E4:A9:84:0D:7D:3A:96:D7:C0:4F:E2:43:4C:89:2E
SHA1:	  A8:98:5D:3A:65:E5:E5:C4:B2:D7:D6:6D:40:C6:DD:2F:B1:9C:54:36
SHA256: 43:48:A0:E9:44:4C:78:CB:26:5E:05:8D:5E:89:44:B4:D8:4F:96:62:BD:26:DB:25:7F:89:34:A4:43:C7:01:61

Key:
Size: 2048 bit RSA
Key Usage: Digital Signature, Certificate Sign, CRL Sign
Basic Constraints: CA:TRUE

Subject Key Identifier:
03:DE:50:35:56:D1:4C:BB:66:F0:A3:E2:1B:1B:C3:97:B2:3D:D1:55

Signature Algorithm:
sha1WithRSAEncryption
*/
"-----BEGIN CERTIFICATE-----\n"
"MIIDrzCCApegAwIBAgIQCDvgVpBCRrGhdWrJWZHHSjANBgkqhkiG9w0BAQUFADBh\n"
"MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\n"
"d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBD\n"
"QTAeFw0wNjExMTAwMDAwMDBaFw0zMTExMTAwMDAwMDBaMGExCzAJBgNVBAYTAlVT\n"
"MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j\n"
"b20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IENBMIIBIjANBgkqhkiG\n"
"9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jvhEXLeqKTTo1eqUKKPC3eQyaKl7hLOllsB\n"
"CSDMAZOnTjC3U/dDxGkAV53ijSLdhwZAAIEJzs4bg7/fzTtxRuLWZscFs3YnFo97\n"
"nh6Vfe63SKMI2tavegw5BmV/Sl0fvBf4q77uKNd0f3p4mVmFaG5cIzJLv07A6Fpt\n"
"43C/dxC//AH2hdmoRBBYMql1GNXRor5H4idq9Joz+EkIYIvUX7Q6hL+hqkpMfT7P\n"
"T19sdl6gSzeRntwi5m3OFBqOasv+zbMUZBfHWymeMr/y7vrTC0LUq7dBMtoM1O/4\n"
"gdW7jVg/tRvoSSiicNoxBN33shbyTApOB6jtSj1etX+jkMOvJwIDAQABo2MwYTAO\n"
"BgNVHQ8BAf8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUA95QNVbR\n"
"TLtm8KPiGxvDl7I90VUwHwYDVR0jBBgwFoAUA95QNVbRTLtm8KPiGxvDl7I90VUw\n"
"DQYJKoZIhvcNAQEFBQADggEBAMucN6pIExIK+t1EnE9SsPTfrgT1eXkIoyQY/Esr\n"
"hMAtudXH/vTBH1jLuG2cenTnmCmrEbXjcKChzUyImZOMkXDiqw8cvpOp/2PV5Adg\n"
"06O/nVsJ8dWO41P0jmP6P6fbtGbfYmbW0W5BjfIttep3Sp+dWOIrWcBAI+0tKIJF\n"
"PnlUkiaY4IBIqDfv8NZ5YBberOgOzW6sRBc4L0na4UU+Krk2U886UAb3LujEV0ls\n"
"YSEY1QSteDwsOoBrp+uvFRTp2InBuThs4pFsiv9kuXclVzDAGySj4dzp30d8tbQk\n"
"CAUw7C29C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=\n"
"-----END CERTIFICATE-----\n"

/*
DigiCert Global Root G3
https://global-root-g3.chain-demos.digicert.com/info/index.html
Reason for adding: requested by Dan Cooper since we will eventually be moving everything to DigiCert.

Serial Number:
055556BCF25EA43535C3A40FD5AB4572

Subject:
CN=DigiCert Global Root G3
O=DigiCert Inc
OU=www.digicert.com
C=US

Validity period:
Not Before: 01-Aug-2013
Not After : 18-Jan-2038

Fingerprints:
MD5:	7E:04:DE:89:6A:3E:66:6D:00:E6:87:D3:3F:FA:D9:3B:E8:3D:34:9E
SHA1:	F5:5D:A4:50:A5:FB:28:7E:1E:0F:0D:CC:96:57:56:CA

Key:
Size: secp384r1 (384 bit Elliptic Curve key)
Key Usage: Digital Signature, Certificate Sign, CRL Sign
Basic Constraints: CA:TRUE

Subject Key Identifier:
B3:DB:48:A4:F9:A1:C5:D8:AE:36:41:CC:11:63:69:62:29:BC:4B:C6

Signature Algorithm:
ecdsa-with-SHA384
*/
"-----BEGIN CERTIFICATE-----\n"
"MIICPzCCAcWgAwIBAgIQBVVWvPJepDU1w6QP1atFcjAKBggqhkjOPQQDAzBhMQsw\n"
"CQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cu\n"
"ZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBHMzAe\n"
"Fw0xMzA4MDExMjAwMDBaFw0zODAxMTUxMjAwMDBaMGExCzAJBgNVBAYTAlVTMRUw\n"
"EwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20x\n"
"IDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IEczMHYwEAYHKoZIzj0CAQYF\n"
"K4EEACIDYgAE3afZu4q4C/sLfyHS8L6+c/MzXRq8NOrexpu80JX28MzQC7phW1FG\n"
"fp4tn+6OYwwX7Adw9c+ELkCDnOg/QW07rdOkFFk2eJ0DQ+4QE2xy3q6Ip6FrtUPO\n"
"Z9wj/wMco+I+o0IwQDAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBhjAd\n"
"BgNVHQ4EFgQUs9tIpPmhxdiuNkHMEWNpYim8S8YwCgYIKoZIzj0EAwMDaAAwZQIx\n"
"AK288mw/EkrRLTnDCgmXc/SINoyIJ7vmiI1Qhadj+Z4y3maTD/HMsQmP3Wyr+mt/\n"
"oAIwOWZbwmSNuJ5Q3KjVSaLtx9zRSX8XAbjIho9OjIgrqJqpisXRAL34VOKa5Vt8\n"
"sycX\n"
"-----END CERTIFICATE-----\n"

/*
DigiCert Trusted Root G4
https://trusted-root-g4.chain-demos.digicert.com/info/index.html
Reason for adding: requested by Dan Cooper since we will eventually be moving everything to DigiCert.

Serial Number:
059B1B579E8E2132E23907BDA777755C

Subject:
CN=DigiCert Trusted Root G4
O=DigiCert Inc
OU=www.digicert.com
C=US

Validity period:
Not Before: 01-Aug-2013
Not After : 15-Jan-2038

Fingerprints:
MD5:	78:F2:FC:AA:60:1F:2F:B4:EB:C9:37:BA:53:2E:75:49
SHA1:	DD:FB:16:CD:49:31:C9:73:A2:03:7D:3F:C8:3A:4D:7D:77:5D:05:E4

Key:
Size: 4096 bit RSA
Key Usage: Digital Signature, Certificate Sign, CRL Sign
Basic Constraints: CA:TRUE

Subject Key Identifier:
EC:D7:E3:82:D2:71:5D:64:4C:DF:2E:67:3F:E7:BA:98:AE:1C:0F:4F

Signature Algorithm:
sha384WithRSAEncryption
*/
"-----BEGIN CERTIFICATE-----\n"
"MIIFkDCCA3igAwIBAgIQBZsbV56OITLiOQe9p3d1XDANBgkqhkiG9w0BAQwFADBi\n"
"MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\n"
"d3cuZGlnaWNlcnQuY29tMSEwHwYDVQQDExhEaWdpQ2VydCBUcnVzdGVkIFJvb3Qg\n"
"RzQwHhcNMTMwODAxMTIwMDAwWhcNMzgwMTE1MTIwMDAwWjBiMQswCQYDVQQGEwJV\n"
"UzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQu\n"
"Y29tMSEwHwYDVQQDExhEaWdpQ2VydCBUcnVzdGVkIFJvb3QgRzQwggIiMA0GCSqG\n"
"SIb3DQEBAQUAA4ICDwAwggIKAoICAQC/5pBzaN675F1KPDAiMGkz7MKnJS7JIT3y\n"
"ithZwuEppz1Yq3aaza57G4QNxDAf8xukOBbrVsaXbR2rsnnyyhHS5F/WBTxSD1If\n"
"xp4VpX6+n6lXFllVcq9ok3DCsrp1mWpzMpTREEQQLt+C8weE5nQ7bXHiLQwb7iDV\n"
"ySAdYyktzuxeTsiT+CFhmzTrBcZe7FsavOvJz82sNEBfsXpm7nfISKhmV1efVFiO\n"
"DCu3T6cw2Vbuyntd463JT17lNecxy9qTXtyOj4DatpGYQJB5w3jHtrHEtWoYOAMQ\n"
"jdjUN6QuBX2I9YI+EJFwq1WCQTLX2wRzKm6RAXwhTNS8rhsDdV14Ztk6MUSaM0C/\n"
"CNdaSaTC5qmgZ92kJ7yhTzm1EVgX9yRcRo9k98FpiHaYdj1ZXUJ2h4mXaXpI8OCi\n"
"EhtmmnTK3kse5w5jrubU75KSOp493ADkRSWJtppEGSt+wJS00mFt6zPZxd9LBADM\n"
"fRyVw4/3IbKyEbe7f/LVjHAsQWCqsWMYRJUadmJ+9oCw++hkpjPRiQfhvbfmQ6QY\n"
"uKZ3AeEPlAwhHbJUKSWJbOUOUlFHdL4mrLZBdd56rF+NP8m800ERElvlEFDrMcXK\n"
"chYiCd98THU/Y+whX8QgUWtvsauGi0/C1kVfnSD8oR7FwI+isX4KJpn15GkvmB0t\n"
"9dmpsh3lGwIDAQABo0IwQDAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIB\n"
"hjAdBgNVHQ4EFgQU7NfjgtJxXWRM3y5nP+e6mK4cD08wDQYJKoZIhvcNAQEMBQAD\n"
"ggIBALth2X2pbL4XxJEbw6GiAI3jZGgPVs93rnD5/ZpKmbnJeFwMDF/k5hQpVgs2\n"
"SV1EY+CtnJYYZhsjDT156W1r1lT40jzBQ0CuHVD1UvyQO7uYmWlrx8GnqGikJ9yd\n"
"+SeuMIW59mdNOj6PWTkiU0TryF0Dyu1Qen1iIQqAyHNm0aAFYF/opbSnr6j3bTWc\n"
"fFqK1qI4mfN4i/RN0iAL3gTujJtHgXINwBQy7zBZLq7gcfJW5GqXb5JQbZaNaHqa\n"
"sjYUegbyJLkJEVDXCLG4iXqEI2FCKeWjzaIgQdfRnGTZ6iahixTXTBmyUEFxPT9N\n"
"cCOGDErcgdLMMpSEDQgJlxxPwO5rIHQw0uA5NBCFIRUBCOhVMt5xSdkoF1BN5r5N\n"
"0XWs0Mr7QbhDparTwwVETyw2m+L64kW4I1NsBm9nVX9GtUw/bihaeSbSpKhil9Ie\n"
"4u1Ki7wb/UdKDd9nZn6yW0HQO+T0O/QEY+nvwlQAUaCKKsnOeMzV6ocEGLPOr0mI\n"
"r/OSmbaz5mEP0oUA51Aa5BuVnRmhuZyxm7EAHu/QD09CbMkKvO5D+jpxpchNJqU1\n"
"/YldvIViHTLSoCtU7ZpXwdv6EM8Zt4tKG48BtieVU+i2iW1bvGjUI+iLUaJW+fCm\n"
"gKDWHrO8Dw9TdSmq6hN35N6MgSGtBxBHEa2HPQfRdbzP82Z+\n"
"-----END CERTIFICATE-----\n"

#if RSG_PC
/*
T2 Palo Alto CA
Reason for adding: requested by Dan Cooper to support SSL intercept
on PC titles.

Valid Until: 7/12/2025
Serial Number: 00e13b2bc17ad6ac5b
Thumbprint: 42e7e6d0747bd976120ec351ca98450da9805f71
*/
"-----BEGIN CERTIFICATE-----\n"
"MIIDIjCCAgqgAwIBAgIJAOE7K8F61qxbMA0GCSqGSIb3DQEBCwUAMEExCzAJBgNV\n"
"BAYTAlVTMQswCQYDVQQIEwJOWTELMAkGA1UECxMCSVQxGDAWBgNVBAMTD1QyIFBh\n"
"bG8gQWx0byBDQTAeFw0xNTA3MTUyMzUzNDJaFw0yNTA3MTIyMzUzNDJaMEExCzAJ\n"
"BgNVBAYTAlVTMQswCQYDVQQIEwJOWTELMAkGA1UECxMCSVQxGDAWBgNVBAMTD1Qy\n"
"IFBhbG8gQWx0byBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANBa\n"
"nVZxAhk+spmp8JBzwtgPS+2oQ/Kpv7BRWn7Rj52oAL++a4DH2zRLb3f4mMyPpgEX\n"
"nNrvZSkonxJToEMhAed5eIKNOjcwDT8kLatfmE/LHD4xNeK0FN0p0OlTJkwLI5KZ\n"
"rne/YKqkYscYfjxyugYPb3ukjYfxeGNfeF9HGKTPAcL+fj0/fgkffJJPZ+d9+C2Z\n"
"YXXI36DoXEjHUPxOz/hKL5ohM4m86OHt6qvZaG/b43raI99ckRFTwmoF+AfdIgVa\n"
"3cuaxhnGyv+bC41LTxd1Azzys0txfxkFrt14oBUgOmPJelC26KgFoHnG8hSykNaH\n"
"Uh8S8i8jmXgEdUN+bGkCAwEAAaMdMBswDAYDVR0TBAUwAwEB/zALBgNVHQ8EBAMC\n"
"AgQwDQYJKoZIhvcNAQELBQADggEBAAqFo1P7Btgt9BZRpwmjA8ghQmQZQ9Fek1m2\n"
"aOmkPMa6OqWxXDT5VU/b02LfKb+uVLsdBC1cv7ZrRYZc7ATt8d19rohvSn7DL8GR\n"
"FEjYgj1OFvblbN8zHogDVx4gD57j9imT40vNpBcsEiRlNXDu/ExbhsDiing1tdmq\n"
"eULdQg13kdJzqVS6xJ10qddqRRUWASkq5qUwvTO2MQkCjPSGjUAHeHa9vCZRiVWQ\n"
"vWVwZ7CZ6XTzqcpfKdT/xKGHbT6xQxIhLVOvSMIhirh8OyFVzM5m+PHYN4KO6x5j\n"
"+Je/viLhvfaEVUntOkOcpQVmwOYZezQZqDDRWXo9k0vqGnZ8Gog=\n"
"-----END CERTIFICATE-----\n"

/*
T2 Internal Root CA
Reason for adding: requested by Dan Cooper to support SSL intercept
on PC titles.

Valid Until: 8/19/2035
Serial Number: 1e3af86f417725ab4350e925e9729059
Thumbprint: a0d9f27e30ffc0cb4f820409999ccddb48251b6c
*/
"-----BEGIN CERTIFICATE-----\n"
"MIIFnjCCA4agAwIBAgIQHjr4b0F3JatDUOkl6XKQWTANBgkqhkiG9w0BAQsFADBg\n"
"MQswCQYDVQQGEwJVUzEmMCQGA1UEChMdVGFrZS1Ud28gSW50ZXJhY3RpdmUgU29m\n"
"dHdhcmUxCzAJBgNVBAsTAklUMRwwGgYDVQQDExNUMiBJbnRlcm5hbCBSb290IENB\n"
"MB4XDTE1MDgxOTE0MjEwOFoXDTM1MDgxOTE0MzAzMFowYDELMAkGA1UEBhMCVVMx\n"
"JjAkBgNVBAoTHVRha2UtVHdvIEludGVyYWN0aXZlIFNvZnR3YXJlMQswCQYDVQQL\n"
"EwJJVDEcMBoGA1UEAxMTVDIgSW50ZXJuYWwgUm9vdCBDQTCCAiIwDQYJKoZIhvcN\n"
"AQEBBQADggIPADCCAgoCggIBAKtAcBNQXncmflG36c5yaoCclux13SPho+UrF5IG\n"
"LDJlXKrtzZjh/c9kcP/rL0ZfIHvuvMZlrX0YEHYWL0dV/hDUl7yNDKR4pK6N7LnZ\n"
"KF4iSLZYyxOTZ/YSM9UuGYiyrXNXRKVqMkMpnu8oZ63k4R2GxV5BUlHAIniPg9Md\n"
"oky5v6bFU9NBa9E5aiDTWrLv9G0xw4zOI/RchZ4rOPCDjs2vcKVntMkSGeVlah+E\n"
"s5BfeVQB13kEXXBsWTVuEh8zswEYs5thWjWsdzmfC471GCm1M25+xQGcnpFwMmJk\n"
"pwR2Boc/Fr1EY7N/EMisroJpcKKivKbC3/8gE9KKeGlVsMi/GjrtKKSwE/T3pvPA\n"
"TJwt9aGrhsP8MdnnV9sXdw8RnVrEtoqQxfvSNAxo8+rGNII+5ogn4zVj1rw8wT8K\n"
"ZLZzYXWnyDHOIP7vV7r9NG1QyGoWZSnHTh1HVd/ha2D6qCn8m41leOnHax5dwATH\n"
"GGuezXiO9vXbTl0iI4wQo9biyqj0bqVyBhSBQmoZTtQIufF9ut1t66Xcdxo09GfV\n"
"oyl8PDis4VRP1j0qys4EkAt1seOpilXemBN6q/Co3sO65/WWF/7V6fQ82b5SQgDy\n"
"B2VX7lgWBf7Lf6gYcNQFWUkYFqHcoPaDjwE9F8r4wjmOMe87A5zg0+6Bx58JF9uh\n"
"tbSPAgMBAAGjVDBSMA4GA1UdDwEB/wQEAwIBhjAPBgNVHRMBAf8EBTADAQH/MB0G\n"
"A1UdDgQWBBQjNzji1RKkOPqxhv1Aaz3N3bAthjAQBgkrBgEEAYI3FQEEAwIBADAN\n"
"BgkqhkiG9w0BAQsFAAOCAgEAgxXKhwje7xYoaaRdSk8yb9UtWDrBPDU6O7qHF5wy\n"
"PKQibOdyZLrZ61bgYT3Zm4aI8w1u71BQDX2OiXRh+MrgnIFWcbY+MTDnlW0nnyzE\n"
"ocFAQOtObEUPVDtPjFioPyY2r0iEPItHPFuqRt5WTGSMuzfcosSoOUQnsIe2nUom\n"
"BU7hUSrBo2dg/4WlWaGGMhxg7KGRGo8Uypiucz2kux6o7HOT40/5I1SC/fELN3Uf\n"
"iaARAFoW10AgZV910zRYc+T+OU3pRgRSDtSawcpzHiiP7Z8JwKCdxG4gLGtW9V3b\n"
"4BIZ9yXZLaAwMsi9HznpkY6x0+jsHdz+qlSLrvxYmDx72DD3rPuGMX5ugPqi6u7m\n"
"QcgGxszykr5uk/UGB6JbmcCURuTMyKq6adduYkIDp85dU2udpHIUELVocgBMAyo8\n"
"VuRnElEDT6f6H0/zOH4qgoV5o+YgN261TLVkuWi1OAGuC+J2lwfPTKy/L4EBfgYj\n"
"edzMdJprVD3fo2mcMn1r/zmFV3v/OO3TlEqcpq7T/xwbAwCwPiUTKQp98gfkhU5x\n"
"s/QlnBrm2dFFYIMsuLNbfNMcPN507F8omHlFUeHMk/uWsDcIeEhXc+3FuSML0czo\n"
"RC3buDefJ1sb8gWjFUFHh3JP2xGZkD/4iZSqLDSVzsPjy03U6HBQSxuYVLj42MNT\n"
"rNQ=\n"
"-----END CERTIFICATE-----\n"
#endif
;

/*static*/
WOLFSSL_CTX*
rlRos::GetRosSslContext()
{
    return s_RosSslCtx;
}

const char*
rlRos::GetRosCertificateAuthority()
{
	return s_RosCAs;
}

bool
rlRos::InitRosSslContext()
{
    rtry
    {
        rverify(wolfSSL_Init() == SSL_SUCCESS,
                catchall,
                rlError("Error initializing SSL"));

		if(!PARAM_rlsslnocert.Get() RL_FORCE_STAGE_ENVIRONMENT_ONLY(&& !rlIsForcedEnvironment()))
		{
            //@@: range RLROS_INITROSSSLCONTEXT {
            s_RosSslCtx = netTcp::GetSecureSslCtx();
            rverify(s_RosSslCtx != NULL,
                    catchall,
                    wolfSSL_Cleanup(); rlError("Error creating ros SSL context"));

            //Load our CAs.
            const int loadCAResult = wolfSSL_CTX_load_verify_buffer(s_RosSslCtx, (const unsigned char*)s_RosCAs, (long)strlen(s_RosCAs), SSL_FILETYPE_PEM);
			//@@: } RLROS_INITROSSSLCONTEXT 
            rverify(loadCAResult == SSL_SUCCESS,
                    catchall,
                    netError("Error loading ros CA buffer: %d", loadCAResult));
        }
        else
        {
            s_RosSslCtx = netTcp::GetInsecureSslCtx();
            rverify(s_RosSslCtx != NULL,
                    catchall,
                    wolfSSL_Cleanup(); rlError("Error creating ros SSL context"));
        }

		OUTPUT_ONLY(DumpWolfSslCapabilities();)
		
        return true;
    }
    rcatchall
    {
        return false;
    }
}

#if !__NO_OUTPUT
void rlRos::DumpWolfSslCapabilities()
{
#if defined(WOLFSSL_AESNI)
	const bool wolfSslAesNi = true;
#else
	const bool wolfSslAesNi = false;
#endif

#if defined(USE_INTEL_SPEEDUP)
	const bool wolfSslIntelSpeedUp = true;
#else
	const bool wolfSslIntelSpeedUp = false;
#endif

#if defined(WOLFSSL_SP)
	const bool wolfSslSpMath = true;
#else
	const bool wolfSslSpMath = false;
#endif

    rlDebug3("wolfSSL version %s", wolfSSL_lib_version());
   
	rlDebug3("wolfSSL CPU features:");
	rlDebug3("    AES-NI define: %s", LogEnabled(wolfSslAesNi));
	rlDebug3("    USE_INTEL_SPEEDUP define: %s", LogEnabled(wolfSslIntelSpeedUp));
    rlDebug3("    WOLFSSL_SP define: %s", LogEnabled(wolfSslSpMath));

#if defined(WOLFSSL_AESNI) || defined(USE_INTEL_SPEEDUP)
	const word32 cpuidFlags = cpuid_get_flags();

	rlDebug3("    AES-NI: %s", LogEnabled(IS_INTEL_AESNI(cpuidFlags) != 0));
	rlDebug3("    AVX1: %s", LogEnabled(IS_INTEL_AVX1(cpuidFlags) != 0));
	rlDebug3("    AVX2: %s", LogEnabled(IS_INTEL_AVX2(cpuidFlags) != 0));
	rlDebug3("    RDRAND: %s", LogEnabled(IS_INTEL_RDRAND(cpuidFlags) != 0));
	rlDebug3("    RDSEED: %s", LogEnabled(IS_INTEL_RDSEED(cpuidFlags) != 0));
	rlDebug3("    BMI2: %s", LogEnabled(IS_INTEL_BMI2(cpuidFlags) != 0));
	rlDebug3("    ADX: %s", LogEnabled(IS_INTEL_ADX(cpuidFlags) != 0));
	rlDebug3("    MOVEBE: %s", LogEnabled(IS_INTEL_MOVBE(cpuidFlags) != 0));
#endif

	char cipherSuites[8192] = {0};
	int ret = wolfSSL_get_ciphers(cipherSuites, (int)sizeof(cipherSuites));
	if(ret == SSL_SUCCESS)
	{
		rlDebug3("wolfSSL cipher suites available:");

		char *cipherSuite = safestrtok((char*)cipherSuites, ":");
		while(cipherSuite != nullptr)
		{
			rlDebug3("    %s", cipherSuite);
			cipherSuite = safestrtok(NULL, ":");
		}
	}
}
#endif

}   //namespace rage
