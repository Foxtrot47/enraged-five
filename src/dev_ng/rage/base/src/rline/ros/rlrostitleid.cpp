// 
// rline/rlrostitleid.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrostitleid.h"
#include "diag/seh.h"
#include "system/memops.h"
#include "system/nelem.h"
#include "string/string.h"

#include "wolfssl/wolfcrypt/asn.h"

namespace rage
{

#if __RGSC_DLL
	// get rid of this in final since we're shipping the SC before the game.
	PARAM(rlrosdomainenv,       "Overrides ROS environment string used in domain names (ci-rage,rage,dev,load,qa,cert,prod)");
#elif __MASTER
	PARAM(rlrosdomainenv,       "Overrides ROS environment string used in domain names (ci-rage,rage,dev,load,qa,cert,prod)");
#else
	NOSTRIP_PC_PARAM(rlrosdomainenv,       "Overrides ROS environment string used in domain names (ci-rage,rage,dev,load,qa,cert,prod)");
#endif

#if RLROS_SC_PLATFORM
rlRosTitleId::rlRosTitleId(const char* titleName,
						   const char* titleDirectoryName,
                           const int scVersion,
                           const int titleVersion,
                           const char* titleSecretsBase64,
#if RSG_PC && __STEAM_BUILD
						   const char* steamAuthTicket,
						   u32 steamAppId,
#endif
#if __MAC_APPSTORE_BUILD
						   char* macAppStoreReceipt,
						   const u32 macAppStoreReceiptLen,
#endif
                           const rlRosEnvironment env,
                           const u8* publicRsaKey,
                           const unsigned publicRsaKeyLen)
: m_Env(env)
, m_ForcedEnv(RLROS_ENV_UNKNOWN)
{
    rlAssert(RLROS_ENV_UNKNOWN != m_Env);

	safecpy(m_TitleDirectoryName, titleDirectoryName);

#if RSG_PC && __STEAM_BUILD
	safecpy(m_SteamAuthTicket, steamAuthTicket);
	m_SteamAppId = steamAppId;
#endif	

#if __MAC_APPSTORE_BUILD
	m_MacAppStoreReceipt = macAppStoreReceipt;
	m_MacAppstoreReceiptLength = macAppStoreReceiptLen;
#endif

    CommonInit(titleName, 
               scVersion, 
               titleVersion, 
               titleSecretsBase64,
               publicRsaKey,
               publicRsaKeyLen);
}

const char* rlRosTitleId::GetTitleSecretsBase64() const
{
	return m_TitleSecretsBase64;
}

const char* rlRosTitleId::GetTitleDirectoryName() const
{
	return m_TitleDirectoryName;
}

#if RSG_PC && __STEAM_BUILD
const char* rlRosTitleId::GetSteamAuthTicket() const
{
	return m_SteamAuthTicket;
}

u32 rlRosTitleId::GetSteamAppId() const
{
	return m_SteamAppId;
}
#endif


#if __MAC_APPSTORE_BUILD
char * rlRosTitleId::GetMacAppStoreReceipt() const
{
	return m_MacAppStoreReceipt;
}

u32 rlRosTitleId::GetMacAppstoreReceiptLength() const
{
	return m_MacAppstoreReceiptLength;
}
#endif

#else

rlRosTitleId::rlRosTitleId(const char* titleName, 
                           const int scVersion,
                           const int titleVersion,
                           const char* titleSecretsBase64,
                           const u8* publicRsaKey,
                           const unsigned publicRsaKeyLen)
: m_ForcedEnv(RLROS_ENV_UNKNOWN)
{
    CommonInit(titleName, 
               scVersion, 
               titleVersion, 
               titleSecretsBase64,
               publicRsaKey,
               publicRsaKeyLen);
}

#endif

const char* 
rlRosTitleId::GetTitleName() const 
{ 
    return m_TitleName; 
}

int 
rlRosTitleId::GetScVersion() const 
{ 
    return m_ScVersion; 
}

int 
rlRosTitleId::GetTitleVersion() const 
{ 
    return m_TitleVersion; 
}

const char* 
rlRosTitleId::GetPlatformName()
{ 
#if RSG_DURANGO
	return "xboxone";
#elif RSG_ORBIS
	return "ps4";
#elif RSG_PC
    return "pcros";
#else
    return 0;
#endif
}

u32
rlRosTitleId::GetPlatformId()
{
#if RSG_DURANGO
	return PLATFORM_ID_DURANGO;
#elif RSG_ORBIS
	return PLATFORM_ID_ORBIS;
#elif RSG_PC
	return PLATFORM_ID_PC; 
#elif RSG_PROSPERO
	return PLATFORM_ID_PROSPERO;
#elif RSG_SCARLETT
	return PLATFORM_ID_SCARLETT;
#else
	return PLATFORM_ID_INVALID;
#endif
}

const rlRosTitleSecrets&
rlRosTitleId::GetTitleSecrets() const 
{ 
    return m_TitleSecrets;
}

struct EnvNameMap
{
    const char* Name;
    rlRosEnvironment Env;
};

const EnvNameMap ENV_NAME_MAP[] =
{
    {"rage", RLROS_ENV_RAGE},
    {"dev", RLROS_ENV_DEV},
    {"cert", RLROS_ENV_CERT},
    {"prod", RLROS_ENV_PROD},
	// ADD NEW ENVIRONMENTS AFTER THIS, DO NOT CHANGE ORDER
	{"cert2", RLROS_ENV_CERT_2},
	{"ci-rage", RLROS_ENV_CI_RAGE},
};

rlRosEnvironment 
rlRosTitleId::GetEnvironment() const 
{
    // rlRosGetForcedEnvironment is also checked in the platform specific calls to
    // ensure the native env is set to the overwritten env where needed.
    // So this isn't technically needed but rlEnvironment and rlRosEnvironment
    // don't match 1 to 1 so this ensures all Ros environments can be tested.
    rlRosEnvironment forcedEnvironment = rlRosGetForcedEnvironment();
    if(forcedEnvironment != RLROS_ENV_UNKNOWN)
    {
        return forcedEnvironment;
    }

    const rlEnvironment env = rlGetEnvironment();
    switch(env)
    {
    case RL_ENV_DEV:    return RLROS_ENV_DEV;
    case RL_ENV_CERT:   return RLROS_ENV_CERT;
    case RL_ENV_CERT_2: return RLROS_ENV_CERT_2;
    case RL_ENV_PROD:   return RLROS_ENV_PROD;
    case RL_ENV_CI_RAGE:return RLROS_ENV_CI_RAGE;
    default:            return RLROS_ENV_UNKNOWN;
    }
}

const char* 
rlRosTitleId::GetEnvironmentName() const
{
	switch(GetEnvironment())
	{
	case RLROS_ENV_RAGE:    return "rage"; break;
	case RLROS_ENV_CI_RAGE: return "ci-rage"; break;
	case RLROS_ENV_DEV:     return "dev"; break;
	case RLROS_ENV_CERT:    return "cert"; break;
	case RLROS_ENV_CERT_2:  return "cert2"; break;
	case RLROS_ENV_PROD:    return "prod"; break;
	default:                return NULL;   
	}
}

const char*
rlRosTitleId::GetRosBaseDomainName() const
{
	return RLROS_BASE_DOMAIN_NAME;
}

void
rlRosTitleId::SetDomainNamePrefix(const char* hostNamePrefix)
{
	safecpy(m_HostNamePrefix, hostNamePrefix);
}

const char* 
rlRosTitleId::GetDomainNamePrefix() const
{
	return m_HostNamePrefix;
}

const char*
rlRosTitleId::GetDomainName() const
{
    if('\0' == m_DomainName[0])
    {
        const char* envName = GetEnvironmentName();
		if (envName == NULL)
		{
			return NULL;
		}
       
        formatf(m_DomainName, sizeof(m_DomainName), "%s.%s", 
                envName,
                RLROS_BASE_DOMAIN_NAME);
    }

    return m_DomainName;
}

const char*
rlRosTitleId::GetCloudDomainName() const
{
    if('\0' == m_CloudDomainName[0])
    {
		const char* envName = GetEnvironmentName();
		if (envName == NULL)
		{
			return NULL;
		}

        formatf(m_CloudDomainName, sizeof(m_CloudDomainName), "%s.%s", 
                envName,
                RLROS_CLOUD_BASE_DOMAIN_NAME);
    }

    return m_CloudDomainName;
}

bool 
rlRosTitleId::GetPublicRsaKey(const u8** encodedKey, unsigned* keyLen) const
{
    *encodedKey = m_PublicRsaKey;
    *keyLen = m_PublicRsaKeyLen;

    return *keyLen > 0;
}

rlRosSecurityFlags rlRosTitleId::GetDefaultSecurityFlags() const
{
	return m_SecurityFlags;
}

void rlRosTitleId::SetDefaultSecurityFlags(rlRosSecurityFlags flags)
{
	m_SecurityFlags = flags;
}

void
rlRosTitleId::Reset()
{
    sysMemSet(m_TitleName, 0, sizeof(m_TitleName));
	sysMemSet(m_HostNamePrefix, 0, sizeof(m_HostNamePrefix));

    m_TitleVersion = 0;
    m_ScVersion = 0;
	m_SecurityFlags = RLROS_SECURITY_DEFAULT;
    m_TitleSecrets.Clear();
    m_PublicRsaKey = NULL;
    m_PublicRsaKeyLen = 0;
}

bool
rlRosTitleId::CommonInit(const char* titleName, 
                         const int scVersion,
                         const int titleVersion,
                         const char* titleSecretsBase64,
                         const u8* publicRsaKey,
                         const unsigned publicRsaKeyLen)
{
    rtry
    {
        Reset();

        rverify(titleName, catchall, );

		//Ignore master secret and http key/salt on LAN (SC DLL/sample_session); crypto is unsupported there.
        if(strcmp(titleName, "lanoire") != 0)
        {
#if RLROS_SC_PLATFORM
			safecpy(m_TitleSecretsBase64, titleSecretsBase64);
			rverify(strlen(m_TitleSecretsBase64) == strlen(titleSecretsBase64), catchall, );
#endif
            rverify(m_TitleSecrets.Reset(titleSecretsBase64),
                    catchall,
                    rlError("Failed to parse title secret string"));
        }

        m_DomainName[0] = '\0';
        m_CloudDomainName[0] = '\0';
		m_HostNamePrefix[0] = '\0';
        safecpy(m_TitleName, titleName, sizeof(m_TitleName));
        m_ScVersion = scVersion;
        m_TitleVersion = titleVersion;
        m_PublicRsaKey = publicRsaKey;
        m_PublicRsaKeyLen = publicRsaKeyLen;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

}   //namespace rage
