// 
// rline/rlroscredtasks.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlroscredtasks.h"
#include "rlros.h"
#include "rlroscommon.h"
#include "rline/rltitleid.h"
#include "rline/rlpc.h"
#include "rline/rlpcpipe.h"
#include "rline/rlpresence.h"
#include "rline/socialclub/rlsocialclub.h"
#include "data/aes.h"
#include "data/rson.h"
#include "diag/seh.h"
#include "net/crypto.h"
#include "net/nethardware.h"
#include "net/task.h"
#include "net/status.h"
#include "parser/manager.h"
#include "system/timer.h"
#include "system/nelem.h"
#include "data/base64.h"

#if RSG_DURANGO
#include "rline/durango/rlxbl_interface.h"
#endif

PARAM(cloudkey, "Cloud key for encryption (base 64)");
PARAM(geoLocCountry, "Set a custom result for the geo location country");
PARAM(ticketExpirationSec, "Set a custom ticket expiration time (in seconds)");
NOSTRIP_FINAL_LOGGING_PARAM(geoLocFakeIp, "Set a fake IP to use for geoloc");

#if RSG_ORBIS
PARAM(rlRosForceUseAccountIds, "If present, client will always use account Ids");
#endif

namespace rage
{
#if RSG_DURANGO && !__FINAL
	XPARAM(xuid);
#endif // RSG_DURANGO && !__FINAL

extern const rlTitleId* g_rlTitleId;

RAGE_DEFINE_SUBCHANNEL(rline_ros, credtasks)

#undef __rage_channel
#define __rage_channel rline_ros_credtasks

//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketResponse
//////////////////////////////////////////////////////////////////////////
rlRosCreateTicketResponse::rlRosCreateTicketResponse()
{
	Clear();
}

void 
rlRosCreateTicketResponse::Clear()
{
	m_AccountId = 0;
	m_CloudKeyBase64[0] = '\0';
	m_DisabledSecurityFlags = 0;
	m_IsSubAccount = false;
	m_NumServiceHosts = 0;
	m_NumSslServices = 0;
	m_PosixTime = 0;
	m_PrivilegesCsv[0] = '\0';
	m_PublicIp[0] = '\0';
	m_Region = 0;
	m_RockstarAcct.Clear();
	m_SecsUntilExpiration = 0;
	m_SessionId = 0;
	m_SessionKeyBase64[0] = '\0';
	m_SessionTicket[0] = '\0';
	m_Ticket[0] = '\0';

#if RSG_ORBIS
#if !__FINAL
    // default to true if we're using Np4, false otherwise
    m_UseNpAccountIds = !rlRos::UseCreateTicketNp3();
#else
    // final builds default to Np4
    m_UseNpAccountIds = true;
#endif
#endif

	for (int i = 0; i < NELEM(m_PrivilegeInfos); i++)
	{
		m_PrivilegeInfos[i].m_IsValid = false;
	}
}

void 
rlRosCreateTicketResponse::DebugPrint() const
{
#if !__NO_OUTPUT
	rlDebug("<Ticket>=%s", m_Ticket);
	rlDebug("<CloudKey>=%s", m_CloudKeyBase64);
	rlDebug("<DisabledSecurityFlags>=%d", m_DisabledSecurityFlags);
	rlDebug("<IsSubAccount>=%s", m_IsSubAccount ? "True" : "False");
	rlDebug("<PlayerAccountId>=%d", m_AccountId);
	rlDebug("<PosixTime>=%" I64FMT "u", m_PosixTime);
	rlDebug("<Privileges>=%s", m_PrivilegesCsv);

#if RSG_ORBIS
    rlDebug("<UseNpAccountIds>=%s", m_UseNpAccountIds ? "True" : "False");
#endif

	int numPrivs = 0;
	for (unsigned i = 0; i < NELEM(m_PrivilegeInfos); i++)
	{
		if (m_PrivilegeInfos[i].m_IsValid) 
		{ 
			++numPrivs; 
		}
	}
	if (numPrivs > 0)
	{
		rlDebug("<Privs>: %d", numPrivs);
		for (unsigned i = 0; i < NELEM(m_PrivilegeInfos); i++)
		{
			const rlRosCredentials::PrivilegeInfo& p = m_PrivilegeInfos[i];
			if (p.m_IsValid)
			{
				rlDebug("    <Priv>= id:%d, g:%s, ed:%" I64FMT "u",
					i,
					p.m_IsGranted ? "true" : "false",
					p.m_HasEndDate ? p.m_EndPosixTime : 0);
			}
		}
	}

	rlDebug("<PublicIp>=%s", m_PublicIp);
	rlDebug("<Region>=%d", m_Region);

	if (m_RockstarAcct.m_RockstarId > 0)
	{
		rlDebug("<RockstarAccount>");
		rlDebug("    <RockstarId>=%" I64FMT "d", m_RockstarAcct.m_RockstarId);
		rlDebug("    <Age>=%d", m_RockstarAcct.m_Age);
		rlDebug("    <AvatarUrl>=%s", m_RockstarAcct.m_AvatarUrl);
		rlDebug("    <CountryCode>=%s", m_RockstarAcct.m_CountryCode);
		rlDebug("    <Email>=%s", m_RockstarAcct.m_Email);
		rlDebug("    <LanguageCode>=%s", m_RockstarAcct.m_LanguageCode);
		rlDebug("    <Nickname>=%s", m_RockstarAcct.m_Nickname);
		rlDebug("    <ZipCode>=%s", m_RockstarAcct.m_ZipCode);
	}

	rlDebug("<SecsUntilExpiration>=%d", m_SecsUntilExpiration);

	if (m_NumServiceHosts > 0)
	{
		rlDebug("<ServiceHosts>: %d", m_NumServiceHosts);
		for (unsigned i = 0; i < m_NumServiceHosts; i++)
		{
			const rlRosServiceHost& sh = m_ServiceHosts[i];
			rlDebug("    <ServiceHost>[%d]: ep:%s, h:%s", i, sh.Endpoint, sh.Host);
		}
	}

	rlDebug("<SessionId>=%" I64FMT "d", m_SessionId);
	rlDebug("<SessionKey>=%s", m_SessionKeyBase64);
	rlDebug("<SessionTicket>=%s", m_SessionTicket);

	if (m_NumSslServices > 0)
	{
		rlDebug("<SslServices>: %d", m_NumSslServices);
		for (unsigned i = 0; i < m_NumSslServices; i++)
		{
			const rlRosSslService& s = m_SslServices[i];
			rlDebug("    <SslServices>[%d]=%s", i, s.Endpoint);
		}
	}
#endif //!__NO_OUTPUT
}

//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketResponseHelper
//////////////////////////////////////////////////////////////////////////
bool
rlRosCreateTicketResponseHelper::ApplyCreateTicketResponse(
	const rlRosCreateTicketResponse& response,
#if RSG_ORBIS
	const int localGamerIndex,
#else
	const int UNUSED_PARAM(localGamerIndex),
#endif
	rlRosCredentials* cred)
{
	rlAssert(cred);

	rlDebug("ApplyCreateTicketResponse");

	rtry
	{
		//Validate the response.
		rverify('\0' != response.m_Ticket[0], catchall, rlError("<Ticket> cannot be empty"));
		rverify(response.m_SecsUntilExpiration > 0, catchall, rlError("<SecsUntilExpiration> must be greater than zero"));
		rverify(response.m_AccountId > 0, catchall, rlError("<PlayerAccountId> must be greater than zero"));
		rverify('\0' != response.m_SessionKeyBase64[0], catchall, rlError("<SessionKey> cannot be empty"));

#if __RGSC_DLL
		// The RGSC DLL needs to stay backwards compatible with older games.
		// lanoire does not use a session ticket, so we should not fail if it doesn't exist.
		if ('\0' == response.m_SessionTicket[0])
		{
			rverify(!strcmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "lanoire"),
				catchall,
				rlError("Session ticket required by all titles except lanoire"));
		}
#else
		rverify('\0' != response.m_SessionTicket[0], catchall, rlError("<SessionTicket> cannot be empty"));
#endif

		//Unpack the privilege CSV into a bit set.
		atFixedBitSet<RLROS_NUM_PRIVILEGEID> privileges;
		int len = istrlen(response.m_PrivilegesCsv);
		int startIndex = 0;
		for (int i = 0; i < len; i++)
		{
			if (response.m_PrivilegesCsv[i] == ',' || (i == (len - 1)))
			{
				int bitIndex;
				rverify(sscanf(&response.m_PrivilegesCsv[startIndex], "%d", &bitIndex), catchall,);
				startIndex = i + 1;

				// If the web service returns a privilege index to an older version of Rage that doesn't
				// know about the privilege, don't try to set the bit (avoids an array out of bounds error).
				if (bitIndex < RLROS_NUM_PRIVILEGEID)
				{
					privileges.Set(bitIndex, true);
				}
			}
		}

		//Unpack session key.
		rlRosCredentials::SessionKey sessionKey;
		rverify(
			sessionKey.FromBase64(response.m_SessionKeyBase64), 
			catchall, 
			rlError("Failed to decode session key %s", response.m_SessionKeyBase64));

		//Unpack cloud key.
		const char* cloudKeyBase64 = response.m_CloudKeyBase64;
#if !__FINAL
		const char* cloudKeyParam = NULL;
		if (PARAM_cloudkey.Get(cloudKeyParam))
		{
			rlDebug("Overriding cloud key from PARAM_cloudkey");
			cloudKeyBase64 = cloudKeyParam;
		}
#endif
		rlRosCredentials::CloudKey cloudKey;
		if (NULL != cloudKeyBase64 && cloudKeyBase64[0] != '\0')
		{
			rverify(
				cloudKey.FromBase64(cloudKeyBase64),
				catchall,
				rlError("Failed to decode cloud key %s", cloudKeyBase64));
		}

		//Ticket expiration time can be overridden by cmdline param.
		int secsUntilExpiration = response.m_SecsUntilExpiration;
#if !__FINAL
		u32 ticketExpirationOverride;
		if (PARAM_ticketExpirationSec.Get(ticketExpirationOverride))
		{
			secsUntilExpiration = ticketExpirationOverride;
			rlDebug("PARAM_ticketExpirationSec override: <SecsUntilExpiration>=%d", secsUntilExpiration);
		}
#endif

		//Apply the parsed data.
		rlRos::SetServiceHosts(const_cast<rlRosServiceHost*>(response.m_ServiceHosts), response.m_NumServiceHosts);
		rlRos::SetSslServices(const_cast<rlRosSslService*>(response.m_SslServices), response.m_NumSslServices);
		rlRos::SetDisabledSecurityFlags((rlRosSecurityFlags)response.m_DisabledSecurityFlags);

		PlayerAccountId playerAccountId = response.m_AccountId;
		
		unsigned validMs = secsUntilExpiration * 1000;
		
		rcheck(cred->Reset(
				response.m_Ticket,
				validMs,
				response.m_Region,
				playerAccountId, 
				response.m_RockstarAcct,
				response.m_SessionId,
				privileges,
				sessionKey,
				response.m_SessionTicket,
				cloudKey,
				response.m_PrivilegeInfos),
			catchall,
			rlError("Failed to reset credentials"));

		AES::CreateCloudAes((const unsigned char*)cred->GetCloudKey().GetKeyBuffer());

		if (response.m_PublicIp[0] != '\0')
		{
			netIpAddress publicIpAddr;
			publicIpAddr.FromString(response.m_PublicIp);
			netHardware::SetPublicIpAddress(publicIpAddr);
		}

		if (response.m_PosixTime > 0)
		{
			rlSetPosixTime(response.m_PosixTime);
#if !__NO_OUTPUT
			diagChannel::SetPosixTime(response.m_PosixTime);
#endif
		}

#if RSG_ORBIS 

        bool useAccountIds = response.m_UseNpAccountIds;
#if !__FINAL
        if(PARAM_rlRosForceUseAccountIds.Get())
        {
            useAccountIds = true;
        }
#endif

        rlGamerHandle::SetUsingAccountIdAsKey(useAccountIds);

		if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && response.m_IsSubAccount)
		{
			g_rlNp.GetNpAuth().SetIsSubAccount(localGamerIndex, response.m_IsSubAccount);
		}
#endif
		return true;
	}
	rcatchall
	{
		return false;
	}
}

//////////////////////////////////////////////////////////////////////////
// rlRosXmlCreateTicketResponseParser
//////////////////////////////////////////////////////////////////////////
bool 
rlRosXmlCreateTicketResponseParser::Parse(
	const parTreeNode* node, 
	rlRosCreateTicketResponse* response)
{
    rlAssert(node);
	rlAssert(response);

    rtry
	{
		//WCF services return the create ticket result in a <Result> child element.
		//Check to see if one exists, and if it does process that node instead.
		const parTreeNode* resultNode = node->FindChildWithNameIgnoreNs("Result");
		if (resultNode)
		{
			return Parse(resultNode, response);
		}

		response->Clear();
		
		const char* ticket = rlHttpTaskHelper::ReadString(node, "Ticket", NULL);
		rcheck(ticket, catchall, rlError("Failed to read <Ticket>"));
		safecpy(response->m_Ticket, ticket);

		rcheck(rlHttpTaskHelper::ReadInt(response->m_SecsUntilExpiration, node, "SecsUntilExpiration", NULL),
			catchall,
			rlError("Failed to read <SecsUntilExpiration>"));

		rlHttpTaskHelper::ReadInt(response->m_Region, node, "Region", NULL);

		rcheck(rlHttpTaskHelper::ReadInt(response->m_AccountId, node, "PlayerAccountId", NULL),
			catchall,
			rlError("Failed to read <PlayerAccountId>"));

		//.asmx services return Privileges as attributes, but WCF services return them
		//as a child element, so we check for both.
		const char* privilegesCsv = rlHttpTaskHelper::ReadString(node, NULL, "Privileges");
		if (!privilegesCsv)
		{
			privilegesCsv = rlHttpTaskHelper::ReadString(node, "Privileges", NULL);
		}
		if (privilegesCsv)
		{
			safecpy(response->m_PrivilegesCsv, privilegesCsv);
		}

		rverify(ParsePrivilegeInfos(node, response->m_PrivilegeInfos), catchall,);

		rverify(ParseServiceHosts(node, response->m_ServiceHosts, &response->m_NumServiceHosts), catchall,);

		rverify(ParseSslServices(node, response->m_SslServices, &response->m_NumSslServices), catchall,);

		rlHttpTaskHelper::ReadInt(response->m_DisabledSecurityFlags, node, "DisabledSecurityFlags", NULL);

		rverify(ParseRockstarAccount(node, response->m_RockstarAcct), catchall,);

		rlHttpTaskHelper::ReadInt64(response->m_SessionId, node, "SessionId", NULL);

		const char* sessionKeyBase64 = rlHttpTaskHelper::ReadString(node, "SessionKey", NULL);
		rverify(sessionKeyBase64, catchall, rlError("Failed to read <SessionKey>"));
		safecpy(response->m_SessionKeyBase64, sessionKeyBase64);

		const char* sessionTicket = rlHttpTaskHelper::ReadString(node, "SessionTicket", NULL);
		if (sessionTicket)
		{
			safecpy(response->m_SessionTicket, sessionTicket);
		}

		const char* cloudKeyBase64 = rlHttpTaskHelper::ReadString(node, "CloudKey", NULL);
		if (cloudKeyBase64)
		{
			safecpy(response->m_CloudKeyBase64, cloudKeyBase64);
		}

		const char* publicIp = rlHttpTaskHelper::ReadString(node, "PublicIp", NULL);
		if (publicIp)
		{
			safecpy(response->m_PublicIp, publicIp);
		}

		const char* posixTime = rlHttpTaskHelper::ReadString(node, "PosixTime", NULL);
        if (posixTime)
		{
            if(!rlVerify(sscanf(posixTime, "%" I64FMT "u", &response->m_PosixTime) == 1))
            {
                rlError("Failed to read posix time from create ticket response");
            }
		}

#if RSG_ORBIS
        rlHttpTaskHelper::ReadBool(response->m_UseNpAccountIds, node, "UseNpAccountIds", NULL);
#endif

		rlHttpTaskHelper::ReadBool(response->m_IsSubAccount, node, "IsSubAccount", NULL);

		rlDebug("Parsed response:");
		response->DebugPrint();
		
        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlRosXmlCreateTicketResponseParser::ParsePrivilegeInfos(
	const parTreeNode* node,
	rlRosCredentials::PrivilegeInfo (&privilegeInfos)[RLROS_NUM_PRIVILEGEID])
{
	rlAssert(node);

	for (int i = 0; i < RLROS_NUM_PRIVILEGEID; i++)
	{
		privilegeInfos[i].m_IsValid = false;
	}

	const parTreeNode* privilegeInfosNode = node->FindChildWithNameIgnoreNs("Privs");
	if (privilegeInfosNode)
    {
        for (parTreeNode::ChildNodeIterator it = privilegeInfosNode->BeginChildren(); 
			it != privilegeInfosNode->EndChildren(); 
			++it)
        {
            const parTreeNode& node = **it;
                
            rtry
            {
				//.asmx services return these items as attributes, but WCF services return them
				//as child elements, so we check for both.

                //Read the privilege id and make sure it's valid
                int privilegeId = RLROS_PRIVILEGEID_NONE;
                rverify(rlHttpTaskHelper::ReadInt(privilegeId, &node, NULL, "id") 
					|| rlHttpTaskHelper::ReadInt(privilegeId, &node, "id", NULL), privs,);
                rverify(privilegeId >= 0 && privilegeId < NELEM(privilegeInfos), 
					privs, 
					rlError("Privilege with id: %d is out of range (Count: %d)", privilegeId, RLROS_NUM_PRIVILEGEID));

                //Read whether or not the privilege is granted/denied
                bool isGranted = false;
                rverify(rlHttpTaskHelper::ReadBool(isGranted, &node, NULL, "g")
						|| rlHttpTaskHelper::ReadBool(isGranted, &node, "g", NULL), privs,);

				rlRosCredentials::PrivilegeInfo& pi = privilegeInfos[privilegeId];
                pi.m_IsValid = true; 
				pi.m_IsGranted = isGranted;

                //Read the end date POSIX time (optional)
				pi.m_HasEndDate = 
				    rlHttpTaskHelper::ReadUInt64(pi.m_EndPosixTime, &node, NULL, "ed") ||
					rlHttpTaskHelper::ReadUInt64(pi.m_EndPosixTime, &node, "ed", NULL);
            }
            rcatch(privs)
            {
            }
        }
    }

	return true;
}

bool
rlRosXmlCreateTicketResponseParser::ParseServiceHosts(
	const parTreeNode* node,
	rlRosServiceHost (&serviceHosts)[rlRos::MAX_SERVICE_HOSTS],
	unsigned* numServiceHosts)
{
	rlAssert(node);
	rlAssert(numServiceHosts);
	
	*numServiceHosts = 0;

	const parTreeNode* servicesNode = node->FindChildWithNameIgnoreNs("Services");
	if (servicesNode)
	{
		rtry
		{
			//.asmx services return Count as an attribute, but WCF services returns it
			//as a child element, so we check for both.
			int count;
			rcheck(rlHttpTaskHelper::ReadInt(count, servicesNode, NULL, "Count") ||
				   rlHttpTaskHelper::ReadInt(count, servicesNode, "Count", NULL),
				catchall,
				rlError("Failed to read <Count> from <Services>"));

			//WCF services cannot easily write arrays without an enclosing element,
			//so if don't find loose array items, we look for the enclosing element.
			const parTreeNode* epNode = servicesNode->FindChildWithNameIgnoreNs("S");
			if (epNode == NULL)
			{
				const parTreeNode* hostsNode = servicesNode->FindChildWithNameIgnoreNs("Hosts");
				if (hostsNode != NULL)
				{
					epNode = hostsNode->FindChildWithNameIgnoreNs("S");
				}
			}

			int numRead = 0;
			for (int i = 0;
				i < count && i < rlRos::MAX_SERVICE_HOSTS; 
				++i, epNode = epNode->GetSibling())
			{
				rverify(epNode,	catchall, rlError("Failed to find 'S' node %d in <Services>", i));

				const char* ep = rlHttpTaskHelper::ReadString(epNode, NULL, "ep");
				if (ep == NULL)
				{
					ep = rlHttpTaskHelper::ReadString(epNode, "ep", NULL);
				}
				rverify(ep,	catchall,rlError("Failed to find 'ep' from 'S' node %d in <Services>", i));

				const char* h = rlHttpTaskHelper::ReadString(epNode, NULL, "h");
				if (h == NULL)
				{
					h = rlHttpTaskHelper::ReadString(epNode, "h", NULL);
				}
				rverify(h, catchall, rlError("Failed to find 'h' from 'S' node %d in <Services>", i));

				safecpy(serviceHosts[i].Endpoint, ep);
				safecpy(serviceHosts[i].Host, h);
				++numRead;
			}

			*numServiceHosts = numRead;
		}
		rcatch(catchall)
		{
			//It's ok to fail here - don't fail the entire create ticket operation.
		}
	}

	return true;
}

bool
rlRosXmlCreateTicketResponseParser::ParseSslServices(
	const parTreeNode* node,
	rlRosSslService (&sslServices)[rlRos::MAX_SSL_SERVICES],
	unsigned* numSslServices)
{
	rlAssert(node);
	rlAssert(numSslServices);
	
	*numSslServices = 0;

	const parTreeNode* sslServicesNode = node->FindChildWithNameIgnoreNs("SslServices");
	if (sslServicesNode)
	{
		int numRead = 0;
		for (parTreeNode::ChildNodeIterator it = sslServicesNode->BeginChildren(); 
			it != sslServicesNode->EndChildren() && numRead < rlRos::MAX_SSL_SERVICES;
			++it)
		{
			const parTreeNode& node = **it;
			safecpy(sslServices[numRead].Endpoint, node.GetData());
			++numRead;
		}

		*numSslServices = numRead;
	}

	return true;
}

bool
rlRosXmlCreateTicketResponseParser::ParseRockstarAccount(
	const parTreeNode* node,
	rlScAccountInfo& rockstarAcct)
{
	rlAssert(node);

	rtry
	{
		rockstarAcct.Clear();

        const parTreeNode* rockstarAcctNode = node->FindChildWithNameIgnoreNs("RockstarAccount");

        if (rockstarAcctNode)
        {
			rcheck(rlHttpTaskHelper::ReadInt64(rockstarAcct.m_RockstarId, rockstarAcctNode, "RockstarId", NULL),
				catchall,
				rlError("Failed to read <RockstarId> from <RockstarAccount>"));

            rlHttpTaskHelper::ReadInt(rockstarAcct.m_Age, rockstarAcctNode, "Age", NULL);

            const char* temp;

            temp = rlHttpTaskHelper::ReadString(rockstarAcctNode, "AvatarUrl", NULL);
			if (temp) rockstarAcct.SetAvatarUrl(temp);

            temp = rlHttpTaskHelper::ReadString(rockstarAcctNode, "CountryCode", NULL);
			if (temp) safecpy(rockstarAcct.m_CountryCode, temp, sizeof(rockstarAcct.m_CountryCode));

            temp = rlHttpTaskHelper::ReadString(rockstarAcctNode, "Email", NULL);
			if (temp) safecpy(rockstarAcct.m_Email, temp, sizeof(rockstarAcct.m_Email));

            temp = rlHttpTaskHelper::ReadString(rockstarAcctNode, "LanguageCode", NULL);
			if (temp) safecpy(rockstarAcct.m_LanguageCode, temp, sizeof(rockstarAcct.m_LanguageCode));

            temp = rlHttpTaskHelper::ReadString(rockstarAcctNode, "Nickname", NULL);
			if (temp) safecpy(rockstarAcct.m_Nickname, temp, sizeof(rockstarAcct.m_Nickname));

            temp = rlHttpTaskHelper::ReadString(rockstarAcctNode, "ZipCode", NULL);
            if (temp) safecpy(rockstarAcct.m_ZipCode, temp, sizeof(rockstarAcct.m_ZipCode));
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

//////////////////////////////////////////////////////////////////////////
// rlRosXmlCreateTicketHttpTask
//////////////////////////////////////////////////////////////////////////
rlRosXmlCreateTicketHttpTask::rlRosXmlCreateTicketHttpTask()
	: m_Cred(NULL)
	, m_ResponseBuf(NULL)
{
	m_ServiceMethod[0] = '\0';
}

bool 
rlRosXmlCreateTicketHttpTask::Configure(
	const char* serviceMethod,
	const int localGamerIndex,
	rlRosCredentials* cred,
	datGrowBuffer* responseBuf)
{
	rtry
	{
		rverify(serviceMethod, catchall,);
		rverify(cred, catchall,);

		safecpy(m_ServiceMethod, serviceMethod);
		m_Cred = cred;
		m_ResponseBuf = responseBuf;

		rverify(rlRosHttpTask::Configure(localGamerIndex), 
			catchall, 
			rlTaskError("Failed to configure base class"));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
rlRosXmlCreateTicketHttpTask::AddRequestHeader(const char* name, const char* value)
{
	rlAssert(!IsActive());
	m_HttpRequest.AddRequestHeaderValue(name, value);
}

bool
rlRosXmlCreateTicketHttpTask::AddIntParam(const char* name, const int value)
{
	rlAssert(!IsActive());
	return AddIntParameter(name, value);
}

bool
rlRosXmlCreateTicketHttpTask::AddStringParam(const char* name, const char* value)
{
	rlAssert(!IsActive());
	return AddStringParameter(name, value, true);
}

bool
rlRosXmlCreateTicketHttpTask::AddUnsParam(const char* name, const unsigned value)
{
	rlAssert(!IsActive());
	return AddUnsParameter(name, value);
}

bool
rlRosXmlCreateTicketHttpTask::AddUns64Param(const char* name, const u64 value)
{
	rlAssert(!IsActive());
	return AddUnsParameter(name, value);
}

const char* 
rlRosXmlCreateTicketHttpTask::GetServiceMethod() const
{
	return m_ServiceMethod;
}

bool
rlRosXmlCreateTicketHttpTask::GetServicePath(char* svcPath, const unsigned bufSize) const
{
	return rlRosHttpTask::GetServicePath(svcPath, bufSize);
}

bool
rlRosXmlCreateTicketHttpTask::UseHttps() const
{
	//Auth always uses HTTPS, even with ROS encryption.
	return true;
}

rlRosSecurityFlags
rlRosXmlCreateTicketHttpTask::GetSecurityFlags() const
{
	//Don't use any security behind SSL.
	return UseHttps() ? RLROS_SECURITY_NONE : RLROS_SECURITY_DEFAULT;
}

bool
rlRosXmlCreateTicketHttpTask::UseFilter() const
{
	return rlRosHttpTask::UseFilter();
}

bool
rlRosXmlCreateTicketHttpTask::ProcessResponse(const char* response, int& resultCode)
{
	if (m_ResponseBuf != NULL)
	{
		unsigned responseLen = (unsigned)(strlen(response) + 1); // +1 for null terminator
		rlDebug("Copying response (%u bytes) into buffer", responseLen);
		rlVerify(m_ResponseBuf->AppendOrFail(response, responseLen));
	}

	return rlRosHttpTask::ProcessResponse(response, resultCode);
}

bool
rlRosXmlCreateTicketHttpTask::ProcessSuccess(
	const rlRosResult& /*result*/,
	const parTreeNode* node,
	int& /*resultCode*/)
{
	rtry
	{
		rverify(node, catchall,);

		rlRosCreateTicketResponse response;
		rverify(rlRosXmlCreateTicketResponseParser::Parse(node, &response), 
			catchall, 
			rlTaskError("Failed to parse CreateTicketResponse"));

		rverify(rlRosCreateTicketResponseHelper::ApplyCreateTicketResponse(response, GetLocalGamerIndex(), m_Cred), 
			catchall, 
			rlTaskError("Failed to apply CreateTicketResponse"));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* 
rlRosXmlCreateTicketHttpTask::GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
{
	//Look up the host name from the service we're calling.
	char servicePath[1024];
	const char* hn = NULL;
	if (GetServicePath(servicePath, sizeof(servicePath)))
	{
		hn = rlRos::GetServiceHost(servicePath, hostnameBuf, sizeofBuf);
	}

	if (!hn)
	{
		hn = formatf(hostnameBuf, sizeofBuf, "auth-%s", g_rlTitleId->m_RosTitleId.GetDomainName());
	}

	return hn;
}

///////////////////////////////////////////////////////////////////////////////
//  rlRosCredentialsChangingTask
///////////////////////////////////////////////////////////////////////////////
rlRosCredentialsChangingTask::rlRosCredentialsChangingTask()
{ 
}

bool
rlRosCredentialsChangingTask::Configure(const int localGamerIndex)
{
    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }

    if(!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        return false;
    }

    return true;    
}

bool
rlRosCredentialsChangingTask::UseHttps() const 
{
    // This needs to take place behind ssl to protect, among
    // other things, the negotiated session key, STS token, etc...
    return true;
}

rlRosSecurityFlags
rlRosCredentialsChangingTask::GetSecurityFlags() const
{
    // Don't use any security behind Ssl
    return UseHttps() ? RLROS_SECURITY_NONE : RLROS_SECURITY_DEFAULT;
}

bool 
rlRosCredentialsChangingTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                             const parTreeNode* node, 
                                             int& /*resultCode*/)
{
	rlRosCreateTicketResponse response;
	if (!rlRosXmlCreateTicketResponseParser::Parse(node, &response))
	{
		rlTaskError("Failed to parse create ticket response");
		return false;
	}

	rlRosCredentials cred;
	if (!rlRosCreateTicketResponseHelper::ApplyCreateTicketResponse(
		response,
		GetLocalGamerIndex(),
		&cred))
	{
		rlTaskError("Failed to apply create ticket response");
		return false;
	}

    if(!rlRos::SetCredentials(GetLocalGamerIndex(), &cred))
    {
        rlTaskError("Failed to set credentials");
        return false;
    }

    return true;
}

void
rlRosCredentialsChangingTask::Finish(const FinishType finishType, const int resultCode)
{
	this->rlRosHttpTask::Finish(finishType, resultCode);
}

//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketScsRequestTask
//////////////////////////////////////////////////////////////////////////
bool
rlRosCreateTicketScsRequestTask::Configure(
	const int localGamerIndex,
	const rlRosPlatformCredentials* platformCred,
	rlRosCredentials* cred,
	datGrowBuffer* responseBuf)
{
	rlTaskBase* task = NULL;

	rtry
	{
		rverify(cred, catchall, );

		// before connecting to SCS, validate our environment configuration is allowed
		rverify(rlValidateEnvironment(), catchall, )

		rlRosXmlCreateTicketHttpTask* xmlTask = rlGetTaskManager()->CreateTask<rlRosXmlCreateTicketHttpTask>();
		rverify(xmlTask, catchall, rlError("Failed to allocate rlRosXmlCreateTicketHttpTask"));
		task = xmlTask;

		rcheck(rlTaskBase::Configure(
			xmlTask,
			GetServiceMethod(),
			localGamerIndex,
			cred,
			responseBuf,
			&m_MyStatus),
			catchall,
			rlError("Failed to configure task"));

		rverify(WriteXmlContent(localGamerIndex, platformCred, xmlTask), catchall, );
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall, );

		return true;
	}
	rcatchall
	{
		if (task)
		{
			rlGetTaskManager()->DestroyTask(task);
			task = NULL;
		}
		return false;
	}
}

bool
rlRosCreateTicketScsRequestTask::IsCancelable() const
{
	return true;
}

void
rlRosCreateTicketScsRequestTask::DoCancel()
{
	if (m_MyStatus.Pending())
	{
		rlGetTaskManager()->CancelTask(&m_MyStatus);
	}
}

void
rlRosCreateTicketScsRequestTask::Finish(const FinishType finishType, const int resultCode)
{
	rlRosCreateTicketTask::Finish(finishType, resultCode);
}

void
rlRosCreateTicketScsRequestTask::Update(const unsigned timeStep)
{
	rlRosCreateTicketTask::Update(timeStep);

	if (this->WasCanceled())
	{
		//Wait until dependencies finish
		if (!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}
		return;
	}
	else if (!m_MyStatus.Pending())
	{
		if (m_MyStatus.Succeeded())
		{
			this->Finish(FINISH_SUCCEEDED);
		}
		else if (m_MyStatus.Failed())
		{
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
		}
	}
}

#if RSG_DURANGO

//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketXbl3Task
//////////////////////////////////////////////////////////////////////////
bool 
rlRosCreateTicketXbl3Task::WriteXmlContent(
	const int localGamerIndex,
	const rlRosPlatformCredentials* platformCred,
	rlRosXmlCreateTicketHttpTask* xmlTask)
{
	rtry
	{
		const rlRosCredentials& curCred = rlRos::GetCredentials(localGamerIndex);

		// Create the magic header that tells the XDK to retrieve the token and signature
		// and add it to the request automatically.
		wchar_t userHash[1024];
		rverify(g_rlXbl.GetPresenceManager()->GetXboxUserHash(localGamerIndex, userHash, COUNTOF(userHash)), catchall, );

		unsigned hashLen = static_cast<unsigned>(wcslen(userHash));
		char* szUserHash = (char*)Alloca(char, hashLen + 1);
		WideToAscii(szUserHash, (char16*)userHash, hashLen + 1);

		xmlTask->AddRequestHeader(netXblHttp::GetAuthActorHeaderName(), szUserHash);

		rverify(xmlTask->AddStringParam("ticket", curCred.GetTicket()), catchall, );
		rverify(xmlTask->AddStringParam("platformName", rlRosTitleId::GetPlatformName()), catchall, );
		rverify(xmlTask->AddUns64Param("xuid", platformCred->m_Xuid), catchall, );
		rverify(xmlTask->AddStringParam("gamertag", platformCred->m_Gamertag), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}

#endif //RSG_DURANGO

#if RSG_ORBIS
//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketNp3Task
//////////////////////////////////////////////////////////////////////////
bool
rlRosCreateTicketNp3Task::Configure(
	const int localGamerIndex, 
	const rlRosPlatformCredentials* platformCred, 
	rlRosCredentials* cred, 
	datGrowBuffer* responseBuf)
{
	rtry
	{
		rverify(platformCred->IsValid(), catchall,);

		m_LocalGamerIndex = localGamerIndex;
		m_PlatformCred = platformCred;
		m_Cred = cred;
		m_ResponseBuf = responseBuf;

		m_CreateTicketState = STATE_GET_AUTH_CODE;
		m_IssuerId = 0;
		m_RequestId = -1;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
rlRosCreateTicketNp3Task::IsCancelable() const
{
	return true;
}

void 
rlRosCreateTicketNp3Task::DoCancel()
{
	if (m_MyStatus.Pending())
	{
		switch (m_CreateTicketState)
		{
		case STATE_GET_AUTH_CODE:
			break;
		case STATE_GETTING_AUTH_CODE:
			g_rlNp.GetNpAuth().CancelAuthCodeRequest(
				m_LocalGamerIndex,
				m_RequestId,
				&m_MyStatus);
			break;
		case STATE_RECEIVING:
			rlGetTaskManager()->CancelTask(&m_MyStatus);
			break;
		}
	}
}

void
rlRosCreateTicketNp3Task::Update(const unsigned timeStep)
{
	rlRosCreateTicketTask::Update(timeStep);

	if (IsFinished())
	{
		return;
	}

	if (this->WasCanceled())
	{
		// Wait for cancel to finish.
		if (!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}
		return;
	}

	switch (m_CreateTicketState)
	{
	case STATE_GET_AUTH_CODE:
		m_RequestId = g_rlNp.GetNpAuth().GetAuthCode(m_LocalGamerIndex, m_AuthCode, &m_IssuerId, &m_MyStatus);
		if (m_RequestId >= 0)
		{
			m_CreateTicketState = STATE_GETTING_AUTH_CODE;
		}
		else
		{
			this->Finish(FINISH_FAILED, -1);
		}
		break;
	case STATE_GETTING_AUTH_CODE:
		g_rlNp.GetNpAuth().UpdateAuthCodeRequest(m_LocalGamerIndex, m_RequestId, &m_MyStatus);

		if (!m_MyStatus.Pending())
		{
			if (m_MyStatus.Succeeded())
			{
				InnerCreateTicketNp3Task* task = NULL;

				rtry
				{
					rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

					task->m_AuthCode = &m_AuthCode;
					task->m_IssuerId = m_IssuerId;

					rverify(rlTaskBase::Configure(
							task,		
							m_LocalGamerIndex,
							m_PlatformCred,
							m_Cred,
							m_ResponseBuf,
							&m_MyStatus), 
						catchall,);

					m_CreateTicketState = STATE_RECEIVING;
					rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
				}
				rcatchall
				{
					if(task)
					{
						rlGetTaskManager()->DestroyTask(task);
					}
					this->Finish(FINISH_FAILED, -1);
				}
			}
			else
			{
				this->Finish(FINISH_FAILED, -1);
			}
		}
		break;
	case STATE_RECEIVING:
		if (!m_MyStatus.Pending())
		{
            if (m_MyStatus.Succeeded())
			{
				this->Finish(FINISH_SUCCEEDED);
			}
			else if (m_MyStatus.Failed())
			{
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
		}
		break;
	}
}

rlRosCreateTicketNp3Task::InnerCreateTicketNp3Task::InnerCreateTicketNp3Task()
	: m_AuthCode(NULL)
	, m_IssuerId(0)
{
}

bool 
rlRosCreateTicketNp3Task::InnerCreateTicketNp3Task::WriteXmlContent(
	const int localGamerIndex,
	const rlRosPlatformCredentials* UNUSED_PARAM(platformCred),
	rlRosXmlCreateTicketHttpTask* xmlTask)
{
	rtry
	{
		const rlRosCredentials& curCred = rlRos::GetCredentials(localGamerIndex);
		rverify(xmlTask->AddStringParam("ticket", curCred.GetTicket()), catchall, );
		rverify(xmlTask->AddStringParam("platformName", rlRosTitleId::GetPlatformName()), catchall, );
		rverify(xmlTask->AddStringParam("authCode", m_AuthCode->code), catchall, );
		rverify(xmlTask->AddIntParam("issuerId", m_IssuerId), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketNp4Task
//////////////////////////////////////////////////////////////////////////
bool
rlRosCreateTicketNp4Task::Configure(
    const int localGamerIndex,
    const rlRosPlatformCredentials* platformCred,
    rlRosCredentials* cred,
    datGrowBuffer* responseBuf)
{
    rtry
    {
        rverify(platformCred->IsValid(), catchall,);

        m_LocalGamerIndex = localGamerIndex;
        m_PlatformCred = platformCred;
        m_Cred = cred;
        m_ResponseBuf = responseBuf;

        m_CreateTicketState = STATE_GET_AUTH_CODE;
        m_IssuerId = 0;
        m_RequestId = -1;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlRosCreateTicketNp4Task::IsCancelable() const
{
    return true;
}

void
rlRosCreateTicketNp4Task::DoCancel()
{
    if(m_MyStatus.Pending())
    {
        switch(m_CreateTicketState)
        {
            case STATE_GET_AUTH_CODE:
                break;
            case STATE_GETTING_AUTH_CODE:
                g_rlNp.GetNpAuth().CancelAuthCodeRequest(
                    m_LocalGamerIndex,
                    m_RequestId,
                    &m_MyStatus);
                break;
            case STATE_RECEIVING:
                rlGetTaskManager()->CancelTask(&m_MyStatus);
                break;
        }
    }
}

void
rlRosCreateTicketNp4Task::Update(const unsigned timeStep)
{
    rlRosCreateTicketTask::Update(timeStep);

    if(IsFinished())
    {
        return;
    }

    if(this->WasCanceled())
    {
        // Wait for cancel to finish.
        if(!m_MyStatus.Pending())
        {
            this->Finish(FINISH_CANCELED);
        }
        return;
    }

    switch(m_CreateTicketState)
    {
        case STATE_GET_AUTH_CODE:
            m_RequestId = g_rlNp.GetNpAuth().GetAuthCode(m_LocalGamerIndex, m_AuthCode, &m_IssuerId, &m_MyStatus);
            if(m_RequestId >= 0)
            {
                m_CreateTicketState = STATE_GETTING_AUTH_CODE;
            }
            else
            {
                this->Finish(FINISH_FAILED, -1);
            }
            break;
        case STATE_GETTING_AUTH_CODE:
            g_rlNp.GetNpAuth().UpdateAuthCodeRequest(m_LocalGamerIndex, m_RequestId, &m_MyStatus);

            if(!m_MyStatus.Pending())
            {
                if(m_MyStatus.Succeeded())
                {
                    InnerCreateTicketNp4Task* task = NULL;

                    rtry
                    {
                        rverify(rlGetTaskManager()->CreateTask(&task),catchall,);

                        task->m_AuthCode = &m_AuthCode;
                        task->m_IssuerId = m_IssuerId;

                        rverify(rlTaskBase::Configure(
                            task,
                            m_LocalGamerIndex,
                            m_PlatformCred,
                            m_Cred,
                            m_ResponseBuf,
                            &m_MyStatus),
                            catchall,);

                        m_CreateTicketState = STATE_RECEIVING;
                        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);
                    }
                    rcatchall
                    {
                        if(task)
                        {
                            rlGetTaskManager()->DestroyTask(task);
                        }
                        this->Finish(FINISH_FAILED, -1);
                    }
                }
                else
                {
                    this->Finish(FINISH_FAILED, -1);
                }
            }
            break;
        case STATE_RECEIVING:
            if(!m_MyStatus.Pending())
            {
                if(m_MyStatus.Succeeded())
                {
                    this->Finish(FINISH_SUCCEEDED);
                }
                else if(m_MyStatus.Failed())
                {
                    this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
                }
            }
            break;
    }
}

rlRosCreateTicketNp4Task::InnerCreateTicketNp4Task::InnerCreateTicketNp4Task()
    : m_AuthCode(NULL)
    , m_IssuerId(0)
{
}

bool
rlRosCreateTicketNp4Task::InnerCreateTicketNp4Task::WriteXmlContent(
    const int localGamerIndex,
    const rlRosPlatformCredentials* UNUSED_PARAM(platformCred),
    rlRosXmlCreateTicketHttpTask* xmlTask)
{
    rtry
    {
        const rlRosCredentials& curCred = rlRos::GetCredentials(localGamerIndex);
        rverify(xmlTask->AddStringParam("ticket", curCred.GetTicket()), catchall,);
        rverify(xmlTask->AddStringParam("platformName", rlRosTitleId::GetPlatformName()), catchall,);
        rverify(xmlTask->AddStringParam("authCode", m_AuthCode->code), catchall,);
        rverify(xmlTask->AddIntParam("issuerId", m_IssuerId), catchall,);
        return true;
    }
    rcatchall
    {
        return false;
    }
}

#endif //RSG_ORBIS

#if RLROS_SC_PLATFORM

//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketScSteam2Task
//////////////////////////////////////////////////////////////////////////
bool 
rlRosCreateTicketScSteam2Task::WriteXmlContent(
	const int localGamerIndex,
	const rlRosPlatformCredentials* platformCred,
	rlRosXmlCreateTicketHttpTask* xmlTask)
{
	rtry
	{
		const rlRosCredentials& curCred = rlRos::GetCredentials(localGamerIndex);
		rverify(xmlTask->AddStringParam("ticket", curCred.GetTicket()), catchall,);
		rverify(xmlTask->AddStringParam("platformName", rlRosTitleId::GetPlatformName()), catchall, );
		rverify(xmlTask->AddStringParam("email", platformCred->m_Email), catchall, );

#if __RGSC_DLL
		if (GetRgscConcreteInstance()->GetTitleId()->GetPlatform() == ITitleId::PLATFORM_STEAM)
		{
			bool notify = false;
			GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_REFRESH_STEAM_AUTH_TICKET, &notify);
			rverify(xmlTask->AddStringParam("steamAuthTicket", GetRgscConcreteInstance()->GetTitleId()->GetSteamAuthTicket()), catchall, );
			rverify(xmlTask->AddIntParam("steamAppId", GetRgscConcreteInstance()->GetTitleId()->GetSteamAppId()), catchall, );
			rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );
			rverify(xmlTask->AddStringParam("nickname", GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetName()), catchall, );
			rverify(xmlTask->AddStringParam("password", ""), catchall, );
		}
#elif __STEAM_BUILD
		g_rlPc.RefreshSteamAuthTicket(false);

		char nickname[RL_MAX_NAME_BUF_SIZE] = { '\0' };
		rverify(rlPresence::GetName(localGamerIndex, nickname), catchall, );

		rverify(xmlTask->AddStringParam("steamAuthTicket", g_rlPc.GetSteamAuthTicket()), catchall, );
		rverify(xmlTask->AddIntParam("steamAppId", g_rlTitleId->m_RosTitleId.GetSteamAppId()), catchall, );
		rverify(xmlTask->AddStringParam("nickname", nickname), catchall, );
		rverify(xmlTask->AddStringParam("password", ""), catchall, );
#endif
		return true;
	}
	rcatchall
	{
		return false;
	}
}

//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketScAuthTokenTask
//////////////////////////////////////////////////////////////////////////
bool 
rlRosCreateTicketScAuthTokenTask::WriteXmlContent(
	const int UNUSED_PARAM(localGamerIndex),
	const rlRosPlatformCredentials* platformCred,
	rlRosXmlCreateTicketHttpTask* xmlTask)
{
	rtry
	{
		rverify(xmlTask->AddStringParam("platformName", rlRosTitleId::GetPlatformName()), catchall, );
		rverify(xmlTask->AddStringParam("scAuthToken", platformCred->m_ScAuthToken), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}

//////////////////////////////////////////////////////////////////////////
// rlRosCreateTicketSc3Task
//////////////////////////////////////////////////////////////////////////
bool 
rlRosCreateTicketSc3Task::WriteXmlContent(
	const int localGamerIndex,
	const rlRosPlatformCredentials* platformCred,
	rlRosXmlCreateTicketHttpTask* xmlTask)
{
	rtry
	{
		const rlRosCredentials& curCred = rlRos::GetCredentials(localGamerIndex);
		rverify(xmlTask->AddStringParam("ticket", curCred.GetTicket()), catchall,);
		rverify(xmlTask->AddStringParam("platformName", rlRosTitleId::GetPlatformName()), catchall, );
		rverify(xmlTask->AddStringParam("nickname", ""), catchall, );
		rverify(xmlTask->AddStringParam("password", ""), catchall, );
		rverify(xmlTask->AddStringParam("email", platformCred->m_Email), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}

#endif //RLROS_SC_PLATFORM

#if RSG_PC && (!__NO_OUTPUT || defined(RSG_LEAN_CLIENT) || defined(MASTER_NO_SCUI))

//////////////////////////////////////////////////////////////////////////
// rlRosSignInWithoutScUiTask
//////////////////////////////////////////////////////////////////////////
rlRosSignInWithoutScUiTask::rlRosSignInWithoutScUiTask()
: m_State(STATE_INVALID)
{
	m_Email[0] = '\0';
	m_Password[0] = '\0';
}

bool
rlRosSignInWithoutScUiTask::Configure(const char* email, const char* password)
{
	rtry
	{
		rverify(email && (email[0] != '\0'), catchall, );
		rverify(password && (password[0] != '\0'), catchall, );

		m_State = STATE_INVALID;

		safecpy(m_Email, email);
		safecpy(m_Password, password);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
rlRosSignInWithoutScUiTask::Start()
{
    rlDebug2("Signing in without scui...");
	this->rlTaskBase::Start();
	m_State = STATE_CREATE_TICKET;
}

void
rlRosSignInWithoutScUiTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDebug2("Signing in without scui %s", FinishString(finishType));
    this->rlTaskBase::Finish(finishType, resultCode);
}
void
rlRosSignInWithoutScUiTask::DoCancel()
{
}

void
rlRosSignInWithoutScUiTask::Update(const unsigned timeStep)
{
    this->rlTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

    do
    {
        switch(m_State)
        {
		case STATE_CREATE_TICKET:
			if(this->CreateInnerCreateTicketTask())
			{
				m_State = STATE_CREATING_TICKET;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_CREATING_TICKET:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_CREATE_AUTH_TOKEN;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;
			
		case STATE_CREATE_AUTH_TOKEN:
			if(this->CreateAuthToken())
			{
				m_State = STATE_CREATING_AUTH_TOKEN;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;
			
		case STATE_CREATING_AUTH_TOKEN:
			if(m_MyStatus.Succeeded())
			{				
				if (rlSocialClub::SetLogin(RL_RGSC_GAMER_INDEX, m_Email, m_AuthToken))
				{
					if(rlRos::SetCredentials(RL_RGSC_GAMER_INDEX, &m_RosCred))
					{
						m_State = STATE_SUCCEEDED;
					}
					else
					{
						m_State = STATE_FAILED;
					}
				}
				else
				{
					m_State = STATE_FAILED;
				}
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

        case STATE_SUCCEEDED:
            this->Finish(FINISH_SUCCEEDED);
            break;

        case STATE_FAILED:
			rlAssertf(false, "Signing in without the scui using email '%s' failed", m_Email);
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
rlRosSignInWithoutScUiTask::CreateAuthToken()
{
	const char* audience = "Game";
	const int ttlMinutes = (30 * 24 * 60);

	return rlSocialClub::CreateScAuthToken(
		RL_RGSC_GAMER_INDEX, 
		m_RosCred.GetTicket(), 
		m_AuthToken, 
		sizeof(m_AuthToken), 
		audience, 
		ttlMinutes, 
		NULL, 
		&m_MyStatus);
}

bool 
rlRosSignInWithoutScUiTask::CreateInnerCreateTicketTask()
{
    bool success = false;

	InnerCreateTicketScTask* task = NULL;

    rtry
    {
		int localGamerIndex = RL_RGSC_GAMER_INDEX;
		rlRosPlatformCredentials* platformCred = NULL;
		datGrowBuffer* responseBuf = NULL;

		task = rlGetTaskManager()->CreateTask<InnerCreateTicketScTask>(m_Email, m_Password);
		rverify(task, catchall, );

        rverify(rlTaskBase::Configure(
				task,		
				localGamerIndex,
				platformCred,
				&m_RosCred,
				responseBuf,
				&m_MyStatus), 
			catchall,);

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        success = true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
    }

    return success;
}

rlRosSignInWithoutScUiTask::InnerCreateTicketScTask::InnerCreateTicketScTask(
	const char* email,
	const char* password)
{
	safecpy(m_Email, email);
	safecpy(m_Password, password);
}

bool 
rlRosSignInWithoutScUiTask::InnerCreateTicketScTask::WriteXmlContent(
	const int localGamerIndex,
	const rlRosPlatformCredentials* UNUSED_PARAM(platformCred),
	rlRosXmlCreateTicketHttpTask* xmlTask)
{
	rtry
	{
		const rlRosCredentials& curCred = rlRos::GetCredentials(localGamerIndex);
		rverify(xmlTask->AddStringParam("ticket", curCred.GetTicket()), catchall,);
		rverify(xmlTask->AddStringParam("platformName", rlRosTitleId::GetPlatformName()), catchall, );
		rverify(xmlTask->AddStringParam("nickname", ""), catchall, );
		rverify(xmlTask->AddStringParam("password", m_Password), catchall, );
		rverify(xmlTask->AddStringParam("email", m_Email), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}

#endif //RSG_PC && (!__NO_OUTPUT || defined(RSG_LEAN_CLIENT))
//////////////////////////////////////////////////////////////////////////
// rlRosGetGeolocationDataTask
//////////////////////////////////////////////////////////////////////////
rlRosGetGeolocationInfoTask::rlRosGetGeolocationInfoTask()
: m_GeoLocInfo(NULL)
{
}

bool
rlRosGetGeolocationInfoTask::Configure(const netIpAddress& ip,
                                        rlRosGeoLocInfo* geolocInfo)
{
    int localGamerIndex;
    const rlRosCredentials& cred = rlRos::GetFirstValidCredentials(&localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("No valid credentials");
        return false;
    }

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }
	
	u32 ipV4 = ip.ToV4().ToU32();

#if !__FINAL || __FINAL_LOGGING
	const char* geoLocFakeIp = NULL;
	if(PARAM_geoLocFakeIp.Get(geoLocFakeIp))
	{
		netIpV4Address fakeIp;
		if(fakeIp.FromString(geoLocFakeIp))
		{
			ipV4 = fakeIp.ToU32();		
		}
		else
		{
			rlTaskError("fake geoloc IP is invalid: %s", geoLocFakeIp);
		}
	}
#endif

	// TODO: NS - IPv4 specific - the netIpAddress can be an IPv6 address here
    if(!AddStringParameter("ticket", cred.GetTicket(), true)
        || !AddUnsParameter("ipAddrStr", ipV4))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_GeoLocInfo = geolocInfo;
    m_GeoLocInfo->Clear();

    return true;
}

const char* 
rlRosGetGeolocationInfoTask::GetServiceMethod() const
{
    return "GeoLocation.asmx/GetLocationInfoFromIP";
}

bool 
rlRosGetGeolocationInfoTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                          const parTreeNode* node, 
                                          int& /*resultCode*/)
{
    m_GeoLocInfo->Clear();

    rtry
    {
        rverify(node, catchall, );

        const parTreeNode* locInfoNode = node->FindChildWithName("LocInfo");
        rcheck(locInfoNode, catchall, 
            rlTaskError("Failed to find <LocInfo> element"));

        const parTreeNode* relaysListNode = node->FindChildWithName("RelaysList");
        rcheck(relaysListNode, catchall, 
            rlTaskError("Failed to find <RelaysList> element"));

        const parAttribute* attr;
        const parElement* el = &locInfoNode->GetElement();

        //Region code
        attr = el->FindAttribute("RegionCode");
        unsigned regionCode;
        if(attr)
        {
            regionCode = attr->FindIntValue();
        }
        else
        {
            rlTaskError("Failed to find \"RegionCode\" attribute");
            regionCode = 0;
        }

        //Longitude
        attr = el->FindAttribute("Longitude");
        float longitude;
        if(attr)
        {
            longitude = attr->FindFloatValue();
        }
        else
        {
            rlTaskError("Failed to find \"Longitude\" attribute");
            longitude = 0;
        }

        //Latitude
        attr = el->FindAttribute("Latitude");
        float latitude;
        if(attr)
        {
            latitude = attr->FindFloatValue();
        }
        else
        {
            rlTaskError("Failed to find \"Latitude\" attribute");
            latitude = 0;
        }

		// Country Code
        attr = el->FindAttribute("CountryCode");
        const char* countryCode;
        if(attr)
        {
            countryCode = attr->GetStringValue();
        }
        else
        {
            rlTaskError("Failed to find \"CountryCode\" attribute");
            countryCode = NULL;
        }

#if !__FINAL
		const char* cc = NULL;
		if (PARAM_geoLocCountry.Get(cc))
		{
			countryCode = cc;
		}
#endif

        el = &relaysListNode->GetElement();

        //Relay count
        attr = el->FindAttribute("Count");
        unsigned numRelays;
        if(attr)
        {
            numRelays = attr->FindIntValue();
        }
        else
        {
            rlTaskError("Failed to find \"Count\" attribute");
            numRelays = 0;
        }

		//IsSecure flag
		bool isSecure;
		attr = el->FindAttribute("IsSecure");
		if(!attr)
		{
            rlTaskError("Failed to find \"IsSecure\" attribute");
			isSecure = false;
		}
		else
		{
			isSecure = attr->FindBoolValue();
		}

        if(numRelays > rlRosGeoLocInfo::MAX_RELAY_ADDRESSES)
        {
            numRelays = rlRosGeoLocInfo::MAX_RELAY_ADDRESSES;
        }

        if(numRelays > 0)
        {
            const parTreeNode* relayNode = relaysListNode->GetChild();

            netSocketAddress* addrs = Alloca(netSocketAddress, numRelays);
            unsigned numAddrs = 0;


            for(int i = 0; i < (int)numRelays && relayNode; ++i, relayNode = relayNode->GetSibling())
            {
                el = &relayNode->GetElement();

                //Host address
                attr = el->FindAttribute("Host");
                if(!attr)
                {
                    rlTaskError("Failed to find \"Host\" attribute");
                    continue;
                }

                char addrStr[64];
                attr->GetStringRepr(addrStr, sizeof(addrStr), true);
                if(!rlVerifyf(addrs[numAddrs].Init(addrStr),
                            "Error parsing address:%s", addrStr))
                {
                    continue;
                }

                //Is SG
                bool isSg;
                attr = el->FindAttribute("IsXblSg");
                if(!attr)
                {
                    isSg = false;
                }
                else
                {
                    isSg = attr->FindBoolValue();
                }

                numAddrs += isSg ? 0 : 1;
            }

            if(0 == numAddrs)
            {
                rlTaskError("No relay addresses returned");
            }

            m_GeoLocInfo->Init(regionCode,
                                longitude,
                                latitude,
							    countryCode,
								isSecure,
                                addrs,
                                numAddrs);
        }
        else
        {
            m_GeoLocInfo->Init(regionCode,
                                longitude,
                                latitude,
							    countryCode,
								false,
                                NULL,
                                0);
        }

#if !__NO_OUTPUT
        rlTaskDebug("Discovered geoloc info:");
		rlTaskDebug("    IsSecure: %s", m_GeoLocInfo->m_IsSecure ? "true" : "false");
        rlTaskDebug("    Region code: %u", m_GeoLocInfo->m_RegionCode);
		rlTaskDebug("    Country Code: %s", m_GeoLocInfo->m_CountryCode);
        rlTaskDebug("    Long/Lat: %f/%f", m_GeoLocInfo->m_Longitude, m_GeoLocInfo->m_Latitude);
        for(int i = 0; i < (int)m_GeoLocInfo->m_NumRelayAddrs; ++i)
        {
            rlTaskDebug("    Relay server address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_GeoLocInfo->m_RelayAddrs[i]));
        }
#endif

        return true;
    }
    rcatchall
    {
        return false;
    }
}

//////////////////////////////////////////////////////////////////////////
// rlRosGetPresenceServerInfoTask
//////////////////////////////////////////////////////////////////////////
rlRosGetPresenceServerInfoTask::rlRosGetPresenceServerInfoTask()
: m_PresenceInfo(NULL)
{
}

bool
rlRosGetPresenceServerInfoTask::Configure(rlRosPresenceInfo* presenceInfo)
{
    int localGamerIndex;
    const rlRosCredentials& cred = rlRos::GetFirstValidCredentials(&localGamerIndex);
    if(!cred.IsValid())
    {
        rlTaskError("No valid credentials");
        return false;
    }

	if (!rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_PRESENCE_WRITE))
	{
		rlTaskError("No valid privilege");
		return false;
	}

    if(!rlRosHttpTask::Configure(localGamerIndex))
    {
        rlTaskError("Failed to configure base class");
        return false;
    }
	
    if(!AddStringParameter("ticket", cred.GetTicket(), true))
    {
        rlTaskError("Failed to add params");
        return false;
    }

    m_PresenceInfo = presenceInfo;
    m_PresenceInfo->Clear();

    return true;
}

const char* 
rlRosGetPresenceServerInfoTask::GetServiceMethod() const
{
    return "presence.asmx/GetPresenceServers";
}

bool 
rlRosGetPresenceServerInfoTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                          const parTreeNode* node, 
                                          int& /*resultCode*/)
{
    m_PresenceInfo->Clear();

    rtry
    {
        rverify(node, catchall, );

        const parTreeNode* resultNode = node->FindChildWithName("Result");
        rcheck(resultNode, catchall, 
            rlTaskError("Failed to find <Result> element"));

        const parAttribute* attr;
        const parElement* el = &resultNode->GetElement();

        //Result count
        attr = el->FindAttribute("Count");
        unsigned numResults;
        if(attr)
        {
            numResults = attr->FindIntValue();
        }
        else
        {
            rlTaskError("Failed to find \"Count\" attribute");
            numResults = 0;
        }

		//IsSecure flag
		bool isSecure;
		attr = el->FindAttribute("IsSecure");
		if(!attr)
		{
            rlTaskError("Failed to find \"IsSecure\" attribute");
			isSecure = false;
		}
		else
		{
			isSecure = attr->FindBoolValue();
		}

        if(numResults > rlRosPresenceInfo::MAX_PRESENCE_ADDRESSES)
        {
            numResults = rlRosPresenceInfo::MAX_PRESENCE_ADDRESSES;
        }

        if(numResults > 0)
        {
            const parTreeNode* result = resultNode->GetChild();

			rlRosPresenceInfo::Host addrs[rlRosPresenceInfo::MAX_PRESENCE_ADDRESSES];
            unsigned numAddrs = 0;

            for(int i = 0; i < (int)numResults && result; ++i, result = result->GetSibling())
            {
                el = &result->GetElement();

                //Host address
                attr = el->FindAttribute("Host");
                if(!attr)
                {
                    rlTaskError("Failed to find \"Host\" attribute");
                    continue;
                }

				// hostname + port
				char serverStr[MAX_HOSTNAME_BUF_SIZE + 32] = {0};
                attr->GetStringRepr(serverStr, sizeof(serverStr), true);

				char* colon = strrchr(serverStr, ':');
				if(!rlVerify(colon))
				{
					rlTaskError("Failed to find the ':' in the host string");
					continue;
				}

				unsigned port = 0;
				if(!rlVerify((sscanf(colon, ":%u", &port) == 1) && (port < 65536)) )
				{
					rlTaskError("Failed to find the port in the host string");
					continue;
				}
				*colon = '\0';

				char hostname[MAX_HOSTNAME_BUF_SIZE] = {0};
				safecpy(hostname, serverStr);
				addrs[numAddrs].Init(hostname, (u16)port);

                //Is SG
                bool isSg;
                attr = el->FindAttribute("IsXblSg");
                if(!attr)
                {
                    isSg = false;
                }
                else
                {
                    isSg = attr->FindBoolValue();
                }

                numAddrs += isSg ? 0 : 1;
            }

            if(0 == numAddrs)
            {
                rlTaskError("No presence server addresses returned");
            }

            m_PresenceInfo->Init(isSecure,
                                addrs,
                                numAddrs);
        }
        else
        {
            m_PresenceInfo->Init(false,
                                NULL,
                                0);
        }

#if !__NO_OUTPUT
        rlTaskDebug("Discovered presence server info:");
        rlTaskDebug("    IsSecure: %s", m_PresenceInfo->m_IsSecure ? "true" : "false");
        for(u32 i = 0; i < m_PresenceInfo->m_NumPresenceAddrs; ++i)
        {
            rlTaskDebug("    Presence server address: %s:%u", m_PresenceInfo->m_PresenceAddrs[i].m_Hostname, (u32)m_PresenceInfo->m_PresenceAddrs[i].m_Port);
        }
#endif

        return true;
    }
    rcatchall
    {
        return false;
    }
}

//////////////////////////////////////////////////////////////////////////
// rlRosGetNatDiscoveryServersTask
//////////////////////////////////////////////////////////////////////////
rlRosGetNatDiscoveryServersTask::rlRosGetNatDiscoveryServersTask()
: m_NatServers(NULL)
{
}

bool
rlRosGetNatDiscoveryServersTask::Configure(const netIpAddress& ip,
										   rlRosNatDiscoveryServers* natDiscoveryServers)
{
	int localGamerIndex;
	const rlRosCredentials& cred = rlRos::GetFirstValidCredentials(&localGamerIndex);
	if(!cred.IsValid())
	{
		rlTaskError("No valid credentials");
		return false;
	}

	if(!rlRosHttpTask::Configure(localGamerIndex))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	// TODO: NS - IPv4 specific - the netIpAddress can be an IPv6 address here
	if(!AddStringParameter("ticket", cred.GetTicket(), true)
		|| !AddUnsParameter("ipAddrStr", ip.ToV4().ToU32())
		|| !AddStringParameter("secure", "false", true)) // we use the non-DTLS relays for NAT type detection
	{
		rlTaskError("Failed to add params");
		return false;
	}

    m_NatServers = natDiscoveryServers;
    m_NatServers->Clear();

    return true;
}

const char* 
rlRosGetNatDiscoveryServersTask::GetServiceMethod() const
{
    return "GeoLocation.asmx/GetRelayServers";
}

bool 
rlRosGetNatDiscoveryServersTask::ProcessSuccess(const rlRosResult& /*result*/, 
                                          const parTreeNode* node, 
                                          int& /*resultCode*/)
{
    m_NatServers->Clear();

    rtry
    {
        rverify(node, catchall, );

        const parTreeNode* relaysListNode = node->FindChildWithName("RelaysList");
        rcheck(relaysListNode, catchall, 
            rlTaskError("Failed to find <RelaysList> element"));

        const parElement* el = &relaysListNode->GetElement();
		const parAttribute* attr = el->FindAttribute("Count");

        //Relay count
        unsigned numRelays;
        if(attr)
        {
            numRelays = attr->FindIntValue();
        }
        else
        {
            rlTaskError("Failed to find \"Count\" attribute");
            numRelays = 0;
        }

		//IsSecure flag
		bool isSecure;
		attr = el->FindAttribute("IsSecure");
		if(!attr)
		{
            rlTaskError("Failed to find \"IsSecure\" attribute");
			isSecure = false;
		}
		else
		{
			isSecure = attr->FindBoolValue();
		}

        if(numRelays > rlRosGeoLocInfo::MAX_RELAY_ADDRESSES)
        {
            numRelays = rlRosGeoLocInfo::MAX_RELAY_ADDRESSES;
        }

        if(numRelays > 0)
        {
            const parTreeNode* relayNode = relaysListNode->GetChild();

            rlRosNatDiscoveryServers::Host* addrs = Alloca(rlRosNatDiscoveryServers::Host, numRelays);
            unsigned numAddrs = 0;

            for(int i = 0; i < (int)numRelays && relayNode; ++i, relayNode = relayNode->GetSibling())
            {
                el = &relayNode->GetElement();

                //Host address
                attr = el->FindAttribute("Host");
                if(!attr)
                {
                    rlTaskError("Failed to find \"Host\" attribute");
                    continue;
                }

                char addrStr[64];
                attr->GetStringRepr(addrStr, sizeof(addrStr), true);
				netSocketAddress socketAddr;
                if(!rlVerifyf(socketAddr.Init(addrStr), "Error parsing address:%s", addrStr))
                {
                    continue;
                }
				addrs[numAddrs].Init(socketAddr);
				
                //Is SG
                bool isSg;
                attr = el->FindAttribute("IsXblSg");
                if(!attr)
                {
                    isSg = false;
                }
                else
                {
                    isSg = attr->FindBoolValue();
                }

                numAddrs += isSg ? 0 : 1;
            }

            if(0 == numAddrs)
            {
                rlTaskError("No NAT discovery server addresses returned");
            }

            m_NatServers->Init(addrs,
                                numAddrs);
        }
        else
        {
            m_NatServers->Init(NULL,
                                0);
        }

#if !__NO_OUTPUT
        rlTaskDebug("Discovered NAT discovery server info:");
        for(u32 i = 0; i < m_NatServers->m_NumAddrs; ++i)
        {
			rlTaskDebug("    NAT discovery server address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_NatServers->m_Addrs[i].m_Addr));
        }
#endif

        return true;
    }
    rcatchall
    {
        return false;
    }
}

//////////////////////////////////////////////////////////////////////////
// rlRosTicketExchangeTask
//////////////////////////////////////////////////////////////////////////
void rlRosTicketExchangeTask::HandleError(const rlRosResult& /*result*/, int& resultCode)
{
	resultCode = LN_SERVICES_UNEXPECTED_RESULT;
}

void rlRosTicketExchangeTask::ProcessError(const rlRosResult& result, const parTreeNode* /*node*/, int& resultCode)
{
	HandleError(result, resultCode);

	if (resultCode == LN_SERVICES_UNEXPECTED_RESULT)
	{
		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
	}
}

bool rlRosTicketExchangeTask::Configure(int titleId, rlRosCredentials* gameCredentials)
{
	rtry
	{
		// Verify resulting cred ptr is valid, and ensure we're online.
		rverify(gameCredentials, catchall, );
		rverify(rlRos::IsOnline(RL_RGSC_GAMER_INDEX), catchall, );

		m_GameCredentials = gameCredentials;
		m_TitleId = titleId;

		rverify(rlRosHttpTask::Configure(RL_RGSC_GAMER_INDEX), catchall, rlTaskError("Failed to configure base class"));
		const rlRosCredentials& curCred = rlRos::GetCredentials(RL_RGSC_GAMER_INDEX);
		rverify(curCred.IsValid(), catchall, );

		rverify(AddStringParameter("ticket", curCred.GetTicket(), true), catchall, rlTaskError("Failed to add ticket param"));
		rverify(AddIntParameter("titleId", m_TitleId, true), catchall, rlTaskError("Failed to add titleId param"));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool rlRosTicketExchangeTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& /*resultCode*/)
{
	rtry
	{
		rlRosCreateTicketResponse response;
		rverify(rlRosXmlCreateTicketResponseParser::Parse(node, &response),
			catchall,
			rlTaskError("Failed to parse CreateTicketResponse"));

		rverify(rlRosCreateTicketResponseHelper::ApplyCreateTicketResponse(response, RL_RGSC_GAMER_INDEX, m_GameCredentials),
			catchall,
			rlTaskError("Failed to apply CreateTicketResponse"));

		return true;
	}
	rcatchall
	{
		m_GameCredentials->Clear();
		return false;
	}
}

const char* rlRosTicketExchangeTask::GetServiceMethod() const
{
	return "auth.asmx/ExchangeTicket"; 
}

} //namespace rage
