// 
// rline/rlroscredtasks.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROSCREDTASKS_H
#define RLINE_RLROSCREDTASKS_H

#include "rlros.h"
#include "rlroshttptask.h"
#include "data/rson.h"

namespace rage
{

class netStatus;
class rlRosGeoLocInfo;
class rlRosPresenceInfo;
class rlRosNatDiscoveryServers;

//PURPOSE
//	Used to hold results of parsing either a CreateTicketResponse (from XML)
//  or a CreateTicketResult (from JSON). Both contain the same information.
struct rlRosCreateTicketResponse
{
	int m_AccountId;
	char m_CloudKeyBase64[DAT_BASE64_MAX_ENCODED_SIZE(rlRosCredentials::CLOUD_KEY_SIZE)];
	int m_DisabledSecurityFlags;
	bool m_IsSubAccount;
	u64 m_PosixTime;
	char m_PrivilegesCsv[(RLROS_NUM_PRIVILEGEID * 4) + 1]; //Assume max of 4 chars per priv, w/comma.
	rlRosCredentials::PrivilegeInfo m_PrivilegeInfos[RLROS_NUM_PRIVILEGEID];
	char m_PublicIp[netIpAddress::MAX_STRING_BUF_SIZE];
	int m_Region;
	rlScAccountInfo m_RockstarAcct;
	int m_SecsUntilExpiration;
	unsigned m_NumServiceHosts;
	rlRosServiceHost m_ServiceHosts[rlRos::MAX_SERVICE_HOSTS];
	unsigned m_NumSslServices;
	rlRosSslService m_SslServices[rlRos::MAX_SSL_SERVICES];
	s64 m_SessionId;
	char m_SessionKeyBase64[DAT_BASE64_MAX_ENCODED_SIZE(rlRosCredentials::SESSION_KEY_SIZE)];
	char m_SessionTicket[RLROS_MAX_SESSION_TICKET_SIZE];
	char m_Ticket[RLROS_MAX_TICKET_SIZE];

#if RSG_ORBIS
    bool m_UseNpAccountIds;
#endif

	rlRosCreateTicketResponse();
	
	//PURPOSE
	//	Sets all fields to their default values.
	void Clear();
	
	//PURPOSE
	//	Prints the response to debug output.
	void DebugPrint() const;
};

//PURPOSE
// Helper class working with rlRosCreateTicketResponses.
class rlRosCreateTicketResponseHelper
{
public:
	//PURPOSE
	//	Applies a parsed CreateTicketResponse to the given local gamer and credentials.
	static bool ApplyCreateTicketResponse(
		const rlRosCreateTicketResponse& response,
		const int localGamerIndex,
		rlRosCredentials* cred);
};

//PURPOSE
// Helper class for parsing an rlRosCreateTicketResponse from XML.
// XML CreateTicketResponses are returned by gameservices/auth.asmx web methods,
// and any other .asmx service that might change the client's ticket.
class rlRosXmlCreateTicketResponseParser
{
public:
	//PURPOSE
	//	Parses a CreateTicketResponse from XML.
	static bool Parse(const parTreeNode* node, rlRosCreateTicketResponse* response);

private:
	static bool ParsePrivilegeInfos(
		const parTreeNode* node,
		rlRosCredentials::PrivilegeInfo(&privilegeInfos)[RLROS_NUM_PRIVILEGEID]);

	static bool ParseServiceHosts(
		const parTreeNode* node,
		rlRosServiceHost(&serviceHosts)[rlRos::MAX_SERVICE_HOSTS],
		unsigned* numServiceHosts);

	static bool ParseSslServices(
		const parTreeNode* node,
		rlRosSslService(&sslServices)[rlRos::MAX_SSL_SERVICES],
		unsigned* numSslServices);

	static bool ParseRockstarAccount(const parTreeNode* node, rlScAccountInfo& rockstarAcct);
};

////////////////////////////////////////////////////////////////////////////////
// An rlRosHttpTask that expects the request to be form params, and the response
// to be XML CreateTicketResponse (see: \\rage\ros\rage\src\auth\responses.cs).
// Upon success, the response is automatically parsed and applied.
////////////////////////////////////////////////////////////////////////////////
class rlRosXmlCreateTicketHttpTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosXmlCreateTicketHttpTask);
	RL_TASK_USE_CHANNEL(rline_ros);

	rlRosXmlCreateTicketHttpTask();
	virtual ~rlRosXmlCreateTicketHttpTask() override { };

	//PURPOSE
	//	Configures the task.
	//PARAMS
	//	serviceMethod: Service and web method part of the the URL (ex. "auth.asmx/CreateTicketSc3")
	//	localGamerIndex: Local gamer to apply to response to.
	//	cred: Credentials object to apply response to.
	//	responseBuf: (optional) If non-null, the response will be copied into the buffer.
	bool Configure(
		const char* serviceMethod,
		const int localGamerIndex,
		rlRosCredentials* cred,
		datGrowBuffer* responseBuf);

	//PURPOSE
	//	Adds a header to the request. This should be called after Configure()
	//	but before the task has been started.
	void AddRequestHeader(const char* name, const char* value);

	//PURPOSE
	//	Methods to add form parameters to the request. These should be called after Configure()
	// but before the task has been started.
	bool AddIntParam(const char* name, const int value);
	bool AddStringParam(const char* name, const char* value);
	bool AddUnsParam(const char* name, const unsigned value);
	bool AddUns64Param(const char* name, const u64 value);

protected:
	virtual rlRosSecurityFlags GetSecurityFlags() const override;
	virtual bool GetServicePath(char* svcPath, const unsigned bufSize) const override;
	virtual const char* GetServiceMethod() const override;
	virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const override;
	virtual bool ProcessResponse(const char* response, int& resultCode) override;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode) override;
	virtual bool UseFilter() const override;
	virtual bool UseHttps() const override;

	static const int MAX_SERVICE_METHOD_LEN = 128;
	char m_ServiceMethod[MAX_SERVICE_METHOD_LEN];
	rlRosCredentials* m_Cred;
	datGrowBuffer* m_ResponseBuf;
};

//////////////////////////////////////////////////////////////////////////
// Base class for tasks that create or refresh tickets.
//////////////////////////////////////////////////////////////////////////
class rlRosCreateTicketTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlRosCreateTicketTask);
	RL_TASK_USE_CHANNEL(rline_ros);

	virtual ~rlRosCreateTicketTask() override { };

	//PURPOSE
	//	Configures the task.
	//PARAMS
	//	localGamerIndex: Local gamer to apply to response to.
	//	platformCred: Platform credentials for gamer making request.
	//	cred: Credentials object to apply response to.
	//	responseBuf: (optional) If non-null, the response will be copied into the buffer.
	virtual bool Configure(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosCredentials* cred,
		datGrowBuffer* responseBuf) = 0;
};

//////////////////////////////////////////////////////////////////////////
// Base class for tasks that call SCS web services that return a ticket.
//////////////////////////////////////////////////////////////////////////
class rlRosCreateTicketScsRequestTask : public rlRosCreateTicketTask
{
public:
	RL_TASK_DECL(rlRosCreateTicketTask);
	RL_TASK_USE_CHANNEL(rline_ros);

	virtual ~rlRosCreateTicketScsRequestTask() override { };

	virtual bool Configure(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosCredentials* cred,
		datGrowBuffer* responseBuf) override;

	virtual void Finish(const FinishType finishType, const int resultCode = 0) override;
	virtual bool IsCancelable() const override;

protected:
	virtual void DoCancel() override;
	virtual void Update(const unsigned timeStep) override;

	// Specifies service method to call when request is expected to contain form
	// parameters, and the response is expected to be an XML CreateTicketResponse
	// (see: //rage/ros/rage/src/auth/Responses.cs).
	// It should look like "{webservice}/{webmethod}. (ex. "auth.asmx/CreateTicketSc3")
	virtual const char* GetServiceMethod() const = 0;

	// Callback for derived class to write XML request content (via xmlTask->Add{Type}() methods)
	// and/or set request headers (via xmlTask->AddRequestHeader()).
	virtual bool WriteXmlContent(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosXmlCreateTicketHttpTask* xmlTask) = 0;

private:
	netStatus m_MyStatus;
};

#if RSG_DURANGO

class rlRosCreateTicketXbl3Task : public rlRosCreateTicketScsRequestTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosCreateTicketXbl3Task);

	virtual ~rlRosCreateTicketXbl3Task() override { }

protected:
	virtual const char* GetServiceMethod() const override { return "auth.asmx/CreateTicketXbl3"; }

	virtual bool WriteXmlContent(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosXmlCreateTicketHttpTask* xmlTask) override;
};

//////////////////////////////////////////////////////////////////////////
// Identical to rlRosCreateTicketXbl3Task except that it calls a different
// web method that allows impersonating an XBL user.
//////////////////////////////////////////////////////////////////////////
class rlRosImpersonateCreateTicketXblTask : public rlRosCreateTicketXbl3Task
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosImpersonateCreateTicketXblTask);

	virtual ~rlRosImpersonateCreateTicketXblTask() override { }

protected:
	virtual const char* GetServiceMethod() const override { return "auth.asmx/ImpersonateCreateTicketXbl"; }
};

#endif // RSG_DURANGO

#if RSG_ORBIS

class rlRosCreateTicketNp3Task : public rlRosCreateTicketTask
{
public:
	virtual ~rlRosCreateTicketNp3Task() override { };

	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosCreateTicketNp3Task);

	virtual bool Configure(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosCredentials* cred,
		datGrowBuffer* responseBuf) override;

	virtual bool IsCancelable() const override;

protected:
	virtual void DoCancel() override;
	virtual void Update(const unsigned timeStep) override;

	//Cache config params because we don't start the SCS HTTP task until NP auth completes.
	int m_LocalGamerIndex;
	const rlRosPlatformCredentials* m_PlatformCred;
	rlRosCredentials* m_Cred;
	datGrowBuffer* m_ResponseBuf;

	//Tracks state of getting NP auth code and issuer ID from PSN.
	enum CreateTicketState
	{
		STATE_GET_AUTH_CODE,
		STATE_GETTING_AUTH_CODE,
		STATE_RECEIVING
	};

	CreateTicketState m_CreateTicketState;
	rlSceNpAuthorizationCode m_AuthCode;
	int m_IssuerId;
	int m_RequestId;

private:	   
	// Used to call SCS CreateTicket service after we've received auth info from PSN.
	class InnerCreateTicketNp3Task : public rlRosCreateTicketScsRequestTask
	{
		typedef rlRosCreateTicketScsRequestTask Base;

	public:
		RL_TASK_DECL(InnerCreateTicketNp3Task);
		RL_TASK_USE_CHANNEL(rline_ros);

		InnerCreateTicketNp3Task();
		virtual ~InnerCreateTicketNp3Task() override { }

		const rlSceNpAuthorizationCode* m_AuthCode;
		int m_IssuerId;

	protected:
		virtual const char* GetServiceMethod() const override { return "auth.asmx/CreateTicketNp3"; }

		virtual bool WriteXmlContent(
			const int localGamerIndex,
			const rlRosPlatformCredentials* platformCred,
			rlRosXmlCreateTicketHttpTask* xmlTask) override;
	};

	netStatus m_MyStatus;
};

class rlRosCreateTicketNp4Task : public rlRosCreateTicketTask
{
public:
	virtual ~rlRosCreateTicketNp4Task() override { };

	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosCreateTicketNp4Task);

	virtual bool Configure(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosCredentials* cred,
		datGrowBuffer* responseBuf) override;

	virtual bool IsCancelable() const override;

protected:
	virtual void DoCancel() override;
	virtual void Update(const unsigned timeStep) override;

	//Cache config params because we don't start the SCS HTTP task until NP auth completes.
	int m_LocalGamerIndex;
	const rlRosPlatformCredentials* m_PlatformCred;
	rlRosCredentials* m_Cred;
	datGrowBuffer* m_ResponseBuf;

	//Tracks state of getting NP auth code and issuer ID from PSN.
	enum CreateTicketState
	{
		STATE_GET_AUTH_CODE,
		STATE_GETTING_AUTH_CODE,
		STATE_RECEIVING
	};

	CreateTicketState m_CreateTicketState;
	rlSceNpAuthorizationCode m_AuthCode;
	int m_IssuerId;
	int m_RequestId;

private:	   
	// Used to call SCS CreateTicket service after we've received auth info from PSN.
	class InnerCreateTicketNp4Task : public rlRosCreateTicketScsRequestTask
	{
		typedef rlRosCreateTicketScsRequestTask Base;

	public:

		RL_TASK_DECL(InnerCreateTicketNp4Task);

		RL_TASK_USE_CHANNEL(rline_ros);

        InnerCreateTicketNp4Task();
		virtual ~InnerCreateTicketNp4Task() override { }

		const rlSceNpAuthorizationCode* m_AuthCode;
		int m_IssuerId;

	protected:

        virtual const char* GetServiceMethod() const override { return "auth.asmx/CreateTicketNp4"; }

		virtual bool WriteXmlContent(
			const int localGamerIndex,
			const rlRosPlatformCredentials* platformCred,
			rlRosXmlCreateTicketHttpTask* xmlTask) override;
	};

	netStatus m_MyStatus;
};

#endif // RSG_ORBIS

#if RLROS_SC_PLATFORM

class rlRosCreateTicketScSteam2Task : public rlRosCreateTicketScsRequestTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosCreateTicketScSteam2Task);

	virtual ~rlRosCreateTicketScSteam2Task() override { }

protected:
	virtual const char* GetServiceMethod() const override { return "auth.asmx/CreateTicketScSteam2"; }

	virtual bool WriteXmlContent(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosXmlCreateTicketHttpTask* xmlTask) override;
};

class rlRosCreateTicketScAuthTokenTask : public rlRosCreateTicketScsRequestTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosCreateTicketScAuthTokenTask);

	virtual ~rlRosCreateTicketScAuthTokenTask() override { }

protected:
	virtual const char* GetServiceMethod() const override { return "auth.asmx/CreateTicketScAuthToken"; }

	virtual bool WriteXmlContent(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosXmlCreateTicketHttpTask* xmlTask) override;
};

class rlRosCreateTicketSc3Task : public rlRosCreateTicketScsRequestTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosCreateTicketSc3Task);

	virtual ~rlRosCreateTicketSc3Task() override { }

protected:
	virtual const char* GetServiceMethod() const override { return "auth.asmx/CreateTicketSc3"; }

	virtual bool WriteXmlContent(
		const int localGamerIndex,
		const rlRosPlatformCredentials* platformCred,
		rlRosXmlCreateTicketHttpTask* xmlTask) override;
};

#endif // RLROS_SC_PLATFORM

//PURPOSE
//  Base class for any task that calls a service that supplies a ticket
//  AND receives a CreateTicketResponse containing a new ticket.
//  For example, SC account creation and linking take a ticket and 
//  return an updated ticket containing the Rockstar ID.
class rlRosCredentialsChangingTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlRosCredentialsChangingTask);

    rlRosCredentialsChangingTask();
    virtual ~rlRosCredentialsChangingTask() {}

    bool Configure(const int localGamerIndex);

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

protected:
	virtual bool UseHttps() const;
	virtual rlRosSecurityFlags GetSecurityFlags() const;

    //PURPOSE
    //  Extracts the ticket and calls rlRos::SetCredentials.
    //  If this is not desirable, derived tasks should override.
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
};

#if RSG_PC && (!__NO_OUTPUT || defined(RSG_LEAN_CLIENT) || defined(MASTER_NO_SCUI))

class rlRosSignInWithoutScUiTask : public rlTaskBase
{
public:
	RL_TASK_DECL(rlRosSignInWithoutScUiTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_CREATE_TICKET,
		STATE_CREATING_TICKET,
		STATE_CREATE_AUTH_TOKEN,
		STATE_CREATING_AUTH_TOKEN,
		STATE_SUCCEEDED,
		STATE_FAILED
	};

	rlRosSignInWithoutScUiTask();
	virtual ~rlRosSignInWithoutScUiTask() { };

	bool Configure(const char* email, const char* password);

	virtual void Start() override;
	virtual void Finish(const FinishType finishType, const int resultCode = 0) override;
	virtual void Update(const unsigned timeStep) override;
	virtual bool IsCancelable() const override { return true; }
	virtual void DoCancel() override;

private:
	class InnerCreateTicketScTask : public rlRosCreateTicketScsRequestTask
	{
	public:
		RL_TASK_DECL(InnerCreateTicketScTask);
		RL_TASK_USE_CHANNEL(rline_ros);

		InnerCreateTicketScTask(const char* email, const char* password);
		virtual ~InnerCreateTicketScTask() override { }

	protected:
		virtual const char* GetServiceMethod() const override { return "auth.asmx/CreateTicketSc3"; }

		virtual bool WriteXmlContent(
			const int localGamerIndex,
			const rlRosPlatformCredentials* platformCred,
			rlRosXmlCreateTicketHttpTask* xmlTask) override;

	private:
		char m_Email[RLSC_MAX_EMAIL_CHARS];
		char m_Password[RLSC_MAX_PASSWORD_CHARS];
	};

	bool CreateAuthToken();
	bool CreateInnerCreateTicketTask();

	int m_State;
	netStatus m_MyStatus;
	rlRosCredentials m_RosCred;

	char m_Email[RLSC_MAX_EMAIL_CHARS];
	char m_Password[RLSC_MAX_PASSWORD_CHARS];
	char m_AuthToken[RLSC_MAX_SCAUTHTOKEN_CHARS];
};

#endif // RSG_PC && (!__NO_OUTPUT || defined(RSG_LEAN_CLIENT))


class rlRosGetGeolocationInfoTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlRosGetGeolocationInfoTask);

    rlRosGetGeolocationInfoTask();
    virtual ~rlRosGetGeolocationInfoTask() {}

    bool Configure(const netIpAddress& ip,
                    rlRosGeoLocInfo* geoLocInfo);

protected:
    virtual const char* GetServiceMethod() const;

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

private:

    rlRosGeoLocInfo* m_GeoLocInfo;
};

class rlRosGetPresenceServerInfoTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlRosGetPresenceServerInfoTask);

    rlRosGetPresenceServerInfoTask();
    virtual ~rlRosGetPresenceServerInfoTask() {}

    bool Configure(rlRosPresenceInfo* presenceInfo);

protected:
    virtual const char* GetServiceMethod() const;

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

private:

    rlRosPresenceInfo* m_PresenceInfo;
};

class rlRosGetNatDiscoveryServersTask : public rlRosHttpTask
{
public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlRosGetNatDiscoveryServersTask);

    rlRosGetNatDiscoveryServersTask();
    virtual ~rlRosGetNatDiscoveryServersTask() {}

    bool Configure(const netIpAddress& ip,
				   rlRosNatDiscoveryServers* natDiscoveryServers);

protected:
    virtual const char* GetServiceMethod() const;

    virtual bool ProcessSuccess(const rlRosResult& result,
                                const parTreeNode* node,
                                int& resultCode);

private:

    rlRosNatDiscoveryServers* m_NatServers;
};

class rlRosTicketExchangeTask : public rage::rlRosHttpTask
{
public:

	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosTicketExchangeTask);

	enum lnServicesErrorCode
	{
		LN_SERVICES_UNEXPECTED_RESULT = -1
	};

	rlRosTicketExchangeTask() {}
	virtual ~rlRosTicketExchangeTask() {}

	bool Configure(int titleId, rlRosCredentials* gameCredentials);

	//Version for non-derived tasks.  Does same this as overridden ProcessError.
	static void HandleError(const rage::rlRosResult& result, int& resultCode);

protected:
	virtual void ProcessError(const rage::rlRosResult& result, const rage::parTreeNode* node, int& resultCode);
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual const char* GetServiceMethod() const;

private:

	int m_TitleId;
	rlRosCredentials* m_GameCredentials;
};
} //namespace rage

#endif
