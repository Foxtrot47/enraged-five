// 
// rline/rlrosserviceaccesstokenmgr.h 
// 
// Copyright (C) 2014 Rockstar Games.  All Rights Reserved. 
// 
#ifndef RLINE_RLROSSERVICEACCESSTOKENSTORE_H
#define RLINE_RLROSSERVICEACCESSTOKENSTORE_H


#include "atl/map.h"
#include "net/status.h"

#include "rlroscommon.h"
#include "rlroshttptask.h"

namespace rage
{

class rlRosServiceAccessTokenStore
{
public:
	rlRosServiceAccessTokenStore();
	~rlRosServiceAccessTokenStore();

	bool Init();
	void Update(const unsigned timeStepMs);
	void Shutdown();
	void ClearAccessRequest();

	//Request a token for a given service
	bool RequestAccessToken(const int localGamerIndex);

	//Invalidate and Request a token for a given service
	NOTFINAL_ONLY( void InvalidateOrRequestToken(const int localGamerIndex); )

	//Get the access token for the given service.  
	const rlRosServiceAccessInfo& GetAccessInfoForService(const int localGamerIndex) const;

	bool IsRequestPending(const int localGamerIndex, int& retryCount, int& retryTimeMs) const;
	bool IsRequestFailed(const int localGamerIndex) const;

protected:

	// Event handler for processing Ros
	void OnRosEvent(const rlRosEvent&);

	class ServiceAccessRequest
	{
	public:
		const static int DEFAULT_TOKEN_TTL_MS = 24 * 60 * 60 * 1000;  //24 hours
		ServiceAccessRequest() 
			: m_localGamerIndex(-1)
			, m_retryCount(0)
			, m_requestRetryTimeMs(0)
			, m_routeTaskRetryTimeSecs(0)
			, m_expirationTimeMs(DEFAULT_TOKEN_TTL_MS)
		{
		}

		void SetGamerIndex(const int index);

		int GetGamerIndex() const { return m_localGamerIndex; }
		bool Pending() const { return m_requestStatus.Pending(); }
		bool Failed() const { return m_requestStatus.Failed() || m_requestStatus.Canceled(); }
		bool SucceededAndValid() const { return m_requestStatus.Succeeded() && m_serviceInfo.IsValid(); }

		bool RequestToken();
		void Cancel();
		void Invalidate();
		void Update(const unsigned timeStepMs);
		void InvalidateOrRequestToken();
		int m_localGamerIndex;
		rlRosServiceAccessInfo m_serviceInfo;
		int m_routeTaskRetryTimeSecs;
		netStatus m_routeTaskStatus;

		unsigned int m_retryCount;
		unsigned int m_requestRetryTimeMs;
		int m_expirationTimeMs;
		netStatus m_requestStatus;
	};

	rlRos::Delegate m_ROSDlgt;
	ServiceAccessRequest m_accessRequest;
};
	
} //namespace rage

#endif // RLINE_RLROSSERVICEACCESSTOKENSTORE_H