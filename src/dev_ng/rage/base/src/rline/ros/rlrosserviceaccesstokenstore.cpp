//
//	rlrosserviceaccesstokenclient.cpp
//
// Copyright (C) 2014 Rockstar Games.  All Rights Reserved. 
//
#include "diag/seh.h"
#include "system/param.h"

#include "rlros.h"
#include "rlrosserviceaccesstokenstore.h"
#include "rlroshttptask.h"
#include "rline/rltitleid.h"
#include "parser/treenode.h"

namespace rage {

RAGE_DECLARE_CHANNEL(rline);
RAGE_DEFINE_SUBCHANNEL(rline, servicetoken);
#undef __rage_channel
#define __rage_channel rline_servicetoken

///////////////////////////////////////////////////////////////////////////
// rlRosConductorRouteTask
//////////////////////////////////////////////////////////////////////////
PARAM(rlConductorForceGeo, "Specifies the country code to use to force the conductor to route to a specific geo");
PARAM(rlServiceTokenForceTTL, "Force service token ttl");
class rlRosConductorRouteTask : public rlRosHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_servicetoken);
	RL_TASK_DECL(rlRosConductorRouteTask);

	enum SuccessCode
	{
		SUCCESSCODE_ROUTE_SUCCESSFUL,
		SUCCESSCODE_DELAY_GEO_MOVE,
		SUCCESSCODE_DELAY_NO_SERVER_AVAILABLE,
		SUCCESSCODE_DELAY_NO_SERVER_INGRESS,
		SUCCESCODE_INVALID = -1
	};

	bool Configure( const int localGamerIndex, rlRosServiceAccessInfo* serverInfo, int* retrySecs );
protected:

	virtual bool UseHttps() const { return true; }

	virtual rlRosSecurityFlags GetSecurityFlags() const { return RLROS_SECURITY_NONE; }

	virtual bool UseFilter() const { return false; }
	
	//Returns the host name part of the URL to be used.
	//E.g. http://{hostname}/path/to/resource?arg1=0&arg2=1
	virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
	{
		//Look up the host name from the service we're calling
		char servicePath[1024];
		const char* hn = NULL;
		if(GetServicePath(servicePath, sizeof(servicePath)))
		{
			hn = rlRos::GetServiceHost(servicePath, hostnameBuf, sizeofBuf);
		}

		if(hn == NULL)
		{
			hn = formatf(hostnameBuf, sizeofBuf, "conductor-%s.%s", g_rlTitleId->m_RosTitleId.GetEnvironmentName(), RLROS_BASE_DOMAIN_NAME);
		}
		
		return hn;
	}

	virtual const char* GetServiceMethod() const
	{
		return "Conductor.asmx/Route";
	}

	//Called when we get an success response.  Before this is called
	//resultCode is already set to 0, so this only needs to be overridden
	//if you need to process the node, or set resultCode to something else.
	//Overrides should return false if they encounter an error.
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	rlRosServiceAccessInfo* m_serverInfo;
	int* m_retrySecs;
};


bool rlRosConductorRouteTask::Configure( const int localGamerIndex, rlRosServiceAccessInfo* serverInfo, int* retrySecs )
{
	rtry
	{
		rverify(serverInfo, catchall, rlTaskError("'serverInfo' param must be non-null"));
		rverify(retrySecs, catchall, rlTaskError("'retrySes' param must be non-null"));

		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		rcheck(cred.IsValid(), catchall, rlTaskError("No valid credentials for index %d", localGamerIndex));

		rcheck(rlRosHttpTask::Configure(localGamerIndex), catchall, rlTaskError("Failed to configure base class"));

		const char* ticket = cred.GetTicket();
		rverify(AddStringParameter("ticket", ticket, true), catchall, );

#if !__FINAL
		const char* forcedCountryCode = NULL;
		if(PARAM_rlConductorForceGeo.Get(forcedCountryCode) && forcedCountryCode)
		{
			rlTaskDebug("Applying 'X-Force-Geo' of %s", forcedCountryCode);

			 //allowed values are an ISO country code (FR, GB) or, 
			//in the case of the US and Canada a country + province or statce seperated by an underscore (US_CA, CA_ON).
			m_HttpRequest.AddRequestHeaderValue("X-Force-Geo", forcedCountryCode, ustrlen(forcedCountryCode));
		}
#endif
		m_serverInfo = serverInfo;
		m_retrySecs = retrySecs;
		*m_retrySecs = 0;

		return true;
	}
	rcatchall
	{

	}

	return false;
}

bool rlRosConductorRouteTask::ProcessSuccess( const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode )
{
	
	rtry
	{
		const parTreeNode* routeResponse = node->FindChildWithName("Route");
		
		if (routeResponse)
		{
			const parTreeNode* assignedServer = routeResponse->FindChildWithNameIgnoreNs("assignedServer");
			rcheck(assignedServer, catchall, rlTaskError("No 'assignedServer' received"));

			rverify(rlHttpTaskHelper::ReadInt(m_serverInfo->m_serverInfo.Port, assignedServer, "port", NULL )	, catchall, );
			rverify(rlHttpTaskHelper::ReadInt(m_serverInfo->m_serverInfo.Id, assignedServer, "id", NULL )		, catchall, );
			const char* url = rlHttpTaskHelper::ReadString(assignedServer, "url", NULL );
			rverify(url, catchall, );

			m_serverInfo->m_serverInfo.SetUrl(url);

			const char* serviceToken = rlHttpTaskHelper::ReadString(routeResponse, "serviceAccessToken", NULL );
			rverify(serviceToken, catchall, );
			rverify(strlen(serviceToken) <= RLROS_MAX_SERVICE_ACCESS_TOKEN_SIZE, catchall, );

			//Pull out the ttl, but it's not required.
			rlHttpTaskHelper::ReadInt(m_serverInfo->m_ttl, routeResponse, "ttl", NULL);

#if !__FINAL
			if(PARAM_rlServiceTokenForceTTL.Get(m_serverInfo->m_ttl))
			{
				rlDisplay("Forcing ttl to %d due to -rlServiceTokenForceTTL", m_serverInfo->m_ttl);
			}
#endif
			
			m_serverInfo->m_AccessToken.Set(serviceToken);

			rverify(m_serverInfo->IsValid(), catchall, );

			resultCode = SUCCESSCODE_ROUTE_SUCCESSFUL;

			return true;
		}

		const parTreeNode* delayResponse = node->FindChildWithName("Delay");
		if(delayResponse)
		{
			const char* reason = rlHttpTaskHelper::ReadString(delayResponse, "reason", NULL );
			rverify(reason, catchall, );

			int delaySeconds = 0;
			rverify(rlHttpTaskHelper::ReadInt(delaySeconds, delayResponse, "delaySeconds", NULL), catchall, );


			//@TODO
			//Handle Reasons:
			//	NO_SERVER_AVAILABLE 
			if (strcmp(reason, "NO_SERVER_AVAILABLE") == 0)
			{
				resultCode = SUCCESSCODE_DELAY_NO_SERVER_AVAILABLE;
			}
			else if (strcmp(reason, "GEO_MOVE") == 0)
			{
				*m_retrySecs = delaySeconds;
				resultCode = SUCCESSCODE_DELAY_GEO_MOVE;
			}
			else if (strcmp(reason, "NO_SERVERS_INGRESS") == 0)
			{
				resultCode = SUCCESSCODE_DELAY_NO_SERVER_INGRESS;
			}
			else
			{
				rthrow(catchall, rlTaskError("Invalid reason for <Delay>: %s", reason));
			}
			
			//We only treat a GEO_MOVE delay as a success worthy of waiting.
			return resultCode == SUCCESSCODE_DELAY_GEO_MOVE;
		}

	}
	rcatchall
	{
		resultCode = SUCCESCODE_INVALID;
	}

	return false;
}

bool rlRosServiceAccessTokenStore::RequestAccessToken(const int localGamerIndex)
{
	rlDebug1("Requesting for index %d", localGamerIndex);

	if (!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return false;
	}

	//check to see if we already have one for this gamer index pending
	if (m_accessRequest.Pending() && m_accessRequest.GetGamerIndex() == localGamerIndex)
	{
		rlDebug1("Request for index %d is already pending", localGamerIndex);
		return true;
	}

	m_accessRequest.SetGamerIndex(localGamerIndex);

	//If the creds are already valid for this gamer then go ahead and start the request.
	const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
	if (!creds.IsValid())
	{
		rlError("Invalid creds for gamer index %d", localGamerIndex);
		return false;
	}

	return m_accessRequest.RequestToken();
}

#if !__FINAL
void rlRosServiceAccessTokenStore::InvalidateOrRequestToken(const int localGamerIndex)
{
	rlDebug1("Requesting for index %d", localGamerIndex);

	if (!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return;
	}

	//check to see if we already have one for this gamer index pending
	if (m_accessRequest.Pending() && m_accessRequest.GetGamerIndex() == localGamerIndex)
	{
		rlDebug1("Request for index %d is already pending", localGamerIndex);
		return;
	}

	m_accessRequest.SetGamerIndex(localGamerIndex);

	//If the creds are already valid for this gamer then go ahead and start the request.
	const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
	if (!creds.IsValid())
	{
		rlError("Invalid creds for gamer index %d", localGamerIndex);
		return;
	}

	m_accessRequest.InvalidateOrRequestToken();
}
#endif // !__FINAL

bool rlRosServiceAccessTokenStore::IsRequestPending( const int localGamerIndex, int& retryCount, int& retryTimeMs ) const
{
	if (m_accessRequest.GetGamerIndex() == localGamerIndex && m_accessRequest.Pending())
	{
		retryCount = m_accessRequest.m_retryCount;
		retryTimeMs = m_accessRequest.m_requestRetryTimeMs;
		return true;
	}
	
	return false;
}

bool rlRosServiceAccessTokenStore::IsRequestFailed( const int localGamerIndex ) const
{
	return m_accessRequest.GetGamerIndex() == localGamerIndex && m_accessRequest.Failed();
}

bool rlRosServiceAccessTokenStore::ServiceAccessRequest::RequestToken()
{
	rlRosConductorRouteTask* task = NULL;
	m_routeTaskStatus.Reset();
	m_routeTaskRetryTimeSecs = 0;
	m_expirationTimeMs = DEFAULT_TOKEN_TTL_MS;
	
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(m_localGamerIndex), catchall, );
		rverify(!m_routeTaskStatus.Pending(), catchall, );

		const rlRosCredentials& cred = rlRos::GetCredentials( m_localGamerIndex );
		rcheck(cred.IsValid(), catchall, rlError("Invalid RosCredentials"));

		task = rlGetTaskManager()->CreateTask<rlRosConductorRouteTask>();
		rverify(task,catchall,rlError("Failed to allocate rlRosGetServiceAccessTokenTask"));


		rcheck(rlTaskBase::Configure(task, 
			m_localGamerIndex, 
			&m_serviceInfo, 
			&m_routeTaskRetryTimeSecs,
			&m_routeTaskStatus),
			catchall,
			rlError("Failed to configure task")
			);

		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

		m_requestStatus.SetPending();

		return true;

	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}

		return false;
	}
}

void rlRosServiceAccessTokenStore::ServiceAccessRequest::Cancel()
{
	if (m_routeTaskStatus.Pending())
	{
		rlGetTaskManager()->CancelTask(&m_routeTaskStatus);
	}
	m_requestStatus.SetCanceled();
	m_expirationTimeMs = DEFAULT_TOKEN_TTL_MS;
}

void rlRosServiceAccessTokenStore::ServiceAccessRequest::Invalidate()
{
	rlDebug3("ServiceAccessRequest::Invalidate for index %d", m_localGamerIndex);
	Cancel();
	m_serviceInfo.m_AccessToken.Clear();

}

void rlRosServiceAccessTokenStore::ServiceAccessRequest::Update(const unsigned timeStepMs)
{
	//See if the request is in the pending state waiting for the task
	if (m_requestStatus.Pending())
	{
		//See if the task Succeeded.
		if (m_routeTaskStatus.Succeeded())
		{
			//Check the result code for the success.
			if (m_routeTaskStatus.GetResultCode() == rlRosConductorRouteTask::SUCCESSCODE_DELAY_GEO_MOVE)
			{
				//Once we've tried 3 times, we give up.
				if (m_retryCount == 3)
				{
					rlError("Aborting 'route' after 3 retries");
					m_requestStatus.SetFailed(rlRosConductorRouteTask::SUCCESCODE_INVALID);
					return;
				}

				//We need to delay and ask for a bit.
				m_routeTaskStatus.Reset();
				if (!rlVerifyf(m_routeTaskRetryTimeSecs > 0, ""))
				{
					rlError("Aborting 'route' due to invalid retry time secs");
					m_requestStatus.SetFailed(rlRosConductorRouteTask::SUCCESCODE_INVALID);
					return;
				}

				
				rlDebug1("Starting DELAY_GEO_MOVE for %d secs [attempt %d]", m_routeTaskRetryTimeSecs, m_retryCount);
				m_requestRetryTimeMs = m_routeTaskRetryTimeSecs * 1000;
				m_retryCount++;

				m_expirationTimeMs = DEFAULT_TOKEN_TTL_MS;

				//we're still pending.
			}
			else //Huge success.
			{
				//Update the parent netStatus
				m_requestStatus.SetSucceeded(0);

				//Set our expiration time.
				if (m_serviceInfo.m_ttl > 0)
				{
					m_expirationTimeMs = m_serviceInfo.m_ttl * 1000;
				}
				else
				{
					m_expirationTimeMs = DEFAULT_TOKEN_TTL_MS;
				}
			}
		}
		//Else if failed.
		else if(m_routeTaskStatus.Failed() || m_routeTaskStatus.Canceled())
		{
			if (m_routeTaskStatus.Failed())
			{
				m_requestStatus.SetFailed(m_routeTaskStatus.GetResultCode());
			}
			else
			{
				m_requestStatus.SetCanceled();
			}
		}
		//If the task isn't doing anything, we should be waiting for a delay count down.
		else if (m_requestRetryTimeMs > 0)
		{
			//Update the retry.
			if (timeStepMs >= m_requestRetryTimeMs)
			{
				m_requestRetryTimeMs = 0;
			}
			else
			{
				m_requestRetryTimeMs -= timeStepMs;
			}

#if !__NO_OUTPUT
			if (m_requestRetryTimeMs % 1000 < 33)
			{
				rlDebug3("Waiting on retry [attempt %d] countdown with %d ms left", m_retryCount, m_requestRetryTimeMs);
			}
#endif

			if (m_requestRetryTimeMs == 0)
			{
				//Re-request the token.
				RequestToken();
			}
		}
		else
		{
			if (!rlVerifyf(m_routeTaskStatus.Pending(), "'route' task isn't pending"))
			{
				m_requestStatus.SetFailed(rlRosConductorRouteTask::SUCCESCODE_INVALID);
				return;
			}
		}
	}
	else //Just spinning
	{
		//If we're valid, check the refresh
		if (SucceededAndValid())
		{
			//Decrement the timer.
			if (m_expirationTimeMs > 0)
			{
				m_expirationTimeMs -= timeStepMs;
			}

			if (m_expirationTimeMs <= 0)
			{
				rlDebug1("ServiceAccessRequest: token has expired.  Refresh starting");
				InvalidateOrRequestToken();
			}
		}
	}
}

void rlRosServiceAccessTokenStore::ServiceAccessRequest::SetGamerIndex(const int index)
{
	m_localGamerIndex = index;
	m_requestStatus.Reset();
}

void rlRosServiceAccessTokenStore::ServiceAccessRequest::InvalidateOrRequestToken()
{
	const rlRosCredentials& creds = rlRos::GetCredentials(GetGamerIndex());
	//If the creds aren't valid, clear the tokens associated with this gamer index
	if (!creds.IsValid())
	{
		rlDebug3("ServiceAccessRequest::InvalidateOrRequestToken - Invalidate");
		Invalidate();
	}
	else if (Pending())
	{
		rlDebug3("ServiceAccessRequest::InvalidateOrRequestToken - Request Pending");
	}
	else
	{
		rlDebug3("ServiceAccessRequest::InvalidateOrRequestToken - RequestToken");
		RequestToken();
	}
}

const rlRosServiceAccessInfo& rlRosServiceAccessTokenStore::GetAccessInfoForService(const int localGamerIndex) const
{
	static rlRosServiceAccessInfo InvalidAccessInfo;

	if (m_accessRequest.GetGamerIndex() == localGamerIndex)
	{
		if (m_accessRequest.SucceededAndValid())
		{
			return m_accessRequest.m_serviceInfo;
		}
	}

	rlDebug3("Passing 'InvalidAccessInfo' for gamer index %d", localGamerIndex);
	return InvalidAccessInfo;	
}

rlRosServiceAccessTokenStore::rlRosServiceAccessTokenStore()
{
	m_ROSDlgt.Bind(this, &rlRosServiceAccessTokenStore::OnRosEvent);
}

rlRosServiceAccessTokenStore::~rlRosServiceAccessTokenStore()
{

}

bool rlRosServiceAccessTokenStore::Init()
{
	rlRos::AddDelegate(&m_ROSDlgt);

	return true;
}

void rlRosServiceAccessTokenStore::Update(const unsigned timeStepMs)
{
	m_accessRequest.Update(timeStepMs);
}

void rlRosServiceAccessTokenStore::Shutdown()
{
	m_accessRequest.Cancel();
	rlRos::RemoveDelegate(&m_ROSDlgt);
}

void rlRosServiceAccessTokenStore::ClearAccessRequest()
{
	m_accessRequest.Cancel();
}

void rlRosServiceAccessTokenStore::OnRosEvent( const rlRosEvent& evt)
{
	const unsigned evtId = evt.GetId();

	int nGamerIndex = -1;
	switch (evtId)
	{
	case RLROS_EVENT_ONLINE_STATUS_CHANGED:
		{
			const rlRosEventOnlineStatusChanged& changeEvnt = static_cast<const rlRosEventOnlineStatusChanged&>(evt);
			nGamerIndex = changeEvnt.m_LocalGamerIndex;
		}
		break;
	case RLROS_EVENT_LINK_CHANGED:
		{
			const rlRosEventLinkChanged& linkEvnt = static_cast<const rlRosEventLinkChanged&>(evt);
			nGamerIndex = linkEvnt.m_LocalGamerIndex;
		}
		break;
	case RLROS_EVENT_GET_CREDENTIALS_RESULT:
		{
			const rlRosEventGetCredentialsResult& credEvent = static_cast<const rlRosEventGetCredentialsResult&>(evt);
			nGamerIndex = credEvent.m_LocalGamerIndex;

		}
		break;
	case RLROS_EVENT_RETRIEVED_GEOLOC_INFO:
		{
			//I think we'll need to react to this to 
		}
		break;
	}

	if (nGamerIndex != -1 && RL_IS_VALID_LOCAL_GAMER_INDEX(nGamerIndex))
	{
		if (m_accessRequest.GetGamerIndex() == nGamerIndex)
		{
			m_accessRequest.InvalidateOrRequestToken();
			
		}
	}
}

} //namespace rage