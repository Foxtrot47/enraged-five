// 
// rline/rlroshttptask.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROSHTTPTASK_H
#define RLINE_RLROSHTTPTASK_H

#include "rlroscommon.h"
#include "rline/rlhttptask.h"
#include "data/rson.h"
#include "data/sax.h"
#include "data/sha1.h"
#include "net/crypto.h"
#include "atl/delegate.h"

namespace rage
{

class parTree;
class parTreeNode;
class rlRosSaxReader;
class rlRosHttpTask;

//PURPOSE
//  ROS encryption methods.  These encrypt the HTTP payload.
enum rlRosHttpEncryption
{
    RLROS_ENCRYPTION_NONE = 0,    //No encryption done
    RLROS_ENCRYPTION_TITLE,       //Encryption is done with per title+platform secrets
};

//PURPOSE
//  Creates ROS user-agent header strings, which encode the ROS encryption
//  method used by the request, and the title/platform/version it was made from.
//  The string is obfuscated to make it appear encrypted.
class rlRosHttpUserAgentHeader
{
public:
    rlRosHttpUserAgentHeader();

    //PURPOSE
    //  Clears the instance. After this call, IsValid() will return false.
    void Clear();

    //PURPOSE
    //  Initializes (or reinitializes) this instance.  
    //  After this call, IsValid() will return true.
    void Reset(rlRosHttpEncryption encryption,
               const char* titleName,
               const char* platformName,
               const int version,
			   const s64 sessionId,
			   const PlayerAccountId accountId);

    //PURPOSE
    //  Simple accessors.
    bool IsValid() const { return m_IsValid; }
    rlRosHttpEncryption GetEncryption() const { return m_Encryption; }
    const char* GetTitleName() const { return m_TitleName; }
    const char* GetPlatformName() const { return m_PlatformName; }
    int GetVersion() const { return m_Version; }

    //Maximum length of a ROS user-agent value
    enum { MAX_USER_AGENT_LEN = 160 };

    //PURPOSE
    //  Composes a ROS user-agent value.  The buffer passed in must
    //  be at least MAX_USER_AGENT_LEN in size.
    static bool Compose(rlRosHttpEncryption encryption,
                        const char* titleName,
                        const char* platformName,
                        const int version,
                        const rlRosCredentials& cred,
                        char* buf, 
                        const unsigned bufSize);

    //PURPOSE
    //  Parses a ROS user agent string and Reset()s the instance
    //  with the extracted data.
    static bool Parse(const char* str, rlRosHttpUserAgentHeader* rosUserAgent);

private:
    //This value MUST match the backend value defined in RosHttp.cs
    enum { RANDOM_PREFIX_LEN = 4 };

    rlRosHttpEncryption m_Encryption;
    char m_TitleName[RLROS_MAX_TITLE_NAME_SIZE];
    char m_PlatformName[RLROS_MAX_PLATFORM_NAME_SIZE];
	s64 m_SessionId;
	PlayerAccountId m_AccountId;
    int m_Version;
    bool m_IsValid : 1;
};

//PURPOSE
//  HTTP filter used by ROS requests.  This encrypts the request,
//  and decrypts the response.
class rlRosHttpFilter : public netHttpFilter
{
public:
    typedef atDelegate<bool (const rlRosContentSignature&, const u8* hash, const unsigned hashLen)> VerifySignatureDelegate;

public:
    rlRosHttpFilter();
    virtual ~rlRosHttpFilter();

    //PURPOSE
    //  Configures the filter: determines what encryption will be used
    //  and composes the user-agent string.
    bool Configure(const int localGamerIndex, const char* url);

    //PURPOSE
    //  Override to allow explicitly configuring security flags
    bool Configure(const int localGamerIndex, const char* url, const rlRosSecurityFlags securityFlags);

    virtual bool CanFilterRequest() const;
    virtual bool CanFilterResponse() const;
	virtual bool CanIntercept() const;
	virtual u32 DelayMs() const;

    virtual bool FilterRequest(const u8* data,
                               const unsigned dataLen,
                               const bool finalCall,
                               datGrowBuffer& output);

    virtual bool FilterResponse(const u8* data,
                                const unsigned dataLen,
                                const bool allDataReceived,
                                fiHandle& outputHandle,
                                const fiDevice* outputDevice,
                                unsigned* numProcessed,
                                bool* hasDataPending);

    virtual const char* GetUserAgentString() const;

    virtual bool ProcessRequestHeader(class netHttpRequest* request);

    virtual bool ProcessResponseHeader(const int statusCode, const char* header);
	
	virtual void Receive(void* UNUSED_PARAM(buf),
						 const unsigned UNUSED_PARAM(bufSize),
						 unsigned* UNUSED_PARAM(bytesReceived)) {};

    virtual bool AllowSucceeded(class netHttpRequest* request);

	void Clear();

private:

    //These constants must EXACTLY match the values used on the backend.
    enum
    {
        KEY_LEN = 16,
        SALT_LEN = 16
    };

    struct RequestData
    {
        rlRosHttpEncryption m_Encryption;
        //The original security flags this request was made with (i.e. by caller)
        rlRosSecurityFlags m_OriginalSecurityFlags;
        //Our current security flags, which may change due to being redirected to
        //a non-ros url
        rlRosSecurityFlags m_SecurityFlags;
        Rc4Context m_Rc4;    
        Sha1 m_Sha1;
        bool m_HaveWrittenPreamble;

        //The challenge/nonce we included with the request, used to compute
        //the proper hmac to verify responses with
        u8 m_Challenge[8];

        //Our request headers hmac if using RLROS_SECURITY_HMAC_REQUEST_HEADERS
        u8 m_HeaderHmacDigest[Sha1::SHA1_DIGEST_LENGTH];

        RequestData();
        ~RequestData();
        void Clear();
    };
    RequestData m_Request;

    struct ResponseData
    {
        enum
        {
            //These constants MUST match the backend.
            KEY_OFFSET = 0,
            BLOCK_SIZE_OFFSET = KEY_OFFSET + KEY_LEN,
            BLOCK_SIZE_LEN = 4,
            PREAMBLE_LEN = BLOCK_SIZE_OFFSET + BLOCK_SIZE_LEN
        };

        rlRosHttpEncryption m_Encryption;
        rlRosHttpEncryption m_DefaultEncryption;

        Rc4Context m_Rc4;
        //The combined key from our response
        u8 m_Key[KEY_LEN];

        bool m_HaveReadPreamble : 1;

        //Whether or not the server returned a content signature to verify
        bool m_HaveContentSignature : 1;

        //Whether or not the server returned a content hash to verify
        bool m_HaveContentHash : 1;
        
        //The content hash returned by the server
        u8 m_ExpectedContentHash[Sha1::SHA1_DIGEST_LENGTH];

        //The content signature returned by the server
        rlRosContentSignature m_ExpectedContentSignature;

        //Sha1 used to compute the hash of the entire plaintext response
        Sha1 m_ResponseSha1;

        ResponseData();
        ~ResponseData();
        void Clear();
    };
    ResponseData m_Response;

    char m_UserAgentString[rlRosHttpUserAgentHeader::MAX_USER_AGENT_LEN];

    //The session key, if any, used to encrypt/decrypt the request/response
    bool m_HaveSessionKey;
    rlRosCredentials::SessionKey m_SessionKey;
    //Our session ticket so that we can securely tell the server our session key
    char m_SessionTicket[RLROS_MAX_SESSION_TICKET_SIZE];
    //Our delegate, if any, used to verify a content signature
    VerifySignatureDelegate m_VerifySignatureDelegate;
	// Disabling HTTP dumping
	bool m_bDisableHttpDump;

public:
    //PURPOSE:
    //  Used to set a callback that knows how to verify our content signature,
    //  which should call VerifySignature with the property signature type/resource
    void SetVerifySignatureDelegate(const VerifySignatureDelegate& dlgt);

    //PURPOSE:
    //  Helper method to handle verifying a given signature against a Sha1 hash computed from a file.
    //  This also needs to be passed a signature type and a resource. E.g. cloud signatures use the
    //  RLROS_SIGNATURE_CLOUD type and the resource is the absolute cloud path
    static bool VerifySignature(const rlRosContentSignature& signature, 
                                const rlRosSignatureType signatureType, 
                                const u8* resource, 
                                const unsigned resourceLen,
                                const u8* hash,
                                const unsigned hashLen);

    //PURPOSE:
    //  Retrieves the signature, if any, that the server returned. This can be used to, for instance,
    //  retrieve a signature for a 304 not-modified response and verify it matches that of a cached
    //  version using VerifySignature above
    bool GetContentSignature(rlRosContentSignature* signature) const;

    //PURPOSE:
    //  Retrieves the hash, if any, that the server returned
    bool GetContentHash(u8 (&hash)[Sha1::SHA1_DIGEST_LENGTH]) const;

	// PURPOSE
	//	Disables HTTP dumping for this request
	virtual void DisableHttpDump(bool bDisabled) { m_bDisableHttpDump = bDisabled; }
};

//PURPOSE
//  Base class for tasks that call a ROS web service.
//  This automates the composition of URLs, and the parsing of 
//  standard success and error responses.
//
//NOTE
//  When the call is in the context of a local gamer (see Configure)
//  then if an AuthenticationFailed.Ticket error is received
//  the ticket for that gamer will be automatically invalidated.
class rlRosHttpTask : public rlHttpTask
{
    friend class rlRosSaxReader;

public:
    RL_TASK_USE_CHANNEL(rline_ros);
    RL_TASK_DECL(rlRosHttpTask);

    rlRosHttpTask(sysMemAllocator* allocator = NULL, u8* bounceBuffer = NULL, const unsigned sizeofBounceBuffer = 0);
    virtual ~rlRosHttpTask();

    //PURPOSE
    //  Value to pass to Configure for tasks that don't require a local gamer.
    enum { NO_LOCAL_GAMER = -1 };

    //PURPOSE
    //  Base class configure.  If the call is not in the
    //  context of a local gamer, then pass in NO_LOCAL_GAMER.
    //  The entrie ROS response will be passed to ProcessResponse().
    bool Configure(const int localGamerIndex);

    //PURPOSE
    //  Same as above except the ROS response will be parsed as
    //  a interpreted as an XML document and returned in the parTree
    //  object.  The caller will be responsible for calling delete
    //  on the parTree object.
    bool Configure(const int localGamerIndex, parTree** parserTree);

    //PURPOSE
    //  Same as above except the ROS response will be parsed as
    //  a stream using SAX.  The SAX reader object will call the
    //  tasks SAX methods (see startDocument() et al below).
    //
    //  The main difference is the parser need not wait for the
    //  entire response to be received.  It can parse it as a stream,
    //  thus saving memory.
    bool Configure(const int localGamerIndex, rlRosSaxReader* saxReader);

    //PURPOSE
    //  Wrap up all the possible parameters inside of rlHttpTaskConfig.
    //  FIXME(KB) Someday get rid of the other Configure() variants.
    bool Configure(const int localGamerIndex, const rlHttpTaskConfig* config);

    //PURPOSE
    //  Returns local gamer index we were configured for, or NO_LOCAL_GAMER if
    //  the call is outside the context of a local gamer.
    int GetLocalGamerIndex() const;

	// PURPOSE
	//	rlRos Http Tasks can access either 'gameservices' endpoints or 'launcherservices'
	//	end points. The default value is game services.
	enum rlRosHttpServices
	{
		GAME_SERVICES,
		LAUNCHER_SERVICES
	};

	// PURPOSE
	//	Static override all rlRosHttpTask to use Launcher Services instead of GameServices
	static void SetDefaultServices(rlRosHttpServices service) { sm_DefaultService = service; }
	static rlRosHttpServices GetDefaultServices() { return sm_DefaultService; }

protected:
    //Set to true if request should be sent via HTTPS.
    virtual bool UseHttps() const;
    virtual SSL_CTX* GetSslCtx() const;

    //Allows subclasses to override the security flags used for a request
    virtual rlRosSecurityFlags GetSecurityFlags() const;

    //Returns the host name part of a ROS URL (ex. dev.ros.rockstargames.com). 
    //Derived tasks should not override this method.
    virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
    
    //Parses ROS responses in a standard way, and then calls either
    //ProcessSuccess or ProcessError.
    //Derived tasks should not override this method.
    virtual bool ProcessResponse(const char* response, int& resultCode);  
    
    //Returns the path part of the URL to be used.
    //E.g. http://example.com/{path to resource}?arg1=0&arg2=1
    virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const;

    //Returns portion of the URL after {env}.ros.rockstargames.com/{title}/{version}/gameservices/
    //rlRosHttpTask uses this to compose the final URL in GetServicePath().
    virtual const char* GetServiceMethod() const = 0;

    //Called when we get an success response.  Before this is called
    //resultCode is already set to 0, so this only needs to be overridden
    //if you need to process the node, or set resultCode to something else.
    //Overrides should return false if they encounter an error.
    virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    //Called when we get an error response.  Before this is called
    //resultCode is already set to -1, so this only needs to be overridden
    //if you need to handle specific error codes, set resultCode to something else,
    //or do any special cleanup upon error.
    //You can process node if error responses are expected to have a well-formed
    //response in them, although it may or may not be null.
    virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

    //Callbacks made during SAX parsing.  Note that these will only be called
    //for "payload" elements; the <Response>, <Status>, and <Error> elements
    //are parsed before any of these are called.  These callbacks are only made for
    //successful requests; however, if there is an error while parsing the payload, 
    //the method should return false.  In that event, ProcessError will be called, 
    //and the result will be an error with code "_SaxParsingError".
    //
    //Note that in order for these to be called Configure() must be called
    //with a non-NULL rlRosSaxReader.
    virtual bool SaxStartElement(const char* name);
    virtual bool SaxAttribute(const char* name, const char* val);
    virtual bool SaxCharacters(const char* ch, const int start, const int length);
	virtual bool SaxEndElement(const char* name);

	//We usually use an RosHttpFilter for all.  However, there are some cases we don't want to 
	//use the filter at all.
	virtual bool UseFilter() const;

	// Returns the string to use in URL path for the current service set.
	virtual const char* GetServiceSet() const;

	rlRosHttpFilter m_Filter;

private:
    rlRosHttpTask(const rlRosHttpTask& rhs);
    rlRosHttpTask& operator=(const rlRosHttpTask& rhs);

    //PURPOSE
    //  For non-streaming requests, this calls either ProcessSuccess or ProcessError,
    //  based on the result; successful calls will have a non-null node that will be
    //  parsed by derived classes in ProcessSuccess.
    //
    //  For streaming requests, the XML has already all been parsed so node is null.
    //  If result indicates error, ProcessError is called.
    bool ProcessRosResult(const rlRosResult result, 
                          const parTreeNode* node, 
                          int &resultCode);

    int m_LocalGamerIndex;
	rlRosHttpServices m_Service;
    parTree** m_ParserTree;

	// Static Bool to be toggled globally for rlRosHttpTask to default to using
	//	LauncherServices instead of GameServices. Please note: if we extend to
	//	another set of services used by game code, change this to an enum.
	static rlRosHttpServices sm_DefaultService;
};

// PURPOSE
//	Base class for tasks that call JSON ROS services.
class rlRosJsonHttpTask : public rlHttpTask
{
public:
	RL_TASK_USE_CHANNEL(rline_ros);
	RL_TASK_DECL(rlRosJsonHttpTask);

	rlRosJsonHttpTask();

	//PURPOSE
	//  Base class configure.  If the call is not in the
	//  context of a local gamer, then pass in RL_INVALID_GAMER_INDEX.
	//  The entire ROS response will be passed to ProcessResponse().
	bool Configure(const int localGamerIndex);

protected:

	// Header and Body utilities.
	virtual bool AddRequestHeader(const char* name, const char* value);
	virtual bool SetContent(const char* json, const unsigned jsonLength);

	//Returns the host name part of a ROS URL (ex. dev.ros.rockstargames.com). 
	//Derived tasks should not override this method.
	virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const; //override;

	//This should return a string if we want to use a specific service host name in GetUrlHostName
	//The string is the {serviceId} portion of the URL in {serviceId}-{title}-{env}.ros.rockstargames.com/
	//If default (nullptr), the URL will be in the form {env}.ros.rockstargames.com/
	virtual const char* GetUrlServiceId() const;

	//Returns the path part of the URL to be used.
	//E.g. http://example.com/{path to resource}?arg1=0&arg2=1
	virtual bool GetServicePath(char* svcPath, const unsigned bufSize) const; //override;

	//Returns portion of the URL after {env}.ros.rockstargames.com/{title}/{version}/wcfgameservices/
	//rlRosJsonHttpTask uses this to compose the final URL in GetServicePath().
	virtual const char* GetServiceMethod() const = 0;

	//Set to true if request should be sent via HTTPS.
	virtual bool UseHttps() const;
	virtual SSL_CTX* GetSslCtx() const;

	//Parses ROS responses in a standard way, and then calls either
	//ProcessSuccess or ProcessError.
	//Derived tasks should not override this method.
	virtual bool ProcessResponse(const char* response, int& resultCode); // override;

	//Called when we get an success response.  Before this is called
	//resultCode is already set to 0, so this only needs to be overridden
	//if you need to process the node, or set resultCode to something else.
	//Overrides should return false if they encounter an error.
	virtual bool ProcessSuccess(const rlRosJsonResult& result, RsonReader* resultNode, int& resultCode);

	//Called when we get an error response.  Before this is called
	//resultCode is already set to -1, so this only needs to be overridden
	//if you need to handle specific error codes, set resultCode to something else,
	//or do any special cleanup upon error.
	//You can process node if error responses are expected to have a well-formed
	//response in them, although it may or may not be null.
	virtual void ProcessError(const rlRosJsonResult& result, RsonReader* resultNode, int& resultCode);

protected:

	int m_LocalGamerIndex;
};

//PURPOSE
//  Parses a XML stream to determine the contents of the ROS <Response> element,
//  and passes through all SAX calls to the owning task (m_Task).
//
//  When the owning task's endDocument() is called it should check the
//  rlRosResult (GetResult()) to determine if the web service call was
//  successful
class rlRosSaxReader : public datSaxReader
{
public:
    rlRosSaxReader();

private:
    friend class rlRosHttpTask;

    //datSaxReader overrides, called by rlHttpTask
    virtual void startDocument();
    virtual void endDocument();
    virtual void startElement(const char* url, const char* localName, const char* qName);
    virtual void endElement(const char* url, const char* localName, const char* qName);
    virtual void attribute(const char* tagName, const char* attrName, const char* attrVal);
    virtual void characters(const char* ch, const int start, const int length);

    //Parser states
    enum State
    {
        STATE_EXPECT_RESPONSE,  //Expect the outer <Response> element
        STATE_EXPECT_STATUS,    //Expect next element to be <Status>
        STATE_READ_STATUS,      //Reading the <Status> element
        STATE_EXPECT_ERROR,     //Expect next element to be <Error>
        STATE_READ_ERROR,       //Reading the <Error> element
        STATE_READ_RESULTS,     //Passing through elements to task for processing
        STATE_PARSE_SUCCEEDED,  //We successfully parsed everything we expected to receive.
                                //Ignore remaining data.
        STATE_PARSE_FAILED      //We couldn't parse the response fully; either it was
                                //malformed or incomplete. Ignore remaining data.
    };

    //Task for which SAX methods will be called.
    //This is set by rlRosHttpTask::Configure().
    rlRosHttpTask* m_Task;

    State m_State;
    char m_StatusChars[8];
    char m_ResultCodeBuf[64];
    char m_ResultCodeExBuf[64];
    rlRosResult m_RosResult;
};

} //namespace rage

#endif
