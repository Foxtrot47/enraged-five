// 
// rline/rlroshttptask.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlroshttptask.h"
#include "rlros.h"
#include "data/rson.h"
#include "data/base64.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/device.h"
#include "math/amath.h"
#include "net/netdiag.h"
#include "net/netnonce.h"
#include "parser/manager.h"
#include "rline/rlpc.h"
#include "rline/rltitleid.h"
#include "string/stringutil.h"
#include "system/criticalsection.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/coderivets.h"
#include "system/timer.h"
#include "system/endian.h"

#include "wolfssl/wolfcrypt/asn.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, roshttptask)
#undef __rage_channel
#define __rage_channel rline_roshttptask

PARAM(rlRosHttpNoEncrypt, "If nonzero, disables ROS encryption");
XPARAM(nethttpdump);

extern const rlTitleId* g_rlTitleId;
extern sysMemAllocator* g_rlAllocator;

static sysCriticalSectionToken sm_Cs;
static bool sm_RlRosHttpTaskInitialized = false;
static bool sm_UseRosEncryption = true;
static bool sm_AllowUnencrypted = false;

// Default to game services
rlRosHttpTask::rlRosHttpServices rlRosHttpTask::sm_DefaultService = rlRosHttpTask::GAME_SERVICES;

///////////////////////////////////////////////////////////////////////////////
//  rlRosHttpUserAgentHeader
///////////////////////////////////////////////////////////////////////////////
rlRosHttpUserAgentHeader::rlRosHttpUserAgentHeader()
{
    Clear();
}

void 
rlRosHttpUserAgentHeader::Clear()
{
    m_Encryption = RLROS_ENCRYPTION_NONE;
    sysMemSet(m_TitleName, 0, sizeof(m_TitleName));
    sysMemSet(m_PlatformName, 0, sizeof(m_PlatformName));
    m_Version = 0;
    m_IsValid = false;
	m_SessionId = 0;
	m_AccountId = 0;
}

void 
rlRosHttpUserAgentHeader::Reset(rlRosHttpEncryption encryption,
                                const char* titleName,
                                const char* platformName,
                                const int version,
								const s64 sessionId,
								const PlayerAccountId accountId)
{
    Clear();

    m_Encryption = encryption;
    safecpy(m_TitleName, titleName);
    safecpy(m_PlatformName, platformName);
    m_Version = version;
    m_IsValid = true;
	m_SessionId = sessionId;
	m_AccountId = accountId;
}

bool
rlRosHttpUserAgentHeader::Compose(rlRosHttpEncryption encryption,
                                  const char* titleName,
                                  const char* platformName,
                                  const int version,
                                  const rlRosCredentials& cred,
                                  char* buf, 
                                  const unsigned bufSize)
{
    rlAssert(buf && bufSize);
    rlAssert(bufSize >= MAX_USER_AGENT_LEN);

    rtry
    {
        const char* PREFIX_STR = "ros ";
        const unsigned PREFIX_STR_LEN = istrlen(PREFIX_STR);
        const unsigned KVP_BUF_SIZE = MAX_USER_AGENT_LEN - PREFIX_STR_LEN;

        //Allocate the buffer for the key-value pair portion of the user-agent string
        u8* kvpBuf = (u8*)alloca(KVP_BUF_SIZE);
        rverify(kvpBuf, catchall, );

        //Start the kvp buffer with random bytes...
        netRandom rng(sysTimer::GetSystemMsTime());
        for(unsigned i = 0; i < RANDOM_PREFIX_LEN; i++)
        {
            kvpBuf[i] =(u8)rng.GetRanged(0, 255);
        }

        //Followed by our key-value pair CSV...
        //IMPORTANT! The format here CANNOT change; for efficiency, the server
        //is coded to expect no whitespace, and for the parameters to be specified 
        //in EXACTLY this order.  If you change this, all requests will fail.
        char* kvpCsv = (char*)&kvpBuf[RANDOM_PREFIX_LEN];

        if (cred.IsValid()
            && cred.HasPrivilege(RLROS_PRIVILEGEID_HTTP_REQUEST_LOGGING)
            && cred.GetSessionId() > 0 
            && cred.GetPlayerAccountId() > 0)
        {
            //If localGamerIndex is valid and the player has HTTP Request Logging privileges,
            //it will pass nonzero sessionId and accountId values to Compose.  This generates
            //additional parameters in the user-agent string that, when parsed by the server,
            //will log the HTTP requests.
			formatf(kvpCsv, 
                    KVP_BUF_SIZE - RANDOM_PREFIX_LEN,
			        "e=%d,t=%s,p=%s,v=%d,s=%" I64FMT "d,a=%d",
			        encryption,
			        titleName,
			        platformName,
			        version,
			        cred.GetSessionId(),
			        cred.GetPlayerAccountId());
        }
        else
        {
            formatf(kvpCsv, 
            KVP_BUF_SIZE - RANDOM_PREFIX_LEN,
			        "e=%d,t=%s,p=%s,v=%d",
			        encryption,
			        titleName,
			        platformName,
			        version);
        }

        //Then obfuscate the kvpCsv by XORing with the random bytes
        unsigned startIndex = RANDOM_PREFIX_LEN;
        unsigned endIndex = startIndex + istrlen(kvpCsv);
        for(unsigned i = startIndex; i < endIndex; i++)
        {
            kvpBuf[i] ^= kvpBuf[i % RANDOM_PREFIX_LEN];
        }

        //Convert the kvpBuf to base64
        unsigned b64KvpSize = datBase64::GetMaxEncodedSize(endIndex);
        char* b64Kvp = (char*)alloca(b64KvpSize);
        rverify(b64Kvp, catchall, );

        unsigned b64Len;
        rverify(datBase64::Encode(kvpBuf, endIndex, b64Kvp, b64KvpSize, &b64Len),
                catchall,
                rlError("Failed to base64 encode key-value pairs"));

        //Create the final user-agent string
        formatf(buf, bufSize, "%s%s", PREFIX_STR, b64Kvp);

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlRosHttpUserAgentHeader::Parse(const char* str, rlRosHttpUserAgentHeader* rosUserAgent)
{
    rtry
    {
        rosUserAgent->Clear();

        const char* PREFIX_STR = "ros "; //space added so we don't handle later versions (ex. ros/1.1)

        rcheck(StringStartsWithI(str, PREFIX_STR),
               catchall,
               rlError("Does not start with '%s': %s", PREFIX_STR, str));

        //Decode the base-64 payload
        const char* payload = &str[strlen(PREFIX_STR)];
        while(*payload == ' ') ++payload;

        unsigned kvpSize = datBase64::GetMaxDecodedSize(payload) + 1; //+1 for terminating null
        u8* kvp = (u8*)alloca(kvpSize);
        rverify(kvp, catchall, rlError("Failed to alloc %d bytes for kvp", kvpSize));
        sysMemSet(kvp, 0, kvpSize);

        unsigned kvpLen;
        rverify(datBase64::Decode(payload, kvpSize, kvp, &kvpLen),
                catchall,
                rlError("Failed to b64 decode kvp"));
        kvp[kvpLen] = '\0';

        //Deobfuscate the payload by XORing with the random prefix bytes.
        for(unsigned i = RANDOM_PREFIX_LEN; i < kvpLen; i++)
        {
            kvp[i] ^= kvp[i % RANDOM_PREFIX_LEN];
        }

        //Process the deobfuscated key-value pairs.
        rlRosHttpEncryption encryption = RLROS_ENCRYPTION_NONE;
        const char* titleName = "";
        const char* platformName = "";
        int version = 0;
		s64 sessionId = 0;
		PlayerAccountId accountId = 0;

        const char* kvpStr = (const char*)&kvp[RANDOM_PREFIX_LEN];
        
        while(*kvpStr != '\0')
        {
            //Find the start and end of the key string
            const char* key = kvpStr;
            while(*key == ' ' || *key == ',') ++key;

            if (*key == '\0') break;

            char* keyEnd = (char*)key;
            while(*keyEnd != ' ' && 
                  *keyEnd != '=' &&
                  *keyEnd != '\0')
            {
                ++keyEnd;
            }
         
            rverify(*keyEnd != '\0', 
                    catchall, 
                    rlError("Key %s has no value", key));
            
            *keyEnd = '\0';

            //Find the start and end of the value string
            const char* val = keyEnd + 1;
            while(*val == ' ' || *val == '=') ++val;

            char* valEnd = (char*)val;
            while(*valEnd != ' ' && 
                  *valEnd != ',' &&
                  *valEnd != '\0')
            {
                ++valEnd;
            }

            if (*valEnd == '\0')
            {
                kvpStr = valEnd;
            }
            else
            {
                *valEnd = '\0';
                kvpStr = valEnd + 1;
            }

            //Process the key-value pair
            if(!stricmp(key, "e"))
            {
                int enc;
                rverify(1 == sscanf(val, "%d", &enc), 
                        catchall, 
                        rlError("Failed to parse val '%s' for key '%s'", val, key));
                encryption = (rlRosHttpEncryption)enc;
            }
            else if(!stricmp(key, "t"))
            {
                titleName = val;
            }
            else if(!stricmp(key, "p"))
            {
                platformName = val;
            }
            else if(!stricmp(key, "v"))
            {
                rverify(1 == sscanf(val, "%d", &version), 
                        catchall, 
                        rlError("Failed to parse val '%s' for key '%s'", val, key));
            }
			else if(!stricmp(key, "s"))
			{
				rverify(1 == sscanf(val, "%" I64FMT "d", &sessionId), 
				catchall, 
				rlError("Failed to parse val '%s' for key '%s'", val, key));
			}
			else if(!stricmp(key, "a"))
			{
				rverify(1 == sscanf(val, "%d", &accountId), 
				catchall, 
				rlError("Failed to parse val '%s' for key '%s'", val, key));
			}
            else
            {
                rlWarning("Unhandled key in ROS user agent: '%s'", key);
            }
        }

        rosUserAgent->Reset(encryption, titleName, platformName, version, sessionId, accountId);

        return true;
    }
    rcatchall
    {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
//  rlRosHttpFilter
///////////////////////////////////////////////////////////////////////////////

const static int s_RoSBlockSize = 1024;
const static int s_RoSFullBlockSize = s_RoSBlockSize + Sha1::SHA1_DIGEST_LENGTH;
// Since we don't buffer encrypted data until we reach our block size to decrypt,
// and instead rely on it being buffered by netHttpRequest, make sure that
// netHttpRequest has a default bounce buffer size that is large enough
// to do this
CompileTimeAssert(netHttpRequest::DEFAULT_BOUNCE_BUFFER_MAX_LENGTH >= s_RoSFullBlockSize);

rlRosHttpFilter::RequestData::RequestData()
: m_HaveWrittenPreamble(false)
, m_SecurityFlags(RLROS_SECURITY_NONE)
, m_OriginalSecurityFlags(RLROS_SECURITY_NONE)
{
}

rlRosHttpFilter::RequestData::~RequestData()
{
    Clear();
}

void 
rlRosHttpFilter::RequestData::Clear()
{
    m_Encryption = RLROS_ENCRYPTION_NONE;
    m_SecurityFlags = RLROS_SECURITY_NONE;
    m_OriginalSecurityFlags = RLROS_SECURITY_NONE;
    m_Rc4.Clear();
    m_Sha1.Clear();
    m_HaveWrittenPreamble = false;
}

rlRosHttpFilter::ResponseData::ResponseData()
: m_HaveReadPreamble(true)
{
}

rlRosHttpFilter::ResponseData::~ResponseData()
{
    Clear();
}

void 
rlRosHttpFilter::ResponseData::Clear()
{
    m_Encryption = RLROS_ENCRYPTION_NONE;
    m_DefaultEncryption = RLROS_ENCRYPTION_NONE;
    m_Rc4.Clear();
    m_HaveReadPreamble = false;
    m_HaveContentHash = false;
    m_HaveContentSignature = false;
    m_ExpectedContentSignature.Clear();
}

rlRosHttpFilter::rlRosHttpFilter() 
{
    m_UserAgentString[0] = '\0';
    m_HaveSessionKey = false;
    m_SessionTicket[0] = '\0';
	m_bDisableHttpDump = false;
}

rlRosHttpFilter::~rlRosHttpFilter()
{
}

void rlRosHttpFilter::Clear()
{
	m_Request.Clear();
	m_Response.Clear();
    m_HaveSessionKey = false;
    m_SessionTicket[0] = '\0';
}



bool 
rlRosHttpFilter::Configure(const int localGamerIndex, const char* url)
{
    return Configure(localGamerIndex, url, RLROS_SECURITY_DEFAULT);
}

bool
rlRosHttpFilter::Configure(const int localGamerIndex, const char* url, const rlRosSecurityFlags securityFlags)
{
    m_Request.Clear();
    m_Response.Clear();

    //If necessary, do one-time init
    if (!sm_RlRosHttpTaskInitialized && g_rlTitleId)
    {           
        SYS_CS_SYNC(sm_Cs);

        //Should we default to using ROS encryption?

#if __RGSC_DLL
        //We do not if this is for lanoire (SC DLL)
        sm_UseRosEncryption = (0 != strcmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "lanoire"));
#else
		sm_UseRosEncryption = true;
#endif

        if (sm_UseRosEncryption)
        {
            int noEncrypt = 0;
			// don't use encryption if our cmdline overrode it.
            PARAM_rlRosHttpNoEncrypt.Get(noEncrypt);
            sm_UseRosEncryption = !noEncrypt;
        }

        sm_RlRosHttpTaskInitialized = true;
    }

    bool useEncryption = sm_UseRosEncryption;

    if (useEncryption)
    {
        //Don't use encryption if the server told us not to.
        SYS_CS_SYNC(sm_Cs);
        useEncryption = !sm_AllowUnencrypted;
    }

    if(useEncryption)
    {
        //If this is not a ROS URL, then we can't encrypt the request and
        //must assume the response is also unencrypted
        const char* rosUrl = stristr(url, RLROS_BASE_DOMAIN_NAME);
        if (NULL == rosUrl)
        {
            rlDebug("Non ROS URL, defaulting to unencrypted response: '%s'", url);
            useEncryption = false;
        }
    }

    m_Request.m_Encryption = useEncryption ? RLROS_ENCRYPTION_TITLE : RLROS_ENCRYPTION_NONE;
    
    //By default, we expect the response to use the same encryption as the request
    m_Response.m_DefaultEncryption = m_Request.m_Encryption;

    //Currently tying whether or not we use security flags to whether or not we're
    //using title-encryption, i.e. can only be used on ros domain
    m_Request.m_SecurityFlags = useEncryption ? securityFlags : RLROS_SECURITY_NONE;
    //Remove any security flags that have been disabled via CreateTicket
    rlRos::RemoveDisabledSecurityFlags(m_Request.m_SecurityFlags);
    //Cache our original security flags in the request as they change on redirects
    m_Request.m_OriginalSecurityFlags = m_Request.m_SecurityFlags;
	
    rlRosCredentials cred;
    if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
    {
        cred = rlRos::GetCredentials(localGamerIndex);

#if __RGSC_DLL
		if (!cred.IsValid())
		{
			if (GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn())
			{
				rlDebug3("Using sign in credentials");
				cred = GetRgscConcreteInstance()->_GetProfileManager()->GetSignInCredentials();
			}
		}
#endif
    }

    //Compose our user-agent string
    if (!rlRosHttpUserAgentHeader::Compose(m_Request.m_Encryption,
                                           g_rlTitleId->m_RosTitleId.GetTitleName(),
                                           g_rlTitleId->m_RosTitleId.GetPlatformName(),
                                           g_rlTitleId->m_RosTitleId.GetTitleVersion(),
										   cred,
                                           m_UserAgentString,
                                           sizeof(m_UserAgentString)))
   { 
        rlError("Failed to compose user-agent string");
        return false;
    }

    //If we have valid credentials, cache our plaintext secret key so that we can encrypt the request/decrypt the response with it,
    //as well as our session ticket so that we can secretly communicate our session key to the server
    if (cred.IsValid())
    {
        m_HaveSessionKey = true;
        m_SessionKey = cred.GetSessionKey();
        const char* sessionTicket = cred.GetSessionTicket();
        safecpy(m_SessionTicket, sessionTicket);
    }
    else
    {
        //Currently not having valid credentials is a security risk, since the session-specific encryption key
        //is tied to your credentials.
        //For now we assert that if credentials aren't supplied, then the default security flags must have been overridden,
        //just as a sanity check to make sure something really doesn't need to be secure.
        //The only thing that shouldn't use credentials is CreateTicket which is secured behind SSL.
        //Anything else missing credentials is a security risk, especially if it includes something like the player's
        //ticket which should probably be kept secret
        //TODO: Only a warning for now, some tasks configure the filter before checking to see if the gamer is signed in,
        //so need a better way to track this
        rlWarning("rlRosHttpFilter::Configure: Missing valid credentials with security turned on");
        //rlAssert(securityFlags == RLROS_SECURITY_NONE);
    }

    return true;
}

bool 
rlRosHttpFilter::CanFilterRequest() const
{
    return m_Request.m_Encryption != RLROS_ENCRYPTION_NONE;
}

bool 
rlRosHttpFilter::CanFilterResponse() const
{
    //We need to filter the response even if encryption is turned off
    //e.g. if we want to verify a content hash or signature (so that we can compute a sha1 of the response)
    return m_Response.m_Encryption != RLROS_ENCRYPTION_NONE 
            || m_Response.m_HaveContentHash
            || m_Response.m_HaveContentSignature
            || (m_Request.m_OriginalSecurityFlags & (RLROS_SECURITY_REQUEST_CONTENT_SIG | RLROS_SECURITY_REQUIRE_CONTENT_SIG));
}

u32 
rlRosHttpFilter::DelayMs() const
{
	return 0;
}

bool 
rlRosHttpFilter::CanIntercept() const
{
	return false;
}

bool 
rlRosHttpFilter::FilterRequest(const u8* data,
                               const unsigned dataLen,
                               const bool finalCall,
                               datGrowBuffer& output)
{
#ifdef RLROS_RIVETBEGIN_1
RLROS_RIVETBEGIN_1
#endif

    bool success = false;

    rtry
    {
        rverify(m_Request.m_Encryption == RLROS_ENCRYPTION_TITLE,
                catchall,
                rlError("FilterRequest called, but encryption method is %d", m_Request.m_Encryption));                 

        if (dataLen)
        {
            //Write the preamble, if necessary
            if (!m_Request.m_HaveWrittenPreamble)
            {
                // Our combined hash, which is a combination of a random plaintext key,
                // our title-specific encryption key, and a session key that we securely
                // negotiated with the server
                u8 key[KEY_LEN];

                //Generate a random key, which will be included in the request
                u8 randomKey[KEY_LEN];
                netRandom rng(sysTimer::GetSystemMsTime());
                for(int i = 0; i < sizeof(randomKey); i++)
                {
                    randomKey[i] = (u8)rng.GetRanged(0, 255);
                    key[i] = randomKey[i];
                }

                //Combine the plaintext and secret keys to get the one used for encryption
                u8 httpKey[KEY_LEN];
                g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetKey(httpKey, sizeof(httpKey));

                for(int i = 0; i < sizeof(key); i++)
                {
                    key[i] ^= (u8)httpKey[i % sizeof(httpKey)];
                }

                //Combine with our session key if we have one
                if (m_HaveSessionKey)
                {
                    for (int i = 0; i < sizeof(key); i++)
                    {
                        key[i] ^= m_SessionKey.GetKeyBuffer()[i % m_SessionKey.GetCount()];
                    }
                }

                //Write the random key as plaintext
                rverify(output.AppendOrFail(randomKey, sizeof(randomKey)), 
                        catchall, 
                        rlError("Failed to append plaintext key"));

                //If protecting our body with an hmac, go ahead and reset it now that we know our shared secret key
                //(randomKey ^ titleKey ^ sessionKey)
                if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_BODY)
                {
                    m_Request.m_Sha1.Clear(key, sizeof(key));
                }

                //Update our hash with out plaintext random key
                m_Request.m_Sha1.Update(randomKey, sizeof(randomKey));

                //Reset our encryption context with our combined shared secret key
                m_Request.m_Rc4.Reset(key, sizeof(key));

                m_Request.m_HaveWrittenPreamble = true;
            }

            //Write the data to output			
			unsigned offset = output.Length();	

			rverify(output.AppendOrFail(data, dataLen), 
					catchall,
					rlError("Failed to append %d bytes to output (%d len, %d capacity, %" SIZETFMT "u largest available)",
							dataLen,
							output.Length(),
							output.GetCapacity(),
							g_rlAllocator->GetLargestAvailableBlock()));

			u8* payload = (u8*)output.GetBuffer();
			payload += offset;

			//Encrypt it
			m_Request.m_Rc4.Encrypt(payload, dataLen);

			//Update our hash
			m_Request.m_Sha1.Update(payload, dataLen);
        }

        //If we wrote anything, and we're done sending then append a hash.
        if (m_Request.m_HaveWrittenPreamble && finalCall)
        {
            //Add salt to end of hash
            u8 salt[SALT_LEN];
            g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetSalt(salt, sizeof(salt));
            m_Request.m_Sha1.Update((const u8*)salt, sizeof(salt));

            //Compute final hash and append to output
            u8 sha1Hash[Sha1::SHA1_DIGEST_LENGTH];
            m_Request.m_Sha1.Final(sha1Hash);

            rverify(output.AppendOrFail(sha1Hash, sizeof(sha1Hash)), 
                    catchall, 
                    rlError("Failed to append hash to end of request"));
        }

        success = true;
    }
    rcatchall
    {
    }

#ifdef RLROS_RIVETEND_1
RLROS_RIVETEND_1
#endif
    
    return success;
}

bool 
rlRosHttpFilter::FilterResponse(const u8* data,
                                const unsigned dataLen,
                                const bool allDataReceived,
                                fiHandle& outputHandle,
                                const fiDevice* outputDevice,
                                unsigned* numProcessed,
                                bool* hasDataPending)
{
#ifdef RLROS_RIVETBEGIN_2
RLROS_RIVETBEGIN_2
#endif

    //rlDebug3("FilterReponse (%d bytes, allDataReceived = %s)", dataLen, allDataReceived ? "TRUE" : "FALSE");

    bool success = false;

    rtry
    {
        if (m_Response.m_Encryption == RLROS_ENCRYPTION_TITLE)
        {
            rverify(m_Response.m_Encryption == RLROS_ENCRYPTION_TITLE,
                    catchall,
                    rlError("FilterResponse called, but encryption method is %d", m_Response.m_Encryption));                 

            unsigned remaining = dataLen;
            *numProcessed = 0;

            //Read preamble, if necessary
            if (!m_Response.m_HaveReadPreamble && (remaining >= ResponseData::PREAMBLE_LEN))
            {
                //Read the plaintext key
                sysMemCpy(m_Response.m_Key, &data[ResponseData::KEY_OFFSET], sizeof(m_Response.m_Key));
                *numProcessed += sizeof(m_Response.m_Key);

                //Combine it with our secret to get the final key
                u8 httpKey[KEY_LEN];
                g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetKey(httpKey, sizeof(httpKey));

                for(int i = 0; i < sizeof(m_Response.m_Key); i++)
                {
                    m_Response.m_Key[i] ^= (u8)httpKey[i % sizeof(httpKey)];
                }

                //Combine with our session key if we have one
                if (m_HaveSessionKey)
                {
                    for (int i = 0; i < sizeof(m_Response.m_Key); i++)
                    {
                        m_Response.m_Key[i] ^= m_SessionKey.GetKeyBuffer()[i % m_SessionKey.GetCount()];
                    }
                }

                m_Response.m_Rc4.Reset(m_Response.m_Key, sizeof(m_Response.m_Key));

                //Read the (encrypted) block size
                s32 blockSize;
                sysMemCpy(&blockSize,(u8*) &data[ResponseData::BLOCK_SIZE_OFFSET], sizeof(blockSize));
                m_Response.m_Rc4.Encrypt((u8*) &blockSize, sizeof(blockSize));

                *numProcessed += sizeof(blockSize);

                blockSize = sysEndian::BtoN(blockSize);
                // rlVerify that the block size matches what we expect.
                // TODO: Backend needs to be updated to use the right block size based on our user agent string
                rverify(blockSize == s_RoSBlockSize,
                        catchall,
                        rlError("rlRosHttpFilter::FilterResponse: Invalid block size (%d bytes), expected (%d bytes) for data: %s",
                                blockSize,
                                s_RoSBlockSize,
                                (const char*)data));

                m_Response.m_HaveReadPreamble = true;

                //rlDebug3("FilterResponse: Read preamble (%d bytes)", *numProcessed);
            }

            if (m_Response.m_HaveReadPreamble)
            {
                bool bRecBufferIsDecrypted = false;
                u8 recBuffer[s_RoSFullBlockSize];
                unsigned recBufferLen = 0;

                while (true)
                {
                    //If we have decrypted data, write as much as possible of it to our output device.
                    if (bRecBufferIsDecrypted)
                    {
                        int count = outputDevice->Write(outputHandle, 
                                                        recBuffer, 
                                                        recBufferLen);
                        if (count > 0)
                        {
                            //rlDebug3("FilterResponse: Wrote %d bytes to output device", count);

    #if !__NO_OUTPUT
							if (!m_bDisableHttpDump)
							{
								DumpHttp("Inbound content", (const char*)recBuffer, count);
							}
    #endif

                            // Update our response hash based on what we processed
                            m_Response.m_ResponseSha1.Update(recBuffer, count);

                            datGrowBuffer::Remove(recBuffer, 
                                                  &recBufferLen,
                                                  0, 
                                                  count);

                            bRecBufferIsDecrypted = (recBufferLen > 0);
                        }
                        else
                        {
                            //rlDebug3("FilterResponse: Nothing could be written to the output device");
                        }

                        // This is fatal now since we don't buffer the decrypted data and as a result
                        // it is now lost
                        rverify(bRecBufferIsDecrypted == false,
                                catchall,
                                rlError("Output device failed to receive response, %d bytes remaining", recBufferLen));
                    }

                    rlAssert(!bRecBufferIsDecrypted);

                    //Copy unfiltered data to our receive buffer
                    remaining = dataLen - *numProcessed;

                    // Only consume data if we have enough to decrypt.
                    // We rely on the caller to buffer enough of whatever
                    // we don't consume to eventually give us a full block to decrypt
                    if (remaining > 0
                        && (remaining + recBufferLen >= s_RoSFullBlockSize
                            || allDataReceived))
                    {
                        unsigned bytesToReceive = Min(remaining, COUNTOF(recBuffer) - recBufferLen);

                        if (bytesToReceive)
                        {
                            sysMemCpy(&recBuffer[recBufferLen],
                                      &data[*numProcessed],
                                      bytesToReceive);

                            recBufferLen += bytesToReceive;
                            *numProcessed += bytesToReceive;

                            //rlDebug3("FilterResponse: Added %d to receive buffer (%d now)",
                            //         bytesToReceive, m_Response.m_RecBufferLen);
                        }
                    }
                    else
                    {
                        // Not enough data to decrypt, so stop
                        break;
                    }

                    // At this point, we should have enough to decrypt
                    rlAssert(recBufferLen > 0);
                    rlAssert(recBufferLen == s_RoSFullBlockSize || allDataReceived && recBufferLen <= s_RoSFullBlockSize);

                    //Decrypt the block.
                    unsigned blockSize = recBufferLen;
                
                    rverify(blockSize > Sha1::SHA1_DIGEST_LENGTH, 
                            catchall, 
                            rlError("Block is too small to be valid (%d bytes)", blockSize));

                    unsigned payloadLen = blockSize - Sha1::SHA1_DIGEST_LENGTH;

                    //rlVerify the block's hash/hmac based on our security flags
                    Sha1 sha1;
                    if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_BODY)
                    {
                        //Reset to HMAC-SHA1
                        sha1.Clear(m_Response.m_Key, sizeof(m_Response.m_Key));
                    }

                    sha1.Update(recBuffer, payloadLen);

                    //Include our challenge
                    if (m_Request.m_SecurityFlags & RLROS_SECURITY_CHALLENGE)
                    {
                        sha1.Update(m_Request.m_Challenge, sizeof(m_Request.m_Challenge));
                    }

                    u8 salt[SALT_LEN];
                    g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetSalt(salt, sizeof(salt));
                    sha1.Update((const u8*)salt, sizeof(salt));

                    u8 hash[Sha1::SHA1_DIGEST_LENGTH];
                    sha1.Final(hash);

                    rverify(0 == memcmp(hash, &recBuffer[payloadLen], Sha1::SHA1_DIGEST_LENGTH),
                            catchall,
                            rlError("Block hash does not match expected"));

                    //Decrypt the payload
                    m_Response.m_Rc4.Encrypt(recBuffer, payloadLen);

                    //Strip the hash from the end, and note that we're holding decrypted data.
                    recBufferLen = payloadLen;
                    bRecBufferIsDecrypted = true;

                    //rlDebug3("FilterResponse: Decrypted %d bytes", m_Response.m_RecBufferLen);
                }
            }

            // We don't have data pending, since the caller is responsible for buffering everything
            *hasDataPending = false;

            success = true;
        }
        else
        {
            //If we're not using title encryption, just forward the response to our output device
            //unfiltered, and update our computed sha1 hash

            *hasDataPending = false;

            int count = outputDevice->Write(outputHandle, 
                                            data, 
                                            dataLen);
            if (count > 0)
            {
                //rlDebug3("FilterResponse: Wrote %d bytes to output device", count);

#if !__NO_OUTPUT
				if (!m_bDisableHttpDump)
				{
					DumpHttp("Inbound content", (const char*)data, count);
				}
#endif

                *numProcessed = count;

                // Update our response hash based on what we processed
                m_Response.m_ResponseSha1.Update(data, count);
            }
            else
            {
                *numProcessed = 0;
            }

            success = true;
        }
    }
    rcatchall
    {
    }

#ifdef RLROS_RIVETEND_2
RLROS_RIVETEND_2
#endif

    return success;
}

const char* 
rlRosHttpFilter::GetUserAgentString() const
{
    return m_UserAgentString;
}

bool
rlRosHttpFilter::ProcessRequestHeader(class netHttpRequest* request)
{
    rtry
    {
        static const char* s_SecurityFlagsHeader = "ros-SecurityFlags";
        static const char* s_SessionTicketHeader = "ros-SessionTicket";
        static const char* s_ChallengeHeader = "ros-Challenge";

        //Hmac for our headers if m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_REQUEST_HEADERS
        //so that the server can verify they haven't been tampered with
        Sha1 headerHmac;
        const u8 fieldSeparator[1] = { 0x00 };
        if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_REQUEST_HEADERS)
        {
            //Compute our shared secret
            u8 key[KEY_LEN] = {0};
            g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetKey(key, sizeof(key));

            //Combine with our session key if we have one
            if (m_HaveSessionKey)
            {
                for (int i = 0; i < sizeof(key); i++)
                {
                    key[i] ^= m_SessionKey.GetKeyBuffer()[i % m_SessionKey.GetCount()];
                }
            }

            //Reset our hmac with our secret key
            headerHmac.Clear(key, sizeof(key));

            //The header hmac always includes the http verb and request path so that they can't be tampered with
            const char* httpVerb = request->GetHttpVerbString();
            headerHmac.Update((const u8*)httpVerb, (unsigned)strlen(httpVerb));

            if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
            {
                headerHmac.Update(fieldSeparator, NELEM(fieldSeparator));
            }

            const char* requestPath = request->GetPath();
            headerHmac.Update((const u8*)requestPath, (unsigned)strlen(requestPath));

            if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
            {
                headerHmac.Update(fieldSeparator, NELEM(fieldSeparator));
            }
        }

        // Include any security flags in our outgoing header if we have some
        if (m_Request.m_SecurityFlags != RLROS_SECURITY_NONE)
        {
            char tmp[16];
            formatf(tmp, "%d", m_Request.m_SecurityFlags);
            rverify(request->AddRequestHeaderValue(s_SecurityFlagsHeader, tmp),
                    catchall,
                    rlError("Failed to add security flags"));

            //Include in our hmac if we should
            if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_REQUEST_HEADERS)
            {
                headerHmac.Update((const u8*)tmp, (unsigned)strlen(tmp));
            }
        }
        if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
        {
            headerHmac.Update(fieldSeparator, NELEM(fieldSeparator));
        }

        // Include our session ticket in the request so that the server
        // knows how to decrypt/encrypt it (derive our session key)
        if (m_HaveSessionKey)
        {
#if __RGSC_DLL
			// L.A. Noire doesn't use session tickets (SC DLL)
			if(strcmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "lanoire") != 0)
#endif
			{
				rverify(request->AddRequestHeaderValue(s_SessionTicketHeader, m_SessionTicket),
						catchall,
						rlError("Failed to add session ticket"));

				//Include in our hmac if we should
				if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_REQUEST_HEADERS)
				{
					headerHmac.Update((const u8*)m_SessionTicket, (unsigned)strlen(m_SessionTicket));
				}
			}
        }
        if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
        {
            headerHmac.Update(fieldSeparator, NELEM(fieldSeparator));
        }

        // If using a challenge, generate a random challenge for the server
        // to answer (by signing with our session key)
        if (m_Request.m_SecurityFlags & RLROS_SECURITY_CHALLENGE)
        {
            // It's important for this to be unique during our session not cryptographically random,
            // since it is signed with a session-specific key, so just use a nonce
            u64 nonce = netGenerateNonce();

            CompileTimeAssert(sizeof(nonce) == sizeof(m_Request.m_Challenge));
            sysMemCpy(m_Request.m_Challenge, &nonce, sizeof(nonce));

            unsigned encodedChallengeSize = datBase64::GetMaxEncodedSize(sizeof(m_Request.m_Challenge));
            char* encodedChallenge = Alloca(char, encodedChallengeSize);
            unsigned encodedChallengeLen;
            rverify(datBase64::Encode(m_Request.m_Challenge, sizeof(m_Request.m_Challenge), encodedChallenge, encodedChallengeSize, &encodedChallengeLen),
                    catchall,
                    rlError("Failed to encode challenge"));
            rverify(request->AddRequestHeaderValue(s_ChallengeHeader, encodedChallenge),
                    catchall,
                    rlError("Failed to add challenge header"));

            //Include in our hmac if we should
            if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_REQUEST_HEADERS)
            {
                headerHmac.Update((const u8*)encodedChallenge, (unsigned)strlen(encodedChallenge));
            }
        }
        if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
        {
            headerHmac.Update(fieldSeparator, NELEM(fieldSeparator));
        }

        //Include an hmac of our headers if requested so that the server can verify they haven't
        //been tampered with
        if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_REQUEST_HEADERS)
        {
            //Append our salt
            u8 salt[SALT_LEN];
            g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetSalt(salt, sizeof(salt));
            headerHmac.Update(salt, sizeof(salt));

            //Compute the final hmac
            headerHmac.Final(m_Request.m_HeaderHmacDigest);

            //Encode it and include it in a header. The server will verify that the hmac
            //in the header matches the parts of the header we included in it. We also verify
            //the server got our header hmac by including it in the response header hmac
            unsigned encodedHmacSize = datBase64::GetMaxEncodedSize(Sha1::SHA1_DIGEST_LENGTH);
            char* encodedHmac = Alloca(char, encodedHmacSize);
            unsigned encodedHmacLen;
            rverify(datBase64::Encode(m_Request.m_HeaderHmacDigest, Sha1::SHA1_DIGEST_LENGTH, encodedHmac, encodedHmacSize, &encodedHmacLen),
                    catchall,
                    rlError("Failed to encode header hmac"));
            rverify(request->AddRequestHeaderValue("ros-HeadersHmac", encodedHmac),
                    catchall,
                    rlError("Failed to add hmac header"));
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlRosHttpFilter::ProcessResponseHeader(const int statusCode, const char* header)
{
    rtry
    {
        char* locationHeader = NULL;
        unsigned locationHeaderLen = 0;
        char* contentSigHeader = NULL;
        unsigned contentSigHeaderLen = 0;
        char* contentHashHeader = NULL;
        unsigned contentHashHeaderLen = 0;
        char* headersHmacHeader = NULL;
        unsigned headersHmacHeaderLen = 0;
            
        //Parse all of our headers
        if (header)
        {
            // Location
            netHttpRequest::GetHeaderValue(header, "Location", NULL, &locationHeaderLen);
            if (locationHeaderLen > 0)
            {
                locationHeader = Alloca(char, ++locationHeaderLen);
                rverify(netHttpRequest::GetHeaderValue(header, "Location", locationHeader, &locationHeaderLen),
                        catchall,
                        rlError("Failed to read location header"));
            }

            // Content signature
            netHttpRequest::GetHeaderValue(header, "ros-ContentSignature", NULL, &contentSigHeaderLen);
            if (contentSigHeaderLen > 0)
            {
                contentSigHeader = Alloca(char, ++contentSigHeaderLen);
                rverify(netHttpRequest::GetHeaderValue(header, "ros-ContentSignature", contentSigHeader, &contentSigHeaderLen),
                        catchall,
                        rlError("Failed to read content signature header"));
                
            }

            // Content hash
            netHttpRequest::GetHeaderValue(header, "ros-ContentHash", NULL, &contentHashHeaderLen);
            if (contentHashHeaderLen > 0)
            {
                contentHashHeader = Alloca(char, ++contentHashHeaderLen);
                rverify(netHttpRequest::GetHeaderValue(header, "ros-ContentHash", contentHashHeader, &contentHashHeaderLen),
                        catchall,
                        rlError("Failed to read content hash header"));
            }
            
            // Headers hmac
            netHttpRequest::GetHeaderValue(header, "ros-HeadersHmac", NULL, &headersHmacHeaderLen);
            if (headersHmacHeaderLen > 0)
            {
                headersHmacHeader = Alloca(char, ++headersHmacHeaderLen);
                rverify(netHttpRequest::GetHeaderValue(header, "ros-HeadersHmac", headersHmacHeader, &headersHmacHeaderLen),
                        catchall,
                        rlError("Failed to read hmac header"));
            }
        }
        
        //rlVerify our headers hmac if our security flags require one
        //It must be present unless we countered a serious error or didn't want one
        //We still require an hmac for 404s
        rverify(!(m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_HEADERS)
                || headersHmacHeader != NULL
                || (statusCode >= 400 && statusCode != NET_HTTPSTATUS_NOT_FOUND),
                catchall,
                rlError("Missing expected headers hmac"));
        if (headersHmacHeader != NULL)
        {
            const u8 fieldSeparator[1] = { 0x00 };

            //Decode it
            unsigned hmacLen = 0;
            u8 hmac[Sha1::SHA1_DIGEST_LENGTH];
            rverify(datBase64::Decode(headersHmacHeader, sizeof(hmac), hmac, &hmacLen)
                    && hmacLen == Sha1::SHA1_DIGEST_LENGTH,
                    catchall,
                    rlError("Failed to decode headers hmac"));

            //Compute our shared secret
            u8 key[KEY_LEN] = {0};
            g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetKey(key, sizeof(key));

            //Combine with our session key if we have one
            if (m_HaveSessionKey)
            {
                for (int i = 0; i < sizeof(key); i++)
                {
                    key[i] ^= m_SessionKey.GetKeyBuffer()[i % m_SessionKey.GetCount()];
                }
            }

            //Compute our headers hmac and verify that it matches
            Sha1 hmacSha1(key, sizeof(key));
            //Headers protected with an hmac in order are location, content signature, and content hash
            const char* hmacHeaders[3] = {locationHeader, contentSigHeader, contentHashHeader};
            for (int i = 0; i < NELEM(hmacHeaders); i++)
            {
                if (hmacHeaders[i] != NULL)
                {
                    hmacSha1.Update((const u8*)hmacHeaders[i], (unsigned)strlen(hmacHeaders[i]));
                }

                if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
                {
                    hmacSha1.Update(fieldSeparator, NELEM(fieldSeparator));
                }
            }

            //Include the response code if we're expecting it
            if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_STATUS_CODE)
            {
                //Convert the status code to network byte order for consistency with the server
                const int nboStatusCode = sysEndian::NtoB(statusCode);
                hmacSha1.Update((const u8*)&nboStatusCode, sizeof(nboStatusCode));

                if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
                {
                    hmacSha1.Update(fieldSeparator, NELEM(fieldSeparator));
                }
            }

            //Include our challenge/nonce if we issued one
            if (m_Request.m_SecurityFlags & RLROS_SECURITY_CHALLENGE)
            {
                hmacSha1.Update(m_Request.m_Challenge, sizeof(m_Request.m_Challenge));

                if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
                {
                    hmacSha1.Update(fieldSeparator, NELEM(fieldSeparator));
                }
            }

            //And lastly include the hmac from our request headers, so that we can
            //verify the server didn't process our request with a tampered header
            if (m_Request.m_SecurityFlags & RLROS_SECURITY_HMAC_REQUEST_HEADERS)
            {
                hmacSha1.Update(m_Request.m_HeaderHmacDigest, sizeof(m_Request.m_HeaderHmacDigest));

                if (m_Request.m_SecurityFlags & RLROS_SECURITY_STRUCTURED_HEADER_HMAC)
                {
                    hmacSha1.Update(fieldSeparator, NELEM(fieldSeparator));
                }
            }

            //Finally our salt
            u8 salt[SALT_LEN];
            g_rlTitleId->m_RosTitleId.GetTitleSecrets().GetSalt(salt, sizeof(salt));
            hmacSha1.Update(salt, sizeof(salt));
            
            //And actually verify the hmac matches
            u8 expectedHmac[Sha1::SHA1_DIGEST_LENGTH];
            hmacSha1.Final(expectedHmac);
            rverify(memcmp(expectedHmac, hmac, Sha1::SHA1_DIGEST_LENGTH) == 0,
                    catchall,
                    rlError("Server headers hmac did not match expected"));
        }
        
        //Grab our content signature if it's there. If it's missing and we don't get one
        //from a subsequent redirect then we'll fail the task later in AllowSucceeded
        if (contentSigHeader != NULL)
        {
            //Decode it
            rverify(datBase64::Decode(contentSigHeader, 
                                      NELEM(m_Response.m_ExpectedContentSignature.m_ContentSignature), 
                                      m_Response.m_ExpectedContentSignature.m_ContentSignature, 
                                      &m_Response.m_ExpectedContentSignature.m_Length),
                    catchall,
                    rlError("Failed to decode content signature"));

            //Set our flag to indicate that we should verify our content signature,
            //which will happen before allowing the request to succeed in AllowSucceeded
            m_Response.m_HaveContentSignature = true;
        }

        //Grab our content hash
        if (contentHashHeader != NULL)
        {
            //Decode it
            unsigned contentHashLen;
            rverify(datBase64::Decode(contentHashHeader, 
                                      NELEM(m_Response.m_ExpectedContentHash), 
                                      m_Response.m_ExpectedContentHash, 
                                      &contentHashLen)
                    && contentHashLen == Sha1::SHA1_DIGEST_LENGTH,
                    catchall,
                    rlError("Failed to decode content hash"));

            // Set our flag to indicate that we should verify our content hash
            m_Response.m_HaveContentHash = true;
        }


        //If we're being redirected to a URL outside of ROS or a pre-v11 URL,
        //then we need to assume that we won't be encrypted or have our security flags
        //
        //304 means "not modified".  Sent in response to a request containing
        //the "If-Modified-Since" header.
        if (NET_HTTPSTATUS_NOT_MODIFIED != statusCode
            && statusCode >= 300 && statusCode < 400)
        {
            if (locationHeader != NULL)
            {
                const char* rosUrl = stristr(locationHeader, RLROS_BASE_DOMAIN_NAME);
                if (NULL == rosUrl)
                {
                    //If this is not a ROS URL, then we must assume the response is unencrypted
                    rlDebug("Redirect to non ROS URL, defaulting to unencrypted response: '%s'", locationHeader);
                    m_Response.m_DefaultEncryption = RLROS_ENCRYPTION_NONE;
                    //Likewise we can't use any of our security flags anymore
                    m_Request.m_SecurityFlags = RLROS_SECURITY_NONE;
                }           
                else
                {
                    //Otherwise, we need to see if it's a pre-v11 ROS URL, which 
                    //would be before encryption was implemented in cloud (which is
                    //the only service that might redirect to pre-v11).
                    //NOTE: ELIMINATE THIS ONCE WE DON'T POINT ANYTHING TO V10 ANYWHERE
                    const char* temp = rosUrl + strlen(RLROS_BASE_DOMAIN_NAME);

                    const char* versionStart = NULL;
                    int slashCount = 0;
                    while(*temp != '\0')
                    {
                        if ((*temp == '/') || (*temp == '\\'))
                        {
                            ++slashCount;
                            if (slashCount == 2)
                            {
                                versionStart = temp + 1;
                            }
                            else if (slashCount == 3)
                            {
                                *((char*)temp) = '\0';
                                int version;
                                if (1 == sscanf(versionStart, "%d", &version))
                                {
                                    if (version < 11)
                                    {
                                        rlDebug("Redirect to pre-v11 ROS URL, defaulting to unencrypted response: '%s'", locationHeader);
                                        m_Response.m_DefaultEncryption = RLROS_ENCRYPTION_NONE;
                                        //Likewise we can't use any of our security flags anymore
                                        m_Request.m_SecurityFlags = RLROS_SECURITY_NONE;
                                    }
                                }
                                else
                                {
                                    rlError("Failed to parse version from Location value");
                                }
                                break;
                            }
                        }

                        ++temp;
                    }
                }
            }
            else
            {
                rlWarning("rlRosHttpFilter::ProcessResponseHeader: Redirected, but could not get Location value from header");
            }
        }
        else
        {
            //Configure our encryption based on whether or not this was a serious
            //error
            if (statusCode >= 400)
            {
                m_Response.m_Encryption = RLROS_ENCRYPTION_NONE;
            }
            else
            {
                //Use default encryption
                m_Response.m_Encryption = m_Response.m_DefaultEncryption;
            }
        }

        //TODO: This isn't secure currently. Do we want to keep this?
        /*
        //Check if the Encryption Is Fucked cookie is present and valid.
        //If it is, that indicates future requests should be unencrypted.
        if (header)
        {
            const char* COOKIE_ALLOW_UNENCRYPTED = "ros-eif";

            //See if there is cookie indicating that unencrypted requests are allowed.
            //If they are, rlRosHttpTask will start sending requests unencrypted.

            bool allowUnencrypted = false;

            char cookieValue[128];
            unsigned cookieValueLen = sizeof(cookieValue);
            if (netHttpRequest::GetCookieValue(header, 
                                               COOKIE_ALLOW_UNENCRYPTED, 
                                               cookieValue, 
                                               &cookieValueLen))
            {
                //rlVerify that the cookie value is the SHA1 hash of the user-agent
                //from the original request.  If it is, then allow unencrypted 
                //requests from now on.
                unsigned userAgentLen = istrlen(GetUserAgentString());

                if (userAgentLen)
                {
                    const u8* userAgentBytes = (const u8*)GetUserAgentString();

                    Sha1 sha1;
                    sha1.Update(userAgentBytes, userAgentLen);

                    u8 hash[Sha1::SHA1_DIGEST_LENGTH];
                    sha1.Final(hash);

                    //Decode the cookie value.  It should match our user-agent hash.
                    int maxCookieBytes = datBase64::GetMaxDecodedSize(cookieValue);
                    u8* cookieBytes = (u8*)alloca(maxCookieBytes);
                    if (!cookieBytes)
                    {
                        rlError("Failed to alloc %d cookie bytes", maxCookieBytes);
                        return;
                    }

                    unsigned cookieBytesLen;
                    if (!datBase64::Decode(cookieValue, maxCookieBytes, cookieBytes, &cookieBytesLen))
                    {
                        rlError("Failed to decode cookie value (%s)", cookieValue);
                        return;
                    }

                    if (cookieBytesLen != Sha1::SHA1_DIGEST_LENGTH)
                    {
                        rlError("Decode cookie length (%d) is not SHA1 length", cookieBytesLen);
                        return;
                    }

                    if (memcmp(hash, cookieBytes, Sha1::SHA1_DIGEST_LENGTH))
                    {
                        rlError("Decoded cookie != user-agent SHA1 hash");
                        return;
                    }

                    allowUnencrypted = true;
                }
            }

            //Update the "allow encrypted" flag for rlRosHttpTasks.
            {
                SYS_CS_SYNC(sm_Cs);

                if (sm_AllowUnencrypted != allowUnencrypted)
                {
                    rlDebug("%sALLOWING unencrypted requests now",
                        allowUnencrypted ? "" : "NOT ");
                }

                sm_AllowUnencrypted = allowUnencrypted;
            }  
        }
        */

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlRosHttpFilter::AllowSucceeded(class netHttpRequest* request)
{
    const int statusCode = request->GetStatusCode();

    rtry
    {
        //If we originally required a content signature and didn't get one,
        //don't allow succeeding with anything < 400 or a 404 (we still require a signature for a 404)
        rverify(!(m_Request.m_OriginalSecurityFlags & RLROS_SECURITY_REQUIRE_CONTENT_SIG)
                || m_Response.m_HaveContentSignature
                || (statusCode >= 400 && statusCode != NET_HTTPSTATUS_NOT_FOUND),
                catchall,
                rlError("Missing required content signature"));

        //If we got a content hash or signature and didn't encounter a serious error (other than 404), then verify the content
        //we received matches
        //For HEAD requests, we don't receive a response body so don't attempt to verify it. We still require a signature
        //above if one was required.
        if (request->GetHttpVerb() != NET_HTTP_VERB_HEAD)
        {
            if ((statusCode == NET_HTTPSTATUS_NOT_FOUND || (statusCode < 400 && statusCode != NET_HTTPSTATUS_NOT_MODIFIED))
                && (m_Response.m_HaveContentSignature || m_Response.m_HaveContentHash))
            {
                //Compute the hash of what we received
                u8 hash[Sha1::SHA1_DIGEST_LENGTH];
                m_Response.m_ResponseSha1.Final(hash);

                //If we have a content hash to verify, do it
                if (m_Response.m_HaveContentHash)
                {
                    //We shouldn't have a content hash if there was a 404
                    rverify(statusCode != NET_HTTPSTATUS_NOT_FOUND,
                            catchall,
                            rlError("Received content hash for 404"));

                    rverify(memcmp(m_Response.m_ExpectedContentHash, hash, Sha1::SHA1_DIGEST_LENGTH) == 0,
                            catchall,
                            rlError("Content hash does not match expected"));

                    rlDebug1("Content hash matched");
                }

                //If we have a content signature to verify, do it if we have a delegate set.
                //Otherwise callers are responsible for manually verifying when we complete
                if (m_Response.m_HaveContentSignature
                    && m_VerifySignatureDelegate.IsBound())
                {
                    if (statusCode == NET_HTTPSTATUS_NOT_FOUND)
                    {
                        //404s are a special case, since the file shouldn't exist we need to use a null filehash.
                        //We may have actually been returned some content explaining the 404, but callers are expected
                        //to ignore it
                        rverify(m_VerifySignatureDelegate(m_Response.m_ExpectedContentSignature, NULL, 0),
                                catchall,
                                rlError("Content signature does not match expected"));
                    }
                    else
                    {
                        rverify(m_VerifySignatureDelegate(m_Response.m_ExpectedContentSignature, hash, Sha1::SHA1_DIGEST_LENGTH),
                                catchall,
                                rlError("Content signature does not match expected"));
                    }

                    rlDebug1("Content signature matched");
                }
            }
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

void
rlRosHttpFilter::SetVerifySignatureDelegate(const VerifySignatureDelegate& dlgt)
{
    m_VerifySignatureDelegate = dlgt;
}

bool
rlRosHttpFilter::VerifySignature(const rlRosContentSignature& signature, 
                                 const rlRosSignatureType signatureType, 
                                 const u8* resource, 
                                 const unsigned resourceLen,
                                 const u8* hash,
                                 const unsigned hashLen)
{
    rtry
    {
        //Get our public rsa key to verify the signature with
        const u8* rsaKey = NULL;
        unsigned rsaKeyLen;
        rverify(g_rlTitleId->m_RosTitleId.GetPublicRsaKey(&rsaKey, &rsaKeyLen), 
                catchall,
                rlError("Failed to get public RSA key"));

        //Compute our expected hash, which is a hash of the content hash and resource identifier
        //(e.g. request url)
        Sha1 sha1;
        u8 combinedHash[Sha1::SHA1_DIGEST_LENGTH];
        u8 rawSigtype = static_cast<u8>(signatureType);
        sha1.Update(&rawSigtype, 1);
        sha1.Update(resource, resourceLen);
        sha1.Update(hash, hashLen);
        sha1.Final(combinedHash);

        //Create a copy since the verification decrypts in place
        rlRosContentSignature signatureCopy = signature;
        rcheck(Rsa::VerifySha1(rsaKey, rsaKeyLen, signatureCopy.m_ContentSignature, signatureCopy.m_Length, combinedHash),
               catchall,
               rlError("Failed to verify signature"));

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlRosHttpFilter::GetContentSignature(rlRosContentSignature* signature) const
{
    *signature = m_Response.m_ExpectedContentSignature;

    return m_Response.m_HaveContentSignature;
}

bool
rlRosHttpFilter::GetContentHash(u8 (&hash)[Sha1::SHA1_DIGEST_LENGTH]) const
{
    sysMemCpy(hash, m_Response.m_ExpectedContentHash, Sha1::SHA1_DIGEST_LENGTH);

    return m_Response.m_HaveContentHash;
}

///////////////////////////////////////////////////////////////////////////////
//  rlRosHttpTask
///////////////////////////////////////////////////////////////////////////////
rlRosHttpTask::rlRosHttpTask(sysMemAllocator* allocator, u8* bounceBuffer, const unsigned sizeofBounceBuffer)
: rlHttpTask(allocator, bounceBuffer, sizeofBounceBuffer)
, m_LocalGamerIndex(NO_LOCAL_GAMER)
, m_Service(GAME_SERVICES)
, m_ParserTree(NULL)
{
    // We only consume filter response data once we reach our encryption block size
    // (i.e. we have enough to decrypt). Because of this, we rely on the request itself
    // to buffer data in its bounce buffer until we reach our encryption block size.
    // Because of this, the size of our bounce buffer must be at least as large as our
    // full block size.
    rlAssert(bounceBuffer == NULL || sizeofBounceBuffer >= s_RoSFullBlockSize);
}

rlRosHttpTask::~rlRosHttpTask()
{
}

bool
rlRosHttpTask::Configure(const int localGamerIndex)
{
    rlHttpTaskConfig config;
    return Configure(localGamerIndex, &config);
}

bool
rlRosHttpTask::Configure(const int localGamerIndex, parTree** parserTree)
{
    rtry
    {
        rlHttpTaskConfig config;
        rverify(Configure(localGamerIndex, &config),
                catchall,
                rlTaskError("Failed to configure task"));

        m_ParserTree = parserTree;

        return true;
    }
    rcatchall
    {
    }

    return false;
}

bool
rlRosHttpTask::Configure(const int localGamerIndex, rlRosSaxReader* saxReader)
{
    rlHttpTaskConfig config;
    config.m_SaxReader = saxReader;

    if(saxReader) { saxReader->m_Task = this; }

    return Configure(localGamerIndex, &config);
}

bool
rlRosHttpTask::Configure(const int localGamerIndex, const rlHttpTaskConfig* config)
{
    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) || (localGamerIndex == NO_LOCAL_GAMER),
                catchall,
                rlTaskError("Illegal local gamer index: %d", localGamerIndex))

        m_LocalGamerIndex = localGamerIndex;

        rlHttpTaskConfig finalConfig;
        if(config)
        {
            finalConfig = *config;
        }

		// Use the default service for this request.
		m_Service = sm_DefaultService;

        if(!finalConfig.m_Filter && UseFilter())
        {
            char url[1024];

            BuildUrl(url);
            rlAssert(strlen(url) > 0);

            rcheck(m_Filter.Configure(localGamerIndex, url, GetSecurityFlags()), catchall, rlTaskError("Failed to configure filter"));

            finalConfig.m_Filter = &m_Filter;
        }

        //Always use the Ros Ssl context for ros http tasks
        m_HttpRequest.SetSslContext(rlRos::GetRosSslContext());

        rverify(rlHttpTask::Configure(&finalConfig),
                catchall,
                rlTaskError("Failed to configure base class"))

        return true;
    }
    rcatchall
    {
    }

    return false;
}

int 
rlRosHttpTask::GetLocalGamerIndex() const
{
    return m_LocalGamerIndex;
}

bool
rlRosHttpTask::UseHttps() const
{
    char servicePath[1024];
    if (GetServicePath(servicePath, sizeof(servicePath)))
    {
        //See if this service should be executed behind SSL by default
        return rlRos::IsSslService(servicePath);
    }

	return false;
}

SSL_CTX*
rlRosHttpTask::GetSslCtx() const
{
    return rlRos::GetRosSslContext();
}

bool
rlRosHttpTask::UseFilter() const
{
	return true;
}

const char* rlRosHttpTask::GetServiceSet() const
{
	switch(m_Service)
	{
	case LAUNCHER_SERVICES:
		return "launcherservices";
	case GAME_SERVICES:
	default:
		return "gameservices";
	}
}

rlRosSecurityFlags rlRosHttpTask::GetSecurityFlags() const
{
	return g_rlTitleId->m_RosTitleId.GetDefaultSecurityFlags();
}

const char* 
rlRosHttpTask::GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
{
    //Look up the host name from the service we're calling
    char servicePath[1024];
    const char* hn = NULL;
    if(GetServicePath(servicePath, sizeof(servicePath)))
    {
        hn = rlRos::GetServiceHost(servicePath, hostnameBuf, sizeofBuf);
    }

    if(!hn)
    {
        //If no service/hostname mapping was found then just use the default.
		const char* prefix = g_rlTitleId->m_RosTitleId.GetDomainNamePrefix();
		const char* domainName = g_rlTitleId->m_RosTitleId.GetDomainName();

		if (UseHttps() && !StringNullOrEmpty(prefix))
		{
			hn = formatf(hostnameBuf, sizeofBuf, "%s-%s", prefix, domainName);
		}
		else
		{
			hn = safecpy(hostnameBuf, domainName, sizeofBuf);
		}
    }

    return hn;
}

bool
rlRosHttpTask::GetServicePath(char* svcPath, const unsigned bufSize) const
{
	const rlRosTitleId& titleId = g_rlTitleId->m_RosTitleId;
	formatf(svcPath, bufSize, "%s/%d/%s/%s", titleId.GetTitleName(), titleId.GetTitleVersion(), GetServiceSet(), GetServiceMethod());
    return true;
}

static bool
ParseStdResponse(const parTreeNode* root, rlRosResult& result)
{
    rlAssert(root);

    rtry
    {
        int status = 0;
        rverify(rlHttpTaskHelper::ReadInt(status, root, "Status", NULL), 
            catchall, 
            rlError("No <Status> element in response"));

        result.m_Succeeded = (status == 1);

        const parTreeNode* errorRoot = root->FindChildWithNameIgnoreNs("Error");
        if(errorRoot)
        {
			//The .asmx services return Code And CodeEx as attributes of <Error>,
			//but the WCF services return them as child elements. Check for both.
            result.m_Code = rlHttpTaskHelper::ReadString(errorRoot, NULL, "Code");
			if (NULL == result.m_Code)
			{
				result.m_Code = rlHttpTaskHelper::ReadString(errorRoot, "Code", NULL);
			}

            result.m_CodeEx = rlHttpTaskHelper::ReadString(errorRoot, NULL, "CodeEx");
			if (NULL == result.m_CodeEx)
			{
				result.m_CodeEx = rlHttpTaskHelper::ReadString(errorRoot, "CodeEx", NULL);
			}

            result.m_Ctx = rlHttpTaskHelper::ReadString(errorRoot, "Context", NULL);
            result.m_Location = rlHttpTaskHelper::ReadString(errorRoot, "Location", NULL);
            result.m_Msg = rlHttpTaskHelper::ReadString(errorRoot, "Msg", NULL);
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlRosHttpTask::ProcessRosResult(const rlRosResult result, 
                                const parTreeNode* node, 
                                int &resultCode)
{
    rlTaskDebug("ROS result: %s", result.m_Succeeded ? "SUCCESS" : "ERROR");
    if(result.m_Code)       { rlTaskDebug("   Code   = %s", result.m_Code); }
    if(result.m_CodeEx)     { rlTaskDebug("   CodeEx = %s", result.m_CodeEx); }
    if(result.m_Ctx)        { rlTaskDebug("   Ctx    = %s", result.m_Ctx); }
    if(result.m_Location)   { rlTaskDebug("   Loc    = %s", result.m_Location); }
    if(result.m_Msg)        { rlTaskDebug("   Msg    = %s", result.m_Msg); }

    resultCode = result.m_Succeeded ? 0 : -1;

    if(result.m_Succeeded)
    {
        return ProcessSuccess(result, node, resultCode);
    }
    else
    {
        ProcessError(result, node, resultCode);

		rlRos::ProcessCommonErrors(m_LocalGamerIndex, result);

		//If ProcessError changed the resultCode to 0, that means the error
		//is really considered a warning.
		if (resultCode == 0)
		{
			rlTaskDebug("Error is actually just a warning; treating as success");
			return true;
		}

		return false;
    }
}

bool 
rlRosHttpTask::ProcessResponse(const char* response, int& resultCode)
{
    bool success = false;
    rlRosResult result;
    parTree* tree = 0;

    resultCode = 0;

    INIT_PARSER;

    rtry
    {
        rcheck(response, catchall, rlError("Null response body"));

        // treat the response as a memory file so we can use the XML parser
        char filename[64];
        fiDeviceMemory::MakeMemoryFileName(filename, sizeof(filename), response, strlen(response), false, NULL);

        bool origSafeButSlow = PARSER.Settings().GetFlag(parSettings::READ_SAFE_BUT_SLOW);
        PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
        //Don't parse special attributes, none of our ros services use these and not sure why this would be the default
        bool origProcessSpecialAttrs = PARSER.Settings().GetFlag(parSettings::PROCESS_SPECIAL_ATTRS);
        PARSER.Settings().SetFlag(parSettings::PROCESS_SPECIAL_ATTRS, false);
        tree = PARSER.LoadTree(filename, "xml");
        PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, origSafeButSlow);
        PARSER.Settings().SetFlag(parSettings::PROCESS_SPECIAL_ATTRS, origProcessSpecialAttrs);

        rcheck(tree && tree->GetRoot(), catchall, rlTaskError("Failed to load tree"));

        rcheck(ParseStdResponse(tree->GetRoot(), result),
               catchall,
               rlTaskError("ParseStdResponse failed"));

        success = ProcessRosResult(result, tree->GetRoot(), resultCode);
    }
    rcatchall
    {
    }

    if(m_ParserTree)
    {
        //The caller asked to have the parTree back so
        //give it to him.
        *m_ParserTree = tree;
    }
    else
    {
        //Done with the parTree - delete it.
        delete tree;
    }
    tree = NULL;

    SHUTDOWN_PARSER;

    return success;
}

bool 
rlRosHttpTask::ProcessSuccess(const rlRosResult& NET_ASSERTS_ONLY(result), const parTreeNode*, int& resultCode)
{
    netAssert(result.m_Succeeded);
    resultCode = 0;
    return true;
}

void
rlRosHttpTask::ProcessError(const rlRosResult& NET_ASSERTS_ONLY(result), const parTreeNode* /*node*/, int& resultCode)
{
    netAssert(!result.m_Succeeded);
    resultCode = -1;
}

bool
rlRosHttpTask::SaxStartElement(const char* /*name*/)
{
    return true;
}

bool
rlRosHttpTask::SaxAttribute(const char* /*name*/, const char* /*val*/)
{
    return true;
}

bool
rlRosHttpTask::SaxCharacters(const char* /*ch*/, const int /*start*/, const int /*length*/)
{
    return true;
}

bool
rlRosHttpTask::SaxEndElement(const char* /*name*/)
{
    return true;
}

//////////////////////////////////////////////////////////////////////////
//  rlRosSaxReader
//////////////////////////////////////////////////////////////////////////
rlRosSaxReader::rlRosSaxReader()
: m_Task(NULL)
, m_State(STATE_EXPECT_RESPONSE)
{
    m_StatusChars[0] = m_ResultCodeBuf[0] = m_ResultCodeExBuf[0] = '\0';
}

void
rlRosSaxReader::startDocument()
{
}

void
rlRosSaxReader::endDocument()
{
    //If we didn't successfully parse everything, create a special error.
    if(STATE_PARSE_FAILED == m_State)
    {
        const char* SAX_PARSING_ERROR_CODE = "_SaxParsingError";
        
        this->m_RosResult.m_Succeeded = false;
        this->m_RosResult.m_Code = SAX_PARSING_ERROR_CODE;
    }

    int resultCode;
    if(m_Task->ProcessRosResult(this->m_RosResult, NULL, resultCode))
    {
        m_Task->Finish(rlTaskBase::FINISH_SUCCEEDED, resultCode);
    }
    else
    {
        m_Task->Finish(rlTaskBase::FINISH_FAILED, resultCode);
    }
}

void
rlRosSaxReader::startElement(const char* /*url*/, const char* /*localName*/, const char* qName)
{
    switch((int)m_State)
    {
    case STATE_EXPECT_RESPONSE:
        if(0 == stricmp("response", qName))
        {
            m_State = STATE_EXPECT_STATUS;
        }
        else
        {
            rlError("Expected <Response> but read <%s>", qName);
            m_State = STATE_PARSE_FAILED;
        }
        break;
    
    case STATE_EXPECT_STATUS:
        if(0 == stricmp("status", qName))
        {
            m_State = STATE_READ_STATUS;
        }
        else
        {
            rlError("Expected <Status> but read <%s>", qName);
            m_State = STATE_PARSE_FAILED;
        }
        break;

    case STATE_EXPECT_ERROR:
        if(0 == stricmp("error", qName))
        {
            m_State = STATE_READ_ERROR;
        }
        else
        {
            rlError("Expected <Error> but read <%s>", qName);
            m_State = STATE_PARSE_FAILED;
        }
        break;
    
    case STATE_READ_RESULTS:
        if (!m_Task->SaxStartElement(qName))
        {
            m_State = STATE_PARSE_FAILED;
        }
        break;
    }
}

void
rlRosSaxReader::attribute(const char* /*tagName*/, const char* attrName, const char* attrVal)
{
    switch((int)m_State)
    {
    case STATE_READ_ERROR:
        if(0 == stricmp("code", attrName))
        {
            rlAssert(sizeof(m_ResultCodeBuf) > strlen(attrVal));
            safecpy(m_ResultCodeBuf, attrVal);
            m_RosResult.m_Code = m_ResultCodeBuf;
        }
        else if(0 == stricmp("codeex", attrName))
        {
            rlAssert(sizeof(m_ResultCodeExBuf) > strlen(attrVal));
            safecpy(m_ResultCodeExBuf, attrVal);
            m_RosResult.m_CodeEx = m_ResultCodeExBuf;
        }
        break;

    case STATE_READ_RESULTS:
        if(!m_Task->SaxAttribute(attrName, attrVal))
        {
            m_State = STATE_PARSE_FAILED;
        }
        break;
    }
}

void
rlRosSaxReader::characters(const char* ch, const int start, const int length)
{
    switch((int)m_State)
    {
    case STATE_READ_STATUS:
        {
            //Buffer the element's inner text
            const int slen = (int)strlen(m_StatusChars);
            if(rlVerify(slen + length < (int)sizeof(m_StatusChars)-1))
            {
                sysMemCpy(&m_StatusChars[slen], &ch[start], length);
                m_StatusChars[slen+length] = '\0';
            }
        }
        break;

    case STATE_READ_RESULTS:
        if(!m_Task->SaxCharacters(ch, start, length))
        {
            m_State = STATE_PARSE_FAILED;
        }
        break;
    }
}

void
rlRosSaxReader::endElement(const char* /*url*/, const char* /*localName*/, const char* qName)
{
    switch((int)m_State)
    {
    case STATE_READ_STATUS:
        if(0 == stricmp("status", qName))
        {
            int status;
            if(rlVerify(1 == sscanf(m_StatusChars, "%d", &status)))
            {
                m_RosResult.m_Succeeded = (1 == status);
                if(m_RosResult.m_Succeeded)
                {
                    rlTaskDebugPtr(m_Task, "ROS result: SUCCESS");
                    m_State = STATE_READ_RESULTS;
                }
                else
                {
                    rlTaskDebugPtr(m_Task, "ROS result: FAILED");
                    m_State = STATE_EXPECT_ERROR;
                }
            }
            else
            {
                rlError("Error parsing <Status> inner text");
                m_RosResult.m_Succeeded = false;
                m_State = STATE_PARSE_FAILED;
            }
        }
        break;
    
    case STATE_READ_ERROR:
        if(0 == stricmp("error", qName))
        {
            m_State = STATE_PARSE_SUCCEEDED;
        }
        break;
    
    case STATE_READ_RESULTS:
        if(stricmp("response", qName))
        {
            if (!m_Task->SaxEndElement(qName))
            {
                m_State = STATE_PARSE_FAILED;
            }
        }
        break;
    }
}

///////////////////////////////////////////////////////
// rlRosJsonHttpTask
///////////////////////////////////////////////////////
rlRosJsonHttpTask::rlRosJsonHttpTask()
	: m_LocalGamerIndex(RL_INVALID_GAMER_INDEX)
{
}

bool
rlRosJsonHttpTask::Configure(const int localGamerIndex)
{
	rtry
	{
		rlHttpTaskConfig config;
		config.m_AddDefaultContentTypeHeader = false;
		config.m_HttpVerb = NET_HTTP_VERB_POST;
		rcheck(rlHttpTask::Configure(&config), catchall, rlTaskError("Failed to configure base class"));

		m_HttpRequest.AddRequestHeaderValue("Content-Type", "application/json; charset=utf-8");
		m_LocalGamerIndex = localGamerIndex;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
rlRosJsonHttpTask::AddRequestHeader(const char* name, const char* value)
{
	return AddRequestHeaderValue(name, value);
}

bool
rlRosJsonHttpTask::SetContent(const char* json, const unsigned jsonLength)
{
	return m_HttpRequest.AppendContent(json, jsonLength);
}

const char* 
rlRosJsonHttpTask::GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
{
	//Look up the host name from the service we're calling
	char servicePath[1024];
	const char* hn = NULL;
	if (GetServicePath(servicePath, sizeof(servicePath)))
	{
		hn = rlRos::GetServiceHost(servicePath, hostnameBuf, sizeofBuf);
	}

	if (!hn)
	{
		//If no service/hostname mapping was found then just use the default.
		const char* prefix = g_rlTitleId->m_RosTitleId.GetDomainNamePrefix();
		const char* domainName = g_rlTitleId->m_RosTitleId.GetDomainName();

		if (UseHttps() && !StringNullOrEmpty(prefix))
		{
			hn = formatf(hostnameBuf, sizeofBuf, "%s-%s", prefix, domainName);
		}
		else
		{
			hn = safecpy(hostnameBuf, domainName, sizeofBuf);
		}
	}

	return hn;
}

const char*
rlRosJsonHttpTask::GetUrlServiceId() const
{
	return nullptr;
}

bool
rlRosJsonHttpTask::GetServicePath(char* svcPath, const unsigned bufSize) const
{
	const rlRosTitleId& titleId = g_rlTitleId->m_RosTitleId;
	formatf( svcPath, bufSize, "%s/%d/wcfgameservices/%s", titleId.GetTitleName(), titleId.GetTitleVersion(), GetServiceMethod());
	return true;
}

bool
rlRosJsonHttpTask::UseHttps() const
{
	return true;
}

SSL_CTX* rlRosJsonHttpTask::GetSslCtx() const
{
	return rlRos::GetRosSslContext();
}

bool rlRosJsonHttpTask::ProcessResponse(const char* response, int& resultCode)
{
	resultCode = -1;

	rtry
	{
		rverifyall(response);

		RsonReader responseNode;
		responseNode.Init(response, istrlen(response));

		rlRosJsonResult result;

		int status;
		rverify(responseNode.ReadInt("Status", status), catchall, );
		result.m_Succeeded = (status == 1);

		RsonReader errorNode;
		if (responseNode.GetMember("Error", &errorNode))
		{
			errorNode.ReadString("Code", result.codeStr);
			errorNode.ReadString("CodeEx", result.codeExStr);
			errorNode.ReadString("Msg", result.msgStr);
			errorNode.ReadString("Location", result.locationStr);
			errorNode.ReadString("Context", result.ctxStr);
		}

		rlTaskDebug("ROS result: %s", result.m_Succeeded ? "SUCCESS" : "ERROR");
		if (!StringNullOrEmpty(result.m_Code))		{	rlTaskDebug("   Code   = %s", result.m_Code); }
		if (!StringNullOrEmpty(result.m_CodeEx))	{	rlTaskDebug("   CodeEx = %s", result.m_CodeEx); }
		if (!StringNullOrEmpty(result.m_Ctx))		{	rlTaskDebug("   Ctx    = %s", result.m_Ctx); }
		if (!StringNullOrEmpty(result.m_Location))	{	rlTaskDebug("   Loc    = %s", result.m_Location); }
		if (!StringNullOrEmpty(result.m_Msg))		{	rlTaskDebug("   Msg    = %s", result.m_Msg); }

		if (result.m_Succeeded)
		{
			RsonReader resultNode;
			rverifyall(responseNode.GetMember("Result", &resultNode));

			return ProcessSuccess(result, &resultNode, resultCode);
		}
		else 
		{
			ProcessError(result, &errorNode, resultCode);
			rlRos::ProcessCommonErrors(m_LocalGamerIndex, result);

			//If ProcessError changed the resultCode to 0, that means the error
			//is really considered a warning.
			if (resultCode == 0)
			{
				rlTaskDebug("Error is actually just a warning; treating as success");
				return true;
			}

			return false;
		}
	}
	rcatchall
	{
		return false;
	}
}

bool rlRosJsonHttpTask::ProcessSuccess(const rlRosJsonResult& NET_ASSERTS_ONLY(result), RsonReader* /*resultNode*/, int& resultCode)
{
	rlAssert(result.m_Succeeded);
	resultCode = 0;
	return true;
}

void rlRosJsonHttpTask::ProcessError(const rlRosJsonResult& NET_ASSERTS_ONLY(result), RsonReader* /*resultNode*/, int& resultCode)
{
	rlAssert(!result.m_Succeeded);
	resultCode = -1;
}

}; //namespace rage
