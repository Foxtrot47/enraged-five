// 
// rline/rlrosclient.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrosclient.h"
#include "rlros.h"
#include "rlroscredtasks.h"
#include "rline/rlpresence.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "system/timer.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline_ros, client)
#undef __rage_channel
#define __rage_channel rline_ros_client

#define DEFAULT_CRED_MIN_DELAY_SECS 15
PARAM(rlRosCredMinDelaySecs, "Minimum delay before a cred refresh");

#define DEFAULT_CRED_MAX_DELAY_SECS (60 * 3)
PARAM(rlRosCredMaxDelaySecs, "Maximum delay before a cred refresh");

extern sysMemAllocator* g_rlAllocator;

#if __RGSC_DLL
// Since both the DLL and game can refresh credentials and share a ticket, lets have the DLL refresh
// first and pass the results down to the game before the game attempts to refresh its ticket.
// The launcher should refresh afterwards, but hopefully will not be necessary.
#define DEFAULT_CRED_PREFETCH_SECS 90
#define LAUNCHER_CRED_PREFRETCH_SECS 60
#else
#define DEFAULT_CRED_PREFETCH_SECS 60
#endif
PARAM(rlRosCredPrefetchSecs, "Secs before current cred expiration to start refresh");

XPARAM(netFakeGamerName);

#if RSG_DURANGO && !__FINAL
XPARAM(xuid);
#endif // RSG_DURANGO && !__FINAL

//////////////////////////////////////////////////////////////////////////
//  rlRosClient::RefreshCredTask
//////////////////////////////////////////////////////////////////////////
rlRosClient::RefreshCredTask::RefreshCredTask()
{
    Reset();
}

rlRosClient::RefreshCredTask::~RefreshCredTask()
{
    Reset();
}

void
rlRosClient::RefreshCredTask::Reset()
{
    if(this->IsPending())
    {
        this->Cancel();
    }

    m_RosCred.Clear();

	m_ResponseBuf.Init(g_rlAllocator, datGrowBuffer::Flags::NULL_TERMINATE);
}

bool 
rlRosClient::RefreshCredTask::Configure(const int localGamerIndex, rlRosPlatformCredentials* platformCred)
{
    Reset();

    rlRosCreateTicketTask* task = NULL;

    rtry
    {
        rverify(platformCred->IsValid(), catchall, );

		//Create and configure our subtask based on the platform and configuration
#if RSG_DURANGO
#if !__FINAL
		// If we get gamertag and Xuid from the command line, impersonate that account
		if (PARAM_xuid.Get() || PARAM_netFakeGamerName.Get())
		{
			task = rlGetTaskManager()->CreateTask<rlRosImpersonateCreateTicketXblTask>();
		}
		else
#endif // __FINAL
		{
			task = rlGetTaskManager()->CreateTask<rlRosCreateTicketXbl3Task>();
		}
#elif RSG_ORBIS
#if !__FINAL
        if(!rlRos::UseCreateTicketNp3())
#endif
        {
            task = rlGetTaskManager()->CreateTask<rlRosCreateTicketNp4Task>();
        }
#if !__FINAL
        else
        {
            task = rlGetTaskManager()->CreateTask<rlRosCreateTicketNp3Task>();
        }
#endif
#elif __RGSC_DLL
		if (GetRgscConcreteInstance()->GetTitleId()->GetPlatform() == ITitleId::PLATFORM_STEAM)
		{
			task = rlGetTaskManager()->CreateTask<rlRosCreateTicketScSteam2Task>();
		}
		else
		{
			// If we're renewing credentials, re-use the ticket with CreateTicketSc3 to conserve session id.
			// If we need new credentials, create new ones with CreateTicketScAuthToken
			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			if (cred.IsValid())
			{
				task = rlGetTaskManager()->CreateTask<rlRosCreateTicketSc3Task>();
			}
			else
			{
				task = rlGetTaskManager()->CreateTask<rlRosCreateTicketScAuthTokenTask>();
			}
		}
#elif __STEAM_BUILD
		task = rlGetTaskManager()->CreateTask<rlRosCreateTicketScSteam2Task>();
#elif RLROS_SC_PLATFORM
		const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
		if (cred.IsValid())
		{
			task = rlGetTaskManager()->CreateTask<rlRosCreateTicketSc3Task>();
		}
		else
		{
			task = rlGetTaskManager()->CreateTask<rlRosCreateTicketScAuthTokenTask>();
		}
#else
		rlTaskError("Unsupported platform");
#endif

        rverify(task,catchall,rlTaskError("Failed to allocate CreateTicketTask"));

        rcheck(rlTaskBase::Configure(
				task,
				localGamerIndex,
				platformCred,
				&m_RosCred,
				&m_ResponseBuf,
				&m_MyStatus),
            catchall,
            rlTaskError("Failed to configure task"));

        rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

        return true;
    }
    rcatchall
    {
        if(task)
        {
            rlGetTaskManager()->DestroyTask(task);
        }
        Reset();
        return false;
    }
}

void 
rlRosClient::RefreshCredTask::DoCancel()
{
    if(m_MyStatus.Pending())
    {
        rlGetTaskManager()->CancelTask(&m_MyStatus);
    }    
}

void 
rlRosClient::RefreshCredTask::Update(const unsigned timeStep)
{
    rlTaskBase::Update(timeStep);

    if(m_MyStatus.Succeeded())
    {
        Finish(WasCanceled() ? FINISH_CANCELED : FINISH_SUCCEEDED);
    }
    else if(!m_MyStatus.Pending())
    {
        Finish(FINISH_FAILED);
    }
}

//////////////////////////////////////////////////////////////////////////
// rlRosClient
//////////////////////////////////////////////////////////////////////////
rlRosClient::rlRosClient()
: m_Initialized(false)
, m_LocalGamerIndex(-1)
, m_State(STATE_COUNTDOWN)
, m_NumRetries(0)
{
	//@@: location  RLROSCLIENT_RLROSCLIENT
}

rlRosClient::~rlRosClient()
{
    this->Shutdown();
}

bool
rlRosClient::Init(int localGamerIndex)
{
    rlAssert(!m_Initialized);

    m_LocalGamerIndex = localGamerIndex;
    m_Initialized = true;

    return m_Initialized;
}

void
rlRosClient::Shutdown()
{    
    if(m_Initialized)
    {
        m_RefreshCredTask.Cancel();
        m_LocalGamerIndex = -1;
        m_RosCred.Clear();
        m_PlatformCred.Clear();
        m_Initialized = false;
    }
}

void
rlRosClient::Update(const unsigned timeStep)
{
    //If our platform credentials have changed, clear our ROS credentials,
    //cancel any pending refresh task, and set our delay to a minimum range
    //so that we'll sign in again quickly once platfrom creds are valid again.
    //FIXME(KB) - do this only when a gamer signs in/out
    rlRosPlatformCredentials platformCred;
    rlRosPlatformCredentials::GetLocalPlatformCredentials(m_LocalGamerIndex, &platformCred);

    if((m_PlatformCred.IsValid() || platformCred.IsValid()) && (m_PlatformCred != platformCred))
    {
        if(m_RefreshCredTask.IsActive())
        {
            rlDebug("Platform credentials have changed; cancelling refresh task");
            m_RefreshCredTask.Cancel();
        }

        if(m_RosCred.IsValid())
        {
            rlDebug("Platform credentials have changed; invalidating ROS credentials");
            SetCredentials(0);
        }
        else
        {
            RenewCredentials();
        }

        m_PlatformCred = platformCred;

        m_State = STATE_COUNTDOWN;
    }

    //Count down the time remaining on our credentials.
    if (m_RosCred.m_ValidMs)
    {
        if (m_RosCred.m_ValidMs > timeStep)
        {
            m_RosCred.m_ValidMs -= timeStep;
        }
        else
        {
            rlDebug("ROS credentials have expired; invalidating them");
            SetCredentials(0);

			//Flatten after SetCredentials - otherwise we won't dispatch change
			//events for delegates. 
			m_RosCred.m_ValidMs = 0;
        }
    }

    //Update our current state...
    switch(m_State)
    {
    case STATE_COUNTDOWN:
        //Countdown until it is time to refresh.
        //Don't refresh credentials if this is a guest account
        if(m_PlatformCred.IsValid()
            && !rlPresence::IsGuest(m_LocalGamerIndex))
        {
            m_CreateTicketTimer.Update();

            if(m_CreateTicketTimer.IsTimeToRetry())
            {
#if __RGSC_DLL
				// DLL: If not signed in, reloading or in offline mode, refreshing ticket is not possible. 
				// Retry to force a scaled increase of the create ticket timer.
				if (!GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn() ||
					GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUi() ||
					(GetRgscConcreteInstance()->_GetUiInterface()->IsReadyToAcceptCommands() && 
					 GetRgscConcreteInstance()->_GetUiInterface()->IsOfflineMode()))
				{
					Retry();
				}
				else
#elif RSG_NP || RSG_DURANGO || RSG_PC
                //Can't refresh creds unless we're online with the online
                //service.
                //To be considered online for ROS we need a ticket, so only
                //do this for non-ROS online services.
                if(!rlPresence::IsOnline(m_LocalGamerIndex))
                {
                    Retry();
                }
                else
#else
                //FIXME (KB)
                //In this spot we used to check for the existence of the relay
                //server to determine if we were online.  We can no longer do that
                //because we require a ticket in order to discover the relay server.

                //Perhaps we should do a host resolution on X.ros.rockstargames.com
                //to dertermine if we're online.
#endif
                //Start a refresh task.  If this fails, backoff and resume countdown.
                if(rlTaskBase::Configure(&m_RefreshCredTask, GetLocalGamerIndex(), &m_PlatformCred, &m_RefreshCredStatus)
                    && rlVerify(rlGetTaskManager()->AddParallelTask(&m_RefreshCredTask)))
                {
                    m_State = STATE_REFRESH_PENDING;
                }
                else
                {
                    Retry();
                }
            }
        }
        break;

    case STATE_REFRESH_PENDING:
        //On success, set our credentials.
        if(m_RefreshCredStatus.Succeeded())
        {
            rlDebug("Refresh succeeded");

            SetCredentials(&m_RefreshCredTask.m_RosCred);

#if __RGSC_DLL
			GetRgscConcreteInstance()->HandleNotification( rgsc::NOTIFY_ROS_TICKET_CHANGED, m_RefreshCredTask.GetXmlResponse());
#endif

            m_State = STATE_COUNTDOWN;
        }
        
        //If the task was canceled (probably because credentials changed), 
        //then just go back to countdown state.
        else if(m_RefreshCredStatus.Canceled())
        {
            rlDebug("Refresh was cancelled");
            m_State = STATE_COUNTDOWN;
        }

        //On failure, increment our failure delay and set the countdown to next attempt.
        else if(m_RefreshCredStatus.Failed())
        {
            rlDebug("Refresh failed");

            rlRosEventGetCredentialsResult e(m_LocalGamerIndex, false);
            rlRos::DispatchEvent(e);

            Retry();
            m_State = STATE_COUNTDOWN;
        }
    }
}

rlRosLoginStatus 
rlRosClient::GetLoginStatus() const
{
    //If we have valid platform and ROS credentials, we are considered online.
    if (m_PlatformCred.IsValid() && m_RosCred.IsValid()) 
    {
        return RLROS_LOGIN_STATUS_ONLINE;
    }
    else if (!m_PlatformCred.IsValid()) 
    {
        return RLROS_LOGIN_STATUS_NEED_PLATFORM_CREDENTIALS;
    }
    else if (m_State == STATE_COUNTDOWN) 
    {
        return RLROS_LOGIN_STATUS_COUNTING_DOWN;
    }
    else if (m_State == STATE_REFRESH_PENDING)
    {
        return RLROS_LOGIN_STATUS_IN_PROGRESS;
    }

    return RLROS_LOGIN_STATUS_INVALID;
}

int 
rlRosClient::GetMsUntilNextLoginAttempt() const
{
    if (GetLoginStatus() == RLROS_LOGIN_STATUS_COUNTING_DOWN)
    {
        //Don't allow this to return a negative result; that's reserved
        //for "wrong state to be asking this in!".
        return Max(m_CreateTicketTimer.GetMillisecondsUntilRetry(), 0);
    }
    return -1;
}

const rlRosCredentials&
rlRosClient::GetCredentials() const
{
    return m_RosCred;
}

bool
rlRosClient::SetCredentials(const rlRosCredentials* cred)
{
    m_NumRetries = 0;

    rtry
    {
        rverify(!cred || cred->IsValid(),
            catchall,
            rlError("Cred must be either null or non-null and valid"));

        //Cancel any active refresh task so it won't overwrite this change when it completes.
        if(m_RefreshCredTask.IsActive())
        {
            rlDebug("SetCredentials: Cancelling active refresh cred task");
            m_RefreshCredTask.Cancel();
        }

        //If this is a no-op, return immediately.
        if(!cred && !m_RosCred.IsValid())
        {
            rlDebug("SetCredentials: No-op (null)");
            return true;
        }
        else if(cred && (*cred == m_RosCred))
        {
            rlDebug("SetCredentials: No-op (same)");
            return true;
        }

        //If cred is valid, make sure platform credentials are valid.
        if(cred)
        {
            rlRosPlatformCredentials::GetLocalPlatformCredentials(m_LocalGamerIndex, &m_PlatformCred);
            rverify(m_PlatformCred.IsValid(),
                catchall,
                rlError("Cannot set ROS credentials if platform credentials are invalid"));
        }

        //Note any important changes.
        bool prevOnline = m_RosCred.IsValid();
        bool online = cred && cred->IsValid();

        PlayerAccountId prevAccountId = prevOnline ? m_RosCred.GetPlayerAccountId() : InvalidPlayerAccountId;
        PlayerAccountId accountId = online ? cred->GetPlayerAccountId() : InvalidPlayerAccountId;

        RockstarId prevRockstarId = prevOnline ? m_RosCred.GetRockstarId() : InvalidRockstarId;
        RockstarId rockstarId = online ? cred->GetRockstarId() : InvalidRockstarId;

        //Apply the new cred.  Note that this doesn't change our state 
        //so any pending refresh task will be allowed to complete.
        if(cred)
        {
            //Update our refresh time to be just before the new cred expires.
            rlDebug("SetCredentials: Updating credentials");

            m_RosCred = *cred;

            int prefetchSecs;
            if(!PARAM_rlRosCredPrefetchSecs.Get(prefetchSecs))
            {
#if __RGSC_DLL
				prefetchSecs = (GetRgscConcreteInstance()->IsLauncher()) ? LAUNCHER_CRED_PREFRETCH_SECS : DEFAULT_CRED_PREFETCH_SECS;
#else
                prefetchSecs = DEFAULT_CRED_PREFETCH_SECS;
#endif
            }

            unsigned ms = m_RosCred.GetMsUntilExpiration();
            unsigned prefetchMs = (unsigned)(prefetchSecs * 1000);
            if(ms > prefetchMs) ms -= prefetchMs;

            m_CreateTicketTimer.InitMilliseconds(ms);

            rlDebug("rlRosClient[%d]::SetCredentials: Next credentials request in %d seconds",
                     m_LocalGamerIndex, 
                     m_CreateTicketTimer.GetSecondsUntilRetry());
        }
        else
        {
            rlDebug("SetCredentials: Clearing credentials");
            m_RosCred.Clear();

            //Set timer to next create ticket attempt; if we are clearing
            //because our ticket failed authentication, it may currently
            //be set to just before the ticket was to expire.
            m_CreateTicketTimer.InitSeconds(0, GetFailDelaySeconds());

            rlDebug("rlRosClient[%d]::SetCredentials: Next credentials request in %d seconds",
                     m_LocalGamerIndex, 
                     m_CreateTicketTimer.GetSecondsUntilRetry());
        }

        //Dispatch change events.
        if(accountId != prevAccountId || rockstarId != prevRockstarId)
        {
            rlRosEventOnlineStatusChanged e(m_LocalGamerIndex,
                                            accountId,
                                            prevAccountId,
                                            rockstarId,
                                            prevRockstarId);
            rlRos::DispatchEvent(e);
        }

        if((prevAccountId == accountId) && (prevRockstarId != rockstarId))
        {
            rlRosEventLinkChanged e(m_LocalGamerIndex,
                                    accountId,
                                    rockstarId,
                                    prevRockstarId);
            rlRos::DispatchEvent(e);
        }

		if(cred && cred->IsValid())
		{
			//Indicate that we have valid credentials
			rlRosEventGetCredentialsResult e(m_LocalGamerIndex, true);
			rlRos::DispatchEvent(e);
		}

        return true;
    }
    rcatchall
    {
        return false;
    }
}

void
rlRosClient::RenewCredentials()
{
    m_CreateTicketTimer.InitSeconds(0, GetFailDelaySeconds());
    m_CreateTicketTimer.ForceRetry();

    rlDebug("rlRosClient[%d]::RenewCredentials: Renewing credentials now",
            m_LocalGamerIndex);
}

void 
rlRosClient::Retry()
{
    if(0 == m_NumRetries)
    {
        m_CreateTicketTimer.InitSeconds(0, GetFailDelaySeconds());
    }
    else if(m_CreateTicketTimer.GetCurrentRetryIntervalSeconds() < DEFAULT_CRED_MAX_DELAY_SECS)
    {
        m_CreateTicketTimer.ScaleRange(150); //1.5 times
    }

    ++m_NumRetries;

    m_CreateTicketTimer.Reset();

    rlDebug("rlRosClient[%d]::Retry: Next credentials request in %d seconds",
             m_LocalGamerIndex, 
             m_CreateTicketTimer.GetSecondsUntilRetry());
}

unsigned
rlRosClient::GetFailDelaySeconds()
{
    int secs;
    if(!PARAM_rlRosCredMinDelaySecs.Get(secs))
    {
        secs = DEFAULT_CRED_MIN_DELAY_SECS;
    }

    return secs;
}

}   //namespace rage
