// 
// rline/rlros.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlroscommon.h"
#include "data/base64.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/crypto.h"
#include "rline/rlpresence.h"
#include "rline/rltitleid.h"
#include "rline/socialclub/rlsocialclub.h"
#include "string/string.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

#if RSG_NP
#include "rline/rlnp.h"
#endif

#if RSG_DURANGO
#include <stdio.h>
#endif

namespace rage
{

#if RSG_DURANGO && !__FINAL
	XPARAM(xuid);
#endif // RSG_DURANGO && !__FINAL

#if __RGSC_DLL || __MASTER
	XPARAM(rlrosdomainenv);
#else
	NOSTRIP_PC_XPARAM(rlrosdomainenv);
#endif

extern const rlTitleId* g_rlTitleId;

RAGE_DEFINE_SUBCHANNEL(rline, ros)
#undef __rage_channel
#define __rage_channel rline_ros

AUTOID_IMPL(rlRosEvent);
AUTOID_IMPL(rlRosEventOnlineStatusChanged);
AUTOID_IMPL(rlRosEventLinkChanged);
AUTOID_IMPL(rlRosEventRetrievedGeoLocInfo);
AUTOID_IMPL(rlRosEventRetrievedPresenceServerInfo);
AUTOID_IMPL(rlRosEventGetCredentialsResult);
AUTOID_IMPL(rlRosEventCanReadAchievements);

struct rlEnvNameMap
{
	const char* Name;
	rlRosEnvironment Env;
};

const rlEnvNameMap s_RosEnvironmentNameMap[] =
{
	{ "rage",		RLROS_ENV_RAGE },
	{ "dev",		RLROS_ENV_DEV },
	{ "cert",		RLROS_ENV_CERT },
	{ "prod",		RLROS_ENV_PROD },
	// ADD NEW ENVIRONMENTS AFTER THIS, DO NOT CHANGE ORDER
	{ "cert2",		RLROS_ENV_CERT_2 },
	{ "ci-rage",	RLROS_ENV_CI_RAGE },
};

rlRosEnvironment rlRosGetEnvironment()
{
	rtry
	{
		rcheckall(g_rlTitleId != nullptr);
		return g_rlTitleId->m_RosTitleId.GetEnvironment();
	}
	rcatchall
	{

	}

	return RLROS_ENV_UNKNOWN;
}

rlRosEnvironment
rlRosGetForcedEnvironment()
{
	static rlRosEnvironment s_ForcedEnvironment = RLROS_ENV_UNKNOWN;
	static bool s_HasChecked = false; 

	if(s_HasChecked)
	{
		return s_ForcedEnvironment;
	}

	const char* envName = 0;
	if(PARAM_rlrosdomainenv.Get(envName))
	{
		for(int i = 0; i < COUNTOF(s_RosEnvironmentNameMap); ++i)
		{
			if(!strcmp(envName, s_RosEnvironmentNameMap[i].Name))
			{
				s_ForcedEnvironment = s_RosEnvironmentNameMap[i].Env;
				break;
			}
		}
	}

	// we only need to check this once
	s_HasChecked = true; 

	return s_ForcedEnvironment;
}

const char* rlRosGetEnvironmentAsString(const rlRosEnvironment environment)
{
	switch(environment)
	{
	case RLROS_ENV_UNKNOWN:	return "unknown"; break;
	case RLROS_ENV_RAGE:	return "rage"; break;
	case RLROS_ENV_DEV:		return "dev"; break;
	case RLROS_ENV_CERT:	return "cert"; break;
	case RLROS_ENV_PROD:	return "prod"; break;
	case RLROS_ENV_CERT_2:	return "cert2"; break;
	case RLROS_ENV_CI_RAGE:	return "ci-rage"; break;
	default:                return nullptr;
	}
}

rlEnvironment rlRosEnvironmentToRlEnvironment(const rlRosEnvironment environment)
{
    switch(environment)
    {
    case RLROS_ENV_UNKNOWN: return RL_ENV_UNKNOWN;
    case RLROS_ENV_RAGE:    return RL_ENV_UNKNOWN;
    case RLROS_ENV_DEV:     return RL_ENV_DEV;
    case RLROS_ENV_CERT:    return RL_ENV_CERT;
    case RLROS_ENV_PROD:    return RL_ENV_PROD;
    case RLROS_ENV_CERT_2:  return RL_ENV_CERT_2;
    case RLROS_ENV_CI_RAGE: return RL_ENV_CI_RAGE;
    default:                return RL_ENV_UNKNOWN;
    }
}

//////////////////////////////////////////////////////////////////////////
//  rlRosPlatformCredentials
//////////////////////////////////////////////////////////////////////////
void
rlRosPlatformCredentials::GetLocalPlatformCredentials(const int localGamerIndex,
                                                      rlRosPlatformCredentials* platformCred)
{
    rlAssert(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
    rlAssert(platformCred);

    platformCred->Clear();

    rtry
    {
        rcheck(g_rlTitleId->m_RosTitleId.GetEnvironment() != RLROS_ENV_UNKNOWN, catchall, );

#if RLROS_SC_PLATFORM
		safecpy(platformCred->m_Email, rlSocialClub::m_Email[localGamerIndex], sizeof(platformCred->m_Email));
        safecpy(platformCred->m_ScAuthToken, rlSocialClub::m_ScAuthToken[localGamerIndex], sizeof(platformCred->m_ScAuthToken));
#elif RSG_DURANGO
        rcheck(rlPresence::IsOnline(localGamerIndex), catchall, );
		rcheck(!rlPresence::IsGuest(localGamerIndex), catchall, );

        //Get gamerinfo and copy XUID and gamertag.
        rlGamerInfo gamerInfo;
        rverify(rlPresence::GetGamerInfo(localGamerIndex, &gamerInfo), catchall, );

		platformCred->m_Xuid = gamerInfo.GetGamerHandle().GetXuid();
		safecpy(platformCred->m_Gamertag, gamerInfo.GetName(), sizeof(platformCred->m_Gamertag));
#elif RSG_NP
        rcheck(rlPresence::IsOnline(localGamerIndex), catchall, );

        rlGamerHandle gh;
        rverify(rlPresence::GetGamerHandle(localGamerIndex, &gh),
                catchall,
                rlError("Error retrieving gamer handle for gamer at index: %d", localGamerIndex));

        platformCred->m_AccountId = gh.GetNpAccountId();
#else
        rlError("Not implemented for this service");
#endif
    }
    rcatchall
    {
        platformCred->Clear();
    }
}

rlRosPlatformCredentials::rlRosPlatformCredentials()
{
    Clear();
}

void 
rlRosPlatformCredentials::Clear()
{
#if RLROS_SC_PLATFORM
	m_Email[0] = '\0';
	m_ScAuthToken[0] = '\0';
#elif RSG_DURANGO 
    m_Xuid = 0;
    m_Gamertag[0] = '\0';
#elif RSG_NP
    m_AccountId = RL_INVALID_NP_ACCOUNT_ID;
#endif
}

bool 
rlRosPlatformCredentials::IsValid() const
{
#if RLROS_SC_PLATFORM
    return (m_Email[0] != '\0') && (m_ScAuthToken[0] != '\0');
#elif RSG_DURANGO
    return m_Xuid && (m_Gamertag[0] != '\0');
#elif RSG_NP
    return m_AccountId != RL_INVALID_NP_ACCOUNT_ID;
#endif
}

bool 
rlRosPlatformCredentials::operator==(const rlRosPlatformCredentials& rhs) const
{
    if(!IsValid() || !rhs.IsValid()) return false;

#if RLROS_SC_PLATFORM
    return !strcmp(m_Email, rhs.m_Email) && !strcmp(m_ScAuthToken, rhs.m_ScAuthToken);

#elif RSG_DURANGO
    return (m_Xuid == rhs.m_Xuid) && !strcmp(m_Gamertag, rhs.m_Gamertag);

#elif RSG_NP
	return (m_AccountId == rhs.m_AccountId);
#endif
}

bool 
rlRosPlatformCredentials::operator!=(const rlRosPlatformCredentials& rhs) const
{
    return !(*this == rhs);
}

rlRosPlatformCredentials& 
rlRosPlatformCredentials::operator=(const rlRosPlatformCredentials& rhs)
{
#if RLROS_SC_PLATFORM
	safecpy(m_Email, rhs.m_Email, sizeof(m_Email));
	safecpy(m_ScAuthToken, rhs.m_ScAuthToken, sizeof(m_ScAuthToken));

#elif RSG_DURANGO
    m_Xuid = rhs.m_Xuid;
    safecpy(m_Gamertag, rhs.m_Gamertag, sizeof(m_Gamertag));

#elif RSG_NP
	m_AccountId = rhs.m_AccountId;
#endif

    return *this;
}

//////////////////////////////////////////////////////////////////////////
//  rlRosResult
//////////////////////////////////////////////////////////////////////////
bool 
rlRosResult::IsError(const char* code, const char* codeEx) const
{
    if(m_Succeeded) return false;

    if(codeEx)
    {
        return rlVerifyf(m_Code, "Error code is null") 
            && m_CodeEx
            && !stricmp(code, m_Code)
            && !stricmp(codeEx, m_CodeEx);
    }
    else
    {
        return rlVerifyf(m_Code, "Error code is null") && !stricmp(code, m_Code);
    }
}

//////////////////////////////////////////////////////////////////////////
//  rlRosCredentials
//////////////////////////////////////////////////////////////////////////
rlRosCredentials::rlRosCredentials() 
{
    Clear();
}

void 
rlRosCredentials::Clear()
{
    m_Ticket[0] = '\0';
    m_SessionTicket[0] = '\0';
    m_PlayerAccountId = InvalidPlayerAccountId;
    m_RockstarAcct.Clear();
    m_Privileges.Reset();
    m_Region = 0;
    m_ValidMs = 0;
    m_SessionId = 0;
    m_SessionKey.Clear();
    m_CloudKey.Clear();
}

bool 
rlRosCredentials::IsValid() const
{
    return m_ValidMs > 0;
}

bool 
rlRosCredentials::IsScMember() const
{
    return GetRockstarId() != RL_INVALID_ROCKSTAR_ID;
}

int
rlRosCredentials::GetMsUntilExpiration() const
{
    return m_ValidMs;
}

bool 
rlRosCredentials::HasExpired() const
{
    return m_ValidMs == 0;
}

int
rlRosCredentials::GetRegion() const
{
    return m_Region;
}

PlayerAccountId 
rlRosCredentials::GetPlayerAccountId() const
{
    return IsValid() ? m_PlayerAccountId : 0;
}

RockstarId 
rlRosCredentials::GetRockstarId() const
{
    return IsValid() ? m_RockstarAcct.m_RockstarId : 0;
}

const rlScAccountInfo&
rlRosCredentials::GetRockstarAccount() const
{
    return m_RockstarAcct;
}

s64
rlRosCredentials::GetSessionId() const
{
    return m_SessionId;
}

const char*
rlRosCredentials::GetTicket() const
{
    return IsValid() ? m_Ticket : NULL;
}

void
rlRosCredentials::SetTicket(const char* ticket)
{
	safecpy(m_Ticket, ticket);
}

const char*
rlRosCredentials::GetSessionTicket() const
{
    return IsValid() ? m_SessionTicket : NULL;
}

bool
rlRosCredentials::HasPrivilege(const rlRosPrivilegeId privilegeId) const
{
    if(IsValid() && privilegeId < m_Privileges.GetNumBits())
    {
        switch(privilegeId)
        {
        case RLROS_PRIVILEGEID_BANNED:
        case RLROS_PRIVILEGEID_CREATE_TICKET:
            return m_Privileges.IsSet(privilegeId);
        default:
            //All other privileges are overridden by the BANNED privilege
            return m_Privileges.IsSet(privilegeId) && !m_Privileges.IsSet(RLROS_PRIVILEGEID_BANNED);
        }
    }

    return false;
}

#if __BANK
bool 
rlRosCredentials::SetPrivilege(const rlRosPrivilegeId privilegeId, const bool value)
{
	if(IsValid() && privilegeId < m_Privileges.GetNumBits())
	{
		m_Privileges.Set(privilegeId, value);
		return true;
	}

	return false;
}
#endif

bool 
rlRosCredentials::HasPrivilegeEndDate(const rlRosPrivilegeId privilegeId, bool* isGranted, u64* endPosixTime) const
{
    rtry
    {
        rverify(IsValid() && privilegeId >= 0 && privilegeId < NELEM(m_PrivilegeInfos), catchall, );

        const PrivilegeInfo& privilegeInfo = m_PrivilegeInfos[privilegeId];

        if (privilegeInfo.m_IsValid
            && privilegeInfo.m_HasEndDate)
        {
            if (isGranted)
            {
                *isGranted = privilegeInfo.m_IsGranted;
            }

            if (endPosixTime)
            {
                *endPosixTime = privilegeInfo.m_EndPosixTime;
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlRosCredentials::Reset(const char* ticket,
                        const unsigned validMs,
                        const int region,
                        const PlayerAccountId playerAccountId,
                        const rlScAccountInfo& rockstarAccount,
                        const s64 sessionId,
                        const atFixedBitSet<RLROS_NUM_PRIVILEGEID>& privileges,
                        const SessionKey& sessionKey,
                        const char* sessionTicket,
                        const CloudKey& cloudKey,
                        const PrivilegeInfo (&privilegeInfos)[RLROS_NUM_PRIVILEGEID])
{
    Clear();

    rtry
    {
        rverify(ticket 
            && (ticket[0] != '\0')
            && strlen(ticket) < RLROS_MAX_TICKET_SIZE,
                catchall, 
                rlError("Invalid parameters"));

        safecpy(m_Ticket, ticket);
        m_Region = region;
        m_ValidMs = validMs;
        m_PlayerAccountId = playerAccountId;
        m_RockstarAcct = rockstarAccount;
        m_SessionId = sessionId;
        m_SessionKey = sessionKey;
        m_CloudKey = cloudKey;

#if __RGSC_DLL
		// L.A. Noire doesn't use session tickets
		if(strcmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "lanoire") != 0)
#endif
		{
			rverify(strlen(sessionTicket) < RLROS_MAX_SESSION_TICKET_SIZE,
					catchall,
					rlError("Invalid session ticket length"));
		}

        safecpy(m_SessionTicket, sessionTicket);

        if(privileges.GetNumBits() > 0)
        {
            m_Privileges = privileges;
        }

        for (unsigned k = 0; k < NELEM(privilegeInfos); k++)
        {
            m_PrivilegeInfos[k] = privilegeInfos[k];
        }

        return true;
    }
    rcatchall
    {
        Clear();
        return false;
    }
}

bool 
rlRosCredentials::operator==(const rlRosCredentials& rhs) const
{
    return IsValid()
        && rhs.IsValid()
        && !strcmp(m_Ticket, rhs.m_Ticket)
        && (m_ValidMs == rhs.m_ValidMs)
        && (m_PlayerAccountId == rhs.m_PlayerAccountId)
        && (m_RockstarAcct.m_RockstarId == rhs.m_RockstarAcct.m_RockstarId)
        && (m_SessionId == rhs.m_SessionId)
        && (m_Privileges == rhs.m_Privileges)
        && (m_SessionKey == rhs.m_SessionKey)
        && !strcmp(m_SessionTicket, rhs.m_SessionTicket)
        && (m_CloudKey == rhs.m_CloudKey);
}

const rlRosCredentials::SessionKey& 
rlRosCredentials::GetSessionKey() const
{
    return m_SessionKey;
}

const rlRosCredentials::CloudKey&
rlRosCredentials::GetCloudKey() const
{
    return m_CloudKey;
}

//////////////////////////////////////////////////////////////////////////
//  rlRosPlayerInfo
//////////////////////////////////////////////////////////////////////////
rlRosPlayerInfo::rlRosPlayerInfo() : m_RockstarId(InvalidRockstarId), m_PlayerAccountId(InvalidPlayerAccountId)
{
    sysMemSet(m_Gamertag, 0, COUNTOF(m_Gamertag));
    sysMemSet(m_Nickname, 0, COUNTOF(m_Nickname));
    m_GamerHandle.Clear();
}

void 
rlRosPlayerInfo::Clear() 
{
    m_RockstarId = InvalidRockstarId;
    m_PlayerAccountId = InvalidPlayerAccountId;
    sysMemSet(m_Gamertag, 0, COUNTOF(m_Gamertag));
    sysMemSet(m_Nickname, 0, COUNTOF(m_Nickname));
    m_GamerHandle.Clear();
}

//////////////////////////////////////////////////////////////////////////
//  rlRosTitleSecrets
//////////////////////////////////////////////////////////////////////////
rlRosTitleSecrets::rlRosTitleSecrets()
{
    Clear();
}

void
rlRosTitleSecrets::Clear()
{
    sysMemSet(m_Secret, 0, sizeof(m_Secret));
}

bool 
rlRosTitleSecrets::Reset(const char* titleSecretBase64)
{
    rtry
    {
        Clear();

        unsigned decodedLen;

        rverify(titleSecretBase64, catchall, );
        rverify(datBase64::Decode(titleSecretBase64, sizeof(m_Secret), m_Secret, &decodedLen), catchall, );
        rverify(decodedLen == TOTAL_SIZE, catchall, );

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlRosTitleSecrets::GetKey(u8* buf, const unsigned bufSize) const
{
    if (rlVerifyf(bufSize == KEY_SIZE, "Buffer must be exactly KEY_SIZE"))
    {    
        Rc4Context rc4;
        rc4.Reset(&m_Secret[MASTER_KEY_OFFSET], MASTER_SECRET_SIZE);
        sysMemCpy(buf, &m_Secret[KEY_OFFSET], KEY_SIZE);
        rc4.Encrypt(buf, KEY_SIZE);
        return true;
    }
    return false;
}

bool 
rlRosTitleSecrets::GetSalt(u8* buf, const unsigned bufSize) const
{
    if(rlVerifyf(bufSize == SALT_SIZE, "Buffer must be exactly SALT_SIZE"))
    {    
        Rc4Context rc4;
        rc4.Reset(&m_Secret[MASTER_KEY_OFFSET], MASTER_SECRET_SIZE);
        sysMemCpy(buf, &m_Secret[SALT_OFFSET], SALT_SIZE);
        rc4.Encrypt(buf, SALT_SIZE);
        return true;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////
//  rlRosUserId
//////////////////////////////////////////////////////////////////////////
const char* 
rlRosUserId::FromGamerHandle(const rlGamerHandle& gamerHandle, char* buf, const int bufSize)
{
    if (rlVerifyf(gamerHandle.IsValid() && buf && bufSize, "Invalid arguments"))
    {
#if RSG_DURANGO
        if (rlVerifyf(gamerHandle.GetService() == rlGamerHandle::SERVICE_XBL, 
                      "Wrong service (actual %d, expected %d)",
                      gamerHandle.GetService(), rlGamerHandle::SERVICE_XBL))
        {
            return formatf(buf, bufSize, "%" I64FMT "d", gamerHandle.GetXuid());
        }
#elif RSG_NP
        if (rlVerifyf(gamerHandle.GetService() == rlGamerHandle::SERVICE_NP, 
                      "Wrong service (actual %d, expected %d)",
                      gamerHandle.GetService(), rlGamerHandle::SERVICE_NP))
        {
            if(rlGamerHandle::IsUsingAccountIdAsKey())
            {
                return formatf(buf, bufSize, "%" I64FMT "u", gamerHandle.GetNpAccountId());
            }
            else
            {
                return formatf(buf, bufSize, "%s", gamerHandle.GetNpOnlineId().data);
            }
        }
#elif RLROS_SC_PLATFORM
        if (rlVerifyf(gamerHandle.GetService() == rlGamerHandle::SERVICE_SC, 
                      "Wrong service (actual %d, expected %d)",
                      gamerHandle.GetService(), rlGamerHandle::SERVICE_SC))
        {
            return formatf(buf, bufSize, "%" I64FMT "d", gamerHandle.GetRockstarId());
        }
#else
        rlError("Unsupported service: %d", gamerHandle.GetService());
#endif
    }

    return NULL;
}

bool 
rlRosUserId::ToGamerHandle(const char* userId, rlGamerHandle* gamerHandle)
{
    if (rlVerifyf(userId && gamerHandle, "Invalid arguments"))
    {
        char buf[RL_MAX_GAMER_HANDLE_CHARS];

#if RLROS_SC_PLATFORM
        //UserId is the decimal linked RockstarID
        formatf(buf, sizeof(buf), "SC %s", userId);

#elif RSG_DURANGO
        //UserId is the decimal XUID
        u64 xuid;
        if (rlVerifyf(1 == sscanf(userId, "%" I64FMT "X", &xuid), "Failed to parse XUID from userId %s", userId))
        {
            formatf(buf, sizeof(buf), "XBL %" I64FMT "X", xuid);
        }

#elif RSG_NP
        //UserId is the NP online ID
        formatf(buf, sizeof(buf), "NP %d \"%s\"", (int)g_rlNp.GetEnvironment(), userId);

#else
        rlError("Unsupported platform");
        return false;
#endif

        return gamerHandle->FromString(buf);
    }

    return false;
}

} //namespace rage
