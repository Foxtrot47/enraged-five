// 
// rline/rlrostitleid.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROSTITLEID_H
#define RLINE_RLROSTITLEID_H

#include "rlroscommon.h"

#if __STEAM_BUILD
#include "../../../../suite/src/rgsc/rgsc/rgsc/public_interface/rgsc_common.h"
#endif

namespace rage
{

//PURPOSE
//  To use ROS features, a title must be assigned a ROS title ID, which is
//  unique for each combination of game+platform, and is not the same as the
//  title IDs on other platforms (like XBL).  Contact RAGE networking to get
//  ROS title IDs allocated for your game.
class rlRosTitleId
{
public:
#if RLROS_SC_PLATFORM
	//PURPOSE
	//  Ctor that specifies which environment to run in.
    //NOTE
    //  - The environment can be overridden by the rlrosdomainenv param
    //  - For lanoire (and ONLY lanoire), it is legal to pass null for  
    //    titleSecretsBase64. Encryption is not performed for LAN.
	//  - titleDirectoryName is the name of the directory where data
	//    will be stored for the title (eg. "L.A. Noire", "Max Payne 3").
	//    This will be visible to the user when they browse the file
	//    system and is meant to be 'friendlier' than the ROS titleName.
	//    The Social Club DLL uses this name as part of the path for
	//    storing title-specific data. The name must only contain valid
	//    file system characters (the SC DLL will assert if invalid).
	rlRosTitleId(const char* titleName, 
				 const char* titleDirectoryName,
                 const int scVersion,
                 const int titleVersion,
                 const char* titleSecretsBase64,
#if RSG_PC && __STEAM_BUILD
				 const char* steamAuthTicket,
				 u32 steamAppId,
#endif
#if __MAC_APPSTORE_BUILD
				 char* macAppStoreReceipt,
				 const u32 macAppStoreReceiptLen,
#endif
                 const rlRosEnvironment env,
                 const u8* publicRsaKey,
                 const unsigned publicRsaKeyLen);

    const char* GetTitleSecretsBase64() const;
	const char* GetTitleDirectoryName() const;
#if RSG_PC && __STEAM_BUILD
	const char* GetSteamAuthTicket() const;
	u32 GetSteamAppId() const;
#endif
#if __MAC_APPSTORE_BUILD
	char* rlRosTitleId::GetMacAppStoreReceipt() const;
	u32 GetMacAppstoreReceiptLength() const;
#endif

#else
	//PURPOSE
    //  Ctor that doesn't specify an environment; the environment is determined
    //  automatically by the platform environment the player is logged into.
    //
    //  This ctor is not available on all platforms, but where it is it's the 
    //  recommended way to make submission builds, as it will correctly use
    //  the cert environment during testing and the production one after release.
    rlRosTitleId(const char* titleName, 
                 const int scVersion,
                 const int titleVersion,
                 const char* titleSecrets,
                 const u8* publicRsaKey,
                 const unsigned publicRsaKeyLen);
#endif

	// These values need to be kept consistent with the PlatformId enum on the game server
	enum PlatformId
	{
		PLATFORM_ID_INVALID = 0,
		PLATFORM_ID_PC = 8,
		PLATFORM_ID_ORBIS = 11,
		PLATFORM_ID_DURANGO = 12,
		PLATFORM_ID_IOS = 10,
		PLATFORM_ID_ANDROID = 9,
		PLATFORM_ID_GGP = 19,
		PLATFORM_ID_PROSPERO = 20,
		PLATFORM_ID_SCARLETT = 21
	};

    rlRosEnvironment GetEnvironment() const;

    const char* GetTitleName() const;
	static const char* GetPlatformName();
	static u32 GetPlatformId();
    int GetScVersion() const;
    int GetTitleVersion() const;

    //PURPOSE
    //  Returns interface to title secrets.
	const rlRosTitleSecrets& GetTitleSecrets() const;

	//PURPOSE
	//	Returns the environment prefix uses for constructing URLs.
	const char* GetEnvironmentName() const;

	//PURPOSE
    //  Returns base domain name for anything hosted under ROS
    const char* GetRosBaseDomainName() const;

	// PURPOSE
	//	Sets the title-specific prefix for the domain name.
	void SetDomainNamePrefix(const char* hostNamePrefix);
	const char* GetDomainNamePrefix() const;
	
	//PURPOSE
    //  Returns domain name used for accessing web services for this title, 
    //  platform, and environment
    const char* GetDomainName() const;

    //PURPOSE
    //  Returns domain name used for accessing cloud in the current environment.
    const char* GetCloudDomainName() const;

    //PURPOSE
    //  Fills in a reference to the encoded public rsa key for this title,
    //  and returns true if it exists
    bool GetPublicRsaKey(const u8** encodedKey, unsigned* keyLen) const;

	// PURPOSE
	//	Get/Set the security flags that ROS services should use by default
	rlRosSecurityFlags GetDefaultSecurityFlags() const;
	void SetDefaultSecurityFlags(rlRosSecurityFlags flags);

private:
    void Reset();

    bool CommonInit(const char* titleName, 
                    const int scVersion,
                    const int titleVersion,
                    const char* titleSecrets,
                    const u8* publicRsaKey,
                    const unsigned publicRsaKeyLen);

    char m_TitleName[RLROS_MAX_TITLE_NAME_SIZE];
    int m_TitleVersion;
    int m_ScVersion;
	rlRosSecurityFlags m_SecurityFlags;

    rlRosTitleSecrets m_TitleSecrets;

    const u8* m_PublicRsaKey;
    unsigned m_PublicRsaKeyLen;

#if RLROS_SC_PLATFORM
	//Needed so we can pass this info to the social club dll in rlpc.cpp
    char m_TitleSecretsBase64[rlRosTitleSecrets::TOTAL_BASE64_SIZE];
	char m_TitleDirectoryName[RLROS_MAX_TITLE_DIRECTORY_SIZE];

#if RSG_PC && __STEAM_BUILD
	char m_SteamAuthTicket[rgsc::RGSC_STEAM_TICKET_ENCODED_BUF_SIZE];
	u32 m_SteamAppId;
#endif

#if __MAC_APPSTORE_BUILD
	char* m_MacAppStoreReceipt;
	u32 m_MacAppstoreReceiptLength;
#endif
   
    rlRosEnvironment m_Env;
#endif

    mutable char m_DomainName[128];
    mutable char m_CloudDomainName[128];
	mutable char m_HostNamePrefix[64];
    mutable rlRosEnvironment m_ForcedEnv;
};

}   //namespace rage

#endif  //RLINE_RLROSTITLEID_H
