// 
// rline/rlros.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROSCOMMON_H
#define RLINE_RLROSCOMMON_H

#include "rline/rl.h"
#include "rline/rlgamerinfo.h"
#include "rline/socialclub/rlsocialclubcommon.h"
#include "data/base64.h"
#include "atl/bitset.h"
#include "data/autoid.h"
#include "net/netsocket.h"
#include "system/nelem.h"
#include "system/bit.h"

#if RSG_NP
#include "rline/rlnp.h"
#endif

namespace rage
{

//PURPOSE
//  1 on platforms that use Social Club login as platform credentials.
#define RLROS_SC_PLATFORM (RL_SC_PLATFORMS)

#define RLROS_USE_SERVICE_ACCESS_TOKENS (RSG_PC || (__BANK))

#if RLROS_USE_SERVICE_ACCESS_TOKENS
#define RLROS_USE_SERVICE_ACCESS_TOKENS_ONLY(x) x
#else
#define RLROS_USE_SERVICE_ACCESS_TOKENS_ONLY(x) 
#endif

#if RSG_PC && !__64BIT
#define RLROS_RIVETBEGIN_1 RIVETBEGIN_1
#define RLROS_RIVETEND_1 RIVETEND_1
#define RLROS_RIVETBEGIN_2 RIVETBEGIN_2
#define RLROS_RIVETEND_2 RIVETEND_2
#endif

//PURPOSE
//  Base domain name for anything hosted under ROS.
#define RLROS_BASE_DOMAIN_NAME "ros.rockstargames.com"

//PURPOSE
//  Base domain name for anything hosted on cloud.
#define RLROS_CLOUD_BASE_DOMAIN_NAME "cloud.rockstargames.com"

//PURPOSE
//  Environment in which client thinks it's operating.
//  This is used to compose URLs.
enum rlRosEnvironment
{
	// DO NOT CHANGE THE ORDER OF THESE ENVIRONMENTS - BACKWARDS COMPATIBILITY
    RLROS_ENV_UNKNOWN = -1,
    RLROS_ENV_RAGE,
    RLROS_ENV_DEV,
    RLROS_ENV_CERT,
    RLROS_ENV_PROD,
	// ADD NEW ENVIRONMENTS TO THE END OF THIS LIST - BACKWARDS COMPATIBILITY
	RLROS_ENV_CERT_2,
	RLROS_ENV_CI_RAGE
};

//PURPOSE
//  Returns the ROS environment
rlRosEnvironment rlRosGetEnvironment();

//PURPOSE
//  Returns the ROS environment specified by command line -rlrosdomainenv
//  Will be RLROS_ENV_UNKNOWN if not specific
rlRosEnvironment rlRosGetForcedEnvironment();

//PURPOSE
//  Returns the ROS environment in string format
const char* rlRosGetEnvironmentAsString(const rlRosEnvironment environment);

//PURPOSE
// Convert the rlRosEnvironment to an rlEnvironment
rlEnvironment rlRosEnvironmentToRlEnvironment(const rlRosEnvironment environment);

//PURPOSE
//  Current login status of a local gamer.
enum rlRosLoginStatus
{
    //The login status could not be determined for some reason.
    RLROS_LOGIN_STATUS_INVALID, 

    //ROS login can only begin once we have the required platform-specific
    //credentials.  This means being signed into XBL or PSN, or setting the
    //SC login info (via rlSocialClub::SetLogin()) on SC platforms.
    RLROS_LOGIN_STATUS_NEED_PLATFORM_CREDENTIALS,

    //Login attempts are throttled.  This state indicates that rlRos is
    //counting down to the next attempt.
    RLROS_LOGIN_STATUS_COUNTING_DOWN,

    //The login request is in progress, and should be completed shortly.
    RLROS_LOGIN_STATUS_IN_PROGRESS,

    //Login was successful.  rlRos::GetCredentials() should return valid credentials.
    RLROS_LOGIN_STATUS_ONLINE
};

//PURPOSE
//  Privilege IDs.  These are well-known and fixed.
//  Privileges are enforced serverside, but are provided to the client 
//  for awareness.
enum rlRosPrivilegeId
{
    RLROS_PRIVILEGEID_NONE						= 0,    //Unused
    RLROS_PRIVILEGEID_CREATE_TICKET				= 1,    //Can create ROS tickets
    RLROS_PRIVILEGEID_PROFILE_STATS_WRITE		= 2,    //Can write profile stats
    RLROS_PRIVILEGEID_MULTIPLAYER				= 3,    //Can participate in MP sessions (currently not used)
    RLROS_PRIVILEGEID_LEADERBOARD_WRITE			= 4,    //Can write to leaderboards (currently not used)
    RLROS_PRIVILEGEID_CLOUD_STORAGE_READ		= 5,    //Can write to cloud storage
    RLROS_PRIVILEGEID_CLOUD_STORAGE_WRITE		= 6,    //Can read from cloud storage
    RLROS_PRIVILEGEID_BANNED					= 7,    //Denies all other privileges except CREATE_TICKET
    RLROS_PRIVILEGEID_CLAN						= 8,    //Can use clan functions
    RLROS_PRIVILEGEID_PRIVATE_MESSAGING			= 9,    //Can use private messaging system
    RLROS_PRIVILEGEID_SOCIAL_CLUB				= 10,   //Can login to SC site, and TBD other functionality
    RLROS_PRIVILEGEID_PRESENCE_WRITE			= 11,   //Can update presence
    RLROS_PRIVILEGEID_DEVELOPER					= 12,   //If true, user is considered a developer
	RLROS_PRIVILEGEID_HTTP_REQUEST_LOGGING		= 13,   //If true, user's HTTP request log to SCS gameservices and cloud is logged
	RLROS_PRIVILEGEID_UGCWRITE					= 14,	//If true, allows writing UGC
	RLROS_PRIVILEGEID_PURCHASEVC				= 15,	//If true, allows purchasing virtual currency
	RLROS_PRIVILEGEID_TRANSFERVC				= 16,	//If true, allows transferring virtual currency
    RLROS_PRIVILEGEID_CANBET					= 17,   //If true, player can place bets
    RLROS_PRIVILEGEID_CREATORS					= 18,   //If true, player can access the creators
    RLROS_PRIVILEGEID_CLOUD_TUNABLES			= 19,   //If true, player can download the cloud tunables
    RLROS_PRIVILEGEID_CHEATER_POOL				= 20,   //if true, player is in the cheater pool
	RLROS_PRIVILEGEID_COMMENTWRITE				= 21,   //If true, user can create comments
    RLROS_PRIVILEGEID_CANUSELOTTERY				= 22,   //If true, player can buy lottery tickets
	RLROS_PRIVILEGEID_ALLOW_MEMBER_REDIRECT		= 23,   //If true, allow member space re-directs
	RLROS_PRIVILEGEID_PLAYED_LAST_GEN           = 24,   //If true, grant played last gen privilege
	RLROS_PRIVILEGEID_UNLOCK_SPECIAL_EDITION    = 25,   //If true, grant special edition content
	RLROS_PRIVILEGEID_CONTENT_CREATOR			= 26,   //If true, allows user to create/update/delete Ugc content bypassing owner and profanity checks 
	RLROS_PRIVILEGEID_CONDUCTOR					= 27,   //If true, allows user to access game servers (through conductor)
	RLROS_NUM_PRIVILEGEID  
};

//PURPOSE
//  Size constants
enum 
{
    RLROS_MAX_PLATFORM_NAME_SIZE     = 16,  //Length including terminating null   
    RLROS_MAX_TICKET_SIZE            = 512, //String length of base64 ticket data, including terminating null.
    RLROS_MAX_SESSION_TICKET_SIZE    = 128, //String length of base64 session ticket data, including terminating null.
    RLROS_MAX_TITLE_NAME_SIZE        = 16,  //Length including terminating null   
    RLROS_MAX_TITLE_DIRECTORY_SIZE   = 64,  //Length including terminating null   
    RLROS_MAX_USER_NAME_SIZE         = 64,  //Max length of a ROS PA UserName, including terminating null.
    RLROS_MAX_USERID_SIZE            = 64,  //Max length of a ROS PA UserId, including terminating null.
    RLROS_MAX_RSA_ENCRYPTED_SIZE     = 128, //Max size (in bytes) of an encrypted RSA message (size of modulus)
	RLROS_MAX_SERVICE_ACCESS_TOKEN_SIZE = 2048,  //Max size of an Scs Service Access Token, not including terminating null.
	RLROS_MAX_SERVICE_SERVER_URL_SIZE = 128
};

//PURPOSE
//  Output macros
RAGE_DECLARE_SUBCHANNEL(rline, ros)

//PURPOSE
//  Events generated by rlRos.
enum
{
    RLROS_EVENT_ONLINE_STATUS_CHANGED,
    RLROS_EVENT_LINK_CHANGED,
    RLROS_EVENT_RETRIEVED_GEOLOC_INFO,
	RLROS_EVENT_RETRIEVED_PRESENCE_INFO,
	RLROS_EVENT_RETRIEVED_NAT_DISCOVERY_SERVERS,
	RLROS_EVENT_GET_CREDENTIALS_RESULT,
	RLROS_EVENT_CAN_READ_ACHIEVEMENTS,
};

#define RLROS_EVENT_COMMON_DECL( name )\
    static unsigned EVENT_ID() { return name::GetAutoId(); }\
    virtual unsigned GetId() const { return name::GetAutoId(); }

#define RLROS_EVENT_DECL( name, id )\
    AUTOID_DECL_ID( name, rage::rlRosEvent, id )\
    RLROS_EVENT_COMMON_DECL( name )

//PURPOSE
//  Base class for all ROS event classes.
class rlRosEvent
{
public:
    AUTOID_DECL_ROOT(rlRosEvent);
    RLROS_EVENT_COMMON_DECL(rlRosEvent);

    rlRosEvent() {}
    virtual ~rlRosEvent() {}
};

//PURPOSE
//  Interface for accessing sub-secrets within the title secret.
class rlRosTitleSecrets
{
public:
    enum
    {
        //Current version of title secret format
        SECRET_VERSION = 11,

        //Offsets and sizes of components within the secret
        VERSION_OFFSET = 0,
        VERSION_SIZE = 1,

        MASTER_KEY_OFFSET = VERSION_OFFSET + VERSION_SIZE,
        MASTER_SECRET_SIZE = 32,

        KEY_OFFSET = MASTER_KEY_OFFSET + MASTER_SECRET_SIZE,
        KEY_SIZE = 16,

        SALT_OFFSET = KEY_OFFSET + KEY_SIZE,
        SALT_SIZE  = 16,

        TOTAL_SIZE = SALT_OFFSET + SALT_SIZE,

        TOTAL_BASE64_SIZE = (((TOTAL_SIZE + 2) / 3) * 4) + 1 //datBase64::GetMaxEncodedSize(TOTAL_SIZE)
    };

    rlRosTitleSecrets();

    void Clear();

    //Parses a title secret from a base64 string.
    bool Reset(const char* titleSecretsBase64);

    //Stores the decrypted key in the provided buffer.
    //The buffer must be exactly KEY_SIZE in size.
    bool GetKey(u8* buf, const unsigned bufSize) const;

    //Stores the decrypted salt in the provided buffer.
    //The buffer must be exactly SALT_SIZE in size.
    bool GetSalt(u8* buf, const unsigned bufSize) const;

private:
    u8 m_Secret[TOTAL_SIZE];
};

//A variable length key up to a static max size
template<int MAX_KEY_SIZE>
struct EncryptionKey
{
private:
    u8 m_Key[MAX_KEY_SIZE];
    unsigned m_KeyLength;

public:
    EncryptionKey()
    {
        Clear();
    }

    const u8* GetKeyBuffer() const
    {
        return m_Key;
    }

    unsigned GetCount() const
    {
        return m_KeyLength;
    }

    unsigned GetCapacity() const
    {
        return MAX_KEY_SIZE;
    }

    void Clear()
    {
        sysMemSet(m_Key, 0, sizeof(m_Key));
        m_KeyLength = 0;
    }

    bool FromBase64(const char* base64)
    {
        return datBase64::Decode(base64, sizeof(m_Key), m_Key, &m_KeyLength);
    }

    bool operator==(const EncryptionKey& rhs) const
    {
        return this->GetCount() == rhs.GetCount() 
            && memcmp(this->m_Key, rhs.m_Key, this->GetCount()) == 0;
    }
};

//PURPOSE
//  Container for information obtained during authentication.
struct rlRosCredentials
{
	static const unsigned SESSION_KEY_SIZE = 32;
	static const unsigned CLOUD_KEY_SIZE = 32;

	typedef EncryptionKey<SESSION_KEY_SIZE> SessionKey;
	typedef EncryptionKey<CLOUD_KEY_SIZE> CloudKey;

    struct PrivilegeInfo
    {
        bool m_IsValid:1;
        bool m_IsGranted:1;
        bool m_HasEndDate:1;
        u64 m_EndPosixTime;

        PrivilegeInfo() 
        : m_IsValid(false) 
        , m_IsGranted(false)
		, m_HasEndDate(false)
		, m_EndPosixTime(0)
        {}
    };

    rlRosCredentials();

    //PURPOSE
    //  Clears the credentials.
    void Clear();

    //PURPOSE
    //  Sets our credentials' data.
    bool Reset(const char* ticket,
               const unsigned validMs,
               const int region,
               const PlayerAccountId playerAccountId,
               const rlScAccountInfo& rockstarAccount,
               const s64 sessionId,
               const atFixedBitSet<RLROS_NUM_PRIVILEGEID>& privileges,
               const SessionKey& sessionKey,
               const char* sessionTicket,
               const CloudKey& cloudKey,
               const PrivilegeInfo (&privilegeInfos)[RLROS_NUM_PRIVILEGEID]);

    //PURPOSE
    //  Returns true if the ticket is valid, and hasn't expired.
    //  On nonsecure platforms, this will return false if we don't have a session key.
    bool IsValid() const;

    //PURPOSE
    //  Returns true if the credentials contain a valid rockstar ID.
    bool IsScMember() const;

    //PURPOSE
    //  Returns milliseconds until the ticket expires.
    int GetMsUntilExpiration() const;

	//PURPOSE
	//  Returns true if we have a valid (non NULL) ticket but we have expired
	bool HasExpired() const;

    //PURPOSE
    //  Returns region ID, determined by ROS from this client's public IP.
    int GetRegion() const;

    //PURPOSE
    //  Returns player account ID.  This should never be zero, as the player 
    //  account (which is essentially a shadow account of the platform account)
    //  is automatically created when obtaining credentials.
    PlayerAccountId GetPlayerAccountId() const;

    //PURPOSE
    //  Returns Social Club ID of user.  This will be zero if the player
    //  account is not linked to a Rockstar account.
    RockstarId GetRockstarId() const;

    //PURPOSE
    //  Returns RockstarAccount linked to this local gamer.  If the player is
    //  not linked, the RockstarId on the account will be zero.
    const rlScAccountInfo& GetRockstarAccount() const;

    //PURPOSE
    //  Returns the ID for this session.  Session IDs are unique per player, but not globally unique.
    //  They are used to identify all the calls made within a boot of the game.
    s64 GetSessionId() const;

    //PURPOSE
    //  Returns the base64 ROS ticket, or NULL if no ticket available.
    const char* GetTicket() const;

	// PURPOSE
	//	Sets the base64 ROS ticket
	void SetTicket(const char* ticket);

    //PURPOSE
    //  Returns the base64 ROS session ticket, or NULL if no ticket available.
    const char* GetSessionTicket() const;

    //PURPOSE
    //  Returns the plaintext session key for encryption
    const SessionKey& GetSessionKey() const;

    //PURPOSE
    //  Returns the plaintext cloud key for encryption of cloud files
    const CloudKey& GetCloudKey() const;

    //PURPOSE
    //  Returns true if user has specified privilege.
    bool HasPrivilege(const rlRosPrivilegeId privilegeId) const;

#if __BANK
	//PURPOSE
	//  Change a specified privilege.
	bool SetPrivilege(const rlRosPrivilegeId privilegeId, const bool value);
#endif

    //PURPOSE
    //  Returns true if either the granting or denial of the specified
    //  privilege has an end date, and fills in the end date posix time if so
    bool HasPrivilegeEndDate(const rlRosPrivilegeId privilegeId, bool* isGranted, u64* endPosixTime) const;

    bool operator==(const rlRosCredentials& rhs) const;

private:
    friend class rlRosClient;

    //Ticket blob, encrypted with a private key known only to ROS backend.
    char m_Ticket[RLROS_MAX_TICKET_SIZE];
    //Session ticket blob, encrypted with private key known only to ROS backend.
    //This is basically our way of privately telling the server our m_SessionKey
    char m_SessionTicket[RLROS_MAX_SESSION_TICKET_SIZE];

    unsigned m_ValidMs;
    int m_Region;
    PlayerAccountId m_PlayerAccountId;
    s64 m_SessionId;
    rlScAccountInfo m_RockstarAcct;
    atFixedBitSet<RLROS_NUM_PRIVILEGEID> m_Privileges;

    //Privilege infos for any privileges that aren't the default
    PrivilegeInfo m_PrivilegeInfos[RLROS_NUM_PRIVILEGEID];

    //Our plaintext session key used for encryption/decryption,
    //as chosen by the server.
    SessionKey m_SessionKey;
    //Our plaintext cloud key
    CloudKey m_CloudKey;
};

//PURPOSE
//  Container for platform-specific credentials necessary to obtain 
//  a ROS ticket.  This is only used internally by ROS.
struct rlRosPlatformCredentials
{
    static void GetLocalPlatformCredentials(const int localGamerIndex, 
                                            rlRosPlatformCredentials* cred);

    rlRosPlatformCredentials();

    void Clear();

    bool IsValid() const;

    bool operator==(const rlRosPlatformCredentials& rhs) const;
    bool operator!=(const rlRosPlatformCredentials& rhs) const;

    rlRosPlatformCredentials& operator=(const rlRosPlatformCredentials& rhs);

#if RLROS_SC_PLATFORM
	char m_Email[RLSC_MAX_EMAIL_CHARS];
	char m_ScAuthToken[RLSC_MAX_SCAUTHTOKEN_CHARS];

#elif RSG_DURANGO
    u64 m_Xuid;
    char m_Gamertag[RL_MAX_NAME_BUF_SIZE];

#elif RSG_NP
    rlSceNpAccountId m_AccountId;
#endif
};

//PURPOSE
//  Describes a result received from ROS backend, either success or failure.
struct rlRosResult
{
    rlRosResult() 
    : m_Succeeded(true)
    , m_Code(0)
    , m_CodeEx(0)
    , m_Ctx(0)
    , m_Location(0)
    , m_Msg(0)
    {
    }

    bool m_Succeeded;
    const char* m_Code;     //Always non-null if !m_Succeeded.
    const char* m_CodeEx;   //May be non-null.
    const char* m_Ctx;      //May be non-null, but only in dev or rage environment.
    const char* m_Location; //May be non-null, but only in dev or rage environment.
    const char* m_Msg;      //May be non-null, but only in dev or rage environment.

    //PURPOSE
    //  Convenience method to check for errors by Code and (optionally) CodeEx.
    bool IsError(const char* code, const char* codeEx = 0) const;
};

struct rlRosJsonResult : public rlRosResult
{
	rlRosJsonResult()
	{
		// initialize strings
		codeStr[0] = '\0';
		codeExStr[0] = '\0';
		ctxStr[0] = '\0';
		locationStr[0] = '\0';
		msgStr[0] = '\0';

		// setup base class ptrs
		m_Code = codeStr;
		m_CodeEx = codeExStr;
		m_Ctx = ctxStr;
		m_Location = locationStr;
		m_Msg = msgStr;
	}

	char codeStr[128];
	char codeExStr[128];
	char ctxStr[1024];
	char locationStr[1024];
	char msgStr[4096];
};

class rlRosPlayerInfo
{
public:
    char m_Nickname[RL_MAX_NAME_BUF_SIZE];
    char m_Gamertag[RL_MAX_NAME_BUF_SIZE];
    rlGamerHandle m_GamerHandle;
    RockstarId m_RockstarId;
    PlayerAccountId m_PlayerAccountId;

public:
    rlRosPlayerInfo();

    //Invalidates the player descriptor.
    void Clear();

    //Returns TRUE if either there is a valid rockstarId or a valid gamer.
    bool IsValid() const { return (InvalidRockstarId != m_RockstarId) || (m_GamerHandle.IsValid()); }
};

//PURPOSE
//  Convenience class for creating and parsing ROS player account UserIds.
class rlRosUserId
{
public:
    //PURPOSE
    //  Creates a user ID from a gamerhandle.
    //RETURNS
    //  UserID string on success, NULL on error.
    static const char* FromGamerHandle(const rlGamerHandle& gamerHandle, char* buf, const int bufSize);

    //PURPOSE
    //  Creates a gamerhandle from a user ID.  The online service
    //  and environment are assumed from the runtime environment.
    //RETURNS
    //  True on success, false on error.
    static bool ToGamerHandle(const char* userId, rlGamerHandle* gamerHandle);
};

//PURPOSE
//  Container for all geolocation related data.
class rlRosGeoLocInfo
{
public:

    static const int MAX_RELAY_ADDRESSES    = 128;

    rlRosGeoLocInfo()
        : m_RegionCode(0)
        , m_Longitude(0)
        , m_Latitude(0)
		, m_IsSecure(false)
        , m_NumRelayAddrs(0)
    {
		m_CountryCode[0] = '\0';
    }

    void Init(const unsigned regionCode,
                const float longitude,
                const float latitude,
				const char* countryCode,
				bool isSecure,
                const netSocketAddress* relayAddrs,
                const unsigned numRelayAddrs)
    {
        Clear();

        m_RegionCode = regionCode;
        m_Longitude = longitude;
        m_Latitude = latitude;
		m_IsSecure = isSecure;
        if(countryCode)
        {
		    safecpy(m_CountryCode, countryCode, sizeof(m_CountryCode));
        }

        m_NumRelayAddrs = numRelayAddrs > COUNTOF(m_RelayAddrs) ? COUNTOF(m_RelayAddrs) : numRelayAddrs;

        for(unsigned i = 0; i < m_NumRelayAddrs; ++i)
        {
            m_RelayAddrs[i] = relayAddrs[i];
        }
    }

    void Clear()
    {
        m_RegionCode = 0;
        m_Longitude = m_Latitude = 0;
		m_IsSecure = false;
		m_CountryCode[0] = '\0';
        m_NumRelayAddrs = 0;

        for(int i = 0; i < COUNTOF(m_RelayAddrs); ++i)
        {
            m_RelayAddrs[i].Clear();
        }
    }

    //The region code is an arbitrary code that can be used
    //to determine if players are in the same region.
    //For example, in matchmaking.
    //Region codes can change, so don't write code assuming
    //specific values for region codes.
    unsigned m_RegionCode;
    float m_Longitude;
    float m_Latitude;
	bool m_IsSecure;
	char m_CountryCode[RLSC_MAX_COUNTRY_CODE_CHARS];

    //Network addresses of the geo-located relay servers.
    unsigned m_NumRelayAddrs;
    netSocketAddress m_RelayAddrs[MAX_RELAY_ADDRESSES];
};

//PURPOSE
//  Container for all presence server related data.
class rlRosPresenceInfo
{
public:
    static const int MAX_PRESENCE_ADDRESSES = 32;

	typedef char Hostname[MAX_HOSTNAME_BUF_SIZE];

	struct Host
	{
		Host()
		{
			Clear();
		}

		void Clear()
		{
			sysMemSet(m_Hostname, 0, sizeof(m_Hostname));
			m_Port = 0;
		}

		void Init(const Hostname& hostname, const u16 port)
		{
			Clear();

			safecpy(m_Hostname, hostname);
			m_Port = port;
		}
		
		Hostname m_Hostname;
		u16 m_Port;
	};

    rlRosPresenceInfo()
		: m_IsSecure(false)
        , m_NumPresenceAddrs(0)
    {

	}

    void Init(bool isSecure,
				const rlRosPresenceInfo::Host* presenceAddrs,
				const int numPresenceAddrs)
    {
        Clear();

		m_IsSecure = isSecure;
        m_NumPresenceAddrs = (numPresenceAddrs > COUNTOF(m_PresenceAddrs)) ? COUNTOF(m_PresenceAddrs) : numPresenceAddrs;

        for(u32 i = 0; i < m_NumPresenceAddrs; ++i)
        {
			m_PresenceAddrs[i].Init(presenceAddrs[i].m_Hostname, presenceAddrs[i].m_Port);
        }
    }

    void Clear()
    {
		m_IsSecure = false;
        m_NumPresenceAddrs = 0;
		sysMemSet(m_PresenceAddrs, 0, sizeof(m_PresenceAddrs));
    }

	bool GetPresenceServer(const u32 index, Hostname& hostname, u16& port)
	{
		hostname[0] = '\0';
		port = 0;

		if(rlVerify(index < m_NumPresenceAddrs))
		{
			safecpy(hostname, m_PresenceAddrs[index].m_Hostname);
			port = m_PresenceAddrs[index].m_Port;
			return true;
		}

		return false;
	}

	bool m_IsSecure;

	//addresses/hostnames of the presence servers.
    u32 m_NumPresenceAddrs;
    Host m_PresenceAddrs[MAX_PRESENCE_ADDRESSES];
};

//PURPOSE
//  Container for all NAT discovery server related data.
class rlRosNatDiscoveryServers
{
public:
    static const int MAX_NAT_DISCOVERY_SERVERS = 128;

	struct Host
	{
		Host()
		{
			Clear();
		}

		void Clear()
		{
			m_Addr.Clear();
		}

		void Init(const netSocketAddress& addr)
		{
			Clear();
			m_Addr = addr;
		}
		
		netSocketAddress m_Addr;
	};

    rlRosNatDiscoveryServers()
		: m_NumAddrs(0)
    {

	}

    void Init(const rlRosNatDiscoveryServers::Host* addrs,
			  const int numAddrs)
    {
        Clear();

        m_NumAddrs = (numAddrs > COUNTOF(m_Addrs)) ? COUNTOF(m_Addrs) : numAddrs;

        for(u32 i = 0; i < m_NumAddrs; ++i)
        {
			m_Addrs[i] = addrs[i];
        }
    }

    void Clear()
    {
		sysMemSet(m_Addrs, 0, sizeof(m_Addrs));
    }

	bool GetServer(const u32 index, netSocketAddress& addr)
	{
		addr.Clear();

		if(rlVerify(index < m_NumAddrs))
		{
			addr = m_Addrs[index].m_Addr;
			return true;
		}

		return false;
	}

    u32 m_NumAddrs;
    rlRosNatDiscoveryServers::Host m_Addrs[MAX_NAT_DISCOVERY_SERVERS];
};

//PURPOSE
//  Maps service endpoints to service host names.
//  We can use this to direct specific services, like leaderboards,
//  to different servers.
class rlRosServiceHost
{
public:
	static const int MAX_HOST_LEN = 64;
	static const int MAX_ENDPOINT_LEN = 256;

	rlRosServiceHost()
	{
		Host[0] = Endpoint[0] = '\0';
	}

	//Host name of server
	char Host[MAX_HOST_LEN];
	
	//Service endpoint, e.g. "gameservices/Leaderboards.asmx"
	char Endpoint[MAX_ENDPOINT_LEN];
};

//PURPOSE
//  Defines an endpoint (using wildcards) that clients should
//  connect to over ssl, configured during CreateTicket (which
//  should be behind ssl)
class rlRosSslService
{
public:
	static const int MAX_ENDPOINT_LEN = 256;

	rlRosSslService()
	{
		Endpoint[0] = '\0';
	}
	
	//Service endpoint with wildcards, e.g. "*gameservices/Leaderboards.asmx*"
	char Endpoint[MAX_ENDPOINT_LEN];
};


class rlRosServiceServerInfo
{
public:
	rlRosServiceServerInfo()
	{
		Clear();
	}

	void Clear()
	{
		Id = 0;
		Url[0] = '\0';
		Port = 0;
	}

	void SetUrl(const char* url)
	{
		safecpy(Url, url);
	}


	bool IsValid() const { return Url[0] != '\0'; }

	char Url[RLROS_MAX_SERVICE_SERVER_URL_SIZE];
	int Id;
	int Port;
};

class rlRosServiceAccessToken
{
public:
	rlRosServiceAccessToken()
	{
		Clear();
	}
	
	bool IsValid() const { return AccessToken[0] != '\0'; }
	void Clear() { AccessToken[0] = '\0'; }

	void Set(const char* token)
	{

		safecpy(AccessToken, token);
	}

    //We need to add 1 for null term, we verify strlen(serviceToken) <= RLROS_MAX_SERVICE_ACCESS_TOKEN_SIZE
	char AccessToken[RLROS_MAX_SERVICE_ACCESS_TOKEN_SIZE + 1];
};

class rlRosServiceAccessInfo
{
public:
	rlRosServiceAccessInfo()
	{
		Clear();
	}

	void Clear()
	{
		m_serverInfo.Clear();
		m_AccessToken.Clear();
		m_ttl = 0;
	}

	bool IsValid() const { return m_AccessToken.IsValid() && m_serverInfo.IsValid(); }

	const char* GetAccessToken()	const	{ return m_AccessToken.AccessToken; }
	int			GetServerPort()		const	{ return m_serverInfo.Port; }
	const char* GetServerUrl()		const	{ return m_serverInfo.Url; }

	rlRosServiceServerInfo m_serverInfo;
	rlRosServiceAccessToken m_AccessToken;
	int	m_ttl;
};

//PURPOSE
//  ROS security settings.
//  Controls how responses should be encrypted/signed by the server based on
//  client expectations
//NOTE: This enum must match the RosSecurityFlags enum on server
enum rlRosSecurityFlags
{
    //The original encryption/signature settings. If using RLROS_ENCRYPTION_TITLE:
    //1. Response headers are not signed
    //2. A plaintext sha1 hash of the encrypted block is appended to each encrypted block
    RLROS_SECURITY_NONE =                   0,
    //If present, the client will include a random challenge/nonce with the request, which 
    //the server will include with the hmac for the response headers/body. This is intended
    //to be used with the HMAC_HEADERS and HMAC_BODY options and prevents replaying
    //server responses.
    RLROS_SECURITY_CHALLENGE =              BIT0,
    //Server returns an hmac of certain key headers so that the client can detect any
    //tampering.
    RLROS_SECURITY_HMAC_HEADERS =           BIT1,
    //Encrypted blocks are followed with an hmac instead of a hash
    RLROS_SECURITY_HMAC_BODY =              BIT2,
    //If present, the client will request the server to respond with a content signature,
    //if it so chooses. If the server doesn't, then the request will not fail. If it does,
    //then the client will verify that the response body matches the server signature,
    //and fail the request if it doesn't. This is primarily useful for things like a redirect
    //to the CDN, and when used in conjunction with RLROS_SECURITY_HMAC_HEADERS
    //allows the server to determine whether or not to tell the client to verify the signature
    //against the file downloaded from the CDN
    RLROS_SECURITY_REQUEST_CONTENT_SIG =    BIT3,
    //If present, the client will demand the server to respond with a content signature.
    //If it doesn't, the request should fail, otherwise the client will verify the server's
    //signature.
    RLROS_SECURITY_REQUIRE_CONTENT_SIG =    BIT4,
    //If present, the client will expect the headers hmac to also include the http status code
    //so that it can't be tampered with
    RLROS_SECURITY_HMAC_STATUS_CODE =       BIT5,
    //Similar to RLROS_SECURITY_HMAC_HEADERS, this protects the client request header from
    //being tampered with. If present, the client will include an hmac in its request header
    //that is an hmac of other important request headers. The server will verify that the
    //hmac matches expected based on the headers that it received from the client.
    //Likewise, the client will verify the server has done this by including the request
    //header hmac in the response header hmac.
    RLROS_SECURITY_HMAC_REQUEST_HEADERS =   BIT6,
    //If present, the header hmac should be structured. The header hmac consists of several
    //pieces of the header glued together, allowing someone to move characters between pieces
    //without changing the hmac but potentially altering the meaning of the header. This
    //causes the header hmac to use a pre-defined structure to prevent thi
    RLROS_SECURITY_STRUCTURED_HEADER_HMAC = BIT7,
    RLROS_SECURITY_DEFAULT = RLROS_SECURITY_CHALLENGE 
                                | RLROS_SECURITY_HMAC_HEADERS 
                                | RLROS_SECURITY_HMAC_BODY 
                                | RLROS_SECURITY_REQUEST_CONTENT_SIG 
                                | RLROS_SECURITY_HMAC_STATUS_CODE 
                                | RLROS_SECURITY_HMAC_REQUEST_HEADERS 
                                | RLROS_SECURITY_STRUCTURED_HEADER_HMAC
};

//NOTE: This enum must match the RosSignatureType enum on server
//PURPOSE
enum rlRosSignatureType
{
    RLROS_SIGNATURE_DEFAULT = 0,
    RLROS_SIGNATURE_CLOUD = 1,
	RLROS_SIGNATURE_ENTITLEMENTS = 2,
};

//PURPOSE
//  Simple structure that defines a content signature that can be
//  returned by ROS and verified by the client using a public RSA key
//  See rlRosHttpFilter::VerifySignature
struct rlRosContentSignature
{
    u8 m_ContentSignature[RLROS_MAX_RSA_ENCRYPTED_SIZE];
    unsigned m_Length;

    rlRosContentSignature()
    {
        Clear();
    }

    void Clear()
    {
        m_Length = 0;
    }
};

class rlRosEventOnlineStatusChanged : public rlRosEvent
{
public:
    RLROS_EVENT_DECL(rlRosEventOnlineStatusChanged, RLROS_EVENT_ONLINE_STATUS_CHANGED);

    rlRosEventOnlineStatusChanged(const int localGamerIndex,
                                  const PlayerAccountId playerAccountId,
                                  const PlayerAccountId prevPlayerAccountId,
                                  const RockstarId rockstarId,
                                  const RockstarId prevRockstarId)
    {
        m_LocalGamerIndex = localGamerIndex;
        m_PlayerAccountId = playerAccountId;
        m_PrevPlayerAccountId = prevPlayerAccountId;
        m_RockstarId = rockstarId;
        m_PrevRockstarId = prevRockstarId;
    }

    int m_LocalGamerIndex;
    PlayerAccountId m_PlayerAccountId;
    PlayerAccountId m_PrevPlayerAccountId;
    RockstarId m_RockstarId;
    RockstarId m_PrevRockstarId;
};

class rlRosEventLinkChanged : public rlRosEvent
{
public:
    RLROS_EVENT_DECL(rlRosEventLinkChanged, RLROS_EVENT_LINK_CHANGED);

    rlRosEventLinkChanged(const int localGamerIndex,
                          const PlayerAccountId playerAccountId,
                          const RockstarId rockstarId,
                          const RockstarId prevRockstarId)
    {
        m_LocalGamerIndex = localGamerIndex;
        m_PlayerAccountId = playerAccountId;
        m_RockstarId = rockstarId;
        m_PrevRockstarId = prevRockstarId;
    }

    int m_LocalGamerIndex;
    PlayerAccountId m_PlayerAccountId;
    RockstarId m_RockstarId;
    RockstarId m_PrevRockstarId;
};

class rlRosEventRetrievedGeoLocInfo : public rlRosEvent
{
public:
    RLROS_EVENT_DECL(rlRosEventRetrievedGeoLocInfo, RLROS_EVENT_RETRIEVED_GEOLOC_INFO);

    rlRosEventRetrievedGeoLocInfo(const rlRosGeoLocInfo& geoLocInfo)
    {
        m_GeoLocInfo = geoLocInfo;
    }

    rlRosGeoLocInfo m_GeoLocInfo;
};

class rlRosEventRetrievedPresenceServerInfo : public rlRosEvent
{
public:
    RLROS_EVENT_DECL(rlRosEventRetrievedPresenceServerInfo, RLROS_EVENT_RETRIEVED_PRESENCE_INFO);

    rlRosEventRetrievedPresenceServerInfo(const rlRosPresenceInfo& presenceInfo)
    {
        m_PresenceInfo = presenceInfo;
    }

    rlRosPresenceInfo m_PresenceInfo;
};

class rlRosEventGetCredentialsResult : public rlRosEvent
{
public:
	RLROS_EVENT_DECL(rlRosEventGetCredentialsResult, RLROS_EVENT_GET_CREDENTIALS_RESULT);

	rlRosEventGetCredentialsResult(const int localGamerIndex,
		bool success)
	{
		m_LocalGamerIndex = localGamerIndex;
		m_Success = success;
	}

	int m_LocalGamerIndex;
	bool m_Success;
};

class rlRosEventCanReadAchievements : public rlRosEvent
{
public:
    RLROS_EVENT_DECL(rlRosEventCanReadAchievements, RLROS_EVENT_CAN_READ_ACHIEVEMENTS);

    rlRosEventCanReadAchievements(const int localGamerIndex, bool success)
    {
        m_LocalGamerIndex = localGamerIndex;
        m_Success = success;
    }

    int m_LocalGamerIndex;
    bool m_Success;
};

} //namespace rage

#endif //RLROSCOMMON_H
