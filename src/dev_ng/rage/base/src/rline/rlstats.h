// 
// rline/rlstats.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSTATS_H
#define RLINE_RLSTATS_H

#include "rl.h"
#include "clan/rlclancommon.h"

namespace rage
{

//From <xonline.h>
#define RL_STAT_TYPE_CONTEXT        ((rage::u8)0)
#define RL_STAT_TYPE_INT32          ((rage::u8)1)
#define RL_STAT_TYPE_INT64          ((rage::u8)2)
#define RL_STAT_TYPE_DOUBLE         ((rage::u8)3)
#define RL_STAT_TYPE_UNICODE        ((rage::u8)4)
#define RL_STAT_TYPE_FLOAT          ((rage::u8)5)
#define RL_STAT_TYPE_BINARY         ((rage::u8)6)
#define RL_STAT_TYPE_DATETIME       ((rage::u8)7)
#define RL_STAT_TYPE_NULL           ((rage::u8)0xFF)
#define RL_STAT_TYPE_FROM_ID(id)    ((((id) & 0xF0000000) >> 28) & 0x0F)

//PURPOSE
//  Represents a value stored in a leaderboard.
class rlStatValue
{
public:

    rlStatValue()
    {
        Clear();
    }

    void Clear()
    {
        Int64Val = 0;
        DoubleVal = 0;
    }

    //We don't have values for int32 or float.  We just store int32
    //to the int64 member, and floats to the double member.
    union
    {
        s64 Int64Val;
        double DoubleVal;
    };
};

//PURPOSE
//  Used to write to a leaderboard
class rlLeaderboardInputValue
{
public:

    rlStatValue Value;      //Input value
    unsigned InputId;       //Input ID

    rlLeaderboardInputValue();

    int GetType() const;
    const char* GetTypeString() const;
};

//PURPOSE
//  Describes a leaderboard column.
class rlLeaderboardColumnInfo
{
public:

    unsigned Id;          //Column ID
    int Type;             //Column data type
	bool IsRankingColumn; //Column is the ranking column
};

//PURPOSE
//  Identifies a group for which leaderboard stats can be collected.
//  For example, a clan represents a group.
//  In that case the "handle" is the string representation of the clan ID.
//  Other types of groups are possible, like group by country,
//  by gender, by age, etc.
class rlLeaderboardGroupHandle
{
public:
    rlLeaderboardGroupHandle();

    explicit rlLeaderboardGroupHandle(const char* handle);

    explicit rlLeaderboardGroupHandle(const s64 groupId);

    void Init(const char* handle);

    void Init(const s64 groupId);

    void Clear();

    bool IsValid() const;

    bool operator==(const rlLeaderboardGroupHandle& that) const;

    bool operator!=(const rlLeaderboardGroupHandle& that) const;

    char Handle[RL_GROUP_HANDLE_MAX_CHARS];
};

//PURPOSE
//  Identifies the owner of a row in a leaderboard.
class rlLeaderboardRowId
{
public:
    rlGamerHandle GamerHandle;
    rlLeaderboardGroupHandle GroupHandle;   //E.g. clan ID

    rlLeaderboardRowId();

    explicit rlLeaderboardRowId(const rlGamerHandle& gamerHandle);

    explicit rlLeaderboardRowId(const rlLeaderboardGroupHandle& groupHandle);

    void Init(const rlGamerHandle& gamerHandle);

    void Init(const rlLeaderboardGroupHandle& groupHandle);

    void Clear();
};

//PURPOSE
//  Used to read from a leaderboard.
class rlLeaderboardRow
{
public:
    char GamerDisplayName[RL_MAX_NAME_BUF_SIZE];
    char GroupDisplayName[RL_CLAN_NAME_MAX_CHARS];
    char GroupDisplayTag[RL_CLAN_TAG_MAX_CHARS];
    unsigned Rank;
    rlLeaderboardRowId RowId;
    rlStatValue ColumnValues[RL_MAX_LEADERBOARD_COLUMNS];
    unsigned NumValues;

    rlLeaderboardRow();

    void Clear();
};

//PURPOSE
//  Used to write to a leaderboard.
class rlLeaderboardUpdate
{
public:

    //Leaderboard service to which update is sent
    rlLeaderboardService LeaderboardService;
    //Type of leaderboard
    rlLeaderboardType LeaderboardType;
    //For "group" leaderboards, the type of group.
    //Zero represents clans
    int GroupType;
    unsigned LeaderboardId;
    rlLeaderboardGroupHandle GroupHandle;   //E.g. clan ID
    rlLeaderboardInputValue InputValues[RL_MAX_LEADERBOARD_COLUMNS];
    unsigned NumValues;

    rlLeaderboardUpdate();

    //PURPOSE
    //  Returns true if updating a PLAYER leaderboard
    bool IsPlayerUpdate() const;

    //PURPOSE
    //  Returns true if updating a GROUP leaderboard
    bool IsGroupUpdate() const;
};

//PURPOSE
//  rlStats can be used to read and write stats from/to leaderboards.
//
//  Multiple read/write operations can be queued and are carried out
//  asynchronously.
class rlStats
{
public:

    //PURPOSE
    //  Returns the total number of rows currently in a leaderboard.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbType          - Type of leaderboard to read from.
    //                    For RL_LEADERBOARD_SERVICE_XBL the leaderboard type
    //                    must be RL_LEADERBOARD_TYPE_PLAYER.
    //  lbSvc           - Leaderboard service to use.
    //  groupType       - Type of group.  Zero indicates "clans".
    //                    Ignored for RL_LEADERBOARD_TYPE_PLAYER
    //  groupHandle     - ID of the group from which to
    //                    read group member stats.
    //                    Ignored for all but RL_LEADERBOARD_TYPE_GROUP_MEMBER
    //  numRows         - Will be populated with the number of rows in the leaderboard.
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool GetLeaderboardSize(const int leaderboardId,
                                    const rlLeaderboardType lbType,
                                    const rlLeaderboardService lbSvc,
                                    const int groupType,
                                    const rlLeaderboardGroupHandle& groupHandle,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard starting at a specific row (the rank).
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbType          - Type of leaderboard to read from.
    //                    For RL_LEADERBOARD_SERVICE_XBL the leaderboard type
    //                    must be RL_LEADERBOARD_TYPE_PLAYER.
    //  lbSvc           - Leaderboard service to use.
    //  groupType       - Type of group.  Zero indicates "clans".
    //                    Ignored for RL_LEADERBOARD_TYPE_PLAYER
    //  groupHandle     - ID of the group from which to
    //                    read group member stats.
    //                    Ignored for all but RL_LEADERBOARD_TYPE_GROUP_MEMBER
    //  startRank       - Rank at which to start reading.  Must be >= 1.
    //  numRows         - Number of rows to read
    //  rows            - Destination rows
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRank(const int leaderboardId,
                            const rlLeaderboardType lbType,
                            const rlLeaderboardService lbSvc,
                            const int groupType,
                            const rlLeaderboardGroupHandle& groupHandle,
                            const int startRank,
                            const unsigned numRows,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard corresponding to specific row IDs.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbType          - Type of leaderboard to read from.
    //                    For RL_LEADERBOARD_SERVICE_XBL the leaderboard type
    //                    must be RL_LEADERBOARD_TYPE_PLAYER.
    //  lbSvc           - Leaderboard service to use.
    //  groupType       - Type of group.  Zero indicates "clans".
    //                    Ignored for RL_LEADERBOARD_TYPE_PLAYER
    //  groupHandle     - ID of the group from which to
    //                    read group member stats.
    //                    Ignored for all but RL_LEADERBOARD_TYPE_GROUP_MEMBER
    //  rowIds          - Array of row IDs for which stats will be read.
    //  numRowsRequested    - Number of row IDs in the rowIds array.
    //  rows            - Destination rows.  This array must be long enough
    //                    to accommodate the number of rows requested.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  The row IDs must be appropriate for the leaderboard type.
    //  For RL_LEADERBOARD_TYPE_PLAYER and RL_LEADERBOARD_TYPE_GROUP_MEMBER
    //  the row IDs must be gamer handles.
    //  For RL_LEADERBOARD_TYPE_GROUP the row IDs must be group IDs.
    //
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRow(const int leaderboardId,
                            const rlLeaderboardType lbType,
                            const rlLeaderboardService lbSvc,
                            const int groupType,
                            const rlLeaderboardGroupHandle& groupHandle,
                            const rlLeaderboardRowId* rowIds,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read rows from a group member leaderboard corresponding to specific groups for a specific gamer.
    //PARAMS
    //                  - There is no parameterber for 'lbType' because it is 
    //                    assumed to be RL_LEADERBOARD_TYPE_GROUP_MEMBER.
    //                  - There is no parameterber for 'lbSvc' because it is 
    //                    assumed to be RL_LEADERBOARD_SERVICE_ROS.
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //                    Ignored for RL_LEADERBOARD_TYPE_PLAYER
    //  gamer           - The gamer to lookup group member rows for.
    //  groupHandles    - The list of groups from which to read group member stats.
    //  numRowsRequested - Number of row IDs in the rowIds array.
    //  rows            - Destination rows.  This array must be long enough
    //                    to accommodate the number of rows requested.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  The row IDs must be appropriate for the leaderboard type.
    //  For RL_LEADERBOARD_TYPE_PLAYER and RL_LEADERBOARD_TYPE_GROUP_MEMBER
    //  the row IDs must be gamer handles.
    //  For RL_LEADERBOARD_TYPE_GROUP the row IDs must be group IDs.
    //
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadMemberByGroups(const int leaderboardId,
                                    const int groupType,
                                    const rlGamerHandle& member,
                                    const rlLeaderboardGroupHandle* groupHandles,
                                    const int numRowsRequested,
                                    rlLeaderboardRow* rows,
                                    unsigned* numRowsReturned,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    netStatus* status);

    //PURPOSE
    //  Read a gamer's row from a leaderboard, and the rows above and below their
    //  row within a certain radius.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbType          - Type of leaderboard to read from.
    //                    For RL_LEADERBOARD_SERVICE_XBL the leaderboard type
    //                    must be RL_LEADERBOARD_TYPE_PLAYER.
    //  lbSvc           - Leaderboard service to use.
    //  groupType       - Type of group.  Zero indicates "clans".
    //                    Ignored for RL_LEADERBOARD_TYPE_PLAYER
    //  pivotRowId      - ID of row around which to read "radius" rows.
    //  radius          - Number of rows above and below pivot row.
    //  rows            - Destination rows.  Must have at least (radius*2)+1 rows.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  pivotRowId must be appropriate for the leaderboard type.
    //  For RL_LEADERBOARD_TYPE_PLAYER and RL_LEADERBOARD_TYPE_GROUP_MEMBER
    //  pivotRowId must be a gamer handle.
    //  For RL_LEADERBOARD_TYPE_GROUP pivotRowId must be a group ID.
    //
    //  ReadByRadius is not supported for RL_LEADERBOARD_TYPE_GROUP_MEMBER
    //  leaderboards.  This is due to the cost of query on the server.
    //
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRadius(const int leaderboardId,
                            const rlLeaderboardType lbType,
                            const rlLeaderboardService lbSvc,
                            const int groupType,
                            const rlLeaderboardRowId& pivotRowId,
                            const unsigned radius,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Writes a gamer's stats to a set of leaderboards.
    //PARAMS
    //  localGamerHandle    - Index of the local gamer makeing the request.
    //  session     - Session handle for current stats session
    //  gamerHandle - Gamer for whom to write stats
    //  updates     - Array of leaderboard updates.
    //                These are copied to internal storage, so no need
    //                to maintain the memory during the asynch operation.
    //  numUpdates  - Number of leaderboard updates.
    //  status      - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the the number of leaderboards that can be written
    //  to during a game session to 5, plus one skill leaderboard.
    //
    //  If an update is to a group leaderboard it *must* use the ROS
    //  leaderboard service.
    //
    //  Updates to leaderboards other than RL_LEADERBOARD_TYPE_PLAYER
    //  *must* have a valid group ID.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool Write(const int localGamerIndex,
                        const void* sessionHandle,
                        const rlGamerHandle& gamerHandle,
                        const rlLeaderboardUpdate* updates,
                        const unsigned numUpdates,
                        netStatus* status);

    //PURPOSE
    //  Cancels the operation that is bound to the given status object.
    //PARAMS
    //  status  - A status object in the Pending state that was
    //            passed to one of the Read* operations.
    //NOTES
    //  It's important that the status parameter corresponds to a
    //  status parameter that was previously passed to a read operation,
    //  as that is how the operation to cancel is identified.
    static void Cancel(const netStatus* status);

#if __DEV
    //PURPOSE
    //   Resets a view for all gamers.
    //PARAMS
    //  leaderboardId - leaderboard id.
    //NOTES
    //   the operation is completed synchronously ... (see XUserResetStatsViewAllUsers)
    static bool ResetAllUsersStats(const unsigned long leaderboardId);
#endif // __DEV
};

class rlPlayerStats
{
public:

    //PURPOSE
    //  Returns the total number of rows currently in a leaderboard.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbSvc           - Leaderboard service to use.
    //  numRows         - Will be populated with the number of rows in the leaderboard.
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool GetLeaderboardSize(const int leaderboardId,
                                    const rlLeaderboardService lbSvc,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard starting at a specific row (the rank).
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbSvc           - Leaderboard service to use.
    //  startRank       - Rank at which to start reading.  Must be >= 1.
    //  numRows         - Number of rows to read
    //  rows            - Destination rows
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRank(const int leaderboardId,
                            const rlLeaderboardService lbSvc,
                            const int startRank,
                            const unsigned numRows,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard corresponding to specific gamers.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbSvc           - Leaderboard service to use.
    //  gamerHandles    - Array of gamer handles for which stats will be read.
    //  numRowsRequested    - Number of gamer handles in the gamerHandles array.
    //  rows            - Destination rows.  This array must be long enough
    //                    to accommodate the number of rows requested.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  The row IDs must contain gamer handles.
    //
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByGamers(const int leaderboardId,
                            const rlLeaderboardService lbSvc,
                            const rlGamerHandle* gamerHandles,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read a gamer's row from a leaderboard, and the rows above and below their
    //  row within a certain radius.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  lbSvc           - Leaderboard service to use.
    //  pivotGamer      - Gamer handle around which to read "radius" rows.
    //  radius          - Number of rows above and below pivot row.
    //  rows            - Destination rows.  Must have at least (radius*2)+1 rows.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRadius(const int leaderboardId,
                            const rlLeaderboardService lbSvc,
                            const rlGamerHandle& pivotGamer,
                            const unsigned radius,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Cancels the operation that is bound to the given status object.
    //PARAMS
    //  status  - A status object in the Pending state that was
    //            passed to one of the Read* operations.
    //NOTES
    //  It's important that the status parameter corresponds to a
    //  status parameter that was previously passed to a read operation,
    //  as that is how the operation to cancel is identified.
    static void Cancel(const netStatus* status);

#if __DEV
    //PURPOSE
    //   Resets a view for all gamers.
    //PARAMS
    //  leaderboardId - leaderboard id.
    //NOTES
    //   the operation is completed synchronously ... (see XUserResetStatsViewAllUsers)
    static bool ResetAllUsersStats(const unsigned long leaderboardId);
#endif // __DEV
};

class rlGroupStats
{
public:

    //PURPOSE
    //  Returns the total number of rows currently in a leaderboard.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  numRows         - Will be populated with the number of rows in the leaderboard.
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool GetLeaderboardSize(const int leaderboardId,
                                    const int groupType,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard starting at a specific row (the rank).
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  startRank       - Rank at which to start reading.  Must be >= 1.
    //  numRows         - Number of rows to read
    //  rows            - Destination rows
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRank(const int leaderboardId,
                            const int groupType,
                            const int startRank,
                            const unsigned numRows,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard corresponding to specific groups.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  groupHandles    - Array of group handles for which stats will be read.
    //  numRowsRequested    - Number of group handles in the groupHandles array.
    //  rows            - Destination rows.  This array must be long enough
    //                    to accommodate the number of rows requested.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByGroups(const int leaderboardId,
                            const int groupType,
                            const rlLeaderboardGroupHandle* groupHandles,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read a group's row from a leaderboard, and the rows above and below their
    //  row within a certain radius.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  pivotGroup      - Group of row around which to read "radius" rows.
    //  radius          - Number of rows above and below pivot row.
    //  rows            - Destination rows.  Must have at least (radius*2)+1 rows.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRadius(const int leaderboardId,
                            const int groupType,
                            const rlLeaderboardGroupHandle& pivotGroup,
                            const unsigned radius,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Cancels the operation that is bound to the given status object.
    //PARAMS
    //  status  - A status object in the Pending state that was
    //            passed to one of the Read* operations.
    //NOTES
    //  It's important that the status parameter corresponds to a
    //  status parameter that was previously passed to a read operation,
    //  as that is how the operation to cancel is identified.
    static void Cancel(const netStatus* status);
};

class rlGroupMemberStats
{
public:

    //PURPOSE
    //  Returns the total number of rows currently in a leaderboard.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  groupHandle     - ID of group for which to get the leaderboard size.
    //  numRows         - Will be populated with the number of rows in the leaderboard.
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool GetLeaderboardSize(const int leaderboardId,
                                    const int groupType,
                                    const rlLeaderboardGroupHandle& groupHandle,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard starting at a specific row (the rank).
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  groupHandle     - ID of group for which to read stats.
    //  startRank       - Rank at which to start reading.  Must be >= 1.
    //  numRows         - Number of rows to read
    //  rows            - Destination rows
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByRank(const int leaderboardId,
                            const int groupType,
                            const rlLeaderboardGroupHandle& groupHandle,
                            const int startRank,
                            const unsigned numRows,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read rows from a leaderboard corresponding to specific row IDs.
    //PARAMS
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  groupHandle     - ID of group for which to read stats.
    //  gamerHandles    - Array of group members for which stats will be read.
    //  numRowsRequested    - Number of gamer handles in the gamerHandles array.
    //  rows            - Destination rows.  This array must be long enough
    //                    to accommodate the number of rows requested.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  totalRows       - OPTIONAL.  Total number of rows in the leaderboard.
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadByMembers(const int leaderboardId,
                            const int groupType,
                            const rlLeaderboardGroupHandle& groupHandle,
                            const rlGamerHandle* gamerHandles,
                            const int numRowsRequested,
                            rlLeaderboardRow* rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            netStatus* status);

    //PURPOSE
    //  Read rows from a group member leaderboard corresponding to specific groups for a specific gamer.
    //PARAMS
    //                  - There is no parameterber for 'lbType' because it is 
    //                    assumed to be RL_LEADERBOARD_TYPE_GROUP_MEMBER.
    //                  - There is no parameterber for 'lbSvc' because it is 
    //                    assumed to be RL_LEADERBOARD_SERVICE_ROS.
    //  leaderboardId   - Leaderboard ID.
    //  groupType       - Type of group.  Zero indicates "clans".
    //  member          - The gamer to lookup group member rows for.
    //  groupHandles    - The list of groups from which to read group member stats.
    //  numRowsRequested - Number of group handles in the groupHandles array.
    //  rows            - Destination rows.  This array must be long enough
    //                    to accommodate the number of rows requested.
    //  numRowsReturned - Will be populated with the number of rows actually read
    //  columns         - Array of columns to read
    //  numColumns      - Number of columns to read
    //  status          - Status object that can be polled for completion.
    //NOTES
    //  Xbox Live limits the read to 5 different leaderboards, 101 gamers,
    //  and 64 columns.
    //
    //  Stats within a row will be populated in the same order as requested
    //  via the columnIds array.
    //
    //  This is an asynchronous operation.  It is not complete until the
    //  status object's Pending() function returns false.
    static bool ReadMemberByGroups(const int leaderboardId,
                                    const int groupType,
                                    const rlGamerHandle& member,
                                    const rlLeaderboardGroupHandle* groupHandles,
                                    const int numRowsRequested,
                                    rlLeaderboardRow* rows,
                                    unsigned* numRowsReturned,
                                    const rlLeaderboardColumnInfo* columns,
                                    const unsigned numColumns,
                                    netStatus* status);

    //PURPOSE
    //  Cancels the operation that is bound to the given status object.
    //PARAMS
    //  status  - A status object in the Pending state that was
    //            passed to one of the Read* operations.
    //NOTES
    //  It's important that the status parameter corresponds to a
    //  status parameter that was previously passed to a read operation,
    //  as that is how the operation to cancel is identified.
    static void Cancel(const netStatus* status);
};

//////////////////////////////////////////////////////////////////////////
//  rlLeaderboard2 interfaces
//
//  Leaderboard 2 adds the concept of categories and groups to leaderboards
//
//  Leaderboard categories define how scores can be segregated within a
//  leaderboard.
//
//  A group is a set of players within a category that has a unique ID.
//
//  Examples of categories:
//      Country
//      AgeGroup
//      PlayStyle
//
//  Within each category a group is identified with a unique ID.
//
//  For example, if a LB is configured with Country as a category, then
//  groups within the LB can have IDs like USA, UK, NZ, ES, etc.
//
//  If a LB is configured with AgeGroup as a category then groups within
//  the LB can be Under18, Under25, Under35, Under45.
//
//  If a LB is configured with PlayStyle as a category then groups within
//  the LB can be Casual, Competitive, HardCore.
//
//  Each group within a LB is effectively a logically separate LB.
//  For example, all players in the country USA can be ranked against
//  each other to the exclusion of players from other countries.
//
//  In addition, countries as a whole can be ranked against other countries.
//
//  Multiple categories can be combined.  For example, a single LB can be
//  configured with both Country and AgeGroup.  Then groups within the LB
//  are identified by both a country and an age group, e.g. USA/Under18,
//  USA/Under25, UK/Under25.
//
//  When querying LBs with multiple categories wild cards can be passed
//  for one or more categories.  A wild card is simply a category with
//  no ID.  For example, using the Country/AgeGroup example, querying LBs
//  with "Country=,AgeGroup=Under25" will return results for "under 25"
//  players in all countries.
//////////////////////////////////////////////////////////////////////////

//Maximum number of groups allowed for a single leaderboard
#define RL_LEADERBOARD2_MAX_GROUPS          4
//Maximum number of chars allowed in a category name
#define RL_LEADERBOARD2_CATEGORY_MAX_CHARS  32
//Maximum number of chars allowed in a group name
#define RL_LEADERBOARD2_GROUP_ID_MAX_CHARS  32

//PURPOSE
//  Identifies a group within a leaderboard
class rlLeaderboard2Group
{
public:
    //Name of the category
    char m_Category[RL_LEADERBOARD2_CATEGORY_MAX_CHARS];
    //ID of the group within the category
    char m_Id[RL_LEADERBOARD2_GROUP_ID_MAX_CHARS];
};

//PURPOSE
//  A list of category/group ID pairs that uniquely identifies
//  a group within a leaderboard.
class rlLeaderboard2GroupSelector
{
public:
	rlLeaderboard2GroupSelector()  { Clear(); }
    void Clear()
    {
        m_NumGroups = 0;
    }

    rlLeaderboard2Group m_Group[RL_LEADERBOARD2_MAX_GROUPS];
    unsigned m_NumGroups;
};

//PURPOSE
//  Used to read from a leaderboard.
class rlLeaderboard2Row
{
public:
    //When reading player or clan member leaderboards the gamer
    //handle identifies the gamer owning the row.
    rlGamerHandle m_GamerHandle;

    //Name of the gamer owning the row.
    char m_GamerDisplayName[RL_MAX_NAME_BUF_SIZE];

    //When reading group leaderboards m_GroupSelector identifies
    //the group owning the row.  When reading player, clan, and
    //clan member leaderboards, m_GroupSelector identifies the
    //the group containing the players/members/clans.
    rlLeaderboard2GroupSelector m_GroupSelector;

    //When reading clan and clan member LBs m_ClanId identifies the
    //clan owning the row.
    rlClanId m_ClanId;

    //Name of the clan.
    char m_ClanName[RL_CLAN_NAME_MAX_CHARS];

    //Clan tag
    char m_ClanTag[RL_CLAN_TAG_MAX_CHARS];

    //Clan member count
    unsigned m_ClanMemberCount;

    //Rank of the row within the leaderboard.
    unsigned m_Rank;

    //Column values of the row.
    rlStatValue* m_ColumnValues;

    //Maximum number of column values
    unsigned m_MaxValues;

    //Actual number of column values retrieved
    unsigned m_NumValues;

    rlLeaderboard2Row();

    void Init(rlStatValue* columnValues, const unsigned maxValues);

    void Clear();

private:

    //Prevent copying
    rlLeaderboard2Row(const rlLeaderboard2Row&);
    rlLeaderboard2Row& operator=(const rlLeaderboard2Row&);
};

//PURPOSE
//  Used to write an update to a leaderboard.
class rlLeaderboard2Update
{
public:

    //ID of the leaderboard
    unsigned m_LeaderboardId;
    //Gamer's primary clan.  Pass RL_INVALID_CLAN_ID if there is
    //no primary clan.
    rlClanId m_ClanId;
    //Identifies combination of categories/group IDs to which the
    //leaderboard update should apply.
    rlLeaderboard2GroupSelector m_GroupSelector;

    //The stat values to update.
    rlLeaderboardInputValue* m_InputValues;
    unsigned m_NumValues;
    unsigned m_MaxValues;

    rlLeaderboard2Update();

    void Init(rlLeaderboardInputValue* inputValues,
            const unsigned maxValues);

private:

    //Prevent copying
    rlLeaderboard2Update(const rlLeaderboard2Update&);
    rlLeaderboard2Update& operator=(const rlLeaderboard2Update&);
};

//PURPOSE
//  Handles "player" leaderboards, i.e. leaderboards that rank players
//  against each other.
class rlLeaderboard2PlayerStats
{
public:

    //PURPOSE
    //  Retrieves the size of the leaderboard.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  numRows             - On completion will contain the number of rows.
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  The size of a leaderboard can depend on how it's queried.
    //  For example, if wild cards are passed for any of the group IDs
    //  within the group selector then the size of the leaderboard can be
    //  reported as larger than if no wild cards are passed.
    //
    //  This is an asynchronous operation.  Don't deallocate numRows or
    //  status until the operation completes.
    static bool GetLeaderboardSize(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& groupSelector,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Reads a page of a leaderboard in rank order.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  startRank           - 1-based rank of first row to read.
    //  numRows             - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRank(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const int startRank,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows for specific gamers.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  gamerHandles        - List of gamers for whom to read rows.
    //  numGamers           - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByGamer(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlGamerHandle* gamerHandles,
                            const unsigned numGamers,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows around a "pivot" gamer.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  pivotGamer          - Gamer around which to read rows.
    //  numRows             - Number of rows to read, including the one
    //                        for the pivot gamer.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This function will attempt read a page of rows such that the pivot
    //  gamer appears in the middle of the page, with an equal number of rows above
    //  and below.
    //
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRadius(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlGamerHandle& pivotGamer,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows around a "pivot" score.  The score is
    //  compared against the ranking column of the leaderboard.
    //  This function doesn't support leaderboards ranked on multiple columns.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  pivotScore          - Score around which to read rows.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This function will attempt read a page of rows such that the pivot
    //  score appears in the middle of the page, with an equal number of rows above
    //  and below.
    //
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByScoreFloat(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const double pivotScore,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status);
    static bool ReadByScoreInt(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const s64 pivotScore,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status);

	//PURPOSE
	//  Reads leaderboard rows for specific gamers for a certain platform.
	//PARAMS
	//  localGamerIndex     - local index of gamer making the call.
	//  leaderboardId       - ID of the leaderboard.
	//  groupSelector       - Combination of categories/group IDs.
	//  gamerHandle         - Gamer for whom to read rows.
	//  platformName        - Platform name to read the leaderboard.
	//  numRows             - Number of rows to read.
	//  columns             - List of columns to return.
	//  numColumns          - Number of columns to return.
	//  rows                - Array of pointers to rows.  Will be populated with results.
	//  numRowsReturned     - Upon completion will contain the number of rows returned.
	//  totalRows           - Upon completion will contain the total number of rows
	//                        in the leaderboard.
	//  status              - Can be monitored for completion.
	//NOTES
	//  This is an asynchronous operation.  Don't deallocate columns, rows,
	//  numRowsReturned, totalRows, or status until the operation completes.
	static bool ReadByGamerByPlatformTask(const int localGamerIndex,
											const int leaderboardId,
											const rlLeaderboard2GroupSelector& groupSelector,
											const char* gamerHandle,
											const char* platformName,
											const rlLeaderboardColumnInfo* columns,
											const unsigned numColumns,
											rlLeaderboard2Row** rows,
											unsigned* numRowsReturned,
											unsigned* totalRows,
											netStatus* status);

	//PURPOSE
	//  Writes to a leaderboard.
	//PARAMS
	//  localGamerIndex     - local index of gamer making the call.  This
	//                        is the gamer to which the update will be attributed.
	//  updates             - Array of pointers to updates.
	//  numUpdates          - Number of updates.
	//  status              - Can be monitored for completion.
	//NOTES
	//  This is an asynchronous operation.  Don't deallocate updates
	//  or status until the operation completes.
	static bool Write(const int localGamerIndex,
					const rlLeaderboard2Update** updates,
					const unsigned numUpdates,
					netStatus* status);

    //PURPOSE
    //  Cancels the operation associated with the status object.
    static void Cancel(const netStatus* status);
};

//PURPOSE
//  Handles "clan" leaderboards, i.e. leaderboards that rank clans
//  against each other.
class rlLeaderboard2ClanStats
{
public:

    //PURPOSE
    //  Retrieves the size of the leaderboard.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  numRows             - On completion will contain the number of rows.
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  The size of a leaderboard can depend on how it's queried.
    //  For example, if wild cards are passed for any of the group IDs
    //  within the group selector then the size of the leaderboard can be
    //  reported as larger than if no wild cards are passed.
    //
    //  This is an asynchronous operation.  Don't deallocate numRows or
    //  status until the operation completes.
    static bool GetLeaderboardSize(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& groupSelector,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Reads a page of a leaderboard in rank order.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  startRank           - 1-based rank of first row to read.
    //  numRows             - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRank(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const int startRank,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows for specific clans.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  clanIds             - List of clans for which to read rows.
    //  numClans            - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByClan(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlClanId* clanIds,
                            const unsigned numClans,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows around a "pivot" clan.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  pivotGamer          - Clan around which to read rows.
    //  numRows             - Number of rows to read, including the one
    //                        for the pivot clan.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This function will attempt read a page of rows such that the pivot
    //  clan appears in the middle of the page, with an equal number of rows above
    //  and below.
    //
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRadius(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlClanId pivotClanId,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows around a "pivot" score.  The score is
    //  compared against the ranking column of the leaderboard.
    //  This function doesn't support leaderboards ranked on multiple columns.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  pivotScore          - Score around which to read rows.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This function will attempt read a page of rows such that the pivot
    //  score appears in the middle of the page, with an equal number of rows above
    //  and below.
    //
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByScoreFloat(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const double pivotScore,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status);
    static bool ReadByScoreInt(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const s64 pivotScore,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status);

    //PURPOSE
    //  Cancels the operation associated with the status object.
    static void Cancel(const netStatus* status);
};

//PURPOSE
//  Handles "clan member" leaderboards, i.e. leaderboards that rank clan members
//  from a single clan against each other.
class rlLeaderboard2ClanMemberStats
{
public:

    //PURPOSE
    //  Retrieves the size of the leaderboard.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  clanId              - Identifies the clan containing the clan members.
    //  numRows             - On completion will contain the number of rows.
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  The size of a leaderboard can depend on how it's queried.
    //  For example, if wild cards are passed for any of the group IDs
    //  within the group selector then the size of the leaderboard can be
    //  reported as larger than if no wild cards are passed.
    //
    //  This is an asynchronous operation.  Don't deallocate numRows or
    //  status until the operation completes.
    static bool GetLeaderboardSize(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& groupSelector,
                                    const rlClanId clanId,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Reads a page of a leaderboard in rank order.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  clanId              - Identifies the clan containing the clan members.
    //  startRank           - 1-based rank of first row to read.
    //  numRows             - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRank(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlClanId clanId,
                            const int startRank,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows for specific clan members.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  clanId              - Identifies the clan containing the clan members.
    //  gamerHandles        - List of members for whom to read rows.
    //  numMembers          - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByMember(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlClanId clanId,
                            const rlGamerHandle* gamerHandles,
                            const unsigned numMembers,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows around a "pivot" member.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  clanId              - Identifies the clan containing the clan members.
    //  pivotMember         - Member around which to read rows.
    //  numRows             - Number of rows to read, including the one
    //                        for the pivot member.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This function will attempt read a page of rows such that the pivot
    //  gamer appears in the middle of the page, with an equal number of rows above
    //  and below.
    //
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRadius(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& groupSelector,
                            const rlClanId clanId,
                            const rlGamerHandle& pivotMember,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows around a "pivot" score.  The score is
    //  compared against the ranking column of the leaderboard.
    //  This function doesn't support leaderboards ranked on multiple columns.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  clanId              - Identifies the clan containing the clan members.
    //  pivotScore          - Score around which to read rows.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This function will attempt read a page of rows such that the pivot
    //  score appears in the middle of the page, with an equal number of rows above
    //  and below.
    //
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByScoreFloat(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const rlClanId clanId,
                                const double pivotScore,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status);
    static bool ReadByScoreInt(const int localGamerIndex,
                                const int leaderboardId,
                                const rlLeaderboard2GroupSelector& groupSelector,
                                const rlClanId clanId,
                                const s64 pivotScore,
                                const unsigned numRows,
                                const rlLeaderboardColumnInfo* columns,
                                const unsigned numColumns,
                                rlLeaderboard2Row** rows,
                                unsigned* numRowsReturned,
                                unsigned* totalRows,
                                netStatus* status);

    //PURPOSE
    //  Cancels the operation associated with the status object.
    static void Cancel(const netStatus* status);
};

//PURPOSE
//  Handles "group" leaderboards, i.e. leaderboards that rank groups
//  against each other.
class rlLeaderboard2GroupStats
{
public:

    //PURPOSE
    //  Retrieves the size of the leaderboard.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  numRows             - On completion will contain the number of rows.
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  The size of a leaderboard can depend on how it's queried.
    //  For example, if wild cards are passed for any of the group IDs
    //  within the group selector then the size of the leaderboard can be
    //  reported as larger than if no wild cards are passed.
    //
    //  This is an asynchronous operation.  Don't deallocate numRows or
    //  status until the operation completes.
    static bool GetLeaderboardSize(const int localGamerIndex,
                                    const int leaderboardId,
                                    const rlLeaderboard2GroupSelector& categorySelector,
                                    unsigned* numRows,
                                    netStatus* status);

    //PURPOSE
    //  Reads a page of a leaderboard in rank order.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelector       - Combination of categories/group IDs.
    //  startRank           - 1-based rank of first row to read.
    //  numRows             - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRank(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& categorySelector,
                            const int startRank,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows for specific groups.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  groupSelectors      - Group identifiers.
    //  numGroups           - Number of rows to read.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByGroup(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector* groupSelectors,
                            const unsigned numGroups,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Reads leaderboard rows around a "pivot" group.
    //PARAMS
    //  localGamerIndex     - local index of gamer making the call.
    //  leaderboardId       - ID of the leaderboard.
    //  pivotGroupSelector  - Group around which to read rows.
    //  numRows             - Number of rows to read, including the one
    //                        for the pivot group.
    //  columns             - List of columns to return.
    //  numColumns          - Number of columns to return.
    //  rows                - Array of pointers to rows.  Will be populated with results.
    //  numRowsReturned     - Upon completion will contain the number of rows returned.
    //  totalRows           - Upon completion will contain the total number of rows
    //                        in the leaderboard.
    //  status              - Can be monitored for completion.
    //NOTES
    //  This function will attempt read a page of rows such that the pivot
    //  gamer appears in the middle of the page, with an equal number of rows above
    //  and below.
    //
    //  This is an asynchronous operation.  Don't deallocate columns, rows,
    //  numRowsReturned, totalRows, or status until the operation completes.
    static bool ReadByRadius(const int localGamerIndex,
                            const int leaderboardId,
                            const rlLeaderboard2GroupSelector& pivotGroupSelector,
                            const unsigned numRows,
                            const rlLeaderboardColumnInfo* columns,
                            const unsigned numColumns,
                            rlLeaderboard2Row** rows,
                            unsigned* numRowsReturned,
                            unsigned* totalRows,
                            netStatus* status);

    //PURPOSE
    //  Cancels the operation associated with the status object.
    static void Cancel(const netStatus* status);
};

}   //namespace rage

#endif  //RLINE_RLSTATS_H
