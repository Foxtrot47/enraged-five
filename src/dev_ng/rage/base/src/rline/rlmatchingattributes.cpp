// 
// rline/rlmatchingattributes.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
 
#include "rlmatchingattributes.h"

#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "rldiag.h"
#include "string/string.h"
#include "system/nelem.h"
#include "system/memops.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, matchingattr);
#undef __rage_channel
#define __rage_channel rline_matchingattr

///////////////////////////////////////////////////////////////////////////////
//  rlMatchingAttributes
///////////////////////////////////////////////////////////////////////////////

rlMatchingAttributes::rlMatchingAttributes()
{
    this->Clear();
}


rlMatchingAttributes::rlMatchingAttributes(const unsigned* attrIds,
										   const char* attrNames[],
										   const unsigned count)
{
    this->Reset(attrIds,
				attrNames,
				count);
}

void
rlMatchingAttributes::Clear()
{
    m_Count = 0;
    this->ClearAllValues();
    for(int i = 0; i < COUNTOF(m_Ids); ++i)
    {
        m_Ids[i] = 0;
		m_Names[i][0] = '\0';
    }
}

void
rlMatchingAttributes::ClearAllValues()
{
	m_GameMode = RL_INVALID_GAME_MODE;
    m_SessionPurpose = RL_INVALID_SESSION_TYPE;
    m_AttrMask = 0;
    for(int i = 0; i < COUNTOF(m_Vals); ++i)
    {
        m_Vals[i] = 0;
    }
}

bool
rlMatchingAttributes::IsValid() const
{
    return (RL_INVALID_GAME_MODE != m_GameMode) &&
           (RL_INVALID_SESSION_TYPE != m_SessionPurpose);
}

void
rlMatchingAttributes::Reset(const unsigned* attrIds,
							const char* attrNames[],
							const unsigned count)
{
    rlAssert(count <= RL_MAX_MATCHING_ATTRS);
    rlAssert(count > 0);
    rlAssert(attrIds);

    this->Clear();

    m_Count = count;
    m_AttrMask = 0;

    for(int i = 0; i < (int) count; ++i)
    {
        m_Ids[i] = attrIds[i];
		safecpy(m_Names[i], attrNames[i]);
    }
}

unsigned
rlMatchingAttributes::GetCount() const
{
    return m_Count;
}

unsigned
rlMatchingAttributes::GetAttrId(const unsigned index) const
{
    return rlVerify(index < m_Count) ? m_Ids[index] : 0;
}

const char*
rlMatchingAttributes::GetAttrName(const unsigned index) const
{
	return rlVerify(index < m_Count) ? m_Names[index] : NULL;
}

unsigned
rlMatchingAttributes::GetAttrIds(unsigned (&ids)[RL_MAX_MATCHING_ATTRS]) const
{
    for(int i = 0; i < (int) m_Count; ++i)
    {
        ids[i] = m_Ids[i];
    }

    return m_Count;
}

int
rlMatchingAttributes::GetAttrIndex(const unsigned id) const
{
    int idx = -1;
    for(int i = 0; i < (int) m_Count; ++i)
    {
        if(id == m_Ids[i])
        {
            idx = i;
            break;
        }
    }

    return idx;
}

void
rlMatchingAttributes::SetGameMode(const u16 gameMode)
{
    m_GameMode = gameMode;
}

u16
rlMatchingAttributes::GetGameMode() const
{
    return m_GameMode;
}

void
rlMatchingAttributes::SetSessionPurpose(const u16 sessionPurpose)
{
    m_SessionPurpose = sessionPurpose;
}

u16
rlMatchingAttributes::GetSessionPurpose() const
{
    return m_SessionPurpose;
}

unsigned
rlMatchingAttributes::GetGameModeAttribute() const
{
	return ((m_SessionPurpose & 0xFFFF) << 16) | (m_GameMode & 0xFFFF);
}

void
rlMatchingAttributes::SetValueByIndex(const unsigned index, const unsigned val)
{
    if(rlVerify(index < m_Count))
    {
        m_Vals[index] = val;
        m_AttrMask |= (1 << index);
    }
}

void
rlMatchingAttributes::SetValueById(const unsigned id, const unsigned val)
{
    this->SetValueByIndex(this->GetAttrIndex(id), val);
}

void
rlMatchingAttributes::ClearValueByIndex(const unsigned index)
{
    if(rlVerify(index < m_Count))
    {
        m_AttrMask &= ~(1 << index);
    }
}

void
rlMatchingAttributes::ClearValueById(const unsigned id)
{
    this->ClearValueByIndex(this->GetAttrIndex(id));
}

const u32*
rlMatchingAttributes::GetValueByIndex(const unsigned index) const
{
    return rlVerify(index < m_Count) && (m_AttrMask & (1 << index))
            ? &m_Vals[index]
            : NULL;
}

const u32*
rlMatchingAttributes::GetValueById(const unsigned id) const
{
	int index = this->GetAttrIndex(id);
	return index >= 0 ? this->GetValueByIndex(index) : NULL;
}

bool
rlMatchingAttributes::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    if(!rlVerify(this->IsValid()))
    {
        return false;
    }

	datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    const u32 gameMode = static_cast<u32>(m_GameMode);
    const u32 sessionPurpose = static_cast<u32>(m_SessionPurpose);

    bool success = bb.SerUns(m_Count, datBitsNeeded<RL_MAX_MATCHING_ATTRS>::COUNT)
                    && rlVerifyf(m_Count <= static_cast<unsigned>(RL_MAX_MATCHING_ATTRS), "m_Count[%u] overflow", m_Count)
                    && bb.SerUns(m_AttrMask, RL_MAX_MATCHING_ATTRS)
                    && bb.SerUns(gameMode, 32)
                    && bb.SerUns(sessionPurpose, 32);

	for(int i = 0; i < (int) m_Count && success; ++i)
    {
        if(m_AttrMask & (1 << i))
        {
            success = bb.SerUns(m_Ids[i], 32)
						&& bb.SerStr(m_Names[i], sizeof(m_Names[i]))
                        && bb.SerUns(m_Vals[i], 32);
        }
    }

	if(size){*size = bb.GetNumBytesWritten();}
    return success;
}

bool
rlMatchingAttributes::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    u32 gameMode = 0;
    u32 sessionPurpose = 0;

    bool success = bb.SerUns(m_Count, datBitsNeeded<RL_MAX_MATCHING_ATTRS>::COUNT)
                    && rlVerifyf(m_Count <= static_cast<unsigned>(RL_MAX_MATCHING_ATTRS), "m_Count[%u] overflow", m_Count)
                    && bb.SerUns(m_AttrMask, RL_MAX_MATCHING_ATTRS)
                    && bb.SerUns(gameMode, 32)
                    && bb.SerUns(sessionPurpose, 32);

    m_GameMode = static_cast<u16>(gameMode);
    m_SessionPurpose = static_cast<u16>(sessionPurpose);

	for(int i = 0; i < (int) m_Count && success; ++i)
    {
        if(m_AttrMask & (1 << i))
        {
            success = bb.SerUns(m_Ids[i], 32)
						&& bb.SerStr(m_Names[i], sizeof(m_Names[i]))
                        && bb.SerUns(m_Vals[i], 32);
        }
    }

	success = success && rlVerify(this->IsValid());

    if(size){*size = bb.GetNumBytesRead();}
    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlMatchingFilter
///////////////////////////////////////////////////////////////////////////////

static bool IsValidOp(const unsigned op)
{
    return '==' == op
           || '!=' == op
           || '>=' == op
           || '<=' == op
           || '>' == op
           || '<' == op;
}

rlMatchingFilter::rlMatchingFilter()
{
    this->Clear();
}

void
rlMatchingFilter::Clear()
{
    m_FilterId = INVALID_FILTER_ID;
    m_ConditionCount = 0;
    m_SessionAttrCount = 0;
    m_GameMode = RL_INVALID_GAME_MODE;
    m_SessionPurpose = RL_INVALID_SESSION_TYPE;
    m_ConditionMask = 0;

	sysMemSet(m_SessionAttrNames, 0, sizeof(m_SessionAttrNames));
	sysMemSet(m_FilterName, 0, sizeof(m_FilterName));
	sysMemSet(m_SessionAttrIds, 0, sizeof(m_SessionAttrIds));
    sysMemSet(m_SessionAttrIndicesForConditions, 0, sizeof(m_SessionAttrIndicesForConditions));
    sysMemSet(m_Operators, 0, sizeof(m_Operators));
    sysMemSet(m_ConditionParamIds, 0, sizeof(m_ConditionParamIds));
    sysMemSet(m_ConditionVals, 0, sizeof(m_ConditionVals));
}

void
rlMatchingFilter::Reset(const rlMatchingAttributes& attrs)
{
    this->Clear();

    m_FilterId = AD_HOC_FILTER_ID;
    m_GameMode = attrs.GetGameMode();
    m_SessionPurpose = attrs.GetSessionPurpose();
    m_SessionAttrCount = attrs.GetCount();

    for(int i = 0; i < (int) m_SessionAttrCount; ++i)
    {
        m_SessionAttrIds[i] = attrs.GetAttrId(i);
    }
}

bool
rlMatchingFilter::IsValid() const
{
    return INVALID_FILTER_ID != m_FilterId
        && RL_INVALID_GAME_MODE != m_GameMode
        && RL_INVALID_SESSION_TYPE != m_SessionPurpose;
}

bool
rlMatchingFilter::IsAdHoc() const
{
    return AD_HOC_FILTER_ID == m_FilterId;
}

unsigned
rlMatchingFilter::GetId() const
{
    return m_FilterId;
}

const char*
rlMatchingFilter::GetName() const
{
	return m_FilterName;
}

unsigned
rlMatchingFilter::GetConditionCount() const
{
    return m_ConditionCount;
}

unsigned
rlMatchingFilter::GetSessionAttrCount() const
{
    return m_SessionAttrCount;
}

unsigned
rlMatchingFilter::GetSessionAttrFieldIdForCondition(const unsigned conditionIndex) const
{
    rlAssert(conditionIndex < m_ConditionCount);
    const unsigned idx = m_SessionAttrIndicesForConditions[conditionIndex];
    return m_SessionAttrIds[idx];
}

const char* 
rlMatchingFilter::GetSessionAttrFieldNameForCondition(const unsigned conditionIndex) const
{
	rtry
	{
		rverify(conditionIndex < m_ConditionCount, catchall, );
		const unsigned idx = m_SessionAttrIndicesForConditions[conditionIndex];

		rverify(idx >= 0 && idx < RL_MAX_MATCHING_ATTRS, catchall, );
		return m_SessionAttrNames[idx];
	}
	rcatchall
	{
		return "";
	}
}

unsigned
rlMatchingFilter::GetParamFieldIdForCondition(const unsigned conditionIndex) const
{
    rlAssert(conditionIndex < m_ConditionCount);
	return m_ConditionParamIds[conditionIndex];
}

int
rlMatchingFilter::GetSessionAttrIndexForCondition(const unsigned conditionIndex) const
{
    return rlVerify(conditionIndex < m_ConditionCount)
            ? (int) m_SessionAttrIndicesForConditions[conditionIndex]
            : -1;
}

const unsigned*
rlMatchingFilter::GetSessionAttrFieldIds(unsigned* count) const
{
    *count = m_SessionAttrCount;
    return m_SessionAttrCount ? m_SessionAttrIds : NULL;
}

bool
rlMatchingFilter::GetSessionAttrFieldNames(char names[RL_MAX_MATCHING_ATTRS][RL_MAX_MATCHING_ATTR_NAME_LENGTH]) const
{
	for(unsigned i = 0; i < m_SessionAttrCount; ++i)	
	{
		safecpy(names[i], m_SessionAttrNames[i]);
	}

	return true;
}

unsigned
rlMatchingFilter::GetOperator(const unsigned conditionIndex) const
{
    rlAssert(conditionIndex < m_ConditionCount);
    return m_Operators[conditionIndex];
}

bool
rlMatchingFilter::AddCondition(const unsigned op,
                                const unsigned attrId,
                                const unsigned value)
{
    bool success = false;

    if(rlVerify(this->IsAdHoc())
        && rlVerify(m_ConditionCount < RL_MAX_MATCHING_CONDITIONS)
        && rlVerify(IsValidOp(op)))
    {
        int sessionAttrIdx = -1;

        for(int i = 0; i < (int) m_SessionAttrCount; ++i)
        {
            if(attrId == m_SessionAttrIds[i])
            {
                sessionAttrIdx = i;
                break;
            }
        }

        if(sessionAttrIdx < 0)
        {
            rlError("Invalid attribute id");
        }
        else
        {
            m_SessionAttrIndicesForConditions[m_ConditionCount] = (unsigned) sessionAttrIdx;
            m_Operators[m_ConditionCount] = op;
            ++m_ConditionCount;

            this->SetValue(m_ConditionCount - 1, value);

            success = true;
        }
    }

    return success;
}

void
rlMatchingFilter::SetGameMode(const u16 gameMode)
{
    m_GameMode = gameMode;
}

u16
rlMatchingFilter::GetGameMode() const
{
    return m_GameMode;
}

void
rlMatchingFilter::SetSessionPurpose(const u16 sessionPurpose)
{
    m_SessionPurpose = sessionPurpose;
}

u16
rlMatchingFilter::GetSessionPurpose() const
{
    return m_SessionPurpose;
}

unsigned
rlMatchingFilter::GetGameModeAttribute() const
{
    return ((m_SessionPurpose & 0xFFFF) << 16) | (m_GameMode & 0xFFFF);
}

void
rlMatchingFilter::SetValue(const unsigned conditionIndex, const u32 val)
{
    rlAssert(conditionIndex < this->GetConditionCount());
    rlAssert(m_ConditionParamIds[conditionIndex] > 0);
    m_ConditionVals[conditionIndex] = val;
    m_ConditionMask |= (1 << conditionIndex);
}

void
rlMatchingFilter::ClearValue(const unsigned conditionIndex)
{
    rlAssert(conditionIndex < this->GetConditionCount());
    m_ConditionMask &= ~(1 << conditionIndex);
}

const u32*
rlMatchingFilter::GetValue(const unsigned conditionIndex) const
{
    rlAssert(conditionIndex < this->GetConditionCount());
    return (m_ConditionMask & (1 << conditionIndex))
            ? &m_ConditionVals[conditionIndex]
            : NULL;
}

bool
rlMatchingFilter::IsMatch(const rlMatchingAttributes& attrs) const
{
    rlAssert(this->IsValid());
    rlAssert(attrs.IsValid());

    bool isMatch = (this->GetGameMode() == attrs.GetGameMode());
    isMatch &= (this->GetSessionPurpose() == attrs.GetSessionPurpose());

    for(int i = 0; isMatch && i < (int) this->GetConditionCount(); ++i)
    {
        const int attrIdx = this->GetSessionAttrIndexForCondition(i);
        const u32* attrVal = attrs.GetValueByIndex(attrIdx);

        if(!attrVal)
        {
            isMatch = false;
            break;
        }

        const u32* myVal = this->GetValue(i);
        if(!myVal)
        {
            //Nil filter attributes match any session attribute.
            continue;
        }

        switch(this->GetOperator(i))
        {
        case '==': isMatch = (*attrVal == *myVal); break;
        case '!=': isMatch = (*attrVal != *myVal); break;
        case '>=': isMatch = (*attrVal >= *myVal); break;
        case '<=': isMatch = (*attrVal <= *myVal); break;
        case '>': isMatch = (*attrVal > *myVal); break;
        case '<': isMatch = (*attrVal < *myVal); break;
        }
    }

    return isMatch;
}

bool
rlMatchingFilter::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    if(!rlVerify(this->IsValid()))
    {
        return false;
    }

    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    const u32 gameMode = static_cast<u32>(m_GameMode);
    const u32 sessionPurpose = static_cast<u32>(m_SessionPurpose);

    bool success = bb.SerUns(m_FilterId, 32)
					&& bb.SerStr(m_FilterName, sizeof(m_FilterName))
                    && bb.SerUns(m_ConditionCount, 32)
                    && bb.SerUns(gameMode, 32)
                    && bb.SerUns(sessionPurpose, 32)
                    && bb.SerUns(m_SessionAttrCount, 32)
                    && bb.SerUns(m_ConditionMask, 32);

    rlAssertf(m_SessionAttrCount <= static_cast<unsigned>(COUNTOF(m_SessionAttrIds)), "m_SessionAttrCount[%u] overflow", m_SessionAttrCount);
    int attrCount = rage::Min(m_SessionAttrCount, static_cast<unsigned>(COUNTOF(m_SessionAttrIds)));

    for(int i = 0; success && i < (int) attrCount && success; ++i)
    {
        success = bb.SerUns(m_SessionAttrIds[i], 32)
					&& bb.SerStr(m_SessionAttrNames[i], sizeof(m_SessionAttrNames[i]));
    }

    rlAssertf(m_ConditionCount <= static_cast<unsigned>(RL_MAX_MATCHING_CONDITIONS), "m_ConditionCount[%u] overflow", m_ConditionCount);
    int conditionCount = rage::Min(m_ConditionCount, static_cast<unsigned>(RL_MAX_MATCHING_CONDITIONS));

    for(int i = 0; success && i < (int) conditionCount && success; ++i)
    {
        success = bb.SerUns(m_SessionAttrIndicesForConditions[i], 32)
                    && bb.SerUns(m_Operators[i], 32)
                    && bb.SerUns(m_ConditionVals[i], 32);
    }

    if(size){*size = bb.GetNumBytesWritten();}
    return success;
}

bool
rlMatchingFilter::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    u32 gameMode = 0;
    u32 sessionPurpose = 0;

    bool success = bb.SerUns(m_FilterId, 32)
					&& bb.SerStr(m_FilterName, sizeof(m_FilterName))
                    && bb.SerUns(m_ConditionCount, 32)
                    && bb.SerUns(gameMode, 32)
                    && bb.SerUns(sessionPurpose, 32)
                    && bb.SerUns(m_SessionAttrCount, 32)
                    && bb.SerUns(m_ConditionMask, 32);

    m_GameMode = static_cast<u16>(gameMode);
    m_SessionPurpose = static_cast<u16>(sessionPurpose);

    rlAssertf(m_SessionAttrCount <= static_cast<unsigned>(COUNTOF(m_SessionAttrIds)), "m_SessionAttrCount[%u] overflow", m_SessionAttrCount);
    m_SessionAttrCount = rage::Min(m_SessionAttrCount, static_cast<unsigned>(COUNTOF(m_SessionAttrIds)));

    for(int i = 0; success && i < (int) m_SessionAttrCount && success; ++i)
    {
        success = bb.SerUns(m_SessionAttrIds[i], 32)
					&& bb.SerStr(m_SessionAttrNames[i], sizeof(m_SessionAttrNames[i]));
    }

    rlAssertf(m_ConditionCount <= static_cast<unsigned>(RL_MAX_MATCHING_CONDITIONS), "m_ConditionCount[%u] overflow", m_ConditionCount);
    m_ConditionCount = rage::Min(m_ConditionCount, static_cast<unsigned>(RL_MAX_MATCHING_CONDITIONS));

    for(int i = 0; success && i < (int) m_ConditionCount && success; ++i)
    {
        success = bb.SerUns(m_SessionAttrIndicesForConditions[i], 32)
                    && bb.SerUns(m_Operators[i], 32)
                    && bb.SerUns(m_ConditionVals[i], 32);
    }

    success = success && rlVerify(this->IsValid());

    if(size){*size = bb.GetNumBytesRead();}
    return success;
}

}   //namespace rage
