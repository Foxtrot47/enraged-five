// 
// rline/rlfriendmanagertasks.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLFRIENDMANAGERTASKS_H
#define RLINE_RLFRIENDMANAGERTASKS_H

#include "atl/array.h"
#include "diag/channel.h"
#include "net/task.h"
#include "rline/rl.h"
#include "rline/rltask.h"
#include "rlfriendsmanager.h"

namespace rage
{
	// fwd decl
	struct rlFriendsReference;

	// channel decl
	RAGE_DECLARE_SUBCHANNEL(rline, friendtasks)

	class rlFriendsManagerTask
	{
	public:
		static int SortFriendsById(rlFriendsReference* const* lhs, rlFriendsReference* const* rhs);
	};

#if RSG_DURANGO

	class rlFriendsMgrInitializationTask : public netTask
	{
	public:

		NET_TASK_DECL(rlFriendsMgrInitializationTask);
		NET_TASK_USE_CHANNEL(rline_friendtasks);

		rlFriendsMgrInitializationTask();

		bool Configure(const int localGamerIndex, atArray<rlFriendsReference*>* friendsRefsByStatus, atArray<rlFriendsReference*>* friendRefsById, unsigned * totalFriendsPtr);
		virtual void OnCancel();
		virtual netTaskStatus OnUpdate(int* /*resultCode*/);
		static int SortFriendsById(rlFriendsReference* const* lhs, rlFriendsReference* const* rhs);

	private:

		void Complete(netTaskStatus status);
		bool GetInitialFriendsList();
		bool GetInitialFriendPresence();
		bool SubscribeToFriends();

		enum State
		{
			STATE_GET_FRIENDS,
			STATE_GETTING_FRIENDS,
			STATE_GET_PRESENCE,
			STATE_GETTING_PRESENCE,
			STATE_SUBSCRIBE
		};

		State m_State;
		netStatus m_ReadStatus;
		netStatus m_PresenceStatus;

		int m_LocalGamerIndex;
		atArray<rlFriendsReference*>* m_FriendRefsByStatus;
		atArray<rlFriendsReference*>* m_FriendRefsById;
		unsigned* m_TotalFriendsPtr;
	};

#elif RSG_ORBIS

	class rlFriendsMgrInitializationTask : public netTask
	{
	public:

		NET_TASK_DECL(rlFriendsMgrInitializationTask);
		NET_TASK_USE_CHANNEL(rline_friendtasks);

		rlFriendsMgrInitializationTask();

		bool Configure(const int localGamerIndex, bool bSubscribe, atArray<rlFriendsReference*>* friendRefsById, rlFriendsPage* initialPage);
		virtual void OnCancel();
		virtual netTaskStatus OnUpdate(int* /*resultCode*/);
		static int SortFriendsById(rlFriendsReference* const* lhs, rlFriendsReference* const* rhs);

	private:
		void Complete(netTaskStatus status);
		bool GetFriends();

		bool AddFriend(const rlFriendsReference& ref);
		bool HasFriend(const rlFriendsReference& ref);

		enum State
		{
			STATE_GET_FRIENDS,
			STATE_GETTING_FRIENDS,			
		};

		State m_State;
		netStatus m_ReadStatus;

		int m_LocalGamerIndex;
		int m_CurrentStartIndex;
		bool m_bSubscribe;

		rlFriendsPage m_FriendPage;
		rlFriendsPage* m_InitialPagePtr;
		atArray<rlFriendsReference*>* m_FriendRefsById;
	};

#endif

}   //namespace rage

#endif  //RLINE_RLFRIENDMANAGERTASKS_H
