// 
// rline/rl.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rl.h"
#include "clan/rlclan.h"
#include "cloud/rlcloud.h"
#include "data/autoid.h"
#include "diag/tracker.h"
#include "facebook/rlfacebook.h"
#include "file/tcpip.h"
#include "net/netfuncprofiler.h"
#include "net/nethardware.h"
#include "net/netsocket.h"
#include "net/status.h"
#include "net/net.h"
#include "rline/cloudsave/rlcloudsave.h"
#include "rline/entitlementV1/rlrosentitlementV1.h"
#include "rline/livestream/rllivestream.h"
#include "rline/rlfriendsmanager.h"
#include "rline/rltelemetry.h"
#include "rline/rltitleid.h"
#include "rline/rlpresence.h"
#include "rline/rlprivileges.h"
#include "rline/rlsessionmanager.h"
#include "ros/rlros.h"
#include "scachievements/rlscachievements.h"
#include "scmatchmaking/rlscmatchmaking.h"
#include "scmembership/rlscmembership.h"
#include "scfriends/rlscfriends.h"
#include "scpresence/rlscpresence.h"
#include "string/stringutil.h"
#include "system/memory.h"
#include "system/timer.h"
#include "unlocks/rlunlocks.h"
#include "system/language.h"

#if RSG_PC && !__RGSC_DLL
#include "entitlement/rlrosentitlement.h"
#endif

#include <time.h>

#if RSG_DURANGO
#include "durango/rlxbl_interface.h"
#include "system/xtl.h"
#elif RSG_ORBIS
#include "rlnp.h"
#include <np.h>
#elif RSG_PC
#include "rlpc.h"
#endif

namespace rage
{

RAGE_DEFINE_CHANNEL(rline,
					DIAG_SEVERITY_DEBUG3,
					DIAG_SEVERITY_DISPLAY, 
					DIAG_SEVERITY_ASSERT,
					CHANNEL_POSIX_ON);

#if RSG_SCE
CompileTimeAssert(RL_MAX_NAME_BUF_SIZE >= SCE_USER_SERVICE_MAX_USER_NAME_LENGTH);
#endif

#if __DEV
PARAM(sanityCheckRlHeap, "Run a sanity check on the rline heap");
#endif

#if !__FINAL
PARAM(minAgeRating, "Overrides minimum age rating for the game");
#endif

PARAM(rlEnvironment, "Set the environment to use (dev, stage-dev, cert, stage-cert ...)");

PARAM(fiddler, "Enable fiddler HTTP captures");
PARAM(fiddlerwebsocket, "[rline] Enable fiddler Websocket captures. Also enables HTTP captures as side effect", "Disabled", "All", "");

#if __FINAL_LOGGING
NOSTRIP_XPARAM(nethttpproxy);
NOSTRIP_XPARAM(nethttpsnoproxy);
#else
XPARAM(nethttpproxy);
XPARAM(nethttpsnoproxy);
#endif

#if __RGSC_DLL || __MASTER
XPARAM(rlrosdomainenv);
#else
NOSTRIP_PC_XPARAM(rlrosdomainenv);
#endif
NOSTRIP_PC_XPARAM(netUseStagingEnv);

XPARAM(nethttpstunnel);
XPARAM(rlsslnocert);
XPARAM(netpresenceserveraddr);
XPARAM(netpresencesecure);
XPARAM(netrelayserveraddr);
XPARAM(scAuthStagingEnv);

#if RL_FORCE_ANY_ENVIRONMENT
PARAM(rlIgnoreForcedEnvironment, "[rline] Escapes RL_FORCE_*_ENVIRONMENT and uses environment as supplied by platform");
PARAM(usestandarddevenv, "[rline] Escapes RL_FORCE_*_ENVIRONMENT and uses environment as supplied by platform");
#endif

#if RSG_NP
XPARAM(disabledevspintcheck);
#endif

#if RSG_XBOX
XPARAM(nethttpnoxhttp);
#endif

#if USE_NET_ASSERTS
static const unsigned netMaxIgnore = 128;
static unsigned netlastIgnoredIndex=0;
static rage::u64 netIgnoredVerify[128];

#define VERIFY_ERROR(tag,fmt,...)		diagErrorf(channel,RAGE_LOG_FMT(fmt),##__VA_ARGS__)

bool _NetworkVerifyf(const diagChannel &channel, const char* condAsString, const char *file, int line, const char* fmt, ...)
{
	rage::u64 ignoreCode = (rage::u64)(size_t)file << 32 | line;
	for (unsigned i=0; i<netlastIgnoredIndex; i++)
	{
		if(netIgnoredVerify[i] == ignoreCode)
		{
			return false;
		} 
	}
	rage::sysStack::PrintStackTrace();
	static const unsigned MAX_LOG_LINE_LENGTH = 1024;
	char buffer[MAX_LOG_LINE_LENGTH];
	va_list args;
	va_start(args,fmt);
	vsnprintf(buffer, MAX_LOG_LINE_LENGTH, fmt, args);
	va_end(args);
	VERIFY_ERROR(channel, "NetworkVerifyf(%s) FAILED: %s", condAsString, buffer); 
	if(netlastIgnoredIndex<netMaxIgnore)
	{
		netIgnoredVerify[netlastIgnoredIndex++]=ignoreCode;
	}
	return false;
}
#endif

#if RSG_NP

#if RSG_NP
CompileTimeAssert(RL_PRESENCE_MAX_BUF_SIZE == SCE_NP_BASIC_MAX_PRESENCE_SIZE);
#endif

rlSceNpOnlineId rlSceNpOnlineId::RL_INVALID_ONLINE_ID;

void
rlSceNpOnlineId::Clear()
{
    sysMemSet(this, 0, sizeof(*this));
}

bool
rlSceNpOnlineId::IsValid() const
{
	return data[0] != 0;
}

void rlSceNpOnlineId::FromString(const char* onlineId)
{
	safecpy(data, onlineId, sizeof(data) + 1);
	term = 0;
}

void
rlSceNpOnlineId::Reset(const SceNpOnlineId& that)
{
    CompileTimeAssert(sizeof(rlSceNpOnlineId) == sizeof(that));
    *this = (const rlSceNpOnlineId&) that;
}

SceNpOnlineId&
rlSceNpOnlineId::AsSceNpOnlineId()
{
    CompileTimeAssert(sizeof(SceNpOnlineId) == sizeof(*this));
    return *((SceNpOnlineId*) this);
}

const SceNpOnlineId&
rlSceNpOnlineId::AsSceNpOnlineId() const
{
    return const_cast<rlSceNpOnlineId*>(this)->AsSceNpOnlineId();
}

bool
rlSceNpOnlineId::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    datBitBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    const bool success = bb.WriteBytes(this, sizeof(*this));

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
rlSceNpOnlineId::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datBitBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    const bool success = bb.ReadBytes(this, sizeof(*this)) ;

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

void
rlSceNpId::Clear()
{
    sysMemSet(this, 0, sizeof(*this));
}

void
rlSceNpId::Reset(const SceNpId& that)
{
    CompileTimeAssert(sizeof(rlSceNpId) == sizeof(that));
    *this = (const rlSceNpId&) that;
}

SceNpId&
rlSceNpId::AsSceNpId()
{
    CompileTimeAssert(sizeof(SceNpId) == sizeof(*this));
    return *((SceNpId*) this);
}

const SceNpId&
rlSceNpId::AsSceNpId() const
{
    return const_cast<rlSceNpId*>(this)->AsSceNpId();
}

bool
rlSceNpId::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    datBitBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    const bool success = bb.WriteBytes(this, sizeof(*this));

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
rlSceNpId::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datBitBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    const bool success = bb.ReadBytes(this, sizeof(*this)) ;

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

//rlSceNpAccountId

// value of SCE_NP_INVALID_ACCOUNT_ID (not defined in SDK 1.7)
const rlSceNpAccountId RL_INVALID_NP_ACCOUNT_ID = 0;

#if RSG_ORBIS

//rlSceNpSessionId
void
rlSceNpSessionId::Clear()
{
    sysMemSet(this, 0, sizeof(*this));
}

void
rlSceNpSessionId::Reset(const SceNpSessionId& that)
{
    CompileTimeAssert(sizeof(rlSceNpSessionId) == sizeof(that));
    *this = (const rlSceNpSessionId&) that;
}

SceNpSessionId&
rlSceNpSessionId::AsSceNpSessionId()
{
    CompileTimeAssert(sizeof(SceNpSessionId) == sizeof(*this));
    return *((SceNpSessionId*) this);
}

const SceNpSessionId&
rlSceNpSessionId::AsSceNpSessionId() const
{
    return const_cast<rlSceNpSessionId*>(this)->AsSceNpSessionId();
}

bool rlSceNpSessionId::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	const bool success = bb.SerBytes(data, sizeof(data));

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool rlSceNpSessionId::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	this->term = 0;
	this->padding[0] = this->padding[1] = 0;

	const bool success = bb.SerBytes(data, sizeof(data));

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

//rlSceNpClientId
void
rlSceNpClientId::Clear()
{
    sysMemSet(this, 0, sizeof(*this));
}

void
rlSceNpClientId::Reset(const SceNpClientId& that)
{
    CompileTimeAssert(sizeof(rlSceNpClientId) == sizeof(that));
    *this = (const rlSceNpClientId&) that;
}

SceNpClientId&
rlSceNpClientId::AsSceNpClientId()
{
    CompileTimeAssert(sizeof(SceNpClientId) == sizeof(*this));
    return *((SceNpClientId*) this);
}

const SceNpClientId&
rlSceNpClientId::AsSceNpClientId() const
{
    return const_cast<rlSceNpClientId*>(this)->AsSceNpClientId();
}

#endif

#endif 

static bool s_RlInitialized = false;

//The rline global allocator - available for all rline subsystems.
sysMemAllocator* g_rlAllocator = 0;

//The rline global socket.
//Each subsystem requiring a socket should create a virtual socket
//(netVSocket) and register it with the rline global socket.  The
//rline global socket *should never* be used directly.
//
//The application must ensure that the socket remains valid until
//calling rlShutdown().
//
//The app must also be sure to call Receive() on the socket as
//often as possible. Calling netSocket::Receive() is the only
//way inbound packets will be queued to a virtual socket.
netSocket* g_rlSkt = 0;

static rlTaskManager s_RlTaskMgr;

//Mem for the title id. It's done this way b/c rlTitleId doesn't
//have a default ctor.
static u8 s_TitleIdBuf[sizeof(rlTitleId)];

//Global ptr to title information available to subsystems.
const rlTitleId* g_rlTitleId = nullptr;

static unsigned s_MinAgeRating;

static sysLanguage s_Language = LANGUAGE_ENGLISH;

#if RL_FORCE_ANY_ENVIRONMENT || RSG_OUTPUT
static bool s_IsStagingEnv = false; // True if we're connecting to a staging environment. Used purely for debugging.
#endif

#if RL_SC_PLATFORMS
static rlEnvironment s_NativeEnvironment = RL_ENV_UNKNOWN;
static rlEnvironment s_RosEnvironment = RL_ENV_UNKNOWN;
#endif

rlEnvironment
rlGetNativeEnvironment()
{
#if RSG_NP
	return rlNp::GetRlEnvironmentFromNpEnvironment(g_rlNp.GetNativeEnvironment());
#elif RSG_XBOX
	return rlXbl::GetNativeEnvironment();
#elif RL_SC_PLATFORMS
	return s_NativeEnvironment;
#else
	return RL_ENV_UNKNOWN;
#endif
}

rlEnvironment
rlGetEnvironment()
{
#if RSG_NP
	return rlNp::GetRlEnvironmentFromNpEnvironment(g_rlNp.GetEnvironment());
#elif RSG_XBOX
	return rlXbl::GetEnvironment();
#elif RL_SC_PLATFORMS
	return s_RosEnvironment;
#else
	return RL_ENV_UNKNOWN;
#endif
}

const char* rlGetEnvironmentString(const rlEnvironment environment)
{
	switch(environment)
	{
	case RL_ENV_CI_RAGE:    return "rage"; break;
	case RL_ENV_DEV:        return "dev"; break;
	case RL_ENV_CERT:       return "cert"; break;
	case RL_ENV_CERT_2:     return "cert2"; break;
	case RL_ENV_PROD:       return "prod"; break;
	default:                return nullptr;
	}
}

rlGameRegion 
rlGetGameRegion()
{
#if RSG_XBOX
	return rlXbl::GetGameRegion();
#elif RSG_NP
    return rlNp::GetGameRegion();
#elif RSG_PC
    // TODO: NS - region?
    return RL_GAME_REGION_INVALID;
#else
    return RL_GAME_REGION_INVALID;
#endif
}

void rlEnvironmentInit();
void rlStagingEnvironmentInit(const char* stagingEnv, const char* rosEnv, const char* proxy, const char* presence, const char* relay);
bool rlCheckHostsFile(const char* env);
void rlFiddlerInit();

bool
rlInit(sysMemAllocator* allocator,
        netSocket* skt,
        rlTitleId *titleId,
        const unsigned minAgeRating)
{
    rlAssert(titleId);

    bool success = false;

    if(rlVerify(!s_RlInitialized))
    {
        g_rlAllocator = allocator;
        g_rlSkt = skt;
        sysMemCpy(s_TitleIdBuf, titleId, sizeof(s_TitleIdBuf));
        g_rlTitleId = (const rlTitleId*)s_TitleIdBuf;
        g_rlAllocator->BeginLayer();
        s_RlTaskMgr.Init(g_rlAllocator);
        s_RlInitialized = true;

        success = true;

		// Set up the debug environment from the command line
		rlEnvironmentInit();

		// Initialise title Id, should be called after rlEnvironmentInit
		// This operates on the buffer because g_rlTitleId is const (this is ugly but our title Id 
		// management is a bit of a clunk anyway)
		rlTitleId* titleId = (rlTitleId*)s_TitleIdBuf;
		titleId->Init();

		// Should be called before rlFiddlerInit so manual proxies take precedence
		ENABLE_TCP_PROXY_ARGS_ONLY(netHttpProxy::InitProxies());

		// setup fiddler command lines before initializing platform systems but after the env
		rlFiddlerInit();

#if RSG_DURANGO
		success = success && rlVerifyf(g_rlXbl.Init(allocator), "Error initializing rlXbl");
#elif RSG_NP
        success = success && rlVerifyf(g_rlNp.Init(), "Error initializing rlNp");
#elif RSG_PC && !__RGSC_DLL

		// game can still run when the pc platform DLLs are not available
        rlVerify(g_rlPc.Init());
#endif

        success = success && rlVerifyf(rlRos::InitClass(), "Error initializing rlRos");
#if !__RGSC_DLL
        success = success && rlVerifyf(rlScAchievements::InitClass(), "Error initializing rlScAchievement");
		success = success && rlVerifyf(rlScMatchmaking::InitClass(), "Error initializing rlScMatchmaking");
#endif

		success = success && rlVerifyf(rlCloud::Init(), "Error initializing rlCloud");

#if SC_PRESENCE
        success = success && rlVerifyf(rlScPresence::Init(), "Error initializing rlScPresence");
#endif

        success = success && rlVerifyf(rlPresence::Init(), "Error initializing rlPresence");

#if RL_FACEBOOK_ENABLED
		success = success && rlVerifyf(rlFacebook::Init(), "Error initializing rlFacebook");
#endif // RL_FACEBOOK_ENABLED

		success = success && rlVerifyf(rlPrivileges::Init(), "Error initializing rlPrivileges");

        success = success && rlVerifyf(rlScFriends::Init(), "Error initializing rlScFriends");

		success = success && rlVerifyf(rlFriendsManager::InitClass(), "Error initializing rlFriendsManager");

		success = success && rlVerifyf(rlLiveStream::InitClass(), "Error initializing rlLiveStream");

#if RSG_PC && !__RGSC_DLL
		success = success && rlVerifyf(rlRosEntitlement::Init(), "Error initializing rlRosEntitlement");
#elif ENABLE_ENTITLEMENT_V1
		success = success && rlVerifyf(rlRosEntitlementV1::Init(), "Error initializing rlRosEntitlement");
#endif

#if RL_SC_MEMBERSHIP_ENABLED
		success = success && rlVerifyf(rlScMembership::Init(), "Error initializing rlScMembership");
#endif

#if !__RGSC_DLL
        success = success && rlVerifyf(rlClan::Init(), "Error initializing rlClan");
        //success = success && rlVerifyf(rlUnlocks::Init(), "Error initializing rlUnlocks");
		success = success && rlVerifyf(rlSessionManager::Init(), "Error initializing rlSessionManager");
#else
		success = success && rlVerifyf(rlCloudSave::Init(), "Error initializing SP cloud saves");
#endif

        s_MinAgeRating = minAgeRating;

        AutoIdInit();

#if RSG_ORBIS
		g_rlNp.SetContentRestriction( s_MinAgeRating );
#endif // RSG_ORBIS

        if(!success)
        {
            rlShutdown();
        }
    }

    return success;
}

PARAM(norlupdate,"Disable all RL net update");
PARAM(nonpupdate,"Disable NP update");

void
rlUpdate()
{
	if(PARAM_norlupdate.Get())
		return;

    if(rlVerify(rlIsInitialized()))
    {
        NPROFILE(s_RlTaskMgr.Update());

#if RSG_DURANGO
		NPROFILE(g_rlXbl.Update());
#elif RSG_NP
# if !__FINAL
		if(!PARAM_nonpupdate.Get())
# endif
		{
			NPROFILE(g_rlNp.Update());
		}
#endif

#if RSG_PC && !__RGSC_DLL
		NPROFILE(g_rlPc.Update());
#endif

        NPROFILE(rlCloud::Update());

#if !__RGSC_DLL
		NPROFILE(rlScMatchmaking::UpdateClass());
#endif

        NPROFILE(rlRos::UpdateClass());

#if !__RGSC_DLL
        NPROFILE(rlTelemetry::UpdateClass());
#endif

#if SC_PRESENCE
		NPROFILE(rlScPresence::Update());
#endif

        NPROFILE(rlPresence::Update());
		NPROFILE(rlPrivileges::Update());

#if RSG_PC && !__RGSC_DLL
		NPROFILE(rlRosEntitlement::Update());
#elif ENABLE_ENTITLEMENT_V1
		NPROFILE(rlRosEntitlementV1::Update());
#endif

#if RL_SC_MEMBERSHIP_ENABLED
		NPROFILE(rlScMembership::Update());
#endif

        NPROFILE(rlScFriends::Update());
		NPROFILE(rlFriendsManager::Update());
		NPROFILE(rlLiveStream::Update());

#if !__RGSC_DLL
		NPROFILE(rlClan::Update();)
		NPROFILE(rlSessionManager::Update();)
#endif

#if __DEV
        if(g_rlAllocator && PARAM_sanityCheckRlHeap.Get())
        {
            NPROFILE(g_rlAllocator->SanityCheck());
        }
#endif // __DEV

#if !__NO_OUTPUT
		static size_t lowWaterMark = 0x7FFFFFFF;
		sysMemAllocator *current = g_rlAllocator->GetAllocator(MEMTYPE_GAME_VIRTUAL);
		size_t curLowWaterMark = current->GetLowWaterMark(false);
		size_t heapSize = current->GetHeapSize();
		size_t memUsed = heapSize - curLowWaterMark;

		if(curLowWaterMark < lowWaterMark)
		{
			lowWaterMark = curLowWaterMark;
			rlDebug2("rline memory low water mark: %" SIZETFMT "u", curLowWaterMark);
			rlDebug2("rline max memory used: %" SIZETFMT "u (%.2f%%)", memUsed, ((float)memUsed / (float)heapSize) * 100.0f);
			rlDebug2("rline largest free block: %" SIZETFMT "u", current->GetLargestAvailableBlock());
		}
#endif
    }
}

void
rlShutdown()
{
    if(s_RlInitialized)
    {
        AutoIdShutdown();

#if !__RGSC_DLL
		rlSessionManager::Shutdown();
        //rlUnlocks::Shutdown();
		rlClan::Shutdown();
#else
		rlCloudSave::Shutdown();
#endif

        rlScFriends::Shutdown();
		rlFriendsManager::ShutdownClass();
		rlLiveStream::ShutdownClass();

        rlCloud::Shutdown();

#if !__RGSC_DLL
		rlScMatchmaking::ShutdownClass();
        rlScAchievements::ShutdownClass();
#endif

		rlRos::ShutdownClass();

#if RL_FACEBOOK_ENABLED
		rlFacebook::Shutdown();
#endif // #if RL_FACEBOOK_ENABLED

        rlPresence::Shutdown();
		rlPrivileges::Shutdown();

#if SC_PRESENCE
        rlScPresence::Shutdown();
#endif

#if RL_SC_MEMBERSHIP_ENABLED
		rlScMembership::Shutdown();
#endif

#if RSG_DURANGO
		g_rlXbl.Shutdown();
#elif RSG_NP
        g_rlNp.Shutdown();
#endif

#if RSG_PC && !__RGSC_DLL
		g_rlPc.Shutdown();
#endif

        g_rlAllocator->EndLayer("rline", NULL);
        g_rlSkt = 0;
        sysMemSet(s_TitleIdBuf, 0, sizeof(s_TitleIdBuf));
        s_RlTaskMgr.Shutdown();
        g_rlTitleId = nullptr;
        g_rlAllocator = 0;

        s_RlInitialized = false;
    }
}

#if RL_FORCE_ANY_ENVIRONMENT
// PURPOSE
//	Used to track whether a forced environment has been applied
static bool s_HasAppliedForceEnvironment = false;

// PURPOSE
//	Checks whether we have any settings that mean we escape or ignore the forced environment
bool rlHasEscapedForcedEnvironment()
{
#if RSG_OUTPUT
	static bool s_HasLoggedCommandLine = false;
#endif

	if(PARAM_rlIgnoreForcedEnvironment.Get() || PARAM_usestandarddevenv.Get())
	{
#if RSG_OUTPUT
		if(!s_HasLoggedCommandLine)
		{
			s_HasLoggedCommandLine = true;
			rlDebug("rlHasEscapedForcedEnvironment :: Escaping forced environment (%s) with command line", rlGetEnvironmentString(RL_FORCED_ENVIRONMENT));
		}
#endif
		return true;
	}

	const rlRosEnvironment forcedRosEnvironment = rlRosGetForcedEnvironment();
	if(forcedRosEnvironment != RLROS_ENV_UNKNOWN)
	{
#if RSG_OUTPUT
		if(!s_HasLoggedCommandLine)
		{
			s_HasLoggedCommandLine = true;
			rlDebug("rlHasEscapedForcedEnvironment :: Using command line to force environment (%s)", rlRosGetEnvironmentAsString(forcedRosEnvironment));
		}
#endif
		return true;
	}

	return false;
}

// PURPOSE
//	Checks whether we are allowed to force the environment in our current configuration
bool rlIsAllowedToForceEnvironment(const rlEnvironment nativeEnvironment)
{
	// we are only allowed to apply the forced environment in the native dev environment
	// or RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT (true when build flags indicate a specific environment)
	if(nativeEnvironment == RL_ENV_DEV  || RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT)
		return true;

	return false;
}

bool rlIsForcedEnvironment()
{
	return s_HasAppliedForceEnvironment;
}
#endif // RL_FORCE_ANY_ENVIRONMENT

// make sure that RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT cannot be accidentally enabled on non-SC platforms
CompileTimeAssert(!RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT || RL_SC_PLATFORMS);

bool rlValidateEnvironment()
{
#if RL_FORCE_ANY_ENVIRONMENT
	// if the environment isn't forced, all good
	if(!rlIsForcedEnvironment())
		return true;

	// if forced, make sure it's allowed to be forced - if not, quit because this is bad
	const rlEnvironment nativeEnvironment = rlGetNativeEnvironment();
	if(!rlIsAllowedToForceEnvironment(nativeEnvironment))
	{
		rlDebug("rlValidateEnvironment :: Failed! NativeEnv: %s, AllowedInAny: %s", rlGetEnvironmentString(nativeEnvironment), RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT ? "True" : "False");
		Quitf("rlScAuth :: Quitting due to forced environment when not allowed! NativeEnv: %s, AllowedInAny: %s", rlGetEnvironmentString(nativeEnvironment), RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT ? "True" : "False");
		return false;
	}
#endif // RL_FORCE_ANY_ENVIRONMENT

	return true;
}

rlEnvironment rlCheckAndApplyForcedEnvironment(const rlEnvironment nativeEnv)
{
	rlDebug("rlCheckAndApplyForcedEnvironment :: NativeEnv: %s, IsForcedEnv: %s, IsForcedStageEnv: %s", 
		rlGetEnvironmentString(nativeEnv),
		RL_FORCE_ANY_ENVIRONMENT ? "True" : "False",
		RL_FORCE_STAGE_ENVIRONMENT ? "True" : "False");

	rlAssertf(nativeEnv != RL_ENV_UNKNOWN, "rlCheckAndApplyForcedEnvironment :: Called before native environment is known!");

#if RL_FORCE_ANY_ENVIRONMENT
	rlDebug("rlCheckAndApplyForcedEnvironment :: ForcedEnv: %s", rlGetEnvironmentString(RL_FORCED_ENVIRONMENT));
		
	// check we are allowed to force the environment
	if(!rlIsAllowedToForceEnvironment(nativeEnv))
	{
		rlDebug("rlCheckAndApplyForcedEnvironment :: Use native environment (%s): rlIsAllowedToForceEnvironment returned false", rlGetEnvironmentString(nativeEnv));
		return nativeEnv;
	}

	// check if we have escaped the forced environment
	if(rlHasEscapedForcedEnvironment())
	{
		rlDebug("rlCheckAndApplyForcedEnvironment :: Use native environment (%s): rlIsAllowedToForceEnvironment returned true", rlGetEnvironmentString(nativeEnv));
		return nativeEnv;
	}

	// allowed and not escaped - apply the force (only do this once)
	if(!s_HasAppliedForceEnvironment)
	{
		rlDebug("rlCheckAndApplyForcedEnvironment :: Use forced environment (%s)", rlGetEnvironmentString(RL_FORCED_ENVIRONMENT));

#if RL_FORCE_STAGE_ENVIRONMENT
		rlDebug("\tProxy: %s, Presence: %s", RL_FORCED_PROXY_ADDRESS, RL_FORCED_PRESENCE_ADDRESS);
#endif

#if RSG_NP && !RSG_FINAL
		// this needs set on NP to bypass developer checks (change this to include all platforms)
		if(!PARAM_disabledevspintcheck.Get())
			PARAM_disabledevspintcheck.Set("1");
#endif

		s_IsStagingEnv = RL_FORCE_STAGE_ENVIRONMENT;

#if RL_FORCE_STAGE_ENVIRONMENT
		rlRos::RecreateRosSslContext();
#endif

		s_HasAppliedForceEnvironment = true;
	}

	if(s_HasAppliedForceEnvironment)
		return RL_FORCED_ENVIRONMENT;
#endif

	// rlrosdomainenv is enabled in final so keep that for PC in case it's still needed.
#if RSG_OUTPUT || RL_SC_PLATFORMS
	const rlRosEnvironment rosForcedEnvironment = rlRosGetForcedEnvironment();
	if(rosForcedEnvironment != RLROS_ENV_UNKNOWN)
	{
		const rlEnvironment rosForcedEnvironmentAsRlEnvironment = rlRosEnvironmentToRlEnvironment(rosForcedEnvironment);

		rlDebug("rlCheckAndApplyForcedEnvironment :: Use ROS forced environment: %s", rlGetEnvironmentString(rosForcedEnvironmentAsRlEnvironment));
		return rosForcedEnvironmentAsRlEnvironment;
	}
#endif

	rlDebug("rlCheckAndApplyForcedEnvironment :: Use native environment: %s", rlGetEnvironmentString(nativeEnv));
	return nativeEnv;
}

bool rlIsUsingStagingEnvironment()
{
#if RL_FORCE_ANY_ENVIRONMENT || RSG_OUTPUT
	// PARAM_netUseStagingEnv can probably be removed but keeping it in as fallback for now.
	return s_IsStagingEnv WIN32PC_ONLY(|| PARAM_netUseStagingEnv.Get());
#else
	return false;
#endif
}

bool
rlIsInitialized()
{
    return s_RlInitialized;
}

rlTaskManager* rlGetTaskManager()
{
    return rlVerify(rlIsInitialized()) ? &s_RlTaskMgr : NULL;
}

sysMemAllocator* rlGetAllocator()
{
	return g_rlAllocator;
}

unsigned
rlGetMinAgeRating()
{
#if !__FINAL
	unsigned debugForcedRating;
	if(PARAM_minAgeRating.Get(debugForcedRating))
	{
		return debugForcedRating;
	}
#endif

    return s_MinAgeRating;
}

void
rlSetMinAgeRating(int newAge)
{
	s_MinAgeRating = newAge;
}

void 
rlSetLanguage(sysLanguage language)
{
    if(rlVerify((language > LANGUAGE_UNDEFINED)
        && (language < MAX_LANGUAGES)))
    {
        s_Language = language;

        rlDebug("rline setting language code to %s", rlGetLanguageCode(s_Language));
    }
}

sysLanguage 
rlGetLanguage()
{
    return s_Language;
}

const char* 
rlGetLanguageCode(sysLanguage language)
{
	// do not change the return values
	// Matches Xbox360 XLAST languages.
    if(language == LANGUAGE_ENGLISH)
    {
        return "en-US";
    }
    else if(language == LANGUAGE_SPANISH)
    {
        return "es-ES";
    }
    else if(language == LANGUAGE_FRENCH)
    {
        return "fr-FR";
    }
    else if(language == LANGUAGE_GERMAN)
    {
        return "de-DE";
    }
    else if(language == LANGUAGE_ITALIAN)
    {
        return "it-IT";
    }
    else if(language == LANGUAGE_JAPANESE)
    {
        return "ja-JP";
    }
    else if(language == LANGUAGE_RUSSIAN)
    {
        return "ru-RU";
    }
	else if(language == LANGUAGE_PORTUGUESE)
	{
		return "pt-PT";
	}
	else if(language == LANGUAGE_POLISH)
	{
		return "pl-PL";
	}
	else if(language == LANGUAGE_KOREAN)
	{
		return "ko-KR";
	}
	else if(language == LANGUAGE_CHINESE_TRADITIONAL)
	{
		return "zh-CHT";
	}
	else if(language == LANGUAGE_CHINESE_SIMPLIFIED)
	{
		return "zh-CN";
	}
	else if(language == LANGUAGE_MEXICAN)
	{
		return "es-MX";
	}
	else
    {
        // default to English
        rlAssertf(false, "invalid language");
        return "en-US";
    }
}

static u64 s_PosixMarkTimeMs = 0; //Last time we marked time.
static u64 s_PosixBaseTimeMs = 0; //The time we marked.
static u64 s_ServerPosixTime = 0; //The posix time we get from the server
static u64 s_ServerSyncTime = 0;  //The system time at which we recorded the Server Posix Time

u64 rlGetPosixTime(bool forceSync)
{
    static const u64 REFRESH_INTERVAL_MS  = 60*1000;

    const u64 curTimeMs = sysTimer::GetSystemMsTime() | 0x01;

    //Refresh the posix base time every REFRESH_INTERVAL seconds.
    if((0 == s_PosixMarkTimeMs || (curTimeMs - s_PosixMarkTimeMs >= REFRESH_INTERVAL_MS)) || forceSync)
    {
        u64 posixTimeSec;

#if RSG_DURANGO
        //Difference between the Windows epoch (1601-01-01) and the Posix epoch (1970-01-01).
        static const u64 DELTA_EPOCH_SECONDS = 11644473600UL;

        //Get the time in 100ns units since the Windows epoch.
        FILETIME fileTime;
        GetSystemTimeAsFileTime(&fileTime);

        posixTimeSec = (u64(fileTime.dwHighDateTime) << 32) | u64(fileTime.dwLowDateTime);
        posixTimeSec /= (1000*1000*1000)/100;  //Convert to seconds
        posixTimeSec -= DELTA_EPOCH_SECONDS;   //Convert to Posix epoch

#elif RSG_ORBIS
		//Difference between the NP epoch (0001-01-01) and the Posix epoch (1970-01-01).
		static const u64 DELTA_EPOCH_SECONDS = 62135596800UL;

		SceRtcTick ticks;
		int err = sceRtcGetCurrentNetworkTick(&ticks);

		if(err < 0)
		{
			// use local ticks
			err = sceRtcGetCurrentTick(&ticks);
		}

		if(err >= 0)
		{
			posixTimeSec = ticks.tick;
			posixTimeSec /= (1000*1000);   //Convert to seconds
			posixTimeSec -= DELTA_EPOCH_SECONDS;
		}
		else
		{
			if(s_ServerPosixTime > 0)
			{
				s64 elapsedMs = curTimeMs - s_ServerSyncTime;
				posixTimeSec = s_ServerPosixTime + (elapsedMs / 1000);
			}
			else
			{
				// we haven't received the current posix time from the server yet,
				// so fall back to whatever the user's clock is set to
				posixTimeSec = time(NULL);
			}
		}
#else
		if(s_ServerPosixTime > 0)
		{
			s64 elapsedMs = curTimeMs - s_ServerSyncTime;
			posixTimeSec = s_ServerPosixTime + (elapsedMs / 1000);
		}
		else
		{
			// we haven't received the current posix time from the server yet,
			// so fall back to whatever the user's clock is set to
			posixTimeSec = time(NULL);
		}
#endif

        s_PosixBaseTimeMs = posixTimeSec * 1000;
        s_PosixMarkTimeMs = curTimeMs;
    }

    return (s_PosixBaseTimeMs + (curTimeMs - s_PosixMarkTimeMs))/1000;
}

void rlSetPosixTime(u64 posixTime)
{
	// record the posix time the server sent us, along with the system time at
	// which we recorded the server time. This is used in rlGetPosixTime() above.
	s_ServerPosixTime = posixTime;
	s_ServerSyncTime = sysTimer::GetSystemMsTime();

	// update our cached time
	s_PosixMarkTimeMs = 0;
	rlGetPosixTime();
}

//PURPOSE
//	Returns the size of the system party
u32 rlGetSystemPartySize()
{
#if RSG_DURANGO 
	return g_rlXbl.GetPartyManager()->GetPartySize();
#elif RSG_ORBIS
	return g_rlNp.GetNpParty().GetPartySize();
#else
	rlAssertf(false, "rlGetSystemPartySize not valid on this platform");
	return 0;
#endif
}

static sysCriticalSectionToken s_UuidCs;
static bool s_UuidInitialized = false;
static netRandom s_RlRng;
static u64 s_UuidTime = time(NULL)*1000*1000; //microseconds
static sysTimer s_UuidTimer;
static rlUuid64 s_LastUuid64;
static rlUuid128 s_LastUuid128;
static u64 s_LastUuidTime;
static u8 s_FakeMac[6];
static u8 s_Mac[6];
static u32 s_HashedMac;

static void MakeFakeMac(u8 (&mac)[6])
{
    if(!s_FakeMac[0])
    {
        const unsigned u0 = (unsigned) s_RlRng.GetInt();
        const unsigned u1 = (unsigned) s_RlRng.GetInt();
        s_FakeMac[0] = u8((u0>>24)&0xFF);
        s_FakeMac[1] = u8((u0>>16)&0xFF);
        s_FakeMac[2] = u8((u0>>8)&0xFF);
        s_FakeMac[3] = u8((u0>>0)&0xFF);
        s_FakeMac[4] = u8((u1>>24)&0xFF);
        s_FakeMac[5] = u8((u1>>16)&0xFF);
    }

    mac[0] = s_FakeMac[0];
    mac[1] = s_FakeMac[1];
    mac[2] = s_FakeMac[2];
    mac[3] = s_FakeMac[3];
    mac[4] = s_FakeMac[4];
    mac[5] = s_FakeMac[5];
}

bool
rlCreateUUID(u64 *id)
{
    rlUuid64 uuid64;
    if(rlCreateUUID(&uuid64))
    {
        *id = uuid64.Uuid;
        return true;
    }

    return false;
}

bool rlCreateUUID(rlUuid64* uuid64)
{
    SYS_CS_SYNC(s_UuidCs);

    //Format
    //7-4   : Hashed MAC address
    //3-0   : Time stamp in milliseconds.

    if(!s_UuidInitialized)
    {
        const u64 seed = u64(time(NULL)) + sysTimer::GetTicks();
        s_RlRng.SetFullSeed(seed);
        s_UuidInitialized = true;

        if(!netHardware::GetMacAddress(s_Mac))
        {
            MakeFakeMac(s_Mac);
        }

        s_HashedMac = atDataHash((char*)s_Mac, sizeof(s_Mac));
    }

    s_UuidTime += (u64)s_UuidTimer.GetMsTime();
    s_UuidTimer.Reset();

    if(s_LastUuidTime == s_UuidTime)
    {
        ++s_UuidTime;
    }

    s_LastUuidTime = s_UuidTime;

    uuid64->Uuid = ((u64)s_HashedMac) << 32;
    uuid64->Uuid |= s_UuidTime & 0xFFFFFFFF;

    if(*uuid64 == s_LastUuid64)
    {
        ++uuid64->Uuid;
    }

    s_LastUuid64 = *uuid64;

    return true;
}

bool
rlCreateUUID(rlUuid128* uuid128)
{
    SYS_CS_SYNC(s_UuidCs);

    //Format
    //15-10 : MAC address
    //9-8   : Random
    //7-0   : Time stamp in microseconds.

    if(!s_UuidInitialized)
    {
        const u64 seed = u64(time(NULL)) + sysTimer::GetTicks();
        s_RlRng.SetFullSeed(seed);
        s_UuidInitialized = true;

        if(!netHardware::GetMacAddress(s_Mac))
        {
            MakeFakeMac(s_Mac);
        }

        s_HashedMac = atDataHash((char*)s_Mac, sizeof(s_Mac));
    }

    uuid128->Uuid[0] = (u64(s_Mac[0]) << 56)
                        | (u64(s_Mac[1]) << 48)
                        | (u64(s_Mac[2]) << 40)
                        | (u64(s_Mac[3]) << 32)
                        | (u64(s_Mac[4]) << 24)
                        | (u64(s_Mac[5]) << 16);

    s_UuidTime += (u64)s_UuidTimer.GetUsTime();
    s_UuidTimer.Reset();

    if(s_LastUuidTime == s_UuidTime)
    {
        ++s_UuidTime;
    }

    s_LastUuidTime = s_UuidTime;

    uuid128->Uuid[0] |= u64(s_RlRng.GetInt() & 0xFFFF);
    uuid128->Uuid[1] = s_UuidTime;

    if(*uuid128 == s_LastUuid128)
    {
        //Add one to the low order 64 bits and carry the one
        //if necessary.
        const u64 hibit = uuid128->Uuid[1] >> 63;
        ++uuid128->Uuid[1];
        if((uuid128->Uuid[1] >> 63) != hibit)
        {
            //Carry the one.
            ++uuid128->Uuid[0];
        }
    }

    s_LastUuid128 = *uuid128;

    return true;
}

void* 
#if RAGE_TRACKING
rlLoggedAllocate(const size_t size, const char* fileName, int lineNumber, const char* trackName)
#else
rlLoggedAllocate(const size_t size, const char* fileName, int lineNumber, const char*)
#endif
{
    if(!g_rlAllocator)
    {
        rlError("g_rlAllocator is null, possibly shutdown? can't allocate");
        return 0;
    }

    RAGE_TRACK(rline);
    RAGE_TRACK_NAME(trackName);

    return g_rlAllocator->TryLoggedAllocate(size, 0, 0, fileName, lineNumber);
}

size_t
rlGetAllocSize(const void* ptr)
{
    if(!g_rlAllocator)
    {
        rlError("g_rlAllocator is null, possibly shutdown? can't get alloc size");
        return 0;
    }

    return g_rlAllocator->GetSize(ptr);
}

void
rlFree(const void* ptr)
{
    if(ptr)
    {
        if(!g_rlAllocator)
        {
            rlError("g_rlAllocator is null, possibly shutdown? can't free");
            return;
        }

        rlAssert(g_rlAllocator->IsValidPointer(ptr));
        g_rlAllocator->Free(ptr);
    }
}

void
rlEnvironmentInit()
{
#if RSG_OUTPUT
    const char* env = nullptr;

    if(PARAM_rlEnvironment.Get(env) && env != nullptr && env[0] != '\0')
    {
        if(StringIsEqual(env, "dev"))
        {
			const char* rosEnv = rlRosGetEnvironmentAsString(RLROS_ENV_DEV);
			PARAM_rlrosdomainenv.Set(rosEnv);
			rlDebug("rlEnvironmentInit :: Setting environment to %s", env);

			rlCheckHostsFile(rosEnv);
        }
        else if(StringIsEqual(env, "cert"))
        {
			const char* rosEnv = rlRosGetEnvironmentAsString(RLROS_ENV_CERT);
			PARAM_rlrosdomainenv.Set(rosEnv);
			NP_ONLY(PARAM_disabledevspintcheck.Set("1"));
			rlDebug("rlEnvironmentInit :: Setting environment to %s", env);

			rlCheckHostsFile(rosEnv);
        }
        else if(StringIsEqual(env, "prod"))
        {
			const char* rosEnv = rlRosGetEnvironmentAsString(RLROS_ENV_PROD);
			PARAM_rlrosdomainenv.Set(rosEnv);
			NP_ONLY(PARAM_disabledevspintcheck.Set("1"));
			rlDebug("rlEnvironmentInit :: Setting environment to %s", env);

			rlCheckHostsFile(rosEnv);
        }
        else if(StringIsEqual(env, "stage-dev"))
        {
            const char* rosEnv = rlRosGetEnvironmentAsString(RLROS_ENV_DEV);
            PARAM_rlrosdomainenv.Set(rosEnv);
			rlDebug("rlEnvironmentInit :: Setting environment to %s", env);

            rlStagingEnvironmentInit(env, rosEnv, RL_STAGE_DEV_PROXY_ADDRESS, RL_STAGE_DEV_PRESENCE_ADDRESS, RL_STAGE_DEV_RELAY_ADDRESS);
        }
        else if(StringIsEqual(env, "stage-cert"))
        {
            const char* rosEnv = rlRosGetEnvironmentAsString(RLROS_ENV_CERT);
            PARAM_rlrosdomainenv.Set(rosEnv);
			rlDebug("rlEnvironmentInit :: Setting environment to %s", env);

            rlStagingEnvironmentInit(env, rosEnv, RL_STAGE_CERT_PROXY_ADDRESS, RL_STAGE_CERT_PRESENCE_ADDRESS, RL_STAGE_CERT_RELAY_ADDRESS);
        }
        else if(StringIsEqual(env, "stage-prod"))
        {
            const char* rosEnv = rlRosGetEnvironmentAsString(RLROS_ENV_PROD);
            PARAM_rlrosdomainenv.Set(rosEnv);
			rlDebug("rlEnvironmentInit :: Setting environment to %s", env);

            rlStagingEnvironmentInit(env, rosEnv, RL_STAGE_PROD_PROXY_ADDRESS, RL_STAGE_PROD_PRESENCE_ADDRESS, RL_STAGE_PROD_RELAY_ADDRESS);
        }
        else
        {
            rlAssertf(false, "Unsupported rlEnvironment %s", env);
        }
    }
    else
    {
        // Check if the proxy was set manually and set the staging bool
        const char* proxy = nullptr;
        PARAM_nethttpproxy.Get(proxy);

        s_IsStagingEnv = (proxy != nullptr) && (StringIsEqual(proxy, RL_STAGE_DEV_PROXY_ADDRESS) || StringIsEqual(proxy, RL_STAGE_CERT_PROXY_ADDRESS) || StringIsEqual(proxy, RL_STAGE_PROD_PROXY_ADDRESS));
    }
#endif // RSG_OUTPUT

#if RL_SC_PLATFORMS
	// for PC, we don't have a native environment in the same way as consoles (might change with RGL in future) 
	// just set the native to the default here and check for overrides 
	s_NativeEnvironment = rlRosEnvironmentToRlEnvironment(RL_DEFAULT_ROS_ENVIRONMENT);
	s_RosEnvironment = rlCheckAndApplyForcedEnvironment(s_NativeEnvironment);
	rlDebug("rlEnvironmentInit :: Native: %s, Ros: %s", rlGetEnvironmentString(s_NativeEnvironment), rlGetEnvironmentString(s_RosEnvironment));
#endif
}

void
#if RSG_PC
rlStagingEnvironmentInit(const char* OUTPUT_ONLY(stagingEnv), const char* OUTPUT_ONLY(rosEnv), const char* /*proxy*/, const char* /*presence*/, const char* /*relay*/)
#else
rlStagingEnvironmentInit(const char* OUTPUT_ONLY(stagingEnv), const char* /*rosEnv*/, const char* OUTPUT_ONLY(proxy), const char* OUTPUT_ONLY(presence), const char* OUTPUT_ONLY(relay))
#endif
{
#if RSG_OUTPUT
	s_IsStagingEnv = true;
    PARAM_scAuthStagingEnv.Set(stagingEnv);

#if RSG_PC
	rlCheckHostsFile(rosEnv);
#else
    PARAM_nethttpproxy.Set(proxy);
    PARAM_netpresenceserveraddr.Set(presence);
    PARAM_netrelayserveraddr.Set(relay);
    rlDebug("\tProxy: %s, Presence: %s, Relay: %s", proxy, presence, relay);
#endif
    PARAM_netpresencesecure.Set("true");
    PARAM_rlsslnocert.Set("1");

#if RSG_PC
    PARAM_netUseStagingEnv.Set("true");
#endif

#if RSG_NP
    PARAM_disabledevspintcheck.Set("1");
#endif

    if(PARAM_fiddler.Get() || PARAM_fiddlerwebsocket.Get())
    {
        rlDebug("rlStagingEnvironmentInit :: Disabled Fiddler due to staging environment");
    }

    // Staging environments use proxies so fiddler can't be used at the same time
    PARAM_fiddler.Set(nullptr);
    PARAM_fiddlerwebsocket.Set(nullptr);
#endif
}

bool
rlCheckHostsFile(const char* WIN32PC_ONLY(OUTPUT_ONLY(env)))
{
#if RSG_PC && RSG_OUTPUT
    bool allOk = false;

    //There are more elegant and more accurate ways to check this but this should do for now.
    const char* filePath = "C:\\Windows\\System32\\drivers\\etc\\hosts";

    const fiDevice* device = nullptr;
    fiHandle handle = fiHandleInvalid;
    char* buffer = nullptr;

    char addr1[RAGE_MAX_PATH] = { 0 };
    formatf(addr1, "%s.ros.rockstargames.com", env);

    char addr2[RAGE_MAX_PATH] = { 0 };
    formatf(addr2, "auth-%s.ros.rockstargames.com", env);

    rtry
    {
        device = fiDevice::GetDevice(filePath, true);
        rcheck(device, catchall, rlWarning("No device for %s", filePath));

        handle = device->Open(filePath, true);
        rcheck(fiIsValidHandle(handle), catchall, rlWarning("Failed to open '%s' to check for the staging addresses", filePath));

        u64 len = device->GetFileSize(filePath);
        buffer = rage_new char[len + 2];
        buffer[len] = '\n';
        buffer[len+1] = 0;

        int num = device->Read(handle, buffer, (int)len);
        rcheck(num == (int)len, catchall, rlWarning("Failed to read data for '%s' to check for the staging addresses", filePath));

        char* currentLine = buffer;
        char* pos = buffer;

        bool addr1Found = false;
        bool addr2Found = false;

        while (*pos != '\0' && (addr1Found == false || addr2Found == false))
        {
            if(*pos == '\n' || *pos == '\r')
            {
                *pos = 0;

                while (*currentLine == ' ' || *currentLine == '\t')
                {
                    ++currentLine;
                }

                if(*currentLine != '#' && *currentLine != '\0')
                {
                    const char* comment = strstr(currentLine, "#");

                    if(!addr1Found)
                    {
                        const char* a1 = strstr(currentLine, addr1);
                        addr1Found = a1 != nullptr && (comment == nullptr || comment > a1);
                    }

                    if(!addr2Found)
                    {
                        const char* a2 = strstr(currentLine, addr2);
                        addr2Found = a2 != nullptr && (comment == nullptr || comment > a2);
                    }
                }

                currentLine = pos + 1;
            }

            ++pos;
        }

		// if we are using a staging environment, we expect these to be here
		if(rlIsUsingStagingEnvironment())
		{
			rcheck(addr1Found, catchall, rlWarning("rlCheckHostsFile :: Expected hosts path %s not found", addr1));
			rcheck(addr2Found, catchall, rlWarning("rlCheckHostsFile :: Expected hosts path %s not found", addr2));
		}
		// otherwise, we don't
		else
		{
			rcheck(!addr1Found, catchall, rlWarning("rlCheckHostsFile :: Unexpected hosts path %s found!", addr1));
			rcheck(!addr2Found, catchall, rlWarning("rlCheckHostsFile :: Unexpected hosts path %s found!", addr2));
		}

        allOk = true;
    }
    rcatchall
    {
    }

    if(device != nullptr && fiIsValidHandle(handle))
    {
        device->Close(handle);
    }

    if(buffer != nullptr)
    {
        delete[] buffer;
        buffer = nullptr;
    }

    return allOk;
#else //RSG_PC && RSG_OUTPUT
    return true;
#endif
}

void
rlFiddlerInit()
{
#if !RSG_FINAL
    if(PARAM_fiddlerwebsocket.Get())
    {
        PARAM_fiddler.Set("1");
    }

    if(PARAM_fiddler.Get())
    {
        netIpAddress ipAddr;
        ipAddr.FromString(RSG_PC ? fiDeviceTcpIp::GetLocalIpAddrName() : fiDeviceTcpIp::GetLocalHost());

#if RSG_PC
        if(!ipAddr.IsValid())
        {
            // use localhost if we couldn't get the IP using the above method
            ipAddr = netIpV4Address::GetLoopbackAddress();
        }
#endif

        const u16 FIDDLER_DEFAULT_PORT = 8888;
        netSocketAddress proxyAddr(ipAddr, FIDDLER_DEFAULT_PORT);

        char proxyStr[netSocketAddress::MAX_STRING_BUF_SIZE];
        proxyAddr.Format(proxyStr);

        rlDebug3("-Fiddler enabled, setting -nethttpproxy=%s -nethttpstunnel -rlsslnocert", proxyStr);

        // Fiddler generates a unique self-signed cert and adds it to the Windows list of trusted root certs.
        // However, this cert is not trusted by the game (we don't use the Windows cert list). So we disable
        // certificate verification if we're using Fiddler. If you wanted to test cert verification while using
        // Fiddler's HTTPS decryption, you would need to import Fiddler's cert (eg. via a path on the command line)
        // and add it as a trusted cert in rlRos.cpp where it sets up the SSL context.
        if(!PARAM_rlsslnocert.Get())
        {
            PARAM_rlsslnocert.Set("1");
        }

        if(!PARAM_nethttpstunnel.Get())
        {
            PARAM_nethttpstunnel.Set("1");
        }

#if RSG_XBOX
        // by default, we use Microsoft's native HTTP lib on Xbox One (IXHR2). You can set up IXHR2 to proxy
        // through Fiddler which is the preferred method. However, if you want to use Fiddler without that
        // set up process on Xbox One, we need to disable IXHR2 and use our own HTTP lib instead.
        if(!PARAM_nethttpnoxhttp.Get())
        {
            PARAM_nethttpnoxhttp.Set("1");
        }
#endif

        if(!PARAM_nethttpproxy.Get())
        {
            // We called InitProxies earlier on so here we can set the wildcard
            // to the fiddler address without risking to overwrite other
            // proxy destination.
            ENABLE_TCP_PROXY_ARGS_ONLY(netHttpProxy::AddProxyAddress("*", proxyStr));
        }
    }
#endif
}

#if RSG_SCE

const char* rlNpGetErrorString(const int OUTPUT_ONLY(err))
{
#if !__NO_OUTPUT
#define SCERR_CASE(a) case a: return #a;

    switch(err)
    {

		SCERR_CASE(SCE_NP_ERROR_ALREADY_INITIALIZED)
		SCERR_CASE(SCE_NP_ERROR_NOT_INITIALIZED)
		SCERR_CASE(SCE_NP_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_ERROR_UNKNOWN_PLATFORM_TYPE)
		SCERR_CASE(SCE_NP_ERROR_OUT_OF_MEMORY)
		SCERR_CASE(SCE_NP_ERROR_SIGNED_OUT)
		SCERR_CASE(SCE_NP_ERROR_USER_NOT_FOUND)
		SCERR_CASE(SCE_NP_ERROR_CALLBACK_ALREADY_REGISTERED)
		SCERR_CASE(SCE_NP_ERROR_CALLBACK_NOT_REGISTERED)
		SCERR_CASE(SCE_NP_UTIL_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_UTIL_ERROR_INSUFFICIENT)
		SCERR_CASE(SCE_NP_UTIL_ERROR_PARSER_FAILED)
		SCERR_CASE(SCE_NP_UTIL_ERROR_INVALID_PROTOCOL_ID)
		SCERR_CASE(SCE_NP_UTIL_ERROR_INVALID_NP_ID)
		SCERR_CASE(SCE_NP_UTIL_ERROR_INVALID_NP_ENV)
		SCERR_CASE(SCE_NP_UTIL_ERROR_INVALID_CHARACTER)
		SCERR_CASE(SCE_NP_UTIL_ERROR_NOT_MATCH)
		SCERR_CASE(SCE_NP_UTIL_ERROR_INVALID_TITLEID)
		SCERR_CASE(SCE_NP_UTIL_ERROR_UNKNOWN)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_ALREADY_INITIALIZED)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_NOT_INITIALIZED)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_OUT_OF_MEMORY)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_NO_LOGIN)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_TOO_MANY_OBJECTS)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_ABORTED)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_BAD_RESPONSE)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_BODY_TOO_LARGE)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_HTTP_SERVER)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INVALID_SIGNATURE)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INSUFFICIENT_ARGUMENT)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_UNKNOWN_TYPE)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INVALID_ID)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INVALID_ONLINE_ID)
#if SCE_ORBIS_SDK_VERSION < (0x01020010u)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_CONNECTION_HANDLE_ALREADY_EXISTS)
#endif
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INVALID_TYPE)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_TRANSACTION_ALREADY_END)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INVALID_PARTITION)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_INVALID_ALIGNMENT)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_CLIENT_HANDLE_ALREADY_EXISTS)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_NO_RESOURCE)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_REQUEST_BEFORE_END)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_TOO_MANY_SLOTID)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_TOO_MANY_NPID)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_SCORE_INVALID_SAVEDATA_OWNER)
		SCERR_CASE(SCE_NP_COMMUNITY_ERROR_TUS_INVALID_SAVEDATA_OWNER)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_BAD_REQUEST)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_TICKET)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_SIGNATURE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_NPID)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_FORBIDDEN)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INTERNAL_SERVER_ERROR)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_VERSION_NOT_SUPPORTED)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_SERVICE_UNAVAILABLE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_PLAYER_BANNED)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_CENSORED)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_RECORD_FORBIDDEN)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_USER_PROFILE_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UPLOADER_DATA_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_QUOTA_MASTER_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_TITLE_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_BLACKLISTED_USER_ID)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_GAME_RANKING_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_STORE_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_NOT_BEST_SCORE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_LATEST_UPDATE_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_BOARD_MASTER_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_GAME_DATA_MASTER_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_ANTICHEAT_DATA)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TOO_LARGE_DATA)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_NO_SUCH_USER_NPID)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_ENVIRONMENT)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_ONLINE_NAME_CHARACTER)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_ONLINE_NAME_LENGTH)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_ABOUT_ME_CHARACTER)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_ABOUT_ME_LENGTH)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_SCORE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_OVER_THE_RANKING_LIMIT)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_FAIL_TO_CREATE_SIGNATURE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_MASTER_INFO_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_OVER_THE_GAME_DATA_LIMIT)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_SELF_DATA_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_USER_NOT_ASSIGNED)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_GAME_DATA_ALREADY_EXISTS)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TOO_MANY_RESULTS)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_NOT_RECORDABLE_VERSION)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_USER_STORAGE_TITLE_MASTER_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_INVALID_VIRTUAL_USER)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_USER_STORAGE_DATA_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_NON_PLUS_MEMBER)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UNMATCH_SEQUENCE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_SAVEDATA_NOT_FOUND)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TOO_MANY_SAVEDATA_FILES)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TOO_MUCH_TOTAL_SAVEDATA_SIZE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_NOT_YET_DOWNLOADABLE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_BLACKLISTED_TITLE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TOO_LARGE_ICONDATA)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RECORD_DATE_IS_NEWER_THAN_COMP_DATE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TOO_LARGE_SAVEDATA)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UNMATCH_SIGNATURE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UNMATCH_MD5SUM)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TOO_MUCH_SAVEDATA_SIZE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_CONDITIONS_NOT_SATISFIED)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UNSUPPORTED_PLATFORM)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_MATCHING_BEFORE_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_MATCHING_END_OF_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_MATCHING_MAINTENANCE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_BEFORE_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_END_OF_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_RANKING_MAINTENANCE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_NO_SUCH_TITLE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TITLE_USER_STORAGE_BEFORE_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TITLE_USER_STORAGE_END_OF_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_TITLE_USER_STORAGE_MAINTENANCE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_FSR_BEFORE_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_FSR_END_OF_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_FSR_MAINTENANCE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UBS_BEFORE_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UBS_END_OF_SERVICE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UBS_MAINTENANCE)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_BASIC_BLACKLISTED_USER_ID)
		SCERR_CASE(SCE_NP_COMMUNITY_SERVER_ERROR_UNSPECIFIED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_OUT_OF_MEMORY)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_ALREADY_INITIALIZED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_NOT_INITIALIZED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CONTEXT_MAX)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CONTEXT_ALREADY_EXISTS)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CONTEXT_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CONTEXT_ALREADY_STARTED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CONTEXT_NOT_STARTED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SERVER_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_CONTEXT_ID)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_SERVER_ID)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_WORLD_ID)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_LOBBY_ID)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_ROOM_ID)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_MEMBER_ID)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_ATTRIBUTE_ID)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_CASTTYPE)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_SORT_METHOD)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_MAX_SLOT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_OPT_SIZE)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_MATCHING_SPACE)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_BLOCK_KICK_FLAG)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_MESSAGE_TARGET)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_RANGE_FILTER_MAX)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INSUFFICIENT_BUFFER)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_DESTINATION_DISAPPEARED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_REQUEST_TIMEOUT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_ALIGNMENT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CONNECTION_CLOSED_BY_SERVER)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SSL_VERIFY_FAILED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SSL_HANDSHAKE)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SSL_SEND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SSL_RECV)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_JOINED_SESSION_MAX)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_ALREADY_JOINED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_SESSION_TYPE)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_NP_SIGNED_OUT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_BUSY)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SERVER_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_NOT_ALLOWED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_ABORTED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_REQUEST_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SESSION_DESTROYED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CONTEXT_STOPPED)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_REQUEST_PARAMETER)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_NOT_NP_SIGN_IN)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_ROOM_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_ROOM_MEMBER_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_LOBBY_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_LOBBY_MEMBER_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_KEEPALIVE_TIMEOUT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_TIMEOUT_TOO_SHORT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_TIMEDOUT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_SLOTGROUP)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_INVALID_ATTRIBUTE_SIZE)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_CANNOT_ABORT)
		SCERR_CASE(SCE_NP_MATCHING2_ERROR_SESSION_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_BAD_REQUEST)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_SERVICE_UNAVAILABLE)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_BUSY)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_END_OF_SERVICE)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_INTERNAL_SERVER_ERROR)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_PLAYER_BANNED)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_FORBIDDEN)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_BLOCKED)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_UNSUPPORTED_NP_ENV)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_INVALID_TICKET)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_INVALID_SIGNATURE)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_EXPIRED_TICKET)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_ENTITLEMENT_REQUIRED)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_CONTEXT)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_CLOSED)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_TITLE)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_WORLD)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_LOBBY)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_ROOM)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_LOBBY_INSTANCE)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_ROOM_INSTANCE)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_PASSWORD_MISMATCH)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_LOBBY_FULL)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_ROOM_FULL)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_GROUP_FULL)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_USER)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_GROUP_PASSWORD_MISMATCH)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_TITLE_PASSPHRASE_MISMATCH)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_LOBBY_ALREADY_EXIST)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_ROOM_ALREADY_EXIST)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_CONSOLE_BANNED)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_ROOMGROUP)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_SUCH_GROUP)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NO_PASSWORD)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_INVALID_GROUP_SLOT_NUM)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_INVALID_PASSWORD_SLOT_MASK)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_DUPLICATE_GROUP_LABEL)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_REQUEST_OVERFLOW)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_ALREADY_JOINED)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_NAT_TYPE_MISMATCH)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_ROOM_INCONSISTENCY)
		SCERR_CASE(SCE_NP_MATCHING2_SERVER_ERROR_BLOCKED_USER_IN_ROOM)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_NOT_INITIALIZED)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_ALREADY_INITIALIZED)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_OUT_OF_MEMORY)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_CTXID_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_CTX_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_REQID_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_REQ_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_PARSER_CREATE_FAILED)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_PARSER_FAILED)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_INVALID_NAMESPACE)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_NETINFO_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_PEER_NOT_RESPONDING)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_CONNID_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_CONN_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_PEER_UNREACHABLE)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_TERMINATED_BY_PEER)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_TIMEOUT)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_CTX_MAX)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_RESULT_NOT_FOUND)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_CONN_IN_PROGRESS)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_OWN_NP_ID)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_TOO_MANY_CONN)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_TERMINATED_BY_MYSELF)
		SCERR_CASE(SCE_NP_MATCHING2_SIGNALING_ERROR_MATCHING2_PEER_NOT_FOUND)
		SCERR_CASE(SCE_NP_BANDWIDTH_TEST_ERROR_NOT_INITIALIZED)
		SCERR_CASE(SCE_NP_BANDWIDTH_TEST_ERROR_BAD_RESPONSE)
		SCERR_CASE(SCE_NP_BANDWIDTH_TEST_ERROR_OUT_OF_MEMORY)
		SCERR_CASE(SCE_NP_BANDWIDTH_TEST_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_BANDWIDTH_TEST_ERROR_INVALID_SIZE)
		SCERR_CASE(SCE_NP_BANDWIDTH_TEST_ERROR_CONTEXT_NOT_AVAILABLE)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_UNKNOWN)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_ALREADY_INITIALIZED)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_NOT_INITIALIZED)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_INVALID_ARGUMENT)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_OUT_OF_MEMORY)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_NOT_IN_PARTY)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_VOICE_NOT_ENABLED)
		// SCERR_CASE(SCE_NP_PARTY_ERROR_PARTY_MEMBER_NOT_FOUND)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_NOT_INITIALIZED)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_ALREADY_INITIALIZED)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_OUT_OF_MEMORY)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_CTXID_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_CTX_NOT_FOUND)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_REQID_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_REQ_NOT_FOUND)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_PARSER_CREATE_FAILED)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_PARSER_FAILED)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_INVALID_NAMESPACE)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_NETINFO_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_PEER_NOT_RESPONDING)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_CONNID_NOT_AVAILABLE)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_CONN_NOT_FOUND)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_PEER_UNREACHABLE)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_TERMINATED_BY_PEER)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_TIMEOUT)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_CTX_MAX)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_RESULT_NOT_FOUND)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_CONN_IN_PROGRESS)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_OWN_NP_ID)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_TOO_MANY_CONN)
		SCERR_CASE(SCE_NP_SIGNALING_ERROR_TERMINATED_BY_MYSELF)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_OUT_OF_MEMORY)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_INVALID_ARGUMENT)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_INVALID_LIB_CONTEXT_ID)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_LIB_CONTEXT_NOT_FOUND)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_USER_CONTEXT_NOT_FOUND)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_REQUEST_NOT_FOUND)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_NOT_SIGNED_IN)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_INVALID_CONTENT_PARAMETER)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_ABORTED)
		SCERR_CASE(SCE_NP_WEBAPI_ERROR_USER_CONTEXT_ALREADY_EXIST)
	}
#endif  //__NO_OUTPUT

    return "Unknown NP error";
}

#endif

}   //namespace rage
