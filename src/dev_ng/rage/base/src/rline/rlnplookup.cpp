// 
// rline/rlnplookup.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
 
#if RSG_NP && 0

#include "rlnplookup.h"
#include "diag/seh.h"
#include "rlnp.h"
#include "rlnptitleid.h"
#include "system/nelem.h"

#include <np.h>

#if RSG_ORBIS
#include <sdk_version.h>
#include <libsysmodule.h>

#pragma comment(lib, "libSceNpUtility_stub_weak.a.")
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline_np, lookup)
#undef __rage_channel
#define __rage_channel rline_np_lookup

//------------------------------------------------------------------------------
//struct rlNpLookup::LookupOp
//------------------------------------------------------------------------------
rlNpLookup::LookupTx::LookupTx()
: m_TransCtxId(-1)
, m_TxSlot(-1)
, m_Owner(NULL)
, m_Status(NULL)
{
}

rlNpLookup::LookupTx::~LookupTx()
{
    this->Reset();
}

void 
rlNpLookup::LookupTx::Reset()
{
    if(m_Status)
    {
        if(m_Status->Pending())
        {
            rlWarning("Reset() while operation in progress");
            this->Finished(false);
            m_Status->SetCanceled();
        }

        m_Status = NULL;
    }

    if(m_TransCtxId)
    {
		sceNpLookupDeleteRequest(m_TransCtxId);
        m_TransCtxId = -1;
    }

    if(m_Owner)
    {
        rlAssert(m_TxSlot >= 0);
        m_Owner->m_TxSlots[m_TxSlot] = NULL;
        m_Owner = NULL;
        m_TxSlot = -1;
    }
}

bool 
rlNpLookup::LookupTx::IsPending()
{
    return m_Status && m_Status->Pending();
}

void
rlNpLookup::LookupTx::Update()
{
	if (this->IsPending())
	{
		int result;
		int err = sceNpLookupPollAsync(m_TransCtxId, &result);

		if (err != SCE_NP_LOOKUP_POLL_ASYNC_RET_RUNNING)
		{
			if (err == SCE_NP_LOOKUP_POLL_ASYNC_RET_FINISHED)
			{
				rlDebug("Lookup operation finished");
                this->Finished(true);
                m_Status->SetSucceeded();
			}
			if (err < 0)
			{
                rlNpError("Lookup operation failed", err);
                this->Finished(false);
                m_Status->SetFailed();
			}

            this->Reset();
		}
	}
}

//------------------------------------------------------------------------------
//struct rlNpLookup::TsDownload
//------------------------------------------------------------------------------
rlNpLookup::TsDownload::TsDownload()
: m_DownloadSizePtr(0)
{
}

rlNpLookup::TsDownload::~TsDownload()
{
    this->Reset();
}

void 
rlNpLookup::TsDownload::Reset()
{
    this->LookupTx::Reset();

    m_DownloadSizePtr = NULL;
}

void
rlNpLookup::TsDownload::Finished(const bool succeeded)
{
    if(succeeded)
    {
        rlDebug("Download succeeded (%u bytes)", m_DownloadSize);
        *m_DownloadSizePtr = m_DownloadSize;
    }
    else
    {
        rlDebug("Download failed");
    }
}

//------------------------------------------------------------------------------
//struct rlNpLookup::NpIdLookup
//------------------------------------------------------------------------------
rlNpLookup::NpIdLookup::NpIdLookup()
: m_NpIdPtr(0)
{
}

rlNpLookup::NpIdLookup::~NpIdLookup()
{
    this->Reset();
}

void 
rlNpLookup::NpIdLookup::Reset()
{
    this->LookupTx::Reset();

    m_NpIdPtr = NULL;
}

void
rlNpLookup::NpIdLookup::Finished(const bool succeeded)
{
    if(succeeded)
    {
        rlDebug("NP ID lookup for \"%s\" succeeded",
                m_NpOnlineId.data);

        *m_NpIdPtr = m_NpId.AsSceNpId();
    }
    else
    {
        rlDebug("NP ID lookup for \"%s\" failed",
                m_NpOnlineId.data);
    }
}

//------------------------------------------------------------------------------
//class rlNpLookup
//------------------------------------------------------------------------------
rlNpLookup::rlNpLookup()
: m_Initialized(false)
, m_TitleCtxId(-1)
{
    CompileTimeAssert(COUNTOF(m_TxSlots) <= SCE_NP_LOOKUP_MAX_CTX_NUM);

    sysMemSet(m_TxSlots, 0, sizeof(m_TxSlots));
}

rlNpLookup::~rlNpLookup()
{
    Shutdown();
}

bool 
rlNpLookup::IsInitialized() const
{
    return m_Initialized;
}

#if RSG_ORBIS
void rlNpLookup::InitLibs()
{
	if (sceSysmoduleLoadModule(SCE_SYSMODULE_NP_UTILITY) != SCE_OK) 
	{
		Quitf("Failed to load prx SCE_SYSMODULE_NP_UTILITY");
	}
}
#endif

bool
rlNpLookup::Init()
{
    rlDebug("rlNpLookup::Init()");

    if(rlVerify(!m_Initialized))
    {
        rlAssert(-1 == m_TitleCtxId);

#if __ASSERT
        for(int i = 0; i < COUNTOF(m_TxSlots); ++i)
        {
            rlAssert(!m_TxSlots[i]);
        }
#endif  //__ASSERT

        g_rlNp.AddDelegate(&m_NpDlgt);
        m_NpDlgt.Bind(this, &rlNpLookup::OnNpEvent);

		m_Initialized = true;
	}

	return m_Initialized;
}

void 
rlNpLookup::Shutdown()
{
    rlDebug("rlNp::Shutdown()");

	if (m_Initialized)
	{
        this->ResetAll();

		g_rlNp.RemoveDelegate(&m_NpDlgt);

        if(m_TitleCtxId >= 0)
        {
            sceNpLookupDeleteTitleCtx(m_TitleCtxId);
            m_TitleCtxId = -1;
        }

        m_Initialized = false;
	}
}

void
rlNpLookup::Update()
{
    rlAssert(m_Initialized);

    for(int i = 0; i < COUNTOF(m_TxSlots); ++i)
    {
        //Update() can cause the slot to be cleared,
        //so grab a reference to the transaction before calling Update().
        LookupTx* tx = m_TxSlots[i];

        if(tx)
        {
            rlAssert(tx->IsPending());

            tx->Update();

            if(!tx->IsPending())
            {
                RL_DELETE(tx);
                rlAssert(!m_TxSlots[i]);
            }
        }
    }
}

bool
rlNpLookup::CanStartOperation() const
{
    bool canstart = false;

    for(int i = 0; i < COUNTOF(m_TxSlots); ++i)
    {
        if(!m_TxSlots[i])
        {
            canstart = true;
            break;
        }
    }

    return canstart;
}

bool
rlNpLookup::LookupNpId(const SceNpOnlineId& npOnlineId,
                        SceNpId* npId,
                        const unsigned timeoutMs,
                        netStatus* status)
{
    rlAssert(npId && status && !status->Pending());

    rlDebug("Looking up NP ID for:\"%s\"...", npOnlineId.data);

    bool success = false;

    NpIdLookup* tx = NULL;

    rtry
    {
        tx = RL_NEW(rlNpLookup, NpIdLookup);

        rverify(tx,catchall,rlError("Could not allocate NpIdLookup"));

        rcheck(this->StartTransaction(tx, timeoutMs, status),
                catchall,
                rlError("Failed to start transaction"));

        int err = sceNpLookupNpId(tx->m_TransCtxId, &npOnlineId, &tx->m_NpId.AsSceNpId(), NULL);

        rcheck(err >= 0, catchall, rlNpError("sceNpLookupNpId failed", err));

        tx->m_NpOnlineId.Reset(npOnlineId);
        tx->m_NpIdPtr = npId;

        success = true;
	}
	rcatchall
	{
        if(tx)
        {
            RL_DELETE(tx);
        }

        status->ForceFailed();
	}

    return success;
}

void
rlNpLookup::Cancel(const netStatus* status)
{
    for(int i = 0; i < COUNTOF(m_TxSlots); ++i)
    {
        if(m_TxSlots[i] && status == m_TxSlots[i]->m_Status)
        {
            RL_DELETE(m_TxSlots[i]);
            netAssert(!m_TxSlots[i]);
            break;
        }
    }
}

//private:

bool 
rlNpLookup::StartTransaction(LookupTx* tx,
                            const unsigned timeoutMs,
                            netStatus* status)
{
    bool success = false;

	rtry
    {
        rcheck(m_Initialized, catchall, rlError("NP lookup not initialized yet; cannot download title storage file"));

        rcheck(!tx->IsPending(), catchall, rlError("Operation already in progress"));

        if(m_TitleCtxId < 0)
        {
            netAssert(-1 == m_TitleCtxId);

            const SceNpId* npId = g_rlNp.GetNpId(g_rlNp.GetPrimaryUserServiceIndex());

            rcheck(npId, catchall, rlError("Can't start NP Lookup transaction - no users are logged in"));

            m_TitleCtxId = sceNpLookupCreateTitleCtx(npId);

            if(m_TitleCtxId < 0)
            {
                OUTPUT_ONLY(const int err = m_TitleCtxId;)
                m_TitleCtxId = -1;
                rthrow(catchall, rlNpError("Error calling sceNpLookupCreateTitleCtx()", err));
            }
		}

        rcheck(m_TitleCtxId >= 0, catchall, rlError("Invalid title context id"));

        int slotIdx = -1;

        for(int i = 0; i < COUNTOF(m_TxSlots); ++i)
        {
            if(NULL == m_TxSlots[i])
            {
                slotIdx = i;
                break;
            }
        }

        rverify(slotIdx >= 0, catchall, rlError("No available transaction slots"));

		memset(&tx->m_ReqParam, 0, sizeof(tx->m_ReqParam));
		tx->m_ReqParam.size = sizeof(tx->m_ReqParam);
		tx->m_ReqParam.threadPriority = SCE_KERNEL_PRIO_FIFO_DEFAULT;
		tx->m_ReqParam.cpuAffinityMask = SCE_KERNEL_CPUMASK_USER_ALL;

		tx->m_TransCtxId = sceNpLookupCreateAsyncRequest (m_TitleCtxId, &tx->m_ReqParam);
		rverify(tx->m_TransCtxId >= 0, catchall, rlNpError("sceNpLookupCreateAsyncRequest failed", tx->m_TransCtxId));

        unsigned timeoutUs = timeoutMs * 1000; //Timeout is in microseconds.

        const unsigned MIN_TIMEOUT_US = 10 * 1000 * 1000;
        if(timeoutUs < MIN_TIMEOUT_US)
        {
            timeoutUs = MIN_TIMEOUT_US;
        }

        int err = sceNpLookupSetTimeout(tx->m_TransCtxId, SCE_NP_LOOKUP_TIMEOUT_NO_EFFECT, timeoutUs, timeoutUs, timeoutUs, timeoutUs);
        rcheck(0 == err, catchall, rlNpError("sceNpLookupSetTimeout failed", err));

        tx->m_Status = status;
        tx->m_Status->SetPending();

        tx->m_Owner = this;
        tx->m_TxSlot = slotIdx;
        m_TxSlots[slotIdx] = tx;

        success = true;
	}
	rcatchall
	{
        rlAssert(-1 == tx->m_TxSlot);
        tx->Reset();

        status->ForceFailed();
	}

    return success;
}

void
rlNpLookup::ResetAll()
{
    for(int i = 0; i < COUNTOF(m_TxSlots); ++i)
    {
        if(m_TxSlots[i])
        {
            RL_DELETE(m_TxSlots[i]);
        }

        rlAssert(!m_TxSlots[i]);
    }
}

void 
rlNpLookup::OnNpEvent(rlNp* /*np*/, const rlNpEvent* evt)
{
    if(RLNP_EVENT_ONLINE_STATUS_CHANGED == evt->GetId())
    {
        rlNpEventOnlineStatusChanged* msg = (rlNpEventOnlineStatusChanged*)evt;

        if(!msg->IsSignedOnline())
        {
            rlDebug("NP went offline for index %d, resetting", msg->m_LocalGamerIndex);
            this->ResetAll();
        }
    }
}

}   //namespace rage

#endif	// RSG_NP
