// 
// rline/rlnp.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_NP
#include "rlnp.h"

// RAGE Includes
#include "data/base64.h"
#include "diag/seh.h"
#include "file/device.h"
#include "grcore/setup.h"
#include "net/nethardware.h"
#include "rlnpbasic.h"
#include "rlnpcommon.h"
#include "rlnptrophy.h"
#include "rlnptitleid.h"
#include "rlpresence.h"
#include "rltitleid.h"
#include "system/appcontent.h"
#include "system/timer.h"
#include "system/service_orbis.h"

// PSN Libraries to Include
#include <np.h>
#include <user_service.h>
#include <kernel.h>
#include <libsysmodule.h>
#include <sdk_version.h>
#include <system_service.h>

// PSN Libraries to Link
#pragma comment(lib,"libSceNpCommon_stub_weak.a")
#pragma comment(lib,"libSceNpManager_stub_weak.a")
#pragma comment(lib, "libSceNpAuth_stub_weak.a")

// define for PlayTogether event 
// this is from SDK 3.5 and up but we can include it here and cast to use in SDK 1.7
#define SCE_NP_PLAY_TOGETHER_MAX_ONLINE_ID_LIST_NUM 7
typedef struct SceNpPlayTogetherHostEventParam
{
	SceUserServiceUserId	userId;														///< User ID of the local user that started the game.
	uint32_t				onlineIDListLen;											///< The number of onlineIds in the onlineIDList.
	SceNpOnlineId			onlineIDList[SCE_NP_PLAY_TOGETHER_MAX_ONLINE_ID_LIST_NUM];	///< The onlineIds to invite to the game.
} SceNpPlayTogetherHostEventParam;

namespace rage
{
CompileTimeAssert(sizeof(rage::rlSceNpAuthorizationCode) == sizeof(SceNpAuthorizationCode));

RAGE_DEFINE_SUBCHANNEL(rline, np)
#undef __rage_channel
#define __rage_channel rline_np

PARAM(npCountryCode, "Overrides the country code");

rlNp g_rlNp;

extern SceUserServiceUserId g_UserIds[SCE_USER_SERVICE_MAX_LOGIN_USERS];
extern const rlTitleId* g_rlTitleId;
extern bool g_IsExiting;
#define SOCIAL_CLUB_CLIENT_ID	"cd91bf7c-ace3-4c02-8ba4-69b55bae5931"

#if __BANK
unsigned sm_PresenceIndex = 0;
bool rlNp::sm_bPresenceForcedOffline = false;
bool rlNp::sm_bAuthUnavailable = false;
#endif

ServiceDelegate rlNp::sm_ServiceDelegate(&rlNp::OnServiceEvent);

class rlNpWorker
{

public:

	static const unsigned THREAD_STACK_SIZE = 4 * 1024; 
	static const unsigned RL_NP_WORKER_SLEEP_INTERVAL = 30; 

	rlNpWorker();
	bool Init(rlNp* npPtr);
	void Shutdown();
	static void ThreadFunc(void* p);
	static void NpPresenceCallback(const SceNpOnlineId *pOnlineId, SceNpGamePresenceStatus status, void* pUserArg);
	static void NpStateCallback(SceUserServiceUserId userId, SceNpState state, SceNpId* npId, void* pUserArg);

private:

	sysIpcThreadId			m_ThreadHandle;
	sysIpcSema				m_WaitThreadSema;
	bool					m_Finished; 
	rlNp*					m_Np; 
};

rlNpWorker::rlNpWorker()
	: m_ThreadHandle(sysIpcThreadIdInvalid)
	, m_WaitThreadSema(0)
	, m_Finished(false)
	, m_Np(nullptr)
{

}

bool rlNpWorker::Init(rlNp* npPtr)
{
	bool success = false; 

	rtry
	{
		// validate np pointer
		rverify(npPtr, catchall,);
		m_Np = npPtr;

		// make sure we haven't already created this thread
		rverify(m_ThreadHandle == sysIpcThreadIdInvalid, alreadystarted,);

		// register state callback
		int err = 0; 
		rverify(0 <= (err = sceNpRegisterStateCallback(NpStateCallback, this)), catchall, rlNpError("sceNpRegisterStateCallback failed", err));

		// register presence callback function (this has no return)
		sceNpRegisterGamePresenceCallback(NpPresenceCallback, this);

		// set control flag
		m_Finished = false;

		m_WaitThreadSema = sysIpcCreateSema(0);
		rverify(m_WaitThreadSema, catchall,);

		m_ThreadHandle = sysIpcCreateThread(&rlNpWorker::ThreadFunc,
			this,
			THREAD_STACK_SIZE,
			PRIO_NORMAL,
			"[rline] rlNp",
			-1, 
			"rlNpWorker");

		rverify(m_ThreadHandle != sysIpcThreadIdInvalid, catchall,);

		// wait for the thread to start
		sysIpcWaitSema(m_WaitThreadSema);

		// all good
		success = true; 
	}
	rcatch(alreadystarted)
	{
		// don't clean up here
	}
	rcatchall
	{
		if(m_WaitThreadSema)
		{
			sysIpcDeleteSema(m_WaitThreadSema);
			m_WaitThreadSema = 0;
		}
	}

	return success; 
}

void rlNpWorker::Shutdown()
{
	if(m_ThreadHandle == sysIpcThreadIdInvalid)
		return;

	if(!m_Finished)
	{
		m_Finished = true;

		// wait for the thread to complete.
		sysIpcWaitSema(m_WaitThreadSema);
		sysIpcWaitThreadExit(m_ThreadHandle);
		sysIpcDeleteSema(m_WaitThreadSema);
		m_ThreadHandle = sysIpcThreadIdInvalid;
	}
}

void rlNpWorker::ThreadFunc(void* p)
{
	rlNpWorker* worker = (rlNpWorker*)p;

	// signal that we've started
	sysIpcSignalSema(worker->m_WaitThreadSema);

	// stay here until something indicates we need to finish
	while(!worker->m_Finished)
	{
		// check for callbacks from the system
		sceNpCheckCallback();

		// pull user events
		int32_t ret;
		SceUserServiceEvent event;

		// read through all user service events until 'no event' is reached
		while(true)
		{
			ret = sceUserServiceGetEvent(&event);
			if (ret == SCE_OK) 
			{
				// process a new user service login/logout, and fire the appropriate event
				if (event.eventType == SCE_USER_SERVICE_EVENT_TYPE_LOGIN)
				{
					const rlNpManagerEvent e(rlNpManagerEvent::RLNP_EVENT_USER_SERVICE_LOGIN, 0, event.userId);
					g_rlNp.QueueEvent(e);
				} 
				else if (event.eventType == SCE_USER_SERVICE_EVENT_TYPE_LOGOUT) 
				{
					const rlNpManagerEvent e(rlNpManagerEvent::RLNP_EVENT_USER_SERVICE_LOGOUT, 0, event.userId);
					g_rlNp.QueueEvent(e);
				}
			}
			else if (ret == SCE_USER_SERVICE_ERROR_NO_EVENT) 
			{
				break;
			}
			else
			{
				// error handling
				break;
			}
		}

		// sleep for given interval
		sysIpcSleep(RL_NP_WORKER_SLEEP_INTERVAL);
	}

	// signal that we've finished
	sysIpcSignalSema(worker->m_WaitThreadSema);
}

void rlNpWorker::NpPresenceCallback(const SceNpOnlineId *pOnlineId, SceNpGamePresenceStatus status, void* pUserArg)
{
	// grab the worker
	rlNpWorker* worker = static_cast<rlNpWorker*>(pUserArg);

	// find the user locally
	SceUserServiceUserId userId = SCE_USER_SERVICE_USER_ID_INVALID; 
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		// compare against user name
		if(strncmp(pOnlineId->data, worker->m_Np->GetNpOnlineId(i).data, SCE_NP_ONLINEID_MAX_LENGTH) == 0)
		{
			userId = g_UserIds[i];
			break;
		}
	}

	rlDebug("NpPresenceCallback: For UserId: %d, Status: %s", userId, status == SCE_NP_GAME_PRESENCE_STATUS_OFFLINE ? "OFFLINE" : "ONLINE");	

	// generate an event
	const rlNpManagerEvent e(rlNpManagerEvent::RLNP_EVENT_PRESENCE, static_cast<int>(status), userId);
	worker->m_Np->QueueEvent(e);
}

void rlNpWorker::NpStateCallback(SceUserServiceUserId userId, SceNpState state, SceNpId* UNUSED_PARAM(npId), void* pUserArg)
{
	// grab the worker
	rlNpWorker* worker = static_cast<rlNpWorker*>(pUserArg);

#define MANAGER_EVENT_CASE(x, y) case x:										\
	{																		\
	rlDebug("NpStateCallback: ("#x"), For: %d", userId);					\
	rlNpManagerEvent e(y, static_cast<int>(x), userId);						\
	worker->m_Np->QueueEvent(e);											\
	}																		\
	break;

	switch(state)
	{
		MANAGER_EVENT_CASE(SCE_NP_STATE_UNKNOWN,	rlNpManagerEvent::RLNP_EVENT_STATE_UNKNOWN);
		MANAGER_EVENT_CASE(SCE_NP_STATE_SIGNED_OUT,	rlNpManagerEvent::RLNP_EVENT_STATE_SIGNED_OUT);
		MANAGER_EVENT_CASE(SCE_NP_STATE_SIGNED_IN,	rlNpManagerEvent::RLNP_EVENT_STATE_SIGNED_IN);
	default:
		rlError("Unknown NP state:0x%08x", state);
		break;
	}
#undef MANAGER_EVENT_CASE
}

static rlNpWorker s_rlNpWorker;
static sysCriticalSectionToken s_Cs;

//==============================================================================
// rlNp Static Functions
//==============================================================================
void rlNp::InitLibs()
{
	if (sceSysmoduleLoadModule(SCE_SYSMODULE_NP_AUTH) != SCE_OK)
	{
		Quitf("Failed to load prx SCE_SYSMODULE_NP_AUTH");
	}
#if RL_NP_BANDWIDTH
	rlNpBandwidth::InitLibs();
#endif
}

void rlNp::InitServiceDelegate()
{
	g_SysService.AddDelegate(&sm_ServiceDelegate);
}

const rlNpTitleId& 
rlNp::GetTitleId()
{
    rlAssert(g_rlTitleId);
    return g_rlTitleId->m_NpTitleId;
}

rlGameRegion 
rlNp::GetGameRegion()
{
	// there is no way to get the region on PS4 yet (https://ps4.scedev.net/forums/thread/15879/)
	// So we're using sysAppContent
	if (sysAppContent::IsEuropeanBuild())
		return RL_GAME_REGION_EUROPE;
	if (sysAppContent::IsAmericanBuild())
		return RL_GAME_REGION_AMERICA;
	if (sysAppContent::IsJapaneseBuild())
		return RL_GAME_REGION_JAPAN;
	return RL_GAME_REGION_INVALID;
}

rlGameRegion
rlNp::GetGameRegionFromProductCode(const char* productCode)
{
    rlGameRegion region = RL_GAME_REGION_INVALID;

    if(!productCode)
    {
       rlError("GetGameRegionFromProductCode :: Productcode is null");
    }
    else if(strlen(productCode) <= 4)
    {
       rlError("GetGameRegionFromProductCode :: Productcode (%s) is too short; appears to be invalid", productCode);
    }
    else
    {
        //The third character of the product code determines what
        //region it corresponds to.
        switch(tolower(productCode[2]))
        {
        case 'u':   region = RL_GAME_REGION_AMERICA; break;
        case 'e':   region = RL_GAME_REGION_EUROPE; break;
        case 'j':   region = RL_GAME_REGION_JAPAN; break;
        
        case 'k':   //region = RL_GAME_REGION_KOREA; break;
        case 'a':   //region = RL_GAME_REGION_ASIA_BLUERAY; break;
        case 'h':   //region = RL_GAME_REGION_ASIA_PSN; break;
        default:    
            rlError("GetGameRegionFromProductCode :: Unknown or unhandled region indicator in product code (%s)", productCode);
            break;
        }
    }

    return region;
}

#if !__NO_OUTPUT
const char* rlNp::GetNpEnvironmentAsString(const rlNpEnvironment env)
{
#define ENV_CASE(a) case a: return #a;
	switch(env)
	{
		ENV_CASE(RLNP_ENV_UNRECOGNIZED)
		ENV_CASE(RLNP_ENV_UNKNOWN)
		ENV_CASE(RLNP_ENV_SP_INT)
		ENV_CASE(RLNP_ENV_PROD_QA)
		ENV_CASE(RLNP_ENV_NP)
		ENV_CASE(RLNP_ENV_PROD_QA_2)
		ENV_CASE(RLNP_NUM_ENV)
	}
	return "Unknown Env";
}
#endif

//==============================================================================
// rlNp Class Functions
//==============================================================================
rlNp::rlNp()
: m_OnlineStartTime(0)
, m_Initialized(false)
, m_OwnNp(false)
, m_uLastPresenceChangeTime(0)
, m_uTimeOfLastPresenceDisconnect(0)
, m_uNumUnackedHeartbeatPings(0)
#if RL_NP_SUPPORT_PLAY_TOGETHER
, m_bPendingPlayTogetherEvent(false)
#endif
{
	sysMemSet(&m_DefaultSessionImage, 0, sizeof(m_DefaultSessionImage));

	m_NativeEnv = RLNP_ENV_UNKNOWN;
	m_Env = RLNP_ENV_UNKNOWN;

	for (int i = 0 ; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		m_State[i] = STATE_INVALID;
		m_NextState[i] = STATE_INVALID;
		m_NpStatus[i] = SCE_NP_STATE_UNKNOWN;
		m_AccountIds[i] = RL_INVALID_NP_ACCOUNT_ID;
		m_PlayStationPlusRequiredForRealtimeMultiplayer[i] = true;

		// np unavailable backoff - immediately retry, then wait 60, then backoff for 5 mins
		m_NpUnavailabilityBackoff[i].InitSeconds(1, 60, 300);

		Reset(i);
	}
}

rlNp::~rlNp()
{
    Shutdown();
}

bool
rlNp::Init()
{
    rlDebug("Init");

	while(g_IsExiting)
		sysIpcSleep(1000);

    rtry
    {
        rverify(!m_Initialized, catchall, rlError("Init :: Already initialized"));

        m_Initialized = true; //So Shutdown() will undo all in case of error

        int err;

        //Init NP
		m_OwnNp = true;

		err = sceNpSetNpTitleId(g_rlTitleId->m_NpTitleId.GetSceNpTitleId(), g_rlTitleId->m_NpTitleId.GetSceNpTitleSecret());
		rverify(err == SCE_OK, catchall, );
		
#if !__NO_OUTPUT
		fiDevice::SystemTime currentTime;
		fiDevice::GetLocalSystemTime(currentTime);
		rlAssertf(currentTime.wYear >= 2013, "Your system time is incorrectly set to %02d/%02d/%04d. Please set your system time from the PS4 dashboard to avoid a lot of asserts from failed SDK functionality.", currentTime.wMonth, currentTime.wDay, currentTime.wYear);
#endif // asserts for system clocks

		if(SCE_NP_ERROR_ALREADY_INITIALIZED == err)
		{
			rlDebug("NP is already initialized - won't call sceNp2Term() later");
			m_OwnNp = false;
		}
		else
		{
			rverify(0 <= err, catchall, rlNpError("sceNp2Init failed", err));
		}

		// initialize worker thread
		s_rlNpWorker.Init(this);

		// Retrieve NpStatus for all users
		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			if (g_UserIds[i] != SCE_USER_SERVICE_USER_ID_INVALID)
			{
				rverify(UpdateNpState(i), catchall, rlNpError("sceNpGetState failed", err));
			}
		}

		//Init NP subsystems.
		rverify(m_NpBasic.Init(), catchall, rlError("Init :: Failed to init rlNpBasic"));

		//  NOTE: it's legal for trophy init to fail, as not all apps have trophies.
		if(!m_NpTrophy.Init(GetTitleId().GetCommunicationId(), GetTitleId().GetCommunicationSignature()))
		{
			rlWarning("Failed to init rlNpTrophy - perhaps trophies aren't being used.");
		}

		BANK_ONLY(rverify(m_NpFaultTolerance.Init(), catchall, rlError("Init :: rlNpFaultTolerance failed initialization"));)

		rverify(m_NpWebApi.Init(), catchall, rlError("Failed to init rlNpWebAPI"));
		rverify(m_NpParty.Init(), catchall, rlError("Init :: Failed to init rlNpParty"));

		sysMemSet(&m_ClientId, 0, sizeof(m_ClientId));
		safecpy(m_ClientId.id, SOCIAL_CLUB_CLIENT_ID, SCE_NP_CLIENT_ID_MAX_LEN);

		rverify(m_NpInGameMsgMgr.Init(), catchall, rlError("Failed to init m_NpInGameMsgMgr"));
		rverify(m_NpCommonDialog.Init(), catchall, rlError("Failed to init m_NpCommonDialog"));
		rverify(m_NpAuth.Init(), catchall, rlError("Failed to Init m_NpAuth"));
		rverify(m_NpActivityFeed.Init(), catchall, rlError("Failed to init m_NpActivityFeed"));
#if RL_NP_BANDWIDTH
		rverifyall(m_NpBandwidth.Init());
#endif


        //Kick off state machine.
		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			Restart(i);
		}

		// Add Bank Widgets
		AddWidgets();
    }
    rcatchall
    {
        Shutdown();
    }

    return m_Initialized;
}

void 
rlNp::Shutdown()
{
    rlDebug("Shutdown");

    if(m_Initialized)
    {
		// shutdown worker thread
		s_rlNpWorker.Shutdown();

		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			Reset(i);
		}

        m_Delegator.Clear();

		// Shutdown all rlNp member classes
        m_NpTrophy.Shutdown();
        m_NpBasic.Shutdown();
		BANK_ONLY(m_NpFaultTolerance.Shutdown();)
		m_NpWebApi.Shutdown();
		m_NpParty.Shutdown();
		m_NpInGameMsgMgr.Shutdown();
		m_NpCommonDialog.Shutdown();
#if RL_NP_BANDWIDTH
		m_NpBandwidth.Shutdown();
#endif

		// terminate sce Net
		sceNetTerm();

        rlAssert(!m_MgrEventQueue.GetCount());

		// Remove the service delegate
		g_SysService.RemoveDelegate(&sm_ServiceDelegate);

		for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
		{
			m_NpStatus[i] = SCE_NP_STATE_UNKNOWN;
		}

        m_OwnNp = false;
        m_Initialized = false;
    }
}

bool
rlNp::IsInitialized() const
{
    return m_Initialized;
}

void
rlNp::Update()
{
    SYS_CS_SYNC(m_Cs);

    rlAssert(m_Initialized);

	m_NpParty.Update();
	m_NpCommonDialog.Update();
	m_NpWebApi.Update();
	m_NpAuth.Update();
#if RL_NP_BANDWIDTH
	m_NpBandwidth.Update();
#endif

#if RL_NP_SUPPORT_PLAY_TOGETHER
	// check if we can fire a play together event if it's pending
	CheckPendingPlayTogetherEvent(); 
#endif

	// Check for Np Callbacks from the system
	this->ProcessEvents();

	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if(m_NextState[i] != STATE_INVALID)
		{
			StartState(i, m_NextState[i]);
		}

		UpdateState(i, m_State[i]);

		m_NpBasic.Update(i);
	}

	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if(m_State[i] > STATE_WAIT_FOR_NP_ONLINE)
		{
			if(!netHardware::IsLinkConnected())
			{
				OnNpConnectionLost(i, rlNpSignedOfflineReason::RLNP_SIGNED_OFFLINE_HARDWARE_LINK_DISCONNECTED);
			}
		}
	}
}

bool
rlNp::IsOffline(const int localGamerIndex) const
{
    if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
    {
        return (m_NpStatus[localGamerIndex] == SCE_NP_STATE_UNKNOWN) || (m_NpStatus[localGamerIndex] == SCE_NP_STATE_SIGNED_OUT);
    }

    return true;
}

bool
rlNp::IsLoggedIn(const int localGamerIndex) const
{
	if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
		return (m_NpStatus[localGamerIndex] == SCE_NP_STATE_SIGNED_IN);
	}
	
	return false;
}

bool
rlNp::IsOnline(const int localGamerIndex) const
{
	if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
	{
		return STATE_ONLINE == m_State[localGamerIndex];
	}

	return false;
}

bool 
rlNp::UpdateNpState(const int localGamerIndex)
{
	if(g_UserIds[localGamerIndex] == SCE_USER_SERVICE_USER_ID_INVALID) // invalid 
	{
		return true;
	}

	int err = sceNpGetState(g_UserIds[localGamerIndex], (SceNpState*)&m_NpStatus[localGamerIndex]);
	if(err != SCE_OK)
	{
		rlError("UpdateNpState :: sceNpGetState failed: Error: 0x%08x", err);
		m_NpStatus[localGamerIndex] = SCE_NP_STATE_UNKNOWN;
		return false;
	}

	if(IsLoggedIn(localGamerIndex))
	{
		UpdateNpId(localGamerIndex);
		GetGamePresenceStatus(localGamerIndex);
	}

	return true;
}

bool 
rlNp::UpdateNpId(const int localGamerIndex)
{
	int err = sceNpGetOnlineId(g_UserIds[localGamerIndex], (SceNpOnlineId*)&m_OnlineIds[localGamerIndex]);
	if (err != SCE_OK)
	{
		rlNpError("sceNpGetOnlineId failed", err);
		return false;
	}

	rlSceNpAccountId oldAccountId = m_AccountIds[localGamerIndex];
	err = sceNpGetAccountId((SceNpOnlineId*)&m_OnlineIds[localGamerIndex], (SceNpAccountId*)&m_AccountIds[localGamerIndex]);
	if (err != SCE_OK)
	{
		rlNpError("sceNpGetAccountId failed", err);
		return false;
	}

	// Log out on account ID change.
	if (oldAccountId != m_AccountIds[localGamerIndex])
	{
		rlDebug("UpdateNpId :: Index: %d, OnlineId: %s, AccountId: %" I64FMT "u, UserServiceId: %d",
			localGamerIndex, m_OnlineIds[localGamerIndex].data, m_AccountIds[localGamerIndex], g_UserIds[localGamerIndex]);
	}

	return true;
}

const char* 
rlNp::GetSignInName(const int localGamerIndex)
{
	if(rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)) && '\0' == m_SignInNames[localGamerIndex][0])
	{
		int err;
		if(g_UserIds[localGamerIndex] == SCE_USER_SERVICE_USER_ID_INVALID ||
			SCE_OK != (err = sceUserServiceGetUserName(g_UserIds[localGamerIndex],m_SignInNames[localGamerIndex],sizeof(m_SignInNames[localGamerIndex]))))
		{
			m_SignInNames[localGamerIndex][0] = '\0';
			return NULL;
		}
	}

	return m_SignInNames[localGamerIndex];
}

const rlSceNpOnlineId& 
rlNp::GetNpOnlineId(const int localGamerIndex) const
{
	return this->IsLoggedIn(localGamerIndex) ? m_OnlineIds[localGamerIndex] : rlSceNpOnlineId::RL_INVALID_ONLINE_ID;
}

const rlSceNpAccountId&
rlNp::GetNpAccountId(const int localGamerIndex) const
{
	return this->IsLoggedIn(localGamerIndex) ? m_AccountIds[localGamerIndex] : RL_INVALID_NP_ACCOUNT_ID;
}

const rlSceNpAccountId& 
rlNp::GetNpAccountIdFromNpOnlineId(const rlSceNpOnlineId& onlineId)
{
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if(strncmp(m_OnlineIds[i].data, onlineId.data, SCE_NP_ONLINEID_MAX_LENGTH) == 0)
		{
			return m_AccountIds[i];
		}
	}

	return RL_INVALID_NP_ACCOUNT_ID;
}

void
rlNp::AddDelegate(rlNpEventDelegate* dlgt)
{
    SYS_CS_SYNC(m_Cs);
    m_Delegator.AddDelegate(dlgt);
}

void
rlNp::RemoveDelegate(rlNpEventDelegate* dlgt)
{
    SYS_CS_SYNC(m_Cs);
    m_Delegator.RemoveDelegate(dlgt);
}

void
rlNp::DispatchEvent(const rlNpEvent *e)
{
    m_Delegator.Dispatch(this, e);
}

rlNpEnvironment
rlNp::GetEnvironment() const
{
	return m_Env;
}

rlNpEnvironment
rlNp::GetNativeEnvironment() const
{
	return m_NativeEnv;
}

rlNpEnvironment rlNp::GetNpEnvironmentFromRlEnvironment(const rlEnvironment rlEnv)
{
	switch(rlEnv)
	{
	case RL_ENV_DEV:        return RLNP_ENV_SP_INT;
	case RL_ENV_CERT:       return RLNP_ENV_PROD_QA;
	case RL_ENV_CERT_2:     return RLNP_ENV_PROD_QA_2;
	case RL_ENV_PROD:       return RLNP_ENV_NP;
	default:                return RLNP_ENV_UNKNOWN;
	}
}

rlEnvironment rlNp::GetRlEnvironmentFromNpEnvironment(const rlNpEnvironment npEnv)
{
	switch(npEnv)
	{
	case RLNP_ENV_SP_INT:      return RL_ENV_DEV;
	case RLNP_ENV_PROD_QA:     return RL_ENV_CERT;
	case RLNP_ENV_PROD_QA_2:   return RL_ENV_CERT_2;
	case RLNP_ENV_NP:          return RL_ENV_PROD;
	default:                   return RL_ENV_UNKNOWN;
	}
}

int
rlNp::GetChatRestrictionFlag(int localGamerIndex) const
{
	return m_NpAuth.GetChatRestrictionFlag(localGamerIndex);
}

//private:

#if __BANK
void NpPresenceOnlineCB()
{
	if (g_rlNp.IsLoggedIn(sm_PresenceIndex))
	{
		// generate an event
		const int userId = g_rlNp.GetUserServiceId(sm_PresenceIndex);
		const rlNpManagerEvent e(rlNpManagerEvent::RLNP_EVENT_USER_SERVICE_LOGIN, SCE_NP_GAME_PRESENCE_STATUS_ONLINE, userId);
		g_rlNp.QueueEvent(e);
	}
}

void NpPresenceOfflineCB()
{
	if (g_rlNp.IsLoggedIn(sm_PresenceIndex))
	{
		// generate an event
		const int userId = g_rlNp.GetUserServiceId(sm_PresenceIndex);
		const rlNpManagerEvent e(rlNpManagerEvent::RLNP_EVENT_USER_SERVICE_LOGIN, SCE_NP_GAME_PRESENCE_STATUS_OFFLINE, userId);
		g_rlNp.QueueEvent(e);
	}
}

void NpRecheckAvailabilityCB()
{
	if (g_rlNp.IsLoggedIn(sm_PresenceIndex))
	{
		g_rlNp.RecheckNpAvailability(sm_PresenceIndex);
	}
}

void NpConnectionLostCB()
{
	if (g_rlNp.IsLoggedIn(sm_PresenceIndex))
	{
		g_rlNp.OnNpConnectionLost(sm_PresenceIndex, rlNpSignedOfflineReason::RLNP_SIGNED_OFFLINE_DEBUG);
	}
}
#endif

void
rlNp::AddWidgets()
{
#if __BANK
	bkBank *pBank = BANKMGR.FindBank("Network");

	if(!pBank)
	{
		pBank = &BANKMGR.CreateBank("Network");
	}

	if (!pBank) { return; }

	pBank->PushGroup("NP", false);

	pBank->AddSlider("Np Index", &sm_PresenceIndex, 0, 3, 1);
	pBank->AddSeparator("Presence");
	pBank->AddButton("NP Presence Online Callback", datCallback(NpPresenceOnlineCB));
	pBank->AddButton("NP Presence Offline Callback", datCallback(NpPresenceOfflineCB));
	pBank->AddToggle("NP Presence Offline", &sm_bPresenceForcedOffline, NullCallback);
	pBank->AddSeparator("Availability");
	pBank->AddToggle("NP Unavailable", &sm_bAuthUnavailable, NullCallback);
	pBank->AddButton("OnNpConnectionLost", datCallback(NpConnectionLostCB));
	pBank->AddButton("Recheck Np Availability", datCallback(NpRecheckAvailabilityCB));

	RL_NP_BANDWIDTH_ONLY(m_NpBandwidth.AddWidgets(pBank));
	pBank->PopGroup();
#endif
}

void
rlNp::Reset(const int localGamerIndex)
{
	rlDebug("Reset :: Index: %d", localGamerIndex);
	if(!rlVerify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex)))
		return;

    SYS_CS_SYNC(m_Cs);

    //Clear the event queue
    this->ProcessEvents(localGamerIndex);

	m_NpAuth.Reset(localGamerIndex);

    m_State[localGamerIndex] = m_NextState[localGamerIndex] = STATE_INVALID;

	m_SignInNames[localGamerIndex][0] = '\0';
	m_OnlineIds[localGamerIndex].Clear();
	m_PresenceStatus[localGamerIndex] = SCE_NP_GAME_PRESENCE_STATUS_OFFLINE;

    UpdateNpState(localGamerIndex);
}

void
rlNp::Restart(const int localGamerIndex)
{
    SYS_CS_SYNC(m_Cs);

    if(m_OnlineStartTime)
    {
		rlDebug("Restart :: Uptime: %ums", sysTimer::GetSystemMsTime() - m_OnlineStartTime);
		m_OnlineStartTime = 0;
    }

    Reset(localGamerIndex);
    m_NextState[localGamerIndex] = STATE_FIRST;
    StartState(localGamerIndex, m_NextState[localGamerIndex]);
}

void
rlNp::RetryState(const int localGamerIndex)
{
    SYS_CS_SYNC(m_Cs);
    m_NextState[localGamerIndex] = m_State[localGamerIndex];
}

void
rlNp::StartState(const int localGamerIndex, const int state)
{
    SYS_CS_SYNC(m_Cs);

    m_State[localGamerIndex] = state;
	m_NextState[localGamerIndex] = STATE_INVALID;

#if !__NO_OUTPUT
    static const char *s_rlStateNames[] =
    {
        "STATE_WAIT_FOR_NP_ONLINE",
		"STATE_CHECK_NP_AVAILABILITY",
		"STATE_WAIT_NP_AVAILABILITY",
		"STATE_NP_UNAVAILABLE",
		"STATE_ONLINE",
		"STATE_REQUEST_AUTH",
		"STATE_WAIT_FOR_AUTH",
		"STATE_REQUEST_AUTH_FAILED",
		"STATE_REQUEST_PERMISSIONS",
		"STATE_WAIT_PERMISSIONS",
		"STATE_REQUEST_PERMISSIONS_FAILED"
	};

    CompileTimeAssert(COUNTOF(s_rlStateNames) == STATE_NUM_STATES);

	int stateIndex = m_State[localGamerIndex];
	if( stateIndex < 0 )
	{
		rlDebug("StartState :: Index: %d, State: INVALID STATE, Time: %u", localGamerIndex, sysTimer::GetSystemMsTime());
	}
	else
	{
		rlDebug("StartState :: Index: %d, State: %s, Time: %u", localGamerIndex, s_rlStateNames[stateIndex], sysTimer::GetSystemMsTime());
	}
#endif

	if(STATE_NP_UNAVAILABLE == state)
	{
		rlNpEventNpUnavailable e(localGamerIndex);
		m_Delegator.Dispatch(this, &e);
	}

    if(STATE_ONLINE == state)
    {
        rlNpEventOnlineStatusChanged e(localGamerIndex, rlNpOnlineStatus::RLNP_STATUS_ONLINE, rlNpSignedOfflineReason::RLNP_SIGNED_OFFLINE_INVALID);
        m_Delegator.Dispatch(this, &e);
    }
}

void 
rlNp::UpdateState(const int localGamerIndex, const int state)
{
    SYS_CS_SYNC(m_Cs);

    switch(state)
    {
    case STATE_WAIT_FOR_NP_ONLINE:
		{
			if(IsLoggedIn(localGamerIndex) && netHardware::IsLinkConnected())
			{
				if(UpdateNpId(localGamerIndex))
				{
					m_OnlineStartTime = sysTimer::GetSystemMsTime();
					this->StartState(localGamerIndex, STATE_CHECK_NP_AVAILABILITY);
				}
				else
				{
					rlError("UpdateState :: STATE_WAIT_FOR_NP_ONLINE :: Failed to retrieve NpId");
				}
			}
		}
        break;
	case STATE_CHECK_NP_AVAILABILITY:
		{
			if (m_NpAuth.CanCheckNPAvailability(localGamerIndex))
			{
				m_NpAuth.CheckNPAvailability(localGamerIndex);
				this->StartState(localGamerIndex, STATE_WAIT_NP_AVAILABILITY);
			}
		}
		break;
	case STATE_WAIT_NP_AVAILABILITY:
		{
			if (!m_NpAuth.IsRefreshingNpAvailability(localGamerIndex))
			{
				if (m_NpAuth.IsNpAvailable(localGamerIndex))
				{
					this->StartState(localGamerIndex, STATE_REQUEST_AUTH);
					m_NpUnavailabilityBackoff[localGamerIndex].Reset();
				}
				else
				{
					this->StartState(localGamerIndex, STATE_NP_UNAVAILABLE);
					rlDebug("STATE_WAIT_NP_AVAILABILITY :: Unavailable - Index: %d, Retrying in %ums", localGamerIndex, m_NpUnavailabilityBackoff[localGamerIndex].GetCurrentInterval());
				}
			}
		}
		break;
	case STATE_NP_UNAVAILABLE:
		if (!m_NpAuth.IsRefreshingNpAvailability(localGamerIndex) && 
			m_NpAuth.IsNpUnavailabilityReasonValidForRetry(localGamerIndex) &&
			m_NpUnavailabilityBackoff[localGamerIndex].IsReadyToRetry()) 
		{
			RecheckNpAvailability(localGamerIndex);
			m_NpUnavailabilityBackoff[localGamerIndex].Increment();
		}
		break;
	case STATE_ONLINE:
		{
			// Heartbeat -- Use our once-per-minute presence pings to check for uplink cable/internet loss
			if (!netRelay::IsConnectedToPresence())
			{
				// Calculate how many un-acked presence pings have gone through. If another ping goes through, attempt to clear our PSN presence status.
				unsigned numUnackedPings = netRelay::GetNumUnackedPresencePings();
				if (numUnackedPings > 0 && numUnackedPings > m_uNumUnackedHeartbeatPings)
				{
					rlDebug3("STATE_ONLINE :: Not connected to presence service.");
					rlDebug3("%u unacked presence pings exceeds previous number of unacked presence pings (%u)", netRelay::GetNumUnackedPresencePings(), m_uNumUnackedHeartbeatPings);

					m_uNumUnackedHeartbeatPings = numUnackedPings;

					// clear game presence status - added benefit of kickstarting the NP presence 
					g_rlNp.GetWebAPI().DeleteGameStatus(localGamerIndex);
				}

				// PSN and R* Presence both are not connected, throw a connection lost event. Cap this to once every 5 minutes
				// to avoid state-cycling rapidly in the extreme edge case where we can call almost every service except R*/PSN presence.
				// Require the Presence status to be offline for at least 15 seconds to avoid full removals based on hiccups.
				if (m_PresenceStatus[localGamerIndex] == SCE_NP_GAME_PRESENCE_STATUS_OFFLINE)
				{
					unsigned timeOffLine = sysTimer::GetSystemMsTime() - m_uLastPresenceChangeTime;
					unsigned timeSinceLastPresenceDisconnect = sysTimer::GetSystemMsTime() - m_uTimeOfLastPresenceDisconnect;
					if (timeOffLine > NP_STATE_CHANGE_TIMER_MS && timeSinceLastPresenceDisconnect > PRESENCE_HEARTBEAT_DISCONNECT_INTERVAL_MS)
					{
						m_uTimeOfLastPresenceDisconnect = sysTimer::GetSystemMsTime();
						OnNpConnectionLost(localGamerIndex, rlNpSignedOfflineReason::RLNP_SIGNED_OFFLINE_UNREACHABLE);
					}
				}
			}
		}
		break;
	case STATE_REQUEST_AUTH:
		if (this->CanRequestAuthCode(localGamerIndex))
		{
			if (m_NpAuth.HasRetrievedIssuerId())
			{
				m_NativeEnv = GetEnvironmentFromIssuerId(m_NpAuth.GetIssuerId());
				m_Env = GetEnvironmentFromNativeEnvironment(m_NativeEnv);

				rlDebug1("Environment :: Native: %s, Active: %s", GetNpEnvironmentAsString(m_NativeEnv), GetNpEnvironmentAsString(m_Env));

				this->StartState(localGamerIndex, STATE_REQUEST_PERMISSIONS);
			}
			else if (m_NpAuth.RefreshAuthCode(localGamerIndex))
			{
				this->StartState(localGamerIndex, STATE_WAIT_FOR_AUTH);
			}
			else
			{
				this->StartState(localGamerIndex, STATE_REQUEST_AUTH_FAILED);
			}
		}
		break;
	case STATE_WAIT_FOR_AUTH:
		if (!m_NpAuth.IsRefreshingAuthCode())
		{
			if (m_NpAuth.HasRetrievedIssuerId())
			{
				m_NativeEnv = GetEnvironmentFromIssuerId(m_NpAuth.GetIssuerId());
				m_Env = GetEnvironmentFromNativeEnvironment(m_NativeEnv);

				rlDebug1("Environment :: Native: %s, Active: %s", GetNpEnvironmentAsString(m_NativeEnv), GetNpEnvironmentAsString(m_Env));

				this->StartState(localGamerIndex, STATE_REQUEST_PERMISSIONS);
			}
			else
			{
				this->StartState(localGamerIndex, STATE_REQUEST_AUTH_FAILED);
			}
		}
		break;
	case STATE_REQUEST_AUTH_FAILED:
		this->StartState(localGamerIndex, STATE_REQUEST_AUTH);
        break;
	case STATE_REQUEST_PERMISSIONS:
		if (this->CanRequestPermissions(localGamerIndex))
		{
			if (m_NpAuth.RequestPermissions(localGamerIndex))
			{
				this->StartState(localGamerIndex, STATE_WAIT_PERMISSIONS);
			}
			else
			{
				this->StartState(localGamerIndex, STATE_REQUEST_PERMISSIONS_FAILED);
			}
		}
		break;
	case STATE_WAIT_PERMISSIONS:
		if (!m_NpAuth.IsRefreshingPermissions(localGamerIndex))
		{
			if (m_NpAuth.HasRetrievedPermissions(localGamerIndex))
			{
				// set initial game presence status
				GetGamePresenceStatus(localGamerIndex);

				// commit to online state
				this->StartState(localGamerIndex, STATE_ONLINE);

				// clear presence blob
				rlPresence::SetBlob(NULL, 0);
			}
			else
			{
				this->StartState(localGamerIndex, STATE_REQUEST_PERMISSIONS_FAILED);
			}
		}
		break;
	case STATE_REQUEST_PERMISSIONS_FAILED:
		this->StartState(localGamerIndex, STATE_REQUEST_PERMISSIONS);
		break;
    }
}

static sysCriticalSectionToken s_EventQueueCS;

void
rlNp::QueueEvent(const rlNpManagerEvent& evt)
{
	SYS_CS_SYNC(s_EventQueueCS);
	m_MgrEventQueue.QueueEvent(evt);
}

void
rlNp::ProcessEvents(const int localGamerIndex)
{
	SYS_CS_SYNC(s_EventQueueCS);

	if(m_MgrEventQueue.GetCount() == 0)
		return; 

	rlNpEventQueue<rlNpManagerEvent, RL_NP_EVENTS_QUEUE_SIZE> eventsToPushBackIn;
	rlNpManagerEvent me;

	while(m_MgrEventQueue.NextEvent(&me))
	{
		// TODO: NS - PS4 doesn't tell us when we've been kicked due to duplicate sign-in. Is there another way to detect duplicate sign-ins?
		switch(me.m_EventId)
		{

		case rlNpManagerEvent::RLNP_EVENT_STATE_UNKNOWN:
		case rlNpManagerEvent::RLNP_EVENT_STATE_SIGNED_OUT:
		case rlNpManagerEvent::RLNP_EVENT_STATE_SIGNED_IN:
			{
				// check this should be processed this loop
				if (!ProcessStateEvent(me, localGamerIndex))
				{
					// push the event back in the queue, its not for this localGamerIndex
					eventsToPushBackIn.QueueEvent(me);
					continue;
				}
			}
			break;

		case rlNpManagerEvent::RLNP_EVENT_USER_SERVICE_LOGIN:
			{
				int eventGamerIndex = RL_INVALID_GAMER_INDEX;
				for (int i = 0; i < SCE_USER_SERVICE_MAX_LOGIN_USERS; i++)
				{
					if (g_UserIds[i] == me.m_UserServiceId)
					{
						break;
					}
					else if (g_UserIds[i] == SCE_USER_SERVICE_USER_ID_INVALID)
					{
						g_UserIds[i] = me.m_UserServiceId;
						eventGamerIndex = i;

						// update NpStatus and NpId immediately
						UpdateNpState(eventGamerIndex);
						break;
					}
				}

				ProcessUserServiceEventCommon(me.m_UserServiceId, eventGamerIndex);
			}
			break;

		case rlNpManagerEvent::RLNP_EVENT_USER_SERVICE_LOGOUT:
			{
				int eventGamerIndex = RL_INVALID_GAMER_INDEX;
				for (int i = 0; i < SCE_USER_SERVICE_MAX_LOGIN_USERS; i++)
				{
					if (g_UserIds[i] == me.m_UserServiceId)
					{
						// reset user id and sign in name
						g_UserIds[i] = SCE_USER_SERVICE_USER_ID_INVALID;
						sysMemSet(m_SignInNames[i], 0, sizeof(m_SignInNames[i]));
						m_NpStatus[i] = SCE_NP_STATE_UNKNOWN;
						eventGamerIndex = i;

						break;
					}
				}

				ProcessUserServiceEventCommon(me.m_UserServiceId, eventGamerIndex);
			}
			break;

		case rlNpManagerEvent::RLNP_EVENT_PRESENCE:
			{
#if !__NO_OUTPUT
				int eventGamerIndex = false; 
#endif
				// find the local player, mark as handled if found
				for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
				{
					if (me.m_UserServiceId == GetUserServiceId(i))
					{
						SetPresenceStatus(i, static_cast<SceNpGamePresenceStatus>(me.m_Context));
						OUTPUT_ONLY(eventGamerIndex = i);
						break;
					}
				}

				rlDebug("ProcessEvents :: Handling RLNP_EVENT_PRESENCE for %d [%d], Status: %s", me.m_UserServiceId, eventGamerIndex, (me.m_Context == SCE_NP_GAME_PRESENCE_STATUS_OFFLINE) ? "OFFLINE" : "ONLINE");	
			}
			break;

		default:
			break;
		}
	}

	while(eventsToPushBackIn.NextEvent(&me))
	{
		m_MgrEventQueue.QueueEvent(me);
	}
}

bool rlNp::ProcessStateEvent(const rlNpManagerEvent& evt, int localGamerIndex = RL_INVALID_GAMER_INDEX)
{
	int eventGamerIndex = GetUserServiceIndex(evt.m_UserServiceId);

	// if this resolves to no gamer index, indicate that the event is handled
	// so that we don't automatically requeue it
	if (!RL_IS_VALID_LOCAL_GAMER_INDEX(eventGamerIndex))
		return true;

	// bail if this event is not for the supplied gamer index
	if (localGamerIndex >= 0 && (eventGamerIndex != localGamerIndex))
		return false;

	switch(evt.m_EventId)
	{

	case rlNpManagerEvent::RLNP_EVENT_STATE_UNKNOWN:
		{
			rlDebug("ProcessStateEvent :: Handling RLNP_EVENT_STATE_UNKNOWN for %d [%d]", evt.m_UserServiceId, eventGamerIndex);	
			m_NpStatus[eventGamerIndex] = SCE_NP_STATE_UNKNOWN;
			this->OnNpConnectionLost(eventGamerIndex, rlNpSignedOfflineReason::RLNP_SIGNED_OFFLINE_STATE_UNKNOWN);
		}
		break;

	case rlNpManagerEvent::RLNP_EVENT_STATE_SIGNED_OUT:
		{
			rlDebug("ProcessStateEvent :: Handling SCE_NP_STATE_SIGNED_OUT for %d [%d]", evt.m_UserServiceId, eventGamerIndex);
			m_NpStatus[eventGamerIndex] = SCE_NP_STATE_SIGNED_OUT;
			this->OnNpConnectionLost(eventGamerIndex, rlNpSignedOfflineReason::RLNP_SIGNED_OFFLINE_STATE_SIGNED_OUT);
		}
		break;

	case rlNpManagerEvent::RLNP_EVENT_STATE_SIGNED_IN:
		{
			rlDebug("ProcessStateEvent :: Handling SCE_NP_STATE_SIGNED_IN for %d [%d]", evt.m_UserServiceId, eventGamerIndex);
			m_NpStatus[eventGamerIndex] = SCE_NP_STATE_SIGNED_IN;
		}
		break;

	default:
		rlAssertf(0, "ProcessStateEvent :: Invalid event ID: %d", evt.m_EventId);
		return false;
	}

	// handled
	return true; 
}

void rlNp::ProcessUserServiceEventCommon(const int userId, const int localGamerIndex)
{
	if (localGamerIndex >= 0)
	{
		// Mark the trophy contexts as dirty 
		m_NpTrophy.OnUserLoginStatuschange(localGamerIndex);

		// Send the NpEvent 
		rlNpEventUserServiceStatusChanged e(userId, localGamerIndex);
		m_Delegator.Dispatch(this, &e);
	}
}

bool rlNp::CanRequestAuthCode(const int localGamerIndex)
{
	if (m_NpAuth.IsRefreshingAuthCode())
		return false;

	if (g_UserIds[localGamerIndex] == SCE_USER_SERVICE_USER_ID_INVALID)
		return false;

	if (m_NpAuth.CanRequestAuthCode())
	{
		return true;
	}

	return false;
}

bool rlNp::CanRequestPermissions(const int localGamerIndex)
{
	if (m_NpAuth.IsRefreshingAuthCode() || m_NpAuth.IsRefreshingPermissions(localGamerIndex))
		return false;

	if (g_UserIds[localGamerIndex] == SCE_USER_SERVICE_USER_ID_INVALID)
		return false;

	if (m_NpAuth.CanRequestPermissions(localGamerIndex))
	{
		return true;
	}

	return false;
}

bool rlNp::IsRequestPermissionsPending(const int localGamerIndex)
{
	return m_NpAuth.IsRefreshingPermissions(localGamerIndex);
}

void rlNp::SetContentRestriction(const unsigned minAgeRating)
{
	SceNpContentRestriction param;
	memset(&param, 0x0, sizeof(param));

	param.size = sizeof(param);
	param.defaultAgeRestriction = (int8_t)minAgeRating;

	NET_ASSERTS_ONLY(int ret = )sceNpSetContentRestriction(&param);
	rlAssertf(ret >= 0, "sceNpSetContentRestriction - Failed to set content restriction '0x%08x'", ret);
}

bool rlNp::HasAvailabilityResult(const int localGamerIndex)
{
	return !m_NpAuth.IsRefreshingNpAvailability(localGamerIndex) && m_NpAuth.HasNpResult(localGamerIndex);
}

bool rlNp::RecheckNpAvailability(const int localGamerIndex)
{
	bool bSuccess = false;
	rtry
	{
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rcheck(m_State[localGamerIndex] == STATE_NP_UNAVAILABLE, catchall, );
		m_NpAuth.Reset(localGamerIndex);
		StartState(localGamerIndex, STATE_CHECK_NP_AVAILABILITY);
		bSuccess = true;
	}
	rcatchall
	{
	}

	rlDebug2("RecheckNpAvailability :: Index: %d, Success: %s", localGamerIndex, bSuccess ? "True" : "False");
	return bSuccess;
}

void rlNp::SetPresenceStatus(const int localGamerIndex, SceNpGamePresenceStatus status)
{
	if (m_PresenceStatus[localGamerIndex] != status)
	{
		// Update last presence change time
		rlDebug3("SetPresenceStatus :: Status: %s", status == SCE_NP_GAME_PRESENCE_STATUS_OFFLINE ? "OFFLINE" : "ONLINE");

		// update state
		m_PresenceStatus[localGamerIndex] = status;

		// fire event
		rlNpEventPresenceStatusChanged e(localGamerIndex, status == SCE_NP_GAME_PRESENCE_STATUS_OFFLINE ? 0 : rlNpEventPresenceStatusChanged::FLAG_ONLINE);
		m_Delegator.Dispatch(this, &e);
	}
}

// PURPOSE
// Returns the current NP presence status (ONLINE or OFFLINE)
int rlNp::GetGamePresenceStatus(const int localGamerIndex)
{
#if __BANK
	if (sm_bPresenceForcedOffline)
	{
		SetPresenceStatus(localGamerIndex, SCE_NP_GAME_PRESENCE_STATUS_OFFLINE);
		return SCE_OK;
	}
#endif

	SceNpOnlineId onlineId;
	int ret = sceNpGetOnlineId(this->GetUserServiceId(localGamerIndex), &onlineId);
	if (ret == SCE_OK)
	{
		SceNpGamePresenceStatus newStatus = SCE_NP_GAME_PRESENCE_STATUS_ONLINE;
		ret = sceNpGetGamePresenceStatus(&onlineId, &newStatus);
		rlAssertf(ret >= 0, "sceNpGetGamePresenceStatus - get game presence '0x%08x'", ret);
		SetPresenceStatus(localGamerIndex, newStatus);
		return ret;
	}
	else
	{
		SetPresenceStatus(localGamerIndex, SCE_NP_GAME_PRESENCE_STATUS_OFFLINE);
		return SCE_OK;
	}
}

#if RL_NP_SUPPORT_PLAY_TOGETHER
void rlNp::AddPendingPlayTogetherEvent(const rlNpEventPlayTogetherHost& evt)
{
    netDebug("AddPendingPlayTogetherEvent");
    m_PendingPlayTogetherEvent = evt;
    m_bPendingPlayTogetherEvent = true;

    // make an immediate check
    CheckPendingPlayTogetherEvent();
}

void rlNp::CheckPendingPlayTogetherEvent()
{
	if(!m_bPendingPlayTogetherEvent)
		return;

	if(!m_Initialized)
		return;

	int localGamerIndex = GetUserServiceIndex(m_PendingPlayTogetherEvent.m_UserId);
	if(localGamerIndex < 0)
		return;

	if(!IsOnline(localGamerIndex))
		return;

	rlDebug("CheckPendingPlayTogetherEvent :: PlayTogether - Dispatching Event");
	DispatchEvent(&m_PendingPlayTogetherEvent);

	// reset flag
	m_bPendingPlayTogetherEvent = false;
}
#endif

rlNpEnvironment rlNp::GetEnvironmentFromIssuerId(int issuerId)
{
	rlNpEnvironment env = RLNP_ENV_UNKNOWN;

	if (issuerId >= RLNP_ISSUER_PROD)
	{
		env = RLNP_ENV_NP;
	}
	else if (issuerId == RLNP_ISSUER_DEV)
	{
		env = RLNP_ENV_SP_INT;
	}
	else if (issuerId == RLNP_ISSUER_CERT)
	{
		env = RLNP_ENV_PROD_QA;
	}
	else if (issuerId == RLNP_ISSUER_UNKNOWN)
	{
		env = RLNP_ENV_UNKNOWN;
	}
	else
	{
		env = RLNP_ENV_UNRECOGNIZED;
	}

	return env;
}

rlNpEnvironment rlNp::GetEnvironmentFromNativeEnvironment(const rlNpEnvironment npEnv)
{
	// in the conversion we'll lose some NP-specific environments like RLNP_ENV_UNRECOGNIZED
	// the picked environment will be a combination of the native environment and any forced overrides
	rlEnvironment rlenv = rlCheckAndApplyForcedEnvironment(GetRlEnvironmentFromNpEnvironment(npEnv));
	return GetNpEnvironmentFromRlEnvironment(rlenv);
}

void 
rlNp::OnNpConnectionLost(const int localGamerIndex, const rlNpSignedOfflineReason reason)
{
	rlDebug("OnNpConnectionLost :: Index: %d, Reason: %d", localGamerIndex, reason);
	m_uNumUnackedHeartbeatPings = 0;

    Restart(localGamerIndex);

	rlNpEventOnlineStatusChanged e(localGamerIndex, rlNpOnlineStatus::RLNP_STATUS_OFFLINE, reason);
	DispatchEvent(&e);
}

void 
rlNp::OnNpActingUserChange(const int localGamerIndex)
{
	rlDebug("OnNpActingUserChange :: Index: %d", localGamerIndex);

	rlNpEventActingUserChanged e(localGamerIndex);
	DispatchEvent(&e);
}

int rlNp::GetUserServiceId(int localGamerIndex)
{
	if(RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return g_UserIds[localGamerIndex];
	}

	return SCE_USER_SERVICE_USER_ID_INVALID;
}

int rlNp::GetUserServiceIndex(s32 userId)
{
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if (g_UserIds[i] == userId)
		{
			return i;
		}
	}

	return RL_INVALID_GAMER_INDEX;
}

const int rlNp::GetAge(const int localGamerIndex, int* agePtr) 
{
	if (m_NpAuth.HasValidPermissions(localGamerIndex))
	{
		*agePtr = m_NpAuth.GetAge(localGamerIndex);	
		return SCE_OK;
	}
	else
	{
		*agePtr = -1;
		return *agePtr;
	}
}

const int rlNp::GetChatRestriction(const int localGamerIndex, bool* restriction)
{
	if (m_NpAuth.HasValidPermissions(localGamerIndex))
	{
		*restriction = m_NpAuth.GetChatRestrictionFlag(localGamerIndex);	
		return SCE_OK;
	}
	else
	{
		*restriction = false;
		return -1;
	}
}

const int rlNp::GetUgcRestriction(const int localGamerIndex, bool* restriction)
{
	if (m_NpAuth.HasValidPermissions(localGamerIndex))
	{
		*restriction = m_NpAuth.GetUgcRestriction(localGamerIndex);	
		return SCE_OK;
	}
	else
	{
		*restriction = false;
		return -1;
	}
}

const bool rlNp::HasPlayStationPlusSubscription(const int localGamerIndex)
{
	if (m_NpAuth.HasValidPermissions(localGamerIndex))
	{
		return m_NpAuth.IsPlusAuthorized(localGamerIndex);
	}

	return false;
}

void rlNp::SetPlayStationPlusRequiredForRealtimeMultiplayer(const int localGamerIndex, const bool isRequired)
{
	if (!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
		return;

	if (m_PlayStationPlusRequiredForRealtimeMultiplayer[localGamerIndex] != isRequired)
	{
		rlDebug("SetPlayStationPlusRequiredForRealtimeMultiplayer :: %s -> %s", m_PlayStationPlusRequiredForRealtimeMultiplayer[localGamerIndex] ? "True" : "False", isRequired ? "True" : "False");
		m_PlayStationPlusRequiredForRealtimeMultiplayer[localGamerIndex] = isRequired;
	}
}

const bool rlNp::IsPlayStationPlusRequiredForRealtimeMultiplayer(const int localGamerIndex)
{
	if (RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return m_PlayStationPlusRequiredForRealtimeMultiplayer[localGamerIndex];
	}

	return false;
}

const bool rlNp::CanAccessRealtimeMultiplayer(const int localGamerIndex)
{
	return !IsPlayStationPlusRequiredForRealtimeMultiplayer(localGamerIndex) || HasPlayStationPlusSubscription(localGamerIndex);
}

const char* rlNp::GetDefaultSessionImage()
{
	rlAssertf(m_DefaultSessionImage[0] != '\0', "Default Session/Invitation Image not set. PS4 requires a .JPEG image set for all session/invitations.");
	return m_DefaultSessionImage;
}

void rlNp::SetDefaultSessionImage(const char* imagePath)
{
	safecpy(m_DefaultSessionImage, imagePath);
}

bool rlNp::IsConnectedToPresence(const int localGamerIndex) 
{
	bool bPsnPresenceOnline = m_PresenceStatus[localGamerIndex] == SCE_NP_GAME_PRESENCE_STATUS_ONLINE;
	bool bRockstarOnline = netRelay::IsConnectedToPresence();

	if (bPsnPresenceOnline || bRockstarOnline)
	{
		return true;
	}

	return false;
}

bool rlNp::IsAnyPendingOnline()
{
	bool isPending = false;
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if (IsPendingOnline(i))
		{
			isPending = true;
			break;
		}
	}

	return isPending;
}

bool rlNp::IsPendingOnline(const int localGamerIndex)
{
	if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return m_State[localGamerIndex] == STATE_WAIT_FOR_AUTH || m_State[localGamerIndex] == STATE_REQUEST_AUTH ||
				m_State[localGamerIndex] == STATE_WAIT_PERMISSIONS || m_State[localGamerIndex] == STATE_REQUEST_PERMISSIONS ||
				m_State[localGamerIndex] == STATE_WAIT_NP_AVAILABILITY || m_State[localGamerIndex] == STATE_CHECK_NP_AVAILABILITY;
	}

	return false;
}

bool rlNp::GetCountryCode(const int localGamerIndex, char(&countryCode)[3])
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverifyall(g_UserIds[localGamerIndex] != SCE_USER_SERVICE_USER_ID_INVALID);

		SceNpOnlineId onlineId;
		int ret = sceNpGetOnlineId(this->GetUserServiceId(localGamerIndex), &onlineId);
		rverify(ret == SCE_OK, catchall, rlError("GetCountryCode :: sceNpGetOnlineId failed: 0x%08x", ret));

#if !__FINAL
		// command line override
		const char* npCountryParam;
		if (PARAM_npCountryCode.Get(npCountryParam))
		{
			countryCode[0] = npCountryParam[0];
			countryCode[1] = npCountryParam[1];
			countryCode[2] = '\0';
			return true;
		}
#endif

		SceNpCountryCode npCountryCode;
		int err = sceNpGetAccountCountry(&onlineId, &npCountryCode);
		rverify(err == SCE_OK, catchall, rlError("GetCountryCode :: sceNpGetAccountCountry failed - Error: 0x%08x", err));

		countryCode[0] = npCountryCode.data[0];
		countryCode[1] = npCountryCode.data[1];
		countryCode[2] = '\0';

		return true;
	}
	rcatchall
	{
		countryCode[0] = '\0';
		return false;
	}
}

#if !__NO_OUTPUT
const char* rlNp::GetSceRegionAsString(const rlNpSceRegion region)
{
	switch (region)
	{
	case rlNpSceRegion::Region_Invalid: return "Region_Invalid";
	case rlNpSceRegion::Region_SIEE: return "Region_SIEE";
	case rlNpSceRegion::Region_SIEA: return "Region_SIEA";
	case rlNpSceRegion::Region_SIEJ: return "Region_SIEJ";
	case rlNpSceRegion::Region_SIEAsia: return "Region_SIEAsia";
	case rlNpSceRegion::Region_Max: return "Region_Max";
	default: return "Region_Invalid";
	}
}
#endif

rlNpSceRegion rlNp::GetSceRegion(const int localGamerIndex)
{
	rtry
	{
		char npCountryCode[3];
		rcheckall(GetCountryCode(localGamerIndex, npCountryCode));

		const char* SIEE_COUNTRIES[] =
		{
			COUNTRY_CODE_UAE,
			COUNTRY_CODE_AUSTRIA,
			COUNTRY_CODE_AUSTRALIA,
			COUNTRY_CODE_BELGIUM,
			COUNTRY_CODE_BULGARIA,
			COUNTRY_CODE_BAHRAIN,
			COUNTRY_CODE_SWITZERLAND,
			COUNTRY_CODE_CYPRUS,
			COUNTRY_CODE_CZECH_REPUBLIC,
			COUNTRY_CODE_GERMANY,
			COUNTRY_CODE_DENMARK,
			COUNTRY_CODE_SPAIN,
			COUNTRY_CODE_FINLAND,
			COUNTRY_CODE_FRANCE,
			COUNTRY_CODE_UK,
			COUNTRY_CODE_GREECE,
			COUNTRY_CODE_CROATIA,
			COUNTRY_CODE_HUNGARY,
			COUNTRY_CODE_IRELAND,
			COUNTRY_CODE_ISRAEL,
			COUNTRY_CODE_INDIA,
			COUNTRY_CODE_ICELAND,
			COUNTRY_CODE_ITALY,
			COUNTRY_CODE_KUWAIT,
			COUNTRY_CODE_LEBANON,
			COUNTRY_CODE_LUXEMBOURG,
			COUNTRY_CODE_MALTA,
			COUNTRY_CODE_NETHERLANDS,
			COUNTRY_CODE_NORWAY,
			COUNTRY_CODE_NEW_ZEALAND,
			COUNTRY_CODE_OMAN,
			COUNTRY_CODE_POLAND,
			COUNTRY_CODE_PORTUGAL,
			COUNTRY_CODE_QATAR,
			COUNTRY_CODE_ROMANIA,
			COUNTRY_CODE_RUSSIA,
			COUNTRY_CODE_SAUDI_ARABIA,
			COUNTRY_CODE_SWEDEN,
			COUNTRY_CODE_SLOVENIA,
			COUNTRY_CODE_SLOVAKIA,
			COUNTRY_CODE_TURKEY,
			COUNTRY_CODE_UKRAINE,
			COUNTRY_CODE_SOUTH_AFRICA,
		};

		const char* SIEA_COUNTRIES[] =
		{
			COUNTRY_CODE_ARGENTINA,
			COUNTRY_CODE_BOLIVIA,
			COUNTRY_CODE_BRAZIL,
			COUNTRY_CODE_CANADA,
			COUNTRY_CODE_CHILE,
			COUNTRY_CODE_COLOMBIA,
			COUNTRY_CODE_COSTA_RICA,
			COUNTRY_CODE_ECUADOR,
			COUNTRY_CODE_GUATEMALA,
			COUNTRY_CODE_HONDURAS,
			COUNTRY_CODE_MEXICO,
			COUNTRY_CODE_NICARAGUA,
			COUNTRY_CODE_PANAMA,
			COUNTRY_CODE_PERU,
			COUNTRY_CODE_PARAGUAY,
			COUNTRY_CODE_EL_SALVADOR,
			COUNTRY_CODE_UNITED_STATES,
			COUNTRY_CODE_URUGUAY,
		};

		const char* SIEAsia_COUNTRIES[] =
		{
			COUNTRY_CODE_CHINA,
			COUNTRY_CODE_HONG_KONG,
			COUNTRY_CODE_INDONESIA,
			COUNTRY_CODE_KOREA,
			COUNTRY_CODE_MALAYSIA,
			COUNTRY_CODE_SINGAPORE,
			COUNTRY_CODE_THAILAND,
			COUNTRY_CODE_TAIWAN,
		};

		const char* SIEJ_COUNTRIES[] =
		{
			COUNTRY_CODE_JAPAN,
		};

		static const unsigned COUNTRY_CHARS = 2;

#define CHECK_COUNTRY_LIST(countryArray, region)								\
		for(unsigned i = 0; i < NELEM(countryArray); i++)						\
		{																		\
			if(strncmp(npCountryCode, countryArray[i], COUNTRY_CHARS) == 0)		\
				return region;													\
		}

		// check all country lists
		CHECK_COUNTRY_LIST(SIEE_COUNTRIES, rlNpSceRegion::Region_SIEE);
		CHECK_COUNTRY_LIST(SIEA_COUNTRIES, rlNpSceRegion::Region_SIEA);
		CHECK_COUNTRY_LIST(SIEJ_COUNTRIES, rlNpSceRegion::Region_SIEJ);
		CHECK_COUNTRY_LIST(SIEAsia_COUNTRIES, rlNpSceRegion::Region_SIEAsia);

		// no match
		return rlNpSceRegion::Region_Invalid;
	}
	rcatchall
	{
		return rlNpSceRegion::Region_Invalid;
	}
}

bool rlNp::GetDateOfBirth(const int localGamerIndex, u16& out_year, u8& out_month, u8& out_day)
{
	rtry
	{
		rverifyall(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex));
		rverifyall(g_UserIds[localGamerIndex] != SCE_USER_SERVICE_USER_ID_INVALID);

		SceNpOnlineId onlineId;
		int ret = sceNpGetOnlineId(this->GetUserServiceId(localGamerIndex), &onlineId);
		rverify(ret == SCE_OK, catchall, rlError("GetDateOfBirth :: sceNpGetOnlineId failed: 0x%08x", ret));

		SceNpDate dob = {0};
		int err = sceNpGetAccountDateOfBirth(&onlineId, &dob);
		rverify(err == SCE_OK, catchall, rlError("GetDateOfBirth :: sceNpGetAccountDateOfBirthA failed: 0x%08x", err));

		out_year = dob.year;
		out_month = dob.month;
		out_day = dob.day;
		return true;
	}
	rcatchall
	{
		out_year = 0;
		out_month = 0;
		out_day = 0;
		return false;
	}
}

void rlNp::OnServiceEvent(sysServiceEvent* evt)
{
	if(evt != NULL && evt->GetLevel() == sysServiceEvent::SYSTEM_EVENT)
	{
		switch(evt->GetType())
		{
		case sysServiceEvent::INVITATION_RECEIVED:
			{
				rlDebug("OnServiceEvent :: Service event invitation received");
				sysOrbisServiceEvent *orbisEvt = static_cast<sysOrbisServiceEvent*>(evt);
				if (orbisEvt->GetSytemServiceEvent().eventType == SCE_SYSTEM_SERVICE_EVENT_INVITATION)
				{
					SceNpSessionInvitationEventParam* invitationParam = reinterpret_cast<SceNpSessionInvitationEventParam*>(&orbisEvt->GetSytemServiceEvent().data.param);
					rlDebug("OnServiceEvent :: InvitationId: %s, SessionId: %s, Flags: 0x%x", invitationParam->invitationId.data, invitationParam->sessionId.data, invitationParam->flag);

					if ((invitationParam->flag & SCE_NP_SESSION_INVITATION_EVENT_FLAG_INVITATION) != 0)
					{
						if (invitationParam->invitationId.data[0] != '\0')
						{
							// invite accepted
							g_rlNp.GetBasic().HandleWebApiInviteAccepted(invitationParam->invitationId.data);
						}
						else
						{
							rlError("OnServiceEvent :: Invalid invitation data.");
						}
					}
					// this indicates that a user has performed an action that joins from session information rather than from an invitation.
					else
					{
						// convert to rlSceNpSessionId
						rlSceNpSessionId sessionId;
						sessionId.Reset(invitationParam->sessionId);

						// convert to rlSceNpOnlineId
						rlSceNpOnlineId onlineId;
						onlineId.Reset(invitationParam->onlineId);

						g_rlNp.GetBasic().HandleWebApiJoinSession(sessionId, g_rlNp.GetNpAccountIdFromNpOnlineId(onlineId), onlineId, rlNpGameIntentMemberType::Player);
					}
				}
			}
			break; 

#if RL_NP_SUPPORT_PLAY_TOGETHER
		case sysServiceEvent::PLAYTOGETHER_HOST:
			{
				rlDebug("OnServiceEvent :: Service event play together host received");
				sysOrbisServiceEvent *orbisEvt = static_cast<sysOrbisServiceEvent*>(evt);
				if (orbisEvt->GetSytemServiceEvent().eventType == SCE_SYSTEM_SERVICE_EVENT_PLAY_TOGETHER_HOST)
				{
					SceNpPlayTogetherHostEventParam* playTogetherParam = reinterpret_cast<SceNpPlayTogetherHostEventParam*>(&orbisEvt->GetSytemServiceEvent().data.param);		

					rlDebug("OnServiceEvent :: PlayTogether - UserId: %d, NumUsers: %u", playTogetherParam->userId, playTogetherParam->onlineIDListLen);
					
					rlSceNpOnlineId onlineIdList[RL_MAX_PLAY_TOGETHER_GROUP];
					for(unsigned i = 0; i < playTogetherParam->onlineIDListLen; i++)
					{
						rlDebug("OnServiceEvent :: PlayTogether - Invitee[%u]: %s", i, playTogetherParam->onlineIDList[i].data);
						onlineIdList[i].Reset(playTogetherParam->onlineIDList[i]);
					}

					// prop a pending play together event
					g_rlNp.AddPendingPlayTogetherEvent(rlNpEventPlayTogetherHost(playTogetherParam->userId,
																				 playTogetherParam->onlineIDListLen, 
																				 onlineIdList));
				}
			}
			break;
#endif

#if RSG_PROSPERO
        case sysServiceEvent::GAME_INTENT:
			{
				rlDebug("OnServiceEvent :: Handling GAME_INTENT");
				g_rlNp.ProcessGameIntentEvent();
			} 
			break;
#endif
		default:
			break;
		}
	}
}

bool rlNp::ProcessGameIntentEvent()
{
#if RSG_PROSPERO

    rtry
    {
        // The intent data is not part of the SceSystemServiceEvent so we have to get it manually.
        // With this implementation only the first callback here will get the intent data.
        SceNpGameIntentInfo intentInfo;
        sceNpGameIntentInfoInit(&intentInfo);

        int ret = SCE_OK;

#if !__NO_OUTPUT
        const char* dbgintent = nullptr;
        const char* dbginfo = nullptr;

		if( PARAM_npGameIntent.Get(dbgintent) && dbgintent[0] != 0 && PARAM_mpLauncherGameIntent.Get(dbginfo) )
		{
			const fiDevice *device = fiDevice::GetDevice( dbginfo );
			rlAssertf(device, "No device found for %s.", dbginfo);
			fiHandle hand = device->Open( dbginfo, true );
			rlAssertf(hand != fiHandleInvalid, "Failed to open %s, game was possibly not launched properly.", dbginfo);
			device->Read( hand, &intentInfo, sizeof( intentInfo ) );
			device->Close( hand );
			device->Delete( dbginfo );
		}
		else if (PARAM_npGameIntent.Get(dbgintent) && dbgintent[0] != 0 && PARAM_npGameIntentInfo.Get(dbginfo) && dbginfo[0] != 0)
		{
            safecpy(intentInfo.intentType, dbgintent);
            safecpy((char*)intentInfo.intentData.data, dbginfo, SCE_NP_GAME_INTENT_DATA_MAX_SIZE);
            intentInfo.userId = g_UserIds[0]; // Should be fine for our testing needs.

            PARAM_npGameIntent.Set(nullptr);
        }
        else
#endif
        {
            ret = sceNpGameIntentReceiveIntent(&intentInfo);
        }
        // According to the docs this is valid to fail for example when additional game intents have been pushed
        rcheck(ret == SCE_OK, catchall, rlError("ProcessGameIntentEvent :: sceNpGameIntentReceiveIntent failed - Error: 0x%08x", ret));

        rlNpGameIntentType intentType = rlNpGameIntentType::Undefined;
        rlNpGameIntentMemberType memberType = rlNpGameIntentMemberType::Undefined;
        rlSceNpSessionId sessionId;
        char activityId[rlNpEventGameIntentReceived::GAME_INTENT_ACTIVITY_ID_MAX_SIZE] = { 0 };
        char memberTypeString[rlNpEventGameIntentReceived::GAME_INTENT_MEMBER_TYPE_MAX_SIZE] = { 0 };
        char playerSessionId[rlSceNpSessionId::MAX_DATA_BUF_SIZE] = { 0 };

		// some game intent events might have specific handling
		bool fireGameIntentEvent = false;

        if (strncmp(intentInfo.intentType, "joinSession", sizeof(intentInfo.intentType)) == 0)
        {
            intentType = rlNpGameIntentType::JoinSession;

            ret = sceNpGameIntentGetPropertyValueString(&intentInfo.intentData, "playerSessionId", playerSessionId, sizeof(playerSessionId));
            rcheck(ret == SCE_OK, catchall, rlError("ProcessGameIntentEvent :: sceNpGameIntentGetPropertyValueString 'playerSessionId' failed - Error: 0x%08x", ret));

            ret = sceNpGameIntentGetPropertyValueString(&intentInfo.intentData, "memberType", memberTypeString, sizeof(memberTypeString));
            rcheck(ret == SCE_OK, catchall, rlError("ProcessGameIntentEvent :: sceNpGameIntentGetPropertyValueString 'memberType' failed - Error: 0x%08x", ret));

            sessionId.Reset(playerSessionId);

            if (strncmp(memberTypeString, "player", sizeof(memberTypeString)) == 0)
            {
                memberType = rlNpGameIntentMemberType::Player;
            }
            else if (strncmp(memberTypeString, "spectator", sizeof(memberTypeString)) == 0)
            {
                memberType = rlNpGameIntentMemberType::Spectator;
            }
            else
            {
                rverify(false, catchall, rlError("ProcessGameIntentEvent :: Unsupported MemberType: %s", memberTypeString));
            }

			rlDebug("ProcessGameIntentEvent :: JoinSession received. UserId: %d, SessionId: %s, MemberType: %s", intentInfo.userId, sessionId.data, memberTypeString);

			rlSceNpAccountId targetAccountId = RL_INVALID_NP_ACCOUNT_ID;
            rlSceNpOnlineId onlineId = rlSceNpOnlineId::RL_INVALID_ONLINE_ID;
            g_rlNp.GetBasic().HandleWebApiJoinSession(sessionId, targetAccountId, onlineId, memberType);
        }
        else if (strncmp(intentInfo.intentType, "launchActivity", sizeof(intentInfo.intentType)) == 0)
        {
            intentType = rlNpGameIntentType::LaunchActivity;

            ret = sceNpGameIntentGetPropertyValueString(&intentInfo.intentData, "activityId", activityId, sizeof(activityId));
            rcheck(ret == SCE_OK, catchall, rlError("ProcessGameIntentEvent :: sceNpGameIntentGetPropertyValueString 'activityId' failed - Error: 0x%08x", ret));

			rlDebug("ProcessGameIntentEvent :: LaunchActivity received. UserId: %d, ActivityId: %s", intentInfo.userId, activityId);
			fireGameIntentEvent = true;
        }
        else if (strncmp(intentInfo.intentType, "launchMultiplayerActivity", sizeof(intentInfo.intentType)) == 0)
        {
            intentType = rlNpGameIntentType::LaunchMultiplayerActivity;

            ret = sceNpGameIntentGetPropertyValueString(&intentInfo.intentData, "activityId", activityId, sizeof(activityId));
            rcheck(ret == SCE_OK, catchall, rlError("ProcessGameIntentEvent :: sceNpGameIntentGetPropertyValueString 'activityId' failed - Error: 0x%08x", ret));

            char playerSessionId[rlSceNpSessionId::MAX_DATA_BUF_SIZE];
            ret = sceNpGameIntentGetPropertyValueString(&intentInfo.intentData, "playerSessionId", playerSessionId, sizeof(playerSessionId));
            rcheck(ret == SCE_OK, catchall, rlError("ProcessGameIntentEvent :: sceNpGameIntentGetPropertyValueString 'playerSessionId' failed - Error: 0x%08x", ret));

			sessionId.Reset(playerSessionId);

			rlDebug("ProcessGameIntentEvent :: LaunchMultiplayerActivity received. UserId: %d, SessionId: %s, ActivityId: %s", intentInfo.userId, sessionId.data, activityId);
			fireGameIntentEvent = true; 
        }
        else
        {
            rverify(false, catchall, rlError("ProcessGameIntentEvent :: Unsupported IntentType: %s", intentInfo.intentType));
        }

		if(fireGameIntentEvent)
		{
			rlNpEventGameIntentReceived e(intentType, memberType, sessionId, activityId);
			DispatchEvent(&e);
		}
        
        return true;
    }
    rcatchall
    {
    }

#endif //RSG_PROSPERO

    return false;
}

}   //namespace rage

#endif
