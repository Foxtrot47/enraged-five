// 
// rline/rlachievement.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlachievement.h"
#include "rldiag.h"
#include "diag/seh.h"
//#include "grcore/image.h"
#include "rline/ros/rlros.h"
#include "rline/scachievements/rlscachievements.h"
#include "string/string.h"
#include "system/new.h"
#include "system/memory.h"

#if RSG_NP
#include "rlnp.h"
#include "rlnptrophy.h"
#endif
#if RSG_DURANGO
#include "net/task.h"
#include "rline/durango/rlxbl_interface.h"
#endif
#if RSG_PC
#include "rlpc.h"
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, achievements);
#undef __rage_channel
#define __rage_channel rline_achievements

rlAchievementInfo::rlAchievementInfo()
{
    this->Clear();
}

rlAchievementInfo::~rlAchievementInfo()
{
}

int
rlAchievementInfo::GetId() const
{
    return m_Id;
}

int
rlAchievementInfo::GetCred() const
{
    return m_Cred;
}

const char*
rlAchievementInfo::GetLabel() const
{
    return m_Label;
}

const char*
rlAchievementInfo::GetDescription() const
{
    return m_Description;
}

bool
rlAchievementInfo::IsAchieved() const
{
    return m_Achieved;
}

//protected:

void
rlAchievementInfo::Clear()
{
    m_Id = -1;
    m_Cred = 0;
    m_Label[0] = '\0';
    m_Description[0] = '\0';
    m_Achieved = false;
}

void
rlAchievementInfo::Reset(const int id,
                    const int cred,
                    const char* label,
                    const char* description,
                    const bool achieved)
{
    m_Id = id;
    m_Cred = cred;
    safecpy(m_Label, label, sizeof(m_Label));
    safecpy(m_Description, description, sizeof(m_Description));
    m_Achieved = achieved;
}

///////////////////////////////////////////////////////////////////////////////
// rlAchievementInfoEx
///////////////////////////////////////////////////////////////////////////////

rlAchievementInfoEx::rlAchievementInfoEx()
: m_Image(NULL)
{
	ClearEx();
}

rlAchievementInfoEx::~rlAchievementInfoEx()
{
	ClearEx();
}

bool rlAchievementInfoEx::IsValid() const
{
	return (GetId() >= 0) && (m_Type > ACHIEVEMENT_TYPE_INVALID) && (m_Type <= ACHIEVEMENT_TYPE_OTHER);
}

rlAchievementInfoEx::AchievementType 
rlAchievementInfoEx::GetType() const
{
	return (rlAchievementInfoEx::AchievementType)m_Type;
}

const char* 
rlAchievementInfoEx::GetUnachievedString() const
{
	return m_UnachievedString;
}

grcImage* 
rlAchievementInfoEx::GetImage() const
{
	return m_Image;
}

bool 
rlAchievementInfoEx::ShowUnachieved() const
{
	return m_ShowUnachieved;
}

bool 
rlAchievementInfoEx::IsHidden() const
{
	return !ShowUnachieved();
}

time_t 
rlAchievementInfoEx::GetAchievedTime() const
{
	return m_AchievedTime;
}

void 
rlAchievementInfoEx::ClearEx()
{
	Clear();
	if(m_Image != NULL)
	{
//		m_Image->Release();
		m_Image = NULL;
	}
	m_Type = ACHIEVEMENT_TYPE_INVALID;
	m_UnachievedString[0] = '\0';
	m_ShowUnachieved = false;
	m_AchievedTime = 0;
}

void
rlAchievementInfoEx::ResetEx(const u32 id,
							 const AchievementType type,
							 const u32 cred,
							 const char* label,
							 const char* description,
							 const char* unachievedString,
							 grcImage* image,
							 const bool showUnachieved,
							 const bool isAchieved,
							 const time_t achievedTime)
{
	ClearEx();
	Reset(id, cred, label, description, isAchieved);
	rlAssert((type > ACHIEVEMENT_TYPE_INVALID) && (type <= ACHIEVEMENT_TYPE_OTHER));
	m_Type = type;
    safecpy(m_UnachievedString, unachievedString, sizeof(m_UnachievedString));
	m_Image = image;
	m_ShowUnachieved = showUnachieved;
	m_AchievedTime = achievedTime;
}

void rlAchievement::SyncWithSocialClubRead()
{
	int localGamerIndex = m_ReadCommand->m_RequestingGamer.GetLocalIndex();
	bool localIsTarget = m_ReadCommand->m_RequestingGamer == m_ReadCommand->m_TargetGamer;

	if (localIsTarget && RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex) && rlRos::IsOnline(localGamerIndex))
	{
		unsigned numAchievements = *m_ReadCommand->m_NumAchievements;

		if (numAchievements)
		{
			if (!rlScAchievements::Synchronize(localGamerIndex,  m_ReadCommand->m_Achievements, numAchievements, NULL))  //No status; it's fire-and-forget
			{
				rlError("Call to rlScAchievements::Synchronize failed");
			}
		}
	}
}

void rlAchievement::SyncWithSocialClubWrite()
{
	//We are online and share, synchronize ROS.
	int localGamerIndex = m_WriteCommand->m_GamerInfo.GetLocalIndex();

	if (rlRos::IsOnline(localGamerIndex))
	{
		rlDebug("Notifying ROS of awarded achievement");

		if (!rlScAchievements::AwardAchievement(localGamerIndex, m_WriteCommand->m_AchievementId, NULL, NULL))  //No status; it's fire-and-forget
		{
			rlError("Call to rlScAchievements::AwardAchievement failed");
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// rlAchievement::Worker
///////////////////////////////////////////////////////////////////////////////

rlAchievement::Worker::Worker()
: m_Ach(0)
{
}

bool
rlAchievement::Worker::Start(rlAchievement* ach, const int cpuAffinity)
{
	// Uses rlWorker::Start with fastStart = true, which doesn't wait for the thread to signal that is has started
    const bool success = this->rlWorker::Start("[RAGE] rlAchievement Worker",
                                                sysIpcMinThreadStackSize,
                                                cpuAffinity, true);
    if(success)
    {
        m_Ach = ach;
    }
    return success;
}

bool
rlAchievement::Worker::Stop()
{
    const bool success = this->rlWorker::Stop();
    m_Ach = 0;

    return success;
}

//private:

void
rlAchievement::Worker::Perform()
{
    bool success = false;

    if(m_Ach->m_ReadCommand)
    {
        success = m_Ach->NativeRead();

#if !RLROS_SC_PLATFORM
        if (success)
        {
            //If we were reading for a local gamer, are online, and can share then synchronize ROS.
			m_Ach->SyncWithSocialClubRead();
        }
#endif //!RLROS_SC_PLATFORM
    }
    else if(m_Ach->m_WriteCommand)
    {
        success = m_Ach->NativeWrite();

#if !RLROS_SC_PLATFORM
        if (success)
        {
			m_Ach->SyncWithSocialClubWrite();
        }
#endif //!RLROS_SC_PLATFORM
    }
#if RSG_PC && __DEV
	else if(m_Ach->m_DeleteCommand)
	{
		success = m_Ach->NativeDelete();
	}
#endif
	else
    {
        rlAssert(false);
    }

    SYS_CS_SYNC(m_Ach->m_Cs);

#if RSG_DURANGO
	if (m_Ach->m_WriteCommand)
#endif
	{
		m_Ach->m_ReadCommand = 0;
		m_Ach->m_WriteCommand = 0;

#if RSG_PC && __DEV
		m_Ach->m_DeleteCommand = 0;
#endif

		if (NULL != m_Ach->m_Status)
		{
			success ? m_Ach->m_Status->SetSucceeded() : m_Ach->m_Status->SetFailed();
		}

		m_Ach->m_Status = NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////
// rlAchievement
///////////////////////////////////////////////////////////////////////////////

rlAchievement::rlAchievement()
: m_ReadCommand(0)
, m_WriteCommand(0)
#if RSG_PC && __DEV
, m_DeleteCommand(0)
#endif
, m_Status(NULL)
, m_Initialized(false)
{
}

rlAchievement::~rlAchievement()
{
    this->Shutdown();
}

bool
rlAchievement::Init(const int cpuAffinity)
{
    bool success = false;

    rtry
    {
        rverify(!m_Initialized, catchall,);

        rlAssert(!this->Pending());

        rverify(m_Worker.Start(this, cpuAffinity),
                catchall,
                rlError("Error starting achievement worker"));

        rlDebug("Started achievement worker");

        m_Status = NULL;

        success = m_Initialized = true;
    }
    rcatchall
    {
    }

    return success;
}

void rlAchievement::Update()
{
#if RSG_DURANGO
	// Only Durango Acheivements are Async for now
	// We're waiting for a result and the read command has finished processing.
	if (m_Status != NULL && m_Status->Pending() && 
		m_ReadCommand && (m_ReadCommand->m_ReadStatus.Succeeded() || m_ReadCommand->m_ReadStatus.Failed()))
	{
		// Sync with social club
		if (m_ReadCommand->m_ReadStatus.Succeeded())
		{
			SyncWithSocialClubRead();
		}

		m_Status->SetStatus(m_ReadCommand->m_ReadStatus.GetStatus());
		m_ReadCommand = 0;
		m_Status = NULL;
	}
#endif
}

void
rlAchievement::Shutdown()
{
    if(m_Initialized)
    {
        rlAssert(!this->Pending());

        rlDebug("Stopping achievement worker...");
        rlVerify(m_Worker.Stop());
        rlDebug("Stopped achievement worker");

        m_Status = NULL;

        m_Initialized = false;
    }
}

bool
rlAchievement::Read(const rlGamerInfo& requestingGamer,
					const rlGamerHandle& targetGamerHandle,
					const DetailFlags detailFlags,
					rlAchievementInfo* achievements,
					const int firstAchievementId,
					const int maxAchievements,
					int* numAchievements,
					netStatus* status)
{
    SYS_CS_SYNC(m_Cs);

    rlAssert(requestingGamer.IsLocal());
	rlAssert(detailFlags >= 0 && detailFlags <= ACHIEVEMENT_DETAILS_ALL);

    bool success = false;

    *numAchievements = 0;

    rlDebug("Attempting to read achievements...");

    if(rlVerify(!this->Pending()))
    {
        m_ReadCommand = new (m_RC) ReadCommand;
        m_ReadCommand->m_RequestingGamer = requestingGamer.GetGamerHandle();
		m_ReadCommand->m_TargetGamer = targetGamerHandle;
		m_ReadCommand->m_DetailFlags = detailFlags;
        m_ReadCommand->m_Achievements = achievements;
		m_ReadCommand->m_FirstAchievementId = firstAchievementId;
        m_ReadCommand->m_MaxAchievements = maxAchievements;
        m_ReadCommand->m_NumAchievements = numAchievements;
		m_ReadCommand->m_GamerIndex = requestingGamer.GetLocalIndex();

        *numAchievements = 0;

        m_Status = status ? status : &m_MyStatus;
        m_Status->SetPending();
        m_Worker.Wakeup();

        success = true;
    }
    else if(status)
    {
        status->SetPending();
        status->SetFailed();
    }

    return success;
}

bool
rlAchievement::Read(const rlGamerInfo& requestingGamer,
                    const rlGamerInfo& targetGamer,
                    rlAchievementInfo* achievements,
                    const int maxAchievements,
                    int* numAchievements,
                    netStatus* status)
{
    SYS_CS_SYNC(m_Cs);

    rlAssert(requestingGamer.IsLocal());

    bool success = false;

    *numAchievements = 0;

    rlDebug("Attempting to read achievements for gamer:\"%s\"...", targetGamer.GetName());

    if(rlVerify(!this->Pending()))
    {
        m_ReadCommand = new (m_RC) ReadCommand;
        m_ReadCommand->m_RequestingGamer = requestingGamer.GetGamerHandle();
		m_ReadCommand->m_TargetGamer = targetGamer.GetGamerHandle();
		m_ReadCommand->m_DetailFlags = ACHIEVEMENT_DETAILS_ALL;
        m_ReadCommand->m_Achievements = achievements;
		m_ReadCommand->m_FirstAchievementId = 0;
        m_ReadCommand->m_MaxAchievements = maxAchievements;
        m_ReadCommand->m_NumAchievements = numAchievements;
		m_ReadCommand->m_GamerIndex = requestingGamer.GetLocalIndex();

        *numAchievements = 0;

        m_Status = status ? status : &m_MyStatus;
        m_Status->SetPending();
        m_Worker.Wakeup();

        success = true;
    }
    else if(status)
    {
        status->SetPending();
        status->SetFailed();
    }

    return success;
}

bool
rlAchievement::Write(const rlGamerInfo& gamerInfo,
                    const int achievementId,
                    netStatus* status)
{
    SYS_CS_SYNC(m_Cs);

    rlAssert(gamerInfo.IsLocal());

    bool success = false;

    rlDebug("Attempting to write achievement:%d for gamer:\"%s\"...",
                achievementId,
                gamerInfo.GetName());

    if(rlVerify(!this->Pending()))
    {
        m_WriteCommand = new (m_WC) WriteCommand;
        m_WriteCommand->m_GamerInfo = gamerInfo;
        m_WriteCommand->m_AchievementId = achievementId;
		m_WriteCommand->m_GamerIndex = gamerInfo.GetLocalIndex();

        m_Status = status ? status : &m_MyStatus;
        m_Status->SetPending();
        m_Worker.Wakeup();

        success = true;
    }
    else if(status)
    {
        status->SetPending();
        status->SetFailed();
    }

    return success;
}

#if RSG_PC && __DEV
bool
rlAchievement::DeleteAllAchievements(const rlGamerInfo& gamerInfo,
									 netStatus* status)
{
	SYS_CS_SYNC(m_Cs);

	rlAssert(gamerInfo.IsLocal());

	bool success = false;

	rlDebug("Attempting to delete all achievements for gamer:\"%s\"...",
			 gamerInfo.GetName());

	if(rlVerify(!this->Pending()))
	{
		m_DeleteCommand = new (m_DC) DeleteCommand;
		m_DeleteCommand->m_GamerInfo = gamerInfo;

		m_Status = status ? status : &m_MyStatus;
		m_Status->SetPending();
		m_Worker.Wakeup();

		success = true;
	}
	else if(status)
	{
		status->SetPending();
		status->SetFailed();
	}

	return success;
}
#endif

bool
rlAchievement::Pending() const
{
#if RSG_DURANGO
	return (NULL != m_Status && m_Status->Pending()) || (m_ReadCommand && m_ReadCommand->m_ReadStatus.Pending());
#else
	return NULL != m_Status;
#endif
}

//private:

#if RSG_NP

bool
rlAchievement::NativeRead()
{
	rlAssert(m_ReadCommand);

	rlDebug("Reading achievements...");

	return g_rlNp.GetTrophy().ReadAchievements(m_ReadCommand->m_Achievements,
											   m_ReadCommand->m_MaxAchievements,
											   m_ReadCommand->m_NumAchievements,
											   m_ReadCommand->m_GamerIndex);
}

bool
rlAchievement::NativeWrite()
{
	rlAssert(m_WriteCommand);

	rlDebug("Writing achievement:%d...", m_WriteCommand->m_AchievementId);

	int platinumAchievementId;

	return g_rlNp.GetTrophy().WriteAchievement(m_WriteCommand->m_AchievementId,
											   &platinumAchievementId,
											   m_WriteCommand->m_GamerIndex);
}

#elif RSG_PC

rlAchievementInfoEx::AchievementType 
ConvertRgscTypeToRageType(rgsc::IAchievement::AchievementType rgscType)
{
	rlAchievementInfoEx::AchievementType type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_INVALID;

	switch(rgscType)
	{
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_INVALID:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_INVALID;
		break;
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_COMPLETION:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_COMPLETION;
			break;
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_LEVELING:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_LEVELING;
			break;
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_UNLOCK:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_UNLOCK;
			break;
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_EVENT:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_EVENT;
			break;
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_TOURNAMENT:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_TOURNAMENT;
			break;
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_CHECKPOINT:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_CHECKPOINT;
			break;
	case rgsc::IAchievement::ACHIEVEMENT_TYPE_OTHER:
		type = rlAchievementInfoEx::ACHIEVEMENT_TYPE_OTHER;
			break;
	}

	return type;
}

bool
rlAchievement::NativeRead()
{
	bool success = true;

	rtry
	{
		rverify(m_ReadCommand, catchall, );

		rlDebug("Reading achievements...");

		rlGamerHandle gamerHandle = m_ReadCommand->m_TargetGamer;

		rverify(gamerHandle.IsValid(), catchall, );

		rverify(g_rlPc.IsInitialized(), catchall, );

		// convert rage flags to rgsc flags
		DetailFlags flags = m_ReadCommand->m_DetailFlags;
		u32 intFlags = 0;
		if(flags & ACHIEVEMENT_DETAILS_STRINGS)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_STRINGS;
		}

		if(flags & ACHIEVEMENT_DETAILS_IMAGE)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_IMAGE;
		}

		if(flags & ACHIEVEMENT_DETAILS_IS_AWARDED)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_IS_AWARDED;
		}

		if(flags & ACHIEVEMENT_DETAILS_TIMESTAMP)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_TIMESTAMP;
		}

		rverify(intFlags > 0, catchall, );

		bool extendedInfo = m_ReadCommand->m_Achievements[0].RTTI() > 0;

		rgsc::AchievementId firstAchievementId = (rgsc::AchievementId)m_ReadCommand->m_FirstAchievementId;
		u32 maxAchievementsToRead = m_ReadCommand->m_MaxAchievements;
		u32 numBytesRequired = 0;
		void* handle = NULL;
		g_rlPc.GetAchievementManager()->CreateAchievementEnumerator(rgsc::IID_IAchievementListLatestVersion,
																	NULL,
																	(rgsc::RockstarId) gamerHandle.GetRockstarId(),
																	(rgsc::IAchievement::DetailFlags) intFlags,
																	firstAchievementId,
																	maxAchievementsToRead,
																	&numBytesRequired,
																	&handle);

		void* achievementBuf = RL_ALLOCATE(rlAchievement, numBytesRequired);

		rverify(achievementBuf,
				catchall,
				rlError("Error allocating achievement buffer"));

		rgsc::IAchievementList* listInterface = NULL;
		g_rlPc.GetAchievementManager()->EnumerateAchievements(handle,
															  achievementBuf,
															  numBytesRequired,
															  &listInterface);

		rgsc::IAchievementListLatestVersion* list = (rgsc::IAchievementListLatestVersion*)listInterface;

		*m_ReadCommand->m_NumAchievements = (int) list->GetNumAchievements();

		for(unsigned i = 0; i < list->GetNumAchievements(); i++)
		{
			rgsc::IAchievementLatestVersion* achievement = list->GetAchievement(i);

			if(extendedInfo)
			{
				rlAchievementInfoEx* rageAchievement = &(((rlAchievementInfoEx*)m_ReadCommand->m_Achievements)[i]);
				rlAchievementInfoEx::AchievementType type = ConvertRgscTypeToRageType(achievement->GetType());

				rageAchievement->ResetEx(achievement->GetId(),
										 type,
										 achievement->GetPoints(),
										 achievement->GetLabel(),
										 achievement->GetDescription(),
										 achievement->GetUnachievedString(),
										 NULL,
										 achievement->ShowUnachieved(),
										 achievement->IsAchieved(),
										 achievement->GetAchievedTime());
			}
			else
			{
				rlAchievementInfo* rageAchievement = &m_ReadCommand->m_Achievements[i];

				rageAchievement->Reset(achievement->GetId(),
									   achievement->GetPoints(),
									   achievement->GetLabel(),
									   achievement->GetDescription(),
									   achievement->IsAchieved());
			}
		}

		if(achievementBuf)
		{
			RL_FREE(achievementBuf);
			achievementBuf = NULL;
		}
		
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool
rlAchievement::NativeWrite()
{
	bool success = false;

	rlDebug("Writing achievement:%d...", m_WriteCommand->m_AchievementId);

	rtry
	{
		rverify(m_WriteCommand, catchall, );

		rverify(m_WriteCommand->m_GamerInfo.IsLocal() && m_WriteCommand->m_GamerInfo.IsSignedIn(), catchall, );

		rverify(g_rlPc.IsInitialized(), catchall, );

		rgsc::AchievementId achievementId = (rgsc::AchievementId) m_WriteCommand->m_AchievementId;

		HRESULT hr = g_rlPc.GetAchievementManager()->WriteAchievement(achievementId);

		success = hr == ERROR_SUCCESS;
	}
	rcatchall
	{

	}

    return success;
}

#if RSG_PC && __DEV
bool
rlAchievement::NativeDelete()
{
	bool success = false;

	rlDebug("Deleting achievements...");

	rtry
	{
		rverify(m_DeleteCommand, catchall, );

		rverify(m_DeleteCommand->m_GamerInfo.IsLocal() && m_DeleteCommand->m_GamerInfo.IsSignedIn(), catchall, );

		rverify(g_rlPc.IsInitialized(), catchall, );

		HRESULT hr = g_rlPc.GetAchievementManager()->DeleteAllAchievements();

		success = hr == ERROR_SUCCESS;
	}
	rcatchall
	{

	}

	return success;
}
#endif

#elif RSG_DURANGO

bool rlAchievement::NativeRead()
{
	int localGamerIndex = m_ReadCommand->m_RequestingGamer.GetLocalIndex();

	return g_rlXbl.GetAchivementManager()->ReadAchievements(localGamerIndex,
		m_ReadCommand->m_Achievements,\
		m_ReadCommand->m_MaxAchievements,
		m_ReadCommand->m_NumAchievements,
		&m_ReadCommand->m_ReadStatus);
}

bool rlAchievement::NativeWrite()
{
	return g_rlXbl.GetAchivementManager()->WriteAchievement(m_WriteCommand->m_GamerIndex, m_WriteCommand->m_AchievementId);
}

#else

bool
rlAchievement::NativeRead()
{
    rlWarning("Achievements not supported on this platform");
    return false;
}

bool
rlAchievement::NativeWrite()
{
    rlWarning("Achievements not supported on this platform");

    return false;
}

#endif

}   //namespace rage
