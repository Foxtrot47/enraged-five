// 
// rline/rlsessioninfo.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsessioninfo.h"
#include "data/bitbuffer.h"
#include "data/base64.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "rldiag.h"
#include "rlnpwebapi.h"
#include "rlpc.h"
#include "scpresence/rlscpresence.h"
#include "string/string.h"
#include "system/endian.h"
#include "system/memory.h"

#include <stdio.h>

#if RSG_DURANGO
#include "system/xtl.h"
#elif RSG_NP
#include <np.h>
#endif

namespace rage
{

const rlSessionToken rlSessionToken::INVALID_SESSION_TOKEN;
const rlSessionInfo rlSessionInfo::INVALID_SESSION_INFO;

//Make sure our ToString() buffer spec is large enough
CompileTimeAssert(rlSessionInfo::TO_STRING_BUFFER_SIZE <= RLSC_PRESENCE_STRING_MAX_SIZE);

#if RSG_PC
// we send the gsinfo attribute to the SC DLL, make sure it fits
CompileTimeAssert(rlSessionInfo::TO_STRING_BUFFER_SIZE <= rgsc::RGSC_PRESENCE_STRING_MAX_SIZE);
#endif

#if RSG_NP
// we store the session info in the presence blob, so make sure it fits.
// PS4 docs: User's freely defined data for the in-game presence for the current platform.
//	Binary data up to 128 bytes encoded in Base64 (up to 172 ASCII characters).
CompileTimeAssert(rlSessionInfo::NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES <= RL_PRESENCE_MAX_BUF_SIZE);
CompileTimeAssert(rlSessionInfo::NP_PRESENCE_DATA_TO_STRING_BUFFER_SIZE <= SCE_NP_BASIC_MAX_PRESENCE_BASE64_SIZE);
#endif

#if __STEAM_BUILD
// we store the session info in the presence blob, so make sure it fits
CompileTimeAssert(rlSessionInfo::TO_STRING_BUFFER_SIZE <= RL_PRESENCE_MAX_BUF_SIZE);
#endif

rlSessionInfo::rlSessionInfo()
{
    this->Clear();
}

void
rlSessionInfo::Clear()
{
#if RSG_DURANGO
	m_SessionHandle.Clear();
#elif RSG_PC
	m_SessionId = 0;
#elif RSG_ORBIS
	memset(&m_WebApiSessionId , 0, sizeof(m_WebApiSessionId));
#endif

#if RSG_DURANGO || RSG_NP || RSG_PC
	m_SessionToken.Clear();
	m_HostPeerAddr.Clear();
#endif
}

const char*
rlSessionInfo::ToString(char* buf, const unsigned sizeofBuf, unsigned* size) const
{
    if(rlVerifyf(sizeofBuf >= TO_STRING_BUFFER_SIZE, "Destination buffer is too small"))
    {
		u8 exportBuf[TO_STRING_BUFFER_SIZE] = {0};
        unsigned exportSize;
        if(rlVerifyf(this->Export(exportBuf, sizeof(exportBuf), &exportSize),
                    "Error exporting rlSessionInfo")
            && rlVerifyf(datBase64::GetMaxEncodedSize(exportSize) <= sizeofBuf,
                        "Destination buffer is too small")
            && rlVerifyf(datBase64::Encode(exportBuf, exportSize, buf, sizeofBuf, size),
                        "Error encoding rlSessionInfo"))
        {
            return buf;
        }
    }

    return NULL;
}

bool
rlSessionInfo::FromString(const char* buf, unsigned* size, bool failSilently)
{
	u8 decodeBuf[TO_STRING_BUFFER_SIZE] = {0};
    unsigned decodeSize;
    unsigned importSize;

	bool success = false; 

	rtry
	{
		rcheck(datBase64::GetMaxDecodedSize(buf) <= sizeof(decodeBuf), catchall, if(failSilently) { rlError("Destination buffer is too small"); } else { rlAssertf(0, "Destination buffer is too small"); });
		rcheck(datBase64::Decode(buf, sizeof(decodeBuf), decodeBuf, &decodeSize), catchall, if(failSilently) { rlError("Error decoding rlSessionInfo"); } else { rlAssertf(0, "Error decoding rlSessionInfo"); });
		rcheck(this->Import(decodeBuf, decodeSize, &importSize, failSilently), catchall, if(failSilently) { rlError("Error importing rlSessionInfo"); } else { rlAssertf(0, "Error importing rlSessionInfo"); });

		success = true; 
	}
	rcatchall
	{

	}

    if(size)
    {
        *size = success ? ustrlen(buf) : 0;
    }

    return success;
}

void
rlSessionInfo::ClearNonAdvertisableData()
{
	this->m_HostPeerAddr.ClearNonAdvertisableData();
}

#if RSG_DURANGO

bool
rlSessionInfo::IsValid() const
{
	return m_SessionToken.IsValid()
		&& m_HostPeerAddr.IsValid();
}

bool 
rlSessionInfo::IsClear() const
{
	return !m_SessionToken.IsValid()
		&& !m_HostPeerAddr.IsValid();
}

const rlSessionToken
rlSessionInfo::GetToken() const
{
	return m_SessionToken;
}

const rlPeerAddress&
rlSessionInfo::GetHostPeerAddress() const
{
	return m_HostPeerAddr;
}

const rlXblSessionHandle& 
rlSessionInfo::GetSessionHandle() const
{
	return m_SessionHandle;
}

void
rlSessionInfo::Init(const rlXblSessionHandle& sessionHandle,
					const rlSessionToken& sessionToken,
					const rlPeerAddress &hostPeerAddr)
{
	m_SessionHandle = sessionHandle;
	m_SessionToken = sessionToken;
	m_HostPeerAddr = hostPeerAddr;
}

bool
rlSessionInfo::Export(void* buf,
					  const unsigned sizeofBuf,
					  unsigned* size) const
{
	// if you change the exported size, make sure
	// to change MAX_EXPORTED_SIZE_IN_BYTES

	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	netPeerAddress peerAddr = m_HostPeerAddr;
	peerAddr.ClearNonAdvertisableData();

	const bool success = bb.SerUser(m_SessionHandle)
						 && bb.WriteUns(m_SessionToken.m_Value, 64)
						 && bb.SerUser(peerAddr);

	rlAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool
rlSessionInfo::Import(const void* buf,
					  const unsigned sizeofBuf,
					  unsigned* size,
					  bool UNUSED_PARAM(failSilently))
{
    this->Clear();

    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	const bool success = bb.SerUser(m_SessionHandle)
						 && bb.ReadUns(m_SessionToken.m_Value, 64)
						 && bb.SerUser(m_HostPeerAddr);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

bool
rlSessionInfo::operator==(const rlSessionInfo& that) const
{
	return ((that.m_SessionToken == m_SessionToken) && (m_SessionToken.IsValid()));
}

bool
rlSessionInfo::operator!=(const rlSessionInfo& that) const
{
	return (that.m_SessionToken != m_SessionToken);
}

#elif RSG_PC || RSG_NP

bool
rlSessionInfo::IsValid() const
{
	return m_SessionToken.IsValid()
		&& m_HostPeerAddr.IsValid();
}

bool 
rlSessionInfo::IsClear() const
{
	return !m_SessionToken.IsValid();
}

const rlSessionToken
rlSessionInfo::GetToken() const
{
	return m_SessionToken;
}

#if RSG_PC
const u64 rlSessionInfo::GetSessionId() const
{
	return m_SessionId;
}
#endif

const rlPeerAddress&
rlSessionInfo::GetHostPeerAddress() const
{
	return m_HostPeerAddr;
}

void
rlSessionInfo::Init(const u64 sessionId,
					const rlSessionToken& sessionToken,
					const rlPeerAddress &hostPeerAddr)
{
#if RSG_ORBIS
	m_WebApiSessionId.Clear();
#endif

#if RSG_PC
	rlAssert(sessionId != 0);
	m_SessionId = sessionId;
#endif
	m_SessionToken = sessionToken;
	m_HostPeerAddr = hostPeerAddr;
}

bool
rlSessionInfo::Export(void* buf,
					  const unsigned sizeofBuf,
					  unsigned* size) const
{
	// if you change the exported size, make sure
	// to change MAX_EXPORTED_SIZE_IN_BYTES

	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	// the exported session info never contains the player's real address
	netPeerAddress peerAddr = m_HostPeerAddr;
	peerAddr.ClearNonAdvertisableData();

	const bool success = bb.SerUser(peerAddr)
						 WIN32PC_ONLY(&& bb.WriteUns(m_SessionId, 64))
						 && bb.WriteUns(m_SessionToken.m_Value, 64)
						 ORBIS_ONLY(&& bb.SerUser(m_WebApiSessionId));

	rlAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool
rlSessionInfo::Import(const void* buf,
					  const unsigned sizeofBuf,
					  unsigned* size,
					  bool failSilently)
{
	this->Clear();

    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	bool success = false; 

	rtry
	{
		rcheck(bb.SerUser(m_HostPeerAddr), catchall, if(failSilently) { rlError("Error importing HostPeerAddr. Buffer: %s, Size: %u", buf, sizeofBuf); } else { rlAssertf(0, "Error importing HostPeerAddr. Buffer: %s, Size: %u", buf, sizeofBuf); });
#if RSG_PC
		rcheck(bb.ReadUns(m_SessionId, 64), catchall, if(failSilently) { rlError("Error importing SessionId. Buffer: %s, Size: %u, Id: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionId); } else { rlAssertf(0, "Error importing SessionId. Buffer: %s, Size: %u, Id: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionId); });
#endif
		rcheck(bb.ReadUns(m_SessionToken.m_Value, 64), catchall, if(failSilently) { rlError("Error importing SessionToken. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); } else { rlAssertf(0, "Error importing SessionToken. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); });
#if RSG_ORBIS
		rcheck(bb.SerUser(m_WebApiSessionId), catchall, if(failSilently) { rlError("Error importing WebApiSessionId. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); } else { rlAssertf(0, "Error importing WebApiSessionId. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); });
#endif
		success = true; 
	}
	rcatchall
	{

	}

	if(size)
	{
		*size = success ? bb.GetNumBytesRead() : 0;
	}

	return success;
}

#if RSG_ORBIS

bool
rlSessionInfo::ExportForNpPresence(void* buf,
								   const unsigned sizeofBuf,
								   unsigned* size) const
{
	// if you change the exported size, make sure
	// to change NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES

	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	// the exported session info never contains the player's real address
	netPeerAddress peerAddr = m_HostPeerAddr;
	peerAddr.ClearNonAdvertisableData();

	u8 peerAddrBuf[netPeerAddress::NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES] = {0};
	unsigned expSize;
	const bool success = peerAddr.ExportForNpPresence(peerAddrBuf, sizeof(peerAddrBuf), &expSize)
						 && bb.WriteBytes(peerAddrBuf, expSize)
						 && bb.WriteUns(m_SessionToken.m_Value, 64)
						 && bb.SerUser(m_WebApiSessionId);

	rlAssert(bb.GetNumBytesWritten() <= NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool
rlSessionInfo::ImportFromNpPresence(const void* buf,
									const unsigned sizeofBuf,
									unsigned* size,
									bool failSilently)
{
	this->Clear();

    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	bool success = false; 

	rtry
	{
		unsigned impSize;
		rcheck(m_HostPeerAddr.ImportFromNpPresence(buf, sizeofBuf, &impSize), catchall, if(failSilently) { rlError("Error importing HostPeerAddr. Buffer size: %u", sizeofBuf); } else { rlAssertf(0, "Error importing HostPeerAddr. Buffer size: %u", sizeofBuf); })
		rcheck(bb.SkipBytes(impSize), catchall, )
		rcheck(bb.SetNumBitsRead(bb.GetCursorPos()), catchall, );
		rcheck(bb.ReadUns(m_SessionToken.m_Value, 64), catchall, if(failSilently) { rlError("Error importing SessionToken. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); } else { rlAssertf(0, "Error importing SessionToken. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); });
		rcheck(bb.SerUser(m_WebApiSessionId), catchall, if(failSilently) { rlError("Error importing WebApiSessionId. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); } else { rlAssertf(0, "Error importing WebApiSessionId. Buffer: %s, Size: %u, Token: 0x%" I64FMT "x", buf, sizeofBuf, m_SessionToken.m_Value); });

		success = true; 
	}
	rcatchall
	{

	}

	if(size)
	{
		*size = success ? bb.GetNumBytesRead() : 0;
	}

	return success;
}
#endif

bool
rlSessionInfo::operator==(const rlSessionInfo& that) const
{
	return ((that.m_SessionToken == m_SessionToken) && (m_SessionToken.IsValid())) &&
			(that.m_HostPeerAddr.GetPeerId() == m_HostPeerAddr.GetPeerId());
}

bool
rlSessionInfo::operator!=(const rlSessionInfo& that) const
{
	return !operator==(that);
}

#else

bool
rlSessionInfo::IsValid() const
{
    return false;
}

bool
rlSessionInfo::IsClear() const
{
	return true;
}

const rlSessionToken
rlSessionInfo::GetToken() const
{
    rlAssert(false);
	return rlSessionToken::INVALID_SESSION_TOKEN;
}

const rlPeerAddress&
rlSessionInfo::GetHostPeerAddress() const
{
    rlAssert(false);
	return netPeerAddress::INVALID_PEER_ADDRESS;
}

bool
rlSessionInfo::Export(void* /*buf*/,
                        const unsigned /*sizeofBuf*/,
                        unsigned* /*size*/) const
{
    return false;
}

bool
rlSessionInfo::Import(const void* /*buf*/,
                        const unsigned /*sizeofBuf*/,
                        unsigned* /*size*/,
						bool /*failSilently*/)
{
    return false;
}

bool
rlSessionInfo::operator==(const rlSessionInfo& /*that*/) const
{
    return false;
}

bool
rlSessionInfo::operator!=(const rlSessionInfo& that) const
{
    return !this->operator==(that);
}

#endif

#if RSG_ORBIS
const rlSceNpSessionId& rlSessionInfo::GetWebApiSessionId() const
{
	return m_WebApiSessionId;
}

void rlSessionInfo::SetWebApiSessionId(const rlSceNpSessionId& sessionId)
{
	m_WebApiSessionId.Reset(sessionId.AsSceNpSessionId());
}

void rlSessionInfo::ClearWebApiSessionId()
{
	m_WebApiSessionId.Clear();
}
#endif

}   //namespace rage
