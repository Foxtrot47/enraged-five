// 
// rline/rlsessionmanager.h 
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLSESSIONMANAGER_H
#define RLINE_RLSESSIONMANAGER_H

#include "rl.h"
#include "rlgamerinfo.h"
#include "rlsessioninfo.h"
#include "rlsessionconfig.h"
#include "scmatchmaking/rlscmatchmaking.h"
#include "atl/delegate.h"

namespace rage
{

class netConnectionManager;
class netTransactor;
class netRequestHandler;
class netRequest;
class rlSession;
class netStatus;
class rlPresenceEvent;

class rlSessionManagerLink
{
public:

    rlSessionManagerLink()
    {
        m_Next = m_Prev = this;
    }

    void LinkBefore(rlSessionManagerLink* next)
    {
        Unlink();
        m_Next = next;
        m_Prev = next->m_Prev;
        m_Prev->m_Next = m_Next->m_Prev = this;
    }

    void LinkAfter(rlSessionManagerLink* prev)
    {
        Unlink();
        m_Prev = prev;
        m_Next = prev->m_Next;
        m_Prev->m_Next = m_Next->m_Prev = this;
    }

    void Unlink()
    {
        m_Next->m_Prev = m_Prev;
        m_Prev->m_Next = m_Next;
        m_Next = m_Prev = this;
    }

    rlSessionManagerLink* m_Next;
    rlSessionManagerLink* m_Prev;
};

//PURPOSE
//  Dispatched on the host via the snSessionOwner::GetGamerData callback when
//  the session requires application-specific gamer data.
//
class rlSessionInfoMine
{
public:

	rlSessionInfoMine()
		: m_SizeofData(0)
	{
	}
  
	u8 m_Data[RL_MAX_SESSION_DATA_MINE_SIZE];
	u16 m_SizeofData;
};

class rlSessionDetail
{
public:

    rlSessionDetail();

    void Reset(const rlPeerInfo& hostPeerInfo,
                const rlSessionInfo& sessionInfo,
                const rlSessionConfig& sessionConfig,
                const rlGamerHandle& hostHandle,
				const char* hostName,
                const unsigned numFilledPubSlots,
                const unsigned numFilledPrivSlots,
				const u8* sessionUserData,
				const u16 sessionUserDataSize,
				const u8* sessionInfoMineData,
				const u16 sessionInfoMineDataSize);

	void Reset(const rlSessionInfo& sessionInfo);

    void Clear();

	bool IsValid() const { return m_SessionInfo.IsValid(); }

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    rlPeerInfo m_HostPeerInfo;
    rlSessionInfo m_SessionInfo;
    rlSessionConfig m_SessionConfig;
	rlGamerHandle m_HostHandle;
	char m_HostName[RL_MAX_NAME_BUF_SIZE];
    unsigned m_NumFilledPublicSlots;
    unsigned m_NumFilledPrivateSlots;
	u16 m_SessionUserDataSize;
	u8 m_SessionUserData[RL_MAX_SESSION_USER_DATA_SIZE];
	u16 m_SessionInfoMineDataSize;
	u8 m_SessionInfoMineData[RL_MAX_SESSION_DATA_MINE_SIZE];
};

class rlSessionManager
{
    friend class rlSession;

public:

	enum
	{
		HOST_QUERY_DEFAULT_TIMEOUT_MS   = 1250,
		HOST_QUERY_DEFAULT_MAX_ATTEMPTS = 10,
		DEFAULT_NUM_QUERY_RESULTS_BEFORE_EARLY_OUT = 0, // default to 0, which means we disable the early-out feature
		DEFAULT_MAX_WAIT_SINCE_LAST_RESULT = 3000,
	};

public:

    static bool Init();

    static void Shutdown();

    static bool IsInitialized();

    static void Update();

    //PURPOSE
    //  Returns true if the local client is in the session
    static bool IsInSession(const rlSessionInfo& sessionInfo);

	//PURPOSE
	//  Returns true if the local client is in the session and is the host of the session.
	static bool IsHostingSession(const rlSessionInfo& sessionInfo);
	static bool IsHostingSession(const rlSessionInfo& sessionInfo, const rlScMatchmakingMatchId& matchId);

	//PURPOSE
	//  Returns a list of matchIds for the sessions we're hosting
	static bool GetHostedMatchIds(const int localGamerIndex, char* buf, const unsigned sizeOfBuf);

    //PURPOSE
    //  Sets the port used by LAN sessions to listen for
    //  matchmaking queries.
    static void SetLanMatchingPort(const unsigned short lanPort);
    static unsigned short GetLanMatchingPort();

	//PURPOSE
	//  Removes invalid (local and duplicates) sessions from list
	static unsigned RemoveInvalidSessions(rlSessionInfo* sessionInfos, const unsigned numSessions);

    //PURPOSE
    //  Queries the details of a remote session.
    //PARAMS
    //  netMode         - Network mode.
    //  channelId       - Channel on which to send the query.
	//  timeoutMs       - Timeout per request attempt (0 for general policy) 
	//  maxAttempts     - Max request attempts (0 for general policy) 
	//  numResultsBeforeEarlyOut - Sets the minimum number of successful tunnel/host query results we want before
 	//							   we early out of the rlSessionQueryDetailTask. This prevents long-running errant
 	//							   tunnel requests or host queries to delay the game from joining sessions
 	//							   (or...don't let one bad apple spoil the whole damn bunch)
    //  sessionInfos    - Sessions to query.
    //  numSessions     - Number of sessions to query.
    //  details         - Will be populated with the results of the query.
    //  numResults      - Number of details populated.
    //  status          - Status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    static bool QueryDetail(const rlNetworkMode netMode,
                            const unsigned channelId,
							unsigned timeoutMs,
							unsigned maxAttempts,
							unsigned numResultsBeforeEarlyOut,
							const bool solicited,
							const rlSessionInfo* sessionInfos,
                            const unsigned numSessions,
                            rlSessionDetail* details,
                            unsigned* numResults,
                            netStatus* status);

	static bool QueryDetail(const rlNetworkMode netMode,
							const unsigned channelId,
							unsigned timeoutMs,
							unsigned maxAttempts,
							unsigned numResultsBeforeEarlyOut,
							const bool solicited,
							const rlSessionInfo* sessionInfos,
							const unsigned numSessions,
							const rlGamerHandle targetHandle,
							rlSessionDetail* details,
							unsigned* numResults,
							netStatus* status);

    static void Cancel(netStatus* status);

	static void SetDiscriminator(const unsigned discriminator);
	static void SetRetryPolicy(const unsigned timeoutMs, const unsigned maxAttempts);
	static void SetDataMineEnabled(const bool enable);
	static void SetDefaultNumEarlyOutResults(const unsigned numResults);
	static void SetDefaultMaxWaitTimeAfterResult(const unsigned maxWaitTime);

	typedef atDelegate<const rlSession* (const rlSession* session, const rlGamerHandle& gamerHandle, rlSessionInfo* sessionInfo)> QueryReceivedDelegate;
	static QueryReceivedDelegate ms_rlSesMgrQueryDelegate; 

	typedef atDelegate<void (const rlSession* session, rlSessionInfoMine*)> GetInfoMine;
	static GetInfoMine ms_rlSesMgrGetInfoMineDelegate; 

private:

    static bool RegisterSession(rlSession* session);

    static void UnregisterSession(rlSession* session);

    static rlSession* GetSession(const rlSessionInfo& sessionInfo);

    static void OnRequest(netTransactor* transactor,
                            netRequestHandler* handler,
                            const netRequest* rqst);

    static void OnPresenceEvent(const rlPresenceEvent* event);

#if SUPPORT_LAN_MULTIPLAYER
    static void HandleLanQueries();
#endif
};

}   //namespace rage

#endif  //RLINE_RLSESSIONMANAGER_H
