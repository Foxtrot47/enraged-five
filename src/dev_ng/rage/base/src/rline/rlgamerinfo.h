// 
// rline/rlgamerinfo.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLGAMERINFO
#define RLINE_RLGAMERINFO

#include "rl.h"
#include "rlgamerhandle.h"
#include "rlpeerinfo.h"

#if RSG_NP
struct SceNpId;
#endif

extern "C" {
    typedef int GPProfile;
}

namespace rage
{

class rlPresence;

//PURPOSE
//  rlGamerId provides a lightweight platform independent globally unique
//  identifier that can be used to uniquely identify a gamer for the duration
//  of that gamer's login period, or for the duration of sessions in which the
//  gamer is a member.
//
//  rlGamerId differs from rlGamerHandle in that rlGamerId typically does
//  not contain information that is used in platform APIs.
//
//  rlGamerId is usually a combination of the MAC address of the gamer's
//  machine and the gamer's local index, or "seat", or controller index,
//  on the machine.
//
//  DO NOT store the gamer id to persistent storage as it is ephemeral.
//  The same gamer might have different gamer ids when playing on
//  different consoles, or even on the same console between reboots.
class rlGamerId
{
    friend class rlPresence;

public:

    rlGamerId();

    //PURPOSE
    //  Invalidates the gamer id.
    void Clear();

    //PURPOSE
    //  Returns true if the gamer id is valid.
    bool IsValid() const;

    //PURPOSE
    //  Creates a null-terminated string representing the gamer id.
    //PARAMS
    //  dst         - Destination char array.
    //  dstLen      - Number of chars in the array.
    const char* ToString(char* dst,
                        const unsigned dstLen) const;
    template<int SIZE>
    const char* ToString(char (&buf)[SIZE]) const
    {
        return ToString(buf, SIZE);
    }
    const char* ToHexString(char* dst,
                        const unsigned dstLen) const;
    template<int SIZE>
    const char* ToHexString(char (&buf)[SIZE]) const
    {
        return ToHexString(buf, SIZE);
    }

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    bool operator==(const rlGamerId& that) const;
    bool operator!=(const rlGamerId& that) const;

    //PURPOSE
    //  Implemented for the benefit of containers like std::map.
    bool operator<(const rlGamerId& that) const;

#if HACK_GTA4
    u64 GetId() const {return m_Id;}
    void SetId(u64 id) { m_Id = id; }
#endif

private:
    u64 m_Id;
};

//PURPOSE
//  rlGamerInfo contains various pieces of data related to a gamer.
//  These include the gamer handle, the gamer id, and the peer info
//  for the machine from which the gamer hails.
//
//NOTES
//  rlGamerInfo contains data that changes with each reboot of the
//  local machine.
//
//  *** DO NOT SAVE rlGamerInfo TO PERSISTENT STORAGE!!! ***
//
class rlGamerInfo
{
    friend class rlPresence;

public:

    //PURPOSE
    //  Returns true if the gamer is permitted
    //  to host/join multi-player sessions.
    static bool HasMultiplayerPrivileges(const int localGamerIndex);

    //PURPOSE
    //  Returns true if the gamer is permitted
    //  to voice chat in multi-player sessions.
    static bool HasChatPrivileges(const int localGamerIndex);

	//PURPOSE
	//  Returns true if the gamer is permitted
	//  to text chat in multi-player sessions.
	static bool HasTextPrivileges(const int localGamerIndex);

    //PURPOSE
    //  Returns true if the gamer is permitted
    //  to view and play user created content.
    static bool HasUserContentPrivileges(const int localGamerIndex);

	//PURPOSE
	// Returns true if the gamer is permitted social networking access
	static bool HasSocialNetworkingPrivileges(const int localGamerIndex);

	//PURPOSE
	// Returns true if the gamer is permitted store access
	static bool HasStorePrivileges(const int localGamerIndex);
	
	//PURPOSE
	// Returns true if the gamer is restricted access by age
	static bool IsRestrictedByAge(const int localGamerIndex);

#if RSG_ORBIS
	// PURPOSE
	// Checks if the player is subscribed to PS+
	static bool HasPlayStationPlusSubscription(const int localGamerIndex);

	// PURPOSE
	// Checks if a PS+ check request in pending
	static bool IsPlaystationPlusCheckPending(const int localGamerIndex);
#endif

	//PURPOSE
	//	Returns TRUE if the user is a PSN sub account
	//	ORBIS-ONLY, other platforms return false!
	static bool IsPlaystationSubAccount(const int localGamerIndex);

	//PURPOSE
	//	Returns TRUE if the user is signed out of PSN
	// ORBIS-ONLY, other platforms return false!
	static bool IsSignedOutOfPSN(const int localGamerIndex);

    //PURPOSE
    //  Returns true if the gamer has sufficient privileges
    //  to maintain development secrecy.
    //  Internally exits the game if false.
    //NOTES
    //  Only available in non-production builds
    //  Use -disabledevprivacycheck on the command line to disable this check.
    static bool CheckDevPrivacyPrivilegesAndBail(const int localGamerIndex);

    //PURPOSE
    //  Returns the age of the local gamer at the given index.
    //NOTES
    //  Correct age will be returned only after the player signs in to the
    //  online service (XBL, PSN, SC).
    //  On Xbox a title needs a special exemption to have access to the GetAge() API.
    //  If the player is not signed online, or the XBL exemption has not been granted,
    //  age will be negative.
    static int GetAge(const int localGamerIndex);

    //PURPOSE
    //  Returns the age group of the local gamer at the given index.
    //  See the rlAgeGroup enumeration.
    //NOTES
    //  The function is supported only on Xbox.  On other platforms
    //  RL_AGEGROUP_INVALID will be returned.
    //
    //  Which group a specific age falls into varies by region.
    //
    //  Be sure to check for RL_AGEGROUP_PENDING which indicates the
    //  age group is being retrieved.
    static rlAgeGroup GetAgeGroup(const int localGamerIndex);

    rlGamerInfo();

    //PURPOSE
    //  Invalidates the gamer info.
    void Clear();

    //PURPOSE
    //  Returns true if the gamer id is valid.
    bool IsValid() const;

    //PURPOSE
    //  Returns the local index of the gamer.  Returns -1 for remote
    //  gamers.
    int GetLocalIndex() const;

    //PURPOSE
    //  Returns true if the gamer is local.
    bool IsLocal() const;

    //PURPOSE
    //  Returns true if the gamer is remote
    bool IsRemote() const;

    //PURPOSE
    //  Returns true if the gamer is signed in.
    bool IsSignedIn() const;

    //PURPOSE
    //  Returns true if the gamer is signed online.
    bool IsOnline() const;

    //PURPOSE
    //  Returns the peer information for the gamer's local machine.
    const rlPeerInfo& GetPeerInfo() const;

    //PURPOSE
    //  Returns the gamer's unique identifier.
    const rlGamerId& GetGamerId() const;

    //PURPOSE
    //  Returns the gamer's online service-specific handle.
    const rlGamerHandle& GetGamerHandle() const;

    //PURPOSE
    //  Returns the gamer's ASCII name.
    //NOTES
    //  See the static rlGamerInfo::GetName() for rules on how the name is 
    //  determined, and notes about it's usage.
    const char* GetName() const;

	//PURPOSE
	//  Returns true if the display name has been set.
	bool HasDisplayName() const;

	//PURPOSE
	//  Returns the gamer's UTF-8 encoded unicode display name.
	//  This is only provided as a convenience to UI/script that want
	//  to get display names without calling async functions.
	//  The display name is only valid in certain cases where
	//  SetDisplayName() has been called (for example, when gamers
	//  are added to the local session) or if this is a local gamer.
	//  Call HasDisplayName() to determine if it has been set.
	const char* GetDisplayName() const;

#if HACK_GTA4
    //PURPOSE
    //  Sets the various data managed by this class
    //PARAMS
    //  peerInfo        - Peer info to associate with this gamer
    //  gamerId         - Gamer ID to associate with this gamer
    //  gamerHandle     - Gamer handle to associate with this gamer
    //  localGamerIndex - Local index for this gamer
    //  name            - Name of this gamer
    void SetGamerInfoData(const rlPeerInfo    &peerInfo,
                          const rlGamerId     &gamerId,
                          const rlGamerHandle &gamerHandle,
                          const int            localGamerIndex,
                          const char          *name);
#endif						 

#if RSG_DURANGO
	//PURPOSE
	//  Sets the display name. This should be called when 
	//  rlGamerInfos are discovered if the UI is to ever display
	//  their name on screen.
	void SetDisplayName(rlDisplayName* displayName);
#endif

#if RSG_DURANGO || RSG_ORBIS
	//PURPOSE
	//  Used to properly convert a wchar_t display name to utf8. It also adds an ellipsis if the name is too long
	static char* DisplayNameToUtf8(char(&out)[RL_MAX_DISPLAY_NAME_BUF_SIZE], const wchar_t *in);
#endif

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    bool operator==(const rlGamerInfo& that) const;
    bool operator!=(const rlGamerInfo& that) const;

private:
#if RSG_DURANGO
	void PopulateLocalDisplayName() const;
#endif

    rlPeerInfo m_PeerInfo;
    rlGamerId m_GamerId;
    rlGamerHandle m_GamerHandle;
    int m_LocalIndex;

    char m_Name[RL_MAX_NAME_BUF_SIZE];

#if RSG_DURANGO
	rlDisplayName m_DisplayName;
#endif

	bool m_IsSignedIn : 1;
    bool m_IsOnline   : 1;
};

}   //namespace rage

#endif  //RLINE_RLGAMERINFO
