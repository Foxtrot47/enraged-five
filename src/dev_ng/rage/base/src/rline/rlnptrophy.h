// 
// rline/rlnptrophy.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNPTROPHY_H
#define RLINE_RLNPTROPHY_H

#if RSG_NP

#include "system/ipc.h"
#include "rl.h"

struct SceNpCommunicationId;
struct SceNpCommunicationSignature;

#if RSG_ORBIS
#include <np.h>
#endif

namespace rage
{
class rlAchievementInfo;

class rlNpTrophy
{
public:
    rlNpTrophy();
    ~rlNpTrophy();

    bool Init(const SceNpCommunicationId* commId,
              const SceNpCommunicationSignature* commSig);
    void Shutdown();

    bool ReadAchievements(rlAchievementInfo* achievements,
                          const int maxAchievements,
                          int* numAchievements,
						  int gamerIndex);

    bool WriteAchievement(const int achievementId,
                          int *platinumAchievementId,
						  int gamerIndex);

#if RSG_ORBIS
	void OnUserLoginStatuschange(int localGamerIndex);
#endif

    //PURPOSE
    //  Returns the amount of HDD space required to store the trophy context.
    //RETURNS
    //  -1 on error, nonnegative on success.
    s64 GetRequiredDiskSpace();

	// PURPOSE: Get the result of sceNpTrophyRegisterContext if it has completed. 
	// PARAMS:	result -	Result from sceNpTrophyRegisterContext from the register context 
	//						worker thread if it has completed. Unchanged otherwise.
	// RETURNS:	Returns true if the context result is valid, false otherwise.
	bool GetTrophyRegisterContextResult(int& result);

	bool IsInitialised() { return m_Initialized; }

	static bool GetHasTrophyDataBeenReinstalled() { return ms_bTrophyDataHasBeenReinstalled; }
	static void SetHasTrophyDataBeenReinstalled(bool bHasBeenReinstalled) { ms_bTrophyDataHasBeenReinstalled = bHasBeenReinstalled; }

	static void WaitUntilTrophyStatusIsKnown()
	{

	}

	void WaitForWorker(ORBIS_ONLY(int index));

private:
	static void RegisterContextWorker(void*);
	bool CheckRegisterContextWorker(ORBIS_ONLY(int index));

#if RSG_ORBIS
	void DestroyContextAndHandles(int index);
	bool RefreshContextsIfNecessary(int index);
#endif

    const SceNpCommunicationId* m_CommId;

#if RSG_ORBIS
	struct RegisterHandleClosure
	{
		RegisterHandleClosure()
		{
			m_TrophyPtr = NULL;
			index = 0;
		}

		rlNpTrophy* m_TrophyPtr;
		int index;
	};

	int m_TrophyContext[RL_MAX_LOCAL_GAMERS];
	int m_TrophyHandle[RL_MAX_LOCAL_GAMERS];
	sysIpcThreadId m_TrophyWorker[RL_MAX_LOCAL_GAMERS];
	int m_TrophyRegisterContextResult[RL_MAX_LOCAL_GAMERS];
	RegisterHandleClosure m_Closures[RL_MAX_LOCAL_GAMERS];
	SceNpTrophyFlagArray m_UnlockedTrophies[RL_MAX_LOCAL_GAMERS];
	bool m_bDirtyContext[RL_MAX_LOCAL_GAMERS];
#endif

    s64 m_TrophyContextDiskSpaceBytes;

    volatile bool m_Initialized;

	static bool ms_bTrophyDataHasBeenReinstalled;
};

}

#endif // RSG_NP

#endif  //RLINE_NPTROPHY_H
