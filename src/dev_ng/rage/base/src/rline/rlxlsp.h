// 
// rline/rlxlsp.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
 
#ifndef RLINE_RLXLSP_H
#define RLINE_RLXLSP_H

#include "atl/delegate.h" 
#include "data/autoid.h"
#include "net/netaddress.h"
#include "net/status.h"

namespace rage
{

enum rlXlspEnvironment
{
    RLXLSP_ENV_UNKNOWN = -1,
    RLXLSP_ENV_PARTNET,
    RLXLSP_ENV_CERTNET,
	RLXLSP_ENV_CERTNET_2,    // for ros cert2
    RLXLSP_ENV_PRODNET
};

#if __LIVE

class rlXlspResolveRequest;
class rlXlspSecureGateway;

//PURPOSE
//  Manages discovery and connecting to the title's XLSP SGs.
//
//  Server connection info will be cached and reused if the same server name
//  or IP is requested.
//
//  If multiple servers satisfy a request one will be chosen at random, effectively
//  load balancing requests.
//
//  rlXlspManager will remain connected to all XLSP servers unless Disconnect() is
//  called explicitly.
//
//  Server resolution requests are processed serially.  If multiple requests are
//  queued, the first one in the queue must complete before the next one starts.
class rlXlspManager
{
public:
    //PURPOSE
    //  Initialize the XLSP manager.
    static bool Init();

    //PURPOSE
    //  Disconnect from XLSP server and shutdown the XLSP manager.
    static void Shutdown();

    //PURPOSE
    //  Returns true if rlXlspManager has been initialized.
    static bool IsInitialized();

    //PURPOSE
    //  Update pending operations.  Should be called at least once per frame.
    static void Update();

    //PURPOSE
    //  Disconnects from XLSP server, if connected.  This will
    //  invalidate any socket connections made to the server.
    //NOTES
    //  Disconnect() must be called to reclaim resources allocated
    //  when a server address is resolved.  Specifically, resolving
    //  a server address involves calling XNetConnect().  Disconnect()
    //  effectively calls XNetUnregisterInAddr().
    static void Disconnect(const netIpAddress& secureIp);

    //PURPOSE
    //  Asynchronously retrieves the address for the XLSP server with the
    //  specified name.
    //  The name must match that used in the userdata for this title in the
    //  XLSP server's sgconfig.ini.
    //
    //  The address returned can be used with socket calls.
    //PARAMS
    //  name        - Name of the server set in the userdata of sgconfig.ini.
    //  secureAddr  - Upon completion will contain the secure address
    //                of the title server.
    //  timeoutSecs - Number of seconds before timing out.
    //  status      - Can be polled for completion
    //RETURNS
    //  True on success, false on failure.
    //NOTES
    //  Resolving a server address has the side effect of creating a secure
    //  connection to the server using XNetConnect().  When finished with a
    //  server address (i.e. the address is not longer associated with any
    //  sockets) call Disconnect().  Calling Disconnect() will invalidate
    //  any sockets bound to the the resolved address.
    static bool ResolveServerAddr(const char* name,
                                    netSocketAddress* secureAddr,
                                    const unsigned timeoutSecs,
                                    netStatus* status);

    //PURPOSE
    //  Asynchronously connects to the server at the specific IP
    //  address and port.
    //PARAMS
    //  publicIp    - Public IP address of server.
    //  port        - Port on which to connect to the server.
    //  secureAddr  - Upon completion will contain the secure address
    //                of the title server.
    //NOTES
    //  This function can be used if the IP address of the server is
    //  already known.
    static bool ConnectToServer(const netIpAddress& publicIp,
                                const u16 port,
                                netSocketAddress* secureAddr);

    //PURPOSE
    //  Cancels a pending request.
    static void Cancel(netStatus* status);

    //PURPOSE
    //  Returns the environment under which the XLSP server
    //  (and thus the game) is running
    static rlXlspEnvironment GetEnvironment();

    //PURPOSE
    //  Returns the name of the given environment
    static const char* GetEnvironmentName(const rlXlspEnvironment env);

    //PURPOSE
    //  Retrieves a connected server's public IP given it's secure IP.
    static bool GetPublicIpFromSecureIp(const netIpAddress& secureIp,
                                        netIpAddress* publicIp);

    //PURPOSE
    //  Retrieves a connected server's secure IP given it's public IP.
    static bool GetSecureIpFromPublicIp(const netIpAddress& publicIp,
                                        netIpAddress* secureIp);

private:

    //PURPOSE
    //  Process pending host resolution requests
    static void ProcessPendingRequests();

    //PURPOSE
    //  Given a title server name, find the public IP
    //  and port for that title server
    static bool FindPublicIpAndPortByName(const char* titleServerName,
                                    netIpAddress* publicIp,
                                    u16* port);

    static rlXlspSecureGateway* FindSgByPublicIp(const netIpAddress& publicIp);
    static rlXlspSecureGateway* FindSgBySecureIp(const netIpAddress& secureIp);

    static bool FindSecureIpByPublicIp(const netIpAddress& publicIp,
                                        netIpAddress* secureIp);

    static bool FindPublicIpBySecureIp(const netIpAddress& secureIp,
                                        netIpAddress* publicIp);

    //PURPOSE
    //  Complete a pending request.
    static void CompleteRequest(rlXlspResolveRequest* resReq,
                                netStatus::StatusCode statusCode);

    //PURPOSE
    //  Begin the server discovery process.
    static bool BeginDiscovery();

    //PURPOSE
    //  Complete the server discovery process.
    static void EndDiscovery();
};

#endif //__LIVE

}   //namespace rage

#endif //RLINE_RLXLSP_H
