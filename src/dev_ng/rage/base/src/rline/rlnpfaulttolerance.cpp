// 
// rline/rlnpfaulttolerance.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#include "rlnpfaulttolerance.h"

#if RSG_ORBIS
#if ENABLE_NP_FAULT_TOLERANCE

#include "diag/channel.h"
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "string/stringutil.h"

#if __BANK
#include "bank/bkmgr.h"
#include "bank/bank.h"
#endif

namespace rage
{
	PARAM(npfaultrules, "Enable NP Fault Tolerance and specify a rules file. -npfaultrules=npfaults.xml");

	RAGE_DEFINE_SUBCHANNEL(rline, npfaulttolerance);
	#undef __rage_channel
	#define __rage_channel rline_npfaulttolerance

	bool rlNpFaultTolerance::Init()
	{
		const char* filename = NULL;
		if(!PARAM_npfaultrules.Get(filename))
		{
			return true;
		}

		/* Example rules file
			<?xml version="1.0" encoding="utf-8"?>
			<NpFaultTolerance>
				<WebApi>
					<Rule Name="Rule 1" Identifier="PUT_GAME_DATA" Match="*" ResponseCode="400" ResponseFile="400_Error.txt" EnabledAtStartup="false" />
					<Rule Name="Rule 2" Identifier="GET_FRIENDSLIST" Match="*rsjmoore*" ResponseCode="404" ResponseFile="404_NotFound.txt" DelayMS="5000" EnabledAtStartup="false" />
				</WebApi>
				<Tasks>
					<Rule Name="Rule 3" Identifier="NpAuth" Match="*" EnabledAtStartup="false" />
				</Tasks>
				<NpApi>
					<Rule Name="Rule 4" Identifier="NpAuthGetCode" Match="*" EnabledAtStartup="false" />
				</NpApi>
			</NpFaultTolerance>
		*/

		bool success = false;

		INIT_PARSER;

		parTree* tree = NULL;

		rtry
		{
			tree = PARSER.LoadTree(filename, "xml");

			rverify(tree && tree->GetRoot(), catchall, );

			parTreeNode* pRoot = tree->GetRoot();
			rverify(pRoot, catchall, );
			rverify(pRoot->FindNumChildren() == 3, catchall, rlError("Np Fault Tolerance file requires 3 children."));

			// Web API
			parTreeNode* pWebApi = pRoot->GetChild();
			rverify(pWebApi, catchall, );
			for(unsigned i = 0; i < (unsigned)pWebApi->FindNumChildren(); i++)
			{
				parTreeNode* pNode = pWebApi->FindChildWithIndex(i);
				rverify(pNode, catchall, );

				if(stricmp(pNode->GetElement().GetName(), "Rule") != 0)
				{
					continue;
				}

				rverify(ImportWebApiRule(pNode), catchall, );
			}

			// TASKS
			parTreeNode* pTasks = pWebApi->GetSibling();
			rverify(pTasks, catchall, );
			for(unsigned i = 0; i < (unsigned)pTasks->FindNumChildren(); i++)
			{
				parTreeNode* pNode = pTasks->FindChildWithIndex(i);
				rverify(pNode, catchall, );

				if(stricmp(pNode->GetElement().GetName(), "Rule") != 0)
				{
					continue;
				}

				rverify(ImportRule(pNode), catchall, );
			}
			
			// Np API
			parTreeNode* pNpApi = pTasks->GetSibling();
			rverify(pNpApi, catchall, );
			for(unsigned i = 0; i < (unsigned)pNpApi->FindNumChildren(); i++)
			{
				parTreeNode* pNode = pNpApi->FindChildWithIndex(i);
				rverify(pNode, catchall, );

				if(stricmp(pNode->GetElement().GetName(), "Rule") != 0)
				{
					continue;
				}

				rverify(ImportRule(pNode), catchall, );
			}

			AddWidgets();

			success = true;
		}
		rcatchall
		{
		}

		if(tree)
		{
			delete tree;
			tree = NULL;
		}

		SHUTDOWN_PARSER;

		return success;
	}

	void rlNpFaultTolerance::Shutdown()
	{
		m_Rules.clear();
	}

	void rlNpFaultTolerance::AddWidgets()
	{
		bkBank *pBank = BANKMGR.FindBank("Network");

		if(!pBank)
		{
			pBank = &BANKMGR.CreateBank("Network");
		}

		pBank->PushGroup("NP Fault Tolerance", false);

		rlNpFaultToleranceRules::iterator it = m_Rules.begin();
		rlNpFaultToleranceRules::iterator stop = m_Rules.end();
		for(; stop != it; ++it)
		{
			rlNpFaultToleranceRule* rule = (*it);
			pBank->AddToggle(rule->m_Name, &rule->m_Enabled, NullCallback, rule->m_Match);
		}

		pBank->PopGroup();
	}

	rlNpFaultToleranceRule* rlNpFaultTolerance::GetRule(const char * identifier, const char * path)
	{
		rlNpFaultToleranceRules::iterator it = m_Rules.begin();
		rlNpFaultToleranceRules::iterator stop = m_Rules.end();
		for(; stop != it; ++it)
		{
			rlNpFaultToleranceRule* rule = (*it);

			if(rule->m_Enabled)
			{
				// If identifier does not match, continue;
				if (stricmp(identifier, (*it)->m_Identifier))
				{
					continue;
				}

				// false means it's a match
				if(StringWildcardCompare((*it)->m_Match, path, true) == false)
				{
					return *it;
				}
			}
		}

		return NULL;
	}

	rlNpFaultToleranceRule* rlNpFaultTolerance::GetRule(u32 hashStr)
	{
		if (hashStr == 0)
		{
			return NULL;
		}

		rlNpFaultToleranceRules::iterator it = m_Rules.begin();
		rlNpFaultToleranceRules::iterator stop = m_Rules.end();
		for(; stop != it; ++it)
		{
			rlNpFaultToleranceRule* rule = (*it);

			if(rule->m_Enabled)
			{
				// If identifier does not match, continue;
				if (hashStr != rule->m_IdentifierHash)
				{
					continue;
				}

				return *it;
			}
		}

		return NULL;
	}

	void rlNpWebApiFaultToleranceRule::ApplyDelay()
	{
		if (m_DelayMs > 0 )
		{
			sysIpcSleep(m_DelayMs);
		}
	}

	bool rlNpWebApiFaultToleranceRule::GetResponse(datGrowBuffer* readBuffer, size_t* readSize)
	{
		rtry
		{
			rverify(readBuffer, catchall, rlError("No read buffer passed to Fault Tolerance response"));
			rverify(readSize, catchall, rlError("No read size passed to Fault Tolerance response"));

			*readSize = 0;

			if(m_ResponseFile[0] != '\0')
			{
				const fiDevice *pDevice = fiDevice::GetDevice(m_ResponseFile, true);
				fiHandle hFile = fiHandleInvalid;
				if (pDevice)
				{
					hFile = pDevice->Open(m_ResponseFile, true);
					if (fiIsValidHandle(hFile))
					{
						int bytesRead = 0;
						do
						{
							u8 buffer[256] = {0};
							bytesRead = pDevice->Read(hFile, buffer, sizeof(buffer));
							readBuffer->Append(buffer, bytesRead);
							*readSize = *readSize + bytesRead;
						}while(bytesRead > 0);

						pDevice->Close(hFile);

					}
				}
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlNpFaultTolerance::ImportRule(parTreeNode* pNode)
	{
		rtry
		{
			const parAttribute *attr = pNode->GetElement().FindAttributeAnyCase("Name");
			rverify(attr, catchall, );
			const char* name = attr->GetStringValue();
			rverify(name, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("Identifier");
			rverify(attr, catchall, );
			const char* identifier = attr->GetStringValue();
			rverify(identifier, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("Match");
			rverify(attr, catchall, );
			const char* match = attr->GetStringValue();
			rverify(match, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("ResponseCode");
			rverify(attr, catchall, );
			const char* responseCode = attr->GetStringValue();
			rverify(responseCode, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("EnabledAtStartup");
			rverify(attr, catchall, );
			const char* enableAtStartup = attr->GetStringValue();
			rverify(enableAtStartup, catchall, );

			rlNpFaultToleranceRule* rule = rage_new rlNpFaultToleranceRule;
			rverify(rule, catchall, );

			bool valid = rule->Init(name, identifier, match, responseCode, stricmp(enableAtStartup, "true") == 0);

			if(valid)
			{
				m_Rules.push_back(rule);
			}
			else
			{
				delete rule;
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool rlNpFaultTolerance::ImportWebApiRule(parTreeNode* pNode)
	{
		rtry
		{
			const parAttribute *attr = pNode->GetElement().FindAttributeAnyCase("Name");
			rverify(attr, catchall, );
			const char* name = attr->GetStringValue();
			rverify(name, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("Identifier");
			rverify(attr, catchall, );
			const char* identifier = attr->GetStringValue();
			rverify(identifier, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("Match");
			rverify(attr, catchall, );
			const char* match = attr->GetStringValue();
			rverify(match, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("ResponseCode");
			rverify(attr, catchall, );
			const char* responseCode = attr->GetStringValue();
			rverify(responseCode, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("ResponseFile");
			const char* responseFile = NULL;
			if(attr)
			{
				responseFile = attr->GetStringValue();
			}

			attr = pNode->GetElement().FindAttributeAnyCase("DelayMs");
			u32 delayMs = 0;
			if(attr)
			{
				const char* szDelayMs = attr->GetStringValue();
				rverify((szDelayMs != NULL) && (sscanf(szDelayMs, "%u", &delayMs) == 1), catchall, );
			}

			attr = pNode->GetElement().FindAttributeAnyCase("EnabledAtStartup");
			rverify(attr, catchall, );
			const char* enableAtStartup = attr->GetStringValue();
			rverify(enableAtStartup, catchall, );

			rlNpWebApiFaultToleranceRule* rule = rage_new rlNpWebApiFaultToleranceRule;
			rverify(rule, catchall, );

			bool valid = rule->Init(name, identifier, match, responseCode, delayMs, responseFile, stricmp(enableAtStartup, "true") == 0);

			if(valid)
			{
				m_Rules.push_back(rule);
			}
			else
			{
				delete rule;
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

} // namespace rage

#endif // ENABLE_NP_FAULT_TOLERANCE
#endif // RSG_ORBIS