// 
// rline/entitlementV1/rlRosEntitlementV1task.cpp
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rline/entitlementV1/rlRosEntitlementV1.h"

#if ENABLE_ENTITLEMENT_V1

#include "diag/seh.h"
#include "parser/treenode.h"
#include "rline/ros/rlros.h"
#include "string/stringhash.h"
#include "atl/string.h"

#include "rlRosEntitlementV1task.h"

namespace rage
{

#define XML_ENTRY_LENGTH 256
#define TOKEN_ENTRY_LENGTH 4096

#define ENTITLED_SKU_XML_TAG "EntitledSKUs"
#define CONTENTS_XML_TAG "Contents"

#define SKU_NAME_XML_TAG "SkuName"
#define INTERNAL_NAME_XML_TAG "InternalName"
#define FULL_NAME_XML_TAG "FullName"
#define SERIAL_NUM_XML_TAG "SerialNumber"
#define ACTIVATED_XML_TAG "IsActivated"

#define CONTENT_ID_XML_TAG "Id"
#define CONTENT_NAME_XML_TAG "Name"
#define SKU_NAMES_XML_TAG "SkuNames"
#define CONTENT_TITLE_ID_XML_TAG "TitleId"
#define CONTENT_PLATFORM_ID_XML_TAG "PlatformId"
#define REGISTERED_SKU_XML_TAG "RegisteredSku"
#define IS_ACTIVATED_XML_TAG "IsActivated"

#define TOKENS_XML_TAG "Tokens"
#define URI_XML_TAG "Uri"

#define ERROR_CASE(code, codeEx, resCode) \
	if(result.IsError(code, codeEx)) \
	{ \
	resultCode = resCode; \
	return; \
}

	//////////////////////////////////////////////////////////////////////////
	// rlRosEntitlementV1SkuTask
	//////////////////////////////////////////////////////////////////////////
	rlRosEntitlementV1SkuTask::rlRosEntitlementV1SkuTask()
	{

	}

	rlRosEntitlementV1SkuTask::~rlRosEntitlementV1SkuTask()
	{

	}

	bool rlRosEntitlementV1SkuTask::Configure(int localGamerIndex, rlRosEntitlementV1Data* entitlementData)
	{
		rtry
		{
			rverify(entitlementData, catchall, );

			m_EntitlementData = entitlementData;
			m_EntitlementData->m_Populated = false;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlRosEntitlementV1SkuTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
	{

		rtry
		{
			rverify(node, catchall, rlTaskError("No valid XML node to process"));
			rverify(m_EntitlementData, catchall, rlTaskError("No valid entitlement data to populate"));

			parTreeNode* pEntitledSkusNode = node->FindChildWithName(ENTITLED_SKU_XML_TAG);
			rverify(pEntitledSkusNode, catchall,);

			for(parTreeNode::ChildNodeIterator entitledNodeIt = pEntitledSkusNode->BeginChildren();
				*entitledNodeIt != NULL;
				++entitledNodeIt)
			{
				const parTreeNode &entitlementInfo = *(*entitledNodeIt);

				char skuName[XML_ENTRY_LENGTH] = { 0 };
				char internalName[XML_ENTRY_LENGTH] = { 0 };
				char fullName[XML_ENTRY_LENGTH] = { 0 };
				char serialNumber[XML_ENTRY_LENGTH] = { 0 };

				const char *pSkuName = entitlementInfo.GetElement().FindAttributeStringValue(SKU_NAME_XML_TAG, "", skuName, XML_ENTRY_LENGTH, true);
				const char *pInternalName = entitlementInfo.GetElement().FindAttributeStringValue(INTERNAL_NAME_XML_TAG, "", internalName, XML_ENTRY_LENGTH, true);
				const char *pFullName = entitlementInfo.GetElement().FindAttributeStringValue(FULL_NAME_XML_TAG, "", fullName, XML_ENTRY_LENGTH, true);
				const char *pSerialNumber = entitlementInfo.GetElement().FindAttributeStringValue(SERIAL_NUM_XML_TAG, "", serialNumber, XML_ENTRY_LENGTH, true);
				bool isActivated = entitlementInfo.GetElement().FindAttributeBoolValue(ACTIVATED_XML_TAG, false);

				if (!pSkuName || !pInternalName || !pFullName || !pSerialNumber)
				{
					rlTaskError("Invalid SKUEntitlementInfo XML entry");
					continue;
				}

				rlRosEntitlementV1Sku* newSku = rage_new rlRosEntitlementV1Sku();

				safecpy(newSku->m_SkuName, pSkuName);
				safecpy(newSku->m_InternalName, pInternalName);
				safecpy(newSku->m_FullName, pFullName);
				safecpy(newSku->m_SerialNum, pSerialNumber);
				newSku->m_IsActivated = isActivated;

				parTreeNode* contentsNode = entitlementInfo.FindChildWithName(CONTENTS_XML_TAG);
				rverify(contentsNode, catchall, rlTaskWarning("No content for entitled SKU: %s", newSku->m_SkuName));

				for(parTreeNode::ChildNodeIterator contentNodeIt = contentsNode->BeginChildren();
					*contentNodeIt != NULL;
					++contentNodeIt)
				{
					const parTreeNode &contentNode = *(*contentNodeIt);

					char contentName[XML_ENTRY_LENGTH] = { 0 };
					char skuNames[1024] = { 0 };

					const char *pContentName = contentNode.GetElement().FindAttributeStringValue(CONTENT_NAME_XML_TAG, "", contentName, XML_ENTRY_LENGTH, true);
					const char *pSkuNames = contentNode.GetElement().FindAttributeStringValue(SKU_NAMES_XML_TAG, "", skuNames, 1024, true);

					int id = contentNode.GetElement().FindAttributeIntValue(CONTENT_ID_XML_TAG, -1);
					int titleId = contentNode.GetElement().FindAttributeIntValue(CONTENT_TITLE_ID_XML_TAG, -1);
					int platformId = contentNode.GetElement().FindAttributeIntValue(CONTENT_PLATFORM_ID_XML_TAG, -1);

					if (!pContentName || id == -1 || titleId == -1 || platformId == -1)
					{
						rlTaskWarning("Invalid XML ContentInfo entry");
						continue;
					}

					rlRosEntitlementV1Content* newContent = rage_new rlRosEntitlementV1Content();

					newContent->m_ContentID = id;
					newContent->m_TitleID = titleId;
					newContent->m_PlatformID = platformId;

					char* skuName = strtok((char*)pSkuNames, ",");

					while(skuName)
					{
						char* newSku = rage_new char[64];
						safecpy(newSku, skuName, 64);
						newContent->m_SkuNames.PushAndGrow(newSku);

						skuName = strtok(NULL, ",");
					}

					safecpy(newContent->m_ContentName, pContentName);

					newSku->m_ContentArray.PushAndGrow(newContent);
				}

				u32 skuNameHash = atStringHash(newSku->m_SkuName);
				
				bool bInsert = true;

				// check for existing sku
				rlRosEntitlementV1Sku** existingSku = m_EntitlementData->m_EntitlementSkus.Access(skuNameHash);
				if (existingSku)
				{
					rlRosEntitlementV1Sku* sku = *existingSku;
					if (sku)
					{
						// there already exists an activated entry.
						if (sku->m_IsActivated)
						{
							delete newSku;
							bInsert = false;
						}
						else
						{
							// otherwise, delete the old sku.
							delete sku;
							m_EntitlementData->m_EntitlementSkus.Delete(skuNameHash);
						}
					}
				}

				if (bInsert)
				{
					m_EntitlementData->m_EntitlementSkus.Insert(skuNameHash, newSku);
				}
			}
		}
		rcatchall
		{
			return false;
		}

		m_EntitlementData->m_Populated = true;

		return true;
	}

	void rlRosEntitlementV1SkuTask::ProcessError(const rlRosResult& result, const parTreeNode* /* node */, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed", "Ticket", RLENV1_ERROR_AUTHENTICATIONFAILED_TICKET);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlRosEntitlementV1SkuTask::GetServiceMethod() const
	{
		return "entitlements.asmx/GetEntitlementsBySku";
	}

	//////////////////////////////////////////////////////////////////////////
	// rlRosEntitlementV1SkuWithSyncTask
	//////////////////////////////////////////////////////////////////////////
	rlRosEntitlementV1SkuWithSyncTask::rlRosEntitlementV1SkuWithSyncTask()
	{

	}

	rlRosEntitlementV1SkuWithSyncTask::~rlRosEntitlementV1SkuWithSyncTask()
	{

	}

	bool rlRosEntitlementV1SkuWithSyncTask::Configure(int localGamerIndex, rlRosEntitlementV1Data* entitlementData)
	{
		rtry
		{
			rverify(entitlementData, catchall, );

			m_EntitlementData = entitlementData;
			m_EntitlementData->m_Populated = false;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
			rverify(AddStringParameter("doSync", "true", true), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlRosEntitlementV1SkuWithSyncTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
	{
		rtry
		{
			rverify(node, catchall, rlTaskError("No valid XML node to process"));
			rverify(m_EntitlementData, catchall, rlTaskError("No valid entitlement data to populate"));

			parTreeNode* pEntitledSkusNode = node->FindChildWithName(ENTITLED_SKU_XML_TAG);
			rverify(pEntitledSkusNode, catchall,);

			for(parTreeNode::ChildNodeIterator entitledNodeIt = pEntitledSkusNode->BeginChildren();
				*entitledNodeIt != NULL;
				++entitledNodeIt)
			{
				const parTreeNode &entitlementInfo = *(*entitledNodeIt);

				char skuName[XML_ENTRY_LENGTH] = { 0 };
				char internalName[XML_ENTRY_LENGTH] = { 0 };
				char fullName[XML_ENTRY_LENGTH] = { 0 };
				char serialNumber[XML_ENTRY_LENGTH] = { 0 };

				const char *pSkuName = entitlementInfo.GetElement().FindAttributeStringValue(SKU_NAME_XML_TAG, "", skuName, XML_ENTRY_LENGTH, true);
				const char *pInternalName = entitlementInfo.GetElement().FindAttributeStringValue(INTERNAL_NAME_XML_TAG, "", internalName, XML_ENTRY_LENGTH, true);
				const char *pFullName = entitlementInfo.GetElement().FindAttributeStringValue(FULL_NAME_XML_TAG, "", fullName, XML_ENTRY_LENGTH, true);
				const char *pSerialNumber = entitlementInfo.GetElement().FindAttributeStringValue(SERIAL_NUM_XML_TAG, "", serialNumber, XML_ENTRY_LENGTH, true);
				bool isActivated = entitlementInfo.GetElement().FindAttributeBoolValue(ACTIVATED_XML_TAG, false);

				if (!pSkuName || !pInternalName || !pFullName || !pSerialNumber)
				{
					rlTaskError("Invalid SKUEntitlementInfo XML entry");
					continue;
				}

				rlRosEntitlementV1Sku* newSku = rage_new rlRosEntitlementV1Sku();

				safecpy(newSku->m_SkuName, pSkuName);
				safecpy(newSku->m_InternalName, pInternalName);
				safecpy(newSku->m_FullName, pFullName);
				safecpy(newSku->m_SerialNum, pSerialNumber);
				newSku->m_IsActivated = isActivated;

				parTreeNode* contentsNode = entitlementInfo.FindChildWithName(CONTENTS_XML_TAG);
				rverify(contentsNode, catchall, rlTaskWarning("No content for entitled SKU: %s", newSku->m_SkuName));

				for(parTreeNode::ChildNodeIterator contentNodeIt = contentsNode->BeginChildren();
					*contentNodeIt != NULL;
					++contentNodeIt)
				{
					const parTreeNode &contentNode = *(*contentNodeIt);

					char contentName[XML_ENTRY_LENGTH] = { 0 };
					char skuNames[1024] = { 0 };

					const char *pContentName = contentNode.GetElement().FindAttributeStringValue(CONTENT_NAME_XML_TAG, "", contentName, XML_ENTRY_LENGTH, true);
					const char *pSkuNames = contentNode.GetElement().FindAttributeStringValue(SKU_NAMES_XML_TAG, "", skuNames, 1024, true);

					int id = contentNode.GetElement().FindAttributeIntValue(CONTENT_ID_XML_TAG, -1);
					int titleId = contentNode.GetElement().FindAttributeIntValue(CONTENT_TITLE_ID_XML_TAG, -1);
					int platformId = contentNode.GetElement().FindAttributeIntValue(CONTENT_PLATFORM_ID_XML_TAG, -1);

					if (!pContentName || id == -1 || titleId == -1 || platformId == -1)
					{
						rlTaskWarning("Invalid XML ContentInfo entry");
						continue;
					}

					rlRosEntitlementV1Content* newContent = rage_new rlRosEntitlementV1Content();

					newContent->m_ContentID = id;
					newContent->m_TitleID = titleId;
					newContent->m_PlatformID = platformId;

					char* skuName = strtok((char*)pSkuNames, ",");

					while(skuName)
					{
						char* newSku = rage_new char[64];
						safecpy(newSku, skuName, 64);
						newContent->m_SkuNames.PushAndGrow(newSku);

						skuName = strtok(NULL, ",");
					}

					safecpy(newContent->m_ContentName, pContentName);

					newSku->m_ContentArray.PushAndGrow(newContent);
				}

				u32 skuNameHash = atStringHash(newSku->m_SkuName);

				bool bInsert = true;

				// check for existing sku
				rlRosEntitlementV1Sku** existingSku = m_EntitlementData->m_EntitlementSkus.Access(skuNameHash);
				if (existingSku)
				{
					rlRosEntitlementV1Sku* sku = *existingSku;
					if (sku)
					{
						// there already exists an activated entry.
						if (sku->m_IsActivated)
						{
							delete newSku;
							bInsert = false;
						}
						else
						{
							// otherwise, delete the old sku.
							delete sku;
							m_EntitlementData->m_EntitlementSkus.Delete(skuNameHash);
						}
					}
				}
				
				if (bInsert)
				{
					m_EntitlementData->m_EntitlementSkus.Insert(skuNameHash, newSku);
				}
			}
		}
		rcatchall
		{
			return false;
		}

		m_EntitlementData->m_Populated = true;

		return true;
	}

	void rlRosEntitlementV1SkuWithSyncTask::ProcessError(const rlRosResult& result, const parTreeNode* /* node */, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed", "Ticket", RLENV1_ERROR_AUTHENTICATIONFAILED_TICKET);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlRosEntitlementV1SkuWithSyncTask::GetServiceMethod() const
	{
		return "entitlements.asmx/GetEntitlementsBySkuWithSync";
	}

	//////////////////////////////////////////////////////////////////////////
	// rlRosEntitlementV1RegisterPurchaseBySNTask
	//////////////////////////////////////////////////////////////////////////
	rlRosEntitlementV1RegisterPurchaseBySNTask::rlRosEntitlementV1RegisterPurchaseBySNTask()
		: m_skuFriendlyName(NULL)
		, m_skuFriendlyNameBufLen(0)
	{

	}

	rlRosEntitlementV1RegisterPurchaseBySNTask::~rlRosEntitlementV1RegisterPurchaseBySNTask()
	{

	}

	bool rlRosEntitlementV1RegisterPurchaseBySNTask::Configure(int localGamerIndex, int projectId, char* serialNumber, bool* isActivated, char* skuName, unsigned skuNameBufLen)
	{
		return Configure(localGamerIndex, projectId, serialNumber, isActivated, skuName, skuNameBufLen, NULL, 0);
	}

	bool rlRosEntitlementV1RegisterPurchaseBySNTask::Configure(int localGamerIndex, int projectId, char* serialNumber, bool* isActivated, 
		char* skuName, unsigned skuNameBufLen, char* friendlyName, unsigned friendlyLength)
	{
		rtry
		{
			rverify(serialNumber, catchall, );
			rverify(isActivated, catchall, );
			rverify(skuName, catchall, );

			m_skuNameBufLen = skuNameBufLen;
			m_isActivated = isActivated;
			m_skuName = skuName;
			m_skuName[0] = '\0';

			m_skuFriendlyName = friendlyName;
			m_skuFriendlyNameBufLen = friendlyLength;

			if (m_skuFriendlyName)
			{
				m_skuFriendlyName[0] = '\0';
			}

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
			rverify(AddIntParameter("projectId", projectId), catchall, );
			rverify(AddStringParameter("serialNumber", serialNumber, true), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlRosEntitlementV1RegisterPurchaseBySNTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
	{
		rtry
		{
			rverify(node, catchall, );
			rverify(m_skuName, catchall, );

			parTreeNode* pSku = node->FindChildWithName(REGISTERED_SKU_XML_TAG);
			rverify(pSku, catchall,);

			pSku->GetElement().FindAttributeStringValue(SKU_NAME_XML_TAG, "<unknown>", m_skuName, m_skuNameBufLen, true);

			if (m_skuFriendlyName)
			{
				pSku->GetElement().FindAttributeStringValue(FULL_NAME_XML_TAG, "", m_skuFriendlyName, m_skuFriendlyNameBufLen, true);
			}

			*m_isActivated = pSku->GetElement().FindAttributeBoolValue(IS_ACTIVATED_XML_TAG, false);
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlRosEntitlementV1RegisterPurchaseBySNTask::ProcessError(const rlRosResult& result, const parTreeNode* /* node */, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed", "Ticket", RLENV1_ERROR_AUTHENTICATIONFAILED_TICKET);
		ERROR_CASE("DoesNotMatch", NULL, RLENV1_ERROR_SERIAL_DOES_NOT_EXIST);
		ERROR_CASE("DoesNotExist", NULL, RLENV1_ERROR_SERIAL_DOES_NOT_EXIST);
		ERROR_CASE("NotAllowed", NULL, RLENV1_ERROR_SERIAL_ALREADY_ACTIVATED);
		ERROR_CASE("Redundant", NULL, RLENV1_ERROR_REDUNDANT_SERIAL);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlRosEntitlementV1RegisterPurchaseBySNTask::GetServiceMethod() const
	{
		return "entitlements.asmx/RegisterSNCurrentOwner";
	}

	//////////////////////////////////////////////////////////////////////////
	// rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask
	//////////////////////////////////////////////////////////////////////////
	rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask::rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask()
	{

	}

	rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask::~rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask()
	{

	}

	bool rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask::Configure(int localGamerIndex, int projectId, char* serialNumber)
	{
		rtry
		{
			rverify(serialNumber, catchall, );

			rverify(rlRosCredentialsChangingTask::Configure(localGamerIndex), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
			rverify(AddIntParameter("projectId", projectId), catchall, );
			rverify(AddStringParameter("serialNumber", serialNumber, true), catchall, );
			rverify(AddStringParameter("fingerprint", "", true), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask::ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode)
	{
		return rlRosCredentialsChangingTask::ProcessSuccess(result, node, resultCode);
	}

	void rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask::ProcessError(const rlRosResult& result, const parTreeNode* /* node */, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed", "Ticket", RLENV1_ERROR_AUTHENTICATIONFAILED_TICKET);
		ERROR_CASE("DoesNotMatch", NULL, RLENV1_ERROR_SERIAL_DOES_NOT_EXIST);
		ERROR_CASE("DoesNotExist", NULL, RLENV1_ERROR_SERIAL_DOES_NOT_EXIST);
		ERROR_CASE("NotAllowed", NULL, RLENV1_ERROR_SERIAL_ALREADY_ACTIVATED);
		ERROR_CASE("Redundant", NULL, RLENV1_ERROR_REDUNDANT_SERIAL);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask::GetServiceMethod() const
	{
		return "entitlements.asmx/RegisterAndActivateSN";
	}

	//////////////////////////////////////////////////////////////////////////
	// rlRosEntitlementV1ValidateContentPlayerAgeTask
	//////////////////////////////////////////////////////////////////////////
	rlRosEntitlementV1ValidateContentPlayerAgeTask::rlRosEntitlementV1ValidateContentPlayerAgeTask()
	{

	}

	rlRosEntitlementV1ValidateContentPlayerAgeTask::~rlRosEntitlementV1ValidateContentPlayerAgeTask()
	{

	}

	bool rlRosEntitlementV1ValidateContentPlayerAgeTask::Configure (int localGamerIndex, const char* contentName)
	{
		rtry
		{
			rverify(contentName, catchall, );
			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);
			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
			rverify(AddStringParameter("contentName", contentName, true), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlRosEntitlementV1ValidateContentPlayerAgeTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& resultCode)
	{
		rtry
		{
			rverify(node, catchall, rlTaskError("No valid XML node to process"));
			resultCode = 1;
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlRosEntitlementV1ValidateContentPlayerAgeTask::ProcessError(const rlRosResult& result, const parTreeNode* /* node */, int& resultCode)
	{
		ERROR_CASE("AuthenticationFailed", "Ticket", RLENV1_ERROR_AUTHENTICATIONFAILED_TICKET);
		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlRosEntitlementV1ValidateContentPlayerAgeTask::GetServiceMethod() const
	{
		return "entitlements.asmx/ValidateContentPlayerAge";
	}

	//////////////////////////////////////////////////////////////////////////
	// rlRosEntitlementV1DownloadURLTask
	//////////////////////////////////////////////////////////////////////////
	rlRosEntitlementV1DownloadURLTask::rlRosEntitlementV1DownloadURLTask()
	{
		m_downloadURL = NULL;
		m_downloadURLBufLen = 0;
	}

	rlRosEntitlementV1DownloadURLTask::~rlRosEntitlementV1DownloadURLTask()
	{

	}

	bool rlRosEntitlementV1DownloadURLTask::Configure(int localGamerIndex, const char* skuName, char* downloadURL, unsigned int downloadURLBufLen)
	{
		rtry
		{
			rverify(skuName, catchall, );
			rverify(downloadURL, catchall, );
			rverify(downloadURLBufLen > 0, catchall, );

			m_downloadURL = downloadURL;
			m_downloadURLBufLen = downloadURLBufLen;

			rverify(rlRosHttpTask::Configure(localGamerIndex), catchall, );

			const rlRosCredentials& cred = rlRos::GetCredentials(localGamerIndex);

			rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
			rverify(AddStringParameter("contentName", skuName, true), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	bool rlRosEntitlementV1DownloadURLTask::ProcessSuccess(const rlRosResult& UNUSED_PARAM(result), const parTreeNode* node, int& UNUSED_PARAM(resultCode))
	{
		rtry
		{
			rverify(node, catchall, rlTaskError("No valid XML node to process"));
			rverify(m_downloadURL, catchall, rlTaskError("Invalid download URL string"));

			parTreeNode* pSku = node->FindChildWithName(URI_XML_TAG);
			rverify(pSku, catchall, rlTaskError("Invalid download URL response"));

			safecpy(m_downloadURL, pSku->GetData(), m_downloadURLBufLen);
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	void rlRosEntitlementV1DownloadURLTask::ProcessError(const rlRosResult& result, const parTreeNode* /* node */, int& resultCode)
	{
		ERROR_CASE("DoesNotExist", "RockstarAccount", RLENV1_ERROR_AUTHENTICATIONFAILED_TICKET);
		ERROR_CASE("NotAllowed", "ContentName", RLENV1_ERROR_NOT_ENTITLED);

		rlTaskWarning("Unhandled error: Code=%s  CodeEx=%s", result.m_Code, result.m_CodeEx ? result.m_CodeEx : "[NULL]");
		resultCode = RLSC_ERROR_UNEXPECTED_RESULT;
	}

	const char* rlRosEntitlementV1DownloadURLTask::GetServiceMethod() const
	{
		return "entitlements.asmx/GetContentDownloadUri";
	}

} // namespace rage

#endif // #if ENABLE_ENTITLEMENT_V1
