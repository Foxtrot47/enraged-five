// 
// rline/entitlementV1/rlrosentitlementV1.h
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLROSENTITLEMENTV1_H
#define RLROSENTITLEMENTV1_H

#include "file/file_config.h"

#if defined(__RGSC_DLL) && __RGSC_DLL && RSG_CPU_X86
	#define ENABLE_ENTITLEMENT_V1 (1)
#else
	#define ENABLE_ENTITLEMENT_V1 (0)
#endif

#if ENABLE_ENTITLEMENT_V1

#include "atl/array.h"
#include "atl/map.h"

namespace rage
{

	enum 
	{
		RLEN_MAX_SKU_NAME_CHARS = 256,
		RLEN_MAX_CONTENT_NAME_CHARS = 256,
		RLEN_MAX_INT_NAME_CHARS = 256,
		RLEN_MAX_FULL_NAME_CHARS = 512,
		RLEN_MAX_SERIAL_NUM_CHARS = 64,
		RLEN_MAX_DOWNLOAD_TOKEN_CHARS = 4096
	};

	enum eEntitlementV1Status
	{
		ENTITLEMENT_NOT_OWNED,
		ENTITLEMENT_OWNED_SKU,
		ENTITLEMENT_OWNED_CONTENT
	};

	class netStatus;

	class rlRosEntitlementV1Content
	{
	public:
		rlRosEntitlementV1Content();

		char m_ContentName[RLEN_MAX_CONTENT_NAME_CHARS];
		int m_ContentID;
		int m_TitleID;
		int m_PlatformID;
		atArray<char*> m_SkuNames;

		void Copy(rlRosEntitlementV1Content* content)
		{
			if (!content)
				return;

			safecpy(m_ContentName, content->m_ContentName, sizeof(m_ContentName));

			m_ContentID = content->m_ContentID;
			m_TitleID = content->m_TitleID;
			m_PlatformID = content->m_PlatformID;
		}
	};

	class rlRosEntitlementV1Sku
	{
	public:
		rlRosEntitlementV1Sku();

		char m_SkuName[RLEN_MAX_SKU_NAME_CHARS];
		char m_InternalName[RLEN_MAX_INT_NAME_CHARS];
		char m_FullName[RLEN_MAX_FULL_NAME_CHARS];
		char m_SerialNum[RLEN_MAX_SERIAL_NUM_CHARS];
		char m_DownloadToken[RLEN_MAX_DOWNLOAD_TOKEN_CHARS];
		bool m_IsActivated : 1;

		atArray<rlRosEntitlementV1Content*> m_ContentArray;

		void Copy(rlRosEntitlementV1Sku* sku)
		{
			if (!sku)
				return;

			m_ContentArray.Reset();

			safecpy(m_SkuName, sku->m_SkuName, sizeof(m_SkuName));
			safecpy(m_InternalName, sku->m_InternalName, sizeof(m_InternalName));
			safecpy(m_FullName, sku->m_FullName, sizeof(m_FullName));
			safecpy(m_SerialNum, sku->m_SerialNum, sizeof(m_SerialNum));
			safecpy(m_DownloadToken, sku->m_DownloadToken, sizeof(m_DownloadToken));

			m_IsActivated = sku->m_IsActivated;

			for (int i = 0; i < sku->m_ContentArray.GetCount(); i++)
			{
				rlRosEntitlementV1Content* newContent = rage_new rlRosEntitlementV1Content();
				if (!newContent)
					return;

				newContent->Copy(sku->m_ContentArray[i]);
				m_ContentArray.PushAndGrow(newContent);
			}
		}
	};

	class rlRosEntitlementV1Data
	{
	public:
		rlRosEntitlementV1Data() { m_Populated = false; }

		rlRosEntitlementV1Sku* GetSkuByName(const char* skuName);

		void Reset()
		{
			m_EntitlementSkus.Kill();
			m_Populated = false;
		}

		atMap<u32, rlRosEntitlementV1Sku*> m_EntitlementSkus;

		bool m_Populated;
	};

	class rlRosEntitlementV1
	{
	private:

	public:
		static bool Init();
		static bool IsInitialized();
		static void Shutdown();
		static void Update();

		static void Cancel();

		static void RefreshEntitlementData(const int localGamerIndex, bool refreshArvatoEntitlement = false);

		static bool GetEntitlementsBySku(const int localGamerIndex, rlRosEntitlementV1Data* entitlementData, netStatus* status);
		static bool GetEntitlementsBySkuWithSync(const int localGamerIndex, rlRosEntitlementV1Data* entitlementData, netStatus* status);
		static bool RegisterPurchaseBySN(const int localGamerIndex, int entitlementProjectId, char* serialNumber, bool* isActivated, char* skuName, 
										unsigned skuNameBufLen, char* friendlyName, unsigned friendlyLength, netStatus* status);
		static bool RegisterPurchaseAndActivationBySN(const int localGamerIndex, int entitlementProjectId, char* serialNumber, netStatus* status);
		static bool GetValidateContentPlayerAge(const int localGamerIndex, const char* contentName, netStatus* status);
		static bool GetSkuDownloadURL(const int localGamerIndex, const char* skuName, char* downloadUrl, unsigned int downloadUrlBufLen, netStatus* status);
		static eEntitlementV1Status IsEntitledToContent(const char* skuName);
		static bool GetContentNameBySku(const char * skuName, char * contentName, unsigned int contentNameLen);

		static rlRosEntitlementV1Data* GetCurrentEntitlementData()	{ return &sm_CurrentEntitlementData; }

		static bool IsEntitlementUpToDate()		{ return sm_EntitlementUpToDate; }
		static bool IsCheckingForEntitlement()	{ return sm_UpdatingEntitlement || sm_RefreshEntitlement; }

	public:
		static int	sm_LocalGamerIndex;
		static bool sm_Initialized;
		static netStatus sm_EntitlementStatus;
		static bool sm_RefreshEntitlement;
		static bool sm_RefreshArvatoEntitlement;
		static bool sm_EntitlementUpToDate;
		static bool sm_UpdatingEntitlement;

	private:
		static rlRosEntitlementV1Data sm_CurrentEntitlementData;
	};

}

#endif // ENABLE_ENTITLEMENT_V1

#endif // RLROSENTITLEMENTV1_H