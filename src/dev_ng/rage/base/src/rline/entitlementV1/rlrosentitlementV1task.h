// 
// rline/entitlementV1/rlRosEntitlementV1task.h
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROSENTITLEMENTV1TASKS_H
#define RLINE_RLROSENTITLEMENTV1TASKS_H

#include "rline/entitlementV1/rlRosEntitlementV1.h"

#if ENABLE_ENTITLEMENT_V1

#include "rline/ros/rlroshttptask.h"
#include "rline/ros/rlroscredtasks.h"

namespace rage
{
	enum rlEntitlementV1ErrorCode
	{
		RLENV1_ERROR_UNEXPECTED_RESULT = -1,              //Unhandled error code
		RLENV1_ERROR_NONE = 0,                            //Success
		RLENV1_ERROR_AUTHENTICATIONFAILED_TICKET,			//Invalid ROS Ticket
		RLENV1_ERROR_ALREADY_EXISTS,						
		RLENV1_ERROR_SERIAL_DOES_NOT_EXIST,				//Serial Number does not exist in DB
		RLENV1_ERROR_SERIAL_DOES_NOT_MATCH,				//Serial Number does not match
		RLENV1_ERROR_REDUNDANT_SERIAL,					//Redundant serial 
		RLENV1_ERROR_SERIAL_ALREADY_ACTIVATED,			//Serial Number already activated by another user.
		RLENV1_ERROR_SERIAL_NOT_ACTIVATED_ON_SERVER,		//Serial Number is owned but has not been activated on the license server
		RLENV1_ERROR_INVALID_SKU,							//Invalid SKU to activate/purchase
		RLENV1_ERROR_NOT_ENTITLED							//User is not entitled to particular content
	};

	class netStatus;

	class rlRosEntitlementV1SkuTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1SkuTask);

		rlRosEntitlementV1SkuTask();

		virtual ~rlRosEntitlementV1SkuTask();

		bool Configure(int localGamerIndex, rlRosEntitlementV1Data* entitlementData);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char* GetServiceMethod() const;

	public:
		rlRosEntitlementV1Data*	m_EntitlementData;
	};

	class rlRosEntitlementV1SkuWithSyncTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1SkuWithSyncTask);

		rlRosEntitlementV1SkuWithSyncTask();

		virtual ~rlRosEntitlementV1SkuWithSyncTask();

		bool Configure(int localGamerIndex, rlRosEntitlementV1Data* entitlementData);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char* GetServiceMethod() const;

	public:
		rlRosEntitlementV1Data*	m_EntitlementData;
	};

	class rlRosEntitlementV1ActivationTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1ActivationTask);

		rlRosEntitlementV1ActivationTask();

		virtual ~rlRosEntitlementV1ActivationTask();

		bool Configure(int localGamerIndex, char* skuName, char* serialNumber, char* fingerprint);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char* GetServiceMethod() const;
	};

	class rlRosEntitlementV1RegisterPurchaseBySNTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1RegisterPurchaseBySNTask);

		rlRosEntitlementV1RegisterPurchaseBySNTask();

		virtual ~rlRosEntitlementV1RegisterPurchaseBySNTask();

		bool Configure(int localGamerIndex, int projectId, char* serialNumber, bool* isActivated, char* skuName, unsigned skuNameBufLen);
		bool Configure(int localGamerIndex, int projectId, char* serialNumber, bool* isActivated,  char* skuName, unsigned skuNameBufLen, char* friendlyName, unsigned friendlyLength);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char* GetServiceMethod() const;

		bool*		m_isActivated;
		char*		m_skuName;
		unsigned	m_skuNameBufLen;

		char*		m_skuFriendlyName;
		unsigned	m_skuFriendlyNameBufLen;
	};

	class rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask : public rlRosCredentialsChangingTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask);

		rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask();

		virtual ~rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask();

		bool Configure(int localGamerIndex, int projectId, char* serialNumber);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char* GetServiceMethod() const;
	};


	class rlRosEntitlementV1DownloadTokenTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1DownloadTokenTask);

		rlRosEntitlementV1DownloadTokenTask();

		virtual ~rlRosEntitlementV1DownloadTokenTask();

		bool Configure(int localGamerIndex, const char* skuName, const char* serialNum, atMap<u32, atString>* downloadTokens);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char* GetServiceMethod() const;

	public:
		atMap<u32, atString> *m_DownloadTokens;
	};

	class rlRosEntitlementV1ValidateContentPlayerAgeTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1ValidateContentPlayerAgeTask);
		rlRosEntitlementV1ValidateContentPlayerAgeTask();
		virtual ~rlRosEntitlementV1ValidateContentPlayerAgeTask();

		bool Configure (int localGamerIndex, const char* contentName);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char * GetServiceMethod() const;

	};

	class rlRosEntitlementV1DownloadURLTask : public rlRosHttpTask
	{
	public:
		RL_TASK_DECL(rlRosEntitlementV1DownloadURLTask);

		rlRosEntitlementV1DownloadURLTask();

		virtual ~rlRosEntitlementV1DownloadURLTask();

		bool Configure(int localGamerIndex, const char* skuName, char* downloadURL, unsigned int downloadURLBufLen);

		virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
		virtual void ProcessError(const rlRosResult& result, const parTreeNode* node, int& resultCode);

		virtual const char* GetServiceMethod() const;

	public:
		char* m_downloadURL;
		unsigned int m_downloadURLBufLen;
	};

} // namespace rage

#endif // ENABLE_ENTITLEMENT_V1
#endif // RLINE_RLROSENTITLEMENTV1TASKS_H