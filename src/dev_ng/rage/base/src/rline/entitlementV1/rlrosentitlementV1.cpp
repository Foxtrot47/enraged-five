// 
// rline/entitlementV1/rlRosEntitlementV1V1.cpp
// 
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved. 
// 

#include "rlRosEntitlementV1.h"

#if ENABLE_ENTITLEMENT_V1

#include "rlRosEntitlementV1task.h"

#include "atl/string.h"
#include "diag/seh.h"
#include "rline/ros/rlros.h"
#include "string/stringhash.h"

namespace rage
{

	rlRosEntitlementV1Content::rlRosEntitlementV1Content()
	{
		m_ContentName[0] = '\0';
		m_ContentID = -1;
		m_PlatformID = -1;
		m_TitleID = -1;
	}

	rlRosEntitlementV1Sku::rlRosEntitlementV1Sku()
	{
		m_SkuName[0] = '\0';
		m_InternalName[0] = '\0';
		m_FullName[0] = '\0';
		m_SerialNum[0] = '\0';
		m_DownloadToken[0] = '\0';
		m_IsActivated = false;
	}

	rlRosEntitlementV1Sku* rlRosEntitlementV1Data::GetSkuByName(const char* skuName)
	{
		if (!skuName)
		{
			rlError("Invalid SKU name to query for entitlement data");
			return NULL;
		}

		u32 skuNameHash = atStringHash(skuName);
		rlRosEntitlementV1Sku** pEntitlementSku = m_EntitlementSkus.Access(skuNameHash);

		if (!pEntitlementSku || !*pEntitlementSku)
		{
			//rlWarning("SKU: %s is not entitled for this SC user", skuName);
			return NULL;
		}

		return *pEntitlementSku;
	}

#define RLENTITLEMENTV1_CREATETASK(T, status, localGamerIndex, ...) 	                        \
	bool success = false;																	\
	rlTaskBase* task = NULL;                                                                \
	rtry																					\
	{																						\
	rcheck(rlRos::IsOnline(localGamerIndex),                                            \
	catchall,                                                                   \
	rlDebug("Local gamer %d is not signed into ROS", localGamerIndex));         \
	T* newTask = rlGetTaskManager()->CreateTask<T>();									\
	rverify(newTask,catchall,);															\
	task = newTask;																		\
	rverify(rlTaskBase::Configure(newTask, localGamerIndex, __VA_ARGS__, status),		\
	catchall, rlError("Failed to configure task"));                         \
	rverify(rlGetTaskManager()->AppendSerialTask(task), catchall, );       			    \
	success = true;	                                                                    \
	}																						\
	rcatchall																				\
	{																						\
	if(task)																			\
	{																					\
	rlGetTaskManager()->DestroyTask(task);								            \
	}																					\
	}																						\
	return success;																			

	bool		rlRosEntitlementV1::sm_RefreshEntitlement = false;
	bool		rlRosEntitlementV1::sm_RefreshArvatoEntitlement = false;
	bool		rlRosEntitlementV1::sm_EntitlementUpToDate = false;
	bool		rlRosEntitlementV1::sm_UpdatingEntitlement = false;
	netStatus	rlRosEntitlementV1::sm_EntitlementStatus;
	int			rlRosEntitlementV1::sm_LocalGamerIndex = -1;

	rlRosEntitlementV1Data rlRosEntitlementV1::sm_CurrentEntitlementData;

	bool rlRosEntitlementV1::sm_Initialized = false;

	bool rlRosEntitlementV1::Init()
	{		
		rlAssert(!sm_Initialized);    
		//@@: location RLROSENTITLEMENTV1_INIT
		sm_Initialized = true;

		return sm_Initialized;
	}

	bool rlRosEntitlementV1::IsInitialized()
	{
		return sm_Initialized;
	}

	void rlRosEntitlementV1::Shutdown()
	{
		if(sm_Initialized)
		{
			sm_Initialized = false;
		}
	}

	void rlRosEntitlementV1::Update()
	{
		if (sm_RefreshEntitlement)
		{
			//@@: location RLROSENTITLEMENTV1_UPDATE_CHECK_PENDING
			if (sm_EntitlementStatus.Pending())
			{
				Cancel();
			}

			//@@: range  RLROSENTITLEMENTV1_UPDATE_BODY {
			sm_RefreshEntitlement = false;
			sm_CurrentEntitlementData.Reset();
			//@@: location RLROSENTITLEMENTV1_UPDATE_REFRESH_ENTITLEMENT
			if (sm_RefreshArvatoEntitlement)
			{
				sm_RefreshArvatoEntitlement = false;

				if (!GetEntitlementsBySkuWithSync(sm_LocalGamerIndex, &sm_CurrentEntitlementData, &sm_EntitlementStatus))
				{
					rlError("Could not get entitlement information.");
					sm_UpdatingEntitlement = false;
					sm_EntitlementUpToDate = true;
					return;
				}
			}
			else
			{
				if (!GetEntitlementsBySku(sm_LocalGamerIndex, &sm_CurrentEntitlementData, &sm_EntitlementStatus))
				{
					rlError("Could not get entitlement information.");
					sm_UpdatingEntitlement = false;
					sm_EntitlementUpToDate = true;
					return;
				}
			}
			//@@: } RLROSENTITLEMENTV1_UPDATE_BODY 
		}

		if (sm_UpdatingEntitlement)
		{
			if (sm_EntitlementStatus.Succeeded() || sm_EntitlementStatus.Failed())
			{
				//@@: location RLROSENTITLEMENTV1_UPDATE_DESTROY_TASKS
				sm_EntitlementUpToDate = true;
				sm_UpdatingEntitlement = false;
			}
		}
	}

	void rlRosEntitlementV1::Cancel()
	{
		rlGetTaskManager()->CancelTask(&sm_EntitlementStatus);
	}

	void rlRosEntitlementV1::RefreshEntitlementData(const int localGamerIndex, bool refreshArvatoEntitlement)
	{
		sm_LocalGamerIndex = localGamerIndex;

		sm_EntitlementUpToDate = false;
		sm_RefreshEntitlement = true;
		sm_UpdatingEntitlement = true;

		sm_RefreshArvatoEntitlement = refreshArvatoEntitlement;
	}


	eEntitlementV1Status rlRosEntitlementV1::IsEntitledToContent(const char* skuName)
	{
		//@@: range RLROSENTITLEMENTV1_ISENTITLEDTOCONTENT {
		if (IsEntitlementUpToDate() && !IsCheckingForEntitlement() && skuName != NULL)
		{
			rlRosEntitlementV1Data* entitlementData = GetCurrentEntitlementData();

			if (entitlementData)
			{
				// Check to see if we have top-level entitlement for a given piece of content
				rlRosEntitlementV1Sku* topLevelSku = GetCurrentEntitlementData()->GetSkuByName(skuName);

				if (topLevelSku)
					return ENTITLEMENT_OWNED_SKU;

				// ContentName's map to a skuName without the "RS-" prefix
				atString contentName;
				contentName = skuName;
				if (contentName.StartsWith("RS-"))
				{
					contentName.Replace("RS-", "");
				}

				// If we aren't entitled directly to the piece of content, check any child items.
				// R* Pass for instance has a handful of child items that can also entitle single pieces of content
				atMap<u32, rlRosEntitlementV1Sku*>::Iterator iter = sm_CurrentEntitlementData.m_EntitlementSkus.CreateIterator();

				for (iter.Start(); !iter.AtEnd(); iter.Next())
				{
					topLevelSku = iter.GetData();

					if (topLevelSku)
					{
						for (int i = 0; i < topLevelSku->m_ContentArray.GetCount(); i++)
						{
							rlRosEntitlementV1Content* content = topLevelSku->m_ContentArray[i];

							if (content)
							{
								if (!stricmp(content->m_ContentName, contentName))
									return ENTITLEMENT_OWNED_CONTENT;
							}
						}
					}
				}
			}
		}

		return ENTITLEMENT_NOT_OWNED;
		//@@: } RLROSENTITLEMENTV1_ISENTITLEDTOCONTENT
	}

	bool rlRosEntitlementV1::GetContentNameBySku(const char * skuName, char * contentNameOut, unsigned int contentNameLen)
	{
		//@@: range RLROSENTITLEMENTV1_GETCONTENTNAMEBYSKU {
		if (contentNameOut == NULL || skuName == NULL)
			return false;
		//@@: location RLROSENTITLEMENTV1_GETCONTENTNAMEBYSKU_CREATE_ITERATOR
		atMap<u32, rlRosEntitlementV1Sku*>::Iterator iter = sm_CurrentEntitlementData.m_EntitlementSkus.CreateIterator();
		rlRosEntitlementV1Sku* topLevelSku = NULL;

		// ContentName's map to a skuName without the "RS-" prefix
		atString contentNameStr;
		contentNameStr = skuName;
		if (contentNameStr.StartsWith("RS-"))
		{
			contentNameStr.Replace("RS-", "");
		}

		for (iter.Start(); !iter.AtEnd(); iter.Next())
		{
			topLevelSku = iter.GetData();

			if (topLevelSku)
			{
				for (int i = 0; i < topLevelSku->m_ContentArray.GetCount(); i++)
				{
					rlRosEntitlementV1Content* content = topLevelSku->m_ContentArray[i];

					if (content && !stricmp(content->m_ContentName, contentNameStr))
					{
						safecpy(contentNameOut, content->m_ContentName, contentNameLen);
						return true;
					}
				}
			}
		}

		return false;
		//@@: } RLROSENTITLEMENTV1_GETCONTENTNAMEBYSKU
	}


	bool rlRosEntitlementV1::GetEntitlementsBySku(const int localGamerIndex, rlRosEntitlementV1Data* entitlementData, netStatus* status)
	{
		RLENTITLEMENTV1_CREATETASK(rlRosEntitlementV1SkuTask, status, localGamerIndex, entitlementData);
	}

	bool rlRosEntitlementV1::GetEntitlementsBySkuWithSync(const int localGamerIndex, rlRosEntitlementV1Data* entitlementData, netStatus* status)
	{
		RLENTITLEMENTV1_CREATETASK(rlRosEntitlementV1SkuWithSyncTask, status, localGamerIndex, entitlementData);
	}

	bool rlRosEntitlementV1::RegisterPurchaseBySN(const int localGamerIndex, int entitlementProjectId, char* serialNumber, bool* isActivated, char* skuName, 
													unsigned skuNameBufLen, char* friendlyName, unsigned friendlyLength, netStatus* status)
	{
		RLENTITLEMENTV1_CREATETASK(rlRosEntitlementV1RegisterPurchaseBySNTask, status, localGamerIndex,  entitlementProjectId, serialNumber, isActivated, skuName, skuNameBufLen, friendlyName, friendlyLength);
	}

	bool rlRosEntitlementV1::RegisterPurchaseAndActivationBySN(const int localGamerIndex, int entitlementProjectId, char* serialNumber, netStatus* status)
	{
		RLENTITLEMENTV1_CREATETASK(rlRosEntitlementV1RegisterPurchaseAndActivationBySNTask, status, localGamerIndex,  entitlementProjectId, serialNumber);
	}

	bool rlRosEntitlementV1::GetValidateContentPlayerAge(const int localGamerIndex, const char* contentName, netStatus* status)
	{
		RLENTITLEMENTV1_CREATETASK(rlRosEntitlementV1ValidateContentPlayerAgeTask, status, localGamerIndex, contentName);
	}

	bool rlRosEntitlementV1::GetSkuDownloadURL(const int localGamerIndex, const char* skuName, char* downloadUrl, unsigned int downloadUrlBufLen, netStatus* status)
	{
		RLENTITLEMENTV1_CREATETASK(rlRosEntitlementV1DownloadURLTask, status, localGamerIndex, skuName, downloadUrl, downloadUrlBufLen);
	}

}

#endif // #if ENABLE_ENTITLEMENT_V1