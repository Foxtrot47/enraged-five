// 
// rline/rlnpfaulttolerance.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#if RSG_ORBIS

#ifndef RLINE_RLNPFAULTTOLERANCE_H
#define RLINE_RLNPFAULTTOLERANCE_H

#define ENABLE_NP_FAULT_TOLERANCE __BANK

#include "atl/inlist.h"
#include "data/growbuffer.h"
#include "diag/seh.h"
#include "file/device.h"
#include "parser/manager.h"
#include "string/string.h"
#include "string/stringhash.h"

namespace rage
{

#if ENABLE_NP_FAULT_TOLERANCE

#define FAULT_TOLERANCE_CANCEL(identifierHash,taskId)														\
{																											\
	rlNpFaultToleranceRule* rule = g_rlNp.GetFaultTolerance().GetRule(identifierHash);						\
	if (rule != NULL)																						\
	{																										\
		netTask::Cancel(taskId);																			\
	}																										\
}

#define FAULT_TOLERANCE_EXIT(identifierHash)																\
{																										    \
	rlNpFaultToleranceRule* rule = g_rlNp.GetFaultTolerance().GetRule(identifierHash);						\
	if (rule != NULL)																						\
	{																										\
		return false;																						\
	}																										\
}

#define FAULT_TOLERANCE_FAILURE(identifierHash,ret)															\
{																											\
	rlNpFaultToleranceRule* rule = g_rlNp.GetFaultTolerance().GetRule(identifierHash);						\
	if (rule != NULL)																						\
	{																										\
		ret = rule->m_ResponseCode;																			\
	}																										\
}
#else
#define FAULT_TOLERANCE_CANCEL(identifierHash,taskId)
#define FAULT_TOLERANCE_EXIT(identifierHash)
#define FAULT_TOLERANCE_FAILURE(identifierHash,ret)
#endif // ENABLE_NP_FAULT_TOLERANCE


///////////////////////////////////////////////////////////////////////////////
//  rlNpFaultToleranceRule
///////////////////////////////////////////////////////////////////////////////
struct rlNpFaultToleranceRule
{
	bool Init(const char* name, const char* identifier, const char* match, const char* responseCode, bool enabled)
	{
		rtry
		{
			rverify(name, catchall, );
			rverify(identifier, catchall, );
			rverify(match, catchall, );
			rverify(responseCode, catchall, );

			safecpy(m_Name, name);
			safecpy(m_Identifier, identifier);
			safecpy(m_Match, match);
			m_Enabled = enabled;
			m_IdentifierHash = atStringHash(identifier);
			m_ResponseCode = atoi(responseCode);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	char m_Name[40];
	char m_Identifier[128];
	char m_Match[128];
	bool m_Enabled;
	int m_ResponseCode;
	u32 m_IdentifierHash;

	inlist_node<rlNpFaultToleranceRule> m_ListLink;
};

struct rlNpWebApiFaultToleranceRule : public rlNpFaultToleranceRule
{
	bool Init(const char* name, const char* identifier, const char* match, const char* responseCode, u32 delayMs, const char* responseFile, bool enabled)
	{
		rtry
		{
			rverify(rlNpFaultToleranceRule::Init(name, identifier, match, responseCode, enabled), catchall, );
			rverify(responseFile, catchall, );
			safecpy(m_ResponseFile, responseFile);

			m_DelayMs = delayMs;

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	//PURPOSE
	//  Blocks the current thread for the rule's delay (used for work items on their own thread)
	void ApplyDelay();

	//PURPOSE
	// Reads in the response file and copies it to the grow buffer (BLOCKING)
	bool GetResponse(datGrowBuffer* readBuffer, size_t* readSize);

	char m_ResponseFile[128];
	u32 m_DelayMs;
};

class rlNpFaultTolerance
{
public:
	bool Init();
	void AddWidgets();
	void Shutdown();

	rlNpFaultToleranceRule* GetRule(const char * identifier, const char * path);
	rlNpFaultToleranceRule* GetRule(u32 hashStr);

private:
	
	bool ImportRule(parTreeNode* pNode);
	bool ImportWebApiRule(parTreeNode* pNode);

	typedef inlist<rlNpFaultToleranceRule, &rlNpFaultToleranceRule::m_ListLink> rlNpFaultToleranceRules;
	rlNpFaultToleranceRules m_Rules;
};

} // namespace rage

#endif // RLINE_RLNPFAULTTOLERANCE_H
#endif // RSG_ORBIS