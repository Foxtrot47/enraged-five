// 
// rline/rlCredentials.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlcredentials.h"

#if !NET_SECURE_PLATFORM

#if __LIVE 
#error
#endif //Make certain Xenon is considered a secure platform, and does not use rlGamespy

#include "rlgamespy.h"
#include "data/bitbuffer.h"
#include "gamespy/sc/sc.h"
#include "system/memory.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, cred)
#undef __rage_channel
#define __rage_channel rline_cred

bool 
rlCredentials::GetLocalCredentials(rlCredentials* cred)
{
    rlAssert(cred);

    cred->Clear();

#if __GAMESPY
    if(!g_rlGamespy.IsOnline())
    {
        rlError("GetLocalCredentials: Cannot get credentials (not online)");
        return false;
    }

    const GSLoginCertificate* cert = g_rlGamespy.GetLoginCertificate();
    rlAssert(cert);

    CompileTimeAssert(sizeof(GSLoginCertificate) == sizeof(cred->m_PublicData));
    sysMemCpy(cred->m_PublicData, cert, sizeof(cred->m_PublicData));
    cred->m_IsPublicValid = true;

    const GSLoginPrivateData* privateData = g_rlGamespy.GetLoginPrivateData();
    rlAssert(privateData);

    CompileTimeAssert(sizeof(GSLoginPrivateData) == sizeof(cred->m_PrivateData));
    sysMemCpy(cred->m_PrivateData, privateData, sizeof(cred->m_PrivateData));
    cred->m_IsPrivateValid = true;

    return true;
#else
    return false;
#endif
}

rlCredentials::rlCredentials() 
{
    Clear();
}

void 
rlCredentials::Clear()
{
#if __GAMESPY
    m_IsPublicValid = m_IsPrivateValid = false;
    sysMemSet(m_PublicData, 0, sizeof(m_PublicData));
    sysMemSet(m_PrivateData, 0, sizeof(m_PrivateData));
#endif
}

bool 
rlCredentials::IsPublicValid() const
{
#if __GAMESPY
    return m_IsPublicValid;
#else
    return false;
#endif
}

bool 
rlCredentials::IsPrivateValid() const
{
#if __GAMESPY
    return m_IsPrivateValid;
#else
    return false;
#endif
}

bool 
rlCredentials::Export(void* buf,
                      const unsigned sizeofBuf,
                      unsigned* size) const
{
    rlAssert(IsPublicValid());

#if __GAMESPY
    CompileTimeAssert(sizeof(GSLoginCertificate) == sizeof(m_PublicData));
    const GSLoginCertificate* cert = (const GSLoginCertificate*)m_PublicData;
    rlAssert(wsLoginCertIsValid(cert));

    u8 writeBuf[1024];
    unsigned bytesWritten;

    if(!wsLoginCertWriteBinary(cert, (char*)writeBuf, sizeof(writeBuf), &bytesWritten))
    {
        rlError("Export: wsLoginCertWriteBinary failed");
        return false;
    }

    //rlDebug2("Export: Wrote %u bytes", bytesWritten);

    if(bytesWritten > sizeofBuf)
    {
        rlError("Export: Buffer too small (%u bytes, must be at least %u bytes)",
                 sizeofBuf, bytesWritten);
        return false;
    }

    //Sanity checking
    //unsigned bytesRead;
    //if(!wsLoginCertReadBinary((GSLoginCertificate*)cert, (char*)writeBuf, sizeof(writeBuf), &bytesRead))
    //{
    //    rlError("Import: Failed to read login certificate");
    //    return false;
    //}
    //rlDebug2("Export: Read %u bytes", bytesRead);
    //rlAssert(wsLoginCertIsValid(cert) && (bytesRead == bytesWritten));

    sysMemCpy(buf, writeBuf, bytesWritten);

    if(size) {*size = bytesWritten; }

    return true;

#else
    return false;
#endif
}

bool 
rlCredentials::Import(const void* buf,
                      const unsigned sizeofBuf,
                      unsigned* size)
{
    Clear();

#if __GAMESPY
    //Read the certificate
    CompileTimeAssert(sizeof(GSLoginCertificate) == sizeof(m_PublicData));
    GSLoginCertificate* cert = (GSLoginCertificate*)m_PublicData;

    unsigned bytesRead;

    if(!wsLoginCertReadBinary(cert, (char*)buf, sizeofBuf, &bytesRead))
    {
        rlError("Import: Failed to read login certificate");
        return false;
    }

    //rlDebug2("Import: Read %u bytes", bytesRead);

    //rlVerify the certificate
    if(!wsLoginCertIsValid(cert))
    {
        rlError("Import: wsLoginCertIsValid failed");
        return false;
    }

    m_IsPublicValid = true;

    if(size) {*size = bytesRead; }

    return true;
#else
    return false;
#endif
}

bool 
rlCredentials::Encrypt(const u8* plainText,
                       const unsigned plainTextSize,
                       u8* cipherText,
                       const unsigned ASSERT_ONLY(cipherTextMaxSize),
                       unsigned* cipherTextSize) const
{
    rlAssert(IsPublicValid());
    rlAssert(MAX_PLAINTEXT_SIZE >= plainTextSize);
    rlAssert(MIN_CIPHERTEXT_SIZE <= cipherTextMaxSize);
    rlAssert(MAX_CIPHERTEXT_SIZE >= cipherTextMaxSize);

#if __GAMESPY
    CompileTimeAssert(sizeof(GSLoginCertificate) == sizeof(m_PublicData));
    const GSLoginCertificate* cert = (const GSLoginCertificate*)m_PublicData;

    if(gsCryptRSAEncryptBuffer(&cert->mPeerPublicKey, 
                               plainText, 
                               plainTextSize, 
                               cipherText))
    {
        rlError("Encrypt: gsCryptRSAEncryptBuffer failed");
        return false;
    }

    *cipherTextSize = sizeof(SCPeerKeyExchangeMsg);

    return true;
#else
    return false;
#endif
}

bool 
rlCredentials::Decrypt(const u8* cipherText,
                       const unsigned ASSERT_ONLY(cipherTextSize),
                       u8* plainText,
                       const unsigned ASSERT_ONLY(plainTextMaxSize),
                       unsigned* plainTextSize) const
{
    rlAssert(IsPrivateValid());

    rlAssert(MAX_PLAINTEXT_SIZE >= plainTextMaxSize);
    rlAssert(MIN_CIPHERTEXT_SIZE <= cipherTextSize);
    rlAssert(MAX_CIPHERTEXT_SIZE >= cipherTextSize);

#if __GAMESPY   
    CompileTimeAssert(sizeof(GSLoginPrivateData) == sizeof(m_PrivateData));
    const GSLoginPrivateData* privateData = (const GSLoginPrivateData*)m_PrivateData;

    if(gsCryptRSADecryptBuffer(&privateData->mPeerPrivateKey, 
                               cipherText, 
                               plainText,
                               plainTextSize))
    {
        rlError("Decrypt: gsCryptRSADecryptBuffer failed");
        return false;
    }

    return true;
#else
    return false;
#endif
}

bool 
rlCredentials::operator==(const rlCredentials& that) const
{
#if __GAMESPY
    if(!IsPublicValid() || !that.IsPublicValid())
    {
        return false;
    }

    if(IsPrivateValid() != that.IsPrivateValid())
    {
        return false;
    }

    return (0 == memcmp(m_PublicData, that.m_PublicData, sizeof(m_PublicData)))
           && (0 == memcmp(m_PrivateData, that.m_PrivateData, sizeof(m_PrivateData)));
#else
    return false;
#endif
}

bool 
rlCredentials::operator!=(const rlCredentials& that) const
{
#if __GAMESPY
    return !(*this == that);
#else
    return false;
#endif
}

}   // namespace rage

#endif //!NET_SECURE_PLATFORM
