// 
// rline/rlmailboxtasks.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlmailboxtasks.h"
#include "rlgamerinfo.h"
#include "rlmailbox.h"
#include "net/status.h"

using namespace rage;

///////////////////////////////////////////////////////////////////////////////
// rlMailboxTask
///////////////////////////////////////////////////////////////////////////////
rlMailboxTask::rlMailboxTask() 
: m_LocalGamerIndex(-1)
, m_MailboxName(0) 
{
}

rlMailboxTask::~rlMailboxTask() 
{
}

void 
rlMailboxTask::Configure(rlMailbox* mailbox,
                         const int localGamerIndex,
                         const char* mailboxName,
                         netStatus* status)
{
    rlTask<rlMailbox>::Configure(mailbox, status);
    m_LocalGamerIndex = localGamerIndex;
    m_MailboxName = mailboxName;
}

int 
rlMailboxTask::GetLocalGamerIndex() const 
{ 
    return m_LocalGamerIndex; 
}

const char* 
rlMailboxTask::GetMailboxName() const 
{ 
    return m_MailboxName; 
}

void 
rlMailboxTask::SetRecordId(rlMailboxRecord* r, const rlMailboxRecordId& id) 
{ 
    r->m_RecordId = id;
}

void 
rlMailboxTask::SetSenderId(rlMailboxRecord* r, const s64& id) 
{ 
    r->m_SenderHandle.ResetRockon(id);
}

void 
rlMailboxTask::SetUserDataLength(rlMailboxRecord* r, const unsigned len) 
{ 
    r->SetUserDataLength(len);
};

void 
rlMailboxTask::SetMsgDataLength(rlMailboxRecord* r, const unsigned len) 
{ 
    r->SetMsgDataLength(len);
};

rlRockonManager* 
rlMailboxTask::GetRockonManager()
{
    return m_Ctx->GetRockonManager();
}



///////////////////////////////////////////////////////////////////////////////
// rlMailboxBroadcastMsgTask
///////////////////////////////////////////////////////////////////////////////
void 
rlMailboxBroadcastMsgTask::Configure(rlMailbox* mailbox,
                                     const int localGamerIndex,
                                     const char* mailboxName, 
                                     const u8* userData,
                                     const unsigned userDataLength,
                                     const u8* msgData,
                                     const unsigned msgDataLength,
                                     const unsigned expirationMinutes,
                                     rlMailboxRecordId* recordId,
                                     netStatus* status)
{
    rlMailboxTask::Configure(mailbox, localGamerIndex, mailboxName, status);

    m_UserData = userData;
    m_UserDataLength = userDataLength;
    m_MsgData = msgData;
    m_MsgDataLength = msgDataLength;
    m_ExpirationMinutes = expirationMinutes;
    m_RecordId = recordId;
}

void 
rlMailboxBroadcastMsgTask::Start()
{
    rlMailboxTask::Start();

    if(!rlGamerInfo::GetDisplayName(GetLocalGamerIndex(), m_SenderDisplayName, sizeof(m_SenderDisplayName)))
    {
        Finish(FINISH_FAILED, -1);
    }

    if(!rlRockonMailboxServiceProxy::BroadcastMessage(
        m_Ctx->GetRockonManager(),
        GetLocalGamerIndex(),
        GetMailboxName(),
        m_SenderDisplayName,
        m_UserData,
        m_UserDataLength,
        m_MsgData,
        m_MsgDataLength,
        m_ExpirationMinutes,
        m_RecordId,
        &m_MyStatus))
    {
        Finish(FINISH_FAILED, -1);
    }
}

void 
rlMailboxBroadcastMsgTask::Update(const unsigned timestep)
{
    rlMailboxTask::Update(timestep);

    if(m_MyStatus.Succeeded())
    {
        Finish(FINISH_SUCCEEDED, 0);
    }
    else if(!m_MyStatus.Pending())
    {
        Finish(FINISH_FAILED, -1);
    }
}



///////////////////////////////////////////////////////////////////////////////
// rlMailboxSendMsgTask
///////////////////////////////////////////////////////////////////////////////
void 
rlMailboxSendMsgTask::Configure(rlMailbox* mailbox,
                                         const int localGamerIndex,
                                         const char* mailboxName, 
                                         const rlGamerHandle& gamerHandle,
                                         const u8* userData,
                                         const unsigned userDataLength,
                                         const u8* msgData,
                                         const unsigned msgDataLength,
                                         const unsigned expirationMinutes,
                                         rlMailboxRecordId* recordId,
                                         netStatus* status)
{
    rlMailboxTask::Configure(mailbox, localGamerIndex, mailboxName, status);

    m_ReceiverGamerHandle = gamerHandle;
    m_UserData = userData;
    m_UserDataLength = userDataLength;
    m_MsgData = msgData;
    m_MsgDataLength = msgDataLength;
    m_ExpirationMinutes = expirationMinutes;
    m_RecordId = recordId;
}

void 
rlMailboxSendMsgTask::Start()
{
    rlMailboxTask::Start();

    if(!rlGamerInfo::GetDisplayName(GetLocalGamerIndex(), m_SenderDisplayName, sizeof(m_SenderDisplayName)))
    {
        Finish(FINISH_FAILED, -1);
    }

    if(!m_ReceiverGamerHandle.IsValid()
        || (m_ReceiverGamerHandle.GetService() != rlGamerHandle::SERVICE_ROCKON))
    {
        Finish(FINISH_FAILED, -1);
    }

    if(!rlRockonMailboxServiceProxy::SendMessage(
        m_Ctx->GetRockonManager(),
        GetLocalGamerIndex(),
        GetMailboxName(),
        m_SenderDisplayName,
        m_ReceiverGamerHandle.GetRockonAccountId(),
        m_UserData,
        m_UserDataLength,
        m_MsgData,
        m_MsgDataLength,
        m_ExpirationMinutes,
        m_RecordId,
        &m_MyStatus))
    {
        Finish(FINISH_FAILED, -1);
    }
}

void 
rlMailboxSendMsgTask::Update(const unsigned timestep)
{
    rlMailboxTask::Update(timestep);

    if(m_MyStatus.Succeeded())
    {
        Finish(FINISH_SUCCEEDED, 0);
    }
    else if(!m_MyStatus.Pending())
    {
        Finish(FINISH_FAILED, -1);
    }
}

///////////////////////////////////////////////////////////////////////////////
// rlMailboxGetNumRecordsTask
///////////////////////////////////////////////////////////////////////////////
void 
rlMailboxGetNumRecordsTask::Configure(rlMailbox* mailbox,
                                     const int localGamerIndex,
                                     const char* mailboxName,                     
                                     const rlMailboxRecordId startRecordId,
                                     const rlMailboxRecordId endRecordId,
                                     unsigned* numRecords,
                                     netStatus* status)
{
    rlMailboxTask::Configure(mailbox, localGamerIndex, mailboxName, status);

    m_StartRecordId = startRecordId;
    m_EndRecordId = endRecordId;
    m_NumRecords = numRecords;
}

void 
rlMailboxGetNumRecordsTask::Start()
{
    rlMailboxTask::Start();

    if(!rlRockonMailboxServiceProxy::GetNumRecords(
        m_Ctx->GetRockonManager(),
        GetLocalGamerIndex(),
        GetMailboxName(),
        m_StartRecordId,
        m_EndRecordId,
        false,
        m_NumRecords,
        &m_MyStatus))
    {
        Finish(FINISH_FAILED, -1);
    }
}

void 
rlMailboxGetNumRecordsTask::Update(const unsigned timestep)
{
    rlMailboxTask::Update(timestep);

    if(m_MyStatus.Succeeded())
    {
        Finish(FINISH_SUCCEEDED, 0);
    }
    else if(!m_MyStatus.Pending())
    {
        Finish(FINISH_FAILED, -1);
    }
}


///////////////////////////////////////////////////////////////////////////////
// rlMailboxReadMsgDataTask
///////////////////////////////////////////////////////////////////////////////
void 
rlMailboxReadMsgDataTask::Configure(rlMailbox* mailbox,
                                   const int localGamerIndex,
                                   const char* mailboxName,                     
                                   const rlMailboxRecordId recordId,
                                   u8* msgData,
                                   const unsigned msgDataMaxLength,
                                   unsigned* msgDataLength,
                                   netStatus* status)
{
    rlMailboxTask::Configure(mailbox, localGamerIndex, mailboxName, status);

    m_RecordId = recordId;
    m_MsgData = msgData;
    m_MsgDataMaxLength = msgDataMaxLength;
    m_MsgDataLength = msgDataLength;

    m_ProxyBuf.Reset(m_MsgData, m_MsgDataMaxLength);
}

void 
rlMailboxReadMsgDataTask::Start()
{
    rlMailboxTask::Start();

    if(!rlRockonMailboxServiceProxy::ReadMsgData(
        m_Ctx->GetRockonManager(),
        GetLocalGamerIndex(),
        GetMailboxName(),
        m_RecordId,
        &m_ProxyBuf,
        &m_MyStatus))
    {
        Finish(FINISH_FAILED, -1);
    }
}

void 
rlMailboxReadMsgDataTask::Update(const unsigned timestep)
{
    rlMailboxTask::Update(timestep);

    if(m_MyStatus.Succeeded())
    {
        *m_MsgDataLength = m_ProxyBuf.m_Length;

        Finish(FINISH_SUCCEEDED, 0);
    }
    else if(!m_MyStatus.Pending())
    {
        Finish(FINISH_FAILED, -1);
    }
}

///////////////////////////////////////////////////////////////////////////////
// rlMailboxDeleteRecordsTask
///////////////////////////////////////////////////////////////////////////////
void 
rlMailboxDeleteRecordsTask::Configure(rlMailbox* mailbox,
                                      const int localGamerIndex,
                                      const char* mailboxName,                     
                                      const rlMailboxRecordId startRecordId,
                                      const rlMailboxRecordId endRecordId,
                                      unsigned* numDeleted,
                                      netStatus* status)
{
    rlMailboxTask::Configure(mailbox, localGamerIndex, mailboxName, status);

    m_StartRecordId = startRecordId;
    m_EndRecordId = endRecordId;
    m_NumDeleted = numDeleted;
}

void 
rlMailboxDeleteRecordsTask::Start()
{
    rlMailboxTask::Start();

    if(!rlRockonMailboxServiceProxy::DeleteRecords(
        m_Ctx->GetRockonManager(),
        GetLocalGamerIndex(),
        GetMailboxName(),
        m_StartRecordId,
        m_EndRecordId,
        m_NumDeleted,
        &m_MyStatus))
    {
        Finish(FINISH_FAILED, -1);
    }
}

void 
rlMailboxDeleteRecordsTask::Update(const unsigned timestep)
{
    rlMailboxTask::Update(timestep);

    if(m_MyStatus.Succeeded())
    {
        Finish(FINISH_SUCCEEDED, 0);
    }
    else if(!m_MyStatus.Pending())
    {
        Finish(FINISH_FAILED, -1);
    }
}
