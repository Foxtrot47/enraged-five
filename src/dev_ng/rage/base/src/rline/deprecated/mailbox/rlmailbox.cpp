// 
// rline/rlMailbox.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlmailbox.h"
#include "rlrockonmailboxserviceproxy.h"
#include "system/memory.h"
#include "system/timer.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, mailbox)

#define rlMailboxDebug1(fmt, ...)    RAGE_DEBUGF1(rline_mailbox, fmt, ##__VA_ARGS__)
#define rlMailboxDebug2(fmt, ...)    RAGE_DEBUGF2(rline_mailbox, fmt, ##__VA_ARGS__)
#define rlMailboxDebug3(fmt, ...)    RAGE_DEBUGF3(rline_mailbox, fmt, ##__VA_ARGS__)
#define rlMailboxWarning(fmt, ...)   RAGE_WARNINGF(rline_mailbox, fmt, ##__VA_ARGS__)
#define rlMailboxError(fmt, ...)     RAGE_ERRORF(rline_mailbox, fmt, ##__VA_ARGS__)

#define RLMAILBOX_CREATETASK(taskname, ...) \
    taskname* task = RL_NEW(rlMailbox, taskname); \
    if(rlVerify(task)) \
    { \
        task->Configure(this, __VA_ARGS__); \
        SYS_CS_SYNC(m_TaskListCs); \
        m_TaskList.push_back(task); \
        return true; \
    } \
    else \
    { \
        return false; \
    }

///////////////////////////////////////////////////////////////////////////////
// rlMailbox
///////////////////////////////////////////////////////////////////////////////
rlMailbox::rlMailbox()
: m_Initialized(false)
, m_LastUpdateTime(0)
{
}

rlMailbox::~rlMailbox()
{
    this->Shutdown();
}

bool
rlMailbox::Init(rlRockonManager* rm)
{
    rlAssert(rm);
    rlAssert(!m_Initialized);

    m_RockonMgr = rm;

    m_Initialized = true;

    return m_Initialized;
}

void
rlMailbox::Shutdown()
{
    if(!m_Initialized) return;

    SYS_CS_SYNC(m_TaskListCs);

    while(!m_TaskList.empty())
    {
        rlMailboxTask* task = *m_TaskList.begin();
        m_TaskList.erase(m_TaskList.begin());       
        rlDelete(task);
    }

    m_LastUpdateTime = 0;

    m_Initialized = false;
}

void
rlMailbox::Update()
{
    rlAssert(m_Initialized);

    const unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;
    unsigned deltaMs = m_LastUpdateTime ? (curTime - m_LastUpdateTime) : 0;
    m_LastUpdateTime = curTime;

    bool done = false;

    SYS_CS_SYNC(m_TaskListCs);

    while(!done && !m_TaskList.empty())
    {
        rlMailboxTask* task = m_TaskList.front();

        if(!task->IsStarted())
        {
            task->Start();
            done = true;
        }

        if(task->IsActive())
        {
            task->Update(deltaMs);
            done = true;
        }

        if(task->IsFinished())
        {
            m_TaskList.pop_front();
            rlDelete(task);
        }
    }
}

bool 
rlMailbox::BroadcastMsg(const int localGamerIndex,
                        const char* mailboxName,                     
                        const u8* userData,
                        const unsigned userDataLength,
                        const u8* msgData,
                        const unsigned msgDataLength,
                        const unsigned expirationMinutes,
                        rlMailboxRecordId* recordId,
                        netStatus* status)
{
    rlAssert(m_Initialized);

    RLMAILBOX_CREATETASK(rlMailboxBroadcastMsgTask, 
        localGamerIndex, 
        mailboxName, 
        userData,
        userDataLength,
        msgData,
        msgDataLength,
        expirationMinutes,
        recordId,
        status);
}

bool 
rlMailbox::SendMsg(const int localGamerIndex,
                   const char* mailboxName,                     
                   const rlGamerHandle gamerHandle,
                   const u8* userData,
                   const unsigned userDataLength,
                   const u8* msgData,
                   const unsigned msgDataLength,
                   const unsigned expirationMinutes,
                   rlMailboxRecordId* recordId,
                   netStatus* status)
{
    rlAssert(m_Initialized);

    RLMAILBOX_CREATETASK(rlMailboxSendMsgTask, 
        localGamerIndex, 
        mailboxName, 
        gamerHandle,
        userData,
        userDataLength,
        msgData,
        msgDataLength,
        expirationMinutes,
        recordId,
        status);
}

bool 
rlMailbox::GetNumRecords(const int localGamerIndex,
                         const char* mailboxName,                       
                         const rlMailboxRecordId startRecordId,
                         const rlMailboxRecordId endRecordId,
                         unsigned* numRecords,
                         netStatus* status)
{
    rlAssert(m_Initialized);

    RLMAILBOX_CREATETASK(rlMailboxGetNumRecordsTask,
        localGamerIndex, 
        mailboxName, 
        startRecordId,
        endRecordId,
        numRecords,
        status);
}

bool 
rlMailbox::ReadMsgData(const int localGamerIndex,
                       const char* mailboxName,                     
                       const rlMailboxRecordId recordId,
                       u8* msgData,
                       const unsigned msgDataMaxLength,
                       unsigned* msgDataLength,
                       netStatus* status)
{
    rlAssert(m_Initialized);

    RLMAILBOX_CREATETASK(rlMailboxReadMsgDataTask,
        localGamerIndex, 
        mailboxName, 
        recordId,
        msgData,
        msgDataMaxLength,
        msgDataLength,
        status);
}

bool 
rlMailbox::DeleteRecords(const int localGamerIndex,
                         const char* mailboxName,                     
                         const rlMailboxRecordId startRecordId,
                         const rlMailboxRecordId endRecordId,
                         unsigned* numDeleted,
                         netStatus* status)
{
    rlAssert(m_Initialized);

    RLMAILBOX_CREATETASK(rlMailboxDeleteRecordsTask, 
        localGamerIndex, 
        mailboxName, 
        startRecordId, 
        endRecordId, 
        numDeleted,
        status);
}

}   //namespace rage
