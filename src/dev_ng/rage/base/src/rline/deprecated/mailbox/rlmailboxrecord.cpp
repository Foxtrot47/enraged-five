// 
// rline/rlmailboxrecord.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlmailbox.h"

using namespace rage;

rlMailboxRecord::rlMailboxRecord() 
{ 
    Clear(); 
}

rlMailboxRecord::~rlMailboxRecord() 
{
}

void 
rlMailboxRecord::Clear()
{
    m_RecordId = 0;
    m_SenderHandle.Clear();
    sysMemSet(m_SenderDisplayName, 0, sizeof(m_SenderDisplayName));
    m_UserDataLength = 0;
    m_MsgDataLength = 0;
}

s64 
rlMailboxRecord::GetRecordId() const 
{ 
    return m_RecordId; 
}

const rlGamerHandle&
rlMailboxRecord::GetSenderHandle() const 
{ 
    return m_SenderHandle; 
}

const char* 
rlMailboxRecord::GetSenderDisplayName() const 
{ 
    return m_SenderDisplayName; 
}

unsigned 
rlMailboxRecord::GetSenderDisplayNameMaxLength() const 
{ 
    return sizeof(m_SenderDisplayName); 
}

const u8* 
rlMailboxRecord::GetUserData() const 
{ 
    return 0; 
}

const u8* 
rlMailboxRecord::GetUserDataBuffer() const 
{ 
    return 0; 
}

unsigned 
rlMailboxRecord::GetUserDataMaxLength() const 
{ 
    return 0; 
}

unsigned 
rlMailboxRecord::GetUserDataLength() const 
{ 
    return m_UserDataLength; 
}   

const u8* 
rlMailboxRecord::GetMsgData() const 
{ 
    return 0; 
}

const u8* 
rlMailboxRecord::GetMsgDataBuffer() const 
{ 
    return 0; 
}

unsigned 
rlMailboxRecord::GetMsgDataMaxLength() const 
{ 
    return 0; 
}

unsigned 
rlMailboxRecord::GetMsgDataLength() const 
{ 
    return m_MsgDataLength; 
}

void 
rlMailboxRecord::SetRecordId(const rlMailboxRecordId& id) 
{ 
    m_RecordId = id; 
}

void 
rlMailboxRecord::SetUserDataLength(const unsigned len) 
{ 
    rlVerify(!GetUserData() || (len <= GetUserDataMaxLength())); 
    m_UserDataLength = len; 
};

void 
rlMailboxRecord::SetMsgDataLength(const unsigned len) 
{ 
    rlVerify(!GetMsgData() || (len <= GetMsgDataMaxLength())); 
    m_MsgDataLength = len; 
};

const char*
rlMailboxRecord::GetSendDateTimeStr() const
{ 
    return m_SendDateTimeStr;
};

const char*
rlMailboxRecord::GetExpirationDateTimeStr() const
{ 
    return m_ExpirationDateTimeStr;
};
