// 
// rline/rlmailboxtasks.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLMAILBOXTASKS_H
#define RLINE_RLMAILBOXTASKS_H

#include "rlgamerinfo.h"
#include "rlmailboxrecord.h"
#include "rlrockonmailboxserviceproxy.h"
#include "rltask.h"
#include "atl/inlist.h"
#include "net/status.h"

namespace rage
{

//PURPOSE
//  rlMailbox internally keeps a queue of tasks to carry out
//  user requests.  This is the base class for those tasks.
//  Users never directly instantiate any of these classes.
class rlMailboxTask : public rlTask<class rlMailbox>
{
public:
    RL_DECL_TASK(rlMailboxTask);
    
    rlMailboxTask();    
    virtual ~rlMailboxTask();

    void Configure(rlMailbox* mailbox,
                   const int localGamerIndex,
                   const char* mailboxName,
                   netStatus* status);

    int GetLocalGamerIndex() const;

    const char* GetMailboxName() const;

protected:
    //Methods to allow derived tasks to modify rlMailboxRecord without us
    //having to friend them all.
    void SetRecordId(rlMailboxRecord* r, const rlMailboxRecordId& id);
    void SetSenderId(rlMailboxRecord* r, const s64& id);
    void SetUserDataLength(rlMailboxRecord* r, const unsigned len);
    void SetMsgDataLength(rlMailboxRecord* r, const unsigned len);

    rlRockonManager* GetRockonManager();

private:
    rlMailboxTask(const rlMailboxTask& rhs);
    int m_LocalGamerIndex;
    const char* m_MailboxName;

    friend class rlMailbox; //To have access to m_ListLink.
    inlist_node<rlMailboxTask> m_ListLink;
};

///////////////////////////////////////////////////////////////////////////////
// rlMailboxBroadcastMsgTask
///////////////////////////////////////////////////////////////////////////////
class rlMailboxBroadcastMsgTask : public rlMailboxTask
{
public:
    RL_DECL_TASK(rlMailboxBroadcastMsgTask);

    rlMailboxBroadcastMsgTask() {}
    virtual ~rlMailboxBroadcastMsgTask() {}

    void Configure(rlMailbox* mailbox,
                   const int localGamerIndex,
                   const char* mailboxName,                     
                   const u8* userData,
                   const unsigned userDataLength,
                   const u8* msgData,
                   const unsigned msgDataLength,
                   const unsigned expirationMinutes,
                   rlMailboxRecordId* recordId,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timestep);

private:
    char m_SenderDisplayName[RL_MAX_DISPLAY_NAME_CHARS];
    const u8* m_UserData;
    unsigned m_UserDataLength;
    const u8* m_MsgData;
    unsigned m_MsgDataLength;
    unsigned m_ExpirationMinutes;
    rlMailboxRecordId* m_RecordId;
    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlMailboxSendMsgTask
///////////////////////////////////////////////////////////////////////////////
class rlMailboxSendMsgTask : public rlMailboxTask
{
public:
    RL_DECL_TASK(rlMailboxSendMsgTask);

    rlMailboxSendMsgTask() {}
    virtual ~rlMailboxSendMsgTask() {}

    void Configure(rlMailbox* mailbox,
        const int localGamerIndex,
        const char* mailboxName,                     
        const rlGamerHandle& gamerHandle,
        const u8* userData,
        const unsigned userDataLength,
        const u8* msgData,
        const unsigned msgDataLength,
        const unsigned expirationMinutes,
        rlMailboxRecordId* recordId,
        netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timestep);

private:
    char m_SenderDisplayName[RL_MAX_DISPLAY_NAME_CHARS];
    rlGamerHandle m_ReceiverGamerHandle;
    const u8* m_UserData;
    unsigned m_UserDataLength;
    const u8* m_MsgData;
    unsigned m_MsgDataLength;
    unsigned m_ExpirationMinutes;
    rlMailboxRecordId* m_RecordId;
    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlMailboxGetNumRecordsTask
///////////////////////////////////////////////////////////////////////////////
class rlMailboxGetNumRecordsTask : public rlMailboxTask
{
public:
    RL_DECL_TASK(rlMailboxGetNumRecordsTask);

    rlMailboxGetNumRecordsTask() {}
    virtual ~rlMailboxGetNumRecordsTask() {}

    void Configure(rlMailbox* mailbox,
        const int localGamerIndex,
        const char* mailboxName,  
        const rlMailboxRecordId startRecordId,
        const rlMailboxRecordId endRecordId,
        unsigned* numRecords,
        netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timestep);

private:
    rlMailboxRecordId m_StartRecordId;
    rlMailboxRecordId m_EndRecordId;
    unsigned* m_NumRecords;
    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlMailboxReadMsgDataTask
///////////////////////////////////////////////////////////////////////////////
class rlMailboxReadMsgDataTask : public rlMailboxTask
{
public:
    RL_DECL_TASK(rlMailboxReadMsgDataTask);

    rlMailboxReadMsgDataTask() {}
    virtual ~rlMailboxReadMsgDataTask() {}

    void Configure(rlMailbox* mailbox,
        const int localGamerIndex,
        const char* mailboxName,                     
        const rlMailboxRecordId recordId,
        u8* msgData,
        const unsigned msgDataMaxLength,
        unsigned* msgDataLength,
        netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timestep);

private:
    rlMailboxRecordId m_RecordId;
    u8* m_MsgData;
    unsigned m_MsgDataMaxLength;
    unsigned* m_MsgDataLength;
    rlRockonProxyBuffer<u8> m_ProxyBuf;
    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlMailboxDeleteRecordsTask
///////////////////////////////////////////////////////////////////////////////
class rlMailboxDeleteRecordsTask : public rlMailboxTask
{
public:
    RL_DECL_TASK(rlMailboxDeleteRecordsTask);

    rlMailboxDeleteRecordsTask() {}
    virtual ~rlMailboxDeleteRecordsTask() {}

    void Configure(rlMailbox* mailbox,
                   const int localGamerIndex,
                   const char* mailboxName,                     
                   const rlMailboxRecordId startRecordId,
                   const rlMailboxRecordId endRecordId,
                   unsigned* numDeleted,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timestep);

private:
    rlMailboxRecordId m_StartRecordId;
    rlMailboxRecordId m_EndRecordId;
    unsigned* m_NumDeleted;
    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlMailboxReadRecordsTask
///////////////////////////////////////////////////////////////////////////////
template<class T>
class rlMailboxReadRecordsTask : public rlMailboxTask
{
public:
    RL_DECL_TASK(rlMailboxReadRecordsTask);

    rlMailboxReadRecordsTask()
    : m_ProxyRecords(0)
    {
    }

    virtual ~rlMailboxReadRecordsTask()
    {
        if(m_ProxyRecords)
        {
            rlDelete(m_ProxyRecords, m_MaxRecords);
            m_ProxyRecords = 0;
        }
    }

    void Configure(rlMailbox* mailbox,
                   const int localGamerIndex,
                   const char* mailboxName,                     
                   const rlMailboxRecordId startRecordId,
                   const rlMailboxRecordId endRecordId,
                   const unsigned maxRecords,
                   const bool oldestFirst,
                   T* records,
                   unsigned* numRecords,
                   netStatus* status)
    {
        rlMailboxTask::Configure(mailbox, localGamerIndex, mailboxName, status);

        m_StartRecordId = startRecordId;
        m_EndRecordId = endRecordId;
        m_MaxRecords = maxRecords;
        m_OldestFirst = oldestFirst;
        m_Records = records;
        m_NumRecords = numRecords;

        m_ProxyRecords = RL_NEW_ARRAY(rlMailboxReadRecordsTask, rlRockonMailboxServiceProxy::MailboxRecord, m_MaxRecords);
        if(!rlVerify(m_ProxyRecords))
        {
            //rlMailboxError("Failed to allocate proxy mailbox records");
            Finish(FINISH_FAILED, -1);
            return;
        }

        for(unsigned i = 0; i < m_MaxRecords; i++)
        {
            rlMailboxRecord* r = (rlMailboxRecord*)&m_Records[i];
            rlRockonMailboxServiceProxy::MailboxRecord& pr = m_ProxyRecords[i];

            r->Clear();

            //Init the proxy record buffers to point to memory we own.
            pr.m_SenderDisplayName.Reset((char*)r->GetSenderDisplayName(),
                                         r->GetSenderDisplayNameMaxLength());

            pr.m_UserData.Reset((u8*)r->GetUserDataBuffer(), r->GetUserDataMaxLength());

            pr.m_MsgData.Reset((u8*)r->GetMsgDataBuffer(), r->GetMsgDataMaxLength());

            pr.m_SendDateTime.Reset((char*)r->GetSendDateTimeStr(), 
                rlMailboxRecord::MAX_DATETIME_STRING_LENGTH);

            pr.m_ExpirationDateTime.Reset((char*)r->GetExpirationDateTimeStr(), 
                rlMailboxRecord::MAX_DATETIME_STRING_LENGTH);
        }
    }


    virtual void Start()
    {
        rlMailboxTask::Start();

        rlMailboxRecord* r = (rlMailboxRecord*)&m_Records[0];

        if(!rlRockonMailboxServiceProxy::ReadRecords(
            GetRockonManager(),
            GetLocalGamerIndex(),
            GetMailboxName(),
            m_StartRecordId,
            m_EndRecordId,
            m_MaxRecords,
            r->GetUserDataMaxLength() > 0,
            r->GetMsgDataMaxLength() > 0,
            m_OldestFirst,
            m_ProxyRecords,
            m_NumRecords,
            &m_MyStatus))
        {
            Finish(FINISH_FAILED, -1);
        }
    }

    virtual void Update(const unsigned timestep)
    {
        rlMailboxTask::Update(timestep);

        if(m_MyStatus.Succeeded())
        {
            //Copy data from proxy records
            for(unsigned i = 0; i < *m_NumRecords; i++)
            {
                rlMailboxRecord* r = (rlMailboxRecord*)&m_Records[i];

                SetRecordId(r, m_ProxyRecords[i].m_RecordId);
                SetSenderId(r, m_ProxyRecords[i].m_SenderId);
                SetUserDataLength(r, m_ProxyRecords[i].m_UserDataLength);
                SetMsgDataLength(r, m_ProxyRecords[i].m_MsgDataLength);
            }

            Finish(FINISH_SUCCEEDED, 0);
        }
        else if(!m_MyStatus.Pending())
        {
            Finish(FINISH_FAILED, -1);
        }
    }

private:
    rlMailboxRecordId m_StartRecordId;
    rlMailboxRecordId m_EndRecordId;
    unsigned m_MaxRecords;
    bool m_OldestFirst;
    T* m_Records;
    unsigned* m_NumRecords;
    rlRockonMailboxServiceProxy::MailboxRecord* m_ProxyRecords;
    netStatus m_MyStatus;
};

}   //namespace rage

#endif  //RLINE_RLMAILBOXTASKS_H
