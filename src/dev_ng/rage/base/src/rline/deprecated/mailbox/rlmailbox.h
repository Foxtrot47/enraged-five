// 
// rline/rlmailbox.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLMAILBOX_H
#define RLINE_RLMAILBOX_H

#include "rlmailboxrecord.h"
#include "rlmailboxtasks.h"
#include "atl/inlist.h"
#include "net/status.h"
#include "system/criticalsection.h"

namespace rage
{

class rlRockonManager;

//PURPOSE
//  Persistent messaging API.
class rlMailbox
{
public:
    //Convenience constants for expiration times passed to message
    //sending methods.  Regardless of the expiration time specified,
    //it will be capped serverside by the mailbox's limits.
    enum 
    {
        MIN_EXPIRATION_MINUTES = 0,
        MAX_EXPIRATION_MINUTES = 0xFFFFFFFF
    };

    rlMailbox();
    ~rlMailbox();

    //PURPOSE
    //  Initializes the instance.  Returns true on success.
    bool Init(rlRockonManager* rm);
    
    //PURPOSE
    //  Shuts down the instance.
    void Shutdown();
    
    //PURPOSE
    //  Updates state.  This should be called frequently, ideally every frame
    void Update();

    //PURPOSE
    //  Returns rlRockonManager we were initialized with.
    rlRockonManager* GetRockonManager() const { return m_RockonMgr; }

    //PURPOSE
    //  Broadcasts a message to the specified mailbox.  All gamers that can read from
    //  the mailbox (including the sender) will be able to read the message.
    //
    //  Since the broadcast might not be deletable by gamers, an expiration time (specified
    //  in minutes) must be supplied.  This time will be constrained by the limits
    //  set on the mailbox.
    //
    //NOTE
    //  There are a number of ways this can fail, including:
    //  - The gamer does not have permission to broadcast in this mailbox
    //  - The userdata or msg data size exceeds the mailbox's limits
    //  - The gamer already has too many sent messages in the mailbox
    //
    //  Userdata can be zero length, but the msg data must be nonzero length.
    //
    bool BroadcastMsg(const int localGamerIndex,
                      const char* mailboxName,                     
                      const u8* userData,
                      const unsigned userDataLength,
                      const u8* msgData,
                      const unsigned msgDataLength,
                      const unsigned expirationMinutes,
                      rlMailboxRecordId* recordId,
                      netStatus* status);

    //PURPOSE
    //  Sends a message to a specific gamer, identified by an rlGamerHandle whose
    //  service is SERVICE_ROCKON.
    //
    //  As with broadcasts, an expiration time (specified in minutes) must be supplied.  
    //  This time will be constrained by the limits set on the mailbox.
    //
    //NOTE
    //  There are a number of ways this can fail, including:
    //  - The gamer does not have permission to send personal messages in this mailbox
    //  - The userdata or msg data size exceeds the mailbox's limits
    //  - The gamer already has too many sent messages in the mailbox
    //  - The receiver already has too many messages in the mailbox
    //
    //  Userdata can be zero length, but the msg data must be nonzero length.
    //
    bool SendMsg(const int localGamerIndex,
                 const char* mailboxName,                     
                 const rlGamerHandle gamerHandle,
                 const u8* userData,
                 const unsigned userDataLength,
                 const u8* msgData,
                 const unsigned msgDataLength,
                 const unsigned expirationMinutes,
                 rlMailboxRecordId* recordId,
                 netStatus* status);

    //PURPOSE
    //  Gets the number of messages that can be read by a local gamer in a mailbox.
    //  Your specify a range of record IDs to count within, which are interpreted as:
    //      0, 0    All records
    //      0, n    All records up to and including ID n
    //      n, 0    All records after and including ID n
    //      n, n    Just record n
    //      m, n    All records between and including IDs m and n
    //
    //  Only records the local gamer can read are counted.  Usually this will be
    //  messages addressed to them, or broadcasts. Sometimes, though, the gamer may 
    //  be prohibited from reading these messages (ex. they've been banned).
    //
    //RETURNS
    //  False if the operation failed to start.
    bool GetNumRecords(const int localGamerIndex,
                       const char* mailboxName,                     
                       const rlMailboxRecordId startRecordId,
                       const rlMailboxRecordId endRecordId,
                       unsigned* numRecords,
                       netStatus* status);

    //PURPOSE
    //  Reads one or more records for a local gamer from specified mailbox.
    //  Your specify a range of record IDs, which are interpreted as follows:
    //      0, 0    All records
    //      0, n    All records up to and including ID n
    //      n, 0    All records after and including ID n
    //      n, n    Just record n
    //      m, n    All records between and including IDs m and n
    //
    //  Only records the local gamer can read are returned.  Usually this will be
    //  messages addressed to them, or broadcasts. Sometimes, though, the gamer 
    //  may be prohibited from reading these messages (ex. they've been banned).
    //
    //  "oldestFirst" determines the order that results are returned in the
    //  records array.  If true, the oldest record will be at index 0.  If false,
    //  the newest will be there.
    //
    //RETURNS
    //  False if the operation failed to start, true otherwise.
    //  The success/failure of the operation after the call returns can be 
    //  monitored with the status object.
    //  Note that it is possible for the read to succeed, yet return no
    //  records.
    //
    //NOTE
    //  The type of record you pass in determines whether the user and msg data
    //  is read.  If you pass in an rlMailboxRecordPartial, then user data will
    //  be read. An rlMailboxRecordFull will read both user and msg data.
    //  An instance of the base rlMailboxRecord class will read neither, which
    //  can be useful if you're just trying to get minimal information, like if
    //  you don't have enough memory to store all the downloaded msg data.
    //
    template<typename T>
    bool ReadRecords(const int localGamerIndex,
                     const char* mailboxName,                     
                     const rlMailboxRecordId startRecordId,
                     const rlMailboxRecordId endRecordId,
                     const unsigned maxRecords,
                     const bool oldestFirst,
                     T* records,
                     unsigned* numRecords,
                     netStatus* status)
    {
        return CommonReadRecords(localGamerIndex, 
            mailboxName, 
            startRecordId, 
            endRecordId, 
            maxRecords, 
            oldestFirst, 
            records, 
            numRecords, 
            status);
    }

    //PURPOSE
    //  Downloads the message data for specified record.
    //RETURNS
    //  False if the operation failed to start.
    //NOTE
    //  This will fail if the local gamer is not the msg recipient.
    bool ReadMsgData(const int localGamerIndex,
                     const char* mailboxName,                     
                     const rlMailboxRecordId recordId,
                     u8* msgData,
                     const unsigned msgDataMaxLength,
                     unsigned* msgDataLength,
                     netStatus* status);

    //PURPOSE
    //  Deletes one or more records for a local gamer from specified mailbox.
    //  Your specify a range of record IDs, which are interpreted as follows:
    //      0, 0    All records
    //      0, n    All records up to and including ID n
    //      n, 0    All records after and including ID n
    //      n, n    Just record n
    //      m, n    All records between and including IDs m and n
    //
    //  Only records addressed directly to the local gamer can be deleted. 
    //  Broadcasts will not be deleted.  Also, in special cases the local gamer 
    //  may be prohibited from deleting messages (ex. they've been banned).
    bool DeleteRecords(const int localGamerIndex,
                       const char* mailboxName,                     
                       const rlMailboxRecordId startRecordId,
                       const rlMailboxRecordId endRecordId,
                       unsigned* numDeleted,
                       netStatus* status);

private:
    template<typename T>
    bool CommonReadRecords(const int localGamerIndex,
                           const char* mailboxName,                     
                           const rlMailboxRecordId startRecordId,
                           const rlMailboxRecordId endRecordId,
                           const unsigned maxRecords,
                           const bool oldestFirst,
                           T* records,
                           unsigned* numRecords,
                           netStatus* status)
    { 
        rlMailboxReadRecordsTask<T>* task = RL_NEW(rlMailbox, rlMailboxReadRecordsTask<T> );
        
        if(rlVerify(task))
        { 
            task->Configure(this, 
                localGamerIndex, 
                mailboxName, 
                startRecordId, 
                endRecordId, 
                maxRecords, 
                oldestFirst, 
                records, 
                numRecords, 
                status);

            SYS_CS_SYNC(m_TaskListCs);
            m_TaskList.push_back(task);
            return true;
        }
        else
        {
            return false;
        }
    }

    rlRockonManager* m_RockonMgr;

    friend class rlMailboxTask;
    typedef inlist<rlMailboxTask, &rlMailboxTask::m_ListLink> TaskList;
    TaskList m_TaskList;
    sysCriticalSectionToken m_TaskListCs;

    unsigned m_LastUpdateTime;

    bool m_Initialized  : 1;
};

}   //namespace rage

#endif  //RLINE_RLMAILBOX_H
