// 
// rline/rlmailboxrecord.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLMAILBOXRECORD_H
#define RLINE_RLMAILBOXRECORD_H

#include "rl.h"
#include "rlgamerinfo.h"
#include "system/memops.h"

namespace rage
{

//PURPOSE
//  ID number for a particular record.  ID numbers are unique within a mailbox.
typedef s64 rlMailboxRecordId;


//PURPOSE
//  Base class for records in a mailbox.  This class only provides access 
//  and storage for the minimal set of info describing the record.
//
//  If users wish to retrieve user and msg data via rlMailbox::ReadRecords(),
//  they must use a derived record class that provides storage for user 
//  and/or msg data.  This is so apps can easily and efficiently work with 
//  mailboxes with widely varying sizes of user and msg data.
// 
class rlMailboxRecord
{
public:
    enum
    {
        //Max size of date strings, in format YYYY-MM-DDTHH:MM:SS.mmm
        MAX_DATETIME_STRING_LENGTH = 24
    };

    rlMailboxRecord();
    virtual ~rlMailboxRecord();

    virtual void Clear();

    //PURPOSE
    //  The ID of the record.  IDs are unique within the same mailbox.
    rlMailboxRecordId GetRecordId() const;

    //PURPOSE
    //  Returns handle of sender.
    const rlGamerHandle& GetSenderHandle() const;

    //PURPOSE
    //  Display name of the person who sent the message.
    const char* GetSenderDisplayName() const;
    unsigned GetSenderDisplayNameMaxLength() const;   

    //PURPOSE
    //  User data associated with the record.
    //NOTE
    //  The length of the user data is always known, but the actual data itself
    //  is only read by rlMailboxRecordPartial/Full.  Instances of the base
    //  rlMailboxRecord class will have nonzero userdata length, but null userdata
    //  and zero max length.
    //
    unsigned GetUserDataLength() const;
    virtual const u8* GetUserData() const;
    virtual const u8* GetUserDataBuffer() const;
    virtual unsigned GetUserDataMaxLength() const;

    //PURPOSE
    //  Msg data associated with the record.
    //NOTE
    //  The length of the msg data is always known, but the actual data itself
    //  is only read by rlMailboxRecordFull.  Instances of the base rlMailboxRecord 
    //  class and rlMailboxRecordPartial will have nonzero msgdata length, but null 
    //  msgdata and zero max length.
    //
    //  Also note that if you are reading multiple records and only need the msg data
    //  of one at a time, it is most efficient to read using rlMailboxRecords, and 
    //  then call rlMailbox::ReadMsgData() using the record IDs.
    //
    unsigned GetMsgDataLength() const;
    virtual const u8* GetMsgData() const;
    virtual const u8* GetMsgDataBuffer() const;
    virtual unsigned GetMsgDataMaxLength() const;

    //PURPOSE
    //  Returns date message was sent.
    const char* GetSendDateTimeStr() const;

    //PURPOSE
    //  Returns date message will (or did) expire.
    const char* GetExpirationDateTimeStr() const;

private:
    friend class rlMailboxTask;

    //Methods called by rlMailboxTask to intialize the record as it is read.
    void SetRecordId(const rlMailboxRecordId& id);
    void SetUserDataLength(const unsigned len);
    void SetMsgDataLength(const unsigned len);

    rlMailboxRecordId m_RecordId;
    rlGamerHandle m_SenderHandle;
    char m_SenderDisplayName[RL_MAX_DISPLAY_NAME_CHARS];
    unsigned m_UserDataLength;
    unsigned m_MsgDataLength;
    char m_SendDateTimeStr[MAX_DATETIME_STRING_LENGTH];
    char m_ExpirationDateTimeStr[MAX_DATETIME_STRING_LENGTH];
};



//PURPOSE
//  Mailbox record that has storage for user data, but not message data.
//  This will likely be the most commonly used type of mailbox record, as
//  many apps will require userdata when readind records, but will want 
//  to pull down msg data separately via rlMailbox::ReadMsgData().
//
template<unsigned T_USERDATA_MAX_SIZE>
class rlMailboxRecordPartial : public rlMailboxRecord
{
public:
    rlMailboxRecordPartial() { Clear(); }
    virtual ~rlMailboxRecordPartial() {}

    virtual void Clear()
    {
        rlMailboxRecord::Clear();
        sysMemSet(m_UserData, 0, sizeof(m_UserData));
    }

    virtual const u8* GetUserData() const { return GetUserDataLength() ? m_UserData : 0; }
    virtual const u8* GetUserDataBuffer() const { return m_UserData; }
    virtual unsigned GetUserDataMaxLength() const { return sizeof(m_UserData); }

private:
    u8 m_UserData[T_USERDATA_MAX_SIZE];
};



//PURPOSE
//  Mailbox record that has storage for both user and msg data.
//  It is recommended that this be used only when the app would 
//  immediately call rlMailbox::ReadMsgData() on the record.
//  (Doing so saves a call to the rockon backend, so it is more efficient.)
//
template<unsigned T_USERDATA_MAX_SIZE, unsigned T_MSGDATA_MAX_SIZE>
class rlMailboxRecordFull : public rlMailboxRecord
{
public:
    rlMailboxRecordFull() { Clear(); }
    virtual ~rlMailboxRecordFull() {}

    virtual void Clear()
    {
        rlMailboxRecord::Clear();
        sysMemSet(m_UserData, 0, sizeof(m_UserData));
        sysMemSet(m_MsgData, 0, sizeof(m_MsgData));
    }

    virtual const u8* GetUserData() const { return GetUserDataLength() ? m_UserData : 0; }
    virtual const u8* GetUserDataBuffer() const { return m_UserData; }
    virtual unsigned GetUserDataMaxLength() const { return sizeof(m_UserData); }

    virtual const u8* GetMsgData() const { return GetMsgDataLength() ? m_MsgData : 0; }
    virtual const u8* GetMsgDataBuffer() const { return m_MsgData; }
    virtual unsigned GetMsgDataMaxLength() const { return sizeof(m_MsgData); }

private:
    u8 m_UserData[T_USERDATA_MAX_SIZE];
    u8 m_MsgData[T_MSGDATA_MAX_SIZE];
};

}   //namespace rage

#endif  //RLINE_RLMAILBOXRECORD_H
