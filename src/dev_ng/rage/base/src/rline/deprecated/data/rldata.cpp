// 
// rline/rldata.cpp
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rldata.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "string/string.h"
#include "system/memops.h"
#include "system/endian.h"
#include <stdio.h>

namespace rage
{

const char* 
rlDataValueTypeToString(const rlDataValueType t)
{
    static const char* s_VtStrs[RLDATA_NUM_VALUETYPES] =
    {
        "string",   //RLDATA_VALUETYPE_ASCII_STRING
        "bool",     //RLDATA_VALUETYPE_BOOL
        "double",   //RLDATA_VALUETYPE_DOUBLE
        "float",    //RLDATA_VALUETYPE_FLOAT
        "int16",    //RLDATA_VALUETYPE_INT16
        "int32",    //RLDATA_VALUETYPE_INT32
        "int64",    //RLDATA_VALUETYPE_INT64
        "uns8",     //RLDATA_VALUETYPE_UNS8
    };

    const char* vtStr = 0;

    if(t >=0 && t < RLDATA_NUM_VALUETYPES)
    {
        vtStr = s_VtStrs[t];
    }

    if(vtStr)
    {
        rlAssert(strlen(vtStr) < RLDATA_MAX_VALUE_TYPE_STR_CHARS);
    }

    return vtStr;
}

rlDataValueType
rlDataValueTypeFromString(const char* typeName)
{
#define VT_CHECK(vt) { const char* vtStr = rlDataValueTypeToString(vt);  if(vtStr && !strcmp(typeName, vtStr)) return vt; }

    VT_CHECK(RLDATA_VALUETYPE_ASCII_STRING);
    VT_CHECK(RLDATA_VALUETYPE_BOOL);
    VT_CHECK(RLDATA_VALUETYPE_DOUBLE);
    VT_CHECK(RLDATA_VALUETYPE_FLOAT);
    VT_CHECK(RLDATA_VALUETYPE_INT16);
    VT_CHECK(RLDATA_VALUETYPE_INT32);
    VT_CHECK(RLDATA_VALUETYPE_INT64);
    VT_CHECK(RLDATA_VALUETYPE_UNS8);

    return RLDATA_VALUETYPE_INVALID;
}

//-----------------------------------------------------------------------------
// rlDataValue
//-----------------------------------------------------------------------------
rlDataValue::rlDataValue()
: m_Type(RLDATA_VALUETYPE_INVALID)
{
}

rlDataValue::~rlDataValue()
{
    Clear();
}

rlDataValue::rlDataValue(const rlDataValue& rhs)
{
    *this = rhs;
}

rlDataValue& 
rlDataValue::operator=(const rlDataValue& rhs)
{
    Clear();

    if(rhs.IsNull())
    {
        SetNull(rhs.GetType());
    }
    else
    {
        switch(rhs.GetType())
        {
        case RLDATA_VALUETYPE_ASCII_STRING: SetAsciiString(rhs.GetAsciiString()); break;
        case RLDATA_VALUETYPE_BOOL:         SetBool(rhs.GetBool()); break;
        case RLDATA_VALUETYPE_DOUBLE:       SetDouble(rhs.GetDouble()); break;
        case RLDATA_VALUETYPE_FLOAT:        SetFloat(rhs.GetFloat()); break;
        case RLDATA_VALUETYPE_INT16:        SetInt16(rhs.GetInt16()); break;
        case RLDATA_VALUETYPE_INT32:        SetInt32(rhs.GetInt32()); break;
        case RLDATA_VALUETYPE_INT64:        SetInt64(rhs.GetInt64()); break;
        case RLDATA_VALUETYPE_UNS8:         SetUns8(rhs.GetUns8()); break;
        default:                            rlError("Unhandled value type: %d", rhs.GetType());
        };
    }

    return *this;
}

bool 
rlDataValue::operator==(const rlDataValue& that) const
{
    //Invalid values can never be equal.
    if(!IsValid() || !that.IsValid()) return false;

    //Values of different types cannot be equal.
    const rlDataValueType vt = GetType();
    if(vt != that.GetType()) return false;

    //Null values of the same type are considered equal.
    if(IsNull() == that.IsNull()) return true;

    switch(vt)
    {
    case RLDATA_VALUETYPE_ASCII_STRING: return !strcmp(m_Union.m_AsciiString, that.m_Union.m_AsciiString);
    case RLDATA_VALUETYPE_BOOL:         return m_Union.m_Bool == that.m_Union.m_Bool;
    case RLDATA_VALUETYPE_DOUBLE:       return m_Union.m_Double == that.m_Union.m_Double;
    case RLDATA_VALUETYPE_FLOAT:        return m_Union.m_Float == that.m_Union.m_Float;
    case RLDATA_VALUETYPE_INT16:        return m_Union.m_Int16 == that.m_Union.m_Int16;
    case RLDATA_VALUETYPE_INT32:        return m_Union.m_Int32 == that.m_Union.m_Int32;
    case RLDATA_VALUETYPE_INT64:        return m_Union.m_Int64 == that.m_Union.m_Int64;
    case RLDATA_VALUETYPE_UNS8:         return m_Union.m_Uns8 == that.m_Union.m_Uns8;
    default:                            return false;
    }
}

bool 
rlDataValue::operator!=(const rlDataValue& that) const
{
    return !(*this == that);
}

bool 
rlDataValue::IsValid() const 
{ 
    return GetType() != RLDATA_VALUETYPE_INVALID; 
}

bool
rlDataValue::IsNull() const
{
    return (m_Type & RLDATA_NULL_FLAG) || (m_Type == RLDATA_VALUETYPE_INVALID);
}

rlDataValueType 
rlDataValue::GetType() const 
{ 
    return (rlDataValueType)(m_Type & ~((rlDataValueType)RLDATA_NULL_FLAG)); 
}

void 
rlDataValue::Clear()
{
    switch(GetType())
    {
    case RLDATA_VALUETYPE_ASCII_STRING:
        if(m_Union.m_AsciiString)
        {
            rlFree(m_Union.m_AsciiString);
            m_Union.m_AsciiString = 0;
        }
        break;

    default:
        break;
    }

    m_Type = RLDATA_VALUETYPE_INVALID;
}

const char* 
rlDataValue::GetAsciiString() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_ASCII_STRING && !IsNull());
    return m_Union.m_AsciiString; 
}

bool 
rlDataValue::GetBool() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_BOOL && !IsNull());
    return m_Union.m_Bool; 
}

double
rlDataValue::GetDouble() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_DOUBLE && !IsNull());
    return m_Union.m_Double; 
}

float 
rlDataValue::GetFloat() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_FLOAT && !IsNull());
    return m_Union.m_Float; 
}

s16 
rlDataValue::GetInt16() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_INT16 && !IsNull());
    return m_Union.m_Int16; 
}

s32 
rlDataValue::GetInt32() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_INT32 && !IsNull());
    return m_Union.m_Int32; 
}

s64 
rlDataValue::GetInt64() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_INT64 && !IsNull());
    return m_Union.m_Int64; 
}

u8 
rlDataValue::GetUns8() const 
{ 
    rlAssert(GetType() == RLDATA_VALUETYPE_UNS8 && !IsNull());
    return m_Union.m_Uns8; 
}

bool 
rlDataValue::SetAsciiString(const char* str)
{
    Clear();

    unsigned size = strlen(str) + 1;

    if(0 == (m_Union.m_AsciiString = (char*)RL_ALLOCATE(rlDataValue, size)))
    {
        rlError("Failed to allocate %u bytes for string value", size);
        return false;
    }

    safecpy(m_Union.m_AsciiString, str, size);
    m_Type = RLDATA_VALUETYPE_ASCII_STRING;

    return true;
}

void 
rlDataValue::SetBool(const bool val) 
{ 
    Clear(); 
    m_Type = RLDATA_VALUETYPE_BOOL;
    m_Union.m_Bool = val; 
}

void 
rlDataValue::SetDouble(const double val) 
{ 
    Clear(); 
    m_Type = RLDATA_VALUETYPE_DOUBLE;
    m_Union.m_Double = val; 
}

void 
rlDataValue::SetFloat(const float val) 
{ 
    Clear(); 
    m_Type = RLDATA_VALUETYPE_FLOAT;
    m_Union.m_Float = val; 
}

void 
rlDataValue::SetInt16(const s16 val) 
{ 
    Clear(); 
    m_Type = RLDATA_VALUETYPE_INT16;
    m_Union.m_Int16 = val; 
}

void 
rlDataValue::SetInt32(const s32 val) 
{ 
    Clear(); 
    m_Type = RLDATA_VALUETYPE_INT32;
    m_Union.m_Int32 = val; 
}

void 
rlDataValue::SetInt64(const s64 val) 
{ 
    Clear(); 
    m_Type = RLDATA_VALUETYPE_INT64;
    m_Union.m_Int64 = val; 
}

void 
rlDataValue::SetUns8(const u8 val) 
{ 
    Clear(); 
    m_Type = RLDATA_VALUETYPE_UNS8;
    m_Union.m_Uns8 = val; 
}

void
rlDataValue::SetNull(const rlDataValueType vt)
{
    Clear(); 
    m_Type = (rlDataValueType)(vt & RLDATA_NULL_FLAG);
}

const char* 
rlDataValue::ToString(char* buf, const unsigned bufSize) const
{
    rlAssert(buf && bufSize);

    buf[0] = '\0';

    if(IsNull())
    {
        safecpy(buf, "NULL", bufSize);
    }
    else
    {
        switch(GetType())
        {
        case RLDATA_VALUETYPE_INVALID:      safecpy(buf, "INVALID", bufSize); break;
        case RLDATA_VALUETYPE_ASCII_STRING: safecpy(buf, GetAsciiString(), bufSize); break;
        case RLDATA_VALUETYPE_BOOL:         safecpy(buf, GetBool() ? "true" : "false", bufSize); break;
        case RLDATA_VALUETYPE_DOUBLE:       formatf(buf, bufSize, "%Lf", GetDouble()); break;
        case RLDATA_VALUETYPE_FLOAT:        formatf(buf, bufSize, "%f", GetFloat()); break;
        case RLDATA_VALUETYPE_INT16:        formatf(buf, bufSize, "%d", GetInt16()); break;
        case RLDATA_VALUETYPE_INT32:        formatf(buf, bufSize, "%d", GetInt32()); break;
        case RLDATA_VALUETYPE_INT64:        formatf(buf, bufSize, "0x%"I64FMT"x", GetInt64()); break;
        case RLDATA_VALUETYPE_UNS8:         formatf(buf, bufSize, "%u", GetUns8()); break;
        default:                            rlError("Unhandled or invalid value type (%d)", m_Type);
        }
    }

    return buf;
}

bool 
rlDataValue::FromString(const char* str, const rlDataValueType asType)
{
    switch(asType)
    {
    case RLDATA_VALUETYPE_ASCII_STRING:
        return SetAsciiString(str);
    case RLDATA_VALUETYPE_BOOL:
        if(!stricmp("true", str))
        {
            SetBool(true);
        }
        else if(!stricmp("false", str))
        {
            SetBool(false);
        }
        else
        {
            return false;
        }
        break;
    case RLDATA_VALUETYPE_DOUBLE:
        {
            double val;
            if(sscanf(str, "%Lf", &val) != 1) return false;
            SetDouble(val);
        }
        break;
    case RLDATA_VALUETYPE_FLOAT:
        {
            float val;
            if(sscanf(str, "%f", &val) != 1) return false;
            SetFloat(val);
        }
        break;
    case RLDATA_VALUETYPE_INT16:
        {
            int val;
            if(sscanf(str, "%d", &val) != 1) return false;
            SetInt16((s16)val);
        }
        break;
    case RLDATA_VALUETYPE_INT32:
        {
            int val;
            if(sscanf(str, "%d", &val) != 1) return false;
            SetInt32((s32)val);
        }
        break;
    case RLDATA_VALUETYPE_INT64:
        {
            s64 val;
            if((strlen(str) > 2) && '0' == str[0] && 'x' == str[1])
            {
                if(sscanf(str, "0x%" I64FMT "x", &val) != 1) return false;
            }
            else
            {
                if(sscanf(str, "%" I64FMT "u", &val) != 1) return false;
            }
            SetInt64(val);
        }
        break;
    case RLDATA_VALUETYPE_UNS8:
        {
            int val;
            if(sscanf(str, "%d", &val) != 1) return false;
            SetUns8((u8)val);
        }
        break;
    default:
        return false;
    }

    return true;
}

const char* 
rlDataValue::TypeToString() const
{
    return rlDataValueTypeToString(GetType());
}

//-----------------------------------------------------------------------------
// rlDataSchema
//-----------------------------------------------------------------------------
rlDataColumn::rlDataColumn()
: m_Id(0)
, m_Index(-1)
, m_Type(RLDATA_VALUETYPE_INVALID)
{
}

//-----------------------------------------------------------------------------
// rlDataSchema
//-----------------------------------------------------------------------------
rlDataSchemaBase::rlDataSchemaBase()
{
}

rlDataSchemaBase::~rlDataSchemaBase()
{
}

int
rlDataSchemaBase::GetRowByteSize() const
{
    int size = 0;

    for(unsigned i = 0; i < GetNumColumns(); i++)
    {
        rlDataValueType vt = GetColumnByIndex(i)->m_Type;

        switch(vt)
        {
        case RLDATA_VALUETYPE_ASCII_STRING: return -1;   //Cannot be determined from type
        case RLDATA_VALUETYPE_BOOL:         size += sizeof(bool); break;
        case RLDATA_VALUETYPE_DOUBLE:       size += sizeof(double); break;
        case RLDATA_VALUETYPE_FLOAT:        size += sizeof(float); break;
        case RLDATA_VALUETYPE_INT16:        size += sizeof(s16); break;
        case RLDATA_VALUETYPE_INT32:        size += sizeof(s32); break;
        case RLDATA_VALUETYPE_INT64:        size += sizeof(s64); break;
        case RLDATA_VALUETYPE_UNS8:         size += sizeof(u8); break;
        default:
            rlError("Unhandled value type (%d)", vt);
            return -1;
        }
    }

    return size;
}

//-----------------------------------------------------------------------------
// rlDataDynamicSchema
//-----------------------------------------------------------------------------
rlDataDynamicSchema::rlDataDynamicSchema()
: m_MaxCols(0)
, m_NumCols(0)
, m_Cols(0)
{
}

rlDataDynamicSchema::~rlDataDynamicSchema()
{
    Clear();
}

bool 
rlDataDynamicSchema::Reset(const unsigned maxColumns)
{
    if(rlVerify(maxColumns))
    {
        Clear();
        
        m_Cols = RL_NEW_ARRAY(rlDataDynamicSchema, rlDataColumn, maxColumns);
        if(!m_Cols)
        {
            rlError("Failed to allocated %d columns", maxColumns);
            return false;
        }

        m_MaxCols = maxColumns;
        
        return true;
    }
    return false;
}

void 
rlDataDynamicSchema::Clear()
{
    if(m_Cols)
    {
        RL_DELETE_ARRAY(m_Cols, m_MaxCols);
        m_Cols = 0;
    }

    m_MaxCols = 0;
    m_NumCols = 0;
}

bool 
rlDataDynamicSchema::AddColumn(const int id, const rlDataValueType vt)
{
    if(!rlVerify(vt != RLDATA_VALUETYPE_INVALID))
    {
        return false;
    }

    if(rlVerify(m_NumCols < GetMaxColumns()))
    {
        rlDataColumn* col = &m_Cols[m_NumCols];
        col->m_Id = id;
        col->m_Type = vt;
        col->m_Index = (s16)m_NumCols;           
        ++m_NumCols;            
        return true;
    }
    return false;
}

unsigned 
rlDataDynamicSchema::GetMaxColumns() const
{
    return m_MaxCols;
}

unsigned 
rlDataDynamicSchema::GetNumColumns() const
{
    return m_NumCols;
}

const rlDataColumn* 
rlDataDynamicSchema::GetColumnByIndex(const unsigned index) const
{
    if(index < m_NumCols) return &m_Cols[index];
    return 0;
}

const rlDataColumn* 
rlDataDynamicSchema::GetColumnById(const int id) const
{
    //NOTE: We don't use a map for lookup because the links would add
    //      add significant memory overhead when there are thousands
    //      stats in a schema (which is likely with profile stats).
    for(unsigned i = 0; i < m_NumCols; i++)
    {
        if(m_Cols[i].m_Id == id) return &m_Cols[i];
    }
    return 0;
}

//-----------------------------------------------------------------------------
// rlDataSet
//-----------------------------------------------------------------------------
rlDataSet::rlDataSet()
: m_MaxRows(0)
, m_NumRows(0)
, m_Values(0)
{
}

rlDataSet::~rlDataSet()
{
}

bool 
rlDataSet::IsValid() const
{
    return m_Schema && m_MaxRows && m_Values;
}

void
rlDataSet::Reset(const rlDataSchemaBase* schema,
                 const unsigned maxRows,
                 rlDataValue* values)
{
    rlAssert(schema && maxRows && values);

    m_Schema = schema;
    m_MaxRows = maxRows;
    m_Values = values;
    m_NumRows = 0;
}

void 
rlDataSet::Clear(const bool recordsOnly)
{
    if(IsValid())
    {
        unsigned numValues = m_Schema->GetNumColumns() * m_NumRows;

        for(unsigned i = 0; i < numValues; i++)
        {
            m_Values[i].Clear();
        }

        if(!recordsOnly)
        {
            m_Schema = 0;
            m_MaxRows = 0;
            m_Values = 0;
        }
    }

    m_NumRows = 0;
}

const rlDataSchemaBase*
rlDataSet::GetSchema() const
{
    return m_Schema;
}

unsigned 
rlDataSet::GetMaxRows() const     
{ 
    return m_MaxRows; 
}

unsigned 
rlDataSet::GetNumRows() const     
{ 
    return m_NumRows; 
}

const rlDataValue& 
rlDataSet::GetValue(const unsigned row,
                          const unsigned column) const
{
    rlAssert(column < m_Schema->GetNumColumns());
    rlAssert(m_Values);

    return m_Values[(row * m_Schema->GetNumColumns()) + column];
}

rlDataValue& 
rlDataSet::GetValue(const unsigned row,
                          const unsigned column)
{
    rlAssert(column < m_Schema->GetNumColumns());
    rlAssert(m_Values);

    return m_Values[(row * m_Schema->GetNumColumns()) + column];
}

bool
rlDataSet::SetNumRows(const unsigned num) 
{
    if(rlVerify(num <= m_MaxRows))
    {
        m_NumRows = num;
        return true;
    }
    return false;
}

const char* 
rlDataSet::GetAsciiStringById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetAsciiString();
}

bool 
rlDataSet::GetBoolById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetBool();
}

double
rlDataSet::GetDoubleById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetDouble();
}

float 
rlDataSet::GetFloatById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetFloat();
}

s16 
rlDataSet::GetInt16ById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetInt16();
}

s32 
rlDataSet::GetInt32ById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetInt32();
}

s64 
rlDataSet::GetInt64ById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetInt64();
}

u8 
rlDataSet::GetUns8ById(const unsigned row, const int id) const
{
    return GetValue(row, m_Schema->GetColumnById(id)->m_Index).GetUns8();
}

const char* 
rlDataSet::GetAsciiString(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetAsciiString();
}

bool 
rlDataSet::GetBool(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetBool();
}

double 
rlDataSet::GetDouble(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetDouble();
}

float 
rlDataSet::GetFloat(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetFloat();
}

s16 
rlDataSet::GetInt16(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetInt16();
}

s32 
rlDataSet::GetInt32(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetInt32();
}

s64 
rlDataSet::GetInt64(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetInt64();
}

u8 
rlDataSet::GetUns8(const unsigned row, const unsigned column) const
{
    return GetValue(row, column).GetUns8();
}

bool 
rlDataSet::SetAsciiString(const unsigned row, const unsigned column, const char* str)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_ASCII_STRING))
    {
        GetValue(row, column).SetAsciiString(str);
        return true;
    }
    return false;
}

bool
rlDataSet::SetBool(const unsigned row, const unsigned column, const bool val)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_BOOL))
    {
        GetValue(row, column).SetBool(val);
        return true;
    }
    return false;
}

bool 
rlDataSet::SetDouble(const unsigned row, const unsigned column, const double val)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_DOUBLE))
    {
        GetValue(row, column).SetDouble(val);
        return true;
    }
    return false;
}

bool 
rlDataSet::SetFloat(const unsigned row, const unsigned column, const float val)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_FLOAT))
    {
        GetValue(row, column).SetFloat(val);
        return true;
    }
    return false;
}

bool 
rlDataSet::SetInt16(const unsigned row, const unsigned column, const s16 val)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_INT16))
    {
        GetValue(row, column).SetInt16(val);
        return true;
    }
    return false;
}

bool 
rlDataSet::SetInt32(const unsigned row, const unsigned column, const s32 val)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_INT32))
    {
        GetValue(row, column).SetInt32(val);    
        return true;
    }
    return false;
}

bool 
rlDataSet::SetInt64(const unsigned row, const unsigned column, const s64 val)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_INT64))
    {
        GetValue(row, column).SetInt64(val);
        return true;
    }
    return false;
}

bool 
rlDataSet::SetUns8(const unsigned row, const unsigned column, const u8 val)
{
    if(rlVerify(m_Schema && m_Schema->GetColumnByIndex(column)->m_Type == RLDATA_VALUETYPE_UNS8))
    {
        GetValue(row, column).SetUns8(val);
        return true;
    }
    return false;
}

};
