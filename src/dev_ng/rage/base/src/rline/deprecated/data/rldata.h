// 
// rline/rldata.h 
// 
// Copyright (C) 2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLDATA_H
#define RLINE_RLDATA_H

#include "rline/rl.h"

namespace rage
{

//PURPOSE
//  Possible types of rlDataValue objects.
enum rlDataValueTypeIds
{
    RLDATA_VALUETYPE_INVALID = -1,
    RLDATA_VALUETYPE_ASCII_STRING,  //Includes UTF-8, but not other Unicode formats
    RLDATA_VALUETYPE_BOOL,
    RLDATA_VALUETYPE_DOUBLE,
    RLDATA_VALUETYPE_FLOAT,
    RLDATA_VALUETYPE_INT16,
    RLDATA_VALUETYPE_INT32,
    RLDATA_VALUETYPE_INT64,
    RLDATA_VALUETYPE_UNS8,
    RLDATA_NUM_VALUETYPES,

    RLDATA_NULL_FLAG = 0x1 << 6     //Flag packed into rlDataValueType that indicates value is null
};

typedef s8 rlDataValueType;

//Convert value type to/from string.
const unsigned RLDATA_MAX_VALUE_TYPE_STR_CHARS = 7;
const char* rlDataValueTypeToString(const rlDataValueType t);
rlDataValueType rlDataValueTypeFromString(const char* str);

//PURPOSE
//  Represents a typed value.  This is useful when either the data type is not known up-front,
//  or when you need a convenient way to store arrays of mixed types (which is often the case
//  when reading results from a backend query).
class rlDataValue
{
public:
    rlDataValue();
    ~rlDataValue();

    rlDataValue(const rlDataValue& rhs);
    rlDataValue& operator=(const rlDataValue& rhs);

    bool operator==(const rlDataValue& that) const;
    bool operator!=(const rlDataValue& that) const;

    //Set type to invalid, and releases any allocated resources.
    void Clear();

    //Returns true if type is valid, even if value is null.
    bool IsValid() const;

    //Returns value's type.
    rlDataValueType GetType() const;
    
    //Returns true if value is null.  Values can have a type and still be null.
    bool IsNull() const;

    //Accessors that get the value as specified type.
    //NOTE: These will not do conversion; if the value is not the specified
    //      type, this will assert and return a default value (zero, false, or null).
    const char* GetAsciiString() const;
    bool GetBool() const;
    double GetDouble() const;
    float GetFloat() const;
    s16 GetInt16() const;
    s32 GetInt32() const;
    s64 GetInt64() const;
    u8 GetUns8() const;

    //Sets our type and value.
    bool SetAsciiString(const char* str);
    void SetBool(const bool val);
    void SetDouble(const double val);
    void SetFloat(const float val);
    void SetInt16(const s16 val);
    void SetInt32(const s32 val);
    void SetInt64(const s64 val);
    void SetUns8(const u8 val);

    //Sets the value to be null, of specified type.
    void SetNull(const rlDataValueType vt);

    const char* TypeToString() const;
    const char* ToString(char* buf, const unsigned bufSize) const;
    bool FromString(const char* str, const rlDataValueType asType);

private:
    union ValueUnion
    {
        char*   m_AsciiString;
        bool    m_Bool;
        double  m_Double;
        float   m_Float;
        s16     m_Int16;
        s32     m_Int32;
        s64     m_Int64;
        u8      m_Uns8;
    } m_Union;

    rlDataValueType m_Type;
};

//PURPOSE
//  Describes a column in a schema.
class rlDataColumn
{
public:
    rlDataColumn();

    int m_Id;
    s16 m_Index;
    rlDataValueType m_Type;

    //TODO: Add a max length member, so we can determine max row size for
    //      schema that have string or binary columns?
};

//PURPOSE
//  A schema describes a set of columns by type and ID.
class rlDataSchemaBase
{
public:
    rlDataSchemaBase();
    virtual ~rlDataSchemaBase();

    //PURPOSE
    //  Clears the schema, setting it's number of fields to zero and releasing
    //  any dynamically allocated resources.
    virtual void Clear() = 0;

    //PURPOSE
    //  Adds a column to the schema, and increments the number of columns.
    //  Returns false if schema already contained its maximum number of columns.
    virtual bool AddColumn(const int id, const rlDataValueType vt) = 0;

    //PURPOSE
    //  Simple Accessors
    virtual unsigned GetMaxColumns() const = 0;
    virtual unsigned GetNumColumns() const = 0;
    virtual const rlDataColumn* GetColumnByIndex(const unsigned index) const = 0;
    virtual const rlDataColumn* GetColumnById(const int id) const = 0;

    //PURPOSE
    //  Returns the byte size of a row with this schema, or -1 if it
    //  cannot be determined statically (like if any of the rows are strings).
    int GetRowByteSize() const;
};

//PURPOSE
//  Schema that can have any number of columns.  This is ideal if you don't
//  know how many columns the schema will have (like when importing a schema).
class rlDataDynamicSchema : public rlDataSchemaBase
{
public:
    rlDataDynamicSchema();
    virtual ~rlDataDynamicSchema();

    //PURPOSE
    //  Clears the schema and sets its max size.
    bool Reset(const unsigned maxColumns);

    //PURPOSE
    //  rlDataSchemaBase overrides.
    virtual void Clear();
    virtual bool AddColumn(const int id, const rlDataValueType vt);
    virtual unsigned GetMaxColumns() const;
    virtual unsigned GetNumColumns() const;
    virtual const rlDataColumn* GetColumnByIndex(const unsigned index) const;
    virtual const rlDataColumn* GetColumnById(const int id) const;

private:
    rlDataDynamicSchema(const rlDataDynamicSchema& that);
    rlDataDynamicSchema& operator=(const rlDataDynamicSchema& that);

    unsigned m_MaxCols;
    unsigned m_NumCols;
    rlDataColumn* m_Cols;
};

//PURPOSE
//  A schema whose maximum number of fields is fixed at compile time.
template<unsigned T_MAX_COLUMNS>
class rlDataFixedSchema : public rlDataSchemaBase
{
public:
    rlDataFixedSchema() { Clear(); }
    virtual ~rlDataFixedSchema() {}

    virtual void Clear() 
    { 
        m_NumCols = 0; 
    }
    
    virtual bool AddColumn(const int id, const rlDataValueType vt)
    {
        if(!rlVerify(vt != RLDATA_VALUETYPE_INVALID))
        {
            return false;
        }

        if(rlVerify(m_NumCols < GetMaxColumns()))
        {
            rlDataColumn* col = &m_Cols[m_NumCols];
            col->m_Id = id;
            col->m_Type = vt;
            col->m_Index = (s16)m_NumCols;           
            ++m_NumCols;            
            return true;
        }
        return false;
    }

    virtual unsigned GetMaxColumns() const { return T_MAX_COLUMNS; }

    virtual unsigned GetNumColumns() const { return m_NumCols; }

    virtual const rlDataColumn* GetColumnByIndex(const unsigned index) const 
    { 
        if(rlVerify(index < m_NumCols)) return &m_Cols[index];
        return 0;
    }

    virtual const rlDataColumn* GetColumnById(const int id) const
    {
        //NOTE: We don't use a map for lookup because the links would add
        //      add significant memory overhead when there are thousands
        //      stats in a schema (which is likely with profile stats).
        //      If this lookup is too slow for your title, contact RAGE
        //      networking to help determine the best solution.
        for(unsigned i = 0; i < m_NumCols; i++)
        {
            if(m_Cols[i].m_Id == id) return &m_Cols[i];
        }
        return 0;
    }    

private:
    rlDataFixedSchema(const rlDataFixedSchema& that);
    rlDataFixedSchema& operator=(const rlDataFixedSchema& that);

    unsigned m_NumCols;
    rlDataColumn m_Cols[T_MAX_COLUMNS];   
};

//PURPOSE
//  Class that combines a schema with storage for a number of records.
//  The user calls Reset() to set the schema, max records the set may hold,
//  and a user-allocated array of values large enough to hold the record values.
class rlDataSet
{
public:
    rlDataSet();
    ~rlDataSet();

    //PURPOSE
    //  Returns true if rlDataSet has been successfully Reset().
    bool IsValid() const;

    //PURPOSE
    //  Sets the schema for the results, and the array of values it stores results in.
    //  This array must be allocated and freed by the user, and must be at least
    //  schema.GetNumColumns() * maxRows in size.
    void Reset(const rlDataSchemaBase* schema,
               const unsigned maxRows,
               rlDataValue* values);

    //PURPOSE
    //  Clears the dataset.  If rowsOnly is set to true, this only sets the 
    //  number of rows to zero and clears all values.  If set to false, this
    //  also releases references to schema and values, meaning that Reset() will
    //  need to be called before the dataset can be used again.
    void Clear(const bool rowsOnly = true);

    //PURPOSE
    //  Simple accessors.
    const rlDataSchemaBase* GetSchema() const;
    unsigned GetMaxRows() const;
    unsigned GetNumRows() const;

    //PURPOSE
    //  Searches
    int GetRowIndexContaining(const int id,
                              const char* value,
                              const unsigned startIndex) const;

    //PURPOSE
    //  Access the value at specified cell as a certain type.
    const char* GetAsciiString(const unsigned row, const unsigned column) const;
    bool GetBool(const unsigned row, const unsigned column) const;
    double GetDouble(const unsigned row, const unsigned column) const;
    float GetFloat(const unsigned row, const unsigned column) const;
    s16 GetInt16(const unsigned row, const unsigned column) const;
    s32 GetInt32(const unsigned row, const unsigned column) const;
    s64 GetInt64(const unsigned row, const unsigned column) const;
    u8 GetUns8(const unsigned row, const unsigned column) const;

    //PURPOSE
    //  Access value at column with given ID in specified row.
    const char* GetAsciiStringById(const unsigned row, const int id) const;
    bool GetBoolById(const unsigned row, const int id) const; 
    double GetDoubleById(const unsigned row, const int id) const;
    float GetFloatById(const unsigned row, const int id) const;
    s16 GetInt16ById(const unsigned row, const int id) const;
    s32 GetInt32ById(const unsigned row, const int id) const;
    s64 GetInt64ById(const unsigned row, const int id) const;
    u8 GetUns8ById(const unsigned row, const int id) const;

    //PURPOSE
    //  Set the value for specified cell in the data set.
    //  If the type does not match the schema, this will fail.
    //NOTES
    //  You can set values past the current number of records in the set,
    //  but not past the max records.  Best practice when populating datasets 
    //  is to set all the values for your rows, and then call SetNumRows().
    bool SetAsciiString(const unsigned row, const unsigned column, const char* str);
    bool SetBool(const unsigned row, const unsigned column, const bool val);
    bool SetDouble(const unsigned row, const unsigned column, const double val);
    bool SetFloat(const unsigned row, const unsigned column, const float val);
    bool SetInt16(const unsigned row, const unsigned column, const s16 val);
    bool SetInt32(const unsigned row, const unsigned column, const s32 val);
    bool SetInt64(const unsigned row, const unsigned column, const s64 val);
    bool SetUns8(const unsigned row, const unsigned column, const u8 val);

    //PURPOSE
    //  Sets the number of records the set contains. 
    //  This should be called only when new rows are added, and only after you
    //  have set the values for the new rows with the SetXXX() methods.
    //  Returns false if you try to exceed the maximum number of records.
    bool SetNumRows(const unsigned num);

private:    
    rlDataSet(const rlDataSet& rhs);
    rlDataSet& operator=(const rlDataSet& rhs);

    const rlDataValue& GetValue(const unsigned row, const unsigned column) const;
    rlDataValue& GetValue(const unsigned row, const unsigned column);

    const rlDataSchemaBase* m_Schema;
    unsigned m_MaxRows;
    unsigned m_NumRows;
    rlDataValue* m_Values;
};

} //namespace rage

#endif  //RLINE_RLDATA_H
