// 
// rline/rlCredentials.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLCREDENTIALS_H
#define RLINE_RLCREDENTIALS_H

#include "net/net.h"

#if !NET_SECURE_PLATFORM

namespace rage
{

class rlCredentials
{
public:
    static bool GetLocalCredentials(rlCredentials* cred);

    rlCredentials();

    void Clear();

    bool IsPublicValid() const;
    
    bool IsPrivateValid() const;

    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);
    
    enum
    {
#if __GAMESPY
        //GS RSA encryption can only work on specific buffer sizes.
        MAX_PLAINTEXT_SIZE  = 128 - 11,
        MIN_CIPHERTEXT_SIZE = 128,
        MAX_CIPHERTEXT_SIZE = 128   //NOTE: Ciphertext is always padded to 128 bytes
#else
        //Other platforms are not currently supported.
        MAX_PLAINTEXT_SIZE = 0,
        MIN_CIPHERTEXT_SIZE = 0,
        MAX_CIPHERTEXT_SIZE = 0
#endif
    };

    bool Encrypt(const u8* plainText,
                 const unsigned plainTextSize,
                 u8* cipherText,
                 const unsigned cipherTextMaxSize,
                 unsigned* cipherTextSize) const;

    bool Decrypt(const u8* cipherText,
                 const unsigned cipherTextSize,
                 u8* plainText,
                 const unsigned plainTextMaxSize,
                 unsigned* plainTextSize) const;

    bool operator==(const rlCredentials& that) const;
    bool operator!=(const rlCredentials& that) const;

private:
#if __GAMESPY
    //Structs that mirror Gamespy counterparts, for doing sizeof w/o needing includes.
    struct rlGsLargeInt_t
    {
	    u32 mLength;
	    u32 mData[64];
    };

    struct rlGsCryptRsaKey
    {
	    rlGsLargeInt_t modulus;
	    rlGsLargeInt_t exponent;
    };

    struct rlGsLoginCertificate
    {
	    bool mIsValid;    	
	    u32 mLength;
	    u32 mVersion;
	    u32 mPartnerCode;
	    u32 mNamespaceId;
	    u32 mUserId;
	    u32 mProfileId;
	    u32 mExpireTime;
	    char mProfileNick[31];
	    char mUniqueNick[21];
	    char mCdKeyHash[33];
 	    rlGsCryptRsaKey mPeerPublicKey;
	    u8 mSignature[128];
	    u8 mServerData[128];
    };

    struct rlGsLoginCertificatePrivate
    {
	    rlGsCryptRsaKey mPeerPrivateKey;
	    char mKeyHash[16];
    };

    u8   m_PublicData[sizeof(rlGsLoginCertificate)];            //sizeof(GSLoginCertificate)
    u8   m_PrivateData[sizeof(rlGsLoginCertificatePrivate)];    //sizeof(GSLoginPrivateData)
    bool m_IsPublicValid : 1;
    bool m_IsPrivateValid : 1;
#endif
};

}   // namespace rage

#endif //!NET_SECURE_PLATFORM

#endif //RLINE_RLCREDENTIALS_H
