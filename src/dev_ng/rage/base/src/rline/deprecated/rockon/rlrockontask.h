// 
// rline/rlrockontask.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONTASK_H
#define RLINE_RLROCKONTASK_H

#include "rltask.h"
#include "atl/inlist.h"

namespace rage
{

//PURPOSE
//  Base class for Rockon operations.
class rlRockonTask : public rlTask<class rlRockonClient>
{
friend class rlRockonClient;

public:
    RL_DECL_TASK(rlRockonTask);

    rlRockonTask();
    virtual ~rlRockonTask();

    void Configure(rlRockonClient* client, netStatus* status);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode /* = 0 */);

private:
    rlRockonTask(const rlRockonTask& rhs);
    rlRockonTask& operator=(const rlRockonTask& rhs);

    inlist_node<rlRockonTask> m_ListLink;

    //Set when queued on an rlRockonClient.
    bool m_DeleteWhenFinished   : 1;
};

} //namespace rage

#endif  //RLINE_RLROCKONTASK_H
