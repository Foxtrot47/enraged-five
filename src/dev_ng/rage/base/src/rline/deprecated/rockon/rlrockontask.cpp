// 
// rline/rlrockontask.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrockontask.h"
#include "rlrockonmanager.h"

namespace rage
{
rlRockonTask::rlRockonTask()
: m_DeleteWhenFinished(false)
{
}

rlRockonTask::~rlRockonTask()
{
    if(this->IsPending())
    {
        this->Finish(FINISH_CANCELED, 0);
    }
}

void 
rlRockonTask::Configure(rlRockonClient* client, netStatus* status)
{
    rlTask<rlRockonClient>::Configure(client, status);
}

void 
rlRockonTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTask<rlRockonClient>::Finish(finishType, resultCode);
}

}; //namespace rage
