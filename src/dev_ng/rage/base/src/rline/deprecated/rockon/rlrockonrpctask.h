// 
// rline/rlrockonrpctask.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONRPCTASK_H
#define RLINE_RLROCKONRPCTASK_H

#include "rlrockontask.h"
#include "net/rttp.h"

namespace rage
{

class RsonReader;
class RsonWriter;

//PURPOSE
//  Base class for tasks that send an RSON RPC request to the RockOn SG,
//  and expect a standard {Result} in RSON.
class rlRockonRpcTask : public rlRockonTask
{
public:
    RL_DECL_TASK(rlRockonRpcTask);

    rlRockonRpcTask();
    virtual ~rlRockonRpcTask();

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

protected:
    //Return name of web method, including service it belongs to (ex. MailboxService.GetNumRecords)
    virtual const char* GetMethodName() = 0;

    //Write optional arguments to the method call.  
    //Note that the RockOn SG automatically supplies the title
    //and account ID parameters, so you should never write those.
    virtual bool WriteArgs(RsonWriter& rw) = 0;

    //Read the contents of the {Result} element in the 
    //rlRockonResult returned by the web method.
    virtual bool ReadResult(RsonReader& rr) = 0;

private:
    bool WriteMsg(RsonWriter& rw);
    bool SendRequest();
    bool ProcessRsonResponse(const RsonReader& rr);

    netRttpRequest m_RttpRqst;
    char* m_RqstBuf;
};

} //namespace rage

#endif  //RLINE_RLROCKONRPCTASK_H
