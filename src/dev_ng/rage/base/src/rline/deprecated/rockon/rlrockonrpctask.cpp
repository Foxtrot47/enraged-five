// 
// rline/rlrockontask.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrockonrpctask.h"
#include "rlrockonmanager.h"
#include "rlrockonprotocol.h"
#include "rlrockonresult.h"
#include "data/rson.h"
#include "diag/seh.h"
#include "system/memory.h"

namespace rage
{

//Elements that must appear in every rlRockonRpcTask message.
//These must stay in sync with what the server expects.
static const char* ELEMENT_MSG_TYPE     = "t";
static const char* ELEMENT_METHOD_NAME  = "m";
static const char* ELEMENT_ARGS         = "args";


///////////////////////////////////////////////////////////////////////////////
// rlRockonRpcTask
///////////////////////////////////////////////////////////////////////////////
rlRockonRpcTask::rlRockonRpcTask()
: m_RqstBuf(NULL)
{
}

rlRockonRpcTask::~rlRockonRpcTask()
{
    rlFree(m_RqstBuf);
}

void
rlRockonRpcTask::Start()
{
    rlAssert(!m_RqstBuf);

    this->rlRockonTask::Start();

    if(!m_Ctx->IsLoggedIn())
    {
        rlRockonError("%s failed: local gamer %d not logged in", 
                      GetTaskName(), m_Ctx->GetLocalGamerIndex());
        this->Finish(FINISH_FAILED, 0);
    }
    else if(!this->SendRequest())
    {
        this->Finish(FINISH_FAILED, 0);
    }
}

void
rlRockonRpcTask::Finish(const FinishType finishType, const int resultCode)
{
    this->rlRockonTask::Finish(finishType, resultCode);

    rlAssert(!m_RttpRqst.Pending());

    if(m_RqstBuf)
    {
        rlFree(m_RqstBuf);
        m_RqstBuf = NULL;
    }
}

void 
rlRockonRpcTask::Update(const unsigned timeStep)
{
    rlRockonTask::Update(timeStep);

    if(!m_RttpRqst.Pending())
    {
        if(m_RttpRqst.Succeeded())
        {
            rlRockonDebug2("Received response(%u)", m_RttpRqst.GetResponseMsg().GetTransactionId());

            netRttpMessage msg;
            if(!m_Ctx->ProcessReceivedMsg(m_RttpRqst.GetResponseMsg(), &msg))
            {
                rlRockonError("Error processing received msg");
            }

            RsonReader rr;

            if(!rr.Init((const char*) msg.GetContent(), 0))
            {
                rlRockonError("Error parsing response");
                this->Finish(FINISH_FAILED);
            }
            else if(!this->ProcessRsonResponse(rr))
            {
                rlRockonError("Error processing response");
                this->Finish(FINISH_FAILED);
            }
            else
            {
                this->Finish(FINISH_SUCCEEDED);
            }
        }
        else
        {
            this->Finish(FINISH_FAILED);
        }
    }
}

bool
rlRockonRpcTask::WriteMsg(RsonWriter& rw)
{
    return rw.Begin()
        && rw.WriteInt(ELEMENT_MSG_TYPE, ROCKON_MSG_TYPE_RPC)
        && rw.WriteString(ELEMENT_METHOD_NAME, GetMethodName())
        && rw.Begin(ELEMENT_ARGS)
        && this->WriteArgs(rw)
        && rw.End()
        && rw.End();
}

bool
rlRockonRpcTask::SendRequest()
{
    rtry
    {
        RsonWriter rw;

        //Calculate the size required.
        rw.InitNull();

        rverify(WriteMsg(rw),
                catchall,
                rlRockonError("Failed to write msg to calculate size"));

        const unsigned rqstLen = rw.Length() + 1;

        rverify(0 != (m_RqstBuf = (char*)RL_ALLOCATE(rlRockonRpcTask, rqstLen)),
                catchall,
                rlRockonError("Failed to allocate request buffer (%u chars)", rqstLen));

        rw.Init(m_RqstBuf, rqstLen);

        rverify(WriteMsg(rw),
            catchall,
            rlRockonError("Failed to write msg to request buffer"));
        rlAssert(strlen(m_RqstBuf) == rqstLen - 1);

        rcheck(m_Ctx->SendRequest(m_RqstBuf, &m_RttpRqst),
               catchall,
               rlRockonError("Failed to request message"));

        rlRockonDebug2("Sent request(txid=%u) (%u bytes)",
                        m_RttpRqst.GetRequestMsg().GetTransactionId(),
                        rqstLen - 1);

        return true;
    }
    rcatchall
    {
        if(m_RqstBuf)
        {
            rlFree(m_RqstBuf);
            m_RqstBuf = NULL;
        }

        return false;
    }
}

bool
rlRockonRpcTask::ProcessRsonResponse(const RsonReader& rr)
{
    rtry
    {
        rlRockonResult result;
        rverify(result.Reset(rr), catchall, );

        if(result.Succeeded())
        {
            if(result.HasResult())
            {
                RsonReader rrResult;
                rverify(result.GetResultReader(rrResult), catchall, );

                rcheck(ReadResult(rrResult),
                       catchall,
                       rlRockonError("Failed to read RSON result"));
            }
        }
        else if(result.HasError())
        {
            char errCodeBuf[128];
            char errMsgBuf[2048];

            result.GetErrorCode(errCodeBuf, sizeof(errCodeBuf));
            
            if(result.HasErrorMsg())
            {
                result.GetErrorMsg(errMsgBuf, sizeof(errMsgBuf));
            }

            rlRockonDebug2("ProcessRsonResponse: ErrorCode=%s  Msg=%s", 
                           errCodeBuf,
                           result.HasErrorMsg() ? errMsgBuf : "[No message]");

            //TODO: For the moment, just make this fail the request.
            //      Later, write the error information to the netStatus,
            //      so it's available to the app.

            return false;
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

}; //namespace rage
