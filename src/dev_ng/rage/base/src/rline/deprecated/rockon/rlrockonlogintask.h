// 
// rline/rlrockonlogintask.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONLOGINTASK_H
#define RLINE_RLROCKONLOGINTASK_H

#include "rlrockon.h"
#include "rlrockontask.h"
#include "rlserverresolver.h"
#include "atl/inlist.h"
#include "net/netaddress.h"
#include "net/rttp.h"
#include "net/tcp.h"

namespace rage
{

class rlRockonClient;
class RsonReader;
class RsonWriter;

//PURPOSE
//  Task used by rlRockonClient to login.
class rlRockonLoginTask : public rlRockonTask
{
public:
    RL_DECL_TASK(rlRockonLoginTask);

    rlRockonLoginTask();
    virtual ~rlRockonLoginTask();

    void Configure(rlRockonClient* client, netStatus* status);

    virtual void Start();
    virtual void Finish(const FinishType finishType, const int resultCode);
    virtual void Update(const unsigned timeStep);

private:
    bool ConnectToServer();
    bool SendLoginRequest();
    bool NativeWriteLoginMsg(RsonWriter& rw);

#if !ROCKON_SECURE_PLATFORM
    bool ReadCryptoKeys(RsonReader& rr);
#endif

    enum State
    {
        STATE_RESOLVE_SERVER_ADDR,
        STATE_RESOLVING_SERVER_ADDR,
        STATE_CONNECT,
        STATE_CONNECTING,
        STATE_LOGIN,
        STATE_LOGGING_IN
    };

    State m_State;

    netTcpAsyncOp m_TcpOp;
    netRttpRequest m_LoginRqst;
    netAddress m_ServerAddr;
    char* m_RqstBuf;

    rlServerResolver m_Resolver;
    netStatus m_MyStatus;
};

} //namespace rage

#endif  //RLINE_RLROCKONLOGINTASK_H
