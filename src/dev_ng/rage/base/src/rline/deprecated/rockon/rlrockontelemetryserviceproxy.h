// 
// rline/rlrockontelemetryserviceproxy.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONTELEMETRYSERVICEPROXY_H
#define RLINE_RLROCKONTELEMETRYSERVICEPROXY_H

#include "rlrockonproxy.h"

namespace rage
{

class netStatus;
class rlRockonManager;

class rlRockonTelemetryServiceProxy
{
public:
    //PURPOSE
    //  Sends a line of RSON (given to rlMetricsManager::Save()) to the 
    //  telemetry backend for processing (archiving, saving in DB tables, etc).
    static bool ProcessSubmission(rlRockonManager* rockonMgr,
        const int localGamerIndex,
        const char* rson,
        netStatus* status);
};

} //namespace rage

#endif  //RLINE_RLROCKONTELEMETRYSERVICEPROXY_H
