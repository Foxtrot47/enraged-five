// 
// rline/rlRockonMailboxServiceProxy.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrockonmailboxserviceproxy.h"
#include "rlrockonmanager.h"
#include "rlrockonproxy.h"
#include "rlrockonrpctask.h"
#include "data/rson.h"
#include "net/status.h"

using namespace rage;

//////////////////////////////////////////////////////////////////////////
// BroadcastMessageTask
//////////////////////////////////////////////////////////////////////////
class BroadcastMessageTask : public rlRockonRpcTask
{
public:
    RL_DECL_TASK(BroadcastMessageTask);

    BroadcastMessageTask() {};
    virtual ~BroadcastMessageTask() {};

    void Configure(rlRockonClient* client,
                   const char* mailboxName,
                   const char* senderDisplayName,
                   const u8* userData,
                   const unsigned userDataLength,
                   const u8* msgData,
                   const unsigned msgDataLength,
                   const unsigned expirationMinutes,
                   s64* recordId,
                   netStatus* status)
    {
        m_MailboxName = mailboxName;
        m_SenderDisplayName = senderDisplayName;
        m_UserData = userData;
        m_UserDataLength = userDataLength;
        m_MsgData = msgData;
        m_MsgDataLength = msgDataLength;
        m_ExpirationMinutes = expirationMinutes;
        m_RecordId = recordId;

        rlRockonRpcTask::Configure(client, status);
    }

protected:
    virtual const char* GetMethodName() { return "MailboxService.BroadcastMessage"; }    

    virtual bool WriteArgs(RsonWriter& rw)      
    { 
        return rw.WriteString("mailboxName", m_MailboxName)
            && rw.WriteString("senderDisplayName", m_SenderDisplayName)
            && rw.WriteBinary("userData", m_UserData, m_UserDataLength)
            && rw.WriteBinary("msgData", m_MsgData, m_MsgDataLength)
            && rw.WriteUns("expirationMinutes", m_ExpirationMinutes);
    }

    virtual bool ReadResult(RsonReader& rr)     
    {
        RLROCKONPROXY_READ_UNNAMED_INT64(rr, *m_RecordId);
        return true;
    }

private:
    const char* m_MailboxName;
    const char* m_SenderDisplayName;
    const u8* m_UserData;
    unsigned m_UserDataLength;
    const u8* m_MsgData;
    unsigned m_MsgDataLength;
    unsigned m_ExpirationMinutes;
    s64* m_RecordId;
};

//////////////////////////////////////////////////////////////////////////
// SendMessageTask
//////////////////////////////////////////////////////////////////////////
class SendMessageTask : public rlRockonRpcTask
{
public:
    RL_DECL_TASK(SendMessageTask);

    SendMessageTask() {};
    virtual ~SendMessageTask() {};

    void Configure(rlRockonClient* client,
        const char* mailboxName,
        const char* senderDisplayName,
        const s64 receiverId,
        const u8* userData,
        const unsigned userDataLength,
        const u8* msgData,
        const unsigned msgDataLength,
        const unsigned expirationMinutes,
        s64* recordId,
        netStatus* status)
    {
        m_MailboxName = mailboxName;
        m_SenderDisplayName = senderDisplayName;
        m_ReceiverId = receiverId;
        m_UserData = userData;
        m_UserDataLength = userDataLength;
        m_MsgData = msgData;
        m_MsgDataLength = msgDataLength;
        m_ExpirationMinutes = expirationMinutes;
        m_RecordId = recordId;

        rlRockonRpcTask::Configure(client, status);
    }

protected:
    virtual const char* GetMethodName() { return "MailboxService.BroadcastMessage"; }    

    virtual bool WriteArgs(RsonWriter& rw)      
    { 
        return rw.WriteString("mailboxName", m_MailboxName)
            && rw.WriteString("senderDisplayName", m_SenderDisplayName)
            && rw.WriteInt64("receiverId", m_ReceiverId)
            && rw.WriteBinary("userData", m_UserData, m_UserDataLength)
            && rw.WriteBinary("msgData", m_MsgData, m_MsgDataLength)
            && rw.WriteUns("expirationMinutes", m_ExpirationMinutes);
    }

    virtual bool ReadResult(RsonReader& rr)     
    {
        RLROCKONPROXY_READ_UNNAMED_INT64(rr, *m_RecordId);
        return true;
    }

private:
    const char* m_MailboxName;
    const char* m_SenderDisplayName;
    s64 m_ReceiverId;
    const u8* m_UserData;
    unsigned m_UserDataLength;
    const u8* m_MsgData;
    unsigned m_MsgDataLength;
    unsigned m_ExpirationMinutes;
    s64* m_RecordId;
};

//////////////////////////////////////////////////////////////////////////
// GetNumRecordsTask
//////////////////////////////////////////////////////////////////////////
class GetNumRecordsTask : public rlRockonRpcTask
{
public:
    RL_DECL_TASK(GetNumRecordsTask);

    GetNumRecordsTask() {};
    virtual ~GetNumRecordsTask() {};

    void Configure(rlRockonClient* client,
                   const char* mailboxName,
                   const s64 startRecordId,
                   const s64 endRecordId,
                   const bool includeExpired,
                   unsigned* numRecords,
                   netStatus* status)
    {
        m_MailboxName = mailboxName;
        m_StartRecordId = startRecordId;
        m_EndRecordId = endRecordId;
        m_IncludeExpired = includeExpired;
        m_NumRecords = numRecords;

        rlRockonRpcTask::Configure(client, status);
    }

protected:
    virtual const char* GetMethodName() { return "MailboxService.GetNumRecords"; }    
    
    virtual bool WriteArgs(RsonWriter& rw)      
    { 
        return rw.WriteString("mailboxName", m_MailboxName)
            && rw.WriteInt64("startRecordId", m_StartRecordId)
            && rw.WriteInt64("endRecordId", m_EndRecordId)
            && rw.WriteBool("includeExpired", m_IncludeExpired);
    }

    virtual bool ReadResult(RsonReader& rr)     
    {
        RLROCKONPROXY_READ_UNNAMED_UNS(rr, *m_NumRecords);
        return true;
    }

private:
    const char* m_MailboxName;
    s64 m_StartRecordId;
    s64 m_EndRecordId;
    bool m_IncludeExpired;
    unsigned* m_NumRecords;
};

//////////////////////////////////////////////////////////////////////////
// ReadRecordsTask
//////////////////////////////////////////////////////////////////////////
class ReadRecordsTask : public rlRockonRpcTask
{
public:
    RL_DECL_TASK(ReadRecordsTask);

    ReadRecordsTask() {};
    virtual ~ReadRecordsTask() {};

    void Configure(rlRockonClient* client,
                   const char* mailboxName,
                   const s64 startRecordId,
                   const s64 endRecordId,
                   const unsigned maxRecords,
                   const bool readUserData,
                   const bool readMsgData,
                   const bool oldestFirst,
                   rlRockonMailboxServiceProxy::MailboxRecord* records,
                   unsigned* numRecords,
                   netStatus* status)
    {
        m_MailboxName = mailboxName;
        m_StartRecordId = startRecordId;
        m_EndRecordId = endRecordId;
        m_MaxRecords = maxRecords;
        m_ReadUserData = readUserData;
        m_ReadMsgData = readMsgData;
        m_OldestFirst = oldestFirst;
        m_Records = records;
        m_NumRecords = numRecords;

        rlRockonRpcTask::Configure(client, status);
    }

protected:
    virtual const char* GetMethodName() { return "MailboxService.ReadRecords"; }

    virtual bool WriteArgs(RsonWriter& rw)      
    { 
        return rw.WriteString("mailboxName", m_MailboxName)
            && rw.WriteInt64("startRecordId", m_StartRecordId)
            && rw.WriteInt64("endRecordId", m_EndRecordId)
            && rw.WriteUns("maxRecords", m_MaxRecords)
            && rw.WriteBool("readUserData", m_ReadUserData)
            && rw.WriteBool("readMsgData", m_ReadMsgData)
            && rw.WriteBool("oldestFirst", m_OldestFirst);
    }

    virtual bool ReadResult(RsonReader& rr)     
    {
        if(!rr.GetMember("Records", &rr))
        {
            rlRockonError("Failed to find m_Records");
            return false;
        }

        *m_NumRecords = 0;

        RsonReader rrChild;
        for(bool success = rr.GetFirstMember(&rrChild); success; (success = rrChild.GetNextSibling(&rrChild)), (*m_NumRecords)++)
        {
            rlRockonMailboxServiceProxy::MailboxRecord& rec = m_Records[*m_NumRecords];

            RLROCKONPROXY_READ_INT64(rrChild, "RecordId", rec.m_RecordId);
            RLROCKONPROXY_READ_INT64(rrChild, "SenderId", rec.m_SenderId);
            RLROCKONPROXY_READ_STRING(rrChild, "SenderDisplayName", rec.m_SenderDisplayName);
            RLROCKONPROXY_READ_UNS(rrChild, "UserDataLength", rec.m_UserDataLength);
            RLROCKONPROXY_READ_BINARY(rrChild, "UserData", rec.m_UserData);
            RLROCKONPROXY_READ_UNS(rrChild, "MsgDataLength", rec.m_MsgDataLength);
            RLROCKONPROXY_READ_BINARY(rrChild, "MsgData", rec.m_MsgData);
            RLROCKONPROXY_READ_STRING(rrChild, "SendDateTime", rec.m_SendDateTime);
            RLROCKONPROXY_READ_STRING(rrChild, "ExpirationDateTime", rec.m_ExpirationDateTime);
        }

        return true;
    }

private:
    const char* m_MailboxName;
    s64 m_StartRecordId;
    s64 m_EndRecordId;
    unsigned m_MaxRecords;
    bool m_ReadUserData;
    bool m_ReadMsgData;
    bool m_OldestFirst;
    rlRockonMailboxServiceProxy::MailboxRecord* m_Records;
    unsigned* m_NumRecords;
};

//////////////////////////////////////////////////////////////////////////
// ReadMsgDataTask
//////////////////////////////////////////////////////////////////////////
class ReadMsgDataTask : public rlRockonRpcTask
{
public:
    RL_DECL_TASK(ReadMsgDataTask);

    ReadMsgDataTask() {};
    virtual ~ReadMsgDataTask() {};

    void Configure(rlRockonClient* client,
                   const char* mailboxName,
                   const s64 recordId,
                   rlRockonProxyBuffer<u8>* msgData,
                   netStatus* status)
    {
        m_MailboxName = mailboxName;
        m_RecordId = recordId;
        m_MsgData = msgData;

        rlRockonRpcTask::Configure(client, status);
    }

protected:
    virtual const char* GetMethodName() { return "MailboxService.ReadMsgData"; }    

    virtual bool WriteArgs(RsonWriter& rw)      
    { 
        return rw.WriteString("mailboxName", m_MailboxName)
            && rw.WriteInt64("recordId", m_RecordId);
    }

    virtual bool ReadResult(RsonReader& rr)     
    {
        RLROCKONPROXY_READ_UNNAMED_BINARY(rr, *m_MsgData);

        return true;
    }

private:
    const char* m_MailboxName;
    s64 m_RecordId;
    rlRockonProxyBuffer<u8>* m_MsgData;
};

//////////////////////////////////////////////////////////////////////////
// DeleteRecordsTask
//////////////////////////////////////////////////////////////////////////
class DeleteRecordsTask : public rlRockonRpcTask
{
public:
    RL_DECL_TASK(DeleteRecordsTask);

    DeleteRecordsTask() {};
    virtual ~DeleteRecordsTask() {};

    void Configure(rlRockonClient* client,
        const char* mailboxName,
        const s64 startRecordId,
        const s64 endRecordId,
        unsigned* numDeleted,
        netStatus* status)
    {
        m_MailboxName = mailboxName;
        m_StartRecordId = startRecordId;
        m_EndRecordId = endRecordId;
        m_NumDeleted = numDeleted;
        rlRockonRpcTask::Configure(client, status);
    }

protected:
    virtual const char* GetMethodName() { return "MailboxService.DeleteRecords"; }    

    virtual bool WriteArgs(RsonWriter& rw)      
    { 
        return rw.WriteString("mailboxName", m_MailboxName)
            && rw.WriteInt64("startRecordId", m_StartRecordId)
            && rw.WriteInt64("endRecordId", m_EndRecordId);
    }

    virtual bool ReadResult(RsonReader& rr)
    {
        RLROCKONPROXY_READ_UNNAMED_UNS(rr, *m_NumDeleted);
        return true;
    }

private:
    const char* m_MailboxName;
    s64 m_StartRecordId;
    s64 m_EndRecordId;
    unsigned* m_NumDeleted;
};

//////////////////////////////////////////////////////////////////////////
// rlRockonMailboxServiceProxy
//////////////////////////////////////////////////////////////////////////
bool 
rlRockonMailboxServiceProxy::BroadcastMessage(rlRockonManager* rockonMgr,
                                              const int localGamerIndex,
                                              const char* mailboxName,                     
                                              const char* senderDisplayName,
                                              const u8* userData,
                                              const unsigned userDataLength,
                                              const u8* msgData,
                                              const unsigned msgDataLength,
                                              const unsigned expirationMinutes,
                                              s64* recordId,
                                              netStatus* status)
{
    RLROCKONPROXY_CREATETASK(BroadcastMessageTask,
                             rockonMgr,
                             localGamerIndex,
                             mailboxName,
                             senderDisplayName,
                             userData,
                             userDataLength,
                             msgData,
                             msgDataLength,
                             expirationMinutes,
                             recordId,
                             status);
}

bool 
rlRockonMailboxServiceProxy::SendMessage(rlRockonManager* rockonMgr,
                                         const int localGamerIndex,
                                         const char* mailboxName,                     
                                         const char* senderDisplayName,
                                         const s64 receiverId,
                                         const u8* userData,
                                         const unsigned userDataLength,
                                         const u8* msgData,
                                         const unsigned msgDataLength,
                                         const unsigned expirationMinutes,
                                         s64* recordId,
                                         netStatus* status)
{
    RLROCKONPROXY_CREATETASK(SendMessageTask,
        rockonMgr,
        localGamerIndex,
        mailboxName,
        senderDisplayName,
        receiverId,
        userData,
        userDataLength,
        msgData,
        msgDataLength,
        expirationMinutes,
        recordId,
        status);
}

bool 
rlRockonMailboxServiceProxy::GetNumRecords(rlRockonManager* rockonMgr,
                                             const int localGamerIndex,
                                             const char* mailboxName,
                                             const s64 startRecordId,
                                             const s64 endRecordId,
                                             const bool includeExpired,
                                             unsigned* numRecords,
                                             netStatus* status)
{
    RLROCKONPROXY_CREATETASK(GetNumRecordsTask, 
                             rockonMgr, 
                             localGamerIndex, 
                             mailboxName, 
                             startRecordId,
                             endRecordId,
                             includeExpired,
                             numRecords, 
                             status);
}

bool 
rlRockonMailboxServiceProxy::ReadRecords(rlRockonManager* rockonMgr,
                                            const int localGamerIndex,
                                            const char* mailboxName,
                                            const s64 startRecordId,
                                            const s64 endRecordId,
                                            const unsigned maxRecords,
                                            const bool readUserData,
                                            const bool readMsgData,
                                            const bool oldestFirst,
                                            MailboxRecord* records,
                                            unsigned* numRecords,
                                            netStatus* status)
{
    RLROCKONPROXY_CREATETASK(ReadRecordsTask, 
                             rockonMgr, 
                             localGamerIndex, 
                             mailboxName, 
                             startRecordId,
                             endRecordId,
                             maxRecords,
                             readUserData,
                             readMsgData,
                             oldestFirst,
                             records, 
                             numRecords,
                             status);
}
 
bool
rlRockonMailboxServiceProxy::ReadMsgData(rlRockonManager* rockonMgr,
                                          const int localGamerIndex,
                                          const char* mailboxName,
                                          const s64 recordId,
                                          rlRockonProxyBuffer<u8>* msgData,
                                          netStatus* status)
{
    RLROCKONPROXY_CREATETASK(ReadMsgDataTask,
                             rockonMgr,
                             localGamerIndex,
                             mailboxName,
                             recordId,
                             msgData,
                             status);
}

bool
rlRockonMailboxServiceProxy::DeleteRecords(rlRockonManager* rockonMgr,
                                           const int localGamerIndex,
                                           const char* mailboxName,
                                           const s64 startRecordId,
                                           const s64 endRecordId,
                                           unsigned* numDeleted,
                                           netStatus* status)
{
    RLROCKONPROXY_CREATETASK(DeleteRecordsTask,
                             rockonMgr,
                             localGamerIndex,
                             mailboxName,
                             startRecordId,
                             endRecordId,
                             numDeleted,
                             status);
}