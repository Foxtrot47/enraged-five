// 
// rline/rlrockonresult.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrockonresult.h"

namespace rage
{

rlRockonResult::rlRockonResult()
{
    Clear();
}

rlRockonResult::~rlRockonResult()
{
}

void
rlRockonResult::Clear()
{
    m_Success = false;
    m_HasError = false;
    m_HasErrorMsg = false;
    m_HasResult = false;
}

bool 
rlRockonResult::Reset(const RsonReader& rr)
{
    Clear();

    bool success;
    if(!rr.ReadBool("Success", success))
    {
        rlRockonError("Failed to read {Success} element");
        return false;
    }

    m_Success = success;

    if(m_Success)
    {            
        if(rr.GetMember("Result", &m_ResultReader))
        {
            m_HasResult = true;
        }
    }
    else
    {
        if(rr.GetMember("Error", &m_ErrorReader))
        {
            m_HasError = true;

            RsonReader rrTemp;
            m_HasErrorMsg = m_ErrorReader.GetMember("Msg", &rrTemp);
        }
    }

    return true;
}

bool 
rlRockonResult::Succeeded() const
{
    return m_Success;
}

bool 
rlRockonResult::HasError() const
{
    return m_HasError;
}

bool 
rlRockonResult::HasErrorMsg() const
{
    return m_HasErrorMsg;
}

bool 
rlRockonResult::HasResult() const
{
    return m_HasResult;
}

bool
rlRockonResult::GetErrorCode(char* buf, const unsigned bufLen) const
{
    rlAssert(HasError());

    if(!m_ErrorReader.ReadString("Code", buf, bufLen))
    {
        rlRockonError("Failed to read {Code} error element");
        return false;
    }

    return true;
}

bool
rlRockonResult::GetErrorMsg(char* buf, const unsigned bufLen) const
{
    rlAssert(HasError() && HasErrorMsg());

    if(!m_ErrorReader.ReadString("Msg", buf, bufLen))
    {
        rlRockonError("Failed to read {Msg} error element");
        return false;
    }

    return true;
}

bool
rlRockonResult::GetResultReader(RsonReader& rr) const
{
    rlAssert(HasResult());

    //Give them a copy of the Result reader.
    rr = m_ResultReader;

    return true;
}

} //namespace rage
