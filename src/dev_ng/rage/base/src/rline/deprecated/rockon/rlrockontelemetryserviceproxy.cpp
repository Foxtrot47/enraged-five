// 
// rline/rlRockontelemetryServiceProxy.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrockontelemetryserviceproxy.h"
#include "rlrockonmanager.h"
#include "rlrockonproxy.h"
#include "rlrockonrpctask.h"
#include "data/rson.h"
#include "net/status.h"

using namespace rage;

//////////////////////////////////////////////////////////////////////////
// ProcessSubmissionTask
//////////////////////////////////////////////////////////////////////////
class ProcessSubmissionTask : public rlRockonRpcTask
{
public:
    RL_DECL_TASK(ProcessSubmissionTask);

    ProcessSubmissionTask() {};
    virtual ~ProcessSubmissionTask() {};

    void Configure(rlRockonClient* client,
                   const char* rson,
                   netStatus* status)
    {
        m_Rson = rson;

        rlRockonRpcTask::Configure(client, status);
    }

protected:
    virtual const char* GetMethodName() { return "TelemetryService.ProcessSubmission"; }    

    virtual bool WriteArgs(RsonWriter& rw)      
    { 
        return rw.WriteString("rson", m_Rson);
    }

    virtual bool ReadResult(RsonReader& /*rr*/)     
    {
        return true;
    }

private:
    const char* m_Rson;
};

//////////////////////////////////////////////////////////////////////////
// rlRockonTelemetryServiceProxy
//////////////////////////////////////////////////////////////////////////
bool 
rlRockonTelemetryServiceProxy::ProcessSubmission(rlRockonManager* rockonMgr,
                                              const int localGamerIndex,
                                              const char* rson,
                                              netStatus* status)
{
    RLROCKONPROXY_CREATETASK(ProcessSubmissionTask,
                             rockonMgr,
                             localGamerIndex,
                             rson,
                             status);
}
