// 
// rline/rlrockonproxy.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONPROXY_H
#define RLINE_RLROCKONPROXY_H

#include "rlrockon.h"

namespace rage
{

//PURPOSE
//  Structure used by proxy classes to help read string or binary values.
//  Users are expected to call Reset() to provide a pointer to a buffer 
//  that they own.  The proxy code fills the buffer with string or binary
//  data, and sets the length variable so the user code can tell how
//  much was read.
//
template<class T>
struct rlRockonProxyBuffer
{
    rlRockonProxyBuffer()
    {
        Reset(0, 0);
    }

    void Reset(T* buf, const unsigned maxLength)
    {
        m_Buf = buf;
        m_MaxLength = maxLength;
        m_Length = 0;
    }

    T* m_Buf;
    unsigned m_MaxLength;
    unsigned m_Length;
};


//PURPOSE
//  Macro for creating and queuing a proxy task with an rlRockonManager.
//
#define RLROCKONPROXY_CREATETASK(taskname, rockonMgr, localGamerIndex, ...) \
    rlRockonClient* client = rockonMgr->GetClient(localGamerIndex); \
    taskname* task = client->CreateAndQueueTask<taskname>(); \
    if(!task) \
    { \
        rlRockonError("Failed to create "#taskname); \
        return false; \
    } \
    task->Configure(client, __VA_ARGS__); \
    return true;


//PURPOSE
//  Macros to simplify reading result values in the proxy code.
//
#define RLROCKONPROXY_READ_BOOL(rr, name, val)  RLROCKONPROXY_READ_VALUE(rr, ReadBool, name, val)
#define RLROCKONPROXY_READ_FLOAT(rr, name, val) RLROCKONPROXY_READ_VALUE(rr, ReadFloat, name, val)
#define RLROCKONPROXY_READ_INT(rr, name, val)   RLROCKONPROXY_READ_VALUE(rr, ReadInt, name, val)
#define RLROCKONPROXY_READ_INT64(rr, name, val) RLROCKONPROXY_READ_VALUE(rr, ReadInt64, name, val)
#define RLROCKONPROXY_READ_UNS(rr, name, val)   RLROCKONPROXY_READ_VALUE(rr, ReadUns, name, val)
#define RLROCKONPROXY_READ_UNS64(rr, name, val) RLROCKONPROXY_READ_VALUE(rr, ReadUns64, name, val)

#define RLROCKONPROXY_READ_STRING(rr, name, proxybuf) \
    if((proxybuf).m_Buf) \
    { \
        const char* tmpName = (name); /*Workaround for SNC warning 237 "controlling expression is constant"*/ \
        int temp = tmpName ? (rr).GetValueLength(name) : (rr).GetValueLength(); \
        if(-1 != temp) \
        { \
            if((unsigned)temp > (proxybuf).m_MaxLength) \
            { \
                return false; \
            } \
            else if(tmpName && !(rr).ReadString(name, (proxybuf).m_Buf, (proxybuf).m_MaxLength)) \
            { \
                return false; \
            } \
            else if(!tmpName && !(rr).AsString((proxybuf).m_Buf, (proxybuf).m_MaxLength)) \
            { \
                return false; \
            } \
            (proxybuf).m_Length = (unsigned)temp; \
        } \
        else \
        { \
            (proxybuf).m_Length = 0; \
        } \
    }

#define RLROCKONPROXY_READ_BINARY(rr, name, proxybuf) \
    if((proxybuf).m_Buf) \
    { \
        const char* tmpName = (name); /*Workaround for SNC warning 237 "controlling expression is constant"*/ \
        int temp = tmpName ? (rr).GetBinaryValueLength(name) : (rr).GetBinaryValueLength(); \
        if(-1 != temp) \
        { \
            if((unsigned)temp > (proxybuf).m_MaxLength) \
            { \
                return false; \
            } \
            else if(tmpName && !(rr).ReadBinary(name, (proxybuf).m_Buf, (proxybuf).m_MaxLength, &temp)) \
            { \
                return false; \
            } \
            else if(!tmpName && !(rr).AsBinary((proxybuf).m_Buf, (proxybuf).m_MaxLength, &temp)) \
            { \
                return false; \
            } \
            (proxybuf).m_Length = (unsigned)temp; \
        } \
        else \
        { \
            (proxybuf).m_Length = 0; \
        } \
    }

#define RLROCKONPROXY_READ_UNNAMED_BOOL(rr, val)  RLROCKONPROXY_READ_UNNAMED_VALUE(rr, AsBool, val)
#define RLROCKONPROXY_READ_UNNAMED_FLOAT(rr, val) RLROCKONPROXY_READ_UNNAMED_VALUE(rr, AsFloat, val)
#define RLROCKONPROXY_READ_UNNAMED_INT(rr, val)   RLROCKONPROXY_READ_UNNAMED_VALUE(rr, AsInt, val)
#define RLROCKONPROXY_READ_UNNAMED_INT64(rr, val) RLROCKONPROXY_READ_UNNAMED_VALUE(rr, AsInt64, val)
#define RLROCKONPROXY_READ_UNNAMED_UNS(rr, val)   RLROCKONPROXY_READ_UNNAMED_VALUE(rr, AsUns, val)
#define RLROCKONPROXY_READ_UNNAMED_UNS64(rr, val) RLROCKONPROXY_READ_UNNAMED_VALUE(rr, AsUns64, val)

#define RLROCKONPROXY_READ_UNNAMED_STRING(rr, proxybuf) \
    RLROCKONPROXY_READ_STRING(rr, 0, proxybuf)

#define RLROCKONPROXY_READ_UNNAMED_BINARY(rr, proxybuf) \
    RLROCKONPROXY_READ_BINARY(rr, 0, proxybuf)

#define RLROCKONPROXY_READ_VALUE(rr, op, name, val) \
    if(!rr.op(name, val)) \
    { \
        rlRockonError(#op"(\""#name"\", "#val") failed"); \
        return false; \
    }

#define RLROCKONPROXY_READ_UNNAMED_VALUE(rr, op, val) \
    if(!rr.op(val)) \
    { \
        rlRockonError(#op"("#val") failed"); \
        return false; \
    }

} //namespace rage

#endif  //RLINE_RLROCKONPROXY_H
