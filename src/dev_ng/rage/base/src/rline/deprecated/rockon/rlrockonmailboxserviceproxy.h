// 
// rline/rlrockonmailboxserviceproxy.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONMAILBOXSERVICEPROXY_H
#define RLINE_RLROCKONMAILBOXSERVICEPROXY_H

#include "rlrockonproxy.h"

namespace rage
{

class netStatus;
class rlRockonManager;

class rlRockonMailboxServiceProxy
{
public:
    struct MailboxRecord
    {
        MailboxRecord()
        : m_RecordId(0)
        , m_SenderId(0)
        , m_UserDataLength(0)
        , m_MsgDataLength(0)
        {
        }

        s64 m_RecordId;
        
        s64 m_SenderId;
        rlRockonProxyBuffer<char> m_SenderDisplayName;

        rlRockonProxyBuffer<u8> m_UserData;
        unsigned m_UserDataLength;

        rlRockonProxyBuffer<u8> m_MsgData;
        unsigned m_MsgDataLength;

        rlRockonProxyBuffer<char> m_SendDateTime;
        rlRockonProxyBuffer<char> m_ExpirationDateTime;
    };

    static bool BroadcastMessage(rlRockonManager* rockonMgr,
        const int localGamerIndex,
        const char* mailboxName,                     
        const char* senderDisplayName,
        const u8* userData,
        const unsigned userDataLength,
        const u8* msgData,
        const unsigned msgDataLength,
        const unsigned expirationMinutes,
        s64* recordId,
        netStatus* status);

    static bool SendMessage(rlRockonManager* rockonMgr,
        const int localGamerIndex,
        const char* mailboxName,                     
        const char* senderDisplayName,
        const s64 receiverId,
        const u8* userData,
        const unsigned userDataLength,
        const u8* msgData,
        const unsigned msgDataLength,
        const unsigned expirationMinutes,
        s64* recordId,
        netStatus* status);

    static bool GetNumRecords(rlRockonManager* rockonMgr,
        const int localGamerIndex,
        const char* mailboxName,
        const s64 startRecordId,
        const s64 endRecordId,
        const bool includeExpired,
        unsigned* numRecords,
        netStatus* status);

    static bool ReadRecords(rlRockonManager* rockonMgr,
        const int localGamerIndex,
        const char* mailboxName,
        const s64 startRecordId,
        const s64 endRecordId,
        const unsigned maxRecords,
        const bool readUserData,
        const bool readMsgData,
        const bool oldestFirst,
        MailboxRecord* records,
        unsigned* numRecords,
        netStatus* status);
 
    static bool ReadMsgData(rlRockonManager* rockonMgr,
        const int localGamerIndex,
        const char* mailboxName,
        const s64 recordId,
        rlRockonProxyBuffer<u8>* msgData,
        netStatus* status);

    static bool DeleteRecords(rlRockonManager* rockonMgr,
        const int localGamerIndex,
        const char* mailboxName,
        const s64 startRecordId,
        const s64 endRecordId,
        unsigned* numDeleted,
        netStatus* status);
};

} //namespace rage

#endif  //RLINE_RLROCKONMAILBOXSERVICEPROXY_H
