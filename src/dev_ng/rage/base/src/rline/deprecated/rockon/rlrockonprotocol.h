// 
// rline/rlrockonprotocol.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONPROTOCOL_H
#define RLINE_RLROCKONPROTOCOL_H

namespace rage
{

//NOTE: Any time the protocol changes at all, the version number must be
//      incremented and the SG synced.
enum eRockonProtocolVersions
{
    ROCKON_VERSION_0       = 0,    //Original version
    ROCKON_NUM_VERSIONS,
    ROCKON_CURRENT_VERSION = ROCKON_VERSION_0
};

//NOTE: These message types must stay in sync with the SG.
enum eRockonMsgTypes
{
    ROCKON_MSG_TYPE_RPC         = 0,
    ROCKON_MSG_TYPE_TELEMETRY   = 1,
    ROCKON_NUM_MSG_TYPES
};

} //namespace rage

#endif  //RLINE_RLROCKONPROTOCOL_H
