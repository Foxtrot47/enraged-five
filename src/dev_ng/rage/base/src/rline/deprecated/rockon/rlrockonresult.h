// 
// rline/rlrockonresult.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLROCKONRESULT_H
#define RLINE_RLROCKONRESULT_H

#include "rlrockon.h"
#include "data/rson.h"

namespace rage
{

//PURPOSE
//  Reads a standard Rockon web method result, in RSON.  Users pass the result RSON
//  to Reset(), and can then use accessors to query the result details.
//
//  Every result has a true/false success result that can be checked with Succeeded().
//
//  If Succeeded() return true, then there may be an optional Result block
//  containing call-specific data.
//
//  If Succeeded() returns false, then there may be an optional Error block that
//  contains a mandatory error code and an optional error message.
class rlRockonResult
{
public:
    rlRockonResult();
    ~rlRockonResult();

    void Clear();

    bool Reset(const RsonReader& rr);

    bool Succeeded() const;

    bool HasError() const;
    bool HasErrorMsg() const;
    bool HasResult() const;

    bool GetErrorCode(char* buf, const unsigned bufLen) const;
    bool GetErrorMsg(char* buf, const unsigned bufLen) const;
    bool GetResultReader(RsonReader& rr) const;

private:
    rlRockonResult(const rlRockonResult& rhs);

    bool m_Success      : 1;
    bool m_HasError     : 1;
    bool m_HasErrorMsg  : 1;
    bool m_HasResult    : 1;

    RsonReader m_ErrorReader;
    RsonReader m_ResultReader;
};

} //namespace rage

#endif  //RLINE_RLROCKONRESULT_H
