// 
// rline/rlrockonlogintask.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlrockonlogintask.h"

#include "rlgamerinfo.h"
#include "rlrockonclient.h"
#include "rlrockonprotocol.h"
#include "rlrockonresult.h"
#include "rltitleid.h"
#include "data/rson.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "net/tcp.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"

#if __LIVE || __WIN32PC
#include "system/xtl.h"
#elif __PPU
#include <netdb.h>
#include <arpa/inet.h>
#include "rlnp.h"
#include "net/crypto.h"
#endif  //__LIVE || __WIN32PC

namespace rage
{

//Elements that appear in the login msg.
//These must be in sync with what the server is expecting.
static const char* ELEMENT_VERSION      = "v";
static const char* ELEMENT_TITLE_ID     = "titleId";
#if __LIVE
static const char* ELEMENT_XUID         = "xuid";
static const char* ELEMENT_GAMERTAG     = "gamertag";
#elif __PPU
static const char* ELEMENT_TICKET       = "ticket";
#endif


#if __LIVE

static const char ROCKON_SG_TITLE_SERVER_NAME[]  = {"rockonsg"};
XPARAM(rockonxlspconnecttimeoutms);
const unsigned DEFAULT_XLSP_SG_CONNECT_TIMEOUT_MS = 15;

#else

//Platforms that communicate directly with the Rockon SG get its
//address by the URL corresponding to the environment we're in.
static const char* ROCKON_ENV_SG_URL[] =
{
    "dev.sg.rockon.rockstargames.com",
    "cert.sg.rockon.rockstargames.com",
    "sg.rockon.rockstargames.com"
};

#endif

//Default port on which to connect to the SG
static const u16 ROCKON_SG_DEFAULT_PORT = 12121;

extern const rlTitleId* g_rlTitleId;

XPARAM(rockontcpconnecttimeoutsecs);
static const unsigned DEFAULT_TCP_CONNECT_TIMEOUT_SECS  = 15;

//Command line options for specifying the rockon server IP/port.
//These will override automatic server discovery.
XPARAM(rockonip);
XPARAM(rockonport);

//////////////////////////////////////////////////////////////////////////
// rlRockonLoginTask::LoginTask
//////////////////////////////////////////////////////////////////////////
rlRockonLoginTask::rlRockonLoginTask()
: m_RqstBuf(NULL)
{
};

rlRockonLoginTask::~rlRockonLoginTask()
{
    rlFree(m_RqstBuf);
};

void
rlRockonLoginTask::Configure(rlRockonClient* client, 
                             netStatus* status)
{
    rlAssert(!m_RqstBuf);
    rlAssert(-1 == client->m_Socket);

    m_ServerAddr.Clear();

    rlRockonTask::Configure(client, status);
}

void
rlRockonLoginTask::Start()
{
    rlRockonDebug2("Local gamer %d logging in...", m_Ctx->GetLocalGamerIndex());

    rlAssert(!m_RqstBuf);
    rlAssert(-1 == m_Ctx->m_Socket);

    const char* ipStr;
    int port;

    if(PARAM_rockonip.Get(ipStr))
    {
        unsigned ip = inet_addr(ipStr);

        if(rlVerify(INADDR_NONE != ip))
        {
            ip = ntohl(ip);

            if(PARAM_rockonport.Get(port))
            {
                m_ServerAddr.Reset(ip, (unsigned short) port);
            }
            else
            {
                m_ServerAddr.Reset(ip, ROCKON_SG_DEFAULT_PORT);
            }
        }
    }
    else
    {
        m_ServerAddr.Clear();
    }

    if(m_ServerAddr.IsValid())
    {
        m_State = STATE_CONNECT;
    }
    else
    {
        m_State = STATE_RESOLVE_SERVER_ADDR;
    }

    this->rlRockonTask::Start();
}

void
rlRockonLoginTask::Finish(const FinishType finishType, const int resultCode)
{
    rlRockonDebug2("Local gamer %d login %s",
                    m_Ctx->GetLocalGamerIndex(),
                    FINISH_SUCCEEDED == finishType ? "succeeded" : "failed");

    rlAssert(!m_TcpOp.Pending());
    rlAssert(!m_LoginRqst.Pending());

    this->rlRockonTask::Finish(finishType, resultCode);

    if(FINISH_SUCCEEDED != finishType)
    {
         m_ServerAddr.Clear();
    }

    m_LoginRqst.Dispose();
    rlFree(m_RqstBuf);
    m_RqstBuf = NULL;
}

void
rlRockonLoginTask::Update(const unsigned timeStep)
{
    rlRockonTask::Update(timeStep);

    if(!IsActive())
    {
        return;
    }

    if(!rlGamerInfo::IsOnline(m_Ctx->GetLocalGamerIndex()))
    {
        this->Finish(FINISH_FAILED, 0);
    }
    else
    {
        switch(m_State)
        {
        case STATE_RESOLVE_SERVER_ADDR:
#if __LIVE
            if(m_Resolver.Resolve(ROCKON_SG_TITLE_SERVER_NAME, 
                                  &m_ServerAddr, 
                                  &m_MyStatus))
#else   
            if(m_Resolver.Resolve(ROCKON_ENV_SG_URL[g_rlTitleId->m_RockonTitleId.GetEnvironment()],
                                  ROCKON_SG_DEFAULT_PORT,
                                  &m_ServerAddr,
                                  &m_MyStatus))
#endif
            {
                m_State = STATE_RESOLVING_SERVER_ADDR;
            }
            else
            {
                rlRockonError("Resolve() failed");
                this->Finish(FINISH_FAILED, 0);
            }
            break;

        case STATE_RESOLVING_SERVER_ADDR:
            if(m_MyStatus.Succeeded())
            {
                rlRockonDebug2("Resolved server address: %d.%d.%d.%d:%d", NET_ADDR_FOR_PRINTF(m_ServerAddr));
                m_State = STATE_CONNECT;
            }
            else if(!m_MyStatus.Pending())
            {
                rlRockonError("Failed to resolve server address");
                this->Finish(FINISH_FAILED, 0);
            }
            break;

        case STATE_CONNECT:
            if(this->ConnectToServer())
            {
                m_State = STATE_CONNECTING;
            }
            else
            {
                rlRockonError("ConnectToServer() failed");
                this->Finish(FINISH_FAILED, 0);
            }
            break;
        case STATE_CONNECTING:
            if(m_TcpOp.Succeeded())
            {
                if(m_Ctx->InitRttp())
                {
                    m_State = STATE_LOGIN;
                }
                else
                {
                    rlRockonError("Failed to initialize RTTP");
                    this->Finish(FINISH_FAILED, 0);
                }
            }
            else if(!m_TcpOp.Pending())
            {
                rlRockonError("Failed to connect to server");
                this->Finish(FINISH_FAILED, 0);
            }
            break;
        case STATE_LOGIN:
            if(this->SendLoginRequest())
            {
                m_State = STATE_LOGGING_IN;
            }
            else
            {
                rlRockonError("Login() failed");
                this->Finish(FINISH_FAILED, 0);
            }
            break;
        case STATE_LOGGING_IN:
            if(m_LoginRqst.Succeeded())
            {
                RsonReader rr;
                rlRockonResult result;

                if(!rr.Init((const char*)m_LoginRqst.GetResponseMsg().GetContent(), 0) || !result.Reset(rr))
                {
                   rlRockonError("Failed to parse login result");
                   this->Finish(FINISH_FAILED, 0);
                }
                else if(result.Succeeded())
                {
#if !ROCKON_SECURE_PLATFORM
                    //On nonsecure platforms, we require crypto keys to be passed
                    //int the login message. If they are not received, login fails.
                    bool success = false;

                    if(result.HasResult()
                        && result.GetResultReader(rr) 
                        && ReadCryptoKeys(rr))
                    {
                        rlRockonDebug2("Read crypto keys");
                        success = true;
                    }
                    else
                    {
                        rlRockonError("Failed to read crypto keys");
                    }

                    this->Finish(success ? FINISH_SUCCEEDED : FINISH_FAILED, 0);
#else
                    this->Finish(FINISH_SUCCEEDED, 0);
#endif                
                }
                else if(result.HasError())
                {
                    char errorCodeBuf[128];
                    if(result.GetErrorCode(errorCodeBuf, sizeof(errorCodeBuf)))
                    {
                        if(result.HasErrorMsg())
                        {
                            char errorMsgBuf[256];
                            if(result.GetErrorMsg(errorMsgBuf, sizeof(errorMsgBuf)))
                            {
                                rlRockonError("Login failed [%s]: %s", errorCodeBuf, errorMsgBuf);
                            }
                            else
                            {
                                rlRockonError("Login failed [%s]: (Failed to read error msg)", errorCodeBuf);
                            }
                        }
                        else
                        {
                            rlRockonError("Login failed [%s]", errorCodeBuf);
                        }
                    }
                    else
                    {
                        rlRockonError("Login failed [could not read error code]");
                    }

                    this->Finish(FINISH_FAILED, 0);
                }
            }
            else if(!m_LoginRqst.Pending())
            {
                rlRockonError("Failed to login");
                this->Finish(FINISH_FAILED, 0);
            }
            break;
        }
    }
}

#if !ROCKON_SECURE_PLATFORM
bool
rlRockonLoginTask::ReadCryptoKeys(RsonReader& rr)
{
    rtry
    {
        //Read the encrypt and decrypt keys
        int dkeyLen = 0;
        int ekeyLen = 0;

        rcheck(-1 != (dkeyLen = rr.GetBinaryValueLength("DecryptKey")),
            catchall,
            rlRockonError("Failed to read decrypt key len"));

        rcheck(-1 != (ekeyLen = rr.GetBinaryValueLength("EncryptKey")),
            catchall,
            rlRockonError("Failed to read encrypt key len"));

        u8* dkey = Alloca(u8, dkeyLen);
        u8* ekey = Alloca(u8, ekeyLen);

        rcheck(dkey && ekey,
            catchall,
            rlRockonError("Failed to alloc decrypt and encrypt key buffers"));

        int len = -1;
        rcheck(rr.ReadBinary("DecryptKey", dkey, dkeyLen, &len),
            catchall,
            rlRockonError("Failed to read decrypt key"));

        rcheck(rr.ReadBinary("EncryptKey", ekey, ekeyLen, &len),
            catchall,
            rlRockonError("Failed to read encrypt key"));

        //These keys are encrypted using a shared secret. Decrypt them using it,
        //and store them on the client.
        u8* sharedSecret = 0;
        unsigned sharedSecretLen = 0;

#if __PPU
        u8 cookie[rlNp::TICKET_COOKIE_SIZE];
        sharedSecret = cookie;
        rcheck(g_rlNp.GetTicketCookie(sharedSecret, &sharedSecretLen, sizeof(cookie)),
            catchall,
            rlRockonError("Failed to get NP cookie"));
#endif

        rcheck(sharedSecret && sharedSecretLen,
            catchall,
            rlRockonError("Null shared secret; cannot decrypt crypto keys"));

        Rc4Context rc;
        rc.Reset(sharedSecret, sharedSecretLen);
        rc.Encrypt(dkey, dkeyLen);

        rc.Reset(sharedSecret, sharedSecretLen);
        rc.Encrypt(ekey, ekeyLen);

        m_Ctx->m_Decryptor.Reset(dkey, dkeyLen);
        m_Ctx->m_Encryptor.Reset(ekey, ekeyLen);

        return true;
    }
    rcatchall
    {
        return false;
    }
}
#endif

bool
rlRockonLoginTask::ConnectToServer()
{
    rlAssert(m_ServerAddr.IsValid());
    rlAssert(-1 == m_Ctx->m_Socket);

    rlRockonDebug2("Local gamer %d connecting to server:%d.%d.%d.%d:%d...",
        m_Ctx->GetLocalGamerIndex(),
        NET_ADDR_FOR_PRINTF(m_ServerAddr));

    int connectTimeoutSecs = DEFAULT_TCP_CONNECT_TIMEOUT_SECS;
    PARAM_rockontcpconnecttimeoutsecs.Get(connectTimeoutSecs);

    return netTcp::ConnectAsync(m_ServerAddr,
                                &m_Ctx->m_Socket,
                                connectTimeoutSecs,
                                &m_TcpOp);
}

bool
rlRockonLoginTask::SendLoginRequest()
{
    rlRockonDebug2("Local gamer %d sending login request..",
                    m_Ctx->GetLocalGamerIndex());

    rtry
    {
        RsonWriter rw;

        rw.InitNull();

        rverify(NativeWriteLoginMsg(rw),
                catchall,
                rlRockonError("Failed to write msg to calculate size"));

        const unsigned rqstLen = rw.Length() + 1;

        rlAssert(!m_RqstBuf);
        rverify(0 != (m_RqstBuf = (char*)RL_ALLOCATE(rlRockonLoginTask, rqstLen)),
                catchall,
                rlRockonError("Failed to allocate request buffer (%u chars)", rqstLen));

        rw.Init(m_RqstBuf, rqstLen);

        rverify(NativeWriteLoginMsg(rw),
                catchall,
                rlRockonError("Failed to write login message"));

        rverify(m_Ctx->SendRequest(m_RqstBuf, &m_LoginRqst),
                catchall,
                rlRockonError("Error sending login request"));

        return true;
    }
    rcatchall
    {
        if(m_RqstBuf)
        {
            rlFree(m_RqstBuf);
            m_RqstBuf = NULL;
        }

        return false;
    }
}

#if __LIVE

bool
rlRockonLoginTask::NativeWriteLoginMsg(RsonWriter& rw)
{
    rlGamerInfo gi;   
    if(rlVerify(rlGamerInfo::GetLocalGamerInfo(m_Ctx->GetLocalGamerIndex(), &gi)))
    {
        const rlGamerHandle& gh = gi.GetGamerHandle();

        return rw.Begin()
            && rw.WriteInt(ELEMENT_VERSION, ROCKON_CURRENT_VERSION)
            && rw.WriteInt(ELEMENT_TITLE_ID, g_rlTitleId->m_RockonTitleId.GetTitleId())
            && rw.WriteUns64(ELEMENT_XUID, gh.GetXuid())
            && rw.WriteString(ELEMENT_GAMERTAG, gi.GetName())
            && rw.End();
    }
    
    return false;
}

#elif __PPU

bool
rlRockonLoginTask::NativeWriteLoginMsg(RsonWriter& rw)
{
    rtry
    {
        char ticketBuf[SCE_NP_TICKET_MAX_SIZE]; //64K
        unsigned ticketSize = 0;

        rcheck(g_rlNp.GetTicket(ticketBuf, &ticketSize, sizeof(ticketBuf)),
            catchall,
            rlRockonError("Failed to get auth ticket"));

        return rw.Begin()
            && rw.WriteInt(ELEMENT_VERSION, ROCKON_CURRENT_VERSION)
            && rw.WriteInt(ELEMENT_TITLE_ID, g_rlTitleId->m_RockonTitleId.GetTitleId())
            && rw.WriteBinary(ELEMENT_TICKET, ticketBuf, ticketSize)
            && rw.End();
    }
    rcatchall
    {
        return false;
    }
}

#else

bool
rlRockonLoginTask::NativeWriteLoginMsg(RsonWriter& /*rw*/)
{
    return false;
}

#endif

}   //namespace rage
