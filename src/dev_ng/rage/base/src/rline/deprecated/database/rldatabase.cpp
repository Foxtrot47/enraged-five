// 
// rline/rldatabase.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rldatabase.h"
#include "rlgamerinfo.h"
#include "data/base64.h"
#include "data/bitbuffer.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "diag/tracker.h"
#include "system/memory.h"
#include "system/new.h"
#include "system/param.h"
#include "string/string.h"
#include <stdio.h>

#if RLDB_USE_SAKE
#include "gamespy\sake\sake.h"
#endif

using namespace rage;

namespace rage
{

//NOTE: Post-sort filter is huge because when reading stats it can include
//      the names of all your friends. (Up to the XBL max of 101 gamers.)
CompileTimeAssert(RLDB_MAX_POSTSORT_FILTER_CHARS >= (101 * RL_MAX_HANDLE_CHARS));

#if RLDB_USE_SAKE
//Externed so we don't have to include rlgamespysake.h, which includes a ton
//of stuff that's not required by rlDb.
extern bool g_rlGamespySakeUseManualRating;
#endif

RAGE_DEFINE_SUBCHANNEL(rline, db)

const char*
rlDbGetResultAsString(const int result)
{
#define RESULT_CASE(x) case x: return #x;

    switch(result)
    {
        RESULT_CASE(RLDB_RESULT_NONE);
        RESULT_CASE(RLDB_RESULT_SUCCESS);
        RESULT_CASE(RLDB_RESULT_CANCELLED);
        RESULT_CASE(RLDB_RESULT_TIMED_OUT);
        RESULT_CASE(RLDB_RESULT_OUT_OF_MEMORY);
        RESULT_CASE(RLDB_RESULT_UNKNOWN_ERROR);   
        RESULT_CASE(RLDB_RESULT_SOCKET_ERROR);
        RESULT_CASE(RLDB_RESULT_AUTHENTICATION_FAILED);
        RESULT_CASE(RLDB_RESULT_NETWORK_ERROR);
        RESULT_CASE(RLDB_RESULT_INVALID_ARGUMENTS);
        RESULT_CASE(RLDB_RESULT_NO_PERMISSION);
        RESULT_CASE(RLDB_RESULT_RECORD_LIMIT_REACHED);
        RESULT_CASE(RLDB_RESULT_ALREADY_RATED);
        RESULT_CASE(RLDB_RESULT_NOT_RATEABLE);
        RESULT_CASE(RLDB_RESULT_NOT_OWNED);
        RESULT_CASE(RLDB_RESULT_FILE_NOT_FOUND);
        RESULT_CASE(RLDB_RESULT_FILE_TOO_LARGE);
        RESULT_CASE(RLDB_RESULT_BUFFER_OVERFLOW);
        RESULT_CASE(RLDB_RESULT_DNS_ERROR);
        RESULT_CASE(RLDB_RESULT_GAMESPY_HTTP_ERROR);
        RESULT_CASE(RLDB_RESULT_GAMESPY_CONNECTION_ERROR);
        RESULT_CASE(RLDB_RESULT_GAMESPY_START_ERROR);

    default:
        return "UNKNOWN RESULT";
    };

#undef RESULT_CASE
}

const char*
rlDbGetFieldName(const int fieldId)
{
    rlAssert((fieldId >=0) && (fieldId < RLDB_NUM_FIELDS));

    switch(fieldId)
    {
    case RLDB_FIELD_RECORD_ID:      return "recordid";
    case RLDB_FIELD_OWNER_ID:       return "ownerid";
    case RLDB_FIELD_GAMER_NAME:     return "rs_GamerName";    
    case RLDB_FIELD_GAMER_HANDLE:   return "rs_GamerHandle";
    case RLDB_FIELD_RANK:           return "row";
    case RLDB_FIELD_NUM_RATINGS:     
#if RLDB_USE_SAKE
        return g_rlGamespySakeUseManualRating ? "rs_NumRatings" : "num_ratings";
#else
        return "rs_NumRatings";
#endif

    case RLDB_FIELD_AVG_RATING:      
#if RLDB_USE_SAKE
        return g_rlGamespySakeUseManualRating ? "rs_AvgRating" : "average_rating";
#else
        return "rs_AvgRating";
#endif

    default:                    
        return "[UNKNOWN]";
    }
}

const char* 
rlDbValueTypeToString(const rlDbValueType t)
{
    static const char* s_VtStrs[RLDB_NUM_VALUETYPES] =
    {
        "bool",     //RLDB_VALUETYPE_BOOL
        "uns8",     //RLDB_VALUETYPE_UNS8
        "int16",    //RLDB_VALUETYPE_INT16
        "int32",    //RLDB_VALUETYPE_INT32
        "int64",    //RLDB_VALUETYPE_INT64
        "float",    //RLDB_VALUETYPE_FLOAT
        "string",   //RLDB_VALUETYPE_ASCII_STRING
        "binary"    //RLDB_VALUETYPE_BINARY_DATA
    };

    const char* vtStr = 0;

    if(t >=0 && t < RLDB_NUM_VALUETYPES)
    {
        vtStr = s_VtStrs[t];
    }

    if(vtStr)
    {
        rlAssert(strlen(vtStr) < RLDB_MAX_VALUE_TYPE_STR_CHARS);
    }

    return vtStr;
}

rlDbValueType
rlDbValueTypeFromString(const char* typeName)
{
#define VT_CHECK(vt) { const char* vtStr = rlDbValueTypeToString(vt);  if(vtStr && !strcmp(typeName, vtStr)) return vt; }

    VT_CHECK(RLDB_VALUETYPE_BOOL);
    VT_CHECK(RLDB_VALUETYPE_UNS8);
    VT_CHECK(RLDB_VALUETYPE_INT16);
    VT_CHECK(RLDB_VALUETYPE_INT32);
    VT_CHECK(RLDB_VALUETYPE_INT64);
    VT_CHECK(RLDB_VALUETYPE_FLOAT);
    VT_CHECK(RLDB_VALUETYPE_ASCII_STRING);
    VT_CHECK(RLDB_VALUETYPE_BINARY_DATA);

    return RLDB_VALUETYPE_INVALID;
}

} //namespace rage

//-----------------------------------------------------------------------------
// rlDbBinaryData
//-----------------------------------------------------------------------------
rlDbBinaryData::rlDbBinaryData()
: m_Data(0)
, m_Size(0)
, m_OwnData(false)
{
}

rlDbBinaryData::~rlDbBinaryData()
{
    Clear();
}

void 
rlDbBinaryData::Clear()
{
    if(m_OwnData)
    {
        rlFree(m_Data);
    }

    m_Data = 0;
    m_Size = 0;
    m_OwnData = false;
}

bool 
rlDbBinaryData::Reset(const u8* data, 
                      const unsigned size,
                      const eMode mode)
{
    Clear();

    m_Size = size;

    if(MODE_COPY_PTR_ONLY == mode)
    {
        m_Data = (u8*)data;
    }
    else
    {   
        rlAssert(MODE_COPY_DATA == mode);

        if(0 == (m_Data = (u8*)RL_ALLOCATE(rlDbBinaryData, size)))
        {
            return false;
        }

        m_OwnData = true;

        sysMemCpy(m_Data, data, size);
    }

    return true;
}

const u8* 
rlDbBinaryData::GetData() const 
{ 
    return m_Data; 
}

unsigned 
rlDbBinaryData::GetSize() const 
{
    return m_Size; 
}

bool
rlDbBinaryData::Export(datExportBuffer& bb) const
{
    rtry
    {
        rcheck(bb.WriteUns(m_Size, 32),
            catchall,
            rlDbError("Failed to write binary data size"));

        if(m_Size)
        {
            rcheck(bb.WriteBytes(m_Data, m_Size),
                catchall,
                rlDbError("Failed to write binary data"));
        }
    }
    rcatchall
    {
        return false;
    }

    return true;
}

bool
rlDbBinaryData::Import(const datImportBuffer& bb)
{
    Clear();

    rtry
    {
        rcheck(bb.ReadUns(m_Size, 32),
            catchall,
            rlDbError("Failed to read binary data size"));

        if(m_Size)
        {
            rcheck(0 != (m_Data = (u8*)RL_ALLOCATE(rlDbBinaryData, m_Size)),
                catchall,
                rlDbError("Failed to allocate binary data during import"));

            m_OwnData = true;

            rcheck(bb.ReadBytes(m_Data, m_Size),
                catchall,
                rlDbError("Failed to read binary data"));
        }
    }
    rcatchall
    {
        Clear();
        return false;
    }

    return true;
}

//-----------------------------------------------------------------------------
// rlDbValue
//-----------------------------------------------------------------------------
rlDbValue::rlDbValue()
: m_Type(RLDB_VALUETYPE_INVALID)
{
}

rlDbValue::~rlDbValue()
{
    Clear();
}

rlDbValue::rlDbValue(const rlDbValue& rhs)
{
    *this = rhs;
}

rlDbValue& 
rlDbValue::operator=(const rlDbValue& rhs)
{
    Clear();

    switch(rhs.GetType())
    {
    case RLDB_VALUETYPE_BOOL:         
        SetBool(rhs.GetBool()); 
        break;
    case RLDB_VALUETYPE_UNS8:         
        SetUns8(rhs.GetUns8()); 
        break;
    case RLDB_VALUETYPE_INT16:        
        SetInt16(rhs.GetInt16()); 
        break;
    case RLDB_VALUETYPE_INT32:        
        SetInt32(rhs.GetInt32()); 
        break;
    case RLDB_VALUETYPE_INT64:        
        SetInt64(rhs.GetInt64()); 
        break;
    case RLDB_VALUETYPE_FLOAT:        
        SetFloat(rhs.GetFloat()); 
        break;
    case RLDB_VALUETYPE_ASCII_STRING: 
        SetAsciiString(rhs.GetAsciiString()); 
        break;
    case RLDB_VALUETYPE_BINARY_DATA:  
        SetBinaryData(rhs.GetBinaryData().GetData(),
            rhs.GetBinaryData().GetSize(),
            rlDbBinaryData::MODE_COPY_DATA);
        break;
    default:
        break;
    };

    return *this;
}

rlDbValueType 
rlDbValue::GetType() const 
{ 
    return m_Type; 
}

void 
rlDbValue::Clear()
{
    switch(m_Type)
    {
    case RLDB_VALUETYPE_ASCII_STRING:
        if(m_Union.m_AsciiString)
        {
            rlFree(m_Union.m_AsciiString);
            m_Union.m_AsciiString = 0;
        }
        break;

    case RLDB_VALUETYPE_BINARY_DATA:
        m_BinaryData.Clear();
        break;

    default:
        break;
    }

    m_Type = RLDB_VALUETYPE_INVALID;
}

bool 
rlDbValue::GetBool() const 
{ 
    return m_Union.m_Bool; 
}

u8 
rlDbValue::GetUns8() const 
{ 
    return m_Union.m_Uns8; 
}

s16 
rlDbValue::GetInt16() const 
{ 
    return m_Union.m_Int16; 
}

s32 
rlDbValue::GetInt32() const 
{ 
    return m_Union.m_Int32; 
}

s64 
rlDbValue::GetInt64() const 
{ 
    return m_Union.m_Int64; 
}

float 
rlDbValue::GetFloat() const 
{ 
    return m_Union.m_Float; 
}

const char* 
rlDbValue::GetAsciiString() const 
{ 
    return m_Union.m_AsciiString; 
}

const rlDbBinaryData& 
rlDbValue::GetBinaryData() const 
{ 
    return m_BinaryData; 
}

void 
rlDbValue::SetBool(const bool val) 
{ 
    Clear(); 
    m_Type = RLDB_VALUETYPE_BOOL;
    m_Union.m_Bool = val; 
}

void 
rlDbValue::SetUns8(const u8 val) 
{ 
    Clear(); 
    m_Type = RLDB_VALUETYPE_UNS8;
    m_Union.m_Uns8 = val; 
}

void 
rlDbValue::SetInt16(const s16 val) 
{ 
    Clear(); 
    m_Type = RLDB_VALUETYPE_INT16;
    m_Union.m_Int16 = val; 
}

void 
rlDbValue::SetInt32(const s32 val) 
{ 
    Clear(); 
    m_Type = RLDB_VALUETYPE_INT32;
    m_Union.m_Int32 = val; 
}

void 
rlDbValue::SetInt64(const s64 val) 
{ 
    Clear(); 
    m_Type = RLDB_VALUETYPE_INT64;
    m_Union.m_Int64 = val; 
}

void 
rlDbValue::SetFloat(const float val) 
{ 
    Clear(); 
    m_Type = RLDB_VALUETYPE_FLOAT;
    m_Union.m_Float = val; 
}

bool 
rlDbValue::SetAsciiString(const char* str)
{
    Clear();

    unsigned size = strlen(str) + 1;

    if(0 == (m_Union.m_AsciiString = (char*)RL_ALLOCATE(rlDbValue, size)))
    {
        return false;
    }

    safecpy(m_Union.m_AsciiString, str, size);
    m_Type = RLDB_VALUETYPE_ASCII_STRING;

    return true;
}

bool 
rlDbValue::SetBinaryData(const u8* data,
                         const unsigned size,
                         const rlDbBinaryData::eMode mode)
{
    Clear();

    if(!m_BinaryData.Reset(data, size, mode))
    {
        return false;
    }

    m_Type = RLDB_VALUETYPE_BINARY_DATA;

    return true;
}

const char* 
rlDbValue::ToString(char* buf, const unsigned bufSize) const
{
    rlAssert(buf && bufSize);

    buf[0] = '\0';

    switch(m_Type)
    {
    case RLDB_VALUETYPE_INVALID:
        safecpy(buf, "INVALID", bufSize);
        break;
    case RLDB_VALUETYPE_BOOL:
        safecpy(buf, GetBool() ? "true" : "false", bufSize);
        break;
    case RLDB_VALUETYPE_UNS8:
        formatf(buf, bufSize, "%u", GetUns8());
        break;
    case RLDB_VALUETYPE_INT16:
        formatf(buf, bufSize, "%d", GetInt16());
        break;
    case RLDB_VALUETYPE_INT32:
        formatf(buf, bufSize, "%d", GetInt32());
        break;
    case RLDB_VALUETYPE_INT64:
        formatf(buf, bufSize, "0x%"I64FMT"x", GetInt64());
        break;
    case RLDB_VALUETYPE_FLOAT:
        formatf(buf, bufSize, "%f", GetFloat());
        break;
    case RLDB_VALUETYPE_ASCII_STRING:
        safecpy(buf, GetAsciiString(), bufSize);
        break;
    case RLDB_VALUETYPE_BINARY_DATA:
        {
            const rlDbBinaryData& bd = GetBinaryData();
            unsigned size = 0;

            if(!datBase64::Encode(bd.GetData(), bd.GetSize(), bufSize, buf, &size))
            {
                buf[0] = '\0';
            }
        }
        break;
    default:
        rlDbError("Unhandled or invalid value type (%d)", m_Type);
    }

    return buf;
}

bool 
rlDbValue::FromString(const char* str, const rlDbValueType asType)
{
    switch(asType)
    {
    case RLDB_VALUETYPE_BOOL:
        {
            if(!stricmp("true", str))
            {
                SetBool(true);
            }
            else if(!stricmp("false", str))
            {
                SetBool(false);
            }
            else
            {
                return false;
            }
        }
        break;
    case RLDB_VALUETYPE_UNS8:
        {
            int val;
            if(sscanf(str, "%d", &val) != 1) return false;
            SetUns8((u8)val);
        }
        break;
    case RLDB_VALUETYPE_INT16:
        {
            int val;
            if(sscanf(str, "%d", &val) != 1) return false;
            SetInt16((s16)val);
        }
        break;
    case RLDB_VALUETYPE_INT32:
        {
            int val;
            if(sscanf(str, "%d", &val) != 1) return false;
            SetInt32((s32)val);
        }
        break;
    case RLDB_VALUETYPE_INT64:
        {
            s64 val;
            if((strlen(str) > 2) && '0' == str[0] && 'x' == str[1])
            {
                if(sscanf(str, "0x%" I64FMT "x", &val) != 1) return false;
            }
            else
            {
                if(sscanf(str, "%" I64FMT "u", &val) != 1) return false;
            }
            SetInt64(val);
        }
        break;
    case RLDB_VALUETYPE_FLOAT:
        {
            float val;
            if(sscanf(str, "%f", &val) != 1) return false;
            SetFloat(val);
        }
        break;
    case RLDB_VALUETYPE_ASCII_STRING:
        {
            return SetAsciiString(str);
        }
    case RLDB_VALUETYPE_BINARY_DATA:
        {
            unsigned dataMaxSize = datBase64::GetMaxDecodedSize(str);
            if(!dataMaxSize)
            {
                rlDbError("Failed to compute max size of binary data");
                return false;
            }

            u8* data = (u8*)RL_ALLOCATE("rkDbValue::FromString", dataMaxSize);
            if(!data)
            {
                rlDbError("Failed to allocate binary data buffer");
                return false;
            }

            unsigned dataSize = 0;

            if(!datBase64::Decode(str, dataMaxSize, data, &dataSize))
            {
                rlFree(data);
                return false;
            }

            bool success = SetBinaryData(data, dataSize, rlDbBinaryData::MODE_COPY_DATA);
            rlFree(data);
            return success;
        }
    default:
        return false;
    }

    return true;
}

const char* 
rlDbValue::TypeToString() const
{
    return rlDbValueTypeToString(m_Type);
}

bool
rlDbValue::Export(datExportBuffer& bb) const
{
    rtry
    {
        rcheck(bb.WriteUns(m_Type, datBitsNeeded<RLDB_NUM_VALUETYPES>::COUNT + 1),
            catchall,
            rlDbError("Failed to write value type"));

        switch(m_Type)
        {
        case RLDB_VALUETYPE_BOOL:
            rcheck(bb.WriteBool(GetBool()),
                   catchall,
                   rlDbError("Failed to write bool"));
            break;
        case RLDB_VALUETYPE_UNS8:
            rcheck(bb.WriteUns(GetUns8(), 8),
                   catchall,
                   rlDbError("Failed to write uns8"));
            break;
        case RLDB_VALUETYPE_INT16:
            rcheck(bb.WriteInt(GetInt16(), 16),
                   catchall,
                   rlDbError("Failed to write int16"));
            break;
        case RLDB_VALUETYPE_INT32:
            rcheck(bb.WriteInt(GetInt32(), 32),
                   catchall,
                   rlDbError("Failed to write int32"));
            break;
        case RLDB_VALUETYPE_INT64:
            rcheck(bb.WriteInt(GetInt64(), 64),
                   catchall,
                   rlDbError("Failed to write int64"));
            break;
        case RLDB_VALUETYPE_FLOAT:
            rcheck(bb.WriteFloat(GetFloat()),
                   catchall,
                   rlDbError("Failed to write float"));
            break;
        case RLDB_VALUETYPE_ASCII_STRING:
            {
                unsigned size = strlen(m_Union.m_AsciiString) + 1;

                rcheck(bb.WriteUns(size, 32),
                       catchall,
                       rlDbError("Failed to write ascii string size"));

                if(size)
                {
                    rcheck(bb.WriteBytes(m_Union.m_AsciiString, size),
                           catchall,
                           rlDbError("Failed to write ascii string"));
                }
            }
            break;
        case RLDB_VALUETYPE_BINARY_DATA:            
            rcheck(m_BinaryData.Export(bb),
                   catchall,
                   rlDbError("Failed to write binary data"));
            break;

        case RLDB_VALUETYPE_INVALID:
        default:
            rcheck(false,
                   catchall,
                   rlDbError("Unknown value type"));
        }
    }
    rcatchall
    {
        return false;
    }

    return true;
}

bool
rlDbValue::Import(const datImportBuffer& bb)
{
    Clear();

    rtry
    {
        rcheck(bb.ReadUns(m_Type, datBitsNeeded<RLDB_NUM_VALUETYPES>::COUNT + 1),
            catchall,
            rlDbError("Failed to write value type"));

        switch(m_Type)
        {
        case RLDB_VALUETYPE_BOOL:
            rcheck(bb.ReadBool(m_Union.m_Bool),
                   catchall,
                   rlDbError("Failed to read bool"));
            break;
        case RLDB_VALUETYPE_UNS8:
            rcheck(bb.ReadUns(m_Union.m_Uns8, 8),
                   catchall,
                   rlDbError("Failed to read uns8"));
            break;
        case RLDB_VALUETYPE_INT16:
            rcheck(bb.ReadInt(m_Union.m_Int16, 16),
                   catchall,
                   rlDbError("Failed to read int16"));
            break;
        case RLDB_VALUETYPE_INT32:
            rcheck(bb.ReadInt(m_Union.m_Int32, 32),
                   catchall,
                   rlDbError("Failed to read int32"));
            break;
        case RLDB_VALUETYPE_INT64:
            rcheck(bb.ReadInt(m_Union.m_Int64, 64),
                   catchall,
                   rlDbError("Failed to read int64"));
            break;
        case RLDB_VALUETYPE_FLOAT:
            rcheck(bb.ReadFloat(m_Union.m_Float),
                   catchall,
                   rlDbError("Failed to read float"));
            break;
        case RLDB_VALUETYPE_ASCII_STRING:
            {
                unsigned size = 0;
                rcheck(bb.ReadUns(size, 32),
                       catchall,
                       rlDbError("Failed to read ascii string size"));

                if(size)
                {
                    rcheck(0 != (m_Union.m_AsciiString = (char*)RL_ALLOCATE(rlDbValue, size)),
                           catchall,
                           rlDbError("Failed to allocate ascii string"));

                    rcheck(bb.ReadBytes(m_Union.m_AsciiString, size),
                           catchall,
                           rlDbError("Failed to read ascii string"));
                }
            }
            break;
        case RLDB_VALUETYPE_BINARY_DATA:            
            rcheck(m_BinaryData.Import(bb),
                   catchall,
                   rlDbError("Failed to read binary data"));
            break;

        case RLDB_VALUETYPE_INVALID:
        default:
            rcheck(false,
                   catchall,
                   rlDbError("Unknown value type"));
        }
    }
    rcatchall
    {
        Clear();
        return false;
    }

    return true;
}

//-----------------------------------------------------------------------------
// rlDbField
//-----------------------------------------------------------------------------
rlDbField::rlDbField()
{
}

rlDbField::~rlDbField()
{
}

void 
rlDbField::Clear()
{
    sysMemSet(m_Name, 0, sizeof(m_Name));
    m_Value.Clear();
}

const char* 
rlDbField::GetName() const
{
    return m_Name;
}

void 
rlDbField::SetName(const char* name)
{
    rlAssert(name && (name[0] != '\0') &&(strlen(name) < RLDB_MAX_FIELD_NAME_CHARS));
    safecpy(m_Name, name, sizeof(m_Name));
}

const rlDbValue& 
rlDbField::GetValue() const
{
    return m_Value;
}

rlDbValue& 
rlDbField::GetValue()
{
    return m_Value;
}

bool
rlDbField::Export(datExportBuffer& bb) const
{
    return bb.WriteStr(m_Name, sizeof(m_Name)) && m_Value.Export(bb);
}

bool
rlDbField::Import(const datImportBuffer& bb)
{
    Clear();
    return bb.ReadStr(m_Name, sizeof(m_Name)) && m_Value.Import(bb);
}

//-----------------------------------------------------------------------------
// rlDbSchema
//-----------------------------------------------------------------------------
rlDbSchemaBase::rlDbSchemaBase()
: m_NumFields(0)
{
}

rlDbSchemaBase::~rlDbSchemaBase()
{
    Clear();
}

void 
rlDbSchemaBase::Clear()
{
    m_NumFields = 0;
}

bool 
rlDbSchemaBase::AddField(const char* name, const rlDbValueType vt)
{
    if(!rlVerify(name && strlen(name) && (vt != RLDB_VALUETYPE_INVALID)))
    {
        return false;
    }

    if(rlVerify(m_NumFields < GetMaxFields()))
    {
        if(CopyFieldNames())
        {
            safecpy((char*)GetFieldNames()[m_NumFields], name, RLDB_MAX_FIELD_NAME_CHARS);
        }
        else
        {
            GetFieldNames()[m_NumFields] = name;
        }
        
        (rlDbValueType&)GetFieldTypes()[m_NumFields] = vt;

        ++m_NumFields;
        return true;
    }
    return false;
}

unsigned 
rlDbSchemaBase::GetNumFields() const
{
    return m_NumFields;
}

const char* 
rlDbSchemaBase::GetFieldName(const unsigned index) const
{
    if(m_NumFields)
    {
        return GetFieldNames()[index];
    }
    return 0;
}

rlDbValueType 
rlDbSchemaBase::GetFieldType(const unsigned index) const
{
    if(m_NumFields)
    {
        return GetFieldTypes()[index];
    }
    return RLDB_VALUETYPE_INVALID;
}

int 
rlDbSchemaBase::GetFieldIndex(const char* name) const
{
    for(unsigned i = 0; i < m_NumFields; i++)
    {
        if(!strcmp(name, GetFieldName(i)))
        {
            return i;
        }
    }
    return -1;
}

bool 
rlDbSchemaBase::Export(datExportBuffer& bb) const
{
    rtry
    {
        rcheck(bb.WriteUns(m_NumFields, datBitsNeeded<RLDB_MAX_FIELDS_PER_TABLE>::COUNT),
            catchall,
            rlDbError("Failed to write num fields"));

        for(unsigned i = 0; i < m_NumFields; i++)
        {
            rcheck(bb.WriteStr(GetFieldName(i), RLDB_MAX_FIELD_NAME_CHARS),
                catchall,
                rlDbError("Failed to write name for field %u", i));

            rcheck(bb.WriteInt(GetFieldType(i), datBitsNeeded<RLDB_NUM_VALUETYPES>::COUNT + 1),
                catchall,
                rlDbError("Failed to write type for field %u", i));
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

//-----------------------------------------------------------------------------
// rlDbDynSchema
//-----------------------------------------------------------------------------
rlDbDynSchema::rlDbDynSchema()
: m_MaxFields(0)
, m_FieldNames(0)
, m_FieldTypes(0)
, m_FieldNamesBuf(0)
{
}

rlDbDynSchema::~rlDbDynSchema()
{
    Clear();
}

bool 
rlDbDynSchema::Reset(const unsigned maxFields, const eMode mode)
{
    rlAssert(maxFields);

    rtry
    {
        Clear();

        m_MaxFields = maxFields;

        rcheck(0 != (m_FieldNames = (const char**)RL_ALLOCATE(rlDbDynSchema, sizeof(char*) * m_MaxFields)),
            catchall,
            rlDbError("Failed to allocate pointers for %u field names", m_MaxFields));

        rcheck(0 != (m_FieldTypes = (rlDbValueType*)RL_ALLOCATE(rlDbDynSchema, sizeof(rlDbValueType) * m_MaxFields)),
            catchall,
            rlDbError("Failed to allocate value types for %u fields", m_MaxFields));

        if(mode == COPY_NAME_STRING)
        {
            unsigned bufSize = RLDB_MAX_FIELD_NAME_CHARS * m_MaxFields;

            rcheck(0 != (m_FieldNamesBuf = (char*)RL_ALLOCATE(rlDbDynSchema, bufSize)),
                catchall,
                rlDbError("Failed to allocate field names buffer"));
            
            sysMemSet(m_FieldNamesBuf, 0, bufSize);

            for(unsigned i = 0; i < m_MaxFields; i++)
            {
                m_FieldNames[i] = (char*)&m_FieldNamesBuf[i * RLDB_MAX_FIELD_NAME_CHARS];
            }
        }

        return true;
    }
    rcatchall
    {
        Clear();
        return false;
    }
}

unsigned 
rlDbDynSchema::GetMaxFields() const 
{ 
    return m_MaxFields; 
}

const char** 
rlDbDynSchema::GetFieldNames() const 
{ 
    return m_FieldNames; 
}

const rlDbValueType* 
rlDbDynSchema::GetFieldTypes() const 
{ 
    return m_FieldTypes; 
}

bool 
rlDbDynSchema::Import(const datImportBuffer& bb)
{
    rtry
    {
        unsigned numFields = 0;
        rcheck(bb.ReadUns(numFields, datBitsNeeded<RLDB_MAX_FIELDS_PER_TABLE>::COUNT),
            catchall,
            rlDbError("Failed to read num fields"));

        rcheck(Reset(numFields, COPY_NAME_STRING), catchall, rlDbError("Failed to Reset for %u fields", numFields));

        rcheck(0 != (m_FieldNamesBuf = (char*)RL_ALLOCATE(rlDbDynSchema, RLDB_MAX_FIELD_NAME_CHARS * m_MaxFields)),
            catchall,
            rlDbError("Failed to allocate field names buffer"));

        for(unsigned i = 0; i < numFields; i++)
        {
            char name[RLDB_MAX_FIELD_NAME_CHARS];
            //char* name = (char*)&m_FieldNamesBuf[i * RLDB_MAX_FIELD_NAME_CHARS];

            rcheck(bb.ReadStr(name, sizeof(name)), //RLDB_MAX_FIELD_NAME_CHARS),
                catchall,
                rlDbError("Failed to read name for field %u", i));

            rlDbValueType vt;
            rcheck(bb.ReadInt(vt, datBitsNeeded<RLDB_NUM_VALUETYPES>::COUNT + 1),
                catchall,
                rlDbError("Failed to read type for field %u", i));

            AddField(name, vt);
        }

        return true;
    }
    rcatchall
    {
        Clear();
        return false;
    }
}

bool 
rlDbDynSchema::CopyFieldNames() const
{
     return m_FieldNamesBuf != 0;
}

void 
rlDbDynSchema::Clear()
{
    rlDbSchemaBase::Clear();

    if(m_FieldNames) 
    {
        rlFree(m_FieldNames);
        m_FieldNames = 0;
    }

    if(m_FieldTypes) 
    {
        rlFree(m_FieldTypes);
        m_FieldTypes = 0;
    }

    if(m_FieldNamesBuf)
    {
        rlFree(m_FieldNamesBuf);
        m_FieldNamesBuf = 0;
    }

    m_MaxFields = 0;
}

//-----------------------------------------------------------------------------
// rlDbReadResults
//-----------------------------------------------------------------------------
rlDbReadResults::rlDbReadResults()
: m_MaxRecords(0)
, m_NumRecords(0)
, m_Values(0)
{
}

rlDbReadResults::~rlDbReadResults()
{
}

void
rlDbReadResults::Reset(const rlDbSchemaBase* schema,
                       const unsigned maxRecords,
                       rlDbValue* values)
{
    m_Schema = schema;
    m_MaxRecords = maxRecords;
    m_Values = values;
    m_NumRecords = 0;
}

const rlDbSchemaBase*
rlDbReadResults::GetSchema() const
{
    return m_Schema;
}

unsigned 
rlDbReadResults::GetMaxRecords() const     
{ 
    return m_MaxRecords; 
}

unsigned 
rlDbReadResults::GetNumRecords() const     
{ 
    return m_NumRecords; 
}

const rlDbValue& 
rlDbReadResults::GetValue(const unsigned row,
                          const unsigned column) const
{
    rlAssert(column < m_Schema->GetNumFields());
    rlAssert(m_Values);

    return m_Values[(row * m_Schema->GetNumFields()) + column];
}

rlDbValue& 
rlDbReadResults::GetValue(const unsigned row,
                          const unsigned column)
{
    rlAssert(column < m_Schema->GetNumFields());
    rlAssert(m_Values);

    return m_Values[(row * m_Schema->GetNumFields()) + column];
}

void 
rlDbReadResults::SetNumRecords(const unsigned num) 
{
    rlAssert(num <= m_MaxRecords);
    m_NumRecords = num;
}

bool
rlDbReadResults::Export(datExportBuffer& bb) const
{
    //When exporting, the receiver already knows the schema for the results.
    //All we need to write is the number of records read, and the values (w/o
    //type information, which the receiver already knows).

    rtry
    {
        //FIXME: Redefine rlError et al to write to rline_db, so no need for rlDbXX macros
        rcheck(bb.WriteUns(m_NumRecords, 32),
               catchall,
               rlDbError("Failed to write read results header"));

        for(unsigned row = 0; row < m_NumRecords; row++)
        {
            for(unsigned column = 0; column < m_Schema->GetNumFields(); column++)
            {
                const rlDbValue& val = GetValue(row, column);
                
                rcheck(val.GetType() == m_Schema->GetFieldType(column),
                       catchall,
                       rlDbError("Value [%u, %u] type (%d) does not match schema (%d)",
                                 row, column, val.GetType(), m_Schema->GetFieldType(column)));

                rcheck(val.Export(bb),
                       catchall,
                       rlDbError("Failed to export value [%d, %d]", row, column));
            }
        }
    }
    rcatchall
    {
        return false;
    }

    return true;
}

bool
rlDbReadResults::Import(const datImportBuffer& bb)
{
    //When importing, we use our current schema to guide the reading, and our
    //current values array to guide the writing.  So this isn't a true import
    //that completely initializes the whole object, but it works for our 
    //purposes, and avoids unnecessary dynamic allocation (unnecessary because
    //when reading you should already know the schema you're reading for, and
    //the maximum number of rows to read).

    rtry
    {
        rcheck(bb.ReadUns(m_NumRecords, 32),
               catchall,
               rlDbError("Failed to read number of records"));

        rcheck(m_NumRecords <= m_MaxRecords,
               catchall,
               rlDbError("More records than allowed (%u, max is %u)", m_NumRecords, m_MaxRecords));

        for(unsigned row = 0; row < m_NumRecords; row++)
        {
            for(unsigned column = 0; column < m_Schema->GetNumFields(); column++)
            {
                rlDbValue& val = GetValue(row, column);

                rcheck(val.Import(bb),
                    catchall,
                    rlDbError("Failed to export value [%d, %d]", row, column));
            }
        }
    }
    rcatchall
    {
        m_NumRecords = 0;
        return false;
    }

    return true;
}

const char* 
rlDbReadResults::GetFieldName(const unsigned column) const
{
    return m_Schema->GetFieldName(column);
}

rlDbValueType 
rlDbReadResults::GetFieldType(const unsigned column) const
{
    return m_Schema->GetFieldType(column);
}

int 
rlDbReadResults::GetRowIndexContaining(const char* fieldName, 
                                       const char* value,
                                       const unsigned startIndex) const
{
    rlAssert(fieldName);
    rlAssert(value);

    int col = m_Schema->GetFieldIndex(fieldName);

    if(rlVerify(col != -1))
    {
        for(unsigned i = startIndex; i < m_NumRecords; i++)
        {
            if(!strcmp(GetAsciiString(i, col), value))
            {
                return i;
            }
        }
    }
    
    return -1;
}

bool 
rlDbReadResults::GetBoolByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetBool();
}

u8 
rlDbReadResults::GetUns8ByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetUns8();
}

s16 
rlDbReadResults::GetInt16ByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetInt16();
}

s32 
rlDbReadResults::GetInt32ByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetInt32();
}

s64 
rlDbReadResults::GetInt64ByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetInt64();
}

float 
rlDbReadResults::GetFloatByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetFloat();
}

const char* 
rlDbReadResults::GetAsciiStringByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetAsciiString();
}

const rlDbBinaryData& 
rlDbReadResults::GetBinaryDataByName(const unsigned row, const char* name) const
{
    return GetValue(row, m_Schema->GetFieldIndex(name)).GetBinaryData();
}

bool 
rlDbReadResults::GetBool(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_BOOL);
    return GetValue(row, column).GetBool();
}

u8 
rlDbReadResults::GetUns8(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_UNS8);
    return GetValue(row, column).GetUns8();
}

s16 
rlDbReadResults::GetInt16(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_INT16);
    return GetValue(row, column).GetInt16();
}

s32 
rlDbReadResults::GetInt32(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_INT32);
    return GetValue(row, column).GetInt32();
}

s64 
rlDbReadResults::GetInt64(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_INT64);
    return GetValue(row, column).GetInt64();
}

float 
rlDbReadResults::GetFloat(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_FLOAT);
    return GetValue(row, column).GetFloat();
}

const char* 
rlDbReadResults::GetAsciiString(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_ASCII_STRING);
    return GetValue(row, column).GetAsciiString();
}

const rlDbBinaryData& 
rlDbReadResults::GetBinaryData(const unsigned row, const unsigned column) const
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_BINARY_DATA);
    return GetValue(row, column).GetBinaryData();
}

void 
rlDbReadResults::SetBool(const unsigned row, const unsigned column, const bool val)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_BOOL);
    return GetValue(row, column).SetBool(val);
}

void 
rlDbReadResults::SetUns8(const unsigned row, const unsigned column, const u8 val)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_UNS8);
    return GetValue(row, column).SetUns8(val);
}

void 
rlDbReadResults::SetInt16(const unsigned row, const unsigned column, const s16 val)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_INT16);
    return GetValue(row, column).SetInt16(val);
}

void 
rlDbReadResults::SetInt32(const unsigned row, const unsigned column, const s32 val)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_INT32);
    return GetValue(row, column).SetInt32(val);
}

void 
rlDbReadResults::SetInt64(const unsigned row, const unsigned column, const s64 val)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_INT64);
    return GetValue(row, column).SetInt64(val);
}

void 
rlDbReadResults::SetFloat(const unsigned row, const unsigned column, const float val)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_FLOAT);
    return GetValue(row, column).SetFloat(val);
}

bool 
rlDbReadResults::SetAsciiString(const unsigned row, const unsigned column, const char* str)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_ASCII_STRING);
    return GetValue(row, column).SetAsciiString(str);
}

bool 
rlDbReadResults::SetBinaryData(
                   const unsigned row, const unsigned column, 
                   const u8* data,
                   const unsigned size,
                   const rlDbBinaryData::eMode owner)
{
    rlAssert(GetFieldType(column) == RLDB_VALUETYPE_BINARY_DATA);
    return GetValue(row, column).SetBinaryData(data, size, owner);
}

const char* 
rlDbReadResults::ToString(const unsigned row, 
                          const unsigned column,
                          char* buf,
                          const unsigned bufSize) const
{
    return GetValue(row, column).ToString(buf, bufSize);
}
