// 
// rline/rldatabaseclient.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rldatabaseclient.h"
#include "diag/seh.h"
#include "diag/tracker.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

#if __GAMESPY
#include "rlgamespy.h"
#endif

#if __LIVE
#include "rlxlsp.h"
#endif

namespace rage
{

#define CREATETASK(taskname, ...) \
    rlAssert(m_Initialized); \
    if(!IsServiceAvailable()) \
    { \
        rlDbDebug2("rlDb"#taskname"Task could not be started, service not available atm"); \
        return false; \
    } \
    rlDb##taskname##Task* task = RL_NEW(rlDbClient, rlDb##taskname##Task); \
    if(!task) { rlDbError("Failed to allocate rlDb"#taskname"Task"); return false; } \
    if(!rlTaskBase::Configure(task, __VA_ARGS__)) \
    { \
        rlDbError("Failed to configure rlDb"#taskname"Task"); \
        rlDelete(task); \
        return false; \
    } \
    { \
        SYS_CS_SYNC(m_TaskListCs); \
        m_TaskList.push_back(task); \
    } \
    return true;

#if RLDB_USE_PROXY

#if __LIVE
#define RLDB_TITLE_SERVER_NAME "dbproxy"
#endif

#define RLDB_USE_LOCAL_PROXY 0

#if RLDB_USE_LOCAL_PROXY
    //Port on which to connect to the proxy
    static const u16 RLDB_LOCAL_PROXY_PORT = 12121;
    static const netAddress RLDB_LOCAL_PROXY_ADDR(NET_MAKE_IP(10, 0, 19, 233), RLDB_LOCAL_PROXY_PORT);
#endif


///////////////////////////////////////////////////////////////////////////////
// rlDbClient::ProxyWorker
///////////////////////////////////////////////////////////////////////////////
rlDbClient::ProxyWorker::ProxyWorker()
: m_Owner(0)
{
}

bool
rlDbClient::ProxyWorker::Start(rlDbClient* owner, const int cpuAffinity)
{
    const bool success = this->rlWorker::Start("[RAGE] rlDbClient Worker",
                                                sysIpcMinThreadStackSize,
                                                cpuAffinity);
    if(success)
    {
        m_Owner = owner;
    }

    return success;
}

bool
rlDbClient::ProxyWorker::Stop()
{
    const bool success = this->rlWorker::Stop();
    m_Owner = 0;
    return success;
}

void
rlDbClient::ProxyWorker::Perform()
{
    rlDbTask* task = 0;

    {
        SYS_CS_SYNC(m_Owner->m_TaskListCs);

        rlAssert(!m_Owner->m_TaskList.empty());
        task = *m_Owner->m_TaskList.begin();
        m_Owner->m_TaskList.pop_front();
    }

    rlAssert(task->IsPending() && !task->IsStarted());

    rtry
    {
        rcheck(m_Owner->ConnectToServer(),
               catchall,
               rlDbError("Worker: Failed to connect to proxy server"));

        task->Start();

        while(!task->IsFinished())
        {
            task->Update(0);
            sysIpcSleep(100);
        }
    }
    rcatchall
    {
        rlDbDebug2("%s could not be started, returning failure", task->GetTaskName());
    }

    rlDelete(task);
}

#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbClient
///////////////////////////////////////////////////////////////////////////////
rlDbClient::rlDbClient()
: m_Initialized(false)
#if RLDB_USE_PROXY
, m_Presence(0)
, m_Socket(-1)
, m_LastBusyTime(0)
, m_MsgBuf(0)
, m_MsgBufSize(0)
#endif
{
}

rlDbClient::~rlDbClient()
{
    Shutdown();
}

bool 
#if RLDB_USE_PROXY
rlDbClient::Init(rlPresence* presence, 
                 const int cpuAffinity,
                 u8* msgBuf,
                 const unsigned msgBufSize)
#else
rlDbClient::Init()
#endif
{
    rlAssert(!m_Initialized);

#if RLDB_USE_PROXY
    rlAssert(presence);
    rlAssert(msgBuf && msgBufSize);

    //Bind our presence delegate, which we use to tell if everyone has gone offline
    //and we must close any open server connection.
    m_Presence = presence;
    m_Presence->AddDelegate(&m_PresenceDlgt);
    m_PresenceDlgt.Bind(this, &rlDbClient::OnPresenceEvent);

    m_MsgBuf = msgBuf;
    m_MsgBufSize = msgBufSize;
    
#if __GFWL
    //Apparent hack for GFWL
    const int cpuAffinity = 0;
#endif

    rtry
    {
        rverify(m_Worker.Start(this, cpuAffinity),
                catchall,
                rlDbError("Init: Error starting rlDbClient worker"));

        rlDbDebug2("Init: Started rlDbClient worker");

        m_Initialized = true;
    }
    rcatchall
    {
        Shutdown();
    }
    
#else
    m_Initialized = true;
#endif

    return m_Initialized;
}

void 
rlDbClient::Shutdown()
{
    if(!m_Initialized) return;

#if RLDB_USE_PROXY
    if(m_Presence)
    {
        m_Presence->RemoveDelegate(&m_PresenceDlgt);
        m_Presence = 0;
    }

    if(m_Worker.IsRunning())
    {
        m_Worker.Stop();
        rlDbDebug2("Shutdown: Stopped rlDbClient worker");
    }

    m_MsgBuf = 0;
    m_MsgBufSize = 0;
#endif

    {
        SYS_CS_SYNC(m_TaskListCs);
     
        while(!m_TaskList.empty())
        {
            TaskList::iterator it = m_TaskList.begin();

            rlDbTask* t = *it;
            rlAssert(t);

            m_TaskList.erase(it);

            rlDelete(t);
        }
    }

    m_Initialized = false;
}

void 
rlDbClient::Update()
{
    rlAssert(m_Initialized);

    SYS_CS_SYNC(m_TaskListCs);

#if RLDB_USE_PROXY
    //If we haven't executed any commands for awhile, close the LSP server connection.
    //Note that the timeout is five minutese because the overhead of setting up a secure 
    //connection far outweighs the cost of maintaining it. (According to MS.)
    const unsigned MAX_IDLE_MS = 5 * 60 * 1000; //5 min

    if(m_Worker.IsPerforming())
    {
        m_LastBusyTime = sysTimer::GetSystemMsTime();
    }
    else if(!m_TaskList.empty())
    {
        m_Worker.Wakeup();
    }
    else if(IsConnectedToServer() && ((sysTimer::GetSystemMsTime() - m_LastBusyTime) >= MAX_IDLE_MS))
    {
        rlDbDebug2("Update: No tasks for more than %ums, disconnecting from server", MAX_IDLE_MS);
        ResetServer();
    }

#else
    if(!m_TaskList.empty())
    {
        TaskList::iterator it = m_TaskList.begin();

        while(!m_TaskList.empty() && (it != m_TaskList.end()))
        {
            rlDbTask* t = *it;
            rlAssert(t);

            if(!t->IsStarted())
            {
                rlAssert(t->IsPending());
                t->Start();
            }

            if(t->IsActive())
            {
                t->Update(0);
            }

            if(t->IsFinished())
            {
                TaskList::iterator nextIt = it;
                ++nextIt;               
                m_TaskList.erase(it);
                it = nextIt;

                rlDelete(t);
            }
            else
            {
                ++it;
            }
        }
    }
#endif
}

bool 
rlDbClient::IsServiceAvailable() const
{
#if __GAMESPYPC
	// can't check IsOnline() on PC because it will only return true
	// when the player is fully signed in. However, we need to perform
	// database queries during the sign-in process.
	return g_rlGamespy.IsConnected();
#elif __GAMESPY
    return g_rlGamespy.IsOnline();
#else
    return true;
#endif
}

bool 
rlDbClient::CreateRecord(const char* tableName,
                         const rlDbField* fields,
                         const unsigned numFields,
                         rlDbRecordId* recordId,
                         netStatus* status)
{
   CREATETASK(CreateRecord, this, tableName, fields, numFields, recordId, status);
}

bool 
rlDbClient::UpdateRecord(const char* tableName,
                               const rlDbRecordId recordId,
                               const rlDbField* fields,
                               const unsigned numFields,
                               netStatus* status)
{
    CREATETASK(UpdateRecord, this, tableName, recordId, fields, numFields, status);
}

bool 
rlDbClient::DeleteRecord(const char* tableName,
                               const rlDbRecordId recordId,
                               netStatus* status)
{
    CREATETASK(DeleteRecord, this, tableName, recordId, status);
}

bool 
rlDbClient::RateRecord(const char* tableName,
                             const rlDbRecordId recordId,
                             const unsigned curNumRatings,
                             const float curAvgRating,
                             const rlDbRating rating,
                             unsigned* numRatings,
                             float* avgRating,
                             netStatus* status)
{
    CREATETASK(RateRecord, this, tableName, recordId, curNumRatings, curAvgRating, rating, numRatings, avgRating, status);
}

bool 
rlDbClient::GetNumRecords(const char* tableName,
                                const char* filter,
                                unsigned* numRecords,
                                netStatus* status)
{
    CREATETASK(GetNumRecords, this, tableName, filter, numRecords, status);
}

bool 
rlDbClient::ReadRecords(const char* tableName,
                              const char* filter,
                              const char* sort,
                              const unsigned startingRank,
                              const char* postSortFilter,
                              const unsigned radius,
                              const bool cacheResults,
                              const unsigned maxRecords,
                              rlDbReadResults* results,
                              netStatus* status)
{
    CREATETASK(ReadRecords, this, tableName, filter, sort, startingRank, postSortFilter, radius, cacheResults, maxRecords, results, status);
}

bool 
rlDbClient::ReadRecordById(const char* tableName,
                           const rlDbRecordId recordId,
                           rlDbReadResults* results,
                           netStatus* status)
{
    CREATETASK(ReadRecordById, this, tableName, recordId, results, status);
}

bool 
rlDbClient::ReadRandomRecord(const char* tableName,
                             const char* filter,
                             rlDbReadResults* results,
                             netStatus* status)
{
    CREATETASK(ReadRandomRecord, this, tableName, filter, results, status);
}

bool 
rlDbClient::UploadFile(const void* data,
                             const unsigned size,
                             float* progress,
                             rlDbFileId* fileId,
                             netStatus* status)
{
    CREATETASK(UploadFile, this, data, size, progress, fileId, status);
}

bool 
rlDbClient::DownloadFile(const rlDbFileId fileId,
                               u8* buf,
                               const unsigned bufSize,
                               float* progress,
                               unsigned* fileSize,
                               netStatus* status)
{
    CREATETASK(DownloadFile, this, fileId, buf, bufSize, progress, fileSize, status);
}

#if RLDB_USE_PROXY

void 
rlDbClient::OnPresenceEvent(rlPresence* /*p*/, const rlPresenceEvent* evt)
{
    if(PRESENCE_EVENT_SIGNIN_STATUS_CHANGED == evt->GetId())
    {
        if(IsConnectedToServer() && !CanConnectToServer())
        {
            rlDbDebug2(("No gamers online; closing server connection"));
            ResetServer();
            return;
        }
    }
}

bool
rlDbClient::CanConnectToServer() const
{
    bool gamerOnline = false;

    if(m_Presence)
    {
        for(unsigned i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
        {
            const rlGamerInfo* gi = m_Presence->GetGamerInfo(i);
            rlAssert(gi);

            if(gi->IsOnline())
            {
                gamerOnline = true;
                break;
            }
        }
    }

    return gamerOnline;
}

bool
rlDbClient::IsConnectedToServer() const
{
    return (m_Socket != -1);
}

void
rlDbClient::ResetServer()
{
    rlDbDebug2("ResetServer()");

    if(m_Socket >= 0)
    {
        netTcp::Close(m_Socket);
        m_Socket = -1;
    }
}

bool 
rlDbClient::ConnectToServer()
{
    if(IsConnectedToServer())
    {
        return true; //Already connected
    }

    if(!CanConnectToServer())
    {
        return false;
    }

    rtry
    {
        netAddress addr;

#if RLDB_USE_LOCAL_PROXY
        addr = RLDB_LOCAL_PROXY_ADDR;
#else
        netStatus status;

#if __LIVE
        rcheck(rlXlspManager::ResolveServerAddr(RLDB_TITLE_SERVER_NAME, &addr, 60, &status),
               catchall,
               rlDbError("Failed to begin resolving server address"));
#else
        rlDbError("rlDbClient::ConnectToServer: unsupported platform");
#endif

        while(status.Pending())
        {
            sysIpcSleep(1);
        }

        rcheck(status.Succeeded() && addr.IsValid(),
            catchall,
            rlDbWarning("Failed to resolve server address"));
#endif

        rlAssert(false);
        //NO LONGER SUPPORTED - use ConnectAsync()
        //rcheck(netTcp::Connect(addr, &m_Socket, 20), catchall, );

        return true;
    }
    rcatchall
    {
        ResetServer();
        return false;
    }
}
#endif //RLDB_USE_PROXY

}; //namespace rage
