// 
// rline/rldatabaseclient.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLDBCLIENT_H
#define RLINE_RLDBCLIENT_H

#include "rldatabase.h"
#include "rldatabasetasks.h"
#include "rlpresence.h"
#include "rlworker.h"
#include "net\status.h"
#include "system\criticalsection.h"

namespace rage
{

///////////////////////////////////////////////////////////////////////////////
// rlDbClient
///////////////////////////////////////////////////////////////////////////////
class rlDbClient
{
public:
    rlDbClient();
    ~rlDbClient();

#if RLDB_USE_PROXY
    bool Init(rlPresence* presence, 
              const int cpuAffinity,
              u8* msgBuf,
              const unsigned msgBufSize);
#else
    bool Init();
#endif

    void Shutdown();
    
    void Update();

    bool IsServiceAvailable() const;

    bool CreateRecord(const char* tableName,
                      const rlDbField* fields,
                      const unsigned numFields,
                      rlDbRecordId* recordId,
                      netStatus* status);

    bool UpdateRecord(const char* tableName,
                      const rlDbRecordId recordId,
                      const rlDbField* fields,
                      const unsigned numFields,
                      netStatus* status);

    bool DeleteRecord(const char* tableName,
                      const rlDbRecordId recordId,
                      netStatus* status);

    bool RateRecord(const char* tableName,
                    const rlDbRecordId recordId,
                    const unsigned curNumRatings,
                    const float curAvgRating,
                    const rlDbRating rating,
                    unsigned* numRatings,
                    float* avgRating,
                    netStatus* status);

    bool GetNumRecords(const char* tableName,
                       const char* filter,
                       unsigned* numRecords,
                       netStatus* status);

    bool ReadRecords(const char* tableName,
                     const char* filter,
                     const char* sort,
                     const unsigned startingRank,
                     const char* postSortFilter,
                     const unsigned radius,
                     const bool cacheResults,
                     const unsigned maxRecords,
                     rlDbReadResults* results,
                     netStatus* status);

    bool ReadRecordById(const char* tableName,
                        const rlDbRecordId recordId,
                        rlDbReadResults* results,
                        netStatus* status);

    bool ReadRandomRecord(const char* tableName,
                          const char* filter,
                          rlDbReadResults* results,
                          netStatus* status);

    bool UploadFile(const void* data,
                    const unsigned size,
                    float* progress,
                    rlDbFileId* fileId,
                    netStatus* status);

    bool DownloadFile(const rlDbFileId fileId,
                      u8* buf,
                      const unsigned bufSize,
                      float* progress,
                      unsigned* bytesRead,                      
                      netStatus* status);

private:
    typedef inlist<rlDbTask, &rlDbTask::m_ListLink> TaskList;
    TaskList m_TaskList;
    sysCriticalSectionToken m_TaskListCs;

#if RLDB_USE_PROXY
    friend class rlDbTask;

    bool CanConnectToServer() const;
    bool IsConnectedToServer() const;
    bool ConnectToServer();

    void ResetServer();

    struct ProxyWorker : public rlWorker
    {
        ProxyWorker();

        bool Start(rlDbClient* owner, const int cpuAffinity);
        bool Stop();

    private:
        //Make Start() un-callable
        virtual bool Start(const char* /*name*/,
                           const unsigned /*stackSize*/,
                           const int /*cpuAffinity*/)
        {
            return rlVerify(false);
        }

        virtual void Perform();

        rlDbClient* m_Owner;
    };
    ProxyWorker m_Worker;

    int m_Socket;
    unsigned m_LastBusyTime;
    u8* m_MsgBuf;
    unsigned m_MsgBufSize;

    rlPresence* m_Presence;
    rlPresence::Delegate m_PresenceDlgt;
    void OnPresenceEvent(rlPresence* p, const rlPresenceEvent* evt);
#endif

    bool m_Initialized  : 1;
};

}; //namespace rage

#endif //RLINE_RLDBCLIENT_H
