// 
// rline/rldatabase.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLDB_H
#define RLINE_RLDB_H

#include "rl.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"

namespace rage
{

#define RLDB_USE_SAKE     __GAMESPY
#define RLDB_USE_PROXY    !RLDB_USE_SAKE

RAGE_DECLARE_SUBCHANNEL(rline, db)

#define rlDbDebug1(fmt, ...)  RAGE_DEBUGF1(rline_db, fmt, ##__VA_ARGS__)
#define rlDbDebug2(fmt, ...)  RAGE_DEBUGF2(rline_db, fmt, ##__VA_ARGS__)
#define rlDbDebug3(fmt, ...)  RAGE_DEBUGF3(rline_db, fmt, ##__VA_ARGS__)
#define rlDbWarning(fmt, ...) RAGE_WARNINGF(rline_db, fmt, ##__VA_ARGS__)
#define rlDbError(fmt, ...)   RAGE_ERRORF(rline_db, fmt, ##__VA_ARGS__)

typedef int rlDbRecordId;   //Unique ID for a record in a table
typedef int rlDbFileId;     //Unique ID for an uploaded file
typedef u8  rlDbRating;     //Legal range for ratings given to a record is 0-255
typedef s8  rlDbValueType;  //Type of value for a field (int16, float, string, etc)

const rlDbRecordId RLDB_INVALID_RECORDID        = 0;
const rlDbFileId RLDB_INVALID_FILEID            = 0;

//PURPOSE
//  Limit constants. If you find any of these are too small, let RAGE networking
//  know and we'll see what we can do.
const unsigned RLDB_MAX_TABLE_NAME_CHARS        = 32;
const unsigned RLDB_MAX_FIELD_NAME_CHARS        = 32;
const unsigned RLDB_MAX_FILTER_CHARS            = 128;
const unsigned RLDB_MAX_SORT_CHARS              = 64;
const unsigned RLDB_MAX_VALUE_TYPE_STR_CHARS    = 7;
const rlDbRating RLDB_MAX_RATING                = 255;
const unsigned RLDB_MAX_FIELDS_PER_TABLE        = 1023;

//NOTE: Post-sort filter is huge because when reading stats it can include
//      the names of all your friends. (Up to the XBL max of 101 gamers.)
const unsigned RLDB_MAX_POSTSORT_FILTER_CHARS    = (101 * 35) + 1; 

//NOTE: We have a max file size to prevent hacked messages from crashing
//      the server by telling it to allocate gigabytes for a bogus file.
//      If this needs to be increased, let RAGE networking know.
const unsigned RLDB_MAX_FILE_SIZE               = 1024 * 1024;

//PURPOSE
//  Result codes set on the netStatus object passed to rlDbClient methods.
enum rlDbResultCode
{
    RLDB_RESULT_NONE = 0,                 //Result code was not set
    RLDB_RESULT_SUCCESS,
    RLDB_RESULT_CANCELLED,
    RLDB_RESULT_TIMED_OUT,
    RLDB_RESULT_MSG_WRITE_FAILED,
    RLDB_RESULT_MSG_READ_FAILED,
    RLDB_RESULT_OUT_OF_MEMORY,
    RLDB_RESULT_SOCKET_ERROR,
    RLDB_RESULT_AUTHENTICATION_FAILED,
    RLDB_RESULT_NETWORK_ERROR,
    RLDB_RESULT_INVALID_ARGUMENTS,
    RLDB_RESULT_NO_PERMISSION,
    RLDB_RESULT_RECORD_LIMIT_REACHED,
    RLDB_RESULT_ALREADY_RATED,
    RLDB_RESULT_NOT_RATEABLE,
    RLDB_RESULT_NOT_OWNED,
    RLDB_RESULT_FILE_NOT_FOUND,
    RLDB_RESULT_FILE_TOO_LARGE,
    RLDB_RESULT_BUFFER_OVERFLOW,
    RLDB_RESULT_DNS_ERROR,
    RLDB_RESULT_GAMESPY_HTTP_ERROR,
    RLDB_RESULT_GAMESPY_CONNECTION_ERROR,
    RLDB_RESULT_GAMESPY_START_ERROR,
    RLDB_RESULT_UNKNOWN_ERROR,   
    RLDB_NUM_RESULT_CODES
};
const char* rlDbGetResultAsString(const int result);

//PURPOSE
//  Standard database fields used frequently by multiple systems.
enum rlDbFieldId
{
    RLDB_FIELD_RECORD_ID = 0,
    RLDB_FIELD_OWNER_ID,
    RLDB_FIELD_GAMER_NAME,
    RLDB_FIELD_GAMER_HANDLE,
    RLDB_FIELD_NUM_RATINGS,
    RLDB_FIELD_AVG_RATING,
    RLDB_FIELD_RANK,
    RLDB_NUM_FIELDS
};
const char* rlDbGetFieldName(const int fieldId);

#if RLDB_USE_SAKE
//PURPOSE
//  Sets whether to use "manual" rating or Sake's built-in rating.  This is 
//  required for the database proxy.  The default is false, and unless you
//  really know what you're doing, you shouldn't change it :)
void rlDbSetUsingManualRating(const bool useManualRating);
bool rlDbIsUsingManualRating();
#endif

//PURPOSE
//  Possible values for rlDbValueType.
enum rlDbValueTypeIds
{
    RLDB_VALUETYPE_INVALID = -1,
    RLDB_VALUETYPE_BOOL,
    RLDB_VALUETYPE_UNS8,
    RLDB_VALUETYPE_INT16,
    RLDB_VALUETYPE_INT32,
    RLDB_VALUETYPE_INT64,
    RLDB_VALUETYPE_FLOAT,
    RLDB_VALUETYPE_ASCII_STRING,    //Includes UTF-8, but not other Unicode formats
    RLDB_VALUETYPE_BINARY_DATA,
    RLDB_NUM_VALUETYPES
};
const char* rlDbValueTypeToString(const rlDbValueType t);
rlDbValueType rlDbValueTypeFromString(const char* str);

class rlGamespySakeRequest;
class rlGamespySakeReadRandomDbRecordRequest;

//PURPOSE
//  Stores the binary data for a binary rlDbValue. This data can either be a
//  reference or allocated and owned by the object, usually depending on
//  whether we're the client or server, respectively.
class rlDbBinaryData
{
public:
    enum eMode
    {
        MODE_INVALID,
        MODE_COPY_PTR_ONLY,
        MODE_COPY_DATA     
    };

    rlDbBinaryData();
    ~rlDbBinaryData();

    void Clear();

    bool Reset(const u8* data, 
               const unsigned size,
               const eMode mode);

    const u8* GetData() const;
    unsigned GetSize() const;

    bool Export(datExportBuffer& bb) const;
    bool Import(const datImportBuffer& bb);

private:
    rlDbBinaryData(const rlDbBinaryData& rhs);
    rlDbBinaryData& operator=(const rlDbBinaryData& rhs);

    u8* m_Data;
    unsigned m_Size;
    bool m_OwnData : 1;
};

//PURPOSE
//  Stores the value for a field.
class rlDbValue
{
public:
    rlDbValue();
    ~rlDbValue();

    rlDbValue(const rlDbValue& rhs);
    rlDbValue& operator=(const rlDbValue& rhs);

    void Clear();

    rlDbValueType GetType() const;

    bool GetBool() const;
    u8 GetUns8() const;
    s16 GetInt16() const;
    s32 GetInt32() const;
    s64 GetInt64() const;
    float GetFloat() const;
    const char* GetAsciiString() const;
    const rlDbBinaryData& GetBinaryData() const;

    void SetBool(const bool val);
    void SetUns8(const u8 val);
    void SetInt16(const s16 val);
    void SetInt32(const s32 val);
    void SetInt64(const s64 val);
    void SetFloat(const float val);
    bool SetAsciiString(const char* str);
    bool SetBinaryData(const u8* data,
                       const unsigned size,
                       const rlDbBinaryData::eMode owner);

    const char* ToString(char* buf, const unsigned bufSize) const;
    bool FromString(const char* str, const rlDbValueType asType);

    const char* TypeToString() const;

    bool Export(datExportBuffer& bb) const;
    bool Import(const datImportBuffer& bb);

private:
    union ValueUnion
    {
        bool    m_Bool;
        u8      m_Uns8;
        s16     m_Int16;
        s32     m_Int32;
        s64     m_Int64;
        float   m_Float;
        char*   m_AsciiString;
    };

    rlDbBinaryData m_BinaryData;    
    ValueUnion m_Union;
    rlDbValueType m_Type;
};

//PURPOSE
//  Describes a DB field (a name and value).  This is only used when writing
//  to the DB; for reading, rlDbSchema and rlDbReadResults are used.
class rlDbField
{
public:
    rlDbField();
    ~rlDbField();

    void Clear();

    const char* GetName() const;
    void SetName(const char* name);

    const rlDbValue& GetValue() const;
    rlDbValue& GetValue();

    bool Export(datExportBuffer& bb) const;
    bool Import(const datImportBuffer& bb);

private:
    char m_Name[RLDB_MAX_FIELD_NAME_CHARS];
    rlDbValue m_Value;
};

//PURPOSE
//  A schema describes the name and type of a set of columns.  These can be either
//  all of a table's columns, or a subset.  Schemas are used when reading from 
//  a table.
class rlDbSchemaBase
{
public:
    rlDbSchemaBase();
    virtual ~rlDbSchemaBase();

    //PURPOSE
    //  Clears the schema, setting it's number of fields to zero and releasing
    //  any dynamically allocated resources.
    virtual void Clear();

    //PURPOSE
    //  Adds a field to the schema, and increments the number of fields.
    //  Returns false if schema already contained its maximum number of fields.
    bool AddField(const char* name, const rlDbValueType vt);

    //PURPOSE
    //  Simple Accessors
    unsigned GetNumFields() const;
    const char* GetFieldName(const unsigned index) const;
    rlDbValueType GetFieldType(const unsigned index) const;
    
    //PURPOSE
    //  Finds the index of a field by name.  Returns -1 if not found.
    int GetFieldIndex(const char* name) const;

    //PURPOSE
    //  Returns maximum number of fields that can be added to the schema.
    virtual unsigned GetMaxFields() const = 0;

    //PURPOSE
    //  Returns pointer to array of field names.  Only valid of GetNumFields()
    //  is greater than zero.
    virtual const char** GetFieldNames() const = 0;

    //PURPOSE
    //  Returns pointer to array of field types.  Only valid of GetNumFields()
    //  is greater than zero.
    virtual const rlDbValueType* GetFieldTypes() const = 0;
    
    //PURPOSE
    //  Imports the schema from a bit buffer.
    virtual bool Import(const datImportBuffer& bb) = 0;

    bool Export(datExportBuffer& bb) const;

protected:
    //PURPOSE
    //  Tells us whether to copy just the field name pointer, or the actual string.
    //  In the latter case, it is required that the derived class have allocated
    //  RLDB_MAX_FIELD_NAME_CHARS for the string, and that GetFieldNames()[index] 
    //  points to that space.
    virtual bool CopyFieldNames() const = 0;

private:
    unsigned m_NumFields;
};

//PURPOSE
//  Schema that can have any number of maximum fields, and can either copy
//  the field names or only keep pointers to them.  This is ideal if you don't
//  know how many fields the schema will have (like when importing a schema).
class rlDbDynSchema : public rlDbSchemaBase
{
public:
    enum eMode
    {
        COPY_NAME_PTR_ONLY = 0,
        COPY_NAME_STRING
    };

    rlDbDynSchema();
    virtual ~rlDbDynSchema();

    //PURPOSE
    //  Clears the schema, setting it's number of fields to zero and releasing
    //  any dynamically allocated resources.
    virtual void Clear();

    //PURPOSE
    //  Clears the schema, sets its max size, and sets whether to copy only
    //  name pointers or the actual string data.
    bool Reset(const unsigned maxFields, const eMode mode = COPY_NAME_PTR_ONLY);

    virtual unsigned GetMaxFields() const;
    virtual const char** GetFieldNames() const;
    virtual const rlDbValueType* GetFieldTypes() const;
    virtual bool Import(const datImportBuffer& bb);

protected:
    virtual bool CopyFieldNames() const;

private:
    rlDbDynSchema(const rlDbDynSchema& that);
    rlDbDynSchema& operator=(const rlDbDynSchema& that);
    
    unsigned m_MaxFields;
    const char** m_FieldNames;
    rlDbValueType* m_FieldTypes;
    char* m_FieldNamesBuf;
};

//PURPOSE
//  A schema with a fixed maximum number of fields.  This is ideal when you
//  know in advance how many fields you'll use, and have pointers to static
//  field names (the schema does not copy the strings).
template<unsigned T_MAX_FIELDS>
class rlDbFixedSchema : public rlDbSchemaBase
{
public:
    rlDbFixedSchema() {}
    virtual ~rlDbFixedSchema() {}

    virtual unsigned GetMaxFields() const { return T_MAX_FIELDS; }
    virtual const char** GetFieldNames() const { return (const char**)m_FieldNames; }
    virtual const rlDbValueType* GetFieldTypes() const { return m_FieldTypes; }

    virtual bool Import(const datImportBuffer& bb)
    {
        rtry
        {
            unsigned numFields = 0;
            rcheck(bb.ReadUns(numFields, datBitsNeeded<RLDB_MAX_FIELDS_PER_TABLE>::COUNT),
                   catchall,
                   rlDbError("Failed to read num fields"));

            rcheck(numFields <= T_MAX_FIELDS, 
                   catchall, 
                   rlDbError("Too many fields (%u, max is %u)", numFields, T_MAX_FIELDS));

            for(unsigned i = 0; i < numFields; i++)
            {
                char name[RLDB_MAX_FIELD_NAME_CHARS];
                rcheck(bb.ReadStr(name, sizeof(name)),
                       catchall,
                       rlDbError("Failed to read name for field %u", i));

                rlDbValueType vt;
                rcheck(bb.ReadInt(vt, datBitsNeeded<RLDB_NUM_VALUETYPES>::COUNT + 1),
                       catchall,
                       rlDbError("Failed to read type for field %u", i));

                AddField(name, vt);
            }

            return true;
        }
        rcatchall
        {
            Clear();
            return false;
        }
    }

protected:
    virtual bool CopyFieldNames() const { return false; }

private:
    rlDbFixedSchema(const rlDbFixedSchema& that);
    rlDbFixedSchema& operator=(const rlDbFixedSchema& that);

    const char* m_FieldNames[T_MAX_FIELDS];
    rlDbValueType m_FieldTypes[T_MAX_FIELDS];
};

//PURPOSE
//  Important class that stores the results of a read operation.  The user calls
//  Reset() to set the schema (columns to read), max records to read, and a 
//  user-allocated array of values large enough to hold the results.  The 
//  rlDbReadResults object is then passed into one of the rlDbClient::ReadXX()
//  methods, which fills it in.  The results can then be retrieved with the
//  various GetXX() methods.
class rlDbReadResults
{
public:
    rlDbReadResults();
    ~rlDbReadResults();

    //Resets the schema for the results, and the array of values it stores results in.
    //This array must be allocated and freed by the user, and must be at least
    //schema.GetNumFields() * maxRecords in size.
    void Reset(const rlDbSchemaBase* schema,
               const unsigned maxRecords,
               rlDbValue* values);

    const rlDbSchemaBase* GetSchema() const;
    const char* GetFieldName(const unsigned column) const;
    rlDbValueType GetFieldType(const unsigned column) const;

    unsigned GetMaxRecords() const;
    unsigned GetNumRecords() const;

    int GetRowIndexContaining(const char* fieldName, 
                              const char* value,
                              const unsigned startIndex) const;

    bool GetBoolByName(const unsigned row, const char* name) const; 
    u8 GetUns8ByName(const unsigned row, const char* name) const;
    s16 GetInt16ByName(const unsigned row, const char* name) const;
    s32 GetInt32ByName(const unsigned row, const char* name) const;
    s64 GetInt64ByName(const unsigned row, const char* name) const;
    float GetFloatByName(const unsigned row, const char* name) const;
    const char* GetAsciiStringByName(const unsigned row, const char* name) const;
    const rlDbBinaryData& GetBinaryDataByName(const unsigned row, const char* name) const;

    bool GetBool(const unsigned row, const unsigned column) const;
    u8 GetUns8(const unsigned row, const unsigned column) const;
    s16 GetInt16(const unsigned row, const unsigned column) const;
    s32 GetInt32(const unsigned row, const unsigned column) const;
    s64 GetInt64(const unsigned row, const unsigned column) const;
    float GetFloat(const unsigned row, const unsigned column) const;
    const char* GetAsciiString(const unsigned row, const unsigned column) const;
    const rlDbBinaryData& GetBinaryData(const unsigned row, const unsigned column) const;

    const char* ToString(const unsigned row, 
                         const unsigned column,
                         char* buf,
                         const unsigned bufSize) const;

    bool Export(datExportBuffer& bb) const;
    bool Import(const datImportBuffer& bb);

    void SetNumRecords(const unsigned num);

    void SetBool(const unsigned row, const unsigned column, const bool val);
    void SetUns8(const unsigned row, const unsigned column, const u8 val);
    void SetInt16(const unsigned row, const unsigned column, const s16 val);
    void SetInt32(const unsigned row, const unsigned column, const s32 val);
    void SetInt64(const unsigned row, const unsigned column, const s64 val);
    void SetFloat(const unsigned row, const unsigned column, const float val);
    bool SetAsciiString(const unsigned row, const unsigned column, const char* str);
    bool SetBinaryData(const unsigned row, 
        const unsigned column, 
        const u8* data,
        const unsigned size,
        const rlDbBinaryData::eMode owner);

private:    
    rlDbReadResults(const rlDbReadResults& rhs);
    rlDbReadResults& operator=(const rlDbReadResults& rhs);

    const rlDbValue& GetValue(const unsigned row, const unsigned column) const;
    rlDbValue& GetValue(const unsigned row, const unsigned column);

    const rlDbSchemaBase* m_Schema;
    unsigned m_MaxRecords;
    unsigned m_NumRecords;
    rlDbValue* m_Values;
};

}; //namespace rage

#endif //RLINE_RLDB_H
