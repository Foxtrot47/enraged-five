// 
// rline/rldatabasetasks.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rldatabasetasks.h"
#include "rldatabaseclient.h"
#include "system/memory.h"

#if __LIVE
#include "system/xtl.h"
#endif  //__LIVE

#if RLDB_USE_SAKE
#include "rlgamespy.h"
#include "rlgamespysake.h"
#endif

#if RLDB_USE_PROXY
#include "net/tcp.h"
#endif

namespace rage
{

//Convenience macros for simple allocation and deallocation
#define RLDBTASK_ALLOC(var, type, num) \
    if(0 == (var = RL_NEW_ARRAY(rlDbTask, type, num))) \
    { \
        rlTaskError("Failed to allocate %u %s", num, #type); \
        return false; \
    }

#define RLDBTASK_FREE(var, num) rlDelete(var, num);

#if RLDB_USE_SAKE

#define RLDBTASK_SAKE_REQUEST(type) \
    RLDBTASK_ALLOC(m_SakeReq, type, 1); \
    type* req = (type*)m_SakeReq;

#endif

//Convenience macros for importing/exporting requests and replies
#if RLDB_USE_PROXY

#define RLDBTASK_EXPORT(msg) \
    if(!msg.Export(buf, bufSize, msgSize)) \
    { \
       rlTaskError("Failed to export " #msg ); \
       return RLDB_RESULT_MSG_WRITE_FAILED; \
    } \
    return RLDB_RESULT_SUCCESS;

#define RLDBTASK_IMPORT(msgType) \
    unsigned msgId = 0; \
    if(!netMessage::GetId(&msgId, data, size)) \
    { \
        rlTaskError("Failed to get " #msgType " msgId from msg data"); \
        return RLDB_RESULT_MSG_READ_FAILED; \
    } \
    if(msgType::MSG_ID() != msgId) \
    { \
        rlTaskError("Received msg (id=%d) other than expected reply (" #msgType "::MSG_ID()", msgId); \
        return RLDB_RESULT_MSG_READ_FAILED; \
    } \
    msgType msg; \
    if(!msg.Import(data, size)) \
    { \
        rlTaskError("Failed to import " #msgType); \
        return RLDB_RESULT_MSG_READ_FAILED; \
    } \
    if(!msg.m_Success) \
    { \
        rlTaskDebug2("Request failed (result code %d)", msg.m_ResultCode); \
        return msg.m_ResultCode; \
    }

#define RLDBTASK_IMPORT_RECORDS() \
    unsigned msgId = 0; \
    if(!netMessage::GetId(&msgId, data, size)) \
    { \
        rlTaskError("Failed to get msgId from msg data"); \
        return RLDB_RESULT_MSG_READ_FAILED; \
    } \
    if(rlDbMsgReadRecordsReply::MSG_ID() != msgId) \
    { \
        rlTaskError("Received msg other than expected reply"); \
        return RLDB_RESULT_MSG_READ_FAILED; \
    } \
    rlDbMsgReadRecordsReply msg; \
    msg.ResetForImport(m_Results); \
    if(!msg.Import(data, size)) \
    { \
        rlTaskError("Failed to import message"); \
        return RLDB_RESULT_MSG_READ_FAILED; \
    } \
    if(!msg.m_Success) \
    { \
        rlTaskDebug2("Failed reply (result code %d)", msg.m_ResultCode); \
        return msg.m_ResultCode; \
    }

#endif //RLDB_USE_PROXY


#if RLDB_USE_SAKE
static int 
sSakeResultToDb(const int result)
{
    switch(result)
    {
    case rlGamespySakeRequest::RESULT_SUCCESS:
        return RLDB_RESULT_SUCCESS;

    case rlGamespySakeRequest::RESULT_CANCELLED:
        return RLDB_RESULT_CANCELLED;

    case rlGamespySakeRequest::RESULT_TIMED_OUT:
        return RLDB_RESULT_TIMED_OUT;

    case rlGamespySakeRequest::RESULT_SAKE_START_ERROR:
        return RLDB_RESULT_GAMESPY_START_ERROR;

    case rlGamespySakeRequest::RESULT_SAKE_AUTHENTICATION_FAILED:
        return RLDB_RESULT_UNKNOWN_ERROR;

    case rlGamespySakeRequest::RESULT_SAKE_NETWORK_ERROR:
        return RLDB_RESULT_NETWORK_ERROR;

    case rlGamespySakeRequest::RESULT_SAKE_CONNECTION_ERROR:
        return RLDB_RESULT_GAMESPY_CONNECTION_ERROR;

    case rlGamespySakeRequest::RESULT_SAKE_OUT_OF_MEMORY:
        return RLDB_RESULT_OUT_OF_MEMORY;

    case rlGamespySakeRequest::RESULT_SAKE_INVALID_ARGUMENTS:
        return RLDB_RESULT_INVALID_ARGUMENTS;

    case rlGamespySakeRequest::RESULT_SAKE_NO_PERMISSION:
        return RLDB_RESULT_NO_PERMISSION;

    case rlGamespySakeRequest::RESULT_SAKE_RECORD_LIMIT_REACHED:
        return RLDB_RESULT_RECORD_LIMIT_REACHED;

    case rlGamespySakeRequest::RESULT_SAKE_ALREADY_RATED:
        return RLDB_RESULT_ALREADY_RATED;

    case rlGamespySakeRequest::RESULT_SAKE_NOT_RATEABLE:
        return RLDB_RESULT_NOT_RATEABLE;

    case rlGamespySakeRequest::RESULT_SAKE_NOT_OWNED:
        return RLDB_RESULT_NOT_OWNED;

    case rlGamespySakeRequest::RESULT_SAKE_UNKNOWN_ERROR:
        return RLDB_RESULT_UNKNOWN_ERROR;

    case rlGamespySakeRequest::RESULT_HTTP_POST_ERROR:
    case rlGamespySakeRequest::RESULT_HTTP_GET_ERROR:
    case rlGamespySakeRequest::RESULT_HTTP_FILE_RESULT_HEADER_ERROR:
    case rlGamespySakeRequest::RESULT_HTTP_FILE_ID_HEADER_ERROR:
        return RLDB_RESULT_GAMESPY_HTTP_ERROR;

    case rlGamespySakeRequest::RESULT_HTTP_OUT_OF_MEMORY:    
        return RLDB_RESULT_OUT_OF_MEMORY;
    
    case rlGamespySakeRequest::RESULT_HTTP_BUFFER_OVERFLOW:
        return RLDB_RESULT_BUFFER_OVERFLOW;

    case rlGamespySakeRequest::RESULT_HTTP_INVALID_ARGUMENTS:
        return RLDB_RESULT_INVALID_ARGUMENTS;
    
    case rlGamespySakeRequest::RESULT_HTTP_DNS_ERROR:
        return RLDB_RESULT_DNS_ERROR;
    
    case rlGamespySakeRequest::RESULT_HTTP_NETWORK_ERROR:
        return RLDB_RESULT_NETWORK_ERROR;
    
    case rlGamespySakeRequest::RESULT_HTTP_FILE_NOT_FOUND:
        return RLDB_RESULT_FILE_NOT_FOUND;

    case rlGamespySakeRequest::RESULT_HTTP_UNKNOWN_ERROR:
        return RLDB_RESULT_UNKNOWN_ERROR;

    case rlGamespySakeRequest::RESULT_FILE_INVALID_ARGUMENTS:
        return RLDB_RESULT_INVALID_ARGUMENTS;

    case rlGamespySakeRequest::RESULT_FILE_NOT_FOUND:
        return RLDB_RESULT_FILE_NOT_FOUND;

    case rlGamespySakeRequest::RESULT_FILE_TOO_LARGE:
        return RLDB_RESULT_FILE_TOO_LARGE;

    case rlGamespySakeRequest::RESULT_FILE_UNKNOWN_ERROR:
        return RLDB_RESULT_UNKNOWN_ERROR;

    default:
        return RLDB_RESULT_UNKNOWN_ERROR;
    }
}

static bool
sDbFieldToSake(const rlDbField& dbField,
               SAKEField* sakeField)
{
    sakeField->mName = (char*)dbField.GetName();
    
    switch(dbField.GetValue().GetType())
    {
        case RLDB_VALUETYPE_BOOL:         
            sakeField->mType = SAKEFieldType_BOOLEAN;
            sakeField->mValue.mBoolean = dbField.GetValue().GetBool();
            break;
        case RLDB_VALUETYPE_UNS8:         
            sakeField->mType = SAKEFieldType_BYTE;
            sakeField->mValue.mByte = dbField.GetValue().GetUns8();
            break;
        case RLDB_VALUETYPE_INT16:        
            sakeField->mType = SAKEFieldType_SHORT;
            sakeField->mValue.mShort = dbField.GetValue().GetInt16();
            break;
        case RLDB_VALUETYPE_INT32:          
            sakeField->mType = SAKEFieldType_INT;
            sakeField->mValue.mInt = dbField.GetValue().GetInt32();
            break;
        case RLDB_VALUETYPE_INT64:        
            sakeField->mType = SAKEFieldType_INT64;
            sakeField->mValue.mInt64 = dbField.GetValue().GetInt64();
            break;
        case RLDB_VALUETYPE_FLOAT:        
            sakeField->mType = SAKEFieldType_FLOAT;
            sakeField->mValue.mFloat = dbField.GetValue().GetFloat();
            break;
        case RLDB_VALUETYPE_ASCII_STRING: 
            sakeField->mType = SAKEFieldType_ASCII_STRING;
            sakeField->mValue.mAsciiString = (char*)dbField.GetValue().GetAsciiString();
            break;
        case RLDB_VALUETYPE_BINARY_DATA:  
            sakeField->mType = SAKEFieldType_BINARY_DATA;
            sakeField->mValue.mBinaryData.mValue = (gsi_u8*)dbField.GetValue().GetBinaryData().GetData();
            sakeField->mValue.mBinaryData.mLength = dbField.GetValue().GetBinaryData().GetSize();
            break;
        default:
            rlDbError("Unknown database value type (%d)", dbField.GetValue().GetType());
            return false;
    };

    return true;
}

static bool
sDbFieldsToSake(const rlDbField* fields,
                const unsigned numFields,
                SAKEField* sakeFields, 
                const unsigned maxSakeFields)
{
    rlAssert(fields && numFields && sakeFields && maxSakeFields && (maxSakeFields >= numFields));

    for(unsigned i = 0; (i < numFields) && (i < maxSakeFields); i++)
    {
        if(!sDbFieldToSake(fields[i], &sakeFields[i])) 
        {
            return false;
        }
    }
    return true;
}

#endif //RLDB_USE_SAKE

///////////////////////////////////////////////////////////////////////////////
// rlDbTask
///////////////////////////////////////////////////////////////////////////////
rlDbTask::rlDbTask()
: m_Progress(0)
#if RLDB_USE_PROXY
, m_RequestMsg(0)
#elif RLDB_USE_SAKE
, m_SakeReq(0)
#endif
{
}

rlDbTask::~rlDbTask()
{
    if(this->IsPending())
    {
        this->Finish(FINISH_CANCELED, RLDB_RESULT_CANCELLED);
    }
}

void
rlDbTask::Start()
{
    rlTaskDebug2("Starting");

    rlTask<rlDbClient>::Start();

#if RLDB_USE_SAKE
    m_SakeReq->Start();
#endif
}

void 
rlDbTask::Update(const unsigned timeStep)
{
    rlTask<rlDbClient>::Update(timeStep);

    if(!rlVerify(IsActive()))
    {
        return;
    }

#if RLDB_USE_PROXY
    u8* buf = m_Ctx->m_MsgBuf;
    const unsigned bufSize = m_Ctx->m_MsgBufSize;
    u32 msgSize = 0;        //Size of message data, not including 4-byte header
    int result = 0;

    //Estimate how much buffer we need for the request message.
    if(NULL == m_RequestMsg)
    {
        rlTaskError("Null request message; should have been set in derived task Configure()");
        Finish(FINISH_FAILED, RLDB_RESULT_UNKNOWN_ERROR);
        return;
    }

    if(0 == (msgSize = m_RequestMsg->GetExportByteSize()))
    {
        rlTaskError("Zero request msg size");
        Finish(FINISH_FAILED, RLDB_RESULT_MSG_WRITE_FAILED);
        return;
    }

    unsigned totalMsgSize = msgSize + sizeof(msgSize);

    if(bufSize < totalMsgSize)
    {
        rlTaskError("Msg buffer (%u bytes) is too small to send msg (%u bytes)", bufSize, totalMsgSize);
        Finish(FINISH_FAILED, RLDB_RESULT_OUT_OF_MEMORY);
        return;
    }

    //Export the message into our buffer after the 4-byte header.
    result = ExportRequestMsg(&buf[sizeof(msgSize)], msgSize, &msgSize);

    if(RLDB_RESULT_SUCCESS != result)
    {
        Finish(FINISH_FAILED, result);
        return;
    }

    msgSize = htonl(msgSize);
    buf[0] = ((char*)&msgSize)[0];
    buf[1] = ((char*)&msgSize)[1];
    buf[2] = ((char*)&msgSize)[2];
    buf[3] = ((char*)&msgSize)[3];

    //Send the message data
    if(NETTCP_RESULT_OK != netTcp::SendBuffer(m_Ctx->m_Socket, buf, totalMsgSize, 20))
    {
        Finish(FINISH_FAILED, RLDB_RESULT_SOCKET_ERROR);
        return;
    }

    rlTaskDebug3("Sent request msg (%u bytes)", totalMsgSize);

    while(1)
    {
        //Read the message size.
        if(NETTCP_RESULT_OK != netTcp::ReceiveBuffer(m_Ctx->m_Socket, &msgSize, sizeof(msgSize), 20))
        {
            rlTaskError("Failed to read message size");
            Finish(FINISH_FAILED, RLDB_RESULT_SOCKET_ERROR);
            return;
        }

        msgSize = ntohl(msgSize);

        rlTaskDebug3("Read reply msg size (%u bytes)", sizeof(msgSize));

        if(bufSize < msgSize)
        {
            rlTaskError("Msg buffer (%u bytes) is too small to receive msg (%u bytes)", bufSize, msgSize);
            Finish(FINISH_FAILED, RLDB_RESULT_OUT_OF_MEMORY);
            return;
        }

        //Read the message data
        if(NETTCP_RESULT_OK != netTcp::ReceiveBuffer(m_Ctx->m_Socket, buf, msgSize, 20))
        {
            rlTaskError("Failed to receive message buffer");
            Finish(FINISH_FAILED, RLDB_RESULT_SOCKET_ERROR);
            return;
        }

        rlTaskDebug3("Read reply msg (%u bytes)", msgSize);

        //If this is a progress message, read it now and update monitor var.
        unsigned msgId = 0;
        
        if(!netMessage::GetId(&msgId, buf, msgSize))
        {
           rlTaskError("Failed to get msgId from msg data");
           Finish(FINISH_FAILED, RLDB_RESULT_MSG_READ_FAILED);
           return;
        }

        if(rlDbMsgProgress::MSG_ID() == msgId)
        {
            rlDbMsgProgress progressMsg;

            if(!progressMsg.Import(buf, msgSize))
            {
               rlTaskError("Failed to import progress message");
               Finish(FINISH_FAILED, RLDB_RESULT_MSG_READ_FAILED);
               return;
            }

            rlTaskDebug3("Got progress message from proxy (%0.2f)", progressMsg.m_Progress);

            if(m_Progress)
            {
                sysInterlockedExchange((unsigned*)m_Progress, *(unsigned*)&progressMsg.m_Progress);
            }
        }
        else
        {
            break;
        }
    }

    //Import and apply the message
    result = ImportReplyMsg(buf, msgSize);
    
    Finish((result == RLDB_RESULT_SUCCESS) ? FINISH_SUCCEEDED : FINISH_FAILED, result);

#elif RLDB_USE_SAKE
    if(!m_SakeReqStatus.Pending())
    {
        Finish(m_SakeReqStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED,
               m_SakeReqStatus.GetResultCode());
    }
#endif
}

void 
rlDbTask::Finish(const FinishType finishType, const int resultCode)
{
    switch(finishType)
    {
    case FINISH_FAILED:
        rlTaskDebug2("%s (%d)", rlTaskBase::FinishString(finishType), resultCode);
        break;
    default:
        rlTaskDebug2("%s", rlTaskBase::FinishString(finishType));
    }    

    int result = resultCode;

#if RLDB_USE_SAKE
    result = sSakeResultToDb(result);
#endif

#if RLDB_USE_PROXY
    //If we had an exception on our socket, the socket will have problems until reopened.
    //Best to start clean if a request failed.
    if(RLDB_RESULT_SUCCESS != result)
    {
        m_Ctx->ResetServer();
    }
#endif

    rlTask<rlDbClient>::Finish(finishType, result);

#if RLDB_USE_SAKE
    RLDBTASK_FREE(m_SakeReq, 1);
#endif
}

#if RLDB_USE_PROXY
int
rlDbTask::ExportRequestMsg(u8* /*buf*/, const unsigned /*bufSize*/, unsigned* /*msgSize*/) const
{
    return RLDB_RESULT_UNKNOWN_ERROR;
}

int
rlDbTask::ImportReplyMsg(const u8* /*data*/, const unsigned /*size*/)
{
    return RLDB_RESULT_UNKNOWN_ERROR;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbCreateRecordTask
///////////////////////////////////////////////////////////////////////////////
rlDbCreateRecordTask::rlDbCreateRecordTask()
#if RLDB_USE_PROXY
: m_RecordId(0)
#elif RLDB_USE_SAKE
: m_SakeFields(0)
, m_NumFields(0)
#endif
{
}

rlDbCreateRecordTask::~rlDbCreateRecordTask()
{
#if RLDB_USE_SAKE
    RLDBTASK_FREE((SAKEField*)m_SakeFields, m_NumFields);
#endif
}

bool 
rlDbCreateRecordTask::Configure(rlDbClient* /*ctx*/,
                                const char* tableName,
                                const rlDbField* fields,
                                const unsigned numFields,
                                rlDbRecordId* recordId)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && ((fields && numFields) || ((fields == NULL) && (numFields == 0)))
                     && recordId))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    m_RecordId = recordId;
    m_MyMsg.ResetForExport(tableName, (rlDbField*)fields, numFields);
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    m_NumFields = numFields;

	if(m_NumFields > 0)
	{
		RLDBTASK_ALLOC(m_SakeFields, SAKEField, numFields);

		if(!sDbFieldsToSake(fields, numFields, (SAKEField*)m_SakeFields, numFields))
		{
			rlTaskError("sDbFieldsToSake failed");
			return false;
		}
	}

    RLDBTASK_SAKE_REQUEST(rlGamespySakeCreateRecordRequest);

    if(!rlTaskBase::Configure(req,
                               g_rlGamespy.GetSake(),
                               tableName,
                               (SAKEField*)m_SakeFields,
                               numFields,
                               recordId,
                               &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbCreateRecordTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbCreateRecordTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT(rlDbMsgCreateRecordReply);

    rlTaskDebug2("Success reply (record id=%d)", msg.m_RecordId);
    *m_RecordId = msg.m_RecordId;

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbUpdateRecordTask
///////////////////////////////////////////////////////////////////////////////
rlDbUpdateRecordTask::rlDbUpdateRecordTask()
#if RLDB_USE_SAKE
: m_SakeFields(0)
, m_NumFields(0)
#endif
{
}

rlDbUpdateRecordTask::~rlDbUpdateRecordTask()
{
#if RLDB_USE_SAKE
    RLDBTASK_FREE((SAKEField*)m_SakeFields, m_NumFields);
#endif
}

bool 
rlDbUpdateRecordTask::Configure(rlDbClient* /*ctx*/,
                                      const char* tableName,
                                      const rlDbRecordId recordId,
                                      const rlDbField* fields,
                                      const unsigned numFields)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && fields 
                     && numFields 
                     && recordId))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    m_MyMsg.ResetForExport(tableName, recordId, (rlDbField*)fields, numFields);
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
     RLDBTASK_ALLOC(m_SakeFields, SAKEField, numFields);

    if(!sDbFieldsToSake(fields, numFields, (SAKEField*)m_SakeFields, numFields))
    {
        rlTaskError("sDbFieldsToSake failed");
        return false;
    }

    m_NumFields = numFields;

    RLDBTASK_SAKE_REQUEST(rlGamespySakeUpdateRecordRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        tableName,
                        (SAKEField*)m_SakeFields,
                        m_NumFields,
                        recordId,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbUpdateRecordTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbUpdateRecordTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT(rlDbMsgUpdateRecordReply);

    rlTaskDebug2("Success reply");

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbDeleteRecordTask
///////////////////////////////////////////////////////////////////////////////
rlDbDeleteRecordTask::rlDbDeleteRecordTask()
{
}

rlDbDeleteRecordTask::~rlDbDeleteRecordTask()
{
}

bool 
rlDbDeleteRecordTask::Configure(rlDbClient* /*ctx*/,
                                const char* tableName,
                                const rlDbRecordId recordId)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && recordId))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    m_MyMsg.Reset(tableName, recordId);
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    RLDBTASK_SAKE_REQUEST(rlGamespySakeDeleteRecordRequest);

    if(!rlTaskBase::Configure(req,
                                g_rlGamespy.GetSake(),
                                tableName,
                                recordId,
                                &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbDeleteRecordTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbDeleteRecordTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT(rlDbMsgDeleteRecordReply);

    rlTaskDebug2("Success reply");

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbRateRecordTask
///////////////////////////////////////////////////////////////////////////////
rlDbRateRecordTask::rlDbRateRecordTask()
{
}

rlDbRateRecordTask::~rlDbRateRecordTask()
{
}

bool 
rlDbRateRecordTask::Configure(rlDbClient* /*ctx*/,
                              const char* tableName,
                              const rlDbRecordId recordId,
                              const unsigned curNumRatings,
                              const float curAvgRating,
                              const rlDbRating rating,
                              unsigned* numRatings,
                              float* avgRating)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && recordId))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    m_NumRatings = numRatings;
    m_AvgRating = avgRating;
    m_MyMsg.Reset(tableName, recordId, curNumRatings, curAvgRating, rating);
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    RLDBTASK_SAKE_REQUEST(rlGamespySakeRateRecordRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        tableName,
                        recordId,
                        curNumRatings,
                        curAvgRating,
                        rating,
                        numRatings,
                        avgRating,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbRateRecordTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbRateRecordTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT(rlDbMsgRateRecordReply);

    rlTaskDebug2("Success reply (numRatings=%u  avgRating=%0.2f", msg.m_NumRatings, msg.m_AvgRating);
    if(m_NumRatings) *m_NumRatings = msg.m_NumRatings;
    if(m_AvgRating) *m_AvgRating = msg.m_AvgRating;

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbGetNumRecordsTask
///////////////////////////////////////////////////////////////////////////////
rlDbGetNumRecordsTask::rlDbGetNumRecordsTask()
#if RLDB_USE_PROXY
: m_NumRecords(0)
#endif
{
}

rlDbGetNumRecordsTask::~rlDbGetNumRecordsTask()
{
}

bool 
rlDbGetNumRecordsTask::Configure(rlDbClient* /*ctx*/,
                                 const char* tableName,
                                 const char* filter,
                                 unsigned* numRecords)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && numRecords))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    if(filter && (strlen(filter) >= RLDB_MAX_FILTER_CHARS))
    {
        rlTaskError("Filter string is too long (%u chars, %u max): %s",
            strlen(filter),
            RLDB_MAX_FILTER_CHARS,
            filter);
        return false;
    }

    m_NumRecords = numRecords;   
    m_MyMsg.Reset(tableName, filter ? filter : "");
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    RLDBTASK_SAKE_REQUEST(rlGamespySakeGetNumRecordsRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        tableName,
                        filter ? filter : "",
                        numRecords,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbGetNumRecordsTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbGetNumRecordsTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT(rlDbMsgGetNumRecordsReply);

    rlTaskDebug2("Success reply (%u records)", msg.m_NumRecords);
    *m_NumRecords = msg.m_NumRecords;

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbReadRecordsTask
///////////////////////////////////////////////////////////////////////////////
rlDbReadRecordsTask::rlDbReadRecordsTask()
#if RLDB_USE_PROXY
: m_Results(0)
#endif
{
}

rlDbReadRecordsTask::~rlDbReadRecordsTask()
{
}

bool 
rlDbReadRecordsTask::Configure(rlDbClient* /*ctx*/,
                                     const char* tableName,
                                     const char* filter,
                                     const char* sort,
                                     const unsigned startingRank,
                                     const char* postSortFilter,
                                     const unsigned radius,
                                     const bool cacheResults,
                                     const unsigned maxRecords,
                                     rlDbReadResults* results)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && (startingRank > 0) 
                     && results 
                     && maxRecords 
                     && (maxRecords <= results->GetMaxRecords())))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    if(sort && (strlen(sort) >= RLDB_MAX_SORT_CHARS))
    {
        rlTaskError("Sort string is too long (%u chars, %u max): %s",
            strlen(sort),
            RLDB_MAX_SORT_CHARS,
            sort);
        return false;
    }

    if(postSortFilter && (strlen(postSortFilter) >= RLDB_MAX_POSTSORT_FILTER_CHARS))
    {
        rlTaskError("Post-sort string is too long (%u chars, %u max): %s",
            strlen(postSortFilter),
            RLDB_MAX_POSTSORT_FILTER_CHARS,
            postSortFilter);
        return false;
    }

    if(filter && (strlen(filter) >= RLDB_MAX_FILTER_CHARS))
    {
        rlTaskError("Filter string is too long (%u chars, %u max): %s",
            strlen(filter),
            RLDB_MAX_FILTER_CHARS,
            filter);
        return false;
    }

    m_Results = results;
    m_MyMsg.ResetForExport(tableName, 
                  filter ? filter : "", 
                  sort ? sort : "",
                  startingRank,
                  postSortFilter ? postSortFilter : "",
                  radius,
                  cacheResults,
                  results->GetSchema(),
                  results->GetMaxRecords());
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    RLDBTASK_SAKE_REQUEST(rlGamespySakeReadDbRecordsRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        tableName,
                        filter ? filter : "",
                        sort ? sort : "",
                        startingRank,
                        postSortFilter ? postSortFilter : "",
                        radius,
                        maxRecords,
                        cacheResults,
                        results,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbReadRecordsTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbReadRecordsTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT_RECORDS();

    rlTaskDebug2("Success reply (%u records)", m_Results->GetNumRecords());

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbReadRecordByIdTask
///////////////////////////////////////////////////////////////////////////////
rlDbReadRecordByIdTask::rlDbReadRecordByIdTask()
#if RLDB_USE_PROXY
: m_Results(0)
#endif
{
}

rlDbReadRecordByIdTask::~rlDbReadRecordByIdTask()
{
}

bool 
rlDbReadRecordByIdTask::Configure(rlDbClient* /*ctx*/,
                                  const char* tableName,
                                  const rlDbRecordId recordId,
                                  rlDbReadResults* results)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && recordId 
                     && results))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    m_Results = results;

    formatf(m_Filter, 
            sizeof(m_Filter), 
            "%s = %d", 
            rlDbGetFieldName(RLDB_FIELD_RECORD_ID),
            recordId);

    m_MyMsg.ResetForExport(tableName, 
                           m_Filter, 
                           "",
                           1,
                           "",
                           0,
                           false,
                           results->GetSchema(),
                           1);
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    formatf(m_Filter, 
            sizeof(m_Filter), 
            "%s = %d", 
            rlDbGetFieldName(RLDB_FIELD_RECORD_ID),
            recordId);

    RLDBTASK_SAKE_REQUEST(rlGamespySakeReadDbRecordsRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        tableName,
                        m_Filter,
                        "",
                        1,
                        "",
                        0,
                        results->GetMaxRecords(),
                        false,
                        results,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbReadRecordByIdTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbReadRecordByIdTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT_RECORDS();

    rlTaskDebug2("Success reply (%u records)", m_Results->GetNumRecords());

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbReadRandomRecordTask
///////////////////////////////////////////////////////////////////////////////
rlDbReadRandomRecordTask::rlDbReadRandomRecordTask()
#if RLDB_USE_PROXY
: m_Results(0)
#endif
{
}

rlDbReadRandomRecordTask::~rlDbReadRandomRecordTask()
{
}

bool 
rlDbReadRandomRecordTask::Configure(rlDbClient* /*ctx*/,
                                    const char* tableName,
                                    const char* filter,
                                    rlDbReadResults* results)
{
    if(!rlVerify(tableName 
                     && (strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS)
                     && filter 
                     && results))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    if(filter && strlen(filter) >= RLDB_MAX_FILTER_CHARS)
    {
        rlTaskError("Filter string is too long (%u chars, %u max): %s",
            strlen(filter),
            RLDB_MAX_FILTER_CHARS,
            filter);
        return false;
    }

    m_Results = results;

    m_MyMsg.ResetForExport(tableName, 
                           filter ? filter : "",
                           results->GetSchema());
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    RLDBTASK_SAKE_REQUEST(rlGamespySakeReadRandomDbRecordRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        tableName,
                        filter ? filter : "",
                        //results->GetSchema().GetFieldNames(),,
                        results,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbReadRandomRecordTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbReadRandomRecordTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT_RECORDS();

    rlTaskDebug2("Success reply (%u records)", m_Results->GetNumRecords());

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbUploadFileTask
///////////////////////////////////////////////////////////////////////////////
rlDbUploadFileTask::rlDbUploadFileTask()
#if RLDB_USE_PROXY
: m_FileId(0)
#endif
{
}

rlDbUploadFileTask::~rlDbUploadFileTask()
{
}

bool 
rlDbUploadFileTask::Configure(rlDbClient* /*ctx*/,
                                    const void* data,
                                    const unsigned size,
                                    float* progress,
                                    rlDbFileId* fileId)
{
    if(!rlVerify(data && size && fileId))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    m_FileId = fileId;
    m_Progress = progress;

    m_MyMsg.Reset((u8*)data, size);
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    RLDBTASK_SAKE_REQUEST(rlGamespySakeUploadRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        data,
                        size,
                        fileId,
                        progress,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbUploadFileTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbUploadFileTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    RLDBTASK_IMPORT(rlDbMsgUploadFileReply);

    rlTaskDebug2("Success reply (fileId=%d)", msg.m_FileId);
    *m_FileId = msg.m_FileId;

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

///////////////////////////////////////////////////////////////////////////////
// rlDbDownloadFileTask
///////////////////////////////////////////////////////////////////////////////
rlDbDownloadFileTask::rlDbDownloadFileTask()
#if RLDB_USE_PROXY
: m_Buf(0)
, m_BufSize(0)
, m_FileSize(0)
#endif
{
}

rlDbDownloadFileTask::~rlDbDownloadFileTask()
{
}

bool 
rlDbDownloadFileTask::Configure(rlDbClient* /*ctx*/,
                                      const rlDbFileId fileId,
                                      u8* buf,
                                      const unsigned bufSize,
                                      float* progress,
                                      unsigned* fileSize)
{
    if(!rlVerify(buf && bufSize && fileId))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

#if RLDB_USE_PROXY
    m_Buf = buf;
    m_BufSize = bufSize;
    m_FileSize = fileSize;
    m_Progress = progress;

    m_MyMsg.Reset(fileId, bufSize);
    m_RequestMsg = &m_MyMsg;

#elif RLDB_USE_SAKE
    RLDBTASK_SAKE_REQUEST(rlGamespySakeDownloadRequest);

    if(!rlTaskBase::Configure(req,
                        g_rlGamespy.GetSake(),
                        fileId,
                        bufSize,
                        buf,
                        fileSize,
                        progress,
                        &m_SakeReqStatus))
    {
        rlTaskError("Failed to Configure request");
        Finish(FINISH_FAILED, m_SakeReqStatus.GetResultCode());
        return false;
    }
#endif

    return true;
}

#if RLDB_USE_PROXY
int
rlDbDownloadFileTask::ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const
{
    RLDBTASK_EXPORT(m_MyMsg);
}

int
rlDbDownloadFileTask::ImportReplyMsg(const u8* data, const unsigned size)
{
    unsigned msgId = 0;
    
    if(!netMessage::GetId(&msgId, data, size))
    {
       rlTaskError("Failed to get msgId from msg data");
       return RLDB_RESULT_MSG_READ_FAILED;
    }
    
    if(rlDbMsgDownloadFileReply::MSG_ID() != msgId)
    {
       rlTaskError("Received msg other than expected reply");
       return RLDB_RESULT_MSG_READ_FAILED;
    }
    
    rlDbMsgDownloadFileReply msg(m_Buf, m_BufSize);
    if(!msg.Import(data, size))
    {
       rlTaskError("Failed to import message");
       return RLDB_RESULT_MSG_READ_FAILED;
    }

    if(!msg.m_Success)
    {
        rlTaskDebug2("Failed reply (result code %d)", msg.m_ResultCode);
        return msg.m_ResultCode;
    }
    
    rlTaskDebug2("Success reply (fileSize=%u bytes)", msg.m_NumBytesDownloaded);
    *m_FileSize = msg.m_NumBytesDownloaded;

    return RLDB_RESULT_SUCCESS;
}
#endif //RLDB_USE_PROXY

}; //namespace rage
