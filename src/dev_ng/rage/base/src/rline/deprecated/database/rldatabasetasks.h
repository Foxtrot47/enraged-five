// 
// rline/rldatabasetasks.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLDBTASKS_H
#define RLINE_RLDBTASKS_H

#include "rldatabase.h"
#include "rltask.h"
#include "atl/inlist.h"
#include "net/status.h"

#if RLDB_USE_PROXY
#include "rldatabasemsgs.h"
#endif

namespace rage
{

///////////////////////////////////////////////////////////////////////////////
//Convenience macro for declaring virtual methods that derived tasks override.
///////////////////////////////////////////////////////////////////////////////
#if RLDB_USE_PROXY
#define RL_DECL_DBTASK(taskName) \
    RL_TASK_DECL(taskName) \
    virtual int ExportRequestMsg(u8* buf, const unsigned bufSize, unsigned* msgSize) const; \
    virtual int ImportReplyMsg(const u8* data, const unsigned size);
#else
#define RL_DECL_DBTASK(taskName) \
    RL_TASK_DECL(taskName)
#endif

///////////////////////////////////////////////////////////////////////////////
// rlDbTask
// Base class for all tasks an rlDbClient will perform.
///////////////////////////////////////////////////////////////////////////////
class rlDbTask : public rlTask<class rlDbClient>
{
public:
    RL_DECL_DBTASK(rlDbTask);
    RL_TASK_USE_CHANNEL(rline_db);

    rlDbTask();
    virtual ~rlDbTask();

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    float* m_Progress;

#if RLDB_USE_PROXY
    //Set by derived classes.  We use this to simplify getting max exported request msg size.
    rlDbMsg* m_RequestMsg;

#elif RLDB_USE_SAKE
    //Created by derived classes. Our Update() monitors it for completion.
    class rlGamespySakeRequest* m_SakeReq;
    netStatus m_SakeReqStatus;
#endif

private:
    friend class rlDbClient;
    inlist_node<rlDbTask> m_ListLink;
};

///////////////////////////////////////////////////////////////////////////////
// rlDbCreateRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlDbCreateRecordTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbCreateRecordTask);

    rlDbCreateRecordTask();
    virtual ~rlDbCreateRecordTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const rlDbField* fields,
                   const unsigned numFields,
                   rlDbRecordId* recordId);
private:
#if RLDB_USE_PROXY
    rlDbRecordId* m_RecordId;
    rlDbMsgCreateRecordRequest m_MyMsg;

#elif RLDB_USE_SAKE
    void* m_SakeFields;
    unsigned m_NumFields;
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbUpdateRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlDbUpdateRecordTask : public rlDbTask 
{
public:
    RL_DECL_DBTASK(rlDbUpdateRecordTask);

    rlDbUpdateRecordTask();
    virtual ~rlDbUpdateRecordTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const rlDbRecordId recordId,
                   const rlDbField* fields,
                   const unsigned numFields);

private:
#if RLDB_USE_PROXY
    rlDbMsgUpdateRecordRequest m_MyMsg;

#elif RLDB_USE_SAKE
    void* m_SakeFields;
    unsigned m_NumFields;
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbDeleteRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlDbDeleteRecordTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbDeleteRecordTask);

    rlDbDeleteRecordTask();
    virtual ~rlDbDeleteRecordTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const rlDbRecordId recordId);

private:
#if RLDB_USE_PROXY
    rlDbMsgDeleteRecordRequest m_MyMsg;
#endif
};
 
///////////////////////////////////////////////////////////////////////////////
// rlDbRateRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlDbRateRecordTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbRateRecordTask);

    rlDbRateRecordTask();
    virtual ~rlDbRateRecordTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const rlDbRecordId recordId,
                   const unsigned curNumRatings,
                   const float curAvgRating,
                   const rlDbRating rating,
                   unsigned* numRatings,
                   float* avgRating);

private:
#if RLDB_USE_PROXY
    unsigned* m_NumRatings;
    float* m_AvgRating;
    rlDbMsgRateRecordRequest m_MyMsg;
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbGetNumRecordsTask
///////////////////////////////////////////////////////////////////////////////
class rlDbGetNumRecordsTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbGetNumRecordsTask);

    rlDbGetNumRecordsTask();
    virtual ~rlDbGetNumRecordsTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const char* filter,
                   unsigned* numRecords);

private:
#if RLDB_USE_PROXY
    unsigned*   m_NumRecords;
    rlDbMsgGetNumRecordsRequest m_MyMsg;
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbReadRecordsTask
///////////////////////////////////////////////////////////////////////////////
class rlDbReadRecordsTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbReadRecordsTask);

    rlDbReadRecordsTask();
    virtual ~rlDbReadRecordsTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const char* filter,
                   const char* sort,
                   const unsigned startingRank,
                   const char* postSortFilter,
                   const unsigned radius,
                   const bool cacheResults,
                   const unsigned maxRecords,
                   rlDbReadResults* results);

private:
#if RLDB_USE_PROXY
    rlDbReadResults* m_Results;
    rlDbMsgReadRecordsRequest m_MyMsg;
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbReadRecordsByIdTask
///////////////////////////////////////////////////////////////////////////////
class rlDbReadRecordByIdTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbReadRecordByIdTask);

    rlDbReadRecordByIdTask();
    virtual ~rlDbReadRecordByIdTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const rlDbRecordId recordId,
                   rlDbReadResults* results);

private:
#if RLDB_USE_PROXY
    rlDbReadResults* m_Results;
    char m_Filter[128];
    rlDbMsgReadRecordsRequest m_MyMsg;
#endif

#if RLDB_USE_SAKE
    char m_Filter[128];
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbReadRandomRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlDbReadRandomRecordTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbReadRandomRecordTask);

    rlDbReadRandomRecordTask();
    virtual ~rlDbReadRandomRecordTask();

    bool Configure(rlDbClient* ctx,
                   const char* tableName,
                   const char* filter,
                   rlDbReadResults* results);

private:
#if RLDB_USE_PROXY
    rlDbReadResults* m_Results;
    rlDbMsgReadRandomRecordRequest m_MyMsg;
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbUploadFileTask
///////////////////////////////////////////////////////////////////////////////
class rlDbUploadFileTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbUploadFileTask);

    rlDbUploadFileTask();
    virtual ~rlDbUploadFileTask();

    bool Configure(rlDbClient* ctx,
                   const void* data,
                   const unsigned size,
                   float* progress,
                   rlDbFileId* fileId);

private:
#if RLDB_USE_PROXY
    rlDbFileId* m_FileId;
    rlDbMsgUploadFileRequest m_MyMsg;
#endif
};

///////////////////////////////////////////////////////////////////////////////
// rlDbDownloadFileTask
///////////////////////////////////////////////////////////////////////////////
class rlDbDownloadFileTask : public rlDbTask
{
public:
    RL_DECL_DBTASK(rlDbDownloadFileTask);

    rlDbDownloadFileTask();
    virtual ~rlDbDownloadFileTask();

    bool Configure(rlDbClient* ctx,
                   const rlDbFileId fileId,
                   u8* buf,
                   const unsigned bufSize,
                   float* progress,
                   unsigned* fileSize);

private:
#if RLDB_USE_PROXY
    u8* m_Buf;
    unsigned m_BufSize;
    unsigned* m_FileSize;
    rlDbMsgDownloadFileRequest m_MyMsg;
#endif
};

}; //namespace rage

#endif //RLINE_RLDBTASKS_H
