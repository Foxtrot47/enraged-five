// 
// rline/rldatabasemsgs.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLDBMSGS_H
#define RLINE_RLDBMSGS_H

#include "rldatabase.h"
#include "net/message.h"
#include "string/string.h"
#include "system/memory.h"

namespace rage
{

enum rlDbMsgIds
{
    RLDBMSG_PROGRESS = 1000,

    RLDBMSG_CREATE_RECORD_REQUEST,
    RLDBMSG_CREATE_RECORD_REPLY,

    RLDBMSG_UPDATE_RECORD_REQUEST,
    RLDBMSG_UPDATE_RECORD_REPLY,

    RLDBMSG_DELETE_RECORD_REQUEST,
    RLDBMSG_DELETE_RECORD_REPLY,

    RLDBMSG_RATE_RECORD_REQUEST,
    RLDBMSG_RATE_RECORD_REPLY,

    RLDBMSG_GET_NUM_RECORDS_REQUEST,
    RLDBMSG_GET_NUM_RECORDS_REPLY,

    RLDBMSG_READ_RECORDS_REQUEST,
    RLDBMSG_READ_RANDOM_RECORD_REQUEST,
    RLDBMSG_READ_RECORDS_REPLY,

    RLDBMSG_UPLOAD_FILE_REQUEST,
    RLDBMSG_UPLOAD_FILE_REPLY,

    RLDBMSG_DOWNLOAD_FILE_REQUEST,
    RLDBMSG_DOWNLOAD_FILE_REPLY,
};

#define RLDBMSG_SER_ARRAY(data, size, type) \
    if(size) \
    { \
        if(!data) \
        { \
            data = RL_NEW_ARRAY(rlDbMsg, type, size); \
            if(!data) return false; \
        } \
        for(unsigned i = 0; i < size; i++) \
        { \
            if(!bb.SerSer(data[i])) return false; \
        } \
    }

////////////////////////////////////////////////////////////////////////////////
// Database msg base class
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsg
{
    rlDbMsg() {};
    virtual ~rlDbMsg() {};

    //Estimates buffer size required to export the message. This is necessary
    //because the DB msgs can vary so wildly in size.
    virtual unsigned GetExportByteSize() const = 0;
};

////////////////////////////////////////////////////////////////////////////////
// Progress (feedback for requests that can take a long time)
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgProgress : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgProgress, RLDBMSG_PROGRESS);

    rlDbMsgProgress()
    : m_Progress(0)
    {
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    void Reset(const float progress)
    {
        m_Progress = progress;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerFloat(msg.m_Progress);
    }
    
    float m_Progress;
};

////////////////////////////////////////////////////////////////////////////////
// Create a record
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgCreateRecordRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgCreateRecordRequest, RLDBMSG_CREATE_RECORD_REQUEST);

    rlDbMsgCreateRecordRequest()
    : m_NumFields(0)
    , m_Fields(0)
    , m_OwnMem(true)
    {
    }

    virtual ~rlDbMsgCreateRecordRequest()
    {
        Clear();
    }

    void Clear()
    {
        if(m_OwnMem)
        {
            rlDelete(m_Fields, m_NumFields);
            m_OwnMem = false;
        }
 
        m_NumFields = 0;
        m_Fields = 0;
        sysMemSet(m_TableName, 0, sizeof(m_TableName));
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    void ResetForExport(const char* tableName,
                        rlDbField* fields,
                        const unsigned numFields)
    {
        Clear();

        safecpy(m_TableName, tableName, sizeof(m_TableName));
        m_Fields = fields;
        m_NumFields = numFields;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!bb.SerStr(msg.m_TableName, sizeof(msg.m_TableName))) return false;
        if(!bb.SerUns(msg.m_NumFields, datBitsNeeded<RLDB_MAX_FIELDS_PER_TABLE>::COUNT)) return false;

        if(msg.m_NumFields)
        {
            if(!msg.m_Fields)
            {
                msg.m_OwnMem = true;

                msg.m_Fields = RL_NEW_ARRAY(rlDbMsgCreateRecordRequest, rlDbField, msg.m_NumFields);
                if(!msg.m_Fields) return false;
            }

            for(unsigned i = 0; i < msg.m_NumFields; i++)
            {
                if(!bb.SerSer(msg.m_Fields[i])) return false;
            }
        }

        return true;
    }
    
    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];
    mutable unsigned m_NumFields;
    mutable rlDbField* m_Fields;
    mutable bool m_OwnMem;
};

struct rlDbMsgCreateRecordReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgCreateRecordReply, RLDBMSG_CREATE_RECORD_REPLY);

    void Reset(const bool success,
               const int resultCode,
               const rlDbRecordId recordId)
    {
        m_Success = success;       
        m_ResultCode = resultCode;
        m_RecordId = recordId;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!bb.SerBool(msg.m_Success) ||
           !bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1))
        {
             return false;
        }
        if(msg.m_Success) return bb.SerInt(msg.m_RecordId, 32);
        return true;
    }

    bool m_Success;
    int m_ResultCode;
    rlDbRecordId m_RecordId;
};

////////////////////////////////////////////////////////////////////////////////
// Update a record
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgUpdateRecordRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgUpdateRecordRequest, RLDBMSG_UPDATE_RECORD_REQUEST);

    rlDbMsgUpdateRecordRequest()
    : m_RecordId(RLDB_INVALID_RECORDID)
    , m_NumFields(0)
    , m_Fields(0)
    , m_OwnMem(true)
    {
    }

    virtual ~rlDbMsgUpdateRecordRequest()
    {
        Clear();
    }

    void Clear()
    {
        if(m_OwnMem)
        {
            rlDelete(m_Fields, m_NumFields);
            m_OwnMem = false;
        }

        m_NumFields = 0;
        m_Fields = 0;
        sysMemSet(m_TableName, 0, sizeof(m_TableName));
        m_RecordId = RLDB_INVALID_RECORDID;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    void ResetForExport(const char* tableName,
                        const rlDbRecordId recordId,
                        rlDbField* fields,
                        const unsigned numFields)
    {
        Clear();

        safecpy(m_TableName, tableName, sizeof(m_TableName));
        m_Fields = fields;
        m_NumFields = numFields;
        m_RecordId = recordId;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!bb.SerStr(msg.m_TableName, sizeof(msg.m_TableName))) return false;
        if(!bb.SerInt(msg.m_RecordId, 32)) return false;
        if(!bb.SerUns(msg.m_NumFields, datBitsNeeded<RLDB_MAX_FIELDS_PER_TABLE>::COUNT)) return false;

        if(msg.m_NumFields)
        {
            if(!msg.m_Fields)
            {
                msg.m_OwnMem = true;

                msg.m_Fields = RL_NEW_ARRAY(rlDbMsgCreateRecordRequest, rlDbField, msg.m_NumFields);
                if(!msg.m_Fields) return false;
            }

            for(unsigned i = 0; i < msg.m_NumFields; i++)
            {
                if(!bb.SerSer(msg.m_Fields[i])) return false;
            }
        }

        return true;
    }

    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];
    rlDbRecordId m_RecordId;
    mutable unsigned m_NumFields;
    mutable rlDbField* m_Fields;
    mutable bool m_OwnMem;
};

struct rlDbMsgUpdateRecordReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgUpdateRecordReply, RLDBMSG_UPDATE_RECORD_REPLY);

    void Reset(const bool success,
               const int resultCode)
    {
        m_Success = success;       
        m_ResultCode = resultCode;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!bb.SerBool(msg.m_Success) ||
           !bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1))
        {
             return false;
        }
        return true;
    }

    bool m_Success;
    int m_ResultCode;
};

////////////////////////////////////////////////////////////////////////////////
// Delete a record
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgDeleteRecordRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgDeleteRecordRequest, RLDBMSG_DELETE_RECORD_REQUEST);

    void Reset(const char* tableName, 
               const rlDbRecordId recordId)
    {
        safecpy(m_TableName, tableName, sizeof(m_TableName));
        m_RecordId = recordId;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerStr(msg.m_TableName, sizeof(msg.m_TableName)) &&
               bb.SerInt(msg.m_RecordId, 32);
    }

    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];
    rlDbRecordId m_RecordId;
};

struct rlDbMsgDeleteRecordReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgDeleteRecordReply, RLDBMSG_DELETE_RECORD_REPLY);

    void Reset(const bool success,
               const int resultCode)
    {
        m_Success = success;
        m_ResultCode = resultCode;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerBool(msg.m_Success) &&
               bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1);
    }

    bool m_Success;
    int m_ResultCode;
};

////////////////////////////////////////////////////////////////////////////////
// Rate a record
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgRateRecordRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgRateRecordRequest, RLDBMSG_RATE_RECORD_REQUEST);

    void Reset(const char* tableName, 
               const rlDbRecordId recordId,
               const unsigned curNumRatings,
               const float curAvgRating,
               const rlDbRating rating)
    {
        safecpy(m_TableName, tableName, sizeof(m_TableName));
        m_RecordId = recordId;
        m_CurNumRatings = curNumRatings;
        m_CurAvgRating = curAvgRating;
        m_Rating = rating;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerStr(msg.m_TableName, sizeof(msg.m_TableName)) &&
               bb.SerInt(msg.m_RecordId, 32) &&
               bb.SerUns(msg.m_CurNumRatings, 32) &&
               bb.SerFloat(msg.m_CurAvgRating) &&
               bb.SerUns(msg.m_Rating, 8);
    }

    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];
    rlDbRecordId m_RecordId;
    unsigned m_CurNumRatings;
    float m_CurAvgRating;
    rlDbRating m_Rating;
};

struct rlDbMsgRateRecordReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgRateRecordReply, RLDBMSG_RATE_RECORD_REPLY);

    void Reset(const bool success,
               const int resultCode,
               const unsigned numRatings = 0,
               const float averageRating = 0.0f)
    {
        m_Success = success;
        m_ResultCode = resultCode;
        m_NumRatings = numRatings;
        m_AvgRating = averageRating;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerBool(msg.m_Success)
            && bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1)
            && bb.SerUns(msg.m_NumRatings, 32)
            && bb.SerFloat(msg.m_AvgRating);
    }

    bool m_Success;
    int m_ResultCode;
    unsigned m_NumRatings;
    float m_AvgRating;
};

////////////////////////////////////////////////////////////////////////////////
// Get number of records that pass a filter
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgGetNumRecordsRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgGetNumRecordsRequest, RLDBMSG_GET_NUM_RECORDS_REQUEST);

    void Reset(const char* tableName,
               const char* filter)
    {
        rlAssert(strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS);
        rlAssert(strlen(filter) < RLDB_MAX_FILTER_CHARS);
        
        safecpy(m_TableName, tableName, sizeof(m_TableName));
        safecpy(m_Filter, filter, sizeof(m_Filter));
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerStr(msg.m_TableName, sizeof(msg.m_TableName)) &&
               bb.SerStr(msg.m_Filter, sizeof(msg.m_Filter));
    }

    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];   
    char m_Filter[RLDB_MAX_FILTER_CHARS];
};

struct rlDbMsgGetNumRecordsReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgGetNumRecordsReply, RLDBMSG_GET_NUM_RECORDS_REPLY);

    void Reset(const bool success, 
               const int resultCode,
               const unsigned numRecords = 0)
    {
        m_Success = success;
        m_ResultCode = resultCode;
        m_NumRecords = numRecords;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerBool(msg.m_Success) &&
               bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1) &&
               bb.SerUns(msg.m_NumRecords, 32);
    }

    bool m_Success;
    int m_ResultCode;
    unsigned m_NumRecords;
};

////////////////////////////////////////////////////////////////////////////////
// Read records
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgReadRecordsRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgReadRecordsRequest, RLDBMSG_READ_RECORDS_REQUEST);

    rlDbMsgReadRecordsRequest()
    : m_OwnMem(false)
    , m_Schema(0)
    , m_StartingRank(0)
    , m_Radius(0)
    , m_CacheResults(true)
    , m_MaxRecords(0)
    {
    }

    virtual ~rlDbMsgReadRecordsRequest()
    {
        if(m_OwnMem)
        {
            rlDelete(m_Schema);
            m_Schema = 0;
        }
    }

    void ResetForExport(const char* tableName,
                        const char* filter,
                        const char* sort,
                        const unsigned startingRank,
                        const char* postSortFilter,
                        const unsigned radius,
                        const bool cacheResults,
                        const rlDbSchemaBase* schema,
                        const unsigned maxRecords)
    {
        rlAssert(strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS);
        rlAssert(strlen(filter) < RLDB_MAX_FILTER_CHARS);
        rlAssert(strlen(sort) < RLDB_MAX_SORT_CHARS);
        
        safecpy(m_TableName, tableName, sizeof(m_TableName));
        safecpy(m_Filter, filter, sizeof(m_Filter));
        safecpy(m_Sort, sort, sizeof(m_Sort));
        safecpy(m_PostSortFilter, postSortFilter, sizeof(m_PostSortFilter));

        m_StartingRank = startingRank;

        m_Radius = radius;
        m_CacheResults = cacheResults;

        m_Schema = (rlDbSchemaBase*)schema;
        m_MaxRecords = maxRecords;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!(bb.SerStr(msg.m_TableName, sizeof(msg.m_TableName)) &&
             bb.SerStr(msg.m_Filter, sizeof(msg.m_Filter)) &&
             bb.SerUns(msg.m_StartingRank, 32) &&
             bb.SerStr(msg.m_Sort, sizeof(msg.m_Sort)) &&
             bb.SerStr(msg.m_PostSortFilter, sizeof(msg.m_PostSortFilter)) &&
             bb.SerUns(msg.m_Radius, 32) &&
             bb.SerBool(msg.m_CacheResults) &&
             bb.SerUns(msg.m_MaxRecords, 32)))
        {
            return false;
        }

        if(!msg.m_Schema) 
        {
            msg.m_Schema = RL_NEW(rlDbMsgReadRecordsRequest, rlDbDynSchema);
            if(!msg.m_Schema) return false;
            msg.m_OwnMem = true;
        }

        if(!bb.SerSer(*msg.m_Schema)) return false;

        return true;
    }

    mutable bool m_OwnMem;
    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];
    char m_Filter[RLDB_MAX_FILTER_CHARS];
    unsigned m_StartingRank;
    char m_Sort[RLDB_MAX_SORT_CHARS];
    char m_PostSortFilter[RLDB_MAX_POSTSORT_FILTER_CHARS];
    unsigned m_Radius;
    bool m_CacheResults;
    mutable rlDbSchemaBase* m_Schema;
    mutable unsigned m_MaxRecords;
};

struct rlDbMsgReadRecordsReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgReadRecordsReply, RLDBMSG_READ_RECORDS_REPLY);

    rlDbMsgReadRecordsReply()
    : m_Success(false)
    , m_Results(0)
    {
    }

    virtual ~rlDbMsgReadRecordsReply()
    {
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    //Call this on sender side to set the results to export, before the Export call
    void ResetForExport(const bool success, 
                        const int resultCode,
                        rlDbReadResults* results)
    {
        m_Success = success;
        m_ResultCode = resultCode;
        m_Results = results;
    }

    //Call this on receiver side to set the results to import, before the import call.
    //There should never be a time when you are importing results whose schema or 
    //max records you don't know.
    void ResetForImport(rlDbReadResults* results)
    {
        m_Results = results;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!rlVerify(msg.m_Results)) return false;

        if(!bb.SerBool(msg.m_Success) ||
           !bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1))
        {
             return false;
        }

        if(msg.m_Success)
        {
            if(!bb.SerSer(*msg.m_Results)) return false;
        }
        return true;
    }

    mutable rlDbReadResults* m_Results;
    int m_ResultCode;
    bool m_Success;
};

////////////////////////////////////////////////////////////////////////////////
// Read a random record
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgReadRandomRecordRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgReadRandomRecordRequest, RLDBMSG_READ_RANDOM_RECORD_REQUEST);

    rlDbMsgReadRandomRecordRequest()
    : m_OwnMem(false)
    , m_Schema(0)
    {
    }

    ~rlDbMsgReadRandomRecordRequest()
    {
        if(m_OwnMem)
        {
            rlDelete(m_Schema);
            m_Schema = 0;
        }
    }

    void ResetForExport(const char* tableName,
                        const char* filter,
                        const rlDbSchemaBase* schema)
    {
        rlAssert(strlen(tableName) < RLDB_MAX_TABLE_NAME_CHARS);
        rlAssert(strlen(filter) < RLDB_MAX_FILTER_CHARS);
        
        safecpy(m_TableName, tableName, sizeof(m_TableName));
        safecpy(m_Filter, filter, sizeof(m_Filter));

        m_Schema = (rlDbSchemaBase*)schema;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!(bb.SerStr(msg.m_TableName, sizeof(msg.m_TableName)) &&
             bb.SerStr(msg.m_Filter, sizeof(msg.m_Filter))))
        {
            return false;
        }

        if(!msg.m_Schema) 
        {
            msg.m_Schema = RL_NEW(rlDbMsgReadRandomRecordRequest, rlDbDynSchema);
            if(!msg.m_Schema) return false;
            msg.m_OwnMem = true;
        }

        if(!bb.SerSer(*msg.m_Schema)) return false;

        return true;
    }

    mutable bool m_OwnMem;
    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];
    char m_Filter[RLDB_MAX_FILTER_CHARS];
    mutable rlDbSchemaBase* m_Schema;
};

////////////////////////////////////////////////////////////////////////////////
// Upload a file
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgUploadFileRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgUploadFileRequest, RLDBMSG_UPLOAD_FILE_REQUEST);

    rlDbMsgUploadFileRequest()
    : m_FileData(0)
    , m_FileSize(0)
    , m_AllocatedFileBuf(false)
    {
    }

    virtual ~rlDbMsgUploadFileRequest()
    {
        if(m_AllocatedFileBuf)
        {
            rlFree(m_FileData);
            m_FileData = 0;
        }
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    bool Reset(u8* fileData,
               const unsigned fileSize)
    {
        if(!fileData || !fileSize)
        {
            return false;
        }

        m_FileData = fileData;
        m_FileSize = fileSize;

        return true;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!bb.SerUns(msg.m_FileSize, 32)) return false;

        if(msg.m_FileSize)
        {
            if(!msg.m_FileData)
            {
                if(msg.m_FileSize > RLDB_MAX_FILE_SIZE) 
                {
                    rlDbWarning("File size (%u) exceeds max allowed (%u)", msg.m_FileSize, RLDB_MAX_FILE_SIZE);
                    return false;
                }

                msg.m_FileData = (u8*)RL_ALLOCATE(rlDbMsg, msg.m_FileSize);
                if(!msg.m_FileData) return false;
                msg.m_AllocatedFileBuf = true;
            }

            if(!bb.SerBytes(msg.m_FileData, msg.m_FileSize))
            {
                return false;
            }
        }

        return true;
    }

    mutable u8*         m_FileData;
    mutable unsigned    m_FileSize;
    mutable bool        m_AllocatedFileBuf;
};

struct rlDbMsgUploadFileReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgUploadFileReply, RLDBMSG_UPLOAD_FILE_REPLY);

    void Reset(const bool success,
               const int resultCode,
               const rlDbFileId fileId)
    {
        m_Success = success;       
        m_ResultCode = resultCode;
        m_FileId = fileId;
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!bb.SerBool(msg.m_Success) ||
           !bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1))
        {
             return false;
        }
        if(msg.m_Success) return bb.SerInt(msg.m_FileId, 32);
        return true;
    }

    bool m_Success;
    int m_ResultCode;
    rlDbFileId m_FileId;
};

////////////////////////////////////////////////////////////////////////////////
// Download a file
////////////////////////////////////////////////////////////////////////////////
struct rlDbMsgDownloadFileRequest : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgDownloadFileRequest, RLDBMSG_DOWNLOAD_FILE_REQUEST);

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    void Reset(const int fileId,
               const unsigned bufSize)
    {
        m_FileId = fileId;
        m_BufSize = bufSize;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerInt(msg.m_FileId, 32) &&
               bb.SerUns(msg.m_BufSize, 32);
    }

    int m_FileId;
    unsigned m_BufSize;
};

struct rlDbMsgDownloadFileReply : public rlDbMsg
{
    NET_MESSAGE_DECL_ID(rlDbMsgDownloadFileReply, RLDBMSG_DOWNLOAD_FILE_REPLY);

    rlDbMsgDownloadFileReply(u8* buf, const unsigned bufSize)
    : m_Buf(buf)
    , m_BufSize(bufSize)
    , m_NumBytesDownloaded(0)
    {
    }

    void Reset(const bool success,
               const int resultCode,
               const unsigned numBytesDownloaded)
    {
        m_Success = success;       
        m_ResultCode = resultCode;
        m_NumBytesDownloaded = numBytesDownloaded;
        rlAssert(m_NumBytesDownloaded <= m_BufSize);
    }

    virtual unsigned GetExportByteSize() const
    {
        datNullExportBuffer bb;
        this->Export(bb);
        return bb.GetNumBytesWritten();
    }

    NET_MESSAGE_SER(bb, msg)
    {
        if(!bb.SerBool(msg.m_Success) ||
           !bb.SerInt(msg.m_ResultCode, datBitsNeeded<RLDB_NUM_RESULT_CODES>::COUNT + 1))
        {
             return false;
        }
        if(!bb.SerUns(msg.m_NumBytesDownloaded, 32)) return false;
        if(msg.m_NumBytesDownloaded)
        {
            if(!bb.SerBytes(msg.m_Buf, msg.m_NumBytesDownloaded)) return false;
        }
        return true;
    }

    bool m_Success;
    int m_ResultCode;
    unsigned m_NumBytesDownloaded;
    u8* m_Buf;
    unsigned m_BufSize;
};

}; //namespace rage

#endif //RLINE_RLDBMSGS_H
