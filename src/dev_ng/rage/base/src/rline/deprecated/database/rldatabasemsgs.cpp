// 
// rline/rldatabasemsgs.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rldatabasemsgs.h"

namespace rage
{

NET_MESSAGE_IMPL(rlDbMsgProgress);

NET_MESSAGE_IMPL(rlDbMsgCreateRecordRequest);
NET_MESSAGE_IMPL(rlDbMsgCreateRecordReply);

NET_MESSAGE_IMPL(rlDbMsgUpdateRecordRequest);
NET_MESSAGE_IMPL(rlDbMsgUpdateRecordReply);

NET_MESSAGE_IMPL(rlDbMsgDeleteRecordRequest);
NET_MESSAGE_IMPL(rlDbMsgDeleteRecordReply);

NET_MESSAGE_IMPL(rlDbMsgRateRecordRequest);
NET_MESSAGE_IMPL(rlDbMsgRateRecordReply);

NET_MESSAGE_IMPL(rlDbMsgGetNumRecordsRequest);
NET_MESSAGE_IMPL(rlDbMsgGetNumRecordsReply);

NET_MESSAGE_IMPL(rlDbMsgReadRecordsRequest);
NET_MESSAGE_IMPL(rlDbMsgReadRandomRecordRequest);
NET_MESSAGE_IMPL(rlDbMsgReadRecordsReply);

NET_MESSAGE_IMPL(rlDbMsgUploadFileRequest);
NET_MESSAGE_IMPL(rlDbMsgUploadFileReply);

NET_MESSAGE_IMPL(rlDbMsgDownloadFileRequest);
NET_MESSAGE_IMPL(rlDbMsgDownloadFileReply);

}; //namespace rage
