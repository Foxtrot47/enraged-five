// 
// rline/rlupload.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rlupload.h"
#include "rluploadrecord.h"
#include "rluploadtasks.h"

#define CREATETASK(taskname, ...) \
    rlUpload##taskname##Task* task = RL_NEW(rlUpload, rlUpload##taskname##Task); \
    if(!task) { rlUploadError("Failed to allocate rlUpload"#taskname"Task"); return false; } \
    if(task->Configure(__VA_ARGS__)) \
    { \
        SYS_CS_SYNC(m_TaskListCs); \
        m_TaskList.push_back(task); \
        return true; \
    } \
    else \
    { \
        rlUploadError("Failed to configure rlUpload"#taskname"Task"); \
        rlDelete(task); \
        return false; \
    }

namespace rage
{

///////////////////////////////////////////////////////////////////////////////
// rlUpload
///////////////////////////////////////////////////////////////////////////////
rlUpload::rlUpload()
: m_Initialized(false)
, m_DbClient(0)
{
}

rlUpload::~rlUpload()
{
    this->Shutdown();
}

bool
rlUpload::Init(rlDbClient* dbClient)
{
    rlAssert(dbClient);

    m_DbClient = dbClient;
    m_Initialized = true;

    return m_Initialized;
}

void
rlUpload::Shutdown()
{
    if(m_Initialized)
    {
        SYS_CS_SYNC(m_TaskListCs);

        while(!m_TaskList.empty())
        {
            TaskList::iterator it = m_TaskList.begin();

            rlUploadTask* t = *it;
            rlAssert(t);

            m_TaskList.erase(it);

            rlDelete(t);
        }

        m_DbClient = 0;

        m_Initialized = false;
    }
}

void
rlUpload::Update()
{
    SYS_CS_SYNC(m_TaskListCs);

    if(!m_TaskList.empty())
    {
        TaskList::iterator it = m_TaskList.begin();

        while(!m_TaskList.empty() && (it != m_TaskList.end()))
        {
            rlUploadTask* t = *it;
            rlAssert(t);

            if(!t->IsStarted())
            {
                rlAssert(t->IsPending());
                t->Start();
            }

            if(t->IsActive())
            {
                t->Update(0);
            }

            if(t->IsFinished())
            {
                TaskList::iterator nextIt = it;
                ++nextIt;               
                m_TaskList.erase(it);
                it = nextIt;

                rlDelete(t);
            }
            else
            {
                ++it;
            }
        }
    }
}

bool 
rlUpload::GetTableSize(const char* tableId,
                       const unsigned minNumRatings,
                       const unsigned maxNumRatings,
                       unsigned* numRecords,
                       netStatus* status)
{
    if(!(tableId && numRecords))
    {
        rlUploadDebug2("GetTableSize: Invalid arguments");
        return false;
    }

    CREATETASK(GetTableSize, this, tableId, minNumRatings, maxNumRatings, numRecords, status);
}

bool 
rlUpload::GetNumOwned(const char* tableId,
                      const rlGamerHandle& gamerHandle,
                      unsigned* numOwned,
                      netStatus* status)
{
    if(!(tableId && gamerHandle.IsValid() && numOwned))
    {
        rlUploadDebug2("GetNumOwned: Invalid arguments");
        return false;
    }

    CREATETASK(GetNumOwned, this, tableId, gamerHandle, numOwned, status);
}

bool 
rlUpload::ReadRecordsByGamer(const char* tableId,
                             const rlGamerHandle& gamerHandle,
                             const unsigned maxRecords,
                             rlUploadRecord* records,
                             unsigned* numRecordsRead,
                             netStatus* status)
{
    if(!(tableId && gamerHandle.IsValid() && maxRecords && records && numRecordsRead))
    {
        rlUploadDebug2("ReadRecordsByGamer: Invalid arguments");
        return false;
    }

    CREATETASK(ReadRecordsByGamer, this, tableId, gamerHandle, maxRecords, records, numRecordsRead, status);
}

bool 
rlUpload::ReadRandomRecord(const char* tableId,
                           const rlGamerHandle& gamerHandle,
                           const unsigned minNumRatings,
                           const unsigned maxNumRatings,
                           const u32 contentIdFlags,
                           rlUploadRecord* record,
                           unsigned* numRecordsRead, //Will be 0 or 1
                           netStatus* status)
{
    if(!(tableId && gamerHandle.IsValid() && record && numRecordsRead))
    {
        rlUploadDebug2("ReadRandomRecord: Invalid arguments");
        return false;
    }

    CREATETASK(ReadRandomRecord, this, tableId, gamerHandle, minNumRatings, maxNumRatings, contentIdFlags, record, numRecordsRead, status);
}

bool 
rlUpload::ReadRecordsByRank(const char* tableId,
                            const unsigned startingRank,
                            const unsigned minNumRatings,
                            const unsigned maxRecords,
                            rlUploadRecord* records,
                            unsigned* numRecordsRead,
                            netStatus* status)
{
    if(!(tableId && startingRank && maxRecords && records && numRecordsRead))
    {
        rlUploadDebug2("ReadRecordsByRank: Invalid arguments");
        return false;
    }

    CREATETASK(ReadRecordsByRank, this, tableId, startingRank, minNumRatings, maxRecords, records, numRecordsRead, status);
}

bool 
rlUpload::ReadRecord(const char* tableId,
                     const int recordId,
                     rlUploadRecord* record,
                     unsigned* numRecordsRead, //Will be 0 or 1
                     netStatus* status)
{
    if(!(tableId && (recordId != 0) && record && numRecordsRead))
    {
        rlUploadDebug2("ReadRecord: Invalid arguments");
        return false;
    }

    CREATETASK(ReadRecord, this, tableId, recordId, record, numRecordsRead, status);
}

bool 
rlUpload::RateRecord(const char* tableId,
                     const rlUploadRecord& record,
                     const u8 rating,
                     rlUploadRecord* newRecord,
                     netStatus* status)
{
    if(!(tableId && record.IsValid()))
    {
        rlUploadDebug2("RateRecord: Invalid arguments");
        return false;
    }

    CREATETASK(RateRecord, this, tableId, record, rating, newRecord,/*numRatings, averageRating,*/ status );
}

bool 
rlUpload::DeleteRecord(const char* tableId,
                       const int recordId,
                       netStatus* status)
{
    if(!(tableId && recordId))
    {
        rlUploadDebug2("DeleteRecord: Invalid arguments");
        return false;
    }

    CREATETASK(DeleteRecord, this, tableId, recordId, status);
}

bool 
rlUpload::UploadFile(const char* tableId,
                     const unsigned recordLimit,
                     const rlGamerHandle& gamerHandle,
                     const char* gamerName,
                     const unsigned numFiles,
                     const u8** fileData,
                     const unsigned* fileSizes,
                     const void* userData,
                     const unsigned userDataSize,
                     const u8 contentId,
                     rlUploadRecord* record,
                     float* progress,
                     netStatus* status)
{
    if(!(tableId && gamerHandle.IsValid() && gamerName && numFiles && fileData && fileSizes && record && progress && (contentId < rlUploadRecord::MAX_CONTENT_IDS)))
    {
        rlUploadDebug2("UploadFile: Invalid arguments");
        return false;
    }

    for(unsigned i = 0; i < numFiles; i++)
    {
        if(!fileData[i] || !fileSizes[i])
        {
            rlUploadDebug2("UploadFile: Invalid arguments (null file data or size for file %u)", i);
            return false;
        }
    }

    CREATETASK(UploadFile, this, tableId, recordLimit, gamerHandle, gamerName, numFiles, fileData, fileSizes, userData, userDataSize, contentId, record, progress, status);
}

bool 
rlUpload::OverwriteFile(const char* tableId,
                        const rlUploadRecord& currentRecord,
                        const unsigned numFiles,
                        const u8** fileData,
                        const unsigned* fileSizes,
                        const void* userData,
                        const unsigned userDataSize,
                        const u8 contentId,
                        rlUploadRecord* newRecord,
                        float* progress,
                        netStatus* status)
{
    if(!(tableId && currentRecord.IsValid() && numFiles && fileData && fileSizes && newRecord && progress && (contentId < rlUploadRecord::MAX_CONTENT_IDS)))
    {
        rlUploadDebug2("OverwriteFile: Invalid arguments");
        return false;
    }

    CREATETASK(OverwriteFile, this, tableId, currentRecord, numFiles, fileData, fileSizes, userData, userDataSize, contentId, newRecord, progress, status);
}

bool 
rlUpload::DownloadFile(const int fileId,
                       u8* buf,
                       const unsigned bufSize,
                       unsigned* bytesDownloaded,
                       float* progress,
                       netStatus* status)
{
    if(!(fileId && buf && bufSize && bytesDownloaded && progress))
    {
        rlUploadDebug2("DownloadFile: Invalid arguments");
        return false;
    }

    CREATETASK(DownloadFile, this, fileId, buf, bufSize, bytesDownloaded, progress, status);
}

bool 
rlUpload::OverwriteUserData(const char* tableId,
                            const rlUploadRecord& currentRecord,
                            const void* userData,
                            const unsigned userDataSize,
                            rlUploadRecord* newRecord,
                            netStatus* status)
{
    if(!(tableId && userData && userDataSize && newRecord))
    {
        rlUploadDebug2("OverwriteUserData: Invalid arguments");
        return false;
    }

    CREATETASK(OverwriteUserData, this, tableId, currentRecord, userData, userDataSize, newRecord, status);
}

}   //namespace rage
