// 
// rline/rlUpload.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUPLOAD_H
#define RLINE_RLUPLOAD_H

#include "rluploadtasks.h"
#include "rlworker.h"
#include "system/criticalsection.h"

namespace rage
{
class rlDbClient;
class rlUploadRecord;

//PURPOSE
//  Allows async upload/download, querying, rating, and deleting of files.
//
//  The files themselves are stored in a file server, and can only be referenced by a "file ID".
//  For each file there is a corresponding record in a database table, and the record has a
//  field containing the file's fileID.  
//
//  So long as at least one record references the fileID, the file will not be delete from the
//  file server.  However, once all references are removed the file will be deleted within
//  a day or so.

class rlUpload
{
public:
    rlUpload();
    ~rlUpload();

    //PARAMS
    //  dbClient        - DB client used internally
    bool Init(rlDbClient* dbClient);

    void Shutdown();

    //PURPOSE
    //  Updates currently active requests. This should be called frequently,
    //  ideally every frame.
    void Update();

    //PURPOSE
    //  Returns total number of records currently in a table.
    //PARAMETERS
    //  - tableId           ID of table to query
    //  - numRecords        (out) Number of records in table
    //  - status            Pointer to status variable used to monitor success/failure
    bool GetTableSize(const char* tableId,
                      const unsigned minNumRatings,
                      const unsigned maxNumRatings,
                      unsigned* numRecords,
                      netStatus* status);

    //PURPOSE
    //  Returns the number of records currently owned by a particular gamer.
    //PARAMETERS
    //  - tableId           ID of table to query
    //  - gamerHandle       Gamer whose records you want to count
    //  - numOwned          (out) Number of records owned by gamer
    //  - status            Pointer to status variable used to monitor success/failure
    bool GetNumOwned(const char* tableId,
                     const rlGamerHandle& gamerHandle,
                     unsigned* numOwned,
                     netStatus* status);

    //PURPOSE
    //  Gets record(s) for a particular gamer.
    //PARAMETERS
    //  - tableId           ID of table records exist in
    //  - gamerHandle       Gamer whose records you want
    //  - maxRecords        Max number of records to read
    //  - records           (out) Array of records to store results
    //  - numRecordsRead    (out) Number of records actually read
    bool ReadRecordsByGamer(const char* tableId,
                            const rlGamerHandle& gamerHandle,
                            const unsigned maxRecords,
                            rlUploadRecord* records,
                            unsigned* numRecordsRead,
                            netStatus* status);

    //PURPOSE
    //  Gets a random record that does NOT belong to specified gamer's, and whose numRatings
    //  lie between a range.
    //PARAMETERS
    //  - tableId           ID of table record exists in
    //  - gamerHandle       Gamer whose records you do NOT want; usually the querying gamer
    //  - minNumRatings     Minimum number of ratings to be considered; 0 for any
    //  - minNumRatings     Maximum number of ratings to be considered; 0 for any
    //  - contentIdFlags    Bitflags indicating which content IDs are allowed to be selected
    //  - record            (out) Holds randomly selected record
    //  - numRecordsRead    (out) Number of records read (0 or 1)
    //  - status            Pointer to status variable used to monitor success/failure
    //NOTES
    //  The rank of the returned record will be UNKNOWN_RANK; it is currently
    //  not possible to determine rank of a randomly fetched record.
    bool ReadRandomRecord(const char* tableId,
                          const rlGamerHandle& gamerHandle,
                          const unsigned minNumRatings,
                          const unsigned maxNumRatings,
                          const u32 contentIdFlags,
                          rlUploadRecord* record,
                          unsigned* numRecordsRead, //NOTE: Will always be 0 or 1
                          netStatus* status);

    //PURPOSE
    //  Gets records by rank (where rank 1 has highest average rating).
    //PARAMETERS
    //  - tableId           ID of table records exist in
    //  - startingRank      Rank to begin read at
    //  - minNumRatings     Minimum num ratings to be considered; 0 for any
    //  - maxRecords        Max records to return
    //  - records           (out) Array of records to store results
    //  - numRecordsRead    (out) Number of records returned
    //  - status            Pointer to status variable used to monitor success/failure
    bool ReadRecordsByRank(const char* tableId,
                           const unsigned startingRank,
                           const unsigned minNumRatings,
                           const unsigned maxRecords,
                           rlUploadRecord* records,
                           unsigned* numRecordsRead,
                           netStatus* status);

    //PURPOSE
    //  Gets a specific record by its record ID.  This is useful for re-getting 
    //  a record previously retrieved with one of the other read methods, and
    //  is the most efficient way to get a single specific record.
    //PARAMETERS
    //  - tableId           ID of table record exists in
    //  - recordId          ID of record to read
    //  - record            (out) Pointer to record in which to store results
    //  - numRecordsRead    (out) Number of records actually read
    //  - status            Pointer to status variable used to monitor success/failure
    bool ReadRecord(const char* tableId,
                    const int recordId,
                    rlUploadRecord* record,
                    unsigned* numRecordsRead,
                    netStatus* status);

    //PURPOSE
    //  Rates a record.
    //PARAMETERS
    //  - tableId           ID of table record exists in
    //  - record            Record to rate
    //  - rating            Rating value
    //  - newRecord         (Optional, out) Will hold new num ratings and avg rating on success
    //  - status            Pointer to status variable used to monitor success/failure
    //NOTES
    //  The new record will have a rank of UNKNOWN_RANK.
    bool RateRecord(const char* tableId,
                    const rlUploadRecord& record,
                    const u8 rating,             
                    rlUploadRecord* newRecord,
                    netStatus* status);

    //PURPOSE
    //  Deletes a record.
    //PARAMETERS
    //  - tableId           ID of table record exists in
    //  - recordId          ID of record to delete.  Associated files will also be deleted.
    //  - status            Pointer to status variable used to monitor success/failure
    bool DeleteRecord(const char* tableId,
                      const int recordId,
                      netStatus* status);

    //PURPOSE
    //  Uploads a new file for a gamer.
    //PARAMETERS
    //  - tableId           ID of table to create record in
    //  - recordLimit       Maximum number of records gamer may own in this table
    //  - gamerHandle       Handle of gamer uploading the data
    //  - gamerName         Name of gamer uploading the data
    //  - numFiles          Number of files to upload
    //  - fileData          Array of buffers containing file data
    //  - fileSizes         Array of file sizes
    //  - userData          Userdata buffer; use 0 if none.
    //  - userDataSize      Size of userdata buffer; user 0 if none.
    //  - contentId         ID indicating what DLC pack (or original content) the file belongs to
    //  - record            (out) The resulting record after uploading
    //  - progress          (out) Optional pointer to variable that gets updated with progress of operation (0.0f to 1.0f)
    //  - status            Pointer to status variable used to monitor success/failure
    bool UploadFile(const char* tableId,
                    const unsigned recordLimit,
                    const rlGamerHandle& gamerHandle,
                    const char* gamerName,
                    const unsigned numFiles,
                    const u8** fileData,
                    const unsigned* fileSizes,
                    const void* userData,
                    const unsigned userDataSize,
                    const u8 contentId,
                    rlUploadRecord* record,
                    float* progress,
                    netStatus* status);

    //PURPOSE
    //  Overwrites the file associated with a particular record.
    //PARAMETERS
    //  - tableId           ID of table record is in
    //  - currentRecord     Record associated with file to overwrite
    //  - numFiles          Number of files to upload with record; must be at least 1
    //  - fileData          Array of buffers containing file data
    //  - fileSizes         Arrray of file sizes
    //  - userData          Userdata buffer; use 0 if none.
    //  - userDataSize      Size of userdata buffer; user 0 if none.
    //  - contentId         ID indicating what DLC pack (or original content) the file belongs to
    //  - newRecord         (out) The updated record after overwriting (most importantly, has the new file ID).
    //                      It is safe for this to be a pointer to currentRecord.
    //  - progress          (out) Optional pointer to variable that gets updated with progress of operation (0.0f to 1.0f)
    //  - status            Pointer to status variable used to monitor success/failure
    //NOTES
    //  Overwriting the file will reset the record's ratings.  Also, the original
    //  record will first be destroyed, so the record you pass in will no longer
    //  be valid; use the new record thereafter.
    bool OverwriteFile(const char* tableId,
                       const rlUploadRecord& currentRecord,
                       const unsigned numFiles,
                       const u8** fileData,
                       const unsigned* fileSizes,
                       const void* userData,
                       const unsigned userDataSize,
                       const u8 contentId,
                       rlUploadRecord* newRecord,
                       float* progress,
                       netStatus* status);

    //PURPOSE
    //  Downloads previously uploaded file to a buffer.
    //PARAMETERS
    //  - fileId            ID of file to download
    //  - buf               Buffer to receive downloaded data
    //  - bufSize           Size of buffer
    //  - bytesDownloaded   (out) Actual number of bytes downloaded
    //  - progress          (out) Optional pointer to variable that gets updated with progress of operation (0.0f to 1.0f)
    //  - status            Pointer to status variable used to monitor success/failure
    //NOTES
    //  - The buffer passed in must be one byte larger than the largest file you
    //    want to download.  This is because the underlying Gamespy code requires
    //    an extra byte while downloading.  The extra byte read is at the end of
    //    the buffer, and is not included in the reported bytesDownloaded.
    bool DownloadFile(const int fileId,
                      u8* buf,
                      const unsigned bufSize,
                      unsigned* bytesDownloaded,
                      float* progress,
                      netStatus* status);

    //PURPOSE
    //  Overwrites the userdata of a record with new data.
    //PARAMETERS
    //  - tableId           ID of table record is in
    //  - currentRecord     Record to modify
    //  - userData          New user data
    //  - userDataSize      Size of new user data buffer
    //  - newRecord         (out) Record after the user data has been modified
    //  - status            Pointer to status variable used to monitor success/failure
    bool OverwriteUserData(const char* tableId,
                           const rlUploadRecord& currentRecord,
                           const void* userData,
                           const unsigned userDataSize,
                           rlUploadRecord* newRecord,
                           netStatus* status);

private:
    friend class rlUploadTask;
    typedef inlist<rlUploadTask, &rlUploadTask::m_ListLink> TaskList;
    TaskList m_TaskList;
    sysCriticalSectionToken m_TaskListCs;

    void QueueTask(rlUploadTask* task);

    rlDbClient* m_DbClient;

    bool m_Initialized  : 1;
};

}   //namespace rage

#endif  //RLINE_RLUPLOAD_H
