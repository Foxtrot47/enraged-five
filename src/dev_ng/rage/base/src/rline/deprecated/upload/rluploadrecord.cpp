// 
// rline/rluploadrecord.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rluploadrecord.h"
#include "rldatabase.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "string/string.h"
#include "system/memory.h"

namespace rage
{
RAGE_DEFINE_SUBCHANNEL(rline, upload)

rlUploadRecord::rlUploadRecord()
{
    Clear();
}

void 
rlUploadRecord::Clear()
{
    Reset(rlGamerHandle(),      //rlGamerHandle
          "",                   //GamerName
          0,                    //recordId
          0,                    //numFiles
          0,                    //fileIds
          0,                    //fileSizes
          0,                    //numRatings
          0,                    //averageRating
          UNKNOWN_RANK,         //rank
          0,                    //userData
          0,                    //userDataSize
          DEFAULT_CONTENT_ID);  //contentId
}

bool 
rlUploadRecord::IsValid() const
{
    if(!m_GamerHandle.IsValid()
       || !strlen(m_GamerName)
       || !m_RecordId
       || !m_NumFiles)
    {
        return false;
    }

    for(unsigned i = 0; i < m_NumFiles; i++)
    {
        if(!m_FileIds[i] || !m_FileSizes[i])
        {
            return false;
        }
    }

    return true;
}

void 
rlUploadRecord::Reset(const rlGamerHandle& gamerHandle,
                      const char* gamerName,
                      const int recordId,
                      const unsigned numFiles,
                      const int* fileIds,
                      const unsigned* fileSizes,
                      const unsigned numRatings,
                      const float averageRating,
                      const unsigned rank,
                      const u8* userData,
                      const unsigned userDataSize,
                      const u8 contentId)
{
    m_GamerHandle = gamerHandle;
    sysMemSet(m_GamerName, 0, sizeof(m_GamerName));
    safecpy(m_GamerName, gamerName, sizeof(m_GamerName));   
    m_RecordId = recordId;    

    sysMemSet(m_FileIds, 0 , sizeof(m_FileIds));
    sysMemSet(m_FileSizes, 0 , sizeof(m_FileSizes));

    m_NumFiles = numFiles;

    for(unsigned i = 0; i < m_NumFiles; i++)
    {
        m_FileIds[i] = fileIds[i];
        m_FileSizes[i] = fileSizes[i];
    }

    m_NumRatings = numRatings;
    m_AvgRating = averageRating;
    m_Rank = rank;

    sysMemSet(m_UserData, 0, sizeof(m_UserData));
    m_UserDataSize = 0;
    if(userData)
    {
        sysMemCpy(m_UserData, userData, userDataSize);
        m_UserDataSize = userDataSize;
    }    

    m_ContentId = contentId;
}

bool
rlUploadRecord::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    unsigned numBytes = 0;
    
    rtry
    {
        unsigned numGhBytes;

        rcheck(m_GamerHandle.Export(buf, sizeofBuf, &numGhBytes),
               catchall,
               rlUploadError("Failed to export gamerhandle"));

        datBitBuffer bb;
        bb.SetReadWriteBytes(&((u8*)buf)[numGhBytes], sizeofBuf - numGhBytes);

        rcheck((bb.WriteStr(m_GamerName, sizeof(m_GamerName)) &&
                bb.WriteInt(m_RecordId, 32) &&
                bb.WriteUns(m_NumFiles, datBitsNeeded<rlUploadRecord::MAX_FILES>::COUNT) &&
                bb.WriteUns(m_NumRatings, 32) &&
                bb.WriteFloat(m_AvgRating) &&
                bb.WriteUns(m_Rank, 32) &&
                bb.WriteUns(m_UserDataSize, datBitsNeeded<rlUploadRecord::MAX_USER_DATA_SIZE>::COUNT) &&
                bb.WriteUns(m_ContentId, datBitsNeeded<MAX_CONTENT_IDS>::COUNT)),
                catchall,
                rlUploadError("Failed to export rlUploadRecord fields"));

        for(unsigned i = 0; i < m_NumFiles; i++)
        {
            rcheck(bb.WriteInt(m_FileIds[i], 32) &&
                   bb.WriteUns(m_FileSizes[i], 32),
                   catchall,
                   rlUploadError("Failed to export rlUploadRecord file %u info", i));
        }
        
        if(m_UserDataSize)
        {
            rcheck(bb.WriteBytes(m_UserData, m_UserDataSize),
                   catchall,
                   rlUploadError("Failed to export userdata"));
        }

        numBytes = numGhBytes + bb.GetNumBytesWritten();
    }
    rcatchall
    {
    }

    if(size){*size = numBytes;}

    return numBytes > 0;
}

bool
rlUploadRecord::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    unsigned numBytes = 0;
    
    rtry
    {
        unsigned numGhBytes;

        rcheck(m_GamerHandle.Import(buf, sizeofBuf, &numGhBytes),
               catchall,
               rlUploadError("Failed to import gamerhandle"));

        datBitBuffer bb;
        bb.SetReadOnlyBytes(&((u8*)buf)[numGhBytes], sizeofBuf - numGhBytes);

        rcheck((bb.ReadStr(m_GamerName, sizeof(m_GamerName)) &&
                bb.ReadInt(m_RecordId, 32) &&
                bb.ReadUns(m_NumFiles, datBitsNeeded<rlUploadRecord::MAX_FILES>::COUNT) &&
                bb.ReadUns(m_NumRatings, 32) &&
                bb.ReadFloat(m_AvgRating) &&
                bb.ReadUns(m_Rank, 32) &&
                bb.ReadUns(m_UserDataSize, datBitsNeeded<rlUploadRecord::MAX_USER_DATA_SIZE>::COUNT) &&
                bb.ReadUns(m_ContentId, datBitsNeeded<MAX_CONTENT_IDS>::COUNT)),
                catchall,
                rlUploadError("Failed to import rlUploadRecord fields"));

        for(unsigned i = 0; i < m_NumFiles; i++)
        {
            rcheck(bb.ReadInt(m_FileIds[i], 32) &&
                   bb.ReadUns(m_FileSizes[i], 32),
                   catchall,
                   rlUploadError("Failed to import rlUploadRecord file %u info", i));
        }

        if(m_UserDataSize)
        {
            rcheck(bb.ReadBytes(m_UserData, m_UserDataSize),
                   catchall,
                   rlUploadError("Failed to import userdata"));
        }

        numBytes = numGhBytes + bb.GetNumBytesRead();
    }
    rcatchall
    {
    }

    if(size){*size = numBytes;}

    return numBytes > 0;
}

const char*
rlUploadRecord::GetFieldName(const int fieldId)
{
    rlAssert((fieldId >=0) && (fieldId < NUM_FIELDS));

    switch(fieldId)
    {
    case FIELD_RECORD_ID:       return rlDbGetFieldName(RLDB_FIELD_RECORD_ID);
    case FIELD_GAMER_NAME:      return rlDbGetFieldName(RLDB_FIELD_GAMER_NAME);
    case FIELD_GAMER_HANDLE:    return rlDbGetFieldName(RLDB_FIELD_GAMER_HANDLE);
    case FIELD_CONTENT_ID:      return "rs_ContentId";
    case FIELD_NUM_FILES:       return "rs_NumFiles";
    case FIELD_FILE0_ID:        return "rs_FileId0";
    case FIELD_FILE1_ID:        return "rs_FileId1";
    case FIELD_FILE2_ID:        return "rs_FileId2";
    case FIELD_FILE0_SIZE:      return "rs_FileId0.size";
    case FIELD_FILE1_SIZE:      return "rs_FileId1.size";
    case FIELD_FILE2_SIZE:      return "rs_FileId2.size";
    case FIELD_USER_DATA:       return "rs_UserData";
    case FIELD_NUM_RATINGS:     return rlDbGetFieldName(RLDB_FIELD_NUM_RATINGS);
    case FIELD_AVG_RATING:      return rlDbGetFieldName(RLDB_FIELD_AVG_RATING);
    case FIELD_RANK:            return rlDbGetFieldName(RLDB_FIELD_RANK);
    default:                    return "[UNKNOWN]";
    }
}

const rlGamerHandle& 
rlUploadRecord::GetGamerHandle() const 
{ 
    return m_GamerHandle; 
}

const char* 
rlUploadRecord::GetGamerName() const 
{ 
    return m_GamerName; 
}

int         
rlUploadRecord::GetRecordId() const 
{ 
    return m_RecordId; 
}

unsigned    
rlUploadRecord::GetNumFiles() const 
{ 
    return m_NumFiles; 
}

int         
rlUploadRecord::GetFileId(const unsigned index) const 
{ 
    rlAssert(index < m_NumFiles); 
    return m_FileIds[index]; 
}

unsigned    
rlUploadRecord::GetFileSize(const unsigned index) const 
{ 
    rlAssert(index < m_NumFiles); 
    return m_FileSizes[index]; 
}

unsigned    
rlUploadRecord::GetNumRatings() const 
{ 
    return m_NumRatings; 
}

float       
rlUploadRecord::GetAverageRating() const 
{ 
    return m_AvgRating; 
}

unsigned    
rlUploadRecord::GetRank() const 
{ 
    return m_Rank; 
}

const u8*   
rlUploadRecord::GetUserData() const 
{ 
    return m_UserData; 
}

unsigned    
rlUploadRecord::GetUserDataSize() const 
{ 
    return m_UserDataSize; 
}

u8          
rlUploadRecord::GetContentId() const 
{ 
    return m_ContentId; 
}

}   //namespace rage
