// 
// rline/rlploadtasks.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUPLOADTASKS_H
#define RLINE_RLUPLOADTASKS_H

#include "rluploadrecord.h"
#include "rldatabase.h"
#include "rltask.h"
#include "atl/inlist.h"
#include "net/status.h"

namespace rage
{

class rlDbClient;

//PURPOSE
//  rlUpload internally keeps a queue of tasks to carry out
//  user requests.  This is the base class for those tasks.
//  Users never directly instantiate any of these classes.
class rlUploadTask : public rlTask<class rlUpload>
{
friend class rlUpload;

public:
    RL_DECL_TASK(rlUploadTask);    
    RL_TASK_USE_SUBCHANNEL(rline, upload);

    rlUploadTask();
    virtual ~rlUploadTask();

    bool Configure(rlUpload* ctx, netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    rlDbClient* GetDbClient();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    static bool DbResultsToUploadRecords(const rlDbReadResults& results,
                                         rlUploadRecord* uploadRecords,
                                         const unsigned maxUploadRecords,
                                         unsigned* numUploadRecords,
                                         const bool requireRow);

private:
    inlist_node<rlUploadTask> m_ListLink;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadGetTableSizeTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadGetTableSizeTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadGetTableSizeTask);

    rlUploadGetTableSizeTask();
    virtual ~rlUploadGetTableSizeTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const unsigned minNumRatings,
                   const unsigned maxNumRatings,
                   unsigned* numRecords,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    const char* m_TableId;
    unsigned*   m_NumRecords;

    enum{ FILTER_MAX_CHARS = 128 };
    char m_Filter[FILTER_MAX_CHARS];

    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadGetNumOwnedTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadGetNumOwnedTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadGetNumOwnedTask);

    rlUploadGetNumOwnedTask();
    virtual ~rlUploadGetNumOwnedTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const rlGamerHandle& gamerHandle,
                   unsigned* numOwned,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    const char*     m_TableId;
    unsigned*       m_NumOwned;
    
    enum{ FILTER_MAX_CHARS = 128 };
    char m_Filter[FILTER_MAX_CHARS];

    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordsTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadReadRecordsTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadReadRecordsTask);

    rlUploadReadRecordsTask();
    virtual ~rlUploadReadRecordsTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const char* filter,
                   const char* sort,
                   const unsigned startingRank,
                   const char* postSortFilter,
                   const unsigned radius,
                   const bool cacheResults,
                   const unsigned maxRecords,
                   rlUploadRecord* records,
                   unsigned* numRecordsRead,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    void Clear();

    const char* m_TableId;
    const char* m_Filter;
    const char* m_Sort;
    unsigned    m_StartingRank;
    const char* m_PostSortFilter;
    unsigned    m_Radius;
    bool        m_CacheResults;

    rlUploadRecord* m_Records;
    unsigned* m_NumRecordsRead;

    rlDbFixedSchema<rlUploadRecord::NUM_FIELDS> m_Schema;
    rlDbValue* m_Values;
    rlDbReadResults m_DbResults;

    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordsByGamerTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadReadRecordsByGamerTask : public rlUploadReadRecordsTask
{
public:
    RL_DECL_TASK(rlUploadReadRecordsByGamerTask);

    rlUploadReadRecordsByGamerTask();
    virtual ~rlUploadReadRecordsByGamerTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const rlGamerHandle& gamerHandle,
                   const unsigned maxRecords,
                   rlUploadRecord* records,
                   unsigned* numRecordsRead,
                   netStatus* status);

private:
    enum{ FILTER_MAX_CHARS = 128 };
    char m_PostSortFilter[FILTER_MAX_CHARS];

    enum{ SORT_MAX_CHARS = 128 };
    char m_Sort[SORT_MAX_CHARS];
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordsByRankTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadReadRecordsByRankTask : public rlUploadReadRecordsTask
{
public:
    RL_DECL_TASK(rlUploadReadRecordsByRankTask);

    rlUploadReadRecordsByRankTask();
    virtual ~rlUploadReadRecordsByRankTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const unsigned startingRank,
                   const unsigned minNumRatings,
                   const unsigned maxRecords,
                   rlUploadRecord* records,
                   unsigned* numRecordsRead,
                   netStatus* status);

private:
    enum{ FILTER_MAX_CHARS = 128 };
    char m_Filter[FILTER_MAX_CHARS];

    enum{ SORT_MAX_CHARS = 128 };
    char m_Sort[SORT_MAX_CHARS];
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadReadRecordTask : public rlUploadReadRecordsTask
{
public:
    rlUploadReadRecordTask();
    virtual ~rlUploadReadRecordTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const int recordId,
                   rlUploadRecord* record,
                   unsigned* numRecordsRead,
                   netStatus* status);

private:
    enum{ FILTER_MAX_CHARS = 128 };
    char m_Filter[FILTER_MAX_CHARS];

    enum{ SORT_MAX_CHARS = 128 };
    char m_Sort[SORT_MAX_CHARS];
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRandomRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadReadRandomRecordTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadReadRandomRecordTask);

    rlUploadReadRandomRecordTask();
    virtual ~rlUploadReadRandomRecordTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const rlGamerHandle& gamerHandle,
                   const unsigned minNumRatings,
                   const unsigned maxNumRatings,
                   const u32 contentIdFlags,
                   rlUploadRecord* record,
                   unsigned* numRecordsRead,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    const char*     m_TableId;
    rlUploadRecord* m_Record;
    unsigned*       m_NumRecordsRead;
    
    enum{ FILTER_MAX_CHARS = 512 };
    char m_Filter[FILTER_MAX_CHARS];

    netStatus m_MyStatus;

    rlDbFixedSchema<rlUploadRecord::NUM_FIELDS-1> m_Schema;
    rlDbValue m_Values[rlUploadRecord::NUM_FIELDS-1]; //-1 because random cannot get rank
    rlDbReadResults m_DbResults;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadRateRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadRateRecordTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadRateRecordTask);

    rlUploadRateRecordTask();
    virtual ~rlUploadRateRecordTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const rlUploadRecord& record,
                   const u8 rating,
                   rlUploadRecord* newRecord,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    const char*     m_TableId;
    rlUploadRecord  m_Record;
    u8              m_Rating;
    rlUploadRecord* m_NewRecord;
    unsigned        m_NumRatings;
    float           m_AvgRating;

    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadDeleteRecordTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadDeleteRecordTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadDeleteRecordTask);

    rlUploadDeleteRecordTask();
    virtual ~rlUploadDeleteRecordTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const int recordId,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    const char*     m_TableId;
    int             m_RecordId;

    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadUploadFileTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadUploadFileTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadUploadFileTask);

    rlUploadUploadFileTask();
    virtual ~rlUploadUploadFileTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const unsigned recordLimit,
                   const rlGamerHandle& gamerHandle,
                   const char* gamerName,
                   const unsigned numFiles,
                   const u8** fileData,
                   const unsigned* fileSizes,
                   const void* userData,
                   const unsigned userDataSize,
                   const u8 contentId,
                   rlUploadRecord* record,
                   float* progress,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    enum eState
    {
        STATE_IDLE = 0,
        STATE_GET_NUM_OWNED,
        STATE_UPLOAD_FILE,
        STATE_UPLOADING_FILE,
        STATE_CREATE_RECORD,
        STATE_CREATING_RECORD
    };
    int m_State;

    const char*     m_TableId;
    unsigned        m_RecordLimit;
    rlGamerHandle   m_GamerHandle;
    char            m_HandleStr[RL_MAX_HANDLE_CHARS];
    const char*     m_GamerName;
    unsigned        m_NumFiles;
    const u8**      m_FileData;
    const unsigned* m_FileSizes;
    const void*     m_UserData;
    unsigned        m_UserDataSize;
    u8              m_ContentId;
    rlUploadRecord* m_Record;
    float*          m_Progress;
    
    unsigned        m_NumOwned;
    int             m_RecordId;
    int             m_FileIds[rlUploadRecord::MAX_FILES];

    enum { NUM_FIELDS = 5 + rlUploadRecord::MAX_FILES };
    rlDbField m_Fields[NUM_FIELDS];
    
    enum{ FILTER_MAX_CHARS = 128 };
    char m_Filter[FILTER_MAX_CHARS];

    netStatus m_MyStatus;
    unsigned m_CurFile;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadOverwriteFileTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadOverwriteFileTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadOverwriteFileTask);

    rlUploadOverwriteFileTask();
    virtual ~rlUploadOverwriteFileTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const rlUploadRecord& currentRecord,
                   const unsigned numFiles,
                   const u8** fileData,
                   const unsigned* fileSizes,
                   const void* userData,
                   const unsigned userDataSize,
                   const u8 contentId,
                   rlUploadRecord* newRecord,
                   float* progress,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    enum eState
    {
        STATE_IDLE = 0,
        STATE_UPLOAD_FILE,
        STATE_UPLOADING_FILE,
        STATE_DELETE_RECORD,
        STATE_DELETING_RECORD,
        STATE_CREATE_RECORD,
        STATE_CREATING_RECORD
    };
    int m_State;

    const char*     m_TableId;
    rlUploadRecord  m_CurrentRecord;
    unsigned        m_NumFiles;
    const u8**      m_FileData;
    const unsigned* m_FileSizes;
    const void*     m_UserData;
    unsigned        m_UserDataSize;
    u8              m_ContentId;
    rlUploadRecord* m_NewRecord;
    float*          m_Progress;
    
    int             m_RecordId;
    int             m_FileIds[rlUploadRecord::MAX_FILES];

    char            m_HandleStr[RL_MAX_HANDLE_CHARS];

    enum { NUM_FIELDS = 5 + rlUploadRecord::MAX_FILES };
    rlDbField m_Fields[NUM_FIELDS];
    
    netStatus m_MyStatus;
    unsigned m_CurFile;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadDownloadFileTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadDownloadFileTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadDownloadFileTask);

    rlUploadDownloadFileTask();
    virtual ~rlUploadDownloadFileTask();

    bool Configure(rlUpload* ctx, 
                   const int fileId,
                   u8* buf,
                   const unsigned bufSize,
                   unsigned* bytesDownloaded,
                   float* progress,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    int         m_FileId;
    u8*         m_Buf;
    unsigned    m_BufSize;
    unsigned*   m_BytesDownloaded;
    float*      m_Progress;

    netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
// rlUploadOverwriteUserDataTask
///////////////////////////////////////////////////////////////////////////////
class rlUploadOverwriteUserDataTask : public rlUploadTask
{
public:
    RL_DECL_TASK(rlUploadOverwriteUserDataTask);

    rlUploadOverwriteUserDataTask();
    virtual ~rlUploadOverwriteUserDataTask();

    bool Configure(rlUpload* ctx, 
                   const char* tableId,
                   const rlUploadRecord& currentRecord,
                   const void* userData,
                   const unsigned userDataSize,
                   rlUploadRecord* newRecord,
                   netStatus* status);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    virtual void Finish(const FinishType finishType, const int resultCode = 0);

private:
    const char*     m_TableId;
    rlUploadRecord  m_CurrentRecord;
    const void*     m_UserData;
    unsigned        m_UserDataSize;
    rlUploadRecord* m_NewRecord;

    enum { NUM_FIELDS = 1 };
    rlDbField m_Fields[NUM_FIELDS];

    netStatus m_MyStatus;
};

}   //namespace rage

#endif  //RLINE_RLUPLOADTASKS_H
