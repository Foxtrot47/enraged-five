// 
// rline/rluploadrecord.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLUPLOADRECORD_H
#define RLINE_RLUPLOADRECORD_H

#include "rlgamerinfo.h"
#include "system/param.h"

namespace rage
{

//All the rlUpload files include this header, so we define these here.
RAGE_DECLARE_SUBCHANNEL(rline, upload)

#define rlUploadDebug1(fmt, ...)    RAGE_DEBUGF1(rline_upload, fmt, ##__VA_ARGS__)
#define rlUploadDebug2(fmt, ...)    RAGE_DEBUGF2(rline_upload, fmt, ##__VA_ARGS__)
#define rlUploadDebug3(fmt, ...)    RAGE_DEBUGF3(rline_upload, fmt, ##__VA_ARGS__)
#define rlUploadWarning(fmt, ...)   RAGE_WARNINGF(rline_upload, fmt, ##__VA_ARGS__)
#define rlUploadError(fmt, ...)     RAGE_ERRORF(rline_upload, fmt, ##__VA_ARGS__)


//PURPOSE
//  A record in a database table, describing a previously uploaded file.
class rlUploadRecord
{
public:
    enum 
    { 
        MAX_USER_DATA_SIZE  = 64,
        MAX_FILES           = 3,
        UNKNOWN_RANK        = 0,
        DEFAULT_CONTENT_ID  = 0,
        MAX_CONTENT_IDS     = 32,
    };

    enum eFieldId
    {
        FIELD_RECORD_ID = 0,
        FIELD_GAMER_NAME,
        FIELD_GAMER_HANDLE,
        FIELD_CONTENT_ID,
        FIELD_NUM_FILES,
        FIELD_FILE0_ID,
        FIELD_FILE1_ID,
        FIELD_FILE2_ID,
        FIELD_FILE0_SIZE,
        FIELD_FILE1_SIZE,
        FIELD_FILE2_SIZE,
        FIELD_USER_DATA,
        FIELD_NUM_RATINGS,
        FIELD_AVG_RATING,
        FIELD_RANK,
        NUM_FIELDS
    };

    //Returns name of field used in rlDbClient calls.
    static const char* GetFieldName(const int fieldId);

    rlUploadRecord();

    //Returns true if the record appears to have valid data. 
    //This is not foolproof, but can spot many common errors.
    bool IsValid() const;

    //Members are read-only, because only rlUpload and certain tasks are allowed to modify rlUploadRecord instances.
    const rlGamerHandle& GetGamerHandle() const;
    const char* GetGamerName() const;
    int         GetRecordId() const;
    unsigned    GetNumFiles() const;
    int         GetFileId(const unsigned index) const;
    unsigned    GetFileSize(const unsigned index) const;
    unsigned    GetNumRatings() const;
    float       GetAverageRating() const;
    unsigned    GetRank() const;
    const u8*   GetUserData() const;
    unsigned    GetUserDataSize() const;
    u8          GetContentId() const;

    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

private:
    void Clear();

    void Reset(const rlGamerHandle& gamerHandle,    //Gamer that uploaded the file
               const char* gamerName,               //Display name of gamer
               const int recordId,                  //ID of record in table
               const unsigned numFiles,             //Number of files uploaded with record
               const int* fileIds,                  //ID of uploaded file associated with record
               const unsigned* fileSizes,           //Size of uploaded file
               const unsigned numRatings,           //Number of times the record has been rated by users
               const float averageRating,           //Average rating
               const unsigned rank,                 //Rank in table, sorted by descending rating; 0 means "unknown"
               const u8* userData,                  //Arbitrary userdata attached to record, up to RLUPLOAD_MAX_USERDATA_SIZE bytes
               const unsigned userDataSize,         //Size of userdata
               const u8 contentId);                 //ID associating this record with either original content or a DLC pack

    rlGamerHandle   m_GamerHandle;
    char            m_GamerName[RL_MAX_NAME_CHARS];
    int             m_RecordId;
    unsigned        m_NumFiles;
    int             m_FileIds[MAX_FILES];
    unsigned        m_FileSizes[MAX_FILES];
    unsigned        m_NumRatings;
    float           m_AvgRating;
    unsigned        m_Rank;
    u8              m_UserData[rlUploadRecord::MAX_USER_DATA_SIZE];
    unsigned        m_UserDataSize;
    u8              m_ContentId;

    friend class rlUploadTask;
    friend class rlUploadRateRecordTask;
    friend class rlUploadUploadFileTask;
    friend class rlUploadOverwriteFileTask;
    friend class rlUploadOverwriteUserDataTask;
};

}   //namespace rage

#endif  //RLINE_RLUPLOADRECORD_H
