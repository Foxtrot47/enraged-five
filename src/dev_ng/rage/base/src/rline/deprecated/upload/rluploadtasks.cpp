// 
// rline/rluploadtasks.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rluploadtasks.h"
#include "rlupload.h"
#include "rldatabaseclient.h"
#include "diag/seh.h"
#include "string/string.h"
#include "system/memops.h"
#include "system/nelem.h"

namespace rage
{

//Gets array of sake field names. Note that this is a function because we don't want to 
//initialize it until after the manual rating flag has been set.
static const char *sSakeFieldNames[] = { 
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_NAME),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RECORD_ID),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_FILES),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE0_ID),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE1_ID),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE2_ID),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE0_SIZE),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE1_SIZE),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE2_SIZE),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_USER_DATA),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_CONTENT_ID),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_AVG_RATING),
        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RANK) //NOTE: Row MUST be last, b/c random can't get it
    }; 

static const rlDbValueType sSakeFieldTypes[] = { 
        RLDB_VALUETYPE_ASCII_STRING, //rlUploadRecord::FIELD_GAMER_HANDLE
        RLDB_VALUETYPE_ASCII_STRING, //rlUploadRecord::FIELD_GAMER_NAME
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_RECORD_ID
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_NUM_FILES
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_FILE0_ID
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_FILE1_ID
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_FILE2_ID
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_FILE0_SIZE
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_FILE1_SIZE
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_FILE2_SIZE
        RLDB_VALUETYPE_BINARY_DATA, //rlUploadRecord::FIELD_USER_DATA
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_CONTENT_ID
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_NUM_RATINGS
        RLDB_VALUETYPE_FLOAT, //rlUploadRecord::FIELD_AVG_RATING
        RLDB_VALUETYPE_INT32, //rlUploadRecord::FIELD_RANK
    };

///////////////////////////////////////////////////////////////////////////////
// rlUploadTask
///////////////////////////////////////////////////////////////////////////////
rlUploadTask::rlUploadTask()
{
}

rlUploadTask::~rlUploadTask()
{
}

bool 
rlUploadTask::Configure(rlUpload* ctx, netStatus* status)
{
    return rlTask<class rlUpload>::Configure(ctx, status);
}

void 
rlUploadTask::Start()
{
    rlTaskDebug2("starting");

    rlTask<class rlUpload>::Start();
}

void 
rlUploadTask::Update(const unsigned timeStep)
{
    rlTask<class rlUpload>::Update(timeStep);
}

rlDbClient* 
rlUploadTask::GetDbClient()
{
    return m_Ctx->m_DbClient;
}

void 
rlUploadTask::Finish(const FinishType finishType, const int resultCode)
{
    switch(finishType)
    {
    case FINISH_FAILED:
        rlTaskDebug2("%s (%d)", rlTaskBase::FinishString(finishType), resultCode);
        break;
    default:
        rlTaskDebug2("%s", rlTaskBase::FinishString(finishType));
    }    

    rlTask<class rlUpload>::Finish(finishType, resultCode);
}

bool 
rlUploadTask::DbResultsToUploadRecords(const rlDbReadResults& results,
                                       rlUploadRecord* uploadRecords,
                                       const unsigned maxUploadRecords,
                                       unsigned* numUploadRecords,
                                       const bool requireRow)
{
    unsigned numValidRecords = 0;

    for(unsigned i = 0; i < results.GetNumRecords(); i++)
    {
        const rlDbSchemaBase* schema = results.GetSchema();

        rlUploadRecord r;

        unsigned numRequiredFields =    1 + //recordid
                                        1 + //rs_GamerHandle
                                        1 + //rs_GamerName
                                        1 + //rs_NumRatings
                                        1 + //rs_AvgRating
                                        1 + //rs_UserData
                                        1 + //rs_NumFiles
                                        1;  //rs_ContentId
        if(requireRow) 
        {
            ++numRequiredFields;
        }

        u8 numValidFields = 0;

        for(unsigned j = 0; j < schema->GetNumFields(); j++)
		{
            const char* fieldName = schema->GetFieldName(j);
            
            //rlUploadDebug3("Field %d: %s [%s]=%s", 
            //               j,
            //               fieldName,
            //               rlDbValue::GetTypeString(schema->GetFieldType(j)),
            //               results.GetValueString(i, j));

            if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE)))
            {
                if(r.m_GamerHandle.FromString(results.GetAsciiString(i, j)))
                {
                    ++numValidFields;
                }
                else
                {
                    rlUploadError("Failed to read gamer handle (%s); skipping record", results.GetAsciiString(i, j));
                    break;
                }
            }
            else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_NAME)))
            {
                sysMemSet(r.m_GamerName, 0, sizeof(r.m_GamerName));
                safecpy(r.m_GamerName, results.GetAsciiString(i, j), sizeof(r.m_GamerName));
                ++numValidFields;
            }
            else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RECORD_ID)))
            {
                r.m_RecordId = results.GetInt32(i, j);
                ++numValidFields;
            }
            else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_CONTENT_ID)))
            {
                r.m_ContentId = (u8)results.GetInt32(i, j);
                ++numValidFields;
            }
            else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_FILES)))
            {
                if(results.GetInt32(i, j) <= rlUploadRecord::MAX_FILES)
                {
                    r.m_NumFiles = results.GetInt32(i, j);
                    ++numValidFields;
                }
                else
                {
                    rlUploadError("numFiles (%d) is greater than max allowed (%d)", 
                             results.GetInt32(i, j),
                             rlUploadRecord::MAX_FILES);
                    numValidFields = 0;
                    break;
                }
            }
            else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_USER_DATA)))
            {
                const rlDbBinaryData& bd = results.GetBinaryData(i, j);

                r.m_UserDataSize = bd.GetSize();
                if(r.m_UserDataSize)
                {
                    sysMemCpy(r.m_UserData, bd.GetData(), r.m_UserDataSize);
                }
                ++numValidFields;
            }
            else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS)))
            {
                r.m_NumRatings = results.GetInt32(i, j);
                ++numValidFields;
            }
            else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_AVG_RATING)))
            {
                r.m_AvgRating = results.GetFloat(i, j);
                ++numValidFields;
            }
            else if(requireRow && !strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RANK)))
            {
                r.m_Rank = results.GetInt32(i, j);
                ++numValidFields;
            }
        }

        if(numValidFields != numRequiredFields)
        {
            rlDbWarning("Skipping record %d, only %d of %d required fields were valid", 
                        i, numValidFields, numRequiredFields);
            continue;
        }

        numRequiredFields += r.GetNumFiles() * 2; //ID and size

        for(unsigned fileIndex = 0; fileIndex < r.GetNumFiles(); fileIndex++)
        {
            for(unsigned fieldIndex = 0; fieldIndex < schema->GetNumFields(); fieldIndex++)
		    {
                const char* fieldName = schema->GetFieldName(fieldIndex);

                if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE0_ID + fileIndex)))
                {
                    r.m_FileIds[fileIndex] = results.GetInt32(i, fieldIndex);
                    ++numValidFields;
                }
                else if(!strcmp(fieldName, rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE0_SIZE + fileIndex)))
                {
                    r.m_FileSizes[fileIndex] = results.GetInt32(i, fieldIndex);
                    ++numValidFields;
                }
            }
        }

        if(numValidFields != numRequiredFields)
        {
            rlDbWarning("Skipping record %d; one or more fileID fields missing", i);
            continue;
        }

        if(!requireRow)
        {
            r.m_Rank = 0;
        }

#if !__NO_OUTPUT
        if(Channel_rline_upload.TtyLevel >= 3)
        {
            char handleBuf[128];
            r.GetGamerHandle().ToString(handleBuf, sizeof(handleBuf));

            rlUploadDebug3("DbResultsToUploadRecords: Record %d...", i);
            rlUploadDebug3("     m_GamerHandle       = %s", handleBuf);
            rlUploadDebug3("     m_GamerName         = %s", r.GetGamerName());
            rlUploadDebug3("     m_NumRatings        = %u", r.GetNumRatings());
            rlUploadDebug3("     m_AvgRating         = %0.1f", r.GetAverageRating());
            rlUploadDebug3("     m_Rank              = %u", r.GetRank());
            rlUploadDebug3("     m_NumFiles          = %u", r.GetNumFiles());

            for(unsigned fileIndex = 0; fileIndex < r.GetNumFiles(); fileIndex++)
            {
                rlUploadDebug3("     m_FileId%d          = %u", fileIndex, r.GetFileId(fileIndex));
                rlUploadDebug3("     m_FileId%d.size     = %u", fileIndex, r.GetFileSize(fileIndex));
            }
        }
#endif

        if(numValidRecords < maxUploadRecords)
        {
            uploadRecords[numValidRecords] = r;
            ++numValidRecords;

            //rlDebug2("DbResultsToUploadRecords: Added record to results (%d now)", numValidRecords);
        }
        else
        {
            char handleBuf[128];
            r.GetGamerHandle().ToString(handleBuf, sizeof(handleBuf));

            rlUploadError("More records found (%u) for '%s' (handle='%s') than can be stored (%u); discarding", 
                             results.GetNumRecords(), r.GetGamerName(), handleBuf, maxUploadRecords);
        }
    }

    *numUploadRecords = numValidRecords;

    return true;
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadGetTableSizeTask
///////////////////////////////////////////////////////////////////////////////
rlUploadGetTableSizeTask::rlUploadGetTableSizeTask()
: m_TableId(0)
, m_NumRecords(0)
{
}

rlUploadGetTableSizeTask::~rlUploadGetTableSizeTask()
{
}

bool 
rlUploadGetTableSizeTask::Configure(rlUpload* ctx, 
                                    const char* tableId,
                                    const unsigned minNumRatings,
                                    const unsigned maxNumRatings,
                                    unsigned* numRecords,
                                    netStatus* status)
{
    if(!rlVerify(tableId && numRecords)) 
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_NumRecords = numRecords;

    m_Filter[0] = '\0';

    if(maxNumRatings && minNumRatings)
    {
        formatf(m_Filter, 
                sizeof(m_Filter), 
                "(%s >= %u) and (%s <= %u)", 
                rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
                minNumRatings,
                rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
                maxNumRatings);
    }
    else if(minNumRatings)
    {
        formatf(m_Filter, 
                sizeof(m_Filter), 
                "(%s >= %u)", 
                rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
                minNumRatings);
    }
    else if(maxNumRatings)
    {
        formatf(m_Filter, 
                sizeof(m_Filter), 
                "(%s <= %u)", 
                rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
                maxNumRatings);
    }
    else
    {
        formatf(m_Filter, sizeof(m_Filter), "recordid > 0");
    }

    return true;
}

void 
rlUploadGetTableSizeTask::Start()
{
    rlUploadTask::Start();

    if(!GetDbClient()->GetNumRecords(m_TableId,
                                         m_Filter,
                                         m_NumRecords,
                                         &m_MyStatus))
    {
        rlTaskError("Failed to start request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadGetTableSizeTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED,
                m_MyStatus.GetResultCode());
    }
}

void 
rlUploadGetTableSizeTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadGetNumOwnedTask
///////////////////////////////////////////////////////////////////////////////
rlUploadGetNumOwnedTask::rlUploadGetNumOwnedTask()
: m_TableId(0)
, m_NumOwned(0)
{
}

rlUploadGetNumOwnedTask::~rlUploadGetNumOwnedTask()
{
}

bool 
rlUploadGetNumOwnedTask::Configure(rlUpload* ctx, 
                                   const char* tableId,
                                   const rlGamerHandle& gamerHandle,
                                   unsigned* numOwned,
                                   netStatus* status)
{
    if(!rlVerify(tableId && gamerHandle.IsValid() && numOwned))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_NumOwned = numOwned;

    char handleStr[RL_MAX_HANDLE_CHARS];
    if(!gamerHandle.ToString(handleStr, sizeof(handleStr)))
    {
        rlTaskError("gamerhandle tostring failed");
        return false;
    }

    formatf(m_Filter, 
            sizeof(m_Filter), 
            "%s = '%s'", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE),
            handleStr);

    return true;
}

void 
rlUploadGetNumOwnedTask::Start()
{
    rlUploadTask::Start();

    if(!GetDbClient()->GetNumRecords(m_TableId,
                                  m_Filter,
                                  m_NumOwned,
                                  &m_MyStatus))
    {
        rlTaskError("Failed to start request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadGetNumOwnedTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadGetNumOwnedTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordsTask
///////////////////////////////////////////////////////////////////////////////
rlUploadReadRecordsTask::rlUploadReadRecordsTask()
: m_TableId(0)
, m_Records(0)
, m_NumRecordsRead(0)
, m_Values(0)
{
}

rlUploadReadRecordsTask::~rlUploadReadRecordsTask()
{
    Clear();
}

void 
rlUploadReadRecordsTask::Clear()
{
    if(m_Values)
    {
        rlDelete(m_Values, m_Schema.GetNumFields() * m_DbResults.GetMaxRecords());
        m_Values = 0;
    }
}

bool 
rlUploadReadRecordsTask::Configure(rlUpload* ctx, 
                                   const char* tableId,
                                   const char* filter,
                                   const char* sort,
                                   const unsigned startingRank,
                                   const char* postSortFilter,
                                   const unsigned radius,
                                   const bool cacheResults,
                                   const unsigned maxRecords,
                                   rlUploadRecord* records,
                                   unsigned* numRecordsRead,
                                   netStatus* status)
{
    rtry
    {
        if(!rlVerify(tableId && filter && sort && (startingRank >= 1) && postSortFilter && maxRecords && records && numRecordsRead && status))
        {
            rlTaskError("Invalid arguments");
            return false;
        }

        Clear();

        unsigned numFields = rlUploadRecord::NUM_FIELDS;
        unsigned numValues = numFields * maxRecords;

        m_Values = RL_NEW_ARRAY(rlUploadReadRecordsTask, rlDbValue, numValues);
        rcheck(m_Values, catchall, rlTaskError("Failed to allocate %u values", numValues));
        
        m_Schema.Clear();
        for(unsigned i = 0; i < numFields; i++)
        {
            m_Schema.AddField(sSakeFieldNames[i], sSakeFieldTypes[i]);
        }

        m_DbResults.Reset(&m_Schema, maxRecords, m_Values);

        rcheck(rlUploadTask::Configure(ctx, status), catchall, rlTaskError("Failed to configure base class"));

        m_TableId = tableId;
        m_Filter = filter;
        m_Sort = sort;
        m_StartingRank = startingRank;
        m_PostSortFilter = postSortFilter;
        m_Radius = radius;
        m_CacheResults = cacheResults;

        m_Records = records;
        m_NumRecordsRead = numRecordsRead;
    }
    rcatchall
    {
        Clear();
    }

    return true;
}

void 
rlUploadReadRecordsTask::Start()
{
    rlUploadTask::Start();

    if(!GetDbClient()->ReadRecords(m_TableId, 
                                m_Filter,
                                m_Sort,
                                m_StartingRank,
                                m_PostSortFilter,
                                m_Radius, //Radius
                                m_CacheResults,
                                m_DbResults.GetMaxRecords(),
                                &m_DbResults,
                                &m_MyStatus))
    {
        rlTaskError("Failed to start request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadReadRecordsTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        else
        {
            Finish(DbResultsToUploadRecords(m_DbResults, 
                                            m_Records, 
                                            m_DbResults.GetMaxRecords(),
                                            m_NumRecordsRead,
                                            true) ? FINISH_SUCCEEDED : FINISH_FAILED);
        }
    }
}

void 
rlUploadReadRecordsTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordsByGamerTask
///////////////////////////////////////////////////////////////////////////////
rlUploadReadRecordsByGamerTask::rlUploadReadRecordsByGamerTask()
{
}

rlUploadReadRecordsByGamerTask::~rlUploadReadRecordsByGamerTask()
{
}

bool 
rlUploadReadRecordsByGamerTask::Configure(rlUpload* ctx, 
                                          const char* tableId,
                                          const rlGamerHandle& gamerHandle,
                                          const unsigned maxRecords,
                                          rlUploadRecord* records,
                                          unsigned* numRecordsRead,
                                          netStatus* status)
{
    if(!rlVerify(gamerHandle.IsValid()))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    formatf(m_Sort, 
            sizeof(m_Sort), 
            "%s desc, %s desc", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_AVG_RATING),
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RECORD_ID));

    char handleStr[RL_MAX_HANDLE_CHARS];
    if(!gamerHandle.ToString(handleStr, sizeof(handleStr)))
    {
        rlError("gamerhandle tostring failed");            
        return false;
    }

    formatf(m_PostSortFilter, 
            sizeof(m_PostSortFilter), 
            "%s = '%s'", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE),
            handleStr);

    return rlUploadReadRecordsTask::Configure(ctx, 
                                              tableId,
                                              "",
                                              m_Sort,
                                              1,
                                              m_PostSortFilter,
                                              0,
                                              false,
                                              maxRecords,
                                              records,
                                              numRecordsRead,
                                              status);
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordsByRankTask
///////////////////////////////////////////////////////////////////////////////
rlUploadReadRecordsByRankTask::rlUploadReadRecordsByRankTask()
{
}

rlUploadReadRecordsByRankTask::~rlUploadReadRecordsByRankTask()
{
}

bool 
rlUploadReadRecordsByRankTask::Configure(rlUpload* ctx, 
                                         const char* tableId,
                                         const unsigned startingRank,
                                         const unsigned minNumRatings,
                                         const unsigned maxRecords,
                                         rlUploadRecord* records,
                                         unsigned* numRecordsRead,
                                         netStatus* status)
{
    formatf(m_Sort, 
            sizeof(m_Sort), 
            "%s desc, %s desc", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_AVG_RATING),
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RECORD_ID));

    if(minNumRatings)
    {
        formatf(m_Filter, 
                sizeof(m_Filter), 
                "(%s >= %u)", 
                rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
                minNumRatings);
    }
    else
    {
        formatf(m_Filter, sizeof(m_Filter), "recordid > 0");
    }

    return rlUploadReadRecordsTask::Configure(ctx, 
                                              tableId,
                                              m_Filter,
                                              m_Sort,
                                              startingRank,
                                              "",
                                              0,
                                              true,
                                              maxRecords,
                                              records,
                                              numRecordsRead,
                                              status);
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRecordTask
///////////////////////////////////////////////////////////////////////////////
rlUploadReadRecordTask::rlUploadReadRecordTask()
{
}

rlUploadReadRecordTask::~rlUploadReadRecordTask()
{
}

bool 
rlUploadReadRecordTask::Configure(rlUpload* ctx, 
                                  const char* tableId,
                                  const int recordId,
                                  rlUploadRecord* record,
                                  unsigned* numRecordsRead,
                                  netStatus* status)
{
    if(!rlVerify(recordId != 0))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    formatf(m_Sort, 
            sizeof(m_Sort), 
            "%s desc, %s desc", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_AVG_RATING),
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RECORD_ID));

    formatf(m_Filter, 
            sizeof(m_Filter), 
            "%s = %d", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_RECORD_ID),
            recordId);

    return rlUploadReadRecordsTask::Configure(ctx, 
                                              tableId,
                                              m_Filter,
                                              m_Sort,
                                              1,
                                              "",
                                              0,
                                              true,
                                              1,
                                              record,
                                              numRecordsRead,
                                              status);
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadReadRandomRecordTask
///////////////////////////////////////////////////////////////////////////////
rlUploadReadRandomRecordTask::rlUploadReadRandomRecordTask()
: m_TableId(0)
, m_Record(0)
, m_NumRecordsRead(0)
{
}

rlUploadReadRandomRecordTask::~rlUploadReadRandomRecordTask()
{
}

bool 
rlUploadReadRandomRecordTask::Configure(rlUpload* ctx, 
                                        const char* tableId,
                                        const rlGamerHandle& gamerHandle,
                                        const unsigned minNumRatings,
                                        const unsigned maxNumRatings,
                                        const u32 contentIdFlags,
                                        rlUploadRecord* record,
                                        unsigned* numRecordsRead,
                                        netStatus* status)
{
    unsigned numFields = COUNTOF(m_Values);

    m_Schema.Clear();
    for(unsigned i = 0; i < numFields; i++)
    {
        m_Schema.AddField(sSakeFieldNames[i], sSakeFieldTypes[i]);
    }

    m_DbResults.Reset(&m_Schema, 1, m_Values);

    if(!rlVerify(tableId && gamerHandle.IsValid() && record && numRecordsRead))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_Record = record;
    m_NumRecordsRead = numRecordsRead;

    char handleStr[RL_MAX_HANDLE_CHARS];
    if(!gamerHandle.ToString(handleStr, sizeof(handleStr)))
    {
        rlTaskError("gamerhandle tostring failed");
        return false;
    }

    m_Filter[0] = '\0';

    if(maxNumRatings)
    {
        formatf(m_Filter, 
                sizeof(m_Filter), 
                "(%s <= %u) and ", 
                rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
                maxNumRatings);
    }

    char buf[256];
    formatf(buf, 
            sizeof(buf), 
            "(%s != '%s')", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE),
            handleStr);
    safecat(m_Filter, buf, sizeof(m_Filter));

    if(minNumRatings)
    {
        formatf(buf, 
                sizeof(buf), 
                " and (%s >= %u)", 
                rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_RATINGS),
                minNumRatings);
        safecat(m_Filter, buf, sizeof(m_Filter));
    }

    if(contentIdFlags)
    {
        safecat(m_Filter, " and (", sizeof(m_Filter));

        u8 numFound = 0;

        for(unsigned i = 0; i < (sizeof(contentIdFlags)*8); i++)
        {
            if(contentIdFlags & (1 << i))
            {
                formatf(buf, sizeof(buf), "%s%s=%u",
                        (numFound != 0) ? " or " : "",
                        rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_CONTENT_ID),
                        i);
                
                safecat(m_Filter, buf, sizeof(m_Filter));

                ++numFound;
            }
        }

        safecat(m_Filter, ")", sizeof(m_Filter));
    }

    rlTaskDebug3("Final filter (%u chars): %s", strlen(m_Filter), m_Filter);

    return true;
}

void 
rlUploadReadRandomRecordTask::Start()
{
    rlUploadTask::Start();

    if(!GetDbClient()->ReadRandomRecord(m_TableId, 
                                     m_Filter, 
                                     &m_DbResults, 
                                     &m_MyStatus))
    {
        rlTaskError("Failed to start request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadReadRandomRecordTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        else
        {
            Finish(DbResultsToUploadRecords(m_DbResults, 
                                            m_Record, 
                                            1,
                                            m_NumRecordsRead,
                                            false) ? FINISH_SUCCEEDED : FINISH_FAILED);
        }
    }
}

void 
rlUploadReadRandomRecordTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}


///////////////////////////////////////////////////////////////////////////////
// rlUploadRateRecordTask
///////////////////////////////////////////////////////////////////////////////
rlUploadRateRecordTask::rlUploadRateRecordTask()
: m_TableId(0)
, m_Rating(0)
, m_NewRecord(0)
, m_NumRatings(0)
, m_AvgRating(0)
{
}

rlUploadRateRecordTask::~rlUploadRateRecordTask()
{
}

bool 
rlUploadRateRecordTask::Configure(rlUpload* ctx, 
                                  const char* tableId,
                                  const rlUploadRecord& record,
                                  const u8 rating,
                                  rlUploadRecord* newRecord,
                                  netStatus* status)
{
    if(!rlVerify(tableId && record.IsValid()))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_Record = record;
    m_Rating = rating;
    m_NewRecord = newRecord;
    
    return true;
}

void 
rlUploadRateRecordTask::Start()
{
    rlUploadTask::Start();

    if(!GetDbClient()->RateRecord(m_TableId,
                               m_Record.GetRecordId(),
                               m_Record.GetNumRatings(),
                               m_Record.GetAverageRating(),
                               m_Rating,
                               &m_NumRatings,
                               &m_AvgRating,
                               &m_MyStatus))
    {
        rlTaskError("Failed to start request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadRateRecordTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadRateRecordTask::Finish(const FinishType finishType, const int resultCode)
{
    if((RLDB_RESULT_SUCCESS == resultCode) && m_NewRecord)
    {
        *m_NewRecord = m_Record;
        m_NewRecord->m_NumRatings = m_NumRatings;
        m_NewRecord->m_AvgRating = m_AvgRating;
        m_NewRecord->m_Rank = rlUploadRecord::UNKNOWN_RANK;
    }

    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadDeleteRecordTask
///////////////////////////////////////////////////////////////////////////////
rlUploadDeleteRecordTask::rlUploadDeleteRecordTask()
: m_TableId(0)
, m_RecordId(0)
{
}

rlUploadDeleteRecordTask::~rlUploadDeleteRecordTask()
{
}

bool 
rlUploadDeleteRecordTask::Configure(rlUpload* ctx, 
                                    const char* tableId,
                                    const int recordId,
                                    netStatus* status)
{
    if(!rlVerify(tableId && (recordId != 0)))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_RecordId = recordId;

    return true;
}

void 
rlUploadDeleteRecordTask::Start()
{
    rlUploadTask::Start();

    if(!GetDbClient()->DeleteRecord(m_TableId,
                                 m_RecordId,
                                 &m_MyStatus))
    {
        rlTaskError("Failed to start request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadDeleteRecordTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadDeleteRecordTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadUploadFileTask
///////////////////////////////////////////////////////////////////////////////
rlUploadUploadFileTask::rlUploadUploadFileTask()
: m_State(STATE_IDLE)
, m_RecordLimit(0)
, m_GamerName(0)
, m_NumFiles(0)
, m_FileData(0)
, m_FileSizes(0)
, m_UserData(0)
, m_UserDataSize(0)
, m_ContentId(rlUploadRecord::DEFAULT_CONTENT_ID)
, m_Record(0)
, m_Progress(0)
, m_NumOwned(0)
, m_RecordId(0)
, m_CurFile(0)
{
    sysMemSet(m_FileIds, 0, sizeof(m_FileIds));
}

rlUploadUploadFileTask::~rlUploadUploadFileTask()
{
}

bool 
rlUploadUploadFileTask::Configure(rlUpload* ctx, 
                                  const char* tableId,
                                  const unsigned recordLimit,
                                  const rlGamerHandle& gamerHandle,
                                  const char* gamerName,
                                  const unsigned numFiles,
                                  const u8** fileData,
                                  const unsigned* fileSizes,
                                  const void* userData,
                                  const unsigned userDataSize,
                                  const u8 contentId,
                                  rlUploadRecord* record,
                                  float* progress,                                    
                                  netStatus* status)
{
    if(!rlVerify(tableId && recordLimit && gamerHandle.IsValid() && gamerName && numFiles && fileData && fileSizes && record && progress && (contentId < rlUploadRecord::MAX_CONTENT_IDS)))
    {
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_RecordLimit = recordLimit;
    m_GamerHandle = gamerHandle;
    m_GamerName = gamerName;
    m_NumFiles = numFiles;
    m_FileData = fileData;
    m_FileSizes = fileSizes;
    m_UserData = userData;
    m_UserDataSize = userDataSize;
    m_ContentId = contentId;
    m_Record = record;
    m_Progress = progress;

    return true;
}

void 
rlUploadUploadFileTask::Start()
{
    rlUploadTask::Start();

    //First, max sure the player is not at the max allowed records already.
    if(!m_GamerHandle.ToString(m_HandleStr, sizeof(m_HandleStr)))
    {
        rlTaskError("gamerhandle tostring failed");
        Finish(FINISH_FAILED);
        return;
    }

    formatf(m_Filter, 
            sizeof(m_Filter), 
            "%s = '%s'", 
            rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE),
            m_HandleStr);

    if(!GetDbClient()->GetNumRecords(m_TableId,
                                  m_Filter,
                                  &m_NumOwned,
                                  &m_MyStatus))
    {
        rlTaskError("Failed to start get num records request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        return;
    }

    m_State = STATE_GET_NUM_OWNED;
}

void 
rlUploadUploadFileTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    switch(m_State)
    {
    case STATE_GET_NUM_OWNED:
        if(m_MyStatus.Succeeded())
        {
            //See if gamer is below their upload limit.
            if(m_NumOwned >= m_RecordLimit)
            {
                rlTaskDebug2("Gamer %s already at or past record limit (%u, %u); cannot upload new file",
                          m_GamerName,
                          m_NumOwned,
                          m_RecordLimit);
                Finish(FINISH_FAILED, RLDB_RESULT_RECORD_LIMIT_REACHED);
                return;
            }

            m_CurFile = 0;
            m_State = STATE_UPLOAD_FILE;
        }
        else if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        break;

    case STATE_UPLOAD_FILE:
        {
            rlTaskDebug3("Starting file %d upload", m_CurFile);
            
            if(!GetDbClient()->UploadFile(m_FileData[m_CurFile],
                                       m_FileSizes[m_CurFile],
                                       m_Progress,
                                       &m_FileIds[m_CurFile],
                                       &m_MyStatus))
            {
                rlTaskError("Failed to start upload req");
                Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
                return;
            }

            m_State = STATE_UPLOADING_FILE;
        }
        break;

    case STATE_UPLOADING_FILE:
        if(m_MyStatus.Succeeded())
        {
            rlTaskDebug3("File %d successfully uploaded", m_CurFile);

            ++m_CurFile;
            m_State = (m_CurFile < m_NumFiles) ? STATE_UPLOAD_FILE : STATE_CREATE_RECORD;
        }
        else if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        break;

    case STATE_CREATE_RECORD:
        {
            rlTaskDebug3("Starting create record");

            //Create a new record, using the uploaded fileIds
            rlDbField* f = m_Fields;
            unsigned numFields = 0;

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE));
            f[numFields].GetValue().SetAsciiString(m_HandleStr);
            ++numFields;

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_NAME));
            f[numFields].GetValue().SetAsciiString(m_GamerName);
            ++numFields;

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_FILES));
            f[numFields].GetValue().SetInt32(m_NumFiles);
            ++numFields;

            for(unsigned i = 0; i < m_NumFiles; i++)
            {
                f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE0_ID + i));
                f[numFields].GetValue().SetInt32(m_FileIds[i]);
                ++numFields;
            }

            if(m_UserDataSize)
            {
                f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_USER_DATA));
                f[numFields].GetValue().SetBinaryData((u8*)m_UserData, 
                                                      m_UserDataSize, 
                                                      rlDbBinaryData::MODE_COPY_PTR_ONLY);
                ++numFields;
            }

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_CONTENT_ID));
            f[numFields].GetValue().SetInt32(m_ContentId);
            ++numFields;

            rlAssert(numFields <= NUM_FIELDS);

            if(!GetDbClient()->CreateRecord(m_TableId,
                                         (rlDbField*)m_Fields,
                                         numFields,
                                         &m_RecordId,
                                         &m_MyStatus))
            {
                rlTaskError("CreateRecord failed");
                Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
                return;
            }

            m_State = STATE_CREATING_RECORD;
        }
        break;

    case STATE_CREATING_RECORD:
        if(m_MyStatus.Succeeded())
        {
            rlTaskDebug3("Record created");

            m_Record->Reset(m_GamerHandle,
                            m_GamerName,
                            m_RecordId,
                            m_NumFiles, 
                            m_FileIds,
                            m_FileSizes,
                            0,     //Clear num ratings
                            0,     //Clear avg rating
                            0,     //Clear rank
                            (const u8*)m_UserData,
                            m_UserDataSize,
                            m_ContentId);

            Finish(FINISH_SUCCEEDED);
        }
        else if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        break;

    default:
        rlTaskError("Unhandled state: %d", m_State);
    }
}

void 
rlUploadUploadFileTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadOverwriteFileTask
///////////////////////////////////////////////////////////////////////////////
rlUploadOverwriteFileTask::rlUploadOverwriteFileTask()
: m_State(STATE_IDLE)
, m_NumFiles(0)
, m_FileData(0)
, m_FileSizes(0)
, m_UserData(0)
, m_UserDataSize(0)
, m_ContentId(rlUploadRecord::DEFAULT_CONTENT_ID)
, m_NewRecord(0)
, m_Progress(0)
, m_RecordId(0)
, m_CurFile(0)
{
    sysMemSet(m_FileIds, 0, sizeof(m_FileIds));
}

rlUploadOverwriteFileTask::~rlUploadOverwriteFileTask()
{
}

bool 
rlUploadOverwriteFileTask::Configure(rlUpload* ctx, 
                                     const char* tableId,
                                     const rlUploadRecord& currentRecord,
                                     const unsigned numFiles,
                                     const u8** fileData,
                                     const unsigned* fileSizes,
                                     const void* userData,
                                     const unsigned userDataSize,
                                     const u8 contentId,
                                     rlUploadRecord* newRecord,
                                     float* progress,
                                     netStatus* status)
{
    if(!rlVerify(tableId && currentRecord.IsValid() && numFiles && fileData && fileSizes && newRecord && progress && (contentId < rlUploadRecord::MAX_CONTENT_IDS)))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_CurrentRecord = currentRecord;
    m_NumFiles = numFiles;
    m_FileData = fileData;
    m_FileSizes = fileSizes;
    m_UserData = userData;
    m_UserDataSize = userDataSize;
    m_ContentId = contentId;
    m_NewRecord = newRecord;
    m_Progress = progress;

    return true;
}

void 
rlUploadOverwriteFileTask::Start()
{
    rlUploadTask::Start();

    m_CurFile = 0;
    m_State = STATE_UPLOAD_FILE;
}

void 
rlUploadOverwriteFileTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    switch(m_State)
    {
    case STATE_UPLOAD_FILE:
        {
            if(!GetDbClient()->UploadFile(m_FileData[m_CurFile],
                                       m_FileSizes[m_CurFile],
                                       m_Progress,
                                       &m_FileIds[m_CurFile],
                                       &m_MyStatus))
            {
                rlTaskError("Failed to start upload req");
                Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
                return;
            }

            m_State = STATE_UPLOADING_FILE;
        }
        break;

    case STATE_UPLOADING_FILE:
        if(m_MyStatus.Succeeded())
        {
            ++m_CurFile;
            m_State = (m_CurFile < m_NumFiles) ? STATE_UPLOAD_FILE : STATE_DELETE_RECORD;
        }
        else if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        break;

    case STATE_DELETE_RECORD:
        {
            //Delete the existing record.  We do this because there is currently no
            //way to reset ratings in a rateable table.
            if(!GetDbClient()->DeleteRecord(m_TableId,
                                         m_CurrentRecord.m_RecordId,
                                         &m_MyStatus))
            {
                rlTaskError("Failed to start delete record request");
                Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
                return;
            }

            m_State = STATE_DELETING_RECORD;
        }
        break;

    case STATE_DELETING_RECORD:
        if(m_MyStatus.Succeeded())
        {
            m_State = STATE_CREATE_RECORD;
        }
        else if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        break;

    case STATE_CREATE_RECORD:
        {
            if(!m_CurrentRecord.GetGamerHandle().ToString(m_HandleStr, sizeof(m_HandleStr)))
            {
                rlTaskError("gamerhandle tostring failed");
                Finish(FINISH_FAILED);
                return;
            }

            rlDbField* f = m_Fields;
            unsigned numFields = 0;

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_HANDLE));
            f[numFields].GetValue().SetAsciiString(m_HandleStr);
            ++numFields;

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_GAMER_NAME));
            f[numFields].GetValue().SetAsciiString(m_CurrentRecord.GetGamerName());
            ++numFields;

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_NUM_FILES));
            f[numFields].GetValue().SetInt32(m_NumFiles);
            ++numFields;

            for(unsigned i = 0; i < m_NumFiles; i++)
            {
                f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_FILE0_ID + i));
                f[numFields].GetValue().SetInt32(m_FileIds[i]);
                ++numFields;
            }

            if(m_UserDataSize)
            {
                f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_USER_DATA));
                f[numFields].GetValue().SetBinaryData((u8*)m_UserData, 
                                                      m_UserDataSize, 
                                                      rlDbBinaryData::MODE_COPY_PTR_ONLY);
                ++numFields;
            }

            f[numFields].SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_CONTENT_ID));
            f[numFields].GetValue().SetInt32(m_ContentId);
            ++numFields;

            rlAssert(numFields <= NUM_FIELDS);

            if(!GetDbClient()->CreateRecord(m_TableId,
                                         (rlDbField*)m_Fields,
                                         numFields,
                                         &m_RecordId,
                                         &m_MyStatus))
            {
                rlTaskError("CreateRecord failed");
                Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
                return;
            }

            m_State = STATE_CREATING_RECORD;
        }
        break;

    case STATE_CREATING_RECORD:
        if(m_MyStatus.Succeeded())
        {
            m_NewRecord->Reset(m_CurrentRecord.GetGamerHandle(),
                               m_CurrentRecord.GetGamerName(),
                               m_RecordId,
                               m_NumFiles,
                               m_FileIds,
                               m_FileSizes,
                               0,     //Clear num ratings
                               0,     //Clear avg rating
                               0,     //Clear rank
                               (const u8*)m_UserData,
                               m_UserDataSize,
                               m_ContentId);

            Finish(FINISH_SUCCEEDED);
        }
        else if(m_MyStatus.Failed())
        {
            Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        }
        break;

    default:
        rlTaskError("Unhandled state: %d", m_State);
    }
}

void 
rlUploadOverwriteFileTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadDownloadFileTask
///////////////////////////////////////////////////////////////////////////////
rlUploadDownloadFileTask::rlUploadDownloadFileTask()
: m_FileId(0)
, m_Buf(0)
, m_BufSize(0)
, m_BytesDownloaded(0)
, m_Progress(0)
{
}

rlUploadDownloadFileTask::~rlUploadDownloadFileTask()
{
}

bool 
rlUploadDownloadFileTask::Configure(rlUpload* ctx, 
                                    const int fileId,
                                    u8* buf,
                                    const unsigned bufSize,
                                    unsigned* bytesDownloaded,
                                    float* progress,
                                    netStatus* status)
{
    if(!rlVerify((fileId != 0) && buf && bufSize && bytesDownloaded && progress))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_FileId = fileId;
    m_Buf = buf;
    m_BufSize = bufSize;
    m_BytesDownloaded = bytesDownloaded;
    m_Progress = progress;
    
    return true;
}

void 
rlUploadDownloadFileTask::Start()
{
    rlUploadTask::Start();

    if(!GetDbClient()->DownloadFile(m_FileId,
                                 m_Buf,
                                 m_BufSize,
                                 m_Progress,
                                 m_BytesDownloaded,
                                 &m_MyStatus))
    {
        rlTaskError("Failed to start request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadDownloadFileTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadDownloadFileTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();
}

///////////////////////////////////////////////////////////////////////////////
// rlUploadOverwriteUserDataTask
///////////////////////////////////////////////////////////////////////////////
rlUploadOverwriteUserDataTask::rlUploadOverwriteUserDataTask()
: m_TableId(0)
, m_UserData(0)
, m_UserDataSize(0)
, m_NewRecord(0)
{
}

rlUploadOverwriteUserDataTask::~rlUploadOverwriteUserDataTask()
{
}

bool 
rlUploadOverwriteUserDataTask::Configure(rlUpload* ctx, 
                                         const char* tableId,
                                         const rlUploadRecord& currentRecord,
                                         const void* userData,
                                         const unsigned userDataSize,
                                         rlUploadRecord* newRecord,
                                         netStatus* status)
{
    if(!rlVerify(tableId && userData && userDataSize && newRecord))
    {
        rlTaskError("Invalid arguments");
        return false;
    }

    if(!rlUploadTask::Configure(ctx, status))
    {
        return false;
    }

    m_TableId = tableId;
    m_CurrentRecord = currentRecord;
    m_UserData = userData;
    m_UserDataSize = userDataSize;
    m_NewRecord = newRecord;

    return true;
}

void 
rlUploadOverwriteUserDataTask::Start()
{
    rlUploadTask::Start();

    rlDbField* f = m_Fields;

    f->SetName((char*)rlUploadRecord::GetFieldName(rlUploadRecord::FIELD_USER_DATA));
    f->GetValue().SetBinaryData((u8*)m_UserData, 
                                m_UserDataSize,
                                rlDbBinaryData::MODE_COPY_PTR_ONLY);

    if(!GetDbClient()->UpdateRecord(m_TableId,
                                 m_CurrentRecord.GetRecordId(),
                                 (rlDbField*)m_Fields,
                                 NUM_FIELDS,
                                 &m_MyStatus))
    {
        rlTaskError("Failed to start update record request");
        Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
        return;
    }
}

void 
rlUploadOverwriteUserDataTask::Update(const unsigned timeStep)
{
    rlUploadTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED, m_MyStatus.GetResultCode());
    }
}

void 
rlUploadOverwriteUserDataTask::Finish(const FinishType finishType, const int resultCode)
{
    rlUploadTask::Finish(finishType, resultCode);
    m_MyStatus.Reset();

    if(0 == resultCode)
    {
        int fileIds[rlUploadRecord::MAX_FILES];
        unsigned fileSizes[rlUploadRecord::MAX_FILES];

        for(unsigned i = 0; i < m_CurrentRecord.GetNumFiles(); i++)
        {
            fileIds[i] = m_CurrentRecord.GetFileId(i);
            fileSizes[i] = m_CurrentRecord.GetFileSize(i);
        }

        m_NewRecord->Reset(m_CurrentRecord.GetGamerHandle(),
                           m_CurrentRecord.GetGamerName(),
                           m_CurrentRecord.GetRecordId(),
                           m_CurrentRecord.GetNumFiles(),
                           fileIds,
                           fileSizes,
                           m_CurrentRecord.GetNumRatings(),
                           m_CurrentRecord.GetAverageRating(),
                           m_CurrentRecord.GetRank(),
                           (const u8*)m_UserData,
                           m_UserDataSize,
                           m_CurrentRecord.GetContentId());
    }
}

}   //namespace rage
