// 
// rline/rlfriendsmanager.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLFRIENDSMANAGER_H
#define RLINE_RLFRIENDSMANAGER_H

#include "atl/array.h"
#include "atl/delegate.h"
#include "net/status.h"
#include "net/time.h"
#include "rline/rlfriendevents.h"
#include "rline/rlfriendsreader.h"
#include "rline/rlfriend.h"
#include "rline/rlpc.h"
#include "rline/durango/rlxbl_interface.h"

#if RSG_NP
#include "rline/rlnp.h"
#endif

#define ALLOW_MANUAL_FRIEND_REFRESH (RSG_DURANGO || RSG_PC)

namespace rage
{

// Contains a subset of friends from the full list
class rlFriendsPage
{
public:
	// PURPOSE
	rlFriendsPage();
	~rlFriendsPage();

	// PURPOSE
	//	Initializes the friends page with a size of maxFriends
	bool Init(const int maxFriends = MAX_FRIEND_PAGE_SIZE);

	// PURPOSE
	//	Shuts down the friends page, clearing out friends and freeing memory
	void Shutdown();

	//PURPOSE
	//	Clears the friends data, resets the number of friends 
	void ClearFriendsArray();

	//PURPOSE
	//	Determines if a Friends Page contains the data needed for another friends page
	bool Contains(rlFriendsPage* page);

	//PURPOSE
	//	Return the friend who matches the given gamer handle
	rlFriend* GetFriend(const rlGamerHandle& gamerHandle);

	//PURPOSE
	//	Return the index of a friend which matches the friend name
	int GetFriendIndex(const char* friendName) { return GetFriendIndex(friendName, 0 , m_NumFriends); }

	//PURPOSE
	//	Return the index of a friend which matches the friend name
	int GetFriendIndex(const char* friendName, const unsigned startIndex, unsigned count);

	//PURPOSE
	// Fills out an array with the correct number of friends and their gamer handles
	void FillFriendArray(atArray<rlGamerHandle>& out_FilledArray);

	// PURPOSE
	//	Handle a friend is fully updated
	bool OnFriendUpdated(const rlFriend& f);

	//PURPOSE
	// Handle a change in a friend's online status
	void OnFriendStatusChange(const rlFriend& f);

	//PURPOSE
	// Handle a change in a friend's title status
	void OnFriendTitleChange(const rlFriend& f);

	//PURPOSE
	// Handle a change in a friend's title status
	void OnFriendSessionChange(const rlFriend& f);

	//PURPOSE
	//	Sorts the friends list by ID and Status
	void Sort();

	int m_StartIndex;
	unsigned int m_NumFriends;
	unsigned int m_MaxFriends;
	rlFriend* m_Friends;

#if RSG_PC
	static const int MAX_FRIEND_PAGE_SIZE = 250;
#else
	static const int MAX_FRIEND_PAGE_SIZE = 100;
#endif
};

//PURPOSE
//  Manages a full list of friends
class rlFriendsManager
{
	typedef atDelegator<void (const rlFriendEvent*)> Delegator;
    friend class rlFriendsReader;

public:
	enum
	{
		INVALID_FRIEND_INDEX = -1,
		MAX_PRESENCE_PAGE_SIZE = 32
	};

	static const int DEFAULT_TIME_BETWEEN_REFRESH = 30 * 1000;

	static const int RETRY_TIMERS = 4;
	static int ACTIVATION_FAILURE_RETRY_TIMER[RETRY_TIMERS];

	//PURPOSE
	//  Delegate for handling events.
	//  See rlclancommon.h for event types.
	typedef Delegator::Delegate Delegate;

	//PURPOSE
	//	Initializes the rlFriendsReader (calls Clear internally).
	//	Cannot be re-initialized.
	static bool InitClass();

	//PURPOSE
	//	Updates the friends manager state machine and internal friends reader
	static void Update();

	//PURPOSE
	//	Shuts down the friends manager, canceling all requests and clearing all friends.
	static bool ShutdownClass();

	//PURPOSE
	//	Returns true if the platform can perform a back-end presort of the friends list.
	static bool CanPresort();

	//PURPOSE
	//	Activates the friend manager for the given local gamer index. Due to the large memory
	//	requirements for XB1/PS4 friend lists, only one gamer index can be activated at a time.
	// Note: A friends page could still be requested for another gamer index. If we ever attempt
	//	to add multiplayer/local co-op titles, caching upwards of 8000 friends is not feasible. 
	//	Smaller, synchronous only queries would need to be used and possibly Microsoft can sort out their backend.
	static bool Activate(const int localGamerIndex);

	//PURPOSE
	//	Deactivates the friend manager. Calls Clear() internally. Must be called before
	//	attempting to activate a new gamer index.
	static bool Deactivate();

	//PURPOSE
	//	Refreshes and re-activates friend manager. Calls clear() internally, and re-reads the friends list.
	static bool Reactivate();

	// PURPOSE
	//	Returns TRUE if the Activation Status is pending
	static bool IsActivating() { return sm_ActivationStatus.Pending(); }

	//PURPOSE
	//	Returns true if the friend's manager is currently reading a list of friends
	static bool IsReading();
	static bool IsSyncingFriends();
	static bool IsOutofSync();

	//PURPOSE
	//	Returns true if the friends manager is in an initialized state
	static bool IsInitialized() { return sm_bInitialized; }

	//PURPOSE
	//	Returns true if the friend's manager is in an activated state
	static bool IsActivated() { return sm_ActivateState == STATE_ACTIVATED; }

	//PURPOSE
	//	Returns true if the friends manager activation fails
	static bool ActivationFailed() { return sm_ActivateState == STATE_ACTIVATION_FAILED; }

	//PURPOSE
	//	Returns the size of a user's friends list. Not all of these friend's may be loaded into memory.
	static unsigned GetTotalNumFriends(const int localGamerIndex);

	//PURPOSE
	//	Returns the count of the most recent friends reader count
	static unsigned GetFriendReadResultCount() { return sm_FriendsReadResultCount; }

	//PURPOSE
	//	Asynchronously fills a container of friends. If the specified request is already in memory,
	//	the netStatus object will be SetSucceeded immediately.
	static bool GetFriends(const int localGamerIndex, rlFriendsPage* page, int friendFlags, netStatus* status);

	//PURPOSE
	//	Returns true if the user is friend's with another user.
	//	Warning: This function should not be called every frame.
	static bool IsFriendsWith(const int localGamerIndex, const rlGamerHandle& gamer);

	//PURPOSE
	// Returns true if the user is a pending friend
	//	Warning: This function should not be called every frame
	static bool IsPendingFriend(const int localGamerIndex, const rlGamerHandle& gamer);

	//PURPOSE
	//	Returns true if the manager is currently sorting friends
	static bool IsSortingFriends();

	//PURPOSE
	//	Returns the friend's index in the IdReferences array, or INVALID_FRIEND_INDEX if not found
	static int GetFriendIndex(const int localGamerIndex, const rlGamerHandle& gamer);

	//PURPOSE
	//	Returns the gamer handle of a friend at the given index
	static rlGamerHandle* GetGamerHandle(const int friendIndex, rlGamerHandle* hGamer);

#if RSG_PC
	//PURPOSE
	static rlFriendsPage* GetDefaultFriendPage() { return &sm_DefaultFriends; }
#endif
	
	//PURPOSE
	//	When a user adds a friend to their list, add a reference for it
	static bool AddFriendReferenceById(rlFriendId id, bool IsTwoWay = true);

	//PURPOSE
	// When a user removes a friend from their list, remove the reference
	static bool RemoveFriendReferenceByIndex(int index);

	//PURPOSE
	// Returns true if the friend reference at the given index is online, in the same title, in session, etc.
	static bool IsFriendOnline(const int friendIndex);
	static bool IsFriendInSameTitle(const int friendIndex);
	static bool IsFriendInSession(const int friendIndex);

	//PURPOSE
	//	Returns true if no friend refresh queries are in progress
	static bool CanQueueFriendsRefresh(const int localGamerIndex);
	static void ForceAllowFriendsRefresh();

	//PURPOSE
	//	Refreshes data on a subset of friends
	static bool RequestRefreshFriendsPage(const int localGamerIndex, rlFriendsPage* page, int startIndex, int numFriends, bool bRefreshPresence = true);

	//PURPOSE
	//	Requests a sync between the game and RGSC DLL version of the friends list
	static bool RequestFriendSync();

	//PURPOSE
	//	Requests the gamertag/display name for friends given their gamerhandle
	static bool LookupFriendsName(const int localGamerIndex, const rlGamerHandle* gamerHandles, const unsigned numGamerhandles, rlDisplayName* displayNames, rlGamertag* gamertags, netStatus* status);

	//PURPOSE
	// Checks the friends list for changes
	// NOTES:
	//	XB1 only
	static bool CheckListForChanges(const int localGamerIndex);

	//PURPOSE
	//  Add or remove a delegate that will be called with event notifications.
	static void AddDelegate(Delegate* dlgt);
	static void RemoveDelegate(Delegate* dlgt);

	//PURPOSE
	//	Cancels a friends read using the manager's reader
	static void CancelRead(netStatus* status);
	static void CancelTaskIfNecessary(netStatus* status);

	//PURPOSE
	// Platform Event callbacks
#if RSG_NP
	static void OnNpEvent(rlNp* np, const rlNpEvent* evt);
	static void UpdateFriendPresence(const int localGamerIndex, const rlSceNpAccountId accountId, const rlSceNpOnlineId& onlineId, bool bIsOnline, bool bIsInSameTitle, const void *data, const unsigned dataSize, bool bRaiseEvent = true);
#elif RSG_PC
	static void OnPcEvent(rlPc* pc, const rlPcEvent* evt);
#elif RSG_DURANGO
	static void OnXblEvent(rlXbl* xbl, const rlXblEvent* evt);
#endif

	// PURPOSE
	//  Handle the event when a friend's rich presence has changed.
	static void OnFriendRichPresenceChanged(const rlGamerHandle& gh, const char* presenceString);

	//PURPOSE
	//	Dispatches rlFriendEvents 
	static void DispatchEvent(const rlFriendEvent* e);

private:

	// PURPOSE
	//	Does a platform/title analysis to determine if subscriptions to
	//  presence should be done.
	static bool ShouldSubscribeToPresence();

	//PURPOSE
	//	Performs a binary search for a friend ID
	//	Returns the friend index, or -1 if not found.
	static int BinarySearchFriends(rlFriendId id);

	//PURPOSE
	//	Sorts the friends list by ID and Status
	static void SortFriends();

	//PURPOSE
	// Empties out the friend arrays and clears all pending requests.
	static void Clear();

	//PURPOSE
	//	Initializes the initial friends list, subscribes to presence callbacks
	//PLATFORMS
	//  XB1: connects to RTA if necessary and subscribes to friends RTA callbacks
	//	PS4: connects to Web API push notifications
	//	 PC: does nothing, returns true
	static bool ReadAndSubscribeToFriends(bool bSubscribe);

	//PURPOSE
	//	Clears the cached friends, unsubscribes to presence callbacks
	//PLATFORMS
	//	XB1: disconnects from Realtime activity
	//	PS4: disconnects from Web API push notifications
	//	 PC: does nothing, return true
	static bool UnsubscribeToFriends();

	//PURPOSE
	// Update Friend SC Subscriptions (different than XB1/PSN subscriptions)
	static void UpdateScSubscriptions();

	// PURPOSE
	// Subscribe/Unsubscribe from SC
	static bool AddRemoveScSubscription(rlGamerHandle* gh, bool bSubscribe);

	enum ActivationState
	{
		STATE_IDLE,
		STATE_ACTIVATING,
		STATE_ACTIVATED,
		STATE_ACTIVATION_FAILED
	};

	// Activation and Initialization states
	static bool sm_bInitialized;
	static ActivationState sm_ActivateState;
	static int sm_ActiveGamerIndex;

	// Activation failure retries
	static int sm_CurrentActivationTimer;
	static netRetryTimer sm_ActivationRetryTimer;

	// Friend Reader, References and descriptors
	static rlFriendsReader sm_FriendsReader;
	static atArray<rlFriendsReference*> sm_FriendReferencesById;
	static unsigned sm_FriendsReadResultCount;
	static unsigned sm_NumFriends;

	// Friends Sorting
	static bool sm_bSortFriends;

	// Friends Refreshing
	static unsigned sm_LastFriendsRefresh;
	static bool sm_NeedToRefreshFriends;
	static bool sm_CheckForListChanges;
	static int sm_NumFriendsToRefresh;
	static u32 sm_TimeBetweenRefreshes;

	// Friends Presence/Sessions (PC) Refreshing
	static bool sm_RefreshPresence;
	static int sm_RefreshPresenceNumFriends;
	static int sm_RefreshPresenceStartIndex;

	// Default friends list. Contains your first 'N' friends to be a catchall for small friends lists, 
	//	your full friends list on PC and helps any friends functionality starting with index 0 to be
	// very responsive. (i.e. give me your first 100 online friends can return synchronously)
	static rlFriendsPage sm_DefaultFriends;

	// Various net status objects for reads, and soft refreshes of the list size/session status
	static netStatus sm_ReadStatus;
	static netStatus sm_ActivationStatus;
	static netStatus sm_XblFriendCountStatus;
	static netStatus sm_XblPresenceSessionStatus;

#if RSG_DURANGO
	static atArray<rlFriendsReference*> sm_FriendReferencesByStatus;
	static rlXblEventDelegator::Delegate sm_XblDlgt;
#elif RSG_NP
	static rlNpEventDelegator::Delegate sm_NpDlgt;
#endif

	
	static Delegator sm_Delegator;
};

}   //namespace rage

#endif  //RLINE_RLFRIENDSMANAGER_H
