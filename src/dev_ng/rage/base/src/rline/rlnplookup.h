// 
// rline/rlnplookup.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLNPLOOKUP_H
#define RLINE_RLNPLOOKUP_H

#if RSG_NP

#include "rlnpcommon.h"

#if RSG_ORBIS
#include <sdk_version.h>
#include <np.h>
#endif

namespace rage
{

class netStatus;

class rlNpLookup
{
public:
    rlNpLookup();
    ~rlNpLookup();

#if RSG_ORBIS
    static void InitLibs();
#endif

    bool Init();
    void Update();
    void Shutdown();

    bool IsInitialized() const;

    //PURPOSE
    //  Returns true if a new operation can be started.
    //  An operation can be started if there is an a lookup
    //  transaction slot available.  Currently there are
    //  32 transaction slots, i.e. 32 operations can be
    //  performed in parallel.
    bool CanStartOperation() const;

    bool LookupNpId(const SceNpOnlineId& npOnlineId,
                    SceNpId* npId,
                    const unsigned timeoutMs,
                    netStatus* status);

    void Cancel(const netStatus* status);

private:

    struct LookupTx;

    bool StartTransaction(LookupTx* lookupOp,
                        const unsigned timeoutMs,
                        netStatus* status);

    void ResetAll();

    //NP event handler
    rlNpEventDelegator::Delegate m_NpDlgt;
    void OnNpEvent(rlNp* np, const rlNpEvent* evt);

    struct LookupTx
    {
        LookupTx();
        virtual ~LookupTx();

        virtual void Reset();
        bool IsPending();
        virtual void Update();

        virtual void Finished(const bool succeeded) = 0;

        int m_TransCtxId;
        int m_TxSlot;
        rlNpLookup* m_Owner;
        netStatus* m_Status;
#if RSG_ORBIS
		SceNpLookupCreateAsyncRequestParameter m_ReqParam;
#endif
    };

    //Tracks status of title storage download.
    struct TsDownload : public LookupTx
    {
        TsDownload();
        ~TsDownload();

        virtual void Reset();

        virtual void Finished(const bool succeeded);

        unsigned m_DownloadSize;
        unsigned* m_DownloadSizePtr;
    };

    //Looks up an NP ID given an NP online ID.
    struct NpIdLookup : public LookupTx
    {
        NpIdLookup();
        ~NpIdLookup();

        virtual void Reset();

        virtual void Finished(const bool succeeded);

        rlSceNpOnlineId m_NpOnlineId;
        rlSceNpId m_NpId;
        SceNpId* m_NpIdPtr;
    };

    //Slots for currently pending operations.
    LookupTx* m_TxSlots[32];

    int m_TitleCtxId;

    bool m_Initialized      : 1;
};

}

#endif // RSG_NP

#endif  //RLINE_NPLOOKUP_H
