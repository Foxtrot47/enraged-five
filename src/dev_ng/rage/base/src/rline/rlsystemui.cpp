// 
// rline/rlsystemui.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlsystemui.h"

#include "diag/seh.h"
#include "input/pad.h"
#include "rldiag.h"
#include "rlgamerinfo.h"
#include "rlnp.h"
#include "rltask.h"
#include "system/nelem.h"
#include "system/new.h"
#include "system/timer.h"

#if RSG_ORBIS
#include "system/service.h"
#elif RSG_PC
#include "rlpc.h"
#include "system/service.h"
#elif RSG_DURANGO
#include "rline/durango/rlxbl_interface.h"
#endif //RSG_ORBIS

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, systemui);
#undef __rage_channel
#define __rage_channel rline_systemui

rlSystemUi g_SystemUi;

rlSystemUi::rlSystemUi()
: m_Initialized(false)
{
}

rlSystemUi::~rlSystemUi()
{
    this->Shutdown();
}

bool
rlSystemUi::Init()
{
    bool success = false;

    if(rlVerify(!m_Initialized))
    {
        success = this->NativeInit();

        m_Initialized = true;
    }

    return success;
}

void
rlSystemUi::Shutdown()
{
    if(m_Initialized)
    {
        if(IsUiShowing())
        {
            this->FinishUi();
            this->TerminateUi();
        }

        this->NativeShutdown();

        m_Initialized = false;
    }
}

void
rlSystemUi::Update()
{
    this->NativeUpdate();
}

bool
rlSystemUi::ShowSigninUi(const int localGamerIndex, unsigned flags)
{
    if(IsUiShowing())
    {
        return false;
    }
    
    netDebug(__FUNCTION__);
    return this->NativeShowSigninUi(localGamerIndex, flags);
}

bool
rlSystemUi::ShowGamerProfileUi(const int localGamerIndex,
                               const rlGamerHandle& target)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowGamerProfileUi(localGamerIndex, target);
}

bool 
rlSystemUi::ShowMessageComposeUi(const int localGamerIndex,
                                 const rlGamerHandle* recipients, 
                                 const unsigned numRecipients, 
                                 const char* subject, 
                                 const char* message)
{
	rlAssert(subject
		&& strlen(subject) < RL_MAX_MESSAGE_SUBJECT_CHARS);
	rlAssert(message
		&& strlen(message) < RL_MAX_MESSAGE_CONTENT_CHARS);

    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowMessageComposeUi(localGamerIndex, recipients, numRecipients, subject, message);
}

bool
rlSystemUi::ShowGamerFeedbackUi(const int localGamerIndex,
                                const rlGamerHandle& target)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowGamerFeedbackUi(localGamerIndex, target);
}

bool 
rlSystemUi::ShowMarketplaceUi(const int localGamerIndex,
							  const int iOfferType, 
							  const unsigned long iOfferId, 
							  const int iContentCategories)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowMarketplaceUi(localGamerIndex, iOfferType, iOfferId, iContentCategories);
}

bool 
rlSystemUi::ShowMarketplaceDownloadItemsUi(const int localGamerIndex, 
										   const int iUItype, 
										   const unsigned long long* pOfferId, 
										   const int iOfferIdSize)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowMarketplaceDownloadItemsUi(localGamerIndex, iUItype, pOfferId, iOfferIdSize);
}

bool 
rlSystemUi::ShowMarketplaceEmptyUi(const int localGamerIndex)
{
	if(IsUiShowing())
	{
		return false;
	}

	netDebug( __FUNCTION__ );
	return this->NativeShowMarketplaceEmptyUi( localGamerIndex );
}

bool
rlSystemUi::ShowPartySessionsUi(const int localGamerIndex)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowPartySessionsUi(localGamerIndex);
}

bool
rlSystemUi::ShowSendInviteUi(const int localGamerIndex)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowSendInviteUi(localGamerIndex);
}

bool
rlSystemUi::ShowAccountUpgradeUI(const int localGamerIndex)
{
	if (IsUiShowing())
	{
		return false;
	}

	netDebug(__FUNCTION__);
	return this->NativeShowAccountUpgradeUI(localGamerIndex);
}

//PURPOSE
// Shows the system UI for adding friends
bool rlSystemUi::ShowAddFriendUI(const int localGamerIndex, const rlGamerHandle& gamerHandle, const char* szMessage)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowAddFriendUI(localGamerIndex, gamerHandle, szMessage);
}

bool rlSystemUi::ShowWebBrowser(const int localGamerIndex, const char * url, const char* callbackUrl)
{
	rlSystemBrowserConfig config;
	config.m_Url = url;
	config.m_CallbackUrl = callbackUrl;

	return ShowWebBrowser(localGamerIndex, config);
}

bool rlSystemUi::ShowWebBrowser(const int localGamerIndex, const rlSystemBrowserConfig& config)
{
#if !RSG_PC
	// PC doesn't care if the system UI is on screen, URLs are loaded externally.
	if (IsUiShowing())
	{
		return false;
	}
#endif

	netDebug(__FUNCTION__);
	return NativeShowWebBrowser(localGamerIndex, config);
}

bool rlSystemUi::ShowAppHelpMenu(const int localGamerIndex)
{
	if (IsUiShowing())
	{
		return false;
	}

	netDebug(__FUNCTION__);
	return NativeShowAppHelpMenu(localGamerIndex);
}

bool
rlSystemUi::IsUiShowing() const
{
	static bool last = false;
	bool cur;
#if RSG_PC
	cur = g_rlPc.IsUiShowing() STEAMBUILD_ONLY(|| g_SysService.IsUiOverlaid());
#elif RSG_DURANGO
	cur = g_rlXbl.GetSystemUi()->IsUiShowing();
#elif RSG_ORBIS
	cur = g_SysService.IsUiOverlaid() || g_SysService.IsInBackgroundExecution();
#else
	cur = ioPad::IsIntercepted();
#endif
    if(cur != last)
    {
        last = cur;
        rlDebug("rlSystemUi::IsUiShowing() == %s", cur ? "true" : "false");
    }

    return cur;
}

//private:

void
rlSystemUi::FinishUi()
{
    this->NativeFinishUi();
}

void
rlSystemUi::TerminateUi()
{
    this->NativeTerminateUi();
}

bool rlSystemUi::CanSendMessages()
{
#if RSG_XDK
	return true;
#elif RSG_PC || RSG_ORBIS
	return false;
#else
	rlAssert(false);
	return false;
#endif
}

bool rlSystemUi::CanAddFriendUI()
{
#if RSG_PC || RSG_DURANGO 
	return true;
#elif RSG_ORBIS
	return false;
#else
	rlAssert(false);
	return false;
#endif
}

bool rlSystemUi::ShowFriendSearchUi(const int localGamerIndex)
{
    if(IsUiShowing())
    {
        return false;
    }

    netDebug(__FUNCTION__);
    return this->NativeShowFriendSearchUi(localGamerIndex);
}

#if RSG_ORBIS

bool rlSystemUi::NativeInit()
{
	return true;
}

void rlSystemUi::NativeShutdown()
{
}

void rlSystemUi::NativeUpdate()
{
}

bool rlSystemUi::NativeShowSigninUi(const int, unsigned UNUSED_PARAM(flags))
{
	rlAssert(!IsUiShowing());
	rlAssertf(false, "Showing the Signin UI is not supported in the PS4 SDK.");
	return false;
}

class rlNpGetOnlineIdAndShowSystemUiTask : public rlTaskBase
{
public:

    RL_TASK_DECL(rlNpGetOnlineIdAndShowSystemUiTask);
    rlNpGetOnlineIdAndShowSystemUiTask()
    {
        m_LocalGamerIndex = RL_INVALID_GAMER_INDEX;
        m_AccountId = RL_INVALID_NP_ACCOUNT_ID;
        m_OnlineId = rlSceNpOnlineId::RL_INVALID_ONLINE_ID;
    }

    bool Configure(const unsigned localGamerIndex, const rlSceNpAccountId& accountId)
    {
        if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
            return false; 

        if(accountId == RL_INVALID_NP_ACCOUNT_ID)
            return false;

        m_LocalGamerIndex = localGamerIndex;
        m_AccountId = accountId;
        return true; 
    }

    virtual void Start()
    {
        this->rlTaskBase::Start();
        g_rlNp.GetWebAPI().GetOnlineId(m_LocalGamerIndex, m_AccountId, &m_OnlineId, &m_MapperStatus);
    }

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        this->rlTaskBase::Finish(finishType, resultCode);
        if(finishType == rlTaskBase::FINISH_SUCCEEDED)
        {
            rlGamerHandle gamer;
            gamer.ResetNp(g_rlNp.GetEnvironment(), m_AccountId, &m_OnlineId);
            g_SystemUi.ShowGamerProfileUi(m_LocalGamerIndex, gamer);
        }
    }

    virtual void Update(const unsigned timeStep)
    {
        this->rlTaskBase::Update(timeStep);
        if(!m_MapperStatus.Pending())
        {
            Finish(m_MapperStatus.Succeeded() ? rlTaskBase::FINISH_SUCCEEDED : rlTaskBase::FINISH_FAILED);
        }
    }

private:

    int m_LocalGamerIndex; 
    rlSceNpAccountId m_AccountId;
    rlSceNpOnlineId m_OnlineId; 
    netStatus m_MapperStatus; 
};

bool rlSystemUi::NativeShowGamerProfileUi(const int localGamerIndex, const rlGamerHandle& target)
{
	rlAssert(!IsUiShowing());

    // if the server starts sending down account Ids, we might end up with handles that don't have a valid online Id
    if(!target.GetNpOnlineId().IsValid())
    {
        rlAssert(target.GetNpAccountId() != RL_INVALID_NP_ACCOUNT_ID);
        rlFireAndForgetTask<rlNpGetOnlineIdAndShowSystemUiTask>* pTask = NULL;
        rlGetTaskManager()->CreateTask(&pTask);
        if(pTask)
        {
            rlTaskBase::Configure(pTask, localGamerIndex, target.GetNpAccountId(), &pTask->m_Status);
            rlGetTaskManager()->AddParallelTask(pTask);
        }
        return true; 
    }

	return g_rlNp.GetCommonDialog().ShowProfile(g_rlNp.GetUserServiceId(localGamerIndex), target);
}

bool rlSystemUi::NativeShowMessageComposeUi(const int /*localGamerIndex*/, const rlGamerHandle* /*recipients*/, const unsigned /*numRecipients*/, const char* /*subject*/, const char* /*salutation*/)
{
	rlAssert(!IsUiShowing());
	return false;
}

bool rlSystemUi::NativeShowGamerFeedbackUi(const int /*localGamerIndex*/, const rlGamerHandle& /*target*/)
{
	rlAssert(!IsUiShowing());
	return false;
}

bool rlSystemUi::NativeShowMarketplaceUi(const int /*localGamerIndex*/,  const int /*iOfferType*/, const unsigned long /*iOfferId*/, const int /*iContentCategories*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowMarketplaceDownloadItemsUi(const int /*localGamerIndex*/,  const int /*iUItype*/,  const unsigned long long* /*pOfferId*/, const int /*iOfferIdSize*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowMarketplaceEmptyUi( const int /*localGamerIndex*/ )
{
	rlAssert( !IsUiShowing() );
	return g_rlNp.GetCommonDialog().ShowEmptyStoreMsg() == SCE_OK;
}

bool rlSystemUi::NativeShowPartySessionsUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool
rlSystemUi::NativeShowSendInviteUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowAccountUpgradeUI(const int localGamerIndex)
{
    rlAssert(!IsUiShowing());
	int userId = g_rlNp.GetUserServiceId(localGamerIndex);
    return g_rlNp.GetCommonDialog().ShowCommercePSPlusDialog(userId);
}

bool rlSystemUi::NativeShowWebBrowser(const int localGamerIndex, const rlSystemBrowserConfig& config)
{
	rlAssert(!IsUiShowing());

	int userId = g_rlNp.GetUserServiceId(localGamerIndex);
	return g_rlNp.GetCommonDialog().ShowUrl(userId, config);
}

bool rlSystemUi::NativeShowAppHelpMenu(const int)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowAddFriendUI(const int , const rlGamerHandle& , const char* )
{
	rlAssert(!IsUiShowing());
	rlAssertf(false, "Not implemented in ORBIS");
	return false;
}

void rlSystemUi::NativeFinishUi()
{
}

void rlSystemUi::NativeTerminateUi()
{
}

bool 
rlSystemUi::NativeShowFriendSearchUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssertf(false, "Not implemented.");
	return false;
}

#elif RSG_PC

bool
rlSystemUi::NativeInit()
{
	return true;
}

void
rlSystemUi::NativeShutdown()
{

}

void
rlSystemUi::NativeUpdate()
{

}

bool
rlSystemUi::NativeShowSigninUi(const int, unsigned UNUSED_PARAM(flags))
{
	rlAssert(!IsUiShowing());
	return g_rlPc.ShowSigninUi();
}

bool
rlSystemUi::NativeShowGamerProfileUi(const int /*localGamerIndex*/,
									 const rlGamerHandle& target)
{
	rlAssert(!IsUiShowing());
	return g_rlPc.ShowGamerProfileUi(target);
}

bool
rlSystemUi::NativeShowMessageComposeUi(const int /*localGamerIndex*/, 
									   const rlGamerHandle* /*recipients*/, 
									   const unsigned /*numRecipients*/, 
									   const char* /*subject*/, 
									   const char* /*salutation*/)
{
	rlAssert(!IsUiShowing());
	return false;
}

bool
rlSystemUi::NativeShowGamerFeedbackUi(const int /*localGamerIndex*/,
									  const rlGamerHandle& /*target*/)
{
	rlAssert(!IsUiShowing());
	return false;
}

bool 
rlSystemUi::NativeShowMarketplaceUi(const int /*localGamerIndex*/, 
									const int /*iOfferType*/, 
									const unsigned long /*iOfferId*/, 
									const int /*iContentCategories*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool 
rlSystemUi::NativeShowMarketplaceDownloadItemsUi(const int /*localGamerIndex*/, 
												 const int /*iUItype*/, 
												 const unsigned long long* /*pOfferId*/,
												 const int /*iOfferIdSize*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool 
rlSystemUi::NativeShowMarketplaceEmptyUi( const int /*localGamerIndex*/ )
{
	rlAssert( !IsUiShowing() );
	rlAssert( false );
	return false;
}

bool
rlSystemUi::NativeShowPartySessionsUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool
rlSystemUi::NativeShowSendInviteUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowAccountUpgradeUI(const int /* localGamerIndex */)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowWebBrowser(const int UNUSED_PARAM(localGamerIndex), const rlSystemBrowserConfig& config)
{
#if __STEAM_BUILD
	rtry
	{
		rverify(config.m_Url, catchall, );
		rverify(SteamFriends(), nosteamfriends, );
		SteamFriends()->ActivateGameOverlayToWebPage(config.m_Url);
		return true;
	}
	rcatch(nosteamfriends)
	{
		INT_PTR result = (INT_PTR)ShellExecuteA(NULL, "open", config.m_Url, NULL, NULL, SW_SHOWMAXIMIZED);
		return result >= 32;
	}
	rcatchall
	{
		return false;
	}
#else
	INT_PTR result = (INT_PTR)ShellExecuteA(NULL, "open", config.m_Url, NULL, NULL, SW_SHOWMAXIMIZED);
	return result >= 32;
#endif
}

bool rlSystemUi::NativeShowAppHelpMenu(const int)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowAddFriendUI(const int , const rlGamerHandle& gamerHandle, const char* )
{
	if (g_rlPc.GetPlayerManager() != NULL)
	{
        return SUCCEEDED(g_rlPc.GetPlayerManager()->ShowFriendRequestUi(gamerHandle.GetRockstarId()));
	}

	return false;
}

void
rlSystemUi::NativeFinishUi()
{
}

void
rlSystemUi::NativeTerminateUi()
{
}

bool
rlSystemUi::NativeShowFriendSearchUi(const int /*localGamerIndex*/)
{
	if (g_rlPc.GetPlayerManager() != NULL)
	{
		return SUCCEEDED(g_rlPc.GetPlayerManager()->ShowFriendSearchUi());
	}

	return false;
}

#elif RSG_DURANGO

bool rlSystemUi::NativeInit()
{
	return true;
}

void rlSystemUi::NativeShutdown()
{
}

void rlSystemUi::NativeUpdate()
{
}

bool rlSystemUi::NativeShowSigninUi(const int localGamerIndex, unsigned UNUSED_PARAM(bFilterToOnlineAccountsOnly))
{
	rlAssert(!IsUiShowing());
	return g_rlXbl.GetSystemUi()->ShowSigninUi(localGamerIndex);
}

bool rlSystemUi::NativeShowGamerProfileUi(const int localGamerIndex, const rlGamerHandle& target)
{
	rlAssert(!IsUiShowing());
	return g_rlXbl.GetSystemUi()->ShowGamerProfileUi(localGamerIndex, target);
}

bool rlSystemUi::NativeShowMessageComposeUi(const int localGamerIndex, const rlGamerHandle* recipients, const unsigned numRecipients, const char* , const char* salutation)
{
	return g_rlXbl.GetSystemUi()->ShowComposeMessage(localGamerIndex, salutation, recipients, numRecipients);
}

bool rlSystemUi::NativeShowGamerFeedbackUi(const int /*localGamerIndex*/, const rlGamerHandle& /*target*/)
{
	rlAssert(!IsUiShowing());
	return false;
}

bool rlSystemUi::NativeShowMarketplaceUi(const int /*localGamerIndex*/, const int /*iOfferType*/, const unsigned long /*iOfferId*/, const int /*iContentCategories*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowMarketplaceDownloadItemsUi(const int /*localGamerIndex*/, const int /*iUItype*/, const unsigned long long* /*pOfferId*/, const int /*iOfferIdSize*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowMarketplaceEmptyUi( const int /*localGamerIndex*/ )
{
	rlAssert( !IsUiShowing() );
	rlAssert( false );
	return false;
}

bool rlSystemUi::NativeShowPartySessionsUi(const int localGamerIndex)
{
	rlAssert(!IsUiShowing());
	return g_rlXbl.GetSystemUi()->ShowPartySessionsUi(localGamerIndex);
}

bool
rlSystemUi::NativeShowSendInviteUi(const int localGamerIndex)
{
	rlAssert(!IsUiShowing());
	return g_rlXbl.GetSystemUi()->ShowSendInvitesUi(localGamerIndex);
}

bool rlSystemUi::NativeShowAccountUpgradeUI(const int /* localGamerIndex */)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowWebBrowser(const int localGamerIndex, const rlSystemBrowserConfig& config)
{
	return g_rlXbl.GetSystemUi()->ShowWebBrowser(localGamerIndex, config.m_Url);
}

bool rlSystemUi::NativeShowAppHelpMenu(const int localGamerIndex)
{
    return g_rlXbl.GetSystemUi()->ShowAppHelpMenu(localGamerIndex);
}

bool rlSystemUi::NativeShowAddFriendUI(const int localGamerIndex, const rlGamerHandle& handle, const char* )
{
    return g_rlXbl.GetSystemUi()->ShowAddRemoveFriend(localGamerIndex, handle.GetXuid());
}

void rlSystemUi::NativeFinishUi()
{
}

void rlSystemUi::NativeTerminateUi()
{
}

bool 
rlSystemUi::NativeShowFriendSearchUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssertf(false, "Not implemented.");
	return false;
}

#else

bool
rlSystemUi::NativeInit()
{
	return true;
}

void
rlSystemUi::NativeShutdown()
{
}

void
rlSystemUi::NativeUpdate()
{
}

bool
rlSystemUi::NativeShowSigninUi(const int, unsigned UNUSED_PARAM(flags))
{
    rlAssert(!IsUiShowing());
    return false;
}

bool
rlSystemUi::NativeShowGamerProfileUi(const int /*localGamerIndex*/,
                                     const rlGamerHandle& /*target*/)
{
    rlAssert(!IsUiShowing());
    return false;
}

bool
rlSystemUi::NativeShowMessageComposeUi(const int /*localGamerIndex*/, 
									   const rlGamerHandle* /*recipients*/, 
									   const unsigned /*numRecipients*/, 
									   const char* /*subject*/, 
									   const char* /*salutation*/)
{
	rlAssert(!IsUiShowing());
	return false;
}

bool
rlSystemUi::NativeShowGamerFeedbackUi(const int /*localGamerIndex*/,
                                      const rlGamerHandle& /*target*/)
{
    rlAssert(!IsUiShowing());
    return false;
}

bool 
rlSystemUi::NativeShowMarketplaceUi(const int /*localGamerIndex*/, 
									const int /*iOfferType*/, 
									const unsigned long /*iOfferId*/, 
									const int /*iContentCategories*/)
{
    rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool 
rlSystemUi::NativeShowMarketplaceDownloadItemsUi(const int /*localGamerIndex*/, 
												 const int /*iUItype*/, 
												 const unsigned long long* /*pOfferId*/,
												 const int /*iOfferIdSize*/)
{
    rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool 
rlSystemUi::NativeShowMarketplaceEmptyUi( const int /*localGamerIndex*/ )
{
	rlAssert( !IsUiShowing() );
	rlAssert( false );
	return false;
}

bool
rlSystemUi::NativeShowPartySessionsUi(const int /*localGamerIndex*/)
{
    rlAssert(!IsUiShowing());
    rlAssert(false);
    return false;
}

bool
rlSystemUi::NativeShowSendInviteUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowAccountUpgradeUI(const int /* localGamerIndex */)
{
	rlAssert(!IsUiShowing());
	rlAssert(false);
	return false;
}

bool rlSystemUi::NativeShowAddFriendUI(const int , const rlGamerHandle& , const char* )
{
	rlAssert(!IsUiShowing());
	rlAssertf(false, "Not implemented.");
	return false;
}

bool rlSystemUi::NativeShowAppHelpMenu(const int )
{
	rlAssertf(false, "Not implemented.");
	return false;
}

void
rlSystemUi::NativeFinishUi()
{
}

void
rlSystemUi::NativeTerminateUi()
{
}

bool 
rlSystemUi::NativeShowFriendSearchUi(const int /*localGamerIndex*/)
{
	rlAssert(!IsUiShowing());
	rlAssertf(false, "Not implemented.");
	return false;
}

bool rlSystemUi::NativeShowWebBrowser(const int , const rlSystemBrowserConfig& UNUSED_PARAM(config))
{
	return false;
}

#endif //RSG_ORBIS

}   //namespace rage
