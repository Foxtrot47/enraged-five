// 
// rline/rlpeerinfo.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPEERINFO
#define RLINE_RLPEERINFO

#include "rlpeeraddress.h"

namespace rage
{

//PURPOSE
//  This class encapsulates a peer info.
//
//  Call rlPeerInfo::GetLocalInfo() to get the peer info
//  for the local machine.
class rlPeerInfo
{
public:
    //PURPOSE
    //  Retrieves the peer ID for the local machine. This is a lighter weight 
    //  call than GetLocalInfo, and preferred if you only need the ID.
    //PARAMS
    //  id          - Local peer ID (out)
    //RETURNS
    //  True on success.
    //NOTES
    //  The peer id should *not* be saved to persistent storage.  It will
    //  change with each reboot.
    static bool GetLocalPeerId(u64 *id);

    //PURPOSE
    //  Retrieves the peer info for the local machine.
    //PARAMS
    //  info        - Peer info.
    //RETURNS
    //  True on success.
    static bool GetLocalPeerInfo(rlPeerInfo* info, const int localGamerIndex);

    rlPeerInfo();

	//PURPOSE
	//  Initialize the instance.
	bool Init(const u64 peerId, const rlPeerAddress& peerAddr);

    //PURPOSE
    //  Clears the instance to its initial state.
    void Clear();

    //PURPOSE
    //  Returns true if the instance contains valid data.
    bool IsValid() const;

    //PURPOSE
    //  Returns true if the instance contains a valid peer address.
    bool HasValidPeerAddress() const;

    //PURPOSE
    //  Returns the globally unique id for the peer.
    //NOTES
    //  This value will change with each reboot of the local machine.
    //
    //  DO NOT SAVE THIS VALUE TO PERSISTENT STORAGE!!!.
    u64 GetPeerId() const;

    //PURPOSE
    //  Returns the peer's peer address.
    const rlPeerAddress& GetPeerAddress() const;

    //PURPOSE
    //  Returns true if the peer info represents the local peer.
    bool IsLocal() const;

    //PURPOSE
    //  Returns true if the peer info represents a remote peer.
    bool IsRemote() const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    bool operator==(const rlPeerInfo& that) const;
    bool operator!=(const rlPeerInfo& that) const;

private:

    rlPeerAddress m_PeerAddr;
    u64 m_PeerId;
};

}   //namespace rage

#endif  //RLINE_RLPEERINFO
