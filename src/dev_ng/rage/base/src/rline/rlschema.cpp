// 
// rline/rlschema.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rlschema.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "rldiag.h"
#include "system/memory.h"

//From <xonline.h>
#define XUSER_DATA_TYPE_CONTEXT     ((rage::u8)0)
#define XUSER_DATA_TYPE_INT32       ((rage::u8)1)
#define XUSER_DATA_TYPE_INT64       ((rage::u8)2)
#define XUSER_DATA_TYPE_DOUBLE      ((rage::u8)3)
#define XUSER_DATA_TYPE_UNICODE     ((rage::u8)4)
#define XUSER_DATA_TYPE_FLOAT       ((rage::u8)5)
#define XUSER_DATA_TYPE_BINARY      ((rage::u8)6)
#define XUSER_DATA_TYPE_DATETIME    ((rage::u8)7)
#define XUSER_DATA_TYPE_NULL        ((rage::u8)0xFF)
#define TYPE_FROM_ID(id)            ((((id) & 0xF0000000) >> 28) & 0x0F)

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(rline, schema);
#undef __rage_channel
#define __rage_channel rline_schema

///////////////////////////////////////////////////////////////////////////////
// rlSchema
///////////////////////////////////////////////////////////////////////////////

rlSchema::rlSchema()
    : m_IsConcrete(false)
{
    CompileTimeAssert(sizeof(m_Data) > SIZEOF_SCHEMA_HEADER);

    sysMemSet(m_Data, 0, SIZEOF_SCHEMA_HEADER);
}

rlSchema::rlSchema(const rlSchema& that)
    : m_IsConcrete(false)
{
    *this = that;
}

rlSchema&
rlSchema::operator=(const rlSchema& that)
{
    if(&that != this)
    {
        if(!m_IsConcrete)
        {
            this->Clear();
        }

        this->CopyFieldsFrom(that);

        //Don't change the value of m_IsConcrete.
    }

    return *this;
}

void
rlSchema::Clear()
{
    rlAssert(!m_IsConcrete);

    CompileTimeAssert(sizeof(m_Data) > SIZEOF_SCHEMA_HEADER);

    sysMemSet(m_Data, 0, SIZEOF_SCHEMA_HEADER);
}

void
rlSchema::ClearFieldData()
{
    if(m_Data)
    {
        *(u64*) &m_Data[OFFSETOF_MASK] = 0;
    }
}

int
rlSchema::AddField(const unsigned newFieldId)
{
    rlAssert(!m_IsConcrete);

    int newFieldIndex = this->GetFieldIndex(newFieldId);

    if(newFieldIndex < 0)
    {
        const int numFields = this->GetFieldCount();

        if(rlVerify(numFields < MAX_FIELD_COUNT))
        {
            const int newSize = rlSchema::FieldSizeFromId(newFieldId);

            if(newSize > 0)
            {
                Field* field;
                
                if(numFields > 0)
                {
                    field = this->SkipTo(numFields - 1);
                    field = this->Next(field);
                }
                else
                {
                    field = this->GetFields();
                }

                if(rlVerify(field))
                {
                    field->SetId(newFieldId);

                    newFieldIndex = numFields;

                    *(u32*) &m_Data[OFFSETOF_COUNT] = u32(numFields + 1);
                }
            }
        }
    }

    return newFieldIndex;
}

int
rlSchema::GetFieldCount() const
{
    const int count = m_Data ? (int) *(u32*) &m_Data[OFFSETOF_COUNT] : 0;

    rlAssert(count <= MAX_FIELD_COUNT && count >= 0);

    return count;
}

bool
rlSchema::SetNil(const int index)
{
    bool success = false;

    if(rlVerify(index >= 0 && index < this->GetFieldCount()))
    {
        const u64 mask = this->GetMask();
        this->SetMask(mask & ~(u64(1) << index));
        success = true;
    }

    return success;
}

bool
rlSchema::IsNil(const int index) const
{
    rlAssert(index >= 0 && index < this->GetFieldCount());

    return index >= 0 &&
           index < this->GetFieldCount() &&
           !(this->GetMask() & (u64(1) << index));
}

bool
rlSchema::HasNilFields() const
{
    const int numFields = this->GetFieldCount();
    const u64 check = (u64(1) << numFields) - 1;

    return check != (check & this->GetMask());
}

bool
rlSchema::HasField(const unsigned fieldId) const
{
    return this->GetFieldIndex(fieldId) >= 0;
}

unsigned
rlSchema::GetFieldId(const int index) const
{
    const Field* field = this->SkipTo(index);
    return rlVerify(field) ? field->GetId() : (unsigned)INVALID_FIELD_ID;
}

int
rlSchema::GetFieldType(const int index) const
{
    const Field* field = this->SkipTo(index);
    int ftype = FIELDTYPE_INVALID;
    if(rlVerify(field))
    {
        const unsigned t = TYPE_FROM_ID(field->GetId());
        switch(t)
        {
            case XUSER_DATA_TYPE_CONTEXT:
            case XUSER_DATA_TYPE_INT32:
                ftype = FIELDTYPE_INT32;
                break;

            case XUSER_DATA_TYPE_INT64:
            case XUSER_DATA_TYPE_DATETIME:
                ftype = FIELDTYPE_INT64;
                break;

            case XUSER_DATA_TYPE_FLOAT:
                ftype = FIELDTYPE_FLOAT;
                break;

            case XUSER_DATA_TYPE_DOUBLE:
                ftype = FIELDTYPE_DOUBLE;
                break;

            default:
                rlWarning("Invalid field type:0x%0x", t);
                rlAssert(false);
                break;
        }
    }

    return ftype;
}

int
rlSchema::GetFieldIndex(const unsigned fieldId) const
{
    int idx = -1;

    const Field* field = this->GetFields();
    const int count = this->GetFieldCount();

    for(int i = 0; i < count; ++i, field = this->Next(field))
    {
        rlAssert(field);

        if(field->GetId() == fieldId)
        {
            idx = i;
            break;
        }
    }

    return idx;
}

bool
rlSchema::SetFieldData(const int index, const void* data, const int dataSize)
{
    rlAssert(dataSize <= MAX_FIELD_SIZE);

    bool success = false;

    Field* field = this->SkipTo(index);

    if(rlVerify(field)
        && rlVerify(field->GetSizeofData() == dataSize))
    {
        sysMemCpy(field->GetData(), data, dataSize);

        *(u64*) &m_Data[OFFSETOF_MASK] |= (u64(1) << index);

        success = true;
    }

    return success;
}

const void*
rlSchema::GetFieldData(const int index) const
{
    const Field* field = !this->IsNil(index) ? this->SkipTo(index) : 0;

    return rlVerify(field) ? field->GetData() : 0;
}

bool
rlSchema::GetFieldData(const int index, void* dst, const int dstSize) const
{
    bool success = false;

    const Field* field = this->SkipTo(index);

    if(rlVerify(field)
        && rlVerify(field->GetSizeofData() <= dstSize))
    {
        sysMemCpy(dst, field->GetData(), dstSize);
        success = true;
    }

    return success;
}

int
rlSchema::GetSizeofFieldData(const int index) const
{
    const Field* field = this->SkipTo(index);

    return rlVerify(field) ? field->GetSizeofData() : -1;
}

int
rlSchema::GetTotalSizeofFieldData() const
{
    int size = 0;
    
    if(m_Data)
    {
        const Field* field = this->GetFields();
        const int count = this->GetFieldCount();

        for(int i = 0; i < count; ++i, field = this->Next(field))
        {
            rlAssert(field);
            size += field->GetSizeofData();
        }
    }

    return size;
}

int
rlSchema::GetSize() const
{
    int size = 0;
    
    if(m_Data)
    {
        size += SIZEOF_SCHEMA_HEADER;

        const Field* field = this->GetFields();
        const int count = this->GetFieldCount();

        for(int i = 0; i < count; ++i, field = this->Next(field))
        {
            rlAssert(field);
            size += SIZEOF_FIELD_HEADER + field->GetSizeofData();
        }
    }

    return size;
}

int
rlSchema::CopyFieldsFrom(const rlSchema& that)
{
    int count = 0;

    if(m_IsConcrete)
    {
        count = this->CopyExistingFieldsFrom(that);
    }
    else
    {
        const int fieldCount = that.GetFieldCount();

        for(int i = 0; i < fieldCount; ++i)
        {
            const unsigned fieldId = that.GetFieldId(i);
            int myFieldIdx = this->GetFieldIndex(fieldId);

            if(myFieldIdx < 0)
            {
                //Don't have this field
                myFieldIdx = this->AddField(fieldId);
                rlAssert(myFieldIdx >= 0);
            }

            if(that.IsNil(i))
            {
                continue;
            }

            if(rlVerify(myFieldIdx >= 0))
            {
                this->SetFieldData(myFieldIdx,
                                    that.GetFieldData(i),
                                    rlSchema::FieldSizeFromId(fieldId));

                ++count;
            }
        }
    }

    return count;
}

int
rlSchema::CopyExistingFieldsFrom(const rlSchema& that)
{
    int count = 0;

    const int fieldCount = that.GetFieldCount();

    for(int i = 0; i < fieldCount; ++i)
    {
        if(that.IsNil(i))
        {
            continue;
        }

        const unsigned fieldId = that.GetFieldId(i);
        const int myFieldIdx = this->GetFieldIndex(fieldId);

        if(myFieldIdx < 0)
        {
            //Don't have this field
            continue;
        }

        this->SetFieldData(myFieldIdx,
                            that.GetFieldData(i),
                            rlSchema::FieldSizeFromId(fieldId));

        ++count;
    }

    return count;
}

bool
rlSchema::Commit(const int localGamerIndex, const unsigned flags) const
{
    rlAssert(this->GetFieldCount() > 0);

    bool success = false;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),
                 catchall,
                 rlError("Invalid gamer index:%d", localGamerIndex));

        const Field* field = this->GetFields();
        const int count = this->GetFieldCount();

        for(int i = 0; i < count; ++i, field = this->Next(field))
        {
            rlAssert(field);

            if(this->IsNil(i))
            {
                rverify(!(COMMIT_NO_NIL_FIELDS & flags),
                         catchall,
                         rlError("Field:%d is nil", i));

                continue;
			}
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool
rlSchema::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    bool success = false;

    datBitBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);
    const int numFields = this->GetFieldCount();

    if(bb.WriteUns(this->GetSize(), 32)
        && bb.WriteUns(numFields, 32))
    {
        success = true;

        for(int i = 0; i < numFields; ++i)
        {
            const bool isNil = this->IsNil(i);
            const unsigned fid = this->GetFieldId(i);

            if(!bb.WriteUns(fid, 32) || !bb.WriteBool(isNil))
            {
                success = false; break;
            }

            if(isNil) continue;

            bool wroteIt = false;
            const unsigned ftype = TYPE_FROM_ID(fid);

            switch(ftype)
            {
                case XUSER_DATA_TYPE_CONTEXT:
                case XUSER_DATA_TYPE_INT32:
                    wroteIt = bb.WriteUns(*(const u32*)this->GetFieldData(i), 32);
                    break;

                case XUSER_DATA_TYPE_INT64:
                case XUSER_DATA_TYPE_DATETIME:
                    wroteIt = bb.WriteUns(*(const u64*)this->GetFieldData(i), 64);
                    break;

                case XUSER_DATA_TYPE_FLOAT:
                    wroteIt = bb.WriteFloat(*(const float*)this->GetFieldData(i));
                    break;

                case XUSER_DATA_TYPE_DOUBLE:
                    wroteIt = bb.WriteDouble(*(const double*)this->GetFieldData(i));
                    break;

                default:
                    rlWarning("Invalid field type:0x%0x", ftype);
                    rlAssert(false);
                    break;
            }

            if(!wroteIt)
            {
                success = false; break;
            }
        }
    }

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}
    return success;
}

bool
rlSchema::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    bool success = false;

    datBitBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);
    unsigned numFields, sizeofSchema;

    this->ClearFieldData();

    if(bb.ReadUns(sizeofSchema, 32)
        && rlVerify(sizeofSchema <= MAX_DATA_SIZE)
        && bb.ReadUns(numFields, 32)
        && rlVerify(numFields <= MAX_FIELD_COUNT))
    {
        success = true;

        for(int i = 0; i < (int) numFields; ++i)
        {
            bool isNil;
            unsigned fid;

            if(!bb.ReadUns(fid, 32)
                || !bb.ReadBool(isNil))
            {
                success = false; break;
            }

            if(i >= this->GetFieldCount()
                && !rlVerify(i == this->AddField(fid)))
            {
                success = false; break;
            }

            if(isNil) continue;

            bool readIt = false;
            const unsigned ftype = TYPE_FROM_ID(fid);

            switch(ftype)
            {
                case XUSER_DATA_TYPE_CONTEXT:
                case XUSER_DATA_TYPE_INT32:
                    {
                        u32 a;
                        readIt = bb.ReadUns(a, 32)
                                && this->SetFieldData(i, &a, sizeof(a));
                    }
                    break;

                case XUSER_DATA_TYPE_INT64:
                case XUSER_DATA_TYPE_DATETIME:
                    {
                        u64 a;
                        readIt = bb.ReadUns(a, 64)
                                && this->SetFieldData(i, &a, sizeof(a));
                    }
                    break;

                case XUSER_DATA_TYPE_FLOAT:
                    {
                        float a;
                        readIt = bb.ReadFloat(a)
                                && this->SetFieldData(i, &a, sizeof(a));
                    }
                    break;

                case XUSER_DATA_TYPE_DOUBLE:
                    {
                        double a;
                        readIt = bb.ReadDouble(a)
                                && this->SetFieldData(i, &a, sizeof(a));
                    }
                    break;

                default:
                    rlWarning("Invalid field type:0x%0x", ftype);
                    rlAssert(false);
                    break;
            }

            if(!readIt)
            {
                success = false; break;
            }
        }
    }

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}
    return success;
}

#if RSG_PC

int
rlSchema::GetPropertyCount() const
{
    int propCount = 0;

    const Field* field = this->GetFields();
    const int count = this->GetFieldCount();

    for(int i = 0; i < count; ++i, field = this->Next(field))
    {
        rlAssert(field);
        const unsigned fid = field->GetId();

        if(XUSER_DATA_TYPE_CONTEXT != TYPE_FROM_ID(fid))
        {
            ++propCount;
        }
    }

    return propCount;
}

int
rlSchema::GetContextCount() const
{
    int ctxCount = 0;

    const Field* field = this->GetFields();
    const int count = this->GetFieldCount();

    for(int i = 0; i < count; ++i, field = this->Next(field))
    {
        rlAssert(field);
        const unsigned fid = field->GetId();

        if(XUSER_DATA_TYPE_CONTEXT == TYPE_FROM_ID(fid))
        {
            ++ctxCount;
        }
    }

    return ctxCount;
}

bool
rlSchema::IsProperty(const int index) const
{
    const Field* field = this->SkipTo(index);
    const unsigned fid = rlVerify(field) ? field->GetId() : 0;
    const unsigned ftype = TYPE_FROM_ID(fid);
    return rlVerify(field) ? XUSER_DATA_TYPE_CONTEXT != ftype : false;
}

bool
rlSchema::IsContext(const int index) const
{
    const Field* field = this->SkipTo(index);
    const unsigned fid = rlVerify(field) ? field->GetId() : 0;
    const unsigned ftype = TYPE_FROM_ID(fid);
    return rlVerify(field) ? XUSER_DATA_TYPE_CONTEXT == ftype : false;
}
#endif  //RSG_PC

int
rlSchema::FieldSizeFromId(const unsigned fieldId)
{
    int size = -1;

    switch(TYPE_FROM_ID(fieldId))
    {
        case XUSER_DATA_TYPE_CONTEXT:
        case XUSER_DATA_TYPE_INT32:
            size = sizeof(u32);
            break;

        case XUSER_DATA_TYPE_INT64:
        case XUSER_DATA_TYPE_DATETIME:
            size = sizeof(u64);
            break;

        case XUSER_DATA_TYPE_FLOAT:
            size = sizeof(float);
            break;

        case XUSER_DATA_TYPE_DOUBLE:
            size = sizeof(double);
            break;

        default:
            rlWarning("Invalid field type:0x%0x", TYPE_FROM_ID(fieldId));
            break;
    }

    return size;
}

rlSchema::FieldType
rlSchema::FieldTypeFromId(const unsigned fieldId)
{
    rlSchema::FieldType ft = FIELDTYPE_INVALID;

    switch(TYPE_FROM_ID(fieldId))
    {
        case XUSER_DATA_TYPE_CONTEXT:
        case XUSER_DATA_TYPE_INT32:
            ft = FIELDTYPE_INT32;
            break;

        case XUSER_DATA_TYPE_INT64:
        case XUSER_DATA_TYPE_DATETIME:
            ft = FIELDTYPE_INT64;
            break;

        case XUSER_DATA_TYPE_FLOAT:
            ft = FIELDTYPE_FLOAT;
            break;

        case XUSER_DATA_TYPE_DOUBLE:
            ft = FIELDTYPE_DOUBLE;
            break;

        default:
            rlWarning("Invalid field type:0x%0x", TYPE_FROM_ID(fieldId));
            break;
    }

    return ft;
}

//protected:

rlSchema::Field*
rlSchema::SkipTo(const int index)
{
    Field* field = 0;

    if(rlVerify(index >= 0 && index < this->GetFieldCount()))
    {
        field = this->GetFields();

        for(int i = 0; i < index; ++i, field = this->Next(field))
        {
            rlAssert(field);
        }
    }

    return field;
}

const rlSchema::Field*
rlSchema::SkipTo(const int index) const
{
    return const_cast< rlSchema* >(this)->SkipTo(index);
}

rlSchema::Field*
rlSchema::Next(const Field* field)
{
    u8* next = 0;

    if((const u8*) field >= m_Data
        && (const u8*) field < &m_Data[sizeof(m_Data)])
    {
        const int size = field->GetSizeofData();

        next = ((u8*) field->GetData()) + size;

        if(next < m_Data || next >= &m_Data[sizeof(m_Data)])
        {
            next = 0;
        }
    }

    return (Field*) next;
}

const rlSchema::Field*
rlSchema::Next(const Field* field) const
{
    return const_cast< rlSchema* >(this)->Next(field);
}

void
rlSchema::SetMask(const u64 mask)
{
    if(m_Data){*(u64*) &m_Data[OFFSETOF_MASK] = mask;}
}

u64
rlSchema::GetMask() const
{
    return m_Data ? *(u64*) &m_Data[OFFSETOF_MASK] : 0;
}

u8*
rlSchema::GetData()
{
    return m_Data ? &m_Data[OFFSETOF_FIELDS] : 0;
}

const u8*
rlSchema::GetData() const
{
    return m_Data ? &m_Data[OFFSETOF_FIELDS] : 0;
}

}   //namespace rage
