// 
// rline/rl.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RL_H
#define RLINE_RL_H

#include "file/file_config.h"
#include "diag/channel.h"
#include "diag/tracker.h"
#include "system/bit.h"
#include "system/language.h"

#if RSG_NP
#include <sys/types.h>
#endif

#if RSG_ORBIS
#include <np.h>
#endif

#if RSG_NP
struct SceNpOnlineId;
struct SceNpId;
struct SceNpOnlineName;;
struct SceNpAvatarUrl;
struct SceNpUserInfo;
struct SceNpTusVariable;
#endif  //RSG_NP

#if RSG_ORBIS
struct SceNpSessionId;
struct SceNpInvitationId;
#endif

namespace rage
{

typedef s64 RockstarId;
typedef s32 PlayerAccountId;
#define InvalidRockstarId       RockstarId(0)
#define InvalidPlayerAccountId  PlayerAccountId(0)

#define RL_INVALID_ROCKSTAR_ID      InvalidRockstarId
#define RL_INVALID_PLAYERACCOUNT_ID InvalidPlayerAccountId
#define RL_INVALID_GAMER_INDEX		-1

#define RL_RAGE_TRACK(x)    RAGE_TRACK(rline);RAGE_TRACK(x)

#define RL_IS_VALID_LOCAL_GAMER_INDEX(i) ((i) >= 0 && (i) < RL_MAX_LOCAL_GAMERS)

#define RL_VERIFY_LOCAL_GAMER_INDEX(i) rlVerifyf(RL_IS_VALID_LOCAL_GAMER_INDEX(i), "Invalid local gamer index: %d", i)

#define RL_MAX_BUF_FOR_UTF_CHARS(i) ((i*4)+1) // 4-byte UTF8

#define RL_SC_PLATFORMS		(RSG_PC || RSG_ANDROID || RSG_IOS || RSG_OSX || RSG_NX || RSG_P)

#if RL_SC_PLATFORMS
#define SC_PLATFORMS_ONLY(...) __VA_ARGS__
#define NON_SC_PLATFORMS_ONLY(...)
#else
#define SC_PLATFORMS_ONLY(...) 
#define NON_SC_PLATFORMS_ONLY(...) __VA_ARGS__
#endif // RL_SC_PLATFORMS

//PURPOSE
//  To mark NP specific code no longer needed or supported in PS5. Doesn't replace RSG_ORBIS
#define RL_NP_LEGACY	(RSG_ORBIS)

//PURPOSE
// Is the UDS/Activities service supported
#define RL_NP_UDS		(RSG_PROSPERO)

#if RL_NP_UDS
#define NP_UDS_ONLY(...) __VA_ARGS__
#else
#define NP_UDS_ONLY(...)
#endif

//PURPOSE
///  Does the NP API support play together
#define RL_NP_SUPPORT_PLAY_TOGETHER	(RSG_ORBIS)

#if RL_NP_SUPPORT_PLAY_TOGETHER
#define RL_NP_PLAYTOGETHER_ONLY(...) __VA_ARGS__
#else
#define RL_NP_PLAYTOGETHER_ONLY(...)
#endif

//PURPOSE
///  Deep link config
#define RL_NP_DEEP_LINK_MANUAL_CLOSE (RSG_ORBIS)

#if RL_NP_DEEP_LINK_MANUAL_CLOSE
#define RL_NP_DEEP_LINK_MANUAL_CLOSE_ONLY(...) __VA_ARGS__
#else
#define RL_NP_DEEP_LINK_MANUAL_CLOSE_ONLY(...) 
#endif

//PURPOSE
///  Are we using the old PS4 webapi.
#define RL_NP_WEBAPI1	(RSG_ORBIS)

//PURPOSE
//  Are we using the cpp webapi library. Theoretically Prospero could work without but we're currently relying on it.
//  It can also be used on Orbis with the Cross Platform SDK.
#define RL_NP_CPPWEBAPI	(RSG_PROSPERO)

#if RL_NP_CPPWEBAPI
#define NP_CPPWEBAPI_ONLY(...) __VA_ARGS__
#else
#define NP_CPPWEBAPI_ONLY(...)
#endif

#if !__FINAL
#define RL_SUPPORT_NETWORK_BOTS 1
#else
#define RL_SUPPORT_NETWORK_BOTS 0
#endif // (__DEV || __BANK)

#define LIVESTREAM_ENABLED (RSG_PC && 0)

// Xbox One doesn't support LAN broadcasting or multicasting.
// We don't create a LAN socket if SUPPORT_LAN_MULTIPLAYER is 0.
#define SUPPORT_LAN_MULTIPLAYER (!__NO_OUTPUT && !RSG_DURANGO)

#define USE_NET_ASSERTS (__DEV || __BANK || __FINAL_LOGGING)

#if USE_NET_ASSERTS
#	define NET_ASSERTS_ONLY(...)    __VA_ARGS__
#else
#	define NET_ASSERTS_ONLY(...)
#endif

#if USE_NET_ASSERTS
bool _NetworkVerifyf(const diagChannel &channel, const char* condAsString, const char *file, int line, const char* fmt, ...);
#endif 

enum
{
    RLSC_INVALID_FRIEND_ID = InvalidRockstarId,
	RLSC_FRIENDS_MAX_FRIENDS = 250,
	RLSC_FRIENDS_MAX_PENDING_INVITES_SENT = 100,
	RLSC_FRIENDS_MAX_PENDING_INVITES_RECEIVED = 100,
	RLSC_FRIENDS_MAX_BLOCKED_PLAYERS = 1000,
};

enum
{
#if RSG_ORBIS
	RL_MAX_LOCAL_GAMERS         = 4,
	RL_MAX_NAME_BUF_SIZE        = 17,   //SCE_USER_SERVICE_MAX_USER_NAME_LENGTH; includes terminating null
	RL_MAX_DISPLAY_NAME_LENGTH  = RL_MAX_NAME_BUF_SIZE, 
	RL_MAX_DISPLAY_NAME_BUF_SIZE= ((RL_MAX_DISPLAY_NAME_LENGTH * 3) + 1),   // utf-8 encoded unicode + terminating null
	RL_PRESENCE_MAX_BUF_SIZE    = 128,  // User_Profile_WebAPI-Reference - 128 bytes (binary), base64 encoded up to 172 ASCII characters
	RL_MAX_PRESENCE_STR_BUF_SIZE= 192,  // SCE_NP_GAME_STATUS_MAX_BUF_SIZE
	RL_MAX_BLOCK_LIST			= 2000,	// see Sony docs on User_Profile_WebAPI-Reference
	RL_MAX_PARTY_SIZE			= 8,
	RL_MAX_PLAY_TOGETHER_GROUP  = 7,
#elif RSG_DURANGO
	RL_MAX_LOCAL_GAMERS         = 16,
	RL_MAX_NAME_BUF_SIZE        = 16,   //Includes terminating null
	RL_MAX_DISPLAY_NAME_LENGTH  = 17, // 16 unicode chars + ellipsis. Technically real names can be longer but this is the min length we are required by XRs to display on screen.
	RL_MAX_DISPLAY_NAME_BUF_SIZE= ((RL_MAX_DISPLAY_NAME_LENGTH * 3) + 1),   // utf-8 encoded unicode + terminating null
	RL_PRESENCE_MAX_BUF_SIZE	= 128,	// Parity with PS4
	RL_PRESENCE_MAX_STAT_SIZE	= 64,	// The XB1 User Events provides a 64-length buffer
	RL_PRESENCE_MAX_FIELDS		= 3,
	RL_MAX_PARTY_SIZE			= 32,
	RL_MAX_STAT_BUF_SIZE		= 256,
#elif RSG_PC
	RL_MAX_LOCAL_GAMERS         = 1,
	RL_MAX_NAME_BUF_SIZE        = 17,   // social club names can be up to 16 characters + 1 for null terminator
    RL_PRESENCE_MAX_BUF_SIZE    = 256,	// For Steam: max value length passed to SetRichPresence() is k_cchMaxRichPresenceValueLength (256 chars)
	RL_MAX_DISPLAY_NAME_BUF_SIZE = RL_MAX_NAME_BUF_SIZE,
	RL_MAX_PARTY_SIZE			= 8,
#else
    RL_MAX_LOCAL_GAMERS         = 1,
    RL_MAX_NAME_BUF_SIZE        = 16,   //Includes terminating null
	RL_MAX_PARTY_SIZE			= 8,
#endif

    RL_MAX_NAME_LENGTH          = RL_MAX_NAME_BUF_SIZE - 1,

    RL_MAX_INVITE_SUBJECT_CHARS     = 16,
    RL_MAX_INVITE_SALUTATION_CHARS  = 128,

	RL_MAX_MESSAGE_SUBJECT_CHARS	= 18,  // SCE_NP_BASIC_SUBJECT_CHARACTER_MAX
	RL_MAX_MESSAGE_CONTENT_CHARS	= 256, // SCE_NP_BASIC_BODY_CHARACTER_MAX (512)
	RL_MAX_MESSAGE_RECIPIENTS		= 10,

	RL_MAX_USERID_BUF_LENGTH		= 128,

	RL_MAX_URL_BUF_LENGTH = 2048, // ps4 SCE_WEB_BROWSER_DIALOG_URL_SIZE, pc INTERNET_MAX_PATH_LENGTH

    RL_MAX_SERVICE_ABBREV_CHARS = 3,

    //This must be large enough to hold largest result of rlGamerHandle::ToString()
    RL_MAX_GAMER_HANDLE_CHARS         =
        RL_MAX_SERVICE_ABBREV_CHARS     //Service id
        + 1                             //Space
        + 4                             //NP environment (ticket issuer ID)
        + 2                             //Space plus " (for NP)
        + RL_MAX_NAME_BUF_SIZE          //Gamer name
        + 2                             //" plus space (for NP)
        + 1,                            //Null terminator
    
    RL_MAX_GAMERS_PER_SESSION   = 32,
    RL_MAX_LEADERBOARD_COLUMNS  = 64,

    // Maximum number of chars in a group handle (see rlStats.h, group leaderboards).
    // FIXME(KB) This should be reduced to 32 or 64 when the backend can handle it.
    RL_GROUP_HANDLE_MAX_CHARS   = 128,  //Includes the terminating NULL

#if RL_SUPPORT_NETWORK_BOTS
    RL_MAX_NETWORK_BOTS = RL_MAX_GAMERS_PER_SESSION - RL_MAX_LOCAL_GAMERS,
#endif

	//Max length of a filter name, including null terminator.
	RL_MAX_MATCHING_FILTER_NAME_LENGTH       = 24,

	//Max length of attribute name, including null terminator.
	RL_MAX_MATCHING_ATTR_NAME_LENGTH       = 24,

    //Max number of attributes that can be advertised in matchmaking.
	RL_MAX_MATCHING_ATTRS       = 8,

    //Max number of conditions in a matchmaking filter.
    RL_MAX_MATCHING_CONDITIONS  = 32,

    //Max size of app-specific data that can be returned with QoS responses
    RL_MAX_QOS_USER_DATA_SIZE   = 512,

	//Max size of app-specific data that can be returned with config responses
	RL_MAX_SESSION_USER_DATA_SIZE   = 256,

	//Max size of app-specific data-mine that can be returned with config responses
	RL_MAX_SESSION_DATA_MINE_SIZE   = 128,

	//Several places in code we do a search for a set of sessions or gather session
	//details from those sessions. This is a common limit that can be 
	//searched/queried/addressed at once.
	RL_FIND_SESSIONS_MAX_SESSIONS_TO_FIND = 20,

	// The RGSC SDK leverages RAGE functionality that often requires a gamer index.
	// In all cases, this gamer index is defined as ZERO.
	RL_RGSC_GAMER_INDEX = 0,

	// Session "Rich Presence" - Sessions on PS4 can contain names, statuses and images.
	RL_MAX_SESSION_NAME_LENGTH	= 64,
	RL_MAX_SESSION_NAME_BUF_SIZE = RL_MAX_BUF_FOR_UTF_CHARS(RL_MAX_SESSION_NAME_LENGTH), // 64 UTF-8 characters + null terminator
	RL_MAX_SESSION_STATUS_LENGTH = 128,
	RL_MAX_SESSION_STATUS_BUF_SIZE = RL_MAX_BUF_FOR_UTF_CHARS(RL_MAX_SESSION_STATUS_LENGTH), // 64 UTF-8 characters + null terminator
	RL_MAX_SESSION_IMAGE_BUF_SIZE = (160 * 1024), // 160 KiB
};

typedef char rlDisplayName[RL_MAX_DISPLAY_NAME_BUF_SIZE];
typedef char rlGamertag[RL_MAX_NAME_BUF_SIZE];
typedef char rlUserIdBuf[RL_MAX_USERID_BUF_LENGTH];

enum rlLanguage
{
	RL_LANGUAGE_INVALID         = -1,
	RL_LANGUAGE_ENGLISH,
	RL_LANGUAGE_SPANISH,
	RL_LANGUAGE_FRENCH,
	RL_LANGUAGE_GERMAN,
	RL_LANGUAGE_ITALIAN,
	RL_LANGUAGE_JAPANESE,
	RL_LANGUAGE_RUSSIAN,
	RL_LANGUAGE_PORTUGUESE,
	RL_LANGUAGE_POLISH,
	RL_LANGUAGE_KOREAN,
	RL_LANGUAGE_NUM_LANGUAGES
};

enum rlNetworkMode
{
    RL_NETMODE_INVALID      = -1,
    RL_NETMODE_OFFLINE,
    RL_NETMODE_ONLINE,
    RL_NETMODE_LAN,

    RL_NETMODE_NUM_MODES
};

enum rlSlotType
{
    RL_SLOT_INVALID     = -1,
    RL_SLOT_PUBLIC,
    RL_SLOT_PRIVATE,

    RL_SLOT_NUM_TYPES
};

//PURPOSE
//  Environment IDs.
enum rlEnvironment
{
    RL_ENV_UNKNOWN = -1,    //Platform has no native service, or environment has not yet been determined
    RL_ENV_DEV,             //Development
    RL_ENV_CERT,            //Certification
    RL_ENV_PROD,            //Production
	// ADD NEW ENVIRONMENTS AFTER THIS, DO NOT CHANGE ORDER.
	RL_ENV_CERT_2,          //Certification2
	RL_ENV_CI_RAGE,			//Continuous-Integration of Rage
    RL_NUM_ENV
};

// These force a particular environment 
// The check for an existing definition allows a build tool to specify the environment we run to make it 
// easier for the build team to make custom exes
// Without a pre-define, the forced environment will only be applied when the environment supplied is in 
// the development environment / sandbox
#if !defined(RL_FORCE_DEV_ENVIRONMENT)
 #define RL_FORCE_DEV_ENVIRONMENT			(0)	// force dev environment
#elif RL_FORCE_DEV_ENVIRONMENT
 #define RL_HAS_FORCED_ENVIRONMENT			(1)
#endif

#if !defined(RL_FORCE_STAGE_DEV_ENVIRONMENT)
 #define RL_FORCE_STAGE_DEV_ENVIRONMENT		(0)	// force stage-dev environment
#elif RL_FORCE_STAGE_DEV_ENVIRONMENT
 #define RL_HAS_FORCED_ENVIRONMENT			(1)
#endif

#if !defined(RL_FORCE_CERT_ENVIRONMENT)
 #define RL_FORCE_CERT_ENVIRONMENT			(0)	// force cert environment
#elif RL_FORCE_CERT_ENVIRONMENT
 #define RL_HAS_FORCED_ENVIRONMENT			(1)
#endif

#if !defined(RL_FORCE_STAGE_CERT_ENVIRONMENT)
 #define RL_FORCE_STAGE_CERT_ENVIRONMENT	(0)	// force stage-cert environment
#elif RL_FORCE_STAGE_CERT_ENVIRONMENT
 #define RL_HAS_FORCED_ENVIRONMENT			(1)
#endif

#if !defined(RL_FORCE_PROD_ENVIRONMENT)
 #define RL_FORCE_PROD_ENVIRONMENT			(0)	// force prod environment
#elif RL_FORCE_PROD_ENVIRONMENT
 #define RL_HAS_FORCED_ENVIRONMENT			(1)
#endif

#if !defined(RL_FORCE_STAGE_PROD_ENVIRONMENT)
 #define RL_FORCE_STAGE_PROD_ENVIRONMENT	(0)	// force stage-prod environment
#elif RL_FORCE_STAGE_PROD_ENVIRONMENT
 #define RL_HAS_FORCED_ENVIRONMENT			(1)
#endif

#define RL_FORCE_ANY_ENVIRONMENT    (RL_FORCE_DEV_ENVIRONMENT || RL_FORCE_STAGE_DEV_ENVIRONMENT || RL_FORCE_CERT_ENVIRONMENT || RL_FORCE_STAGE_CERT_ENVIRONMENT || RL_FORCE_PROD_ENVIRONMENT || RL_FORCE_STAGE_PROD_ENVIRONMENT)

// Specifies the environment we are forcing
#if RL_FORCE_DEV_ENVIRONMENT || RL_FORCE_STAGE_DEV_ENVIRONMENT
#define RL_FORCED_ENVIRONMENT       (rlEnvironment::RL_ENV_DEV)
#elif RL_FORCE_CERT_ENVIRONMENT || RL_FORCE_STAGE_CERT_ENVIRONMENT
#define RL_FORCED_ENVIRONMENT       (rlEnvironment::RL_ENV_CERT)
#elif RL_FORCE_PROD_ENVIRONMENT || RL_FORCE_STAGE_PROD_ENVIRONMENT
#define RL_FORCED_ENVIRONMENT       (rlEnvironment::RL_ENV_PROD)
#endif

// This will force the staging environment of RL_FORCED_ENVIRONMENT
// Needs RL_FORCED_ENVIRONMENT to be defined via one of the RL_FORCE_*_ENVIRONMENT defines
#define RL_FORCE_STAGE_ENVIRONMENT  (RL_FORCE_STAGE_DEV_ENVIRONMENT || RL_FORCE_STAGE_CERT_ENVIRONMENT || RL_FORCE_STAGE_PROD_ENVIRONMENT)

#if RL_SC_PLATFORMS
// On these platforms, we don't have sandboxes that determine the default ROS environment so this needs to be specified by the client
#if __MASTER
 #define RL_DEFAULT_ROS_ENVIRONMENT RLROS_ENV_PROD
#else
 #define RL_DEFAULT_ROS_ENVIRONMENT RLROS_ENV_DEV
#endif
#endif

// We only allow force overrides to be applied in the dev environment (this is necessary to prevent our shipped executable from including a 
// forced override in the dev_live branch where RL_FORCE_STAGE_CERT_ENVIRONMENT is defaulted to on for QA / testing). 
// This define bypasses that dev requirement for the purposes of a build tool that allows the build team to create custom master 
// executables that connect to environments other than prod by specifying the forced environment as a build parameter
// This is only required on SC platforms where we do not have functionality to have the platform provide an environment for us
// In future, this would ideally come from RGL or an equivalent. 
#if defined(RL_HAS_FORCED_ENVIRONMENT) && RL_SC_PLATFORMS
#define RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT	(1)
#else
#define RL_ALLOW_FORCE_IN_ANY_ENVIRONMENT	(0)
#endif

#if RL_FORCE_ANY_ENVIRONMENT
 #define RL_FORCED_ENVIRONMENT_ONLY(...) __VA_ARGS__
#else
 #define RL_FORCED_ENVIRONMENT_ONLY(...) 
#endif

#if RSG_OUTPUT || RL_FORCE_ANY_ENVIRONMENT
#define RL_STAGE_DEV_PROXY_ADDRESS "192.81.241.92"
#define RL_STAGE_DEV_PRESENCE_ADDRESS "192.81.241.92:61457"
#define RL_STAGE_DEV_RELAY_ADDRESS (const char*)nullptr

#define RL_STAGE_CERT_PROXY_ADDRESS "192.81.241.98"
#define RL_STAGE_CERT_PRESENCE_ADDRESS "192.81.241.98:61457"
#define RL_STAGE_CERT_RELAY_ADDRESS (const char*)nullptr

#define RL_STAGE_PROD_PROXY_ADDRESS "192.81.241.99"
#define RL_STAGE_PROD_PRESENCE_ADDRESS "192.81.241.54:61457"
#define RL_STAGE_PROD_RELAY_ADDRESS (const char*)nullptr
#endif

#if RL_FORCE_STAGE_ENVIRONMENT
 #if !RL_FORCE_ANY_ENVIRONMENT
  #error "Must define RL_FORCED_ENVIRONMENT"
 #endif
 #define RL_FORCE_STAGE_ENVIRONMENT_ONLY(...) __VA_ARGS__
 #if RL_FORCE_STAGE_DEV_ENVIRONMENT
  #define RL_FORCED_PROXY_ADDRESS      RL_STAGE_DEV_PROXY_ADDRESS
  #define RL_FORCED_PRESENCE_ADDRESS   RL_STAGE_DEV_PRESENCE_ADDRESS
 #elif RL_FORCE_STAGE_CERT_ENVIRONMENT
  #define RL_FORCED_PROXY_ADDRESS      RL_STAGE_CERT_PROXY_ADDRESS
  #define RL_FORCED_PRESENCE_ADDRESS   RL_STAGE_CERT_PRESENCE_ADDRESS
 #elif RL_FORCE_STAGE_PROD_ENVIRONMENT
  #define RL_FORCED_PROXY_ADDRESS      RL_STAGE_PROD_PROXY_ADDRESS
  #define RL_FORCED_PRESENCE_ADDRESS   RL_STAGE_PROD_PRESENCE_ADDRESS
 #endif
#else
 #define RL_FORCE_STAGE_ENVIRONMENT_ONLY(...) 
#endif

// for PC, we need to use hosts file overrides for proxying
#define RL_USE_FORCED_HTTP_PROXY (RL_FORCE_STAGE_ENVIRONMENT && !RSG_PC)

//PURPOSE
//  Returns the environment that we are using. This is a combination of the native / platform
//  environment and any forced environment overrides at run or compile time
rlEnvironment rlGetEnvironment();

//PURPOSE
//  Returns the environment the native online service is in.  This may not always 
//  be known, either because the platform has no native online service (ex. PC) or 
//  because it is not known until some action has been performed (ex. retrieving NP 
//  ticket, connecting to XLSP SG).  If this returns RL_ENV_UNKNOWN, it may just be
//  that the environment hasn't been determined yet.
rlEnvironment rlGetNativeEnvironment();

//PURPOSE
//  Returns the environment as a string
const char* rlGetEnvironmentString(const rlEnvironment environment);

//PURPOSE
//  Regions that the game can be built for.    
enum rlGameRegion
{
    RL_GAME_REGION_INVALID = -1,
    RL_GAME_REGION_AMERICA,
    RL_GAME_REGION_EUROPE,
    RL_GAME_REGION_JAPAN,

    RL_GAME_REGION_NUM_REGIONS,
};

enum rlPresenceInviteFlags
{
	Flags_None = 0,
	Flag_IsJoin = BIT0,
	Flag_IsSpectator = BIT1,
};

enum rlPresenceInviteUnavailableReason
{
	Reason_Invalid = -1,
	Reason_NotOnline,
	Reason_NoOnlinePrivilege,
	Reason_ServiceError,
};

//PURPOSE
//  Country codes
#define COUNTRY_CODE_UAE			"ae"
#define COUNTRY_CODE_ARGENTINA		"ar"
#define COUNTRY_CODE_AUSTRIA		"at"
#define COUNTRY_CODE_AUSTRALIA		"au"
#define COUNTRY_CODE_BELGIUM		"be"
#define COUNTRY_CODE_BULGARIA		"bg"
#define COUNTRY_CODE_BAHRAIN		"bh"
#define COUNTRY_CODE_BOLIVIA		"bo"
#define COUNTRY_CODE_BRAZIL			"br"
#define COUNTRY_CODE_CANADA			"ca"
#define COUNTRY_CODE_SWITZERLAND	"ch"
#define COUNTRY_CODE_CHILE			"cl"
#define COUNTRY_CODE_CHINA			"cn"
#define COUNTRY_CODE_COLOMBIA		"co"
#define COUNTRY_CODE_COSTA_RICA		"cr"
#define COUNTRY_CODE_CYPRUS			"cy"
#define COUNTRY_CODE_CZECH_REPUBLIC	"cz"
#define COUNTRY_CODE_GERMANY		"de"
#define COUNTRY_CODE_DENMARK		"dk"
#define COUNTRY_CODE_ECUADOR		"ec"
#define COUNTRY_CODE_SPAIN			"es"
#define COUNTRY_CODE_FINLAND		"fi"
#define COUNTRY_CODE_FRANCE			"fr"
#define COUNTRY_CODE_UK				"gb"
#define COUNTRY_CODE_GREECE			"gr"
#define COUNTRY_CODE_GUATEMALA		"gt"
#define COUNTRY_CODE_HONG_KONG		"hk"
#define COUNTRY_CODE_HONDURAS		"hn"
#define COUNTRY_CODE_CROATIA		"hr"
#define COUNTRY_CODE_HUNGARY		"hu"
#define COUNTRY_CODE_INDONESIA		"id"
#define COUNTRY_CODE_IRELAND		"ie"
#define COUNTRY_CODE_ISRAEL			"il"
#define COUNTRY_CODE_INDIA			"in"
#define COUNTRY_CODE_ICELAND		"is"
#define COUNTRY_CODE_ITALY			"it"
#define COUNTRY_CODE_JAPAN			"jp"
#define COUNTRY_CODE_KOREA			"kr"
#define COUNTRY_CODE_KUWAIT			"kw"
#define COUNTRY_CODE_LEBANON		"lb"
#define COUNTRY_CODE_LUXEMBOURG		"lu"
#define COUNTRY_CODE_MALTA			"mt"
#define COUNTRY_CODE_MEXICO			"mx"
#define COUNTRY_CODE_MALAYSIA		"my"
#define COUNTRY_CODE_NICARAGUA		"ni"
#define COUNTRY_CODE_NETHERLANDS	"nl"
#define COUNTRY_CODE_NORWAY			"no"
#define COUNTRY_CODE_NEW_ZEALAND	"nz"
#define COUNTRY_CODE_OMAN			"om"
#define COUNTRY_CODE_PANAMA			"pa"
#define COUNTRY_CODE_PERU			"pe"
#define COUNTRY_CODE_POLAND			"pl"
#define COUNTRY_CODE_PORTUGAL		"pt"
#define COUNTRY_CODE_PARAGUAY		"py"
#define COUNTRY_CODE_QATAR			"qa"
#define COUNTRY_CODE_ROMANIA		"ro"
#define COUNTRY_CODE_RUSSIA			"ru"
#define COUNTRY_CODE_SAUDI_ARABIA	"sa"
#define COUNTRY_CODE_SWEDEN			"se"
#define COUNTRY_CODE_SINGAPORE		"sg"
#define COUNTRY_CODE_SLOVENIA		"si"
#define COUNTRY_CODE_SLOVAKIA		"sk"
#define COUNTRY_CODE_EL_SALVADOR	"sv"
#define COUNTRY_CODE_THAILAND		"th"
#define COUNTRY_CODE_TURKEY			"tr"
#define COUNTRY_CODE_TAIWAN			"tw"
#define COUNTRY_CODE_UKRAINE		"ua"
#define COUNTRY_CODE_UNITED_STATES	"us"
#define COUNTRY_CODE_URUGUAY		"uy"
#define COUNTRY_CODE_SOUTH_AFRICA	"za"

//PURPOSE
//  Age group of signed-in player.
//  What each age group implies about actual age varies by region.
//  Currently these are only used on XBL.  PSN gives us the actual
//  age of the player.
enum rlAgeGroup
{
    RL_AGEGROUP_INVALID = -1,
    RL_AGEGROUP_PENDING,        //Age group is being retrieved.
    RL_AGEGROUP_CHILD,
    RL_AGEGROUP_TEEN,
    RL_AGEGROUP_ADULT
};

//PURPOSE
//  Returns the region the game was built for.
//NOTES
//  This may be called before rlInit().
rlGameRegion rlGetGameRegion();

//PURPOSE
//	Flag set for different levels of creDENTIALS
enum rlCheckCredentialsFlags
{
	RL_CREDS_NONE = 0,
	RL_CREDS_PLATFORM = BIT0,
	RL_CREDS_ROS = BIT1, 
	RL_CREDS_ROCKSTAR_ID = BIT2,

	RL_CREDS_SOCIALCLUB_MEMBER = RL_CREDS_ROS | RL_CREDS_ROCKSTAR_ID,
};

//PURPOSE
//  Leaderboard service IDs.  Usually you will always be using RL_LBSERVICE_DEFAULT,
//  as most platforms only allow one service.  In rare instances, two services will
//  need to be used (like Gamespy on the 360), in which case these IDs must be used
//  to specify which service to use.
enum rlLeaderboardService
{
    RL_LEADERBOARD_SERVICE_INVALID = -1,
    RL_LEADERBOARD_SERVICE_XBL,
    RL_LEADERBOARD_SERVICE_ROS,
    RL_LEADERBOARD_SERVICE_NUM_SERVICES,
    RL_LEADERBOARD_SERVICE_DEFAULT = RL_LEADERBOARD_SERVICE_ROS,
};

//PURPOSE
//  Used to identify the leaderboard type.
enum rlLeaderboardType
{
    RL_LEADERBOARD_TYPE_INVALID = 0,
    RL_LEADERBOARD_TYPE_PLAYER,         //Ranks players
    RL_LEADERBOARD_TYPE_GROUP,          //Ranks groups
    RL_LEADERBOARD_TYPE_GROUP_MEMBER    //Ranks members within a group
};

//PURPOSE
//  Used to identify the leaderboard type.
enum rlLeaderboard2Type
{
    RL_LEADERBOARD2_TYPE_INVALID = 0,
    RL_LEADERBOARD2_TYPE_PLAYER,        //Ranks players
    RL_LEADERBOARD2_TYPE_CLAN,          //Ranks clans
    RL_LEADERBOARD2_TYPE_CLAN_MEMBER,   //Ranks members within a clan
    RL_LEADERBOARD2_TYPE_GROUP,         //Ranks groups
    RL_LEADERBOARD2_TYPE_GROUP_MEMBER   //Ranks members within a group
};

const unsigned RL_LEADERBOARD_INVALID_ID = 0;

const u64 RL_INVALID_PEER_ID = ~u64(0);

class netSocket;
class netStatus;
class sysMemAllocator;
class rlTitleId;
class rlTaskManager;

//PURPOSE
//  Initialize rline.
//PARAMS
//  allocator   - Used for all rline memory allocation.
//  skt         - Socket used by rline to communicate with various services
//                (e.g. matchmaking, stats, NAT negotiation, etc.).
//  titleId     - Ptr to title information.
//  minAgeRating    - Minimum age rating for the game - used to restrict multiplayer
//                    privileges.
//NOTES
//  The netSocket* instance should be the same socket used by the game
//  for game-related communication.  Rline will multiplex its own
//  communication on this socket by using virtual sockets (netVSocket)
//  that it registers with the netSocket.
//
//  In most applications there should only ever be *one* netSocket instance.
//  NAT negotiation proceeds much more smoothly if this is the case because
//  all net communication occurs over a single port.
//
//  The user is responsible for the data pointed to by titleId, and should
//  not deallocate it until rlShutdown() is called.
bool rlInit(sysMemAllocator* allocator,
            netSocket* skt,
            rlTitleId *titleId,
            const unsigned minAgeRating);

//PURPOSE
//  Updates rline internals.  Should be called every frame.
void rlUpdate();

//PURPOSE
//  Shuts down rline.
void rlShutdown();

//PURPOSE
//  Returns true if rline has been initialized.
bool rlIsInitialized();

// PURPOSE
//	global task manager for rline tasks
rlTaskManager* rlGetTaskManager();

// PURPOSE
//	global rline allocator
sysMemAllocator* rlGetAllocator();

//PURPOSE
//  Returns the minimum age to play the game.
unsigned rlGetMinAgeRating();

//PURPOSE
//  Sets the minimum age to play the game.
void rlSetMinAgeRating(int newAge);

//PURPOSE
// Sets the current language for rline systems that require it
void rlSetLanguage(sysLanguage language);

//PURPOSE
//  Returns the language set by rlSetLanguage().
sysLanguage rlGetLanguage();

//PURPOSE
//  Returns the language code for the specified language enum.
const char* rlGetLanguageCode(sysLanguage language);

//PURPOSE
//   Returns posix time (seconds since midnight 1970-01-01).
//   When online the time is synchronized to server time.
u64 rlGetPosixTime(bool forceSync = false);

//PURPOSE
//   Sets the current posix time (seconds since midnight 1970-01-01).
//   Should come from a trusted online server.
void rlSetPosixTime(u64 posixTime);

//PURPOSE
//	Returns the size of the system party
u32 rlGetSystemPartySize();

//PURPOSE
//	Returns whether we are in a forced environment
bool rlIsForcedEnvironment();

//PURPOSE
//  True if we're connecting to a staging environment.
bool rlIsUsingStagingEnvironment();

//PURPOSE
//  Validates the current environment setup is allowed. Quits if bad configuration detected.
bool rlValidateEnvironment();

//PURPOSE
//  Passes in the native environment and returns the ROS environment we will connect to based on that
//  and force settings / defines
rlEnvironment rlCheckAndApplyForcedEnvironment(const rlEnvironment nativeEnv);

//PURPOSE
//  64-bit UIID
struct rlUuid64
{
    u64 Uuid;

    bool operator==(const rlUuid64& that)
    {
        return Uuid == that.Uuid;
    }

    bool operator!=(const rlUuid64& that)
    {
        return Uuid != that.Uuid;
    }
};

//PURPOSE
//  128-bit UIID
struct rlUuid128
{
    u64 Uuid[2];

    bool operator==(const rlUuid128& that)
    {
        //Compare the second u64 first because it varies faster
        return Uuid[1] == that.Uuid[1] && Uuid[0] == that.Uuid[0];
    }

    bool operator!=(const rlUuid128& that)
    {
        return Uuid[1] != that.Uuid[1] || Uuid[0] != that.Uuid[0];
    }
};

//PURPOSE
//  Creates a unique 64-bit id.
//  IDs created by the 64-bit version of rlCreateUUID
//  should not be persisted, and are not guaranteed to be globally unique.
bool rlCreateUUID(u64* uuid);
bool rlCreateUUID(rlUuid64* uuid64);

//PURPOSE
//  Creates a unique 128-bit id.
//  IDs created by the 64-bit version of rlCreateUUID
//  should not be persisted, and are not guaranteed to be globally unique.
bool rlCreateUUID(rlUuid128* uuid128);

////////////////////////////////////////////////////////////////////////////////
//  Rline heap helper functions. These are meant for rline internal use only.
////////////////////////////////////////////////////////////////////////////////

//Convenience macros for allocating bytes and objects with rline allocator.
#if !__FINAL
#define RL_ALLOCATE(trackName, size)        rlLoggedAllocate(size, __FILE__, __LINE__, #trackName)
#define RL_ALLOCATE_T(trackName, type)      (type*)rlLoggedAllocate(sizeof(type), __FILE__, __LINE__, #trackName)
#define RL_NEW(trackName, type)             rlLoggedNew< type >(1, __FILE__, __LINE__, #trackName)
#define RL_NEW_ARRAY(trackName, type, len)  rlLoggedNew< type >(len, __FILE__, __LINE__, #trackName)
#else
// don't put __FILE__ or trackName into exe's released to the public
#define RL_ALLOCATE(trackName, size)        rlLoggedAllocate(size, "", 0, "")
#define RL_ALLOCATE_T(trackName, type)      (type*)rlLoggedAllocate(sizeof(type), "", 0, "")
#define RL_NEW(trackName, type)             rlLoggedNew< type >(1, "", 0, "")
#define RL_NEW_ARRAY(trackName, type, len)  rlLoggedNew< type >(len, "", 0, "")
#endif

#define RL_FREE(a)                          rlFree(a)
#define RL_DELETE(a)                        rlDelete(a)
#define RL_DELETE_ARRAY(a, len)             rlDelete(a, len)

//PURPOSE
// Does a logged allocate with the rline allocator.
void* rlLoggedAllocate(const size_t size, const char* fileName, int lineNumber, const char* trackName);


#if __WIN32
//Necessary to get rid of this warning:
//  behavior change: an object of POD type constructed with an initializer of the form () will be default-initialized
#pragma warning (disable:4345 )
#endif

template<typename T>
T* rlLoggedNew(const size_t len, const char* fileName, int lineNumber, const char* trackName)
{
    T* t = (T*)rlLoggedAllocate(sizeof(T) * len, fileName, lineNumber, trackName);
    if(t)
    {
        for(unsigned i = 0; i < len; i++) 
        {
            new(&t[i]) T();
        }
    }
    return t;
}

//PURPOSE
//  Returns size of an allocation made on the rline heap.
size_t rlGetAllocSize(const void* ptr);

//PURPOSE
//  Frees memory allocated with non-template rlAlloc.
void rlFree(const void* ptr);

//PURPOSE
//  Deletes one or more objects allocated with rlAlloc<T> or rlNew<T>.
template<typename T>
void rlDelete(T* t, const unsigned count = 1)
{
    if(t)
    {
        for(unsigned i = 0; i < count; ++i)
        {
            t[i].~T();
        }

        rlFree(t);
    }
}

#if RSG_NP

struct rlSceNpOnlineId
{
	static const unsigned MAX_DATA_BUF_SIZE = 16;
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = MAX_DATA_BUF_SIZE + (sizeof(char) * 4);

	static rlSceNpOnlineId RL_INVALID_ONLINE_ID; 

	rlSceNpOnlineId() { Clear(); }

	void Clear();
	bool IsValid() const;
	void FromString(const char* onlineId);

    void Reset(const SceNpOnlineId& that);
    SceNpOnlineId& AsSceNpOnlineId();
    const SceNpOnlineId& AsSceNpOnlineId() const;

    //PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    char data[SCE_NP_ONLINEID_MAX_LENGTH];
    char term;
    char dummy[3];
};

struct rlSceNpId
{
    void Clear();
    void Reset(const SceNpId& that);
    SceNpId& AsSceNpId();
    const SceNpId& AsSceNpId() const;

    //PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    rlSceNpOnlineId handle;
    u8 opt[8];
    u8 reserved[8];
};

typedef u64 rlSceNpAccountId;
extern const rlSceNpAccountId RL_INVALID_NP_ACCOUNT_ID;

#if RSG_ORBIS

struct rlSceNpInvitationId
{
	void Clear();
	void Reset(const SceNpInvitationId& that);
	SceNpInvitationId& AsSceNpInvitationId();
	const SceNpInvitationId& AsSceNpInvitationId() const;

	char data[60];
    char term;
};

struct rlSceNpSessionId
{
	static const unsigned MAX_DATA_BUF_SIZE = 45;
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = MAX_DATA_BUF_SIZE;

	void Clear();
    void Reset(const SceNpSessionId& that);
    SceNpSessionId& AsSceNpSessionId();
    const SceNpSessionId& AsSceNpSessionId() const;
	bool IsValid() const { return data[0] != '\0'; }

	char data[MAX_DATA_BUF_SIZE];
	char term;
	char padding[2]; // Alignment

	//PURPOSE
	//  Exports data in a platform/endian independent format.
	//PARAMS
	//  buf         - Destination buffer.
	//  sizeofBuf   - Size of destination buffer.
	//  size        - Set to number of bytes exported.
	//RETURNS
	//  True on success.
	bool Export(void* buf, const unsigned sizeofBuf, unsigned* size = 0) const;

	//PURPOSE
	//  Imports data exported with Export().
	//  buf         - Source buffer.
	//  sizeofBuf   - Size of source buffer.
	//  size        - Set to number of bytes imported.
	//RETURNS
	//  True on success.
	bool Import(const void* buf, const unsigned sizeofBuf, unsigned* size = 0);
};

struct rlSceNpAuthorizationCode
{
	char code[128 + 1];
	uint8_t padding[7];
};

struct rlSceNpClientId
{
	void Clear();
    void Reset(const SceNpClientId& that);
    SceNpClientId& AsSceNpClientId();
    const SceNpClientId& AsSceNpClientId() const;

	char id[128 + 1];
	uint8_t padding[7];
};

#endif

const char* rlNpGetErrorString(const int err);

#define rlNpError(msg, errorCode)\
    rlError("%s:0x%08x:%s",\
             msg,\
             errorCode,\
             ::rage::rlNpGetErrorString(errorCode))

#endif 

}   //namespace rage

#endif  //RLINE_RL_H
