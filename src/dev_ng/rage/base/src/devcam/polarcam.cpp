//
// devcam/polarcam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "polarcam.h"

#include "bank/bank.h"
#include "input/pad.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "math/angmath.h"
#include "spatialdata/sphere.h"
#include "vector/matrix34.h"
#include "grcore/device.h"

using namespace rage;

void dcamPolarCam::Init(float dist,float azm,float inc,Vector3::Vector3Param Offset) {
	m_Mapper.Reset();

	m_Distance=dist;
	m_Azimuth=azm;
	m_OrientAzimuth=0.0f;
	m_Incline=inc;

	m_InitDistance = dist;
	m_InitAzimuth = azm;
	m_InitIncline = inc;
	m_InitOffset.Set(Offset);

	m_LinearRate=10.0f;
	m_AngularRate=2.0f;
	m_OrientRate=0.05f;

	m_Offset.Set(Offset);
	m_Follow=0;
	m_Orient=0;
	m_FollowOffset=false;

	m_WorldMtx.Identity();
	m_EnableInput=true;

	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::R1, m_ZoomOut);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L1, m_ZoomIn);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::SELECT, m_Control);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LLEFT, m_AziLeft);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LRIGHT, m_AziRight);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LUP, m_IncUp);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LDOWN, m_IncDown);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::R2, m_Up);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L2, m_Down);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_INSERT, m_ZoomOut);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_PAGEUP, m_ZoomIn);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_DELETE, m_AziLeft);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_PAGEDOWN, m_AziRight);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_HOME, m_IncUp);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_END, m_IncDown);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SHIFT, m_Shift);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_Control);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_Alt);

	// Update m_WorldMtx and m_ViewMtx. This used to not be done here,
	// but the result was that if you called Init() and then GetWorldMtx()
	// without calling Update(), you would get the identity matrix rather than
	// the correct initial camera matrix. /FF
	UpdateMatrix();
}

void dcamPolarCam::Init(const Vector3 &cameraPos, const Vector3 &lookAtPos) {
	float dist = cameraPos.Dist(lookAtPos);
	Vector3 lookDir;
	lookDir.Subtract(cameraPos, lookAtPos);

	// Do a quick calculation of azimuth & incline
	float azm, inc;

	if(m_UseZUp)
	{
		azm = acosf(lookDir.y / lookDir.XYMag());
		inc = 0.5f * PI - acosf(lookDir.z / dist);
	}
	else
	{
		azm = acosf(lookDir.z / lookDir.XZMag());
		inc = 0.5f * PI - acosf(lookDir.y / dist);
	}

	Init(dist, azm, inc, lookAtPos);
}

void dcamPolarCam::Reset() {
	Init(m_InitDistance, m_InitAzimuth, m_InitIncline, m_InitOffset);
}

bool dcamPolarCam::UpdateInput(float dt) {
	float angDelta=m_AngularRate * dt;
	float distDelta=m_LinearRate * dt;
	bool moved = false;

	m_Mapper.Update();

	if (m_Control.IsDown()) {
		angDelta*=2.0f;
		distDelta*=10.0f;
	}
	else if (m_Alt.IsDown()) {
		angDelta*=4.0f;
		distDelta*=100.0f;
	}

	if (m_Shift.IsDown()) {
		if (m_AziLeft.IsDown()) 
		{
			m_Offset.AddScaled(m_WorldMtx.a,-distDelta);
			moved = true;
		}
		if (m_AziRight.IsDown()) 
		{
			m_Offset.AddScaled(m_WorldMtx.a,distDelta);
			moved = true;
		}
		if (m_IncUp.IsDown()) 
		{
			if(m_UseZUp)
			{
				m_Offset.z+=distDelta;
			}
			else
			{
				m_Offset.y+=distDelta;
			}
			moved = true;
		}
		if (m_IncDown.IsDown()) 
		{
			if(m_UseZUp)
			{
				m_Offset.z-=distDelta;
			}
			else
			{
				m_Offset.y-=distDelta;
			}
			moved = true;
		}
		if (m_ZoomIn.IsDown() || m_ZoomOut.IsDown()) {
			Vector3 c=m_WorldMtx.c;
			c.y=0;
			c.Normalize();
			if(m_ZoomIn.IsDown()) 
			{
				m_Offset.AddScaled(c,-distDelta);
				moved = true;
			}
			else 
			{
				m_Offset.AddScaled(c,distDelta);
				moved = true;
			}
		}
	}
	else {
		if (m_Up.IsDown())
		{
			if(m_UseZUp)
			{
				m_Offset.z+=distDelta;
			}
			else
			{
				m_Offset.y+=distDelta;
			}
			moved = true;
		}
		if (m_Down.IsDown())
		{
			if(m_UseZUp)
			{
				m_Offset.z-=distDelta;
			}
			else
			{
				m_Offset.y-=distDelta;
			}
			moved = true;
		}
		if (m_ZoomOut.IsDown())	
		{
			m_Distance += distDelta;
			moved = true;
		}
		if (m_ZoomIn.IsDown())		
		{
			m_Distance -= distDelta;
			moved = true;
		}
		if (m_AziLeft.IsDown())	
		{
			m_Azimuth -= angDelta;
			moved = true;
		}
		if (m_AziRight.IsDown())	
		{
			m_Azimuth += angDelta;
			moved = true;
		}
		if (m_IncUp.IsDown())		
		{
			m_Incline += angDelta;
			moved = true;
		}
		if (m_IncDown.IsDown())	
		{
			m_Incline -= angDelta;
			moved = true;
		}
	}

	return moved;
}

bool dcamPolarCam::Update(float dt) {
	bool moved = false;

	if (m_EnableInput)
	{
		moved = UpdateInput(dt);
	}

	if (m_Orient)
	{
		float desiredAzimuth;

		if(m_UseZUp)
		{
			desiredAzimuth = atan2f(m_Orient->x, m_Orient->y);
		}
		else
		{
			desiredAzimuth = atan2f(m_Orient->x, m_Orient->z);
		}

		m_OrientAzimuth=CanonicalizeAngle(powf(m_OrientRate, dt*60.0f)*SubtractAngleShorter(desiredAzimuth, m_OrientAzimuth) + m_OrientAzimuth);
	}

	UpdateMatrix();

	return moved;
}

void dcamPolarCam::Frame(const spdSphere& frameSphere)
{
	Vector3 lookAtPos = VEC3V_TO_VECTOR3(frameSphere.GetCenter());
	Vector3 cameraPos = lookAtPos;
	cameraPos.AddScaled(m_WorldMtx.c, frameSphere.GetRadius().Getf()*2.0f);

	m_Distance = cameraPos.Dist(lookAtPos);
	
	Vector3 lookDir;
	lookDir.Subtract(cameraPos, lookAtPos);

	m_Offset=lookAtPos;
}

void dcamPolarCam::UpdateMatrix()
{
	// This all used to be done inside Update(), but I moved it into its
	// own function so it can also be called from Init(). /FF

	PolarView(m_Distance,m_Azimuth+m_OrientAzimuth,m_Incline,m_WorldMtx);
	m_WorldMtx.d.Add(m_Offset);
	if(m_Follow)
	{
		m_WorldMtx.d.Add(*m_Follow);
		if(m_FollowOffset)
		{
			m_WorldMtx.d.Subtract(m_Offset);
			Vector3 worldOffset;
			m_WorldMtx.Transform3x3(m_Offset,worldOffset);
			m_WorldMtx.d.Add(worldOffset);
		}
	}

	Matrix34 temp;
	if (temp.Inverse(m_WorldMtx))
		Convert(m_ViewMtx,temp);
}


void dcamPolarCam::PolarView(float dist, float azm, float inc, Matrix34 &mtx) {

	/*
	Vector3 v(-inc,azm,0);

	float sx,sy,sz,cx,cy,cz;
	if(v.x==0.0f) {sx=0.0f; cx=1.0f;}
	else {
		cx = cosf(v.x);
		sx = sinf(v.x);
		//cos_and_sin(cx,sx,v.x);
	}
	if(v.y==0.0f) {sy=0.0f; cy=1.0f;}
	else {
		cy = cosf(v.y);
		sy = sinf(v.y);
		//cos_and_sin(cy,sy,v.y);
	}
	if(v.z==0.0f) {sz=0.0f; cz=1.0f;}
	else {
		cz = cosf(v.z);
		sz = sinf(v.z);
		//cos_and_sin(cz,sz,v.z);
	}

	mtx.a.Set(  cz*cy + sz*sx*sy, sz*cx, -cz*sy + sz*sx*cy);
	mtx.b.Set( -sz*cy + cz*sx*sy, cz*cx,  sz*sy + cz*sx*cy);
	mtx.c.Set(  cx*sy,           -sx,     cx*cy);
	*/

	if(m_UseZUp)
	{
		mtx.Identity();
		mtx.RotateX(-inc);
		mtx.RotateZ(azm);
	}
	else
	{
		mtx.Identity();
		mtx.RotateX(-inc);
		mtx.RotateY(azm);
	}

	mtx.d.Scale(mtx.c,dist);

}

void dcamPolarCam::Draw(int scrX, int scrY, float alpha) const {
	// show instructions on screen:
#if !__CONSOLE
	scrY=DrawLine(scrX, scrY, "INSERT & PAGEUP: zoom out/in", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "DELETE & PAGEDOWN: azimuth left/right", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "HOME & END: incline left/right", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "hold down CONTROL to modify distance 10x & angle delta 2x", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "hold down ALT to modify distance 100x & angle delta 4x", Color32(1.f,1.f,1.f,alpha));
#endif
	scrY=DrawLine(scrX, scrY, "L1 & R1: zoom in/out", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "L2 & R2: up/down", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "lLeft & lRight: azimuth left/right", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "lUp & lDown: incline left/right", Color32(1.f,1.f,1.f,alpha));
}

#if __BANK
void dcamPolarCam::AddWidgets(bkBank &bk) {
	bk.AddSlider("Distance",&m_Distance,0,1000000,0.1f);
	bk.AddSlider("Azimuth",&m_Azimuth,-100,100,0.01f);
	bk.AddSlider("Incline",&m_Incline,-100,100,0.01f);
	bk.AddSlider("Offset",&m_Offset,-1000000.0f,1000000.0f,0.1f);
}
#endif
