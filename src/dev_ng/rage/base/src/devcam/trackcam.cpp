//
// devcam/trackcam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "trackcam.h"

#include "trackball.h"

#include "bank/bank.h"
#include "grcore/device.h"
#include "grcore/im.h"
#include "input/input.h"
#include "system/timer.h"

using namespace rage;

void dcamTrackCam::Init(float x,float y,float z, float transScale) 
{
	m_StartPos.Set(x,y,z);
	m_Scale = transScale;
	
	m_POV = false;

	m_MouseWindowOverrideWidth = 0.0f;
	m_MouseWindowOverrideHeight = 0.0f;

	m_StartLookAt.Set(0.f, 0.f, 0.f);

	Reset();

    UpdateMatrix( 0.0f ); // initialize m_WorldMtx
}

void dcamTrackCam::Init(const Vector3 &camPos, const Vector3 & lookAtPos) {
	m_StartPos.Set(camPos);
	m_StartLookAt.Set(lookAtPos);

	Reset();
}

void dcamTrackCam::Reset() 
{
	m_ViewMtx.Identity();
	m_ViewMtx.d = m_StartPos;
	m_ViewMtx.LookAt(m_StartLookAt, m_vUp);
	
	m_Axis.Identity();

	m_Quat[0] = m_Quat[1] = m_Quat[2] = 0.0f;
	m_Quat[3] = 1.0f;

	m_WheelScale = 1.0f;
	m_Reverse = false;
	
	m_LastX = MOUSE.GetX();
	m_LastY = MOUSE.GetY();
}

bool dcamTrackCam::Update(float dt)
{
	bool moved = UpdateInput(dt);
	UpdateMatrix(dt);

	return moved;
}

bool dcamTrackCam::UpdateInput(float dt) {
	bool moved = false;

	// Assumes that someone else has already updated the mouse.
	// Normalize to the size of the window
#if HAVE_MOUSE
	float mouseInvWidth = (m_MouseWindowOverrideWidth > 0.0f ? 1.0f / m_MouseWindowOverrideWidth : MOUSE.GetInvWidth() );
	float mouseInvHeight = (m_MouseWindowOverrideHeight > 0.0f ? 1.0f / m_MouseWindowOverrideHeight : MOUSE.GetInvHeight() );

	float dx = (MOUSE.GetX() - m_LastX) * mouseInvWidth;
	float dy = (MOUSE.GetY() - m_LastY) * mouseInvHeight;
	int dz = MOUSE.GetDZ();
	
	if (dx || dy || dx)
	{
		moved = true;
	}

	static bool hadMotion=false;
	static float zoomAccum;
	static sysTimer zoomTimer;

	// Button assignments are like node, finally!
	if ((MOUSE.GetButtons() & ioMouse::MOUSE_RIGHT) || dz || (MOUSE.GetButtons() == (ioMouse::MOUSE_LEFT| ioMouse::MOUSE_MIDDLE))) {	// Translate down our C axis.
		if (MOUSE.GetPressedButtons() & ioMouse::MOUSE_RIGHT)
			hadMotion = false;
		if (MOUSE.GetButtons() == (ioMouse::MOUSE_LEFT | ioMouse::MOUSE_MIDDLE)) {
			zoomTimer.Reset();
			zoomAccum = 0;
		}
		Matrix34 M;
		M.Identity();
		M.d.Set(0,0,m_Scale * dt * (dz? (((float)dz)*m_WheelScale) : (dx - dy)));
		m_ViewMtx.Dot(M);
		if (dx || dy)
			hadMotion = true;
	}
	else if (zoomAccum < 0.1f) {
		zoomAccum = zoomTimer.GetTime();
	}
	else if (MOUSE.GetButtons() & ioMouse::MOUSE_MIDDLE) {	// Translate along our AB axis
		const float tranScale = 1;
		float factor = m_Reverse ? -1.0f : 1.0f;
		Matrix34 M;
		M.Identity();
		M.d.Set(m_Scale * tranScale * dx * factor,m_Scale * tranScale * -dy * factor,0);
		m_ViewMtx.Dot(M);
	}
	else if (MOUSE.GetButtons() & ioMouse::MOUSE_LEFT) {	// Trackball mode!
		// Just pressed the button, remember
		float normX = ((MOUSE.GetX() * mouseInvWidth) - 0.5f) * 2.0f;
		float normY = ((MOUSE.GetY() * mouseInvHeight) - 0.5f) * -2.0f;

		if (MOUSE.GetPressedButtons() & ioMouse::MOUSE_LEFT) {
			m_LastNormX = normX;
			m_LastNormY = normY;

			m_TrackPreMtx = m_ViewMtx;
			m_TrackPreMtx.d = -m_Axis.d;
			m_TrackPreMtx.d.Dot3x3(m_TrackPreMtx);  // translates points to "axis origin", prior to trackball rotation

			m_TrackPostTrans = m_ViewMtx.d-m_TrackPreMtx.d;  // this translates the points back after trackball rotation	
		}

		// Get the quat
		trk::TrackBall(m_Quat,m_LastNormX,m_LastNormY,normX,normY);

		Matrix34 m;
		trk::BuildRotMatrix(m,m_Quat);	

		m_ViewMtx.Dot(m_TrackPreMtx,m);					
		m_ViewMtx.d += m_TrackPostTrans;		
	}

# if __BANK
	if ((MOUSE.GetReleasedButtons() & ioMouse::MOUSE_RIGHT) && !hadMotion)
		PostMenu();
# endif

	if (!KEYBOARD.KeyDown(KEY_LCONTROL)) {
		m_LastX = MOUSE.GetX();
		m_LastY = MOUSE.GetY();
	}
	else {
		m_LastX = GRCDEVICE.GetWidth() >> 1;
		m_LastY = GRCDEVICE.GetHeight() >> 1;
	}
#else
	(void)dt;
#endif

	return moved;
}

void dcamTrackCam::Draw(int scrX, int scrY, float alpha) const {
	// show instructions on screen:
	scrY=DrawLine(scrX, scrY, "Hold Mouse Right or Left & Middle Buttons or Mouse Wheel: Zoom In/Out", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Mouse Middle: Translate In/Out", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Mouse Left: Trackball", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Mouse Right: Context Menu", Color32(1.f,1.f,1.f,alpha));
}

void dcamTrackCam::UpdateMatrix(float)
{
	if (m_POV)
		AxisToCamera();

	m_WorldMtx.FastInverse(m_ViewMtx);
}

void dcamTrackCam::AxisToCamera() {
	Matrix34 m;
	if (m.Inverse(m_ViewMtx))
		SetAxisOrigin(m.d);
}

void dcamTrackCam::SetAxisOrigin(const Vector3 & pos) {
	m_Axis.d = pos;
}

void dcamTrackCam::DrawAxis() const {
	grcWorldMtx(m_Axis);

	grcBegin(drawLines,6);
	grcColor3f(0.5f,0,0);
	grcVertex3f(0,0,0);
	grcVertex3f(1,0,0);
	grcColor3f(0,0.5f,0);
	grcVertex3f(0,0,0);
	grcVertex3f(0,1,0);
	grcColor3f(0,0,0.5f);
	grcVertex3f(0,0,0);
	grcVertex3f(0,0,1);
	grcEnd();

	grcWorldIdentity();
}

void dcamTrackCam::TopView(float dist) {
	m_ViewMtx.a.Set(1,0,0);
	m_ViewMtx.b.Set(0,0,1);
	m_ViewMtx.c.Set(0,-1,0);
	m_ViewMtx.d.Set(0,0,-dist);
}


#if __BANK
static void ToFrontCB(dcamTrackCam *_this)			{ _this->AxisToCamera(); }
static void ToTopCB(dcamTrackCam *_this)			{ _this->TopView(); }
static void TrackCamResetCB(dcamTrackCam *_this)	{ _this->Reset(); }

void dcamTrackCam::AddWidgets(bkBank &b) {
//	static const char *axis[] = {"X","Y","Z"};
	b.AddSlider("Trans Scale",&m_Scale,5.0f,1000.0f,5.0f);
	b.AddSlider("Wheel Scale",&m_WheelScale,0.0001f,1000.0f,0.1f);
	b.AddSlider("Axis Origin",&m_Axis.d,-1000.0f,1000.0f,1.0f);
	b.AddButton("Axis to Camera",datCallback(CFA1(ToFrontCB),this));
	b.AddButton("Top View",datCallback(CFA1(ToTopCB),this));
	b.AddButton("Reset",datCallback(CFA1(TrackCamResetCB),this));
	b.AddToggle("POV Mode",&m_POV);
}

void dcamTrackCam::PostMenu() {
	/*
#if __WIN32PC
	bkMenu p;
	p.AddItem("Fine",10010,m_Scale==10.0f);
	p.AddItem("Medium",10100,m_Scale==100.0f);
	p.AddItem("Coarse",11000,m_Scale==1000.0f);
	p.AddSeparator();
	p.AddItem("Axis to Camera",10);
	p.AddItem("Top View",20);
	p.AddItem("Reset",30);
	p.AddItem("POV Mode",40,m_POV);
	int result = p.Post();
	switch (result) {
	case 0:	// no selection.
		break;
	case 10: AxisToCamera(); break;
	case 20: TopView(); break;
	case 30: Reset(); break;
	case 40: m_POV = !m_POV; break;
	default:
		m_Scale = (float)(result - 10000);
		break;
	}
#endif
	*/
}

#endif
