// 
// devcam/mrknorthcam.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "mrknorthcam.h"
#include "input/pad.h"
#include "grprofile/drawmanager.h"

#if !__FINAL

using namespace rage;

dcamMrkNorthCam::dcamMrkNorthCam()
{

}

dcamMrkNorthCam::~dcamMrkNorthCam()
{

}

const char* dcamMrkNorthCam::GetName() const
{
	return "R*North Camera";
}

void dcamMrkNorthCam::DrawInfoText(float scrX, float scrY)
{
	static const char* texts[] = {
	 "Camera for R*North",
	 "Uses gamepad(#1) to move the camera and do actions.",
	 "",
	 "Controls  XBox 360 Controller",
	 "-----------------------------------",
	 "(B) = Detach/attach camera to player",
	 "(X) = Moves Forward",
	 "(A) = Moves Backward",
	 "(Directional Pad L) = Decrease FOV",
	 "(Directional Pad R) = Increase FOV",
	 "(Directional Pad Down) = Reset FOV",
	 "(Left Stick) = Move Camera - Up, Down, Left, and Right",
	 "(Right Stick) = Look - Up, Down, Left, and Right like FPS",
	 "(Y + Left Bumper) = Rotate Counter-clockwise",
	 "(Y + Right Bumper) = Rotate clockwise",
	 "(Right Trigger) = Drop Player",
	 "(Start + Left Bumper) = Widgets Menu"
	};

	int count = sizeof(texts)/sizeof(char*);
	int lineSize = 10;
	for(int i = 0; i < count; i++)
	{
		grcFont::GetCurrent().Drawf(scrX,scrY+(i*lineSize), texts[i]);
	}
}

bool dcamMrkNorthCam::Update(float dt)
{
	m_dt = dt;
	//on the second controller
	const float deadZone = 0.23f;	// MAGIC!
	Vector2 lStick(rage::ioAddDeadZone(ioPad::GetPad(0).GetNormLeftX(),deadZone), rage::ioAddDeadZone(ioPad::GetPad(0).GetNormLeftY(),deadZone));
	Vector2 rStick(rage::ioAddDeadZone(ioPad::GetPad(0).GetNormRightX(),deadZone), rage::ioAddDeadZone(ioPad::GetPad(0).GetNormRightY(),deadZone));

	if (lStick.IsNonZero())
	{
		HandleLStick(lStick * m_dt);
	}

	if (rStick.IsNonZero())
	{
		HandleRStick(rStick * m_dt);
	}

	HandleHeldButtons(ioPad::GetPad(0).GetButtons());
	HandleHeldDebugButtons(ioPad::GetPad(0).GetDebugButtons());
	HandleButtons(ioPad::GetPad(0).GetPressedButtons());
	HandleDebugButtons(ioPad::GetPad(0).GetPressedDebugButtons());

	return false;
}

void dcamMrkNorthCam::HandleHeldButtons(const u32 buttons)
{
	//(Y + Right Bumper) = Rotate clockwise
	if (buttons & ioPad::RUP && buttons & ioPad::R1)
	{
		m_WorldMtx.Rotate(m_WorldMtx.c,-(m_RollAmount * m_dt));//use forward
	}

	//(Y + Left Bumper) = Rotate Counter-clockwise
	if (buttons & ioPad::RUP && buttons & ioPad::L1)
	{
		m_WorldMtx.Rotate(m_WorldMtx.c,(m_RollAmount * m_dt));//use forward
	}

	//(X) = Moves Forward
	if (buttons & ioPad::RLEFT)
	{
		Vector3 temp = m_WorldMtx.c;//use forward
		temp.Scale(-(m_MoveAmount * m_dt));
		m_WorldMtx.Translate(temp);
	}

	//(A) = Moves Backward
	if (buttons & ioPad::RDOWN)
	{
		Vector3 temp = m_WorldMtx.c;//use forward
		temp.Scale((m_MoveAmount * m_dt));
		m_WorldMtx.Translate(temp);
	}
}

void dcamMrkNorthCam::HandleButtons(const u32 buttons)
{
	//(Right Trigger) = Drop Player
	if (buttons & ioPad::R2)
	{
		//Drop player callback
		if (m_DropPlayerCallback.IsBound())
		{
			m_DropPlayerCallback();
		}
	}

	//(Directional Pad L) = Decrease FOV
	if (buttons & ioPad::LLEFT)
	{
		//Decrease FOV callback
		if (m_DecreaseFOVCallback.IsBound())
		{
			m_DecreaseFOVCallback(1.0f);
		}
	}

	//(Directional Pad R) = Increase FOV
	if (buttons & ioPad::LRIGHT)
	{
		//Increase FOV callback
		if (m_IncreaseFOVCallback.IsBound())
		{
			m_IncreaseFOVCallback(1.0f);
		}
	}

	//(Directional Pad Down) = Reset FOV
	if (buttons & ioPad::LDOWN)
	{
		//Reset FOV callback
		if (m_ResetFOVCallback.IsBound())
		{
			m_ResetFOVCallback();
		}
	}
}

void dcamMrkNorthCam::HandleDebugButtons(const u32 /*dButtons*/)
{

}

void dcamMrkNorthCam::HandleLStick(const Vector2& vectorXY)
{
	Vector3 worldup(0,1,0); //Y up world
	if (GetUseZUp())
	{
		//Z up world
		worldup.Set(0,0,1);
	}

	Vector3 temp = worldup; //use world up vector
	temp.Scale((m_MoveAmount*-(vectorXY.y)));
	m_WorldMtx.Translate(temp);

	Vector3 up = m_WorldMtx.GetLocalUp();
	Vector3 right;
	right.Cross(up, m_WorldMtx.c);//use forward
	right.Normalize();
	right.Scale((m_MoveAmount*vectorXY.x));
	m_WorldMtx.Translate(right);
}

void dcamMrkNorthCam::HandleTriggers(const float , const float )
{
}


#endif //#if !__FINAL
