//
// devcam/mayacam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_MAYACAM_H
#define DEVCAM_MAYACAM_H

#include "vector/matrix34.h"
#include "input/mapper.h"
#include "devcam.h"

namespace rage {

class spdSphere;

// PURPOSE
//  Mimics the Maya cameras -- they may be convoluted, but everyone knows how
//  to use them.
//
// NOTES
//  * Using a gamepad *
//  <TABLE>
//      Button                   Action
//      -----------------------  ----------------------------------------------------
//      Rdown + left stick       Pan camera
//      RRight + left stick      Zoom camera
//      RLeft + left stick       Rotate camera
//      Left stick in (L3)       Cycle camera change rates
//      SELECT                   Reset the camera to default position & orientation
//  </TABLE>
//
//  * Using a Mouse *
//  <TABLE>
//      Key                                  Action
//      -----------------------------------  ----------------------------------------------------
//      Left mouse btn + mouse movement      Rotate camera
//      Middle mouse btn + mouse movement    Ran camera
//      Right mouse btn + mouse movement     Zoom camera
//      SPACE BAR                            Reset the camera
//  </TABLE>
// <FLAG Component>
//
class dcamMayaCam : public dcamCam {
public:
	// PURPOSE: default constructor
	dcamMayaCam();
	// PURPOSE: destructor
	~dcamMayaCam();
	
	const char *GetName() const;
	void Init(const Vector3 &pos, const Vector3 &lookAtPos);
	bool Update(float dt);
	// <COMBINE dcamCam::Draw>
	void Draw(int scrX,int scrY,float alpha) const;
	void Reset();

	// PURPOSE
	//	Get the focus distance
	//
    float GetFocusDistance();

	// PURPOSE
	//	Set the default speed of the pans/zoom.
	// PARAMS
	//	s - the default speed in units per second 
	//
	void SetDefaultSpeed(float s);

	// PURPOSE
	//	Set the default position of the camera
	// PARAMS
	//	pos - the default position
	//
	void SetDefaultPosition(const Matrix34 &pos) {m_ResetMtx = pos;}

	// PURPOSE
	//	Set the default speed of the rotations.
	// PARAMS
	//	s - the default speed in units per second 
	//
	void SetRotateSpeed(float s);


	// PURPOSE
	//	Set the default speed of the pan.
	// PARAMS
	//	s - the default speed in units per second 
	//
	void SetPanSpeed(float s);

	// PURPOSE
	//	Set the default max zoom distance
	void SetMaxZoom(float s);

	//
	// PURPOSE
	//	Set the distance of the focus point to the camera
	// PARAMS
	//	focusPt - the world space point to focus on
	//
	void SetFocusPoint( const Vector3 &focusPt );

	// 
	// PURPOSE: accessor the current matrix of this camera
	// RETURNS: a reference to the world matrix
	const Matrix34 &GetWorldMtx() const;

	virtual void SetWorldMtx(const Matrix34 &mtx);

	virtual void Frame(const spdSphere& frameSphere);
	// 
	//
	// PURPOSE
	//	Control the active state (whether or not to accept input)
	// PARAMS
	//	active - true if camera should accept input, false if not
	//
	void SetActive(bool active);


	// PURPOSE
	//	Change to Alternate zoom: pan speed adjusted based on zoom level and zoom is reversed
	// PARAMS
	//	active - true if alternate zoom, false if not
	void SetAltZoom(bool mode) {m_AltZoomMode = mode;}

	// PURPOSE: accessor to check if camera accepts input
	// RETURNS: true if camera accepts input, false if not
	bool GetActive() const;

#if __BANK
	void AddWidgets(class bkBank &bk);
#endif

private:
	bool UpdateMouse(float dt);
	
	bool UpdatePad(float dt);

	// Process pan/zoom/rotate
	bool ProcessInputs(bool pan, bool zoom, bool rotate, float x, float y, float z, float dt);
	void DoPan(float x, float y, float dt);
	void DoZoom(float x, float y, float dt);
	void DoRotate(float x, float y, float dt);

	Matrix34	m_ResetMtx;
	Matrix34	m_WorldMtx;
	Vector3		m_ResetFocusPoint;
	Vector3		m_FocusPoint;
	float		m_Speed;
	float		m_PanSpeed;
	float		m_RotateSpeed;
	float		m_FocusDist;
	float		m_SpeedScalar;
	float		m_MouseWheelScale;
	int			m_LastMouseX;
	int			m_LastMouseY;

	ioMapper	m_Mapper;
	ioValue		m_Pan;
	ioValue		m_Zoom;
	ioValue		m_Rotate;
	ioValue		m_Reset;
	ioValue		m_Xaxis;
	ioValue		m_Yaxis;
	ioValue		m_SpeedToggle;
	ioValue		m_ResetRequest;
	ioValue		m_RequiredKey;
	ioValue     m_MoveSlowly;
	ioValue     m_MoveQuickly;

	bool		m_DidZoom;
	bool		m_DidPan;
	bool		m_DidRotate;
	bool		m_Active;
	bool	    m_AltZoomMode;
};


//=============================================================================
// Implementations

inline const char* dcamMayaCam::GetName() const 
{
	return "Maya Cam";
}

inline void dcamMayaCam::SetDefaultSpeed(float s) 
{ 
	m_Speed = s; 
}

inline void dcamMayaCam::SetRotateSpeed(float s) 
{ 
	m_RotateSpeed = s; 
}

inline void dcamMayaCam::SetPanSpeed(float s) 
{ 
	m_PanSpeed = s;
}

inline const Matrix34& dcamMayaCam::GetWorldMtx() const
{ 
	return m_WorldMtx; 
}

inline void dcamMayaCam::SetWorldMtx(const Matrix34 &mtx)
{
	m_WorldMtx.Set(mtx);
}

inline void dcamMayaCam::SetActive(bool active)				
{ 
	m_Active = active; 
}

inline bool dcamMayaCam::GetActive() const		
{ 
	return m_Active; 
}

}	// namespace rage

#endif // DEVCAM_MAYACAM_H
