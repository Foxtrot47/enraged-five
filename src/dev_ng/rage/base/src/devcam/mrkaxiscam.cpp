// 
// devcam/mrkaxiscam.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "mrkaxiscam.h"
#include "input/pad.h"

#if !__FINAL
using namespace rage;

dcamMrkAxisCam::dcamMrkAxisCam()
{

}

dcamMrkAxisCam::~dcamMrkAxisCam()
{

}

const char* dcamMrkAxisCam::GetName() const
{
	return "R*NY Marketing Axis Camera";
}

void dcamMrkAxisCam::DrawInfoText(float scrX, float scrY)
{
	static const char* texts[] = {
	"Locked Axis camera for R*NY marketing.",
	"Uses gamepad(#2) to move the camera and do actions.",
	"",
	"Controls  XBox 360 Controller",
	"-----------------------------------",
	"(Left Stick) = move along the X and Z axis at the current Y height.",
	"(X) = Moves along Y Up",
	"(A) = Moves along Y Down",
	"",
	"Inherited Controls XBox 360 Controller",
	 "-----------------------------------",	
	 "(Right Stick) = Forward, Back, Left, and Right like a 1st person shooter",
	 "(L3) = Pause time cycle",
	 "(Start Button) = Pause Game",
	 "(Back Button) = Switch HUD on and off",
	 "(Start + Y) = increase move speed by 0.001",
	 "(Start + A) = slow move speed by 0.001",
	 "(Start + X) = increase rot speed by 0.001",
	 "(Start + R3) = slow rot speed by 0.001",
	 "(Directional Pad R) = Advance Time",
	 "(Directional Pad L) = Weather Type",
	 "(Directional Pad Down) = Drop Player",
	 "(Directional Pad Up) = Step forward one frame (while paused)",
	 "(Left Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Right Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Left Trigger) = Decrease FOV",
	 "(Right Trigger) = Increase FOV",
	 "(Start + Left Bumper) = Widgets Menu",
	 "(Start + B) Toggles between Detached Cam and In-Game Cam",
	 "(Start + Right Bumper) cycles through all Cam modes."
	};

	int count = sizeof(texts)/sizeof(char*);
	int lineSize = 10;
	for(int i = 0; i < count; i++)
	{
		grcFont::GetCurrent().Drawf(scrX,scrY+(i*lineSize), texts[i]);
	}
}

void dcamMrkAxisCam::HandleLStick(const Vector2& vectorXY)
{
	Vector3 temp(1.0f,0.0f,0.0f);
	temp.Scale((m_MoveAmount*vectorXY.x*m_MoveSpeedAugment));
	m_WorldMtx.Translate(temp);

	if (GetUseZUp())
	{
		temp.Set(0.0f,1.0f,0.0f);
		temp.Scale((m_MoveAmount*vectorXY.y*m_MoveSpeedAugment));
		m_WorldMtx.Translate(temp);
	}
	else
	{
		temp.Set(0.0f,0.0f,1.0f);
		temp.Scale((m_MoveAmount*vectorXY.y*m_MoveSpeedAugment));
		m_WorldMtx.Translate(temp);
	}
}

void dcamMrkAxisCam::HandleHeldButtons(const u32 buttons)
{
	Vector3 temp(0.0f,1.0f,0.0f);
	if (GetUseZUp())
	{
		temp.Set(0.0f,0.0f,1.0f);
	}

	//(X) = Moves along Y Up
	if (buttons & ioPad::RLEFT)
	{
		temp.Scale((m_MoveAmount*m_dt*m_MoveSpeedAugment));
		m_WorldMtx.Translate(temp);
		return; //so base does not process this
	}

	//(A) = Moves along Y Down
	if (buttons & ioPad::RDOWN)
	{
		temp.Scale(-(m_MoveAmount*m_dt*m_MoveSpeedAugment));
		m_WorldMtx.Translate(temp);
		return; //so base does not process this
	}	

	dcamMrkCamBase::HandleHeldButtons(buttons);
}

#endif //#if !__FINAL