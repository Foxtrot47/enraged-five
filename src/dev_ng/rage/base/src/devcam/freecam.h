//
// devcam/freecam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_FREECAM_H
#define DEVCAM_FREECAM_H

#include "vector/matrix44.h"
#include "devcam.h"

namespace rage {

class spdSphere;

//
// PURPOSE
//	this is a fly-through camera based on Descent-like keys.  
//
//	<TABLE>
//      Key                 Action
//      ------------------  ----------------------
//      KEY_NUMPADENTER     strafe right
//      KEY_NUMPAD0         strafe left
//      KEY_NUMPAD7         up
//      KEY_NUMPAD1         down
//      KEY_NUMPAD2         forward
//      KEY_NUMPAD8         back
//      KEY_E               look up
//      KEY_D               look down
//      KEY_NUMPAD4         look left
//      KEY_NUMPAD6         look right
//      KEY_S               rotate around z (+)
//      KEY_F               rotate around z (-)
//	</TABLE>
//
// NOTES
//	This camera might interfere with your program's keystrokes.
// <FLAG Component>
//
class dcamFreeCam : public dcamCam 
{
public:
	// PURPOSE: default constructor
	dcamFreeCam();

	const char* GetName() const;

	bool Update(float dt);
	void Init(const Vector3 &camPos, const Vector3 &lookAtPos);
	void Reset();


	//
	// PURPOSE
	//	sets the world space location of the camera
	// PARAMS
	//	loc - the world space position
	//
	void SetLocation(const Vector3 &loc);
	//
	// PURPOSE
	//	adds an offset to the world space location of the camera
	// PARAMS
	//	loc - the offset 
	//
	void OffsetLocation(const Vector3 &loc);
	// PURPOSE: sets the world space matrix to the identity matrix
	void Identity();
	// PURPOSE: sets the 3x3 part of the world space matrix to the identity matrix
	void Identity3x3();
	
	//
	// PURPOSE
	//	rotates the camera's matrix by an euler rotation in XYZ order
	// PARAMS
	//	rotate - the euler rotation to apply (in XYZ order)
	//
	void Rotate(const Vector3 &rotate);

	// PURPOSE: accessor for the camera's view matrix (inverse of the world matrix)
	// RETURNS: a reference to the camera's view matrix
	const Matrix44 &GetViewMtx();

	const Matrix34 &GetWorldMtx() const;

	void SetWorldMtx(const Matrix34 &mtx);

	virtual void Frame(const spdSphere& frameSphere);

	//
	// PURPOSE
	//	sets the translation and rotation speeds
	// PARAMS
	//	transSpeed - the translational speed
	//	rotSpeed - the rotational speed
	//
	static void SetSpeed(float transSpeed,float rotSpeed);

	void Draw(int scrX, int scrY, float alpha) const;

private:
	void InitInternal();

	Matrix44 m_ViewMtx;
	Matrix34 m_WorldMtx;

	Vector3 m_InitPos;
	Vector3 m_LookAtPos;
};

inline dcamFreeCam::dcamFreeCam() 
{ 
	InitInternal(); 
}

inline const char* dcamFreeCam::GetName() const 
{
	return "Free Cam";
}

inline void dcamFreeCam::SetLocation(const Vector3 &loc)	
{ 
	m_WorldMtx.d = loc; 
}

inline void dcamFreeCam::OffsetLocation(const Vector3 &loc)
{ 
	m_WorldMtx.d.Add(loc); 
}

inline void dcamFreeCam::Identity()	
{ 
	m_WorldMtx.Identity(); 
}

inline void dcamFreeCam::Identity3x3()						
{ 
	m_WorldMtx.Identity3x3(); 
}

inline const Matrix44& dcamFreeCam::GetViewMtx()
{ 
	return m_ViewMtx; 
}

inline const Matrix34& dcamFreeCam::GetWorldMtx() const
{
	return m_WorldMtx; 
}

inline void dcamFreeCam::SetWorldMtx(const Matrix34 &mtx)
{
	m_WorldMtx.Set(mtx);

	Matrix34 temp;
	temp.FastInverse(m_WorldMtx);
	Convert(m_ViewMtx,temp);
}




// PURPOSE:
//	Similar to the dcamFreeCam, but different key configuration 
//  and also deals with camera rotate/tilt properly.
//	
// NOTES:
//	* Windows *
// <TABLE>
//		Key                              Action
//		-------------------------------  ---------------------
//		NUMPAD arrows or regular arrows  rotate/move
//		PGUP/PGDN                        tilt (stops at vertical)
//		A/Z                              raise/lower
//		</>                              strafe
//		NUMPAD 5                         set horizontal view
//		HOME                             point north
//		hold shift                       moves faster
// </TABLE>
//
//  * Controller *
// <TABLE>
//		Key                              Action
//		-------------------------------  ---------------------
//		rDown/rUp                        move forward/back
//		R1/R2                            tilt
//	    L1/L2                            raise/lower
//	    rLeft/rRight                     strafe
//	    hole down L3                     moves faster
// </TABLE>
//	This camera might interfere with your program's keystrokes.
// <FLAG Component>
//
class dcamFreeCam2 : public dcamCam  
{
public:
	// PURPOSE: default constructor
	dcamFreeCam2();

	const Matrix34 &GetWorldMtx() const;
	bool Update(float dt);
	void Draw(int scrX, int scrY, float alpha) const;

private:
	float m_Orientation;
	float m_Elevation;
	Matrix34 m_WorldMtx;
};

inline const Matrix34& dcamFreeCam2::GetWorldMtx() const		
{	
	return m_WorldMtx;	
}

}	// namespace rage

#endif // DEVCAM_FREECAM_H
