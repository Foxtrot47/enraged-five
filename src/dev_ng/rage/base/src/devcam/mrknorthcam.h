// 
// devcam/mrknorthcam.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEVCAM_MRKNORTHCAM_H 
#define DEVCAM_MRKNORTHCAM_H 

#if !__FINAL
#include "mrkcambase.h"

namespace rage {

//
// PURPOSE:
//	Camera for R*North. Uses gamepad(#1) to move the camera and do actions.
// NOTES:
//  <TABLE>
//		If changes are made please update the GetInfoText function
//      Key                                          Action
//      -----------------------------------          ----------------------------------------------------
//		(B)											 Detach/attach camera to player
//		(X)											 Moves Forward
//		(A)											 Moves Backward
//		(Directional Pad L)							 Decrease FOV
//		(Directional Pad R)							 Increase FOV
//		(Directional Pad Down)						 Reset FOV
//		(Left Stick)								 Move Camera - Up, Down, Left, and Right
//		(Right Stick)								 Look - Up, Down, Left, and Right like FPS
//		(Y + Left Bumper)							 Rotate Counter-clockwise
//		(Y + Right Bumper)							 Rotate clockwise
//		(Right Trigger)								 Drop Player
//		(Start + Left Bumper)						 Widgets Menu
//  </TABLE>
//<FLAG Component>
//
class dcamMrkNorthCam : public dcamMrkCamBase {
public:
	// PURPOSE: default constructor
	dcamMrkNorthCam();
	virtual ~dcamMrkNorthCam();

	virtual const char*	GetName() const;
	virtual bool Update(float dt);

protected:

	//derived interface
	virtual void DrawInfoText(float scrX, float scrY);

	virtual void HandleButtons(const u32 buttons);
	virtual void HandleDebugButtons(const u32 dButtons);
	virtual void HandleHeldButtons(const u32 buttons);
	virtual void HandleLStick(const Vector2& vectorXY);
	virtual void HandleTriggers(const float left, const float right);

private:

};
} // namespace rage

#endif // #if !__FINAL
#endif // DEVCAM_MRKNORTHCAM_H 
