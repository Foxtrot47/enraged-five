// 
// devcam/lhcam.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEVCAM_LHCAM_H
#define DEVCAM_LHCAM_H

#include "devcam.h"

#include "input/mapper.h"
#include "vector/matrix44.h"


namespace rage {

class bkBank;
class spdSphere;

// PURPOSE
//  This is a camera based on Vienna's LH viewers.
//	This camera functions similar to the polar cam, but maps to the controller differently, and adds mouse support. 
//
// NOTES
//  * Using a Gamepad *
//  <TABLE>
//      Button                   Action
//      -----------------------  ----------------------------------------------------
//      right stick              (left/right) Orbit, (up/down) Incline
//      left stick               (up/down) Zoom In/Out
//      R1 & R2                  Move Camera Up/Down
//  </TABLE>
//
//  * Using a Mouse *
//  <TABLE>
//      Key                                        Action
//      ----------------------------------------   ----------------------------------------------------
//      ALT + Left mouse btn + mouse movement      (left/right) Orbit, (up/down) Incline
//      ALT + Middle mouse btn + mouse movement    (up/down) Move Camera Up/Down
//      ALT + Right mouse btn + mouse movement     (up/down) Zoom In/Out
//  </TABLE>
// <FLAG Component>
//
class dcamLhCam : public dcamCam {
public:
	// PURPOSE: default constructor
	dcamLhCam();

	virtual void	Init(const Vector3 &cameraPos, const Vector3 &lookAtPos);
	virtual bool	Update(float dt);
	virtual void	Draw(int scrX, int scrY, float alpha) const;
	virtual void	Reset();
	const char*		GetName() const;
	virtual const	Matrix34 &GetWorldMtx() const;
	virtual void	SetWorldMtx(const Matrix34 &mtx);
	virtual void	Frame(const spdSphere& frameSphere);

#if __BANK
	//<COMBINE dcamCam::AddWidgets>
	virtual void AddWidgets(bkBank &bk);
#endif

	//
	// PURPOSE
	//	Initializes polar cam position and orientation.
	// PARAMS
	//	dist - distance from origin
	//	azm - azimuth
	//	inc - incline
	//
	void Init(float dist = 10.0f,float azm = 0.0f,float inc = 0.1f,Vector3::Vector3Param Offset = ORIGIN);

	//
	// PURPOSE
	//	sets the position for the camera to follow
	// PARAMS
	//	v - a reference to the position that is followed
	//
	void SetFollow(const Vector3 &v);
    //
    // PURPOSE
    //  turns off the camera follow
    //
    void StopFollow();

	//
	// PURPOSE
	//	sets the translational offset
	// PARAMS
	//	v - the translational offset
	//
	void SetOffset(const Vector3 &v);	

	//
	// PURPOSE
	//	sets if we should add the offset to the follow position
	// PARAMS
	//	active - true if we add offset, false if not
	// SEE ALSO
	//	SetFollow
	//
	void SetFollowOffset(bool active=true);	
	//
	// PURPOSE
	//	sets the orientation that the camera should follow
	// PARAMS
	//	v - a pointer to the orientation
	//
	void SetOrient(const Vector3 *v);
	//
	// PURPOSE
	//	sets the linear rate that the camera should follow
	// PARAMS
	//	lr - the linear rate (in meters per seconds)
	//
	void SetLinearRate(float lr);
	//
	// PURPOSE
	//	sets the angular rate that the camera should follow
	// PARAMS
	//	ar - the angular rate (in radians per seconds)
	//
	void SetAngularRate(float ar);
	//
	// PURPOSE
	//	sets the orient rate that the camera should follow
	//  if <c>SetOrient()</c> has been called
	// PARAMS
	//	ora - the orient rate (in radians per seconds)
	//
	void SetOrientRate(float ora);			
	//
	// PURPOSE
	//	sets if the camera should look at input to update 
	// PARAMS
	//	b - true if camera should use input, false if not
	//
	void SetEnableInput(bool b);			
	//
	// PURPOSE
	//	sets distance from the origin of the camera
	// PARAMS
	//	dist - the distance from the origin
	//
	void SetDistance(float dist);			
	//
	// PURPOSE
	//	sets the azimuth of the camera
	// PARAMS
	//  azm - the azimuth value to set
	//
	void SetAzimuth(float azm);			
	//
	// PURPOSE
	//	sets the incline of the camera
	// PARAMS
	//	inc - the incline value to set
	//
	void SetIncline(float inc);				

	// PURPOSE: accessor for input enable state
	// RETURNS: true if input is enabled, false if not
	bool GetEnableInput() const;				
	// PURPOSE: accessor for the camera's distance from the origin
	// RETURNS: the camera's distance from the origin
	float GetDistance() const;				
	// PURPOSE: accessor for the camera's azimuth
	// RETURNS: the camera's azimuth
	float GetAzimuth() const;				
	// PURPOSE: accessor for the camera's incline
	// RETURNS: the camera's incline
	float GetIncline() const;				
	// PURPOSE: accessor for the camera's translational offset
	// RETURNS: the translational offset
	const Vector3 &GetOffset() const;

	// PURPOSE: accessor for the camera's view matrix (inverse of the world matrix)
	// RETURNS: a reference to the camera's view matrix
	const Matrix44 &GetViewMtx() const;

private:
	bool	UpdateInput(float dt);
	void	UpdateMatrix();
	void	PolarView(float dist, float azm, float inc, Matrix34 &mtx);

	bool	m_EnableInput;
	float	m_Distance,m_Azimuth,m_OrientAzimuth,m_Incline;
	float	m_LinearRate,m_AngularRate,m_OrientRate;
	Vector3 m_Offset;
	const	Vector3 *m_Follow;
	bool	m_FollowOffset;
	const	Vector3 *m_Orient;

	// Initial values - saved off to be restored on a Reset().
	float m_InitDistance, m_InitAzimuth, m_InitIncline;
	Vector3 m_InitOffset;
	
	Matrix44 m_ViewMtx;
	Matrix34 m_WorldMtx;

	ioMapper m_Mapper;

	ioValue m_Shift, m_Control, m_Alt;
	ioValue m_PadFaster, m_PadSlower;
	ioValue m_ioAzimuth;
	ioValue m_ioIncline;
	ioValue m_ioZoom;

	ioValue m_Up, m_Down;

	int	m_LastMouseX;
	int	m_LastMouseY;
};


inline dcamLhCam::dcamLhCam() {
	Init(); 
}

inline const char* dcamLhCam::GetName() const {
	return "LH Cam";
}

inline void dcamLhCam::SetFollow(const Vector3 &v) {
	m_Follow=&v;
}

inline void dcamLhCam::StopFollow()
{
    m_Follow = 0;
}

inline void dcamLhCam::SetOffset(const Vector3 &v) {
	m_Offset=v;
}

inline void dcamLhCam::SetFollowOffset(bool active)	{
	m_FollowOffset=active;
}

inline void dcamLhCam::SetOrient(const Vector3 *v) {
	m_Orient=v;
}

inline void dcamLhCam::SetLinearRate(float lr) {
	m_LinearRate=lr;
}

inline void dcamLhCam::SetAngularRate(float ar) {
	m_AngularRate=ar;
}

inline void dcamLhCam::SetOrientRate(float ora) {
	m_OrientRate=ora;
}

inline void dcamLhCam::SetEnableInput(bool b) {
	m_EnableInput=b;
}

inline void dcamLhCam::SetDistance(float dist) {
	m_Distance=dist;
}

inline void dcamLhCam::SetAzimuth(float azm)	{
	m_Azimuth=azm;
}

inline void dcamLhCam::SetIncline(float inc) {
	m_Incline=inc;
}

inline bool dcamLhCam::GetEnableInput() const {
	return m_EnableInput;
}

inline float dcamLhCam::GetDistance() const {
	return m_Distance;
}

inline float dcamLhCam::GetAzimuth() const {
	return m_Azimuth;
}

inline float dcamLhCam::GetIncline() const {
	return m_Incline;
}

inline const Vector3 &dcamLhCam::GetOffset() const {
	return m_Offset;
}

inline const Matrix44 &dcamLhCam::GetViewMtx() const {
	return m_ViewMtx;
}

inline const Matrix34 &dcamLhCam::GetWorldMtx() const {
	return m_WorldMtx;
}

inline void dcamLhCam::SetWorldMtx(const Matrix34 &mtx) {
	m_WorldMtx.Set(mtx);

	m_FollowOffset = false;
	if (m_Follow)
	{
		m_WorldMtx.d.Subtract(*m_Follow);
	}

	m_OrientAzimuth = 0.0f;
	m_Distance = m_InitDistance;

	m_Offset.AddScaled(mtx.d,mtx.c,-m_Distance);

	//Calculate the azimuth
	m_Azimuth = AcosfSafe(mtx.a.x);

	if(m_UseZUp)
	{
		if (mtx.a.y < 0.0f)
			m_Azimuth = -m_Azimuth;
	}
	else
	{
		if (mtx.a.z > 0.0f)
			m_Azimuth = -m_Azimuth;
	}
	
	//Calculate the incline
	if(m_UseZUp)
	{
		m_Incline = -AcosfSafe(mtx.c.z);
	}
	else
	{
		m_Incline = -AcosfSafe(mtx.b.y);

		if (mtx.c.y > 0.0f)
			m_Incline = -m_Incline;
	}
}

}	// namespace rage

#endif //DEVCAM_LHCAM_H
