//
// devcam/quakecam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "quakecam.h"

#include "input/keys.h"
#include "input/mouse.h"
#include "input/pad.h"
#include "spatialdata/sphere.h"
#include "string/string.h"
#include "system/timemgr.h"

using namespace rage;

dcamQuakeCam::dcamQuakeCam()
{
	m_ResetMtx.Identity();
	m_WorldMtx.Identity();
	
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::SELECT, m_ResetRequest);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SPACE, m_ResetRequest);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_W, m_Forward);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_S, m_Reverse);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_A, m_StrafeLeft);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_D, m_StrafeRight);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SHIFT, m_MoveSlowly);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_MoveQuickly);

	m_Active = true;

	m_LastX = (float)ioMouse::GetX();
	m_LastY = (float)ioMouse::GetY();

	Reset();
}

dcamQuakeCam::~dcamQuakeCam()
{
}

void dcamQuakeCam::Init(const Vector3 &pos, const Vector3 &lookAtPos)
{
	m_ResetMtx.Identity();
	m_ResetMtx.d.Set(pos);
	m_ResetMtx.LookAt(lookAtPos, m_vUp);
	Reset();
}

bool dcamQuakeCam::Update(float dt)
{
	if ( !m_Active )
		return false;
		
	dt *= m_SpeedScalar;
	m_Mapper.Update();
	
	bool moved = UpdateMouse(dt);
	UpdatePad(dt);

	return moved;
}

void dcamQuakeCam::Frame(const spdSphere& frameSphere)
{
	m_WorldMtx.d = VEC3V_TO_VECTOR3(frameSphere.GetCenter());
	m_WorldMtx.d.AddScaled(m_WorldMtx.c, frameSphere.GetRadius().Getf()*2.0f);
}

void dcamQuakeCam::Draw(int  scrX, int scrY, float alpha) const
{
	if ( !m_Active )
		return;

	// int xOffset = 100;
	
	const char *info = "Hold LMB to look\naround. \nWASD to move.\nSpace to Reset";

	if ( info )
		scrY=DrawLine(scrX, scrY, info, Color32(1.f, 1.f, 1.f, alpha));
}


void dcamQuakeCam::Reset()
{
	m_WorldMtx.Set(m_ResetMtx);
	m_SpeedScalar = 10.f;
	m_RotateScalar = 0.25f;
}

#if __BANK
#include "bank/bank.h"
void dcamQuakeCam::AddWidgets(bkBank &bk)
{
	bk.AddSlider("RotateSpeed", &m_RotateScalar, 0.1f, 10000.f, 0.1f);
	bk.AddSlider("MoveSpeed", &m_SpeedScalar, 0.1f, 10000.f, 0.1f);
}
#endif

bool dcamQuakeCam::UpdateMouse(float dt)
{
	bool rotate = (ioMouse::GetButtons() & ioMouse::MOUSE_LEFT) != 0;
	
	float dx = (float) (ioMouse::GetX() - m_LastX);
	float dy = (float) (ioMouse::GetY() - m_LastY);

	bool moved = false;

	if (dx != 0.0f || dy != 0.0f)
	{
		moved = true;
	}

	m_LastX = (float)ioMouse::GetX();
	m_LastY = (float)ioMouse::GetY();

	float x = dx*dt;
	float y = dy*dt;

	if(rotate)
	{
		if(m_UseZUp)
		{
			m_WorldMtx.Rotate( M34_IDENTITY.c, -x * m_RotateScalar * dt);
			m_WorldMtx.Rotate( m_WorldMtx.a, -y * m_RotateScalar * dt);
		}
		else
		{
			m_WorldMtx.Rotate( M34_IDENTITY.b, -x * m_RotateScalar * dt);
			m_WorldMtx.Rotate( m_WorldMtx.a, -y * m_RotateScalar * dt);
		}
	}

	return moved;
}

void dcamQuakeCam::UpdatePad(float dt)
{
	const float SPEED_MOD_FACTOR = 10.0f;
	float speedScalar = m_SpeedScalar;
	if(m_MoveSlowly.IsDown())
	{
		speedScalar /= SPEED_MOD_FACTOR;
	}
	if(m_MoveQuickly.IsDown())
	{
		speedScalar *= SPEED_MOD_FACTOR;
	}

	if(m_Forward.IsDown())
	{
		m_WorldMtx.d.AddScaled(m_WorldMtx.c,-speedScalar*dt);
	}
	if(m_Reverse.IsDown())
	{
		m_WorldMtx.d.AddScaled(m_WorldMtx.c,speedScalar*dt);
	}
	if(m_StrafeLeft.IsDown())
	{
		m_WorldMtx.d.AddScaled(m_WorldMtx.a,-speedScalar*dt);
	}
	if(m_StrafeRight.IsDown())
	{
		m_WorldMtx.d.AddScaled(m_WorldMtx.a,speedScalar*dt);
	}

	if(m_ResetRequest.IsDown())
	{
		Reset();
	}
}
