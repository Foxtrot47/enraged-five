//
// devcam/quakecam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_QUAKECAM_H
#define DEVCAM_QUAKECAM_H

#include "vector/matrix34.h"
#include "input/mapper.h"
#include "devcam.h"

namespace rage {

class spdSphere;

// PURPOSE
/*
 *	This Camera is set up to behave just like any old first person shooter
 */

class dcamQuakeCam : public dcamCam {
public:
	// PURPOSE: default constructor
	dcamQuakeCam();

	// PURPOSE: destructor
	~dcamQuakeCam();
	
	const char *GetName() const;
	void Init(const Vector3 &pos, const Vector3 &lookAtPos);
	bool Update(float dt);
	// <COMBINE dcamCam::Draw>
	void Draw(int scrX, int scrY, float alpha) const;
	void Reset();

	// 
	// PURPOSE: accessor the current matrix of this camera
	// RETURNS: a reference to the world matrix
	const Matrix34 &GetWorldMtx() const;

	void SetWorldMtx(const Matrix34 &mtx);

	virtual void Frame(const spdSphere& frameSphere);

	//
	// PURPOSE
	//	Control the active state (whether or not to accept input)
	// PARAMS
	//	active - true if camera should accept input, false if not
	//
	void SetActive(bool active);

	// PURPOSE: accessor to check if camera accepts input
	// RETURNS: true if camera accepts input, false if not
	bool GetActive() const;

#if __BANK
	void AddWidgets(class bkBank &bk);
#endif

private:
	bool UpdateMouse(float dt);
	void UpdatePad(float dt);

	Matrix34	m_ResetMtx;
	Matrix34	m_WorldMtx;
	float		m_SpeedScalar;
	float		m_RotateScalar;
	float		m_LastX;
	float		m_LastY;

	ioMapper	m_Mapper;
	ioValue		m_Rotate;
	ioValue     m_Forward;
	ioValue     m_Reverse;
	ioValue     m_StrafeLeft;
	ioValue     m_StrafeRight;
	ioValue		m_ResetRequest;
	ioValue     m_MoveSlowly;
	ioValue     m_MoveQuickly;

	bool		m_Active;
};


//=============================================================================
// Implementations

inline const char* dcamQuakeCam::GetName() const 
{
	return "Quake Cam";
}


inline const Matrix34& dcamQuakeCam::GetWorldMtx() const
{ 
	return m_WorldMtx; 
}

inline void dcamQuakeCam::SetWorldMtx(const Matrix34 &mtx)
{
	m_WorldMtx.Set(mtx);
}

inline void dcamQuakeCam::SetActive(bool active)				
{ 
	m_Active = active; 
}

inline bool dcamQuakeCam::GetActive() const		
{ 
	return m_Active; 
}

}	// namespace rage

#endif // DEVCAM_QUAKECAM_H
