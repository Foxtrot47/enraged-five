//
// devcam/mayacam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mayacam.h"

#include "input/input.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "input/pad.h"
#include "spatialdata/sphere.h"
#include "string/string.h"
#include "system/timemgr.h"

using namespace rage;

dcamMayaCam::dcamMayaCam()
{
	m_ResetMtx.Identity();
	m_WorldMtx.Identity();
	m_Speed = 4.f;
	m_PanSpeed = 5.f;
	m_RotateSpeed = 1.f;
	m_MouseWheelScale = 120.0f;
	m_ResetFocusPoint.Zero();

	m_LastMouseX = m_LastMouseY = 0;

	m_FocusPoint.Zero();
	m_ResetFocusPoint.Zero();

	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RLEFT, m_Rotate);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RRIGHT, m_Zoom);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RDOWN, m_Pan);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L3, m_SpeedToggle);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::SELECT, m_ResetRequest);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SHIFT, m_MoveSlowly);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_MoveQuickly);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SPACE, m_ResetRequest);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_RequiredKey);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LX, m_Xaxis);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LY, m_Yaxis);

	m_Active = true;
	m_AltZoomMode = false;

	Reset();
}

dcamMayaCam::~dcamMayaCam()
{
}

void dcamMayaCam::Init(const Vector3 &pos, const Vector3 &lookAtPos)
{
	m_ResetMtx.Identity();
	m_ResetMtx.d.Set(pos);
	m_ResetMtx.LookAt(lookAtPos, m_vUp);
	m_ResetFocusPoint.Set(lookAtPos);
	SetFocusPoint(lookAtPos);
	Reset();
}

void dcamMayaCam::Frame(const spdSphere& frameSphere)
{
	m_WorldMtx.d = VEC3V_TO_VECTOR3(frameSphere.GetCenter());
	m_WorldMtx.d.AddScaled(m_WorldMtx.c, frameSphere.GetRadius().Getf()*2.0f);

	SetFocusPoint(VEC3V_TO_VECTOR3(frameSphere.GetCenter()));
}

bool dcamMayaCam::Update(float dt)
{
	if ( !m_Active )
	{
		return false;
	}
		
	dt *= m_SpeedScalar;
	m_Mapper.Update();
	
	if ( UpdateMouse(dt) )
	{
		return true;
	}
	return UpdatePad(dt);
}


void dcamMayaCam::Draw(int  scrX, int scrY, float alpha) const
{
	if ( !m_Active )
		return;

	const char *panInfo =	" /|\\ \n<-+->\n \\|/ ";
	const char *zoomInfo =	"  /\\  \n /  \\ \n/_  _\\\n /  \\";
	const char *rotateInfo =" /-> \\ \n{     )\n \\ <-/";
	const char *inputInfo = ioInput::GetUseMouse()?"[] -> Rotate\nX -> Pan\nO -> Zoom" : "X -> Rotate\nA -> Pan\nB -> Zoom";

	const char *info = 0;

	// int xOffset = 100;
	if ( m_DidPan ) 
	{
		info = panInfo;
	}
	else if ( m_DidZoom ) 
	{
		info = zoomInfo;
	}
	else if ( m_DidRotate ) 
	{
		info = rotateInfo;
	}
	
	info = inputInfo;
	
	if (INPUT.GetUseKeyboard() && info && !m_RequiredKey.IsDown())
	{
		scrY=DrawLine(scrX, scrY, "For Mouse Input: Hold ALT key", Color32(1.f, 1.f, 1.f, alpha));
	}

	char speed[24];
	formatf(speed, 24, "x%d", (int) m_SpeedScalar);
	scrY=DrawLine(scrX, scrY, speed,Color32(1.f,1.f,1.f,alpha));

	if ( info )
	{
		scrY=DrawLine(scrX, scrY, info, Color32(1.f, 1.f, 1.f, alpha));
	}
}


void dcamMayaCam::Reset()
{
	m_WorldMtx.Set(m_ResetMtx);
	m_DidZoom = false;
	m_DidPan = false;
	m_DidRotate = false;
	m_FocusDist = 0.f;
	m_SpeedScalar = 1.f;
	m_FocusPoint.Set(m_ResetFocusPoint);
	m_FocusDist = m_FocusPoint.Dist(m_WorldMtx.d);
}

void dcamMayaCam::SetFocusPoint( const Vector3 &focusPt )
{
	m_FocusPoint.Set(focusPt);
	m_FocusDist = m_FocusPoint.Dist(m_WorldMtx.d);
}

float dcamMayaCam::GetFocusDistance()
{
    return m_FocusDist;
}


#if __BANK
#include "bank/bank.h"
void dcamMayaCam::AddWidgets(bkBank &bk)
{
	bk.AddSlider("Speed", &m_Speed, 0.1f, 10000.f, 0.1f);
	bk.AddSlider("Pan Speed", &m_PanSpeed, 0.1f, 10000.f, 0.1f);
	bk.AddSlider("Rotate Speed", &m_RotateSpeed, 0.1f, 10000.f, 0.1f);
}
#endif

bool dcamMayaCam::UpdateMouse(float dt)
{
	float x = (float) (ioMouse::GetX() - m_LastMouseX); 
	float y = (float) (ioMouse::GetY() - m_LastMouseY); 

	float movementScalar = 1.0;
	if(m_MoveSlowly.IsDown())
	{
		movementScalar /= 5.0f;
	}
	if(m_MoveQuickly.IsDown())
	{
		movementScalar *= 5.0f;
	}
	x *= movementScalar * 0.5f;
	y *= movementScalar * 0.5f;
	
	m_LastMouseX = ioMouse::GetX();
	m_LastMouseY = ioMouse::GetY();
	bool pan = (ioMouse::GetButtons() & ioMouse::MOUSE_MIDDLE) != 0;
	bool zoom = ((ioMouse::GetButtons() & ioMouse::MOUSE_RIGHT) || ((ioMouse::GetButtons() & ioMouse::MOUSE_LEFT) && (ioMouse::GetButtons() & ioMouse::MOUSE_MIDDLE))) != 0;

	bool rotate = (ioMouse::GetButtons() & ioMouse::MOUSE_LEFT) != 0;
	
	if ( pan || zoom ) 
	{
		x = -x;
		y = -y;
	}

	return ProcessInputs(pan, zoom, rotate, x, y, ioMouse::GetDZ()*m_MouseWheelScale*movementScalar, dt);
}

bool dcamMayaCam::UpdatePad(float dt)
{
	float x = m_Xaxis.GetNorm(0.15f);
	float y = m_Yaxis.GetNorm(0.15f);

	return ProcessInputs(m_Pan.IsDown(), m_Zoom.IsDown(), m_Rotate.IsDown(), x, y, 0.0f, dt);
}

bool dcamMayaCam::ProcessInputs(bool pan, bool zoom, bool rotate, float x, float y, float z, float dt)
{
	if (INPUT.GetUseKeyboard() && m_RequiredKey.IsUp())
	{
		return false;
	}

	bool zooming, panning, rotating;
	zooming=panning=rotating=false;
	if ( zoom ) 
	{
		// Zooming takes priority over rotating if both buttons are down.
		DoZoom(x, y, dt);
		//DoZoom(y, x, dt);
		zooming=true;
	}
	else if ( pan )
	{
		// Panning takes priority if more than one button is down.
		DoPan(x, y, dt);
		panning = true;
	}
	else if ( rotate ) 
	{
		DoRotate(x, y, dt);
		rotating=true;
	}
	else if (z!=0.0f)
	{
		DoZoom(z,0, dt);
		zooming=true;
	}

	// Handle rotate distance code (Maya is kinda tricky about this -- seems like zooming out determines
	//	the "focus point" for rotations)
	if ( m_DidZoom && zooming == false ) 
	{
		m_FocusDist = m_FocusPoint.Dist(m_WorldMtx.d);
	}

	if ( m_SpeedToggle.IsPressed() )
	{
		m_SpeedScalar = (m_SpeedScalar < 16.f) ? m_SpeedScalar * 2.f : 1.f;
	}

	if ( m_ResetRequest.IsDown() )
	{
		Reset();
	}

	m_DidZoom = zooming;
	m_DidPan = panning;
	m_DidRotate = rotating;

	return ( panning || zooming || rotating );
}

void dcamMayaCam::DoPan(float x, float y, float dt)
{
	//up pan speed based on zoom level 
	if(m_AltZoomMode)
	{
		m_WorldMtx.d.AddScaled(m_WorldMtx.a, x * (m_PanSpeed + m_WorldMtx.d.y/3) * dt);
		m_WorldMtx.d.AddScaled(m_WorldMtx.b, -y * (m_PanSpeed + m_WorldMtx.d.y/3) * dt);	
	}
	else
	{
		m_WorldMtx.d.AddScaled(m_WorldMtx.a, x * (m_PanSpeed) * dt);
		m_WorldMtx.d.AddScaled(m_WorldMtx.b, -y * (m_PanSpeed) * dt);	
	}
}

void dcamMayaCam::DoRotate(float x, float y, float dt) 
{
	const float cFocusLength = m_FocusDist;
	Vector3 target(m_WorldMtx.d);
	target.AddScaled(m_WorldMtx.c, -cFocusLength);

	if(m_UseZUp)
	{
		m_WorldMtx.Rotate( M34_IDENTITY.c, -x * m_RotateSpeed * dt );
		m_WorldMtx.RotateLocalX( -y * m_RotateSpeed * dt );
	}
	else
	{
		m_WorldMtx.Rotate( M34_IDENTITY.b, -x * m_RotateSpeed * dt );
		m_WorldMtx.RotateLocalX( -y * m_RotateSpeed * dt );
	}
	
	m_WorldMtx.d.Set(target);
	m_WorldMtx.d.AddScaled(m_WorldMtx.c, cFocusLength);
}

void dcamMayaCam::DoZoom(float x, float y, float dt) 
{
	float zoomAmount = x+y;
	float magnitude = zoomAmount * m_Speed * dt;

	if(m_AltZoomMode)
		zoomAmount = -zoomAmount;

	// See if we need to cache the point we started zooming out
	if ( !m_DidZoom ) 
	{
		if ( zoomAmount > 0.f )
			m_FocusPoint.Set(m_WorldMtx.d);
		else
			m_FocusPoint.AddScaled(m_WorldMtx.d, m_WorldMtx.c, -m_FocusDist);
	}
	else if(zoomAmount < 0.f && (m_FocusDist < 0.01f || m_FocusPoint.Dist(m_WorldMtx.d) < (0.25f * m_FocusDist))  ) 
	{
		m_FocusDist = magnitude;		
		m_FocusPoint.AddScaled(m_WorldMtx.d, m_WorldMtx.c, -m_FocusDist);
	}

	m_WorldMtx.d.AddScaled(m_WorldMtx.c, magnitude);
}
