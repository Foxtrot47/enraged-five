//
// devcam/flycam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "flycam.h"

#include "input/mouse.h"
#include "vector/matrix34.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

dcamFlyCam::dcamFlyCam()
{
	m_RadsPerTick=0.0002f;
	m_Speed=0.1f;

	m_InitPos.Set(0.f,0.f,0.f);
	m_LookAtPos.Set(0.f, 0.f, 1.f);

	Reset();
}

////////////////////////////////////////////////////////////////////////////////

void dcamFlyCam::Reset()
{
	m_Azimuth=0.0f;
	m_Incline=0.0f;

	m_WorldMtx.Identity();
	m_WorldMtx.d.Set(m_InitPos);
	m_WorldMtx.LookAt(m_LookAtPos, m_vUp);
	m_ViewMtx.Identity();
}

////////////////////////////////////////////////////////////////////////////////

void dcamFlyCam::Init(const Vector3 &camPos, const Vector3 &lookAtPos)
{
	m_InitPos.Set(camPos);
	m_LookAtPos.Set(lookAtPos);
	
	Reset();
}

bool dcamFlyCam::Update(float)
{
	float oldAzimuth = m_Azimuth;
	float oldIncline = m_Incline;

	m_Azimuth=-2.0f*PI*ioMouse::GetNormX();
	m_Incline=PI*ioMouse::GetNormY()-PI*0.5f;

	unsigned leftMouse = ioMouse::GetButtons()&ioMouse::MOUSE_LEFT;
	unsigned rightMouse = ioMouse::GetButtons()&ioMouse::MOUSE_RIGHT;

	// Don't update if the mouse isn't moving.  Allows us to keep 
	// a world matrix set from the outside, if we're just cycling 
	// through devcams.
	if (oldAzimuth!=m_Azimuth || oldIncline!=m_Incline || leftMouse || rightMouse)
	{
		m_WorldMtx.MakeRotateX(m_Incline);
		m_WorldMtx.RotateY(m_Azimuth);
	}

	if(leftMouse) m_WorldMtx.d.AddScaled(m_WorldMtx.c,-m_Speed);
	else if(rightMouse) m_WorldMtx.d.AddScaled(m_WorldMtx.c,m_Speed);

	Matrix34 temp;
	temp.FastInverse(m_WorldMtx);
	Convert(m_ViewMtx,temp);

	return oldAzimuth != m_Azimuth || oldIncline != m_Incline || ioMouse::GetButtons();
}

void dcamFlyCam::Draw(int scrX, int scrY, float alpha) const
{
	// show instructions on screen:
	scrY=DrawLine(scrX, scrY, "Mouse X: controls azimuth (left/right)", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Mouse Y: controls incline (up/down)", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Mouse Left Button: move forward", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Mouse Right Button: move backward", Color32(1.f,1.f,1.f,alpha));
}

////////////////////////////////////////////////////////////////////////////////

void dcamFlyCam::SetWorldMtx(const Matrix34& mtx)
{
	m_WorldMtx.Set(mtx);

	// Calculate these so that Update will know whether or not to overwrite
	// the world matrix we just set (if the mouse is moving).
	m_Azimuth=-2.0f*PI*ioMouse::GetNormX();
	m_Incline=PI*ioMouse::GetNormY()-PI*0.5f;

	Matrix34 temp;
	temp.FastInverse(m_WorldMtx);
	Convert(m_ViewMtx,temp);
}

////////////////////////////////////////////////////////////////////////////////
