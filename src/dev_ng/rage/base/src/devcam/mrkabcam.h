//
// devcam/polarcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_MRKABCAM_H
#define DEVCAM_MRKABCAM_H
#if !__FINAL
#include "mrkcambase.h"

namespace rage {

class dcamMrkABCam : public dcamMrkCamBase {
public:
	// PURPOSE: default constructor
	dcamMrkABCam();
	virtual ~dcamMrkABCam();

	virtual bool		Update(float dt);
	virtual const char*	GetName() const;
	virtual void		Reset();
	virtual void		DebugDraw(float scrX, float scrY);
#if __BANK
	virtual void		AddWidgets(class bkBank &bk);
#endif

	void SetSetFOVKeyCallback(const atDelegate< void (void) > & callback);
	void SetStartFOVTransitionCallback(const atDelegate< void (void) > & callback);
	void SetUpdateFOVTransitionCallback(const atDelegate< void (float percentDone) > & callback);
protected:

	//derived interface
	virtual void DrawInfoText(float scrX, float scrY);

	virtual void HandleHeldDebugButtons(const u32 dButtons);
	virtual void HandleDebugButtons(const u32 dButtons);

private:
	void UpdateTransition(float dt);

	bool m_AIsSet;
	bool m_DoTransition;
	bool m_AtoB;
	bool m_SlowIn;
	bool m_SlowOut;
	bool m_SlowInOut;
	float m_TransStep;
	float m_ATextTime;
	float m_BTextTime;
	float m_MaxTextDisplayTime;
	float m_CurrentTime;
	float m_TotalTransitionTime;
	Matrix34 m_A;
	Matrix34 m_B;

	atDelegate< void (void) > m_SetFOVKeyCallback;
	atDelegate< void (void) > m_StartFOVTransitionCallback;
	atDelegate< void (float percentDone) > m_UpdateFOVTransitionCallback;
};

}	// namespace rage

#endif //#if !__FINAL
#endif
