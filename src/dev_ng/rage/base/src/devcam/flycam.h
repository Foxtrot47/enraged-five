//
// devcam/flycam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_FLYCAM_H
#define DEVCAM_FLYCAM_H

#include "devcam.h"

#include "vector/matrix44.h"

namespace rage {

class spdSphere;
//
// PURPOSE
//   A simple camera controlled by the mouse. 
//
// NOTES
//	<TABLE>
//      Mouse                  Action
//      ---------------------  ----------------------
//      Horizontal             azimuth, left-right
//      Vertical               elevation, up-down
//      Left Button            move forward
//      Right button           move backward
//	</TABLE>
// <FLAG Component>
//
class dcamFlyCam : public dcamCam
{
public:
	// PURPOSE: default constructor
	dcamFlyCam();
	void Reset();
	bool Update(float dt);
	void Init(const Vector3 &camPos, const Vector3 &lookAtPos);
	void Draw(int  scrX, int scrY, float alpha) const;
	const Matrix34 &GetWorldMtx() const;
	void SetWorldMtx(const Matrix34&);
	virtual void Frame(const spdSphere& frameSphere);
	// PURPOSE: accessor for the camera's view matrix (inverse of the world matrix)
	// RETURNS: a reference to the camera's view matrix
	Matrix44 &GetViewMtx();
	//
	// PURPOSE
	//	sets the mouse translation speed 
	// PARAMS
	//	s - the speed of the camera
	//
	void SetSpeed(float s);

	const char *GetName() const;

private:
	Matrix34 m_WorldMtx;
	Matrix44 m_ViewMtx;

	Vector3 m_InitPos;
	Vector3 m_LookAtPos;

	float m_Azimuth;
	float m_Incline;

	float m_RadsPerTick;		// Radians per mouse tick
	float m_Speed;				// Flight speed
};

inline const Matrix34& dcamFlyCam::GetWorldMtx() const		
{
	return m_WorldMtx;
}

inline void dcamFlyCam::Frame(const spdSphere& /*frameSphere*/)
{
	//Not implemented
}

inline Matrix44 & dcamFlyCam::GetViewMtx()				
{
	return m_ViewMtx;
}

inline void dcamFlyCam::SetSpeed(float s)		
{ 
	m_Speed=s; 
}

inline const char* dcamFlyCam::GetName() const 
{
	return "Fly Cam";
}

}	// namespace rage

#endif // DEVCAM_FLYCAM_H
