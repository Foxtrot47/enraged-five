//
// devcam/devcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_DEVCAM_H
#define DEVCAM_DEVCAM_H

#include "grcore/font.h"
#include "input/mapper.h"
#include "input/keys.h"
#include "vector/matrix34.h"
#include "vector/color32.h"
#include "system/alloca.h"
#include "string/string.h"

namespace rage {

class Vector3;
class spdSphere;

//
// PURPOSE
//   An interface class for development cameras. The dcamCamMgr holds a
//   collection of these.
//
class dcamCam {
public:
	// PURPOSE: default constructor
	dcamCam() 
		: m_UseZUp(false), m_vUp(YAXIS)
	{};

	// PURPOSE: destructor
	virtual ~dcamCam() {}
	
	// 
	//
	// PURPOSE
	//	Init camera with camera's position and the "look at" position
	// PARAMS
	//	camPos - the world space position of the camera
	//	targetPos - the world space point to look at
	//
	virtual void Init(const Vector3 &camPos, const Vector3 &targetPos) = 0;
	
	// PURPOSE
	//  Control whether an activation key must be held down to actually move the camera.
	// PARAMS
	//  requireKey - true to turn on the requirement of holding down a key.
	// NOTES
	//  Initially implemented for the Maya camera.
	virtual void SetRequiresKey(bool requiresKey = true);

	//
	// PURPOSE
	//	updates the camera
	// PARAMS
	//	dt - delta time since last frame
	//
	virtual bool Update(float dt) = 0;

	// PURPOSE: Reset the camera to the init position & target position
	virtual void Reset() = 0;
	// 
	// PURPOSE: accessor for the world space matrix of the camera
	// RETURNS: world space matrix of the camera
	virtual const Matrix34& GetWorldMtx() const = 0;

	// PURPOSE: allows setting the camera's world matrix.
	virtual void SetWorldMtx(const Matrix34&) = 0;

	// PURPOSE
	//  Frame the supplied sphere in the camera view.
	// PARAMS
	//  frameSphere - bounding sphere to frame 
	virtual void Frame(const spdSphere& frameSphere) = 0;

	// 
	//
	// PURPOSE
	//	Draw any camera usage info 
	// PARAMS
	//	scrX - the x position to start drawing at
	//	scrY - the y position to start drawing at
	//
	virtual void Draw(int scrX, int scrY, float alpha) const = 0;

	// 
	//
	// PURPOSE
	//	Draws Camera's Name
	// PARAMS
	//	scrX - the x position to start drawing at
	//	scrY - the y position to start drawing at
	virtual int DrawCameraName(int x,int y, float alpha) const
	{
		// Display name of active camera
		int ty=y;
		ty = DrawLine(x,ty,GetName(),Color32(1.f,1.f,1.f,alpha));

		int len=StringLength(GetName());
		char* separator=Alloca(char,len+1);
		for (int i=0;i<len;i++)
		{
			separator[i]='-';
		}
		separator[len]='\0';
		ty=DrawLine(x,ty,separator,Color32(1.f,1.f,1.f,alpha));
		return ty;
	}

	// PURPOSE: accessor for the name of the camera
	// RETURNS: returns a char* pointing to the name of the camera
	virtual const char *GetName() const = 0;

	// PURPOSE: Called when cameramgr activates a camera
	virtual void Activated();
	// PURPOSE: Called when cameramgr deactivates a camera
	virtual void Deactivated() {}

	//
	// PURPOSE
	//  Set the use Z-Up flag
	// PARAMS
	//  bUseZUp - boolean indicating if Z should be the up axis.
	inline void SetUseZUp(bool bUseZUp);

	// 
	// PURPOSE
	//  Get the use Z-Up flag
	// PARAMS
	//	none
	// RETURNS
	//	Value of the use Z-Up flag
	inline bool GetUseZUp() const;

#if __BANK
	//
	// PURPOSE
	//	adds camera widgets to a bank
	// PARAMS
	//	bk - the bank to add widgets to
	//
	virtual void AddWidgets(class bkBank & /*bk*/) {}
#endif

	//
	// PURPOSE
	//	draws a line of colored text in the current font
	// PARAMS
	//	scrX - the x position to start drawing at
	//	scrY - the y position to start drawing at
	//	str - the text string to draw
	//	color - the color of the text string
	// RETURNS
	//	adjusted screen Y position after drawn line
	//
	static int DrawLine(int scrX,int scrY,const char* str,Color32 color);

protected:
	bool	m_UseZUp;
	Vector3	m_vUp;
};

inline void dcamCam::SetRequiresKey(bool UNUSED_PARAM(requiresKey))
{
}

inline void dcamCam::Activated()
{
}

inline int dcamCam::DrawLine(int scrX,int scrY,const char* str,Color32 color)
{
	const grcFont& font=grcFont::GetCurrent();

	if (color.GetAlpha() > 0.0f)
	{
		Color32 black(0, 0, 0, color.GetAlpha());

		font.DrawScaled((float)scrX + 1, (float)scrY + 2, 0, black,1,1,str);
		font.DrawScaled((float)scrX    , (float)scrY    , 0, color,1,1,str);
	}

	return scrY+font.GetHeight()+2;
}

inline void dcamCam::SetUseZUp(bool bUseZUp)
{
	m_UseZUp = bUseZUp;
	m_vUp.Set(ZAXIS);
}

inline bool dcamCam::GetUseZUp() const
{
	return m_UseZUp;
}

}	// namespace rage

#endif		// DEVCAM_DEVCAM_H
