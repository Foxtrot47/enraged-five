//
// devcam/roamcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_ROAMCAM_H
#define DEVCAM_ROAMCAM_H

#include "devcam.h"

#include "input/mapper.h"
#include "vector/matrix34.h"

namespace rage {

class bkBank;
class spdSphere;

//
// PURPOSE:
//	A camera for development only. It uses the gamepad to control the camera position and orientation.
// NOTES:
// * Gamepad Controls *
//  <TABLE>
//      Button/Joystick  Action
//      ---------------  --------------------------------------------------------------------------
//      Left stick       moves camera right, left, forward, backward (does not change elevation)
//	    Right stick      pitches and yaws camera.
//      R1, R2           Move camera higher, lower.
//	    L1               Polar lock, left stick moves camera forward, backward (changes elevation), 
//                         right stick rotates camera around a point in front of the camera.
//  </TABLE>
//<FLAG Component>
//
class dcamRoamCam : public dcamCam {
public:
	// PURPOSE: default constructor
	dcamRoamCam();

	virtual const char* GetName() const;
	virtual void Init(const Vector3 &camPos, const Vector3 &lookAtPos);
	virtual bool Update(float);
	virtual void Reset();
	virtual void Draw(int scrX, int scrY, float alpha) const;
	virtual const Matrix34 &GetWorldMtx() const;
	virtual void SetWorldMtx(const Matrix34 &mtx);
	virtual void Frame(const spdSphere& frameSphere);
	Matrix34 &GetWorldMtx();
#if __BANK
	//<COMBINE dcamCam::AddWidgets>
	virtual void AddWidgets(bkBank &bk);
#endif

	//
	// PURPOSE
	//	force the camera to look down the -y axis
	// PARAMS
	//	b - true if camera should look down -y, false if not
	//
	void SetForceLookDown(bool b);

private:
	void InternalInit();

	Matrix34 m_WorldMtx;
	Vector3 m_Interest;
	ioMapper m_Mapper;
	ioValue m_Heading, m_Pitch, m_Strafe, m_Forward, m_Sink, m_Rise, m_Accelerator, m_ResetPitch, m_PolarLock;
	float m_FastSpeed, m_SlowSpeed;
	bool m_ForceLookDown;
	Vector3 m_InitPos;
	Vector3 m_LookAtPos;
};


inline dcamRoamCam::dcamRoamCam() {
	InternalInit();
}

inline const char* dcamRoamCam::GetName() const {
	return "Roam Cam";
}

inline const Matrix34 &dcamRoamCam::GetWorldMtx() const {
	return m_WorldMtx;
}

inline void dcamRoamCam::Frame(const spdSphere& /*frameSphere*/)
{
	//Not implemented
}

inline Matrix34 &dcamRoamCam::GetWorldMtx() {
	return m_WorldMtx;
}

inline void dcamRoamCam::SetWorldMtx(const Matrix34 &mtx)
{
	m_WorldMtx.Set(mtx);
	m_Interest.AddScaled(m_WorldMtx.d,m_WorldMtx.c,-25);	// arbitrary
}

inline void dcamRoamCam::SetForceLookDown(bool b) {
	m_ForceLookDown = b;
}

}	// namespace rage

#endif // DEVCAM_ROAMCAM_H
