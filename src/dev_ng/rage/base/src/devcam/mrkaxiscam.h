// 
// devcam/mrkaxiscam.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEVCAM_MRKAXISCAM_H 
#define DEVCAM_MRKAXISCAM_H 
#if !__FINAL
#include "mrkcambase.h"

namespace rage {

	class dcamMrkAxisCam : public dcamMrkCamBase {
	public:
		// PURPOSE: default constructor
		dcamMrkAxisCam();
		virtual ~dcamMrkAxisCam();

		virtual const char*			GetName() const;

	protected:

		//derived interface
		virtual void DrawInfoText(float scrX, float scrY);

		virtual void HandleLStick(const Vector2& vectorXY);
		virtual void HandleHeldButtons(const u32 buttons);
	private:
		
	};

}	// namespace rage

#endif //#if !__FINAL
#endif // DEVCAM_MRKAXISCAM_H 
