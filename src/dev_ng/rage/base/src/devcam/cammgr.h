//
// devcam/cammgr.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_CAMMGR_H
#define DEVCAM_CAMMGR_H

#include "devcam.h"

#include "atl/array.h"
#include "input/mapper.h"

namespace rage {

class spdSphere;

// 
// PURPOSE
//   Manages a set of dcamCam's, and allows the user to switch between them.
// <FLAG Component>
//
class dcamCamMgr
{
public:
	// default constructor
	dcamCamMgr();
	// Destructor
	~dcamCamMgr();
	
	// List of available cameras (if you add more, make sure you allocate one in cammgr.c's ctor)
	enum eCamera { CAM_POLAR, CAM_MAYA, CAM_FLY, CAM_FREE, CAM_ROAM, CAM_TRACK, CAM_QUAKE, CAM_LH, CAM_WAR, CAM_COUNT };

#if !__FINAL
	// List of available (NYC) marketing specific cameras (if you add more, make sure you allocate one in cammgr.c's ctor)
	enum eMrkCamera { MRK_CAM_FREE, MRK_CAM_ORBIT, MRK_CAM_MAN, MRK_CAM_AB, MRK_CAM_AXIS, MRK_CAM_NORTH, MRK_CAM_COUNT };

	//
	// PURPOSE: Cycle to the next marketing camera in the enum
	void			CycleMarketingCamera();

	//
	// PURPOSE: Reset the current marketing camera and set the current back to the default
	void			ResetMrkCamera();

	//
	// PURPOSE: Update the active marketing camera
	void			UpdateMrkCamera();

	//
	// PURPOSE: Reset the active marketing camera and dont reset the active cam to default
	void			ResetCurrentMrkCamera();

	//
	// PURPOSE: get the matrix of the current marketing camera
	// RETURNS
	//	matrix of the current marketing camera
	const Matrix34& GetCurrentMrkCamMatrix();

	//
	// PURPOSE: Set the matrix of the current marketing camera
	// PARAMS
	//	const Matrix34& matrix - matrix to set
	void			SetCurrentMrkCamMatrix(const Matrix34& matrix);

	//
	// PURPOSE: Draw the name of the camera on the screen using the pfDraw system
	// PARAMS
	//	X - x position on the screen
	//  Y - y position on the screen
	void			DrawMrkCamName(float x,float y) const;

	//
	// PURPOSE: Get the enum value of the current marketing camera
	// RETURNS
	//	enum value of the active marketing camera
	eMrkCamera		GetCurrentMrkCamera() const;

	//
	// PURPOSE: Set the enum value of the current marketing camera
	// PARAMS
	//	cam - the camera type 
	void			SetCurrentMrkCamera(eMrkCamera cam);

	//
	// PURPOSE: Return a marketing camera pointer
	// PARAMS
	//	cam - the camera type 
	// RETURNS
	//	a pointer to the requested marketing camera
	dcamCam*		GetMrkCamera(eMrkCamera cam);

	//
	// PURPOSE
	//	Draw debug info (mainly this is for marketing system so text drawing can be done in release builds)
	// PARAMS
	//	x - the x position to start drawing at
	//	y - the y position to start drawing at 
	//
	void DebugDraw(float x,float y);
#endif

	// 
	//
	// PURPOSE
	//	Init the camera system
	// PARAMS
	//	camPos - the initial camera position in world space
	//	lookAtPos - the point that the camera is looking at 
	//	initialCam - the active camera
	//
	void Init(const Vector3 &camPos, const Vector3 &lookAtPos, eCamera initialCam=CAM_POLAR);
	
	// PURPOSE: Update the active camera, or change to new camera if requested
	void Update();

	// PURPOSE: Reset the camera system (the selected camera remains active)
	void Reset();
	
	// PURPOSE: Frame the supplied sphere in the camera view.
	void Frame(const spdSphere& frameSphere);

	//
	// PURPOSE
	//	Draws the current camera type and its instructions. Call this after the scene has been rendered.
	// PARAMS
	//	x - the x position to start drawing at
	//	y - the y position to start drawing at 
	//
	void Draw(float x,float y) const;

	// PURPOSE: Draws the current camera type and its instructions. Call this after the scene has been rendered.
	void Draw() const;

	// PURPOSE: Retrieve the current fade value of the on-screen text
	float GetInfoFadeTime() const;

	//
	// PURPOSE
	//	Accessor for a requested camera
	// PARAMS
	//	cam - the camera type 
	// RETURNS
	//	a pointer to the requested camera or NULL if the camera does not exist
	//
	inline dcamCam& GetCamera(eCamera cam);

	// PURPOSE: accessor for the current camera's world matrix
	// RETURNS: returns the current camera's world matrix
	inline const Matrix34 &GetWorldMtx() const;

	// PURPOSE: allows setting of the current camera's world matrix
	inline void SetWorldMtx(const Matrix34&);

    inline void SetEnableDrawHelp(bool enable);

	inline void SetAcceptInput(bool accept);

	//
	// PURPOSE
	//	Switch to a different camera type
	// PARAMS
	//	newCamera - the camera to activate
	//
	void SetCamera(eCamera newCamera);

	//
	// PURPOSE
	//  Get the current camera type
	// PARAMS
	//  none
	inline eCamera GetCurrentCamera() const;

	//
	// PURPOSE
	//  Set the use Z-Up flag
	// PARAMS
	//  bUseZUp - boolean indicating if Z should be the up axis.
	inline void SetUseZUp(bool bUseZUp);

	// 
	// PURPOSE
	//  Get the use Z-Up flag
	// PARAMS
	//	none
	// RETURNS
	//	Value of the use Z-Up flag
	inline bool GetUseZUp() const;

#if __BANK
	//
	// PURPOSE
	//	adds all widgets of each contained camera type to a specified bank
	// PARAMS
	//	bk - the bank to add the widgets to
	//
	void AddWidgets(class bkBank &bk);

	//
	// PURPOSE
	//	Only add marketing camera widgets of each contained camera type to a specified bank
	// PARAMS
	//	bk - the bank to add the widgets to
	//
	void AddMrkWidgets(class bkBank &bk);
#endif

private:
	//
	// PURPOSE
	//	Updates the fading value
	// PARAMS
	//	dt - delta time since last frame
	//
	void UpdateFade(float dt);

	eCamera								m_CurrentCamera;
	eCamera								m_InitialCamera;
	atRangeArray<dcamCam *, CAM_COUNT>	m_Cameras;
	ioMapper							m_Mapper;
	ioValue								m_Help;
	ioValue								m_NextCam;
	ioValue								m_ToggleInput;
	bool								m_AcceptInput;
    bool                                m_DrawCameraHelp;

	// PURPOSE: The time remaining to fade out the on-screen info
	float								m_InfoFadeTime;

	bool								m_UseZUp;
#if !__FINAL
	eMrkCamera								m_CurrentMrkCamera;
	eMrkCamera								m_InitialMrkCamera;
	atRangeArray<dcamCam* , MRK_CAM_COUNT>	m_MrkCameras;
#endif
};

inline void dcamCamMgr::Frame(const spdSphere& frameSphere)
{
	for(int i=0; i<CAM_COUNT; i++)
		m_Cameras[i]->Frame(frameSphere);
}

inline float dcamCamMgr::GetInfoFadeTime() const
{
	return m_InfoFadeTime;
}

inline dcamCam& dcamCamMgr::GetCamera(eCamera cam) 
{ 
	FastAssert(m_Cameras[cam]);
	return *m_Cameras[cam]; 
}

inline dcamCamMgr::eCamera dcamCamMgr::GetCurrentCamera() const
{
	return m_CurrentCamera;
}

inline const Matrix34& dcamCamMgr::GetWorldMtx() const 
{ 
	return m_Cameras[m_CurrentCamera]->GetWorldMtx(); 
}

inline void dcamCamMgr::SetWorldMtx(const Matrix34& mtx)
{ 
	m_Cameras[m_CurrentCamera]->SetWorldMtx(mtx); 
}

inline void dcamCamMgr::SetEnableDrawHelp(bool enable)
{
    m_DrawCameraHelp = enable;
}

inline void dcamCamMgr::SetAcceptInput(bool accept)
{
    m_AcceptInput = accept;
}

inline void dcamCamMgr::UpdateFade(float dt)
{
	const float CAMERA_FADE_SECOND = 5.0f;

	m_InfoFadeTime -= dt * (1.0f / CAMERA_FADE_SECOND);

	if (m_Help.IsDown())
	{
		m_InfoFadeTime = 1.0f;
	}
	else if (m_InfoFadeTime < 0.0f)
	{
		m_InfoFadeTime = 0.0f;
	}
}

inline void dcamCamMgr::SetUseZUp(bool bUseZUp)
{
	m_UseZUp = bUseZUp;

	for(int i=0; i<CAM_COUNT; i++)
		m_Cameras[i]->SetUseZUp(bUseZUp);
}

inline bool dcamCamMgr::GetUseZUp() const
{
	return m_UseZUp;
}

}	// namespace rage

#endif // DEVCAM_CAMMGR_H
