//
// devcam/cammgr.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "cammgr.h"

#include "flycam.h"
#include "freecam.h"
#include "mayacam.h"
#include "polarcam.h"
#include "quakecam.h"
#include "roamcam.h"
#include "trackcam.h"
#include "lhcam.h"
#include "warcam.h"

#include "mrkabcam.h"
#include "mrkmancam.h"
#include "mrkorbitcam.h"
#include "mrkfreecam.h"
#include "mrkaxiscam.h"
#include "mrknorthcam.h"

#include "bank/bank.h"
#include "input/input.h"
#include "input/keys.h"
#include "input/pad.h"
#include "grcore/device.h"
#include "grcore/font.h"
#include "system/alloca.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "grprofile/drawmanager.h"

using namespace rage;

PARAM(campos, "[devcam] Initial camera position to use in 'x,y,z' format");
PARAM(xview, "x-view");
PARAM(camfocus, "[devcam] Initial position for camera to look at in 'x,y,z' format");
PARAM(separatedevcams, "[devcam] Treat devcams as separate cameras; do not maintain current view when switching.");

dcamCamMgr::dcamCamMgr()
	: m_CurrentCamera(CAM_POLAR)
	, m_InitialCamera(CAM_POLAR)
    , m_AcceptInput(true)
	, m_DrawCameraHelp(true)
	, m_InfoFadeTime(1.0f)
	, m_UseZUp(false)
#if !__FINAL
	, m_CurrentMrkCamera(MRK_CAM_FREE)
	, m_InitialMrkCamera(MRK_CAM_FREE)
#endif
{
	// Create the various dev cameras
	m_Cameras[CAM_QUAKE] = rage_new dcamQuakeCam;
	m_Cameras[CAM_FLY] = rage_new dcamFlyCam;
	m_Cameras[CAM_FREE] = rage_new dcamFreeCam;
	m_Cameras[CAM_MAYA] = rage_new dcamMayaCam;
	m_Cameras[CAM_POLAR] = rage_new dcamPolarCam;
	m_Cameras[CAM_ROAM] = rage_new dcamRoamCam;
	m_Cameras[CAM_TRACK] = rage_new dcamTrackCam;
	m_Cameras[CAM_LH] = rage_new dcamLhCam;
	m_Cameras[CAM_WAR] = rage_new dcamWarCam;

#if !__FINAL
	// Marketing cameras
	m_MrkCameras[MRK_CAM_FREE] = rage_new dcamMrkFreeCam;
	m_MrkCameras[MRK_CAM_ORBIT] = rage_new dcamMrkOrbitCam;
	m_MrkCameras[MRK_CAM_MAN] = rage_new dcamMrkManCam;
	m_MrkCameras[MRK_CAM_AB] = rage_new dcamMrkABCam;
	m_MrkCameras[MRK_CAM_AXIS] = rage_new dcamMrkAxisCam;
	m_MrkCameras[MRK_CAM_NORTH] = rage_new dcamMrkNorthCam;
#endif

	// Map controls
	m_Mapper.Reset();
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::R3, m_NextCam);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_TAB, m_NextCam);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_F1, m_Help);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_GRAVE, m_ToggleInput);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::SELECT, m_ToggleInput);
    m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L1, m_Help);
}

dcamCamMgr::~dcamCamMgr()
{
	for (int i = 0; i < CAM_COUNT; ++i)
	{
		delete m_Cameras[i];
		m_Cameras[i] = 0;
	}

#if !__FINAL
	for (int i = 0; i < MRK_CAM_COUNT; ++i)
	{
		delete m_MrkCameras[i];
		m_MrkCameras[i] = 0;
	}
#endif
}

#if !__FINAL
void dcamCamMgr::CycleMarketingCamera()
{
	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	Matrix34 mat = m_MrkCameras[m_CurrentMrkCamera]->GetWorldMtx();
	m_MrkCameras[m_CurrentMrkCamera]->Reset(); //reset the current camera

	int temp = m_CurrentMrkCamera;
	temp++;
	temp %= MRK_CAM_COUNT;
	m_CurrentMrkCamera = (eMrkCamera)temp;

	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	m_MrkCameras[m_CurrentMrkCamera]->SetWorldMtx(mat);
}

void dcamCamMgr::ResetMrkCamera()
{
	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	m_MrkCameras[m_CurrentMrkCamera]->Reset();

	m_CurrentMrkCamera = m_InitialMrkCamera;

	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	m_MrkCameras[m_CurrentMrkCamera]->Reset();
}

void dcamCamMgr::ResetCurrentMrkCamera()
{
	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	m_MrkCameras[m_CurrentMrkCamera]->Reset();
}

void dcamCamMgr::UpdateMrkCamera()
{
	float dt = TIME.GetUnwarpedRealtimeSeconds();

	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	m_MrkCameras[m_CurrentMrkCamera]->Update(dt);
}

const Matrix34& dcamCamMgr::GetCurrentMrkCamMatrix()
{
	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	return m_MrkCameras[m_CurrentMrkCamera]->GetWorldMtx();
}

void dcamCamMgr::SetCurrentMrkCamMatrix(const Matrix34& matrix)
{
	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);
	m_MrkCameras[m_CurrentMrkCamera]->SetWorldMtx(matrix);
}

void dcamCamMgr::DrawMrkCamName(float x,float y) const
{
	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	Assert(m_MrkCameras[m_CurrentMrkCamera]);

	//Display control widgets
	grcFont::GetCurrent().Drawf(x,y,m_MrkCameras[m_CurrentMrkCamera]->GetName());
}

dcamCamMgr::eMrkCamera dcamCamMgr::GetCurrentMrkCamera() const
{
	Assert(m_CurrentMrkCamera >= MRK_CAM_FREE && m_CurrentMrkCamera < MRK_CAM_COUNT);
	return m_CurrentMrkCamera;
}

void dcamCamMgr::SetCurrentMrkCamera(eMrkCamera cam)
{
	Assert(cam >= MRK_CAM_FREE && cam < MRK_CAM_COUNT);
	m_CurrentMrkCamera = cam;
}

dcamCam* dcamCamMgr::GetMrkCamera(eMrkCamera cam)
{
	Assert(cam >= MRK_CAM_FREE && cam < MRK_CAM_COUNT);
	Assert(m_MrkCameras[cam]);
	return m_MrkCameras[cam];
}

void dcamCamMgr::DebugDraw(float x,float y)
{
	dcamMrkCamBase* mrkCamera = (dcamMrkCamBase*)GetMrkCamera(GetCurrentMrkCamera());
	mrkCamera->DebugDraw(x,y);
}

#endif

void dcamCamMgr::Init(const Vector3 &camPos, const Vector3 &lookAtPos, eCamera initialCam)
{
	Vector3 pos(camPos);
	float vals[3];
	if ( PARAM_campos.GetArray(vals, 3) == 3 ) {
		pos.x = vals[0];
		pos.y = vals[1];
		pos.z = vals[2];
	}
	if(PARAM_xview.Get())
		pos.x+=5.0f;
	Vector3 focus(lookAtPos);
	if ( PARAM_camfocus.GetArray(vals, 3) == 3) {
		PARAM_camfocus.GetArray(vals, 3);
		focus.x = vals[0];
		focus.y = vals[1];
		focus.z = vals[2];
	}
	
	for (int i = 0; i < CAM_COUNT; ++i)
	{
		m_Cameras[i]->Init(pos, focus);
	}
	m_InitialCamera = initialCam;
	m_CurrentCamera = initialCam;
}

void dcamCamMgr::Update()
{
	float dt = TIME.GetUnwarpedRealtimeSeconds();

	m_Mapper.Update();
	if(m_ToggleInput.IsPressed())
	{
		m_InfoFadeTime = 1.0f;
		m_AcceptInput = !m_AcceptInput;
	}

    if(!m_AcceptInput)
	{
		UpdateFade(dt);
		return;
	}

	//If we just turned on then activate the current camera
	if(m_ToggleInput.IsPressed())
	{
		m_Cameras[m_CurrentCamera]->Activated();
	}

	if(m_NextCam.IsPressed())
	{
		eCamera nextCam = (eCamera) ((m_CurrentCamera + 1) % CAM_COUNT);
		SetCamera(nextCam);
	}

	UpdateFade(dt);

	if (m_Cameras[m_CurrentCamera]->Update(dt) && m_DrawCameraHelp)
	{
        m_InfoFadeTime = 1.0f;
	}
}

void dcamCamMgr::Reset()
{ 
	for (int i = 0; i < CAM_COUNT; ++i)
	{
		m_Cameras[i]->Reset();
	}
}

void dcamCamMgr::Draw() const
{
	Draw((float)(GRCDEVICE.GetWidth() - 140),(float)(GRCDEVICE.GetHeight() - 120));
}

void dcamCamMgr::Draw(float x,float y) const
{
	if(!m_AcceptInput)
	{
		dcamCam::DrawLine((int)x,(int)y,"Camera Input Off",Color32(1.0f,1.0f,1.0f,m_InfoFadeTime));
		return;
	}

	int sy = m_Cameras[m_CurrentCamera]->DrawCameraName((int)x, (int)(y), m_InfoFadeTime);
	m_Cameras[m_CurrentCamera]->Draw((int)x, (int)(sy), m_InfoFadeTime);
}

void dcamCamMgr::SetCamera(eCamera newCamera)
{
	m_InfoFadeTime = 1.0f;
	if(newCamera == m_CurrentCamera)
		return;

	const Matrix34& currentMatrix = m_Cameras[m_CurrentCamera]->GetWorldMtx();
	m_Cameras[m_CurrentCamera]->Deactivated();
	m_CurrentCamera = newCamera;
	if (!PARAM_separatedevcams.Get())
	{
		m_Cameras[m_CurrentCamera]->SetWorldMtx(currentMatrix);
	}
	m_Cameras[m_CurrentCamera]->Activated();
}


#if __BANK
void dcamCamMgr::AddWidgets(class bkBank &bk)
{
	bk.PushGroup("Dev Cameras", false);
	for (int i = 0; i < CAM_COUNT; ++i)
	{
		m_Cameras[i]->AddWidgets(bk);
	}
	bk.PopGroup();
}

void dcamCamMgr::AddMrkWidgets(class bkBank &bk)
{
	for (int i = 0; i < MRK_CAM_COUNT; ++i)
	{
		bk.PushGroup(m_MrkCameras[i]->GetName(), false);
		m_MrkCameras[i]->AddWidgets(bk);
		bk.PopGroup();
	}
}
#endif	// __BANK
