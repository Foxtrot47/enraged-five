//
// devcam/polarcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_MRKFREECAM_H
#define DEVCAM_MRKFREECAM_H
#if !__FINAL
#include "mrkcambase.h"

namespace rage {

class dcamMrkFreeCam : public dcamMrkCamBase {
public:
	// PURPOSE: default constructor
	dcamMrkFreeCam();
	virtual ~dcamMrkFreeCam();

	virtual const char*	GetName() const;
	virtual bool		Update(float dt);

	void SetScreenshotCallback(const atDelegate< void (void) > & callback);
	void SetCameraLightToggleCallback(const atDelegate< void (void) > & callback);
	void SetCameraLightUpdateCallback(const atDelegate< void (const Vector3 &position) > & callback);
	void SetCameraLightIntensityCallback(const atDelegate< void (bool add) > & callback);
	void SetCameraLightRadiusCallback(const atDelegate< void (bool add) > & callback);
protected:

	//derived interface
	virtual void DrawInfoText(float scrX, float scrY);

	virtual void HandleButtons(const u32 buttons);
	virtual void HandleHeldDebugButtons(const u32 dButtons);

private:
	atDelegate< void (void) > m_ScreenshotCallback;
	atDelegate< void (void) > m_CameraLightToggleCallback;
	atDelegate< void (const Vector3 &position) > m_CameraLightUpdateCallback;
	atDelegate< void (bool add) > m_CameraLightIntensityCallback;
	atDelegate< void (bool add) > m_CameraLightRadiusCallback;
};

}	// namespace rage

#endif //#if !__FINAL
#endif
