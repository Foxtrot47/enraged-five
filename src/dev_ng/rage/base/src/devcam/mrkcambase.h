// 
// devcam/mrkcambase.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEVCAM_MRKCAMBASE_H 
#define DEVCAM_MRKCAMBASE_H 
#if !__FINAL
#include "devcam.h"
#include "atl/delegate.h"

namespace rage {

class dcamMrkCamBase : public dcamCam {
public:
	// PURPOSE: default constructor
	dcamMrkCamBase();
	virtual ~dcamMrkCamBase();

	//dcamCam Pure virtual interface
	virtual void				Init(const Vector3 &cameraPos, const Vector3 &lookAtPos);
	virtual bool				Update(float dt);
	virtual void				Draw(int scrX, int scrY, float alpha) const;
	virtual void				Reset();
	virtual const char*			GetName() const;
	virtual const Matrix34 &	GetWorldMtx() const;
	virtual void				SetWorldMtx(const Matrix34 &mtx);
	virtual void				Frame(const spdSphere& frameSphere);
	virtual void				DebugDraw(float scrX, float scrY);
#if __BANK
	virtual void				AddWidgets(class bkBank &bk);
#endif
	
	void SetHUDToggleCallback(const atDelegate< void (void) > & callback);
	void SetStepTimeCallback(const atDelegate< void (void) > & callback);
	void SetDropPlayerCallback(const atDelegate< void (void) > & callback);
	void SetAdvanceTimeCallback(const atDelegate< void (void) > & callback);
	void SetCycleWeatherCallback(const atDelegate< void (void) > & callback);
	void SetPauseGameCallback(const atDelegate< void (void) > & callback);
	void SetToggleTimeCycleCallback(const atDelegate< void (void) > & callback);
	void SetIncreaseFOVCallback(const atDelegate< void (float) > & callback);
	void SetDecreaseFOVCallback(const atDelegate< void (float) > & callback);
	void SetResetFOVCallback(const atDelegate< void (void) > & callback);

protected:
	//derived interface
	virtual void DrawInfoText(float scrX, float scrY);

	virtual void HandleButtons(const u32 buttons);
	virtual void HandleDebugButtons(const u32 dButtons);
	virtual void HandleLStick(const Vector2& vectorXY);
	virtual void HandleRStick(const Vector2& vectorXY);
	virtual void HandleTriggers(const float left, const float right);
	virtual void HandleHeldButtons(const u32 buttons);
	virtual void HandleHeldDebugButtons(const u32 dButtons);

protected:
	float m_CurRollDiff;
	float m_RollAmount;
	float m_MoveAmount;
	float m_MoveSpeedAugment;
	float m_RotSpeedAugment;
	float m_dt;
	Matrix34 m_WorldMtx;
	bool m_DrawInfo;

	atDelegate< void (void) > m_HUDToggleCallback;
	atDelegate< void (void) > m_StepTimeCallback;
	atDelegate< void (void) > m_DropPlayerCallback;
	atDelegate< void (void) > m_AdvanceTimeCallback;
	atDelegate< void (void) > m_CycleWeatherCallback;
	atDelegate< void (void) > m_PauseGameCallback;
	atDelegate< void (void) > m_ToggleTimeCycleCallback;
	atDelegate< void (float)> m_IncreaseFOVCallback;
	atDelegate< void (float)> m_DecreaseFOVCallback;
	atDelegate< void (void) > m_ResetFOVCallback;
};

} // namespace rage

#endif // #if !__FINAL
#endif // DEVCAM_MRKCAMBASE_H 
