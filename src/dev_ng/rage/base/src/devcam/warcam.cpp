//
// devcam/freecam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "warcam.h"

#include "bank/bkmgr.h"
#include "input/input.h"
#include "spatialdata/sphere.h"
#include "vector/matrix34.h"
#include "vector/quaternion.h"


using namespace rage;

// ============================================================================
// ========================================================   CONSTANTS   =====
#define M_Deg2Rad( _d )		( ((_d) * 3.141592653589f) / 180.0f )
const float fYawRate        = M_Deg2Rad(150.0f);	// radians/sec
const float fPitchRate      = M_Deg2Rad( 90.0f);
const float fFwdSpeed       = 4.00f;				// meters/sec
const float fSideSpeed      = 3.00f;
const float fVerticalSpeed  = 3.25f;
const float fMaxAcceleration = 4.0f;
const float fCameraRollRate = M_Deg2Rad( 75.0f);	// radians/sec
const float fYawDeadzone    = 0.15f;
const float fPitchDeadzone  = 0.30f;

// ============================================================================
// ============================================================================
dcamWarCam::dcamWarCam()
{
	// Note: we change the mapping for L1, since this is used by the camera manager for help.
	// This mapping is restored in the camera manager when the current camera is changed from this one.
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_RX, m_Yaw);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_RY, m_Pitch);
	//m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LX, m_Strafe);
	//m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LY, m_Forward);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LUP,    m_Forward);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LDOWN,  m_Reverse);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LLEFT,  m_StrafeLeft);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::LRIGHT, m_StrafeRight);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L1, m_Sink);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::R1, m_Rise);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L2, m_Roll);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::R2, m_Accelerator);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L3, m_Reset);

	// Alternate keyboard+mouse control scheme.
	////m_Mapper.Map(IOMS_MOUSE_RELATIVEAXIS, IOM_AXIS_X, m_Yaw);
	////m_Mapper.Map(IOMS_MOUSE_RELATIVEAXIS, IOM_AXIS_Y, m_Pitch);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_W, m_Forward);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_S, m_Reverse);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_A, m_StrafeLeft);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_D, m_StrafeRight);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_Z, m_Sink);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_C, m_Rise);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_LSHIFT, m_Accelerator);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_Q, m_RollLeft);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_E, m_RollRight);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_R, m_RollReset);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SPACE, m_Reset);

	m_RollAngle = 0.0f;

	m_WorldMtx.Identity();
	m_InitPos.Set(0.0f, 0.0f, 0.0f);
	m_LookAtPos.Set(0.0f, 0.0f, 1.0f);
	m_WorldMtx.d.y = 10;
}
// ============================================================================
// ============================================================================
void dcamWarCam::Init(const Vector3 &camPos, const Vector3 &lookAtPos)
{
	m_InitPos.Set(camPos);
	m_LookAtPos.Set(lookAtPos);
	Reset();
}
// ============================================================================
// ============================================================================
void dcamWarCam::Reset()
{
	m_WorldMtx.Identity();
	m_WorldMtx.d.Set(m_InitPos);
	Vector3 look(m_LookAtPos);
	////look.y = m_InitPos.y;	// Have to zero out y delta due to how this camera works
	m_WorldMtx.LookAt(look, m_vUp);
}
// ============================================================================
// ============================================================================
void dcamWarCam::Frame(const spdSphere& frameSphere)
{
	Vector3 lookAtPos = VEC3V_TO_VECTOR3(frameSphere.GetCenter());
	Vector3 cameraPos = lookAtPos;
	cameraPos.AddScaled(m_WorldMtx.c, frameSphere.GetRadius().Getf()*2.0f);

	m_WorldMtx.d.Set(cameraPos);
	Vector3 look(lookAtPos);
	////look.y = m_InitPos.y;	// Have to zero out y delta due to how this camera works
	m_WorldMtx.LookAt(look, m_vUp);
}
// ============================================================================
// ============================================================================
bool dcamWarCam::Update(float elapsed)
{
	m_Mapper.Update();

	const Vector3 oriRightV3(   1.0f,  0.0f,  0.0f);
	const Vector3 oriUpV3(      0.0f,  1.0f,  0.0f);
	const Vector3 oriForwardV3( 0.0f,  0.0f, -1.0f);

	const int MAT_FORWARD = 2;
	const int MAT_RIGHT = 0;
	const int MAT_UP = 1;

	bool bReceivedInput = ( m_Yaw.GetNorm(fYawDeadzone) != 0.0f || m_Pitch.GetNorm(fPitchDeadzone) != 0.0f ||
							m_Forward.GetValue() > 0 || (m_Reverse.GetValue() > 0 && m_Roll.GetValue() == 0) ||
							m_StrafeLeft.GetValue() > 0 || m_StrafeRight.GetValue() > 0 || m_Roll.GetValue() > 0 ||
							m_RollRight.GetValue() > 0 || m_RollLeft.GetValue() > 0 || m_RollReset.GetValue() > 0 ||
							m_Sink.GetValue() > 0 || m_Rise.GetValue() > 0 );
	bool bIsRolling = ( (m_Roll.GetValue() > 0 && (m_Forward.GetValue() > 0 || m_StrafeLeft.GetValue() > 0 || m_StrafeRight.GetValue() > 0)) ||
						(m_RollRight.GetValue() > 0 || m_RollLeft.GetValue() > 0 || m_RollReset.GetValue() > 0) );
	bool bCanUseMouse = true;

	Vector3 posUpV3 = oriUpV3;
	if (GetUseZUp())
	{
		posUpV3.Set( 0.0f,  0.0f,  1.0f);
	}

	Vector3    position;
	Quaternion orientation;

	orientation.FromMatrix34(m_WorldMtx);
	position.Set(m_WorldMtx.d);

	if (m_Reset.GetValue() > 0)
	{
		m_RollAngle = 0.0f;
		Reset();
		return true;
	}

#if defined(USE_XINPUT)
	float	fYawDelta   = -m_Yaw.GetNorm(fYawDeadzone) * elapsed * fYawRate;
	float	fPitchDelta = -m_Pitch.GetNorm(fPitchDeadzone) * elapsed * fPitchRate;
#else
	float	fYawDelta   = m_Yaw.GetNorm(fYawDeadzone) * elapsed * fYawRate;
	float	fPitchDelta = m_Pitch.GetNorm(fPitchDeadzone) * elapsed * fPitchRate;
#endif

	float	fThrust = 0.0f;
	float	fStrafe = 0.0f;
	float   fVertical = 0.0f;

	bool bRollReset = false;
	if (m_Roll.GetValue() > 0 || m_RollReset.GetValue() > 0)
	{
		if (m_Forward.GetValue() > 0 || m_RollReset.GetValue() > 0)
		{
			Vector3 vCameraForward(m_WorldMtx.GetVector(MAT_FORWARD));
			vCameraForward.Normalize();		// just to be safe

			// Reset camera roll when DPAD UP is pressed, while Roll button is down.
			bRollReset = true;

			// Undo roll.
			Quaternion dfRoll;
			dfRoll.FromRotation(oriForwardV3, -m_RollAngle);
			orientation.Multiply(dfRoll);

			m_WorldMtx.FromQuaternion(orientation);
			m_WorldMtx.NormalizeSafe();

			m_RollAngle = 0.0f;
		}
	}
	else
	{
		// If Roll button is up, DPAD controls thrust and strafe.
		fThrust = (float)(m_Reverse.GetValue() - m_Forward.GetValue()) * (1.0f/255.0f) * elapsed * fFwdSpeed;
		fVertical = (float)(m_Rise.GetValue() - m_Sink.GetValue()) * (1.0f/255.0f) * elapsed * fVerticalSpeed;
		fStrafe = (float)(m_StrafeRight.GetValue() - m_StrafeLeft.GetValue()) * (1.0f/255.0f) * elapsed * fSideSpeed;
	}

	// Mouse support.
	if (bCanUseMouse)
	{
		float x = ioMouse::GetNormX();
		float y = ioMouse::GetNormY();

		bool move = false; //(ioMouse::GetButtons() & ioMouse::MOUSE_LEFT) != 0;
		// bool accel = false; //((ioMouse::GetButtons() & ioMouse::MOUSE_RIGHT) && (ioMouse::GetButtons() & ioMouse::MOUSE_LEFT)) != 0;
		bool rotate = (ioMouse::GetButtons() & ioMouse::MOUSE_RIGHT) != 0;
		bool reset = false; //(ioMouse::GetButtons() & ioMouse::MOUSE_MIDDLE) != 0;

		if (reset)
		{
			m_RollAngle = 0.0f;
			Reset();
			return false;
		}

		const float XAxisThreshold = 0.20f;
		const float YAxisThreshold = 0.20f;

		float fForward = 0.0f;
		float fReverse = 0.0f;
		float fLeft = 0.0f;
		float fRight = 0.0f;
		if (x < XAxisThreshold)
		{
			fLeft = (XAxisThreshold - x) / XAxisThreshold;
		}
		else if (x > (1.0f - XAxisThreshold))
		{
			fRight = (x - 1.0f + XAxisThreshold) / XAxisThreshold;
		}

		if (y < YAxisThreshold)
		{
			fForward = (YAxisThreshold - y) / YAxisThreshold;
		}
		else if (y > (1.0f - YAxisThreshold))
		{
			fReverse = (y - 1.0f + YAxisThreshold) / YAxisThreshold;
		}

		if (move)
		{
#if 0		// SetCurrentValue is private now, not sure anybody is even using this class any longer.
			if (accel)
			{
				m_Accelerator.SetCurrentValue(255); // (255, 0);
			}
#endif

			fThrust = (fReverse - fForward) * elapsed * fFwdSpeed;
			fStrafe = (fRight - fLeft) * elapsed * fSideSpeed;
		}
		else if (rotate)
		{
			fYawDelta   = (fLeft - fRight) * elapsed * fYawRate;
			fPitchDelta = (fForward - fReverse) * elapsed * fPitchRate;
		}
	}

	// -------------------------------------------
	//  Orientation. (apply pitch and yaw, roll is applied later)
	// -------------------------------------------

	// Ignore orientation changes while camera is rolling.
	if (bIsRolling == false)
	{
		Vector3 vCameraRight(m_WorldMtx.GetVector(MAT_RIGHT));
		vCameraRight.Normalize();		// just to be safe

		Vector3 vCameraUp(m_WorldMtx.GetVector(MAT_UP));
		vCameraUp.Normalize();		// just to be safe

		Quaternion dfPitch;
		dfPitch.FromRotation(vCameraRight, fPitchDelta);

		Quaternion dfYaw;
		dfYaw.FromRotation(vCameraUp, fYawDelta);

		Quaternion rotation;
		rotation.Multiply(dfYaw, dfPitch);

		orientation.MultiplyFromLeft(rotation);
		m_WorldMtx.FromQuaternion(orientation);
		m_WorldMtx.NormalizeSafe();
	}

	// -------------------------------------------
	// Apply acceleration to thrust (forward, side (aka strafe) and vertical).
	// TODO: acceleration for yaw and pitch?
	if (m_Accelerator.GetValue() > 0)
	{
		// Unfortunately, mapping to an analog button does not give analog values.
		// (i.e. m_Accelerator is either 0 or 255)
		float fAnalogAcceleration = fMaxAcceleration * ((float)m_Accelerator.GetValue() / 255.0f);
		fThrust *= 1.0f + fAnalogAcceleration;
		fStrafe *= 1.0f + fAnalogAcceleration;
		fVertical *= 1.0f + fAnalogAcceleration;
	}


	// -------------------------------------------
	//  Position.
	// -------------------------------------------

	// Apply thrust to the _new_ orientation.
	Vector3 delta;
	delta.Scale(m_WorldMtx.GetVector(MAT_FORWARD), fThrust);
	position.Add(delta);

	// Apply strafe to the _new_ orientation.
	delta.Scale(m_WorldMtx.GetVector(MAT_RIGHT), fStrafe);
	position.Add(delta);

	// Apply vertical movement. (relative to camera's up)
	//position.AddScaled(posUpV3, fVertical);		// this is relative to World Up
	delta.Scale(m_WorldMtx.GetVector(MAT_UP), fVertical);
	position.Add(delta);

	m_WorldMtx.d.Set(position);


	// -------------------------------------------
	//  Roll.
	// -------------------------------------------

	if ( bRollReset == false )
	{
		if (m_Roll.GetValue() > 0 || m_RollRight.GetValue() > 0 || m_RollLeft.GetValue() > 0)
		{
			float fRollInput = 0.0f;
			if (m_StrafeRight.GetValue() > 0 || m_StrafeLeft.GetValue() > 0)
			{
				fRollInput = (float)(m_StrafeRight.GetValue() - m_StrafeLeft.GetValue());
			}
			else if (m_RollRight.GetValue() > 0 || m_RollLeft.GetValue() > 0)
			{
				fRollInput = (float)(m_RollRight.GetValue() - m_RollLeft.GetValue());
			}

			if (fRollInput != 0.0f)
			{
				float fCameraRollDelta = fRollInput * (1.0f/255.0f);
				fCameraRollDelta = fCameraRollDelta * elapsed * fCameraRollRate;
				m_RollAngle += fCameraRollDelta;

				// Roll camera about forward vector, 
				Quaternion dfRoll;
				dfRoll.FromRotation(oriForwardV3, fCameraRollDelta);
				orientation.Multiply(dfRoll);

				m_WorldMtx.FromQuaternion(orientation);
				m_WorldMtx.NormalizeSafe();
			}
		}
	}

	return (bReceivedInput);
}
// ============================================================================
// ============================================================================
void dcamWarCam::Draw(int scrX, int scrY, float alpha) const
{
	// show instructions on screen:
#if !__CONSOLE
	// reset:
	scrY=DrawLine(scrX, scrY, "SPACE resets the initial position", Color32(1.f,1.f,1.f,alpha));

	// rotate:
	scrY=DrawLine(scrX, scrY, "Rotate using mouse while RMB is down", Color32(1.f,1.f,1.f,alpha));

	// acceleration:
	scrY=DrawLine(scrX, scrY, "Hold Left SHIFT to move 4x faster", Color32(1.f,1.f,1.f,alpha));

	// roll:
	scrY=DrawLine(scrX, scrY, "Roll using Q/E, R to clear", Color32(1.f,1.f,1.f,alpha));

	// translate:
	scrY=DrawLine(scrX, scrY, "Move using W A S D", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Move up/down C/Z", Color32(1.f,1.f,1.f,alpha));

#else //!__CONSOLE
	// rotate:
	scrY=DrawLine(scrX, scrY, "RIGHT ANALOG x-axis: yaw and y-axis: pitch", Color32(1.f,1.f,1.f,alpha));

	scrY=DrawLine(scrX, scrY, "R1 moves the camera up and L1 moves down", Color32(1.f,1.f,1.f,alpha));

	if (m_Roll.GetValue() == 0)
	{
		// translate:
		scrY=DrawLine(scrX, scrY, "DPAD moves camera, forward/back/right/left", Color32(1.f,1.f,1.f,alpha));
		scrY=DrawLine(scrX, scrY, "Holding R2 increases movement speed 4x", Color32(1.f,1.f,1.f,alpha));
		scrY=DrawLine(scrX, scrY, "Hold L2 for roll controls", Color32(1.f,1.f,1.f,alpha));
	}
	else
	{
		// roll:
		scrY=DrawLine(scrX, scrY, "Hold L2 to roll camera via DPAD LEFT/RIGHT", Color32(1.f,1.f,1.f,alpha));
		scrY=DrawLine(scrX, scrY, " DPAD UP clears camera roll (reset)", Color32(1.f,1.f,1.f,alpha));
		scrY=DrawLine(scrX, scrY, "Release L2 for translation control", Color32(1.f,1.f,1.f,alpha));
	}

	// reset:
	scrY=DrawLine(scrX, scrY, "L3 resets the initial position", Color32(1.f,1.f,1.f,alpha));
#endif //!__CONSOLE
}
