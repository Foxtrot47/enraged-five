//
// devcam/mrkabcam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mrkabcam.h"
#include "input/pad.h"
#include "vector/quaternion.h"
#include "bank/bank.h"

#if !__FINAL
using namespace rage;

dcamMrkABCam::dcamMrkABCam()
{
	m_A.Identity();
	m_B.Identity();
	m_AIsSet = false;
	m_DoTransition = false;
	m_AtoB = false; //so first toggle of the system goes from A to B
	m_MaxTextDisplayTime = 1.0f;
	m_ATextTime = m_MaxTextDisplayTime;
	m_BTextTime = m_MaxTextDisplayTime;
	m_TransStep = 0.5f;
	m_CurrentTime = 0.0f;
	m_TotalTransitionTime = 2.0f;
	m_SlowIn = false;
	m_SlowOut = false;
	m_SlowInOut = false;
}

dcamMrkABCam::~dcamMrkABCam()
{

}

void dcamMrkABCam::DebugDraw(float scrX, float scrY)
{
	if (m_ATextTime < m_MaxTextDisplayTime && m_BTextTime < m_MaxTextDisplayTime)
	{
		//Display control widgets
		grcFont::GetCurrent().Drawf(scrX,scrY, "R*NY Marketing AB Camera: A and B points set");
	}
	else if (m_ATextTime < m_MaxTextDisplayTime)
	{
		//Display control widgets
		grcFont::GetCurrent().Drawf(scrX,scrY, "R*NY Marketing AB Camera: A point set");
	}
	else if (m_BTextTime < m_MaxTextDisplayTime)
	{
		//Display control widgets
		grcFont::GetCurrent().Drawf(scrX,scrY, "R*NY Marketing AB Camera: B point set");
	}

	dcamMrkCamBase::DebugDraw(scrX,scrY);
}

#if __BANK
void dcamMrkABCam::AddWidgets(class bkBank &bk)
{
	bk.AddSlider("Total Transition time (sec)", &m_TotalTransitionTime, 1, 120, 1);
	bk.AddToggle("Slow In", &m_SlowIn);
	bk.AddToggle("Slow Out", &m_SlowOut);
	bk.AddToggle("Slow In and Out", &m_SlowInOut);
	//dcamMrkCamBase::AddWidgets(bk);
}
#endif

bool dcamMrkABCam::Update(float dt)
{
	if (m_ATextTime < m_MaxTextDisplayTime && m_BTextTime < m_MaxTextDisplayTime)
	{
		m_ATextTime += dt;
		m_BTextTime += dt;
	}
	else if (m_ATextTime < m_MaxTextDisplayTime)
	{
		m_ATextTime += dt;
	}
	else if (m_BTextTime < m_MaxTextDisplayTime)
	{
		m_BTextTime += dt;
	}

	if (m_DoTransition)
	{
		UpdateTransition(dt);
	}
	return dcamMrkCamBase::Update(dt);
}

void dcamMrkABCam::SetSetFOVKeyCallback(const atDelegate< void (void) > & callback)
{
	m_SetFOVKeyCallback = callback;
}

void dcamMrkABCam::SetUpdateFOVTransitionCallback(const atDelegate< void (float step) > & callback)
{
	m_UpdateFOVTransitionCallback = callback;
}

void dcamMrkABCam::SetStartFOVTransitionCallback(const atDelegate< void (void) > & callback)
{
	m_StartFOVTransitionCallback = callback;
}

const char* dcamMrkABCam::GetName() const
{
	return "R*NY Marketing AB Camera";
}

void dcamMrkABCam::Reset()
{
	m_A.Identity();
	m_B.Identity();
	m_AIsSet = false;
	m_DoTransition = false;
	m_AtoB = false; //so first toggle of the system goes from A to B
	m_MaxTextDisplayTime = 1.0f;
	m_ATextTime = m_MaxTextDisplayTime;
	m_BTextTime = m_MaxTextDisplayTime;
	m_TransStep = 0.5f;
	m_CurrentTime = 0.0f;
	dcamMrkCamBase::Reset();
}

void dcamMrkABCam::DrawInfoText(float scrX, float scrY)
{
	static const char *texts[32] = {
		"Same as the R*NY Marketing Free-Cam, but with an A-B element.",
		"Uses gamepad(#2) to move the camera and do actions.",
		"",
		"Controls  XBox 360 Controller",
		"-----------------------------------",
		"(Start + L-Trigger) = Assign A or B coordinate",
		"(Start + R-Trigger) = Start A to B or B to A",
		"(Start + Directional Pad Up and Down) = Speed of camera between A and B",
		"",
		"",
		 "Inherited Controls XBox 360 Controller",
		 "-----------------------------------",
		 "(L3) = Pause time cycle",
		 "(Start Button) = Pause Game",
		 "(Back Button) = Switch HUD on and off",
		 "(Start + Y) = increase move speed by 0.001",
		 "(Start + A) = slow move speed by 0.001",
		 "(Start + X) = increase rot speed by 0.001",
		 "(Start + R3) = slow rot speed by 0.001",
		 "(X) = Moves Forward",
		 "(A) = Moves Backward",
		 "(Directional Pad R) = Advance Time",
		 "(Directional Pad L) = Weather Type",
		 "(Directional Pad Down) = Drop Player",
		 "(Directional Pad Up) = Step forward one frame (while paused)",
		 "(Left Bumper) = Roll camera.  x-y rotation of the frame",
		 "(Right Bumper) = Roll camera.  x-y rotation of the frame",
		 "(Left Trigger) = Decrease FOV",
		 "(Right Trigger) = Increase FOV",
		 "(Start + Left Bumper) = Widgets Menu",
		 "(Start + B) Toggles between Detached Cam and In-Game Cam",
		 "(Start + Right Bumper) cycles through all Cam modes."
	};

	int count = sizeof(texts)/sizeof(char*);
	int lineSize = 10;
	for(int i = 0; i < count; i++)
	{
		grcFont::GetCurrent().Drawf(scrX,scrY+(i*lineSize), texts[i]);
	}

	grcFont::GetCurrent().Drawf(scrX,scrY+(count*lineSize), "CurrentTime %f", m_CurrentTime);
	grcFont::GetCurrent().Drawf(scrX,scrY+((count+1)*lineSize), "TotalTime %f", m_TotalTransitionTime);
}

void dcamMrkABCam::HandleDebugButtons(const u32 dButtons)
{
	//(Start + L-Trigger) = Assign A or B coordinate
	if (dButtons & ioPad::L2)
	{
		if (!m_DoTransition) //not doing the transition
		{
			if (!m_AIsSet)
			{
				if (m_SetFOVKeyCallback.IsBound())
				{
					m_SetFOVKeyCallback();
				}
				m_A = GetWorldMtx();
				m_AIsSet = true;//we have set A
				m_ATextTime = 0.0f;
			}
			else
			{
				if (m_SetFOVKeyCallback.IsBound())
				{
					m_SetFOVKeyCallback();
				}
				m_B = GetWorldMtx();
				m_AIsSet = false;//we have set b so next press should set A again
				m_BTextTime = 0.0f;
			}
		}
		return; //so base class does not eval this
	}

	//(Start + R-Trigger) = Start A to B or B to A
	if (dButtons & ioPad::R2)
	{
		m_AtoB = !m_AtoB;
		if (!m_DoTransition)
		{
			if (m_AtoB)
			{
				SetWorldMtx(m_A);
			}
			else
			{
				SetWorldMtx(m_B);
			}
		}
		if (m_StartFOVTransitionCallback.IsBound())
		{
			m_StartFOVTransitionCallback();
		}
		m_CurrentTime = 0.0f;
		m_DoTransition = true;
		return; //so base class does not eval this
	}

	dcamMrkCamBase::HandleDebugButtons(dButtons);
}

void dcamMrkABCam::HandleHeldDebugButtons(const u32 dButtons)
{
	//(Start + Directional Pad Up and Down) = Speed of camera between A and B
	if (dButtons & ioPad::LUP)
	{
		m_MoveSpeedAugment += 0.05f;
		return; //so base class does not eval this
	}

	//(Start + Directional Pad Up and Down) = Speed of camera between A and B
	if (dButtons & ioPad::LDOWN)
	{
		m_MoveSpeedAugment -= 0.05f;
		if (m_MoveSpeedAugment < 0.01f)
		{
			m_MoveSpeedAugment = 0.01f;
		}
		return; //so base class does not eval this
	}

	dcamMrkCamBase::HandleHeldDebugButtons(dButtons);
}

void dcamMrkABCam::UpdateTransition(float dt)
{
	m_CurrentTime+=dt;
	float percentDone = (m_CurrentTime)/m_TotalTransitionTime;
	
	if (m_SlowIn)
	{
		percentDone = SlowIn(percentDone);
	}
	else if (m_SlowOut)
	{
		percentDone = SlowOut(percentDone);
	}
	else if (m_SlowInOut)
	{
		percentDone = SlowInOut(percentDone);
	}

	Matrix34 result;
	if (m_AtoB)
	{
		result.Interpolate(m_A,m_B,percentDone);
		if (percentDone>=1.0f)
		{
			m_DoTransition = false;
		}
	}
	else
	{
		result.Interpolate(m_B,m_A,percentDone);
		if (percentDone>=1.0f)
		{
			m_DoTransition = false;
		}
	}
	SetWorldMtx(result);

	if (m_UpdateFOVTransitionCallback.IsBound())
	{
		m_UpdateFOVTransitionCallback(percentDone);
	}
}
#endif //#if !__FINAL
