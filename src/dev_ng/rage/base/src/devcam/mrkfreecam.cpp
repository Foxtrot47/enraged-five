//
// devcam/mrkfreecam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mrkfreecam.h"
#include "input/pad.h"

#if !__FINAL
using namespace rage;

dcamMrkFreeCam::dcamMrkFreeCam()
{

}

dcamMrkFreeCam::~dcamMrkFreeCam()
{

}

const char* dcamMrkFreeCam::GetName() const
{
	return "R*NY Marketing Free Camera";
}

void dcamMrkFreeCam::SetScreenshotCallback(const atDelegate< void (void) > & callback)
{
	m_ScreenshotCallback = callback;
}

void dcamMrkFreeCam::SetCameraLightToggleCallback(const atDelegate< void (void) > & callback)
{
	m_CameraLightToggleCallback = callback;
}

void dcamMrkFreeCam::SetCameraLightUpdateCallback(const atDelegate< void (const Vector3 &position) > & callback)
{
	m_CameraLightUpdateCallback = callback;
}

void dcamMrkFreeCam::SetCameraLightIntensityCallback(const atDelegate< void (bool add) > & callback)
{
	m_CameraLightIntensityCallback = callback;
}

void dcamMrkFreeCam::SetCameraLightRadiusCallback(const atDelegate< void (bool add) > & callback)
{
	m_CameraLightRadiusCallback = callback;
}

void dcamMrkFreeCam::DrawInfoText(float scrX, float scrY)
{
	static const char* texts[] = {
	"Free camera for R*NY marketing.",
	"Uses gamepad(#2) to move the camera and do actions.",
	"",
	"Controls  XBox 360 Controller",
	"-----------------------------------",
	"(R3) = Screenshot",
	"(Y) = turn on/off camera light",
	"(Start + Directional Pad Up and Down) = camera light intensity add/subtract",
	"(Start + Directional Pad L and R) = camera light radius add/subtract",
	"",
	 "Inherited Controls XBox 360 Controller",
	 "-----------------------------------",	
	 "(Left Stick) = Pitch and Yaw like a 1st person shooter = Forward, Back, Left, and Right like a 1st person shooter",
	 "(Right Stick) = Forward, Back, Left, and Right like a 1st person shooter",
	 "(L3) = Pause time cycle",
	 "(Start Button) = Pause Game",
	 "(Back Button) = Switch HUD on and off",
	 "(Start + Y) = increase move speed by 0.001",
	 "(Start + A) = slow move speed by 0.001",
	 "(Start + X) = increase rot speed by 0.001",
	 "(Start + R3) = slow rot speed by 0.001",
	 "(X) = Moves Forward",
	 "(A) = Moves Backward",
	 "(Directional Pad R) = Advance Time",
	 "(Directional Pad L) = Weather Type",
	 "(Directional Pad Down) = Drop Player",
	 "(Directional Pad Up) = Step forward one frame (while paused)",
	 "(Left Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Right Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Left Trigger) = Decrease FOV",
	 "(Right Trigger) = Increase FOV",
	 "(Start + Left Bumper) = Widgets Menu",
	 "(Start + B) Toggles between Detached Cam and In-Game Cam",
	 "(Start + Right Bumper) cycles through all Cam modes."
	};

	int count = sizeof(texts)/sizeof(char*);
	int lineSize = 10;
	for(int i = 0; i < count; i++)
	{
		grcFont::GetCurrent().Drawf(scrX,scrY+(i*lineSize), texts[i]);
	}
}

void dcamMrkFreeCam::HandleButtons(const u32 buttons)
{
	//(R3) = Screenshot
	if (buttons & ioPad::R3)
	{
		//Screenshot call back
		if (m_ScreenshotCallback.IsBound())
		{
			m_ScreenshotCallback();
		}
		return; //so base class does not eval this
	}

	//(Y) = turn on/off camera light
	if (buttons & ioPad::RUP)
	{
		//light call back
		if (m_CameraLightToggleCallback.IsBound())
		{
			m_CameraLightToggleCallback();
		}
		return; //so base class does not eval this
	}

	dcamMrkCamBase::HandleButtons(buttons);
}

void dcamMrkFreeCam::HandleHeldDebugButtons(const u32 dButtons)
{
	//(Start + Directional Pad Up and Down) = camera light intensity add/subtract
	if (dButtons & ioPad::LUP)
	{
		if (m_CameraLightIntensityCallback.IsBound())
		{
			m_CameraLightIntensityCallback(true);
		}
		return; //so base class does not eval this
	}

	if (dButtons & ioPad::LDOWN)
	{
		if (m_CameraLightIntensityCallback.IsBound())
		{
			m_CameraLightIntensityCallback(false);
		}
		return; //so base class does not eval this
	}

	//(Start + Directional Pad L and R) = camera light radius add/subtract
	if (dButtons & ioPad::LLEFT)
	{
		if (m_CameraLightRadiusCallback.IsBound())
		{
			m_CameraLightRadiusCallback(true);
		}
		return; //so base class does not eval this
	}

	if (dButtons & ioPad::LRIGHT)
	{
		if (m_CameraLightRadiusCallback.IsBound())
		{
			m_CameraLightRadiusCallback(false);
		}
		return; //so base class does not eval this
	}

	dcamMrkCamBase::HandleHeldDebugButtons(dButtons);
}

bool dcamMrkFreeCam::Update(float dt)
{
	bool retval = dcamMrkCamBase::Update(dt);

	//light call back
	if (m_CameraLightUpdateCallback.IsBound())
	{
		m_CameraLightUpdateCallback(GetWorldMtx().d);
	}

	return retval;
}
#endif //#if !__FINAL
