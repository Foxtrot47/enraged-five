//
// devcam/warcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_WARCAM_H
#define DEVCAM_WARCAM_H

#include "input/mapper.h"
#include "vector/matrix44.h"
#include "devcam.h"

namespace rage {

class spdSphere;

// PURPOSE:
//	Based on Warriors Free camera, movement on dpad, 'look' on right analog.
//	Also does vertical movement and roll.
//
// NOTES:
//  * Controller *
// <TABLE>
//		Key                              Action
//		-------------------------------  ---------------------
//		rDown/rUp                        move forward/back
//		rLeft/rRight                     strafe
//		RightAnalog x-axis               yaw
//		RightAnalog y-axis               pitch
//		R1/L1                            raise/lower
//		hold down L2                     moves faster
//		hold down R2                     changes dpad to camera roll controls (roll left/right, up resets)
// </TABLE>
//
// NOTES
//	This camera requires a controller.
//	This camera supports the zup option.
// <FLAG Component>
//
class dcamWarCam : public dcamCam  
{
public:
	// PURPOSE: default constructor
	dcamWarCam();

	const char* GetName() const;

	bool Update(float dt);
	void Init(const Vector3 &camPos, const Vector3 &lookAtPos);
	void Reset();
	void Frame(const spdSphere& frameSphere);

	void Draw(int scrX, int scrY, float alpha) const;

	// PURPOSE: accessor for the camera's view matrix (inverse of the world matrix)
	// RETURNS: a reference to the camera's view matrix
	const Matrix44& GetViewMtx();

	const Matrix34& GetWorldMtx() const;

	void SetWorldMtx(const Matrix34 &mtx);

private:
	ioMapper m_Mapper;

	ioValue m_Yaw;
	ioValue m_Pitch;
	ioValue m_Forward;
	ioValue m_Reverse;
	ioValue m_StrafeLeft;
	ioValue m_StrafeRight;
	ioValue m_Sink;
	ioValue m_Rise;
	ioValue m_Accelerator;
	ioValue m_Roll;
	ioValue m_RollRight;
	ioValue m_RollLeft;
	ioValue m_RollReset;
	ioValue m_Reset;

	float   m_RollAngle;

	Vector3 m_InitPos;
	Vector3 m_LookAtPos;

	Matrix44 m_ViewMtx;
	Matrix34 m_WorldMtx;
};

inline const Matrix44& dcamWarCam::GetViewMtx()
{ 
	return m_ViewMtx; 
}

inline const Matrix34& dcamWarCam::GetWorldMtx() const
{
	return m_WorldMtx; 
}

inline void dcamWarCam::SetWorldMtx(const Matrix34 &mtx)
{
	m_WorldMtx.Set(mtx);

	Matrix34 temp;
	temp.FastInverse(m_WorldMtx);
	Convert(m_ViewMtx,temp);
}

inline const char* dcamWarCam::GetName() const 
{
	return "Warriors Cam";
}

}	// namespace rage

#endif // DEVCAM_FREECAM_H
