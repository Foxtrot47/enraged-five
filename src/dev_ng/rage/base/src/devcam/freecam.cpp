//
// devcam/freecam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "freecam.h"

#include "bank/bkmgr.h"
#include "input/input.h"
#include "spatialdata/sphere.h"
#include "vector/matrix34.h"


using namespace rage;

static float s_TranslateSpeed = 10.0f;
static float s_RotateSpeed = 2.0f;


void dcamFreeCam::InitInternal()
{
	m_WorldMtx.Identity();
	m_InitPos.Set(0.0f, 0.0f, 0.f);
	m_LookAtPos.Set(0.f, 0.f, 1.f );
}

void dcamFreeCam::Init(const Vector3 &camPos, const Vector3 &lookAtPos)
{
	m_InitPos.Set(camPos);
	m_LookAtPos.Set(lookAtPos);
	Reset();
}

void dcamFreeCam::Reset()
{
	m_WorldMtx.Identity();
	m_WorldMtx.d.Set(m_InitPos);
	// Have to zero out y delta due to how this camera works
	Vector3 look(m_LookAtPos);
	look.y = m_InitPos.y;
	m_WorldMtx.LookAt(look, m_vUp);
}

void dcamFreeCam::Frame(const spdSphere& frameSphere)
{
	Vector3 lookAtPos = VEC3V_TO_VECTOR3(frameSphere.GetCenter());
	Vector3 cameraPos = lookAtPos;
	cameraPos.AddScaled(m_WorldMtx.c, frameSphere.GetRadius().Getf()*2.0f);

	m_WorldMtx.d.Set(cameraPos);
	Vector3 look(lookAtPos);
	look.y = cameraPos.y;
	m_WorldMtx.LookAt(look, m_vUp);
}

void dcamFreeCam::Rotate(const Vector3 &rotate)
{
	Matrix34 rx, ry, rz;

	rx.MakeRotateUnitAxis(m_WorldMtx.a,rotate.x);
	ry.MakeRotateUnitAxis(m_WorldMtx.b,rotate.y);
	rz.MakeRotateUnitAxis(m_WorldMtx.c,rotate.z);

	m_WorldMtx.Dot3x3(rx);
	m_WorldMtx.Dot3x3(ry);
	m_WorldMtx.Dot3x3(rz);
}


bool dcamFreeCam::Update(float dt)
{
	Vector3 translate, rotate;

	translate.Zero();
	rotate.Zero();
#if !__CONSOLE
	if (ioKeyboard::KeyDown(KEY_NUMPADENTER))
	{
		translate.x += s_TranslateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_NUMPAD0))
	{
		translate.x -= s_TranslateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_NUMPAD7))
	{
		translate.y += s_TranslateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_NUMPAD1))
	{
		translate.y -= s_TranslateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_NUMPAD2))
	{
		translate.z += s_TranslateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_NUMPAD8))
	{
		translate.z -= s_TranslateSpeed;
	}

	if (ioKeyboard::KeyDown(KEY_E))
	{
		rotate.x += s_RotateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_D))
	{
		rotate.x -= s_RotateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_NUMPAD4))
	{
		rotate.y += s_RotateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_NUMPAD6))
	{
		rotate.y -= s_RotateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_S))
	{
		rotate.z += s_RotateSpeed;
	}
	if (ioKeyboard::KeyDown(KEY_F))
	{
		rotate.z -= s_RotateSpeed;
	}

	if (ioKeyboard::KeyDown(KEY_CONTROL))
	{
		translate.Scale(2);
	}
#else
	const int padIndex = 0;
	ioPad & pad = ioPad::GetPad(padIndex);

	if (pad.GetButtons() & ioPad::RRIGHT)
	{
		translate.x += s_TranslateSpeed;
	}
	if (pad.GetButtons() & ioPad::RLEFT)
	{
		translate.x -= s_TranslateSpeed;
	}
#if __BANK
	if (!BANKMGR.IsUsingPad(padIndex))
#endif
	{
		if (pad.GetButtons() & ioPad::LUP)
		{
			translate.y += s_TranslateSpeed;
		}
		if (pad.GetButtons() & ioPad::LDOWN)
		{
			translate.y -= s_TranslateSpeed;
		}
	}

	if (pad.GetButtons() & ioPad::RUP)
	{
		translate.z += s_TranslateSpeed;
	}
	if (pad.GetButtons() & ioPad::RDOWN)
	{
		translate.z -= s_TranslateSpeed;
	}

#if __BANK
	if (!BANKMGR.IsUsingPad(padIndex))
#endif
	{
		if (pad.GetButtons() & ioPad::LLEFT)
		{
			rotate.y += s_RotateSpeed;
		}
		if (pad.GetButtons() & ioPad::LRIGHT)
		{
			rotate.y -= s_RotateSpeed;
		}
	}

	if (pad.GetButtons() & ioPad::L1)
	{
		rotate.z += s_RotateSpeed;
	}
	if (pad.GetButtons() & ioPad::R1)
	{
		rotate.z -= s_RotateSpeed;
	}

	if (pad.GetButtons() & ioPad::L2)
	{
		rotate.x += s_RotateSpeed;
	}
	if (pad.GetButtons() & ioPad::R2)
	{
		rotate.x -= s_RotateSpeed;
	}

	if (pad.GetButtons() & ioPad::START)
	{
		m_WorldMtx.Identity3x3();
	}
#endif
	translate.Scale(dt);
	rotate.Scale(dt);

	Rotate(rotate);

	m_WorldMtx.d.AddScaled(m_WorldMtx.a,translate.x);
	m_WorldMtx.d.AddScaled(m_WorldMtx.b,translate.y);
	m_WorldMtx.d.AddScaled(m_WorldMtx.c,translate.z);

	Matrix34 temp;
	temp.FastInverse(m_WorldMtx);
	Convert(m_ViewMtx,temp);

	return translate.Mag2() != 0.0f || rotate.Mag2() != 0.0f;
}

void dcamFreeCam::SetSpeed(float t,float r)
{
	s_TranslateSpeed = t;
	s_RotateSpeed = r;
}

void dcamFreeCam::Draw(int scrX, int scrY, float alpha) const
{
	// show instructions on screen:
#if !__CONSOLE
	// translate:
	scrY=DrawLine(scrX, scrY, "ENTER & NUMPAD 0: translate +/- x", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "NUMPAD 7 & NUMPAD 1: translate +/- y", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "NUMPAD 2 & NUMPAD 8: translate +/- z", Color32(1.f,1.f,1.f,alpha));

	// rotate:
	scrY=DrawLine(scrX, scrY, "E & D: rotate +/- x", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "NUMPAD 4 & NUMPAD 6: rotate +/- y", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "S & F: rotate +/- z", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "- Hold Down CTRL for 2x translate speed", Color32(1.f,1.f,1.f,alpha));
#else
	// translate:
	scrY=DrawLine(scrX, scrY, "rRight and rLeft: translate +/- x", Color32(1.f,1.f,1.f,alpha));
#if __BANK
	const int padIndex = 0;
	if (!BANKMGR.IsUsingPad(padIndex))
	{
		scrY=DrawLine(scrX, scrY, "lUp and lDown: translate +/- y", Color32(1.f,1.f,1.f,alpha));
	}
#endif
	scrY=DrawLine(scrX, scrY, "rUp and rDown: translate +/- z", Color32(1.f,1.f,1.f,alpha));

	// rotate:
	scrY=DrawLine(scrX, scrY, "L2 and R2: rotate +/- x", Color32(1.f,1.f,1.f,alpha));
#if __BANK
	if (!BANKMGR.IsUsingPad(padIndex))
	{
		scrY=DrawLine(scrX, scrY, "lLeft and lRight: rotate +/- y", Color32(1.f,1.f,1.f,alpha));
	}
#endif
	scrY=DrawLine(scrX, scrY, "L1 and R1: rotate +/- z", Color32(1.f,1.f,1.f,alpha));

	scrY=DrawLine(scrX, scrY, "Start: reset camera", Color32(1.f,1.f,1.f,alpha));
#endif
}



////////////////////////////////////////////////////////////////////////
// devFreecam2

// Allow stationary view.
const float	PS2_JOY_THRESH=0.25f;

dcamFreeCam2::dcamFreeCam2()
{
	m_Orientation = 0;
	m_Elevation = 0;
	m_WorldMtx.Identity();
	m_WorldMtx.d.y = 10;
}

bool dcamFreeCam2::Update(float elapsed)
{
	bool moved = false;

#if	!__CONSOLE

	float speed = ioKeyboard::KeyDown(KEY_SHIFT) ? 3.0f : 1.0f;

	if (ioKeyboard::KeyDown(KEY_A))
	{
		m_WorldMtx.d.y += 4.5f*elapsed*speed;
		moved = true;
	}
	if (ioKeyboard::KeyDown(KEY_Z))
	{
		m_WorldMtx.d.y -= 4.5f*elapsed*speed;
		moved = true;
	}

	if (ioKeyboard::KeyDown(KEY_LEFT) || ioKeyboard::KeyDown(KEY_NUMPAD4))
	{
		m_Orientation += 0.6f*elapsed*speed;
		moved = true;
	}
	if (ioKeyboard::KeyDown(KEY_RIGHT) || ioKeyboard::KeyDown(KEY_NUMPAD6))
	{
		m_Orientation -= 0.6f*elapsed*speed;
		moved = true;
	}

	if (ioKeyboard::KeyDown(KEY_PAGEUP) || ioKeyboard::KeyDown(KEY_NUMPAD9))
	{
		m_Elevation += 0.6f*elapsed*speed;
		moved = true;
	}
	if (ioKeyboard::KeyDown(KEY_PAGEDOWN) || ioKeyboard::KeyDown(KEY_NUMPAD3))
	{
		m_Elevation -= 0.6f*elapsed*speed;
		moved = true;
	}

	if (ioKeyboard::KeyDown(KEY_NUMPAD5))
	{
		m_Elevation = 0;
		moved = true;
	}
	if (ioKeyboard::KeyDown(KEY_HOME) || ioKeyboard::KeyDown(KEY_NUMPAD7))
	{
		m_Orientation = 0;
		moved = true;
	}

	// Get z-vector of matrix, and take out y component so we always
	// traverse x/z plane.
	Vector3 direction = m_WorldMtx.c;
	direction.y = 0;
	direction.Normalize();
	direction *= 12.0f*elapsed*speed;
	if (ioKeyboard::KeyDown(KEY_UP) || ioKeyboard::KeyDown(KEY_NUMPAD8))
	{
		m_WorldMtx.d -= direction;
		moved = true;
	}
	if (ioKeyboard::KeyDown(KEY_DOWN) || ioKeyboard::KeyDown(KEY_NUMPAD2))
	{
		m_WorldMtx.d += direction;
		moved = true;
	}
	// Similarly for strafe motion.
	direction.Cross(Vector3(0, 1, 0));
	if (ioKeyboard::KeyDown(KEY_PERIOD))
	{
		m_WorldMtx.d -= direction;
		moved = true;
	}
	if (ioKeyboard::KeyDown(KEY_COMMA))
	{
		m_WorldMtx.d += direction;
		moved = true;
	}

#else //!__CONSOLE
	unsigned int butt = ioPad::GetPad(0).GetButtons();
	float speed = (butt & ioPad::L3) ? 3.0f : 1.0f;

	// Note: analog rotations not subject to speed turbo (too
	// wild and funky).
	// Left analog stick = left/right rotate.
	float leftright = ioPad::GetPad(0).GetNormLeftX();
	if (fabsf(leftright) > PS2_JOY_THRESH)
	{
		m_Orientation -= 0.8f*leftright*elapsed;//*speed;
		moved = true;
	}

	// Left shoulders tilt up/down.
	if (butt & ioPad::L1)
	{
		m_Elevation += 0.6f*elapsed;//*speed;
		moved = true;
	}
	if (butt & ioPad::L2)
	{
		m_Elevation -= 0.6f*elapsed;//*speed;
		moved = true;
	}

	// Right shoulders traverse up/down.
	if (butt & ioPad::R1)
	{
		m_WorldMtx.d.y += 4.5f*elapsed*speed;
		moved = true;
	}
	if (butt & ioPad::R2)
	{
		m_WorldMtx.d.y -= 4.5f*elapsed*speed;
		moved = true;
	}

	// Forward motion.
	float forward = ioPad::GetPad(0).GetNormLeftY();
	if (butt & ioPad::RUP)			// X/triangle backwards forwards.
	{
		forward += 1;
		moved = true;
	}
	if (butt & ioPad::RDOWN)
	{
		forward -= 1;
		moved = true;
	}

	if (fabsf(forward) > PS2_JOY_THRESH)
	{
		// Flatten dead zone of joystick.
		if (forward < -PS2_JOY_THRESH)
		{
			forward += PS2_JOY_THRESH;
			moved = true;
		}
		else if (forward > PS2_JOY_THRESH)
		{
			forward -= PS2_JOY_THRESH;
			moved = true;
		}

		Vector3 direction = m_WorldMtx.c;
		direction.y = 0;
		direction.Normalize();
		direction *= 12.0f*forward*elapsed*speed;
		m_WorldMtx.d += direction;
	}

	// Strafe motion.
	float sideways = 0;
	if (butt & ioPad::RLEFT)		// square/circle
	{
		sideways += 1;
	}
	if (butt & ioPad::RRIGHT)
	{
		sideways -= 1;
	}
	if (sideways)
	{
		moved = true;
		Vector3 direction = m_WorldMtx.c;
		direction.y = 0;
		direction.Normalize();
		direction.Cross(Vector3(0, 1, 0));
		direction *= 12.0f*sideways*elapsed*speed;
		m_WorldMtx.d += direction;
	}

#endif //!__CONSOLE

	// Keep nice clean limits.
	while (m_Orientation < 0)
		m_Orientation += 2*PI;
	while (m_Orientation > 2*PI)
		m_Orientation -= 2*PI;

	// Dont let elevation quite reach PI/2, so that surface
	// heading doesn't flip into reverse at extreme view.
	if (m_Elevation < -PI/2 + 0.00001f)
		m_Elevation = -PI/2 + 0.00001f;
	if (m_Elevation > PI/2 - 0.00001f)
		m_Elevation = PI/2 - 0.00001f;

	m_WorldMtx.Identity3x3();
	m_WorldMtx.RotateX(m_Elevation);
	m_WorldMtx.RotateY(m_Orientation);

	return moved;
}

void dcamFreeCam2::Draw(int scrX, int scrY, float alpha) const
{
	// show instructions on screen:
#if !__CONSOLE
	// translate:
	scrY=DrawLine(scrX, scrY, "LEFT ARROW & RIGHT ARROW/NUMPAD 8 & NUMPAD 2: translate +/- z", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, ". & ,: strafe +/- y", Color32(1.f,1.f,1.f,alpha));

	// rotate:
	scrY=DrawLine(scrX, scrY, "PAGE UP & PAGE DOWN/NUMPAD 9 & NUMPAD 3: rotate +/- x", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "LEFT ARROW & RIGHT ARROW/NUMPAD 4 & NUMPAD 6: rotate +/- y", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "S & F: rotate +/- z", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "- Hold Down SHIFT for 3x translate speed", Color32(1.f,1.f,1.f,alpha));

	// 
	scrY=DrawLine(scrX, scrY, "NUMPAD 5: reset rotate y", Color32(1.f,1.f,1.f,alpha));
#else //!__CONSOLE
	// translate:
	scrY=DrawLine(scrX, scrY, "R1 & R2: translate +/- y", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "rLeft & rRight: translate +/- z", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "rUp & rDown & Right Analog Up/Down: translate +/- z", Color32(1.f,1.f,1.f,alpha));

	// rotate:
	scrY=DrawLine(scrX, scrY, "L1 & L2: rotate +/- x", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Left Analog Left/Right: rotate +/- y", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Hold Down L3 for 3x translate speed", Color32(1.f,1.f,1.f,alpha));
#endif //!__CONSOLE
}
