// 
// devcam/mrkcambase.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "mrkcambase.h"
#include "input/pad.h"
#include "bank/bank.h"

#if !__FINAL
using namespace rage;

dcamMrkCamBase::dcamMrkCamBase()
{
	m_WorldMtx.Identity();
	m_dt = 0;
	m_MoveAmount = 4.0f;
	m_RollAmount = 0.5f;
	m_MoveSpeedAugment = 1.0f;
	m_RotSpeedAugment = 1.0f;
	m_DrawInfo = false;
	m_CurRollDiff = 0.0f;
}

dcamMrkCamBase::~dcamMrkCamBase()
{

}

void dcamMrkCamBase::Init(const Vector3 &/*cameraPos*/, const Vector3 &/*lookAtPos*/)
{
	m_WorldMtx.Identity();
	m_dt = 0;
	m_MoveAmount = 4.0f;
	m_RollAmount = 0.5f;
	m_MoveSpeedAugment = 1.0f;
	m_RotSpeedAugment = 1.0f;
	m_DrawInfo = false;
	m_CurRollDiff = 0.0f;
}

bool dcamMrkCamBase::Update(float dt)
{
	m_dt = dt;
	//on the second controller
	const float deadZone = 0.23f;	// MAGIC!
	Vector2 lStick(rage::ioAddDeadZone(ioPad::GetPad(1).GetNormLeftX(),deadZone), rage::ioAddDeadZone(ioPad::GetPad(1).GetNormLeftY(),deadZone));
	Vector2 rStick(rage::ioAddDeadZone(ioPad::GetPad(1).GetNormRightX(),deadZone), rage::ioAddDeadZone(ioPad::GetPad(1).GetNormRightY(),deadZone));
	
	const float triggerDeadZone = 0.23f;	// MAGIC!
	float lTrigger = rage::ioAddDeadZone(ioPad::GetPad(1).GetNormAnalogButton(ioPad::L2_INDEX),triggerDeadZone);
	float rTrigger = rage::ioAddDeadZone(ioPad::GetPad(1).GetNormAnalogButton(ioPad::R2_INDEX),triggerDeadZone);

	if (lStick.IsNonZero())
	{
		HandleLStick(lStick * m_dt);
	}

	if (rStick.IsNonZero())
	{
		HandleRStick(rStick * m_dt);
	}

	HandleHeldButtons(ioPad::GetPad(1).GetButtons());
	HandleHeldDebugButtons(ioPad::GetPad(1).GetDebugButtons());
	HandleButtons(ioPad::GetPad(1).GetPressedButtons());
	HandleDebugButtons(ioPad::GetPad(1).GetPressedDebugButtons());
	HandleTriggers(lTrigger, rTrigger);

	return false;
}

void dcamMrkCamBase::Draw(int /*scrX*/, int /*scrY*/, float /*alpha*/) const
{
	// Do Nothing ...
}

void dcamMrkCamBase::DebugDraw(float scrX, float scrY)
{
	if(m_DrawInfo)
	{
		//Display control widgets
		DrawInfoText(scrX,scrY);
	}
}

#if __BANK
void dcamMrkCamBase::AddWidgets(class bkBank &bk)
{
	bk.AddSlider("Movement Speed", &m_MoveSpeedAugment, 0.001f, 20.0f, 0.001f);
	bk.AddSlider("Roll Speed", &m_RotSpeedAugment, 0.001f, 20.0f, 0.001f);
	dcamCam::AddWidgets(bk);
}
#endif

void dcamMrkCamBase::Reset()
{
	m_WorldMtx.Identity();
	m_dt = 0;
}

const char* dcamMrkCamBase::GetName() const
{
	return "R*NY Marketing Camera Base";
}

const Matrix34 & dcamMrkCamBase::GetWorldMtx() const
{
	return m_WorldMtx;
}

void dcamMrkCamBase::SetWorldMtx(const Matrix34 &mtx)
{
	m_WorldMtx.Set(mtx);
}

void dcamMrkCamBase::Frame(const spdSphere& /*frameSphere*/)
{
	//Do nothing
}

void dcamMrkCamBase::SetHUDToggleCallback(const atDelegate< void (void) > & callback)
{
	m_HUDToggleCallback = callback;
}

void dcamMrkCamBase::SetStepTimeCallback(const atDelegate< void (void) > & callback)
{
	m_StepTimeCallback = callback;
}

void dcamMrkCamBase::SetDropPlayerCallback(const atDelegate< void (void) > & callback)
{
	m_DropPlayerCallback = callback;
}

void dcamMrkCamBase::SetAdvanceTimeCallback(const atDelegate< void (void) > & callback)
{
	m_AdvanceTimeCallback = callback;
}

void dcamMrkCamBase::SetCycleWeatherCallback(const atDelegate< void (void) > & callback)
{
	m_CycleWeatherCallback = callback;
}

void dcamMrkCamBase::SetPauseGameCallback(const atDelegate< void (void) > & callback)
{
	m_PauseGameCallback = callback;
}

void dcamMrkCamBase::SetToggleTimeCycleCallback(const atDelegate< void (void) > & callback)
{
	m_ToggleTimeCycleCallback = callback;
}

void dcamMrkCamBase::SetIncreaseFOVCallback(const atDelegate< void (float) > & callback)
{
	m_IncreaseFOVCallback = callback;
}

void dcamMrkCamBase::SetDecreaseFOVCallback(const atDelegate< void (float) > & callback)
{
	m_DecreaseFOVCallback = callback;
}

void dcamMrkCamBase::SetResetFOVCallback(const atDelegate< void (void) > & callback)
{
	m_ResetFOVCallback = callback;
}

void dcamMrkCamBase::DrawInfoText(float scrX, float scrY)
{
	static const char* texts[] = {
	"Base camera for R*NY marketing.",
	"Uses gamepad(#2) to move the camera and do actions.",
	"",
	"Controls XBox 360 Controller",
	"-----------------------------------",
	"(Left Stick) = Pitch and Yaw like a 1st person shooter = Forward, Back, Left, and Right like a 1st person shooter",
	"(Right Stick) = Forward, Back, Left, and Right like a 1st person shooter",
	"(L3) = Pause time cycle",
	"(Start Button) = Pause Game",
	"(Back Button) = Switch HUD on and off",
	 "(Start + Y) = increase move speed by 0.001",
	 "(Start + A) = slow move speed by 0.001",
	 "(Start + X) = increase rot speed by 0.001",
	 "(Start + R3) = slow rot speed by 0.001",
	 "(X) = Moves Forward",
	 "(A) = Moves Backward",
	 "(Directional Pad R) = Advance Time",
	 "(Directional Pad L) = Weather Type",
	 "(Directional Pad Down) = Drop Player",
	 "(Directional Pad Up) = Step forward one frame (while paused)",
	 "(Left Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Right Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Left Trigger) = Decrease FOV",
	 "(Right Trigger) = Increase FOV",
	 "(Start + Left Bumper) = Widgets Menu",
	 "(Start + B) Toggles between Detached Cam and In-Game Cam",
	 "(Start + Right Bumper) cycles through all Cam modes."
	};

	int count = sizeof(texts)/sizeof(char*);
	int lineSize = 10;
	for(int i = 0; i < count; i++)
	{
		grcFont::GetCurrent().Drawf(scrX,scrY+(i*lineSize), texts[i]);
	}
}

void dcamMrkCamBase::HandleButtons(const u32 buttons)
{
	//(L3) = Pause time cycle
	if (buttons & ioPad::L3)
	{
		//Pause time cycle callback
		if (m_ToggleTimeCycleCallback.IsBound())
		{
			m_ToggleTimeCycleCallback();
		}
	}

	//(Start Button) = Pause Game
	if (buttons & ioPad::START)
	{
		//Pause game callback
		if (m_PauseGameCallback.IsBound())
		{
			m_PauseGameCallback();
		}
	}

	//(Back Button) = Switch HUD on and off
	if (buttons & ioPad::SELECT)
	{
		//HUD toggle callback
		if (m_HUDToggleCallback.IsBound())
		{
			m_HUDToggleCallback();
		}
	}

	//(Directional Pad L) = Weather Type
	if (buttons & ioPad::LLEFT)
	{
		//Weather type callback
		if (m_CycleWeatherCallback.IsBound())
		{
			m_CycleWeatherCallback();
		}
	}

	//(Directional Pad Down) = Drop Player
	if (buttons & ioPad::LDOWN)
	{
		//Drop player callback
		if (m_DropPlayerCallback.IsBound())
		{
			m_DropPlayerCallback();
		}
	}

	//(Directional Pad Up) = Step forward one frame (while paused)
	if (buttons & ioPad::LUP)
	{
		//Time step while paused callback
		//step for (.1) sec
		if (m_StepTimeCallback.IsBound())
		{
			m_StepTimeCallback();
		}
	}
}

void dcamMrkCamBase::HandleDebugButtons(const u32 dButtons)
{
	//(Start + Y) = increase move speed by 0.001
	if (dButtons & ioPad::RUP)
	{
		m_MoveSpeedAugment += 0.001f;
	}

	//(Start + A) = slow move speed by 0.001
	if (dButtons & ioPad::RDOWN)
	{
		m_MoveSpeedAugment -= 0.001f;
		if (m_MoveSpeedAugment < 0.0f)
		{
			m_MoveSpeedAugment = 0.001f;
		}
	}

	//(Start + X) = increase rot speed by 0.001
	if (dButtons & ioPad::RLEFT)
	{
		m_RotSpeedAugment += 0.001f;
	}

	//(Start + R3) = slow rot speed by 0.001
	if (dButtons & ioPad::R3)
	{
		m_RotSpeedAugment -= 0.001f;
		if (m_RotSpeedAugment < 0.0f)
		{
			m_RotSpeedAugment = 0.001f;
		}
	}
}

void dcamMrkCamBase::HandleLStick(const Vector2& vectorXY)
{
	Vector3 temp = m_WorldMtx.c; //use forward
	temp.Scale((m_MoveAmount*vectorXY.y*m_MoveSpeedAugment));
	m_WorldMtx.Translate(temp);

	Vector3 up = m_WorldMtx.GetLocalUp();
	Vector3 right;
	right.Cross(up, m_WorldMtx.c);//use forward
	right.Normalize();
	right.Scale((m_MoveAmount*vectorXY.x*m_MoveSpeedAugment));
	m_WorldMtx.Translate(right);
}

void dcamMrkCamBase::HandleRStick(const Vector2& vectorXY)
{
	if (GetUseZUp())
	{
		//Z up world
		m_WorldMtx.RotateZ((-vectorXY.x)*m_RotSpeedAugment);
	}
	else
	{
		//Y up world
		m_WorldMtx.RotateY((-vectorXY.x)*m_RotSpeedAugment);
	}
	m_WorldMtx.RotateLocalX((-vectorXY.y)*m_RotSpeedAugment);
}

void dcamMrkCamBase::HandleHeldButtons(const u32 buttons)
{
	if (buttons & ioPad::R1 && buttons & ioPad::L1)
	{
		//Reset by rotating back to original position
		if (m_CurRollDiff != 0.0f)
		{
			m_WorldMtx.Rotate(m_WorldMtx.c,-(m_CurRollDiff));//use forward
		}
		m_CurRollDiff = 0.0f;
	}
	else
	{
	//(Left Bumper) = Roll camera.  x-y rotation of the frame
	if (buttons & ioPad::L1)
	{
		m_WorldMtx.Rotate(m_WorldMtx.c,(m_RollAmount * m_dt));//use forward
			m_CurRollDiff += (m_RollAmount * m_dt);
	}

	//(Right Bumper) = Roll camera.  x-y rotation of the frame
	if (buttons & ioPad::R1)
	{
		m_WorldMtx.Rotate(m_WorldMtx.c,-(m_RollAmount * m_dt));//use forward
			m_CurRollDiff -= (m_RollAmount * m_dt);
		}
	}

	//(X) = Moves Forward
	if (buttons & ioPad::RLEFT)
	{
		Vector3 temp = m_WorldMtx.c;//use forward
		temp.Scale(-(m_MoveAmount * m_dt*m_MoveSpeedAugment));
		m_WorldMtx.Translate(temp);
	}

	//(A) = Moves Backward
	if (buttons & ioPad::RDOWN)
	{
		Vector3 temp = m_WorldMtx.c;//use forward
		temp.Scale((m_MoveAmount * m_dt*m_MoveSpeedAugment));
		m_WorldMtx.Translate(temp);
	}

	//(Directional Pad R) = Advance Time
	if (buttons & ioPad::LRIGHT)
	{
		//Advance time callback
		if (m_AdvanceTimeCallback.IsBound())
		{
			m_AdvanceTimeCallback();
		}
	}
}

void dcamMrkCamBase::HandleHeldDebugButtons(const u32 dButtons)
{
	m_DrawInfo = false;
	//(Start + Left Bumper) = Widgets Menu
	if (dButtons & ioPad::L1)
	{
		m_DrawInfo = true;
	}
}

void dcamMrkCamBase::HandleTriggers(const float left, const float right)
{
	if (left > 0.01f && right > 0.01f)
	{
		//Reset FOV callback
		if (m_ResetFOVCallback.IsBound())
		{
			m_ResetFOVCallback();
		}
	}
	else
	{
		//(Left Trigger) = Decrease FOV
		if (left > 0.01f)
		{
			//Decrease FOV callback
			if (m_DecreaseFOVCallback.IsBound())
			{
				m_DecreaseFOVCallback(left);
			}
		}

		//(Right Trigger) = Increase FOV
		if (right > 0.01f)
		{
			//Increase FOV callback
			if (m_IncreaseFOVCallback.IsBound())
			{
				m_IncreaseFOVCallback(right);
			}
		}
	}
}

#endif //#if !__FINAL
