//
// devcam/mrkmancam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mrkmancam.h"
#include "input/pad.h"
#include "bank/bank.h"

#if !__FINAL
using namespace rage;

dcamMrkManCam::dcamMrkManCam()
{
	m_Locked = false;
	m_LookAtPos.Zero();
	m_Offset.Zero();
	m_OffsetAmount = 2.0f;
}

dcamMrkManCam::~dcamMrkManCam()
{

}

bool dcamMrkManCam::Update(float dt)
{
	UpdateLookAtPosition();

	//apply offset
	m_LookAtPos += m_Offset;

	Matrix34 current = GetWorldMtx();
	current.LookAt(m_LookAtPos);
	SetWorldMtx(current);

	return dcamMrkCamBase::Update(dt);
}

const char* dcamMrkManCam::GetName() const
{
	return "R*NY Marketing Camera Sticky Camera";
}

void dcamMrkManCam::Reset()
{
	m_Locked = false;
	m_LookAtPos.Zero();
	m_Offset.Zero();
	m_OffsetAmount = 2.0f;
	dcamMrkCamBase::Reset();
}

#if __BANK
void dcamMrkManCam::AddWidgets(class bkBank &bk)
{
	bk.AddVector("Focal Point Offset", &m_Offset, -999999.9f, 999999.9f, m_OffsetAmount);
	dcamMrkCamBase::AddWidgets(bk);
}
#endif

void dcamMrkManCam::SetLookAtCallback(const atDelegate< Vector3 (void) > & callback)
{
	m_LookAtCallback = callback;
}

void dcamMrkManCam::DrawInfoText(float scrX, float scrY)
{
	static const char* texts[] = {
	"Lockable tracking camera for R*NY marketing.",
	"Uses gamepad(#2) to move the camera and do actions.",
	"",
	"Controls  XBox 360 Controller",
	"-----------------------------------",
	"(Right Stick) = Zooms in and out of the players car and Pan Left (focal offset + X) and Right (focal offset - X)",
	"(Left Stick) = Rotates the cam azimuthally around the player",
	"(Start + Directional Pad L) = focal offset + Z ",
	"(Start + Directional Pad R) = focal offset - Z",
	"(Start + Directional Pad Up) = focal offset + Y ",
	 "(Start + Directional Pad Down) = focal offset - Y ",
	 "",
	 "Inherited Controls XBox 360 Controller",
	 "-----------------------------------",
	 "(L3) = Pause time cycle",
	 "(Start Button) = Pause Game",
	 "(Back Button) = Switch HUD on and off",
	 "(Start + Y) = increase move speed by 0.001",
	 "(Start + A) = slow move speed by 0.001",
	 "(Start + X) = increase rot speed by 0.001",
	 "(Start + R3) = slow rot speed by 0.001",
	 "(X) = Moves Forward",
	 "(A) = Moves Backward",
	 "(Directional Pad R) = Advance Time",
	 "(Directional Pad L) = Weather Type",
	 "(Directional Pad Down) = Drop Player",
	 "(Directional Pad Up) = Step forward one frame (while paused)",
	 "(Left Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Right Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Left Trigger) = Decrease FOV",
	 "(Right Trigger) = Increase FOV",
	 "(Start + Left Bumper) = Widgets Menu",
	 "(Start + B) Toggles between Detached Cam and In-Game Cam",
	 "(Start + Right Bumper) cycles through all Cam modes."
	};

	int count = sizeof(texts)/sizeof(char*);
	int lineSize = 10;
	for(int i = 0; i < count; i++)
	{
		grcFont::GetCurrent().Drawf(scrX,scrY+(i*lineSize), texts[i]);
	}
}

void dcamMrkManCam::HandleLStick(const Vector2& vectorXY)
{
	// (Left Stick) = Rotates the cam azimuthally around the player
	Matrix34 current = GetWorldMtx();

	//be at the origin
	Matrix34 temp = current;
	temp.d.Set(0.0f);//at the origin

	//rotate on the right axis by the amount
	temp.Rotate(current.b,(-vectorXY.x)*m_RotSpeedAugment);

	//rotate on the right axis by the amount
	temp.Rotate(current.a,(-vectorXY.y)*m_RotSpeedAugment);

	//get the radius i am to be away
	float dist = m_LookAtPos.Dist(current.d);

	//get my direction
	Vector3 forward = temp.c;

	//scale the forward by the distance we are to be at
	forward.Scale(dist);

	//get my final location
	Vector3 pos = m_LookAtPos + forward;

	//get the final matrix by looking at the look at position from our new camera position
	Matrix34 final;
	final.LookAt(pos,m_LookAtPos,YAXIS);

	SetWorldMtx(final);
}

void dcamMrkManCam::HandleRStick(const Vector2& vectorXY)
{
	// (Right Stick) = Zooms in and out from the player
	Matrix34 result = GetWorldMtx();
	Vector3 temp = result.c; //use forward
	temp.Scale((m_MoveAmount*vectorXY.y));
	result.Translate(temp);
	SetWorldMtx(result);

	// (Right Stick) = Pan Left and Right
	m_Offset.x += (m_OffsetAmount * vectorXY.x);
}

void dcamMrkManCam::HandleHeldDebugButtons(const u32 dButtons)
{
	//(Directional Pad L) = focal offset Z 
	if (dButtons & ioPad::LLEFT)
	{
		if (GetUseZUp())
		{
			m_Offset.y += (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.z += (m_OffsetAmount * m_dt);
		}
		return;
	}

	//(Directional Pad R) = focal offset Z
	if (dButtons & ioPad::LRIGHT)
	{
		if (GetUseZUp())
		{
			m_Offset.y -= (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.z -= (m_OffsetAmount * m_dt);
		}
		return;
	}

	//(Directional Pad U) = focal offset Y 
	if (dButtons & ioPad::LUP)
	{
		if (GetUseZUp())
		{
			m_Offset.z += (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.y += (m_OffsetAmount * m_dt);
		}
		return;
	}

	//(Directional Pad D) = focal offset Y 
	if (dButtons & ioPad::LDOWN)
	{
		if (GetUseZUp())
		{
			m_Offset.z -= (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.y -= (m_OffsetAmount * m_dt);
		}
		return;
	}

	dcamMrkCamBase::HandleHeldDebugButtons(dButtons);
}

void dcamMrkManCam::UpdateLookAtPosition()
{
	//use call back to get info from the game
	if (m_LookAtCallback.IsBound())
	{
		m_LookAtPos = m_LookAtCallback();
	}
}

#endif //#if !__FINAL
