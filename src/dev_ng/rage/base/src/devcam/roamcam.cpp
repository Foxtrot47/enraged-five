//
// devcam/roamcam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "roamcam.h"

#include "bank/bank.h"
#include "input/pad.h"


using namespace rage;


void dcamRoamCam::InternalInit() {
	m_WorldMtx.Identity();
	m_Interest.Zero();
	
	m_InitPos.Zero();
	m_LookAtPos.Zero();
	
	m_Mapper.Map(IOMS_PAD_AXIS,IOM_AXIS_RX,m_Heading);
	m_Mapper.Map(IOMS_PAD_AXIS,IOM_AXIS_RY,m_Pitch);
	m_Mapper.Map(IOMS_PAD_AXIS,IOM_AXIS_LX,m_Strafe);
	m_Mapper.Map(IOMS_PAD_AXIS,IOM_AXIS_LY,m_Forward);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON,ioPad::R2,m_Sink);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON,ioPad::R1,m_Rise);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON,ioPad::L2,m_Accelerator);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON,ioPad::L1,m_PolarLock);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON,ioPad::R3,m_ResetPitch);

	// This actually ends up making it un-inverted in practice.
	// GTA3 treats pitch as inverted, so we might as well too.
	// Pitch.MakeInverted();

	m_FastSpeed = 200;
	m_SlowSpeed = 50;

	m_ForceLookDown = false;
}

void dcamRoamCam::Init(const Vector3 &camPos, const Vector3 &lookAtPos) {
	m_InitPos.Set(camPos);
	m_LookAtPos.Set(lookAtPos);
	Reset();
}

void dcamRoamCam::Reset() {
	m_WorldMtx.Identity();
	m_WorldMtx.d.Set(m_InitPos);
	m_Interest.Set(m_LookAtPos);
	m_WorldMtx.LookAt(m_Interest, m_vUp);
}


bool dcamRoamCam::Update(float dt) {
	m_Mapper.Update();

	const float dz = 0.3f;
	float dr = 3.0f * dt;
	float dp = (m_Accelerator.IsDown()? m_FastSpeed : m_SlowSpeed) * dt;
	float heading = m_Heading.GetNorm(dz) * dr;
	float pitch = m_Pitch.GetNorm(0.5f) * dr;
	float strafe = m_Strafe.GetNorm(dz) * dp;
	float forward = m_Forward.GetNorm(dz) * dp;

	bool moved = false;

	if (heading || pitch || strafe || forward)
	{
		moved = true;
	}

	if (m_PolarLock.IsPressed())
		m_Interest.AddScaled(m_WorldMtx.d,m_WorldMtx.c,-25);

	if (m_PolarLock.IsDown()) {
		float scale = -10.0f;
		m_WorldMtx.d.AddScaled(m_WorldMtx.b,pitch * scale);
		m_WorldMtx.d.AddScaled(m_WorldMtx.a,heading * scale);
		m_WorldMtx.d.AddScaled(m_WorldMtx.c,forward);
		// Don't go crazy trying to look at something zero meters away
		if (m_WorldMtx.d.Dist(m_Interest) < 3)
			m_Interest.AddScaled(m_WorldMtx.d,m_WorldMtx.c,-3);
		m_WorldMtx.LookAt(m_Interest, m_vUp);
	}
	else {
		m_WorldMtx.Rotate(m_vUp,-heading);
		m_WorldMtx.Rotate(m_WorldMtx.a,pitch);
		m_WorldMtx.d.AddScaled(m_WorldMtx.a,strafe);

		Vector3 flatc(m_WorldMtx.c);
		if (m_UseZUp) {
			if(fabsf(flatc.x)<1e-5f && fabsf(flatc.y)<1e-5f) {
				flatc.Set(m_WorldMtx.c);
				flatc.Negate();
			}
			else {
				flatc.z = 0;
			}
		}
		else {
			if(fabsf(flatc.x)<1e-5f && fabsf(flatc.z)<1e-5f) {
				flatc.Set(m_WorldMtx.b);
				flatc.Negate();
			}
			else {
				flatc.y = 0;
			}
		}
		if (flatc.Mag2()) {
			flatc.Normalize();
			if (m_ResetPitch.IsDown()) {
				m_WorldMtx.c.Set(flatc);
				m_WorldMtx.b.Set(0,1,0);
				m_WorldMtx.a.Cross(m_WorldMtx.b,m_WorldMtx.c);
			}
		}
		m_WorldMtx.d.AddScaled(flatc,forward);
	
		if (m_UseZUp) {
			m_WorldMtx.d.z -= dp * m_Sink.GetNorm01();
			m_WorldMtx.d.z += dp * m_Rise.GetNorm01();
		}
		else {
			m_WorldMtx.d.y -= dp * m_Sink.GetNorm01();
			m_WorldMtx.d.y += dp * m_Rise.GetNorm01();
		}

		m_WorldMtx.Normalize();
	}

	if (m_ForceLookDown) {
		Vector3 down(0.0f,-1.0f,0.0f);
		m_WorldMtx.LookDown(down, m_vUp);
	}

	return moved;
}

void dcamRoamCam::Draw(int scrX, int scrY, float alpha) const {
	// show instructions on screen:
	scrY=DrawLine(scrX, scrY, "Right stick: Heading / Pitch (R3: Reset Pitch)", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Left stick: Move (L2: Faster)", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "R1/R2: Altitude higher / lower", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "L1: Polar lock (right stick is polar, left zooms in / out)", Color32(1.f,1.f,1.f,alpha));
}

#if __BANK
void dcamRoamCam::AddWidgets(bkBank &bk) {
	bk.AddSlider("Slow Speed",&m_SlowSpeed,1,200,1);
	bk.AddSlider("Fast Speed",&m_FastSpeed,10,1000,1);
	bk.AddToggle("Force Look Down",&m_ForceLookDown);
}
#endif
