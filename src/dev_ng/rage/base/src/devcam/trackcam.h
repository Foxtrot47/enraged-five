//
// devcam/trackcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_TRACKCAM_H
#define DEVCAM_TRACKCAM_H

#include "devcam.h"

#include "vector/matrix44.h"

namespace rage {

class bkBank;
class spdSphere;

//
// PURPOSE:
//	A camera for development only.  It uses the mouse to move the camera using a trackball style rotation,
//  and camera relative panning.
// NOTES:
// * Mouse Controls *
//  <TABLE>
//      Mouse                     Action
//      -----------------------   ---------------------------------------------
//      Left mouse button drag    rotate scene using a virtual trackball
//      Middle mouse button drag  move camera left, right, up and down
//      Right mouse button drag   move camera forward, backward.
//      Right mouse button click  open context menu
//      Mouse wheel scroll        zoom in and out
//  </TABLE>
//<FLAG Component>
//
class dcamTrackCam : public dcamCam {
public:
	// PURPOSE: default constructor
	dcamTrackCam();

	virtual	const char* GetName() const;
	virtual void Init(const Vector3 &camPos, const Vector3 &lookAtPos);
	virtual void Reset();
	virtual bool Update(float dt);
	virtual void Draw(int scrX, int scrY, float alpha) const;
	const Matrix34& GetWorldMtx() const;
	virtual void SetWorldMtx(const Matrix34 &mtx);
	virtual void Frame(const spdSphere& frameSphere);

#if __BANK
	//<COMBINE dcamCam::AddWidgets>
	virtual void AddWidgets(bkBank&);
#endif

	//
	// PURPOSE
	//	Initializes track camera.
	// PARAMS
	//	x - start position x
	//	y - start position y
	//	z - start position z
	//	transScale - translational scale
	//
	void Init(float x=0.0f, float y = 0.0f, float z = -30.0f, float transScale=10.0f);
	//
	// PURPOSE
	//	sets the axis origin of the trackball
	// PARAMS
	//	v - the axis origin
	//
	void SetAxisOrigin(const Vector3 &v);

	// PURPOSE: renders the origin axis of the trackball
	void DrawAxis() const;

	// PURPOSE: sets the trackball axis to the negative of the camera translation
	void AxisToCamera();
	//
	// PURPOSE
	//	sets the camera matrix to look from a distance above the xz plane
	// PARAMS
	//	dist - the distance along the +y axis above the xz plane
	//
	void TopView(float dist = 1000);
	// PURPOSE: accessor for the camera's view matrix (inverse of the world matrix)
	// RETURNS: a reference to the camera's view matrix
	const Matrix34& GetViewMtx() const;
	// PURPOSE: accessor for the trackball axis matrix
	// RETURNS: the trackball axis matrix
	Matrix34& GetAxis();

	//
	// PURPOSE
	//	sets translation activated by middle mouse to be reversed 
	// PARAMS
	//	b - true if translation should be reversed, false if not
	//
	void SetReverse(bool b);
	//
	// PURPOSE
	//	sets the wheel zoom in/out scale
	// PARAMS
	//	scale - the scale to zoom in/out 
	//
	void SetWheelScale(float scale);
	//
	// PURPOSE
	//	sets the window dimensions to use instead of dimensions obtained through the mouse class
	// PARAMS
	//	width - width of the window 
	//	height - height of the window
	//
	void SetMouseWindowOverride(float width, float height);

private:
#if __BANK
	void PostMenu();
#endif
	bool UpdateInput(float dt);		// applies mouse and keyboard input to camera
	void UpdateMatrix(float dt);	// updates axis origin and inverses view matrix

	Matrix34 m_ViewMtx, m_Axis;  // view-port matrix & current axis for camera
	Matrix34 m_WorldMtx;
	Vector3 m_StartPos;			// so we can reset to starting position
	Vector3 m_StartLookAt;		//	...... focus point
	
	Vector3 m_TrackPostTrans;	// temporary for track cam rotation
	Matrix34 m_TrackPreMtx;

	float m_WheelScale;
	float m_Scale;
	float m_Quat[4];

#if __WIN32PC
	float m_LastNormX, m_LastNormY;
#endif
	int m_LastX, m_LastY;
	bool m_POV;
	bool m_Reverse;

	float m_MouseWindowOverrideWidth, m_MouseWindowOverrideHeight;
};

inline dcamTrackCam::dcamTrackCam()	{ 
	Init(); 
}

inline const char* dcamTrackCam::GetName() const {
	return "Track Cam";
}

inline const Matrix34& dcamTrackCam::GetViewMtx() const {
	return m_ViewMtx;
}

inline const Matrix34& dcamTrackCam::GetWorldMtx() const {
	return m_WorldMtx;
}

inline void dcamTrackCam::Frame(const spdSphere& /*fameSphere*/)
{
	//Not implemented
}

inline void dcamTrackCam::SetWorldMtx(const Matrix34 &mtx)
{
	m_WorldMtx.Set(mtx);

	m_ViewMtx.FastInverse(m_WorldMtx);
}

inline Matrix34& dcamTrackCam::GetAxis() {
	return m_Axis;
}

inline void	dcamTrackCam::SetReverse(bool b) {
	m_Reverse = b;
}

inline void	dcamTrackCam::SetWheelScale(float scale) {
	m_WheelScale = scale;
}

inline void	dcamTrackCam::SetMouseWindowOverride(float width, float height) {
	m_MouseWindowOverrideWidth = width; m_MouseWindowOverrideHeight = height;
}


}	// namespace rage

#endif // DEVCAM_TRACKCAM_H
