//
// devcam/polarcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_MRKORBITCAM_H
#define DEVCAM_MRKORBITCAM_H
#if !__FINAL
#include "mrkcambase.h"
#include "atl/delegate.h"

namespace rage {

class dcamMrkOrbitCam : public dcamMrkCamBase {
public:
	// PURPOSE: default constructor
	dcamMrkOrbitCam();
	virtual ~dcamMrkOrbitCam();

	virtual bool		Update(float dt);
	virtual const char*	GetName() const;
	virtual void		Reset();
#if __BANK
	virtual void		AddWidgets(class bkBank &bk);
#endif

	void SetLookAtCallback(const atDelegate< Vector3 (void) > & callback);
	void SetObjectMatrixCallback(const atDelegate< Matrix34 (void) > & callback);

protected:

	//derived interface
	virtual void DrawInfoText(float scrX, float scrY);

	virtual void HandleLStick(const Vector2& vectorXY);
	virtual void HandleRStick(const Vector2& vectorXY);
	virtual void HandleButtons(const u32 buttons);
	virtual void HandleHeldDebugButtons(const u32 dButtons);
private:

	Matrix34 m_DiffMatrix;
	Vector3 m_OrigForward;
	Vector3 m_LookAtPos;
	Vector3 m_Offset;
	float m_OffsetAmount;
	float m_OffsetDist;
	bool m_FirstLoop;
	bool m_FixedMode;
	atDelegate< Vector3 (void) > m_LookAtCallback;
	atDelegate< Matrix34 (void) > m_GetObjectMatrixCallback;
};

}	// namespace rage

#endif //#if !__FINAL
#endif
