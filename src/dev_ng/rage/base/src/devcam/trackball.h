//
// devcam/trackball.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_TRACKBALL_H
#define DEVCAM_TRACKBALL_H

namespace rage {

class Matrix34;


namespace trk {
	/*
	* (c) Copyright 1993, 1994, Silicon Graphics, Inc.
	* ALL RIGHTS RESERVED
	* Permission to use, copy, modify, and distribute this software for
	* any purpose and without fee is hereby granted, provided that the above
	* copyright notice appear in all copies and that both the copyright notice
	* and this permission notice appear in supporting documentation, and that
	* the name of Silicon Graphics, Inc. not be used in advertising
	* or publicity pertaining to distribution of the software without specific,
	* written prior permission.
	*
	* THE MATERIAL EMBODIED ON THIS SOFTWARE IS PROVIDED TO YOU "AS-IS"
	* AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR OTHERWISE,
	* INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR
	* FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL SILICON
	* GRAPHICS, INC.  BE LIABLE TO YOU OR ANYONE ELSE FOR ANY DIRECT,
	* SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY
	* KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION,
	* LOSS OF PROFIT, LOSS OF USE, SAVINGS OR REVENUE, OR THE CLAIMS OF
	* THIRD PARTIES, WHETHER OR NOT SILICON GRAPHICS, INC.  HAS BEEN
	* ADVISED OF THE POSSIBILITY OF SUCH LOSS, HOWEVER CAUSED AND ON
	* ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE
	* POSSESSION, USE OR PERFORMANCE OF THIS SOFTWARE.
	*
	* US Government Users Restricted Rights
	* Use, duplication, or disclosure by the Government is subject to
	* restrictions set forth in FAR 52.227.19(c)(2) or subparagraph
	* (c)(1)(ii) of the Rights in Technical Data and Computer Software
	* clause at DFARS 252.227-7013 and/or in similar or successor
	* clauses in the FAR or the DOD or NASA FAR Supplement.
	* Unpublished-- rights reserved under the copyright laws of the
	* United States.  Contractor/manufacturer is Silicon Graphics,
	* Inc., 2011 N.  Shoreline Blvd., Mountain View, CA 94039-7311.
	*
	* OpenGL(TM) is a trademark of Silicon Graphics, Inc.
	*/
	/*
	* trackball.h
	* A virtual trackball implementation
	* Written by Gavin Bell for Silicon Graphics, November 1988.
	*/


//
// PURPOSE
//   Project the points onto the virtual
//   trackball, then figure out the axis of rotation, which is the cross
//   product of P1 P2 and O P1 (O is the center of the ball, 0,0,0)
// PARAMS
//	qOut - the resulting rotation
//	p1x - point 1's x position
//	p1y - point 1's y position
//	p2x - point 2's x position
//	p2y - point 2's y position
// NOTES
//   * Pass the x and y coordinates of the last and current positions of
//     the mouse, scaled so they are from (-1.0 ... 1.0).
//   * This is a deformed trackball-- is a trackball in the center,
//     but is deformed into a hyperbolic sheet of rotation away from the
//     center.  This particular function was chosen after trying out
//     several variations.
//
void TrackBall(float qOut[4], float p1x, float p1y, float p2x, float p2y);

//
// PURPOSE
//  Build a rotation matrix, given a quaternion rotation.
// PARAMS
//	mOut - the resulting matrix
//	q - the quaternion to build the matrix from
//
void BuildRotMatrix(Matrix34& mOut, float q[4]);

}	// namespace trk

}	// namespace rage

#endif // DEVCAM_TRACKBALL_H

