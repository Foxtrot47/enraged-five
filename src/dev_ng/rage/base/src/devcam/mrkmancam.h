//
// devcam/polarcam.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DEVCAM_MRKMANCAM_H
#define DEVCAM_MRKMANCAM_H
#if !__FINAL
#include "mrkcambase.h"

namespace rage {

class dcamMrkManCam : public dcamMrkCamBase {
public:
	// PURPOSE: default constructor
	dcamMrkManCam();
	virtual ~dcamMrkManCam();

	virtual bool		Update(float dt);
	virtual const char*	GetName() const;
	virtual void		Reset();
#if __BANK
	virtual void		AddWidgets(class bkBank &bk);
#endif

	void SetLookAtCallback(const atDelegate< Vector3 (void) > & callback);
protected:

	//derived interface
	virtual void DrawInfoText(float scrX, float scrY);

	void HandleLStick(const Vector2& vectorXY);
	void HandleRStick(const Vector2& vectorXY);
	void HandleHeldDebugButtons(const u32 dButtons);

private:
	void UpdateLookAtPosition();

	bool m_Locked;
	Vector3 m_LookAtPos;
	Vector3 m_Offset;
	float m_OffsetAmount;
	atDelegate< Vector3 (void) > m_LookAtCallback;
};

}	// namespace rage

#endif //#if !__FINAL
#endif
