//
// devcam/mrkorbitcam.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mrkorbitcam.h"
#include "input/pad.h"
#include "atl/delegate.h"
#include "bank/bank.h"

#if !__FINAL
using namespace rage;

dcamMrkOrbitCam::dcamMrkOrbitCam()
{
	m_DiffMatrix.Identity();
	m_LookAtPos.Zero();
	m_Offset.Zero();
	m_OffsetAmount = 2.0f;
	m_OffsetDist = 0.0f;
	m_FirstLoop = true;
	m_FixedMode = false;
}

dcamMrkOrbitCam::~dcamMrkOrbitCam()
{

}

bool dcamMrkOrbitCam::Update(float dt)
{
	//use call back to get info from the game
	if (m_LookAtCallback.IsBound())
	{
		m_LookAtPos = m_LookAtCallback();
	}

	//apply offset
	m_LookAtPos += m_Offset;


	Matrix34 current = GetWorldMtx();
	if (m_FirstLoop)
	{
		m_OffsetDist = m_LookAtPos.Dist(current.d);
		m_FirstLoop = false;
	}	

	if (m_FixedMode)
	{
		if (m_GetObjectMatrixCallback.IsBound())
		{
			Matrix34 objMat = m_GetObjectMatrixCallback();

			Matrix34 temp;
			temp.Add(objMat, m_DiffMatrix);

			Vector3 pos = temp.d;

			//get the final matrix by looking at the look at position from our new camera position
			Matrix34 final;
			final.LookAt(pos,m_LookAtPos,YAXIS);

			SetWorldMtx(final);
		}
	}
	else
	{
		//get my direction
		Vector3 forward = current.c;

		//scale the forward by the distance we are to be at
		forward.Scale(m_OffsetDist);

		//get my final location
		Vector3 pos = m_LookAtPos + forward;

		//get the final matrix by looking at the look at position from our new camera position
		Matrix34 final;
		final.LookAt(pos,m_LookAtPos,YAXIS);

		SetWorldMtx(final);
	}
	
	return dcamMrkCamBase::Update(dt);
}

const char* dcamMrkOrbitCam::GetName() const
{
	return "R*NY Marketing Orbit Camera";
}

void dcamMrkOrbitCam::Reset()
{
	m_DiffMatrix.Identity();
	m_LookAtPos.Zero();
	m_Offset.Zero();
	m_FirstLoop = true;
	m_OffsetDist = 0.0f;
	m_OffsetAmount = 2.0f;
	m_FixedMode = false;
	dcamMrkCamBase::Reset();
}

#if __BANK
void dcamMrkOrbitCam::AddWidgets(class bkBank &bk)
{
	bk.AddVector("Focal Point Offset", &m_Offset, -999999.9f, 999999.9f, m_OffsetAmount);
	dcamMrkCamBase::AddWidgets(bk);
}
#endif

void dcamMrkOrbitCam::SetObjectMatrixCallback(const atDelegate< Matrix34 (void) > & callback)
{
	m_GetObjectMatrixCallback = callback;
}

void dcamMrkOrbitCam::SetLookAtCallback(const atDelegate< Vector3 (void) > & callback)
{
	m_LookAtCallback = callback;
}

void dcamMrkOrbitCam::DrawInfoText(float scrX, float scrY)
{
	static const char* texts[] = {
 	"R*NY marketing camera for orbiting an object. Will track the object focused on.",
	"Uses gamepad(#2) to move the camera and do actions.",
	"",
	"Controls  XBox 360 Controller",
	"-----------------------------------",
	"(Right Stick) = Rotates the cam azimuthally around the player",
	"(Left Stick) = zoom in and out of focused object and Pan Left (focal offset + X) and Right (focal offset - X)",
	"(Start + Directional Pad L) = focal offset + Z ",
	"(Start + Directional Pad R) = focal offset - Z",
	"(Start + Directional Pad Up) = focal offset + Y ",
	 "(Start + Directional Pad Down) = focal offset - Y ",
	 "(R3) = toggle Fixed orbital camera mode",
	 "",
	 "Inherited Controls XBox 360 Controller",
	 "-----------------------------------",
	 "(L3) = Pause time cycle",
	 "(Start Button) = Pause Game",
	 "(Back Button) = Switch HUD on and off",
	 "(Start + Y) = increase move speed by 0.001",
	 "(Start + A) = slow move speed by 0.001",
	 "(Start + X) = increase rot speed by 0.001",
	 "(Start + R3) = slow rot speed by 0.001",
	 "(X) = Moves Forward",
	 "(A) = Moves Backward",
	 "(Directional Pad R) = Advance Time",
	 "(Directional Pad L) = Weather Type",
	 "(Directional Pad Down) = Drop Player",
	 "(Directional Pad Up) = Step forward one frame (while paused)",
	 "(Left Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Right Bumper) = Roll camera.  x-y rotation of the frame",
	 "(Left Trigger) = Decrease FOV",
	 "(Right Trigger) = Increase FOV",
	 "(Start + Left Bumper) = Widgets Menu",
	 "(Start + B) Toggles between Detached Cam and In-Game Cam",
	 "(Start + Right Bumper) cycles through all Cam modes."
	};
	

	int count = sizeof(texts)/sizeof(char*);
	int lineSize = 10;
	for(int i = 0; i < count; i++)
	{
		grcFont::GetCurrent().Drawf(scrX,scrY+(i*lineSize), texts[i]);
	}
}

void dcamMrkOrbitCam::HandleLStick(const Vector2& vectorXY)
{
	// (Left Stick) = zoom in and out of focused object
	m_OffsetDist += (m_OffsetAmount * vectorXY.y);

	// (Left Stick) = Pan Left and Right
	m_Offset.x += (m_OffsetAmount * vectorXY.x);
}

void dcamMrkOrbitCam::HandleRStick(const Vector2& vectorXY)
{
	if (m_FixedMode)
		return;

	// (Right Stick) = Rotates the cam azimuthally around the player
	Matrix34 current = GetWorldMtx();

	//be at the origin
	Matrix34 temp = current;
	temp.d.Set(0.0f);//at the origin

	//rotate on the right axis by the amount
	temp.Rotate(current.b,(-vectorXY.x)*m_RotSpeedAugment);

	//rotate on the right axis by the amount
	temp.Rotate(current.a,(-vectorXY.y)*m_RotSpeedAugment);

	//get the radius i am to be away
	float dist = m_OffsetDist;

	//get my direction
	Vector3 forward = temp.c;

	//scale the forward by the distance we are to be at
	forward.Scale(dist);

	//get my final location
	Vector3 pos = m_LookAtPos + forward;

	//get the final matrix by looking at the look at position from our new camera position
	Matrix34 final;
	final.LookAt(pos,m_LookAtPos,YAXIS);

	SetWorldMtx(final);
}

void dcamMrkOrbitCam::HandleButtons(const u32 buttons)
{
	//(R3) = toggle Fixed orbital camera mode 
	if (buttons & ioPad::R3)
	{
		/*
		m_FixedMode = !m_FixedMode;
		if (m_GetObjectMatrixCallback.IsBound())
		{
			Matrix34 objMat = m_GetObjectMatrixCallback();
			m_DiffMatrix.Subtract(GetWorldMtx(),objMat);
		
		}
		*/
		return;
	}

	dcamMrkCamBase::HandleButtons(buttons);
}

void dcamMrkOrbitCam::HandleHeldDebugButtons(const u32 dButtons)
{
	//(Directional Pad L) = focal offset Z 
	if (dButtons & ioPad::LLEFT)
	{
		if (GetUseZUp())
		{
			m_Offset.y += (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.z += (m_OffsetAmount * m_dt);
		}
		return;
	}

	//(Directional Pad R) = focal offset Z
	if (dButtons & ioPad::LRIGHT)
	{
		if (GetUseZUp())
		{
			m_Offset.y -= (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.z -= (m_OffsetAmount * m_dt);
		}
		return;
	}

	//(Directional Pad U) = focal offset Y 
	if (dButtons & ioPad::LUP)
	{
		if (GetUseZUp())
		{
			m_Offset.z += (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.y += (m_OffsetAmount * m_dt);
		}
		return;
	}

	//(Directional Pad D) = focal offset Y 
	if (dButtons & ioPad::LDOWN)
	{
		if (GetUseZUp())
		{
			m_Offset.z -= (m_OffsetAmount * m_dt);
		}
		else
		{
			m_Offset.y -= (m_OffsetAmount * m_dt);
		}
		return;
	}

	dcamMrkCamBase::HandleHeldDebugButtons(dButtons);
}

#endif //#if !__FINAL
