// 
// devcam/lhcam.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "lhcam.h"

#include "bank/bank.h"
#include "input/pad.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "math/angmath.h"
#include "spatialdata/sphere.h"
#include "vector/matrix34.h"
#include "grcore/device.h"

using namespace rage;

void dcamLhCam::Init(float dist,float azm,float inc,Vector3::Vector3Param Offset) {
	m_Mapper.Reset();

	m_Distance=dist;
	m_Azimuth=azm;
	m_OrientAzimuth=0.0f;
	m_Incline=inc;

	m_InitDistance = dist;
	m_InitAzimuth = azm;
	m_InitIncline = inc;
	m_InitOffset.Set(Offset);

	m_LinearRate=10.0f;
	m_AngularRate=2.0f;
	m_OrientRate=0.05f;

	m_Offset.Set(Offset);
	m_Follow=0;
	m_Orient=0;
	m_FollowOffset=false;

	m_WorldMtx.Identity();
	m_EnableInput=true;

	m_LastMouseX = m_LastMouseY = 0;

	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LY, m_ioZoom);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_RX, m_ioAzimuth);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_RY, m_ioIncline);
	
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::R2, m_Up);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L2, m_Down);

	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::R1, m_PadSlower);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L1, m_PadFaster);

	m_Mapper.Map(IOMS_KEYBOARD, KEY_SHIFT, m_Shift);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_Control);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_Alt);

	// Update m_WorldMtx and m_ViewMtx. This used to not be done here,
	// but the result was that if you called Init() and then GetWorldMtx()
	// without calling Update(), you would get the identity matrix rather than
	// the correct initial camera matrix. /FF
	UpdateMatrix();
}

void dcamLhCam::Init(const Vector3 &cameraPos, const Vector3 &lookAtPos) {
	float dist = cameraPos.Dist(lookAtPos);
	Vector3 lookDir;
	lookDir.Subtract(cameraPos, lookAtPos);

	// Do a quick calculation of azimuth & incline
	float azm, inc;

	if(m_UseZUp)
	{
		azm = acosf(lookDir.y / lookDir.XYMag());
		inc = 0.5f * PI - acosf(lookDir.z / dist);
	}
	else
	{
		azm = acosf(lookDir.z / lookDir.XZMag());
		inc = 0.5f * PI - acosf(lookDir.y / dist);
	}
	Init(dist, azm, inc, lookAtPos);
}

void dcamLhCam::Reset() {
	Init(m_InitDistance, m_InitAzimuth, m_InitIncline, m_InitOffset);
}

bool dcamLhCam::UpdateInput(float dt) 
{
	bool moved = false;

	float deltaMult	= 1.0f;
	
	m_Mapper.Update();

	if (m_Control.IsDown() || m_PadFaster.IsDown() ) 
	{
		deltaMult*=5.0f;
	}
	else if (m_Shift.IsDown() || m_PadSlower.IsDown() ) 
	{
		deltaMult*=0.1f;
	}

	float x = (float) (ioMouse::GetX() - m_LastMouseX);
	float y = (float) (ioMouse::GetY() - m_LastMouseY); 

	m_LastMouseX = ioMouse::GetX();
	m_LastMouseY = ioMouse::GetY();

	if(m_Alt.IsDown())
	{
		//Update from the mouse/keyboard
		if(ioMouse::GetButtons() & ioMouse::MOUSE_LEFT)
		{
			//Azimuth control
			m_Azimuth -= x * dt * deltaMult;

			//Incline control
			m_Incline += y * dt * deltaMult;

			if(m_UseZUp)
			{
				//Clamp the incline at 0 and -PI
				if(m_Incline > 0.0f)
					m_Incline = -0.001f;
				else if(m_Incline < -PI)
					m_Incline = -PI + 0.001f;
			}
			else
			{
				//Clamp the incline at PI/2 and -PI/2
				if(m_Incline > (PI*0.5f))
					m_Incline = ((PI*0.5f) - 0.001f);
				else if(m_Incline < -(PI*0.5f))
					m_Incline = ((-PI*0.5f) + 0.001f);
			}
		}
		if(ioMouse::GetButtons() & ioMouse::MOUSE_RIGHT)
		{
			//Distance control
			m_Distance += y * dt * deltaMult;
		}
		if(ioMouse::GetButtons() & ioMouse::MOUSE_MIDDLE)
		{
			//Up/Down control
			if(m_UseZUp)
				m_Offset.z += y * dt * deltaMult;
			else
				m_Offset.y += y * dt * deltaMult;
		}
	}
	else
	{
		//Update from the controller
		if (m_Up.IsDown())
		{
			if(m_UseZUp)
			{
				m_Offset.z+= (0.1f * deltaMult);
			}
			else
			{
				m_Offset.y+= (0.1f * deltaMult);
			}
			moved = true;
		}
		if (m_Down.IsDown())
		{
			if(m_UseZUp)
			{
				m_Offset.z-= (0.1f * deltaMult);
			}
			else
			{
				m_Offset.y-= (0.1f * deltaMult);
			}
			moved = true;
		}

		float ioZoomVal = m_ioZoom.GetNorm(0.3f) * (dt * deltaMult);
		if(ioZoomVal != 0.0f)
		{
			m_Distance += ioZoomVal;
			moved = true;
		}

		float ioAziVal = m_ioAzimuth.GetNorm(0.3f) * (dt * deltaMult);
		if(ioAziVal != 0.0f)
		{
			m_Azimuth -= ioAziVal;
			moved = true;
		}
		
		float ioIncVal = m_ioIncline.GetNorm(0.3f) * (dt * deltaMult);
		if(ioIncVal != 0.0f)
		{
			m_Incline += ioIncVal;
			moved = true;

			if(m_UseZUp)
			{
				//Clamp the incline at 0 and -PI
				if(m_Incline > 0.0f)
					m_Incline = -0.001f;
				else if(m_Incline < -PI)
					m_Incline = -PI + 0.001f;
			}
			else
			{
				//Clamp the incline at PI/2 and -PI/2
				if(m_Incline > (PI*0.5f))
					m_Incline = ((PI*0.5f) - 0.001f);
				else if(m_Incline < -(PI*0.5f))
					m_Incline = ((-PI*0.5f) + 0.001f);
			}
		}
	}

	return moved;
}

bool dcamLhCam::Update(float dt) 
{
	bool moved = false;

	if (m_EnableInput)
	{
		moved = UpdateInput(dt);
	}

	if (m_Orient)
	{
		float desiredAzimuth;

		if(m_UseZUp)
		{
			desiredAzimuth = atan2f(m_Orient->x, m_Orient->y);
		}
		else
		{
			desiredAzimuth = atan2f(m_Orient->x, m_Orient->z);
		}

		m_OrientAzimuth=CanonicalizeAngle(powf(m_OrientRate, dt*60.0f)*SubtractAngleShorter(desiredAzimuth, m_OrientAzimuth) + m_OrientAzimuth);
	}

	UpdateMatrix();

	return moved;
}

void dcamLhCam::Frame(const spdSphere& frameSphere)
{
	Vector3 lookAtPos = VEC3V_TO_VECTOR3(frameSphere.GetCenter());
	Vector3 cameraPos = lookAtPos;
	cameraPos.AddScaled(m_WorldMtx.c, 2.0f*frameSphere.GetRadius().Getf());

	m_Distance = cameraPos.Dist(lookAtPos);
	
	Vector3 lookDir;
	lookDir.Subtract(cameraPos, lookAtPos);

	m_Offset=lookAtPos;
}

void dcamLhCam::UpdateMatrix()
{
	// This all used to be done inside Update(), but I moved it into its
	// own function so it can also be called from Init(). /FF

	PolarView(m_Distance,m_Azimuth+m_OrientAzimuth,m_Incline,m_WorldMtx);
	m_WorldMtx.d.Add(m_Offset);
	if(m_Follow)
	{
		m_WorldMtx.d.Add(*m_Follow);
		if(m_FollowOffset)
		{
			m_WorldMtx.d.Subtract(m_Offset);
			Vector3 worldOffset;
			m_WorldMtx.Transform3x3(m_Offset,worldOffset);
			m_WorldMtx.d.Add(worldOffset);
		}
	}

	Matrix34 temp;
	if (temp.Inverse(m_WorldMtx))
		Convert(m_ViewMtx,temp);
}


void dcamLhCam::PolarView(float dist, float azm, float inc, Matrix34 &mtx) 
{
	if(m_UseZUp)
	{
		mtx.Identity();
		mtx.RotateX(-inc);
		mtx.RotateZ(azm);
	}
	else
	{
		mtx.Identity();
		mtx.RotateX(-inc);
		mtx.RotateY(azm);
	}

	mtx.d.Scale(mtx.c,dist);
}

void dcamLhCam::Draw(int scrX, int scrY, float alpha) const 
{
	// show instructions on screen:
#if !__CONSOLE
	scrY=DrawLine(scrX, scrY, "Hold down ALT to activate camera control.", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "LMB : (left/right) Orbit, (up/down) Incline", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "RMB : (up/down) Zoom In/Out", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "MMB : (up/down) Move Camera Up/Down", Color32(1.f,1.f,1.f,alpha));
#else
	scrY=DrawLine(scrX, scrY, "Right Stick : (left/right) Oribt, (up/down) Incline", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "Left Stick : (up/down) Zoom In/Out", Color32(1.f,1.f,1.f,alpha));
	scrY=DrawLine(scrX, scrY, "R1 & R2 : Move Camera Up/Down",Color32(1.f,1.f,1.f,alpha));
#endif
}

#if __BANK
void dcamLhCam::AddWidgets(bkBank &bk) {
	bk.AddSlider("Distance",&m_Distance,0,1000000,0.1f);
	bk.AddSlider("Azimuth",&m_Azimuth,-100,100,0.01f);
	bk.AddSlider("Incline",&m_Incline,-100,100,0.01f);
	bk.AddSlider("Offset",&m_Offset,-1000000.0f,1000000.0f,0.1f);
}
#endif
