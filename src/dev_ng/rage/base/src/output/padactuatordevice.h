//
// output/padactuator.h : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_PADACTUATOR_H_
#define OUTPUT_PADACTUATOR_H_

// rage headers.
#include "actuatordevice.h"
#include "input/pad.h"

namespace rage
{
// PURPOSE: Actuator interface for an ioPad.
class ioPadActuatorDevice : public ioAcutatorDevice
{
public:
	// PURPOSE: Constructor representing an invalid ioPadActuatorDevice.
	ioPadActuatorDevice();

	// PURPOSE: Constructor.
	// PARAMS:	padIndex the index of the pad to apply actuations to.
	ioPadActuatorDevice(int padIndex);

	// PURPOSE: Destructor.
	virtual ~ioPadActuatorDevice() {}

	// PURPOSE: Indicates that this device supports rumble actuators.
	virtual bool SupportsRumble() const;

	// PURPOSE: Indicates that this device supports force actuators.
	virtual bool SupportsForce() const;

	// PURPOSE: Indicates if the device supports a specific actuator.
	// PRAMS:   actuator - the actuator to check.
	// RETURNS: true if the specified actuator is supported.
	virtual bool HasActuator(Actuator actuator) const;

	// PURPOSE: Sets a supported actuator's value.
	// PRAMS:   actuator - the actuator to set.
	//          value - the value to set the actuator two.
	virtual void SetActuator(Actuator actuator, float value);

	// PURPOSE: Indicates if a supported actuator has a corresponding input source.
	// PARAMS:	actuator - the actuator to check.
	// RETURNS: true if the supported actuator has an input source.
	// NOTES:	Unsupported actuators will return false. An example of a corresponding
	//          input would be the trigger's input value for a trigger rumble or a
	//          steering wheel's position in a force feedback wheel.
	virtual bool HasInputValue(Actuator actuator) const;

	// PURPOSE: Retrieves the input value of a corresponding input source.
	// PARAMS:	actuator - the actuator to check.
	// RETURNS: the input value (in the rage -1.0f to 1.0f or 0.0f if there is no input value.
	// NOTES:	The return value will not be dead-zoned. An example of a corresponding input
	//			would be the trigger's input value for a trigger rumble or a steering wheel's
	//			position in a force feedback wheel.
	virtual float GetInputValue(Actuator actuator) const;

	// PURPOSE:	Updates the actuator(s).
	// NOTES:   If concrete implementations cache/merge actuator updates, they can/should apply
	//          them here.
	virtual void Update();

	// PURPOSE: Scales the cached actuators.
	// PARAMS:  scale - the value to scale by in the range 0.0f...1.0f.
	void ScaleActuators(float scale);

	// PURPOSE: Resets the cached actuator values.
	void Reset();

private:
	// PURPOSE: Updates the local cached version of the actuator value.
	void UpdateActuator(ioPad::PadActuator actuator, float value);

	// PURPOSE: The index of the pad to be actuated.
	int m_PadIndex;

	// PURPOSE: The actuator values.
	float m_ActuatorValues[ioPad::MAX_ACTUATORS];
};

inline ioPadActuatorDevice::ioPadActuatorDevice()
	: m_PadIndex(-1)
{
	Reset();
}

inline ioPadActuatorDevice::ioPadActuatorDevice(int padIndex)
	: m_PadIndex(padIndex)
{
	Reset();
}

inline void ioPadActuatorDevice::UpdateActuator(ioPad::PadActuator actuator, float value)
{
	if(m_ActuatorValues[actuator] < value)
	{
		m_ActuatorValues[actuator] = value;
	}
}
}

#endif // OUTPUT_PADACTUATOR_H_