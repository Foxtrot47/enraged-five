//
// output/padlightdevice.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "padlightdevice.h"

// rage headers.
#include "input/pad.h"

namespace rage
{

bool ioPadLightDevice::IsValid() const
{
#if RSG_ORBIS
	return true;
#else
	return false;
#endif // RSG_ORBIS
}

bool ioPadLightDevice::HasLightSource(Source ORBIS_ONLY(source)) const
{
#if RSG_ORBIS
	if(source == MIDDLE)
	{
		return true;
	}
	else
#endif // RSG_ORBIS
	{
		return false;
	}
}

void ioPadLightDevice::SetLightColor(Source ORBIS_ONLY(source), u8 ORBIS_ONLY(red), u8 ORBIS_ONLY(green), u8 ORBIS_ONLY(blue))
{
#if RSG_ORBIS
	if(source == MIDDLE)
	{
		ioPad::GetPad(m_PadIndex).SetLightbar(red, green, blue);
	}
#endif // RSG_ORBIS
}

void ioPadLightDevice::SetAllLightsToDefaultColor()
{
#if RSG_ORBIS
	ioPad::GetPad(m_PadIndex).ResetLightbar();
#endif // RSG_ORBIS
}

void ioPadLightDevice::Update()
{
}

}