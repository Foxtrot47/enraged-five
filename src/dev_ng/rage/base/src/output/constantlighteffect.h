//
// light/constantlighteffect.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef LIGHT_CONSTANTLIGHTEFFECT_H_
#define LIGHT_CONSTANTLIGHTEFFECT_H_

#include "lighteffect.h"

// rage headers.
#include "vector/color32.h"

namespace rage
{
// PURPOSE: Represents a constant color light effect.
class ioConstantLightEffect : public ioLightEffect
{
public:
	// PURPOSE: Default constructor represents no light.
	ioConstantLightEffect();

	// PURPOSE:	Constructor.
	// PARAMS:	red - the red color part.
	//			green - the green color part.
	//			blue - the blue color part.
	// NOTES:	Duration defaults to 1ms to set the color and leave it.
	ioConstantLightEffect(u8 red, u8 green, u8 blue);

	// PURPOSE:	Constructor.
	// PARAMS:	color - the color  to use.
	// NOTES:	Duration defaults to 1ms to set the color and leave it.
	ioConstantLightEffect(Color32 color);

	// PURPOSE:	Constructor.
	// PARAMS:	duration - the duration of the effect.
	//			red - the red color part.
	//			green - the green color part.
	//			blue - the blue color part.
	ioConstantLightEffect(u32 duration, u8 red, u8 green, u8 blue);

	// PURPOSE:	Constructor.
	// PARAMS:	duration - the duration of the effect.
	//			color - the color  to use.
	ioConstantLightEffect(u32 duration, Color32 color);
	
	// PURPOSE:	Destructor.
	virtual ~ioConstantLightEffect() {}
	
	// PURPOSE: Update the light effect.
	// PARAMS:  progress - the progress through the light effect.
	//          device - the device the apply the effect to.
	// NOTES:   Update() is marked as a const method as ioLightEffects are stateless
	//          so the same affect can be applied to multiple devices.
	virtual void Update(float progress, ioLightDevice* device) const;

	// RETURNS: The red color component.
	u8 GetRed() const;

	// RETURNS: The green color component.
	u8 GetGreen() const;
	
	// RETURNS: The blue color component.
	u8 GetBlue() const;

private:
	// PURPOSE:	The red color part.
	u8 m_Red;
	
	// PURPOSE: The green color part.
	u8 m_Green;
	
	// PURPOSE: The blue color part.
	u8 m_Blue;
};

inline ioConstantLightEffect::ioConstantLightEffect()
	: ioLightEffect(0)
	, m_Red(0)
	, m_Green(0)
	, m_Blue(0)
{}

inline ioConstantLightEffect::ioConstantLightEffect(u8 red, u8 green, u8 blue)
	: ioLightEffect(1)
	, m_Red(red)
	, m_Green(green)
	, m_Blue(blue)
{
}

inline ioConstantLightEffect::ioConstantLightEffect(Color32 color)
	: ioLightEffect(1)
	, m_Red(color.GetRed())
	, m_Green(color.GetGreen())
	, m_Blue(color.GetBlue())
{
}

inline ioConstantLightEffect::ioConstantLightEffect(u32 duration, u8 red, u8 green, u8 blue)
	: ioLightEffect(duration)
	, m_Red(red)
	, m_Green(green)
	, m_Blue(blue)
{
}

inline ioConstantLightEffect::ioConstantLightEffect(u32 duration, Color32 color)
	: ioLightEffect(duration)
	, m_Red(color.GetRed())
	, m_Green(color.GetGreen())
	, m_Blue(color.GetBlue())
{
}

inline u8 ioConstantLightEffect::GetRed() const
{
	return m_Red;
}

inline u8 ioConstantLightEffect::GetGreen() const
{
	return m_Green;
}

inline u8 ioConstantLightEffect::GetBlue() const
{
	return m_Blue;
}

}

#endif // LIGHT_CONSTANTLIGHTEFFECT_H_