//
// output/padlightdevice.h
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_LOGITECHLIGHTDEVICE_H_
#define OUTPUT_LOGITECHLIGHTDEVICE_H_

#if RSG_PC

#include "lightdevice.h"

#include "atl/array.h"

#define SUPPORT_PER_KEY_LIGHTS (1)

#if SUPPORT_PER_KEY_LIGHTS
#define PER_KEY_LIGHTS_ONLY(...)		__VA_ARGS__
#define NOT_PER_KEY_LIGHTS_ONLY(...)
#else
#define PER_KEY_LIGHTS_ONLY(...)
#define NOT_PER_KEY_LIGHTS_ONLY(...)	__VA_ARGS__
#endif // SUPPORT_PER_KEY_LIGHTS

namespace rage
{

class ioLogitechLedDevice : public ioLightDevice
{
public:
	// PURPOSE: Default constructor does not references a pad.
	ioLogitechLedDevice();

	// PURPOSE: Destructor.
	virtual ~ioLogitechLedDevice();

	// PURPOSE: Indicates that the light device is valid and working.
	virtual bool IsValid() const;

	// PURPOSE: Indicates if a given light source is available.
	// PARAMS:  source - the source to check.
	virtual bool HasLightSource(Source source) const;

	// PURPOSE: Sets a light source's color.
	// PARAMS:  source - the light source to set.
	//          red - the red color part.
	//          green - the green part.
	//          blue - the blue part.
	// NOTES:	On some devices this will set the color immediately, other devices will do this in the Update() function.
	//			Once all sources have been set, call Update().
	virtual void SetLightColor(Source source, u8 red, u8 green, u8 blue);

	// PURPOSE: Sets all lights source's colors to their defaults (if they have one).
	virtual void SetAllLightsToDefaultColor();

	// PURPOSE: Updates the light effect color.
	// NOTES:	Some devices need to send all the light effects in one go for each region, if this is the case this is done here.
	virtual void Update();

private:
	// PURPOSE: Sets the constant light effect color.
	void SetKeyboardColor(u8 red, u8 green, u8 blue);

	// PURPOSE:	Helper function to convert a color from 0-255 into a percentage.
	static int ConvertColorToPercentage(u8 color);

	// PURPOSE: Indicates if the user has a driver that supports the per key light effects without any issues.
	bool HasCompatibleDrivers() const;

	// PURPOSE: Indicates if the led interface has been initialized.
	bool m_Initialized;

#if SUPPORT_PER_KEY_LIGHTS
	// PURPOSE: Indicates that the lights have been updated.
	bool m_Updated;

	// PURPOSE: Indicates that the user has drivers that does not flicker when setting both types of light devices.
	bool m_HasCompatibleDrivers;

	// PURPOSE:	The color indexes into the cached colors array.
	enum ColorsIndex
	{
		BLUE = 0,
		GREEN,
		RED,
		MAX_COLORS,
		ALPHA = MAX_COLORS,
	};

	// PURPOSE:	The colors for the individual regions.
	// NOTES:	Due to the way we call the API, we need to cache these. Two dimensional array or Red 
	atRangeArray<atRangeArray<u8, MAX_COLORS>, MAX_LIGHT_SOURCES> m_RegionColors;

	// PURPOSE: Clears cached color values.
	void ClearCachedColors();
#endif // SUPPORT_PER_KEY_LIGHTS
};

inline int ioLogitechLedDevice::ConvertColorToPercentage(u8 color)
{
	const float COLOR_SCALE = 0.392156862745098f;
	return static_cast<int>(color * COLOR_SCALE + 0.5f);
}

}

#endif // RSG_PC

#endif // OUTPUT_LOGITECHLIGHTDEVICE_H_
