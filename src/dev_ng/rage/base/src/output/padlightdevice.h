//
// output/padlightdevice.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_PADLIGHTDEVICE_H_
#define OUTPUT_PADLIGHTDEVICE_H_

#include "lightdevice.h"

namespace rage
{
// PURPOSE: Represents a pad light device.
class ioPadLightDevice : public ioLightDevice
{
public:
	// PURPOSE: Default constructor does not references a pad.
	ioPadLightDevice();

	// PURPOSE: Constructor.
	// PARAMS:  padIndex - the pad index that this ioPadLightDevice represents.
	ioPadLightDevice(s32 padIndex);
	
	// PURPOSE: Destructor.
	virtual ~ioPadLightDevice() {}

	// PURPOSE: Indicates that the light device is valid and working.
	virtual bool IsValid() const;
	
	// PURPOSE: Indicates if a given light source is available.
	// PARAMS:  source - the source to check.
	virtual bool HasLightSource(Source source) const;
	
	// PURPOSE: Sets a light source's color.
	// PARAMS:  source - the light source to set.
	//          red - the red color part.
	//          green - the green part.
	//          blue - the blue part.
	// NOTES:	On some devices this will set the color immediately, other devices will do this in the Update() function.
	//			Once all sources have been set, call Update().
	virtual void SetLightColor(Source source, u8 red, u8 green, u8 blue);

	// PURPOSE: Sets all lights source's colors to their defaults (if they have one).
	virtual void SetAllLightsToDefaultColor();

	// PURPOSE: Updates the light effect color.
	// NOTES:	Some devices need to send all the light effects in one go for each region, if this is the case this is done here.
	virtual void Update();
private:
	// PURPOSE: The pad index that this ioPadLightDevice represents.
	s32 m_PadIndex;
};

inline ioPadLightDevice::ioPadLightDevice()
	: ioLightDevice()
	, m_PadIndex(-1)
{
}

inline ioPadLightDevice::ioPadLightDevice(s32 padIndex)
 : ioLightDevice()
 , m_PadIndex(padIndex)
{
}

}

#endif // OUTPUT_PADLIGHTDEVICE_H_