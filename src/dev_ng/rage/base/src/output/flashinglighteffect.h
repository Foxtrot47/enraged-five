//
// light/flashinglighteffect.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef LIGHT_FLASHINGLIGHTEFFECT_H_
#define LIGHT_FLASHINGLIGHTEFFECT_H_

#include "constantlighteffect.h"

// rage headers.
#include "vector/color32.h"

namespace rage
{
// PURPOSE: Represents a light effect that flashes a single color.
class ioFlashingLightEffect : public ioLightEffect
{
public:
	// PURPOSE: Default constructor represents no light.
	ioFlashingLightEffect();

	// PURPOSE:	Constructor.
	// PARAMS:	duration - the duration of the effect.
	//			The intensity to flash down to.
	//			red - the red color part.
	//			green - the green color part.
	//			blue - the blue color part.
	ioFlashingLightEffect(u32 duration, float lowIntensity, u8 red, u8 green, u8 blue);
	
	// PURPOSE:	Constructor.
	// PARAMS:	duration - the duration of the effect.
	//			The intensity to flash down to.
	//			color - the color  to use.
	ioFlashingLightEffect(u32 duration, float lowIntensity, Color32 color);
	
	// PURPOSE:	Destructor.
	virtual ~ioFlashingLightEffect() {}
	
	// PURPOSE: Update the light effect.
	// PARAMS:  progress - the progress through the light effect.
	//          device - the device the apply the effect to.
	// NOTES:   Update() is marked as a const method as ioLightEffects are stateless
	//          so the same affect can be applied to multiple devices.
	virtual void Update(float progress, ioLightDevice* device) const;

	// RETURNS: The red color component.
	u8 GetRed() const;

	// RETURNS: The green color component.
	u8 GetGreen() const;

	// RETURNS: The blue color component.
	u8 GetBlue() const;

	// RETURNS: The low flash intensity.
	float GetLowIntensity() const;
	
private:
	// PURPOSE: The intensity to flash down to.
	float m_LowIntensity;

	// PURPOSE:	The red color part.
	u8 m_Red;
	
	// PURPOSE: The green color part.
	u8 m_Green;
	
	// PURPOSE: The blue color part.
	u8 m_Blue;
};

inline ioFlashingLightEffect::ioFlashingLightEffect()
	: ioLightEffect(0)
	, m_LowIntensity(0)
	, m_Red(0)
	, m_Green(0)
	, m_Blue(0)
{}

inline ioFlashingLightEffect::ioFlashingLightEffect(u32 duration, float lowIntensity, u8 red, u8 green, u8 blue)
	: ioLightEffect(duration)
	, m_LowIntensity(lowIntensity)
	, m_Red(red)
	, m_Green(green)
	, m_Blue(blue)
{
	Assertf(lowIntensity >= 0.0f && lowIntensity <= 1.0f, "lowIntensity is invalid, check calling code!");
}

inline ioFlashingLightEffect::ioFlashingLightEffect(u32 duration, float lowIntensity, Color32 color)
 : ioLightEffect(duration)
 , m_LowIntensity(lowIntensity)
 , m_Red(color.GetRed())
 , m_Green(color.GetGreen())
 , m_Blue(color.GetBlue())
{
	Assertf(lowIntensity >= 0.0f && lowIntensity <= 1.0f, "lowIntensity is invalid, check calling code!");
}

inline u8 ioFlashingLightEffect::GetRed() const
{
	return m_Red;
}

inline u8 ioFlashingLightEffect::GetGreen() const
{
	return m_Green;
}

inline u8 ioFlashingLightEffect::GetBlue() const
{
	return m_Blue;
}

inline float ioFlashingLightEffect::GetLowIntensity() const
{
	return m_LowIntensity;
}

}

#endif // LIGHT_FLASHINGLIGHTEFFECT_H_