//
// output/rumbleeffect.h : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_RUMBLEEFFECT_H_
#define OUTPUT_RUMBLEEFFECT_H_

#include "actuatoreffect.h"
#include "input/pad.h"

namespace rage
{
// PURPOSE: A rumble effect for devices that support it.
class ioRumbleEffect: public ioActuatorEffect
{
public:
	// PURPOSE: Constructor.
	// NOTES:   Default constructor does not rumble any device.
	ioRumbleEffect();

	// PURPOSE: Constructor.
	// PARAMS:	duration - the duration of the the effect to.
	//          intensity - the intensity of the the rumble.
	ioRumbleEffect(u32 duration, float intensity);

	// PURPOSE: Constructor.
	// PARAMS:	duration - the duration of the the effect to.
	//          lightMotorIntensity - the intensity to apply to the light motor (0.0f - 1.0f).
	//          heavyMotorIntensity - the intensity to apply to the heavy motor (0.0f - 1.0f).
	ioRumbleEffect(u32 duration, float lightMotorIntensity, float heavyMotorIntensity);

	// PURPOSE: Destructor.
	virtual ~ioRumbleEffect(){}

	// PURPOSE: Update the force effect.
	// PARAMS:  progress - the progress of the effect. 0.0f is the start and 1.0f is the end.
	//          actuator - the actuator to apply the effect to.
	// NOTES:   Progress is not used for rumble effects.
	virtual void Update(float UNUSED_PARAM(progress), ioAcutatorDevice* actuator) const;

private:
	// PURPOSE: Sets the intensity of the motors.
	// PARAMS:  intensity - the intensity to set the motors to.
	void SetIntensity(float intensity);

	// PURPOSE: The light motor intensity to be applied (0.0f - 1.0f).
	float m_LightMotorIntensity;

	// PURPOSE: The heavy motor intensity to be applied (0.0f - 1.0f).
	float m_HeavyMotorIntensity;
};

inline ioRumbleEffect::ioRumbleEffect()
	: ioActuatorEffect(0)
	, m_LightMotorIntensity(0.0f)
	, m_HeavyMotorIntensity(0.0f)
{
}

inline ioRumbleEffect::ioRumbleEffect( u32 duration, float intensity )
	: ioActuatorEffect(duration)
{
	SetIntensity(intensity);
}

inline ioRumbleEffect::ioRumbleEffect(u32 duration, float lightMotorIntensity, float heavyMotorIntensity)
	: ioActuatorEffect(duration)
	, m_LightMotorIntensity(lightMotorIntensity)
	, m_HeavyMotorIntensity(heavyMotorIntensity)
{
	Assertf(lightMotorIntensity >= 0.0f && lightMotorIntensity <= 1.0f, "Invalid light motor intensity in ioRumbleEffect! Check calling code.");
	Assertf(heavyMotorIntensity >= 0.0f && heavyMotorIntensity <= 1.0f, "Invalid heavy motor intensity in ioRumbleEffect! Check calling code.");
}

#if HAS_TRIGGER_RUMBLE
// PURPOSE: A rumble effect for devices that support it.
class ioTriggerRumbleEffect: public ioActuatorEffect
{
public:
	// PURPOSE: Constructor.
	// NOTES:   Default constructor does not rumble any device.
	ioTriggerRumbleEffect();

	// PURPOSE: Constructor.
	// PARAMS:	duration - the duration of the the effect to.
	//          intensity - the intensity of the the rumble.
	ioTriggerRumbleEffect( u32 duration, float intensity );

	// PURPOSE: Constructor.
	// PARAMS:	duration - the duration of the the effect to.
	//          lightMotorIntensity - the intensity to apply to the light motor (0.0f - 1.0f).
	//          heavyMotorIntensity - the intensity to apply to the heavy motor (0.0f - 1.0f).
	ioTriggerRumbleEffect( u32 duration, float leftMotorIntensity, float rightMotorIntensity );

	// PURPOSE: Destructor.
	virtual ~ioTriggerRumbleEffect(){}

	// PURPOSE: Update the force effect.
	// PARAMS:  progress - the progress of the effect. 0.0f is the start and 1.0f is the end.
	//          actuator - the actuator to apply the effect to.
	// NOTES:   Progress is not used for rumble effects.
	virtual void Update(float UNUSED_PARAM(progress), ioAcutatorDevice* actuator) const;

private:
	// PURPOSE: Sets the intensity of the motors.
	// PARAMS:  intensity - the intensity to set the motors to.
	void SetIntensity(float intensity);

	// PURPOSE: The left motor intensity to be applied (0.0f - 1.0f).
	float m_LeftMotorIntensity;

	// PURPOSE: The right motor intensity to be applied (0.0f - 1.0f).
	float m_RightMotorIntensity;
};

inline ioTriggerRumbleEffect::ioTriggerRumbleEffect()
	: ioActuatorEffect(0)
	, m_LeftMotorIntensity(0.0f)
	, m_RightMotorIntensity(0.0f)
{
}

inline ioTriggerRumbleEffect::ioTriggerRumbleEffect( u32 duration, float intensity )
	: ioActuatorEffect( duration )
{
	SetIntensity( intensity );
}

inline ioTriggerRumbleEffect::ioTriggerRumbleEffect( u32 duration, float leftMotorIntensity, float rightMotorIntensity )
	: ioActuatorEffect( duration )
	, m_LeftMotorIntensity( leftMotorIntensity )
	, m_RightMotorIntensity( rightMotorIntensity )
{
	Assertf(leftMotorIntensity >= 0.0f && leftMotorIntensity <= 1.0f, "Invalid left motor intensity in ioTriggerRumbleEffect! Check calling code.");
	Assertf(rightMotorIntensity >= 0.0f && rightMotorIntensity <= 1.0f, "Invalid right motor intensity in ioTriggerRumbleEffect! Check calling code.");
}
#endif // #if HAS_TRIGGER_RUMBLE

}

#endif //OUTPUT_RUMBLEEFFECT_H_