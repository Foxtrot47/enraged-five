//
// output/actuatoreffect.h : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_ACTUATOREFFECT_H_
#define OUTPUT_ACTUATOREFFECT_H_

namespace rage
{
class ioAcutatorDevice;

// PURPOSE: Base class for actuator effects.
// NOTES:   An actuator effect must be stateless i.e. not require a previous frame state in
//          order to process a subsequent frame.
class ioActuatorEffect
{
public:
	// PURPOSE: Destructor.
	virtual ~ioActuatorEffect() {}

	// PURPOSE: Update the force effect.
	// PARAMS:	progress - the progress of the effect in the range 0.0f to 1.0f.
	//          actuator - the actuator to apply the effect to.
	// NOTES:   Update() is marked as const as ioActuatorEffect's should be stateless.
	virtual void Update(float progress, ioAcutatorDevice* actuator) const = 0;

	// PURPOSE: Retrieves the duration of that the effect will be applied to.
	u32 GetDuration() const;

protected:
	// PURPOSE: Can only be constructed by a sub-class.
	// PARAMS:	duration - the duration of the effect to.
	ioActuatorEffect(u32 duration);

private:
	// PURPOSE: The duration of the effect to.
	u32 m_Duration;
};

inline ioActuatorEffect::ioActuatorEffect(u32 duration)
	: m_Duration(duration)
{
}

inline u32 ioActuatorEffect::GetDuration() const
{
	return m_Duration;
}

}

#endif // OUTPUT_ACTUATOREFFECT_H_