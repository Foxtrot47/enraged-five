//
// output/actuatordevice.h : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_ACTUATORDEVICE_H_
#define OUTPUT_ACTUATORDEVICE_H_

namespace rage
{
// PURPOSE: Represents an actuatable object.
// NOTES:   Concrete implementations of actuator handle multiple actuator values e.g.
//          if an actuator is given two different values between the call to Update()
//          then the implementation decides which is used or how these value are
//          interpreted.
class ioAcutatorDevice
{
public:
	// PURPOSE: Destructor.
	virtual ~ioAcutatorDevice() {}

	// PURPOSE: Represents the possible actuators a device can support.
	enum Actuator
	{
		// PURPOSE: Represents a heavy rumble motor.
		HEAVY_RUMBLE = 0,

		// PURPOSE: Represents a light rumble motor.
		LIGHT_RUMBLE,

		// PURPOSE: Represents a lift trigger rumble.
		LEFT_TRIGGER_RUMBLE,

		// PURPOSE: Represents a right trigger rumble.
		RIGHT_TRIGGER_RUMBLE,

		// PURPOSE: Represents an x-directional force.
		X_FORCE,

		// PURPOSE: Represents a y-directional force.
		Y_FORCE,

		// PURPOSE: Represents a z-directional force.
		Z_FORCE,
	};

	// PURPOSE: Indicates that this device supports rumble actuators.
	virtual bool SupportsRumble() const = 0;

	// PURPOSE: Indicates that this device supports force actuators.
	virtual bool SupportsForce() const = 0;

	// PURPOSE: Indicates if the device supports a specific actuator.
	// PRAMS:   actuator - the actuator to check.
	// RETURNS: true if the specified actuator is supported.
	virtual bool HasActuator(Actuator actuator) const = 0;

	// PURPOSE: Sets a supported actuator's value.
	// PRAMS:   actuator - the actuator to set.
	//          value - the value to set the actuator two.
	virtual void SetActuator(Actuator actuator, float value) = 0;

	// PURPOSE: Indicates if a supported actuator has a corresponding input source.
	// PARAMS:	actuator - the actuator to check.
	// RETURNS: true if the supported actuator has an input source.
	// NOTES:	Unsupported actuators will return false. An example of a corresponding
	//          input would be the trigger's input value for a trigger rumble or a
	//          steering wheel's position in a force feedback wheel.
	virtual bool HasInputValue(Actuator actuator) const = 0;

	// PURPOSE: Retrieves the input value of a corresponding input source.
	// PARAMS:	actuator - the actuator to check.
	// RETURNS: the input value or 0.0f if there is no input value.
	// NOTES:	The return value will not be dead-zoned. An example of a corresponding input
	//			would be the trigger's input value for a trigger rumble or a steering wheel's
	//			position in a force feedback wheel.
	virtual float GetInputValue(Actuator actuator) const = 0;

	// PURPOSE:	Updates the actuator(s).
	// NOTES:   If concrete implementations cache/merge actuator updates, they can/should apply
	//          them here.
	virtual void Update() = 0;

protected:
	// PURPOSE: ioActuator can only be created by concrete implementations.
	ioAcutatorDevice();
};

inline ioAcutatorDevice::ioAcutatorDevice()
{
}

}

#endif //  OUTPUT_ACTUATORDEVICE_H_
