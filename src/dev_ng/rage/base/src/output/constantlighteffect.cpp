//
// light/constantlighteffect.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "constantlighteffect.h"

// rage headers.
#include "lightdevice.h"

namespace rage
{
void ioConstantLightEffect::Update(float UNUSED_PARAM(progress), ioLightDevice* device) const
{
	if(device != NULL)
	{
		for(u32 i = 0; i < ioLightDevice::MAX_LIGHT_SOURCES; ++i)
		{
			device->SetLightColor(static_cast<ioLightDevice::Source>(i), m_Red, m_Green, m_Blue);
		}

		device->Update();
	}
}
}