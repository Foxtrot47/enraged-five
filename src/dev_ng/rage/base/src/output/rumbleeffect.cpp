//
// output/rumbleeffect.cpp : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "rumbleeffect.h"

// rage headers.
#include "input/pad.h"
#include "math/amath.h"
#include "output/actuatordevice.h"

namespace rage
{
void ioRumbleEffect::Update( float UNUSED_PARAM(progress), ioAcutatorDevice* actuator ) const
{
	if(actuator != NULL && actuator->SupportsRumble())
	{
		actuator->SetActuator(ioAcutatorDevice::HEAVY_RUMBLE, m_HeavyMotorIntensity);
		actuator->SetActuator(ioAcutatorDevice::LIGHT_RUMBLE, m_LightMotorIntensity);
	}
}

void ioRumbleEffect::SetIntensity( float intensity )
{
	Assertf(intensity >= 0.0f && intensity <= 1.0f, "Invalid intensity in ioRumbleEffect! Check calling code.");

	// NOTE: This code was taken from CPad for figuring out how a single intensity is distributed across two motors.
	intensity = Clamp(intensity,0.0f,1.0f);

	// At low intensity only motor 1 plays (it is weaker)
	// then motor 0 replaces it
	// ..finally both come in

#if __PS3
	static dev_float minPadShakeFreq0 = 100.0f;
	static dev_float maxPadShakeFreq0 = 255.0f;
	static dev_float minPadShakeFreq1 = 1.0f;
	static dev_float maxPadShakeFreq1 = 255.0f;
	static dev_float motor1Stop = -1.0f;			// Don't use motor 1 at low end for ps3
	static dev_float bothMotorStart = 0.9f;
#else
	static dev_float minPadShakeFreq0 = 15.0f;
	static dev_float maxPadShakeFreq0 = 200.0f;
	static dev_float minPadShakeFreq1 = 50.0f;
	static dev_float maxPadShakeFreq1 = 120.0f;
	static dev_float motor1Stop = 0.1f;
	static dev_float bothMotorStart = 0.9f;
#endif

	if(intensity == 0.0f)
	{
		m_HeavyMotorIntensity = 0.0f;
		m_LightMotorIntensity = 0.0f;
	}
	else if (intensity < motor1Stop)
	{
		m_HeavyMotorIntensity = 0.0f;
		m_LightMotorIntensity = (minPadShakeFreq1 + ((maxPadShakeFreq1 - minPadShakeFreq1)*(intensity/motor1Stop))) / maxPadShakeFreq1;
	}
	else if (intensity < bothMotorStart)
	{
		m_HeavyMotorIntensity = (minPadShakeFreq0 + ((maxPadShakeFreq0 - minPadShakeFreq0)*intensity)) / maxPadShakeFreq0;
		m_LightMotorIntensity = 0.0f;
	}
	else
	{
		m_HeavyMotorIntensity = (minPadShakeFreq0 + ((maxPadShakeFreq0 - minPadShakeFreq0)*intensity)) / maxPadShakeFreq0;
		m_LightMotorIntensity = maxPadShakeFreq1;
	}
}

#if HAS_TRIGGER_RUMBLE
void ioTriggerRumbleEffect::Update( float UNUSED_PARAM(progress), ioAcutatorDevice* actuator ) const
{
	if( actuator != NULL && actuator->SupportsRumble() )
	{
		actuator->SetActuator( ioAcutatorDevice::LEFT_TRIGGER_RUMBLE, m_LeftMotorIntensity );
		actuator->SetActuator( ioAcutatorDevice::RIGHT_TRIGGER_RUMBLE, m_RightMotorIntensity );
	}
}

void ioTriggerRumbleEffect::SetIntensity( float intensity )
{
	Assertf(intensity >= 0.0f && intensity <= 1.0f, "Invalid intensity in ioRumbleEffect! Check calling code.");

	// NOTE: This code was taken from CPad for figuring out how a single intensity is distributed across two motors.
	intensity = Clamp(intensity,0.0f,1.0f);

	// At low intensity only motor 1 plays (it is weaker)
	// then motor 0 replaces it
	// ..finally both come in

	static dev_float minPadShakeFreq0 = 15.0f;
	static dev_float maxPadShakeFreq0 = 200.0f;
	//static dev_float minPadShakeFreq1 = 50.0f;
	static dev_float maxPadShakeFreq1 = 120.0f;
	//static dev_float motor1Stop = 0.1f;
	//static dev_float bothMotorStart = 0.9f;

	if(intensity == 0.0f)
	{
		m_LeftMotorIntensity = 0.0f;
		m_RightMotorIntensity = 0.0f;
	}
	else
	{
		m_LeftMotorIntensity = (minPadShakeFreq0 + ((maxPadShakeFreq0 - minPadShakeFreq0)*intensity)) / maxPadShakeFreq0;
		m_RightMotorIntensity = maxPadShakeFreq1;
	}
}
#endif // #if HAS_TRIGGER_RUMBLE

};
