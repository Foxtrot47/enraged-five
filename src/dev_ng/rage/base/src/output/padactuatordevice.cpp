//
// output/padactuator.cpp : base class for all entity archetypes
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "padactuatordevice.h"

// rage headers
#include "math/amath.h"

namespace rage
{
bool ioPadActuatorDevice::SupportsRumble() const
{
	return true;
}

bool ioPadActuatorDevice::SupportsForce() const
{
	return false;
}

bool ioPadActuatorDevice::HasActuator(Actuator actuator) const
{
	switch(actuator)
	{
	case HEAVY_RUMBLE:
	case LIGHT_RUMBLE:
#if HAS_TRIGGER_RUMBLE
	case LEFT_TRIGGER_RUMBLE:
	case RIGHT_TRIGGER_RUMBLE:
#endif // HAS_TRIGGER_RUMBLE
		return true;

	default:
		return false;
	}
}

void ioPadActuatorDevice::SetActuator(Actuator actuator, float value)
{
	switch(actuator)
	{
	case HEAVY_RUMBLE:
		UpdateActuator(ioPad::HEAVY_MOTOR, value);
		break;

	case LIGHT_RUMBLE:
		UpdateActuator(ioPad::LIGHT_MOTOR, value);
		break;

#if HAS_TRIGGER_RUMBLE
	case LEFT_TRIGGER_RUMBLE:
		UpdateActuator(ioPad::LEFT_TRIGGER, value);
		break;

	case RIGHT_TRIGGER_RUMBLE:
		UpdateActuator(ioPad::RIGHT_TRIGGER, value);
		break;
#endif // HAS_TRIGGER_RUMBLE

	default:
		break;
	}
}

bool ioPadActuatorDevice::HasInputValue(Actuator TRIGGER_RUMBLE_ONLY(actuator)) const
{
#if HAS_TRIGGER_RUMBLE
	if (actuator == LEFT_TRIGGER_RUMBLE || actuator == RIGHT_TRIGGER_RUMBLE)
	{
		return true;
	}
#endif // HAS_TRIGGER_RUMBLE

	return false;
}

float ioPadActuatorDevice::GetInputValue(Actuator TRIGGER_RUMBLE_ONLY(actuator)) const
{
#if HAS_TRIGGER_RUMBLE
	// On gamepads only trigger rumbles had an associated input value (the trigger).
	if(actuator == LEFT_TRIGGER_RUMBLE)
	{
		return static_cast<float>(ioPad::GetPad(m_PadIndex).GetAnalogButton(ioPad::L2_INDEX)) / 255.0f;
	}
	else if(actuator == RIGHT_TRIGGER_RUMBLE)
	{
		return static_cast<float>(ioPad::GetPad(m_PadIndex).GetAnalogButton(ioPad::R2_INDEX)) / 255.0f;
	}
#endif // HAS_TRIGGER_RUMBLE

	return 0.0f;
}

void ioPadActuatorDevice::Update()
{
	ioPad& pad = ioPad::GetPad(m_PadIndex);

	for(u32 i = 0; i < ioPad::MAX_ACTUATORS; ++i)
	{
		pad.SetActuatorValue(i, m_ActuatorValues[i]);
	}

	Reset();
}

void ioPadActuatorDevice::Reset()
{
	for(u32 i = 0; i < ioPad::MAX_ACTUATORS; ++i)
	{
		m_ActuatorValues[i] = 0.0f;
	}
}

void ioPadActuatorDevice::ScaleActuators( float scale )
{
	// Scale all cached actuator values.
	scale = Clamp(scale, 0.0f, 1.0f);
	for (u32 i = 0; i < ioPad::MAX_ACTUATORS; ++i)
	{
		m_ActuatorValues[i] *= scale;
	}
}

}