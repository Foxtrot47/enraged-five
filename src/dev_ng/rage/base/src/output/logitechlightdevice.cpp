// output/padlightdevice.cpp
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#if RSG_PC

#include "logitechlightdevice.h"

#include "profile/timebars.h"

// The logitech library uses BYTE but does not define what that is.
typedef rage::u8 BYTE;
#include "Logitech/LEDSDK_8.58.102/Include/LogitechLEDLib.h"
#pragma comment(lib, "LogitechLEDLib.lib")

#define PER_KEY_REQUIRED_MAJOR	8
#define PER_KEY_REQUIRED_MINOR	58
#define PER_KEY_REQUIRED_BUILD	102

namespace rage
{
ioLogitechLedDevice::ioLogitechLedDevice()
	: m_Initialized(false)
#if SUPPORT_PER_KEY_LIGHTS
	, m_Updated(false)
	, m_HasCompatibleDrivers(false)
#endif // SUPPORT_PER_KEY_LIGHTS
{
	m_Initialized = LogiLedInit();

	PER_KEY_LIGHTS_ONLY(m_HasCompatibleDrivers = HasCompatibleDrivers());
	PER_KEY_LIGHTS_ONLY(ClearCachedColors());
}

ioLogitechLedDevice::~ioLogitechLedDevice()
{
	LogiLedShutdown();
	m_Initialized = false;
}

bool ioLogitechLedDevice::IsValid() const
{
	return m_Initialized;
}

bool ioLogitechLedDevice::HasLightSource(Source NOT_PER_KEY_LIGHTS_ONLY(source)) const
{
	return m_Initialized NOT_PER_KEY_LIGHTS_ONLY(&& source == ioLightDevice::MIDDLE);
}

void ioLogitechLedDevice::SetLightColor(Source source, u8 red, u8 green, u8 blue)
{
#if SUPPORT_PER_KEY_LIGHTS
	if(red != m_RegionColors[source][RED] || green != m_RegionColors[source][GREEN] || blue != m_RegionColors[source][BLUE])
	{
		m_RegionColors[source][RED]   = red;
		m_RegionColors[source][GREEN] = green;
		m_RegionColors[source][BLUE]  = blue;
		m_Updated = true;
	}
#else
	if(source == ioLightDevice::MIDDLE)
	{
		SetKeyboardColor(red, green, blue);
	}
#endif // SUPPORT_PER_KEY_LIGHTS
}

void ioLogitechLedDevice::Update()
{
#if SUPPORT_PER_KEY_LIGHTS
	if(m_Updated)
	{
		PF_AUTO_PUSH_TIMEBAR("LogitechLightDevice.Update");
		// For keyboards that do not have regions we set the whole board to the LEFT color so we get the flashing effects.
		SetKeyboardColor(m_RegionColors[MIDDLE][RED], m_RegionColors[MIDDLE][GREEN], m_RegionColors[MIDDLE][BLUE]);

		if(m_HasCompatibleDrivers)
		{
			PF_PUSH_TIMEBAR("LogitechLightDevice.Update.CalculateKeyLights");
			m_Updated = false;

			atRangeArray<BYTE, LOGI_LED_BITMAP_SIZE> keyColors;
			for(u32 y = 0; y < LOGI_LED_BITMAP_HEIGHT; ++y)
			{
				const u32 yIndex = y * LOGI_LED_BITMAP_WIDTH * LOGI_LED_BITMAP_BYTES_PER_KEY;

				// Set the left region lights.
				for(u32 x = 0; x < LOGI_LED_BITMAP_WIDTH; ++x)
				{
					const u32 xIndex = x * LOGI_LED_BITMAP_BYTES_PER_KEY;

					Source region = MIDDLE;

					// Flash the F keys like police lights!
					// NOTE: This is a bit map so the function keys go from 1 (as 0 is ESC) on the top row.
					if(y == 0)
					{
						// F1 - F6
						if(x >= 1 && x <= 6)
						{
							region = LEFT;
						}
						// F7 - F12
						else if(x >= 7 && x <= 12)
						{
							region = RIGHT;
						}
					}

					keyColors[yIndex + xIndex + RED]   = m_RegionColors[region][RED];
					keyColors[yIndex + xIndex + GREEN] = m_RegionColors[region][GREEN];
					keyColors[yIndex + xIndex + BLUE]  = m_RegionColors[region][BLUE];
					keyColors[yIndex + xIndex + ALPHA] = 255; // 100%
				}
			}
			PF_POP_TIMEBAR();

			PF_PUSH_TIMEBAR("LogitechLightDevice.Update.LogiLedSetLightingFromBitmap");
			LogiLedSetTargetDevice(LOGI_DEVICETYPE_PERKEY_RGB);
			
			// Now set regions for keyboard that support it.
			LogiLedSetLightingFromBitmap(keyColors.GetElements());
			PF_POP_TIMEBAR();
		}
	}
#endif // SUPPORT_PER_KEY_LIGHTS
}

void ioLogitechLedDevice::SetAllLightsToDefaultColor()
{
	PER_KEY_LIGHTS_ONLY(ClearCachedColors());

	PER_KEY_LIGHTS_ONLY(LogiLedSetTargetDevice(LOGI_DEVICETYPE_ALL));
	LogiLedRestoreLighting();
}

void ioLogitechLedDevice::SetKeyboardColor(u8 red, u8 green, u8 blue)
{
	PF_AUTO_PUSH_TIMEBAR("LogitechLightDevice.SetKeyboardColor");

#if SUPPORT_PER_KEY_LIGHTS
	// Only target the device if we are using a per key effect.
	if(m_HasCompatibleDrivers)
	{
		LogiLedSetTargetDevice(LOGI_DEVICETYPE_RGB);
	}
#endif // SUPPORT_PER_KEY_LIGHTS

	// NOTE: The range of these parameters are 0-100.
	LogiLedSetLighting(	ConvertColorToPercentage(red),
						ConvertColorToPercentage(green),
						ConvertColorToPercentage(blue) );
}

#if SUPPORT_PER_KEY_LIGHTS
void ioLogitechLedDevice::ClearCachedColors()
{
	for(s32 i = 0; i < m_RegionColors.size(); ++i)
	{
		for(s32 j = 0; j < m_RegionColors[i].size(); ++j)
		{
			m_RegionColors[i][j] = 255; // 100% on.
		}
	}
}

bool ioLogitechLedDevice::HasCompatibleDrivers() const
{
	int major = 0;
	int minor = 0;
	int build = 0;

	// If we cannot get the driver version then assume not compatible.
	if(!m_Initialized || !LogiLedGetSdkVersion(&major, &minor, &build))
		return false;

	// Check the major version number.
	if(major < PER_KEY_REQUIRED_MAJOR)
		return false;
	// We only want to check the minor if the major is equal (bigger is automatically supported).
	else if(major == PER_KEY_REQUIRED_MAJOR)
	{
		if(minor < PER_KEY_REQUIRED_MINOR)
			return false;
		// We only want to check the build if the minor is equal (bigger is automatically supported).
		else if(minor == PER_KEY_REQUIRED_MINOR && build < PER_KEY_REQUIRED_BUILD)
			return false;
	}

	return true;
}

#endif // SUPPORT_PER_KEY_LIGHTS

}

#endif // RSG_PC