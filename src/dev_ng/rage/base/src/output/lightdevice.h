//
// output/lightdevice.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_LIGHTDEVICE_H
#define OUTPUT_LIGHTDEVICE_H

#include "vector/color32.h"

namespace rage
{
// PURPOSE: Represents a device that can output light.
class ioLightDevice
{
public:
	// PURPOSE: Destructor.
	virtual ~ioLightDevice() {}
	
	// PURPOSE: Represents a source of light.
	// NOTES:   A source might not represent a single light but could represent a
	//          collection of lights. For example, a device that has a top-left 
	//          and bottom-left light could group these as LEFT.
	enum Source
	{
		// PURPOSE: Light source on the left.
		LEFT = 0,
		
		// PURPOSE: Light source in the middle.
		MIDDLE,
		
		// PURPOSE: Light source on the right.
		RIGHT,
		
		// PURPOSE: The maximum number of possible light sources.
		MAX_LIGHT_SOURCES,
	};

	// PURPOSE: Indicates that the light device is valid and working.
	virtual bool IsValid() const = 0;
	
	// PURPOSE: Indicates if a given light source is available.
	// PARAMS:  source - the source to check.
	virtual bool HasLightSource(Source source) const = 0;
	
	// PURPOSE: Sets a light source's color.
	// PARAMS:  source - the light source to set.
	//          red - the red color part.
	//          green - the green part.
	//          blue - the blue part.
	// NOTES:	On some devices this will set the color immediately, other devices will do this in the Update() function.
	//			Once all sources have been set, call Update().
	virtual void SetLightColor(Source source, u8 red, u8 green, u8 blue) = 0;
	
	// PURPOSE: Sets a light source's color.
	// PARAMS:  source - the light source to set.
	//          color - the color to set the light source to.
	void SetLightColor(Source, Color32 color);

	// PURPOSE: Sets all lights source's colors to their defaults (if they have one).
	virtual void SetAllLightsToDefaultColor() = 0;

	// PURPOSE: Updates the light effect color.
	// NOTES:	Some devices need to send all the light effects in one go for each region, if this is the case this is done here.
	virtual void Update() = 0;
	
protected:
	// PURPOSE: Only concrete implementations can create an ioLightDevice.
	ioLightDevice();
};

inline ioLightDevice::ioLightDevice()
{
}

inline void ioLightDevice::SetLightColor(Source source, Color32 color)
{
	SetLightColor(source, color.GetRed(), color.GetGreen(), color.GetBlue());
}

}

#endif // OUTPUT_LIGHTDEVICE_H