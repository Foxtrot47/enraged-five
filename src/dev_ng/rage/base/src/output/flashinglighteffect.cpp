//
// light/flashinglighteffect.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "flashinglighteffect.h"

// rage headers.
#include "lightdevice.h"

namespace rage
{
void ioFlashingLightEffect::Update(float progress, ioLightDevice* device) const
{
	if(device != NULL)
	{
		float intenisty;
		if(progress < 0.5f)
		{
			intenisty = 1.0f;
		}
		else
		{
			intenisty = m_LowIntensity;
		}

		for(u32 i = 0; i < ioLightDevice::MAX_LIGHT_SOURCES; ++i)
		{
			device->SetLightColor( static_cast<ioLightDevice::Source>(i),
								   static_cast<u8>(m_Red * intenisty),
								   static_cast<u8>(m_Green * intenisty),
								   static_cast<u8>(m_Blue * intenisty) );
		}

		device->Update();
	}
}
}