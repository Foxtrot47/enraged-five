//
// output/lighteffect.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef OUTPUT_LIGHTEFFECT_H_
#define OUTPUT_LIGHTEFFECT_H_

namespace rage
{

class ioLightDevice;

// PURPOSE: Represents an effect that can play on an output light source.
// NOTES:   Avoid triggering seizure. (5Hz-70Hz, which is 14ms to 200ms, is bad,
//          http://www.birket.com/technical-library/144)
//          ioLightEffects are stateless and should not maintain state between calls
//          to Update(). This is so an ioLightEffect can be used on multiple devices.
class ioLightEffect
{
public:
	// PURPOSE: Destructor.
	virtual ~ioLightEffect() {}

	// PURPOSE: Update the light effect.
	// PARAMS:  progress - the progress through the light effect.
	//          device - the device the apply the effect to.
	// NOTES:   Update() is marked as a const method as ioLightEffects are stateless
	//          so the same affect can be applied to multiple devices.
	virtual void Update(float progress, ioLightDevice* device) const = 0;
	
	// PURPOSE: Retrieves the duration of the effect.
	u32 GetDuration() const;

protected:
	// PURPOSE: ioLightEffect can only be created through a concrete implementation.
	// PARAMS   duration - the duration of the effect.
	ioLightEffect(u32 duration);
	
private:
	// PURPOSE: The duration of the effect.
	u32 m_Duration;
};

inline ioLightEffect::ioLightEffect(u32 duration)
	: m_Duration(duration)
{
}

inline u32 ioLightEffect::GetDuration() const
{
	return m_Duration;
}
}

#endif // OUTPUT_LIGHTEFFECT_H_