#ifndef ZLIB_INFLATESERVER_H
#define ZLIB_INFLATESERVER_H

#include <cell/spurs/event_flag.h>

#include "inflateState.h"

namespace rage {

// Zero is not a valid command because we use the event flag to pass it
enum Command { CMD_RESET = 1, CMD_INFLATE, CMD_COPY, CMD_EXIT };

struct SharedBuffer {
	CellSpursEventFlag ppu2spu;
	CellSpursEventFlag spu2ppu;
	
	zlibInflateState state;
} __attribute__((aligned(128)));

}	// namespace rage

#endif	// ZLIB_INFLATESERVER_H
