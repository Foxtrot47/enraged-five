REM Sets compile variables for inflateServer.task
REM ASM version - replace %CD%\edgezlib_inffast.spu.s by %CD%\inffast.c for default version
SET CPPFLAGS=%CPPFLAGS% -DNO_GZIP 

REM   Here we set DEF_WARN in order to remove the -Wno-reorder.
set DEF_WARN=-Wall -Werror -Wundef -Wno-multichar
set COMPILE_SET_0_FILES=%COMPILE_SET_0_FILES% inflate.cpp edgezlib_inffast.spu.s inftrees.cpp zutil.cpp


