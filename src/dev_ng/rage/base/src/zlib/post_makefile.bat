echo You need to have ProDG VSI v1.6.12 or higher installed now.
echo Otherwise you'll get compile errors in zlib and jpeg.
exit/b

for %%I in (%ARCHIVE% %ARCHIVE%_2005 %ARCHIVE%_dbr_2005) do (
	sed s/-frtti//g %%I.vcproj > tmp
	sed s/-fno-rtti//g tmp > %%I.vcproj
	sed s/-Wno-reorder//g %%I.vcproj > tmp
	move tmp %%I.vcproj
)
