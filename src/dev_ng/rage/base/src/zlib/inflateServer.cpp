#if __SPU

#include "system\dma.h"

#include <cell/spurs/task.h>
#include <cell/dma.h>

#include <string.h>		// for memset
#include <stdlib.h>

#include "inflateServer.h"
#include "zlib.h"

#include "profile/cellspurstrace.h"

extern "C" char _end[];

#if __FINAL
#undef Quitf
#define Quitf(...) while (true);
#endif

namespace rage {
RAGETRACE_ONLY(pfSpuTrace* g_pfSpuTrace = 0;)
} // namespace rage

using namespace rage;

struct Buffer {
	void Init(Byte *d,size_t s,int c) {
		data = d;
		size = 0;
		capacity = s;
		channel = c;
		channelMask = 1<<c;
	}

	Byte *data;
	size_t size, capacity;
	int channel, channelMask, pending;

	void BeginFill(zlibInflateState &state) {
		Assert(size == 0);
		size_t amt = state.avail_in;
		if (amt > capacity)
			amt = capacity;
		cellDmaLargeGet(data, (uint64_t)state.next_in, amt, channel, 0, 0);
		state.next_in = (void*)((char*)state.next_in + amt);
		state.total_in += amt;
		state.avail_in -= amt;
		pending = true;
		size = amt;
	}
	void EndFill() {
		if (pending) {
			cellDmaWaitTagStatusAll(channelMask);
			pending = false;
		}
	}
	void BeginSend(zlibInflateState &state) {
		Assert(size && size <= state.avail_out[0]);
		cellDmaLargePut(data, (uint64_t)state.next_out[0], size, channel, 0, 0);
		state.next_out[0] = (void*)((char*)state.next_out[0] + size);
		state.total_out += size;
		state.avail_out[0] -= size;

		// Move to the next chunk if we're done with this one.
		if (state.avail_out[0] == 0) {
			for (int x=1; x<zlibInflateState::MAX_OUTPUT_CHUNKS; x++) {
				state.avail_out[x-1] = state.avail_out[x];
				state.next_out[x-1] = state.next_out[x];
			}

			state.next_out[zlibInflateState::MAX_OUTPUT_CHUNKS-1] = NULL;
			state.avail_out[zlibInflateState::MAX_OUTPUT_CHUNKS-1] = 0;
		}
		pending = true;
		size = 0;
	}
	void EndSend() {
		if (pending) {
			cellDmaWaitTagStatusAll(channelMask);
			pending = false;
		}
	}
};

int *stackLevel;

void findStackLevel()
{
	int myself;
	stackLevel = &myself;
}

extern "C"
int cellSpursTaskMain(qword argTask, uint64_t argTaskset)
{
	z_stream zs;

	uint64_t cb_ea = argTaskset;
	uint64_t event_ppu2spu_ea = cb_ea;
	uint64_t event_spu2ppu_ea = cb_ea + 128;
	uint64_t state_ea = cb_ea + 256;
	// Displayf("inflateServer started - Comm buffer at %x, _end at %x",(int)cb_ea,(int)_end);

	memset(&zs, 0, sizeof(zs));
	int err = inflateInit2(&zs,-MAX_WBITS);
	if (err < 0)
		Quitf("inflateServer - inflateInit failed, code %d",err);

	static zlibInflateState state __attribute__((aligned(128)));
	static Byte inputBuffers[2][4096] __attribute__((aligned(128)));
	static Byte outputBuffers[2][8192] __attribute__((aligned(128)));

	Buffer inputs[2], outputs[2];
	int nextInputDma = 0, nextInputToZlib = 0;
	int nextOutputFromZlib = 0;
	int debugSpew = 0;
	uint16_t mask;

// #define dprintf(x) if (debugSpew >= 99999999) Displayf x
#define dprintf(x) // Displayf x
	
	cellSpursEventFlagSet(event_spu2ppu_ea, 0xffff); //dont let the PPU continue until the init has received this event
	// Displayf("inflateServer sent start event to PPU");

	// DO NOT declare any local variables after this point, we want to make sure our gonzo stack-measuring
	// code is sufficient.
	findStackLevel();
	Displayf("**** inflateServer - _end at %p, stack at %p",_end,stackLevel);
	if ((int)_end > 128*1024)
		Quitf("inflateServer - _end is beyond 128k, fix the PPU code (second param in cellSpursTaskGenerateLsPattern in inflateClient.cpp)");
	if ((int)stackLevel < (128+120)*1024)
		Quitf("inflateServer - stack is larger than 8k, fix the PPU code (third param in cellSpursTaskGenerateLsPattern in inflateClient.cpp)");

	for (;;) {
		// Now that we're no longer a run-to-complete task, do a proper wait here so we'll yield if necessary.
		// Should fix some PAsuite problems that showed up as well.
		for (;;) {
			mask = 0xFFFF;
			int result = cellSpursEventFlagWait(event_ppu2spu_ea, &mask, CELL_SPURS_EVENT_FLAG_OR);
			if (result)
				Errorf("inflateServer - Wait returned code %x",result);
			else
				break;
		}
		// Reset?
		if (mask == CMD_RESET) {
			inflateReset(&zs);

			zs.avail_in = 0;
			zs.avail_out = 0;

			inputs[0].Init(inputBuffers[0],sizeof(inputBuffers[0]),0);
			inputs[1].Init(inputBuffers[1],sizeof(inputBuffers[1]),1);

			outputs[0].Init(outputBuffers[0],sizeof(outputBuffers[0]),2);
			outputs[1].Init(outputBuffers[1],sizeof(outputBuffers[1]),3);
			++debugSpew;	// HACK - start spewing on third streaming attempt.
		}
		else if (mask == CMD_INFLATE) {
			/*	This function is called when we either have new input data, more output space,
				or both.  We maintain a double-buffered input buffer which we attempt to keep
				as full as possible.  In the startup case we will initiate our first DMA and
				block on its completion before doing any decompression.

				An input buffer will always be full unless it's the end of the stream.

				An output buffer will only be flushed back to main memory if it's full (which
				is guaranteed to maintain alignment) or it's the last packet.

				Typical flow control:

				Reset(state,size_t,size_t) (informs server how much total data to consume and produce).
				Inflate(state);
					DMA from input into input buffer 0
					if more data, DMA from input to input buffer 1
					wait for input buffer 0 to complete
					set up output buffer 0
					inflate()
					if input buffer 0 fully consumed, switch to input 1
					if output buffer 0 filled (or done), DMA from output buffer 0 to output
			*/


			// read state from ppu
			cellDmaGet(&state, state_ea, sizeof(state), 8, 0, 0);
			cellDmaWaitTagStatusAll(1<<8);

#if RAGETRACE
			uint32_t traceData = state.traceSpu;
			rage::pfSpuTrace trace(traceData);
			if (traceData)
			{
				rage::g_pfSpuTrace = &trace;
				trace.Push((rage::pfTraceCounterId*)(state.traceId));
			}
#endif

			// prime the first dma
			if (zs.total_in == 0) {
				dprintf(("fill input 0 with (at most) %lu",state.avail_in));
				nextOutputFromZlib = 0;
				nextInputToZlib = 0;
				nextInputDma = 1;
				inputs[0].BeginFill(state);
			}

			do {
				// Schedule next dma?
				if (!inputs[nextInputDma].size && state.avail_in) {
					dprintf(("fill input %d with (at most) %lu",nextInputDma,state.avail_in));
					inputs[nextInputDma].BeginFill(state);
					nextInputDma ^= 1;
				}

				// If there's no input for zlib, wait for some
				if (!zs.avail_in) {
					dprintf(("wait for input %d dma complete",nextInputToZlib));
					zs.next_in = inputs[nextInputToZlib].data;
					zs.avail_in = inputs[nextInputToZlib].size;
					inputs[nextInputToZlib].EndFill();
				}

				// Do we need a new output buffer?
				if (zs.avail_out == 0) {
					if (outputs[nextOutputFromZlib].pending) {
						dprintf(("wait for output %d dma complete",nextOutputFromZlib));
						outputs[nextOutputFromZlib].EndSend();
					}
					dprintf(("setup output buffer %d",nextOutputFromZlib));
					zs.next_out = outputs[nextOutputFromZlib].data;
					zs.avail_out = outputs[nextOutputFromZlib].capacity;
					if (state.avail_out[0] < zs.avail_out)
						zs.avail_out = state.avail_out[0];
					outputs[nextOutputFromZlib].size = zs.avail_out;
				}

				dprintf(("BEFORE INFLATE zs next_in=%p avail_in=%u  next_out=%p avail_out=%u",zs.next_in,zs.avail_in,zs.next_out,zs.avail_out));
				int err = inflate(&zs,0);
				if (err < 0)
				{
					// We're about to quit anyway, may as well just use one of our internal buffers as temp storage.
#if !__FINAL
					sysDmaGetAndWait(inputBuffers[0], (u32) state.debugName, 128, 1);
					const char *debugName = (const char *) inputBuffers[0];
#else // !__FINAL
	#if !__SPU && __NO_OUTPUT		// Quitf is a macro on SPU
					const char *debugName = "?";
	#endif // __NO_OUTPUT
#endif // !__FINAL

					Quitf("error in inflate - %d (debugSpew = %d), while decompressing %s. next_in=%p, avail_in=%u, next_out=%p, avail_out=%u",
						err,debugSpew,debugName,zs.next_in,zs.avail_in,zs.next_out,zs.avail_out);
				}
				dprintf(("AFTER INFLATE zs next_in=%p avail_in=%u  next_out=%p avail_out=%u",zs.next_in,zs.avail_in,zs.next_out,zs.avail_out));

				// If we just consumed the last input, mark the buffer free and switch buffers.
				if (zs.avail_in == 0) {
					inputs[nextInputToZlib].size = 0;
					nextInputToZlib ^= 1;
				}
				// If we filled the output buffer, send it.
				if (zs.avail_out == 0) {
					dprintf(("send output %d",nextOutputFromZlib));
					outputs[nextOutputFromZlib].BeginSend(state);
					nextOutputFromZlib ^= 1;
				}
			} while ((state.avail_in || zs.avail_in || inputs[nextInputToZlib].pending) && (state.avail_out[0] || zs.avail_out));

			// Wait for all output to flush if we're done with the inflate
			if (state.avail_out[0] == 0) {
				dprintf(("final output flush"));
				outputs[1-nextOutputFromZlib].EndSend();
				outputs[nextOutputFromZlib].EndSend();
			}

#if RAGETRACE
			if (traceData)
			{
				trace.Pop();
				trace.Finish(8, false);
			}
#endif

			// Send state object back to PPU.
			cellDmaPut(&state, state_ea, sizeof(state), 8, 0, 0);
			cellDmaWaitTagStatusAll(1<<8);
		}
		else if (mask == CMD_COPY) {
			// read state from ppu
			cellDmaGet(&state, state_ea, sizeof(state), 8, 0, 0);
			cellDmaWaitTagStatusAll(1<<8);
			uint64_t eaSrc = (uint64_t) state.next_in;
			uint64_t eaDst = (uint64_t) state.next_out[0];
			size_t toCopy = state.avail_in;

			dprintf(("spu copy from %llx to %llx, %lu bytes", eaSrc,eaDst,toCopy));
#if RAGETRACE
			uint32_t traceData = state.traceSpu;
			rage::pfSpuTrace trace(traceData);
			if (traceData)
			{
				rage::g_pfSpuTrace = &trace;
				trace.Push((rage::pfTraceCounterId*)(state.traceId));
			}
#endif

			while (toCopy) {
				size_t thisCopy = sizeof(outputBuffers);
				if (thisCopy > toCopy)
					thisCopy = toCopy;
				cellDmaLargeGet(outputBuffers, eaSrc, thisCopy, 8, 0, 0);
				eaSrc += thisCopy;
				cellDmaWaitTagStatusAll(1<<8);
				cellDmaLargePut(outputBuffers, eaDst, thisCopy, 8, 0, 0);
				cellDmaWaitTagStatusAll(1<<8);
				eaDst += thisCopy;
				toCopy -= thisCopy;
			}

#if RAGETRACE
			if (traceData)
			{
				trace.Pop();
				trace.Finish(8);
			}
#endif

		}
		// Send whatever we received back to the caller to let them know we're done.
		cellSpursEventFlagSet(event_spu2ppu_ea, mask);

		// ..and if it was an exit request, bail
		if (mask == CMD_EXIT)
			break;
	}

	// Displayf("calling inflateEnd");
	inflateEnd(&zs);

	return 0;
}

#endif		// __SPU
