#ifndef ZLIB_INFLATESTATE_H
#define ZLIB_INFLATESTATE_H

#include <stddef.h>

namespace rage {

struct zlibInflateState {
	enum {
#if __PS3
		MAX_OUTPUT_CHUNKS = 9,
#else // __PS3
		MAX_OUTPUT_CHUNKS = 1,
#endif // __PS3
	};
	size_t avail_in;						// Number of compressed bytes that are available to read
	void *next_in;							// Pointer to the compressed data
	size_t total_in;
	size_t avail_out[MAX_OUTPUT_CHUNKS];	// Number of bytes available for decompression (for each of the next_out entries), 0 indicates that there are no more chunks available.
	void *next_out[MAX_OUTPUT_CHUNKS];		// Pointers to the data the decompressed data should be written to. Note that only CMD_INFLATE uses multiple buffers, CMD_COPY
											// only checks [0].
	size_t total_out;
#if __PS3
	uint32_t traceSpu;
	uint32_t traceId;
#else
	size_t final_in;
	size_t final_out;
#endif // RAGETRACE	

#if !__FINAL
	const char *debugName;					// This pointer MUST be aligned by 16 bytes!!
#endif // !__FINAL

// This structure must be a multiple of 16 bytes in size 
// due to the DMA functions we use on PS3.
} PS3_ONLY();

}	// namespace rage

#endif	// ZLIB_INFLATESTATE_H
