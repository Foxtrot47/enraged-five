//
// zlib/inflateClient.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
// Note, this header file is only ever referenced by pgStreamer's internal implementation,
// so we don't worry too much about the usual scoping / forward referencing rules.
// (In particular, this header is never referenced by another header)
#ifndef ZLIB_INFLATECLIENT_H
#define ZLIB_INFLATECLIENT_H

#include "inflateState.h"
#include "string/string.h"

#if __PS3
#include <cell/spurs/task.h>
#include "inflateServer.h"
#elif __XENON
#include "system/xtl.h"
#include <xcompress.h>
#else
#include "zlib.h"
#endif

namespace rage {

class zlibInflater {
public:
	zlibInflater();

	// PURPOSE:	Abstraction of nonlocal inflateInit()
	void Init();

#if __PS3
	// PURPOSE:	Wait for the ps3 task to run at init time, so we don't have to block in Init()
	void WaitForInit();
	bool IsInitialized() const { return m_initialized; }
#endif // __PS3

	// PURPOSE: Abstraction of nonlocal inflateEnd()
	void Shutdown();

	// PURPOSE:	Abstraction of nonlocal inflateReset()
	void Reset(zlibInflateState &state,size_t compressedSize,size_t uncompressedSize);

	// PURPOSE: Abstraction of nonlocal inflate()
	void InflateBegin(zlibInflateState &state);
	
	// PURPOSE: Wait for last InflateBegin call to finish
	void InflateEnd(zlibInflateState &state);

	// PURPOSE: Copy memory via SPU (intended for VRAM)
	void Copy(const void *dst,const void *src,size_t amt);

#if !__FINAL
	// PURPOSE: Set a debug string to identify the current stream, shown in error
	// messages. The string may be temporary, it will be copied into an internal
	// buffer.
	void SetCurrentStreamName(const char *streamName)		{ safecpy(m_CurrentStreamName, streamName, sizeof(m_CurrentStreamName)); }

	// RETURNS: The last string set by SetCurrentStreamName().
	const char *GetCurrentStreamName() const				{ return m_CurrentStreamName; }
#endif // __FINAL

private:
#if !__FINAL
	char m_CurrentStreamName[128];		// User-provided name of the stream being decompressed. This must be an address aligned by 16 on PS3!!
										// Note that zLibInflater contains SharedBuffer, which already enforces an alignment of 128 bytes.
#endif // !__FINAL

#if __PS3
	SharedBuffer m_Buffer;
	CellSpursTaskset m_SpursTaskset;
	CellSpursTaskId m_SpursTaskId;
	char *m_Context;
	bool m_initialized;
#elif __XENON
	XMEMDECOMPRESSION_CONTEXT m_DecompressionContext;
#else
	z_stream m_zStream;
#endif
};

}

#endif	// ZLIB_INFLATECLIENT_H
