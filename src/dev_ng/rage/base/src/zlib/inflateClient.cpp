//
// zlib/inflateClient.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "inflateClient.h"

#include <string.h>

#if __PS3
#include <stdlib.h>
#include "system/new.h"
#include "profile/cellspurstrace.h"
#include "system/task.h"
extern char SPURS_TASK_START(inflateServer)[];
#elif __XENON
#include "system/xcompress_settings.h"
#endif

#include "data/resourceheader.h"
#include "diag/errorcodes.h"

namespace rage {

zlibInflater::zlibInflater()
{
#if __PS3
	m_initialized = false;
#endif // __PS3
#if !__FINAL
		m_CurrentStreamName[0] = 0;
#endif // !__FINAL
}



#if __PS3

#if RAGETRACE
extern uint32_t (*g_pIdFromName)(const char*);
#endif // RAGETRACE

extern CellSpurs *g_Spurs;

void zlibInflater::Init()
{
	// Try to run on SPU0, and use SPU3 as a backup.
	// We have to avoid SPU1 and SPU2, they're used by BINK and will cause
	// lock-ups if these two collide.
	uint8_t prio[8] = { 1,0,0,15,0,0,0,0 };
	int err;

	// Displayf("zlibInflater - arg buffer at %p",&s_Buffer);

	// Use primary spurs instance now that it's really fast

	rage::sysTaskManager::CreateTaskset(&m_SpursTaskset,  (uint64_t)&m_Buffer, prio, 1, "zlibInflater");

	err = cellSpursEventFlagInitialize(&m_SpursTaskset, &m_Buffer.ppu2spu, CELL_SPURS_EVENT_FLAG_CLEAR_AUTO, CELL_SPURS_EVENT_FLAG_PPU2SPU);
	if (err < 0)
		Quitf("zlibInflater - EventFlagInit(ppu2spu) failed, code %x",err);

	err = cellSpursEventFlagInitialize(&m_SpursTaskset, &m_Buffer.spu2ppu, CELL_SPURS_EVENT_FLAG_CLEAR_AUTO, CELL_SPURS_EVENT_FLAG_SPU2PPU);
	if (err < 0)
		Quitf("zlibInflater - EventFlagInit(spu2ppu) failed, code %x",err);

	err = cellSpursEventFlagAttachLv2EventQueue(&m_Buffer.spu2ppu);
	if (err < 0)
		Quitf("zlibInflater - AttachLv2EventQ(spu2ppu) failed, code %x",err);


	CellSpursTaskLsPattern lsPattern;
	CellSpursTaskLsPattern roPattern; 
	cellSpursTaskGetReadOnlyAreaPattern(&roPattern,	SPURS_TASK_START(inflateServer));
	lsPattern.u64[0] = gCellSpursTaskLsAll.u64[0] & ~roPattern.u64[0];
	lsPattern.u64[1] = gCellSpursTaskLsAll.u64[1] & ~roPattern.u64[1];

	cellSpursTaskGenerateLsPattern(&roPattern, 128*1024, 120*1024);
	lsPattern.u64[0] &= ~roPattern.u64[0];
	lsPattern.u64[1] &= ~roPattern.u64[1];

	uint32_t contextSize = 0;
	err = cellSpursTaskGetContextSaveAreaSize(&contextSize, &lsPattern);
	if (err < 0)
		Quitf("zlibInflater - GetContextSaveAreaSize failed");
	Displayf("Context size is %dk",contextSize>>10);

	m_Context = rage_aligned_new(128) char[contextSize];
	err = cellSpursCreateTask(&m_SpursTaskset, &m_SpursTaskId, SPURS_TASK_START(inflateServer), m_Context, contextSize, &lsPattern, NULL);
	if (err < 0)
		Quitf("zlibInflater - CreateTask failed, code %x",err);

	m_initialized = false;
}

void zlibInflater::WaitForInit()
{
	if (m_initialized)
		return;

	// Wait for the task to set its first event, so we know its running
	uint16_t mask = 0xFFFF;
	while (cellSpursEventFlagWait(&m_Buffer.spu2ppu, &mask, CELL_SPURS_EVENT_FLAG_OR))
		;

	m_initialized = true;
}


void zlibInflater::Reset(zlibInflateState &state,size_t XENON_ONLY(compressedSize),size_t XENON_ONLY(uncompressedSize))
{
	// Send exit command.
	cellSpursEventFlagSet(&m_Buffer.ppu2spu, CMD_RESET);

	// Wait for completion.
	uint16_t mask = 0xFFFF;
	while (cellSpursEventFlagWait(&m_Buffer.spu2ppu, &mask, CELL_SPURS_EVENT_FLAG_OR))
		;

	Assert(mask == CMD_RESET);

	state.avail_in = state.avail_out[0] = state.total_in = state.total_out = 0;
	state.next_in = state.next_out[0] = NULL;
#if __XENON
	state.final_in = compressedSize;
	state.final_out = uncompressedSize;
#endif // __XENON	
}

void zlibInflater::InflateBegin(zlibInflateState &state)
{
#if !__FINAL
	// This will be DMA'd over, so it needs to be aligned properly.
	CompileTimeAssert((OffsetOf(zlibInflater, m_CurrentStreamName) & 0xf) == 0);
#endif // !__FINAL

	m_Buffer.state = state;
#if RAGETRACE
	// Store information for trace
	m_Buffer.state.traceSpu = (uint32_t)g_pfSpuTrace;
	m_Buffer.state.traceId = g_pIdFromName("Inflate");
#endif
	// Displayf("client - decomp %u from %p to up to %u at %p",state.avail_in,state.next_in,state.avail_out,state.next_out);
	cellSpursEventFlagSet(&m_Buffer.ppu2spu, CMD_INFLATE);
}


void zlibInflater::InflateEnd(zlibInflateState &state)
{
	uint16_t mask = 0xFFFF;
	while (cellSpursEventFlagWait(&m_Buffer.spu2ppu, &mask, CELL_SPURS_EVENT_FLAG_OR))
		;
	Assert(mask == CMD_INFLATE);
	state = m_Buffer.state;
	// Displayf("client - done, now %u at %p",state.avail_out,state.next_out);
}


void zlibInflater::Copy(const void *dst,const void *src,size_t amt)
{
#if RAGETRACE
	// Store information for trace
	m_Buffer.state.traceSpu = (uint32_t)g_pfSpuTrace;
	m_Buffer.state.traceId = g_pIdFromName("Copy");
#endif
	m_Buffer.state.next_in = (void*) src;
	m_Buffer.state.next_out[0] = (void*) dst;
	m_Buffer.state.avail_in = amt;

	cellSpursEventFlagSet(&m_Buffer.ppu2spu, CMD_COPY);
	uint16_t mask = 0xFFFF;
	while (cellSpursEventFlagWait(&m_Buffer.spu2ppu, &mask, CELL_SPURS_EVENT_FLAG_OR))
		;
	Assert(mask == CMD_COPY);
}


void zlibInflater::Shutdown()
{
	// Send exit command.
	cellSpursEventFlagSet(&m_Buffer.ppu2spu, CMD_EXIT);

	// Wait for completion.
	uint16_t mask = 0xFFFF;
	while (cellSpursEventFlagWait(&m_Buffer.spu2ppu, &mask, CELL_SPURS_EVENT_FLAG_OR))
		;
	Assert(mask == CMD_EXIT);

	// Don't need to destroy the event flags, but do need to disconnect from the Lv2 event queue
	cellSpursEventFlagDetachLv2EventQueue(&m_Buffer.spu2ppu);

	cellSpursShutdownTaskset(&m_SpursTaskset);

	cellSpursJoinTaskset(&m_SpursTaskset);

	delete[] m_Context;
	m_Context = NULL;
}

#else		// !__PS3 - Use XCompress on 360 and zlib on PC.

void zlibInflater::Init()
{
#if __XENON
	XMEMCODEC_PARAMETERS_LZX codecParams = XCOMPRESS_SETTINGS;
	if(FAILED(XMemCreateDecompressionContext(XMEMCODEC_LZX, (VOID*)&codecParams, XMEMCOMPRESS_STREAM, &m_DecompressionContext)))
		Quitf("zlibInflater::Init - XMemCreateDecompressionContext failed");
#else
	memset(&m_zStream,0,sizeof(m_zStream));
	if (inflateInit2(&m_zStream,-MAX_WBITS) < 0) {
		Quitf(ERR_GEN_ZLIB_1,"zlibInflater::Init - inflateInit failed");
	}
#endif
}


void zlibInflater::Reset(zlibInflateState &state,size_t compressedSize,size_t uncompressedSize)
{
#if __XENON
	XMemResetDecompressionContext(m_DecompressionContext);
#else
	inflateReset(&m_zStream);
#endif
	state.avail_in = state.avail_out[0] = state.total_in = state.total_out = 0;
	state.next_in = state.next_out[0] = NULL;
	state.final_in = compressedSize;
	state.final_out = uncompressedSize;
}

void zlibInflater::InflateBegin(zlibInflateState &state)
{
#if __XENON
# if XCOMPRESS_FILE_IDENTIFIER_LZXNATIVE + XCOMPRESS_LZXNATIVE_VERSION_MINOR != 0xff512f1
# error "You need at least XeDK 7776 installed on this machine."
# endif
#endif

#if __XENON
		CompileTimeAssert(zlibInflateState::MAX_OUTPUT_CHUNKS == 1);
		HRESULT hr;

		SIZE_T uOutputConsumed = state.avail_out[0];
		SIZE_T uInputConsumed = state.avail_in;

		if (uInputConsumed > state.final_in - state.total_in) {
			/// Displayf("Fixing input length due to bad file size!"); // This happens all the time now in new resource header code.
			uInputConsumed = state.final_in - state.total_in;
		}

		if(FAILED(hr=XMemDecompressStream(m_DecompressionContext, state.next_out[0], &uOutputConsumed,
			state.next_in, &uInputConsumed)) && hr != XMCDERR_MOREDATA)
			Quitf("zlibInflater::InflateBegin - error %08x in file %s",hr, GetCurrentStreamName());

		Assertf(uOutputConsumed || hr == S_OK,"Likely data error or bad network connection while decompressing file %s", GetCurrentStreamName());

		state.next_out[0] = ((char*)state.next_out[0]) + uOutputConsumed;
		state.next_in = ((char*)state.next_in) + uInputConsumed;

		state.avail_out[0] -= uOutputConsumed;
		state.avail_in -= uInputConsumed;

		state.total_out += uOutputConsumed;
		state.total_in += uInputConsumed;
#else
		m_zStream.avail_in = (uInt) state.avail_in;
		m_zStream.avail_out = (uInt) state.avail_out[0];
		m_zStream.next_in = (Bytef*)state.next_in;
		m_zStream.next_out = (Bytef*)state.next_out[0];
		m_zStream.total_in = (uLong) state.total_in;
		m_zStream.total_out = (uLong) state.total_out;

		int err = inflate(&m_zStream,Z_SYNC_FLUSH);
		if (err < 0)
#if !__FINAL
			Quitf(ERR_GEN_ZLIB_2,"zlibInflater::InflateBegin - error %d, msg %s in file %s",err, (err == Z_DATA_ERROR && m_zStream.msg) ? m_zStream.msg : "unknown", GetCurrentStreamName());
#else
			Quitf(ERR_GEN_ZLIB_2,"zlibInflater::InflateBegin - error %d, msg %s in file %s",err, (err == Z_DATA_ERROR && m_zStream.msg) ? m_zStream.msg : "unknown", "");
#endif
		state.avail_in = m_zStream.avail_in;
		state.avail_out[0] = m_zStream.avail_out;
		state.next_in = m_zStream.next_in;
		state.next_out[0] = m_zStream.next_out;
		state.total_in = m_zStream.total_in;
		state.total_out = m_zStream.total_out;
#endif
}


void zlibInflater::InflateEnd(zlibInflateState &)
{
}


void zlibInflater::Shutdown()
{
#if __XENON
	XMemDestroyDecompressionContext(m_DecompressionContext);
#else
	inflateEnd(&m_zStream);
#endif
}

#endif		// !__PS3

}
