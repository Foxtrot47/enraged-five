//
// parsercore/element.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PARSERCORE_ELEMENT_H
#define PARSERCORE_ELEMENT_H

#include "attribute.h"
#include "utils.h"

#include "atl/array.h"

namespace rage {

// PURPOSE: An element is analogous to an XML element. It has a name and a list of attributes where each
// attribute is a name=value pair. The attribute names must be unique and order doesn't matter.
class parElement
{
public:
	parElement() : m_Name(NULL) {}

	explicit parElement(const char* name, bool copyName)
		: m_Name(NULL)
	{
		SetName(name, copyName);
	}

	~parElement();

	// RETURNS: The name of the element
	const char* GetName() const;

    // RETURNS: The name of the element with no namespace.
	const char* GetNameNoNs() const;

	// PURPOSE: Sets the name of the element
	// PARAMS:
	//		name - The new name for the element
	//		copy - If true, the element will make a copy of the string
	void SetName(const char* name, bool copy = true);

	// PURPOSE: Sets this element to be a copy of other
	void CopyFrom(const parElement& other);

	// PURPOSE: Swaps the contents of this element with the contents of 'other'
	void SwapWith(parElement& other);

#if __DEV
	// PURPOSE: Prints an element (for debugging purposes)
	void Print();
#endif

	// PURPOSE: Deletes the contents of an element
	// NOTES: Only deletes the name string if a local copy was made.
	void Reset();

	// PURPOSE: Add a new string attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - string value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	//		copyvalue - If true, the attribute will copy the value string
	parAttribute* AddAttribute(const char* name, const char* value, bool copyname = true, bool copyvalue = true);

	// PURPOSE: Add a new int attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - int value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	parAttribute* AddAttribute(const char* name, int value, bool copyname = true);
	parAttribute* AddAttribute(const char* name, s64 value, bool copyname = true);

	// PURPOSE: Add a new float attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - float value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	parAttribute* AddAttribute(const char* name, float value, bool copyname = true);
	parAttribute* AddAttribute(const char* name, double value, bool copyname = true);

	// PURPOSE: Add a new bool attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - bool value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	parAttribute* AddAttribute(const char* name, bool value, bool copyname = true);

	// PURPOSE: Returns the array of attributes
	atArray<parAttribute>& GetAttributes();

	// PURPOSE: Sorts the attributes by name, to speed up FindAttribute
	void SortAttributes();

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	parAttribute* FindAttribute(const char* name);

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	const parAttribute* FindAttribute(const char* name) const;

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	parAttribute* FindAttributeAnyCase(const char* name);

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	const parAttribute* FindAttributeAnyCase(const char* name) const;

	// PURPOSE: Searches for an attribute in the attribute list. Asserts that it exists
	// RETURNS: Reference to the attribute
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	parAttribute& FindRequiredAttribute(const char* name);

	// PURPOSE: Searches for an attribute in the attribute list. Asserts that it exists
	// RETURNS: Reference to the attribute
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	const parAttribute& FindRequiredAttribute(const char* name) const;

	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The string value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	//		buf - A temporary buffer used in case we need to convert from a non-string type to string. It should be at least 50 bytes
	//		bufSize - Size of the temporary buffer
	//		alwaysCopy - Normally we only write to the 'buf' buffer when converting from a non-string type. If alwaysCopy is true we always
	//				write the result to the buffer (even if its the default value).
	const char* FindAttributeStringValue(const char* attrName, const char* def, char* buf, int bufSize, bool alwaysCopy = false, bool warnIfMissing = false) const;

	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The float value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	float FindAttributeFloatValue(const char* attrName, float def, bool warnIfMissing = false) const;
	double FindAttributeDoubleValue(const char* attrName, double def, bool warnIfMissing = false) const;

	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The int value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	int FindAttributeIntValue(const char* attrName, int def, bool warnIfMissing = false) const;
	s64 FindAttributeInt64Value(const char* attrName, s64 def, bool warnIfMissing = false) const;

	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The bool value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	bool FindAttributeBoolValue(const char* attrName, bool def, bool warnIfMissing = false) const;

	// PURPOSE: Returns the "user" flag, which is a flag users can set for any reason.
	// NOTES:
	//		The user flag will not be saved to an output file
	bool GetUserFlag() const
	{
		return GetFlags().IsSet(FLAG_USER);
	}

	// PURPOSE: Sets the "user" flag
	// NOTES:
	//		The user flag will not be saved to an output file
	void SetUserFlag(bool newVal)
	{
		return GetFlags().Set(FLAG_USER, newVal);
	}

	atFixedBitSet16& GetFlags() {return m_Attributes.GetFlags();}
	const atFixedBitSet16& GetFlags() const {return m_Attributes.GetFlags();}

	u8& GetUserData1() {return m_Attributes.GetUserData1();}
	const u8& GetUserData1() const {return m_Attributes.GetUserData1();}

	u8& GetUserData2() {return m_Attributes.GetUserData2();}
	const u8& GetUserData2() const {return m_Attributes.GetUserData2();}

	parAttributeList& GetAttributeListRef() { return m_Attributes; }

	enum Flags
	{
		FLAG_OWNS_NAME_STR		= parAttributeList::FLAG_OWNER_FLAGS,
		FLAG_USER,
		FLAG_ELT_OWNER_FLAGS,
	};

protected:
	const char* m_Name;
	parAttributeList m_Attributes;
};

class parHtmlElement : public parElement
{
public:
	enum parHtmlElementType
	{
		parHET_Paragraph,
		parHET_LineBreak,
		parHET_Hyperlink,
		parHET_Font,
		parHET_Bold,
		parHET_Italic,
		parHET_Underline,
		parHET_Bullets,
		parHET_TextFormat,
		parHET_Tab,
		parHET_Text,
		
		parHET_Unknown,
	};

	void SetName(const char* name, bool copy = true);
	void SetType(parHtmlElementType type)	{ m_Type = type; }

	parHtmlElementType GetType() const		{ return m_Type; }

protected:
	parHtmlElementType			m_Type;
};



inline const char* parElement::GetName() const
{
	return m_Name;
}

inline const char* parElement::GetNameNoNs() const
{
    const char* nameNoNs = m_Name ? strchr(m_Name, ':') : NULL;
    return nameNoNs ? nameNoNs + 1 : m_Name;
}

inline atArray<parAttribute>& parElement::GetAttributes()
{
	return m_Attributes.GetAttributeArray();
}

inline const parAttribute* parElement::FindAttribute(const char* name) const
{
	return const_cast<parElement*>(this)->FindAttribute(name);
}

inline const parAttribute* parElement::FindAttributeAnyCase(const char* name) const
{
	return const_cast<parElement*>(this)->FindAttributeAnyCase(name);
}

inline parAttribute& parElement::FindRequiredAttribute(const char* name) {
	parAttribute* attr = FindAttribute(name);
	parAssertf(attr != NULL, "Couldn't find required attribute \"%s\" in element %s", name, m_Name);
	return *attr;
}

inline const parAttribute& parElement::FindRequiredAttribute(const char* name) const
{
	return FindRequiredAttribute(name);
}

inline parAttribute* parElement::AddAttribute(const char* name, float value, bool copyname /* = true */)
{
	return AddAttribute(name, (double)value, copyname);
}

inline parAttribute* parElement::AddAttribute(const char* name, int value, bool copyname /* = true */)
{
	return AddAttribute(name, (s64)value, copyname);
}

inline float parElement::FindAttributeFloatValue(const char* attrName, float def, bool warnIfMissing /* = false */) const
{
	return (float)FindAttributeDoubleValue(attrName, (float)def, warnIfMissing);
}

inline int parElement::FindAttributeIntValue(const char* attrName, int def, bool warnIfMissing /* = false */) const
{
	return (int)FindAttributeInt64Value(attrName, (s64)def, warnIfMissing);
}

} // namespace rage

#endif // PARSERCORE_ELEMENT_H
