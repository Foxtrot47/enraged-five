// 
// parser/settings.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "settings.h"

#include "parser/optimisations.h"

PARSER_OPTIMISATIONS();

using namespace rage;

parSettings parSettings::sm_StandardSettings;
parSettings parSettings::sm_StrictSettings;
parSettings parSettings::sm_EmbeddedSettings;

void parSettings::CreateStandardSettings()
{
	sm_StandardSettings.m_Flags.Set(WRITE_XML_HEADER);
	sm_StandardSettings.m_Flags.Set(PROCESS_SPECIAL_ATTRS);
	sm_StandardSettings.m_Flags.Set(ASSERT_ON_OUT_OF_ORDER_REGISTRATION);
	sm_StandardSettings.m_Flags.Set(WARN_ON_MULTIPLE_REGISTRATION);
	sm_StandardSettings.m_Flags.Set(INITIALIZE_NEW_DATA);
	sm_StandardSettings.m_Flags.Set(WARN_ON_MISMATCHED_PAR_MACRO);
	sm_StandardSettings.m_Flags.Set(PRELOAD_FILE);
	sm_StandardSettings.m_Flags.Set(WARN_ON_FIXED_SIZE_ARRAY_MISMATCH);

	sm_StrictSettings = sm_StandardSettings;
	sm_StrictSettings.m_Flags.Set(WARN_ON_UNUSED_DATA);
	sm_StrictSettings.m_Flags.Set(WARN_ON_MISSING_DATA);
	sm_StrictSettings.m_Flags.Set(WARN_ON_MINOR_VERSION_MISMATCH);

	sm_EmbeddedSettings = sm_StandardSettings;
	sm_EmbeddedSettings.m_Flags.Clear(WRITE_XML_HEADER);
	sm_EmbeddedSettings.m_Flags.Clear(PRELOAD_FILE);
	sm_EmbeddedSettings.m_Flags.Clear(WRITE_UTF8_BOM);
}
