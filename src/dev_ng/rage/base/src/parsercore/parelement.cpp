// 
// parsercore/parelement.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "element.h" //RAGE_QA: INCLUDE_OK

#include "attribute.h"
#include "utils.h"

#include "file/stream.h"
#include "math/amath.h" // for "SwapEm"
#include "parser/macros.h" // normally parsercore shouldn't #include from parser, this is just for output macros
#include "parser/optimisations.h"
#include "string/string.h"

#include <algorithm>

PARSER_OPTIMISATIONS();

using namespace rage;

parElement::~parElement()
{
	if (GetFlags().IsSet(FLAG_OWNS_NAME_STR)) {
		delete [] m_Name;
	}
}

void parElement::CopyFrom(const parElement& other)
{
	if (this == &other) {
		return;
	}
	Reset();
	if (other.GetFlags().IsSet(FLAG_OWNS_NAME_STR))
	{
		m_Name = StringDuplicate(other.m_Name);
	}
	else
	{
		m_Name = other.m_Name;
	}

	m_Attributes.CopyFrom(other.m_Attributes);
}

void parElement::SwapWith(parElement& other)
{
	if (this == &other) {
		return;
	}

	SwapEm(m_Name, other.m_Name);
	SwapEm(GetUserData1(), other.GetUserData1());
	SwapEm(GetUserData2(), other.GetUserData2());
	SwapEm(GetFlags(), other.GetFlags());
	m_Attributes.GetAttributeArray().Swap(other.m_Attributes.GetAttributeArray());
}

void parElement::SetName(const char* name, bool copy)
{
	if (GetFlags().IsSet(FLAG_OWNS_NAME_STR)) {
		delete [] m_Name;
	}
	if (copy)
	{
		m_Name = StringDuplicate(name);
		GetFlags().Set(FLAG_OWNS_NAME_STR);
	}
	else
	{
		m_Name = name;
		GetFlags().Clear(FLAG_OWNS_NAME_STR);
	}
}

parAttribute* parElement::AddAttribute(const char* name, const char* value, bool copyname, bool copyvalue)
{
	return m_Attributes.AddAttribute(name, value, copyname, copyvalue);
}

parAttribute* parElement::AddAttribute(const char* name, bool value, bool copyname)
{
	return m_Attributes.AddAttribute(name, value, copyname);
}

parAttribute* parElement::AddAttribute(const char* name, s64 value, bool copyname)
{
	return m_Attributes.AddAttribute(name, value, copyname);
}

parAttribute* parElement::AddAttribute(const char* name, double value, bool copyname)
{
	return m_Attributes.AddAttribute(name, value, copyname);
}

void parElement::SortAttributes()
{
	m_Attributes.SortAttributes();
}

parAttribute* parElement::FindAttribute(const char* name)
{
	return m_Attributes.FindAttribute(name);
}

parAttribute* parElement::FindAttributeAnyCase(const char* name)
{
	return m_Attributes.FindAttributeAnyCase(name);
}

void parElement::Reset()
{
	if (GetFlags().IsSet(FLAG_OWNS_NAME_STR)) {
		delete [] m_Name;
	}

	m_Name = NULL;
	m_Attributes.Reset();
}

/*
void parElement::RequireVersion(const parVersionInfo& DEV_ONLY(version)) const {
#if __DEV
	parVersionInfo::Status s = parVersionInfo::CompareVersions(m_Version, version);
	parAssertf(s != parVersionInfo::INCOMPATABLE && "File and code version numbers are incompatable");
	if(s == parVersionInfo::FILE_UNVERSIONED)
	{
		parWarningf("Found no version number, expected element with version number %d.%d", version.GetMajor(), version.GetMinor());
	}
#endif
}
*/

#if __DEV
void parElement::Print()
{
	char buf[64];
	printf("<%s", m_Name);
	for(int i = 0; i < m_Attributes.GetAttributeArray().GetCount(); i++) 
	{
		const char* stringVal = m_Attributes.GetAttributeArray()[i].GetStringRepr(buf, 64);
		printf(" %s=\"%s\"", m_Attributes.GetAttributeArray()[i].GetName(), stringVal);
	}
	printf(">\n");
}
#endif

bool parElement::FindAttributeBoolValue(const char* attrName, bool def, bool warnIfMissing /* = false */) const
{
	return m_Attributes.FindAttributeBoolValue(attrName, def, warnIfMissing);
}

s64 parElement::FindAttributeInt64Value(const char* attrName, s64 def, bool warnIfMissing /* = false */) const
{
	return m_Attributes.FindAttributeInt64Value(attrName, def, warnIfMissing);
}

double parElement::FindAttributeDoubleValue(const char* attrName, double def, bool warnIfMissing /* = false */) const
{
	return m_Attributes.FindAttributeDoubleValue(attrName, def, warnIfMissing);
}

const char* parElement::FindAttributeStringValue(const char* attrName, const char* def, char* buf, int bufSize, bool alwaysCopy /* = false */, bool warnIfMissing /* = false */) const
{
	return m_Attributes.FindAttributeStringValue(attrName, def, buf, bufSize, alwaysCopy, warnIfMissing);
}

void parHtmlElement::SetName(const char* name, bool copy)
{
	parElement::SetName(name, copy);

	if( !stricmp(name, "p") )
		m_Type = parHET_Paragraph;
	else if( !stricmp(name, "br") )
		m_Type = parHET_LineBreak;
	else if( !stricmp(name, "a") )
		m_Type = parHET_Hyperlink;
	else if( !stricmp(name, "font") )
		m_Type = parHET_Font;
	else if( !stricmp(name, "b") )
		m_Type = parHET_Bold;
	else if( !stricmp(name, "i") )
		m_Type = parHET_Italic;
	else if( !stricmp(name, "u") )
		m_Type = parHET_Underline;
	else if( !stricmp(name, "li") )
		m_Type = parHET_Bullets;
	else if( !stricmp(name, "textformat") )
		m_Type = parHET_TextFormat;
	else if( !stricmp(name, "tab") )
		m_Type = parHET_Tab;
	else
		m_Type = parHET_Unknown;
}

