// 
// parsercore/attribute.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "attribute.h"
#include "streamxml.h"

#include "file/stream.h"
#include "parser/optimisations.h"
#include "parser/treenode.h"
#include "string/string.h"

#include <algorithm>
#include <float.h>

#include "attribute_parser.h"

#include "parser/manager.h"

PARSER_OPTIMISATIONS();

using namespace rage;

void parAttribute::Clear()
{
	if (m_Flags.IsSet(FLAG_OWNS_NAME_STR)) {
		delete [] m_Name;
        m_Name = NULL;
	}
	ClearOldValueString();
}

void parAttribute::SetName(const char* str, bool copy)
{
	parAssertf(parStreamInXml::IsXmlNameString(str), "Name \"%s\" can't be used as an attribute name - XML attributes may only contain characters 'A'-'Z', 'a'-'z', '0'-'9', '_', '-', ':', '.'", str);
	if (m_Flags.IsSet(FLAG_OWNS_NAME_STR)) {
		delete [] m_Name;
        m_Name = NULL;
	}

	if (copy && str)
	{
		// not a special attribute, do the slow thing.
		m_Name = StringDuplicate(str);
		m_Flags.Set(FLAG_OWNS_NAME_STR);
	}
	else
	{
		m_Name = str;
		m_Flags.Clear(FLAG_OWNS_NAME_STR);
	}
}

void parAttribute::SetValue(const char* str, bool copy)
{
	ClearOldValueString();

	m_Type = STRING;
	if (copy && str)
	{
		m_Value.m_String = StringDuplicate(str);
		m_Flags.Set(FLAG_OWNS_VALUE_STR);
	}
	else
	{
		m_Value.m_String = str;
		m_Flags.Clear(FLAG_OWNS_VALUE_STR);
	}
}

void parAttribute::SetValue(s64 i)
{
	ClearOldValueString();
	m_Value.m_Int64 = i;
	m_Type = INT64;
}

void parAttribute::SetValue(double d)
{
	ClearOldValueString();
	m_Value.m_Double = d;
	m_Type = DOUBLE;
}

void parAttribute::SetValue(bool b)
{
	ClearOldValueString();
	m_Value.m_Bool = b;
	m_Type = BOOL;
}

bool parAttribute::FindBoolValue() const
{
	switch(m_Type)
	{
	case BOOL:
		return m_Value.m_Bool;
	case STRING:
		if (m_Value.m_String)
		{
			return parUtils::StringToBool(m_Value.m_String);
		}
		else
		{
			parErrorf("Can't convert empty string attribute %s=\"\" to bool", m_Name);
			break;
		}
	case INT64:
		parErrorf("Can't convert int attribute %s=%" I64FMT "d to bool", m_Name, m_Value.m_Int64);
		break;
	case DOUBLE:
		parErrorf("Can't convert float attribute %s=%f to bool", m_Name, m_Value.m_Double);
		break;
	default:
		{
			Quitf(ERR_PAR_INVALID_10,"Invalid type value %d", m_Type);
		}
		break;
	}
	return false;
}

s64 parAttribute::FindInt64Value() const
{
	switch(m_Type)
	{
	case INT64:
		return m_Value.m_Int64;
	case STRING:
		if (m_Value.m_String)
		{
			return parUtils::StringToInt64(m_Value.m_String);
		}
		else
		{
			parErrorf("Can't convert empty string attribute %s=\"\" to int", m_Name);
			break;
		}
	case DOUBLE:
		parErrorf("Can't convert float attribute %s=%f to int", m_Name, m_Value.m_Double);
		break;
	case BOOL:
		parErrorf("Can't convert boolean attribute %s=%s to int", m_Name, m_Value.m_Bool ? "true" : "false");
		break;
	default:
		{
			Quitf(ERR_PAR_INVALID_11,"Invalid type value %d", m_Type);
		}
		break;
	}
	return 0;
}

double parAttribute::FindDoubleValue() const
{
	switch(m_Type)
	{
	case DOUBLE:
		return m_Value.m_Double;
	case STRING:
		if (m_Value.m_String)
		{
			return parUtils::StringToDouble(m_Value.m_String);
		}
		else
		{
			parErrorf("Can't convert empty string attribute %s=\"\" to float", m_Name);
			break;
		}
	case INT64:
		if (m_Value.m_Int64 >= -((s64)1 << DBL_MANT_DIG) && m_Value.m_Int64 <= ((s64)1 << DBL_MANT_DIG)) // its bigger than this may or may not be exactly representable with a float
		{
			return (double)(m_Value.m_Int64);
		}
		else
		{
			parErrorf("Value %s=%" I64FMT "d is to large to be converted to a float without loss of precision", m_Name, m_Value.m_Int64);
			break;
		}
	case BOOL:
		parErrorf("Can't convert boolean attribute %s=%s to float", m_Name, m_Value.m_Bool ? "true" : "false");
		break;
	default:
		{
			Quitf(ERR_PAR_INVALID_12,"Invalid type value %d", m_Type);
		}
		break;
	}
	return 0.0f;
}

void parAttribute::ConvertToInt64()
{
	parAssertf(m_Type == STRING, "Invalid type - expected STRING and got %d", m_Type);
	s64 val = FindInt64Value();
	ClearOldValueString();
	m_Type = INT64;
	m_Value.m_Int64 = val;
}

void parAttribute::ConvertToDouble()
{
	parAssertf(m_Type == STRING, "Invalid type - expected STRING and got %d", m_Type);
	double val = FindDoubleValue();
	ClearOldValueString();
	m_Type = DOUBLE;
	m_Value.m_Double = val;
}

void parAttribute::ConvertToBool()
{
	parAssertf(m_Type == STRING, "Invalid type - expected STRING and got %d", m_Type);
	bool newVal = FindBoolValue();
	ClearOldValueString();
	m_Type = BOOL;
	m_Value.m_Bool = newVal;
}

void parAttribute::ConvertToString()
{
	char buffer[64];

	if (m_Type == STRING)
	{
		return;
	}

	m_Value.m_String = StringDuplicate(GetStringRepr(buffer, 64));
	m_Flags.Set(FLAG_OWNS_VALUE_STR);
	m_Type = STRING;
}

const char* parAttribute::GetStringRepr(char* buffer, int size, bool alwaysCopy /* = false */) const
{
	switch(m_Type)
	{
	case STRING:
		if (alwaysCopy && m_Value.m_String)
		{
			safecpy(buffer, m_Value.m_String, size);
			return buffer;
		}
		return m_Value.m_String;
	case INT64:
		{
			FastAssert(size > 24); // About the max size for an int (0xffffffffffffff = 18, 1^64 ~= 10^19)

			const char* format = "%" I64FMT "d";
			if (Unlikely(m_Flags.IsSet(FLAG_HEXADECIMAL)))
			{
				if ((u64)m_Value.m_Int64 > UINT_MAX)
				{
					format = "0x%016X";
				}
				else
				{
					format = "0x%08X";
				}
			}
			else if (Unlikely(m_Flags.IsSet(FLAG_UNSIGNED)))
			{
				format = "%" I64FMT "u";
			}
			formatf(buffer, size, format, m_Value.m_Int64);
		}
		break;
	case DOUBLE:

		if (m_Flags.IsSet(FLAG_HIGH_PRECISION))
		{
			FastAssert(size > 42); // Dunno here... but with 17 sig figs. lets add some extra room for padding
			formatf(buffer, size, "%#.17g", m_Value.m_Double);
			break;
		}
		else
		{
			FastAssert(size > 42); // FLT_MAX = 3.4e38, so 38 digits plus a '-' and a '.' and one to spare
			formatf(buffer, size, "%f", m_Value.m_Double);
			break;
		}
	case BOOL:
		FastAssert(size > 5);
		safecpy(buffer, m_Value.m_Bool ? "true" : "false", size);
	}
	return buffer;
}


parAttributeList::~parAttributeList()
{
	Reset();
}

void parAttributeList::Reset()
{
	for(int i =0; i < m_Attributes.GetCount(); i++) 
	{
		m_Attributes[i].Clear();
	}

    m_Attributes.Reset();
}

parAttribute* parAttributeList::AddAttribute(const char* name, const char* value, bool copyname, bool copyvalue)
{
	parAttribute& attr = m_Attributes.Grow(4);
	attr.SetName(name, copyname);
	attr.SetValue(value, copyvalue);
	m_Flags.Clear(FLAG_ATTR_LIST_SORTED);
	return &attr;
}

parAttribute* parAttributeList::AddAttribute(const char* name, bool value, bool copyname)
{
	parAttribute& attr = m_Attributes.Grow(4);
	attr.SetName(name, copyname);
	attr.SetValue(value);
	m_Flags.Clear(FLAG_ATTR_LIST_SORTED);
	return &attr;
}

parAttribute* parAttributeList::AddAttribute(const char* name, s64 value, bool copyname)
{
	parAttribute& attr = m_Attributes.Grow(4);
	attr.SetName(name, copyname);
	attr.SetValue(value);
	m_Flags.Clear(FLAG_ATTR_LIST_SORTED);
	return &attr;
}

parAttribute* parAttributeList::AddAttribute(const char* name, double value, bool copyname)
{
	parAttribute& attr = m_Attributes.Grow(4);
	attr.SetName(name, copyname);
	attr.SetValue(value);
	m_Flags.Clear(FLAG_ATTR_LIST_SORTED);
	return &attr;
}

struct AttrLess {
	bool operator()(parAttribute const & attr1, parAttribute const & attr2) const {
		return strcmp(attr1.GetName(), attr2.GetName()) < 0;
	}
};


void parAttributeList::SortAttributes()
{
	// could also throw in a check here like if number of attributes < 3, return.
	// could also do a quick check to see if its already sorted.
	// (if we sort-on-write it usually will be)
	if (m_Attributes.GetCount() > 0) {
		std::sort(m_Attributes.begin(), m_Attributes.end(), AttrLess());
		m_Flags.Set(FLAG_ATTR_LIST_SORTED);
	}
}

parAttribute* parAttributeList::FindAttribute(const char* name)
{
	if (!name || m_Attributes.GetCount() == 0)
	{
		return NULL;
	}
	if (IsSorted())
	{
		parAttribute* start = m_Attributes.begin();
		parAttribute* end = m_Attributes.end();
		parAttribute key;
		key.SetName(name, false);
		parAttribute* iter = std::lower_bound(start, end, key, AttrLess());
		if (iter != end && !strcmp((*iter).GetName(), name)) {
			return iter;
		}
		return NULL;
	}

	for(int i = 0; i < m_Attributes.GetCount(); i++) {
		if (!strcmp(name, m_Attributes[i].GetName())) {
			return &m_Attributes[i];
		}
	}
	return NULL;
}

parAttribute* parAttributeList::FindAttributeAnyCase(const char* name)
{
	if (!name || m_Attributes.GetCount() == 0)
	{
		return NULL;
	}

	for(int i = 0; i < m_Attributes.GetCount(); i++) {
		if (!stricmp(name, m_Attributes[i].GetName())) {
			return &m_Attributes[i];
		}
	}
	return NULL;
}

void parAttributeList::MergeFrom(const parAttributeList& other, bool overwrite /* = true */)
{
	bool wasSorted = IsSorted();
	for(int i = 0; i < other.m_Attributes.GetCount(); i++)
	{
		if (!overwrite && FindAttribute(other.m_Attributes[i].GetName()))
		{
			continue;
		}
		parAttribute* attr = NULL;
		switch(other.m_Attributes[i].GetType())
		{
		case parAttribute::BOOL:
			attr = AddAttribute(other.m_Attributes[i].GetName(), other.m_Attributes[i].GetBoolValue(), other.m_Attributes[i].m_Flags.IsSet(parAttribute::FLAG_OWNS_NAME_STR));
			break;
		case parAttribute::INT64:
			attr = AddAttribute(other.m_Attributes[i].GetName(), other.m_Attributes[i].GetInt64Value(), other.m_Attributes[i].m_Flags.IsSet(parAttribute::FLAG_OWNS_NAME_STR));
			break;
		case parAttribute::DOUBLE:
			attr = AddAttribute(other.m_Attributes[i].GetName(), other.m_Attributes[i].GetDoubleValue(), other.m_Attributes[i].m_Flags.IsSet(parAttribute::FLAG_OWNS_NAME_STR));
			break;
		case parAttribute::STRING:
			attr = AddAttribute(other.m_Attributes[i].GetName(), other.m_Attributes[i].GetStringValue(), 
				other.m_Attributes[i].m_Flags.IsSet(parAttribute::FLAG_OWNS_NAME_STR),
				other.m_Attributes[i].m_Flags.IsSet(parAttribute::FLAG_OWNS_VALUE_STR));
			break;
		}
		if (attr)
		{
			attr->m_Flags = other.m_Attributes[i].m_Flags;
		}
	}
	if (wasSorted)
	{
		SortAttributes();
	}
}

void parAttributeList::CopyFrom(const parAttributeList& other)
{
    if ( (other.m_Attributes.GetCount() > m_Attributes.GetCapacity())
        || (m_Attributes.GetCapacity() - other.m_Attributes.GetCount() > 4) )
    {
        // re-allocate because our array is not big enough, or we are more than 4 slots too long
        Reset();
        
        m_Attributes.Reserve( other.m_Attributes.GetCount() );
    }
    else if ( other.m_Attributes.GetCount() < m_Attributes.GetCount() )
    {
        // delete elements off the end
        while ( other.m_Attributes.GetCount() < m_Attributes.GetCount() )
        {
            m_Attributes[m_Attributes.GetCount() - 1].Clear();
            m_Attributes.Delete( m_Attributes.GetCount() - 1 );
        }
    }
    
    // resize the array so we can minimize our (re)allocations
    m_Attributes.Resize( other.m_Attributes.GetCount() );

	for(int i = 0; i < other.m_Attributes.GetCount(); i++) 
    {
		parAttribute& newAttr = m_Attributes[i];
		const parAttribute& oldAttr = other.m_Attributes[i];
		newAttr.SetName(oldAttr.GetName(), oldAttr.m_Flags.IsSet(parAttribute::FLAG_OWNS_NAME_STR));
		switch(oldAttr.m_Type)
		{
		case parAttribute::BOOL:
			newAttr.SetValue(oldAttr.m_Value.m_Bool);
			break;
		case parAttribute::INT64:
			newAttr.SetValue(oldAttr.m_Value.m_Int64);
			break;
		case parAttribute::DOUBLE:
			newAttr.SetValue(oldAttr.m_Value.m_Double);
			break;
		case parAttribute::STRING:
			newAttr.SetValue(oldAttr.m_Value.m_String, oldAttr.m_Flags.IsSet(parAttribute::FLAG_OWNS_VALUE_STR));
			break;
		}
		newAttr.m_Flags = oldAttr.m_Flags;
	}

	m_Flags = other.m_Flags;
	m_UserData1 = other.m_UserData1;
	m_UserData2 = other.m_UserData2;
}

bool parAttributeList::FindAttributeBoolValue(const char* attrName, bool def, bool OUTPUT_ONLY(warnIfMissing) /*= false*/) const
{
	const parAttribute* attr = FindAttribute(attrName);
	if (attr)
	{
		return attr->FindBoolValue();
	}
	else
	{
#if !__NO_OUTPUT
		if (warnIfMissing)
		{
			parWarningf("Couldn't find expected attribute \"%s\" - using default value %s", attrName, def ? "true" : "false");
		}
#endif
		return def;
	}

}

s64 parAttributeList::FindAttributeInt64Value(const char* attrName, s64 def, bool OUTPUT_ONLY(warnIfMissing) /*= false*/) const
{
	const parAttribute* attr = FindAttribute(attrName);
	if (attr)
	{
		return attr->FindInt64Value();
	}
	else
	{
#if !__NO_OUTPUT
		if (warnIfMissing)
		{
			parWarningf("Couldn't find expected attribute \"%s\" - using default value %" I64FMT "d", attrName, def);
		}
#endif
		return def;
	}
}

int parAttributeList::FindAttributeIntValue(const char* attrName, int def, bool warnIfMissing /* = false */) const
{
	return (int)FindAttributeInt64Value(attrName, def, warnIfMissing);
}

double parAttributeList::FindAttributeDoubleValue(const char* attrName, double def, bool OUTPUT_ONLY(warnIfMissing) /*= false*/) const
{
	const parAttribute* attr = FindAttribute(attrName);
	if (attr)
	{
		return attr->FindFloatValue();
	}
	else
	{
#if !__NO_OUTPUT
		if (warnIfMissing)
		{
			parWarningf("Couldn't find expected attribute \"%s\" - using default value %f", attrName, def);
		}
#endif
		return def;
	}
}

float parAttributeList::FindAttributeFloatValue(const char* attrName, float def, bool warnIfMissing /* = false */) const
{
	return (float)FindAttributeDoubleValue(attrName, def, warnIfMissing);
}

const char* parAttributeList::FindAttributeStringValue(const char* attrName, const char* def, char* buf, int bufSize, bool alwaysCopy /* = false */, bool OUTPUT_ONLY(warnIfMissing) /*= false*/) const
{

	const parAttribute* attr = FindAttribute(attrName);
	if (attr)
	{
		return attr->GetStringRepr(buf, bufSize, alwaysCopy);
	}
	else 
	{
#if !__NO_OUTPUT
		if (warnIfMissing)
		{
			parWarningf("Couldn't find expected attribute \"%s\" - using default value \"%s\"", attrName, def);
		}
#endif
		if (def && alwaysCopy)
		{
			safecpy(buf, def, bufSize);
			return buf;
		}
		return def;
	}

}

void parAttributeList::OnLoad(parTreeNode* node)
{
	Reset();

	parTreeNode::ChildNodeIterator iter = node->BeginChildren();

	while(iter != node->EndChildren())
	{
		parTreeNode* child = *iter;
		const char* name = child->GetElement().GetName();
		int type = child->GetElement().FindAttributeIntValue("type", -1);
		switch(type)
		{
		case parAttribute::BOOL: AddAttribute(name, child->ReadStdLeafBool()); break;
		case parAttribute::INT64: AddAttribute(name, child->ReadStdLeafInt64()); break;
		case parAttribute::DOUBLE: AddAttribute(name, child->ReadStdLeafDouble()); break;
		case parAttribute::STRING: AddAttribute(name, child->ReadStdLeafString()); break;
		default:
			parAssertf(0, "Unknown type %d in attribute %s", type, name);
		}
		++iter;
	}

	if (!PARSER.Settings().GetFlag(parSettings::NEVER_SORT_ATTR_LIST))
	{
		SortAttributes();
	}
}

void parAttributeList::OnSave(parTreeNode* node)
{
	for(int i = 0; i < m_Attributes.GetCount(); i++)
	{
		parAttribute& attr = m_Attributes[i];
		parTreeNode* child = NULL;
		switch(attr.GetType())
		{
		case parAttribute::BOOL: child = node->AppendStdLeafChild(attr.GetName(), attr.GetBoolValue()); break;
		case parAttribute::INT64: child = node->AppendStdLeafChild(attr.GetName(), attr.GetInt64Value()); break;
		case parAttribute::DOUBLE: child = node->AppendStdLeafChild(attr.GetName(), attr.GetDoubleValue()); break;
		case parAttribute::STRING: child = node->AppendStdLeafChild(attr.GetName(), attr.GetStringValue()); break;
		default:
			parAssertf(0, "Unknown type %d in attribute %s", attr.GetType(), attr.GetName());
		}
		if (child)
		{
			child->GetElement().AddAttribute("type", attr.GetType());
		}
	}
}
