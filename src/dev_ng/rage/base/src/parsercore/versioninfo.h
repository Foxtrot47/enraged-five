// 
// parsercore/versioninfo.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSERCORE_VERSIONINFO_H
#define PARSERCORE_VERSIONINFO_H

#include <limits.h>

namespace rage {

// PURPOSE: Represents version information for a parser element. There are two components to the version
// info, a major version number and a minor version number. Two version numbers (e.g. one from the 
// file and one from the code) can be compared to tell whether they are incompatable or which version
// is newer. Version numbers are incompatable if their major version numbers differ.
class parVersionInfo
{
public:

	parVersionInfo() : m_Major(0), m_Minor(0) {};
	parVersionInfo(u32 major, u32 minor) : m_Major((u16)major), m_Minor((u16)minor)
	{
		FastAssert(major < USHRT_MAX);
		FastAssert(minor < USHRT_MAX);
	};

	// PURPOSE: Builds a parVersionInfo structure from a string in "major.minor" notation. e.g. "1.24" or "54.1462"
	explicit parVersionInfo(const char* str);

	parVersionInfo& operator=(const parVersionInfo& vers)
	{
		m_Major = vers.m_Major;
		m_Minor = vers.m_Minor;
		return *this;
	}

	// PURPOSE: An enumeration describing how two version info structures compare
	enum Status {
		INCOMPATABLE,		// Major version numbers are different. 
		FILE_IS_NEWER,		// Major version numbers are the same, file's minor version is greater than code's.
		CODE_IS_NEWER,		// Major version numbers are the same, file's minor version is less than code's.
		EXACT_MATCH,		// Major and minor version numbers are the same.
		FILE_UNVERSIONED,	// File does not contain a version number (or version number is 0.0)
	};

	// PURPOSE: Compare two version info structures
	// PARAMS:
	//		fileVersion - The version number coming from the file
	//		codeVersion - The version number coming from the code 
	// RETURNS:	Status code giving the comparison result.
	static Status CompareVersions(const parVersionInfo& fileVersion, const parVersionInfo& codeVersion);

	// PURPOSE: Returns true on the special case of version number 0.0
	bool IsZero() const;

	// RETURNS: the major version number.
	u32 GetMajor() const;

	// RETURNS: The minor version number.
	u32 GetMinor() const;

protected:
	u16 m_Major;
	u16 m_Minor;
};

inline bool parVersionInfo::IsZero() const
{
	return m_Major == 0 && m_Minor == 0;
}

inline u32 parVersionInfo::GetMajor() const
{
	return m_Major;
}

inline u32 parVersionInfo::GetMinor() const
{
	return m_Minor;
}

} // namespace rage

#endif // PARSERCORE_VERSIONINFO_H
