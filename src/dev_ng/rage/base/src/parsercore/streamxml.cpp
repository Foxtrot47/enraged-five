// 
// parsercore/streamxml.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "streamxml.h"

#include "attribute.h"
#include "element.h"

#include "file/asset.h"
#include "diag/output.h"
#include "math/float16.h"
#include "parser/optimisations.h"
#include "string/string.h"
#include "system/alloca.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"

#include <limits>

#define PARXML_ASSERT(test, msg, ...) parAssertf((test), "File '%s' line %d: " msg, m_Stream->GetName(), m_LineNumber+1,##__VA_ARGS__)

#define PARSE_CHARACTER_CODES 1

PARSER_OPTIMISATIONS();

using namespace rage;

const char* common_strings[] = {
	// common attribute names
	"x",
	"y",
	"z",
	"w",
	"v",
	"value",
	"content",
	"type",
	"parent",
	"ref",
	// common attribute values
	"0.000000",
	"1.000000",
	"-1.000000",
	"0.500000",
	"100.000000",
	"true",
	"false",
	"0",
	"1",
	"ascii",
	"0.0",
	"1.0",
	"char_array",
	"short_array",
	"int_array",
	"float_array",
	"vector2_array",
	"vec2v_array",
	"vector3_array",
	"vector4_array",
	"matrix33",
	"matrix34",
	"matrix43",
	"matrix44",
	"int64_array",
	"double_array",
	"utf16",
	"binary",
	// Some common element names
	"Item",
	"Type",
	"Position",
	"Direction",
	"Size",
	"Color", "Colour",
	"Name",
	"xi:include",
	"xi:fallback",
	NULL
};

u8 s_ToBase64LUT[] = {
	'A','B','C','D','E','F','G','H',
	'I','J','K','L','M','N','O','P',
	'Q','R','S','T','U','V','W','X',
	'Y','Z','a','b','c','d','e','f',
	'g','h','i','j','k','l','m','n',
	'o','p','q','r','s','t','u','v',
	'w','x','y','z','0','1','2','3',
	'4','5','6','7','8','9','+','/'
};

// Takes a 6 bit chunk, converts it to the base64 representation
inline u8 ToBase64(u8 in)
{
	parAssertf(in < 64 , "Value out of range for base64 encoding");
	return s_ToBase64LUT[in];
}

// Takes a base64 encoded character, converts it to its 6-bit chunk
u8 s_FromBase64LUT[256] = {
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,				//0-F
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,				//10-1F
	0,0,0,0,0,0,0,0,0,0,0,62,0,0,0,63,				//20-2F
	52,53,54,55,56,57,58,59,60,61,0,0,0,0,0,0,		//30-3F
	0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,			//40-4F
	15,16,17,18,19,20,21,22,23,24,25,0,0,0,0,0,		//50-5F
	0,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40, //60-6F
	41,42,43,44,45,46,47,48,49,50,51,0,0,0,0,0,		//70-7F
};
inline u8 FromBase64(u8 in)
{
	parAssertf(in < 128 , "Value out of range for base64 encoding");
	return s_FromBase64LUT[in];
}

#define N (1 << 0) // Name
#define S (1 << 1) // Space
#define B (1 << 2) // NameBegin
#define F (1 << 3) // Float character
#define I (1 << 4) // Int character

// Table in comments from http://web.cs.mun.ca/~michael/c/ascii-table.html
const u8 parStreamInXml::sm_XmlCharacterTraitTable[256] = {
			//  Char  Dec  Oct  Hex 
			//  --------------------
	0,		//  (nul)   0 0000 0x00 
	0,		//  (soh)   1 0001 0x01 
	0,		//  (stx)   2 0002 0x02 
	0,		//  (etx)   3 0003 0x03 
	0,		//  (eot)   4 0004 0x04 
	0,		//  (enq)   5 0005 0x05 
	0,		//  (ack)   6 0006 0x06 
	0,		//  (bel)   7 0007 0x07 
	0,		//  (bs)    8 0010 0x08 
	S,		//  (ht)    9 0011 0x09 
	S,		//  (nl)   10 0012 0x0a 
	S,		//  (vt)   11 0013 0x0b 
	0,		//  (np)   12 0014 0x0c 
	S,		//  (cr)   13 0015 0x0d 
	0,		//  (so)   14 0016 0x0e 
	0,		//  (si)   15 0017 0x0f 
	0,		//  (dle)  16 0020 0x10 
	0,		//  (dc1)  17 0021 0x11 
	0,		//  (dc2)  18 0022 0x12 
	0,		//  (dc3)  19 0023 0x13 
	0,		//  (dc4)  20 0024 0x14 
	0,		//  (nak)  21 0025 0x15 
	0,		//  (syn)  22 0026 0x16 
	0,		//  (etb)  23 0027 0x17 
	0,		//  (can)  24 0030 0x18 
	0,		//  (em)   25 0031 0x19 
	0,		//  (sub)  26 0032 0x1a 
	0,		//  (esc)  27 0033 0x1b 
	0,		//  (fs)   28 0034 0x1c 
	0,		//  (gs)   29 0035 0x1d 
	0,		//  (rs)   30 0036 0x1e 
	0,		//  (us)   31 0037 0x1f 
	S,		//  (sp)   32 0040 0x20
	0,		//  !      33 0041 0x21
	0,		//  "      34 0042 0x22
	0,		//  #      35 0043 0x23
	0,		//  $      36 0044 0x24
	0,		//  %      37 0045 0x25
	0,		//  &      38 0046 0x26
	0,		//  '      39 0047 0x27
	0,		//  (      40 0050 0x28
	0,		//  )      41 0051 0x29
	0,		//  *      42 0052 0x2a
	F+I,	//  +      43 0053 0x2b
	0,		//  ,      44 0054 0x2c
	N+F+I,  //  -      45 0055 0x2d
	N+F,	//  .      46 0056 0x2e
	0,		//  /      47 0057 0x2f
	N+F+I,	//  0      48 0060 0x30
	N+F+I,	//  1      49 0061 0x31
	N+F+I,	//  2      50 0062 0x32
	N+F+I,	//  3      51 0063 0x33
	N+F+I,	//  4      52 0064 0x34
	N+F+I,	//  5      53 0065 0x35
	N+F+I,	//  6      54 0066 0x36
	N+F+I,	//  7      55 0067 0x37
	N+F+I,	//  8      56 0070 0x38
	N+F+I,	//  9      57 0071 0x39
	N+B,	//  :      58 0072 0x3a
	0,		//  ;      59 0073 0x3b
	0,		//  <      60 0074 0x3c
	0,		//  =      61 0075 0x3d
	0,		//  >      62 0076 0x3e
	0,		//  ?      63 0077 0x3f
	0,		//  @      64 0100 0x40
	N+B+I,	//  A      65 0101 0x41
	N+B+I,	//  B      66 0102 0x42
	N+B+I,	//  C      67 0103 0x43
	N+B+I,	//  D      68 0104 0x44
	N+B+F+I,//  E      69 0105 0x45
	N+B+I,	//  F      70 0106 0x46
	N+B,	//  G      71 0107 0x47
	N+B,	//  H      72 0110 0x48
	N+B,	//  I      73 0111 0x49
	N+B,	//  J      74 0112 0x4a
	N+B,	//  K      75 0113 0x4b
	N+B,	//  L      76 0114 0x4c
	N+B,	//  M      77 0115 0x4d
	N+B,	//  N      78 0116 0x4e
	N+B,	//  O      79 0117 0x4f
	N+B,	//  P      80 0120 0x50
	N+B,	//  Q      81 0121 0x51
	N+B,	//  R      82 0122 0x52
	N+B,	//  S      83 0123 0x53
	N+B,	//  T      84 0124 0x54
	N+B,	//  U      85 0125 0x55
	N+B,	//  V      86 0126 0x56
	N+B,	//  W      87 0127 0x57
	N+B+I,	//  X      88 0130 0x58
	N+B,	//  Y      89 0131 0x59
	N+B,	//  Z      90 0132 0x5a
	0,		//  [      91 0133 0x5b
	0,		//  \      92 0134 0x5c
	0,		//  ]      93 0135 0x5d
	0,		//  ^      94 0136 0x5e
	N+B,	//  _      95 0137 0x5f
	0,		//  `      96 0140 0x60
	N+B+I,	//  a      97 0141 0x61
	N+B+I,	//  b      98 0142 0x62
	N+B+I,	//  c      99 0143 0x63
	N+B+I,	//  d     100 0144 0x64
	N+B+F+I,//  e     101 0145 0x65
	N+B+I,	//  f     102 0146 0x66
	N+B,	//  g     103 0147 0x67
	N+B,	//  h     104 0150 0x68
	N+B,	//  i     105 0151 0x69
	N+B,	//  j     106 0152 0x6a
	N+B,	//  k     107 0153 0x6b
	N+B,	//  l     108 0154 0x6c
	N+B,	//  m     109 0155 0x6d
	N+B,	//  n     110 0156 0x6e
	N+B,	//  o     111 0157 0x6f
	N+B,	//  p     112 0160 0x70
	N+B,	//  q     113 0161 0x71
	N+B,	//  r     114 0162 0x72
	N+B,	//  s     115 0163 0x73
	N+B,	//  t     116 0164 0x74
	N+B,	//  u     117 0165 0x75
	N+B,	//  v     118 0166 0x76
	N+B,	//  w     119 0167 0x77
	N+B+I,	//  x     120 0170 0x78
	N+B,	//  y     121 0171 0x79
	N+B,	//  z     122 0172 0x7a
	0,		//  {     123 0173 0x7b
	0,		//  |     124 0174 0x7c
	0,		//  }     125 0175 0x7d
	0,		//  ~     126 0176 0x7e
	0,		//  (del) 127 0177 0x7f

};

#undef N
#undef S
#undef B
#undef F
#undef I

atCaseSensitiveStringMap<const char*> parStreamInXml::sm_MiniNameTable;

// Could use MAKE_MAGIC_NUMBER here but then the bytes are in the wrong order.
// We want big-endian order since its easier to read.
#define CHAR_CODE(a,b,c,d) (u32)(((a) << 24) | ((b) << 16) | ((c) << 8) | (d))

u32 g_XmlStartTable[] = 
{
	//  Characters to use. ' ' means
	//  any whitespace                  Mask bits, only these are significant
	CHAR_CODE('<', '*', '*', '*' ), 0xFF000000,
	CHAR_CODE(' ', '<', '*', '*' ), 0xFFFF0000,
	CHAR_CODE(' ', ' ', '<', '*' ), 0xFFFFFF00,
	CHAR_CODE(' ', ' ', ' ', '<' ), 0xFFFFFFFF,
	CHAR_CODE(' ', ' ', ' ', ' ' ), 0xFFFFFFFF,

	CHAR_CODE(0xEF,0xBB,0xBF,'<' ), 0xFFFFFFFF,
	CHAR_CODE(0xEF,0xBB,0xBF,' ' ), 0xFFFFFFFF,

	CHAR_CODE(0xFE,0xFF,0x00,'<' ), 0xFFFFFFFF,
	CHAR_CODE(0xFE,0xFF,0x00,' ' ), 0xFFFFFFFF,

	CHAR_CODE(0xFF,0xFE,'<', 0x00), 0xFFFFFFFF,
	CHAR_CODE(0xFF,0xFE,' ', 0x00), 0xFFFFFFFF,

	CHAR_CODE(0x00,'<', '*', '*' ), 0xFFFF0000,
	CHAR_CODE(0x00,' ', 0x00,'<' ), 0xFFFFFFFF,
	CHAR_CODE(0x00,' ', 0x00,' ' ), 0xFFFFFFFF,

//	CHAR_CODE('<', 0x0, '*', '*' ), 0xFFFF0000, // redundant with first pattern
	CHAR_CODE(' ', 0x00,'<', 0x00), 0xFFFFFFFF,
	CHAR_CODE(' ', 0x00,' ', 0x00), 0xFFFFFFFF,
};

bool parStreamInXml::IsValidXmlStart(u32 headerBytes)
{
	headerBytes = sysEndian::NtoB(headerBytes);

	char* headerChars = reinterpret_cast<char*>(&headerBytes);
	if (IsXmlWhitespaceCharacter((u8)headerChars[0])) {headerChars[0] = ' ';}
	if (IsXmlWhitespaceCharacter((u8)headerChars[1])) {headerChars[1] = ' ';}
	if (IsXmlWhitespaceCharacter((u8)headerChars[2])) {headerChars[2] = ' ';}
	if (IsXmlWhitespaceCharacter((u8)headerChars[3])) {headerChars[3] = ' ';}

	int numTableEntries = sizeof(g_XmlStartTable) / (sizeof(u32) * 2);
	for(int i = 0; i < numTableEntries; i++)
	{
		u32 compValue = g_XmlStartTable[i*2];
		u32 mask = g_XmlStartTable[i*2+1];
		if ((headerBytes & mask) == (compValue & mask))
		{
			return true;
		}
	}
	return false;
}

void parStreamInXml::AddCommonGameStrings(const char* const names[])
{
	for(int i = 0; names[i] != NULL; i++)
	{
		sm_MiniNameTable.Insert(names[i], names[i]);
	}
	sm_MiniNameTable.FinishInsertion();
}

void parStreamInXml::BeginAddingStaticStrings()
{

}

void parStreamInXml::AddStaticString(const char* str)
{
	u32 hash = sm_MiniNameTable.Hash(str);

	if (sm_MiniNameTable.Has(hash))
	{
		const char** oldStr = sm_MiniNameTable.SafeGet(hash);
		if (strcmp(*oldStr, str) != 0)
		{
			Quitf(ERR_PAR_COLL,"Found a hash collision with autoregistered strings '%s' and '%s'", str, *oldStr);
		}
		else
		{
			// nothing to do
		}
	}
	else
	{
		sm_MiniNameTable.InsertSorted(hash, str); // using insertsorted here instead of insert since we do all the 'has' checks above
	}
}

void parStreamInXml::EndAddingStaticStrings()
{
	sm_MiniNameTable.FinishInsertion();
}

void parStreamInXml::InitClass()
{
	for(int i = 0; common_strings[i] != NULL; i++)
	{
		sm_MiniNameTable.Insert(common_strings[i], common_strings[i]);
	}

	sm_MiniNameTable.FinishInsertion();
}

void parStreamInXml::ShutdownClass()
{
	sm_MiniNameTable.Reset();
}

parStreamInXml::parStreamInXml(fiStream * s, const parSettings& settings)
: parStreamIn(s, settings)
, m_ReadUnendedLeaf(false)
, m_JustBeganElement(false)
, m_JustReadData(false)
, m_DataIncomplete(false)
, m_ReadingCdata(false)
, m_ExpectingData(false)
, m_LastEncoding(UNSPECIFIED)
, m_Base64Buffer(0)
, m_Base64BufferSize(0)
, m_LineNumber(0)
, m_FileEncoding(ENC_UTF8)
, m_ParseError(false)
, m_UnclosedTagDepth(0)
{
}

parStreamInXml::~parStreamInXml()
{

}

bool parStreamInXml::IsDataReadIncomplete()
{
	return m_DataIncomplete;
}

void parStreamInXml::SetHeaderBytes(u32 bytes)
{
	PushBack(reinterpret_cast<const char*>(&bytes), 4);
}

/*
Pull mode pseudocode

ReadElementInternal()
Read <name
Read attr list
If next char is /, next is >, return true
else if it's >, return false

ReadBeginElement()
Assert(!justReadLeaf)
bool leaf = ReadElementInternal()
justReadLeaf = leaf
justBeganChild = true

ReadLeafElement()
Assert(!justReadLeaf)
bool leaf = ReadElementInternal()
if (!leaf):
skip past whitespace
assert that next non-whitespace thing is a matching close tag
justReadLeaf = true
justBeganChild = false

ReadEndElement()
if (justReadLeaf:
justReadLeaf = false
return
skip past whitespace
assert that the next non-whitespace thing is a close tag
justreadleaf = false
justbeganchild = false

ReadData()
assert(justbeganchild)
read through the next close tag (make sure it matches parent?)
justReadLeaf = true // data can be considered a leaf node
justbeganchild = false
*/

__forceinline int parStreamInXml::ReadEscapedCharacter()
{
	int ch = ReadRawCharacter();
	if (Unlikely(ch == '&')) {
		char buf[64];
		int stored = 0;
		while ((ch = ReadRawCharacter()) >= 'a' && ch <= 'z' )
		{
			if (stored < (int)sizeof(buf)-1)
			{
				buf[stored++] = (char)ch;
			}
		}
#if PARSE_CHARACTER_CODES
		if (Unlikely(stored == 0 && ch == '#'))
		{
			buf[stored++] = '#';
			while(1)
			{
				ch = ReadRawCharacter();
				if (!((stored == 1 && ch == 'x') || (ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F') || (ch >= 'a' && ch <= 'f')))
				{
					break;
				}
				buf[stored++] = (char)ch;
			}
		}
#endif

		buf[stored] = 0;
		PARXML_ASSERT(ch == ';', "All entity references should end in ';' - got \"&%s%c\"", buf, ch);

		// add 256 to metacharacters so we can tell them apart from real ones
		// BUT casting the result to a (char) will "just work"
		if (!strcmp(buf,"lt")) 
		{
			return ESC_LT;
		}
		else if (!strcmp(buf,"gt")) 
		{
			return ESC_GT;
		}
		else if (!strcmp(buf,"apos")) 
		{
			return ESC_APOS;
		}
		else if (!strcmp(buf,"quot")) 
		{
			return ESC_QUOT;
		}
		else if (!strcmp(buf,"amp")) 
		{
			return ESC_AMP;
		}
#if PARSE_CHARACTER_CODES
		else if (buf[0] == '#')
		{
			int intval = 0;
			if (buf[1] == 'x') {
				intval = strtol(buf+2,NULL, 16);
			}
			else
			{
				intval = strtol(buf+1, NULL, 10);
			}
			PARXML_ASSERT(intval > 0 && intval < SHRT_MAX, "Invalid character code 0x%x", intval);
			// might be a high character - utf encode it
			char utfBuf[3];
			int numChars = WideToUtf8Char((char16)intval, utfBuf);
			if (numChars > 1)
			{
				PushBack(utfBuf+1, numChars-1);
			}
			return utfBuf[0] + 256; // "escape" the returned character - hopefully should not be necessary for other utf8 encoded chars
		}
#endif
		else 
		{
			PARXML_ASSERT(0, "Don't know how to handle entity reference &%s;", buf);
			return ESC_UNKNOWN;
		}
	}
	else {
		return ch;
	}
}

void parStreamInXml::PushBackAndEscape(int c)
{
	switch(c)
	{
	case ESC_LT:	PushBack("&lt;", 4);	break;
	case ESC_GT:	PushBack("&gt;", 4);	break;
	case ESC_APOS:	PushBack("&apos;", 6);	break;
	case ESC_QUOT:	PushBack("&quot;", 6);	break;
	case ESC_AMP:	PushBack("&amp;", 5);	break;
	default:
		PushBack((char)c);
	}
}

// out-of-lined from ReadRawCharacter, to improve icache usage where RRC is inlined
int parStreamInXml::InternalReadUtf16Char(int c)
{
	char16 utf16Char;
	char* utf16Bytes = (char*)(&utf16Char);
	utf16Bytes[0] = (char)c;
	utf16Bytes[1] = (char)m_Stream->FastGetCh();

	utf16Char = (m_FileEncoding == ENC_UTF16_BE) ? sysEndian::BtoN(utf16Char) : sysEndian::LtoN(utf16Char);
	char buf[3];
	int numChars = WideToUtf8Char(utf16Char, buf);
	c = buf[0];
	if (numChars > 1)
	{
		PushBack(buf+1, numChars-1);
	}
	return c;
}


bool parStreamInXml::Peek(const char* str, bool consumeOnMatch) 
{
	int i;
	for(i = 0; str[i] != '\0'; i++) {
		int c = ReadRawCharacter();
		if (c != str[i]) {
			// no match. push the chars back
			PushBack((char)c);
			PushBack(str, i);
			return false;
		}
	}
	if (!consumeOnMatch) {
		PushBack(str, i);
	}
	return true;
}

bool parStreamInXml::Match(const char* str, char* tagBuffer)
{
	for(int i = 0; str[i] != '\0'; i++) {
		if (str[i] == 's') {
			SkipWhitespace();
		}
		else if (str[i] == 'S') {
			SkipWhitespaceAndComments();
		}
		else if (str[i] == 'w') {
			ReadToken(tagBuffer);
		}
		else if (str[i] != (char)ReadRawCharacter())
		{
			PARXML_ASSERT(0, "Data file is not in the correct format");
			m_ParseError = true;
		}
	}
	return true;
}

bool parStreamInXml::Match(const char* str)
{
	char buf[MAX_ELEMENT_NAME_LENGTH];
	return Match(str, buf);
}

template<bool TokenPred(int)>
int parStreamInXml::ReadAnyToken(char* buf, int bufSize)
{
	int c = ReadRawCharacter();
	int len = 0;

	while(TokenPred(c)) {
			if (Unlikely(len >= bufSize-1))
			{
				break;
			}
			buf[len] = (char)c;
			++len;
			c = ReadRawCharacter();
		};
	PARXML_ASSERT(len < bufSize-1, "Element name is too long");
	buf[len] = '\0';

	PARXML_ASSERT(len > 0, "Couldn't find a token to read");

	if (Likely(c >= 0)) {
		PushBack((char)c);
	}

	return len;
}

int parStreamInXml::ReadToken(char* buf, int bufSize/* =MAX_ELEMENT_NAME_LENGTH */)
{
	return ReadAnyToken<IsXmlNameCharacter>(buf, bufSize);
}

void parStreamInXml::SkipTo(const char* str)
{
	bool stringFound = false;
	char firstChar = str[0];
	const char* rest = str+1;
	while (!stringFound)
	{
		int c = firstChar + 1; // something that's not == to firstChar
		while (c != firstChar)
		{
			c = ReadRawCharacter();
			PARXML_ASSERT(c != -1, "Couldn't find match for string \"%s\"", str);
			if (c == -1)
			{
				m_ParseError = true;
				return;
			}
		}
		if (rest == '\0' || Peek(rest)) {
			stringFound = true;
		}
	}
}

void parStreamInXml::SkipWhitespace()
{
	int c = ' ';
	do
	{
		c = ReadRawCharacter();
	}
	while(IsXmlWhitespaceCharacter(c));

	PushBack((char)c);
}

int parStreamInXml::SkipWhitespaceAndComments(char* buf, int bufSize)
{
	// skips over whitespace, comments
	int c = ' ';
	int len = 0;
	if (!buf) {
		len = bufSize - 2;
	}

	bool whitespace = true;

	while(whitespace && len < bufSize-1) {
		c = ReadRawCharacter();
		if (c == '<') {
			int nextChar = ReadRawCharacter();
			if (nextChar == '!' || nextChar == '?')
			{
				PushBack((char)nextChar);
				if (Peek("!--")) {
					SkipTo("-->");
				}
				else if (Peek("![CDATA[", false))
				{
					PushBack((char)c);
					whitespace = false;
				}
				else if (Peek('!')) {
					// Skip DTD data
					int bracketCount = 1;
					while(bracketCount > 0) {
						c = ReadRawCharacter();
						if (c == '<') {
							bracketCount++;
						}
						else if (c == '>') {
							bracketCount--;
						}
						else if (c == -1)
						{
							PARXML_ASSERT(0, "Found the end of file while skipping over DTD data.");
							m_ParseError = true;
							whitespace = false;
							break;
						}
					}
				}
				else if (Peek('?')) {
					SkipTo("?>");
				}
				else {
					whitespace = false;
					PushBack((char)c);
				}
			}
			else {
				whitespace = false;
				/*
				PushBack((char)nextChar);
				PushBack((char)c);
				*/
				// these PushBacks were causing LHSes in reading and writing the array count. Do it by hand:
				int count = m_PushBackBuffer.GetCount();
				m_PushBackBuffer.SetCount(count+2);
				m_PushBackBuffer.GetElements()[count] = (char)nextChar;
				m_PushBackBuffer.GetElements()[count+1] = (char)c;
			}
		}
		else if (IsXmlWhitespaceCharacter(c)) {
			whitespace = true;
			if (buf)
			{
				buf[len] = (char)c;
				len++;
			}
		}
		else {
			whitespace = false;
			if (c != -1) // it's possible we've read up to the EOF. Don't put the EOF character in the push-back buffer.
			{
				PushBack((char)c);
			}
		}
	}
	if (buf)
	{
		buf[len] = '\0';
		len++;

		// Two options for what to do if the whitespace buffer is full
		// Either assume the next bit is data, or assume it's an element.
#if 0
		// Assume it's data. The next character that gets read, if its a <, will
		// start an element block and if its more whitespace, will start
		// a data block.
#else
		// Assume it's an element. Skip any more whitespace (but don't add it to the buffer)
		if (len == bufSize)
		{
			SkipWhitespaceAndComments(NULL, -1);
		}
#endif
	}
	return len;
}

int parStreamInXml::ReadAttributeValue(char* str) {
	int len = 0;
	int c = ReadEscapedCharacter();
	PARXML_ASSERT((c == '"' || c == '\''), "All attributes must begin and end with quotes");
	if ((c != '"' && c != '\''))
	{
		m_ParseError = true;
		return 0;
	}
	int quoteChar = c;
	bool truncateAttributes = GetSettings().GetFlag(parSettings::TRUNCATE_ATTRIBUTES);

	c = ReadEscapedCharacter();
	while(c > 0 && c != quoteChar && ((len < MAX_ATTRIBUTE_VALUE_LENGTH-1) || truncateAttributes))
	{
		PARXML_ASSERT(c != '<', "Unescaped <s are not allowed in attribute values");
		if (c == '<')
		{
			m_ParseError = true;
			break;
		}

		if (len < MAX_ATTRIBUTE_VALUE_LENGTH - 1)
		{
			str[len] = (char)c;
			len++;
		}
		c = ReadEscapedCharacter();
	}
	PARXML_ASSERT(truncateAttributes || (len < MAX_ATTRIBUTE_VALUE_LENGTH-1), "Attribute value is too long");
	PARXML_ASSERT(c == quoteChar, "Error: Found an unclosed attribute");
	if (((len >= MAX_ATTRIBUTE_VALUE_LENGTH-1) || (c != quoteChar)) && !truncateAttributes)
	{
		m_ParseError = true;
	}
	str[len] = '\0';
	len++;

	return len;
}

int parStreamInXml::ReadBinaryString(char* str, int size)
{
//	parAssertf(m_PushBackBuffer.GetCount() == 0 && "Pushback buffer must be empty, or we need a slow path for decoding binary");
	// did we read any data from before?
	int pos = 0;
	switch(m_Base64BufferSize) {
		case 0:
			break;
		case 1:
			str[pos++] = (u8)(m_Base64Buffer & 0xFF);
			break;
		case 2:
			str[pos++] = (u8)((m_Base64Buffer >> 8) & 0xFF);
			str[pos++] = (u8)(m_Base64Buffer & 0xFF);
			break;
		default:
			PARXML_ASSERT(0, "Invalid buffer size");
	}

	m_Base64Buffer = 0;
	m_Base64BufferSize = 0;

	int buf[4];
	buf[0] = buf[1] = buf[2] = buf[3] = 0;

	while(pos < size) {
		if (m_PushBackBuffer.GetCount() > 0)
		{
			SkipWhitespace();
			buf[0] = ReadRawCharacter();
			if (buf[0] == '<') {
				m_DataIncomplete = false;
				PushBack((char)buf[0]);
				return pos;
			}
			buf[1] = ReadRawCharacter();
			buf[2] = ReadRawCharacter();
			buf[3] = ReadRawCharacter();
		}
		else
		{
			do 
			{
				buf[0] = m_Stream->FastGetCh();
			} while(IsXmlWhitespaceCharacter(buf[0]));
			if (buf[0] == '<') {
				m_DataIncomplete = false;
				PushBack((char)buf[0]);
				return pos;
			}
			buf[1] = m_Stream->FastGetCh();
			buf[2] = m_Stream->FastGetCh();
			buf[3] = m_Stream->FastGetCh();
		}

		if (buf[0] == -1 || buf[1] == -1 || buf[2] == -1 || buf[3] == -1)
		{
			PARXML_ASSERT(0, "Found EOF while reading base64 encoded data");
			m_ParseError = true;
			m_DataIncomplete = false;
			return pos;
		}

		u8 chunks[4];
		chunks[0] = s_FromBase64LUT[buf[0]];
		chunks[1] = s_FromBase64LUT[buf[1]];
		chunks[2] = s_FromBase64LUT[buf[2]];
		chunks[3] = s_FromBase64LUT[buf[3]];

		for(int i = 0; i < 4; i++)
		{
			if (chunks[i] == 0 && buf[i] != 'A')
			{
				PARXML_ASSERT(0, "Found a non-base64 character in a block of base64 data");
				m_ParseError = true;
				m_DataIncomplete = false;
				return pos;
			}
		}

		u8 bytes[3];
		bytes[0] = chunks[0] << 2;
		bytes[0] |= chunks[1] >> 4;
		bytes[1] = chunks[1] << 4;
		bytes[1] |= chunks[2] >> 2;
		bytes[2] = (chunks[2] << 6) & 0xc0;
		bytes[2] |= chunks[3];

		if (buf[2] == '=') {
			str[pos++] = bytes[0];
			return pos;
		}
		else if (buf[3] == '=') {
			str[pos++] = bytes[0];
			if (pos < size) {
				str[pos++] = bytes[1];
			}
			else {
				m_Base64Buffer = bytes[1];
				m_Base64BufferSize = 1;
			}
			return pos;
		}
		else {
			if (pos + 3 <= size) {
				str[pos++] = bytes[0];
				str[pos++] = bytes[1];
				str[pos++] = bytes[2];
			}
			else {
				// handle end of data
				if (pos == size - 1) {
					str[pos++] = bytes[0];
					m_Base64BufferSize = 2;
					m_Base64Buffer = (bytes[1] << 8) | bytes[2];
				}
				else if (pos == size - 2) {
					str[pos++] = bytes[0];
					str[pos++] = bytes[1];
					m_Base64BufferSize = 1;
					m_Base64Buffer = bytes[2];
				}
			}

		}

	}
	return pos;
}

int parStreamInXml::ReadUtf16String(char16* str, int size)
{
	int pos = 0;
	u8 cbuf[3];
	while(pos < size-1)
	{
		int c = ReadEscapedCharacter();
		if (c == '<') {
			// Normally it's bogus to call PushBack on an escaped character, but in this case
			// we know that we're talking about a character that started as "<" not "&lt;"
			// so pushing and poping it won't change its value.
			PushBack((u8)c);
			str[pos] = 0; // Add the terminating \0
			pos++;
			m_DataIncomplete = false;
			return pos;
		}
		if (c == -1)
		{
#if __WIN32
			PARXML_ASSERT(0, "Found an EOF while reading a UTF16 string. String was %w", str);
#else
			PARXML_ASSERT(0, "Found an EOF while reading a UTF16 string.");
#endif
			m_ParseError = true;
			return pos;
		}
		cbuf[0] = (u8)c;
		if (cbuf[0] >= 0xC0) {
			cbuf[1] = (u8)ReadRawCharacter();
		}
		if (cbuf[0] >= 0xE0) {
			cbuf[2] = (u8)ReadRawCharacter();
		}
		Utf8ToWideChar((char*)cbuf, str[pos++]);
	}
	return pos;
}
template<typename _Type>
int parStreamInXml::ReadIntArrayString(_Type* data, int size)
{
	char buffer[128];
	int pos = 0;
	while(pos < size)
	{
		SkipWhitespace();
		if (Peek('<', false)) {
			m_DataIncomplete = false;
			return pos;
		}
		int len = ReadAnyToken<IsIntCharacter>(buffer, 128);
		if (len == 0)
		{
			m_ParseError = true;
			break;
		}
		data[pos] = (_Type)parUtils::StringToInt64(buffer);
		pos++;
	}
	return pos;
}

template<typename _Type>
int parStreamInXml::ReadFloatArrayString(_Type* data, int size)
{
	char buffer[128];
	int pos = 0;
	while(pos < size)
	{
		SkipWhitespace();
		if (Unlikely(Peek('<', false))) {
			m_DataIncomplete = false;
			return pos;
		}

		if (Unlikely(ReadAnyToken<IsFloatCharacter>(buffer, 128) == 0))
		{
			m_ParseError = true;
			break;
		}
		data[pos] = (_Type)parUtils::StringToDouble(buffer);
		pos++;
	}
	return pos;
}

int parStreamInXml::ReadVector2ArrayString(Vector2* data, int size)
{
	char buffer[128];
	int pos = 0;
	while(pos < size)
	{
		for(int i = 0; i < 2; i++)
		{
			SkipWhitespace();
			if (Peek('<', false)) {
				m_DataIncomplete = false;
				return pos;
			}
			if (Unlikely(ReadAnyToken<IsFloatCharacter>(buffer, 128) == 0))
			{
				m_ParseError = true;
				break;
			}
			data[pos][i] = (float)atof(buffer);
		}
		pos++;
	}
	return pos;
}

int parStreamInXml::ReadVec2VArrayString(Vec2V* data, int size)
{
	char buffer[128];
	int pos = 0;
	while(pos < size)
	{
		for(int i = 0; i < 2; i++)
		{
			SkipWhitespace();
			if (Peek('<', false)) {
				m_DataIncomplete = false;
				return pos;
			}
			if (Unlikely(ReadAnyToken<IsFloatCharacter>(buffer, 128) == 0))
			{
				m_ParseError = true;
				break;
			}
			data[pos][i] = (float)atof(buffer);
		}
		pos++;
	}
	return pos;
}

int parStreamInXml::ReadVector3ArrayString(Vector3* data, int size)
{
	char buffer[128];
	int pos = 0;
	while(pos < size)
	{
		for(int i = 0; i < 3; i++)
		{
			SkipWhitespace();
			if (Peek('<', false)) {
				m_DataIncomplete = false;
				return pos;
			}
			if (Unlikely(ReadAnyToken<IsFloatCharacter>(buffer, 128) == 0))
			{
				m_ParseError = true;
				break;
			}
			data[pos][i] = (float)atof(buffer);
		}
		pos++;
	}
	return pos;
}

int parStreamInXml::ReadVector4ArrayString(Vector4* data, int size)
{
	char buffer[128];
	int pos = 0;
	while(pos < size)
	{
		for(int i = 0; i < 4; i++)
		{
			SkipWhitespace();
			if (Peek('<', false)) {
				m_DataIncomplete = false;
				return pos;
			}
			if (Unlikely(ReadAnyToken<IsFloatCharacter>(buffer, 128) == 0))
			{
				m_ParseError = true;
				break;
			}
			data[pos][i] = (float)atof(buffer);
		}
		pos++;
	}
	return pos;
}

int parStreamInXml::ReadDataInternal(char* str, int size, DataEncoding enc)
{
	/* Different rules for different encodings.

	1) ASCII
	Assert on any markup characters (<, &). These must be escaped.
	2) UTF-16
	Assert on any markup characters, convert the UTF-8 data into UTF-16
	3) Binary
	Assert on any invalid base64 character, convert from base64 to binary
	4) Raw XML
	The tricky one - Read all data up to the _matching_ end tag.
	Stream needs to support multicharacter push-back for this, so we can hit the end tag and back up.
	5) Int array
	Assert on any non-integer character
	6) Float array
	Assert on any non-float character
	7) Vector array
	Assert on any non-float character
	*/
	int c;

	// The functions here set this to false if the data read was completed.
	m_DataIncomplete = true;

	switch (enc) {
		case UNSPECIFIED:
		case ASCII:
			{
				int len = 0;

				bool done = false;
				while(!done)
				{
					if (!m_ReadingCdata)
					{
						// read as many chars as possible from the pcdata span
						c = ReadEscapedCharacter();
						while(c != -1 && c != '<' && len < size-1) {
							PARXML_ASSERT(c != '&', "'&' not allowed here. Use &amp; instead");
							if (c == '&')
							{
								m_ParseError = true;
								break;
							}
							str[len] = (char)c;
							len++;
							c = ReadEscapedCharacter();
						}
						str[len] = '\0';

						// now see what ended the loop - could be:
						// End of pcdata
						// Start of cdata
						// End of file
						// Buffer full
						if (c == '<')
						{
							if (Peek("![CDATA[", true))
							{
								m_ReadingCdata = true;
							}
							else
							{
								// Normally it's bogus to call PushBack on an escaped character, but in this case
								// we know that we're talking about a character that started as "<" not "&quot;"
								// so pushing and poping it won't change its value.
								PushBack((char)c);
								len++; // make sure to include the terminating '\0' in the final block of the data buffer
								m_DataIncomplete = false;
								done = true;
							}
						}
						else if (len == size-1)
						{
							PushBackAndEscape(c);
							m_DataIncomplete = true;
							done = true;
						}
						else if (c == -1)
						{
							PARXML_ASSERT(0, "Found an unclosed PCDATA section. String contents are \"%s\"", str);
							m_ParseError = true;
							m_DataIncomplete = false;
							done = true;
						}
					}
					else { // Reading CDATA
						c = ReadRawCharacter();
						while(c != -1 && len < size-1)
						{
							if (c == ']' && Peek("]>", true))
							{
								m_ReadingCdata = false;
								break;
							}
							str[len] = (char)c;
							len++;
							c = ReadRawCharacter();
						}
						str[len] = '\0';

						// Now see what ended the loop - could be:
						// End of cdata
						// End of file
						// Buffer full
						if (len == size-1)
						{
							PushBackAndEscape(c);
							m_DataIncomplete = true;
							done = true;
						}
						else if (c == -1)
						{
							PARXML_ASSERT(0, "Found an unclosed CDATA section. String contents are \"%s\"", str);
							m_ParseError = true;
							m_DataIncomplete = false;
							done = true;
						}
					}
				}
				return len;
			}

			/*NOTREACHED*/
			break;
		case UTF16:
			return sizeof(char16) * ReadUtf16String(reinterpret_cast<char16*>(str), (size / sizeof(char16))-1);
		case BINARY:
			return ReadBinaryString(str, size);
		case RAW_XML:
			PARXML_ASSERT(0, "Raw XML data is currently unimplemented");
			break;
		case INT_ARRAY:
			return sizeof(int) * ReadIntArrayString<int>(reinterpret_cast<int*>(str), size / sizeof(int));
		case CHAR_ARRAY:
			return sizeof(char) * ReadIntArrayString<char>(reinterpret_cast<char*>(str), size / sizeof(char));
		case SHORT_ARRAY:
			return sizeof(short) * ReadIntArrayString<short>(reinterpret_cast<short*>(str), size / sizeof(short));
		case INT64_ARRAY:
			return sizeof(s64) * ReadIntArrayString<s64>(reinterpret_cast<s64*>(str), size / sizeof(s64));
		case FLOAT_ARRAY:
			return sizeof(float) * ReadFloatArrayString<float>(reinterpret_cast<float*>(str), size / sizeof(float));
		case DOUBLE_ARRAY:
			return sizeof(double) * ReadFloatArrayString<double>(reinterpret_cast<double*>(str), size / sizeof(double));
		case VECTOR2_ARRAY:
			return sizeof(Vector2) * ReadVector2ArrayString(reinterpret_cast<Vector2*>(str), size / sizeof(Vector2));
		case VEC2V_ARRAY:
			return sizeof(Vec2V) * ReadVec2VArrayString(reinterpret_cast<Vec2V*>(str), size / sizeof(Vec2V));
		case VECTOR3_ARRAY:
			return sizeof(Vector3) * ReadVector3ArrayString(vector_cast<Vector3*>(str), size / sizeof(Vector3));
		case VECTOR4_ARRAY:
			return sizeof(Vector4) * ReadVector4ArrayString(vector_cast<Vector4*>(str), size / sizeof(Vector4));
		case MATRIX33:
			{
				const int mtxElts = 3*3;
				PARXML_ASSERT(size >= sizeof(float) * mtxElts, "Need a buffer that's at least %" SIZETFMT "d bytes to load a matrix", sizeof(float) * mtxElts);
				float temp[ mtxElts ];
				ASSERT_ONLY(int count =) ReadFloatArrayString(temp, sizeof(temp) / sizeof(float));
				PARXML_ASSERT(count == mtxElts, "Found a matrix with the wrong number of elements (got %d, expected %d)", count, mtxElts);

				float* floats = reinterpret_cast<float*>(str);

				// "Transpose" from the row-major order we read these in into column major order.
				floats[0] = temp[0];	floats[4] = temp[1];	floats[8] = temp[2];
				floats[1] = temp[3];	floats[5] = temp[4];	floats[9] = temp[5];
				floats[2] = temp[6];	floats[6] = temp[7];	floats[10] = temp[8];
				floats[3] = 0.0f;		floats[7] = 0.0f;		floats[11] = 0.0f;

				m_DataIncomplete = false;

				return sizeof(float) * 12;
			}
		case MATRIX34:
			{
				const int mtxElts = 3*4;
				PARXML_ASSERT(size >= sizeof(float) * 16, "Need a buffer that's at least %" SIZETFMT "d bytes to load a matrix", sizeof(float) * 16);
				float temp[ mtxElts + 1 ]; // Add one so ReadFloatArrayString below will try and fail to read an extra element, instead of filling the buffer and doing another read
				ASSERT_ONLY(int count =) ReadFloatArrayString(temp, sizeof(temp) / sizeof(float));
				PARXML_ASSERT(count == mtxElts, "Found a matrix with the wrong number of elements (got %d, expected %d)", count, mtxElts);

				float* floats = reinterpret_cast<float*>(str);

				// "Transpose" from the row-major order we read these in into column major order.
				floats[0] = temp[0];	floats[4] = temp[1];	floats[8] = temp[2];		floats[12] = temp[3];
				floats[1] = temp[4];	floats[5] = temp[5];	floats[9] = temp[6];		floats[13] = temp[7];
				floats[2] = temp[8];	floats[6] = temp[9];	floats[10] = temp[10];		floats[14] = temp[11];
				floats[3] = 0.0f;		floats[7] = 0.0f;		floats[11] = 0.0f;			floats[15] = 0.0f;
	
				m_DataIncomplete = false;

				return sizeof(float) * 16;
			}
		case MATRIX43:
			{
				const int mtxElts = 4*3;
				PARXML_ASSERT(size >= sizeof(float) * mtxElts, "Need a buffer that's at least %" SIZETFMT "d bytes to load a matrix", sizeof(float) * mtxElts);
				float temp[ mtxElts + 1 ];
				ASSERT_ONLY(int count =) ReadFloatArrayString(temp, sizeof(temp) / sizeof(float));
				PARXML_ASSERT(count == mtxElts, "Found a matrix with the wrong number of elements (got %d, expected %d)", count, mtxElts);

				float* floats = reinterpret_cast<float*>(str);

				// "Transpose" from the row-major order we read these in into column major order.
				floats[0] = temp[0];	floats[4] = temp[1];	floats[8] = temp[2];		
				floats[1] = temp[3];	floats[5] = temp[4];	floats[9] = temp[5];		
				floats[2] = temp[6];	floats[6] = temp[7];	floats[10] = temp[8];		
				floats[3] = temp[9];	floats[7] = temp[10];	floats[11] = temp[11];			

				m_DataIncomplete = false;

				return sizeof(float) * 12;
			}
		case MATRIX44:
			{
				const int mtxElts = 4*4;
				PARXML_ASSERT(size >= sizeof(float) * mtxElts, "Need a buffer that's at least %" SIZETFMT "d bytes to load a matrix", sizeof(float) * mtxElts);
				float temp[ mtxElts + 1 ];
				ASSERT_ONLY(int count =) ReadFloatArrayString(temp, sizeof(temp) / sizeof(float));
				PARXML_ASSERT(count == mtxElts, "Found a matrix with the wrong number of elements (got %d, expected %d)", count, mtxElts);

				float* floats = reinterpret_cast<float*>(str);

				// "Transpose" from the row-major order we read these in into column major order.
				floats[0] = temp[0];	floats[4] = temp[1];	floats[8] = temp[2];		floats[12] = temp[3];
				floats[1] = temp[4];	floats[5] = temp[5];	floats[9] = temp[6];		floats[13] = temp[7];
				floats[2] = temp[8];	floats[6] = temp[9];	floats[10] = temp[10];		floats[14] = temp[11];
				floats[3] = temp[12];	floats[7] = temp[13];	floats[11] = temp[14];		floats[15] = temp[15];

				m_DataIncomplete = false;

				return sizeof(float) * 16;
			}

	}
	return -1;
}

bool parStreamInXml::ReadElementInternal(parElement& outElement)
{
	outElement.Reset();

	if (Unlikely(Peek("</", false))) 
		return true;

	bool match;
	match = Match("S<");
	if (!match)
	{
		m_ParseError = true;
		return true;
	}
	char elemName[MAX_ELEMENT_NAME_LENGTH];
	ReadToken(elemName);
	PARXML_ASSERT(IsXmlNameStartCharacter(elemName[0]), "Bad name for an XML element: %s", elemName);

	u32 elemNameHash = sm_MiniNameTable.Hash(elemName);
	const char** tableElemName = sm_MiniNameTable.SafeGet(elemNameHash);

	outElement.SetName(tableElemName ? *tableElemName : elemName, !tableElemName);
	SkipWhitespace();

	if (elemNameHash == 0x671ed6fb) // PAR_STRINGKEY(xi:include)
	{
#if __ASSERT
		const char* testStr = "xi:include";
#endif
		PARXML_ASSERT(!strcmp(elemName, testStr), "Hash collision. %d should be %s, was %s", elemNameHash, testStr, elemName);
		m_HasXInclude = true;
	}

	m_Base64Buffer = 0;
	m_Base64BufferSize = 0;
	m_LastEncoding = UNSPECIFIED;

	// read attribute list;
	while (!Peek('>', false) && !Peek("/>", false)) {
		char attrName[MAX_ELEMENT_NAME_LENGTH];

		if (Unlikely(ReadToken(attrName) == 0))
		{
			PARXML_ASSERT(0, "Couldn't read attribute name for element %s", elemName);
			m_ParseError = true;
			return true;
		}

		match = Match("s=s");
		if (!match)
		{
			m_ParseError = true;
			return true;
		}

		char attrValue[MAX_ATTRIBUTE_VALUE_LENGTH];
#if __ASSERT
		int attrValueLen = 
#endif
			ReadAttributeValue(attrValue);
		if (m_ParseError)
		{
			return true;
		}

		PARXML_ASSERT(outElement.FindAttribute(attrName) == NULL, "In element \"%s\", duplicate attribute \"%s\"", elemName, attrName);

		u32 attrNameHash = sm_MiniNameTable.Hash(attrName);

		const char* testStr = NULL;

		bool processed = false;

		if (Likely(GetSettings().GetFlag(parSettings::PROCESS_SPECIAL_ATTRS)))
		{
			processed = true; // Assume we find one of our special attributes
			// Magic numbers computed here with scrhasher... 
			// TODO: Use PAR_STRINGKEY for these, but can't now because of parser / parsercore dependencies
			switch(attrNameHash)
			{
			case 0x9303a5e5: // PAR_STRINGKEY(x):
				testStr = "x";
				PARXML_ASSERT(!strcmp(attrName, testStr), "Hash collision. %d should be %s, was %s", attrNameHash, testStr, attrName);
				if (attrValue[0] == 't' || attrValue[0] == 'f' || attrValue[0] == 'T' || attrValue[0] == 'F')
				{
					// TODO: make sure its really a bool value
					outElement.AddAttribute(testStr, parUtils::StringToBool(attrValue), false);
				}
				else
				{
					outElement.AddAttribute(testStr, parUtils::StringToFloat(attrValue), false);
				}
				break;
			case 0x80950108: // PAR_STRINGKEY(y):
				testStr = "y";
				PARXML_ASSERT(!strcmp(attrName, testStr), "Hash collision. %d should be %s, was %s", attrNameHash, testStr, attrName);
				if (attrValue[0] == 't' || attrValue[0] == 'f' || attrValue[0] == 'T' || attrValue[0] == 'F')
				{
					// TODO: make sure its really a bool value
					outElement.AddAttribute(testStr, parUtils::StringToBool(attrValue), false);
				}
				else
				{
					outElement.AddAttribute(testStr, parUtils::StringToFloat(attrValue), false);
				}
				break;
			case 0xb6606c9e: //PAR_STRINGKEY(z):
				testStr = "z";
				PARXML_ASSERT(!strcmp(attrName, testStr), "Hash collision. %d should be %s, was %s", attrNameHash, testStr, attrName);
				if (attrValue[0] == 't' || attrValue[0] == 'f' || attrValue[0] == 'T' || attrValue[0] == 'F')
				{
					// TODO: make sure its really a bool value
					outElement.AddAttribute(testStr, parUtils::StringToBool(attrValue), false);
				}
				else
				{
					outElement.AddAttribute(testStr, parUtils::StringToFloat(attrValue), false);
				}
				break;
			case 0x58a0b120: // PAR_STRINGKEY(w):
				testStr = "w";
				PARXML_ASSERT(!strcmp(attrName, testStr), "Hash collision. %d should be %s, was %s", attrNameHash, testStr, attrName);
				if (attrValue[0] == 't' || attrValue[0] == 'f' || attrValue[0] == 'T' || attrValue[0] == 'F')
				{
					// TODO: make sure its really a bool value
					outElement.AddAttribute(testStr, parUtils::StringToBool(attrValue), false);
				}
				else
				{
					outElement.AddAttribute(testStr, parUtils::StringToFloat(attrValue), false);
				}
				break;
			case 0x1e720953: // PAR_STRINGKEY(value):
				// Value is kind of a pain in the ass, but here's the rule we'll use... This should work ok:
				// if it starts with 't', 'T', 'f', or 'F' its a bool
				// if it contains a '.' its a float
				// else its an int
				testStr = "value";
				PARXML_ASSERT(!strcmp(attrName, testStr), "Hash collision. %d should be %s, was %s", attrNameHash, testStr, attrName);
				PARXML_ASSERT(attrValueLen > 0, "Attribute %s needs data", testStr);
				if (attrValue[0] == 't' || attrValue[0] == 'f' || attrValue[0] == 'T' || attrValue[0] == 'F')
				{
					// TODO: make sure its really a bool value
					outElement.AddAttribute(testStr, parUtils::StringToBool(attrValue), false);
				}
				else if (strchr(attrValue, '.'))
				{
					// TODO: make sure its really a float value
					outElement.AddAttribute(testStr, parUtils::StringToDouble(attrValue), false);
				}
				else if (attrValue[0] == 'I' && strcmp(attrValue, "Infinity") == 0)
				{
					outElement.AddAttribute(testStr, GetInf(), false);
				}
				else if (attrValue[0] == '-' && strcmp(attrValue, "-Infinity") == 0)
				{
					outElement.AddAttribute(testStr, GetNegInf(), false);
				}
				else if (attrValue[0] == 'N' && strcmp(attrValue, "NaN") == 0)
				{
					outElement.AddAttribute(testStr, GetNaN(), false);
				}
				else {
					// TODO: make sure its really an int value
					outElement.AddAttribute(testStr, parUtils::StringToInt64(attrValue), false);
				}
				break;
			case 0x753db284: // PAR_STRINGKEY(content)
				testStr = "content";
				PARXML_ASSERT(!strcmp(attrName, testStr), "Hash collision. %d should be %s, was %s", attrNameHash, testStr, attrName);
				m_LastEncoding = FindEncoding(attrValue);
				m_ExpectingData = true;
				processed = false; // We did find a special attribute, but need to add it to the attribute list as a string all the same
			default:
				processed = false;
			}
		}

		if (Unlikely(!processed))
		{
			// look for attr name, value in name table. Use them if they're there, otherwise make copies.
			const char** tableName = sm_MiniNameTable.SafeGet(attrNameHash);

			const char** tableValue = sm_MiniNameTable.SafeGet(attrValue);

			outElement.AddAttribute(
				tableName ? *tableName : attrName,
				tableValue ? *tableValue : attrValue,
				!tableName,
				!tableValue);
		}

		SkipWhitespace();
	}

	if (Unlikely(!GetSettings().GetFlag(parSettings::NEVER_SORT_ATTR_LIST) && outElement.GetAttributes().GetCount() > 4)) {
		outElement.SortAttributes();
	}

	if (Peek('>')) {
		m_UnclosedTagDepth++;
		if (m_UnclosedTagHashes.GetCount() < m_UnclosedTagHashes.GetMaxCount())
		{
			m_UnclosedTagHashes.Push(atLiteralStringHash(outElement.GetName()));
		}
		return false;
	}
	else if (Peek("/>")) {
		// don't change m_UnclosedTagHashes, we know this one was closed 
		return true;
	}
	else {
		PARXML_ASSERT(0, "Error: Tag %s isn't closed properly", outElement.GetName());
		m_ParseError = true;
	}
	return false;
}

void parStreamInXml::ReadBeginElement(parElement& outElement)
{
	FastAssert(m_Stream);
	//Assert(!m_ReadUnendedLeaf && !m_JustReadData); 

	// Could use some other way of testing whether or not we need to call this...
	if (m_LineNumber == 0)
	{
		SkipHeader();
#if !__DEV
		m_LineNumber = -1; // Set this to something other than 0, so that subsequent calls won't call SkipHeader() (m_LineNumber only increments in __DEV builds)
#endif
	}

	bool leaf = ReadElementInternal(outElement); // sets m_LastEncoding

	m_ReadUnendedLeaf = leaf; // we've read a <foo/> and need to call EndElement before calling Begin again
	m_JustBeganElement = true;
	m_JustReadData = false;
	m_DataIncomplete = false;
	m_ReadingCdata = false;
}

void parStreamInXml::ReadLeafElement(parElement& outElement)
{
	FastAssert(m_Stream);
	PARXML_ASSERT(!m_ReadUnendedLeaf, "Just read a leaf node with ReadBeginElement and haven't ended it yet");

	bool leaf = ReadElementInternal(outElement);

	if (!leaf) { 
		// the end tag wasn't built in (we called ReadLeafElement on <foo></foo>). 
		// scan ahead to find it. Assert on anything that's not the end tag.
		Match("S</sws>");
	}

	m_ReadUnendedLeaf = false;
	m_JustBeganElement = false;
	m_JustReadData = false;
	m_DataIncomplete = false;
	m_ReadingCdata = false;
	m_LastEncoding = UNSPECIFIED;
	m_ExpectingData = false;
}

void parStreamInXml::ReadEndElement(char* endTagBuffer)
{
	FastAssert(m_Stream);
	if (!m_ReadUnendedLeaf)
	{
		// read the end element tag
		Match("S</sws>", endTagBuffer);
#if __ASSERT 
		if (m_UnclosedTagDepth <= m_UnclosedTagHashes.GetMaxCount())
		{
			u32 endTagHash = atLiteralStringHash(endTagBuffer);
			PARXML_ASSERT(m_UnclosedTagHashes.Top() == endTagHash, "Mismatched end tag. Found </%s> (hash %d) and was expecting hash %d", endTagBuffer, endTagHash, m_UnclosedTagHashes.Top());
			m_UnclosedTagHashes.Pop();
		}
		m_UnclosedTagDepth--;
#endif
	}

	m_ReadUnendedLeaf = false;
	m_JustReadData = false;
	m_JustBeganElement = false;
	m_DataIncomplete = false;
	m_ReadingCdata = false;
	m_LastEncoding = UNSPECIFIED;
	m_ExpectingData = false;
}

void parStreamInXml::ReadEndElement()
{
	FastAssert(m_Stream);
	char buf[MAX_ELEMENT_NAME_LENGTH];
	ReadEndElement(buf);
}

int parStreamInXml::ReadData(char* data, int maxSize, DataEncoding enc)
{
	FastAssert(m_Stream);
	//Assert(!m_ReadUnendedLeaf && (m_JustBeganElement || (m_JustReadData && m_DataIncomplete)));
	if( m_ReadUnendedLeaf || !(m_JustBeganElement || (m_JustReadData && m_DataIncomplete)) )
		return 0;

	// read as much data as will fit. stop when hitting the next < tag.
	int size = ReadDataInternal(data, maxSize, enc != UNSPECIFIED ? enc : m_LastEncoding); // sets m_DataIncomplete

	m_ReadUnendedLeaf = false;
	m_JustReadData = true;
	m_JustBeganElement = false;
	m_ExpectingData = false;

	return size;
}

void parStreamInXml::SkipElement()
{
	// while nesting count > 0
	// scan forward to next <
	// if next char is /, decrement nesting count
	// else scan past the element, if element ends in />, don't change nesting count
	// else increment nesting count
	m_JustReadData = false;
	m_JustBeganElement = false;
	m_DataIncomplete = false;
	m_ReadingCdata = false;
	m_LastEncoding = UNSPECIFIED;
	m_ExpectingData = false;

	// We called ReadBegin on a leaf node. So SkipElement is a no-op.
	if (m_ReadUnendedLeaf)
	{
		m_ReadUnendedLeaf = false;
		return;
	}

	int nestCount = 1;

	while(nestCount > 0)
	{
		SkipTo("<");
		if (Peek('/')) {
			nestCount--;
			SkipTo(">");
		}
		else if (Peek("![CDATA["))
		{
			SkipTo("]]>");
		}
		else {
			int prevChar = ' ';
			int currChar = ' ';
			while(currChar != '>') {
				prevChar = currChar;
				currChar = ReadRawCharacter();
			}
			if (prevChar == '/') {
				// leaf node
			}
			else
			{
				nestCount++;
			}
		}
	}
}

/* Push mode
Create 1K push back buffer
while (!EOF):
	scan through whitespace
	if char is <
		if next char is /
			read the close tag
			EndElementCB(false)
		else:
			push back <
			leaf = ReadElementInternal
			if leaf
				BeginElementCB(true)
				EndElementCB(true)
			else
				BeginElementCB(false)
				justbeganchild = true
	else:
		assert(justbeganchild)
		push back char
		push back whitespace
		while (char is not <):
			add char to buffer
			when buffer is full, call DataCB(true)
		call DataCB(false)
*/

void parStreamInXml::FindUtfEncodingFromHeader()
{
	m_FileEncoding = ENC_UTF8; // make sure we just read one char at a time to read the first 4 bytes
	int header[4];
	header[0] = ReadRawCharacter();
	header[1] = ReadRawCharacter();
	header[2] = ReadRawCharacter();
	header[3] = ReadRawCharacter();

	// First check for unicode byte order marks, they make it easy.
	if (
		header[0] == 0xEF &&
		header[1] == 0xBB &&
		header[2] == 0xBF)
	{
		m_FileEncoding = ENC_UTF8;
		PushBack((char)header[3]);
	}
	else if (
		header[0] == 0xFE &&
		header[1] == 0xFF)
	{
		m_FileEncoding = ENC_UTF16_BE;
		PushBack((char)header[3]);
		PushBack((char)header[2]);
	}
	else if (
		header[0] == 0xFF &&
		header[1] == 0xFE)
	{
		m_FileEncoding = ENC_UTF16_LE;
		PushBack((char)header[3]);
		PushBack((char)header[2]);
	}

	// OK that didn't work, lets check the first character. In a well 
	// formed XML document it has to be '<' or ascii whitespace (0x20, 0x9, 0xD, 0xA)
	// so just check if we have 00xx, xx00, or xxxx
	else if (header[0] == 0x0 &&
		header[1] != 0x0)
	{
		m_FileEncoding = ENC_UTF16_BE;
		PushBack((char)header[3]);
		PushBack((char)header[2]);
		PushBack((char)header[1]);
		PushBack((char)header[0]);
	}
	else if (header[0] != 0x0 &&
		header[1] == 0x0)
	{
		m_FileEncoding = ENC_UTF16_LE;
		PushBack((char)header[3]);
		PushBack((char)header[2]);
		PushBack((char)header[1]);
		PushBack((char)header[0]);
	}
	else
	{
		m_FileEncoding = ENC_UTF8;
		PushBack((char)header[3]);
		PushBack((char)header[2]);
		PushBack((char)header[1]);
		PushBack((char)header[0]);
	}

	if (m_FileEncoding == ENC_UTF16_LE || m_FileEncoding == ENC_UTF16_BE)
	{
		// Must be utf16, make sure we decode the rest of the pushback buffer into utf8
		int numPushbackChars = m_PushBackBuffer.GetCount();


		if (numPushbackChars == 2)
		{
			char16 utf16Char;
			char* utf16Bytes = (char*)(&utf16Char);
			char utf8Bytes[3];
			utf16Bytes[0] = m_PushBackBuffer.Pop();
			utf16Bytes[1] = m_PushBackBuffer.Pop();
			utf16Char = (m_FileEncoding == ENC_UTF16_BE) ? sysEndian::BtoN(utf16Char) : sysEndian::LtoN(utf16Char);
			int numChars = WideToUtf8Char(utf16Char, utf8Bytes);
			PushBack(utf8Bytes, numChars);
		}
		else if (numPushbackChars == 4)
		{
			char16 utf16Char1, utf16Char2;
			char* utf16Bytes1 = (char*)(&utf16Char1);
			char* utf16Bytes2 = (char*)(&utf16Char2);
			utf16Bytes1[0] = m_PushBackBuffer.Pop();
			utf16Bytes1[1] = m_PushBackBuffer.Pop();
			utf16Bytes2[0] = m_PushBackBuffer.Pop();
			utf16Bytes2[1] = m_PushBackBuffer.Pop();

			utf16Char1 = (m_FileEncoding == ENC_UTF16_BE) ? sysEndian::BtoN(utf16Char1) : sysEndian::LtoN(utf16Char1);
			utf16Char2 = (m_FileEncoding == ENC_UTF16_BE) ? sysEndian::BtoN(utf16Char2) : sysEndian::LtoN(utf16Char2);

			char utf8Bytes[3];
			int numChars;

			numChars = WideToUtf8Char(utf16Char2, utf8Bytes);
			PushBack(utf8Bytes, numChars);
			numChars = WideToUtf8Char(utf16Char1, utf8Bytes);
			PushBack(utf8Bytes, numChars);
		}
		else
		{
			parErrorf("Invalid number of characters in the pushback buffer (%d). Should be 2 or 4 bytes (1 or 2 UTF-16 chars)", numPushbackChars);
		}
	}
}

void parStreamInXml::SkipHeader()
{
	FindUtfEncodingFromHeader();

	SkipWhitespaceAndComments();
}

#if __DEV
void PrintStreamToFile(fiStream *pStream)
{
	char wsBuffer[1024] = {0};
	pStream->Seek(0);
	char filePath[RAGE_MAX_PATH];
	strcpy(filePath, "common:/XML_error.xml");
	ASSET.CreateLeadingPath(filePath);
	fiStream* XMLFile = ASSET.Create(filePath, "");
	while (pStream->Read(wsBuffer, 1023))
	{
		if(XMLFile != NULL)
		{
			fprintf(XMLFile, "%s", wsBuffer);
		}
		memset(wsBuffer, 0, sizeof(wsBuffer));
	}

	if(XMLFile != NULL)
	{
		XMLFile->Close();
	}
}
#endif

bool parStreamInXml::ReadWithCallbacksInternal()
{
	parAssertf(m_Stream, "No stream to read from");
	char wsBuffer[1024];
	int wsBufLen = 0;

	SkipHeader();

	// Should this be alloca'ed instead?
	//	char* dataBuf = rage_new char[m_MaxDataSize];
	char* dataBuf = (char*)alloca(sizeof(char) * m_DataChunkSize);
	int depth = 0;

	bool eof = false;
	while(!eof) {
		wsBufLen = SkipWhitespaceAndComments(wsBuffer, 1024);
		if (m_ExpectingData)
		{
			// peek ahead and see if the next chars form an end tag. if so, push the whitespace back
			// so we can read it as data.
			if (Peek("</",false)) {
				PushBack(wsBuffer, wsBufLen-1);
				wsBufLen = 0;
			}
		}

		int c = ReadRawCharacter();
		if (c == -1 || c == 0) {
			if (depth > 0)
			{
				parErrorf("Found %d unclosed tags before the EOF", depth);
			}
			else
			{
				parErrorf("Hit an EOF before reading any tags - was the file empty?");
			}
			return false;
		}
		else if (c == '<' && !Peek("![CDATA[", false)) {
			wsBufLen = 0; // clear the whitespace buffer
			int nextC = ReadRawCharacter();
			if (nextC == '/') {
				if (!Match("sws>", wsBuffer)) // not skipping whitespace here because we don't want to consume past eof
				{
					return false;
				}
				if (depth > 0 && m_UnclosedTagDepth <= m_UnclosedTagHashes.GetMaxCount())
				{
					u32 endTagHash = atLiteralStringHash(wsBuffer);
					if (m_UnclosedTagHashes.Top() != endTagHash)
					{
						#if __DEV
						PrintStreamToFile(m_Stream);
						#endif
						Errorf("XML file format error in file %s on line %d\n",  m_Stream->GetName(), m_LineNumber+1);
						PARXML_ASSERT(m_UnclosedTagHashes.Top() == endTagHash, "Mismatched end tag. Found </%s> (hash %d) and was expecting hash %d", wsBuffer, endTagHash, m_UnclosedTagHashes.Top());
						return false;
					}
					m_UnclosedTagHashes.Pop();
				}
				m_UnclosedTagDepth--;
				--depth;
				if (depth < 0)
				{
					parErrorf("Found a close tag before an open tag");
					eof = true;
					return false;
				}
				else if (depth == 0)
				{
					eof = true;
				}
				else
				{
					SkipWhitespaceAndComments(); // Data can't follow an end tag.
				}
				m_EndElementCB(false);
				m_JustBeganElement = false;
				m_JustReadData = false;
				m_ExpectingData = false;
			}
			else {
				if (m_JustReadData)
				{
					#if __DEV
					PrintStreamToFile(m_Stream);
					#endif
					Errorf("XML file format error in file %s on line %d\n",  m_Stream->GetName(), m_LineNumber+1);
					PARXML_ASSERT(!m_JustReadData, "Found a start tag following data. Mixed content is not allowed");
					return false;
				}
				PushBack((char)nextC);
				PushBack((char)c);
				parElement elem;
				bool leaf = ReadElementInternal(elem);
				if (m_ParseError)
				{
					return false;
				}
				if (leaf)
				{
					if (depth == 0)
					{
						eof = true; // root element was a leaf
					}
					else
					{
						SkipWhitespaceAndComments(); // skipping whitespace here because data can't follow an end tag, and not filling the wsBuffer is a little more efficient.
					}
					m_BeginElementCB(elem, true);
					m_EndElementCB(true);
					m_JustBeganElement = false;
				}
				else
				{
					++depth;
					m_BeginElementCB(elem, false);
					m_JustBeganElement = true;
				}
				m_JustReadData = false;
			}
		}
		else
		{
			if (!m_JustBeganElement)
			{
				#if __DEV
				PrintStreamToFile(m_Stream);
				#endif
				Errorf("XML file format error in file %s on line %d\n",  m_Stream->GetName(), m_LineNumber+1);
				PARXML_ASSERT(m_JustBeganElement, "XML file format error");
				return false;
			}
			PushBack((char)c);
			if (wsBufLen > 0)
			{
				PushBack(wsBuffer, wsBufLen-1); // send len-1 because len includes the terminating 0
				wsBufLen = 0;
			}
			m_DataIncomplete = true;
			m_JustBeganElement = false;
			m_ExpectingData = false;
			while(m_DataIncomplete)
			{
				int len = ReadDataInternal(dataBuf, m_DataChunkSize, m_LastEncoding);
				if (m_ParseError)
				{
					return false;
				}
				m_DataCB(dataBuf, len, m_DataIncomplete);
			}
			m_JustReadData = true;
		}
	}
	return !m_ParseError;
}

bool parStreamInXml::ReadWithCallbacks()
{
	return ReadWithCallbacksInternal();
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////      parStreamOutXml       ////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

parStreamOutXml::parStreamOutXml(fiStream* s, const parSettings& settings)
: parStreamOut(s, settings)
, m_IndentLevel(0)
, m_JustWroteData(false)
, m_Base64Buffer(0)
, m_Base64BufferSize(0)
, m_NumDataChars(0)
, m_LastEncoding(UNSPECIFIED)
{
}

parStreamOutXml::~parStreamOutXml()
{

}

void parStreamOutXml::WriteEscapedCharacter(fiStream* stream, char c, bool attribute)
{
	switch(c) {
	case '<':
		stream->Write("&lt;", 4);
		break;
	case '>':
		stream->Write("&gt;", 4);
		break;
	case '&':
		stream->Write("&amp;", 5);
		break;
	case '\'':
		if (attribute) {
			stream->Write("&apos;", 6);
		}
		else
		{
			stream->FastPutCh(c);
		}
		break;
	case '\"':
		if (attribute) {
			stream->Write("&quot;", 6);
		}
		else
		{
			stream->FastPutCh(c);
		}
		break;
	default:
		stream->FastPutCh(c);
	}
}

void parStreamOutXml::WriteEscapedString(fiStream* stream, const char* str, int size, bool attribute)
{
	if (str == NULL) {
		return;
	}

	u32 fullSize;
	if (size >= 0) {
		fullSize = size;
	}
	else {
		fullSize = 0xFFFFFFFF;
	}
	for(u32 i = 0; i < fullSize && str[i] != '\0'; i++)
	{
		WriteEscapedCharacter(stream, str[i], attribute);
	}
}

void parStreamOutXml::WriteUtf16String(const char16* str, int size) const {
	u32 fullSize;
	if (size >= 0) {
		fullSize = size;
	}
	else {
		fullSize = 0xFFFFFFFF;
	}
	char buf[4];
	for(u32 i = 0; i < fullSize && str[i] != 0; i++)
	{
		int bytes = WideToUtf8Char(str[i], buf);
		if (bytes == 1) {
			WriteEscapedCharacter(m_Stream, buf[0], false);
		}
		else if (bytes == 2) {
			m_Stream->FastPutCh(buf[0]);
			m_Stream->FastPutCh(buf[1]);
		}
		else if (bytes == 3) {
			m_Stream->FastPutCh(buf[0]);
			m_Stream->FastPutCh(buf[1]);
			m_Stream->FastPutCh(buf[2]);
		}
	}
}

template<typename _Type>
void parStreamOutXml::WriteFloatArrayString(fiStream* stream, int indentLevel, const _Type* data, size_t size, WriteDataOptions* options, int skip /* = 0*/)
{
	bool highPrec = false;
	int valuesPerLine = 1;
	if (options)
	{
		highPrec = options->m_HighPrecision;
		valuesPerLine = options->m_ValuesPerLine;
	}
	stream->FastPutCh('\r');
	stream->FastPutCh('\n');
	IndentN(stream, indentLevel-1);
	const char* format = highPrec ? "%#.9g\t" : "%f\t";
	if (sizeof(_Type) == 8)
	{
		format = "%#.17g\t";
	}
	parAssertf(valuesPerLine >= 1, "Invalid number of values per line: %d", valuesPerLine);
	int i = 0;
	while(i < (int)size) {
		IndentN(stream, 1);
		for(int j = 0; j < valuesPerLine && i < (int)size; j++, i++)
		{
			fprintf(stream, format, data[i]);
		}
		i += skip; // skip this many elements at the end of each line

		stream->FastPutCh('\r');
		stream->FastPutCh('\n');
		IndentN(stream, indentLevel-1);
	}
}

template void parStreamOutXml::WriteFloatArrayString(fiStream* stream, int indentLevel, const float* data, size_t size, WriteDataOptions* options, int skip);
template void parStreamOutXml::WriteFloatArrayString(fiStream* stream, int indentLevel, const double* data, size_t size, WriteDataOptions* options, int skip);

void parStreamOutXml::WriteFloat16ArrayString(fiStream* stream, int indentLevel, const Float16* data, size_t size, WriteDataOptions* options, int skip /* = 0*/)
{
	int valuesPerLine = 1;
	if (options)
	{
		valuesPerLine = options->m_ValuesPerLine;
	}
	stream->FastPutCh('\r');
	stream->FastPutCh('\n');
	IndentN(stream, indentLevel-1);
	parAssertf(valuesPerLine >= 1, "Invalid number of values per line: %d", valuesPerLine);
	int i = 0;
	while(i < (int)size) {
		IndentN(stream, 1);
		for(int j = 0; j < valuesPerLine && i < (int)size; j++, i++)
		{
			fprintf(stream, "%f\t", data[i].GetFloat32_FromFloat16());
		}
		i += skip; // skip this many elements at the end of each line

		stream->FastPutCh('\r');
		stream->FastPutCh('\n');
		IndentN(stream, indentLevel-1);
	}
}

void parStreamOutXml::WriteVec2VArrayString(fiStream* stream, int indentLevel, const Vec2V* data, size_t size, WriteDataOptions* options)
{
	WriteDataOptions opts = options ? *options : WriteDataOptions();
	opts.m_ValuesPerLine = 2;
	WriteFloatArrayString(stream, indentLevel, reinterpret_cast<const float*>(data), size * (sizeof(Vec2V) / sizeof(float)), &opts, 2); // skip 'z', 'w'
}

void parStreamOutXml::WriteVector2ArrayString(fiStream* stream, int indentLevel, const Vector2* data, size_t size, WriteDataOptions* options)
{
	WriteDataOptions opts = options ? *options : WriteDataOptions();
	opts.m_ValuesPerLine = 2;
	WriteFloatArrayString(stream, indentLevel, reinterpret_cast<const float*>(data), size * (sizeof(Vector2) / sizeof(float)), &opts, 0);
}

void parStreamOutXml::WriteVector3ArrayString(fiStream* stream, int indentLevel, const Vector3* data, size_t size, WriteDataOptions* options)
{
	WriteDataOptions opts = options ? *options : WriteDataOptions();
	opts.m_ValuesPerLine = 3;
	WriteFloatArrayString(stream, indentLevel, reinterpret_cast<const float*>(data), size * (sizeof(Vector3) / sizeof(float)), &opts, 1); // skip 'w'
}

void parStreamOutXml::WriteVector4ArrayString(fiStream* stream, int indentLevel, const Vector4* data, size_t size, WriteDataOptions* options)
{
	WriteDataOptions opts = options ? *options : WriteDataOptions();
	opts.m_ValuesPerLine = 4;
	WriteFloatArrayString(stream, indentLevel, reinterpret_cast<const float*>(data), size * (sizeof(Vector4) / sizeof(float)), &opts, 0);
}


void parStreamOutXml::WriteBinaryString(const char* str, size_t size)
{
	if (!m_JustWroteData) {
		m_Stream->FastPutCh('\r');
		m_Stream->FastPutCh('\n');
	}

	int i = 0;
	while(i < (int)size)
	{
		m_Base64Buffer <<= 8;
		m_Base64Buffer |= (u8)str[i];
		m_Base64BufferSize++;
		i++;
		if (m_Base64BufferSize == 3) {
			m_Stream->FastPutCh(s_ToBase64LUT[((m_Base64Buffer >> 18) & 0x3F)]);
			m_Stream->FastPutCh(s_ToBase64LUT[((m_Base64Buffer >> 12) & 0x3F)]);
			m_Stream->FastPutCh(s_ToBase64LUT[((m_Base64Buffer >> 6) & 0x3F)]);
			m_Stream->FastPutCh(s_ToBase64LUT[((m_Base64Buffer >> 0) & 0x3F)]);

			m_Base64BufferSize = 0;
			m_Base64Buffer = 0;

			m_NumDataChars += 4;
			if (m_NumDataChars == 64) {
				m_Stream->FastPutCh('\r');
				m_Stream->FastPutCh('\n');
				m_NumDataChars = 0;
			}
		}
	}
}

void parStreamOutXml::WritePaddingBytes()
{
	switch(m_Base64BufferSize) {
		case 0:
			return;
		case 1:
			m_Base64Buffer <<= 16;
			m_Stream->FastPutCh(s_ToBase64LUT[(u8)((m_Base64Buffer >> 18) & 0x3F)]);
			m_Stream->FastPutCh(s_ToBase64LUT[(u8)((m_Base64Buffer >> 12) & 0x3F)]);
			m_Stream->FastPutCh('=');
			m_Stream->FastPutCh('=');
			break;
		case 2:
			m_Base64Buffer <<= 8;
			m_Stream->FastPutCh(s_ToBase64LUT[(u8)((m_Base64Buffer >> 18) & 0x3F)]);
			m_Stream->FastPutCh(s_ToBase64LUT[(u8)((m_Base64Buffer >> 12) & 0x3F)]);
			m_Stream->FastPutCh(s_ToBase64LUT[(u8)((m_Base64Buffer >> 6) & 0x3F)]);
			m_Stream->FastPutCh('=');
			break;
		default:
			parAssertf(0 , "Invalid case for number of buffered characters");
	}
	m_Stream->FastPutCh('\r');
	m_Stream->FastPutCh('\n');

	m_Base64Buffer = 0;
	m_Base64BufferSize = 0;
	m_NumDataChars = 0;
}

void parStreamOutXml::WriteHeader(parHeaderInfo& /*info*/)
{
	if (m_WroteHeader)
	{
		return;
	}
	FastAssert(m_Stream);
	if (GetSettings().GetFlag(parSettings::WRITE_UTF8_BOM))
	{
		m_Stream->PutCh(0xEF);
		m_Stream->PutCh(0xBB);
		m_Stream->PutCh(0xBF);
	}
	if (GetSettings().GetFlag(parSettings::WRITE_XML_HEADER))
	{
		fprintf(m_Stream, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
	}
	m_WroteHeader = true;
}


void parStreamOutXml::WriteComment(const char* comment)
{
	FastAssert(m_Stream);
	parAssertf(!m_JustWroteData, "Can't put a comment in a data block");
	fprintf(m_Stream, "\r\n<!-- %s -->\r\n", comment);
}

void parStreamOutXml::IndentN(fiStream* stream, int n)
{
	for(int i = 0; i < n; i++)
	{
		//stream->FastPutCh('\t');
		stream->FastPutCh(' ');
		stream->FastPutCh(' ');
	}
}

void parStreamOutXml::WriteBeginElementInternal(parElement& elt, bool leaf)
{
	char buffer[64];
	m_Stream->FastPutCh('\r');
	m_Stream->FastPutCh('\n');
	IndentN(m_Stream, m_IndentLevel);
	fprintf(m_Stream, "<%s", elt.GetName());

	m_LastEncoding = UNSPECIFIED;

	bool attrPerLine = elt.GetAttributes().GetCount() > GetSettings().GetMaxSingleLineAttributes();

	for(int i = 0; i < elt.GetAttributes().GetCount(); i++) 
	{
		parAttribute& attr = elt.GetAttributes()[i];
		if (attrPerLine)
		{
			m_Stream->FastPutCh('\r');
			m_Stream->FastPutCh('\n');
			IndentN(m_Stream, m_IndentLevel + 1);
			fprintf(m_Stream, "%s=\"", attr.GetName());
		}
		else 
		{
			fprintf(m_Stream, " %s=\"", attr.GetName());
		}

		WriteEscapedString(m_Stream, attr.GetStringRepr(buffer, 64), -1, true);
		fprintf(m_Stream, "\"");

		if (!strcmp(attr.GetName(), "content"))
		{
			m_LastEncoding = FindEncoding(attr.GetStringValue());
		}
	}

	if (attrPerLine && elt.GetAttributes().GetCount() > 0) {
		m_Stream->FastPutCh('\r');
		m_Stream->FastPutCh('\n');
		IndentN(m_Stream, m_IndentLevel);
	}

	if (leaf)
	{
		fprintf(m_Stream, " />");
	}
	else
	{
		m_Stream->FastPutCh('>');
		m_IndentLevel++;
	}

	m_JustWroteData = false;
	m_Base64Buffer = 0;
	m_Base64BufferSize = 0;
	m_NumDataChars = 0;
}

void parStreamOutXml::WriteBeginElement(parElement& elt)
{
	FastAssert(m_Stream);
	WriteBeginElementInternal(elt, false);
}

void parStreamOutXml::WriteEndElement(parElement& elt)
{
	FastAssert(m_Stream);
	m_IndentLevel--;
	if (m_JustWroteData)
	{
		if (m_LastEncoding == BINARY)
		{
			WritePaddingBytes();
		}
	}
	else
	{
		m_Stream->FastPutCh('\r');
		m_Stream->FastPutCh('\n');
		IndentN(m_Stream, m_IndentLevel);
	}

	fprintf(m_Stream, "</%s>", elt.GetName());

	m_JustWroteData = false;
	m_LastEncoding = UNSPECIFIED;
}

void parStreamOutXml::WriteLeafElement(parElement& elt)
{
	FastAssert(m_Stream);
	WriteBeginElementInternal(elt, true);
	m_LastEncoding = UNSPECIFIED;
}


template<typename _Type>
void parStreamOutXml::WriteIntArrayString(fiStream* stream, int indentLevel, const _Type* data, size_t size, WriteDataOptions* options /* = NULL */)
{
	const char* format = "%d ";

	if(sizeof(_Type) == 8)
	{
		format = "%" I64FMT "d";
	}

	int valuesPerLine = 1;
	if (options)
	{
		valuesPerLine = options->m_ValuesPerLine;
		if (options->m_Unsigned)
		{
			parAssertf(!std::numeric_limits<_Type>::is_signed, "Can't call WriteIntArrayString with the unsigned output option and a signed input type");
			format = "%u ";
			if (sizeof(_Type) == 8)
			{
				format = "%" I64FMT "u ";
			}
		}
		if (options->m_Hexadecimal)
		{
			if (sizeof(_Type) == 1)
			{
				format = "0x%02X ";
			}
			else if (sizeof(_Type) == 2)
			{
				format = "0x%04X ";
			}
			else if (sizeof(_Type) == 4)
			{
				format = "0x%08X ";
			}
			else if (sizeof(_Type) == 8)
			{
				format = "0x%016" I64FMT "X ";
			}
			else 
			{
				format = "0x%X ";
			}
		}
	}

	parAssertf(valuesPerLine >= 1, "Bad number of values per line: %d", valuesPerLine);

	stream->FastPutCh('\r');
	stream->FastPutCh('\n');
	IndentN(stream, indentLevel-1);
	size_t i = 0;
	while(i < size)
	{
		IndentN(stream, 1);
		for(int j = 0; j < valuesPerLine && i < size; j++, i++)
		{
			fprintf(stream, format, data[i]);	
		}
		stream->FastPutCh('\r');
		stream->FastPutCh('\n');
		IndentN(stream, indentLevel-1);
	}
}

// Explicit template instantiation for the WriteIntArrayString:
template void parStreamOutXml::WriteIntArrayString(fiStream* stream, int indentLevel, const int* data, size_t size, WriteDataOptions* options /* = NULL */);
template void parStreamOutXml::WriteIntArrayString(fiStream* stream, int indentLevel, const unsigned int* data, size_t size, WriteDataOptions* options /* = NULL */);
template void parStreamOutXml::WriteIntArrayString(fiStream* stream, int indentLevel, const short* data, size_t size, WriteDataOptions* options /* = NULL */);
template void parStreamOutXml::WriteIntArrayString(fiStream* stream, int indentLevel, const unsigned short* data, size_t size, WriteDataOptions* options /* = NULL */);
template void parStreamOutXml::WriteIntArrayString(fiStream* stream, int indentLevel, const char* data, size_t size, WriteDataOptions* options /* = NULL */);
template void parStreamOutXml::WriteIntArrayString(fiStream* stream, int indentLevel, const unsigned char* data, size_t size, WriteDataOptions* options /* = NULL */);

void parStreamOutXml::WriteData(const char* data, int size, bool /*completeData*/, DataEncoding enc, WriteDataOptions* options /* = NULL */)
{
	FastAssert(m_Stream);

	if (!data) {
		m_JustWroteData = true;
		return;
	}

	if (enc == UNSPECIFIED) {
		enc = m_LastEncoding;
	}

	if (Unlikely(!GetSettings().GetFlag(parSettings::PROCESS_SPECIAL_ATTRS))) // Ignore any content= attributes, which means all the data gets treated as ascii
	{
		enc = UNSPECIFIED;
	}

	bool unsign = options ? options->m_Unsigned : false;

	switch(enc)
	{
	case UNSPECIFIED:
	case ASCII:
		WriteEscapedString(m_Stream, data, size, false);
		break;
	case UTF16:
		FastAssert(size < 0 || (size % sizeof(char16)) == 0);
		WriteUtf16String(reinterpret_cast<const char16*>(data), (size_t)size / sizeof(char16));
		break;
	case BINARY:
		WriteBinaryString(data, size);
		break;
	case RAW_XML:
		parAssertf(0 , "Writing raw xml data is currently unimplemented");
		break;
	case INT_ARRAY:
		FastAssert((size % sizeof(int)) == 0);
		if (Unlikely(unsign))
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const unsigned int*>(data), size / sizeof(int), options);
		}
		else
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const int*>(data), size / sizeof(int), options);
		}
		break;
	case CHAR_ARRAY:
		FastAssert((size % sizeof(char)) == 0);
		if (Unlikely(unsign))
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const unsigned char*>(data), size / sizeof(char), options);
		}
		else
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const char*>(data), size / sizeof(char), options);
		}
		break;
	case SHORT_ARRAY:
		FastAssert((size % sizeof(short)) == 0);
		if (Unlikely(unsign))
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const unsigned short*>(data), size / sizeof(short), options);
		}
		else
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const short*>(data), size / sizeof(short), options);
		}
		break;
	case INT64_ARRAY:
		FastAssert((size % sizeof(s64)) == 0);
		if (Unlikely(unsign))
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const u64*>(data), size / sizeof(u64), options);
		}
		else
		{
			WriteIntArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const s64*>(data), size / sizeof(s64), options);
		}
		break;
	case FLOAT_ARRAY:
		FastAssert((size % sizeof(float)) == 0);
		WriteFloatArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const float*>(data), size / sizeof(float), options);
		break;
	case DOUBLE_ARRAY:
		FastAssert((size % sizeof(double)) == 0);
		WriteFloatArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const double*>(data), size / sizeof(double), options);
		break;
	case VECTOR2_ARRAY:
		FastAssert((size % sizeof(Vector2)) == 0);
		WriteVector2ArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const Vector2*>(data), size / sizeof(Vector2), options);
		break;
	case VEC2V_ARRAY:
		FastAssert((size % sizeof(Vec2V)) == 0);
		WriteVec2VArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const Vec2V*>(data), size / sizeof(Vec2V), options);
		break;
	case VECTOR3_ARRAY:
		FastAssert((size % sizeof(Vector3)) == 0);
		WriteVector3ArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const Vector3*>(data), size / sizeof(Vector3), options);
		break;
	case VECTOR4_ARRAY:
		FastAssert((size % sizeof(Vector4)) == 0);
		WriteVector4ArrayString(m_Stream, m_IndentLevel, reinterpret_cast<const Vector4*>(data), size / sizeof(Vector4), options);
		break;
	case MATRIX33:
		{
			FastAssert(size == sizeof(Mat33V));
			const int mtxElts = 3*3;
			float temp[mtxElts];
			const float* floats = reinterpret_cast<const float*>(data);

			// Transpose into row-major order for writing
			temp[0] = floats[0];		temp[1] = floats[4];		temp[2] = floats[8];
			temp[3] = floats[1];		temp[4] = floats[5];		temp[5] = floats[9];
			temp[6] = floats[2];		temp[7] = floats[6];		temp[8] = floats[10];
			// skip w floats[3];				  floats[7];				  floats[11];
			
			WriteDataOptions mtxOpts = options ? *options : WriteDataOptions();
			mtxOpts.m_ValuesPerLine = 3;

			WriteFloatArrayString(m_Stream, m_IndentLevel, temp, mtxElts, &mtxOpts);
		}
		break;
	case MATRIX34:
		{
			FastAssert(size == sizeof(Mat34V));
			const int mtxElts = 3*4;
			float temp[mtxElts];
			const float* floats = reinterpret_cast<const float*>(data);

			// Transpose into row-major order for writing
			temp[0] = floats[0];		temp[1] = floats[4];		temp[2] = floats[8];		temp[3] = floats[12];
			temp[4] = floats[1];		temp[5] = floats[5];		temp[6] = floats[9];		temp[7] = floats[13];
			temp[8] = floats[2];		temp[9] = floats[6];		temp[10] = floats[10];		temp[11] = floats[14];
			// skip w floats[3];				  floats[7];				  floats[11];				 floats[15];

			WriteDataOptions mtxOpts = options ? *options : WriteDataOptions();
			mtxOpts.m_ValuesPerLine = 4;

			WriteFloatArrayString(m_Stream, m_IndentLevel, temp, mtxElts, &mtxOpts);
		}
		break;
	case MATRIX43:
		{
			FastAssert(size == sizeof(Mat34V));
			const int mtxElts = 4*3;
			float temp[mtxElts];
			const float* floats = reinterpret_cast<const float*>(data);

			// Transpose into row-major order for writing
			temp[0] = floats[0];		temp[1] = floats[4];		temp[2] = floats[8];	
			temp[3] = floats[1];		temp[4] = floats[5];		temp[5] = floats[9];	
			temp[6] = floats[2];		temp[7] = floats[6];		temp[8] = floats[10];	
			temp[9] = floats[3];		temp[10] = floats[7];		temp[11] = floats[11];

			WriteDataOptions mtxOpts = options ? *options : WriteDataOptions();
			mtxOpts.m_ValuesPerLine = 3;

			WriteFloatArrayString(m_Stream, m_IndentLevel, temp, mtxElts, &mtxOpts);
		}
		break;
	case MATRIX44:
		{
			FastAssert(size == sizeof(Mat34V));
			const int mtxElts = 4*4;
			float temp[mtxElts];
			const float* floats = reinterpret_cast<const float*>(data);

			// Transpose into row-major order for writing
			temp[0] = floats[0];		temp[1] = floats[4];		temp[2] = floats[8];		temp[3] = floats[12];
			temp[4] = floats[1];		temp[5] = floats[5];		temp[6] = floats[9];		temp[7] = floats[13];
			temp[8] = floats[2];		temp[9] = floats[6];		temp[10] = floats[10];		temp[11] = floats[14];
			temp[12] = floats[3];		temp[13] = floats[7];		temp[14] = floats[11];		temp[15] = floats[15];

			WriteDataOptions mtxOpts = options ? *options : WriteDataOptions();
			mtxOpts.m_ValuesPerLine = 4;

			WriteFloatArrayString(m_Stream, m_IndentLevel, temp, mtxElts, &mtxOpts);
		}
		break;

	}
	m_JustWroteData = true;
}



