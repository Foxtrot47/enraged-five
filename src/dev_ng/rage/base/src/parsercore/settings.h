// 
// parser/settings.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_SETTINGS_H
#define PARSER_SETTINGS_H

#include "atl/bitset.h"

namespace rage {

// PURPOSE: A class that contains various settings to control the parser behavior.
class parSettings
{
public:
	parSettings();

	enum SettingsFlags
	{
		/* PURPOSE: If true, writes the XML header: <?xml version="1.0" encoding="UTF-8"?> */
		WRITE_XML_HEADER,

		/* PURPOSE: If true, for text output formats, write the UTF-8 Byte Order Mark (0xEF 0xBB 0xBF). 
		Technically not necessary but WordPad expects it. */
		WRITE_UTF8_BOM,

		/* PURPOSE: If false, the v=, content= and value= attributes will be treated
		as normal attributes. Note that this could cause errors if trying to
		load data created by the parser using parStructures. */
		PROCESS_SPECIAL_ATTRS,

		/* PURPOSE: If true, it will truncate attributes to MAX_ATTRIBUTE_VALUE_LENGTH-1 and not raise an error */
		TRUNCATE_ATTRIBUTES,

		/* PURPOSE: If true, we won't sort attribute lists. Sorting them after
		load can make accessing the attributes faster, but means that a load
		followed by a save can change the order they are output in */
		NEVER_SORT_ATTR_LIST,

		/* PURPOSE: Do we print out warnings whenever encountering an element that
		doesn't map to a member variable */
		WARN_ON_UNUSED_DATA,

		/* PURPOSE: Do we print out warnings whenever a member variable isn't specified
		in the loaded data */
		WARN_ON_MISSING_DATA,

		/* PURPOSE: Do we print out warnings whenever a fixed sized array doesn't have
		the correct number of items in the loaded data */
		WARN_ON_FIXED_SIZE_ARRAY_MISMATCH,

		/* PURPOSE: Do we print a warning if the minor version numbers of
		a structure don't match */
		WARN_ON_MINOR_VERSION_MISMATCH,

		/* PURPOSE: Do we allow arrays to be resized if they already exist when loading. */
		RESIZE_EXISTING_ARRAYS,

		/* PURPOSE: If loading data and a pointer to an allocated but incompatable type
		already exists in the target structure, do we delete and reallocate that pointer? */
		REALLOCATE_MISTYPED_POINTERS,

		/* PURPOSE: For any data not specified in the loaded file, do we initialize it or
		leave it alone? */
		INITIALIZE_MISSING_DATA,

		/* PURPOSE: When the parser is loading, should the parser initialize any data
		that it creates? */
		INITIALIZE_NEW_DATA,

		/* PURPOSE: Should we always delete and reallocate pointers and arrays? */
		ALWAYS_REALLOCATE,

		/* PURPOSE: Should we print a warning when a derived or contained type is registered
		before the base or container type? 
		   NOTES: This does not happen during autoregistration
		   */
		WARN_ON_OUT_OF_ORDER_REGISTRATION,

		/* PURPOSE: Should we assert when a derived or contained type is registered before the
		base or container type? 
		   NOTES: This does not happen during autoregistration
		 */
		ASSERT_ON_OUT_OF_ORDER_REGISTRATION,

		/* PURPOSE: Should we warn when we try to register an already-registered object? */
		WARN_ON_MULTIPLE_REGISTRATION,

		/* PURPOSE: When encountering an unknown type on load (at the top level of a file, or
		in a type= attribute), rather than asserting we should use the type of the object
		we're loading into or the pointer type, if no object exists yet. */
		LOAD_UNKNOWN_TYPES,

		/* PURPOSE: If your class has virtual functions, you should use PAR_PARSABLE, not
		PAR_SIMPLE_PARSABLE. This flag will print a warning if the class is using the wrong
		macro. 
		NOTES: This won't catch the case where you're using PAR_PARSABLE and could be using
		PAR_SIMPLE_PARSABLE. */
		WARN_ON_MISMATCHED_PAR_MACRO,

		/* PURPOSE: Use the temporary heap for parser allocations. It does NOT use the temp
		heap when allocating new parts of an object you're loading in.*/
		USE_TEMPORARY_HEAP,

		/* PURPOSE: Stack-allocate a small (1k) block for reading in binary and string data,
		instead of the normal 16k block. Could be important for running parser in memory-limited threads. */
		USE_SMALL_DATA_CHUNK_SIZE,

		/* PURPOSE: If true, preloads the entire XML file into memory before parsing it. */
		PRELOAD_FILE,

		/* PURPOSE: unused */
		READ_SAFE_BUT_SLOW,

		/* PURPOSE: If true, <xi:include> nodes are processed and resolved. This currently works for 
		whole-tree loading only */
		PROCESS_XINCLUDES,

		/* PURPOSE: If true, ensure that any name we're looking up in metadata will exist in all builds
		(not just in PARSER_ALL_METADATA_HAS_NAMES builds). */
		ENSURE_NAMES_IN_ALL_BUILDS,

		/* PURPOSE: If true, then platform data for other platforms will be removed from the loaded objects */
		CULL_OTHER_PLATFORM_DATA,

		/* PURPOSE: Removes default fields on save */
		WRITE_IGNORE_DEFAULTS,

		NUM_FLAGS
	};

	// PURPOSE: Returns one of the SettingsFlags values 
	bool GetFlag(int flag) const;

    // PURPOSE: Sets one of the SettingsFlags values to newVal
	// RETURNS: The previous value for the flag
	bool SetFlag(int flag, bool newVal);

	// PURPOSE: Returns the max number of attributes we can write on a single line
	int GetMaxSingleLineAttributes() const;

	// PURPOSE: Sets the max number of attributes we can write on a single line
	void SetMaxSingleLineAttributes(int num);

	// PURPOSE: Initializes the sm_StandardSettings and sm_StrictSettings objects
	static void CreateStandardSettings();

	// PURPOSE: The normal settings for the parser.
	// NOTES: This sets WRITE_XML_HEADER, PROCESS_SPECIAL_ATTRIBUTES, 
	//        ASSERT_ON_OUT_OF_ORDER_REGISTRATION, WARN_ON_MULTIPLE_REGISTRATION
	//		  and INITIALIZE_NEW_DATA
	static parSettings sm_StandardSettings;

	// PURPOSE: Stricter settings for the parser.
	// NOTES: Same as sm_StandardSettings, but with all other warnings turned on as well.
	static parSettings sm_StrictSettings;

	// PURPOSE: Settings for chunks of parsable data embedded within other files
	// NOTES: Same as standard settings, but doesn't write any XML header stuff
	static parSettings sm_EmbeddedSettings;


protected:
	/* PURPOSE: Fewer than this # of attributes and we print them all on one line */
	int m_MaxSingleLineAttributes;

	atFixedBitSet<NUM_FLAGS> m_Flags;

};

inline parSettings::parSettings()
{
	m_MaxSingleLineAttributes = 4;
	m_Flags.Set(WRITE_XML_HEADER);
	m_Flags.Set(PROCESS_SPECIAL_ATTRS);
	m_Flags.Set(PROCESS_XINCLUDES);
}


inline bool parSettings::GetFlag(int flag) const
{
	return m_Flags.IsSet(flag);	
}

inline bool parSettings::SetFlag(int flag, bool newVal)
{
	bool oldVal = m_Flags.IsSet(flag);
	m_Flags.Set(flag, newVal);
	return oldVal;
}

inline int parSettings::GetMaxSingleLineAttributes() const
{
	return m_MaxSingleLineAttributes;
}

inline void parSettings::SetMaxSingleLineAttributes(int num)
{
	m_MaxSingleLineAttributes = num;
}

} // namespace rage

#endif
