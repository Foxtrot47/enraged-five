// 
// parsercore/streamrbf.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSERCORE_STREAMRBF_H
#define PARSERCORE_STREAMRBF_H

#include "stream.h"

#include "atl/array.h"
#include "atl/map.h"

namespace rage {

class parStreamInRbf : public parStreamIn
{
public:
	parStreamInRbf(fiStream* s, const parSettings& settings);
	virtual ~parStreamInRbf();

	virtual void SetHeaderBytes(u32 bytes);

	virtual void		ReadBeginElement(parElement& outElement);
	virtual void		ReadLeafElement(parElement& outElement);

	// maxSize can't be too small. At least 4 chars.

	virtual int			ReadData(char* data, int maxSize, DataEncoding enc = UNSPECIFIED);
	virtual void		ReadEndElement(); 
	virtual void		SkipElement();

	virtual bool		IsDataReadIncomplete() {
		return m_DataRemaining > 0;
	}

	virtual bool		ReadWithCallbacks();

protected:
	void ReadFullRecordID(u16& recordID, u16& flags, u32& nameID, const char*& name, bool isAttribute);

	bool ReadElementInternal(parElement& elt, u16 recordFlags);
	void ReadAttributes(parElement& elt);

	void SkipSizedBlock();

	u16 GetNextRecordID(); // skips comments

	DataEncoding m_LastEncoding;
	parNameTable m_InternalNameTable; // used if m_NameTable is NULL
	u32 m_DataRemaining;
	bool m_JustReadLeaf;
	bool m_ReadingData;
	bool m_JustBeganElement;
};


class parStreamOutRbf : public parStreamOut
{
public:
	parStreamOutRbf(fiStream* s, const parSettings& settings);
	virtual ~parStreamOutRbf();

	virtual void		WriteHeader(parHeaderInfo&);
	virtual void		WriteComment(const char*);
	virtual void		WriteBeginElement(parElement&);
	virtual void		WriteEndElement(parElement&);
	virtual void		WriteLeafElement(parElement&);
	virtual void		WriteData(const char*, int size = -1, bool completeData = false, DataEncoding enc = UNSPECIFIED, WriteDataOptions* options = NULL);

protected:
	void WriteRecordID(u16 flags, const char* name);

	void WriteDataInternal(const char* data, int size, DataEncoding enc);

	void FinishData();

	DataEncoding m_LastEncoding;
	size_t m_DataSizeLocation;
	bool m_WritingData;
	u32 m_DataSize;
	atMap<ConstString, u32> m_NameMap;		// TODO: Consider a u32 hash here
};

} // namespace rage

#endif // PARSERCORE_STREAMBUF_H
