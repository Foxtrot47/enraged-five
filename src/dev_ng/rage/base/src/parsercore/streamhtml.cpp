// 
// parsercore/streamhtml.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "streamhtml.h"

#include "parser/optimisations.h"

PARSER_OPTIMISATIONS();

namespace rage
{

parStreamInHtml::parStreamInHtml(fiStream* s, const parSettings& settings)
	: parStreamInXml(s, settings)
{
}

parStreamInHtml::~parStreamInHtml()
{
}

bool parStreamInHtml::Match(const char* str, char* tagBuffer)
{
	for(int i = 0; str[i] != '\0'; i++) {
		if (str[i] == 's') {
			SkipWhitespace();
		}
		else if (str[i] == 'S') {
			SkipWhitespaceAndComments();
		}
		else if (str[i] == 'w') {
			ReadToken(tagBuffer);
		}
		else if ( !Peek(str[i], true) )
		{
			return false;
		}
	}
	return true;
}

bool parStreamInHtml::Match(const char* str)
{
	char buf[MAX_ELEMENT_NAME_LENGTH];
	return Match(str, buf);
}

int parStreamInHtml::Tell()
{
	return (m_Stream->Tell() - m_PushBackBuffer.GetCount());
}

void parStreamInHtml::Seek(int pos)
{
	// Clear pushback buffer
	while( m_PushBackBuffer.GetCount() )
		m_PushBackBuffer.Pop();

	// Seek to the file position
	m_Stream->Seek(pos);
}

void parStreamInHtml::ReadInternal(parHtmlElement& outElement)
{
	outElement.Reset();
	m_DataIncomplete = true;

	if (Peek("</", false)) 
	{
		m_DataIncomplete = false;
		return;
	}

	bool bMatch = Match("S<");
	if( !bMatch )
	{
		// This is raw text data
		int pos = Tell();
		
		char c;
		do
		{
			c = (char)ReadRawCharacter();
		} while( c != '<' );

		int textLength = Tell() - pos;
		Seek(pos);

		outElement.SetType(parHtmlElement::parHET_Text);

		char* textData = Alloca(char, textLength + 1);
		ReadDataInternal(textData, textLength, m_LastEncoding);
		textData[textLength] = 0;

		outElement.AddAttribute("text", textData);
	}
	else
	{
		char elemName[MAX_ELEMENT_NAME_LENGTH];
		ReadToken(elemName);
		outElement.SetName(elemName);
		SkipWhitespace();

		// read attribute list;
		while (!Peek('>', false) && !Peek("/>", false)) 
		{
			char attrName[MAX_ELEMENT_NAME_LENGTH];
			ReadToken(attrName);

			Match("s=s");

			char attrValue[MAX_ATTRIBUTE_VALUE_LENGTH];
			ReadAttributeValue(attrValue);

			outElement.AddAttribute(attrName, attrValue);

			SkipWhitespace();
		}

		if (Peek('>')) {
			m_DataIncomplete = true;
		}
		else if (Peek("/>")) {
			m_DataIncomplete = false;
		}
		else {
			parAssertf(0 , "Error: Tag isn't closed properly");
		}
	}
}

void parStreamInHtml::ReadBeginElement(parElement& outElement)
{
	if (GetSettings().GetFlag(parSettings::READ_SAFE_BUT_SLOW))
	{
		parWarningf("READ_SAFE_BUT_SLOW Doesn't work with HTML files");
	}

	ReadInternal(*(parHtmlElement*)&outElement);
}

void parStreamInHtml::ReadLeafElement(parElement& outElement)
{
	if (GetSettings().GetFlag(parSettings::READ_SAFE_BUT_SLOW))
	{
		parWarningf("READ_SAFE_BUT_SLOW Doesn't work with HTML files");
	}

	ReadBeginElement(outElement);
}

bool parStreamInHtml::IsAtEndTag()
{
	return Peek("</", false);
}

bool parStreamInHtml::IsAtEndOfStream()
{
	return !(m_Stream->Tell() < m_Stream->Size());
}


} // namespace rage
