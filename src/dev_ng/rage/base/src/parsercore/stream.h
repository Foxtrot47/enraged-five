// 
// parsercore/stream.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSERCORE_STREAM_H
#define PARSERCORE_STREAM_H

#include "settings.h"

#include "atl/array.h"
#include "atl/delegate.h"

namespace rage {

class fiStream;
class parElement;

class parHeaderInfo
{
public:
};

typedef atArray<char*> parNameTable;

// PURPOSE: A parStream is a wrapper for an fiStream that also contains some parser-specific utilities.
class parStream 
{
public:
	virtual ~parStream();

	// PURPOSE: An enum specifying what type of data is in a raw data block.
	enum DataEncoding {
		UNSPECIFIED,		// If unspecified, uses the parent node's "content" attribute if there was one
		ASCII,				// The data block contains ascii data
		UTF16,				// The data block contains UTF-16 data
		BINARY,				// The data block contains arbitrary binary data
		RAW_XML,			// The data block contains raw UTF-8 encoded XML data. The data must be proper XML.
		INT_ARRAY,			// The data is an array of ints. Byte swapping is done as necessary.
		CHAR_ARRAY,			// The data is an array of chars
		SHORT_ARRAY,		// The data is an array of shorts
		FLOAT_ARRAY,		// The data is an array of floats.
		VECTOR2_ARRAY,		// The data is an array of Vector2
		VEC2V_ARRAY,		// The data is an array of Vec2V (not the same as Vector2 - Vec2V has padding!)
		VECTOR3_ARRAY,		// The data is an array of Vector3 or Vec3V
		VECTOR4_ARRAY,		// The data is an array of Vector4 or Vec4V

		MATRIX33,			// The data is a 3x3 matrix in COLUMN MAJOR order INCLUDING 'w' components for each column. 
							// In 'spreadsheet' notation the data is stored:
							// [a1, a2, a3, (a4), b1, b2, b3, (b4), c1, c2, c3, (c4)]

		MATRIX34,			// The data is a 3x4 matrix (3 rows, 4 columns) in COLUMN MAJOR order INCLUDING 'w' components.
							// In 'spreadsheet' notation the data is stored:
							// [a1, a2, a3, (a4), b1, b2, b3, (b4), c1, c2, c3, (c4), d1, d2, d3, (d4)]

		MATRIX43,			// The data is a 4x3 matrix (4 rows, 3 columns) in COLUMN MAJOR order
							// In 'spreadsheet' notation the data is stored:
							// [a1, a2, a3, a4, b1, b2, b3, b4, c1, c2, c3, c4]

		MATRIX44,			// The data is a 4x4 matrix in COLUMN MAJOR order
							// In 'spreadsheet' notation the data is stored:
							// [a1, a2, a3, a4, b1, b2, b3, b4, c1, c2, c3, c4, d1, d2, d3, d4]
		INT64_ARRAY,		// The data is an array of int64s
		DOUBLE_ARRAY,		// The data is an array of doubles
	};

	enum {
		MAX_ELEMENT_NAME_LENGTH = 128,			// The maximum length of an element (and attribute) name
		MAX_ATTRIBUTE_VALUE_LENGTH = 1024,		// The maximum length for a string attribute's string value.
		NUM_ENCODINGS = (int)DOUBLE_ARRAY + 1,	// The number of encodings - not part of the enum above so we don't have to deal with it in switch stmts
	};

	// PURPOSE: Given an encoding name, returns the enumeration corresponding to that name.
	static DataEncoding FindEncoding(const char* enc);

	// PURPOSE: Given an encoding, return the string name for that encoding
	static const char* FindEncodingName(DataEncoding enc);

	// PURPOSE: Closes the fiStream
	void				Close();

	fiStream*			GetFiStream() {return m_Stream;}

	const parSettings&	GetSettings() {return m_Settings;}

	void				SetSettings(const parSettings& settings);

protected:
	parStream(fiStream* stream, const parSettings& settings)
	: m_Stream(stream)
	, m_Settings(settings)
	{
	}

	fiStream*			m_Stream;
	parSettings			m_Settings; 
};

// PURPOSE: parStreamIn specifies an interface for reading with the parser. 
// It provides two styles of reading, a "push" interface where the user registers
// callbacks that get called during reading and the "pull" interface where the
// user knows the data format in advance and explicitly reads elements, child elements,
// data blocks, end element tokens, etc.
class parStreamIn : public parStream
{
public:
	// PURPOSE:
	// The BeginElementCB is a callback that gets called any time the reader reads
	// a new element. 
	// NOTES:
	// The first argument is the parElement that gets read. The memory used by this element
	// will be reclaimed by the reader, so the callee should make a copy if desired. The second
	// argument is true if the reader is certain this is a leaf element. It's still possible to read
	// a leaf element and have the second argument be false. Also, EndElementCB will still be
	// called after reading a leaf.
	typedef atDelegate<void (parElement&, bool)>		BeginElementCB;

	// PURPOSE:
	// The EndElementCB gets called whenever a reader reads an end-element token.
	// NOTES:
	// The first argument will be true if the reader was certain that a leaf node was just read.
	typedef atDelegate<void (bool)>					EndElementCB;

	// PURPOSE:
	// Reads a raw data block.
	// NOTES:
	// The reader reads at most the number of bytes set with SetDataChunkSize. For any 
	// given data block it can call the DataCB repeatedly until the entire block has been read.
	// The arguments are: a pointer to the read-in chunk, the number of bytes read, and whether
	// or not this was the last chunk in the block.
	// The reader will reclaim the memory pointed to by the first argument, so the callbacks
	// should make a copy if necessary.
	typedef atDelegate<void (char*, int, bool)>		DataCB;

	// If called before reading, it contains the first 4 bytes in the file. 
	// Some formats may ignore these bytes, some may need to push them into a push-back buffer
	virtual void		SetHeaderBytes(u32 bytes) = 0;

	// the "push" interface for reading.

	// PURPOSE: Read from the stream, calling the BeginElement, EndElement and Data callbacks.
	// RETURNS: false if there was a parse error - the data you read so far may or may not have been valid
	virtual bool		ReadWithCallbacks() = 0;

	// PURPOSE: Sets the callback that gets called for each new element that gets read.
	void				SetBeginElementCallback(BeginElementCB cb);

	// PURPOSE: Sets the callback that gets called for each end-element token.
	void				SetEndElementCallback(EndElementCB cb);

	// PURPOSE: Sets the callback that gets called for reading raw data.
	void				SetDataCallback(DataCB cb);

	// PURPOSE: Sets the largest amount of data to read at once.
	void				SetDataChunkSize(int size);

	// the "pull" interface for reading

	// PURPOSE: Reads an element into outElement
	virtual void		ReadBeginElement(parElement& outElement) = 0;

	// PURPOSE: Convenience function for elements with no children. Equivalent to ReadBeginElement(outElement); ReadEndElement();
	virtual void		ReadLeafElement(parElement& outElement) = 0;

	// PURPOSE: Reads a data block.
	// PARAMS:
	//		data - Where to store the data
	//		maxSize - The maximum amount to read, in bytes
	//		enc - What encoding to use
	// RETURNS: The number of bytes read.
	virtual int			ReadData(char* data, int maxSize, DataEncoding enc = UNSPECIFIED) = 0;

	// PURPOSE: Reads an end element tag.
	virtual void		ReadEndElement() = 0; 

	// PURPOSE: Skips forward past the matching end element tag.
	virtual void		SkipElement() = 0;

	// RETURNS: true if there is more data to be read in this data block.
	virtual bool		IsDataReadIncomplete() = 0;

	// PURPOSE: Sets a place to store all names (if NULL, all elements have copies of the names)
	void				SetNameTable(parNameTable* table);

	// PURPOSE: If true, after reading in an attribute list we won't sort it for fast searching
	void				SetNeverSortAttrList(bool val);

	// PURPOSE: If true - the parser can use safer (but slower) reading methods. Normally only used when loading from a source that can't be tested beforehand
	void				SetReadSafely(bool val);

	bool				GetReadSafely() const;

	bool				GetHasXInclude() const;

protected:
	parStreamIn(fiStream* stream, const parSettings& settings)
	: parStream(stream, settings)
	, m_BeginElementCB()
	, m_EndElementCB()
	, m_DataCB()
	, m_DataChunkSize(1024)
	, m_NameTable(NULL)
	, m_HasXInclude(false)
	{
		if (!settings.GetFlag(parSettings::USE_SMALL_DATA_CHUNK_SIZE))
		{
			SetDataChunkSize(1 << 14); // 16kb chunks
		}
	}

	BeginElementCB		m_BeginElementCB;
	EndElementCB		m_EndElementCB;
	DataCB				m_DataCB;
	int					m_DataChunkSize;
	parNameTable*		m_NameTable;
	bool				m_HasXInclude;
};

// PURPOSE: parStreamOut specifies an interface for writing elements and data to an fiStream
class parStreamOut : public parStream
{
public:
	// PURPOSE: Write any header information. Should be called first.
	virtual void		WriteHeader(parHeaderInfo&) = 0;

	// PURPOSE: Write a comment string to the file. The writers don't need to actually
	// write a comment string, nor can comment strings be read in.
	virtual void		WriteComment(const char*) = 0;

	// PURPOSE: Writes an element to the stream, along with all the element's attributes.
	virtual void		WriteBeginElement(parElement& elt) = 0;

	// PURPOSE: Writes a leaf (childless) element to the stream
	virtual void		WriteLeafElement(parElement& elt) = 0;

	// PURPOSE: This structure contains some formatting options that can be passed along to the WriteData function.
	// Output formats may treat anything specified here as a suggestion, and need not follow what the options say
	struct WriteDataOptions
	{
		WriteDataOptions() : m_ValuesPerLine(1), m_Unsigned(false), m_Hexadecimal(false), m_HighPrecision(false) {}
		u8		m_ValuesPerLine;	// For int and float types (not vectors, strings, etc.) - how many values on each line
		bool	m_Unsigned;			// For int types - write out the value as an unsigned int or signed
		bool	m_Hexadecimal;		// For int types - write out the hex value (prepended with 0x)
		bool	m_HighPrecision;	// For float and vector types - write out in high precision format?
	};

	// PURPOSE: Writes data to the string
	// PARAMS:
	//		data - The data to write
	//		size - Size of data (in bytes). Optional for ASCII encoding.
	//		completeData - Pass true if you are certain WriteData won't be called again for this data block. Enables some writer optimizations.
	//		enc - The data encoding.
	// NOTES:
	// May be called multiple times before WriteEndElement is called
	// If size is < 0, will write the data to the stream until hitting a '\0' if the encoding allows it
	// completeData is a suggestion to the writer. If you know that you'll only be calling WriteData once
	// before calling WriteEndElement, then pass true and the writing can go faster. If you don't know 
	// then passing false will still write the data correctly but could be slower.
	virtual void		WriteData(const char* data, int size = -1, bool completeData = false, DataEncoding enc = UNSPECIFIED, WriteDataOptions* options = NULL) = 0;

	// PURPOSE: Writes the end element token to the stream. 
	// PARAMS: elt - Should be the same element as the one that was used for the matching WriteBeginElement()
	virtual void		WriteEndElement(parElement& elt) = 0;

protected:
	parStreamOut(fiStream* stream, const parSettings& settings)
	: parStream(stream, settings)
	, m_WroteHeader(false)
	{
	}

	bool				m_WroteHeader;

};

inline void parStreamIn::SetNameTable(parNameTable* table)
{
	m_NameTable = table;
}

inline void parStreamIn::SetBeginElementCallback(BeginElementCB cb)
{
	m_BeginElementCB = cb;
}

inline void parStreamIn::SetEndElementCallback(EndElementCB cb)
{
	m_EndElementCB = cb;
}

inline void parStreamIn::SetDataCallback(DataCB cb)
{
	m_DataCB = cb; 
}

inline void parStreamIn::SetDataChunkSize(int size)
{
	m_DataChunkSize = size;
}

inline bool parStreamIn::GetHasXInclude() const
{
	return m_HasXInclude;
}

} // namespace rage



#endif // PARSERCORE_STREAM_H
