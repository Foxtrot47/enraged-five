// 
// parsercore/streamxml.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSERCORE_STREAMXML_H
#define PARSERCORE_STREAMXML_H

#include "stream.h"

#include "atl/array.h"
#include "atl/binmap.h"
#include "atl/bitset.h"
#include "file/stream.h"

namespace rage {

class Float16;
class Vec2V;
class Vector2;
class Vector3;
class Vector4;

class parStreamInXml : public parStreamIn
{
public:
	parStreamInXml(fiStream* s, const parSettings& settings);
	virtual ~parStreamInXml();

	virtual void SetHeaderBytes(u32 bytes);

	virtual void		ReadBeginElement(parElement& outElement);
	virtual void		ReadLeafElement(parElement& outElement);

	// maxSize can't be too small. At least 4 chars.

	virtual int			ReadData(char* data, int maxSize, DataEncoding enc = UNSPECIFIED);
	virtual void		ReadEndElement(); 
	virtual void		ReadEndElement(char* endTagBuffer);
	virtual void		SkipElement();

	virtual bool		IsDataReadIncomplete();

	virtual bool		ReadWithCallbacks();


	// Checks for the following character sequence. If found, it returns true and consumes the characters.
	// Otherwise returns false, pushes the character back on the stream
	bool Peek(const char* str, bool consumeOnMatch=true);
	bool Peek(char c, bool consumeOnMatch=true);

	// Manual override for the unended leaf flag
	void SetUnendedLeaf(bool bFlag);

	static void			InitClass();
	static void			ShutdownClass();

	// PURPOSE: Pass in a NULL-terminitated list of static strings, and when creating parTrees
	// the parser will use pointers to those strings instead of allocating new strings.
	static void			AddCommonGameStrings(const char* const names[]);

	// PURPOSE: For internal use only - parser structure autoregistration will call these functions
	// to add all the static strings to the nametable
	static void			BeginAddingStaticStrings();
	static void			AddStaticString(const char* str);
	static void			EndAddingStaticStrings();

	static inline bool			IsXmlNameCharacter(int c);
	static inline bool			IsXmlNameStartCharacter(int c);
	static inline bool			IsXmlWhitespaceCharacter(int c);
	static inline bool			IsIntCharacter(int c);
	static inline bool			IsFloatCharacter(int c);
	static inline bool			IsXmlNameString(const char* name);

	static bool					IsValidXmlStart(u32 headerBytes);

protected:
	__forceinline int	ReadRawCharacter();
	int InternalReadUtf16Char(int c);
	__forceinline int	BufferRawCharacter();

	bool ReadWithCallbacksInternal();

	enum {
		ESC_LT = '<' + 256,
		ESC_GT = '>' + 256,
		ESC_AMP = '&' + 256,
		ESC_APOS = '\'' + 256,
		ESC_QUOT = '"' + 256,
		ESC_UNKNOWN = '?' + 256
	};
	int ReadEscapedCharacter(); // turns &lt; into <, etc. Characters that were escaped will have the values from the enum above.

	void PushBack(const char* string, int length = -1);
	void PushBack(char);

	// Like PushBack, but if you read an escaped character (one in the enums above)
	// this will push the escaped version back onto the push buffer ('&lt;' instead of '<')
	void PushBackAndEscape(int);

	int ReadBinaryString(char* str, int size);

	template<typename _Type>
	int ReadIntArrayString(_Type* data, int size);

	template<typename _Type>
	int ReadFloatArrayString(_Type* data, int size);
	int ReadVec2VArrayString(Vec2V* data, int size);
	int ReadVector2ArrayString(Vector2* data, int size);
	int ReadVector3ArrayString(Vector3* data, int size);
	int ReadVector4ArrayString(Vector4* data, int size);
	int ReadUtf16String(char16* data, int size);

	int ReadDataInternal(char*, int size, DataEncoding enc);
	bool ReadElementInternal(parElement& outElement);
	int ReadAttributeValue(char* string);

	// Buf is a buffer that recieves all the whitespace. It will not have comments in it, only actual whitespace.
	void SkipTo(const char* string);
	void SkipWhitespace();
	int SkipWhitespaceAndComments(char* buf = NULL, int bufSize = -1);

	template<bool TokenPred(int)>
	int ReadAnyToken(char* tok, int bufSize=MAX_ELEMENT_NAME_LENGTH);

	int ReadToken(char* tok, int bufSize=MAX_ELEMENT_NAME_LENGTH);

	void SkipHeader(); // skips any unicode byte order marks, processing directives and comments.

	void FindUtfEncodingFromHeader(); // determines the file's encoding (based on the first four bytes in the file)

	// Asserts if there is no match.
	// It's a little regexpy. There are some special characters in the match string.
	// s - any amount of whitespace (no comments)
	// S - any amount of whitespace (with comments)
	// w - one or more token characters ([a-z][A-Z][0-9]:_)
	bool Match(const char* str);
	bool Match(const char* str, char* tagBuffer);

	atFixedArray<u8, 1024> m_PushBackBuffer;

	DataEncoding m_LastEncoding;

	u32 m_Base64Buffer;
	int m_Base64BufferSize;

	atFixedArray<u32, 32> m_UnclosedTagHashes;
	int m_UnclosedTagDepth; // seperate from m_LastUnclosedTagName.GetMaxDepth() because it could be larger

	int m_LineNumber;

	enum FileEncoding
	{
		ENC_UTF8,
		ENC_UTF16_BE,
		ENC_UTF16_LE
	};

	FileEncoding m_FileEncoding;

	bool m_ReadUnendedLeaf;
	bool m_JustBeganElement;
	bool m_JustReadData;
	bool m_DataIncomplete;
	bool m_ReadingCdata;
	bool m_ExpectingData;
	bool m_ParseError;

	// Holds the names of common attributes and values
	static atCaseSensitiveStringMap<const char*> sm_MiniNameTable;

	enum XmlCharacterTrait
	{
		IS_NAME_CHARACTER = 1 << 0,
		IS_WHITESPACE_CHARACTER = 1 << 1,
		IS_NAME_START_CHARACTER = 1 << 2,
		IS_FLOAT_CHARACTER = 1 << 3,
		IS_INT_CHARACTER = 1 << 4,
	};

	static const u8 sm_XmlCharacterTraitTable[];
	// Holds a bitmap with a '1' for each valid token character
	// static atFixedBitSet<256> sm_TokenCharBitmap;
};

class parStreamOutXml : public parStreamOut
{
public:
	parStreamOutXml(fiStream* s, const parSettings& settings);
	virtual ~parStreamOutXml();

	virtual void		WriteHeader(parHeaderInfo&);
	virtual void		WriteComment(const char*);
	virtual void		WriteBeginElement(parElement&);
	virtual void		WriteEndElement(parElement&);
	virtual void		WriteLeafElement(parElement&);
	virtual void		WriteData(const char*, int size = -1, bool completeData = false, DataEncoding enc = UNSPECIFIED, WriteDataOptions* options = NULL);

	// helper functions

	template<typename _Type> // This is explicitly instantiated for (unsigned?) (int, short, char) only
	static void WriteIntArrayString(fiStream* stream, int indentLevel, const _Type* data, size_t size, WriteDataOptions* options);

	template<typename _Type> // This is explicitly instantiated for float and double only
	static void WriteFloatArrayString(fiStream* stream, int indentLevel, const _Type* data, size_t size, WriteDataOptions* options, int skip = 0);

	static void WriteFloat16ArrayString(fiStream* stream, int indentLevel, const Float16* data, size_t size, WriteDataOptions* options, int skip = 0);
	static void WriteVec2VArrayString(fiStream* stream, int indentLevel, const Vec2V* data, size_t size, WriteDataOptions* options);
	static void WriteVector2ArrayString(fiStream* stream, int indentLevel, const Vector2* data, size_t size, WriteDataOptions* options);
	static void WriteVector3ArrayString(fiStream* stream, int indentLevel, const Vector3* data, size_t size, WriteDataOptions* options);
	static void WriteVector4ArrayString(fiStream* stream, int indentLevel, const Vector4* data, size_t size, WriteDataOptions* options);

	static void IndentN(fiStream* stream, int n);

	// If size is < 0, writes until hitting a '\0', otherwise writes until hitting size bytes _or_ a '\0'
	static void WriteEscapedString(fiStream* stream, const char*, int size, bool attribute); // turns < into &lt;, etc.
	static void WriteEscapedCharacter(fiStream* stream, char c, bool attribute);

protected:
	void WriteUtf16String(const char16* data, int size) const;

	void WriteBinaryString(const char* str, size_t size);
	void WritePaddingBytes();

	void WriteBeginElementInternal(parElement& elt, bool leaf);

	int m_IndentLevel;
	u32 m_Base64Buffer;
	int m_Base64BufferSize;
	int m_NumDataChars;

	DataEncoding m_LastEncoding;
	bool m_JustWroteData;
};

inline void parStreamInXml::PushBack(char c)
{
	m_PushBackBuffer.Push(c);
}

inline void parStreamInXml::PushBack(const char* str, int length)
{
	if (length < 0) {
		length = (int)strlen(str);
	}
	int pushCount = m_PushBackBuffer.GetCount();
	m_PushBackBuffer.SetCount(pushCount + length);

	for(int i = length-1; i >= 0; i--)
	{
		m_PushBackBuffer.GetElements()[pushCount] = str[i];
		++pushCount;
	}
}

// Make sure at least one element is in the pushback buffer
__forceinline int parStreamInXml::BufferRawCharacter()
{
	if (m_PushBackBuffer.GetCount() == 0)
	{
		int c = ReadRawCharacter();
		PushBack((char)c);
	}
	return m_PushBackBuffer.Top();
}

__forceinline int parStreamInXml::ReadRawCharacter()
{
	if (m_PushBackBuffer.GetCount() > 0) {
		return m_PushBackBuffer.Pop();
	}
	int c = m_Stream->FastGetCh();
	if (m_FileEncoding != ENC_UTF8)
	{
		c = InternalReadUtf16Char(c);
	}
#if __DEV
	if (c == '\r')
	{
		m_LineNumber++;
	}
#endif
	return c;
}

inline bool parStreamInXml::Peek(char c, bool consumeOnMatch)
{
	
	int curr = BufferRawCharacter();
	if (curr == c && consumeOnMatch)
	{
		m_PushBackBuffer.Pop();
	}
	return curr == c;
}

inline void parStreamInXml::SetUnendedLeaf(bool bFlag)
{
	m_ReadUnendedLeaf = bFlag; 
}

inline bool parStreamInXml::IsXmlNameCharacter(int c)
{
	FastAssert(c >= -1 && c <= 255); // including -1 to check for EOF characters. The cast below turns -1 into 255, which is in our trait table as entry 255
	return (sm_XmlCharacterTraitTable[(u8)c] & IS_NAME_CHARACTER) != 0; 
}

inline bool parStreamInXml::IsXmlNameStartCharacter(int c)
{
	FastAssert(c >= -1 && c <= 255); // including -1 to check for EOF characters. The cast below turns -1 into 255, which is in our trait table as entry 255
	return (sm_XmlCharacterTraitTable[(u8)c] & IS_NAME_START_CHARACTER) != 0; 
}

inline bool parStreamInXml::IsIntCharacter(int c)
{
	FastAssert(c >= -1 && c <= 255); // including -1 to check for EOF characters. The cast below turns -1 into 255, which is in our trait table as entry 255
	return (sm_XmlCharacterTraitTable[(u8)c] & IS_INT_CHARACTER) != 0; 
}


inline bool parStreamInXml::IsFloatCharacter(int c)
{
	FastAssert(c >= -1 && c <= 255); // including -1 to check for EOF characters. The cast below turns -1 into 255, which is in our trait table as entry 255
	return (sm_XmlCharacterTraitTable[(u8)c] & IS_FLOAT_CHARACTER) != 0; 
}

inline bool parStreamInXml::IsXmlNameString(const char* name)
{
	if (!name) return false;
	if (!IsXmlNameStartCharacter(name[0])) return false;
	while(*name)
	{
		if (!IsXmlNameCharacter(*name))
		{
			return false;
		}
		name++;
	}
	return true;
}

inline bool parStreamInXml::IsXmlWhitespaceCharacter(int c)
{
	FastAssert(c >= -1 && c <= 255); // including -1 to check for EOF characters. The cast below turns -1 into 255, which is in our trait table as entry 255
	return (sm_XmlCharacterTraitTable[(u8)c] & IS_WHITESPACE_CHARACTER) != 0;
}


} // namespace rage

#endif // PARSERCORE_STREAMXML_H
