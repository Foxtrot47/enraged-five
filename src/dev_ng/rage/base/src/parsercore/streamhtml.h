// 
// parsercore/streamhtml.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSERCORE_STREAMHTML_H
#define PARSERCORE_STREAMHTML_H

#include "element.h"
#include "stream.h"
#include "streamxml.h"

#include "file/stream.h"

namespace rage {


class parStreamInHtml : public parStreamInXml
{
public:
	parStreamInHtml(fiStream* s, const parSettings& settings);
	virtual ~parStreamInHtml();

	
	virtual void		ReadBeginElement(parElement& outElement);
	virtual void		ReadLeafElement(parElement& outElement);

	bool				IsAtEndTag();
	bool				IsAtEndOfStream();

private:
	void ReadInternal(parHtmlElement& outElement);

	// It's a little regexpy. There are some special characters in the match string.
	// s - any amount of whitespace (no comments)
	// S - any amount of whitespace (with comments)
	// w - one or more token characters ([a-z][A-Z][0-9]:_)
	bool Match(const char* str);
	bool Match(const char* str, char* tagBuffer);

	int Tell();
	void Seek(int pos);

protected:
};

} // namespace rage

#endif // PARSERCORE_STREAMHTML_H
