// 
// parsercore/utils.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include <limits.h>
#include "utils.h"
#include "system/nelem.h"
#include <algorithm>
#include <stdio.h>
#include <time.h>

#include "file/asset.h"
#include "file/remote.h"
#include "file/stream.h"
#include "parser/macros.h"
#include "parser/optimisations.h"
#include "system/exec.h"

PARSER_OPTIMISATIONS();

using namespace rage;

namespace rage
{
namespace parUtils
{

sysMemAllocator* g_ParPhysicalAllocator = NULL;
eMemoryType g_ParPhysicalHeapIdx = MEMTYPE_GAME_PHYSICAL;

#if RSG_BANK
bool P4PromptHelper(bool autoAction, bool promptForAction, const char* promptFmtString, const char* title, const char* cmdFmtString, const char* filename)
{
	bool shouldDoAction = false;
	if (autoAction)
	{
		shouldDoAction = true;
	}
	else if (promptForAction)
	{
		// TODO: We probably only want to prompt for checkout when there is some sort of GUI in place too,
		// command line only tools like ragebuilder shouldn't do it. But how can we tell from a RageCore library whether there
		// is a GUI present or not?
		char prompt[RAGE_MAX_PATH];
		formatf(prompt, promptFmtString, filename);
		u32 answer = fiRemoteShowMessageBox(prompt, title, MB_YESNOCANCEL | MB_ICONQUESTION | MB_SETFOREGROUND, IDNO);
		switch(answer)
		{
		case IDYES: shouldDoAction = true; break;
		case IDNO: break;
		case IDCANCEL: return false; 
		default:
			parErrorf("Unexpected message box response %d", answer);
			break;
		}
	}

	if (shouldDoAction)
	{
		char cmd[RAGE_MAX_PATH];
		formatf(cmd, cmdFmtString, filename);
		int result = sysExec(cmd);
		if (result == 0)
		{
			parDisplayf("Completed: %s", cmd);
		}
		else
		{
			parWarningf("Failed (%d): %s", result, cmd);
		}
	}
	return true;
}
#endif

fiStream* OpenWritableFileWithOptionalCheckout(const char* filename, const char* ext, u32 BANK_ONLY(checkoutTypeFlags))
{
#if RSG_BANK
	char fullWriteName[RAGE_MAX_PATH];
	ASSET.FullWritePath(fullWriteName, RAGE_MAX_PATH, filename, ext);
	const fiDevice* dev = fiDevice::GetDevice(fullWriteName, false);

	// If we are trying to write over RFS or are on a PC writing to a local path
	const fiDevice* lowLevelDevice = dev ? dev->GetLowLevelDevice() : NULL;
	if (lowLevelDevice == &fiDeviceRemote::GetInstance() || (RSG_PC && lowLevelDevice == &fiDeviceLocal::GetInstance()))
	{
		char lowLevelName[RAGE_MAX_PATH];
		dev->FixRelativeName(lowLevelName, RAGE_MAX_PATH, fullWriteName);

		// Replace / with \, for sending to DOS commands
		for(char* c = &lowLevelName[0]; *c; ++c)
		{
			if (*c == '/')
			{
				*c = '\\';
			}
		}

		u32 attributes = ASSET.GetAttributes(lowLevelName, "");
		if (attributes == FILE_ATTRIBUTE_INVALID) // File is missing. Should we add it?
		{
			if (!P4PromptHelper((checkoutTypeFlags & ADD_AUTO) != 0, (checkoutTypeFlags & ADD_PROMPT) != 0, 
				"File \"%s\" doesn't exist. Add it to perforce?",
				"Add file to perforce?",
				((checkoutTypeFlags & ADD_AS_BINARY) != 0) ? "p4 add -t binary \"%s\"" : "p4 add \"%s\"",
				lowLevelName))
			{
				return NULL;
			}
		}
		else 
		{
			if (attributes & FILE_ATTRIBUTE_READONLY || checkoutTypeFlags & CHECKOUT_WRITABLE)
			{
				if (!P4PromptHelper((checkoutTypeFlags & CHECKOUT_AUTO) != 0, (checkoutTypeFlags & CHECKOUT_PROMPT) != 0,
					"File \"%s\" isn't writable, check out from perforce?",
					"Check out file from perforce?",
					"p4 edit \"%s\"",
					lowLevelName))
				{
					return NULL;
				}
			}
			if (checkoutTypeFlags & LOCAL_BACKUP)
			{
				char cmd[RAGE_MAX_PATH * 2];
				formatf(cmd, "move /Y \"%s\" \"%s.bak\"", lowLevelName, lowLevelName);
				ASSERT_ONLY(int result =) sysExec(cmd);
				parAssertf(result == 0, "Couldn't back up %s", lowLevelName);
			}
		}
	}
#endif

	return ASSET.Create(filename, ext);
}

bool StringToIntSafe(const char* str, int& outValue, const char* endStr)
{
	// error checking for strtol really sucks...
	// You have to check errno, but only if the function first returns a potential overflow value. 
	// Plus you need to see how many characters were converted.

	char* endp = NULL;
#if __WIN32
	_set_errno(0);
#else
	errno = 0;
#endif
	bool checkError = false;

	if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X'))
	{
		u32 outu32 = (strtoul(str+2, &endp, 16));
		if (outu32 == UINT_MAX)
		{
			checkError = true; // Check for overflow
		}
		outValue = (int)outu32;
	}
	else
	{
		outValue = strtol(str, &endp, 10);
		if (outValue == INT_MIN || outValue == INT_MAX)
		{
			checkError = true; // Check for overflow
		}
	}

	bool foundError = false;

	if (endp != endStr && *endp != '\0') // We didn't scan the full string for some reason
	{
		foundError = true;
	}

	if (checkError)
	{
#if __WIN32
		int err;
		_get_errno(&err);
		foundError = (err == ERANGE);
#else	
		foundError = (errno == ERANGE);
#endif
	}

	return !foundError;
}

bool MemoryMapFile(const char* filename, char*& outData, int& outSize)
{
	char* storage = NULL;
	int size = 0;
	fiStream* file = NULL;
	bool allocatedData = false;

	outData = NULL;
	outSize = 0;

	if (!strncmp(filename, "memory:", 7))
	{
		// we have a memory file, just get the addr and size directly.

		int args = sscanf(filename, "memory:$%p,%d", &storage, &size);

		if (args != 2)
		{
			return false;
		}
	}
	else if (!strncmp(filename, "growbuffer:", 11))
	{
		int args = sscanf(filename, "growbuffer:$%p,%d", &storage, &size);
		if (args != 2)
		{
			return false;
		}
	}
	else
	{
		file = ASSET.Open(filename, "");

		if (!file)
		{
			return false;
		}

		size = file->Size();
		storage = rage_aligned_new(16) char[size];
		file->Read(storage, size);
		file->Close();

		allocatedData = true;
	}

	outData = storage;
	outSize = size;
	return allocatedData;
}

void SetPhysicalAllocator(sysMemAllocator* allocator, eMemoryType heapIdx)
{
	g_ParPhysicalAllocator = allocator;
	g_ParPhysicalHeapIdx = heapIdx;
}

char* PhysicalAllocate(size_t size, size_t align)
{
	MEM_USE_USERDATA(MEMUSERDATA_PARSER);
	if (g_ParPhysicalAllocator)
	{
		return (char*)g_ParPhysicalAllocator->Allocate(size, align, g_ParPhysicalHeapIdx);
	}
	else
	{
//		sysMemAutoUseAllocator allocator(sysMemAllocator::GetMaster());
		return (char*)physical_new(size, align);
	}
}

void PhysicalFree(char* ptr)
{
	MEM_USE_USERDATA(MEMUSERDATA_PARSER);
	if (g_ParPhysicalAllocator)
	{
		g_ParPhysicalAllocator->Free(ptr);
	}
	else
	{
//		sysMemAutoUseAllocator allocator(sysMemAllocator::GetMaster());
		delete [] ptr;
	}
}

} // namespace parUtils
} // namespace rage

