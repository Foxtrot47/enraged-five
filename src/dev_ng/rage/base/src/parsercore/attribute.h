// 
// parsercore/attribute.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSERCORE_ATTRIBUTE_H
#define PARSERCORE_ATTRIBUTE_H

#include "utils.h"

#include "atl/array.h"
#include "atl/bitset.h"

#include "parser/macros.h"

namespace rage {

using namespace parUtils;

class parElement;
class parManager;
class parStructure;
class parTreeNode;

// PURPOSE: A class to represent name=value pairs. The value can be a string, int, bool, float.
// Strings for both value and name can have either internal or external ownership.
class parAttribute
{

public:
	parAttribute();

	// NOTES:
	// Does NOT free any memory used by the name or a string value. Use Clear() for that.
	~parAttribute();

	// PURPOSE:
	// Releases memory used by the name and string value (if the attribute owned those pointers
	void Clear();

	// RETURNS: The name of the attribute
	const char*			GetName() const {return m_Name;}

	// PURPOSE: Sets the name of the attribute.
	// PARAMS:
	//		str - The name of the attribute
	//		copy - If true, the attribute will make a copy of name
	void				SetName(const char* str, bool copy = true);

	// PURPOSE: Sets the value of the attribute.
	// PARAMS:
	//		str - The string value for the attribute
	//		copy - If true, the attribute will make a copy of the value
	void				SetValue(const char* str, bool copy = true);

	// PURPOSE: Sets the value of the attribute
	// PARAMS:
	//		i - The int value for the attribute
	void				SetValue(s64 i);

	// PURPOSE: Sets the value of the attribute
	// PARAMS:
	//		f - The float value for the attribute
	void				SetValue(double f);

	// PURPOSE: Sets the value of the attribute
	// PARAMS:
	//		b - The bool value for the attribute
	void				SetValue(bool b);

	// PURPOSE: Returns the string value of the attribute. Attribute's type must be string.
	const char*			GetStringValue() const;

	// PURPOSE: Returns the bool value of the attribute. Attribute's type must be bool.
	bool				GetBoolValue() const;

	// PURPOSE: Returns the int value of the attribute. Attribute's type must be int64.
	s64					GetInt64Value() const;
	int					GetIntValue() const;

	// PURPOSE: Returns the float value of the attribute. Attribute's type must be double.
	double				GetDoubleValue() const;
	float				GetFloatValue() const;

	// PURPOSE: Returns the bool value of the attribute. Attributes type can be string or bool.
	bool				FindBoolValue() const;

	// PURPOSE: Returns the int value of the attribute. Attributes type can be string or int.
	s64					FindInt64Value() const;
	int					FindIntValue() const;

	// PURPOSE: Returns the float value of the attribute. Attributes type can be string or float.
	double				FindDoubleValue() const;
	float				FindFloatValue() const;

	// PURPOSE: Convert a string valued attribute to an int.
	void				ConvertToInt64();

	// PURPOSE: Convert a string valued attribute to an float.
	void				ConvertToDouble();

	// PURPOSE: Convert a string valued attribute to an bool.
	void				ConvertToBool();

	// PURPOSE: Convert a attribute of any type to a string.
	void				ConvertToString();

	// PURPOSE: Gets the string representation of the attribute
	// PARAMS:
	//		buffer - A temp buffer to hold the results if the attribute had to be converted from a non-string type
	//		size - The size of the buffer in bytes
	//		alwaysCopy - If true, even string values get copied into buffer (and the buffer pointer is returned
	//				if the string is non-NULL)
	// NOTES:
	//   buffer must be at least 50 bytes long to be able to store the string representation of floats up to +/- FLT_MAX
	//   If type is string, returns a pointer to the string
	//   Otherwise, fills buffer with the new string value, returns pointer to buffer.
	const char*			GetStringRepr(char* buffer, int size, bool alwaysCopy = false) const;
    template<int SIZE>
	const char*			GetStringRepr(char (&buffer)[SIZE], bool alwaysCopy = false) const
    {
        return GetStringRepr(buffer, SIZE, alwaysCopy);
    }

	// PURPOSE: Enumeration for all the allowed types for attribute values
	enum Type
	{
		STRING,		// char* type. The string can be copied locally or not.
		INT64,		// int (s64) type
		DOUBLE,		// double type
		BOOL,		// bool type
	};

	// RETURNS: The type of the attribute
	Type				GetType() const;

	bool				GetHighPrecision() const;
	bool				GetUnsigned() const;
	bool				GetHexadecimal() const;

	void				SetHighPrecision(bool b);
	void				SetUnsigned(bool b);
	void				SetHexadecimal(bool b);

protected:
	friend class parElement;
	friend class parAttributeList;

	enum Flags
	{
		FLAG_OWNS_NAME_STR,
		FLAG_OWNS_VALUE_STR,
		FLAG_HIGH_PRECISION,
		FLAG_UNSIGNED,
		FLAG_HEXADECIMAL,
	};

	const char* m_Name;

	union {
		const char* m_String;
		s64			m_Int64;
		double		m_Double;
		bool		m_Bool;
	} m_Value;

	u8 m_Type;
	atFixedBitSet8 m_Flags;

	void ClearOldValueString() {
		if (m_Type == STRING && m_Flags.IsSet(FLAG_OWNS_VALUE_STR))
		{
			delete [] m_Value.m_String;
            m_Value.m_String = NULL;
		}
	}
};

class parAttributeList
{
public:
	parAttributeList() : m_UserData1(0), m_UserData2(0) {}

	~parAttributeList();

	void Reset();

	// PURPOSE: Add a new string attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - string value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	//		copyvalue - If true, the attribute will copy the value string
	parAttribute* AddAttribute(const char* name, const char* value, bool copyname = true, bool copyvalue = true);

	// PURPOSE: Add a new int attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - int value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	parAttribute* AddAttribute(const char* name, int value, bool copyname = true) { return AddAttribute(name, (s64)value, copyname); }
	parAttribute* AddAttribute(const char* name, s64 value, bool copyname = true);

	// PURPOSE: Add a new float attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - float value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	parAttribute* AddAttribute(const char* name, double value, bool copyname = true);

	// PURPOSE: Add a new bool attribute to the element
	// PARAMS:
	//		name - Name of the new attribute
	//		value - bool value for the new attribute
	//		copyname - If true, the attribute will copy the name string
	parAttribute* AddAttribute(const char* name, bool value, bool copyname = true);

	// PURPOSE: Sorts the attributes by name, to speed up FindAttribute
	void SortAttributes();

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	parAttribute* FindAttribute(const char* name);

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	const parAttribute* FindAttribute(const char* name) const;

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	parAttribute* FindAttributeAnyCase(const char* name);

	// PURPOSE: Searches for an attribute in the attribute list.
	// RETURNS: Pointer to the attribute if found, NULL otherwise.
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	const parAttribute* FindAttributeAnyCase(const char* name) const;

	// PURPOSE: Searches for an attribute in the attribute list. Asserts that it exists
	// RETURNS: Reference to the attribute
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	parAttribute& FindRequiredAttribute(const char* name);

	// PURPOSE: Searches for an attribute in the attribute list. Asserts that it exists
	// RETURNS: Reference to the attribute
	// NOTES: If SortAttributes() has been called and no attributes have been added since, uses a binary search.
	const parAttribute& FindRequiredAttribute(const char* name) const;

	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The string value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	//		buf - A temporary buffer used in case we need to convert from a non-string type to string. It should be at least 50 bytes
	//		bufSize - Size of the temporary buffer
	//		alwaysCopy - Normally we only write to the 'buf' buffer when converting from a non-string type. If alwaysCopy is true we always
	//				write the result to the buffer (even if its the default value).
	//		warnIfMissing - If the attribute is missing, print a warning
	const char* FindAttributeStringValue(const char* attrName, const char* def, char* buf, int bufSize, bool alwaysCopy=false, bool warnIfMissing = false) const;

	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The float value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	//		warnIfMissing - If the attribute is missing, print a warning
	float FindAttributeFloatValue(const char* attrName, float def, bool warnIfMissing = false) const;
	double FindAttributeDoubleValue(const char* attrName, double def, bool warnIfMissing = false) const;

	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The int value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	//		warnIfMissing - If the attribute is missing, print a warning
	int FindAttributeIntValue(const char* attrName, int def, bool warnIfMissing = false) const;
	s64 FindAttributeInt64Value(const char* attrName, s64 def, bool warnIfMissing = false) const;


	// PURPOSE: Searches for an attribute in the attribute list, returns the attribute's value
	// RETURNS: The bool value for the attribute
	// PARAMS:
	//		attrName - The attribute to search for
	//		def - The default value to return if the attribute is not found
	//		warnIfMissing - If the attribute is missing, print a warning
	bool FindAttributeBoolValue(const char* attrName, bool def, bool warnIfMissing = false) const;

	// PURPOSE: Brings all the attributes in the 'other' list into this list
	// PARAMS:
	//		other - The attribute list to merge into this one
	//		overwrite - If true, attributes in 'other' will overwrite ones that are in this list.
	void MergeFrom(const parAttributeList& other, bool overwrite=true);

	// PURPOSE: Replaces the contents of the current attribute list with the new one
	void CopyFrom(const parAttributeList& other);

	// For serialization when this is part of another class
	void OnLoad(parTreeNode* node);
	void OnSave(parTreeNode* node);

	enum Flags
	{
		FLAG_ATTR_LIST_SORTED,
		FLAG_OWNER_FLAGS, // flags from here on can use
	};

	atFixedBitSet16& GetFlags() {return m_Flags;}
	const atFixedBitSet16& GetFlags() const {return m_Flags;}

	u8& GetUserData1() {return m_UserData1;}
	const u8& GetUserData1() const {return m_UserData1;}

	u8& GetUserData2() {return m_UserData2;}
	const u8& GetUserData2() const {return m_UserData2;}

	bool IsSorted()
	{
		return m_Flags.IsSet(FLAG_ATTR_LIST_SORTED);
	}

	atArray<parAttribute>& GetAttributeArray()
	{
		return m_Attributes;
	}

    const atArray<parAttribute>& GetAttributeArray() const
    {
        return m_Attributes;
    }

    PAR_SIMPLE_PARSABLE;

protected:
	atArray<parAttribute> m_Attributes;
	u8 m_UserData1;			 // Not used by the attribute list itself, but might as well make the space available for storage.
	u8 m_UserData2;			 // Not used by the attribute list itself, but might as well make the space available for storage.
	atFixedBitSet16 m_Flags; // Only one flag gets used by the attribute list itself, but parent objects and use anything starting at FLAG_OWNER_FLAGS
};

inline parAttribute::parAttribute() 
: m_Name(NULL) 
, m_Type(STRING)
, m_Flags(0)
{
	m_Value.m_String = NULL;
}

inline parAttribute::~parAttribute()
{
}

inline const char* parAttribute::GetStringValue() const
{
	FastAssert(m_Type == STRING); 
	return m_Value.m_String;
}

inline bool parAttribute::GetBoolValue() const
{
	FastAssert(m_Type == BOOL); 
	return m_Value.m_Bool;
}

inline s64 parAttribute::GetInt64Value() const
{
	FastAssert(m_Type == INT64); 
	return m_Value.m_Int64;
}

inline int parAttribute::GetIntValue() const { return (int)GetInt64Value(); }
inline int parAttribute::FindIntValue() const { return (int)FindInt64Value(); }

inline double parAttribute::GetDoubleValue() const
{
	FastAssert(m_Type == DOUBLE); 
	return m_Value.m_Double;
}

inline float parAttribute::GetFloatValue() const { return (float)GetDoubleValue(); }
inline float parAttribute::FindFloatValue() const { return (float)FindDoubleValue(); }

inline parAttribute::Type parAttribute::GetType() const
{
	return (Type)m_Type;
}

inline bool parAttribute::GetHighPrecision() const
{
	return m_Flags.IsSet(FLAG_HIGH_PRECISION);
}

inline bool parAttribute::GetUnsigned() const
{
	return m_Flags.IsSet(FLAG_UNSIGNED);
}

inline bool parAttribute::GetHexadecimal() const
{
	return m_Flags.IsSet(FLAG_HEXADECIMAL);
}

inline void parAttribute::SetHighPrecision(bool b) 
{
	m_Flags.Set(FLAG_HIGH_PRECISION, b);
}

inline void parAttribute::SetUnsigned(bool b)
{
	m_Flags.Set(FLAG_UNSIGNED, b);
}

inline void parAttribute::SetHexadecimal(bool b)
{
	m_Flags.Set(FLAG_HEXADECIMAL, b);
}

inline const parAttribute* parAttributeList::FindAttribute(const char* name) const
{
	return const_cast<parAttributeList*>(this)->FindAttribute(name);
}

inline const parAttribute* parAttributeList::FindAttributeAnyCase(const char* name) const
{
	return const_cast<parAttributeList*>(this)->FindAttributeAnyCase(name);
}



} // namespace rage;

#endif // PARSERCORE_ATTRIBUTE_H
