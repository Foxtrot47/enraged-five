// 
// parsercore/versioninfo.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "versioninfo.h"

#include "parser/optimisations.h"

#include <stdlib.h>

PARSER_OPTIMISATIONS();

using namespace rage;

parVersionInfo::parVersionInfo(const char* str)
{
	char buf[32];
	char* minorPtr = NULL;
	// copy the string into buf, and change maj.min into  maj\0min
	int i;
	for(i = 0; (str[i] != '\0') && (i < 31); i++) {
		if (str[i] == '.') {
			buf[i] = '\0';
			minorPtr = buf + i + 1;
		}
		else {
			buf[i] = str[i];
		}
	}

	buf[i] = '\0';

	int val = atoi(buf);
	FastAssert(val < USHRT_MAX);
	m_Major = (u16)val;

	if(minorPtr && (minorPtr[0] != '\0'))
	{
		val = atoi(minorPtr);
		FastAssert(val < USHRT_MAX);
		m_Minor = (u16)val;
	}
}

parVersionInfo::Status parVersionInfo::CompareVersions(const parVersionInfo& fileVersion, const parVersionInfo& codeVersion)
{
	if (fileVersion.m_Major == 0 && fileVersion.m_Minor == 0) {
		return FILE_UNVERSIONED;
	}

	// Major version number must match exactly.
	if (fileVersion.m_Major != codeVersion.m_Major) {
		return INCOMPATABLE;
	}

	// Minor version numbers are allowed to differ, but report the type of difference.
	if (fileVersion.m_Minor > codeVersion.m_Minor) {
		return FILE_IS_NEWER;
	}
	else if (fileVersion.m_Minor < codeVersion.m_Minor) {
		return CODE_IS_NEWER;
	}
	else {
		return EXACT_MATCH;
	}
}

