// 
// parsercore/utils.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSERCORE_UTILS_H
#define PARSERCORE_UTILS_H

#include "atl/bitset.h"
#include "atl/hashstring.h"
#include "string/string.h"
#include "system/memory.h"
#include "system/platform.h"

#include <stdlib.h>
#include "errno.h"

namespace rage {

class fiStream;

namespace parUtils
{
	// PURPOSE: Converts a string to a boolean
	// NOTES: An empty string, or a string starting with 'f', 'F', or '0' are false. Everything else is true.
	inline bool StringToBool(const char* str);

	// PURPOSE: Converts a string to an int. 
	inline int StringToInt(const char* str);
	inline s64 StringToInt64(const char* str);

	// PURPOSE: Converts a string to an int, but makes sure the parsing doesn't stop early
	// PARAMS:
	//		str - The string to read
	//		outValue - the output value, valid if the function returns true, otherwise will be overwritten with junk
	//		endStr - OPTIONAL - the parse is successful if we end on a '\0' or end on endStr without errors.
	// NOTES: Doesn't handle ints > 2^31
	bool StringToIntSafe(const char* str, int& outValue, const char* endStr = NULL);

	// PURPOSE: Converts a string to a float (really just for symmetry)
	inline float StringToFloat(const char* str);
	inline double StringToDouble(const char* str);

	// PURPOSE: Returns true if the strings match
	// PARAMS:
	//  a - The first string
	//  b - The second string
	//  caseInsensitive - Should we do a case-insensitive comparison?
	inline bool StringEquals(const char* a, const char* b, bool caseInsensitive = false);

	// RETURNS: True if the string is all whitespace characters (" \t\n\r\f")
	inline bool IsAllWhitespace(const char* s);

	// PURPOSE: Looks in the final hash dictionary and then the non-final dictionary for the string for this hash
	inline const char* FindStringFromHash(atHashValue hash);

	// RETURNS: True if platform represents the current platform
	// NOTES: Takes all the standard strings from platform.cpp, but "pc" means pc or x64 and you can also pass in "console" or "tool"
	inline bool IsCurrentPlatform(const char* platform);

	// PURPOSE: Ensures the entire file is loaded memory somewhere
	// RETURNS: True if we needed to allocate new memory
	// NOTES: If the filename is a "memory:" or "growbuffer:" filename, this will not allocate new memory, just return pointers to the
	// existing memory
	bool MemoryMapFile(const char* filename, char*& outData, int& outSize);

	enum CheckoutType
	{
		CHECKOUT_AUTO		= BIT1, // Should we automatically try to check out existing files if they are read-only (takes precedence over PROMPT below)
		CHECKOUT_PROMPT		= BIT2, // Should we prompt to check out existing read-only files
		CHECKOUT_WRITABLE	= BIT3, // Should the above flags apply to existing WRITABLE files as well?
		ADD_AUTO			= BIT4, // Should we automatically try to add new files to p4 (takes precendence over PROMPT below)
		ADD_PROMPT			= BIT5, // Should we prompt to add new files?
		ADD_AS_BINARY		= BIT6, // Force new files to be added as binary. If you don't specify this, they'll be added based on their extension and 'p4 typemap' 
		LOCAL_BACKUP		= BIT7, // Make a local backup if the file already exists

		NO_CHECKOUT = 0,	// Don't try and check out or add any files
		AUTO_CHECKOUT_AND_ADD = CHECKOUT_AUTO | ADD_AUTO ,	// Automatically add missing files, automatically check out readonly files
		PROMPT_FOR_CHECKOUT_DONT_ADD = CHECKOUT_PROMPT ,		// Prompt to check out readonly files, don't add new files
		PROMPT_FOR_CHECKOUT_OR_ADD = CHECKOUT_PROMPT | ADD_PROMPT ,	// Prompt to check out readonly files, prompt to add missing files
		AUTO_CHECKOUT_WRITABLE_AND_ADD = CHECKOUT_WRITABLE | CHECKOUT_AUTO | ADD_AUTO ,	// Always try to check out or add files, even writable ones.
	};
	fiStream* OpenWritableFileWithOptionalCheckout(const char* filename, const char* ext, u32 checkoutTypeFlags);

	// PURPOSE: Sets the magic array cookie for a C-style array to 'newCount'
	inline void SetArrayCookie(void *pointer, size_t alignment, size_t ASSERT_ONLY(storageSize), size_t newCount);

	// PURPOSE: Works around a problem where we can't use the normal rage physical allocator, but also can't get to the streaming allocator since it's defined in rage\framework
	void SetPhysicalAllocator(sysMemAllocator* allocator, eMemoryType heapIdx);

	// PURPOSE: Does a physical allocation using the preferred physical allocator
	char* PhysicalAllocate(size_t size, size_t align);
	void PhysicalFree(char*);

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// Implementations:
	
	inline bool StringToBool(const char* str)
	{
		return (str && !(str[0] == 'F' || str[0] == 'f' || str[0] == '0'));
	}

	inline int StringToInt(const char* str)
	{
		return (int)StringToInt64(str);
	}

	inline s64 StringToInt64(const char* str)
	{
		if (!str) return 0;
		if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X'))
		{
#if __WIN32
			return (s64)_strtoui64(str+2, NULL, 16); 
#else
			return (s64)strtoull(str, NULL, 16);
#endif
		}
		bool negate = false;
		if (str[0] == '-')
		{
			negate = true;
			++str;
		}
		s64 val;
#if __WIN32
		val = (s64)_strtoui64(str, NULL, 10);
#else
		val = (s64)strtoull(str, NULL, 10);
#endif
		return negate ? -val : val;
	}
	
	inline float StringToFloat(const char* str)
	{
		return (float)StringToDouble(str);
	}

	inline double StringToDouble(const char* str)
	{
		if (!str) return 0.0;
		return atof(str);
	}

	inline bool StringEquals(const char* a, const char* b, bool caseInsensitive /*= false*/)
	{
		return !(caseInsensitive ? stricmp(a, b) : strcmp(a, b));
	}

	inline bool IsAllWhitespace(const char* s)
	{
		if (!s || s[0] == '\0') {
			return false;
		}
		while(*s)
		{
			char c = *s;
			if (c != ' ' && c != '\t' && c != '\n' && c != '\r' && c != '\f')
			{
				return false;
			}
			++s;
		}
		return true;
	}

	inline const char* FindStringFromHash(atHashValue hash)
	{
		const char* str = atFinalHashString::TryGetString(hash.GetHash());
		if (str)
		{
			return str;
		}
#if !__FINAL
		return atNonFinalHashString::TryGetString(hash.GetHash());
#else
		return NULL;
#endif
	}

	inline bool IsCurrentPlatform(const char* platform)
	{
#if RSG_ORBIS
		// TODO: Remove this temp hack once PS4-specific data is in
		if (!stricmp(platform, "ps4"))
		{
			return (g_sysPlatform == platform::ORBIS);
		}
#endif // RSG_ORBIS
#if RSG_DURANGO
		// TODO: Remove this temp hack once XBOX One-specific data is in
		if (!stricmp(platform, "xbox1"))
		{
			return (g_sysPlatform == platform::DURANGO);

		}
#endif // RSG_DURANGO
#if RSG_PC
		// TODO: Remove this temp hack once XBOX One and PS4 data are separate from PC
		if (!stricmp(platform, "pc_only"))
		{
			return (g_sysPlatform == platform::WIN32PC || g_sysPlatform == platform::WIN64PC);
		}
#endif // RSG_PC
		if (!stricmp(platform, "pc"))  // in sysGetPlatform 'pc' means just win32pc, but for parser purposes it means any pc
		{
			return (g_sysPlatform == platform::WIN32PC || g_sysPlatform == platform::WIN64PC || g_sysPlatform == platform::DURANGO || g_sysPlatform == platform::ORBIS);
		}
		if (!stricmp(platform, "console"))
		{
			return (g_sysPlatform == platform::XENON || g_sysPlatform == platform::PS3 || g_sysPlatform == platform::PSP2);
		}
		if (!stricmp(platform, "tool"))
		{
			return false;	// Should we do something more clever here? The problem is that ragebuilder will be a tool, but we will 
							// export PC data from it by passing -platform=win32. So when we load XML files, what parts do we strip out?
							// For now strip out all tools-only data.
		}
		if (!stricmp(platform, "64bit"))
		{
			return sysIs64Bit(g_sysPlatform);
		}
		if (!stricmp(platform, "32bit"))
		{
			return !sysIs64Bit(g_sysPlatform); // This works until we get 128 bit machines :)
		}
		return g_sysPlatform == sysGetPlatform(platform);
	}


	inline void SetArrayCookie(void *pointer, size_t alignment, size_t ASSERT_ONLY(storageSize), size_t newCount) {
		// THIS IS ABI DEPENDENT, but according to the C++ spec the array cookie will always be stored in front of the array.
		// Just exactly where in front of the array, how big the cookie is, etc. is the ABI dependent part. 

		// This is a good description of what's going on: 
		// http://refspecs.linux-foundation.org/cxxabi-1.83.html#array-cookies
		// But that's not the actual layout that we see. Instead we see what's below, where alignment of 16 messes things up

		if (alignment < 16 || RSG_PS3)
		{
			// Note the 'u32' here. What about 64-bit you wonder? Me too, but apparently for our x64 builds the array cookies are 4 bytes.
			u32* p = reinterpret_cast<u32*>(pointer);
			p -= 1;
			Assertf(*p == storageSize, "Expected %x, got %x",(int)storageSize,(int)*p);
			*p = (u32)newCount;
		}
		else
		{
			// Windows' ABIs do a weird thing with 16 byte alignment, they puts the cookie 16b from the front of the array instead of 4b
			u32* p = reinterpret_cast<u32*>(pointer);
			p -= 16 / sizeof(u32);
			Assertf(*p == storageSize, "Expected %x, got %x",(int)storageSize,(int)*p);
			*p = (u32)newCount;
		}
	}


} // namespace parUtils

} // namespace rage

#endif // PARSERCORE_UTILS_H
