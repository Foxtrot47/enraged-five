// 
// parsercore/streamrbf.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "streamrbf.h"

#include "attribute.h"
#include "element.h"

#include "file/stream.h"
#include "parser/optimisations.h"
#include "string/stringhash.h"
#include "system/alloca.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"

PARSER_OPTIMISATIONS();

using namespace rage;

enum RecordType
{
	CONTAINER_RECORD	= 0x0,
	INT_RECORD			= 0x1,
	BOOL_TRUE_RECORD	= 0x2,
	BOOL_FALSE_RECORD	= 0x3,
	FLOAT_RECORD		= 0x4,
	VECTOR3_RECORD		= 0x5,
	STRING_RECORD		= 0x6,

	SPECIAL_RECORD		= 0xF
};

enum SpecialRecordID
{
	POP_RECORD_ID				= 0xFFFF,
	EXTENDED_RECORD_ID			= 0xFFFE,
	DATA_CONTAINER_RECORD_ID	= 0xFFFD,
	COMMENT_RECORD_ID			= 0xFFFC
};

inline u16 GetRecordIDFlags(u16 rid)
{
	return rid >> 12;
}

inline u32 GetRecordIDNameID(u16 rid)
{
	return rid & 0x0FFF;
}


template<typename _T>
inline const _T& PAR_MIN(const _T& a, const _T& b)
{
	return (a < b) ? a : b;
}

template<typename _T>
inline const _T& PAR_MAX(const _T& a, const _T& b)
{
	return (a < b) ? a : b;
}


parStreamInRbf::parStreamInRbf(fiStream* s, const parSettings& settings)
: parStreamIn(s, settings)
, m_LastEncoding(UNSPECIFIED)
, m_DataRemaining(0)
, m_JustReadLeaf(false)
, m_ReadingData(false)
, m_JustBeganElement(false)
{
}

parStreamInRbf::~parStreamInRbf()
{
	// delete the internal nametable
	for(int i = 0; i < m_InternalNameTable.GetCount(); ++i)
	{
		delete m_InternalNameTable[i];
	}
	m_InternalNameTable.Reset();
}

void parStreamInRbf::SetHeaderBytes(u32 /*bytes*/)
{
	// ignore them.
}

void parStreamInRbf::SkipSizedBlock()
{
	char* dataBuf = Alloca(char, MAX_ATTRIBUTE_VALUE_LENGTH);

	u32 dataRemaining;
	m_Stream->ReadInt(&dataRemaining, 1);
	while(dataRemaining > 0)
	{
		dataRemaining -= m_Stream->Read(dataBuf, PAR_MIN<int>(dataRemaining, m_DataChunkSize));
	}
}

u16 parStreamInRbf::GetNextRecordID()
{
	// skips comments
	u16 recordID = COMMENT_RECORD_ID;
	bool done = false;
	while(!done) {
		m_Stream->ReadShort(&recordID, 1);
		if (recordID == COMMENT_RECORD_ID) {
			SkipSizedBlock();
		}
		else {
			done = true;
		}
	}
	return recordID;
}

void parStreamInRbf::ReadFullRecordID(u16& recordID, u16& flags, u32& nameID, const char*& name, bool isAttribute)
{
	recordID = GetNextRecordID();

	if (recordID == EXTENDED_RECORD_ID)
	{
		u32 realID;
		m_Stream->ReadInt(&realID, 1);
		flags = (u16)(realID >> 28);
		nameID = realID & 0x0FFFFFFF;
	}
	else
	{
		flags = recordID >> 12;
		nameID = recordID & 0x0FFF;
	}

	if (flags == SPECIAL_RECORD) 
	{
		name = NULL;
	}
	else
	{
		parNameTable& activeNT = m_NameTable ? *m_NameTable : m_InternalNameTable;
		parAssertf((int)nameID <= activeNT.GetCount(), "Bad nametable index %d, max is %d", nameID, activeNT.GetCount());
		if ((int)nameID == activeNT.GetCount()) {
			char*& newName = activeNT.Grow();
			u16 nameLen;
			m_Stream->ReadShort(&nameLen, 1);
			newName = rage_new char[nameLen+1];
			m_Stream->Read(newName, nameLen);
			newName[nameLen] = '\0';

			if (!isAttribute)
			{
				u32 newNameHash = atStringHash(newName);
				if (Unlikely(newNameHash == 0x671ed6fb)) // "xi:include"
				{
					m_HasXInclude = true;
				}
			}
		}
		name = activeNT[nameID];
	}
}

void parStreamInRbf::ReadAttributes(parElement& elt)
{
	char* dataBuf = (char*)alloca(MAX_ATTRIBUTE_VALUE_LENGTH);

	u16 unused;
	m_Stream->ReadShort(&unused, 1);
	m_Stream->ReadShort(&unused, 1);

	u16 numAttributes;
	m_Stream->ReadShort(&numAttributes, 1);

	elt.GetAttributes().Reserve(numAttributes);

	for(int i =0; i < numAttributes; i++)
	{
		// read attributes
		u16 attrID;
		u16 attrFlags;
		u32 attrNameID;
		const char* attrName;
		ReadFullRecordID(attrID, attrFlags, attrNameID, attrName, true);
		switch(attrFlags)
		{
		case STRING_RECORD:
			{
				u16 stringLen;
				m_Stream->ReadShort(&stringLen, 1);
				m_Stream->Read(dataBuf, stringLen);
				dataBuf[stringLen] = '\0';
				elt.AddAttribute(attrName, dataBuf, !m_NameTable, true);
				if (!strcmp(attrName, "content")) {
					m_LastEncoding = FindEncoding(dataBuf);
				}
			}
			break;
		case INT_RECORD:
			{
				int value;
				m_Stream->ReadInt(&value, 1);
				elt.AddAttribute(attrName, value, !m_NameTable);
			}
			break;
		case BOOL_TRUE_RECORD:
			{
				elt.AddAttribute(attrName, true, !m_NameTable);
			}
			break;
		case BOOL_FALSE_RECORD:
			{
				elt.AddAttribute(attrName, false, !m_NameTable);
			}
			break;
		case FLOAT_RECORD:
			{
				float value;
				m_Stream->ReadFloat(&value, 1);
				elt.AddAttribute(attrName, value, !m_NameTable);
			}
			break;
		default:
			parErrorf("Unknown attribute record %d", attrFlags);
			break;
		}
	}

	if (!GetSettings().GetFlag(parSettings::NEVER_SORT_ATTR_LIST) && elt.GetAttributes().GetCount() > 4) {
		elt.SortAttributes();
	}

}

bool parStreamInRbf::ReadElementInternal(parElement& elt, u16 recordFlags)
{
	m_LastEncoding = UNSPECIFIED;

	switch(recordFlags) {
	case CONTAINER_RECORD:
		{
			ReadAttributes(elt);
			return false;
		}
		/*NOTREACHED*/
		break;
	case INT_RECORD:
		{
			int value;
			m_Stream->ReadInt(&value, 1);
			elt.AddAttribute("value", value, false);
			return true;
		}
		/*NOTREACHED*/
		break;
	case BOOL_TRUE_RECORD:
		{
			elt.AddAttribute("value", true, false);
			return true;
		}
		/*NOTREACHED*/
		break;
	case BOOL_FALSE_RECORD:
		{
			elt.AddAttribute("value", false, false);
			return true;
		}
		/*NOTREACHED*/
		break;
	case FLOAT_RECORD:
		{
			float value;
			m_Stream->ReadFloat(&value, 1);
			elt.AddAttribute("value", value, false);
			return true;
		}
		/*NOTREACHED*/
		break;
	case VECTOR3_RECORD:
		{
			float value[3];
			m_Stream->ReadFloat(value, 3);
			elt.GetAttributes().Reserve(3);
			elt.AddAttribute("x", value[0], false);
			elt.AddAttribute("y", value[1], false);
			elt.AddAttribute("z", value[2], false);
			return true;
		}
	}
	return false;
}

void parStreamInRbf::ReadBeginElement(parElement& outElement)
{
	FastAssert(m_Stream);
	u16 recordID;
	u16 recordFlags;
	u32 nameID;
	const char* name;
	ReadFullRecordID(recordID, recordFlags, nameID, name, false);
	parAssertf(recordID != POP_RECORD_ID && recordID != DATA_CONTAINER_RECORD_ID && recordID != COMMENT_RECORD_ID, "Unexpected record id %d", recordID);

	outElement.SetName(name, !m_NameTable);
	m_JustReadLeaf = ReadElementInternal(outElement, recordFlags);
	m_JustBeganElement = true;
}

void parStreamInRbf::ReadEndElement()
{
	FastAssert(m_Stream);
	if (m_JustReadLeaf) 
	{
		return;
	}
	u16 recordID;
	u16 recordFlags;
	u32 nameID;
	const char* name;
	ReadFullRecordID(recordID, recordFlags, nameID, name, false);

	parAssertf(recordID == POP_RECORD_ID, "Unexpected record id %d", recordID);

	m_JustReadLeaf = false;
	m_DataRemaining = 0;
	m_ReadingData = false;
	m_JustBeganElement = false;
	m_LastEncoding = UNSPECIFIED;
}

void parStreamInRbf::ReadLeafElement(parElement& outElement)
{
	FastAssert(m_Stream);
	ReadBeginElement(outElement);
	ReadEndElement();

	m_JustReadLeaf = false;
	m_DataRemaining = 0;
	m_ReadingData = false;
	m_JustBeganElement = false;
	m_LastEncoding = UNSPECIFIED;
}

void parStreamInRbf::SkipElement()
{
	// kind of a pain. have to process each element since there's no good way to skip
	parElement elt;

	int depth = 1;


	while (depth > 0) {
		u16 recordID;
		u16 recordFlags;
		u32 nameID;
		const char* name;
		ReadFullRecordID(recordID, recordFlags, nameID, name, false);
		
		switch(recordID) {
		case POP_RECORD_ID:
			depth--;
			break;
		case DATA_CONTAINER_RECORD_ID:
		case COMMENT_RECORD_ID:
			SkipSizedBlock();
			break;
		default:
			{
				bool leaf = ReadElementInternal(elt, recordFlags);
				if (!leaf) {
					depth++;
				}
			}
		}
	}

	m_JustReadLeaf = false;
	m_DataRemaining = 0;
	m_ReadingData = false;
	m_JustBeganElement = false;
	m_LastEncoding = UNSPECIFIED;
}

int parStreamInRbf::ReadData(char* data, int maxSize, DataEncoding enc)
{
	FastAssert(m_Stream);
	if (enc == UNSPECIFIED) {
		enc = m_LastEncoding;
	}

	parAssertf(m_ReadingData || m_JustBeganElement, "Parser doesn't support mixed content, an element can have either child nodes or text content, not both");

	if (!m_ReadingData) {
		u16 recordID;
		u16 recordFlags;
		u32 nameID;
		const char* name;
		ReadFullRecordID(recordID, recordFlags, nameID, name, false);

		parAssertf(recordID == DATA_CONTAINER_RECORD_ID, "Unexpected record id %d", recordID);
		m_ReadingData = true;
		m_Stream->ReadInt(&m_DataRemaining, 1);
	}

	int amtToRead = PAR_MIN((u32)maxSize, m_DataRemaining);

	switch(enc)
	{
	case BINARY:
		m_Stream->Read(data, amtToRead);
		break;
	case UNSPECIFIED:
	case ASCII:
	case RAW_XML:
		// use a different amtToRead for ascii (since we add a \0 at the end)
		amtToRead = PAR_MIN((u32)maxSize-1, m_DataRemaining);
		m_Stream->Read(data, amtToRead);
		if ((u32)amtToRead == m_DataRemaining) {
			data[amtToRead] = '\0';
			amtToRead++;
			m_DataRemaining++; // because we subtract off amtToRead later - this cancels out the extra ++ above
		}
		break;
	case UTF16:
		// use a different amtToRead for utf16 (since we add a \0 at the end)
		amtToRead = PAR_MIN((u32)maxSize-2, m_DataRemaining);
		FastAssert(amtToRead % sizeof(u16) == 0);
		m_Stream->ReadShort(reinterpret_cast<short*>(data), amtToRead / sizeof(u16));
		if ((u32)amtToRead == m_DataRemaining) {
			data[amtToRead] = '\0';
			amtToRead++;
			m_DataRemaining++; // because we subtract off amtToRead later - this cancels out the extra ++ above
			data[amtToRead] = '\0';
			amtToRead++;
			m_DataRemaining++; // because we subtract off amtToRead later - this cancels out the extra ++ above
		}
		break;
	case INT_ARRAY:
		FastAssert(amtToRead % sizeof(int) == 0);
		m_Stream->ReadInt(reinterpret_cast<int*>(data), amtToRead / sizeof(int));
		break;
	case CHAR_ARRAY:
		FastAssert(amtToRead % sizeof(char) == 0);
		m_Stream->ReadByte(reinterpret_cast<char*>(data), amtToRead / sizeof(char));
		break;
	case SHORT_ARRAY:
		FastAssert(amtToRead % sizeof(short) == 0);
		m_Stream->ReadShort(reinterpret_cast<short*>(data), amtToRead / sizeof(short));
		break;
	case FLOAT_ARRAY:
		FastAssert(amtToRead % sizeof(float) == 0);
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case VECTOR2_ARRAY:
		FastAssert(amtToRead % sizeof(Vector2) == 0);
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case VEC2V_ARRAY:
		FastAssert(amtToRead % sizeof(Vec2V) == 0);
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case VECTOR3_ARRAY:
		FastAssert(amtToRead % sizeof(Vector3) == 0);
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case VECTOR4_ARRAY:
		FastAssert(amtToRead % sizeof(Vector4) == 0);
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case MATRIX33:
		FastAssert(amtToRead == sizeof(Mat33V));
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case MATRIX34:
		FastAssert(amtToRead == sizeof(Mat34V));
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case MATRIX43:
		FastAssert(amtToRead == 12 * sizeof(float));
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case MATRIX44:
		FastAssert(amtToRead == sizeof(Mat44V));
		m_Stream->ReadFloat(reinterpret_cast<float*>(data), amtToRead / sizeof(float));
		break;
	case INT64_ARRAY:
		FastAssert(amtToRead % sizeof(s64) == 0);
		m_Stream->ReadLong(reinterpret_cast<s64*>(data), amtToRead / sizeof(s64));
		break;
	case DOUBLE_ARRAY:
		FastAssert(amtToRead % sizeof(double) == 0);
		m_Stream->ReadDouble(reinterpret_cast<double*>(data), amtToRead / sizeof(double));
		break;
	}

	m_DataRemaining -= amtToRead;

	return amtToRead;
}

bool parStreamInRbf::ReadWithCallbacks()
{
	FastAssert(m_Stream);
	u16 recordID;
	u16 recordFlags;
	u32 nameID;
	const char* name;

	int depth = 0;

	bool done = false;

	char* dataBuf = (char*)alloca(sizeof(char) * m_DataChunkSize);

	while(!done)
	{
		ReadFullRecordID(recordID, recordFlags, nameID, name, false);

		switch(recordID)
		{
		case POP_RECORD_ID:
			m_EndElementCB(false);
			depth--;
			if (depth <= 0) {
				done = true;
			}
			break;
		case DATA_CONTAINER_RECORD_ID:
			{
				m_Stream->ReadInt(&m_DataRemaining, 1);

				m_ReadingData = true;

				while(m_DataRemaining > 0)
				{
					bool finalChunk = ((int)m_DataRemaining <= m_DataChunkSize);
					int bytesRead = ReadData(dataBuf, m_DataChunkSize, UNSPECIFIED);
					if (bytesRead == 0)
					{
						parErrorf("Couldn't read the full contents of record '%s'. File was truncated?", name);
						return false;
					}
//					u32 bytesRead = m_Stream->Read(dataBuf, PAR_MIN<int>(dataRemaining, m_DataChunkSize));
					m_DataCB(dataBuf, bytesRead, !finalChunk);
				}

				m_ReadingData = false;
				m_LastEncoding = UNSPECIFIED;
			}
			break;
		case COMMENT_RECORD_ID:
			{
				SkipSizedBlock();
			}
			break;
		default:
			{
				parElement elt;
				elt.SetName(name, !m_NameTable);

				bool leaf = ReadElementInternal(elt, recordFlags);

				m_BeginElementCB(elt, leaf);
				if (leaf) {
					m_EndElementCB(true);
				}
				else {
					depth++;
				}
			}
		}
	} 

	return true;
}






parStreamOutRbf::parStreamOutRbf(fiStream* s, const parSettings& settings)
: parStreamOut(s, settings)
, m_LastEncoding(UNSPECIFIED)
, m_DataSizeLocation(0)
, m_WritingData(false)
, m_DataSize(0)
{

}

parStreamOutRbf::~parStreamOutRbf()
{
}


void parStreamOutRbf::WriteHeader(parHeaderInfo&)
{
	if (m_WroteHeader)
	{
		return;
	}
	FastAssert(m_Stream);
	m_Stream->Write("RBF0", 4);
	m_WroteHeader = true;
}

void parStreamOutRbf::WriteComment(const char* comment)
{
	FastAssert(m_Stream);
	FinishData();
	int len = (int)strlen(comment);
	u16 recordID = COMMENT_RECORD_ID;
	m_Stream->WriteShort(&recordID, 1);
	m_Stream->WriteInt(&len, 1);
	m_Stream->Write(comment, len);
}

void parStreamOutRbf::FinishData()
{
	if (!m_WritingData) {
		return;
	}
	size_t currPos = m_Stream->Tell();

	// TODO: Change casts if m_Stream->Tell and Seek use size_t
	m_Stream->Seek((int)m_DataSizeLocation);
	m_Stream->WriteInt(&m_DataSize, 1);
	m_Stream->Seek((int)currPos);

	m_WritingData = false;
}

void parStreamOutRbf::WriteDataInternal(const char* data, int size, DataEncoding enc)
{
	if (!data) {
		return;
	}

	if (Unlikely(!GetSettings().GetFlag(parSettings::PROCESS_SPECIAL_ATTRS)))
	{
		enc = UNSPECIFIED;
	}

	switch(enc) {
	case UNSPECIFIED:
	case ASCII:
	case BINARY:
	case RAW_XML:
		m_Stream->Write(data, size);
		break;
	case UTF16:
		FastAssert(size % sizeof(u16) == 0);
		m_Stream->WriteShort(reinterpret_cast<const short*>(data), size / sizeof(u16));
		break;
	case INT_ARRAY:
		FastAssert(size % sizeof(int) == 0);
		m_Stream->WriteInt(reinterpret_cast<const int*>(data), size / sizeof(int));
		break;
	case CHAR_ARRAY:
		FastAssert(size % sizeof(char) == 0);
		m_Stream->WriteByte(reinterpret_cast<const char*>(data), size / sizeof(char));
		break;
	case SHORT_ARRAY:
		FastAssert(size % sizeof(short) == 0);
		m_Stream->WriteShort(reinterpret_cast<const short*>(data), size / sizeof(short));
		break;
	case FLOAT_ARRAY:
		FastAssert(size % sizeof(float) == 0);
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case VECTOR2_ARRAY:
		FastAssert(size % sizeof(Vector2) == 0);
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case VEC2V_ARRAY:
		FastAssert(size % sizeof(Vec2V) == 0);
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case VECTOR3_ARRAY:
		FastAssert(size % sizeof(Vector3) == 0);
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case VECTOR4_ARRAY:
		FastAssert(size % sizeof(Vector4) == 0);
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case MATRIX33:
		FastAssert(size == sizeof(Mat33V));
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case MATRIX34:
		FastAssert(size == sizeof(Mat34V));
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case MATRIX43:
		FastAssert(size == 12 * sizeof(float));
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case MATRIX44:
		FastAssert(size == sizeof(Mat44V));
		m_Stream->WriteFloat(reinterpret_cast<const float*>(data), size / sizeof(float));
		break;
	case INT64_ARRAY:
		FastAssert(size % sizeof(int) == 0);
		m_Stream->WriteLong(reinterpret_cast<const s64*>(data), size / sizeof(int));
		break;
	case DOUBLE_ARRAY:
		FastAssert(size % sizeof(int) == 0);
		m_Stream->WriteDouble(reinterpret_cast<const double*>(data), size / sizeof(int));
		break;
	}
}

void parStreamOutRbf::WriteData(const char* data, int size, bool completeData, DataEncoding enc, WriteDataOptions* UNUSED_PARAM(options) /* = NULL */)
{
	FastAssert(m_Stream);
	if (enc == UNSPECIFIED)
	{
		enc = m_LastEncoding;
	}

	if (size < 0) {
		if (enc == ASCII) {
			if (data) {
				size = (int)strlen(data);
			}
		}
		else if (enc == UTF16) {
			if (data) {
				size = (int)wcslen(reinterpret_cast<const char16*>(data)) * sizeof(char16);
			}
		}
		else {
			parAssertf(0, "Size < 0 only allowed if data is ASCII or UTF16");
			return;
		}
	}

	if (!data) {
		size = 0;
	}

	if (!m_WritingData && completeData) {
		// use the fast write
		u16 recordID = DATA_CONTAINER_RECORD_ID;
		m_Stream->WriteShort(&recordID, 1);
		m_Stream->WriteInt(&size, 1);
		WriteDataInternal(data, size, enc);
		return;
	}

	// use the slow, seekey write.
	if (!m_WritingData) {
		u16 recordID = DATA_CONTAINER_RECORD_ID;
		m_Stream->WriteShort(&recordID, 1);
		
		m_DataSizeLocation = m_Stream->Tell();

		u32 sizeGoesHere = 0xCDCDCDCD;
		m_Stream->WriteInt(&sizeGoesHere, 1);

		m_WritingData = true;
		m_DataSize = 0;
	}
	WriteDataInternal(data, size, enc);
	m_DataSize += size;
}

void parStreamOutRbf::WriteRecordID(u16 flags, const char* name)
{
	u32* currID = m_NameMap.Access(name);
	u32 nameID;
	const char* newName = NULL;
	u16 newNameLen = 0;
	if (currID) 
	{
		nameID = *currID;
	}
	else
	{
		nameID = m_NameMap.GetNumUsed();
		newName = name;
		newNameLen = (u16)strlen(newName);
		m_NameMap[ConstString(name)] = nameID;
	}

	if (nameID < (1 << 13)) {
		// write regular ID
		u16 recordID = (flags << 12);
		recordID |= (u16)nameID;
		m_Stream->WriteShort(&recordID, 1);
	}
	else {
		u16 recordID = EXTENDED_RECORD_ID;
		u32 extRec = (flags << 28);
		extRec |= nameID;
		m_Stream->WriteShort(&recordID, 1);
		m_Stream->WriteInt(&extRec, 1);
	}

	if (newName != NULL)
	{
		m_Stream->WriteShort(&newNameLen, 1);
		m_Stream->Write(newName, newNameLen);
	}
}

void parStreamOutRbf::WriteBeginElement(parElement& elt)
{
	FastAssert(m_Stream);
	parAssertf(elt.GetName(), "Can't write unnamed element");

	FinishData();

	m_LastEncoding = UNSPECIFIED;

	WriteRecordID(CONTAINER_RECORD, elt.GetName());

	// don't store version info here anymore
	u16 zero = 0;
	m_Stream->WriteShort(&zero, 1);
	m_Stream->WriteShort(&zero, 1);

	u16 numAttrs = (u16)elt.GetAttributes().GetCount();
	m_Stream->WriteShort(&numAttrs, 1);

	for(int i = 0; i < elt.GetAttributes().GetCount(); i++)
	{
		parAttribute& attr = elt.GetAttributes()[i];
		switch(attr.GetType())
		{
		case parAttribute::STRING:
			{
				WriteRecordID(STRING_RECORD, attr.GetName());
				u16 len = 0;
				if (attr.GetStringValue()) {
					len = (u16)strlen(attr.GetStringValue());
				}
				m_Stream->WriteShort(&len, 1);
				if (len) {
					m_Stream->Write(attr.GetStringValue(), len);
				}
				if (!strcmp(attr.GetName(), "content")) {
					m_LastEncoding = FindEncoding(attr.GetStringValue());
				}
			}
			break;
		case parAttribute::BOOL:
			{
				if (attr.GetBoolValue())
				{
					WriteRecordID(BOOL_TRUE_RECORD, attr.GetName());
				}
				else
				{
					WriteRecordID(BOOL_FALSE_RECORD, attr.GetName());
				}
			}
			break;
		case parAttribute::INT64:
			{
				WriteRecordID(INT_RECORD, attr.GetName());
				u32 val = attr.GetIntValue();
				m_Stream->WriteInt(&val, 1);
			}
			break;
		case parAttribute::DOUBLE:
			{
				WriteRecordID(FLOAT_RECORD, attr.GetName());
				float val = attr.GetFloatValue();
				m_Stream->WriteFloat(&val, 1);
			}
			break;
		}
	}
}

void parStreamOutRbf::WriteEndElement(parElement& /*elt*/)
{
	FastAssert(m_Stream);
	FinishData();

	u16 recordID = POP_RECORD_ID;
	m_Stream->WriteShort(&recordID, 1);

	m_LastEncoding = UNSPECIFIED;
}

void parStreamOutRbf::WriteLeafElement(parElement& elt)
{
	FastAssert(m_Stream);
	FinishData();

	m_LastEncoding = UNSPECIFIED;

	// write all elements as normal container elements for now.
	// In the future, check that the element meets the requirements for
	// output as a special typed element.
	
	if (elt.GetAttributes().GetCount() == 1) {
		parAttribute* attr = elt.FindAttribute("value");
		if (attr) {
			switch(attr->GetType()) {
				case parAttribute::INT64:
					{
						WriteRecordID(INT_RECORD, elt.GetName());
						u32 val = attr->GetIntValue();
						m_Stream->WriteInt(&val, 1);
						return;
					}
					/*NOTREACHED*/
					break;
				case parAttribute::BOOL:
					{
						if (attr->GetBoolValue()) {
							WriteRecordID(BOOL_TRUE_RECORD, elt.GetName());
						}
						else {
							WriteRecordID(BOOL_FALSE_RECORD, elt.GetName());
						}
						return;
					}
					/*NOTREACHED*/
					break;
				case parAttribute::DOUBLE:
					{
						WriteRecordID(FLOAT_RECORD, elt.GetName());
						float val = attr->GetFloatValue();
						m_Stream->WriteFloat(&val, 1);
						return;
					}
					/*NOTREACHED*/
					break;
				default:
					break;
			}
		}
	}
	else if (elt.GetAttributes().GetCount() == 3) {
		parAttribute* xAttr = elt.FindAttribute("x");
		if (xAttr) {
			parAttribute* yAttr = elt.FindAttribute("y");
			if (yAttr) {
				parAttribute* zAttr = elt.FindAttribute("z");
				if (zAttr && 
					xAttr->GetType() == parAttribute::DOUBLE &&
					yAttr->GetType() == parAttribute::DOUBLE &&
					zAttr->GetType() == parAttribute::DOUBLE) {
					float vals[3];
					vals[0] = xAttr->GetFloatValue();
					vals[1] = yAttr->GetFloatValue();
					vals[2] = zAttr->GetFloatValue();
					WriteRecordID(VECTOR3_RECORD, elt.GetName());
					m_Stream->WriteFloat(vals, 3);
					return;
				}
			}
		}
	}

	WriteBeginElement(elt);
	WriteEndElement(elt);

}
