<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::parAttributeList" simple="true" onPostSave="OnSave" onPreLoad="OnLoad">
	<pad bytes="8"/> <!-- attributes array -->
	<u8 name="m_UserData1"/>
	<u8 name="m_UserData2"/>
	<pad bytes="2"/> <!-- m_flags -->
</structdef>

</ParserSchema>