// 
// parsercore/parstream.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "stream.h"

#include "parser/macros.h"
#include "parser/optimisations.h"

#include "file/stream.h"

PARSER_OPTIMISATIONS();

using namespace rage;

parStream::~parStream()
{
}

void parStream::Close()
{
	m_Stream->Close();
	m_Stream = NULL;
}

const char* g_Encodings[] = 
{
	NULL,
	"ascii",		
	"utf16",		
	"binary",		
	"raw_xml",		
	"int_array",	
	"char_array",	
	"short_array",	
	"float_array",	
	"vector2_array",
	"vec2v_array",
	"vector3_array",
	"vector4_array",
	"matrix33",
	"matrix34",
	"matrix43",
	"matrix44",
	"int64_array",
	"double_array",
};

const char* parStream::FindEncodingName(parStream::DataEncoding enc)
{
	return g_Encodings[(int)enc];
}

parStream::DataEncoding parStream::FindEncoding(const char* enc)
{
	// Note: skip UNSPECIFIED
	for(int i = 1; i < NUM_ENCODINGS; i++)
	{
		if (!strcmp(enc, g_Encodings[i]))
		{
			return (DataEncoding)i;
		}
	}

	if (!strcmp(enc, "text"))
	{
		return ASCII;
	}

	return UNSPECIFIED;
}

void parStream::SetSettings(const parSettings& settings)
{
	parAssertf(m_Settings.GetFlag(parSettings::USE_SMALL_DATA_CHUNK_SIZE) == settings.GetFlag(parSettings::USE_SMALL_DATA_CHUNK_SIZE), "Can't change the data chunk size after the stream has been constructed");
	m_Settings = settings;
}
