// 
// audiodata/containerbuilder.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef ADAT_CONTAINERBUILDER_H
#define ADAT_CONTAINERBUILDER_H

#define AUD_CONTAINERBUILDER ( __TOOL ) //__WIN32PC && __BANK)

#if AUD_CONTAINERBUILDER

#include <stdlib.h> // for the array qsort
#include "atl/array.h"
#include "file/stream.h"
#include "system/endian.h"
#include "container.h"

namespace rage
{

	struct adatContainerBuilderChunk
	{
		u32 typeNameHash;
		u8 *data;
		u32 sizeBytes;
		u32 alignment;

		u32 calculatedOffset;
	};

	struct adatContainerBuilderObject
	{
		u32 nameHash;
		atArray<adatContainerBuilderChunk *> dataChunks;
	};

	struct adatChunkObjectIDPair
	{
		adatObjectId objectID;
		adatContainerBuilderChunk* dataChunk;
	};

	class adatContainerBuilder
	{
	public:
		adatContainerBuilder(adatContainer::PackingType packingType);
		~adatContainerBuilder();

		// PURPOSE
		// Add an object to the container
		// RETURNS
		//	The ID of the newly added object, audContainer::InvalidId on failure
		// NOTES
		//	This function will fail if an object already exists in this container with the same name
		adatObjectId AddObject(const u32 objectNameHash);

		// PURPOSE
		//	Removes the specified object from the container
		// RETURNS
		//	true if the object was successfully removed, false on failure
		bool DeleteObject(const adatObjectId objectId);

		// PURPOSE
		//	Returns the id of the specified object, adatContainer::InvalidId if the object was not found
		//	in the container.
		adatObjectId FindObject(const u32 objectNameHash);

		// PURPOSE
		//	Add a data chunk to an object
		// RETURNS
		//	The ID of the newly added data chunk, audContainer::InvalidId on failure
		// NOTES
		//	This function will fail if the specified object already has a data chunk with the specified dataTypeHash
		//	The supplied data is duplicated so callee is free to reuse the memory pointed to by data
		adatObjectDataChunkId AddDataChunk(const adatObjectId objectId, const u32 dataTypeHash, const u8 *data, const u32 dataSizeBytes, const u32 alignment = 1);

		// PURPOSE
		//	Returns the chunk id for the specified object data chunk, adatContainer::InvalidId if not found
		adatObjectDataChunkId FindDataChunk(const adatObjectId objectId, const u32 dataTypeHash) const;

		// PURPOSE
		//	Sets the alignment for the specified chunk
		void SetDataChunkAlignment(const adatObjectId objectId, const adatObjectDataChunkId chunkId, const u32 alignment);

		// PURPOSE
		//	Removes the specified data chunk from the specified object
		// RETURNS
		//	true if the data chunk was successfully removed, false otherwise
		bool DeleteDataChunk(const adatObjectId objectId, const adatObjectDataChunkId chunkId);

		// PURPOSE
		//	Populates this container builder from a prebuilt binary container
		// RETURNS
		//	true if the specified container data was loaded into this container
		// NOTES
		//	The specified container must be fully loaded into memory before this function is called
		bool PopulateFromPrebuiltContainer(const adatContainer *const container);

		// PURPOSE
		//	Serialises this container to the specified stream
		// PARAMS
		//	stream - the stream to write to
		//	outputBigEndian - if true then output will be in big endian format, otherwise it will be in little endian
		// RETURNS
		//	true on success, false on failure
		bool Serialize(fiStream *stream, const bool outputBigEndian);

		// PURPOSE
		//	Returns the size in bytes of the container header
		// NOTES
		//	Header size is defined as sizeof(adatContainerHeader) + (sizeof(adatContainerObjectTableEntry) * numObjects)
		//		+ (sizeof(adatContainerObjectDataTableEntry) * numDataChunks)
		u32 ComputeHeaderSize() const;

		// PURPOSE
		//	Returns the number of objects in this container
		u32 GetNumObjects() const;

		// PURPOSE
		//	Returns the number of data chunks associated with the specified object
		u32 GetNumDataChunks(const adatObjectId objectId) const;

		// PURPOSE
		//	Set the 'encrypted data' flag for this container
		void SetEncryptedData(const bool encrypted) { m_EncryptedData = encrypted; }

	private:

		u32 CountNumObjects() const;
		u32 CountNumDataChunks(adatContainerBuilderObject *object) const;

		template<class _T> static void ConditionalSwap(_T &val, const bool swap)
		{
			if(swap)
			{
				val = sysEndian::Swap(val);
			}
		}

		bool SerializeDataOptimized(fiStream *stream, const u32 dataOffset, const bool outputBigEndian, const bool isStreamingBank);
		bool SerializeDataNormal(fiStream *stream, const u32 dataOffset, const bool outputBigEndian);

		void Reset();

		atArray<adatContainerBuilderObject *> m_Objects;
		adatContainer::PackingType m_PackingType;
		bool m_HasFastLookupTable;
		bool m_EncryptedData;
	};

} // namespace rage

#endif // #if AUD_CONTAINERBUILDER
#endif // ADAT_CONTAINERBUILDER_H
