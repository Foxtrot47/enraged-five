// 
// simpletypes.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure definitions - do not edit.
// 

#ifndef AUD_SIMPLETYPES_H
#define AUD_SIMPLETYPES_H

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "audioengine/metadataref.h"

// macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
	#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
	#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) (TristateValue)((flagvar |= ((trival&0x03) << (flagid<<1))))
	namespace rage
	{
		enum TristateValue
		{
			AUD_TRISTATE_FALSE = 0,
			AUD_TRISTATE_TRUE,
			AUD_TRISTATE_UNSPECIFIED,
		}; // enum TristateValue
	} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE

namespace ravesimpletypes
{
	#define SIMPLETYPES_SCHEMA_VERSION 4
	
	#define SIMPLETYPES_METADATA_COMPILER_VERSION 2
	
	// NOTE: doesn't include abstract objects
	#define AUD_NUM_SIMPLETYPES 11
	
	#define AUD_DECLARE_SIMPLETYPES_FUNCTION(fn) void* GetAddressOf_##fn(rage::u8 classId);
	
	#define AUD_DEFINE_SIMPLETYPES_FUNCTION(fn) \
	void* GetAddressOf_##fn(rage::u8 classId) \
	{ \
		switch(classId) \
		{ \
			case Int::TYPE_ID: return (void*)&Int::s##fn; \
			case UnsignedInt::TYPE_ID: return (void*)&UnsignedInt::s##fn; \
			case Float::TYPE_ID: return (void*)&Float::s##fn; \
			case String::TYPE_ID: return (void*)&String::s##fn; \
			case StringHash::TYPE_ID: return (void*)&StringHash::s##fn; \
			case Vector3::TYPE_ID: return (void*)&Vector3::s##fn; \
			case Vector4::TYPE_ID: return (void*)&Vector4::s##fn; \
			case VariableList::TYPE_ID: return (void*)&VariableList::s##fn; \
			case Slot::TYPE_ID: return (void*)&Slot::s##fn; \
			case WaveSlots::TYPE_ID: return (void*)&WaveSlots::s##fn; \
			case ERSettings::TYPE_ID: return (void*)&ERSettings::s##fn; \
			default: return NULL; \
		} \
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gAudioConfigGetBaseTypeId(const rage::u32 classId);
	
	// PURPOSE - Determines if a type inherits from another type
	bool gAudioConfigIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId);
	
	// PURPOSE - Determines if a type inherits from another type
	template<class _ObjectType>
	bool gAudioConfigIsOfType(const _ObjectType* const obj, const rage::u32 baseTypeId)
	{
		return gAudioConfigIsOfType(obj->ClassId, baseTypeId);
	}
	
	// 
	// Enumerations
	// 
	enum SlotLoadType
	{
		SLOT_LOAD_TYPE_BANK = 0,
		SLOT_LOAD_TYPE_WAVE,
		SLOT_LOAD_TYPE_STREAM,
		NUM_SLOTLOADTYPE,
		SLOTLOADTYPE_MAX = NUM_SLOTLOADTYPE,
	}; // enum SlotLoadType
	const char* SlotLoadType_ToString(const SlotLoadType value);
	SlotLoadType SlotLoadType_Parse(const char* str, const SlotLoadType defaultValue);
	
	// 
	// Int
	// 
	
	struct Int
	{
		static const rage::u32 TYPE_ID = 0;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		Int() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA),
			Value(0)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		rage::s32 Value;
	}; // struct Int
	
	// 
	// UnsignedInt
	// 
	
	struct UnsignedInt
	{
		static const rage::u32 TYPE_ID = 1;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		UnsignedInt() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA),
			Value(0U)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		rage::u32 Value;
	}; // struct UnsignedInt
	
	// 
	// Float
	// 
	
	struct Float
	{
		static const rage::u32 TYPE_ID = 2;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		Float() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA),
			Value(0.0f)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		float Value;
	}; // struct Float
	
	// 
	// String
	// 
	
	struct String
	{
		static const rage::u32 TYPE_ID = 3;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		String() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		char Value[64];
	}; // struct String
	
	// 
	// StringHash
	// 
	
	struct StringHash
	{
		static const rage::u32 TYPE_ID = 4;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		StringHash() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA),
			Value(0U)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		rage::u32 Value;
	}; // struct StringHash
	
	// 
	// Vector3
	// 
	
	struct Vector3
	{
		static const rage::u32 TYPE_ID = 5;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		Vector3() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		
		rage::Vector3 Value;
		
	}; // struct Vector3
	
	// 
	// Vector4
	// 
	
	struct Vector4
	{
		static const rage::u32 TYPE_ID = 6;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		Vector4() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		
		rage::Vector4 Value;
		
	}; // struct Vector4
	
	// 
	// VariableList
	// 
	
	struct VariableList
	{
		static const rage::u32 TYPE_ID = 7;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		VariableList() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		
		static const rage::u32 MAX_VARIABLES = 255;
		rage::u32 numVariables;
		struct tVariable
		{
			rage::u32 Name;
			float InitialValue;
		} Variable[MAX_VARIABLES]; // struct tVariable
		
	}; // struct VariableList
	
	// 
	// Slot
	// 
	
	struct Slot
	{
		static const rage::u32 TYPE_ID = 8;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		Slot() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA),
			LoadType(SLOT_LOAD_TYPE_BANK),
			padding0(0),
			padding1(0),
			padding2(0),
			MaxHeaderSize(0U),
			Size(0U),
			StaticBank(1849449579U), // 0
			MaxMetadataSize(0U),
			MaxDataSize(0U)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		rage::u8 LoadType;
		rage::u8 padding0;
		rage::u8 padding1;
		rage::u8 padding2;
		rage::u32 MaxHeaderSize;
		rage::u32 Size;
		rage::u32 StaticBank;
		rage::u32 MaxMetadataSize;
		rage::u32 MaxDataSize;
	}; // struct Slot
	
	// 
	// WaveSlots
	// 
	
	struct WaveSlots
	{
		static const rage::u32 TYPE_ID = 9;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		WaveSlots() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		
		static const rage::u32 MAX_SLOTREFS = 255;
		rage::u32 numSlotRefs;
		struct tSlotRef
		{
			rage::audMetadataRef SlotNameHash;
		} SlotRef[MAX_SLOTREFS]; // struct tSlotRef
		
	}; // struct WaveSlots
	
	// 
	// ERSettings
	// 
	
	struct ERSettings
	{
		static const rage::u32 TYPE_ID = 10;
		static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
		
		ERSettings() :
			ClassId(0xFF),
			NameTableOffset(0XFFFFFF),
			Flags(0xAAAAAAAA),
			RoomSize(12.0f)
		{}
		
		// PURPOSE - Returns a pointer to the field whose name is specified by the hash
		void* GetFieldPtr(const rage::u32 fieldNameHash);
		
		rage::u32 ClassId : 8;
		rage::u32 NameTableOffset : 24;
		rage::u32 Flags;
		
		float RoomSize;
		
		struct tRoomDimensions
		{
			float widthRatio;
			float lengthRatio;
			float heightRatio;
		} RoomDimensions; // struct tRoomDimensions
		
		
		struct tListenerPos
		{
			float x;
			float y;
			float z;
		} ListenerPos; // struct tListenerPos
		
		
		static const rage::u8 MAX_ALLPASSES = 4;
		rage::u8 numAllpasses;
		struct tAllpasses
		{
			float Diffusion;
			rage::u32 DelayLength;
		} Allpasses[MAX_ALLPASSES]; // struct tAllpasses
		
		
		struct tNodeGainMatrix
		{
			
			struct tX1
			{
				float FL;
				float FR;
				float RL;
				float RR;
			} X1; // struct tX1
			
			
			struct tX2
			{
				float FL;
				float FR;
				float RL;
				float RR;
			} X2; // struct tX2
			
			
			struct tY1
			{
				float FL;
				float FR;
				float RL;
				float RR;
			} Y1; // struct tY1
			
			
			struct tY2
			{
				float FL;
				float FR;
				float RL;
				float RR;
			} Y2; // struct tY2
			
			
			struct tZ1
			{
				float FL;
				float FR;
				float RL;
				float RR;
			} Z1; // struct tZ1
			
			
			struct tZ2
			{
				float FL;
				float FR;
				float RL;
				float RR;
			} Z2; // struct tZ2
			
		} NodeGainMatrix; // struct tNodeGainMatrix
		
		
		struct tGain_1stOrder
		{
			float FL;
			float FR;
			float RL;
			float RR;
		} Gain_1stOrder; // struct tGain_1stOrder
		
		
		struct tGain_2ndOrder
		{
			float FL;
			float FR;
			float RL;
			float RR;
		} Gain_2ndOrder; // struct tGain_2ndOrder
		
		
		struct tGain_3rdOrder
		{
			float FL;
			float FR;
			float RL;
			float RR;
		} Gain_3rdOrder; // struct tGain_3rdOrder
		
		
		static const rage::u32 MAX_NODELPF_1STORDERS = 6;
		rage::u32 numNodeLPF_1stOrders;
		struct tNodeLPF_1stOrder
		{
			float FL_Cutoff;
			float FR_Cutoff;
			float RL_Cutoff;
			float RR_Cutoff;
		} NodeLPF_1stOrder[MAX_NODELPF_1STORDERS]; // struct tNodeLPF_1stOrder
		
		
		static const rage::u32 MAX_NODELPF_2NDORDERS = 6;
		rage::u32 numNodeLPF_2ndOrders;
		struct tNodeLPF_2ndOrder
		{
			float FL_Cutoff;
			float FR_Cutoff;
			float RL_Cutoff;
			float RR_Cutoff;
		} NodeLPF_2ndOrder[MAX_NODELPF_2NDORDERS]; // struct tNodeLPF_2ndOrder
		
		
		static const rage::u32 MAX_NODELPF_3RDORDERS = 6;
		rage::u32 numNodeLPF_3rdOrders;
		struct tNodeLPF_3rdOrder
		{
			float FL_Cutoff;
			float FR_Cutoff;
			float RL_Cutoff;
			float RR_Cutoff;
		} NodeLPF_3rdOrder[MAX_NODELPF_3RDORDERS]; // struct tNodeLPF_3rdOrder
		
	}; // struct ERSettings
	
} // namespace ravesimpletypes
#endif // AUD_SIMPLETYPES_H
