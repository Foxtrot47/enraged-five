// 
// audiodata/container.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef ADAT_CONTAINER_H
#define ADAT_CONTAINER_H

#include "atl/array.h"
#include "math/amath.h"
#include "system/bit.h"
#include "system/magicnumber.h"
#include "data/bitfield.h"

namespace rage
{

struct adatContainerHeader
{
	// Note: ensure a high bit is set so that perforce's auto-typemap recognises this as a binary file.
	static const u32 VERSION = 0xFF000001;

	// lower 16 bits used for format version
	enum {VersionMask = 0xffff};

	// bits 16 - 24 available for flags
	enum 
	{
		Flag_FastLookupTable =			BIT(16),
		Flag_ContiguousObjectPacking =	BIT(17),
		Flag_StreamingObjectPacking =	BIT(18),
		Flag_EncryptedData			=	BIT(19),
	};

	void Default()
	{
		magic = MAKE_MAGIC_NUMBER('A','D','A','T');
		version = VERSION;
		totalHeaderSize = sizeof(adatContainerHeader);
		numObjects = 0;
	}

	// magic number: either ADAT or TADA depending on endianness
	u32 magic;
	// container format version
	u32 version;
	// number of objects in the object table
	u32 numObjects;
	// total size in bytes of the header / lookup tables
	u32 totalHeaderSize;
};

struct adatContainerObjectTableEntry
{
	// objectNameHash - the hashed name of the object, truncated to 29 bits.  GTA IV had only
	//					4 collisions from 64,600 unique wave names so the chances of getting a collision
	//					in a single bank is small enough to live with.
	// numDataTableEntries - the number of data chunks that make up this object (max of 8)
	enum {kObjectNameHashWidth = 29};
	enum {kObjectNameHashMask = (1<<kObjectNameHashWidth)-1};
	enum {kNumDataTableEntriesWidth = 3};
	enum {kMaxNumDataTableEntries = 1<<kNumDataTableEntriesWidth};
	u32 DECLARE_BITFIELD_2(objectNameHash, kObjectNameHashWidth, numDataTableEntries, kNumDataTableEntriesWidth);
};

struct adatContainerObjectDataTableEntry
{
	// dataOffsetBytes - offset in bytes (256MB limit)
	// sizeBytes - size in bytes (256MB limit)
	// dataTypeHash - the hashed name of this data chunk, truncated to 8bits
	enum {kDataOffsetWidth = 28};
	enum {kMaxChunkDataOffsetBytes = 1<<kDataOffsetWidth};
	enum {kDataSizeBytesWidth = 28};
	enum {kMaxChunkDataSizeBytes = 1<<kDataSizeBytesWidth};
	enum {kDataTypeHashWidth = 8};
	enum {kDataTypeHashMask = (1<<kDataTypeHashWidth)-1};
	u64 DECLARE_BITFIELD_3(dataOffsetBytes, kDataOffsetWidth, sizeBytes, kDataSizeBytesWidth, dataTypeHash, kDataTypeHashWidth);
};

typedef s32 adatObjectId;
typedef s32 adatObjectDataChunkId;

class adatContainer
{
public:
	
	adatContainer(const adatContainerHeader *const header);
	adatContainer() { m_Header = NULL; }

	// PURPOSE
	//	Validate that the data pointed to by header has the correct magic number for an adatContainerHeader
	static bool IsValidHeader(const void *header);

	void SetHeader(const adatContainerHeader *const header)
	{
		m_Header = header;
		if(header)
		{
			Init();
		}
	}

	// PURPOSE
	//	Returns true if this container is in native endian format
	bool IsNativeEndian() const;

	// PURPOSE
	//	Returns true if this container is valid
	bool IsValid() const { return m_Header != NULL; }

	enum {InvalidId = -1};	
	enum PackingType
	{
		OPTIMISED_PACKING, // Optimise for size: sort chunks by chunk alignment
		NORMAL_PACKING, // Enable per object loads: sort chunks by associated object
		STREAM_PACKING, // Sort chunks by chunk alignment, but ensure that the global object data chunk (the
						// interleaved data) is last.
	};

	PackingType GetPackingType() const;

	// PURPOSE
	//	Returns the ID of the requested object, or InvalidId if the object was not found in this container
	adatObjectId FindObject(const u32 objectNameHash) const;
	adatObjectId FindObject(const char *objectName) const;

	// PURPOSE
	//	Returns the ID of the requested object data chunk for the specified object, or InvalidId if the specified
	//	object doesn't have a matching data chunk
	adatObjectDataChunkId FindObjectData(const adatObjectId objectId, const u32 dataTypeHash) const;
	adatObjectDataChunkId FindObjectData(const adatObjectId objectId, const char *dataTypeName) const;

	// PURPOSE
	//	Returns the name hash of the specified object
	u32 GetObjectNameHash(const adatObjectId objectId) const;

	// PURPOSE
	//	Returns the offset and size of the requested data chunk
	// NOTES
	//	dataTypeHash is truncated to 8 bits
	void GetObjectDataChunk(const adatObjectId objectId, const adatObjectDataChunkId dataChunkId, u32 &dataTypeHash, u32 &offsetBytes, u32 &sizeBytes) const;

	// PURPOSE
	//	Returns the number of objects in this container
	u32 GetNumObjects() const;

	// PURPOSE
	//	Returns the size of the complete container header
	u32 GetHeaderSize() const;

	// PURPOSE
	//	Returns the number of data chunks for the specified object
	u32 GetNumDataChunksForObject(const adatObjectId objectId) const;

	// PURPOSE
	//	Returns true if this container has an object data entry lookup table
	bool HasFastLookupTable() const;
	
	// PURPOSE
	//	Returns true if data chunks in this container have been encrypted
	bool IsDataEncrypted() const;

	// PURPOSE
	//	Returns a pointer to data offset 0
	// NOTES
	//	This currently relies on offsets being relative to the start of the header; ie one contiguous block of
	//	mem for header + data.
	const u8 *GetDataPointer() const { return (u8*)m_Header; }
	
	// PURPOSE
	//	Returns size for a single contiguous load of all chunks belonging to the specified object.
	// NOTE
	//	This is only meaningful for containers that are packed in object/chunk order.  It includes any padding
	// between chunks, and as such is not the same as summing the size of each chunk.
	u32 ComputeContiguousObjectSize(const adatObjectId objectId) const;
	
	// PURPOSE
	//	Returns the computed size contiguous metadata segment, which is assumed to be everything other than DATA chunks
	u32 ComputeContiguousMetadataSize() const;

	// PURPOSE
	//	Returns sum of chunk sizes for the specified object
	// NOTE
	//	This does not include padding and as such is only useful for reporting purposes
	u32 ComputeObjectDataSize(const adatObjectId objectId) const;
	
	static void EndianSwap(const adatContainerObjectTableEntry *in, adatContainerObjectTableEntry &out);
	static void EndianSwap(adatContainerObjectTableEntry &inOut)
	{
		EndianSwap(&inOut,inOut);
	}
	static void EndianSwap(const adatContainerObjectDataTableEntry *in, adatContainerObjectDataTableEntry &out);
	static void EndianSwap(adatContainerObjectDataTableEntry &inOut)
	{
		EndianSwap(&inOut,inOut);
	}

#if __TOOL
	static void Copy(const adatContainerObjectTableEntry *in, adatContainerObjectTableEntry &out);
#endif

private:

	void Init();

	u32 GetFirstObjectDataTableEntryIndex(const adatObjectId objectId) const;

	const adatContainerObjectDataTableEntry *FindFirstObjectDataTableEntry(const adatObjectId objectId) const;

	const adatContainerObjectTableEntry *GetObjectTable() const
	{
		Assert(m_Header);
		if(HasFastLookupTable())
		{
			// header | (u16 * numObjects) | objectTable
			return (adatContainerObjectTableEntry*)(((u16 *)(m_Header+1)) + GetNumObjects());
		}
		else		
		{
			// header | objectTable
			return (adatContainerObjectTableEntry*)(m_Header + 1);
		}
	}

	const u16 *GetFastLookupTable() const 
	{
		Assert(m_Header);
		Assert(HasFastLookupTable());
		return (u16*)(m_Header + 1); 
	}

	const adatContainerHeader *m_Header;
};

#if __SPU

class adatContainerSpu
{
public:

	bool Init(const void *containerEA)
	{
		if(m_ContainerPtrEA != (u32)containerEA)
		{
			m_ObjectDataTableFirstEntryId = m_ObjectFastLookupTableEntryId = m_ObjectTableCacheFirstId = ~0U;

			m_ContainerPtrEA = (u32)containerEA;
			sysDmaGetAndWait(&m_Header, m_ContainerPtrEA, sizeof(adatContainerHeader), 6);
			m_Container.SetHeader(&m_Header);
		}

		if(m_Container.IsValid() &&
			m_Container.IsNativeEndian())
		{
			return true;
		}

		m_Container.SetHeader(NULL);

		return false;
	}

	adatObjectId FindObject(const u32 objectNameHash);

	adatObjectDataChunkId FindObjectData(const adatObjectId objectId, const u32 chunkNameHash);
	void GetObjectDataChunk(const adatObjectId objectId, const adatObjectDataChunkId dataChunkId, u32 &dataTypeHash, u32 &offsetBytes, u32 &sizeBytes);

	u32 GetNumObjects() const { return m_Container.GetNumObjects(); }

	u32 GetObjectNameHash(const u32 objectIndex)
	{
		if(!IsObjectTableEntryInCache(objectIndex))
		{
			FetchObjectTableEntry(objectIndex);
		}
		return m_ObjectTableCache[objectIndex - m_ObjectTableCacheFirstId].objectNameHash;
	}

	bool IsValid() const { return m_Container.IsValid(); }

private:
	
	void GetObjectTableEntry(const u32 objectIndex, adatContainerObjectTableEntry &entry);

	bool IsObjectTableEntryInCache(const u32 objectIndex) const;

	void FetchObjectTableEntry(const u32 objectIndex);
	
	u32 GetFastObjectLookupTableEA() const { Assert(m_Container.HasFastLookupTable()); return m_ContainerPtrEA + sizeof(adatContainerHeader); }
	u32 GetObjectTableEA() const 
	{ 
		if(m_Container.HasFastLookupTable())
		{
			return GetFastObjectLookupTableEA() + sizeof(u16) * GetNumObjects(); 
		}
		return m_ContainerPtrEA + sizeof(adatContainerHeader);
	}
	u32 GetObjectDataTableEA() const { return GetObjectTableEA() + sizeof(adatContainerObjectTableEntry) * GetNumObjects(); }

	u32 GetFirstObjectDataTableEntryIndex(const u32 objectIndex);

	const adatContainerObjectDataTableEntry &GetObjectDataTableEntry(const u32 entryIndex);

	enum { kNumObjectTableCacheEntries = 128 };
	enum { kNumObjectDataTableCacheEntries = 8 };

	ALIGNAS(16) adatContainerHeader m_Header ;
	atRangeArray<adatContainerObjectTableEntry, kNumObjectTableCacheEntries> m_ObjectTableCache;
	atRangeArray<adatContainerObjectDataTableEntry, kNumObjectDataTableCacheEntries> m_ObjectDataTableCache;
	
	adatContainer m_Container;
	u32 m_ContainerPtrEA;
	
	u32 m_ObjectTableCacheFirstId;
	u32 m_ObjectDataTableFirstEntryId;

	u32 m_ObjectFastLookupTableEntryId;
	u16 m_ObjectFastLookupTableEntry;


}; // class audContainerSpu
#endif // __SPU

} // namespace rage

#endif // ADAT_CONTAINER_H
