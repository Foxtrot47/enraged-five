// 
// audiodata/container.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "container.h"
#include "containerbuilder.h"

#include "audiohardware/channel.h"
#include "math/amath.h"
#include "string/stringhash.h"
#include "system/endian.h"
#include "system/magicnumber.h"

#include "vectormath/vectormath.h"

namespace rage {

	using namespace Vec;

	adatContainer::adatContainer(const adatContainerHeader *const header)
	{
		Assert(header);
		
		m_Header = header;
		Init();
	}
 
	bool adatContainer::IsValidHeader(const void *header)
	{
		const adatContainerHeader *ptr = reinterpret_cast<const adatContainerHeader*>(header);
		return ptr->magic == MAKE_MAGIC_NUMBER('A','D','A','T') || ptr->magic == MAKE_MAGIC_NUMBER('T','A','D','A');
	}

	void adatContainer::Init()
	{
		const u32 version = IsNativeEndian() ? m_Header->version : sysEndian::Swap(m_Header->version);
		
		Assert((version&adatContainerHeader::VersionMask)
			== (adatContainerHeader::VERSION&adatContainerHeader::VersionMask));
		audAssertf(m_Header->magic == MAKE_MAGIC_NUMBER('A','D','A','T') || m_Header->magic == MAKE_MAGIC_NUMBER('T','A','D','A'), "Invalid data container header magic number: %c%c%c%c", EXPAND_MAGIC_NUMBER(m_Header->magic));

		if((version&adatContainerHeader::VersionMask) != (adatContainerHeader::VERSION&adatContainerHeader::VersionMask) || 
			(m_Header->magic != MAKE_MAGIC_NUMBER('A','D','A','T') && m_Header->magic != MAKE_MAGIC_NUMBER('T','A','D','A')))
		{
			audErrorf("Invalid adatContainer header");
			m_Header = NULL;
		}		
	}

	bool adatContainer::HasFastLookupTable() const
	{
		Assert(m_Header);
		const u32 version = IsNativeEndian() ? m_Header->version : sysEndian::Swap(m_Header->version);
		return ((version & adatContainerHeader::Flag_FastLookupTable) != 0);
	}

	bool adatContainer::IsDataEncrypted() const
	{
		Assert(m_Header);
		const u32 version = IsNativeEndian() ? m_Header->version : sysEndian::Swap(m_Header->version);
		return ((version & adatContainerHeader::Flag_EncryptedData) != 0);
	}

	adatContainer::PackingType adatContainer::GetPackingType() const
	{
		Assert(m_Header);
		const u32 version = IsNativeEndian() ? m_Header->version : sysEndian::Swap(m_Header->version);
		if((version & adatContainerHeader::Flag_ContiguousObjectPacking) != 0)
		{
			// 'Normal' - chunks are ordered by object such that a single contiguous read retrieves all data for an object
			return NORMAL_PACKING;
		}
		else if((version & adatContainerHeader::Flag_StreamingObjectPacking) != 0)
		{
			return STREAM_PACKING;
		}
		else
		{
			// 'Optimised' - chunks are ordered by alignment to minimise padding
			return OPTIMISED_PACKING;
		}
	}

	bool adatContainer::IsNativeEndian() const
	{
		return (m_Header->magic == MAKE_MAGIC_NUMBER('A','D','A','T'));
	}

	u32 adatContainer::GetNumObjects() const
	{
		return (IsNativeEndian() ? m_Header->numObjects : sysEndian::Swap(m_Header->numObjects));
	}

	adatObjectId adatContainer::FindObject(const char *objectName) const
	{
		return FindObject(atStringHash(objectName));
	}

	adatObjectId adatContainer::FindObject(const u32 objectNameHash) const
	{
		Assert(m_Header);
		if(!m_Header)
		{
			return InvalidId;
		}

		const u32 numObjects = GetNumObjects();

		// convert the requested hash to the container format
		const u32 hashMask = (1<<adatContainerObjectTableEntry::kObjectNameHashWidth)-1;
		const u32 objectHashToCompare = objectNameHash & hashMask;
		
		// object table is directly after the header
		const adatContainerObjectTableEntry *const objectTable = GetObjectTable();

	#if !AUD_CONTAINERBUILDER

	#if __ASSERT
		// only TOOL builds need to deal with endian swapping
		Assert(IsNativeEndian());

		u32 lastHash = 0;
		for(u32 i = 0; i < numObjects; i++)
		{
			Assert(objectTable[i].objectNameHash >= lastHash);
			lastHash = objectTable[i].objectNameHash;
		}
	#endif

		if(numObjects > 0)
		{
			// binary search
			s32 start = 0;
			s32 middle;
			s32 end = static_cast<s32>(numObjects) - 1;

			//Binary search for matching wave name hash;
			while(start <= end)
			{
				middle = (start + end) >> 1;	//Compute mid-point.
				if(objectHashToCompare > objectTable[middle].objectNameHash)
				{
					start = middle + 1;		//Repeat search in top half.
				}
				else if(objectHashToCompare < objectTable[middle].objectNameHash)
				{
					end = middle - 1;		//Repeat search in bottom half.
				}
				else
				{
					//Found it!
					return (adatObjectId)middle;
				}
			}
		}

#else
		for(u32 i = 0; i < numObjects; i++)
		{
			if(IsNativeEndian())
			{
				if(objectTable[i].objectNameHash == objectHashToCompare)
				{
					return (adatObjectId)i;
				}
			}
			else
			{
				adatContainerObjectTableEntry entry;
				EndianSwap(objectTable + i, entry);
				if(entry.objectNameHash == objectHashToCompare)
				{
					return (adatObjectId)i;
				}
			}
		}
#endif
		return InvalidId;
	}

	adatObjectDataChunkId adatContainer::FindObjectData(const adatObjectId objectId, const char *dataTypeName) const
	{
		return FindObjectData(objectId, atStringHash(dataTypeName));
	}

	adatObjectDataChunkId adatContainer::FindObjectData(const rage::adatObjectId objectId, const rage::u32 dataTypeHash) const
	{
		Assert(m_Header);
		Assert(objectId != InvalidId);
		Assert(objectId < (adatObjectId)GetNumObjects());

		const adatContainerObjectTableEntry *const objectTable = GetObjectTable();
		
		const bool isNativeEndian = IsNativeEndian();
		
		//const u32 numDataTableEntries = isNativeEndian ? objectTable[objectId].numDataTableEntries : sysEndian::Swap(objectTable[objectId].numDataTableEntries);

		u32 numDataTableEntries = 0;
		if(isNativeEndian)
		{
			numDataTableEntries = objectTable[objectId].numDataTableEntries;
		}
		else
		{
			adatContainerObjectTableEntry entry;
			EndianSwap(objectTable+objectId, entry);
			numDataTableEntries = entry.numDataTableEntries;
		}
		
		const adatContainerObjectDataTableEntry *const dataChunkTable = FindFirstObjectDataTableEntry(objectId);

		// convert requested hash to truncated container format
		const u32 dataTypeHashToCompare = dataTypeHash & ((1<<adatContainerObjectDataTableEntry::kDataTypeHashWidth)-1);
		for(u32 i = 0 ; i < numDataTableEntries; i++)
		{
			if(isNativeEndian)
			{
				if(dataChunkTable[i].dataTypeHash == dataTypeHashToCompare)
				{
					return (adatObjectDataChunkId)i;
				}
			}
			else
			{
				adatContainerObjectDataTableEntry entry;
				EndianSwap(dataChunkTable + i, entry);
				if(entry.dataTypeHash == dataTypeHashToCompare)
				{
					return (adatObjectDataChunkId)i;
				}
			}
		}
		return InvalidId;
	}

	u32 adatContainer::GetObjectNameHash(const adatObjectId objectId) const
	{
		Assert(m_Header);
		Assert(objectId != InvalidId);
		Assert(objectId < (adatObjectId)GetNumObjects());

		if(IsNativeEndian())
		{
			return GetObjectTable()[objectId].objectNameHash;
		}
		else
		{
			adatContainerObjectTableEntry entry;
			EndianSwap(GetObjectTable() + objectId, entry);
			return entry.objectNameHash;
		}
	}

	void adatContainer::GetObjectDataChunk(const adatObjectId objectId, const adatObjectDataChunkId dataChunkId, u32 &dataTypeHash, u32 &offsetBytes, u32 &sizeBytes) const
	{
		Assert(m_Header);
		Assert(objectId != InvalidId);
		Assert(objectId < (adatObjectId)GetNumObjects());
	
		const adatContainerObjectDataTableEntry *const dataChunkEntry = FindFirstObjectDataTableEntry(objectId) + dataChunkId;

		if(IsNativeEndian())
		{
			ASSERT_ONLY(const adatContainerObjectTableEntry *const objectTableEntry = GetObjectTable() + objectId);
			Assert((u32)dataChunkId < objectTableEntry->numDataTableEntries);

			offsetBytes = dataChunkEntry->dataOffsetBytes;
			sizeBytes = dataChunkEntry->sizeBytes;
			dataTypeHash = dataChunkEntry->dataTypeHash;
		}
		else
		{
			ASSERT_ONLY(adatContainerObjectTableEntry objectTableEntry);
			ASSERT_ONLY(EndianSwap(GetObjectTable() + objectId, objectTableEntry));
			Assert((u32)dataChunkId < objectTableEntry.numDataTableEntries);

			adatContainerObjectDataTableEntry entry;
			EndianSwap(dataChunkEntry, entry);
			
			offsetBytes = entry.dataOffsetBytes;
			sizeBytes = entry.sizeBytes;
			dataTypeHash = entry.dataTypeHash;
		}	
	}

	const adatContainerObjectDataTableEntry *adatContainer::FindFirstObjectDataTableEntry(const adatObjectId objectId) const
	{
		Assert(m_Header);
		Assert(objectId != InvalidId);
		Assert(objectId < (adatObjectId)GetNumObjects());

		const adatContainerObjectTableEntry *const objectTable = GetObjectTable();
		const adatContainerObjectDataTableEntry *const firstDataTableEntry = (const adatContainerObjectDataTableEntry*)(objectTable + GetNumObjects());

		if(HasFastLookupTable())
		{
			return firstDataTableEntry + GetFirstObjectDataTableEntryIndex(objectId);
		}

		Assert((((size_t)objectTable)&15) == 0);

		u32 numChunksOffset = 0;
		if(IsNativeEndian())
		{

#if __TOOL
			adatContainerObjectTableEntry entry;
			for(s32 i = 0; i < objectId; i++)
			{
				Copy(objectTable + i, entry);
				numChunksOffset += entry.numDataTableEntries;
			}
#elif RSG_PC || RSG_DURANGO || RSG_ORBIS
			for(s32 i = 0; i < objectId; i++)
			{
				numChunksOffset += objectTable[i].numDataTableEntries;
			}
#else
			const Vector_4V numDataTableEntriesMask = V4VConstant<7U,7U,7U,7U>();
			const Vector_4V *vEntry = (const Vector_4V *)objectTable;
			Vector_4V sum = V4VConstant(V_ZERO);
			const s32 objectIdAligned = (objectId - (objectId & 3));

			for(s32 i = 0; i < objectIdAligned; i += 4)
			{
#if __BE
				Vector_4V maskedEntry = V4ShiftRightAlgebraic<29>(*vEntry);
#else
				Vector_4V maskedEntry = *vEntry;
	#endif
				maskedEntry = V4And(maskedEntry, numDataTableEntriesMask);

				sum = V4AddInt(sum, maskedEntry);
				vEntry++;
			}

			const Vector_4V splattedSum = V4AddInt(V4SplatW(sum), V4AddInt(V4AddInt(sum, V4SplatZ(sum)), V4SplatY(sum)));
			// mop up trailing entries
			for(s32 i = objectIdAligned; i < objectId; i++)
			{
				numChunksOffset += objectTable[i].numDataTableEntries;
			}
			numChunksOffset += (u32)GetXi(splattedSum);
#endif

		}
		else
		{
			adatContainerObjectTableEntry entry;
			for(s32 i = 0; i < objectId; i++)
			{
				EndianSwap(objectTable + i, entry);
				numChunksOffset += entry.numDataTableEntries;
			}
		}

		return firstDataTableEntry + numChunksOffset;
	}

	u32 adatContainer::GetFirstObjectDataTableEntryIndex(const adatObjectId objectId) const
	{
		Assert(HasFastLookupTable());
#if __TOOL
		if(!IsNativeEndian())
		{
			return sysEndian::Swap(GetFastLookupTable()[objectId]);
		}
#endif
		Assert(IsNativeEndian());
		return GetFastLookupTable()[objectId];
	}
	
	u32 adatContainer::GetNumDataChunksForObject(const adatObjectId objectId) const
	{
		Assert(m_Header);
		Assert(objectId != InvalidId);
		Assert(objectId < (adatObjectId)GetNumObjects());

		const adatContainerObjectTableEntry *const objectTable = GetObjectTable();
		u32 numDataTableEntries = 0;
		if(IsNativeEndian())
		{
			numDataTableEntries = objectTable[objectId].numDataTableEntries;
		}
		else
		{
			adatContainerObjectTableEntry entry;
			EndianSwap(objectTable+objectId, entry);
			numDataTableEntries = entry.numDataTableEntries;			
		}
		
		return numDataTableEntries;
	}

	u32 adatContainer::GetHeaderSize() const
	{
		Assert(m_Header);
		return IsNativeEndian() ? m_Header->totalHeaderSize : sysEndian::Swap(m_Header->totalHeaderSize);
	}

	u32 adatContainer::ComputeContiguousMetadataSize() const
	{
		// TODO: implement
		return 8*1024;
	}

	u32 adatContainer::ComputeObjectDataSize(const adatObjectId objectId) const
	{
		const u32 numChunks = GetNumDataChunksForObject(objectId);

		u32 summedSize = 0;

		for(u32 i = 0; i < numChunks; i++)
		{
			u32 hash;
			u32 offset;
			u32 size;

			GetObjectDataChunk(objectId, i, hash, offset, size);

			summedSize += size;
		}

		return summedSize;
	}

	u32 adatContainer::ComputeContiguousObjectSize(const adatObjectId objectId) const
	{
		const u32 numChunks = GetNumDataChunksForObject(objectId);

		if(numChunks == 0)
		{
			//object has no chunks
			return 0;
		}

		u32 smallestOffset = ~0U;
		u32 largestOffset = 0;
		u32 largestSize = 0;

		for(u32 i = 0; i < numChunks; i++)
		{
			u32 hash;
			u32 offset;
			u32 size;

			GetObjectDataChunk(objectId, i, hash, offset, size);

			smallestOffset = Min(smallestOffset, offset);
			
			if(offset >= largestOffset)
			{
				largestOffset = offset;
				largestSize = size;
			}
		}

		//object has one chunk
		if(smallestOffset == largestOffset)
		{
			//size is offset plus length of chunk
			return (largestOffset + largestSize);
		}
		else
		{
			//size is difference between start of first chunk and end of last
			return ((largestOffset + largestSize) - smallestOffset);
		}
	}

#if __TOOL
	void adatContainer::Copy(const adatContainerObjectTableEntry *in, adatContainerObjectTableEntry &out)
	{
		CompileTimeAssert(sizeof(u32) == sizeof(adatContainerObjectTableEntry));
		Assert(in);
		*(u32*)&out = *(u32*)in;
	}
#endif

	void adatContainer::EndianSwap(const adatContainerObjectTableEntry *in, adatContainerObjectTableEntry &out)
	{
		CompileTimeAssert(sizeof(u32) == sizeof(adatContainerObjectTableEntry));
		Assert(in);
		*(u32*)&out = sysEndian::Swap(*(u32*)in);	
	}

	void adatContainer::EndianSwap(const adatContainerObjectDataTableEntry *in, adatContainerObjectDataTableEntry &out)
	{
		CompileTimeAssert(sizeof(u64) == sizeof(adatContainerObjectDataTableEntry));
		Assert(in);
		*(u64*)&out = sysEndian::Swap(*(u64*)in);	
	}

#if __SPU

	adatObjectId adatContainerSpu::FindObject(const u32 objectNameHash)
	{
		// convert the requested hash to the container format
		const u32 objectHashToCompare = objectNameHash & adatContainerObjectTableEntry::kObjectNameHashMask;

		const u32 numObjects = GetNumObjects();

		if(numObjects > 0)
		{
			// binary search
			s32 start = 0;
			s32 middle;
			s32 end = static_cast<s32>(numObjects) - 1;

			//Binary search for matching wave name hash;
			while(start <= end)
			{
				middle = (start + end) >> 1;	//Compute mid-point.
				const u32 middleHash = GetObjectNameHash(middle);
				if(objectHashToCompare > middleHash)
				{
					start = middle + 1;		//Repeat search in top half.
				}
				else if(objectHashToCompare < middleHash)
				{
					end = middle - 1;		//Repeat search in bottom half.
				}
				else
				{
					//Found it!
					return (adatObjectId)middle;
				}
			}
		}

		return adatContainer::InvalidId;
	}

	bool adatContainerSpu::IsObjectTableEntryInCache(const u32 objectIndex) const
	{
		Assert(objectIndex < GetNumObjects());
		return (objectIndex >= m_ObjectTableCacheFirstId && objectIndex < m_ObjectTableCacheFirstId + kNumObjectTableCacheEntries);
	}

	void adatContainerSpu::FetchObjectTableEntry(const u32 objectIndex)
	{
		const u32 objectIndexToFetch = Min(objectIndex, GetNumObjects() - kNumObjectTableCacheEntries);

		ALIGNAS(16) u8 buf[sizeof(u32) * kNumObjectTableCacheEntries + 16] ;
		dmaUnalignedGet(buf, sizeof(buf), 
			&m_ObjectTableCache[0], kNumObjectTableCacheEntries * sizeof(u32), 
			GetObjectTableEA() + objectIndexToFetch * sizeof(u32));

		m_ObjectTableCacheFirstId = objectIndexToFetch;
	}

	void adatContainerSpu::GetObjectTableEntry(const u32 objectIndex, adatContainerObjectTableEntry &entry)
	{
		if(!IsObjectTableEntryInCache(objectIndex))
		{
			FetchObjectTableEntry(objectIndex);
		}
		CompileTimeAssert(sizeof(adatContainerObjectTableEntry) == sizeof(u32));
		entry = m_ObjectTableCache[objectIndex - m_ObjectTableCacheFirstId];
	}

	u32 adatContainerSpu::GetFirstObjectDataTableEntryIndex(const u32 objectIndex)
	{
		if(m_Container.HasFastLookupTable())
		{
			if(m_ObjectFastLookupTableEntryId != objectIndex)
			{
				const u32 ea = GetFastObjectLookupTableEA() + sizeof(u16) * objectIndex;
				m_ObjectFastLookupTableEntry = sysDmaGetUInt16(ea, 6);
				m_ObjectFastLookupTableEntryId = objectIndex;
			}			
			return m_ObjectFastLookupTableEntry;
		}
		else
		{
			// Need to search through the object table.
			adatContainerObjectTableEntry entry;
			u32 count = 0;
			for(u32 i = 0; i < objectIndex; i++)
			{
				GetObjectTableEntry(i, entry);
				count += entry.numDataTableEntries;
			}
			return count;
		}
	}

	adatObjectDataChunkId adatContainerSpu::FindObjectData(const adatObjectId objectId, const u32 chunkNameHash)
	{
		adatContainerObjectTableEntry objectTableEntry;
		GetObjectTableEntry(objectId, objectTableEntry);

		const u32 dataTableEntryIndex = GetFirstObjectDataTableEntryIndex(objectId);

		const u32 dataTypeHashToCompare = chunkNameHash & ((1<<adatContainerObjectDataTableEntry::kDataTypeHashWidth)-1);

		for(u32 i = 0; i < objectTableEntry.numDataTableEntries; i++)
		{
			const adatContainerObjectDataTableEntry &entry = GetObjectDataTableEntry(i + dataTableEntryIndex);
			if(entry.dataTypeHash == dataTypeHashToCompare)
			{
				return (adatObjectDataChunkId)i;
			}
		}
		return adatContainer::InvalidId;
	}

	const adatContainerObjectDataTableEntry &adatContainerSpu::GetObjectDataTableEntry(const u32 entryIndex)
	{
		if(entryIndex < m_ObjectDataTableFirstEntryId || entryIndex >= m_ObjectDataTableFirstEntryId + kNumObjectDataTableCacheEntries)
		{
			ALIGNAS(16) u8 buf[sizeof(adatContainerObjectDataTableEntry) * kNumObjectDataTableCacheEntries + 32] ;
			const u32 ea = GetObjectDataTableEA() + sizeof(adatContainerObjectDataTableEntry) * entryIndex;
			dmaUnalignedGet(buf, sizeof(buf), &m_ObjectDataTableCache[0], sizeof(adatContainerObjectDataTableEntry) * kNumObjectDataTableCacheEntries, ea);
			m_ObjectDataTableFirstEntryId = entryIndex;
		}
		return m_ObjectDataTableCache[entryIndex - m_ObjectDataTableFirstEntryId];
	}

	void adatContainerSpu::GetObjectDataChunk(const adatObjectId objectId, const adatObjectDataChunkId dataChunkId, u32 &dataTypeHash, u32 &offsetBytes, u32 &sizeBytes)
	{
		const u32 dataTableEntryIndex = GetFirstObjectDataTableEntryIndex(objectId) + dataChunkId;
		const adatContainerObjectDataTableEntry &entry = GetObjectDataTableEntry(dataTableEntryIndex);

		dataTypeHash = entry.dataTypeHash;
		offsetBytes = entry.dataOffsetBytes;
		sizeBytes = entry.sizeBytes;
	}

#endif
} // namespace rage
