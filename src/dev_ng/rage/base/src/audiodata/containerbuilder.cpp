// 
// audiodata/containerbuilder.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "containerbuilder.h"

#if AUD_CONTAINERBUILDER

#include "system/memops.h"
#include "string/stringhash.h"

namespace rage {

	adatContainerBuilder::adatContainerBuilder(adatContainer::PackingType packingType) 
		: m_PackingType(packingType)
		, m_EncryptedData(false)
	{
		// Optimised packing suggests the container will be statically loaded with lots of objects in it
		// so the fast lookup table is worth having.
		if(m_PackingType == adatContainer::OPTIMISED_PACKING)
		{
			m_HasFastLookupTable = true;
		}
	}

	adatContainerBuilder::~adatContainerBuilder()
	{
		Reset();
	}

	void adatContainerBuilder::Reset()
	{
		// reclaim memory used by dynamically allocated object and chunk structures
		for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin() ; i != m_Objects.end(); i++)
		{
			if((*i))
			{
				for(atArray<adatContainerBuilderChunk*>::iterator ci = (*i)->dataChunks.begin(); ci != (*i)->dataChunks.end(); ci++)
				{
					if((*ci))
					{
						delete[] (*ci)->data;
						delete (*ci);
					}
				}
				delete (*i);
			}
		}
		m_Objects.Reset();
	}

	adatObjectId adatContainerBuilder::AddObject(const u32 objectNameHash)
	{
		const u32 hashMask = ((1<<adatContainerObjectTableEntry::kObjectNameHashWidth)-1);
		const u32 truncatedHash = objectNameHash & hashMask;
		if(!Verifyf(FindObject(truncatedHash) == adatContainer::InvalidId, "Object with name hash %u (%u) already exists in container", objectNameHash, truncatedHash))
		{
			return adatContainer::InvalidId;
		}
		adatContainerBuilderObject *object = rage_new adatContainerBuilderObject();
		object->nameHash = truncatedHash;
		const adatObjectId objectId = static_cast<adatObjectId>(m_Objects.GetCount());
		m_Objects.PushAndGrow(object);
		return objectId;
	}

	bool adatContainerBuilder::DeleteObject(const adatObjectId objectId)
	{
		Assert(objectId != adatContainer::InvalidId);
		Assert(objectId < m_Objects.GetCount());
		Assert(m_Objects[objectId]);
		if(m_Objects[objectId])
		{
			// don't remove the entry from the array as that would invalidate any adatObjectId's stored externally.
			// instead null the slot out.
			delete m_Objects[objectId];
			m_Objects[objectId] = NULL;
			return true;
		}
		return false;
	}

	adatObjectId adatContainerBuilder::FindObject(const u32 objectNameHash)
	{
		adatObjectId objectId = 0;
		const u32 hashMask = ((1<<adatContainerObjectTableEntry::kObjectNameHashWidth)-1);
		for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin() ; i != m_Objects.end(); i++)
		{
			if((*i))
			{
				if(((*i)->nameHash&hashMask) == (objectNameHash&hashMask))
				{
					return objectId;
				}
			}
			objectId++;
		}
		return adatContainer::InvalidId;
	}

	adatObjectDataChunkId adatContainerBuilder::AddDataChunk(const adatObjectId objectId, const u32 dataTypeHash, const u8 *data, const u32 dataSizeBytes, const u32 alignment /*=1*/)
	{
		Assert(objectId != adatContainer::InvalidId);
		Assert(objectId < m_Objects.GetCount());
		Assert(m_Objects[objectId]);
		Assert(alignment != 0);

		if(!Verifyf(FindDataChunk(objectId, dataTypeHash) == adatContainer::InvalidId, "Object id %d already has a data chunk with type hash %u", objectId, dataTypeHash))
		{
			return adatContainer::InvalidId;
		}

		const adatObjectDataChunkId chunkId = static_cast<adatObjectDataChunkId>(m_Objects[objectId]->dataChunks.GetCount());
		adatContainerBuilderChunk *chunk = rage_new adatContainerBuilderChunk;

		chunk->data = rage_new u8[dataSizeBytes];

		sysMemCpy(chunk->data, data, dataSizeBytes);
		chunk->sizeBytes = dataSizeBytes;
		chunk->typeNameHash = dataTypeHash;
		chunk->alignment = alignment;

		m_Objects[objectId]->dataChunks.PushAndGrow(chunk);

		return chunkId;
	}

	adatObjectDataChunkId adatContainerBuilder::FindDataChunk(const adatObjectId objectId, const u32 dataTypeHash) const
	{
		Assert(objectId != adatContainer::InvalidId);
		Assert(objectId < m_Objects.GetCount());
		Assert(m_Objects[objectId]);

		const u32 hashMask = ((1<<adatContainerObjectDataTableEntry::kDataTypeHashWidth)-1);

		adatObjectDataChunkId chunkId = 0;
		for(atArray<adatContainerBuilderChunk *>::iterator iter = m_Objects[objectId]->dataChunks.begin(); iter != m_Objects[objectId]->dataChunks.end(); iter++)
		{
			if((*iter) && ((*iter)->typeNameHash&hashMask) == (dataTypeHash&hashMask))
			{
				return chunkId;
			}
			chunkId++;
		}
		return adatContainer::InvalidId;
	}

	bool adatContainerBuilder::DeleteDataChunk(const adatObjectId objectId, const adatObjectDataChunkId chunkId)
	{
		Assert(objectId != adatContainer::InvalidId);
		Assert(objectId < m_Objects.GetCount());
		Assert(m_Objects[objectId]);

		Assert(chunkId < m_Objects[objectId]->dataChunks.GetCount());
		Assert(m_Objects[objectId]->dataChunks[chunkId]);
		if(m_Objects[objectId]->dataChunks[chunkId])
		{
			delete[] m_Objects[objectId]->dataChunks[chunkId]->data;
			// leave the slot allocated in the array to avoid invalidating cached adatObjectDataChunkIds 
			delete m_Objects[objectId]->dataChunks[chunkId];
			m_Objects[objectId]->dataChunks[chunkId] = NULL;
			return true;
		}

		return false;
	}

	bool adatContainerBuilder::PopulateFromPrebuiltContainer(const adatContainer *const container)
	{
		Reset();

		for(adatObjectId objectId = 0; objectId < static_cast<adatObjectId>(container->GetNumObjects()); objectId++)
		{
			adatObjectId newObjectId = AddObject(container->GetObjectNameHash(objectId));
			Assert(newObjectId != adatContainer::InvalidId);

			const u32 numDataChunks = container->GetNumDataChunksForObject(objectId);

			for(adatObjectDataChunkId chunkId = 0; chunkId < static_cast<adatObjectDataChunkId>(numDataChunks); chunkId++)
			{
				u32 dataTypeHash;
				u32 dataOffsetBytes;
				u32 dataSizeBytes;
				container->GetObjectDataChunk(objectId, chunkId, dataTypeHash, dataOffsetBytes, dataSizeBytes);
				// NOTE: we don't know the chunk alignment (and cannot work it out), so this cannot be preserved.
				Verifyf(adatContainer::InvalidId != AddDataChunk(newObjectId, dataTypeHash, container->GetDataPointer() + dataOffsetBytes, dataSizeBytes), "Failed to add data chunk to container.  Type hash: %u, size %u bytes (alignment: %u)", dataTypeHash, dataSizeBytes);
			}
		}

		return true;
	}

	u32 adatContainerBuilder::CountNumObjects() const
	{
		u32 numObjects = 0;
		for(atArray<adatContainerBuilderObject*>::const_iterator i = m_Objects.begin(); i != m_Objects.end(); i++)
		{
			if((*i))
			{
				numObjects++;
			}
		}

		return numObjects;
	}

	u32 adatContainerBuilder::CountNumDataChunks(adatContainerBuilderObject *object) const
	{
		u32 numChunks = 0;
		for(atArray<adatContainerBuilderChunk *>::iterator iter = object->dataChunks.begin(); iter != object->dataChunks.end(); iter++)
		{
			if(*iter)
			{
				numChunks++;
			}
		}
		return numChunks;
	}

	u32 adatContainerBuilder::ComputeHeaderSize() const
	{
		// fixed file header
		u32 sizeBytes = sizeof(adatContainerHeader);

		// object table
		const u32 numObjects = CountNumObjects();
		sizeBytes += sizeof(adatContainerObjectTableEntry) * numObjects;

		if(m_HasFastLookupTable)
		{
			sizeBytes += numObjects * sizeof(u16);
		}

		// object chunk table
		for(atArray<adatContainerBuilderObject*>::const_iterator i = m_Objects.begin(); i != m_Objects.end(); i++)
		{
			if((*i))
			{
				const u32 numDataChunks = CountNumDataChunks(*i);
				sizeBytes += sizeof(adatContainerObjectDataTableEntry) * numDataChunks;
			}
		}

		return sizeBytes;
	}

	int ObjectComparer(adatContainerBuilderObject* const* lhs, adatContainerBuilderObject* const* rhs)
	{
		return (*lhs)->nameHash > (*rhs)->nameHash ? 1 : -1;
	}

	int ObjectChunkComparer(adatContainerBuilderChunk* const* lhs, adatContainerBuilderChunk* const* rhs)
	{
		return (*lhs)->typeNameHash > (*rhs)->typeNameHash ? 1 : -1;
	}

	int ObjectChunkAlignmentComparer(adatChunkObjectIDPair const* lhs, adatChunkObjectIDPair const* rhs)
	{
		return (*lhs).dataChunk->alignment > (*rhs).dataChunk->alignment ? 1 : -1;
	}

	int ObjectChunkStreamingComparer(adatChunkObjectIDPair const* lhs, adatChunkObjectIDPair const* rhs)
	{
		// Ensure data chunk for global object is sorted last
		if( (*lhs).objectID == 0 && (*lhs).dataChunk->typeNameHash == ATSTRINGHASH("DATA", 0x5EB5E655) )
		{
			return 1;
		}
		return (*lhs).dataChunk->alignment > (*rhs).dataChunk->alignment ? 1 : -1;
	}

	bool adatContainerBuilder::Serialize(fiStream *stream, const bool outputBigEndian)
	{
		const bool isNativeEndian = (outputBigEndian == __BE);
		const bool needEndianSwap = !isNativeEndian;

		adatContainerHeader header;
		header.Default();
		header.totalHeaderSize = ComputeHeaderSize();

		Assign(header.numObjects, CountNumObjects());
		const u32 numObjects = header.numObjects;		

		ConditionalSwap(header.magic, needEndianSwap);
		ConditionalSwap(header.numObjects, needEndianSwap);
		ConditionalSwap(header.totalHeaderSize, needEndianSwap);

		if(m_HasFastLookupTable)
		{
			header.version |= adatContainerHeader::Flag_FastLookupTable;
		}

		if(m_EncryptedData)
		{
			header.version |= adatContainerHeader::Flag_EncryptedData;
		}

		if(m_PackingType == adatContainer::NORMAL_PACKING)
		{
			header.version |= adatContainerHeader::Flag_ContiguousObjectPacking;
		}
		else if(m_PackingType == adatContainer::STREAM_PACKING)
		{
			header.version |= adatContainerHeader::Flag_StreamingObjectPacking;
		}

		ConditionalSwap(header.version, needEndianSwap);

		if(stream->Write(&header, sizeof(header)) != sizeof(header))
		{
			return false;
		}

		// Object table is always sorted by hash to facilitate binary searching at runtime
		m_Objects.QSort(0,-1,&ObjectComparer);

		// (optionally) write fast lookup table
		u32 currentDataChunkEntryIndex = 0;
		if(m_HasFastLookupTable)
		{
			for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin(); i != m_Objects.end(); i++)
			{
				u16 tableValue;
				Assign(tableValue, currentDataChunkEntryIndex);
				ConditionalSwap(tableValue, needEndianSwap);
				if(stream->Write(&tableValue, sizeof(tableValue)) != sizeof(tableValue))
				{
					return false;
				}
				const u32 numDataChunks = CountNumDataChunks(*i);
				currentDataChunkEntryIndex += numDataChunks;
			}
		}

		
		u32 dataChunkTableOffset = 0;

		// serialise object table
		for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin(); i != m_Objects.end(); i++)
		{
			if((*i))
			{			
				adatContainerObjectTableEntry entry;
	
				const u32 numDataChunks = CountNumDataChunks(*i);

				Assert(numDataChunks < adatContainerObjectTableEntry::kMaxNumDataTableEntries);
				if(numDataChunks > adatContainerObjectTableEntry::kMaxNumDataTableEntries)
				{
					return false;
				}

				entry.numDataTableEntries = numDataChunks;
				Assert(entry.numDataTableEntries == numDataChunks);

				const u32 truncatedHash = (*i)->nameHash & ((1<<adatContainerObjectTableEntry::kObjectNameHashWidth)-1);;
				entry.objectNameHash = truncatedHash;
				Assert(entry.objectNameHash == truncatedHash);

				dataChunkTableOffset += sizeof(adatContainerObjectDataTableEntry) * numDataChunks;

				if(needEndianSwap)
				{
					adatContainer::EndianSwap(entry);
				}

				if(stream->Write(&entry, sizeof(adatContainerObjectTableEntry)) != sizeof(adatContainerObjectTableEntry))
				{
					return false;
				}
			}
		}

		const u32 sizeOfObjectTable = sizeof(adatContainerObjectTableEntry) * numObjects;
		const u32 sizeOfFastLookupTable = m_HasFastLookupTable ? numObjects * sizeof(u16) : 0;
		const u32 dataOffset = sizeof(adatContainerHeader) + sizeOfFastLookupTable + sizeOfObjectTable + dataChunkTableOffset;

		if(m_PackingType == adatContainer::NORMAL_PACKING)
		{
			return SerializeDataNormal(stream, dataOffset, needEndianSwap);
		}
		else if(m_PackingType == adatContainer::STREAM_PACKING)
		{
			return SerializeDataOptimized(stream, dataOffset, needEndianSwap, true);
		}
		else
		{ 
			return SerializeDataOptimized(stream, dataOffset, needEndianSwap, false);
		}
	}

	bool adatContainerBuilder::SerializeDataNormal(fiStream *stream, const u32 dataOffset, const bool needEndianSwap)
	{
		// serialize data chunk table
		u32 localDataOffset = dataOffset;

		for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin(); i != m_Objects.end(); i++)
		{
			if((*i))
			{
				//(*i)->dataChunks.QSort(0,-1,&ObjectChunkComparer);
				
				u32 firstChunkOffset = 0;
				bool isFirstChunk = true;

				for(atArray<adatContainerBuilderChunk *>::iterator ci = (*i)->dataChunks.begin(); ci != (*i)->dataChunks.end(); ci++)
				{
					
					adatContainerObjectDataTableEntry entry;					

					entry.dataOffsetBytes = localDataOffset;

					// ensure chunk is aligned as requested
					const u32 alignment = (*ci)->alignment;
					if(isFirstChunk)
					{
						while((entry.dataOffsetBytes % alignment) != 0)
						{
							entry.dataOffsetBytes++;
							localDataOffset++;
						}
						// align all chunks for this object relative to the first chunk, rather than the container base address
						firstChunkOffset = localDataOffset;
						isFirstChunk = false;
					}
					else
					{
						while(((localDataOffset - firstChunkOffset) % (*ci)->alignment) != 0)
						{
							entry.dataOffsetBytes++;
							localDataOffset++;
						}
					}
					entry.sizeBytes = (*ci)->sizeBytes;
					Assert(entry.sizeBytes == (*ci)->sizeBytes);

					const u32 truncatedHash = (*ci)->typeNameHash & ((1<<adatContainerObjectDataTableEntry::kDataTypeHashWidth)-1);
					entry.dataTypeHash = truncatedHash;
					Assert(entry.dataTypeHash == truncatedHash);

					localDataOffset += (*ci)->sizeBytes;

					if(needEndianSwap)
					{
						adatContainer::EndianSwap(entry);
					}

					if(stream->Write(&entry, sizeof(entry)) != sizeof(entry))
					{
						return false;
					}
				}
			}
		}

		// serialize data
		localDataOffset = dataOffset;
		static const u8 padding[1] = {0};
		for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin(); i != m_Objects.end(); i++)
		{
			if((*i))
			{
				u32 firstChunkOffset = 0;
				bool isFirstChunk = true;

				for(atArray<adatContainerBuilderChunk *>::iterator ci = (*i)->dataChunks.begin(); ci != (*i)->dataChunks.end(); ci++)
				{
					const u32 alignment = (*ci)->alignment;

					// pad to required alignment
					if(isFirstChunk)
					{
						while((localDataOffset % alignment) != 0)
						{
							stream->WriteByte(padding,1);
							localDataOffset++;
						}
						firstChunkOffset = localDataOffset;
						isFirstChunk = false;
					}
					else
					{
						while(((localDataOffset - firstChunkOffset) % (*ci)->alignment) != 0)
						{
							stream->WriteByte(padding,1);
							localDataOffset++;
						}
					}

					if(stream->Write((*ci)->data, (*ci)->sizeBytes) != (s32)(*ci)->sizeBytes)
					{
						return false;
					}
					localDataOffset += (*ci)->sizeBytes;
				}
			}
		}

		return true;
	}

	bool adatContainerBuilder::SerializeDataOptimized(fiStream *stream, const u32 dataOffset, const bool needEndianSwap, const bool isStreamingBank)
	{
		//Create array of chunk/objectId objects
		atArray<adatChunkObjectIDPair> chunks;
		s32 index = 0;
		for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin(); i != m_Objects.end(); index++,i++)
		{
			if((*i))
			{
				for(atArray<adatContainerBuilderChunk *>::iterator ci = (*i)->dataChunks.begin(); ci != (*i)->dataChunks.end(); ci++)
				{
					adatChunkObjectIDPair chunkObjectIDPair;
					chunkObjectIDPair.objectID = index;
					chunkObjectIDPair.dataChunk = (*ci);					
					chunks.PushAndGrow(chunkObjectIDPair);
				}
			}
		}

		//sort by alignment
		chunks.QSort(0,-1,isStreamingBank ? &ObjectChunkStreamingComparer : &ObjectChunkAlignmentComparer);

		// calculate chunk offsets		
		u32 currentOffset = dataOffset;
		for(atArray<adatChunkObjectIDPair>::iterator ci = chunks.begin(); ci != chunks.end(); ci++)
		{
			// ensure chunk is aligned as requested
			const u32 alignment = ci->dataChunk->alignment;
			while((currentOffset % alignment) != 0)
			{
				currentOffset++;
			}
			ci->dataChunk->calculatedOffset = currentOffset;
			currentOffset += ci->dataChunk->sizeBytes;
		}


		//write lookup table for chunks
		// This must be sequential per object, regardless of how the chunks themselves are sorted
		for(atArray<adatContainerBuilderObject*>::iterator i = m_Objects.begin(); i != m_Objects.end(); i++)
		{
			if((*i))
			{
				// sort the chunk lookup by hash to enable fast searching at runtime
				(*i)->dataChunks.QSort(0,-1,&ObjectChunkComparer);

				for(atArray<adatContainerBuilderChunk *>::iterator ci = (*i)->dataChunks.begin(); ci != (*i)->dataChunks.end(); ci++)
				{
					adatContainerObjectDataTableEntry entry;

					entry.dataOffsetBytes = (*ci)->calculatedOffset;					
					entry.sizeBytes = (*ci)->sizeBytes;

					// check for overflow
					Assert(entry.sizeBytes == (*ci)->sizeBytes);

					const u32 truncatedHash = (*ci)->typeNameHash & ((1<<adatContainerObjectDataTableEntry::kDataTypeHashWidth)-1);
					entry.dataTypeHash = truncatedHash;

					// check for overflow
					Assert(entry.dataTypeHash == truncatedHash);

					if(needEndianSwap)
					{
						adatContainer::EndianSwap(entry);
					}

					if(stream->Write(&entry, sizeof(entry)) != sizeof(entry))
					{
						return false;
					}
				}
			}
		}

		// now serialize chunk data
		static const u8 padding[1] = {0};
		currentOffset = dataOffset;
		for(atArray<adatChunkObjectIDPair>::iterator ci = chunks.begin(); ci != chunks.end(); ci++)
		{
			// ensure chunk is aligned as requested
			const u32 alignment = ci->dataChunk->alignment;
			while((currentOffset % alignment) != 0)
			{
				currentOffset++;
				stream->WriteByte(padding,1);
			}
			Assert(ci->dataChunk->calculatedOffset == currentOffset);
			currentOffset += ci->dataChunk->sizeBytes;

			if(stream->Write(ci->dataChunk->data, ci->dataChunk->sizeBytes) != (s32)ci->dataChunk->sizeBytes)
			{
				chunks.Reset();
				return false;
			}
		}

		return true;
	}

	void adatContainerBuilder::SetDataChunkAlignment(const adatObjectId objectId, const adatObjectDataChunkId chunkId, const u32 alignment)
	{
		Assert(objectId != adatContainer::InvalidId);
		Assert(objectId < m_Objects.GetCount());
		Assert(m_Objects[objectId]);

		Assert(chunkId < m_Objects[objectId]->dataChunks.GetCount());
		Assert(m_Objects[objectId]->dataChunks[chunkId]);
		m_Objects[objectId]->dataChunks[chunkId]->alignment = alignment;
	}

	u32 adatContainerBuilder::GetNumObjects() const
	{
		return m_Objects.GetCount();
	}

	u32 adatContainerBuilder::GetNumDataChunks(const adatObjectId objectId) const
	{
		Assert(objectId != adatContainer::InvalidId);
		Assert(objectId < m_Objects.GetCount());
		if(m_Objects[objectId])
		{
			return m_Objects[objectId]->dataChunks.GetCount();
		}
		return 0;
	}

} // namespace rage

#endif //AUD_CONTAINERBUILDER

