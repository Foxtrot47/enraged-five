// 
// simpletypes.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure functions - do not edit.
// 

#include "simpletypes.h"
#include "string/stringhash.h"

namespace ravesimpletypes
{
	// 
	// Int
	// 
	void* Int::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 510789971U: return &Value;
			default: return NULL;
		}
	}
	
	// 
	// UnsignedInt
	// 
	void* UnsignedInt::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 510789971U: return &Value;
			default: return NULL;
		}
	}
	
	// 
	// Float
	// 
	void* Float::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 510789971U: return &Value;
			default: return NULL;
		}
	}
	
	// 
	// String
	// 
	void* String::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 510789971U: return &Value;
			default: return NULL;
		}
	}
	
	// 
	// StringHash
	// 
	void* StringHash::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 510789971U: return &Value;
			default: return NULL;
		}
	}
	
	// 
	// Vector3
	// 
	void* Vector3::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 510789971U: return &Value;
			default: return NULL;
		}
	}
	
	// 
	// Vector4
	// 
	void* Vector4::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 510789971U: return &Value;
			default: return NULL;
		}
	}
	
	// 
	// VariableList
	// 
	void* VariableList::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2728961826U: return &Variable;
			default: return NULL;
		}
	}
	
	// 
	// Slot
	// 
	void* Slot::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1785252124U: return &LoadType;
			case 1941482115U: return &padding0;
			case 3180641850U: return &padding1;
			case 2346113727U: return &padding2;
			case 1259108736U: return &MaxHeaderSize;
			case 3226656423U: return &Size;
			case 1023328305U: return &StaticBank;
			case 3984165254U: return &MaxMetadataSize;
			case 4070075390U: return &MaxDataSize;
			default: return NULL;
		}
	}
	
	// 
	// WaveSlots
	// 
	void* WaveSlots::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2613755285U: return &SlotRef;
			default: return NULL;
		}
	}
	
	// 
	// ERSettings
	// 
	void* ERSettings::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2443385927U: return &RoomSize;
			case 526475892U: return &RoomDimensions;
			case 1563361181U: return &ListenerPos;
			case 3312544021U: return &Allpasses;
			case 2230395868U: return &NodeGainMatrix;
			case 1383030666U: return &Gain_1stOrder;
			case 1349591851U: return &Gain_2ndOrder;
			case 2102044020U: return &Gain_3rdOrder;
			case 125222005U: return &NodeLPF_1stOrder;
			case 1953868310U: return &NodeLPF_2ndOrder;
			case 3939270627U: return &NodeLPF_3rdOrder;
			default: return NULL;
		}
	}
	
	// 
	// Enumeration Conversion
	// 
	
	// PURPOSE - Convert a SlotLoadType value into its string representation.
	const char* SlotLoadType_ToString(const SlotLoadType value)
	{
		switch(value)
		{
			case SLOT_LOAD_TYPE_BANK: return "SLOT_LOAD_TYPE_BANK";
			case SLOT_LOAD_TYPE_WAVE: return "SLOT_LOAD_TYPE_WAVE";
			case SLOT_LOAD_TYPE_STREAM: return "SLOT_LOAD_TYPE_STREAM";
			case NUM_SLOTLOADTYPE: return "NUM_SLOTLOADTYPE";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a SlotLoadType value.
	SlotLoadType SlotLoadType_Parse(const char* str, const SlotLoadType defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 3789003763U: return SLOT_LOAD_TYPE_BANK;
			case 3361441860U: return SLOT_LOAD_TYPE_WAVE;
			case 1608829701U: return SLOT_LOAD_TYPE_STREAM;
			case 39188130U:
			case 2988942933U: return NUM_SLOTLOADTYPE;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gAudioConfigGetBaseTypeId(const rage::u32 classId)
	{
		switch(classId)
		{
			case Int::TYPE_ID: return Int::BASE_TYPE_ID;
			case UnsignedInt::TYPE_ID: return UnsignedInt::BASE_TYPE_ID;
			case Float::TYPE_ID: return Float::BASE_TYPE_ID;
			case String::TYPE_ID: return String::BASE_TYPE_ID;
			case StringHash::TYPE_ID: return StringHash::BASE_TYPE_ID;
			case Vector3::TYPE_ID: return Vector3::BASE_TYPE_ID;
			case Vector4::TYPE_ID: return Vector4::BASE_TYPE_ID;
			case VariableList::TYPE_ID: return VariableList::BASE_TYPE_ID;
			case Slot::TYPE_ID: return Slot::BASE_TYPE_ID;
			case WaveSlots::TYPE_ID: return WaveSlots::BASE_TYPE_ID;
			case ERSettings::TYPE_ID: return ERSettings::BASE_TYPE_ID;
			default: return 0xFFFFFFFF;
		}
	}
	
	// PURPOSE - Determines if a type inherits from another type
	bool gAudioConfigIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId)
	{
		if(objectTypeId == 0xFFFFFFFF)
		{
			return false;
		}
		else if(objectTypeId == baseTypeId)
		{
			return true;
		}
		else
		{
			return gAudioConfigIsOfType(gAudioConfigGetBaseTypeId(objectTypeId), baseTypeId);
		}
	}
} // namespace ravesimpletypes
