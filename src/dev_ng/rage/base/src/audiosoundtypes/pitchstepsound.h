//
// audiosoundtypes/pitchstepsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_PITCH_STEP_SOUND_H
#define AUD_PITCH_STEP_SOUND_H

#include "sound.h"
#include "sounddefs.h"

namespace rage {

// PURPOSE
//  A pitch step sound varies the pitch of the child sound on consecutive triggers using a predefined step size, in cents,
//	through a predefined number of steps around 0 pitch. This sound is intended to manage the mitigation of the phasing
//	effects that can occur when overlapping the playback of similar wave assets.
class audPitchStepSound : public audSound
{
public:
	audPitchStepSound();
	~audPitchStepSound();

	AUD_DECLARE_STATIC_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *metadata, const audSoundInternalInitParams* initParams);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();
	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

protected:
	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState Prepare(audWaveSlot *slot);
};

} // namespace rage

#endif //AUD_PITCH_STEP_SOUND_H
