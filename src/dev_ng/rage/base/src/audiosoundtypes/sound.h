//
// audioengine/sound.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SOUND_H
#define AUD_SOUND_H

#include "soundcontrol.h"
#include "sounddefs.h"
#include "audioengine/enginedefs.h"
#include "audioengine/entityvariablecache.h"
#include "audioengine/engineutil.h"
#include "audioengine/metadataref.h"
#include "audioengine/requestedsettings.h"
#include "audioengine/soundpool.h"
#include "audiohardware/debug.h"
#include "string/stringhash.h"
#include "system/performancetimer.h"
#include "vector/vector3.h"
#include "diag/output.h"	// for DIAG_CONTEXT_MESSAGE
#include "diag/trap.h"
#include "system/dma.h"


namespace rage {

	class audCategory;
	class audEntity;
	class audTracker;
	class audWaveSlot;
	class bkBank;

#define __SOUND_DEBUG_DRAW (!__SPU && __BANK)
		
#define NULL_HASH 0

#if __ASSERT
#ifdef __AUD_ENVIRONMENT_UPDATE
#define SoundAssertMsg AssertMsg
#define SoundAssertf Assertf
#define SoundAssert Assert
#else
#define SoundAssertMsg(x, msg) (void)((x) || SoundAssertFailed(msg, #x,__FILE__,__LINE__) || (__debugbreak(),0));
#define SoundAssertf(x,fmt,...) (void)((x) || SoundAssertFailed(#x,__FILE__,__LINE__,"%s: " fmt,##__VA_ARGS__) || (__debugbreak(),0))
#define SoundAssert(x) SoundAssertMsg(x,NULL)
#endif
#else
#define SoundAssertMsg(x,y)
#define SoundAssertf(x,fmt,...)
#define SoundAssert(x)
#endif

#define AUD_SOUND_USE_VIRTUALS 0

#if __SPU && !AUD_SOUND_USE_VIRTUALS
#define AUD_VIRTUAL 
#else
#define AUD_VIRTUAL virtual
#endif

#define AUD_INLINE __forceinline

#define AUD_CPP_MEMBER_INVOKES 0

#if AUD_SOUND_USE_VIRTUALS

#define AUD_IMPLEMENT_STATIC_WRAPPERS(soundType)
#define AUD_DECLARE_STATIC_WRAPPERS
#define AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS

#else

struct audSoundCombineBuffer;

#if AUD_CPP_MEMBER_INVOKES

typedef bool (audSound::*SoundUpdateFunction )(u32,audSoundCombineBuffer &);
typedef void (audSound::*SoundPauseFunction)(u32);
typedef void (audSound::*SoundKillFunction)();
typedef audPrepareState (audSound::*SoundPrepareFunction)(audWaveSlot*,bool);
typedef void (audSound::*SoundPlayFunction)(u32,audSoundCombineBuffer &);
typedef s32 (audSound::*SoundComputeDurationMsExcludingStartOffsetAndPredelayFunction)(bool*);
typedef void (*SoundDestructFunction)(audSound*);
typedef void (audSound::*SoundActionReleaseRequestFunction)(u32);

#define INVOKE_MEMBER_FN(fn)  ((*this).*(fn))()
#define INVOKE_MEMBER_FN_1(fn,a)  ((*this).*(fn))(a)
#define INVOKE_MEMBER_FN_2(fn,a,b)  ((*this).*(fn))(a,b)
#define INVOKE_MEMBER_FN_3(fn,a,b,c)  ((*this).*(fn))(a,b,c)

#else

typedef bool (*SoundUpdateFunction)(audSound*,u32,audSoundCombineBuffer &);
typedef void (*SoundPauseFunction)(audSound*,u32);
typedef void (*SoundKillFunction)(audSound* );
typedef audPrepareState (*SoundPrepareFunction)(audSound*,audWaveSlot*,bool);
typedef void (*SoundPlayFunction)(audSound*,u32,audSoundCombineBuffer &);
typedef s32 (*SoundComputeDurationMsExcludingStartOffsetAndPredelayFunction)(audSound*,bool*);
typedef void (*SoundDestructFunction)(audSound*);
typedef void (*SoundActionReleaseRequestFunction)(audSound*,u32);

#define INVOKE_MEMBER_FN(fn) fn(this)
#define INVOKE_MEMBER_FN_1(fn, a) fn(this, a)
#define INVOKE_MEMBER_FN_2(fn, a, b) fn(this, a, b)
#define INVOKE_MEMBER_FN_3(fn, a, b, c) fn(this, a, b, c)

#endif

#if AUD_CPP_MEMBER_INVOKES

#define AUD_IMPLEMENT_STATIC_WRAPPERS(soundType) \
	void soundType::sDestruct(audSound *_this) \
	{ \
		((soundType*)_this)->~soundType(); \
	}

#define AUD_DECLARE_STATIC_WRAPPERS static void sDestruct(audSound *_this);

#else
#define AUD_IMPLEMENT_STATIC_WRAPPERS(soundType) \
bool soundType::sAudioUpdate(audSound *_this, u32 timeInMs, audSoundCombineBuffer &combineBuffer) \
{ \
	return ((soundType*)_this)->AudioUpdate(timeInMs, combineBuffer); \
} \
void soundType::sAudioKill(audSound *_this) \
{ \
	((soundType*)_this)->AudioKill(); \
} \
audPrepareState soundType::sAudioPrepare(audSound *_this, audWaveSlot *slot, const bool checkStateOnly) \
{ \
	return ((soundType*)_this)->AudioPrepare(slot, checkStateOnly); \
} \
void soundType::sAudioPlay(audSound *_this, u32 timeInMs, audSoundCombineBuffer &combineBuffer) \
{ \
	((soundType*)_this)->AudioPlay(timeInMs, combineBuffer); \
} \
s32 soundType::sComputeDurationMsExcludingStartOffsetAndPredelay(audSound *_this, bool *isLooping) \
{ \
	 return ((soundType*)_this)->ComputeDurationMsExcludingStartOffsetAndPredelay(isLooping); \
} \
void soundType::sDestruct(audSound *_this) \
{ \
	((soundType*)_this)->~soundType(); \
}

#define AUD_DECLARE_STATIC_WRAPPERS \
	static bool sAudioUpdate(audSound *_this, u32 timeInMs, audSoundCombineBuffer &combineBuffer); \
	static void sAudioKill(audSound *_this); \
	static audPrepareState sAudioPrepare(audSound *_this,audWaveSlot *,bool); \
	static void sAudioPlay(audSound *_this,u32 timeInMs, audSoundCombineBuffer &combineBuffer); \
	static s32 sComputeDurationMsExcludingStartOffsetAndPredelay(audSound *_this, bool *isLooping); \
	static void sDestruct(audSound *_this);

#endif


#define AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS 

#endif


enum audDynamicValueId
{
	AUD_DYNAMICVALUE_NONE = 0,
	AUD_DYNAMICVALUE_LISTENER_DISTANCE,
	AUD_DYNAMICVALUE_POSX,
	AUD_DYNAMICVALUE_POSY,
	AUD_DYNAMICVALUE_POSZ,
	AUD_DYNAMICVALUE_VELX,
	AUD_DYNAMICVALUE_VELY,
	AUD_DYNAMICVALUE_VELZ,
	AUD_DYNAMICVALUE_SPEED,
	AUD_DYNAMICVALUE_FWDSPEED,
	AUD_DYNAMICVALUE_UPSPEED,
	AUD_DYNAMICVALUE_CLIENTVAR,
	AUD_DYNAMICVALUE_PLAYTIME,
	AUD_DYNAMICVALUE_POSSEED,
	AUD_DYNAMICVALUE_MAX = 1024
};

typedef void* audVariableHandle;

#if __SPU
extern s32 g_TaskBucketMemOffset;
extern variableNameValuePair *g_GlobalVariableEAStart, *g_GlobalVariableEAEnd, *g_GlobalSoundVariables;
extern float *g_EntityVariableEAStart, *g_EntityVariableEAEnd;
extern audEngineContext g_Context;
// if this is in the global variable range then add the global variable offset
// otherwise just add on the bucket offset
#define AUD_GET_VARIABLE(x) GetVariableValue(ResolveSoundVariablePtr(x))
#define AUD_SET_VARIABLE(x, y) SetVariableValue(x, y)
#define RESOLVE_SOUND_VARIABLE_PTR(x) ResolveSoundVariablePtr(x)

class audEntityVariableCache;
extern audEntityVariableCache g_EntityVariableCache;

audVariableHandle ResolveSoundVariablePtr(audVariableHandle handle);

#else

#define AUD_GET_VARIABLE(x) GetVariableValue(x)
#define AUD_SET_VARIABLE(x, y) SetVariableValue(x,y) 

#endif

// For variables, as they are now floats, and so our comparisons need to be smart.
//#define FLOAT_EPSILON 1.192092896e-07F  // This seemed sensible, but doesn't work!
#define FLOAT_EPSILON 0.0001f // This is entirely arbitrary! And, I guess, potentially still dodgy.
#define FLOAT_EQ(x,v) (((v - FLOAT_EPSILON) < x) && (x <( v + FLOAT_EPSILON)))
#define FLOAT_LTEQ(x,v) (x < (v + FLOAT_EPSILON))
#define FLOAT_LT(x,v) (x < (v - FLOAT_EPSILON))
#define FLOAT_GTEQ(x,v) (x > (v - FLOAT_EPSILON))
#define FLOAT_GT(x,v) (x > (v + FLOAT_EPSILON))                           
								 
// PURPOSE
//  Playstate enum, used for lifecycle management.
enum
{
	AUD_SOUND_DORMANT,
	AUD_SOUND_PREPARING,
	AUD_SOUND_PLAYING,
	AUD_SOUND_WAITING_TO_BE_DELETED,
	AUD_SOUND_STATE_MAX
};

// PURPOSE
//  Predelay state enum, used to manage the stages of a sound's predelay
enum
{
	AUD_PREDELAY_NOT_STARTED,
	AUD_PREDELAY_ONGOING,
	AUD_PREDELAY_FINISHED
};

// PURPOSE
//	Used by the sound type profiler - the list of functions profiled
enum
{
	AUD_SOUND_UPDATE,
	AUD_SOUND_PREPARE,
	AUD_SOUND_PLAY,

	AUD_NUM_PROFILED_SOUND_FUNCTIONS,
};

enum { kSoundLengthUnknown = -1 };

// This forces the vptr into the same place regardless of platform.
// Note that NO subclasses of audSound can introduce new virtuals (except if they're
// marked AUD_VIRTUAL) or else the SPU will see a vptr after all and will get hosed.
#if !AUD_SOUND_USE_VIRTUALS
class audSoundBase 
{
#if __SPU
	u32 vptr;
#else
public:
	virtual ~audSoundBase() { }
#endif
};
#endif

#if __WIN32
#pragma warning(disable : 4265) // class has virtual functions, but destructor is not virtual
#endif
// PURPOSE
//  audSounds encapsulate sound metadata, and are managed in a pool. They can control subsounds, or waves.
//  They are managed by a combination of audEntities, audControllers, and the SoundManager.
class audSound 
#if !AUD_SOUND_USE_VIRTUALS
	: public audSoundBase
#endif
{
	friend class audSoundFactory;
	friend class audSoundManager;
	friend class audRequestedSettings; // req settings use the pool defined in audSound
	friend class audRemoteControl;
	friend class audController;
	friend class audSoundSyncSetMasterId;
	friend class audSoundSyncSetSlaveId;
public:

	enum {kMaxChildren = 8};
	enum {kPredelayStartThresholdMs = 15};

	enum VariableUsage
	{
		SetFromGame = 0,
		SetFromSounds,
		ReadOnly
	};

#ifndef AUD_VOICE_SPU

	bool IsInitialised() const
	{
		return m_SoundFlags.Initialized;
	}

	void SetIsInitialised()
	{
		m_SoundFlags.Initialized = true;
	}

	// PURPOSE
	//  Returns true if the sound should update its audEntity every frame
	bool ShouldUpdateEntity() const
	{
		SoundAssert(m_RequestedSettingsIndex != 0xff);
		return GetRequestedSettings()->ShouldUpdateEntity();
	}

	// PURPOSE
	//  Checks the AllowOrphaned flag - if unset, orphened sounds will be automatically stopped and deleted
	bool IsAllowOrphaned() const
	{
		SoundAssert(m_RequestedSettingsIndex != 0xff);
		return GetRequestedSettings()->IsAllowOrphaned();
	}

	void SetAllowOrphaned(const bool allowOrphaned)
	{
		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetAllowOrphaned(allowOrphaned);
	}

	u32 GetTimerId() const
	{
		return m_InitParams.TimerId;
	}

	bool HasPendingChildInstantation() const
	{
		return m_SoundFlags.PendingChildInstantation;
	}

protected:
	
	
	// PURPOSE
	//  Returns true if the sound is set to play next audio frame
	bool IsSetToPlay() const
	{
		return m_SoundFlags.SetToPlay;
	}

	bool IsEffectRouteValid() const
	{
		return m_SoundFlags.IsEffectRouteValid;
	}

	void SetToPlay()
	{
		m_SoundFlags.SetToPlay = true;
	}
	
	bool IsSetToBeKilled() const
	{
		return m_SoundFlags.SetToBeKilled;
	}

	void SetIsSetToBeKilled()
	{
		m_SoundFlags.SetToBeKilled = true;
	}

	bool IsSetToRelease() const
	{
		return m_SoundFlags.SetToRelease;
	}

	void SetIsSetToRelease()
	{
		m_SoundFlags.SetToRelease = true;
	}
public:

	bool IsBeingReleased() const
	{
		return m_SoundFlags.BeingReleased;
	}

	bool IsInReleasePhase() const
	{
		return m_SoundFlags.InReleasePhase;
	}

	bool IsReferencedByGame() const
	{
		if(m_RequestedSettingsIndex == 0xff)
		{
			return false;
		}
		else
		{
			return GetRequestedSettings()->IsReferencedDirectlyByGame();
		}
	}

#if __BANK
	void SetForceMuteEnabled(bool enabled);
#endif
	
protected:
	void SetIsBeingReleased()
	{
		m_SoundFlags.BeingReleased = true;
	}

	bool IsToBeStopped() const
	{
		return m_SoundFlags.SetToBeStopped;
	}

	void SetIsToBeStopped()
	{
		m_SoundFlags.SetToBeStopped = true;
	}

	bool IsWaitingForChildrenToRelease() const
	{
		return m_SoundFlags.WaitingForChildrenToRelease;
	}

	void SetIsWaitingForChildrenToRelease(const bool waiting)
	{
		m_SoundFlags.WaitingForChildrenToRelease = waiting;
	}

	bool IsSetToBeDestroyed() const
	{
		return m_SoundFlags.SetToBeDestroyed;
	}

	void SetToBeDestroyed()
	{
		m_SoundFlags.SetToBeDestroyed = true;
	}	

	void SetReferencedDirectlyByGame(const bool isReferenced)
	{
		if(m_RequestedSettingsIndex != 0xff)
		{
			GetRequestedSettings()->SetIsReferencedDirectlyByGame(isReferenced);
		}
	}
	
	// deprecated, but saves changing a lot of code
	bool GetCanDynamicallyLoad() const
	{
		return CanDynamicallyLoad();
	}

	bool CanDynamicallyLoad() const
	{
		return m_SoundFlags.CanDynamicallyLoad;
	}

	void SetCanDynamicallyLoad(const bool is)
	{
		m_SoundFlags.CanDynamicallyLoad = is;
	}

	void SetWaveLoadPriority(const u32 prio)
	{
		m_WaveLoadPriority = prio;
		audAssertf(prio == m_WaveLoadPriority, "Invalid wave load priority: %u", prio);
	}

	u32 GetWaveLoadPriority() const
	{
		return m_WaveLoadPriority;
	}
	
	// PURPOSE
	//	Returns true if start offset was requested as %
	bool IsRequestedStartOffsetPercentage() const
	{
		return m_SoundFlags.RequestedStartOffsetPercentage;
	}

	void SetIsRequestedStartOffsetPercentage(const bool is)
	{
		m_SoundFlags.RequestedStartOffsetPercentage = is;
	}
	
	bool WasSetToReleaseLastFrame() const
	{
		return m_SoundFlags.WasSetToReleaseLastFrame;
	}

	void SetWasSetToReleaseLastFrame(const bool was)
	{
		m_SoundFlags.WasSetToReleaseLastFrame = was;
	}

	void SetIsInReleasePhase()
	{
		m_SoundFlags.InReleasePhase = true;
	}

	// PURPOSE
	//	True if this sound is being removed from the hierarchy, and if so shouldnt delete its children in its
	//	destructor.
	bool IsBeingRemovedFromHierarchy() const
	{
		return m_SoundFlags.BeingRemovedFromHierarchy;
	}

	void SetIsBeingRemovedFromHierarchy()
	{
		m_SoundFlags.BeingRemovedFromHierarchy = true;
	}
	
	bool CanBeRemovedFromHierarchy() const
	{
		return m_SoundFlags.CanBeRemovedFromHiearchy;
	}

	void SetCanBeRemovedFromHierarchy(const bool can)
	{
		m_SoundFlags.CanBeRemovedFromHiearchy = can;
	}

	bool IsWaitingToBeRemovedFromHierarchy() const
	{
		return m_SoundFlags.IsWaitingToBeRemovedFromHierarchy;
	}

	void SetIsWaitingToBeRemovedFromHierarchy()
	{
		m_SoundFlags.IsWaitingToBeRemovedFromHierarchy = true;
	}

	bool CanDeleteRequestedSettings() const
	{
		return m_SoundFlags.CanDeleteRequestedSettings;
	}

	void SetCanDeleteRequestedSettings(const bool can)
	{
		m_SoundFlags.CanDeleteRequestedSettings = can;
	}
	
	bool HasDynamicChildren() const
	{
		return m_SoundFlags.HasDynamicChildren;
	}

	void SetHasDynamicChildren()
	{
		m_SoundFlags.HasDynamicChildren = true;
	}
	
	bool ShouldFinishWhenEnvelopeAttenuationFull() const
	{
		return m_SoundFlags.ShouldFinishWhenEnvelopeAttenuationFull;
	}

	void SetShouldFinishWhenEnvelopeAttenuationFull(const bool finish)
	{
		m_SoundFlags.ShouldFinishWhenEnvelopeAttenuationFull = finish;
	}

	s32 GetMetadataPredelay() const
	{
		return m_MetadataPredelay;
	}

	s32 GetVariablePredelay() const;

	u32 GetMetadataFlags() const
	{
		return m_MetadataFlags;
	}

	f32 GetVariableValue(audVariableHandle handle)const;
	void SetVariableValue(audVariableHandle handle, f32 value);
	f32 ComputeDynamicValue(audDynamicValueId type)const;

private: 

	audEntity *_GetEntity(const audRequestedSettings *reqSets)
	{
		if(reqSets->IsEntityPtrInvalid())
		{
			return NULL;
		}
		return m_Entity;
	}

	audSound** _GetEntitySoundReference(const audRequestedSettings *reqSets)
	{
		if(reqSets->IsEntityRefPtrInvalid())
		{
			return NULL;
		}
		return m_EntitySoundReference;
	}

	audTracker *_GetTracker(const audRequestedSettings *reqSets)
	{	
		if(reqSets->IsTrackerPtrInvalid())
		{
			return NULL;
		}
		return m_Tracker;
	}

	bool IsDestructing() const
	{
		return m_SoundFlags.IsDestructing;
	}

	void SetIsDestructing()
	{
		m_SoundFlags.IsDestructing = true;
	}
	
#if !__SPU
	// PURPOSE
	//	Initializes pool
	static void InitClass(u32 soundSlotSize, u32 numSoundSlotsPerBucket, u32 requestedSettingsSlotSize, u32 numRequestedSettingsSlotsPerBucket,u32 numBuckets, u32 numReservedBuckets);

	// PURPOSE
	//  Shuts down pool
	static void ShutdownClass();
#endif // !__SPU

	// PURPOSE
	//	Create debug widgets
#if __BANK
	static void AddWidgets(bkBank &bank);
#endif

public:

	// PURPOSE
	//  After creating a sound, an audio entity must call Prepare() on it before requesting it be played,
	//  as this phase ensures that all required resources are loaded.
	//  The sound should only be played if the returned state is AUD_PREPARED. Any loading triggered by Prepare()
	//  is handled asynchronously.
	// PARAMS
	//	waveSlot - the slot that the sound should use for any dynamically loaded wave content
	//	allowLoad - if true then the sound will request a bank/wave load if the wave resource it requires isn't
	//				loaded into the specified slot.
	//  priority - defaults to 0, larger numbers translate to higher priority in the audio stream queue
	// NOTES
	//	Statically loaded slots are searched first so if a sound is to use statically loaded content it's valid
	//	to specify no waveSlot to Prepare()
	// RETURNS
	//	audPrepareState 
	audPrepareState Prepare(audWaveSlot *waveSlot = NULL, const bool allowLoad = false, const u32 priority = 0);
		
	// PURPOSE
	//  Requests the sound be played. This will be picked up on the next audio frame. After creating a sound, game code
	//  should ensure Prepare() returns AUD_PREPARED before calling Play().
	void Play();

	// PREPARE
	//  Prepares the sound and then plays it, if it Prepares within the time limit.
	// PARAMS
	//	waveSlot - the slot that the sound should use for any dynamically loaded wave content
	//	allowLoad - if true then the sound will request a bank/wave load if the wave resource it requires isn't
	//				loaded into the specified slot.
	//  timeLimit - the time limit after which if the sound has not prepared, it will not get played. Use 0 to never
	//              wait for loading, and -1 for the max time limit (currently 30 seconds).
	//	priority - defaults to 0, larger numbers translate to higher priority in the audio stream queue
	// NOTES
	//	Will assert and clamp any specified time limit out with the allowed range, currently 0 to 30000ms
#if !__SPU
	void PrepareAndPlay(audWaveSlot* waveSlot = NULL, bool allowLoad = false, s32 timeLimit = 0, const u32 priority = 0);
#endif

	// PURPOSE
	//  Flags the sound to Stop, which puts it into its release phase. This is the usual way sounds should be stopped
	//  by game code, as uses the RAVE designed stopping behaviour.
	// NOTES
	//	DO NOT call this from sounds - they should use _Stop()
	void Stop();

	// PURPOSE
	//  Flags the sound to Stop, which puts it into its release phase.
	//  This also removes any persistent reference back to the audEntity in the sound, so it can be instantly forgotten. 
	//  It is not safe to use the ptr to this sound at any point after calling this function.
	// PARAMS
	//	continueUpdatingEntity - if set to true, this sound will keep its pointer to its owning entity and use
	//							 that pointer to continue calling the entity's UpdateSound() method.
	void StopAndForget(bool continueUpdatingEntity = false);

	///////////////////////////////////////////////////////
	// These fns are only ever called from the game thread
	void SetClientVariable(f32 val)
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetClientVariable(val);
	}
	void SetClientVariable(u32 val)
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetClientVariable(val);
	}	
	void GetClientVariable(f32 &val)
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->GetClientVariable(val);
	}
	void GetClientVariable(u32 &val)
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->GetClientVariable(val);
	}
	
	// PURPOSE
	//  Gets a ptr to the sound's requested settings structure, should it have one.
	audRequestedSettings *GetRequestedSettings() const
	{
		return GetRequestedSettingsFromIndex(m_InitParams.BucketId, m_RequestedSettingsIndex);
	}

	// PURPOSE
	//	Returns the requested volume, ie the last volume passed to SetRequestedVolume()
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	f32	GetRequestedVolume() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetVolume();
	}
	// PURPOSE
	//	Sets the requested volume
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedVolume(const f32 vol)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		SoundAssert(FPIsFinite(vol));
		GetRequestedSettings()->SetVolume(vol);
	}

	// PURPOSE
	//	Returns the requested post-submix volume attenuation.
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	f32	GetRequestedPostSubmixVolumeAttenuation() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetPostSubmixVolumeAttenuation();
	}
	// PURPOSE
	//	Sets the requested post-submix volume attenuation
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedPostSubmixVolumeAttenuation(const f32 vol)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		SoundAssert(FPIsFinite(vol)); 
		GetRequestedSettings()->SetPostSubmixVolumeAttenuation(vol);
	}

	// PURPOSE
	//	Returns the requested volumeCurveScale (rolloff)
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	f32	GetRequestedVolumeCurveScale() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetVolumeCurveScale();
	}
	// PURPOSE
	//	Sets the requested volumeCurveScale (rolloff)
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedVolumeCurveScale(const f32 volCurveScale)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		SoundAssert(FPIsFinite(volCurveScale));
		GetRequestedSettings()->SetVolumeCurveScale(volCurveScale);
	}

	// PURPOSE
	//	Returns the requested pitch offset in cents
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	s32	GetRequestedPitch() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetPitch();
	}
	// PURPOSE
	//	Sets the requested pitch offset
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedPitch(const s32 pitch)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetPitch(pitch);
	}
	// PURPOSE
	//	Returns the requested frequency offset - a ratio, so 2.0f is an octave up.
	//  In practice this just converts from pitch (in cents) as this is how freq is stored internally.
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	f32	GetRequestedFrequencyRatio() const;

	// PURPOSE
	//	Sets the requested frequency offset - a ratio, so 2.0f is an octave up.
	//  In practice this just converts to pitch (in cents) as this is how freq is stored internally.
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedFrequencyRatio(const f32 frequencyRatio);

	// PURPOSE
	//	Returns the requested pan value in degrees
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	s32	GetRequestedPan() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetPan();
	}

	// PURPOSE
	//	Sets the requested pan value
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedPan(const s32 pan)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetPan(pan);
	}

	// PURPOSE
	//	Returns the requested Position vector
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	Vec3V_Out GetRequestedPosition() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetPosition();
	}

#if __SOUND_DEBUG_DRAW
	// PURPOSE
	//	Returns the Position of this sound for debug purposes
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	AUD_VIRTUAL Vec3V_Out GetDebugPositionOffset() const
	{
		return Vec3V(V_ZERO);
	}
#endif

	// PURPOSE
	//	Sets the requested Position vector
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedPosition(Vec3V_In pos)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetPosition(pos);
	}

	void SetRequestedPosition(const Vector3 & pos)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SetRequestedPosition(VECTOR3_TO_VEC3V(pos));
	}

	// PURPOSE
	//	Sets the requested Orientation
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedOrientation(QuatV_In ori)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetOrientation(ori);
	}

	// PURPOSE
	//	Sets the requested Orientation
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedOrientation(audCompressedQuat ori)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetOrientation(ori);
	}
	
	void SetRequestedMixerPlayTimeAbsolute(const u32 playTimeMixerFrame, const u16 subFrameOffset)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		SoundAssertf(subFrameOffset < kMixBufNumSamples, "Invalid subFrameOffset");
		GetRequestedSettings()->SetMixerPlayTime(playTimeMixerFrame, subFrameOffset);
	}

	// PURPOSE
	//	Sets the requested Orientation
	//  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	void SetRequestedOrientation(Mat34V_In ori)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		QuatV quat = QuatVFromMat34V(ori);
		GetRequestedSettings()->SetOrientation(quat);
	}

	// PURPOSE
	//	Returns the requested source effect wet/dry mix
	f32	GetRequestedSourceEffectWetMix() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetSourceEffectWetMix();
	}

	// PURPOSE
	//  Returns the requested source effect dry mix
	f32 GetRequestedSourceEffectDryMix() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetSourceEffectDryMix();
	}

	// PURPOSE
	//	Sets the source effect wet/dry mix
	// PARAMS
	//	wet - 1.0f full wet
	//  dry - 1.0f full dry
	// NOTES
	//  see also SetRequestedSourceEffectDryMix
	void SetRequestedSourceEffectMix(const f32 wet, const f32 dry)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetSourceEffectMix(wet, dry);
	}

	void SetRequestedShouldDisableRearAttenuation(TristateValue state)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetShouldDisableRearAttenuation(state);
	}

	void SetRequestedShouldDisableRearFiltering(TristateValue state)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetShouldDisableRearFiltering(state);
	}

	void SetRequestedShouldAttenuateOverDistance(TristateValue state)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetShouldAttenuateOverDistance(state);
	}

	void SetRequestedEntityVariableBlock(const int index)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetEntityVariableBlock(index);
	}

	void SetRequestedQuadSpeakerLevels(const f32* volumes)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		SoundAssert(volumes);
		GetRequestedSettings()->SetQuadSpeakerLevels(volumes);
	}

	void SetRequestedDopplerFactor(const f32 dopplerFactor)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetDopplerFactor(dopplerFactor);
	}

	f32 GetRequestedDopplerFactor() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetDopplerFactor();
	}

	void SetRequestedVirtualisationScoreOffset(const u32 scoreOffset)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetVirtualisationScoreOffset(scoreOffset);
	}

	u32 GetRequestedVirtualisationOffset() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetVirtualisationScoreOffset();
	}

	void SetRequestedSpeakerMask(const u8 speakerMask)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetSpeakerMask(speakerMask);
	}

	u8 GetRequestedSpeakerMask() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetSpeakerMask();
	}

	void SetRequestedListenerMask(const u8 listenerMask)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetListenerMask(listenerMask);
	}

	u8 GetRequestedListenerMask() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetListenerMask();
	}

	void SetRequestedEnvironmentalLoudness(const u8 environmentalLoudness)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetEnvironmentalLoudness(environmentalLoudness);
	}

	u8 GetRequestedEnvironmentalLoudness() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetEnvironmentalLoudness();
	}

	void SetRequestedEnvironmentalLoudnessFloat(const f32 environmentalLoudness)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetEnvironmentalLoudnessFloat(environmentalLoudness);
	}

	f32 GetRequestedEnvironmentalLoudnessFloat() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetEnvironmentalLoudnessFloat();
	}

	u32 GetRequestedLPFCutoff() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetLowPassFilterCutoff();
	}

	void SetRequestedLPFCutoff(const u32 cutoff)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->SetLowPassFilterCutoff(cutoff);
	}

	void SetRequestedLPFCutoff(const f32 cutoff)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SetRequestedLPFCutoff((u32)cutoff);
	}

	void SetRequestedHPFCutoff(const u32 cutoff)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);	
		GetRequestedSettings()->SetHighPassFilterCutoff(cutoff);
	}

	void SetRequestedHPFCutoff(const f32 cutoff)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SetRequestedHPFCutoff((u32)cutoff);
	}

	u32 GetRequestedHPFCutoff() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetHighPassFilterCutoff();
	}

	// PURPOSE
	//	Returns the current frequency smoothing rate
	//	  This is cached in a thread-safe way, to enable simultaneous read-writes between game and audio threads.
	f32 GetRequestedFrequencySmoothingRate() const
	{
		SoundAssert(m_RequestedSettingsIndex!=0xff);
		return GetRequestedSettings()->GetFrequencySmoothingRate();
	}

	// PURPOSE
	//	Sets the frequency smoothing rate, specified as max frequency scaling change per second
	void SetRequestedFrequencySmoothingRate(const f32 rate)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex!=0xff);
		GetRequestedSettings()->SetFrequencySmoothingRate(rate);
	}

	// PURPOSE
	//	Specifies that PCM sources in this sound hierarchy should provide master sync
	//	for the specified mixer sync id.
	void SetRequestedSyncMasterId(const u32 syncId)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetSyncMasterId(syncId);
	}

	// PURPOSE
	//	Specifies that PCM sources in this sound hierarchy should slave to the specified mixer sync id.
	void SetRequestedSyncSlaveId(const u32 syncId)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetSyncSlaveId(syncId);
	}

	// PURPOSE
	//	Specifies that the sound should not be culled when a system requests to clear the sound pool of all active sounds
	void SetShouldPersistOverClears(bool shouldPersist)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetShouldPersistOverClears(shouldPersist);
	}

	u32 GetTimeInitialised() const
	{
#if AUD_DEBUG_SOUNDS
		return GetVarianceCache()->TimeInitialised;
#else
		return ~0U;
#endif

	}
	// End of game thread only functions
	///////////////////////////////////////////////////////

	// PURPOSE
	//	Returns the requested predelay value in ms
	u32	GetPredelay() const
	{
		return m_RequestedPredelay;
	}
	
	
	// PURPOSE
	//	Returns the requested start offset
	s32		GetStartOffset() const
	{
		return m_RequestedStartOffset;
	}


	// PURPOSE
	//	Sets the release time
	// NOTES
	//	This will overwrite any metadata set release time.  It is only valid to call this function before a sound is releasing.
	//	-1 specifies no release
	void SetReleaseTime(const s32 releaseTime)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(!IsBeingReleased());
		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetReleaseTime(releaseTime);
	}

	// PURPOSE
	//	Returns the release time
	s32	GetReleaseTime() const
	{
		SoundAssert(m_RequestedSettingsIndex != 0xff);
		return GetRequestedSettings()->GetReleaseTime();
	}

	u32 GetSimpleReleaseTime()
	{
		return m_SimpleReleaseTime;
	}

	void InvalidateTracker(audRequestedSettings *reqSets);

	void InvalidateTracker()
	{
		DIAG_CONTEXT_MESSAGE(GetName());
		SoundAssert(m_RequestedSettingsIndex != 0xff);
		InvalidateTracker(GetRequestedSettings());
	}

	void InvalidateEntity()
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->InvalidateEntityPtr();
	}
	void InvalidateEntitySoundReference()
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->InvalidateEntityRefPtr();
	}

	void SetUpdateEntity(const bool shouldUpdateEntity)
	{
		DIAG_CONTEXT_MESSAGE(GetName());

		SoundAssert(m_RequestedSettingsIndex != 0xff);
		GetRequestedSettings()->SetUpdateEntity(shouldUpdateEntity);
	}

	// PURPOSE
	//	Sets the entity and entity sound reference pointers on this sound.
	// NOTES
	//	This should be called very infrequently as it has to block on all sound processing
	void SetEntityAndEntityReference(audEntity *entity, audSound **entitySoundRef);
	// PURPOSE
	//	Gets the tracker object used by this sound for tracking a game entity's position.
	audTracker *GetTracker()
	{
		return _GetTracker(GetRequestedSettings());
	}

	// PURPOSE
	//	Gets the audio entity that created this sound.
	audEntity *GetEntity() { return _GetEntity(GetRequestedSettings()); }
	
	// PURPOSE
	//	Gets the ptr to the pointer to this sound in the creating audEntity.
	// (ie *(sound->GetEntitySoundReference()) == sound)
	audSound **GetEntitySoundReference(void)
	{
		return _GetEntitySoundReference(GetRequestedSettings());
	}

	// PURPOSE
	//	Returns the allocated bucket id
	u8 GetBucketId() const
	{
		return m_InitParams.BucketId;
	}

	// PURPOSE
	//	Returns the current play time in milliseconds.
	// PARAMS
	//	timeInMs	- the amount of time since the sound was triggered to play. 
    u32 GetCurrentPlayTime(u32 timeInMs) const
	{
		if(m_TimeTriggered >= 0)
		{
			return (timeInMs - (u32)m_TimeTriggered);
		}
		else
		{
			return 0;
		}
	}
	
	// PURPOSE
	//	Returns the elapsed time since the sound was triggered, in milliseconds.
	u32 GetCurrentPlayTime() const;

	// PURPOSE
	//	Returns a pointer to this sounds cached wave slot
	audWaveSlot *GetWaveSlot() const;

	audDynamicValueId GetDynamicVariableHandleFromName(const u32 nameHash);

	bool FindVariableValue(u32 nameHash, f32& value);

	// PURPOSE
	//  Searches down the hierarchy for a variable of the correct name, and sets its value. 
	AUD_VIRTUAL void FindAndSetVariableValue(const u32 nameHash, const f32 newValue);
	void SetVariableValueDownHierarchyFromName(const char* name, const f32 newValue);
	void SetVariableValueDownHierarchyFromName(const u32 nameHash, const f32 newValue);

	// PURPOSE
	//  Returns the class id of the sound. The list of these is stored near the top of the autogenerated sounddefs.h.
	//  Checking this is useful just before you cast an audSound* to a specific sound-type, to call some
	//  custom function on it - getting metadata from a WaveMetadataMarkerSound for example.
	//  Because sound structures can be altered independently from code, it's risky to just assume the top level sound
	//  is of the correct type.
	u8 GetSoundTypeID() const
	{
		return m_SoundTypeId;
	}

	// TODO:
	//	need this for GTA, remove once we implement a better approach
	// PURPOSE
	//	Returns the duration of this sound in milliseconds including metadata predelay, start offset
	//	and children where appropriate
	s32 ComputeDurationMsIncludingStartOffsetAndPredelay(bool* isLooping);
#if !AUD_SOUND_USE_VIRTUALS
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		return InvokeComputeDurationMsExcludingStartOffsetAndPredelay(isLooping);
	}
#endif

	// PURPOSE
	//  Returns the sound's play state, for lifecycle management. See the Playstate enum at the top of this file.
	s32 GetPlayState() const
	{
		return static_cast<s32>(m_PlayState);
	}

	template<class _T> static const _T* DecompressMetadata(const Sound *compressedMetadata, Sound &uncompressedMetadata)
	{
		if(compressedMetadata && compressedMetadata->ClassId == _T::TYPE_ID)
		{
			return reinterpret_cast<const _T *>(DecompressMetadata_Untyped(compressedMetadata, uncompressedMetadata));
		}
		return NULL;
	}
	static const void *DecompressMetadata_Untyped(const Sound *compressedMetadata, Sound &uncompressedMetadata);

#if !__SPU
	static u32 ReserveBucket()
	{
		return sm_Pool.GetNextReservedBucketId();
	}
#endif

	// PURPOSE
	//	Returns a string containing this sound's name
	// NOTES
	//	This is only valid in __DEV builds
	const char *GetName() const;

	//***************************************************
	// internal public functions
#if AUD_SOUND_USE_VIRTUALS
	AUD_VIRTUAL void Pause(const u32 timeInMs)
	{
		BasePause(timeInMs);
	}
#endif

	// PURPOSE
	//  Cascades down the hierarchy, until the overriden version in an EnvironmentSound, which will ask its 
	//  voice to pause.
	void BasePause(const u32 timeInMs);

	void InvokePause(u32 timeInMs)
	{
		Assert(m_SoundTypeId > 0 && m_SoundTypeId <= AUD_NUM_SOUNDDEFS);

#if AUD_SOUND_USE_VIRTUALS
		Pause(timeInMs);
#else
		Assert(sm_SoundTypePauseFunctions[m_SoundTypeId-1]);
		INVOKE_MEMBER_FN_1(sm_SoundTypePauseFunctions[m_SoundTypeId-1], timeInMs);
#endif
	}
#ifdef AUD_IMPL


	// PURPOSE
	//  This base sound function deals with envelopes, predelays, etc, then calls the overriden AudioPlay()
	void _ManagedAudioPlay(u32 timeInMs, audSoundCombineBuffer combineBuffer);

	// PURPOSE
	//  This base sound function deals with envelopes, predelays, etc, then calls the overriden AudioUpdate()
	bool _ManagedAudioUpdate(u32 timeInMs, audSoundCombineBuffer combineBuffer);

	// PURPOSE
	//  This base sound function deals with envelopes, predelays, etc, then calls the overriden AudioKill()
	void _ManagedAudioKill();

	// Instantly destroys the sound, with no nice release behaviour. It may be called at any stage of prepare/playback
	void _ManagedAudioDestroy();

	audPrepareState _ManagedAudioPrepare(audWaveSlot* waveSlot, const bool allowLoad, const bool checkStateOnly, const u32 priority);
	
	audPrepareState _ManagedAudioPrepareFromSoundManager();

	void _ActionStopRequest();

	void _Stop();

	// PURPOSE
	//	Returns the duration of this sound in milliseconds including metadata predelay, start offset
	//	and children where appropriate
	s32 _ComputeDurationMsIncludingStartOffsetAndPredelay(bool* isLooping);

	// PURPOSE
	//  Takes the metadata and requested StartOffset and Predelay and works out the combined values for playback
	void	_ComputePlaybackPredelayAndStartOffset();

	// PURPOSE
	//  Gets a list of sounds that match the given sound type. This method of searching for sounds is (currently)
	//  only available to other sounds in the hierarchy, on the audio thread. As the same type of sound may exist
	//  multiple times in the hierarchy, an array of matching sounds is returned. 
	//  Sounds that dynamically create children will not return those children even if their types match, unless
	//	allowDynamicChildren is specified as true. This is to protect against the risk of these children being
	//	deleted at any time. (Although actually it'd be safe to control those children in the same fn...)
	//  Nothing will be returned if hierarchy is flagged to allow sound removal for optimisation. Again, this 
	//  would actually be safe for immediate use - may change this.)
	// PARAMS
	//  soundType				- type of the sound to search for - use emum from sounddefs.h
	//  soundList				- ptr to an array of results
	//  maxFoundSound			- the size of that array. If full, no more hits will be added.
	//	allowDynamicChildren	- If true, dynamic children will be included in the search. Caution should observed on
	//								setting this flag as these children may not remain valid on consecutive frames.
	void _SearchForSoundsByType(u32 soundType, audSound** soundList, int maxFoundSounds, bool allowDynamicChildren = false);

	
	// PURPOSE
	//  Puts the sound into the release phase of its envelope. This is only called internally now, as Stop()
	//  is used externally.
	void _Release()
	{
		// Set up the requested release envelope curve
		SetIsSetToRelease();
	}

	// PURPOSE
	//  For a child sound, returns its parent sound. Null for sounds with no parent.
	AUD_INLINE audSound *_GetParent() const
	{
		return GetSoundFromIndex(m_InitParams.BucketId, m_ParentIndex);
	}

	void Pause(const u32 timeInMs)
	{
		BasePause(timeInMs);
	}
	void ActionReleaseRequest(const u32 timeInMs)
	{
		BaseActionReleaseRequest(timeInMs);
	}
	
	static void sPause(audSound *_this, u32 timeInMs)
	{
		_this->Pause(timeInMs);
	}
	
	static void sActionReleaseRequest(audSound *_this, u32 timeInMs)
	{
		_this->ActionReleaseRequest(timeInMs);
	}

	// PURPOSE
	//	Sets the internal predelay, used to cascade residual predelays down the sound hierarchy
	void	_SetRequestedInternalPredelay(const u32 predelayMs)
	{
		SoundAssertf(GetPlayState() == AUD_SOUND_DORMANT || GetPlayState() == AUD_SOUND_PREPARING, "Playstate: %i", GetPlayState());
		m_RequestedPredelay = predelayMs;
	}

	// PURPOSE
	//  Sets the sound's requested settings structure.
	void _SetRequestedSettingsIndex(u8 index)
	{
		Assert(m_RequestedSettingsIndex == 0xff);
		if(m_RequestedSettingsIndex == 0xff)
		{
			m_RequestedSettingsIndex = index;
			SetCanDeleteRequestedSettings(false);
		}
	}
	
	// PURPOSE
	//	Stores the newly instantiated child sound in the child sound array and invokes the callback
	void _HandleDeferredChildInstantiation(const u32 timeInMs, const u32 childIndex, const audSound *child)
	{
		m_SoundFlags.PendingChildInstantation = false;
		if(childIndex != ~0U)
		{
			SetChildSound(childIndex, child);
		}
		ChildSoundCallback(timeInMs, childIndex);
	}


	// PURPOSE
	//  Sets the sound to instantly stop (not be released) on the next audio frame
	void Kill()
	{
		SetIsSetToBeKilled();
	}

	void SetStartOffset(const s32 startOffset, const bool isPercentage = false);

	// PURPOSE
	//  Gets the handle of the named (well, hashed) variable. Passes down the request to children if it doesn't
	//  have its own variable block
	// RETURNS
	//  A ptr to the variable - NULL if the variable was not found.
	AUD_VIRTUAL audVariableHandle _FindVariableDownHierarchy(const u32 nameHash, const VariableUsage usage);
	
	// virtual internal functions cant be hidden by the AUD_IMPL macro
#endif // AUD_IMPL
	// PURPOSE
	//  Gets the handle of the named (well, hashed) variable. Passes up the request to parent if it doesn't
	//  have its own variable block, and will ultimately search the sound managers global variable list if no
	//	match is found.
	// NOTES
	//	Only valid to call during Init() so will always run on PPU
	// RETURNS
	//  A ptr to the variable - NULL if the variable was not found.
	AUD_VIRTUAL audVariableHandle _FindVariableUpHierarchy(const u32 nameHash);

#if AUD_DEBUG_SOUNDS
	u32 GetNameTableOffset() const
	{
		return m_BaseMetadata->NameTableOffset;
	}
#endif // AUD_DEBUG_SOUNDS

	// PURPOSE
	//	Route all audSound allocation requests through the custom sound pool
	void *operator new(size_t size, u32 bucketId)
	{
		return sm_Pool.AllocateSoundSlot(size, bucketId);
	}

	void operator delete(void *ptr)
	{
		FastAssert(ptr);
		audSound *sound = (audSound*)ptr;
		if(sound)
		{
			sound->Delete();
		}
	}

#if !__SPU
	audSound();
#endif

	// NOTE: This will be virtual on the PPU, but not the SPU, because of audSoundBase.
	AUD_VIRTUAL ~audSound();


#if __SOUND_DEBUG_DRAW
	void DebugDraw(audDebugDrawManager &drawManager, const bool drawDynamicChildren, const bool drawHierarchyOnly) const;
	// RETURNS
	//	true if children should be drawn by default code
	AUD_VIRTUAL bool DebugDrawInternal(const u32 UNUSED_PARAM(timeInMs), audDebugDrawManager &UNUSED_PARAM(drawManager), const bool UNUSED_PARAM(drawDynamicChildren)) const { return true; }
#endif

#endif // AUD_VOICE_SPU

	static u32 sm_CurrentSyncMasterId;
	static u32 sm_CurrentSyncSlaveId;

	static u32 GetCurrentSyncMasterId() { return sm_CurrentSyncMasterId; }
	static u32 SetCurrentSyncMasterId(const u32 syncId) 
	{
		const u32 ret = sm_CurrentSyncMasterId;
		sm_CurrentSyncMasterId = syncId;
		return ret;
	}
	static u32 GetCurrentSyncSlaveId() { return sm_CurrentSyncSlaveId; }
	static u32 SetCurrentSyncSlaveId(const u32 syncId)
	{
		const u32 ret = sm_CurrentSyncSlaveId;
		sm_CurrentSyncSlaveId = syncId;
		return ret;
	}

	static audSoundPool &GetStaticPool()
	{
		return sm_Pool;
	}
protected: 
	static audSoundPool sm_Pool;


#ifdef GTA_REPLAY_RAGE
public:
	void	SetReplayId( s32 id, bool* pIsTaken, void* pReplaySoundHandle = NULL )	
	{ 
		m_replayId				= id;
		m_pIsReplayIdTaken		= pIsTaken;
		m_pReplaySoundHandle	= pReplaySoundHandle;
	}
	s32		GetReplayId() const		{ return m_replayId;			}
	bool	IsRecorded()  const		{ return (m_replayId != -1);	}
protected:
#endif

#ifndef AUD_VOICE_SPU

#if !__SPU
	// PURPOSE
	//	Initialises an instance of the specific sound type.
	// PARAMS
	//  pMetadata - ptr to this sound's metadata
	//  initParams - a structure containing runtime initialisation parameters. See soundcontrol.h for details.
	//	scratchInitParams - a structure containing runtime init data that doesn't persist outside of the Init() process
	AUD_VIRTUAL bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams* scratchInitParams);

	// PARAMS
	//	Initialises this sound as a top level parent sound
	void InitAsParent(const audSoundParentInitParams *parentInitParams);

	// PARAMS
	//	If this is a parent sound, this is called post Init
	//	Notes: Added because tracker reference counts never decrement if Init fails and the sound deletes (CMS 6/2/2009)
	void PostInitAsParent(const audSoundParentInitParams *parentInitParams);

	void PopulateScratchInitParams(audSoundScratchInitParams *scratchInitParams, Sound &uncompressedMetadata);
#endif

#if __ASSERT
#define ValidateInvoke(array) do { \
	Assert(m_SoundTypeId > 0 && m_SoundTypeId <= AUD_NUM_SOUNDDEFS); \
	Assert(array[m_SoundTypeId-1]); \
	} while (0)
#else
#define ValidateInvoke(array) do { \
	TrapZ(m_SoundTypeId); \
	TrapGT(m_SoundTypeId,AUD_NUM_SOUNDDEFS); \
	} while (0)
#endif


	// PURPOSE
	//	Returns the duration of this sound in milliseconds, excluding predelay and start offset.
	//  This function should be overriden by new sound types, which need to calculate this value themselves,
	//  based on the knowledge of how they play child sounds, 
	//  and by calling _ComputeDurationMsIncludingStartOffsetAndPredelay() to determine their children's length.
#if AUD_SOUND_USE_VIRTUALS
	AUD_VIRTUAL s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping) = 0;
#endif
	s32 InvokeComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping)
	{
#if AUD_SOUND_USE_VIRTUALS
		return ComputeDurationMsExcludingStartOffsetAndPredelay(isLooping);
#else
		FastAssert(m_SoundTypeId > 0 && m_SoundTypeId <= AUD_NUM_SOUNDDEFS);
		FastAssert(sm_SoundTypeComputeDurationMsExcludingStartOffsetAndPredelayFunctions[m_SoundTypeId-1]);
		return INVOKE_MEMBER_FN_1(sm_SoundTypeComputeDurationMsExcludingStartOffsetAndPredelayFunctions[m_SoundTypeId-1], isLooping);
#endif
	}


	// PURPOSE
	//	Actually do something associated with starting the sound - kicking off children or starting voices.
	//  This function must be overriden for all sound types.
#if AUD_SOUND_USE_VIRTUALS
	AUD_VIRTUAL void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer) = 0;
#endif
	void InvokeAudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
#if AUD_SOUND_USE_VIRTUALS
		AudioPlay(timeInMs);
#else
		ValidateInvoke(sm_SoundTypePlayFunctions);
		INVOKE_MEMBER_FN_2(sm_SoundTypePlayFunctions[m_SoundTypeId-1], timeInMs, combineBuffer);
#endif
	}

	// PURPOSE
	//  Updates a playing Sound, cascades down to playing subsounds.
	//  This function must be overriden for all sound types.
	// RETURNS
	//  true if the sound is in the PLAYING state.
#if AUD_SOUND_USE_VIRTUALS
	AUD_VIRTUAL bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer) = 0;
#endif
	bool InvokeAudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
#if AUD_SOUND_USE_VIRTUALS
		return AudioUpdate(timeInMs,combineBuffer);
#else
		ValidateInvoke(sm_SoundTypeUpdateFunctions);
		return INVOKE_MEMBER_FN_2(sm_SoundTypeUpdateFunctions[m_SoundTypeId-1],timeInMs,combineBuffer);
#endif
	}

	// PURPOSE
	//  Actually do something associated with stopping the sound - stop children or kill voices.
	//  This acts synchronously, so at the end of an AudioKill call, the sound should be safe to delete
	//  if it's no longer being referenced by game code.
	//  This function must be overriden for all sound types.
#if AUD_SOUND_USE_VIRTUALS
	AUD_VIRTUAL void AudioKill() = 0;
#endif
	void InvokeAudioKill()
	{
#if AUD_SOUND_USE_VIRTUALS
		AudioKill();
#else
		ValidateInvoke(sm_SoundTypeKillFunctions);
		INVOKE_MEMBER_FN(sm_SoundTypeKillFunctions[m_SoundTypeId-1]);
#endif
	}
	// PURPOSE
	//	prepare the sound for playing including loading wave resources
	//  This function must be overriden for all sound types.
	// RETURNS
	//	audPrepareState
#if AUD_SOUND_USE_VIRTUALS
	AUD_VIRTUAL audPrepareState AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly) = 0;
#endif
	audPrepareState InvokeAudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
	{
#if AUD_SOUND_USE_VIRTUALS
		return AudioPrepare(waveSlot, checkStateOnly);
#else
		ValidateInvoke(sm_SoundTypePrepareFunctions);
		return INVOKE_MEMBER_FN_2(sm_SoundTypePrepareFunctions[m_SoundTypeId-1],waveSlot,checkStateOnly);
#endif
	}

		// PURPOSE
	//  Called on the first frame SetToRelease is true. Overridden for EnvelopeSound.
	void BaseActionReleaseRequest(const u32 timeInMs);
#if AUD_SOUND_USE_VIRTUALS
	AUD_VIRTUAL void ActionReleaseRequest(const u32 timeInMs)
	{
		BaseActionReleaseRequest(timeInMs);
	}
#endif
	void InvokeActionReleaseRequest(const u32 timeInMs)
	{
#if AUD_SOUND_USE_VIRTUALS
		ActionReleaseRequest(timeInMs);
#else
		ValidateInvoke(sm_SoundTypeActionReleaseRequestFunctions);
		return INVOKE_MEMBER_FN_1(sm_SoundTypeActionReleaseRequestFunctions[m_SoundTypeId-1],timeInMs);
#endif
	}

public:
	// PURPOSE
	//  Gets the top level parent sound in a hierarchy.
	audSound *GetTopLevelParent() const;

	const Sound *GetBaseMetadata() const
	{
		return m_BaseMetadata;
	}

	//	PURPOSE
	//	Returns the amount of predelay time remaining before sound starts.
	u32 GetPredelayRemainingMs() const;

	static u32 ApplyVariance(const u32 val, const u32 variance, const u32 limit)
	{
		return (u32)Min<s32>(limit,((s32)val + ComputeOffsetFromVariance(Min<s32>(val, variance))));
	}
protected:
	
	// PURPOSE
	//	Flag accessor function
	bool IsStartOffsetPercentage() const;
	// PURPOSE
	//	Flag accessor function
	bool ShouldVirtualiseAsGroup() const;	

	// PURPOSE 
	//  Sets the time at which we should give up Preparing and bin the sound
	//  Note that all prepare times are treated as real times, not adjusted for pausing.
	void SetPrepareTimeLimit(u32 timeLimit)
	{
		// Remember, this is used to compare the actual time, not a limit.
		m_PrepareTimeLimit = timeLimit;
	}

	// PURPOSE 
	//  Gets the time at which we should give up Preparing and bin the sound
	//  Note that all prepare times are treated as real times, not adjusted for pausing.
	u32 GetPrepareTimeLimit() const
	{
		return m_PrepareTimeLimit;
	}

	// PURPOSE
	//	Returns a pointer to this sounds metadata
	// NOTE
	//	
	AUD_INLINE const void *GetMetadata() const
	{
#if AUD_DEBUG_SOUNDS
		SoundAssert(sm_Pool.IsBucketProcessingChildSoundRequests(m_InitParams.BucketId) || m_SoundFlags.IsInitialising);
#endif
		return (u8*)m_BaseMetadata + m_TypeMetadataOffset;
	}

#if __BANK && !__SPU
	AUD_INLINE const void *GetMetadataForRAVEChanges() const
	{
		return (u8*)m_BaseMetadata + m_TypeMetadataOffset;
	}
#endif

	// PURPOSE
	//	Returns a string containing this sounds hierarchy
	// PARAMS
	//	multiline - if true then hierarchy elements will be separated with \n
	// NOTES
	//	This is only valid in __DEV builds
#if AUD_DEBUG_SOUNDS
	char *ComputeHierarchyString(bool multiline=false) const;
#endif

	// PURPOSE
	//	Generates a SoundAssert message with sound hierarchy
	// NOTES
	//	Use the SoundAssert() macro
	int SoundAssertFailed(const char *msg, const char *exp, const char *file, const int line) const;
	int SoundAssertFailed(const char *exp, const char *file, const int line, const char *fmt,...) const;

	// PURPOSE
	//  Returns the Predelay state - AUD_PREDELAY_NOT_STARTED, AUD_PREDELAY_ONGOING, AUD_PREDELAY_FINISHED
	s32 GetPredelayState() const
	{
		return static_cast<s32>(m_PredelayState);
	}

	// PURPOSE
	//  Sets the Predelay state
	void SetPredelayState(s32 state);

	// PURPOSE
	//  Returns the time at which the sound's predelay will finish, and the sound proper start playing
	u32 GetPredelayFinishTime() const;

	u32 GetPlaybackStartOffset() const
	{
		return m_PlaybackStartOffset;
	}

	u32 GetPredelayRemainingMs(const u32 timeInMs) const
	{
		const u32 finishTime = GetPredelayFinishTime();
		return (finishTime <= timeInMs) ? 0 : finishTime - timeInMs;
	}

	// PURPOSE
	//  Returns true if any of the child sounds are in the PLAYING state
	bool AreChildrenPlaying() const;

	// PURPOSE
	//  Calls ManagedAudioStop() on all the child sounds.
	virtual void ManagedAudioStopChildren();

	// PURPOSE
	//	Called when a sound type serves no purpose in the hierarchy other than updating its child -
	//	it is removed from the chain and the parent will update the child directly.
	// NOTES
	//	The child sound to be linked directly to this sounds parent _MUST_ be child sound 0
	void RemoveMyselfFromHierarchy();

	// PURPOSE	
	//	Returns the child sound stored at the specified index in the m_ChildSounds array.
	// NOTES
	//	Index is child sound index, not sound pool index
	inline audSound *GetChildSound(const u32 index) const
	{
		return GetSoundFromIndex(m_InitParams.BucketId, m_ChildSounds[index]);
	}

	// PURPOSE
	//	Returns true if there is a child sound stored in the specified index
	inline bool HasChildSound(const u32 index) const
	{
		return m_ChildSounds[index] != 0xff;
	}

	// PURPOSE
	//	Returns the number of child sounds owned by this sound
	inline u8 GetNumChildSounds() const
	{
		u8 count = 0;
		for(u32 i = 0; i < kMaxChildren; i++)
		{
			if(HasChildSound(i))
			{
				count++;
			}
		}
		return count;
	}

	// PURPOSE
	//	Stores a pointer to the specified sound at the specified index in the m_ChildSounds array
	inline void SetChildSound(const u32 index, const audSound *sound)
	{
		SoundAssert(index < audSound::kMaxChildren);
		m_ChildSounds[index] = GetIndexFromSound(m_InitParams.BucketId, sound);
	}

	void DeleteChild(const u32 index)
	{
		SoundAssert(index < audSound::kMaxChildren);
		audSound* childSound = GetChildSound(index);
		SoundAssert(childSound);		
		if(childSound)
		{
			delete childSound;
			SetChildSound(index, NULL);
		}
	}

	// PURPOSE
	//	This is called on the parent sound to let it know that the child sound instantiation is requested
	//	has completed.
	// NOTES
	//	This executes on the PPU at the end of the audio frame for this bucket, so keep any processing to a minimum
	AUD_VIRTUAL void ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 UNUSED_PARAM(childIndex)){}

	// PURPOSE
	//	Request a dynamic child instantation asynchronously.  All dynamic children must be instantiated through this
	//	function, as sound instantiation cannot happen on an SPU.  The actual instantiation will occur at the end of
	//	the audio frame, and ChildSoundCallback() will be called on this parent sound, allow the parent to do any
	//	additional setup.
	void RequestChildSoundFromOffset(const audMetadataRef offset, const u32 childIndex, const audSoundScratchInitParams *scratchInitParams);

#if __SPU
	// PURPOSE
	//	Requests that the ChildSoundCallback function be executed later in the frame on the PPU.
	void RequestPPUCallback();
#endif

	//////////////////////////////////////////////////////////////////////////////////////////////
	// ComputePlayback functions - called during a push to calculate this sounds contribution

	// PURPOSE
	//	Returns the metadata pan combined with parent and requested
	AUD_INLINE s32	ComputePlaybackPan(const s32 parentPan, const s32 requestedPan) const
	{
		s32 playbackPan = -1;

		if(!m_ParentOverrides.BitFields.PanOverridesParent)
		{
			playbackPan = parentPan;
		}
		
		playbackPan = CombinePan(playbackPan, m_MetadataPan);
		playbackPan = CombinePan(playbackPan, requestedPan);

		if(playbackPan!=-1)
		{
			playbackPan %= 360;
		}
		return playbackPan; 
	}												

	// PURPOSE
	//	Returns combo of metadata, requested and parent volume
	AUD_INLINE f32	ComputePlaybackVolume(const float parentVol, const float requestedVol) const
	{
		float volume = m_MetadataVolume + requestedVol;
		
		if(!m_ParentOverrides.BitFields.VolumeOverridesParent)
		{
			volume += parentVol;
		}

		return volume;
	}
	
	// PURPOSE
	//	Returns combo of parent and requested post-submix volume
	AUD_INLINE f32	ComputePlaybackPostSubmixVolumeAttenuation(const float parentVolAttenuation, const float requestedPostSubmixAtten) const
	{
		return parentVolAttenuation + requestedPostSubmixAtten;
	}

	// PURPOSE
	//	Returns the metadata pitch combined with parent and requested pitch
	AUD_INLINE s32	ComputePlaybackPitch(const s32 parentPitch, const s32 requestedPitch) const
	{
		s32 pitch = requestedPitch;

		if(!m_ParentOverrides.BitFields.PitchOverridesParent)
		{
			pitch += parentPitch;
		}

		pitch += m_MetadataPitch;

		return pitch;
	}


	// PURPOSE
	//  Returns the low pass filter cutoff frequency, combining parent, requested and metadata values
	AUD_INLINE u32 ComputePlaybackLPFCutoff(const u32 parentCutoff, const u32 requestedCutoff) const
	{
		u32 cutoff = m_MetadataLPFCutoff;

		cutoff = Min(cutoff, requestedCutoff);

		if(!m_ParentOverrides.BitFields.LPFCutoffOverridesParent)
		{
			cutoff = Min(cutoff, parentCutoff);
		}
		return cutoff;
	}


	// PURPOSE
	//  Returns the high pass filter cutoff frequency, combining parent, requested and metadata values
	AUD_INLINE u32 ComputePlaybackHPFCutoff(const u32 parentCutoff, const u32 requestedCutoff) const
	{
		u32 cutoff = m_MetadataHPFCutoff;
		
		cutoff = Max(cutoff,  requestedCutoff);
		
		if(!m_ParentOverrides.BitFields.HPFCutoffOverridesParent)
		{
			cutoff = Max<u32>(cutoff, parentCutoff);
		}
		return cutoff;
	}
		
	// PURPOSE
	//	returns the group ID for virtualisation purposes, taking into account parent value. This 
	// RETURNS
	//	top level group id, defaultGroup if there is no group
	u32		ComputeVirtualisationGroupID(u32 defaultGroup = 0);
	
	// PURPOSE
	//  Utility function, to combine two pans. Negative pans are presumed to mean the pan is not set and should be ignored.
	static AUD_INLINE s32 CombinePan(s32 pan1, s32 pan2)
	{	
		if(pan1 != -1)
		{
			if(pan2 != -1)
			{
				return pan1 + pan2;
			}
			else
			{
				return pan1;
			}
		}
		else
		{
			return pan2;
		}
	}

	static AUD_INLINE audSound* GetSoundFromIndex(const u32 bucket, const u32 index)
	{
		return (audSound*)(index!=0xff?audSound::sm_Pool.GetSoundSlot(bucket, index):NULL);
	}

	static AUD_INLINE u8 GetIndexFromSound(const u32 bucket, const audSound *sound)
	{
		return (u8)(sound?audSound::sm_Pool.GetSoundSlotIndex(bucket, sound):0xff);
	}

	void SetPrepareFailed()
	{
		m_SoundFlags.PrepareFailed = true;
	}

	bool HasPrepareFailed() const
	{
		return m_SoundFlags.PrepareFailed;
	}

	void ClearHasPrepared()
	{
		m_SoundFlags.Prepared = false;
	}

	void SetHasPrepared()
	{
		m_SoundFlags.Prepared = true;
	}

	bool HasPrepared() const
	{
		return m_SoundFlags.Prepared;
	}

	static audRequestedSettings *GetRequestedSettingsFromIndex(const u32 bucket, const u32 index)
	{
		return (audRequestedSettings*)(index!=0xff?audSound::sm_Pool.GetRequestedSettingsSlot(bucket, index):NULL);
	}

	// PURPOSE
	//	returns a random offset within the specified range (+- variance)
	static s32 ComputeOffsetFromVariance(s32 variance)
	{
		return audEngineUtil::GetRandomNumberInRange(0 - variance, variance);
	}

private:

	void Delete()
	{
		audSound::sm_Pool.DeleteSound(this,m_InitParams.BucketId);
	}

#if !AUD_SOUND_USE_VIRTUALS
	// PURPOSE
	// calls the destructor for this sound type
	void InvokeDestructor()
	{

		FastAssert(m_SoundTypeId > 0 && m_SoundTypeId <= AUD_NUM_SOUNDDEFS);
		FastAssert(sm_SoundTypeDestructFunctions[m_SoundTypeId-1]);
		sm_SoundTypeDestructFunctions[m_SoundTypeId-1](this);
	}
#endif

	///////////////////////////////////////////////////
	// These functions are only used internally, by the base sound class
	void ActionHierarchyRemovalRequest();

	void CombineMetadata(f32 volume, s32 pitch, s32 pan, u32 simpleReleaseTime, u32 lpfCutoff, u32 hpfCutoff);

	void CombineRequestedInternalSettings(f32 volume, s32 pan);
	
	
	// PURPOSE
	//	Fetches based metadata values into member variables to avoid hitting metadata every frame.
	// PARAMS
	//	useCache - if true random offsets wont be recalculated
	// NOTES
	//	This is called once on Init(), and every audio frame in __BANK builds (to catch RAVE changes)
	void PullMetadataValues(void *metadata, bool useCache, Sound &uncompressedMetadata);

	// PURPOSE
	//	computes offsets from variances (volume etc). 
	// PARAMS
	//	useCache - in DEV builds this is set to true if the cached offset should be used, rather than recalculating
	//	the (random) offset
	void ComputeOffsetsFromVariances(const Sound *metadata, const bool useCache);

	// PURPOSE
	//  Sets the sound's play state, for lifecycle management. We want to restrict this to base sound management,
	//  hence being Private.
	void SetPlayState(s32 playState)
	{
		SoundAssert(playState >= AUD_SOUND_DORMANT && playState <= AUD_SOUND_WAITING_TO_BE_DELETED);
		m_PlayState = static_cast<u8>(playState);
		SoundAssert(static_cast<s32>(m_PlayState) == playState);
	}

	// PURPOSE
	//	Grabs a requested settings structure, required for any sound that is to be controlled from
	//	game code.
	void AllocateRequestedSettings()
	{
#if __DEV
		audRequestedSettings *reqSettings = (audRequestedSettings*)sm_Pool.AllocateRequestedSettingsSlot(sizeof(audRequestedSettings), m_InitParams.BucketId, GetName());
#else
		audRequestedSettings *reqSettings = (audRequestedSettings*)sm_Pool.AllocateRequestedSettingsSlot(sizeof(audRequestedSettings), m_InitParams.BucketId);
#endif
		if(reqSettings)
		{
			::new(reqSettings) audRequestedSettings(this);
			m_RequestedSettingsIndex = sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, reqSettings);
		}
	}

	// PURPOSE
	//	Deletes requested settings the clean way
	void DeleteRequestedSettings()
	{
		if(CanDeleteRequestedSettings() && m_RequestedSettingsIndex != 0xff)
		{
			GetRequestedSettings()->~audRequestedSettings();
			sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_RequestedSettingsIndex);
			m_RequestedSettingsIndex = 0xff;
		}
	}

	// PURPOSE
	//  Combines this sounds settings with its parent's
	void CombineBuffers(audSoundCombineBuffer &combineBuffer);

	// PURPOSE
	//  Causes the sound to call out to its audEntity or audTracker for updates.
	//  This is called by the sound's audController, and should not be used by game code.
	void Update(const u32 timeInMs, const audRequestedSettings::Indices &reqSetIndices);

	// PURPOSE
	// Returns the metadata speaker mask combined with parent
	u8		ComputeMetadataSpeakerMaskCombinedWithParent();

#if !__SPU
	// PURPOSE
	//  If not referenced, a WAITING_TO_BE_DELETED top level sound will be deleted next audio frame.
	//  A referenced sound must have a requested settings structure, so it can be controlled by game code,
	//  so one is allocated if not already present. If the sound is not at the top of its sound hierarchy,
	//  it also flags this registration up the chain, so that the top level sound can keep track of how many
	//  of its children are referenced by the game.
	void SetIsReferencedByGame(const bool referenced)
	{
		// If we're being unreferenced, see if we've got a req set, and if we do, clear it's occlusion group - this is the only
		// way we now ref count occlusion groups
		if (!referenced)
		{
			if (GetRequestedSettings())
			{
				if (GetRequestedSettings()->GetEnvironmentGroup())
				{
					GetRequestedSettings()->SetEnvironmentGroup(NULL);
				}
			}
		}

		if(referenced)
		{
			if(m_RequestedSettingsIndex == 0xff)
			{
				AllocateRequestedSettings();
			}
			
			SetReferencedDirectlyByGame(true);
		}
		else
		{
			SetReferencedDirectlyByGame(false);
		}
	}

	// PURPOSE
	//  Sets the sound's parent - used during sound instantiation.
	// NOTES
	//	BucketId is passed as a param since its not available as m_InitParams.BucketId until after Init() and SetParent()
	//	needs to be called before that
	void SetParent(u32 bucketId, audSound *parent)
	{
		m_ParentIndex = (parent?sm_Pool.GetSoundSlotIndex(static_cast<u8>(bucketId), parent):0xff);
	}

	// PURPOSE
	//	Returns the metadata pitch combined with parents pitch depending on override
	s32		ComputeMetadataPitchCombinedWithParent();

	// PURPOSE
	//	Returns the volume offset calculated from metadata variance
	AUD_INLINE f32	ComputeMetadataVolumeOffset(u16 volumeVariance, const bool SUPPORT_RAVE_EDITING_ONLY(useCached=false))
	{
#if AUD_SUPPORT_RAVE_EDITING
		if(useCached)
		{
			return GetVarianceCache()->VolumeOffset;
		}
		else
		{
			return (GetVarianceCache()->VolumeOffset = ComputeOffsetFromVariance(volumeVariance) * 0.01f);
		}
#else
		return ComputeOffsetFromVariance(volumeVariance) * 0.01f;
#endif
	}

	// PURPOSE
	//	Returns the pitch offset calculated from metadata variance
	AUD_INLINE s16 ComputeMetadataPitchOffset( u16 PitchVariance, const bool SUPPORT_RAVE_EDITING_ONLY(useCached=false))
	{
#if AUD_SUPPORT_RAVE_EDITING
		if(useCached)
		{
			return GetVarianceCache()->PitchOffset;
		}
		else
		{
			return (GetVarianceCache()->PitchOffset = (s16)ComputeOffsetFromVariance( PitchVariance ));
		}
#else
		return (s16)ComputeOffsetFromVariance( PitchVariance );
#endif
	}

	// PURPOSE
	//	Returns the pan offset calculated from metadata variance
	AUD_INLINE s16 ComputeMetadataPanOffset( u16 PanVariance,  const bool SUPPORT_RAVE_EDITING_ONLY(useCached=false))
	{
#if AUD_SUPPORT_RAVE_EDITING
		if(useCached)
		{
			return GetVarianceCache()->PanOffset;
		}
		else
		{
			return (GetVarianceCache()->PanOffset = (s16)ComputeOffsetFromVariance(PanVariance));
		}
#else
		return (s16)ComputeOffsetFromVariance(PanVariance);
#endif
	}

	// PURPOSE
	//	Returns the start-offset offset calculated from metadata variance
	AUD_INLINE s32 ComputeMetadataStartOffsetOffset(u32 StartOffsetVariance, const bool SUPPORT_RAVE_EDITING_ONLY(useCached))
	{
#if AUD_SUPPORT_RAVE_EDITING
		if(useCached)
		{
			return GetVarianceCache()->StartOffsetOffset;
		}
		else
		{
			return (GetVarianceCache()->StartOffsetOffset = ComputeOffsetFromVariance(StartOffsetVariance));
		}
#else
		return ComputeOffsetFromVariance(StartOffsetVariance);
#endif
	}

	// PURPOSE
	//	Returns the predelay offset calculated from metadata variance
	AUD_INLINE s32 ComputeMetadataPredelayOffset(u16 preDelayVariance, const bool SUPPORT_RAVE_EDITING_ONLY(useCached))
	{
#if AUD_SUPPORT_RAVE_EDITING
		if(useCached)
		{
			return GetVarianceCache()->PredelayOffset;
		}
		else
		{
			return (GetVarianceCache()->PredelayOffset = ComputeOffsetFromVariance(preDelayVariance));
		}
#else
		return ComputeOffsetFromVariance(preDelayVariance);
#endif
	}

	// PURPOSE
	//	Returns the cutoff offset calculated from metadata variance
	AUD_INLINE s32 ComputeMetadataLPFCutoffOffset(const u32 cutoffVariance, const bool SUPPORT_RAVE_EDITING_ONLY(useCached))
	{
#if AUD_SUPPORT_RAVE_EDITING
		if(useCached)
		{
			return GetVarianceCache()->LPFOffset;
		}
		else
		{
			Assign(GetVarianceCache()->LPFOffset, ComputeOffsetFromVariance(cutoffVariance));
			return GetVarianceCache()->LPFOffset;
		}
#else
		return ComputeOffsetFromVariance(cutoffVariance);
#endif
	}

	// PURPOSE
	//	Returns the cutoff offset calculated from metadata variance
	AUD_INLINE s32 ComputeMetadataHPFCutoffOffset(const u32 cutoffVariance, const bool SUPPORT_RAVE_EDITING_ONLY(useCached))
	{
#if AUD_SUPPORT_RAVE_EDITING
		if(useCached)
		{
			return GetVarianceCache()->HPFOffset;
		}
		else
		{
			Assign(GetVarianceCache()->HPFOffset, ComputeOffsetFromVariance(cutoffVariance));
			return GetVarianceCache()->HPFOffset;
		}
#else
		return ComputeOffsetFromVariance(cutoffVariance);
#endif
	}

#endif // !__SPU

	// PURPOSE
	//	Returns the metadata doppler factor combined with parent's doppler factor depending on flags
	f32		ComputeMetadataDopplerFactorCombinedWithParent();

	// PURPOSE
	//  Applies a simple attenuation factor if the sound is being released as has a >=0 release time. 
	//  Returns false if attenuation is full, and ShouldFinishWhenEnvelopeAttenuationFull() is false.
	bool ApplySimpleReleaseEnvelope(const u32 timeInMs, audSoundCombineBuffer &combineBuffer);

	// PURPOSE
	//	Applies a simple attenuation factor if the sound is in its attack phase
	// RETURNS
	//	True if an attack is currently active, false otherwise
	bool ApplySimpleAttackEnvelope(const u32 timeInMs, audSoundCombineBuffer &combineBuffer);

	// PURPOSE
	//	Replaces the specified child sound
	void ReplaceChild(audSound *oldChild, audSound *newChild);
	

private:

	bool IsMuted() const
	{
		return (AUD_GET_TRISTATE_VALUE(m_MetadataFlags, FLAG_ID_SOUND_MUTE) == AUD_TRISTATE_TRUE);
	}

	audVariableHandle m_PredelayVariable;
	audVariableHandle m_StartOffsetVariable;

	audTracker		*m_Tracker;
	audEntity		*m_Entity;
	audSound		**m_EntitySoundReference;

	//	not directly available on SPUs, use with care ...
	// Don't use this directly; sound type implementations should call GetMetadata()
	Sound			*m_BaseMetadata;

	u32				m_PlaybackPredelay;
	u32				m_PlaybackStartOffset;

	u32				m_RequestedPredelay;

	s32				m_MetadataStartOffset;
	s32				m_RequestedStartOffset;	// 108 bytes to here

	u32				m_MetadataFlags;

	// Made this a u32 from a u16, as it's an actual time, rather than a difference.
	u32				m_PrepareTimeLimit;
	// PURPOSE
	// Set when the sound is first Played.
	s32				m_TimeTriggered;

	f32				m_MetadataVolume;

	u32				m_SimpleReleaseStartTime;

protected:

	audSoundInternalInitParams m_InitParams; // 4 bytes
	enum {kNoSimpleRelease = 0xffff};
	u16				m_SimpleReleaseTime;

#if AUD_SUPPORT_RAVE_EDITING && !__SPU
	audSoundVarianceCache *GetVarianceCache() { return sm_Pool.GetVarianceCache(m_InitParams.BucketId, this); }
	const audSoundVarianceCache *GetVarianceCache() const { return sm_Pool.GetVarianceCache(m_InitParams.BucketId, this); }
#endif

	u8				GetTopLevelRequestedSettingsIndex() const { return m_TopLevelRequestedSettingsIndex; }

private:
	u16				m_MetadataPredelay;
	u16				m_MetadataHPFCutoff;
	u16				m_MetadataLPFCutoff;
	u16				m_SimpleAttackTime;

	s16				m_MetadataPan;
	s16				m_MetadataPitch;

	u16				m_CachedVirtualisationGroupID; // Set the first time we're asked for our id	

	u32				m_Padding;
	// 25/01/06 We now store all child sounds generically under the base sound. Metadata is still stored on a sound-type
	// basis, and these audSound ptrs are used to point to the instantiated child sounds. This lets us generically
	// release all children, navigate the sound hierarchy, etc, etc.
	// NOTES
	//	We store this as an array of indices into the sound pool to save memory, use GetChildSound() and SetChildSound()
	//	to access
	atRangeArray<u8, audSound::kMaxChildren> m_ChildSounds;	

	//	Index into sound pool to a requested settings class - only present (i.e. not 0xff) in sounds being controlled
	//	from game code
	u8				m_RequestedSettingsIndex;
	u8				m_TopLevelRequestedSettingsIndex;
private:
	u8				m_ParentIndex;

	u8				m_MetadataSmallReverb;
	u8				m_MetadataMediumReverb;
	u8				m_MetadataLargeReverb;

	Sound::tParentOverrides m_ParentOverrides; // 2 bytes

	struct tSoundFlags
	{
		tSoundFlags() :
		Initialized(false),
		SetToPlay(false),
		SetToBeKilled(false),
		SetToBeDestroyed(false),
		SetToRelease(false),
		BeingReleased(false),
		WasSetToReleaseLastFrame(false),
		InReleasePhase(false),
		Prepared(false),
		PrepareFailed(false),
		SetToBeStopped(false),
		WaitingForChildrenToRelease(false),
		RequestedStartOffsetPercentage(false),
		CanDynamicallyLoad(false),
		BeingRemovedFromHierarchy(false),
		CanBeRemovedFromHiearchy(false),
		IsWaitingToBeRemovedFromHierarchy(false),
		// defaults to true:
		CanDeleteRequestedSettings(true),
		HasDynamicChildren(false),
		// defaults to true, sound types can override behaviour in Init():
		ShouldFinishWhenEnvelopeAttenuationFull(true),
		IsDestructing(false),
		PendingChildInstantation(false),
		TrackEntityPosition(false),
		IsEffectRouteValid(true)
	{
	}

	bool IsInitialising : 1;
	bool Initialized : 1;
	bool SetToPlay : 1;
	bool SetToBeKilled : 1;
	bool SetToBeDestroyed : 1;
	bool SetToRelease : 1;
	bool BeingReleased : 1;
	bool WasSetToReleaseLastFrame : 1;
	bool InReleasePhase : 1;
	bool Prepared : 1;
	bool PrepareFailed : 1;
	bool SetToBeStopped : 1;
	bool WaitingForChildrenToRelease : 1;
	bool RequestedStartOffsetPercentage : 1;
	bool CanDynamicallyLoad : 1;
	bool BeingRemovedFromHierarchy : 1;
	bool CanBeRemovedFromHiearchy : 1;
	bool IsWaitingToBeRemovedFromHierarchy : 1;
	bool CanDeleteRequestedSettings : 1;
	bool HasDynamicChildren : 1;
	bool ShouldFinishWhenEnvelopeAttenuationFull : 1;
	bool IsDestructing : 1;
	bool PendingChildInstantation : 1;
	bool TrackEntityPosition : 1;
	bool IsEffectRouteValid : 1;
	}m_SoundFlags; // 3 bytes

	// Offset in bytes from m_BaseMetadata to the type specific metadata
	u8				m_TypeMetadataOffset : 7;
	u8				m_SoundTypeId : 6;	
	u8				m_PlayState : 2;
	u8				m_PredelayState : 2;
	u8				m_WaveLoadPriority : 2;
	
#ifdef GTA_REPLAY_RAGE
	class CReplaySoundHandle
	{
	public:
		void*	m_pData;
		bool	m_IsPersistent;
	};

	s32		m_replayId;
	bool*	m_pIsReplayIdTaken;
	void*	m_pReplaySoundHandle;
#endif

#if !AUD_SOUND_USE_VIRTUALS
public:

	static SoundUpdateFunction sm_SoundTypeUpdateFunctions[AUD_NUM_SOUNDDEFS];
	static SoundPlayFunction sm_SoundTypePlayFunctions[AUD_NUM_SOUNDDEFS];
	static SoundKillFunction sm_SoundTypeKillFunctions[AUD_NUM_SOUNDDEFS];
	static SoundPrepareFunction sm_SoundTypePrepareFunctions[AUD_NUM_SOUNDDEFS];
	static SoundPauseFunction sm_SoundTypePauseFunctions[AUD_NUM_SOUNDDEFS];
	static SoundComputeDurationMsExcludingStartOffsetAndPredelayFunction sm_SoundTypeComputeDurationMsExcludingStartOffsetAndPredelayFunctions[AUD_NUM_SOUNDDEFS];
	static SoundDestructFunction sm_SoundTypeDestructFunctions[AUD_NUM_SOUNDDEFS];
	static SoundActionReleaseRequestFunction sm_SoundTypeActionReleaseRequestFunctions[AUD_NUM_SOUNDDEFS];
private:
#endif

	static bool sm_ShouldPreserveFullHierarchy;

#if __SOUND_DEBUG_DRAW
	static atRangeArray<audSoundCombineBuffer, 256> sm_CachedCombineBuffers;
#endif

protected:
	
#if AUD_DEBUG_SOUNDS
	
	// PURPOSE
	//	Called by sound factory in DEV builds to enable asserts that catch touching metadata out of
	//	the Init() call stack
	void SetIsInitialising(bool isInitialising)
	{
		m_SoundFlags.IsInitialising = isInitialising;
	}

#endif // AUD_DEBUG_SOUNDS

#endif // AUD_VOICE_SPU
};
//#pragma warning(error : 4265) // class has virtual functions, but destructor is not virtual

class audSoundSyncSetMasterId
{
public:
	audSoundSyncSetMasterId()
	{
		m_Previous = audSound::GetCurrentSyncMasterId();
	}

	~audSoundSyncSetMasterId()
	{
		audSound::SetCurrentSyncMasterId(m_Previous);
	}

	void Set(const u32 val)
	{
		m_Previous = audSound::SetCurrentSyncMasterId(val);
	}

private:
	u32 m_Previous;
};

class audSoundSyncSetSlaveId
{
public:
	audSoundSyncSetSlaveId()
	{
		m_Previous = audSound::GetCurrentSyncSlaveId();
	}

	~audSoundSyncSetSlaveId()
	{
		audSound::SetCurrentSyncSlaveId(m_Previous);
	}

	void Set(const u32 val)
	{
		m_Previous = audSound::SetCurrentSyncSlaveId(val);
	}

private:
	u32 m_Previous;
};

} // namespace rage

#endif // AUD_SOUND_H
