//
// audioengine/multitracksound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_MULTITRACKSOUND_H
#define AUD_MULTITRACKSOUND_H

#include "sound.h"

#include "soundDefs.h"

namespace rage {
	

// PURPOSE
//  A multi-track sound simultaneously plays all of its children.
class audMultitrackSound : public audSound
{
public:

	AUD_DECLARE_STATIC_WRAPPERS;

	~audMultitrackSound();

#if !__SPU
	audMultitrackSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Returns the number of child sounds present.
	u32 GetNumSoundReferences();

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:
	u32 m_NumSoundRefs;
	u32 m_SyncId;
	bool m_HaveReleasedChildren;
	bool m_ShouldStopWhenChildStops;
};

} // namespace rage

#endif // AUD_MULTITRACKSOUND_H
