//
// audioengine/speechsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SPEECH_SOUND_H
#define AUD_SPEECH_SOUND_H

#include "sound.h"
#include "audiosoundtypes/speechdefs.h"
#include "audiohardware/waveref.h"
#include "audioengine/metadatamanager.h"

namespace rage {
	
	class audWavePlayer;
	class audSpeechSound;
	struct audSpeechSoundInfo
	{
		audSpeechSoundInfo() : sound(NULL), slot(NULL)
		{

		}
		audSpeechSound *sound;
		audWaveSlot *slot;
	};

	struct audSpeechSoundParams
	{
		u32 WaveNameHash;
		u32 BankId;
		u32 PredelayMs;
	};

// PURPOSE
//  A speech sound has no child sounds, and plays a single wave that is derived from a specified voice, speech context and
//	variation number.
class audSpeechSound : public audSound
{
public:
	
	~audSpeechSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audSpeechSound();

	//
	// PURPOSE
	//  Loads and reconstructs the speech lookup table.
	//
	static bool InitClass(void);
	//
	// PURPOSE
	//  Frees the speech lookup table.
	//
	static void ShutdownClass(void);

	static bool LoadSpeechMetadata(const char* metadataName, const char* path);
	static bool UnloadSpeechMetadata(const char* metadataName);
	
	//
	// PURPOSE
	//	Searches the speech lookup table to determine the number of speech variations available for a specified voice name
	//	and context name. Will return 0 if no variations are available, or if the voice or context names could not be
	//	found.
	// PARAMS
	//	voiceName	- The name of the voice to be searched for in the speech lookup table, e.g. "COP1".
	//	contextName	- The name of the speech context to be searched for in the speech lookup table, e.g. "CHAT".
	// RETURNS
	//	The number of variations available for a specified voice name and context name. Will return 0 if no variations are
	//	available, or if the voice or context names could not be found in the lookup table.
	//
	static u32 FindNumVariationsForVoiceAndContext(const u32 voiceNameHash, const u32 contextNamePartialHash);
	static u32 FindNumVariationsForVoiceAndContext(const u32 voiceNameHash, const char *contextName);
	static u32 FindNumVariationsForVoiceAndContext(const char *voiceName, const char *contextName);
	//
	// PURPOSE
	//	Searches the speech lookup table and extracts a number of parameters related to the speech variations available for a
	//	specified voice name and context name.
	// PARAMS
	//	voiceName		- The name of the voice to be searched for in the speech lookup table, e.g. "COP1".
	//	contextName		- The name of the speech context to be searched for in the speech lookup table, e.g. "CHAT".
	//	numVariations	- Used to return the number of speech variations available.
	//	variationData	- Used to return an array of variation-specific metadata. If this parameter is NULL,
	//						no variation-specific metadata will be extracted.
	//	variationUids	- Used to return an array of unique identifiers for the available speech variations. If this parameter
	//						is NULL, no unique identifiers will be computed.
	// RETURNS
	//	True if speech variations were located for the specified voice name and context name, or false if no variations
	//	could be found.
	//
	static bool FindVariationInfoForVoiceAndContext(const u32 voiceNameHash, const u32 contextNamePartialHash, u32 &numVariations,
		u8 *variationData = NULL, u32 *variationUids = NULL);
	static bool FindVariationInfoForVoiceAndContext(const char *voiceName, const char *contextName, u32 &numVariations,
		u8 *variationData = NULL, u32 *variationUids = NULL);

	// PURPOSE
	//  Find the bank Id of a voice and context pairing - needed if you're going to load banks of speech manually
	static u32 FindBankIdForVoiceAndContext(const u32 voiceNameHash, const char *contextName);
	static u32 FindBankIdForVoiceAndContext(const char *voiceName, const char *contextName);

	//
	// PURPOSE
	//  Derives the bank name and wave name hash to be used during the prepare phase. The specified voice and context
	//	names are used to extract data from the speech lookup table, which is then combined with the specified
	//	variation number.
	// PARAMS
	//	voiceName	- The name of the voice to be used, e.g. "COP1".
	//	contextName	- The name of the speech context to be used, e.g. "CHAT".
	// RETURNS
	//	True if the speech sound was successfully initialized in readiness for preparation, or false if an error occurred.
	// NOTES
	//	This function must be called in advance of preparing the sound.
	//
	bool InitSpeech(const char *voiceName, const char *contextName, u32 variationNum);
	bool InitSpeech(const u32 voiceNameHash, const char *contextName, u32 variationNum);

	//
	// PURPOSE
	//  Derives the bank name and wave name hash to be used during the prepare phase. The specified voice and context
	//	name hashes are used to extract data from the speech lookup table, which is then combined with the specified
	//	variation number.
	// PARAMS
	//	voiceNameHash	- The hashed name of the voice to be used, e.g. "COP1".
	//	contextNameHash	- The partial hashed name of the speech context to be used, e.g. "CHAT" (use atPartialStringHash).
	//	variationNum	- The variation index (1-n)
	//	voiceName		- Optional, used to provide human-friendly error/warning messages
	//	contextName		- Optional, used to provide human-friendly error warning messages
	// RETURNS
	//	True if the speech sound was successfully initialized in readiness for preparation, or false if an error occurred.
	// NOTES
	//	This function must be called in advance of preparing the sound.
	//
	bool InitSpeech(const u32 voiceNameHash, const u32 contextNamePartialHash, u32 variationNum, const char *voiceName = NULL, const char *contextName = NULL);

	//
	// PURPOSE
	//  Initialises this sound with the specified bank/wave, bypassing the speech lookup tables
	// RETURNS
	//	True if the speech sound was successfully initialized in readiness for preparation, or false if an error occurred.
	// NOTES
	//	This function must be called in advance of preparing the sound.
	//
	bool InitSpeech(const audSpeechSoundParams &params);
	
	// PURPOSE
	//  Returns true if the voice exists
	static bool DoesVoiceExist(u32 voiceNameHash);

	// PURPOSE
	//	Prepares a batch of speech sounds in one go, locking the disk head between reads to cut down on seeking
	// RETURNS
	//	Prepared once they are all prepared, preparing if some sounds are still loading, failed if any load fails
	// NOTES
	//	InitSpeech() must have been called before calling this function
	static audPrepareState BatchPrepare(audSpeechSoundInfo *sounds, const u32 numSounds);

	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::Init()
	bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);

	//
	// PURPOSE
	//  Searches for a voice in the speech lookup table based upon the specified voice name hash and returns that voices paintype.
	// PARAMS
	//	voiceNameHash - The hashed name of the voice to be extracted.
	// RETURNS
	//	The paintype hash, or 0 if it fails to find a voice.
	//
	static u8 FindPainType(u32 voiceNameHash);

	static audMetadataManager &GetMetadataManager() { return sm_MetadataMgr; }

	// PURPOSE
	//	Set the prefix to use when searching for variation exclusion objects, or NULL to disable (allow all variations)
	static void SetExclusionPrefix(const char *exclPrefix);

#endif // !__SPU
	
	

	//
	// PURPOSE
	//  Returns the wave name hash determined by the voice, context and variation number specified when initializing this
	//	speech sound, or NULL if the sound has not yet been initialized or an error occurred.
	// RETURNS
	//	The wave name hash determined by the voice, context and variation number specified when initializing this speech
	//	sound, or NULL.
	//
	u32 GetWaveNameHash() const;


	s32 GetWavePlayerId() const
	{
		return m_WavePlayerSlotId;
	}

	// PURPOSE
	//	Returns the bank id containing the requested context/variation
	u32 GetBankId() const;
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::AudioPlay()
	//
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::AudioUpdate()
	//
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::AudioKill()
	//
	void AudioKill();
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::ComputeDurationMsExcludingStartOffsetAndPredelay()
	//
	int ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	audEnvironmentSound *GetEnvironmentSound() const
	{
		return (audEnvironmentSound*)GetChildSound(0);
	}

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;


	s32 GetWavePlaytime() const { return m_WavePlaytime; }


	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::Prepare()
	//
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

	f32 GetWaveHeadroom() const;


	//
	// PURPOSE
	//  Workaround to allow us to assign a predelay once we know the viseme predelay times to a speech sound which has already been created 
	// PARAMS
	//	predelayMs - Additional predelay in milliseconds
	//
	void SetAdditionalPredelay(const u32 predelayMs);

	//
	// PURPOSE
	//  Accessor for the predelay held in the speech params
	u32 GetAdditionalPredelay();


private:

#if !__SPU
	// PURPOSE
	//  Looks up bank id from bank lookup index stored in VoiceContextInfo->VariationGroupInfo[].bankName
	// PARAMS
	//	index	- index from voiceContextInfo->VariationGroupInfo[currentVariationGroupInfo].bankName
	//  chunkIndex - chunk to search in, required because we may have duplicates
	// RETURNS
	//	Wave bank id, or AUD_INVALID_BANK_ID if the bank could not be found.
	//
	static u32 FindBankIdFromBankNameTableIndexAndChunkIndex(u32 index, u32 chunkIndex);

#endif

private:

	bool SetupWavePlayer();
	void ChildSoundCallback(const u32 timeInMs, const u32 childIndex);


	audWaveRef m_WaveReference;
	s32 m_WavePlaytime;
	s32 m_WaveLengthMs;
	s32 m_SlotIndex;
	u16 m_WaveHeadroom;
	s16	m_WavePlayerSlotId;
	u8 m_SpeechParamsIndex;

#if !__SPU
	static audMetadataManager sm_MetadataMgr;
	static u32 sm_ExclusionPrefixPartialHash;
#endif
};

} // namespace rage

#endif // AUD_SPEECH_SOUND_H
