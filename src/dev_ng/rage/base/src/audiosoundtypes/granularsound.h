// 
// audiosoundtypes/granularsound.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_GRANULARSOUND_SOUND_H
#define AUD_GRANULARSOUND_SOUND_H

#include "sound.h"
#include "environmentsound.h"
#include "audiohardware/grainplayer.h"

namespace rage 
{
	class audGrainPlayer;
	class audGranularSound : public audSound
	{
	public:
		AUD_DECLARE_STATIC_WRAPPERS;
		
		~audGranularSound();

#if !__SPU
		audGranularSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 UNUSED_PARAM(timeInMs), audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping);

		audEnvironmentSound *GetEnvironmentSound() const
		{
			return (audEnvironmentSound*)GetChildSound(0);
		}

		u16 GetBankId() const
		{
			return m_BankNameIndex;
		}

		s16 GetGrainPlayerID() const { return m_GrainPlayerId; }
		void SetInitialParameters(f32 xValue);

		void SetX(const f32 x, const u32 elapsedTime);
		void ForceX(const f32 x);
		void AllocateVariableBlock();
		void SetXSmoothRate(const f32 smoothRate) const;
		void SetGrainPlaybackStyle(u32 channel, audGranularMix::audGrainPlaybackStyle playbackStyle) const;
		void SetGranularRandomisationStyle(audGranularSubmix::audGrainPlaybackOrder randomisationStyle) const;
		void SetGrainPlayerQuality(audGranularMix::audGrainPlayerQuality quality);
		void SetLoopEnabled(u32 channel, u32 loopIndex, bool enabled) const;
		void SnapToNearestPlaybackStyle(u32 channel) const;
		void SetWobbleEnabled(u32 length, f32 speed, f32 pitch, f32 volume, u32 clockIndex = 0) const;
		void SetGranularSlidingWindowSize(f32 windowSizeHz, u32 minGrains, u32 maxGrains, u32 minRepeatRate) const;
		void MuteChannel(u32 channel, bool muted);
		void LockPitch(bool lock) const;
		void SetGranularChangeRateForLoops(f32 changeRate) const;
		void SetChannelVolume(u32 channel, f32 volume, f32 smoothRate);
		void SetAsPureRandom(u32 channel, bool setAsRandom) const;
		void SetAsPureRandomWithStyle(u32 channel, audGranularSubmix::audGrainPlaybackOrder randomisationStyle) const;
		void SetMixNative(u32 channel, bool mixNative) const;
		void SetGrainSkippingEnabled(u32 channel, bool enabled, u32 numToPlay = 0, u32 numToSkip = 0, f32 skippedGrainVol = 0.0f) const;

#if __BANK
		void SetDebugRenderingEnabled(bool enabled) const;
		void ShiftLoopRight() const;
		void ShiftLoopLeft() const;
		void GrowLoop() const;
		void ShrinkLoop() const;
		void DeleteLoop() const;
		void CreateLoop() const;
		void ExportData() const;
		void SetTestToneEnabled(bool enabled) const;
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

		// PURPOSE
		//	Handles setting up child sound - in this case actually setting wave metadata stuff on the environment sound
		void ChildSoundCallback(const u32 timeInMs, const u32 index);

	private:
		struct audGrainChannelInitData
		{
			f32 volumeScale;
			f32 initialVolume;
			f32 maxLoopProportion;
			u32 waveNameHash;
			u8 outputChannel:3;
			u8 granularClock:2;
			bool matchMinPitch:1;
			bool matchMaxPitch:1;
			bool allowLoopGrainOverlap:1;
		};

	private:
		void InitGrainPlayer();
		audGrainChannelInitData* GetChannelInitData(const u32 index);

	private:
		enum {kMaxChannelInitDataSlots = 2};
		atRangeArray<u8,kMaxChannelInitDataSlots> m_ChannelInitDataSlotIds;
		atFixedArray<GranularSound::tGranularClock, GranularSound::MAX_GRANULARCLOCKS> m_GranularClocks;
		atFixedBitSet<audGrainPlayer::kMaxGranularMixersPerPlayer> m_ChannelMuted;
		atFixedBitSet<audGrainPlayer::kMaxGranularMixersPerPlayer> m_ChannelValid;
		audGranularMix::audGrainPlayerQuality m_Quality;

		u32		m_PrimaryWaveNameHash;
		f32		m_LoopRandomisationChangeRate;
		f32		m_LoopRandomisationMaxPitchFraction;
		f32		m_InitialX;
		f32		m_PrevXValue;
		s16		m_GrainPlayerId;
		s16		m_StaticSlotIndex;
		s16		m_SlotIndex;
		u16		m_BankNameIndex;
		s8		m_NumChannels;
		bool	m_Initialised;
		bool	m_GrainPlayerInitialised;
		bool	m_LoopRandomisationEnabled;
	};

} // namespace rage

#endif //AUD_GRANULARSOUND_SOUND_H
