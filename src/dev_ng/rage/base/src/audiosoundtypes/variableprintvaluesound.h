// 
// audiosoundtypes/variableprintvaluesound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_VARIABLEPRINTVALUESOUND_H
#define AUD_VARIABLEPRINTVALUESOUND_H

#include "audiosoundtypes/sound.h"

#include "audiosoundtypes/sounddefs.h"

namespace rage {

// PURPOSE
//  audDisplayf()'s the current value of a variable specified in metadata
class audVariablePrintValueSound : public audSound
{
public:

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:	

	audVariableHandle m_Variable;
	const char *m_Message;
	u32 m_MessageLen;
	BANK_ONLY(bool m_ShouldPrint);
};

} // namespace rage

#endif // AUD_VARIABLEPRINTVALUESOUND_H
