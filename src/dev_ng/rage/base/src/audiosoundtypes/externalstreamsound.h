// 
// audiosoundtypes/externalstreamsound.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_EXTERNALSTREAMSOUND_SOUND_H
#define AUD_EXTERNALSTREAMSOUND_SOUND_H

#include "sound.h"
#include "environmentsound.h"
#include "audiohardware/streamplayer.h"

namespace rage 
{
	class audExternalStreamSound : public audSound
	{
	public:
		AUD_DECLARE_STATIC_WRAPPERS;

		~audExternalStreamSound();

#if !__SPU
		audExternalStreamSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 UNUSED_PARAM(timeInMs), audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping);

		audEnvironmentSound *GetEnvironmentSound(const s32 index) const
		{
			return (audEnvironmentSound*)GetChildSound(index);
		}
	
		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

		bool InitStreamPlayer(audReferencedRingBuffer *ringBuffer, const u32 numChannels, const u32 sampleRateHz);

		// PURPOSE
		//	Apply a gain factor (decibels) to the incoming stream
		void SetGain(const float gainDb) { m_Gain = gainDb; }

		u32 GetCurrentPlayTimeOfWave() const { return m_CurrentPlayTimeMs; }
		
		// PURPOSE
		//	Low-latency route to stop playback, allowing the referenced ringbuffer to be reused sooner than the normal StopAndForget mechanism.
		void StopStreamPlayback();

		s32 GetStreamPlayerId() const { return m_StreamPlayerId; }
	private:
		
		struct Params
		{
			audReferencedRingBuffer *ringBuffer;
			u32 numChannels;
			u32 sampleRate;
		};

		Params *GetParams();

		float m_Gain;
		u32 m_NumEnvironmentSounds;
		s32 m_StreamPlayerId;
		u32 m_ReqSetSlotId;
		u32 m_CurrentPlayTimeMs;
		bool m_NeedToStopStreamPlayer;
		
	};

} // namespace rage

#endif //AUD_GRANULARSOUND_SOUND_H
