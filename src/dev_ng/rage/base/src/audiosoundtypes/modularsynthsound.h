//
// audioengine/modularsynthsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_MODULARSYNTH_SOUND_H
#define AUD_MODULARSYNTH_SOUND_H

#include "sound.h"
#include "environmentsound.h"

namespace rage {
	
	struct audSynthVariable
	{
		u32 keyHash;
		union
		{
			audVariableHandle valPtr;
			f32 val;
		};
		bool isPtr;
		
	};

	class synthSynthesizer;
	class audModularSynthSound : public audSound
	{
	public:

		AUD_DECLARE_STATIC_WRAPPERS;

		
		~audModularSynthSound();

#if !__SPU
		audModularSynthSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 UNUSED_PARAM(timeInMs), audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();

		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping);

		audEnvironmentSound *GetEnvironmentSound(const u32 index) const
		{
			FastAssert(index < m_NumEnvironmentSounds);
			return (audEnvironmentSound*)GetChildSound(index);
		}

		static void sActionReleaseRequest(audModularSynthSound *_this, u32 timeInMs)
		{
			_this->ActionReleaseRequest(timeInMs);
		}

		s32 GetPcmSourceId() const { return m_SynthSlotId; }

#if __SOUND_DEBUG_DRAW
		AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

		// PURPOSE
		//  Set the release envelope in motion. This overrides the base sound version, so it also clears various settings
		//  so that the default behaviour isn't applied as well.
		void ActionReleaseRequest(const u32 timeInMs);

#ifdef AUD_IMPL
		void Pause(const u32 timeInMs)
		{
			BasePause(timeInMs);
		}
#endif
	
		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

	private:
		bool HasPresetReference() const { return m_PresetReference != 0; }
		void UpdateVariables();

		u32 m_PlayTimeLimitMs;
		u32 m_SynthReference;
		u32 m_PresetReference;
		u32 m_NumEnvironmentSounds;
		u32 m_AssetCost;
		s16 m_SynthSlotId;
		u8 m_VariableSlotId;
		u8 m_NumVariables;
		bool m_ReleaseThroughSynth;
	};

} // namespace rage

#endif // AUD_MODULARSYNTH_SOUND_H
