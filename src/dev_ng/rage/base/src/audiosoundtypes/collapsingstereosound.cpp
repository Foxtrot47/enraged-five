//
// audioengine/collapsingstereosound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "collapsingstereosound.h"
#include "audioengine/engine.h"
#include "audioengine/environment.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/driverdefs.h"


namespace rage 
{

extern audEnvironment *g_audEnvironment;

enum tCollapsingStereoSound
{
	AUD_LEFT_FRONTEND,
	AUD_RIGHT_FRONTEND,
	AUD_LEFT_POSITIONED,
	AUD_RIGHT_POSITIONED
};

AUD_IMPLEMENT_STATIC_WRAPPERS(audCollapsingStereoSound);

audCollapsingStereoSound::~audCollapsingStereoSound()
{
	for(u32 i = 0; i < audSound::kMaxChildren; i++)
	{
		if(GetChildSound(i))
		{
			DeleteChild(i);
		}	
	}
}

#if !__SPU

audCollapsingStereoSound::audCollapsingStereoSound()
{
	for(u32 i = 0 ; i < audSound::kMaxChildren; i++)
	{
		SetChildSound(i,NULL);
	}

	m_CrossfadeOverrideVariable = NULL;
	m_MinDistanceVariable = NULL;
	m_MaxDistanceVariable = NULL;
	m_PositionRelativePanDampingVariable = NULL;
	
	m_Mode = CROSSFADE_MODE_BOTH; // although this will be defaulted in data

	m_FrontendChildVolume = m_PositionedChildVolume = 0.f;
	m_PanL = m_PanR = -1;	
}

bool audCollapsingStereoSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	CollapsingStereoSound *collapsingStereoSoundData = (CollapsingStereoSound*)GetMetadata();

	m_MinDistance = collapsingStereoSoundData->MinDistance;
	m_MaxDistance = collapsingStereoSoundData->MaxDistance;
	m_PositionRelativePanDamping = collapsingStereoSoundData->PositionRelativePanDamping;
	m_Mode = collapsingStereoSoundData->Mode;

	if (collapsingStereoSoundData->MinDistanceVariable)
	{
		m_MinDistanceVariable = _FindVariableUpHierarchy(collapsingStereoSoundData->MinDistanceVariable);
	}
	if (collapsingStereoSoundData->MaxDistanceVariable) 
	{
		m_MaxDistanceVariable = _FindVariableUpHierarchy(collapsingStereoSoundData->MaxDistanceVariable);
	}
	if (collapsingStereoSoundData->CrossfadeOverrideVariable)
	{
		m_CrossfadeOverrideVariable = _FindVariableUpHierarchy(collapsingStereoSoundData->CrossfadeOverrideVariable);
	}
	if(collapsingStereoSoundData->FrontendLeftPan)
	{
		m_FrontendPanLVariable = _FindVariableUpHierarchy(collapsingStereoSoundData->FrontendLeftPan);
	}
	if(collapsingStereoSoundData->FrontendRightPan)
	{
		m_FrontendPanRVariable = _FindVariableUpHierarchy(collapsingStereoSoundData->FrontendRightPan);
	}
	if(collapsingStereoSoundData->PositionRelativePanDampingVariable)
	{
		m_PositionRelativePanDampingVariable = _FindVariableUpHierarchy(collapsingStereoSoundData->PositionRelativePanDampingVariable);
	}

	audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;	

	if (m_Mode != CROSSFADE_MODE_POSITION_RELATIVE_PAN)
	{
		bool stereoOnly = false;
		// Spot simple stereo setup and do the bare minimum
		if(m_MinDistance == -1.f && m_MaxDistance == -1.f && !m_MinDistanceVariable && !m_MaxDistanceVariable)
		{
			m_Mode = CROSSFADE_MODE_FIXED;
			stereoOnly = true;
		}
		else if (m_Mode == CROSSFADE_MODE_FIXED)
		{
			// In Fixed mode we delete children immediately in AudioPlay, so it's not safe to query the hierarchy below this point
			// from other threads.
			SetHasDynamicChildren();
		}

		// traditional collapsing stereo sound - stereo pair a positioned pair
		SetChildSound(AUD_LEFT_FRONTEND, SOUNDFACTORY.GetChildInstance(collapsingStereoSoundData->LeftSoundRef, this, initParams, scratchInitParams, false));
		*scratchInitParams = scratchInitParamsCopy;
		SetChildSound(AUD_RIGHT_FRONTEND, SOUNDFACTORY.GetChildInstance(collapsingStereoSoundData->RightSoundRef, this, initParams, scratchInitParams, false));
		
		if(!stereoOnly)
		{
			*scratchInitParams = scratchInitParamsCopy;
			SetChildSound(AUD_LEFT_POSITIONED, SOUNDFACTORY.GetChildInstance(collapsingStereoSoundData->LeftSoundRef, this, initParams, scratchInitParams, false));
			*scratchInitParams = scratchInitParamsCopy;
			SetChildSound(AUD_RIGHT_POSITIONED, SOUNDFACTORY.GetChildInstance(collapsingStereoSoundData->RightSoundRef, this, initParams, scratchInitParams, false));
		}

		for (s32 i = AUD_LEFT_FRONTEND; i <= (stereoOnly ? AUD_RIGHT_FRONTEND : AUD_RIGHT_POSITIONED); i++)
		{
			if(!HasChildSound(i))
			{
				// if any of our required children don't exist, we haven't initialised properly.
				return false;
			}
		}

		if(m_FrontendPanRVariable && m_FrontendPanLVariable)
		{
			// Set up the front end ones to be panned
			Assign(m_PanL, (s32)AUD_GET_VARIABLE(m_FrontendPanLVariable));
			Assign(m_PanR, (s32)AUD_GET_VARIABLE(m_FrontendPanRVariable));
		}
		else
		{
			// Set up the front end ones to be panned
			m_PanL = 270;
			m_PanR = 90;
		}
	}
	else
	{
		// new style - with a single sound, and controlling its SpreadPan variable
		SetChildSound(AUD_LEFT_POSITIONED, SOUNDFACTORY.GetChildInstance(collapsingStereoSoundData->LeftSoundRef, this, initParams, scratchInitParams, false));
		if(!GetChildSound(AUD_LEFT_POSITIONED))
		{
			// if any of our children don't exist, we haven't initialised properly.
			return false;
		}
		// set that we're spread-panning, and initialise to a full pan
		audRequestedSettings *requestedSettingsForPosition = (audRequestedSettings*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, GetTopLevelRequestedSettingsIndex());
		if (requestedSettingsForPosition)
		{
			requestedSettingsForPosition->SetPositionRelativePan(true);
			requestedSettingsForPosition->SetPositionRelativePanDamping(1.0f);
		}
	}
	return true;
}
#endif // !__SPU

audPrepareState audCollapsingStereoSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	audPrepareState state;
	bool finishedPreparing = true;

	for (s32 i = AUD_LEFT_FRONTEND; i <= AUD_RIGHT_POSITIONED; i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->SetStartOffset((s32)GetPlaybackStartOffset());
			state = childSound->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
			if(state == AUD_PREPARE_FAILED)
			{
				// bail out if any of our children have failed to prepare
				return AUD_PREPARE_FAILED;
			}
			else if(state == AUD_PREPARING)
			{
				finishedPreparing = false;
			}
		}
	}

	if(finishedPreparing)
	{
		return AUD_PREPARED;
	}
	else
	{
		return AUD_PREPARING;
	}
}

void audCollapsingStereoSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	// Work out how far through we want to play, based on precalculated PlaybackStartOffset
	u32 startOffset = GetPlaybackStartOffset(); // this is always in ms, not percentage

	// SetAttenuationFactors() returns instantly if positions are <0, so set up positioned sounds as silence
	// and stereo sounds at 0dB, as this is what we'll want if positions are -1.
	audSound *sndLtFrontend = GetChildSound(AUD_LEFT_FRONTEND);
	audSound *sndRtFrontend = GetChildSound(AUD_RIGHT_FRONTEND);
	audSound *sndLtPositionend = GetChildSound(AUD_LEFT_POSITIONED);
	audSound *sndRtPositionend = GetChildSound(AUD_RIGHT_POSITIONED);
	
	if (m_Mode != CROSSFADE_MODE_POSITION_RELATIVE_PAN)
	{
		m_FrontendChildVolume = 0.f;
		m_PositionedChildVolume = g_SilenceVolume;

		if(m_FrontendPanRVariable && m_FrontendPanLVariable)
		{
			SoundAssert(sndLtFrontend);
			SoundAssert(sndRtFrontend);
			// Set up the front end ones to be panned
			Assign(m_PanL, (s32)AUD_GET_VARIABLE(m_FrontendPanLVariable));
			Assign(m_PanR, (s32)AUD_GET_VARIABLE(m_FrontendPanRVariable));
		}
	}
	f32 ratio = SetAttenuationFactors(true);

	// if we're in FIXED mode, see if we're only playing one of the two, and bin the other. 
	if (m_Mode==CROSSFADE_MODE_FIXED)
	{
		if (ratio == 1.0f)
		{
			// we're just playing positioned - bin the frontend ones
			if (sndLtFrontend)
			{
				sndLtFrontend->_ManagedAudioKill(); // Safe to do this, I think, as we're about to set its volume to silence anyway
				DeleteChild(AUD_LEFT_FRONTEND);
			}
			if (sndRtFrontend)
			{
				sndRtFrontend->_ManagedAudioKill(); // Safe to do this, I think, as we're about to set its volume to silence anyway
				DeleteChild(AUD_RIGHT_FRONTEND);
			}
		}
		else if (ratio == 0.0f)
		{
			// we're just playing frontend - bin the positioned ones
			if (sndLtPositionend)
			{
				sndLtPositionend->_ManagedAudioKill(); // Safe to do this, I think, as we're about to set its volume to silence anyway
				DeleteChild(AUD_LEFT_POSITIONED);
			}
			if (sndRtPositionend)
			{
				sndRtPositionend->_ManagedAudioKill(); // Safe to do this, I think, as we're about to set its volume to silence anyway
				DeleteChild(AUD_RIGHT_POSITIONED);
			}
		}
	}
	
	const float baseVolume = combineBuffer.Volume;
	const s16 basePan = combineBuffer.Pan;
	
	for (s32 i = 0; i <= AUD_RIGHT_POSITIONED; i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			if(i == AUD_LEFT_FRONTEND)
			{
				Assign(combineBuffer.Pan, CombinePan(m_PanL, basePan));	
			}
			else if(i == AUD_RIGHT_FRONTEND)
			{
				Assign(combineBuffer.Pan, CombinePan(m_PanR, basePan));
			}
			else
			{
				combineBuffer.Pan = basePan;
			}

			if(i >= AUD_LEFT_FRONTEND && i <= AUD_RIGHT_FRONTEND)
			{
				combineBuffer.Volume = baseVolume + m_FrontendChildVolume;
			}
			else
			{
				combineBuffer.Volume = baseVolume + m_PositionedChildVolume;
			}

			childSound->SetStartOffset((s32)startOffset, false);
			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}
}

bool audCollapsingStereoSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	bool stillPlaying = false;

	SetAttenuationFactors(false);

	if(m_FrontendPanRVariable && m_FrontendPanLVariable && m_Mode != CROSSFADE_MODE_POSITION_RELATIVE_PAN)
	{
		SoundAssert(GetChildSound(AUD_LEFT_FRONTEND));
		SoundAssert(GetChildSound(AUD_RIGHT_FRONTEND));
		// Set up the front end ones to be panned
		Assign(m_PanL, (s32)AUD_GET_VARIABLE(m_FrontendPanLVariable));
		Assign(m_PanR, (s32)AUD_GET_VARIABLE(m_FrontendPanRVariable));
	}

	const float baseVolume = combineBuffer.Volume;
	const s16 basePan = combineBuffer.Pan;
	for (int i=AUD_LEFT_FRONTEND; i<(AUD_RIGHT_POSITIONED+1); i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			if (childSound->GetPlayState() == AUD_SOUND_PLAYING)
			{
				if(i == AUD_LEFT_FRONTEND)
				{
					Assign(combineBuffer.Pan, CombinePan(m_PanL, basePan));
				}
				else if(i == AUD_RIGHT_FRONTEND)
				{
					Assign(combineBuffer.Pan, CombinePan(m_PanR, basePan));
				}
				else
				{
					combineBuffer.Pan = basePan;
				}

				if(i >= AUD_LEFT_FRONTEND && i <= AUD_RIGHT_FRONTEND)
				{
					combineBuffer.Volume = baseVolume + m_FrontendChildVolume;
				}
				else
				{
					combineBuffer.Volume = baseVolume + m_PositionedChildVolume;
				}

				if (childSound->_ManagedAudioUpdate(timeInMs, combineBuffer))
				{
					stillPlaying = true;
				}
			}
		}
	}	

	if (!stillPlaying)
	{
		// All our subsounds have finished, so we have too
		return false;
	}

	return true;
}

void audCollapsingStereoSound::AudioKill()
{
	for (int i=AUD_LEFT_FRONTEND; i<(AUD_RIGHT_POSITIONED+1); i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->_ManagedAudioKill();
		}
	}
}

s32 audCollapsingStereoSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	s32 longestChildDuration = 0, childDuration;

	for (int i=AUD_LEFT_FRONTEND; i<(AUD_RIGHT_POSITIONED+1); i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			bool isChildLooping = false;
			childDuration = childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
			if (isChildLooping && isLooping)
			{
				*isLooping = true;
			}
			if(childDuration > longestChildDuration)
			{
				longestChildDuration = childDuration;
			}
		}
	}

	return longestChildDuration;
}

f32 audCollapsingStereoSound::SetAttenuationFactors(bool firstCall)
{
	f32 ratio = 0.0f;

	// Don't do anything if our distances are -ve, this sound could just be being used as a stereo player.
	// Also make sure we don't have any variables that might override that.
	if ((m_MinDistance < 0.0f) &&
		(!m_MaxDistanceVariable) && (!m_CrossfadeOverrideVariable) && (!m_PositionRelativePanDampingVariable))
	{
		return 0.0f;
	}
	if (m_Mode == CROSSFADE_MODE_FIXED && !firstCall)
	{
		return 0.5f; // non-commital, and ignored
	}

	// If we've got an override variable, ignore all the distance stuff and just use that
	if (m_CrossfadeOverrideVariable)
	{
		ratio = ClampRange(AUD_GET_VARIABLE(m_CrossfadeOverrideVariable), 0.0f, 1.0f);
	}
	else
	{
		// Check our distance, and hence our frontend-positioned attenuation.
		// Note that this is using the combine buffer for this sound - hence it'll include all relevant positional info
		// above this sound, but nothing below. It's free to use this combine buffer, but the actual distance calculation
		// isn't the cheapest thing, and is also being done in the environment sound, so this is slightly wasteful.
//		Vector3 position = m_CombineBuffer.Position;
		Vec3V position = ((audRequestedSettings*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, GetTopLevelRequestedSettingsIndex()))->GetPosition_AudioThread();
		//Convert world-relative position to listener-relative.
		f32 distanceRelativeToListener =  g_audEnvironment->ComputeDistanceRelativeToVolumeListener(position);

		f32 minDist = -1.0f;
		if (m_MinDistanceVariable)
		{
			minDist = AUD_GET_VARIABLE(m_MinDistanceVariable);
		}
		else
		{
			minDist = m_MinDistance;
		}
		f32 maxDist = -1.0f;
		if (m_MaxDistanceVariable)
		{
			maxDist = AUD_GET_VARIABLE(m_MaxDistanceVariable);
		}
		else
		{
			maxDist = m_MaxDistance;
		}
		ratio = ClampRange(distanceRelativeToListener, minDist, maxDist);
	//		(distanceRelativeToListener - minDist) / (maxDist - minDist);
	//	ratio = Clamp(ratio, 0.0f, 1.0f);
	}

	if (m_Mode == CROSSFADE_MODE_POSITION_RELATIVE_PAN) 
	{

		audRequestedSettings *requestedSettingsForPosition = (audRequestedSettings*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId,GetTopLevelRequestedSettingsIndex());
		//audRequestedSettings *requestedSettingsForPosition = GetRequestedSettings();
		if (requestedSettingsForPosition)
		{
			// factor-in our overall damping, and the damping due to distance
			f32 nonDistanceDamping = 1.0f;
			if (m_PositionRelativePanDampingVariable)
			{
				nonDistanceDamping = AUD_GET_VARIABLE(m_PositionRelativePanDampingVariable);
			}
			else
			{
				nonDistanceDamping = m_PositionRelativePanDamping;
			}

			requestedSettingsForPosition->SetPositionRelativePanDamping((1.0f - ratio) * nonDistanceDamping);
		}
	}
	else
	{
		m_FrontendChildVolume = audDriverUtil::ComputeDbVolumeFromLinear(1.0f - ratio);
		m_PositionedChildVolume = audDriverUtil::ComputeDbVolumeFromLinear(ratio);
	}
	return ratio;
}

} // namespace rage
