//
// audiosoundtypes/speechdefs.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#if !__SPU

#ifndef SPEECH_DEFS_H
#define SPEECH_DEFS_H

namespace rage {

//Disable struct alignment.

#pragma pack(push, r1, 1)


//
// PURPOSE
//  Defines the structure of the voice context lookup table produced by the Speech Builder and used by the audSpeechSound.
//
typedef struct
{
	union
	{
		u32 bankNameIndex;						//An index into the speech bank name table.
		char *bankName;
	};
	union
	{
		s32 variationDataOffsetBytes;			//W.r.t start of concatenated variation data arrays.
		u8 *variationData;
	};
	u32 nameHash;								//Genuine 32-bit hash.
	u8 contextData;
	u8 numVariations;
} audVoiceContextLookupEntry;

//
// PURPOSE
//  Defines the structure of the voice lookup table produced by the Speech Builder and used by the audSpeechSound.
//
typedef struct
{
	union
	{
		u32 contextsOffsetBytes;				//W.r.t start of concatenated voice context lookup tables.
		audVoiceContextLookupEntry *contexts;
	};
	u32 nameHash;								//Genuine 32-bit hash.
	u16 numContexts;
	u8 painHash;								//Hash & 0xff
} audVoiceLookupEntry;

//Restore struct alignment.
#pragma pack(pop, r1)

} // namespace rage

#endif // SPEECH_DEFS_H

#endif // !__SPU
