//
// audioengine/dynamicentitysound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DYNAMICENTITYSOUND_H
#define AUD_DYNAMICENTITYSOUND_H

#include "audiosoundtypes/sound.h"

#include "audiosoundtypes/sounddefs.h"

namespace rage {

// PURPOSE
//  A dynamic entity sound dynamically creates its child from an audio entity.
class audDynamicEntitySound : public audSound
{
public:
	
	~audDynamicEntitySound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:

	bool m_ShouldChooseOnInit;
};

} // namespace rage

#endif // AUD_DYNAMICENTITYSOUND_H
