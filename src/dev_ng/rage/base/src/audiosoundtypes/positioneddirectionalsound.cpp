//
// audioengine/positionedsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "positioneddirectionalsound.h"
#include "audioengine/engine.h"
#include "audioengine/environment.h"
#include "audioengine/soundfactory.h"

#include "math/amath.h"
#include "vector/vector3.h"
#include "vector/matrix34.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audPositionedDirectionalSound);

audPositionedDirectionalSound::audPositionedDirectionalSound()
{
//	GetChildSound(0) = NULL;
	m_Offset = Vector3(0.0f, 0.0f, 0.0f);
	m_Matrix = NULL;
}

audPositionedDirectionalSound::~audPositionedDirectionalSound()
{
	if (GetChildSound(0))
	{
		delete GetChildSound(0);
		SetChildSound(0,NULL);
	}
}

bool audPositionedDirectionalSound::Init(const void *metadata, const audSoundInternalInitParams* initParams)
{
#ifdef __AUD_SOUND_TASK
	Assert(0);
	return false;
#else
	if(!audSound::Init(metadata, initParams))
		return false;

	PositionedDirectionalSound *positionedDirectionalSoundData = (PositionedDirectionalSound*)GetMetadata();

	m_VolumeCone.Init("LINEAR_RISE", Vector3(positionedDirectionalSoundData->ConeDirection.x,positionedDirectionalSoundData->ConeDirection.y,positionedDirectionalSoundData->ConeDirection.z), positionedDirectionalSoundData->RearAttenuation, positionedDirectionalSoundData->InnerConeAngle, positionedDirectionalSoundData->OuterConeAngle);

	
	m_Offset.x = positionedDirectionalSoundData->Position.x;
	m_Offset.y = positionedDirectionalSoundData->Position.y;
	m_Offset.z = positionedDirectionalSoundData->Position.z;

	SetChildSound(0,SOUNDFACTORY.GetChildInstanceFromOffset(positionedDirectionalSoundData->SoundRef, this, initParams));
	if (GetChildSound(0)==NULL)
	{
		return false;
	}

	return true;
#endif
}


audPrepareState audPositionedDirectionalSound::Prepare(audWaveSlot *waveSlot)
{
	if (!GetChildSound(0))
	{
		return AUD_PREPARE_FAILED;
	}
	else
	{
		GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset());
		return GetChildSound(0)->ManagedPrepare(waveSlot, GetCanDynamicallyLoad());
	}
}

void audPositionedDirectionalSound::AudioPlay(u32 timeInMs)
{
	// Work out how far through we want to play, based on precalculated PlaybackStartOffset
	u32 startOffset = GetPlaybackStartOffset(); // this is always in ms, not percentage

	if (GetChildSound(0))
	{
		GetChildSound(0)->SetStartOffset((s32)startOffset, false);
		GetChildSound(0)->_ManagedAudioPlay(timeInMs);
	}
}

bool audPositionedDirectionalSound::AudioUpdate(u32 timeInMs)
{
	if (GetChildSound(0))
	{
		return GetChildSound(0)->_ManagedAudioUpdate(timeInMs);
	}
	else
	{
		return false;
	}
}

void audPositionedDirectionalSound::AudioKill()
{
	if (GetChildSound(0))
	{
		GetChildSound(0)->_ManagedAudioKill();
	}
}

s32 audPositionedDirectionalSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	if (GetChildSound(0))
	{
		return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
	}
	else
	{
		return AUD_SOUND_LENGTH_UNKNOWN;
	}
}

void audPositionedDirectionalSound::GameThreadUpdate(u32 UNUSED_PARAM(timeInMs))
{
	// Work out the position of the sound, and store this in the RequestedSettings structure, to make
	// it thread safe nice and easily.
	// We use a 3d offset given from code if there's a non-zero one, and metadata if not.
	Vector3 absolute;
	if (m_Matrix == NULL)
	{
		return;
	}

	absolute = m_Matrix->d;

	// This is *basically* right, but could well have signs wrong - I've not checked it yet.
	absolute.Add(m_Matrix->a*(m_Offset.x));
	absolute.Add(m_Matrix->b*(m_Offset.y));
	absolute.Add(m_Matrix->c*(m_Offset.z));
	
	SetRequestedPosition(absolute);

	// do directionality stuff ...
	GetChildSound(0)->SetRequestedVolume(m_VolumeCone.ComputeAttenuation());
}

/*
void audPositionedDirectionalSound::SetOffset(Vector3& offset)
{
	m_Offset = offset;
}
*/

void audPositionedDirectionalSound::SetMatrix(Matrix34* matrix)
{
	audSound::SetMatrix(matrix);
	m_Matrix = matrix;
	m_VolumeCone.SetMatrix(matrix);
}

} // namespace rage
