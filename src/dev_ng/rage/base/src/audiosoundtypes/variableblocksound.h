// 
// audiosoundtypes/variableblocksound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_VARIABLEBLOCKSOUND_H
#define AUD_VARIABLEBLOCKSOUND_H

#include "audiosoundtypes/sound.h"

#include "audiosoundtypes/sounddefs.h"

namespace rage {

struct audVariable
{
	u32 nameHash;
	float value;
#if AUD_SUPPORT_RAVE_EDITING
	float valueOffset;
#endif
};

// PURPOSE
//  A scripted sound plays all of its children in turn, and maintains a variable block.
class audVariableBlockSound : public audSound
{
public:
	friend class audRemoteControl;

	~audVariableBlockSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audVariableBlockSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	// PURPOSE
	//  Scans through its own variable block, and if doesn't find variable, asks its parent, if it has one
	audVariableHandle _FindVariableUpHierarchy(const u32 nameHash);
	// PURPOSE
	//  Scans through its own variable block, and if doesn't find variable, asks its children, if it has any
	audVariableHandle _FindVariableDownHierarchy(const u32 nameHash, const audSound::VariableUsage usage);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

#if __SOUND_DEBUG_DRAW
	AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);


private:

	u32 GetNumAudioVariables() const { return m_NumAudioVariables; }
	u32 GetNumGameVariables() const { return m_NumGameVariables; }

	void GetAudioVariableFromIndex(const u32 index, u32 &nameHash, float &value) const;
	void GetGameVariableFromIndex(const u32 index, u32 &nameHash, float &value) const;

	// PURPOSE
	//  Returns the address of the variable, if it belongs to us. If not, returns NULL. This isn't an overloaded
	//  thing, it's just an internal helper function used by our overloaded GetVariableAddressUp/DownHierarchy().
	audVariableHandle		GetVariableHandle(const u32 nameHash, const audSound::VariableUsage usage);

	u32 m_NumAudioVariables;
	u32 m_NumGameVariables;

	u8 m_AudioVariableSlotIndex;
	u8 m_GameVariableSlotIndex;
};

} // namespace rage

#endif // AUD_VARIABLEBLOCKSOUND_H
