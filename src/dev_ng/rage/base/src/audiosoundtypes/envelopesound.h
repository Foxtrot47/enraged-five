// 
// audiosoundtypes/envelopesound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_ENVELOPESOUND_H
#define AUD_ENVELOPESOUND_H

#include "sound.h"

namespace rage
{

enum
{
	AUD_ENVELOPE_NOT_STARTED,
	AUD_ENVELOPE_ATTACK,
	AUD_ENVELOPE_DECAY,
	AUD_ENVELOPE_SUSTAIN,
	AUD_ENVELOPE_RELEASE,
	AUD_ENVELOPE_FINISHED
};

enum
{
	AUD_TOTAL_ENVELOPE_NONE,
	AUD_TOTAL_ENVELOPE_PROCESSING,
	AUD_TOTAL_ENVELOPE_RELEASE_VIA_CHILDREN
};

enum
{
	AUD_ATTACK_CURVE = 0,
	AUD_DECAY_CURVE,
	AUD_RELEASE_CURVE,
};

struct audEnvelopeSoundState
{
	audEnvelopeSoundState()
	{
		m_CodeSetChildRef.SetInvalid();
		m_IsCodeSetEnvelope = false;
	}
	audCurve curves[3];
	audSoundScratchInitParams m_ScratchInitParams;
	s32 m_Hold, m_Release;
	audMetadataRef m_CodeSetChildRef;
	u16 m_Attack, m_Decay;	
	u8 m_Sustain;
	bool m_IsCodeSetEnvelope;
};

class audEnvelopeSound : public audSound
{
public:
	
	~audEnvelopeSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audEnvelopeSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
	void SetRequestedEnvelope(const u32 attack, const u32 decay, const u32 sustain, const s32 hold, const s32 release);
	void SetChildSoundReference(const u32 soundHash);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();
	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	// PURPOSE
	//  Set the release envelope in motion. This overrides the base sound version, so it also clears various settings
	//  so that the default behaviour isn't applied as well.
	void ActionReleaseRequest(const u32 timeInMs)
	{
		ActionReleaseRequestInternal(timeInMs, false);
	}

	static void sActionReleaseRequest(audEnvelopeSound* _this, u32 timeInMs)
	{
		_this->ActionReleaseRequest(timeInMs);
	}


#if __SOUND_DEBUG_DRAW
	AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

	void SetRequestedRelease(s32 release);

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

private:

	// PURPOSE
	//  Calculates the start times for attack and decay phases of the metadata envelope. This could be
	//  done every frame, but doing it once save computation effort, since the metadata one won't change over
	//  the lifespan of the sound.
	void ComputeMetadataEnvelopeStartTimes();

	bool ApplyEnvelope(u32 timeInMs);
	// PURPOSE
	//  Calculates the attenuation due to the envelopes. Work on states, for SoundAssert() safety and cheapness.
	bool ComputeEnvelopeAttenuationAndState(u32 timeSinceTriggered, u32 timeInMs);
	bool ComputeMetadataEnvelopeAttenuationAndState(s32 timeSinceTriggered);
	
	// PURPOSE
	//	Sets up the child sound
	void ChildSoundCallback(const u32 timeInMs, const u32 index);

	void ActionReleaseRequestInternal(const u32 timeInMs, const bool isReleasingDueToEnvelope);

	void GetMetadataEnvelope(u32 &attack, u32 &decay, u32 &sustain, s32 &hold, s32 &release) const;
	void ApplyEnvelopeToChild(audSoundCombineBuffer &combineBuffer);
	
	f32				m_AttackFactor;
	f32				m_DecayFactor;
	f32				m_ReleaseFactor;
	s32				m_AttackStartTime;
	s32				m_DecayStartTime;
	s32				m_DecayStopTime;
	s32				m_HoldStopTime;
	s32				m_ReleaseStartTime;
	s32				m_ReleaseStopTime;

	audVariableHandle m_AttackVariable;
	audVariableHandle m_DecayVariable;
	audVariableHandle m_HoldVariable;
	audVariableHandle m_SustainVariable;
	audVariableHandle m_ReleaseVariable;

	audVariableHandle m_OutputVariable;

	f32 m_OutputMin, m_OutputMax;

	s32 m_Hold, m_Release;
	u16 m_Attack, m_Decay;
	
	u8	m_Sustain;
	u8	m_TotalEnvelopeState;
	u8	m_EnvelopeState;	
	u8	m_CurveSlotIndex;

	u16	m_Mode : 8;
	u16 m_IsEnvelopeActive : 1;
	u16 m_HasReleaseEnvelope : 1;
	u16 m_HasEnvelopeFinished : 1;
	u16 m_IsCodeSetEnvelope : 1;
	u16 m_ShouldStopChildren : 1;
	u16 m_HadExternalRelease : 1;
	u16 m_ShouldCascadeRelease : 1;
};

}//namespace rage
#endif // AUD_ENVELOPESOUND_H
