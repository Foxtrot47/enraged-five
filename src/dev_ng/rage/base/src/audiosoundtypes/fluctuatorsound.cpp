// 
// audiosoundtypes/fluctuatorsound.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "fluctuatorsound.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/debug.h"
#include "vectormath/legacyconvert.h"
#include "phcore/conversion.h"
#include "grcore/debugdraw.h" 

namespace rage 
{
	AUD_IMPLEMENT_STATIC_WRAPPERS(audFluctuatorSound);

	audFluctuatorSound::~audFluctuatorSound()
	{
		for(u32 loop = 0; loop < kMaxFluctuatorSlotIds; loop++)
		{
			if(m_FluctuatorSlotIds[loop] != 0xff)
			{
				audSound *slot = (audSound *)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_FluctuatorSlotIds[loop]);
				sm_Pool.DeleteSound(slot, m_InitParams.BucketId);
				m_FluctuatorSlotIds[loop] = 0xff;
			}
		}
		
		if(GetChildSound(0))
		{
			DeleteChild(0);
		}
	}

#if !__SPU
	audFluctuatorSound::audFluctuatorSound()
	{
		m_NumFluctuators = 0;

		for(u32 i = 0; i < kMaxFluctuatorSlotIds; i++)
		{
			m_FluctuatorSlotIds[i] = 0xff;
		}
	}

	bool audFluctuatorSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
			return false;

		FluctuatorSound * settings = (FluctuatorSound*)GetMetadata();
		SetChildSound(0,SOUNDFACTORY.GetChildInstance(settings->SoundRef, this, initParams, scratchInitParams, false));

		Assign(m_NumFluctuators, settings->numFluctuators);

		const u32 numFluctuatorsPerSlot = sm_Pool.GetSoundSlotSize() / sizeof(FluctuatedVariable);
		const u32 numFluctuatorSlots = (u32)ceilf(m_NumFluctuators / (float)numFluctuatorsPerSlot);

		if(!audVerifyf(numFluctuatorSlots < kMaxFluctuatorSlotIds, "%s exceeded max fluctuators (limit is %u, sound has %u)", GetName(), kMaxFluctuatorSlotIds * numFluctuatorsPerSlot, m_NumFluctuators))
		{
			return false;
		}

		for(u32 i = 0; i < numFluctuatorSlots; i++)
		{
			FluctuatedVariable *fluctuatorSlot = (FluctuatedVariable*)sm_Pool.AllocateSoundSlot(numFluctuatorsPerSlot * sizeof(FluctuatedVariable), m_InitParams.BucketId, true);
			if(!fluctuatorSlot)
			{
				return false;
			}

			Assign(m_FluctuatorSlotIds[i], sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, fluctuatorSlot));
		}

		u32 currentFluctuatorSlot = 0;
		u32 slotRelativeFluctuatorIndex = 0;

		if(m_NumFluctuators)
		{
			FluctuatedVariable *fluctuatorSlot = GetFluctuatorSlot(currentFluctuatorSlot);

			for(u32 i = 0; i < m_NumFluctuators; i++)
			{
				if(slotRelativeFluctuatorIndex >= numFluctuatorsPerSlot)
				{
					currentFluctuatorSlot++;
					slotRelativeFluctuatorIndex = 0;
					fluctuatorSlot = GetFluctuatorSlot(currentFluctuatorSlot);
				}
				f32 bandOneMin = settings->Fluctuator[i].BandOneMinimum;
				f32 bandOneMax = settings->Fluctuator[i].BandOneMaximum; 
				f32 bandTwoMin = settings->Fluctuator[i].BandTwoMinimum; 
				f32 bandTwoMax = settings->Fluctuator[i].BandTwoMaximum; 

				if(fluctuatorSlot[slotRelativeFluctuatorIndex].Destination == PARAM_DESTINATION_PITCH)
				{
					bandOneMin = audDriverUtil::ConvertPitchToRatio(settings->Fluctuator[i].BandOneMinimum);
					bandOneMax = audDriverUtil::ConvertPitchToRatio(settings->Fluctuator[i].BandOneMaximum);
					bandTwoMin = audDriverUtil::ConvertPitchToRatio(settings->Fluctuator[i].BandTwoMinimum);
					bandTwoMax = audDriverUtil::ConvertPitchToRatio(settings->Fluctuator[i].BandTwoMaximum);
				}
				if(settings->Fluctuator[i].Mode == PROBABILITY_BASED)
				{
					fluctuatorSlot[slotRelativeFluctuatorIndex].Fluctuator.FluctuatorProb.Init(settings->Fluctuator[i].IncreaseRate, 
						settings->Fluctuator[i].DecreaseRate, 
						bandOneMin, 
						bandOneMax, 
						bandTwoMin,
						bandTwoMax, 
						settings->Fluctuator[i].IntraBandFlipProbabilty, 
						settings->Fluctuator[i].InterBandFlipProbabilty,
						settings->Fluctuator[i].InitialValue);
				}
				else
				{
					fluctuatorSlot[slotRelativeFluctuatorIndex].Fluctuator.FluctuatorTime.Init(settings->Fluctuator[i].IncreaseRate, 
						settings->Fluctuator[i].DecreaseRate, 
						bandOneMin, 
						bandOneMax, 
						settings->Fluctuator[i].MinSwitchTime, 
						settings->Fluctuator[i].MaxSwitchTime,
						settings->Fluctuator[i].InitialValue);
				}
				Assign(fluctuatorSlot[slotRelativeFluctuatorIndex].Mode, settings->Fluctuator[i].Mode);
				Assign(fluctuatorSlot[slotRelativeFluctuatorIndex].Destination, settings->Fluctuator[i].Destination);

				if(fluctuatorSlot[slotRelativeFluctuatorIndex].Destination == PARAM_DESTINATION_VARIABLE)
				{
					fluctuatorSlot[slotRelativeFluctuatorIndex].OutputVariable = _FindVariableUpHierarchy(settings->Fluctuator[i].OutputVariable);

					if(!audVerifyf(fluctuatorSlot[slotRelativeFluctuatorIndex].OutputVariable, "%s: Failed to find output variable %u (fluctuator %u)", GetName(), settings->Fluctuator[i].OutputVariable, i))
					{
						return false;
					} 
				}
				else
				{
					fluctuatorSlot[slotRelativeFluctuatorIndex].OutputVariable = NULL;
				}
				slotRelativeFluctuatorIndex++;
			}
		}
		return true;
	}
#endif // !__SPU

	void audFluctuatorSound::UpdateFluctuatorSoundVariables(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		const u32 numFluctuatorsPerSlot = sm_Pool.GetSoundSlotSize() / sizeof(FluctuatedVariable);
		u32 slotRelativeIndex = 0;
		u32 currentSlotIndex = 0;


		if(m_NumFluctuators)
		{
			FluctuatedVariable* fluctuatorSlot = GetFluctuatorSlot(currentSlotIndex);

			for(u32 i = 0; i < m_NumFluctuators; i++)
			{
				if(slotRelativeIndex >= numFluctuatorsPerSlot)
				{
					slotRelativeIndex = 0;
					currentSlotIndex++;
					fluctuatorSlot = GetFluctuatorSlot(currentSlotIndex);
				}

				FluctuatedVariable &fluctuatedVariable = fluctuatorSlot[slotRelativeIndex];
				f32 result = 0.f;
				if(fluctuatedVariable.Mode == PROBABILITY_BASED)
				{
					result = fluctuatedVariable.Fluctuator.FluctuatorProb.CalculateValue();
				}
				else
				{
					result = fluctuatedVariable.Fluctuator.FluctuatorTime.CalculateValue(timeInMs);
				}

				switch(fluctuatedVariable.Destination)
				{
				case PARAM_DESTINATION_VOLUME:
					combineBuffer.Volume += audDriverUtil::ComputeDbVolumeFromLinear(result);
					break;
				case PARAM_DESTINATION_PITCH:
					combineBuffer.Pitch += (s16)audDriverUtil::ConvertRatioToPitch(result);
					break;
				case PARAM_DESTINATION_PAN:
					combineBuffer.Pan = (s16)CombinePan(combineBuffer.Pan, (s32)(result * 360.f));
					break;
				case PARAM_DESTINATION_LPF:
					combineBuffer.LPFCutoff = Min(combineBuffer.LPFCutoff, (u16)(result * kVoiceFilterLPFMaxCutoff));
					break;
				case PARAM_DESTINATION_HPF:
					combineBuffer.HPFCutoff = Max(combineBuffer.HPFCutoff, (u16)(result * kVoiceFilterHPFMaxCutoff));
					break;
				case PARAM_DESTINATION_VARIABLE:
					SoundAssert(fluctuatedVariable.OutputVariable);
					AUD_SET_VARIABLE(fluctuatedVariable.OutputVariable, result);
					break;
				default:
					SoundAssert(0);
					break;
				}

				slotRelativeIndex++;
			}
		}
	}

	audFluctuatorSound::FluctuatedVariable* audFluctuatorSound::GetFluctuatorSlot(const u32 index)
	{
		SoundAssert(m_FluctuatorSlotIds[index] != 0xff);
		return (FluctuatedVariable*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_FluctuatorSlotIds[index]);
	}

	void audFluctuatorSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		audSound* childSound = GetChildSound(0);

		if(childSound)
		{
			UpdateFluctuatorSoundVariables(timeInMs, combineBuffer);
			childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}

	bool audFluctuatorSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		if(GetChildSound(0))
		{
			SoundAssert (GetChildSound(0)->GetPlayState() == AUD_SOUND_PLAYING);
			UpdateFluctuatorSoundVariables(timeInMs, combineBuffer);
			return GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
		}

		return false;
	}

	void audFluctuatorSound::AudioKill()
	{
		if(GetChildSound(0))
		{
			GetChildSound(0)->_ManagedAudioKill();
		}
	}

	audPrepareState audFluctuatorSound::AudioPrepare(audWaveSlot *slot, bool checkStateOnly)
	{
		if(GetChildSound(0))
		{
			GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			return GetChildSound(0)->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		}
		else
		{
			return AUD_PREPARED;
		}
	}

	s32 audFluctuatorSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		if(GetChildSound(0))
		{
			return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			// We don't support lengths and start offets on logical sounds with no children
			return kSoundLengthUnknown;
		}
	}
}
