//
// audioengine/randomizedsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_RANDOMIZED_SOUND_H
#define AUD_RANDOMIZED_SOUND_H

#include "sound.h"
#include "sounddefs.h"

namespace rage
{

// PURPOSE
//  A randomized sound plays one of a number of child subsounds, picked at random. A history is maintained
//  across sound instances, and the same child sound will not be selected for as long as it is maintained
//  in that history.
class audRandomizedSound : public audSound
{
public:
	AUD_DECLARE_STATIC_WRAPPERS;

	~audRandomizedSound();

#if !__SPU
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	// PURPOSE
	//  Works out where our variations are stored, before ComputeVariationIndex() picks one.
	static void GetVariations(const RandomizedSound *metadata, u8 &size, RandomizedSound::tVariations **ptr);
	
	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

	// PURPOSE
	//  Returns the most recently selected variation inde
	inline u8 GetSelectedVariationIndex() const { return m_SelectedVariationIndex;}

private:
	
	// PURPOSE
	//  Returns true if a selected variation is in the history.
	bool IsVariationInHistory(const RandomizedSound *metadata, const u8 variation);

	// PURPOSE
	//  Adds a selected variation to the history, so it won't be selected again too soon.
	void AddVariationToHistory(RandomizedSound *metadata, const u8 variation);

	// PURPOSE
	//  Based on our variation choices and history, selects a valid variation at randon.
	u8 ComputeVariationIndex(const RandomizedSound *metadata,const u32 numVariations, const RandomizedSound::tVariations *variations);

private:
	u8 m_SelectedVariationIndex;

};

} // namespace rage

#endif //AUD_RANDOMIZED_SOUND_H
