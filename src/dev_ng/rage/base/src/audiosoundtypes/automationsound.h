// 
// audiosoundtypes/automationsound.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_AUTOMATIONSOUND_H
#define AUD_AUTOMATIONSOUND_H

#include "audiohardware/waveref.h"
#include "audiosoundtypes/sound.h"
#include "audiosoundtypes/sounddefs.h"

#include "atl/array.h"
#include "atl/bitset.h"

namespace rage 
{
	class audAutomationSound : public audSound
	{
	public:

		~audAutomationSound();

		AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
		audAutomationSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();

		// PURPOSE
		//  Implements base class function for this sound.
		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

#if __SOUND_DEBUG_DRAW
		AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

	private:
		enum {kMaxOutputs = 16};

		struct AutomationSequenceState
		{
			audSoundScratchInitParams ScratchInitParams;
			enum {kMaxNoteRanges = 16};
			struct NoteRange
			{
				audMetadataRef soundRef;
				u8 firstNoteId;
				u8 lastNoteId;
				u8 mode;
			}noteRanges[kMaxNoteRanges];
			u8 NumNoteRanges;
									
			atRangeArray<u8, kMaxOutputs> ChannelIds;
			atRangeArray<audVariableHandle, kMaxOutputs> OutputVariables;

		};
		const AutomationSequenceState *GetSequenceState();

		void Process(const u32 timeStepMs, const AutomationSequenceState *sequenceState);
		void ProcessEvent(const AutomationSequenceState *sequenceState);
		const u8 *GetDataPtr() const;

		void SetOutputValue(const u32 channelId, const u32 val, const AutomationSequenceState *state);
		void ProcessNoteOn(const u32 channelId, const u32 noteId, const u32 velocity, const AutomationSequenceState *sequenceState);
		void ProcessNoteOff(const u32 channelId, const u32 noteId, const u32 velocity, const AutomationSequenceState *sequenceState);
		void ProcessNoteAftertouch(const u32 channelId, const u32 noteId, const u32 velocity, const AutomationSequenceState *sequenceState);

		audMetadataRef GetSoundRefForNote(const u32 noteId, u32 &noteDelta, const AutomationSequenceState *state) const;

		bool ParseHeader();

		bool ReadDword(u32 &result);
		bool ReadWord(u16 &result);
		bool ReadVariableLength(u32 &result);
		bool ReadByte(u8 &result);
		bool Read14Bit(u32 &result);
		bool Read7Bit(u32 &result);

		SPU_ONLY(void PrefetchData(const size_t bytes));

		bool AtEnd() { return m_CurrentOffset >= m_DataSize; }
		
		const u8 *m_Data;
		u32 m_CurrentOffset;
		u32 m_DataSize;

		u32 m_MidiDataSize;
		u32 m_LastEventTime;
		float m_TimeOffset;
		u32 m_NumOutputs;
		u32 m_LastUpdateTime;

		atRangeArray<u8, kMaxChildren> m_ChildNoteDelta;
		atRangeArray<u8, kMaxChildren> m_ChildVelocity;
		atRangeArray<u8, kMaxChildren> m_ChildNote;
		atFixedBitSet<kMaxChildren> m_IsPreparing;
		
		float m_PlaybackRate;
		audVariableHandle m_PlaybackRateVariable;

		audWaveRef m_MidiRef;
		u32 m_MidiWaveNameHash;

		PS3_ONLY(u32 m_DataCacheOffset);

		u16 m_BankNameIndex;
		u8 m_SequenceStateSlot;		
		
		PS3_ONLY(u8 m_DataCacheSlot);

		bool m_IsLooping;

	};

} // namespace rage

#endif // AUD_AUTOMATIONSOUND_H
