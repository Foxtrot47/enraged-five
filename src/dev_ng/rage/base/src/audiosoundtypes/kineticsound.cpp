// 
// audiosoundtypes/directionalsound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "kineticsound.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/debug.h"
#include "vectormath/legacyconvert.h"
#include "phcore/conversion.h"
#include "grcore/debugdraw.h" 

namespace rage 
{

	audKineticSoundState::audKineticSoundState()
	{
		m_VelocityVariable = m_InertiaVariable = m_AngVelVariable = m_ForceVariable = m_AccelerationVariable = m_AngAccelVariable = m_AccelDeltaVariable = NULL;		
	}
	AUD_IMPLEMENT_STATIC_WRAPPERS(audKineticSound);

	audKineticSound::~audKineticSound()
	{
		if(m_StateSlotIndex != 0xff)
		{
			audSound *slot = (audSound *)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_StateSlotIndex);
			sm_Pool.DeleteSound(slot, m_InitParams.BucketId);
			m_StateSlotIndex = 0xff;
		}
		if(!IsBeingRemovedFromHierarchy() && GetChildSound(0))
		{
			DeleteChild(0);
		}
	}

#if !__SPU

	audKineticSound::audKineticSound()
	{
		m_StateSlotIndex = 0xff;
	}

	bool audKineticSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
			return false;


		audKineticSoundState *state = (audKineticSoundState*)sm_Pool.AllocateSoundSlot(sizeof(audKineticSoundState), m_InitParams.BucketId, true);
		if(!state)
		{
			return false;
		}


		::new(state) audKineticSoundState();
		m_StateSlotIndex = sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, state);

		const KineticSound * settings = (KineticSound*)GetMetadata();
		state->m_Mass = settings->Mass;

		state->m_VelocityVariable = _FindVariableUpHierarchy(audStringHash("soundVelocity"));
		state->m_InertiaVariable = _FindVariableUpHierarchy(audStringHash("soundInertia"));
		state->m_AccelerationVariable = _FindVariableUpHierarchy(audStringHash("soundAcceleration"));
		state->m_ForceVariable = _FindVariableUpHierarchy(audStringHash("soundForce"));
		state->m_AngVelVariable = _FindVariableUpHierarchy(audStringHash("soundAngVelocity"));
		state->m_AngAccelVariable = _FindVariableUpHierarchy(audStringHash("soundAngAccel"));
		state->m_AccelDeltaVariable = _FindVariableUpHierarchy(audStringHash("soundAccelDelta"));

		TristateValue val = AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_KINETICSOUND_VELOCITYORIENTATION); 
		m_UseOrientaionForVelocity = (val == AUD_TRISTATE_TRUE);

		f32 x = Cosf(PH_DEG2RAD(settings->YawAngle))*Cosf(PH_DEG2RAD(settings->PitchAngle));
		f32 y = Sinf(PH_DEG2RAD(settings->YawAngle))*Cosf(PH_DEG2RAD(settings->PitchAngle));
		f32 z = Sinf(PH_DEG2RAD(settings->PitchAngle));

		state->m_Orientation = Vec3V(x,y,z);

		SetChildSound(0,SOUNDFACTORY.GetChildInstance(settings->SoundRef, this, initParams, scratchInitParams, false));

		return true;
	}
#endif // !__SPU

	void audKineticSound::UpdateKineticSoundVariables(u32 UNUSED_PARAM(timeInMs))
	{

		Assert(m_StateSlotIndex != 0xff);
		audKineticSoundState *state = (audKineticSoundState*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_StateSlotIndex);
	
		audRequestedSettings * settings = GetRequestedSettingsFromIndex(m_InitParams.BucketId, GetTopLevelRequestedSettingsIndex());
		if(settings)
		{
			u8 gameFrame = (u8)settings->GetSettings_AudioThread().UpdateFrame;
			if(gameFrame != m_GameFrame)
			{
				m_GameFrame = gameFrame;

				QuatV quat;
				quat.ZeroComponents();
				settings->GetOrientation(quat);
				
				Vec3V eulers, oldEulers;
				Vec3V transformedOri = Transform(quat, state->m_Orientation);


				eulers = QuatVToEulersXYZ(quat);
				oldEulers = QuatVToEulersXYZ(m_LastOrientation);
				Vec3V lastTransOri = Transform(m_LastOrientation, state->m_Orientation);
				
				u32 time = settings->GetGameTimeStep(); 
				f32 invDeltaTime = time > 0 ? 1000.f/time : 0;

				Vec3V velocity;
				settings->GetVelocity(velocity);
				velocity = Scale(velocity, ScalarV(30.f));
				f32 variableVelocity = Abs(m_UseOrientaionForVelocity ? Dot(transformedOri, velocity).Getf() : Mag(velocity).Getf());
				const f32 oriAcceleration = (Dot(velocity, transformedOri).Getf() - Dot(lastTransOri, m_LastVelocity).Getf());
				const f32 angularSpeed = Abs(Mag(Subtract(transformedOri, lastTransOri)).Getf()*invDeltaTime);
				f32 angularAccel = Abs(angularSpeed - m_LastAngVel);
				f32 soundAcceleration =  Abs(m_UseOrientaionForVelocity? oriAcceleration : Abs(variableVelocity - m_LastVariableVelocity));

				if(variableVelocity == 0.f || m_LastVariableVelocity == 0.f)
				{
					soundAcceleration = 0.f;
				}

				m_LastVariableVelocity = variableVelocity;


				if(state->m_VelocityVariable)
				{
					AUD_SET_VARIABLE(state->m_VelocityVariable, variableVelocity);
				}
				
				if(state->m_InertiaVariable)
				{
					AUD_SET_VARIABLE(state->m_InertiaVariable, variableVelocity*state->m_Mass);
				}
				if(state->m_ForceVariable)
				{
					AUD_SET_VARIABLE(state->m_ForceVariable, soundAcceleration*state->m_Mass);
				}

				if(state->m_AccelerationVariable)
				{
					AUD_SET_VARIABLE(state->m_AccelerationVariable, soundAcceleration);
				}
				if(state->m_AccelDeltaVariable)
				{
					AUD_SET_VARIABLE(state->m_AccelDeltaVariable, Abs(soundAcceleration - m_LastAccelleration)*invDeltaTime);
				}
					
				if(state->m_AngVelVariable)
				{
					AUD_SET_VARIABLE(state->m_AngVelVariable, angularSpeed);
				}

				if(state->m_AngAccelVariable)
				{
					AUD_SET_VARIABLE(state->m_AngAccelVariable, angularAccel);
				}

				m_LastVelocity = velocity;
				m_LastOrientation = quat;
				m_LastAngVel = angularSpeed;
				m_LastAccelleration = soundAcceleration;

			}
		}
	}

	
	void audKineticSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		audSound* childSound = GetChildSound(0);

		Assert(m_StateSlotIndex != 0xff);

		if(childSound)
		{
			UpdateKineticSoundVariables(timeInMs);
			childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}

	}

	bool audKineticSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{

		if(GetChildSound(0))
		{

			SoundAssert (GetChildSound(0)->GetPlayState() == AUD_SOUND_PLAYING);
			UpdateKineticSoundVariables(timeInMs);


			return GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
		}

		return false;
	}

	void audKineticSound::AudioKill()
	{

		audSound* childSound = GetChildSound(0);
		if(childSound)
		{
			childSound->_ManagedAudioKill();
		}
	}

	audPrepareState audKineticSound::AudioPrepare(audWaveSlot *slot, bool checkStateOnly)
	{

		audSound* childSound = GetChildSound(0);
		if(childSound)
		{
			childSound->SetStartOffset((s32)GetPlaybackStartOffset());
			return childSound->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		}
		else
		{
			return AUD_PREPARED;
		}
	}

	s32 audKineticSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{

		audSound* childSound = GetChildSound(0);
		if(childSound)
		{
			return childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			// we have no sound so we have a length of 0ms
			if (isLooping)
			{
				*isLooping = false;
			}
			return 0;
		}
	}
}
