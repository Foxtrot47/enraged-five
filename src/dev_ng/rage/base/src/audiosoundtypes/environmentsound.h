//
// audioengine/environmentsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SIMPLE_ENVIRONMENT_SOUND_H
#define AUD_SIMPLE_ENVIRONMENT_SOUND_H

#include "sound.h"
#include "sounddefs.h"
#include "audioengine/requestedsettings.h"
#include "audioengine/widgets.h"
#include "audiohardware/voicemgr.h"
#include "audiohardware/voicesettings.h"
#include "audiohardware/waveref.h"
#include "audiohardware/waveslot.h"

namespace rage {

struct audEnvironmentSoundMetrics;

class audPcmSource;
// PURPOSE
//  An environment sound controls the playback of a physical wave, via a virtual voice. It handles calculation of
//  playback volume, pitch, position, etc. 
class audEnvironmentSound : public audSound
{
public:

#if !defined(__AUD_ENVIRONMENT_UPDATE)

	AUD_DECLARE_STATIC_WRAPPERS;
	
	~audEnvironmentSound();

	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

#if !__SPU	
	static void InitClass();

	audEnvironmentSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

#if __BANK
	static void DebugDrawInvalidCategorySounds();
	static void SetRecordInvalidCategorySounds(bool record) { sm_RecordInvalidSoundHistory = record; }
	static void SetDisableMixgroupSoundProcess(bool disable) { sm_DisableMixgroups = disable; }
#endif

	static void SetMixGroupPitchFrequencyScalingEnabled(bool enabled) { sm_EnableMixGroupPitchFrequencyScaling = enabled; }

	void InitEnvironment(const s32 pcmSourceId, const float assetHeadroom, const s32 assetLength = kSoundLengthUnknown, const bool isAssetLooping = false, const u32 firstPeakLevelSample = kDefaultPeakLevel_u16, const u32 pcmSourceCost = audVoiceSettings::kDefaultVoiceCost);
	void InitSubmix(const s32 submixId);

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);

	// PURPOSE
	void AudioKill();

	// PURPOSE
	// Set the PCM source cost for this voice
	void SetPCMSourceCost(const u32 pcmSourceCost) { Assign(m_PcmSourceCost, pcmSourceCost); }

	// PURPOSE
	// Get the PCM source cost for this voice
	u32 GetPCMSourceCost() { return m_PcmSourceCost; }

	// PURPOSE
	//  Frees the voice associated with this voiced sound.
	void FreeVoice();
	// PURPOSE
	//  Sets the voice freq scaling factor to 0.0f (paused)
	void Pause(const u32 timeInMs);

	static void sPause(audEnvironmentSound *_this, u32 timeInMs)
	{
		_this->Pause(timeInMs);
	}

	//
	// PURPOSE
	//	Gets whether or not this sound is physical playing (via its voice.)
	// RETURNS
	//	Returns true if this sound is physically playing.
	//
	bool IsPhysicallyPlaying(void);
	
#endif
	
	void AudioUpdateVoice(audSoundCombineBuffer &combineBuffer);
	
#if __SOUND_DEBUG_DRAW
	AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

#if !defined(__AUD_ENVIRONMENT_UPDATE)
	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *UNUSED_PARAM(slot), const bool UNUSED_PARAM(checkStateOnly)){return AUD_PREPARED;};
#endif // !defined(__AUD_ENVIRONMENT_UPDATE)

	void SetIsControllingPcmSource(const bool isControlling)
	{
		m_IsControllingPcmSource = isControlling;
	}

public:
	s16 GetPcmSourceChannelId() const { return m_PcmSourceId; }
	
private:

	void SetFrequencyScalingFactor(const float scalingFactor);

	audVoiceSettings *GetVoiceSettings() const
	{
		Assert(m_VoiceId != 0xff);
		if(m_VoiceId == 0xff)
		{
			return NULL;
		}
		return sm_Pool.GetVoiceSettings(m_InitParams.BucketId, m_VoiceId);
	}

	void UpdateMixGroup(s32 mixGroupId, u32 &mixGroupInstanceId,s16 &mixGroupIndex);

	audSimpleSmoother			m_FrequencySmoother;

	f32 m_VolumeCurveScale;
	
	f32 m_DopplerFactor, m_DopplerFrequencyScalingFactor;
	f32 m_HeadroomScalingLinear;
	float m_FrequencyScalingFactor;

	u32 m_MixGroupInstanceId;
	u32 m_MixGroupPrevInstanceId;
	s16 m_MixGroupIndex;
	s16 m_MixGroupPrevIndex;

	s32 m_SubmixId;
	s32 m_AssetLengthMs;

	s16 m_PcmSourceId;
	s16 m_PcmSourceChannelId;

	u16 m_SyncMasterId;
	u16 m_SyncSlaveId;

	s16 m_CategoryIndex;

	u16 m_DefaultPeakLevel;
	u16 m_PcmSourceCost;
	
	u8 	m_VolumeCurvePlateau;

	u8 m_VoiceId;
	u8 m_SpeakerMask;
	u8 m_VolumeCurveId;
	u8 m_HPFDistanceCuveId;
	u8 m_LPFDistanceCuveId;
	u8 m_MetricsSlot;
	
	u8 m_LastUpdateFrame : 6;
	u8 m_EffectRoute : 5;
	// should attenuate over distance is 2 bit tristate
	u8 m_ShouldAttenuateOverDistance : 2;
	u8 m_ShouldApplyEnvironmentalEffects : 2;
	bool m_VoiceHasBeenUpdated : 1;
	bool m_InvertPhase : 1;
	bool m_IsControllingSubmix : 1;
	bool m_ShouldPlayPhysically : 1;
	bool m_IsControllingPcmSource : 1;
	bool m_HasStartedPcmSource : 1;
	bool m_DisableRearAttenuation : 1;
	bool m_ShouldMuteOnUserMusic : 1; // PS3 needs to set this flag on init because it can't get access to category info on update
	bool m_IsAssetLooping : 1;
	bool m_IsUncancellable : 1;
	bool m_ShouldStopWhenVirtual : 1;

	static bool sm_EnableMixGroupPitchFrequencyScaling;
#if __BANK
	struct InvalidCategorySound
	{
		u32 parentNTO;
		u32 soundNTO;
		u32 timeTriggered;
	};

	static atArray<InvalidCategorySound> sm_InvalidCategorySoundHistory;
	static bool sm_RecordInvalidSoundHistory;
	static bool sm_DisableMixgroups;
#endif
};

} // namespace rage

#endif // AUD_SIMPLE_ENVIRONMENT_SOUND_H
