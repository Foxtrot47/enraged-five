//
// audiosoundtypes/loopingsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_LOOPING_SOUND_H
#define AUD_LOOPING_SOUND_H

#include "sounddefs.h"
#include "sound.h"

namespace rage {

// PURPOSE 
//  A looping sound plays its child sound until its finished, then deletes and reinstantiates it and starts again.
// NOTE
//  This could mean playing a different set of physical subsounds, as they could be randomised. Also,
//  this sound type will always return true on Prepare(), as it prepares dynamically when it needs to play
//	another sound.
class audLoopingSound : public audSound
{
public:
	
	~audLoopingSound();

	AUD_DECLARE_STATIC_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);

#if !__SPU
	audLoopingSound();
	// PURPOSE
	// Implements functionality as required by the base class
	bool Init(const void *Metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	// Implements functionality as required by the base class
	void AudioKill();

	// PURPOSE
	// Implements functionality as required by the base class
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

#if __SOUND_DEBUG_DRAW
	AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	//  Actually a bit of a cheat - it will try and Prepare() all its children, but actually just
	//  returns true whatever, since it deals with dynamically preparing sounds during playback.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:


	// PURPOSE
	//  Plays the child sound. This is done seperately from the AudioPlay() fn, as it is
	//  called repeatedly during the life of the sound, whenever a new child sound is
	//  instantiated.
	void AudioPlayChild(u32 timeInMs, const u32 index, audSoundCombineBuffer &combineBuffer);

	// PURPOSE
	//	Sets up the child sound
	void ChildSoundCallback(const u32 timeInMs, const u32 index);

	audSoundScratchInitParams m_ScratchInitParams;

	s32 m_LoopCount;
	u32 m_LoopPoint;
	s32 m_CurrentLoopCount;
	audMetadataRef m_SoundRef;
	
	u32 m_Index;
	struct syncPair
	{
		u16 masterSyncId;
		u16 slaveSyncId;
	};
	atRangeArray<syncPair,2> m_SyncIds;
	atRangeArray<bool,2> m_IsPreparing;

	bool m_SpliceMode;

	audVariableHandle m_LoopingCountVariable;

};

} // namespace rage

#endif // AUD_LOOPING_COMPOSITE_SOUND_H
