//
// audiosoundtypes/retriggeredoverlappedsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_RETRIGGEREDOVERLAPPEDSOUND_H
#define AUD_RETRIGGEREDOVERLAPPEDSOUND_H

#include "atl/array.h"
#include "atl/bitset.h"

#include "sound.h"
#include "sounddefs.h"
#include "audioengine/metadataref.h"

namespace rage {

// PURPOSE
//  This sound type retriggers it's child sound every X milliseconds
class audRetriggeredOverlappedSound : public audSound
{
public:
	
	~audRetriggeredOverlappedSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audRetriggeredOverlappedSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	// PURPOSE
	//	Sets the time between child triggering - overrides metadata delay time
	void SetDelayTime(u32 timeInMs)
	{
		m_DelayTime = timeInMs;
	}

	// PURPOSE
	//	Overrides the metadata set loop count
	void SetLoopCount(s32 loopCount)
	{
		m_LoopCount = loopCount;
	}

	// PURPOSE
	//  This overrides the base sound version, so it also clears various settings
	//  so that the default behaviour isn't applied as well.
	void ActionReleaseRequest(const u32 timeInMs);

	static void sActionReleaseRequest(audRetriggeredOverlappedSound* _this, u32 timeInMs)
	{
		_this->ActionReleaseRequest(timeInMs);
	}

#ifdef AUD_IMPL
	void Pause(const u32 timeInMs)
	{
		BasePause(timeInMs);
	}
#endif

	void ChildSoundCallback(const u32 timeInMs, const u32 childIndex);

#if __SOUND_DEBUG_DRAW
	AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

	void ManagedAudioStopChildren();

	void SetupChildSound(const s32 which, const u32 startTimeMs, const bool isStopping);

private:
	void StartQueuedChildren(const u32 timeInMs, audSoundCombineBuffer &combineBuffer);

	bool HasOnStopSound() const { return m_OnStopSoundRef.IsValid(); }
	bool HasOnStartSound() const { return m_OnStartSoundRef.IsValid(); }

	audSoundScratchInitParams m_ScratchInitParams;

	u32  m_LastPlayTime, m_DelayTime;
	s32	 m_LoopCount;
	audMetadataRef m_SoundRef;
	audMetadataRef m_OnStopSoundRef;
	audMetadataRef m_OnStartSoundRef;
	audVariableHandle m_DelayTimeVariable;
	audVariableHandle m_LoopCountVariable;

	enum { kMaxRTOChildren = audSound::kMaxChildren };
	atRangeArray<u32, kMaxRTOChildren> m_ChildStartTime;
	
	atFixedBitSet8 m_Preparing;
	u8 m_CurrentIndex;
	
	bool m_ShouldReleaseChildren : 1;
	bool m_AllowChildToFinish : 1;
	bool m_HavePlayedOnStopSound : 1;
	bool m_HavePlayedOnStartSound : 1;
	bool m_SetLoopCountVariableOnce : 1;
};

} // namespace rage

#endif // AUD_RETRIGGEREDOVERLAPPEDSOUND_H
