// 
// audiosoundtypes/switchsound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SWITCHSOUND_H
#define AUD_SWITCHSOUND_H

#include "audiosoundtypes/sound.h"
#include "audiosoundtypes/sounddefs.h"
#include "audioengine/metadataref.h"

namespace rage
{

const u32 g_MaxSoundRefsInBasicSwitchSound = 8;
// PURPOSE
//  A switch sound plays one of a number of child subsounds, using the index supplied in a variable
class audSwitchSound : public audSound
{
public:
	
	~audSwitchSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audSwitchSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();
	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	void ChildSoundCallback(const u32 timeInMs, const u32 childIndex);

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

private:

	audSoundScratchInitParams m_ScratchInitParams;
	audSoundCombineBuffer m_CachedCombineBuffer;

	audVariableHandle m_Variable;
	audMetadataRef m_SoundRefs[g_MaxSoundRefsInBasicSwitchSound];
	u8 m_NumSoundRefs;
	u8 m_StorageSlotIndex;
	bool m_PreparingChild : 1;
	bool m_WaitingForChild : 1;
	bool m_ShouldScaleInput : 1;

};

} // namespace rage

#endif //AUD_SWITCHSOUND_H
