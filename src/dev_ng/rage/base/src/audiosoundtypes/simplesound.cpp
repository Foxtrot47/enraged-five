//
// audioengine/simplesound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "simplesound.h"

#include "environmentsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/channel.h"
#include "audiohardware/debug.h"
#include "audiohardware/waveref.h"
#include "audiohardware/waveslot.h"
#include "system/param.h"
#include "audiohardware/mixer.h"
#include "audiohardware/waveplayer.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audSimpleSound);

audSimpleSound::~audSimpleSound()
{
	if (!IsBeingRemovedFromHierarchy() && GetChildSound(0))
	{
		DeleteChild(0);
	}
	
	if(m_WavePlayerSlotId != -1)
	{
		audPcmSourceInterface::Release(m_WavePlayerSlotId);
		m_WavePlayerSlotId = -1;
	}
}

#if !__SPU

audSimpleSound::audSimpleSound()
{
	m_SlotIndex = -1;
	m_LoadedSlotIndex = -1;
	m_WavePlayerSlotId = -1;
	m_CurrentPlayTimeMs = 0;
	m_PlayTimeCalculationFrame = 0;
	m_WaveLengthMs = ~0U;
}

bool audSimpleSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
	{
		return false;
	}

	SimpleSound *soundMetadata = (SimpleSound*)GetMetadata();

	SetChildSound(0,SOUNDFACTORY.GetEnvironmentSound(this, initParams, scratchInitParams));
	if(!GetChildSound(0))
	{
		return false;
	}

	// note: when running -rave bank references are stored as hashes in metadata, so we need to resolve them
	// at runtime.
	u32 bankNameIndex = SOUNDFACTORY.GetBankIndexFromMetadataRef(soundMetadata->WaveRef.BankName);
	if(bankNameIndex >= AUD_INVALID_BANK_ID)
	{
		audErrorf("SimpleSound %s failed to find bank name index for wave %u (bank ref %u)", GetName(),soundMetadata->WaveRef.WaveName, soundMetadata->WaveRef.BankName);
		return false;
	}
	Assign(m_BankNameIndex, bankNameIndex);
	m_WaveNameHash = soundMetadata->WaveRef.WaveName;

	// cache the static slot index in metadata to save looking up every time
	// 0 is unset - store index from 1 (254 wave slots should be plenty!)
	audAssertf(audWaveSlot::GetNumWaveSlots() < 255, "Too many waveslots (%d)", audWaveSlot::GetNumWaveSlots());
	
	if(soundMetadata->WaveSlotIndex == 0)
	{
		audWaveSlot * waveSlot = audWaveSlot::FindLoadedBankWaveSlot(m_BankNameIndex);

		Assign(m_LoadedSlotIndex, audWaveSlot::GetWaveSlotIndex(waveSlot));
		if(m_LoadedSlotIndex != -1 && waveSlot->IsStatic())
		{
			Assign(soundMetadata->WaveSlotIndex, static_cast<u32>(1 + m_LoadedSlotIndex));
		}
	}
	else
	{
		Assign(m_LoadedSlotIndex, static_cast<s32>(soundMetadata->WaveSlotIndex) - 1);
	}

	return true;
}
#endif // !__SPU

audPrepareState audSimpleSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
{
	if(!slot && m_LoadedSlotIndex == -1)
	{
		slot = audWaveSlot::FindLoadedBankWaveSlot(m_BankNameIndex);

		if(!slot)
		{
			slot = audWaveSlot::FindLoadedWaveAssetWaveSlot(m_WaveNameHash, m_BankNameIndex);

			if(slot)
			{
				SoundAssert(slot->GetLoadedBankId() == m_BankNameIndex && slot->GetLoadedWaveNameHash() == m_WaveNameHash);
			}
		}
	}

	const s32 slotIndex = (m_LoadedSlotIndex!=-1?m_LoadedSlotIndex:audWaveSlot::GetWaveSlotIndex(slot));
	if(slotIndex == -1)
	{
#if !__SPU
		if(!checkStateOnly)
		{
			audWarningf("Wave bank %s (%d) not found in any wave slot and no slot specified for SimpleSound: %s", audWaveSlot::GetBankName(m_BankNameIndex), m_BankNameIndex, GetName());
		}
#endif
		return AUD_PREPARE_FAILED;
	}

	audEnvironmentSound *envSound = (audEnvironmentSound*)GetChildSound(0);
	if (!envSound)
	{
		return AUD_PREPARE_FAILED;
	}
	
#if FAST_WAVESLOT
	audWaveSlot *slotPtr = audWaveSlot::GetWaveSlotFromIndexFast(slotIndex);
#else
	audWaveSlot *slotPtr = audWaveSlot::GetWaveSlotFromIndex(slotIndex);
#endif
	SoundAssert(slotPtr);
	const audWaveSlot::audWaveSlotLoadStatus status = slotPtr->GetAssetLoadingStatus(m_BankNameIndex, m_WaveNameHash);
	if(checkStateOnly)
	{
		switch(status)
		{
		case audWaveSlot::LOADED:
			return AUD_PREPARED;
		case audWaveSlot::FAILED:
			return AUD_PREPARE_FAILED;
		default:
			return AUD_PREPARING;
		}
	}
	else
	{
		Assign(m_SlotIndex, slotIndex);

		switch(status)
		{
			case audWaveSlot::LOADED:		
				{
					SoundAssert(m_SlotIndex>=0);

					if(!SetupWavePlayer(slotPtr))
					{
						return AUD_PREPARE_FAILED;
					}

					return AUD_PREPARED;
				}				
			case audWaveSlot::LOADING:
				return AUD_PREPARING;
			case audWaveSlot::FAILED:
				return AUD_PREPARE_FAILED;
			case audWaveSlot::NOT_REQUESTED:
				if(GetCanDynamicallyLoad())
				{
					bool bSuccess = slotPtr->LoadAsset(m_BankNameIndex, m_WaveNameHash, GetWaveLoadPriority());
					SoundAssert(bSuccess);
					return bSuccess ? AUD_PREPARING : AUD_PREPARE_FAILED;
				}
			// else - fall through to default
			default:
				return AUD_PREPARE_FAILED;
		}	
	}
}


s32 audSimpleSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound* environmentSound = GetEnvironmentSound();
	return environmentSound ? environmentSound->ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping) : 0u;
}

void audSimpleSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	s32 playbackStartOffset = (s32)GetPlaybackStartOffset();

	audSound *envSound = GetChildSound(0);
	SoundAssert(envSound);
	if (envSound)
	{
		envSound->SetStartOffset(playbackStartOffset, false);
		const u32 residualPredelay = GetPredelayRemainingMs(timeInMs);
		Assert(residualPredelay <= kPredelayStartThresholdMs);
		if(residualPredelay)
		{
			const u32 delaySamples = audDriverUtil::ConvertMsToSamples(residualPredelay, kMixerNativeSampleRate);
			audPcmSourceInterface::SetParam(m_WavePlayerSlotId, audWavePlayer::Params::DelaySamples, delaySamples);
		}
		envSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		
		// we serve no further purpose - remove ourselves from the hierarchy if possible
		RemoveMyselfFromHierarchy();
	}
}

bool audSimpleSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound *envSound = GetChildSound(0);
 	if (IsWaitingForChildrenToRelease() && envSound)
	{
		(envSound->_Stop());
	}
	if (!envSound)
	{
		return false;
	}
	
	PcmSourceState state;
	if(audPcmSourceInterface::GetState(state, m_WavePlayerSlotId))
	{
		if(!state.hasStartedPlayback)
		{
			m_CurrentPlayTimeMs = 0;
		}
		else if(state.PlaytimeSamples == ~0U)
		{
			m_CurrentPlayTimeMs = ~0U;
		}
		else
		{
			m_CurrentPlayTimeMs = audDriverUtil::ConvertSamplesToMs(state.PlaytimeSamples, state.AuthoredSampleRate);
		}

		m_PlayTimeCalculationFrame = state.UpdateMixerFrame;
	}
	
	return	envSound->_ManagedAudioUpdate(timeInMs, combineBuffer);
}

void audSimpleSound::AudioKill()
{
	audSound *envSound = GetChildSound(0);
	if (envSound)
	{
		envSound->_ManagedAudioKill();
	}
}

bool audSimpleSound::SetupWavePlayer(audWaveSlot *slot)
{
	if(!m_WaveReference.Init(slot, m_WaveNameHash))
	{
#if __SPU
		audWarningf("Failed to find wave %u %u for simple sound %s", m_BankNameIndex, m_WaveNameHash, GetName());
#else
		audWarningf("Failed to find wave %s %u for simple sound %s", audWaveSlot::GetBankName(m_BankNameIndex), m_WaveNameHash, GetName());
#endif
		return false;
	}

	// allocate a wave player
	if(m_WavePlayerSlotId == -1)
	{
		Assign(m_WavePlayerSlotId, audPcmSourceInterface::Allocate(AUD_PCMSOURCE_WAVEPLAYER));
		//SoundAssert(m_WavePlayerSlotId != -1);
		if(m_WavePlayerSlotId == -1)
		{
			// failed to allocate a wave player
			return false;
		}
	}
	// Add an extra wave reference for the wave player.  Adding it here (synchronously) ensures that even if the simple
	// sound is destroyed before the wave player is initialised, the slot will remain referenced until the wave player
	// shuts down.
	audWaveSlot::AddSlotReference(m_WaveReference.GetSlotId());
	if(!audPcmSourceInterface::SetParam(m_WavePlayerSlotId, audWavePlayer::Params::WaveReference, m_WaveReference.GetAsU32()))
	{
		return false;
	}

	// We need to cache Wave length info, since although it is available through the PCM generator
	// state there will be a delay before that is updated
	u32 formatChunkSize = 0;
	const audWaveFormat *format = m_WaveReference.FindFormat(formatChunkSize);
	SoundAssert(format);
	if(format)
	{	
		m_WaveLengthMs = audDriverUtil::ConvertSamplesToMs(format->LengthSamples, format->SampleRate);
		m_IsLooping = format->LoopPointSamples != -1;

		const u16 firstPeakLevel = AUD_READ_FIRST_PEAK_SAMPLE(format, formatChunkSize);
		const f32 headroom = format->Headroom * 0.01f;
		GetEnvironmentSound()->InitEnvironment(m_WavePlayerSlotId, headroom, (s32)m_WaveLengthMs, m_IsLooping, firstPeakLevel);
		return true;
	}
	return false;
}

#if !__SPU
void audSimpleSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 UNUSED_PARAM(index))
{
	if(!SetupWavePlayer(audWaveSlot::GetWaveSlotFromIndex(m_SlotIndex)))
	{
		// Kill our environment sound, which will also tell the wave player to Stop
		DeleteChild(0);
	}
}
#endif

} // namespace rage
