// 
// sounddefs.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure functions - do not edit.
// 

#include "sounddefs.h"
#include "string/stringhash.h"

namespace rage
{
	// 
	// Sound
	// 
	void* Sound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1762878194U: return &Volume;
			case 1627554643U: return &VolumeVariance;
			case 1061927116U: return &Pitch;
			case 4149052553U: return &PitchVariance;
			case 2063470374U: return &Pan;
			case 3944586171U: return &PanVariance;
			case 1610825783U: return &preDelay;
			case 2805117913U: return &preDelayVariance;
			case 1754157472U: return &StartOffset;
			case 1922762202U: return &StartOffsetVariance;
			case 4027599505U: return &AttackTime;
			case 424159857U: return &ReleaseTime;
			case 1796469895U: return &DopplerFactor;
			case 2052871693U: return &Category;
			case 248793657U: return &LPFCutoff;
			case 209131976U: return &LPFCutoffVariance;
			case 1700098281U: return &HPFCutoff;
			case 2985320547U: return &HPFCutoffVariance;
			case 1715499322U: return &VolumeCurve;
			case 1390626977U: return &VolumeCurveScale;
			case 1397127271U: return &VolumeCurvePlateau;
			case 2843859466U: return &SpeakerMask;
			case 3697023157U: return &EffectRoute;
			case 3705621960U: return &PreDelayVariable;
			case 2824295079U: return &StartOffsetVariable;
			case 2010550034U: return &SmallReverbSend;
			case 4126590696U: return &MediumReverbSend;
			case 1958040013U: return &LargeReverbSend;
			default: return NULL;
		}
	}
	
	// 
	// LoopingSound
	// 
	void* LoopingSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3076944391U: return &LoopCount;
			case 1792075919U: return &LoopCountVariance;
			case 2026583962U: return &LoopPoint;
			case 1245614956U: return &SoundRef;
			case 2642819994U: return &LoopCountVariable;
			default: return NULL;
		}
	}
	
	// 
	// EnvelopeSound
	// 
	void* EnvelopeSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3372951053U: return &Envelope;
			case 2452420412U: return &AttackVariable;
			case 3373481664U: return &DecayVariable;
			case 3949949531U: return &SustainVariable;
			case 2801932598U: return &HoldVariable;
			case 136647715U: return &ReleaseVariable;
			case 1245614956U: return &SoundRef;
			case 1746230403U: return &Mode;
			case 1941482115U: return &padding0;
			case 3180641850U: return &padding1;
			case 4006020030U: return &OutputVariable;
			case 2897048306U: return &OutputRange;
			default: return NULL;
		}
	}
	
	// 
	// TwinLoopSound
	// 
	void* TwinLoopSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1542143162U: return &MinSwapTime;
			case 3899532379U: return &MaxSwapTime;
			case 3237329787U: return &MinCrossfadeTime;
			case 3132529897U: return &MaxCrossfadeTime;
			case 3055626598U: return &CrossfadeCurve;
			case 3573728257U: return &MinSwapTimeVariable;
			case 1571724261U: return &MaxSwapTimeVariable;
			case 2389799088U: return &MinCrossfadeTimeVariable;
			case 1595436026U: return &MaxCrossfadeTimeVariable;
			case 1245614956U: return &SoundRef;
			default: return NULL;
		}
	}
	
	// 
	// SpeechSound
	// 
	void* SpeechSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1474688573U: return &LastVariation;
			case 3302149841U: return &DynamicFieldName;
			case 3964159216U: return &VoiceName;
			case 2826094965U: return &ContextName;
			default: return NULL;
		}
	}
	
	// 
	// OnStopSound
	// 
	void* OnStopSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 679666594U: return &ChildSoundRef;
			case 293392571U: return &StopSoundRef;
			case 2007213383U: return &FinishedSoundRef;
			default: return NULL;
		}
	}
	
	// 
	// WrapperSound
	// 
	void* WrapperSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 1898878336U: return &LastPlayTime;
			case 1712646659U: return &FallbackSoundRef;
			case 1990439652U: return &MinRepeatTime;
			case 242159986U: return &VariableRefs;
			default: return NULL;
		}
	}
	
	// 
	// SequentialSound
	// 
	void* SequentialSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			default: return NULL;
		}
	}
	
	// 
	// StreamingSound
	// 
	void* StreamingSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3674968789U: return &Duration;
			case 1245614956U: return &SoundRef;
			default: return NULL;
		}
	}
	
	// 
	// RetriggeredOverlappedSound
	// 
	void* RetriggeredOverlappedSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3076944391U: return &LoopCount;
			case 1792075919U: return &LoopCountVariance;
			case 3657885768U: return &DelayTime;
			case 2698009658U: return &DelayTimeVariance;
			case 2642819994U: return &LoopCountVariable;
			case 2948081777U: return &DelayTimeVariable;
			case 2315441462U: return &StartSound;
			case 1245614956U: return &SoundRef;
			case 560604366U: return &StopSound;
			default: return NULL;
		}
	}
	
	// 
	// CrossfadeSound
	// 
	void* CrossfadeSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2515753127U: return &NearSoundRef;
			case 964665914U: return &FarSoundRef;
			case 1746230403U: return &Mode;
			case 3953290452U: return &MinDistance;
			case 1153964379U: return &MaxDistance;
			case 4095720642U: return &Hysteresis;
			case 3055626598U: return &CrossfadeCurve;
			case 4076449226U: return &DistanceVariable;
			case 3550072902U: return &MinDistanceVariable;
			case 302964708U: return &MaxDistanceVariable;
			case 1695081916U: return &HysteresisVariable;
			case 643563145U: return &CrossfadeVariable;
			default: return NULL;
		}
	}
	
	// 
	// CollapsingStereoSound
	// 
	void* CollapsingStereoSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2873750049U: return &LeftSoundRef;
			case 1518162910U: return &RightSoundRef;
			case 3953290452U: return &MinDistance;
			case 1153964379U: return &MaxDistance;
			case 3550072902U: return &MinDistanceVariable;
			case 302964708U: return &MaxDistanceVariable;
			case 724940803U: return &CrossfadeOverrideVariable;
			case 220383540U: return &FrontendLeftPan;
			case 2428459334U: return &FrontendRightPan;
			case 2478422810U: return &PositionRelativePanDamping;
			case 3258116414U: return &PositionRelativePanDampingVariable;
			case 1746230403U: return &Mode;
			default: return NULL;
		}
	}
	
	// 
	// SimpleSound
	// 
	void* SimpleSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2706480107U: return &WaveRef;
			case 3628942244U: return &WaveSlotIndex;
			default: return NULL;
		}
	}
	
	// 
	// MultitrackSound
	// 
	void* MultitrackSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			default: return NULL;
		}
	}
	
	// 
	// RandomizedSound
	// 
	void* RandomizedSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3153897539U: return &HistoryIndex;
			case 1481830564U: return &HistorySpace;
			case 1773442490U: return &Variations;
			default: return NULL;
		}
	}
	
	// 
	// EnvironmentSound
	// 
	void* EnvironmentSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 85245374U: return &ChannelId;
			default: return NULL;
		}
	}
	
	// 
	// DynamicEntitySound
	// 
	void* DynamicEntitySound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1482944544U: return &ObjectRefs;
			default: return NULL;
		}
	}
	
	// 
	// SequentialOverlapSound
	// 
	void* SequentialOverlapSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3657885768U: return &DelayTime;
			case 2948081777U: return &DelayTimeVariable;
			case 3457170126U: return &SequenceDirection;
			case 3775186204U: return &ChildSound;
			default: return NULL;
		}
	}
	
	// 
	// ModularSynthSound
	// 
	void* ModularSynthSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2630927315U: return &SynthDef;
			case 2973707311U: return &Preset;
			case 3785294931U: return &PlayBackTimeLimit;
			case 1775984213U: return &VirtualisationMode;
			case 489324196U: return &pad1;
			case 3941112353U: return &pad0;
			case 304387976U: return &EnvironmentSound;
			case 1908844139U: return &ExposedVariable;
			default: return NULL;
		}
	}
	
	// 
	// GranularSound
	// 
	void* GranularSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2033773052U: return &WaveSlotPtr;
			case 1794079354U: return &Channel0;
			case 1370245108U: return &Channel1;
			case 3099563525U: return &Channel2;
			case 3875369600U: return &Channel3;
			case 608693524U: return &Channel4;
			case 311577001U: return &Channel5;
			case 2051009272U: return &ChannelSettings0;
			case 1884804904U: return &ChannelSettings1;
			case 4104445888U: return &ChannelSettings2;
			case 3800054647U: return &ChannelSettings3;
			case 918938629U: return &ChannelSettings4;
			case 628244830U: return &ChannelSettings5;
			case 1731978031U: return &LoopRandomisationChangeRate;
			case 3827484199U: return &LoopRandomisationPitchFraction;
			case 3812444708U: return &Channel0_Volume;
			case 3424218409U: return &Channel1_Volume;
			case 1049992525U: return &Channel2_Volume;
			case 4112852535U: return &Channel3_Volume;
			case 3871537690U: return &Channel4_Volume;
			case 2265552436U: return &Channel5_Volume;
			case 275073196U: return &ParentSound;
			case 1071299901U: return &GranularClock;
			default: return NULL;
		}
	}
	
	// 
	// DirectionalSound
	// 
	void* DirectionalSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 3055736763U: return &InnerAngle;
			case 3720058240U: return &OuterAngle;
			case 2575646875U: return &RearAttenuation;
			case 366871217U: return &YawAngle;
			case 2157585005U: return &PitchAngle;
			default: return NULL;
		}
	}
	
	// 
	// KineticSound
	// 
	void* KineticSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 1271239883U: return &Mass;
			case 366871217U: return &YawAngle;
			case 2157585005U: return &PitchAngle;
			default: return NULL;
		}
	}
	
	// 
	// SwitchSound
	// 
	void* SwitchSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2728961826U: return &Variable;
			case 1245614956U: return &SoundRef;
			default: return NULL;
		}
	}
	
	// 
	// VariableCurveSound
	// 
	void* VariableCurveSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 3217495625U: return &InputVariable;
			case 4006020030U: return &OutputVariable;
			case 257035089U: return &Curve;
			default: return NULL;
		}
	}
	
	// 
	// VariablePrintValueSound
	// 
	void* VariablePrintValueSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2728961826U: return &Variable;
			case 2319945591U: return &Message;
			default: return NULL;
		}
	}
	
	// 
	// VariableBlockSound
	// 
	void* VariableBlockSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 2728961826U: return &Variable;
			default: return NULL;
		}
	}
	
	// 
	// IfSound
	// 
	void* IfSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 3502413237U: return &TrueSoundRef;
			case 2356385304U: return &FalseSoundRef;
			case 1864121129U: return &LeftVariable;
			case 79290053U: return &ConditionType;
			case 1215632803U: return &RHS;
			default: return NULL;
		}
	}
	
	// 
	// MathOperationSound
	// 
	void* MathOperationSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 2561733933U: return &Operation;
			default: return NULL;
		}
	}
	
	// 
	// ParameterTransformSound
	// 
	void* ParameterTransformSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 1981294945U: return &ParameterTransform;
			default: return NULL;
		}
	}
	
	// 
	// FluctuatorSound
	// 
	void* FluctuatorSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 4190957331U: return &Fluctuator;
			default: return NULL;
		}
	}
	
	// 
	// AutomationSound
	// 
	void* AutomationSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 1245614956U: return &SoundRef;
			case 2905281742U: return &PlaybackRate;
			case 1288275196U: return &PlaybackRateVariance;
			case 1722266857U: return &PlaybackRateVariable;
			case 320501252U: return &NoteMap;
			case 2182424095U: return &DataRef;
			case 2171254422U: return &VariableOutputs;
			default: return NULL;
		}
	}
	
	// 
	// ExternalStreamSound
	// 
	void* ExternalStreamSound::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 304387976U: return &EnvironmentSound;
			default: return NULL;
		}
	}
	
	// 
	// SoundSet
	// 
	void* SoundSet::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2499414043U: return &Sounds;
			default: return NULL;
		}
	}
	
	// 
	// NoteMap
	// 
	void* NoteMap::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 332484516U: return &Range;
			default: return NULL;
		}
	}
	
	// 
	// SoundSetList
	// 
	void* SoundSetList::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 4048745621U: return &SoundSet;
			default: return NULL;
		}
	}
	
	// 
	// SoundHashList
	// 
	void* SoundHashList::GetFieldPtr(const rage::u32 fieldNameHash)
	{
		switch(fieldNameHash)
		{
			case 2023143708U: return &CurrentSoundIdx;
			case 438785695U: return &SoundHashes;
			default: return NULL;
		}
	}
	
	// 
	// Enumeration Conversion
	// 
	
	// PURPOSE - Convert an EffectRoutes value into its string representation.
	const char* EffectRoutes_ToString(const EffectRoutes value)
	{
		switch(value)
		{
			case EFFECT_ROUTE_AS_PARENT: return "EFFECT_ROUTE_AS_PARENT";
			case EFFECT_ROUTE_MUSIC: return "EFFECT_ROUTE_MUSIC";
			case EFFECT_ROUTE_FRONT_END: return "EFFECT_ROUTE_FRONT_END";
			case EFFECT_ROUTE_POSITIONED: return "EFFECT_ROUTE_POSITIONED";
			case EFFECT_ROUTE_SML_REVERB_FULL_WET: return "EFFECT_ROUTE_SML_REVERB_FULL_WET";
			case EFFECT_ROUTE_MED_REVERB_FULL_WET: return "EFFECT_ROUTE_MED_REVERB_FULL_WET";
			case EFFECT_ROUTE_LRG_REVERB_FULL_WET: return "EFFECT_ROUTE_LRG_REVERB_FULL_WET";
			case EFFECT_ROUTE_SFX: return "EFFECT_ROUTE_SFX";
			case EFFECT_ROUTE_MASTER: return "EFFECT_ROUTE_MASTER";
			case EFFECT_ROUTE_PADSPEAKER_1: return "EFFECT_ROUTE_PADSPEAKER_1";
			case EFFECT_ROUTE_POSITIONED_MUSIC: return "EFFECT_ROUTE_POSITIONED_MUSIC";
			case EFFECT_ROUTE_PADHAPTICS_1: return "EFFECT_ROUTE_PADHAPTICS_1";
			case NUM_EFFECTROUTES: return "NUM_EFFECTROUTES";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into an EffectRoutes value.
	EffectRoutes EffectRoutes_Parse(const char* str, const EffectRoutes defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 2669543874U: return EFFECT_ROUTE_AS_PARENT;
			case 3458820295U: return EFFECT_ROUTE_MUSIC;
			case 3311224844U: return EFFECT_ROUTE_FRONT_END;
			case 2844349546U: return EFFECT_ROUTE_POSITIONED;
			case 891734689U: return EFFECT_ROUTE_SML_REVERB_FULL_WET;
			case 443638764U: return EFFECT_ROUTE_MED_REVERB_FULL_WET;
			case 2421039844U: return EFFECT_ROUTE_LRG_REVERB_FULL_WET;
			case 2128341852U: return EFFECT_ROUTE_SFX;
			case 2702231680U: return EFFECT_ROUTE_MASTER;
			case 1598597266U: return EFFECT_ROUTE_PADSPEAKER_1;
			case 189467648U: return EFFECT_ROUTE_POSITIONED_MUSIC;
			case 3551024448U: return EFFECT_ROUTE_PADHAPTICS_1;
			case 2119050431U:
			case 2915338026U: return NUM_EFFECTROUTES;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a ParameterDestinations value into its string representation.
	const char* ParameterDestinations_ToString(const ParameterDestinations value)
	{
		switch(value)
		{
			case PARAM_DESTINATION_VOLUME: return "PARAM_DESTINATION_VOLUME";
			case PARAM_DESTINATION_PITCH: return "PARAM_DESTINATION_PITCH";
			case PARAM_DESTINATION_PAN: return "PARAM_DESTINATION_PAN";
			case PARAM_DESTINATION_STARTOFFSET: return "PARAM_DESTINATION_STARTOFFSET";
			case PARAM_DESTINATION_PREDELAY: return "PARAM_DESTINATION_PREDELAY";
			case PARAM_DESTINATION_LPF: return "PARAM_DESTINATION_LPF";
			case PARAM_DESTINATION_HPF: return "PARAM_DESTINATION_HPF";
			case PARAM_DESTINATION_VARIABLE: return "PARAM_DESTINATION_VARIABLE";
			case PARAM_DESTINATION_ROLLOFF: return "PARAM_DESTINATION_ROLLOFF";
			case PARAM_DESTINATION_ELEVATIONPAN: return "PARAM_DESTINATION_ELEVATIONPAN";
			case PARAM_DESTINATION_HAPTICSENDLEVEL: return "PARAM_DESTINATION_HAPTICSENDLEVEL";
			case NUM_PARAMETERDESTINATIONS: return "NUM_PARAMETERDESTINATIONS";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a ParameterDestinations value.
	ParameterDestinations ParameterDestinations_Parse(const char* str, const ParameterDestinations defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 2598646991U: return PARAM_DESTINATION_VOLUME;
			case 4279262834U: return PARAM_DESTINATION_PITCH;
			case 2003464980U: return PARAM_DESTINATION_PAN;
			case 3577759420U: return PARAM_DESTINATION_STARTOFFSET;
			case 2714116189U: return PARAM_DESTINATION_PREDELAY;
			case 2650395438U: return PARAM_DESTINATION_LPF;
			case 1616203739U: return PARAM_DESTINATION_HPF;
			case 4209931355U: return PARAM_DESTINATION_VARIABLE;
			case 2976028789U: return PARAM_DESTINATION_ROLLOFF;
			case 2906658465U: return PARAM_DESTINATION_ELEVATIONPAN;
			case 107050502U: return PARAM_DESTINATION_HAPTICSENDLEVEL;
			case 3881388842U:
			case 1910496793U: return NUM_PARAMETERDESTINATIONS;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert an EnvelopeSoundMode value into its string representation.
	const char* EnvelopeSoundMode_ToString(const EnvelopeSoundMode value)
	{
		switch(value)
		{
			case kEnvelopeSoundVolume: return "kEnvelopeSoundVolume";
			case kEnvelopeSoundPitch: return "kEnvelopeSoundPitch";
			case kEnvelopeSoundPan: return "kEnvelopeSoundPan";
			case kEnvelopeSoundLPF: return "kEnvelopeSoundLPF";
			case kEnvelopeSoundHPF: return "kEnvelopeSoundHPF";
			case kEnvelopeSoundVariable: return "kEnvelopeSoundVariable";
			case NUM_ENVELOPESOUNDMODE: return "NUM_ENVELOPESOUNDMODE";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into an EnvelopeSoundMode value.
	EnvelopeSoundMode EnvelopeSoundMode_Parse(const char* str, const EnvelopeSoundMode defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 3681441611U: return kEnvelopeSoundVolume;
			case 1185141890U: return kEnvelopeSoundPitch;
			case 3172205392U: return kEnvelopeSoundPan;
			case 947130540U: return kEnvelopeSoundLPF;
			case 2488756297U: return kEnvelopeSoundHPF;
			case 318265568U: return kEnvelopeSoundVariable;
			case 2331394829U:
			case 1154944102U: return NUM_ENVELOPESOUNDMODE;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a CrossfadeMode value into its string representation.
	const char* CrossfadeMode_ToString(const CrossfadeMode value)
	{
		switch(value)
		{
			case CROSSFADE_MODE_FIXED: return "CROSSFADE_MODE_FIXED";
			case CROSSFADE_MODE_BOTH: return "CROSSFADE_MODE_BOTH";
			case CROSSFADE_MODE_RETRIGGER: return "CROSSFADE_MODE_RETRIGGER";
			case CROSSFADE_MODE_POSITION_RELATIVE_PAN: return "CROSSFADE_MODE_POSITION_RELATIVE_PAN";
			case NUM_CROSSFADEMODE: return "NUM_CROSSFADEMODE";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a CrossfadeMode value.
	CrossfadeMode CrossfadeMode_Parse(const char* str, const CrossfadeMode defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 2539659852U: return CROSSFADE_MODE_FIXED;
			case 612829038U: return CROSSFADE_MODE_BOTH;
			case 2210398167U: return CROSSFADE_MODE_RETRIGGER;
			case 3679300111U: return CROSSFADE_MODE_POSITION_RELATIVE_PAN;
			case 1709479200U:
			case 1217264781U: return NUM_CROSSFADEMODE;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a VirtualisationBehaviour value into its string representation.
	const char* VirtualisationBehaviour_ToString(const VirtualisationBehaviour value)
	{
		switch(value)
		{
			case kVirtualisationNormal: return "kVirtualisationNormal";
			case kVirtualisationStop: return "kVirtualisationStop";
			case kVirtualisationUcancellable: return "kVirtualisationUcancellable";
			case NUM_VIRTUALISATIONBEHAVIOUR: return "NUM_VIRTUALISATIONBEHAVIOUR";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a VirtualisationBehaviour value.
	VirtualisationBehaviour VirtualisationBehaviour_Parse(const char* str, const VirtualisationBehaviour defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 84012549U: return kVirtualisationNormal;
			case 1235629483U: return kVirtualisationStop;
			case 3449959757U: return kVirtualisationUcancellable;
			case 2934803889U:
			case 3054698069U: return NUM_VIRTUALISATIONBEHAVIOUR;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a GranularPitchCalcMode value into its string representation.
	const char* GranularPitchCalcMode_ToString(const GranularPitchCalcMode value)
	{
		switch(value)
		{
			case kGranularPitchMapChannel0: return "kGranularPitchMapChannel0";
			case kGranularPitchMapChannel1: return "kGranularPitchMapChannel1";
			case kGranularPitchMapChannel2: return "kGranularPitchMapChannel2";
			case kGranularPitchMapChannel3: return "kGranularPitchMapChannel3";
			case kGranularPitchMapChannel4: return "kGranularPitchMapChannel4";
			case kGranularPitchMapChannel5: return "kGranularPitchMapChannel5";
			case kGranularPitchMapClamp: return "kGranularPitchMapClamp";
			case kGranularPitchMapDirectionalClamp: return "kGranularPitchMapDirectionalClamp";
			case kGranularPitchMapDirectionalMinMax: return "kGranularPitchMapDirectionalMinMax";
			case kGranularPitchMapAverage: return "kGranularPitchMapAverage";
			case NUM_GRANULARPITCHCALCMODE: return "NUM_GRANULARPITCHCALCMODE";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a GranularPitchCalcMode value.
	GranularPitchCalcMode GranularPitchCalcMode_Parse(const char* str, const GranularPitchCalcMode defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 3373748922U: return kGranularPitchMapChannel0;
			case 3647337303U: return kGranularPitchMapChannel1;
			case 3949500252U: return kGranularPitchMapChannel2;
			case 78760430U: return kGranularPitchMapChannel3;
			case 366701633U: return kGranularPitchMapChannel4;
			case 3867708820U: return kGranularPitchMapChannel5;
			case 1208683601U: return kGranularPitchMapClamp;
			case 1481446853U: return kGranularPitchMapDirectionalClamp;
			case 1522853857U: return kGranularPitchMapDirectionalMinMax;
			case 2216900494U: return kGranularPitchMapAverage;
			case 1448326210U:
			case 2224537471U: return NUM_GRANULARPITCHCALCMODE;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a VariableUsage value into its string representation.
	const char* VariableUsage_ToString(const VariableUsage value)
	{
		switch(value)
		{
			case VARIABLE_USAGE_SOUND: return "VARIABLE_USAGE_SOUND";
			case VARIABLE_USAGE_CODE: return "VARIABLE_USAGE_CODE";
			case VARIABLE_USAGE_CONSTANT: return "VARIABLE_USAGE_CONSTANT";
			case NUM_VARIABLEUSAGE: return "NUM_VARIABLEUSAGE";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a VariableUsage value.
	VariableUsage VariableUsage_Parse(const char* str, const VariableUsage defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 21080404U: return VARIABLE_USAGE_SOUND;
			case 2137098167U: return VARIABLE_USAGE_CODE;
			case 2975073045U: return VARIABLE_USAGE_CONSTANT;
			case 4023680314U:
			case 1778298013U: return NUM_VARIABLEUSAGE;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a ConditionTypes value into its string representation.
	const char* ConditionTypes_ToString(const ConditionTypes value)
	{
		switch(value)
		{
			case IF_CONDITION_LESS_THAN: return "IF_CONDITION_LESS_THAN";
			case IF_CONDITION_LESS_THAN_OR_EQUAL_TO: return "IF_CONDITION_LESS_THAN_OR_EQUAL_TO";
			case IF_CONDITION_GREATER_THAN: return "IF_CONDITION_GREATER_THAN";
			case IF_CONDITION_GREATER_THAN_OR_EQUAL_TO: return "IF_CONDITION_GREATER_THAN_OR_EQUAL_TO";
			case IF_CONDITION_EQUAL_TO: return "IF_CONDITION_EQUAL_TO";
			case IF_CONDITION_NOT_EQUAL_TO: return "IF_CONDITION_NOT_EQUAL_TO";
			case NUM_CONDITIONTYPES: return "NUM_CONDITIONTYPES";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a ConditionTypes value.
	ConditionTypes ConditionTypes_Parse(const char* str, const ConditionTypes defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 4049672550U: return IF_CONDITION_LESS_THAN;
			case 3692400506U: return IF_CONDITION_LESS_THAN_OR_EQUAL_TO;
			case 4077908300U: return IF_CONDITION_GREATER_THAN;
			case 3893020818U: return IF_CONDITION_GREATER_THAN_OR_EQUAL_TO;
			case 3513169537U: return IF_CONDITION_EQUAL_TO;
			case 502478792U: return IF_CONDITION_NOT_EQUAL_TO;
			case 1601278497U:
			case 4139253351U: return NUM_CONDITIONTYPES;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a MathOperations value into its string representation.
	const char* MathOperations_ToString(const MathOperations value)
	{
		switch(value)
		{
			case MATH_OPERATION_ADD: return "MATH_OPERATION_ADD";
			case MATH_OPERATION_SUBTRACT: return "MATH_OPERATION_SUBTRACT";
			case MATH_OPERATION_MULTIPLY: return "MATH_OPERATION_MULTIPLY";
			case MATH_OPERATION_DIVIDE: return "MATH_OPERATION_DIVIDE";
			case MATH_OPERATION_SET: return "MATH_OPERATION_SET";
			case MATH_OPERATION_MOD: return "MATH_OPERATION_MOD";
			case MATH_OPERATION_MIN: return "MATH_OPERATION_MIN";
			case MATH_OPERATION_MAX: return "MATH_OPERATION_MAX";
			case MATH_OPERATION_ABS: return "MATH_OPERATION_ABS";
			case MATH_OPERATION_SIGN: return "MATH_OPERATION_SIGN";
			case MATH_OPERATION_FLOOR: return "MATH_OPERATION_FLOOR";
			case MATH_OPERATION_CEIL: return "MATH_OPERATION_CEIL";
			case MATH_OPERATION_RAND: return "MATH_OPERATION_RAND";
			case MATH_OPERATION_SIN: return "MATH_OPERATION_SIN";
			case MATH_OPERATION_COS: return "MATH_OPERATION_COS";
			case MATH_OPERATION_SQRT: return "MATH_OPERATION_SQRT";
			case MATH_OPERATION_DBTOLINEAR: return "MATH_OPERATION_DBTOLINEAR";
			case MATH_OPERATION_LINEARTODB: return "MATH_OPERATION_LINEARTODB";
			case MATH_OPERATION_PITCHTORATIO: return "MATH_OPERATION_PITCHTORATIO";
			case MATH_OPERATION_RATIOTOPITCH: return "MATH_OPERATION_RATIOTOPITCH";
			case MATH_OPERATION_GETTIME: return "MATH_OPERATION_GETTIME";
			case MATH_OPERATION_FSEL: return "MATH_OPERATION_FSEL";
			case MATH_OPERATION_VALUEINRANGE: return "MATH_OPERATION_VALUEINRANGE";
			case MATH_OPERATION_CLAMP: return "MATH_OPERATION_CLAMP";
			case MATH_OPERATION_POW: return "MATH_OPERATION_POW";
			case MATH_OPERATION_ROUND: return "MATH_OPERATION_ROUND";
			case MATH_OPERATION_SCALEDSIN: return "MATH_OPERATION_SCALEDSIN";
			case MATH_OPERATION_SCALEDTRI: return "MATH_OPERATION_SCALEDTRI";
			case MATH_OPERATION_SCALEDSAW: return "MATH_OPERATION_SCALEDSAW";
			case MATH_OPERATION_SCALEDSQUARE: return "MATH_OPERATION_SCALEDSQUARE";
			case MATH_OPERATION_SMOOTH: return "MATH_OPERATION_SMOOTH";
			case MATH_OPERATION_GETSCALEDTIME: return "MATH_OPERATION_GETSCALEDTIME";
			case MATH_OPERATION_CLAMPRANGE: return "MATH_OPERATION_CLAMPRANGE";
			case MATH_OPERATION_GETPOS_X: return "MATH_OPERATION_GETPOS_X";
			case MATH_OPERATION_GETPOS_Y: return "MATH_OPERATION_GETPOS_Y";
			case MATH_OPERATION_GETPOS_Z: return "MATH_OPERATION_GETPOS_Z";
			case MATH_OPERATION_TRACE: return "MATH_OPERATION_TRACE";
			case NUM_MATHOPERATIONS: return "NUM_MATHOPERATIONS";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a MathOperations value.
	MathOperations MathOperations_Parse(const char* str, const MathOperations defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 3238846531U: return MATH_OPERATION_ADD;
			case 2192178464U: return MATH_OPERATION_SUBTRACT;
			case 4142921869U: return MATH_OPERATION_MULTIPLY;
			case 3921469002U: return MATH_OPERATION_DIVIDE;
			case 1750496957U: return MATH_OPERATION_SET;
			case 3406593465U: return MATH_OPERATION_MOD;
			case 421040904U: return MATH_OPERATION_MIN;
			case 533174590U: return MATH_OPERATION_MAX;
			case 2917874768U: return MATH_OPERATION_ABS;
			case 649661794U: return MATH_OPERATION_SIGN;
			case 2022566699U: return MATH_OPERATION_FLOOR;
			case 509680911U: return MATH_OPERATION_CEIL;
			case 3827659110U: return MATH_OPERATION_RAND;
			case 3722012253U: return MATH_OPERATION_SIN;
			case 4205502095U: return MATH_OPERATION_COS;
			case 2433152550U: return MATH_OPERATION_SQRT;
			case 4041060456U: return MATH_OPERATION_DBTOLINEAR;
			case 881280900U: return MATH_OPERATION_LINEARTODB;
			case 596005848U: return MATH_OPERATION_PITCHTORATIO;
			case 1549837U: return MATH_OPERATION_RATIOTOPITCH;
			case 3064355156U: return MATH_OPERATION_GETTIME;
			case 958977005U: return MATH_OPERATION_FSEL;
			case 2302698404U: return MATH_OPERATION_VALUEINRANGE;
			case 296304568U: return MATH_OPERATION_CLAMP;
			case 3482280021U: return MATH_OPERATION_POW;
			case 3306587885U: return MATH_OPERATION_ROUND;
			case 3595373955U: return MATH_OPERATION_SCALEDSIN;
			case 645723382U: return MATH_OPERATION_SCALEDTRI;
			case 2807393712U: return MATH_OPERATION_SCALEDSAW;
			case 2517965487U: return MATH_OPERATION_SCALEDSQUARE;
			case 2006184430U: return MATH_OPERATION_SMOOTH;
			case 1134657343U: return MATH_OPERATION_GETSCALEDTIME;
			case 3469773197U: return MATH_OPERATION_CLAMPRANGE;
			case 4100136170U: return MATH_OPERATION_GETPOS_X;
			case 578615581U: return MATH_OPERATION_GETPOS_Y;
			case 280417681U: return MATH_OPERATION_GETPOS_Z;
			case 2369381937U: return MATH_OPERATION_TRACE;
			case 901975974U:
			case 1702126702U: return NUM_MATHOPERATIONS;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a FluctuatorModes value into its string representation.
	const char* FluctuatorModes_ToString(const FluctuatorModes value)
	{
		switch(value)
		{
			case PROBABILITY_BASED: return "PROBABILITY_BASED";
			case TIME_BASED: return "TIME_BASED";
			case NUM_FLUCTUATORMODES: return "NUM_FLUCTUATORMODES";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a FluctuatorModes value.
	FluctuatorModes FluctuatorModes_Parse(const char* str, const FluctuatorModes defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 4265409893U: return PROBABILITY_BASED;
			case 2869616500U: return TIME_BASED;
			case 940599966U:
			case 1368927766U: return NUM_FLUCTUATORMODES;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Convert a NoteRangeMode value into its string representation.
	const char* NoteRangeMode_ToString(const NoteRangeMode value)
	{
		switch(value)
		{
			case NOTE_RANGE_TRACK_PITCH: return "NOTE_RANGE_TRACK_PITCH";
			case NOTE_RANGE_UNITY_PITCH: return "NOTE_RANGE_UNITY_PITCH";
			case NUM_NOTERANGEMODE: return "NUM_NOTERANGEMODE";
			default: return NULL;
		}
	}
	
	// PURPOSE - Parse a string into a NoteRangeMode value.
	NoteRangeMode NoteRangeMode_Parse(const char* str, const NoteRangeMode defaultValue)
	{
		const rage::u32 hash = atStringHash(str);
		switch(hash)
		{
			case 399654324U: return NOTE_RANGE_TRACK_PITCH;
			case 320300946U: return NOTE_RANGE_UNITY_PITCH;
			case 2984416600U:
			case 2481344421U: return NUM_NOTERANGEMODE;
			default: return defaultValue;
		}
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gSoundsGetBaseTypeId(const rage::u32 classId)
	{
		switch(classId)
		{
			case Sound::TYPE_ID: return Sound::BASE_TYPE_ID;
			case LoopingSound::TYPE_ID: return LoopingSound::BASE_TYPE_ID;
			case EnvelopeSound::TYPE_ID: return EnvelopeSound::BASE_TYPE_ID;
			case TwinLoopSound::TYPE_ID: return TwinLoopSound::BASE_TYPE_ID;
			case SpeechSound::TYPE_ID: return SpeechSound::BASE_TYPE_ID;
			case OnStopSound::TYPE_ID: return OnStopSound::BASE_TYPE_ID;
			case WrapperSound::TYPE_ID: return WrapperSound::BASE_TYPE_ID;
			case SequentialSound::TYPE_ID: return SequentialSound::BASE_TYPE_ID;
			case StreamingSound::TYPE_ID: return StreamingSound::BASE_TYPE_ID;
			case RetriggeredOverlappedSound::TYPE_ID: return RetriggeredOverlappedSound::BASE_TYPE_ID;
			case CrossfadeSound::TYPE_ID: return CrossfadeSound::BASE_TYPE_ID;
			case CollapsingStereoSound::TYPE_ID: return CollapsingStereoSound::BASE_TYPE_ID;
			case SimpleSound::TYPE_ID: return SimpleSound::BASE_TYPE_ID;
			case MultitrackSound::TYPE_ID: return MultitrackSound::BASE_TYPE_ID;
			case RandomizedSound::TYPE_ID: return RandomizedSound::BASE_TYPE_ID;
			case EnvironmentSound::TYPE_ID: return EnvironmentSound::BASE_TYPE_ID;
			case DynamicEntitySound::TYPE_ID: return DynamicEntitySound::BASE_TYPE_ID;
			case SequentialOverlapSound::TYPE_ID: return SequentialOverlapSound::BASE_TYPE_ID;
			case ModularSynthSound::TYPE_ID: return ModularSynthSound::BASE_TYPE_ID;
			case GranularSound::TYPE_ID: return GranularSound::BASE_TYPE_ID;
			case DirectionalSound::TYPE_ID: return DirectionalSound::BASE_TYPE_ID;
			case KineticSound::TYPE_ID: return KineticSound::BASE_TYPE_ID;
			case SwitchSound::TYPE_ID: return SwitchSound::BASE_TYPE_ID;
			case VariableCurveSound::TYPE_ID: return VariableCurveSound::BASE_TYPE_ID;
			case VariablePrintValueSound::TYPE_ID: return VariablePrintValueSound::BASE_TYPE_ID;
			case VariableBlockSound::TYPE_ID: return VariableBlockSound::BASE_TYPE_ID;
			case IfSound::TYPE_ID: return IfSound::BASE_TYPE_ID;
			case MathOperationSound::TYPE_ID: return MathOperationSound::BASE_TYPE_ID;
			case ParameterTransformSound::TYPE_ID: return ParameterTransformSound::BASE_TYPE_ID;
			case FluctuatorSound::TYPE_ID: return FluctuatorSound::BASE_TYPE_ID;
			case AutomationSound::TYPE_ID: return AutomationSound::BASE_TYPE_ID;
			case ExternalStreamSound::TYPE_ID: return ExternalStreamSound::BASE_TYPE_ID;
			case SoundSet::TYPE_ID: return SoundSet::BASE_TYPE_ID;
			case NoteMap::TYPE_ID: return NoteMap::BASE_TYPE_ID;
			case SoundSetList::TYPE_ID: return SoundSetList::BASE_TYPE_ID;
			case SoundHashList::TYPE_ID: return SoundHashList::BASE_TYPE_ID;
			default: return 0xFFFFFFFF;
		}
	}
	
	// PURPOSE - Determines if a type inherits from another type
	bool gSoundsIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId)
	{
		if(objectTypeId == 0xFFFFFFFF)
		{
			return false;
		}
		else if(objectTypeId == baseTypeId)
		{
			return true;
		}
		else
		{
			return gSoundsIsOfType(gSoundsGetBaseTypeId(objectTypeId), baseTypeId);
		}
	}
} // namespace rage
