// 
// audiosoundtypes/parametertransformsound.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_PARAMETERTRANSFORMSOUND_H
#define AUD_PARAMETERTRANSFORMSOUND_H

#include "audiosoundtypes/sound.h"
#include "audiosoundtypes/sounddefs.h"

namespace rage 
{

	typedef ParameterTransformSound::tParameterTransform::tOutput::tTransformPoints PointType;

	// PURPOSE
	//  Passes a variable through a curve, storing the output in another variable
	class audParameterTransformSound : public audSound
	{
	public:
		
		~audParameterTransformSound();

		AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
		audParameterTransformSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();

		// PURPOSE
		//  Implements base class function for this sound.
		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

	private:

		void ReleaseSlots();
		bool InitSlots(const ParameterTransformSound *metadata);

		struct ParameterTransform
		{
			enum {kMaxTransformOutputs = 4};
			audVariableHandle InputVariable;
			float MinInput;
			float MaxInput;
			struct ParameterTransformOutput
			{
				audVariableHandle OutputVariable;			
				float MinOutput;
				float MaxOutput;
				float InputSmoothRate;

				float LastInputValue;
				float LastOutputValue;
				

				u16 FirstPointIndex;
				u8 NumPoints;
				u8 Destination;
			}Outputs[kMaxTransformOutputs];
		};

		bool CopyPoints(const PointType *src, const u32 startIndex, const u32 numPoints);
		void ProcessTransforms(const float timeStep, const bool isFirstTime, audSoundCombineBuffer &combineBuffer, s32 *predelay = NULL, s32 *startOffset = NULL);
		float ComputeTransform(const float inputValue, const u32 outputIndex, const ParameterTransform &transform) const;
		float ComputeCurve(const float x, const u32 firstPointIndex, const u32 numPoints) const;

		const PointType *GetPointsSlot(const u32 index) const;
		ParameterTransform *GetTransformSlot(const u32 index);

		u32 m_LastUpdateTime;

		enum {kMaxTransformSlotIds = 4};
		atRangeArray<u8,kMaxTransformSlotIds> m_TransformSlotIds;
		
		enum {kMaxPointSlotIds = 6};
		atRangeArray<u8,kMaxPointSlotIds> m_PointSlotIds;

		u8 m_NumTransforms;

	};

} // namespace rage

#endif // AUD_PARAMETERTRANSFORMSOUND_H
