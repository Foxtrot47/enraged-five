//
// audioengine/collapsingsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_COLLAPSINGSTEREOSOUND_H
#define AUD_COLLAPSINGSTEREOSOUND_H

#include "sound.h"

#include "soundDefs.h"

namespace rage {


// PURPOSE
//  A collapsing stereo sound plays it's children frontend when close, and collapses them to mono positioned at distance.
class audCollapsingStereoSound : public audSound
{
public:
	AUD_DECLARE_STATIC_WRAPPERS;

	~audCollapsingStereoSound();

#if !__SPU
	audCollapsingStereoSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

	f32 SetAttenuationFactors(bool firstCall);

private:
	// PURPOSE
	//  We don't want to be searching up the tree for variables every frame. Instead, we cache
	//  ptrs to them at Init, and then go straight there from then on. 
	f32 m_MinDistance, m_MaxDistance;
	audVariableHandle m_MinDistanceVariable;
	audVariableHandle m_MaxDistanceVariable;
	audVariableHandle m_CrossfadeOverrideVariable;
	audVariableHandle m_FrontendPanLVariable, m_FrontendPanRVariable;
	f32 m_PositionRelativePanDamping;
	audVariableHandle m_PositionRelativePanDampingVariable;

	float m_FrontendChildVolume;
	float m_PositionedChildVolume;
	s16 m_PanL;
	s16 m_PanR;	
	u8 m_Mode;

};

} // namespace rage

#endif // AUD_COLLAPSINGSTEREOSOUND_H
