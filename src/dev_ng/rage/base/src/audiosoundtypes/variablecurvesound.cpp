// 
// audiosoundtypes/variablecurvesound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "variablecurvesound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audVariableCurveSound);

audVariableCurveSound::~audVariableCurveSound()
{
	if(GetChildSound(0))
	{
		DeleteChild(0);
	}
}

#if !__SPU
bool audVariableCurveSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	VariableCurveSound *variableCurveSoundData = (VariableCurveSound*)GetMetadata();

	SetChildSound(0,SOUNDFACTORY.GetChildInstance(variableCurveSoundData->SoundRef, this, initParams, scratchInitParams, false));

	m_Curve.Init(variableCurveSoundData->Curve);
	SoundAssert(m_Curve.IsValidInSoundTask());
	if(!m_Curve.IsValidInSoundTask())
	{
		return false;
	}

	m_InputVariable = _FindVariableUpHierarchy(variableCurveSoundData->InputVariable);
	m_OutputVariable = _FindVariableUpHierarchy(variableCurveSoundData->OutputVariable);
	
	if (!m_OutputVariable || !m_InputVariable)
	{
		// We don't have an input/output variable, so do nothing and quit out
		SoundAssert(0);
		return false;
	}
	return true;
}
#endif


audPrepareState audVariableCurveSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	if(GetChildSound(0))
	{
		GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		return GetChildSound(0)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
	}
	else
	{
		return AUD_PREPARED;
	}
}

void audVariableCurveSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	ApplyCurve();

	audSound *childSound = GetChildSound(0);
	if (childSound)
	{
		childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		// Ripple down start offset, as we may have an audible sound below us that makes real sense of it.
		childSound->SetStartOffset((s32)GetPlaybackStartOffset());
		childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
	}
}

bool audVariableCurveSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	// If we have a child sound, we update that, and mirror its play state - if not, we report that we're finished.
	// (and DON'T apply our curve again - which is debatable, but fits with the way other variable sounds currently
	//  work, and is probably best.)
	if (GetChildSound(0))
	{
		ApplyCurve();
		return GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
	}
	else
	{
		return false;
	}
}

void audVariableCurveSound::ApplyCurve()
{
	AUD_SET_VARIABLE(m_OutputVariable, m_Curve.CalculateValue(AUD_GET_VARIABLE(m_InputVariable)));
	return;
}

void audVariableCurveSound::AudioKill()
{
	if(GetChildSound(0))
	{
		GetChildSound(0)->_ManagedAudioKill();
	}
}

s32 audVariableCurveSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	if(GetChildSound(0))
	{
		return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
	}
	else
	{
		// We don't support lengths and start offets on logical sounds with no children
		return kSoundLengthUnknown;
	}
}

} // namespace rage
