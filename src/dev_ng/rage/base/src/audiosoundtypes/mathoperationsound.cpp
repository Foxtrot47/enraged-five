// 
// audiosoundtypes/mathoperationsound.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "mathoperationsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "profile/profiler.h"

namespace rage {

PF_PAGE(MathOpSound,"audMathOperationSound");
PF_GROUP(MathOpSound);
PF_LINK(MathOpSound, MathOpSound);
PF_VALUE_FLOAT(Trace1, MathOpSound);
PF_VALUE_FLOAT(Trace2, MathOpSound);
PF_VALUE_FLOAT(Trace3, MathOpSound);

#define IMPLEMENT_OPERATION(enumVal, opFunc) case enumVal:	\
		result = opFunc(p1, p2, p3); \
		break; 
	
AUD_IMPLEMENT_STATIC_WRAPPERS(audMathOperationSound);

audMathOperationSound::~audMathOperationSound()
{
	if(!IsBeingRemovedFromHierarchy())
	{
		if(GetChildSound(0))
		{
			DeleteChild(0);
		}
	}

	if(m_SlotIndexForResolvedOperations != 0xff)
	{
		sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_SlotIndexForResolvedOperations), m_InitParams.BucketId);
		m_SlotIndexForResolvedOperations = 0xff;
	}

#if __BANK
	if(m_CacheSlotIndex != 0xff)
	{
		sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_CacheSlotIndex), m_InitParams.BucketId);
		m_CacheSlotIndex = 0xff;
	}
#endif
}

#if !__SPU
audMathOperationSound::audMathOperationSound()
{
	m_SlotIndexForResolvedOperations = 0xff;	
	m_RunOnce = false;
	BANK_ONLY(m_CacheSlotIndex = 0xff);
}

bool audMathOperationSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;
	MathOperationSound *mathOperationData = (MathOperationSound*)GetMetadata();
	SetChildSound(0,SOUNDFACTORY.GetChildInstance(mathOperationData->SoundRef, this, initParams, scratchInitParams, false));

	m_RunOnce = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_MATHOPERATIONSOUND_ONLYRUNONCE) == AUD_TRISTATE_TRUE);

	// grab a slot from the pool to store the resolved operations
	const u32 storageSize = mathOperationData->numOperations * sizeof(audMathOp);
	SoundAssert(sm_Pool.GetSoundSlotSize() >= storageSize);
	if(sm_Pool.GetSoundSlotSize() >= storageSize)
	{
		void *storage = sm_Pool.AllocateSoundSlot(storageSize, m_InitParams.BucketId, true);
		if(!storage)
		{
			return false;
		}
		m_SlotIndexForResolvedOperations = sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, storage);
		ResolveOperations(mathOperationData);

#if __BANK
		if(initParams->IsAuditioning)
		{
			void *cacheSlot = sm_Pool.AllocateSoundSlot(mathOperationData->numOperations * sizeof(audMathOpCache), m_InitParams.BucketId, true);
			if(cacheSlot)
			{
				m_CacheSlotIndex = sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, cacheSlot);
			}
		}
#endif

		return true;
	}
	else
	{
		return false;
	}
}
#endif

audPrepareState audMathOperationSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	audSound *child = GetChildSound(0);
	if(child)
	{
		child->SetStartOffset((s32)GetPlaybackStartOffset());
		return child->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
	}
	else
	{
		return AUD_PREPARED;
	}
}

void audMathOperationSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	RunComputation(timeInMs);

	audSound *child = GetChildSound(0);
	if(child)
	{
		child->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		child->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		child->_ManagedAudioPlay(timeInMs, combineBuffer);
	}
}

bool audMathOperationSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound *child = GetChildSound(0);
	if(child)
	{
		if(!m_RunOnce)
		{
			RunComputation(timeInMs);
		}
		return child->_ManagedAudioUpdate(timeInMs, combineBuffer);
	}
	else
	{
		// We don't do anything, and report that we've finished, as we do our setting on Play()
		return false;
	}
}

void audMathOperationSound::AudioKill()
{
	audSound *child = GetChildSound(0);
	if(child)
	{
		child->_ManagedAudioKill();
	}
}

s32 audMathOperationSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound *child = GetChildSound(0);
	if(child)
	{
		return child->ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
	}
	else
	{
		// We don't support lengths and start offets on logical sounds
		return kSoundLengthUnknown;
	}
}

void audMathOperationSound::ResolveOperations(MathOperationSound *metadata)
{
	m_NumOperations = metadata->numOperations;
	
	audMathOp *resolvedOperations = (audMathOp*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_SlotIndexForResolvedOperations);
	
	for(u32 i = 0 ; i < m_NumOperations; i++)
	{
		resolvedOperations[i].op = metadata->Operation[i].Operation;

		// Grab all our variables/values
		// param1
		if (metadata->Operation[i].param1.Variable == NULL_HASH)
		{
			// We don't have a variable, so use the numerical value instead
			resolvedOperations[i].param1.immediate = metadata->Operation[i].param1.Value;
			SetParamImm(1, i, true);
		}
		else
		{
			// We do have a variable, so use that
			resolvedOperations[i].param1.var = _FindVariableUpHierarchy(metadata->Operation[i].param1.Variable);
			if(resolvedOperations[i].param1.var==NULL)
			{		
				audWarningf("MathOperationSound %s with invalid variable reference: %u for param1 step %u [bucket: %u slot: %u]", GetName(), metadata->Operation[i].param1.Variable, i+1, m_InitParams.BucketId, sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, this));

				// may as well use 1.0 as its the safest option for most operations
				resolvedOperations[i].param1.immediate = 1.f;
				SetParamImm(1, i, true);
			}
			else
			{
				// flag the op as having a variable first param
				SetParamImm(1, i, false);
			}
		}

		// param2
		if (metadata->Operation[i].param2.Variable == NULL_HASH)
		{ 
			// We don't have a variable, so use the numerical value instead
			resolvedOperations[i].param2.immediate = metadata->Operation[i].param2.Value;
			SetParamImm(2, i, true);
		}
		else
		{
			// We do have a variable, so use that
			resolvedOperations[i].param2.var = _FindVariableUpHierarchy(metadata->Operation[i].param2.Variable);
			if(resolvedOperations[i].param2.var==NULL)
			{		
				audWarningf("MathOperationSound %s with invalid variable reference: %u for param2 step %u [bucket: %u slot: %u]", GetName(), metadata->Operation[i].param2.Variable, i, m_InitParams.BucketId, sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, this));

				// may as well use 1.0 as its the safest option for most operations
				resolvedOperations[i].param2.immediate = 1.f;
				SetParamImm(2, i, true);
			}
			else
			{
				// flag the op as having a variable second param
				SetParamImm(2, i, false);
			}
		}

		// param3
		if (metadata->Operation[i].param3.Variable == NULL_HASH)
		{
			// We don't have a variable, so use the numerical value instead
			resolvedOperations[i].param3.immediate = metadata->Operation[i].param3.Value;
			SetParamImm(3, i, true);
		}
		else
		{
			// We do have a variable, so use that
			resolvedOperations[i].param3.var = _FindVariableUpHierarchy(metadata->Operation[i].param3.Variable);
			if(resolvedOperations[i].param3.var==NULL)
			{		
				audWarningf("MathOperationSound %s with invalid variable reference: %u for param3 step %u [bucket: %u slot: %u]", GetName(), metadata->Operation[i].param3.Variable, i, m_InitParams.BucketId, sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, this));

				// may as well use 1.0 as its the safest option for most operations
				resolvedOperations[i].param3.immediate = 1.f;
				SetParamImm(3, i, true);
			}
			else
			{
				// flag the op as having a variable third param
				SetParamImm(3, i, false);
			}
		}

		// result
		resolvedOperations[i].result = _FindVariableUpHierarchy(metadata->Operation[i].Result);
		if(!resolvedOperations[i].result)
		{
			audWarningf("MathOperationSound %s with invalid result variable: step %u [bucket: %u slot: %u]", GetName(), i, m_InitParams.BucketId, sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, this));
		}
	}
}

void audMathOperationSound::RunComputation(const u32 timeInMs)
{
	audMathOp *resolvedOperations = (audMathOp*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_SlotIndexForResolvedOperations);

	m_TimeInMs = timeInMs;

	audRequestedSettings *reqSets = (audRequestedSettings*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, GetTopLevelRequestedSettingsIndex());
	SoundAssert(reqSets);

#if __BANK
	audMathOpCache *cache = m_CacheSlotIndex == 0xff ? NULL : (audMathOpCache*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_CacheSlotIndex);
#endif

	for(u32 i = 0 ; i < m_NumOperations; i++)
	{
		if(resolvedOperations[i].result STATS_ONLY(|| resolvedOperations[i].op == MATH_OPERATION_TRACE))
		{
			f32 result=0.0f;
			f32 p1,p2,p3;
			if(IsParamImm(1, i))
			{
				p1 = resolvedOperations[i].param1.immediate;
			}
			else
			{
				p1 = AUD_GET_VARIABLE(resolvedOperations[i].param1.var);
			}
			if(IsParamImm(2, i))
			{
				p2 = resolvedOperations[i].param2.immediate;
			}
			else
			{
				p2 = AUD_GET_VARIABLE(resolvedOperations[i].param2.var);
			}
			if(IsParamImm(3, i))
			{
				p3 = resolvedOperations[i].param3.immediate;
			}
			else
			{
				p3 = AUD_GET_VARIABLE(resolvedOperations[i].param3.var);
			}
			switch(resolvedOperations[i].op)
			{
				IMPLEMENT_OPERATION(MATH_OPERATION_ADD,opAdd)
				IMPLEMENT_OPERATION(MATH_OPERATION_SUBTRACT,opSubtract)
				IMPLEMENT_OPERATION(MATH_OPERATION_MULTIPLY,opMultiply)
				IMPLEMENT_OPERATION(MATH_OPERATION_DIVIDE,opDivide)
				IMPLEMENT_OPERATION(MATH_OPERATION_SET,opSet)
				IMPLEMENT_OPERATION(MATH_OPERATION_MOD,opMod)
				IMPLEMENT_OPERATION(MATH_OPERATION_MIN,opMin)
				IMPLEMENT_OPERATION(MATH_OPERATION_MAX,opMax)
				IMPLEMENT_OPERATION(MATH_OPERATION_ABS,opAbs)
				IMPLEMENT_OPERATION(MATH_OPERATION_SIGN,opSign)
				IMPLEMENT_OPERATION(MATH_OPERATION_FLOOR,opFloor)
				IMPLEMENT_OPERATION(MATH_OPERATION_CEIL,opCeil)
				IMPLEMENT_OPERATION(MATH_OPERATION_RAND,opRandomize)
				IMPLEMENT_OPERATION(MATH_OPERATION_SIN,opSin)
				IMPLEMENT_OPERATION(MATH_OPERATION_COS,opCos)
				IMPLEMENT_OPERATION(MATH_OPERATION_SQRT,opSqrt)
				IMPLEMENT_OPERATION(MATH_OPERATION_DBTOLINEAR,opdBToLinear)
				IMPLEMENT_OPERATION(MATH_OPERATION_LINEARTODB,opLinearTodB)
				IMPLEMENT_OPERATION(MATH_OPERATION_PITCHTORATIO,opPitchToRatio)
				IMPLEMENT_OPERATION(MATH_OPERATION_RATIOTOPITCH,opRatioToPitch)
				IMPLEMENT_OPERATION(MATH_OPERATION_GETTIME,opGetTime)
				IMPLEMENT_OPERATION(MATH_OPERATION_FSEL, opFsel)
				IMPLEMENT_OPERATION(MATH_OPERATION_VALUEINRANGE, opValueInRange)
				IMPLEMENT_OPERATION(MATH_OPERATION_CLAMP, opClampValue)
				IMPLEMENT_OPERATION(MATH_OPERATION_POW, opPow)
				IMPLEMENT_OPERATION(MATH_OPERATION_ROUND, opRound)
				IMPLEMENT_OPERATION(MATH_OPERATION_SCALEDSIN, opScaledSin)
				IMPLEMENT_OPERATION(MATH_OPERATION_SCALEDTRI, opScaledTri)
				IMPLEMENT_OPERATION(MATH_OPERATION_SCALEDSAW, opScaledSaw)
				IMPLEMENT_OPERATION(MATH_OPERATION_SCALEDSQUARE, opScaledSquare)
				IMPLEMENT_OPERATION(MATH_OPERATION_SMOOTH, opSmooth)
				IMPLEMENT_OPERATION(MATH_OPERATION_GETSCALEDTIME, opGetScaledTime)
				IMPLEMENT_OPERATION(MATH_OPERATION_CLAMPRANGE, opClampRange)
				
			case MATH_OPERATION_GETPOS_X:
				result = reqSets->GetPosition_AudioThread().GetXf();
				break;
			case MATH_OPERATION_GETPOS_Y:
				result = reqSets->GetPosition_AudioThread().GetYf();
				break;
			case MATH_OPERATION_GETPOS_Z:
				result = reqSets->GetPosition_AudioThread().GetZf();
				break;

#if __STATS
			case MATH_OPERATION_TRACE:
				PF_SET(Trace1, p1);
				PF_SET(Trace2, p2);
				PF_SET(Trace3, p3);
				break;
#endif

			}

			STATS_ONLY(if(resolvedOperations[i].result))
			{
				AUD_SET_VARIABLE(resolvedOperations[i].result, result);
			}

#if __BANK
			if(cache)
			{
				cache[i].p1 = p1;
				cache[i].p2 = p2;
				cache[i].p3 = p3;
				cache[i].result = result;
			}
#endif
		}
	}
}

#if __SOUND_DEBUG_DRAW
bool audMathOperationSound::DebugDrawInternal(const u32 UNUSED_PARAM(timeInMs), audDebugDrawManager &drawManager, const bool UNUSED_PARAM(drawDynamicChildren)) const
{
	audMathOp *resolvedOperations = (audMathOp*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_SlotIndexForResolvedOperations);
	audMathOpCache *cache = m_CacheSlotIndex == 0xff ? NULL : (audMathOpCache*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_CacheSlotIndex);

	if(resolvedOperations && cache)
	{
		for(u32 i = 0 ; i < m_NumOperations; i++)
		{
			if(resolvedOperations[i].result)
			{
				char buf[128];
				formatf(buf, "%u: %s(%f,%f,%f) = %f", i, MathOperations_ToString((MathOperations)resolvedOperations[i].op)+15, cache[i].p1,cache[i].p2,cache[i].p3,cache[i].result);
				drawManager.DrawLine(buf);
			}
			else
			{
				drawManager.DrawLine("Invalid result");
			}
		}
	}
	return true;
}
#endif // __SOUND_DEBUG_DRAW

} // namespace rage
