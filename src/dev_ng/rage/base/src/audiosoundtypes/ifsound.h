//
// audioengine/logicalandsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_IFSOUND_H
#define AUD_IFSOUND_H

#include "audiosoundtypes/sound.h"

#include "audiosoundtypes/sounddefs.h"

namespace rage {

// PURPOSE
//  An if sound plays one of two sounds, depending on whether its condition is true or false.
class audIfSound : public audSound
{
public:

	~audIfSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:
	// PURPOSE
	//  Determines which of the child sounds should be played, based upon the variables and condition.
	void ComputeBranchToBePlayed(void);

	audVariableHandle m_LeftVariable;
	f32 m_RightValue;
	audVariableHandle m_RightVariable;
	u32 m_ConditionType;
	u32 m_BranchToBePlayed;
	bool m_ShouldChooseOnInit;
};

} // namespace rage

#endif // AUD_IFSOUND_H
