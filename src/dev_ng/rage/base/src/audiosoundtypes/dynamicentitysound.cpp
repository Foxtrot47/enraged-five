//
// audioengine/dynamicentitysound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "dynamicentitysound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audioengine/entity.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audDynamicEntitySound);

audDynamicEntitySound::~audDynamicEntitySound()
{
	if(!IsBeingRemovedFromHierarchy())
	{
		if(GetChildSound(0))
		{
			DeleteChild(0);
		}
	}
}

#if !__SPU
bool audDynamicEntitySound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;
	DynamicEntitySound *dynamicEntitySoundData = (DynamicEntitySound*)GetMetadata();

	m_ShouldChooseOnInit = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_IFSOUND_CHOOSEONINIT) == AUD_TRISTATE_TRUE);	

	if(dynamicEntitySoundData->ObjectRefs)
	{
		const u32 * pObjectRef = &(dynamicEntitySoundData->ObjectRefs->GameObjectHash);
		u32 numRefs = dynamicEntitySoundData->numObjectRefs;
		//Strip away any empty entries on the path
		for(int i = 0, j = numRefs; i<j; i++)
		{
			if(*pObjectRef)
			{
				break;
			}
			--numRefs;
			++pObjectRef;
		}
		
		if(numRefs>=2)
		{

			audSound* myParent = this->GetTopLevelParent();
			if (myParent)
			{
				audEntity * myEntity = myParent->GetEntity();
				if(myEntity)
				{
					const u32 nameHash = myEntity->QuerySoundNameFromObjectAndField(pObjectRef, numRefs);
					if(nameHash != 0)
					{
						SetChildSound(0, SOUNDFACTORY.GetChildInstance(nameHash, this, initParams, scratchInitParams, false));
					}
					else
					{
						SetChildSound(0, SOUNDFACTORY.GetChildInstance(g_NullSoundHash, this, initParams, scratchInitParams, false));
					}
					
					if(!GetChildSound(0))
					{
						return false;
					}
					return true;
				}
			}
		}
	}
	return false;
}
#endif

audPrepareState audDynamicEntitySound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	if (GetChildSound(0))
	{
		GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		return GetChildSound(0)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
	}
	else
	{
		return AUD_PREPARED;
	}
}

void audDynamicEntitySound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound *childSound = GetChildSound(0);
	if (childSound)
	{
		childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		childSound->_ManagedAudioPlay(timeInMs, combineBuffer);

		RemoveMyselfFromHierarchy();
	}
}

bool audDynamicEntitySound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	if(GetChildSound(0))
	{
		return GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
	}
	else
	{
		// we dont have a true sound so we're done
		return false;
	}
}

void audDynamicEntitySound::AudioKill()
{
	// See which branch we're playing, and stop it. If it's neither, that's maybe valid?
	if (GetChildSound(0))
	{
		GetChildSound(0)->_ManagedAudioKill();
	}
}

s32 audDynamicEntitySound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	if(GetChildSound(0))
	{
		return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
	}
	else
	{
		return 0;
	}
}

} // namespace rage