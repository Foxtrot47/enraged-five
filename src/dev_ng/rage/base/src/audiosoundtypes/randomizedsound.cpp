//
// audioengine/randomizedsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "randomizedsound.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"

namespace rage
{

AUD_IMPLEMENT_STATIC_WRAPPERS(audRandomizedSound);

audRandomizedSound::~audRandomizedSound()
{
	if(!IsBeingRemovedFromHierarchy() && GetChildSound(0))
	{
		DeleteChild(0);
	}
}

#if !__SPU
bool audRandomizedSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	RandomizedSound::tVariations *variations;
	u8 numVariations = 0;
	m_SelectedVariationIndex = 0;

	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	RandomizedSound *randomizedSoundData = (RandomizedSound*)GetMetadata();

	GetVariations(randomizedSoundData, numVariations, &variations);

	if(numVariations == 0)
	{
		// If we don't have any variations to choose from then we can't create a child sound,
		// so don't even try.
		return false;
	}

	if (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_RANDOMIZEDSOUND_ORDEREDPLAYLIST) == AUD_TRISTATE_TRUE)
	{
		m_SelectedVariationIndex = randomizedSoundData->HistoryIndex;
		randomizedSoundData->HistoryIndex = (randomizedSoundData->HistoryIndex + 1) % numVariations;
	}
	else
	{
		m_SelectedVariationIndex = ComputeVariationIndex(randomizedSoundData, numVariations, variations);
		AddVariationToHistory(randomizedSoundData,m_SelectedVariationIndex);
	}

	SetChildSound(0,SOUNDFACTORY.GetChildInstance(variations[m_SelectedVariationIndex].Variation, this, initParams, scratchInitParams, false));

	if(!GetChildSound(0))
	{
		return false;
	}

	return true;
}
#endif // !__SPU

void audRandomizedSound::GetVariations(const RandomizedSound *metadata, u8 &size, RandomizedSound::tVariations **ptr)
{
	// variations are after variable length history array
	u8 *p = (u8*)&metadata->HistorySpace[metadata->numHistorySpaceElems];
	size = *p;
	p++;
	*ptr = (RandomizedSound::tVariations*)p;
}

void audRandomizedSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(GetChildSound(0));
	childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
	childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
	childSound->_ManagedAudioPlay(timeInMs, combineBuffer);

	// once we have set up our child sound we're effectively done ...
	RemoveMyselfFromHierarchy();
}

bool audRandomizedSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound *child = GetChildSound(0);
	SoundAssert(child);
	SoundAssert (child->GetPlayState() == AUD_SOUND_PLAYING);
	if (child->_ManagedAudioUpdate(timeInMs, combineBuffer))
	{
		return (true);
	}
	else
	{
		return (false);
	}	
}

void audRandomizedSound::AudioKill()
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound);
	if(childSound)
	{
		childSound->_ManagedAudioKill();
	}
}

u8 audRandomizedSound::ComputeVariationIndex(const RandomizedSound *metadata, const u32 numVariations, const RandomizedSound::tVariations *variations)
{
	u8 variation = 0;
	f32 weightSum = 0.f;
	bool useHistory = true;

	SoundAssert(metadata->numHistorySpaceElems < numVariations);

	if(metadata->numHistorySpaceElems >= numVariations)
	{
#if __DEV
		audWarningf("audRandomizedSound %s : number of history space elements too great, history disabled for this sound", GetName());
#endif
		useHistory = false;
	}
	
	// compute sum of weights - only consider variations that are not in history
	for(u32 i = 0 ; i < numVariations; i++)
	{
		if(!useHistory || !IsVariationInHistory(metadata,(u8)i))
		{
			weightSum += variations[i].Weight;
		}
	}

	// compute random r in range [0.f,weightSum]
	f32 r = audEngineUtil::GetRandomNumberInRange(0.f, weightSum);

	// find where r falls in weight range to pick variation, again only consider
	// variations that are not in history
	for(u32 i = 0; i < numVariations; i++)
	{
		if(!useHistory || !IsVariationInHistory(metadata,(u8)i))
		{
			r -= variations[i].Weight;
			if(r <= 0.f)
			{
				variation = (u8)i;
				break;
			}
		}
	}

	// If r > 0 then either weights or history have changed since the summing loop.
	// This could happen since this code will run on the game thread and RAVE could be
	// tweaking things from the audio thread so don't assert, just warn.
	//SoundAssert(r<=0.f);
#if __DEV
	if(r>0.f)
	{
		audWarningf("audRandomizedSound: weights or history have changed - random result not accurate");
	}
#endif

	return variation;	
}

void audRandomizedSound::AddVariationToHistory(RandomizedSound *metadata, const u8 variation)
{
	if(metadata->numHistorySpaceElems > 0)
	{
		SoundAssert(metadata->HistoryIndex < metadata->numHistorySpaceElems);
		metadata->HistorySpace[metadata->HistoryIndex] = variation;
		metadata->HistoryIndex = (metadata->HistoryIndex + 1) % metadata->numHistorySpaceElems;
	}
}

bool audRandomizedSound::IsVariationInHistory(const RandomizedSound *metadata, const u8 variation)
{
	for(int i = 0 ; i < metadata->numHistorySpaceElems; i++)
	{
		if(metadata->HistorySpace[i] == variation)
		{
			return true;
		}
	}
	return false;
}

audPrepareState audRandomizedSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound);
	childSound->SetStartOffset((s32)GetPlaybackStartOffset());
	return childSound->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
}

s32 audRandomizedSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound);
	return childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
}

}
