//
// audiosoundtypes/kineticsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_KINETIC_SOUND_H
#define AUD_KINETIC_SOUND_H

#include "sound.h"
#include "sounddefs.h"
#include "audioengine/widgets.h"


namespace rage
{

	class audKineticSound;

	struct audKineticSoundState
	{
		audKineticSoundState();
		
		Vec3V m_Orientation;
		audVariableHandle m_VelocityVariable;
		audVariableHandle m_InertiaVariable;
		audVariableHandle m_AccelerationVariable;
		audVariableHandle m_ForceVariable;
		audVariableHandle m_AngVelVariable;
		audVariableHandle m_AngAccelVariable;
		audVariableHandle m_AccelDeltaVariable;
		f32 m_Mass;
	};

	// PURPOSE
	//A kinetic sound updates sound variables with force, torque, acceleration 
	class audKineticSound : public audSound
	{
	public:
		
		~audKineticSound();

		AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
		audKineticSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif // !__SPU

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();
		// PURPOSE
		//  Implements base class function for this sound.
		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

		// PURPOSE
		//Pushes out sound variables with the current accelleration, linear force, torque etc
		void UpdateKineticSoundVariables(u32 timeInMs);

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;
		
		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *slot, bool checkStateOnly);
	
	private:		

		Vec3V m_LastVelocity;
		QuatV m_LastOrientation;
		f32 m_LastAngVel;
		f32 m_LastAccelleration;
		f32 m_LastVariableVelocity;
		u8	m_StateSlotIndex;
		u8	m_GameFrame;
		bool m_UseOrientaionForVelocity :1;
	};

} // namespace rage

#endif //AUD_KINETIC_SOUND_H
