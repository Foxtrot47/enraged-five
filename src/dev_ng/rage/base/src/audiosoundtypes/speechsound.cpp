//
// audioengine/speechsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "speechsound.h"

#include "environmentsound.h"

#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/waveref.h"
#include "audiohardware/waveslot.h"
#include "audiohardware/mixer.h"
#include "audiohardware/pcmsource_interface.h"
#include "string/stringhash.h"
#include "system/bit.h"
#include "system/param.h"
#include "audiohardware/waveplayer.h"
#include "audioengine/speechdefs.h"

namespace rage {
extern bool NIY;
#if !__SPU

audMetadataManager audSpeechSound::sm_MetadataMgr;
u32 audSpeechSound::sm_ExclusionPrefixPartialHash = 0;

#endif // !__SPU

AUD_IMPLEMENT_STATIC_WRAPPERS(audSpeechSound);

audSpeechSound::~audSpeechSound()
{
	if (!IsBeingRemovedFromHierarchy() && GetChildSound(0))
	{
		DeleteChild(0);
	}
	if(m_SpeechParamsIndex != 0xff)
	{
		sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_SpeechParamsIndex);
		m_SpeechParamsIndex = 0xff;
	}
	if(m_WavePlayerSlotId != -1)
	{
		audPcmSourceInterface::Release(m_WavePlayerSlotId);
		m_WavePlayerSlotId = -1;
	}
}

#if !__SPU

audSpeechSound::audSpeechSound()
{
	m_WavePlaytime = -1;
	m_SlotIndex = -1;
	m_SpeechParamsIndex = 0xff;
	m_WavePlayerSlotId = -1;
	m_WaveLengthMs = 0;
	m_WaveHeadroom = 0;
}

bool audSpeechSound::InitClass(void)
{
	if(!sm_MetadataMgr.Init("SpeechSound", g_AudioEngine.GetConfig().GetSpeechMetadata(), audMetadataManager::External_NameTable_Never, SPEECHDEFS_SCHEMA_VERSION))
	{
		return false;
	}
	SOUNDFACTORY.GetMetadataManager().AddExtraStringTable(sm_MetadataMgr.GetChunk(sm_MetadataMgr.GetNumChunks() - 1));
		
	return true;
}

void audSpeechSound::ShutdownClass(void)
{	
	for(int i=0; i< sm_MetadataMgr.GetNumChunks(); i++)
	{
		SOUNDFACTORY.GetMetadataManager().RemoveExtraStringTable(sm_MetadataMgr.GetChunk(i).chunkNameHash);
	}

	sm_MetadataMgr.Shutdown();
}

bool audSpeechSound::LoadSpeechMetadata(const char* metadataName, const char* path)
{
	if(sm_MetadataMgr.LoadMetadataChunk(metadataName, path))
	{
		SOUNDFACTORY.GetMetadataManager().AddExtraStringTable(sm_MetadataMgr.GetChunk(sm_MetadataMgr.FindChunkId(metadataName)));
		return true;
	}	
	return false;
}

bool audSpeechSound::UnloadSpeechMetadata(const char* metadataName)
{
	if(sm_MetadataMgr.UnloadMetadataChunk(metadataName))
	{
		SOUNDFACTORY.GetMetadataManager().RemoveExtraStringTable(atStringHash(metadataName));
		return true;
	}
	return false;
}

u32 audSpeechSound::FindNumVariationsForVoiceAndContext(const char *voiceName, const char *contextName)
{
	return FindNumVariationsForVoiceAndContext(atStringHash(voiceName), atPartialStringHash(contextName));
}

u32 audSpeechSound::FindNumVariationsForVoiceAndContext(const u32 voiceNameHash, const char *contextName)
{
	return FindNumVariationsForVoiceAndContext(voiceNameHash, atPartialStringHash(contextName));
}


u32 audSpeechSound::FindNumVariationsForVoiceAndContext(const u32 voiceNameHash, const u32 contextNamePartialHash)
{
	u32 numVariations;
	FindVariationInfoForVoiceAndContext(voiceNameHash, contextNamePartialHash, numVariations);
	return numVariations;
}

bool audSpeechSound::FindVariationInfoForVoiceAndContext(const char *voiceName, const char *contextName,
														 u32 &numVariations, u8 *variationData, u32 *variationUids)
{
	return FindVariationInfoForVoiceAndContext(atStringHash(voiceName), atPartialStringHash(contextName), numVariations, variationData, variationUids);
}

bool audSpeechSound::FindVariationInfoForVoiceAndContext(const u32 voiceNameHash, const u32 contextNamePartialHash,
	u32 &numVariations, u8 *variationData, u32 *variationUids)
{
	numVariations = 0;

	const u32 contextNameHash = atFinalizeHash(contextNamePartialHash);	
		
	u32 lookupHash = voiceNameHash ^ contextNameHash;
	if(voiceNameHash == contextNameHash)
		lookupHash = voiceNameHash;

	char lookupString[32];
	formatf(lookupString, "%X", lookupHash);

	const u32 finalHash = atStringHash(lookupString);

	u16 variationMask = 0xFFFF;
	u32 numRejectedVariations = 0;
	if(sm_ExclusionPrefixPartialHash != 0)
	{
		const u32 exclusionObjNameHash = atStringHash(lookupString, sm_ExclusionPrefixPartialHash);
		VoiceContextVariationMask* voiceContextVarMask = (VoiceContextVariationMask*)sm_MetadataMgr.GetObjectMetadataPtr(exclusionObjNameHash);
		if(voiceContextVarMask)
		{
			// we store the variation mask as 1 = invalid, so negate
			variationMask = ~voiceContextVarMask->variationMask;
			numRejectedVariations = CountOnBits((u32)voiceContextVarMask->variationMask);
		}
	}

	VoiceContextInfo* voiceContextInfo = (VoiceContextInfo*)sm_MetadataMgr.GetObjectMetadataPtr(finalHash);
	if(voiceContextInfo)
	{
		if(variationData)
		{
			char lookupString[32];
			formatf(lookupString, "%X_VD", lookupHash);

			const u32 vdFinalHash = atStringHash(lookupString);

			VoiceContextInfoVarGroupData* voiceContextInfoVarGroupData = (VoiceContextInfoVarGroupData*)sm_MetadataMgr.GetObjectMetadataPtr(vdFinalHash);
			if(voiceContextInfoVarGroupData)
			{
				VariationGroupInfoData* varGroupInfoData = (VariationGroupInfoData*)sm_MetadataMgr.GetObjectMetadataPtr(voiceContextInfoVarGroupData->ref);
				if(varGroupInfoData)
				{
					for(u8 k=0,numAllowedVariations=0,varBit=1; k<varGroupInfoData->numVariationDatas; k++)
					{
						// Treat 0xffff as 'all variations allowed'; we don't support masking on contexts with >16 variations
						if(variationMask == 0xffff || variationMask & varBit)
						{
							variationData[numVariations + numAllowedVariations++] = varGroupInfoData->VariationData[k].val;
						}
						varBit <<= 1;
					}
				}
			}
		}

		// limit number of variations by mask
		numRejectedVariations = Min<u32>(numRejectedVariations, voiceContextInfo->numVariations);
		const u32 numAllowedVariations = static_cast<u32>(voiceContextInfo->numVariations) - numRejectedVariations;
		if(variationUids)
		{
			for(u32 variationIndex=0; variationIndex < numAllowedVariations; variationIndex++)
			{
				//Generate a UID for a variation by combining the voice name hash, context name hash
				//and variation index.
				variationUids[numVariations + variationIndex] = (lookupHash ^ variationIndex);
			}
		}
		numVariations += numAllowedVariations;
	}
	
	return (numVariations > 0);
}

u32 audSpeechSound::FindBankIdForVoiceAndContext(const char *voiceName, const char *contextName)
{
	return FindBankIdForVoiceAndContext(atStringHash(voiceName), contextName);
}

u32 audSpeechSound::FindBankIdForVoiceAndContext(const u32 voiceNameHash, const char *contextName){

	const u32 contextNameHash = atStringHash(contextName);
	u32 lookupHash = voiceNameHash ^ contextNameHash;
	if(voiceNameHash == contextNameHash)
		lookupHash = voiceNameHash;

	char lookupString[32];
	formatf(lookupString, "%X", lookupHash);

	const u32 finalHash = atStringHash(lookupString);

	u32 chunkId = 0;
	VoiceContextInfo* voiceContextInfo = (VoiceContextInfo*)sm_MetadataMgr.GetObjectMetadataPtrAndChunkId(finalHash, chunkId);
	if(voiceContextInfo)
	{
		u16 bankNameTableIndex = voiceContextInfo->bankNameTableIndex;
		return FindBankIdFromBankNameTableIndexAndChunkIndex(bankNameTableIndex, chunkId);
	}
	
	return AUD_INVALID_BANK_ID;
}

u8 audSpeechSound::FindPainType(u32 voiceNameHash)
{
	Voice* voice = (Voice*)sm_MetadataMgr.GetObjectMetadataPtr(voiceNameHash);
	if(voice)
	{
		return voice->painType;
	}	
	return 0;
}

u32 audSpeechSound::FindBankIdFromBankNameTableIndexAndChunkIndex(u32 index, u32 chunkIndex)
{
	char bankIndexString[32];
	formatf(bankIndexString, "%d", index);
	BankNameTable* bankNameTableEntry = NULL;

	bankNameTableEntry = (BankNameTable*)sm_MetadataMgr.GetObjectMetadataPtrFromSpecificChunk(atStringHash(bankIndexString), chunkIndex);
	if(bankNameTableEntry)
	{
		const char* bankName = sm_MetadataMgr.GetStringFromTableIndex(bankNameTableEntry->bankName);
		u32 bankId = audWaveSlot::FindBankId(bankName);
		if(bankId != ~0U)
			return bankId;
	}
	return AUD_INVALID_BANK_ID;
}

void audSpeechSound::SetExclusionPrefix(const char *exclPrefix)
{
	if(!exclPrefix)
	{
		sm_ExclusionPrefixPartialHash = 0;
	}
	else
	{
		sm_ExclusionPrefixPartialHash = atPartialStringHash("_", atPartialStringHash(exclPrefix));
	}
}

bool audSpeechSound::InitSpeech(const u32 voiceNameHash, const char *contextName, u32 variationNum)
{
	return InitSpeech(voiceNameHash, atPartialStringHash(contextName), variationNum, NULL, contextName);
}

bool audSpeechSound::InitSpeech(const char *voiceName, const char *contextName, u32 variationNum)
{
	return InitSpeech(atStringHash(voiceName), atPartialStringHash(contextName), variationNum, voiceName, contextName);
}

bool audSpeechSound::InitSpeech(const u32 voiceNameHash, const u32 contextNamePartialHash, u32 variationNum, const char *OUTPUT_ONLY(voiceName), const char *OUTPUT_ONLY(contextName))
{
	audSpeechSoundParams *params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
	SoundAssert(params);

	const u32 contextNameHash = atFinalizeHash(contextNamePartialHash);

	if(!params)
	{
		return false;
	}

	if(variationNum == 0)
	{
		audWarningf("Invalid variation (0) for context (%s) - variations are not zero-indexed!",contextName ? contextName : "(null)");
		return false;
	}

	u32 lookupHash = voiceNameHash ^ contextNameHash;
	if(voiceNameHash == contextNameHash)
		lookupHash = voiceNameHash;

	char lookupString[32];
	formatf(lookupString, "%X", lookupHash);

	const u32 finalHash = atStringHash(lookupString);

	u32 chunkId = 0;
	VoiceContextInfo* voiceContextInfo = (VoiceContextInfo*)sm_MetadataMgr.GetObjectMetadataPtrAndChunkId(finalHash, chunkId);
	if(voiceContextInfo)
	{
		// if we're using an exclusion mask then we may need to extend the requested variation number to the real variation number
		if(sm_ExclusionPrefixPartialHash != 0)
		{
			const u32 exclusionObjNameHash = atStringHash(lookupString, sm_ExclusionPrefixPartialHash);
			VoiceContextVariationMask* voiceContextVarMask = (VoiceContextVariationMask*)sm_MetadataMgr.GetObjectMetadataPtr(exclusionObjNameHash);
			if(voiceContextVarMask)
			{
				// we store the variation mask as 1 = invalid
				// NOTE: bit 0 corresponds to variation 1
				u32 variationIncr = 0;
				for(u32 testVar = 1, testMask = 1; testVar <= variationNum; testVar++, testMask <<= 1)
				{
					if(testMask & voiceContextVarMask->variationMask)
					{
						variationIncr++;
					}
				}

				variationNum += variationIncr;
			}
		}
	
		if((variationNum > 0) && (variationNum <= voiceContextInfo->numVariations))
		{
			//We found the specific voice context that contains the variation we require.
			char waveName[8];

			params->BankId = FindBankIdFromBankNameTableIndexAndChunkIndex(voiceContextInfo->bankNameTableIndex, chunkId);

			AssertMsg(params->BankId < AUD_INVALID_BANK_ID , "Invalid speech bank");
			AssertMsg(params->BankId != ~0U, "Invalid speech bank");

			if(params->BankId == AUD_INVALID_BANK_ID)
			{
				Displayf("Invalid Speech Bank - lookup string %s", lookupString);
				return false;
			}

			//Generate strictly-formatted wave name from context name and variation number.
			formatf(waveName, "_%02d", variationNum);
			// wave name is CONTEXT_XX, so we use the context name hash as an initial value for the hashing function
			params->WaveNameHash = atStringHash(waveName, contextNamePartialHash);

#if __ASSERT
			if(contextName != NULL)
			{
				char testString[64];
				formatf(testString, "%s_%02d", contextName, variationNum);
				const u32 testStringHash = atStringHash(testString);
				Assert(testStringHash == params->WaveNameHash);
			}
#endif

			return true;
		}
		audWarningf("Invalid variation (%d) for context %u (%s) voice %u (%s)", variationNum, contextNameHash, contextName ? contextName : "(null)", voiceNameHash, voiceName ? voiceName : "(null)");
	}
	else
	{
		audWarningf("VoiceContextInfo not found Voice(%u) VoiceContextInfo(%x) (%s)", voiceNameHash, finalHash, voiceName ? voiceName : "(null)");
	}
	

	return false;
}

bool audSpeechSound::InitSpeech(const audSpeechSoundParams &inputParams)
{
	audSpeechSoundParams *params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
	SoundAssert(params);

	if(!params)
	{
		return false;
	}

	*params = inputParams;

	return true;
}

bool audSpeechSound::DoesVoiceExist(u32 voiceNameHash)
{	
	Voice* voice = (Voice*)sm_MetadataMgr.GetObjectMetadataPtr(voiceNameHash);
	if(voice)
	{
		return true;
	}
	
	return false;
}

audPrepareState audSpeechSound::BatchPrepare(audSpeechSoundInfo *sounds, const u32 numSounds)
{
	bool needRequest = false;
	for(u32 i = 0; i < numSounds; i++)
	{
		audWaveSlot::audWaveSlotLoadStatus status = sounds[i].slot->GetWaveLoadingStatus(static_cast<u16>(sounds[i].sound->GetBankId()), sounds[i].sound->GetWaveNameHash());
		if(status == audWaveSlot::FAILED)
		{
			return AUD_PREPARE_FAILED;
		}
		else if(status == audWaveSlot::LOADING)
		{
			return AUD_PREPARING;
		}
		else if(status == audWaveSlot::NOT_REQUESTED)
		{
			needRequest = true;
		}
	}

	// Ensure we never return PREPARED if a batch is processing, even if the sounds are currently ready to go (which would be an
	// unusual case but can happen)
	if(audWaveSlot::IsBatchWaveLoadRequested())
	{
		return AUD_PREPARING;	
	}

	if(needRequest)
	{
		if(!audWaveSlot::IsBatchWaveLoadRequested())
		{
			audBatchWaveLoadRequest request[g_MaxWaveLoadsPerBatch];
			for(u32 i = 0; i < numSounds; i++)
			{
				if(sounds[i].slot->GetReferenceCount() > 0)
				{
					audWarningf("audSpeechSound: Trying to perform BatchPrepare on slot with non-zero ref count.");
				}

				Assign(request[i].slotIndex, audWaveSlot::GetWaveSlotIndex(sounds[i].slot));
				Assign(request[i].bankId, sounds[i].sound->GetBankId());
				request[i].waveNameHash = sounds[i].sound->GetWaveNameHash();
			}
			if(!audWaveSlot::RequestBatchWaveLoad(&request[0], numSounds))
			{
				return AUD_PREPARE_FAILED;
			}
		}
		
		return AUD_PREPARING;	
	}

	return AUD_PREPARED;
}

bool audSpeechSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
	{
		return false;
	}

	audSpeechSoundParams *p = (audSpeechSoundParams*)sm_Pool.AllocateRequestedSettingsSlot(sizeof(audSpeechSoundParams), m_InitParams.BucketId DEV_ONLY(, GetName()));
	if(!p)
	{
		return false;
	}
	p->BankId = AUD_INVALID_BANK_ID;
	p->WaveNameHash = 0;
	p->PredelayMs = 0;
	m_SpeechParamsIndex = sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, p);

	SetChildSound(0,SOUNDFACTORY.GetEnvironmentSound(this, initParams, scratchInitParams));
	if(!GetChildSound(0))
	{
		return false;
	}

	SpeechSound *speechSoundData = (SpeechSound*)GetMetadata();
	if(speechSoundData)
	{
		if(speechSoundData->ContextNameLen != 0)
		{
			Assertf(speechSoundData->ContextNameLen < 128, "Context name set in speech sound too long.  Context %s", speechSoundData->ContextName);
			u32 voiceNameHash = speechSoundData->VoiceName;
			char contextName[128];
			safecpy(contextName, speechSoundData->ContextName, Min(128, speechSoundData->ContextNameLen+1));
			u32 numVariations = FindNumVariationsForVoiceAndContext(voiceNameHash, contextName);
			u32 variation = 1;
			if(numVariations == 0)
				return false;

			if(numVariations != 1)
			{
				variation = (audEngineUtil::GetRandomInteger() % numVariations) + 1;
				if(variation == speechSoundData->LastVariation)
					variation = (variation % numVariations) + 1;
			}
			InitSpeech(voiceNameHash, contextName, variation);
			speechSoundData->LastVariation = variation;
		}
		else if(speechSoundData->DynamicFieldName != 0u)
		{
			audSound* myParent = this->GetTopLevelParent();
			if (myParent)
			{
				audEntity * myEntity = myParent->GetEntity();
				if(myEntity)
				{
					u32 voiceNameHash, contextNamePartialHash = 0u;
					myEntity->QuerySpeechVoiceAndContextFromField(speechSoundData->DynamicFieldName, voiceNameHash, contextNamePartialHash);

					if(voiceNameHash != 0u && contextNamePartialHash != 0u)
					{
						u32 numVariations = FindNumVariationsForVoiceAndContext(voiceNameHash, contextNamePartialHash);
						u32 variation = 1;

						if(numVariations == 0)
							return false;

						if(numVariations != 1)
						{
							variation = (audEngineUtil::GetRandomInteger() % numVariations) + 1;
						}

						InitSpeech(voiceNameHash, contextNamePartialHash, variation);
						speechSoundData->LastVariation = variation;
					}
				}
			}
		}
	}


	return true;
}
#endif

u32 audSpeechSound::GetBankId() const
{
	u32 ret = AUD_INVALID_BANK_ID;
	SoundAssert(m_SpeechParamsIndex!=0xff);
	audSpeechSoundParams *params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
	if(params)
	{
		ret = params->BankId;
	}
	return ret;
}

u32 audSpeechSound::GetWaveNameHash() const
{
	u32 ret = 0;
	SoundAssert(m_SpeechParamsIndex!=0xff);
	audSpeechSoundParams *params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;	
	if(params)
	{
		ret = params->WaveNameHash;
	}
	return ret;
}

audPrepareState audSpeechSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
{
	SoundAssert(slot);
	const s32 slotIndex = audWaveSlot::GetWaveSlotIndex(slot);

	audSpeechSoundParams *params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
	SoundAssert(params);
	if(!params)
	{
		return AUD_PREPARE_FAILED;
	}

	if(slotIndex == -1)
	{
		audWarningf("A host wave slot was not specified for sound - %s", GetName());
		return AUD_PREPARE_FAILED;
	}

	audEnvironmentSound *envSound = (audEnvironmentSound*)GetChildSound(0);
	if (params->BankId >= AUD_INVALID_BANK_ID || !envSound)
	{
		return AUD_PREPARE_FAILED;
	}

	const audWaveSlot::audWaveSlotLoadStatus status = slot->GetAssetLoadingStatus((u16)params->BankId, params->WaveNameHash);
	if(checkStateOnly)
	{
		switch(status)
		{
		case audWaveSlot::LOADED:
			return AUD_PREPARED;
		case audWaveSlot::FAILED:
			return AUD_PREPARE_FAILED;
		default:
			return AUD_PREPARING;
		}
	}
	else
	{
		m_SlotIndex = slotIndex;

		switch(status)
		{
			case audWaveSlot::LOADED:
				SoundAssert(m_SlotIndex>=0);

				if(!SetupWavePlayer())
				{
					return AUD_PREPARE_FAILED;
				}

				return AUD_PREPARED;
			case audWaveSlot::LOADING:
				return AUD_PREPARING;
			case audWaveSlot::FAILED:
				return AUD_PREPARE_FAILED;
			case audWaveSlot::NOT_REQUESTED:
				if(GetCanDynamicallyLoad())
				{
					slot->LoadAsset((u16)params->BankId, params->WaveNameHash, GetWaveLoadPriority());
					return AUD_PREPARING;
				}
				// else - fall through to default
			default:
				return AUD_PREPARE_FAILED;
		}
	}
}

s32 audSpeechSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	SoundAssert(GetEnvironmentSound());
	return GetEnvironmentSound()->ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
}

void audSpeechSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound *envSound = GetChildSound(0);
	if (envSound)
	{
		audSpeechSoundParams* params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
		SoundAssert(params);
		if(params->PredelayMs)
		{
			envSound->_SetRequestedInternalPredelay(params->PredelayMs);
		}
		else
		{
			const u32 residualPredelay = GetPredelayRemainingMs(timeInMs);
			Assert(residualPredelay <= kPredelayStartThresholdMs);
			if(residualPredelay)
			{
				const u32 delaySamples = audDriverUtil::ConvertMsToSamples(residualPredelay, kMixerNativeSampleRate);
				audPcmSourceInterface::SetParam(m_WavePlayerSlotId, audWavePlayer::Params::DelaySamples, delaySamples);
			}
		}

		envSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		envSound->_ManagedAudioPlay(timeInMs, combineBuffer);

		// we have nothing more to add ...
		RemoveMyselfFromHierarchy();
	}
}

bool audSpeechSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audEnvironmentSound *envSound = (audEnvironmentSound*)GetChildSound(0);
	if (!envSound)
	{
		return false;
	}

	PcmSourceState state;
	if(audPcmSourceInterface::GetState(state, m_WavePlayerSlotId))
	{
		m_WaveHeadroom = state.CurrentPeakLevel;
		if(!state.hasStartedPlayback)
		{
			m_WavePlaytime = 0;
		}
		else if(state.PlaytimeSamples == ~0U)
		{
			m_WavePlaytime = -1;
		}
		else
		{
			m_WavePlaytime = audDriverUtil::ConvertSamplesToMs(state.PlaytimeSamples, state.AuthoredSampleRate);
		}
	}
	
	return envSound->_ManagedAudioUpdate(timeInMs, combineBuffer);
}

f32 audSpeechSound::GetWaveHeadroom() const
{
	// 1 / 65536
	return m_WaveHeadroom * 0.0000152587890625f;
}

void audSpeechSound::SetAdditionalPredelay(const u32 predelayMs)
{
	audSpeechSoundParams* params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
	if(params)
	{
		SoundAssert(params->PredelayMs == 0);
		params->PredelayMs = predelayMs;
	}
}

u32 audSpeechSound::GetAdditionalPredelay()
{
	audSpeechSoundParams* params = m_SpeechParamsIndex != 0xff ? (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
	if(params)
	{
		return params->PredelayMs;
	}
	return 0;
}

void audSpeechSound::AudioKill()
{
	audSound *envSound = GetChildSound(0);
	if (envSound)
	{
		envSound->_ManagedAudioKill();
	}
}

bool audSpeechSound::SetupWavePlayer()
{
	audSpeechSoundParams *params = m_SpeechParamsIndex != 0xff ?  (audSpeechSoundParams*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SpeechParamsIndex) : NULL;
	SoundAssert(params);
	if(!m_WaveReference.Init(m_SlotIndex, params->WaveNameHash))
	{
#if __SPU
		audWarningf("Failed to find wave %u %u for speech sound %s", params->BankId, params->WaveNameHash, GetName());
#else
		audWarningf("Failed to find wave %s %u for speech sound %s", audWaveSlot::GetBankName(params->BankId), params->WaveNameHash, GetName());
#endif
		return false;
	}

	// allocate a wave player
	if(m_WavePlayerSlotId == -1)
	{
		Assign(m_WavePlayerSlotId, audPcmSourceInterface::Allocate(AUD_PCMSOURCE_WAVEPLAYER));
		if(m_WavePlayerSlotId == -1)
		{
			// failed to allocate a wave player
			return false;
		}
	}

	// Pull out headroom information and pass to environment sound
	u32 formatChunkSize = 0;
	const audWaveFormat *format = m_WaveReference.FindFormat(formatChunkSize);
	if(Verifyf(format, "Wave missing format chunk, bank %s wave %u", audWaveSlot::GetWaveSlotFromIndex(m_SlotIndex)->GetLoadedBankName(), params->WaveNameHash))
	{
		const f32 headroom = format->Headroom * 0.01f;	
		m_WaveLengthMs = (s32)audDriverUtil::ConvertSamplesToMs(format->LengthSamples, format->SampleRate);
		const u16 firstPeakLevel = AUD_READ_FIRST_PEAK_SAMPLE(format, formatChunkSize);
		GetEnvironmentSound()->InitEnvironment(m_WavePlayerSlotId, headroom, (s32)m_WaveLengthMs, format->LoopPointSamples != -1, firstPeakLevel);
	}
	else
	{
		return false;
	}

	// Add an extra wave reference for the wave player.  Adding it here (synchronously) ensures that even if the speech
	// sound is destroyed before the wave player is initialised, the slot will remain referenced until the wave player
	// shuts down.
	audWaveSlot::AddSlotReference(m_WaveReference.GetSlotId());
	return audPcmSourceInterface::SetParam(m_WavePlayerSlotId, audWavePlayer::Params::WaveReference, m_WaveReference.GetAsU32());
}

#if !__SPU
void audSpeechSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 UNUSED_PARAM(childIndex))
{
	if(!SetupWavePlayer())
	{
		// Kill our environment sound, which will also tell the wave player to Stop
		DeleteChild(0);
	}
}
#endif // __SPU

} // namespace rage
