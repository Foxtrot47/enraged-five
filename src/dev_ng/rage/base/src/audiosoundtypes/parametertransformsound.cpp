// 
// audiosoundtypes/parametertransformsound.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "parametertransformsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"

namespace rage 
{
	AUD_IMPLEMENT_STATIC_WRAPPERS(audParameterTransformSound);

	audParameterTransformSound::~audParameterTransformSound()
	{
		ReleaseSlots();
		if(GetChildSound(0))
		{
			DeleteChild(0);
		}
	}

	void audParameterTransformSound::ReleaseSlots()
	{
		for(u32 i = 0; i < kMaxTransformSlotIds; i++)
		{
			if(m_TransformSlotIds[i] != 0xff)
			{
				sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_TransformSlotIds[i]), m_InitParams.BucketId);
				m_TransformSlotIds[i] = 0xff;
			}
		}

		for(u32 i = 0; i < kMaxPointSlotIds; i++)
		{
			if(m_PointSlotIds[i] != 0xff)
			{
				sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_PointSlotIds[i]), m_InitParams.BucketId);
				m_PointSlotIds[i] = 0xff;
			}
		}
	}

#if !__SPU
	audParameterTransformSound::audParameterTransformSound()
	{
		for(u32 i = 0; i < kMaxTransformSlotIds; i++)
		{
			m_TransformSlotIds[i] = 0xff;
		}
		for(u32 i = 0; i < kMaxPointSlotIds; i++)
		{
			m_PointSlotIds[i] = 0xff;
		}
	}

	bool audParameterTransformSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		CompileTimeAssert(sizeof(PointType) == sizeof(float)*2);
		if(!audSound::Init(metadata, initParams, scratchInitParams))
			return false;

		ParameterTransformSound *soundData = (ParameterTransformSound*)GetMetadata();

		SetChildSound(0,SOUNDFACTORY.GetChildInstance(soundData->SoundRef, this, initParams, scratchInitParams, false));

		return InitSlots(soundData);
	}

	bool audParameterTransformSound::InitSlots(const ParameterTransformSound *soundData)
	{
		Assign(m_NumTransforms, soundData->numParameterTransforms);
		if(m_NumTransforms == 0)
		{
			return true;
		}

		const u32 numTransformsPerSlot = sm_Pool.GetSoundSlotSize() / sizeof(ParameterTransform);
		const u32 numTransformSlots = (u32)ceilf(m_NumTransforms / (float)numTransformsPerSlot);

		if(!audVerifyf(numTransformSlots <= kMaxTransformSlotIds, "%s exceeded max transforms (limit is %u, sound has %u)", GetName(), kMaxTransformSlotIds * numTransformsPerSlot, m_NumTransforms))
		{
			return false;
		}

		for(u32 i = 0; i < numTransformSlots; i++)
		{
			ParameterTransform *stateSlot = (ParameterTransform*)sm_Pool.AllocateSoundSlot(numTransformsPerSlot * sizeof(ParameterTransform), m_InitParams.BucketId, true);
			if(!stateSlot)
			{
				return false;
			}
			Assign(m_TransformSlotIds[i], sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, stateSlot));
		}

		u32 currentPointIndex = 0;
		u32 currentTransformSlot = 0;
		u32 slotRelativeTransformIndex = 0;
		ParameterTransform *stateSlot = GetTransformSlot(currentTransformSlot);

		const ParameterTransformSound::tParameterTransform *metadataPtr = &soundData->ParameterTransform[0];
		for(u32 transformId = 0; transformId < m_NumTransforms; transformId++)
		{
			if(slotRelativeTransformIndex >= numTransformsPerSlot)
			{
				currentTransformSlot++;
				slotRelativeTransformIndex = 0;
				stateSlot = GetTransformSlot(currentTransformSlot);				
			}
			stateSlot[slotRelativeTransformIndex].InputVariable = _FindVariableUpHierarchy(metadataPtr->InputVariable);
			if(!stateSlot[slotRelativeTransformIndex].InputVariable)
			{
				audWarningf("%s: Failed to find input variable %u (transform %u)", GetName(), metadataPtr->InputVariable, transformId);
				return false;
			}
 
			if(metadataPtr->InputRange.Min > metadataPtr->InputRange.Max)
			{
				// Min/max inverted
				stateSlot[slotRelativeTransformIndex].MaxInput = metadataPtr->InputRange.Min;
				stateSlot[slotRelativeTransformIndex].MinInput = metadataPtr->InputRange.Max;
			}
			else
			{
				stateSlot[slotRelativeTransformIndex].MaxInput = metadataPtr->InputRange.Max;
				stateSlot[slotRelativeTransformIndex].MinInput = metadataPtr->InputRange.Min;
			}

			const u32 numOutputs = metadataPtr->numOutputs;
			SoundAssert(numOutputs <= ParameterTransform::kMaxTransformOutputs);
			const ParameterTransformSound::tParameterTransform::tOutput *outputMetadataPtr = &metadataPtr->Output[0];

			for(u32 outputId = 0; outputId < numOutputs; outputId++)
			{
				ParameterTransform::ParameterTransformOutput &output = stateSlot[slotRelativeTransformIndex].Outputs[outputId];
				Assign(output.Destination, outputMetadataPtr->Destination);
				if(output.Destination == PARAM_DESTINATION_VARIABLE)
				{
					output.OutputVariable = _FindVariableUpHierarchy(outputMetadataPtr->OutputVariable);
					if(!audVerifyf(output.OutputVariable, "%s: Failed to find output variable %u (transform %u)", GetName(), outputMetadataPtr->OutputVariable, transformId))
					{
						return false;
					}
				}
				else
				{
					output.OutputVariable = NULL;
				}

				Assign(output.FirstPointIndex, currentPointIndex);
				Assign(output.NumPoints, outputMetadataPtr->numTransformPoints);

				output.MaxOutput = outputMetadataPtr->OutputRange.Max;
				output.MinOutput = outputMetadataPtr->OutputRange.Min;
				output.InputSmoothRate = outputMetadataPtr->InputSmoothRate;

				output.LastInputValue = 0.0f;
				output.LastOutputValue = 0.0f;

				currentPointIndex += outputMetadataPtr->numTransformPoints;			

				if(!CopyPoints(&outputMetadataPtr->TransformPoints[0], output.FirstPointIndex, output.NumPoints))
				{
					return false;
				}

				outputMetadataPtr = (const ParameterTransformSound::tParameterTransform::tOutput*)(&outputMetadataPtr->TransformPoints[outputMetadataPtr->numTransformPoints]);

			} // foreach output

			// Next transform starts at the end of the previous transform's final output
			metadataPtr = (const ParameterTransformSound::tParameterTransform*)outputMetadataPtr;

			slotRelativeTransformIndex++;
		}

		return true;
	}

	bool audParameterTransformSound::CopyPoints(const PointType *src, const u32 startIndex, const u32 numPoints)
	{
		const u32 numPointsPerSlot = sm_Pool.GetSoundSlotSize() / (sizeof(PointType));

		u32 numCopied = 0;

		while(numCopied < numPoints)
		{
			u32 currentSlotIndex = (startIndex + numCopied) / numPointsPerSlot;
			if(!audVerifyf(currentSlotIndex < kMaxPointSlotIds, "%s: Exceeded max cumulative transform points (%u, limit is %u)", GetName(), numPoints, kMaxPointSlotIds * numPointsPerSlot))
			{
				return false;
			}
			const u32 slotRelativeIndex = (startIndex + numCopied) - (currentSlotIndex * numPointsPerSlot);
			const u32 numPointsLeftInCurrentSlot = numPointsPerSlot - slotRelativeIndex;
			const u32 numPointsToCopyThisTime = Min(numPointsLeftInCurrentSlot, numPoints - numCopied);
			
			PointType *pointsSlot;
			if(m_PointSlotIds[currentSlotIndex] == 0xff)
			{
				pointsSlot
					= (PointType *)sm_Pool.AllocateSoundSlot(numPointsPerSlot * sizeof(PointType), m_InitParams.BucketId, true);
				if(!pointsSlot)
				{
					return false;
				}
				Assign(m_PointSlotIds[currentSlotIndex], sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, pointsSlot));
			}
			else
			{
				pointsSlot = const_cast<PointType*>(GetPointsSlot(currentSlotIndex));
			}
			
			sysMemCpy(pointsSlot + slotRelativeIndex, src + numCopied, numPointsToCopyThisTime * sizeof(PointType));
			numCopied += numPointsToCopyThisTime;
		}

		return true;
	}

#endif // !__SPU

	const PointType *audParameterTransformSound::GetPointsSlot(const u32 index) const
	{
		TrapEQ(m_PointSlotIds[index], 0xff);
		return (const PointType*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_PointSlotIds[index]);
	}

	audParameterTransformSound::ParameterTransform *audParameterTransformSound::GetTransformSlot(const u32 index)
	{
		TrapEQ(m_TransformSlotIds[index], 0xff);
		return (ParameterTransform*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_TransformSlotIds[index]);
	}

	void audParameterTransformSound::ProcessTransforms(const float timeStep, const bool isFirstTime, audSoundCombineBuffer &combineBuffer, s32 *predelay, s32 *startOffset)
	{
		if(m_NumTransforms == 0)
		{
			return;
		}
		const u32 numTransformsPerSlot = sm_Pool.GetSoundSlotSize() / sizeof(ParameterTransform);
		u32 slotRelativeIndex = 0;
		u32 currentSlotIndex = 0;
		ParameterTransform *transforms = GetTransformSlot(currentSlotIndex);

		for(u32 transformId = 0; transformId < m_NumTransforms; transformId++)
		{
			if(slotRelativeIndex >= numTransformsPerSlot)
			{
				slotRelativeIndex = 0;
				currentSlotIndex++;
				transforms = GetTransformSlot(currentSlotIndex);
			}
			ParameterTransform &transform = transforms[slotRelativeIndex];

			float inputValue = Clamp(AUD_GET_VARIABLE(transform.InputVariable), transform.MinInput, transform.MaxInput);

			for(u32 outputId = 0; outputId < ParameterTransform::kMaxTransformOutputs; outputId++)
			{
				ParameterTransform::ParameterTransformOutput &output = transform.Outputs[outputId];
				if(output.Destination < PARAMETERDESTINATIONS_MAX && output.NumPoints != 0)
				{	
					float outputValue = output.LastOutputValue;
					if(isFirstTime || inputValue != output.LastInputValue)
					{
						if(!isFirstTime)
						{					
							const float smoothRate = output.InputSmoothRate;
							const float inputRange = transform.MaxInput - transform.MinInput;
							const float maxDelta = (inputRange / smoothRate) * timeStep;
							const float lastInput = output.LastInputValue;

							const float smoothedInputIncreasing = Min(lastInput + maxDelta, inputValue);
							const float smoothedInputDecreasing = Max(lastInput - maxDelta, inputValue);
							// is input increasing or decreasing?
							const float smoothedInput = Selectf(inputValue - lastInput, smoothedInputIncreasing, smoothedInputDecreasing);
							// Use the raw input if smoothRate <= 0
							inputValue = Selectf(smoothRate - SMALL_FLOAT, smoothedInput, inputValue);
						}

						outputValue = ComputeTransform(inputValue, outputId, transform);
						// Note: store the smoothed input for comparison so that we recompute even if input is static but we've not 
						// reached it yet due to smoothing.
						output.LastInputValue = inputValue;
						output.LastOutputValue = outputValue;
					}

					switch(output.Destination)
					{
					case PARAM_DESTINATION_VOLUME:
						combineBuffer.Volume += audDriverUtil::ComputeDbVolumeFromLinear(outputValue);
						break;
					case PARAM_DESTINATION_PITCH:
						combineBuffer.Pitch += (s16)audDriverUtil::ConvertRatioToPitch(outputValue);
						break;
					case PARAM_DESTINATION_PAN:
						combineBuffer.Pan = (s16)CombinePan(combineBuffer.Pan, (s32)(outputValue * 360.f));
						break;
					case PARAM_DESTINATION_STARTOFFSET:
						if(startOffset)
						{
							*startOffset = (s32)(outputValue * 1000.f);
						}
						break;
					case PARAM_DESTINATION_PREDELAY:
						if(predelay)
						{
							*predelay = (s32)(outputValue * 1000.f);
						}
						break;
					case PARAM_DESTINATION_LPF:
						combineBuffer.LPFCutoff = Min(combineBuffer.LPFCutoff, (u16)(outputValue * kVoiceFilterLPFMaxCutoff));
						break;
					case PARAM_DESTINATION_HPF:
						combineBuffer.HPFCutoff = Max(combineBuffer.HPFCutoff, (u16)(outputValue * kVoiceFilterHPFMaxCutoff));
						break;
					case PARAM_DESTINATION_VARIABLE:
						SoundAssert(output.OutputVariable);
						AUD_SET_VARIABLE(output.OutputVariable, outputValue);
						break;
					default:
						SoundAssert(0);
						break;
					}
				}
			}

			slotRelativeIndex++;
		}
	}

	float audParameterTransformSound::ComputeTransform(const float inputValue, const u32 outputId, const ParameterTransform &transform) const
	{
		const float rescaledInput = Range(inputValue, transform.MinInput, transform.MaxInput);
		const float outputValue = ComputeCurve(rescaledInput, transform.Outputs[outputId].FirstPointIndex, transform.Outputs[outputId].NumPoints);
		return Lerp(outputValue, transform.Outputs[outputId].MinOutput, transform.Outputs[outputId].MaxOutput);
	}

	float audParameterTransformSound::ComputeCurve(const float inputValue, const u32 firstPointIndex, const u32 numPoints) const
	{
		const u32 numPointsPerSlot = sm_Pool.GetSoundSlotSize() / (sizeof(PointType));
		u32 currentSlotIndex = firstPointIndex / numPointsPerSlot;
		u32 slotRelativeIndex = firstPointIndex - (currentSlotIndex * numPointsPerSlot);
		
		const PointType *points = GetPointsSlot(currentSlotIndex);
		if(inputValue <= points[slotRelativeIndex].x)
		{
			return points[slotRelativeIndex].y;
		}
		
		// see which straight-line section we're in
		
		u32 i = 1;
		float lastX = points[slotRelativeIndex].x;
		float lastY = points[slotRelativeIndex].y;
		for (i = 1; i < numPoints; i++)
		{
			slotRelativeIndex++;
			if(slotRelativeIndex >= numPointsPerSlot)
			{
				slotRelativeIndex = 0;
				currentSlotIndex++;
				points = GetPointsSlot(currentSlotIndex);
			}

			if (points[slotRelativeIndex].x >= inputValue)
				break;

			lastX = points[slotRelativeIndex].x;
			lastY = points[slotRelativeIndex].y;

		}

		const float deltaX = Max(SMALL_FLOAT, points[slotRelativeIndex].x - lastX);
		const float linearRatio = (inputValue-lastX) / deltaX;		
		return Clamp(Lerp(linearRatio, lastY, points[slotRelativeIndex].y), 0.f, 1.f);
	}

	audPrepareState audParameterTransformSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
	{
		if(GetChildSound(0))
		{
			GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			return GetChildSound(0)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		}
		else
		{
			return AUD_PREPARED;
		}
	}

	void audParameterTransformSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		m_LastUpdateTime = timeInMs;

		s32 transformStartOffset = 0;
		s32 transformPredelay = 0;
		ProcessTransforms(0.f, true, combineBuffer, &transformPredelay, &transformStartOffset);

		audSound *childSound = GetChildSound(0);
		if (childSound)
		{
			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs) + transformPredelay);
			// Ripple down start offset, as we may have an audible sound below us that makes real sense of it.
			childSound->SetStartOffset((s32)GetPlaybackStartOffset() + transformStartOffset);
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}

	bool audParameterTransformSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
#if __BANK && !__SPU
		// See if our metadata has been edited, and reinitialise curves if it has		
		bool treatAsFirstTime = false;

		if(g_AudioEngine.GetRemoteControl().IsConnected())
		{
			const u32 nameHash = atStringHash(GetName());

			if(SOUNDFACTORY.IsSoundNameInEditedList(nameHash))
			{
				const ParameterTransformSound *soundData = (ParameterTransformSound*)GetMetadataForRAVEChanges();
				ReleaseSlots();
				if(!InitSlots(soundData))
				{
					audWarningf("ParameterTransformSound  %s failed to reinitialise after RAVE change; stopping", GetName());
					return false;
				}
				treatAsFirstTime = true;
			}
		}		
#else
		const bool treatAsFirstTime = false;
#endif

		const float timeStep = (timeInMs - m_LastUpdateTime) * 0.001f;
		m_LastUpdateTime = timeInMs;
		// If we have a child sound, we update that, and mirror its play state - if not, we report that we're finished.
		// (and DON'T apply our curve again - which is debatable, but fits with the way other variable sounds currently
		//  work, and is probably best.)
		if (GetChildSound(0))
		{
			ProcessTransforms(timeStep, treatAsFirstTime, combineBuffer);
			return GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
		}
		else
		{
			return false;
		}
	}

	void audParameterTransformSound::AudioKill()
	{
		if(GetChildSound(0))
		{
			GetChildSound(0)->_ManagedAudioKill();
		}
	}

	s32 audParameterTransformSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		if(GetChildSound(0))
		{
			return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			// We don't support lengths and start offsets on logical sounds with no children
			return kSoundLengthUnknown;
		}
	}

} // namespace rage
