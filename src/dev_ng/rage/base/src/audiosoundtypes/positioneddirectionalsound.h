//
// audioengine/positioneddirectionalsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_POSITIONEDDIRECTIONALSOUND_H
#define AUD_POSITIONEDDIRECTIONALSOUND_H

#include "sound.h"

#include "soundDefs.h"

#include "audioengine/widgets.h"

namespace rage {

// PURPOSE
//  Sets it's position based on a passed-in matrix and a 3d offset.
class audPositionedDirectionalSound : public audSound
{
public:
	audPositionedDirectionalSound();
	~audPositionedDirectionalSound();

	AUD_DECLARE_STATIC_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams);

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

//	void SetOffset(Vector3& offset);
	// this can only ever be called from the game thread, ie PPU, so is ok to be virtual
	void SetMatrix(Matrix34* matrix);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

protected:
	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState Prepare(audWaveSlot *);

	void GameThreadUpdate(u32 UNUSED_PARAM(timeInMs));

	Vector3	m_Offset;
	Matrix34* m_Matrix;

	audVolumeCone m_VolumeCone;

};

} // namespace rage

#endif // AUD_POSITIONEDDIRECTIONALSOUND_H
