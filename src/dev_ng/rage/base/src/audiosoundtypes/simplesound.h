//
// audioengine/simplesound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SIMPLE_SOUND_H
#define AUD_SIMPLE_SOUND_H

#include "sound.h"
#include "environmentsound.h"
#include "audiohardware/waveref.h"

namespace rage {
class audWavePlayer;
// PURPOSE
//  A simple sound has no child sounds, and plays a single wave, via a voice. It inherrits from VoicedSound,
//  to provide some of the Voice interaction.
class audSimpleSound : public audSound
{
public:

	AUD_DECLARE_STATIC_WRAPPERS;

	~audSimpleSound();

#if !__SPU
	audSimpleSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping);

	u16 GetBankId() const
	{
		return m_BankNameIndex;
	}

	audEnvironmentSound *GetEnvironmentSound() const
	{
		return (audEnvironmentSound*)GetChildSound(0);
	}

	u32 GetWaveNameHash() const
	{
		return m_WaveNameHash;
	}

	s32 GetWavePlayerId() const
	{
		return m_WavePlayerSlotId;
	}

	u32 GetCurrentPlayTimeOfWave(u32* mixerCalculationFrame = nullptr) const 
	{ 
		if (mixerCalculationFrame)
		{
			*mixerCalculationFrame = m_PlayTimeCalculationFrame;
		}

		return m_CurrentPlayTimeMs; 
	}

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

	// PURPOSE
	//	PPU Callback to set up wave reference / PCM generator
	void ChildSoundCallback(const u32 timeInMs, const u32 index);

private:

	bool SetupWavePlayer(audWaveSlot *slot);

	audWaveRef m_WaveReference;
	u32		m_WaveLengthMs;
	u32		m_CurrentPlayTimeMs;
	u32		m_PlayTimeCalculationFrame;
	u32		m_WaveNameHash;
	s16		m_LoadedSlotIndex;
	s16		m_SlotIndex;
	u16		m_BankNameIndex;
	s16		m_WavePlayerSlotId;	
	bool	m_IsLooping;
};

} // namespace rage

#endif // AUD_SIMPLE_SOUND_H
