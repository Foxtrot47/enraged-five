// 
// audiosoundtypes/pitchstepsound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "pitchstepsound.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audPitchStepSound);

audPitchStepSound::audPitchStepSound()
{

}

audPitchStepSound::~audPitchStepSound()
{
	if(!GetIsBeingRemovedFromHierarchy() && GetChildSound(0))
	{
		delete GetChildSound(0);
		SetChildSound(0, NULL);
	}
}

bool audPitchStepSound::Init(const void *metadata, const audSoundInternalInitParams* initParams)
{
#ifdef __AUD_SOUND_TASK
	Assert(0);
	return false;
#else
	if(!audSound::Init(metadata, initParams))
		return false;

	PitchStepSound *pitchStepSoundData = (PitchStepSound *)GetMetadata();
	SetChildSound(0, SOUNDFACTORY.GetChildInstanceFromOffset(pitchStepSoundData->SoundRef, this, initParams));
	if (!GetChildSound(0))
	{
		// we failed to get our child, so do nothing
		return false;
	}

	if((pitchStepSoundData->NumSteps > 1) && (pitchStepSoundData->StepSize > 0))
	{
		//Derive pitch for current step index.
		s16 steppedPitch = (pitchStepSoundData->StepIndex * pitchStepSoundData->StepSize) -
			((pitchStepSoundData->NumSteps - 1) * pitchStepSoundData->StepSize / 2);

		GetChildSound(0)->_SetRequestedInternalPitch(steppedPitch);

		pitchStepSoundData->StepIndex = (pitchStepSoundData->StepIndex + 1) % pitchStepSoundData->NumSteps;
	}

	return true;
#endif
}

void audPitchStepSound::AudioPlay(u32 timeInMs)
{
	if(GetChildSound(0))
	{
		GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		GetChildSound(0)->_ManagedAudioPlay(timeInMs);
		RemoveMyselfFromHierarchy();
	}
}

bool audPitchStepSound::AudioUpdate(u32 timeInMs)
{
	if(GetChildSound(0))
	{
		SoundAssert (GetChildSound(0)->_GetPlayState() == AUD_SOUND_PLAYING);
		if (GetChildSound(0)->_ManagedAudioUpdate(timeInMs))
		{
			return (true);
		}
		else
		{
			return (false);
		}
	}
	else
	{
		//If we don't have a sound, then we're done immediately.
		return false;
	}
}

void audPitchStepSound::AudioKill()
{
	if(GetChildSound(0))
	{
		GetChildSound(0)->_ManagedAudioKill();
	}
}

audPrepareState audPitchStepSound::Prepare(audWaveSlot *slot)
{
	if(GetChildSound(0))
	{
		GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		return GetChildSound(0)->ManagedPrepare(slot, GetCanDynamicallyLoad());
	}
	else
	{
		return AUD_PREPARED;
	}
}

s32 audPitchStepSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	if(GetChildSound(0))
	{
		return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
	}
	else
	{
		//We have no sound, so we have a length of 0ms.
		return 0;
	}
}

} // namespace rage
