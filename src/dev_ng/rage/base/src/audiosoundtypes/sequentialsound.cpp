//
// audioengine/sequentialsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "sequentialsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audioengine/soundmanager.h"
#include "diag/output.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audSequentialSound);

enum
{
	kOptimizedPlaybackBuffer = 2,
};
audSequentialSound::~audSequentialSound()
{
	for(int i = 0; i < audSound::kMaxChildren; i++)
	{
		if(GetChildSound(i))
		{
			DeleteChild(i);
		}	
	}
	if(m_SoundRefSlotIndex != 0xff)
	{
		sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_SoundRefSlotIndex), m_InitParams.BucketId);
		m_SoundRefSlotIndex = 0xff;
	}
}

#if !__SPU
audSequentialSound::audSequentialSound()
{
	m_CurrentPlayingSoundIndex = -1;
	m_SoundRefSlotIndex = 0xff;
	m_ChildIndexToPrepare = 0xff;
}

bool audSequentialSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
	{
		return false;
	}

	SequentialSound *sequentialSoundData = (SequentialSound*)GetMetadata();
	m_NumSoundRefs = sequentialSoundData->numSoundRefs;
	m_OptimizedPlayback = ((AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SEQUENTIALSOUND_OPTIMIZEPLAYBACK) == AUD_TRISTATE_TRUE) || (m_NumSoundRefs > audSound::kMaxChildren));
	if(m_OptimizedPlayback)
	{
		if(!audVerifyf(m_NumSoundRefs <= static_cast<s32>(sm_Pool.GetSoundSlotSize() / sizeof(u32)), "SequentialSound with too many children: limit is %" SIZETFMT "u, sound has %u", sm_Pool.GetSoundSlotSize() / sizeof(u32), m_NumSoundRefs))
		{		
			return false;
		}
		// need to copy our sound refs into an extra slot
		audMetadataRef *soundRefs = (audMetadataRef*)sm_Pool.AllocateSoundSlot(m_NumSoundRefs * sizeof(audMetadataRef), m_InitParams.BucketId, true);
		if(!soundRefs)
		{
			return false;
		}
		m_SoundRefSlotIndex = sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, soundRefs);
		
		// dont bother with reference name...
		for(s32 i = 0; i < m_NumSoundRefs; i++)
		{
			soundRefs[i] = sequentialSoundData->SoundRef[i].SoundId;
		}

		m_ScratchInitParams = *scratchInitParams;
	}

	SetHasDynamicChildren();

	audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;
	// store first 8 child sounds
	for(s32 i = 0; i < (m_OptimizedPlayback?kOptimizedPlaybackBuffer:m_NumSoundRefs); i++)
	{
		*scratchInitParams = scratchInitParamsCopy;
		SetChildSound(i,SOUNDFACTORY.GetChildInstance(sequentialSoundData->SoundRef[i].SoundId,this,initParams, scratchInitParams, false));
		if(!GetChildSound(i))
		{
			return false;
		}
	}

	return true;
}
#endif // !__SPU

audPrepareState audSequentialSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	bool prepared = true, failed = false;

	for(s32 i = 0; i < (m_OptimizedPlayback?kOptimizedPlaybackBuffer:m_NumSoundRefs); i++)
	{
		if(GetChildSound(i))
		{
			audPrepareState state = GetChildSound(i)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
			if(state == AUD_PREPARE_FAILED)
			{
				failed = true;
			}
			else if(state == AUD_PREPARING)
			{
				prepared = false;
			}			
		}
	}

	if(failed)
	{
		return AUD_PREPARE_FAILED;
	}
	else if(prepared)
	{
		return AUD_PREPARED;
	}
	else
	{
		return AUD_PREPARING;
	}
}

void audSequentialSound::ComputeStartingSoundIndexAndOffset(s32 &startingSoundIndex, u32 &startOffset)
{
	if(m_OptimizedPlayback)
	{
		startingSoundIndex = 0;
		startOffset = 0;
	}
	else
	{
		u32 overallStartOffset = GetPlaybackStartOffset();
		u32 subsoundTotalLength = 0;
		startOffset = 0;

		for(startingSoundIndex=0; startingSoundIndex<m_NumSoundRefs; startingSoundIndex++)
		{
			bool isChildLooping = false;
			s32 thisLength = GetChildSound(startingSoundIndex)->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
			if(thisLength == kSoundLengthUnknown)
			{
				//We can't offset because we've got a sound of unknown length.
				startingSoundIndex = 0;
				break;
			}
			subsoundTotalLength += thisLength;
			// We play this one if it goes to longer than our offset, or if it's looping.
			if(subsoundTotalLength >= overallStartOffset || isChildLooping)
			{
				//We need to start somewhere through this sound.
				startOffset = overallStartOffset - (subsoundTotalLength - thisLength);
				break;
			}
		}

		//Make sure startOffset isn't longer than our total.
		if(startingSoundIndex >= m_NumSoundRefs)
		{
			//We don't want to play anything.
			startingSoundIndex = -1;
		}
	}
}

void audSequentialSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	// Ignore start offset, as we're a logic sound, which can't be partially skipped.
	// This is tricky actually - if we really are logic, we want to ignore offset, because you don't want to 
	// skip a zero length variable sound - but equally, we could just be two simple sounds back to back, which
	// you'd want to be able to seek into. 

	s32 startingSoundIndex;
	u32 startOffset;
	ComputeStartingSoundIndexAndOffset(startingSoundIndex, startOffset);

	if(startingSoundIndex >= 0)
	{
		m_CurrentPlayingSoundIndex = startingSoundIndex;

		// may as well free up any sounds we've skipped over
		for(s32 i = 0; i < startingSoundIndex; i++)
		{
			DeleteChild(i);
			HandleFreeSlot(i);
		}

		audSound *startingSound = GetChildSound(startingSoundIndex);
		if(startingSound)
		{
			//Kick off the appropriate sound, and mark that that's the one we're playing.
			startingSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			startingSound->SetStartOffset(startOffset);
			startingSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}
}

bool audSequentialSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	if(m_ChildIndexToPrepare != 0xff)
	{
		SoundAssert(m_OptimizedPlayback);
		// optimized playback relies on the child being prepared immediately; bin it if its not good to go
		if(GetChildSound(m_ChildIndexToPrepare)->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority()) != AUD_PREPARED)
		{
			DeleteChild(m_ChildIndexToPrepare);
		}
		m_ChildIndexToPrepare = 0xff;
	}

	// Sanity check we're playing a valid one
	if ((m_CurrentPlayingSoundIndex<0) || (m_CurrentPlayingSoundIndex>=m_NumSoundRefs))
	{
		// only assert if we actually have sound references, a scripted sound without any will get
		// into this state.
		// We're now allowing -1 to mean don't play anything - needed for the case of a start offet longer than
		// all the sounds combined.
		//SoundAssert(GetNumSoundReferences()==0);
		return false;
	}

	// Update the one we're currently playing, and if it's finished, kick off the next.

	s32 actualIndex = (m_OptimizedPlayback?m_CurrentPlayingSoundIndex % kOptimizedPlaybackBuffer:m_CurrentPlayingSoundIndex);
	if(GetChildSound(actualIndex) && GetChildSound(actualIndex)->_ManagedAudioUpdate(timeInMs, combineBuffer))
	{
		// We're still playing our child sound, so we're still playing ourselves
		return true;
	}

	// don't proceed to creating new children if we've run out and are waiting for them to stop
	if(IsWaitingForChildrenToRelease())
	{
		return false;
	}
	
	// Our current child sound has finished, delete it once we're sure we're still playing
	s32 indexToDelete = actualIndex;

	// and kick off another if there is one, otherwise we've finished
	m_CurrentPlayingSoundIndex++;
	actualIndex = (m_OptimizedPlayback?m_CurrentPlayingSoundIndex % kOptimizedPlaybackBuffer:m_CurrentPlayingSoundIndex);
	if ((m_NumSoundRefs<=m_CurrentPlayingSoundIndex) || !GetChildSound(actualIndex))
	{
		// We're out of child sounds - so we're finished
		return false;
	}
	else
	{
		// Kick off the next one
		SoundAssert(GetChildSound(actualIndex));
		if(GetChildSound(actualIndex))
		{
			GetChildSound(actualIndex)->_ManagedAudioPlay(timeInMs, combineBuffer);
		}

		// delete the sound that just finished and create the next one thats going to start
		if(GetChildSound(indexToDelete))
		{
			DeleteChild(indexToDelete);
			HandleFreeSlot(indexToDelete);
		}
	}

	return true;
}

void audSequentialSound::AudioKill()
{
	// Sanity check we're playing a valid one
	if ((m_CurrentPlayingSoundIndex<0) || (m_CurrentPlayingSoundIndex>=m_NumSoundRefs))
	{
		//SoundAssert(0);
		return;
	}

	// Stop the one we're currently playing - only one at any one time, no cross-fades on scripted sounds
	s32 actualIndex = (m_OptimizedPlayback?m_CurrentPlayingSoundIndex % kOptimizedPlaybackBuffer:m_CurrentPlayingSoundIndex);
	GetChildSound(actualIndex)->_ManagedAudioKill();
}

s32 audSequentialSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	s32 totalLength = kSoundLengthUnknown;
	if(!m_OptimizedPlayback)
	{
		for(int i = 0; i < m_NumSoundRefs; i++)
		{
			if(GetChildSound(i))
			{
				bool isChildLooping = false;
				s32 childSoundLength = GetChildSound(i)->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
				if (isChildLooping && isLooping)
				{
					*isLooping = true;
				}
				if (childSoundLength == kSoundLengthUnknown)
				{
					return kSoundLengthUnknown;
				}
				else
				{
					totalLength += childSoundLength;
				}
			}
		}
	}
	return totalLength;
}

void audSequentialSound::HandleFreeSlot(s32 childIndex)
{
	if(m_OptimizedPlayback)
	{
		// childIndex has just been freed up - check to see if we have more sounds to instantiate
		// current playing index has been incremented by the time this is called now
		s32 nextChildToCreate = m_CurrentPlayingSoundIndex + kOptimizedPlaybackBuffer - 1;
		SoundAssert(nextChildToCreate > 0);
		if(nextChildToCreate > 0 && nextChildToCreate < m_NumSoundRefs)
		{
			audMetadataRef *soundRefs = static_cast<audMetadataRef*>(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_SoundRefSlotIndex));
			RequestChildSoundFromOffset(soundRefs[nextChildToCreate], childIndex, &m_ScratchInitParams);
		}
	}
}

void audSequentialSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 index)
{
	if(GetChildSound(index))
	{
		SoundAssert(m_ChildIndexToPrepare == 0xff);
		Assign(m_ChildIndexToPrepare, index); 
	}
}

} // namespace rage
