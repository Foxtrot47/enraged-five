//
// audiosoundtypes/streamingsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_STREAMING_SOUND_H
#define AUD_STREAMING_SOUND_H

#include "sound.h"
#include "atl/array.h"

namespace rage {

class audStreamingWaveSlot;

//
// PURPOSE
//  A streaming sound simultaneously plays all of its children and manages the loading of a streaming Wave Bank (via its
//	Wave slot.)
//
class audStreamingSound : public audSound
{
public:
	
	~audStreamingSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audStreamingSound();
	//
	// PURPOSE
	//  Implements base class function for this sound.
	//
	bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::AudioPlay()
	//
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::AudioUpdate()
	//
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::AudioKill()
	//
	void AudioKill();
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::ComputeDurationMsExcludingStartOffsetAndPredelay()
	//
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);
	//
	// PURPOSE
	//  Returns the current play time of the streaming Wave (Bank).
	// PARAMS
	//	timeInMs	- The current audio system time, in milliseconds.
	// RETURNS
	//	The current play time of the streaming Wave (Bank), or AUD_SOUND_LENGTH_UNKNOWN if a problem occurred.
	//
	u32 GetCurrentPlayTimeOfWave()
	{
		return m_CurrentPlayTime;
	}

	u32 GetPlayTimeCalculationMixerFrame()
	{
		return m_PlayTimeCalculationFrame;
	}

	//
	// PURPOSE
	//	Returns a pointer to the local requested setting associated with the specified child sound.
	// PARAMS
	//	soundIndex	- The index of the child sound for which the pointer to the local req setting is to be returned.
	// RETURNS
	//	A pointer to the local req setting associated with the specified child sound.
	// NOTES
	//  These local requested settings are committed within the overridden CommitRequestedSettings().
	//
	audRequestedSettings *GetRequestedSettingsForChild(u32 soundIndex);

#ifdef AUD_IMPL
	void Pause(const u32 timeInMs)
	{
		BasePause(timeInMs);
	}
#endif

	void ActionReleaseRequest(const u32 timeInMs)
	{
		BaseActionReleaseRequest(timeInMs);
	} 	

	u32 GetStreamingWaveBankIndex() const
	{
		return m_StreamingWaveBankIndex;
	}

	// PURPOSE
	//	Expose the first wave player from the underlying hierarchy
	s32 GetFirstWavePlayerId() const { return GetWavePlayerId(0); }

	s32 GetWavePlayerIdCount() const { return m_ChildWavePlayerIds.GetCount(); }
	s32 GetWavePlayerId(const s32 index) const { return m_ChildWavePlayerIds[index]; }
	
	//
	// PURPOSE
	//  Implements base class function for this sound.
	// SEE ALSO
	//	audSound::Prepare()
	//
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

	void AdjustStartOffset(s32 startOffsetDelta);

private:

#if !__SPU
	//
	// PURPOSE
	//  Create all child sounds from metadata.
	// RETURNS
	//	True if all children were created successfully when not deferred.
	//
	bool CreateChildren(audSoundScratchInitParams *scratchInitParams, const StreamingSound *metadata);
#endif

	bool UpdateChildren(u32 timeInMs, audSoundCombineBuffer &combineBuffer);


	//
	// PURPOSE
	//  Prepares all of the child sounds, one for each Wave within the loaded streaming Wave Bank.
	// PARAMS
	//	startOffsetMs - The playback start offset, in milliseconds, to be requested of the children.
	// RETURNS
	//	Returns the overall prepare state of all the child sounds.
	// 
	audPrepareState InitChildSounds(u32 startOffsetMs);
	//
	// PURPOSE
	//  Calculates the current play time of the streaming Wave (Bank). This is derived by taking the earliest play time
	//	reported by the child sounds.
	// RETURNS
	//	The current play time of the streaming Wave (Bank), or AUD_SOUND_LENGTH_UNKNOWN if a problem occurred.
	//
	s32 ComputeCurrentPlayTimeOfWave(u32* mixerCalculationFrame = nullptr);

	audStreamingWaveSlot *GetStreamingWaveSlot()
	{
		return (audStreamingWaveSlot*)GetWaveSlot();
	}

	atRangeArray<u8, audSound::kMaxChildren> m_RequestedSettingsIndicesForChildSounds;
	enum {kMaxChildWavePlayerIds = 16};
	atFixedArray<s16, kMaxChildWavePlayerIds> m_ChildWavePlayerIds;
	u32 m_CurrentPlayTime;
	u32 m_PlayTimeCalculationFrame;
	u32 m_StartOffsetToPlayMs;
	
	u32 m_NumSoundRefs;
	u32 m_Duration;

	u32 m_SyncId;
		
	u16 m_StreamingWaveBankIndex;

	bool m_HaveReleasedChildren;
	bool m_HaveStartedPreparing;
	bool m_ShouldStopWhenChildStops;
	bool m_IsLooping;
};

} // namespace rage

#endif // AUD_STREAMING_SOUND_H
