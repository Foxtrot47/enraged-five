//
// audioengine/twinloopsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "twinloopsound.h"

#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"

#include "audiohardware/driverdefs.h"
#include "audiohardware/driverutil.h"

namespace rage
{

AUD_IMPLEMENT_STATIC_WRAPPERS(audTwinLoopSound);

audTwinLoopSound::~audTwinLoopSound()
{
	if(GetChildSound(0))
	{
		DeleteChild(0);
	}
	if(GetChildSound(1))
	{
		DeleteChild(1);
	}
}

#if !__SPU
bool audTwinLoopSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	TwinLoopSound *twinLoopSoundData = (TwinLoopSound*)GetMetadata();

	audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;
	
	SetChildSound(0, SOUNDFACTORY.GetChildInstance(twinLoopSoundData->SoundRef[0].SoundId, this, initParams, scratchInitParams, false));
	SetChildSound(1, SOUNDFACTORY.GetChildInstance(twinLoopSoundData->SoundRef[1].SoundId, this, initParams, &scratchInitParamsCopy, false));

	if(!GetChildSound(0) || !GetChildSound(1))
	{
		return false;
	}

	m_MinCrossfadeTime = twinLoopSoundData->MinCrossfadeTime;
	m_MaxCrossfadeTime = twinLoopSoundData->MaxCrossfadeTime;
	m_MinSwapTime = twinLoopSoundData->MinSwapTime;
	m_MaxSwapTime = twinLoopSoundData->MaxSwapTime;
 
	m_MinSwapTimeVariable = _FindVariableUpHierarchy(twinLoopSoundData->MinSwapTimeVariable);
	m_MaxSwapTimeVariable = _FindVariableUpHierarchy(twinLoopSoundData->MaxSwapTimeVariable);
	m_MinCrossfadeTimeVariable = _FindVariableUpHierarchy(twinLoopSoundData->MinCrossfadeTimeVariable);
	m_MaxCrossfadeTimeVariable = _FindVariableUpHierarchy(twinLoopSoundData->MaxCrossfadeTimeVariable);

	m_CrossfadeSmoother.Init(1.0f, true);
	f32 smoothRate = GetNewSmoothRate();
	m_CrossfadeSmoother.SetRate(smoothRate);
	static const u32 equalPowerRise = 1011033664u;//("EQUAL_POWER_RISE");
	static const u32 linearFall = 2494450397u;//("LINEAR_FALL");
	if(twinLoopSoundData->CrossfadeCurve == equalPowerRise)
	{
		m_CrossfadeType = AUD_TWINLOOP_XFADE_EQUAL_POWER;
	}
	else if(twinLoopSoundData->CrossfadeCurve == linearFall)
	{
		m_CrossfadeType = AUD_TWINLOOP_XFADE_LINEAR;
	}
	else
	{
		SoundAssertMsg(false, "Unsupported crossfade curve - please choose EQUAL_POWER_RISE or LINEAR_FALL");
		return false;
	}

	m_IgnoreStoppedChild = AUD_GET_TRISTATE_VALUE(GetBaseMetadata()->Flags, FLAG_ID_TWINLOOPSOUND_IGNORESTOPPEDCHILD) == AUD_TRISTATE_TRUE;

	return true;
}
#endif // !__SPU

f32 audTwinLoopSound::GetNewSmoothRate()
{
	f32 minCrossfadeTime = (f32)m_MinCrossfadeTime;
	if (m_MinCrossfadeTimeVariable)
	{
		minCrossfadeTime += (AUD_GET_VARIABLE(m_MinCrossfadeTimeVariable));
	}
	f32 maxCrossfadeTime = (f32)m_MaxCrossfadeTime;
	if (m_MaxCrossfadeTimeVariable)
	{
		maxCrossfadeTime += (AUD_GET_VARIABLE(m_MaxCrossfadeTimeVariable));
	}

	f32 crossfadeTime = audEngineUtil::GetRandomNumberInRange(minCrossfadeTime, maxCrossfadeTime);

	// We want to turn this time, in milliseconds, into a smoothing rate, which is a max change per millisecond.
	// Check it's not essentially zero.
	f32 smoothRate = 1.0f;
	if (crossfadeTime > 1.0f)
	{
		smoothRate = 1.0f/crossfadeTime;
	}

	return smoothRate;
}

u32 audTwinLoopSound::GetNewSwapTime(const u32 timeInMs)
{
	u32 minSwapTime = m_MinSwapTime;
	if (m_MinSwapTimeVariable)
	{
		minSwapTime += (u16)(AUD_GET_VARIABLE(m_MinSwapTimeVariable));
	}
	u32 maxSwapTime = m_MaxSwapTime;
	if (m_MaxSwapTimeVariable)
	{
		maxSwapTime += (u16)(AUD_GET_VARIABLE(m_MaxSwapTimeVariable));
	} 
	s32 s32minSwapTime = (s32) minSwapTime;
	s32 s32maxSwapTime = (s32) maxSwapTime;
	return timeInMs + audEngineUtil::GetRandomNumberInRange(s32minSwapTime, s32maxSwapTime);
}

void audTwinLoopSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	SoundAssert(childSound0);
	SoundAssert(childSound1);

	m_Crossfade = audEngineUtil::ResolveProbability(0.5f) ? 1.0f : 0.f;
	f32 crossfade = m_CrossfadeSmoother.CalculateValue(m_Crossfade, timeInMs);
	f32 linVol1 = 0.f, linVol2 = 0.f;
	if(m_CrossfadeType == AUD_TWINLOOP_XFADE_EQUAL_POWER)
	{
		const float PI_over_2 = (PI / 2.0f);
		linVol1 = Sinf(crossfade * PI_over_2);
		linVol2 = Sinf((1.f - crossfade) * PI_over_2);
	}
	else
	{
		linVol1 = crossfade;
		linVol2 = 1.f - crossfade;
	}
	const f32 firstVolume = audDriverUtil::ComputeDbVolumeFromLinear(linVol1) + combineBuffer.Volume;
	const f32 secondVolume = audDriverUtil::ComputeDbVolumeFromLinear(linVol2) + combineBuffer.Volume;

	childSound0->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
	childSound0->SetStartOffset((s32)GetPlaybackStartOffset());
	combineBuffer.Volume = firstVolume;
	childSound0->_ManagedAudioPlay(timeInMs, combineBuffer);

	childSound1->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
	childSound1->SetStartOffset((s32)GetPlaybackStartOffset());
	combineBuffer.Volume = secondVolume;
	childSound1->_ManagedAudioPlay(timeInMs, combineBuffer);

	m_SwapTime = GetNewSwapTime(timeInMs);
}

bool audTwinLoopSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	if(timeInMs > m_SwapTime)
	{
		SwapSounds(timeInMs);
	}

	const f32 crossfade = m_CrossfadeSmoother.CalculateValue(m_Crossfade, timeInMs);
	f32 linVol1 = 0.f, linVol2 = 0.f;
	if(m_CrossfadeType == AUD_TWINLOOP_XFADE_EQUAL_POWER)
	{
		const float PI_over_2 = (PI / 2.0f);
		linVol1 = Sinf(crossfade * PI_over_2);
		linVol2 = Sinf((1.f - crossfade) * PI_over_2);
	}
	else
	{
		linVol1 = crossfade;
		linVol2 = 1.f - crossfade;
	}
	const f32 firstVolume = audDriverUtil::ComputeDbVolumeFromLinear(linVol1) + combineBuffer.Volume;
	const f32 secondVolume = audDriverUtil::ComputeDbVolumeFromLinear(linVol2) + combineBuffer.Volume;

	int validUpdateNum = 0;
	if(childSound0 && childSound0->GetPlayState() == AUD_SOUND_PLAYING)
	{
		combineBuffer.Volume = firstVolume;
		validUpdateNum += childSound0->_ManagedAudioUpdate(timeInMs, combineBuffer);
	}
#if __ASSERT
	else if (!m_IgnoreStoppedChild)
	{
		SoundAssertMsg(0, "[audTwinLoopSound] Child 0 not playing");
	}
#endif

	if(childSound1 && childSound1->GetPlayState() == AUD_SOUND_PLAYING)
	{
		combineBuffer.Volume = secondVolume;
		validUpdateNum += childSound1->_ManagedAudioUpdate(timeInMs, combineBuffer);
	}
#if __ASSERT
	else if (!m_IgnoreStoppedChild)
	{
		SoundAssertMsg(0, "[audTwinLoopSound] Child 1 not playing");
	}
#endif

	switch (validUpdateNum)
	{
	case 0: return false;
	case 1: return m_IgnoreStoppedChild;
	case 2: return true;
	}
	return false;
}

void audTwinLoopSound::SwapSounds(u32 timeInMs)
{
	// Get a new smooth rate
	f32 newSmoothRate = GetNewSmoothRate();
	m_CrossfadeSmoother.SetRate(newSmoothRate);

	// switch which one we're playing - volume changes now happen every frame as part of AudioUpdate()
	m_Crossfade = 1.0f - m_Crossfade;

	m_SwapTime = GetNewSwapTime(timeInMs);
}

void audTwinLoopSound::AudioKill()
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	SoundAssert(childSound0);
	childSound0->_ManagedAudioKill();
	SoundAssert(childSound1);
	childSound1->_ManagedAudioKill();
}

audPrepareState audTwinLoopSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	SoundAssert(childSound0);
	SoundAssert(childSound1);
	
	// Why does this only set a start offset on child 0?  Doesn't really matter, but seems very arbitrary
	childSound0->SetStartOffset((s32)GetPlaybackStartOffset());

	audPrepareState state1 = childSound0->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
	audPrepareState state2 = childSound1->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
	

	if(state1 == AUD_PREPARE_FAILED || state2 == AUD_PREPARE_FAILED)
	{
		return AUD_PREPARE_FAILED;
	}
	else if(state1 == AUD_PREPARING || state2 == AUD_PREPARING)
	{
		return AUD_PREPARING;
	}
	else
	{
		return AUD_PREPARED;
	}
}

int audTwinLoopSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	// TODO:  figure out what this should reasonably return
	int dur1, dur2;

	SoundAssert(childSound0);
	SoundAssert(childSound1);

	bool isChild1Looping = false, isChild2Looping = false;

	dur1 = childSound0->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChild1Looping);
	dur2 = childSound1->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChild2Looping);

	if (isLooping && (isChild1Looping || isChild2Looping))
	{
		*isLooping = true;
	}

	return (dur1 > dur2 ? dur1 : dur2);
}

} // namespace rage


