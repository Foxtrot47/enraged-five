// 
// audiosoundtypes/granularsound.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "granularsound.h"

#include "environmentsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "audiohardware/grainplayer.h"
#include "audiohardware/pcmsource_interface.h"

namespace rage 
{
	#define VerifyValidGrainPlayer() if(m_GrainPlayerId<0){SoundAssertf(false, "Invalid GrainPlayerId", GetName()); return;}

	AUD_IMPLEMENT_STATIC_WRAPPERS(audGranularSound);

	audGranularSound::~audGranularSound()
	{
		for(u32 loop = 0; loop < kMaxChannelInitDataSlots; loop++)
		{
			if(m_ChannelInitDataSlotIds[loop] != 0xff)
			{
				audSound *slot = (audSound *)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_ChannelInitDataSlotIds[loop]);
				sm_Pool.DeleteSound(slot, m_InitParams.BucketId);
				m_ChannelInitDataSlotIds[loop] = 0xff;
			}
		}

		if (!IsBeingRemovedFromHierarchy() && GetChildSound(0))
		{
			DeleteChild(0);
		}

		if(m_GrainPlayerId != -1)
		{
			audPcmSourceInterface::Release(m_GrainPlayerId);
			m_GrainPlayerId = -1;
		}
	}

#if !__SPU

	audGranularSound::audGranularSound()
	{
		m_SlotIndex = -1;
		m_StaticSlotIndex = -1;
		m_GrainPlayerId = -1;
		m_NumChannels = 0;
		m_GrainPlayerInitialised = false;
		m_Initialised = false;
		m_Quality = audGranularMix::GrainPlayerQualityMax;

		for(u32 i = 0; i < kMaxChannelInitDataSlots; i++)
		{
			m_ChannelInitDataSlotIds[i] = 0xff;
		}
	}

	bool audGranularSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
		{
			return false;
		}

		GranularSound *soundMetadata = (GranularSound*)GetMetadata();

		SetChildSound(0,SOUNDFACTORY.GetEnvironmentSound(this, initParams, scratchInitParams));
		if(!GetChildSound(0))
		{
			return false;
		}

		const GranularSound* parentMetadata = SOUNDFACTORY.DecompressMetadata<GranularSound>(soundMetadata->ParentSound);

		if(!parentMetadata)
		{
			parentMetadata = soundMetadata;
		}

		// note: when running -rave bank references are stored as hashes in metadata, so we need to resolve them
		// at runtime.
		Assign(m_BankNameIndex, SOUNDFACTORY.GetBankIndexFromMetadataRef(soundMetadata->Channel0.BankName));

		const u32 numInitPacketsPerSlot = sm_Pool.GetSoundSlotSize() / sizeof(audGrainChannelInitData);
		const u32 numInitPacketSlots = (u32)ceilf(audGrainPlayer::kMaxGranularMixersPerPlayer / (float)numInitPacketsPerSlot);

		if(!audVerifyf(numInitPacketSlots < kMaxChannelInitDataSlots, "%s exceeded max init packet slots (limit is %u, sound has %u)", GetName(), kMaxChannelInitDataSlots * numInitPacketsPerSlot, audGrainPlayer::kMaxGranularMixersPerPlayer))
		{
			return false;
		}

		for(u32 i = 0; i < numInitPacketSlots; i++)
		{
			audGrainChannelInitData* channelInitData = (audGrainChannelInitData*)sm_Pool.AllocateSoundSlot(Min(numInitPacketsPerSlot, (u32)audGrainPlayer::kMaxGranularMixersPerPlayer) * sizeof(audGrainChannelInitData), m_InitParams.BucketId, true);
			if(!channelInitData)
			{
				return false;
			}

			Assign(m_ChannelInitDataSlotIds[i], sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, channelInitData));
			SoundAssert(m_ChannelInitDataSlotIds[i] != 0xff);
		}

		m_PrimaryWaveNameHash = soundMetadata->Channel0.WaveName;

		audGrainChannelInitData* initData = GetChannelInitData(0);
		initData->waveNameHash = soundMetadata->Channel0.WaveName;
		initData->outputChannel = parentMetadata->ChannelSettings0.OutputBuffer;
		audAssertf(parentMetadata->ChannelSettings0.GranularClockIndex < 4, "Max 4 clocks due to bitfield");
		initData->granularClock = parentMetadata->ChannelSettings0.GranularClockIndex;
		initData->matchMinPitch = parentMetadata->ChannelSettings0.StretchToMinPitch0 == 1;
		initData->matchMaxPitch = parentMetadata->ChannelSettings0.StretchToMaxPitch0 == 1;
		initData->maxLoopProportion = parentMetadata->ChannelSettings0.MaxLoopProportion;
		initData->volumeScale = parentMetadata->Channel0_Volume;
		initData->initialVolume = 0;
		initData->allowLoopGrainOverlap = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL0) == AUD_TRISTATE_TRUE);

		initData = GetChannelInitData(1);
		initData->waveNameHash = soundMetadata->Channel1.WaveName;
		initData->outputChannel = parentMetadata->ChannelSettings1.OutputBuffer;
		audAssertf(parentMetadata->ChannelSettings1.GranularClockIndex < 4, "Max 4 clocks due to bitfield");
		initData->granularClock = parentMetadata->ChannelSettings1.GranularClockIndex;
		initData->matchMinPitch = parentMetadata->ChannelSettings1.StretchToMinPitch1 == 1;
		initData->matchMaxPitch = parentMetadata->ChannelSettings1.StretchToMaxPitch1 == 1;
		initData->maxLoopProportion = parentMetadata->ChannelSettings1.MaxLoopProportion;
		initData->volumeScale = parentMetadata->Channel1_Volume;
		initData->initialVolume = 0;
		initData->allowLoopGrainOverlap = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL1) == AUD_TRISTATE_TRUE);
		
		initData = GetChannelInitData(2);
		initData->waveNameHash = soundMetadata->Channel2.WaveName;
		initData->outputChannel = parentMetadata->ChannelSettings2.OutputBuffer;
		audAssertf(parentMetadata->ChannelSettings2.GranularClockIndex < 4, "Max 4 clocks due to bitfield");
		initData->granularClock = parentMetadata->ChannelSettings2.GranularClockIndex;
		initData->matchMinPitch = parentMetadata->ChannelSettings2.StretchToMinPitch2 == 1;
		initData->matchMaxPitch = parentMetadata->ChannelSettings2.StretchToMaxPitch2 == 1;
		initData->maxLoopProportion = parentMetadata->ChannelSettings2.MaxLoopProportion;
		initData->volumeScale = parentMetadata->Channel2_Volume;
		initData->initialVolume = 0;
		initData->allowLoopGrainOverlap = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL2) == AUD_TRISTATE_TRUE);

		initData = GetChannelInitData(3);
		initData->waveNameHash = soundMetadata->Channel3.WaveName;
		initData->outputChannel = parentMetadata->ChannelSettings3.OutputBuffer;
		audAssertf(parentMetadata->ChannelSettings3.GranularClockIndex < 4, "Max 4 clocks due to bitfield");
		initData->granularClock = parentMetadata->ChannelSettings3.GranularClockIndex;
		initData->matchMinPitch = parentMetadata->ChannelSettings3.StretchToMinPitch3 == 1;
		initData->matchMaxPitch = parentMetadata->ChannelSettings3.StretchToMaxPitch3 == 1;
		initData->maxLoopProportion = parentMetadata->ChannelSettings3.MaxLoopProportion;
		initData->volumeScale = parentMetadata->Channel3_Volume;
		initData->initialVolume = 0;
		initData->allowLoopGrainOverlap = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL3) == AUD_TRISTATE_TRUE);

		initData = GetChannelInitData(4);
		initData->waveNameHash = soundMetadata->Channel4.WaveName;
		initData->outputChannel = parentMetadata->ChannelSettings4.OutputBuffer;
		audAssertf(parentMetadata->ChannelSettings4.GranularClockIndex < 4, "Max 4 clocks due to bitfield");
		initData->granularClock = parentMetadata->ChannelSettings4.GranularClockIndex;
		initData->matchMinPitch = parentMetadata->ChannelSettings4.StretchToMinPitch4 == 1;
		initData->matchMaxPitch = parentMetadata->ChannelSettings4.StretchToMaxPitch4 == 1;
		initData->maxLoopProportion = parentMetadata->ChannelSettings4.MaxLoopProportion;
		initData->volumeScale = parentMetadata->Channel4_Volume;
		initData->initialVolume = 0;
		initData->allowLoopGrainOverlap = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL4) == AUD_TRISTATE_TRUE);

		initData = GetChannelInitData(5);
		initData->waveNameHash = soundMetadata->Channel5.WaveName;
		initData->outputChannel = parentMetadata->ChannelSettings5.OutputBuffer;
		audAssertf(parentMetadata->ChannelSettings5.GranularClockIndex < 4, "Max 4 clocks due to bitfield");
		initData->granularClock = parentMetadata->ChannelSettings5.GranularClockIndex;
		initData->matchMinPitch = parentMetadata->ChannelSettings5.StretchToMinPitch5 == 1;
		initData->matchMaxPitch = parentMetadata->ChannelSettings5.StretchToMaxPitch5 == 1;
		initData->maxLoopProportion = parentMetadata->ChannelSettings5.MaxLoopProportion;
		initData->volumeScale = parentMetadata->Channel5_Volume;
		initData->initialVolume = 0;
		initData->allowLoopGrainOverlap = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL5) == AUD_TRISTATE_TRUE);

		m_NumChannels = 0;
		for(u32 loop = 0; loop < audGrainPlayer::kMaxGranularMixersPerPlayer; loop++)
		{
			m_ChannelMuted.Set(loop, false);
			initData = GetChannelInitData(loop);

			if(initData->waveNameHash != 0)
			{
				m_ChannelValid.Set(loop, true);
				m_NumChannels++;
			}
			else
			{
				m_ChannelValid.Set(loop, false);
			}
		}

		for(u8 loop = 0; loop < parentMetadata->numGranularClocks; loop++)
		{
			m_GranularClocks.Push(parentMetadata->GranularClock[loop]);
		}

		audAssertf(parentMetadata->numGranularClocks > 0, "Bank %s - Granular sound not configured with any pitch clock min/max hertz values. Use RAG granular debug view to show suggested values", SOUNDFACTORY.GetBankNameFromIndex(m_BankNameIndex));

		m_LoopRandomisationEnabled = AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_GRANULARSOUND_LOOPRANDOMISATIONENABLED) != AUD_TRISTATE_FALSE;
		m_LoopRandomisationChangeRate = parentMetadata->LoopRandomisationChangeRate;
		m_LoopRandomisationMaxPitchFraction = parentMetadata->LoopRandomisationPitchFraction;
		m_InitialX = 0.0f;
		m_PrevXValue = 0.0f;

		if((soundMetadata->Channel1.WaveName != 0 && soundMetadata->Channel1.BankName != soundMetadata->Channel0.BankName) ||
		   (soundMetadata->Channel2.WaveName != 0 && soundMetadata->Channel2.BankName != soundMetadata->Channel0.BankName) ||
		   (soundMetadata->Channel3.WaveName != 0 && soundMetadata->Channel3.BankName != soundMetadata->Channel0.BankName) ||
		   (soundMetadata->Channel4.WaveName != 0 && soundMetadata->Channel4.BankName != soundMetadata->Channel0.BankName) ||
		   (soundMetadata->Channel5.WaveName != 0 && soundMetadata->Channel5.BankName != soundMetadata->Channel0.BankName))
		{
			audAssertf(false, "WaveRefs for granular sound channels must all share the same bank!");
			return false;
		}

		// cache the static slot ptr in metadata to save looking up everytime
		if(soundMetadata->WaveSlotPtr != ~0U)
		{
			Assign(m_StaticSlotIndex, audWaveSlot::GetWaveSlotIndex((audWaveSlot*)((size_t)soundMetadata->WaveSlotPtr)));
			if(m_StaticSlotIndex == -1)
			{
				audWaveSlot* waveSlot = audWaveSlot::FindLoadedBankWaveSlot(m_BankNameIndex);

				if(waveSlot && waveSlot->IsStatic())
				{
					s32 waveSlotIndex = audWaveSlot::GetWaveSlotIndex(waveSlot);
					Assign(m_StaticSlotIndex, waveSlotIndex);
					soundMetadata->WaveSlotPtr = (u32)(size_t)(waveSlot);
				}
				else
				{
					soundMetadata->WaveSlotPtr = ~0U;
				}
			}
		}

		if(m_StaticSlotIndex != -1)
		{
			if(audWaveSlot::GetWaveSlotFromIndex(m_StaticSlotIndex)->GetContainer().FindObject(m_PrimaryWaveNameHash) == adatContainer::InvalidId)
			{
				SetPrepareFailed();
			}
		}

		m_Initialised = true;
		return true;
	}
#endif // !__SPU

	audPrepareState audGranularSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
	{
		const s32 slotIndex = (m_StaticSlotIndex!=-1?m_StaticSlotIndex:audWaveSlot::GetWaveSlotIndex(slot));
		if(slotIndex == -1)
		{
			audWarningf("Host wave slot not found for granular sound - %s\n", GetName());
			return AUD_PREPARE_FAILED;
		}

		audEnvironmentSound *envSound = (audEnvironmentSound*)GetChildSound(0);
		if (!envSound)
		{
			return AUD_PREPARE_FAILED;
		}

		audWaveSlot *slotPtr = audWaveSlot::GetWaveSlotFromIndex(slotIndex);
		SoundAssert(slotPtr);
		const audWaveSlot::audWaveSlotLoadStatus status = slotPtr->GetAssetLoadingStatus(m_BankNameIndex, m_PrimaryWaveNameHash);
		if(checkStateOnly)
		{
			switch(status)
			{
			case audWaveSlot::LOADED:
				if(m_GrainPlayerInitialised)
				{
					return AUD_PREPARED;
				}
				else
				{
					return AUD_PREPARING;
				}
			case audWaveSlot::FAILED:
				return AUD_PREPARE_FAILED;
			default:
				return AUD_PREPARING;
			}
		}
		else
		{

			// allocate a grain player
			if(m_GrainPlayerId == -1)
			{
				Assign(m_GrainPlayerId, audPcmSourceInterface::Allocate(AUD_PCMSOURCE_GRAINPLAYER));
				if(m_GrainPlayerId == -1)
				{
					// failed to allocate a wave player
					return AUD_PREPARE_FAILED;
				}

				envSound->InitEnvironment(m_GrainPlayerId, 0.f, kSoundLengthUnknown, false, kDefaultPeakLevel_u16, (u32)(45.0f * (1.0f + (1.5f * m_PrevXValue))));
			}

			Assign(m_SlotIndex, slotIndex);

			switch(status)
			{
			case audWaveSlot::LOADED:
				{

					// If we're on the ppu, grab our wave headroom and length, 
					// if we're on spu, set up a callback, and claim we're still not prepared
#if __SPU
					if(!m_GrainPlayerInitialised)
					{
						// We initialise headroom to an invalid setting, so that we can tell if we need to defer the wave data 
						ASSERT_ONLY(audEnvironmentSound* environmentSound = (audEnvironmentSound*)GetChildSound(0));
						Assert(environmentSound && "No environment sound");

						RequestPPUCallback();
						return AUD_PREPARING;				
					}
					else
					{
						return AUD_PREPARED;
					}
#else

					audWaveSlot* waveSlot = audWaveSlot::GetWaveSlotFromIndex(slotIndex);

					if(waveSlot)
					{
						if (!waveSlot || waveSlot->GetContainer().FindObject(m_PrimaryWaveNameHash) == adatContainer::InvalidId)
						{
							return AUD_PREPARE_FAILED;
						}	
					}
					
					ASSERT_ONLY(audEnvironmentSound* environmentSound = (audEnvironmentSound*)GetChildSound(0));
					SoundAssert(environmentSound);
					InitGrainPlayer();
#endif

				}
				return AUD_PREPARED;
			case audWaveSlot::LOADING:
				return AUD_PREPARING;
			case audWaveSlot::FAILED:
				return AUD_PREPARE_FAILED;
			case audWaveSlot::NOT_REQUESTED:
				if(GetCanDynamicallyLoad())
				{
					bool bSuccess = slotPtr->LoadAsset(m_BankNameIndex, m_PrimaryWaveNameHash, GetWaveLoadPriority());
					SoundAssert(bSuccess);
					return bSuccess ? AUD_PREPARING : AUD_PREPARE_FAILED;
				}
				// else - fall through to default
			default:
				return AUD_PREPARE_FAILED;
			}	
		}
	}

	void audGranularSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 UNUSED_PARAM(index))
	{
#if !__SPU
		audWaveSlot* waveSlot = audWaveSlot::GetWaveSlotFromIndex(m_SlotIndex);

		if(waveSlot)
		{
			if (!waveSlot || waveSlot->GetContainer().FindObject(m_PrimaryWaveNameHash) == adatContainer::InvalidId)
			{
				SetPrepareFailed();
				return;
			}	
		}

		ASSERT_ONLY(audEnvironmentSound* environmentSound = (audEnvironmentSound*)GetChildSound(0));
		SoundAssert(environmentSound);
		InitGrainPlayer();
#endif
	}

#if !__SPU
	void audGranularSound::InitGrainPlayer()
	{
		if(m_GrainPlayerInitialised)
		{
			return;
		}

		u32 validChannels = 0;

		for(u32 loop = 0; loop < audGrainPlayer::kMaxGranularMixersPerPlayer; loop++)
		{
			audGrainChannelInitData* initData = GetChannelInitData(loop);
			if(initData && initData->waveNameHash != 0)
			{
				audGrainPlayer::audInitGrainPlayerCommandPacket initPacket;
				initPacket.channel = loop;
				initPacket.waveSlot = (u32)m_SlotIndex;
				initPacket.waveNameHash = initData->waveNameHash;				
				initPacket.outputChannel = initData->outputChannel;
				initPacket.matchMinPitch = initData->matchMinPitch;
				initPacket.matchMaxPitch = initData->matchMaxPitch;
				initPacket.granularClock = initData->granularClock;
				initPacket.allowLoopGrainOverlap = initData->allowLoopGrainOverlap;
				initPacket.maxLoopProportion = initData->maxLoopProportion;
				initPacket.initialVolume = initData->initialVolume/1000.0f;
				initPacket.quality = m_Quality;
				audAssertf(initData->granularClock < m_GranularClocks.GetCount(), "Granular channel %d is using an invalid clock", loop);
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::InitParams, &initPacket);
				validChannels++;
			}
		}

		for(u32 loop = 0; loop < audGrainPlayer::kMaxGranularMixersPerPlayer; loop++)
		{
			audGrainChannelInitData* initData = GetChannelInitData(loop);
			audGrainPlayer::audGrainPlayerSetChannelVolumePacket volumePacket;
			volumePacket.channel = loop;
			volumePacket.volume = audDriverUtil::ComputeLinearVolumeFromDb(initData->volumeScale/100.0f);
			volumePacket.masterVolume = true;
			audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
		}

		if(m_GranularClocks.GetCount() > 0)
		{
			for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
			{
				audGrainPlayer::audGrainPlayerInitGranularClockPacket initGranularClockPacket;
				initGranularClockPacket.minHertz = m_GranularClocks[loop].MinPitch;
				initGranularClockPacket.maxHertz = m_GranularClocks[loop].MaxPitch;
				initGranularClockPacket.clockIndex = loop;
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::InitGranularClock, &initGranularClockPacket);
			}
		}
		else
		{
			audGrainPlayer::audGrainPlayerInitGranularClockPacket initGranularClockPacket;
			initGranularClockPacket.minHertz = .0f;
			initGranularClockPacket.maxHertz = .0f;
			initGranularClockPacket.clockIndex = 0;
			audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::InitGranularClock, &initGranularClockPacket);
		}

		// Delete all the init channel data as its no longer required
		for(u32 loop = 0; loop < kMaxChannelInitDataSlots; loop++)
		{
			if(m_ChannelInitDataSlotIds[loop] != 0xff)
			{
				audSound *slot = (audSound *)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_ChannelInitDataSlotIds[loop]);
				sm_Pool.DeleteSound(slot, m_InitParams.BucketId);
				m_ChannelInitDataSlotIds[loop] = 0xff;
			}
		}

		m_GrainPlayerInitialised = true;

		audGrainPlayer::audGrainPlayerSetLoopRandomisationParams loopRandomisationPacket;
		loopRandomisationPacket.randomisationEnabled = m_LoopRandomisationEnabled;
		loopRandomisationPacket.randomisationChangeRate = m_LoopRandomisationChangeRate;
		loopRandomisationPacket.randomisationMaxPitchFraction = m_LoopRandomisationMaxPitchFraction;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetLoopRandomisationParams, &loopRandomisationPacket);
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::XValueForced, m_InitialX);

		if(m_Quality < audGranularMix::GrainPlayerQualityMax)
		{
			SetGrainPlayerQuality(m_Quality);
		}
	}
#endif

	s32 audGranularSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		ASSERT_ONLY(audEnvironmentSound* environmentSound = (audEnvironmentSound*)GetChildSound(0));
		SoundAssert(environmentSound);
		if(isLooping)
		{
			*isLooping = true;
		}
		return ~0U;
	}

	audGranularSound::audGrainChannelInitData* audGranularSound::GetChannelInitData(const u32 index)
	{
		const u32 numInitPacketsPerSlot = sm_Pool.GetSoundSlotSize() / sizeof(audGrainChannelInitData);
		u32 slotIndex = index/numInitPacketsPerSlot;
		u32 dataIndex = index - (slotIndex * numInitPacketsPerSlot);
		SoundAssertf(m_ChannelInitDataSlotIds[slotIndex] != 0xff, "Invalid slot (%u %u) on channel %u! %s %s", GetName(), slotIndex, dataIndex, index, m_Initialised? "TRUE" : "FALSE", m_GrainPlayerInitialised? "TRUE" : "FALSE");
		audGrainChannelInitData* slotData = (audGrainChannelInitData*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_ChannelInitDataSlotIds[slotIndex]);
		return &slotData[dataIndex];
	}

	void audGranularSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{		
		audSound *envSound = GetChildSound(0);
		SoundAssert(envSound);
		if (envSound)
		{
			envSound->_ManagedAudioPlay(timeInMs, combineBuffer);			
		}
	}

	bool audGranularSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
#if __BANK && !__SPU
		if(m_GrainPlayerId >= 0)
		{
			const u32 nameHash = atStringHash(GetName());

			if(SOUNDFACTORY.IsSoundNameInEditedList(nameHash))
			{
				GranularSound* granularSound = (GranularSound*)GetMetadataForRAVEChanges();
				audGrainPlayer::audGrainPlayerSetChannelVolumePacket volumePacket;
				volumePacket.masterVolume = true;

				volumePacket.channel = 0;
				volumePacket.volume = audDriverUtil::ComputeLinearVolumeFromDb(granularSound->Channel0_Volume/100.0f);
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
				
				volumePacket.channel = 1;
				volumePacket.volume = audDriverUtil::ComputeLinearVolumeFromDb(granularSound->Channel1_Volume/100.0f);
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
				
				volumePacket.channel = 2;
				volumePacket.volume = audDriverUtil::ComputeLinearVolumeFromDb(granularSound->Channel2_Volume/100.0f);
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
				
				volumePacket.channel = 3;
				volumePacket.volume = audDriverUtil::ComputeLinearVolumeFromDb(granularSound->Channel3_Volume/100.0f);
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
				
				volumePacket.channel = 4;
				volumePacket.volume = audDriverUtil::ComputeLinearVolumeFromDb(granularSound->Channel4_Volume/100.0f);
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
				
				volumePacket.channel = 5;
				volumePacket.volume = audDriverUtil::ComputeLinearVolumeFromDb(granularSound->Channel5_Volume/100.0f);
				audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
			}
		}
#endif

		audSound *envSound = GetChildSound(0);
		if (IsWaitingForChildrenToRelease() && envSound)
		{
			(envSound->_Stop());
		}
		if (!envSound)
		{
			return false;
		}

		u32 channelsPlaying = 0;
		for(u32 loop = 0; loop < audGrainPlayer::kMaxGranularMixersPerPlayer; loop++)
		{
			if(m_ChannelValid.IsSet(loop) && !m_ChannelMuted.IsSet(loop))
			{
				if(m_Quality == audGranularMix::GrainPlayerQualityHigh && (loop == 0 || loop == 1))
				{
					channelsPlaying += 2;
				}
				else 
				{
					channelsPlaying++;
				}
			}
		}

#if RSG_PS3
		((audEnvironmentSound*)envSound)->SetPCMSourceCost((u32)(f32(channelsPlaying) * Lerp(m_PrevXValue, 38.f, 106.f)));
#else
		((audEnvironmentSound*)envSound)->SetPCMSourceCost((u32)(30.f * channelsPlaying * (1.0f + m_PrevXValue)));
#endif
		return	envSound->_ManagedAudioUpdate(timeInMs, combineBuffer);
	}

	void audGranularSound::AudioKill()
	{
		audSound *envSound = GetChildSound(0);
		if (envSound)
		{
			envSound->_ManagedAudioKill();
		}
	}

	void audGranularSound::SetX(const f32 x, const u32 elapsedTime)
	{	
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::TimeStep, elapsedTime);
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::XValue, x);
		m_PrevXValue = x;
	}

	void audGranularSound::ForceX(const f32 x)
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::XValueForced, x);
		m_PrevXValue = x;
	}

	void audGranularSound::AllocateVariableBlock()
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::AllocateVariableBlock, 1u);
	}

	void audGranularSound::SetXSmoothRate(const f32 smoothRate) const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::XValueSmoothRate, smoothRate);
	}

	void audGranularSound::SetGrainPlaybackStyle(u32 channel, audGranularMix::audGrainPlaybackStyle playbackStyle) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetPlaybackStylePacket playbackStylePacket;
		playbackStylePacket.channel = channel;
		playbackStylePacket.style = playbackStyle;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::GrainPlaybackStyle, &playbackStylePacket);
	}	

	void audGranularSound::SetGrainPlayerQuality(audGranularMix::audGrainPlayerQuality quality)
	{
		m_Quality = quality;
	}	

	void audGranularSound::SnapToNearestPlaybackStyle(u32 channel) const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::SnapToNearestPlaybackType, channel);
	}

	void audGranularSound::SetGranularRandomisationStyle(audGranularSubmix::audGrainPlaybackOrder randomisationStyle) const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::GrainRandomisationStyle, (u32)randomisationStyle);
	}

#if __BANK
	void audGranularSound::SetDebugRenderingEnabled(bool enabled) const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::SetDebugRenderingEnabled, enabled? 1u : 0u);
	}

	void audGranularSound::ShiftLoopRight() const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::ShiftLoopRight, 1u);
	}

	void audGranularSound::ShiftLoopLeft() const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::ShiftLoopLeft, 1u);
	}

	void audGranularSound::GrowLoop() const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::GrowLoop, 1u);
	}

	void audGranularSound::ShrinkLoop() const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::ShrinkLoop, 1u);
	}

	void audGranularSound::CreateLoop() const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::CreateLoop, 1u);
	}

	void audGranularSound::ExportData() const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::ExportData, 1u);
	}

	void audGranularSound::DeleteLoop() const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::DeleteLoop, 1u);
	}

	void audGranularSound::SetTestToneEnabled(bool enabled) const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::ToneGeneratorEnabled, enabled? 1u : 0u);
	}
#endif

	void audGranularSound::SetGranularSlidingWindowSize(f32 windowSizeHz, u32 minGrains, u32 maxGrains, u32 minRepeatRate) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetSlidingWindowSizePacket windowSizePacket;
		windowSizePacket.windowSizeHz = windowSizeHz;
		windowSizePacket.minGrains = minGrains;
		windowSizePacket.maxGrains = maxGrains;
		windowSizePacket.minRepeatRate = minRepeatRate;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetGranularSlidingWindowSize, &windowSizePacket);
	}

	void audGranularSound::MuteChannel(u32 channel, bool muted)
	{
		VerifyValidGrainPlayer();
		m_ChannelMuted.Set(channel, muted);
		audGrainPlayer::audGrainPlayerSetChannelFlagPacket muteChannelPacket;
		muteChannelPacket.channel = channel;
		muteChannelPacket.flag = muted;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::MuteChannel, &muteChannelPacket);
	}

	void audGranularSound::SetInitialParameters(f32 initialX)
	{
		m_InitialX = initialX;
	}

	void audGranularSound::LockPitch(bool lock) const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::SetPitchLocked, lock? 1u : 0u);
	}

	void audGranularSound::SetGranularChangeRateForLoops(f32 changeRate) const
	{
		VerifyValidGrainPlayer();
		audPcmSourceInterface::SetParam(m_GrainPlayerId, audGrainPlayer::Params::SetGranularChangeRateForLoops, changeRate);
	}

	void audGranularSound::SetChannelVolume(u32 channel, f32 volume, f32 smoothRate)
	{
		audAssert(channel < audGrainPlayer::kMaxGranularMixersPerPlayer);

#if !__SPU
		if(m_Initialised && !m_GrainPlayerInitialised)
		{
			audGrainChannelInitData* initData = GetChannelInitData(channel);
			audAssert(initData);
			if(initData)
			{
				initData[channel].initialVolume = volume;
			}
		}
		else if(GetPlayState() == AUD_SOUND_PLAYING)
#endif
		{
			VerifyValidGrainPlayer();
			audGrainPlayer::audGrainPlayerSetChannelVolumePacket volumePacket;
			volumePacket.channel = channel;
			volumePacket.volume = volume;
			volumePacket.smoothRate = smoothRate;
			audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetChannelVolume, &volumePacket);
		}

		m_ChannelMuted.Set(channel, volume == 0.0f);
	}

	void audGranularSound::SetAsPureRandom(u32 channel, bool setAsRandom) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetChannelFlagPacket pureRandomPacket;
		pureRandomPacket.channel = channel;
		pureRandomPacket.flag = setAsRandom;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetAsPureRandomPlayer, &pureRandomPacket);
	}

	void audGranularSound::SetAsPureRandomWithStyle(u32 channel, audGranularSubmix::audGrainPlaybackOrder randomisationStyle) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetChannelU32Packet pureRandomPacket;
		pureRandomPacket.channel = channel;
		pureRandomPacket.value = (u32)randomisationStyle;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetAsPureRandomPlayerWithStyle, &pureRandomPacket);
	}

	void audGranularSound::SetMixNative(u32 channel, bool mixNative) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetChannelFlagPacket mixNativePacket;
		mixNativePacket.channel = channel;
		mixNativePacket.flag = mixNative;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetMixNative, &mixNativePacket);
	}

	void audGranularSound::SetGrainSkippingEnabled(u32 channel, bool enabled, u32 numToPlay, u32 numToSkip, f32 skippedGrainVol) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetGrainSkippingEnabledPacket grainSkipPacket;
		grainSkipPacket.channel = channel;
		grainSkipPacket.enabled = enabled;
		grainSkipPacket.numToPlay = numToPlay;
		grainSkipPacket.numToSkip = numToSkip;
		grainSkipPacket.skippedGrainVol = skippedGrainVol;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetGrainSkippingEnabled, &grainSkipPacket);
	}
	
	void audGranularSound::SetLoopEnabled(u32 channel, u32 loopID, bool enabled) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetLoopEnabledPacket setLoopEnabledPacket;
		setLoopEnabledPacket.loopEnabled = enabled;
		setLoopEnabledPacket.loopID = loopID;
		setLoopEnabledPacket.channel = channel;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetLoopEnabled, &setLoopEnabledPacket);
	} 

	void audGranularSound::SetWobbleEnabled(u32 length, f32 speed, f32 pitch, f32 volume, u32 clockIndex) const
	{
		VerifyValidGrainPlayer();
		audGrainPlayer::audGrainPlayerSetWobbleEnabledPacket setWobbleEnabledPacket;
		setWobbleEnabledPacket.wobbleLength = length;
		setWobbleEnabledPacket.wobbleSpeed = speed;
		setWobbleEnabledPacket.wobbleVolume = volume;
		setWobbleEnabledPacket.wobblePitch = pitch;
		setWobbleEnabledPacket.grainClock = clockIndex;
		audPcmSourceInterface::WriteCustomCommandPacket(m_GrainPlayerId, audGrainPlayer::Params::SetWobbleEnabled, &setWobbleEnabledPacket);
	}

} // namespace rage
