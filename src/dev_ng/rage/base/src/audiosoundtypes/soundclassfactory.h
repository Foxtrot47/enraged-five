// 
// soundclassfactory.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated sound class factory functions - do not edit.
// 

#ifndef AUD_SOUNDCLASSFACTORY_H
#define AUD_SOUNDCLASSFACTORY_H

#include "audiosoundtypes/Sound.h"
#include "audiosoundtypes/LoopingSound.h"
#include "audiosoundtypes/EnvelopeSound.h"
#include "audiosoundtypes/TwinLoopSound.h"
#include "audiosoundtypes/SpeechSound.h"
#include "audiosoundtypes/OnStopSound.h"
#include "audiosoundtypes/WrapperSound.h"
#include "audiosoundtypes/SequentialSound.h"
#include "audiosoundtypes/StreamingSound.h"
#include "audiosoundtypes/RetriggeredOverlappedSound.h"
#include "audiosoundtypes/CrossfadeSound.h"
#include "audiosoundtypes/CollapsingStereoSound.h"
#include "audiosoundtypes/SimpleSound.h"
#include "audiosoundtypes/MultitrackSound.h"
#include "audiosoundtypes/RandomizedSound.h"
#include "audiosoundtypes/EnvironmentSound.h"
#include "audiosoundtypes/DynamicEntitySound.h"
#include "audiosoundtypes/SequentialOverlapSound.h"
#include "audiosoundtypes/ModularSynthSound.h"
#include "audiosoundtypes/GranularSound.h"
#include "audiosoundtypes/DirectionalSound.h"
#include "audiosoundtypes/KineticSound.h"
#include "audiosoundtypes/SwitchSound.h"
#include "audiosoundtypes/VariableCurveSound.h"
#include "audiosoundtypes/VariablePrintValueSound.h"
#include "audiosoundtypes/VariableBlockSound.h"
#include "audiosoundtypes/IfSound.h"
#include "audiosoundtypes/MathOperationSound.h"
#include "audiosoundtypes/ParameterTransformSound.h"
#include "audiosoundtypes/FluctuatorSound.h"
#include "audiosoundtypes/AutomationSound.h"
#include "audiosoundtypes/ExternalStreamSound.h"

namespace rage
{
	audSound* gSoundClassFactory(rage::u32 bucketId, rage::u32 classId)
	{
		audSound* obj = NULL;
		switch(classId)
		{
			case LoopingSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audLoopingSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audLoopingSound();
				}
				break;
			}
			case EnvelopeSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audEnvelopeSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audEnvelopeSound();
				}
				break;
			}
			case TwinLoopSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audTwinLoopSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audTwinLoopSound();
				}
				break;
			}
			case SpeechSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audSpeechSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audSpeechSound();
				}
				break;
			}
			case OnStopSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audOnStopSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audOnStopSound();
				}
				break;
			}
			case WrapperSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audWrapperSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audWrapperSound();
				}
				break;
			}
			case SequentialSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audSequentialSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audSequentialSound();
				}
				break;
			}
			case StreamingSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audStreamingSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audStreamingSound();
				}
				break;
			}
			case RetriggeredOverlappedSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audRetriggeredOverlappedSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audRetriggeredOverlappedSound();
				}
				break;
			}
			case CrossfadeSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audCrossfadeSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audCrossfadeSound();
				}
				break;
			}
			case CollapsingStereoSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audCollapsingStereoSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audCollapsingStereoSound();
				}
				break;
			}
			case SimpleSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audSimpleSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audSimpleSound();
				}
				break;
			}
			case MultitrackSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audMultitrackSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audMultitrackSound();
				}
				break;
			}
			case RandomizedSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audRandomizedSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audRandomizedSound();
				}
				break;
			}
			case EnvironmentSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audEnvironmentSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audEnvironmentSound();
				}
				break;
			}
			case DynamicEntitySound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audDynamicEntitySound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audDynamicEntitySound();
				}
				break;
			}
			case SequentialOverlapSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audSequentialOverlapSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audSequentialOverlapSound();
				}
				break;
			}
			case ModularSynthSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audModularSynthSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audModularSynthSound();
				}
				break;
			}
			case GranularSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audGranularSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audGranularSound();
				}
				break;
			}
			case DirectionalSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audDirectionalSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audDirectionalSound();
				}
				break;
			}
			case KineticSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audKineticSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audKineticSound();
				}
				break;
			}
			case SwitchSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audSwitchSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audSwitchSound();
				}
				break;
			}
			case VariableCurveSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audVariableCurveSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audVariableCurveSound();
				}
				break;
			}
			case VariablePrintValueSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audVariablePrintValueSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audVariablePrintValueSound();
				}
				break;
			}
			case VariableBlockSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audVariableBlockSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audVariableBlockSound();
				}
				break;
			}
			case IfSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audIfSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audIfSound();
				}
				break;
			}
			case MathOperationSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audMathOperationSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audMathOperationSound();
				}
				break;
			}
			case ParameterTransformSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audParameterTransformSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audParameterTransformSound();
				}
				break;
			}
			case FluctuatorSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audFluctuatorSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audFluctuatorSound();
				}
				break;
			}
			case AutomationSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audAutomationSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audAutomationSound();
				}
				break;
			}
			case ExternalStreamSound::TYPE_ID :
			{
				obj = (audSound*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audExternalStreamSound), bucketId);
				if (NULL != obj)
				{
					::new(obj) audExternalStreamSound();
				}
				break;
			}
		}
		return obj;
	}
	
	// PURPOSE - Gets the size of the largest object
	rage::u32 gSoundClassFactoryMaxSize()
	{
		rage::u32 size = 0;
		
		#if __NO_OUTPUT
			#define MAXSIZE(type) if (sizeof(type) > size) { size = sizeof(type); }
		#else
			const char* largestObjectTypeName = NULL;
			#define MAXSIZE(type) if (sizeof(type) > size) { size = sizeof(type); largestObjectTypeName = #type; }
		#endif
		
		MAXSIZE(audSound)
		MAXSIZE(audLoopingSound)
		MAXSIZE(audEnvelopeSound)
		MAXSIZE(audTwinLoopSound)
		MAXSIZE(audSpeechSound)
		MAXSIZE(audOnStopSound)
		MAXSIZE(audWrapperSound)
		MAXSIZE(audSequentialSound)
		MAXSIZE(audStreamingSound)
		MAXSIZE(audRetriggeredOverlappedSound)
		MAXSIZE(audCrossfadeSound)
		MAXSIZE(audCollapsingStereoSound)
		MAXSIZE(audSimpleSound)
		MAXSIZE(audMultitrackSound)
		MAXSIZE(audRandomizedSound)
		MAXSIZE(audEnvironmentSound)
		MAXSIZE(audDynamicEntitySound)
		MAXSIZE(audSequentialOverlapSound)
		MAXSIZE(audModularSynthSound)
		MAXSIZE(audGranularSound)
		MAXSIZE(audDirectionalSound)
		MAXSIZE(audKineticSound)
		MAXSIZE(audSwitchSound)
		MAXSIZE(audVariableCurveSound)
		MAXSIZE(audVariablePrintValueSound)
		MAXSIZE(audVariableBlockSound)
		MAXSIZE(audIfSound)
		MAXSIZE(audMathOperationSound)
		MAXSIZE(audParameterTransformSound)
		MAXSIZE(audFluctuatorSound)
		MAXSIZE(audAutomationSound)
		MAXSIZE(audExternalStreamSound)
		
		#undef MAXSIZE
		
		#if !__NO_OUTPUT
			Displayf("Largest object type: %s (%u bytes)", largestObjectTypeName, size);
		#endif
		return size;
	}
	
	// PURPOSE - Gets the name of the type given the type id
	const char* gSoundClassFactoryGetTypeName(const rage::u32 classId)
	{
		switch(classId)
		{
			case Sound::TYPE_ID: return "audSound";
			case LoopingSound::TYPE_ID: return "audLoopingSound";
			case EnvelopeSound::TYPE_ID: return "audEnvelopeSound";
			case TwinLoopSound::TYPE_ID: return "audTwinLoopSound";
			case SpeechSound::TYPE_ID: return "audSpeechSound";
			case OnStopSound::TYPE_ID: return "audOnStopSound";
			case WrapperSound::TYPE_ID: return "audWrapperSound";
			case SequentialSound::TYPE_ID: return "audSequentialSound";
			case StreamingSound::TYPE_ID: return "audStreamingSound";
			case RetriggeredOverlappedSound::TYPE_ID: return "audRetriggeredOverlappedSound";
			case CrossfadeSound::TYPE_ID: return "audCrossfadeSound";
			case CollapsingStereoSound::TYPE_ID: return "audCollapsingStereoSound";
			case SimpleSound::TYPE_ID: return "audSimpleSound";
			case MultitrackSound::TYPE_ID: return "audMultitrackSound";
			case RandomizedSound::TYPE_ID: return "audRandomizedSound";
			case EnvironmentSound::TYPE_ID: return "audEnvironmentSound";
			case DynamicEntitySound::TYPE_ID: return "audDynamicEntitySound";
			case SequentialOverlapSound::TYPE_ID: return "audSequentialOverlapSound";
			case ModularSynthSound::TYPE_ID: return "audModularSynthSound";
			case GranularSound::TYPE_ID: return "audGranularSound";
			case DirectionalSound::TYPE_ID: return "audDirectionalSound";
			case KineticSound::TYPE_ID: return "audKineticSound";
			case SwitchSound::TYPE_ID: return "audSwitchSound";
			case VariableCurveSound::TYPE_ID: return "audVariableCurveSound";
			case VariablePrintValueSound::TYPE_ID: return "audVariablePrintValueSound";
			case VariableBlockSound::TYPE_ID: return "audVariableBlockSound";
			case IfSound::TYPE_ID: return "audIfSound";
			case MathOperationSound::TYPE_ID: return "audMathOperationSound";
			case ParameterTransformSound::TYPE_ID: return "audParameterTransformSound";
			case FluctuatorSound::TYPE_ID: return "audFluctuatorSound";
			case AutomationSound::TYPE_ID: return "audAutomationSound";
			case ExternalStreamSound::TYPE_ID: return "audExternalStreamSound";
			default: return NULL;
		}
	}
} // namespace rage
#endif // AUD_SOUNDCLASSFACTORY_H
