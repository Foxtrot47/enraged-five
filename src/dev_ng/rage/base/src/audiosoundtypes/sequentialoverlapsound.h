// 
// audiosoundtypes/sequentialoverlapsound.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_SEQUENTIALOVERLAPSOUND_H
#define AUD_SEQUENTIALOVERLAPSOUND_H

#include "sound.h"

#include "soundDefs.h"

namespace rage {

	// PURPOSE
	//  This sound type triggers it's next child sound every X milliseconds
	class audSequentialOverlapSound : public audSound
	{
	public:
		
		~audSequentialOverlapSound();

		AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
		audSequentialOverlapSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();

		// PURPOSE
		//  Implements base class function for this sound.
		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

		// PURPOSE
		//	Sets the time between child triggering - overrides metadata delay time
		void SetDelayTime(u32 timeInMs);

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

		void ChildSoundCallback(const u32 timeInMs, const u32 childIndex);

		enum {kMaxSequentialOverlapChildren = 32};

		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

	private:

		void SetupChildSound(const int which);

		audSoundScratchInitParams m_ScratchInitParams;

		u32 m_LastPlayTime, m_DelayTime;
		u32 m_MetadataDelayTime;
		audVariableHandle m_DelayTimeVariable;
		audVariableHandle m_DirectionVariable;
		u32	m_NumChildren;
		u32	m_CurrentChildIndex;
		u8 m_StateSlotIndex;
		u8 m_CurrentSyncID;
		u8 m_Toggle;

		bool m_ShouldReleaseChildren : 1;
		bool m_IsLooping : 1;
		bool m_IsDelayTimeRemaining : 1;
		bool m_IsDelayTimePercentage : 1;
		bool m_TriggerOnChildReleasing : 1;
		bool m_SpliceMode : 1;
		
		struct syncPair
		{
			u16 masterSyncId;
			u16 slaveSyncId;
		};
		atRangeArray<syncPair,2> m_SyncIds;
		atRangeArray<bool, 2> m_Preparing;
		
	};

} // namespace rage

#endif // AUD_SEQUENTIALOVERLAPSOUND_H
