//
// audioengine/multitracksound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "crossfadesound.h"
#include "audioengine/engine.h"
#include "audioengine/environment.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/debug.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/driverdefs.h"

namespace rage 
{

extern audEnvironment *g_audEnvironment;

enum tCrossfadeSound
{
	AUD_NEAR,
	AUD_FAR
};

AUD_IMPLEMENT_STATIC_WRAPPERS(audCrossfadeSound);

audCrossfadeSound::~audCrossfadeSound()
{
	for(u32 i = 0; i < audSound::kMaxChildren; i++)
	{
		if(GetChildSound(i))
		{
			DeleteChild(i);
		}	
	}
	if(m_StateStorageSlot != 0xff)
	{
		audSound::GetStaticPool().DeleteSound(audSound::GetStaticPool().GetSoundSlot(m_InitParams.BucketId, m_StateStorageSlot), m_InitParams.BucketId);
		m_StateStorageSlot = 0xff;
	}
}

#if !__SPU
audCrossfadeSound::audCrossfadeSound()
{
	m_StateStorageSlot = 0xff;
	m_ChildVolumes[0] = m_ChildVolumes[1] = 0.f;
}

bool audCrossfadeSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	audCrossfadeSoundState *state = (audCrossfadeSoundState*)audSound::GetStaticPool().AllocateSoundSlot(sizeof(audCrossfadeSoundState), m_InitParams.BucketId, true);
	if(state == NULL)
	{
		return false;
	}
	
	Assign(m_StateStorageSlot, audSound::GetStaticPool().GetSoundSlotIndex(m_InitParams.BucketId, state));
	
	::new(state) audCrossfadeSoundState();

	state->m_ScratchInitParams = *scratchInitParams;

	CrossfadeSound *crossfadeSoundData = (CrossfadeSound*)GetMetadata();

	SetChildSound(AUD_NEAR, SOUNDFACTORY.GetChildInstance(crossfadeSoundData->NearSoundRef, this, initParams, scratchInitParams, false));
	*scratchInitParams = state->m_ScratchInitParams;
	SetChildSound(AUD_FAR, SOUNDFACTORY.GetChildInstance(crossfadeSoundData->FarSoundRef, this, initParams, scratchInitParams, false));

	if ((!GetChildSound(AUD_NEAR)) || (!GetChildSound(AUD_FAR)))
	{
		// if either of our children don't exist, we haven't initialised properly.
		return false;
	}

	state->m_Mode = crossfadeSoundData->Mode;
	state->m_MinDistance = crossfadeSoundData->MinDistance;
	state->m_MaxDistance = crossfadeSoundData->MaxDistance;
	state->m_Hysteresis = crossfadeSoundData->Hysteresis;

	state->m_NearSoundRef = crossfadeSoundData->NearSoundRef;
	state->m_FarSoundRef = crossfadeSoundData->FarSoundRef;

	// Even in fixed mode we delete children in AudioPlay
	SetHasDynamicChildren();
	

	if (crossfadeSoundData->DistanceVariable)
	{
		state->m_DistanceVariable = _FindVariableUpHierarchy(crossfadeSoundData->DistanceVariable);
	}
	if (crossfadeSoundData->MinDistanceVariable) 
	{
		state->m_MinDistanceVariable = _FindVariableUpHierarchy(crossfadeSoundData->MinDistanceVariable);
	}
	if (crossfadeSoundData->MaxDistanceVariable)
	{
		state->m_MaxDistanceVariable = _FindVariableUpHierarchy(crossfadeSoundData->MaxDistanceVariable);
	}
	if (crossfadeSoundData->CrossfadeVariable)
	{
		state->m_CrossfadeVariable = _FindVariableUpHierarchy(crossfadeSoundData->CrossfadeVariable);
	}

	state->m_CrossfadeCurve.Init(crossfadeSoundData->CrossfadeCurve);
	SoundAssert(state->m_CrossfadeCurve.IsValidInSoundTask());
	return true;
}
#endif

audPrepareState audCrossfadeSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	audPrepareState state;
	bool finishedPreparing = true;

	for (int i=AUD_NEAR; i<(AUD_FAR+1); i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->SetStartOffset((s32)GetPlaybackStartOffset());
			state = childSound->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
			if(state == AUD_PREPARE_FAILED)
			{
				// bail out if any of our children have failed to prepare
				return AUD_PREPARE_FAILED;
			}
			else if(state == AUD_PREPARING)
			{
				finishedPreparing = false;
			}
		}
	}

	if(finishedPreparing)
	{
		return AUD_PREPARED;
	}
	else
	{
		return AUD_PREPARING;
	}
}

void audCrossfadeSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audCrossfadeSoundState *state = (audCrossfadeSoundState*)audSound::GetStaticPool().GetSoundSlot(m_InitParams.BucketId, m_StateStorageSlot);
	
	// Work out how far through we want to play, based on precalculated PlaybackStartOffset
	u32 startOffset = GetPlaybackStartOffset(); // this is always in ms, not percentage

	// Important we do both these here, as we don't do them for FIXED mode in Update(), so they'd never be kicked off
	// or stopped.
	SetAttenuationFactors(state);
	UpdateChildSounds(state);

	m_CachedCombineBuffer = combineBuffer;

	const float baseVolume = combineBuffer.Volume;
	for (int i=AUD_NEAR; i<(AUD_FAR+1); i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->SetStartOffset((s32)startOffset, false);
			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			combineBuffer.Volume = baseVolume + m_ChildVolumes[i];
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}
}

bool audCrossfadeSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	bool stillPlaying = false;
	audCrossfadeSoundState *state = (audCrossfadeSoundState*)audSound::GetStaticPool().GetSoundSlot(m_InitParams.BucketId, m_StateStorageSlot);

#if __BANK && !__SPU
	// Go get all our variables again, incase they've been tweaked.
	CrossfadeSound *crossfadeSoundData = (CrossfadeSound*)GetMetadataForRAVEChanges();
	state->m_Mode = crossfadeSoundData->Mode;
	state->m_MinDistance = crossfadeSoundData->MinDistance;
	state->m_MaxDistance = crossfadeSoundData->MaxDistance;
	state->m_Hysteresis = crossfadeSoundData->Hysteresis;

	if (crossfadeSoundData->DistanceVariable)
	{
		state->m_DistanceVariable = _FindVariableUpHierarchy(crossfadeSoundData->DistanceVariable);
	}
	if (crossfadeSoundData->MinDistanceVariable)
	{
		state->m_MinDistanceVariable = _FindVariableUpHierarchy(crossfadeSoundData->MinDistanceVariable);
	}
	if (crossfadeSoundData->MaxDistanceVariable)
	{
		state->m_MaxDistanceVariable = _FindVariableUpHierarchy(crossfadeSoundData->MaxDistanceVariable);
	}
	if (crossfadeSoundData->CrossfadeVariable)
	{
		state->m_CrossfadeVariable = _FindVariableUpHierarchy(crossfadeSoundData->CrossfadeVariable);
	}
#endif

	bool hasRequestedNewChild = false;
	// Don't update any of this in FIXED mode - we call the fns once on play and then leave them be.
	if (state->m_Mode != CROSSFADE_MODE_FIXED)
	{
		SetAttenuationFactors(state);
		hasRequestedNewChild = UpdateChildSounds(state);
	}

	m_CachedCombineBuffer = combineBuffer;

	// Only check for children playing if we haven't requested a new one this frame.
	if ( !hasRequestedNewChild )
	{
		const float baseVolume = combineBuffer.Volume;

		for (int i=AUD_NEAR; i<(AUD_FAR+1); i++)
		{
			audSound *childSound = GetChildSound(i);
			if(childSound)
			{
				if (childSound->GetPlayState() == AUD_SOUND_PLAYING)
				{
					combineBuffer.Volume = m_ChildVolumes[i] + baseVolume;
					if (GetChildSound(i)->_ManagedAudioUpdate(timeInMs, combineBuffer))
					{
						stillPlaying = true;
					}
				}
				else if (childSound->GetPlayState() == AUD_SOUND_PREPARING)
				{
					// This is because if we've cancelled one and kicked off another, we still want to carry on playing.
					stillPlaying = true;
				}
			}
		}	

		if (!stillPlaying)
		{
			// All our subsounds have finished, so we have too
			return false;
		}
	}

	return true;
}

void audCrossfadeSound::AudioKill()
{
	for (int i=AUD_NEAR; i<(AUD_FAR+1); i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->_ManagedAudioKill();
		}
	}
}

s32 audCrossfadeSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	s32 longestChildDuration = 0, childDuration;

	for (int i=AUD_NEAR; i<(AUD_FAR+1); i++)
	{
		audSound *childSound = GetChildSound(i);
		if(childSound)
		{
			bool isChildLooping = false;
			childDuration = childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
			if (isChildLooping && isLooping)
			{
				*isLooping = true;
			}
			if(childDuration > longestChildDuration)
			{
				longestChildDuration = childDuration;
			}
		}
	}

	return longestChildDuration;
}

void audCrossfadeSound::SetAttenuationFactors(audCrossfadeSoundState *state)
{
	// Important that we don't call this fn every frame if we're in FIXED mode, or we'll act like
	// we're in RETRIGGER mode, but with no hysteresis.
	f32 ratio = 0.0f;
	f32 hysteresisComparison = 0.0f;

	if (state->m_DistanceVariable)
	{
		state->m_Distance = AUD_GET_VARIABLE(state->m_DistanceVariable);
	}
	if (state->m_MinDistanceVariable)
	{
		state->m_MinDistance = AUD_GET_VARIABLE(state->m_MinDistanceVariable);
	}
	if (state->m_MaxDistanceVariable)
	{
		state->m_MaxDistance = AUD_GET_VARIABLE(state->m_MaxDistanceVariable);
	}

	// Calculate the ratio, based on crossfade variable if there is one, or distance if not.
	if (state->m_CrossfadeVariable)
	{
		ratio = ClampRange(AUD_GET_VARIABLE(state->m_CrossfadeVariable), 0.0f, 1.0f);
	}
	else
	{
		// Check our distance, and hence our frontend-positioned attenuation.
		// Note that this is using the combine buffer for this sound - hence it'll include all relevant positional info
		// above this sound, but nothing below. It's free to use this combine buffer, but the actual distance calculation
		// isn't the cheapest thing, and is also being done in the environment sound, so this is slightly wasteful.
//		Vector3 position = m_CombineBuffer.Position;
		const Vec3V position = ((audRequestedSettings*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, GetTopLevelRequestedSettingsIndex()))->GetPosition_AudioThread();
		//Convert world-relative position to listener-relative.
		f32 distanceRelativeToListener = state->m_DistanceVariable ? state->m_Distance : g_audEnvironment->ComputeDistanceToClosestVolumeListener(position);
		ratio = ClampRange(distanceRelativeToListener, state->m_MinDistance, state->m_MaxDistance);
		hysteresisComparison = Max((state->m_MinDistance-distanceRelativeToListener), (distanceRelativeToListener-state->m_MaxDistance));
	}

	f32 nearAttenuation = state->m_CrossfadeCurve.CalculateValue(ratio);
	f32 farAttenuation = state->m_CrossfadeCurve.CalculateValue(1.0f-ratio);
	state->m_NearAttenuation = audDriverUtil::ComputeDbVolumeFromLinear(nearAttenuation);
	state->m_FarAttenuation = audDriverUtil::ComputeDbVolumeFromLinear(farAttenuation);

	state->m_PlayNear = true;
	state->m_PlayFar = true;

	// We only do any cancelling and starting if we're in RETRIGGER mode - otherwise just set volume on whatever's playing
	if (state->m_Mode == CROSSFADE_MODE_RETRIGGER)
	{
		if ((state->m_CrossfadeVariable && (ratio <= state->m_Hysteresis)) || (!state->m_CrossfadeVariable && ((ratio == 0.0f) && (hysteresisComparison > state->m_Hysteresis))))
		{
			state->m_PlayFar = false;
		}
		if ((state->m_CrossfadeVariable && ((1.f - ratio) <= state->m_Hysteresis)) || (!state->m_CrossfadeVariable && ((ratio == 1.0f) && (hysteresisComparison > state->m_Hysteresis))))
		{
			state->m_PlayNear = false;
		}
	}
	else if (state->m_Mode == CROSSFADE_MODE_FIXED)
	{
		// Important that we don't call this fn every frame if we're in FIXED mode, or we'll act like
		// we're in RETRIGGER mode, but with no hysteresis.
		if (ratio == 0.0f)
		{
			state->m_PlayFar = false;
		}
		else if (ratio == 1.0f)
		{
			state->m_PlayNear = false;
		}
	}
}

bool audCrossfadeSound::UpdateChildSounds(audCrossfadeSoundState *state)
{
	bool hasRequestedNewChild = false;
	audSound* sndNear = GetChildSound(AUD_NEAR);
	audSound* sndFar = GetChildSound(AUD_FAR);

	bool shouldPlayAnotherChild = !IsToBeStopped();
	
	// See if we're meant to be playing anything we're not, or not meant to be playing anything we are.
	if (sndNear && !state->m_PlayNear)
	{
		sndNear->_ManagedAudioKill(); // Safe to do this, I think, as we're about to set its volume to silence anyway
		DeleteChild(AUD_NEAR);
	}
	else if (!sndNear && state->m_PlayNear && shouldPlayAnotherChild)
	{
		RequestChildSoundFromOffset(state->m_NearSoundRef, AUD_NEAR, &state->m_ScratchInitParams);
		hasRequestedNewChild = true;
	}

	if (sndFar && !state->m_PlayFar)
	{
		sndFar->_ManagedAudioKill(); // Safe to do this, I think, as we're about to set its volume to silence anyway
		DeleteChild(AUD_FAR);
	}
	else if (!sndFar && state->m_PlayFar && shouldPlayAnotherChild)
	{
		RequestChildSoundFromOffset(state->m_FarSoundRef, AUD_FAR, &state->m_ScratchInitParams);
		hasRequestedNewChild = true;
	}

	m_ChildVolumes[AUD_NEAR] = state->m_NearAttenuation;
	m_ChildVolumes[AUD_FAR] = state->m_FarAttenuation;
	
	return hasRequestedNewChild;
}

void audCrossfadeSound::ChildSoundCallback(const u32 timeInMs, const u32 childIndex)
{
	audSound *childSound = GetChildSound(childIndex);
	if (childSound)
	{
		// Insist on this being already prepared - otherwise it's both stupid and really irritating.
		if (childSound->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority()) == AUD_PREPARED)
		{
			audSoundCombineBuffer combineBuffer = m_CachedCombineBuffer;
			combineBuffer.Volume += m_ChildVolumes[childIndex];
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
        }
		else
		{
			childSound->_ManagedAudioDestroy();
			DeleteChild(childIndex);
			// Warn rather than asserting, as this can happen if the bucket is full.
			audWarningf("%s - audCrossfadeSound trying to play child that isn't prepared", GetName());
		}
	}
}
} // namespace rage
