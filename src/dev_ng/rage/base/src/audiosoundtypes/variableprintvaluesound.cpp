// 
// audiosoundtypes/variableprintvaluesound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "variableprintvaluesound.h"
#include "audiohardware/channel.h"
#include "audioengine/engine.h"
#include "audioengine/remotecontrol.h"
#include "audioengine/soundfactory.h"
#include "audioengine/spuutil.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audVariablePrintValueSound);

#if !__SPU

bool audVariablePrintValueSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

#if __BANK
	//	Only Rave users really care to see this output anyway
	m_ShouldPrint = g_AudioEngine.GetSoundManager().GetFactory().GetMetadataManager().IsRAVEConnected();
#endif

	VariablePrintValueSound *variablePrintValueSoundData = (VariablePrintValueSound*)GetMetadata();
	m_Variable = _FindVariableUpHierarchy(variablePrintValueSoundData->Variable);
	m_Message = variablePrintValueSoundData->Message;
	m_MessageLen = variablePrintValueSoundData->MessageLen;
	return true;
}

#endif // !__SPU

audPrepareState audVariablePrintValueSound::AudioPrepare(audWaveSlot *UNUSED_PARAM(waveSlot), bool UNUSED_PARAM(checkStateOnly))
{
	return AUD_PREPARED;
}

void audVariablePrintValueSound::AudioPlay(u32 UNUSED_PARAM(timeInMs), audSoundCombineBuffer &UNUSED_PARAM(combineBuffer))
{
#if __BANK
	if (m_ShouldPrint)
	{
		SPU_ONLY(ALIGNAS(16) char tempBuffer[128] );

		char messageBuf[128];		
		const u32 len = Min<u32>(m_MessageLen, sizeof(messageBuf) - 1);

		if(len > 0)
		{
#if __SPU		
			dmaUnalignedGet(tempBuffer, sizeof(tempBuffer), messageBuf, len, (u32)m_Message);
#else
			sysMemCpy(&messageBuf, m_Message, len);
#endif
		}

		messageBuf[len] = 0;
		audDisplayf("audVariablePrintValueSound: %s: %s: %f", GetName(), messageBuf, (m_Variable?AUD_GET_VARIABLE(m_Variable):0.f));
	}
#endif
}

bool audVariablePrintValueSound::AudioUpdate(u32 UNUSED_PARAM(timeInMs), audSoundCombineBuffer &UNUSED_PARAM(combineBuffer))
{
	// We don't do anything, and report that we've finished, as we do our stuff on Play()
	return false;
}

void audVariablePrintValueSound::AudioKill()
{
	// We don't do anything, as we do our stuff on Play()
}

s32 audVariablePrintValueSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* UNUSED_PARAM(isLooping))
{
	// We don't support lengths and start offets on logical sounds
	return kSoundLengthUnknown;
}

} // namespace rage
