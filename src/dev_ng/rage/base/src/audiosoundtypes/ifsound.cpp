//
// audioengine/ifsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "ifsound.h"
#include "audiohardware\channel.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"

namespace rage {

enum
{
	AUD_IF_BRANCH_TRUE,
	AUD_IF_BRANCH_FALSE,
	AUD_IF_BRANCH_NEITHER
};

AUD_IMPLEMENT_STATIC_WRAPPERS(audIfSound);

#if !__SPU

bool audIfSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	IfSound *ifSoundData = (IfSound*)GetMetadata();

	m_ShouldChooseOnInit = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_IFSOUND_CHOOSEONINIT) ==
		AUD_TRISTATE_TRUE);
		
	m_BranchToBePlayed = AUD_IF_BRANCH_NEITHER;

	m_RightValue = ifSoundData->RHS.RightValue;
	m_RightVariable = _FindVariableUpHierarchy(ifSoundData->RHS.RightVariable);
	m_LeftVariable = _FindVariableUpHierarchy(ifSoundData->LeftVariable);
	m_ConditionType = ifSoundData->ConditionType;

	if(m_ShouldChooseOnInit)
	{
		ComputeBranchToBePlayed();

		//Only create the child we intend to play.
		if(m_BranchToBePlayed == AUD_IF_BRANCH_TRUE)
		{
			SetChildSound(0, SOUNDFACTORY.GetChildInstance(ifSoundData->TrueSoundRef, this, initParams, scratchInitParams, false));
			if(!GetChildSound(0))
			{
				return false;
			}
		}
		else if(m_BranchToBePlayed == AUD_IF_BRANCH_FALSE)
		{
			SetChildSound(1, SOUNDFACTORY.GetChildInstance(ifSoundData->FalseSoundRef, this, initParams, scratchInitParams, false));
			if(!GetChildSound(1))
			{
				return false;
			}
		}
	}
	else
	{
		//Create both children as we don't yet know which we will play.
		audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;
		SetChildSound(0, SOUNDFACTORY.GetChildInstance(ifSoundData->TrueSoundRef, this, initParams, scratchInitParams, false));
		SetChildSound(1,SOUNDFACTORY.GetChildInstance(ifSoundData->FalseSoundRef, this, initParams, &scratchInitParamsCopy, false));
		if(!GetChildSound(0) && !GetChildSound(1))
		{
			return false;
		}

		// We delete children in AudioPlay if ChooseOnInit isn't set, so it's not safe to query the hierarchy from the game thread.
		SetHasDynamicChildren();
	}

	return true;
}

#endif // !__SPU

audIfSound::~audIfSound()
{
	if(!IsBeingRemovedFromHierarchy())
	{
		if(GetChildSound(0))
		{
			DeleteChild(0);
		}
		if(GetChildSound(1))
		{
			DeleteChild(1);
		}
	}
}

audPrepareState audIfSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	// For now, we prepare both branches. This is potentially dangerous, or at least undesired. eg, if the two branches
	// were two speech sounds that loaded into the same slot, you simply can't prepare both. A solution is to always
	// report that we're prepared, and only actually prepare on Play() - this is handled through the dynamically preparing flag

	bool failed = false, prepared = true;
	audPrepareState state;

	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	if (childSound0)
	{
		childSound0->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		state = childSound0->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		if(state == AUD_PREPARE_FAILED)
		{
			failed = true;
		}
		else if(state == AUD_PREPARING)
		{
			prepared = false;
		}
	}
	if (childSound1)
	{
		childSound1->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		state = childSound1->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		if(state == AUD_PREPARE_FAILED)
		{
			failed = true;
		}
		else if(state == AUD_PREPARING)
		{
			prepared = false;
		}
	}

	if(failed)
	{
		return AUD_PREPARE_FAILED;
	}
	else if(prepared)
	{
		return AUD_PREPARED;
	}
	else
	{
		return AUD_PREPARING;
	}
}

void audIfSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	// Ignore start offset, as we're a logic sound, which can't be partially skipped 

	if(!m_ShouldChooseOnInit)
	{
		//Choose the child to play now.
		ComputeBranchToBePlayed();
	}

	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	if (m_BranchToBePlayed==AUD_IF_BRANCH_TRUE && childSound0)
	{
		childSound0->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		childSound0->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		childSound0->_ManagedAudioPlay(timeInMs, combineBuffer);

		if(childSound1)
		{
			DeleteChild(1);
		}
		RemoveMyselfFromHierarchy();
	}
	else if (m_BranchToBePlayed==AUD_IF_BRANCH_FALSE && childSound1)
	{
		childSound1->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		childSound1->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		childSound1->_ManagedAudioPlay(timeInMs, combineBuffer);
		if(childSound0)
		{
			DeleteChild(0);
		}
		// need to shuffle stuff around so childsound0 is the one we're playing
		//RemoveMyselfFromHierarchy(GetChildSound(1));
	}
}

bool audIfSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	// See which branch we're playing, and update it. If it's neither, shit's gone wrong, so SoundAssert and report finished
	if (m_BranchToBePlayed==AUD_IF_BRANCH_TRUE)
	{
		if(childSound0)
		{
			return(childSound0->_ManagedAudioUpdate(timeInMs, combineBuffer));
		}
		else
		{
			// we dont have a true sound so we're done
			return false;
		}
	}
	else if (m_BranchToBePlayed==AUD_IF_BRANCH_FALSE)
	{
		if(childSound1)
		{
			return(childSound1->_ManagedAudioUpdate(timeInMs, combineBuffer));
		}
		else
		{
			// we dont have a false sound so we're done
			return false;
		}
	}
	else
	{
		// Should be one or the other
		return false;
	}
}

void audIfSound::AudioKill()
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	// See which branch we're playing, and stop it. If it's neither, that's maybe valid?
	if (m_BranchToBePlayed==AUD_IF_BRANCH_TRUE && childSound0)
	{
		childSound0->_ManagedAudioKill();
	}
	else if (m_BranchToBePlayed==AUD_IF_BRANCH_FALSE && childSound1)
	{
		childSound1->_ManagedAudioKill();
	}
}

s32 audIfSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound* childSound0 = GetChildSound(0);
	audSound* childSound1 = GetChildSound(1);

	// If we've decided which sound to play, return that one's length. If not, return the length if both child
	// sounds have the same length.
	if (m_BranchToBePlayed==AUD_IF_BRANCH_FALSE)
	{
		if(childSound1)
		{
			return childSound1->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			return 0;
		}
	}
	else if (m_BranchToBePlayed==AUD_IF_BRANCH_TRUE)
	{
		if(childSound0)
		{
			return childSound0->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			return 0;
		}
	}
	else
	{
		bool trueLooping = false, falseLooping = false;
		s32 trueLength  =  (childSound0?childSound0->_ComputeDurationMsIncludingStartOffsetAndPredelay(&trueLooping):0);
		s32 falseLength = (childSound1?childSound1->_ComputeDurationMsIncludingStartOffsetAndPredelay(&falseLooping):0);
		if (trueLength == falseLength)
		{
			if (trueLength && falseLooping && isLooping)
			{
				*isLooping = true;
			}
			else if (isLooping)
			{
				*isLooping = false;
			}
			return trueLength;
		}
		else
		{
			if (isLooping)
			{
				*isLooping = false;
			}
			return kSoundLengthUnknown;
		}
	}
}

void audIfSound::ComputeBranchToBePlayed(void)
{
	// Do our test here, just the once. From then on, we'll update whichever one we're playing.
	// First, get our numerical values - this is fudged right now to have a variable name on the left
	// and a fixed value on the right. Later both sides should be either type, and we structure most of the 
	// code with this in mind.

	f32 rightVariableValue = 0;

	if (!m_LeftVariable) 
	{
		// We didn't find that variable, so do nothing, and SoundAssert
		audWarningf("IfSound with invalid left variable reference - %s", GetName());
		m_BranchToBePlayed = AUD_IF_BRANCH_NEITHER;
		return;
	}

	f32 leftVariableValue = AUD_GET_VARIABLE(m_LeftVariable);

	if (!m_RightVariable)
	{
		// We don't have a variable, so use the numerical value instead
		rightVariableValue = m_RightValue;
	}
	else
	{
		// We do have a variable, so use that
		rightVariableValue = AUD_GET_VARIABLE(m_RightVariable);
	}
	
	m_BranchToBePlayed = AUD_IF_BRANCH_FALSE;
	// Now see what condition we have, and do the compare.
	switch (m_ConditionType)
	{
	case IF_CONDITION_LESS_THAN:
		if (FLOAT_LT(leftVariableValue, rightVariableValue))
		{
			m_BranchToBePlayed = AUD_IF_BRANCH_TRUE;
		}
		break;
	case IF_CONDITION_LESS_THAN_OR_EQUAL_TO:
		if (FLOAT_LTEQ(leftVariableValue, rightVariableValue))
		{
			m_BranchToBePlayed = AUD_IF_BRANCH_TRUE;
		}
		break;
	case IF_CONDITION_GREATER_THAN:
		if (FLOAT_GT(leftVariableValue, rightVariableValue))
		{
			m_BranchToBePlayed = AUD_IF_BRANCH_TRUE;
		}
		break;
	case IF_CONDITION_GREATER_THAN_OR_EQUAL_TO:
		if (FLOAT_GTEQ(leftVariableValue, rightVariableValue))
		{
			m_BranchToBePlayed = AUD_IF_BRANCH_TRUE;
		}
		break;
	case IF_CONDITION_EQUAL_TO:
		if (FLOAT_EQ(leftVariableValue, rightVariableValue))
		{
			m_BranchToBePlayed = AUD_IF_BRANCH_TRUE;
		}
		break;
	case IF_CONDITION_NOT_EQUAL_TO:
		if(!FLOAT_EQ(leftVariableValue, rightVariableValue))
		{
			m_BranchToBePlayed = AUD_IF_BRANCH_TRUE;
		}
		break;
	default:
		SoundAssert(0);
		m_BranchToBePlayed = AUD_IF_BRANCH_NEITHER;
		break;
	}
}

} // namespace rage
