//
// audiosoundtypes/fluctuatorsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_FLUCTUATOR_SOUND_H
#define AUD_FLUCTUATOR_SOUND_H

#include "audiosoundtypes/sound.h"
#include "audiosoundtypes/sounddefs.h"
#include "audioengine/widgets.h"
#include "audiosoundtypes/sounddefs.h"

namespace rage
{
	// PURPOSE
	// A fluctuator sound updates sound variables using an audFluctuator object 
	class audFluctuatorSound : public audSound
	{
	public:

		~audFluctuatorSound();

		AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
		audFluctuatorSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif // !__SPU

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();
		// PURPOSE
		//  Implements base class function for this sound.
		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

		// PURPOSE
		//Pushes out sound variables with the current accelleration, linear force, torque etc
		void UpdateFluctuatorSoundVariables(u32 timeInMs, audSoundCombineBuffer &combineBuffer);

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;
		
		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *slot, bool checkStateOnly);

	private:
		struct FluctuatedVariable
		{
			union
			{
				audFluctuatorProbBased FluctuatorProb;
				audFluctuatorTimeBased FluctuatorTime;
			}Fluctuator;
			u8 Destination;
			u8 Mode; // Probability based || Time based.
			audVariableHandle OutputVariable;
		};
		FluctuatedVariable* GetFluctuatorSlot(const u32 index);

		enum {kMaxFluctuatorSlotIds = 4};
		atRangeArray<u8,kMaxFluctuatorSlotIds> m_FluctuatorSlotIds;
		u8	m_NumFluctuators;
	};

} // namespace rage

#endif //AUD_FLUCTUATOR_SOUND_H
