//
// audiosoundtypes/onstopsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_ON_STOP_SOUND_H
#define AUD_ON_STOP_SOUND_H

#include "sounddefs.h"
#include "sound.h"

namespace rage {

	// PURPOSE 
	//  When receiving a Stop() request, triggers a child stop sound, on finishing, plays a finished sound.
	//  Otherwise, just wraps a single child sound.
	class audOnStopSound : public audSound
	{
	public:
		
		~audOnStopSound();

		AUD_DECLARE_STATIC_WRAPPERS;

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);

#if !__SPU
		audOnStopSound();
		// PURPOSE
		// Implements functionality as required by the base class
		bool Init(const void *Metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		// Implements functionality as required by the base class
		void AudioKill();

		// PURPOSE
		// Implements functionality as required by the base class
		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

		// PURPOSE
		//  Calls ManagedAudioStop() on all the child sounds.
		void ManagedAudioStopChildren();

	private:
		bool m_Stopping;
		bool m_OnStopTriggered;
		bool m_FinishedTriggered;
	};

} // namespace rage

#endif // AUD_ON_STOP_SOUND_H
