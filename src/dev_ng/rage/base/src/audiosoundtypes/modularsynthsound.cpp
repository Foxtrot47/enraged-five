//
// audioengine/modularsynthsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "modularsynthsound.h"

#include "environmentsound.h"
#include "audioengine/engine.h"
#include "audioengine/metadatamanager.h"
#include "audioengine/soundfactory.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/pcmsource_interface.h"
#include "audiohardware/voicesettings.h"

#include "audiosynth/module.h"
#include "audiosynth/modulefactory.h"
#include "audiosynth/synthcore.h"
#include "audiosynth/synthcoredisasm.h"
#include "audiosynth/synthesizer.h"
#include "audiosynth/oscillator.h"

#if __SPU
#include "audiosynth/synthesizer.cpp"
#endif

namespace rage {

#if __SYNTH_EDITOR
	extern s32 g_SynthEditorSlotId;
#endif

	AUD_IMPLEMENT_STATIC_WRAPPERS(audModularSynthSound);

	audModularSynthSound::~audModularSynthSound()
	{
		if (!IsBeingRemovedFromHierarchy())
		{
			for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
			{
				if(HasChildSound(i))
				{
					DeleteChild(i);
				}
			}
		}
		if(m_SynthSlotId != -1)
		{
			audPcmSourceInterface::Release(m_SynthSlotId);
			m_SynthSlotId = -1;
		}
		if(m_VariableSlotId != 0xff)
		{
			sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_VariableSlotId);
			m_VariableSlotId = 0xff;
		}
	}

#if !__SPU
	audModularSynthSound::audModularSynthSound()
	{
		m_SynthSlotId = -1;
		m_VariableSlotId = 0xff;
		m_NumEnvironmentSounds = 0;
		m_NumVariables = 0;
		m_AssetCost = audVoiceSettings::kDefaultVoiceCost;
	}

	bool audModularSynthSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
		{
			return false;
		}

		const ModularSynthSound *soundMetadata = reinterpret_cast<const ModularSynthSound*>(GetMetadata());

		m_SynthReference = soundMetadata->SynthDef;
		m_PresetReference = soundMetadata->Preset;

		if(soundMetadata->VirtualisationMode == kVirtualisationStop)
		{
			scratchInitParams->shouldStopWhenVirtual = true;
		}
		else if(soundMetadata->VirtualisationMode == kVirtualisationUcancellable)
		{
			scratchInitParams->isUncancellable = true;
		}

		if(soundMetadata->PlayBackTimeLimit <= 0.f)
		{
			m_PlayTimeLimitMs = ~0U;
		}
		else
		{
			m_PlayTimeLimitMs = static_cast<u32>(soundMetadata->PlayBackTimeLimit * 1000.f);
		}
		
		m_NumEnvironmentSounds = soundMetadata->numEnvironmentSounds;
		if(m_NumEnvironmentSounds == 0)
		{
			SetChildSound(0,SOUNDFACTORY.GetEnvironmentSound(this, initParams, scratchInitParams));
			if(!GetChildSound(0))
			{
				return false;
			}
			m_NumEnvironmentSounds = 1;
		}
		else
		{
			// Environment sounds specified in metadata
			for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
			{
				SetChildSound(i, SOUNDFACTORY.GetChildInstance(soundMetadata->EnvironmentSound[i].SoundRef, this, initParams, scratchInitParams, false));
				if(!GetChildSound(i) || !Verifyf(GetChildSound(i)->GetSoundTypeID() == EnvironmentSound::TYPE_ID, "ModularSynthSound with invalid environment sound reference: %s %u", GetName(), i))
				{
					return false;
				}
			}
		}

		m_ReleaseThroughSynth = AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_MODULARSYNTHSOUND_RELEASETHROUGHSYNTH) == AUD_TRISTATE_TRUE;

		Assign(m_NumVariables, soundMetadata->numExposedVariables);
		if(m_NumVariables > 0)
		{
			audSynthVariable *vars = (audSynthVariable*)sm_Pool.AllocateRequestedSettingsSlot(m_NumVariables * sizeof(audSynthVariable), m_InitParams.BucketId DEV_ONLY(, GetName()));
			if(!vars)
			{
				return false;
			}
			m_VariableSlotId = sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, vars);

			for(u32 i = 0; i < m_NumVariables; i++)
			{
				vars[i].keyHash = soundMetadata->ExposedVariable[i].SynthKey;
				vars[i].valPtr = _FindVariableUpHierarchy(soundMetadata->ExposedVariable[i].Variable);
				if(vars[i].valPtr)
				{
					vars[i].isPtr = true;
				}
				else
				{ 
					vars[i].val = soundMetadata->ExposedVariable[i].Value;
				}
			}
		}

#if RSG_PS3
		m_AssetCost = synthCore::GetCostEstimate(m_SynthReference);
#else
		// add overhead (more in assert builds) to the computed cost estimate
		m_AssetCost = ((__ASSERT ? 47 : 40) * synthCore::GetCostEstimate(m_SynthReference)) >> 5;
#endif

		return true;
	}
#endif // !__SPU


	void audModularSynthSound::ActionReleaseRequest(const u32 timeInMs)
	{
		// If a sound is stopped during its predelay it should revert to base sound release functionality; ie stop
		// immediately.
		if(m_SynthSlotId == -1 || !m_ReleaseThroughSynth || GetPredelayState() != AUD_PREDELAY_FINISHED)
		{
			BaseActionReleaseRequest(timeInMs);
			return;
		}
		else
		{
			SetWasSetToReleaseLastFrame(true);
			audPcmSourceInterface::SetParam(m_SynthSlotId, ATSTRINGHASH("IsReleasing", 0x491F6B3D), 1.f);
		}
	}

	audPrepareState audModularSynthSound::AudioPrepare(audWaveSlot *UNUSED_PARAM(slot), const bool checkStateOnly)
	{
		if(checkStateOnly)
		{
			// Act like we're prepared, since there's no actual delay
			return AUD_PREPARED;
		}
#if __SYNTH_EDITOR
		s32 allocatedSynthId = g_SynthEditorSlotId;
#else
		s32 allocatedSynthId = -1;
	
		allocatedSynthId = audPcmSourceInterface::Allocate(AUD_PCMSOURCE_SYNTHCORE);
		if(allocatedSynthId != -1)
		{
			audPcmSourceInterface::SetParam(allocatedSynthId, synthCorePcmSource::Params::CompiledSynthNameHash, m_SynthReference);
			if(HasPresetReference())
			{
				audPcmSourceInterface::SetParam(allocatedSynthId, synthCorePcmSource::Params::PresetNameHash, m_PresetReference);
			}

			if(m_ReleaseThroughSynth)
			{
				audPcmSourceInterface::SetParam(allocatedSynthId, ATSTRINGHASH("IsReleasing", 0x491F6B3D), 0.f);
			}
		}
#endif
		Assign(m_SynthSlotId, allocatedSynthId);
		if(m_SynthSlotId == -1 )
		{
			return AUD_PREPARE_FAILED;
		}

		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			audEnvironmentSound *es = GetEnvironmentSound(i);
			es->InitEnvironment(m_SynthSlotId, 0.f, kSoundLengthUnknown, false, kDefaultPeakLevel_u16, m_AssetCost);
			es->SetIsControllingPcmSource(i == 0);

		}

		return AUD_PREPARED;
	}

	s32 audModularSynthSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		if(isLooping)
		{
			*isLooping = true;
		}
		//	return m_WaveLengthMs;
		return kSoundLengthUnknown;
	}

	void audModularSynthSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		SoundAssert(m_NumEnvironmentSounds > 0);
		UpdateVariables();
		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			GetEnvironmentSound(i)->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}

	void audModularSynthSound::UpdateVariables()
	{
		if(m_NumVariables > 0)
		{
			audSynthVariable *vars = (audSynthVariable*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_VariableSlotId);
			SoundAssert(vars);
			for(u32 i = 0; i < m_NumVariables; i++)
			{
#if __BANK && !__SPU
				// Pick up RAVE changes for immediate values
				if(!vars[i].isPtr)
				{
					ModularSynthSound *soundMetadata = (ModularSynthSound*)GetMetadataForRAVEChanges();
					vars[i].val = soundMetadata->ExposedVariable[i].Value;
				}
#endif
				// push variable value down to synth
				float val = vars[i].val;
				if(vars[i].isPtr)
				{
					val = AUD_GET_VARIABLE(vars[i].valPtr);
				}
				
				audPcmSourceInterface::SetParam(m_SynthSlotId, vars[i].keyHash, val);
			}
		}
	}

	bool audModularSynthSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		UpdateVariables();

		u32 numFinished = 0;
	
		const bool shouldStopChildren = IsWaitingForChildrenToRelease();
		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			audEnvironmentSound *envSound = GetEnvironmentSound(i);
			if (shouldStopChildren)
			{
				envSound->_Stop();
			}

			if(envSound->GetPlayState() == AUD_SOUND_WAITING_TO_BE_DELETED || !envSound->_ManagedAudioUpdate(timeInMs, combineBuffer))
			{
				numFinished++;
			}
		}	

		const bool isFinished = (numFinished == m_NumEnvironmentSounds || GetCurrentPlayTime(timeInMs) >= m_PlayTimeLimitMs);
		return !isFinished;
	}

	void audModularSynthSound::AudioKill()
	{
		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			GetEnvironmentSound(i)->_ManagedAudioKill();
		}
	}

#if __SOUND_DEBUG_DRAW
	bool audModularSynthSound::DebugDrawInternal(const u32 UNUSED_PARAM(timeInMs), audDebugDrawManager &drawManager, const bool UNUSED_PARAM(drawDynamicChildren)) const
	{		
		char buf[64];
		audMetadataObjectInfo info;
		if(synthSynthesizer::GetMetadataManager().GetObjectInfo(m_SynthReference, ATSTRINGHASH("OPTIMISED", 0xC319250E),info))
		{
			formatf(buf, "Synth: %s", info.GetName());
			drawManager.DrawLine(buf);
		}		
		return true;
	}
#endif // __SOUND_DEBUG_DRAW

} // namespace rage
