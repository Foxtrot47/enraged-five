//
// audiosoundtypes/onstopsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "onstopsound.h"

#include "audiohardware/driverutil.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"


namespace rage {

	AUD_IMPLEMENT_STATIC_WRAPPERS(audOnStopSound);

	audOnStopSound::~audOnStopSound()
	{
		for (int i=0; i<3; i++)
		{
			if(HasChildSound(i))
			{
				DeleteChild(i);
			}
		}
	}

#if !__SPU
	audOnStopSound::audOnStopSound()
	{
		m_Stopping = false;
		m_OnStopTriggered = false;
		m_FinishedTriggered = false;
	}
	bool audOnStopSound::Init(const void *Metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(Metadata, initParams, scratchInitParams))
		{
			return false;
		}

		// this is set to true by default on sounds:
		SetShouldFinishWhenEnvelopeAttenuationFull(false);

		OnStopSound *onStopSoundData = (OnStopSound*)GetMetadata();

		// We don't allow simple release times on OnStopSounds - it's hard to do, involves hacks to audSound, and
		// isn't necessary.
		if (m_SimpleReleaseTime != kNoSimpleRelease)
		{
			SoundAssertMsg(0, "Setting a release time on an OnStopSound is not supported");
			m_SimpleReleaseTime = kNoSimpleRelease;
		}

		audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;

		SetChildSound(0,SOUNDFACTORY.GetChildInstance(onStopSoundData->ChildSoundRef, this, initParams, scratchInitParams, false));
		if(!GetChildSound(0))
		{
			return false;
		}

		if (onStopSoundData->StopSoundRef.IsValid())
		{
			*scratchInitParams = scratchInitParamsCopy;
			SetChildSound(1, SOUNDFACTORY.GetChildInstance(onStopSoundData->StopSoundRef, this, initParams, scratchInitParams, false));
		}
		if (onStopSoundData->FinishedSoundRef.IsValid())
		{
			*scratchInitParams = scratchInitParamsCopy;
			SetChildSound(2, SOUNDFACTORY.GetChildInstance(onStopSoundData->FinishedSoundRef, this, initParams, scratchInitParams, false));
		}	

		return true;
	}
#endif // !__SPU

	audPrepareState audOnStopSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
	{
		SoundAssert(GetChildSound(0));
		if (!GetChildSound(0))
		{
			return AUD_PREPARE_FAILED;
		}

		GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset());

		audPrepareState state;
		bool finishedPreparing = true;

		for(int i = 0; i < 3; i++)
		{
			if(GetChildSound(i))
			{
				state = GetChildSound(i)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
				if(state == AUD_PREPARE_FAILED)
				{
					// bail out if any of our children have failed to prepare
					return AUD_PREPARE_FAILED;
				}
				else if(state == AUD_PREPARING)
				{
					finishedPreparing = false;
				}
			}
		}

		if(finishedPreparing)
		{
			return AUD_PREPARED;
		}
		else
		{
			return AUD_PREPARING;
		}
	}

	void audOnStopSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		audSound *childSound = GetChildSound(0);
		if(childSound)
		{
			childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}

	bool audOnStopSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		bool childStillPlaying = false;
		bool onStopStillPlaying = false;
		bool finishedStillPlaying = false;

		audSound *child0 = GetChildSound(0);
		audSound *child1 = GetChildSound(1);
		audSound *child2 = GetChildSound(2);

		if (IsToBeStopped() && !m_Stopping)
		{
			// We've just been asked to stop, but haven't yet kicked off our child sound
			SoundAssert(!m_OnStopTriggered);
			m_Stopping = true;
			m_OnStopTriggered = true;
			if (child1)
			{
                child1->_ManagedAudioPlay(timeInMs, combineBuffer);
				child1 = GetChildSound(1);
			}
		}

		if(child0 && child0->GetPlayState() == AUD_SOUND_PLAYING)
		{
			childStillPlaying = child0->_ManagedAudioUpdate(timeInMs, combineBuffer);
			if (!childStillPlaying)
			{
//				SoundAssert(!m_OnStopTriggered);
				// Our child sound has finished, so play our OnFinished sound
				m_FinishedTriggered = true;
				if (child2)
				{
					child2->_ManagedAudioPlay(timeInMs, combineBuffer);
					child2 = GetChildSound(2);
				}
			}
/*
			else
			{
				// See if our envelope is finished, and hence most sounds would stop, but we're carrying on.
			//	if(m_HasEnvelopeFinished)
				{
 					GetChildSound(0)->_ManagedAudioKill(); // don't do this gracefully, as it's silent anyway.
					m_FinishedTriggered = true;
					if (GetChildSound(2))
					{
						GetChildSound(2)->_ManagedAudioPlay(timeInMs);
					}
				}
			}
*/
		}

		// Update OnStop and Finished if they're playing (is this valid? We might only have triggered them this frame?)
		if (child1 && m_OnStopTriggered && (child1->GetPlayState() == AUD_SOUND_PLAYING))
		{
			onStopStillPlaying = child1->_ManagedAudioUpdate(timeInMs, combineBuffer);
		}

		if (child2 && m_FinishedTriggered && (child2->GetPlayState() == AUD_SOUND_PLAYING))
		{
			finishedStillPlaying = child2->_ManagedAudioUpdate(timeInMs, combineBuffer);
		}

		// See if we're entirely done, and return false if we are.
		if (!childStillPlaying && !onStopStillPlaying && !finishedStillPlaying)
		{
			_ManagedAudioKill();
			return false;
		}
		else
		{
			return true;
		}
	}


	void audOnStopSound::ManagedAudioStopChildren()
	{
		// We've been asked to stop our children, because we've had Stop() called, but don't have our own release env
		// We'll have kicked off our OnStopSound separately, so here, just stop our one valid child (need to override,
		// or base sound would also kill OnStop and OnFinished sounds).
		audSound *child0 = GetChildSound(0);
		if (child0 && child0->GetPlayState() == AUD_SOUND_PLAYING)
		{
			child0->_Stop();
		}
	}

	s32 audOnStopSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		if (HasChildSound(0))
		{
			return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			return 0;
		}
	}

	void audOnStopSound::AudioKill()
	{
		for (int i=0; i<3; i++)
		{
			if (HasChildSound(i))
			{
				GetChildSound(i)->_ManagedAudioKill();
			}
		}
	}
} // namespace rage
