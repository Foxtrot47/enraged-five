// 
// audiosoundtypes/automationsound.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "automationsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/mp3util.h"
#include "system/endian.h"
#include "system/magicnumber.h"

#include "profile/profiler.h"

namespace rage 
{
	AUD_IMPLEMENT_STATIC_WRAPPERS(audAutomationSound);

	audAutomationSound::~audAutomationSound()
	{
		for(u32 i = 0; i < kMaxChildren; i++)
		{
			if(HasChildSound(i))
			{
				DeleteChild(i);
			}
		}
		if(m_SequenceStateSlot != 0xff)
		{
			sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_SequenceStateSlot);
			m_SequenceStateSlot = 0xff;
		}
#if __PS3
		if(m_DataCacheSlot != 0xff)
		{
			sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_DataCacheSlot), m_InitParams.BucketId);
			m_DataCacheSlot = 0xff;
		}
#endif
	}

#if !__SPU
	PF_PAGE(AutomationSound,"Automation Sound");
	PF_GROUP(AutomationSound);
	PF_LINK(AutomationSound, AutomationSound);
	PF_VALUE_FLOAT(Channel0, AutomationSound);
	PF_VALUE_FLOAT(Channel1, AutomationSound);
	PF_VALUE_FLOAT(Channel2, AutomationSound);
	PF_VALUE_FLOAT(Channel3, AutomationSound);
	PF_VALUE_FLOAT(Channel4, AutomationSound);
	PF_VALUE_FLOAT(Channel5, AutomationSound);
	PF_VALUE_FLOAT(Channel6, AutomationSound);
	PF_VALUE_FLOAT(Channel7, AutomationSound);
	PF_VALUE_FLOAT(Channel8, AutomationSound);
	PF_VALUE_FLOAT(Channel9, AutomationSound);
	PF_VALUE_FLOAT(Channel10, AutomationSound);
	PF_VALUE_FLOAT(Channel11, AutomationSound);
	PF_VALUE_FLOAT(Channel12, AutomationSound);
	PF_VALUE_FLOAT(Channel13, AutomationSound);
	PF_VALUE_FLOAT(Channel14, AutomationSound);
	PF_VALUE_FLOAT(Channel15, AutomationSound);

	audAutomationSound::audAutomationSound()
	{
		m_SequenceStateSlot = 0xff;
		PS3_ONLY(m_DataCacheSlot = 0xff);
	}

	bool audAutomationSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
			return false;

		AutomationSound *soundData = (AutomationSound*)GetMetadata();

		if(soundData->numVariableOutputs > kMaxOutputs)
		{
			return false;
		}

		SetHasDynamicChildren();

		// note: when running -rave bank references are stored as hashes in metadata, so we need to resolve them
		// at runtime.
		u32 bankNameIndex = SOUNDFACTORY.GetBankIndexFromMetadataRef(soundData->DataRef.BankName);
		if(bankNameIndex >= AUD_INVALID_BANK_ID)
		{
			audErrorf("AutomationSound %s failed to find bank name index for MIDI wave %u (bank ref %u)", GetName(), soundData->DataRef.WaveName, soundData->DataRef.BankName);
			return false;
		}

		Assign(m_BankNameIndex, bankNameIndex);
		m_MidiWaveNameHash = soundData->DataRef.WaveName;

		CompileTimeAssert(sizeof(AutomationSequenceState) <= sizeof(audRequestedSettings));
		AutomationSequenceState *stateSlot = (AutomationSequenceState*)sm_Pool.AllocateRequestedSettingsSlot(sizeof(AutomationSequenceState), m_InitParams.BucketId DEV_ONLY(,GetName()));
		if(!stateSlot)
		{
			return false;
		}
		Assign(m_SequenceStateSlot, sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, stateSlot));

		NoteMap *noteMap = SOUNDFACTORY.GetMetadataManager().GetObject<NoteMap>(soundData->NoteMap);

		if(!noteMap)
		{
			SetChildSound(0,SOUNDFACTORY.GetChildInstance(soundData->SoundRef, this, initParams, scratchInitParams, false));
		}
		else
		{		
			stateSlot->ScratchInitParams = *scratchInitParams; 
			if(noteMap->numRanges > AutomationSequenceState::kMaxNoteRanges)
			{
				audErrorf("AutomationSound %s note map is too large, has %u ranges (max is %u)", GetName(), noteMap->numRanges, AutomationSequenceState::kMaxNoteRanges);
				return false;
			}
			stateSlot->NumNoteRanges = noteMap->numRanges;
			for(u32 i = 0; i < stateSlot->NumNoteRanges; i++)
			{
				stateSlot->noteRanges[i].soundRef = noteMap->Range[i].SoundRef;
				stateSlot->noteRanges[i].firstNoteId = noteMap->Range[i].FirstNoteId;
				stateSlot->noteRanges[i].lastNoteId = noteMap->Range[i].LastNoteId;
				stateSlot->noteRanges[i].mode = noteMap->Range[i].Mode;
			}
		}

		m_PlaybackRate = soundData->PlaybackRate + audEngineUtil::GetRandomNumberInRange(-soundData->PlaybackRateVariance, soundData->PlaybackRateVariance);
		m_PlaybackRateVariable = _FindVariableUpHierarchy(soundData->PlaybackRateVariable);
		m_NumOutputs = soundData->numVariableOutputs;
		m_IsLooping = AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_AUTOMATIONSOUND_LOOPSEQUENCE) == AUD_TRISTATE_TRUE;

		for(u32 i = 0; i < m_NumOutputs; i++)
		{
			Assign(stateSlot->ChannelIds[i], soundData->VariableOutputs[i].ChannelId);
			stateSlot->OutputVariables[i] = _FindVariableUpHierarchy(soundData->VariableOutputs[i].OutputVariable);
			if(!audVerifyf(stateSlot->OutputVariables[i], "%s: Failed to find output variable %u (channel %u / %u)", GetName(), soundData->VariableOutputs[i].OutputVariable, soundData->VariableOutputs[i].ChannelId, i))
			{
				return false;
			}
		}

#if __PS3
		u8 *cacheSlot = (u8*)sm_Pool.AllocateSoundSlot(sm_Pool.GetSoundSlotSize(), m_InitParams.BucketId, true);
		if(!cacheSlot)
		{
			return false;
		}
		Assign(m_DataCacheSlot, sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, cacheSlot));
		// Start with an invalidated cache
		m_DataCacheOffset = ~0U;
#endif

		m_Data = NULL;
		m_DataSize = 0;

		m_CurrentOffset = 0;
		m_TimeOffset = 0.f;
		m_LastEventTime = 0;

		return true;
	}

	

#endif // !__SPU

	const audAutomationSound::AutomationSequenceState *audAutomationSound::GetSequenceState()
	{
		if(m_SequenceStateSlot != 0xff)
		{
			return (const AutomationSequenceState*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_SequenceStateSlot);
		}
		return NULL;
	}

	audPrepareState audAutomationSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
	{
		audWaveSlot *staticSlot = audWaveSlot::FindLoadedBankWaveSlot(m_BankNameIndex);
		if(staticSlot && (waveSlot == NULL || staticSlot->IsStatic()))
		{
			waveSlot = staticSlot;
		}
		if(waveSlot == NULL)
		{
			audWarningf("Host wave slot not found for AutomationSound sound - %s", GetName());
			return AUD_PREPARE_FAILED;
		}

		const audWaveSlot::audWaveSlotLoadStatus status = waveSlot->GetAssetLoadingStatus(m_BankNameIndex, m_MidiWaveNameHash);
		if(checkStateOnly)
		{
			switch(status)
			{
			case audWaveSlot::LOADED:
				if(GetChildSound(0))
				{
					return GetChildSound(0)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
				}
				else
				{
					return AUD_PREPARED;
				}
			case audWaveSlot::FAILED:
				return AUD_PREPARE_FAILED;
			default:
				return AUD_PREPARING;
			}
		}
		else
		{
			switch(status)
			{
			case audWaveSlot::LOADED:		
				{
					if(!m_MidiRef.Init(waveSlot, m_MidiWaveNameHash))
					{
						return AUD_PREPARE_FAILED;
					}

					m_Data = (u8*)m_MidiRef.FindChunk(ATSTRINGHASH("MID",0x71DE4C68), m_DataSize);
					if(!m_Data)
					{
						return AUD_PREPARE_FAILED;
					}

					if(!ParseHeader())
					{
						return AUD_PREPARE_FAILED;
					}

					if(GetChildSound(0))
					{
						GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
						return GetChildSound(0)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
					}
					else
					{
						return AUD_PREPARED;
					}
				}				
			case audWaveSlot::LOADING:
				return AUD_PREPARING;
			case audWaveSlot::FAILED:
				return AUD_PREPARE_FAILED;
			case audWaveSlot::NOT_REQUESTED:
				if(GetCanDynamicallyLoad())
				{
					bool bSuccess = waveSlot->LoadAsset(m_BankNameIndex, m_MidiWaveNameHash, GetWaveLoadPriority());
					SoundAssert(bSuccess);
					return bSuccess ? AUD_PREPARING : AUD_PREPARE_FAILED;
				}
				// else - fall through to default
			default:
				return AUD_PREPARE_FAILED;
			}	
		}
	}

	void audAutomationSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		m_LastUpdateTime = timeInMs;

		Process(0, GetSequenceState());

		audSound *childSound = GetChildSound(0);
		if (childSound)
		{
			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			// Ripple down start offset, as we may have an audible sound below us that makes real sense of it.
			childSound->SetStartOffset((s32)GetPlaybackStartOffset());
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}

	bool audAutomationSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		const u32 timeStepMs = (timeInMs - m_LastUpdateTime);
		m_LastUpdateTime = timeInMs;

		const AutomationSequenceState *sequenceState = GetSequenceState();
		Process(timeStepMs, sequenceState);

		if(sequenceState->NumNoteRanges > 0)
		{
			s32 basePitch = combineBuffer.Pitch;
			f32 baseVolume = combineBuffer.Volume;
			for(u32 i = 0; i < kMaxChildren; i++)
			{
				if(HasChildSound(i))
				{
					const s32 childPitch = (s32)m_ChildNoteDelta[i]*100;
					Assign(combineBuffer.Pitch, basePitch + childPitch);
					const f32 childVol = audDriverUtil::ComputeDbVolumeFromLinear(m_ChildVelocity[i] / 127.f);
					combineBuffer.Volume = baseVolume + childVol;

					if(m_IsPreparing.IsSet(i))
					{
						audPrepareState state = GetChildSound(i)->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority());
						if(state == AUD_PREPARED)
						{
							m_IsPreparing.Clear(i);
							GetChildSound(i)->_ManagedAudioPlay(timeInMs, combineBuffer);
						}
					}
					else if(!GetChildSound(i)->_ManagedAudioUpdate(timeInMs, combineBuffer))
					{
						DeleteChild(i);
					}					
				}
			}

			if(AtEnd() && !m_IsLooping)
			{
				return false;
			}
			return true;
		}
		else
		{
			if (GetChildSound(0))
			{	
				return GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
			}
			else
			{
				return false;
			}
		}
	}

	void audAutomationSound::AudioKill()
	{
		for(u32 i = 0; i < kMaxChildren; i++)
		{
			if(HasChildSound(i))
			{
				GetChildSound(i)->_ManagedAudioKill();
			}	
		}
	}

	s32 audAutomationSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		if(GetChildSound(0))
		{
			return GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			// We don't support lengths and start offsets on logical sounds with no children
			return kSoundLengthUnknown;
		}
	}

	void audAutomationSound::Process(const u32 timeStepMs, const AutomationSequenceState *sequenceState)
	{
		// 960 ticks per beat
		// 120 beats per minute
		// 1920 ticks per second

		float timeStepTicks = (timeStepMs * 1.92f) * m_PlaybackRate;
		if(m_PlaybackRateVariable)
		{
			timeStepTicks *= AUD_GET_VARIABLE(m_PlaybackRateVariable);
		}

		u32 now = (u32)(m_TimeOffset + timeStepTicks);

		u32 timeSinceLastEvent = now - m_LastEventTime;

		m_TimeOffset += timeStepTicks;

		while(!AtEnd())
		{
			u32 eventWaitTime = 0;
			u32 peakOffset = m_CurrentOffset;
			if(!ReadVariableLength(eventWaitTime))
			{
				return;
			}
			
			if(timeSinceLastEvent >= eventWaitTime)
			{
				m_LastEventTime = now;
				timeSinceLastEvent = 0;
				ProcessEvent(sequenceState);

				if(m_CurrentOffset >= m_DataSize && m_IsLooping)
				{
					// Skip header
					m_CurrentOffset = m_DataSize - m_MidiDataSize;
					// reset timers
					m_TimeOffset = 0.f;
					m_LastEventTime = 0;
					now = 0;
					timeSinceLastEvent = 0;

					for(u32 i = 0; i < kMaxChildren; i++)
					{
						if(HasChildSound(i))
						{
							GetChildSound(i)->_Release();
						}
					}
				}
			}
			else
			{
				m_CurrentOffset = peakOffset;
				return;
			}
		}
	}

	void audAutomationSound::ProcessEvent(const AutomationSequenceState *sequenceState)
	{
		u8 eventType;
		enum EventTypes
		{
			NoteOff = 0x80,
			NoteOn = 0x90,
			NoteAftertouch = 0xA0,
			Controller = 0xB0,
			ProgramChange = 0xC0,
			ChannelAftertouch = 0xD0,
			PitchBend = 0xE0,
			MetaEvent = 0xF0,
		};
		if(ReadByte(eventType))
		{
			const u32 channel = eventType & 0xF;

			switch(eventType & 0xF0)
			{
			case PitchBend:
				{
					u32 val;
					if(Read14Bit(val))
					{
						SetOutputValue(channel, val, sequenceState);
					}
				}
				break;
			case NoteOn:
			case NoteOff:
				{
					if(sequenceState)
					{
						u32 noteId;
						if(!Read7Bit(noteId))
						{
							return;
						}

						u32 velocity;
						if(!Read7Bit(velocity))
						{
							return;
						}

						if((eventType&0xF0) == NoteOn && velocity > 0)
						{
							ProcessNoteOn(channel, noteId, velocity, sequenceState);
						}
						else
						{
							ProcessNoteOff(channel, noteId, velocity, sequenceState);
						}
					}
				}
				break;
			case NoteAftertouch:
				{
					u32 noteId;
					if(!Read7Bit(noteId))
					{
						return;
					}

					u32 velocity;
					if(!Read7Bit(velocity))
					{
						return;
					}
					ProcessNoteAftertouch(channel, noteId, velocity, sequenceState);
				}
				break;
			case Controller:
				{
					// for now simply ignore
					m_CurrentOffset += 2; // skip controller type and value
				}
				break;
			case ProgramChange:
				{
					// for now simply ignore
					m_CurrentOffset++; // skip program number
				}
				break;
			case ChannelAftertouch:
				{
					// for now simply ignore
					m_CurrentOffset++; // skip program number
				}
				break;
			case MetaEvent:
				{
					// Skip type byte
					m_CurrentOffset++;
					u32 lengthBytes;
					if(!ReadVariableLength(lengthBytes))
					{
						return;
					}
					m_CurrentOffset += lengthBytes;
				}
				break;
			default:
				audErrorf("Unhandled MIDI message: %02X at offset %u", eventType, m_CurrentOffset);
				m_CurrentOffset = m_DataSize;
				break;
			}
		}
	}

	void audAutomationSound::ProcessNoteOn(const u32 UNUSED_PARAM(channel), const u32 noteId, const u32 velocity, const AutomationSequenceState *sequenceState)
	{
		u32 freeSlot = kMaxChildren;
		for(u32 i = 0; i < kMaxChildren; i++)
		{
			if(!HasChildSound(i) && !m_IsPreparing.IsSet(i))
			{
				freeSlot = i;
				break;
			}
		}
		if(freeSlot >= kMaxChildren)
		{
			// Out of room, and we don't currently implement any sort of voice stealing
			return;
		}

		u32 noteDelta;
		const audMetadataRef soundRef = GetSoundRefForNote(noteId, noteDelta, sequenceState);
		if(soundRef.IsValid())
		{
			Assign(m_ChildNoteDelta[freeSlot], noteDelta);
			Assign(m_ChildVelocity[freeSlot], velocity);
			Assign(m_ChildNote[freeSlot], noteId);

			RequestChildSoundFromOffset(soundRef, freeSlot, &sequenceState->ScratchInitParams);
			m_IsPreparing.Set(freeSlot);
		}
	}

	void audAutomationSound::ProcessNoteOff(const u32 UNUSED_PARAM(channelId), const u32 noteId, const u32 UNUSED_PARAM(velocity), const AutomationSequenceState *UNUSED_PARAM(sequenceState))
	{
		for(u32 i = 0; i < kMaxChildren; i++)
		{
			if(HasChildSound(i) && m_ChildNote[i] == noteId)
			{
				GetChildSound(i)->_Release();
			}
		}
	}

	void audAutomationSound::ProcessNoteAftertouch(const u32 UNUSED_PARAM(channelId), const u32 noteId, const u32 velocity, const AutomationSequenceState *UNUSED_PARAM(sequenceState))
	{
		for(u32 i = 0; i < kMaxChildren; i++)
		{
			if(HasChildSound(i) && m_ChildNote[i] == noteId)
			{
				Assign(m_ChildVelocity[i], velocity);
			}
		}
	}

	audMetadataRef audAutomationSound::GetSoundRefForNote(const u32 noteId, u32 &noteDelta, const AutomationSequenceState *state) const
	{
		for(u32 i = 0; i < state->NumNoteRanges; i++)
		{
			if(noteId >= state->noteRanges[i].firstNoteId && noteId <= state->noteRanges[i].lastNoteId)
			{
				if(state->noteRanges[i].mode == NOTE_RANGE_TRACK_PITCH)
				{
					noteDelta = noteId - state->noteRanges[i].firstNoteId;
				}
				else
				{
					noteDelta = 0;
				}
				return state->noteRanges[i].soundRef;
			}
		}
		return audMetadataRef::Create(~0U);
	}

	void audAutomationSound::SetOutputValue(const u32 channelId, const u32 val, const AutomationSequenceState *state)
	{
		const float scalar = 1.f / 16384.f;
		const float floatVal = val * scalar;
		for(u32 i = 0; i < kMaxOutputs; i++)
		{
			if(state->ChannelIds[i] == channelId && state->OutputVariables[i])
			{
				AUD_SET_VARIABLE(state->OutputVariables[i], floatVal);
			}
		}

#if !__SPU
		if(m_InitParams.IsAuditioning)
		{
			switch(channelId)
			{
			case 0:
				PF_SET(Channel0, floatVal);
				break;
			case 1:
				PF_SET(Channel1, floatVal);
				break;
			case 2:
				PF_SET(Channel2, floatVal);
				break;
			case 3:
				PF_SET(Channel3, floatVal);
				break;
			case 4:
				PF_SET(Channel4, floatVal);
				break;
			case 5:
				PF_SET(Channel5, floatVal);
				break;
			case 6:
				PF_SET(Channel6, floatVal);
				break;
			case 7:
				PF_SET(Channel7, floatVal);
				break;
			case 8:
				PF_SET(Channel8, floatVal);
				break;
			case 9:
				PF_SET(Channel9, floatVal);
				break;
			case 10:
				PF_SET(Channel10, floatVal);
				break;
			case 11:
				PF_SET(Channel11, floatVal);
				break;
			case 12:
				PF_SET(Channel12, floatVal);
				break;
			case 13:
				PF_SET(Channel13, floatVal);
				break;
			case 14:
				PF_SET(Channel14, floatVal);
				break;
			case 15:
				PF_SET(Channel15, floatVal);
				break;
			}
		}
#endif
	}

	const u8 *audAutomationSound::GetDataPtr() const
	{
#if __SPU
		TrapLT(m_CurrentOffset, m_DataCacheOffset);
		TrapGT(m_CurrentOffset - m_DataCacheOffset, sm_Pool.GetSoundSlotSize());
		const u8 *basePtr = (const u8*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_DataCacheSlot);
		return basePtr + (m_CurrentOffset - m_DataCacheOffset);
#else
		return m_Data + m_CurrentOffset;
#endif
	}

#if __SPU
	void audAutomationSound::PrefetchData(const size_t sizeBytes)
	{
		const u32 slotSize = sm_Pool.GetSoundSlotSize();
		if(m_CurrentOffset >= m_DataCacheOffset && (m_CurrentOffset - m_DataCacheOffset) + sizeBytes <= slotSize)
		{
			return;
		}
		// we need to update our cache
		const u32 fetchOffset = m_CurrentOffset&~15;
		u8 *ls = (u8*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_DataCacheSlot);
		sysDmaGetAndWait(ls, (u32)(m_Data + fetchOffset), slotSize, 5);
		m_DataCacheOffset = fetchOffset;
	}
#endif

	bool audAutomationSound::ReadDword(u32 &res)
	{
		if(m_CurrentOffset + 4 <= m_DataSize)
		{
			SPU_ONLY(PrefetchData(sizeof(u32)));
			
			res = audMp3Util::LoadU32Unaligned(GetDataPtr());
			m_CurrentOffset += sizeof(u32);
			res = sysEndian::Swap(res);
			return true;
		}
		return false;
	}

	bool audAutomationSound::ReadWord(u16 &res)
	{
		if(m_CurrentOffset + sizeof(u16) <= m_DataSize)
		{
			SPU_ONLY(PrefetchData(sizeof(u16)));
			res = audMp3Util::LoadU16Unaligned(GetDataPtr());
			m_CurrentOffset += sizeof(u16);
			res = sysEndian::Swap(res);
			return true;
		}
		return false;
	}

	bool audAutomationSound::ReadByte(u8 &res)
	{
		if(m_CurrentOffset + 1 <= m_DataSize)
		{
			SPU_ONLY(PrefetchData(sizeof(u8)));
			res = *GetDataPtr();
			m_CurrentOffset++;
			return true;
		}
		return false;
	}

	bool audAutomationSound::Read7Bit(u32 &res)
	{
		u8 r;
		if(!ReadByte(r))
		{
			return false;
		}
		res = r & 0x7F;
		return true;
	}

	bool audAutomationSound::ReadVariableLength(u32 &res)
	{
		res = 0;
		for(u32 i = 0; i < 4; i++)
		{
			u8 b;
			if(!ReadByte(b))
			{
				return false;
			}
			
			res = (res << 7) + (b & 0x7f);

			// MSB signals whether to continue
			if(!(b & 0x80))
			{
				// We're finished decoding this value.
				return true;
			}
		}
		return true;
	}

	bool audAutomationSound::Read14Bit(u32 &val)
	{
		u8 b0,b1;
		if(!ReadByte(b0))
		{
			return false;
		}
		if(!ReadByte(b1))
		{
			return false;
		}

		val = ((b1&0x7F)<<7) | (b0&0x7F);

		return true;		
	}

	bool audAutomationSound::ParseHeader()
	{
		u32 header;
		// MThd
		if(!ReadDword(header) || header != MAKE_MAGIC_NUMBER('d','h','T','M'))
		{
			audErrorf("Invalid magic number %08X (expected %08X)", header, MAKE_MAGIC_NUMBER('d','h','T','M'));
			return false;
		}

		u32 headerSize;
		if(!ReadDword(headerSize) || headerSize != 6) // MIDI header is always 6 bytes
		{
			audErrorf("Header size mismatch; got %u rather than 6", headerSize);
			return false;
		}

		u32 numMidiTracks;
		if(!ReadDword(numMidiTracks))
		{
			return false;
		}
		if(!audVerifyf(numMidiTracks == 1, "Invalid type 0 MIDI data; %u tracks", numMidiTracks))
		{
			return false;
		}

		u16 ticksPerBeat;
		if(!ReadWord(ticksPerBeat))
		{
			return false;
		}

		// Playback is calculated at 960 ticks per beat, so scale playback rate based on
		// asset
		m_PlaybackRate *= (ticksPerBeat / 960.f);

		// MTrk
		if(!ReadDword(header) || header != MAKE_MAGIC_NUMBER('k','r','T','M'))
		{
			audErrorf("Invalid magic number %08X (expected %08X)", header, MAKE_MAGIC_NUMBER('k','r','T','M'));
			return false;
		}

		u32 midiDataSize;
		if(!ReadDword(midiDataSize))
		{
			return false;
		}

		m_MidiDataSize = m_DataSize - m_CurrentOffset;
		if(midiDataSize != m_MidiDataSize)
		{
			audWarningf("%s: MIDI data size mismatch: %u in header vs %u actual", GetName(), midiDataSize, m_MidiDataSize);
		}		


		return true;
	}

#if __SOUND_DEBUG_DRAW
	bool audAutomationSound::DebugDrawInternal(const u32 UNUSED_PARAM(timeInMs), audDebugDrawManager &drawManager, const bool UNUSED_PARAM(drawDynamicChildren)) const
	{
		char buf[64];
		formatf(buf, "Ticks: %.3f, bytes: %u / %u", m_TimeOffset, m_CurrentOffset, m_DataSize);
		drawManager.DrawLine(buf);

		return true;
	}
#endif // __SOUND_DEBUG_DRAW

} // namespace rage
