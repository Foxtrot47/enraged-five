//
// audiosoundtypes/directionalsound.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DIRECTIONAL_SOUND_H
#define AUD_DIRECTIONAL_SOUND_H

#include "sound.h"
#include "sounddefs.h"
#include "audioengine/widgets.h"
#include "vectormath/classes.h"


namespace rage
{

	// PURPOSE
	//  A directional sound applies a volume cone which will follow the orientation of the sound
	class audDirectionalSound : public audSound
	{
	public:
		
		~audDirectionalSound();

		AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
		audDirectionalSound();
		// PURPOSE
		//  Implements base class function for this sound.
		bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

		// PURPOSE
		//  Implements base class function for this sound.
		void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
		// PURPOSE
		//  Implements base class function for this sound.
		void AudioKill();
		// PURPOSE
		//  Implements base class function for this sound.
		s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

		AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

		// PURPOSE
		//  Implements base class function for this sound.
		audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

	private:

		// PURPOSE
		// Applies the volume attenuation based on orientation
		void ApplyConeToChild(audSoundCombineBuffer &combineBuffer);
		audVolumeConeLite m_VolumeCone;
	};

} // namespace rage

#endif //AUD_DIRECTIONAL_SOUND_H
