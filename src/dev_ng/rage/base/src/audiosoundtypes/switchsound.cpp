// 
// audiosoundtypes/switchsound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "switchsound.h"
#include "audiohardware\channel.h"

#include "audioengine/soundfactory.h"

namespace rage
{
AUD_IMPLEMENT_STATIC_WRAPPERS(audSwitchSound);

audSwitchSound::~audSwitchSound()
{
	if(!IsBeingRemovedFromHierarchy() && GetChildSound(0))
	{
		DeleteChild(0);
	}

	if(m_StorageSlotIndex != 0xff)
	{
		sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_StorageSlotIndex), m_InitParams.BucketId);
		m_StorageSlotIndex = 0xff;
	}
}

#if !__SPU
audSwitchSound::audSwitchSound()
{
	m_PreparingChild = false;
	m_WaitingForChild = false;
	m_StorageSlotIndex = 0xff;
}

bool audSwitchSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	m_ScratchInitParams = *scratchInitParams;
	SwitchSound *switchSoundData = (SwitchSound*)GetMetadata();

	m_Variable = _FindVariableUpHierarchy(switchSoundData->Variable);
	if(!m_Variable)
	{
		audWarningf("audSwitchSound: invalid switch variable - %s variableHash %u", GetName(),switchSoundData->Variable);
		return false;
	}

	SetHasDynamicChildren();

	const u32 maxRefsInSlot = sm_Pool.GetSoundSlotSize() / sizeof(audMetadataRef);
	m_ShouldScaleInput = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SWITCHSOUND_SCALEINPUT) == AUD_TRISTATE_TRUE);
	SoundAssert(switchSoundData->numSoundRefs <= maxRefsInSlot);
	if(switchSoundData->numSoundRefs > maxRefsInSlot)
	{
		return false;
	}
	m_NumSoundRefs = switchSoundData->numSoundRefs;
	if(m_NumSoundRefs <= g_MaxSoundRefsInBasicSwitchSound)
	{
		for(u32 i = 0; i < m_NumSoundRefs; i++)
		{
			m_SoundRefs[i] = switchSoundData->SoundRef[i].SoundId;
		}
	}
	else
	{
		m_NumSoundRefs = Min((u8)maxRefsInSlot,switchSoundData->numSoundRefs);
		audMetadataRef *refs = (audMetadataRef*)sm_Pool.AllocateSoundSlot(sizeof(audMetadataRef) * m_NumSoundRefs, m_InitParams.BucketId, true);
		if(!refs)
		{
			return false;
		}

		m_StorageSlotIndex = sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, refs);
		for(u32 i = 0; i < m_NumSoundRefs; i++)
		{
			refs[i] = switchSoundData->SoundRef[i].SoundId;
		}
	}	
	
	return true;
}
#endif // !__SPU

void audSwitchSound::AudioPlay(u32 UNUSED_PARAM(timeInMs), audSoundCombineBuffer &combineBuffer)
{
	u32 index;

	if(m_ShouldScaleInput)
	{
		index = (u32)floor(Clamp(AUD_GET_VARIABLE(m_Variable), 0.0f, 1.0f) * m_NumSoundRefs);
		if(index >= m_NumSoundRefs)
		{
			index = (m_NumSoundRefs-1);
		}
	}
	else
	{
		index = (u32)floor(AUD_GET_VARIABLE(m_Variable));
	}

	SoundAssert(index < m_NumSoundRefs);
	
	if(index < m_NumSoundRefs)
	{
		m_CachedCombineBuffer = combineBuffer;
		if(m_NumSoundRefs <= g_MaxSoundRefsInBasicSwitchSound)
		{
			RequestChildSoundFromOffset(m_SoundRefs[index], 0, &m_ScratchInitParams);
		}
		else
		{
			SoundAssert(m_StorageSlotIndex != 0xff);
			audMetadataRef *refs = (audMetadataRef*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_StorageSlotIndex);
			RequestChildSoundFromOffset(refs[index], 0, &m_ScratchInitParams);
			sm_Pool.DeleteSound(refs, m_InitParams.BucketId);
			m_StorageSlotIndex = 0xff;
		}
		m_WaitingForChild = true;
	}
}

void audSwitchSound::ChildSoundCallback(const u32 timeInMs, const u32 childIndex)
{	
	SoundAssert(childIndex == 0);
	audSound* childSound = GetChildSound(childIndex);
	if(childSound)
	{
		childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		if(childSound->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority())!=AUD_PREPARED)
		{
			m_PreparingChild = true;
		}
		else
		{
			childSound->_ManagedAudioPlay(timeInMs, m_CachedCombineBuffer);
		}		
	}
	m_WaitingForChild = false;
}

bool audSwitchSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound = GetChildSound(0);
	if(m_PreparingChild)
	{
		SoundAssert(childSound);
		audPrepareState state = childSound->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority());

		if(state == AUD_PREPARE_FAILED)
		{
			DeleteChild(0);
			m_PreparingChild = false;
			return false;
		}
		else if(state == AUD_PREPARED)
		{
			m_PreparingChild = false;
			childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
			RemoveMyselfFromHierarchy();
		}
		// we're preparing so keep playing
		return true;
	}
	else
	{
		// we're not preparing, if we dont have a sound at this point we're done.
		if ((childSound && childSound->_ManagedAudioUpdate(timeInMs, combineBuffer)) || m_WaitingForChild)
		{
			if(!m_WaitingForChild)
			{
				RemoveMyselfFromHierarchy();
			}
			return (true);
		}
		else
		{
			return (false);
		}	
	}
}

void audSwitchSound::AudioKill()
{
	audSound* childSound = GetChildSound(0);
	if(childSound)
	{
		childSound->_ManagedAudioKill();
	}
}

audPrepareState audSwitchSound::AudioPrepare(audWaveSlot *UNUSED_PARAM(slot), bool UNUSED_PARAM(checkStateOnly))
{
	//we have to prepare during Play since we dont want to prepare every
	//possible child sound
	return AUD_PREPARED;
}

s32 audSwitchSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound* childSound = GetChildSound(0);
	if(childSound)
	{
		return childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
	}
	else
	{
		return kSoundLengthUnknown;
	}
}

} // namespace rage
