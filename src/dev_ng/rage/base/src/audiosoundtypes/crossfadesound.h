//
// audioengine/multitracksound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CROSSFADESOUND_H
#define CROSSFADESOUND_H

#include "sound.h"
#include "sounddefs.h"

#include "audioengine/metadataref.h"

namespace rage {

struct audCrossfadeSoundState
{
	audCrossfadeSoundState()
	{
		m_Mode = CROSSFADE_MODE_BOTH;
		m_PlayNear = false;
		m_PlayFar = false;
		m_NearAttenuation = g_SilenceVolume;
		m_FarAttenuation = g_SilenceVolume;
		m_MinDistance = -1.0f;
		m_MaxDistance = -1.0f;
		m_Hysteresis = 0.0f;

		m_DistanceVariable = NULL;
		m_MinDistanceVariable = NULL;
		m_MaxDistanceVariable = NULL;
		m_HysteresisVariable = NULL;
		m_CrossfadeVariable = NULL;
	}
	audCurve m_CrossfadeCurve;

	audSoundScratchInitParams m_ScratchInitParams;

	f32  m_NearAttenuation;
	f32  m_FarAttenuation;
	f32  m_MinDistance, m_MaxDistance, m_Hysteresis;
	audVariableHandle m_DistanceVariable;
	audVariableHandle m_MinDistanceVariable;
	audVariableHandle m_MaxDistanceVariable;
	audVariableHandle m_HysteresisVariable;
	audVariableHandle m_CrossfadeVariable;
	f32 m_Distance;
	audMetadataRef m_NearSoundRef, m_FarSoundRef;
	u32 m_TimeLastPlayingBoth;
	u8	 m_Mode;
	bool m_PlayNear;
	bool m_PlayFar;
};

class audCrossfadeSound : public audSound
{
public:
	
	~audCrossfadeSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audCrossfadeSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:
	// PURPOSE
	//  Sets up the attenuation factors, and which of the two sounds should currently be playing.
	//  This isn't called every frame if we're in FIXED mode, as we never recalculate.
	void SetAttenuationFactors(audCrossfadeSoundState *state);
	// PURPOSE
	//  This sets the attenuation, as calculated by SetAttenuationFactors, on our child sounds.
	//  It also kicks off sounds when appropriate, and can be called on Init, and then all subsequent frames,
	//  to ensure the correct sounds are playing at all times, regardless of mode.
	//  This isn't called every frame if we're in FIXED mode, as we never recalculate.
	// RETURNS true if a new child has been requested this frame.
	bool UpdateChildSounds(audCrossfadeSoundState *state);

	void ChildSoundCallback(const u32 timeInMs, const u32 childIndex);

	audSoundCombineBuffer m_CachedCombineBuffer;
	atRangeArray<float, 2> m_ChildVolumes;
	u8 m_StateStorageSlot;

	
};

} // namespace rage

#endif // AUD_MULTITRACKSOUND_H
