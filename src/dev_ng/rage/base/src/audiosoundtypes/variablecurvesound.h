// 
// audiosoundtypes/variablecurvesound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_VARIABLECURVESOUND_H
#define AUD_VARIABLECURVESOUND_H

#include "audiosoundtypes/sound.h"

#include "audiosoundtypes/sounddefs.h"

namespace rage {

// PURPOSE
//  Passes a variable through a curve, storing the output in another variable
class audVariableCurveSound : public audSound
{
public:
	
	~audVariableCurveSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:
	void ApplyCurve();

	audVariableHandle m_InputVariable;
	audVariableHandle m_OutputVariable;

	audCurve m_Curve;

};

} // namespace rage

#endif // AUD_VARIABLECURVESOUND_H
