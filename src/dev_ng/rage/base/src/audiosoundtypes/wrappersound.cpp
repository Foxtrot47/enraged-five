// 
// audiosoundtypes/wrappersound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "wrappersound.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"
#include "audioengine/soundmanager.h"

namespace rage
{

AUD_IMPLEMENT_STATIC_WRAPPERS(audWrapperSound);

audWrapperSound::~audWrapperSound()
{
	if(!IsBeingRemovedFromHierarchy() && GetChildSound(0))
	{
		DeleteChild(0);
	}
}

audWrapperSound::audWrapperSound()
: m_VolumeRef(NULL)
, m_PitchRef(NULL)
, m_PanRef(NULL)
, m_VolumeCurveScaleRef(NULL)
, m_LPFCutoffRef(NULL)
, m_HPFCutoffRef(NULL)
{

}

#if !__SPU

bool audWrapperSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	const WrapperSound *wrapperMetadata = reinterpret_cast<const WrapperSound*>(GetMetadata());
	
	for(u32 i = 0; i < wrapperMetadata->numVariableRefs; i++)
	{
		audVariableHandle *dest;
		audVariableHandle t;
		switch(wrapperMetadata->VariableRefs[i].Destination)
		{
		case PARAM_DESTINATION_VOLUME:
			dest = &m_VolumeRef;
			break;
		case PARAM_DESTINATION_PITCH:
			dest = &m_PitchRef;
			break;
		case PARAM_DESTINATION_ROLLOFF:
			dest = &m_VolumeCurveScaleRef;
			break;
		case PARAM_DESTINATION_PAN:
			dest = &m_PanRef;
			break;
		case PARAM_DESTINATION_LPF:
			dest = &m_LPFCutoffRef;
			break;
		case PARAM_DESTINATION_HPF:
			dest = &m_HPFCutoffRef;
			break;
		default:
			SoundAssertf(wrapperMetadata->VariableRefs[i].Destination == PARAM_DESTINATION_ELEVATIONPAN || wrapperMetadata->VariableRefs[i].Destination == PARAM_DESTINATION_HAPTICSENDLEVEL, "Invalid WrapperSound variable destination (%u)", wrapperMetadata->VariableRefs[i].Destination);
			// Don't crash
			dest = &t;
			break;
		}
		
		*dest = _FindVariableUpHierarchy(wrapperMetadata->VariableRefs[i].VR_Ref);
	}
	
	m_HasVariables =
		(	
			m_VolumeRef != NULL || 
			m_PitchRef != NULL || 
			m_PanRef != NULL ||
			m_LPFCutoffRef != NULL ||
			m_HPFCutoffRef != NULL ||
			m_VolumeCurveScaleRef != NULL
		);

	if(wrapperMetadata->MinRepeatTime > 0 && wrapperMetadata->FallbackSoundRef.IsValid())
	{
		// Use the unpausable timer
		const u32 timeInMs = g_AudioEngine.GetSoundManager().GetTimeInMilliseconds(1);
		if(wrapperMetadata->LastPlayTime <= timeInMs && wrapperMetadata->LastPlayTime + wrapperMetadata->MinRepeatTime > timeInMs)
		{
			// use fall-back sound
			audSound *childSound = SOUNDFACTORY.GetChildInstance(wrapperMetadata->FallbackSoundRef, this, initParams, scratchInitParams, false);
			SetChildSound(0, childSound);
		}
		else
		{
			// use main sound
			audSound *childSound = SOUNDFACTORY.GetChildInstance(wrapperMetadata->SoundRef, this, initParams, scratchInitParams, false);
			SetChildSound(0, childSound);

			// update last play time
			const_cast<WrapperSound*>(wrapperMetadata)->LastPlayTime = timeInMs;
		}

		return true;
	}
	// If AllowLoad is true then we don't need to use the fallback sound
	else if(!initParams->AllowLoad && wrapperMetadata->FallbackSoundRef.IsValid())
	{
		// Child sound initialisation will modify scratch init params, so we need to keep a copy 
		audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;

		audSound *childSound = SOUNDFACTORY.GetChildInstance(wrapperMetadata->SoundRef, this, initParams, scratchInitParams, false);
		SetChildSound(0, childSound);

		// This prepare call is set to query only; won't change any state
		if(childSound && childSound->_ManagedAudioPrepare(NULL, false, true, 0) == AUD_PREPARED)
		{
			return true;
		}
		else
		{
			if(childSound)
			{
				DeleteChild(0);
			}
			childSound = SOUNDFACTORY.GetChildInstance(wrapperMetadata->FallbackSoundRef, this, initParams, &scratchInitParamsCopy, false);
			SetChildSound(0, childSound);
			return (childSound != NULL);
		}
	}
	else
	{
		audSound *childSound = SOUNDFACTORY.GetChildInstance(wrapperMetadata->SoundRef, this, initParams, scratchInitParams, false);
		SetChildSound(0, childSound);
	}
	
	return true;
}
#endif // !__SPU

void audWrapperSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound = GetChildSound(0);
	if(childSound)
	{
		if(m_HasVariables)
		{
			ApplyVariables(combineBuffer);
		}
		childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		if(!m_HasVariables)
		{
			RemoveMyselfFromHierarchy();
		}
	}
}

bool audWrapperSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound = GetChildSound(0);
	if(childSound)
	{
		if(m_HasVariables)
		{
			ApplyVariables(combineBuffer);
		}

		SoundAssert (childSound->GetPlayState() == AUD_SOUND_PLAYING);
		if (childSound->_ManagedAudioUpdate(timeInMs, combineBuffer))
		{
			return (true);
		}
		else
		{
			return (false);
		}
	}
	else
	{
		// if we don't have a sound then we're done immediately
		return false;
	}
}

void audWrapperSound::ApplyVariables(audSoundCombineBuffer &combineBuffer)
{
	if(m_VolumeRef)
	{
		combineBuffer.Volume += AUD_GET_VARIABLE(m_VolumeRef);
	}
	if(m_VolumeCurveScaleRef)
	{
		combineBuffer.VolumeCurveScale.SetFloat16_FromFloat32(
			combineBuffer.VolumeCurveScale.GetFloat32_FromFloat16() * AUD_GET_VARIABLE(m_VolumeCurveScaleRef)
			);
	}
	if(m_PitchRef)
	{
		Assign(combineBuffer.Pitch, combineBuffer.Pitch + (s32)(AUD_GET_VARIABLE(m_PitchRef)*100.f));
	}
	if(m_PanRef)
	{
		Assign(combineBuffer.Pan, CombinePan(combineBuffer.Pan, (s32)AUD_GET_VARIABLE(m_PanRef)));
	}
	if(m_LPFCutoffRef)
	{
		Assign(combineBuffer.LPFCutoff, Min<u32>(combineBuffer.LPFCutoff, (u32)(AUD_GET_VARIABLE(m_LPFCutoffRef))));
	}
	if(m_HPFCutoffRef)
	{
		Assign(combineBuffer.HPFCutoff, Max<u32>(combineBuffer.HPFCutoff, (u32)(AUD_GET_VARIABLE(m_HPFCutoffRef))));
	}
}

void audWrapperSound::AudioKill()
{
	audSound* childSound = GetChildSound(0);
	if(childSound)
	{
		childSound->_ManagedAudioKill();
	}
}

audPrepareState audWrapperSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
{
	audSound* childSound = GetChildSound(0);
	if(childSound)
	{
		childSound->SetStartOffset((s32)GetPlaybackStartOffset());
		return childSound->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
	}
	else
	{
		return AUD_PREPARED;
	}
}

s32 audWrapperSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound* childSound = GetChildSound(0);
	if(childSound)
	{
		return childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
	}
	else
	{
		// we have no sound so we have a length of 0ms
		if (isLooping)
		{
			*isLooping = false;
		}
		return 0;
	}
}

}
