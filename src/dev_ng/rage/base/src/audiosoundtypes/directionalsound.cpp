// 
// audiosoundtypes/directionalsound.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "directionalsound.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"
#include "vectormath/legacyconvert.h"
#include "phcore/conversion.h"

namespace rage
{
	AUD_IMPLEMENT_STATIC_WRAPPERS(audDirectionalSound);

	audDirectionalSound::~audDirectionalSound()
	{
		if(!IsBeingRemovedFromHierarchy() && GetChildSound(0))
		{
			DeleteChild(0);
		}
	}

#if !__SPU
	audDirectionalSound::audDirectionalSound()
	{
		
	}

	bool audDirectionalSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
			return false;

		DirectionalSound settings;
		sysMemCpy(&settings, GetMetadata(), sizeof(DirectionalSound));

		f32 x = Cosf(PH_DEG2RAD(settings.YawAngle))*Cosf(PH_DEG2RAD(settings.PitchAngle));
		f32 y = Sinf(PH_DEG2RAD(settings.YawAngle))*Cosf(PH_DEG2RAD(settings.PitchAngle));
		f32 z = Sinf(PH_DEG2RAD(settings.PitchAngle));

		m_VolumeCone.Init(Vec3V(x,y,z), settings.RearAttenuation, settings.InnerAngle, settings.OuterAngle);

		SetChildSound(0,SOUNDFACTORY.GetChildInstance(settings.SoundRef, this, initParams, scratchInitParams, false));
		return true;
	}
#endif

	void audDirectionalSound::ApplyConeToChild(audSoundCombineBuffer &combineBuffer)
	{	
		audRequestedSettings * settings = GetRequestedSettingsFromIndex(m_InitParams.BucketId, GetTopLevelRequestedSettingsIndex());
		if(settings)
		{
			Mat34V directionMat;
			QuatV quat;
			quat.ZeroComponents();
			settings->GetOrientation(quat);
			Mat34VFromQuatV(directionMat, quat);
			Vec3V pos = settings->GetPosition_AudioThread();
			directionMat.SetCol3(pos);

			combineBuffer.Volume += m_VolumeCone.ComputeAttenuation(directionMat);
		}
	}
	
	void audDirectionalSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		audSound* childSound = GetChildSound(0);
	
		if(childSound)
		{
			ApplyConeToChild(combineBuffer);

			childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}

	bool audDirectionalSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		if(GetChildSound(0))
		{
			SoundAssert (GetChildSound(0)->GetPlayState() == AUD_SOUND_PLAYING);

			ApplyConeToChild(combineBuffer); 
			return GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
		}
		return false;
	}

	void audDirectionalSound::AudioKill()
	{
		audSound* childSound = GetChildSound(0);
		if(childSound)
		{
			childSound->_ManagedAudioKill();
		}
	}

	audPrepareState audDirectionalSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
	{
		audSound* childSound = GetChildSound(0);
		if(childSound)
		{
			childSound->SetStartOffset((s32)GetPlaybackStartOffset());
			return childSound->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		}
		else
		{
			return AUD_PREPARED;
		}
	}

	s32 audDirectionalSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		audSound* childSound = GetChildSound(0);
		if(childSound)
		{
			return childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
		}
		else
		{
			// we have no sound so we have a length of 0ms
			if (isLooping)
			{
				*isLooping = false;
			}
			return 0;
		}
	}
}
