// 
// audiosoundtypes/sequentialoverlapsound.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "sequentialoverlapsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/syncsource.h"

namespace rage {

	AUD_IMPLEMENT_STATIC_WRAPPERS(audSequentialOverlapSound);

	audSequentialOverlapSound::~audSequentialOverlapSound()
	{
		for(s32 i = 0; i < 2; i++)
		{
			audMixerSyncManager::Get()->Release(m_SyncIds[i].masterSyncId);
			audMixerSyncManager::Get()->Release(m_SyncIds[i].slaveSyncId);

			m_SyncIds[i].masterSyncId = m_SyncIds[i].slaveSyncId = audMixerSyncManager::InvalidId;

			if(HasChildSound(i))
			{
				DeleteChild(i);
			}
		}

		if(m_StateSlotIndex != 0xff)
		{
			sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_StateSlotIndex);
			m_StateSlotIndex = 0xff;
		}
	}

#if !__SPU
	audSequentialOverlapSound::audSequentialOverlapSound()
	{
		m_Toggle = 0;
		m_LastPlayTime = 0;
		m_IsLooping = false;
		m_IsDelayTimePercentage = false;
		m_IsDelayTimeRemaining = false;
		m_CurrentChildIndex = 0;
		m_CurrentSyncID = 0;

		for(s32 i = 0; i < 2; i++)
		{
			m_SyncIds[i].masterSyncId = m_SyncIds[i].slaveSyncId = audMixerSyncManager::InvalidId;
			m_Preparing[i] = false;
		}

		m_StateSlotIndex = 0xff;
	}

	bool audSequentialOverlapSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
			return false;

		m_ScratchInitParams = *scratchInitParams;

		SetHasDynamicChildren();

		SequentialOverlapSound *sequentialOverlapSoundData = (SequentialOverlapSound*)GetMetadata();

		m_IsLooping = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SEQUENTIALOVERLAPSOUND_LOOP) == AUD_TRISTATE_TRUE);
		m_ShouldReleaseChildren = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SEQUENTIALOVERLAPSOUND_RELEASECHILDREN) == AUD_TRISTATE_TRUE);
		m_TriggerOnChildReleasing = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SEQUENTIALOVERLAPSOUND_TRIGGERONCHILDRELEASE) == AUD_TRISTATE_TRUE);
		m_SpliceMode = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SEQUENTIALOVERLAPSOUND_SPLICEMODE) == AUD_TRISTATE_TRUE);
		m_MetadataDelayTime = m_DelayTime = sequentialOverlapSoundData->DelayTime;
		m_IsDelayTimePercentage = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SEQUENTIALOVERLAPSOUND_DELAYTIMEISPERCENTAGE) == AUD_TRISTATE_TRUE);
		m_IsDelayTimeRemaining = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_SEQUENTIALOVERLAPSOUND_DELAYTIMEISREMAINING) == AUD_TRISTATE_TRUE);

		m_NumChildren = Min((u8)kMaxSequentialOverlapChildren,sequentialOverlapSoundData->numChildSounds);
		audMetadataRef *storage = (audMetadataRef*)sm_Pool.AllocateRequestedSettingsSlot(sizeof(audMetadataRef) * m_NumChildren,m_InitParams.BucketId DEV_ONLY(, GetName()));
		if(storage == NULL)
		{
			return false;
		}
		m_StateSlotIndex = sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, storage);
		for(u32 i = 0; i < m_NumChildren; i++)
		{
			storage[i] = sequentialOverlapSoundData->ChildSound[i].SoundRef;
		}
		
		SetChildSound(0,SOUNDFACTORY.GetChildInstance(storage[m_CurrentChildIndex], this, initParams, scratchInitParams, false));
				
		m_DelayTimeVariable = _FindVariableUpHierarchy(sequentialOverlapSoundData->DelayTimeVariable);
		m_DirectionVariable = _FindVariableUpHierarchy(sequentialOverlapSoundData->SequenceDirection);

		if(m_DirectionVariable == NULL || AUD_GET_VARIABLE(m_DirectionVariable) > 0.f)
		{
			m_CurrentChildIndex ++;
		}
		else
		{
			m_CurrentChildIndex = m_NumChildren-1;
		}
		
		return true;
	}
#endif // !__SPU

	audPrepareState audSequentialOverlapSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
	{
		audPrepareState state = AUD_PREPARE_FAILED;

		if (HasChildSound(0))
		{
			state = GetChildSound(0)->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		}

		return state;
	}

	void audSequentialOverlapSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		if (m_DelayTimeVariable)
		{
			// delay time variable specified in seconds - convert to ms
			m_DelayTime = (u32)((AUD_GET_VARIABLE(m_DelayTimeVariable))*1000.f);
		}

		//If we're prepared, we should have a first child sound
		audSound *childSound = GetChildSound(0);
		SoundAssert(childSound);

		if (childSound->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority()) == AUD_PREPARED)
		{
			audSoundSyncSetMasterId setSyncMasterId;
			audSoundSyncSetSlaveId setSyncSlaveId;

			if(m_SpliceMode)
			{
				Assign(m_SyncIds[m_CurrentSyncID].masterSyncId, audMixerSyncManager::Get()->Allocate());
				const u32 currentSlaveId = GetCurrentSyncSlaveId();
				audMixerSyncManager::Get()->AddOwnerRef(currentSlaveId);
				Assign(m_SyncIds[m_CurrentSyncID].slaveSyncId, currentSlaveId);
				setSyncMasterId.Set(m_SyncIds[m_CurrentSyncID].masterSyncId);
				setSyncSlaveId.Set(m_SyncIds[m_CurrentSyncID].slaveSyncId);
			}

			if (!m_DelayTimeVariable && !m_TriggerOnChildReleasing)
			{
				const s32 childDuration = childSound->ComputeDurationMsIncludingStartOffsetAndPredelay(NULL);
				if (m_IsDelayTimeRemaining)
				{
					if (m_IsDelayTimePercentage)
					{
						m_DelayTime = (u32)Max(0, s32(childDuration - (childDuration * (f32)m_MetadataDelayTime/100.f)));
					}
					else
					{
						m_DelayTime = (u32)Max(0, s32(childDuration - m_MetadataDelayTime));
					}
				}
				else
				{
					if (m_IsDelayTimePercentage)
					{
						m_DelayTime = (u32)Max(0, s32(childDuration * (f32)m_MetadataDelayTime/100.f));
					}
					else
					{
						m_DelayTime = m_MetadataDelayTime;
					}
				}
			}
			
			// Apply residual delay to the first child sound only
			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
			m_LastPlayTime = 0;
		}
	}

	bool audSequentialOverlapSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		audSound *childSounds[2];
		childSounds[0] = GetChildSound(0);
		childSounds[1] = GetChildSound(1);

		// Child sounds get updated and deleted when they've finished regardless.
		for(s32 i = 0; i < 2; i++)
		{			
			if (childSounds[i])
			{
				if ((childSounds[i]->GetPlayState() == AUD_SOUND_PLAYING))
				{
					audSoundSyncSetMasterId setSyncMasterId;
					audSoundSyncSetSlaveId setSyncSlaveId;

					if(m_SpliceMode)
					{
						setSyncMasterId.Set(m_SyncIds[i].masterSyncId);
						setSyncSlaveId.Set(m_SyncIds[i].slaveSyncId);
					}

					if(!childSounds[i]->_ManagedAudioUpdate(timeInMs, combineBuffer))
					{
						//Sound is waiting to be deleted (done)
						DeleteChild(i);
						childSounds[i] = NULL;
					}
				}
			}
		}

		if (m_DelayTimeVariable)
		{
			// delay time variable specified in seconds - convert to ms
			m_DelayTime = (u32)((AUD_GET_VARIABLE(m_DelayTimeVariable))*1000.f);
		}

		//Check delay time for switching between children
		bool readyToPlayNext = false;
		
		if(m_TriggerOnChildReleasing)
		{
			readyToPlayNext = childSounds[m_Toggle] == NULL || childSounds[m_Toggle]->IsInReleasePhase();
		}
		else
		{
			readyToPlayNext = (GetCurrentPlayTime(timeInMs) - m_LastPlayTime) >= m_DelayTime;
		}
		if (readyToPlayNext)
		{
			if ((m_IsLooping || m_CurrentChildIndex < m_NumChildren) && !IsWaitingForChildrenToRelease()) 
			{
				u32 nextToggle = (m_Toggle+1)&1;
				
				if(m_ShouldReleaseChildren && childSounds[m_Toggle])
				{
					childSounds[m_Toggle]->_Stop();
				}

				SetupChildSound(nextToggle);
				childSounds[nextToggle] = NULL;

				m_LastPlayTime = GetCurrentPlayTime(timeInMs);
				m_Preparing[nextToggle] = true;
			
				Assign(m_Toggle, nextToggle);
			}
			else
			{
				if (!HasChildSound(0) && !HasChildSound(1))
				{
					return false;
				}
			}
		}

		for(s32 i = 0; i < 2; i++)
		{
			if (childSounds[i] && m_Preparing[i])
			{
				if (childSounds[i]->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority()) == AUD_PREPARED)
				{
					if (!m_DelayTimeVariable)
					{
						const s32 childDuration = childSounds[i]->ComputeDurationMsIncludingStartOffsetAndPredelay(NULL);
						if (m_IsDelayTimeRemaining)
						{
							if (m_IsDelayTimePercentage)
								m_DelayTime = (u32)Max(0, s32(childDuration - (childDuration * (f32)m_MetadataDelayTime/100.f)));
							else
								m_DelayTime = (u32)Max(0, s32(childDuration - m_MetadataDelayTime));
						}
						else
						{
							if (m_IsDelayTimePercentage)
								m_DelayTime = (u32)Max(0, s32(childDuration * (f32)m_MetadataDelayTime/100.f));
							else
								m_DelayTime = m_MetadataDelayTime;
						}
					}

					if(m_SpliceMode)
					{
						u8 nextSyncID = (m_CurrentSyncID+1)&1;

						audSoundSyncSetSlaveId setSyncSlaveId;
						audSoundSyncSetMasterId setSyncMasterId;

						// We're about to overwrite this sync id, so ensure we've released it first
						audMixerSyncManager::Get()->Release(m_SyncIds[nextSyncID].masterSyncId);
						audMixerSyncManager::Get()->Release(m_SyncIds[nextSyncID].slaveSyncId);

						// Next child should slave to current sync ID and master (newly allocated) next sync id.
						Assign(m_SyncIds[nextSyncID].masterSyncId, audMixerSyncManager::Get()->Allocate());

						audMixerSyncManager::Get()->AddOwnerRef(m_SyncIds[m_CurrentSyncID].masterSyncId);
						Assign(m_SyncIds[nextSyncID].slaveSyncId, m_SyncIds[m_CurrentSyncID].masterSyncId);

						setSyncMasterId.Set(m_SyncIds[nextSyncID].masterSyncId);
						setSyncSlaveId.Set(m_SyncIds[nextSyncID].slaveSyncId);

						childSounds[i]->_ManagedAudioPlay(timeInMs, combineBuffer);

						m_CurrentSyncID = nextSyncID;
					}
					else
					{
						childSounds[i]->_ManagedAudioPlay(timeInMs, combineBuffer);
					}

					m_Preparing[i] = false;
				}
			}
		}

		return true;
	}

	void audSequentialOverlapSound::SetupChildSound(const int which)
	{
		if (HasChildSound(which))
		{
			GetChildSound(which)->_ManagedAudioDestroy();
			DeleteChild(which);
		}

		const audMetadataRef *storage = (audMetadataRef*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_StateSlotIndex);
		RequestChildSoundFromOffset(storage[m_CurrentChildIndex % m_NumChildren], which, &m_ScratchInitParams);
		
		if(!m_DirectionVariable || AUD_GET_VARIABLE(m_DirectionVariable) > 0.f)
		{
			m_CurrentChildIndex ++;
		}
		else
		{
			if(m_CurrentChildIndex == 0)
			{
				m_CurrentChildIndex = m_NumChildren-1;
			}
			else
			{
				m_CurrentChildIndex--;
			}
		}
	}

	void audSequentialOverlapSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 childIndex)
	{
		if(HasChildSound(childIndex))
		{
			GetChildSound(childIndex)->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority());
		}
	}

	void audSequentialOverlapSound::AudioKill()
	{
		if (HasChildSound(0))
		{
			GetChildSound(0)->_ManagedAudioKill();
		}

		if (HasChildSound(1))
		{
			GetChildSound(1)->_ManagedAudioKill();
		}
	}

	s32 audSequentialOverlapSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping)
	{
		s32 length = m_NumChildren * m_DelayTime;

		if (isLooping)
		{
			*isLooping = m_IsLooping;
		}

		return length;
	}

} // namespace rage
