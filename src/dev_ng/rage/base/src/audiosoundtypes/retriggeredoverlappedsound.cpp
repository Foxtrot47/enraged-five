//
// audiosoundtypes/retriggeredoverlapped.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "retriggeredoverlappedsound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audRetriggeredOverlappedSound);

audRetriggeredOverlappedSound::~audRetriggeredOverlappedSound()
{
	for(s32 i = 0; i < kMaxRTOChildren; i++)
	{
		if(HasChildSound(i))
		{
			DeleteChild(i);
		}
	}
}

#if !__SPU

audRetriggeredOverlappedSound::audRetriggeredOverlappedSound()
{
	// If this fires then m_Prepared is incorrectly sized
	CompileTimeAssert(kMaxRTOChildren==8);
	m_CurrentIndex = 0;
	m_LastPlayTime = 0;
	m_LoopCount = 0;

	m_Preparing.Reset();

	m_HavePlayedOnStopSound = false;
	m_HavePlayedOnStartSound = false;
}

bool audRetriggeredOverlappedSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	m_ScratchInitParams = *scratchInitParams;
	
	SetHasDynamicChildren();

	RetriggeredOverlappedSound *retriggeredOverlappedSoundData = (RetriggeredOverlappedSound*)GetMetadata();
	
	m_LoopCount = retriggeredOverlappedSoundData->LoopCount;
	m_DelayTime = retriggeredOverlappedSoundData->DelayTime;
	m_DelayTime = ApplyVariance(m_DelayTime, retriggeredOverlappedSoundData->DelayTimeVariance, 1<<16U);

	if(m_LoopCount != -1)
	{
		m_LoopCount = ApplyVariance(m_LoopCount, retriggeredOverlappedSoundData->LoopCountVariance, 1024);
	}

	m_SoundRef = retriggeredOverlappedSoundData->SoundRef;
	m_OnStopSoundRef = retriggeredOverlappedSoundData->StopSound;
	m_OnStartSoundRef = retriggeredOverlappedSoundData->StartSound;

	m_DelayTimeVariable = _FindVariableUpHierarchy(retriggeredOverlappedSoundData->DelayTimeVariable);
	m_LoopCountVariable = _FindVariableUpHierarchy(retriggeredOverlappedSoundData->LoopCountVariable);

	m_ShouldReleaseChildren = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_RELEASECHILDREN)==AUD_TRISTATE_TRUE);
	m_AllowChildToFinish = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_ALLOWCHILDTOFINISH)==AUD_TRISTATE_TRUE);
	m_SetLoopCountVariableOnce = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_SETLOOPCOUNTVARIABLEONCE)==AUD_TRISTATE_TRUE);

	 
	if(m_AllowChildToFinish)
	{
		SetShouldFinishWhenEnvelopeAttenuationFull(false);
	}

	return true;
}
#endif // !__SPU

audPrepareState audRetriggeredOverlappedSound::AudioPrepare(audWaveSlot *UNUSED_PARAM(waveSlot), const bool UNUSED_PARAM(checkStateOnly))
{	
	return AUD_PREPARED;
}

void audRetriggeredOverlappedSound::ActionReleaseRequest(const u32 timeInMs)
{
	if(m_AllowChildToFinish)
	{
		SetWasSetToReleaseLastFrame(true);
	}
	else
	{
		BaseActionReleaseRequest(timeInMs);
	}
}

void audRetriggeredOverlappedSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &UNUSED_PARAM(combineBuffer))
{
	if (m_LoopCountVariable)
	{
		m_LoopCount = (s32)(AUD_GET_VARIABLE(m_LoopCountVariable));
	}
	if (m_DelayTimeVariable)
	{
		// delay time variable specified in seconds - convert to ms
		m_DelayTime = (u32)((AUD_GET_VARIABLE(m_DelayTimeVariable))*1000.f);
	}

	// Create enough children to fill our time window based on the delay time
	s32 numChildrenToCreate = Clamp<s32>(kPredelayStartThresholdMs / Max<s32>(1,m_DelayTime), 1, kMaxRTOChildren);

	if(m_LoopCount != -1)
	{
		numChildrenToCreate = Clamp(numChildrenToCreate, 1, m_LoopCount);
		m_LoopCount -= numChildrenToCreate;
		if(m_LoopCountVariable && !m_SetLoopCountVariableOnce)
		{
			AUD_SET_VARIABLE(m_LoopCountVariable, (f32)m_LoopCount);
		}
	}

	const u32 residualPredelay = GetPredelayRemainingMs(timeInMs);
	u32 startTime = residualPredelay + GetCurrentPlayTime(timeInMs);

	const s32 lastChildIndex = (m_LoopCount == 0 ? numChildrenToCreate - 1 : -1);

	for(s32 i = 0; i < numChildrenToCreate; i++)
	{
		const bool isLastChild = (i == lastChildIndex);
		SetupChildSound(i, startTime + kPredelayStartThresholdMs, isLastChild);
		m_LastPlayTime = startTime;
		startTime += m_DelayTime;
		m_CurrentIndex++;
	}

	if(m_CurrentIndex == kMaxRTOChildren)
	{
		m_CurrentIndex = 0;
	}
}

bool audRetriggeredOverlappedSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	const u32 currentPlayTime = GetCurrentPlayTime(timeInMs);

	// Grab a variable every frame, as the variable itself is counted down in the code below, but might also be reset by other sounds
	if (m_LoopCountVariable && !m_SetLoopCountVariableOnce)
	{
		m_LoopCount = (s32)(AUD_GET_VARIABLE(m_LoopCountVariable));
	}
	if (m_DelayTimeVariable)
	{
		// delay time variable specified in seconds - convert to ms
		m_DelayTime = (u32)((AUD_GET_VARIABLE(m_DelayTimeVariable))*1000.f);
	}

	//Sounds get updated and deleted when they've finished regardless.
	u32 numActiveChildren = 0;
	for(s32 i = 0; i < kMaxRTOChildren; i++)
	{
		audSound *child = GetChildSound(i);
		if(child && child->GetPlayState() == AUD_SOUND_PLAYING)
		{
			if(!child->_ManagedAudioUpdate(timeInMs, combineBuffer))
			{
				// Child has stopped on its own; clear the slot
				DeleteChild(i);
			}
			else
			{
				numActiveChildren++;
				// Check to see if its time to stop this child
				if(m_ShouldReleaseChildren && currentPlayTime > m_ChildStartTime[i] && currentPlayTime - m_ChildStartTime[i] >= m_DelayTime)
				{
					child->_Stop();
				}
			}		
		}
	}
	
	u32 ourPlayTime = currentPlayTime;
	u32 endPlayTime = ourPlayTime + kPredelayStartThresholdMs;

	bool finished = false;
	while(!finished)
	{
		u32 nextPlayTime = m_LastPlayTime + m_DelayTime;
		if(nextPlayTime > endPlayTime)
		{
			break;
		}
		if(numActiveChildren >= kMaxRTOChildren)
		{
			break;
		}
		if(HasChildSound(m_CurrentIndex))
		{
			break;
		}

		bool shouldPlayAnotherChild = (m_LoopCount == -1 || m_LoopCount > 0);
		if(IsToBeStopped() && m_AllowChildToFinish)
		{
			if(!HasOnStopSound() || (HasOnStopSound() && m_HavePlayedOnStopSound))
			{
				shouldPlayAnotherChild = false;
			}
		}
		else if(IsWaitingForChildrenToRelease())
		{
			shouldPlayAnotherChild = false;
		}
		
		if (shouldPlayAnotherChild)
		{
			if(m_LoopCount != -1)
			{
				m_LoopCount--; // don't do this in the line above - we'll keep re-checking and go past zero and start playing again
				// If we've got a variable, update that as well, so next time we grab it, it'll be valid, but we can reset it in other sounds
				if (m_LoopCountVariable && !m_SetLoopCountVariableOnce)
				{
					AUD_SET_VARIABLE(m_LoopCountVariable, (f32)m_LoopCount);
				}
			}

			const bool isLastChild = IsToBeStopped() || (m_LoopCount == 0);
			// Queue up the next child to be played
			SetupChildSound(m_CurrentIndex, nextPlayTime + kPredelayStartThresholdMs, isLastChild);
			
			m_CurrentIndex = (m_CurrentIndex+1) % kMaxRTOChildren;
			ourPlayTime = nextPlayTime;
			m_LastPlayTime = nextPlayTime;
			numActiveChildren++;
		}
		else
		{
			finished = true;
		}

	}

	if(finished)
	{
		bool haveAnyChildren = false;
		for(s32 i = 0; i < kMaxRTOChildren; i++)
		{
			if(HasChildSound(i))
			{
				haveAnyChildren = true;
				break;
			}
		}
		if(!haveAnyChildren)
		{
			return false;
		}
	}

	StartQueuedChildren(timeInMs, combineBuffer);

	return true;
}

void audRetriggeredOverlappedSound::StartQueuedChildren(const u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	const u32 currentPlayTime = GetCurrentPlayTime(timeInMs);

	for(s32 i = 0; i < kMaxRTOChildren; i++)
	{
		if(HasChildSound(i) && m_Preparing.IsSet(i))
		{
			audSound *child = GetChildSound(i);
			audPrepareState state = child->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority());
			if(state == AUD_PREPARED)
			{
				// Work out subframe offset / delay
				const u32 predelay = currentPlayTime < m_ChildStartTime[i] ? m_ChildStartTime[i] - currentPlayTime : 0;

				if(predelay <= audSound::kPredelayStartThresholdMs)
				{
					child->_SetRequestedInternalPredelay(predelay);
					child->_ManagedAudioPlay(timeInMs, combineBuffer);
					m_Preparing.Clear(i);
				}

			}
			else if(state == AUD_PREPARE_FAILED)
			{
				DeleteChild(i);
				m_Preparing.Clear(i);
			}
		}
	}
}

void audRetriggeredOverlappedSound::SetupChildSound(const s32 which, const u32 startTimeMs, const bool isStopping)
{
	SoundAssert(!HasChildSound(which));

	audMetadataRef soundRef = m_SoundRef;

	if(isStopping)
	{
		if(HasOnStopSound())
		{
			soundRef = m_OnStopSoundRef;
		}
		m_HavePlayedOnStopSound = true;
	}
	else if(HasOnStartSound() && !m_HavePlayedOnStartSound)
	{
		soundRef = m_OnStartSoundRef;
		m_HavePlayedOnStartSound = true;
	}

	RequestChildSoundFromOffset(soundRef, which, &m_ScratchInitParams);

	m_ChildStartTime[which] = startTimeMs;
	m_Preparing.Set(which);
}

void audRetriggeredOverlappedSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 UNUSED_PARAM(childIndex))
{
	//StartQueuedChildren(timeInMs);
}

void audRetriggeredOverlappedSound::AudioKill()
{
	for(s32 i = 0; i < kMaxRTOChildren; i++)
	{
		if(HasChildSound(i))
		{
			GetChildSound(i)->_ManagedAudioKill();
		}
	}
}

s32 audRetriggeredOverlappedSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool *isLooping)
{
	s32 length = m_LoopCount * m_DelayTime;

	if (m_LoopCount == -1)
	{
		length = 0;
	}

	if (isLooping)
	{
		if (m_LoopCount==-1 && isLooping)
		{
			*isLooping = true;
		}
		else
		{
			*isLooping = false;
		}
	}

	return length;
}

void audRetriggeredOverlappedSound::ManagedAudioStopChildren()
{
	if(m_AllowChildToFinish)
	{
		return;
	}
	audSound::ManagedAudioStopChildren();
}


#if __SOUND_DEBUG_DRAW
bool audRetriggeredOverlappedSound::DebugDrawInternal(const u32 UNUSED_PARAM(timeInMs), audDebugDrawManager &drawManager, const bool UNUSED_PARAM(drawDynamicChildren)) const
{
	char buf[64];
	if(m_LoopCount == -1)
	{
		formatf(buf, "- DelayTime: %u / inf", m_DelayTime);
	}
	else
	{
		formatf(buf, "- DelayTime: %u / %u", m_DelayTime, m_LoopCount);
	}
	drawManager.DrawLine(buf);	

	return true;
}
#endif

} // namespace rage
