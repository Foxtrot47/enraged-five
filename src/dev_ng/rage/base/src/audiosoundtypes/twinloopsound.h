//
// audioengine/twinloopsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_TWINLOOP_SOUND_H
#define AUD_TWINLOOP_SOUND_H

#include "sound.h"
#include "sounddefs.h"
#include "audioengine/widgets.h"
#include "audioengine/math.h"

namespace rage
{

	enum audTwinLoopCrossfade
	{
		AUD_TWINLOOP_XFADE_EQUAL_POWER = 0,
		AUD_TWINLOOP_XFADE_LINEAR,
	};

// PURPOSE
//  A twin loop sound plays two looping sound, and switches between them at random, within
//  defined upper and lower time limits.
class audTwinLoopSound : public audSound
{
public:
	
	~audTwinLoopSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *pMetadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif // !__SPU

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();
	// PURPOSE
	//  Implements base class function for this sound.
	int ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

private:

	// PURPOSE
	//  Swaps which of the two looping sounds is currently being played. Called by AudioUpdate at the appropriate time.
	void SwapSounds(u32 timeInMs);

	u32 GetNewSwapTime(const u32 timeInMs);

	// PURPOSE
	//  Calculates a new smooth rate, based on the max and min crossfade times.
	f32 GetNewSmoothRate();

	u32 m_SwapTime;
	
	u16 m_MinCrossfadeTime, m_MaxCrossfadeTime;
	u16 m_MinSwapTime, m_MaxSwapTime;
	audVariableHandle m_MinCrossfadeTimeVariable, m_MaxCrossfadeTimeVariable;
	audVariableHandle m_MinSwapTimeVariable, m_MaxSwapTimeVariable;

	audSimpleSmoother m_CrossfadeSmoother;

	audFixed16Scalar m_Crossfade;
	u8 m_CrossfadeType;
	bool m_IgnoreStoppedChild;
};

} // namespace rage
#endif //AUD_TWINLOOP_SOUND_H
