//
// audioengine/externalstreamsound.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//


#include "externalstreamsound.h"

#include "environmentsound.h"
#include "audioengine/engine.h"
#include "audioengine/metadatamanager.h"
#include "audioengine/soundfactory.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/pcmsource_interface.h"


namespace rage {

	AUD_IMPLEMENT_STATIC_WRAPPERS(audExternalStreamSound);

	audExternalStreamSound::~audExternalStreamSound()
	{
		if (!IsBeingRemovedFromHierarchy())
		{
			for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
			{
				DeleteChild(i);
			}
		}
		if(m_StreamPlayerId != -1)
		{
			audPcmSourceInterface::Release(m_StreamPlayerId);
			m_StreamPlayerId = -1;
		}
		if(m_ReqSetSlotId != 0xff)
		{
			sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_ReqSetSlotId);
		}
	}

#if !__SPU
	audExternalStreamSound::audExternalStreamSound()
	{
		m_ReqSetSlotId = 0xff;
		m_StreamPlayerId = -1;
		m_NumEnvironmentSounds = 0;
		m_Gain = 0.f;
		m_NeedToStopStreamPlayer = false;
	}

	bool audExternalStreamSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
	{
		if(!audSound::Init(metadata, initParams, scratchInitParams))
		{
			return false;
		}
	
		ExternalStreamSound *soundMetadata = (ExternalStreamSound*)GetMetadata();

		m_NumEnvironmentSounds = soundMetadata->numEnvironmentSounds;
		if(m_NumEnvironmentSounds == 0)
		{
			audEnvironmentSound *es = SOUNDFACTORY.GetEnvironmentSound(this, initParams, scratchInitParams);
			SetChildSound(0, es);
			if(!es)
			{
				return false;
			}
			m_NumEnvironmentSounds = 1;
		}
		else
		{
			// Environment sounds specified in metadata
			for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
			{
				audEnvironmentSound *es = (audEnvironmentSound*)SOUNDFACTORY.GetChildInstance(soundMetadata->EnvironmentSound[i].SoundRef, this, initParams, scratchInitParams, false);
				SetChildSound(i, es);
				if(!es || !Verifyf(es->GetSoundTypeID() == EnvironmentSound::TYPE_ID, "ExternalStreamSound with invalid environment sound reference: %s %u", GetName(), i))
				{
					return false;
				}
			}
		}

		void *paramsSlot = sm_Pool.AllocateRequestedSettingsSlot(sizeof(Params), m_InitParams.BucketId DEV_ONLY(, GetName()));
		if(!paramsSlot)
			return false;

		m_ReqSetSlotId = sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, paramsSlot);

		return true;
	}
#endif // !__SPU

	audExternalStreamSound::Params *audExternalStreamSound::GetParams()
	{
		SoundAssert(m_ReqSetSlotId != 0xff);
		return reinterpret_cast<Params*>(sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_ReqSetSlotId));
	}

	audPrepareState audExternalStreamSound::AudioPrepare(audWaveSlot *UNUSED_PARAM(slot), const bool checkStateOnly)
	{
		if(checkStateOnly)
		{
			return AUD_PREPARED;
		}

		m_StreamPlayerId = audPcmSourceInterface::Allocate(AUD_PCMSOURCE_EXTERNALSTREAM);
		if(m_StreamPlayerId == -1)
		{
			return AUD_PREPARE_FAILED;
		}

		Params *params = GetParams();
		audStreamPlayer::audInitStreamPlayerCommandPacket initPacket;
		Assign(initPacket.numChannels, params->numChannels);
		Assign(initPacket.sampleRate, params->sampleRate);
		initPacket.ringBuffer = params->ringBuffer;
		if(!audPcmSourceInterface::WriteCustomCommandPacket(m_StreamPlayerId, audStreamPlayer::Params::InitParams, &initPacket))
		{
			return AUD_PREPARE_FAILED;
		}

		audPcmSourceInterface::SetParam(m_StreamPlayerId, audStreamPlayer::Params::Gain, m_Gain);	

		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			audEnvironmentSound *es = reinterpret_cast<audEnvironmentSound*>(GetChildSound(i));
			es->InitEnvironment(m_StreamPlayerId, 0.f);
			// Only the first environment sound controls the PCM source
			es->SetIsControllingPcmSource(i == 0);
		}
		return AUD_PREPARED;
	}

	s32 audExternalStreamSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
	{
		if(isLooping)
		{
			*isLooping = true;
		}
		return kSoundLengthUnknown;
	}

	void audExternalStreamSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{
		SoundAssert(m_NumEnvironmentSounds > 0);
		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			GetEnvironmentSound(i)->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}

	bool audExternalStreamSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
	{	
		u32 numFinished = 0;
		if(m_NeedToStopStreamPlayer && m_StreamPlayerId != -1)
		{	
			audPcmSourceInterface::SetParam(m_StreamPlayerId, audStreamPlayer::Params::StopPlayback, 1U);
			m_NeedToStopStreamPlayer = false;	
		}

		const bool shouldStopChildren = IsWaitingForChildrenToRelease();
		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			if (shouldStopChildren)
			{
				GetEnvironmentSound(i)->_Stop();
			}

			if(!GetEnvironmentSound(i)->_ManagedAudioUpdate(timeInMs, combineBuffer))
			{
				numFinished++;
			}
		}	

		PcmSourceState state; 
		if(audPcmSourceInterface::GetState(state, m_StreamPlayerId)) 
		{ 
			if(state.PlaytimeSamples == 0 || state.AuthoredSampleRate == 0)
			{
				m_CurrentPlayTimeMs = 0;
			}
			else
			{
				m_CurrentPlayTimeMs = audDriverUtil::ConvertSamplesToMs(state.PlaytimeSamples, state.AuthoredSampleRate); 
			}
		}

		return	!(numFinished == m_NumEnvironmentSounds);
	}

	void audExternalStreamSound::AudioKill()
	{
		for(u32 i = 0; i < m_NumEnvironmentSounds; i++)
		{
			GetEnvironmentSound(i)->_ManagedAudioKill();
		}
	}

	bool audExternalStreamSound::InitStreamPlayer(audReferencedRingBuffer *ringBuffer, const u32 numChannels, const u32 sampleRateHz)
	{		
		Params *params = GetParams();
		if(params)
		{
			ringBuffer->AddRef();	
			params->ringBuffer = ringBuffer;
			params->numChannels = numChannels;
			params->sampleRate = sampleRateHz;
			return true;
		}

		return false;
	}

	void audExternalStreamSound::StopStreamPlayback()
	{
		m_NeedToStopStreamPlayer = true;
	}

} // namespace rage
