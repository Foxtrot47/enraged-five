// 
// audiosoundtypes/variableblocksound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "variableblocksound.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/remotecontrol.h"
#include "audioengine/soundfactory.h"
#include "audioengine/soundmanager.h"
#include "diag/output.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audVariableBlockSound);

audVariableBlockSound::~audVariableBlockSound()
{
	if(GetChildSound(0))
	{
		DeleteChild(0);
	}	
	if(m_AudioVariableSlotIndex != 0xff)
	{
		sm_Pool.DeleteSound(sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_AudioVariableSlotIndex),m_InitParams.BucketId);
		m_AudioVariableSlotIndex = 0xff;
	}
	if(m_GameVariableSlotIndex != 0xff)
	{
		sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_GameVariableSlotIndex);
		m_GameVariableSlotIndex = 0xff;
	}
}

#if !__SPU

audVariableBlockSound::audVariableBlockSound()
{
	m_AudioVariableSlotIndex = 0xff;
	m_GameVariableSlotIndex = 0xff;
}

bool audVariableBlockSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	VariableBlockSound *variableBlockSoundData = (VariableBlockSound*)GetMetadata();

	m_NumAudioVariables = m_NumGameVariables = 0;
	for(u32 i = 0; i < variableBlockSoundData->numVariables; i++)
	{
		if(variableBlockSoundData->Variable[i].Usage == VARIABLE_USAGE_CODE)
		{
			m_NumGameVariables++;
		}
		else
		{
			m_NumAudioVariables++;
		}
	}

	if(m_NumAudioVariables > 0)
	{
		audVariable *vars = (audVariable*)sm_Pool.AllocateSoundSlot(sm_Pool.GetSoundSlotSize(), m_InitParams.BucketId, true);
		if(!vars)
		{
			return false;
		}

		m_AudioVariableSlotIndex = sm_Pool.GetSoundSlotIndex(m_InitParams.BucketId, vars);
		SoundAssert(m_AudioVariableSlotIndex != 0xff);

		if(m_NumAudioVariables > sm_Pool.GetSoundSlotSize()/sizeof(audVariable))
		{
			audAssertf(false, "%s has %u sound-set variables; max is %" SIZETFMT "u.", GetName(), m_NumAudioVariables, sm_Pool.GetSoundSlotSize()/sizeof(audVariable));
			return false;
		}

		for(u32 i = 0, j = 0; i < variableBlockSoundData->numVariables; i++)
		{
			if(variableBlockSoundData->Variable[i].Usage == VARIABLE_USAGE_SOUND 
				|| variableBlockSoundData->Variable[i].Usage == VARIABLE_USAGE_CONSTANT)
			{
				vars[j].nameHash = variableBlockSoundData->Variable[i].VariableRef;
				const float variance = variableBlockSoundData->Variable[i].InitialVariance;
				const float randomOffset =  audEngineUtil::GetRandomNumberInRange(-variance, variance);
#if AUD_SUPPORT_RAVE_EDITING
				vars[j].valueOffset = randomOffset;
#endif
				vars[j++].value = variableBlockSoundData->Variable[i].VariableValue + randomOffset;
			}
		}
	}

	if(m_NumGameVariables > 0)
	{
		audVariable *vars = (audVariable*)sm_Pool.AllocateRequestedSettingsSlot(m_NumGameVariables * sizeof(audVariable), m_InitParams.BucketId DEV_ONLY(, GetName()));
		if(!vars)
		{
			return false;
		}
		m_GameVariableSlotIndex = sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, vars);
		SoundAssert(m_GameVariableSlotIndex != 0xff);

		for(u32 i = 0, j = 0; i < variableBlockSoundData->numVariables; i++)
		{
			if(variableBlockSoundData->Variable[i].Usage == VARIABLE_USAGE_CODE)
			{
				vars[j].nameHash = variableBlockSoundData->Variable[i].VariableRef;
				const float variance = variableBlockSoundData->Variable[i].InitialVariance;
				const float randomOffset =  audEngineUtil::GetRandomNumberInRange(-variance, variance);
#if AUD_SUPPORT_RAVE_EDITING
				vars[j].valueOffset = randomOffset;
#endif
				vars[j++].value = variableBlockSoundData->Variable[i].VariableValue + randomOffset;
			}
		}
	}

	// need to ensure that our variables are setup before we instantiate our child
	SetChildSound(0,SOUNDFACTORY.GetChildInstance(variableBlockSoundData->SoundRef, this, initParams, scratchInitParams, false));
		
	if(!GetChildSound(0))
	{
		return false;
	}
	return true;
}
#endif

audPrepareState audVariableBlockSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound);
	childSound->SetStartOffset((s32)GetPlaybackStartOffset());
	return childSound->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
}

void audVariableBlockSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound);
	childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
	childSound->SetStartOffset((s32)GetPlaybackStartOffset(), false);
	childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
}

bool audVariableBlockSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
#if __BANK && !__SPU
	// Catch any RAVE edits to constant vars and update our internal state
	if(g_AudioEngine.GetSoundManager().GetFactory().GetMetadataManager().IsRAVEConnected())
	{
		if(m_NumAudioVariables > 0 || m_NumGameVariables > 0)
		{			
			const VariableBlockSound *metadata = static_cast<const VariableBlockSound*>(GetMetadataForRAVEChanges());
			SoundAssert(metadata);
			for(u32 i = 0; i < metadata->numVariables; i++)
			{
				if(metadata->Variable[i].Usage == VARIABLE_USAGE_CONSTANT || (m_InitParams.IsAuditioning && metadata->Variable[i].Usage == VARIABLE_USAGE_CODE))
				{
					// Lie and pretend we're looking for a ReadOnly ref to bypass the usual validation
					float* var = (float*)GetVariableHandle(metadata->Variable[i].VariableRef, ReadOnly); 
					if(var)
					{
						AUD_SET_VARIABLE(var, metadata->Variable[i].VariableValue);
	#if AUD_SUPPORT_RAVE_EDITING
						// Add on initial variance
						*var +=  *(var+1);
	#endif
					}
				}
			}
		}
	}


#endif

	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound && childSound->GetPlayState() == AUD_SOUND_PLAYING);
	return childSound->_ManagedAudioUpdate(timeInMs, combineBuffer);
}

void audVariableBlockSound::AudioKill()
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound);
	childSound->_ManagedAudioKill();
}

s32 audVariableBlockSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	audSound* childSound = GetChildSound(0);
	SoundAssert(childSound);
	return childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(isLooping);
}

audVariableHandle audVariableBlockSound::_FindVariableDownHierarchy(const u32 ref, const audSound::VariableUsage usage)
{
#if __SPU
	Assert(0);
#endif
	// First of all, see if it's one of ours
	audVariableHandle variable = GetVariableHandle(ref, usage);
	if (variable)
	{
		return variable;
	}

	// It's not one of our variables, so see if it's down the chain, using base-class fn
	return audSound::_FindVariableDownHierarchy(ref, usage);
}

audVariableHandle audVariableBlockSound::_FindVariableUpHierarchy(const u32 ref)
{
#if __SPU
	Assert(0);
#endif
	// First of all, see if it's one of ours
	// Using ReadOnly here to bypass validation for now - ideally would be passed up with either SetFromSounds or ReadOnly as appropriate.
	audVariableHandle variable = GetVariableHandle(ref, ReadOnly);
	if (variable)
	{
		return variable;
	}

	// It's not one of our variables, so see if it's up the chain, if we're not at the top
	audSound *parent = _GetParent();
	if (!parent)
	{
		if(GetRequestedSettings())
		{
			audVariableBlock * var = audEntity::GetVariableBlock(GetRequestedSettings()->GetEntityVariableBlock());
			if(var)
			{
				audVariableHandle value = var->FindVariableAddress(ref);
				if(value)
				{
					return value;
				}
			}
		}
		return g_AudioEngine.GetSoundManager().GetVariableAddress(ref);
	}

	return (_GetParent()->_FindVariableUpHierarchy(ref));
}

audVariableHandle audVariableBlockSound::GetVariableHandle(const u32 nameHash, const audSound::VariableUsage ASSERT_ONLY(usage))
{
#if __SPU
	Assert(0);
#endif

	if(m_NumAudioVariables > 0)
	{
		audVariable *vars = (audVariable*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_AudioVariableSlotIndex);
		SoundAssert(vars);
		for (u32 i=0; i<m_NumAudioVariables; i++)
		{
			if (vars[i].nameHash == nameHash)
			{
				// We own the variable it's asking about, so return it
				audAssertf(usage == ReadOnly || usage == SetFromSounds, "%s - invalid variable usage (%u).  Variable is marked SetFromSounds or Constant but the game (code/script) is writing to it.", GetName(), nameHash);
				return audVariableHandle(&(vars[i].value));
			}
		}
	}
	if(m_NumGameVariables > 0)
	{
		audVariable *vars = (audVariable*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_GameVariableSlotIndex);
		SoundAssert(vars);
		for (u32 i=0; i<m_NumGameVariables; i++)
		{
			if (vars[i].nameHash == nameHash)
			{
				audAssertf(usage == ReadOnly || usage == SetFromGame, "%s - invalid variable usage (%u).  Variable is marked SetFromGame but a sound is writing to it.", GetName(), nameHash);
				// We own the variable it's asking about, so return it
				return audVariableHandle(&(vars[i].value));
			}
		}
	}
	return NULL;
}

void audVariableBlockSound::GetAudioVariableFromIndex(const u32 index, u32 &nameHash, float &value) const
{
	Assert(index < m_NumAudioVariables);
	audVariable *vars = (audVariable*)sm_Pool.GetSoundSlot(m_InitParams.BucketId, m_AudioVariableSlotIndex);
	SoundAssert(vars);
	nameHash = vars[index].nameHash;
	value = vars[index].value;
}


void audVariableBlockSound::GetGameVariableFromIndex(const u32 index, u32 &nameHash, float &value) const
{
	Assert(index < m_NumGameVariables);
	audVariable *vars = (audVariable*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_GameVariableSlotIndex);
	SoundAssert(vars);
	nameHash = vars[index].nameHash;
	value = vars[index].value;
}

#if __SOUND_DEBUG_DRAW
bool audVariableBlockSound::DebugDrawInternal(const u32 UNUSED_PARAM(timeInMs), audDebugDrawManager &drawManager, const bool UNUSED_PARAM(drawDynamicChildren)) const
{
	if(GetNumAudioVariables())
	{
		drawManager.PushSection("Audio variables:");
		
		for(u32 j = 0; j < GetNumAudioVariables(); j++)
		{
			u32 nameHash = 0;
			float value = 0.f;
			GetAudioVariableFromIndex(j, nameHash, value);
			char buf[64];
			formatf(buf, "%u (%u): %f", j+1, nameHash, value);
			drawManager.DrawLine(buf);
		}

		drawManager.PopSection();
	}
	if(GetNumGameVariables())
	{
		drawManager.PushSection("Game variables:");

		for(u32 j = 0; j < GetNumGameVariables(); j++)
		{
			u32 nameHash = 0;
			float value = 0.f;
			GetGameVariableFromIndex(j, nameHash, value);
			char buf[64];
			formatf(buf, "%u (%u): %f", j+1, nameHash, value);
			drawManager.DrawLine(buf);
		}

		drawManager.PopSection();
	}
	
	// we've not drawn child sounds - leave that up to standard behaviour
	return true;
}
#endif

} // namespace rage
