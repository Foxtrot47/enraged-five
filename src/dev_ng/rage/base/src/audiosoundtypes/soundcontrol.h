//
// audioengine/sound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SOUNDCONTROL_H
#define AUD_SOUNDCONTROL_H

#include "sounddefs.h"
#include "atl/array.h"
#include "audioengine/enginedefs.h"
#include "audiohardware/channel.h"
#include "audiohardware/driverdefs.h"
#include "vector/vector3.h"
#include "math/float16.h"

namespace rage 
{

class audEnvironmentGroupInterface;
class audTracker;
class audEffect;
class audCategory;
class audWaveSlot;
class audSound;
class audEntity;

//////////////////////////////////////////////////////////////////////////
// PURPOSE
//   This structure defines parameters that can be set when initializing an audSound
//   pointer through the following methods found in audioengine\entity.h: 
//	   CreateAndPlaySound(), CreateAndPlaySound_Persistent(), CreateSound_LocalReference(),
//     CreateSound_PersistentReference().
//   When the audSound is created, these parameters are exposed and used throughout 
//   the sound hierarchy, if possible.
//
// Note: This struct needs to be handled correctly in a range of places involving sounds, 
//  including whenever a sound type generates child sounds.  If altering this structure, 
//  search the whole project for occurrences of "audSoundInitParams" and check if that 
//  usage needs updating too - it probably will.
struct audSoundInternalInitParams
{
public:
	audSoundInternalInitParams() :
		WaveSlotIndex(-1),
		BucketId(0xff),
		TimerId(0x1F),
		AllowLoad(false),
		RemoveHierarchy(true),
		IsAuditioning(false)
	{
	}

	//////////////////////////////////////////////////////////////////////////
	// Data Members

	// PURPOSE
	//  Pointer to a waveslot into which the sound should load
	s16 WaveSlotIndex;

	// PURPOSE
	//	The sound pool bucket this hierarchy should be allocated from
	u8  BucketId;

	// PURPOSE
	//	This defines the timer used to update this hierarchy
	u8 TimerId : 5;

	// PURPOSE
	//  TRUE - (default) Allow this sound to request wave load
	bool AllowLoad : 1;

	// PURPOSE
	//  TRUE - (default) Remove the hierarchy after load as an optimization step.
	//         If child sound navigation is necessary set this parameter to FALSE.
	bool RemoveHierarchy : 1;

	// PURPOSE
	//	TRUE - enable debug functionality
	bool IsAuditioning : 1;
};

struct audSoundInitParams
{
public:
	audSoundInitParams() :

	  Position(Vector3::ZeroType),
		Volume(0.0f),
		PostSubmixAttenuation(0.0f),
		Pitch(0),
		Pan(-1),
		VolumeCurveScale(1.f),
		PrepareTimeLimit(0),
		Tracker(NULL),
		WaveSlot(NULL),
		Category(NULL),
		EnvironmentGroup(NULL),
		StartOffset(0),
		Predelay(0),
		AttackTime(0),
		TimerId(~0U),
		u32ClientVar(0),
		LPFCutoff(kVoiceFilterLPFMaxCutoff),
		HPFCutoff(0),
		VirtualisationScoreOffset(0),
		ShadowPcmSourceId(-1),
		PcmSourceChannelId(-1),
		SourceEffectSubmixId(-1),
		EffectRoute(0), // 0 means use parent value
		BucketId(0xff),
		SpeakerMask(0),
		PrimaryShadowSound(false),
		IsStartOffsetPercentage(false),
		UpdateEntity(false),
		AllowOrphaned(false),
		AllowLoad(false),
		RemoveHierarchy(true),
		ShouldPlayPhysically(false),
		IsAuditioning(false),
		TrackEntityPosition(false)
	{
	};

	//////////////////////////////////////////////////////////////////////////
	// Data Members


	// PURPOSE
	//	Pointer to sound position
	Vector3 	Position;

	struct audVariableValue
	{
		audVariableValue()
			: nameHash(0)
		{
		}
		u32 nameHash;
		float value;
	};
	enum {kMaxInitParamVariables = 4};
	atRangeArray<audVariableValue, kMaxInitParamVariables> Variables;
	// PURPOSE
	//  Initial volume (in dB) added to metadata and category volumes
	f32 Volume;

	// PURPOSE
	//	Volume attenuation (in dB) applied after submix processing
	f32 PostSubmixAttenuation;

	// PURPOSE
	//  Initial pitch
	s16	Pitch;

	// PURPOSE
	//	Defines the pan to use, -1 for unused (ie positioned)
	s16 Pan;

	// PURPOSE
	//	Requested volume curve scale multiplier
	f32 VolumeCurveScale;

	// PURPOSE
	//  Time limit to wait to prepare a sound
	s32	PrepareTimeLimit;

	// PURPOSE
	//  Pointer to a tracker
	const audTracker * Tracker;

	// PURPOSE
	//  Pointer to a waveslot into which the sound should load
	const audWaveSlot *	WaveSlot;

	// PURPOSE
	//  Pointer to a category assigment (if other than metadata)
	const audCategory *	Category;

	// PURPOSE
	//  Pointer to the occlusion group
	const audEnvironmentGroupInterface * EnvironmentGroup;

	s32 StartOffset;

	s32 Predelay;

	u32 AttackTime;

	union
	{
		u32 u32ClientVar;
		f32 f32ClientVar;
		const void *ptrClientVar;
	};

	// PURPOSE
	//	Defines the timer that should be used to update this sound
	u32 TimerId;

	// PURPOSE
	//	Defines the low pass filter cutoff 
	u32 LPFCutoff;
	// PURPOSE
	//	Defines the high pass filter cutoff 
	u32 HPFCutoff;

	// PURPOSE
	// Offset to apply to virtualisation score, to artificially boost a sound's score.
	// NOTES
	//	Should be used very carefully, and only in cases where a critical sound is heard dropping out
	//	of the mix even though it is quieter than other voices (for example a high pitched,
	//	pure tone such as a tinitus loop would be heard over a lower frequency noisy source)
	u32 VirtualisationScoreOffset;

	// PURPOSE
	//	This enables a sound to use the specified PCM generator as its source
	s32 ShadowPcmSourceId;

	// PURPOSE
	//	Override the PCM generator channel
	s32 PcmSourceChannelId;

	// PURPOSE
	//	Specify an optional source effect submix id
	s16 SourceEffectSubmixId;

	// PURPOSE
	//	The sound pool bucket this hierarchy should be allocated from
	u8  BucketId;

	// PURPOSE
	//	Defines the effect route for this sound
	u8 EffectRoute;

	// PURPOSE
	//	Speaker output routing flags
	u8 SpeakerMask;

	bool PrimaryShadowSound : 1;

	bool IsStartOffsetPercentage : 1;
	// PURPOSE
	//  FALSE - (default) The sound will not update its entity each frame.
	bool UpdateEntity : 1;

	// PURPOSE
	//  FALSE - (default) The sound will be released if the audEntity is destroyed
	bool AllowOrphaned : 1;

	// PURPOSE
	//  TRUE - (default) Load the sound
	bool AllowLoad : 1;

	// PURPOSE
	//  TRUE - (default) Remove the hierarchy after load as an optimization step.
	//         If child sound navigation is necessary set this parameter to FALSE.
	bool RemoveHierarchy : 1;

	// PURPOSE
	//	Set to true if this hierarchy should always be given a physical voice
	bool ShouldPlayPhysically : 1;

	bool IsAuditioning : 1;

	// PURPOSE
	//	When true the sound will query the owning audio entity for position/orientation, rather than
	//	the audTracker.
	bool TrackEntityPosition : 1;

	// PURPOSE
	//	Set the initial value of the specified variable
	// NOTES
	//	We only have space for a few variables in an initParams block; attempting to exceed that will trigger an assert
	// SEE ALSO
	// kMaxInitParamVariables
	bool SetVariableValue(const u32 nameHash, const float variableValue)
	{
		for(u32 i = 0; i < kMaxInitParamVariables; i++)
		{
			if(Variables[i].nameHash == 0 || Variables[i].nameHash == nameHash)
			{
				Variables[i].nameHash = nameHash;
				Variables[i].value = variableValue;
				return true;
			}
		}
		audAssertf(0, "Failed to store variable value in audSoundInitParams - %u,%f", nameHash, variableValue);
		return false;
	}

};

struct audSoundScratchInitParams
{
	audSoundScratchInitParams() :
	volCurveHash(~0U),
	volCurveScale(1.f),
	dopplerFactor(1.f),
	shadowPcmSourceId(-1),
	categoryIndex(-1),
	pcmSourceChannelId(-1),
	sourceEffectSubmixId(-1),
	volCurvePlateau(0),
	speakerMask(0),
	topLevelRequestedSettingsIndex(0xff),
	effectRoute(0),
	shouldAttenuateOverDistance(AUD_TRISTATE_UNSPECIFIED),
	shouldApplyEnvironmentalEffects(AUD_TRISTATE_UNSPECIFIED),
	shouldInvertPhase(false),
	shouldMuteOnUserMusic(false),
	overriddenCategory(false),
	shouldPlayPhysically(false),
	isUncancellable(false),
	shouldStopWhenVirtual(false)
	{

	}

	u32 volCurveHash;
	f32 volCurveScale;
	f32 dopplerFactor;
	s16 shadowPcmSourceId;	
	s16 categoryIndex;
	s8 pcmSourceChannelId;
	s8 sourceEffectSubmixId;
	u8 volCurvePlateau;
	u8 speakerMask;
	u8 topLevelRequestedSettingsIndex;
	u8 effectRoute : 6;
	
	u8 shouldAttenuateOverDistance : 2;
	u8 shouldApplyEnvironmentalEffects : 2;
	bool shouldInvertPhase : 1;
	bool shouldMuteOnUserMusic : 1;
	bool overriddenCategory : 1;
	bool shouldPlayPhysically : 1;
	bool isUncancellable : 1;
	bool shouldStopWhenVirtual : 1;
};

struct audSoundParentInitParams
{
	audSoundParentInitParams() : 
	Entity(NULL),
	EntitySoundRef(NULL),
	Tracker(NULL),
	Predelay(0),
	StartOffset(0),
	AttackTime(0),
	ShouldUpdateEntity (false),
	IsStartOffsetPercentage(false),
	TrackEntityPosition(false)
	{

	}

	audEntity *Entity;
	audSound **EntitySoundRef;
	audTracker *Tracker;
	u32 Predelay;
	s32 StartOffset;
	u32 AttackTime;
	bool ShouldUpdateEntity;
	bool IsStartOffsetPercentage;
	bool TrackEntityPosition;
};

enum audPrepareState
{
	AUD_PREPARING,
	AUD_PREPARED,
	AUD_PREPARE_FAILED,
};

struct audSoundCombineBuffer
{	
	audSoundCombineBuffer()
		: Volume(0.f)
		, PostSubmixVolumeAttenuation(0.f)
		, LPFCutoff(kVoiceFilterLPFMaxCutoff)
		, HPFCutoff(kVoiceFilterHPFMinCutoff)
		, Pan(-1)
		, Pitch(0)
		, VolumeCurveScale(u16(Float16::BINARY_1))
		, SmallReverbSend(0U)
		, MediumReverbSend(0U)
		, LargeReverbSend(0U)
		BANK_ONLY(, IsMuted(false))
		BANK_ONLY(, IsSoloed(false))
	{

	}
	f32		Volume;
	f32		PostSubmixVolumeAttenuation;
	u16		LPFCutoff;
	u16		HPFCutoff;
	s16		Pan;
	s16		Pitch;
	Float16 VolumeCurveScale;
	u8		SmallReverbSend;
	u8		MediumReverbSend;
	u8		LargeReverbSend;

#if RSG_BANK
	bool IsMuted;
	bool IsSoloed;
#endif // RSG_BANK
};

} // namespace rage

#endif // AUD_SOUNDCONTROL_H
