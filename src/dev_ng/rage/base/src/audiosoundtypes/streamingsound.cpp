//
// audiosoundtypes/streamingsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "streamingsound.h"

#include "simplesound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/debug.h"
#include "audiohardware/device.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/driver.h"
#include "audiohardware/streamingwaveslot.h"
#include "audiohardware/syncsource.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audStreamingSound);

audStreamingSound::~audStreamingSound()
{
	for(u32 i = 0; i < m_NumSoundRefs; i++)
	{
		if(GetChildSound(i))
		{
			DeleteChild(i);
		}

		if(m_RequestedSettingsIndicesForChildSounds[i] != 0xff)
		{
			GetRequestedSettingsForChild(i)->~audRequestedSettings();
			sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId, m_RequestedSettingsIndicesForChildSounds[i]);
			m_RequestedSettingsIndicesForChildSounds[i] = 0xff;
		}
	}

	audMixerSyncManager::Get()->Release(m_SyncId);
	m_SyncId = audMixerSyncManager::InvalidId;

}

#if !__SPU
audStreamingSound::audStreamingSound()
{
	m_CurrentPlayTime = 0;
	m_PlayTimeCalculationFrame = 0;
	m_StartOffsetToPlayMs = 0;
	m_HaveReleasedChildren = false;
	m_HaveStartedPreparing = false;
	m_StreamingWaveBankIndex = 0xffff;
	sysMemSet(&m_RequestedSettingsIndicesForChildSounds, 0xff, sizeof(m_RequestedSettingsIndicesForChildSounds));	
	m_SyncId = audMixerSyncManager::InvalidId;
}

bool audStreamingSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	bool wasInitSuccessful = false;	

	if(audSound::Init(metadata, initParams, scratchInitParams))
	{
		if(audVerifyf(GetStaticPool().IsReservedBucket(m_InitParams.BucketId), "StreamingSounds must be created in reserved buckets: %s", GetName()))
		{
			SetCanBeRemovedFromHierarchy(false);

			const StreamingSound *soundMetadata = (StreamingSound *)GetMetadata();

			m_NumSoundRefs = soundMetadata->numSoundRefs;

			m_InitParams.RemoveHierarchy = false;

			m_ShouldStopWhenChildStops = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(),
				FLAG_ID_STREAMINGSOUND_STOPWHENCHILDSTOPS) != AUD_TRISTATE_FALSE);
			m_IsLooping = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_STREAMINGSOUND_LOOPING) ==
				AUD_TRISTATE_TRUE);

			m_Duration = soundMetadata->Duration;

			m_SyncId = audDriver::GetMixer()->GetSyncManager().Allocate();

			// For now, all streaming voices must be played physically
			scratchInitParams->shouldPlayPhysically = true;

			wasInitSuccessful = CreateChildren(scratchInitParams, soundMetadata);
		}
	}

	return wasInitSuccessful;
}
#endif // !__SPU

audPrepareState audStreamingSound::AudioPrepare(audWaveSlot *UNUSED_PARAM(slot), const bool checkStateOnly)
{
	if(checkStateOnly)
	{
		return AUD_PREPARING;
	}

	if(!GetStreamingWaveSlot() || !GetStreamingWaveSlot()->IsStreaming())
	{
		audErrorf("Streaming sound wave slot not found for %s", GetName());
		return AUD_PREPARE_FAILED;
	}

	if(m_StreamingWaveBankIndex == 0xffff)
	{
		//Extract streaming Wave Bank name from first child simple sound.
		audSound *childSimpleSound;
		childSimpleSound = NULL;
		_SearchForSoundsByType(SimpleSound::TYPE_ID, &childSimpleSound, 1, true);

		SoundAssert(childSimpleSound);
		if(!childSimpleSound)
		{
			audErrorf("audStreamingSound with no SimpleSound children\n");
			return AUD_PREPARE_FAILED;
		}

		Assign(m_StreamingWaveBankIndex,((audSimpleSound *)childSimpleSound)->GetBankId());
	}

	if(m_StreamingWaveBankIndex == AUD_INVALID_BANK_ID)
	{
		return AUD_PREPARE_FAILED;
	}

#if !__SPU
	if(!m_HaveStartedPreparing)
	{
		if(!GetStreamingWaveSlot()->ResetLoadedState())
		{
			// We can't reset the slot while it's in use, but we wouldn't be able to succesfully prepare anyway
			// so wait until we managed to reset.
			return AUD_PREPARING;
		}
		m_HaveStartedPreparing = true;
	}
#endif

	_ComputePlaybackPredelayAndStartOffset();
	u32 startOffsetMs = GetPlaybackStartOffset();

	//audDisplayf("%s: AudioPrepare(): %d [%d]", GetName(), startOffsetMs, m_StreamingState);
	audPrepareState prepareState = AUD_PREPARE_FAILED;
	const audWaveSlot::audWaveSlotLoadStatus status = GetStreamingWaveSlot()->GetStreamingBankLoadingStatusForTimeMs(m_StreamingWaveBankIndex, startOffsetMs);

	DEBUG_STREAMING_ONLY(audDisplayf("GetStreamingBankLoadingStatusForTimeMs(%d) = %d", startOffsetMs, status));

	switch(status)
	{
		case audWaveSlot::LOADED:
			prepareState = InitChildSounds(startOffsetMs);

			if(prepareState == AUD_PREPARED)
			{
				m_StartOffsetToPlayMs = startOffsetMs;
			}
			break;

		case audWaveSlot::LOADING:
			prepareState = AUD_PREPARING;
			break;

		case audWaveSlot::FAILED:
			SetPrepareFailed();
			prepareState = AUD_PREPARE_FAILED;
			break;

		case audWaveSlot::NOT_REQUESTED:
			Assert(m_StreamingWaveBankIndex != AUD_INVALID_BANK_ID);
			GetStreamingWaveSlot()->PreloadStreamingBankBlocks(m_StreamingWaveBankIndex, startOffsetMs, m_IsLooping);
			prepareState = AUD_PREPARING;
			break;

		default:
			//Default case to shut the compiler up
			prepareState = AUD_PREPARE_FAILED;
			break;
	}

	if(prepareState != AUD_PREPARED)
	{
		SoundAssert(!HasPrepared());
		ClearHasPrepared();
	}

	return prepareState;
}

void audStreamingSound::AdjustStartOffset(s32 startOffsetDelta)
{
	audAssert(m_StartOffsetToPlayMs + startOffsetDelta >= 0);
	m_StartOffsetToPlayMs += startOffsetDelta;
}

audPrepareState audStreamingSound::InitChildSounds(u32 startOffsetMs)
{
	audPrepareState childState, overallState = AUD_PREPARED;

	for(u32 i=0; i<m_NumSoundRefs; i++)
	{
		audSound* childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->SetStartOffset(startOffsetMs, false);
			childState = childSound->_ManagedAudioPrepare(GetStreamingWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority());
			if(childState == AUD_PREPARE_FAILED)
			{
				//Bail out if any of our children have failed to prepare.
				return AUD_PREPARE_FAILED;
			}
			else if(childState == AUD_PREPARING)
			{
				overallState = AUD_PREPARING;
			}
		}
	}

	// Cache wave player Ids for shadowing purposes
	if(overallState == AUD_PREPARED && m_ChildWavePlayerIds.GetCount() == 0)
	{		
		audSound *childSimpleSound[kMaxChildWavePlayerIds] = {NULL};
		_SearchForSoundsByType(SimpleSound::TYPE_ID, &childSimpleSound[0],	kMaxChildWavePlayerIds, true);
		for(s32 i = 0; i < kMaxChildWavePlayerIds; i++)
		{
			if(childSimpleSound[i])
			{
				s16 wavePlayerId = -1;
				Assign(wavePlayerId, ((audSimpleSound*)childSimpleSound[i])->GetWavePlayerId());
				m_ChildWavePlayerIds.Append() = wavePlayerId;
			}
		}
	}

	return overallState;
}
#define SYNC_STREAM_CHANNELS 1
void audStreamingSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	// If there is a slave sync source set from above we'll use that
#if SYNC_STREAM_CHANNELS
	u32 syncSourceToMaster = m_SyncId;
	if(GetCurrentSyncSlaveId() != audMixerSyncManager::InvalidId)
	{
		syncSourceToMaster = GetCurrentSyncSlaveId();
	}

	audSoundSyncSetMasterId masterSyncId;
	masterSyncId.Set(syncSourceToMaster);
	audSoundSyncSetSlaveId slaveSyncId;
	slaveSyncId.Set(syncSourceToMaster);
#endif

	m_CurrentPlayTime = m_StartOffsetToPlayMs;
	m_PlayTimeCalculationFrame = audDriver::GetMixer()->GetMixerTimeFrames();

	//Initiate playback of all child sounds
	// Also setup the requesting settings for the child sounds to use our local copies if we have them.
	for(u32 childSoundIndex=0; childSoundIndex<m_NumSoundRefs; childSoundIndex++)
	{
		audSound *childSound = GetChildSound(childSoundIndex);
		if(childSound)
		{
			//Point the child sounds at our requested settings, so that their position etc. can be updated.
			//(if we have one.)
			if(m_RequestedSettingsIndicesForChildSounds[childSoundIndex]!=0xff)
			{
				childSound->_SetRequestedSettingsIndex(m_RequestedSettingsIndicesForChildSounds[childSoundIndex]);
			}

			childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
			childSound->SetStartOffset(m_StartOffsetToPlayMs, false);
			childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
		}
	}
}

bool audStreamingSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
#if SYNC_STREAM_CHANNELS
	u32 syncSourceToMaster = m_SyncId;
	if(GetCurrentSyncSlaveId() != audMixerSyncManager::InvalidId)
	{
		syncSourceToMaster = GetCurrentSyncSlaveId();
	}

	audSoundSyncSetMasterId masterSyncId;
	masterSyncId.Set(syncSourceToMaster);
	audSoundSyncSetSlaveId slaveSyncId;
	slaveSyncId.Set(syncSourceToMaster);
#endif

	bool isPlaying = true;
	isPlaying = UpdateChildren(timeInMs, combineBuffer);
	m_CurrentPlayTime = ComputeCurrentPlayTimeOfWave(&m_PlayTimeCalculationFrame);	
	return isPlaying;
}

bool audStreamingSound::UpdateChildren(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{	
	if(!GetStreamingWaveSlot())
	{
		return false;
	}

	bool isStillPlaying = false, shouldReleaseChildren = false;
	
	for(u32 i=0; i<m_NumSoundRefs; i++)
	{
		audSound* childSound = GetChildSound(i);
		if(childSound)
		{
			if(childSound->GetPlayState() == AUD_SOUND_PLAYING)
			{
				isStillPlaying |= childSound->_ManagedAudioUpdate(timeInMs, combineBuffer);
			}
			else if(childSound->GetPlayState() == AUD_SOUND_WAITING_TO_BE_DELETED)
			{
				shouldReleaseChildren = true;
			}
		}
	}

	if(!isStillPlaying)
	{
		//All our child sounds have finished, so we have too.
		return false;
	}
	else
	{
		if(shouldReleaseChildren && m_ShouldStopWhenChildStops && !m_HaveReleasedChildren)
		{
			for(u32 i = 0; i < m_NumSoundRefs; i++)
			{
				audSound* childSound = GetChildSound(i);
				if(childSound && childSound->GetPlayState() == AUD_SOUND_PLAYING)
				{
					childSound->_Release();
				}
			}

			m_HaveReleasedChildren = true;
		}
	}
	
	return true;
}

void audStreamingSound::AudioKill()
{
	for(u32 i=0 ; i < m_NumSoundRefs; i++)
	{		
		audSound* childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->_ManagedAudioKill();
			DeleteChild(i);
		}
	}
}

s32 audStreamingSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	s32 durationMs = kSoundLengthUnknown;

	if(m_Duration > 0)
	{
		durationMs = m_Duration;
	}
	else if(GetStreamingWaveSlot() && HasPrepared())
	{
		durationMs = GetStreamingWaveSlot()->ComputeStreamingWaveBankLengthMs();
	}

	if(m_IsLooping && isLooping)
	{
		*isLooping = true;
	}
	else if (isLooping)
	{
		*isLooping = false;
	}

	return durationMs;
}

s32 audStreamingSound::ComputeCurrentPlayTimeOfWave(u32* mixerCalculationFrame)
{
	// Take the playtime of the first simple sound we find
	audSound *childSimpleSound = NULL;
	_SearchForSoundsByType(SimpleSound::TYPE_ID, &childSimpleSound,	1, true);

	if(childSimpleSound)
	{
		SoundAssert(childSimpleSound->GetSoundTypeID() == SimpleSound::TYPE_ID);
		audSimpleSound *simpleSound = (audSimpleSound*)childSimpleSound;
		return simpleSound->GetCurrentPlayTimeOfWave(mixerCalculationFrame);
	}
	return -1;
}

audRequestedSettings *audStreamingSound::GetRequestedSettingsForChild(u32 soundIndex)
{
	Assert(soundIndex < audSound::kMaxChildren);

	if(soundIndex < audSound::kMaxChildren)
	{
		return (m_RequestedSettingsIndicesForChildSounds[soundIndex]!=0xff?(audRequestedSettings*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_RequestedSettingsIndicesForChildSounds[soundIndex]):NULL);
	}
	else
	{
		return NULL;
	}
}

#if !__SPU

bool audStreamingSound::CreateChildren(audSoundScratchInitParams *scratchInitParams, const StreamingSound *metadata)
{
	bool wasSuccessful = false;

	audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;

	if(m_NumSoundRefs <= audSound::kMaxChildren)
	{
		wasSuccessful = true;
		for(u32 i=0; i<m_NumSoundRefs; i++)
		{
			audSound* childSound = GetChildSound(i);
			Assert(!childSound);

			SetChildSound(i, SOUNDFACTORY.GetChildInstance(metadata->SoundRef[i].SoundId, this,
				&m_InitParams, scratchInitParams, false));
			childSound = GetChildSound(i);
			if(!childSound)
			{
				wasSuccessful = false;
			}
			else
			{
				*scratchInitParams = scratchInitParamsCopy;
			}
		}
	}
	else
	{
		SoundAssertMsg(0,"audStreamingSound with too many children\n");
	}

	return wasSuccessful;
}

#endif // !__SPU

} // namespace rage
