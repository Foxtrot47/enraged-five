// 
// sounddefs.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure definitions - do not edit.
// 

#ifndef AUD_SOUNDDEFS_H
#define AUD_SOUNDDEFS_H

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "audioengine/metadataref.h"

// macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
	#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
	#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) ((flagvar |= ((trival&0x03) << (flagid<<1))))
	namespace rage
	{
		enum TristateValue
		{
			AUD_TRISTATE_FALSE = 0,
			AUD_TRISTATE_TRUE,
			AUD_TRISTATE_UNSPECIFIED,
		}; // enum TristateValue
	} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE

namespace rage
{
	#define SOUNDDEFS_SCHEMA_VERSION 54
	
	#define SOUNDDEFS_METADATA_COMPILER_VERSION 2
	
	// NOTE: doesn't include abstract objects
	#define AUD_NUM_SOUNDDEFS 35
	
	#define AUD_DECLARE_SOUNDDEFS_FUNCTION(fn) void* GetAddressOf_##fn(rage::u8 classId);
	
	#define AUD_DEFINE_SOUNDDEFS_FUNCTION(fn) \
	void* GetAddressOf_##fn(rage::u8 classId) \
	{ \
		switch(classId) \
		{ \
			case LoopingSound::TYPE_ID: return (void*)&audLoopingSound::s##fn; \
			case EnvelopeSound::TYPE_ID: return (void*)&audEnvelopeSound::s##fn; \
			case TwinLoopSound::TYPE_ID: return (void*)&audTwinLoopSound::s##fn; \
			case SpeechSound::TYPE_ID: return (void*)&audSpeechSound::s##fn; \
			case OnStopSound::TYPE_ID: return (void*)&audOnStopSound::s##fn; \
			case WrapperSound::TYPE_ID: return (void*)&audWrapperSound::s##fn; \
			case SequentialSound::TYPE_ID: return (void*)&audSequentialSound::s##fn; \
			case StreamingSound::TYPE_ID: return (void*)&audStreamingSound::s##fn; \
			case RetriggeredOverlappedSound::TYPE_ID: return (void*)&audRetriggeredOverlappedSound::s##fn; \
			case CrossfadeSound::TYPE_ID: return (void*)&audCrossfadeSound::s##fn; \
			case CollapsingStereoSound::TYPE_ID: return (void*)&audCollapsingStereoSound::s##fn; \
			case SimpleSound::TYPE_ID: return (void*)&audSimpleSound::s##fn; \
			case MultitrackSound::TYPE_ID: return (void*)&audMultitrackSound::s##fn; \
			case RandomizedSound::TYPE_ID: return (void*)&audRandomizedSound::s##fn; \
			case EnvironmentSound::TYPE_ID: return (void*)&audEnvironmentSound::s##fn; \
			case DynamicEntitySound::TYPE_ID: return (void*)&audDynamicEntitySound::s##fn; \
			case SequentialOverlapSound::TYPE_ID: return (void*)&audSequentialOverlapSound::s##fn; \
			case ModularSynthSound::TYPE_ID: return (void*)&audModularSynthSound::s##fn; \
			case GranularSound::TYPE_ID: return (void*)&audGranularSound::s##fn; \
			case DirectionalSound::TYPE_ID: return (void*)&audDirectionalSound::s##fn; \
			case KineticSound::TYPE_ID: return (void*)&audKineticSound::s##fn; \
			case SwitchSound::TYPE_ID: return (void*)&audSwitchSound::s##fn; \
			case VariableCurveSound::TYPE_ID: return (void*)&audVariableCurveSound::s##fn; \
			case VariablePrintValueSound::TYPE_ID: return (void*)&audVariablePrintValueSound::s##fn; \
			case VariableBlockSound::TYPE_ID: return (void*)&audVariableBlockSound::s##fn; \
			case IfSound::TYPE_ID: return (void*)&audIfSound::s##fn; \
			case MathOperationSound::TYPE_ID: return (void*)&audMathOperationSound::s##fn; \
			case ParameterTransformSound::TYPE_ID: return (void*)&audParameterTransformSound::s##fn; \
			case FluctuatorSound::TYPE_ID: return (void*)&audFluctuatorSound::s##fn; \
			case AutomationSound::TYPE_ID: return (void*)&audAutomationSound::s##fn; \
			case ExternalStreamSound::TYPE_ID: return (void*)&audExternalStreamSound::s##fn; \
			default: return NULL; \
		} \
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gSoundsGetBaseTypeId(const rage::u32 classId);
	
	// PURPOSE - Determines if a type inherits from another type
	bool gSoundsIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId);
	
	// PURPOSE - Determines if a type inherits from another type
	template<class _ObjectType>
	bool gSoundsIsOfType(const _ObjectType* const obj, const rage::u32 baseTypeId)
	{
		return gSoundsIsOfType(obj->ClassId, baseTypeId);
	}
	
	// 
	// Enumerations
	// 
	enum EffectRoutes
	{
		EFFECT_ROUTE_AS_PARENT = 0,
		EFFECT_ROUTE_MUSIC,
		EFFECT_ROUTE_FRONT_END,
		EFFECT_ROUTE_POSITIONED,
		EFFECT_ROUTE_SML_REVERB_FULL_WET,
		EFFECT_ROUTE_MED_REVERB_FULL_WET,
		EFFECT_ROUTE_LRG_REVERB_FULL_WET,
		EFFECT_ROUTE_SFX,
		EFFECT_ROUTE_MASTER,
		EFFECT_ROUTE_PADSPEAKER_1,
		EFFECT_ROUTE_POSITIONED_MUSIC,
		EFFECT_ROUTE_PADHAPTICS_1,
		NUM_EFFECTROUTES,
		EFFECTROUTES_MAX = NUM_EFFECTROUTES,
	}; // enum EffectRoutes
	const char* EffectRoutes_ToString(const EffectRoutes value);
	EffectRoutes EffectRoutes_Parse(const char* str, const EffectRoutes defaultValue);
	
	enum ParameterDestinations
	{
		PARAM_DESTINATION_VOLUME = 0,
		PARAM_DESTINATION_PITCH,
		PARAM_DESTINATION_PAN,
		PARAM_DESTINATION_STARTOFFSET,
		PARAM_DESTINATION_PREDELAY,
		PARAM_DESTINATION_LPF,
		PARAM_DESTINATION_HPF,
		PARAM_DESTINATION_VARIABLE,
		PARAM_DESTINATION_ROLLOFF,
		PARAM_DESTINATION_ELEVATIONPAN,
		PARAM_DESTINATION_HAPTICSENDLEVEL,
		NUM_PARAMETERDESTINATIONS,
		PARAMETERDESTINATIONS_MAX = NUM_PARAMETERDESTINATIONS,
	}; // enum ParameterDestinations
	const char* ParameterDestinations_ToString(const ParameterDestinations value);
	ParameterDestinations ParameterDestinations_Parse(const char* str, const ParameterDestinations defaultValue);
	
	enum EnvelopeSoundMode
	{
		kEnvelopeSoundVolume = 0,
		kEnvelopeSoundPitch,
		kEnvelopeSoundPan,
		kEnvelopeSoundLPF,
		kEnvelopeSoundHPF,
		kEnvelopeSoundVariable,
		NUM_ENVELOPESOUNDMODE,
		ENVELOPESOUNDMODE_MAX = NUM_ENVELOPESOUNDMODE,
	}; // enum EnvelopeSoundMode
	const char* EnvelopeSoundMode_ToString(const EnvelopeSoundMode value);
	EnvelopeSoundMode EnvelopeSoundMode_Parse(const char* str, const EnvelopeSoundMode defaultValue);
	
	enum CrossfadeMode
	{
		CROSSFADE_MODE_FIXED = 0,
		CROSSFADE_MODE_BOTH,
		CROSSFADE_MODE_RETRIGGER,
		CROSSFADE_MODE_POSITION_RELATIVE_PAN,
		NUM_CROSSFADEMODE,
		CROSSFADEMODE_MAX = NUM_CROSSFADEMODE,
	}; // enum CrossfadeMode
	const char* CrossfadeMode_ToString(const CrossfadeMode value);
	CrossfadeMode CrossfadeMode_Parse(const char* str, const CrossfadeMode defaultValue);
	
	enum VirtualisationBehaviour
	{
		kVirtualisationNormal = 0,
		kVirtualisationStop,
		kVirtualisationUcancellable,
		NUM_VIRTUALISATIONBEHAVIOUR,
		VIRTUALISATIONBEHAVIOUR_MAX = NUM_VIRTUALISATIONBEHAVIOUR,
	}; // enum VirtualisationBehaviour
	const char* VirtualisationBehaviour_ToString(const VirtualisationBehaviour value);
	VirtualisationBehaviour VirtualisationBehaviour_Parse(const char* str, const VirtualisationBehaviour defaultValue);
	
	enum GranularPitchCalcMode
	{
		kGranularPitchMapChannel0 = 0,
		kGranularPitchMapChannel1,
		kGranularPitchMapChannel2,
		kGranularPitchMapChannel3,
		kGranularPitchMapChannel4,
		kGranularPitchMapChannel5,
		kGranularPitchMapClamp,
		kGranularPitchMapDirectionalClamp,
		kGranularPitchMapDirectionalMinMax,
		kGranularPitchMapAverage,
		NUM_GRANULARPITCHCALCMODE,
		GRANULARPITCHCALCMODE_MAX = NUM_GRANULARPITCHCALCMODE,
	}; // enum GranularPitchCalcMode
	const char* GranularPitchCalcMode_ToString(const GranularPitchCalcMode value);
	GranularPitchCalcMode GranularPitchCalcMode_Parse(const char* str, const GranularPitchCalcMode defaultValue);
	
	enum VariableUsage
	{
		VARIABLE_USAGE_SOUND = 0,
		VARIABLE_USAGE_CODE,
		VARIABLE_USAGE_CONSTANT,
		NUM_VARIABLEUSAGE,
		VARIABLEUSAGE_MAX = NUM_VARIABLEUSAGE,
	}; // enum VariableUsage
	const char* VariableUsage_ToString(const VariableUsage value);
	VariableUsage VariableUsage_Parse(const char* str, const VariableUsage defaultValue);
	
	enum ConditionTypes
	{
		IF_CONDITION_LESS_THAN = 0,
		IF_CONDITION_LESS_THAN_OR_EQUAL_TO,
		IF_CONDITION_GREATER_THAN,
		IF_CONDITION_GREATER_THAN_OR_EQUAL_TO,
		IF_CONDITION_EQUAL_TO,
		IF_CONDITION_NOT_EQUAL_TO,
		NUM_CONDITIONTYPES,
		CONDITIONTYPES_MAX = NUM_CONDITIONTYPES,
	}; // enum ConditionTypes
	const char* ConditionTypes_ToString(const ConditionTypes value);
	ConditionTypes ConditionTypes_Parse(const char* str, const ConditionTypes defaultValue);
	
	enum MathOperations
	{
		MATH_OPERATION_ADD = 0,
		MATH_OPERATION_SUBTRACT,
		MATH_OPERATION_MULTIPLY,
		MATH_OPERATION_DIVIDE,
		MATH_OPERATION_SET,
		MATH_OPERATION_MOD,
		MATH_OPERATION_MIN,
		MATH_OPERATION_MAX,
		MATH_OPERATION_ABS,
		MATH_OPERATION_SIGN,
		MATH_OPERATION_FLOOR,
		MATH_OPERATION_CEIL,
		MATH_OPERATION_RAND,
		MATH_OPERATION_SIN,
		MATH_OPERATION_COS,
		MATH_OPERATION_SQRT,
		MATH_OPERATION_DBTOLINEAR,
		MATH_OPERATION_LINEARTODB,
		MATH_OPERATION_PITCHTORATIO,
		MATH_OPERATION_RATIOTOPITCH,
		MATH_OPERATION_GETTIME,
		MATH_OPERATION_FSEL,
		MATH_OPERATION_VALUEINRANGE,
		MATH_OPERATION_CLAMP,
		MATH_OPERATION_POW,
		MATH_OPERATION_ROUND,
		MATH_OPERATION_SCALEDSIN,
		MATH_OPERATION_SCALEDTRI,
		MATH_OPERATION_SCALEDSAW,
		MATH_OPERATION_SCALEDSQUARE,
		MATH_OPERATION_SMOOTH,
		MATH_OPERATION_GETSCALEDTIME,
		MATH_OPERATION_CLAMPRANGE,
		MATH_OPERATION_GETPOS_X,
		MATH_OPERATION_GETPOS_Y,
		MATH_OPERATION_GETPOS_Z,
		MATH_OPERATION_TRACE,
		NUM_MATHOPERATIONS,
		MATHOPERATIONS_MAX = NUM_MATHOPERATIONS,
	}; // enum MathOperations
	const char* MathOperations_ToString(const MathOperations value);
	MathOperations MathOperations_Parse(const char* str, const MathOperations defaultValue);
	
	enum FluctuatorModes
	{
		PROBABILITY_BASED = 0,
		TIME_BASED,
		NUM_FLUCTUATORMODES,
		FLUCTUATORMODES_MAX = NUM_FLUCTUATORMODES,
	}; // enum FluctuatorModes
	const char* FluctuatorModes_ToString(const FluctuatorModes value);
	FluctuatorModes FluctuatorModes_Parse(const char* str, const FluctuatorModes defaultValue);
	
	enum NoteRangeMode
	{
		NOTE_RANGE_TRACK_PITCH = 0,
		NOTE_RANGE_UNITY_PITCH,
		NUM_NOTERANGEMODE,
		NOTERANGEMODE_MAX = NUM_NOTERANGEMODE,
	}; // enum NoteRangeMode
	const char* NoteRangeMode_ToString(const NoteRangeMode value);
	NoteRangeMode NoteRangeMode_Parse(const char* str, const NoteRangeMode defaultValue);
	
	// disable struct alignment
	#if !__SPU
	#pragma pack(push, r1, 1)
	#endif // !__SPU
		// 
		// Sound
		// 
		enum SoundFlagIds
		{
			FLAG_ID_SOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_SOUND_DISTANCEATTENUATION,
			FLAG_ID_SOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_SOUND_VIRTUALISEASGROUP,
			FLAG_ID_SOUND_DYNAMICPREPARE,
			FLAG_ID_SOUND_MUTEONUSERMUSIC,
			FLAG_ID_SOUND_INVERTPHASE,
			FLAG_ID_SOUND_MUTE,
			FLAG_ID_SOUND_MAX,
		}; // enum SoundFlagIds
		
		struct Sound
		{
			static const rage::u32 TYPE_ID = 0;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			Sound() :
				ClassId(0xFF),
				DEBUGAUDIO_ONLY(NameTableOffset(0XFFFFFF),)
				Compression(0xFFFFFFFF),
				Flags(0xAAAAAAAA),
				Volume(0),
				VolumeVariance(0),
				Pitch(0),
				PitchVariance(0),
				Pan(-1),
				PanVariance(0),
				preDelay(0),
				preDelayVariance(0),
				StartOffset(0U),
				StartOffsetVariance(0U),
				AttackTime(0),
				ReleaseTime(-1),
				DopplerFactor(100),
				Category(0U),
				LPFCutoff(24000),
				LPFCutoffVariance(0),
				HPFCutoff(0),
				HPFCutoffVariance(0),
				VolumeCurve(0U),
				VolumeCurveScale(100),
				VolumeCurvePlateau(0),
				EffectRoute(EFFECT_ROUTE_AS_PARENT),
				PreDelayVariable(0U),
				StartOffsetVariable(0U),
				SmallReverbSend(0),
				MediumReverbSend(0),
				LargeReverbSend(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			#if __USEDEBUGAUDIO
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			#else
			rage::u8 ClassId;
			#endif // __USEDEBUGAUDIO
			rage::u32 Compression;
			rage::u32 Flags;
			
			struct tParentOverrides
			{
				union
				{
					rage::u16 Value;
					struct
					{
						bool VolumeOverridesParent:1;
						bool PitchOverridesParent:1;
						bool PanOverridesParent:1;
						bool preDelayOverridesParent:1;
						bool StartOffsetOverridesParent:1;
						bool AttackTimeOverridesParent:1;
						bool ReleaseTimeOverridesParent:1;
						bool DopplerFactorOverridesParent:1;
						bool LPFCutoffOverridesParent:1;
						bool HPFCutoffOverridesParent:1;
						bool VolumeCurveOverridesParent:1;
						bool VolumeCurveScaleOverridesParent:1;
						bool VolumeCurvePlateauOverridesParent:1;
						bool SpeakerMaskOverridesParent:1;
						bool padding:2; // padding to next byte boundary
					} BitFields;
				};
			} ParentOverrides; // struct tParentOverrides
			
			rage::s16 Volume;
			rage::u16 VolumeVariance;
			rage::s16 Pitch;
			rage::u16 PitchVariance;
			rage::s16 Pan;
			rage::u16 PanVariance;
			rage::u16 preDelay;
			rage::u16 preDelayVariance;
			rage::u32 StartOffset;
			rage::u32 StartOffsetVariance;
			rage::u16 AttackTime;
			rage::s16 ReleaseTime;
			rage::u16 DopplerFactor;
			rage::u32 Category;
			rage::u16 LPFCutoff;
			rage::u16 LPFCutoffVariance;
			rage::u16 HPFCutoff;
			rage::u16 HPFCutoffVariance;
			rage::u32 VolumeCurve;
			rage::u16 VolumeCurveScale;
			rage::u8 VolumeCurvePlateau;
			
			struct tSpeakerMask
			{
				union
				{
					rage::u8 Value;
					struct
					{
						#if __BE
							bool FrontRightOfCenter:1;
							bool FrontLeftOfCenter:1;
							bool RearRight:1;
							bool RearLeft:1;
							bool LFE:1;
							bool FrontCenter:1;
							bool FrontRight:1;
							bool FrontLeft:1;
						#else // __BE
							bool FrontLeft:1;
							bool FrontRight:1;
							bool FrontCenter:1;
							bool LFE:1;
							bool RearLeft:1;
							bool RearRight:1;
							bool FrontLeftOfCenter:1;
							bool FrontRightOfCenter:1;
						#endif // __BE
					} BitFields;
				};
			} SpeakerMask; // struct tSpeakerMask
			
			rage::u8 EffectRoute;
			rage::u32 PreDelayVariable;
			rage::u32 StartOffsetVariable;
			rage::u16 SmallReverbSend;
			rage::u16 MediumReverbSend;
			rage::u16 LargeReverbSend;
		} SPU_ONLY(__attribute__((packed))); // struct Sound
		
		// 
		// LoopingSound
		// 
		enum LoopingSoundFlagIds
		{
			FLAG_ID_LOOPINGSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_LOOPINGSOUND_DISTANCEATTENUATION,
			FLAG_ID_LOOPINGSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_LOOPINGSOUND_VIRTUALISEASGROUP,
			FLAG_ID_LOOPINGSOUND_DYNAMICPREPARE,
			FLAG_ID_LOOPINGSOUND_MUTEONUSERMUSIC,
			FLAG_ID_LOOPINGSOUND_INVERTPHASE,
			FLAG_ID_LOOPINGSOUND_MUTE,
			FLAG_ID_LOOPINGSOUND_ENABLESPLICING,
			FLAG_ID_LOOPINGSOUND_MAX,
		}; // enum LoopingSoundFlagIds
		
		struct LoopingSound
		{
			static const rage::u32 TYPE_ID = 1;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			LoopingSound() :
				LoopCount(-1),
				LoopCountVariance(0),
				LoopPoint(0),
				LoopCountVariable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::s16 LoopCount;
			rage::u16 LoopCountVariance;
			rage::u16 LoopPoint;
			rage::audMetadataRef SoundRef;
			rage::u32 LoopCountVariable;
		} SPU_ONLY(__attribute__((packed))); // struct LoopingSound
		
		// 
		// EnvelopeSound
		// 
		enum EnvelopeSoundFlagIds
		{
			FLAG_ID_ENVELOPESOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_ENVELOPESOUND_DISTANCEATTENUATION,
			FLAG_ID_ENVELOPESOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_ENVELOPESOUND_VIRTUALISEASGROUP,
			FLAG_ID_ENVELOPESOUND_DYNAMICPREPARE,
			FLAG_ID_ENVELOPESOUND_MUTEONUSERMUSIC,
			FLAG_ID_ENVELOPESOUND_INVERTPHASE,
			FLAG_ID_ENVELOPESOUND_MUTE,
			FLAG_ID_ENVELOPESOUND_SHOULDSTOPCHILDREN,
			FLAG_ID_ENVELOPESOUND_SHOULDCASCADERELEASE,
			FLAG_ID_ENVELOPESOUND_MAX,
		}; // enum EnvelopeSoundFlagIds
		
		struct EnvelopeSound
		{
			static const rage::u32 TYPE_ID = 2;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			EnvelopeSound() :
				AttackVariable(0U),
				DecayVariable(0U),
				SustainVariable(0U),
				HoldVariable(0U),
				ReleaseVariable(0U),
				Mode(kEnvelopeSoundVolume),
				padding0(0),
				padding1(0),
				OutputVariable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			struct tEnvelope
			{
				rage::u16 Attack;
				rage::u16 AttackVariance;
				rage::u16 Decay;
				rage::u16 DecayVariance;
				rage::u8 Sustain;
				rage::u8 SustainVariance;
				rage::s32 Hold;
				rage::u16 HoldVariance;
				rage::s32 Release;
				rage::u32 ReleaseVariance;
				rage::u32 AttackCurve;
				rage::u32 DecayCurve;
				rage::u32 ReleaseCurve;
			} Envelope; // struct tEnvelope
			
			rage::u32 AttackVariable;
			rage::u32 DecayVariable;
			rage::u32 SustainVariable;
			rage::u32 HoldVariable;
			rage::u32 ReleaseVariable;
			rage::audMetadataRef SoundRef;
			rage::u8 Mode;
			rage::u8 padding0;
			rage::u16 padding1;
			rage::u32 OutputVariable;
			
			struct tOutputRange
			{
				float min;
				float max;
			} OutputRange; // struct tOutputRange
			
		} SPU_ONLY(__attribute__((packed))); // struct EnvelopeSound
		
		// 
		// TwinLoopSound
		// 
		enum TwinLoopSoundFlagIds
		{
			FLAG_ID_TWINLOOPSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_TWINLOOPSOUND_DISTANCEATTENUATION,
			FLAG_ID_TWINLOOPSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_TWINLOOPSOUND_VIRTUALISEASGROUP,
			FLAG_ID_TWINLOOPSOUND_DYNAMICPREPARE,
			FLAG_ID_TWINLOOPSOUND_MUTEONUSERMUSIC,
			FLAG_ID_TWINLOOPSOUND_INVERTPHASE,
			FLAG_ID_TWINLOOPSOUND_MUTE,
			FLAG_ID_TWINLOOPSOUND_IGNORESTOPPEDCHILD,
			FLAG_ID_TWINLOOPSOUND_MAX,
		}; // enum TwinLoopSoundFlagIds
		
		struct TwinLoopSound
		{
			static const rage::u32 TYPE_ID = 3;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			TwinLoopSound() :
				MinSwapTime(0),
				MaxSwapTime(0),
				MinCrossfadeTime(0),
				MaxCrossfadeTime(0),
				CrossfadeCurve(1011033664U), // EQUAL_POWER_RISE
				MinSwapTimeVariable(0U),
				MaxSwapTimeVariable(0U),
				MinCrossfadeTimeVariable(0U),
				MaxCrossfadeTimeVariable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u16 MinSwapTime;
			rage::u16 MaxSwapTime;
			rage::u16 MinCrossfadeTime;
			rage::u16 MaxCrossfadeTime;
			rage::u32 CrossfadeCurve;
			rage::u32 MinSwapTimeVariable;
			rage::u32 MaxSwapTimeVariable;
			rage::u32 MinCrossfadeTimeVariable;
			rage::u32 MaxCrossfadeTimeVariable;
			
			static const rage::u8 MAX_SOUNDREFS = 2;
			rage::u8 numSoundRefs;
			struct tSoundRef
			{
				rage::audMetadataRef SoundId;
			} SoundRef[MAX_SOUNDREFS]; // struct tSoundRef
			
		} SPU_ONLY(__attribute__((packed))); // struct TwinLoopSound
		
		// 
		// SpeechSound
		// 
		enum SpeechSoundFlagIds
		{
			FLAG_ID_SPEECHSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_SPEECHSOUND_DISTANCEATTENUATION,
			FLAG_ID_SPEECHSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_SPEECHSOUND_VIRTUALISEASGROUP,
			FLAG_ID_SPEECHSOUND_DYNAMICPREPARE,
			FLAG_ID_SPEECHSOUND_MUTEONUSERMUSIC,
			FLAG_ID_SPEECHSOUND_INVERTPHASE,
			FLAG_ID_SPEECHSOUND_MUTE,
			FLAG_ID_SPEECHSOUND_MAX,
		}; // enum SpeechSoundFlagIds
		
		struct SpeechSound
		{
			static const rage::u32 TYPE_ID = 4;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			SpeechSound() :
				LastVariation(1U),
				DynamicFieldName(0U),
				VoiceName(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 LastVariation;
			rage::u32 DynamicFieldName;
			rage::u32 VoiceName;
			rage::u8 ContextNameLen;
			char ContextName[255];
		} SPU_ONLY(__attribute__((packed))); // struct SpeechSound
		
		// 
		// OnStopSound
		// 
		enum OnStopSoundFlagIds
		{
			FLAG_ID_ONSTOPSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_ONSTOPSOUND_DISTANCEATTENUATION,
			FLAG_ID_ONSTOPSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_ONSTOPSOUND_VIRTUALISEASGROUP,
			FLAG_ID_ONSTOPSOUND_DYNAMICPREPARE,
			FLAG_ID_ONSTOPSOUND_MUTEONUSERMUSIC,
			FLAG_ID_ONSTOPSOUND_INVERTPHASE,
			FLAG_ID_ONSTOPSOUND_MUTE,
			FLAG_ID_ONSTOPSOUND_MAX,
		}; // enum OnStopSoundFlagIds
		
		struct OnStopSound
		{
			static const rage::u32 TYPE_ID = 5;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef ChildSoundRef;
			rage::audMetadataRef StopSoundRef;
			rage::audMetadataRef FinishedSoundRef;
		} SPU_ONLY(__attribute__((packed))); // struct OnStopSound
		
		// 
		// WrapperSound
		// 
		enum WrapperSoundFlagIds
		{
			FLAG_ID_WRAPPERSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_WRAPPERSOUND_DISTANCEATTENUATION,
			FLAG_ID_WRAPPERSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_WRAPPERSOUND_VIRTUALISEASGROUP,
			FLAG_ID_WRAPPERSOUND_DYNAMICPREPARE,
			FLAG_ID_WRAPPERSOUND_MUTEONUSERMUSIC,
			FLAG_ID_WRAPPERSOUND_INVERTPHASE,
			FLAG_ID_WRAPPERSOUND_MUTE,
			FLAG_ID_WRAPPERSOUND_MAX,
		}; // enum WrapperSoundFlagIds
		
		struct WrapperSound
		{
			static const rage::u32 TYPE_ID = 6;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			WrapperSound() :
				LastPlayTime(0U),
				MinRepeatTime(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			rage::u32 LastPlayTime;
			rage::audMetadataRef FallbackSoundRef;
			rage::u16 MinRepeatTime;
			
			static const rage::u8 MAX_VARIABLEREFS = 8;
			rage::u8 numVariableRefs;
			struct tVariableRefs
			{
				rage::u32 VR_Ref;
				rage::u8 Destination;
			} VariableRefs[MAX_VARIABLEREFS]; // struct tVariableRefs
			
		} SPU_ONLY(__attribute__((packed))); // struct WrapperSound
		
		// 
		// SequentialSound
		// 
		enum SequentialSoundFlagIds
		{
			FLAG_ID_SEQUENTIALSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_SEQUENTIALSOUND_DISTANCEATTENUATION,
			FLAG_ID_SEQUENTIALSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_SEQUENTIALSOUND_VIRTUALISEASGROUP,
			FLAG_ID_SEQUENTIALSOUND_DYNAMICPREPARE,
			FLAG_ID_SEQUENTIALSOUND_MUTEONUSERMUSIC,
			FLAG_ID_SEQUENTIALSOUND_INVERTPHASE,
			FLAG_ID_SEQUENTIALSOUND_MUTE,
			FLAG_ID_SEQUENTIALSOUND_OPTIMIZEPLAYBACK,
			FLAG_ID_SEQUENTIALSOUND_MAX,
		}; // enum SequentialSoundFlagIds
		
		struct SequentialSound
		{
			static const rage::u32 TYPE_ID = 7;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			static const rage::u8 MAX_SOUNDREFS = 255;
			rage::u8 numSoundRefs;
			struct tSoundRef
			{
				rage::audMetadataRef SoundId;
			} SoundRef[MAX_SOUNDREFS]; // struct tSoundRef
			
		} SPU_ONLY(__attribute__((packed))); // struct SequentialSound
		
		// 
		// StreamingSound
		// 
		enum StreamingSoundFlagIds
		{
			FLAG_ID_STREAMINGSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_STREAMINGSOUND_DISTANCEATTENUATION,
			FLAG_ID_STREAMINGSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_STREAMINGSOUND_VIRTUALISEASGROUP,
			FLAG_ID_STREAMINGSOUND_DYNAMICPREPARE,
			FLAG_ID_STREAMINGSOUND_MUTEONUSERMUSIC,
			FLAG_ID_STREAMINGSOUND_INVERTPHASE,
			FLAG_ID_STREAMINGSOUND_MUTE,
			FLAG_ID_STREAMINGSOUND_STOPWHENCHILDSTOPS,
			FLAG_ID_STREAMINGSOUND_LOOPING,
			FLAG_ID_STREAMINGSOUND_STREAMPHYSICALLY,
			FLAG_ID_STREAMINGSOUND_MAX,
		}; // enum StreamingSoundFlagIds
		
		struct StreamingSound
		{
			static const rage::u32 TYPE_ID = 8;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			StreamingSound() :
				Duration(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 Duration;
			
			static const rage::u8 MAX_SOUNDREFS = 32;
			rage::u8 numSoundRefs;
			struct tSoundRef
			{
				rage::audMetadataRef SoundId;
			} SoundRef[MAX_SOUNDREFS]; // struct tSoundRef
			
		} SPU_ONLY(__attribute__((packed))); // struct StreamingSound
		
		// 
		// RetriggeredOverlappedSound
		// 
		enum RetriggeredOverlappedSoundFlagIds
		{
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_DISTANCEATTENUATION,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_VIRTUALISEASGROUP,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_DYNAMICPREPARE,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_MUTEONUSERMUSIC,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_INVERTPHASE,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_MUTE,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_RELEASECHILDREN,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_ALLOWCHILDTOFINISH,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_SETLOOPCOUNTVARIABLEONCE,
			FLAG_ID_RETRIGGEREDOVERLAPPEDSOUND_MAX,
		}; // enum RetriggeredOverlappedSoundFlagIds
		
		struct RetriggeredOverlappedSound
		{
			static const rage::u32 TYPE_ID = 9;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			RetriggeredOverlappedSound() :
				LoopCount(-1),
				LoopCountVariance(0),
				DelayTime(0),
				DelayTimeVariance(0),
				LoopCountVariable(0U),
				DelayTimeVariable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::s16 LoopCount;
			rage::u16 LoopCountVariance;
			rage::u16 DelayTime;
			rage::u16 DelayTimeVariance;
			rage::u32 LoopCountVariable;
			rage::u32 DelayTimeVariable;
			rage::audMetadataRef StartSound;
			rage::audMetadataRef SoundRef;
			rage::audMetadataRef StopSound;
		} SPU_ONLY(__attribute__((packed))); // struct RetriggeredOverlappedSound
		
		// 
		// CrossfadeSound
		// 
		enum CrossfadeSoundFlagIds
		{
			FLAG_ID_CROSSFADESOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_CROSSFADESOUND_DISTANCEATTENUATION,
			FLAG_ID_CROSSFADESOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_CROSSFADESOUND_VIRTUALISEASGROUP,
			FLAG_ID_CROSSFADESOUND_DYNAMICPREPARE,
			FLAG_ID_CROSSFADESOUND_MUTEONUSERMUSIC,
			FLAG_ID_CROSSFADESOUND_INVERTPHASE,
			FLAG_ID_CROSSFADESOUND_MUTE,
			FLAG_ID_CROSSFADESOUND_MAX,
		}; // enum CrossfadeSoundFlagIds
		
		struct CrossfadeSound
		{
			static const rage::u32 TYPE_ID = 10;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			CrossfadeSound() :
				Mode(CROSSFADE_MODE_BOTH),
				MinDistance(-1.0f),
				MaxDistance(-1.0f),
				Hysteresis(0.0f),
				CrossfadeCurve(3949077534U), // EQUAL_POWER_FALL_CROSSFADE
				DistanceVariable(0U),
				MinDistanceVariable(0U),
				MaxDistanceVariable(0U),
				HysteresisVariable(0U),
				CrossfadeVariable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef NearSoundRef;
			rage::audMetadataRef FarSoundRef;
			rage::u8 Mode;
			float MinDistance;
			float MaxDistance;
			float Hysteresis;
			rage::u32 CrossfadeCurve;
			rage::u32 DistanceVariable;
			rage::u32 MinDistanceVariable;
			rage::u32 MaxDistanceVariable;
			rage::u32 HysteresisVariable;
			rage::u32 CrossfadeVariable;
		} SPU_ONLY(__attribute__((packed))); // struct CrossfadeSound
		
		// 
		// CollapsingStereoSound
		// 
		enum CollapsingStereoSoundFlagIds
		{
			FLAG_ID_COLLAPSINGSTEREOSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_COLLAPSINGSTEREOSOUND_DISTANCEATTENUATION,
			FLAG_ID_COLLAPSINGSTEREOSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_COLLAPSINGSTEREOSOUND_VIRTUALISEASGROUP,
			FLAG_ID_COLLAPSINGSTEREOSOUND_DYNAMICPREPARE,
			FLAG_ID_COLLAPSINGSTEREOSOUND_MUTEONUSERMUSIC,
			FLAG_ID_COLLAPSINGSTEREOSOUND_INVERTPHASE,
			FLAG_ID_COLLAPSINGSTEREOSOUND_MUTE,
			FLAG_ID_COLLAPSINGSTEREOSOUND_MAX,
		}; // enum CollapsingStereoSoundFlagIds
		
		struct CollapsingStereoSound
		{
			static const rage::u32 TYPE_ID = 11;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			CollapsingStereoSound() :
				MinDistance(-1.0f),
				MaxDistance(-1.0f),
				MinDistanceVariable(0U),
				MaxDistanceVariable(0U),
				CrossfadeOverrideVariable(0U),
				FrontendLeftPan(0U),
				FrontendRightPan(0U),
				PositionRelativePanDamping(1.0f),
				PositionRelativePanDampingVariable(0U),
				Mode(CROSSFADE_MODE_BOTH)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef LeftSoundRef;
			rage::audMetadataRef RightSoundRef;
			float MinDistance;
			float MaxDistance;
			rage::u32 MinDistanceVariable;
			rage::u32 MaxDistanceVariable;
			rage::u32 CrossfadeOverrideVariable;
			rage::u32 FrontendLeftPan;
			rage::u32 FrontendRightPan;
			float PositionRelativePanDamping;
			rage::u32 PositionRelativePanDampingVariable;
			rage::u8 Mode;
		} SPU_ONLY(__attribute__((packed))); // struct CollapsingStereoSound
		
		// 
		// SimpleSound
		// 
		enum SimpleSoundFlagIds
		{
			FLAG_ID_SIMPLESOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_SIMPLESOUND_DISTANCEATTENUATION,
			FLAG_ID_SIMPLESOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_SIMPLESOUND_VIRTUALISEASGROUP,
			FLAG_ID_SIMPLESOUND_DYNAMICPREPARE,
			FLAG_ID_SIMPLESOUND_MUTEONUSERMUSIC,
			FLAG_ID_SIMPLESOUND_INVERTPHASE,
			FLAG_ID_SIMPLESOUND_MUTE,
			FLAG_ID_SIMPLESOUND_MAX,
		}; // enum SimpleSoundFlagIds
		
		struct SimpleSound
		{
			static const rage::u32 TYPE_ID = 12;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			SimpleSound() :
				WaveSlotIndex(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			struct tWaveRef
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} WaveRef; // struct tWaveRef
			
			rage::u8 WaveSlotIndex;
		} SPU_ONLY(__attribute__((packed))); // struct SimpleSound
		
		// 
		// MultitrackSound
		// 
		enum MultitrackSoundFlagIds
		{
			FLAG_ID_MULTITRACKSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_MULTITRACKSOUND_DISTANCEATTENUATION,
			FLAG_ID_MULTITRACKSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_MULTITRACKSOUND_VIRTUALISEASGROUP,
			FLAG_ID_MULTITRACKSOUND_DYNAMICPREPARE,
			FLAG_ID_MULTITRACKSOUND_MUTEONUSERMUSIC,
			FLAG_ID_MULTITRACKSOUND_INVERTPHASE,
			FLAG_ID_MULTITRACKSOUND_MUTE,
			FLAG_ID_MULTITRACKSOUND_STOPWHENCHILDSTOPS,
			FLAG_ID_MULTITRACKSOUND_SYNCCHILDREN,
			FLAG_ID_MULTITRACKSOUND_MAX,
		}; // enum MultitrackSoundFlagIds
		
		struct MultitrackSound
		{
			static const rage::u32 TYPE_ID = 13;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			static const rage::u8 MAX_SOUNDREFS = 8;
			rage::u8 numSoundRefs;
			struct tSoundRef
			{
				rage::audMetadataRef SoundId;
			} SoundRef[MAX_SOUNDREFS]; // struct tSoundRef
			
		} SPU_ONLY(__attribute__((packed))); // struct MultitrackSound
		
		// 
		// RandomizedSound
		// 
		enum RandomizedSoundFlagIds
		{
			FLAG_ID_RANDOMIZEDSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_RANDOMIZEDSOUND_DISTANCEATTENUATION,
			FLAG_ID_RANDOMIZEDSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_RANDOMIZEDSOUND_VIRTUALISEASGROUP,
			FLAG_ID_RANDOMIZEDSOUND_DYNAMICPREPARE,
			FLAG_ID_RANDOMIZEDSOUND_MUTEONUSERMUSIC,
			FLAG_ID_RANDOMIZEDSOUND_INVERTPHASE,
			FLAG_ID_RANDOMIZEDSOUND_MUTE,
			FLAG_ID_RANDOMIZEDSOUND_ORDEREDPLAYLIST,
			FLAG_ID_RANDOMIZEDSOUND_MAX,
		}; // enum RandomizedSoundFlagIds
		
		struct RandomizedSound
		{
			static const rage::u32 TYPE_ID = 14;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			RandomizedSound() :
				HistoryIndex(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u8 HistoryIndex;
			rage::u8 numHistorySpaceElems;
			rage::s8 HistorySpace[254];
			
			static const rage::u8 MAX_VARIATIONS = 254;
			rage::u8 numVariations;
			struct tVariations
			{
				rage::audMetadataRef Variation;
				float Weight;
			} Variations[MAX_VARIATIONS]; // struct tVariations
			
		} SPU_ONLY(__attribute__((packed))); // struct RandomizedSound
		
		// 
		// EnvironmentSound
		// 
		enum EnvironmentSoundFlagIds
		{
			FLAG_ID_ENVIRONMENTSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_ENVIRONMENTSOUND_DISTANCEATTENUATION,
			FLAG_ID_ENVIRONMENTSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_ENVIRONMENTSOUND_VIRTUALISEASGROUP,
			FLAG_ID_ENVIRONMENTSOUND_DYNAMICPREPARE,
			FLAG_ID_ENVIRONMENTSOUND_MUTEONUSERMUSIC,
			FLAG_ID_ENVIRONMENTSOUND_INVERTPHASE,
			FLAG_ID_ENVIRONMENTSOUND_MUTE,
			FLAG_ID_ENVIRONMENTSOUND_MAX,
		}; // enum EnvironmentSoundFlagIds
		
		struct EnvironmentSound
		{
			static const rage::u32 TYPE_ID = 15;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			EnvironmentSound() :
				ChannelId(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u8 ChannelId;
		} SPU_ONLY(__attribute__((packed))); // struct EnvironmentSound
		
		// 
		// DynamicEntitySound
		// 
		enum DynamicEntitySoundFlagIds
		{
			FLAG_ID_DYNAMICENTITYSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_DYNAMICENTITYSOUND_DISTANCEATTENUATION,
			FLAG_ID_DYNAMICENTITYSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_DYNAMICENTITYSOUND_VIRTUALISEASGROUP,
			FLAG_ID_DYNAMICENTITYSOUND_DYNAMICPREPARE,
			FLAG_ID_DYNAMICENTITYSOUND_MUTEONUSERMUSIC,
			FLAG_ID_DYNAMICENTITYSOUND_INVERTPHASE,
			FLAG_ID_DYNAMICENTITYSOUND_MUTE,
			FLAG_ID_DYNAMICENTITYSOUND_MAX,
		}; // enum DynamicEntitySoundFlagIds
		
		struct DynamicEntitySound
		{
			static const rage::u32 TYPE_ID = 16;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			static const rage::u8 MAX_OBJECTREFS = 8;
			rage::u8 numObjectRefs;
			struct tObjectRefs
			{
				rage::u32 GameObjectHash;
			} ObjectRefs[MAX_OBJECTREFS]; // struct tObjectRefs
			
		} SPU_ONLY(__attribute__((packed))); // struct DynamicEntitySound
		
		// 
		// SequentialOverlapSound
		// 
		enum SequentialOverlapSoundFlagIds
		{
			FLAG_ID_SEQUENTIALOVERLAPSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_DISTANCEATTENUATION,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_VIRTUALISEASGROUP,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_DYNAMICPREPARE,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_MUTEONUSERMUSIC,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_INVERTPHASE,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_MUTE,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_LOOP,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_RELEASECHILDREN,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_DELAYTIMEISPERCENTAGE,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_DELAYTIMEISREMAINING,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_TRIGGERONCHILDRELEASE,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_SPLICEMODE,
			FLAG_ID_SEQUENTIALOVERLAPSOUND_MAX,
		}; // enum SequentialOverlapSoundFlagIds
		
		struct SequentialOverlapSound
		{
			static const rage::u32 TYPE_ID = 17;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			SequentialOverlapSound() :
				DelayTime(0),
				DelayTimeVariable(0U),
				SequenceDirection(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u16 DelayTime;
			rage::u32 DelayTimeVariable;
			rage::u32 SequenceDirection;
			
			static const rage::u8 MAX_CHILDSOUNDS = 254;
			rage::u8 numChildSounds;
			struct tChildSound
			{
				rage::audMetadataRef SoundRef;
			} ChildSound[MAX_CHILDSOUNDS]; // struct tChildSound
			
		} SPU_ONLY(__attribute__((packed))); // struct SequentialOverlapSound
		
		// 
		// ModularSynthSound
		// 
		enum ModularSynthSoundFlagIds
		{
			FLAG_ID_MODULARSYNTHSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_MODULARSYNTHSOUND_DISTANCEATTENUATION,
			FLAG_ID_MODULARSYNTHSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_MODULARSYNTHSOUND_VIRTUALISEASGROUP,
			FLAG_ID_MODULARSYNTHSOUND_DYNAMICPREPARE,
			FLAG_ID_MODULARSYNTHSOUND_MUTEONUSERMUSIC,
			FLAG_ID_MODULARSYNTHSOUND_INVERTPHASE,
			FLAG_ID_MODULARSYNTHSOUND_MUTE,
			FLAG_ID_MODULARSYNTHSOUND_RELEASETHROUGHSYNTH,
			FLAG_ID_MODULARSYNTHSOUND_MAX,
		}; // enum ModularSynthSoundFlagIds
		
		struct ModularSynthSound
		{
			static const rage::u32 TYPE_ID = 18;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			ModularSynthSound() :
				SynthDef(0U),
				Preset(0U),
				PlayBackTimeLimit(-1.0f),
				VirtualisationMode(kVirtualisationNormal),
				pad1(0),
				pad0(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 SynthDef;
			rage::u32 Preset;
			float PlayBackTimeLimit;
			rage::u8 VirtualisationMode;
			rage::u8 pad1;
			rage::u16 pad0;
			
			static const rage::u32 MAX_ENVIRONMENTSOUNDS = 4;
			rage::u32 numEnvironmentSounds;
			struct tEnvironmentSound
			{
				rage::audMetadataRef SoundRef;
			} EnvironmentSound[MAX_ENVIRONMENTSOUNDS]; // struct tEnvironmentSound
			
			
			static const rage::u32 MAX_EXPOSEDVARIABLES = 64;
			rage::u32 numExposedVariables;
			struct tExposedVariable
			{
				rage::u32 SynthKey;
				rage::u32 Variable;
				float Value;
			} ExposedVariable[MAX_EXPOSEDVARIABLES]; // struct tExposedVariable
			
		} SPU_ONLY(__attribute__((packed))); // struct ModularSynthSound
		
		// 
		// GranularSound
		// 
		enum GranularSoundFlagIds
		{
			FLAG_ID_GRANULARSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_GRANULARSOUND_DISTANCEATTENUATION,
			FLAG_ID_GRANULARSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_GRANULARSOUND_VIRTUALISEASGROUP,
			FLAG_ID_GRANULARSOUND_DYNAMICPREPARE,
			FLAG_ID_GRANULARSOUND_MUTEONUSERMUSIC,
			FLAG_ID_GRANULARSOUND_INVERTPHASE,
			FLAG_ID_GRANULARSOUND_MUTE,
			FLAG_ID_GRANULARSOUND_LOOPRANDOMISATIONENABLED,
			FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL0,
			FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL1,
			FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL2,
			FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL3,
			FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL4,
			FLAG_ID_GRANULARSOUND_ALLOWLOOPGRAINCROSSOVERCHANNEL5,
			FLAG_ID_GRANULARSOUND_MAX,
		}; // enum GranularSoundFlagIds
		
		struct GranularSound
		{
			static const rage::u32 TYPE_ID = 19;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			GranularSound() :
				WaveSlotPtr(0U),
				LoopRandomisationChangeRate(0.01f),
				LoopRandomisationPitchFraction(0.05f),
				Channel0_Volume(0),
				Channel1_Volume(0),
				Channel2_Volume(0),
				Channel3_Volume(0),
				Channel4_Volume(0),
				Channel5_Volume(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 WaveSlotPtr;
			
			struct tChannel0
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} Channel0; // struct tChannel0
			
			
			struct tChannel1
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} Channel1; // struct tChannel1
			
			
			struct tChannel2
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} Channel2; // struct tChannel2
			
			
			struct tChannel3
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} Channel3; // struct tChannel3
			
			
			struct tChannel4
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} Channel4; // struct tChannel4
			
			
			struct tChannel5
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} Channel5; // struct tChannel5
			
			
			struct tChannelSettings0
			{
				rage::u8 OutputBuffer;
				rage::u8 GranularClockIndex;
				rage::u8 StretchToMinPitch0;
				rage::u8 StretchToMaxPitch0;
				float MaxLoopProportion;
			} ChannelSettings0; // struct tChannelSettings0
			
			
			struct tChannelSettings1
			{
				rage::u8 OutputBuffer;
				rage::u8 GranularClockIndex;
				rage::u8 StretchToMinPitch1;
				rage::u8 StretchToMaxPitch1;
				float MaxLoopProportion;
			} ChannelSettings1; // struct tChannelSettings1
			
			
			struct tChannelSettings2
			{
				rage::u8 OutputBuffer;
				rage::u8 GranularClockIndex;
				rage::u8 StretchToMinPitch2;
				rage::u8 StretchToMaxPitch2;
				float MaxLoopProportion;
			} ChannelSettings2; // struct tChannelSettings2
			
			
			struct tChannelSettings3
			{
				rage::u8 OutputBuffer;
				rage::u8 GranularClockIndex;
				rage::u8 StretchToMinPitch3;
				rage::u8 StretchToMaxPitch3;
				float MaxLoopProportion;
			} ChannelSettings3; // struct tChannelSettings3
			
			
			struct tChannelSettings4
			{
				rage::u8 OutputBuffer;
				rage::u8 GranularClockIndex;
				rage::u8 StretchToMinPitch4;
				rage::u8 StretchToMaxPitch4;
				float MaxLoopProportion;
			} ChannelSettings4; // struct tChannelSettings4
			
			
			struct tChannelSettings5
			{
				rage::u8 OutputBuffer;
				rage::u8 GranularClockIndex;
				rage::u8 StretchToMinPitch5;
				rage::u8 StretchToMaxPitch5;
				float MaxLoopProportion;
			} ChannelSettings5; // struct tChannelSettings5
			
			float LoopRandomisationChangeRate;
			float LoopRandomisationPitchFraction;
			rage::s16 Channel0_Volume;
			rage::s16 Channel1_Volume;
			rage::s16 Channel2_Volume;
			rage::s16 Channel3_Volume;
			rage::s16 Channel4_Volume;
			rage::s16 Channel5_Volume;
			rage::audMetadataRef ParentSound;
			
			static const rage::u8 MAX_GRANULARCLOCKS = 2;
			rage::u8 numGranularClocks;
			struct tGranularClock
			{
				float MinPitch;
				float MaxPitch;
			} GranularClock[MAX_GRANULARCLOCKS]; // struct tGranularClock
			
		} SPU_ONLY(__attribute__((packed))); // struct GranularSound
		
		// 
		// DirectionalSound
		// 
		enum DirectionalSoundFlagIds
		{
			FLAG_ID_DIRECTIONALSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_DIRECTIONALSOUND_DISTANCEATTENUATION,
			FLAG_ID_DIRECTIONALSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_DIRECTIONALSOUND_VIRTUALISEASGROUP,
			FLAG_ID_DIRECTIONALSOUND_DYNAMICPREPARE,
			FLAG_ID_DIRECTIONALSOUND_MUTEONUSERMUSIC,
			FLAG_ID_DIRECTIONALSOUND_INVERTPHASE,
			FLAG_ID_DIRECTIONALSOUND_MUTE,
			FLAG_ID_DIRECTIONALSOUND_MAX,
		}; // enum DirectionalSoundFlagIds
		
		struct DirectionalSound
		{
			static const rage::u32 TYPE_ID = 20;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			DirectionalSound() :
				InnerAngle(65.0f),
				OuterAngle(135.0f),
				RearAttenuation(0.0f),
				YawAngle(0.0f),
				PitchAngle(0.0f)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			float InnerAngle;
			float OuterAngle;
			float RearAttenuation;
			float YawAngle;
			float PitchAngle;
		} SPU_ONLY(__attribute__((packed))); // struct DirectionalSound
		
		// 
		// KineticSound
		// 
		enum KineticSoundFlagIds
		{
			FLAG_ID_KINETICSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_KINETICSOUND_DISTANCEATTENUATION,
			FLAG_ID_KINETICSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_KINETICSOUND_VIRTUALISEASGROUP,
			FLAG_ID_KINETICSOUND_DYNAMICPREPARE,
			FLAG_ID_KINETICSOUND_MUTEONUSERMUSIC,
			FLAG_ID_KINETICSOUND_INVERTPHASE,
			FLAG_ID_KINETICSOUND_MUTE,
			FLAG_ID_KINETICSOUND_VELOCITYORIENTATION,
			FLAG_ID_KINETICSOUND_MAX,
		}; // enum KineticSoundFlagIds
		
		struct KineticSound
		{
			static const rage::u32 TYPE_ID = 21;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			KineticSound() :
				Mass(100.0f),
				YawAngle(0.0f),
				PitchAngle(0.0f)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			float Mass;
			float YawAngle;
			float PitchAngle;
		} SPU_ONLY(__attribute__((packed))); // struct KineticSound
		
		// 
		// SwitchSound
		// 
		enum SwitchSoundFlagIds
		{
			FLAG_ID_SWITCHSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_SWITCHSOUND_DISTANCEATTENUATION,
			FLAG_ID_SWITCHSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_SWITCHSOUND_VIRTUALISEASGROUP,
			FLAG_ID_SWITCHSOUND_DYNAMICPREPARE,
			FLAG_ID_SWITCHSOUND_MUTEONUSERMUSIC,
			FLAG_ID_SWITCHSOUND_INVERTPHASE,
			FLAG_ID_SWITCHSOUND_MUTE,
			FLAG_ID_SWITCHSOUND_SCALEINPUT,
			FLAG_ID_SWITCHSOUND_MAX,
		}; // enum SwitchSoundFlagIds
		
		struct SwitchSound
		{
			static const rage::u32 TYPE_ID = 22;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			SwitchSound() :
				Variable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 Variable;
			
			static const rage::u8 MAX_SOUNDREFS = 32;
			rage::u8 numSoundRefs;
			struct tSoundRef
			{
				rage::audMetadataRef SoundId;
			} SoundRef[MAX_SOUNDREFS]; // struct tSoundRef
			
		} SPU_ONLY(__attribute__((packed))); // struct SwitchSound
		
		// 
		// VariableCurveSound
		// 
		enum VariableCurveSoundFlagIds
		{
			FLAG_ID_VARIABLECURVESOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_VARIABLECURVESOUND_DISTANCEATTENUATION,
			FLAG_ID_VARIABLECURVESOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_VARIABLECURVESOUND_VIRTUALISEASGROUP,
			FLAG_ID_VARIABLECURVESOUND_DYNAMICPREPARE,
			FLAG_ID_VARIABLECURVESOUND_MUTEONUSERMUSIC,
			FLAG_ID_VARIABLECURVESOUND_INVERTPHASE,
			FLAG_ID_VARIABLECURVESOUND_MUTE,
			FLAG_ID_VARIABLECURVESOUND_MAX,
		}; // enum VariableCurveSoundFlagIds
		
		struct VariableCurveSound
		{
			static const rage::u32 TYPE_ID = 23;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			VariableCurveSound() :
				InputVariable(0U),
				OutputVariable(0U),
				Curve(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			rage::u32 InputVariable;
			rage::u32 OutputVariable;
			rage::u32 Curve;
		} SPU_ONLY(__attribute__((packed))); // struct VariableCurveSound
		
		// 
		// VariablePrintValueSound
		// 
		enum VariablePrintValueSoundFlagIds
		{
			FLAG_ID_VARIABLEPRINTVALUESOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_VARIABLEPRINTVALUESOUND_DISTANCEATTENUATION,
			FLAG_ID_VARIABLEPRINTVALUESOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_VARIABLEPRINTVALUESOUND_VIRTUALISEASGROUP,
			FLAG_ID_VARIABLEPRINTVALUESOUND_DYNAMICPREPARE,
			FLAG_ID_VARIABLEPRINTVALUESOUND_MUTEONUSERMUSIC,
			FLAG_ID_VARIABLEPRINTVALUESOUND_INVERTPHASE,
			FLAG_ID_VARIABLEPRINTVALUESOUND_MUTE,
			FLAG_ID_VARIABLEPRINTVALUESOUND_MAX,
		}; // enum VariablePrintValueSoundFlagIds
		
		struct VariablePrintValueSound
		{
			static const rage::u32 TYPE_ID = 24;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			VariablePrintValueSound() :
				Variable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 Variable;
			rage::u8 MessageLen;
			char Message[255];
		} SPU_ONLY(__attribute__((packed))); // struct VariablePrintValueSound
		
		// 
		// VariableBlockSound
		// 
		enum VariableBlockSoundFlagIds
		{
			FLAG_ID_VARIABLEBLOCKSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_VARIABLEBLOCKSOUND_DISTANCEATTENUATION,
			FLAG_ID_VARIABLEBLOCKSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_VARIABLEBLOCKSOUND_VIRTUALISEASGROUP,
			FLAG_ID_VARIABLEBLOCKSOUND_DYNAMICPREPARE,
			FLAG_ID_VARIABLEBLOCKSOUND_MUTEONUSERMUSIC,
			FLAG_ID_VARIABLEBLOCKSOUND_INVERTPHASE,
			FLAG_ID_VARIABLEBLOCKSOUND_MUTE,
			FLAG_ID_VARIABLEBLOCKSOUND_MAX,
		}; // enum VariableBlockSoundFlagIds
		
		struct VariableBlockSound
		{
			static const rage::u32 TYPE_ID = 25;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			
			static const rage::u8 MAX_VARIABLES = 20;
			rage::u8 numVariables;
			struct tVariable
			{
				rage::u32 VariableRef;
				float VariableValue;
				float InitialVariance;
				rage::u8 Usage;
			} Variable[MAX_VARIABLES]; // struct tVariable
			
		} SPU_ONLY(__attribute__((packed))); // struct VariableBlockSound
		
		// 
		// IfSound
		// 
		enum IfSoundFlagIds
		{
			FLAG_ID_IFSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_IFSOUND_DISTANCEATTENUATION,
			FLAG_ID_IFSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_IFSOUND_VIRTUALISEASGROUP,
			FLAG_ID_IFSOUND_DYNAMICPREPARE,
			FLAG_ID_IFSOUND_MUTEONUSERMUSIC,
			FLAG_ID_IFSOUND_INVERTPHASE,
			FLAG_ID_IFSOUND_MUTE,
			FLAG_ID_IFSOUND_CHOOSEONINIT,
			FLAG_ID_IFSOUND_MAX,
		}; // enum IfSoundFlagIds
		
		struct IfSound
		{
			static const rage::u32 TYPE_ID = 26;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			IfSound() :
				LeftVariable(0U),
				ConditionType(IF_CONDITION_EQUAL_TO)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef TrueSoundRef;
			rage::audMetadataRef FalseSoundRef;
			rage::u32 LeftVariable;
			rage::u8 ConditionType;
			
			struct tRHS
			{
				float RightValue;
				rage::u32 RightVariable;
			} RHS; // struct tRHS
			
		} SPU_ONLY(__attribute__((packed))); // struct IfSound
		
		// 
		// MathOperationSound
		// 
		enum MathOperationSoundFlagIds
		{
			FLAG_ID_MATHOPERATIONSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_MATHOPERATIONSOUND_DISTANCEATTENUATION,
			FLAG_ID_MATHOPERATIONSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_MATHOPERATIONSOUND_VIRTUALISEASGROUP,
			FLAG_ID_MATHOPERATIONSOUND_DYNAMICPREPARE,
			FLAG_ID_MATHOPERATIONSOUND_MUTEONUSERMUSIC,
			FLAG_ID_MATHOPERATIONSOUND_INVERTPHASE,
			FLAG_ID_MATHOPERATIONSOUND_MUTE,
			FLAG_ID_MATHOPERATIONSOUND_ONLYRUNONCE,
			FLAG_ID_MATHOPERATIONSOUND_MAX,
		}; // enum MathOperationSoundFlagIds
		
		struct MathOperationSound
		{
			static const rage::u32 TYPE_ID = 27;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			
			static const rage::u8 MAX_OPERATIONS = 10;
			rage::u8 numOperations;
			struct tOperation
			{
				rage::u8 Operation;
				
				struct tparam1
				{
					float Value;
					rage::u32 Variable;
				} param1; // struct tparam1
				
				
				struct tparam2
				{
					float Value;
					rage::u32 Variable;
				} param2; // struct tparam2
				
				
				struct tparam3
				{
					float Value;
					rage::u32 Variable;
				} param3; // struct tparam3
				
				rage::u32 Result;
			} Operation[MAX_OPERATIONS]; // struct tOperation
			
		} SPU_ONLY(__attribute__((packed))); // struct MathOperationSound
		
		// 
		// ParameterTransformSound
		// 
		enum ParameterTransformSoundFlagIds
		{
			FLAG_ID_PARAMETERTRANSFORMSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_PARAMETERTRANSFORMSOUND_DISTANCEATTENUATION,
			FLAG_ID_PARAMETERTRANSFORMSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_PARAMETERTRANSFORMSOUND_VIRTUALISEASGROUP,
			FLAG_ID_PARAMETERTRANSFORMSOUND_DYNAMICPREPARE,
			FLAG_ID_PARAMETERTRANSFORMSOUND_MUTEONUSERMUSIC,
			FLAG_ID_PARAMETERTRANSFORMSOUND_INVERTPHASE,
			FLAG_ID_PARAMETERTRANSFORMSOUND_MUTE,
			FLAG_ID_PARAMETERTRANSFORMSOUND_MAX,
		}; // enum ParameterTransformSoundFlagIds
		
		struct ParameterTransformSound
		{
			static const rage::u32 TYPE_ID = 28;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			
			static const rage::u32 MAX_PARAMETERTRANSFORMS = 16;
			rage::u32 numParameterTransforms;
			struct tParameterTransform
			{
				rage::u32 InputVariable;
				
				struct tInputRange
				{
					float Min;
					float Max;
				} InputRange; // struct tInputRange
				
				
				static const rage::u32 MAX_OUTPUTS = 4;
				rage::u32 numOutputs;
				struct tOutput
				{
					float InputSmoothRate;
					rage::u8 Destination;
					rage::u8 padding0;
					rage::u8 padding1;
					rage::u8 padding2;
					rage::u32 OutputVariable;
					
					struct tOutputRange
					{
						float Min;
						float Max;
					} OutputRange; // struct tOutputRange
					
					
					static const rage::u32 MAX_TRANSFORMPOINTS = 16;
					rage::u32 numTransformPoints;
					struct tTransformPoints
					{
						float x;
						float y;
					} TransformPoints[MAX_TRANSFORMPOINTS]; // struct tTransformPoints
					
				} Output[MAX_OUTPUTS]; // struct tOutput
				
			} ParameterTransform[MAX_PARAMETERTRANSFORMS]; // struct tParameterTransform
			
		} SPU_ONLY(__attribute__((packed))); // struct ParameterTransformSound
		
		// 
		// FluctuatorSound
		// 
		enum FluctuatorSoundFlagIds
		{
			FLAG_ID_FLUCTUATORSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_FLUCTUATORSOUND_DISTANCEATTENUATION,
			FLAG_ID_FLUCTUATORSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_FLUCTUATORSOUND_VIRTUALISEASGROUP,
			FLAG_ID_FLUCTUATORSOUND_DYNAMICPREPARE,
			FLAG_ID_FLUCTUATORSOUND_MUTEONUSERMUSIC,
			FLAG_ID_FLUCTUATORSOUND_INVERTPHASE,
			FLAG_ID_FLUCTUATORSOUND_MUTE,
			FLAG_ID_FLUCTUATORSOUND_MAX,
		}; // enum FluctuatorSoundFlagIds
		
		struct FluctuatorSound
		{
			static const rage::u32 TYPE_ID = 29;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			
			static const rage::u32 MAX_FLUCTUATORS = 4;
			rage::u32 numFluctuators;
			struct tFluctuator
			{
				rage::u8 Mode;
				rage::u8 Destination;
				rage::u32 OutputVariable;
				float IncreaseRate;
				float DecreaseRate;
				float BandOneMinimum;
				float BandOneMaximum;
				float BandTwoMinimum;
				float BandTwoMaximum;
				float IntraBandFlipProbabilty;
				float InterBandFlipProbabilty;
				rage::u32 MinSwitchTime;
				rage::u32 MaxSwitchTime;
				float InitialValue;
			} Fluctuator[MAX_FLUCTUATORS]; // struct tFluctuator
			
		} SPU_ONLY(__attribute__((packed))); // struct FluctuatorSound
		
		// 
		// AutomationSound
		// 
		enum AutomationSoundFlagIds
		{
			FLAG_ID_AUTOMATIONSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_AUTOMATIONSOUND_DISTANCEATTENUATION,
			FLAG_ID_AUTOMATIONSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_AUTOMATIONSOUND_VIRTUALISEASGROUP,
			FLAG_ID_AUTOMATIONSOUND_DYNAMICPREPARE,
			FLAG_ID_AUTOMATIONSOUND_MUTEONUSERMUSIC,
			FLAG_ID_AUTOMATIONSOUND_INVERTPHASE,
			FLAG_ID_AUTOMATIONSOUND_MUTE,
			FLAG_ID_AUTOMATIONSOUND_LOOPSEQUENCE,
			FLAG_ID_AUTOMATIONSOUND_MAX,
		}; // enum AutomationSoundFlagIds
		
		struct AutomationSound
		{
			static const rage::u32 TYPE_ID = 30;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			AutomationSound() :
				PlaybackRate(1.0f),
				PlaybackRateVariance(0.0f),
				PlaybackRateVariable(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::audMetadataRef SoundRef;
			float PlaybackRate;
			float PlaybackRateVariance;
			rage::u32 PlaybackRateVariable;
			rage::audMetadataRef NoteMap;
			
			struct tDataRef
			{
				rage::u32 BankName;
				rage::u32 WaveName;
			} DataRef; // struct tDataRef
			
			
			static const rage::u32 MAX_VARIABLEOUTPUTS = 8;
			rage::u32 numVariableOutputs;
			struct tVariableOutputs
			{
				rage::u32 ChannelId;
				rage::u32 OutputVariable;
			} VariableOutputs[MAX_VARIABLEOUTPUTS]; // struct tVariableOutputs
			
		} SPU_ONLY(__attribute__((packed))); // struct AutomationSound
		
		// 
		// ExternalStreamSound
		// 
		enum ExternalStreamSoundFlagIds
		{
			FLAG_ID_EXTERNALSTREAMSOUND_STARTOFFSETPERCENTAGE = 0,
			FLAG_ID_EXTERNALSTREAMSOUND_DISTANCEATTENUATION,
			FLAG_ID_EXTERNALSTREAMSOUND_ENVIRONMENTALEFFECTS,
			FLAG_ID_EXTERNALSTREAMSOUND_VIRTUALISEASGROUP,
			FLAG_ID_EXTERNALSTREAMSOUND_DYNAMICPREPARE,
			FLAG_ID_EXTERNALSTREAMSOUND_MUTEONUSERMUSIC,
			FLAG_ID_EXTERNALSTREAMSOUND_INVERTPHASE,
			FLAG_ID_EXTERNALSTREAMSOUND_MUTE,
			FLAG_ID_EXTERNALSTREAMSOUND_MAX,
		}; // enum ExternalStreamSoundFlagIds
		
		struct ExternalStreamSound
		{
			static const rage::u32 TYPE_ID = 31;
			static const rage::u32 BASE_TYPE_ID = Sound::TYPE_ID;
			
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			
			static const rage::u8 MAX_ENVIRONMENTSOUNDS = 4;
			rage::u8 numEnvironmentSounds;
			struct tEnvironmentSound
			{
				rage::audMetadataRef SoundRef;
			} EnvironmentSound[MAX_ENVIRONMENTSOUNDS]; // struct tEnvironmentSound
			
		} SPU_ONLY(__attribute__((packed))); // struct ExternalStreamSound
		
		// 
		// SoundSet
		// 
		enum SoundSetFlagIds
		{
			FLAG_ID_SOUNDSET_MAX = 0,
		}; // enum SoundSetFlagIds
		
		struct SoundSet
		{
			static const rage::u32 TYPE_ID = 32;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			SoundSet() :
				ClassId(0xFF),
				DEBUGAUDIO_ONLY(NameTableOffset(0XFFFFFF),)
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			#if __USEDEBUGAUDIO
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			#else
			rage::u8 ClassId;
			#endif // __USEDEBUGAUDIO
			rage::u32 Flags;
			
			
			static const rage::u32 MAX_SOUNDS = 1000;
			rage::u32 numSounds;
			struct tSounds
			{
				rage::u32 Name;
				rage::audMetadataRef Sound;
			} Sounds[MAX_SOUNDS]; // struct tSounds
			
		} SPU_ONLY(__attribute__((packed))); // struct SoundSet
		
		// 
		// NoteMap
		// 
		enum NoteMapFlagIds
		{
			FLAG_ID_NOTEMAP_MAX = 0,
		}; // enum NoteMapFlagIds
		
		struct NoteMap
		{
			static const rage::u32 TYPE_ID = 33;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			NoteMap() :
				ClassId(0xFF),
				DEBUGAUDIO_ONLY(NameTableOffset(0XFFFFFF),)
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			#if __USEDEBUGAUDIO
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			#else
			rage::u8 ClassId;
			#endif // __USEDEBUGAUDIO
			rage::u32 Flags;
			
			
			static const rage::u8 MAX_RANGES = 128;
			rage::u8 numRanges;
			struct tRange
			{
				rage::u8 FirstNoteId;
				rage::u8 LastNoteId;
				rage::u8 Mode;
				rage::audMetadataRef SoundRef;
			} Range[MAX_RANGES]; // struct tRange
			
		} SPU_ONLY(__attribute__((packed))); // struct NoteMap
		
		// 
		// SoundSetList
		// 
		enum SoundSetListFlagIds
		{
			FLAG_ID_SOUNDSETLIST_MAX = 0,
		}; // enum SoundSetListFlagIds
		
		struct SoundSetList
		{
			static const rage::u32 TYPE_ID = 34;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			SoundSetList() :
				ClassId(0xFF),
				DEBUGAUDIO_ONLY(NameTableOffset(0XFFFFFF),)
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			#if __USEDEBUGAUDIO
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			#else
			rage::u8 ClassId;
			#endif // __USEDEBUGAUDIO
			rage::u32 Flags;
			
			
			static const rage::u32 MAX_SOUNDSETS = 65535;
			rage::u32 numSoundSets;
			struct tSoundSet
			{
				rage::audMetadataRef SoundSetHash;
			} SoundSet[MAX_SOUNDSETS]; // struct tSoundSet
			
		} SPU_ONLY(__attribute__((packed))); // struct SoundSetList
		
		// 
		// SoundHashList
		// 
		enum SoundHashListFlagIds
		{
			FLAG_ID_SOUNDHASHLIST_MAX = 0,
		}; // enum SoundHashListFlagIds
		
		struct SoundHashList
		{
			static const rage::u32 TYPE_ID = 35;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			SoundHashList() :
				ClassId(0xFF),
				DEBUGAUDIO_ONLY(NameTableOffset(0XFFFFFF),)
				Flags(0xAAAAAAAA),
				CurrentSoundIdx(0)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			#if __USEDEBUGAUDIO
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			#else
			rage::u8 ClassId;
			#endif // __USEDEBUGAUDIO
			rage::u32 Flags;
			
			rage::u16 CurrentSoundIdx;
			
			static const rage::u32 MAX_SOUNDHASHES = 65535;
			rage::u32 numSoundHashes;
			struct tSoundHashes
			{
				rage::u32 SoundHash;
			} SoundHashes[MAX_SOUNDHASHES]; // struct tSoundHashes
			
		} SPU_ONLY(__attribute__((packed))); // struct SoundHashList
		
	// enable struct alignment
	#if !__SPU
	#pragma pack(pop, r1)
	#endif // !__SPU
} // namespace rage
#endif // AUD_SOUNDDEFS_H
