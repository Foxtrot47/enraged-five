//
// audiosoundtypes/loopingsound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "loopingsound.h"
#include "audioengine/categorymanager.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/syncsource.h"

namespace rage 
{

AUD_IMPLEMENT_STATIC_WRAPPERS(audLoopingSound);

audLoopingSound::~audLoopingSound()
{
	for(s32 i = 0; i < 2; i++)
	{
		audMixerSyncManager::Get()->Release(m_SyncIds[i].masterSyncId);
		audMixerSyncManager::Get()->Release(m_SyncIds[i].slaveSyncId);

		m_SyncIds[i].masterSyncId = m_SyncIds[i].slaveSyncId = audMixerSyncManager::InvalidId;
		
		if(HasChildSound(i))
		{
			DeleteChild(i);
		}
	}
}

#if !__SPU

audLoopingSound::audLoopingSound()
{
	for(s32 i = 0; i < 2; i++)
	{
		m_SyncIds[i].masterSyncId = m_SyncIds[i].slaveSyncId = audMixerSyncManager::InvalidId;
		m_IsPreparing[i] = false;
	}
	m_CurrentLoopCount = 0;

	m_SpliceMode = false;
}

bool audLoopingSound::Init(const void *Metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(Metadata, initParams, scratchInitParams))
	{
		return false;
	}

	m_ScratchInitParams = *scratchInitParams;

	SetHasDynamicChildren();

	LoopingSound *loopingSoundData = (LoopingSound*)GetMetadata();

	m_LoopingCountVariable = _FindVariableUpHierarchy(loopingSoundData->LoopCountVariable);
	if(m_LoopingCountVariable)
	{
		m_LoopCount = (s32)(AUD_GET_VARIABLE(m_LoopingCountVariable));
	}
	else
	{
		m_LoopCount = loopingSoundData->LoopCount;
	} 
	if(m_LoopCount != -1)
	{
		m_LoopCount = ApplyVariance(m_LoopCount, loopingSoundData->LoopCountVariance, 1024);
	}
	m_LoopPoint = loopingSoundData->LoopPoint;
	m_SoundRef = loopingSoundData->SoundRef;

	m_SpliceMode = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_LOOPINGSOUND_ENABLESPLICING) == AUD_TRISTATE_TRUE);

	SetChildSound(0,SOUNDFACTORY.GetChildInstance(m_SoundRef, this, initParams, scratchInitParams, false));
	if(!GetChildSound(0))
	{
		return false;
	}

	m_Index = 0;

	return true;
}

#endif

audPrepareState audLoopingSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	audSound *childSound = GetChildSound(0);
	SoundAssert(childSound);

	if(childSound)
	{
		childSound->SetStartOffset((s32)GetPlaybackStartOffset());
		return childSound->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());		
	}
	else
	{
		return AUD_PREPARE_FAILED;
	}
}

void audLoopingSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	audSound *childSound = GetChildSound(0);
	SoundAssert(childSound);
	s32 playbackStartOffset = (s32)GetPlaybackStartOffset();
	
	childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));

	audSoundSyncSetMasterId setSyncMasterId;
	audSoundSyncSetSlaveId setSyncSlaveId;

	if(m_SpliceMode)
	{
		Assign(m_SyncIds[m_Index].masterSyncId, audMixerSyncManager::Get()->Allocate());
		const u32 currentSlaveId = GetCurrentSyncSlaveId();
		audMixerSyncManager::Get()->AddOwnerRef(currentSlaveId);
		Assign(m_SyncIds[m_Index].slaveSyncId, currentSlaveId);

		setSyncMasterId.Set(m_SyncIds[m_Index].masterSyncId);
		setSyncSlaveId.Set(m_SyncIds[m_Index].slaveSyncId);
	}

	m_IsPreparing[0] = false;
	if(m_LoopingCountVariable)
	{
		m_LoopCount = (s32)(AUD_GET_VARIABLE(m_LoopingCountVariable));
	}
	if (m_LoopCount == -1)
	{
		childSound->SetStartOffset(playbackStartOffset);
	}
	else
	{
		bool isChildLooping = false;
		s32 childLength = GetChildSound(0)->ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
		if (childLength == kSoundLengthUnknown || isChildLooping)
		{
			// We have no idea, so just offset into the child sound by the requested amount
			childSound->SetStartOffset(playbackStartOffset);
		}
		else
		{
			if (childLength != 0)
			{
				s32 offsetIntoSection = playbackStartOffset % childLength;
				s32 loopSection = playbackStartOffset / childLength; // it's an int, so it'll be clipped
				m_CurrentLoopCount = loopSection; // this'll just never play if we're past the end of it
				childSound->SetStartOffset(offsetIntoSection);
			}
		}
	}

	AudioPlayChild(timeInMs, m_Index, combineBuffer);
}

void audLoopingSound::AudioPlayChild(u32 timeInMs, const u32 index, audSoundCombineBuffer &combineBuffer)
{
	if(HasChildSound(index))
	{
		GetChildSound(index)->_ManagedAudioPlay(timeInMs, combineBuffer);	
	}
	m_CurrentLoopCount++;
}

bool audLoopingSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	bool stillPlaying = false;
	bool playedThisFrame = false;

	const u32 nextIndex = (m_Index+1)&1;

	if(!HasChildSound(m_Index))
	{
		return false;
	}

	if(!m_SpliceMode && m_IsPreparing[m_Index])
	{
		audPrepareState state = GetChildSound(m_Index)->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority());
		if(state == AUD_PREPARED)
		{
			AudioPlayChild(timeInMs, m_Index, combineBuffer);
			m_IsPreparing[m_Index] = false;
			playedThisFrame = true;
		}
	}

	if(!HasChildSound(nextIndex))
	{
		if(m_SpliceMode)
		{
			// Create next child as early as possible
			RequestChildSoundFromOffset(m_SoundRef, nextIndex, &m_ScratchInitParams);
			m_IsPreparing[nextIndex] = true;
		}
	}
	else if(m_IsPreparing[nextIndex])
	{
		audPrepareState state = GetChildSound(nextIndex)->_ManagedAudioPrepare(GetWaveSlot(), GetCanDynamicallyLoad(), false, GetWaveLoadPriority());
		if(state == AUD_PREPARED)
		{
			if(m_SpliceMode)
			{
				audSoundSyncSetSlaveId setSyncSlaveId;
				audSoundSyncSetMasterId setSyncMasterId;

				// We're about to overwrite this sync id, so ensure we've released it first
				audMixerSyncManager::Get()->Release(m_SyncIds[nextIndex].masterSyncId);
				audMixerSyncManager::Get()->Release(m_SyncIds[nextIndex].slaveSyncId);

				// Next child should slave to current sync ID and master (newly allocated) next sync id.
				Assign(m_SyncIds[nextIndex].masterSyncId, audMixerSyncManager::Get()->Allocate());
			
				audMixerSyncManager::Get()->AddOwnerRef(m_SyncIds[m_Index].masterSyncId);
				Assign(m_SyncIds[nextIndex].slaveSyncId, m_SyncIds[m_Index].masterSyncId);
							
				setSyncMasterId.Set(m_SyncIds[nextIndex].masterSyncId);
				setSyncSlaveId.Set(m_SyncIds[nextIndex].slaveSyncId);
	
				AudioPlayChild(timeInMs, nextIndex, combineBuffer);
			}

			m_IsPreparing[nextIndex] = false;
		}
		else if(state == AUD_PREPARE_FAILED)
		{
			// prepare on one of our children has failed - for now just bail out.
			// should probably just move onto the next child?
			return false;
		}
	}
	else if(m_SpliceMode)
	{
		audSoundSyncSetSlaveId setSyncSlaveId;
		audSoundSyncSetMasterId setSyncMasterId;

		// Next child should slave to current sync ID and master next sync id.
		setSyncMasterId.Set(m_SyncIds[nextIndex].masterSyncId);
		setSyncSlaveId.Set(m_SyncIds[nextIndex].slaveSyncId);

		// Need to update our next child so that volume/pitch etc is up to date when the voice is triggered
		audSound *nextChild = GetChildSound(nextIndex);
		if(nextChild->GetPlayState() == AUD_SOUND_PLAYING)
		{
			nextChild->_ManagedAudioUpdate(timeInMs, combineBuffer);			
		}
	}

	audSoundSyncSetMasterId setSyncMasterId;
	audSoundSyncSetSlaveId setSyncSlaveId;

	if(m_SpliceMode)
	{
		setSyncMasterId.Set(m_SyncIds[m_Index].masterSyncId);
		setSyncSlaveId.Set(m_SyncIds[m_Index].slaveSyncId);
	}

	audSound *currentChild = GetChildSound(m_Index);
	if(currentChild && !playedThisFrame)
	{
		if (currentChild->GetPlayState() == AUD_SOUND_PLAYING)
		{
			if (currentChild->_ManagedAudioUpdate(timeInMs, combineBuffer))
			{
				stillPlaying = true;
			}
		}
		else if(currentChild->GetPlayState() == AUD_SOUND_PREPARING && m_IsPreparing[m_Index])
		{
			stillPlaying = true;
		}
	}

	if (!stillPlaying && !playedThisFrame)
	{
		if(m_LoopingCountVariable)
		{
			m_LoopCount = (s32)(AUD_GET_VARIABLE(m_LoopingCountVariable));
		}
		if((m_LoopCount != -1 && m_CurrentLoopCount >= m_LoopCount) ||
			IsWaitingForChildrenToRelease())
		{
			return false;
		}
		
		if(!m_IsPreparing[nextIndex])
		{
			// Ready to move on to the next child sound.
			if(currentChild)
			{
				DeleteChild(m_Index);
			}

			if(!m_SpliceMode)
			{
				// Current child has stopped so request the next one
				RequestChildSoundFromOffset(m_SoundRef, nextIndex, &m_ScratchInitParams);
				m_IsPreparing[nextIndex] = true;
			}
			m_Index = nextIndex;
		}
		return true;
	}
	else
	{
		return true;
	}
}

s32 audLoopingSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	SoundAssert(GetChildSound(0));
	bool isChildLooping = false;
	s32 childLength = GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);

	if (m_LoopCount == -1)
	{
		if (isLooping)
		{
			*isLooping = true;
		}
		return childLength;
	}
	if (!isChildLooping)
	{
		if(isLooping)
		{
			*isLooping = false;
		}
		if (childLength != kSoundLengthUnknown)
		{
			return (childLength * m_LoopCount);
		}
		return kSoundLengthUnknown;
	}
	if(isLooping)
	{
		*isLooping = true;
	}
	return childLength;
}

void audLoopingSound::AudioKill()
{
	if(HasChildSound(0))
	{
		GetChildSound(0)->_ManagedAudioKill();
	}
	if(HasChildSound(1))
	{
		GetChildSound(1)->_ManagedAudioKill();
	}
}

void audLoopingSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 childIndex)
{
	if(GetChildSound(childIndex))
	{
		GetChildSound(childIndex)->SetStartOffset(m_LoopPoint);
	}
}

#if __SOUND_DEBUG_DRAW
bool audLoopingSound::DebugDrawInternal(const u32 UNUSED_PARAM(timeInMs), audDebugDrawManager &drawManager, const bool UNUSED_PARAM(drawDynamicChildren)) const
{
	char buf[64];
	if(m_LoopCount == -1)
	{
		formatf(buf, "- LoopCount: %u / inf", m_CurrentLoopCount);
	}
	else
	{
		formatf(buf, "- LoopCount: %u / %u", m_CurrentLoopCount, m_LoopCount);
	}
	drawManager.DrawLine(buf);	
	
	return true;
}
#endif
} // namespace rage
