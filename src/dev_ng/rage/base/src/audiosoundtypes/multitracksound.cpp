//
// audioengine/multitracksound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "multitracksound.h"
#include "audioengine/engine.h"
#include "audioengine/soundfactory.h"

#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/syncsource.h"

namespace rage {

AUD_IMPLEMENT_STATIC_WRAPPERS(audMultitrackSound);

audMultitrackSound::~audMultitrackSound()
{
	for(u32 i = 0; i < m_NumSoundRefs; i++)
	{
		if(GetChildSound(i))
		{
			DeleteChild(i);
		}	
	}

	audMixerSyncManager::Get()->Release(m_SyncId);
	m_SyncId = audMixerSyncManager::InvalidId;
}

#if !__SPU
audMultitrackSound::audMultitrackSound()
{
	for(u32 i = 0 ; i < audSound::kMaxChildren; i++)
	{
		SetChildSound(i,NULL);
	}

	m_HaveReleasedChildren = false;
	m_SyncId = audMixerSyncManager::InvalidId;
}

bool audMultitrackSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	bool success;
	
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	// We delete finished children immediately when they finish in AudioUpdate, to free up pool slots as early as possible.  This means
	// it's not safe to query the hierarchy from other threads.
	SetHasDynamicChildren();

	MultitrackSound* multitrackSoundData = (MultitrackSound*)GetMetadata();
	m_NumSoundRefs = multitrackSoundData->numSoundRefs;

	m_ShouldStopWhenChildStops = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_MULTITRACKSOUND_STOPWHENCHILDSTOPS) == AUD_TRISTATE_TRUE);

	const bool shouldSyncChildren = (AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_MULTITRACKSOUND_SYNCCHILDREN) == AUD_TRISTATE_TRUE);
	if(shouldSyncChildren)
	{
		m_SyncId = audDriver::GetMixer()->GetSyncManager().Allocate();
	}
	audSoundScratchInitParams scratchInitParamsCopy = *scratchInitParams;
	if(audVerifyf(m_NumSoundRefs <= audSound::kMaxChildren, "MultitrackSound %s has too many children (%u, limit is %u)", GetName(), m_NumSoundRefs, audSound::kMaxChildren))
	{
		success = true;
		for(u32 i = 0; i < m_NumSoundRefs; i++)
		{
			*scratchInitParams = scratchInitParamsCopy;
			SetChildSound(i, SOUNDFACTORY.GetChildInstance(multitrackSoundData->SoundRef[i].SoundId, this, initParams, scratchInitParams, false));
			if(!GetChildSound(i))
			{
				success = false;
			}
		}
		return success;
	}
	
	return false;
}
#endif

audPrepareState audMultitrackSound::AudioPrepare(audWaveSlot *waveSlot, const bool checkStateOnly)
{
	audPrepareState state;
	bool finishedPreparing = true;

	for(u32 i = 0; i < m_NumSoundRefs; i++)
	{
		audSound* childSound = GetChildSound(i);
		if(childSound)
		{
			childSound->SetStartOffset((s32)GetPlaybackStartOffset());
			state = childSound->_ManagedAudioPrepare(waveSlot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
			if(state == AUD_PREPARE_FAILED)
			{
				// bail out if any of our children have failed to prepare
				return AUD_PREPARE_FAILED;
			}
			else if(state == AUD_PREPARING)
			{
				finishedPreparing = false;
			}
		}
	}

	if(finishedPreparing)
	{
		return AUD_PREPARED;
	}
	else
	{
		return AUD_PREPARING;
	}
}


u32 audMultitrackSound::GetNumSoundReferences()
{
	return m_NumSoundRefs;
}

void audMultitrackSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	u32 syncSourceToMaster = m_SyncId;
	if(GetCurrentSyncSlaveId() != audMixerSyncManager::InvalidId)
	{
		syncSourceToMaster = GetCurrentSyncSlaveId();
	}

	audSoundSyncSetMasterId masterSyncId;
	masterSyncId.Set(syncSourceToMaster);
	audSoundSyncSetSlaveId slaveSyncId;
	slaveSyncId.Set(syncSourceToMaster);

	u32 startOffset = GetPlaybackStartOffset(); // this is always in ms, not percentage

	for(u32 i = 0 ; i < m_NumSoundRefs; i++)
	{
		audSound* childSound = GetChildSound(i);
		if(childSound)
		{
			/*
			bool isChildLooping = false;
			s32 thisLength = childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
			if ((thisLength != AUD_SOUND_LENGTH_UNKNOWN) && ((u32)thisLength <= startOffset) && !isChildLooping)
			{
				// We have a valid length for our child, and it's shorter than our start offset, so don't play it. In fact, delete it, to save space.
				delete childSound;
				SetChildSound(i,NULL);
			}
			else
			*/
			{
				childSound->SetStartOffset((s32)startOffset, false);
				childSound->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
				childSound->_ManagedAudioPlay(timeInMs, combineBuffer);
			}
		}
	}
}

bool audMultitrackSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	u32 syncSourceToMaster = m_SyncId;
	if(GetCurrentSyncSlaveId() != audMixerSyncManager::InvalidId)
	{
		syncSourceToMaster = GetCurrentSyncSlaveId();
	}

	audSoundSyncSetMasterId masterSyncId;
	masterSyncId.Set(syncSourceToMaster);
	audSoundSyncSetSlaveId slaveSyncId;
	slaveSyncId.Set(syncSourceToMaster);

	bool stillPlaying = false, shouldReleaseChildren = false;

	for(u32 i = 0 ; i < m_NumSoundRefs; i++)
	{
		audSound* childSound = GetChildSound(i);
		if(childSound)
		{
			if (childSound->GetPlayState() == AUD_SOUND_PLAYING)
			{
				if (childSound->_ManagedAudioUpdate(timeInMs, combineBuffer))
				{
					stillPlaying = true;
				}
			}
			else if(GetChildSound(i)->GetPlayState() == AUD_SOUND_WAITING_TO_BE_DELETED)
			{
				shouldReleaseChildren = true;
				// we can clean this child up immediately
				DeleteChild(i);
			}
		}
	}	

	if (!stillPlaying)
	{
		// All our subsounds have finished, so we have too
		return (false);
	}
	else
	{
		if(shouldReleaseChildren && m_ShouldStopWhenChildStops && !m_HaveReleasedChildren)
		{
			for(u32 i = 0; i < m_NumSoundRefs; i++)
			{
				audSound* childSound = GetChildSound(i);
				if(childSound && childSound->GetPlayState() == AUD_SOUND_PLAYING)
				{
					childSound->_Release();
				}
			}
			m_HaveReleasedChildren = true;
		}
		return (true);
	}
}

void audMultitrackSound::AudioKill()
{
	for(u32 i = 0 ; i < m_NumSoundRefs; i++)
	{
		if(GetChildSound(i))
		{
			GetChildSound(i)->_ManagedAudioKill();
		}
	}
}

s32 audMultitrackSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	s32 longestChildDuration = 0, childDuration;

	for(u32 i = 0 ; i < m_NumSoundRefs; i++)
	{
		audSound* childSound = GetChildSound(i);
		if(childSound)
		{
			bool isChildLooping = false;
			childDuration = childSound->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
			if (isChildLooping && isLooping)
			{
				*isLooping = true;
			}
			if(childDuration > longestChildDuration)
			{
				longestChildDuration = childDuration;
			}
		}
	}

	return longestChildDuration;
}

} // namespace rage
