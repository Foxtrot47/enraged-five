//
// audiosoundtypes/wrappersound.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_WRAPPER_SOUND_H
#define AUD_WRAPPER_SOUND_H

#include "sound.h"
#include "sounddefs.h"

namespace rage
{

// PURPOSE
//  A wrapper sound adds another layer of base sound properties on its child sound, and allows the
//	sound designer to affect e.g. the pitch of a shared sound without altering the actual shared sound
class audWrapperSound : public audSound
{
public:
	audWrapperSound();
	~audWrapperSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();
	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *slot, const bool checkStateOnly);

private:

	void ApplyVariables(audSoundCombineBuffer &combineBuffer);

	audVariableHandle m_VolumeRef;
	audVariableHandle m_VolumeCurveScaleRef;
	audVariableHandle m_PitchRef;
	audVariableHandle m_PanRef;
	audVariableHandle m_LPFCutoffRef;
	audVariableHandle m_HPFCutoffRef;

	bool m_HasVariables;
};

} // namespace rage

#endif //AUD_WRAPPER_SOUND_H
