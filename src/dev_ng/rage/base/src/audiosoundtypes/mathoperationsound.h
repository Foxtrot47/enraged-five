// 
// audiosoundtypes/mathoperationsound.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_MATHOPERATIONSOUND_H
#define AUD_MATHOPERATIONSOUND_H

#include "audioengine/engineutil.h"
#include "audiohardware/driverutil.h"
#include "audiosoundtypes/sound.h"
#include "audiosoundtypes/sounddefs.h"

#include "atl/bitset.h"

namespace rage {

struct audMathOp
{
	union
	{
		audVariableHandle var;
		f32 immediate;
	}param1;
	union 
	{
		audVariableHandle var;
		f32 immediate;
	}param2;
	union 
	{
		audVariableHandle var;
		f32 immediate;
	}param3;
	audVariableHandle result;
	u32 op;
};

struct audMathOpCache
{
	float p1;
	float p2;
	float p3;
	float result;
};

class audMathOperationSound : public audSound
{
public:
	~audMathOperationSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audMathOperationSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;

#if __SOUND_DEBUG_DRAW
	AUD_VIRTUAL bool DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool drawDynamicChildren) const;
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:

	// PURPOSE
	//	Stores resolved operation data by parsing supplied metadata.
	void ResolveOperations(MathOperationSound *metadata);

	// PURPOSE
	//	Executes all operations
	void RunComputation(const u32 timeInMs);

	__forceinline f32 opAdd(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return p1 + p2;
	}

	__forceinline f32 opSubtract(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return p1 - p2;
	}

	__forceinline f32 opMultiply(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return p1 * p2;
	}

	__forceinline f32 opDivide(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return p1 / p2;
	}

	__forceinline f32 opSet(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return p1;
	}

	__forceinline f32 opMod(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return (p2 == 0.f ? 0.f : fmod(p1,p2));
	}

	__forceinline f32 opMin(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return Min(p1,p2);
	}

	__forceinline f32 opMax(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return Max(p1,p2);
	}

	__forceinline f32 opAbs(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return Abs(p1);
	}

	__forceinline f32 opSign(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return Sign(p1);
	}

	__forceinline f32 opFloor(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return floorf(p1);
	}

	__forceinline f32 opCeil(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return ceilf(p1);
	}

	__forceinline f32 opRandomize(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return audEngineUtil::GetRandomNumberInRange(p1,p2);
	}

	__forceinline f32 opSin(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return sinf(p1);
	}

	__forceinline f32 opCos(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return cosf(p1);
	}

	__forceinline f32 opSqrt(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return Sqrtf(p1);
	}

	__forceinline f32 opdBToLinear(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return audDriverUtil::ComputeLinearVolumeFromDb(p1);
	}

	__forceinline f32 opLinearTodB(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return audDriverUtil::ComputeDbVolumeFromLinear(p1);
	}

	__forceinline f32 opPitchToRatio(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return audDriverUtil::ConvertPitchToRatio(static_cast<s32>(p1));
	}

	__forceinline f32 opRatioToPitch(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return static_cast<f32>(audDriverUtil::ConvertRatioToPitch(p1));
	}

	__forceinline f32 opGetTime(const f32 UNUSED_PARAM(p1), const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return m_TimeInMs / 1000.f;
	}

	__forceinline f32 opFsel(const f32 p1, const f32 p2, const f32 p3) const
	{
		return Selectf(p1,p2,p3);
	}

	__forceinline f32 opValueInRange(const f32 p1, const f32 p2, const f32 p3) const
	{
		return (p2 + ((p3-p2) * p1));
	}

	__forceinline f32 opClampRange(const f32 p1, const f32 p2, const f32 p3) const
	{
		return ClampRange(p1,p2,p3);
	}

	__forceinline f32 opClampValue(const f32 p1, const f32 p2, const f32 p3) const
	{
		return Clamp(p1,p2,p3);
	}

	__forceinline f32 opPow(const f32 p1, const f32 p2, const f32 UNUSED_PARAM(p3)) const
	{
		return Powf(p1,p2);
	}

	__forceinline f32 opRound(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return static_cast<f32>(round(p1));
	}

	__forceinline f32 opScaledSin(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return ((sinf(p1 * 2.0f * PI) + 1.0f)*0.5f);
	}

	__forceinline f32 opScaledTri(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		const f32 x = p1 - floorf(p1);
		const f32 risingVal = x * 2.f;
		const f32 fallingVal = 1.f - ((x-0.5f) * 2.f);
		return Selectf(x - 0.5f, fallingVal, risingVal);
	}

	__forceinline f32 opScaledSaw(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		return p1 - floorf(p1);
	}

	__forceinline f32 opScaledSquare(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		f32 x = p1 - floorf(p1);
		return Selectf(x - 0.5f, 0.0f, 1.0f);
	}

	__forceinline f32 opSmooth(const f32 p1, const f32 p2, const f32 p3) const
	{
		f32 delta = p2 - p1;
		f32 sign = Sign(delta);
		delta = Min(Abs(delta), p3) * sign;
		return p2 - delta;
	}

	__forceinline f32 opGetScaledTime(const f32 p1, const f32 UNUSED_PARAM(p2), const f32 UNUSED_PARAM(p3)) const
	{
		f32 timeFactor = opGetTime(0.0f,0.0f,0.0f) / p1;
		return (timeFactor - floorf(timeFactor));
	}

	bool IsParamImm(const u32 paramId, const u32 stepIndex)
	{
		Assert(paramId >= 1 && paramId <= 3);
		return m_Immediates.IsSet(stepIndex*3 + paramId - 1);
	}

	void SetParamImm(const u32 paramId, const u32 stepIndex, const bool is)
	{
		Assert(paramId >= 1 && paramId <= 3);
		m_Immediates.Set(stepIndex*3 + paramId - 1, is);
	}

	atFixedBitSet32 m_Immediates;
	u32 m_TimeInMs;
	u8 m_NumOperations;
	u8 m_SlotIndexForResolvedOperations;
	
#if __BANK
	u8 m_CacheSlotIndex;
#endif

	bool m_RunOnce;
};

} // namespace rage

#endif // AUD_MATHOPERATIONSOUND_H
