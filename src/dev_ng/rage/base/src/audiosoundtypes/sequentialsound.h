//
// audioengine/sequentialsound.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SEQUENTIALSOUND_H
#define AUD_SEQUENTIALSOUND_H

#include "sound.h"

#include "soundDefs.h"

namespace rage {

// PURPOSE
//  A scripted sound plays all of its children in turn, and maintains a variable block.
class audSequentialSound : public audSound
{
public:
	
	~audSequentialSound();

	AUD_DECLARE_STATIC_WRAPPERS;

#if !__SPU
	audSequentialSound();
	// PURPOSE
	//  Implements base class function for this sound.
	bool Init(const void* metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams);
#endif

	// PURPOSE
	//  Implements base class function for this sound.
	void AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	bool AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer);
	// PURPOSE
	//  Implements base class function for this sound.
	void AudioKill();

	// PURPOSE
	//  Implements base class function for this sound.
	s32 ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping);

	AUD_IMPLEMENT_BASE_FUNCTION_WRAPPERS;
	
	// PURPOSE
	//  Implements base class function for this sound.
	audPrepareState AudioPrepare(audWaveSlot *, const bool checkStateOnly);

private:
	// PURPOSE
	//  Calcluates the index of the child sound that will be played first and its start offset, in milliseconds, based upon
	//	the overall playback start offset for this sound.
	// PARAMS
	//	startingSoundIndex	- Used to return the index of the first child sound to be played. 
	//	startOffset			- Used to return the start offset for the first child sound to be played, in milliseconds. 
	void ComputeStartingSoundIndexAndOffset(s32 &startingSoundIndex, u32 &startOffset);

	void HandleFreeSlot(s32 index);

	// PURPOSE
	//	Sets up the child sound
	void ChildSoundCallback(const u32 timeInMs, const u32 index);



	audSoundScratchInitParams m_ScratchInitParams;
	s32 m_NumSoundRefs;
	s32	m_CurrentPlayingSoundIndex;
	u8 m_ChildIndexToPrepare;
	u8 m_SoundRefSlotIndex;
	bool m_OptimizedPlayback;
};

} // namespace rage

#endif // AUD_SEQUENTIALSOUND_H
