// 
// audiosoundtypes/envelopesound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "envelopesound.h"
#include "audiohardware/driverutil.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"

namespace rage
{

AUD_IMPLEMENT_STATIC_WRAPPERS(audEnvelopeSound);

audEnvelopeSound::~audEnvelopeSound()
{
	if(GetChildSound(0))
	{
		DeleteChild(0);
	}

	if(m_CurveSlotIndex != 0xff)
	{
		sm_Pool.DeleteRequestedSettings(m_InitParams.BucketId,m_CurveSlotIndex);
		m_CurveSlotIndex = 0xff;
	}
}

#if !__SPU

audEnvelopeSound::audEnvelopeSound()
{
	m_TotalEnvelopeState = AUD_TOTAL_ENVELOPE_NONE;

	m_EnvelopeState = AUD_ENVELOPE_NOT_STARTED;
	m_AttackFactor = 1.0f;
	m_DecayFactor = 1.0f;
	m_ReleaseFactor = 1.0f;
	m_AttackStartTime = 0;
	m_DecayStartTime = 0;
	m_DecayStopTime = 0;
	m_HoldStopTime = -1;
	m_ReleaseStartTime = -1;
	m_ReleaseStopTime = -1;

	m_AttackVariable = NULL;
	m_DecayVariable = NULL;
	m_HoldVariable = NULL;
	m_SustainVariable = NULL;
	m_ReleaseVariable = NULL;

	m_OutputVariable = NULL;
	m_Mode = kEnvelopeSoundVolume;
	m_OutputMin = 0.f;
	m_OutputMax = 1.0f;

	m_ShouldStopChildren = true;
	m_HasEnvelopeFinished = false;
	m_HasReleaseEnvelope = false;
	m_IsCodeSetEnvelope = false;
	m_ShouldCascadeRelease = false;

	m_HadExternalRelease = false;

	m_CurveSlotIndex = 0xff;
}

void audEnvelopeSound::SetRequestedEnvelope(const u32 attack, const u32 decay, const u32 sustain, const s32 hold, const s32 release)
{
	// We shouldn't be changing this setting while the sound is playing
	SoundAssert(GetPlayState()!=AUD_SOUND_PLAYING);
	SoundAssert(m_CurveSlotIndex != 0xff);
	audEnvelopeSoundState *state = (audEnvelopeSoundState*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_CurveSlotIndex);

	Assign(state->m_Attack, attack);
	Assign(state->m_Decay, decay);
	Assign(state->m_Sustain, sustain);
	Assign(state->m_Hold, hold);
	Assign(state->m_Release, release);
	state->m_IsCodeSetEnvelope = true;
}

void audEnvelopeSound::SetChildSoundReference(const u32 soundHash)
{
	SoundAssert(m_CurveSlotIndex != 0xff);
	audEnvelopeSoundState *state = (audEnvelopeSoundState*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_CurveSlotIndex);
	state->m_CodeSetChildRef = SOUNDFACTORY.GetMetadataManager().GetObjectMetadataRefFromHash(soundHash);
}

void audEnvelopeSound::ChildSoundCallback(const u32 UNUSED_PARAM(timeInMs), const u32 ASSERT_ONLY(index))
{
	SoundAssert(index == 0);
	SoundAssert(m_CurveSlotIndex != 0xff);
	// we can modify the requested settings slot here since its a PPU callback
	audEnvelopeSoundState *state = (audEnvelopeSoundState*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_CurveSlotIndex);
	state->m_CodeSetChildRef.SetInvalid();
}

bool audEnvelopeSound::Init(const void *metadata, const audSoundInternalInitParams* initParams, audSoundScratchInitParams *scratchInitParams)
{
	if(!audSound::Init(metadata, initParams, scratchInitParams))
		return false;

	CompileTimeAssert(sizeof(audEnvelopeSoundState) <= sizeof(audRequestedSettings));
	audEnvelopeSoundState *state = (audEnvelopeSoundState*)sm_Pool.AllocateRequestedSettingsSlot(sizeof(audEnvelopeSoundState), m_InitParams.BucketId DEV_ONLY(, GetName()) );
	if(!state)
	{
		return false;
	}

	::new(state) audEnvelopeSoundState();
	state->m_ScratchInitParams = *scratchInitParams;
	m_CurveSlotIndex = sm_Pool.GetRequestedSettingsSlotIndex(m_InitParams.BucketId, state);

	EnvelopeSound *envelopeSoundData = (EnvelopeSound*)GetMetadata();

	// if a child sound has been specified then we fail to Init() if we can't get it.
	if(envelopeSoundData->SoundRef.IsValid())
	{
		SetChildSound(0,SOUNDFACTORY.GetChildInstance(envelopeSoundData->SoundRef, this, initParams, scratchInitParams, false));
		if(!GetChildSound(0))
		{
			return false;
		}
	}

	m_Attack = (u16)ApplyVariance(envelopeSoundData->Envelope.Attack, envelopeSoundData->Envelope.AttackVariance, 1<<16U);
	m_Decay = (u16)ApplyVariance(envelopeSoundData->Envelope.Decay, envelopeSoundData->Envelope.DecayVariance, 1<<16U);
	m_Sustain = (u8)ApplyVariance(envelopeSoundData->Envelope.Sustain, envelopeSoundData->Envelope.SustainVariance, 100);
	m_Hold = envelopeSoundData->Envelope.Hold;
	if(m_Hold > 0)
	{
		m_Hold += ComputeOffsetFromVariance(Min<s32>(m_Hold, envelopeSoundData->Envelope.HoldVariance));
	}
	m_Release = envelopeSoundData->Envelope.Release;
	if(m_Release > 0)
	{
		m_Release += ComputeOffsetFromVariance(Min<s32>(m_Release, envelopeSoundData->Envelope.ReleaseVariance));
	}
	m_AttackVariable = _FindVariableUpHierarchy(envelopeSoundData->AttackVariable);
	m_DecayVariable = _FindVariableUpHierarchy(envelopeSoundData->DecayVariable);
	m_HoldVariable = _FindVariableUpHierarchy(envelopeSoundData->HoldVariable);
	m_SustainVariable = _FindVariableUpHierarchy(envelopeSoundData->SustainVariable); 
	m_ReleaseVariable = _FindVariableUpHierarchy(envelopeSoundData->ReleaseVariable);

	m_ShouldStopChildren = AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_ENVELOPESOUND_SHOULDSTOPCHILDREN) != AUD_TRISTATE_FALSE;
	m_ShouldCascadeRelease = AUD_GET_TRISTATE_VALUE(GetMetadataFlags(), FLAG_ID_ENVELOPESOUND_SHOULDCASCADERELEASE) == AUD_TRISTATE_TRUE;

	m_Mode = envelopeSoundData->Mode;
	if(m_Mode == kEnvelopeSoundVariable)
	{
		m_OutputVariable = _FindVariableUpHierarchy(envelopeSoundData->OutputVariable);
		SoundAssertMsg(m_OutputVariable, "EnvelopeSound in variable mode with invalid output variable");
		if(!m_OutputVariable)
		{
			return false;
		}
		m_OutputMin = envelopeSoundData->OutputRange.min;
		m_OutputMax = envelopeSoundData->OutputRange.max;
	}
	else if(m_Mode == kEnvelopeSoundVolume)
	{
		m_OutputMin = audDriverUtil::ComputeLinearVolumeFromDb(envelopeSoundData->OutputRange.min);
		m_OutputMax = audDriverUtil::ComputeLinearVolumeFromDb(envelopeSoundData->OutputRange.max);
	}
	else if(m_Mode == kEnvelopeSoundPan)
	{
		m_OutputMin = Clamp(envelopeSoundData->OutputRange.min, 0.0f, 360.f);
		m_OutputMax = Clamp(envelopeSoundData->OutputRange.max, 0.0f, 360.f);
	}
	else if(m_Mode == kEnvelopeSoundHPF)
	{
		m_OutputMin = Clamp(envelopeSoundData->OutputRange.min, 0.0f, float(kVoiceFilterHPFMaxCutoff));
		m_OutputMax = Clamp(envelopeSoundData->OutputRange.max, 0.0f, float(kVoiceFilterHPFMaxCutoff));
	}
	else if(m_Mode == kEnvelopeSoundLPF)
	{
		m_OutputMin = Clamp(envelopeSoundData->OutputRange.min, 0.0f, float(kVoiceFilterLPFMaxCutoff));
		m_OutputMax = Clamp(envelopeSoundData->OutputRange.max, 0.0f, float(kVoiceFilterLPFMaxCutoff));
	}
	else
	{
		SoundAssert(m_Mode == kEnvelopeSoundPitch);
		// convert from semitones to ratio
		m_OutputMin = audDriverUtil::ConvertPitchToRatio(envelopeSoundData->OutputRange.min*100.f);
		m_OutputMax = audDriverUtil::ConvertPitchToRatio(envelopeSoundData->OutputRange.max*100.f);
	}
	
	// Initialise the curves associated with this sound
	state->curves[AUD_ATTACK_CURVE].Init(envelopeSoundData->Envelope.AttackCurve);
	state->curves[AUD_DECAY_CURVE].Init(envelopeSoundData->Envelope.DecayCurve);
	state->curves[AUD_RELEASE_CURVE].Init(envelopeSoundData->Envelope.ReleaseCurve);
	SoundAssert(state->curves[AUD_ATTACK_CURVE].IsValidInSoundTask());
	SoundAssert(state->curves[AUD_DECAY_CURVE].IsValidInSoundTask());
	SoundAssert(state->curves[AUD_RELEASE_CURVE].IsValidInSoundTask());
	if(!state->curves[AUD_ATTACK_CURVE].IsValidInSoundTask() || 
		!state->curves[AUD_RELEASE_CURVE].IsValidInSoundTask() || 
		!state->curves[AUD_DECAY_CURVE].IsValidInSoundTask())
	{
		return false;
	}
	return true;
}
#endif

void audEnvelopeSound::ActionReleaseRequestInternal(const u32 timeInMs, const bool isReleasingDueToEnvelope)
{
	if(!isReleasingDueToEnvelope)
	{
		m_HadExternalRelease = true;
	}

	// If an envelope sound is stopped during its predelay it should revert to base sound release functionality; ie stop
	// immediately.
	if(GetPredelayState() != AUD_PREDELAY_FINISHED)
	{
		BaseActionReleaseRequest(timeInMs);
		return;
	}

	u32 attack,decay,sustain;
	s32 release, hold;
	GetMetadataEnvelope(attack, decay, sustain, hold, release);
	s32 timeSinceTriggered = (s32)GetCurrentPlayTime(timeInMs);
	// Check to see if we've an release envelope. If not, flag the base sound to deal with release env.
	if (release>=0)
	{
		SetWasSetToReleaseLastFrame(true);
//		SetIsBeingReleased();
		m_HasReleaseEnvelope = true;
		// Quite a few options here - what we do depends on the current state of the metadata and requested envelopes,
		// and the presence/absence of release times on them.

		// We have a release envelope - see which ones, and what our current states are.
		if (release>=0)
		{
			// We're set to release and we don't have a RequestedRelease, so go into MetadataRelease, if not already.
			if (m_EnvelopeState != AUD_ENVELOPE_RELEASE)
			{
				// We're set to release, and we're not already in MetadataRelease.
				m_EnvelopeState = AUD_ENVELOPE_RELEASE;
				m_ReleaseStartTime = timeSinceTriggered;
				m_ReleaseStopTime = timeSinceTriggered + release;
			}
			// else - we do nothing if you try to release a sound already in its release phase
		}
	} // endif - (set to release this frame)

	// if we're set to not stop child after release phase then we need to pass the release down to child sounds
	if((release < 0 || m_ShouldCascadeRelease) 
		&& (m_ShouldStopChildren || (!m_ShouldStopChildren && !isReleasingDueToEnvelope)))
	{
		// We pretend we're not releasing in the least, so as to fool the base sound, and get updates like normal.
		// (unless we don't have any release, in which case we act just the same as the base sound does)
		SetWasSetToReleaseLastFrame(true);
//		SetIsBeingReleased();
		// If we have no release envelope, we release via children
		// Tell child sounds to stop, so that they all start releasing
		ManagedAudioStopChildren();
		SetIsWaitingForChildrenToRelease(true);
		m_HasReleaseEnvelope = (release >= 0);
	}
}

void audEnvelopeSound::AudioPlay(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	// figure out if there is a non-default envelope on this sound
	audEnvelopeSoundState *state = (audEnvelopeSoundState*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_CurveSlotIndex);
	if(state->m_IsCodeSetEnvelope)
	{
		m_Attack = state->m_Attack;
		m_Decay = state->m_Decay;
		m_Sustain = state->m_Sustain;
		m_Hold = state->m_Hold;
		m_Release = state->m_Release;
		m_IsCodeSetEnvelope = true;
	}

	if(m_Attack != 0 || m_Decay != 0 || m_Sustain != 100 
		|| m_Hold != -1
		|| m_Release > 0 || m_AttackVariable || m_DecayVariable 
		|| m_HoldVariable || m_SustainVariable|| m_ReleaseVariable)
	{
		m_IsEnvelopeActive = true;
		m_TotalEnvelopeState = AUD_TOTAL_ENVELOPE_PROCESSING;
	}
				
	// Calculate times the attack and decay phases of the envelope start at.
	ComputeMetadataEnvelopeStartTimes();

	// We need to calculate this on the first frame, as _ManagedAudioPlay on a SimpleSound now starts playing the same frame.
	ApplyEnvelope(timeInMs);

	audSound *child = GetChildSound(0);
	if(child)
	{
		ApplyEnvelopeToChild(combineBuffer);		
		
		child->SetStartOffset((s32)GetPlaybackStartOffset(), false);
		child->_SetRequestedInternalPredelay(GetPredelayRemainingMs(timeInMs));
		child->_ManagedAudioPlay(timeInMs, combineBuffer);
	}
}

void audEnvelopeSound::ApplyEnvelopeToChild(audSoundCombineBuffer &combineBuffer)
{
	SoundAssert(GetChildSound(0));
	const f32 envelopeLinearFactor = m_AttackFactor * m_DecayFactor * m_ReleaseFactor;
	const f32 scaledFactor = Lerp(envelopeLinearFactor, m_OutputMin, m_OutputMax);

	if(Likely(m_Mode == kEnvelopeSoundVolume))
	{
		const f32 envelopeVolumeDb = audDriverUtil::ComputeDbVolumeFromLinear(scaledFactor);
		combineBuffer.Volume += envelopeVolumeDb;
	}
	else
	{
		switch(m_Mode)
		{		
			case kEnvelopeSoundPitch:
			{
				const s32 pitch = audDriverUtil::ConvertRatioToPitch(scaledFactor);
				combineBuffer.Pitch += (s16)pitch;
			}
			break;
		case kEnvelopeSoundPan:
			{
				const s32 pan = (s32)(scaledFactor);
				combineBuffer.Pan = (s16)CombinePan(combineBuffer.Pan, pan);
			}
			break;
		case kEnvelopeSoundLPF:
			{
				const u16 cutoff = (u16)(scaledFactor);
				combineBuffer.LPFCutoff = Min<u16>(combineBuffer.LPFCutoff, cutoff);
			}
			break;
		case kEnvelopeSoundHPF:
			{
				const u16 cutoff = (u16)(scaledFactor);
				combineBuffer.HPFCutoff = Max<u16>(combineBuffer.HPFCutoff, cutoff);
			}
			break;
		case kEnvelopeSoundVariable:
			SoundAssert(m_OutputVariable);
			AUD_SET_VARIABLE(m_OutputVariable, scaledFactor);			
			break;		
		}
	}
}

bool audEnvelopeSound::AudioUpdate(u32 timeInMs, audSoundCombineBuffer &combineBuffer)
{
	if(GetChildSound(0))
	{
		SoundAssert (GetChildSound(0)->GetPlayState() == AUD_SOUND_PLAYING);

		bool playing = ApplyEnvelope(timeInMs);
		
		if(!playing)
		{
			GetChildSound(0)->_ManagedAudioKill();
		}
		else
		{
			ApplyEnvelopeToChild(combineBuffer);
			playing = GetChildSound(0)->_ManagedAudioUpdate(timeInMs, combineBuffer);
		}

		return playing;
	}
	return false;
}

void audEnvelopeSound::AudioKill()
{
	if(GetChildSound(0))
	{
		GetChildSound(0)->_ManagedAudioKill();
	}
}

audPrepareState audEnvelopeSound::AudioPrepare(audWaveSlot *slot, const bool checkStateOnly)
{
	SoundAssert(m_CurveSlotIndex != 0xff);
	audEnvelopeSoundState *state = (audEnvelopeSoundState*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_CurveSlotIndex);
	if(!state->m_CodeSetChildRef.IsValid())
	{
		if(GetChildSound(0))
		{
			GetChildSound(0)->SetStartOffset((s32)GetPlaybackStartOffset(), false);
			return GetChildSound(0)->_ManagedAudioPrepare(slot, GetCanDynamicallyLoad(), checkStateOnly, GetWaveLoadPriority());
		}
		else
		{
			return AUD_PREPARE_FAILED;
		}
	}
	else
	{
		// TODO: This is not entirely thread safe.  We need to mark EnvelopeSounds as allowing code override, and 
		// setting this flag on the basis of this in Init() (and ignore SetChildSoundReference requests if the flag isn't set)
		SetHasDynamicChildren();
		if(!checkStateOnly)
		{
			if(GetChildSound(0))
			{
				DeleteChild(0);
			}

			RequestChildSoundFromOffset(state->m_CodeSetChildRef, 0, &state->m_ScratchInitParams);
		}
		return AUD_PREPARING;
	}
}

s32 audEnvelopeSound::ComputeDurationMsExcludingStartOffsetAndPredelay(bool* isLooping)
{
	if(!GetChildSound(0))
	{
		// we have no sound so we have a length of 0ms
		if (isLooping)
		{
			*isLooping = false;
		}
		return 0;
	}

	bool isChildLooping = false;
	s32 childLength = GetChildSound(0)->_ComputeDurationMsIncludingStartOffsetAndPredelay(&isChildLooping);
	// If we have a hold time, our length will depend on our envelope phases, and the length of the child sound.
	// If not, it's just the usual child length.
	u32 metadataAttack, metadataDecay;
	u32  metadataSustain;
	s32 metadataHold, metadataRelease;
	GetMetadataEnvelope(metadataAttack, metadataDecay, metadataSustain, metadataHold, metadataRelease);
	s32 envelopeLength = -1;
	if (metadataHold>=0)
	{
		// We have a metadata envelope hold time. So the length is either the total phase time, or the length of our (non-looping) child.
		envelopeLength = metadataAttack + metadataDecay + metadataHold;
		if (metadataRelease>=0)
		{
			envelopeLength += metadataRelease;
		}
		if (!isChildLooping && childLength>=0 && childLength<envelopeLength)
		{
			envelopeLength = childLength;
		}
	}


	// If either envelope is >=0, take the smallest of the two, otherwise, go with the child sound's length
	s32 length = -1;
	bool areWeLooping = false;
	// if we're not going to stop the child at the end of our release phase then use child length
	if (envelopeLength>=0 && m_ShouldStopChildren)
	{
		length = envelopeLength;
		areWeLooping = false;
	}
	else
	{
		// No hold times, so go with the child sound
		length = childLength;
		areWeLooping = isChildLooping;
	}
	
	if (isLooping)
	{
		*isLooping = areWeLooping;
	}
	return length;
}

void audEnvelopeSound::GetMetadataEnvelope(u32 &attack, u32 &decay, u32 &sustain, s32 &hold, s32 &release) const
{
	attack = m_Attack;
	decay = m_Decay;
	sustain = m_Sustain;
	hold = m_Hold;
	release = m_Release;

	// Add on any variable settings - need to make sure we do this for release a bit later, too.
	if (m_AttackVariable)
	{
		attack = (u16)(AUD_GET_VARIABLE(m_AttackVariable)*1000.f);
	}
	if (m_DecayVariable)
	{
		decay = (u16)(AUD_GET_VARIABLE(m_DecayVariable)*1000.f);
	}
	if (m_SustainVariable)
	{
		SoundAssert(AUD_GET_VARIABLE(m_SustainVariable) <=100.f);
		sustain = (u8)(AUD_GET_VARIABLE(m_SustainVariable));
	}
	if (m_HoldVariable)
	{
		hold = (s32)(AUD_GET_VARIABLE(m_HoldVariable)*1000.f);
	}
	if (m_ReleaseVariable)
	{
		release = (s32)(AUD_GET_VARIABLE(m_ReleaseVariable)*1000.f);
	}
}

void audEnvelopeSound::ComputeMetadataEnvelopeStartTimes()
{
	u32 attack;
	u32 decay;
	s32 hold;
	s32 release;
	u32 sustain;
	
	GetMetadataEnvelope(attack, decay, sustain, hold, release);

	// The attack phase starts after the Requested Predelay, plus the Metadata Predelay, minus the Requested StartOffset.
	// This could actually be negative, but we don't care, as we'll just be checking we're < or > this. 
	// Other times follow accordingly. We'll store relative to time triggered, to allow -ve values and not risk overflow.
	m_AttackStartTime = GetPredelay() + GetMetadataPredelay() + GetVariablePredelay() - GetStartOffset();
	// If we have a sub-frame predelay we want the attack to start early
	if(m_AttackStartTime <= kPredelayStartThresholdMs)
	{
		m_AttackStartTime = 0;
	}

	m_DecayStartTime = m_AttackStartTime + attack;
	m_DecayStopTime = m_DecayStartTime + decay;
	if (hold>=0)
	{
		m_HoldStopTime = m_DecayStopTime + hold;
	}
	else
	{
		// We don't have a hold value, so don't have an end time
		m_HoldStopTime = -1;
	}

	// Also prep our volume factors, so they're set to go
	m_AttackFactor = 1.0f;
	m_DecayFactor = 1.0f;
	m_ReleaseFactor = 1.0f;
}

bool audEnvelopeSound::ApplyEnvelope(u32 timeInMs)
{
	if (IsSetToRelease() && !m_HasReleaseEnvelope)
	{
		// We're releasing via children - do nothing here
		return true;
	}

	bool playing = true;
	s32 timeSinceTriggered = (s32)GetCurrentPlayTime(timeInMs);

	if (m_TotalEnvelopeState == AUD_TOTAL_ENVELOPE_NONE)
	{
		if (m_HasReleaseEnvelope)
		{
			// Someone has set a requested release time on us, so we transition to being in the PROCESSING state.
			// Feels like this could be even neater, as we know that we didn't have anything else in the envelope
			// before this happened - but we want to keep the number of states relatively small and manageable.
			m_TotalEnvelopeState = AUD_TOTAL_ENVELOPE_PROCESSING;
		}
	}
	else if (m_TotalEnvelopeState == AUD_TOTAL_ENVELOPE_RELEASE_VIA_CHILDREN)
	{
		// The simplest case - nothing else can change, we're just waiting on our children. So check if they're done
		// and finish if they are.
		playing = AreChildrenPlaying();
	}

	// We're now in the correct state, with the correct release envelope times. If we're meant to be processing our
	// envs, do it now
	if (m_TotalEnvelopeState == AUD_TOTAL_ENVELOPE_PROCESSING)
	{
		playing = ComputeEnvelopeAttenuationAndState(timeSinceTriggered, timeInMs);
	}

	// Stopping the child is optional, unless we've been asked to stop
	if(m_ShouldStopChildren == false && !(m_HadExternalRelease && m_HasEnvelopeFinished))
	{
		playing = true;
	}

	return playing;
}

bool audEnvelopeSound::ComputeEnvelopeAttenuationAndState(u32 timeSinceTriggered, u32 timeInMs)
{
	u32 attack,decay,sustain;
	s32 hold,release;
	GetMetadataEnvelope(attack, decay, sustain, hold, release);
	
	// If metadata envelope's finished, we know we're done.
	if (m_IsEnvelopeActive)
	{
		if (!ComputeMetadataEnvelopeAttenuationAndState(timeSinceTriggered))
		{
			return false;
		}
	}
	
	// envelope has non-zero volume factors, so we're still playing.
	// See if our envelope is in release phase, but neither actually have a release time - hence we wait on
	// our children.
	if (m_EnvelopeState == AUD_ENVELOPE_RELEASE && release < 0
		&& m_TotalEnvelopeState != AUD_TOTAL_ENVELOPE_RELEASE_VIA_CHILDREN)
	{
		m_TotalEnvelopeState = AUD_TOTAL_ENVELOPE_RELEASE_VIA_CHILDREN;	
		ActionReleaseRequestInternal(timeInMs, true);
	}
	return true;
}

bool audEnvelopeSound::ComputeMetadataEnvelopeAttenuationAndState(s32 timeSinceTriggered)
{
	// Grab all our metadata envelope values
	u32 metadataAttack, metadataDecay;
	u32 metadataSustain;
	s32 metadataHold;
	s32 metadataRelease;
	GetMetadataEnvelope(metadataAttack, metadataDecay, metadataSustain, metadataHold, metadataRelease);

	// Work out if we're just past the hold phase, and not already releasing
	if ((metadataHold>=0) &&
		(m_EnvelopeState != AUD_ENVELOPE_RELEASE) &&
		(timeSinceTriggered > m_HoldStopTime))
	{
		// We have a hold time, we're not yet in release and are past the end of the hold phase.
		m_EnvelopeState = AUD_ENVELOPE_RELEASE;
		m_ReleaseStartTime = m_HoldStopTime;
		m_ReleaseStopTime = m_HoldStopTime + metadataRelease;

		// Allow our parent to know we are releasing
		SetIsInReleasePhase();

		// Also, since we could have skipped here instantly with a start-offset or the like, set
		// the rest of our volume factors to the sustain level
		m_AttackFactor = 1.0f;
		m_DecayFactor = (((f32)metadataSustain)/100.0f); 
		m_ReleaseFactor = 1.0f; // about to get overridden

		if(m_ShouldCascadeRelease)
		{
			SetWasSetToReleaseLastFrame(true);
			ManagedAudioStopChildren();
			SetIsWaitingForChildrenToRelease(true);
		}
	}
	audEnvelopeSoundState *state = (audEnvelopeSoundState*)sm_Pool.GetRequestedSettingsSlot(m_InitParams.BucketId, m_CurveSlotIndex);
	
	// Deal with being in release already
	if (m_EnvelopeState == AUD_ENVELOPE_RELEASE)
	{
		// Make sure we actually have a release time, and aren't going to be releasing via our children
		if(metadataRelease>=0)
		{
			// Recalculate release volume factor
			if (timeSinceTriggered>m_ReleaseStopTime)
			{
				// We've finished releasing, so stop ourselves
				m_HasEnvelopeFinished = true;
				return false;
			}
			else
			{
				// We're during the release phase
//				m_MetadataEnvelopeState = AUD_ENVELOPE_RELEASE;
				m_ReleaseFactor = state->curves[AUD_RELEASE_CURVE].CalculateRescaledValue(0.0f, 1.0f, 
					(f32)m_ReleaseStartTime, (f32)m_ReleaseStopTime, (f32)timeSinceTriggered);
			}
		}
		m_EnvelopeState = AUD_ENVELOPE_RELEASE;
	}
	else
	{
		// We're not in release (so we're not after sustain time either, so work out factors for Meta and Requested envelopes

		// Do the metadata envelope
		if (m_EnvelopeState == AUD_ENVELOPE_FINISHED)
		{
			// Why the hell would this ever happen?
			Assert(0);
			// Set at least something to be totally faded out
			m_AttackFactor = 1.0f;
			m_DecayFactor = 1.0f; // this 'should' be the sustain factor
			m_ReleaseFactor = 0.0f;
		}
		else
		{
			// See where we are, and hence what state we're in and what our params should be.
			if (timeSinceTriggered>=m_DecayStopTime)
			{
				m_EnvelopeState = AUD_ENVELOPE_SUSTAIN;
				m_AttackFactor = 1.0f;
				m_DecayFactor = (((f32)metadataSustain)/100.0f); 
				m_ReleaseFactor = 1.0f;
			}
			else if (timeSinceTriggered<m_AttackStartTime)
			{
				// We've not started yet
				m_EnvelopeState = AUD_ENVELOPE_NOT_STARTED;
				m_AttackFactor = 0.0f;
				m_DecayFactor = 1.0f; 
				m_ReleaseFactor = 1.0f;
			}
			else if (timeSinceTriggered<m_DecayStartTime)
			{
				// We're during the attack phase
				m_EnvelopeState = AUD_ENVELOPE_ATTACK;
				m_AttackFactor = state->curves[AUD_ATTACK_CURVE].CalculateRescaledValue(0.0f, 1.0f, 
					(f32)m_AttackStartTime, (f32)m_DecayStartTime, (f32)timeSinceTriggered);
				m_DecayFactor = 1.0f; 
				m_ReleaseFactor = 1.0f;
			}
			else if (timeSinceTriggered<m_DecayStopTime)
			{
				m_EnvelopeState = AUD_ENVELOPE_DECAY;
				f32 sustainFactor = (((f32)metadataSustain)/100.0f);
				m_AttackFactor = 1.0f;
				m_DecayFactor = state->curves[AUD_DECAY_CURVE].CalculateRescaledValue(sustainFactor, 1.0f, 
					(f32)m_DecayStartTime, (f32)m_DecayStopTime, (f32)timeSinceTriggered);
				m_ReleaseFactor = 1.0f;
			}
		}
	}
	return true;
}

#if __SOUND_DEBUG_DRAW
bool audEnvelopeSound::DebugDrawInternal(const u32 timeInMs, audDebugDrawManager &drawManager, const bool UNUSED_PARAM(drawDynamicChildren)) const
{
	const char *stateNames[] = {"Not Started","Attack","Decay","Sustain","Release","Finished"};
	char buf[64];
	
	const s32 state = m_EnvelopeState;
	
	const u32 playTimeMs = GetCurrentPlayTime(timeInMs);
	u32 nextStateChange = playTimeMs;

	switch(state)
	{
	case AUD_ENVELOPE_ATTACK:
		nextStateChange = m_DecayStartTime;
		break;
	case AUD_ENVELOPE_DECAY:
		nextStateChange = m_DecayStopTime;
		break;
	case AUD_ENVELOPE_SUSTAIN:
		nextStateChange = m_HoldStopTime;
		break;
	case AUD_ENVELOPE_RELEASE:
		nextStateChange = m_ReleaseStopTime;
			break;
	}

	u32 metadataAttack, metadataDecay;
	u32 metadataSustain;
	s32 metadataHold;
	s32 metadataRelease;
	GetMetadataEnvelope(metadataAttack, metadataDecay, metadataSustain, metadataHold, metadataRelease);

	const float stateTimeRemaining = (nextStateChange - playTimeMs) * 0.001f;
	
	if(state == AUD_ENVELOPE_SUSTAIN && metadataHold < 0)
	{
		formatf(buf, "- %s, (inf hold)", stateNames[state]);
	}
	else
	{
		formatf(buf, "- %s, %02.02f seconds remaining", stateNames[state], stateTimeRemaining);
	}
	drawManager.DrawLine(buf);
	return true;
}
#endif // __SOUND_DEBUG_DRAW

}
