// 
// squish/squishspu.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SQUISHSPU_H
#define SQUISHSPU_H

// For Squish flags
#include "squish/squish.h"

#include "system/task.h"
#include "system/taskheader.h"

namespace rage
{
	class grcTexture;
	class grcRenderTarget;

	// This must be kept alive for the duration of the conversion
	// Either call Wait once, or keep calling Poll until it returns true, then this structure is safe to free.
	class sqCompressState
	{
	public:
		sqCompressState() : m_Handle(NULL), m_AlphaThreshold(128) { }
		~sqCompressState() { AssertMsg(!m_Handle,"CompressState destroyed without waiting for task completion!"); Wait(); }
		
		// NOTE - The format of dstTexture is used (and must be in DXT1/3/5 format) regardless of what squishFlags indicates.
		// void Compress(rage::grcTexture* srcTexture, rage::grcTexture* dstTexture, int squishFlags, int schedulerIndex);
		void Compress(grcTexture* srcTexture, grcTexture* dstTexture, int squishFlags, u8 alphaThreshold, int schedulerIndex);

		// NOTE - The format of dstTexture is used (and must be in DXT1/3/5 format) regardless of what squishFlags indicates.
		// void Compress(rage::grcTexture* srcTexture, rage::grcTexture* dstTexture, int squishFlags, int schedulerIndex);
		void Compress(grcTexture* srcTexture, grcTexture* dstTexture, int squishFlags, int schedulerIndex);

		// NOTE - This function actually needs the correct compression format in squishFlags
		void CompressMips(grcTexture* srcTexture, char** dstOffsets, u32* dstStrides, s32 numMips, s32 width, s32 height, int squishFlags, int schedulerIndex);

		// NOTE - This function actually needs the correct compression format in squishFlags
		void CompressMips(grcTexture* srcTexture, char** dstOffsets, u32* dstStrides, s32 numMips, s32 width, s32 height, int squishFlags, u8 alphaThreshold, int schedulerIndex);

		void Wait() { 
			while (m_Handle) { 
				sysTaskManager::Wait(m_Handle); 
				NextSlice(); 
			} 
		}
		void Abort() {
			if (m_Handle)
				sysTaskManager::Wait(m_Handle);
			m_Handle = NULL;
		}
		// Call this at least once per frame to kick the next slice of work
		bool Poll() { 
			if (!m_Handle)
				return true;
			else if (sysTaskManager::Poll(m_Handle)) { 
				NextSlice(); 
				return !m_Handle;	// We're done only if last subtask is done.
			} 
			else 
				return false; 
		}
	private:
		void NextSlice();
		sysTaskParameters m_Params;
		sysTaskHandle m_Handle;
		int m_CurMip, m_MipCount, m_CurSlice, m_SchedIndex;
		int m_Width, m_Height;
		char *m_Dest[12];
		char *m_Src[12];
		int m_SrcPitch[12];
		int m_DestPitch[12];
		int m_Steps[12];
		int m_Stops[12];
		u8 m_AlphaThreshold;
		ATTR_UNUSED u8 m_Padding[3];
#if __WIN32PC || RSG_DURANGO || RSG_ORBIS
		grcTexture *m_DestTex, *m_SrcTex;
#endif
	};

	// alternate compression techniques for non SPU compression
	#define SQUISH_USE_STB_COMPRESSION		(0 && !__XENON)  // 5x faster than XGCompressSurface(), this should work on other platforms, just has not been tested. there is additional overhead on the first call to stb_compress_dxt_block().
	#define SQUISH_USE_FASTVMX_COMPRESSION	(1 && __XENON )  // 30x faster than XGCompressSurface(), just on XENON for now, (derived from sample code)
	#define SQUISH_USE_INLINE_UNTILE		(1 && __XENON && (SQUISH_USE_STB_COMPRESSION || SQUISH_USE_FASTVMX_COMPRESSION))    // untli as we compress (360 specific)

} // namespace rage

#endif // SQUISHSPU_H
