PROJECT = squish

INCLUDES = -I ..\..\..\..\ragestlport\STLport-5.0RC5\stlport -I ..
DEFINES = \

OBJS = \
	$(INTDIR)/squishspu.job.obj	\
	$(INTDIR)/squishspu.obj	\
	$(INTDIR)/squish.obj	\
	$(INTDIR)/singlecolourfit.obj	\
	$(INTDIR)/rangefit.obj	\
	$(INTDIR)/maths.obj	\
	$(INTDIR)/colourset.obj	\
	$(INTDIR)/colourfit.obj	\
	$(INTDIR)/colourblock.obj	\
	$(INTDIR)/clusterfit.obj	\
	$(INTDIR)/alpha.obj	\


include ..\..\..\..\rage\build\Makefile.template

$(INTDIR)/squishspu.job.obj: ./squishspu.job
	call ..\..\..\..\rage\build\make_spurs_job_pb $(subst /,\,$(abspath $(subst \,/,.))) $(INTDIR) $(basename squishspu.job) "$(INTDIR)"

$(INTDIR)/squishspu.obj: ./squishspu.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/squish.obj: ./squish.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/singlecolourfit.obj: ./singlecolourfit.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/rangefit.obj: ./rangefit.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/maths.obj: ./maths.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/colourset.obj: ./colourset.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/colourfit.obj: ./colourfit.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/colourblock.obj: ./colourblock.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/clusterfit.obj: ./clusterfit.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/alpha.obj: ./alpha.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

HEADERS: $(HEADERS)
