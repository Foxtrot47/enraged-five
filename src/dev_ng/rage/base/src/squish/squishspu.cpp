// 
// squish/squishspu.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "squishspu.h"
#include "system/cache.h"

#if RSG_ORBIS
#include "grcore/texture_gnm.h"
#endif

#if RSG_DURANGO
#include "grcore/texture_durango.h"
#endif

#if SQUISH_USE_STB_COMPRESSION	
#include "grcore/image_dxt.h"
#define STB_DXT_IMPLEMENTATION
#include "stb/stb_dxt.h"
#endif

#if __PS3
enum SpuUserData
{
	kWidth,
	kFlags,
	kSrcPitch,
	kDstPitch,
	kSliceHeight,
	kAlphaThreshold,
	kUserDataCount
};
#elif RSG_ORBIS || RSG_DURANGO
enum SpuUserData
{
	kWidth,
	kHeight,
	kFlags,
	kSrcTexture,
	kDstTexture,
	kSrcPitch,
	kDstPitch,
	kSliceStart,
	kSliceHeight,
	kAlphaThreshold,
	kUserDataCount
};
#else
enum SpuUserData
{
	kWidth,
	kHeight,
	kFlags,
	kSrcPitch,
	kDstPitch,
	kSliceStart,
	kSliceHeight,
	kAlphaThreshold,
	kUserDataCount
};
#endif

#if	SQUISH_USE_FASTVMX_COMPRESSION
#define SIZE_OF_SLICE_IN_BYTES 65536 // fast VMX compression is 20-30x faster, so we can do more per a time.
#else
#define SIZE_OF_SLICE_IN_BYTES 16384
#endif

#if __SPU
#include "math/amath.h"

#include "system/taskheader.h"

// Squish implementation
#include "alpha.cpp"
#include "clusterfit.cpp"
#include "colourblock.cpp"
#include "colourfit.cpp"
#include "colourset.cpp"
#include "maths.cpp"
#include "rangefit.cpp"
#include "singlecolourfit.cpp"
#include "squish.cpp"

using namespace rage;

DECLARE_TASK_INTERFACE(squishspu);

void squishspu(rage::sysTaskParameters& taskParams)
{
	const int flags = taskParams.UserData[kFlags].asInt;

	u8 *srcStorage = (u8*)taskParams.Input.Data;
	int width = taskParams.UserData[kWidth].asInt;
	int srcPitch = taskParams.UserData[kSrcPitch].asInt;
	int dstPitch = taskParams.UserData[kDstPitch].asInt;
	int sliceHeight = taskParams.UserData[kSliceHeight].asInt;
	u8 alphaThreshold = (u8)taskParams.UserData[kAlphaThreshold].asUInt;

	// Swap ARGB->BGRA
	// 10xxxxxx	0x00
	// 110xxxxx	0xFF
	// 111xxxxx	0x80
	// 	otherwise the byte of (a||b) addressed by the rightmost 5 bits of pattern
	// change elements 3/7/11/15 below to 0xC0 to force alpha to 1.0
#if 0
	vec_uchar16 reverseControlDXT1 = { 3, 2, 1, 0xC0, 7, 6, 5, 0xC0, 11, 10, 9, 0xC0, 15, 14, 13, 0xC0 };
	vec_uchar16 reverseControlDXT5 = { 3, 2, 1, 0, 7, 6, 5, 4, 11, 10, 9, 8, 15, 14, 13, 12 };
	vec_uchar16 reverseControl = (flags & squish::kDxt1)? reverseControlDXT1 : reverseControlDXT5;
#else
	vec_uchar16 reverseControl = { 3, 2, 1, 0, 7, 6, 5, 4, 11, 10, 9, 8, 15, 14, 13, 12 };
#endif
	vec_uint4* __restrict__ row = reinterpret_cast<vec_uint4*>(srcStorage);
	int qwordWidth = (srcPitch * sliceHeight) >> 4;
	for (int qwordIdx = 0; qwordIdx < qwordWidth; ++qwordIdx)
		row[qwordIdx] = spu_shuffle(row[qwordIdx], row[qwordIdx], reverseControl);
	
	squish::CompressImage(srcStorage,width,sliceHeight,srcPitch,dstPitch,taskParams.Output.Data,flags,alphaThreshold);
}

#elif __WIN32PC || RSG_DURANGO

using namespace rage;

void squishspu(rage::sysTaskParameters& taskParams)
{
	const int width = taskParams.UserData[kWidth].asInt;
	// const int height = taskParams.UserData[kHeight].asInt;
	const int flags = taskParams.UserData[kFlags].asInt;

	int srcPitch = taskParams.UserData[kSrcPitch].asInt;
	int dstPitch = taskParams.UserData[kDstPitch].asInt;
	int sliceStart = taskParams.UserData[kSliceStart].asInt;
	int sliceHeight = taskParams.UserData[kSliceHeight].asInt;
	u8 alphaThreshold = (u8)taskParams.UserData[kAlphaThreshold].asUInt;

	u8 swappedData[SIZE_OF_SLICE_IN_BYTES];
	const u8 *src = (u8*)taskParams.Input.Data + (srcPitch * sliceStart);
	u32 numBytes = srcPitch * sliceHeight;
	for (u32 i=0; i<numBytes; i+=4) {
		swappedData[i] = src[i+2];
		swappedData[i+1] = src[i+1];
		swappedData[i+2] = src[i+0];
		swappedData[i+3] = src[i+3];
	}

	squish::CompressImage(swappedData,
		width,
		sliceHeight,
		srcPitch,
		dstPitch,
		(u8*)taskParams.Output.Data + ((dstPitch * sliceStart) >> 2),
		flags,
		alphaThreshold);

#if RSG_DURANGO
	//when we've finished the whole mip the we need to tile it into place
	if (sliceStart + sliceHeight == taskParams.UserData[kHeight].asInt)
	{
		grcTextureDurango* pDurangoTextureDst = reinterpret_cast<grcTextureDurango*> (taskParams.UserData[kDstTexture].asPtr);
		pDurangoTextureDst->TileInPlace();
	}
#endif
}

void dummytask(rage::sysTaskParameters&)
{
}

#elif RSG_ORBIS

using namespace rage;

void squishspu(rage::sysTaskParameters& taskParams)
{
	const int width = taskParams.UserData[kWidth].asInt;
	const int flags = taskParams.UserData[kFlags].asInt;

	int srcPitch = taskParams.UserData[kSrcPitch].asInt;
	int dstPitch = taskParams.UserData[kDstPitch].asInt;
	int sliceStart = taskParams.UserData[kSliceStart].asInt;
	int sliceHeight = taskParams.UserData[kSliceHeight].asInt;
	u8 alphaThreshold = (u8)taskParams.UserData[kAlphaThreshold].asUInt;

	const u8 *src = (u8*)taskParams.Input.Data + (srcPitch * sliceStart);

	grcTexture* pSrcTex = reinterpret_cast<grcTexture*> (taskParams.UserData[kSrcTexture].asPtr);
	const sce::Gnm::Texture* srcGnmTexture = reinterpret_cast<const sce::Gnm::Texture*>(pSrcTex->GetTexturePtr());

	if (srcGnmTexture->getTileMode() != sce::Gnm::kTileModeDisplay_LinearAligned)
	{
		// Forcing the source render target to have a linear aligned tile mode for now.
		// In order to detile a render target with a 2D tile mode, we have to detile the entire texture in one
		//  pass rather than split it into smaller slices.

		//AssertMsg(false,"squishspu - Source render target tile mode must be linear aligned");
		return;

		// Working detile code below if we ever decide to either compress the texture in one pass.

		//sce::GpuAddress::TilingParameters tp;
		//tp.initFromTexture(srcGnmTexture, 0, 0);
		//tp.m_linearHeight = sliceHeight;

		//u8 untiledPixels[sizeOfSliceInBytes];
		//sce::GpuAddress::detileSurface(untiledPixels, src, &tp);

		//squish::CompressImage(untiledPixels,
		//						width,
		//						sliceHeight,
		//						srcPitch,
		//						dstPitch,
		//						(u8*)taskParams.Output.Data + (((dstPitch * sliceStart)) >> 2),
		//						flags,
		//						alphaThreshold);
	}
	else
	{
		squish::CompressImage(	src,
								width,
								sliceHeight,
								srcPitch,
								dstPitch,
								(u8*)taskParams.Output.Data + (((dstPitch * sliceStart)) >> 2),
								flags,
								alphaThreshold);
	}

	//when we've finished the whole mip the we need to tile it into place
	if (sliceStart + sliceHeight == taskParams.UserData[kHeight].asInt)
	{
		grcTextureGNM* pOrbisTextureDst = reinterpret_cast<grcTextureGNM*>(taskParams.UserData[kDstTexture].asPtr);
		Assert(pOrbisTextureDst != NULL);
		if (pOrbisTextureDst)
			pOrbisTextureDst->TileInPlace();
	}
}

#elif __XENON

#include "system/xtl.h"
#include "system/xgraphics.h"

namespace rage {
#if SQUISH_USE_STB_COMPRESSION
	void Compress_STB(BYTE * srcBase, BYTE * dstBase, int width, int height, int srcPitch, int dstPitch, int startY, int stopY, bool hasAlpha);
#elif SQUISH_USE_FASTVMX_COMPRESSION
	void Compress_FastVMX(BYTE * srcBase, BYTE * dstBase, int width, int height, int srcPitch, int dstPitch, int startY, int stopY, bool hasAlpha);
#endif
};

using namespace rage;

void squishspu(rage::sysTaskParameters& taskParams)
{
	const int width = taskParams.UserData[kWidth].asInt;
	const int height = taskParams.UserData[kHeight].asInt;
	const int flags = taskParams.UserData[kFlags].asInt;

	int srcPitch = taskParams.UserData[kSrcPitch].asInt;
	int dstPitch = taskParams.UserData[kDstPitch].asInt;
	int sliceStart = taskParams.UserData[kSliceStart].asInt;
	int sliceHeight = taskParams.UserData[kSliceHeight].asInt;


	// These routines allocate at most about 8k internally (via operator new) to process a 128x128 texture.
	// It may go up a bit for larger textures, but not by too much because we adjust our slice size smaller
	// for larger textures.
	extern __THREAD int RAGE_LOG_DISABLE;
	++RAGE_LOG_DISABLE;

	// On 360 the task system doesn't really DMA inputs and outputs.
	// Keep in mind we want to do small amounts of work here though because it allocates XTL memory 
	// internally temporarily

#if 0
	// Byte swap and red/blue swap
	unsigned char ALIGNAS(16) reverseControl[] = { 1, 2, 3, 0,  5, 6, 7, 4,  9, 10, 11, 8,  13, 14, 15, 12 };
	__vector4 * row = reinterpret_cast<__vector4*>((u8*)taskParams.Input.Data + (srcPitch * sliceStart));
	int qwordWidth = (srcPitch * sliceHeight) >> 4;
	for (int qwordIdx = 0; qwordIdx < qwordWidth; ++qwordIdx)
		row[qwordIdx] = __vperm(row[qwordIdx], __vspltisw(-1) /*row[qwordIdx]*/, *(__vector4*)reverseControl);
	squish::CompressImage((u8*)taskParams.Input.Data + (srcPitch * sliceStart),
		width,
		sliceHeight,
		srcPitch,
		dstPitch,
		(u8*)taskParams.Output.Data + ((dstPitch * sliceStart) >> 2),
		flags,
		alphaThreshold);
#if __DEV	// restore damage done to input texture (rotate three more times, dull)
	for (int qwordIdx = 0; qwordIdx < qwordWidth; ++qwordIdx)
		row[qwordIdx] = __vperm(row[qwordIdx], __vspltisw(-1) /*row[qwordIdx]*/, *(__vector4*)reverseControl);
	for (int qwordIdx = 0; qwordIdx < qwordWidth; ++qwordIdx)
		row[qwordIdx] = __vperm(row[qwordIdx], __vspltisw(-1) /*row[qwordIdx]*/, *(__vector4*)reverseControl);
	for (int qwordIdx = 0; qwordIdx < qwordWidth; ++qwordIdx)
		row[qwordIdx] = __vperm(row[qwordIdx], __vspltisw(-1) /*row[qwordIdx]*/, *(__vector4*)reverseControl);
#endif
	// word swap the output
	unsigned char ALIGNAS(16) reverseControl2[] = { 1, 0, 3, 2,  5, 4, 7, 6,  9, 8, 11, 10,  13, 12, 15, 14 };
	row = (__vector4*) ((u8*)taskParams.Output.Data + ((dstPitch * sliceStart) >> 2));
	for (int qwordIdx = 0; qwordIdx < qwordWidth; ++qwordIdx)
		row[qwordIdx] = __vperm(row[qwordIdx], __vspltisw(-1) /*row[qwordIdx]*/, *(__vector4*)reverseControl2);
#else

	#if SQUISH_USE_FASTVMX_COMPRESSION
		Compress_FastVMX(	(BYTE *)taskParams.Input.Data, 
							(BYTE *)taskParams.Output.Data, 
							width, 
							height, 
							srcPitch,
							dstPitch,
							sliceStart, 
							sliceStart + sliceHeight,
							flags & squish::kDxt1? 0 : 1);

	#elif SQUISH_USE_STB_COMPRESSION
		Compress_STB(	(BYTE *)taskParams.Input.Data, 
						(BYTE *)taskParams.Output.Data, 
						width, 
						height, 
						srcPitch,
						dstPitch,
						sliceStart, 
						sliceStart + sliceHeight,
						flags & squish::kDxt1? 0 : 1);

	#else  // just use XDK (slow)
		POINT dstPoint = { 0, sliceStart };
		RECT srcRect = { 0, sliceStart, width, sliceStart + sliceHeight };
		HRESULT result = XGCompressSurface( taskParams.Output.Data,
											dstPitch,
											width, 
											height,
											flags & squish::kDxt1? D3DFMT_LIN_DXT1 : D3DFMT_LIN_DXT5,
											&dstPoint,
											taskParams.Input.Data,
											srcPitch,
											D3DFMT_LIN_A8R8G8B8,
											&srcRect,
											XGCOMPRESS_NO_DITHERING,
											0.0f );
		if (result != S_OK)
			Errorf("squishspu() - XGCompressSurface failed!");
	#endif
#endif

	// We need to untile the texture when the entire miplevel is done
	// because compression writes to different parts of the texture
	// (for example, compressing the top quarter of a DXT1 writes
	// to the top quarter of the destination; but compressing the
	// top-middle quarter writes to the middle-bottom quarter!)
	if (sliceStart + sliceHeight == height && ((flags&squish::kDxt3)==0)) { // skip tiling on 360 is dxt3 is used (not realy dxt3, just a flag)
		XGTileSurface( taskParams.Output.Data,
			width >> 2,
			height >> 2,
			NULL,
			taskParams.Output.Data,
			dstPitch,
			NULL,
			(flags & squish::kDxt1)? 8 : 16);
		// If we don't flush the cache we may leave little speckles
		WritebackDC(taskParams.Output.Data,(dstPitch * height) >> 2);
	}

	--RAGE_LOG_DISABLE;

	// TODO: Tile the result!
}
#endif


#if !__SPU

#include "grcore/texture.h"

#include "system/nelem.h"
#include "system/task.h"
#include "system/taskheader.h"

DECLARE_TASK_INTERFACE(squishspu);

namespace rage
{


void sqCompressState::Compress(grcTexture *src,grcTexture *dst, int squishFlags, int schedulerIndex)
{
	Compress(src, dst, squishFlags, 128, schedulerIndex);
}

void sqCompressState::Compress(grcTexture *src,grcTexture *dst, int squishFlags, u8 alphaThreshold, int schedulerIndex)
{
	using namespace squish;
	Assert(src != NULL);
	if (src == NULL)
		return;
	Assert(dst != NULL);
	if (dst == NULL)
		return;

	Assert(src->GetBitsPerPixel() == 32);
	Assert(dst->GetBitsPerPixel() <= 8);
	Assert(src->GetWidth() == dst->GetWidth());
	Assert(src->GetHeight() == dst->GetHeight());
	AssertMsg(!m_Handle,"Starting new compress before previous one finished?");

#if __WIN32PC || RSG_DURANGO || RSG_ORBIS
	m_SrcTex = src;
	m_DestTex = dst;
#endif

	int textureFormat = 0;
	int mipMapCount = Min(src->GetMipMapCount(),dst->GetMipMapCount());
	m_Width = src->GetWidth();
	m_Height = src->GetHeight();
	m_AlphaThreshold = alphaThreshold;

	for (int mipIdx = 0; mipIdx < mipMapCount; ++mipIdx) 
	{
		grcTextureLock srcLock;

		if (src->LockRect(0,mipIdx,srcLock,grcsRead)) 
		{
			if (!textureFormat) 
			{
				textureFormat = srcLock.RawFormat;
				PS3_ONLY(AssertMsg((textureFormat & CELL_GCM_TEXTURE_LN), "Source texture must be linear"));
			}

			m_Src[mipIdx] = (char*) srcLock.Base;
			m_SrcPitch[mipIdx] = srcLock.Pitch;
			m_Stops[mipIdx] = srcLock.Height;
			// Displayf("src mip %d %p p=%d w=%d h=%d",mipIdx,srcLock.Base,srcLock.Pitch,srcLock.Width,srcLock.Height);

			// We try to handle exactly 16k of source data per call.  If that's too slow, we can use a smaller numerator instead.
			m_Steps[mipIdx] = (SIZE_OF_SLICE_IN_BYTES) / srcLock.Pitch;

			if (m_Steps[mipIdx] > m_Stops[mipIdx])
				m_Steps[mipIdx] = m_Stops[mipIdx];

#if SQUISH_USE_INLINE_UNTILE
				// make sure we doe multiple of 4 at a time
				Assertf((m_Steps[mipIdx]&0x3)==0,"need to step in multiples of 4 for fast compressors");
#endif
			src->UnlockRect(srcLock);
		}
		else
			grcErrorf("LockRect of src mip %d failed",mipIdx);

#if __WIN32PC || RSG_DURANGO	// On PC we defer the lock as long as possible, to NextSlice
		m_Dest[mipIdx] = NULL;
#else
		grcTextureLock dstLock;
		if (dst->LockRect(0,mipIdx,dstLock,grcsWrite|grcsAllowVRAMLock))
		{
			m_Dest[mipIdx] = (char*) dstLock.Base;
			m_DestPitch[mipIdx] = dstLock.Pitch;
			dst->UnlockRect(dstLock);
		}
		else
			grcErrorf("LockRect of dst mip %d failed",mipIdx);
#endif
	}

	// Ignore any format bits in the flags, deduce from actual destination texture
	squishFlags &= ~(squish::kDxt1 | squish::kDxt3 | squish::kDxt5);
#if __XENON
	XGTEXTURE_DESC surfaceDescription;
	XGGetTextureDesc(dst->GetTexturePtr(), 0, &surfaceDescription);
	if (surfaceDescription.Format==D3DFMT_LIN_DXT4 || surfaceDescription.Format==D3DFMT_LIN_DXT5)
		squishFlags |= squish::kDxt3;  // on 360, repurpose the dxt3 flag and use if for dxt5Linear (no tiling needed at the end)
	else
#endif	
	if (dst->GetBitsPerPixel() == 4)
		squishFlags |= squish::kDxt1;
	else
		squishFlags |= squish::kDxt5;
	m_Params.UserData[kFlags].asInt = squishFlags;

	m_Params.UserDataCount = kUserDataCount;

	m_Params.ReadOnlyCount = 0;
	m_Params.Scratch.Size = 0;
	m_Params.SpuStackSize = 0;
	m_CurSlice = 0;
	m_CurMip = 0;
	m_MipCount = mipMapCount;
	m_SchedIndex = schedulerIndex;

	NextSlice();
}

void sqCompressState::CompressMips(grcTexture* src, char** dstOffsets, u32* dstStrides, s32 numMips, s32 width, s32 height, int squishFlags, int schedulerIndex)
{
	CompressMips(src, dstOffsets, dstStrides, numMips, width, height, squishFlags, 128, schedulerIndex);
}

void sqCompressState::CompressMips(grcTexture* src, char** dstOffsets, u32* dstStrides, s32 numMips, s32 ASSERT_ONLY(width), s32 ASSERT_ONLY(height), int squishFlags, u8 alphaThreshold, int schedulerIndex)
{
	using namespace squish;

	Assert(src->GetBitsPerPixel() == 32);
	Assert(src->GetWidth() == width);
	Assert(src->GetHeight() == height);
	AssertMsg(!m_Handle,"Starting new compress before previous one finished?");

	int textureFormat = 0;
	m_Width = src->GetWidth();
	m_Height = src->GetHeight();
	m_AlphaThreshold = alphaThreshold;

	for (int mipIdx = 0; mipIdx < numMips; ++mipIdx) 
	{
		grcTextureLock srcLock;

		if (src->LockRect(0,mipIdx,srcLock,grcsRead)) 
		{
			if (!textureFormat) 
			{
				textureFormat = srcLock.RawFormat;
				PS3_ONLY(AssertMsg((textureFormat & CELL_GCM_TEXTURE_LN), "Source texture must be linear"));
			}

			m_Src[mipIdx] = (char*) srcLock.Base;
			m_SrcPitch[mipIdx] = srcLock.Pitch;
			m_Stops[mipIdx] = srcLock.Height;
			// Displayf("src mip %d %p p=%d w=%d h=%d",mipIdx,srcLock.Base,srcLock.Pitch,srcLock.Width,srcLock.Height);

			// We try to handle exactly 16k of source data per call.  If that's too slow, we can use a smaller numerator instead.
			m_Steps[mipIdx] = 16384 / srcLock.Pitch;
			if (m_Steps[mipIdx] > m_Stops[mipIdx])
				m_Steps[mipIdx] = m_Stops[mipIdx];

			src->UnlockRect(srcLock);
		}
		else
			grcErrorf("LockRect of src mip %d failed",mipIdx);

		m_Dest[mipIdx] = (char*)dstOffsets[mipIdx];
		m_DestPitch[mipIdx] = dstStrides[mipIdx];
	}

	m_Params.UserData[kFlags].asInt = squishFlags;

	m_Params.UserDataCount = kUserDataCount;

	m_Params.ReadOnlyCount = 0;
	m_Params.Scratch.Size = 0;
	m_Params.SpuStackSize = 0;
	m_CurSlice = 0;
	m_CurMip = 0;
	m_MipCount = numMips;
	m_SchedIndex = schedulerIndex;

	NextSlice();
}

void sqCompressState::NextSlice()
{
	using namespace squish;

	if (m_Handle) {
		if (m_CurSlice >= m_Stops[m_CurMip]) {
			m_CurSlice = 0;
			if (++m_CurMip == m_MipCount) {
				m_Handle = NULL;
				return;
			}
		}
	}

#if RSG_PC || RSG_DURANGO
	grcTextureLock srcLock, dstLock;
	m_SrcTex->LockRect(0,m_CurMip,srcLock,grcsRead);
	m_Src[m_CurMip] = (char*) srcLock.Base;
	ASSERT_ONLY(bool dstLockedOk = )m_DestTex->LockRect(0,m_CurMip,dstLock,grcsWrite | grcsAllowVRAMLock);
	m_Dest[m_CurMip] = (char*) dstLock.Base;
	m_DestPitch[m_CurMip] = dstLock.Pitch;

	Assertf(dstLockedOk, "Failed to lock destination texture!");
#endif

	// We do up to 16k at a time
#if __PPU
	m_Params.Input.Data = m_Src[m_CurMip] + m_CurSlice * m_SrcPitch[m_CurMip];				// source pitch is in single scanline units
	m_Params.Input.Size = m_Steps[m_CurMip] * m_SrcPitch[m_CurMip];
	m_Params.Output.Data = m_Dest[m_CurMip] + (m_CurSlice>>2) * m_DestPitch[m_CurMip];		// destination pitch is in four-scanline units
	m_Params.Output.Size = (m_Steps[m_CurMip] * m_DestPitch[m_CurMip]) >> 2;						// destination pitch is in four-scanline units
#else
	m_Params.Input.Data = m_Src[m_CurMip];
	m_Params.Input.Size = 16;		// not actually used
	m_Params.Output.Data = m_Dest[m_CurMip];
	m_Params.Output.Size = 16;		// not actually used
	m_Params.UserData[kSliceStart].asInt = m_CurSlice;
	m_Params.UserData[kHeight].asInt = Max(m_Height>>m_CurMip,1);
#endif
	m_Params.UserData[kWidth].asInt = Max(m_Width>>m_CurMip,1);
	m_Params.UserData[kSrcPitch].asInt = m_SrcPitch[m_CurMip];
	m_Params.UserData[kDstPitch].asInt = m_DestPitch[m_CurMip];
	m_Params.UserData[kSliceHeight].asInt = m_Steps[m_CurMip];
	m_Params.UserData[kAlphaThreshold].asUInt = m_AlphaThreshold;

#if RSG_ORBIS || RSG_DURANGO
	m_Params.UserData[kSrcTexture].asPtr = m_SrcTex;
	m_Params.UserData[kDstTexture].asPtr = m_DestTex;
#endif //RSG_ORBIS || RSG_DURANGO

	// Displayf("NextSlice: %p->%p curmip %d srcpitch %d destpitch %d curslice %d curstep %d",m_Params.Input.Data,m_Params.Output.Data,m_CurMip,m_SrcPitch[m_CurMip],m_DestPitch[m_CurMip],m_CurSlice,m_Steps[m_CurMip]);

	m_CurSlice += m_Steps[m_CurMip];
	m_Handle = sysTaskManager::Create(TASK_INTERFACE(squishspu), m_Params, m_SchedIndex);
#if RSG_PC || RSG_DURANGO
	// Holding a lock across frames is bad news.  We need something better here but let's wait until DX11 to figure that out.
	// Probably a dummy surface that we blit to the real target when done.  Or maybe even just let the DirectX runtime do
	// the conversion during the blit, if possible
	sysTaskManager::Wait(m_Handle);
	m_SrcTex->UnlockRect(srcLock);
#if !RSG_PC
	m_DestTex->UnlockRect(dstLock);
#endif
	m_Handle = sysTaskManager::Create(TASK_INTERFACE(dummytask), m_Params, m_SchedIndex);
#endif
}

//
//
// Alternate compressors for XENON, much (3-10x faster than the XDK calls). 
// they also do the untiling as they go, saving time
// NOTE: these other options should go in there own files, but for now...
//
//

#if SQUISH_USE_INLINE_UNTILE
	// modified XGAddress2DTiledOffset with fixed texel pitch
	inline UINT Squish_Fast_XGAddress2DTiledOffset(const UINT x,const UINT y,const UINT Width/*,TexelPitch*/)
	{
		const UINT fixedLog = 2;

		Assert(x < Width);
		Assert(Width == ((Width + 31) & ~31));
	//	Assert(XGLog2LE16(TexelPitch) == fixedLog);

		const UINT Macro = ((x >> 5) + (y >> 5) * (Width >> 5)) << (fixedLog + 7);
		const UINT Micro = (((x & 7) + ((y & 6) << 2)) << fixedLog);
		const UINT Offset = Macro + ((Micro & ~15) << 1) + (Micro & 15) + ((y & 8) << (3 + fixedLog)) + ((y & 1) << 4);

		const UINT result = (((Offset & ~511) << 3) + ((Offset & 448) << 2) + (Offset & 63) + 
			((y & 16) << 7) + (((((y & 8) >> 2) + (x >> 3)) & 3) << 6)) >> fixedLog;
		return result;
	}
#endif

#if SQUISH_USE_STB_COMPRESSION  // 5x faster than the system call (more if you consider it does not have to until first)

#if RSG_PC || RSG_DURANGO
#include "grcore/texture_d3d11.h"
#include "grcore/rendertarget_d3d11.h"
#endif

enum {kLinesPerFrame=8}; // must be a multiple of 4

void Compress_STB(BYTE * srcBase, BYTE * dstBase, int w, int h, int srcPitch, int dstPitch, int startY, int stopY, bool hasAlpha)
{
	bool bIsDXT5 = hasAlpha;
	const int dstStep = (16*(bIsDXT5 ? 8 : 4))/(8*4); // block size divided by 4
	if (stopY>h)
		stopY = h;

	for ( int y = startY;y < stopY; y += 4)
	{
		const int by = y / 4;
		u8* dstRow = dstBase + (by * dstPitch);

		const u8* srcRow0 = (const u8*)srcBase + (y + 0) * srcPitch;
		const u8* srcRow1 = (const u8*)srcBase + (y + 1) * srcPitch;
		const u8* srcRow2 = (const u8*)srcBase + (y + 2) * srcPitch;
		const u8* srcRow3 = (const u8*)srcBase + (y + 3) * srcPitch;

		for (int x = 0; x < w; x += 4)
		{
			DXT::ARGB8888 temp[16];

#if SQUISH_USE_INLINE_UNTILE
			srcRow0 = (const u8*)srcBase + 4*Squish_Fast_XGAddress2DTiledOffset(x,y+0,w);
			srcRow1 = srcRow0+16; // the tiled pixels are in groups of 4x2 blocks
			srcRow2 = (const u8*)srcBase + 4*Squish_Fast_XGAddress2DTiledOffset(x,y+2,w);
			srcRow3 = srcRow2+16; 
			static const int xoffset = 0;
#else
			const int xoffset = x;
#endif
			temp[0x00] = ((const DXT::ARGB8888*)srcRow0)[xoffset + 0];
			temp[0x01] = ((const DXT::ARGB8888*)srcRow0)[xoffset + 1];
			temp[0x02] = ((const DXT::ARGB8888*)srcRow0)[xoffset + 2];
			temp[0x03] = ((const DXT::ARGB8888*)srcRow0)[xoffset + 3];
			temp[0x04] = ((const DXT::ARGB8888*)srcRow1)[xoffset + 0];
			temp[0x05] = ((const DXT::ARGB8888*)srcRow1)[xoffset + 1];
			temp[0x06] = ((const DXT::ARGB8888*)srcRow1)[xoffset + 2];
			temp[0x07] = ((const DXT::ARGB8888*)srcRow1)[xoffset + 3];
			temp[0x08] = ((const DXT::ARGB8888*)srcRow2)[xoffset + 0];
			temp[0x09] = ((const DXT::ARGB8888*)srcRow2)[xoffset + 1];
			temp[0x0a] = ((const DXT::ARGB8888*)srcRow2)[xoffset + 2];
			temp[0x0b] = ((const DXT::ARGB8888*)srcRow2)[xoffset + 3];
			temp[0x0c] = ((const DXT::ARGB8888*)srcRow3)[xoffset + 0];
			temp[0x0d] = ((const DXT::ARGB8888*)srcRow3)[xoffset + 1];
			temp[0x0e] = ((const DXT::ARGB8888*)srcRow3)[xoffset + 2];
			temp[0x0f] = ((const DXT::ARGB8888*)srcRow3)[xoffset + 3];


#if !RSG_PC && !RSG_DURANGO
	#if __XENON
			// 	 argb -> rgba
			for (int i = 0; i < 16; i++)
			{
				const u8 tmp = temp[i].a;
				temp[i].a = temp[i].r;
				temp[i].r = temp[i].g;
				temp[i].g = temp[i].b;
				temp[i].b = tmp;
			}
	#else
			// argb -> bgra
			for (int i = 0; i < 16; i++)
			{
				const u8 tmp1 = temp[i].a;
				temp[i].a = temp[i].b;
				temp[i].b = tmp1;
				const u8 tmp2 = temp[i].r;
				temp[i].r = temp[i].g;
				temp[i].g = tmp2;
			}
	#endif
#endif
			stb_compress_dxt_block((unsigned char *)(dstRow + x * dstStep), (const unsigned char *)temp, (int)hasAlpha, /*STB_DXT_HIGHQUAL*/ STB_DXT_NORMAL);
		}
	}
}

#endif // SQUISH_USE_STB_COMPRESSION 



#if SQUISH_USE_FASTVMX_COMPRESSION  // 30x fater than XGCompressSurface(), (based on XDK sample code + until on the fly) 
//
// Snipped from the XDK sample FastBlockCompress, plus until just in time code added.
//

//---------------------------------------------------------------------------------------------------------
// FastBlockCompress.h
//
// XNA Developer Connection
// Copyright ( C ) Microsoft Corporation. All rights reserved.
//---------------------------------------------------------------------------------------------------------

#include <xgraphics.h>
//---------------------------------------------------------------------------------------------------------
// Convenient texture descriptor
//---------------------------------------------------------------------------------------------------------
struct TextureDescAndBaseAddress
{
	XGTEXTURE_DESC Desc;
	UINT BaseAddress;
};

//---------------------------------------------------------------------------------------------------------
// FastBlockCompressVMX.cpp
//
// This file is designed to be a nearly standalone module, which could be cut-and-pasted into
// title code without significant dependencies.
//
// XNA Developer Connection
// Copyright ( C ) Microsoft Corporation. All rights reserved.
//---------------------------------------------------------------------------------------------------------

#include <xtl.h>    // must come before the others
#include <assert.h>
#include <VectorIntrinsics.h>

//---------------------------------------------------------------------------------------------------------
// Specifier for reading a single channel out of a 2- or 4-channel texel
//---------------------------------------------------------------------------------------------------------
enum CHANNEL
{
	CHANNEL_ALPHA, 
	CHANNEL_RED, 
	CHANNEL_GREEN, 
	CHANNEL_BLUE, 

	CHANNEL_COUNT
};


//---------------------------------------------------------------------------------------------------------
// VMX Compression functions for full D3D texture formats
//---------------------------------------------------------------------------------------------------------
typedef VOID ( *BlockFn )( BYTE *pDst, 
	__vector4 vSrc0123, __vector4 vSrc4567, __vector4 vSrc89ab, __vector4 vSrccdef );



//---------------------------------------------------------------------------------------------------------
// Helper functions.  
//---------------------------------------------------------------------------------------------------------
template <typename t_type> static inline t_type RoundDownToPowerOf2( const t_type& t, DWORD dwPowerOf2 )
{
	return ( t_type )( ( ( DWORD )t ) & ~( dwPowerOf2 - 1 ) );
}

template <typename t_type> static inline t_type RoundUpToPowerOf2( const t_type& t, DWORD dwPowerOf2 )
{
	return ( t_type )( ( ( ( DWORD )t ) + ( dwPowerOf2 - 1 ) ) & ~( dwPowerOf2 - 1 ) );
}


//---------------------------------------------------------------------------------------------------------
// Kick data into the cache.  
//---------------------------------------------------------------------------------------------------------
static void Prefetch( const BYTE* pData, DWORD dwSize )
{
	static const DWORD g_dwCacheLineSize = 128;

	// Expand input range to be cache-line aligned:
	const BYTE* pEnd = pData + dwSize;
	pData = RoundDownToPowerOf2( pData, g_dwCacheLineSize );
	dwSize = RoundUpToPowerOf2( pEnd, g_dwCacheLineSize ) - pData;

	for( size_t i = 0; i < dwSize; i += g_dwCacheLineSize )
	{
		__dcbt( i, pData );
	}
}


#pragma warning( push )
#pragma warning( disable:4700 ) // use of uninitialized variable in __vpkd3d, __vrlimi

//---------------------------------------------------------------------------------------------------------
// Helper functions for VMX compressors.  These are all force-inlined, so that
// the algorithms are globally optimized.  They are separated into functions
// for clarity and re-usability.
//---------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------
// Name: ConditionalExchange( )
// Desc: If vCondition is true, then interchange vA and vB.  Implemented branchlessly as two __vsel
// instructions.  This function is used to switch the anchor order in DXT1 blocks, because the wrong
// anchor order implies a 1-bit alpha block.
//---------------------------------------------------------------------------------------------------------
__forceinline void ConditionalExchange( __vector4& vA, __vector4& vB, __vector4 vCondition )
{
	__vector4 vTemp = vA;
	vA = __vsel( vA, vB, vCondition );
	vB = __vsel( vB, vTemp, vCondition );
}


//---------------------------------------------------------------------------------------------------------
// Name: AbsDiffByte( )
// Desc: Input is two vectors, interpreted as 16-wide BYTEs.  Output is the absolute value of the 
// difference, for each BYTE.
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 AbsDiffByte( __vector4 a, __vector4 b )
{
	return __vsububs( __vmaxub( a, b ), __vminub( a, b ) );
}


//---------------------------------------------------------------------------------------------------------
// Name: HorizSumBytesInHalf( )
// Desc: Input and output are vectors, interpreted as 16-wide unsigned BYTEs.  
// Each element of the output is the sum of two BYTEs of the input:
// 
//  Out[ 0] = Out[ 1] = In[ 0] + In[ 1]
//  ...
//  Out[14] = Out[15] = In[14] + In[15]
// 
// On overflow, the additions saturate to 255.
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 HorizSumBytesInHalf( __vector4 a )
{
	// If the calling function contains a '__vspltisb( 8 )', compiler will optimize
	// this one away.  We use BYTE rather than HALF because the calling function
	// often already loads this constant.
	__vector4 vByteEight        = __vspltisb( 8 );
	a                           = __vaddubs( a, __vrlh( a, vByteEight ) );

	return a;
}


//---------------------------------------------------------------------------------------------------------
// Name: HorizSumRGBBytesInWord( )
// Desc: Input and output are vectors, interpreted as 16-wide BYTEs.  
// Each element of the output is the sum of three BYTEs of the input (ignoring a fourth BYTE):
// 
//  Out[ 0] = Out[ 1] = Out[ 2] = Out[ 3] = In[ 1] + In[ 2] + In[ 3]
//  ...
//  Out[12] = Out[13] = Out[14] = Out[15] = In[13] + In[14] + In[15]
// 
// On overflow, the additions saturate to 255.
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 HorizSumRGBBytesInWord( __vector4 a )
{
	// If the calling function contains a '__vspltisb( 8 )', compiler will optimize
	// this one away.  We use BYTE rather than HALF because the calling function
	// often already loads this constant.
	__vector4 vByteEight        = __vspltisb( 8 );
	__vector4 vWordNegSixteen   = __vspltisw( -16 );    // Positive 16 overflows __vspltisw
	a                           = __vslw( a, vByteEight ); // eliminate A from ARGB
	a                           = __vaddubs( a, __vrlh( a, vByteEight ) );
	a                           = __vaddubs( a, __vrlw( a, vWordNegSixteen ) );

	return a;
}


//---------------------------------------------------------------------------------------------------------
// Name: FindMinMaxARGB( )
// Desc: The inputs are 4 VMX registers, each consisting of 16 bytes, interpreted as 4 ARGB vectors.
// The outputs are vMinARGB and vMaxARGB, which contain respectively the min and max overall values 
// of ARGB, replicated 4 times into xyzw.  
//
// Both the FLOAT and BYTE algorithms call this routine, because it's more efficient to compute the
// min/max as bytes, and there's no advantage to using FLOAT.
//---------------------------------------------------------------------------------------------------------
__forceinline void FindMinMaxARGB( __vector4& vMinARGB, __vector4& vMaxARGB, 
	__vector4 vSrc0123, __vector4 vSrc4567, __vector4 vSrc89ab, __vector4 vSrccdef )
{
	__vector4 vARGBx4[4] = { vSrc0123, vSrc4567, vSrc89ab, vSrccdef };
	// vARGBx4[0]       [ A0 | R0 | G0 | B0 ][ A1 | R1 | G1 | B1 ][ A2 | R2 | G2 | B2 ][ A3 | R3 | G3 | B3 ]
	// vARGBx4[1]       [ A4 | R4 | G4 | B4 ][ A5 | R5 | G5 | B5 ][ A6 | R6 | G6 | B6 ][ A7 | R7 | G7 | B7 ]
	// vARGBx4[2]       [ A8 | R8 | G8 | B8 ][ A9 | R9 | G9 | B9 ][ Aa | Ra | Ga | Ba ][ Ab | Rb | Gb | Bb ]
	// vARGBx4[3]       [ Ac | Rc | Gc | Bc ][ Ad | Rd | Gd | Bd ][ Ae | Re | Ge | Be ][ Af | Rf | Gf | Bf ]

	__vector4 vMinARGBx4        = __vminub( __vminub( vARGBx4[0], vARGBx4[1] ), 
		__vminub( vARGBx4[2], vARGBx4[3] ) );
	__vector4 vMaxARGBx4        = __vmaxub( __vmaxub( vARGBx4[0], vARGBx4[1] ), 
		__vmaxub( vARGBx4[2], vARGBx4[3] ) );
	vMinARGB                    = vMinARGBx4;
	vMinARGB                    = __vminub( vMinARGB, __vsldoi( vMinARGB, vMinARGB, 4 ) );
	vMinARGB                    = __vminub( vMinARGB, __vsldoi( vMinARGB, vMinARGB, 8 ) );
	vMaxARGB                    = vMaxARGBx4;
	vMaxARGB                    = __vmaxub( vMaxARGB, __vsldoi( vMaxARGB, vMaxARGB, 4 ) );
	vMaxARGB                    = __vmaxub( vMaxARGB, __vsldoi( vMaxARGB, vMaxARGB, 8 ) );
}


//---------------------------------------------------------------------------------------------------------
// Name: FindMinMaxUVUV( )
// Desc: The inputs are 2 VMX registers, each consisting of 16 bytes, interpreted as 8 UV vectors.
// The outputs are vMinUVUV and vMaxUVUV, which contain respectively the min and max overall values 
// of UV, replicated 8 times.  
//
// Both the FLOAT and BYTE algorithms call this routine, because it's more efficient to compute the
// min/max as bytes, and there's no advantage to using FLOAT.
//---------------------------------------------------------------------------------------------------------
__forceinline void FindMinMaxUVUV( __vector4& vMinUVUV, __vector4& vMaxUVUV, 
	__vector4 vSrc01234567, __vector4 vSrc89abcdef )
{
	__vector4 vUVUVx8[2] = { vSrc01234567, vSrc89abcdef };
	// vUVUVx8[0]       [ u0 | v0 | u1 | v1 ][ u2 | v2 | u3 | v3 ][ u4 | v4 | u5 | v5 ][ u6 | v6 | u7 | v7 ]
	// vUVUVx8[1]       [ u8 | v8 | u9 | v9 ][ ua | va | ub | vb ][ uc | vc | ud | vd ][ ue | ve | uf | vf ]

	// Find the min/max UV
	// Replicate min/max for U to xz and for V to yw
	vMinUVUV                    = __vminub( vUVUVx8[0], vUVUVx8[1] );
	vMinUVUV                    = __vminub( vMinUVUV, __vsldoi( vMinUVUV, vMinUVUV, 8 ) );
	vMinUVUV                    = __vminub( vMinUVUV, __vsldoi( vMinUVUV, vMinUVUV, 4 ) );
	vMinUVUV                    = __vminub( vMinUVUV, __vsldoi( vMinUVUV, vMinUVUV, 2 ) );
	vMaxUVUV                    = __vmaxub( vUVUVx8[0], vUVUVx8[1] );
	vMaxUVUV                    = __vmaxub( vMaxUVUV, __vsldoi( vMaxUVUV, vMaxUVUV, 8 ) );
	vMaxUVUV                    = __vmaxub( vMaxUVUV, __vsldoi( vMaxUVUV, vMaxUVUV, 4 ) );
	vMaxUVUV                    = __vmaxub( vMaxUVUV, __vsldoi( vMaxUVUV, vMaxUVUV, 2 ) );
}


//---------------------------------------------------------------------------------------------------------
// Name: ExpandBYTEToFLOAT( )
// Desc: Input is a vector interpreted as 16-wide BYTEs.  Output is a vector interpreted as 4-wide FLOATS.
// The output components are equal to the first four values of the input:
//
// vARGBBYTE            [ A  | R  | G  | B  ][    |    |    |    ][    |    |    |    ][    |    |    |    ]
//
// IF t_bUnswap == TRUE:
// vRGBAFLOAT           [         A         ][         R         ][         G         ][         B         ]
//
// IF t_bUnswap == FALSE:
// vRGBAFLOAT           [         R         ][         G         ][         B         ][         A         ]
//
// The remaining 12 values of the input are not used.
//
// The vupkd3d VMX instruction has an automatic swap from ARGB to RGBA when using VPACK_D3DCOLOR.  
// The t_bUnswap parameter undoes this swap.  We do this primarily for clarity's sake --- for 
// for instance when the inputs actually represent UVUV, or AAAA.  
//
// For performance, it may be a better idea to retain the swapped order throughout the calculation.
//---------------------------------------------------------------------------------------------------------
template< BOOL t_bUnswap > 
__forceinline void ExpandBYTEToFLOAT( __vector4& vRGBAFLOAT, __vector4 vARGBBYTE )
{
	static CONST __vector4   UnpackOffset = XM_UNPACK_UNSIGNEDN_OFFSET( 8, 8, 8, 8 );
	static CONST __vector4   UnpackScale = XM_UNPACK_UNSIGNEDN_SCALE( 8, 8, 8, 8 );

	vRGBAFLOAT                  = __vupkd3d( vARGBBYTE, VPACK_D3DCOLOR );
	vRGBAFLOAT                  = __vmaddcfp( UnpackScale, vRGBAFLOAT, UnpackOffset );
#pragma warning( push )
#pragma warning( disable:4127 )   // conditional expression is constant
	if ( t_bUnswap )
#pragma warning( pop )
	{
		vRGBAFLOAT              = __vpermwi( vRGBAFLOAT, VPERMWI_CONST( 3, 0, 1, 2 ) );
	}
}


//---------------------------------------------------------------------------------------------------------
// Name: ExpandBYTEToFLOATRGBA( )
// Desc: Input is a vector interpreted as 16-wide BYTEs.  Output is 4 vectors, each interpreted as 4-wide 
// FLOATS.   The output components are equal to the values of the input.
//
// vARGBBYTE            [ A0 | R0 | G0 | B0 ][ A1 | R1 | G1 | B1 ][ A2 | R2 | G2 | B2 ][ A3 | R3 | G3 | B3 ]
//
// vRGBAFLOAT[0]        [         R0        ][         G0        ][         B0        ][         A0        ]
// vRGBAFLOAT[1]        [         R1        ][         G1        ][         B1        ][         A1        ]
// vRGBAFLOAT[2]        [         R2        ][         G2        ][         B2        ][         A2        ]
// vRGBAFLOAT[3]        [         R3        ][         G3        ][         B3        ][         A3        ]
//---------------------------------------------------------------------------------------------------------
__forceinline void ExpandBYTEToFLOATRGBA( __vector4 vRGBAFLOAT[4], __vector4 vARGBBYTE )
{
	for ( UINT i = 0; i < 4; ++i )
	{
		vARGBBYTE               = __vsldoi( vARGBBYTE, vARGBBYTE, 1 << 2 );
		ExpandBYTEToFLOAT<FALSE>( vRGBAFLOAT[i], vARGBBYTE );
	}
}


//---------------------------------------------------------------------------------------------------------
// Name: ExpandBYTEToFLOATRGBA( )
// Desc: Input is 4 vectors interpreted as 16-wide BYTEs.  Output is 16 vectors, each interpreted as 4-wide 
// FLOATS.   The output components are equal to input values expanded to FLOAT.
//
// vMin and vMax are expanded in place.  Upon input, they contain the same 4 values replicated 4 times,
// so no data is lost by the expansion.
//
// vARGBBYTE[0]         [ A0 | R0 | G0 | B0 ][ A1 | R1 | G1 | B1 ][ A2 | R2 | G2 | B2 ][ A3 | R3 | G3 | B3 ]
// vARGBBYTE[1]         [ A4 | R4 | G4 | B4 ][ A5 | R5 | G5 | B5 ][ A6 | R6 | G6 | B6 ][ A7 | R7 | G7 | B7 ]
// vARGBBYTE[2]         [ A8 | R8 | G8 | B8 ][ A9 | R9 | G9 | B9 ][ Aa | Ra | Ga | Ba ][ Ab | Rb | Gb | Bb ]
// vARGBBYTE[3]         [ Ac | Rc | Gc | Bc ][ Ad | Rd | Gd | Bd ][ Ae | Re | Ge | Be ][ Af | Rf | Gf | Bf ]
//
// vRGBAFLOAT[ 0]       [         R0        ][         G0        ][         B0        ][         A0        ]
// vRGBAFLOAT[ 1]       [         R1        ][         G1        ][         B1        ][         A1        ]
// vRGBAFLOAT[ 2]       [         R2        ][         G2        ][         B2        ][         A2        ]
// vRGBAFLOAT[ 3]       [         R3        ][         G3        ][         B3        ][         A3        ]
// vRGBAFLOAT[ 4]       [         R4        ][         G4        ][         B4        ][         A4        ]
// vRGBAFLOAT[ 5]       [         R5        ][         G5        ][         B5        ][         A5        ]
// vRGBAFLOAT[ 6]       [         R6        ][         G6        ][         B6        ][         A6        ]
// vRGBAFLOAT[ 7]       [         R7        ][         G7        ][         B7        ][         A7        ]
// vRGBAFLOAT[ 8]       [         R8        ][         G8        ][         B8        ][         A8        ]
// vRGBAFLOAT[ 9]       [         R9        ][         G9        ][         B9        ][         A9        ]
// vRGBAFLOAT[10]       [         Ra        ][         Ga        ][         Ba        ][         Aa        ]
// vRGBAFLOAT[11]       [         Rb        ][         Gb        ][         Bb        ][         Ab        ]
// vRGBAFLOAT[12]       [         Rc        ][         Gc        ][         Bc        ][         Ac        ]
// vRGBAFLOAT[13]       [         Rd        ][         Gd        ][         Bd        ][         Ad        ]
// vRGBAFLOAT[14]       [         Re        ][         Ge        ][         Be        ][         Ae        ]
// vRGBAFLOAT[15]       [         Rf        ][         Gf        ][         Bf        ][         Af        ]
//---------------------------------------------------------------------------------------------------------
__forceinline void ExpandBYTEToFLOATRGBA( __vector4 vRGBAFLOAT[16], __vector4 vARGBBYTE[4] )
{
	ExpandBYTEToFLOATRGBA( &vRGBAFLOAT[0],  vARGBBYTE[0] );
	ExpandBYTEToFLOATRGBA( &vRGBAFLOAT[4],  vARGBBYTE[1] );
	ExpandBYTEToFLOATRGBA( &vRGBAFLOAT[8],  vARGBBYTE[2] );
	ExpandBYTEToFLOATRGBA( &vRGBAFLOAT[12], vARGBBYTE[3] );
}


//---------------------------------------------------------------------------------------------------------
// Name: ExpandBYTEToFLOATAAAA( )
// Desc: Input is a vectors interpreted as 16-wide BYTEs.  Output is 4 vectors, each interpreted as 4-wide 
// FLOATS.   The output components are equal to the values of the input.
//
// vSrc                 [ A0 | A1 | A2 | A3 ][ A4 | A5 | A6 | A7 ][ A8 | A9 | Aa | Ab ][ Ac | Ad | Ae | Af ]
//
// vAlpha[0]            [         A0        ][         A1        ][         A2        ][         A3        ]
// vAlpha[1]            [         A4        ][         A5        ][         A6        ][         A7        ]
// vAlpha[2]            [         A8        ][         A9        ][         Aa        ][         Ab        ]
// vAlpha[3]            [         Ac        ][         Ad        ][         Ae        ][         Af        ]
//---------------------------------------------------------------------------------------------------------
__forceinline void ExpandBYTEToFLOATAAAA( __vector4 vAlpha[4], __vector4 vSrc )
{
	for ( UINT i = 0; i < 4; ++i )
	{
		vSrc                    = __vsldoi( vSrc, vSrc, 1 << 2 );
		ExpandBYTEToFLOAT<TRUE>( vAlpha[i], vSrc );
	}
}


//---------------------------------------------------------------------------------------------------------
// Name: SelectOneChannelFromFour( )
// Desc: This is a helper function to enable the same code to be used for alpha, U, or V compression.
// Input is 4 vectors, interpreted as 16-wide BYTEs.  Output is a vector, interpreted as 16-wide BYTEs.
//
// The output contains some 16 of the 64 input values, as controlled by the hard-coded permutation masks.
// When the transpose option is selected, then the output is transposed as if it were a 4x4 matrix.
// This option sometimes helps simplify the subsequent calculation.
//
// Some examples:
//
// IF t_bTranspose == FALSE, t_iChannel == CHANNEL_ALPHA:
// In[0]                [ A0 | R0 | G0 | B0 ][ A1 | R1 | G1 | B1 ][ A2 | R2 | G2 | B2 ][ A3 | R3 | G3 | B3 ]
// In[1]                [ A4 | R4 | G4 | B4 ][ A5 | R5 | G5 | B5 ][ A6 | R6 | G6 | B6 ][ A7 | R7 | G7 | B7 ]
// In[2]                [ A8 | R8 | G8 | B8 ][ A9 | R9 | G9 | B9 ][ Aa | Ra | Ga | Ba ][ Ab | Rb | Gb | Bb ]
// In[3]                [ Ac | Rc | Gc | Bc ][ Ad | Rd | Gd | Bd ][ Ae | Re | Ge | Be ][ Af | Rf | Gf | Bf ]
//---------------------------------------------------------------------------------------------------------
// Out                  [ A0 | A1 | A2 | A3 ][ A4 | A5 | A6 | A7 ][ A8 | A9 | Aa | Ab ][ Ac | Ad | Ae | Af ]
//
// IF t_bTranspose == TRUE, t_iChannel == CHANNEL_ALPHA:
// In[0]                [ A0 | R0 | G0 | B0 ][ A1 | R1 | G1 | B1 ][ A2 | R2 | G2 | B2 ][ A3 | R3 | G3 | B3 ]
// In[1]                [ A4 | R4 | G4 | B4 ][ A5 | R5 | G5 | B5 ][ A6 | R6 | G6 | B6 ][ A7 | R7 | G7 | B7 ]
// In[2]                [ A8 | R8 | G8 | B8 ][ A9 | R9 | G9 | B9 ][ Aa | Ra | Ga | Ba ][ Ab | Rb | Gb | Bb ]
// In[3]                [ Ac | Rc | Gc | Bc ][ Ad | Rd | Gd | Bd ][ Ae | Re | Ge | Be ][ Af | Rf | Gf | Bf ]
//---------------------------------------------------------------------------------------------------------
// Out                  [ A0 | A4 | A8 | Ac ][ A1 | A5 | A9 | Ad ][ A2 | A6 | Aa | Ae ][ A3 | A7 | Ab | Af ]
//
//---------------------------------------------------------------------------------------------------------
template< BOOL t_bTranspose, UINT t_iChannel >
__forceinline void SelectOneChannelFromFour( __vector4& vOneChannelx16, __vector4 vFourChannelx4[4] )
{
	// Merge all 16 Alpha values into 1 VMX register as BYTEs
#pragma warning( push )
#pragma warning( disable:4127 )   // conditional expression is constant
	if( t_bTranspose )
#pragma warning( pop )
	{
		// 'Transpose' so that:
		//  Elements 0/4/8/c are in the .x component
		//  Elements 1/5/9/d are in the .y component
		//  Elements 2/6/a/e are in the .z component
		//  Elements 3/7/c/f are in the .w component
		static CONST __vector4i viPermSelectChannel[CHANNEL_COUNT] = 
		{
			// transpose
			{ 0x00100414, 0x08180c1c, 0x00100414, 0x08180c1c, },    // alpha
			{ 0x01110515, 0x09190d1d, 0x01110515, 0x09190d1d, },    // B
			{ 0x02120616, 0x0a1a0e1e, 0x02120616, 0x0a1a0e1e, },    // G
			{ 0x03130717, 0x0b1b0f1f, 0x03130717, 0x0b1b0f1f, }     // R
		};

		vOneChannelx16          = __vmrglb( 
			__vperm( vFourChannelx4[0], vFourChannelx4[2], 
			*( __vector4* )&viPermSelectChannel[t_iChannel] ), 
			__vperm( vFourChannelx4[1], vFourChannelx4[3], 
			*( __vector4* )&viPermSelectChannel[t_iChannel] ) );
	}
	else
	{
		// Leave in native order so that:
		//  Elements 0-3   are in the x component
		//  Elements 4-7   are in the y component
		//  Elements 8-11  are in the z component
		//  Elements 12-15 are in the w component
		static CONST __vector4i viPermSelectChannel[CHANNEL_COUNT] = 
		{
			// non-transpose
			{ 0x0004080c, 0x1014181c, 0x0004080c, 0x1014181c, },    // alpha
			{ 0x0105090d, 0x1115191d, 0x0105090d, 0x1115191d, },    // B
			{ 0x02060a0e, 0x12161a1e, 0x02060a0e, 0x12161a1e, },    // G
			{ 0x03070b0f, 0x13171b1f, 0x03070b0f, 0x13171b1f, },    // R
		};

		vOneChannelx16          = __vmrglw( 
			__vperm( vFourChannelx4[0], vFourChannelx4[2], 
			*( __vector4* )&viPermSelectChannel[t_iChannel] ), 
			__vperm( vFourChannelx4[1], vFourChannelx4[3], 
			*( __vector4* )&viPermSelectChannel[t_iChannel] ) );
	}
}


//---------------------------------------------------------------------------------------------------------
// Name: SelectOneChannelFromFour( )
// Desc: This is a helper function to enable the same code to be used for alpha, U, V compression.
// Input and output are vectors, interpreted as 16-wide BYTEs.
//
// The input values are assumed to contain the same data, replicated either 2 or 4 times, and the output
// values are also replicated.
//
// Some examples:
//
// IF t_iChannel == CHANNEL_ALPHA:
// In                   [ A  | R  | G  | B  ][ A  | R  | G  | B  ][ A  | R  | G  | B  ][ A  | R  | G  | B  ]
//---------------------------------------------------------------------------------------------------------
// Out                  [ A  | A  | A  | A  ][ A  | A  | A  | A  ][ A  | A  | A  | A  ][ A  | A  | A  | A  ]
//
//---------------------------------------------------------------------------------------------------------
template< UINT t_iChannel >
__forceinline void SelectOneChannelFromFour( __vector4& vOneChannelx16, __vector4 vFourChannelx4 )
{
	// Replicate one BYTE from each of xyzw to all four BYTEs
	C_ASSERT( CHANNEL_ALPHA == 0 && CHANNEL_RED == 1 && CHANNEL_GREEN == 2 && CHANNEL_BLUE == 3);
	vOneChannelx16              = __vspltb( vFourChannelx4, t_iChannel % 4 );
}


//---------------------------------------------------------------------------------------------------------
// Name: FindCovarianceRGB( )
// Desc: Finds covariances between R and G, and B and G, across the 16 input vectors.
// Where the actual covariance formula uses the mean, we substitute the center, for computational 
// efficiency.
//
// The results are used to estimate which of the 4 bounding box diagonals is a closest fit to the
// principal axis.
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 FindCovarianceRGB( __vector4 vRGBA[16], __vector4 vCenter )
{
	// Move everything relative to the center.  
	__vector4 vDiffCenter[16];
	for( UINT i = 0; i < 16; ++i )
	{
		vDiffCenter[i]          = __vsubfp( vRGBA[i], vCenter ); 
	}
	__vector4 vCovPartial[8];
	// Sum up (R*G, G*G, B*G, A*A) over all 16 points
	for( UINT i = 0; i < 8; ++i )
	{
		//   R_i   * G_i,   G_i   * G_i,   B_i   * B_i
		// + R_i+i * G_i+1, G_i+1 * G_i+1, B_i+1 * B_i+1 
		vCovPartial[i]          = __vmaddfp( vDiffCenter[2*i+0], 
			__vpermwi( vDiffCenter[2*i+0], VPERMWI_CONST( 1, 1, 1, 3 ) ), 
			__vmulfp( vDiffCenter[2*i+1], 
			__vpermwi( vDiffCenter[2*i+1], VPERMWI_CONST( 1, 1, 1, 3 ) ) ) );
	}
	__vector4 vCovariance       = __vaddfp( 
		__vaddfp( 
		__vaddfp( vCovPartial[0], vCovPartial[1] ), 
		__vaddfp( vCovPartial[2], vCovPartial[3] ) ), 
		__vaddfp( 
		__vaddfp( vCovPartial[4], vCovPartial[5] ), 
		__vaddfp( vCovPartial[6], vCovPartial[7] ) ) );

	return vCovariance;
}

//---------------------------------------------------------------------------------------------------------
// Name: Find2BitPaletteIndicesFLOAT( )
// Desc: Input is 4 vectors containing 16 FLOAT dot products, and an increment, to normalize these 
// values to the range 0-3.
//
// Output is the normalized indices, packed as bits into:
//
// vPackedIndices:      [            | 31-24][            | 23-16][            | 15-8 ][            |  7-0 ]
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 Find2BitPaletteIndicesFLOAT( __vector4 vDotx4[4], __vector4 vStepInc )
{
	__vector4 vZero             = __vzero( );
	__vector4 vWordOne          = __vspltisw( 1 );
	__vector4 vWordTwo          = __vspltisw( 2 );
	__vector4 vWordThree        = __vspltisw( 3 );
	__vector4 vFloatOneHalf     = __vcfsx( vWordOne, 1 );

	__vector4 vPackedIndices    = vZero;
	for ( UINT i = 0; i < 4; ++i )
	{
		// This calculation assumes no pixels are outside the range of the anchor colors.
		// Otherwise need a max/min.
		__vector4 vPalFloat     = __vmaddfp( vDotx4[i], vStepInc, vFloatOneHalf );
		__vector4 vPalIndex     = __vand( vWordThree, __vctuxs( vPalFloat, 0 ));

		// Incoming index is quantized distance from vAnchor[0] to vAnchor[1]
		// The indexing in the DXT1 standard differs from this indexing as follows:
		//
		// Above:  0 means vAnchor[0]
		//         1 means lerp( vAnchor[0], vAnchor[1], 1/3 )
		//         2 means lerp( vAnchor[0], vAnchor[1], 2/3 )
		//         3 means vAnchor[1]
		//
		// DXT1:   0 means vAnchor[0]
		//         1 means vAnchor[1]
		//         2 means lerp( vAnchor[0], vAnchor[1], 1/3 )
		//         3 means lerp( vAnchor[0], vAnchor[1], 2/3 )
		// Implied remapping: 0/1/2/3 --> 0/2/3/1 
		//
		// Remapping in one instruction:
		static CONST __vector4i viIndexRemapping = {0x00020301, 0x00020301, 0x00020301, 0x00020301}; 
		__vector4 vRemappedIndex= __vperm( *(__vector4*)&viIndexRemapping, *(__vector4*)&viIndexRemapping, 
			vPalIndex );
		vPackedIndices          = __vor( __vslw( vPackedIndices, vWordTwo ), vRemappedIndex );
	}

	return vPackedIndices;
}


//---------------------------------------------------------------------------------------------------------
// Name: Find3BitPaletteIndicesFLOAT( )
// Desc: Input is 4 vectors containing 16 FLOAT differences, and an increment, to normalize these 
// values to the range 0-7.
//
// Output is the normalized indices, packed as bits into:
//
// vPackedIndices:      [            | 11-0 ][            | 23-12][            | 35-24][            | 47-36]
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 Find3BitPaletteIndicesFLOAT( __vector4 vDotx4[4], __vector4 vStepInc )
{
	__vector4 vZero             = __vzero( );
	__vector4 vWordOne          = __vspltisw( 1 );
	__vector4 vWordThree        = __vspltisw( 3 );
	__vector4 vWordSeven        = __vspltisw( 7 );
	__vector4 vFloatOneHalf     = __vcfsx( vWordOne, 1 );

	__vector4 vPackedIndices    = vZero;
	for ( UINT i = 0; i < 4; ++i )
	{
		// This calculation assumes no pixels are outside the range of the anchor colors.
		// Otherwise need a max/min.
		__vector4 vPalFloat     = __vmaddfp( vDotx4[i], vStepInc, vFloatOneHalf );
		__vector4 vPalIndex     = __vand( vWordSeven, __vctuxs( vPalFloat, 0 ));

		// Incoming index is quantized distance from vAnchor[0] to vAnchor[1]
		// The indexing in the DXT1 standard differs from this indexing as follows:
		//
		// Above:  0 means vAnchor[0]
		//         1 means lerp( vAnchor[0], vAnchor[1], 1/7 )
		//         2 means lerp( vAnchor[0], vAnchor[1], 2/7 )
		//         3 means lerp( vAnchor[0], vAnchor[1], 3/7 )
		//         4 means lerp( vAnchor[0], vAnchor[1], 4/7 )
		//         5 means lerp( vAnchor[0], vAnchor[1], 5/7 )
		//         6 means lerp( vAnchor[0], vAnchor[1], 6/7 )
		//         7 means vAnchor[1]
		//
		// DXT1:   0 means vAnchor[0]
		//         1 means vAnchor[1]
		//         2 means lerp( vAnchor[0], vAnchor[1], 1/7 )
		//         3 means lerp( vAnchor[0], vAnchor[1], 2/7 )
		//         4 means lerp( vAnchor[0], vAnchor[1], 3/7 )
		//         5 means lerp( vAnchor[0], vAnchor[1], 4/7 )
		//         6 means lerp( vAnchor[0], vAnchor[1], 5/7 )
		//         7 means lerp( vAnchor[0], vAnchor[1], 6/7 )
		// Implied remapping: 0/1/2/3/4/5/6/7 --> 0/2/3/4/5/6/7/1 
		//
		// Remapping in one instruction:
		static CONST __vector4i viIndexRemapping = {0x00020304, 0x05060701, 0x00020304, 0x05060701}; 
		__vector4 vRemappedIndex= __vperm( *(__vector4*)&viIndexRemapping, *(__vector4*)&viIndexRemapping, 
			vPalIndex );
		vPackedIndices          = __vor( __vslw( vPackedIndices, vWordThree ), vRemappedIndex );
	}

	return vPackedIndices;
}



//---------------------------------------------------------------------------------------------------------
// Name: Pack8888To32( )
// Desc: On input, vPackedIndices contains palette indices for the block, packed as:
//
// vPackedIndices:      [             | 15-8][             |  7-0][             |31-24][             |23-16]
//
// On output, vPackedIndices contains palette indices for the block, packed as:
//
// vPackedIndices:      [  15-0   |  31-16  ][  15-0   |  31-16  ][  15-0   |  31-16  ][  15-0   |  31-16  ]
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 Pack8888To32( __vector4 vPackedIndices )
{
	vPackedIndices              = __vpkuhum( vPackedIndices, vPackedIndices );
	vPackedIndices              = __vpkuhum( vPackedIndices, vPackedIndices );

	return vPackedIndices;
}

//---------------------------------------------------------------------------------------------------------
// Name: PackAnchorsAnd12121212To64( )
// Desc: On input, vPackedIndices contains palette indices for the block, packed as:
//
// vPackedIndices:      [            | 11-0 ][            | 23-12][            | 35-24][            | 47-36]
// 
// And vPackedAnchors contains block anchors, packed as:
//
// vPackedAnchors:      [                   ][         | a1 | a0 ][                   ][         | a1 | a0 ]
//
// On output, vPackedBlock contains anchors and indices for the block, packed as:
//
// vPackedBlock:        [ a1 | a0 |  15-0   ][  31-16  |  47-32  ][ a1 | a0 |  15-0   ][  31-16  |  47-32  ]
//---------------------------------------------------------------------------------------------------------
__forceinline __vector4 PackAnchorsAnd12121212To64( __vector4 vPackedAnchors, __vector4 vPackedIndices )
{
	__vector4 vWordOne          = __vspltisw( 1 );
	__vector4 vWordEight        = __vspltisw( 8 );
	__vector4 vWordSixteen      = __vadduws( vWordEight, vWordEight );
	__vector4 vWord255          = __vsubuws( __vslw( vWordOne, vWordEight ), vWordOne );
	__vector4 vWord8to15Bits    = __vslw( vWord255, vWordSixteen );

	// Currently we have this:
	// vPackedAnchors:  [                   ][         | a1 | a0 ][                   ][         | a1 | a0 ]
	// vPackedIndices:  [            | 11-0 ][            | 23-12][            | 35-24][            | 47-36]
	//
	// To determine endianness, let's consider the 48 bits of indices broken into 8-bit chunks.
	//
	//          Bits:   48 -------------------------------------------------------------- 0
	//                   [ 47 - 40 ][ 39 - 32 ][ 31 - 24 ][ 23 - 16 ][ 15 -  8 ][  7 -  0 ]
	//
	// Because the Xbox 360 CPU is big-endian, the order becomes:
	//
	//          Bits:   48 -------------------------------------------------------------- 0
	//                   [  7 -  0 ][ 15 -  8 ][ 23 - 16 ][ 31 - 24 ][ 39 - 32 ][ 47 - 40 ]
	//
	// D3DFMT_DXT5A uses an endianness of GPUENDIAN_8IN16 for legacy reasons.  Therefore, the order becomes: 
	//
	//          Bits:   48 -------------------------------------------------------------- 0
	//                   [ 15 -  8 ][  7 -  0 ][ 31 - 24 ][ 23 - 16 ][ 47 - 40 ][ 39 - 32 ]
	//
	// Therefore the end result we hope for is:
	//
	// Block:           [ a1 | a0 |  15-0   ][  31-16  |  47-32  ][ a1 | a0 |  15-0   ][  31-16  |  47-32  ]
	//
	static CONST __vector4i viRotateCounts =  {0x00000000, 0x00000000c, 0x00000018, 0x00000004};
	__vector4 vRotatedIndices   = __vrlw( vPackedIndices, *( __vector4* )viRotateCounts );
	// Now we have this:
	// vRotatedIndices: [           | 11-0  ][   | 23-12 |       ][-24 |          | 35][       | 47-36 |   ]

	__vector4 vIndicesBy24s     = __vor( vRotatedIndices, __vrlimi( vIndicesBy24s, vRotatedIndices, 0xf, 3 ));
	// Now we have this:
	// vIndicesBy24s:   [    garbage        ][   |      23-0     ][      garbage      ][-24 |  |    47-    ]

	__vector4 vPackedWord0      = __vmrghh( vPackedAnchors, vIndicesBy24s );
	// Now we have this:
	// vPackedWord0:    [      garbage      ][      garbage      ][      garbage      ][ a1 | a0 |  15-0   ]

	__vector4 vPackedWord1      = __vor( vIndicesBy24s, 
		__vrlimi( vPackedWord1, __vand( vIndicesBy24s, vWord8to15Bits ), 0xf, 2 ));
	// Now we have this:
	// vPackedWord1:    [      garbage      ][      garbage      ][      garbage      ][  31-16  |  47-32  ]

	__vector4 vPackedBlock      = __vmrglw( 
		__vmrglw( vPackedWord0, vPackedWord0 ), 
		__vmrglw( vPackedWord1, vPackedWord1 ));
	// vPackedBlock:    [ a1 | a0 |  15-0   ][  31-16  |  47-32  ][ a1 | a0 |  15-0   ][  31-16  |  47-32  ]

	return vPackedBlock;
}


//---------------------------------------------------------------------------------------------------------
// Name: CompressRGBBlockVMXFLOAT( )
// Desc: Encode a single 64-bit RGB block, as part of either a DXT1 or a DXT4/5 texture.  
//
// Input is 4 vectors, each interpreted as 16-wide BYTEs.  Input ordering is the following in 
// texture coordinates:
//
// -----------------
// | 0 | 1 | 2 | 3 |
// -----------------
// | 4 | 5 | 6 | 7 |
// -----------------
// | 8 | 9 | a | b |
// -----------------
// | c | d | e | f |
// -----------------
//
// The input data is expanded to FLOAT, and much of the calculation uses FLOAT VMX intrinsics.
//
// We do not handle DXT1 blocks with 1-bit alpha.  This may be useful to implement in some cases, 
// but it would severely impact performance for non-alpha textures.
//
// This routine currently compiles to code with no branches and no writes to memory, other than 
// the 64-bit output.  Both of these are important for performance.
//---------------------------------------------------------------------------------------------------------
__forceinline VOID CompressRGBBlockVMXFLOAT( BYTE *pDst, 
	__vector4 vMin, 
	__vector4 vMax, 
	__vector4 vSrc0123, 
	__vector4 vSrc4567, 
	__vector4 vSrc89ab, 
	__vector4 vSrccdef )
{
	// Load needed constants.  Note most of these operations get scheduled during 
	// pipeline stalls, and are therefore effectively free.
	__vector4 vZero             = __vzero( );
	__vector4 vWordOne          = __vspltisw( 1 );
	__vector4 vWordFour         = __vspltisw( 4 );
	__vector4 vWordSixteen      = __vslw( vWordOne, vWordFour );
	__vector4 vFloatOneHalf     = __vcfsx( vWordOne, 1 );
	__vector4 vFloatOne         = __vaddfp( vFloatOneHalf, vFloatOneHalf );
	__vector4 vFloatTwo         = __vaddfp( vFloatOne, vFloatOne );
	__vector4 vFloatThree       = __vaddfp( vFloatTwo, vFloatOne );

	// Unpack to full float.  Note that VPACK_D3DCOLOR has an implicit endian swap
	// from ARGB to RGBA.
	__vector4 vARGBx4[4] = { vSrc0123, vSrc4567, vSrc89ab, vSrccdef };
	__vector4 vRGBA[16];
	ExpandBYTEToFLOATRGBA( vRGBA, vARGBx4 );
	// vRGBA[ 0]        [         R0        ][         G0        ][         B0        ][         A0        ]
	// vRGBA[ 1]        [         R1        ][         G1        ][         B1        ][         A1        ]
	// vRGBA[ 2]        [         R2        ][         G2        ][         B2        ][         A2        ]
	// vRGBA[ 3]        [         R3        ][         G3        ][         B3        ][         A3        ]
	// vRGBA[ 4]        [         R4        ][         G4        ][         B4        ][         A4        ]
	// vRGBA[ 5]        [         R5        ][         G5        ][         B5        ][         A5        ]
	// vRGBA[ 6]        [         R6        ][         G6        ][         B6        ][         A6        ]
	// vRGBA[ 7]        [         R7        ][         G7        ][         B7        ][         A7        ]
	// vRGBA[ 8]        [         R8        ][         G8        ][         B8        ][         A8        ]
	// vRGBA[ 9]        [         R9        ][         G9        ][         B9        ][         A9        ]
	// vRGBA[10]        [         Ra        ][         Ga        ][         Ba        ][         Aa        ]
	// vRGBA[11]        [         Rb        ][         Gb        ][         Bb        ][         Ab        ]
	// vRGBA[12]        [         Rc        ][         Gc        ][         Bc        ][         Ac        ]
	// vRGBA[13]        [         Rd        ][         Gd        ][         Bd        ][         Ad        ]
	// vRGBA[14]        [         Re        ][         Ge        ][         Be        ][         Ae        ]
	// vRGBA[15]        [         Rf        ][         Gf        ][         Bf        ][         Af        ]

	__vector4 vMinFLOAT, vMaxFLOAT;
	ExpandBYTEToFLOAT<FALSE>( vMinFLOAT, vMin );
	ExpandBYTEToFLOAT<FALSE>( vMaxFLOAT, vMax );
	__vector4 vCenter           = __vmaddfp( vFloatOneHalf, vMinFLOAT, __vmulfp( vFloatOneHalf, vMaxFLOAT ));

	// Find the covariances between R and G, B and G.  Check whether each is positive or negative
	// vAnchorMask has these meanings:
	//  .x == 0xff <--> R and G are negatively correlated
	//  .y == 0xff <--> G and G are negatively correlated (never)
	//  .z == 0xff <--> B and G are negatively correlated
	// We only need 2 bits of information to choose from among the 4 diagonals.  
	// The mostly visually important are those involving G.  
	__vector4 vCovariance       = FindCovarianceRGB( vRGBA, vCenter );
	__vector4 vAnchorMask       = __vcmpgefp( vZero, vCovariance );

	// At this point, vAnchorMask is 0x00 for components of vAnchor[0] which should equal 
	// vMin, and 0xff for components of vAnchor[0] which should equal vMax.
	__vector4 vAnchorFLOAT[2];
	vAnchorFLOAT[0]             = __vsel( vMinFLOAT, vMaxFLOAT, vAnchorMask );
	vAnchorFLOAT[1]             = __vsel( vMaxFLOAT, vMinFLOAT, vAnchorMask );

	// These are floating point 31.0, 63.0, 31.0
	static CONST __vector4i vi565Scale =  {0x41f80000, 0x427c0000, 0x41f80000, 0x00000000};

	// These are bit shifts for the anchor colors.
	// These swap red and blue as well.
	static CONST __vector4i vi565Shift =  {0x0000000b, 0x00000005, 0x00000000, 0x00000000};

	// Find the packed anchors.  Note experiments indicate that it is slightly faster to generate 
	// these from the FLOAT anchors than from the BYTE anchors.
	__vector4 vAnchor565[2];
	for ( UINT k = 0; k < 2; ++k )
	{
		// vAnchorFLOAT[k]
		//              [         R         ][         G         ][         B         ][         A         ]
		vAnchor565[k]           = __vmaddfp( vAnchorFLOAT[k], *( __vector4* )&vi565Scale, vFloatOneHalf );
		vAnchor565[k]           = __vctuxs( vAnchor565[k], 0 );

		assert( vAnchor565[k].u[0] < 32 && vAnchor565[k].u[1] < 64 && vAnchor565[k].u[2] < 32 );

		vAnchor565[k]           = __vslw( vAnchor565[k], *( __vector4* )&vi565Shift );
		// vAnchor565[k]
		//              [          |R5|     ][             |G6|  ][                |B5][                   ]

		vAnchor565[k]           = __vor( __vspltw( vAnchor565[k], 0 ), 
			__vor( __vspltw( vAnchor565[k], 1 ), 
			__vspltw( vAnchor565[k], 2 )) );
		// vAnchor565[k]
		//              [          |R5|G6|B5][          |R5|G6|B5][          |R5|G6|B5][          |R5|G6|B5]
	}

	// Switch the anchors if they would indicate an alpha block.
	// If the anchors are equal, write all zeros to the palette later.
	__vector4 vAnchorsEqual     = __vcmpequw( vAnchor565[1], vAnchor565[0] );
	__vector4 vAnchorsSwapped   = __vcmpgtuw( vAnchor565[1], vAnchor565[0] );
	ConditionalExchange( vAnchor565[0], vAnchor565[1], vAnchorsSwapped );
	ConditionalExchange( vAnchorFLOAT[0], vAnchorFLOAT[1], vAnchorsSwapped );

	// Put the packed anchors in the x component of a vector
	__vector4 vPackedAnchors    = __vor( __vslw( vAnchor565[0], vWordSixteen ), vAnchor565[1] );
	// vPackedAnchors   [ R5|G6|B5| r5|g6|b5][ R5|G6|B5| r5|g6|b5][ R5|G6|B5| r5|g6|b5][ R5|G6|B5| r5|g6|b5]

	// $TODO: palettize to the expanded 5:6:5 anchors, not the raw anchors?
	// For each input color, find the closest representable step along the line joining the anchors.
	__vector4 vDiag             = __vsubfp( vAnchorFLOAT[1], vAnchorFLOAT[0] );
	__vector4 vDot[16];
	for ( UINT i = 0; i < 16; ++i )
	{
		__vector4 vDiff         = __vsubfp( vRGBA[i], vAnchorFLOAT[0] );
		vDot[i]                 = __vmsum3fp( vDiff, vDiag );
	}

	// Condense 16 dot products from 16 vectors into 4 and 'transpose' so that elements are in 
	// the desired output order, reading down each column and then over to the right.  A 'column-major'
	// order is necessary, because the values in each column will end up consecutive in the output.
	//
	// Here is the correct arrangement:
	//
	//  Elements 7-4   are in the .x components
	//  Elements 3-0   are in the .y components
	//  Elements 15-12 are in the .z components
	//  Elements 11-8  are in the .w components
	//
	//  vDotx4[0]       [        dot7       ][        dot3       ][        dotf       ][        dotb       ]
	//  vDotx4[1]       [        dot6       ][        dot2       ][        dote       ][        dota       ]
	//  vDotx4[2]       [        dot5       ][        dot1       ][        dotd       ][        dot9       ]
	//  vDotx4[3]       [        dot4       ][        dot0       ][        dotc       ][        dot8       ]
	//
	// This swizzle order matches the DXT standard, Xbox CPU conventions, and D3DFMT_DXT1 endianness, 
	// as follows:
	//
	// DXT standard is that the 0th input goes in the lowest order bits, and so forth.  This implies
	// an order of:
	// 
	//          Bits:   31 ------------------------------------------ 0
	//                  15|14|13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1| 0
	//
	// Because the Xbox 360 CPU is big-endian, the order becomes:
	//
	//          Bits:   31 ------------------------------------------ 0
	//                   3| 2| 1| 0| 7| 6| 5| 4|11|10| 9| 8|15|14|13|12
	//
	// D3DFMT_DXT1 uses an endianness of GPUENDIAN_8IN16 for legacy reasons.  Therefore, the order becomes: 
	//
	//          Bits:   31 ------------------------------------------ 0
	//                   7| 6| 5| 4| 3| 2| 1| 0|15|14|13|12|11|10| 9| 8
	//
	__vector4 vDotx4[4];
	vDotx4[0]                   = __vmrglw(
		__vmrglw( vDot[ 7], vDot[15] ), 
		__vmrglw( vDot[ 3], vDot[11] ));
	vDotx4[1]                   = __vmrglw(             
		__vmrglw( vDot[ 6], vDot[14] ), 
		__vmrglw( vDot[ 2], vDot[10] ));
	vDotx4[2]                   = __vmrglw(             
		__vmrglw( vDot[ 5], vDot[13] ), 
		__vmrglw( vDot[ 1], vDot[ 9] ));
	vDotx4[3]                   = __vmrglw(             
		__vmrglw( vDot[ 4], vDot[12] ), 
		__vmrglw( vDot[ 0], vDot[ 8] ));

	// Convert each dot product into an integral number of steps of 1/3 the distance between anchors.
	__vector4 vStepInc          = __vmulfp( vFloatThree, __vrefp( __vmsum3fp( vDiag, vDiag )) );
	__vector4 vPackedIndices = Find2BitPaletteIndicesFLOAT( vDotx4, vStepInc );

	// Currently we have this:
	// vPackedIndices:  [             | 15-8][             |  7-0][             |31-24][             |23-16]
	// We want this:
	// vPackedIndices:  [  15-0   |  31-16  ][  15-0   |  31-16  ][  15-0   |  31-16  ][  15-0   |  31-16  ]
	vPackedIndices = Pack8888To32( vPackedIndices );

	// Special case to avoid alpha pixels when anchors are the same
	vPackedIndices              = __vsel( vPackedIndices, vZero, vAnchorsEqual );

	// Store 64 bits to possibly unaligned, possibly non-cacheable address
	__stvewx( vPackedAnchors, pDst, 0 );
	__stvewx( vPackedIndices, pDst, 4 );

	// Non-alpha block
	assert( (( WORD* )pDst )[0] > ( (WORD* )pDst )[1]
	|| ( (( WORD* )pDst )[0] == ( (WORD* )pDst )[1] && ( (DWORD* )pDst )[1] == 0 ));
}




//---------------------------------------------------------------------------------------------------------
// Name: CompressABlockVMXFLOAT( )
// Desc: Encode a single 64-bit Alpha block, as part of either a DXT4/5 or a DXN texture.  This routine
// could also be used to generate 1-channel DXT5A textures (not implemented).  
//
// Input is 4 vectors, each interpreted as 16-wide BYTEs.  Input ordering is the following in texture 
// coordinates:
//
// -----------------
// | 0 | 1 | 2 | 3 |
// -----------------
// | 4 | 5 | 6 | 7 |
// -----------------
// | 8 | 9 | a | b |
// -----------------
// | c | d | e | f |
// -----------------
//
// The input data is expanded to FLOAT, and much of the calculation uses FLOAT VMX intrinsics.
//
// We do not handle Alpha blocks with explicit 0 and 1 (transparent/opaque) encoding.  This may be useful 
// to implement in some cases, but it would severely impact performance.
//
// This routine currently compiles to code with no branches and no writes to memory, other than 
// the 64-bit output.  Both of these are important for performance.
//---------------------------------------------------------------------------------------------------------
template< UINT t_iChannel >
__forceinline VOID CompressABlockVMXFLOAT( BYTE *pDst, 
	__vector4 vMin, 
	__vector4 vMax, 
	__vector4 vSrc0123, 
	__vector4 vSrc4567, 
	__vector4 vSrc89ab, 
	__vector4 vSrccdef )
{
	// Load needed constants.  Note most of these operations get scheduled during 
	// pipeline stalls, and are therefore effectively free.
	__vector4 vWordOne          = __vspltisw( 1 );
	__vector4 vWordSeven        = __vspltisw( 7 );
	__vector4 vWordEight        = __vspltisw( 8 );
	__vector4 vWord255          = __vsubuws( __vslw( vWordOne, vWordEight ), vWordOne );
	__vector4 vFloatOneHalf     = __vcfsx( vWordOne, 1 );
	__vector4 vFloatSeven       = __vcfsx( vWordSeven, 0 );
	__vector4 vFloat255         = __vcfsx( vWord255, 0 );

	__vector4 vARGBx4[4] = { vSrc0123, vSrc4567, vSrc89ab, vSrccdef };

	// Merge all 16 Alpha values into 1 VMX register as BYTEs
	__vector4 vAlphax16;
	SelectOneChannelFromFour<TRUE, t_iChannel>( vAlphax16, vARGBx4 );

	SelectOneChannelFromFour<t_iChannel>( vMin, vMin );
	SelectOneChannelFromFour<t_iChannel>( vMax, vMax );

	// Unpack to full float.  Note that VPACK_D3DCOLOR has an implicit endian swap
	// from ARGB to RGBA, and therefore we must either spend instructions to undo
	// this swap, or work in the order: ( 1, 2, 3, 0 ).
	// For clarity, we choose to spend the instructions to regain the normal order.
	__vector4 vAlphax4[4];
	ExpandBYTEToFLOATAAAA( vAlphax4, vAlphax16 );

	ExpandBYTEToFLOAT<FALSE>( vMin, vMin );
	ExpandBYTEToFLOAT<FALSE>( vMax, vMax );

	__vector4 vAnchor[2];
	vAnchor[0] = vMax;
	vAnchor[1] = vMin;

	// We presently have this ordering:
	//
	// vAlpha[0]x4      [         A0        ][         A4        ][         A8        ][         Ac        ]
	// vAlpha[1]x4      [         A1        ][         A5        ][         A9        ][         Ad        ]
	// vAlpha[2]x4      [         A2        ][         A6        ][         Aa        ][         Ae        ]
	// vAlpha[3]x4      [         A3        ][         A7        ][         Ab        ][         Af        ]
	//
	// We will now subtract Anchor[0] from each alpha, and also reverse this order from top-to-bottom:
	//
	// vDiffx4[0]       [       diff3       ][       diff7       ][       diffb       ][       difff       ]
	// vDiffx4[1]       [       diff2       ][       diff6       ][       diffa       ][       diffe       ]
	// vDiffx4[2]       [       diff1       ][       diff5       ][       diff9       ][       diffd       ]
	// vDiffx4[3]       [       diff0       ][       diff4       ][       diff8       ][       diffc       ]
	//
	// This swizzle order matches the DXT standard, Xbox CPU conventions, and D3DFMT_DXT1 endianness, 
	// as follows:
	//
	// DXT standard is that the 0th input goes in the lowest order bits, and so forth.  This implies
	// an order of:
	// 
	//          Bits:   48 ---------------------------------------------------------- 0
	//                   15| 14| 13| 12| 11| 10|  9|  8|  7|  6|  5|  4|  3|  2|  1|  0
	//
	// Since endian-swaps will break into the middle of indices, we treat this case differently 
	// from previous cases.  We can see that we will want indices to occur in descending order, so 
	// for now, we simply use the standard ordering 15/14/13/12/11/10/9/8/7/6/5/4/3/2/1/0.
	//
	__vector4 vDiffx4[4];
	vDiffx4[0]                  = __vsubfp( vAlphax4[3], vAnchor[0] );
	vDiffx4[1]                  = __vsubfp( vAlphax4[2], vAnchor[0] );
	vDiffx4[2]                  = __vsubfp( vAlphax4[1], vAnchor[0] );
	vDiffx4[3]                  = __vsubfp( vAlphax4[0], vAnchor[0] );

	// Find the 3-bit palette entries
	__vector4 vStepInc = __vmulfp( vFloatSeven, __vrefp( __vsubfp( vAnchor[1], vAnchor[0] )) );
	__vector4 vPackedIndices = Find3BitPaletteIndicesFLOAT( vDiffx4, vStepInc );

	// Find the packed anchors.  Note experiments indicate that it is slightly faster to regenerate 
	// these from the FLOAT anchors than to simply use the BYTE anchors (!?).
	__vector4 vWordAnchor[2];
	vWordAnchor[0]              = __vctuxs( __vmaddfp( vAnchor[0], 
		*( __vector4* )&vFloat255, vFloatOneHalf ), 0 );
	vWordAnchor[1]              = __vctuxs( __vmaddfp( vAnchor[1], 
		*( __vector4* )&vFloat255, vFloatOneHalf ), 0 );

	__vector4 vPackedAnchors    = __vmrglb( vWordAnchor[1], vWordAnchor[0] );
	// Now we have this:
	// vPackedAnchors:  [                   ][         | a1 | a0 ][                   ][         | a1 | a0 ]

	__vector4 vPackedBlock = PackAnchorsAnd12121212To64( vPackedAnchors, vPackedIndices );
	// vPackedBlock:    [ a1 | a0 |  15-0   ][  31-16  |  47-32  ][ a1 | a0 |  15-0   ][  31-16  |  47-32  ]

	// Store 64 bits to possibly unaligned, possibly non-cacheable address
	__stvewx( vPackedBlock, pDst, 0 );
	__stvewx( vPackedBlock, pDst, 4 );
}



//---------------------------------------------------------------------------------------------------------
// Name: CompressDXT1BlockVMXFLOAT( )
// Desc: Compress a single 64-bit DXT1 block, using primarily SIMD FLOAT operations.  
// Input is 4 vectors, each interpreted as 16-wide BYTEs.  
//---------------------------------------------------------------------------------------------------------
VOID CompressDXT1BlockVMXFLOAT( BYTE *pDst, __vector4 vSrc0123, __vector4 vSrc4567, __vector4 vSrc89ab, __vector4 vSrccdef )
{
	// Take min/max
	__vector4 vMin, vMax;
	FindMinMaxARGB( vMin, vMax, vSrc0123, vSrc4567, vSrc89ab, vSrccdef );

	CompressRGBBlockVMXFLOAT( pDst, vMin, vMax, vSrc0123, vSrc4567, vSrc89ab, vSrccdef );
}


//---------------------------------------------------------------------------------------------------------
// Name: CompressDXT5BlockVMXFLOAT( )
// Desc: Compress a single 128-bit DXT5 block, using primarily SIMD FLOAT operations.  
// Input is 4 vectors, each interpreted as 16-wide BYTEs.  
//---------------------------------------------------------------------------------------------------------
VOID CompressDXT5BlockVMXFLOAT( BYTE *pDst, __vector4 vSrc0123, __vector4 vSrc4567, __vector4 vSrc89ab, __vector4 vSrccdef )
{
	// Take min/max
	__vector4 vMin, vMax;
	FindMinMaxARGB( vMin, vMax, vSrc0123, vSrc4567, vSrc89ab, vSrccdef );

	// The ordering of these calls causes a significant difference in performance
	// possibly due to write-combine order
	CompressABlockVMXFLOAT<CHANNEL_ALPHA>(  pDst, vMin, vMax, vSrc0123, vSrc4567, vSrc89ab, vSrccdef  );
	CompressRGBBlockVMXFLOAT( pDst + 8, vMin, vMax, vSrc0123, vSrc4567, vSrc89ab, vSrccdef );
}

#pragma warning( pop )


//---------------------------------------------------------------------------------------------------------
// Name: CompressTextureVMX( )
// Desc: Encode a full texture, using the CompressBlock routine on each 4x4 block.  
//---------------------------------------------------------------------------------------------------------
template <BlockFn CompressBlock>
VOID CompressTextureVMX(BYTE * srcBase, BYTE * dstBase, UINT width, UINT srcPitch, UINT dstPitch, UINT startY, UINT stopY, UINT dstBytesPerBlock)
{
	BYTE* pDstRow = dstBase + ((startY/4) * dstPitch);

#if !SQUISH_USE_INLINE_UNTILE
	const BYTE* pSrcRow = srcBase;
	UINT iSrcBytesPerTexel = 4; // we're going to assume 8888 source.
	UINT iSrcRowPitchInBytes = srcPitch;
	UINT iSrcBlockPitchInBytes = 4* iSrcRowPitchInBytes;


	// Let's bring in two rows of blocks into the L2 cache at a time 
	// ( which is 8x512x4 = 4 KB for a 512x512 8:8:8:8 source ). 
	// It should be okay to prefetch past the end of the buffer ---
	// prefetches are discarded for addresses which aren't in the TLB.
	// Even for the max 2D texture dimensions of 4096x4096, this is
	// only 32 KB, so not a huge displacement of cache contents.

	//		Prefetch( pSrcRow, iSrcBlockPitchInBytes );
	for( UINT jBlock = startY/4; jBlock < stopY / 4 ; ++jBlock )
	{
		//			Prefetch( pSrcRow + iSrcBlockPitchInBytes, iSrcBlockPitchInBytes );

		const BYTE* pSrcBlock = pSrcRow;
		BYTE* pDstBlock = pDstRow;

		for( UINT iBlock = 0; iBlock < pSrcDescAndBaseAddress->Desc.Width / 4; ++iBlock )
		{
			__vector4 vSrc0123           = __lvlx( pSrcBlock, 0 * iSrcRowPitchInBytes );
			__vector4 vSrc4567           = __lvlx( pSrcBlock, 1 * iSrcRowPitchInBytes );
			__vector4 vSrc89ab           = __lvlx( pSrcBlock, 2 * iSrcRowPitchInBytes );
			__vector4 vSrccdef           = __lvlx( pSrcBlock, 3 * iSrcRowPitchInBytes );

			CompressBlock( pDstBlock, vSrc0123, vSrc4567, vSrc89ab, vSrccdef );

			pSrcBlock += 4 * iSrcBytesPerTexel;
			pDstBlock += dstBytesPerBlock;
		}

		pSrcRow += iSrcBlockPitchInBytes;  // Src pitch per Dst row of block
		pDstRow += dstPitch;

	}
#else  
	UNREFERENCED_PARAMETER(srcPitch);

	// We'll untile as we go, saving the untile pass all together.
	const BYTE* baseAddress = srcBase;
	static const UINT kBytesPerTileBlock = 4096; 
	UINT linesPerTileBlock = kBytesPerTileBlock/(width*4);  // number of lines that fit into a 4k tile block

	Prefetch( baseAddress, kBytesPerTileBlock );

	for( UINT y = startY; y < stopY; y += 4 )
	{
		BYTE* pDstBlock = pDstRow;

		if ((y%linesPerTileBlock) == 0)
			Prefetch( baseAddress + 4*Squish_Fast_XGAddress2DTiledOffset(0,y+linesPerTileBlock,width), kBytesPerTileBlock );

		for( UINT x = 0; x < width ; x+=4 )
		{
			int offset0 = 4*Squish_Fast_XGAddress2DTiledOffset(x,y+0,width);
			int offset1 = offset0+16;  // the tiled pixels are in groups of 4x2 blocks
			int offset2 = 4*Squish_Fast_XGAddress2DTiledOffset(x,y+2,width);
			int offset3 = offset2+16; 

			__vector4 vSrc0123           = __lvlx( baseAddress, offset0 );
			__vector4 vSrc4567           = __lvlx( baseAddress, offset1 );
			__vector4 vSrc89ab           = __lvlx( baseAddress, offset2 );
			__vector4 vSrccdef           = __lvlx( baseAddress, offset3 );

			CompressBlock( pDstBlock, vSrc0123, vSrc4567, vSrc89ab, vSrccdef );
			pDstBlock += dstBytesPerBlock;
		}
		pDstRow += dstPitch;
	}
#endif	
}

//---------------------------------------------------------------------------------------------------------
// Name: VMXCompressor::CompressTextureVMX( )
// Desc: Encode a full texture, choosing the appropriate routine to use on each 4x4 block.  
//---------------------------------------------------------------------------------------------------------

void Compress_FastVMX( BYTE * srcBase, BYTE * dstBase, int width, int height, int srcPitch, int dstPitch, int startY, int stopY, bool hasAlpha)
{
	if (stopY>height)
		stopY = height;
	if (hasAlpha)
		CompressTextureVMX<&CompressDXT5BlockVMXFLOAT>( srcBase, dstBase, (UINT)width, (UINT)srcPitch, (UINT)dstPitch, (UINT)startY, (UINT)stopY, 16 ); 
	else
		CompressTextureVMX<&CompressDXT1BlockVMXFLOAT>( srcBase, dstBase, (UINT)width, (UINT)srcPitch, (UINT)dstPitch, (UINT)startY, (UINT)stopY, 8 );
}

#endif // SQUISH_USE_FASTVMX_COMPRESSION



} // namespace rage

#endif		// !__SPU

