/* -----------------------------------------------------------------------------

	Copyright (c) 2006 Simon Brown                          si@sjbrown.co.uk

	Permission is hereby granted, free of charge, to any person obtaining
	a copy of this software and associated documentation files (the 
	"Software"), to	deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish,
	distribute, sublicense, and/or sell copies of the Software, and to 
	permit persons to whom the Software is furnished to do so, subject to 
	the following conditions:

	The above copyright notice and this permission notice shall be included
	in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
	CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
	TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
   -------------------------------------------------------------------------- */
   
#ifndef SQUISH_SIMD_VE_H
#define SQUISH_SIMD_VE_H

#include "math/intrinsics.h"

namespace squish {

#define VEC4_CONST( X ) Vec4( _vsplatf( X ) )

class Vec4
{
public:
	typedef Vec4 Arg;

	Vec4() {}
		
	explicit Vec4( __vector4 v ) : m_v( v ) {}
	
	Vec4( Vec4 const& arg ) : m_v( arg.m_v ) {}
	
	Vec4& operator=( Vec4 const& arg )
	{
		m_v = arg.m_v;
		return *this;
	}
	
	explicit Vec4( float s )
	{
		union { __vector4 v; float c[4]; } u;
		u.c[0] = s;
		u.c[1] = s;
		u.c[2] = s;
		u.c[3] = s;
		m_v = u.v;
	}
	
	Vec4( float x, float y, float z, float w )
	{
		union { __vector4 v; float c[4]; } u;
		u.c[0] = x;
		u.c[1] = y;
		u.c[2] = z;
		u.c[3] = w;
		m_v = u.v;
	}
	
	Vec3 GetVec3() const
	{
		union { __vector4 v; float c[4]; } u;
		u.v = m_v;
		return Vec3( u.c[0], u.c[1], u.c[2] );
	}
	
	Vec4 SplatX() const { return Vec4( __vspltw( m_v, 0 ) ); }
	Vec4 SplatY() const { return Vec4( __vspltw( m_v, 1 ) ); }
	Vec4 SplatZ() const { return Vec4( __vspltw( m_v, 2 ) ); }
	Vec4 SplatW() const { return Vec4( __vspltw( m_v, 3 ) ); }

	Vec4& operator+=( Arg v )
	{
		m_v = __vaddfp( m_v, v.m_v );
		return *this;
	}
	
	Vec4& operator-=( Arg v )
	{
		m_v = __vsubfp( m_v, v.m_v );
		return *this;
	}
	
	Vec4& operator*=( Arg v )
	{
		m_v = __vmulfp( m_v, v.m_v );
		return *this;
	}
	
	friend Vec4 operator+( Vec4::Arg left, Vec4::Arg right  )
	{
		return Vec4( __vaddfp( left.m_v, right.m_v ) );
	}
	
	friend Vec4 operator-( Vec4::Arg left, Vec4::Arg right  )
	{
		return Vec4( __vsubfp( left.m_v, right.m_v ) );
	}
	
	friend Vec4 operator*( Vec4::Arg left, Vec4::Arg right  )
	{
		return Vec4( __vmulfp( left.m_v, right.m_v ) );
	}
	
	//! Returns a*b + c
	friend Vec4 MultiplyAdd( Vec4::Arg a, Vec4::Arg b, Vec4::Arg c )
	{
		return Vec4( __vmaddfp( a.m_v, b.m_v, c.m_v ) );
	}
	
	//! Returns -( a*b - c )
	friend Vec4 NegativeMultiplySubtract( Vec4::Arg a, Vec4::Arg b, Vec4::Arg c )
	{
		return Vec4( __vnmsubfp( a.m_v, b.m_v, c.m_v ) );
	}
	
	friend Vec4 Reciprocal( Vec4::Arg v )
	{
		// get the reciprocal estimate
		__vector4 estimate = __vrefp( v.m_v );
		
		// one round of Newton-Rhaphson refinement
		__vector4 diff = __vnmsubfp( estimate, v.m_v, _vsplatf(1.0f) );
		return Vec4( __vmaddfp( diff, estimate, estimate ) );
	}
	
	friend Vec4 Min( Vec4::Arg left, Vec4::Arg right )
	{
		return Vec4( __vminfp( left.m_v, right.m_v ) );
	}
	
	friend Vec4 Max( Vec4::Arg left, Vec4::Arg right )
	{
		return Vec4( __vmaxfp( left.m_v, right.m_v ) );
	}
	
	friend Vec4 Truncate( Vec4::Arg v )
	{
		return Vec4( __vrfiz( v.m_v ) );
	}
	
	friend bool CompareAnyLessThan( Vec4::Arg left, Vec4::Arg right ) 
	{
#if __XENON
		unsigned int cr;
		__vcmpgtfpR( right.m_v, left.m_v, &cr );
		return (cr & 0x20) == 0;
#elif __PS3
		return vec_any_lt( left.m_v, right.m_v );
#endif
	}
	
private:
	__vector4 m_v;
};

} // namespace squish

#endif // ndef SQUISH_SIMD_VE_H
