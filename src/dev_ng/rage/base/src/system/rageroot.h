//
// system/rageroot.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SYSTEM_RAGEROOT_H
#define SYSTEM_RAGEROOT_H

/*
	This should be somewhere in the sample code hierarchy, but we currently
	have testers under base/src that need to know this as well, so keep it
	here for now.
*/

#ifndef RAGE_ASSET_ROOT
#define RAGE_ASSET_ROOT "x:/rage/assets/"
#endif

#endif		// SYSTEM_RAGEROOT_H
