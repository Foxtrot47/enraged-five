// 
// system/task_spu2.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if __SPU
#define TASK_SPU_2 (1)
#include "taskheaderspu.h"
#include "task_spu.h"
#endif
