// 
// system/main_spu.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SYSTEM_MAIN_SPU_H
#define SYSTEM_MAIN_SPU_H

extern int main(void);

#endif
