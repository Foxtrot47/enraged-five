﻿/* SIE CONFIDENTIAL
 * PlayStation(R)4 Programmer Tool Runtime Library Release 01.700.781
 * Copyright (C) 2019 Sony Interactive Entertainment Inc.
 */

#ifndef _SCE_KERNEL_CPUMODE_PLATFORM_H_
#define _SCE_KERNEL_CPUMODE_PLATFORM_H_

#include <sys/cdefs.h>
#include <sys/types.h>
#include <kernel.h>

__BEGIN_DECLS

int sceKernelIsProspero(void);

__END_DECLS

#endif
