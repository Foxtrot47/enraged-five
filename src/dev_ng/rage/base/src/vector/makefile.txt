Project vector

IncludePath $(RAGE_DIR)\base\src

Files {
	allbitsf.h
	allbitsf.cpp
	color32.cpp
	color32.h
	color64.cpp
	color64.h
	colorvector3.cpp
	colorvector3.h
	hull.cpp
	hull.h
	geometry.cpp
	geometry.h
	matrix34.cpp
	matrix34.h
	matrix33.cpp
	matrix33.h
	matrix44.cpp
	matrix44.h
	quantize.cpp
	quantize.h
	quaternion.cpp
	quaternion.h
	vector2.cpp
	vector2.h
	vector3.cpp
	vector3.h
	vector4.cpp
	vector4.h
	vec.cpp
	vec.h
	colors.h
	vecstd.h
	vectorn.h
	matrixt.h
	vectort.h
	vector3_config.h
	vector3_consts_spu.cpp
	vector3_default.h
	vector3_strings.h
	vector3_win32.h
	vector3_xenon.h
	matrix34_default.h
	matrix34_xenon.h
	matrix33_default.h
	matrix33_xenon.h
	vector4_default.h
	vector4_win32.h
	vector4_xenon.h
	quaternion_default.h
	quaternion_win32.h
	quaternion_xenon.h
}
Custom {
	vector.dtx
}
