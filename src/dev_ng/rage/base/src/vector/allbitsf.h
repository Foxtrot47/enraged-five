// 
// vector/allbitsf.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef VECTOR_ALLBITSF_H 
#define VECTOR_ALLBITSF_H 

// Intentionally not in any namespace.
extern "C" float allBitsF, finf, fnan, noBitsF;

#endif // VECTOR_ALLBITSF_H 
