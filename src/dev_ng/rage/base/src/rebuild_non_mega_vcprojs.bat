@echo off
pushd %~dp0
set started=%time% 
@echo REBUILD NON MEGA PROJECTS STARTED : %started%

if not defined RS_TOOLSCONFIG call setenv

if "%1"=="VS2008" goto VS2008

set build_script=%RS_TOOLSROOT%\script\util\projgen\rebuildRageBaseSrcLibrary.bat

call %RS_TOOLSROOT%\script\util\projGen\sync.bat

call %build_script% 	audioasiolib\makefile.txt audiodata\makefile.txt audioeffecttypes\makefile.txt audioengine\makefile.txt audiohardware\makefile.txt ^
                    	audioscriptsoundtypes\makefile.txt audiosoundtypes\makefile.txt audiosynth\makefile.txt bank\makefile.txt cranimation\makefile.txt ^
			crbody\makefile.txt creature\makefile.txt crmetadata\makefile.txt crskeleton\makefile.txt curve\makefile.txt data\makefile.txt devcam\makefile.txt ^
			diag\makefile.txt edge\makefile.txt file\makefile.txt gamespy\makefile.txt gizmo\makefile.txt grblendshapes\makefile.txt grcore\makefile.txt grcustom\makefile.txt ^
			grmodel\makefile.txt init\makefile.txt input\makefile.txt jpeg\makefile.txt math\makefile.txt mathext\makefile.txt ^
			mesh\makefile.txt net\makefile.txt paging\makefile.txt parser\makefile.txt parsercore\makefile.txt pharticulated\makefile.txt phbound\makefile.txt ^
			phbullet\makefile.txt phcore\makefile.txt pheffects\makefile.txt phsolver\makefile.txt physics\makefile.txt profile\makefile.txt qa\makefile.txt ^
			rline\makefile.txt rmcore\makefile.txt scaleform\makefile.txt shaderlib\makefile.txt shaderMaterial\makefile.txt spatialdata\makefile.txt ^
			squish\makefile.txt stb\makefile.txt string\makefile.txt system\makefile.txt text\makefile.txt vector\makefile.txt vectormath\makefile.txt xmldata\makefile.txt ^
			zlib\makefile.txt 

set HDR_PROJGEN=

if "%1"=="VS2010" goto END

:VS2008
@echo 2008 Project generation started ( Old Project Generator )
@echo https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder

pushd %RAGE_DIR%\base\samples
@call %RS_TOOLSROOT%\script\coding\projbuild\convert_dir.bat
popd

:END
popd
@echo STARTED  : %started% FINISHED : %time% 