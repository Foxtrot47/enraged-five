//
// text/hashtable.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_HASHTABLE_H
#define TEXT_HASHTABLE_H

#include "text/config.h"

namespace rage {

class txtStringData;

// txtHashEntry //

class txtHashEntry 
{
	friend class txtHashTable;
	friend class txtHashPosition;

private:
	txtHashEntry() {}
	txtHashEntry(unsigned int mHash,txtStringData *data,txtHashEntry *next);

	unsigned int mHash;
	txtStringData *mData;
	txtHashEntry *mNext;
};

// txtHashPosition //

class txtHashPosition
{
	friend class txtHashTable;

public:
	txtHashPosition()					{mEntry=NULL;mSlotNum=0;}
	txtStringData *GetData() const		{return mEntry?mEntry->mData:NULL;}

private:
	txtHashEntry *mEntry;
	int mSlotNum;
};

// txtHashTable //

class txtHashTable
{
public:

	txtHashTable(int numEntries=100);
	~txtHashTable();					// this calls DeleteEntries()

	void DeleteEntries();					// only deletes the hash entries - does not delete the data they pointed to (txtStringData)
	void DeleteEntriesAndData();
	void DeleteEntry(unsigned int hash);	// only deletes the entry in this hash - does not delete the data they pointed to (txtStringData)

	txtStringData *Access(unsigned int hash);
	bool Insert(unsigned int hash,txtStringData *data);

	bool GetFirstEntry(txtHashPosition &pos);
	bool GetNextEntry(txtHashPosition &pos);

	int GetNumEntries() const					{return mNumEntries;}

private:
	int ComputePrime(int n);

	int mNumSlots;
	txtHashEntry **mSlots;
	int mNumEntries;
};

}	// namespace rage

#endif

