//
// text/stringtable.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stringtable.h"
#include "hashtable.h"
#include "stringtex.h"
#include "fonttex.h"
#include "diag/memstats.h"
#include "file/asset.h"
#include "system/alloca.h"
#include "string/stringhash.h"


extern int assetDebug;				// defined in data/asset.cpp

using namespace rage;

const char *txtStringTable::sm_DefaultFont=NULL;



// txtHashTable::DeleteEntriesAndData() //

void txtHashTable::DeleteEntriesAndData()
{
	for (int i=0;i<mNumSlots;i++)
	{
		while (mSlots[i])
		{
			txtHashEntry *next=mSlots[i]->mNext;

//			if (txtFontTex::GetHashtable().GetNumEntries()==0)
//			{
//				Errorf("You need to call STRINGTABLE.Kill() before calling setup.DestroyFactories(); setup.EndGfx() or HashTable::KillAll()");
//				return;
//			}

			delete mSlots[i]->mData;

			delete mSlots[i];
			mSlots[i]=next;
		}
	}
}



// STRINGTABLE //

txtStringTable rage::STRINGTABLE;

txtStringTable::eMissingMode txtStringTable::sm_MissingMode=txtStringTable::kWARNING;



// txtStringData //

txtStringData::txtStringData()
{
	m_Hash=0;
	m_Font=NULL;
	m_String=NULL;
	m_Scale.Set(1,1);
	m_OffsetX=m_OffsetY=0;
	m_Flags=0x0001;

#if __DEV
	m_MemoryAllocated = 0;
#endif

}



txtStringData::~txtStringData()
{
	if (m_String)
		delete []m_String;

	if (m_Font)
	{
		if (((size_t)m_Font)&0x1)
			delete[] (char*)(size_t(m_Font)&~0x1);
		else
			txtFreeFontTex(m_Font);
	}
}



bool txtStringData::Load(fiStream& stream,unsigned int flags,bool loadFont , eStringType stringType)
{
	stream.ReadInt(&m_Hash,1);
	stream.ReadShort(&m_Flags,1);

	Assert(!m_Font);

	int fontLength=0;
	char *font=NULL;
	stream.ReadInt(&fontLength,1);
	if (fontLength)
	{
		font=Alloca(char,fontLength+1);
		stream.Read(font,fontLength);
		font[fontLength]='\0';
	}

	int stringLength=0;
	stream.ReadInt(&stringLength,1);
	char16 *tempString=Alloca(char16,stringLength+1);
	stream.ReadShort(tempString,stringLength);

	stream.ReadFloat(&m_Scale.x,2);
	stream.Read(&m_OffsetX,sizeof(m_OffsetX));		// s8
	stream.Read(&m_OffsetY,sizeof(m_OffsetY));		// s8

	if (m_Flags & flags)
	{
		if (loadFont)
			m_Font=txtGetFontTex(font,false);

		if(stringType == UTF8)
		{
			char* tempUTF8String=Alloca(char,(stringLength*6)+1);//probably never encounter a 6 byte char but playing it safe
			WideToUtf8(tempUTF8String,tempString,stringLength);
			m_StringUTF8=StringDuplicate(tempUTF8String);
#if __DEV
			m_MemoryAllocated += (int)strlen(m_StringUTF8);//note that strlen should be # of bytes and not necessarily # of letters
#endif
		}
		else
		{
			m_String=WideStringDuplicate(tempString);
#if __DEV
			m_MemoryAllocated += (int)wcslen(m_String);
#endif
		}
		
		
		return true;
	}
	else
		return false;
}


#if __DEV
void txtStringData::Save(fiStream& stream)
{	
	stream.WriteInt(&m_Hash,1);
	stream.WriteShort(&m_Flags,1);

	if (m_Font)
	{
		const char *fontName;

		if (m_Font && size_t(m_Font)&0x1)
			fontName=(const char*)(size_t(m_Font)&~0x1);
		else
			fontName=m_Font->GetName();

		int length=(int)strlen(fontName);
		stream.WriteInt(&length,1);
		stream.Write(fontName,length);
	}
	else
	{
		int length=0;
		stream.WriteInt(&length,1);
	}

	int length=(int)wcslen(m_String)+1;
	stream.WriteInt(&length,1);
	stream.WriteShort(m_String,length);

	stream.WriteFloat((float*)&m_Scale,2);
	stream.Write(&m_OffsetX,sizeof(m_OffsetX));		// s8
	stream.Write(&m_OffsetY,sizeof(m_OffsetY));		// s8
}
#endif



txtFontTex *txtStringData::GetFont() const
{
	if (m_Font && !(size_t(m_Font)&0x1))
		return m_Font;
	else
		return NULL;
}



const char *txtStringData::GetFontName() const
{
	if (m_Font && size_t(m_Font)&0x1)
		return (const char*)(size_t(m_Font)&~0x1);
	else
		return NULL;
}



// txtStringTable //

txtStringTable::txtStringTable()
{
	m_HashTable=NULL;

	m_Identifiers=NULL;
	m_NumIdentifiers=0;
#if __DEV
	m_MemoryAllocated = 0;
#endif
}



txtStringTable::~txtStringTable()
{
	if (m_HashTable)
		Warningf("Call Kill() before destructing STRINGTABLE (harmless if we were already crashing for some other reason)");
}



void txtStringTable::Init()
{
	AssertMsg(!m_HashTable , "Call Kill() before Init()");

	m_HashTable=rage_new txtHashTable;
}



void txtStringTable::Kill()
{
	AssertMsg(m_HashTable , "Call Init() or Load() before Kill()");

	m_HashTable->DeleteEntriesAndData();
	delete m_HashTable;

	m_HashTable=NULL;

	for (int i=0;i<m_NumIdentifiers;i++)
		delete []m_Identifiers[i];

	delete m_Identifiers;

	m_NumIdentifiers=0;
	m_Identifiers=NULL;
#if __DEV
	m_MemoryAllocated = 0;
#endif
}


#if __WIN32
void txtStringTable::Print()
{
	txtHashPosition position;
	AssertVerify(m_HashTable->GetFirstEntry(position));
	USES_CONVERSION;

	char16 *formatString=A2W("font='%s'  string='%s'  scale=%0.2f %0.2f\n");

	do
	{
		txtStringData *stringData=position.GetData();
		Assert(stringData);

#if __UNICODE
	const char16 *_tstring=stringData->m_String;
	if (hasWides(_tstring))
		_tstring=(char16*)"\0[\0un\0pr\0in\0ta\0bl\0e]\0";
#else
	const char *_tstring=stringData->m_String;
#endif

		wprintf((wchar_t*)formatString,stringData->GetFontName(),_tstring,stringData->m_Scale.x,stringData->m_Scale.y);

	} while (m_HashTable->GetNextEntry(position));

	if (m_NumIdentifiers)
	{
		Displayf("\n--- Identifiers ---");

		for (int i=0;i<m_NumIdentifiers;i++)
		{
			Displayf("%s",m_Identifiers[i]);
		}
	}
}
#endif



bool txtStringTable::Load(const char *filename,txtLanguage::eLanguage language,unsigned int flags,bool loadIdentifiers, bool append, bool loadFont, bool preloadFile , eStringType stringType)
{
#if __ASSERT
	if(!append)															// see if we would like to append to an already existing stringtable
		AssertMsg(!m_HashTable , "Call Kill() before Load()");				// if not, then assert if one already exists
#endif
	diagLogMemoryMessage("Loading Stringtable");
	diagLogMemoryPush();
	if(!append || (append && !m_HashTable))								// if we are not appending-> create new hashtable. Or if we are appending but don't have an existing one, create one, too
		m_HashTable=rage_new txtHashTable;
	diagLogMemory("Hash Table");

	fiStream *stream = ASSET.Open(filename,"strtbl");

	if (!stream)
	{
		Errorf("Failed to load stringtable %s",filename);
		return false;
	}

	if (preloadFile)
	{
		stream = fiStream::PreLoad(stream);
		Assert(stream!=NULL);
	}

	int numLanguages=0;
	stream->ReadInt(&numLanguages,1);
	Assert(numLanguages==txtLanguage::GetCount());		// usually detects an old .strtbl file

	int seekPosition=0;
	for (int l=0;l<numLanguages;l++)
	{
		int temp;
		stream->ReadInt(&temp,1);
		if (l==language)
			seekPosition=temp;
	}

	unsigned int version;

	stream->ReadInt(&version,1);

	Assert(version>0x0100 && version<0x1000);

	if (loadIdentifiers)
	{
		stream->ReadInt(&m_NumIdentifiers,1);
		m_Identifiers=rage_new char*[m_NumIdentifiers];
#if __DEV
		m_MemoryAllocated += m_NumIdentifiers;
#endif
		for (int i=0;i<m_NumIdentifiers;i++)
		{
			int length;
			stream->ReadInt(&length,1);
			m_Identifiers[i]=rage_new char[length+1];
#if __DEV
			m_MemoryAllocated += (length+1);
#endif
			stream->Read(m_Identifiers[i],length+1);
			m_Identifiers[i][length]='\0';
		}
	}

	if (seekPosition==0)
	{
		Errorf("No data for language %d",language);
		stream->Close();
		return false;
	}

	stream->Seek(seekPosition);

	int num_Strings=0;
	stream->ReadInt(&num_Strings,1);

	for (int i=0;i<num_Strings;i++)
	{
		txtStringData temp;
		if (temp.Load(*stream,flags,loadFont,stringType))
		{
			txtStringData *stringData=rage_new txtStringData;
			memcpy(stringData,&temp,sizeof(temp));
			unsigned int hash = stringData->GetHash();
			if (!m_HashTable->Insert(hash,stringData) && append)
			{
				m_HashTable->DeleteEntry(hash);
				m_HashTable->Insert(hash,stringData);
			}
#if __DEV
			m_MemoryAllocated += sizeof(temp);
#endif
		}
		temp.m_Font=NULL;		// these are so the destructor
		temp.m_String=NULL;		// won't try and deallocate these
	}

	stream->Close();

	diagLogMemoryPop();
	diagLogMemory("Stringtable Total");

	if (num_Strings==0)
	{
		Errorf("No translations available for %s",txtLanguage::Get(language));
		return false;
	}

	return true;
}

#if __DEV
int	txtStringTable::GetMemoryAllocated()
{
	int memoryAllocated=0;

	txtHashPosition position;
	AssertVerify(m_HashTable->GetFirstEntry(position));

	do
	{
		txtStringData *stringData=position.GetData();
		Assert(stringData);

		memoryAllocated += stringData->GetMemoryAllocated();

	} while (m_HashTable->GetNextEntry(position));

	memoryAllocated += m_MemoryAllocated;

	return memoryAllocated;
}
#endif

#if __DEV
bool txtStringTable::Save(fiStream& stream)
{
	int num_Strings=m_HashTable->GetNumEntries();
	
	stream.WriteInt(&num_Strings,1);

	txtHashPosition position;
	AssertVerify(m_HashTable->GetFirstEntry(position));
	do
	{
		txtStringData *stringData=position.GetData();
		Assert(stringData);

		stringData->Save(stream);

	} while (m_HashTable->GetNextEntry(position));

	return true;
}
#endif


txtStringData *txtStringTable::Get(u32 hash)
{
	char	buffer[64];

	if (!m_HashTable)
	{
		Errorf("Must call STRINGTABLE.Init() or STRINGTABLE.Load() before txtStringTable::Get()");
		return NULL;
	}

	txtStringData *stringData=(txtStringData*)m_HashTable->Access(hash);

	if (!stringData)
	{
		USES_CONVERSION;

		if ((sm_MissingMode==kWARNING) || (sm_MissingMode==kHIGHLIGHT))
			Warningf("String with hash value = '%d' is not in stringtable (creating temporary entry).",hash);
		else if (sm_MissingMode==kQUIT)
			Quitf("String with hash value = '%d' is not in stringtable",hash);
		
		if (sm_MissingMode==kNOCREATE)
		{
			//do nothing. just return NULL.  let the client handle missing strings
			return NULL;
		}


		char16 *_tstring;
		if (sm_MissingMode==kHIGHLIGHT)
		{
			sprintf(buffer,"!!%d!!",hash);
			_tstring=A2W(buffer);
		}
		else
		{
			formatf(buffer,sizeof(u32)+1,"%d",hash);
			_tstring=A2W((char*)buffer);
		}
		stringData=rage_new txtStringData;
		stringData->m_String=WideStringDuplicate(_tstring);
		stringData->SetHash(hash);
		if (sm_DefaultFont)
			stringData->m_Font=txtGetFontTex(sm_DefaultFont);
		m_HashTable->Insert(stringData->GetHash(),stringData);

		//		return NULL;									// HACK - enable this for release?
	}

	return stringData;
}

txtStringData *txtStringTable::Get(const txtCharString string)
{
	if (!m_HashTable)
	{
		Errorf("Must call STRINGTABLE.Init() or STRINGTABLE.Load() before txtStringTable::Get()");
		return NULL;
	}

	unsigned int hash=Hash(string);

	txtStringData *stringData=(txtStringData*)m_HashTable->Access(hash);

	if (!stringData)
	{
		USES_CONVERSION;

		if ((sm_MissingMode==kWARNING) || (sm_MissingMode==kHIGHLIGHT))
			Warningf("String '%s' not in stringtable (creating temporary entry).",string);
		else if (sm_MissingMode==kQUIT)
			Quitf("String '%s' not in stringtable",string);

		if (sm_MissingMode==kNOCREATE)
		{
			//do nothing. just return NULL.  let the client handle missing strings
			return NULL;
		}

		char16 *_tstring;
		if (sm_MissingMode==kHIGHLIGHT)
		{
			char *buffer = Alloca(char, strlen(string)+5);
			strcpy(buffer,"!!");
			strcat(buffer,string);
			strcat(buffer,"!!");
			_tstring=A2W(buffer);
		}
		else
			_tstring=A2W((char*)string);

		stringData=rage_new txtStringData;
		stringData->m_String=WideStringDuplicate(_tstring);
		stringData->SetHash(Hash(string));
		if (sm_DefaultFont)
			stringData->m_Font=txtGetFontTex(sm_DefaultFont);
		m_HashTable->Insert(stringData->GetHash(),stringData);

//		return NULL;									// HACK - enable this for release?
	}

	return stringData;
}



bool txtStringTable::Exists(const txtCharString string) const
{
	if (!m_HashTable)
	{
		Errorf("Must call STRINGTABLE.Init() or STRINGTABLE.Load() before txtStringTable::Exists()");
		return false;
	}

	unsigned int hash=Hash(string);

	txtStringData *stringData=(txtStringData*)m_HashTable->Access(hash);

	return stringData?true:false;
}



bool txtStringTable::Add(txtStringData *stringData)
{
	return m_HashTable->Insert(stringData->m_Hash,stringData);
}


unsigned int txtStringTable::Hash(const char *identifier)
{
	return atStringHash(identifier,0);
}

bool txtStringTable::GetNextEntry(txtHashPosition &pos)
{
	return m_HashTable->GetNextEntry(pos);
}

bool txtStringTable::GetFirstEntry(txtHashPosition &pos)
{
	return m_HashTable->GetFirstEntry(pos);
}
