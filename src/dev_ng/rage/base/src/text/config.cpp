//
// text/config.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "config.h"

namespace rage {

// Helpers //

#if __UNICODE
bool hasWides(const char16 *str)
{
	for (const char16 *chr=str;*chr;chr++)
		if (*chr>127)
			return true;

	return false;
}
#else
bool hasWides(const char16*) {return false;}
#endif

}	// namespace rage
