//
// text/cursor.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "cursor.h"

// txtCursor //

using namespace rage;

txtCursor::txtCursor()
{
	m_Attributes=kLeft | kLineMode | kRepositionX | kRepositionY | kClipped;

	m_CursorX=0;
	m_CursorY=0;

	m_Top=0;
	m_Bottom=9999;
	m_Left=0;
	m_Right=9999;

	m_SubstringStart=0;
	m_SubstringEnd=9999;

	m_MonospaceWidth=0;

	m_ExtentX=m_ExtentY=0;

	m_ScaleX=m_ScaleY=1.f;

	m_Color=Color32(255,255,255);
	m_ShadowColor=Color32(255,255,255);

//	m_TexColorOp=texopModulate;
}

void txtCursor::Print(txtStringTex &/*string*/)
{
}

void txtCursor::PrintLine(txtStringTex &/*string*/)
{
}

void txtCursor::PrintCR()
{
}

//void txtCursor::SetTexColorOp(gfxTextureOp op) {
//	m_TexColorOp=op;
//}
