//
// text/stringtable.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_STRINGTABLE_H
#define TEXT_STRINGTABLE_H

#include "config.h"
#include "language.h"

#include "vector/vector2.h"

namespace rage {

class txtHashTable;
class txtHashPosition;
class txtFontTex;
class fiStream;

enum eStringType
{
	UNICODE,
	UTF8
};

//
// PURPOSE:
//   Describes an entry in the string table.
// <FLAG Component>
class txtStringData {

	friend class txtStringTable;
	friend class txtStringEditor;

public:
	// PURPOSE: default constructor
	txtStringData();
	// PURPOSE: destructor
	~txtStringData();

	//
	// PURPOSE
	//	loads an entry from the stringtable
	// PARAMS
	//	stream - the stream we load from
	//	flags - specify the flags that we support
	//	loadFont - true if we should load the font needed by the stringtable entry
	// RETURNS
	//	returns true if load was successful
	//
	bool Load(fiStream& stream,unsigned int flags,bool loadFont=true , eStringType stringType = UNICODE);
	//
	// PURPOSE
	//	saves the string table entry
	// PARAMS
	//	stream - the stream we save to
	//
#if __DEV
	void Save(fiStream& stream);
	int GetMemoryAllocated(){return m_MemoryAllocated;}
#endif

	// PURPOSE: accessor for the name of the font for this stringtable entry
	// RETURNS: the name of the font
	const char *GetFontName() const;
	// PURPOSE: accessor for the font for this stringtable entry
	// RETURNS: the font
	txtFontTex *GetFont() const;

	// PURPOSE: accessor for the string table entry's hash value
	// RETURNS: the hash entry
	unsigned int GetHash() const;
	//
	// PURPOSE
	//	set the hash value for this string table entry
	// PARAMS
	//	hash - the hash value to set
	//
	void SetHash(unsigned int hash);	

	// PURPOSE: accessor for the x value of the display offset 
	// RETURNS: the x display offset
	unsigned char GetOffsetX() const;
	// PURPOSE: accessor for the y value of the display offset 
	// RETURNS: the y display offset
	unsigned char GetOffsetY() const;
	// PURPOSE: accessor for flags for this string table entry
	// RETURNS: the flags
	unsigned short GetFlags() const;
	// PURPOSE: accessor for the scale value of this stringtable entry
	// RETURNS: a reference to a vector that holds the x and y scale values
	const Vector2& GetScale() const;	
	// PURPOSE: accessor for the Unicode string
	// RETURNS: returns the Unicode string	
	const char16* GetString() const;	
	// PURPOSE: accessor for the Unicode string
	// RETURNS: returns the Unicode string
	const char16* GetStringUnicode() const;	
	// PURPOSE: accessor for the UTF8 string
	// RETURNS: returns the UTF8 string
	const char* GetStringUTF8() const;	

private:
	unsigned int	m_Hash;
	txtFontTex*		m_Font;	

	union
	{
		char16* m_String;
		char* m_StringUTF8;
	};
	
	Vector2			m_Scale;
	signed char		m_OffsetX,m_OffsetY;
	unsigned short	m_Flags;

#if __DEV
	int				m_MemoryAllocated;
#endif
};

inline unsigned int txtStringData::GetHash() const	
{
	return m_Hash;
}

inline void txtStringData::SetHash(unsigned int hash)			
{
	m_Hash=hash;
}

inline unsigned char txtStringData::GetOffsetX() const			
{
	return m_OffsetX;
}

inline unsigned char txtStringData::GetOffsetY() const			
{
	return m_OffsetY;
}
inline unsigned short txtStringData::GetFlags() const				
{
	return m_Flags;
}

inline const Vector2& txtStringData::GetScale() const				
{
	return m_Scale;
}

inline const char16* txtStringData::GetString() const				
{
	return m_String;
}

inline const char16* txtStringData::GetStringUnicode() const				
{
	return m_String;
}

inline const char* txtStringData::GetStringUTF8() const				
{
	return m_StringUTF8;
}

//
// PURPOSE:
//   A table of string translations that take keys and return you the string in the current selected language.
// <FLAG Component>
class txtStringTable 
{
	friend class txtStringEditor;
	friend class CFontToolDlg;

public:

	// PURPOSE: default constructor
	txtStringTable();
	// PURPOSE: destructor
	~txtStringTable();

	// PURPOSE: clears out the string table and the memory it uses
	void Kill();

	// PURPOSE: allocates memory for the string table
	void Init();
	//
	// PURPOSE
	//	Loads a stringtable from a file
	// PARAMS
	//	filename - the name of the file to load the string table from
	//	language - the language of the translation that we'd like to load
	//	flags - the flags mask that is passed to txtStringData::Load()
	//	loadIdentifiers - whether or not to read in the identifiers
	//	append - true if we append to the currently loaded stringtable, false if not
	//	loadFont - true if we should load any fonts specified in the stringtable
	// RETURNS
	//	returns true if translations were loaded, false if not
	//
	bool Load(const char *filename,txtLanguage::eLanguage language=txtLanguage::kEn,unsigned int flags=0xffffffff,bool loadIdentifiers=false, bool append = false, bool loadFont=true, bool preloadFile=true, eStringType stringType=UNICODE);
	//
	// PURPOSE
	//	saves the string table out to a stream
	// PARAMS
	//	stream - the stream we want to save to
	// RETURNS
	//	returns true if successful, false if not
	//
#if __DEV
	bool Save(fiStream& stream);
	int GetMemoryAllocated();
#endif

	//
	// PURPOSE
	//	a function that translations a key to a translated string
	// PARAMS
	//	string - the string we wish to translate
	// RETURNS
	//	the translated string
	//
	txtStringData *Get(const txtCharString string);

	//
	// PURPOSE
	//	a function that translations a key to a translated string
	// PARAMS
	//	hash - the hash used to retrieve the string we wish to translate
	// RETURNS
	//	the translated string
	//
	txtStringData *Get(rage::u32 hash);

	//
	// PURPOSE
	//	checks to see if the key exists in the stringtable
	// PARAMS
	//	string - the key to check
	// RETURNS
	//	true if the key exists in the stringtable, false if not
	//
	bool Exists(const txtCharString string) const;

	// PURPOSE: prints out all entries in the string table
	void Print();

	//
	// PURPOSE
	//	returns the i'th identifier
	// PARAMS
	//	i - the index of the identifier to return
	// RETURNS
	//	a pointer to the identifier
	//
	const char *GetIdentifier(int i) const;
	// PURPOSE: accessor for the number of identifiers
	// RETURNS: the number of identifiers
	int GetNumIdentifiers() const;

	// PURPOSE: returns true if the hash table for the stringtable has been allocated
	// RETURNS: true if the hash table has been allocated, false if not
	bool IsValid() const;

	//
	// PURPOSE
	//	compute the hash value for a string
	// PARAMS
	//	key - the string to compute the hash value for
	// RETURNS
	//	the computed hash value
	//
	static unsigned int Hash(const char* key);

	//
	// PURPOSE
	//	return the first string in the string table (not necessarily in alphabetical order, could be random)
	// PARAMS
	//	pos - will be populated with the first position data
	// RETURNS
	//	returns true if a first entry exists, false if no strings exist
	//
	bool GetFirstEntry(txtHashPosition &pos);

	//
	// PURPOSE
	//	retrieves the next string that is listed after the given pos (not necessarily in alphabetical order, could be random)
	// PARAMS
	//	pos - will be populated with the first position data
	// RETURNS
	//	returns true if a next entry exists false otherwise
	//
	bool GetNextEntry(txtHashPosition &pos);

	enum eMissingMode 
	{
		kIGNORE,		// don't print a message if the translation for the key in the loaded language doesn't exist
		kWARNING,	// print a warning message if the translation for the key in the loaded language doesn't exist
		kHIGHLIGHT,	// highlight keys that don't have translations for the loaded language
		kQUIT,		// print an error message and quit if the translation for the key in the loaded language doesn't exist
		kNOCREATE	// does not create a string and add to table if it doesn't exist.  does not print out anything. (for clients who handle their own missing strings)
	};

	// PURPOSE: set the mode for processing missing translations to Quit mode
	static void SetMissingModeQuit();
	// PURPOSE: set the mode for processing missing translations to Warning mode
	static void SetMissingModeWarning();
	// PURPOSE: set the mode for processing missing translations to Highlight mode
	static void SetMissingModeHighlight();
	// PURPOSE: set the mode for processing missing translations to Ignore mode
	static void SetMissingModeIgnore();
	// PURPOSE: set the mode for processing missing translations to Ignore mode
	static void SetMissingModeNoCreate();

	// PURPOSE
	//	sets the mode for processing missing translations
	// PARAMS
	//	mode - the mode to set
	//
	static void SetMissingMode(eMissingMode mode);

	// PURPOSE: returns the currently set missing mode
	// RETURNS: returns the currently set missing mode
	static eMissingMode GetMissingMode();

	//
	// PURPOSE
	//	set the name of the default font
	// PARAMS
	//	font - the name of the default font
	//
	static void SetDefaultFont(const char* font);

private:

	bool Add(txtStringData *stringData);

	txtHashTable*	m_HashTable;
	char**			m_Identifiers;				// only used in certain testers
	int				m_NumIdentifiers;			//

#if __DEV
	int				m_MemoryAllocated;
#endif

	static eMissingMode sm_MissingMode;
	static const char*	sm_DefaultFont;
};

extern txtStringTable STRINGTABLE;

inline const char* txtStringTable::GetIdentifier(int i) const
{
	return m_Identifiers ? m_Identifiers[i] : NULL;
}
inline int txtStringTable::GetNumIdentifiers() const		
{
	return m_NumIdentifiers;
}

inline bool txtStringTable::IsValid() const		
{
	return m_HashTable?true:false;
}

inline void txtStringTable::SetMissingModeQuit()	
{
	sm_MissingMode=kQUIT;
}

inline void txtStringTable::SetMissingModeWarning()	
{
	sm_MissingMode=kWARNING;
}

inline void txtStringTable::SetMissingModeHighlight()
{
	sm_MissingMode=kHIGHLIGHT;
}

inline void txtStringTable::SetMissingModeIgnore()	
{
	sm_MissingMode=kIGNORE;
}

inline void txtStringTable::SetMissingModeNoCreate()	
{
	sm_MissingMode=kNOCREATE;
}

inline txtStringTable::eMissingMode txtStringTable::GetMissingMode()
{
	return sm_MissingMode;
}

inline void txtStringTable::SetMissingMode(eMissingMode mode)
{
	sm_MissingMode = mode;
}

inline void txtStringTable::SetDefaultFont(const char *font)	
{
	sm_DefaultFont=font;
}

}	// namespace rage


#endif

