//
// text/stringtex.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_STRINGTEX_H
#define TEXT_STRINGTEX_H

#include "atl/array.h"
#include "atl/bitset.h"
#include "atl/functor.h"
#include "atl/map.h"
#include "atl/string.h"
#include "text/config.h"
#include "vector/color32.h"

namespace rage {

//#############################################################################

class txtStringData;
class txtCursor;
class Vector3;

//#############################################################################

// PURPOSE: A class for holding and drawing a txtStringData's unformatted text.
class txtStringTex {

public:

	txtStringTex();
    txtStringTex(txtCharString string);

    virtual ~txtStringTex();
	
    // PURPOSE: Provide the Identifier used to resolve the txtStringData item in
    //    STRINGTABLE.
    // PARAMS:
    //    string - the Identifier
	virtual void SetString( txtCharString string );

    // PURPOSE: Get the txtStringData associated with this object.
    // RETURNS: The txtStringData.
    const txtStringData *GetStringData() const;

    // PURPOSE: Draw the txtStringData at the given location with the given cursor.
    // PARAMS:
    //    x - the x position.
    //    y - the y position.
    //    cursor - the cursor to use.
    //    dontDraw - whether or not to actually draw the text.
	virtual void Draw(float x,float y,txtCursor *cursor=NULL,bool dontDraw=false);

    // PURPOSE: Draw the txtStringData at the given location with the given cursor.
    // PARAMS:
    //    x - the x position.
    //    y - the y posiiton.
    //    cursor - the cursor to use.
    //    dontDraw - whether or not to actually draw the text.
	inline void Draw(int x,int y,txtCursor *cursor=NULL,bool dontDraw=false);

    // PURPOSE: Draw the txtStringData at the given location in 3D space with the given cursor.
    // PARAMS:
    //    position - the position.
    //    scaleDistance - whether or not to scale the text with the distance from the screen.
    //    cursor - the cursor to use.
    //    dontDraw - whether or not to actually draw the text.
	virtual void Draw3D(const Vector3 &position,bool scaleDistance=true,txtCursor *cursor=NULL,bool dontDraw=false);

    // PURPOSE: Calculates the height and width needed to draw the txtStringData with the given cursor.
    // PARAMS:
    //    xsize - the address of where to store the width
    //    ysize - the address of where to store the height
    //    cursor - the cursor to use.
	virtual void ComputeExtents(int *xsize,int *ysize,txtCursor *cursor=NULL);

#if __DEV
    // PURPOSE: Draw a box around the text to visualize how much screen space it takes up. 
    // PARAMS:
    //    x - the x position.
    //    y - the y posiiton.
    //    cursor - the cursor to use.
	virtual void DrawExtents(float x,float y,txtCursor *cursor=NULL);
	
    // PURPOSE: Draw the cursor
    // PARAMS:
    //    x - the x position.
    //    y - the y posiiton.
    //    cursor - the cursor to use.
    virtual void DrawCursor(float x,float y,txtCursor *cursor=NULL);
#endif

protected:
	const txtStringData *mStringData;
};

inline void txtStringTex::Draw(int x,int y,txtCursor *cursor,bool dontDraw)
{
	Draw(float(x),float(y),cursor,dontDraw);
}

inline const txtStringData* txtStringTex::GetStringData() const
{
    return mStringData;
}

//#############################################################################

// PURPOSE: A class for parsing the special subtitle string format that is output
//    from the Subtitle Editor (and the forthcoming Localized Text Editor).
class txtFormattedStringParser
{
public:
    // PURPOSE: The functor for resolving game-specific variables.
    // PARAMS:
    //    txtCharString - the variable name
    //    char16* - the buffer to return the value in
    //    int - the length of the buffer
    // RETURNS: true if found, otherwise false
    typedef Functor3Ret<bool, txtCharString, char16 *, int> GetStringTableVariableFunc;

    txtFormattedStringParser();
    ~txtFormattedStringParser();

    // PURPOSE: The structure that contains information for drawing a single
    //  span of subtitle text
    struct SpanData
    {
        SpanData()
            : string(NULL)
            , color(255,255,255)
        {
            format.Set( AUTO_COLOR );
        }

        ~SpanData()
        {
            if ( string != NULL )
            {
                delete [] string;
                string = NULL;
            }
        }

        enum EFormat
        {
            AUTO_COLOR,
            BOLD,
            ITALIC,
            UNDERLINE
        };

        char16* string;
        Color32 color;
        atFixedBitSet8 format;
    };

    // PURPOSE: Parses the given wide string.
    // PARAMS:
    //  string - the wide string to parse
    void Parse( const char16* string );

    // PURPOSE: Gets the number of lines that was parsed.
    // RETURNS: The number of lines.
    int GetNumLines() const;

    // PURPOSE: Retrieves the list of SpanData items for the given line.
    // PARAMS:
    //  line - the line index
    // RETURNS: The list of SpanData items.
    const atArray<const SpanData *>& GetLine( int line ) const;

    // PURPOSE: Gets the number SpanData items in the given line.
    // PARAMS:
    //  line - the line index
    // RETURNS: The number of items in the line.
    int GetNumInLine( int line ) const;

    // PURPOSE: Retrieves the SpanData for the given line and index.
    // PARAMS:
    //  line - the line index
    //  index - the index in the line
    // RETURNS: The SpanData.
    const SpanData& GetSpanData( int line, int index ) const;    

    // PURPOSE: Sets the external function for resolving game-specific variables
    // PARAMS:
    //    func - the functor
    // NOTES:  This is only called if the variable is not in the STRINGTABLE.
    static void SetGetStringTableVariableFunc( GetStringTableVariableFunc func );

protected:
    void ParseInternal( const char16* string, atMap<atString, atString> &resolvedItems );

    static void ResolveVariables( char* output, int outputLen, const char* input, atMap<atString, atString> &resolvedItems );

    static bool GetStringTableVariable( txtCharString var, char16 *val, int valLen, atMap<atString, atString> &resolvedItems );

    static bool DefaultGetStringTableVariable( txtCharString var, char16 *val, int valLen );

    static GetStringTableVariableFunc sm_getStringTableVariableFunc;

    atArray< atArray<const SpanData *> > mSpanDataLines;
};

inline void txtFormattedStringParser::Parse( const char16* string )
{
    atMap<atString, atString> resolvedItems;
    ParseInternal( string, resolvedItems );
}

inline int txtFormattedStringParser::GetNumLines() const
{
    return mSpanDataLines.GetCount();
}

inline const atArray<const txtFormattedStringParser::SpanData *>& txtFormattedStringParser::GetLine( int line ) const
{
    return mSpanDataLines[line];
}

inline int txtFormattedStringParser::GetNumInLine( int line ) const
{
    return mSpanDataLines[line].GetCount();
}

inline const txtFormattedStringParser::SpanData& txtFormattedStringParser::GetSpanData( int line, int index ) const
{
    return *(mSpanDataLines[line][index]);
}

inline void txtFormattedStringParser::SetGetStringTableVariableFunc( txtFormattedStringParser::GetStringTableVariableFunc func )
{
    sm_getStringTableVariableFunc = func;
}

inline bool txtFormattedStringParser::DefaultGetStringTableVariable( txtCharString /*var*/, char16* /*val*/, int /*valLen*/ )
{
    return false;
}

//#############################################################################

// PURPOSE: A class for holding and displaying a txtStringData's formatted text.  
class txtFormattedStringTex : public txtStringTex
{
public:
    txtFormattedStringTex();
    txtFormattedStringTex( txtCharString string );

    virtual ~txtFormattedStringTex();

    // PURPOSE: Provide the Identifier used to resolve the txtStringData item in
    //    STRINGTABLE.
    // PARAMS:
    //    string - the Identifier
    virtual void SetString( txtCharString string );

    // PURPOSE: Draw the txtStringData at the given location with the given cursor.
    // PARAMS:
    //    x - the x position.
    //    y - the y position.
    //    cursor - the cursor to use.
    //    dontDraw - whether or not to actually draw the text.
    // NOTES:
    //    The text is drawn using x as the center and y as the bottom, so the provided position
    //    should typically be the middle of the screen at the bottom of the safe display zone.
    virtual void Draw( float x, float y, txtCursor *cursor=NULL, bool dontDraw=false );

    // PURPOSE: Draw the txtStringData at the given location in 3D space with the given cursor.
    // PARAMS:
    //    position - the position.
    //    scaleDistance - whether or not to scale the text with the distance from the screen.
    //    cursor - the cursor to use.
    //    dontDraw - whether or not to actually draw the text.
    // NOTES:
    //    The text is drawn using x as the center and y as the bottom, so the provided position
    //    should typically be the middle of the screen at the bottom of the safe display zone.
    virtual void Draw3D( const Vector3 &position, bool scaleDistance=true, txtCursor *cursor=NULL, bool dontDraw=false );

    // PURPOSE: Calculates the height and width needed to draw the txtStringData with the given cursor.
    // PARAMS:
    //    xsize - the address of where to store the width
    //    ysize - the address of where to store the height
    //    cursor - the cursor to use.
    virtual void ComputeExtents( int *xsize, int *ysize, txtCursor *cursor=NULL );

#if __DEV
    // PURPOSE: Draw a box around the text to visualize how much screen space it takes up. 
    // PARAMS:
    //    x - the x position.
    //    y - the y posiiton.
    //    cursor - the cursor to use.
    // NOTES:
    //    The text is drawn using x as the center and y as the bottom, so the provided position
    //    should typically be the middle of the screen at the bottom of the safe display zone.
    virtual void DrawExtents( float x, float y, txtCursor *cursor=NULL );

    // PURPOSE: Draw the cursor
    // PARAMS:
    //    x - the x position.
    //    y - the y posiiton.
    //    cursor - the cursor to use.
    // NOTES:
    //    The text is drawn using x as the center and y as the bottom, so the provided position
    //    should typically be the middle of the screen at the bottom of the safe display zone.
    virtual void DrawCursor( float x, float y, txtCursor *cursor=NULL );
#endif

protected:

    enum EDrawMode
    {
        DRAW,
        DRAW3D,
        DRAW_EXTENTS,
        DRAW_CURSOR
    };

    void Draw( EDrawMode mode, const Vector3 &position, txtCursor *cursor=NULL, bool scaleDistance=true, bool dontDraw=false );

    txtFormattedStringParser mFormatParser;
};

//#############################################################################

}	// namespace rage

#endif

