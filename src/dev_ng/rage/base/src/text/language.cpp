//
// text/language.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "language.h"
#include <string.h>

using namespace rage;

// make sure these names match valud names accepted by "setlocale" on Windows (or else we need a seperate array or locale strings)
const char *txtLanguage::mNames[kLanguageCount]=
{
	"English",
	"Spanish",
	"French",
	"German",
	"Italian",
	"Portugese",
	"Japanese",
	"Chinese-traditional",
	"Chinese-simplified",
	"Korean",
	"Norwegian"
};


const char *txtLanguage::mAbbreviations[kLanguageCount]=
{
	"en","es","fr","de","it","pt","jp","cht","chs","ko","no"
};


const char *txtLanguage::Get(int language)
{
	Assert(language>=0 && language<kLanguageCount);
	return mAbbreviations[language];
}

const char *txtLanguage::GetLocale(int language)
{
	Assert(language>=0 && language<kLanguageCount);
	return mNames[language];
}


// the better way to do this would be to have language 0 defined
// as 'unknown' and return a txtLanguage::eLanguage.
int txtLanguage::Get(const char *language)
{
	for (int i=0;i<kLanguageCount;i++)
		if (!stricmp(language,mAbbreviations[i]))
			return i;

	return -1;
}

