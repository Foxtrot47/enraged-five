//
// text/glyph.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_GLYPH_H
#define TEXT_GLYPH_H

#include "vector/vector2.h"

namespace rage {

class bkBank;
class fiStream;

class txtGlyph {

public:

	txtGlyph();

	void DrawWire();
 	void AddWidgets(bkBank &bank);

	void operator=(txtGlyph&);

	bool IsValid() const;
	bool ContainsPoint(int x,int y) const;

public:

	int mUnicode;

	union {int mLeft;int left;};
	union {int mTop;int top;};
	union {int mRight;int right;};
	union {int mBottom;int bottom;};

//	int mLeft,mRight,mTop,mBottom;
};

}	// namespace rage

#endif

