//
// text/drawbuffer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_DRAWBUFFER_H
#define TEXT_DRAWBUFFER_H

#include "grcore/device.h"

namespace rage {

class txtDrawBufferNode;
class grcTexture;

// txtDrawBuffer is intende

// txtDrawBuffer //

class txtDrawBuffer
{
public:

	txtDrawBuffer();
	~txtDrawBuffer();

public:

	void Add(grcTexture *texture,s16 *destxywh,u8 *srcxywh,int count);

	void Draw(float posx=0,float posy=0,Color32 color = Color32(255,255,255,255));

	void SetBilinear(bool bilinear)			{mBilinear=bilinear;}

	txtDrawBufferNode *mHead;
	bool mBilinear;
};

}	// namespace rage

#endif

