//
// text/hashtable.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "hashtable.h"

#include "system/new.h"

using namespace rage;

// txtHashEntry //

txtHashEntry::txtHashEntry(unsigned int hash,txtStringData *data,txtHashEntry *next)
{
	mHash=hash;
	mData=data;
	mNext=next;
}

// txtHashTable //

txtHashTable::txtHashTable(int numEntries)
{
	mNumSlots=ComputePrime(numEntries);
	mSlots=rage_new txtHashEntry*[mNumSlots];
	for (int i=0;i<mNumSlots;i++)
		mSlots[i]=NULL;
	mNumEntries=0;
}

txtHashTable::~txtHashTable()
{
	int numItems=0;

	for (int i=0;i<mNumSlots;i++)
	{
		txtHashEntry *entry=mSlots[i];
		while (entry)
		{
			numItems++;
			entry=entry->mNext;
		}
	}

	if (numItems)
	{
		Warningf("txtHashTable::~txtHashTable() - %d items still remaining",numItems);
		DeleteEntries();
	}

	delete []mSlots;
}

void txtHashTable::DeleteEntries()
{
	for (int i=0;i<mNumSlots;i++)
	{
		while (mSlots[i])
		{
			txtHashEntry *next=mSlots[i]->mNext;
			delete mSlots[i];
			mSlots[i]=next;
		}
	}
}

bool txtHashTable::Insert(unsigned int hash,txtStringData *data)
{
	const int slotNum=hash%mNumSlots;
	txtHashEntry *entry=mSlots[slotNum];

	while (entry)
	{
		if (entry->mHash==hash)
			return false;
		entry=entry->mNext;
	}

	txtHashEntry *newEntry=rage_new txtHashEntry(hash,data,mSlots[slotNum]);
	newEntry->mNext=mSlots[slotNum];
	mSlots[slotNum]=newEntry;
	mNumEntries++;

	return true;
}

txtStringData *txtHashTable::Access(unsigned int hash)
{
	const int slotNum=hash%mNumSlots;
	txtHashEntry *entry=mSlots[slotNum];

	while (entry)
	{
		if (entry->mHash==hash)
			return entry->mData;
		entry=entry->mNext;
	}

	return NULL;
}

void txtHashTable::DeleteEntry(unsigned int hash)
{
	const int slotNum=hash%mNumSlots;
	txtHashEntry *entry=mSlots[slotNum];
	txtHashEntry *prev = NULL;

	while (entry)
	{
		if (entry->mHash==hash)
		{
			if (prev)
				prev->mNext = entry->mNext;
			else
				mSlots[slotNum] = entry->mNext;
			delete entry;
			return;
		}
		prev = entry;
		entry=entry->mNext;
	}
}

int txtHashTable::ComputePrime(int n)
{
	int i;
	if(n%2==0) n++;
	while(1) {
		for(i=3;i<=(n>>1);i+=2)
			if(n%i==0) {
				n+=2;
				i=n;
				break;
			}
		if(i!=n) return(n);
	}
}

bool txtHashTable::GetFirstEntry(txtHashPosition &pos)
{
	for (int i=0;i<mNumSlots;i++)
	{
		if (mSlots[i])
		{
			pos.mEntry=mSlots[i];
			pos.mSlotNum=i;
			return true;
		}
	}

	return false;
}

bool txtHashTable::GetNextEntry(txtHashPosition &pos)
{
	if (pos.mEntry && pos.mEntry->mNext)
	{
		pos.mEntry=pos.mEntry->mNext;
		return true;
	}

	for (int i=pos.mSlotNum+1;i<mNumSlots;i++)
	{
		if (mSlots[i])
		{
			pos.mEntry=mSlots[i];
			pos.mSlotNum=i;
			return true;
		}
	}

	return false;
}

