//
// text/fonttex.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_FONTTEX_H
#define TEXT_FONTTEX_H

#include "atl/array.h"

#include "string/unicode.h"

namespace rage {

class txtFontTex;
class txtCursor;
class txtDrawBuffer;
class CFontToolDlg;
class rscResourceBase;
class rscResource;
class grcImage;
class grcTexture;
class Vector3;
class HashTable;
class fiStream;
class bkBank;

#define txtFontTex_MAGIC			MAKE_MAGIC_NUMBER('F','N','T','0')


txtFontTex *txtGetFontTex(const char *fontName,bool verbose=false);

void txtFreeFontTex(txtFontTex *font);



// txtFontTexGlyph //

class txtFontTexGlyph
{
public:

	txtFontTexGlyph();

	void Load(fiStream *stream);
	void Save(fiStream *stream);

public:

	int m_Character;

	unsigned char m_X;
	unsigned char m_Y;
	unsigned char m_W;
	unsigned char m_H;

	signed char m_Baseline;
	signed char m_AddWidth;
};



// txtFontTex //

class txtFontTex {

	friend class CFontToolDlg;
	friend class rscResource;
	friend txtFontTex *txtGetFontTex(const char *fontName,bool verbose);
	friend void txtFreeFontTex(txtFontTex *font);

private:

	// Creators //

	txtFontTex();			// use ::txtGetFontTex() instead
	~txtFontTex();	// use ::txtFreeFontTex() instead

public:

	void Init(int numGlyphs=0,const char *name="default");
	bool Load(const char *fontName,bool verbose=false);
	void Kill();

	bool SaveFontInfo(const char *pathname);
	bool SaveFontTexturesQuantized(const char *basePathname,bool force4BitTextures=false);
	bool SaveFontShadowsQuantized(const char *basePathname,bool force4BitTextures=false);

	// Manipulators //

	// this is the one that does all the work
	// x and y are in screen (pixel) space, origin at top left corner
	void Draw(float x,float y,const char16 *string,float scaleX=1.f,float scaleY=1.f,txtCursor *cursor=NULL,bool dontDraw=false,txtDrawBuffer *drawBuffer=NULL);

	// these are a bit more convenient
	inline void Draw(float x,float y,const char16 *string,txtCursor *cursor,bool dontDraw=false,txtDrawBuffer *drawBuffer=NULL);
	inline void Draw(int x,int y,const char16 *string,float scaleX=1.f,float scaleY=1.f,txtCursor *cursor=NULL,bool dontDraw=false,txtDrawBuffer *drawBuffer=NULL);
	inline void Draw(int x,int y,const char16 *string,txtCursor *cursor,bool dontDraw=false,txtDrawBuffer *drawBuffer=NULL);

	// project into the current viewport
	void Draw3D(const Vector3 &position,const char16 *string,bool scaleDistance=true,float scaleX=1.f,float scaleY=1.f,txtCursor *cursor=NULL,bool dontDraw=false,txtDrawBuffer *drawBuffer=NULL,bool centered=false);

	void DrawExtents(float x,float y,const char16 *string,float scaleX=1.f,float scaleY=1.f,txtCursor *cursor=NULL);
	void DrawCursor(float x,float y,txtCursor *cursor);

	void ComputeExtents(int *x,int *y,const char16 *string,float scaleX=1.f,float scaleY=1.f,txtCursor *cursor=NULL);

	void SetBilinear(bool bilinear);
	void SetMonospaceWidth(int width)					{m_MonospaceWidth=width;}

	void SetCharSpacing(int charSpacing)				{m_CharSpacing=charSpacing;}

	// Accessors //

	const char *GetName()								{return m_Name;}
	int	GetRefCount() const								{return m_RefCount;}
	txtFontTexGlyph *GetGlyph(int glyphNum) const		{return &m_Glyphs[glyphNum];}
	const char *GetName() const							{return m_Name;}
	int GetNumGlyphs() const							{return m_NumGlyphs;}
	int GetCharSpacing() const							{return m_CharSpacing;}
	int GetMonospaceWidth() const						{return m_MonospaceWidth;}
	int GetSpaceWidth() const							{return m_SpaceWidth;}
	int GetHeight() const								{return m_CharHeight;}
	u8 GetGlyphIndex(int i) const						{return m_GlyphIndex[i];}
	u16 GetGlyphIndexMax() const						{return m_GlyphIndexMax;}
	int GetNumTextures() const							{return m_Textures.GetCount();}
	const grcTexture* GetTexture(int n) const			{return m_Textures[n];}

	// Widgets //

	void AddWidgets(bkBank &bank);

	// Statics //

	enum eMissingMode {kIgnore,kWarning,kHighlight,kHighlightAndWarning,kQuit};

	static void SetMissingModeQuit()					{sm_MissingMode=kQuit;}
	static void SetMissingModeWarning()					{sm_MissingMode=kWarning;}
	static void SetMissingModeHighlight()				{sm_MissingMode=kHighlight;}
	static void SetMissingModeHighlightAndWarning()		{sm_MissingMode=kHighlightAndWarning;}
	static void SetMissingModeIgnore()					{sm_MissingMode=kIgnore;}

	static void SetMissingCharacter(u16 character)		{sm_MissingCharacter=character;}
	static u16 GetMissingCharacter()					{return sm_MissingCharacter;}

	static void PrintFontRefs();

	static const HashTable &GetHashtable()				{return sm_HashTable;}

#if __TOOL
	// Write resources
	void WriteResource (rscResourceBase *rsc) const;
#endif

private:

	// Used by FontTool //

	void LoadTextures(const char *textureName,int numTextures);

	void SetName(const char *name);
	void SetCharHeight(int charHeight)					{m_CharHeight=charHeight;}
	void SetMaxAscent(int maxAscent)					{m_MaxAscent=maxAscent;}
	void SetMaxDescent(int maxDescent)					{m_MaxDescent=maxDescent;}
	void SetSpaceWidth(int spaceWidth)					{m_SpaceWidth=spaceWidth;}
	void SetNumTextures(int numTextures);
	void SetTexture(int textureNum,grcTexture *texture);
	void SetShadowTexture(int textureNum,grcTexture *texture);
	void SetShadowParameters(int x1,int x2,int y1,int y2);
	void SetNumImages(int numImages);
	void SetImage(int imageNum,grcImage *image);
	void SetShadowImage(int imageNum,grcImage *image);
	void SetGlyphsPerTexture(int textureNum,int numGlyphs)		{m_GlyphsPerTexture[textureNum]=numGlyphs;}
	void DecreaseNumGlyphs(int numGlyphs);
	grcImage *GetImage(int imageNum) const				{return m_Images[imageNum];}
	grcImage *GetShadowImage(int imageNum) const		{return m_ShadowImages[imageNum];}
	int GetNumImages() const							{return m_Images.GetCount();}

	// Statics //

	static void ResetMemory();					// blows away all fonts and resets all statics

	// Resources //

	txtFontTex(class datResource&);
	static void ResourcePageIn(class datResource&);
	static class datResourceRegister sm_Register;


private:

	// Texture //

	atArray<grcTexture*>	m_Textures;				// used only for displaying
	atArray<grcImage*>		m_Images;				// used only during generation - remove later?
	atArray<grcTexture*>	m_Shadows;
	atArray<grcImage*>		m_ShadowImages;			// used only during generation - remove later?

	atArray<int>	m_GlyphsPerTexture;

	// Parameters //

	int m_CharHeight;							// delta y for a linefeed
	int m_CharSpacing;							// spacing between adjacent characters (very small)
	int m_SpaceWidth;							// width of a space (not counting m_CharSpacing)
	int m_MonospaceWidth;						// width of characters in monospace mode (not counting m_CharSpacing)
	int m_MaxAscent,m_MaxDescent;					// maxheight above and below the baseline
	int m_ShadowLeft,m_ShadowRight;				// extra space left and right of each glyph for shadow
	int m_ShadowTop,m_ShadowBottom;				// extra room above and below each glyph for shadow

	int m_ShadowX1,m_ShadowX2;					// inclusive set of offsets to use when generating shadowmap
	int m_ShadowY1,m_ShadowY2;					// only used during generation; x1<=x2 and y1<=y2; can be + or -

	// Variables //

	bool m_Bilinear;

	// Glyphs //

	txtFontTexGlyph *m_Glyphs;
	int m_NumGlyphs;
	u8 m_GlyphIndex[256];						// maps unicode to glyphNum for unicodes values 0-255
	u16 m_GlyphIndexMax;							// glyphNum of highest glyph indexed by mGlyphIndex

	// Members //

	char *m_Name;
	txtFontTex *m_Next;

	mutable int m_RefCount;

	// Statics //

	static u16 sm_MissingCharacter;				// Substitute this character if not in font (usually '?')

	static txtFontTex *sm_First;

	static HashTable sm_HashTable;

	static bool sm_FreeTextures;

	static eMissingMode sm_MissingMode;

};

inline void txtFontTex::Draw(float posx,float posy,const char16 *string,txtCursor *cursor,bool dontDraw,txtDrawBuffer *drawBuffer)
{
	Draw(posx,posy,string,1.f,1.f,cursor,dontDraw,drawBuffer);
}

inline void txtFontTex::Draw(int x,int y,const char16 *string,float scaleX,float scaleY,txtCursor *cursor,bool dontDraw,txtDrawBuffer *drawBuffer)
{
	Draw(float(x),float(y),string,scaleX,scaleY,cursor,dontDraw,drawBuffer);
}

inline void txtFontTex::Draw(int posx,int posy,const char16 *string,txtCursor *cursor,bool dontDraw,txtDrawBuffer *drawBuffer)
{
	Draw(float(posx),float(posy),string,1.f,1.f,cursor,dontDraw,drawBuffer);
}

}	// namespace rage

#endif

