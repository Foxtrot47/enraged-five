//
// text/fonttex.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "fonttex.h"
#include "cursor.h"
#include "drawbuffer.h"

#include "grcore/viewport.h"
#include "grcore/device.h"

#include "grcore/im.h"
#include "grcore/image.h"

#include "bank/bank.h"
#include "file/asset.h"
#include "data/resource.h"
// #include "obsolete/hash.h" /* FIXME */
#include "file/stream.h"

#include "system/alloca.h"

extern int assetDebug;				// defined in data/asset.cpp


namespace rage {

// NOTE: look for 'future' for future file format modifications.

OUTPUT_ONLY(const float currentVersion=1.02f);

const float kShimX=0.499f;
const float kShimY=0.499f;

const char *kDefaultFont="arial12";

// HashTable txtFontTex::sm_HashTable;

txtFontTex *txtFontTex::sm_First=NULL;


txtFontTex *txtGetFontTex(const char *fontName,bool verbose)
{
	txtFontTex *font;

	char lowerCase[80];

	if (!fontName)
		strncpy(lowerCase,kDefaultFont,80);
	else
		for (int i=0;i<80;i++)
		{
			lowerCase[i]=fontName[i]>='A'&&fontName[i]<='Z' ? char(fontName[i]+32) : fontName[i];		// tolower()
			if (!fontName[i])
				break;
		}

	/* TODO font=(txtFontTex*)txtFontTex::sm_HashTable.Access(lowerCase);
	if (font)
	{
		font->m_RefCount++;
		return font;
	} */

	font=rage_new txtFontTex();					// note: returns a txtFontTex, not just a txtFont

	if (!font->Load(lowerCase,verbose))
	{
		if (!kDefaultFont)
			return NULL;

		if (!font->Load(kDefaultFont,verbose))
		{
			Errorf("Failed to load default font (%s)",kDefaultFont);
			delete font;
			return NULL;
		}
		font->SetName(lowerCase);
	}

	// TODO txtFontTex::sm_HashTable.Insert(lowerCase,font);
	return font;
}



void txtFreeFontTex(txtFontTex *font)
{
	Assert(!font || font->GetRefCount()>0);

	if (!font)
		return;

	if (font->GetRefCount()==1)
	{
		//if (font->GetName())
		//{
		//	AssertVerify(txtFontTex::sm_HashTable.Delete(font->GetName()));
		//}

		if (txtFontTex::sm_First==font)
		{
			txtFontTex::sm_First=font->m_Next;
			delete font;
			return;
		}
		else
		{
			txtFontTex *ptr=txtFontTex::sm_First;
			while (ptr)
			{
				if (ptr->m_Next==font)
				{
					ptr->m_Next=font->m_Next;
					delete font;
					return;
				}
				else
					ptr=ptr->m_Next;
			}
			Quitf("txtFreeFontTex() - font not found in font list");
		}
	}

	font->m_RefCount--;
}



// txtFontTexGlyph //

txtFontTexGlyph::txtFontTexGlyph()
{
	m_Character=0;
	m_X=m_Y=m_W=m_H=0;
	m_Baseline=m_AddWidth=0;
}



void txtFontTexGlyph::Load(fiStream *stream)
{
	stream->ReadInt(&m_Character,1);
	stream->Read(&m_X,sizeof(m_X));						// u8
	stream->Read(&m_Y,sizeof(m_Y));						// u8
	stream->Read(&m_W,sizeof(m_W));						// u8
	stream->Read(&m_H,sizeof(m_H));						// u8

	stream->Read(&m_Baseline,sizeof(m_Baseline));		// s8
	stream->Read(&m_AddWidth,sizeof(m_AddWidth));		// s8
}



void txtFontTexGlyph::Save(fiStream *stream)
{
	stream->WriteInt(&m_Character,1);
	stream->Write(&m_X,sizeof(m_X));					// u8
	stream->Write(&m_Y,sizeof(m_Y));					// u8
	stream->Write(&m_W,sizeof(m_W));					// u8
	stream->Write(&m_H,sizeof(m_H));					// u8

	stream->Write(&m_Baseline,sizeof(m_Baseline));		// s8
	stream->Write(&m_AddWidth,sizeof(m_AddWidth));		// s8
}



// txtFontTex //

bool txtFontTex::sm_FreeTextures=true;

txtFontTex::eMissingMode txtFontTex::sm_MissingMode=txtFontTex::kIgnore;

u16 txtFontTex::sm_MissingCharacter=u16('?');



txtFontTex::txtFontTex()
{
	m_Next=sm_First;
	sm_First=this;
	m_Name=NULL;

	m_RefCount=1;

	// Texture //


	// Parameters //

	m_CharHeight=32;
	m_CharSpacing=1;
	m_SpaceWidth=32;
	m_MonospaceWidth=32;
	m_MaxAscent=m_MaxDescent=0;

	m_ShadowLeft=m_ShadowRight=m_ShadowTop=m_ShadowBottom=0;
	m_ShadowX1=m_ShadowX2=m_ShadowY1=m_ShadowY2=0;

	// Variables //

	m_Bilinear=false;

	// Glyphs //

	m_Glyphs=NULL;
	m_NumGlyphs=0;

	// Glyph Index //

	memset(&m_GlyphIndex,0,sizeof(m_GlyphIndex[0]));
	m_GlyphIndexMax=0;

}



txtFontTex::~txtFontTex()
{
	Kill();

	// remove from the static font list:
	if (this==sm_First)
		sm_First=sm_First->m_Next;
	else 
	{
		txtFontTex *font=sm_First;
		while (font)
		{
			if (font->m_Next==this)
			{
				font->m_Next=font->m_Next->m_Next;
				break;
			}
			font=font->m_Next;
		}
	}

	delete []m_Name;
}



void txtFontTex::Init(int numGlyphs,const char *name)
{
	for (int i=0;i<GetNumTextures();i++)
	{
		if (sm_FreeTextures)
		{
			grcReleaseTexture(m_Textures[i]);
			grcReleaseTexture(m_Shadows[i]);
		}
	}

	for (int i=0;i<GetNumImages();i++)
	{
		if (m_Images[i])
			m_Images[i]->Release();
		if (m_ShadowImages[i])
			m_ShadowImages[i]->Release();
	}

	m_Textures.Reset();
	m_Shadows.Reset();
	m_Images.Reset();
	m_ShadowImages.Reset();
	m_GlyphsPerTexture.Reset();

	delete []m_Name;
#ifdef OPNEW
	m_Name=__StringDuplicate(name,__FILE__,__LINE__);
#else
	m_Name=StringDuplicate(name);
#endif

	if (m_Glyphs)
		delete []m_Glyphs;

	if (numGlyphs)
		m_Glyphs=rage_new txtFontTexGlyph[numGlyphs];
	else
		m_Glyphs=NULL;

	m_NumGlyphs=numGlyphs;

	memset(&m_GlyphIndex,0,sizeof(m_GlyphIndex));
	m_GlyphIndexMax=0;
}



bool txtFontTex::Load(const char *filename,bool verbose)
{
	Init();

	delete []m_Name;
#ifdef OPNEW
	m_Name=__StringDuplicate(filename,__FILE__,__LINE__);
#else
	m_Name=StringDuplicate(filename);
#endif

	char fullFilename[256];

	fiStream *stream = ASSET.Open(filename,"fonttex");

	if (!stream)
	{
		Errorf("Failed to load font %s",filename);
		return false;
	}

	if (verbose)
		Displayf("Loading font '%s'...",filename);

	float version=0.0f;
	stream->ReadFloat(&version,1);
	if (version==1.0f)
		;
	else if (version==1.01f)
		;
	else if (version==1.02f)
		;
	else
	{
		Warningf("Version number on file '%s' is %0.2f; expected %0.2f",filename,version,currentVersion);
//		stream->Close();
//		return false;
	}

	stream->ReadInt(&m_CharHeight,1);
	stream->ReadInt(&m_CharSpacing,1);
	stream->ReadInt(&m_SpaceWidth,1);

	if (version>=1.01f)
		stream->ReadInt(&m_MonospaceWidth,1);
	else
		m_MonospaceWidth=m_SpaceWidth*2;

	if (version>=1.02f)
	{
		stream->ReadInt(&m_MaxAscent,1);
		stream->ReadInt(&m_MaxDescent,1);
		stream->ReadInt(&m_ShadowLeft,1);
		stream->ReadInt(&m_ShadowRight,1);
		stream->ReadInt(&m_ShadowTop,1);
		stream->ReadInt(&m_ShadowBottom,1);
	}

	stream->ReadInt(&m_NumGlyphs,1);
	m_Glyphs=rage_new txtFontTexGlyph[m_NumGlyphs];
	int i;
	for (i=0;i<m_NumGlyphs;i++)
		m_Glyphs[i].Load(stream);

	int numTextures;
	stream->ReadInt(&numTextures,1);
	m_Textures.Resize(numTextures);
	m_Shadows.Resize(numTextures);
	m_GlyphsPerTexture.Resize(numTextures);
	for (i=0;i<numTextures;i++)
		stream->ReadInt(&m_GlyphsPerTexture[i],1);

	stream->Close();

	// Load Textures //

	for (i=0;i<GetNumTextures();i++)
	{
		sprintf(fullFilename,"%s_%02d.dds",filename,i);

#if __TOOL
		m_Textures[i]=(grcTexture*)StringDuplicate(fullFilename);
#else
		m_Textures[i]=grcCreateTexture(fullFilename);
#endif

		if (m_ShadowRight || m_ShadowLeft || m_ShadowTop || m_ShadowBottom)
		{
			sprintf(fullFilename,"%s_%02ds.dds",filename,i);
#if __TOOL
			m_Shadows[i]=(grcTexture*)StringDuplicate(fullFilename);
#else
			m_Shadows[i]=grcCreateTexture(fullFilename);
#endif
		}
		else
			m_Shadows[i]=NULL;
	}

	SetBilinear(m_Bilinear);

	int maxWidth=0;
	for (i=0;i<m_NumGlyphs;i++)
		maxWidth=Max(m_Glyphs[i].m_W+m_Glyphs[i].m_AddWidth,maxWidth);
	m_MonospaceWidth=maxWidth;

	// Glyph Index //

	m_GlyphIndexMax=0;
	for (i=0;i<m_NumGlyphs;i++)
	{
		if (m_Glyphs[i].m_Character>255)
			break;
		m_GlyphIndexMax=u16(i);
		m_GlyphIndex[m_Glyphs[i].m_Character]=u8(i);
	}

	return true;
}



void txtFontTex::Kill()
{
	for (int i=0;i<GetNumTextures();i++)
	{
		if (sm_FreeTextures)
		{
			grcReleaseTexture(m_Textures[i]);
			grcReleaseTexture(m_Shadows[i]);
		}

	}

	for (int i=0;i<GetNumImages();i++)
	{
		if (m_Images[i])
			m_Images[i]->Release();
		if (m_ShadowImages[i])
			m_ShadowImages[i]->Release();

	}

	m_Textures.Reset();
	m_Shadows.Reset();
	m_Images.Reset();
	m_ShadowImages.Reset();
	m_GlyphsPerTexture.Reset();

	delete []m_Name;
	m_Name=NULL;

	delete []m_Glyphs;
	m_Glyphs=NULL;

	m_NumGlyphs=0;
}



bool txtFontTex::SaveFontInfo(const char *pathname)
{
	fiStream *stream=fiStream::Create(pathname);

	if (!stream)
		return false;

	Displayf("Saving font '%s'",pathname);

	float version=1.02f;
	stream->WriteFloat(&version,1);

	stream->WriteInt(&m_CharHeight,1);
	stream->WriteInt(&m_CharSpacing,1);
	stream->WriteInt(&m_SpaceWidth,1);

	if (version>=1.01f)
		stream->WriteInt(&m_MonospaceWidth,1);

	if (version>=1.02f)
	{
		stream->WriteInt(&m_MaxAscent,1);
		stream->WriteInt(&m_MaxDescent,1);
		stream->WriteInt(&m_ShadowLeft,1);
		stream->WriteInt(&m_ShadowRight,1);
		stream->WriteInt(&m_ShadowTop,1);
		stream->WriteInt(&m_ShadowBottom,1);
	}

	stream->WriteInt(&m_NumGlyphs,1);
	for (int i=0;i<m_NumGlyphs;i++)
		m_Glyphs[i].Save(stream);

	int numTextures=GetNumTextures();
	stream->WriteInt(&numTextures,1);
	for (int i=0;i<numTextures;i++)
		stream->WriteInt(&m_GlyphsPerTexture[i],1);

	stream->Close();

	return true;
}



void txtFontTex::SetName(const char *name)
{
	delete []m_Name;
#ifdef OPNEW
	m_Name=__StringDuplicate(name,__FILE__,__LINE__);
#else
	m_Name=StringDuplicate(name);
#endif
}

void txtFontTex::SetNumImages(int numImages)
{
	int lastNum=m_Images.GetCount();
	m_Images.Resize(numImages);
	m_ShadowImages.Resize(numImages);
	
	// make sure we empty new slots:
	for (int i=lastNum;i<numImages;i++)
	{
		m_Images[i]=NULL;
		m_ShadowImages[i]=NULL;
	}
}

void txtFontTex::SetNumTextures(int numTextures)
{
	int lastNum=m_Textures.GetCount();

	m_Textures.Resize(numTextures);
	m_Shadows.Resize(numTextures);
	m_GlyphsPerTexture.Resize(numTextures);
	
	// make sure we empty new slots:
	for (int i=lastNum;i<numTextures;i++)
	{
		m_Textures[i]=NULL;
		m_Shadows[i]=NULL;
		m_GlyphsPerTexture[i]=0;
	}
}

void txtFontTex::SetTexture(int textureNum,grcTexture *texture)
{
	Assert(textureNum<GetNumTextures());

//	if (sm_FreeTextures)
//	{
//		if (m_Textures[textureNum])							// HACK - this crashes the whole friggin' computer
//			gfxFreeTexture(m_Textures[textureNum]);			// (only used by FontTool)
//	}

	m_Textures[textureNum]=texture;
	// texture->SetFilter(m_Bilinear? grctfBilinear : grctfPoint);
	/* if (m_Bilinear)
		m_Textures[textureNum]->SetTexEnv(m_Textures[textureNum]->GetTexEnv() | gfxTexEnvBilinear);
	else
		m_Textures[textureNum]->SetTexEnv(m_Textures[textureNum]->GetTexEnv() & ~gfxTexEnvBilinear); */
}



void txtFontTex::SetShadowTexture(int textureNum,grcTexture *texture)
{  
//	if (sm_FreeTextures)
//	{
//		if (m_ShadowTextures[textureNum])							// HACK - this crashes the whole friggin' computer
//			gfxFreeTexture(m_ShadowTextures[textureNum]);			// (only used by FontTool)
//	}

	m_Shadows[textureNum]=texture;

	// texture->SetFilter(m_Bilinear? grctfBilinear : grctfPoint);
	/* if (m_Bilinear)
		m_Shadows[textureNum]->SetTexEnv(m_Shadows[textureNum]->GetTexEnv() | gfxTexEnvBilinear);
	else
		m_Shadows[textureNum]->SetTexEnv(m_Shadows[textureNum]->GetTexEnv() & ~gfxTexEnvBilinear); */
}



void txtFontTex::SetImage(int imageNum,grcImage *image)
{
	if (m_Images[imageNum])
		m_Images[imageNum]->Release();
	m_Images[imageNum]=image;
}



void txtFontTex::SetShadowImage(int imageNum,grcImage *image)
{
	if (m_ShadowImages[imageNum])
		m_ShadowImages[imageNum]->Release();
	m_ShadowImages[imageNum]=image;
}



#if 1
void txtFontTex::Draw3D(const Vector3 &,const char16 *,bool ,float ,float ,txtCursor *,bool ,txtDrawBuffer *,bool )
{
}
#else
void txtFontTex::Draw3D(const Vector3 &position,const char16 *string,bool scaleDistance,float scaleX,float scaleY,txtCursor *cursor,bool dontDraw,txtDrawBuffer *drawBuffer,bool centered)
{
	Vector4 screen;
	Vector4 world(position.x,position.y,position.z,1);
	grcViewport &viewport = *grcViewport::GetCurrent();
	viewport.Project(screen,world);

	if ((screen.w <= 0.f) || (screen.w >= 1.f))
		return;

	if ((screen.x <= 0.f) || (screen.x >= 4095.f) || (screen.y <= 0.f) || (screen.y >= 4095.f) || (screen.z <= 0.f))
		return;

	if (scaleDistance)
	{
		float scale = screen.w;
		scaleX *= scale;
		scaleY *= scale;
	}

//	Displayf("pos=%0.2f  %0.2f  %0.2f    x=%0.2f  y=%0.2f  z=%0.2f  w=%0.2f",position.x,position.y,position.z,screen.x,screen.y,screen.z,screen.w);

	// can't use cursor to center text when drawing 3D because it draws it viewport-relative
	if(centered)
	{
		int xsize = 0, ysize = 0;
		ComputeExtents( &xsize, &ysize, string, scaleX, scaleY, cursor );
		screen.x -= ((float)xsize) / 2.0f;
		screen.y -= ((float)ysize) / 2.0f;
	}

	Draw(screen.x,screen.y,string,scaleX,scaleY,cursor,dontDraw,drawBuffer);
}
#endif



void txtFontTex::Draw(float posx,float posy,const char16 *string,float scaleX,float scaleY,txtCursor *cursor,bool dontDraw,txtDrawBuffer *drawBuffer)
{
	s16 i=0;

	if (drawBuffer)
		drawBuffer->SetBilinear(m_Bilinear);

	// Vector3 position(posx,posy,0);

	Color32 color(255,255,255);

	s16 firstCharacter=0;
	s16 lastCharacter=0;
	s16 length=s16(wcslen(string));
	bool monospace=false;
	s16 monospaceWidth=s16(m_MonospaceWidth);

	if (cursor)
	{
		firstCharacter=s16(cursor->m_SubstringStart);
		lastCharacter=s16(Min(cursor->m_SubstringEnd,length-1));
		if (int(lastCharacter)<int(firstCharacter))
			return;
		length=s16(lastCharacter-firstCharacter+1);

		monospace=cursor->m_Attributes&cursor->kMonospaced?true:false;
		if (cursor->m_MonospaceWidth)
			monospaceWidth=s16(cursor->m_MonospaceWidth);
	}

	// First pass: Make a list of glyphs and the textures those glyphs appear in.
	// Constructs textureList[] and glyphList[].

	s16 *glyphList=Alloca(s16,length);
	s8 *textureList=Alloca(s8,length);

	for (i=0;i<length;i++)
	{
		int chr=(u16)string[firstCharacter+i];

		glyphList[i]=-1;
		textureList[i]=-1;

		bool nonPrintingCharacter=false;

		// 0x00A0 is a non-breaking space
		if (chr=='\n' || chr=='\r' || chr=='\b' || chr==' ' || chr==0x00A0)
		{
			nonPrintingCharacter=true;
			continue;
		}
		else if (chr < 256 && m_GlyphIndexMax>0)
		{
			u16 g=m_GlyphIndex[chr];

			if (m_Glyphs[g].m_Character==chr)
			{
				glyphList[i]=g;

				for (s8 t=0;t<GetNumTextures();t++)
					if (g<m_GlyphsPerTexture[t])
					{
						textureList[i]=t;
						break;
					}
			}
		}
		else
		{
			u16 lowest=m_GlyphIndexMax;
			u16 highest=u16(m_NumGlyphs-1);

			if ((m_Glyphs[lowest].m_Character<=chr) &&
				(m_Glyphs[highest].m_Character>=chr) )
			{
				// pseudo-binary search for the right glyph
				while (lowest<=highest)
				{
					u16 g=u16(lowest+((highest-lowest)>>1));
					if (m_Glyphs[g].m_Character<chr)
					{
						lowest=u16(g+1);
						if (lowest>=m_NumGlyphs) // if not found, quit
							break;
						continue;
					}
					else if (m_Glyphs[g].m_Character>chr)
					{
						highest=u16(g-1);
						if (highest == 0xffff) // if not found, quit
							break;
						continue;
					}
					else
					{
						glyphList[i]=g;

						for (s8 t=0;t<GetNumTextures();t++)
							if (g<m_GlyphsPerTexture[t])
							{
								textureList[i]=t;
								break;
							}
						break;
					}
				}
			}
		}

		if (!nonPrintingCharacter && glyphList[i]==-1)
		{
#if !__NO_OUTPUT
			const char *message="Missing %s char 0x%04X (%d) in %s";
#endif

#if !__NO_OUTPUT
			const char *str=NULL;
			static char tmp[4]="'?'";
			if (chr>32 && chr<256)
				tmp[1]=(char)(chr&0xff),str=tmp;
			else
				str="???";
#endif

			switch(sm_MissingMode)
			{
			case kIgnore:
			case kHighlight:
				break;

			case kQuit:
#if !__NO_OUTPUT
				Quitf(message,str,chr&0xffff,chr&0xffff,GetName());
#endif
				break;

			case kHighlightAndWarning:
			case kWarning:
#if !__NO_OUTPUT
				Warningf(message,str,chr&0xffff,chr&0xffff,GetName());
#endif
				break;
			};

			if (sm_MissingMode==kHighlight || sm_MissingMode==kHighlightAndWarning)
			{
				AssertMsg(sm_MissingCharacter<=255 , "txtFontTex::sm_MissingCharacter must be between 0 and 255");

				// if highlighting mode, try replacing it with a '?'
				chr=sm_MissingCharacter;

				if (m_GlyphIndexMax>0)
				{
					u16 g=m_GlyphIndex[chr];

					if (m_Glyphs[g].m_Character==chr)
					{
						glyphList[i]=g;

						for (s8 t=0;t<GetNumTextures();t++)
							if (g<m_GlyphsPerTexture[t])
							{
								textureList[i]=t;
								break;
							}
					}
				}
			}
		}
	}

	// Second Pass: Calculate screen positions of all glyphs.
	// This includes left/right/center justification
	// and word wrapping.  The positions of the letters go
	// into destxy[].

	float cursorX=kShimX;
	float cursorY=kShimY;
	float x,y,w,h;
	s16 *destxy=Alloca(s16,length*2);
	float fMonospaceWidth=float(monospaceWidth);
	s16 lastSpace=-1;
	const s16 maxNewLines=128;
	s16 newLines[maxNewLines];
	float lineBottoms[maxNewLines];
	s16 numNewLines=0;

	lineBottoms[numNewLines] = cursorY;
	newLines[numNewLines++] = 0;

	memset(newLines,0,maxNewLines*2);
	memset(destxy,0,2*length*sizeof(s16));

	if (cursor)
	{
		// if wordwrapping is enabled and the cursor is outside right or left margins,
		// just put it at the left margin
		if (cursor->m_Attributes&cursor->kWrapped && (cursor->m_CursorX<cursor->m_Left || cursor->m_CursorX>cursor->m_Right))
			cursorX=float(cursor->m_Left)+kShimX;
		else
			cursorX=float(cursor->m_CursorX)+kShimX;

		cursorY=float(cursor->m_CursorY)+kShimY;

		//take the scale factor stored in the cursor into account:
		scaleX *= cursor->m_ScaleX;
		scaleY *= cursor->m_ScaleY;
	}

	for (i=0;i<length;i++)
	{
		char16 chr=string[firstCharacter+i];

		if (glyphList[i]<0)	// non-printing character (space, <cr>, or no glyph)
		{
			if (chr == '\n')
			{
				if (cursor)
					cursorX = cursor->m_Left+kShimX;
				else
					cursorX = kShimX;
				cursorY += m_CharHeight;
				lineBottoms[numNewLines] = cursorY;
				newLines[numNewLines++] = s16(i+1);
				AssertMsg(numNewLines<maxNewLines-1 , "Increase maxNewLines");
				lastSpace = -1;
			}
			else if (chr == ' ')
			{
				cursorX += monospace ? monospaceWidth : m_SpaceWidth;
				lastSpace = i;
			}
			else if (chr == '\b')
				cursorX -= (monospace ? monospaceWidth : m_SpaceWidth) / 4;
			else
				cursorX += monospace ? monospaceWidth : m_SpaceWidth; // unrecognized character
			continue;
		}

		txtFontTexGlyph *glyph = &m_Glyphs[glyphList[i]];

		if (!monospace)
		{
			x = scaleX*cursorX;
			y = scaleY*(cursorY+glyph->m_Baseline+m_MaxAscent);

			w = scaleX*glyph->m_W;
			h = scaleY*glyph->m_H;

//			Displayf("x=%0.2f  y=%0.2f  w=%0.2f  h=%0.2f",x,y,w,h);

			// deal with word wrapping if enabled
			if (cursor && (cursor->m_Attributes & cursor->kWrapped))
			{
				if (x+w >= cursor->m_Right)
				{
					// back up to last space
					if (lastSpace >= 0)
					{
						i = lastSpace;

						cursorX = float(cursor->m_Left)+kShimX;
						cursorY += m_CharHeight;
						lineBottoms[numNewLines] = cursorY;
						newLines[numNewLines++] = s16(i+1);
						AssertMsg(numNewLines<maxNewLines-1 , "Increase maxNewLines");
						lastSpace = -1;
						glyphList[i] = -1;

						continue;
					}
					else
					{
						Warningf("txtFontTex::Draw() - not enough room for word wrapping");

						cursorX = float(cursor->m_Left)+kShimX;
						cursorY += m_CharHeight;
						lineBottoms[numNewLines] = cursorY;
						newLines[numNewLines++] = s16(i);
						AssertMsg(numNewLines<maxNewLines-1 , "Increase maxNewLines");
						lastSpace = -1;

						x = scaleX*cursorX;
						y = scaleY*(cursorY+glyph->m_Baseline+m_MaxAscent);
					}
				}
			}

			// clip text to window if cursor is left justified and clipping is enabled 
			if (cursor && (cursor->m_Attributes&cursor->kClipped) && !(cursor->m_Attributes & (cursor->kCentered | cursor->kRight)))
				if (x+w>=cursor->m_Right || y+h>=cursor->m_Bottom || x<cursor->m_Left || y<cursor->m_Top)
					glyphList[i] = -1;

			destxy[i*2] = s16(16.f*x);
			destxy[i*2+1] = s16(16.f*y);

			cursorX+=glyph->m_W+glyph->m_AddWidth+m_CharSpacing;
		}
		else
		{
			// center each monospaced character so skinny characters aren't shoved to the left
			float center = (monospaceWidth - (glyph->m_W + glyph->m_AddWidth)) * 0.5f;

			// this clamp can lead to thin monospaced characters not
			// centering correctly
//			if (center<0)
//				center=0;

			x = scaleX * (center + cursorX);

			// this is another potential solution instead of the
			// clamp above; however, it could cause the first character
			// in a line to squish too close with the second character
//			if (x<0)
//				x=0;

			// with neither clamp, you run the risk of the first character
			// disappearing because it's being drawn with a negative x
			// to counteract this, we're allowing x to go slightly below
			// cursor->m_Left (see below)

			// this could be another solution, but it would
			// move all monospaced text to the right a bit
//			x = scaleX * (monospaceWidth + center + cursorX);

			y = scaleY*(cursorY+glyph->m_Baseline+m_MaxAscent);

			w = scaleX * glyph->m_W;
			h = scaleY * glyph->m_H;

//			Displayf("x=%0.2f  y=%0.2f  w=%0.2f  h=%0.2f",x,y,w,h);

			// deal with word wrapping if enabled
			if (cursor && (cursor->m_Attributes & cursor->kWrapped))
			{
				if (x+w >= cursor->m_Right)
				{
					// back up to last space
					if (lastSpace>=0)
					{
						i=lastSpace;

						cursorX=float(cursor->m_Left)+kShimX;
						cursorY+=m_CharHeight;
						lineBottoms[numNewLines] = cursorY;
						newLines[numNewLines++] = s16(i+1);
						AssertMsg(numNewLines<maxNewLines-1 , "Increase maxNewLines");
						lastSpace=-1;
						glyphList[i]=-1;

						continue;
					}
					else
					{
						Warningf("txtFontTex::Draw() - not enough room for word wrapping");

						cursorX=float(cursor->m_Left)+kShimX;
						cursorY+=m_CharHeight;
						lineBottoms[numNewLines] = cursorY;
						newLines[numNewLines++] = s16(i);
						AssertMsg(numNewLines<maxNewLines-1 , "Increase maxNewLines");
						lastSpace=-1;

						x = scaleX * (center + cursorX);
						y = scaleY*(cursorY+glyph->m_Baseline+m_MaxAscent);
					}

				}
			}

			// clip text to window if cursor is left justified and clipping is enabled 
			if (cursor && (cursor->m_Attributes&cursor->kClipped) && !(cursor->m_Attributes & (cursor->kCentered | cursor->kRight)))
				if (x+w>=cursor->m_Right || y+h>=cursor->m_Bottom || (x-scaleX*(0.f+center))<cursor->m_Left || y<cursor->m_Top)
					glyphList[i]=-1;

			destxy[i*2] = s16(16.f*x);
			destxy[i*2+1] = s16(16.f*y);

			cursorX+=fMonospaceWidth;
		}
	}

	newLines[numNewLines]=length;			// kind of a HACK

//	for (i=0;i<numNewLines;i++) Printf("%d \t",newLines[i]); Printf("\n");

	// Third Pass: Fix justification for centered and right justified text
	// This operates on destxy[]

	if (cursor && cursor->m_Attributes&(cursor->kRight|cursor->kCentered))
	{
		for (s16 line=0;line<numNewLines;line++)
		{
			s16 first=newLines[line];
			s16 last=s16(newLines[line+1]-1);
			s16 right=0;

			if (last<0)
				continue;

			for (s16 i=last;i>=first;i--)
			{
				if (glyphList[i] >= 0)
				{
					if (monospace)
						right = s16((destxy[i*2]>>4) + scaleX * monospaceWidth);
					else
						right = s16((destxy[i*2]>>4) + scaleX * (m_Glyphs[glyphList[i]].m_W + m_Glyphs[glyphList[i]].m_AddWidth));
					break;
				}
			}

			s16 offset = 0;

//			Displayf("line %d - first=%d  last=%d  right=%d  offset=%d",line,first,last,right,offset);

			if (cursor->m_Attributes&cursor->kRight)
				offset = s16((cursor->m_Right-right) << 4);
			else if (cursor->m_Attributes&cursor->kCentered)
				offset = s16((cursor->m_Right-right) << 3);

			for (s16 j=first;j<=last;j++)
				destxy[j*2] = s16(destxy[j*2]+offset);
		}
	}

	// Compute Extents

	if (cursor)
	{
		cursor->m_ExtentX = 0;
		cursor->m_ExtentY = 0;

		s16 lastActualLine = -1;

		// loop through each line, looking for rightmost edge
		for (s16 line=0;line<numNewLines;line++)
		{
			s16 first = newLines[line];
			s16 last = s16(newLines[line+1]-1);
			s16 right = 0;

			if (first>last || last<0)
				continue;

			lastActualLine=line;

			// look for last printable character in the line
			// and use its rightmost edge
			for (i=last;i>=first;i--)
			{
				if (glyphList[i] >= 0)
				{
					right = s16((destxy[i*2]>>4) + scaleX * (m_Glyphs[glyphList[i]].m_W + m_Glyphs[glyphList[i]].m_AddWidth) + 1);
					break;
				}
			}

			if (right > cursor->m_ExtentX)
				cursor->m_ExtentX = right;
		}

		cursor->m_ExtentY = int(scaleY*(lineBottoms[lastActualLine]+m_MaxAscent+m_MaxDescent+1));

//		Displayf("Extents=%d %d",cursor->m_ExtentX,cursor->m_ExtentY);
	}

	if (dontDraw)
		return;

	// Fourth Pass: Render the text, one texture map at a time to minimize state changes.
	// Constructs srcxywh[] and destxywh[].
	// For a font with three textures and dropshadows, for example, this means six texture passes.

	int numPasses=1;
	if (m_ShadowRight || m_ShadowLeft || m_ShadowTop || m_ShadowBottom)
		numPasses=2;

	for (s16 pass=0;pass<numPasses;pass++)
	{
		if(cursor)
		{
			if(numPasses>1 && pass==0 && cursor->IsShadowColorOverridden())
				color=cursor->m_ShadowColor;
			else
				color=cursor->m_Color;
		}
			

		for (s16 t=0;t<GetNumTextures();t++)
		{
			const s16 maxChars=128;
			s16 destxywh[maxChars*4];
			u8 srcxywh[maxChars*4];

			s16 totalItems = 0;
			while (totalItems<length)
			{
				s16 count=0;

				s16 currentLength=Min(maxChars,s16(length-totalItems));

				for (i=0;i<currentLength;i++)
				{
					if (glyphList[totalItems+i]<0)
						continue;

					if (textureList[totalItems+i]!=t)
						continue;

					txtFontTexGlyph *glyph=&m_Glyphs[glyphList[totalItems+i]];

					destxywh[count*4+0] = destxy[(totalItems+i)*2+0];
					destxywh[count*4+1] = destxy[(totalItems+i)*2+1];
					destxywh[count*4+2] = s16(16.f*scaleX*glyph->m_W);
					destxywh[count*4+3] = s16(16.f*scaleY*glyph->m_H);

					srcxywh[count*4] = glyph->m_X;
					srcxywh[count*4+1] = glyph->m_Y;
					srcxywh[count*4+2] = glyph->m_W;
					srcxywh[count*4+3] = glyph->m_H;

					count++;
				}

				if (count>0)
				{
					grcTexture *texture=NULL;

					if (numPasses==1 || pass==1)
						texture=m_Textures[t];
					else
						texture=m_Shadows[t];

					if (drawBuffer)
						drawBuffer->Add(texture,destxywh,srcxywh,count);
					else
					{
						grcBindTexture(texture);

#ifdef FIXME
						if (cursor && cursor->m_TexColorOp!=texopModulate) // override SetTexture default
							RSTATE.SetTexColorOp(0, cursor->m_TexColorOp);
#endif
						GRCDEVICE.BlitText(int(ceilf(posx)),int(ceilf(posy)),0,destxywh,srcxywh,count,color,m_Bilinear);
					}
				}

				totalItems = s16(totalItems+currentLength);
			}
		}
	}

	if (cursor)
	{
		if (cursor->m_Attributes & txtCursor::kLineMode)
		{
			// continue on next line after this one
			cursor->m_CursorX=0;
			cursor->m_CursorY=int(cursorY)+m_CharHeight;
		}
		else
		{
			// continue exactly where we left off:
			cursor->m_CursorX=int(cursorX);
			cursor->m_CursorY=int(cursorY);
		}
	}
}



void txtFontTex::ComputeExtents(int *xsize,int *ysize,const char16 *string,float scaleX,float scaleY,txtCursor *cursor)
{
	txtCursor tempCursor;
	if (cursor)
		tempCursor=*cursor;

	Draw(0,0,string,scaleX,scaleY,&tempCursor,true);			// true=>dontDraw

	*xsize=tempCursor.m_ExtentX;
	*ysize=tempCursor.m_ExtentY;
}



void txtFontTex::SetBilinear(bool bilinear)
{
	m_Bilinear=bilinear;

	for (int i=0;i<GetNumTextures();i++)
	{
		/* if (m_Textures[i])
			m_Textures[i]->SetFilter(bilinear? grctfBilinear : grctfPoint);
		if (m_Shadows[i])
			m_Shadows[i]->SetFilter(bilinear? grctfBilinear : grctfPoint); */
	}
}



void txtFontTex::DecreaseNumGlyphs(int numGlyphs)
{
	Assert(numGlyphs<=m_NumGlyphs);
	m_NumGlyphs=numGlyphs;
}



void txtFontTex::SetShadowParameters(int x1,int x2,int y1,int y2)
{
	Assert(x1<=x2);
	Assert(y1<=y2);

	m_ShadowX1=x1;
	m_ShadowX2=x2;
	m_ShadowY1=y1;
	m_ShadowY2=y2;

	m_ShadowLeft=Max(-x1,0);
	m_ShadowRight=Max(x2,0);
	m_ShadowTop=Max(-y1,0);
	m_ShadowBottom=Max(y2,0);
}



#if __BANK && !__TOOL
void txtFontTex::AddWidgets(bkBank &bank)
{
	bank.AddSlider("CharHeight",&m_CharHeight,-1000,1000,1);
	bank.AddSlider("CharSpacing",&m_CharSpacing,-1000,1000,1);
	bank.AddSlider("SpaceWidth",&m_SpaceWidth,-1000,1000,1);
	bank.AddSlider("MonospaceWidth",&m_MonospaceWidth,-1000,1000,1);
}
#else
void txtFontTex::AddWidgets(bkBank &) {}
#endif


#ifndef BUILD_SYSTEM_VERSION
static inline void *operator new(unsigned,void *place) { return place; }
static inline void operator delete(void *,void *) { }
#endif


#if 0
txtFontTex::txtFontTex(datResource &rsc)
{
	m_RefCount = 10000000;

//	Displayf("NumTextures=%d  m_NumGlyphs=%d",m_NumTextures,m_NumGlyphs);

	// patch all of the texture references
	int i;
	for (i=0; i<m_NumTextures; i++)
		m_Textures[i] = (gfxTexture*) rsc.Translate((int) m_Textures[i], gfxTexture_MAGIC);
	for (i=0; i<m_NumTextures; i++)
		m_Shadows[i] = (gfxTexture*) rsc.Translate((int) m_Shadows[i], gfxTexture_MAGIC);

	rsc.PointerFixup(m_Glyphs);

	rsc.PointerFixup(m_Name);
}



void txtFontTex::ResourcePageIn(datResource &rsc)
{
	::new (rsc.GetBase()) txtFontTex(rsc);
}



#if __TOOL
void txtFontTex::WriteResource(rscResourceBase *rsc) const
{
	for (int i=0;i<m_Textures.GetCount();i++)
		rsc->WriteInt(m_GlyphsPerTexture[i]);
	rsc->WriteInt(m_GlyphsPerTexture.GetCount()); // count	
	rsc->WriteInt(m_GlyphsPerTexture.GetCount()); // alloc count

	rsc->WriteInt(m_CharHeight);
	rsc->WriteInt(m_CharSpacing);
	rsc->WriteInt(m_SpaceWidth);
	rsc->WriteInt(m_MonospaceWidth);
	rsc->WriteInt(m_MaxAscent);
	rsc->WriteInt(m_MaxDescent);
	rsc->WriteInt(m_ShadowLeft);
	rsc->WriteInt(m_ShadowRight);
	rsc->WriteInt(m_ShadowTop);
	rsc->WriteInt(m_ShadowBottom);

	rsc->WriteInt(m_ShadowX1);
	rsc->WriteInt(m_ShadowX2);
	rsc->WriteInt(m_ShadowY1);
	rsc->WriteInt(m_ShadowY2);

	rsc->WriteInt(m_Bilinear);

	rsc->WriteRef(m_Glyphs);
	rsc->WriteInt(m_NumGlyphs);
	rsc->Write(m_GlyphIndex,256);
	rsc->WriteShort(m_GlyphIndexMax);
	rsc->WritePad(4);

	rsc->WriteRef(m_Name);
	rsc->WriteInt(0);				// next
	rsc->WriteInt(1000000);			// refcount

	rsc->ResolveRef(m_Name);
	rsc->Write(m_Name,strlen(m_Name)+1);
	rsc->WritePad(4);

	rsc->ResolveRef(m_Glyphs);

	for (int i=0;i<m_NumGlyphs;i++)
	{
//		Displayf("i=%d  char=%d  x=%d  y=%d  w=%d  h=%d  m_Baseline=%d  m_AddWidth=%d",
//			i,m_Glyphs[i].mCharacter,
//			m_Glyphs[i].x,m_Glyphs[i].y,m_Glyphs[i].w,m_Glyphs[i].h,
//			m_Glyphs[i].m_Baseline,m_Glyphs[i].m_AddWidth);

		rsc->WriteInt(m_Glyphs[i].m_Character);

		rsc->WriteByte(m_Glyphs[i].m_X);
		rsc->WriteByte(m_Glyphs[i].m_Y);
		rsc->WriteByte(m_Glyphs[i].m_W);
		rsc->WriteByte(m_Glyphs[i].m_H);

		rsc->WriteByte(m_Glyphs[i].m_Baseline);
		rsc->WriteByte(m_Glyphs[i].m_AddWidth);

		rsc->WritePad(4);
	}
}
#endif
#endif


void txtFontTex::ResetMemory()
{
//	Warningf("Inside txtFontTex::ResetMemory()");

	while (sm_First)
		txtFreeFontTex(sm_First);

//	sm_HashTable.Kill();
}



void txtFontTex::PrintFontRefs()
{
	txtFontTex *font=sm_First;
	while (font)
	{
		Displayf("Font '%s' refcount=%d",font->GetName(),font->GetRefCount());
		font=font->m_Next;
	}
	if (!sm_First)
		Displayf("Font list is empty");
}



// HACK - THis is just a debugging function
void txtFontTex::DrawExtents(float x,float y,const char16 *string,float scaleX,float scaleY,txtCursor *cursor)
{
	int xsize,ysize;
	ComputeExtents(&xsize,&ysize,string,scaleX,scaleY,cursor);

	grcViewport *oldViewport = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());

/** you'd think all this shit would make a difference, but it doesn't
	grcWorldIdentity();
	RSTATE.SetTexture(grcTexture::None);
	RSTATE.SetLighting(false);
	RSTATE.SetAlphaBlendEnable(false);
	RSTATE.SetZTestEnable(false);
**/

	grcBindTexture(NULL);

	grcColor3f(0,1,1);
	grcBegin(drawLineStrip,5);
	grcVertex2f(x,y);
	grcVertex2f(x+xsize,y);
	grcVertex2f(x+xsize,y+ysize);
	grcVertex2f(x,y+ysize);
	grcVertex2f(x,y);
	grcEnd();

	grcColor3f(1,1,0);
	const int size=4;

	grcBegin(drawLines,2);
	grcVertex2f(x+xsize-size,y+ysize);
	grcVertex2f(x+xsize+size,y+ysize);
	grcEnd();

	grcBegin(drawLines,2);
	grcVertex2f(x+xsize,y+ysize-size);
	grcVertex2f(x+xsize,y+ysize+size);
	grcEnd();

	grcViewport::SetCurrent(oldViewport);
}



// HACK - THis is just a debugging function
void txtFontTex::DrawCursor(float x,float y,txtCursor *cursor)
{
	float left=x+float(cursor->m_Left);
	float right=x+float(cursor->m_Right);
	float top=y+float(cursor->m_Top);
	float bottom=y+float(cursor->m_Bottom);

	grcViewport *oldViewport = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());

	grcBindTexture(NULL);

/** you'd think all this shit would make a difference, but it doesn't
	grcWorldIdentity();
	RSTATE.SetTexture(grcTexture::None);
	RSTATE.SetLighting(false);
	RSTATE.SetAlphaBlendEnable(false);
	RSTATE.SetZTestEnable(false);
**/

	grcColor3f(1,0,1);
	grcBegin(drawLineStrip,5);
	grcVertex2f(left,top);
	grcVertex2f(right,top);
	grcVertex2f(right,bottom);
	grcVertex2f(left,bottom);
	grcVertex2f(left,top);
	grcEnd();

	grcColor3f(1,1,0);
	const int size=4;

	grcBegin(drawLines,2);
	grcVertex2f(x-size,y);
	grcVertex2f(x+size,y);
	grcEnd();

	grcBegin(drawLines,2);
	grcVertex2f(x,y-size);
	grcVertex2f(x,y+size);
	grcEnd();

	grcViewport::SetCurrent(oldViewport);
}


}	// namespace rage
