//
// text/stringtex.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stringtex.h"

#include "cursor.h"
#include "stringtable.h"
#include "fonttex.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/streamxml.h"
#include "string/unicode.h"
#include "vector/vector3.h"

using namespace rage;

//#############################################################################

txtStringTex::txtStringTex()
{
	mStringData=NULL;
}

txtStringTex::~txtStringTex()
{

}

txtStringTex::txtStringTex(txtCharString string)
{
	mStringData=STRINGTABLE.Get(string);
}

void txtStringTex::SetString(txtCharString string)
{
	mStringData=STRINGTABLE.Get(string);
}

void txtStringTex::Draw(float x,float y,txtCursor *cursor,bool dontDraw)
{
	mStringData->GetFont()->Draw(x+mStringData->GetOffsetX(),y+mStringData->GetOffsetY(),mStringData->GetString(),mStringData->GetScale().x,mStringData->GetScale().y,cursor,dontDraw);
}

void txtStringTex::Draw3D(const Vector3 &position,bool scaleDistance,txtCursor *cursor,bool dontDraw)
{
	Vector3 newPosition(position.x+float(mStringData->GetOffsetX()),position.y+float(mStringData->GetOffsetY()),position.z);
	mStringData->GetFont()->Draw3D(newPosition,mStringData->GetString(),scaleDistance,mStringData->GetScale().x,mStringData->GetScale().y,cursor,dontDraw);
}

void txtStringTex::ComputeExtents(int *xsize,int *ysize,txtCursor *cursor)
{
	mStringData->GetFont()->ComputeExtents(xsize,ysize,mStringData->GetString(),mStringData->GetScale().x,mStringData->GetScale().y,cursor);
}

#if __DEV
void txtStringTex::DrawExtents(float x,float y,txtCursor *cursor)
{
	mStringData->GetFont()->DrawExtents(x+mStringData->GetOffsetX(),y+mStringData->GetOffsetY(),mStringData->GetString(),mStringData->GetScale().x,mStringData->GetScale().y,cursor);
}

void txtStringTex::DrawCursor(float x,float y,txtCursor *cursor)
{
//	mStringData->GetFont()->DrawCursor(x+mStringData->GetOffsetX(),y+mStringData->GetOffsetY(),cursor);
	mStringData->GetFont()->DrawCursor(x,y,cursor);
}
#endif

//#############################################################################

txtFormattedStringParser::GetStringTableVariableFunc txtFormattedStringParser::sm_getStringTableVariableFunc 
    = MakeFunctorRet( txtFormattedStringParser::DefaultGetStringTableVariable );

txtFormattedStringParser::txtFormattedStringParser()
{

}

txtFormattedStringParser::~txtFormattedStringParser()
{
    for ( int i = 0; i < mSpanDataLines.GetCount(); ++i )
    {
        mSpanDataLines[i].Reset();
    }

    mSpanDataLines.Reset();
}

void txtFormattedStringParser::ParseInternal( const char16* string, atMap<atString, atString> &resolvedItems )
{
    USES_CONVERSION;

    int len = int(wcslen( string ) * sizeof(char16)) + 1;

    char memFileName[RAGE_MAX_PATH];
    fiDevice::MakeMemoryFileName( memFileName, sizeof(memFileName), string, len, false, "" );

    atArray<Color32> colors;
    colors.PushAndGrow( Color32( 255, 255, 255 ) );

    int lineIndex = -1;

    fiStream* stream = ASSET.Open( memFileName, "" );
    if ( stream != NULL )
    {
        parTree *pTree = PARSER.LoadTree( stream );
        if ( pTree != NULL )
        {
            parTreeNode *pRoot = pTree->GetRoot();
            {
                parTreeNode *pChild = pRoot->GetChild();
                while ( pChild != NULL )
                {
                    if ( strcmp( pChild->GetElement().GetName(), "clrs" ) == 0 )
                    {
                        parTreeNode *pColorNode = pChild->GetChild();
                        while ( pColorNode != NULL )
                        {
                            if ( strcmp( pColorNode->GetElement().GetName(), "c" ) == 0 )
                            {
                                int r = pColorNode->GetElement().FindAttributeIntValue( "r", 255 );
                                int g = pColorNode->GetElement().FindAttributeIntValue( "g", 255 );
                                int b = pColorNode->GetElement().FindAttributeIntValue( "b", 255 );

                                colors.PushAndGrow( Color32( r, g, b ) );
                            }

                            pColorNode = pColorNode->GetSibling();
                        }
                    }
                    else if ( strcmp( pChild->GetElement().GetName(), "txt" ) == 0 )
                    {
                        parTreeNode *pChildNode = pChild->GetChild();
                        while ( pChildNode != NULL )
                        {
                            if ( strcmp( pChildNode->GetElement().GetName(), "br" ) == 0 )
                            {
                                atArray<const SpanData *> newLine;
                                mSpanDataLines.PushAndGrow( newLine );
                                ++lineIndex;
                            }
                            else if ( strcmp( pChildNode->GetElement().GetName(), "sp" ) == 0 )
                            {
                                if ( lineIndex == -1 )
                                {
                                    atArray<const SpanData *> newLine;
                                    mSpanDataLines.PushAndGrow( newLine );
                                    ++lineIndex;
                                }

                                SpanData *pSpanData = rage_new SpanData();

                                char style[50];
                                const char *pStyle = pChildNode->GetElement().FindAttributeStringValue( "st", NULL, style, 50 );
                                if ( pStyle != NULL )
                                {
                                    atString style( pStyle );

                                    atArray<atString> styleSplit;
                                    style.Split( styleSplit, ';', true );
                                    for ( int i = 0; i < styleSplit.GetCount(); ++i )
                                    {
                                        if ( styleSplit[i][0] == 'c' )
                                        {
                                            atArray<atString> colorSplit;
                                            styleSplit[i].Split( colorSplit, ':', true );
                                            if ( colorSplit.GetCount() > 1 )
                                            {
                                                int colorIndex = atoi( colorSplit[1].c_str() );
                                                if ( (colorIndex >= 0) && (colorIndex < colors.GetCount()) )
                                                {
                                                    pSpanData->format.Clear( SpanData::AUTO_COLOR );
                                                    pSpanData->color = colors[colorIndex];
                                                }
                                            }
                                        }
                                        else if ( styleSplit[i][0] == 'b' )
                                        {
                                            pSpanData->format.Set( SpanData::BOLD );
                                        }
                                        else if ( styleSplit[i][0] == 'i' )
                                        {
                                            pSpanData->format.Set( SpanData::ITALIC );
                                        }
                                        else if ( styleSplit[i][0] == 'u' )
                                        {
                                            pSpanData->format.Set( SpanData::UNDERLINE );
                                        }
                                    }
                                }

                                // resolve variables, if any                               
                                char output[1024];
                                ResolveVariables( output, sizeof(output), pChildNode->GetData(), resolvedItems );

                                // convert the char back to char16 and copy it
                                char16 *utf16String = UTF8_TO_WIDE( output );
                                pSpanData->string = WideStringDuplicate( utf16String );

                                mSpanDataLines[lineIndex].PushAndGrow( pSpanData );
                            }

                            pChildNode = pChildNode->GetSibling();
                        }
                    }

                    pChild = pChild->GetSibling();
                }
            }
        }
        else
        {
            atArray<const SpanData *> newLine;
            mSpanDataLines.PushAndGrow( newLine );

            SpanData *pSpanData = rage_new SpanData();

            // resolve variables, if any
            char output[1024];
            ResolveVariables( output, sizeof(output), WIDE_TO_UTF8( string ), resolvedItems );

            // convert the char back to char16 and copy it
            char16 *utf16String = UTF8_TO_WIDE( output );
            pSpanData->string = WideStringDuplicate( utf16String );

            mSpanDataLines[0].PushAndGrow( pSpanData );
        }

        stream->Close();
    }
}

void txtFormattedStringParser::ResolveVariables( char* output, int outputLen, const char* input, atMap<atString, atString> &resolvedItems )
{
    USES_CONVERSION;

    atString s( input );

    int startIndex = s.IndexOf( "$(" );
    while ( startIndex != -1 )
    {
        int endIndex = s.IndexOf( ')', startIndex );
        if ( endIndex != -1 )
        {
            atString var;
            var.Set( s, startIndex, endIndex - startIndex + 1 );
            if ( var.GetLength() > 3 )
            {
                atString varName;
                varName.Set( var, 2, var.GetLength() - 3 );

                atString *pVarValue = resolvedItems.Access( varName );
                if ( pVarValue == NULL )
                {
                    char16 buf[512];
                    if ( GetStringTableVariable( varName.c_str(), buf, 512, resolvedItems ) )
                    {
                        char *pTemp = WIDE_TO_UTF8( buf );
                        s.Replace( var.c_str(), pTemp );
                        startIndex += (int)strlen( pTemp ) - 1;
                    }
                    else
                    {
                        s.Replace( var, atString( "???" ) );
                        resolvedItems[var] = "???";
                    }
                }
                else
                {
                    s.Replace( var, *pVarValue );
                    startIndex += (*pVarValue).GetLength() - 1;
                }
            }
        }

        startIndex = s.IndexOf( "$(", startIndex + 1 );
    }

    safecpy( output, s.c_str(), outputLen );
}

bool txtFormattedStringParser::GetStringTableVariable( txtCharString var, char16 *val, int valLen, atMap<atString, atString> &resolvedItems )
{
    USES_CONVERSION;

    if ( !STRINGTABLE.Exists( var ) )
    {
        if ( sm_getStringTableVariableFunc( var, val, valLen ) )
        {
            atString varStr(var);
            resolvedItems[varStr] = atString(WIDE_TO_UTF8( val ));
            return true;
        }

        return false;
    }

    const txtStringData *pStringData = STRINGTABLE.Get( var );

    // Careful.  I'm assuming that we're not going to have any circular dependencies.
    txtFormattedStringParser parser;
    parser.ParseInternal( pStringData->GetString(), resolvedItems );

    // eliminate formatting and line breaks
    char16 *pValPtr = val;
    for ( int i = 0; i < parser.GetNumLines(); ++i )
    {
        for ( int j = 0; j < parser.GetNumInLine( i ); ++j )
        {
            const SpanData &spanData = parser.GetSpanData( i, j );

            int len = (int)wcslen( spanData.string );
            if ( pValPtr + (len * sizeof(char16)) >= val + (valLen * sizeof(char16)) )
            {
                len = ptrdiff_t_to_int(val + valLen - pValPtr) - 1;
                wcsncpy( pValPtr, spanData.string, len );

                pValPtr += (len * sizeof(char16)) + 1;
                *pValPtr = 0;

                atString varStr(var);
                resolvedItems[varStr] = atString(WIDE_TO_UTF8( val ));
                return true;
            }
            else
            {
                wcscpy( pValPtr, spanData.string );

                pValPtr += (len * sizeof(char16));
            }            
        }
    }

    atString varStr(var);
    resolvedItems[varStr] = atString(WIDE_TO_UTF8( val ));
    return true;
}

//#############################################################################

txtFormattedStringTex::txtFormattedStringTex()
: txtStringTex()
{

}

txtFormattedStringTex::txtFormattedStringTex( txtCharString string )
{
    SetString( string );
}

txtFormattedStringTex::~txtFormattedStringTex()
{

}

void txtFormattedStringTex::SetString( txtCharString string )
{
    txtStringTex::SetString( string );

    mFormatParser.Parse( mStringData->GetString() );
}

void txtFormattedStringTex::Draw( float x, float y, txtCursor *cursor, bool dontDraw )
{
    Vector3 position( x, y, 0.0f );
    Draw( DRAW, position, cursor, false, dontDraw );
}

void txtFormattedStringTex::Draw3D( const Vector3 &position, bool scaleDistance, txtCursor* cursor, bool dontDraw )
{
    Draw( DRAW_EXTENTS, position, cursor, scaleDistance, dontDraw );
}

void txtFormattedStringTex::ComputeExtents( int *xsize, int *ysize, txtCursor *cursor )
{
    *ysize = mFormatParser.GetNumLines() * mStringData->GetFont()->GetHeight();

    *xsize = 0;
    for ( int i = 0; i < mFormatParser.GetNumLines(); ++i )
    {
        // find the total length.
        float totalWidth = 0.0f;

        for ( int j = 0; j < mFormatParser.GetNumInLine( i ); ++j )
        {
            int width, height;
            mStringData->GetFont()->ComputeExtents( &width, &height, mFormatParser.GetSpanData( i, j ).string,
                mStringData->GetScale().x, mStringData->GetScale().y, cursor );

            totalWidth += width;
        }

        if ( (int)totalWidth > *xsize )
        {
            *xsize = (int)totalWidth;
        }
    }
}

#if __DEV
void txtFormattedStringTex::DrawExtents( float x, float y, txtCursor* cursor )
{
    Vector3 position( x, y, 0.0f );
    Draw( DRAW_EXTENTS, position, cursor );
}

void txtFormattedStringTex::DrawCursor( float x, float y, txtCursor* cursor )
{
    Vector3 position( x, y, 0.0f );
    Draw( DRAW_CURSOR, position, cursor );
}
#endif


void txtFormattedStringTex::Draw( EDrawMode mode, const Vector3 &position, txtCursor* cursor, bool scaleDistance, bool dontDraw )
{
    float height = (float)mFormatParser.GetNumLines() * (float)mStringData->GetFont()->GetHeight();
    float midX = position.x + mStringData->GetOffsetX();
    float topY = position.y - height +  mStringData->GetOffsetY();

    txtCursor tempCursor;
    txtCursor *pCursor = cursor;
    if ( pCursor == NULL )
    {
        pCursor = &tempCursor;
    }

    Color32 originalColor = tempCursor.GetColor();

    pCursor->SetLineMode( false );    

    for ( int i = 0; i < mFormatParser.GetNumLines(); ++i, topY += mStringData->GetFont()->GetHeight() )
    {
        // find the lengths so we can compute the middle and draw each part of the line in the correct place.
        atArray<int> widths;
        widths.Reserve( mFormatParser.GetNumInLine( i ) );
        widths.Resize( mFormatParser.GetNumInLine( i ) );
        float totalWidth = 0.0f;
        for ( int j = 0; j < mFormatParser.GetNumInLine( i ); ++j )
        {
            int width, height;
            mStringData->GetFont()->ComputeExtents( &width, &height, mFormatParser.GetSpanData( i, j ).string,
                mStringData->GetScale().x, mStringData->GetScale().y, cursor );

            widths[j] = width;
            totalWidth += width;
        }

        float leftX = midX - (totalWidth / 2.0f);        

        pCursor->SetCursor( (int)leftX, (int)topY );

        for ( int j = 0; j < mFormatParser.GetNumInLine( i ); ++j )
        {            
            const txtFormattedStringParser::SpanData &sData = mFormatParser.GetSpanData( i, j );
            if ( sData.format.IsSet( txtFormattedStringParser::SpanData::AUTO_COLOR ) )
            {
                pCursor->SetColor( originalColor );
            }
            else
            {
                pCursor->SetColor( sData.color );
            }

            switch ( mode )
            {
            case DRAW:
                {
                    mStringData->GetFont()->Draw( 0.0f, 0.0f, sData.string,
                        mStringData->GetScale().x, mStringData->GetScale().y, 
                        pCursor, dontDraw );
                }
                break;
            case DRAW3D:
                {
                    Vector3 spanPosition( 0.0f, 0.0f, position.z );
                    mStringData->GetFont()->Draw3D( spanPosition, sData.string, scaleDistance,
                        mStringData->GetScale().x, mStringData->GetScale().y, 
                        pCursor, dontDraw );
                }
                break;
            case DRAW_EXTENTS:
                {
                    mStringData->GetFont()->DrawExtents( 0.0f, 0.0f, sData.string,
                        mStringData->GetScale().x, mStringData->GetScale().y, 
                        pCursor );
                }
                break;
            case DRAW_CURSOR:
                {
                    mStringData->GetFont()->DrawCursor( 0.0f, 0.0f, pCursor );
                }
                break;
            }            

            leftX += widths[j];
        }    
    }

    pCursor->SetColor( originalColor );
}
