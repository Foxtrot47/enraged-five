//
// text/config.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// much of what used to be in here has moved to data/unicode.h

#ifndef TEXT_CONFIG_H
#define TEXT_CONFIG_H

#include "string/unicode.h"

namespace rage {

// Helpers //

bool hasWides(const char16 *str);

// txtCharString //

typedef const char *txtCharString;						// HACK - txtChar could eventually be more opaque
//typedef void txtCharString;							// like this?

} // namespace rage

#endif
