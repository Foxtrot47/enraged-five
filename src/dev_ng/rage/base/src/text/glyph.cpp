//
// text/glyph.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "glyph.h"
#include "grcore/im.h"
#include "bank/bank.h"

using namespace rage;

txtGlyph::txtGlyph()
{
	mUnicode=0;

	mLeft=0;
	mRight=0;
	mTop=0;
	mBottom=0;
}

void txtGlyph::operator=(txtGlyph &from)
{
	mUnicode=from.mUnicode;

	mLeft=from.mLeft;
	mRight=from.mRight;
	mTop=from.mTop;
	mBottom=from.mBottom;
}

void txtGlyph::DrawWire()
{
	if (!IsValid())
		return;

	grcBegin(drawLineStrip,5);
	grcVertex2i(mLeft,mTop);
	grcVertex2i(mRight,mTop);
	grcVertex2i(mRight,mBottom);
	grcVertex2i(mLeft,mBottom);
	grcVertex2i(mLeft,mTop);
	grcEnd();
}

bool txtGlyph::IsValid() const
{
	if (mRight!=mLeft && mTop!=mBottom)
		return true;
	else
		return false;
}

bool txtGlyph::ContainsPoint(int x,int y) const
{
	if (x>=mLeft && x<=mRight && y>=mTop && y<=mBottom)
		return true;
	else
		return false;
}

#if __BANK
void txtGlyph::AddWidgets(bkBank &bank)
{
	bank.AddSlider("Unicode",&mUnicode,0,65535,1);

/**
	bank.AddSlider("Left",&mLeft,0,txtFontEditor::mImageWidth-1,1);
	bank.AddSlider("Right",&mRight,0,txtFontEditor::mImageWidth-1,1);
	bank.AddSlider("Top",&mTop,0,txtFontEditor::mImageHeight-1,1);
	bank.AddSlider("Bottom",&mBottom,0,txtFontEditor::mImageHeight-1,1);
**/
}
#endif

