//
// text/cursor.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_CURSOR_H
#define TEXT_CURSOR_H

#include "string/unicode.h"
#include "vector/color32.h"

namespace rage {

class txtStringTex;

// txtCursor contains a bunch of extra parameters which you can pass to
// txtStringTex::Draw() and txtFontTex::Draw() to modify the way the text
// is drawn, wordwrapped, centered, spaced, etc.  It also contains an x and
// y cursor position which gets updated each time the cursor is used by one
// of the draw functions, so consecutive calls to the draw functions will
// print on consecutive lines or on the same line (depending on LineMode).
//
// Play around with testcursor to see how all the parameters work.  Note
// that the draw functions also take an x and y value, which gets *added* to
// all the positional parameters in txtCursor.  Generally when you pass a
// txtCursor to the draw functions, you'll want to put your x and y values
// in the draw function parameters *or* in the txtCursor, but not in both,
// because they'll get added together.

// txtCursor //

class txtCursor
{
public:

	txtCursor();

	void Set(int top,int bottom,int left,int right)		{m_Top=top,m_Bottom=bottom,m_Left=left,m_Right=right;}

	void Print(txtStringTex &string);				// draw string and leave cursor
	void PrintLine(txtStringTex &string);			// same as Print plus PrintCR()
	void PrintCR();									//

public:

	void SetLeftJustify()						{m_Attributes=(m_Attributes&~kCentered&~kRight)|kLeft;}
	void SetRightJustify()						{m_Attributes=(m_Attributes&~kLeft&~kCentered)|kRight;}
	void SetCentered()							{m_Attributes=(m_Attributes&~kLeft&~kRight)|kCentered;}

	void SetWrapped(bool wrapped=true)			{m_Attributes=wrapped?m_Attributes|kWrapped:m_Attributes&~kWrapped;}
	void SetLineMode(bool appendCR=true)		{m_Attributes=appendCR?m_Attributes|kLineMode:m_Attributes&~kLineMode;}
	void SetClipped(bool clipped=true)			{m_Attributes=clipped?m_Attributes|kClipped:m_Attributes&~kClipped;}
	
	void SetRepositionX(bool reposX=true)		{m_Attributes=reposX?m_Attributes|kRepositionX:m_Attributes&~kRepositionX;}
	void SetRepositionY(bool reposY=true)		{m_Attributes=reposY?m_Attributes|kRepositionY:m_Attributes&~kRepositionY;}

	void SetMonospace(bool m)					{m_Attributes=m?m_Attributes|kMonospaced:m_Attributes&~kMonospaced;}

	void SetMonospaceWidth(int w=0)				{m_MonospaceWidth=w;}

	void SetShadowColorOverride(bool b)			{m_Attributes=b?m_Attributes|kShadowColorOverride:m_Attributes&~kShadowColorOverride;}
	bool IsShadowColorOverridden() const		{ return (m_Attributes&kShadowColorOverride) != 0;  }

	void SetColor(Color32 color)				{m_Color=color;}
	const Color32 &GetColor()					{return m_Color;}
	void SetShadowColor(Color32 color)			{m_ShadowColor=color;}
	const Color32 &GetShadowColor()				{return m_ShadowColor;}

	void SetSubstring(int start,int end)		{m_SubstringStart=start; m_SubstringEnd=end;}

	void ClearExtents()							{m_ExtentX=m_ExtentY=0;}

	void ClearCursor()							{m_CursorX=m_CursorY=0;}
	void SetCursor(int x,int y)					{m_CursorX=x;m_CursorY=y;}

	void SetScale(float x,float y)				{m_ScaleX=x; m_ScaleY=y;}

//	void SetTexColorOp(gfxTextureOp);

public:			// may eventually become private?

	enum {
		kCentered=0x0001,
		kLeft=0x0002,
		kRight=0x0004,
		kWrapped=0x0008,
		kMonospaced=0x0080,
		kLineMode=0x0100,							// appends a CR after each Draw() call
		kRepositionX=0x0200,						// if m_CursorX is outside [m_Left,m_Right] then automatically reposition it
		kRepositionY=0x0400,						// if m_CursorY is outside [m_Top,m_Bottom] then automatically reposition it
		kClipped=0x0800,							// clip text if it doesn't fit in the window (on by default, only works with left justified text)
		kShadowColorOverride=0x1000					// set the shadow color to use the shadow colow override, otherwise the shadow color will default to the regular color
	};

	unsigned m_Attributes;

	int m_CursorX,m_CursorY;

	int m_Top,m_Bottom,m_Left,m_Right;

	int m_SubstringStart,m_SubstringEnd;				// useful for printing only a substring (0,0=>entire string)

	int m_MonospaceWidth;							// 0=>proportional

	int m_ExtentX,m_ExtentY;							// Draw sets these to the maximum X and Y

	float m_ScaleX,m_ScaleY;							// 1.0,1.0 => 1 texel == 1 pixel

	Color32 m_Color;

	Color32	m_ShadowColor;

//	gfxTextureOp m_TexColorOp;
};

}	// namespace rage

#endif

