//
// text/drawbuffer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "drawbuffer.h"
#include "fonttex.h"

#include "grcore/im.h"
#include "grcore/texture.h"

#include <string.h>

namespace rage {

class txtDrawBufferNode {
public:
	txtDrawBufferNode() {
		mNext=NULL;
		mTexture=NULL;
		mDestXYWH=NULL;
		mSrcXYWH=NULL;
		mCount=0;
	}

	~txtDrawBufferNode() {
		delete [] mDestXYWH;
		delete [] mSrcXYWH;
	}

public:
	txtDrawBufferNode *mNext;
	grcTexture *mTexture;
	s16 *mDestXYWH;
	u8 *mSrcXYWH;
	int mCount;
};

}	// namespace rage


using namespace rage;

// txtDrawBuffer //

txtDrawBuffer::txtDrawBuffer()
{
	mHead=NULL;
	mBilinear=false;
}

txtDrawBuffer::~txtDrawBuffer()
{
	while (mHead)
	{
		txtDrawBufferNode *next=mHead->mNext;
		delete mHead;
		mHead=next;
	}
}

void txtDrawBuffer::Add(grcTexture *texture,s16 *destxywh,u8 *srcxywh,int count)
{
	txtDrawBufferNode **current=&mHead;
	while (*current)
		current=&(*current)->mNext;

	*current=rage_new txtDrawBufferNode;
	(*current)->mTexture=texture;
	(*current)->mDestXYWH=rage_new s16[4*count];
	memcpy((*current)->mDestXYWH,destxywh,4*sizeof(u16)*count);
	(*current)->mSrcXYWH=rage_new u8[4*count];
	memcpy((*current)->mSrcXYWH,srcxywh,4*sizeof(u8)*count);
	(*current)->mCount=count;
	(*current)->mNext=NULL;
}

void txtDrawBuffer::Draw(float posx,float posy,Color32 color)
{
	txtDrawBufferNode *current=mHead;
	int ix = int(ceilf(posx));
	int iy = int(ceilf(posy));
	while (current)
	{
		grcBindTexture(current->mTexture);
		GRCDEVICE.BlitText(ix,iy,0,current->mDestXYWH,current->mSrcXYWH,current->mCount,color,mBilinear);
		current=current->mNext;
	}
}
