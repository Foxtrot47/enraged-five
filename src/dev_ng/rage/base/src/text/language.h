//
// text/language.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_LANGUAGE_H
#define TEXT_LANGUAGE_H

#include "text/config.h"

namespace rage {

class txtLanguage {

public:

	// make sure these match mAbbreviations and mLanguages
	enum eLanguage {
        kInvalid = -1,
		kEn,
		kEs,
		kFr,
		kDe,
		kIt,
		kPt,
		kJp,
		kChT,
		kChS,
		kKo,
		kNo,
		kLanguageCount								// not really a language
	};

	static const char *mAbbreviations[kLanguageCount];
	static const char *mNames[kLanguageCount];

	static int Get(const char *language);			// returns eLanguage

	static const char *Get(int language);			// language is actually an eLanguage
	static const char *GetLocale(int language);		// language is actually an eLanguage

	static int GetCount();
};

inline int txtLanguage::GetCount()
{
	return kLanguageCount;
}

}	// namespace rage

#endif

