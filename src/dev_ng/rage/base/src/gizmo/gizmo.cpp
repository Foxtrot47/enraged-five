// 
// gizmo/gizmo.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "gizmo.h"

#include "manager.h"

namespace rage {

gzGizmo::gzGizmo(Functor0 released,
				 Functor0 dragged)
	: m_Manager(NULL)
	, m_Active(true)
	, m_Size(1.0f)
	, m_Released(released)
	, m_Dragged(dragged)
{
	m_InManagerNode.Data = this;

	// By default a gizmo is given to the singleton manager.
	// This can be later changed through the SetManager function.
	m_Manager = &GIZMOMGR;

	m_Manager->Register(*this);
}

gzGizmo::~gzGizmo()
{
	if(m_Manager)
	{
		m_Manager->Unregister(*this);
	}
}

void gzGizmo::SetManager(gzManager* manager)
{
	if(m_Manager)
	{
		m_Manager->Unregister(*this);
	}
	m_Manager = manager;
	if(m_Manager)
	{
		m_Manager->Register(*this);
	}
}

void gzGizmo::MouseDrag(const Vector3& UNUSED_PARAM(from), const Vector3& UNUSED_PARAM(to))
{
	// NOTE: Call this AFTER your derived class finishes dragging.
	m_Dragged();
}

void gzGizmo::MouseReleased()
{
	m_Released();
}

} // namespace rage
