// 
// gizmo/manager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "manager.h"

#include "gizmo.h"

#include "grcore/im.h"
#include "grcore/viewport.h"
#include "input/mouse.h"
#include "input/keys.h"

namespace rage {

gzManager::gzManager()
	: m_GrabbedGizmo(NULL)
{
	m_Mapper.Reset();
	m_Mapper.Map(IOMS_MOUSE_BUTTON, ioMouse::MOUSE_LEFT,	m_Grab);
	m_Mapper.Map(IOMS_KEYBOARD, 	KEY_ALT,				m_CameraControl);
}

gzManager::~gzManager()
{
	Assert(m_Gizmos.GetHead() == NULL);
}

void gzManager::Register(gzGizmo& gizmo)
{
	m_Gizmos.Append(gizmo.m_InManagerNode);
}

void gzManager::Unregister(gzGizmo& gizmo)
{
	m_Gizmos.PopNode(gizmo.m_InManagerNode);
}

void gzManager::Draw() const
{
	for (const atDNode<gzGizmo*>* node = m_Gizmos.GetHead(); node; node = node->GetNext())
	{
		if (node->Data->IsActive())
		{
			if(m_BestGizmo == node->Data || m_GrabbedGizmo == node->Data)
				node->Data->DrawSelected();
			else
				node->Data->Draw();
		}
	}
}

void gzManager::Update()
{
	m_Mapper.Update();

	if (!GRCDEVICE.CheckThreadOwnership())
	{
		return;
	}

	if(grcViewport::GetCurrent() == NULL)
		return;

	// mouseScreen and mouseFar as where are the world space points on the near plane and far plane respectively which project to the current mouse cursor location.
	Vector3 mouseScreen, mouseFar;
	
	grcViewport::GetCurrent()->ReverseTransformNoWorld(static_cast<float>(ioMouse::GetX()),
		static_cast<float>(ioMouse::GetY()),
		(Vec3V&)mouseScreen,
		(Vec3V&)mouseFar);

	// Go through the gizmos and find one that we're intersecting
	m_BestGizmo = NULL;
	float bestT = 1.0f;

	for (const atDNode<gzGizmo*>* node = m_Gizmos.GetHead(); node; node = node->GetNext())
	{
		float t = node->Data->MouseTest(mouseScreen, mouseFar);

		if (t < bestT)
		{
			bestT = t;
			m_BestGizmo = node->Data;
		}
	}

	if (m_Grab.IsDown() && grcViewport::GetCurrent())
	{
		if (m_Grab.IsPressed() && m_CameraControl.IsUp())
		{
			if (m_BestGizmo)
			{
				m_GrabbedGizmo = m_BestGizmo;
				m_GrabbedGizmo->MousePressed(mouseScreen, mouseFar);
			}
			else
			{
				m_GrabbedGizmo = NULL;
			}
		}

		if (m_GrabbedGizmo)
		{
			m_GrabbedGizmo->MouseDrag(mouseScreen, mouseFar);
		}
	}
	else
	{
		if (m_Grab.IsReleased() && m_GrabbedGizmo)
		{
			m_GrabbedGizmo->MouseReleased();
		}

		m_GrabbedGizmo = NULL;
	}

}

} // namespace rage
