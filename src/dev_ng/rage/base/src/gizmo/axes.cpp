// 
// gizmo/axes.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "axes.h"

#include "grcore/im.h"
#include "vector/geometry.h"

namespace rage {

gzAxes::gzAxes(Matrix34& matrix, Functor0 released, Functor0 dragged)
	: gzGizmo(released, dragged)
	, m_LeftDirection(matrix.a)
	, m_UpDirection(matrix.b)
	, m_InDirection(matrix.c)
	, m_Position(matrix.d)
	, m_AxisGrabWidth(0.1f)

{
}

gzAxes::gzAxes(Vector3& position, Functor0 released, Functor0 dragged)
	: gzGizmo(released, dragged)
	, m_LeftDirection(XAXIS)
	, m_UpDirection(YAXIS)
	, m_InDirection(ZAXIS)
	, m_Position(position)
	, m_AxisGrabWidth(0.1f)

{
}

gzAxes::~gzAxes()
{
}


void gzAxes::DrawSelected() const
{
	Matrix34 drawMatrix;
	drawMatrix.Set(m_LeftDirection, m_UpDirection, m_InDirection, m_Position);

	const int ALPHA = 200;

	const float bs = m_Size*0.1f;
	grcDrawSolidBox(Vector3(bs, bs, bs), drawMatrix, Color32(255,255,255,ALPHA)) ;

	// Draw the local X axis red.
	grcColor(Color32(255, 100, 100, ALPHA));
	Vector3 endPoint;
	endPoint.AddScaled(m_Position, m_LeftDirection, m_Size);
	drawMatrix.MakeTranslate(endPoint);
	grcWorldMtx(drawMatrix);

	grcWorldMtx(drawMatrix);
	grcBegin(drawLines, 2);
	grcVertex3f(ORIGIN);
	grcVertex3f(-m_Size, 0.0f, 0.0f);
	grcEnd();
	DrawAxisDecoration();

	// Draw the local Y axis green.
	grcColor(Color32(100, 255, 100, ALPHA));
	endPoint.AddScaled(m_Position, m_UpDirection, m_Size);
	drawMatrix.MakeTranslate(endPoint);
	drawMatrix.RotateLocalZ(PI * 0.5f);
	grcWorldMtx(drawMatrix);
	grcBegin(drawLines, 2);
	grcVertex3f(ORIGIN);
	grcVertex3f(-m_Size, 0.0f, 0.0f);
	grcEnd();
	DrawAxisDecoration();

	// Draw the local Z axis blue.
	grcColor(Color32(100, 100, 255, ALPHA));
	endPoint.AddScaled(m_Position, m_InDirection, m_Size);
	drawMatrix.MakeTranslate(endPoint);
	drawMatrix.a = m_LeftDirection;
	drawMatrix.b = m_UpDirection;
	drawMatrix.c = m_InDirection;
	drawMatrix.RotateLocalY(PI * -0.5f);
	grcWorldMtx(drawMatrix);
	grcBegin(drawLines, 2);
	grcVertex3f(ORIGIN);
	grcVertex3f(-m_Size, 0.0f, 0.0f);
	grcEnd();

	DrawAxisDecoration();
}

void gzAxes::Draw() const
{
	Matrix34 drawMatrix;
	drawMatrix.Set(m_LeftDirection, m_UpDirection, m_InDirection, m_Position);

	const int ALPHA = 200;

	const float bs = m_Size*0.1f;
	grcDrawSolidBox(Vector3(bs, bs, bs), drawMatrix, Color32(255,255,255,ALPHA)) ;

	// Draw the local X axis red.
	grcColor(Color32(255, 0, 0, ALPHA));
	Vector3 endPoint;
	endPoint.AddScaled(m_Position, m_LeftDirection, m_Size);
	drawMatrix.MakeTranslate(endPoint);
	grcWorldMtx(drawMatrix);

	grcWorldMtx(drawMatrix);
	grcBegin(drawLines, 2);
	grcVertex3f(ORIGIN);
	grcVertex3f(-m_Size, 0.0f, 0.0f);
	grcEnd();
	DrawAxisDecoration();

	// Draw the local Y axis green.
	grcColor(Color32(0, 255, 0, ALPHA));
	endPoint.AddScaled(m_Position, m_UpDirection, m_Size);
	drawMatrix.MakeTranslate(endPoint);
	drawMatrix.RotateLocalZ(PI * 0.5f);
	grcWorldMtx(drawMatrix);
	grcBegin(drawLines, 2);
	grcVertex3f(ORIGIN);
	grcVertex3f(-m_Size, 0.0f, 0.0f);
	grcEnd();
	DrawAxisDecoration();

	// Draw the local Z axis blue.
	grcColor(Color32(0, 0, 255, ALPHA));
	endPoint.AddScaled(m_Position, m_InDirection, m_Size);
	drawMatrix.MakeTranslate(endPoint);
	drawMatrix.a = m_LeftDirection;
	drawMatrix.b = m_UpDirection;
	drawMatrix.c = m_InDirection;
	drawMatrix.RotateLocalY(PI * -0.5f);
	grcWorldMtx(drawMatrix);
	grcBegin(drawLines, 2);
	grcVertex3f(ORIGIN);
	grcVertex3f(-m_Size, 0.0f, 0.0f);
	grcEnd();

	DrawAxisDecoration();
}


float gzAxes::MouseTest(const Vector3& from, const Vector3& to)
{
	float tX, tY, tZ;

	ComputeIntersection(from, to, tX, tY, tZ);

	return Min(tX, tY, tZ);
}

void gzAxes::MousePressed(const Vector3& from, const Vector3& to)
{
	float tX, tY, tZ;
	Vector3 dir;
	dir.Subtract(to, from);

	ComputeIntersection(from, to, tX, tY, tZ);
	if (tY < 1.0f && tX < 1.0f && tZ < 1.0f)
	{
		m_GrabbedAxisDirection = ORIGIN;
		m_GrabbedPoint.AddScaled(from, dir, tX);
	}
	else if (tX <= tY && tX <= tZ)
	{
		m_GrabbedAxisDirection.Scale(m_LeftDirection, m_Size);
		m_GrabbedPoint.AddScaled(from, dir, tX);
	}
	else if (tY <= tX && tY <= tZ)
	{
		m_GrabbedAxisDirection.Scale(m_UpDirection, m_Size);
		m_GrabbedPoint.AddScaled(from, dir, tY);
	}
	else
	{
		m_GrabbedAxisDirection.Scale(m_InDirection, m_Size);
		m_GrabbedPoint.AddScaled(from, dir, tZ);
	}

	m_PositionWhenGrabbed = m_Position;
}


bool InBox(const Vector3& pos, const Vector3& boxMin, const Vector3& boxMax)
{
	return(pos.x<=boxMax.x && pos.x>=boxMin.x &&
	       pos.y<=boxMax.y && pos.y>=boxMin.y &&
	       pos.z<=boxMax.z && pos.z>=boxMin.z);
}


void gzAxes::ComputeIntersection(const Vector3& from, const Vector3& to, float& outTX, float& outTY, float& outTZ)
{
	Matrix34 matrix;
	matrix.Set(m_LeftDirection, m_UpDirection, m_InDirection, m_Position);

	Vector3 localFrom, localTo;
	matrix.UnTransform(from, localFrom);
	matrix.UnTransform(to, localTo);

	Vector3 localDir;
	localDir.Subtract(localTo, localFrom);

	Vector3 unusedNormal;
	int unusedIndex;

	outTX = 1.0f;
	const Vector3 xAxisMin(-m_AxisGrabWidth, -m_AxisGrabWidth, -m_AxisGrabWidth);
	const Vector3 xAxisMax(m_AxisGrabWidth + m_Size, m_AxisGrabWidth, m_AxisGrabWidth);
	geomBoxes::TestSegmentToBox(localFrom, localDir, xAxisMin, xAxisMax, &outTX, &unusedNormal, &unusedIndex, NULL, NULL, NULL);
	if(InBox(localFrom,xAxisMin,xAxisMax))
	{
		outTX = 0.0f;
	}

	outTY = 1.0f;
	const Vector3 yAxisMin(-m_AxisGrabWidth, -m_AxisGrabWidth, -m_AxisGrabWidth);
	const Vector3 yAxisMax(m_AxisGrabWidth, m_AxisGrabWidth + m_Size, m_AxisGrabWidth);
	geomBoxes::TestSegmentToBox(localFrom, localDir, yAxisMin, yAxisMax, &outTY, &unusedNormal, &unusedIndex, NULL, NULL, NULL);
	if(InBox(localFrom,yAxisMin,yAxisMax))
	{
		outTY = 0.0f;
	}

	outTZ = 1.0f;
	const Vector3 zAxisMin(-m_AxisGrabWidth, -m_AxisGrabWidth, -m_AxisGrabWidth);
	const Vector3 zAxisMax(m_AxisGrabWidth, m_AxisGrabWidth, m_AxisGrabWidth + m_Size);
	geomBoxes::TestSegmentToBox(localFrom, localDir, zAxisMin, zAxisMax, &outTZ, &unusedNormal, &unusedIndex, NULL, NULL, NULL);
	if(InBox(localFrom,zAxisMin,zAxisMax))
	{
		outTZ = 0.0f;
	}
}

const gzAxes& gzAxes::operator=(const gzAxes& that)
{
	AssertMsg(false, "I cannot think of a good reason to assign a gizmo at this time.");
	m_Size = that.m_Size;

	return *this;
}

} // namespace rage
