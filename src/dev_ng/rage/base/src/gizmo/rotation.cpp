// 
// gizmo/rotation.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rotation.h"

#include "grcore/im.h"
#include "math/amath.h"
#include "vector/geometry.h"

namespace rage {

gzRotation::gzRotation(Matrix34& matrix, Functor0 released, Functor0 dragged)
	: gzGizmo(released, dragged)
	, m_Matrix(matrix)
{
}

gzRotation::~gzRotation()
{
}

void gzRotation::Draw() const
{
	const int STEPS = 23;
	grcWorldIdentity();

	// Draw the local X rotation axis red.
	grcColor(Color32(255, 0, 0));
	grcDrawCircle(m_Size, m_Matrix.d, m_Matrix.b, m_Matrix.c, STEPS);

	// Draw the local Y rotation axis green.
	grcColor(Color32(0, 255, 0));
	grcDrawCircle(m_Size, m_Matrix.d, m_Matrix.a, m_Matrix.c, STEPS);

	// Draw the local Z rotation axis blue.
	grcColor(Color32(0, 0, 255));
	grcDrawCircle(m_Size, m_Matrix.d, m_Matrix.a, m_Matrix.b, STEPS);
}

void gzRotation::DrawSelected() const
{
	const int STEPS = 23;
	grcWorldIdentity();

	// Draw the local X rotation axis red.
	grcColor(Color32(255, 100, 100));
	grcDrawCircle(m_Size, m_Matrix.d, m_Matrix.b, m_Matrix.c, STEPS);

	// Draw the local Y rotation axis green.
	grcColor(Color32(100, 255, 100));
	grcDrawCircle(m_Size, m_Matrix.d, m_Matrix.a, m_Matrix.c, STEPS);

	// Draw the local Z rotation axis blue.
	grcColor(Color32(1000, 100, 255));
	grcDrawCircle(m_Size, m_Matrix.d, m_Matrix.a, m_Matrix.b, STEPS);
}

float gzRotation::MouseTest(const Vector3& from, const Vector3& to)
{
	float tX, tY, tZ;

	ComputeCoordinates(from, to, tX, tY, tZ);

	return Min(tX, tY, tZ);
}

void gzRotation::MousePressed(const Vector3& from, const Vector3& to)
{
	float tX, tY, tZ;

	ComputeCoordinates(from, to, tX, tY, tZ);

	if (tX < 1.0f || tY < 1.0f || tZ < 1.0f)
	{
		Vector3 dir;
		dir.Subtract(to, from);

		if (tX < tY && tX < tZ)
		{
			m_PlaneNormal = m_Matrix.a;
		}
		else if (tY < tX && tY < tZ)
		{
			m_PlaneNormal = m_Matrix.b;
		}
		else
		{
			m_PlaneNormal = m_Matrix.c;
		}

		ComputeIntersection(from, to, m_GrabPoint);
		m_MatrixWhenGrabbed = m_Matrix;
	}
}

void gzRotation::MouseDrag(const Vector3& from, const Vector3& to)
{
	ComputeIntersection(from, to, m_DragPoint);

	Matrix34 matrixChange(M34_IDENTITY);
	matrixChange.MakeTranslate(0.0f, 0.0f, 0.0f);

	Vector3 localGrab;
	localGrab.Subtract(m_GrabPoint, m_Matrix.d);
	localGrab.Normalize();

	Vector3 localDrag;
	localDrag.Subtract(m_DragPoint, m_Matrix.d);
	localDrag.Normalize();

	matrixChange.MakeRotateTo(localGrab, localDrag);
	m_Matrix.Dot3x3(m_MatrixWhenGrabbed, matrixChange);
	m_Matrix.Normalize();

	gzGizmo::MouseDrag(from, to);
}

void gzRotation::ComputeCoordinates(const Vector3& from, const Vector3& to, float& outTX, float& outTY, float& outTZ)
{
	Vector3 localFrom, localTo;
	m_Matrix.UnTransform(from, localFrom);
	m_Matrix.UnTransform(to, localTo);

	Vector3 localDir;
	localDir.Subtract(localTo, localFrom);

	const float RING_WIDTH = 0.2f;

	if (geomSegments::SegmentToDiskIntersection(localFrom, localDir, square(m_Size + RING_WIDTH), &outTY) == false)
	{
		outTY = 1.0f;
	}
	else
	{
		// Hit the disk, but did we hit the outer ring?
		Vector3 grabbedPoint;
		grabbedPoint.AddScaled(localFrom, localDir, outTY);

		if (grabbedPoint.Mag() < m_Size - RING_WIDTH)
		{
			outTY = 1.0f;
		}
	}

	SwapEm(localDir.x, localDir.y);
	SwapEm(localFrom.x, localFrom.y);

	if (geomSegments::SegmentToDiskIntersection(localFrom, localDir, square(m_Size + RING_WIDTH), &outTX) == false)
	{
		outTX = 1.0f;
	}
	else
	{
		// Hit the disk, but did we hit the outer ring?
		Vector3 grabbedPoint;
		grabbedPoint.AddScaled(localFrom, localDir, outTX);

		if (grabbedPoint.Mag() < m_Size - RING_WIDTH)
		{
			outTX = 1.0f;
		}
	}

	SwapEm(localDir.x, localDir.y);
	SwapEm(localFrom.x, localFrom.y);
	SwapEm(localDir.y, localDir.z);
	SwapEm(localFrom.y, localFrom.z);

	if (geomSegments::SegmentToDiskIntersection(localFrom, localDir, square(m_Size + RING_WIDTH), &outTZ) == false)
	{
		outTZ = 1.0f;
	}
	else
	{
		// Hit the disk, but did we hit the outer ring?
		Vector3 grabbedPoint;
		grabbedPoint.AddScaled(localFrom, localDir, outTZ);

		if (grabbedPoint.Mag() < m_Size - RING_WIDTH)
		{
			outTZ = 1.0f;
		}
	}
}

void gzRotation::ComputeIntersection(const Vector3& from, const Vector3& to, Vector3& outPos)
{
	Vector3 dir;
	dir.Subtract(to, from);

	float totalDot = m_PlaneNormal.Dot(dir);

	Vector3 diff;
	diff.Subtract(from, m_Matrix.d);
	float diffDot = m_PlaneNormal.Dot(diff);

	outPos.AddScaled(from, dir, -diffDot / totalDot);
	outPos.Subtract(m_Matrix.d);
	outPos.Normalize();
	outPos.Add(m_Matrix.d);
}

const gzRotation& gzRotation::operator=(const gzRotation& that)
{
	AssertMsg(false, "I cannot think of a good reason to assign a gizmo at this time.");
	m_Size = that.m_Size;

	return *this;
}

} // namespace rage
