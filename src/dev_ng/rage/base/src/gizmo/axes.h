// 
// gizmo/axes.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GIZMO_AXES_H
#define GIZMO_AXES_H

#include "gizmo.h"
#include "vector/matrix34.h"

namespace rage {

// PURPOSE:
//    gzAxes is an abstract base class that provides common functionality between
//    gzTranslation and the (not yet implemented) gzScale.
//
// NOTES:
//    You can create new kinds of axis-based gizmos by overriding DrawAxisDecoration
//    and MouseDrag.
//
//<FLAG Component>
class gzAxes : public gzGizmo
{
public:
	// PURPOSE: Construct a gzAxes
	// NOTES:
	//    Automatically registers the gizmo with the default gizmo manager. You can later 
	//    change the manager that owns the gizmo with SetManager.
	//
	//    Gizmos are activated when created. If you don't want them to draw and accept mouse 
	//    clicks right away, Deactivate them.
	//
	// PARAMS:
	//    position - The matrix that will be modified when the user interacts with this gizmo
	//    released - A functor that is called once when this gizmo has been clicked on  
	//               and then the mouse button released
	//    dragged - A functor that is called continuously while this gizmo is being 
	//              dragged with the mouse
	gzAxes(Matrix34& matrix,
		   Functor0 released = Functor0::NullFunctor(),
		   Functor0 dragged  = Functor0::NullFunctor());

	// PURPOSE: Construct a gzAxes
	// NOTES:
	//    Automatically registers the gizmo with the default gizmo manager. You can later 
	//    change the manager that owns the gizmo with SetManager.
	//
	//    Gizmos are activated when created. If you don't want them to draw and accept mouse 
	//    clicks right away, Deactivate them.
	//
	// PARAMS:
	//    position - The vector that will be modified when the user interacts with this gizmo
	//    released - A functor that is called once when this gizmo has been clicked on  
	//               and then the mouse button released
	//    dragged - A functor that is called continuously while this gizmo is being 
	//              dragged with the mouse
	gzAxes(Vector3& position,
		   Functor0 released = Functor0::NullFunctor(),
		   Functor0 dragged  = Functor0::NullFunctor());

	// PURPOSE:  Destroy a gzAxes
	virtual ~gzAxes();

	// PURPOSE:  Control how wide the target area is that the user has to click on to 
	//           start interacting with this gizmo.
	//
	// PARAMS: width - The width of the target area, defaulting to 0.1f
	void SetAxisGrabWidth(float width);

protected:
	// PURPOSE:  Hook for adding something that will distinguish visually among derived gzAxes.
	//
	// NOTES: Called from gzAxes draw once for each axis, with the correct matrix set up to
	//        draw a decoration on that axis.
	virtual void DrawAxisDecoration() const = 0;

	virtual void Draw() const;
	virtual void DrawSelected() const;

	virtual float MouseTest(const Vector3& from, const Vector3& to);
	virtual void MousePressed(const Vector3& from, const Vector3& to);

	void ComputeIntersection(const Vector3& from, const Vector3& to, float& outTX, float& outTY, float& outTZ);

	const gzAxes& operator=(const gzAxes& that);

	const Vector3& m_LeftDirection;
	const Vector3& m_UpDirection;
	const Vector3& m_InDirection;
	Vector3& m_Position;

	float m_AxisGrabWidth;

	Vector3 m_GrabbedAxisDirection;
	Vector3 m_GrabbedPoint;
	Vector3 m_PositionWhenGrabbed;
};


//=============================================================================
// Implementations

inline void gzAxes::SetAxisGrabWidth(float width)
{
	m_AxisGrabWidth = width;
}


} // namespace rage

#endif // GIZMO_AXES_H
