// 
// gizmo/manager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GIZMO_MANAGER_H
#define GIZMO_MANAGER_H

#include "atl/dlist.h"
#include "atl/singleton.h"
#include "input/mapper.h"

namespace rage {

class gzGizmo;

// PURPOSE: The gzManager handles the updating and drawing of all of its gzGizmo's.
// NOTES:
//    The user should use INIT_GIZMOS at the beginning of the program to create the
//    gizmo manager. Any gizmos created will automatically register themselves with the
//    gizmo manager.
//
//    Then the user should call GIZMOMGR.Update during the update loop of the program,
//    and GIZMOMGR.Draw during the draw loop. Finally, the user should call SHUTDOWN_GIZMOS
//    when the program terminates.
//
//<FLAG Component>
class gzManager
{
public:
	// PURPOSE: Construct a gzManager
	// NOTES: Usually called through the singleton interface INIT_GIZMOS
	gzManager();

	// PURPOSE: Destroy a gzManager
	// NOTES: Usually called through the singleton interface SHUTDOWN_GIZMOS
	~gzManager();

	// PURPOSE: Register a gizmo with this manager
	// NOTES: Called automatically when a gizmo is create or changes managers.
	void Register(gzGizmo& gizmo);

	// PURPOSE: Unregister a gizmo with this manager
	// NOTES: Called automatically when a gizmo is destroyed or changes managers.
	void Unregister(gzGizmo& gizmo);

	// PURPOSE: Draw all the active gizmos controlled by this manager
	// NOTES: Should be called once per frame during the draw phase of the program.
	void Draw() const;

	// PURPOSE: Update all the active gizmos controlled by this manager
	// NOTES: Polls the mouse and dispaches mouse events to all the gizmos
	void Update();

	bool IsGrabbed( gzGizmo* giz ) const { return m_GrabbedGizmo == giz; }
	void ResetGrabbed() { m_GrabbedGizmo = 0; }
protected:
	atDList<gzGizmo*> m_Gizmos; // The list of gizmos this manager controls

	gzGizmo* m_GrabbedGizmo;    // The gizmo that the user is currently interacting with
	gzGizmo* m_BestGizmo;		// The gizmo that the user will select.

	ioMapper m_Mapper;			// The mapper for polling input devices
	ioValue m_Grab;				// Input control for the grab button (usually the left mouse button)
	ioValue m_CameraControl;	// When down, disables gizmo interaction, because the user is trying to move the camera
};


//=============================================================================
// Singleton

typedef atSingleton<gzManager> gzManagerSingleton;

#define GIZMOMGR ::rage::gzManagerSingleton::InstanceRef()
#define INIT_GIZMOS ::rage::gzManagerSingleton::Instantiate()
#define SHUTDOWN_GIZMOS ::rage::gzManagerSingleton::Destroy()


} // namespace rage

#endif // GIZMO_MANAGER_H
