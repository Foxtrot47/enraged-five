// 
// gizmo/translation.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GIZMO_TRANSLATION_H
#define GIZMO_TRANSLATION_H

#include "axes.h"

namespace rage {

// PURPOSE:
//    gzTranslation provides translational control over a vector or matrix.
//
// NOTES:
//    gzTranslation draws three arrows, one for each axis of rotation. Clicking on
//    one of those arrows initiates a tranlation interaction. By dragging the mouse,
//    the user can translate the gizmo, and the matrix it controls, in the direction
//    of the arrow that was clicked.
//
//    This gizmo was inspired by the translation manipulator in Maya.
//
//<FLAG Component>
class gzTranslation : public gzAxes
{
public:
	// PURPOSE: Construct a gzTranslation
	// NOTES:
	//    Automatically registers the gizmo with the default gizmo manager. You can later 
	//    change the manager that owns the gizmo with SetManager.
	//
	//    Gizmos are activated when created. If you don't want them to draw and accept mouse 
	//    clicks right away, Deactivate them.
	//
	// PARAMS:
	//    matrix - The matrix that will be modified when the user interacts with this gizmo
	//    released - A functor that is called once when this gizmo has been clicked on  
	//               and then the mouse button released
	//    dragged - A functor that is called continuously while this gizmo is being 
	//              dragged with the mouse
	gzTranslation(Matrix34& matrix,
				  Functor0 released = Functor0::NullFunctor(),
				  Functor0 dragged  = Functor0::NullFunctor());

	// PURPOSE: Construct a gzTranslation
	// NOTES:
	//    Automatically registers the gizmo with the default gizmo manager. You can later 
	//    change the manager that owns the gizmo with SetManager.
	//
	//    Gizmos are activated when created. If you don't want them to draw and accept mouse 
	//    clicks right away, Deactivate them.
	//
	// PARAMS:
	//    position - The vector that will be modified when the user interacts with this gizmo
	//    released - A functor that is called once when this gizmo has been clicked on  
	//               and then the mouse button released
	//    dragged - A functor that is called continuously while this gizmo is being 
	//              dragged with the mouse
	gzTranslation(Vector3& position,
				  Functor0 released = Functor0::NullFunctor(),
				  Functor0 dragged  = Functor0::NullFunctor());

	// PURPOSE:  Destroy a gzTranslation
	virtual ~gzTranslation();

protected:
	virtual void DrawAxisDecoration() const;
	
	virtual void MouseDrag(const Vector3& from, const Vector3& to);
};

} // namespace rage

#endif // GIZMO_TRANSLATION_H
