// 
// gizmo/translation.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "translation.h"

#include "grcore/im.h"
#include "vector/geometry.h"
#include "grcore/viewport.h"
namespace rage {

gzTranslation::gzTranslation(Matrix34& matrix, Functor0 released, Functor0 dragged)
	: gzAxes(matrix, released, dragged)
{
}

gzTranslation::gzTranslation(Vector3& position, Functor0 released, Functor0 dragged)
	: gzAxes(position, released, dragged)
{
}

gzTranslation::~gzTranslation()
{
}

void gzTranslation::DrawAxisDecoration() const
{
	const float decorationSize = m_Size * 0.2f;
	const float decorationSize2 = decorationSize*0.6f;

	grcBegin(drawTris,8*3*2);
	for (int i = 0; i < 8; i++)
	{
		float angle = (float)i * 2.0f * PI / 8.0f;
		float angle2 = ((float)i+1.0f) * 2.0f * PI / 8.0f;
		grcVertex3f(ORIGIN);
		grcVertex3f(-decorationSize,decorationSize2 * sinf(angle2),  decorationSize2 * cosf(angle2));
		grcVertex3f(-decorationSize,decorationSize2 * sinf(angle),  decorationSize2 * cosf(angle));
		

		grcVertex3f(-decorationSize,0.0f,0.0f);
		grcVertex3f(-decorationSize,decorationSize2 * sinf(angle),  decorationSize2 * cosf(angle));
		grcVertex3f(-decorationSize,decorationSize2 * sinf(angle2),  decorationSize2 * cosf(angle2));
	}
	grcEnd();
}

void gzTranslation::MouseDrag(const Vector3& from, const Vector3& to)
{
	Vector3 dir;
	dir.Subtract(to, from);

	float mouseT, axisT;

	if ( m_GrabbedAxisDirection.IsEqual( ORIGIN) )
	{
		Vector3 axisAcross = VEC3V_TO_VECTOR3(grcViewport::GetCurrent()->GetCameraMtx().a());
		Vector3 axisUp = VEC3V_TO_VECTOR3(grcViewport::GetCurrent()->GetCameraMtx().b());
		geomTValues::FindTValuesLineToLine(m_GrabbedPoint, axisUp, from, dir, &axisT, &mouseT);
		m_Position.AddScaled(m_PositionWhenGrabbed, axisUp, axisT);

		geomTValues::FindTValuesLineToLine(m_GrabbedPoint, axisAcross, from, dir, &axisT, &mouseT);
		m_Position.AddScaled(m_Position, axisAcross, axisT);
	}
	else
	{
		geomTValues::FindTValuesLineToLine(m_GrabbedPoint, m_GrabbedAxisDirection, from, dir, &axisT, &mouseT);
		m_Position.AddScaled(m_PositionWhenGrabbed, m_GrabbedAxisDirection, axisT);
	}
	gzGizmo::MouseDrag(from, to);
}

} // namespace rage
