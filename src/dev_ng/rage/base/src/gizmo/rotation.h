// 
// gizmo/rotation.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GIZMO_ROTATION_H
#define GIZMO_ROTATION_H

#include "gizmo.h"
#include "vector/matrix34.h"

namespace rage {

// PURPOSE:
//    gzRotation provides rotational control over a matrix.
//
// NOTES:
//    gzRotation draws three orthogonal rings, one for each axis of rotation. 
//    Clicking on one of those rings initiates a rotation interaction. By dragging
//    the mouse, the user can rotate the gizmo, and the matrix it controls, in the
//    axis that was clicked.
//
//    This gizmo was inspired by the rotational manipulator in Maya.
//
//<FLAG Component>
class gzRotation : public gzGizmo
{
public:
	// PURPOSE: Construct a gzRotation
	// NOTES:
	//    Automatically registers the gizmo with the default gizmo manager. You can later 
	//    change the manager that owns the gizmo with SetManager.
	//
	//    Gizmos are activated when created. If you don't want them to draw and accept mouse 
	//    clicks right away, Deactivate them.
	//
	// PARAMS:
	//    matrix - The matrix that will be modified when the user interacts with this gizmo
	//    released - A functor that is called once when this gizmo has been clicked on  
	//               and then the mouse button released
	//    dragged - A functor that is called continuously while this gizmo is being 
	//              dragged with the mouse
	gzRotation(Matrix34& matrix,
			   Functor0 released = Functor0::NullFunctor(),
			   Functor0 dragged  = Functor0::NullFunctor());

	// PURPOSE:  Destroy a gzRotation
	virtual ~gzRotation();

protected:
	virtual void Draw() const;
	virtual void DrawSelected() const;

	virtual float MouseTest(const Vector3& from, const Vector3& to);
	virtual void MousePressed(const Vector3& from, const Vector3& to);
	virtual void MouseDrag(const Vector3& from, const Vector3& to);

	void ComputeCoordinates(const Vector3& from, const Vector3& to, float& outTX, float& outTY, float& outTZ);
	void ComputeIntersection(const Vector3& from, const Vector3& to, Vector3& outPos);

	const gzRotation& operator=(const gzRotation& that);

	Matrix34& m_Matrix;
	Matrix34 m_MatrixWhenGrabbed;
	Vector3 m_GrabPoint;
	Vector3 m_PlaneNormal;

	Vector3 m_DragPoint;
};

} // namespace rage

#endif // GIZMO_ROTATION_H
