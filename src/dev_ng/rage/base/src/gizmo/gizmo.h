// 
// gizmo/gizmo.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GIZMO_GIZMO_H
#define GIZMO_GIZMO_H

#include "atl/dlist.h"
#include "atl/functor.h"

namespace rage {

class Vector3;
class gzManager;

// PURPOSE:
//    gzGizmo is the abstract base class of all gizmos. Users of gizmo don't need to know
//    very much about it, because they will always work with actual concrete gizmos. Its
//    goal is to provide the necessary shims for the gizmo manager to dispatch mouse control
//    and drawing messages to the managed gizmos.
//
//<FLAG Component>
class gzGizmo
{
public:
	friend class gzManager;

	// PURPOSE:  Construct a gzGizmo
	//
	// NOTES:
	//    Automatically registers the gizmo with the default gizmo manager. You can later 
	//    change the manager that owns the gizmo with SetManager.
	//
	//    Gizmos are activated when created. If you don't want them to draw and accept mouse 
	//    clicks right away, Deactivate them.
	//
	// PARAMS:
	//    released - A functor that is called once when this gizmo has been clicked on  
	//               and then the mouse button released
	//    dragged - A functor that is called continuously while this gizmo is being 
	//              dragged with the mouse
	//
	// SEE ALSO:  SetManager, Deactivate
	gzGizmo(Functor0 released = Functor0::NullFunctor(),
			Functor0 dragged  = Functor0::NullFunctor());

	// PURPOSE:  Destroy a gzGizmo
	virtual ~gzGizmo();

	// PURPOSE:  Change the manager that owns this gizmo
	// PARAMS:  manager - The new owning manager for this gizmo
	void SetManager(gzManager * manager);

	// PURPOSE:  Reactivate a gizmo that has been deactivated, causing it to draw
	//           and accept mouse clicks.
	// NOTES:  Gizmos are activated when first created
	// SEE ALSO: Deactivate, IsActive
	void Activate();

	// PURPOSE:  Deactivate a gizmo, causing it to stop drawing and accepting mouse clicks.
	// NOTES:  Gizmos are activated when first created
	// SEE ALSO: Activate, IsActive
	void Deactivate();

	// PURPOSE:  Return a pointer to the "active" bool of  a gizmo, for hooking into a widget
	bool& GetActiveFlag();

	// PURPOSE:  Find out whether a gizmo is activated
	// RETURNS:  True if the gizmo is activated, false otherwise
	// NOTES:  Gizmos are activated when first created
	// SEE ALSO: Activate, Deactivate
	bool IsActive() const;

	// PURPOSE:  Change the size of a gizmo
	// NOTES:  Only derived gzGizmo's actually do anything with their size.
	void SetSize(float size);

protected:
	// PURPOSE:  Draw a gizmo
	// NOTES:  Usually only called in from gzManager::Draw
	virtual void Draw() const = 0;
	virtual void DrawSelected() const = 0;
	
	// PURPOSE:
	//    Determine whether a mouse click will hit a gizmo, and the point along 
	//    that ray where the click will hit it.
	//
	// NOTES:
	//    The gizmo will return 1.0f if it was not hit at all.
	//
	// PARAMS:
	//    from - The starting point of the mouse click ray, usually where
	//           the mouse intersects the near clipping plane
	//    to - The ending point of the mouse click ray, usually where
	//         the mouse intersects the far clipping plane
	//
	// RETURNS:
	//    The t-value where the mouse ray hit the gizmo, 0.0f means it hit right 
	//    on the "from" point, 0.999999f means almost right on the "to" point.
	//    A return value of 1.0f means that this gizmo was not hit at all.
	//
	// SEE ALSO: MousePressed, MouseDrag, MouseReleased
	virtual float MouseTest(const Vector3& from, const Vector3& to) = 0;

	// PURPOSE:
	//    Inform the gizmo that it has been clicked on.
	//
	// NOTES:
	//    When the user clicks the mouse, this function is called on the 
	//    closest gizmo to the screen at the point of the user's click. 
	//    Proximity to the screen is determined by the gizmo that returned
	//    the lowest value from MouseTest. This indicates that the user
	//    has begun interacting with this gizmo.
	//
	// PARAMS:
	//    from - The starting point of the mouse click ray, usually where
	//           the mouse intersects the near clipping plane
	//    to - The ending point of the mouse click ray, usually where
	//         the mouse intersects the far clipping plane
	//
	// SEE ALSO: MouseTest, MouseDrag, MouseRelease
	virtual void MousePressed(const Vector3& from, const Vector3& to) = 0;

	// PURPOSE:
	//    Inform the gizmo that the user has dragged the mouse with the button
	//    held down.
	//
	// NOTES:
	//    Called once per frame while the user drags the gizmo. A series of MouseDrag
	//    calls are always preceeded by a call to MousePressed, and terminated
	//    with a call to MouseReleased.
	//
	// PARAMS:
	//    from - The starting point of the mouse click ray, usually where
	//           the mouse intersects the near clipping plane
	//    to - The ending point of the mouse click ray, usually where
	//         the mouse intersects the far clipping plane
	//
	// SEE ALSO: MouseTest, MousePressed, MouseRelease
	virtual void MouseDrag(const Vector3& from, const Vector3& to);

	// PURPOSE:
	//    Inform the gizmo that the user has was dragging this gizmo, but just
	//    released the mouse button.
	//
	// SEE ALSO: MouseTest, MousePressed, MouseRelease
	virtual void MouseReleased();

	gzManager* m_Manager;				// The manager that owns this gizmo
	atDNode<gzGizmo*> m_InManagerNode;  // The linked list node linking the gizmo to the manager

	bool m_Active;						// Whether the gizmo is active
	float m_Size;						// The size of the gizmo

	Functor0 m_Released;				// Functor that is called when the mouse has been dragging, and is released
	Functor0 m_Dragged;					// Functor that is called every frame while the mouse is dragging the gizmo
};

//=============================================================================
// Implementations

inline void gzGizmo::Activate()
{
	m_Active = true;
}

inline void gzGizmo::Deactivate()
{
	m_Active = false;
}

inline bool gzGizmo::IsActive() const
{
	return m_Active;
}

inline bool& gzGizmo::GetActiveFlag()
{
	return m_Active;
}

inline void gzGizmo::SetSize(float size)
{
	m_Size = size;
}


} // namespace rage

#endif // GIZMO_GIZMO_H
