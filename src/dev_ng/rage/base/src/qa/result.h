// 
// qa/result.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef QA_RESULT_H
#define QA_RESULT_H

#include "qa.h"

#if __QA

#include <string>
#include <vector>

namespace rage {

class qaResultManager;

class qaResult
{
public:
	qaResult();

	void Reset();

	void SetName(const char* name, const char* params);
	void SetExtraResult(float result);
	void SetInitTime(float t);
	void SetShutdownTime(float t);

	void TestTime(float t);

	enum Condition
	{
		INCOMPLETE,
		PASS,
		FAIL
	};

	Condition GetCondition();

	enum ReportType
	{
		PASS_OR_FAIL			= 1<<0,
		PASS_OR_EXTRA			= 1<<1,
		FAIL_OR_EXTRA			= 1<<2,
		PASS_OR_TOTAL_TIME		= 1<<3,
		FAIL_OR_TOTAL_TIME		= 1<<4,
		PASS_OR_AVERAGE_TIME	= 1<<5,
		FAIL_OR_AVERAGE_TIME	= 1<<6,
		PASS_OR_NUM_UPDATES		= 1<<7,
		FAIL_OR_NUM_UPDATES		= 1<<8
	};

	void SetReportType(int reportType);
	const char* GetReportTypeName(ReportType reportType) const;

	void Fail();
	void Pass();

	void Display( bool ShowErrors = false, bool OutputXml = false );
	void Report(qaResultManager& manager);

private:
	static const int MAX_TEST_NAME_LENGTH = 256;
	char m_TestName[MAX_TEST_NAME_LENGTH];
	char m_ParamsName[MAX_TEST_NAME_LENGTH];

	float m_ExtraResult;
	float m_InitTime;
	float m_ShutdownTime;

	float m_TotalTime;
	std::vector<float> m_TestTimes;

	Condition m_Condition;

	ReportType m_ReportType;
};

} // namespace rage

#endif // __QA

#endif // QA_RESULT_H
