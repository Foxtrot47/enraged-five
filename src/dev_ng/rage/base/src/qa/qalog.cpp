// 
// qa/log.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa/qa.h"

#if __QA

#include "log.h"

#include "string/string.h"

#include <stdio.h>
#include <stdarg.h>
#include <time.h>

namespace rage
{

static FILE* s_LogFP;

const char* qaLog::DEFAULT_LOG_FILE_NAME = "qalog.txt";

bool
qaLog::Init( const char* filename, const unsigned flags )
{
    if( !s_LogFP )
    {
        if( filename )
        {
            s_LogFP = ::fopen( filename, ( flags & INIT_TRUNCATE_FILE ) ? "w" : "a" );
        }
        else
        {
            s_LogFP = ::fopen( DEFAULT_LOG_FILE_NAME, ( flags & INIT_TRUNCATE_FILE ) ? "w" : "a" );
        }

        if( s_LogFP )
        {
			// If this craps out on you on PS3, you need to upgrade to 082.
            static const int STAMP_LEN = 256;
            char timestamp[ STAMP_LEN ];
            time_t curtime;

            time( &curtime );

            const struct tm *today = localtime( &curtime );

            ::fprintf( s_LogFP, "\n-------------------------------------------------------------------------------\n" );

            ::strftime( timestamp, STAMP_LEN - 1, "%A, %B %d, %Y %H:%M:%S\n", today );
            ::fprintf( s_LogFP, timestamp );

            ::fprintf( s_LogFP, "-------------------------------------------------------------------------------\n" );

            ::fflush( s_LogFP );
        }
    }

    return ( 0 != s_LogFP );
}

void
qaLog::Shutdown()
{
    if( s_LogFP )
    {
        ::fclose( s_LogFP );

        s_LogFP = 0;
    }
}

void
qaLog::Log( const char* fmt, ... )
{
    if( !s_LogFP )
    {
        qaLog::Init( DEFAULT_LOG_FILE_NAME, 0 );
    }

    char buf[ 256 ];

    va_list args;
    va_start( args, fmt );
    vformatf( buf, sizeof( buf ) - 1, fmt, args );
    va_end( args );

    if( s_LogFP )
    {
        ::fprintf( s_LogFP, "%s\n", buf );
    }

    Displayf( "%s", buf );

    if( s_LogFP )
    {
        ::fflush( s_LogFP );
    }
}

}   //namespace rage

#endif  //__QA
