// 
// qa/resultmanager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa.h"

#if __QA

#include "resultmanager.h"

#include "file/asset.h"
#include "file/token.h"
#include "file/stream.h"

#include <algorithm>

namespace rage {

qaResultManager::qaResultManager()
{
}

void qaResultManager::SetRunName(const char* name)
{
	m_CurrentRunName = name;

	m_CurrentRunIndex = 0;
	for (RunNamesType::iterator rit = m_RunNames.begin(); rit != m_RunNames.end(); ++m_CurrentRunIndex, ++rit)
	{
		if (strcmp(rit->c_str(), name) == 0)
		{
			break;
		}
	}
}

static void WriteCellXML(fiStream* stream, int& currentColumn, int newColumn, const char* data)
{
    Assert(newColumn > currentColumn);

    if (data[0] != '\0')
    {
        char line[1024];

        if (newColumn > currentColumn + 1)
        {
            sprintf(line, "    <Cell ss:Index=\"%d\"><Data ss:Type=\"String\">%s</Data></Cell>\n", newColumn, data);
        }
        else
        {
            sprintf(line, "    <Cell><Data ss:Type=\"String\">%s</Data></Cell>\n", data);
        }

        currentColumn = newColumn;

        stream->Write(line, StringLength(line));
    }
}

void qaResultManager::SaveXML(const char* fileName) const
{
    if (fiStream* historyFile = ASSET.Create(fileName, "xml"))
    {
        char line[1024];

        // Write XML header
        strcpy(line, "<?xml version=\"1.0\"?>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "<?mso-application progid=\"Excel.Sheet\"?>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, " xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, " xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, " xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, " <Worksheet ss:Name=\"qaresults\">\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "  <Table ss:ExpandedColumnCount=\"256\" ss:ExpandedRowCount=\"554\" x:FullColumns=\"1\"\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "   x:FullRows=\"1\">\n"); historyFile->Write(line, StringLength(line));

        // Write out the run names in the top row
        strcpy(line, "   <Row>\n"); historyFile->Write(line, StringLength(line));
        int currentColumn = 1;
        int newColumn = 4;
        for (RunNamesType::const_iterator nit = m_RunNames.begin(); nit != m_RunNames.end(); ++nit)
        {
            WriteCellXML(historyFile, currentColumn, newColumn++, nit->c_str());
        }

        if (m_CurrentRunIndex == m_RunNames.size())
        {
            WriteCellXML(historyFile, currentColumn, newColumn++, m_CurrentRunName.c_str());
        }

        strcpy(line, "   </Row>\n"); historyFile->Write(line, StringLength(line));

        for (ItemsType::const_iterator iit = m_Items.begin(); iit != m_Items.end(); ++iit)
        {
            strcpy(line, "   <Row>\n"); historyFile->Write(line, StringLength(line));

            currentColumn = 0;
            newColumn = 1;

            WriteCellXML(historyFile, currentColumn, newColumn++, iit->first.c_str());

            const Item& item = iit->second;
            char buffer1[BUFFER_LENGTH];
            ConvertDoubleQuotesToSingle(buffer1, item.inputs.c_str());

            WriteCellXML(historyFile, currentColumn, newColumn++, buffer1);
            WriteCellXML(historyFile, currentColumn, newColumn++, item.resultType.c_str());

            for (std::vector<std::string>::const_iterator rit = item.oldRuns.begin(); rit != item.oldRuns.end(); ++rit)
            {
                WriteCellXML(historyFile, currentColumn, newColumn++, rit->c_str());
            }

            if (m_CurrentRunIndex == m_RunNames.size())
            {
                WriteCellXML(historyFile, currentColumn, newColumn++, item.newRun.c_str());
            }

            strcpy(line, "   </Row>\n"); historyFile->Write(line, StringLength(line));
        }

        // Write XML footer
        strcpy(line, "  </Table>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "   <Selected/>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "   <ProtectObjects>False</ProtectObjects>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "   <ProtectScenarios>False</ProtectScenarios>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "  </WorksheetOptions>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, " </Worksheet>\n"); historyFile->Write(line, StringLength(line));
        strcpy(line, "</Workbook>\n"); historyFile->Write(line, StringLength(line));

        historyFile->Close();
    }
}

void qaResultManager::Load(const char* fileName)
{
#define RESULT_ASSERT(X) \
	if (!(X)) \
	{ \
		historyFile->Close(); \
		Quitf("Results file %s can't be read!", fileName); \
	}

	if (fiStream* historyFile = ASSET.Open(fileName, "csv"))
	{
		fiAsciiTokenizer token;
		token.Init(fileName, historyFile);

		char buffer[128 * 1024];

		// Read run names
		token.GetLine(buffer, sizeof(buffer));
		RESULT_ASSERT(buffer[0] == ',' && buffer[1] == ',' && buffer[2] == ',');
		
		char* cursor = &buffer[3];
		char* comma = strchr(cursor, ',');

		while (comma)
		{
			*comma = '\0';
			m_RunNames.push_back(std::string(cursor));

			cursor = comma + 1;
			comma = strchr(cursor, ',');
		}

		if (*cursor)
		{
			m_RunNames.push_back(std::string(cursor));
		}

		while (token.GetLine(buffer, sizeof(buffer)) != -1)
		{
			// Get name
			cursor = buffer;
			comma = strchr(cursor, ',');

			RESULT_ASSERT(comma != NULL);

			*comma = '\0';
			std::string name(cursor);

			// Get inputs
			cursor = comma + 1;
			char* quote = strchr(cursor, '"');

			RESULT_ASSERT(quote != NULL);
			cursor = quote + 1;
			quote = strchr(cursor, '"');
			RESULT_ASSERT(quote != NULL);
			*quote = '\0';

			Item item;
			item.inputs = cursor;
			cursor = quote + 1;
			comma = strchr(cursor, ',');

			RESULT_ASSERT(comma != NULL);
			cursor = comma + 1;
			comma = strchr(cursor, ',');

			RESULT_ASSERT(comma != NULL);

			*comma = '\0';
			item.resultType = cursor;
			cursor = comma + 1;
			comma = strchr(cursor, ',');

			bool anythingThere = false;

			while (comma)
			{
				*comma = '\0';
				item.oldRuns.push_back(std::string(cursor));
				
				if (*cursor)
				{
					anythingThere = true;
				}

				cursor = comma + 1;
				comma = strchr(cursor, ',');
			}

			item.oldRuns.push_back(std::string(cursor));

			if (*cursor)
			{
				anythingThere = true;
			}

			RESULT_ASSERT(m_RunNames.size() == item.oldRuns.size());

			if (anythingThere)
			{
				m_Items.insert(ItemsType::value_type(name, item));
			}
		}

		historyFile->Close();
	}

	// Call SetRunName, in case someone set it before Load
	SetRunName(m_CurrentRunName.c_str());
}

void qaResultManager::Save(const char* fileName) const
{
	if (fiStream* historyFile = ASSET.Create(fileName, "csv"))
	{
		historyFile->Write(",,", 2);

		for (RunNamesType::const_iterator nit = m_RunNames.begin(); nit != m_RunNames.end(); ++nit)
		{
			historyFile->Write(",", 1);
			historyFile->Write(nit->c_str(), (int)nit->length());
		}

		if (m_CurrentRunIndex == m_RunNames.size())
		{
			historyFile->Write(",", 1);
			historyFile->Write(m_CurrentRunName.c_str(), (int)m_CurrentRunName.length());
		}

		historyFile->Write("\n", 1);

		for (ItemsType::const_iterator iit = m_Items.begin(); iit != m_Items.end(); ++iit)
		{
			historyFile->Write(iit->first.c_str(), (int)iit->first.length());
			historyFile->Write(",", 1);

			const Item& item = iit->second;
			char buffer1[BUFFER_LENGTH];
			ConvertDoubleQuotesToSingle(buffer1, item.inputs.c_str());

			// And surround the inputs with double quotes
			char buffer2[BUFFER_LENGTH];
			sprintf(buffer2, "\"%s\"", buffer1);

			historyFile->Write(buffer2, (int)strlen(buffer2));
			historyFile->Write(",", 1);

			historyFile->Write(item.resultType.c_str(), (int)item.resultType.length());

			for (std::vector<std::string>::const_iterator rit = item.oldRuns.begin(); rit != item.oldRuns.end(); ++rit)
			{
				historyFile->Write(",", 1);
				historyFile->Write(rit->c_str(), (int)rit->length());
			}

			if (m_CurrentRunIndex == m_RunNames.size())
			{
				historyFile->Write(",", 1);
				historyFile->Write(item.newRun.c_str(), (int)item.newRun.length());
			}

			historyFile->Write("\n", 1);
		}

		historyFile->Close();
	}
}

std::string& qaResultManager::GetResult(const char* itemName, const char* itemInputs, const char* resultType)
{
	std::pair<ItemsType::iterator, ItemsType::iterator> matches = m_Items.equal_range(itemName);

	char buffer[BUFFER_LENGTH];
	ConvertDoubleQuotesToSingle(buffer, itemInputs);

	for (; matches.first != matches.second; ++matches.first)
	{
		Item& item = matches.first->second;

		if (item.inputs.compare(buffer) == 0 &&
			item.resultType.compare(resultType) == 0)
		{
			if (m_CurrentRunIndex == m_RunNames.size())
			{
				return item.newRun;
			}
			else
			{
				return item.oldRuns[m_CurrentRunIndex];
			}
		}
	}

	// No matching items, make a new one
	Item newItem;
	newItem.inputs = buffer;
	newItem.resultType = resultType;
	newItem.oldRuns.resize(m_RunNames.size());

	ItemsType::iterator newItemIt = m_Items.insert(ItemsType::value_type(itemName, newItem));

	if (m_CurrentRunIndex == m_RunNames.size())
	{
		return newItemIt->second.newRun;
	}
	else
	{
		return newItemIt->second.oldRuns[m_CurrentRunIndex];
	}
}

void qaResultManager::ConvertDoubleQuotesToSingle(char* out, const char* in) const
{
	// Convert any double quotes to single quotes
	strncpy(out, in, BUFFER_LENGTH - 2);
	out[BUFFER_LENGTH - 3] = '\0';
	while ((out = strchr(out, '"')) != NULL)
	{
		*out = '\'';
	}
}

} // namespace rage

#endif // __QA
