// 
// qa/resultmanager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef QA_RESULTMANAGER_H
#define QA_RESULTMANAGER_H

#include <map>
#include <string>
#include <vector>

namespace rage {

class qaItem;

class qaResultManager
{
public:
	qaResultManager();

	void SetRunName(const char* name);

	void SaveXML(const char* fileName) const;
    void Load(const char* fileName);
    void Save(const char* fileName) const;

	std::string& GetResult(const char* itemName, const char* itemInputs, const char* resultType);

private:
	static const int BUFFER_LENGTH = 1024;
	void ConvertDoubleQuotesToSingle(char* out, const char* in) const;
	typedef std::vector<std::string> RunNamesType;
	RunNamesType m_RunNames;

	std::string m_CurrentRunName;
	unsigned int m_CurrentRunIndex;

	struct Item
	{
		std::string				 inputs;
		std::string				 resultType;
		std::vector<std::string> oldRuns;
		std::string				 newRun;
	};

	typedef std::multimap<std::string, Item> ItemsType;
	ItemsType m_Items;
};

} // namespace rage

#endif // QA_RESULTMANAGER_H
