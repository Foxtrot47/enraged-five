// 
// qa/qa.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef QA_QA_H
#define QA_QA_H

#define __QA (!__FINAL && !__SPU && !HACK_GTA4)

#if __QA

#include "item.h"
#include "result.h"
#include "log.h"

#define QA_ITEM_FAMILY(NAME, PARAMS, PARAMNAMES) \
qaItem* Create##NAME PARAMS \
{ \
	NAME* item = rage_new NAME; \
	item->Init PARAMNAMES; \
	return item; \
}

#define QA_ITEM(NAME, PARAMS, REPORTTYPE)
#define QA_ITEM_DRAW(NAME, PARAMS, REPORTTYPE)
#define QA_ITEM_FAST(NAME, PARAMS, REPORTTYPE)

#define TST_PASS \
result.Pass()

#define TST_FAIL \
result.Fail()

#define TST_EXTRA_RESULT(RESULT) \
result.SetExtraResult(RESULT)

//PURPOSE
//  Use QA_CHECK to validate an expression and, if it fails, fail the unit
//  test.  If QA_ASSERT_ON_FAIL is defined and is non-zero, failures will
//  trigger an assert.  Typically QA_ASSERT_ON_FAIL is used during unit test
//  development and then turned off before committing to source control.
#if defined( QA_ASSERT_ON_FAIL ) && QA_ASSERT_ON_FAIL
#define QA_CHECK( expr ) do{ if( !AssertVerify( expr ) ) { QALog( "%s : failed", #expr ); TST_FAIL; return; } }while(0);
# define QA_FAIL { FastAssert( false ); TST_FAIL; return; }
#else
#define QA_CHECK( expr ) do{ if( !( expr ) ) { QALog( "%s : failed", #expr ); TST_FAIL; return; } }while(0);
# define QA_FAIL { TST_FAIL; return; }
#endif

#endif // __QA

#endif // QA_QA_H
