//
// qa/BugstarInterface.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef QA_BUGSTARINTERFACE_H
#define QA_BUGSTARINTERFACE_H

#include "file/handle.h"
#include "file/limits.h"
#include "file/stream.h"

#include <stddef.h>

namespace rage {

#define ENABLE_BUGSTAR (!__FINAL)
#if ENABLE_BUGSTAR

#define MAX_ARG_SIZE 			2048
#define BUGS_CONFIG_LINES		4

typedef enum
{
	MSG_BUG,
	MSG_USER = 1000
} BugstarMessage;


typedef enum
{
	BUG_ASSIGNED_TO,
	BUG_ID, 
	BUG_NUMBER, 
	BUG_REPORTER, 
	BUG_ATTEMPTS, 
	BUG_VERIFICATIONS, 
	BUG_SEVERITY, 
	BUG_STATUS, 
	BUG_CREATION_TS, 
	BUG_SHORT_DESC, 
	BUG_PRODUCT, 
	BUG_REP_PLATFORM, 
	BUG_VERSION, 
	BUG_FIXED_IN_VERSION, 
	BUG_COMPONENT, 
	BUG_RESOLUTION, 
	BUG_DESCRIPTION, 
	BUG_CUSTOMFIELDS 
} BugFields;


class qaBug
{
public:
	qaBug(): m_buff(NULL), m_buff_size(0) {};
	~qaBug()
	{
		ClearBugBuffer();
	};

	qaBug& operator=(const qaBug&);

	void SetField(int id,void* data,size_t size);
	void AppendData(void* data,size_t size);

	void ClearBugBuffer();

	void* GetMessage() const;
	size_t GetMessageSize() const;

	void SetID(int val);
	void SetOwner(const char* val);	
	void SetSeverity(const char* val);
	void SetSummary(const char* val,...);
	void SetProduct(const char* val);
	void SetPlatform(unsigned int val);
	void SetVersion(unsigned int val);
	void SetComponent(unsigned int val);
	void SetDescription(const char* val,...);
	void SetComments(const char* val,...);
	void SetPos(float x,float y,float z);
	void SetGrid(const char* val);
	void SetLogFile(const char *fileName);

	void BeginField(const char* name,const char* dtype);
	void EndField();

	void BeginCustomFieldData();
	void EndCustomFieldData();
	void AddCustomFieldData(void* data,size_t size);
	void AddCustomField(const char *field,const char *fmt,...);

protected:
	void* m_buff;
	size_t m_buff_size;
};


class qaBugstarInterface
{
public:
	qaBugstarInterface();
	virtual ~qaBugstarInterface(void);
	int SendMsg(unsigned int msg_id, void* data, size_t data_size, void *data2, size_t data_size2);
	void CreateBug(qaBug& bug, const char* filename, int id = -1, const char* filenameDebug = NULL, bool isPostRender = false);
	void OpenBugForEdit(qaBug& bug);

	//
	// PURPOSE
	//	Sets the name of the product database to enter bugs into
	// PARAMS
	//	productName - a pointer to the product name (pointer contents are NOT copied!!!)
	//
	static void SetProduct(const char* productName);

#if RSG_PC && !RSG_TOOL
	static void HandleDelayedBugCreate();
#endif // RSG_PC && !RSG_TOOL

protected:
	virtual void Init();
	virtual void Term();
	virtual void ReadConfigData(void);
	virtual void ParseParameterLine(char* param);
	
	void SendData(void* data, size_t size);
	void RecvData(void** data, size_t *size);

	// TCP/IP Bugstar integration protocol
	// Header / Body Message / Footer
	void SendHeader(size_t bodyMessageLength);
	void SendFooter();
	void SendCustomField_Binary(const char* name, fiStream* stream);
	void SendCustomField_Text  (const char* name, const char* text);
	//------
	u32  GetCustomFieldLength       (const char* name, const char* dtype, u32 size);
	u32  GetCustomFieldLength_Binary(const char* name, fiStream* stream);
	u32  GetCustomFieldLength_Text  (const char* name, const char* text);


	unsigned int m_port;
	fiHandle m_socket;
	char m_method[10];
	char m_user_name[64];
	char m_target_name[64];
	char m_target_ip[64];

	static const char* sm_ProductName;

#if RSG_PC && !RSG_TOOL
	struct DelayedBugCreateInfo
	{
		DelayedBugCreateInfo() : isPendingBugCreate(false) {}

		bool isPendingBugCreate;
		qaBug bug;
		int id;
		char filename[RAGE_MAX_PATH];
		char filenameDebug[RAGE_MAX_PATH];
	};

	static DelayedBugCreateInfo sm_DelayedBugCreateInfo;
#endif // RSG_PC && !RSG_TOOL
};
#endif //ENABLE_BUGSTAR

class qaBugList
{
public:
	struct Info {
		int BugNumber;
		float X, Y, Z;
		char Summary[128 - 16];
	};

	// PURPOSE: Call once in a while to check for updated contents.
	// PARAMS:	Filename to check; default is c:\Bug List.csv.
	// RETURNS:	True if the list changed (new contents, or was erased)
	static bool Update(const char *filename = NULL);

	// PURPOSE: Retrieve bug count (may sponaneously change after calling Update)
	static int GetBugCount();

	// PURPOSE: Retrieve bug information.  Copy this locally if necessary,
	//			next call to Update may invalidate it.
	static const qaBugList::Info& GetBugInfo(int idx);
};

}	// namespace rage

#endif	// QA_BUGSTARINTERFACE_H
