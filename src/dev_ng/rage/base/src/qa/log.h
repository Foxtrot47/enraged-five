// 
// qa/log.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef QA_LOG_H
#define QA_LOG_H

#include "qa/qa.h"

#if __QA

namespace rage
{

//PURPOSE:
//  qaLog provides a basic logging facility for the qa unittest system.
class qaLog
{
public:

    enum InitFlags
    {
        //Truncate the log file.  Default is to append.
        INIT_TRUNCATE_FILE  = 0x01,
    };

    static const char* DEFAULT_LOG_FILE_NAME;

    //PURPOSE:
    //  Initializes the logging system.
    //  Opens the log file for writing.
    //NOTES
    //  Calling Init() is optional.  If it is not called a default log file
    //  will be opened.
    static bool Init( const char* filename,
                      const unsigned flags );

    //PURPOSE:
    //  Shutsdown the logging system.
    //  Flushes all output and closes the log file.
    static void Shutdown();

    //PURPOSE:
    //  Logs a message to the log file.
    static void Log( const char* fmt, ... );
};

//PURPOSE:
//  Convenience macro for logging messages to the log file.
#define QALog   qaLog::Log

}   //namespace rage

#endif  //__QA

#endif  //QA_LOG_H
