// 
// qa/item.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "item.h"
#include "result.h"

#include "system/timemgr.h"

namespace rage {

#if __QA

qaItem::qaItem()
{
}

qaItem::~qaItem()
{
}

void qaItem::Perform(qaResult& result)
{
	sysTimer timer;

	while (result.GetCondition() == qaResult::INCOMPLETE)
	{
		Update(result);
		result.TestTime(timer.GetTime());
		timer.Reset();
	}
}

void qaItem::Draw()
{
}

void qaItem::Shutdown()
{
}

#endif // __QA

} // namespace rage

