// 
// qa/item.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef QA_ITEM_H
#define QA_ITEM_H

#include "qa.h"

#if __QA

#include "system/timer.h"

namespace rage {

class qaResult;

class qaItem
{
public:
	qaItem();
	virtual ~qaItem();

	void Register();

	void Perform(qaResult& result);

	virtual void Update(qaResult& result) = 0;
	virtual void Draw();

	virtual void Shutdown();

protected:
};

} // namespace rage

#endif // __QA

#endif // QA_ITEM_H
