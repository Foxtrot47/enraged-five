// 
// qa/result.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "diag/output.h"
#include "diag/xmllog.h"
#include "result.h"
#include "resultmanager.h"
#include "log.h"

#include <stdio.h>

namespace rage {

#if __QA

qaResult::qaResult()
{
	Reset();
}

void qaResult::Reset()
{
	m_TotalTime = 0.0f;
	m_TestTimes.clear();
	m_ExtraResult = -1e30f;
	m_InitTime = -1.0f;
	m_ShutdownTime = -1.0f;
	m_Condition = INCOMPLETE;
	m_ReportType = FAIL_OR_TOTAL_TIME;
}

void qaResult::SetName(const char* name, const char* params)
{
	strncpy(m_TestName, name, MAX_TEST_NAME_LENGTH);
	m_TestName[MAX_TEST_NAME_LENGTH - 1] = '\0';

	strncpy(m_ParamsName, params, MAX_TEST_NAME_LENGTH);
	m_ParamsName[MAX_TEST_NAME_LENGTH - 1] = '\0';
}

void qaResult::SetExtraResult(float result)
{
	m_ExtraResult = result;
}

void qaResult::SetInitTime(float t)
{
	Assert(m_InitTime == -1.0f);
	m_InitTime = t;
}

void qaResult::SetShutdownTime(float t)
{
	Assert(m_ShutdownTime == -1.0f);
	m_ShutdownTime = t;
}

void qaResult::TestTime(float t)
{
	m_TotalTime += t;
	m_TestTimes.push_back(t);
}

qaResult::Condition qaResult::GetCondition()
{
	return m_Condition;
}

void qaResult::SetReportType(int reportType)
{
	m_ReportType = ReportType(reportType);
}

const char* qaResult::GetReportTypeName(ReportType reportType) const
{
	switch (reportType)
	{
	case PASS_OR_FAIL:
		return "Pass/Fail";
	case PASS_OR_EXTRA:
		return "Extra result";
	case FAIL_OR_EXTRA:
		return "Extra result";
	case PASS_OR_TOTAL_TIME:
		return "Total time";
	case FAIL_OR_TOTAL_TIME:
		return "Total time";
	case PASS_OR_AVERAGE_TIME:
		return "Average time";
	case FAIL_OR_AVERAGE_TIME:
		return "Average time";
	case PASS_OR_NUM_UPDATES:
		return "Number of updates";
	case FAIL_OR_NUM_UPDATES:
		return "Number of updates";
	default:
		return "Unknown";
	}
}

void qaResult::Fail()
{
	Assert(m_Condition == INCOMPLETE);
	m_Condition = FAIL;
}

void qaResult::Pass()
{
	Assert(m_Condition == INCOMPLETE);
	m_Condition = PASS;
}

void qaResult::Display( bool outputErrors, bool OutputXml)
{
	if ( OutputXml )
	{
		XmlStdLog().GroupStart("Test");
		XmlStdLog().Write("Name",  m_TestName );
		XmlStdLog().Write("Params",  m_ParamsName );

		XmlStdLog().Write("Success", ( m_Condition == PASS  ));
		XmlStdLog().Write("TotalTime", m_TotalTime);		
		XmlStdLog().GroupEnd("Test");
	}

	if (!OutputXml) QALog("Test: %s%s", m_TestName, m_ParamsName);
	switch (m_Condition)
	{
	case INCOMPLETE:
		if (!OutputXml)  QALog(TRed"Condition: Incomplete");
		if ( outputErrors )
		{
			Errorf( "Test %s%s  Condition: Incomplete " , m_TestName, m_ParamsName);
		}
		break;
	case PASS:
		if (!OutputXml)  QALog(TGreen"Condition: Pass");
		break;
	case FAIL:
		if (!OutputXml) QALog(TRed"Condition: Fail");
		if ( outputErrors )
		{
			Errorf( "Test %s%s  Condition: Fail " , m_TestName, m_ParamsName);
		}
		break;
	}

	if (OutputXml) 
	{
		return;
	}
	if (m_ExtraResult != -1e30f)
	{
		QALog("Result: %f", m_ExtraResult);
	}

	QALog("Init Time: %f", m_InitTime);
	QALog("Shutdown Time: %f", m_ShutdownTime);
	QALog("Frame times:");

	for (unsigned int frame = 0; frame < m_TestTimes.size(); ++frame)
	{
		QALog("   %d: %f", frame, m_TestTimes[frame]);
	}

	QALog("Total test time: %f", m_TotalTime);
	QALog("Average frame time: %f", m_TotalTime / m_TestTimes.size());
}

void qaResult::Report(qaResultManager& manager)
{
	ReportType remainingReportTypes = m_ReportType;

	while (remainingReportTypes)
	{
		ReportType nextRemainingReportTypes = ReportType(remainingReportTypes & (remainingReportTypes - 1));
		ReportType reportType = ReportType(remainingReportTypes - nextRemainingReportTypes);
		remainingReportTypes = nextRemainingReportTypes;
		std::string& dest = manager.GetResult(m_TestName, m_ParamsName, GetReportTypeName(reportType));

		if (m_Condition == INCOMPLETE)
		{
			dest = "Incomplete";
		}
		else
		{
			switch (reportType)
			{
			case PASS_OR_FAIL:
			case PASS_OR_EXTRA:
			case PASS_OR_TOTAL_TIME:
			case PASS_OR_AVERAGE_TIME:
			case PASS_OR_NUM_UPDATES:
				if (m_Condition == PASS)
				{
					dest = "Pass";
					continue;
				}
			default:
				break;
			}

			switch (reportType)
			{
			case PASS_OR_FAIL:
			case FAIL_OR_EXTRA:
			case FAIL_OR_TOTAL_TIME:
			case FAIL_OR_AVERAGE_TIME:
			case FAIL_OR_NUM_UPDATES:
				if (m_Condition == FAIL)
				{
					dest = "Fail";
					continue;
				}
			default:
				break;
			}

			char buffer[256];

			switch (reportType)
			{
			case PASS_OR_EXTRA:
			case FAIL_OR_EXTRA:
				sprintf(buffer, "%f", m_ExtraResult);
				dest = buffer;
				continue;
			case PASS_OR_TOTAL_TIME:
			case FAIL_OR_TOTAL_TIME:
				sprintf(buffer, "%f", m_TotalTime);
				dest = buffer;
				continue;
			case PASS_OR_AVERAGE_TIME:
			case FAIL_OR_AVERAGE_TIME:
				sprintf(buffer, "%f", m_TotalTime / m_TestTimes.size());
				dest = buffer;
				continue;
			case PASS_OR_NUM_UPDATES:
			case FAIL_OR_NUM_UPDATES:
				sprintf(buffer, "%d", m_TestTimes.size());
				dest = buffer;
				continue;
			default:
				Errorf("Unknown report type!");
				continue;
			}
		}
	}
}

#endif // __QA

} // namespace rage

