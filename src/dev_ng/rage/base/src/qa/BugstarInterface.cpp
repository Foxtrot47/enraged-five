//
// qa/BugstarInterface.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "BugstarInterface.h"

#include "string/string.h"
#include "file/remote.h"
#include "file/tcpip.h"
#include "file/stream.h"
#include "file/asset.h"
#include "math/amath.h"

#include "atl/array.h"
#include "system/timer.h"

#include <stdarg.h>

#include "grcore/device.h" // Need for NV_SUPPORT macro

#if RSG_PC && !RSG_TOOL
#pragma warning(disable: 4668)
#include <shlobj.h>
#pragma warning(error: 4668)
#endif // RSG_PC && !RSG_TOOL

#if !defined(__UNITYBUILD)
#ifdef GetMessage
#undef GetMessage
#endif
#endif // RSG_PC

namespace rage {

#if ENABLE_BUGSTAR

/////////// qaBug members /////////////////////////////////////////////////



void qaBug::AppendData(void* data,size_t size)
{
	char* tmp = rage_new char[m_buff_size + size];
	memset(tmp,0,m_buff_size + size);

	memcpy(tmp,m_buff,m_buff_size);
	memcpy(tmp + m_buff_size,data,size);

	delete [] (char*) m_buff;

	m_buff_size += size;
	m_buff = tmp;
}

#if __BE
u32 byte_swap(u32 b) 
{
	return (b >>24) | ((b >> 8) & 0xFF00) | ((b & 0xFF00) << 8) | (b << 24);
}

void memcpy_int(void *dest,void *src) 
{
	u8 *s = (u8*) src;
	u8 *d = (u8*) dest;
	d[0] = s[3];
	d[1] = s[2];
	d[2] = s[1];
	d[3] = s[0];
}
#else
u32 byte_swap(u32 b) { return b; }
#define memcpy_int(d,s) memcpy(d,s,sizeof(int))
#endif

qaBug& qaBug::operator=(const qaBug& other)
{
	char* tmp = rage_new char[other.GetMessageSize()];
	memcpy(tmp,other.GetMessage(),other.GetMessageSize());

	if (m_buff)
	{
		delete [] (char*) m_buff;
	}

	m_buff_size = other.GetMessageSize();
	m_buff = tmp;

	return *this;
}

void qaBug::SetField(int id,void* data,size_t size)
{
	size_t new_size = size + sizeof(int)*2; // strlen of '<tag>data</tag>'

	char* tmp = rage_new char[new_size];

	memcpy_int(tmp,&id);
	memcpy_int(tmp + sizeof(int),&size);
	memcpy(tmp + sizeof(int)*2,data,size);

	AppendData(tmp,new_size);
	
	delete [] tmp;
	
	return;
}


void qaBug::ClearBugBuffer()
{
	if(m_buff!= NULL)
	{
		delete [] (char*) m_buff;
		m_buff = NULL;

		m_buff_size = 0;
	}

}

void* qaBug::GetMessage() const
{
	return m_buff;
}

size_t qaBug::GetMessageSize() const
{
	return m_buff_size;
}

void qaBug::SetID(int val)
{
	char value[MAX_ARG_SIZE];

	formatf(value,sizeof(value),"%d",val);

	SetField(BUG_ID,(void*)value,sizeof(char) * strlen(value));
}

//void BugstarBase::SetOwner(unsigned int val)
void qaBug::SetOwner(const char* val)
{

	SetField(BUG_ASSIGNED_TO,(void*)val,sizeof(char) * strlen(val));
	
}

void qaBug::SetSeverity(const char* val)
{
	
	SetField(BUG_SEVERITY,(void*)val,sizeof(char) * strlen(val));
	
}

void qaBug::SetSummary(const char* val,...)
{
	char value[MAX_ARG_SIZE];
	
	va_list argptr;
	va_start(argptr, val);
	vformatf(value, sizeof(value), val, argptr);
	va_end(argptr);
	
	BeginField("summary","text");
	BeginCustomFieldData();	
	AddCustomFieldData((void*)value,sizeof(char) * strlen(value));
	EndCustomFieldData();	
	EndField();	
	
}

void qaBug::SetLogFile(const char *fileName)
{
	BeginField("logfileFilename", "text");
	BeginCustomFieldData();	
	AddCustomFieldData((void*)fileName,sizeof(char) * strlen(fileName));
	EndCustomFieldData();	
	EndField();	
}

void qaBug::SetProduct(const char* val)
{
	
	SetField(BUG_PRODUCT,(void*)val,sizeof(char) * strlen(val));
	
}

void qaBug::SetPlatform(unsigned int val)
{
	char value[MAX_ARG_SIZE];
	
	formatf(value,sizeof(value),"%d",val);

	SetField(BUG_REP_PLATFORM,(void*)value,sizeof(char) * strlen(value));
	
}
		
void qaBug::SetVersion(unsigned int val)
{
	char value[MAX_ARG_SIZE];
	
	formatf(value,sizeof(value),"%d",val);
	
	SetField(BUG_VERSION,(void*)value,sizeof(char) * strlen(value));
	
}
		
void qaBug::SetComponent(unsigned int val)
{
	char value[MAX_ARG_SIZE];
	
	formatf(value,sizeof(value),"%d",val);
	
	SetField(BUG_COMPONENT,(void*)value,sizeof(char) * strlen(value));
}

void qaBug::SetDescription(const char* val,...)
{
	char value[MAX_ARG_SIZE];

	va_list argptr;
	va_start(argptr, val);
	vformatf(value, sizeof(value), val, argptr);
	va_end(argptr);

//	SetField(BUG_DESCRIPTION, (void*)value, sizeof(char) * strlen(value));

 	BeginField("description","text");
 	BeginCustomFieldData();	
 	AddCustomFieldData((void*)value,sizeof(char) * strlen(value));
 	EndCustomFieldData();	
	EndField();	
}

void qaBug::AddCustomField(const char *name,const char *fmt,...)
{
	char tmp[256];
	va_list args;
	va_start(args,fmt);
	vformatf(tmp,sizeof(tmp),fmt,args);
	va_end(args);

	BeginField(name,"text");
	BeginCustomFieldData();	
	AddCustomFieldData((void*)tmp,sizeof(char) * strlen(tmp));
	EndCustomFieldData();	
	EndField();	
}


void qaBug::SetComments(const char* val,...)
{
	char value[MAX_ARG_SIZE];

	va_list argptr;
	va_start(argptr, val);
	vformatf(value, sizeof(value), val, argptr);
	va_end(argptr);

	BeginField("Comments", "text");
	BeginCustomFieldData();
	AddCustomFieldData((void*)value, sizeof(char) * strlen(value));
	EndCustomFieldData();
	EndField();
}

void qaBug::BeginField(const char* name,const char* dtype)
{
			
	/// send the name and type of a custom field 
	size_t name_len = 0;
	size_t type_len = 0;
	size_t offset = 0;
	
	name_len = strlen(name);
	type_len = strlen(dtype);
	
	size_t size = name_len + type_len + sizeof(int)*2;
	
	char* tmp = rage_new char[size];
	
	// copy the length of the name files 4 bytes
	memcpy_int(tmp,&name_len); 
	offset +=sizeof(int);
	
	// copy in the name field
	memcpy(tmp + offset,name,name_len); 
	offset += name_len;
	
	// copy in the length of the type 4 bytes
	memcpy_int(tmp + offset,&type_len); 
	offset +=sizeof(int);
	
	// copy the type in
	memcpy(tmp + offset,dtype,type_len); 
	offset += type_len;
	
	SetField(BUG_CUSTOMFIELDS,tmp,size);
	
	delete [] tmp;

}
	
void qaBug::EndField()
{
	

}



void qaBug::BeginCustomFieldData()
{


}

void qaBug::EndCustomFieldData()
{
	int tmp = 0;
	
	AppendData(&tmp,sizeof(int));
}
	

void qaBug::AddCustomFieldData(void* data,size_t size)
{

	char* tmp = rage_new char[size + sizeof(int)];
	
	
	memcpy_int(tmp,&size);
	memcpy(tmp + sizeof(int),data,size);

	AppendData((void*)tmp,size + sizeof(int));
	
	delete [] tmp;

}

void qaBug::SetPos(float x,float y,float z)
{
	AddCustomField("XCoord","%f",x);
	AddCustomField("YCoord","%f",y);
	AddCustomField("ZCoord","%f",z);
}

void qaBug::SetGrid(const char* grid)
{
	AddCustomField("grid","%s",grid);
}



///////////// qaBugstarInterface ////////////////////////////////////

const char* qaBugstarInterface::sm_ProductName = "RAGE";
#if RSG_PC && !RSG_TOOL
qaBugstarInterface::DelayedBugCreateInfo qaBugstarInterface::sm_DelayedBugCreateInfo;
#endif // RSG_PC && !RSG_TOOL

void qaBugstarInterface::SetProduct(const char* productName)
{
	sm_ProductName=productName;
}

qaBugstarInterface::qaBugstarInterface()
{
	strcpy(m_method,"method");
	strcpy(m_user_name,"unknown user");
	strcpy(m_target_name,"target");		// is this even used anywhere?
	strcpy(m_target_ip,fiDeviceTcpIp::GetLocalHost());
	m_port = 3490;
}

qaBugstarInterface::~qaBugstarInterface(void)
{
	
}

int qaBugstarInterface::SendMsg(unsigned int msg_id, void* data, size_t data_size, void *data2, size_t data_size2)
{
	m_socket = fiDeviceTcpIp::Connect(m_target_ip,m_port);

	if(!fiIsValidHandle(m_socket))
	{
#if ENABLE_BUGSTAR

#if !__WIN32PC
		if (fiRemoteServerIsRunning)
#endif
		{
			fiRemoteShowMessageBox(
				"Failed to add bug to Bugstar.\n"
				"Make sure that: \n\n"
				"1.        You have Bugstar v4 open \n"
				"2.        You are set to the correct project \n"
				"3.        You have enabled game integration in B* v4: Tools > Enabled Integration \n",
				"Adding Bug to Bugstar failed!",MB_OK|MB_ICONERROR,IDOK);
		}
#endif // ENABLE_BUGSTAR
		Errorf("Failed to connect to bugstar at %s:%d",m_target_ip,m_port);
		return 0;
	}
	
	// send the message id
	u32 send_msg_id = byte_swap(msg_id);
	SendData((void*)&send_msg_id,sizeof(unsigned int));
	
	// sent the message data
	// SendData((void*)data,data_size);
	// SendData sends sizeof(int) data_size (byte swapped), then the bytes, then sizeof(int) zero.

	// If the screenshot is present, its payload is a header word, its bytes, and an internal terminating zero
	u32 send_size = byte_swap((u32)(data_size + data_size2 + (data2 && data_size2? sizeof(int)*2 :0)));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&send_size,sizeof(unsigned int));

	// send the data
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)data,(int)data_size);

	if (data2 && data_size2)
		SendData(data2,data_size2);

	// send a final 0 (doesn't need byte swap)
	unsigned int zero = 0;
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&zero,sizeof(unsigned int));
	
	fiDeviceTcpIp::GetInstance().Close(m_socket);
	m_socket = fiHandleInvalid;
	
	return 0;
}


void qaBugstarInterface::CreateBug(qaBug& bug, const char* filename, int id, const char* filenameDebug, bool isPostRender)
{
#if RSG_PC && NV_SUPPORT && !__RGSC_DLL
	if (!isPostRender && GRCDEVICE.IsStereoEnabled() && GRCDEVICE.CanUseStereo())
	{
		sm_DelayedBugCreateInfo.isPendingBugCreate = true;
		sm_DelayedBugCreateInfo.bug = bug;
		sm_DelayedBugCreateInfo.id = id;
		sm_DelayedBugCreateInfo.filename[0] = '\0';
		sm_DelayedBugCreateInfo.filenameDebug[0] = '\0';

		if (filename)
		{
			strncpy(sm_DelayedBugCreateInfo.filename, filename, MAX_PATH - 1);
			sm_DelayedBugCreateInfo.filename[MAX_PATH - 1] = '\0';
		}

		if (filenameDebug)
		{
			strncpy(sm_DelayedBugCreateInfo.filenameDebug, filenameDebug, MAX_PATH - 1);
			sm_DelayedBugCreateInfo.filenameDebug[MAX_PATH - 1] = '\0';
		}

		return;
	}

	if (isPostRender && !(GRCDEVICE.IsStereoEnabled() && GRCDEVICE.CanUseStereo()))
#else
	if (isPostRender)
#endif // RSG_PC && NV_SUPPORT
	{
		Assertf(false, "qaBugstarInterface::CreateBug() - This assert can be removed if you intentionally want to use this path.");
		return;
	}
#if __XENON
	bug.SetField(BUG_REP_PLATFORM,(void*)"Xbox 360",8);
#elif __PPU
	bug.SetField(BUG_REP_PLATFORM,(void*)"PS3",3);
#elif __WIN32PC
	bug.SetField(BUG_REP_PLATFORM,(void*)"PC",2);		// It's actually 'PC Windows' on RDR2 but we're not shipping on that platform
#elif RSG_ORBIS
	bug.SetField(BUG_REP_PLATFORM,(void*)"PS4",3); 
#elif RSG_DURANGO
	bug.SetField(BUG_REP_PLATFORM,(void*)"Xbox One",8); 
#endif

	if(id != -1)
		bug.SetID(id);

	bug.SetProduct(sm_ProductName);

	ReadConfigData();

	m_socket = fiDeviceTcpIp::Connect(m_target_ip,m_port);

	if(!fiIsValidHandle(m_socket))
	{
#if ENABLE_BUGSTAR

#if !__WIN32PC
		if (fiRemoteServerIsRunning)
#endif
		{
			fiRemoteShowMessageBox(
				"Failed to add bug to Bugstar.\n"
				"Make sure that: \n\n"
				"1.        You have Bugstar v4 open \n"
				"2.        You are set to the correct project \n"
				"3.        You have enabled game integration in B* v4: Tools > Enabled Integration \n",
				"Adding Bug to Bugstar failed!",MB_OK|MB_ICONERROR,IDOK);
		}
#endif // ENABLE_BUGSTAR
		Errorf("Failed to connect to bugstar at %s:%d",m_target_ip,m_port);
		return;
	}

	fiStream *jpg			= NULL;
	fiStream *jpgDebug		= NULL;
	fiStream *stereoImage	= NULL;
	char screenshotStereoCustFieldName[RAGE_MAX_PATH] = "Attachment_";
	u32 bodyMessageLength	= 0;

#if RSG_PC && NV_SUPPORT && !__RGSC_DLL
	if (GRCDEVICE.IsStereoEnabled() && GRCDEVICE.CanUseStereo())
	{
		char stereoImagePath[MAX_PATH] = {0};

		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, stereoImagePath)))
		{	// CSIDL_PERSONAL = My Documents
			const char* stereoImageFolderName = "\\NVStereoscopic3D.IMG\\";
			strncat(stereoImagePath, stereoImageFolderName, MAX_PATH - strlen(stereoImagePath) - 1);
			stereoImagePath[MAX_PATH - 1] = '\0';

			// Iterate through directory and grab latest file.
			char stereoImageSearchPath[MAX_PATH] = {0};
			strncpy(stereoImageSearchPath, stereoImagePath, MAX_PATH - 1);
			strncat(stereoImageSearchPath, "*.pns", MAX_PATH - strlen(stereoImageSearchPath) - 1);
			stereoImageSearchPath[MAX_PATH - 1] = '\0';

			WIN32_FIND_DATA FindFileData;
			HANDLE hFind = FindFirstFileA(stereoImageSearchPath, &FindFileData);

			if (hFind == INVALID_HANDLE_VALUE)
			{
				Errorf("qaBugstarInterface::CreateBug() - Unable to find Nvidia 3D screenshot. Error: 0x%x", GetLastError());
			}
			else
			{
				char mostRecentFileName[MAX_PATH];
				FILETIME mostRecentFileTime;

				strncpy(mostRecentFileName, FindFileData.cFileName, MAX_PATH - 1);
				mostRecentFileName[MAX_PATH - 1] = '\0';
				mostRecentFileTime = FindFileData.ftLastWriteTime;

				while (FindNextFileA(hFind, &FindFileData))
				{
					if (CompareFileTime(&FindFileData.ftLastWriteTime, &mostRecentFileTime) > 0)
					{
						strncpy(mostRecentFileName, FindFileData.cFileName, MAX_PATH - 1);
						mostRecentFileName[MAX_PATH - 1] = '\0';
						mostRecentFileTime = FindFileData.ftLastWriteTime;
					}
				}

				strncat(stereoImagePath, mostRecentFileName, MAX_PATH - strlen(stereoImagePath) - 1);
				stereoImagePath[MAX_PATH - 1] = '\0';
				stereoImage = ASSET.Open(stereoImagePath, "");

				strncat(screenshotStereoCustFieldName, mostRecentFileName, RAGE_MAX_PATH - strlen(screenshotStereoCustFieldName) - 1);
				screenshotStereoCustFieldName[RAGE_MAX_PATH - 1] = '\0';
			}
		}
	}
	else
#endif // RSG_PC && NV_SUPPORT && !__RGSC_DLL
	{
		jpg      = filename? ASSET.Open(filename, "") : NULL;
		jpgDebug = filenameDebug? ASSET.Open(filenameDebug, "") : NULL;
	}


	
	// We send the header info to bugstar with the total body message length
	const char* screenshotCustFieldName      = "ScreenShot";
	const char* screenshotDebugCustFieldName = "ScreenShot_OnlineDebug";
	bodyMessageLength = byte_swap((u32)bug.GetMessageSize());
	bodyMessageLength += GetCustomFieldLength_Binary(screenshotCustFieldName, jpg);
	bodyMessageLength += GetCustomFieldLength_Binary(screenshotDebugCustFieldName, jpgDebug);
	bodyMessageLength += GetCustomFieldLength_Binary(screenshotStereoCustFieldName, stereoImage);

	//--------------
	// - if you want to add an attachment
	// or 
	// - if you want to add a filepath so B* can get the file using TCP
	//--------------
// 	fiStream *attachment = ASSET.Open("X:\\file.exe", "");
// 	bodyMessageLength += GetCustomFieldLength_Binary("Attachment_file.exe", attachment);		// NOTE : the name has to be Attachment_filename.ext
// 	bodyMessageLength += GetCustomFieldLength_Text("Filepath", "X:\\bigFile.mp4");				// NOTE : the absolute filepath (extension included)
	
	SendHeader(bodyMessageLength);
	{
		// send the bug data (platform, bug owner, player pos, etc.)
		fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, bug.GetMessage(), (int) bug.GetMessageSize());
	
		// write each screenshots on the socket if they are required
		if (jpg)
		{
			SendCustomField_Binary(screenshotCustFieldName, jpg);
		}
		if (jpgDebug)
		{
			SendCustomField_Binary(screenshotDebugCustFieldName, jpgDebug);
		}
		if (stereoImage)
		{
			SendCustomField_Binary(screenshotStereoCustFieldName, stereoImage);
		}

		// same as above for additional attachments
// 		SendCustomField_Binary("Attachment_file.exe", attachment);
// 		SendCustomField_Text("Filepath", "X:\\bigFile.mp4");
	}
	SendFooter();		// send the terminating zero

	fiDeviceTcpIp::GetInstance().Close(m_socket);
	m_socket = fiHandleInvalid;

	return;
}

void qaBugstarInterface::OpenBugForEdit(qaBug& bug)
{
	ReadConfigData();

	m_socket = fiDeviceTcpIp::Connect(m_target_ip,m_port);

	if(!fiIsValidHandle(m_socket))
	{
#if ENABLE_BUGSTAR

#if !__WIN32PC
		if (fiRemoteServerIsRunning)
#endif
		{
			fiRemoteShowMessageBox(
				"Failed to add bug to Bugstar.\n"
				"Make sure that: \n\n"
				"1.        You have Bugstar v4 open \n"
				"2.        You are set to the correct project \n"
				"3.        You have enabled game integration in B* v4: Tools > Enabled Integration \n",
				"Adding Bug to Bugstar failed!",MB_OK|MB_ICONERROR,IDOK);
		}
#endif // ENABLE_BUGSTAR
		Errorf("Failed to connect to bugstar at %s:%d",m_target_ip,m_port);
		return;
	}

	const unsigned int zero = 0;

	// send the message id
	u32 send_msg_id = byte_swap(MSG_BUG);
	SendData((void*)&send_msg_id,sizeof(unsigned int));

	u32 data_size = (u32)bug.GetMessageSize();
	void* data = bug.GetMessage();

	u32 send_size = byte_swap((u32)data_size);
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&send_size,sizeof(unsigned int));

	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)data,(int)data_size);
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&zero,sizeof(unsigned int));

	fiDeviceTcpIp::GetInstance().Close(m_socket);
	m_socket = fiHandleInvalid;
}

#if RSG_PC && !RSG_TOOL
void qaBugstarInterface::HandleDelayedBugCreate()
{
	if (sm_DelayedBugCreateInfo.isPendingBugCreate)
	{
		qaBugstarInterface BUGSTAR;
		BUGSTAR.CreateBug(sm_DelayedBugCreateInfo.bug,
							sm_DelayedBugCreateInfo.filename[0] == '\0' ? NULL : sm_DelayedBugCreateInfo.filename,
							sm_DelayedBugCreateInfo.id,
							sm_DelayedBugCreateInfo.filenameDebug[0] == '\0' ? NULL : sm_DelayedBugCreateInfo.filenameDebug,
							true);
		
		sm_DelayedBugCreateInfo.isPendingBugCreate = false;
	}
}
#endif // RSG_PC && !RSG_TOOL

//////////////// protected functions /////////////////////////////////////////////


void qaBugstarInterface::Init()
{

}


void qaBugstarInterface::Term()
{
}


void qaBugstarInterface::SendData(void* data, size_t size)
{	
		// send the size of the data
		u32 send_size = byte_swap((u32)size);
		fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&send_size,sizeof(unsigned int));
		// send the data
		fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, data, (int)size);
		// send a final 0 (doesn't need byte swap)
		unsigned int zero = 0;
		fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&zero,sizeof(unsigned int));
}

void qaBugstarInterface::SendHeader(size_t bodyMessageLength)
{
	// Header layout :
	// 1.1 - int : length of header (constant)
	// 1.2 - int : Message ID
	// 1.3 - int : Error checking (always 0)
	// 1.4 - int : length of the body message
	
	const unsigned int zero = 0;
	u32 header_size = byte_swap((u32)sizeof(unsigned int));
	u32 send_msg_id = byte_swap(MSG_BUG);

	// send the header info in the same order as the header layout above
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&header_size,sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&send_msg_id,(int)sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&zero,sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&bodyMessageLength,sizeof(unsigned int));
}

void qaBugstarInterface::SendFooter()
{
	// The footer is just a terminating zero
	const unsigned int zero = 0;
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&zero,sizeof(unsigned int));
}


//-----------------------------------------------------------------------------
// These 3 functions write out data to the TCP socket following the same layout
// Custom Field (CF) layout :
// 3.1 - int :    CF flag (BUG_CUSTOMFIELDS)
// 3.2 - int :    length of 3.3 + 3.4 + 3.5 + 3.6 (aka information fields)
// 3.3 - int :    length of the name of the CF (ex : strlen("ScreenShot")) 
// 3.4 - string : name of the CF (ex : "ScreenShot")
// 3.5 - int :    length of the type of the CF (ex : strlen("image"))
// 3.6 - string : name of the CF type (ex : "image")
// 3.7 - int :    length of the CF data (ex : jpg->Size())
// 3.8 - void* :  data we're sending
// 3.9 - int :    Error checking (always 0)
//-----------------------------------------------------------------------------

void qaBugstarInterface::SendCustomField_Binary(const char* name, fiStream* stream)
{
	const char* dtype = "binary";
	const u32 customFieldFlag = BUG_CUSTOMFIELDS;
	const u32 nameLength = (u32)strlen(name);
	const u32 typeLength = (u32)strlen(dtype);
	const u32 zero = 0;
	const u32 infoFieldsLength = nameLength + typeLength + sizeof(unsigned int)*2;
	const u32 streamSize = stream->Size();

	// send the CF info in the same order as the CF layout above
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&customFieldFlag,  sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&infoFieldsLength, sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&nameLength,       sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)name,              (int)nameLength);
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&typeLength,       sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)dtype,             (int)typeLength);
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&streamSize,       sizeof(unsigned int));
	
	// write the jpg on the socket (phase 3.8 on the CF layout)
	const u32 BUFFER_SIZE = 32*1024;
	u32 size = streamSize;
	u8 buffer[BUFFER_SIZE];
	while (size)
	{
		u32 toRead = Min(BUFFER_SIZE, size);

		u32	amtRead = stream->Read(buffer,toRead);
		size -= amtRead;

		fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)buffer,(int)amtRead);
	}
	stream->Close();

	// CF terminating zero
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket,(void*)&zero,sizeof(unsigned int));
}

void qaBugstarInterface::SendCustomField_Text(const char* name, const char* text)
{
	const char* dtype = "text";
	const u32 customFieldFlag = BUG_CUSTOMFIELDS;
	const u32 nameLength = (u32)strlen(name);
	const u32 typeLength = (u32)strlen(dtype);
	const u32 textLength = (u32)strlen(text);
	const u32 zero = 0;
	const u32 infoFieldsLength = nameLength + typeLength + sizeof(unsigned int)*2;

	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&customFieldFlag,  sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&infoFieldsLength, sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&nameLength,       sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)name,              (int)nameLength);
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&typeLength,       sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)dtype,             (int)typeLength);
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&textLength,       sizeof(unsigned int));
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)text,              (int)textLength);
	fiDeviceTcpIp::GetInstance().SafeWrite(m_socket, (void*)&zero,             sizeof(unsigned int));
}

u32 qaBugstarInterface::GetCustomFieldLength(const char* name, const char* dtype, u32 size)
{
	u32 customFieldLength = 0;
	const u32 nameLength = (u32)strlen(name);
	const u32 typeLength = (u32)strlen(dtype);

	customFieldLength = nameLength + typeLength + size + sizeof(unsigned int)*6;		// there's 6 ints in the CF layout

	return customFieldLength;
}

u32 qaBugstarInterface::GetCustomFieldLength_Binary(const char* name, fiStream* stream)
{
	if (!stream)
	{
		return 0;
	}

	const u32 streamSize = (u32)stream->Size();
	return GetCustomFieldLength(name, "binary", streamSize);		// sending "binary" but could be anything as long as it's not "text"
}

u32 qaBugstarInterface::GetCustomFieldLength_Text(const char* name, const char* text)
{
	if (!text)
	{
		return 0;
	}

	const u32 textSize = (u32)strlen(text);
	return GetCustomFieldLength(name, "text", textSize);
}

/* void qaBugstarInterface::RecvData(void** data, unsigned int *size)
{

		unsigned int tmp_data = 0;
		
		m_network->RecvBytes(m_socket,(void*)&tmp_data,sizeof(unsigned int));
		
		if(tmp_data == 0)
		{
			return;
		}

		*data = rage_new char[tmp_data];
		*size = tmp_data;

		m_network->RecvBytes(m_socket,(void*)data, (unsigned long)*size);
		 
		m_network->RecvBytes(m_socket,(void*)&tmp_data,sizeof(unsigned int));

		if(tmp_data != 0)
		{
			// error getting the data so clean up
			delete *data;
			*data = NULL;

			*size = 0;
			return;
		}
		
	
	}	
}
*/




void qaBugstarInterface::ReadConfigData()
{
	fiStream *config = fiStream::Open("c:/bugs/bugstar.cfg");
	if (config) 
	{
		char buf[100];
		while (fgetline(buf,sizeof(buf),config))
			ParseParameterLine(buf);
		config->Close();
	}
}


void qaBugstarInterface::ParseParameterLine(char* param)
{
	int i = 0;
	char buff[100];
	int ti = 0;
	
	// read untill the colon
	while(param[i] && param[i] != ':')
	{
		buff[i] = param[i];
		i++;
	}	
	buff[i] = '\0';
	i++;

	// depending on the parameter convert it to the appropriate format and save it 
	// int to the class' member variable
	if(strcmp(buff,"target_ip") == 0)
	{
		// read the ip address
		while(param[i] == ' '){i++;} // eat the white space
		
		while(param[i] != '\n')
		{
			m_target_ip[ti] = param[i];
			i++;
			ti++;
		}
		m_target_ip[ti] = '\0';
	}
	else if(strcmp(buff,"target_name") == 0)
	{
		// read the name
		while(param[i] == ' '){i++;} // eat the white space

		while(param[i] != '\n')
		{
			m_target_name[ti] = param[i];
			i++;
			ti++;
		}
		m_target_name[ti] = '\0';
	}
	else if(strcmp(buff,"user_name") == 0)
	{
		// read the user name
		while(param[i] == ' '){i++;} // eat the white space
	
		while(param[i] != '\n')
		{
			m_user_name[ti] = param[i];
			i++;
			ti++;
			
		}
		m_user_name[ti] = '\0';
	}
	else if(strcmp(buff,"method") == 0)
	{
		// read the user name
		while(param[i] == ' '){i++;} // eat the white space
		
		while(param[i] != '\n')
		{
			m_method[ti] = param[i];
			i++;
			ti++;

		}
		m_method[ti] = '\0';
	}
	else
		Errorf("Unknown config command '%s' ignored",buff);
}

static u64 s_LastFileTime;
static atArray<qaBugList::Info> s_BugInfo;
sysTimer s_PollTimer;

// #pragma optimize("",off)

bool qaBugList::Update(const char *filename)
{
	if (!filename)
		filename = "c:\\Bug List.csv";

	// Don't check constantly and hammer the network.
	if (s_PollTimer.GetTime() < 3.0f)
		return false;
	s_PollTimer.Reset();

	const fiDevice *device = fiDevice::GetDevice(filename);
	if (!device)
		return false;
	u64 thisFileTime = device->GetFileTime(filename);
	if (thisFileTime != s_LastFileTime)
	{
		s_LastFileTime = thisFileTime;

		s_BugInfo.Reset();

		fiSafeStream S(fiStream::Open(filename));

		if (!S) 
		{
			Displayf("[BUGLIST] - File '%s' was deleted.",filename);
			return true;
		}

		char header[512];
		fgetline(header,sizeof(header),S);
		char *h = header, *comma;
		int ci_Number = -1, ci_Summary = -1, ci_X = -1, ci_Y = -1, ci_Z = -1;
		int cols = 0;
		while (*h && (comma = strchr(h,',')) != NULL) 
		{
			*comma = 0;
			if (!strcmp(h,"Number")) ci_Number = cols;
			else if (!strcmp(h,"Summary")) ci_Summary = cols;
			else if (!strcmp(h,"X")) ci_X = cols;
			else if (!strcmp(h,"Y")) ci_Y = cols;
			else if (!strcmp(h,"Z")) ci_Z = cols;
			h = comma + 1;
			++cols;
		}
		Displayf("[BUGLIST] Number=%d, Summary=%d, X=%d, Y=%d, Z=%d",ci_Number,ci_Summary,ci_X,ci_Y,ci_Z);

		if (!Verifyf(ci_Number != -1,"Number column is missing from CSV file '%s', cannot continue.",filename)) return true;
		if (!Verifyf(ci_Summary != -1,"Summary column is missing from CSV file '%s', cannot continue.",filename)) return true;
		if (!Verifyf(ci_X != -1,"X column is missing from CSV file '%s', cannot continue.",filename)) return true;
		if (!Verifyf(ci_Y != -1,"Y column is missing from CSV file '%s', cannot continue.",filename)) return true;
		if (!Verifyf(ci_Z != -1,"Z column is missing from CSV file '%s', cannot continue.",filename)) return true;
		// Count the number of additional lines in the file so we can resize the array.
		int bugs = 0;
		while (fgetline(header,sizeof(header),S))
			++bugs;
		// Rewind and skip the header.
		S->Seek(0);
		fgetline(header,sizeof(header),S);
		s_BugInfo.Reserve(bugs);
		for (int i=0; i<bugs; i++) {
			Info &info = s_BugInfo.Append();
			fgetline(header,sizeof(header),S);
			atRangeArray<const char*,32> columns;
			char *field = header;
			int cols = 0;
			while (*field) 
			{
				if (*field == ',') 
				{
					columns[cols++] = "";
					++field;
				}
				else if (Verifyf(*field == '"',"Unquoted non-empty field in line %d of '%s'",i+2,filename)) 
				{
					columns[cols++] = ++field;
					// Skip to enclosing quote, except that a double-double quote is treated as a single double quote.
					while (field[0] && (field[0]!='"' || (field[0]=='"' && field[1]=='"')))
						field += 1 + (field[0]=='"' && field[1]=='"');
					*field++ = 0;
					Assert(*field == ',');
					++field;
				}
			}
			info.BugNumber = atoi(columns[ci_Number]);
			safecpy(info.Summary, columns[ci_Summary]);
			info.X = (float) atof(columns[ci_X]);
			info.Y = (float) atof(columns[ci_Y]);
			info.Z = (float) atof(columns[ci_Z]);
			// Kill the bug if it doesn't have a valid position.
			if (!info.X && !info.Y && !info.Z)
				s_BugInfo.Pop();
			else
				Displayf("[BUGLIST] %d @ <<%f,%f,%f>> \"%s\"",info.BugNumber,info.X,info.Y,info.Z,info.Summary);
		}
		Displayf("[BUGLIST] - %d bugs with valid positions parsed out of '%s'",s_BugInfo.GetCount(),filename);
		return true;
	}
	else
		return false;
}

int qaBugList::GetBugCount()
{
	return s_BugInfo.GetCount();
}

const qaBugList::Info& qaBugList::GetBugInfo(int idx)
{
	return s_BugInfo[idx];
}

#endif // ENABLE_BUGSTAR

}	// namespace rage
