//
// audiohardware/debug.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DEBUG_H
#define AUD_DEBUG_H

#include "string/string.h"

namespace rage
{

class audDebugDrawManager
{
public:

	audDebugDrawManager() 
		: m_TextScale(1.0f)
	{

	}

	virtual void DrawLine(const char *text) = 0;
	virtual void PushSection(const char *title) = 0;
	virtual void PopSection() = 0;

	void DrawLinef(const char *fmt, ...)
	{
		va_list args;
		va_start(args,fmt);
		char buf[256];
		vformatf(buf,sizeof(buf),fmt,args);
		DrawLine(buf);
		va_end(args);
	}
	virtual ~audDebugDrawManager(){}

	void SetTextScale(const float scale) { m_TextScale = scale; }
	float GetTextScale() const { return m_TextScale; }

private:

	float m_TextScale;
};

} // namespace rage

#endif // AUD_DEBUG_H
