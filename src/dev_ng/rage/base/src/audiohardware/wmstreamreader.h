//=====================================================================================================
// name:		AudioStreamReader.h
// description:	Media stream reader for audio streams.
//=====================================================================================================

#ifndef INC_WMSTREAMREADER_H_
#define INC_WMSTREAMREADER_H_
#if __WIN32PC && 0

#include "mediareader.h"

#pragma warning(disable: 4668)
#include <wmsdk.h>
#pragma warning(error: 4668)

#ifndef SAFE_RELEASE
#define SAFE_RELEASE( x )       if (x != NULL) { x->Release(); x = NULL; }
#endif
#define SAFE_CLOSEHANDLE( x )   if (x != NULL) { CloseHandle(x); x = NULL; }
#define BREAK_ON_FAIL(hResult)  if ((HRESULT)(hResult) < 0) { break; }
#define RETURN_ON_FAIL(hResult) if ((HRESULT)(hResult) < 0) { return 0; }

#define EXTRA_BYTE_BUFFER_SIZE  (128 * 1024) // 128kb

#define ASR_MIN_TRACK_LENGTH_MS	20*1000		

//#define OUTPUT_FILLBUFFER_AUDIO 

struct IWMReader;

namespace rage {

//=====================================================================================================
class audWMStreamReader : audMediaReaderPlatform
{
public:
	//-------------------------------------------------------------------------------------------------
	audWMStreamReader();
	~audWMStreamReader();

	//-------------------------------------------------------------------------------------------------
	bool	MediaOpen(const WCHAR* pszFileName, u32 startOffsetMs);
	bool	MediaClose();

	void Prepare();

	static bool IsAvailable();

	//-------------------------------------------------------------------------------------------------
	s32	FillBuffer(void *sampleBuf, u32 dwBytesToRead);

	//-------------------------------------------------------------------------------------------------
	s32	GetSampleRate();
	s32	GetStreamLengthMs();
	s32	GetStreamPlayTimeMs();

	void	SetCursor(u32 ms);

	virtual const char *GetArtist()
	{
		return m_Artist;
	}

	virtual const char *GetTitle()
	{
		return m_Title;
	}

	s32 GetNumChannels() const { return 2; }

private:
	//-------------------------------------------------------------------------------------------------
	bool	Open(const WCHAR* pwszFileName, u32 startOffsetMs);

	HRESULT	GetStreamNumbers(IWMProfile* pProfile);

	void	SetFileOpen(bool bOpen)				{ m_bFileOpen = bOpen; }
	bool	IsFinished() const					{ return m_bIsFinished; }
	bool	IsFileOpen() const					{ return m_bFileOpen; }

	void	DecodeBuffer();						// called by the decode thread
	bool	DecodeBuffer(void* destAddr0, void* destAddr1, size_t size);


	//-------------------------------------------------------------------------------------------------
	IWMSyncReader*				m_pReader;

	//-------------------------------------------------------------------------------------------------
	WORD						m_wAudioStreamNum;
	bool						m_bFileOpen;

	bool						m_bIsFinished;

	QWORD						m_qwLastSampleTime;
	QWORD						m_qwLastSampleDuration;
	char						m_cExtraBytes[EXTRA_BYTE_BUFFER_SIZE];
	DWORD						m_dwExtraBytes;
	DWORD						m_dwSampleRate;
	long						m_lDuration;
	WORD						m_nBlockAlign; 

	u32							m_StartOffsetMs;

	char						m_Artist[64];
	char						m_Title[64];
	static const u32			m_MaxRead = 1024 * 32;

	static HMODULE				sm_hWMVCore;



#ifdef OUTPUT_FILLBUFFER_AUDIO 
	HANDLE m_hFile;
#endif

};

}
#endif
#endif
