// 
// audiohardware/pcmsource.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "pcmsource.h"

#include "audiohardware/debug.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/grainplayer.h"
#include "audiohardware/sourcesubmix.h"
#include "audiohardware/streamplayer.h"
#include "audiohardware/waveplayer.h"
#include "audiosynth/synthesizer.h"
#include "audiosynth/synthcore.h"
#include "audiohardware/config.h"

namespace rage {

	extern audPcmSourcePool *g_PcmSourcePool;
	
	atRangeArray<audPcmVariableBlock, g_MaxPcmSourceVariableBlocks> audPcmSource::sm_PcmSourceVariableBlocks;
	atRangeArray<float, g_MaxPcmSourceVariableBlocks*audPcmVariableBlock::kNumVariablesPerBlock> audPcmSource::sm_PcmSourceVariableBlockValues ;
	u32 * audPcmSource::sm_PcmSourceVariableBlockRefs = NULL;

	bool audPcmVariableBlock::Init(const char *variableListName)
	{
		return Init(atStringHash(variableListName));
	}

	bool audPcmVariableBlock::Init(const u32 variableListNameHash)
	{
		ptrdiff_t index = this - &(audPcmSource::sm_PcmSourceVariableBlocks[0]);
		m_Values = &(audPcmSource::sm_PcmSourceVariableBlockValues[0]) + (index*kNumVariablesPerBlock);

		m_Metadata = audConfig::GetMetadataObject<ravesimpletypes::VariableList>(variableListNameHash);
		if(audVerifyf(m_Metadata, "Attempting to initialise variable block with invalid variable list (hash: %u)", variableListNameHash))
		{
			for(u32 i = 0; i < m_Metadata->numVariables; i++)
			{	
				m_Values[i] = m_Metadata->Variable[i].InitialValue;
			}
			return true;
		}
		return false;
	}

	f32 *audPcmVariableBlock::FindVariableAddress(const char *name) const
	{
		return FindVariableAddress(atStringHash(name));
	}

	f32 *audPcmVariableBlock::FindVariableAddress(const u32 hash) const
	{
		if(m_Metadata)
		{
			for(u32 i = 0; i < m_Metadata->numVariables; i++)
			{
				if(m_Metadata->Variable[i].Name == hash)
				{
					return &m_Values[i];
				}
			}
		}
		
		return NULL;
	}

	f32 *audPcmVariableBlock::GetVariableAddress(const u32 index) const
	{
		if(m_Metadata)
		{
			audAssert(index < m_Metadata->numVariables);
			return &m_Values[index];
		}

		return NULL;
	}

	u32 audPcmVariableBlock::GetVariableNameHash(const u32 index) const
	{
		if(m_Metadata)
		{
			audAssert(index < m_Metadata->numVariables);
			return m_Metadata->Variable[index].Name;
		}

		return 0;
	}

	void audPcmVariableBlock::SetVariable(const u32 hash, const f32 value)
	{
		if(m_Metadata)
		{
			for(u32 i = 0; i < m_Metadata->numVariables; i++)
			{
				if(m_Metadata->Variable[i].Name == hash)
				{
					m_Values[i] = value;
					return;
				}
			}
		}

		audAssertf(0, "Trying to set variable (hash: %u) but it's not present in this variable block", hash);
	}

	void audPcmSource::InitClass()
	{
		sm_PcmSourceVariableBlockRefs = rage_new u32[g_MaxPcmSourceVariableBlocks];
		sysMemZeroBytes<sizeof(sm_PcmSourceVariableBlocks)>(&sm_PcmSourceVariableBlocks);
		sysMemZeroBytes<sizeof(sm_PcmSourceVariableBlockValues)>(&sm_PcmSourceVariableBlockValues);
		sysMemZeroBytes<sizeof(u32)*g_MaxPcmSourceVariableBlocks>(sm_PcmSourceVariableBlockRefs);
	}

	void audPcmSource::ShutdownClass()
	{
		if(sm_PcmSourceVariableBlockRefs)
		{
			delete[] sm_PcmSourceVariableBlockRefs;
			sm_PcmSourceVariableBlockRefs = NULL;
		}
	}

#if !__SPU
	void audPcmSource::UpdateState(PcmSourceState &state)
	{
		if (IsFinished())
		{
			state.PlaytimeSamples = ~0U;
		}
		else
		{
			state.PlaytimeSamples = GetPlayPositionSamples();
		}

		state.UpdateMixerFrame = audDriver::GetMixer()->GetMixerTimeFrames();
		state.hasStartedPlayback = HasStartedPlayback();
		Assign(state.CurrentPeakLevel, GetCurrentPeakLevel());
	}
#endif

	bool audPcmSource::AllocateVariableBlock(const char * mapName)
	{
		return AllocateVariableBlock(atStringHash(mapName));
	}

	bool audPcmSource::AllocateVariableBlock(u32 mapNameHash) 
	{
		if (sm_PcmSourceVariableBlockRefs)
		{
			for(u16 i=0; i< g_MaxPcmSourceVariableBlocks; i++)
			{
				if(!sm_PcmSourceVariableBlockRefs[i])
				{
					//Found a free variable block
					sm_PcmSourceVariableBlockRefs[i]++;
					m_VariableBlock = i;
					GetVariableBlock()->Init(mapNameHash);
					GetVariableBlock()->SetPcmSource(audDriver::GetMixer()->GetPcmSourceSlotId(this));
					return true;
				}
			}
		}
		m_VariableBlock = -1;
		return false;
	}

	void audPcmSource::ReleaseVariableBlock()
	{
		if(m_VariableBlock >= 0)
		{
			FastAssert(sm_PcmSourceVariableBlockRefs[m_VariableBlock] > 0);
			RemoveVariableBlockRef(m_VariableBlock);
			m_VariableBlock = -1; 
		}
	}

	void audPcmSource::RemoveVariableBlockRef(int index)
	{
		if(index >= 0)
		{
			FastAssert(sm_PcmSourceVariableBlockRefs[index]>0);
			sysInterlockedDecrement(&sm_PcmSourceVariableBlockRefs[index]);

			if(sm_PcmSourceVariableBlockRefs[index] == 0)
			{
				sm_PcmSourceVariableBlocks[index].SetPcmSource(-1);
			}
		}
	}

	void audPcmSource::SetVariable(u32 nameHash, f32 value)
	{
		audPcmVariableBlock* varBlock = GetVariableBlock();
		if(varBlock)
		{
			varBlock->SetVariable(nameHash, value);
		}
	}

	f32 audPcmSource::GetVariableValue(u32 nameHash)
	{
		audPcmVariableBlock* varBlock = GetVariableBlock();
		if(varBlock)
		{
			const f32* var = varBlock->FindVariableAddress(nameHash);
			if(var)
			{
				return *var;
			}
		}
		return 0.f;
	}

	u32 audPcmSourceFactory::GetSlotSize(const audPcmSourceTypeId typeId)
	{
		switch(typeId)
		{
		case AUD_PCMSOURCE_WAVEPLAYER:
			return sizeof(audWavePlayer);
		case AUD_PCMSOURCE_GRAINPLAYER:
			return sizeof(audGrainPlayer);
		case AUD_PCMSOURCE_MODULARSYNTH:
			return sizeof(synthSynthesizer);
		case AUD_PCMSOURCE_SUBMIX:
			return sizeof(audSourceSubmix);
		case AUD_PCMSOURCE_SYNTHCORE:
			return sizeof(synthCorePcmSource);
		case AUD_PCMSOURCE_EXTERNALSTREAM:
			return sizeof(audStreamPlayer);
		default:
			Assert(0);
			return 0;
		}
	}

#if !__SPU
	void audPcmSourceFactory::InitSlot(const u32 slotId, const audPcmSourceTypeId typeId)
	{	
		void *slotPtr = g_PcmSourcePool->GetSlotPtr(slotId);
		Assert(slotPtr);
		switch(typeId)
		{
		case AUD_PCMSOURCE_WAVEPLAYER:
			rage_placement_new(slotPtr) audWavePlayer();
			break;
		case AUD_PCMSOURCE_GRAINPLAYER:
			rage_placement_new(slotPtr) audGrainPlayer();
			break;
#if __SYNTH_EDITOR
		case AUD_PCMSOURCE_MODULARSYNTH:
			rage_placement_new(slotPtr) synthSynthesizer();
			break;
#endif
		case AUD_PCMSOURCE_SUBMIX:
			rage_placement_new(slotPtr) audSourceSubmix();
			break;
		case AUD_PCMSOURCE_SYNTHCORE:
			rage_placement_new(slotPtr) synthCorePcmSource();
			break;
		case AUD_PCMSOURCE_EXTERNALSTREAM:
			rage_placement_new(slotPtr) audStreamPlayer();
			break;
		default:
			Assert(0);
		}
		// Mark slot as initialised
		g_PcmSourcePool->InitSlot(slotId, typeId);
		
	}
#endif // !__SPU

	u32 audPcmSourceFactory::GetMaxSize()
	{
		u32 maxSize = 0;
		for(s32 i = 0; i < AUD_NUM_PCMSOURCES; i++)
		{
			maxSize = Max(maxSize, GetSlotSize((audPcmSourceTypeId)i));
		}
		return maxSize;
	}
} // namespace rage
