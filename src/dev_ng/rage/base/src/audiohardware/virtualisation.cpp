//
// audiohardware/virtualisation.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "system/dma.h"
#include "system/memory.h"
#include "driverdefs.h"
#include "driverutil.h"
#include "pcmsource_interface.h"
#include "virtualisation.h"
#include "voicesettings.h"
#include "waveref.h"
#include "audioengine/soundpool.h"
#include "atl/array.h"

#if !__SPU
#include "device.h"
#include "pcmsource.h"
#include "audiosoundtypes/sound.h"
#include "driver.h"
#else
#include <cell/sync/mutex.h>
#endif

namespace rage
{
#if __DEV
u32 g_MaxActiveVirtualVoices = 0;
u32 g_MaxActiveVoiceGroups = 0;
#endif

#if __SPU
extern u32 g_NumBuckets;
extern u32 g_BucketSize;
extern void *g_FirstBucketEA;
extern u32 g_DeltaTimeMs;
extern void *g_BucketLocks;
#endif

//extern u32 g_DontVirtualiseWaveNameHash;

void BuildPrioritisedVoiceList(PrioritisedVoice *&voiceList, VoiceGroup *&groupList, audVirtualisationJobOutput *header, PrioritisedGroupList& prioritisedGroupList, VoiceBinsList& voiceBinsList)
{
	VoiceSortBin *bins = NULL;
	u32 numActivePlayPhysicalVoices = 0;
	u32 numActiveVoices = 0, numActiveGroups = 0, numNonGroupedActiveVoices = 0;

	PrioritisedVoice *highestPriorityVoice = NULL;
	VoiceGroup *highestPriorityGroup = NULL;
 
	sysMemSet(&prioritisedGroupList[0], 0, sizeof(PrioritisedGroupList));	

#if !__SPU
	const u32 numBuckets = audSound::GetStaticPool().GetNumBuckets();
#else
	const u32 TAG = 7;
	const u32 numBuckets = g_NumBuckets;
#endif

	for(u32 bucketId = 0; bucketId < numBuckets; bucketId++)
	{
#if __SPU

		// grab data
		// Assert that we shouldn't be using the 'Large' DMA functions (ie data < 16kb)
		TrapGE(g_BucketSize, (16U*1024));
		sysDmaGet(g_Bucket, (uint64_t)((char*)g_FirstBucketEA + (bucketId*g_BucketSize)), g_BucketSize, TAG);
		sysDmaWaitTagStatusAll(1<<TAG);
		const audSoundPoolBucket *bucket = g_Bucket;
#else
		const audSoundPoolBucket *bucket = audSound::GetStaticPool().GetBucket(bucketId);
#endif

		for(u32 voice = 0; voice < g_audMaxVoicesPerBucket; voice++)
		{
			const audVoiceSettings *voiceSettings = (bucket->voiceAllocationState.IsSet(voice) ? &bucket->voiceSettings[voice] : NULL);

			if(voiceSettings && voiceSettings->PcmSourceId != -1)
			{
				PcmSourceState physVoiceState;

				if(audPcmSourceInterface::GetState(physVoiceState, voiceSettings->PcmSourceId))
				{
					PrioritisedVoice *pv = &voiceList[numActiveVoices++];

					pv->VoiceId = (((bucketId&0xff)<<8) | (voice&0xff));
					pv->PhysicalState = (audVoiceState)voiceSettings->State;
					pv->Cost = voiceSettings->CostEstimate;

					Assert(voiceSettings->PcmSourceId != -1);

					// 0 is the highest priority
					u32 score = voiceSettings->VirtualisationScore;
					const u32 groupId = voiceSettings->VirtualisationGroupId;

					if(physVoiceState.PlaytimeSamples == ~0U)
					{
						// don't give voices to generators that have finished
						score = ~0U;
					}
					else if(voiceSettings->Flags.ShouldPlayPhysically 
						|| (voiceSettings->State == AUD_VOICE_PLAYING_PHYSICALLY && voiceSettings->Flags.IsUncancellable))
					{
						score = 0;
						numActivePlayPhysicalVoices++;
					}
					else if(voiceSettings->State == AUD_VOICE_PLAYING_VIRTUALLY)
					{
						if(voiceSettings->Flags.ShouldStop)
						{
							// Don't give physical voices to voices that have been told to stop, and aren't yet physical
							score = ~0U;
						}
						else
						{
							// Starting voices physically has a cost, so factor that in
#if RSG_PS3
							enum { kPhysicalVoiceStartupCost = 30 };
#else
							enum { kPhysicalVoiceStartupCost = 5 };
#endif
							pv->Cost += kPhysicalVoiceStartupCost;
						}
					}
					else if(voiceSettings->State == AUD_VOICE_PLAYING_PHYSICALLY || voiceSettings->State == AUD_VOICE_STARTING_PHYSICALLY)
					{
						// equivalent of 1dB @ -60dB, just to help prevent flitting between two equal volume sources
						const u32 playingPhysicallyPrioBump = 0x72081;
						score -= Min(score, playingPhysicallyPrioBump);
					}

					pv->VirtualisationScore = score;

					if(groupId == 0)
					{
						numNonGroupedActiveVoices++;

						// add this voice to the head of the list
						if(!highestPriorityVoice)
						{
							highestPriorityVoice = pv;
							pv->next = NULL;
						}
						else
						{
							pv->next = highestPriorityVoice;
							highestPriorityVoice = pv;
						}
					}
					else
					{
						// this voice belongs in a group - search the group list
						VoiceGroup *groupPtr = NULL;

						for(u32 j =0;j<numActiveGroups;j++)
						{
							if(prioritisedGroupList[j].GroupId == groupId)
							{
								groupPtr = &prioritisedGroupList[j];
								break;
							}
						}

						if(groupPtr == NULL)
						{
							// stick this group at the end of the list
							Assert(numActiveGroups < g_MaxVirtualisationGroups);
							groupPtr = &prioritisedGroupList[numActiveGroups++];

							groupPtr->GroupId = groupId;
							groupPtr->TotalCost = 0;
						}

						if(!groupPtr->head)
						{
							groupPtr->head = pv;
							pv->next = NULL;
							groupPtr->HighestScore = pv->VirtualisationScore;
							groupPtr->TotalCost += pv->Cost;
						}
						else
						{
							pv->next = groupPtr->head;
							groupPtr->head = pv;
							if(groupPtr->HighestScore > pv->VirtualisationScore)
							{
								groupPtr->HighestScore = pv->VirtualisationScore;
								groupPtr->TotalCost += pv->Cost;
							}
						}

						groupPtr->numEntries++;
					}
				}
			} // if voice is allocated
		} // for each voice
	} // for each bucket

	// sort the group list
	// NOTE: this is using a super simple (and inefficient) sort - if groups end up
	// being used a lot then this should probably be rewritten using a radix sort
	for(u32 i = 0 ; i < numActiveGroups; i++)
	{
		if(highestPriorityGroup && prioritisedGroupList[i].HighestScore > highestPriorityGroup->HighestScore)
		{
			VoiceGroup *ptr = highestPriorityGroup;
			while(ptr->next && ptr->next->HighestScore < prioritisedGroupList[i].HighestScore)
			{
				ptr = ptr->next;
			}

			prioritisedGroupList[i].next = ptr->next;
			ptr->next = &prioritisedGroupList[i];
		}
		else
		{
			prioritisedGroupList[i].next = highestPriorityGroup;
			highestPriorityGroup = &prioritisedGroupList[i];
		}
	}
	

#if 0
	// DEBUG check to ensure group list is sorted correctly
	VoiceGroup *gp = highestPriorityGroup;
	u32 numPrioritisedGroups = 0, voiceCount = 0;
	while(gp)
	{
		// check that numEntries corresponds to the linked voices in this group list
		u32 numVoices = gp->numEntries;
		PrioritisedVoice *vp = gp->head;
		while(vp)
		{
			vp=vp->next;
			numVoices--;
		}
		Assert(numVoices == 0);

		voiceCount += gp->numEntries;

		if(gp->next)
		{
			Assert(gp->next->HighestScore >= gp->HighestScore);
		}
		gp = gp->next;
		numPrioritisedGroups++;
	}
	// ensure no group were dropped off during the sort
	Assert(numPrioritisedGroups == numActiveGroups);
	// and ensure that the sum of grouped voices is what we expect
	Assert(voiceCount == (numActiveVoices - numNonGroupedActiveVoices));
	// end of DEBUG check
#endif

	// now sort the full list
	PrioritisedVoice *ptr, *next;
	u32 binListIndex = 0;
	// The radix sort below works in a bitwise fashion, so if virtualisation score is changed from 32bits
	// the loop needs to be altered accordingly.
	CompileTimeAssert(sizeof(ptr->VirtualisationScore) == 4);
	for(u32 i = 0; i < 32; i += 8)
	{
		bins = &voiceBinsList[binListIndex][0];
		binListIndex = (binListIndex+1)&1;

		sysMemSet(bins, 0, sizeof(voiceBinsList[0]));

		ptr = highestPriorityVoice;

		while(ptr)
		{
			next = ptr->next;

			// TODO: rather than a variable shift right, shift VirtualisationScore right by 8 bits each pass.
			// Verify that each voice is accessed at this point exactly once per 'bit'
			// Add 'OriginalVirtualisationScore' to PrioritisedVoice for validation purposes.
#if 0
			const u32 binIndex = static_cast<u32>(ptr->VirtualisationScore & 0xFF);
			ptr->VirtualisationScore >>= 8;
#else
			const u32 binIndex = static_cast<u32>((ptr->VirtualisationScore >> i) & 0xFF);
#endif

			Assert(binIndex < 256);

			ptr->next = NULL;

			if(bins[binIndex].tail)
			{
				bins[binIndex].tail->next = ptr;
				bins[binIndex].tail = ptr;
			}
			else
			{
				bins[binIndex].head = bins[binIndex].tail = ptr;
			}

			ptr = next;
		}

		// construct the list by joining all populated bins together
		highestPriorityVoice = NULL;
		for(u32 j = 0; j < 256; j++)
		{
			if(bins[j].tail)
			{
				if(highestPriorityVoice == NULL)
				{
					highestPriorityVoice = bins[j].head;
				}

				for(u32 j2 = j+1; j2 < 256; j2++)
				{
					if(bins[j2].head)
					{
						bins[j].tail->next = bins[j2].head;

						// we want j to equal j2 when we get back to the top of t he loop so that we search
						// for the next bin to join into j2
						j = j2-1;
						break;
					}
				}
			}
		}
	}

#if 0
	// DEBUG check to ensure all voices are in the list and its sorted correctly (ascending order)
	PrioritisedVoice *p = highestPriorityVoice;
	u32 numPrioritisedVoices = 0;
	while(p)
	{
		if(p->next)
		{
			Assert(p->next->VirtualisationScore >= p->VirtualisationScore);
		}
		p = p->next;
		numPrioritisedVoices++;
	}
	Assert(numPrioritisedVoices == numNonGroupedActiveVoices);
	// end of DEBUG check
#endif

	header->numActiveNonGroupedVoices = static_cast<u16>(numNonGroupedActiveVoices);
	header->numActivePlayPhysicalVoices = static_cast<u16>(numActivePlayPhysicalVoices);
	header->numActiveVirtualGroups = static_cast<u16>(numActiveGroups);
	header->numActiveVirtualVoices = static_cast<u16>(numActiveVoices);
#if __DEV
	g_MaxActiveVirtualVoices = Max(g_MaxActiveVirtualVoices, numActiveVoices);
	g_MaxActiveVoiceGroups = Max(g_MaxActiveVoiceGroups, numActiveGroups);
#endif

	voiceList = highestPriorityVoice;
	groupList = highestPriorityGroup;
}

#if !__FINAL && !__SPU
extern bool g_audIsCapturingVirtualVoices;
#endif

void WriteCapturePacket(const PrioritisedVoice *voice, const VoiceGroup *group)
{
#if !__FINAL && !__SPU
	if(g_audIsCapturingVirtualVoices)
	{
		audVoiceCaptureVirtualVoice packet;
		if(group)
		{
			Assign(packet.groupId, group->GroupId);
		}
		else
		{
			packet.groupId = 0;
		}

		packet.bucketId = (u8) ((voice->VoiceId>>8)&0xff);
		packet.voiceId = (u8)(voice->VoiceId&0xff);
		Assign(packet.virtualisationScore, voice->VirtualisationScore);
		const audVoiceSettings *voiceSettings = audSound::GetStaticPool().GetVoiceSettings(packet.bucketId, packet.voiceId);
		
		packet.hpfCutoff = voiceSettings->HPFCutoff;
		packet.lpfCutoff = voiceSettings->LPFCutoff;
		packet.state = voiceSettings->State;
		packet.costEstimate = voiceSettings->CostEstimate;
		Assign(packet.pcmSourceChannelId, (u32)voiceSettings->PcmSourceChannelId);
		PcmSourceState physState;
		if(audPcmSourceInterface::GetState(physState, voiceSettings->PcmSourceId) && physState.hasStartedPlayback)
		{
			packet.playtime = physState.PlaytimeSamples;	
			packet.peakLevel = physState.CurrentPeakLevel;

			const audPcmSource *pcmSource = audDriver::GetMixer()->GetPcmSource(voiceSettings->PcmSourceId);
			if(pcmSource)
			{
				Assign(packet.pcmSourceType, (u32)pcmSource->GetTypeId());
				if(pcmSource->HasStartedPlayback() && !pcmSource->IsFinished())
				{
					audPcmSource::AssetInfo assetInfo;
					pcmSource->GetAssetInfo(assetInfo);
					Assign(packet.slotId, assetInfo.slotId);
					packet.assetNameHash = assetInfo.assetNameHash;
				}
			}
		}	
		
		audDriver::GetVoiceManager().WriteVoiceCapturePacket(packet);
	}
#else
	(void)voice;
	(void)group;
#endif
}

void AllocatePhysicalVoices(const u32 budget, VoiceGroup *highestPriorityGroup, PrioritisedVoice *highestPriorityVoice, u32 actualPhysicalVoicesFree, u32 physicalVoicesThisFrame, audVirtualisationJobOutput *header)
{
	PrioritisedVoice *voicePtr = highestPriorityVoice;
	VoiceGroup *groupPtr = highestPriorityGroup;
	header->voicesStartedThisFrame = 0;
	header->voicesStoppedThisFrame = 0;
	header->physVoiceCost = 0;

	s32 budgetRemaining = static_cast<s32>(budget);
	
	// we now walk through both the voice and group lists and play what we can
	while(voicePtr || groupPtr)
	{
		if(physicalVoicesThisFrame > 0 && budgetRemaining >= 0)
		{
			// we have physical voices left so allocate

			if(voicePtr && groupPtr)
			{
				// we have grouped and non grouped voices, see which is most important

				if(voicePtr->VirtualisationScore < groupPtr->HighestScore)
				{
					// this voice is more important than the current group
					if(voicePtr->PhysicalState == AUD_VOICE_PLAYING_VIRTUALLY)
					{
						if(actualPhysicalVoicesFree > 0 && budgetRemaining >= voicePtr->Cost)
						{
							StartPlayingVoicePhysically(voicePtr->VoiceId, header);
							actualPhysicalVoicesFree--;
							budgetRemaining -= voicePtr->Cost;
							header->physVoiceCost += voicePtr->Cost;
						}
					}
					else
					{
						// already playing physically
						budgetRemaining -= voicePtr->Cost;
						header->physVoiceCost += voicePtr->Cost;
					}					
					
					physicalVoicesThisFrame--;

					WriteCapturePacket(voicePtr, NULL);
					voicePtr = voicePtr->next;
				}
				else
				{
					// this group is more important than the current voice, see if we
					// can play it
					if(physicalVoicesThisFrame >= groupPtr->numEntries && budgetRemaining >= groupPtr->TotalCost)
					{
						// we can, do so
						PrioritisedVoice *gv = groupPtr->head;
						u32 voicesInGroup = groupPtr->numEntries;
						while(gv)
						{
							if(gv->PhysicalState == AUD_VOICE_PLAYING_VIRTUALLY)
							{
								if(actualPhysicalVoicesFree >= voicesInGroup && budgetRemaining >= groupPtr->TotalCost)
								{
									StartPlayingVoicePhysically(gv->VoiceId, header);
									actualPhysicalVoicesFree--;
									voicesInGroup--;
									budgetRemaining -= gv->Cost;
									header->physVoiceCost += gv->Cost;
								}
								else
								{
									// don't play any voices, skip the group
									break;
								}
							}
							else
							{
								// already playing physically
								budgetRemaining -= gv->Cost;
								header->physVoiceCost += gv->Cost;
							}
												
							physicalVoicesThisFrame--;
							WriteCapturePacket(gv, groupPtr);

							gv = gv->next;
						}
					}

					groupPtr = groupPtr->next;
				}
			}
			else if(voicePtr)
			{
				// we have no grouped voices left
				if(voicePtr->PhysicalState == AUD_VOICE_PLAYING_VIRTUALLY)
				{
					if(actualPhysicalVoicesFree > 0 && budgetRemaining >= voicePtr->Cost)
					{
						StartPlayingVoicePhysically(voicePtr->VoiceId, header);
						actualPhysicalVoicesFree--;
						budgetRemaining -= voicePtr->Cost;
						header->physVoiceCost += voicePtr->Cost;
					}
				}
				else
				{
					budgetRemaining -= voicePtr->Cost;
					header->physVoiceCost += voicePtr->Cost;
				}
				
				physicalVoicesThisFrame--;				
				WriteCapturePacket(voicePtr, NULL);

				voicePtr = voicePtr->next;
			}
			else
			{
				// we just have grouped voices left

				// see if we can play it
				if(physicalVoicesThisFrame >= groupPtr->numEntries)
				{
					// we can, do so
					PrioritisedVoice *gv = groupPtr->head;
					u32 voicesInGroup = groupPtr->numEntries;
					s32 totalCost = groupPtr->TotalCost;
					while(gv)
					{
						if(gv->PhysicalState == AUD_VOICE_PLAYING_VIRTUALLY)
						{
							if(actualPhysicalVoicesFree >= voicesInGroup && budgetRemaining >= totalCost)
							{
								StartPlayingVoicePhysically(gv->VoiceId, header);
								actualPhysicalVoicesFree--;
								voicesInGroup--;
								budgetRemaining -= gv->Cost;
								header->physVoiceCost += gv->Cost;
							}
							else
							{
								// skip this group - don't have enough actual physicals
								break;
							}
						}
						else
						{
							budgetRemaining -= gv->Cost;
							header->physVoiceCost += gv->Cost;
						}
						
						physicalVoicesThisFrame--;

						WriteCapturePacket(gv, groupPtr);

						gv = gv->next;
					}
				}
				groupPtr = groupPtr->next;
			}
		}
		else
		{
			// we're out of physical voices so everything below this line should
			// not be playing physically

			// tell this voice to stop playing physically
			if(voicePtr)
			{
				if(voicePtr->PhysicalState == AUD_VOICE_STARTING_PHYSICALLY || voicePtr->PhysicalState == AUD_VOICE_PLAYING_PHYSICALLY)
				{
					StopPlayingVoicePhysically(voicePtr->VoiceId, header);
				}

				/*audVoiceSettings *settings = audDriver::GetVoiceManager().GetVoiceSettings(voicePtr->VoiceId);
				audPcmSource *pcmSource = audDriver::GetMixer()->GetPcmSource(settings->PcmSourceId);
				if(pcmSource->GetTypeId() == AUD_PCMSOURCE_WAVEPLAYER)
				{
					audWavePlayer *wavePlayer =(audWavePlayer*)pcmSource;
					Assert(wavePlayer->GetWaveNameHash() != g_DontVirtualiseWaveNameHash);
				}*/

				WriteCapturePacket(voicePtr, NULL);
				voicePtr = voicePtr->next;
			}

			// tell this entire group to stop playing physically
			if(groupPtr)
			{
				PrioritisedVoice *gv = groupPtr->head;
				while(gv)
				{
					if(gv->PhysicalState == AUD_VOICE_STARTING_PHYSICALLY || gv->PhysicalState == AUD_VOICE_PLAYING_PHYSICALLY)
					{
						StopPlayingVoicePhysically(gv->VoiceId, header);
					}
					WriteCapturePacket(gv, groupPtr);
					gv = gv->next;
				}

				groupPtr = groupPtr->next;
			}
		}
	}
}

}// namespace rage
