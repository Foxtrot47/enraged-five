//
// audiohardware/voicemgr.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_VOICE_MGR_H
#define AUD_VOICE_MGR_H

#include "audiosoundtypes/sound.h"
#include "atl/array.h"
#include "mixer.h"
#include "pcmsource_interface.h"
#include "voicesettings.h"
#include "wavedefs.h"
#include "system/criticalsection.h"

namespace rage {

class audWaveSlot;
class audEffect;
class bkBank;
class audMixerVoice;

class audVoiceMgr
{
public:
	//
	// PURPOSE
	//	Allocates physical voices to virtual voices on the basis of physical volume (post-ducking.)
	// PARAMS
	//	timeInMs - The current game time, in milliseconds.
	//
	void Update(u32 timeInMs);
	//
	// PURPOSE
	//  Dynamically change the number of physical voices to use.
	// PARAMS
	//	numVoices - The number of physical voice to use.
	//
	void SetNumberOfPhysicalVoices(u32 numVoices);
	//
	// PURPOSE
	//  Get the (dynamic) number of physical voices available for use this frame.
	// RETURNS
	//	The (dynamic) number of physical voices available for use this frame.
	//
	u32 GetNumberOfPhysicalVoicesAvailable() const { return m_PhysicalVoicesThisFrame; }
	//
	// PURPOSE
	//  Gets the (static) number of physical voices available.
	// RETURNS
	//  The (static) number of physical voices available.
	//
	u32 GetMaximumNumberOfPhysicalVoices() const { return m_MaxPhysicalVoices; }
	//
	// PURPOSE
	//  Gets the (static) number of voices to keep back for new physically playing voices, while old ones die.
	// RETURNS
	//  The (static) number of voices to keep back for new physically playing voices, while old ones die.
	//
	u32 GetNumberOfNewPhysicalVoicesBuffer() const { return m_NewPhysicalVoiceBuffer; }
	//
	// PURPOSE
	//	Returns the number of physical voices currently active.
	// RETURNS
	//	The number of physical voices currently active.
	//
	u32 GetNumberOfActivePhysicalVoices() const { return m_NumCurrentlyActivePhysicalVoices; }
	
	//
	// PURPOSE
	//	Returns true if the user is currently playing his/her own music, currently only supported on XENON and PS3
	// RETURNS
	//	true if the user is currently playing his/her own music, currently only supported on XENON and PS3
	//
	bool IsUserMusicPlaying() const { return m_IsUserMusicPlaying; }

	// PURPOSE
	//	Returns true if user music is currently overridden via OverrideUserMusic
	bool IsUserMusicOverridden() const { return m_OverrideUserMusicState == kUserMusicOverridden; }
	

	//
	// PURPOSE
	//	Add a request for the user's current background music to temporarily halt.
	//	Currently implemented on XENON and PS3.  Only intended to be used when absolutely necessary, eg. cutscene with music
	//
	void RequestUserMusicOverride();
	// PURPOSE
	//	Add a request to restore the user's background music after a call to RequestUserMusicOverride()
	//	Currently implemented on XENON and PS3.
	//
	void RequestUserMusicRestore();


	//
	// PURPOSE
	//	Initializes the virtual voice pool and voice management buffers.
	//
	void Init();
	//
	// PURPOSE
	//	Shuts down the virtual voice pool and voice management buffers.
	//
	void Shutdown();

	__inline audVoiceSettings* GetVoiceSettings(const u32 packedVoiceId)
	{
		return GetVoiceSettings((packedVoiceId>>8)&0xff, packedVoiceId&0xff);
	}

	__inline audVoiceSettings* GetVoiceSettings(const u32 bucketId, const u32 voiceId)
	{
		return audSound::GetStaticPool().GetVoiceSettings(bucketId, voiceId);
	}


	audVoiceMgr();
	~audVoiceMgr();

	void DrawDebug();
	void PrintPhysicalVoices();

	PcmSourceState *GetPcmSourceState(const s32 pcmSourceId) { return pcmSourceId == -1 ? NULL : &m_PcmSourceState[pcmSourceId]; }

#if __BANK
	void AddWidgets(bkBank &bank);
#endif

#if !__FINAL
	void StartVoiceCapture() const;
	void StopVoiceCapture() const;
	void CaptureFrameStart() const;
	void CaptureFrameEnd() const;
	void WriteVoiceCapturePacket(const audVoiceCapturePacket &packet) const;
	void DumpVoiceCaptureToFile() const;
#endif
	
	//
	// PURPOSE
	//	Updates m_IsUserMusicPlaying to be true whenever the user is playing his/her own music.
	//	Currently can will be true for XENON and PS3
	//
	void UpdateUserMusicState();

#if RSG_PC
	// PURPOSE
	//	If true, audio quality will be degraded to save CPU
	void SetCPULimitedAudio(const bool isMinSpec);
#endif // RSG_PC

private:
	audMixerVoice *AllocatePhysicalVoice(audVoiceSettings *voiceSettings);
	void FreePhysicalVoice(audMixerVoice *voice);
	//
	// PURPOSE
	//	Updates the physical Voice volumes and calls SynchPlayback on all active voices.
	// PARAMS
	//	timeInMs - The current game time, in milliseconds.
	//
	void UpdatePhysicalVoices(u32 timeInMs);


	void SetPhysicalVoiceLimit();

	//
	// PURPOSE
	//	Starts playing the specified virtual voice id on a physical voice
	//
	void StartPlayingVoicePhysically(const u32 packedVirtualVoiceId);

	//
	// PURPOSE
	//	Stops playing the specified virtual voice id on hardware
	//
	void StopPlayingVoicePhysically(const u32 packedVirtualVoiceId);

	//
	// PURPOSE
	//	Forces the user's current background music to temporarily halt until RestoreBackgroundMusic() is called.
	//	Currently implemented on XENON and PS3.  Only intended to be used when absolutely necessary, eg. cutscene with music
	//
	void OverrideUserMusic();
	//
	// PURPOSE
	//	Restores the user's background music after a call to OverrideBackgroundMusic()
	//	Currently implemented on XENON and PS3.
	//
	void RestoreUserMusic();

	PrioritisedVoice *m_PrioritisedVoiceList;
	// PrioritisedVoice *m_HighestPriorityVoice;

	// VoiceGroup *m_HighestPriorityGroup;

	u32 m_PhysicalVoicesThisFrame;
	u32 m_NumCurrentlyActivePhysicalVoices;
	u32 m_PhysicalVoiceBudget;

	u32 m_UserMusicOverrideCount;

	s32 m_MaxVirtualVoices;
	s32 m_MaxPhysicalVoices, m_NewPhysicalVoiceBuffer;

	enum audOverrideUserMusicState
	{
		kUserMusicRestored,
		kUserMusicShouldRestore,
		kUserMusicOverridden,
		kUserMusicShouldOverride,
	};

	atRangeArray<PcmSourceState, kMaxPcmSources> m_PcmSourceState ;

	audVirtualisationJobInput m_VirtualisationJobInput ;
	audVirtualisationJobOutput m_VirtualisationJobOutput ;

	atRangeArray<audVoiceSettings*, kMaxVoices> m_PhysicalVoiceList;

	audOverrideUserMusicState m_OverrideUserMusicState;

	bool m_IsUserMusicPlaying;


	WIN32PC_ONLY(bool m_IsCPULimited);


#if __BANK
	sysCriticalSectionToken m_PhysVoiceDebugDrawLock;
#endif
};

} // namespace rage


#endif // AUD_VOICE_MGR_H
