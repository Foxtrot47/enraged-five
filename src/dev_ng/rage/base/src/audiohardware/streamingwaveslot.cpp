//
// audiohardware/streamingwaveslot.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "streamingwaveslot.h"

#include "debug.h"
#include "driver.h"
#include "driverutil.h"
#include "waveref.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audioengine/engineutil.h"
#include "audioengine/widgets.h"
#include "string/stringhash.h"

#include "diag/output.h"
#include "grcore/wrapper_gcm.h"

#if !__FINAL && !__SPU
#define AUDSTR_DEBUG_MARKER(fmt, ...) do { SetDebugMarker(fmt, ##__VA_ARGS__); } while (false)
#else
#define AUDSTR_DEBUG_MARKER(fmt, ...)
#endif

namespace rage {

const int audWaveSlot::AUD_WAVE_SLOT_SIZE = sizeof(audStreamingWaveSlot);

#if !__FINAL && !__SPU
const audStreamingDebugInterface *audStreamingWaveSlot::sm_DebugInterface = NULL;

void audStreamingWaveSlot::SetDebugMarker(const char *fmt, ...)
{
	if(sm_DebugInterface)
	{
		char msg[128];
		va_list args;
		va_start(args, fmt);
		vformatf(msg, fmt, args);
		va_end(args);
	
		sm_DebugInterface->SetMarker(msg);
	}
}
#endif

#if !__SPU
extern bool g_UseCriticalQueueForAudio;
audStreamingWaveSlot::audStreamingWaveSlot(u8 *bankMem, const u32 numBytes, const u32 maxHeaderSize, const char *slotName, const bool isVirtual) :
	audWaveSlot(bankMem, numBytes, maxHeaderSize, slotName, isVirtual)
{
	m_StartOffsetMs = -1;
	m_BlockIndexToLoad = -1;
	m_NumLoadBlocksToRead = 0;
	m_NumLoadBlocksReady = 0;
	m_NextStreamingBlockLoadSlotIndex = 0;
	m_NextStreamingBlockPrepareSlotIndex = 0;
	m_StreamHeader = NULL;
	m_IsLooping = false;
	m_FirstBlockRequest = false;
	m_StartBlockIndex = -1;

	m_State = rage_new audStreamingWaveSlotState();
	audAssert(m_State);
	
	for(s32 i = 0; i < kNumStreamingBlocksPerWaveSlot; i++)
	{
		m_StreamingBlockIndexSlots[i] = -1;
	}

	m_IsStreaming = true;
	m_SlotType = kSlotTypeStream;

	// streams need to go through critical streamer queue due to their realtime requirement
	m_UseCriticalQueue = true;
}

audStreamingWaveSlot::~audStreamingWaveSlot()
{
	audAssert(m_State);
	delete m_State;
	m_State = NULL;
}

void audStreamingWaveSlot::Init()
{
	// streaming slot memory layout:
	// header data (maxHeaderSize bytes)

	audWaveSlotState *const internalState = GetInternalState();
	audStreamingWaveSlotState *const state = GetState();

	audAssert(internalState->MainDataSize >= internalState->MaxHeaderSize);
#if AUD_STREAMINGBUFFERS_IN_VRAM
	u32 streamingBlockBytesX2 = internalState->VramAllocationSize;
#else
	u32 streamingBlockBytesX2 = internalState->MainDataSize - GetInternalState()->MaxHeaderSize;
#endif
	audAssert(streamingBlockBytesX2 % 2 == 0);
	state->StreamingBlockBytes = streamingBlockBytesX2 >> 1;

	// set up memory map
	// firstly there is the streaming bank header, which must be padded to a 2k multiple
	audAssert((internalState->MaxHeaderSize&2047) == 0);

	// Initialise with valid block pointers
	SetupBlockPointers(state->StreamingBlockBytes);

	CompileTimeAssert(kNumStreamingBlocksPerWaveSlot==2);
}

void audStreamingWaveSlot::SetupBlockPointers(const u32 streamingBlockBytes)
{
	audWaveSlotState *const internalState = GetInternalState();
	audStreamingWaveSlotState *const state = GetState();

	const u8 *ptr = internalState->BankData + internalState->MaxHeaderSize;
	// extra safety check to ensure we're not going to overwrite anything
	audAssert(internalState->BankData + internalState->SlotBytes >= ptr);
	
	state->BlockData[0] = ptr;
	ptr += streamingBlockBytes;
	state->BlockData[1] = ptr;
	ptr += streamingBlockBytes;
}

bool audStreamingWaveSlot::ForceUnloadBank()
{
	if(audWaveSlot::ForceUnloadBank())
	{
		m_StreamHeader = nullptr;
		m_LoadBlockSeekTable = nullptr;
		return true;
	}

	return false;
}

void audStreamingWaveSlot::Update(void)
{
	// check to see if we need to load the next block
	u32 numActiveClients = 0;
	u32 numClientsWantingData = 0;
	for(s32 i = 0; i < kMaxStreamClients; i++)
	{
		if(GetState()->ClientState[i].state >= audStreamClientState::kAllocated)
		{
			numActiveClients++;
			if(GetState()->ClientState[i].state == audStreamClientState::kNeedsData)
			{
				numClientsWantingData++;
			}
		}
	}
	
	if(numActiveClients > 0 && numClientsWantingData == numActiveClients)
	{
		DEBUG_STREAMING_ONLY(audDisplayf("Frame %u: Slot %s: All %u clients want data", audDriver::GetFrameCount(), GetSlotName(), GetState()->NumClients));
	
		// Shortcut: if this is the first time clients have requested data, clear the preload blocks,
		// since they have already been consumed (just not via RequestStreamingLoadBlock()).
		if(m_FirstBlockRequest)
		{
			// If we have all blocks loaded and we're looping, don't clear them.
			if(!m_IsLooping || m_StreamHeader->header.NumLoadBlocks > 2)
			{
				m_NumLoadBlocksReady = 0;
			}
			m_FirstBlockRequest = false;
		}

		// Request load
		switch(RequestStreamingLoadBlock(m_LoadedBankId))
		{
		case audStreamingWaveSlot::LOADED_BLOCK:
			DEBUG_STREAMING_ONLY(audDisplayf("Frame %u: Slot %s: Loaded block", audDriver::GetFrameCount(), GetSlotName()));
			//Inform all child sounds, on their next call to RequestSampleData(), that the data is now available.
			for(s32 i = 0; i < kMaxStreamClients; i++)
			{
				if(GetState()->ClientState[i].state == audStreamClientState::kNeedsData)
				{
					GetState()->ClientState[i].state = audStreamClientState::kDataAvailable;
				}
			}
			break;

		case audStreamingWaveSlot::LOADING_BLOCK:
			//Wait for block to finish loading.
			break;

		case audStreamingWaveSlot::NO_DATA_FOR_BLOCK:
			//Inform clients that the stream is finishing
			for(s32 i = 0; i < kMaxStreamClients; i++)
			{
				if(GetState()->ClientState[i].state == audStreamClientState::kNeedsData)
				{
					GetState()->ClientState[i].state = audStreamClientState::kOutOfData;
				}
			}
			break;

		case audStreamingWaveSlot::FAILED_TO_LOAD_BLOCK:
			//audAssertf(0,"Failed to load a streaming block from Wave Bank");
			break;

		}	
	}

	if(m_IsLoading)
	{
		//Check to see if loading is complete.
		bool isLoadFinished = AreAnyDecryptionTasksPending() || sysIpcPollSema(GetInternalState()->BankLoadSema);
		
#if RSG_BANK
		if(sm_DelayRequests && isLoadFinished)
		{
			audAssert(!m_RequestDelayTimerSet);
			m_RequestDelayTimerSet = true;
			m_RequestDelayTimer = audEngineUtil::GetCurrentTimeInMilliseconds();
		}

		if(m_RequestDelayTimerSet)
		{
			if(audEngineUtil::GetCurrentTimeInMilliseconds() < m_RequestDelayTimer + sm_StreamingRequestDelayTime)
			{
				isLoadFinished = false;
			}
			else
			{
				m_RequestDelayTimerSet = false;
				isLoadFinished = true;
			}
		}
#endif	

		//Load complete.
		if(isLoadFinished)
		{
			HandleLoadCompletion();
		}
	
	}

	//Intentional fall through allowing another load to be requested immediately that a load completes.
	if(!m_IsLoading && m_IsLoadRequestQueued)
	{
		RequestLoad();
	}
}

bool audStreamingWaveSlot::ResetLoadedState()
{
	if(m_LoadedBankId != AUD_INVALID_BANK_ID || m_LoadFailedBankId != AUD_INVALID_BANK_ID)
	{
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
		if((GetReferenceCount() > 0))
		{
			// Can't reset with active clients
			return false;
		}
		m_LoadedBankId = AUD_INVALID_BANK_ID;
		m_LoadFailedBankId = AUD_INVALID_BANK_ID;
	}	
	return true;
}

void audStreamingWaveSlot::RequestLoad(void)
{
	bool loadFailed = false;
	char fullBankPath[255] = {0};
	
	//**Start of Bank Load Critical Section**//
	{
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

		m_LoadFailedBankId = AUD_INVALID_BANK_ID;

		// m_NumLoadBlocksToRead is only equal to kNumStreamingBlocksPerWaveSlot during the preload, so
		// this check ensures that preload requests are not actioned while there are references to the slot.
		// Once actually streaming there will obviously be references.
		if((GetReferenceCount() > 0) && (m_NumLoadBlocksToRead == kNumStreamingBlocksPerWaveSlot))
		{
			//audWarningf("Attempted to load sample data while %u clients were still playing from the slot\n",
			//	m_ReferenceCount);
			return;
		}

		const char *bankName = GetBankName(m_BankIdToLoad);
		audAssert(bankName);
		if(bankName == NULL)
		{
			audErrorf("Invalid bank ID in audStreamingWaveSlot::RequestLoad (%u)", m_BankIdToLoad);
			m_LoadedBankId = m_BankIdToLoad;
			CleanUpAfterLoadFailure();
			return;
		}

		audWaveSlot::ComputeBankFilePath(fullBankPath, bankName);
		
		u16 bankIdToLoad = m_BankIdToLoad;

		m_BankIdToLoad = AUD_INVALID_BANK_ID;
		m_IsLoadRequestQueued = false;
		m_LoadRequestTimeMs = (u32)-1; //We have actioned this load request, so clear the load request time.
		m_IsLoading = true;
		m_LoadedBankId = bankIdToLoad;		//Only valid when m_IsLoading is false.

		if(m_NumLoadBlocksToRead == kNumStreamingBlocksPerWaveSlot)
		{
			audAssert(!m_StreamHeader);
			audAssert(!m_LoadBlockSeekTable);
		}

	}//***End of Bank Load Critical Section***//

	//Issue queued load request to streamer.
	u32 bankSizeBytes;
	if((GetInternalState()->BankHandle = pgStreamer::Open(fullBankPath, &bankSizeBytes, false)) != pgStreamer::Error)
	{
		u32 loadSizeBytes;
		u32 offsetBytes = 0;
		s32 blockIndex = -1;
		void *loadDestination = GetInternalState()->BankData;
		if(m_StreamHeader == NULL)
		{
			//Read streaming Wave Bank header.
			loadSizeBytes = GetInternalState()->MaxHeaderSize;
		}
		else
		{
			//Load 1 to kNumStreamingBlocksPerWaveSlot blocks of stream data.
			
			// m_StartOffsetMs is only non-zero when preloading
			if(m_StartOffsetMs >= 0)
			{
				if(m_IsLooping && m_StreamHeader->header.NumLoadBlocks <= 2)
				{
					// Load the entire stream if we're looping a short stream
					blockIndex = 0;
				}
				else
				{
					blockIndex = ComputeStreamingBankBlockIndexFromTimeOffset(m_StartOffsetMs);
				}				
			}
			else
			{
				blockIndex = m_BlockIndexToLoad;
			}

			if(blockIndex >= 0)
			{
				//Check that the requested number of blocks are available from the requested start offset.				
				Assign(m_NumLoadBlocksToRead, Min<u32>(m_NumLoadBlocksToRead, m_StreamHeader->header.NumLoadBlocks - blockIndex));

				loadSizeBytes = m_NumLoadBlocksToRead * m_StreamHeader->header.StreamingBlockBytes;
				offsetBytes = GetState()->DataChunkContainerOffsetBytes +	(blockIndex * m_StreamHeader->header.StreamingBlockBytes);
				loadDestination = const_cast<u8*>(GetState()->BlockData[m_NextStreamingBlockLoadSlotIndex]);

				// Don't attempt to read past EOF
				loadSizeBytes = Min(loadSizeBytes, bankSizeBytes - offsetBytes);
			}
			else
			{
				if(m_StartOffsetMs >= 0)
				{
					audErrorf("Requested start offset (%d) is beyond end of Wave data for streaming Wave Bank (%s)\n",
						m_StartOffsetMs, (GetBankName(m_LoadedBankId) ? GetBankName(m_LoadedBankId) : "Invalid"));
				}
				else
				{
					audErrorf("Requested block index (%d) is beyond end of Wave data for streaming Wave Bank (%s)\n",
						m_BlockIndexToLoad, (GetBankName(m_LoadedBankId) ? GetBankName(m_LoadedBankId) : "Invalid"));
				}

				pgStreamer::Close(GetInternalState()->BankHandle);
				loadSizeBytes = 0;
				loadFailed = true;
			}
		}

		if(!loadFailed)
		{
			// check for possibility that the read size exceeds the EOF of the streaming source
			const u32 fileSize = pgStreamer::GetSize(GetInternalState()->BankHandle);
			if (!Verifyf(loadSizeBytes + offsetBytes <= fileSize, "Attempting streaming audio read beyond EOF"))
			{
				loadSizeBytes = fileSize - offsetBytes;
			}

			datResourceChunk chunk;
			chunk.DestAddr = loadDestination;
			chunk.Size = GetState()->LastLoadBytes = loadSizeBytes;

			DEBUG_STREAMING_ONLY(audDisplayf("Submitting stream block load request %s, %s block: %u (dest: %u)", GetSlotName(), GetLoadedBankName(), blockIndex, m_NextStreamingBlockLoadSlotIndex);)

			GetState()->ReadRequestTime = static_cast<float>(audDriver::GetMixer()->GetMixerTimeS());

			audAssert(!AUD_STREAMINGBUFFERS_IN_VRAM || !m_StreamHeader || gcm::IsLocalPtr(chunk.DestAddr));
			const u32 flags = m_UseCriticalQueue && g_UseCriticalQueueForAudio ? (pgStreamer::UNCACHED | pgStreamer::CRITICAL) : pgStreamer::UNCACHED;
			
			if(!pgStreamer::Read(GetInternalState()->BankHandle, &chunk, 1, offsetBytes, GetInternalState()->BankLoadSema, flags))
			{
				pgStreamer::Close(GetInternalState()->BankHandle);
				loadFailed = true;
			}
		}
	}
	else
	{
		loadFailed = true;
	}

	if(loadFailed)
	{
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

		if(m_LoadedWaveNameHash)
		{
			audErrorf("Error loading Wave #%X from Bank %s", m_LoadedWaveNameHash, fullBankPath);
		}
		else
		{
			audErrorf("Error loading Wave Bank %s", fullBankPath);
		}

		CleanUpAfterLoadFailure();
	}
}

void audStreamingWaveSlot::HandleLoadCompletion(void)
{
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
	
	if(!m_StreamHeader)
	{
		//The streaming Bank header has been loaded, so now load the requested Wave sample data,
		//if it's not already been loaded.
		HandleHeaderLoadCompletion();
	}
	else // if(m_HasLoadedStreamingBankHeader)
	{
		if(AreAnyDecryptionTasksPending())
		{
			bool done = true;
			for(s32 i = 0; i < m_DecryptionTaskHandles.GetCount(); i++)
			{
				if(sysTaskManager::Poll(m_DecryptionTaskHandles[i]))
				{
					m_DecryptionTaskHandles.Delete(i);
					i--;
				}
				else
				{
					done = false;
				}
			}
			if(!done)
			{
				return;
			}
		}
		else AUD_SUPPORT_UNENCRYPTED_ASSETS_ONLY(if(GetInternalState()->Container.IsDataEncrypted()))
		{
			// decrypt the load block(s)
			size_t dataLeft = GetState()->LastLoadBytes;
			for(u32 i=0; i<m_NumLoadBlocksToRead; i++)
			{
				sysTaskParameters p;

				p.ReadOnlyCount = 1;
				p.ReadOnly[0].Data = const_cast<u32*>(GetKey(m_LoadedBankId));
				p.ReadOnly[0].Size = 4*sizeof(u32);
				p.Input.Data = p.Output.Data = (u32*)GetState()->BlockData[(m_NextStreamingBlockLoadSlotIndex + i) % kNumStreamingBlocksPerWaveSlot];
				// Decrypt at most one load block at a time
				p.Input.Size = p.Output.Size = Min<size_t>(m_StreamHeader->header.StreamingBlockBytes, dataLeft);
				// Keep track of the actual data to decrypt; so that we have the correct size in the case of a short read on the first two blocks.
				dataLeft -= p.Input.Size;
				m_DecryptionTaskHandles.Append() = sysTaskManager::Create(TASK_INTERFACE(bteatask), p, audWaveSlot::sm_iSchedulerIndex);
			}
			return;
		}

		GetState()->TimeTakenToFulfilRequest = static_cast<float>(audDriver::GetMixer()->GetMixerTimeS() - GetState()->ReadRequestTime);
		
		AUDSTR_DEBUG_MARKER("HandleLoadCompletion: %s:%s - %d", GetSlotName(), GetBankName(m_LoadedBankId), m_BlockIndexToLoad);
		//The streaming Bank header and block(s) has been loaded:
		// m_StartOffsetMs is only >=0 when preloading
		if(m_StartOffsetMs >= 0)
		{
			m_StreamingBlockIndexSlots[m_NextStreamingBlockLoadSlotIndex] =	ComputeStreamingBankBlockIndexFromTimeOffset(m_StartOffsetMs);
		}
		else
		{
			m_StreamingBlockIndexSlots[m_NextStreamingBlockLoadSlotIndex] = m_BlockIndexToLoad;
		}

		DEBUG_STREAMING_ONLY(audDisplayf("Slot %s HandleLoadCompletion: loaded block %u in slot %u (startOffsetMs: %d m_BlockIndexToLoad: %d", GetSlotName(), m_StreamingBlockIndexSlots[m_NextStreamingBlockLoadSlotIndex], m_NextStreamingBlockLoadSlotIndex, m_StartOffsetMs, m_BlockIndexToLoad);)

		for(u32 i=1; i<m_NumLoadBlocksToRead; i++)
		{
			m_StreamingBlockIndexSlots[(m_NextStreamingBlockLoadSlotIndex + i) % kNumStreamingBlocksPerWaveSlot] = 
				m_StreamingBlockIndexSlots[m_NextStreamingBlockLoadSlotIndex] + i;
		}

		DEBUG_STREAMING_ONLY(audDisplayf("Slot %s now contains loaded blocks: %u, %u", GetSlotName(), m_StreamingBlockIndexSlots[0], m_StreamingBlockIndexSlots[1]);)

		m_NextStreamingBlockLoadSlotIndex = (m_NextStreamingBlockLoadSlotIndex+m_NumLoadBlocksToRead) % kNumStreamingBlocksPerWaveSlot;
		m_NumLoadBlocksReady = m_NumLoadBlocksToRead;
		
		m_IsLoading = false;
	}

	if(!m_IsLoading)
	{
		//We are done loading from this Bank.
		pgStreamer::Close(GetInternalState()->BankHandle);
	}
}

void audStreamingWaveSlot::HandleHeaderLoadCompletion()
{
	static const u32 streamFormatHash = 2180599880u;//("STREAMFORMAT");
	static const u32 seekTableHash = 35554979u;//("SEEKTABLE");
	static const u32 dataHash = 1588979285u;//("DATA");

	u32 typeHash,containerOffsetBytes,sizeBytes;

	audWaveSlotState *const internalState = GetInternalState();
	audStreamingWaveSlotState *const state = GetState();

	internalState->Container.SetHeader((const adatContainerHeader*)internalState->BankData);

	if(!audVerifyf(internalState->Container.IsValid() && internalState->Container.IsNativeEndian(), "Invalid header loaded when trying to load %s", GetBankName(m_LoadedBankId)))
	{
		CleanUpAfterLoadFailure();
		return;
	}

	audAssert(internalState->Container.IsValid());
	audAssert(internalState->Container.IsNativeEndian());

	internalState->ContainerHeaderSize = internalState->Container.GetHeaderSize();
	internalState->LoadedContainerOffset = internalState->ContainerHeaderSize;

	const adatObjectId globalObjectId = internalState->Container.FindObject("");
	audAssert(globalObjectId != adatContainer::InvalidId);
	audAssert(globalObjectId == (adatObjectId)0);

	const adatObjectDataChunkId streamFormatChunkId = internalState->Container.FindObjectData(globalObjectId, streamFormatHash);
	audAssert(streamFormatChunkId != adatContainer::InvalidId);

	internalState->Container.GetObjectDataChunk(globalObjectId, streamFormatChunkId, typeHash, containerOffsetBytes, sizeBytes);
	audAssert(typeHash == (streamFormatHash&adatContainerObjectDataTableEntry::kDataTypeHashMask));

	// This is only used for internal state checking, so set it to something fairly valid to get some value from the asserts
	internalState->LoadedBytes = internalState->MaxHeaderSize;

	m_StreamHeader = reinterpret_cast<const audStreamFormat*>(Resolve(containerOffsetBytes,sizeBytes));
	
	// streamformat and seektable chunk must fit within header space (dictated by maxheadersize)
	audAssert(containerOffsetBytes + sizeBytes <= internalState->MaxHeaderSize);
	
	audAssert(m_StreamHeader->header.NumChannels > 0);
	
	audAssertf(m_StreamHeader->header.StreamingBlockBytes <= state->StreamingBlockBytes, "Asset block size mismatch; %u slot block size: %u", m_StreamHeader->header.StreamingBlockBytes, state->StreamingBlockBytes);
#if __ASSERT
	// sanity check the per-channel header data
	for(u32 i = 0; i < m_StreamHeader->header.NumChannels; i++)
	{
		audAssert(m_StreamHeader->Channels[i].SampleRate >= 1000 && m_StreamHeader->Channels[i].SampleRate < 50000);
		// padding initialises to zero so this is another handy sanity check
		audAssert(m_StreamHeader->Channels[i].reserved0 == 0 || m_StreamHeader->Channels[i].reserved0 == 16);
	}
#endif

	if(m_StreamHeader->header.StreamingBlockBytes > state->StreamingBlockBytes)
	{
		// bail out to prevent decode errors
		OUTPUT_ONLY(const char *bankName = GetBankName(m_LoadedBankId));
		audErrorf("Error loading block from streaming Wave Bank %s - invalid streaming block size %u (slot block size %u)", bankName ? bankName : "Invalid", m_StreamHeader->header.StreamingBlockBytes, state->StreamingBlockBytes);
		CleanUpAfterLoadFailure();
		return;
	}

	// Recompute block pointers based on the asset.
	SetupBlockPointers(m_StreamHeader->header.StreamingBlockBytes);

	// This is only used for internal state checking, so set it to something fairly valid to get some value from the asserts
	internalState->LoadedBytes = m_StreamHeader->header.StreamingBlockBytes * 2U + internalState->ContainerHeaderSize;

	// pull out a pointer to the block seek table
	const adatObjectDataChunkId seekTableChunkId = internalState->Container.FindObjectData(globalObjectId, seekTableHash);
	audAssert(seekTableChunkId != adatContainer::InvalidId);

	internalState->Container.GetObjectDataChunk(globalObjectId, seekTableChunkId, typeHash, containerOffsetBytes, sizeBytes);
	audAssert(typeHash == (seekTableHash&adatContainerObjectDataTableEntry::kDataTypeHashMask));
	// streamformat and seektable chunk must fit within header space (dictated by maxheadersize)
	audAssert(containerOffsetBytes + sizeBytes <= internalState->MaxHeaderSize);
	audAssert(sizeBytes == sizeof(audSeekTableEntry) * m_StreamHeader->header.NumLoadBlocks);

	m_LoadBlockSeekTable = reinterpret_cast<const audSeekTableEntry *>(Resolve(containerOffsetBytes, sizeBytes));
	audAssert(m_LoadBlockSeekTable);

	// we also need to store the data chunk offset for future reads
	const adatObjectDataChunkId dataChunkId = internalState->Container.FindObjectData(globalObjectId, dataHash);
	audAssert(dataChunkId != adatContainer::InvalidId);
	u32 dataChunkSizeBytes;
	internalState->Container.GetObjectDataChunk(globalObjectId, dataChunkId, typeHash, state->DataChunkContainerOffsetBytes, dataChunkSizeBytes);
	audAssert(typeHash == (dataHash&adatContainerObjectDataTableEntry::kDataTypeHashMask));

	s32 blockIndex;
	if(m_StreamHeader->header.NumLoadBlocks <= 2 && m_IsLooping)
	{
		blockIndex = 0;		
		m_StartBlockIndex = ComputeStreamingBankBlockIndexFromTimeOffset(Max(0, m_StartOffsetMs));

		// Ensure we resume from the correct block once the preloaded pair have been consumed ('current' block is prepareBlock + 1)
		m_NextStreamingBlockPrepareSlotIndex = (u8)m_StartBlockIndex;
	}
	else if(m_StartOffsetMs >= 0)
	{
		blockIndex = ComputeStreamingBankBlockIndexFromTimeOffset(m_StartOffsetMs);
	}
	else
	{
		blockIndex = m_BlockIndexToLoad;
	}

	if(blockIndex < 0)
	{
		OUTPUT_ONLY(const char *bankName = GetBankName(m_LoadedBankId));
		audErrorf("Error loading block from streaming Wave Bank %s, blockId to load %d", bankName ? bankName : "Invalid", m_BankIdToLoad);
		audErrorf("blockindex < 0; startOffsetMs %d, stream duration ms %d", m_StartOffsetMs, audDriverUtil::ConvertSamplesToMs(m_StreamHeader->Channels[0].LengthSamples, m_StreamHeader->Channels[0].SampleRate));
		CleanUpAfterLoadFailure();
		return;
	}

	// we only load the header in the initial load, so we now need to load the first two load blocks

	//Only attempt to load as many blocks as we have available.
	Assign(m_NumLoadBlocksToRead, Min<u32>(m_NumLoadBlocksToRead, m_StreamHeader->header.NumLoadBlocks - blockIndex));

	if(m_NumLoadBlocksToRead == 1)
	{
		// Handle the case where we start from the final block, in which case we only preload a single block
		m_NextStreamingBlockPrepareSlotIndex = 1;
	}

	datResourceChunk chunk;
	// streaming block bytes includes block metadata size
	chunk.Size = m_NumLoadBlocksToRead * m_StreamHeader->header.StreamingBlockBytes;
	const u32 readOffsetBytes = state->DataChunkContainerOffsetBytes + (blockIndex * m_StreamHeader->header.StreamingBlockBytes);

	// note that we load directly into the metadata pointer
	audAssert(m_NextStreamingBlockLoadSlotIndex == 0);
	chunk.DestAddr = const_cast<u8*>(state->BlockData[m_NextStreamingBlockLoadSlotIndex]);
	
	const u32 fileSize = pgStreamer::GetSize(internalState->BankHandle);

	if (chunk.Size + readOffsetBytes > fileSize)
	{
		chunk.Size = fileSize - readOffsetBytes;
	}

	GetState()->LastLoadBytes = static_cast<u32>(chunk.Size);

	audAssert(!AUD_STREAMINGBUFFERS_IN_VRAM || gcm::IsLocalPtr(chunk.DestAddr));
	const u32 flags = m_UseCriticalQueue && g_UseCriticalQueueForAudio ? (pgStreamer::UNCACHED | pgStreamer::CRITICAL) : pgStreamer::UNCACHED;
	if(!pgStreamer::Read(internalState->BankHandle, &chunk, 1, readOffsetBytes, internalState->BankLoadSema, flags))
	{
		OUTPUT_ONLY(const char *bankName = GetBankName(m_LoadedBankId));
		audErrorf("Error loading block from streaming Wave Bank %s", bankName ? bankName : "Invalid");
		audErrorf("pgStreamer::Read() returned false: chunk.DestAddr: %p, size: %" SIZETFMT "u, readOffsetBytes: %u, fileSize: %u", chunk.DestAddr, chunk.Size, readOffsetBytes, fileSize);
		CleanUpAfterLoadFailure();
		return;
	}

	// populate wave format structures
	if(Verifyf(m_StreamHeader->header.NumChannels <= kMaxWavesPerStream, "Streaming wave bank %s has %u interleaved waves - max is %u", GetBankName(m_LoadedBankId) ? GetBankName(m_LoadedBankId) : "(null)", m_StreamHeader->header.NumChannels, kMaxWavesPerStream))
	{
		for(u32 i = 0; i < m_StreamHeader->header.NumChannels; i++)
		{			
			Assign(state->WaveFormatList[i].Format, m_StreamHeader->Channels[i].Format);
			Assign(state->WaveFormatList[i].LengthSamples, m_StreamHeader->Channels[i].LengthSamples);
			Assign(state->WaveFormatList[i].SampleRate, m_StreamHeader->Channels[i].SampleRate);
			Assign(state->WaveFormatList[i].Headroom, m_StreamHeader->Channels[i].Headroom);
			// necessary for 360 seamless looping streams
			Assign(state->WaveFormatList[i].LoopBegin, m_StreamHeader->Channels[i].LoopBegin);
	
			state->WaveFormatList[i].FirstPeakSample = kDefaultPeakLevel_u16;

			state->WaveFormatList[i].LoopEnd = 0;
			state->WaveFormatList[i].PlayBegin = 0;
			state->WaveFormatList[i].PlayEnd = 0;
			state->WaveFormatList[i].LoopPointSamples = -1;
		}
	}
	else
	{
		CleanUpAfterLoadFailure();
	}
}

void audStreamingWaveSlot::CleanUpAfterLoadFailure(void)
{
	m_LoadFailedBankId = m_LoadedBankId;
	m_LoadFailedWaveNameHash = 0;
	m_LoadedBankId = AUD_INVALID_BANK_ID;
	m_LoadedWaveNameHash = 0;
	m_BankIdToLoad = AUD_INVALID_BANK_ID;
	m_WaveNameHashToLoad = 0;

	m_IsLoading = false;
	m_IsLoadRequestQueued = false;

	m_StreamHeader = NULL;
	m_LoadBlockSeekTable = NULL;
}

void audStreamingWaveSlot::_SyncStreamingWaveSlotRequest(const u32 startOffsetMs, const u32 bankId, const bool isPreload, const bool isLooping)
{
	if(isPreload)
	{
		PreloadStreamingBankBlocks(bankId, startOffsetMs, isLooping);
	}
	else
	{
		RequestStreamingLoadBlock(bankId);
	}
}
#endif // !__SPU

void audStreamingWaveSlot::PreloadStreamingBankBlocks(const u32 bankId, const u32 startOffsetMs, const bool shouldLoop)
{
	audAssert(bankId != AUD_INVALID_BANK_ID);
	//**Start of Bank Load Critical Section**//
#if !__SPU
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

	if(m_IsLoading)
	{
		//Don't alter the settings if a load is currently in progress, keep requesting until it's done.
#if AUD_DEBUG_STREAMING
		audDisplayf("PreloadStreamingBankBlocks: is Loading");
#endif
		return;
	}

	if(GetReferenceCount() > 0)
	{
		//audWarningf("Requested preload of sample data while %u clients were still playing from the slot\n", m_ReferenceCount);		return;
#if AUD_DEBUG_STREAMING
		audDisplayf("PreloadStreamingBankBlocks: referenced");
#endif
		return;
	}

	Assign(m_BankIdToLoad, bankId);
	
	m_IsLooping = shouldLoop;

	m_StartOffsetMs = (s32)startOffsetMs;
	m_BlockIndexToLoad = -1;
	m_NumLoadBlocksToRead = kNumStreamingBlocksPerWaveSlot;
	m_NumLoadBlocksReady = 0;

	//Reset streaming block indices.
	m_NextStreamingBlockLoadSlotIndex = 0;
	m_NextStreamingBlockPrepareSlotIndex = 0;

	m_FirstBlockRequest = true;

	for(u8 i=0; i<kNumStreamingBlocksPerWaveSlot; i++)
	{
		m_StreamingBlockIndexSlots[i] = -1;
	}

	m_StreamHeader = NULL;
	m_LoadBlockSeekTable = NULL;

	m_IsLoadRequestQueued = true;

#if !__SPU
	m_LoadRequestTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();
#endif
}

void audStreamingWaveSlot::LoadStreamingBankBlock(const u32 bankId, const u32 blockIndex)
{
	audAssert(bankId != AUD_INVALID_BANK_ID);

#if !__SPU
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

	if(m_IsLoading)
	{
		//Don't alter the settings if a load is currently in progress, keep requesting until it's done.
		return;
	}

	audAssert(m_StreamHeader);

	Assign(m_BankIdToLoad, bankId);

	m_BlockIndexToLoad = (s32)blockIndex;
	m_StartOffsetMs = -1;
	m_NumLoadBlocksToRead = 1;

	m_IsLoadRequestQueued = true;

#if !__SPU
	m_LoadRequestTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();
#endif
}

audWaveSlot::audWaveSlotLoadStatus audStreamingWaveSlot::GetStreamingBankLoadingStatusForTimeMs(const u32 bankId,
	const u32 startOffsetMs) const
{
	audWaveSlotLoadStatus status = NOT_REQUESTED;

#if !__SPU
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

	bool isCorrectLoadBlockPending = (bankId == m_LoadedBankId) && DoesPendingStreamingBlockIncludeTimeMs(startOffsetMs);

	if(!m_IsLoading && !m_IsLoadRequestQueued && isCorrectLoadBlockPending)
	{
		status = LOADED;
	}
	else if(((bankId == m_LoadedBankId) || (bankId == m_BankIdToLoad)) &&
		isCorrectLoadBlockPending)
	{
		status = LOADING;
	}
	else if(bankId == m_LoadFailedBankId)
	{
		status = FAILED;
	}

	return status;
}

audWaveSlot::audWaveSlotLoadStatus audStreamingWaveSlot::GetStreamingBankLoadingStatusForBlockIndex(const u32 bankId,
	const u32 blockIndex) const
{
	audWaveSlotLoadStatus status = NOT_REQUESTED;

#if !__SPU
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

	if(!m_IsLoading && !m_IsLoadRequestQueued && (bankId == m_LoadedBankId) &&
		(m_BlockIndexToLoad == (s32)blockIndex))
	{
		status = LOADED;
	}
	else if(((bankId == m_LoadedBankId) || (bankId == m_BankIdToLoad)) &&
		(m_BlockIndexToLoad == (s32)blockIndex))
	{
		status = LOADING;
	}
	else if(bankId == m_LoadFailedBankId)
	{
		status = FAILED;
	}
	return status;
}

s32 audStreamingWaveSlot::ComputeStreamingWaveBankLengthMs(void) const
{
	s32 bankLengthMs = -1;

	audAssert(m_StreamHeader);

	if(m_StreamHeader)
	{
		//Find the longest Wave, as this defines the effective playback length of the Bank.
		
		for(u32 waveIndex=0; waveIndex<m_StreamHeader->header.NumChannels; waveIndex++)
		{
			const s32 waveLengthMs = static_cast<s32>(audDriverUtil::ConvertSamplesToMs(m_StreamHeader->Channels[waveIndex].LengthSamples, m_StreamHeader->Channels[waveIndex].SampleRate));
			if(waveLengthMs > bankLengthMs)
			{
				bankLengthMs = waveLengthMs;
			}
		}
	}

	return bankLengthMs;
}

s32 audStreamingWaveSlot::ComputeStreamingBankBlockIndexFromTimeOffset(u32 timeOffsetMs) const
{
	s32 index = -1;

	audAssert(m_LoadBlockSeekTable && m_StreamHeader);

	if(m_LoadBlockSeekTable && m_StreamHeader)
	{
		const u32 timeInSamples = audDriverUtil::ConvertMsToSamples(timeOffsetMs, m_StreamHeader->Channels[0].SampleRate);
		
		const audSeekTableEntry *nextSeekTableEntry;
		for(u32 blockIndex=0; blockIndex < m_StreamHeader->header.NumLoadBlocks - 1; blockIndex++)
		{
			nextSeekTableEntry = &(m_LoadBlockSeekTable[blockIndex + 1]);

			if(timeInSamples < nextSeekTableEntry->StartSample)
			{
				index = blockIndex;
				break;
			}
		}

		if(index == -1 && timeInSamples < m_StreamHeader->Channels[0].LengthSamples)
		{
			index = static_cast<s32>(m_StreamHeader->header.NumLoadBlocks) - 1;
		}
	}

	return index;
}

s32 audStreamingWaveSlot::ComputeStreamingBankBlockStartTimeMs(u32 blockIndex) const
{
	s32 startTimeMs = -1;
	audAssert(m_StreamHeader && m_LoadBlockSeekTable);

	if(m_StreamHeader && m_LoadBlockSeekTable)
	{
		if(blockIndex < m_StreamHeader->header.NumLoadBlocks)
		{
			const audSeekTableEntry *seekTableEntry = &(m_LoadBlockSeekTable[blockIndex]);
			//NOTE: Add 1 ms to ensure that any rounding error cannot force us into the previous load block.
			startTimeMs = (s32)audDriverUtil::ConvertSamplesToMs((f32)seekTableEntry->StartSample,m_StreamHeader->Channels[0].SampleRate) + 1;
		}
	}
	return startTimeMs;
}

bool audStreamingWaveSlot::DoesPendingStreamingBlockIncludeTimeMs(s32 timeOffsetMs) const
{
	bool isTimeIncluded = false;

	if(!m_StreamHeader)
	{
		//If we haven't yet loaded the streaming Wave Bank header, we have to assume that we will eventually load a block
		//of sample data covering this time offset - otherwise loading cannot continue.
#if AUD_DEBUG_STREAMING && !__SPU
		audDisplayf("Slot %s DoesPendingStreamingBlockIncludeTimeMs returned true as we haven't yet loaded the header", GetSlotName());
#endif
		return true;
	}

	// Streams with one or two blocks are loaded completely
	if(m_StreamHeader->header.NumLoadBlocks <= 2)
	{
		return true;
	}

	s32 queriedBlockIndex = ComputeStreamingBankBlockIndexFromTimeOffset(timeOffsetMs);
	if(queriedBlockIndex >= 0)
	{
		s32 pendingBlockIndex;
		if(m_BlockIndexToLoad >= 0)
		{
			// This case is hit once streaming is underway; ie not during preloading.
			// The BlockIndexToLoad is the later of the two load blocks, but starting a stream
			// relies on having two blocks ready, so if we return true here because m_BlockIndexToLoad is equal to 
			// the queriedBlockIndex the voice will be started at the wrong point (one block early).
			pendingBlockIndex = Min(m_StreamingBlockIndexSlots[0], m_StreamingBlockIndexSlots[1]);
		}
		else
		{
			pendingBlockIndex = ComputeStreamingBankBlockIndexFromTimeOffset(m_StartOffsetMs);
		}

		//NOTE: Only check the first of the load blocks if we are loading multiple.
		if(pendingBlockIndex == queriedBlockIndex)
		{
#if AUD_DEBUG_STREAMING && !__SPU
			audDisplayf("Slot %s DoesPendingStreamingBlockIncludeTimeMs %d pending %d queried %d (indexToLoad %d)", GetSlotName(), timeOffsetMs, pendingBlockIndex, queriedBlockIndex, m_BlockIndexToLoad);
#endif
			isTimeIncluded = true;
		}
	}

	return isTimeIncluded;
}

s32 audStreamingWaveSlot::ComputeStreamingBankBlockStartTimeFromTimeOffset(u32 timeOffsetMs) const
{
	s32 startTimeMs = -1;

	s32 blockIndex = ComputeStreamingBankBlockIndexFromTimeOffset(timeOffsetMs);
	if(blockIndex >= 0)
	{
		startTimeMs = ComputeStreamingBankBlockStartTimeMs((u32)blockIndex);
	}

	return startTimeMs;
}

audStreamingWaveSlot::audWaveSlotStreamingBlockLoadStatus audStreamingWaveSlot::RequestStreamingLoadBlock(const u32 bankId)
{
	audWaveSlotStreamingBlockLoadStatus status = FAILED_TO_LOAD_BLOCK;

 	if(bankId == AUD_INVALID_BANK_ID || !m_StreamHeader || (GetBankLoadingStatus(bankId) == FAILED))
	{
 		return status;
 	}

	//**Start of Bank Load Critical Section**//
	{
#if !__SPU
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

		if(m_NumLoadBlocksReady > 0)
		{
			status = LOADED_BLOCK;
			// If we only have one block in the stream and we're looping, we don't advance the slot index
			if(!m_IsLooping || m_StreamHeader->header.NumLoadBlocks > 1)
			{
				m_NextStreamingBlockPrepareSlotIndex = (m_NextStreamingBlockPrepareSlotIndex + 1) %
					kNumStreamingBlocksPerWaveSlot;
			}
			// If we only have one or two blocks and we're looping, reading a block doesn't reduce the number we have available
			if(!m_IsLooping || m_StreamHeader->header.NumLoadBlocks > 2)
			{
				m_NumLoadBlocksReady--;
			}
		}
	}//***End of Bank Load Critical Section***//

	if(status == FAILED_TO_LOAD_BLOCK)
	{
		u32 activeSlotIndex = (m_NextStreamingBlockPrepareSlotIndex + kNumStreamingBlocksPerWaveSlot - 1) %
			kNumStreamingBlocksPerWaveSlot;
		s32 nextStreamingBlockIndexToLoad = m_StreamingBlockIndexSlots[activeSlotIndex] + 1;

		if(m_IsLooping)
		{
			nextStreamingBlockIndexToLoad = nextStreamingBlockIndexToLoad % (s32)m_StreamHeader->header.NumLoadBlocks;
			for(u8 i=0; i<kNumStreamingBlocksPerWaveSlot; i++)
			{
				m_StreamingBlockIndexSlots[i] = m_StreamingBlockIndexSlots[i] % (s32)m_StreamHeader->header.NumLoadBlocks;
			}
		}
		// If we only have one load block in this stream then m_StreamingBLockIndexSlots[activeSlotIndex] will be -1, so nextStreamingBlockIndexToLoad becomes 0 which we already have
		if(m_StreamHeader->header.NumLoadBlocks > 1 && (nextStreamingBlockIndexToLoad >= 0) && (nextStreamingBlockIndexToLoad <	(s32)m_StreamHeader->header.NumLoadBlocks))
		{
			audAssert(bankId!=AUD_INVALID_BANK_ID);

			if(bankId!=AUD_INVALID_BANK_ID)
			{
				switch(GetStreamingBankLoadingStatusForBlockIndex(bankId, nextStreamingBlockIndexToLoad))
				{
					///NOTE: LOADED should only be reported here with thread synch issues - it should be handled by the
					//check of m_NumLoadBlocksReady > 0 above. If we do catch it down here, just wait and catch it
					//correctly next time through.
					case LOADED:
					case LOADING:
						status = LOADING_BLOCK;
						break;

					case FAILED:
						status = FAILED_TO_LOAD_BLOCK;
						break;

					case NOT_REQUESTED:
						LoadStreamingBankBlock(bankId, nextStreamingBlockIndexToLoad);
						status = LOADING_BLOCK;
						break;

					default:
						//Default case to shut the compiler up
						status = FAILED_TO_LOAD_BLOCK;
						break;
				}
			}
			else
			{
				status = FAILED_TO_LOAD_BLOCK;
			}
		}
		else
		{
			status = NO_DATA_FOR_BLOCK;
		}
	}

	return status;
}

u32 audStreamingWaveSlot::GetWaveNameHash(const u32 waveIndex) const
{
	audAssert(m_StreamHeader);
	audAssert(waveIndex < m_StreamHeader->header.NumChannels);
	return m_StreamHeader->Channels[waveIndex].NameHash;
}

#if !__SPU
s32 audStreamingWaveSlot::RegisterClient(const adatObjectId waveId)
{
	const u32 waveNameHash = GetInternalState()->Container.GetObjectNameHash(waveId);
	u32 waveStreamIndex = ~0U;
	for(u32 i = 0; i < m_StreamHeader->header.NumChannels; i++)
	{
		if(m_StreamHeader->Channels[i].NameHash == waveNameHash)
		{
			waveStreamIndex = i;
			break;
		}
	}
	if(waveStreamIndex == ~0U)
	{
		audWarningf("Wave Id doesn't have corresponding entry in stream header: %d (hash: %u)", waveId, waveNameHash);
		return -1;
	}
	for(s32 i = 0; i < kMaxStreamClients; i++)
	{
		if(GetState()->ClientState[i].state == audStreamClientState::kUnallocated)
		{
			GetState()->ClientState[i].state = audStreamClientState::kAllocated;
			Assign(GetState()->ClientState[i].waveIndex, waveStreamIndex);
			GetState()->NumClients++;
			DEBUG_STREAMING_ONLY(audDisplayf("Frame %u: Slot %s: Client %d allocated (Num clients %u)", audDriver::GetFrameCount(), GetSlotName(), i, GetState()->NumClients));
			return i;
		}
	}
	audWarningf("Slot %s: Unable to allocate client; num clients %u", GetSlotName(), GetState()->NumClients);
	return -1;
}

void audStreamingWaveSlot::UnregisterClient(const s32 clientId)
{
	audStreamingWaveSlotState *state = GetState();
	audAssert(state->ClientState[clientId].state >= audStreamClientState::kAllocated);
	audAssert(state->NumClients > 0);

#if AUD_DEBUG_STREAMING
	audDisplayf("Frame %u:Slot %s: Client %d unregistered, now have %u clients", audDriver::GetFrameCount(), GetSlotName(), clientId, GetState()->NumClients-1);
	if(state->ClientState[clientId].state == audStreamClientState::kNeedsData)
	{
		audDisplayf("Had been waiting for data for %u frames", audDriver::GetFrameCount() - state->ClientState[clientId].timeDataRequested);
	}
#endif

	state->ClientState[clientId].state = audStreamClientState::kUnallocated;
	state->NumClients--;
}

bool audStreamingWaveSlot::RequestDataForClient(const s32 clientId)
{
	audAssert(GetState()->ClientState[clientId].state >= audStreamClientState::kAllocated);
	if(GetState()->ClientState[clientId].state == audStreamClientState::kDataAvailable)
	{
		AUDSTR_DEBUG_MARKER("DataAvailable: %s:%s / %d (%d)", GetSlotName(), GetBankName(m_LoadedBankId), GetState()->ClientState[clientId].waveIndex, clientId);
	
		DEBUG_STREAMING_ONLY(audDisplayf("Frame %u: Slot %s: Client %d requested data; data is available (took %u frames)", audDriver::GetFrameCount(), GetSlotName(), clientId, audDriver::GetFrameCount() - GetState()->ClientState[clientId].timeDataRequested));
		GetState()->ClientState[clientId].state = audStreamClientState::kAllocated;
		return true;
	}
	if(GetState()->ClientState[clientId].state != audStreamClientState::kNeedsData)
	{
		AUDSTR_DEBUG_MARKER("DataRequired: %s:%s / %d (%d)", GetSlotName(), GetBankName(m_LoadedBankId), GetState()->ClientState[clientId].waveIndex, clientId);
		DEBUG_STREAMING_ONLY(GetState()->ClientState[clientId].timeDataRequested = audDriver::GetFrameCount());
		DEBUG_STREAMING_ONLY(audDisplayf("Frame %u: Slot %s: Client %d needs data", audDriver::GetFrameCount(), GetSlotName(), clientId));
	}
	
	GetState()->ClientState[clientId].state = audStreamClientState::kNeedsData;
	return false;
}

void audStreamingWaveSlot::GetFirstBlockInfo(const s32 clientIndex, audStreamingWaveBlock &blockInfo, u32 format) const
{
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

	// Return the earliest block out of the two we have
	const u32 waveIndex = GetState()->ClientState[clientIndex].waveIndex;

	// If we only have two blocks, we load them both in disk order, regardless of start offset
	// In that situation we may need to swap them in GetFirst/SecondBlockInfo
	bool swap = false;
	if(m_IsLooping && m_StreamHeader->header.NumLoadBlocks == 2 && m_StartBlockIndex != 0)
	{
		audAssert(m_StartBlockIndex == 1);
		swap = true;
	}

	// Compare as unsigned so that -1 isn't treated as earliest
	const u32 index0Unsigned = (u32)m_StreamingBlockIndexSlots[swap ? 1 : 0];
	const u32 index1Unsigned = (u32)m_StreamingBlockIndexSlots[swap ? 0 : 1];
	if(index0Unsigned > index1Unsigned)
	{
		return GetSpecificBlockInfo(1, waveIndex, blockInfo, format);
	}
	else
	{
		return GetSpecificBlockInfo(0, waveIndex, blockInfo, format);
	}
}

void audStreamingWaveSlot::GetSecondBlockInfo(const s32 clientIndex, audStreamingWaveBlock &blockInfo, u32 format) const
{
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

	// Return the latest block out of the two we have
	const u32 waveIndex = GetState()->ClientState[clientIndex].waveIndex;

	// If we only have two blocks, we load them both in disk order, regardless of start offset
	// In that situation we may need to swap them in GetFirst/SecondBlockInfo
	bool swap = false;
	if(m_IsLooping && m_StreamHeader->header.NumLoadBlocks == 2 && m_StartBlockIndex != 0)
	{
		audAssert(m_StartBlockIndex == 1);
		swap = true;
	}

	// Compare as unsigned so that -1 isn't treated as earliest
	const u32 index0Unsigned = (u32)m_StreamingBlockIndexSlots[swap ? 1 : 0];
	const u32 index1Unsigned = (u32)m_StreamingBlockIndexSlots[swap ? 0 : 1];

	// If we only have one block it will always be in slot 0
	if(index0Unsigned > index1Unsigned || (m_IsLooping && m_StreamHeader->header.NumLoadBlocks == 1))
	{
		return GetSpecificBlockInfo(0, waveIndex, blockInfo, format);
	}
	else
	{
		return GetSpecificBlockInfo(1, waveIndex, blockInfo, format);
	}
}

void audStreamingWaveSlot::GetCurrentBlockInfo(const s32 clientIndex, audStreamingWaveBlock &blockInfo, u32 format) const
{
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

	const u32 waveIndex = GetState()->ClientState[clientIndex].waveIndex;

	const u32 currentBlockIndex = (m_IsLooping && m_StreamHeader->header.NumLoadBlocks == 1) ? 0 :
									(m_NextStreamingBlockPrepareSlotIndex+1) % kNumStreamingBlocksPerWaveSlot;
	GetSpecificBlockInfo(currentBlockIndex, waveIndex, blockInfo, format);
}

void audStreamingWaveSlot::GetSpecificBlockInfo(const u32 currentBlockIndex, const u32 waveIndex, audStreamingWaveBlock &blockInfo, u32 
												#if !__PS3
												format
												#endif
												) const
{
	audAssert(m_StreamHeader);
	audAssert(waveIndex < m_StreamHeader->header.NumChannels);

	DEBUG_STREAMING_ONLY(audDisplayf("Requesting streaming block info for %s, %s block: %u", GetSlotName(), GetLoadedBankName(), m_StreamingBlockIndexSlots[currentBlockIndex]);)

	const audLoadBlockMetadata *metadata = reinterpret_cast<const audLoadBlockMetadata*>(GetState()->BlockData[currentBlockIndex]);

	blockInfo.metadata = metadata[waveIndex];
	// this channel's data lives after any other channels in the block
	u32 bytesToSkip = 0;
	u32 metadataSize = sizeof(audLoadBlockMetadata) * m_StreamHeader->header.NumChannels; 
	DEBUG_STREAMING_ONLY(audDisplayf("Base metadata size: %u (%u channels)", metadataSize, m_StreamHeader->header.NumChannels);)

	// calculate start offset packets by summing the number of packets in the earlier channels
	u32 startOffsetPackets = 0;

	// Regardless of which wave we're looking for, we have to sum the seek table entries for all waves in the block
	for(u32 i = 0; i < m_StreamHeader->header.NumChannels; i++)
	{	
		DEBUG_STREAMING_ONLY(audDisplayf("Channel %u Seek Table Size: %lu (%u packets)", i, sizeof(audSeekTableEntry) * metadata[i].NumPackets, metadata[i].NumPackets);)
		metadataSize += sizeof(audSeekTableEntry) * metadata[i].NumPackets;
	}

	DEBUG_STREAMING_ONLY(audDisplayf("Metadata size including seek table: %u", metadataSize);)

	// Sum only preceding waves in block for startOffsetPackets and bytesToSkip
	for(u32 i = 0; i < waveIndex; i++)
	{
		startOffsetPackets += metadata[i].NumPackets;
#if  __PS3 || RSG_AUDIO_X64
		if(format == audWaveFormat::kMP3)
		{
			const u32 alignment = m_StreamHeader->Channels[waveIndex].reserved0;
			if(alignment == 0)
			{
				bytesToSkip += metadata[i].NumberOfBytes;
			}
			else
			{
				bytesToSkip += (metadata[i].NumberOfBytes+alignment-1) & ~(alignment-1);
			}		
		}
#endif
	}

	if(format != audWaveFormat::kMP3)
	{
		bytesToSkip = startOffsetPackets * 2048;
	}

	metadataSize = (metadataSize + g_StreamingPacketBytes-1) & ~(g_StreamingPacketBytes-1);
	DEBUG_STREAMING_ONLY(audDisplayf("Metadata size (aligned): %u", metadataSize);)

	blockInfo.waveDataPtr = GetState()->BlockData[currentBlockIndex] + metadataSize + bytesToSkip;

	// Sanity check computed values are in range
	if(!audVerifyf(metadataSize + bytesToSkip < m_StreamHeader->header.StreamingBlockBytes, "Invalid streaming block data (%u >= %u): %s, %s block: %d", metadataSize + bytesToSkip, m_StreamHeader->header.StreamingBlockBytes, GetSlotName(), GetLoadedBankName(), m_StreamingBlockIndexSlots[currentBlockIndex]))
	{
		audDisplayf("%u channels", m_StreamHeader->header.NumChannels);

		for(u32 i = 0; i < Min(m_StreamHeader->header.NumChannels, 12u); i++)
		{	
			audDisplayf("Channel %u: %u packets", i, metadata[i].NumPackets);
		}

		audDisplayf("startOffsetPackets = %u", startOffsetPackets);
		audDisplayf("metadataSize (%u) + bytesToSkip (%u) = %u", metadataSize, bytesToSkip, metadataSize + bytesToSkip);
		audDisplayf("m_StreamHeader->header.StreamingBlockBytes = %u", m_StreamHeader->header.StreamingBlockBytes);

		blockInfo.IsFinalBlock = true;
		blockInfo.packetSeekTable = NULL;
		blockInfo.waveDataPtr = NULL;
		return;
	}
	else
	{
#if AUD_DEBUG_STREAMING
		audDisplayf("Valid streaming block data (%u < %u): %s, %s block: %d", metadataSize + bytesToSkip, m_StreamHeader->header.StreamingBlockBytes, GetSlotName(), GetLoadedBankName(), m_StreamingBlockIndexSlots[currentBlockIndex]);
		audDisplayf("%u channels", m_StreamHeader->header.NumChannels);

		for(u32 i = 0; i < Min(m_StreamHeader->header.NumChannels, 12u); i++)
		{	
			audDisplayf("Channel %u: %u packets", i, metadata[i].NumPackets);
		}

		audDisplayf("startOffsetPackets = %u", startOffsetPackets);
		audDisplayf("metadataSize (%u) + bytesToSkip (%u) = %u", metadataSize, bytesToSkip, metadataSize + bytesToSkip);
		audDisplayf("m_StreamHeader->header.StreamingBlockBytes = %u", m_StreamHeader->header.StreamingBlockBytes);
#endif
	}

	const audSeekTableEntry *blockPacketSeekTable = (const audSeekTableEntry*)(GetState()->BlockData[currentBlockIndex] + sizeof(audLoadBlockMetadata)*m_StreamHeader->header.NumChannels);

	blockInfo.packetSeekTable = blockPacketSeekTable + startOffsetPackets;

	blockInfo.IsFinalBlock = ((u32)m_StreamingBlockIndexSlots[currentBlockIndex] == m_StreamHeader->header.NumLoadBlocks-1);
	blockInfo.blockId = m_StreamingBlockIndexSlots[currentBlockIndex];

#if AUD_DEBUG_STREAMING

#if __XENON
		u32 length = 2048;
#elif __PS3
	u32 length = blockInfo.metadata.NumberOfBytes;
#else
	u32 length = blockInfo.metadata.NumSamples<<1;
	if(format == audWaveFormat::kMP3)
	{
		length = blockInfo.metadata.NumberOfBytes;
	}
	if(format == audWaveFormat::kXMA2)
	{
		length = 2048;
	}
#endif

#if __PS3
	u32 align = m_StreamHeader->Channels[0].reserved0;
#else
	u32 align = 0;
	if(format == audWaveFormat::kMP3)
	{
		align = m_StreamHeader->Channels[0].reserved0;
	}
#endif
	
	audDisplayf("Frame %u: wave: %u block: %u (%u) %u metadata align: %u %p size %u (start: %u (%u) end: %u) skip: %u %s", 
		audDriver::GetFrameCount(),
		waveIndex, 
		currentBlockIndex,
		m_StreamingBlockIndexSlots[currentBlockIndex],
		metadataSize,
		align,
		blockInfo.waveDataPtr,
		length,
		blockInfo.packetSeekTable->StartSample,
		blockInfo.packetSeekTable->StartSample+metadata[waveIndex].SamplesToSkip,
		blockInfo.metadata.NumSamples, 
		metadata[waveIndex].SamplesToSkip,
		blockInfo.IsFinalBlock ? "(final block)" : "");

#endif // AUD_DEBUG_STREAMING
}

const audWaveFormat *audStreamingWaveSlot::FindFormat(const adatObjectId waveId) const
{
	const u32 waveNameHash = GetInternalState()->Container.GetObjectNameHash(waveId);
	for(u32 i = 0; i < m_StreamHeader->header.NumChannels; i++)
	{
		if(m_StreamHeader->Channels[i].NameHash == waveNameHash)
		{
			return &GetState()->WaveFormatList[i];
		}
	}
	return NULL;
}
#endif // !__SPU

}	// namespace rage
