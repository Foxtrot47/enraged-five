//=====================================================================================================
// name:		MediaStreamReader.cpp
// description:	Media stream reader for video and audio streams.
//=====================================================================================================
#if __WIN32PC

#include <stdio.h> 
#include <string.h>

#include "wmfstreamreader.h"
#include "audiohardware/driver.h"
#include "system/ipc.h"
#include "system/threadregistry.h"
#include "audiosoundtypes/externalstreamsound.h"

#pragma warning(disable: 4200) 
#pragma warning(disable: 4995)
#include "system/simpleallocator.h"
#include "wtypes.h"
#pragma warning(error: 4200)
#pragma warning(error: 4995)

namespace rage {

const sysIpcPriority audStreamReaderThreadPriority = PRIO_BELOW_NORMAL;
const int audStreamReaderThreadCpu = 0;

const size_t kDecodeSize = 512*1024;
const size_t kRingBufferSizeBytes = 2 * kDecodeSize;

atRangeArray<audMediaReader *, g_MaxMediaReaders> audMediaReader::sm_MediaReader;

#define USER_TRACK_FORMAT_LOOKUP_COUNT (sizeof(sm_UserTrackFormatLookup) / sizeof(USER_TRACK_FORMAT_LOOKUP))

USER_TRACK_FORMAT_LOOKUP audMediaReader::sm_UserTrackFormatLookup[] =
{
//	{".ogg", TRACK_FORMAT_OGG},
	{L".mp3", TRACK_FORMAT_WMF},

	{L".wav", TRACK_FORMAT_WMF},

	{L".wma", TRACK_FORMAT_WMF},
	{L".wmv", TRACK_FORMAT_WMF},

	{L".aac", TRACK_FORMAT_WMF},
	{L".m4a", TRACK_FORMAT_WMF},
};

void audMediaReader::InitClass()
{
	for(int i=0; i<g_MaxMediaReaders; i++)
	{
		sm_MediaReader[i] = NULL;
	}

	audWMFStreamReader::InitClass();
}

bool audMediaReader::IsFormatAvailable(USER_TRACK_FORMAT format)
{
	switch(format)
	{
	case TRACK_FORMAT_WMF:
		return audWMFStreamReader::IsAvailable();
	default:
		return false;
	}
}

bool audMediaReader::IsValidFile(const WCHAR *filePath, s32 &duration)
{
	audMediaReader *reader = rage_new audMediaReader();
	const bool openedSuccessfully = reader->Open(filePath, 0, true);
	if(openedSuccessfully)
	{
		duration = reader->GetStreamLengthMs();
		reader->Close();
	}
	delete reader;
	return openedSuccessfully;
}

//=====================================================================================================
audMediaReader::audMediaReader() :
	m_bFileOpen(false),
	m_MediaStreamer(NULL),
	m_TrackFormat(TRACK_FORMAT_UNKNOWN),
	m_DecoderThreadFinishedSema(NULL),
	m_EnableStreamReaderThread(true),
	m_EnableDecode(false),
	m_IsStreamReaderThreadRunning(false),
	m_RingBuffer(NULL),
	m_SoundBuffer(NULL),
	m_Sound(NULL),
	m_StartOffsetMs(0),
	m_State(TRACK_STATE_INVALID),
	m_DecodeThread(sysIpcThreadIdInvalid)
{
}

//=====================================================================================================
audMediaReader::~audMediaReader()
{
	Close();
	m_MediaStreamer = NULL;
	m_TrackFormat = TRACK_FORMAT_UNKNOWN;
	if(m_DecoderThreadFinishedSema)
	{
		sysIpcDeleteSema(m_DecoderThreadFinishedSema);
	}
}

s32 audMediaReader::GetStreamPlayTimeMs() const
{
	if(m_Sound && *m_Sound && (*m_Sound)->GetSoundTypeID() == ExternalStreamSound::TYPE_ID)
	{
		return static_cast<s32>(((audExternalStreamSound*)m_Sound)->GetCurrentPlayTimeOfWave());
	}
	return -1;
}

bool audMediaReader::Open(const WCHAR* pszFileName, u32 startOffsetMs, bool openOnly)
{
	if(!pszFileName)
	{
		return false;
	}

	Displayf("Opening: %s", pszFileName);
	
	USE_MEMBUCKET(MEMBUCKET_AUDIO);

	// figure out what kind of file we have and who can open it
	m_TrackFormat = GetFileFormat(pszFileName);
	if (IsFileOpen() || m_TrackFormat == TRACK_FORMAT_UNKNOWN)
	{
		return false;
	}

	switch(m_TrackFormat )
	{
		case TRACK_FORMAT_WMF:
			{
				if(audWMFStreamReader::IsAvailable())
				{
					m_MediaStreamer = (audMediaReaderPlatform*)rage_new audWMFStreamReader();
					Assert(m_MediaStreamer);
				}
			}
			break;

		default:
			return false;
			break;
	}

	m_StartOffsetMs = startOffsetMs;

	// do a block open and don't create the decode thread
	if(m_MediaStreamer && openOnly)
	{
		if(m_MediaStreamer->MediaOpen(pszFileName, startOffsetMs))
		{
			m_EnableStreamReaderThread = false;
			SetFileOpen(true);
			return true;
		}
		return false;
	}

	if(m_MediaStreamer)
	{
		safecpy(m_Filename, pszFileName);		
		m_State = TRACK_STATE_INITIALIZING;

		//start up the thread
		if(m_EnableStreamReaderThread)
		{
			m_DecoderThreadFinishedSema = sysIpcCreateSema(0);
			Assert(m_DecoderThreadFinishedSema);

			// create reader thread
			m_DecodeThread = sysIpcCreateThread(StreamReaderThreadEntryProc, this, 65536, audStreamReaderThreadPriority, "[RAGE Audio] User Track Decode Thread", false, audStreamReaderThreadCpu);
			Assert(m_DecodeThread != sysIpcThreadIdInvalid && "Couldn't create User track decode thread!");
			//sysIpcResumeThread(m_DecodeThread);	
		}
	}
	else
	{
		Warningf("Bad user music track");
		m_State = TRACK_STATE_INVALID;
		return false;
	}
	
	return true;
}

//=====================================================================================================
bool audMediaReader::Close()
{
	if(m_State != TRACK_STATE_FAILED)
		m_State = TRACK_STATE_INVALID;

	//if (!IsFileOpen())
	//{
	//	return false;
	//}

	m_bFileOpen = false;

	if(m_EnableStreamReaderThread && m_DecoderThreadFinishedSema)
	{
		m_EnableStreamReaderThread = false;

		while(m_IsStreamReaderThreadRunning)
		{
			sysIpcSleep(0);
		}
		sysIpcWaitSema(m_DecoderThreadFinishedSema);

		sysIpcWaitThreadExit(m_DecodeThread);
		m_DecodeThread = sysIpcThreadIdInvalid;
	}

	if(m_RingBuffer)
	{
		//Displayf("%s : %s : Delete RingBuffer", __FILE__, __FUNCTION__);
		m_RingBuffer->Release();
		m_RingBuffer = NULL;
	}
	if(m_SoundBuffer)
	{
		//Displayf("%s : %s : Delete SoundBuffer", __FILE__, __FUNCTION__);
		audDriver::FreePhysical(m_SoundBuffer);
		m_SoundBuffer = NULL;
	}

	if(m_MediaStreamer)
	{
		m_MediaStreamer->MediaClose();
		delete m_MediaStreamer;
		m_MediaStreamer = NULL;
	}

	return true;
}


USER_TRACK_FORMAT audMediaReader::GetFileFormat(const WCHAR *fileName)
{
	WCHAR* wext = wcsrchr((WCHAR*)fileName, '.');

	if(!wext)
	{
		return TRACK_FORMAT_UNKNOWN;
	}

	for(int i = 0; i < USER_TRACK_FORMAT_LOOKUP_COUNT; i++)
	{
		if(!_wcsicmp(sm_UserTrackFormatLookup[i].fileExt, wext))
		{
			return sm_UserTrackFormatLookup[i].format;	
		}
	}

	return TRACK_FORMAT_UNKNOWN;
}

s32 audMediaReader::FillBuffer(void *sampleBuf, u32 bytesToRead)
{
	if(m_MediaStreamer)
	{
		return m_MediaStreamer->FillBuffer(sampleBuf, bytesToRead);
	}
	return -1;
}

void audMediaReader::UpdateBuffer()
{
	USE_MEMBUCKET(MEMBUCKET_AUDIO);

	if(m_State != TRACK_STATE_FAILED && !IsPlaying())
	{
		return;
	}

	if(m_State == TRACK_STATE_INITIALIZING)
	{
		//Displayf("%s : %s : TRACK_STATE_INITIALIZING", __FILE__, __FUNCTION__);
		if(m_MediaStreamer->MediaOpen(&m_Filename[0], m_StartOffsetMs))
		{
			SetFileOpen(true);
			m_State = TRACK_STATE_PREPARING;
		}
		else
		{
			audErrorf("audMediaReader::UpdateBuffer - MediaOpen failed");
			m_State = TRACK_STATE_FAILED;
		}
	}
	if(m_State == TRACK_STATE_PREPARING)
	{
		//Displayf("%s : %s : TRACK_STATE_PREPARING", __FILE__, __FUNCTION__);
		if(m_MediaStreamer)
			m_MediaStreamer->Prepare();
		if(!m_RingBuffer && m_MediaStreamer && m_MediaStreamer->GetSampleRate() != -1)
		{
			u8 *ringBufferData = rage_aligned_new(16) u8[kRingBufferSizeBytes];
			sysMemSet(ringBufferData, 0, kRingBufferSizeBytes);
			m_RingBuffer = rage_new audReferencedRingBuffer();
			m_RingBuffer->Init(ringBufferData, kRingBufferSizeBytes, true);
			m_SoundBuffer = (u8*)audDriver::AllocatePhysical(kRingBufferSizeBytes, 16);

			//audDisplayf("audMediaStreamer::UpdateBuffer - prepared");
			m_State = TRACK_STATE_PREPARED;
		}
	}
	if(m_RingBuffer)
	{
		u32 freeBytesInRingBuffer = m_RingBuffer->GetBufferSize() - m_RingBuffer->GetBytesAvailableToRead();
		if(freeBytesInRingBuffer > kDecodeSize)
		{
			u32 bytesDecoded = FillBuffer(m_SoundBuffer, freeBytesInRingBuffer);
			if(bytesDecoded > 0)
			{
				//Displayf("bytesDecoded avail %d, bytesDecoded filled %d", freeBytesInRingBuffer, bytesDecoded);
				m_RingBuffer->WriteData(m_SoundBuffer, bytesDecoded);
			}
			else
			{
				//Displayf("I think we're done");
				if(m_RingBuffer->GetBytesAvailableToRead() == 0)
				{
					u32 freeBytesInRingBuffer = m_RingBuffer->GetBufferSize() - m_RingBuffer->GetBytesAvailableToRead();
					if(freeBytesInRingBuffer > 0) // because we're not decoding we'll do a smaller buffer
					{
						//Displayf("%s : %s : nothing to decode, fill with 0", __FILE__, __FUNCTION__);
						memset(m_SoundBuffer, 0, freeBytesInRingBuffer);
						m_RingBuffer->WriteData(m_SoundBuffer, freeBytesInRingBuffer);
					}
				}
			}
		}
	}
}

DECLARE_THREAD_FUNC(audMediaReader::StreamReaderThreadEntryProc)
{
	audMediaReader *reader = (audMediaReader*)ptr;
	reader->m_IsStreamReaderThreadRunning = true;

	while(reader->m_EnableStreamReaderThread)
	{
		if(reader->m_EnableStreamReaderThread)
		{
			reader->UpdateBuffer();
		}
	}
	//Displayf("%s : Decoder finished", __FILE__);
	reader->m_IsStreamReaderThreadRunning = false;
	sysIpcSignalSema(reader->m_DecoderThreadFinishedSema);
}

/*
size_t filenameLength = wcslen((const wchar_t*)fileName);
m_FileName = rage_new unsigned short[filenameLength+1];
memset(m_FileName, 0, filenameLength+1);
if(m_FileName)
{
	wcsncpy((wchar_t*)m_FileName, (const wchar_t*)fileName, filenameLength);
	m_FileName[filenameLength] = 0;
}
*/

audMediaReader* audMediaReader::CreateMediaStreamer(const WCHAR* fileName, audSound** sound, u32 startOffsetMs)
{
	USE_MEMBUCKET(MEMBUCKET_AUDIO);

	int index = -1;
	// find a free slot
	for(int i=0; i<g_MaxMediaReaders; i++)
	{
		if(sm_MediaReader[i] == NULL)
		{
			index = i;
			break;
		}
	}
	if(index == -1)
		return NULL;

	// create new media streamer object
	audMediaReader* mediaReader = rage_new audMediaReader;
	if(mediaReader)
	{
		sm_MediaReader[index] = mediaReader;
		mediaReader->m_Sound = sound;
		if( mediaReader->Open(fileName, startOffsetMs, false) ) // open track, duration is valid after this call
		{
			return mediaReader;
		}
		delete mediaReader;
		sm_MediaReader[index] = NULL;
	}
	return NULL;
}

bool audMediaReader::IsPrepared(audMediaReader* mediaReader)
{
	for(int i=0; i<g_MaxMediaReaders; i++)
	{
		if(sm_MediaReader[i] && sm_MediaReader[i] == mediaReader)
		{
			if(mediaReader->GetState() == TRACK_STATE_PREPARED)
				return true;
		}
	}
	return false;
}

void audMediaReader::DestroyMediaStreamer(audMediaReader* mediaReader)
{
	if(!mediaReader)
		return;

	for(int i=0; i<g_MaxMediaReaders; i++)
	{
		if(sm_MediaReader[i] && sm_MediaReader[i] == mediaReader)
		{
			if(mediaReader->IsPlaying())
			{
				//Displayf("%s : %s : Stop sound", __FILE__, __FUNCTION__);
				mediaReader->StopSound();
			}
			mediaReader->Close();

			delete mediaReader;
			sm_MediaReader[i] = NULL;

			return;
		}
	}
}

}

#endif // WIN32PC
