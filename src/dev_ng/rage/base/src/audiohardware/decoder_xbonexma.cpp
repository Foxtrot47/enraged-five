//
// audiohardware/decoder_xbonexma.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#if RSG_DURANGO

#include "debug.h"
#include "decoder_xbonexma.h"
#include "decodemgr.h"
#include "driver.h"
#include "profile/profiler.h"
#include "file/asset.h"
#include "grcore/debugdraw.h"

//#include "acphal.h"
//#include <xma2defs.h>

//OPTIMISATIONS_OFF()

EXT_PF_GROUP(MixerDevice);

#define AUD_PROFILE_XMAXBONE_DECODER 0
#if AUD_PROFILE_XMAXBONE_DECODER
PF_TIMER(DecoderXmaXboneUpdate,MixerDevice);
PF_TIMER(DecoderXmaXboneUpdate_Wait,MixerDevice);
#define XMA_PF_FUNC(x) PF_FUNC(x)
#else
#define XMA_PF_FUNC(x)
#endif

#define XSF_ERROR_IF_FAILED( exp ) { HRESULT _hr_ = (exp); if( FAILED( _hr_ ) ) Errorf( "Failure with HRESULT of %x in %s at line %d",  ( _hr_ ), __FILE__, __LINE__ ); }
#define CHECK_RESULT( exp ) if(FAILED(exp)) { Errorf( "Failure with HRESULT of %x in %s at line %d", ( exp ), __FILE__, __LINE__ ); return false; }

namespace rage
{
	IAcpHal*		audDecoderXboxOneXma::sm_AcpHal = nullptr;
	HANDLE			audDecoderXboxOneXma::m_TerminateThreads = nullptr;
	long			audDecoderXboxOneXma::m_ExpectedThreadsRunning = 0;
	int				audDecoderXboxOneXma::m_MessageQueueThreadSleep = 33;
	volatile long	audDecoderXboxOneXma::m_NumThreadsRunning = 0;
	bool			audDecoderXboxOneXma::m_Disconnected = false;
	SHAPE_CONTEXTS	audDecoderXboxOneXma::sm_Contexts;
	bool*			audDecoderXboxOneXma::sm_ContextFree = NULL;
	xmaContextInfo*	audDecoderXboxOneXma::sm_ContextInfo = NULL;
	s32				audDecoderXboxOneXma::sm_StartIndex = 0;
	u32				audDecoderXboxOneXma::sm_AudioFrameCount = 0;
#if __BANK
	u32				audDecoderXboxOneXma::sm_PeekAudioMessages = 0;
#endif

	BOOL audDecoderXboxOneXma::FixBlockAlign(DWORD* pBYTES,DWORD BLOCKALIGN,DWORD* pChange)
	{
		if(pBYTES && (BLOCKALIGN > 1))
		{
			DWORD bytes = (*pBYTES);
			(*pBYTES) /=  BLOCKALIGN;
			(*pBYTES) *=  BLOCKALIGN;
			if(pChange)
			{
				(*pChange) = bytes - (*pBYTES);
				if((*pChange) > 0)
				{
					return TRUE;
				}
			}
		}
		return FALSE;
	}

	HRESULT audDecoderXboxOneXma::ApuVirtualAllocate(void** virtualAddress,UINT32 sizeInBytes)
	{
		DWORD change = 0;
		if(audDecoderXboxOneXma::FixBlockAlign((DWORD*)&sizeInBytes,SHAPE_XMA_INPUT_BUFFER_SIZE_ALIGNMENT,&change)) // returns TRUE if alignemt was needed else FALSE
		{  
			return E_INVALIDARG;
		}
		return ApuAlloc(virtualAddress,NULL,sizeInBytes,SHAPE_XMA_INPUT_BUFFER_ALIGNMENT);
	}

	void audDecoderXboxOneXma::ApuVirtualFree(void* virtualAddress)
	{
		if(virtualAddress)
		{
			ApuFree(virtualAddress);
		}
	}

	s32 audDecoderXboxOneXma::GetFreeXmaContextIndex()
	{
		int index = sm_StartIndex;

		for(int i=0; i<kXBOneNumberXMAContexts; i++)
		{
			if(sm_ContextInfo[index].IsFree && sm_ContextInfo[index].State == ACP_CONTEXT_STATE_DISABLED && sm_ContextInfo[index].AudioFrame < sm_AudioFrameCount)
			{
				sm_ContextInfo[index].IsFree = false;
				sm_StartIndex = (sm_StartIndex+1) % kXBOneNumberXMAContexts;
				return index;
			}

			index = (index+1) % kXBOneNumberXMAContexts;
		}

		return -1;
	}

	void audDecoderXboxOneXma::FreeXmaContextIndex(s32 index)	
	{ 
		sm_ContextInfo[index].IsFree = true; 
	}

	void audDecoderXboxOneXma::DisableXmaContext(s32 index)	
	{ 
		if(index >= 0 && index < kXBOneNumberXMAContexts)
		{
			ACP_COMMAND_ENABLE_OR_DISABLE_XMA_CONTEXT enableCommand =
			{
				index
			};
			XSF_ERROR_IF_FAILED( sm_AcpHal->SubmitCommand( ACP_COMMAND_TYPE_DISABLE_XMA_CONTEXT, index+1, ACP_SUBMIT_PROCESS_COMMAND_ASAP, &enableCommand ) ); //index+1 because we can't use 0
		}

		sm_ContextInfo[index].AudioFrame = sm_AudioFrameCount;
		sm_ContextInfo[index].State = ACP_CONTEXT_STATE_DISABLED; 		
	}

	void audDecoderXboxOneXma::EnableXmaContext(s32 index)	
	{ 
		ACP_COMMAND_ENABLE_OR_DISABLE_XMA_CONTEXT enableCommand =
		{
			index
		};
		XSF_ERROR_IF_FAILED( sm_AcpHal->SubmitCommand( ACP_COMMAND_TYPE_ENABLE_XMA_CONTEXT, 0, ACP_SUBMIT_PROCESS_COMMAND_ASAP, &enableCommand ) ); //index+1 because we can't use 0

		sm_ContextInfo[index].AudioFrame = sm_AudioFrameCount;
		sm_ContextInfo[index].State = ACP_CONTEXT_STATE_ENABLE; 
	}


	void audDecoderXboxOneXma::SetXmaContextState(s32 index, audACPContextState state)
	{
		sm_ContextInfo[index].State = state; 
	}

	audACPContextState audDecoderXboxOneXma::GetXmaContextState(s32 index)
	{
		return sm_ContextInfo[index].State;
	}

	bool audDecoderXboxOneXma::InitClass()
	{

		//XSF_ERROR_IF_FAILED( ApuCreateHeap(	64*1024*1024, 2*1024*1024) );

		m_NumThreadsRunning = 0;
		m_ExpectedThreadsRunning = 0;
		m_MessageQueueThreadSleep = 33;

		sm_Contexts.numDmaContexts      = 0;
		sm_Contexts.numPcmContexts      = 0;
		sm_Contexts.numSrcContexts      = 0;  // must be same as sum of PCM and XMA contexts, but not really
		sm_Contexts.numEqCompContexts   = 0;
		sm_Contexts.numFiltVolContexts  = 0;
		sm_Contexts.numXmaContexts		= kXBOneNumberXMAContexts;
		CHECK_RESULT( AcpHalAllocateShapeContexts( &sm_Contexts) );

		// Zero the context arrays
		ZeroMemory( sm_Contexts.xmaContextArray, sm_Contexts.numXmaContexts * sizeof(SHAPE_XMA_CONTEXT) );
		Displayf("Xma context memory size: %d", sm_Contexts.numXmaContexts * sizeof(SHAPE_XMA_CONTEXT));

		sm_ContextFree = rage_new bool[kXBOneNumberXMAContexts];
		audAssert(sm_ContextFree);
		for(int i=0; i<kXBOneNumberXMAContexts; i++)
		{
			sm_ContextFree[i] = true;
		}
		sm_StartIndex = 0;

		sm_ContextInfo = rage_new xmaContextInfo[kXBOneNumberXMAContexts];
		audAssert(sm_ContextInfo);
		const s32 memRequiredBytes = kXBOneNumberXMAContexts * (kXBOneXMAHalOutputBufferBytes + 256);			
		u8* data = NULL;
		ApuVirtualAllocate((void**)&data, memRequiredBytes);
		audAssert(data);
		for(int i=0; i<kXBOneNumberXMAContexts; i++)
		{
			sm_ContextInfo[i].IsFree = true;
			sm_ContextInfo[i].State = ACP_CONTEXT_STATE_DISABLED;
			sm_ContextInfo[i].AudioFrame = 0;
			sm_ContextInfo[i].DecodedOutputBuffer = data;
			data += kXBOneXMAHalOutputBufferBytes; 
			ZeroMemory( sm_ContextInfo[i].DecodedOutputBuffer, memRequiredBytes);

			// Size must be 256 bytes for mono content and 512 bytes for stereo
			sm_ContextInfo[i].OutputOverlapBuffer = data;
			data += 256; 
			ZeroMemory( sm_ContextInfo[i].OutputOverlapBuffer, 256);
		}

		// Create the interface to the HAL
		HRESULT result = AcpHalCreate( &sm_AcpHal);
		CHECK_RESULT(result)

		// Initialize the ACP HAL
		result = sm_AcpHal->Connect( COMMAND_QUEUE_LENGTH, MESSAGE_QUEUE_LENGTH );
		CHECK_RESULT(result)

		// Create the event to terminate the threads
		m_TerminateThreads = CreateEvent( NULL, TRUE, FALSE, NULL );
		//XSF_ERROR_IF_FAILED( ( m_TerminateThreads == NULL) ? HRESULT_FROM_WIN32( GetLastError() ) : S_OK );

		// Create the message queue thread
		HANDLE messageQueueThread = CreateThread( NULL, 0, MessageQueueThreadProc, 0, 0, NULL );
		//XSF_ERROR_IF_FAILED( ( messageQueueThread == NULL) ? HRESULT_FROM_WIN32( GetLastError() ) : S_OK );
		////  Allocate to CPU0
		//DWORD_PTR   oldMsgAffinityMask = SetThreadAffinityMask( messageQueueThread, DWORD_PTR( 1 << SHAPE_MESSAGEQUEUE_CORE ) );
		//XSF_ERROR_IF_FAILED( ( oldMsgAffinityMask == 0 ) ? HRESULT_FROM_WIN32( GetLastError() ) : S_OK );
		CloseHandle( messageQueueThread );
		++m_ExpectedThreadsRunning;

		// Wait for all threads to start
		while( m_NumThreadsRunning < m_ExpectedThreadsRunning )
		{
			Sleep(10);
		}

		ACP_COMMAND_MESSAGE command = { 0 };

		// Register for the following messages:
		command.message |= ACP_MESSAGE_TYPE_DISCONNECTED;

		// We only register for the disconnected message to reduce load on the ACP.
		// Ultimately you would probably register for various error messages too:
		command.message |= ACP_MESSAGE_TYPE_ERROR;

		// Other possible messages that can be registered:
		command.message |= ACP_MESSAGE_TYPE_AUDIO_FRAME_START;
		//command.message |= ACP_MESSAGE_TYPE_FLOWGRAPH_COMPLETED;
		//command.message |= ACP_MESSAGE_TYPE_SRC_BLOCKED;
		//command.message |= ACP_MESSAGE_TYPE_DMA_BLOCKED;
		command.message |= ACP_MESSAGE_TYPE_COMMAND_COMPLETED;
		//command.message |= ACP_MESSAGE_TYPE_FLOWGRAPH_TERMINATED;

		CHECK_RESULT( sm_AcpHal->SubmitCommand(ACP_COMMAND_TYPE_REGISTER_MESSAGE, 0, ACP_SUBMIT_PROCESS_COMMAND_ASAP, &command ) );

		return true;
	}

	void audDecoderXboxOneXma::ShutdownClass()
	{
		// Disconnect from the ACP HAL
		XSF_ERROR_IF_FAILED( sm_AcpHal->Disconnect() );

		// Wait until disconnected 
		while( !m_Disconnected )
		{
			Sleep(100);
		}

		// Terminate all threads
		SetEvent( m_TerminateThreads );
		while( m_NumThreadsRunning > 0 )
		{
			Sleep( 10 );
		}

		//  Release the ACP HAL
		sm_AcpHal->Release();
		sm_AcpHal = nullptr;

		AcpHalReleaseShapeContexts();

		delete sm_ContextFree;
		delete sm_ContextInfo;

	}

#if __BANK


	void audDecoderXboxOneXma::PrintUsedXmaContexts()
	{
#define AUD_BOOL2STR(x) x ? "True" : "False"

		audDisplayf("Printing used xma contexts:");
		int index = sm_StartIndex;
		for(int i=0; i<kXBOneNumberXMAContexts; i++)
		{
			if(!sm_ContextInfo[index].IsFree || sm_ContextInfo[index].State != ACP_CONTEXT_STATE_DISABLED || sm_ContextInfo[index].AudioFrame >= sm_AudioFrameCount)
			{
				audDisplayf("%d: Context for WaveNameHash %u: IsFree (%s), IsDisabled (%s), Valid Audio Frame (%s)", index, sm_ContextInfo->WaveNameHash, AUD_BOOL2STR(sm_ContextInfo[index].IsFree), AUD_BOOL2STR(sm_ContextInfo[index].State == ACP_CONTEXT_STATE_DISABLED), AUD_BOOL2STR(sm_ContextInfo[index].AudioFrame < sm_AudioFrameCount));
			}
		}
	}

	void audDecoderXboxOneXma::PrintDecoderStats()
	{
		//struct ApuHeapState
		//{
		//	UINT32 bytesFree;                 // Size of all unallocated regions
		//	UINT32 bytesAllocated;            // Size of all allocated regions as visible to the title
		//	UINT32 bytesLost;                 // Size of all memory lost to alignment requests and fragmented blocks too small to allocate
		//	UINT32 maximumBlockSizeAvailable; // Largest block available (actual space available can be less due to alignment requirements)
		//	UINT32 allocationCount;           // Count all allocated blocks 
		//};

		if(!sm_AcpHal)
			return;

		ApuHeapState apuHeapStateCached;
		ApuHeapState apuHeapStateUnCached;

		XSF_ERROR_IF_FAILED( ApuHeapGetState(&apuHeapStateCached, APU_ALLOC_CACHED ) );
		XSF_ERROR_IF_FAILED( ApuHeapGetState(&apuHeapStateUnCached, APU_ALLOC_NONCACHED ) );


		audDisplayf("apuHeapStateCached");																	
		audDisplayf("   bytesFree                 : %d", apuHeapStateCached.bytesFree);								
		audDisplayf("   bytesAllocated            : %d", apuHeapStateCached.bytesAllocated);						
		audDisplayf("   maximumBlockSizeAvailable : %d", apuHeapStateCached.maximumBlockSizeAvailable);				
		audDisplayf("   allocationCount           : %d", apuHeapStateCached.allocationCount);						
		audDisplayf("   bytesFree                 : %d", apuHeapStateCached.bytesFree);								
		audDisplayf("apuHeapStateUnCached");																		
		audDisplayf("   bytesFree                 : %d", apuHeapStateUnCached.bytesFree);							
		audDisplayf("   bytesAllocated            : %d", apuHeapStateUnCached.bytesAllocated);						
		audDisplayf("   maximumBlockSizeAvailable : %d", apuHeapStateUnCached.maximumBlockSizeAvailable);			
		audDisplayf("   allocationCount           : %d", apuHeapStateUnCached.allocationCount);						
		audDisplayf("   bytesFree                 : %d", apuHeapStateUnCached.bytesFree);							
		audDisplayf("Peek Messages                : %d", sm_PeekAudioMessages);										


		s32 count = 0;
		for(int i=0; i<kXBOneNumberXMAContexts; i++)
		{
			if(sm_ContextInfo[i].IsFree && sm_ContextInfo[i].State == ACP_CONTEXT_STATE_DISABLED && sm_ContextInfo[i].AudioFrame < sm_AudioFrameCount)
			{
				count++;
			}
		}
		audDisplayf("Active Contexts              : %d", count);



	}

	void audDecoderXboxOneXma::UpdateDecoderStats()
	{
		//struct ApuHeapState
		//{
		//	UINT32 bytesFree;                 // Size of all unallocated regions
		//	UINT32 bytesAllocated;            // Size of all allocated regions as visible to the title
		//	UINT32 bytesLost;                 // Size of all memory lost to alignment requests and fragmented blocks too small to allocate
		//	UINT32 maximumBlockSizeAvailable; // Largest block available (actual space available can be less due to alignment requirements)
		//	UINT32 allocationCount;           // Count all allocated blocks 
		//};

		if(!sm_AcpHal)
			return;
		
		ApuHeapState apuHeapStateCached;
		ApuHeapState apuHeapStateUnCached;

		XSF_ERROR_IF_FAILED( ApuHeapGetState(&apuHeapStateCached, APU_ALLOC_CACHED ) );
		XSF_ERROR_IF_FAILED( ApuHeapGetState(&apuHeapStateUnCached, APU_ALLOC_NONCACHED ) );

#define NEXT_LINE	grcDebugDraw::Text(Vector2(0.1f, lineBase), Color32(255,255,255), tempString ); lineBase += lineInc;

		char tempString[64];
		static bank_float lineInc = 0.015f;
		f32 lineBase = 0.1f;
		sprintf(tempString, "apuHeapStateCached");																	NEXT_LINE;
		sprintf(tempString, "   bytesFree                 : %d", apuHeapStateCached.bytesFree);						NEXT_LINE;		
		sprintf(tempString, "   bytesAllocated            : %d", apuHeapStateCached.bytesAllocated);				NEXT_LINE;		
		sprintf(tempString, "   maximumBlockSizeAvailable : %d", apuHeapStateCached.maximumBlockSizeAvailable);		NEXT_LINE;		
		sprintf(tempString, "   allocationCount           : %d", apuHeapStateCached.allocationCount);				NEXT_LINE;		
		sprintf(tempString, "   bytesFree                 : %d", apuHeapStateCached.bytesFree);						NEXT_LINE;		
		lineBase += lineInc;
		sprintf(tempString, "apuHeapStateUnCached");																NEXT_LINE;		
		sprintf(tempString, "   bytesFree                 : %d", apuHeapStateUnCached.bytesFree);					NEXT_LINE;		
		sprintf(tempString, "   bytesAllocated            : %d", apuHeapStateUnCached.bytesAllocated);				NEXT_LINE;		
		sprintf(tempString, "   maximumBlockSizeAvailable : %d", apuHeapStateUnCached.maximumBlockSizeAvailable);	NEXT_LINE;		
		sprintf(tempString, "   allocationCount           : %d", apuHeapStateUnCached.allocationCount);				NEXT_LINE;		
		sprintf(tempString, "   bytesFree                 : %d", apuHeapStateUnCached.bytesFree);					NEXT_LINE;		
		lineBase += lineInc;
		lineBase += lineInc;
		sprintf(tempString, "Peek Messages                : %d", sm_PeekAudioMessages);								NEXT_LINE;		


		s32 count = 0;
		for(int i=0; i<kXBOneNumberXMAContexts; i++)
		{
			if(sm_ContextInfo[i].IsFree && sm_ContextInfo[i].State == ACP_CONTEXT_STATE_DISABLED && sm_ContextInfo[i].AudioFrame < sm_AudioFrameCount)
			{
				count++;
			}
		}
		sprintf(tempString, "Active Contexts              : %d", count);											NEXT_LINE;


		
	}
#endif

	bool audDecoderXboxOneXma::Init(const audWaveFormat::audStreamFormat ASSERT_ONLY(format), const u32 numChannels, const u32 sampleRate)
	{
		audAssert(format == audWaveFormat::kXMA2);
		audAssert(numChannels == 1 || numChannels == 2);
		m_BytesSubmitted = 0;
		m_SamplesConsumed = 0;
		m_NumQueuedPackets = 0;
		m_DecodingPacketIndex = m_SubmittedPacketIndex = 0;
		m_WaveIdentifier = 0;
		m_NumChannels = numChannels;
		m_SampleRate = GetXmaSampleRate(sampleRate);
		m_PacketNumber = 0;
		m_StallCount = 0;
 
		m_ReadyForMoreData = true;
		m_State = audDecoder::IDLE;
		m_AbortPlayback = false;

		m_LastAudioFrame = 0;

		for(int i=0; i<2; i++)
		{
			m_Packets[i].InputBuffer=NULL;
			m_Packets[i].LoopStartBits=0;
			m_Packets[i].LoopSubframeSkip=0;
			m_Packets[i].LoopEndBits=0;
			m_Packets[i].LoopSubframeEnd=0;
			m_Packets[i].ContextIndex=-1;
			m_Packets[i].OutputBufferByteOffset=0;
			m_Packets[i].State=ACP_CONTEXT_STATE_DISABLED;
			m_Packets[i].InputNumBytes=0;
			m_Packets[i].PacketNumber=0;
			m_Packets[i].BytesConsumed=0;
			m_Packets[i].FinalBlock=false;
		}

#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS
		static int filenameCount = 0;
#endif
#if DUMP_RAW_XBOXONE_STREAMS
		char filename[256];
		sprintf(filename, "xboxone%d.raw", filenameCount);

		ASSET.CreateLeadingPath("filename");
		m_fp = ASSET.Create(filename, "");
#endif
#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS
		m_rawStreamIndex = filenameCount;
		filenameCount++;
		m_BytesConsumedLastUpdate = 0;
		m_BytesAvailLastUpdate = 0;
#endif

		return true;
	}

	//------------------------------------------------------------------------------
	//  Name:   DebugPrintAcpMessage
	//  Desc:   Display a message about an ACP error.
	//------------------------------------------------------------------------------

	void audDecoderXboxOneXma::DebugPrintAcpMessage( const ACP_MESSAGE& message )
	{
		switch( message.type)
		{
		case ACP_MESSAGE_TYPE_AUDIO_FRAME_START:
		//	audDisplayf("MESSAGE: AUDIO_FRAME_START: audio frame = %u\n", message.audioFrameStart.audioFrame);
			break;
		case ACP_MESSAGE_TYPE_FLOWGRAPH_COMPLETED:
			audDisplayf("MESSAGE: FLOWGRAPH_COMPLETED: flowgraph = 0x%p\n", message.flowgraphCompleted.flowgraph);
			break;
		case ACP_MESSAGE_TYPE_SRC_BLOCKED:
			audDisplayf("MESSAGE: SRC_BLOCKED: context = %u - ", message.shapeCommandBlocked.contextIndex);
			break;
		case ACP_MESSAGE_TYPE_DMA_BLOCKED:
			audDisplayf("MESSAGE: DMA_BLOCKED: context = %u - ", message.shapeCommandBlocked.contextIndex);
			break;
		case ACP_MESSAGE_TYPE_COMMAND_COMPLETED:
		//	audDisplayf("MESSAGE: COMMAND_COMPLETED: audioframe = %u, command = %u\n", message.commandCompleted.audioFrame, message.commandCompleted.commandId);
			break;
		case ACP_MESSAGE_TYPE_FLOWGRAPH_TERMINATED:
			audDisplayf("MESSAGE: FLOWGRAPH_TERMINATED: Completed = %u, reason = %s\n", message.flowgraphTerminated.numCommandsCompleted, 
				( message.flowgraphTerminated.reason == ACP_FLOWGRAPH_TERMINATED_REASON_INVALID_GRAPH) ? "ACP_FLOWGRAPH_TERMINATED_REASON_INVALID_GRAPH" : 
				( message.flowgraphTerminated.reason == ACP_FLOWGRAPH_TERMINATED_TIME_EXCEEDED) ? "ACP_FLOWGRAPH_TERMINATED_TIME_EXCEEDED" :
				( message.flowgraphTerminated.reason == ACP_FLOWGRAPH_TERMINATED_DISCONNECT) ? "ACP_FLOWGRAPH_TERMINATED_DISCONNECT" : 
				( message.flowgraphTerminated.reason == ACP_FLOWGRAPH_TERMINATED_REASON_INVALID_GRAPH) ? "ACP_FLOWGRAPH_TERMINATED_REASON_INVALID_GRAPH" :
				( message.flowgraphTerminated.reason == ACP_FLOWGRAPH_TERMINATED_TIME_EXCEEDED) ? "ACP_FLOWGRAPH_TERMINATED_TIME_EXCEEDED" : "UNKNOWN");
			break;
		case ACP_MESSAGE_TYPE_ERROR:
			audDisplayf("MESSAGE: ERROR: errorCode = 0x%x, additionalData = %x\n", message.error.errorCode, message.error.additionalData);
			break;
		case ACP_MESSAGE_TYPE_DISCONNECTED:
			audDisplayf("MESSAGE: DISCONNECTED\n");
			break;
		default:
			audDisplayf("MESSAGE: *** ERROR: UNKNWON MESSAGE ***\n");
			break;
		}
	}

	//------------------------------------------------------------------------------
	//  Name:   MessageQueueThreadProc
	//  Desc:   Message queue thread function
	//------------------------------------------------------------------------------
	DWORD WINAPI audDecoderXboxOneXma::MessageQueueThreadProc(__in  LPVOID UNUSED_PARAM(lpParameter))
	{
		return MessageQueueThreadProc();
	}

	DWORD audDecoderXboxOneXma::MessageQueueThreadProc()
	{
		InterlockedIncrement( &m_NumThreadsRunning );

		for ( ; ; )
		{
			DWORD   waitResult = WaitForSingleObjectEx( m_TerminateThreads, m_MessageQueueThreadSleep, TRUE );

			//  Wait completed because termination event was raised.
			if ( waitResult == WAIT_OBJECT_0 )
			{
				break;
			}

			{
				//XSFScopedNamedEvent( NULL, PIX_COLOR_INDEX(4), L"MessageQueue Processing" );

#if __BANK
				u32 numMssgaes = sm_AcpHal->GetNumMessages();
				if(numMssgaes > sm_PeekAudioMessages)
				{
					sm_PeekAudioMessages = numMssgaes;
				}
#endif

				// Read all messages
				ACP_MESSAGE message;

				while( sm_AcpHal->PopMessage( &message ) )
				{
					DebugPrintAcpMessage( message);
					if( message.type == ACP_MESSAGE_TYPE_DISCONNECTED )
					{
						m_Disconnected = true;
					}
					else if( message.type == ACP_MESSAGE_TYPE_AUDIO_FRAME_START )
					{
						sm_AudioFrameCount = message.audioFrameStart.audioFrame;
					}
					//else if( ( message.type == ACP_MESSAGE_TYPE_FLOWGRAPH_TERMINATED ) && 
					//	( message.flowgraphTerminated.reason == ACP_FLOWGRAPH_TERMINATED_REASON_INVALID_GRAPH ) )
					//{
					//	sm_AcpHal->Disconnect();
					//}
				}
			}
		}

		InterlockedDecrement( &m_NumThreadsRunning );
		return 0;
	}


	void audDecoderXboxOneXma::Shutdown()
	{
		for(s32 i = 0; i < m_Packets.GetMaxCount(); i++)
		{
			if(m_Packets[i].ContextIndex >= 0)
			{
				DisableXmaContext(m_Packets[i].ContextIndex);
				FreeXmaContextIndex(m_Packets[i].ContextIndex);
			}

			m_Packets[i].ContextIndex = -1;
			m_Packets[i].State = ACP_CONTEXT_STATE_DISABLED;
		}
#if DUMP_RAW_XBOXONE_STREAMS
		if(m_fp)
		{
			m_fp->Close();
		}
#endif
	}

	void audDecoderXboxOneXma::AllocateFromAPUMemory(u8** address, u32 dataSize)
	{
		XSF_ERROR_IF_FAILED( audDecoderXboxOneXma::ApuVirtualAllocate( (void**)address, dataSize ) );
	}

	u32 audDecoderXboxOneXma::ComputeNumSamplesInBuffer(const u8 *buffer, const u32 bufferLength)
	{
		u32 byteOffset = 0;
		u32 numSamples = 0;
		while(byteOffset < bufferLength)
		{
			u32 metadata = GetXmaPacketMetadata(buffer+byteOffset);
			if(audVerifyf(metadata == 1, "%u: Invalid XMA2 data; expected 1, got %u at buffer %p offset %u", m_WaveIdentifier, metadata, buffer, byteOffset))
			{
				const u32 packetFrameCount = GetXmaPacketFrameCount(buffer+byteOffset);
				const u32 skip = 1+GetXmaPacketSkipCount(buffer+byteOffset);

				numSamples += XMA_SAMPLES_PER_FRAME * packetFrameCount;
				byteOffset += skip*2048U;
			}
			else
			{
				// Failure case - prevent invalid memory access by giving up when we encounter bad data.
				return 0;
			}
		}
		return numSamples;
	}

	u32 audDecoderXboxOneXma::ComputeBytesToSkip(const u8 *buffer, const u32 bufferLengthBytes, const u32 samplesToSkip, u32 &samplesSkipped)
	{
		u32 byteOffset = 0;
		u32 numSamples = 0;
		while(byteOffset < bufferLengthBytes)
		{
			u32 metadata = GetXmaPacketMetadata(buffer+byteOffset);
			if(audVerifyf(metadata == 1, "%u: Invalid XMA2 data; expected 1, got %u (%p)", m_WaveIdentifier, metadata, buffer+byteOffset))
			{
				const u32 packetFrameCount = GetXmaPacketFrameCount(buffer+byteOffset);
				const u32 skip = 1+GetXmaPacketSkipCount(buffer+byteOffset);
	
				samplesSkipped = numSamples;
				numSamples += XMA_SAMPLES_PER_FRAME * packetFrameCount;
				if(numSamples > samplesToSkip)
				{
					return byteOffset;
				}
				byteOffset += skip*2048U;
			}
			else
			{
				// Failure case - prevent invalid memory access by giving up when we encounter bad data.
				return 0;
			}
		}
		return byteOffset;
	}

	void audDecoderXboxOneXma::ComputeLoopData(const audPacket &packet, audXmaPacketInfo &xmaPacketInfo)
	{
		GetXmaDecodePositionForSample((const BYTE*)packet.inputData, packet.inputBytes, 0, packet.LoopBegin, (DWORD*)&xmaPacketInfo.LoopStartBits, (DWORD*)&xmaPacketInfo.LoopSubframeSkip);
		GetXmaDecodePositionForSample((const BYTE*)packet.inputData, packet.inputBytes, 0, packet.LoopEnd, (DWORD*)&xmaPacketInfo.LoopEndBits, (DWORD*)&xmaPacketInfo.LoopSubframeEnd);

		if(xmaPacketInfo.LoopEndBits == 0)
		{
			const u32 lastBit = (u32)GetLastXmaFrameBitPosition((const BYTE*)packet.inputData, packet.inputBytes, 0);
			xmaPacketInfo.LoopEndBits = lastBit;
		}	
	}

	void audDecoderXboxOneXma::SubmitPacket(rage::audPacket &packet)
	{
		audAssert(!(((size_t)packet.inputData)&2047));
		audAssert(!(packet.inputBytes&2047));
		audAssert(m_ReadyForMoreData);

		audXmaPacketInfo &submittedPacket = m_Packets[m_SubmittedPacketIndex];
		submittedPacket.PacketNumber = m_PacketNumber;
#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS
		//Displayf("SubmitPacket : %d : File %d", m_PacketNumber, m_rawStreamIndex);
		submittedPacket.StreamFileNumber = m_rawStreamIndex;
#endif
		m_PacketNumber++;


		submittedPacket.OutputBufferByteOffset = 0;

		audAssert(submittedPacket.State == ACP_CONTEXT_STATE_DISABLED);
#if DUMP_DEBUG_XBOXONE_STREAMS
		if(submittedPacket.State != ACP_CONTEXT_STATE_DISABLED)
		{
			Displayf("State = %d", submittedPacket.State);
		}
#endif

		// Not sure what to do about this, think it might be ok here
		//if(packet.PlayBegin && packet.PlayBegin == packet.PlayEnd)
		//{
		//	// Entire packet is to be skipped
		//	return;
		//}

		submittedPacket.FinalBlock = packet.IsFinalBlock;
		
		// We might already have a valid context if this packet has been used previously
		if(submittedPacket.ContextIndex == -1)
		{
			submittedPacket.ContextIndex = GetFreeXmaContextIndex();
			audAssert(submittedPacket.ContextIndex != -1);
			if(submittedPacket.ContextIndex == -1)
			{
#if __BANK
				static bool haveDoneDebugPrint = false;
				if(!haveDoneDebugPrint)
				{
					haveDoneDebugPrint = true;
					PrintDecoderStats();
					PrintUsedXmaContexts();
				}
#endif
				audAssertf(0, "Failed to allocate XMA decoder context");
				audErrorf("Failed to allocate XMA decoder context");
				m_State = audDecoderState::DECODER_ERROR;
				return;
			}
		}

		s32 index = submittedPacket.ContextIndex;
		//Displayf("Free context index %d", index);

		BANK_ONLY(sm_ContextInfo[index].WaveNameHash = packet.WaveNameHash;)

		ZeroMemory( &sm_Contexts.xmaContextArray[index], sizeof(SHAPE_XMA_CONTEXT) );

		XSF_ERROR_IF_FAILED( SetShapeXmaNumChannels(&sm_Contexts.xmaContextArray[index], m_NumChannels) );
		XSF_ERROR_IF_FAILED( SetShapeXmaSampleRate(&sm_Contexts.xmaContextArray[index], m_SampleRate) );
		XSF_ERROR_IF_FAILED( SetShapeXmaNumSubframesToDecode(&sm_Contexts.xmaContextArray[index], 4) );
		XSF_ERROR_IF_FAILED( SetShapeXmaOutputBufferSize(&sm_Contexts.xmaContextArray[index], kXBOneXMAHalOutputBufferBytes) ); // on xbox360, this was in samples (12*128)

		XSF_ERROR_IF_FAILED( SetShapeXmaOutputBuffer(&sm_Contexts.xmaContextArray[index], sm_ContextInfo[index].DecodedOutputBuffer) );
		XSF_ERROR_IF_FAILED( SetShapeXmaOutputBufferValid(&sm_Contexts.xmaContextArray[index], true) );
		XSF_ERROR_IF_FAILED( SetShapeXmaOverlapAddBuffer(&sm_Contexts.xmaContextArray[index], sm_ContextInfo[index].OutputOverlapBuffer) );


		//int index = submittedPacket.ContextIndex;
		u32 dataSize = packet.inputBytes;

		// Since the audio data has to be in a buffer allocated by ApuAlloc we're going to cheat for now and copy the waveslot data into 
		// a buffer specifically for the decode. Hopefully we can just allocate the waveslots with ApuAlloc to solve this.

		submittedPacket.InputBuffer = (void*)packet.inputData; // we will need this to update the context once we switch packets
		submittedPacket.InputNumBytes = packet.inputBytes;

		const u8 *data = (const u8*)packet.inputData;
		
		u32 skippedSamples = 0;

		// If we're looping then we need to submit the entire packet to the hardware, even if we're
		// going to skip some of it below
		if(packet.PlayBegin && packet.LoopEnd == 0)
		{		
			u32 skipBytes = ComputeBytesToSkip(data, dataSize, packet.PlayBegin, skippedSamples);
			data += skipBytes;
			dataSize -= skipBytes;
		}

		//audAssertf(packet.PlayEnd == 0 || packet.PlayEnd > packet.PlayBegin, "PlayBegin: %u PlayEnd: %u", packet.PlayBegin, packet.PlayEnd);

		// Set up hardware looping if required
		bool hasLoop = false;
		if(packet.LoopEnd != 0)
		{
			hasLoop = true;
			ComputeLoopData(packet, submittedPacket);
		
			// Populate struct for XMA HAL
			XSF_ERROR_IF_FAILED( SetShapeXmaLoopStartOffset(&sm_Contexts.xmaContextArray[index], submittedPacket.LoopStartBits) );
			XSF_ERROR_IF_FAILED( SetShapeXmaLoopEndOffset(&sm_Contexts.xmaContextArray[index], submittedPacket.LoopEndBits ) );
			XSF_ERROR_IF_FAILED( SetShapeXmaLoopSubframeEnd(&sm_Contexts.xmaContextArray[index], submittedPacket.LoopSubframeEnd) );
			XSF_ERROR_IF_FAILED( SetShapeXmaLoopSubframeSkip(&sm_Contexts.xmaContextArray[index], submittedPacket.LoopSubframeSkip) );
			XSF_ERROR_IF_FAILED( SetShapeXmaNumLoops(&sm_Contexts.xmaContextArray[index], 255) );
		}
		else
		{
			submittedPacket.LoopStartBits = submittedPacket.LoopEndBits = 0;
			submittedPacket.LoopSubframeSkip = submittedPacket.LoopSubframeEnd = 0;
		}

		submittedPacket.NumSamples = ComputeNumSamplesInBuffer(data, dataSize);

		// This fucked me up for ages. In the old code the start offset bits was 0, now it has to be at least 32.
		// Found it in Remarks for SetShapeXmaInputBufferReadOffsetNumSamples
		// "The input buffer read offset for XMA contexts should normally be initialized to 32 for the hardware XMA decoder (or 32 plus some multiple of 2048 bytes)."
		// What I don't get is if the input buffer has multiple packets does that mean I have to feed each packet individually while always having this offset?
		// If not then why do I have to have it on the first packet decode and none after?
		u32 bitOffset = 32, numSubframesToSkip = 0;

		// Compute PlayBegin in terms of whole 512 sample frames, 128 sample subframes and a remainder; 
		// the hardware can skip entire subframes more efficiently than we can.
		u32 numSamplesToSkipAtStart = packet.PlayBegin - skippedSamples;
		if(numSamplesToSkipAtStart >= 128)
		{
			// Leave 128 samples to skip in software, to avoid the ramp up due to hw windowing
			numSamplesToSkipAtStart -= 128;
		}
			
		const u32 numFramesToSkip = numSamplesToSkipAtStart >> 9;
		const u32 numSamplesSkippedInWholeFrames = numFramesToSkip<<9;
		numSubframesToSkip = (numSamplesToSkipAtStart - numSamplesSkippedInWholeFrames) >> 7;
		const u32 numSamplesSkippedInWholeSubframes = numSubframesToSkip << 7;
		const u32 numSamplesSkippedByHardware = numSamplesSkippedInWholeFrames + numSamplesSkippedInWholeSubframes;
	
		submittedPacket.PlayBegin = packet.PlayBegin - skippedSamples;

		if(numFramesToSkip)
		{
			submittedPacket.NumSamplesConsumed = numSamplesSkippedByHardware;
			bitOffset = GetXmaFrameBitPosition(data, dataSize, 0, numFramesToSkip);
		}
		else
		{
			submittedPacket.NumSamplesConsumed = numSamplesSkippedInWholeSubframes;
		}

		submittedPacket.PlayEnd = packet.PlayEnd ? packet.PlayEnd-skippedSamples : submittedPacket.NumSamples;		
	
		m_SubmittedPacketIndex = (m_SubmittedPacketIndex + 1) % m_Packets.size();

		if(m_SubmittedPacketIndex == m_DecodingPacketIndex)
		{
			m_ReadyForMoreData = false;
		}

#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS
		Displayf("     Pack: %d Stream: %d   Context: %d   S: %d   PB: %d -> %d   PE: %d -> %d   skip: %d   bytes: %d",
				submittedPacket.PacketNumber,
				submittedPacket.StreamFileNumber,
				submittedPacket.ContextIndex,
				submittedPacket.NumSamples,
				packet.PlayBegin, submittedPacket.PlayBegin, 
				packet.PlayEnd, submittedPacket.PlayEnd,
				skippedSamples,
				packet.inputBytes);
#endif

		XSF_ERROR_IF_FAILED( SetShapeXmaCurrentInputBuffer(&sm_Contexts.xmaContextArray[index], 0) );
		XSF_ERROR_IF_FAILED( SetShapeXmaInputBuffer0Size(&sm_Contexts.xmaContextArray[index], dataSize) );
		XSF_ERROR_IF_FAILED( SetShapeXmaInputBuffer0(&sm_Contexts.xmaContextArray[index], data) );
		XSF_ERROR_IF_FAILED( SetShapeXmaInputBuffer1Size(&sm_Contexts.xmaContextArray[index], 0) );
		XSF_ERROR_IF_FAILED( SetShapeXmaInputBuffer1(&sm_Contexts.xmaContextArray[index], data) ); // doc say to do this even though not using the buffer

		XSF_ERROR_IF_FAILED( SetShapeXmaInputBuffer0Valid(&sm_Contexts.xmaContextArray[index], true) );
		XSF_ERROR_IF_FAILED( SetShapeXmaInputBuffer1Valid(&sm_Contexts.xmaContextArray[index], false) ); 
		XSF_ERROR_IF_FAILED( SetShapeXmaOutputBufferValid(&sm_Contexts.xmaContextArray[index], true ) );

		XSF_ERROR_IF_FAILED( SetShapeXmaInputBufferReadOffset(&sm_Contexts.xmaContextArray[index], bitOffset) );
		XSF_ERROR_IF_FAILED( SetShapeXmaNumSubframesToSkip(&sm_Contexts.xmaContextArray[index], numSubframesToSkip) );
		
		m_StallCount = 0;

		m_NumQueuedPackets++;
		m_BytesSubmitted += packet.inputBytes;
		m_State = audDecoder::DECODING;
	}

	u32 audDecoderXboxOneXma::QueryNumAvailableSamples() const
	{
		return (m_RingBuffer.GetBytesAvailableToRead()>>1);
	}

	u32 audDecoderXboxOneXma::ReadData(s16 *dest, const u32 numSamples) const
	{
		return m_RingBuffer.PeakData(dest, numSamples<<1)>>1;
	} 

	void audDecoderXboxOneXma::AdvanceReadPtr(const u32 numSamples)
	{
		m_RingBuffer.AdvanceReadPtr(numSamples<<1);
		m_SamplesConsumed += numSamples;
	}

	void audDecoderXboxOneXma::ClearBuffer()
	{
		m_RingBuffer.Reset();
		// Reset to allow skipping into the next packet
		m_BytesSubmitted = 0;
		// TODO: should we also flush the hardware?
	}

	void audDecoderXboxOneXma::PreUpdate()
	{
	}

	void audDecoderXboxOneXma::Update()
	{
		XMA_PF_FUNC(DecoderXmaXboneUpdate);

		if(m_State == audDecoderState::FINISHED)
			return;

		audXmaPacketInfo &packet = m_Packets[m_DecodingPacketIndex];

		if((packet.LoopEndBits == 0 && packet.NumSamplesConsumed >= packet.PlayEnd) || m_AbortPlayback)
		{
			m_AbortPlayback = false;

			DisableXmaContext(packet.ContextIndex);
			packet.State = ACP_CONTEXT_STATE_DISABLED;

			// move onto next packet
			if(m_NumQueuedPackets > 0)
				m_NumQueuedPackets--;

			if(m_NumQueuedPackets > 0 && m_State != audDecoderState::FINISHED)
			{
				// more packets in queue, proceed
				m_DecodingPacketIndex = (m_DecodingPacketIndex+1) % m_Packets.size();
				m_ReadyForMoreData = ((m_DecodingPacketIndex!=m_SubmittedPacketIndex) || m_State != audDecoderState::DECODING);	
			}
			else
			{
				if(!packet.FinalBlock && packet.ContextIndex != -1) 
				{
					m_StallCount++;
					if(m_StallCount < 2000)
					{
						//maybe hit a starvation case, request more data for at least 10 seconds
						m_ReadyForMoreData = ((m_DecodingPacketIndex!=m_SubmittedPacketIndex) || m_State != audDecoderState::DECODING);
						return;
					}
#if __BANK
					else
					{
						Warningf("Audio streaming stalled due to file loading");
					}
#endif
				}
				m_State = audDecoderState::FINISHED;
				m_DecodingPacketIndex = m_SubmittedPacketIndex = 0;
				m_ReadyForMoreData = true;
			}
			return;
		}

		if(packet.State == ACP_CONTEXT_STATE_DISABLED)
		{
			// once the SHAPE_XMA_CONTEXT is populated a submit using ACP_COMMAND_TYPE_ENABLE_XMA_CONTEXT starts the decode
			EnableXmaContext(packet.ContextIndex);
			packet.State = ACP_CONTEXT_STATE_ENABLE;
			return;
		}
		
		// If decodingPacket == submittedPacket then we need to wait before submitting new data
		m_ReadyForMoreData = ((m_DecodingPacketIndex!=m_SubmittedPacketIndex) || m_State != audDecoderState::DECODING);	

		// No point going any futher; we don't have any space to read from the hardware
		if(m_RingBuffer.IsFull() || m_LastAudioFrame >= sm_AudioFrameCount)
		{
			return;
		}

		int index = m_Packets[m_DecodingPacketIndex].ContextIndex;
		u32 bytesAvailable = GetShapeXmaOutputBufferNumBytesAvailable(&sm_Contexts.xmaContextArray[index]); 

#if DUMP_DEBUG_XBOXONE_STREAMS
		if(bytesAvailable == 0)
		{
			Displayf("BytesAvailable %d index %d  %d - %d = %d", 
				bytesAvailable, m_rawStreamIndex, packet.PlayEnd, packet.NumSamplesConsumed, packet.PlayEnd - packet.NumSamplesConsumed);
		}
#endif
	
		//u32 samplesDecoded = 0;

		if(bytesAvailable > 0 && m_State != audDecoder::FINISHED)
		{
			u32 bytesConsumed = 0;

			u32 outputReadOffset = GetShapeXmaOutputBufferReadOffset(&sm_Contexts.xmaContextArray[index]);

#if DUMP_DEBUG_XBOXONE_STREAMS
			if(outputReadOffset != packet.OutputBufferByteOffset)
			{
				Displayf("Offset missmatch %d != %d   new_bytes_avail %d   stream %d   bytesConsumed %d   bytesAvail %d   context %d", outputReadOffset, packet.OutputBufferByteOffset, bytesAvailable, m_rawStreamIndex, m_BytesConsumedLastUpdate, m_BytesAvailLastUpdate, index);
			}
#endif

			if(bytesAvailable)
			{				
				const u32 ringBufferFreeSpace = (m_RingBuffer.GetBufferSize()-m_RingBuffer.GetBytesAvailableToRead());// >> 1;
				if(ringBufferFreeSpace > bytesAvailable)
				{

					//Displayf("outputReadOffset %d %d %d", outputReadOffset, packet.OutputBufferByteOffset, m_rawStreamIndex);
					packet.OutputBufferByteOffset = outputReadOffset;

					u32 bytesToConsume = bytesAvailable;

					// skip any extra bytes that weren't skipped by the hardware to get us to the correct offset
					if(packet.NumSamplesConsumed < packet.PlayBegin)
					{	
						u32 bytesToSkip = Min(bytesAvailable, (packet.PlayBegin - packet.NumSamplesConsumed)*2);
						if(packet.OutputBufferByteOffset + bytesToSkip > kXBOneXMAHalOutputBufferBytes)// && packet.NumSamplesConsumed < packet.PlayEnd)
						{
							u32 bytesAtEndToSkip = kXBOneXMAHalOutputBufferBytes - packet.OutputBufferByteOffset;
							packet.OutputBufferByteOffset = 0;
							bytesToSkip -= bytesAtEndToSkip;
							bytesToConsume -= bytesAtEndToSkip;
							bytesConsumed += bytesAtEndToSkip;
						}

						if(bytesToSkip > 0)
						{
							packet.OutputBufferByteOffset += bytesToSkip;
							bytesToConsume -= bytesToSkip;
							bytesConsumed += bytesToSkip;
						}
					}


					// see if we have some data at end to copy first
					u32 numSamplesConsumed = packet.NumSamplesConsumed + (bytesConsumed/2);
					if(bytesToConsume > 0 && packet.OutputBufferByteOffset + bytesToConsume > kXBOneXMAHalOutputBufferBytes && (packet.LoopEndBits != 0 || numSamplesConsumed < packet.PlayEnd))
					{
						u32 bytesAtEndToCopy = kXBOneXMAHalOutputBufferBytes - packet.OutputBufferByteOffset;
						u32 bytesToCopy = bytesAtEndToCopy;
						u8* outputBufferReadOffset = (u8*)GetShapeXmaOutputBuffer(&sm_Contexts.xmaContextArray[index]) + packet.OutputBufferByteOffset;
						// we need to make sure we trim to packet.PlayEnd
						if(packet.LoopEndBits == 0 && numSamplesConsumed+(bytesAtEndToCopy/2) > packet.PlayEnd)
						{
							bytesToCopy = (packet.PlayEnd - numSamplesConsumed) * 2;
						}
						if(outputBufferReadOffset)
							m_RingBuffer.WriteData(outputBufferReadOffset, bytesToCopy);

						bytesToConsume -= bytesAtEndToCopy;
						bytesConsumed += bytesAtEndToCopy;
						packet.OutputBufferByteOffset = 0;
#if DUMP_RAW_XBOXONE_STREAMS
						if(m_fp)
						{
							s16* sample = (s16*)outputBufferReadOffset;
							for(u32 i=0; i<bytesToCopy/2; i++)
							{
								m_fp->Write(sample, 2);
								sample++;
							}
						}
#endif					
					}

					numSamplesConsumed = packet.NumSamplesConsumed + (bytesConsumed/2);
					if(bytesToConsume > 0 && (numSamplesConsumed < packet.PlayEnd || packet.LoopEndBits != 0))
					{
						u32 bytesToCopy = bytesToConsume;
						u8* outputBufferReadOffset = (u8*)GetShapeXmaOutputBuffer(&sm_Contexts.xmaContextArray[index]) + packet.OutputBufferByteOffset;
						// we need to make sure we trim to packet.PlayEnd
						if(packet.LoopEndBits == 0 && numSamplesConsumed+(bytesToCopy/2) > packet.PlayEnd)
						{
							bytesToCopy = (packet.PlayEnd - numSamplesConsumed) * 2;
						}
						if(outputBufferReadOffset)
							m_RingBuffer.WriteData(outputBufferReadOffset, bytesToCopy);

						bytesConsumed += bytesToConsume;
						packet.OutputBufferByteOffset += bytesToConsume;
						if(packet.OutputBufferByteOffset >= kXBOneXMAHalOutputBufferBytes)
						{
							packet.OutputBufferByteOffset = 0;
						}

#if DUMP_RAW_XBOXONE_STREAMS
						if(m_fp)
						{
							s16* sample = (s16*)outputBufferReadOffset;
							for(u32 i=0; i<bytesToCopy/2; i++)
							{
								m_fp->Write(sample, 2);
								sample++;
							}
						}
#endif
					}

#if DUMP_DEBUG_XBOXONE_STREAMS
					m_BytesConsumedLastUpdate = bytesConsumed;
					m_BytesAvailLastUpdate = bytesAvailable;
#endif

					//samplesDecoded += (bytesConsumed / 2);
					bytesAvailable -= bytesConsumed;
					packet.NumSamplesConsumed += (bytesConsumed / 2);
					packet.BytesConsumed += bytesConsumed;

//#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS
//					Displayf("File %d, Consumed %d of %d, Bytes %d of %d", 
//						packet.StreamFileNumber, 
//						packet.NumSamplesConsumed, 
//						packet.NumSamples, 
//						GetShapeXmaInputBufferReadOffset(&sm_Contexts.xmaContextArray[index])/ SHAPE_XMA_OUTPUT_BUFFER_READ_OFFSET_BITS, 
//						packet.InputNumBytes);
//#endif
					m_LastAudioFrame = sm_AudioFrameCount;
					ACP_COMMAND_INCREMENT_XMA_WRITE_BUFFER_OFFSET_READ command = 
					{
						index, (bytesConsumed >> SHAPE_XMA_OUTPUT_BUFFER_READ_OFFSET_BITS)
					};
					XSF_ERROR_IF_FAILED( sm_AcpHal->SubmitCommand( ACP_COMMAND_TYPE_INCREMENT_XMA_WRITE_BUFFER_OFFSET_READ, 0, ACP_SUBMIT_PROCESS_COMMAND_ASAP, &command ) );
				}
			}
		}


#define AUD_XMA_CHECK_ERRORS (RSG_ASSERT && !__NO_OUTPUT)
#if AUD_XMA_CHECK_ERRORS && 0
		const bool hasErrors = GetShapeXmaErrorSet(&sm_Contexts.xmaContextArray[index]);
		const u32 errorStatus = GetShapeXmaErrorStatus(&sm_Contexts.xmaContextArray[index]);

		// according to MS Game Developer Support errorBits fires spuriously, and the only one we should look 
		// out for is INTO_INVALID_READ_BUFFER.
		// parseErrors are reliable though.
		if(hasErrors)
		{
			//m_State = audDecoder::DECODER_ERROR;
			switch(errorStatus)
			{
			//case SHAPE_XMA_ERROR_STATUS_NO_ERROR_OCCURRED_ERRORSET_IS_0:
			//	audErrorf("XMA Error %s", "SHAPE_XMA_ERROR_STATUS_NO_ERROR_OCCURRED_ERRORSET_IS_0");
			//	break;
			case SHAPE_XMA_ERROR_STATUS_WRITE_BUFFER_INVALID_VALIDWR_IS_0:
				audErrorf("XMA Error %s", "SHAPE_XMA_ERROR_STATUS_WRITE_BUFFER_INVALID_VALIDWR_IS_0");
				break;
			//case SHAPE_XMA_ERROR_STATUS_INSUFFICIENT_WRITE_BUFFER_SPACE_VALIDWR_IS_1:
			//	audErrorf("XMA Error %s", "SHAPE_XMA_ERROR_STATUS_INSUFFICIENT_WRITE_BUFFER_SPACE_VALIDWR_IS_1");
				break;
			case SHAPE_XMA_ERROR_STATUS_READ_BUFFER_INVALID_VALIDBUFFER_CURRBUF_IS_0:
				audErrorf("XMA Error %s", "SHAPE_XMA_ERROR_STATUS_READ_BUFFER_INVALID_VALIDBUFFER_CURRBUF_IS_0");
				break;
			case SHAPE_XMA_ERROR_STATUS_FRAME_CROSSES_BOUNDARY_INTO_INVALID_READ_BUFFER_VALIDBUFFER_CURRBUF_IS_0:
				audErrorf("XMA Error %s", "SHAPE_XMA_ERROR_STATUS_FRAME_CROSSES_BOUNDARY_INTO_INVALID_READ_BUFFER_VALIDBUFFER_CURRBUF_IS_0");
				break;
			case SHAPE_XMA_ERROR_STATUS_FRAME_CROSSES_BOTH_READ_BUFFER_BOUNDARIES:
				audErrorf("XMA Error %s", "SHAPE_XMA_ERROR_STATUS_FRAME_CROSSES_BOTH_READ_BUFFER_BOUNDARIES");
				break;
			}
			if(errorStatus && errorStatus != SHAPE_XMA_ERROR_STATUS_INSUFFICIENT_WRITE_BUFFER_SPACE_VALIDWR_IS_1)
			{
				audErrorf("XMA Error: %u, had consumed %u / %u samples from decoding packet %u (Begin: %u, End: %u).  %u samples in total consumed", 
					errorStatus, 
					m_Packets[m_DecodingPacketIndex].NumSamplesConsumed,
					m_Packets[m_DecodingPacketIndex].NumSamples,
					m_DecodingPacketIndex,
					m_Packets[m_DecodingPacketIndex].PlayBegin,
					m_Packets[m_DecodingPacketIndex].PlayEnd,
					m_SamplesConsumed);
			}
		}
		bool parserError = GetShapeXmaParserErrorSet(&sm_Contexts.xmaContextArray[index]);
		if(parserError)
		{
			UINT32 parserErrorNo = GetShapeXmaParserErrorStatus(&sm_Contexts.xmaContextArray[index]);
			if(parserErrorNo > 0)
			{
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_UNKNOWN_STATE )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_UNKNOWN_STATE \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_OUT_OF_RANGE )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_OUT_OF_RANGE \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_FRAME_SIZE_EXCEEDED )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_FRAME_SIZE_EXCEEDED \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_BITSTREAM_CORRUPTION )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_BITSTREAM_CORRUPTION \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_BARKVLC_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_BARKVLC_ERROR  \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_BARKRLC_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_BARKRLC_ERROR  \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_VLC4D_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_VLC4D_ERROR  \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_VLC2D_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_VLC2D_ERROR  \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_VLC1D_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_VLC1D_ERROR  \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_RLC0_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_RLC0_ERROR  \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_RLC1_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_RLC1_ERROR  \n");
				}
				if(parserErrorNo & SHAPE_XMA_PARSER_ERROR_STATUS_LARGEVAL24_ERROR  )
				{
					audErrorf("XMA Error SHAPE_XMA_PARSER_ERROR_STATUS_LARGEVAL24_ERROR  \n");
				}
			}
		}
		//if(errorBits == XMA_PLAYBACK_ERROR_FRAME_CROSSES_BOUNDARY_INTO_INVALID_READ_BUFFER
		//	|| parseError != XMA_PLAYBACK_PARSE_ERROR_NONE)
		//{
		//	m_State = audDecoder::DECODER_ERROR;
		//	audErrorf("XMA Error: %u:%u, had consumed %u / %u samples from decoding packet %u (Begin: %u, End: %u, Playback: %p).  %u samples in total consumed", errorBits, parseError, 
		//		m_Packets[m_DecodingPacketIndex].NumSamplesConsumed,
		//		m_Packets[m_DecodingPacketIndex].numSamples,
		//		m_DecodingPacketIndex,
		//		m_Packets[m_DecodingPacketIndex].PlayBegin,
		//		m_Packets[m_DecodingPacketIndex].PlayEnd,
		//		m_Packets[m_DecodingPacketIndex].Playback,
		//		m_SamplesConsumed);
		//}
#endif

	}

} // namespace rage

#endif // RSG_DURANGO
