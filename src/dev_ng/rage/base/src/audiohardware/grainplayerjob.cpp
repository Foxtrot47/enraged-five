// 
// audiohardware/grainplayerjob.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "system/taskheader.h"

#if __SPU

#include "grainplayer.h"
#include "grainplayerjob.h"

#include "audiohardware/framebuffercache.h"
#include "audiohardware/mixer.h"
#include "audiohardware/syncsource.cpp"
#include "audiohardware/pcmsource.h"
#include "audiohardware/pcmsourcejob.h"
#include "audioengine/spuutil.h"

#include "math/amath.h"
#include "system/dma.h"

namespace rage 
{
	void *g_Mp3DecoderMain = NULL;
	void *g_Mp3DecoderWorkspace = NULL;

	extern u8 *g_ScratchDecoders[3];
	extern u8 *g_ScratchMixers[audGrainPlayer::kMaxGranularMixersPerPlayer];
	extern u8 *g_ScratchSubmixes[3];
	extern u8 *g_ScratchMixBuffer;
	extern u8 *g_ScratchSampleGathererBuffer;
	extern u8* g_ScratchCrossfadeBuffer;

	void grainplayerjobentry(sysTaskParameters &p)
	{
		InitScratchBuffer(p.Scratch.Data, p.Scratch.Size); 

		// Hook up MP3 decoder state
		g_Mp3DecoderMain = p.UserData[4].asPtr;

		// Allocate MP3 decoder workspace		
		g_Mp3DecoderWorkspace = AllocateFromScratch(CELL_MSMP3_SPU_WORKSPACE_SIZE, 128, "MP3 Decoder workspace");

		// Granular mixing objects
		for(u32 loop = 0; loop < 3; loop++)
		{
			g_ScratchDecoders[loop] = (u8*)AllocateFromScratch(sizeof(audDecoderSpu), 128, "ScratchDecoder");
		}

		for(u32 loop = 0; loop < audGrainPlayer::kMaxGranularMixersPerPlayer; loop++)
		{
			g_ScratchMixers[loop] = (u8*)AllocateFromScratch(sizeof(audGranularMix), 16, "ScratchGranularMix");
		}

		for(u32 loop = 0; loop < 3; loop++)
		{
			g_ScratchSubmixes[loop] = (u8*)AllocateFromScratch(sizeof(audGranularSubmix), 16, "ScratchGranularSubmix"); 
		}

		g_ScratchMixBuffer = (u8*) AllocateFromScratch(sizeof(f32) * audGranularSubmix::s_MixBufferLength, 128, "ScratchMixBuffer");
		g_ScratchSampleGathererBuffer = (u8*) AllocateFromScratch(sizeof(s16) * audGranularSubmix::s_SampleGathererBufferLength, 128, "ScratchSampleDataGatherBuffer");
		g_ScratchCrossfadeBuffer = (u8*)AllocateFromScratch(sizeof(s16) * 512, 16, "ScratchGranularCrossfadeBuffer"); 

		enum {kNumGrainPlayerBuffers = 2};
		ProcessPcmSourcePool<audGrainPlayer, AUD_PCMSOURCE_GRAINPLAYER>(p, kNumGrainPlayerBuffers);
	}
} // namespace rage

#endif // __SPU

using namespace rage;
void grainplayerjob(sysTaskParameters &SPU_ONLY(p))
{
	SPU_ONLY(rage::grainplayerjobentry(p));
}
