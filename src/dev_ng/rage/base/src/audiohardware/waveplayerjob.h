// 
// audiohardware/waveplayerjob.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_WAVEPLAYERJOB_H
#define AUD_WAVEPLAYERJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(WavePlayerJob);

#endif // AUD_WAVEPLAYERJOB_H
