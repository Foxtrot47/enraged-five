// 
// audiohardware/granularmix.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_GRANULARMIX_H
#define AUD_GRANULARMIX_H

#include "granularsubmix.h"
#include "pcmsource.h"
#include "cmdbuffer.h"
#include "decoder.h"
#include "waveref.h"

namespace rage
{
	// ----------------------------------------------------------------
	// audGranularMix - Plays back grain data from a single wave file
	// ----------------------------------------------------------------	
	class audGranularMix
	{
	// Public Types
	public:
		enum audGrainPlaybackStyle
		{
			PlaybackStyleLoopsAndGrains,
			PlaybackStyleLoopsOnly,
			PlaybackStyleGrainsOnly,
			PlaybackStyleMax,
		};

		enum audGrainPlayerQuality
		{
			GrainPlayerQualityLow,
			GrainPlayerQualityHigh,
			GrainPlayerQualityMax,
		};

	// Public Methods
	public:
#if !__SPU
		audGranularMix();
		~audGranularMix();
		bool Init(const s32 waveSlotIndex, const u32 waveNameHash, const u8 outputChannel, const u8 pitchClockIndex, const bool matchMinPitch, const bool matchMaxPitch, const f32 maxLoopProportion);
#endif
		
#if __SPU
		void SpuPostMix();
#endif

		void GenerateFrame(f32* destBuffer, u32 numGrainsGenerated, f32 currentGranularFraction, f32 granuleFractionPerSample);
		void SetLoopRandomisationEnabled(const bool enabled, const f32 changeRate, const f32 maxPitchFraction);
		void SetVolume(const f32 volume, const f32 smoothRate);
		void SetMuted(const bool muted);
		void SnapToNearestPlaybackType(); 
		void SetPureRandom(bool pureRandom);
		void SetPureRandomWithStyle(audGranularSubmix::audGrainPlaybackOrder style);
		void HandleCustomCommandPacket(const audPcmSourceCustomCommandPacket* packet);
		void BeginFrame(const f32 currentRateHz, const f32 predictedRateHz, const f32 xValueStepPerGranule, const f32 volumeModulation);
		void EndFrame(const f32 currentRateHz PS3_ONLY(, const f32 predictedRateHz, const f32 xValueStepPerBuffer));
		u32 QueryNumDecodersInUse() const;
		void SetQuality(audGrainPlayerQuality quality);
		PS3_ONLY(u32 QueryNumVramHelpersInUse() const;)
		void Shutdown();
		void StartPhys();
		void StopPhys();
		void SetMinMaxPitch(const f32 minPitch, const f32 maxPitch);
		u16 ComputeCurrentPeak(f32 xValue) const;

#if __BANK && !__SPU
		void DebugDrawGrainTable(s32 yOffset, f32 xValueCurrent);
		void SetLoopEditorActive(bool active) { m_LoopEditorActive = active; }
		void SetLoopEditorLoopIndex(u32 index) { m_LoopEditorLoopIndex = index; }
		void LoopEditorShiftLeft(u32 amount);
		void LoopEditorShiftRight(u32 amount);
		void LoopEditorGrowLoop();
		void LoopEditorShrinkLoop();
		void LoopEditorCreateLoop();
		void LoopEditorDeleteLoop();
		void LoopEditorExportData(u32 mixIndex);
		void SetToneGeneratorEnabled(bool enabled);
#endif

#if __PS3
		bool QueryAnyVramLoaderDataAvailable() const;
#endif

		void SetGranularSlidingWindowSize(const f32 sizeHz, const u32 minGrains, const u32 maxGrains);
		void SetMinGrainRepeatRate(const u32 rate);

		inline u8 GetOutputChannel() const									{ return m_OutputChannel; }
		inline u8 GetGranularClockIndex() const								{ return m_GranularClockIndex; }
		inline s32 GetLengthSamples() const									{ return m_LengthSamples; }
		inline f32 GetHeadroom() const										{ return m_Headroom; }
		inline bool IsInitialised() const									{ return m_Initialised; }
		inline bool ShouldMixNatve() const									{ return m_MixNative; }
		inline void SetMixNative(const bool mixNative)						{ m_MixNative = mixNative; }
		inline void SetGranularChangeRateForLoops(const f32 changeRate)		{ m_GranularChangeRateForLoops = changeRate; }

#if !__SPU		
		inline audGrainPlayerQuality GetQuality() const						{ return m_Quality; }
		inline s32 GetNumSubmixes() const									{ return m_GranularSubmixes.GetCount(); }
		inline f32 GetGranuleChangeRatePerGranule() const					{ return m_GranuleStepPerGranule; }
		inline audGranularSubmix* GetGranularSubmix(u32 index) 				{ return m_GranularSubmixes[index]; }
		inline bool IsPureRandomMix()										{ return m_PureRandomPlayback; }
		inline void SetPlaybackStyle(audGrainPlaybackStyle style)			{ m_GrainPlaybackStyle = style; }
		inline void SetPitchLocked(bool locked)								{ m_PitchLocked = locked; }
		inline void SetAllowLoopGrainOverlap(bool allowOverlap)				{ m_AllowLoopGrainOverlap = allowOverlap; }
		inline void SetMasterVolumeScale(f32 volumeScale)					{ m_MasterVolumeScale = volumeScale; }
		inline u32 GetNumGrains() const										{ return m_NumGrains; }
		inline f32 GetPitchValue(u32 index) const							{ return m_GrainTable[index].pitch; }
		inline bool IsMuted() const											{ return m_Muted; }
#endif

	// Public Attributes
	public:
		static f32 s_GranularLoopBelowBias;
		static f32 s_GranularLoopToGrainChangeRate;
		static f32 s_GranularGrainToLoopChangeRate;
		static f32 s_DefaultGranularChangeRateForLoops;
		static u32 s_LoopGrainCrossfadeStyle;
		static u32 s_LoopLoopCrossfadeStyle;
		static u32 s_GranularMixCrossfadeStyle;
		static f32 s_LoopRandomisationGranularChangeRate;
		static u32 s_InterGrainCrossfadeStyle;
		static bool s_GranularPeakNormaliseLoops;

#if __BANK && __PS3
		static bool s_DebugDrawVramGrainLoaders;
		static bool s_DebugDrawVramLoopLoaders;
#endif

	// Public Types
	public:
		struct audSyncLoopDefinition
		{
			audGranularSubmix::audGrainPlaybackOrder playbackOrder;
			u32 submixID;

#if __BANK
			atArray<u32> validGrains;
#else
			const u32* validGrains;
#endif

			bool enabled;
			u32 numValidGrains;
			f32 averageValidGrainIndex; 
			f32 averageValidGrainHertz;
			f32 averageValidGrainFraction;
		};

	// Private Methods
	private:
		void InitLoops();
		void GenerateLoopInfo(u32 loopIndex);
		void CalculateMinMaxLoopHz();

#if __PS3
		void ResetVramLoaders();
		void UpdateLoopVramLoaders(const f32 predictedRateHz);
		void UpdateGrainVramLoaders(const f32 predictedRateHz, f32 xValueStepPerBuffer);
		s32 CalculateFinalGrainInVramBuffer(s32 startGrainIndex, u32 numFetchBuffers);
		s32 CalculateFinalNonLoadedGrainInVramBuffer(s32 startGrainIndex, audGranularSubmix::audVramLoader* queryingLoader, audGranularSubmix::audVramLoader* otherLoaders, u32 numOtherLoaders, u32 numFetchBuffers);
#endif

		void CalculateNearestLoops(const f32 currentRateHz, 
			s32& nearestLoopAbove, 
			s32& nearestLoopBelow, 
			f32& nearestLoopAboveVolumeScale, 
			f32& nearestLoopBelowVolumeScale) const;
		
	private:
		atFixedArray<audGranularSubmix*, 3> m_GranularSubmixes;
		audWaveRef m_WaveRef;
		f32 m_Headroom;
		f32 m_CurrentVolumeScale;
		f32 m_MasterVolumeScale;
		f32 m_NearestLoopAboveVolumeScale;
		f32 m_NearestLoopBelowVolumeScale;
		f32 m_LoopVolumeScale;
		f32 m_GrainVolumeScale;
		s32 m_NearestLoopAbove;
		s32 m_NearestLoopBelow;
		u32 m_LengthSamples;
		u8 m_OutputChannel;
		u8 m_GranularClockIndex;
		bool m_Initialised;
		bool m_IsPlayingPhysically;
		bool m_MixNative;

#if !__SPU
	// Private Attributes
	private:
		audSmoother m_VolumeSmoother ;
		audSmoother m_LoopHzSmoother;
		audSmoother m_LoopHzRandomisationApplySmoother;
		audSmoother m_LoopGrainVolumeSmoother;
		audSmoother m_LoopGrainCalcSmoother;
		const audGranularSubmix::audGrainData *m_GrainTable;

		f32 m_TargetVolumeScale;
		f32 m_LoopHzSmootherTarget;
		f32 m_LoopHzSmootherCurrent;
		f32 m_LoopRandomisationChangeRate;
		f32 m_LoopRandomisationMaxPitchFraction;
		f32 m_GranuleStepPerGranule;
		f32 m_ParentMinPitch;
		f32 m_ParentMaxPitch;
		f32 m_PitchShift;
		f32 m_PitchStretch;
		f32 m_MinLoopHz;
		f32 m_MaxLoopHz;
		f32 m_SampleRateConversionRatio;
		f32 m_MaxLoopProportion;
		f32 m_GranularChangeRateForLoops;
		
		audGrainPlaybackStyle m_GrainPlaybackStyle;
		audGrainPlayerQuality m_Quality;
		
		s32 m_WaveSlotIndex;
		u32 m_NumGrains;
		u32 m_DesiredLoopVolumeScale;
		u32 m_LengthBytes;
		BANK_ONLY(u32 m_LoopEditorLoopIndex);
		s16 *m_Data;
		bool m_MatchMinPitch;
		bool m_MatchMaxPitch;
		bool m_Muted;
		bool m_LoopRandomisationEnabled;
		BANK_ONLY(bool m_LoopEditorActive);
		bool m_PureRandomPlayback;
		bool m_PitchLocked;
		bool m_AllowLoopGrainOverlap;

#if __PS3
		enum {kNumLoopVramLoaders = 3};
		enum {kLoopVramLoaderNumFetchBuffers = 2};
		audGranularSubmix::audVramLoader m_LoopVramLoaders[kNumLoopVramLoaders];

		enum {kNumGrainVramLoaders = 2};
		enum {kGrainVramLoaderNumFetchBuffers = 2};
		audGranularSubmix::audVramLoader m_GrainVramLoaders[kNumGrainVramLoaders];

		audCurve m_HertzToDecodesCurve;
#endif

	// Non-spu variables
	private:
		enum {kMaxSyncLoops = 12};
		atFixedArray<audSyncLoopDefinition, kMaxSyncLoops> m_SynchronisedLoopDefinitions;
#endif
	};
}

#endif // AUD_GRANULARMIX_H
