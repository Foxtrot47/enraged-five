//
// audiohardware/lffreelist.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_LFFREELIST_H
#define AUD_LFFREELIST_H

#include "atl/array.h"
#include "system/interlocked.h"
#include "system/spinlock.h" // for sys_lwsync

namespace rage
{

// PURPOSE
//	Implements a lock-free, free list based allocator usable across PPU and SPU
// NOTES
//	'this' should always be the PPU effective address, not a local store address - all member access
//	on SPU is through interlocked/DMA operations.
template<u32 _Size> class audLFFreeList
{
public:

	void Init()
	{
		NumAllocated = 0;
		MakeAllFree(); 
	}

	void MakeAllFree()
	{
		FirstFree = 0;
		for (u32 i = 0; i < Size; i++)
		{
			Assign(List[i], i+1);		// Last entry will be Size, our "end of list" sentinel
		}
	}

	u32 Allocate()
	{
		// Allocate can be called from any thread
		// NOTE: on SPU we are called with this equal to the PPU EA, not a local store address.  All member
		// variable access is via DMAs

		u32 slotIndex;
		u32 newFirstFree;

		do 
		{	
			slotIndex = sysInterlockedRead(&FirstFree);

			if(slotIndex >= Size)
			{
				// Out of space
				return ~0U;
			}
#if __SPU
			const s32 TAG = 6;
			newFirstFree = sysDmaGetUInt16((uint64_t)&List[slotIndex], TAG);
#else
			newFirstFree = List[slotIndex];
#endif

		}while(sysInterlockedCompareExchange(&FirstFree, newFirstFree, slotIndex) != slotIndex);

		sysInterlockedIncrement(&NumAllocated);

		// Enforce Acquire memory access semantics
		sys_lwsync();

		return slotIndex;
	}

#if !__SPU
	void Free(const u32 idx)
	{
		// Add to free list atomically (since Allocate() doesn't take the lock)
		u32 firstFree;
		do
		{
			firstFree = sysInterlockedRead(&FirstFree);
			Assign(List[idx], FirstFree);
			sys_lwsync();
		}while(sysInterlockedCompareExchange(&FirstFree, idx, firstFree) != firstFree);

		sysInterlockedDecrement(&NumAllocated);
	}
#endif

	// Returns true if the free list is empty (ie Allocate will fail)
	bool IsEmpty() const
	{
		return sysInterlockedRead(&FirstFree) == Size;
	}

	u32 GetNumAllocated()
	{
		return sysInterlockedRead(&NumAllocated); 			
	}

	enum {Size=_Size};
private:

	volatile u32 FirstFree;
	volatile u32 NumAllocated;
	atRangeArray<u16, _Size> List;
};

} // namespace rage

#endif // AUD_LFFREELIST_H
