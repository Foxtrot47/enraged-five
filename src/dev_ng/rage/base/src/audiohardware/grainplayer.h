// 
// audiohardware/grainplayer.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_GRAINPLAYER_H
#define AUD_GRAINPLAYER_H

#include "audioengine/widgets.h"
#include "granularmix.h"
#include "pcmsource.h"

namespace rage
{
	// ----------------------------------------------------------------
	// audGranularWobble - stores information about volume and pitch 
	// modifiers to be applied on a per-granule basis
	// ----------------------------------------------------------------	
	class audGranularWobble
	{
	public:
		audGranularWobble();
		virtual ~audGranularWobble() {};

		inline bool IsActive() const {return m_Active;}
		inline void SetActive(bool active) {m_Active = active;}

		void Init(u32 length, f32 speed, f32 volume, f32 pitch);
		void OnGrainChange();
		f32 GetCurrentPitchMultiplier() const;	
		f32 GetCurrentVolumeMultiplier() const;

	private:
		u32 m_CurrentIndex;
		u32 m_Length;
		f32 m_Speed;
		f32 m_Pitch;
		f32 m_Volume;
		bool m_Active;
	};

	// ----------------------------------------------------------------
	// audGranularClock - Helper class for tracking a position between two
	// pitches with smoothing values etc
	// ----------------------------------------------------------------	
	class audGranularClock
	{
	public:
		audGranularClock();
		~audGranularClock() {};
		void BeginFrame();
		void EndFrame();

		void SetX(const f32 val, const u32 timeStep);
		void ForceX(const f32 val);

		inline f32 GetCurrentRateHz() const				{ return m_CurrentRateHz; }
		inline f32 GetPredictedRateHz() const			{ return m_PredictedRateHz; }
		inline f32 GetXValueStepPerGranule() const		{ return m_XValueStepPerGranule; }
		inline f32 GetXValueStepPerBuffer() const		{ return m_XValueStepPerBuffer; }
		inline f32 GetMinPitch() const					{ return m_HertzMin; }
		inline f32 GetMaxPitch() const					{ return m_HertzMax; }
		inline f32 GetWobbleVolumeModulation() const	{ return m_WobbleVolumeModulation; }
		inline u32 GetNumGrainsGenerated() const		{ return m_NumGrainsGenerated; }
		inline f32 GetCurrentGranularFraction() const	{ return m_CurrentGranularFraction; }
		inline f32 GetGranularFractionPerSample() const { return m_GranuleFractionPerSample; }
		inline f32 GetXCurrent() const					{ return m_XCurrent; }
		inline f32 GetFrequencyScalingFactor() const	{ return m_FreqScalingFactor; }
		inline audGranularWobble* GetWobble() 			{ return &m_Wobble; }

		inline void SetFrequencyScalingFactor(f32 scalingFactor)	{ m_FreqScalingFactor = scalingFactor; }
		inline void SetMinMaxPitch(f32 hertzMin, f32 hertzMax)		{ m_HertzMin = hertzMin; m_HertzMax = hertzMax; }
		inline void SetXValueSmoothRate(f32 smoothRate)				{ m_XValueSmoothRate = smoothRate; }
		inline void SetPitchLocked(bool locked)						{ m_PitchLocked = locked; }
		inline void ResetGranularFraction()							{ m_CurrentGranularFraction = 0.0f; }
		inline void SetGranularFractionPerSample(f32 fraction)		{ m_GranuleFractionPerSample = fraction; }

	private:
		audGranularWobble m_Wobble;
		f32 m_WobbleVolumeModulation;
		f32 m_WobblePitchModulation;
		f32 m_CurrentRateHz;
		f32 m_HertzMin;
		f32 m_HertzMax;
		f32 m_GranuleFractionPerSample;
		f32 m_FreqScalingFactor;
		f32 m_XPrev;
		f32 m_XTarget;
		f32 m_XCurrent;
		f32 m_XValueStepPerSec;
		f32 m_XValueStepPerGranule;
		f32 m_XValueStepPerBuffer;
		f32 m_XValueSmoothRate;
		f32 m_CurrentGranularFraction;
		f32 m_SamplesPerGranule;
		u32 m_NumGrainsGenerated;
		f32 m_PredictedRateHz;
		bool m_PitchLocked;
	};

	// ----------------------------------------------------------------
	// audGrainPlayer - Top level Coordinator class for managing granular
	// playback
	// ----------------------------------------------------------------	
	class audGrainPlayer : public audPcmSource
	{
		friend class audMixerVoice;
 
	// Public types
	public:
		enum {kMaxGranularMixersPerPlayer = 6};
		enum {kMaxGranularClocksPerPlayer = 2};

		struct Params
		{
			static const u32 XValueForced = 0x95CAF442;
			static const u32 XValue = 0xF61C3D14;
			static const u32 XValueSmoothRate = 0x5A24CE90;
			static const u32 TimeStep = 0x50597EE2;
			static const u32 GrainPlaybackStyle = 0xCF2E7C07;
			static const u32 GrainRandomisationStyle = 0xAF2317FC;
			static const u32 SetAsPureRandomPlayer = 0x52200BCA;
			static const u32 SetAsPureRandomPlayerWithStyle = 0x45F442E1;
			static const u32 SnapToNearestPlaybackType = 0x86C8B2FE;
			static const u32 InitParams = 0xD9D79602;
			static const u32 SetLoopEnabled = 0xE2006A71;
			static const u32 SetGrainSkippingEnabled = 0x86CEEDC;
			static const u32 SetLoopRandomisationParams = 0x22AE7F30;
			static const u32 SetPitchLocked = 0x8432AF11;
			static const u32 SetWobbleEnabled = 0xBA62229C;
			static const u32 InitGranularClock = 0xFA5EB3C;
			static const u32 SetGranularSlidingWindowSize = 0x32DD6D4F;
			static const u32 SetChannelVolume = 0x28F9F9EC;
			static const u32 MuteChannel = 0xE7155C0F;
			static const u32 AllocateVariableBlock = 0x558E2D6D;
			static const u32 SetMixNative = 0x8643F27D;
			static const u32 SetGranularChangeRateForLoops = 0xEE90C5AD;
#if __BANK
			static const u32 SetDebugRenderingEnabled = 0xD1DA4B52;
			static const u32 ShiftLoopLeft = 0xD7FA509D;
			static const u32 ShiftLoopRight = 0x3530F93E;
			static const u32 GrowLoop = 0x1C647D6A;
			static const u32 ShrinkLoop = 0x547D92A9;
			static const u32 DeleteLoop = 0x1D2004ED;
			static const u32 CreateLoop = 0x24CA6417;
			static const u32 ExportData = 0xF30B1E9A;
			static const u32 ToneGeneratorEnabled = 0x9B560977;
#endif
		};

		struct audInitGrainPlayerCommandPacket : public audPcmSourceCustomCommandPacket
		{
			audInitGrainPlayerCommandPacket()		
			{
				packetSize = sizeof(audInitGrainPlayerCommandPacket);
				initialVolume = 1.0f;
				maxLoopProportion = 1.0f;
				quality = audGranularMix::GrainPlayerQualityMax;
				allowLoopGrainOverlap = true;
			}

			u32 channel;
			f32 initialVolume;
			f32 maxLoopProportion;
			u32 waveNameHash;
			u32 waveSlot;
			u8 outputChannel;
			u8 granularClock;
			bool matchMinPitch;
			bool matchMaxPitch;
			bool allowLoopGrainOverlap;
			audGranularMix::audGrainPlayerQuality quality;
		};

		struct audGrainPlayerSetLoopRandomisationParams : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetLoopRandomisationParams()		
			{
				packetSize = sizeof(audGrainPlayerSetLoopRandomisationParams);
				randomisationEnabled = false;
				randomisationChangeRate = 0.0f;
				randomisationMaxPitchFraction = 0.0f;
			}

			bool randomisationEnabled;
			f32 randomisationChangeRate;
			f32 randomisationMaxPitchFraction;
		};

		struct audGrainPlayerSetChannelFlagPacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetChannelFlagPacket()		
			{
				packetSize = sizeof(audGrainPlayerSetChannelFlagPacket);
				channel = 0;
				flag = false;
			}

			u32 channel;
			bool flag;
		};

		struct audGrainPlayerSetChannelU32Packet : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetChannelU32Packet()		
			{
				packetSize = sizeof(audGrainPlayerSetChannelU32Packet);
				channel = 0;
				value = 0;
			}

			u32 channel;
			u32 value;
		};

		struct audGrainPlayerSetLoopEnabledPacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetLoopEnabledPacket()		
			{
				packetSize = sizeof(audGrainPlayerSetLoopEnabledPacket);
				channel = 0;
				loopID = 0;
			}

			u32 loopID;
			bool loopEnabled;
			u32 channel;
		};

		struct audGrainPlayerSetGrainSkippingEnabledPacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetGrainSkippingEnabledPacket()		
			{
				packetSize = sizeof(audGrainPlayerSetGrainSkippingEnabledPacket);
				channel = 0;
				enabled = false;
				numToPlay = 0;
				numToSkip = 0;
				skippedGrainVol = 0.0f;
			}

			u32 channel; 
			bool enabled;
			u32 numToPlay;
			u32 numToSkip;
			f32 skippedGrainVol;
		};

		struct audGrainPlayerSetChannelVolumePacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetChannelVolumePacket()		
			{
				packetSize = sizeof(audGrainPlayerSetChannelVolumePacket);
				volume = 0.0f;
				smoothRate = 1.0f;
				channel = 0;
				masterVolume = false;
			}

			f32 volume;
			f32 smoothRate;
			u32 channel;
			bool masterVolume;
		};

		struct audGrainPlayerSetPlaybackStylePacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetPlaybackStylePacket()		
			{
				packetSize = sizeof(audGrainPlayerSetPlaybackStylePacket);
				channel = 0;
				style = audGranularMix::PlaybackStyleLoopsAndGrains;
			}

			audGranularMix::audGrainPlaybackStyle style;
			u32 channel;
		};

		struct audGrainPlayerSetSlidingWindowSizePacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetSlidingWindowSizePacket()		
			{
				packetSize = sizeof(audGrainPlayerSetSlidingWindowSizePacket);
				windowSizeHz = 0.0f;
				minGrains = 0;
				maxGrains = 0;
				minRepeatRate = 0;
			}

			f32 windowSizeHz;
			u32 minGrains;
			u32 maxGrains;
			u32 minRepeatRate;
		};

		struct audGrainPlayerSetWobbleEnabledPacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerSetWobbleEnabledPacket()		
			{
				packetSize = sizeof(audGrainPlayerSetWobbleEnabledPacket);
			}

			u32 grainClock;
			u32 wobbleLength;
			f32 wobbleSpeed;
			f32 wobblePitch;
			f32 wobbleVolume;
		};

		struct audGrainPlayerInitGranularClockPacket : public audPcmSourceCustomCommandPacket
		{
			audGrainPlayerInitGranularClockPacket()		
			{
				packetSize = sizeof(audGrainPlayerInitGranularClockPacket);
			}

			u32 clockIndex;
			f32 minHertz;
			f32 maxHertz;
		};

	// Public Methods
	public:
		// Non SPU functions
#if !__SPU
		audGrainPlayer();

		AUD_PCMSOURCE_VIRTUAL void Shutdown();
		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const u32 val);
		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const f32 val);
		AUD_PCMSOURCE_VIRTUAL void HandleCustomCommandPacket(const audPcmSourceCustomCommandPacket* packet);
#endif

		// PURPOSE
		//	Allows code to query if the buffer is looping
		// RETURNS
		//	true 
		AUD_PCMSOURCE_VIRTUAL bool IsLooping()  const { return true; }

		// PURPOSE
		//	Returns the length of the wave in samples
		// NOTES
		//	This converts from original sample rate to native sample rate based on the current
		//	playback frequency, and as such will vary as playback frequency varies
		AUD_PCMSOURCE_VIRTUAL s32 GetLengthSamples() const { return m_GranularMixers[0]->GetLengthSamples(); }

		// PURPOSE
		//	Returns the current playback position of the wave, in samples at the native
		//	sample rate.
		AUD_PCMSOURCE_VIRTUAL u32 GetPlayPositionSamples() const;

		// PURPOSE
		//	Returns true the end of the wave has been reached
		AUD_PCMSOURCE_VIRTUAL bool IsFinished() const { return m_FinishedPlayback; }

		AUD_PCMSOURCE_VIRTUAL f32 GetHeadroom() const { return m_GranularMixers[0]->GetHeadroom(); }

		// PURPOSE
		//	Returns peak level in range [0,65535]
		AUD_PCMSOURCE_VIRTUAL u32 GetCurrentPeakLevel() const { return m_CurrentPeakLevelSample; }

#if __BANK
#if !__SPU
		void DebugDraw();
#endif
		static audGrainPlayer* s_DebugDrawGrainPlayer;
		static bool s_LoopEditorActive;
		static u32 s_LoopEditorMixIndex;
		static u32 s_LoopEditorLoopIndex;
		static u32 s_LoopEditorShiftAmount;
#endif

		static f32 sm_BuffersPerSec;

		AUD_PCMSOURCE_VIRTUAL void BeginFrame();
		AUD_PCMSOURCE_VIRTUAL void GenerateFrame();
		AUD_PCMSOURCE_VIRTUAL void EndFrame();
		AUD_PCMSOURCE_VIRTUAL void StartPhys(s32 channel);
		AUD_PCMSOURCE_VIRTUAL void StopPhys(s32 channel);
		AUD_PCMSOURCE_VIRTUAL void Start() {};
		AUD_PCMSOURCE_VIRTUAL void Stop();

	// Private Methods
	private:
		void CalculateMinMaxPitch(GranularPitchCalcMode pitchCalculationMode, f32& hertzMin, f32& hertzMax);
		void DebugDrawPitchMapping(const char* title, GranularPitchCalcMode pitchMode, u32 offset);
		void UpdateVirtualisedVariableBlockValues(f32 xVal);
		void Mix(u32 granularMixIndex SPU_ONLY(, u32 scratchMixTag));

	// Private Attributes
	private:
		enum audGrainPlaybackState
		{
			AwaitingBeginFrame,
			AwaitingGenerateFrame,
			AwaitingEndFrame,
		};

		atFixedArray<audGranularMix*, kMaxGranularMixersPerPlayer> m_GranularMixers;
		atFixedArray<audGranularClock, kMaxGranularClocksPerPlayer> m_GranularClocks;

		audGrainPlaybackState m_GrainPlaybackState;
		u32 m_NumGeneratedSamples;
		u32 m_TimeStep;
		u16 m_CurrentPeakLevelSample;

		bool m_SkipGenerationThisFrame;
		bool m_FinishedPlayback;
	};
}

#endif // AUD_GRAINPLAYER_H
