// 
// audiohardware/grainplayer.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "device.h"
#include "driver.h"
#include "grainplayer.h"

#include "profile/element.h"
#include "profile/page.h"
#include "profile/group.h"
#include "audioengine/spuutil.h"
#include "grcore/debugdraw.h"

#if __BANK
#include "grcore/viewport.h"
#endif

#define Align16(x) (((x)+15)&~15)

namespace rage 
{
#if __SPU
extern u32 g_ScratchDecoderTags[3];
extern u32 g_ScratchCrossfadeBufferTag;
u8* g_ScratchMixers[audGrainPlayer::kMaxGranularMixersPerPlayer];
#endif

#if __BANK
audGrainPlayer* audGrainPlayer::s_DebugDrawGrainPlayer = NULL;
bool audGrainPlayer::s_LoopEditorActive = false;
u32 audGrainPlayer::s_LoopEditorMixIndex = 0;
u32 audGrainPlayer::s_LoopEditorLoopIndex = 0;
u32 audGrainPlayer::s_LoopEditorShiftAmount = 1;
#endif

f32 audGrainPlayer::sm_BuffersPerSec = (f32)kMixerNativeSampleRate / (f32)kMixBufNumSamples;

PF_PAGE(AudioGrainPlayerTimingPage, "audGrainPlayer Timings");
PF_GROUP(AudioGrainPlayerTimings);
PF_LINK(AudioGrainPlayerTimingPage, AudioGrainPlayerTimings);
PF_TIMER(GenerateFrameGrainPlayer, AudioGrainPlayerTimings);

PF_PAGE(AudioGrainPlayerStatsPage, "audGrainPlayer Stats");
PF_GROUP(AudioGrainPlayerStats);
PF_LINK(AudioGrainPlayerStatsPage, AudioGrainPlayerStats);
PF_VALUE_FLOAT(GranuleChangeRateMix1, AudioGrainPlayerStats);
PF_VALUE_FLOAT(GranuleChangeRateMix2, AudioGrainPlayerStats);
PF_VALUE_FLOAT(GranuleChangeRateMix3, AudioGrainPlayerStats);
PF_VALUE_FLOAT(GranuleChangeRateMix4, AudioGrainPlayerStats);

PF_PAGE(AudioGrainPlayerCounterPage, "audGrainPlayer Counters");
PF_GROUP(AudioGrainPlayerCounts);
PF_LINK(AudioGrainPlayerCounterPage, AudioGrainPlayerCounts);
PF_VALUE_INT(AllocatedDecoders, AudioGrainPlayerCounts);
#if __PS3
PF_VALUE_INT(AllocatedVramHelpers, AudioGrainPlayerCounts);
#endif

#if __BANK
u32 g_NumAllocatedVramHelpers = 0;
#endif

// ----------------------------------------------------------------
// audGranularWobble default constructor
// ----------------------------------------------------------------
audGranularWobble::audGranularWobble()
{
	m_Active = false;
	m_CurrentIndex = 0;
}

// ----------------------------------------------------------------
// audGranularWobble init
// ----------------------------------------------------------------
void audGranularWobble::Init(u32 length, f32 speed, f32 volume, f32 pitch)
{
	m_Length = length;
	m_Speed = speed;
	m_Volume = volume; 
	m_Pitch = pitch;
	m_Active = length > 0;
}

// ----------------------------------------------------------------
// audGranularWobble GetCurrentPitchMultiplier
// ----------------------------------------------------------------
f32 audGranularWobble::GetCurrentPitchMultiplier() const	
{ 
	f32 wobbleValue = ((m_Pitch * (1.0f - (m_CurrentIndex/(f32)m_Length))) * (sin(PI * m_Speed * m_CurrentIndex)));
	wobbleValue *= Clamp(1.0f - (m_CurrentIndex/(f32)m_Length), 0.0f, 1.0f);
	return 1.0f + wobbleValue;
}

// ----------------------------------------------------------------
// audGranularWobble GetCurrentVolumeMultiplier
// ----------------------------------------------------------------
f32 audGranularWobble::GetCurrentVolumeMultiplier() const	
{ 
	f32 wobbleValue = ((m_Volume * (1.0f - (m_CurrentIndex/(f32)m_Length))) * ((-0.5f + sin(PI * m_Speed * m_CurrentIndex))/2.0f));
	wobbleValue *= Clamp(1.0f - (m_CurrentIndex/(f32)m_Length), 0.0f, 1.0f);
	return 1.0f + wobbleValue;
}

// ----------------------------------------------------------------
// audGranularWobble OnGrainChange
// ----------------------------------------------------------------
void audGranularWobble::OnGrainChange()
{
	m_CurrentIndex++;

	if(m_CurrentIndex >= m_Length)
	{
		m_CurrentIndex = 0;
		m_Active = false;
	}
}

// ----------------------------------------------------------------
// audGranularClock constructor
// ----------------------------------------------------------------
audGranularClock::audGranularClock()
{
	m_XCurrent = m_XTarget = m_XPrev = 0.f;
	m_XValueStepPerSec = 0.0f;
	m_XValueStepPerGranule = 0.0f;
	m_XValueStepPerBuffer = 0.0f;
	m_CurrentGranularFraction = 0.0f;
	m_HertzMin = 10.0f;
	m_HertzMax = 70.0f;
	m_XValueSmoothRate = 1.0f;
	m_GranuleFractionPerSample = 0.0f;
	m_SamplesPerGranule = 0.0f;
	m_NumGrainsGenerated = 0;
	m_WobbleVolumeModulation = 1.0f;
	m_WobblePitchModulation = 1.0f;
	m_CurrentRateHz = m_HertzMin;
	m_PredictedRateHz = m_HertzMin;
	m_FreqScalingFactor = 1.0f;
	m_PitchLocked = false;
}

// ----------------------------------------------------------------
// audGranularClock BeginFrame
// ----------------------------------------------------------------
void audGranularClock::BeginFrame()
{
	m_WobblePitchModulation = 1.0f;
	m_WobbleVolumeModulation = 1.0f;

	if(m_Wobble.IsActive())
	{
		m_WobblePitchModulation = m_Wobble.GetCurrentPitchMultiplier();
		m_WobbleVolumeModulation = m_Wobble.GetCurrentVolumeMultiplier();
	}

	// Given our input, work out what this corresponds to in terms of step rate
	m_CurrentRateHz = 0.0f;
	m_CurrentRateHz = m_HertzMin + ((m_HertzMax - m_HertzMin) * m_XCurrent);

	m_SamplesPerGranule = kMixerNativeSampleRate / m_CurrentRateHz;
	const f32 granuleFractionPerBuffer = kMixBufNumSamples / m_SamplesPerGranule;
	m_GranuleFractionPerSample = granuleFractionPerBuffer / kMixBufNumSamples;
	m_GranuleFractionPerSample *= m_WobblePitchModulation;
	m_GranuleFractionPerSample *= m_FreqScalingFactor;
}

// ----------------------------------------------------------------
// audGranularClock EndFrame
// ----------------------------------------------------------------
void audGranularClock::EndFrame()
{
	m_CurrentGranularFraction += (m_GranuleFractionPerSample * kMixBufNumSamples); 

	// Okay, we've done our mixing, so now move the X position onwards
	m_XCurrent += m_XValueStepPerBuffer;

	// Clamp back to the target position if we overshoot
	if(m_XTarget > m_XPrev)
	{
		if(m_XCurrent > m_XTarget)
		{
			m_XCurrent = m_XTarget;
		}
	}
	else
	{
		if(m_XCurrent < m_XTarget)
		{
			m_XCurrent = m_XTarget;
		}
	}

	while(m_CurrentGranularFraction >= 1.0f)
	{
		if(m_Wobble.IsActive())
		{
			m_Wobble.OnGrainChange();
		}

		m_NumGrainsGenerated++;
		m_CurrentGranularFraction -= 1.0f;
	}

	// Predicted Hz required for vram fetching. Assume that we're going to take about one game frame to fetch data.
	// One game frame @ 30fps = roughly 6 audio frames. Also take into account how much data is left in the current granule
	f32 granuleFractionPerBuffer = m_GranuleFractionPerSample * kMixBufNumSamples;
	f32 buffersLeftInGranule = (1.0f - m_CurrentGranularFraction)/granuleFractionPerBuffer;
	f32 predictedXValue = Clamp(0.0f, 1.0f, m_XCurrent + (m_XValueStepPerBuffer * 6.0f) + (m_XValueStepPerBuffer * buffersLeftInGranule));
	m_PredictedRateHz = 0.0f;
	m_PredictedRateHz = m_HertzMin + ((m_HertzMax - m_HertzMin) * predictedXValue);
}

// ----------------------------------------------------------------
// audGranularClock SetX
// ----------------------------------------------------------------	
void audGranularClock::SetX(const f32 val, const u32 timeStep)
{
	m_XPrev = m_XCurrent;

	if(m_PitchLocked)
	{
		m_XCurrent = m_XTarget;
	}
	else
	{
		m_XTarget = Clamp(val, 0.0f, 1.0f);
	}

	m_XValueStepPerSec =  timeStep > 0? (((m_XTarget - m_XPrev) * m_XValueSmoothRate)) / (timeStep * 0.001f) : 0.0f;
	m_XValueStepPerGranule = m_CurrentRateHz > 0.0f? m_XValueStepPerSec / m_CurrentRateHz : 0.0f;
	m_XValueStepPerBuffer = m_XValueStepPerSec / audGrainPlayer::sm_BuffersPerSec;
}


// ----------------------------------------------------------------
// audGranularClock forceX
// ----------------------------------------------------------------	
void audGranularClock::ForceX(const f32 val)
{
	m_XPrev = val;
	m_XCurrent = val;
	m_XTarget = val;
}

#if !__SPU
// ----------------------------------------------------------------
// audGrainPlayer constructor
// ----------------------------------------------------------------	
audGrainPlayer::audGrainPlayer() : audPcmSource(AUD_PCMSOURCE_GRAINPLAYER)
{
	m_GrainPlaybackState = AwaitingBeginFrame;
	m_FinishedPlayback = false;
	m_SkipGenerationThisFrame = false;
	m_NumGeneratedSamples = 0;
	m_TimeStep = 0;
	m_CurrentPeakLevelSample = 0xffff;

	for(u32 loop = 0; loop < kMaxGranularMixersPerPlayer; loop++)
	{
		m_GranularMixers.Push(NULL);
	}
}

// ----------------------------------------------------------------
// Calculate the min max pitch
// ----------------------------------------------------------------
void audGrainPlayer::CalculateMinMaxPitch(GranularPitchCalcMode pitchCalculationMode, f32& hertzMin, f32& hertzMax)
{
	// Use the min/max pitch from a given channel
	if(pitchCalculationMode <= kGranularPitchMapChannel5)
	{
		s32 mixIndex = (s32)pitchCalculationMode;

		if(m_GranularMixers[mixIndex])
		{
			if(m_GranularMixers.GetCount() > mixIndex &&
				m_GranularMixers[mixIndex]->GetNumGrains() > 0)
			{
				const f32 hertzStart = m_GranularMixers[mixIndex]->GetPitchValue(0);
				const f32 hertzEnd = m_GranularMixers[mixIndex]->GetPitchValue(m_GranularMixers[mixIndex]->GetNumGrains() - 1);

				hertzMin = Min(hertzStart, hertzEnd);
				hertzMax = Max(hertzStart, hertzEnd);
			}
		}
	}
	// Use the smallest min/max range from all channels
	else if(pitchCalculationMode == kGranularPitchMapClamp)
	{
		hertzMin = 0.0f;
		hertzMax = 1000.0f;

		for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
		{
			if(m_GranularMixers[loop])
			{
				if(m_GranularMixers[loop]->GetNumGrains() > 50)
				{
					const f32 thisHertzStart = m_GranularMixers[loop]->GetPitchValue(0);
					const f32 thisHertzEnd = m_GranularMixers[loop]->GetPitchValue(m_GranularMixers[loop]->GetNumGrains() - 1);
					const f32 thisHertzMin = Min(thisHertzStart, thisHertzEnd);
					const f32 thisHertzMax = Max(thisHertzStart, thisHertzEnd);
					hertzMin = Max(thisHertzMin, hertzMin);
					hertzMax = Min(thisHertzMax, hertzMax);
				}
			}
		}
	}
	// Use the smallest min/max range, but only calculate the min from channels that decrease in pitch, and the max from channels that increase
	else if(pitchCalculationMode == kGranularPitchMapDirectionalClamp)
	{
		hertzMin = 0.0f;
		hertzMax = 1000.0f;

		for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
		{
			if(m_GranularMixers[loop])
			{
				if(m_GranularMixers[loop]->GetNumGrains() > 50)
				{
					const f32 thisHertzStart = m_GranularMixers[loop]->GetPitchValue(0);
					const f32 thisHertzEnd = m_GranularMixers[loop]->GetPitchValue(m_GranularMixers[loop]->GetNumGrains() - 1);

					if(thisHertzStart < thisHertzEnd)
					{
						hertzMax = Min(thisHertzEnd, hertzMax);
					}
					else
					{
						hertzMin = Max(thisHertzEnd, hertzMin);
					}
				}
			}
		}
	}
	// Use the largest min/max range, but only calculate the min from channels that decrease in pitch, and the max from channels that increase
	else if(pitchCalculationMode == kGranularPitchMapDirectionalMinMax)
	{
		hertzMin = 1000.0f;
		hertzMax = 0.0f;

		for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
		{
			if(m_GranularMixers[loop])
			{
				if(m_GranularMixers[loop]->GetNumGrains() > 50)
				{
					const f32 thisHertzStart = m_GranularMixers[loop]->GetPitchValue(0);
					const f32 thisHertzEnd = m_GranularMixers[loop]->GetPitchValue(m_GranularMixers[loop]->GetNumGrains() - 1);

					if(thisHertzStart < thisHertzEnd)
					{
						hertzMax = Max(thisHertzEnd, hertzMax);
					}
					else
					{
						hertzMin = Min(thisHertzEnd, hertzMin);
					}
				}
			}
		}
	}
	// Average all the min/max pitches from all channels
	else if(pitchCalculationMode == kGranularPitchMapAverage)
	{
		f32 hertzMinAvg = 0.0f;
		f32 hertzMaxAvg = 0.0f;
		u32 numMixes = 0;

		for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
		{
			if(m_GranularMixers[loop])
			{
				if(m_GranularMixers[loop]->GetNumGrains() > 50)
				{
					const f32 thisHertzStart = m_GranularMixers[loop]->GetPitchValue(0);
					const f32 thisHertzEnd = m_GranularMixers[loop]->GetPitchValue(m_GranularMixers[loop]->GetNumGrains() - 1);
					const f32 thisHertzMin = Min(thisHertzStart, thisHertzEnd);
					const f32 thisHertzMax = Max(thisHertzStart, thisHertzEnd);
					hertzMinAvg += thisHertzMin;
					hertzMaxAvg += thisHertzMax;
					numMixes++;
				}
			}
		}

		hertzMin = hertzMinAvg / numMixes;
		hertzMax = hertzMaxAvg / numMixes;
	}

	// Something went wrong - probably because we tried to use a directional method without a full set of assets. Just default back to mix 0.
	if(hertzMin > hertzMax)
	{
		if(m_GranularMixers.GetCount() > 0 &&
			m_GranularMixers[0]->GetNumGrains() > 0)
		{
			const f32 hertzStart = m_GranularMixers[0]->GetPitchValue(0);
			const f32 hertzEnd = m_GranularMixers[0]->GetPitchValue(m_GranularMixers[0]->GetNumGrains() - 1);
			hertzMin = Min(hertzStart, hertzEnd);
			hertzMax = Max(hertzStart, hertzEnd);
		}
	}
}

// ----------------------------------------------------------------
// Called before starting to generate a frame of data
// ----------------------------------------------------------------
void audGrainPlayer::BeginFrame()
{
	AUD_PCMSOURCE_TIMER_BEGINFRAME;

#if __BANK
	g_NumAllocatedVramHelpers = 0;
#endif

	// Because we can have multiple voices running off a single PCM generator, Begin/End frame can get called
	// multiple times for each GenerateFrame. Avoid this by having this mini state machine.
	if(m_GrainPlaybackState != AwaitingBeginFrame)
	{
		return;
	}

	m_SkipGenerationThisFrame = false;

	for(s32 loop = 0; loop < m_GranularMixers.GetCount() && !m_SkipGenerationThisFrame; loop++)
	{
		if(m_GranularMixers[loop])
		{
#if __PS3
			bool vramDataAvailable = m_GranularMixers[loop]->QueryAnyVramLoaderDataAvailable();
#endif
			for(s32 submixLoop = 0; submixLoop < m_GranularMixers[loop]->GetNumSubmixes(); submixLoop++)
			{
				if(!m_GranularMixers[loop]->GetGranularSubmix(submixLoop)->HaveInitialPacketsBeenGenerated() 
#if __PS3
					|| (m_GranularMixers[loop]->GetGranularSubmix(submixLoop)->IsPlayingPhysically() && !vramDataAvailable)
#endif
				)
				{
					m_SkipGenerationThisFrame = true;
					continue;
				}
			}
		}
	}

	for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
	{
		m_GranularClocks[loop].BeginFrame();
	}

	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop] && m_GranularMixers[loop]->IsInitialised()) 
		{
			if(audVerifyf(m_GranularMixers[loop]->GetGranularClockIndex() < m_GranularClocks.GetCount(), "Invalid clock index %d", m_GranularMixers[loop]->GetGranularClockIndex()))
			{
				audGranularClock* granularClock = &m_GranularClocks[m_GranularMixers[loop]->GetGranularClockIndex()];
				m_GranularMixers[loop]->BeginFrame(granularClock->GetCurrentRateHz(), granularClock->GetPredictedRateHz(), granularClock->GetXValueStepPerGranule(), granularClock->GetWobbleVolumeModulation());
			}
		}
	}

	m_GrainPlaybackState = AwaitingGenerateFrame;
}

// ----------------------------------------------------------------
// Called after generating a frame of data
// ----------------------------------------------------------------
void audGrainPlayer::EndFrame()
{
	AUD_PCMSOURCE_TIMER_ENDFRAME;

	// Because we can have multiple voices running off a single PCM generator, Begin/End frame can get called
	// multiple times for each GenerateFrame. Avoid this by having this mini state machine.
	if(m_GrainPlaybackState != AwaitingEndFrame)
	{
		return;
	}

	for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
	{
		m_GranularClocks[loop].EndFrame();
	}

	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop] && m_GranularMixers[loop]->IsInitialised()) 
		{
			if(audVerifyf(m_GranularMixers[loop]->GetGranularClockIndex() < m_GranularClocks.GetCount(), "Invalid clock index %d", m_GranularMixers[loop]->GetGranularClockIndex()))
			{
				audGranularClock* granularClock = &m_GranularClocks[m_GranularMixers[loop]->GetGranularClockIndex()];
				m_GranularMixers[loop]->EndFrame(granularClock->GetCurrentRateHz() PS3_ONLY(, granularClock->GetPredictedRateHz(), granularClock->GetXValueStepPerBuffer()));
			}
		}
	}

	SetVariable(ATSTRINGHASH("granularfrequency", 0x3DD2C9E3), m_GranularClocks[0].GetCurrentRateHz() * m_GranularClocks[0].GetFrequencyScalingFactor());

	f32 frequencyRange = m_GranularClocks[0].GetMaxPitch() - m_GranularClocks[0].GetMinPitch();
	SetVariable(ATSTRINGHASH("granularfrequencyratio", 0x6AF66575), Clamp((m_GranularClocks[0].GetCurrentRateHz() - m_GranularClocks[0].GetMinPitch())/frequencyRange, 0.0f, 1.0f));

	bool grainSkippingActive = false;

	for(s32 mixerLoop = 0; mixerLoop < m_GranularMixers.GetCount(); mixerLoop++)
	{
		audGranularMix* granularMix = m_GranularMixers[mixerLoop];

		if(granularMix && granularMix->IsInitialised() && !granularMix->IsMuted())
		{
			for(s32 submixLoop = 0; submixLoop < granularMix->GetNumSubmixes(); submixLoop++)
			{
				audGranularSubmix* granularSubmix = granularMix->GetGranularSubmix(submixLoop);
				
				if(granularSubmix && granularSubmix->IsSkippingCurrentGrain() && granularSubmix->GetPrevVolumeScale() > 0)
				{
					grainSkippingActive = true;
					break;
				}
			}
		}
	}

	SetVariable(ATSTRINGHASH("grainskippingactive", 0xFB63030F), grainSkippingActive? 1.f : 0.f);

	for(s32 clockLoop = 0; clockLoop < m_GranularClocks.GetCount(); clockLoop++)
	{
		u32 numMixersUsingClock = 0;

		for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
		{
			if(m_GranularMixers[loop] && 
			   m_GranularMixers[loop]->GetGranularClockIndex() == clockLoop && 
			   m_GranularMixers[loop]->IsInitialised() && 
			   !m_GranularMixers[loop]->IsMuted()) 
			{
				numMixersUsingClock++;
			}
		}

		// If nothing is using this clock, reset the granular position. This means that when we restart it, we won't need
		// to do a big decode to catch up with the current read position
		if(numMixersUsingClock == 0)
		{
			m_GranularClocks[clockLoop].ResetGranularFraction();
		}
	}

	m_GrainPlaybackState = AwaitingBeginFrame;
}
#endif //!__SPU

// ----------------------------------------------------------------
// Generate a frame of PCM data
// ----------------------------------------------------------------
void audGrainPlayer::GenerateFrame()
{
	AUD_PCMSOURCE_TIMER_GENERATEFRAME;
	PF_FUNC(GenerateFrameGrainPlayer);

	// Whatever happens, if we have a buffer we need to silence it
	for(s32 channel = 0; channel < kMaxPcmSourceChannels; channel++)
	{
		if(HasBuffer(channel))
		{
			g_FrameBufferCache->ZeroBuffer(GetBufferId(channel));
		}
	}

	if(m_GrainPlaybackState != AwaitingGenerateFrame)
	{
		return;
	}

	m_NumGeneratedSamples += kMixBufNumSamples;

	if(m_SkipGenerationThisFrame)
	{
		m_GrainPlaybackState = AwaitingEndFrame;
	}

#if __SPU
	audGranularSubmix::s_ScratchDecoderIndex = 0;
	audGranularSubmix::s_NumDecodesThisFrame = 0;
	audGranularSubmix::s_NumActiveSubmixes = 0;
	audGranularSubmix::s_NumSubixesStartedThisFrame = 0;
	const u32 mixSize = Align16(sizeof(audGranularMix));
	u32 scratchMixTag = 17;

	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop])
		{
			dmaGet(g_ScratchMixers[loop], m_GranularMixers[loop], mixSize, false, scratchMixTag + loop);
		}
	}
#endif

	// Native mixers go first
	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		audGranularMix* granularMix = NULL;

#if __SPU
		dmaWait(scratchMixTag + loop);
		granularMix = (audGranularMix*) g_ScratchMixers[loop];
#else
		granularMix = m_GranularMixers[loop];
#endif

		if(granularMix && granularMix->IsInitialised())
		{
			if(granularMix->ShouldMixNatve())
			{
				audGranularSubmix* primarySubmix = granularMix->GetGranularSubmix(0);
				f32 startGranuleFraction = primarySubmix->GetGranuleFractionInternal();
				u32 startGrainsGenerated = primarySubmix->GetNumGrainsGenerated();

				Mix(loop);

				f32 endGranuleFraction = granularMix->GetGranularSubmix(0)->GetGranuleFractionInternal();
				u32 endGrainsGenerated = primarySubmix->GetNumGrainsGenerated();
				f32 granularFractionChange = (endGrainsGenerated + endGranuleFraction) - (startGrainsGenerated + startGranuleFraction); 
				f32 granularFractionPerSample = granularFractionChange/kMixBufNumSamples;

				m_GranularClocks[granularMix->GetGranularClockIndex()].SetGranularFractionPerSample(granularFractionPerSample);
			}			
		}
	}

	// Regular mixers last
	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		audGranularMix* granularMix = NULL;

#if __SPU
		dmaWait(scratchMixTag + loop);
		granularMix = (audGranularMix*) g_ScratchMixers[loop];
#else
		granularMix = m_GranularMixers[loop];
#endif

		if(m_GranularMixers[loop])
		{
			if(!m_GranularMixers[loop]->ShouldMixNatve())
			{
				Mix(loop);
			}			
		}
	}

#if __SPU
	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop])
		{
			audGranularMix* granularMix = (audGranularMix*) g_ScratchMixers[loop];

			if(granularMix->IsInitialised())
			{
				granularMix->SpuPostMix();
			}

			dmaPut(granularMix, m_GranularMixers[loop], mixSize, false, scratchMixTag + loop);
		}
	}

	sysDmaWaitTagStatusAll(SYS_DMA_MASK_ALL);
#endif

	m_GrainPlaybackState = AwaitingEndFrame;
}

// ----------------------------------------------------------------
// audGrainPlayer Mix
// ----------------------------------------------------------------
void audGrainPlayer::Mix(u32 granularMixIndex SPU_ONLY(, u32 scratchMixTag))
{
	audGranularMix* granularMix = NULL;

#if __SPU
	dmaWait(scratchMixTag + granularMixIndex);
	granularMix = (audGranularMix*) g_ScratchMixers[granularMixIndex];
#else
	granularMix = m_GranularMixers[granularMixIndex];
#endif

	if(granularMix->IsInitialised())
	{
		u32 channel = granularMix->GetOutputChannel();
		Assert(HasBuffer(channel));
		f32 *destBuffer = g_FrameBufferCache->GetBuffer(GetBufferId(channel));

		if(audVerifyf(granularMix->GetGranularClockIndex() < m_GranularClocks.GetCount(), "Invalid clock index %d", granularMix->GetGranularClockIndex()))
		{
			audGranularClock* granularClock = &m_GranularClocks[granularMix->GetGranularClockIndex()];
			granularMix->GenerateFrame(destBuffer, granularClock->GetNumGrainsGenerated(), granularClock->GetCurrentGranularFraction(), granularClock->GetGranularFractionPerSample());
		}
	}
}

#if !__SPU
// ----------------------------------------------------------------
// audGrainPlayer SetParam u32
// ----------------------------------------------------------------
void audGrainPlayer::SetParam(const u32 paramId, const u32 val)
{
	switch(paramId)
	{
	case audGrainPlayer::Params::GrainRandomisationStyle:
		for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
		{
			if(m_GranularMixers[loop] && !m_GranularMixers[loop]->IsPureRandomMix())
			{
				m_GranularMixers[loop]->GetGranularSubmix(0)->SetPlaybackOrder((audGranularSubmix::audGrainPlaybackOrder)val);
			}
		}
		break;
	case audGrainPlayer::Params::SetPitchLocked:
		{	
			for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
			{
				m_GranularClocks[loop].SetPitchLocked(val == 1);
			}

			for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
			{
				if(m_GranularMixers[loop])
				{
					m_GranularMixers[loop]->SetPitchLocked(val == 1);
				}
			}
		}
		break;
	case audGrainPlayer::Params::SnapToNearestPlaybackType:
		{
			u32 channel = val;

			if(m_GranularMixers[channel])
			{
				m_GranularMixers[channel]->SnapToNearestPlaybackType();
			}
		}
		break;
	case audGrainPlayer::Params::TimeStep:
		m_TimeStep = val;
		break;
	case audGrainPlayer::Params::AllocateVariableBlock:
		{
			if(!GetVariableBlock())
			{
				AllocateVariableBlock(ATSTRINGHASH("GRAINPLAYER_VARIABLES", 0xE9985B82));
				audAssert(GetVariableBlock());
				UpdateVirtualisedVariableBlockValues(m_GranularClocks[0].GetXCurrent());
			}
		}
		break;
#if __BANK
	case audGrainPlayer::Params::SetDebugRenderingEnabled:
		if(val == 1)
		{
			s_DebugDrawGrainPlayer = this;
		}
		else if(s_DebugDrawGrainPlayer == this)
		{
			s_DebugDrawGrainPlayer = NULL;
		}
		break;
	case audGrainPlayer::Params::ShiftLoopLeft:
		if(s_DebugDrawGrainPlayer && s_LoopEditorActive)
		{
			if(s_LoopEditorMixIndex < (u32)m_GranularMixers.GetCount())
			{
				m_GranularMixers[s_LoopEditorMixIndex]->LoopEditorShiftLeft(s_LoopEditorShiftAmount);
			}
		}
		break;
	case audGrainPlayer::Params::ShiftLoopRight:
		if(s_DebugDrawGrainPlayer && s_LoopEditorActive)
		{
			if(s_LoopEditorMixIndex < (u32)m_GranularMixers.GetCount())
			{
				m_GranularMixers[s_LoopEditorMixIndex]->LoopEditorShiftRight(s_LoopEditorShiftAmount);
			}
		}
		break;
	case audGrainPlayer::Params::GrowLoop:
		if(s_DebugDrawGrainPlayer && s_LoopEditorActive)
		{
			if(s_LoopEditorMixIndex < (u32)m_GranularMixers.GetCount())
			{
				m_GranularMixers[s_LoopEditorMixIndex]->LoopEditorGrowLoop();
			}
		}
		break;
	case audGrainPlayer::Params::ShrinkLoop:
		if(s_DebugDrawGrainPlayer && s_LoopEditorActive)
		{
			if(s_LoopEditorMixIndex < (u32)m_GranularMixers.GetCount())
			{
				m_GranularMixers[s_LoopEditorMixIndex]->LoopEditorShrinkLoop();
			}
		}
		break;
	case audGrainPlayer::Params::CreateLoop:
		if(s_DebugDrawGrainPlayer && s_LoopEditorActive)
		{
			if(s_LoopEditorMixIndex < (u32)m_GranularMixers.GetCount())
			{
				m_GranularMixers[s_LoopEditorMixIndex]->LoopEditorCreateLoop();
			}
		}
		break;
	case audGrainPlayer::Params::DeleteLoop:
		if(s_DebugDrawGrainPlayer && s_LoopEditorActive)
		{
			if(s_LoopEditorMixIndex < (u32)m_GranularMixers.GetCount())
			{
				m_GranularMixers[s_LoopEditorMixIndex]->LoopEditorDeleteLoop();
			}
		}
		break;
	case audGrainPlayer::Params::ExportData:
		{
			if(s_LoopEditorMixIndex < (u32)m_GranularMixers.GetCount())
			{
				m_GranularMixers[s_LoopEditorMixIndex]->LoopEditorExportData(s_LoopEditorMixIndex);
			}
		}
		break;
	case audGrainPlayer::Params::ToneGeneratorEnabled:
		{
			for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
			{
				if(m_GranularMixers[loop])
				{
					m_GranularMixers[loop]->SetToneGeneratorEnabled(val == 1);
				}
			}
		}
#endif
	case audPcmSource::Params::StartOffsetMs:
		/* not supported */
		break;
	default:
		Assert(0);
		break;
	}
}

// ----------------------------------------------------------------
// audGrainPlayer UpdateVariableBlockValues
// ----------------------------------------------------------------
void audGrainPlayer::UpdateVirtualisedVariableBlockValues(f32 xVal)
{
	if(GetVariableBlock())
	{
		f32 currentRateHz = m_GranularClocks[0].GetMinPitch() + ((m_GranularClocks[0].GetMaxPitch() - m_GranularClocks[0].GetMinPitch()) * xVal);
		SetVariable(ATSTRINGHASH("granularfrequency", 0x3DD2C9E3), currentRateHz * m_GranularClocks[0].GetFrequencyScalingFactor());
		SetVariable(ATSTRINGHASH("granularfrequencyratio", 0x6AF66575), Clamp(currentRateHz/m_GranularClocks[0].GetMaxPitch(), 0.0f, 1.0f) * m_GranularClocks[0].GetFrequencyScalingFactor());
		SetVariable(ATSTRINGHASH("grainskippingactive", 0xFB63030F), 0.f);
	}
}

// ----------------------------------------------------------------
// audGrainPlayer SetParam f32
// ----------------------------------------------------------------
void audGrainPlayer::SetParam(const u32 paramId, const f32 val)
{
	switch(paramId)
	{
	case audGrainPlayer::Params::XValue:
		{
			for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
			{
				m_GranularClocks[loop].SetX(val, m_TimeStep);
			}

			// Ensure our variable block is kept up to date with sensible values even if we're not physically playing
			if(m_NumPhysicalClients == 0)
			{
				UpdateVirtualisedVariableBlockValues(val);
			}

			m_CurrentPeakLevelSample = 0u;

			for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
			{
				if(m_GranularMixers[loop])
				{
					m_CurrentPeakLevelSample = Max(m_CurrentPeakLevelSample, m_GranularMixers[loop]->ComputeCurrentPeak(val));
				}
			}
		}
		break;
	case audGrainPlayer::Params::XValueForced:
		{
			for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
			{
				m_GranularClocks[loop].ForceX(val);
			}

			// Ensure our variable block is kept up to date with sensible values even if we're not physically playing
			if(m_NumPhysicalClients == 0)
			{
				UpdateVirtualisedVariableBlockValues(val);
			}
		}
		break;
	case audGrainPlayer::Params::XValueSmoothRate:
		{
			for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
			{
				m_GranularClocks[loop].SetXValueSmoothRate(val);
			}
		}
		break;
	case audPcmSource::Params::FrequencyScalingFactor:
		{
			for(s32 loop = 0; loop < m_GranularClocks.GetCount(); loop++)
			{
				m_GranularClocks[loop].SetFrequencyScalingFactor(val);
			}
		}
		break;
	case audGrainPlayer::Params::SetGranularChangeRateForLoops:
		{
			for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
			{
				if(m_GranularMixers[loop])
				{
					m_GranularMixers[loop]->SetGranularChangeRateForLoops(val);
				}
			}
		}
		break;
	default:
		Assert(0);
		break;
	}
}

// ----------------------------------------------------------------
// Handle a custom command packet
// ----------------------------------------------------------------
void audGrainPlayer::HandleCustomCommandPacket(const audPcmSourceCustomCommandPacket* packet)
{
	if(packet)
	{
		switch(packet->paramId)
		{
		case Params::InitParams:
			{
				audInitGrainPlayerCommandPacket* initGrainPlayerPacket = (audInitGrainPlayerCommandPacket*)packet;
				audGranularMix* newGranularMix = rage_aligned_new(16) audGranularMix();

				if(newGranularMix->Init(initGrainPlayerPacket->waveSlot, initGrainPlayerPacket->waveNameHash, initGrainPlayerPacket->outputChannel, initGrainPlayerPacket->granularClock, initGrainPlayerPacket->matchMinPitch, initGrainPlayerPacket->matchMaxPitch, initGrainPlayerPacket->maxLoopProportion))
				{
					newGranularMix->SetVolume(initGrainPlayerPacket->initialVolume, 1.0f);
					newGranularMix->SetMuted(initGrainPlayerPacket->initialVolume <= 0.0f);
					newGranularMix->SetAllowLoopGrainOverlap(initGrainPlayerPacket->allowLoopGrainOverlap);

					if(initGrainPlayerPacket->quality != audGranularMix::GrainPlayerQualityMax)
					{
						newGranularMix->SetQuality(initGrainPlayerPacket->quality);
					}
					
					audAssert(!m_GranularMixers[initGrainPlayerPacket->channel]);
					m_GranularMixers[initGrainPlayerPacket->channel] = newGranularMix;
				}
				else
				{
					audWarningf("Failed to initialise granular mix!");
					delete newGranularMix;
				}
			}
			break;
		case audGrainPlayer::Params::GrainPlaybackStyle:
			{
				audGrainPlayer::audGrainPlayerSetPlaybackStylePacket* playbackStylePacket = (audGrainPlayer::audGrainPlayerSetPlaybackStylePacket*)packet;

				if(m_GranularMixers[playbackStylePacket->channel])
				{
					m_GranularMixers[playbackStylePacket->channel]->SetPlaybackStyle(playbackStylePacket->style);
				}
			}
			break;
		case audGrainPlayer::Params::SetGranularSlidingWindowSize:
			{
				audGrainPlayer::audGrainPlayerSetSlidingWindowSizePacket* slidingWindowPacket = (audGrainPlayer::audGrainPlayerSetSlidingWindowSizePacket*)packet;

				for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
				{
					if(m_GranularMixers[loop])
					{
						m_GranularMixers[loop]->SetGranularSlidingWindowSize(slidingWindowPacket->windowSizeHz, slidingWindowPacket->minGrains, slidingWindowPacket->maxGrains);
						m_GranularMixers[loop]->SetMinGrainRepeatRate(slidingWindowPacket->minRepeatRate);
					}
				}
			}
			break;
		case audGrainPlayer::Params::MuteChannel:
			{
				audGrainPlayerSetChannelFlagPacket* muteChannelPacket = (audGrainPlayerSetChannelFlagPacket*)packet;

				if(m_GranularMixers[muteChannelPacket->channel])
				{
					m_GranularMixers[muteChannelPacket->channel]->SetMuted(muteChannelPacket->flag);
				}
			}
			break;

		case audGrainPlayer::Params::SetMixNative:
			{
				audGrainPlayerSetChannelFlagPacket* mixNativePacket = (audGrainPlayerSetChannelFlagPacket*)packet;

				if(m_GranularMixers[mixNativePacket->channel])
				{
					m_GranularMixers[mixNativePacket->channel]->SetMixNative(mixNativePacket->flag);
				}
			}
			break;

		case audGrainPlayer::Params::SetChannelVolume:
			{
				audGrainPlayerSetChannelVolumePacket* setChannelVolumePacket = (audGrainPlayerSetChannelVolumePacket*)packet;

				if(m_GranularMixers[setChannelVolumePacket->channel])
				{
					if(setChannelVolumePacket->masterVolume)
					{
						m_GranularMixers[setChannelVolumePacket->channel]->SetMasterVolumeScale(setChannelVolumePacket->volume);
					}
					else
					{
						m_GranularMixers[setChannelVolumePacket->channel]->SetVolume(setChannelVolumePacket->volume, setChannelVolumePacket->smoothRate);
					}
				}
			}
			break;
		case Params::SetLoopEnabled:
			{
				audGrainPlayerSetLoopEnabledPacket* loopEnabledPacket = (audGrainPlayerSetLoopEnabledPacket*)packet;

				if(m_GranularMixers[loopEnabledPacket->channel])
				{
					m_GranularMixers[loopEnabledPacket->channel]->HandleCustomCommandPacket(packet);
				}
			}
			break;
		case Params::SetGrainSkippingEnabled:
			{
				audGrainPlayerSetGrainSkippingEnabledPacket* grainSkipPacket = (audGrainPlayerSetGrainSkippingEnabledPacket*)packet;

				if(m_GranularMixers[grainSkipPacket->channel])
				{
					for(s32 loop = 0; loop < m_GranularMixers[grainSkipPacket->channel]->GetNumSubmixes(); loop++)
					{
						m_GranularMixers[grainSkipPacket->channel]->GetGranularSubmix(loop)->SetGrainSkippingEnabled(grainSkipPacket->enabled, grainSkipPacket->numToPlay, grainSkipPacket->numToSkip, grainSkipPacket->skippedGrainVol);
					}
				}
			}
			break;
		case Params::SetAsPureRandomPlayer:
			{
				audGrainPlayerSetChannelFlagPacket* pureRandomPacket = (audGrainPlayerSetChannelFlagPacket*)packet;

				if(m_GranularMixers[pureRandomPacket->channel])
				{
					m_GranularMixers[pureRandomPacket->channel]->SetPureRandom(pureRandomPacket->flag);
				}
			}
			break;
		case Params::SetAsPureRandomPlayerWithStyle:
			{
				audGrainPlayerSetChannelU32Packet* pureRandomPacket = (audGrainPlayerSetChannelU32Packet*)packet;

				if(m_GranularMixers[pureRandomPacket->channel])
				{
					m_GranularMixers[pureRandomPacket->channel]->SetPureRandomWithStyle((audGranularSubmix::audGrainPlaybackOrder)pureRandomPacket->value);
				}
			}
			break;
		case Params::SetLoopRandomisationParams:
			{
				audGrainPlayerSetLoopRandomisationParams* randomisationPacket = (audGrainPlayerSetLoopRandomisationParams*)packet;

				for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
				{
					if(m_GranularMixers[loop])
					{
						m_GranularMixers[loop]->SetLoopRandomisationEnabled(randomisationPacket->randomisationEnabled, randomisationPacket->randomisationChangeRate, randomisationPacket->randomisationMaxPitchFraction);
					}
				}
			}
			break;
		case audGrainPlayer::Params::SetWobbleEnabled:
			{
				audGrainPlayer::audGrainPlayerSetWobbleEnabledPacket* wobbleEnabledPacket = (audGrainPlayer::audGrainPlayerSetWobbleEnabledPacket*)packet;

				if(wobbleEnabledPacket->wobbleLength > 0)
				{
					m_GranularClocks[wobbleEnabledPacket->grainClock].GetWobble()->Init(wobbleEnabledPacket->wobbleLength, wobbleEnabledPacket->wobbleSpeed, wobbleEnabledPacket->wobbleVolume, wobbleEnabledPacket->wobblePitch);
				}
				else
				{
					m_GranularClocks[wobbleEnabledPacket->grainClock].GetWobble()->SetActive(false);
				}
			}
			break;
		case audGrainPlayer::Params::InitGranularClock:
			{
				audGrainPlayer::audGrainPlayerInitGranularClockPacket* initGranularClockPacket = (audGrainPlayer::audGrainPlayerInitGranularClockPacket*)packet;
				audAssert(initGranularClockPacket->clockIndex < kMaxGranularClocksPerPlayer);

				// Create new/update existing clock as appropriate
				if((s32)initGranularClockPacket->clockIndex <= m_GranularClocks.GetCount())
				{
					audGranularClock granularClock;
					m_GranularClocks.Push(granularClock);
				}

				if(initGranularClockPacket->minHertz == 0.0f && initGranularClockPacket->maxHertz == 0.0f)
				{
					f32 minHertz = 0.0f;
					f32 maxHertz = 0.0f;
					CalculateMinMaxPitch(kGranularPitchMapChannel0, minHertz, maxHertz);
					m_GranularClocks[initGranularClockPacket->clockIndex].SetMinMaxPitch(minHertz, maxHertz);
				}
				else
				{
					m_GranularClocks[initGranularClockPacket->clockIndex].SetMinMaxPitch(initGranularClockPacket->minHertz, initGranularClockPacket->maxHertz);
				}
				
				// Inform any submixes of the change
				for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
				{
					if(m_GranularMixers[loop])
					{
						if(m_GranularMixers[loop]->GetGranularClockIndex() == initGranularClockPacket->clockIndex)
						{
							m_GranularMixers[loop]->SetMinMaxPitch(m_GranularClocks[initGranularClockPacket->clockIndex].GetMinPitch(), m_GranularClocks[initGranularClockPacket->clockIndex].GetMaxPitch());
						}
					}
				}
			}
			break;
		default:
			Assert(0);
			break;
		}
	}
}

// ----------------------------------------------------------------
// audGrainPlayer shutdown
// ----------------------------------------------------------------
void audGrainPlayer::Shutdown()
{
#if __BANK
	if(s_DebugDrawGrainPlayer == this)
	{
		s_DebugDrawGrainPlayer = NULL;
	}
#endif

	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop])
		{
			m_GranularMixers[loop]->Shutdown();
			delete m_GranularMixers[loop];
		}
	}

	ReleaseVariableBlock();
	m_GranularMixers.Reset();
	FreeBuffers();
}
#endif // !__SPU

// ----------------------------------------------------------------
// Returns the current playback position of the wave, in samples at the native
//	sample rate.
// ----------------------------------------------------------------	
u32 audGrainPlayer::GetPlayPositionSamples() const
{
	return m_NumGeneratedSamples;
}

// ----------------------------------------------------------------
// audGrainPlayer Stop
// ----------------------------------------------------------------	
void audGrainPlayer::Stop()
{
	m_FinishedPlayback = true;
}

#if !__SPU
// ----------------------------------------------------------------
// audGrainPlayer startPhys
// ----------------------------------------------------------------
void audGrainPlayer::StartPhys(s32 channel)
{
	if(++m_NumPhysicalClients == 1)
	{
		for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
		{
			if(m_GranularMixers[loop])
			{
				u32 mixerChannel = m_GranularMixers[loop]->GetOutputChannel();
				if(!HasBuffer(mixerChannel))
				{
					AllocateBuffer(mixerChannel);
				}

				if(loop < 4)
				{
					m_GranularMixers[loop]->SetPlaybackStyle(m_GranularMixers[loop]->GetQuality() == audGranularMix::GrainPlayerQualityHigh? audGranularMix::PlaybackStyleLoopsAndGrains : audGranularMix::PlaybackStyleLoopsOnly);
				}
			}
		}
	}

	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop])
		{
			if(channel == (s32)m_GranularMixers[loop]->GetOutputChannel())
			{
				m_GranularMixers[loop]->StartPhys();
			}
		}
	}
}
#endif

// ----------------------------------------------------------------
// audGrainPlayer stopPhys
// ----------------------------------------------------------------
void audGrainPlayer::StopPhys(s32 channel)
{
	for(s32 loop = 0; loop < m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop])
		{
			if(channel == (s32)m_GranularMixers[loop]->GetOutputChannel())
			{
				m_GranularMixers[loop]->StopPhys();
			}
		}
	}

	if(--m_NumPhysicalClients == 0)
	{
		FreeBuffers();
	}
}

#if __BANK && !__SPU
// ----------------------------------------------------------------
// audGrainPlayer debug draw pitch calc mode
// ----------------------------------------------------------------
void audGrainPlayer::DebugDrawPitchMapping(const char* title, GranularPitchCalcMode pitchMode, u32 offset)
{
	f32 minHertz = 0.0f;
	f32 maxHertz = 0.0f;
	char tempString[128];

	CalculateMinMaxPitch(pitchMode, minHertz, maxHertz);
	sprintf(tempString, "%s: Min %.02f hz (%d rpm) - Max %.02f hz (%d rpm)", title, minHertz, (u32)(minHertz * 120.0f), maxHertz, (u32)(maxHertz * 120.0f));
	grcDebugDraw::Text(Vector2(0.08f, 0.64f + (0.02f * offset)), Color32(255,255,255), tempString);
}

// ----------------------------------------------------------------
// audGrainPlayer debug draw
// ----------------------------------------------------------------
void audGrainPlayer::DebugDraw()
{
	PUSH_DEFAULT_SCREEN();
	char tempString[128];
	sprintf(tempString, "Current Peak Sample: %.02f", GetCurrentPeakLevel()/65536.0f);
	grcDebugDraw::Text(Vector2(0.08f, 0.56f), Color32(255,255,255), tempString);

	sprintf(tempString, "Suggested Pitch Mappings:");
	grcDebugDraw::Text(Vector2(0.08f, 0.6f), Color32(255,255,255), tempString);

	sprintf(tempString, "Current: Min %.02f hz (%d rpm) - Max %.02f hz (%d rpm)", m_GranularClocks[0].GetMinPitch(), (u32)(m_GranularClocks[0].GetMinPitch() * 120.0f), m_GranularClocks[0].GetMaxPitch(), (u32)(m_GranularClocks[0].GetMaxPitch() * 120.0f));
	grcDebugDraw::Text(Vector2(0.08f, 0.64f), Color32(255,255,255), tempString);

	DebugDrawPitchMapping("Channel 0", kGranularPitchMapChannel0, 1);
	DebugDrawPitchMapping("Channel 1", kGranularPitchMapChannel1, 2);
	DebugDrawPitchMapping("Channel 2", kGranularPitchMapChannel2, 3);
	DebugDrawPitchMapping("Channel 3", kGranularPitchMapChannel3, 4);
	DebugDrawPitchMapping("Channel 4", kGranularPitchMapChannel4, 5);
	DebugDrawPitchMapping("Channel 5", kGranularPitchMapChannel5, 6);
	DebugDrawPitchMapping("Clamp", kGranularPitchMapClamp, 7);
	DebugDrawPitchMapping("Directional Clamp", kGranularPitchMapDirectionalClamp, 8);
	DebugDrawPitchMapping("Directional Min/Max", kGranularPitchMapDirectionalMinMax, 9);
	DebugDrawPitchMapping("Average", kGranularPitchMapAverage, 10);

	for(u32 loop = 0; loop < (u32)m_GranularMixers.GetCount(); loop++)
	{
		if(m_GranularMixers[loop])
		{
			if(s_LoopEditorActive && s_LoopEditorMixIndex == loop)
			{
				m_GranularMixers[loop]->SetLoopEditorActive(true);
				m_GranularMixers[loop]->SetLoopEditorLoopIndex(s_LoopEditorLoopIndex);
			}
			else
			{
				m_GranularMixers[loop]->SetLoopEditorActive(false);
			}

			m_GranularMixers[loop]->DebugDrawGrainTable(45 * loop, m_GranularClocks[0].GetXCurrent());
		}
	}
	POP_DEFAULT_SCREEN();
}
#endif

} // namespace rage
