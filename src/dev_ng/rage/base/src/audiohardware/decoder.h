//
// audiohardware/decoder.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DECODER_H
#define AUD_DECODER_H

#include "waveref.h"
#include "driverdefs.h"

#define AUDIODEC_HANDLE_INVALID ((int32_t)-1)

namespace rage
{
typedef u32 audPacketId;
typedef u32 audStreamId;

struct audPacket
{
	audPacket() :
		inputData(NULL),
		inputBytes(0U),
		PlayBegin(0),
		PlayEnd(0),
		LoopBegin(~0U),
		LoopEnd(0),
		SeekTable(NULL),
		SeekTableSize(0),
		WaveSlotId(-1),
		SkipFirstFrame(false)
#if RSG_AUDIO_X64
		,IsFinalBlock(false)
		,BlockStartSample(0)
#endif
		BANK_ONLY(,WaveNameHash(0))
	{

	}
	const void *inputData;
	u32 inputBytes;
	u32 PlayBegin;
	u32 PlayEnd;
	u32 LoopBegin;
	u32 LoopEnd;
	const u16 *SeekTable;
	u32 SeekTableSize;
	s32 WaveSlotId;
	bool SkipFirstFrame;
#if RSG_AUDIO_X64
	bool IsFinalBlock;
	u32 BlockStartSample;
#endif
#if __BANK
	u32 WaveNameHash;
#endif
};

struct audDecodePosition
{
	u32 frameOffset;
	u32 subFrameOffset;
};


// PURPOSE
//	Abstract interface for a wave decoder
class audDecoder
{
public:
	enum audDecoderState
	{
		IDLE = 0,
		DECODING,
		FINISHED,
		DECODER_ERROR,
	};
	virtual ~audDecoder(){}
	virtual bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 sampleRate) = 0;
	virtual void Shutdown() = 0;
	
	virtual audDecoderState GetState() const = 0;
	virtual void SubmitPacket(audPacket &packet) = 0;
	virtual void Update(){}
	virtual void PreUpdate(){}
	virtual u32 ReadData(s16 *dest, const u32 numSamples) const = 0;
	virtual void AdvanceReadPtr(const u32 numSamples) = 0;
	virtual u32 QueryNumAvailableSamples() const = 0;
	virtual bool QueryReadyForMoreData() const = 0;
	virtual bool IsSafeToDelete() { return true; }

	virtual void SetWaveIdentifier(const u32 waveId) = 0;

};

}//namespace rage
#endif // AUD_DECODER_H
