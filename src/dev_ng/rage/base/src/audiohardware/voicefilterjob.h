// 
// audiohardware/voicefilterjob.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_VOICEFILTERJOB_H
#define AUD_VOICEFILTERJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(voicefilterjob);

namespace rage
{
	
}

#endif // AUD_VOICEFILTERJOB_H

