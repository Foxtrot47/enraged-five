//
// audiohardware/device_pc.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#if __WIN32PC

#include "system/xtl.h"
#include "driver.h"
#include "device.h"
#include "device_pc.h"
#include "submix.h"

// for benchmark:
#include "audiosynth/synthesizer.h"

#include "diag/diagerrorcodes.h"
#include "system/param.h"
#include "system/performancetimer.h"

#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning(disable: 4201)
// #if condition not defined
#pragma warning(disable: 4668)

#if _WIN10
#include <mmreg.h>
#endif

#define NONEWWAVE 1 // this prevents a redef issue in mmreg.h cuased by audiodefs.h
#include <dsound.h>
#if !_WIN10
#include <audiodefs.h>
#endif

// reenable the warnings
#pragma warning(pop)

#pragma comment(lib,"dsound.lib")
#pragma comment(lib,"dxguid.lib")

#include "audioengine/engineutil.h"

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if(p) { p->Release(); p = NULL; }
#endif

namespace rage
{

	const s32 g_NumDSPasses = 2;
	extern struct HWND__ *g_hwndMain;

const sysIpcPriority audMixerThreadPriority = PRIO_TIME_CRITICAL;
const int audMixerThreadCpu = 0;


f32 g_OutputGain = 2.f;
const u32 g_DefaultDSBuffers = 8;

BANK_ONLY(u32 g_RequestedNumDSBuffers = g_DefaultDSBuffers);
BANK_ONLY(f32 g_MixTimeS);
BANK_ONLY(f32 g_MixRealtimeRatio);

bool audMixerDevicePc::CheckForSoundCard()
{
	LPDIRECTSOUND lpDS;
	bool retVal = false;
	if(SUCCEEDED(DirectSoundCreate(&DSDEVID_DefaultPlayback, &lpDS,  NULL)))
	{
		retVal = true;
	}
	SAFE_RELEASE(lpDS);
	return retVal;	
}

BOOL CALLBACK DSEnumCallback(LPGUID lpGUID, 
							 LPCTSTR lpszDesc,
							 LPCTSTR lpszDrvName, 
							 LPVOID lpContext)
{
	GUID mixerDevice = *((GUID*)lpContext);
	if(lpGUID && *lpGUID==mixerDevice)
	{
#if !__ASSERT && __WIN32PC
		(void) lpszDesc; (void) lpszDrvName;	// keep the compiler happy
#endif
		audDisplayf("Sound card: '%s' '%s'", lpszDesc, lpszDrvName);
		return false;
	}
	return true;
}

audMixerDevicePc::audMixerDevicePc() : 
m_DS(NULL), 
m_DSBuffer(NULL), 
m_NumDSBuffers(0),
m_LastGeneratedBufferIdx(0),
m_OutputNonInterleaved(NULL),
m_IsMixThreadRunning(false),
m_ShouldMixThreadBeRunning(false)
{

}

bool audMixerDevicePc::InitHardware()
{
	m_NumDSBuffers = g_DefaultDSBuffers;

#if OUTPUT_MIXBUFFER_AUDIO
	m_hMixerFile = CreateFile("c:\\MixBuffersOut.raw", GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);
#endif

	if(!InitOutputHardwareDS())
	{
		return false;
	}
	return true;
}

bool audMixerDevicePc::InitOutputHardwareDS(bool reinit)
{
	DSBUFFERDESC desc;
	WAVEFORMATEXTENSIBLE wfx;

	HRESULT hr = DirectSoundCreate(&DSDEVID_DefaultPlayback, &m_DS,  NULL);
	if(SUCCEEDED(hr))
	{
		m_DS->SetCooperativeLevel(g_hwndMain, DSSCL_EXCLUSIVE);

		ZeroMemory(&desc, sizeof(desc));
		desc.dwFlags = DSBCAPS_PRIMARYBUFFER;
		desc.dwSize = sizeof(desc);

		LPDIRECTSOUNDBUFFER primaryBuffer;
		if(FAILED(m_DS->CreateSoundBuffer(&desc, &primaryBuffer, NULL)))
		{
			audErrorf("Couldn't get primary buffer");
		}

		DWORD speakerConfig = 0;
		m_DS->GetSpeakerConfig(&speakerConfig);

		/*GUID deviceGuid;
		GetDeviceID(&DSDEVID_DefaultPlayback, &deviceGuid);
		DirectSoundEnumerate(&DSEnumCallback, &deviceGuid);*/

		BYTE numChannels = ((BYTE)(speakerConfig));
		switch(numChannels)  
		{
		case DSSPEAKER_HEADPHONE:
		case DSSPEAKER_MONO:
		case DSSPEAKER_QUAD:
		case DSSPEAKER_STEREO:
			audDriver::SetNumOutputChannels(2);
			audDriver::SetHardwareOutputMode(AUD_OUTPUT_STEREO);
			break;
		case DSSPEAKER_SURROUND:
		case DSSPEAKER_5POINT1:
		case DSSPEAKER_7POINT1:
#ifdef DSSPEAKER_7POINT1_SURROUND
		case DSSPEAKER_7POINT1_SURROUND:
#endif
		case 9: //DSSPEAKER_5POINT1_SURROUND: //this is for directx 10 vista
			audDriver::SetNumOutputChannels(6);
			audDriver::SetHardwareOutputMode(AUD_OUTPUT_5_1);
			if(!reinit)
			{
				m_NumDSBuffers = g_DefaultDSBuffers;
				BANK_ONLY(g_RequestedNumDSBuffers = g_DefaultDSBuffers);
			}
			break;
		default:
			{
				audDriver::SetNumOutputChannels(2);
				audDriver::SetHardwareOutputMode(AUD_OUTPUT_STEREO);
			}
		}

		// allocate DS buffer to write to

		// 16 bit, multi-channel
		desc.dwBufferBytes = ((kMixBufNumSamples * 2) * audDriver::GetNumOutputChannels()) * m_NumDSBuffers * g_NumDSPasses;
#if __FINAL
		desc.dwFlags = DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_GETCURRENTPOSITION2; // | DSBCAPS_LOCHARDWARE;
#else
		desc.dwFlags = DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_GLOBALFOCUS | DSBCAPS_GETCURRENTPOSITION2; // | DSBCAPS_LOCHARDWARE;
#endif
	
		desc.guid3DAlgorithm = GUID_NULL;
		desc.lpwfxFormat = (WAVEFORMATEX*)&wfx;

		wfx.Format.cbSize = 22;
		wfx.Format.nChannels = (WORD)audDriver::GetNumOutputChannels();
		wfx.Format.nSamplesPerSec = kMixerNativeSampleRate;
		wfx.Format.wBitsPerSample = 16;
		wfx.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
		wfx.Format.nBlockAlign = (wfx.Format.nChannels * wfx.Format.wBitsPerSample) / 8;
		wfx.Format.nAvgBytesPerSec = wfx.Format.nSamplesPerSec * wfx.Format.nBlockAlign;

		switch(audDriver::GetNumOutputChannels())
		{
		
		case 2:
			// stereo - front left + front right
			wfx.dwChannelMask = (SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT);
			break;
		case 6:
			// 5.1
			wfx.dwChannelMask = (SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT | SPEAKER_FRONT_CENTER | SPEAKER_LOW_FREQUENCY | SPEAKER_BACK_LEFT | SPEAKER_BACK_RIGHT);
			break;
		case 8:
			// 7.1
			wfx.dwChannelMask = (SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT | SPEAKER_FRONT_CENTER | SPEAKER_LOW_FREQUENCY | SPEAKER_BACK_LEFT | SPEAKER_BACK_RIGHT | SPEAKER_FRONT_LEFT_OF_CENTER | SPEAKER_FRONT_RIGHT_OF_CENTER);
			break;
		default:
			wfx.dwChannelMask = (SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT);;
		}

		wfx.Samples.wValidBitsPerSample = 16;
		wfx.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;

		if(FAILED(primaryBuffer->SetFormat((WAVEFORMATEX*)&wfx)))
		{
			audErrorf("Couldn't setformat on primary buffer\n");
		}

		primaryBuffer->Release();

		HRESULT hr = m_DS->CreateSoundBuffer(&desc, (LPDIRECTSOUNDBUFFER*)&m_DSBuffer, NULL);
		if(SUCCEEDED(hr))
		{
			m_OutputNonInterleaved = rage_new s16[kMixBufNumSamples * audDriver::GetNumOutputChannels()];
			return true;
		}
		else
		{
			if(m_DS)
			{
				m_DS->Release();
				m_DS = NULL;
			}

			rage::diagErrorCodes::SetExtraReturnCodeNumber(hr);
			Quitf(ERR_AUD_HARDWARE_DS_BUFFER, "DirectSound CreateSoundBuffer Failure");
			return false;
		}
	}

	rage::diagErrorCodes::SetExtraReturnCodeNumber(hr);
	Quitf(ERR_AUD_HARDWARE_DS_INIT, "DirectSound Init Failure");
	return false;
}

void audMixerDevicePc::ShutdownHardware()
{
	m_ShouldMixThreadBeRunning = false;

	while(m_IsMixThreadRunning)
	{
		sysIpcSleep(100);
	}

	SAFE_RELEASE(m_DSBuffer);
	SAFE_RELEASE(m_DS);

	delete[] m_OutputNonInterleaved;
	m_OutputNonInterleaved = NULL;
}

void audMixerDevicePc::StartMixing()
{
	if( m_DS )
	{
		m_ShouldMixThreadBeRunning = true;	
		m_MixThread = sysIpcCreateThread(audMixerDevicePc::MixThreadEntryProc_DS, this, sysIpcMinThreadStackSize, audMixerThreadPriority, "[RAGE Audio] Mixer Thread", audMixerThreadCpu);
		audAssertf(m_MixThread != sysIpcThreadIdInvalid, "Couldn't create RAGE software mixer thread");
	}
}

// directsound WAVE_FORMAT_EX
DECLARE_THREAD_FUNC(audMixerDevicePc::MixThreadEntryProc_DS)
{
	
	audMixerDevicePc *device = (audMixerDevicePc*)ptr;

	device->m_IsMixThreadRunning = true;

	device->MixLoop_DS();

	device->m_IsMixThreadRunning = false;
}

void audMixerDevicePc::MixLoop_DS()
{
	void *ptr1, *ptr2;
	DWORD size1, size2, offset;
	IDirectSoundNotify *notify;

	const u32 maxDSBuffers = 32;

	HRESULT hr;

	sysPerformanceTimer tr("mix");

	BANK_ONLY(REINIT:)
		const u32 numOutputChannels = GetNumOutputChannels();

	const u32 numDSBuffers = m_NumDSBuffers;
	Assert(numDSBuffers <= maxDSBuffers);
	s32 lastHandle = 1;

	// setup notifications
	DSBPOSITIONNOTIFY pos[maxDSBuffers];
	HANDLE hEvents[maxDSBuffers];
	const s32 numPasses = g_NumDSPasses;
	const u32 bufSizeBytes = kMixBufNumSamples * numOutputChannels * 2 * numPasses; // * 2 since bytes rather than samples
	for(u32 i = 0; i < numDSBuffers; i++)
	{
		pos[i].dwOffset = i*bufSizeBytes;
		hEvents[i] = pos[i].hEventNotify = CreateEvent(NULL, FALSE, FALSE, NULL);
	}

	hr = m_DSBuffer->QueryInterface(IID_IDirectSoundNotify, (void**)&notify);
	if(FAILED(hr))
	{
		return;
	}

	hr = notify->SetNotificationPositions(numDSBuffers, (DSBPOSITIONNOTIFY*)&pos);
	if(FAILED(hr))
	{
		audErrorf("DirectSound: Failed to SetNotificationPositions: %08X", hr);
		return;
	}

	notify->Release();

	// silence the buffer
	hr = m_DSBuffer->Lock(0, 0, &ptr1, &size1, &ptr2, &size2, DSBLOCK_ENTIREBUFFER);
	if(FAILED(hr))
	{
		return;
	}
	sysMemSet(ptr1, 0, size1);
	hr = m_DSBuffer->Unlock(ptr1, size1, ptr2, size2);
	if(FAILED(hr))
	{
		return;
	}

	hr = m_DSBuffer->Play(0, 0, DSBPLAY_LOOPING);
	if(FAILED(hr))
	{
		return;
	}

	tr.Start();
	while(m_ShouldMixThreadBeRunning)
	{
		s32 handle = WaitForMultipleObjects(numDSBuffers, (HANDLE*)&hEvents, FALSE, INFINITE) - WAIT_OBJECT_0;

		if(handle != lastHandle)
		{
			lastHandle = handle;

			offset = pos[(handle+numDSBuffers-1)%numDSBuffers].dwOffset;

			m_DSBuffer->Lock(offset, bufSizeBytes, &ptr1, &size1, &ptr2, &size2, 0);

#if __BANK
			tr.Stop();
#endif

			s16 *writePtr = (s16*)ptr1;
			for(s32 pass = 0; pass < numPasses; pass++)
			{
				MixBuffers();

				// convert f32 buf to 16bit PCM

				// for now we need to support num output channels thats not divisible by eight
				if(((kMixBufNumSamples * m_NumOutputChannels) & 7)==0)
				{
					CopyMixerOutputToHardware_DS_SSE((s16*)writePtr);
				}
				else
				{
					CopyMixerOutputToHardware_DS_FPU((s16*)writePtr);
				}

				writePtr += (kMixBufNumSamples * m_NumOutputChannels);
			}


#if OUTPUT_MIXBUFFER_AUDIO
			{
				DWORD written;
				WriteFile(m_hMixerFile, ptr1, sizeof(s16) * kMixBufNumSamples * m_NumOutputChannels * numPasses, &written, NULL);
			}
#endif

			m_DSBuffer->Unlock(ptr1, size1, ptr2, size2);

			// check the play head to see if we mixed in time or if there was a glitch
			DWORD playCursor, writeCursor;
			m_DSBuffer->GetCurrentPosition(&playCursor, &writeCursor);

			//if(playCursor >= offset && playCursor < offset + bufSizeBytes)
			//			{
			//				audWarningf("mix glitch: offset: %u playCursor: %u", offset, playCursor);
			//			}

			{
				// need to ensure atomic updates of these two variables
				sysCriticalSection lock(m_TimerLock);
				m_LastGeneratedBufferIdx = ((u32)handle + m_NumDSBuffers - 1) % m_NumDSBuffers;
			}
#if __BANK

			g_MixTimeS = tr.GetTimeMS()/1000.f;
			const f32 mixBufTimeS = (kMixBufNumSamples/(f32)kMixerNativeSampleRate);
			g_MixRealtimeRatio = g_MixTimeS / mixBufTimeS;
			if(g_MixRealtimeRatio>1.f)
			{
				//audWarningf("Mix glitch");
			}			
			tr.Reset();
			tr.Start();
#endif

#if __BANK
			if(m_NumDSBuffers != g_RequestedNumDSBuffers)
			{
				sysCriticalSection lock(m_TimerLock);
				audDisplayf("Reinitialising mix buffers: %u buffers", g_RequestedNumDSBuffers);
				audDisplayf("Stopping DirectSound buffer");
				m_DSBuffer->Stop();
				SAFE_RELEASE(m_DSBuffer);
				//audDisplayf("Recreating mix buffers");
				//ReinitialiseMixBuffers(g_RequestedLatency, GetNumOutputChannels());

				//m_CurrentOutputBuf = m_OutputBuf1;
				//m_PreviousOutputBuf = m_OutputBuf2;

				//sysMemSet(m_CurrentOutputBuf, 0, sizeof(f32) * GetLatency() * GetNumOutputChannels());
				//sysMemSet(m_PreviousOutputBuf, 0, sizeof(f32) * GetLatency() * GetNumOutputChannels());

				m_NumDSBuffers = g_RequestedNumDSBuffers;
				audDisplayf("Reinitialising hardware");
				InitOutputHardwareDS(true);

				tr.Stop();

				goto REINIT;
			}
#endif

		}

	}

	m_DSBuffer->Stop();
}

void audMixerDevicePc::CopyMixerOutputToHardware_DS_FPU(s16 *dest)
{	
	const f32 globalScalar = g_OutputGain;
	f32 *buf = GetOutputBuffer();
	if (buf)
	{
		for (u32 i = 0, k = 0; i < kMixBufNumSamples; i++)
		{
			// interleave buffer
			for (u32 j = 0; j < m_NumOutputChannels; j++)
			{
				// 16 bit
				const f32 scaled_value = buf[i + (kMixBufNumSamples * j)] * 32767.f * globalScalar;
				dest[k] = (s16)(scaled_value);
				k++;
			}
		}
	}
	else
	{
		sysMemSet(dest, 0, kMixBufNumSamples * m_NumOutputChannels * sizeof(s16));
	}
} 

void audMixerDevicePc::CopyMixerOutputToHardware_DS_SSE(s16 *dest)
{	
	// convert to s16 and saturate
	f32 *buf = GetOutputBuffer();
	if (buf)
	{
		Assert(((kMixBufNumSamples * m_NumOutputChannels) & 7) == 0);
		const __m128 multiplier = _mm_set_ps1(32767.f * g_OutputGain);
		for (unsigned int i = 0; i < kMixBufNumSamples * m_NumOutputChannels; i += 8)
		{
			__m128 scaledSamples0 = _mm_mul_ps(multiplier, _mm_load_ps(&buf[i]));
			__m128 scaledSamples1 = _mm_mul_ps(multiplier, _mm_load_ps(&buf[i + 4]));
			__m128i outputSamples = _mm_packs_epi32(_mm_cvtps_epi32(scaledSamples0), _mm_cvtps_epi32(scaledSamples1));
			_mm_store_si128((__m128i*) & m_OutputNonInterleaved[i], outputSamples);
		}
		const u32 channelMap[] =
		{
			RAGE_SPEAKER_ID_FRONT_LEFT,
			RAGE_SPEAKER_ID_FRONT_RIGHT,
			RAGE_SPEAKER_ID_FRONT_CENTER,
			RAGE_SPEAKER_ID_LOW_FREQUENCY,
			RAGE_SPEAKER_ID_BACK_LEFT,
			RAGE_SPEAKER_ID_BACK_RIGHT
		};
		for (u32 i = 0, k = 0; i < kMixBufNumSamples; i++)
		{
			// interleave buffer
			for (u32 j = 0; j < m_NumOutputChannels; j++)
			{
				dest[k++] = m_OutputNonInterleaved[i + (kMixBufNumSamples * channelMap[j])];
			}
		}
	}
	else
	{
		sysMemSet(dest, 0, kMixBufNumSamples * m_NumOutputChannels * sizeof(s16));
	}
} 

f32 audMixerDevicePc::BenchmarkSystem()
{

#if __SYNTH_EDITOR
	// stop the mixer thread 
	m_ShouldMixThreadBeRunning = false;
	while(m_IsMixThreadRunning)
	{
		sysIpcSleep(10);
	}

	m_MixThread = sysIpcThreadIdInvalid;


	const s32 synthId = synthSynthesizer::Create("SYNTH_BENCHMARK");

	for(u32 i = 0 ; i < m_NumVoices; i++)
	{
		audMixerVoice *voice = InitVoice(i);
		voice->SetPcmSource(synthId,0);
		voice->SetFilters(100,15000);
		voice->SetOutput(0, GetSubmixIndex(GetMasterSubmix()), false);
		voice->SetOutput(1, GetSubmixIndex(GetMasterSubmix()), false);
		voice->Start();
	}

	for(u32 i = 0 ; i < 5; i++)
	{
		MixBuffers();
	}

	sysPerformanceTimer timer("MixerBenchmark");
	timer.Start();
	for(u32 i = 0 ; i < 20; i++)
	{
		MixBuffers();
	}
	timer.Stop();

	for(u32 i = 0 ; i < m_NumVoices; i++)
	{
		m_Voices[i].Stop();
	}

	for(u32 i = 0 ; i < 5; i++)
	{
		MixBuffers();
	}

	for(u32 i = 0 ; i < m_NumVoices; i++)
	{
		m_Voices[i].DisconnectAllOutputs();
	}

	// restart mix thread
	m_ShouldMixThreadBeRunning = true;	
	m_MixThread = sysIpcCreateThread(audMixerDevicePc::MixThreadEntryProc_DS, this, 8*1024, audMixerThreadPriority, "[RAGE Audio] Mixer Thread", audMixerThreadCpu);
	Assert(m_MixThread != sysIpcThreadIdInvalid && "Couldn't create mixer thread!");

	return timer.GetTimeMS()/20.f;
#else
	return 0.f;
#endif

}
/*
u32 audMixerDevicePc::GetMixerTimeMs()
{
	sysCriticalSection lock(m_TimerLock);

	Assert(m_DSBuffer);
	DWORD play,write;
	m_DSBuffer->GetCurrentPosition(&play,&write);

	// 2 bytes per sample
	const u32 samplesThroughDSBuffer = play/(2*m_NumOutputChannels);

	// what mix buffer are we currently playing through
	const u32 playBufferIdx = samplesThroughDSBuffer/m_NumMixBufSamples;
	const u32 subBufferPosSamples = (samplesThroughDSBuffer - (playBufferIdx*m_NumMixBufSamples));
	const u32 subBufferPosMs = (u32)(subBufferPosSamples / (m_NativeSampleRate/1000.f));

	u32 numBuffersBehind;
	if(playBufferIdx <= m_LastGeneratedBufferIdx)
	{
		numBuffersBehind = m_LastGeneratedBufferIdx - playBufferIdx;
	}
	else
	{
		numBuffersBehind = m_NumDSBuffers - playBufferIdx + m_LastGeneratedBufferIdx;

	}
	const u32 millisecondsBehind = numBuffersBehind * m_MsPerMixBuffer;
	//Assert(m_MixTimerMs + subBufferPosMs >= millisecondsBehind);

	const u32 ret = m_MixTimerMs - millisecondsBehind + subBufferPosMs;

	return ret;	
}

u32 audMixerDevicePc::GetDeviceLatency() const
{
	return (m_NumDSBuffers+1) * GetLatency();
}

u32 audMixerDevicePc::GetMixerTimeSamples()
{
	sysCriticalSection lock(m_TimerLock);

	Assert(m_DSBuffer);
	DWORD play,write;
	m_DSBuffer->GetCurrentPosition(&play,&write);

	// 2 bytes per sample
	const u32 samplesThroughDSBuffer = play/(2*m_NumOutputChannels);

	// what mix buffer are we currently playing through
	const u32 playBufferIdx = samplesThroughDSBuffer/m_NumMixBufSamples;
	const u32 subBufferPosSamples = (samplesThroughDSBuffer - (playBufferIdx*m_NumMixBufSamples));

	u32 numBuffersBehind;
	if(playBufferIdx <= m_LastGeneratedBufferIdx)
	{
		numBuffersBehind = m_LastGeneratedBufferIdx - playBufferIdx;
	}
	else
	{
		numBuffersBehind = m_NumDSBuffers - playBufferIdx + m_LastGeneratedBufferIdx;

	}

	const u32 samplesBehind = numBuffersBehind * m_NumMixBufSamples;
	const u32 ret = m_MixTimerSamples - samplesBehind + subBufferPosSamples;

	return ret;	
}

*/
} // namespace rage

#endif // __WIN32PC

