// 
// audiohardware/VirtualisationJob.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "VirtualisationJob.h"

#if __SPU

#define AUD_VOICE_SPU

#include "voicesettings.h"
#include "virtualisation.cpp"
#include "pcmsource_interface.cpp"
#include "audioengine/soundpool.h"
#include "audioengine/spuutil.h"

#include "system/memory.h"
#include "virtualisation.h"

namespace rage
{
const PcmSourceState *g_PcmSourceState = NULL;
u32 g_NumBuckets;
u32 g_BucketSize;
void *g_FirstBucketEA;
audSoundPoolBucket *g_Bucket;

#if AUD_SOUNDPOOL_LOCK_ON_SPU
void *g_BucketLocks;
#endif

}


using namespace rage;

void VirtualisationJob(sysTaskParameters &p)
{
	Assert(p.UserDataCount == 4);

	// then number of virtual (LSW) and physical (MSW) voices
	u32 temp = p.UserData[1].asUInt;
	u32 numVirtualVoices = temp & 0xffff;
	//u32 numPhysicalVoices = (temp >> 16) & 0xffff;
	// then number of actual phys voices free (LSW) and physical voices this frame (MSW)
	temp = p.UserData[2].asUInt;
	u32 actualPhysicalVoicesFree = temp & 0xffff;
	u32 physicalVoicesThisFrame = (temp >> 16) & 0xffff;

	const u32 voiceBudget = p.UserData[3].asUInt;
	audVirtualisationJobInput *inputHeader = (audVirtualisationJobInput*)p.Input.Data;

	Assert(p.Input.Size == sizeof(audVirtualisationJobInput));
	Assert(p.Output.Size == sizeof(audVirtualisationJobOutput));
	
	g_NumBuckets = inputHeader->numBuckets;
	g_BucketSize = inputHeader->bucketSize;
	g_FirstBucketEA = inputHeader->eaFirstBucket;

	Assert(p.Scratch.Size == ((sizeof(PrioritisedVoice) * numVirtualVoices) + g_BucketSize + sizeof(PrioritisedGroupList) + sizeof(VoiceBinsList)));
	
	Assert(p.Scratch.Data);

	sysMemSet(p.Output.Data, 0, p.Output.Size);
		
	// output data is header followed by start/stop lists
	audVirtualisationJobOutput *header = (audVirtualisationJobOutput*)p.Output.Data;

	InitScratchBuffer( p.Scratch.Data, p.Scratch.Size );

	// scratch space is prioritised voice list
	PrioritisedVoice *prioVoiceList = AllocateFromScratch<PrioritisedVoice>( numVirtualVoices, 16, "PrioritisedVoice" );
	// g_Bucket is after that
	g_Bucket = AllocateFromScratch<audSoundPoolBucket>( 1, 16, "audSoundPoolBucket" );
	// PrioritisedGroupList and VoiceBinsList follow
	PrioritisedGroupList& prioritisedGroupList = *AllocateFromScratch<PrioritisedGroupList>( 1, 16, "PrioritisedGroupList" );
	VoiceBinsList& voiceBinList = *AllocateFromScratch<VoiceBinsList>( 1, 16, "VoiceBinsList" );

#if AUD_SOUNDPOOL_LOCK_ON_SPU
	g_BucketLocks = inputHeader->eaBucketLocks;
#endif
	// PCM Generator State is Read Only
	Assert(p.ReadOnly[0].Size == sizeof(atRangeArray<PcmSourceState, kMaxPcmSources>));
	g_PcmSourceState = (const PcmSourceState*)p.ReadOnly[0].Data;

	// sort the voice list by priority
	PrioritisedVoice *voiceList = prioVoiceList;
	VoiceGroup *groupList = &prioritisedGroupList[0];
	BuildPrioritisedVoiceList(voiceList, groupList, header, prioritisedGroupList, voiceBinList);
	// walk the sorted list handing out physical voices
	AllocatePhysicalVoices(voiceBudget, groupList, voiceList, actualPhysicalVoicesFree, physicalVoicesThisFrame, header);
}

#else // __SPU
void VirtualisationJob(rage::sysTaskParameters &){}
#endif


