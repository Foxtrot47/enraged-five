//
// audiohardware/decoder_ps4mp3.cpp
//
// Copyright (C) 2014 Rockstar Games.  All Rights Reserved.
//

#if RSG_ORBIS

#include "channel.h"
#include "decoder_PS4mp3.h"
#include "mp3util.h"
#include "file/asset.h"
#include "file/stream.h"

#include "audiohardware/driver.h"
#include "audioengine/spuutil.h"
#include "math/amath.h"
#include "grcore/wrapper_gcm.h"
#include "system/memops.h"

#include <libsysmodule.h>

namespace rage
{
	SceAjmContextId audDecoderPS4Mp3::sm_AjmContext;

#if DUMP_RAW_MP3_STREAMS || ENABLE_MP3_DEBUG
	s32 audDecoderPS4Mp3::sm_FilenameCount = 0;
#endif
	bool audDecoderPS4Mp3::InitClass()
	{
		s32 rv;

		rv = sceAjmInitialize(0, &sm_AjmContext);
		if (rv) {
			Errorf("Failed initializing AJM: %s\n", sceAjmStrError(rv));
			return false;
		}

		rv = sceAjmModuleRegister(sm_AjmContext, SCE_AJM_CODEC_MP3_DEC, 0);
		if (rv) {
			Errorf("Failed to register codec: %s\n", sceAjmStrError(rv));
			return false;
		}

		return true;
	}

	void audDecoderPS4Mp3::ShutdownClass()
	{
		s32 rv;

		rv = sceAjmModuleUnregister(sm_AjmContext, SCE_AJM_CODEC_MP3_DEC);
		if (rv) {
			Errorf("Failed to register codec: %s\n", sceAjmStrError(rv));
		}

		rv = sceAjmFinalize(sm_AjmContext);
		if (rv) {
			Errorf("Failed to register codec: %s\n", sceAjmStrError(rv));
		}
	}

	audDecoderPS4Mp3::audDecoderPS4Mp3() :
		  m_SubmitPacketIndex(0)
		, m_DecodePacketIndex(0)
		, m_NumChannels(0)
		, m_State(audDecoder::IDLE)
#if __BANK
		, m_TotalSamples(0)
#endif
	{

	}

	void audDecoderPS4Mp3::ClearBuffer()
	{
		
		m_SubmitPacketIndex = 0;
		m_DecodePacketIndex = 0;

		sysMemZeroBytes<sizeof(m_Packets)>(&m_Packets);

		m_State = audDecoder::IDLE;
	}

	bool audDecoderPS4Mp3::Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate))
	{
		Assert(format == audWaveFormat::kMP3);

		m_SubmitPacketIndex = 0;
		m_DecodePacketIndex = 0;
		m_DecodedLastFrame = false;

		sysMemZeroBytes<sizeof(m_Packets)>(&m_Packets);

		s32 iRet = sceAjmInstanceCreate(sm_AjmContext, SCE_AJM_CODEC_MP3_DEC, SCE_AJM_INSTANCE_FLAG_MAX_CHANNEL(1), &m_AjmInstance);
		if (iRet) 
		{
			Errorf("Failed to create AJM instance: %s\n", sceAjmStrError(iRet));
			return false;
		}

		m_Packets[0].BatchId = ~0U;
		m_Packets[1].BatchId = ~0U;

		OpenStreamDebugFile();

		m_NumChannels = numChannels;
		m_NumQueuedPackets = 0;
		m_ReadyForMoreData = true;

		m_State = audDecoder::IDLE;

		return (format == audWaveFormat::kMP3);
	}

	bool audDecoderPS4Mp3::IsSafeToDelete()
	{
		for (int i = 0; i < 2; i++)
		{
			if (m_Packets[i].BatchId != ~0U)
			{
				SceAjmBatchError batchError;
				s32 iRet = sceAjmBatchWait(sm_AjmContext, m_Packets[i].BatchId, 0, &batchError);
				if (iRet)
				{
					return false;
				}

				m_Packets[i].BatchId = ~0U;
			}
		}

		return true;
	}

	void audDecoderPS4Mp3::Shutdown()
	{
		for(int i=0; i<2; i++)
		{
			if(m_Packets[i].BatchId != ~0U)
			{
				SceAjmBatchError batchError;
				s32 iRet = sceAjmBatchWait(sm_AjmContext, m_Packets[i].BatchId, SCE_AJM_WAIT_INFINITE, &batchError);
				if (iRet) 
				{
					Warningf("Failed to complete batch: %s\n", sceAjmStrError(iRet));
					BANK_ONLY(sceAjmBatchErrorDump(&m_Packets[i].BatchInfo, &batchError));
				}
			}
		}

		s32 iRet = sceAjmInstanceDestroy(sm_AjmContext, m_AjmInstance);
		if (iRet) 
		{
			Warningf("Failed to destroy AJM instance: %s\n", sceAjmStrError(iRet));
		}

		CloseStreamDebugFile();
		//CloseBlockDebugFile(true);

	}

	void audDecoderPS4Mp3::SubmitPacket(audPacket &packet)
	{
		Assert(packet.inputData);
		Assert(packet.inputBytes);
		Assert(m_ReadyForMoreData);

		sysMemZeroBytes<sizeof(audDecoderMp3Packet)>(&m_Packets[m_SubmitPacketIndex]);

		m_Packets[m_SubmitPacketIndex].InputBufferSize = packet.inputBytes;
		m_Packets[m_SubmitPacketIndex].InputData = (u8*)packet.inputData;
		m_Packets[m_SubmitPacketIndex].InputBufferBytesRead = 0;
		m_Packets[m_SubmitPacketIndex].IsFinalBlock = packet.IsFinalBlock;
		m_Packets[m_SubmitPacketIndex].SkipFirstFrame = packet.SkipFirstFrame;
		m_Packets[m_SubmitPacketIndex].PlayBegin = packet.PlayBegin;
		m_Packets[m_SubmitPacketIndex].PlayEnd = packet.PlayEnd;
		m_Packets[m_SubmitPacketIndex].LoopBeginSamples = packet.LoopBegin;

		if(packet.LoopBegin != ~0U)
		{
			if(packet.SeekTable)
			{
				audMp3Util::ComputeFrameOffsetFromSamples(packet.SeekTable, 
					packet.SeekTableSize,
					packet.LoopBegin,
					m_Packets[m_SubmitPacketIndex].LoopStartOffsetBytes,
					m_Packets[m_SubmitPacketIndex].LoopBegin);
			}
			else
			{
				// turn samples into bytes
				const u32 loopBeginWholeFrames = packet.LoopBegin / audMp3Util::kFrameSizeSamples;
				u32 byteOffset = 
					loopBeginWholeFrames ?
					audMp3Util::ComputeByteOffsetForFrame(m_Packets[m_SubmitPacketIndex].InputData, 
					m_Packets[m_SubmitPacketIndex].InputBufferSize, audMp3Util::kFrameSizeSamples, loopBeginWholeFrames)
					: 0;
				if(byteOffset == ~0U)
				{
					byteOffset = 0;
					Errorf("Bad ComputeByteOffsetForFrame(%d) in wave %u", __LINE__, m_WaveIdentifier);
					Errorf("bufferSize %d, loopBeginWholeFrames %d", m_Packets[m_SubmitPacketIndex].InputBufferSize, loopBeginWholeFrames);
				}

				// Calculate subframe sample offset (not currently used; loops should be aligned to
				// whole frames).
				m_Packets[m_SubmitPacketIndex].LoopBegin = packet.LoopBegin - (loopBeginWholeFrames*audMp3Util::kFrameSizeSamples);
				m_Packets[m_SubmitPacketIndex].LoopStartOffsetBytes = byteOffset;
			}
		}
		else
		{
			m_Packets[m_SubmitPacketIndex].LoopStartOffsetBytes = ~0U;
		}
		
#if __BANK
		m_Packets[m_SubmitPacketIndex].PacketNumber = 0;
#endif

		u32 bytesToSkip = 0;
		const u32 wholeFramesToSkip = packet.PlayBegin / audMp3Util::kFrameSizeSamples;
		if(packet.PlayBegin != 0)
		{

			bytesToSkip = audMp3Util::ComputeByteOffsetForFrame((u8*)packet.inputData, 
																	packet.inputBytes, 
																	audMp3Util::kFrameSizeSamples, 
																	wholeFramesToSkip);
			if(bytesToSkip == ~0U)
			{
				bytesToSkip = 0;
				Errorf("Bad ComputeByteOffsetForFrame(%d) in wave %u", __LINE__, m_WaveIdentifier);
				Errorf("a.inputBytes %d, wholeFramesToSkip %d", packet.inputBytes, wholeFramesToSkip);
			}
		}
		
		if(packet.SkipFirstFrame)
		{
			m_Packets[m_SubmitPacketIndex].PlayBegin += audMp3Util::kFrameSizeSamples + 16;
			if (m_Packets[m_SubmitPacketIndex].PlayEnd != 0)
			{
				// Ensure the Play region length is preserved
				m_Packets[m_SubmitPacketIndex].PlayEnd += audMp3Util::kFrameSizeSamples + 16;
			}
		}

#if ENABLE_MP3_DEBUG
		Displayf("%d : submit : bytes(%d), PB(%d)(%d), BSS(%d), PE(%d), SFF(%d)", 
			m_SubmitPacketIndex, 
			packet.inputBytes, 
			packet.PlayBegin, bytesToSkip,
			packet.BlockStartSample,
			packet.PlayEnd,
			packet.SkipFirstFrame);
#endif

		m_Packets[m_SubmitPacketIndex].InputBufferBytesRead += bytesToSkip;
		m_Packets[m_SubmitPacketIndex].SamplesConsumed = wholeFramesToSkip*audMp3Util::kFrameSizeSamples;

		if(packet.PlayEnd != 0)
		{
			u32 wholeFramesToSkipToEnd = 0;
			
			if (m_Packets[m_SubmitPacketIndex].PlayEnd % audMp3Util::kFrameSizeSamples == 0)
			{
				// Play end is aligned to an exact frame boundary - no need to add an additional frame to get to the end
				wholeFramesToSkipToEnd = (m_Packets[m_SubmitPacketIndex].PlayEnd / audMp3Util::kFrameSizeSamples);
			}
			else
			{
				wholeFramesToSkipToEnd = (m_Packets[m_SubmitPacketIndex].PlayEnd / audMp3Util::kFrameSizeSamples) + 1;
			}			

			u32 bytesToSkipForEnd = audMp3Util::ComputeByteOffsetForFrame(
				(u8*)packet.inputData, 
				packet.inputBytes, 
				audMp3Util::kFrameSizeSamples, 
				wholeFramesToSkipToEnd);
			if(bytesToSkipForEnd == ~0U)
			{
				bytesToSkipForEnd = 0;
				Errorf("Bad ComputeByteOffsetForFrame(%d) in wave %u", __LINE__, m_WaveIdentifier);
				Errorf("b.inputBytes %d, wholeFramesToSkip %d", packet.inputBytes, wholeFramesToSkip);
			}

			m_Packets[m_SubmitPacketIndex].InputBufferSize = bytesToSkipForEnd;
			//if(packet.IsFinalBlock && bytesToSkipForEnd < packet.inputBytes)
			//{
			//	Displayf("Trimmed final block");
			//}
		}

		m_Packets[m_SubmitPacketIndex].BatchId = ~0U;
		
		m_NumQueuedPackets++;

		Assert(m_Packets.size() == 2);
		m_SubmitPacketIndex = (m_SubmitPacketIndex + 1) & 1;

		if(m_SubmitPacketIndex == m_DecodePacketIndex)
		{
			m_ReadyForMoreData = false;
		}

		m_State = audDecoder::DECODING;
	}

	void audDecoderPS4Mp3::Update()
	{
		audDecoderMp3Packet &packet = m_Packets[m_DecodePacketIndex];

		if(m_State == audDecoderState::FINISHED)
			return;

		if(packet.LoopStartOffsetBytes == ~0U && packet.SamplesConsumed >= packet.PlayEnd)
		{
			if(m_NumQueuedPackets > 0)
				m_NumQueuedPackets--;

			if(m_NumQueuedPackets > 0 && m_State != audDecoderState::FINISHED)
			{
				//CloseBlockDebugFile();
				m_DecodePacketIndex = (m_DecodePacketIndex + 1) % m_Packets.size();
				m_ReadyForMoreData = ((m_DecodePacketIndex!=m_SubmitPacketIndex) || m_State != audDecoderState::DECODING);	
			}
			else if(packet.IsFinalBlock)
			{
				m_State = audDecoderState::FINISHED;
				m_DecodePacketIndex = m_SubmitPacketIndex = 0;
				m_ReadyForMoreData = true;
			}
			else
			{
				// If we have no queued packets but we've not had the final block then we're ready for more data.
				m_ReadyForMoreData = true;
			}
			
			return;
		}

		m_ReadyForMoreData = ((m_DecodePacketIndex!=m_SubmitPacketIndex) || m_State != audDecoderState::DECODING);	

		//OpenBlockDebugFile();

		// This could happen if we attempt to start a loop with an initial playback position at (or past) the end
		// of the asset. In this situation just wrap back around to the loop point and start playback from there
		if (packet.LoopStartOffsetBytes != ~0U && packet.State == MP3_DECODE_STATE_SUBMIT && packet.InputBufferBytesRead >= packet.InputBufferSize)
		{
			packet.InputBufferBytesRead = packet.LoopStartOffsetBytes;
			packet.PlayBegin = 0;
			packet.SamplesConsumed = packet.LoopBeginSamples;
		}

		// see if we need to submit packets to the ACP 
		if(packet.State == MP3_DECODE_STATE_SUBMIT && packet.InputBufferBytesRead < packet.InputBufferSize)
		{
			// see how many frames we can submit to ACP decoder
			s32 frames = 0;
			u32 inputBufferBytesRead = packet.InputBufferBytesRead;
			s32 bytesToSubmit = 0;
			while(frames < kPS4NumFramesInOutputBuffer && inputBufferBytesRead < packet.InputBufferSize)
			{
				u8* frameHeader = ((u8*)packet.InputData+inputBufferBytesRead);
				const u32 frameSize = audMp3Util::ComputeSizeOfFrame(frameHeader);

				if(frameSize == 0)
				{
					Warningf("Failed to compute valid frame size, aborting playback!");
					m_State = audDecoder::DECODER_ERROR;
					return;
				}

				bytesToSubmit += frameSize;
				inputBufferBytesRead += frameSize;
				frames++;
			}

			// submit data for dedcode
			sceAjmBatchInitialize(packet.BatchBuffer, sizeof (packet.BatchBuffer), &packet.BatchInfo);
			size_t szFrame = bytesToSubmit;
			size_t szOutput = SCE_AJM_DEC_MP3_MAX_FRAME_SAMPLES*kPS4NumFramesInOutputBuffer*2; // *2 for bytes

			u8* inputBuffer = const_cast<uint8_t *>(((u8*)packet.InputData+packet.InputBufferBytesRead));
			s32 iRet = sceAjmBatchJobDecode(&packet.BatchInfo, m_AjmInstance, inputBuffer, szFrame, packet.OutputBuffer, szOutput, &packet.DecodeResult);
			if (iRet) 
			{
				Warningf("Failed to add batch decode job: %s\n", sceAjmStrError(iRet));
				m_State = audDecoder::DECODER_ERROR;
				return;
			}
			iRet = sceAjmBatchStart(sm_AjmContext, &packet.BatchInfo, SCE_AJM_PRIORITY_GAME_DEFAULT, NULL, &packet.BatchId);
			if (iRet) 
			{
				Warningf("Failed to start MP3 decode batch: %s\n", sceAjmStrError(iRet));
				m_State = audDecoder::DECODER_ERROR;
				return;
			}
			packet.State = MP3_DECODE_STATE_CONSUME;
			return;
		}
		else if(packet.State == MP3_DECODE_STATE_CONSUME)
		{
			// see if there is enough free space in the ring buffer to decode the next block
			s32 spaceAvailableInRingBuffer = m_RingBuffer.GetBufferSize() - m_RingBuffer.GetBytesAvailableToRead();
			if(spaceAvailableInRingBuffer > (SCE_AJM_DEC_MP3_MAX_FRAME_SAMPLES * kPS4NumFramesInOutputBuffer * 2) && m_State != audDecoder::FINISHED)
			{
				// poll to see if decode is ready
				SceAjmBatchError batchError;
				s32 iRet = sceAjmBatchWait(sm_AjmContext, packet.BatchId, 0, &batchError);
				if (iRet) 
				{
					if(iRet == SCE_AJM_ERROR_IN_PROGRESS)
					{
						return;
					}
					Warningf("Failed to complete batch: %s\n", sceAjmStrError(iRet));
					BANK_ONLY(sceAjmBatchErrorDump(&packet.BatchInfo, &batchError));
					m_State = audDecoder::DECODER_ERROR;
					return;
				}

				if (packet.DecodeResult.sResult.iResult < 0 || (packet.DecodeResult.sResult.iResult & SCE_AJM_RESULT_INVALID_DATA)) 
				{
					Warningf("Failed to decode: code 0x%x\n", packet.DecodeResult.sResult.iResult);
					m_State = audDecoder::DECODER_ERROR;
					return;
				}
				packet.BatchId = ~0U;
				packet.State = MP3_DECODE_STATE_SUBMIT;

				//Displayf("decode: %u = %u\n", packet.DecodeResult.sStream.iSizeConsumed, packet.DecodeResult.sStream.iSizeProduced);
				u32 packetSizeBytes = packet.DecodeResult.sStream.iSizeProduced;
				packet.InputBufferBytesRead += packet.DecodeResult.sStream.iSizeConsumed;				

				s32 writeOffset = 0;
				s32 writePacketSize = packetSizeBytes;
				if(packet.SamplesConsumed < packet.PlayBegin) // see if we need to skip some samples
				{
					//consume bytes until we get to the correct start play point
					s32 bytesInBuffer = packetSizeBytes;
					s32 bytesToConsume = (packet.PlayBegin - packet.SamplesConsumed) * 2;
					if(bytesToConsume)
					{
						if(bytesToConsume > bytesInBuffer)
						{
							// consume all the bytes
							Assert(bytesInBuffer/2 == packet.DecodeResult.sStream.uiTotalDecodedSamples);
							packet.SamplesConsumed += packet.DecodeResult.sStream.uiTotalDecodedSamples;
#if __BANK
							packet.PacketNumber++;
#endif
							return;
						}
						else
						{
							bytesInBuffer -= bytesToConsume;
							writeOffset = bytesToConsume;	// skip PlayBegin samples
							writePacketSize = bytesInBuffer;
							packet.SamplesConsumed += (bytesToConsume/2);
						}
					}
				}

				bool hasFinished = false;

				//check if we have to skip some of the first playable packet
				if(packet.LoopStartOffsetBytes == ~0U && packet.IsFinalBlock && packet.PlayEnd > 0) // see if we will go beyond our PlayEnd samples and trim the last packet
				{
					// Was this the last packet? See if we need to trim it.
					if(packet.SamplesConsumed + (writePacketSize/2) >= packet.PlayEnd) // || packet.InputBufferBytesRead >= packet.InputBufferSize)
					{
						// Calulate trim amount
						s32 lastPacketSizeBytes = (packet.PlayEnd - packet.SamplesConsumed) * 2;
						if(lastPacketSizeBytes > 0)
						{
							writePacketSize = lastPacketSizeBytes;
						}
						else
						{
							writePacketSize = 0;	
						}

						hasFinished = true;						

						//CloseBlockDebugFile();
					}
				}

				if(writePacketSize > spaceAvailableInRingBuffer)
				{
					Warningf("Had to trim writePacketSize(%d) so we didn't overflow ring buffer(%d)", writePacketSize, spaceAvailableInRingBuffer);
					m_RingBuffer.WriteData(&packet.OutputBuffer[writeOffset], spaceAvailableInRingBuffer);
				}
				else
				{
					m_RingBuffer.WriteData(&packet.OutputBuffer[writeOffset/2], writePacketSize);
				}

				// Loop logic - return to loop point if we reach the end, ready for the next submit				
				if (hasFinished && packet.LoopStartOffsetBytes != ~0U)
				{
					packet.InputBufferBytesRead = packet.LoopStartOffsetBytes;
					packet.PlayBegin = 0;
					packet.SamplesConsumed = packet.LoopBeginSamples;
				}
				else
				{
					packet.SamplesConsumed += ((writePacketSize/2)/m_NumChannels);
				}

#if __BANK
				m_TotalSamples += writePacketSize;
				packet.PacketNumber++;
#if DUMP_RAW_MP3_STREAMS
				if(m_fp)
				{
					s16* sample = (s16*)&packet.OutputBuffer[writeOffset/2];
					for(u32 i=0; i<writePacketSize/2; i++)
					{
						m_fp->WriteShort(sample,1);
						sample++;
					}
				}
//#if DUMP_RAW_MP3_BLOCKS
//				if(m_blockfp[m_DecodePacketIndex])
//				{
//					s16* sample = (s16*)&packet.OutputBuffer[writeOffset];
//					for(u32 i=0; i<writePacketSize/2; i++)
//					{
//						m_blockfp[m_DecodePacketIndex]->WriteShort(sample,1);
//						sample++;
//					}
//				}
//#endif
#endif
#endif
			}
		}
		else
		{
			packet.SamplesConsumed = packet.PlayEnd;
		}
	}


	u32 audDecoderPS4Mp3::QueryNumAvailableSamples() const
	{
		return (m_RingBuffer.GetBytesAvailableToRead()>>1);
	}

	bool audDecoderPS4Mp3::QueryReadyForMoreData() const
	{
		return m_ReadyForMoreData;
	}

	u32 audDecoderPS4Mp3::ReadData(s16 *buf, const u32 numSamples) const
	{
		return m_RingBuffer.PeakData(buf, numSamples<<1)>>1;
	}

	void audDecoderPS4Mp3::AdvanceReadPtr(const u32 numSamples)
	{
		m_RingBuffer.AdvanceReadPtr(numSamples<<1);
	}


	// debug functions
	void audDecoderPS4Mp3::OpenStreamDebugFile()
	{
#if ENABLE_MP3_DEBUG || DUMP_RAW_MP3_STREAMS
		m_FileNumber = sm_FilenameCount;
#endif
#if DUMP_RAW_MP3_STREAMS
		char filename[256];
		formatf(filename, "X:\\mp3_test%d", sm_FilenameCount);
		m_fp = ASSET.Create(filename, "raw");
#if DUMP_RAW_MP3_BLOCKS
		m_blockfp[0] = NULL;
		m_blockfp[1] = NULL;
		m_BlockNumber = 0;
#endif
#endif
#if ENABLE_MP3_DEBUG || DUMP_RAW_MP3_STREAMS
		sm_FilenameCount++;
#endif

	}
	
	void audDecoderPS4Mp3::CloseStreamDebugFile()
	{
#if DUMP_RAW_MP3_STREAMS
		if(m_fp)
			m_fp->Close();
#endif
	}

	void audDecoderPS4Mp3::OpenBlockDebugFile()
	{
#if DUMP_RAW_MP3_STREAMS && DUMP_RAW_MP3_BLOCKS
		if(m_blockfp[m_DecodePacketIndex] == NULL)
		{
			char filename[256];
			formatf(filename, "X:\\mp3_test%d_block%d", m_FileNumber, m_BlockNumber);
			m_blockfp[m_DecodePacketIndex] = ASSET.Create(filename, "raw");
			m_BlockNumber++;
		}
#endif
	}

	void audDecoderPS4Mp3::CloseBlockDebugFile(bool closeAll)
	{
#if DUMP_RAW_MP3_STREAMS && DUMP_RAW_MP3_BLOCKS
		if(closeAll)
		{
			if(m_blockfp[0])
			{
				m_blockfp[0]->Close();
				m_blockfp[0] = NULL;
			}
			if(m_blockfp[1])
			{
				m_blockfp[1]->Close();
				m_blockfp[1] = NULL;
			}
		}
		else
		{
			if(m_blockfp[m_DecodePacketIndex])
			{
				m_blockfp[m_DecodePacketIndex]->Close();
				m_blockfp[m_DecodePacketIndex] = NULL;
			}
		}
#else
		(void)closeAll;
#endif
	}



} // namespace rage

#endif // __RSG_ORBIS
