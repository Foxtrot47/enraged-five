//
// audiohardware/decoder_xma.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#if __XENON

#include "debug.h"
#include "decoder_xma.h"
#include "decodemgr.h"
#include "driver.h"
#include "profile/profiler.h"

#include <xma2defs.h>

EXT_PF_GROUP(MixerDevice);

#define AUD_PROFILE_XMA_DECODER 0
#if AUD_PROFILE_XMA_DECODER
PF_TIMER(DecoderXmaUpdate,MixerDevice);
PF_TIMER(DecoderXmaUpdate_Wait,MixerDevice);
#define XMA_PF_FUNC(x) PF_FUNC(x)
#else
#define XMA_PF_FUNC(x)
#endif

namespace rage
{
	bool audDecoderXma::Init(const audWaveFormat::audStreamFormat ASSERT_ONLY(format), const u32 numChannels, const u32 sampleRate)
	{
		Assert(format == audWaveFormat::kXMA2);
		Assert(numChannels == 1 || numChannels == 2);
		m_SamplesDecoded = 0;
		m_BytesSubmitted = 0;
		m_SamplesConsumed = 0;
		m_NumQueuedPackets = 0;
		m_DecodingPacketIndex = m_SubmittedPacketIndex = 0;
		m_WaveIdentifier = 0;
		m_NumChannels = numChannels;
		m_SampleRate = GetXmaSampleRate(sampleRate);
		m_HaveRequestedLock = false;

		m_ReadyForMoreData = true;
		m_State = audDecoder::IDLE;
		m_AbortPlayback = false;
		return true;
	}

	void audDecoderXma::Shutdown()
	{
		for(s32 i = 0; i < m_Packets.GetMaxCount(); i++)
		{
			if(m_Packets[i].Playback)
			{
				AssertVerify(SUCCEEDED(XMAPlaybackDestroy(m_Packets[i].Playback)));
				m_Packets[i].Playback = NULL;
				audDriver::FreePhysical(m_Packets[i].ContextMemory);
				m_Packets[i].ContextMemory = NULL;
			}
		}
	}

	u32 audDecoderXma::ComputeNumSamplesInBuffer(const u8 *buffer, const u32 bufferLength)
	{
		u32 byteOffset = 0;
		u32 numSamples = 0;
		while(byteOffset < bufferLength)
		{
			u32 metadata = GetXmaPacketMetadata(buffer+byteOffset);
			if(audVerifyf(metadata == 1, "%u: Invalid XMA2 data; expected 1, got %u at buffer %p offset %u", m_WaveIdentifier, metadata, buffer, byteOffset))
			{
				const u32 packetFrameCount = GetXmaPacketFrameCount(buffer+byteOffset);
				const u32 skip = 1+GetXmaPacketSkipCount(buffer+byteOffset);

				numSamples += XMA_SAMPLES_PER_FRAME * packetFrameCount;
				byteOffset += skip*2048U;
			}
			else
			{
				// Failure case - prevent invalid memory access by giving up when we encounter bad data.
				return 0;
			}
		}
		return numSamples;
	}

	u32 audDecoderXma::ComputeBytesToSkip(const u8 *buffer, const u32 bufferLengthBytes, const u32 samplesToSkip, u32 &samplesSkipped)
	{
		u32 byteOffset = 0;
		u32 numSamples = 0;
		while(byteOffset < bufferLengthBytes)
		{
			u32 metadata = GetXmaPacketMetadata(buffer+byteOffset);
			if(audVerifyf(metadata == 1, "%u: Invalid XMA2 data; expected 1, got %u (%p)", m_WaveIdentifier, metadata, buffer+byteOffset))
			{
				const u32 packetFrameCount = GetXmaPacketFrameCount(buffer+byteOffset);
				const u32 skip = 1+GetXmaPacketSkipCount(buffer+byteOffset);
	
				samplesSkipped = numSamples;
				numSamples += XMA_SAMPLES_PER_FRAME * packetFrameCount;
				if(numSamples > samplesToSkip)
				{
					return byteOffset;
				}
				byteOffset += skip*2048U;
			}
			else
			{
				// Failure case - prevent invalid memory access by giving up when we encounter bad data.
				return 0;
			}
		}
		return byteOffset;
	}

	void audDecoderXma::ComputeLoopData(const audPacket &packet, audXmaPacketInfo &xmaPacketInfo)
	{
		GetXmaDecodePositionForSample((const BYTE*)packet.inputData, packet.inputBytes, 0, packet.LoopBegin, (DWORD*)&xmaPacketInfo.LoopStartBits, (DWORD*)&xmaPacketInfo.LoopSubframeSkip);
		GetXmaDecodePositionForSample((const BYTE*)packet.inputData, packet.inputBytes, 0, packet.LoopEnd, (DWORD*)&xmaPacketInfo.LoopEndBits, (DWORD*)&xmaPacketInfo.LoopSubframeEnd);

		if(xmaPacketInfo.LoopEndBits == 0)
		{
			const u32 lastBit = (u32)GetLastXmaFrameBitPosition((const BYTE*)packet.inputData, packet.inputBytes, 0);
			xmaPacketInfo.LoopEndBits = lastBit;
		}	
	}

	void audDecoderXma::SubmitPacket(rage::audPacket &packet)
	{
		Assert(!(((size_t)packet.inputData)&2047));
		Assert(!(packet.inputBytes&2047));
		Assert(m_ReadyForMoreData);

		audXmaPacketInfo &submittedPacket = m_Packets[m_SubmittedPacketIndex];

		Assert(submittedPacket.ContextMemory == NULL && submittedPacket.Playback == NULL);
		
		// Create a hardware context to decode this packet
		XMA_PLAYBACK_INIT init;
		
		Assign(init.channelCount, m_NumChannels);
		init.outputBufferSizeInSamples = 12*128;
		init.sampleRate = m_SampleRate;
		init.subframesToDecode = 4;

		const u32 memRequiredBytes = XMAPlaybackGetRequiredBufferSize(1, &init);
		audAssertf(memRequiredBytes == audDriver::kXMAContextSizeBytes, "Unexpected XMA context size (new XDK?)  Change audDriver::kXMAContextSizeBytes.  Expected %u, got %u", audDriver::kXMAContextSizeBytes, memRequiredBytes);
		
		submittedPacket.ContextMemory = audDriver::AllocatePhysical(Max<u32>(audDriver::kXMAContextSizeBytes, memRequiredBytes), 128);
		Assert(submittedPacket.ContextMemory);
		if(!submittedPacket.ContextMemory)
		{
			audErrorf("Failed to allocate XMA decoder context memory");
			return;
		}

		submittedPacket.Playback = NULL;
		if(FAILED(XMAPlaybackCreate(1, &init, XMA_PLAYBACK_CREATE_USE_PROVIDED_MEMORY, &submittedPacket.Playback, 
			(BYTE*)submittedPacket.ContextMemory, audDriver::kXMAContextSizeBytes)))
		{
			audErrorf("Failed to create XMA decoder context");
			return;
		}
		Assert(submittedPacket.Playback);
		////

		if(packet.PlayBegin && packet.PlayBegin == packet.PlayEnd)
		{
			// Entire packet is to be skipped
			return;
		}

		const u8 *data = (const u8*)packet.inputData;
		u32 dataSize = packet.inputBytes;

		u32 skippedSamples = 0;

		// If we're looping then we need to submit the entire packet to the hardware, even if we're
		// going to skip some of it below
		if(packet.PlayBegin && packet.LoopEnd == 0)
		{		
			u32 skipBytes = ComputeBytesToSkip(data, dataSize, packet.PlayBegin, skippedSamples);
			data += skipBytes;
			dataSize -= skipBytes;
		}

		audAssertf(packet.PlayEnd == 0 || packet.PlayEnd > packet.PlayBegin, "PlayBegin: %u PlayEnd: %u", packet.PlayBegin, packet.PlayEnd);

		// Set up hardware looping if required
		bool hasLoop = false;
		XMA_PLAYBACK_LOOP loopInfo;
		if(packet.LoopEnd != 0)
		{
			hasLoop = true;
			ComputeLoopData(packet, submittedPacket);
			
			// Populate struct for XMA HAL
			loopInfo.dwLoopStartOffset = submittedPacket.LoopStartBits;
			loopInfo.dwLoopEndOffset = submittedPacket.LoopEndBits;
			Assign(loopInfo.dwLoopSubframeEnd, submittedPacket.LoopSubframeEnd);
			Assign(loopInfo.dwLoopSubframeSkip, submittedPacket.LoopSubframeSkip);
			loopInfo.numLoops = 255;//(packet.LoopCount == audPacket::kInfiniteLoopCount ? 255 : (BYTE)packet.LoopCount);
		}
		else
		{
			submittedPacket.LoopStartBits = submittedPacket.LoopEndBits = 0;
			submittedPacket.LoopSubframeSkip = submittedPacket.LoopSubframeEnd = 0;
		}

		submittedPacket.numSamples = ComputeNumSamplesInBuffer(data, dataSize);

		u32 bitOffset = 0, numSubframesToSkip = 0;
		bool setDecodePosition = false;
	
		// Compute PlayBegin in terms of whole 512 sample frames, 128 sample subframes and a remainder; 
		// the hardware can skip entire subframes more efficiently than we can.
		u32 numSamplesToSkipAtStart = packet.PlayBegin - skippedSamples;
		if(numSamplesToSkipAtStart >= 128)
		{
			// Leave 128 samples to skip in software, to avoid the ramp up due to hw windowing
			numSamplesToSkipAtStart -= 128;
		}
			
		const u32 numFramesToSkip = numSamplesToSkipAtStart >> 9;
		const u32 numSamplesSkippedInWholeFrames = numFramesToSkip<<9;
		numSubframesToSkip = (numSamplesToSkipAtStart - numSamplesSkippedInWholeFrames) >> 7;
		const u32 numSamplesSkippedInWholeSubframes = numSubframesToSkip << 7;
		const u32 numSamplesSkippedByHardware = numSamplesSkippedInWholeFrames + numSamplesSkippedInWholeSubframes;
	
		submittedPacket.PlayBegin = packet.PlayBegin - skippedSamples;

		// We only actually call XMAPlaybackSetDecodePosition if we have any full frames to skip. Therefore we shouldn't
		// increment the samplesConsumed value if this is not the case
		if(numFramesToSkip)
		{
			submittedPacket.NumSamplesConsumed = numSamplesSkippedByHardware;
			bitOffset = GetXmaFrameBitPosition(data, dataSize, 0, numFramesToSkip);
			setDecodePosition = true;
		}
		else
		{
			submittedPacket.NumSamplesConsumed = 0;
		}

		submittedPacket.PlayEnd = packet.PlayEnd ? 
			packet.PlayEnd-skippedSamples : 
		submittedPacket.numSamples;		
	
		m_SubmittedPacketIndex = (m_SubmittedPacketIndex + 1) % m_Packets.size();

		if(m_SubmittedPacketIndex == m_DecodingPacketIndex)
		{
			m_ReadyForMoreData = false;
		}

		AssertVerify(SUCCEEDED(XMAPlaybackRequestModifyLock( submittedPacket.Playback )));
		AssertVerify(SUCCEEDED(XMAPlaybackWaitUntilModifyLockObtained( submittedPacket.Playback )));

		if(hasLoop)
		{
			AssertVerify(SUCCEEDED(XMAPlaybackSetLoop(submittedPacket.Playback, 0, &loopInfo)));
		}

		AssertVerify(SUCCEEDED(XMAPlaybackSubmitData(submittedPacket.Playback, 0, data, dataSize)));

		if(setDecodePosition)
		{
			AssertVerify(SUCCEEDED(XMAPlaybackSetDecodePosition(submittedPacket.Playback, 0, bitOffset, numSubframesToSkip)));
		}
		
		AssertVerify(SUCCEEDED(XMAPlaybackResumePlayback(submittedPacket.Playback)));
		m_NumQueuedPackets++;
		m_BytesSubmitted += packet.inputBytes;
		m_State = audDecoder::DECODING;
	}

	u32 audDecoderXma::QueryNumAvailableSamples() const
	{
		return (m_RingBuffer.GetBytesAvailableToRead()>>1);
	}

	u32 audDecoderXma::ReadData(s16 *dest, const u32 numSamples) const
	{
		return m_RingBuffer.PeakData(dest, numSamples<<1)>>1;
	} 

	void audDecoderXma::AdvanceReadPtr(const u32 numSamples)
	{
		m_RingBuffer.AdvanceReadPtr(numSamples<<1);
		m_SamplesConsumed += numSamples;
	}

	void audDecoderXma::ClearBuffer()
	{
		m_RingBuffer.Reset();
		// Reset to allow skipping into the next packet
		m_BytesSubmitted = 0;
		// TODO: should we also flush the hardware?
	}

	void audDecoderXma::FlushPackets()
	{
		for(u32 packetIndex = 0; packetIndex < 2; packetIndex++)
		{
			audXmaPacketInfo &packet = m_Packets[packetIndex];

			if(packet.Playback)
			{
				XMAPlaybackRequestModifyLock(packet.Playback);
			}
		}

		for(u32 packetIndex = 0; packetIndex < 2; packetIndex++)
		{
			audXmaPacketInfo &packet = m_Packets[packetIndex];

			if(packet.Playback)
			{
				XMAPlaybackWaitUntilModifyLockObtained(packet.Playback);
				XMAPlaybackFlushData(packet.Playback, 0);
				AssertVerify(SUCCEEDED(XMAPlaybackDestroy(packet.Playback)));
				packet.Playback = NULL;
				audDriver::FreePhysical(packet.ContextMemory);
				packet.ContextMemory = NULL;
			}
		}
		
		m_NumQueuedPackets = 0;
		m_State = audDecoder::FINISHED;
		m_DecodingPacketIndex = m_SubmittedPacketIndex = 0;
		m_ReadyForMoreData = true;
		m_HaveRequestedLock = false;
	}

	void audDecoderXma::PreUpdate()
	{
		if(m_Packets[m_DecodingPacketIndex].Playback && !m_RingBuffer.IsFull())
		{
			XMAPlaybackRequestModifyLock( m_Packets[m_DecodingPacketIndex].Playback );
			m_HaveRequestedLock = true;
		}
	}

	void audDecoderXma::Update()
	{
		XMA_PF_FUNC(DecoderXmaUpdate);

		if(m_Packets[m_DecodingPacketIndex].Playback == NULL)
			return;
		
		// If decodingPacket == submittedPacket then we need to wait before submitting new data
		m_ReadyForMoreData = ((m_DecodingPacketIndex!=m_SubmittedPacketIndex) || m_State != DECODING);	

		// No point going any futher; we don't have any space to read from the hardware
		if(m_RingBuffer.IsFull())
		{
			Assert(!m_HaveRequestedLock);
			return;
		}

#define AUD_XMA_CHECK_ERRORS (RSG_ASSERT && !__NO_OUTPUT)
#if AUD_XMA_CHECK_ERRORS
		const u32 errorBits = (u32)XMAPlaybackGetErrorBits(m_Packets[m_DecodingPacketIndex].Playback, 0);
		const u32 parseError = (u32)XMAPlaybackGetParseError(m_Packets[m_DecodingPacketIndex].Playback, 0);
#endif
		
		u32 samplesAvailable = XMAPlaybackQueryAvailableData(m_Packets[m_DecodingPacketIndex].Playback, 0);
		bool outOfSpace = false;
		u32 samplesDecoded = 0;
		u32 pass = 0;

		bool haveLocked = false;

		while(samplesAvailable > 0 && !outOfSpace && m_State != audDecoder::FINISHED && pass++ < 10)
		{
			const s16 *ptr = NULL;
			audXmaPacketInfo &packet = m_Packets[m_DecodingPacketIndex];

		
			// If we're currently skipping data then skip as much as we can, without any concern for our 
			// internal ringbuffer space
			if(packet.NumSamplesConsumed < packet.PlayBegin)
			{
				// Inside the PlayBegin section of this packet
				const u32 numSamplesToSkip = Min(samplesAvailable, packet.PlayBegin - packet.NumSamplesConsumed);
				u32 numSamplesSkipped = 0;
				u32 skipIterations = 0;

				while(numSamplesSkipped < numSamplesToSkip && skipIterations++ < 10)
				{
					if(!haveLocked)
					{
						XMA_PF_FUNC(DecoderXmaUpdate_Wait);
						
						if(!m_HaveRequestedLock)
						{
							XMAPlaybackRequestModifyLock( packet.Playback );
						}
						
						XMAPlaybackWaitUntilModifyLockObtained( packet.Playback );
						haveLocked = true;
						m_HaveRequestedLock = false;
					}
					numSamplesSkipped += XMAPlaybackConsumeDecodedData(packet.Playback, 0, numSamplesToSkip - numSamplesSkipped, (void**)&ptr);
				}

				Assert(numSamplesSkipped == numSamplesToSkip);
				packet.NumSamplesConsumed += numSamplesSkipped;
				samplesAvailable -= numSamplesSkipped;
				samplesDecoded += numSamplesSkipped;
			}
			// Only allow PlayBegin packet trimming when playing loops - loop points are aligned to 128
			// sample boundaries and the hardware takes care of everything.  PlayBegin is necessary to allow
			// subframe start offsets.
			if(packet.LoopEndBits == 0)
			{
				if(samplesAvailable && packet.NumSamplesConsumed >= packet.PlayEnd)
				{				
					// Inside the PlayEnd section of this packet
					// Flush the hardware and mark that this packet has been fully consumed

					packet.NumSamplesConsumed = packet.numSamples;

					if(!haveLocked)
					{
						XMA_PF_FUNC(DecoderXmaUpdate_Wait);
						if(!m_HaveRequestedLock)
						{
							XMAPlaybackRequestModifyLock( packet.Playback );
						}
						m_HaveRequestedLock = false;
						XMAPlaybackWaitUntilModifyLockObtained( packet.Playback );
						haveLocked = true;
					}

					XMAPlaybackFlushData(packet.Playback, 0);
					samplesAvailable = 0;					
				}
			}

			if(samplesAvailable && 
				(packet.NumSamplesConsumed >= packet.PlayBegin && packet.NumSamplesConsumed < packet.PlayEnd) ||
				(packet.LoopEndBits != 0 && packet.NumSamplesConsumed >= packet.PlayBegin))
			{				
				// Inside play region of this packet
				const u32 ringBufferFreeSpace = (m_RingBuffer.GetBufferSize()-m_RingBuffer.GetBytesAvailableToRead()) >> 1;
				if(ringBufferFreeSpace == 0)
				{
					outOfSpace = true;
				}
				else
				{
					// Loops have an infinite 'PlayRegion'
					const u32 samplesLeftInPacketPlayRegion = packet.LoopEndBits == 0 ? 
						1 + packet.PlayEnd - packet.NumSamplesConsumed :
						~0U;

					const u32 samplesToConsume = Min(samplesLeftInPacketPlayRegion, samplesAvailable, ringBufferFreeSpace);
					u32 samplesConsumed = 0;
					u32 pass2 = 0;
					while(samplesConsumed < samplesToConsume && ++pass2 < 2)
					{
						if(!haveLocked)
						{
							XMA_PF_FUNC(DecoderXmaUpdate_Wait);
							if(!m_HaveRequestedLock)
							{
								XMAPlaybackRequestModifyLock( packet.Playback );
							}
							m_HaveRequestedLock = false;
							XMAPlaybackWaitUntilModifyLockObtained( packet.Playback );
							haveLocked = true;
						}
						const u32 numSamplesThisTime = XMAPlaybackConsumeDecodedData(packet.Playback, 0, samplesToConsume-samplesConsumed, (void**)&ptr);
						m_RingBuffer.WriteData(ptr, numSamplesThisTime * sizeof(s16));
						samplesConsumed += numSamplesThisTime;
					}
					/*if(pass > 2)
					{
						audWarningf("XMA decode consume %u samples (%u available) took %u passes", samplesToConsume, samplesAvailable, pass);
					}*/
					samplesDecoded += samplesConsumed;
					samplesAvailable -= samplesConsumed;
					packet.NumSamplesConsumed += samplesConsumed;
					if(samplesConsumed == ringBufferFreeSpace)
					{
						outOfSpace = true;
					}
				}
			}

			// If the packet is set to loop (LoopEndBits!=0) then it will continue decoding forever
			if(packet.LoopEndBits == 0 && m_NumQueuedPackets > 0)
			{
				//Assert(packet.NumSamplesConsumed <= packet.numSamples);
				if(packet.NumSamplesConsumed >= packet.numSamples || m_AbortPlayback)
				{
					m_AbortPlayback = false;

					// destroy playback context for this packet; we're moving on
					m_HaveRequestedLock = false;
					AssertVerify(SUCCEEDED(XMAPlaybackDestroy(packet.Playback)));
					packet.Playback = NULL;
					audDriver::FreePhysical(packet.ContextMemory);
					packet.ContextMemory = NULL;

					// move onto next packet
					m_NumQueuedPackets--;

					if(m_NumQueuedPackets > 0)
					{
						// more packets in queue, proceed
						m_DecodingPacketIndex = (m_DecodingPacketIndex+1) % m_Packets.size();
					}
					else
					{
						m_State = audDecoder::FINISHED;
						m_DecodingPacketIndex = m_SubmittedPacketIndex = 0;
						m_ReadyForMoreData = true;
					}
				}
			}
			
			
			m_SamplesDecoded += samplesDecoded;
			/*if(samplesDecoded > 0)
			{
				audDisplayf("Decoded %u samples [%u avail]", samplesDecoded, m_RingBuffer.GetBytesAvailableToRead()>>1);
			}*/
		}

		if(m_Packets[m_DecodingPacketIndex].Playback)
		{
			if(m_HaveRequestedLock)
			{
				// If we get here, then we've requested a lock on the playback in PreUpdate(), but haven't 
				// yet waited until the lock has been obtained, so do that now before continuing
				XMAPlaybackWaitUntilModifyLockObtained( m_Packets[m_DecodingPacketIndex].Playback );
				m_HaveRequestedLock = false;
			}
	
			XMAPlaybackResumePlayback(m_Packets[m_DecodingPacketIndex].Playback);
		}

#if AUD_XMA_CHECK_ERRORS
		// according to MS Game Developer Support errorBits fires spuriously, and the only one we should look 
		// out for is INTO_INVALID_READ_BUFFER.
		// parseErrors are reliable though.
		if(errorBits == XMA_PLAYBACK_ERROR_FRAME_CROSSES_BOUNDARY_INTO_INVALID_READ_BUFFER
			|| parseError != XMA_PLAYBACK_PARSE_ERROR_NONE)
		{
			m_State = audDecoder::DECODER_ERROR;
			audErrorf("XMA Error: %u:%u, had consumed %u / %u samples from decoding packet %u (Begin: %u, End: %u, Playback: %p).  %u samples in total consumed", errorBits, parseError, 
				m_Packets[m_DecodingPacketIndex].NumSamplesConsumed,
				m_Packets[m_DecodingPacketIndex].numSamples,
				m_DecodingPacketIndex,
				m_Packets[m_DecodingPacketIndex].PlayBegin,
				m_Packets[m_DecodingPacketIndex].PlayEnd,
				m_Packets[m_DecodingPacketIndex].Playback,
				m_SamplesConsumed);
		}
#endif
	}

} // namespace rage

#endif // __XENON
