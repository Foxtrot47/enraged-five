#ifndef AUD_MIDI_H
#define AUD_MIDI_H

#if __WIN32PC
#include "system/xtl.h"
#include "mmsystem.h"
#endif

#include "atl/array.h"
#include "atl/atfunctor.h"
#include "system/criticalsection.h"

namespace rage
{
	struct audMidiMessage
	{
		u32 Message;
		u32 Timestamp;
	};

	typedef atFunctor1<void,const audMidiMessage &> MidiMessageFunctor;

	class audMidiIn
	{
	public:
		enum audMidiMessageTypes
		{
			kNoteOn = 0x90,
			kNoteOff = 0x80,
			kController = 0xB0,
		};

		audMidiIn();
		~audMidiIn();

		static s32 GetNumDevices();

		bool Init(const s32 deviceId);
		void Shutdown();


		const char *GetName() const;

		u32 GetManufacturerId() const { return m_ManufacturerId; }
		u32 GetProductId() const { return m_ProductId; }

		enum EmptyQueueFlag { kEmptyQueue = 0, kDontEmptyQueue};
		bool ProcessQueue(MidiMessageFunctor callback, const u32 channel = 0, EmptyQueueFlag emptyQueue = kDontEmptyQueue);
		void EmptyQueue();
		void SwapQueueBuffer();

	private:

		void AddMessageToQueue(const audMidiMessage &message);

#if __WIN32PC
		static void CALLBACK MidiInProc(
			HMIDIIN hMidiIn,  
			UINT wMsg,        
			DWORD_PTR dwInstance, 
			DWORD_PTR dwParam1,   
			DWORD_PTR dwParam2    
			);

		char m_Name[128];
		HMIDIIN m_Handle;
#endif // __WIN32PC

		enum {kMaxMessages = 64};
		atFixedArray<audMidiMessage, kMaxMessages> m_MessageBuffer[2];

		u32 m_ManufacturerId;
		u32 m_ProductId;

		u32 m_WriteQueueId;

		sysCriticalSectionToken m_Lock;
	};
}

#endif // AUD_MIDI_H
