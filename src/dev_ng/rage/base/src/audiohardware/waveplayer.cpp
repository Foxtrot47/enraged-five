//
// audiohardware/waveplayer.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#if !__SPU

#include "decoder.h"
#include "decodemgr.h"
#include "device.h"
#include "driver.h"
#include "streamingwaveslot.h"
#include "syncsource.h"
#include "voicemgr.h"
#include "wavedefs.h"
#include "waveslot.h"
#include "file/stream.h"
#include "file/asset.h"
#include "profile/profiler.h"
#include "string/stringhash.h"

#else
#include "framebuffercache.h"
#include "system/dma.h"
#endif

#if __XENON
#include "decoder_xma.h"
#include <xma2defs.h>
#endif

#include "decoder_ps4mp3.h"
#include "decoder_spu.h"
#include "mp3util.h"
#include "mixer.h"
#include "src_linear.h"
#include "waveplayer.h"

#include "mixing_vmath.inl"

#include "debug.h"

#include "file/remote.h" // for fiIsShowingMessageBox
#include "math/amath.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"

//OPTIMISATIONS_OFF()

namespace rage
{

#if __SPU
	extern f32 *g_PCMBuffers;

	u8 *g_ScratchDecoder;
	// maximum number of samples required is ((mixBufNumSamples * maxSampleRateRatio)+2), we need an extra 8 for misaligned loads and this must be aligned up to 16 bytes (8 shorts)
	s16 *g_ScratchLoadBuffer;
	size_t g_ScratchLoadBufferSize = sizeof(s16) * ((kMixBufNumSamples*kMaxSampleRateRatio + 2 + 8 + 7) & ~7);
#else
	PF_PAGE(WavePlayerPage, "RAGE Audio WavePlayer");
	PF_GROUP(WavePlayerGroup);
	PF_LINK(WavePlayerPage, WavePlayerGroup);
	PF_VALUE_INT(StreamingDelayFrames, WavePlayerGroup);

	// On PPU this is only used as an SRC output buffer when we have a predelay
	ALIGNAS(16) f32 g_ScratchLoadBuffer[kMixBufNumSamples] ;
	const size_t g_ScratchLoadBufferSize = sizeof(g_ScratchLoadBuffer);
#endif

#if !__SPU
	audWavePlayer::audWavePlayer() : audPcmSource(AUD_PCMSOURCE_WAVEPLAYER)
	{
		m_BaseFrequency = 0;
		m_PlaybackFrequencyOffset = 0;
		m_PlayPositionSamples = 0;
		m_FinishedPlayback = false;
		m_SrcTrailingFrac = 0.f;
		m_IsDecoded = false;
		m_Decoder = NULL;
		m_DecodeState = DECODE_IDLE;
		m_DecodeDelay = 0;
		m_PlayState = kStateIdle;
		m_IsStreaming = false;
		m_StreamClientId = -1;
		// XMA hardware can take a frame to produce data, so add 1 frame predelay to everything to compensate
		m_PredelayFrames = (__XENON || RSG_DURANGO) ? 1 : 0;
		m_PredelaySamples = 0;
		m_HadFinalBlock = false;
		m_IsStarved = false;
		m_StopRequested = false;
		m_FramesWaiting = 0;
		m_FramesWaitingForStreaming = 0;
		m_FramesStarved = 0;

		m_InitialDataSubmittedToDecoder = false;
		m_IsPaused = false;
		m_IsPauseRequested = false;

		m_SyncTriggerTimeSamples = ~0U;

#if AUD_WAVEPLAYER_METERING
		m_PeakLevel = 0.f;
#endif
		m_PeakData = NULL;
		m_NumPeakSamples = 0;
		m_WaveDataPtr = NULL;
	}

	void audWavePlayer::Shutdown()
	{
		Assert(!m_Decoder);
		if(m_Decoder)
		{
			audDriver::GetMixer()->GetDecodeMgr().FreeDecoder(m_Decoder);
			m_Decoder = NULL;
		}
		if(m_StreamClientId != -1)
		{
			GetStreamSlot()->UnregisterClient(m_StreamClientId);
			m_StreamClientId = -1;
		}

		m_WaveRef.Shutdown();

		FreeBuffers();

		if(m_SyncMasterId != audMixerSyncManager::InvalidId)
		{
			audMixerSyncManager::Get()->Cancel(m_SyncMasterId);
			m_SyncMasterId = audMixerSyncManager::InvalidId;
		}
	}

	void audWavePlayer::Init(const u32 waveRef)
	{	
		m_HasBegunFrame = false;
		m_HasEndedFrame = false;
		if(m_WaveRef.HasValidWave())
		{
			audWarningf("Wave player changing wave reference");
			m_WaveRef.Shutdown();
		}

		// this doesn't add a reference to the slot, but one should have been added for us at a higher level.
		m_WaveRef.Init(waveRef);
		if(!m_WaveRef.HasValidWave())
		{
			audErrorf("Wave player with invalid wave reference");
			return;
		}
		
		u32 formatChunkSize = 0;
		m_WaveFormat = m_WaveRef.FindFormat(formatChunkSize);
		Assert(m_WaveFormat);
		// backwards compatible with old format chunk
		m_FirstPeakLevelSample = AUD_READ_FIRST_PEAK_SAMPLE(m_WaveFormat, formatChunkSize);

		m_WaveHeadroom = m_WaveFormat->Headroom * 0.01f;
		
		m_BaseFrequency = m_WaveFormat->SampleRate;
		m_PlaybackFrequencyOffset = 0;

		m_Format = m_WaveFormat->Format;
		ASSERT_ONLY(audWaveSlot *slot = audWaveSlot::GetWaveSlotFromIndex(GetWaveSlotId()));
		audAssertf(slot, "Failed to get wave slot %d", GetWaveSlotId());
		
		if(!audVerifyf(m_Format < audWaveFormat::kNumFormats, "Invalid wave format: %d.  Wave slot %s (%s) wave hash %u", m_Format, slot->GetSlotName(), slot->GetLoadedBankName(), GetWaveNameHash()))
		{
			m_FinishedPlayback = true;
			return;
		}

		const audWaveFormat::audStreamFormat native = (__BE ? audWaveFormat::kPcm16bitBigEndian : audWaveFormat::kPcm16bitLittleEndian);
		if(m_Format != native)
		{
			m_IsDecoded = true;
		}

		// This is only going to cause issues on Xbox, but asserting on all platforms to increase the likelihood of catching this!
		audAssertf(!m_IsDecoded || m_WaveRef.GetSlot()->AllowCompressedAssetLoad(), "Slot %s does not support compressed asset loading (Bank %s)!", m_WaveRef.GetSlot()->GetSlotName(), m_WaveRef.GetSlot()->GetLoadedBankName());

		PcmSourceState *pcmSourceState = audDriver::GetVoiceManager().GetPcmSourceState(audDriver::GetMixer()->GetPcmSourceSlotId(this));

		pcmSourceState->PlaytimeSamples = 0;
		pcmSourceState->AuthoredSampleRate = m_WaveFormat->SampleRate;
		// Currently only mono waves are supported
		pcmSourceState->NumChannels = 1;

		if(m_WaveRef.GetSlot()->IsStreaming())
		{
			m_IsStreaming = true;
			Assign(m_StreamClientId, GetStreamSlot()->RegisterClient(m_WaveRef.GetObjectId()));
			Assert(m_StreamClientId != -1);
			Assert(m_StreamClientId >= -1);
			m_IsLooping = GetStreamSlot()->IsLooping();

			m_LoopStartOffsetSamples = 0;
		}
		else
		{
			m_IsLooping = pcmSourceState->isLooping = (m_WaveFormat->LoopPointSamples != -1);
			m_LoopStartOffsetSamples = m_WaveFormat->LoopPointSamples;
			m_WaveDataPtr = static_cast<const s16*>(m_WaveRef.FindWaveData(m_WaveLengthBytes));
			audAssertf(m_WaveDataPtr, "Failed to find wave data for wave %s %u", m_WaveRef.GetSlot()->GetLoadedBankName(), m_WaveRef.GetWaveNameHash());
		}

#if __PS3
		// We also need a decoder if the source PCM data is in VRAM
		if(!m_WaveRef.GetSlot()->IsStreaming() && gcm::IsLocalPtr(m_WaveDataPtr))
		{
			m_IsDecoded = true;
		}
#endif

#if __XENON || RSG_DURANGO
		if(m_IsStreaming)
		{
			m_WaveLengthSamples = pcmSourceState->LengthSamples = m_WaveFormat->LengthSamples;
		}
		else
		{
			// Use the number of valid samples as our length; ignoring the additional padding generated by the
			// XMA encoder
			if(m_Format == audWaveFormat::kXMA2)
			{
				m_WaveLengthSamples = pcmSourceState->LengthSamples = m_WaveFormat->PlayEnd == 0 ? 0 : (m_WaveFormat->PlayEnd<<7) - 1;
			}
			else
			{
				m_WaveLengthSamples = pcmSourceState->LengthSamples = m_WaveFormat->LengthSamples;
			}
		}

#else
		m_WaveLengthSamples = pcmSourceState->LengthSamples = m_WaveFormat->LengthSamples;
#endif

		// Validate loop point against wave length
		if(!audVerifyf(!m_IsLooping || m_WaveLengthSamples > static_cast<u32>(m_LoopStartOffsetSamples), "Invalid loop start point %d, wave length is %u (%s: %s / %u)", m_LoopStartOffsetSamples, m_WaveLengthSamples, slot->GetSlotName(), slot->GetLoadedBankName(), GetWaveNameHash()))
		{
			m_FinishedPlayback = true;
			return;
		}
			
		u32 peakChunkSize = 0;
		m_PeakData = (const u16*)m_WaveRef.FindChunk(ATSTRINGHASH("PEAK",0x8B946236), peakChunkSize);
		m_NumPeakSamples = peakChunkSize >> 1;

		ComputeCurrentPeak();
	}

	void audWavePlayer::SubmitBlock(const audStreamingWaveBlock &blockInfo, const s32 startOffsetSamples /* = -1 */)
	{
		audPacket packet;

		audAssertf(blockInfo.waveDataPtr, "Streaming load block with NULL wave data pointer: blockId: %u slot id %u, wave id %u, stream client id: %u", blockInfo.blockId, m_WaveRef.GetSlotId(), m_WaveRef.GetObjectId(), m_StreamClientId);
		if(blockInfo.waveDataPtr == NULL)
		{
			audErrorf("Invalid streaming block: %u", GetWaveNameHash());
			m_FinishedPlayback = true;
			return;
		}
		packet.inputData = blockInfo.waveDataPtr;
		packet.PlayBegin = blockInfo.metadata.SamplesToSkip;
		packet.PlayEnd = blockInfo.metadata.NumSamples;

		if(m_Format == audWaveFormat::kAdpcm)
		{
			packet.inputBytes = blockInfo.metadata.NumPackets*2048;						
		}
		else if(m_Format == audWaveFormat::kXMA2)
		{
			// if we are playing this block to follow on from a previously submitted block we must skip the entire packet if SamplesToSkip is non-zero
			// since this means that the first packet is duplicated from the the previous block.  
			// If we are starting from this block (m_SubmittedFirstPacket==false) we must skip the actual number of samples in SamplesToSkip, 
			// since that gets us to the official starting point of the block, however as long as we calculate the buffer relative start
			// offset correctly we get that for free (since the start index in the seek table is the true start offset of this packet, which
			// will be prior to our requested start offset).
			// Loop begin is only necessary when we're looping back round; not on the first submitted packet

			packet.inputBytes = blockInfo.metadata.NumPackets*2048;

			u32 samplesToSkip = 0;

			if(blockInfo.blockId == 0)
			{
				samplesToSkip += (m_WaveFormat->LoopBegin << 7);  // mult by 128,if LoopBegin==3 then here is out 384 offset
			}

			samplesToSkip += blockInfo.metadata.SamplesToSkip; 

			if(blockInfo.IsFinalBlock)
			{
				// Use the stored length in samples to trim the final packet
				const u32 trackLengthSamples = m_WaveFormat->LengthSamples;
				const u32 trimmedBlockLength = trackLengthSamples - blockInfo.packetSeekTable[0].StartSample;
				packet.PlayEnd = trimmedBlockLength;
			}
			
			packet.PlayBegin = Min(packet.PlayEnd, samplesToSkip);
		}
		else if(m_Format == audWaveFormat::kMP3)
		{
			packet.inputBytes = blockInfo.metadata.NumberOfBytes;

			u32 playBegin = 0;
			if(blockInfo.blockId == 0)
			{
				playBegin += audMp3Util::kFrameSizeSamples;
			}
			if(blockInfo.IsFinalBlock)
			{
				packet.LoopEnd = blockInfo.metadata.NumSamples;
			}

			playBegin += blockInfo.metadata.SamplesToSkip;						
			packet.PlayBegin = playBegin;			
		}
		else
		{
			packet.inputBytes = blockInfo.metadata.NumSamples<<1;	// NOTE: assuming 16bit PCM
			packet.PlayEnd = blockInfo.IsFinalBlock ? blockInfo.metadata.NumSamples : ~0U;	
		}
		packet.BlockStartSample = blockInfo.packetSeekTable[0].StartSample;

		packet.IsFinalBlock = blockInfo.IsFinalBlock;

		packet.WaveSlotId = m_WaveRef.GetSlotId();
		//audDisplayf("Wave %d PlayBegin %u, PlayEnd %u, blockId %u %s", m_WaveRef.GetObjectId(), packet.PlayBegin, packet.PlayEnd, blockInfo.blockId, blockInfo.IsFinalBlock ? "(final block)" : "");

		if(startOffsetSamples >= 0)
		{
			packet.PlayBegin = static_cast<u32>(startOffsetSamples);
			if(m_Format == audWaveFormat::kMP3 && blockInfo.blockId == 0)
			{
				packet.PlayBegin += 1152;
			}
		}
		if(packet.PlayBegin >= packet.PlayEnd)
		{
			audWarningf("%u: Skipping packet: wave %u, bank %s", audDriver::GetMixer()->GetMixerTimeFrames(), GetWaveNameHash(), GetBankName());
			if(packet.IsFinalBlock && !GetStreamSlot()->IsLooping())
			{
				m_FinishedPlayback = true;
			}
		}
		else
		{
#if __BANK
			packet.WaveNameHash = GetWaveNameHash();
#endif
			m_Decoder->SubmitPacket(packet);		
		}
	}

	void audWavePlayer::Start()
	{
		if(m_PlayState != kStateIdle)
		{
#if __DEV
			static bool haveTraced = false;
			if(!haveTraced)
			{
				audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
				haveTraced = true;
			}
#endif
			audAssertf(m_PlayState == kStateIdle, "audWavePlayer::Start when not idle (play state %d), id: %d", m_PlayState, audDriver::GetMixer()->GetPcmSourceSlotId(this));
		}

		// On PS3 we won't have had the WaveRef message yet, so m_WaveRef will be invalid.
		// It will be valid before StartPhys() or BeginFrame()

		m_FinishedPlayback = false;
		m_PlayState = kStatePlaying;

		// with shadow sounds, the voice in charge of the PCM source might never get a physical voice assign, in case that happens, make sure we submit the initial data to the decoder when told to play.
		if(!m_InitialDataSubmittedToDecoder && m_NumPhysicalClients > 0)
		{
			SumbitDataToDecoder();
			m_InitialDataSubmittedToDecoder = true;
			audAssertf(m_IsDecoded || !m_Decoder, "At this point we should have a decoder");
		}
	}

	void audWavePlayer::StartPhys(s32 UNUSED_PARAM(channel))
	{
#if RSG_PC || RSG_ORBIS || RSG_DURANGO
		if (!audDriver::GetMixer()->GetDecodeMgr().IsFormatSupported((audWaveFormat::audStreamFormat)m_Format))
		{
			//Warningf("format is unsupported (%u)", m_Format);
			m_FinishedPlayback = true;
			return;
		}
#endif
#if __DEV
		if(!m_WaveRef.HasValidWave())
		{
			static bool haveTraced = false;
			if(!haveTraced)
			{
				audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
				haveTraced = true;
			}
			audAssertf(m_WaveRef.HasValidWave(), "audWavePlayer::StartPhys with invalid wave ref id: %d. State %u, Finished: %s, PlayPosition: %u", audDriver::GetMixer()->GetPcmSourceSlotId(this), m_PlayState, m_FinishedPlayback ? "yes" : "no", m_PlayPositionSamples);
		}
#endif
		if(m_FinishedPlayback ||
			m_PlayState == kStateStopped || 
			(m_PlayPositionSamples >= m_WaveLengthSamples && !IsLooping()))
		{
			// we've already finished - don't bother continuing
			m_FinishedPlayback = true;
			return;
		}

		if(++m_NumPhysicalClients > 1)
		{
#if __DEV
			if(!HasBuffer(0))
			{
				static bool haveTraced = false;
				if(!haveTraced)
				{
					audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
					haveTraced = true;
				}
				audAssertf(HasBuffer(0), "audWavePlayer::StartPhys() - multiple clients but no buffer, id: %d", audDriver::GetMixer()->GetPcmSourceSlotId(this));
			}
#endif
			// If this isn't the first physical client then we don't have to do much if we have already submitted the initial data to the decoders.
			if( m_InitialDataSubmittedToDecoder )
				return;
		}

		if(!HasBuffer(0))
		{
			// Only allocate the buffers once.
			AllocateBuffers(1);
		}

		// We have found a situation (speech + shadow sounds) where an environment sound tells the voice to play physically when the sound hasn't been told to play yet, so no parameters have been set up 
		// on the wave.  To prevent this issue, only submit the initial data to the decoder if the voice has been told to play physically and we haven't done it already.
		if( m_PlayState == kStatePlaying && !m_InitialDataSubmittedToDecoder)
		{
			SumbitDataToDecoder();
			m_InitialDataSubmittedToDecoder = true;
			audAssertf(m_IsDecoded || !m_Decoder, "At this point we should have a decoder");
		}

#if __DEV && 0
		if(m_PlayState != kStatePlaying)
		{
			static bool haveTraced = false;
			if(!haveTraced)
			{
				audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
				haveTraced = true;
			}
			audAssertf(m_PlayState == kStatePlaying, "audWavePlayer::StartPhys when not playing (play state %d), id: %d", m_PlayState, audDriver::GetMixer()->GetPcmSourceSlotId(this));
		}
#endif		
	}

	void audWavePlayer::Stop()
	{
		m_StopRequested = true;
	}

	void audWavePlayer::StopPhys(s32 UNUSED_PARAM(channel))
	{
		Assert(m_WaveRef.HasValidWave());

		if(--m_NumPhysicalClients == 0)
		{
			if(m_Decoder)
			{
				audDriver::GetMixer()->GetDecodeMgr().FreeDecoder(m_Decoder);
				m_Decoder = NULL;
				m_InitialDataSubmittedToDecoder = false;
			}

			FreeBuffers();
		}
	}

	void audWavePlayer::SetParam(const u32 paramId, const f32 val)
	{
		switch(paramId)
		{
		case audPcmSource::Params::FrequencyScalingFactor:
			if(val == 0.f)
			{
				m_IsPauseRequested = true;
			}
			else
			{
				m_IsPauseRequested = false;
				SetFrequencyScalingFactor(val);
			}
			break;
		default:
			Assert(0);
			break;
		}
	}

	void audWavePlayer::SetParam(const u32 paramId, const u32 val)
	{
		switch(paramId)
		{
		case audPcmSource::Params::StartOffsetMs:
			SetPlayPositionMs(val);
			break;
		case audPcmSource::Params::SyncMasterId:
			if(m_SyncMasterId != audMixerSyncManager::InvalidId)
			{
				audMixerSyncManager::Get()->Cancel(m_SyncMasterId);
			}
			Assign(m_SyncMasterId, val);			
			if(m_SyncMasterId != audMixerSyncManager::InvalidId)
			{
				audMixerSyncManager::Get()->AddTriggerRef(val);
			}
			break;
		case audPcmSource::Params::PlayTimeMixerFrame:
			m_PlayTimeMixerFrames = val;
			break;
		case audPcmSource::Params::PlayTimeMixerFrameSubFrameOffset:
			Assign(m_PlayTimeMixerFrameSubFrameOffset, val);
			break;
		case audPcmSource::Params::SyncSlaveId:
			Assign(m_SyncSlaveId, val);
			break;
		case Params::StartOffsetSamples:
			SetPlayPositionSamples(val);
			break;
		case Params::WaveReference:
			Init(val);
			break;
		case Params::DelaySamples:
			SetDelaySamples(val);
			break;
		case Params::SyncTriggerTimeSamples:
			m_SyncTriggerTimeSamples = val;
			break;
		case audPcmSource::Params::PauseGroup:
			if(val == ~0U)
			{
				m_PauseGroup = -1;
			}
			else
			{
				m_PauseGroup = static_cast<s8>(val);
			}
			break;
		default:
			Assert(0);
			break;
		}
	}

	void audWavePlayer::SumbitDataToDecoder()
	{
		if(m_IsDecoded)
		{
			Assert(!m_Decoder);
			m_Decoder = audDriver::GetMixer()->GetDecodeMgr().AllocateDecoder((audWaveFormat::audStreamFormat)m_Format, 1, m_BaseFrequency);
			Assert(m_Decoder);

			if(!m_Decoder)
			{
				audWarningf("Waveplayer failed to allocate a decoder! Voice virtualisation will fail.");
				m_FinishedPlayback = true;
				return;
			}
			
			m_Decoder->SetWaveIdentifier(GetWaveNameHash());

			if(m_IsStreaming)
			{
				// Add first two blocks...

				audStreamingWaveSlot *streamSlot = GetStreamSlot();
				audStreamingWaveBlock blockInfo;		
				streamSlot->GetFirstBlockInfo(m_StreamClientId, blockInfo, m_Format);
				m_HadFinalBlock |= blockInfo.IsFinalBlock;
				s32 bufferRelativeStartOffset = 0;

				// Compensate for start offset
				if(audVerifyf(blockInfo.packetSeekTable, "Invalid seek table (stream slot %s, block %u)", streamSlot->GetSlotName(), blockInfo.blockId))
				{
					audAssertf(m_PlayPositionSamples >= blockInfo.packetSeekTable[0].StartSample, "Invalid play position - %u, %u (block %u)", m_PlayPositionSamples, blockInfo.packetSeekTable[0].StartSample, blockInfo.blockId);
					bufferRelativeStartOffset = m_PlayPositionSamples - blockInfo.packetSeekTable[0].StartSample;					
				}				
				
				Assert((u32)bufferRelativeStartOffset < blockInfo.metadata.NumSamples);
				SubmitBlock(blockInfo, Max(0, bufferRelativeStartOffset));

				if(!m_HadFinalBlock)
				{
					streamSlot->GetSecondBlockInfo(m_StreamClientId, blockInfo, m_Format);
					m_HadFinalBlock |= blockInfo.IsFinalBlock;
					SubmitBlock(blockInfo);
				}
			}
			else
			{
				audPacket packet;
				packet.inputData = (u8*)m_WaveDataPtr;
				packet.inputBytes = m_WaveLengthBytes;

				packet.PlayBegin = m_PlayPositionSamples;
				// Check for 'preserveTransient' flag
				// Note: we ignore preserveTransient for looping waves
				if(m_WaveFormat->Format == audWaveFormat::kMP3)
				{
					packet.LoopEnd = 0;
					if(m_WaveFormat->LoopEnd != 0 && m_WaveFormat->LoopPointSamples == -1)
					{
						ASSERT_ONLY(u32 seekTableSizeBytes = 0);
						ASSERT_ONLY(m_WaveRef.FindSeek(seekTableSizeBytes));
						audAssertf(seekTableSizeBytes/sizeof(u16) > 1, "MP3 marked as 'preserveTransient' only 1 frame long (%u bytes)", m_WaveLengthBytes);
						packet.SkipFirstFrame = true;
					}
					
					packet.IsFinalBlock = true; // we're not streaming so there is only one block
					
					u32 sizeBytes = 0;
					u16 *seekTable = (u16*)m_WaveRef.FindSeek(sizeBytes);

					audAssertf(m_WaveFormat->Format != audWaveFormat::kMP3 || seekTable, "Non streaming MP3 wave with no seek table; waveNameHash: %u", m_WaveRef.GetWaveNameHash());

					packet.SeekTable = seekTable;
					packet.SeekTableSize = sizeBytes;
					packet.PlayEnd = m_WaveFormat->LengthSamples;
					packet.LoopBegin = (u32)m_WaveFormat->LoopPointSamples;
				}
				else if(m_Format == audWaveFormat::kXMA2)
				{
					packet.LoopBegin = m_WaveFormat->LoopBegin<<7;
					packet.LoopEnd = m_WaveFormat->LoopEnd == 0 ? 0 : (m_WaveFormat->LoopEnd<<7) - 1;
					packet.PlayBegin = (m_WaveFormat->PlayBegin<<7) + m_PlayPositionSamples;
					packet.PlayEnd = m_WaveFormat->PlayEnd == 0 ? 0 : (m_WaveFormat->PlayEnd<<7);
					packet.IsFinalBlock = true; // we're not streaming so there is only one block
					
					if(packet.LoopEnd == 0)
					{
						packet.PlayBegin += 384;
						packet.PlayEnd += 384;
					}

					audAssertf(m_WaveFormat->PlayBegin == 0, "PlayBegin: %u (%s/%u)", m_WaveFormat->PlayBegin<<7, GetBankName(), GetWaveNameHash());
					audAssertf(packet.PlayEnd <= m_WaveFormat->LengthSamples, "PlayEnd: %u, LengthSamples: %u (%s/%u)", packet.PlayEnd, m_WaveFormat->LengthSamples, GetBankName(), GetWaveNameHash());
				}
				else
				{
					if(m_Format == audWaveFormat::kAdpcm)
					{
						packet.IsFinalBlock = true;
					}
					packet.PlayBegin = m_PlayPositionSamples;
					packet.PlayEnd = m_WaveFormat->LengthSamples;
					packet.LoopBegin = (u32)m_WaveFormat->LoopPointSamples;
					packet.LoopEnd = m_WaveLengthSamples;
				}

				packet.WaveSlotId = m_WaveRef.GetSlotId();
#if __BANK
				packet.WaveNameHash = GetWaveNameHash();
#endif
				m_Decoder->SubmitPacket(packet);	
			}				

			m_DecodeState = DECODE_SUBMITTED;
		}
	}

	void audWavePlayer::SetDelaySamples(const u32 delaySamples)
	{
		audAssert(m_PlayState == kStateIdle);
		if(delaySamples > 0)
		{
			CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);
			const s32 sampleShift = kMixBufNumSamples == 256 ? 8 : 7;
			m_PredelayFrames = delaySamples >> sampleShift; // (delaySamples / kMixBufNumSamples)
			m_PredelaySamples = delaySamples - (m_PredelayFrames << sampleShift); // delaySamples - (m_PredelayFrames*kMixBufNumSamples)
		}
		else
		{
			m_PredelaySamples = 0;
			m_PredelayFrames = 0;
		}
		// XMA hardware can take a frame to produce data, so add 1 frame predelay to everything to compensate
#if RSG_DURANGO || __XENON
		m_PredelayFrames++;
#endif
	}

	void audWavePlayer::SetPlayPositionMs(const u32 startOffsetMs)
	{
		if(startOffsetMs > 0)
		{
			u32 startOffsetSamples = audDriverUtil::ConvertMsToSamples(startOffsetMs, m_BaseFrequency);
			SetPlayPositionSamples(startOffsetSamples);
		}
	}
	
#if __PS3
	void audWavePlayer::ComputeMP3PacketOffsetFromSamples(u32 sampleOffset, u32 &frameOffsetBytes, u32 &subFrameSampleOffset)
	{
		u32 sizeBytes = 0;
		u16 *seekTable = (u16*)m_WaveRef.FindSeek(sizeBytes);
		
		audAssertf(seekTable, "ComputeMP3PacketOffsetFromSamples called on asset with no seek table; waveNameHash: %u", m_WaveRef.GetWaveNameHash());
		audMp3Util::ComputeFrameOffsetFromSamples(seekTable, sizeBytes, sampleOffset, frameOffsetBytes, subFrameSampleOffset);
	}
#endif

	void audWavePlayer::SetFrequencyScalingFactor(f32 frequencyScalingFactor)
	{
		u32 baseFrequency = m_BaseFrequency;

		u32 frequency = (u32)(baseFrequency*frequencyScalingFactor);
		// clamp frequency to allowed limits
		frequency = Clamp<u32>(frequency, kMinSampleRate, kMaxSampleRate);

		s32 offset = (s32)frequency - (s32)m_BaseFrequency;
		Assign(m_PlaybackFrequencyOffset, offset);
	}

	s32 audWavePlayer::GetLengthSamples() const
	{
		// m_LengthSamples is at the original, asset sample rate
		return static_cast<s32>(m_WaveLengthSamples);
	}

	const char *audWavePlayer::GetBankName() const
	{		
		if(m_WaveRef.HasValidSlot())
		{
			audWaveSlot *slot = audWaveSlot::GetWaveSlotFromIndex(m_WaveRef.GetSlotId());
			return slot->GetLoadedBankName();
		}
		return "Invalid";
	}

	u32 audWavePlayer::GetWaveNameHash() const
	{
		if(m_WaveRef.HasValidWave())
		{
			return m_WaveRef.GetWaveNameHash(); 
		}
		return 0;
	}

	u32 audWavePlayer::GetWaveSlotId() const
	{
		return static_cast<u32>(m_WaveRef.GetSlotId());
	}

	void audWavePlayer::BeginFrame()
	{
		AUD_PCMSOURCE_TIMER_BEGINFRAME;

		Assert(HasBuffer(0));
		Assert(m_WaveRef.HasValidWave());

		if(m_HasBegunFrame)
		{
			return;
		}
		m_HasBegunFrame = true;
		m_HasEndedFrame = false;

		if(m_IsDecoded && m_Decoder)
		{
			if(m_DecodeState == DECODE_FINISHED)
			{
				audDriver::GetMixer()->GetDecodeMgr().FreeDecoder(m_Decoder);
				m_Decoder = NULL;
			}
			else
			{
				m_Decoder->PreUpdate();
			}
			
			if(m_IsStreaming)
			{
				const bool readyForMoreData = m_Decoder->QueryReadyForMoreData();
				if(readyForMoreData)
				{
					if(GetStreamSlot()->RequestDataForClient(m_StreamClientId))
					{
						PF_SET(StreamingDelayFrames, m_FramesWaitingForStreaming);
						m_FramesWaitingForStreaming = 0;

						// Data is available; send it to the decoder
						audPacket packet;
						audStreamingWaveBlock blockInfo;
						GetStreamSlot()->GetCurrentBlockInfo(m_StreamClientId, blockInfo, m_Format);
						m_HadFinalBlock |= blockInfo.IsFinalBlock;
						SubmitBlock(blockInfo);
					}
					else
					{
						m_FramesWaitingForStreaming++;
					}
				}
			}
		}
	}

	BANK_ONLY(extern bool g_UsePeakData);
	void audWavePlayer::EndFrame()
	{
		AUD_PCMSOURCE_TIMER_ENDFRAME;

		if(m_HasEndedFrame)
		{
			return;
		}

		m_HasEndedFrame = true;
		m_HasBegunFrame = false;

		m_IsPaused = m_IsPauseRequested;

		// Safety check: if the decoder takes a long time to return data we give up waiting.
		// Note that this happens when the game is sitting on an assert message box, since our GPU
		// fetch requests don't get serviced.
		if(m_DecodeState == DECODE_WAITING)
		{
			NOTFINAL_ONLY(if(!fiIsShowingMessageBox))
			{
				m_FramesWaiting++;
			}
			if(m_FramesWaiting > 1000)
			{
				m_FinishedPlayback = true;
				audWarningf("Decode waiting for > 1000 frames on wave %u, bank %s, mixer time %u", GetWaveNameHash(), GetBankName(), audDriver::GetFrameCount());
			}
		}
		else
		{
			m_FramesWaiting = 0;
		}

		if(m_IsStarved)
		{
			m_FramesStarved++;
			if(m_FinishedPlayback)
			{
				audErrorf("Wave %s / %u finished while starved for %u frames (waiting for streaming: %u frames)", GetBankName(), GetWaveNameHash(), m_FramesStarved, m_FramesWaitingForStreaming);
			}
			else if(m_FramesStarved > 2000)
			{
				audErrorf("Starved for more than 2000 frames on wave %u, bank %s, mixer time %u", GetWaveNameHash(), GetBankName(), audDriver::GetFrameCount());
				m_FinishedPlayback = true;
			}
			m_IsStarved = false;
		}
		else
		{
			m_FramesStarved = 0;
		}

		if(m_DecodeState == DECODE_ERROR)
		{
			audWarningf("Decode error on wave %u, bank %s", GetWaveNameHash(), GetBankName());
		}
		if(m_StopRequested && m_PlayState == kStatePlaying)
		{
			m_PlayState = kStateStopped;
		}

		if(m_PlayState == kStateStopped)
		{
			m_FinishedPlayback = true;
		}
	}

#else // __SPU
	void audWavePlayer::FetchPCMSamples(s16 *dest, const s16 *src, const s32 numSamples, const s32 pcmDataTag)
	{
		// grab from the nearest (previous) aligned boundary
		const s16 *alignedSrc = src;
		if((((size_t)src) & 15) != 0)
		{
			alignedSrc = (s16*)((((size_t)src+15)&~15) - 16);
		}
		const s32 extraBytesReqd = (src - alignedSrc) * sizeof(s16);
		const s32 requestedBytes = numSamples * sizeof(s16);
		const s32 actualBytes = (((requestedBytes + extraBytesReqd)+15) & ~15);
		Assert(actualBytes <= (s32)g_ScratchLoadBufferSize);
		
		// the requested destination might not be aligned
		const bool isDestAligned = ((((size_t)dest) & 15) == 0);
		const bool needToUseScratchBuffer = (requestedBytes != actualBytes) || (isDestAligned == false);
		s16 *dmaDest = needToUseScratchBuffer ? g_ScratchLoadBuffer : dest;

		//audDisplayf("FetchPCMSamples: %p, %p, %d - alignedSrc: %p, dest: %p extraBytes: %d, actualBytes: %d", 
			//dest, src, requestedBytes, 
			//alignedSrc, dmaDest, extraBytesReqd, actualBytes);

		AlignedAssert(alignedSrc, 16);
		AlignedAssert(dmaDest, 16);
		AlignedAssert(actualBytes, 16);

		sysDmaGet(dmaDest, (std::uint64_t)alignedSrc, actualBytes, pcmDataTag);
		if(needToUseScratchBuffer)
		{
			sysDmaWaitTagStatusAll(1<<pcmDataTag);
			// copy into final destination
			sysMemCpy(dest, ((u8*)g_ScratchLoadBuffer) + extraBytesReqd, requestedBytes);
		}
	}
#endif // __SPU

	void audWavePlayer::ComputeCurrentPeak()
	{
		const u32 sampleIndex = SamplesToPeakIndex(m_PlayPositionSamples);
		if(m_PeakData && sampleIndex > 0)
		{
			// The peak data table stores samples 1,...,n so we must subtract 1 here (sample 0 is m_FirstPeakLevelSample)
			const u32 peakSampleIndex = Min(sampleIndex - 1, m_NumPeakSamples - 1);
#if __SPU
			const u32 peakSample = sysDmaGetUInt16((uint64_t)(m_PeakData+peakSampleIndex), 5);
#else
			const u32 peakSample = m_PeakData[peakSampleIndex];
#endif
			Assign(m_CurrentPeakLevelSample,  peakSample);
		}
		else
		{
			Assign(m_CurrentPeakLevelSample, m_FirstPeakLevelSample);
		}
	}

	u32 audWavePlayer::GetPlayPositionSamples() const
	{
		return m_PlayPositionSamples;
	}

	f32 audWavePlayer::GetHeadroom() const
	{ 	
		return m_WaveHeadroom;
	}

	void audWavePlayer::SetPlayPositionSamples(const u32 requestedStartOffsetSamples)
	{
		u32 startOffsetSamples = requestedStartOffsetSamples;
		if(startOffsetSamples > m_WaveLengthSamples)
		{
			if(IsLooping())
			{
				startOffsetSamples -= m_LoopStartOffsetSamples;
				while(startOffsetSamples > m_WaveLengthSamples)
				{
					startOffsetSamples -= (m_WaveLengthSamples-m_LoopStartOffsetSamples);
				}
			}
			else
			{
				startOffsetSamples = m_WaveLengthSamples;
#if __SPU
				// TODO: Investigate and fix
				//audWarningf("Start offset of %u samples is beyond end of wave %u", requestedStartOffsetSamples, m_WaveRef.GetObjectId());
#else
				//audWarningf("Start offset of %u samples is beyond end of wave %u", requestedStartOffsetSamples, m_WaveRef.GetWaveNameHash());
#endif
			}			
		}

		m_PlayPositionSamples = startOffsetSamples;

		// Update our cached peak level sample
		ComputeCurrentPeak();
	}

	BANK_ONLY(extern bool g_DisableSampleRateConversion);

	bool audWavePlayer::ProcessSyncSignal(const audMixerSyncSignal &signal)
	{		
		if(signal.WasCanceled())
		{
			// if we're in 'self sync' mode don't cancel the ID we're mastering
			if(m_SyncMasterId != audMixerSyncManager::InvalidId && !IsWaitingOnSelfSync())
			{
				audMixerSyncManager::Get()->Cancel(m_SyncMasterId);
				m_SyncMasterId = audMixerSyncManager::InvalidId;
			}
			m_SyncSlaveId = audMixerSyncManager::InvalidId;
			m_FinishedPlayback = true;

			return false;
		}
		if(!signal.HasTriggered())
		{
			// Still waiting on sync source
			return false;
		}
		// Grab subframe predelay from sync source
		u32 subframeOffset = signal.GetSubframeOffset();

		// Invalidate our sync id; it will be recycled after this trigger frame
		m_SyncSlaveId = audMixerSyncManager::InvalidId;
		
		u32 newPredelaySamples = m_PredelaySamples + subframeOffset;
		while(newPredelaySamples >= kMixBufNumSamples)
		{
			m_PredelayFrames++;
			newPredelaySamples -= kMixBufNumSamples;
		}

		m_PredelaySamples = newPredelaySamples;
		// Check for overflow
		Assert(m_PredelaySamples == newPredelaySamples);		
		m_PredelaySamples = subframeOffset;

		return true;
	}

	bool audWavePlayer::EvaluatePredelay()
	{
		if(m_PredelayFrames > 0)
		{
			m_PredelayFrames--;
			return false;
		}
		return true;
	}

	void audWavePlayer::SkipFrame()
	{
		AUD_PCMSOURCE_TIMER_SKIPFRAME;

		if(m_PlayState == kStateIdle || !m_WaveRef.HasValidWave())
		{
			return;
		}
		else if(m_FinishedPlayback || m_PlayState == kStateStopped)
		{
			m_FinishedPlayback = true;
			return;
		}

		if(m_IsStreaming)
		{
			// We don't support virtualised streams currently, and therefore if we fail to allocate physical voices for all channels
			// of a stream at the same time, don't signal our sync source to ensure all voices wait.
			return;
		}

		if(IsWaitingOnSelfSync())
		{
			//audDisplayf("%u SKIP SelfSync: %s trigger %d", audDriver::GetMixer()->GetMixerTimeFrames(), audWaveSlot::GetWaveSlotFromIndex(GetWaveSlotId())->GetLoadedBankName(), m_SyncMasterId);

			audMixerSyncManager::Get()->Trigger(m_SyncMasterId, 0);
			m_SyncMasterId = audMixerSyncManager::InvalidId;
		}

		u32 additionalPredelaySamples = 0u;

		if (m_PlayTimeMixerFrames != ~0u)
		{
			if (audDriver::GetMixer()->GetMixerTimeFrames() == m_PlayTimeMixerFrames)
			{
				additionalPredelaySamples = m_PlayTimeMixerFrameSubFrameOffset;
			}			
			else if (audDriver::GetMixer()->GetMixerTimeFrames() < m_PlayTimeMixerFrames)
			{
				return;
			}			
		}

		if(IsWaitingOnSync())
		{
			return;
		}

		if(!EvaluatePredelay())
		{
			return;
		}

		if((m_IsPaused && m_IsPauseRequested) || (m_PauseGroup != -1 && audDriver::GetMixer()->IsPaused(m_PauseGroup)))
		{
			// Don't move forward when paused
			return;
		}

		const u32 playbackFreq = GetPlaybackFrequency();

		// Store the current peak index so that we can see if we need to re-sample
		const u32 currentPeakSampleIndex = SamplesToPeakIndex(m_PlayPositionSamples);

		const bool isRateConverted = (playbackFreq != kMixerNativeSampleRate) BANK_ONLY(&& !g_DisableSampleRateConversion);

		const u32 numSamplesToGenerate = kMixBufNumSamples - (m_PredelaySamples + additionalPredelaySamples);
		m_PredelaySamples = 0;
		// compute number of input samples
		f32 numInputSamplesF32 = 0.f;
		f32 numInputSamplesFloored = 0.f;
		u32 numInputSamples = 0;
		f32 ratio = 1.f;
		if(isRateConverted)
		{
			static const f32 oneOverNativeSampleRate = 1.f / (f32)kMixerNativeSampleRate;
			ratio = (f32)playbackFreq * oneOverNativeSampleRate;

			numInputSamplesF32 = (ratio * (f32)numSamplesToGenerate) + m_SrcTrailingFrac;

			numInputSamplesFloored = Floorf(numInputSamplesF32);
			numInputSamples = (u32)numInputSamplesFloored;

			m_SrcTrailingFrac = numInputSamplesF32 - numInputSamplesFloored;
		}
		else
		{
			numInputSamples = numSamplesToGenerate;
		}

		u32 numSamplesUsed = AddToCurrentPosition(numInputSamples, true);

		if(m_SyncMasterId != audMixerSyncManager::InvalidId)
		{
			if(m_FinishedPlayback)
			{
				const u32 numOutputSamples = isRateConverted ? (u32)(numSamplesUsed / ratio) : numSamplesUsed;
				audMixerSyncManager::Get()->Trigger(m_SyncMasterId, numOutputSamples);
				m_SyncMasterId = audMixerSyncManager::InvalidId;
			}
			else if(m_PlayPositionSamples >= m_SyncTriggerTimeSamples)
			{
				audMixerSyncManager::Get()->Trigger(m_SyncMasterId, m_PlayPositionSamples - m_SyncTriggerTimeSamples);
				m_SyncMasterId = audMixerSyncManager::InvalidId;
			}
		}

		if(currentPeakSampleIndex != SamplesToPeakIndex(m_PlayPositionSamples))
		{
			ComputeCurrentPeak();
		}
	}

	void audWavePlayer::GenerateFrame()
	{
		AUD_PCMSOURCE_TIMER_GENERATEFRAME;

		// src input buf has to be big in case we have big SRC ratios
#if __SPU || __ASSERT
		const size_t sizeOfSampleDataGatherBuffer = sizeof(s16) * (kMixBufNumSamples * kMaxSampleRateRatio + 2);
#endif
#if __SPU
		audAutoScratchBookmark autoScratch;
		s16* sampleDataGatherBuffer = (s16*) AllocateFromScratch( sizeOfSampleDataGatherBuffer, 16, "SampleDataGatherBuffer" );
#else
		s16* sampleDataGatherBuffer = AllocaAligned( s16, kMixBufNumSamples * kMaxSampleRateRatio + 2, 16 );
#endif

		Assert(m_WaveRef.HasValidWave());
		
		// Whatever happens, if we have a buffer we need to silence it
		Assert(HasBuffer(0));
		const u32 bufferId = GetBufferId(0);
		if(HasBuffer(0))
		{
			g_FrameBufferCache->ZeroBuffer(bufferId);
		}

		if(m_PlayState == kStateIdle)
		{
			return;
		}
		else if(m_FinishedPlayback || m_PlayState == kStateStopped)
		{
			m_FinishedPlayback = true;
			return;
		}

		f32 ratio = 1.f;
		u32 numInputSamples, numSamples1, numSamples2, numSamplesCopied = 0;
		u32 currentPlayPositionSamples = 0;
		const s16 *ptr1,*ptr2;

		if((m_IsPaused && m_IsPauseRequested) || (m_PauseGroup != -1 && audDriver::GetMixer()->IsPaused(m_PauseGroup)))
		{
			// Don't do anything when paused
			return;
		}
		Assert(!m_IsDecoded || (m_Decoder || !m_InitialDataSubmittedToDecoder));

		if(m_IsDecoded && !m_Decoder)
		{
			return;
		}

#if __SPU
		// DMA Decoder state into LS
		const s32 decoderTag = 5;
		audDecoderSpu *decoder = (audDecoderSpu*)g_ScratchDecoder;
		if(m_IsDecoded)
		{
			if(!m_Decoder)
			{
				audErrorf("NULL decoder");
				return;
			}
			sysDmaGetAndWait(g_ScratchDecoder, (uint64_t)m_Decoder, sizeof(audDecoderSpu), decoderTag);			
		}
#else
		audDecoder *decoder = m_Decoder;		
#endif

		if(m_IsDecoded)
		{
			decoder->Update();
		}
		
		// Store the current peak index so that we can see if we need to re-sample
		const u32 currentPeakSampleIndex = SamplesToPeakIndex(m_PlayPositionSamples);

		f32 *destBuffer = g_FrameBufferCache->GetBuffer(bufferId);
		
		const u32 playbackFreq = GetPlaybackFrequency();
		bool isRateConverted = (playbackFreq != kMixerNativeSampleRate) BANK_ONLY(&& !g_DisableSampleRateConversion);

		u32 additionalPredelaySamples = 0u;

		if (m_PlayTimeMixerFrames != ~0u && audDriver::GetMixer()->GetMixerTimeFrames() == m_PlayTimeMixerFrames)
		{
			additionalPredelaySamples = m_PlayTimeMixerFrameSubFrameOffset;
		}

		// compute number of input samples
		const u32 numSamplesToGenerate = kMixBufNumSamples - (m_PredelaySamples + additionalPredelaySamples);
		
		f32 numInputSamplesF32 = 0.f;
		f32 numInputSamplesFloored = 0.f;
		u32 numInputSamplesU32 = 0;
		if(isRateConverted)
		{
			static const f32 oneOverNativeSampleRate = 1.f / (f32)kMixerNativeSampleRate;
			ratio = (f32)playbackFreq * oneOverNativeSampleRate;

			numInputSamplesF32 = (ratio * (f32)numSamplesToGenerate) + m_SrcTrailingFrac;
			// one extra sample since we interpolate from [i] to [i+1]
			numInputSamples = (u32)ceilf(numInputSamplesF32) + 1;
			numInputSamplesFloored = Floorf(numInputSamplesF32);
			numInputSamplesU32 = (u32)numInputSamplesFloored;
		}
		else
		{
			numInputSamples = numSamplesToGenerate;
		}

		if(IsWaitingOnSelfSync())
		{
			bool goodToGo = false;
			// Special case: we're set up to trigger the same sync source that we're waiting on.
			// This is used to synchronise multiple voices over start up latency, so check if we
			// are ready to go and if so fire the trigger
			if(m_IsDecoded)
			{
				if(decoder)
				{
					const u32 numDecodedSamplesAvailable = decoder->QueryNumAvailableSamples();
					if(numDecodedSamplesAvailable >= numInputSamples)
					{
						goodToGo = true;
					}
				}
			}
			else
			{
				// PCM waves (not in VRAM) can be played immediately
				goodToGo = true;
			}

			if(goodToGo)
			{
				//audDisplayf("%u SelfSync: %s trigger %d", audDriver::GetMixer()->GetMixerTimeFrames(), audWaveSlot::GetWaveSlotFromIndex(GetWaveSlotId())->GetLoadedBankName(), m_SyncMasterId);
				audMixerSyncManager::Get()->Trigger(m_SyncMasterId, 0);
				m_SyncMasterId = audMixerSyncManager::InvalidId;
			}
		}

		if (m_PlayTimeMixerFrames != ~0u && audDriver::GetMixer()->GetMixerTimeFrames() < m_PlayTimeMixerFrames)
		{
			return;
		}

		if(IsWaitingOnSync() || !EvaluatePredelay())
		{
			return;
		}

		if(m_IsDecoded)
		{
			const u32 numDecodedSamplesAvailable = decoder->QueryNumAvailableSamples();
			if(numDecodedSamplesAvailable >= numInputSamples)
			{
				Assert(numInputSamples*sizeof(s16) <= sizeOfSampleDataGatherBuffer);
				numSamplesCopied = decoder->ReadData(sampleDataGatherBuffer, numInputSamples);

				u32 numSamplesConsumed;
				if(isRateConverted)
				{
					numSamplesConsumed = numInputSamplesU32;
				}
				else
				{
					numSamplesConsumed = numInputSamples;
				}

				// keep tracking the play time
				decoder->AdvanceReadPtr(numSamplesConsumed);

				AddToCurrentPosition(numSamplesConsumed, true);

				Assert(numSamplesCopied == numInputSamples);

				/*if(m_DecodeState == DECODE_WAITING)
				{
					audAssertf(m_DecodeDelay <= 1, "Decode delay: %u samples", m_DecodeDelay*kMixBufNumSamples);
				}*/

				m_DecodeState = DECODE_RECV_DATA;				
			}
			else
			{
				if(m_IsStreaming && !m_HadFinalBlock)
				{
					m_IsStarved = true;
					// Don't go into the finished state; continue waiting for data
				}
				else if(decoder->GetState() == audDecoder::FINISHED)
				{
					// grab any remaining samples
					Assert(numDecodedSamplesAvailable < numInputSamples);
					Assert(numDecodedSamplesAvailable*sizeof(s16) <= sizeOfSampleDataGatherBuffer);
					numSamplesCopied = decoder->ReadData(sampleDataGatherBuffer, numDecodedSamplesAvailable);
					Assert(numSamplesCopied == numDecodedSamplesAvailable);
					// keep tracking the play time
					decoder->AdvanceReadPtr(numDecodedSamplesAvailable);

					AddToCurrentPosition(numDecodedSamplesAvailable, true);
					
					/*
					// TODO: On PS3 m_WaveLengthSamples isn't aligned to 1152 samples as it should be
					if(!m_FinishedPlayback)
					{
						audWarningf("Decoder has finished, internal tracking disagrees: internal play pos %u, length %u", m_PlayPositionSamples, m_WaveLengthSamples);
					}
					*/

					if(m_IsStreaming && (IsLooping() || !m_HadFinalBlock))
					{
						audWarningf("Stream finished prematurely (decoder reporting finished), have been waiting for data for %.2fS (%u mixer frames)", (m_FramesWaitingForStreaming * kMixBufNumSamples) / (float)kMixerNativeSampleRate, m_FramesWaitingForStreaming);
					}
					if(IsLooping())
					{						
#if __SPU
						audWarningf("wave slot id: %u object id: %u", m_WaveRef.GetSlotId(), m_WaveRef.GetObjectId());
#else
						audWarningf("wave: %s / %u", m_WaveRef.GetSlot()->GetLoadedBankName(), m_WaveRef.GetWaveNameHash());
#endif
						// Streaming starvation will generate the warning above; only assert for non-streaming loops.
						//audAssertf(m_IsStreaming || !IsLooping(), "Decoder reported FINISHED state unexpectedly: %u / %u", m_WaveRef.GetSlotId(), m_WaveRef.GetObjectId());
					}
					m_FinishedPlayback = true;
					m_DecodeState = DECODE_FINISHED;
				
				}
				else if(m_DecodeState == DECODE_FINISHED)
				{
					m_FinishedPlayback = true;
				}
				else if(decoder->GetState() == audDecoder::DECODER_ERROR)
				{
#if __SPU
					audWarningf("Decoder error on wave %u / %u", m_WaveRef.GetSlotId(), m_WaveRef.GetObjectId());
#else
					audWarningf("Decoder error on wave %u", m_WaveRef.GetWaveNameHash());
#endif
					m_DecodeState = DECODE_ERROR;
					m_FinishedPlayback = true;
				}
				else
				{
					// silence buffer until data is ready - leave numSamplesCopied as 0
					//audAssertf(m_DecodeState == DECODE_SUBMITTED || m_DecodeState == DECODE_WAITING, "Decode state: %u but no data available", m_DecodeState);
					if(m_DecodeState == DECODE_RECV_DATA && m_IsStreaming)
					{
						//audWarningf("Stream: decode state: %u but no data available (slot %d wave %d).  Wanted %u samples, only %u available", m_DecodeState, m_WaveRef.GetSlotId(), m_WaveRef.GetObjectId(), numInputSamples, numDecodedSamplesAvailable);
#if __SPU
						// Try to preserve sync with other wave players in the event of a VRAM fetch taking too long
						decoder->SkipSamplesInCurrentPacket(numInputSamples - 1);
#endif
					}
					else
					{
						// We're waiting on the decoder producing the first data
						m_DecodeState = DECODE_WAITING;
						m_DecodeDelay++;
					}
				}
			}
		}
		else if(m_WaveDataPtr)
		{
			// data already in PCM format
			// gather data into input buffer, deal with looping etc

			// if this wave is looping then we have infinite samples available, otherwise we need to stop when we reach the end
			if(!IsLooping())
			{
				numInputSamples = Min(m_WaveLengthSamples - m_PlayPositionSamples, numInputSamples);
				if(numInputSamples <= 1)
				{
					// Disable interpolation if we only have one sample left
					isRateConverted = false;
				}
			}

			numSamples1 = numSamples2 = numSamplesCopied = 0;
			currentPlayPositionSamples = GetPlayPositionSamples();

			// Sample gather loop
			while(numSamplesCopied < numInputSamples && IsMoreDataAvailable())
			{
				GetNextSamples(numInputSamples - numSamplesCopied, ptr1, numSamples1, ptr2, numSamples2);

				// if we're not rate converting then every sample is being consumed, so update play position
				AddToCurrentPosition(numSamples1 + numSamples2, !isRateConverted);
								
#if __SPU
				// fetch s16 sample data into local store
				const s32 pcmDataTag = 6;
				Assert((numSamplesCopied+numSamples1)*sizeof(s16) <= sizeOfSampleDataGatherBuffer);
				FetchPCMSamples(sampleDataGatherBuffer + numSamplesCopied, ptr1, numSamples1, pcmDataTag);
				if(numSamples2)
				{
					Assert((numSamplesCopied+numSamples1+numSamples2)*sizeof(s16) <= sizeOfSampleDataGatherBuffer);
					FetchPCMSamples(sampleDataGatherBuffer + numSamplesCopied + numSamples1, ptr2, numSamples2, pcmDataTag);
				}
				
				sysDmaWaitTagStatusAll(1<<pcmDataTag);
#else
				// copy s16 pcm data to aligned buffer so we can use vectorised conversion routines
				Assert((numSamplesCopied+numSamples1)*sizeof(s16) <= sizeOfSampleDataGatherBuffer);
				sysMemCpy(sampleDataGatherBuffer + numSamplesCopied, ptr1, numSamples1 * sizeof(s16));
				if(numSamples2)
				{
					Assert((numSamplesCopied+numSamples1+numSamples2)*sizeof(s16) <= sizeOfSampleDataGatherBuffer);
					sysMemCpy(sampleDataGatherBuffer + numSamplesCopied + numSamples1, ptr2, numSamples2 * sizeof(s16));
				}
#endif

				numSamplesCopied += numSamples1 + numSamples2;
			}
		}

		// pad with silence if we ran out of data
		Assert(numSamplesCopied <= numInputSamples);
		sysMemSet(&sampleDataGatherBuffer[numSamplesCopied], 0, (numInputSamples - numSamplesCopied) * sizeof(s16));

		CompileTimeAssert(g_ScratchLoadBufferSize >= sizeof(float)*kMixBufNumSamples);
#if __SPU
		float *alignedDestBuffer = (m_PredelaySamples + additionalPredelaySamples) != 0 ? (float*)g_ScratchLoadBuffer : destBuffer;
#else
		float *alignedDestBuffer = (m_PredelaySamples + additionalPredelaySamples) != 0 ? (float*)&g_ScratchLoadBuffer[0] : destBuffer;
#endif
		
		u32 numOutputSamples = 0;
		// resample and convert to normalized f32 data
		if(isRateConverted)
		{
			numOutputSamples = audRateConvertorLinear::Process(ratio, alignedDestBuffer, sampleDataGatherBuffer, numInputSamples, numSamplesToGenerate, m_SrcTrailingFrac);
			m_SrcTrailingFrac = numInputSamplesF32 - numInputSamplesFloored;

			if(!m_IsDecoded)
			{
				// restore the previous play position
				SetPlayPositionSamples(currentPlayPositionSamples);
				// add on number of samples actually used by SRC
				AddToCurrentPosition(numInputSamplesU32, true);
			}
		}
		else
		{
			// we dont need to resample so copy/convert straight into the mix buffer
			if (alignedDestBuffer)
			{
				CopyMonoBufferToMonoMixBuf(alignedDestBuffer, sampleDataGatherBuffer, numInputSamples);
			}
			numOutputSamples = numInputSamples;
		}
	
		if(m_PredelaySamples + additionalPredelaySamples != 0)
		{
			Assert(numSamplesToGenerate + m_PredelaySamples + additionalPredelaySamples == kMixBufNumSamples);
			if(destBuffer)
			{
				sysMemSet(destBuffer, 0, (m_PredelaySamples + additionalPredelaySamples) * sizeof(float));
				sysMemCpy(destBuffer + (m_PredelaySamples + additionalPredelaySamples), alignedDestBuffer, numSamplesToGenerate * sizeof(float));
			}
			numOutputSamples += (m_PredelaySamples + additionalPredelaySamples);

			m_PredelaySamples = 0;			
		}

#if __SPU
		if(m_IsDecoded)
		{
			Assert(m_Decoder);
			Assert(decoder);
			// DMA updated decoder state
			sysDmaPutAndWait(g_ScratchDecoder, (uint64_t)m_Decoder, sizeof(audDecoderSpu), decoderTag);
		}
#endif

		if(m_SyncMasterId != audMixerSyncManager::InvalidId)
		{
			if(m_FinishedPlayback)
			{
				audMixerSyncManager::Get()->Trigger(m_SyncMasterId, numOutputSamples);
				m_SyncMasterId = audMixerSyncManager::InvalidId;
			}
			else if(m_PlayPositionSamples >= m_SyncTriggerTimeSamples)
			{
				audMixerSyncManager::Get()->Trigger(m_SyncMasterId, m_PlayPositionSamples - m_SyncTriggerTimeSamples);
				m_SyncMasterId = audMixerSyncManager::InvalidId;
			}
		}

		// Check if we've moved onto the next peak sample boundary and update if we have
		if(currentPeakSampleIndex != SamplesToPeakIndex(m_PlayPositionSamples))
		{
			ComputeCurrentPeak();
		}

#if AUD_WAVEPLAYER_METERING
		// Real time metering of physical voices:
		m_PeakLevel = ComputePeakLevel<kMixBufNumSamples>(destBuffer);
#endif

		if(destBuffer && (m_IsPaused && !m_IsPauseRequested))
		{
			// apply a one-frame fade-in
			FadeInBuffer<kMixBufNumSamples>(destBuffer);
		}
		else if(destBuffer && (!m_IsPaused && m_IsPauseRequested))
		{
			// apply a one-frame fade-out
			FadeOutBuffer<kMixBufNumSamples>(destBuffer);
		}

	}

	void audWavePlayer::GetNextSamples(u32 numSamplesRequested, const s16 *&ptr1, u32 &sampleCount1, const s16 *&ptr2, u32 &sampleCount2)
	{
		// number of samples from current pos in buffer
		Assert(m_WaveLengthSamples >= m_PlayPositionSamples);
		const u32 numSamplesRemaining = m_WaveLengthSamples - m_PlayPositionSamples;

		// ptr1 is always m_PlayPositionSamples into the data buffer
		ptr1 = m_WaveDataPtr + m_PlayPositionSamples;

		if(numSamplesRemaining >= numSamplesRequested)
		{
			// there is enough data in the buffer to satisfy this request- no wrapping required
			ptr2 = NULL;
			sampleCount2 = 0;
			// returning the number of samples requested
			sampleCount1 = numSamplesRequested;
		}
		else
		{
			if(IsLooping())
			{
				// only wrap if looping buffer
				ptr2 = m_WaveDataPtr + m_LoopStartOffsetSamples;
				// calculate how many samples in ptr2
				// either as many samples as have been requested, or as many samples as are available between the loop point and the end
				sampleCount2 = Min(numSamplesRequested - numSamplesRemaining, m_WaveLengthSamples - m_LoopStartOffsetSamples);
			}

			sampleCount1 = numSamplesRemaining;
		}
	}

	u32 audWavePlayer::AddToCurrentPosition(u32 numSamples, bool updateState)
	{
		m_PlayPositionSamples += numSamples;
		Assert(m_WaveRef.HasValidWave());
		if(m_PlayPositionSamples >= m_WaveLengthSamples)
		{
			if(m_IsLooping)
			{
				if(audVerifyf(!m_IsLooping || m_WaveLengthSamples > static_cast<u32>(m_LoopStartOffsetSamples), "Invalid loop start point %d, wave length is %u (%s: %s / %u)", m_LoopStartOffsetSamples, m_WaveLengthSamples, audWaveSlot::GetWaveSlotFromIndex(GetWaveSlotId())->GetSlotName(), audWaveSlot::GetWaveSlotFromIndex(GetWaveSlotId())->GetLoadedBankName(), GetWaveNameHash()))
				{
					while(m_PlayPositionSamples >= m_WaveLengthSamples)
					{
						m_PlayPositionSamples -= (m_WaveLengthSamples - m_LoopStartOffsetSamples);
					}
				}
				else
				{
					if(updateState)
					{
						m_FinishedPlayback = true;
					}
				}
				
				return numSamples;
			}
			else
			{
				if(updateState)
				{
					m_FinishedPlayback = true;
				}
				// Clamp numSamples to wave length
				return numSamples - (m_PlayPositionSamples - m_WaveLengthSamples);
			}
		}
		else
		{
			return numSamples;
		}
	}

#if !__FINAL && !__SPU
	void audWavePlayer::GetAssetInfo(audPcmSource::AssetInfo &assetInfo) const
	{
		if(m_WaveRef.HasValidWave())
		{
			assetInfo.assetNameHash = m_WaveRef.GetWaveNameHash();
			assetInfo.slotId = static_cast<u32>(m_WaveRef.GetSlotId());
		}
	}
#endif // !__FINAL && !__SPU
}
