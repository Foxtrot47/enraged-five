//
// audiohardware/decodemgr.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "decodemgr.h"
#include "driver.h"
#include "audioengine/engineutil.h"
#include "system/interlocked.h"

#include "decoder_adpcm.h"
#include "decoder_pcm.h"
#include "decoder_spu.h"
#include "decoder_xma.h"
//#include "decoder_atrac9.h"
#include "decoder_PS4mp3.h"
#include "decoder_xbonexma.h"

namespace rage
{
	u32 audDecodeManager::sm_NumStreamsAllocated = 0;
	sysMemFixedAllocator *audDecodeManager::sm_Allocator = NULL;
	u8 *audDecodeManager::sm_HeapMem = NULL;
	atArray<audDecoder*> audDecodeManager::sm_DecodersToFree;

	audDecodeManager::audDecodeManager()
	{

	}

	audDecodeManager::~audDecodeManager()
	{

	}
	template<int _A>size_t Align(size_t v)
	{
		return (v+_A-1)&(~(_A-1));
	}
	bool audDecodeManager::Init()
	{

#if __PS3
		const size_t platformDecoderSize = sizeof(audDecoderSpu);
#elif __XENON
		const size_t platformDecoderSize = sizeof(audDecoderXma);
#elif RSG_AUDIO_X64
		size_t platformDecoderSize = sizeof(audDecoderPcm);
		const size_t adpcmDecoderSize = sizeof(audDecoderAdpcm);
		if(adpcmDecoderSize > platformDecoderSize)
		{
			platformDecoderSize = adpcmDecoderSize;
		}
#if RSG_DURANGO
		const size_t xboneXmaDecoderSize = sizeof(audDecoderXboxOneXma);
		if(xboneXmaDecoderSize > platformDecoderSize)
		{
			platformDecoderSize = xboneXmaDecoderSize;
		}
#endif
#if RSG_ORBIS
		//const size_t atrac9DecoderSize = sizeof(audDecoderAtrac9);
		//if(atrac9DecoderSize > platformDecoderSize)
		//{
		//	platformDecoderSize = atrac9DecoderSize;
		//}
		const size_t mp3DecoderSize = sizeof(audDecoderPS4Mp3);
		if(mp3DecoderSize > platformDecoderSize)
		{
			platformDecoderSize = mp3DecoderSize;
		}
#endif //RSG_ORBIS
#else 
		const size_t platformDecoderSize = 0;
#endif

		const size_t slotSize = Align<16>(Max(sizeof(audDecoderPcm), platformDecoderSize));

		u32 numDecodeStreams = audDriver::GetConfig().GetNumPhysicalVoices() + audDriver::GetConfig().GetNumGranularDecoders();

		sm_HeapMem = rage_aligned_new(16) u8[slotSize * numDecodeStreams];

		sm_Allocator = rage_new sysMemFixedAllocator((void*)sm_HeapMem, slotSize, numDecodeStreams);
		sm_DecodersToFree.Reserve(numDecodeStreams);
		return true;
	}

	void audDecodeManager::Shutdown()
	{
		delete sm_Allocator;
		delete[] sm_HeapMem;
		sm_Allocator = NULL;
	}

	bool audDecodeManager::IsFormatSupported(const audWaveFormat::audStreamFormat format) const
	{
		switch(format)
		{
			// PCM formats are always supported
		case audWaveFormat::kPcm16bitLittleEndian:
		case audWaveFormat::kPcm16bitBigEndian:
		case audWaveFormat::kPcm32FBigEndian:
		case audWaveFormat::kPcm32FLittleEndian:
				return true;
		case audWaveFormat::kAdpcm:
				return true;
#if __XENON || RSG_DURANGO
		case audWaveFormat::kXMA2:
				return true;
#endif
#if __PS3 || RSG_ORBIS
		case audWaveFormat::kMP3:
				return true;
#endif
		case audWaveFormat::kAtrac9: // disabled 
				return false;
			default:
				return false;
		}
	}

	audDecoder *audDecodeManager::AllocateDecoder(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 sampleRate)
	{
		audDecoder *decoder = NULL;

#if __PS3
		decoder = (audDecoderSpu*)sm_Allocator->Allocate(sizeof(audDecoderSpu), 16, 0);

		if(decoder)
		{
			::new(decoder) audDecoderSpu();
		}
#else
		switch(format)
		{	
		case audWaveFormat::kPcm16bitLittleEndian:
		case audWaveFormat::kPcm16bitBigEndian:
		case audWaveFormat::kPcm32FBigEndian:
		case audWaveFormat::kPcm32FLittleEndian:
			decoder = (audDecoderPcm*)sm_Allocator->Allocate(sizeof(audDecoderPcm), 16, 0);
			if(decoder)
			{
				::new(decoder) audDecoderPcm();
			}
			break;
#if RSG_AUDIO_X64
		case audWaveFormat::kAdpcm:
			decoder = (audDecoderPcm*)sm_Allocator->Allocate(sizeof(audDecoderAdpcm), 16, 0);
			if(decoder)
			{
				::new(decoder) audDecoderAdpcm();
			}
			break;
#endif
#if RSG_ORBIS
		//case audWaveFormat::kAtrac9:
		//	decoder = (audDecoderAtrac9*)sm_Allocator->Allocate(sizeof(audDecoderAtrac9), 16, 0);
		//	if(decoder)
		//	{
		//		::new(decoder) audDecoderAtrac9();
		//	}
		//	break;
		case audWaveFormat::kMP3:
			decoder = (audDecoderPS4Mp3*)sm_Allocator->Allocate(sizeof(audDecoderPS4Mp3), 16, 0);
			if(decoder)
			{
				::new(decoder) audDecoderPS4Mp3();
			}
			break;
#endif
#if RSG_DURANGO
		case audWaveFormat::kXMA2:
			decoder = (audDecoderXboxOneXma*)sm_Allocator->Allocate(sizeof(audDecoderXboxOneXma), 16, 0);
			if(decoder)
			{
				::new(decoder) audDecoderXboxOneXma();
			}
			break;
#endif
#if __XENON
		case audWaveFormat::kXMA2:
			decoder = (audDecoderXma*)sm_Allocator->Allocate(sizeof(audDecoderXma), 16, 0);
			if(decoder)
			{
				::new(decoder) audDecoderXma();
			}
			break;
#endif
		default:
			break;
		}
#endif		
		if(decoder)
		{
			if(!decoder->Init(format, numChannels, sampleRate))
			{
				decoder->~audDecoder();
				sm_Allocator->Free(decoder);
				decoder = NULL;
			}
			else
			{
				sysInterlockedIncrement(&sm_NumStreamsAllocated);
			}
		}
		return decoder;
	}

	void audDecodeManager::FreeDecoder(audDecoder *decoder)
	{
		if (decoder)
		{
			sm_DecodersToFree.Push(decoder);
		}
	}

	void audDecodeManager::PostMix()
	{
		for (s32 i = sm_DecodersToFree.GetCount() - 1; i >= 0; i--)
		{
			audDecoder* decoder = sm_DecodersToFree[i];

			if (decoder->IsSafeToDelete())
			{
				decoder->Shutdown();
				decoder->~audDecoder();
				sm_Allocator->Free(decoder);
				sysInterlockedDecrement(&sm_NumStreamsAllocated);
				sm_DecodersToFree.Delete(i);
			}
		}
	}
}// namespace rage
