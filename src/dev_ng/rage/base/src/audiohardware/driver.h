//
// audiohardware/driver.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DRIVER_H
#define AUD_DRIVER_H

#include "audiohardware/channel.h"
#include "audiosynth/synthdefs.h"
#include "driverconfig.h"
#include "driverdefs.h"
#include "voicemgr.h"

#include "bank/bank.h"
#include "system/memory.h"
#include "system/fixedallocator.h"

namespace rage 
{
class audMixerDevice;
class audMixerDevice360;
class audMixerDevicePc;
class audMixerDevicePs3;
class audMidiIn;
class IAudioMixCapturer;
//
// PURPOSE
//	This class provides a platform independent interface for the driver layer initialization/shutdown including
//	wave slots, virtualVoices and physicalVoices
// SEE ALSO
//	audDriverPC, audDriverXenon, audWaveSlot
class audDriver
{
public:
#if RSG_XENON || RSG_DURANGO
	// PURPOSE
	//	Fixed size of XMA context allocations
	enum {kXMAContextSizeBytes = 3456};
#endif
	//
	// PURPOSE
	//	Initializes the audio hardware, wave slot loading and voice manager
	// PARAMS
	//	physicalAllocator	- The memory allocator to use for all physical memory (i.e. wave slots)
	//	virtualAllocator	- The memory allocator to use for all virtual memory
	// RETURNS
	//	True if initialization was successful, false otherwise
	static bool InitClass(sysMemAllocator *physicalAllocator, sysMemAllocator *virtualAllocator, void *externallyAllocatedBankHeap, u32 externallyAllocatedBankHeapSize);

	//
	// PURPOSE
	//	Frees all audio driver resources
	static void ShutdownClass();

	//
	// PURPOSE
	//	Returns the number of output channels the engine is configured for
	// RETURNS
	//	Number of output channels, e.g. 2 for stereo, 6 for 5.1, 8 for 7.1 etc
	static __inline u32 GetNumOutputChannels()
	{
		return m_NumOutputChannels;
	}

	static void SetNumOutputChannels(const u32 numOutputChannels)
	{
		m_NumOutputChannels = numOutputChannels;	
	}

	//
	// PURPOSE
	//	Returns a reference to the driver configuration object for this platform.
	// RETURNS
	//	A reference to the driver configuration object for this platform.
	static audDriverConfig& GetConfig()
	{
		return sm_Config;
	}

	static u32 GetFrameCount()
	{
		return sm_FrameCounter;
	}

	static void IncrementFrameCount() { sm_FrameCounter++; }
	
#if __SPU
	static void SetFrameCount(const u32 count) { sm_FrameCounter = count; }
#endif
	//
	// PURPOSE
	//	Returns true if the game is in control of music playback, false if
	//	it's not (e.g. user music via the HUD on Xbox360.)
	// RETURNS
	//	True if the game is in control of music playback, false if it's not.
	// NOTES
	//	This is currently true on all platforms other than XENON.
	static bool IsGameInControlOfMusicPlayback();

	// PURPOSE
	//	Update the audio hardware from the audio thread
	static void Update(u32 timeInMs);

	static audMixerDevice *GetMixer() { return sm_Mixer; }

#if __BANK
	// PURPOSE
	//	Set up render state for audio debug draw:
	//	- no backface culling, alpha blending enabled, no depth buffer read/writes
	static void SetDebugDrawRenderStates();
	//
	// PURPOSE
	//	Adds any hardware related widgets to the supplied bank.
	// PARAMS
	//	bank	- The bank to which the widgets should be added.
	static void AddWidgets(bkBank &bank);
#endif

	static void* AllocateVirtual(size_t size, size_t align = 16);
	static void FreeVirtual(void *ptr);
	static size_t GetVirtualSize(const void *ptr);
	static void* AllocatePhysical(size_t size, size_t align = 16);
	
	static void FreePhysical(void *ptr);
	static size_t GetPhysicalSize(const void *ptr);

	static audVoiceMgr &GetVoiceManager()
	{
		return m_VoiceManager;
	}

	static audOutputMode GetHardwareOutputMode()
	{
		return sm_HardwareOutputMode;
	}

	static void SetHardwareOutputMode(const audOutputMode outputMode)
	{
		if(outputMode != sm_HardwareOutputMode)
		{
			audDisplayf("Hardware output mode: %u", outputMode);
			sm_HardwareOutputMode = outputMode;
		}
	}

	static audOutputMode GetDownmixOutputMode()
	{
		return sm_DownmixOutputMode;
	}

	static void SetDownmixOutputMode(const audOutputMode outputMode)
	{
		audOutputMode hardwareOutput = Min(sm_HardwareOutputMode, outputMode);
		if(hardwareOutput != sm_DownmixOutputMode)
		{
			audDisplayf("Downmix output mode: %u", hardwareOutput);
			sm_DownmixOutputMode = hardwareOutput;
		}
	}

	static sysMemAllocator* GetPhysicalAllocator()
	{
		return sm_PhysicalAllocator;
	}

	static sysMemAllocator* GetVirtualAllocator()
	{
		return sm_VirtualAllocator;
	}

	static void SetAllocators(sysMemAllocator *physicalAllocator, sysMemAllocator *virtualAllocator)
	{
		sm_PhysicalAllocator = physicalAllocator;
		sm_VirtualAllocator = virtualAllocator;
	}

	static bool InitMixer();

#if __SYNTH_EDITOR
	static audMidiIn *GetMidiInput() { return sm_MidiInputDevice; }
#endif 

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
	static void StartAudioCapture( IAudioMixCapturer& capturer, bool startPaused );
	static void StopAudioCapture();
#endif

private:
	
	static void ShutdownMixer();

	static audVoiceMgr m_VoiceManager;
	static u32 m_NumOutputChannels;
	static audDriverConfig sm_Config;

	static audMixerDevice *sm_Mixer;
	SYNTH_EDITOR_ONLY(static audMidiIn *sm_MidiInputDevice);

	static u32 sm_FrameCounter;
	static sysMemAllocator *sm_PhysicalAllocator, *sm_VirtualAllocator;
	static audOutputMode sm_HardwareOutputMode;
	static audOutputMode sm_DownmixOutputMode;
	
#if __XENON
	static sysMemFixedAllocator *sm_XMAAllocator;
#endif
};

} // namespace rage

#endif // AUD_DRIVER_H
