// 
// audiohardware/dspeffect.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DSPEFFECT_H
#define AUD_DSPEFFECT_H

#include "atl/array.h"
#include "mixer.h"

#if __SPU
#define AUD_DEFINE_DSP_PROCESS_FUNCTION(type) void type::Process(audDspEffectBuffer &buffer, u8 *scratchSpace, const u32 scratchSpaceSize)
#define AUD_DECLARE_DSP_PROCESS_FUNCTION void Process(audDspEffectBuffer &buffer, u8 *scratchSpace, const u32 scratchSpaceSize);
#else
#define AUD_DEFINE_DSP_PROCESS_FUNCTION(type) void type::Process(audDspEffectBuffer &buffer)
#define AUD_DECLARE_DSP_PROCESS_FUNCTION void Process(audDspEffectBuffer &buffer);
#endif

namespace rage
{
	enum audDspEffectId
	{
		AUD_DSPEFFECT_BIQUAD_FILTER = 0,
		AUD_DSPEFFECT_COMPRESSOR,
		AUD_DSPEFFECT_DELAY,
		AUD_DSPEFFECT_DELAY4,
		AUD_DSPEFFECT_VARIABLEDELAY,
		AUD_DSPEFFECT_R360LIMITER,
		AUD_DSPEFFECT_REVERB,
		AUD_DSPEFFECT_REVERB4,
		AUD_DSPEFFECT_WAVESHAPER,
		AUD_DSPEFFECT_SYNTHCORE,
		AUD_DSPEFFECT_REVERB_PROG,
		AUD_DSPEFFECT_EARLY_REFLECTIONS,
		AUD_DSPEFFECT_UNDERWATER,

		AUD_NUM_DSPEFFECTS
	};

	struct audDspEffectBuffer
	{
		atRangeArray<f32 *, 8> ChannelBuffers;
		atRangeArray<u16, 8> ChannelBufferIds;
		u32 NumChannels;
		audMixBufferFormat Format;
	};

#define AUD_TIME_DSP !__SPU

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324)
#endif
	// PURPOSE
	//	DSP effect interface for the software mixer.
	class ALIGNAS(16) audDspEffect
	{
	public:

		audDspEffect(audDspEffectId type) 
			: m_ProcessFormat(Noninterleaved)
			, m_TypeId(type)
#if AUD_SUPPORT_METERING
			, m_InputLevel(0.0f)
			, m_InputMean(0.0f)
#endif // AUD_SUPPORT_METERING
		{

		}

		// need a virtual function on SPU to ensure there is a vtable
		virtual ~audDspEffect() {}

		// PURPOSE
		//	Returns the size of the effect class
		u32 GetSize() const;

		audMixBufferFormat GetProcessFormat() const { return m_ProcessFormat; }

#if __SPU
		// PURPOSE
		//	Process the supplied audio buffer.
		void Process(audDspEffectBuffer &, u8 *scratchSpace, const u32 scratchSpaceSize);
#else
		
		// PURPOSE
		//	Initialise the effect state.
		// PARAMS
		//	numChannels - The number of audio channels that will be passed to this effect to process.
		//	channelMask - A bitmask that defines the channels to be processed
		virtual bool Init(const u32 numChannels, const u32 channelMask) = 0;

		// PURPOSE
		//	Shuts down the effect, should free all resources.
		virtual void Shutdown() = 0;

		// PURPOSE
		//	Process the supplied audio buffer.
		// NOTES
		//	Buffer size is kMixBufNumSamples * numChannels, non-interleaved.
		virtual void Process(audDspEffectBuffer &) = 0;

		// PURPOSE
		//	Sets the specified parameter
		virtual void SetParam(const u32 paramId, const u32 value) = 0;
		virtual void SetParam(const u32 paramId, const f32 value) = 0;
#endif

		audDspEffectId GetTypeId() const { return m_TypeId; }
		
#if AUD_SUPPORT_METERING
		void PreProcess(audDspEffectBuffer &buffer);

		float GetInputLevel() const { return m_InputLevel; }
		float GetInputMean() const { return m_InputMean; }
#endif // AUD_SUPPORT_METERING

	protected:
		
		void SetProcessFormat(const audMixBufferFormat format)
		{
			m_ProcessFormat = format;
		}

#if AUD_TIME_DSP
		static atRangeArray<float, AUD_NUM_DSPEFFECTS> sm_ProcessTime;
#endif // AUD_TIME_DSP

	private:

		audMixBufferFormat  m_ProcessFormat;
		audDspEffectId m_TypeId;

#if AUD_SUPPORT_METERING
		float m_InputLevel;
		float m_InputMean;
#endif // AUD_SUPPORT_METERING


	};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage

#endif // AUD_DSPEFFECT_H
