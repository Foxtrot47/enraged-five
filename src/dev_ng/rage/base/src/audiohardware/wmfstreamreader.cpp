// 
// audiohardware/wmfstreamreader.cpp
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#if __WIN32PC

#include "wmfstreamreader.h"
#include "system/endian.h"

#include <Propvarutil.h>
#include <Mfapi.h>

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if(p) { p->Release(); p = NULL; }
#endif

namespace rage {


bool audWMFStreamReader::sm_IsAvailable = false;

audWMFStreamReader::audWMFStreamReader()
	: m_pReader(NULL)
	, m_OverflowBytes(0)
	, m_EOF(false)
	, m_IsMono(false)
{
	sysMemZeroBytes<sizeof(m_Artist)>(&m_Artist);
	sysMemZeroBytes<sizeof(m_Title)>(&m_Title);
}

audWMFStreamReader::~audWMFStreamReader()
{
	
	SAFE_RELEASE(m_pReader);
}

bool audWMFStreamReader::InitClass()
{
	HRESULT hr = CoInitializeEx( NULL, COINIT_MULTITHREADED );

	//! This thread was already in apartment mode, so just initialize like that
	//! since if COM is already initialized on our thread, there aint much we can do about that at the point we hit here...
	if( hr == RPC_E_CHANGED_MODE )
	{
		hr = CoInitializeEx( NULL, COINIT_APARTMENTTHREADED );
	}

	if(audVerifyf(SUCCEEDED(hr), "audWMFStreamReader::InitClass - Unable to initialize COM - 0x%08lx", hr))
	{
		hr = MFStartup( MF_VERSION );
		if( audVerifyf(SUCCEEDED(hr), "audWMFStreamReader::InitClass - Unable to start up Media Foundation! 0x%08lx", hr))
		{
			audDisplayf("Initialised Windows Media Foundation");
			sm_IsAvailable = true;
			return true;
		}		
	}
	
	return false;
}

HRESULT audWMFStreamReader::ConfigureAudioStream()
{
    IMFMediaType *pUncompressedAudioType = NULL;
    IMFMediaType *pPartialType = NULL;

    // Select the first audio stream, and deselect all other streams.
    HRESULT hr = m_pReader->SetStreamSelection(
        (DWORD)MF_SOURCE_READER_ALL_STREAMS, FALSE);

    if (SUCCEEDED(hr))
    {
        hr = m_pReader->SetStreamSelection(
            (DWORD)MF_SOURCE_READER_FIRST_AUDIO_STREAM, TRUE);
    }

    // Create a partial media type that specifies uncompressed PCM audio.
    hr = MFCreateMediaType(&pPartialType);

    if (SUCCEEDED(hr))
    {
        hr = pPartialType->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio);
    }

    if (SUCCEEDED(hr))
    {
        hr = pPartialType->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_PCM);
    }

    // Set this type on the source reader. The source reader will
    // load the necessary decoder.
    if (SUCCEEDED(hr))
    {
        hr = m_pReader->SetCurrentMediaType(
            (DWORD)MF_SOURCE_READER_FIRST_AUDIO_STREAM,
            NULL, pPartialType);
    }

    // Get the complete uncompressed format.
    if (SUCCEEDED(hr))
    {
        hr = m_pReader->GetCurrentMediaType(
            (DWORD)MF_SOURCE_READER_FIRST_AUDIO_STREAM,
            &pUncompressedAudioType);
    }

    // Ensure the stream is selected.
    if (SUCCEEDED(hr))
    {
        hr = m_pReader->SetStreamSelection(
            (DWORD)MF_SOURCE_READER_FIRST_AUDIO_STREAM,
            TRUE);
    }

    // Return the PCM format to the caller.
    if (SUCCEEDED(hr))
    {
        m_pMediaType = pUncompressedAudioType;
        m_pMediaType->AddRef();
    }

	m_IsMono = GetNumChannels() == 1;

    SAFE_RELEASE(pUncompressedAudioType);
    SAFE_RELEASE(pPartialType);
    return hr;
}

//  Create a media source from a URL.
HRESULT CreateMediaSource(PCWSTR sURL, IMFMediaSource **ppSource)
{
    MF_OBJECT_TYPE ObjectType = MF_OBJECT_INVALID;

    IMFSourceResolver* pSourceResolver = NULL;
    IUnknown* pSource = NULL;

    // Create the source resolver.
    HRESULT hr = MFCreateSourceResolver(&pSourceResolver);
    if (FAILED(hr))
    {
        goto done;
    }

    // Use the source resolver to create the media source.

    // Note: For simplicity this sample uses the synchronous method to create 
    // the media source. However, creating a media source can take a noticeable
    // amount of time, especially for a network source. For a more responsive 
    // UI, use the asynchronous BeginCreateObjectFromURL method.

    hr = pSourceResolver->CreateObjectFromURL(
        sURL,                       // URL of the source.
        MF_RESOLUTION_MEDIASOURCE,  // Create a source object.
        NULL,                       // Optional property store.
        &ObjectType,        // Receives the created object type. 
        &pSource            // Receives a pointer to the media source.
        );
    if (FAILED(hr))
    {
        goto done;
    }

    // Get the IMFMediaSource interface from the media source.
    hr = pSource->QueryInterface(IID_PPV_ARGS(ppSource));

done:
    SAFE_RELEASE(pSourceResolver);
    SAFE_RELEASE(pSource);

    return hr;
}

bool audWMFStreamReader::MediaOpen(const WCHAR* pszFileName, u32 startOffsetMs)
{
	audDebugf1("audWMFStreamReader: opening %S...", pszFileName);
	
	HRESULT hr;

	const bool haveParsedMetadata = ParseFileMetadata(pszFileName);

	IMFMediaSource *mediaSource = NULL;
	hr = CreateMediaSource(pszFileName, &mediaSource);

	/*if(SUCCEEDED(hr = MFCreateSourceReaderFromURL(
	pszFileName,
	NULL,
	&m_pReader
	)))*/
	if(SUCCEEDED(hr))
	{
		hr = MFCreateSourceReaderFromMediaSource(mediaSource, NULL, &m_pReader);
		if(SUCCEEDED(hr)) {
			audDebugf1("Succeeded, configuring audio stream...");
			if(SUCCEEDED(hr = ConfigureAudioStream()) && !m_IsMono)
			{
				if(GetStreamLengthMs() >= 10000)
				{
					audDebugf1("success");

					if(!haveParsedMetadata)
					{
						IMFMetadata *pMetadata = NULL;
						hr = GetMetadata(mediaSource, &pMetadata);
						if(SUCCEEDED(hr))
						{
							PROPVARIANT var;
							if(SUCCEEDED(pMetadata->GetProperty(L"Author", &var)))
							{
								WCHAR *stringPtr = NULL;
								if(SUCCEEDED(PropVariantGetStringElem(var, 0, &stringPtr)))
								{
									int len = (int)wcslen(stringPtr)+1; // expects room for the null
									WideCharToMultiByte(CP_UTF8, 0, stringPtr, len, (LPSTR)&m_Artist, sizeof(m_Artist) - 1, NULL, NULL);
								}
								PropVariantClear(&var);
							}
							if(SUCCEEDED(pMetadata->GetProperty(L"Title", &var)))
							{
								WCHAR *stringPtr = NULL;
								if(SUCCEEDED(PropVariantGetStringElem(var, 0, &stringPtr)))
								{
									int len = (int)wcslen(stringPtr)+1; // expects room for the null
									WideCharToMultiByte(CP_UTF8, 0, stringPtr, len, (LPSTR)&m_Title, sizeof(m_Title) - 1, NULL, NULL);
								}
								PropVariantClear(&var);
							}

							audDebugf1("Artist: %s, Title: %s", m_Artist, m_Title);
						}
						else
						{
							// derive title from filename
							const WCHAR *fileName = wcsrchr(pszFileName, L'\\') + 1;
							const WCHAR *period = wcsrchr(fileName, L'.');
							int len = int(period - fileName)+1; // expects room for the null
							WideCharToMultiByte(CP_UTF8, 0, fileName, len, (LPSTR)&m_Title, sizeof(m_Title) - 1,nullptr,nullptr);
						}
					}

					SetCursor(startOffsetMs);
					return true;
				}
			}
			audWarningf("Failed to configure audio stream %S, HRESULT: %08X", pszFileName, hr);
			return false;
		}

		SAFE_RELEASE(mediaSource);
	}
	audWarningf("Failed to open %S - HRESULT %08X", pszFileName, hr);
	return false;
}


bool audWMFStreamReader::MediaClose() 
{
	SAFE_RELEASE(m_pReader);
	return true;
}

bool audWMFStreamReader::ParseFileMetadata(const WCHAR *fileName)
{
	const size_t fileNameLen = wcslen(fileName);
	const WCHAR *ext = fileName + fileNameLen - 3;

	bool success = false;

	if(!_wcsicmp(ext, L"m4a"))
	{
		
		HANDLE fileHandle = CreateFileW(fileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
		if(fileHandle == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		const char *atomPath[] = {"moov","udta","meta","ilst"};
		const s32 pathLength = sizeof(atomPath) / sizeof(atomPath[0]);
		s32 currentPathIndex = 0;
		bool finished = false;

		while(!finished)
		{
			u32 atomLength = 0;
			char atomName[5] = {0};

			DWORD bytesRead = 0;
			HRESULT hr = ReadFile(fileHandle, &atomLength, sizeof(u32), &bytesRead, NULL);
			if(FAILED(hr) || bytesRead != sizeof(u32) || atomLength < 8)
			{
				audErrorf("Failed to read atom length from %S, hr = %08X (read %u bytes)", fileName, hr, bytesRead);
				break;
			}
			atomLength = sysEndian::BtoN(atomLength);
			const u32 atomNameLength = 4;
			hr = ReadFile(fileHandle, &atomName, atomNameLength, &bytesRead, NULL);
			if(FAILED(hr) || bytesRead != atomNameLength)
			{
				audErrorf("Failed to read atom name from %S, hr = %08X (read %u bytes)", fileName, hr, bytesRead);
				break;
			}

			if(!strcmp(atomPath[currentPathIndex], atomName))
			{
				// found the next level; continue reading
				currentPathIndex++;
				if(currentPathIndex == pathLength)
				{
					finished = true;
				}
				else if(currentPathIndex == 3)
				{
					// Skip meta padding?
					SetFilePointer(fileHandle, 4, NULL, FILE_CURRENT);
				}
			}
			else
			{
				// skip this atom				
				if(INVALID_SET_FILE_POINTER == SetFilePointer(fileHandle, atomLength - 8, NULL, FILE_CURRENT))
				{
					audErrorf("Failed to skip atom %s, %S", atomName, fileName);
					break;
				}
			}
		}

		if(finished)
		{
			// We've found the ilst chunk; now look for the two fields we care about
			bool foundTitle = false;
			bool foundArtist = false;
			while(!(foundTitle && foundArtist))
			{
				u32 atomLength = 0;
				char atomName[5] = {0};

				DWORD bytesRead = 0;
				
				// Cache the parent atom start position
				DWORD currentFilePointer = SetFilePointer(fileHandle, 0, NULL, FILE_CURRENT);

				HRESULT hr = ReadFile(fileHandle, &atomLength, sizeof(u32), &bytesRead, NULL);
				if(FAILED(hr) || bytesRead != sizeof(u32) || atomLength < 8)
				{
					audErrorf("Failed to read atom length from %S, hr = %08X (read %u bytes)", fileName, hr, bytesRead);
					break;
				}
				atomLength = sysEndian::BtoN(atomLength);
				const u32 atomNameLength = 4;
				hr = ReadFile(fileHandle, &atomName, atomNameLength, &bytesRead, NULL);
				if(FAILED(hr) || bytesRead != atomNameLength)
				{
					audErrorf("Failed to read atom name from %S, hr = %08X (read %u bytes)", fileName, hr, bytesRead);
					break;
				}

				if(!foundArtist && !stricmp(atomName, "�art"))
				{
					foundArtist = true;
					if(!ParseDataAtom(fileHandle, atomLength, m_Artist, sizeof(m_Artist)))
					{
						audErrorf("Failed to parse \"�art\" atom, %S", fileName);
						break;
					}
				}
				else if(!foundTitle && !stricmp(atomName, "�nam"))
				{
					foundTitle = true;
					if(!ParseDataAtom(fileHandle, atomLength, m_Title, sizeof(m_Title)))
					{
						audErrorf("Failed to parse \"�nam\" atom, %S", fileName);
						break;
					}
				}
				
				// skip this atom				
				if(INVALID_SET_FILE_POINTER == SetFilePointer(fileHandle, currentFilePointer + atomLength, NULL, FILE_BEGIN))
				{
					audErrorf("Failed to skip atom %s, %S", atomName, fileName);
					break;
				}
				
			}

			success = foundArtist && foundTitle;
		}

		CloseHandle(fileHandle);
	}
	return success;
}

bool audWMFStreamReader::ParseDataAtom(HANDLE fileHandle, const u32 parentAtomSize, char *dstBuffer, const size_t bufSize)
{
	u32 atomLength = 0;
	char atomName[5] = {0};

	DWORD bytesRead = 0;
	HRESULT hr = ReadFile(fileHandle, &atomLength, sizeof(u32), &bytesRead, NULL);
	if(FAILED(hr) || bytesRead != sizeof(u32) || atomLength < 8)
	{		
		return false;
	}
	atomLength = sysEndian::BtoN(atomLength);
	const u32 atomNameLength = 4;
	hr = ReadFile(fileHandle, &atomName, atomNameLength, &bytesRead, NULL);
	if(FAILED(hr) || bytesRead != atomNameLength)
	{		
		return false;
	}

	if(strcmp(atomName, "data"))
	{
		return false;
	}

	if(atomLength >= parentAtomSize)
	{
		return false;
	}

	if(INVALID_SET_FILE_POINTER == SetFilePointer(fileHandle, 8, NULL, FILE_CURRENT))
	{
		return false;
	}

	u32 stringLength = atomLength - 16;

	// Ensure NULL termination
	memset(dstBuffer, 0, bufSize);
	stringLength = Min(stringLength, u32(bufSize) - 1);
	hr = ReadFile(fileHandle, dstBuffer, stringLength, &bytesRead, NULL);
	if(FAILED(hr) || bytesRead != stringLength)
	{		
		return false;
	}

	return true;
}

void audWMFStreamReader::Prepare()
{

}

bool audWMFStreamReader::IsAvailable()
{
	return sm_IsAvailable;
}

	
s32	audWMFStreamReader::FillBuffer(void *sampleBuf, u32 dwBytesToRead)
{
	HRESULT hr = S_OK;
    DWORD cbAudioData = 0;
    DWORD cbBuffer = 0;
    BYTE *pAudioData = NULL;

	s32 bytesWritten = -1;

    IMFSample *pSample = NULL;
    IMFMediaBuffer *pBuffer = NULL;

	if(m_OverflowBytes)
	{
		cbAudioData = Min(dwBytesToRead, m_OverflowBytes);
		sysMemCpy(sampleBuf, m_OverflowBuffer, cbAudioData);

		if(cbAudioData < m_OverflowBytes)
		{
			memmove(m_OverflowBuffer, m_OverflowBuffer + cbAudioData, m_OverflowBytes - cbAudioData);
			m_OverflowBytes -= cbAudioData;
		}
		else
		{
			m_OverflowBytes = 0;
		}
	}

	if(m_EOF)
	{
		return static_cast<s32>(cbAudioData);
	}

    // Get audio samples from the source reader.
    while (true)
    {
        DWORD dwFlags = 0;

        // Read the next sample.
        hr = m_pReader->ReadSample(
            (DWORD)MF_SOURCE_READER_FIRST_AUDIO_STREAM,
            0, NULL, &dwFlags, NULL, &pSample );

        if (FAILED(hr)) { break; }

        if (dwFlags & MF_SOURCE_READERF_CURRENTMEDIATYPECHANGED)
        {
            audErrorf("audWMFStreamReader: Type change");
            break;
        }
        if (dwFlags & MF_SOURCE_READERF_ENDOFSTREAM)
        {
            m_EOF = true;
            break;
        }

        if (pSample == NULL)
        {
            audWarningf("audWMFStreamReader: NULL sample");
            continue;
        }

        // Get a pointer to the audio data in the sample.

        hr = pSample->ConvertToContiguousBuffer(&pBuffer);

        if (FAILED(hr)) { break; }


        hr = pBuffer->Lock(&pAudioData, NULL, &cbBuffer);

        if (FAILED(hr)) { break; }


		DWORD overflowBytes = 0;
        // Make sure not to exceed the specified maximum size.
        if (dwBytesToRead - cbAudioData < cbBuffer)
        {
			overflowBytes = cbBuffer - (dwBytesToRead-cbAudioData);
            cbBuffer = dwBytesToRead - cbAudioData;
        }

        // Copy this data to the output buffer      
		sysMemCpy((BYTE*)sampleBuf + cbAudioData, pAudioData, cbBuffer);

		if(overflowBytes)
		{
			if(overflowBytes > sizeof(m_OverflowBuffer))
			{
				audWarningf("audWMFStreamReader: losing %u overflow bytes", overflowBytes - sizeof(m_OverflowBuffer));
				overflowBytes = sizeof(m_OverflowBuffer);
			}
			
			sysMemCpy(m_OverflowBuffer, pAudioData + cbBuffer, overflowBytes);
			m_OverflowBytes = overflowBytes;
		}
        // Unlock the buffer.
        hr = pBuffer->Unlock();
        pAudioData = NULL;

        if (FAILED(hr)) { break; }

        // Update running total of audio data.
        cbAudioData += cbBuffer;

        if (cbAudioData >= dwBytesToRead)
        {
            break;
        }

        SAFE_RELEASE(pSample);
        SAFE_RELEASE(pBuffer);
    }

    if (SUCCEEDED(hr))
    {
        bytesWritten = static_cast<s32>(cbAudioData);
    }

    if (pAudioData)
    {
        pBuffer->Unlock();
    }

    SAFE_RELEASE(pBuffer);
    SAFE_RELEASE(pSample);

    return bytesWritten;
}

s32	audWMFStreamReader::GetSampleRate()
{
	if(audVerifyf(m_pMediaType, "NULL mediaType"))
	{
		const UINT32 sampleRate = MFGetAttributeUINT32(m_pMediaType, MF_MT_AUDIO_SAMPLES_PER_SECOND, 0);
		return static_cast<s32>(sampleRate);
	}
	return -1;
}

s32	audWMFStreamReader::GetNumChannels() const
{
	if(audVerifyf(m_pMediaType, "NULL mediaType"))
	{
		const UINT32 sampleRate = MFGetAttributeUINT32(m_pMediaType, MF_MT_AUDIO_NUM_CHANNELS, 0);
		return static_cast<s32>(sampleRate);
	}
	return -1;
}

s32	audWMFStreamReader::GetStreamLengthMs()
{
	if(audVerifyf(m_pReader, "NULL m_pReader"))
	{
		LONGLONG duration;
		PROPVARIANT var;
		HRESULT hr = m_pReader->GetPresentationAttribute((DWORD)MF_SOURCE_READER_MEDIASOURCE, MF_PD_DURATION, &var);
		if (SUCCEEDED(hr))
		{
			hr = PropVariantToInt64(var, &duration);
			PropVariantClear(&var);

			// Duration is in 100-nanosecond units
			return static_cast<s32>(duration / 10000);
		}
	}
	return -1;
}

s32	audWMFStreamReader::GetStreamPlayTimeMs()
{
	return -1;
}

void audWMFStreamReader::SetCursor(u32 ms)
{
	if(audVerifyf(m_pReader, "NULL m_pReader"))
	{
		PROPVARIANT var;
		LONGLONG hnsPosition = ms * 10000LL;
		HRESULT hr = InitPropVariantFromInt64(hnsPosition, &var);
		if (SUCCEEDED(hr))
		{
			hr = m_pReader->SetCurrentPosition(GUID_NULL, var);
			PropVariantClear(&var);
		}
	}
}

HRESULT audWMFStreamReader::GetMetadata(IMFMediaSource *pSource, IMFMetadata **ppMetadata)
{
	IMFPresentationDescriptor *pPD = NULL;
    IMFMetadataProvider *pProvider = NULL;

    HRESULT hr = pSource->CreatePresentationDescriptor(&pPD);
    if (FAILED(hr))
    {
        goto done;
    }

    hr = MFGetService(
        pSource, MF_METADATA_PROVIDER_SERVICE, IID_PPV_ARGS(&pProvider));

    if (FAILED(hr))
    {
        goto done;
    }

    hr = pProvider->GetMFMetadata(pPD, 0, 0, ppMetadata);

done:
    SAFE_RELEASE(pPD);
    SAFE_RELEASE(pProvider);
    return hr;
}



} // namespace rage
#endif
