// 
// audiohardware/src_linear.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SRC_LINEAR_H
#define AUD_SRC_LINEAR_H

	
namespace rage {

//
// PURPOSE
//	Implements a linear sample rate converter.
class audRateConvertorLinear
{
public:	
	static u32 Process(const f32 ratio, f32 *output, const s16 *input, const u32 numInputSamples, const u32 numOutputSamples, const f32 trailingFrac);

};

void FractionalResampleBuffer(const unsigned int outputSamples, const float ratio, float trailingFrac, const float *input, float *output);
void FractionalResampleBuffer(const unsigned int outputSamples, const float ratio, float trailingFrac, const s16 *input, float *output);

} // namespace rage

#endif // AUD_SRC_LINEAR_H
