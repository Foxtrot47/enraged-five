// 
// audiohardware/vramhelper.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
//
#ifndef AUD_VRAMHELPER_H
#define AUD_VRAMHELPER_H

#if __PS3

#include "mp3util.h"

#include "atl/array.h"
#include "system/fixedallocator.h"

#include "waveslot.h"

namespace rage
{

// PURPOSE
//	Managed the stream of data stored in VRAM, requesting fetches from the GPU as required by the decoder
class audDecoderVramHelper
{
public:

	static void InitClass();
	static void ShutdownClass();


	audDecoderVramHelper()
		: m_FetchBuffer(NULL)
		, m_GpuFetchComplete(NULL)
		, m_BufferReadIndex(0)
		, m_BufferWriteIndex(0)
		, m_SubmitPacketIndex(0)
		, m_FetchPacketIndex(0)
		, m_NumFetchBuffers(0)
		, m_NumPacketsQueued(0)
		, m_FetchRequestSize(0)
		, m_PeekDword(0)
		, m_IsWaitingOnGpu(false)
		, m_IsInitialised(false)
		, m_IsDataMP3(true)
		, m_HasBufferedEntireWave(false)
	{

	}

	~audDecoderVramHelper();

	void Init(const s32 waveSlotId, const bool isDataMP3, u32 numFetchBuffers = kNumFetchBuffers);
	void Reset();

	void SubmitPacket(const u8 *inputData, const u32 startOffsetBytes, const u32 inputDataSize, const u32 loopPointBytes);

	u32 ReadData(u8 *dest, const u32 sizeBytes) const;
	u32 PeekDword() const { Assert(m_IsPeekDwordValid); return m_PeekDword; }
	void AdvanceReadPtr(const u32 numBytes);

	void Update();

	bool QueryReadyForMoreData() const { return m_NumPacketsQueued < static_cast<u32>(m_Packets.size()); }
	u32 QueryNumBytesAvailable() const;

	void DebugPrint() const;

	enum { kNumFetchBuffers = 4 };
	enum { kFetchBufferSize = 3072 };
	enum { kMaxVramStreams = 116 };

	inline u8* GetFetchBufferPtr() const { return m_FetchBuffer; }
	inline bool IsInitialised() const { return m_IsInitialised; }
	inline bool IsPeekDwordValid() const { return m_IsPeekDwordValid; }
	inline bool HasBufferedEntireWave() const { return m_HasBufferedEntireWave; }

	static void ProcessFreeList();

private:

	u32 GetBytesAvailable(const u32 bufferIndex) const
	{
		return m_BufferSize[bufferIndex] - m_BytesConsumed[bufferIndex];
	}
	
	u8 *m_FetchBuffer;
	u32 *m_GpuFetchComplete;

	void ScheduleFetch(const u32 destIndex);
	void Free();

	static void DeferredFree(u32 *fetchCompleteBuffer, u8 *fetchBuffer, const s32 slotId);

	struct audDecoderPacket
	{
		audDecoderPacket()
			: InputBuffer(NULL)
			, InputBufferSize(0)
			, InputBufferBytesRead(0)
			, LoopPointBytes(~0U)
		{

		}
		const u8 *InputBuffer;
		u32 InputBufferSize;
		u32 InputBufferBytesRead;
		u32 LoopPointBytes;
	};

	atRangeArray<u32, kNumFetchBuffers> m_BufferSize;
	atRangeArray<u32, kNumFetchBuffers> m_BytesConsumed;
	u32 m_BufferReadIndex;
	u32 m_BufferWriteIndex;
	atRangeArray<audDecoderPacket, 2> m_Packets;
	u32 m_SubmitPacketIndex;
	u32 m_FetchPacketIndex;
	u32 m_NumPacketsQueued;

	u32 m_NumFetchBuffers;
	u32 m_FetchRequestSize;
	u32 m_PeekDword;

	u32 m_RequestTimeFrames;
	u32 m_RequestTimeMs;

	u32 m_WaveSlotId;
	
	bool m_IsWaitingOnGpu : 1;
	bool m_IsInitialised : 1;
	bool m_IsDataMP3 : 1;
	bool m_HasBufferedEntireWave : 1;
	bool m_IsPeekDwordValid : 1;

	static u8 *sm_FixedHeap;
	static sysMemFixedAllocator *sm_FetchBufferAllocator;
	static sysMemFixedAllocator *sm_FetchCompleteAllocator;

	struct audVramBufferPair
	{
		u32 *fetchCompleteBuffer;
		u8 *fetchBuffer;
		s32 waveSlotId;
	};
	static atFixedArray<audVramBufferPair, kMaxVramStreams> sm_DeferredFreeList;


};

} // namespace rage
#endif // __PS3
#endif // AUD_VRAMHELPER_H
