// 
// audiohardware/submixcache.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "mixer.h"
#include "submix.h"
#include "submixcache.h"
#include "system/dma.h"


namespace rage 
{
	audSubmixCache *g_SubmixCache = NULL;	
} // namespace rage
