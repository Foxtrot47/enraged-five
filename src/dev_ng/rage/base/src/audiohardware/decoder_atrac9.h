//
// audiohardware/decoder_atrac9.h
//
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DECODER_ATRAC9_H
#define AUD_DECODER_ATRAC9_H

#include "driverdefs.h"

#if RSG_ORBIS

#include "decoder.h"
#include "ringbuffer.h"
#include <audiodec.h>

#define DUMP_RAW_ATRAC_STREAMS 1

namespace rage
{

enum {kAtrac9BlockBytesToDecode = 256};
enum {kAtrac9DecompressedBytesPerBlock = kAtrac9BlockBytesToDecode * 8};
enum {kAtrac9DecompressedSamplesPerBlock = kAtrac9DecompressedBytesPerBlock / 2};

enum {kAtrac9Compressionratio = 8};
enum {kAtrac9SuperFrameBlocks = 4};
enum {kAtrac9BlockSamples = 256};
enum {kAtrac9DelaySamples = 256};
enum {kAtrac9BlockSize = 2048};


		struct audAtrac9Packet
		{
			audAtrac9Packet() :
				NumBlocksAvailable(0),
				InputBytes(0),
				InputData(NULL),
				NumSamplesConsumed(0),
				TotalBytes(0),
				PlayBegin(0),
				LastPacketSize(0),
				BlockStartSample(0)
			{}
			u8* InputData;
			u32 InputBytes;
			u32 PlayBegin;
			u32 LastPacketSize;
			u32 NumSamplesConsumed;
			u32 TotalBytes;
			u32 NumBlocksAvailable;
			u32 SubPacket;
			u32 BlockStartSample;
		};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
class ALIGNAS(16) audDecoderAtrac9 : public audDecoder
{
public:
	
	static bool InitClass();
	static void ShutdownClass();

	virtual bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate));
	virtual void Shutdown();

	virtual audDecoderState GetState() const
	{
		return m_State;
	}
	virtual void SubmitPacket(audPacket &packet);
	virtual void Update();
	virtual u32 ReadData(s16 *dest, const u32 numSamples) const;
	virtual void AdvanceReadPtr(const u32 numSamples);
	virtual u32 QueryNumAvailableSamples() const;

	virtual bool QueryReadyForMoreData() const;

	virtual void SetWaveIdentifier(const u32 waveIdentifier) { m_WaveIdentifier = waveIdentifier; }
private:

	int32_t m_Handle;

	SceAudiodecCtrl m_Ctrl;
	SceAudiodecAuInfo m_Bst;
	SceAudiodecPcmItem m_Pcm;
	SceAudiodecParamAt9 m_Param;
	SceAudiodecAt9Info m_Info;


	atRangeArray<audAtrac9Packet, 2> m_Packets;
	u32 m_SubmitPacketIndex;
	u32 m_DecodePacketIndex;

	audStaticRingBuffer<kAtrac9DecompressedBytesPerBlock*2*2> m_RingBuffer;
	audDecoderState m_State;
	audWaveFormat::audStreamFormat m_Format;
	u32 m_NumChannels;
	u32 m_WaveIdentifier;

	u32 m_TotalSamples;

#if DUMP_RAW_ATRAC_STREAMS
	fiStream* m_fp;
#endif

};
#if __WIN32
#pragma warning(pop)
#endif

} // namespace rage

#endif //RSG_AUDIO_Atrac9

#endif // AUD_DECODER_Atrac9_H
