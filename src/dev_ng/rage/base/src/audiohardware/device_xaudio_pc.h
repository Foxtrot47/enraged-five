//
// audiohardware/device_xaudio_pc.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DEVICE_XAUDIO_PC_H
#define AUD_DEVICE_XAUDIO_PC_H

#if RSG_PC

#include "device.h"

#include "system/ipc.h"
#include "system/xtl.h"
#include "grcore/config.h"
#include "aud_queue.h"

#if RSG_PC && __D3D11_1 // 	#if (_WIN32_WINNT < 0x0602) //_WIN32_WINNT_WIN8
	// Can not use windows 8 sdk audio for windows 7.  Need a better solution than this
	#include <C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include\xaudio2.h>
#else
	#include <Audioclient.h>
	#include <xaudio2.h>
	#include <Mmdeviceapi.h>
#endif
#define AUD_OUTPUT_TO_FILE 0
#define MIXLOOP_IN_OWN_THREAD 0

namespace rage
{

	enum BufferState {
		Free = 0,
		Mixing,
		Submitting,
		Submitted,
		Done
	};

	enum { kXAudioNumberOfBuffers = 8 };
	enum { kXAudioMaxNumberOfBuffers = 16 }; // to keep things simple we will allocate the max number of buffers and just resize the q.
	enum { kXAudioNumberOfSubmitBuffers = 4 };

	class sysMemAllocator;
	class fiStream;
	class audMixerDeviceXAudio2 : public audMixerDevice, IXAudio2VoiceCallback, IMMNotificationClient
	{

	public:

		audMixerDeviceXAudio2();

		// PURPOSE
		//	Initialises XAudio2 output hardware
		// RETURNS
		//	false on failure
		virtual bool InitHardware();
		virtual void ReinitHardware(); //check to see if changes have occurred in the audio device


		// PURPOSE
		//	Releases XAudio2 output resources
		virtual void ShutdownHardware();
		void PartialShutdown();
		void DestroyXAudioVoices();
		
		// PURPOSE
		//	Starts generating output
		virtual void StartMixing();

		// Used to manually pump the mixer during replay capture
		virtual void SuspendedMixerUpdate();
		virtual void TriggerUpdate();
		virtual void WaitOnMixBuffers();
		virtual void SignalWaitOnMixBuffers()		{ sysIpcSignalSema(sm_MixBuffersSema); }

		LONG m_cRef;

		IXAudio2 *GetXAudio2() { return sm_XAudio2; }

		//////////////////////////////////////////////////////////
		// IMMNotificationClient interface
		STDMETHOD_(HRESULT, QueryInterface)(REFIID riid, LPVOID FAR* ppvObj);
		STDMETHOD_(ULONG, AddRef)();
		STDMETHOD_(ULONG, Release)();
		STDMETHOD_ (HRESULT, OnDefaultDeviceChanged)(EDataFlow flow, ERole role,LPCWSTR pwstrDeviceId);
		STDMETHOD_ (HRESULT, OnDeviceAdded)(LPCWSTR pwstrDeviceId);
		STDMETHOD_ (HRESULT, OnDeviceRemoved)(LPCWSTR pwstrDeviceId);
		STDMETHOD_ (HRESULT, OnDeviceStateChanged)(LPCWSTR pwstrDeviceId, DWORD dwNewState);
		STDMETHOD_ (HRESULT, OnPropertyValueChanged)(LPCWSTR pwstrDeviceId,	const PROPERTYKEY key);
		//////////////////////////////////////////////////////////
		

		//////////////////////////////////////////////////////////
		// IXAudio2VoiceCallback interface
		STDMETHOD_(void, OnBufferStart)(void *pContext);
		STDMETHOD_(void, OnBufferEnd)(void *);

		STDMETHOD_(void, OnVoiceProcessingPassStart)(UINT32){}
		STDMETHOD_(void, OnVoiceProcessingPassEnd)(void){}
		STDMETHOD_(void, OnStreamEnd)(void){}
		STDMETHOD_(void, OnLoopEnd)(void *){}
		STDMETHOD_(void, OnVoiceError)(void *,HRESULT){}
		///////////////////////////////////////////////////////////

		static sysCriticalSectionToken sm_SuspendLock;
		static void SetXaudioSuspended(bool suspend);
		static DECLARE_THREAD_FUNC(sMixLoop);

		static void SetNumberOfAudioMixBuffers(u32 numMixBuffers)	{ sm_NewNumberOfAudioBuffers = Clamp(numMixBuffers, (u32)kXAudioNumberOfBuffers, (u32)kXAudioMaxNumberOfBuffers); }

#if __BANK
		static f32 sm_OnBufferStartAverageFrameTime;
		static f32 sm_OnBufferStartPeakFrameTime;

		void DebugDraw();
#endif

		void MixLoop();

	private:

		bool CreateXAudio();		// creates XAudio interface, not audio device actually required
		void DestroyXAudio();		// releases XAudio interface
		bool InitAudioDevice();		// initializes the audio device, fails if not present
		bool CheckForNewDevice();	// enumerates audio devices
		bool DoWeHaveANewDefaultAudioDevice();

		void OnBufferReady();

		s32 GetFreeOutputBuffer();
		void FreeOutputBuffer(s32 id);

		static IXAudio2 *sm_XAudio2;
		static IXAudio2MasteringVoice *sm_MasteringVoice;
		static IXAudio2SourceVoice *sm_OutputVoice;

		static IMMDeviceEnumerator* sm_DeviceEnumerator;
		static IMMDeviceCollection* sm_DeviceCollection;
		static IMMDevice* sm_AudioRenderDevice;

		static LPWSTR m_DefaultAudioDeviceId;

		sysMemAllocator *m_Allocator;

		// Sometimes when you screw with enabling and disabling devices, the system thinks everything is ok, but the OnBufferStart isn't called.
		// This will get zeroed by OnBufferStart, if that doesn't happen for a certain amount of time we will force a re-init
		static u32 sm_ReinitCounter;
		static u32 sm_SuspendCounter;

		static u32 sm_OutputBufferSizeBytes;
		static u32 sm_BufferSize;
		static f32 *sm_EmptyOutputBuffer;
		static u32 sm_NumHardwareChannels;		

		static bool sm_NoAudioHardware;
		static bool sm_ForceReinit;
		static bool sm_DoingReinit;

		static u32 sm_NumberOfAudioBuffers;
		static u32 sm_NewNumberOfAudioBuffers;

		static sysPerformanceTimer* sm_PerfTimer;

		static volatile bool sm_IsMixLoopThreadRunning, sm_ShouldMixLoopThreadBeRunning;
		static sysIpcThreadId sm_MixLoopThreadId;
		

		static sysIpcSema sm_TriggerUpdateSema; // for capturing
		static sysIpcSema sm_MixBuffersSema;	// for capturing
		static bool sm_WasCapturing;

		audQueue<s32> m_OutputBufferQueue;
		BufferState m_BufferState[kXAudioMaxNumberOfBuffers];
		f32*		m_BufferPool[kXAudioMaxNumberOfBuffers];

		f32*		m_SubmitBuffer[kXAudioNumberOfSubmitBuffers];
		s32			m_SubmitBufferIndex;
		volatile u32 m_CurrentSubmitBufferCount;

		u32 m_NumberOfAudioDevices;

		s32 m_BuffersToDropAfterCapturing;

#if __BANK
		static sysPerformanceTimer* sm_OnBufferStartAverageFrameTimer;
		static sysPerformanceTimer* sm_MixerIntervalTimer;

		static const u32 kMaxHardwareChannels = 8;

		void DumpOutput(u32 channelIndex, f32 data); 

		atRangeArray<u32, kMaxHardwareChannels> m_NumOutputDataBytes;
		atRangeArray<fiStream *, kMaxHardwareChannels> m_OutputStreams;
#endif

#if AUD_OUTPUT_TO_FILE
		fiStream *m_DebugOutput;
#endif
		
	};

} // namespace rage

#endif // __WIN32
#endif // AUD_DEVICE_360_h
