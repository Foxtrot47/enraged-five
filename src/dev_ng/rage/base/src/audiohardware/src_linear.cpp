// 
// audiohardware/src_linear.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

// linear sample rate conversion

#include "audiohardware/channel.h"
#include "system/memops.h"

#include "mixer.h"
#include "src_linear.h"
#include "profile/profiler.h"

#if __XENON
#include "VectorIntrinsics.h"
#endif

#include "math/amath.h"
#include "system/cache.h"
#include "vectormath/vectormath.h"

#if RSG_DURANGO || RSG_ORBIS
// from ammintrin.h
//__m128 _mm_macc_ps(__m128, __m128, __m128);    //?
#endif

namespace rage
{
using namespace Vec;

PF_PAGE(ResamplerStats, "RAGEAudio Resampler");
PF_GROUP(ResamplerStats);
PF_LINK(ResamplerStats, ResamplerStats);

PF_TIMER(Resampler_S16, ResamplerStats);
PF_TIMER(Resampler_F32, ResamplerStats);

#if RSG_CPU_INTEL

#if 0
void FractionalResampleBuffer(const u32 outputSamples, const f32 ratio, f32 trailingFrac, const s16 *input, f32 *output)
{
	const f32 oneOverDivisorScalar = 1.f / 32768.f;
	f32 index = trailingFrac;
	for(u32 i = 0; i < outputSamples; i++)
	{
		u32 flooredIndex = (u32)index;
		f32 frac = index - Floorf(index);
		const f32 sample0 = (f32)input[flooredIndex] * oneOverDivisorScalar;
		const f32 sample1 = (f32)input[flooredIndex+1] * oneOverDivisorScalar;
		output[i] = sample0 +  (frac * (sample1 - sample0));
		index = index + ratio;
	}
}

#else

void FractionalResampleBuffer(const unsigned int outputSamples, const float ratio, float trailingFrac, const s16 *input, float *output)
{
	//PF_FUNC(Resampler_S16);
#if RSG_DURANGO || RSG_ORBIS

	static bool g_UseSSSE3Resampler = RSG_DURANGO || RSG_ORBIS;
	static bool s_UseSSE2Resampler = !RSG_ORBIS;
	if(g_UseSSSE3Resampler)
	{
		const __m128 ratioV = _mm_load_ps1(&ratio);

		const __m128 incrX4 = _mm_mul_ps(ratioV, V4VConstant(V_FOUR));
		const __m128 zeroOneTwoThree = V4VConstant<U32_ZERO, U32_ONE, U32_TWO, U32_THREE>();

		__m128 incr = _mm_mul_ps(zeroOneTwoThree, ratioV);
		incr = _mm_add_ps(incr, _mm_load_ps1(&trailingFrac));

		__m128 oneOverDivisor =  V4VConstant<0x38000100,0x38000100,0x38000100,0x38000100>();// 1.f/32768

		const __m128i squashShuffleMask = _mm_castps_si128(V4VConstant<0,1,4,5,8,9,12,13,0,1,4,5,8,9,12,13>());
		const __m128i onesAsInt = _mm_castps_si128(V4VConstant<1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0>());
		const __m128i offsetHigh64bit = _mm_castps_si128(V4VConstant<0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2>());
		
		for(u32 i = 0; i < outputSamples; i += 4)
		{
			// compute floored indices
			const __m128i floorIncrInt = _mm_cvttps_epi32(incr);
			const __m128 floorIncrF32 = _mm_cvtepi32_ps(floorIncrInt);
			// compute fractional indices
			const __m128 fracIncr = _mm_sub_ps(incr, floorIncrF32);

			// step through interp
			incr = _mm_add_ps(incr, incrX4);

			// extract indices from floorIncrInt
			// NOTE:  this is limited to 16 bit WORDS
			s32 baseOffset = _mm_extract_epi16(floorIncrInt, 0);

			// compute relative indices
			__m128 relIndices = _mm_sub_ps(floorIncrF32, V4SplatX(floorIncrF32));
					
			// load 8 16bit samples
			__m128i allSamples = _mm_castps_si128(_mm_loadu_ps((const float*)&input[baseOffset]));

			// load indexed samples by computing shuffle masks from the relative index 
			__m128i byteIndices = _mm_cvttps_epi32(_mm_mul_ps(relIndices, V4VConstant(V_TWO)));
			
			// We need to derive a shuffle mask from the byte indices, such that we load [byteNi, byteNi+1, ...] for both even and odd samples
			
			//currently byteIndices has 32bit int indices, we can combine that with (itself+1)<<8 to get the n+1 sample
			// duplicate and add 2 for each byte to derive the odd byte indices
			__m128i nPlusOneIndices = _mm_add_epi32(byteIndices, onesAsInt);
			nPlusOneIndices = _mm_slli_epi16(nPlusOneIndices, 8);
			byteIndices = _mm_or_si128(nPlusOneIndices, byteIndices);
			// Shuffle even indices into low 64bits, also copied into upper 64bits.
			byteIndices = _mm_shuffle_epi8(byteIndices, squashShuffleMask);
			// add 1 sample to upper 8 indices (odd sample = even sample + 1)
			byteIndices = _mm_add_epi8(byteIndices, offsetHigh64bit);

			__m128i samples = _mm_shuffle_epi8(allSamples, byteIndices);			
		
			// unpack/convert to float
			__m128 sample1357 = _mm_mul_ps(_mm_cvtepi32_ps(_mm_srai_epi32(_mm_unpackhi_epi16(samples, samples), 16)), oneOverDivisor);
			__m128 sample0246 = _mm_mul_ps(_mm_cvtepi32_ps(_mm_srai_epi32(_mm_unpacklo_epi16(samples, samples), 16)), oneOverDivisor);
						
			// lerp
			const __m128 diff = _mm_sub_ps(sample1357, sample0246);

			// multiply-accumulate added in SSE5
#if 0
			const __m128 output0 = _mm_macc_ps(diff, fracIncr, sample0246);
#else
			const __m128 fracDiff = _mm_mul_ps(diff, fracIncr);		
			const __m128 output0 = _mm_add_ps(sample0246, fracDiff);
#endif
		
			*((__m128*)&output[i]) = output0;
		}
	}
	else if(s_UseSSE2Resampler)
	{
		const __m128 ratioV = _mm_load_ps1(&ratio);

		const __m128 incrX4 = _mm_mul_ps(ratioV, V4VConstant(V_FOUR));
		const __m128 zeroOneTwoThree = V4VConstant<U32_ZERO, U32_ONE, U32_TWO, U32_THREE>();

		__m128 incr = _mm_mul_ps(zeroOneTwoThree, ratioV);
		incr = _mm_add_ps(incr, _mm_load_ps1(&trailingFrac));

		__m128 oneOverDivisor =  V4VConstant<0x38000100,0x38000100,0x38000100,0x38000100>();// 1.f/32768

		const __m128i low16BitsMask = _mm_castps_si128(V4VConstant<0xFF,0xFF,0,0,0,0,0,0,0,0,0,0,0,0,0,0>());
		const __m128i second16BitsMask = _mm_castps_si128(V4VConstant<0,0,0xFF,0xFF,0,0,0,0,0,0,0,0,0,0,0,0>());

		for(u32 i = 0; i < outputSamples; i += 4)
		{
			// compute floored indices
			const __m128i floorIncrInt = _mm_cvttps_epi32(incr);
			const __m128 floorIncrF32 = _mm_cvtepi32_ps(floorIncrInt);
			// compute fractional indices
			const __m128 fracIncr = _mm_sub_ps(incr, floorIncrF32);

			// step through interp
			incr = _mm_add_ps(incr, incrX4);

			// extract indices from floorIncrInt
			// NOTE:  this is limited to 16 bit WORDS
			s32 offsets[4] = {
				_mm_extract_epi16(floorIncrInt, 0),
				_mm_extract_epi16(floorIncrInt, 2),
				_mm_extract_epi16(floorIncrInt, 4),
				_mm_extract_epi16(floorIncrInt, 6) 
			};

			// load indexed samples
		
			// load 8 16bit samples (we want X and Y)
			__m128i evenSamplesS16 = _mm_castps_si128(_mm_loadu_ps((const float*)&input[offsets[3]]));
			__m128i oddSamplesS16 = evenSamplesS16;

			evenSamplesS16 = _mm_and_si128(evenSamplesS16, low16BitsMask);
			oddSamplesS16 = _mm_and_si128(oddSamplesS16, second16BitsMask);

			// shift first samples up to make room for next
			evenSamplesS16 = _mm_slli_si128(evenSamplesS16, 2);
			// First odd sample was already shifted

			// load next samples
			__m128i temp = _mm_castps_si128(_mm_loadu_ps((const float*)&input[offsets[2]]));
			evenSamplesS16 = _mm_or_si128(evenSamplesS16, _mm_and_si128(temp, low16BitsMask));
			evenSamplesS16 = _mm_slli_si128(evenSamplesS16, 2);
			oddSamplesS16 = _mm_or_si128(_mm_slli_si128(oddSamplesS16, 2), _mm_and_si128(temp, second16BitsMask));

			temp = _mm_castps_si128(_mm_loadu_ps((const float*)&input[offsets[1]]));
			evenSamplesS16 = _mm_or_si128(evenSamplesS16, _mm_and_si128(temp, low16BitsMask));
			evenSamplesS16 = _mm_slli_si128(evenSamplesS16, 2);
			oddSamplesS16 = _mm_or_si128(_mm_slli_si128(oddSamplesS16, 2), _mm_and_si128(temp, second16BitsMask));
		
			temp = _mm_castps_si128(_mm_loadu_ps((const float*)&input[offsets[0]]));
			evenSamplesS16 = _mm_or_si128(evenSamplesS16, _mm_and_si128(temp, low16BitsMask));
			oddSamplesS16 = _mm_or_si128(oddSamplesS16, _mm_and_si128(_mm_srli_si128(temp, 2), low16BitsMask));
		
			// convert to float
			__m128 sample1357 = _mm_mul_ps(_mm_cvtepi32_ps(_mm_srai_epi32(_mm_unpacklo_epi16(oddSamplesS16, oddSamplesS16), 16)), oneOverDivisor);
			__m128 sample0246 = _mm_mul_ps(_mm_cvtepi32_ps(_mm_srai_epi32(_mm_unpacklo_epi16(evenSamplesS16, evenSamplesS16), 16)), oneOverDivisor);

			// lerp
			const __m128 diff = _mm_sub_ps(sample1357, sample0246);
			const __m128 fracDiff = _mm_mul_ps(diff, fracIncr);		
			const __m128 output0 = _mm_add_ps(sample0246, fracDiff);
		
			*((__m128*)&output[i]) = output0;
		}
	}
	else

#endif // DURANGO | ORBIS
	{
		const f32 oneOverDivisorScalar = 1.f / 32768.f;
		f32 index = trailingFrac;
		for(u32 i = 0; i < outputSamples; i++)
		{
			u32 flooredIndex = (u32)index;
			f32 frac = index - Floorf(index);
			const f32 sample0 = (f32)input[flooredIndex] * oneOverDivisorScalar;
			const f32 sample1 = (f32)input[flooredIndex+1] * oneOverDivisorScalar;
			output[i] = sample0 +  (frac * (sample1 - sample0));
			index = index + ratio;
		}
	}
}

#endif

ALIGNAS(16) const float ratioScalars[4]  = {0.f, 1.f, 2.f, 3.f} ;

void FractionalResampleBuffer(const unsigned int outputSamples, const float ratio, float trailingFrac, const float *input, float *output)
{
	//PF_FUNC(Resampler_F32);
#if RSG_CPU_X86
	const float four = 4.f;
	Assert((outputSamples&3)==0);
	Assert(outputSamples<=kMixBufNumSamples);
	__asm
	{
		//const __m128 incrX4[xmm0] = _mm_set_ps1(ratio * 4.f);					
		MOVSS xmm0, ratio
		MOVSS xmm3, trailingFrac
		MOVAPS xmm1, xmm0
		MULSS xmm0, four
		PSHUFD xmm0, xmm0, 0		
		PSHUFD xmm1, xmm1, 0
		PSHUFD xmm3, xmm3, 0
		//__m128 incr[xmm1] = _mm_set_ps(ratio * 3.f, ratio * 2.f, ratio, 0.f);
		MOVAPD xmm2, ratioScalars
		MULPS xmm1, xmm2
		ADDPS xmm1, xmm3
		XOR ecx, ecx
_loop:
		// compute floored indices
		//const __m128i floorIncrInt[xmm2] = _mm_cvttps_epi32(incr);
		CVTTPS2DQ xmm2, xmm1
		//const __m128 floorIncrF32[xmm3 temp] = _mm_cvtepi32_ps(floorIncrInt);
		CVTDQ2PS xmm3, xmm2
		// compute fractional indices
		//const __m128 fracIncr[xmm4] = _mm_sub_ps(incr, floorIncrF32);
		MOVAPS xmm4, xmm1
		SUBPS xmm4, xmm3 // <-- xmm3 is now free

		// step through interp
		//incr = _mm_add_ps(incr, incrX4);
		ADDPS xmm1, xmm0

		// extract indices from floorIncrInt(xmm2)
		// NOTE:  this is limited to 16 bit WORDS
		PEXTRW eax, xmm2, 0
		PEXTRW edx, xmm2, 2
		PEXTRW esi, xmm2, 4
		PEXTRW edi, xmm2, 6

		// load indexed samples
		// even into xmm2, odd into xmm3 using xmm5 as temp
		MOV ebx, [input]
		MOVSS xmm2, [ebx + edi * 4]
		MOVSS xmm3, [ebx + (edi+4) * 4]
		PSLLDQ xmm2, 4
		PSLLDQ xmm3, 4
		MOVSS xmm5, [ebx + esi * 4]
		ORPS xmm2, xmm5
		MOVSS xmm5, [ebx + (esi+4) * 4]
		ORPS xmm3, xmm5
		PSLLDQ xmm2, 4
		PSLLDQ xmm3, 4
		MOVSS xmm5, [ebx + edx * 4]
		ORPS xmm2, xmm5
		MOVSS xmm5, [ebx + (edx+4) * 4]
		ORPS xmm3, xmm5
		PSLLDQ xmm2, 4
		PSLLDQ xmm3, 4
		MOVSS xmm5, [ebx + eax * 4]
		ORPS xmm2, xmm5
		MOVSS xmm5, [ebx + (eax+4) * 4]
		ORPS xmm3, xmm5

		// xmm2 contains even samples
		// xmm3 contains odd samples
		MOVAPS xmm5, xmm3
		//const __m128 diff = _mm_sub_ps(sample1357, sample0246);
		SUBPS xmm5, xmm2 // xmm5 is diff
		//const __m128 fracDiff[xmm5] = _mm_mul_ps(diff, fracIncr);
		MULPS xmm5, xmm4
		//const __m128 output0[xmm2] = _mm_add_ps(sample0246, fracDiff);
		ADDPS xmm2, xmm5
		MOV ebx, [output]
		MOVAPS [ebx + ecx * 4], xmm2

		ADD ecx, 4
		CMP ecx, outputSamples
		JL _loop
	}
#else

	//x64

	// Temp version (no idea if it works) stolen from below
	f32 index = trailingFrac;
	for(u32 i = 0; i < outputSamples; i++)
	{
		u32 flooredIndex = (u32)index;
		f32 frac = index - floorf(index);
		output[i] = input[flooredIndex] +  (frac * (input[flooredIndex+1] - input[flooredIndex]));
		index = index + ratio;
	}
#endif
}

#else // !RSG_CPU_INTEL

void FractionalResampleBuffer_FP(const unsigned int outputSamples, const float ratio, float trailingFrac, const s16 *input, float *output)
{
	//////////////////////////////////////////////////////////////////////////
	// static data
	const Vector_4V vFour = V4VConstant(V_FOUR);
	// 0, 1, 2, 3
	const Vector_4V vMul = V4VConstant<0U,0x3F800000,0x40000000,0x40400000>();

	const Vector_4V vShiftControl = V4VConstant<
		3,3,3,3,
		7,7,7,7,
		11,11,11,11,
		15,15,15,15>();

	const Vector_4V v0123 = V4VConstant<
		0,1,2,3,
		0,1,2,3,
		0,1,2,3,
		0,1,2,3>();

	const Vector_4V v1234 = V4VConstant<
		4,5,6,7,
		4,5,6,7,
		4,5,6,7,
		4,5,6,7>();

	// 16.48 fixed point index
	enum {kFixedPointFracBits = 48};
	const u64 fixedPointRatioX4 = (u64)(double(ratio*4) * 281474976710656ULL);
	u64 fixedPointIndex = (u64)(trailingFrac * 281474976710656ULL);

	Vector_4V incr = V4LoadScalar32IntoSplatted(ratio);
	const Vector_4V incrX4 = V4Scale(incr, vFour);

	incr = V4Scale(incr,vMul);

	incr = V4Add(V4LoadScalar32IntoSplatted(trailingFrac), incr);

	for(u32 i = 0 ; i < outputSamples; i += 4)
	{
		// convert to integer byte indices
		// 12 cycles, vector float
		Vector_4V vFlooredIndices = V4RoundToNearestIntZero(incr);

		// 12 cycles, vector float
		Vector_4V vFracIndices = V4Subtract(incr,vFlooredIndices);

		// compute indices relative to base index (component 0 of indicesint)
		Vector_4V baseOffset = V4SplatX(vFlooredIndices);

		// convert to byte offsets
		Vector_4V offsetedIndices = V4Subtract(vFlooredIndices,baseOffset);
		Vector_4V vByteIndices = V4FloatToIntRaw<0>(V4Scale(offsetedIndices,vFour));

		// step through interpolation
		incr = V4Add(incr,incrX4);

		const u32 sampleIndexToLoad = (fixedPointIndex >> kFixedPointFracBits);
		fixedPointIndex += (fixedPointRatioX4);


		// load 8 unaligned samples
#if __XENON
		const Vector_4V s16Data = __loadunalignedvector(&input[sampleIndexToLoad]);
#elif __PS3
		//__vor(__lvlx(ptr, 0), __lvrx(ptr,16))
		s16 *ptr = const_cast<s16*>(&input[sampleIndexToLoad]);
		const Vector_4V s16Data = (Vector_4V)(vec_or(vec_lvlx(0, ptr), vec_lvrx(16, ptr)));
#endif

		// convert to normalized f32
		const Vector_4V lowSamples = V4IntToFloatRaw<15>(V4UnpackLowSignedShort(s16Data));
		const Vector_4V highSamples = V4IntToFloatRaw<15>(V4UnpackHighSignedShort(s16Data));

		// compute permute mask to extract samples
		// 4 cycles, vector permute
		Vector_4V vLeftIndices = V4Permute(vByteIndices, vShiftControl);
		// 4 cycles, vector simple
		Vector_4V vLeftMask = V4AddInt(vLeftIndices,v0123);
		// right sample indices are (left sample indices + 4 bytes)
		Vector_4V vRightMask = V4AddInt(vLeftIndices,v1234);
		// 4 cycles, vector permute
		Vector_4V vLeftSamples = V4PermuteTwo(lowSamples,highSamples,vLeftMask);
		Vector_4V vRightSamples = V4PermuteTwo(lowSamples,highSamples,vRightMask);

		Vector_4V vOutput = V4Add(vLeftSamples, V4Scale(V4Subtract(vRightSamples,vLeftSamples),vFracIndices));

		// store result
		*((Vector_4V*)&output[i]) = vOutput;
	}
}

// Must be cache-line aligned
ALIGNAS(128) u32 integerIndices[kMixBufNumSamples];
void FractionalResampleBuffer(const unsigned int outputSamples, const float ratio, float trailingFrac, const s16 *input, float *output)
{
	//////////////////////////////////////////////////////////////////////////
	// static data
	const Vector_4V vFour = V4VConstant(V_FOUR);
	// 0, 1, 2, 3
	const Vector_4V vMul = V4VConstant<0U,0x3F800000,0x40000000,0x40400000>();

	const Vector_4V vShiftControl = V4VConstant<
		3,3,3,3,
		7,7,7,7,
		11,11,11,11,
		15,15,15,15>();

	const Vector_4V v0123 = V4VConstant<
		0,1,2,3,
		0,1,2,3,
		0,1,2,3,
		0,1,2,3>();

	const Vector_4V v1234 = V4VConstant<
		4,5,6,7,
		4,5,6,7,
		4,5,6,7,
		4,5,6,7>();
	
	Assert(outputSamples <= kMixBufNumSamples);

	Vector_4V incr = V4LoadScalar32IntoSplatted(ratio);
	const Vector_4V incrX4 = V4Scale(incr, vFour);

	incr = V4Scale(incr,vMul);

	incr = V4Add(V4LoadScalar32IntoSplatted(trailingFrac), incr);
	
	const Vector_4V incrInit = incr;

	ZeroDC(&integerIndices[0], 0);
	ZeroDC(&integerIndices[0], 128);
	ZeroDC(&integerIndices[0], 256);
	ZeroDC(&integerIndices[0], 384);
	ZeroDC(&integerIndices[0], 512);
	ZeroDC(&integerIndices[0], 640);
	ZeroDC(&integerIndices[0], 768);
	ZeroDC(&integerIndices[0], 896);


	// First pass; calculate base offset indices
	Vector_4V *intPtr = (Vector_4V*)&integerIndices[0];

	for(u32 i = 0 ; i < outputSamples; i += 4)
	{
		// convert to integer byte indices
		// 12 cycles, vector float
		Vector_4V vFlooredIndices = V4RoundToNearestIntZero(incr);
		Vector_4V vIndicesInt = V4FloatToIntRaw<0>(vFlooredIndices);
		// store indices 
		*intPtr++ = vIndicesInt;

		// step through interpolation
		incr = V4Add(incr,incrX4);
	}

	incr = incrInit;

	// Second pass; compute interpolation
	for(u32 i = 0 ; i < outputSamples; i += 4)
	{		
		Vector_4V vFlooredIndices = V4RoundToNearestIntZero(incr);

		// convert to byte offsets
		// compute indices relative to base index (component 0 of indicesint)
		Vector_4V baseOffset = V4SplatX(vFlooredIndices);
		Vector_4V offsetedIndices = V4Subtract(vFlooredIndices,baseOffset);
		Vector_4V vByteIndices = V4FloatToIntRaw<0>(V4Scale(offsetedIndices,vFour));

		Vector_4V vFracIndices = V4Subtract(incr,vFlooredIndices);

		const u32 sampleIndexToLoad = integerIndices[i];


		// step through interpolation
		incr = V4Add(incr,incrX4);

		// load 8 unaligned samples
#if __XENON
		const Vector_4V s16Data = __loadunalignedvector(&input[sampleIndexToLoad]);
#elif __PS3
		//__vor(__lvlx(ptr, 0), __lvrx(ptr,16))
		s16 *ptr = const_cast<s16*>(&input[sampleIndexToLoad]);
		const Vector_4V s16Data = (Vector_4V)(vec_or(vec_lvlx(0, ptr), vec_lvrx(16, ptr)));
#endif

		// convert to normalized f32
		const Vector_4V lowSamples = V4IntToFloatRaw<15>(V4UnpackLowSignedShort(s16Data));
		const Vector_4V highSamples = V4IntToFloatRaw<15>(V4UnpackHighSignedShort(s16Data));

		// compute permute mask to extract samples
		// 4 cycles, vector permute
		Vector_4V vLeftIndices = V4Permute(vByteIndices, vShiftControl);
		// 4 cycles, vector simple
		Vector_4V vLeftMask = V4AddInt(vLeftIndices,v0123);
		// right sample indices are (left sample indices + 4 bytes)
		Vector_4V vRightMask = V4AddInt(vLeftIndices,v1234);
		// 4 cycles, vector permute
		Vector_4V vLeftSamples = V4PermuteTwo(lowSamples,highSamples,vLeftMask);
		Vector_4V vRightSamples = V4PermuteTwo(lowSamples,highSamples,vRightMask);

		Vector_4V vOutput = V4Add(vLeftSamples, V4Scale(V4Subtract(vRightSamples,vLeftSamples),vFracIndices));

		// store result
		*((Vector_4V*)&output[i]) = vOutput;
	}
}

void FractionalResampleBuffer(const unsigned int outputSamples, const float ratio, float trailingFrac, const f32 *input, float *output)
{
	//////////////////////////////////////////////////////////////////////////
	// static data
	const Vector_4V vFour = V4VConstant(V_FOUR);
	// 0, 1, 2, 3
	const Vector_4V vMul = V4VConstant<0U,0x3F800000,0x40000000,0x40400000>();

	const Vector_4V vShiftControl = V4VConstant<
		3,3,3,3,
		7,7,7,7,
		11,11,11,11,
		15,15,15,15>();

	const Vector_4V v0123 = V4VConstant<
		0,1,2,3,
		0,1,2,3,
		0,1,2,3,
		0,1,2,3>();

	const Vector_4V v1234 = V4VConstant<
		4,5,6,7,
		4,5,6,7,
		4,5,6,7,
		4,5,6,7>();

	//////////////////////////////////////////////////////////////////////////

	ALIGNAS(16) unsigned int indices[4] ;

	Vector_4V incr = V4LoadScalar32IntoSplatted(ratio);
	const Vector_4V incrX4 = V4Scale(incr, vFour);

	incr = V4Scale(incr,vMul);

	incr = V4Add(V4LoadScalar32IntoSplatted(trailingFrac), incr);

	for(u32 i = 0 ; i < outputSamples; i += 4)
	{
		// convert to integer byte indices
		// 12 cycles, vector float
		Vector_4V vFlooredIndices = V4RoundToNearestIntZero(incr);

		// 12 cycles, vector float
		Vector_4V vFracIndices = V4Subtract(incr,vFlooredIndices);

		Vector_4V vIndicesInt = V4FloatToIntRaw<0>(vFlooredIndices);
		// store indices 
		*((Vector_4V*)&indices) = vIndicesInt;

		// compute indices relative to base index (component 0 of indicesint)
		Vector_4V baseOffset = V4SplatX(vFlooredIndices);

		// convert to byte offsets
		Vector_4V offsetedIndices = V4Subtract(vFlooredIndices,baseOffset);
		Vector_4V vByteIndices = V4FloatToIntRaw<0>(V4Scale(offsetedIndices,vFour));

		// step through interpolation
		incr = V4Add(incr,incrX4);

		const u32 sampleIndexToLoad = indices[0];

		// load 8 (unaligned) samples
#if __XENON
		const Vector_4V lowSamples = __loadunalignedvector(&input[sampleIndexToLoad]);
		const Vector_4V highSamples = __loadunalignedvector(&input[sampleIndexToLoad] + 4);
#elif __PS3
		//__vor(__lvlx(ptr, 0), __lvrx(ptr,16))
		f32 *ptr = const_cast<f32*>(&input[sampleIndexToLoad]);
		const Vector_4V lowSamples = (Vector_4V)(vec_or(vec_lvlx(0, ptr), vec_lvrx(16, ptr)));
		ptr += 4;
		const Vector_4V highSamples = (Vector_4V)(vec_or(vec_lvlx(0, ptr), vec_lvrx(16, ptr)));
#endif

		// compute permute mask to extract samples
		// 4 cycles, vector permute
		Vector_4V vLeftIndices = V4Permute(vByteIndices, vShiftControl);
		// 4 cycles, vector simple
		Vector_4V vLeftMask = V4AddInt(vLeftIndices,v0123);
		// right sample indices are (left sample indices + 4 bytes)
		Vector_4V vRightMask = V4AddInt(vLeftIndices,v1234);
		// 4 cycles, vector permute
		Vector_4V vLeftSamples = V4PermuteTwo(lowSamples,highSamples,vLeftMask);
		Vector_4V vRightSamples = V4PermuteTwo(lowSamples,highSamples,vRightMask);

		Vector_4V vOutput = V4Add(vLeftSamples, V4Scale(V4Subtract(vRightSamples,vLeftSamples),vFracIndices));

		// store result
		*((Vector_4V*)&output[i]) = vOutput;
	}
}
#endif // !__WIN32PC


#if 0
void FractionalResampleBuffer(const unsigned int outputSamples, const float ratio, float trailingFrac, const float *input, float *output)
{
	f32 index = trailingFrac;
	for(u32 i = 0; i < outputSamples; i++)
	{
		u32 flooredIndex = (u32)index;
		f32 frac = index - floorf(index);
		output[i] = input[flooredIndex] +  (frac * (input[flooredIndex+1] - input[flooredIndex]));
		index = index + ratio;
	}
}

#endif


// PURPOSE
//	Decimate by an integer factor of 2 with an averaging filter
void IntegerDownsample(const unsigned int outputSamples, const f32 *RESTRICT src, f32 *RESTRICT dest)
{
	const Vector_4V oneOverRatio = V4VConstant(V_HALF);

	const u32 trailingSamples = outputSamples & 7;
	const u32 alignedSamples = outputSamples - trailingSamples;
	for(u32 i = 0; i < alignedSamples; i += 4)
	{

		const Vector_4V inputSamples0 = *(Vector_4V*)(&src[i*2]);
		const Vector_4V inputSamples1 = *(Vector_4V*)(&src[i*2+4]);

		const Vector_4V evenSamples = V4PermuteTwo<X1,Z1,X2,Z2>(inputSamples0,inputSamples1);
		const Vector_4V oddSamples = V4PermuteTwo<Y1,W1,Y2,W2>(inputSamples0,inputSamples1);

		const Vector_4V averageSamples = V4Scale(V4Add(evenSamples,oddSamples),oneOverRatio);
		
		*(Vector_4V*)(&dest[i]) = averageSamples;
	}

	const f32 oneOverRatioScalar = 0.5f;
	for(u32 i = alignedSamples; i < outputSamples; i++)
	{
		f32 sum = 0.f;
		for(u32 j = 0; j < 2; j++)
		{
			sum += src[i*2 + j];
		}
		dest[i] = sum * oneOverRatioScalar;
	}
}

// PURPOSE
//	Decimate by an integer factor of 2 with an averaging filter
// NOTES
//	Also converts from s16 to f32
void IntegerDownsample(const unsigned int outputSamples, const s16 *RESTRICT src, f32 *RESTRICT dest)
{
#if RSG_CPU_INTEL
	const Vector_4V oneOverDivisor = V4VConstant<0x38000100,0x38000100,0x38000100,0x38000100>();// 1.f/32768
#endif

	const Vector_4V oneOverRatio = V4VConstant(V_HALF);

	const u32 trailingSamples = outputSamples & 7;
	const u32 alignedSamples = outputSamples - trailingSamples;
	for(u32 i = 0; i < alignedSamples; i += 4)
	{
		const Vector_4V loadedSamples = *(Vector_4V*)(&src[i*2]);

#if RSG_CPU_INTEL
		// On PC V4IntoToFloatRaw is only efficient when exponent = 0, so scale manually
		const Vector_4V inputSamples0 = V4Scale(oneOverDivisor, V4IntToFloatRaw<0>(V4UnpackLowSignedShort(loadedSamples)));
		const Vector_4V inputSamples1 = V4Scale(oneOverDivisor, V4IntToFloatRaw<0>(V4UnpackHighSignedShort(loadedSamples)));
#else
		const Vector_4V inputSamples0 = V4IntToFloatRaw<15>(V4UnpackLowSignedShort(loadedSamples));
		const Vector_4V inputSamples1 = V4IntToFloatRaw<15>(V4UnpackHighSignedShort(loadedSamples));
#endif

		const Vector_4V evenSamples = V4PermuteTwo<X1,Z1,X2,Z2>(inputSamples0,inputSamples1);
		const Vector_4V oddSamples = V4PermuteTwo<Y1,W1,Y2,W2>(inputSamples0,inputSamples1);

		const Vector_4V averageSamples = V4Scale(V4Add(evenSamples,oddSamples),oneOverRatio);

		*(Vector_4V*)(&dest[i]) = averageSamples;
	}

	const f32 oneOverRatioScalar = 0.5f;
	const f32 oneOverDivisorScalar = (1.f/32768.f);
	for(u32 i = alignedSamples; i < outputSamples; i++)
	{
		f32 sum = 0.f;
		for(u32 j = 0; j < 2; j++)
		{
			sum += ((f32)src[i*2+j] * oneOverDivisorScalar);
		}
		dest[i] = sum * oneOverRatioScalar;
	}
}

u32 audRateConvertorLinear::Process(const f32 ratio, f32 *RESTRICT output, const s16 *RESTRICT input, const u32 numInputSamples, u32 numOutputSamples, const f32 trailingFrac)
{
	Assert((((size_t)output)&15)==0);
	Assert((((size_t)input)&15)==0);

	//	Working buffer for the integer downsample - output is max 2 * mixBuf as that is the limit of the fractional
	//	resampler.
#if __SPU
	audAutoScratchBookmark autoScratch;
	f32* srcDownsampleBuf = (f32*) AllocateFromScratch( sizeof(f32) * (kMixBufNumSamples*2 + 1), 16, "SrcDownsampleBuf" );
#else
	f32* srcDownsampleBuf = AllocaAligned( f32, kMixBufNumSamples*2 + 1, 16 );
#endif

	// + 2 since we interp between n and n+1
	const u32 numInputSamplesNeeded = 2 + (u32)((numOutputSamples - 1) * ratio + trailingFrac);
	const u32 numOutputSamplesToGenerate = numOutputSamples;
	if(numInputSamplesNeeded > numInputSamples)
	{
		numOutputSamples = (u32)((numInputSamples - 1.f - trailingFrac) * (1.f/ratio));
		Assert(numOutputSamples <= numOutputSamplesToGenerate);

#if __ASSERT
		if(numOutputSamples > 0)
		{
			const u32 newNumInputSamplesNeeded = 2 + (u32)((numOutputSamples - 1) * ratio + trailingFrac);
			audAssertf(newNumInputSamplesNeeded <= numInputSamples, "newNumInputSamplesNeeded %u numInputSamples %u numOutputSamples %u ratio %f trailingFrac %f", newNumInputSamplesNeeded, numInputSamples, numOutputSamples, ratio, trailingFrac);
		}
#endif
	}
	
	// It's possible that we won't have enough data to produce any samples, in which case skip straight to zero-padding
	if(numOutputSamples > 0)
	{
		// since we are limited by numInputSamples number of samples to generate may not be multiple of 4
		const u32 numOutputSamplesAligned = numOutputSamples & (~3U);
		const f32 oneOver32768 = 1.f/32768.f;

		Assert(ratio <= 4.f);
		if(ratio == 2.0f)
		{
			Assert(numInputSamples>>1 <= (kMixBufNumSamples<<1)+1);
			if (output)
			{
				IntegerDownsample(numOutputSamples, input, output);
			}
		}
		else if(ratio >= 2.f)
		{
			Assert(numInputSamples>>1 <= (kMixBufNumSamples<<1)+1);
			IntegerDownsample(numInputSamples>>1,input,srcDownsampleBuf);
			srcDownsampleBuf[numInputSamples>>1] = 0.f;
			FractionalResampleBuffer(numOutputSamplesAligned, ratio*0.5f, trailingFrac*0.5f, srcDownsampleBuf, output);
			f32 index = (f32)numOutputSamplesAligned * (ratio*0.5f) + (trailingFrac*0.5f);
			// convert trailing samples
			for(u32 i = 0; i < numOutputSamples - numOutputSamplesAligned; i ++)
			{
				const u32 flooredIndex = (u32)index;
				const f32 fracPart = index-Floorf(index);
				audAssertf(flooredIndex+1 <= numInputSamples>>1, "index: %f numInputSamples: %u numOutputSamples: %u trailingFrac: %f ratio: %f", index, numInputSamples, numOutputSamples, trailingFrac, ratio);
				const f32 sample0 = srcDownsampleBuf[flooredIndex];
				const f32 sample1 = srcDownsampleBuf[flooredIndex+1];
				if (output)
				{
					output[i+numOutputSamplesAligned] =  Lerp(fracPart,sample0,sample1);
				}
				index += (ratio*0.5f);
			}
		}
		else
		{
#if !RSG_CPU_INTEL
			if(ratio > 0.13f)
			{
				if (output)
				{
					FractionalResampleBuffer_FP(numOutputSamplesAligned, ratio, trailingFrac, input, output);
				}
			}
			else
#endif
			{
				FractionalResampleBuffer(numOutputSamplesAligned, ratio, trailingFrac, input, output);
			}
			f32 index = (f32)numOutputSamplesAligned * ratio + trailingFrac;
			// convert trailing samples
			for(u32 i = 0; i < numOutputSamples - numOutputSamplesAligned; i ++)
			{
				const u32 flooredIndex = (u32)index;
				const f32 fracPart = index-Floorf(index);
				audAssertf(flooredIndex+1 < numInputSamples, "index: %f numInputSamples: %u numOutputSamples: %u trailingFrac: %f ratio: %f", index, numInputSamples, numOutputSamples, trailingFrac, ratio);
				const f32 sample0 = oneOver32768 * input[flooredIndex];
				const f32 sample1 = oneOver32768 * input[flooredIndex+1];
				if (output)
				{
					output[i+numOutputSamplesAligned] =  Lerp(fracPart,sample0,sample1);	
				}
				index += ratio;
			}
		}
	}

	// ensure we generate the required number of samples; pad with silence if we didn't have enough input samples
	if(numOutputSamplesToGenerate - numOutputSamples != 0)
	{
		sysMemSet(&output[numOutputSamples], 0, sizeof(f32) * (numOutputSamplesToGenerate - numOutputSamples));
	}
	// Return the number of samples generated
	return numOutputSamples;
}

}//namespace rage

