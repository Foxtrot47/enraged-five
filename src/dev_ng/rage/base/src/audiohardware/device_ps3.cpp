// 
// audiohardware/device_ps3.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if __PS3

#include "device_ps3.h"
#include "driver.h"
#include "submix.h"

#include "file/stream.h"

#include <cell/audio.h>
#include <cell/sysmodule.h>
#include <cell/mstream.h> // for the CELL_MS_AUDIOMODESELECT_ #defines

#include <sys/timer.h>
#include <sysutil/sysutil_sysparam.h>

#include "system/memory.h"
#include "system/param.h"
#include "system/performancetimer.h"
#include "system/stack.h"
#include "profile/profiler.h"
#include "system/task_spu_config.h"
#include "vectormath/vectormath.h"

#pragma comment(lib, "audio_stub")

#include "decoder_spu.h"
#include "vramhelper.h"

EXT_PF_PAGE(MixerDevicePage);
PF_GROUP(AudioDevicePS3);
PF_LINK(MixerDevicePage,AudioDevicePS3);

PF_TIMER(WaitTime, AudioDevicePS3);
PF_TIMER(MixTime, AudioDevicePS3);

namespace rage 
{
	
	extern CellSpurs *g_Spurs;
#if !SYSTEM_WORKLOAD
	extern CellSpurs *g_Spurs2;
#endif
	audMixerDevicePs3::audMixerDevicePs3() : 
		m_PortId(~0U), 
		m_PortBuffer(NULL),
		m_LibMP3Mem(NULL),
		m_EventQueueId(0),
		m_EventQueueKey(0),
		m_IsThreadRunning(false)
	{

	}

	bool audMixerDevicePs3::InitHardware()
	{
		s32 ret = ConfigureOutputHardware(CELL_MS_AUDIOMODESELECT_SUPPORTSLPCM  | CELL_MS_AUDIOMODESELECT_SUPPORTSDOLBY | CELL_MS_AUDIOMODESELECT_PREFERDOLBY);
		if(ret < 0)
		{
			audAssertf(false, "ConfigureOutputHardware failed: %d", ret);
			return false;
		}

		const bool dolbyDigitalEnabled = ((ret & 0x10) == 0x10);
		const bool DTSEnabled = ((ret & 0x20) == 0x20);
		const u32 numOutputChannels = (ret & 0xf);

		if(dolbyDigitalEnabled)
		{
			audDisplayf("Dolby Digital audio output");
		}
		else if(DTSEnabled)
		{
			audDisplayf("DTS audio output");
		}

		if(numOutputChannels == 1)
		{
			audDisplayf("Mono audio output :(");
			audDriver::SetHardwareOutputMode(AUD_OUTPUT_MONO);
		}
		else if(numOutputChannels == 2)
		{
			audDisplayf("Stereo audio output");
			audDriver::SetHardwareOutputMode(AUD_OUTPUT_STEREO);
		}
		else if(numOutputChannels == 6)
		{
			audDisplayf("5.1 audio output");
			audDriver::SetHardwareOutputMode(AUD_OUTPUT_5_1);
		}
		else if(numOutputChannels == 8)
		{
			audDisplayf("7.1 audio output");
			audDriver::SetHardwareOutputMode(AUD_OUTPUT_7_1);
		}
		else
		{
			audErrorf("Unsupported number of audio output channels: %u", numOutputChannels);
			audAssertf(false, "Unsupported number of output channels: %u", numOutputChannels);
			audDriver::SetHardwareOutputMode(AUD_OUTPUT_5_1);
		}

		// downmix according to output hardware/setup
		audDriver::SetDownmixOutputMode(audDriver::GetHardwareOutputMode());

		if((ret = cellAudioInit()) != CELL_OK)
		{
			audErrorf("libaudio cellAudioInit() returned %d", ret);
			return false;
		}

		CellAudioPortParam portParam;
		/*typedef struct{
		uint64_t   nChannel;
		uint64_t   nBlock;
		uint64_t   attr;
		float      level;
		} CellAudioPortParam;*/

		portParam.nChannel = CELL_AUDIO_PORT_8CH;

		const s32 blocks = CELL_AUDIO_BLOCK_8;

		portParam.nBlock = blocks;
		portParam.attr = CELL_AUDIO_PORTATTR_INITLEVEL;
		portParam.level = 1.f;
		if(cellAudioPortOpen(&portParam, &m_PortId) != CELL_OK)
		{
			audErrorf("libaudio cellAudioPortOpen() returned %d", ret);
			return false;
		}

		CellAudioPortConfig portConfig;
		if((ret = cellAudioGetPortConfig(m_PortId, &portConfig)) != CELL_OK)
		{
			audErrorf("libaudio cellAudioGetPortConfig() returned %d", ret);
			return false;
		}

		Assert(portConfig.nChannel == CELL_AUDIO_PORT_8CH);
		Assert(portConfig.nBlock == blocks);
		Assert(portConfig.portSize == kMixBufNumSamples * 8 * sizeof(f32) * blocks);
		Assert(portConfig.portAddr);
		Assert(portConfig.status == CELL_AUDIO_STATUS_READY);

		m_PortBuffer = (f32*)portConfig.portAddr;

		// create an event queue
		if((ret = cellAudioCreateNotifyEventQueue(&m_EventQueueId, &m_EventQueueKey)) != CELL_OK)
		{
			audErrorf("cellAudioCreateNotifyEventQueue returned %d", ret);
			return false;
		}

		if((ret = cellAudioSetNotifyEventQueue(m_EventQueueKey)) != CELL_OK)
		{
			audErrorf("cellAudioSetNotifyEventQueue returned %d", ret);
			return false;
		}

		audDecoderSpu::InitClass();

		return true;
	}

	// NOTES: this is based on cellMSSystemConfigureSysUtilEx_220
	// (hence the shitty code.)
	int audMixerDevicePs3::ConfigureOutputHardware(unsigned int _flags)
	{

		/*
		supported flags (taken from mstream.h):
		#define CELL_MS_AUDIOMODESELECT_SUPPORTSLPCM	(1)
		#define CELL_MS_AUDIOMODESELECT_SUPPORTSDOLBY	(2)
		#define CELL_MS_AUDIOMODESELECT_SUPPORTSDTS	(4)
		#define CELL_MS_AUDIOMODESELECT_PREFERDOLBY	(8)
		#define CELL_MS_AUDIOMODESELECT_PREFERDTS	(16)
		*/

		int ret, cnt, AC3On = 0, DTSOn = 0;
		CellAudioOutConfiguration a_config;
		CellAudioOutState a_state;

		memset(&a_config, 0, sizeof(CellAudioOutConfiguration));


		//check that LPCM is at the very least specified as one of the flags!
		if(!(_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSLPCM))
		{
			printf("*** Error *** You MUST specify CELL_MS_AUDIOMODESELECT_SUPPORTSLPCM in the flags passed to cellMSSystemConfigureSysUtilEx!\n");
			printf("LPCM support is a requirement in the TRC!\n");
			return -1;
		}

		//check that preferred modes are specified as supported 
		if(_flags & CELL_MS_AUDIOMODESELECT_PREFERDOLBY)
		{
			if(!(_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDOLBY))
			{
				printf("*** Error *** You cant specify PREFER DOLBY without the SUPPORTS DOLBY flag!\n");
				return -1;
			}
		}


		if(_flags & CELL_MS_AUDIOMODESELECT_PREFERDTS)
		{
			if(!(_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDTS))
			{
				printf("*** Error *** You cant specify PREFER DTS without the SUPPORTS DTS flag!\n");
				return -1;
			}
		}

		//check that only one or the other preferred flags are set - they are mutually exclusive
		if((_flags & CELL_MS_AUDIOMODESELECT_PREFERDOLBY) && (_flags & CELL_MS_AUDIOMODESELECT_PREFERDTS))
		{
			printf("*** Error *** You cant specify both Dolby and DTS as preferred - They are mutually exclusive flags!\n");
			return -1;
		}


		//check that if both Dolby and DTS audio modes are passed as supported, that a preferred mode is also passed
		if((_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDOLBY) && (_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDTS))
		{
			if((!(_flags & CELL_MS_AUDIOMODESELECT_PREFERDOLBY)) && (!(_flags & CELL_MS_AUDIOMODESELECT_PREFERDTS)))
			{
				printf("*** Error *** You MUST specify either Dolby or DTS as preferred!\n");
				return -1;
			}
		}


		int lpcmChannels = cellAudioOutGetSoundAvailability( CELL_AUDIO_OUT_PRIMARY, CELL_AUDIO_OUT_CODING_TYPE_LPCM, CELL_AUDIO_OUT_FS_48KHZ, 0 );
		int ac3Channels = 0;
		int dtsChannels = 0;

		if(_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDOLBY)
			ac3Channels = cellAudioOutGetSoundAvailability( CELL_AUDIO_OUT_PRIMARY, CELL_AUDIO_OUT_CODING_TYPE_AC3, CELL_AUDIO_OUT_FS_48KHZ, 0 );

		if(_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDTS)
			dtsChannels = cellAudioOutGetSoundAvailability( CELL_AUDIO_OUT_PRIMARY, CELL_AUDIO_OUT_CODING_TYPE_DTS, CELL_AUDIO_OUT_FS_48KHZ, 0 );



		if(lpcmChannels == 8)
		{
			//8 channel LPCM output
			printf("[AUDIO] LPCM \tNo downmix \t8 channel\n");
			a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_LPCM;
			a_config.channel = 8;
			a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_NONE;
		}
		else
		{

			//Check whether Dolby or DTS is prefered AND has more channels compared to LPCM output 
			//(if 6 channel LPCM is available, then choose that as its a non-lossy format)
			int preferDolby = _flags & CELL_MS_AUDIOMODESELECT_PREFERDOLBY;
			if(preferDolby)
				preferDolby = ac3Channels > lpcmChannels ? 1 : 0;
			
			int preferDTS = _flags & CELL_MS_AUDIOMODESELECT_PREFERDTS;
			if(preferDTS)
				preferDTS = dtsChannels > lpcmChannels ? 1 : 0;

			if(lpcmChannels == 6 && !(preferDolby || preferDTS))
			{
				//6 channel LPCM output
				audDisplayf("[AUDIO] LPCM \t8->6 channel downmix \t6 channel");
				a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_LPCM;
				a_config.channel = lpcmChannels;
				a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_TYPE_B;
			}
			else
			{

				// If we support both Dolby and DTS then choose the preferred one 
				// If there is no preferred mode then choose 2 channel LPCM by default
				if((_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDOLBY) && (_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDTS))
				{
					//see if Dolby/DTS is preferred and has more than LPCM channels
					bool selectDolby = (_flags & CELL_MS_AUDIOMODESELECT_PREFERDOLBY) && (ac3Channels >= lpcmChannels);
					bool selectDTS = (_flags & CELL_MS_AUDIOMODESELECT_PREFERDTS) && (dtsChannels >= lpcmChannels);

					bool fallbackDolby = false;
					bool fallbackDTS = false;

					//Both Dolby and DTS are supported - Check to see if we can fallback to either Dolby/DTS.
					//Check that Dolby/DTS have more channels than LPCM.
					if(!selectDolby)
						fallbackDTS = (dtsChannels >= lpcmChannels);
					if(!selectDTS)
						fallbackDolby = (ac3Channels >= lpcmChannels);

					printf("[AUDIO] Select Dolby: %s    Select DTS: %s\n", selectDolby ? "Yes" : "No", selectDTS ? "Yes" : "No");
					printf("[AUDIO] Fallback Dolby: %s    Fallback DTS: %s\n", fallbackDolby ? "Yes" : "No", fallbackDTS ? "Yes" : "No");

					if(selectDolby || fallbackDolby)
					{
						//6 channel Dolby output
						audDisplayf("[AUDIO] Dolby \t8->6 channel downmix \t6 channel");
						a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_AC3;
						a_config.channel = ac3Channels;
						a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_TYPE_B;
						AC3On = 1;				
					}
					else if(selectDTS || fallbackDTS)
					{
						//6 channel DTS output
						audDisplayf("[AUDIO] DTS \t8->6 channel downmix \t6 channel");
						a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_DTS;
						a_config.channel = dtsChannels;
						a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_TYPE_B;
						DTSOn = 1;
					}
					else
					{
						//2 channel LPCM output
						audDisplayf("[AUDIO] No preferred bitstream mode available (check VSH settings), \nor no preferred bitstream specified (via flags)\n- Defaulting to: LPCM \t8->2 channel downmix \t2 channel");
						a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_LPCM;
						a_config.channel = 2;
						a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_TYPE_A;
					}
				}
				else 
				{
					// We dont support both Dolby and DTS, so just choose the selected one
					// If neither Dolby nor DTS is supported then select LPCM
					if((_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDOLBY) && ac3Channels >= lpcmChannels)
					{
						//6 channel Dolby output
						audDisplayf("[AUDIO] Dolby \t8->6 channel downmix \t6 channel");
						a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_AC3;
						a_config.channel = ac3Channels;
						a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_TYPE_B;
						AC3On = 1;
					}
					else if((_flags & CELL_MS_AUDIOMODESELECT_SUPPORTSDTS) && dtsChannels >= lpcmChannels)
					{
						//6 channel DTS output
						audDisplayf("[AUDIO] DTS \t8->6 channel downmix \t6 channel");
						a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_DTS;
						a_config.channel = dtsChannels;
						a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_TYPE_B;
						DTSOn = 1;
					}
					else
					{
						//2 channel LPCM output
						audDisplayf("[AUDIO] LPCM \t8->2 channel downmix \t2 channel");
						a_config.encoder = CELL_AUDIO_OUT_CODING_TYPE_LPCM;
						a_config.channel = 2;
						a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_TYPE_A;
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////
		// Rockstar specific change:
		// we handle the downmix ourselves so override the above downmix decision
		a_config.downMixer = CELL_AUDIO_OUT_DOWNMIXER_NONE;

		ret = cellAudioOutConfigure(CELL_AUDIO_OUT_PRIMARY, &a_config, NULL, 1); //(Change 1 to 0 for blocking)
		if (ret != CELL_OK)
		{
			return ret;
		}

		cnt=0;
		do {
			ret = cellAudioOutGetState(CELL_AUDIO_OUT_PRIMARY, 0, &a_state);
			if (ret != CELL_OK)
			{
				printf("*** Error *** cellAudioOutGetState failed!\n");
				return ret;
			}
			sys_timer_usleep(5000);
			cnt++;
			if (cnt==(200*3)) // 3 second passed?
			{
				printf("*** Error *** cellAudioOutGetState failed to find state!\n");
				return(-1);
			}

		} while (a_state.state != CELL_AUDIO_OUT_OUTPUT_STATE_ENABLED);


		// Bits 0-3 = number of channels. Bit 4 = AC3 On/Off, Bit 5 = DTS On/Off
		ret = a_config.channel;
		ret |= AC3On << 4; 
		ret |= DTSOn << 5; 

		return (ret);
	}

	void audMixerDevicePs3::ShutdownHardware()
	{
		cellAudioRemoveNotifyEventQueue(m_EventQueueKey);
		sys_event_queue_destroy(m_EventQueueId, SYS_EVENT_QUEUE_DESTROY_FORCE);
		while(m_IsThreadRunning)
		{
			sysIpcSleep(10);
		}
		cellAudioPortStop(m_PortId);
		cellAudioPortClose(m_PortId);
		m_PortId = ~0U;
		m_PortBuffer = NULL;
		cellAudioQuit();

		audDecoderSpu::ShutdownClass();
	}

	void audMixerDevicePs3::StartMixing()
	{
		Assert(!m_IsThreadRunning);
		cellAudioPortStart(m_PortId);
		enum {kMixThreadCpu = 1};
		sysIpcCreateThread(sMixLoop, this, sysIpcMinThreadStackSize ASSERT_ONLY(+ 4096), PRIO_TIME_CRITICAL, "[RAGE Audio] Mix thread", kMixThreadCpu);
	}

	void audMixerDevicePs3::MixLoop()
	{
		m_IsThreadRunning = true;

		bool finished = false;
		while(!finished)
		{
			sysPerformanceTimer mixTimer("mixTimer");
			mixTimer.Start();

			audDecoderVramHelper::ProcessFreeList();

			MixBuffers();

			Assert(m_NumOutputChannels == 6);
									
			mixTimer.Stop();

			sysPerformanceTimer waitTimer("waitTimer");
			waitTimer.Start();

			sys_event_t event;
			// 10ms timeout
			const s32 ret = sys_event_queue_receive(m_EventQueueId, &event, 10000);

			waitTimer.Stop();

#if !__FINAL && !__NO_OUTPUT
			if(!sysStack::HasExceptionBeenThrown() && mixTimer.GetTimeMS() + waitTimer.GetTimeMS() > 9.5f)
			{
				audWarningf("Long mixer frame: %f, wait time %f", mixTimer.GetTimeMS(), waitTimer.GetTimeMS());
			}
#endif

			if(ret == CELL_OK)
			{				
				ASSERT_ONLY(s32 ret = )
					cellAudioAddData(m_PortId, GetOutputBuffer(), kMixBufNumSamples, 1.f);
				audAssertf(ret == CELL_OK, "cellAudioAddData returned %d", ret);		
			}
			else if(ret == ECANCELED)
			{
				// shutting down the game cancels the event queue
				finished = true;
			}
			else
			{
				switch(ret)
				{
					case ESRCH:
					audErrorf("audMixerDevicePs3: sys_event_queue_receive returned ESRCH (%X)", ret);	
					break;
					case ECANCELED:
					audErrorf("audMixerDevicePs3: sys_event_queue_receive returned ECANCELED (%X)", ret);	
					break;
					case EINVAL:
					audErrorf("audMixerDevicePs3: sys_event_queue_receive returned EINVAL(%X)", ret);	
					break;
					case ETIMEDOUT:
					audErrorf("audMixerDevicePs3: sys_event_queue_receive returned ETIMEDOUT (%X)", ret);	
					break;
					default:
					audErrorf("audMixerDevicePs3: sys_event_queue_receive returned %X", ret);
					break;
				}
			
				//finished = true;
			}
		}

		m_IsThreadRunning = false;
	}

	DECLARE_THREAD_FUNC(audMixerDevicePs3::sMixLoop)
	{
		USE_MEMBUCKET(MEMBUCKET_AUDIO);
		((audMixerDevicePs3*)ptr)->MixLoop();
	}

	bool audMixerDevicePs3::IsPulseHeadsetConnected(const bool loadPRX /*=false*/) const
	{
		bool pulseHeadsetAvailable = false;
		CellAudioOutDeviceInfo2 deviceInfo[16];
		int err = -1;

		if(loadPRX)
		{
			err = cellSysmoduleLoadModule(CELL_SYSMODULE_AVCONF_EXT);
			if(err != CELL_OK)
			{
				audErrorf("Loading CELL_SYSMODULE_AVCONF_EXT returned %d", err);
				return false;
			}
		}
		if((err = cellAudioOutGetAvailableDeviceInfo(16, &deviceInfo[0])) >= 0)
		{
			for(int i = 0; i < err; i++)
			{
				/*audDisplayf("Audio output %d: \"%s\" is %s", i, deviceInfo[i].name, deviceInfo[i].state == CELL_AUDIO_OUT_DEVICE_STATE_AVAILABLE
																		? "Available" : "Unavailable");*/
				const char *pulseHeadsetName = "Wireless Stereo Headset";
				if(deviceInfo[i].state == CELL_AUDIO_OUT_DEVICE_STATE_AVAILABLE && strcmp(deviceInfo[i].name, pulseHeadsetName) == 0)
				{
					pulseHeadsetAvailable = true;
				}
			}
		}
		else
		{
			audErrorf("cellAudioOutGetAvailableDeviceInfo failed with error code %d", err);
		}

		
		if(loadPRX)
		{
			cellSysmoduleUnloadModule(CELL_SYSMODULE_AVCONF_EXT);
		}

		return pulseHeadsetAvailable;
	}
} // namespace rage

#endif // __PS3
