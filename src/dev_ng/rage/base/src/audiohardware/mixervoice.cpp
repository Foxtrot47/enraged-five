//
// audiohardware/mixervoice.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "mixervoice.h"
#include "device.h"
#include "driver.h"
#include "driverdefs.h"
#include "mixer.h"
#include "submix.h"
#include "pcmsource.h"
#include "math/amath.h"
#include "profile/profiler.h"
#include "system/cache.h"
#include "system/memops.h"

#include "mixing_vmath.inl"

EXT_PF_GROUP(MixerDevice);
PF_TIMER(MixerVoiceMix, MixerDevice);

namespace rage
{
#if !__SPU
	struct audSetOutputCommand : public audCommandPacketHeader
	{
		audSetOutputCommand()
		{
			packetSize = sizeof(audSetOutputCommand);
			commandId = audMixerVoice::SET_OUTPUT;
		}
		u8 submixId;
		s8 index;
	};

	struct audMixerVoiceSetPcmSourceCommand : public audCommandPacketHeader
	{
		audMixerVoiceSetPcmSourceCommand()
		{
			packetSize = sizeof(audMixerVoiceSetPcmSourceCommand);
			commandId = audMixerVoice::SET_PCM_SOURCE;
		}
		s16 pcmSourceId;
		s16 channelId;
	};

	struct audSetVoiceVolumesCommand : public audCommandPacketHeader
	{
		audSetVoiceVolumesCommand()
		{
			packetSize = sizeof(audSetVoiceVolumesCommand);
			commandId = audMixerVoice::SET_VOLUMES;
		}
		u32 index;
		u32 pad0;
		u32 pad1;
		Vec::Vector_4V packedVolumes;
	};

	struct audResetVoiceVolumesCommand : public audCommandPacketHeader
	{
		audResetVoiceVolumesCommand()
		{
			packetSize = sizeof(audResetVoiceVolumesCommand);
			commandId = audMixerVoice::RESET_VOLUMES;
		}
		u8 index;
	};

	struct audSetFiltersCommand : public audCommandPacketHeader
	{
		audSetFiltersCommand()
		{
			packetSize = sizeof(audSetFiltersCommand);
			commandId = audMixerVoice::SET_FILTERS;
		}
		u16 lpf;
		u16 hpf;
	};

#if !__NO_OUTPUT
void audMixerVoice::DebugTraceCommandPacket(const audCommandPacketHeader *packet)
{
	const char *commandName = "";
	const char *extraStr = "";
	char extraBuf[128];
	switch(packet->commandId)
	{
	case audMixerVoice::START:
		commandName = "START";
		break;
	case audMixerVoice::STOP:
		commandName = "STOP";
		break;
	case audMixerVoice::DISCONNECT_ALL_OUTPUTS:
		commandName = "DISCONNECT_ALL_OUTPUTS";
		break;
	case audMixerVoice::SET_VOLUMES:
		{
			commandName = "SET_VOLUMES";
			audSetVoiceVolumesCommand *command = (audSetVoiceVolumesCommand*)packet;
			Vector_4V v0,v1;
			audMixerConnection::UnpackVolumeMatrix(command->packedVolumes, v0, v1);
			extraStr = formatf(extraBuf, "%u: [%f,%f,%f,%f,%f,%f,%f,%f]", command->index, 
				GetX(v0),GetY(v0),GetZ(v0),GetW(v0),
				GetX(v1),GetY(v1),GetZ(v1),GetW(v1));
		}
		break;
	case audMixerVoice::SET_FILTERS:
		{
			commandName = "SET_FILTERS";
			audSetFiltersCommand *command = (audSetFiltersCommand*)packet;
			extraStr = formatf(extraBuf, "LPF: %u, HPF: %u", command->lpf, command->hpf);
		}
		break;
	case audMixerVoice::SET_PCM_SOURCE:
		{
			commandName = "SET_PCM_SOURCE";
			audMixerVoiceSetPcmSourceCommand *command = (audMixerVoiceSetPcmSourceCommand*)packet;
			extraStr = formatf(extraBuf, "PcmSource: %d:%d", command->pcmSourceId, command->channelId);
		}
		break;
	case audMixerVoice::SET_OUTPUT:
		{
			commandName = "SET_OUTPUT";
			audSetOutputCommand *command = (audSetOutputCommand*)packet;
			const bool invertPhase = (command->submixId>>7)==1;
			extraStr = formatf(extraBuf, "%u -> %u%s", command->index, command->submixId&0x7F, invertPhase ? " (invert phase)" : "");
		}
		break;
	case audMixerVoice::RESET_FILTERS:
		commandName = "RESET_FILTERS";
		break;
	case audMixerVoice::RESET_VOLUMES:
		{
			commandName = "RESET_VOLUMES";
			audResetVoiceVolumesCommand *command = (audResetVoiceVolumesCommand*)packet;
			extraStr = formatf(extraBuf, "Output %u", command->index);
		}		
		break;
	case audMixerVoice::INIT:
		{
			commandName = "INIT";
		}
	default:
		Assert(0);
		break;
	}

	audDisplayf("COMMAND_VOICE (%u) %s (%u) objectId: %u %s [size: %u bytes]", packet->systemId, commandName, packet->commandId, packet->objectId, extraStr, packet->packetSize);
}
#endif // !__NO_OUTPUT

audMixerVoice::audMixerVoice(void) :
m_State(STOPPED),
m_InputBufferId(audFrameBufferPool::InvalidId),
m_PcmSourceId(-1),
m_StopRequested(false),
m_HasPremixed(false)
{
	for(atFixedArray<u16, kMaxVoiceOutputs>::iterator i = m_ConnectionIds.begin(); i < m_ConnectionIds.end(); i++)
	{
		(*i) = audMixerConnection::INVALID;
	}
}

audMixerVoice::~audMixerVoice(void)
{
	// all outputs should have been disconnected already.
	for(atRangeArray<u16, kMaxVoiceOutputs>::iterator i = m_ConnectionIds.begin(); i < m_ConnectionIds.end(); i++)
	{
		Assert((*i) == audMixerConnection::INVALID);
	}
}

void audMixerVoice::Start()
{
	WriteCommand(START);
}

void audMixerVoice::Stop()
{
	WriteCommand(STOP);
}

void audMixerVoice::Init()
{
	WriteCommand(INIT);
}

void audMixerVoice::DisconnectAllOutputs()
{
	WriteCommand(DISCONNECT_ALL_OUTPUTS);
}

void audMixerVoice::SetOutput(const s32 index, const u32 submixId, const bool invertPhase)
{
	audSetOutputCommand command;
	command.packetSize = sizeof(command);

	u32 packedSubmixId = submixId & 0x7F;
	if(invertPhase)
	{
		packedSubmixId |= (1<<7);
	}
	Assign(command.submixId, packedSubmixId);
	Assign(command.index, index);
	WriteCommand(&command);
}

void audMixerVoice::SetOutputVolumes(u32 outputIndex, Vector_4V_In packedVolumes)
{
#define TEST_FOR_RESET 1
#if TEST_FOR_RESET
	if(V4IsEqualIntAll(packedVolumes, V4VConstant(V_ZERO)))
	{
		audResetVoiceVolumesCommand command;
		Assign(command.index, outputIndex);
		WriteCommand(&command);
	}
	else
#endif
	{	
		audSetVoiceVolumesCommand command;
		command.index = outputIndex;
	
		command.packedVolumes = packedVolumes;
		WriteAlignedCommand(&command);
	}
}

void audMixerVoice::SetPcmSource(const s32 pcmSourceId, const s32 channelId)
{
	// Add a reference on the PCM Source here to ensure it doesn't get deleted before SetPcmSourceInternal() is called on
	// the mixer thread.
	audPcmSourceInterface::AddRef(pcmSourceId);

	audMixerVoiceSetPcmSourceCommand command;
	Assign(command.pcmSourceId,pcmSourceId);
	Assign(command.channelId, channelId);
	WriteCommand(&command);
}

void audMixerVoice::SetFilters(const u32 lpfCutoff, const u32 hpfCutoff)
{
	if(lpfCutoff == kVoiceFilterLPFMaxCutoff && hpfCutoff == kVoiceFilterHPFMinCutoff)
	{
		WriteCommand(RESET_FILTERS);
	}
	else
	{
		audSetFiltersCommand command;
		Assign(command.lpf, lpfCutoff);
		Assign(command.hpf, hpfCutoff);
		WriteCommand(&command);
	}	
}

void audMixerVoice::WriteCommand(audMixerVoiceCommand command)
{
	audCommandPacketHeader packet;
	packet.commandId = command;
	packet.packetSize = sizeof(audCommandPacketHeader);
	WriteCommand(&packet);
}

void audMixerVoice::WriteCommand(audCommandPacketHeader *packet)
{
	packet->systemId = audMixerDevice::COMMAND_VOICE;
	packet->objectId = audDriver::GetMixer()->GetVoiceIndex(this);
	audDriver::GetMixer()->WriteCommand(packet);
}

bool audMixerVoice::WriteAlignedCommand(audCommandPacketHeader *packet)
{
	packet->objectId = audDriver::GetMixer()->GetVoiceIndex(this);
	packet->systemId = audMixerDevice::COMMAND_VOICE;
	return audDriver::GetMixer()->WriteAlignedCommand(packet);
}

void audMixerVoice::ExecuteCommand(const audCommandPacketHeader *cmdData)
{
	switch(cmdData->commandId)
	{
	case audMixerVoice::START:
		StartInternal();
		break;
	case audMixerVoice::STOP:
		StopInternal();
		break;
	case audMixerVoice::DISCONNECT_ALL_OUTPUTS:
		DisconnectAllOutputsInternal();
		break;
	case audMixerVoice::SET_VOLUMES:
		{
			audSetVoiceVolumesCommand *command = (audSetVoiceVolumesCommand*)cmdData;
			SetOutputVolumesInternal(command->index, command->packedVolumes);
		}
		break;
	case audMixerVoice::SET_FILTERS:
		{
			audSetFiltersCommand *command = (audSetFiltersCommand*)cmdData;
			SetFiltersInternal(command->lpf, command->hpf);
		}
		break;
	case audMixerVoice::SET_PCM_SOURCE:
		{
			audMixerVoiceSetPcmSourceCommand *command = (audMixerVoiceSetPcmSourceCommand*)cmdData;
			SetPcmSourceInternal(command->pcmSourceId, command->channelId);
		}
		break;
	case audMixerVoice::SET_OUTPUT:
		{
			audSetOutputCommand *command = (audSetOutputCommand*)cmdData;
			SetOutputInternal(command->index, command->submixId);
		}
		break;
	case audMixerVoice::RESET_FILTERS:
		// Bypass clamps and conditionals; set state directly
		m_IsHPFActive =	m_IsLPFActive = false;
		m_Cutoffs[0] = float(kVoiceFilterLPFMaxCutoff);
		m_Cutoffs[1] = float(kVoiceFilterHPFMinCutoff);
		break;
	case audMixerVoice::RESET_VOLUMES:
		{
			audResetVoiceVolumesCommand *command = (audResetVoiceVolumesCommand*)cmdData;
			SetOutputVolumesInternal(command->index, V4VConstant(V_ZERO));
		}
		break;
	case audMixerVoice::INIT:
			InitInternal();
			break;
	default:
		Assert(0);
		break;
	}
}

void audMixerVoice::DisconnectAllOutputsInternal()
{
	ASSERT_ONLY(u32 outputsRemoved = 0);
	for(atRangeArray<u16, kMaxVoiceOutputs>::iterator i = m_ConnectionIds.begin(); i < m_ConnectionIds.end(); i++)
	{
		if((*i) != audMixerConnection::INVALID)
		{
			ASSERT_ONLY(outputsRemoved++);
			audMixerConnection::DestroyConnection(*i);
			(*i) = audMixerConnection::INVALID;
		}
	}
}

void audMixerVoice::SetOutputVolumesInternal(u32 outputIndex, Vector_4V_In packedVolumes)
{
	m_Volumes[outputIndex].Current = packedVolumes;
}

void audMixerVoice::SetPcmSourceInternal(const s32 pcmSourceId, const s32 channelId)
{
	if(m_PcmSourceId != -1)
	{
		GetPcmSource()->StopPhys(m_PcmSourceChannel);
		audPcmSourceInterface::Release(m_PcmSourceId);
		m_PcmSourceId = -1;
	}

	Assign(m_PcmSourceId, pcmSourceId);
	Assign(m_PcmSourceChannel, channelId);
}
 
BANK_ONLY(extern u32 g_OverrideLPFCutoff);
BANK_ONLY(extern u32 g_OverrideHPFCutoff);
void audMixerVoice::SetFiltersInternal(const u32 lpfCutoff, const u32 hpfCutoff)
{
#if __BANK
	const u32 lpf = Min(lpfCutoff, g_OverrideLPFCutoff);
	const u32 hpf = Max(hpfCutoff, g_OverrideHPFCutoff);
#else
	const u32 lpf = lpfCutoff;
	const u32 hpf = hpfCutoff;
#endif

	m_IsHPFActive = hpf > kVoiceFilterHPFMinCutoff;
	m_IsLPFActive = lpf < kVoiceFilterLPFMaxCutoff;

	m_Cutoffs[0] = static_cast<f32>(Clamp<u32>(lpf, kVoiceFilterLPFMinCutoff, kVoiceFilterLPFMaxCutoff));
	m_Cutoffs[1] = static_cast<f32>(Clamp<u32>(hpf, kVoiceFilterHPFMinCutoff, kVoiceFilterHPFMaxCutoff));
}

void audMixerVoice::SetOutputInternal(const s32 index, const u32 submixId)
{
	Assert(GetState() != PLAYING);

	const bool invertPhase = (submixId>>7)==1;
	audMixerSubmix *submix = audDriver::GetMixer()->GetSubmixFromIndex(submixId&0x7F);

	Assert(submix->HasVoiceInputs());

	if(Verifyf(m_ConnectionIds[index] == audMixerConnection::INVALID, "Attempting to replace an active voice output (%d -> %s)", index, submix->GetName()))
	{
		audMixerConnection *connection = audMixerConnection::Connect(this, submix, invertPhase);
		if(Verifyf(connection, "Failed to allocate connection object for voice->submix %s", submix->GetName()))
		{
			Assign(m_ConnectionIds[index], audMixerConnection::GetConnectionId(connection));
			m_Volumes[index].Current = m_Volumes[index].Previous = V4VConstant(V_ZERO);
		}
	}
}

void audMixerVoice::StartInternal()
{
#if __DEV
	if(m_State == PLAYING)
	{
		static bool haveTraced = false;
		if(!haveTraced)
		{
			audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
			haveTraced = true;
		}
		audAssertf(m_State != PLAYING, "MixerVoice told to start when already playing, id %d", audDriver::GetMixer()->GetVoiceIndex(this));
	}
	if(m_PcmSourceId == -1)
	{
		static bool haveTraced = false;
		if(!haveTraced)
		{
			audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
			haveTraced = true;
		}
		audAssertf(m_PcmSourceId != -1, "MixerVoice told to start with no pcm generator, id %d", audDriver::GetMixer()->GetVoiceIndex(this));
	}
	if(!g_PcmSourcePool->IsSlotInitialised(m_PcmSourceId))
	{
		static bool haveTraced = false;
		if(!haveTraced)
		{
			audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
			haveTraced = true;
		}
		audAssertf(g_PcmSourcePool->IsSlotInitialised(m_PcmSourceId), "MixerVoice told to start with invalid pcm generator id, voice id %d pcm generator id %d", audDriver::GetMixer()->GetVoiceIndex(this), m_PcmSourceId);
	}
#endif

	// Ensure commands are actioned in order; if a stop has been 'actioned' prior to this start, then cancel the stop.
	m_StopRequested = false;

	if(m_PcmSourceId == -1)
	{
		m_State = STOPPED;
		return;
	}

	audPcmSource *pcmSource = GetPcmSource();
	pcmSource->StartPhys(m_PcmSourceChannel);

#if __ASSERT
	if (!pcmSource->IsFinished() && !pcmSource->HasBuffer(m_PcmSourceChannel))
	{
		audPcmSource::AssetInfo info;
		pcmSource->GetAssetInfo(info);		
		audAssertf(false, "PCM Source does not have a buffer for source channel %u (Type: %d, Asset: %u)", m_PcmSourceChannel, pcmSource->GetTypeId(), info.assetNameHash);
	}	
#endif	

	// Cache the Id of the buffer that is being populated by our source PCM generator
	// This won't change while this PCM generator is playing physically
	m_InputBufferId = pcmSource->GetBufferId(m_PcmSourceChannel);

	// copy initial (current) volumes to previous volumes to prevent any fading
	for(u32 i = 0; i < kMaxVoiceOutputs; i++)
	{
		if(m_ConnectionIds[i] != audMixerConnection::INVALID)
		{
			m_Volumes[i].Previous = m_Volumes[i].Current;
		}
	}

	m_State = PLAYING;
}

void audMixerVoice::StopInternal()
{
	m_StopRequested = true;
}

void audMixerVoice::InitInternal()
{
	if(m_PcmSourceId != -1)
	{
		GetPcmSource()->StopPhys(m_PcmSourceChannel);
		audPcmSourceInterface::Release(m_PcmSourceId);
		m_PcmSourceId = -1;
	}
	m_State = STOPPED;
	m_StopRequested = false;
	m_HasPremixed = false;
		
#if AUD_USE_ONEPOLE_V4
	m_PrevSampleLPF = V4VConstant(V_ZERO);
	m_PrevSampleHPF = V4VConstant(V_ZERO);
#else
	m_PrevSample = V4VConstant(V_ZERO);
#endif
}

void audMixerVoice::Premix()
{
	if(audVerifyf(m_PcmSourceId != -1, "Invalid PCM source!"))
    {
	    audPcmSource *pcmSource = GetPcmSource();
	    if(!pcmSource->IsFinished())
	    {
		    pcmSource->BeginFrame();
	    }
	    m_HasPremixed = true;
    }
}

void audMixerVoice::PostMix()
{
	if(m_HasPremixed)
	{
		audPcmSource *pcmSource = GetPcmSource();
		if(pcmSource->IsFinished())
		{
			m_State = STOPPED;
			pcmSource->StopPhys(m_PcmSourceChannel);
			audPcmSourceInterface::Release(m_PcmSourceId);
			m_PcmSourceId = -1;
		}
		else
		{
			pcmSource->EndFrame();
		}
	
		if(m_PcmSourceId != -1 && m_StopRequested)
		{
			m_State = STOPPED;	
			pcmSource->StopPhys(m_PcmSourceChannel);
			audPcmSourceInterface::Release(m_PcmSourceId);
			m_PcmSourceId = -1;
		}

		m_HasPremixed = false;
	}
}

audPcmSource *audMixerVoice::GetPcmSource() const
{
	TrapLT(m_PcmSourceId, 0);
	return audDriver::GetMixer()->GetPcmSource(m_PcmSourceId);
}

#endif // !__SPU

#if __BANK
extern bool g_DisableVoiceFilters;
extern bool g_DisableAllVoiceProcessing;
#endif

ALIGNAS(128) f32 g_VoiceMixBuffer[kMixBufNumSamples];  // CACHE_ALIGNED??

void audMixerVoice::Mix()
{
	PF_FUNC(MixerVoiceMix);
	// Prepare our PCM data - apply voice filtering and envelope as required

	if(m_InputBufferId >= audFrameBufferPool::InvalidId)
	{
		// Our PCM generator has finished
		return;
	}
	const Vector_4V cutoffs = *(Vector_4V*)m_Cutoffs;
	const f32 *pcmData = g_FrameBufferCache->GetBuffer_ReadOnly(m_InputBufferId);

#if __BANK
	if(g_DisableAllVoiceProcessing)
	{
		// debug mode - simply pass PCM data through to all routes at unity gain
		for(u32 i = 0; i < kMaxVoiceOutputs; i++)
		{
			if(m_ConnectionIds[i] != audMixerConnection::INVALID)
			{
				audMixerConnection *connection = audMixerConnection::GetConnectionFromId(m_ConnectionIds[i]);
				audMixerSubmix *submix = audMixerSubmix::GetSubmixFromIndex(connection->GetOutput());
				submix->AllocateBuffers();

				const u32 numOutputs = connection->GetNumOutputs();
				for(u32 k = 0; k < numOutputs; k++)
				{
					if (submix->GetMixBuffer(k))
					{
						sysMemCpy128(submix->GetMixBuffer(k), pcmData, kMixBufNumSamples*sizeof(f32));
					}
				}
			}
		}
		return;
	}

	const bool areFiltersEnabled = !g_DisableVoiceFilters;
#else
	const bool areFiltersEnabled = true;
#endif

	if((m_IsLPFActive || m_IsHPFActive) && areFiltersEnabled)
	{
		// Compute coefficients
		//-2 * PI * freq / Fs
		// = (-2 * PI * 1/Fs) * freq
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const Vector_4V freqScalar = V4VConstant<0xB909421E,0xB909421E,0xB909421E,0xB909421E>();

		const Vector_4V x = audDriverUtil::V4ExpE(V4Scale(cutoffs,freqScalar));
		const Vector_4V a0_ = V4Subtract(V4VConstant(V_ONE),x);

#if AUD_USE_ONEPOLE_V4
		const Vector_4V b1_ = x;

		Vector_4V prevSampleLPF = m_PrevSampleLPF;
		Vector_4V prevSampleHPF = m_PrevSampleHPF;
#else
		const Vector_4V b1_ = V4Scale(V4VConstant(V_NEGONE),x);

		Vector_4V prevSampleLPF = V4SplatX(m_PrevSample);
		Vector_4V prevSampleHPF = V4SplatY(m_PrevSample);
#endif

		const Vector_4V lpf_a0 = V4SplatX(a0_);
		const Vector_4V lpf_b1 = V4SplatX(b1_);
		const Vector_4V hpf_a0 = V4SplatY(a0_);
		const Vector_4V hpf_b1 = V4SplatY(b1_);

		const Vector_4V *RESTRICT inputBuffer = (Vector_4V*)pcmData;
		Vector_4V *outputBuffer = (Vector_4V*)g_VoiceMixBuffer;

		pcmData = g_VoiceMixBuffer;

		// Prepare cache
		for(u32 i = 0; i < kMixBufNumSamples*sizeof(f32); i += 128)
		{
			ZeroDC(g_VoiceMixBuffer, i);
		}

		if(Likely(!m_IsHPFActive))
		{
			// LPF only
			for(u32 i = 0; i < kMixBufNumSamples; i += 4)
			{
				Vector_4V inputSamples = *inputBuffer++;
#if AUD_USE_ONEPOLE_V4
				Vector_4V outputX = OnePoleLPF_V4(V4SplatX(inputSamples), lpf_a0, lpf_b1, prevSampleLPF);
				Vector_4V outputY = OnePoleLPF_V4(V4SplatY(inputSamples), lpf_a0, lpf_b1, outputX);
				Vector_4V outputZ = OnePoleLPF_V4(V4SplatZ(inputSamples), lpf_a0, lpf_b1, outputY);
				Vector_4V outputW = OnePoleLPF_V4(V4SplatW(inputSamples), lpf_a0, lpf_b1, outputZ);

				prevSampleLPF = outputW;

				Vector_4V outputXY = V4PermuteTwo<X1,X2,X1,X2>(outputX, outputY);
				Vector_4V outputZW = V4PermuteTwo<X1,X2,X1,X2>(outputZ, outputW);
				*outputBuffer++ = V4PermuteTwo<X1,Y1,X2,Y2>(outputXY, outputZW);
#else
				*outputBuffer++ = OnePoleLPF(inputSamples, lpf_a0, lpf_b1, prevSampleLPF);
#endif
			}
		}
		else
		{
			// LPF + HPF
			for(u32 i = 0; i < kMixBufNumSamples; i += 4)
			{
				Vector_4V inputSamples = *inputBuffer++;
#if AUD_USE_ONEPOLE_V4
				Vector_4V outputX = OnePoleLPF_V4(V4SplatX(inputSamples), lpf_a0, lpf_b1, prevSampleLPF);
				Vector_4V outputY = OnePoleLPF_V4(V4SplatY(inputSamples), lpf_a0, lpf_b1, outputX);
				Vector_4V outputZ = OnePoleLPF_V4(V4SplatZ(inputSamples), lpf_a0, lpf_b1, outputY);
				Vector_4V outputW = OnePoleLPF_V4(V4SplatW(inputSamples), lpf_a0, lpf_b1, outputZ);

				prevSampleLPF = outputW;

				prevSampleHPF = OnePoleLPF_V4(outputX, hpf_a0, hpf_b1, prevSampleHPF);
				outputX = V4Subtract(outputX, prevSampleHPF);
				prevSampleHPF = OnePoleLPF_V4(outputY, hpf_a0, hpf_b1, prevSampleHPF);
				outputY = V4Subtract(outputY, prevSampleHPF);
				prevSampleHPF = OnePoleLPF_V4(outputZ, hpf_a0, hpf_b1, prevSampleHPF);
				outputZ = V4Subtract(outputZ, prevSampleHPF);
				prevSampleHPF = OnePoleLPF_V4(outputW, hpf_a0, hpf_b1, prevSampleHPF);
				outputW = V4Subtract(outputW, prevSampleHPF);

				Vector_4V outputXY = V4PermuteTwo<X1,X2,X1,X2>(outputX, outputY);
				Vector_4V outputZW = V4PermuteTwo<X1,X2,X1,X2>(outputZ, outputW);
				*outputBuffer++ = V4PermuteTwo<X1,Y1,X2,Y2>(outputXY, outputZW);

#else
				// LPF
				const Vector_4V lowpassOutput = OnePoleLPF(inputSamples, lpf_a0, lpf_b1, prevSampleLPF);
				// HPF = (x-lpf(x))
				const Vector_4V highpassOutput = V4Subtract(lowpassOutput, OnePoleLPF(lowpassOutput, hpf_a0, hpf_b1, prevSampleHPF));

				*outputBuffer++ = highpassOutput;
#endif
			}
		}

#if AUD_USE_ONEPOLE_V4
		m_PrevSampleHPF = prevSampleHPF;
		m_PrevSampleLPF = prevSampleLPF;
#else
		m_PrevSample = V4PermuteTwo<X1,X2,X1,X2>(prevSampleLPF, prevSampleHPF);
#endif
	}
	else
	{
		// Zero output history
#if AUD_USE_ONEPOLE_V4
		m_PrevSampleLPF = m_PrevSampleHPF = V4VConstant(V_ZERO);
#else
		m_PrevSample = V4VConstant(V_ZERO);
#endif
	}

	// Check for INF filter state
	Vector_4V finiteCheck = V4Scale(m_PrevSampleLPF, m_PrevSampleHPF);
	if(!V4IsFiniteAll(finiteCheck))
	{
		sysMemSet(const_cast<float*>(pcmData), 0, kMixBufNumSamples * sizeof(float));
		m_PrevSampleHPF = m_PrevSampleLPF = V4VConstant(V_ZERO);

#if !__FINAL
		audErrorf("%u: Voice generating non-finite data: %p.  PCM Source: %d (%p)", audDriver::GetMixer()->GetMixerTimeFrames(), this, m_PcmSourceId, GetPcmSource());
		audPcmSource *source = GetPcmSource();
		audPcmSource::AssetInfo info;
		source->GetAssetInfo(info);
		audErrorf("%u: Type: %d, asset info: %u, %u", audDriver::GetMixer()->GetMixerTimeFrames(), source->GetTypeId(), info.assetNameHash, info.slotId);
#endif
	}

	// Release envelope
	if(m_StopRequested)
	{
		// Fade from 1 to 0 over one mix frame (256 samples)
		CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);
				
		Vector_4V step1 = kMixBufNumSamples == 256
			// {255/256, 254/256, 253/256, 252/256}
			? V4VConstant<0x3F7F0000,0x3F7E0000,0x3F7D0000,0x3F7C0000>()
		
			// {127/128, 126/128, 125/128, 124/128}	
			: V4VConstant<0x3f7e0000, 0x3f7c0000, 0x3f7a0000, 0x3f780000>();
	
		Vector_4V step2 = kMixBufNumSamples == 256 
				// {251/256, 250/256, 249/256, 248/256}
				? V4VConstant<0x3F7B0000,0x3F7A0000,0x3F790000,0x3F780000>()
				//123/128, 122/128, 121/128, 120/128
				: V4VConstant<0x3f760000, 0x3f740000, 0x3f720000, 0x3f700000>();
		
		const Vector_4V stepX8 = kMixBufNumSamples == 256 ? V4VConstantSplat<0xBD000000>() // (1/256)*-8
														: V4VConstantSplat<0xbd800000>(); // (1/128) * -8
		Vector_4V *inputBuffer = (Vector_4V*)pcmData;
		Vector_4V *outputBuffer = (Vector_4V*)g_VoiceMixBuffer;
		pcmData = g_VoiceMixBuffer;

		if(inputBuffer != outputBuffer)
		{
			// Prepare cache if this code is first to write to g_VoiceMixBuffer
			for(u32 i = 0; i < kMixBufNumSamples*sizeof(f32); i += 128)
			{
				ZeroDC(g_VoiceMixBuffer, i);
			}
		}

		for(u32 i = 0; i < kMixBufNumSamples; i += 8)
		{
			Vector_4V samples0 = *inputBuffer++;	
			Vector_4V samples1 = *inputBuffer++;		

			// apply interpolated scalar
			samples0 = V4Scale(samples0, step1);

			// step through gain interpolation
			step1 = V4Add(step1, stepX8);

			samples1 = V4Scale(samples1, step2);

			// step through gain interpolation
			step2 = V4Add(step2, stepX8);	

			*outputBuffer++ = samples0;
			*outputBuffer++ = samples1;
		}
	}

#define AUD_COMPUTE_PEAK_LEVEL 0
#if AUD_COMPUTE_PEAK_LEVEL
	const float peakVal = ComputePeakLevel<kMixBufNumSamples>(pcmData);
	float outputPeakSum = 0.f;
#endif

#if 0
	const float peakVal = ComputePeakLevel<kMixBufNumSamples>(pcmData);
	if(peakVal > 1.1f)
	{
		audWarningf("Voice generating data > 0dB: %p, %f.  PCM Source: %d (%p)", this, peakVal, m_PcmSourceId, GetPcmSource());
		audPcmSource *source = GetPcmSource();
		audPcmSource::AssetInfo info;
		source->GetAssetInfo(info);
		audWarningf("Type: %d, asset info: %u, %u", source->GetTypeId(), info.assetNameHash, info.slotId);
	}
	//const float meanLevel = ComputeMeanLevel<kMixBufNumSamples>(pcmData);
	/*if(Abs(meanLevel) > 0.18f)
	{
		audWarningf("Voice generating DC offset of %f", meanLevel);
	}*/
#endif

	// Route this data into our destination submixes

	for(u32 i = 0; i < kMaxVoiceOutputs; i++)
	{
		if(m_ConnectionIds[i] != audMixerConnection::INVALID)
		{
			audMixerConnection *connection = audMixerConnection::GetConnectionFromId(m_ConnectionIds[i]);
			audMixerSubmix *submix = audMixerSubmix::GetSubmixFromIndex(connection->GetOutput());

			if(submix->GetProcessingStage() >= 0)
			{
				Vector_4V current0, current1 = V4VConstant(V_ZERO);
				Vector_4V prev0, prev1 = V4VConstant(V_ZERO);

				audMixerConnection::UnpackVolumeMatrix(m_Volumes[i].Current, current0, current1);
				audMixerConnection::UnpackVolumeMatrix(m_Volumes[i].Previous, prev0, prev1);

				if(connection->ShouldInvertPhase())
				{
					current0 = V4Scale(current0, V4VConstant(V_NEGONE));
					current1 = V4Scale(current1, V4VConstant(V_NEGONE));
					prev0 = V4Scale(prev0, V4VConstant(V_NEGONE));
					prev1 = V4Scale(prev1, V4VConstant(V_NEGONE));
				}
							
				submix->AllocateBuffers();
				submix->IncrementVoiceCount();

				const u32 numOutputs = connection->GetNumOutputs();

				if(submix->HasAllocatedBuffers())
				{
					if(submix->GetBufferFormat() == Noninterleaved)
					{				
						if(numOutputs == 1)
						{			
							if (submix->GetMixBuffer(0))
							{
								MixMonoBufToMonoBuf<kMixBufNumSamples>(
									pcmData,
									submix->GetMixBuffer(0),
									V4SplatX(prev0),
									V4SplatX(current0));
							}

#if AUD_COMPUTE_PEAK_LEVEL
								outputPeakSum += peakVal * GetX(current0);
#endif
						}
						else
						{		
							Assert(numOutputs == 2 || numOutputs == 4 || numOutputs == 6 || numOutputs == 8);

							if(numOutputs == 2)
							{
								if (submix->GetMixBuffer(0))
								{
									MixMonoBufToMonoBuf<kMixBufNumSamples>(
										pcmData,
										submix->GetMixBuffer(0),
										V4SplatX(prev0),
										V4SplatX(current0));
								}
								if (submix->GetMixBuffer(1))
								{
									MixMonoBufToMonoBuf<kMixBufNumSamples>(
										pcmData,
										submix->GetMixBuffer(1),
										V4SplatY(prev0),
										V4SplatY(current0));
								}
#if AUD_COMPUTE_PEAK_LEVEL
								outputPeakSum += peakVal * GetX(current0) + peakVal * GetY(current0);
#endif
							}
							else
							{
								// 4, 6 or 8 channel
								if (submix->GetMixBuffer(0))
								{
									MixMonoBufToMonoBuf<kMixBufNumSamples>(
										pcmData,
										submix->GetMixBuffer(0),
										V4SplatX(prev0),
										V4SplatX(current0));
								}
								if (submix->GetMixBuffer(1))
								{
									MixMonoBufToMonoBuf<kMixBufNumSamples>(
										pcmData,
										submix->GetMixBuffer(1),
										V4SplatY(prev0),
										V4SplatY(current0));
								}
								if (submix->GetMixBuffer(2))
								{
									MixMonoBufToMonoBuf<kMixBufNumSamples>(
										pcmData,
										submix->GetMixBuffer(2),
										V4SplatZ(prev0),
										V4SplatZ(current0));
								}
								if (submix->GetMixBuffer(3))
								{
									MixMonoBufToMonoBuf<kMixBufNumSamples>(
										pcmData,
										submix->GetMixBuffer(3),
										V4SplatW(prev0),
										V4SplatW(current0));
								}

#if AUD_COMPUTE_PEAK_LEVEL
								outputPeakSum += peakVal * GetX(current0) + 
												peakVal * GetY(current0) +
												peakVal * GetZ(current0) +
												peakVal * GetW(current0);
#endif
								// Move onto second half of matrix
							
								if(numOutputs >= 6)
								{
									if (submix->GetMixBuffer(4))
									{
										MixMonoBufToMonoBuf<kMixBufNumSamples>(
											pcmData,
											submix->GetMixBuffer(4),
											V4SplatX(prev1),
											V4SplatX(current1));
									}
									if (submix->GetMixBuffer(5))
									{
										MixMonoBufToMonoBuf<kMixBufNumSamples>(
											pcmData,
											submix->GetMixBuffer(5),
											V4SplatY(prev1),
											V4SplatY(current1));
									}

		#if AUD_COMPUTE_PEAK_LEVEL
										outputPeakSum += peakVal * GetX(current1) + peakVal * GetY(current1);
		#endif

									Assert(numOutputs <= 6);
									/*
									if(numOutputs == 8)
									{
										if (submix->GetMixBuffer(0))
										{
											MixMonoBufToMonoBuf<kMixBufNumSamples>(
												pcmData,
												submix->GetMixBuffer(6),
												V4SplatZ(prev1),
												V4SplatZ(current1));
										}
										if (submix->GetMixBuffer(0))
										{
											MixMonoBufToMonoBuf<kMixBufNumSamples>(
											pcmData,
											submix->GetMixBuffer(7),
											V4SplatW(prev1),
											V4SplatW(current1));
										}
#if AUD_COMPUTE_PEAK_LEVEL
										outputPeakSum += peakVal * GetZ(current1) + peakVal * GetW(current1);
#endif
									}
									*/
								}
							}
						}
					}
					else
					{
						// Destination is interleaved format
						audAssertf(numOutputs == 4, "Unsupported number of channels for interleaved mix buffer: %u", numOutputs);
				
						if (submix->GetMixBuffer(0) && submix->GetMixBuffer(1) && submix->GetMixBuffer(2) && submix->GetMixBuffer(3))
						{
							InterleaveAndMix_1to4<kMixBufNumSamples>(	prev0, 
								current0, 
								pcmData,
								submix->GetMixBuffer(0),
								submix->GetMixBuffer(1),
								submix->GetMixBuffer(2),
								submix->GetMixBuffer(3)
								);
						}
					}
				}

				m_Volumes[i].Previous = m_Volumes[i].Current;
			}
		}
	}
}

} // namespace rage
