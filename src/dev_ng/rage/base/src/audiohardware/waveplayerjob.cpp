// 
// audiohardware/waveplayerjob.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DISABLE_ASSERTS
#define AUD_DISABLE_ASSERTS 0
#endif

#if AUD_DISABLE_ASSERTS
#undef __ASSERT
#define __ASSERT 0
#undef ASSERT_ONLY
#define ASSERT_ONLY(x)


#undef Assert
#define Assert(x)
#undef Assertf
#define Assertf(x,...)
#undef FastAssert
#define FastAssert(x)
#undef AssertMsg
#define AssertMsg(x,msg)
#undef AssertVerify
#define AssertVerify(x) (x)
#undef Verifyf
#define Verifyf(x,fmt,...) (x)
#undef DebugAssert
#define DebugAssert(x)
#endif

#include "system/taskheader.h"

#if __SPU

#include "framebuffercache.h"
#include "mixer.h"
#include "waveplayerjob.h"

#include "pcmsource.h"
#include "pcmsourcejob.h"
#include "audioengine/spuutil.h"

#include "math/amath.h"
#include "system/dma.h"

namespace rage 
{	
	bool g_DisableSampleRateConversion = false;
	
	void *g_Mp3DecoderMain = NULL;
	void *g_Mp3DecoderWorkspace = NULL;
	extern u8 *g_ScratchDecoder;
	extern s16 *g_ScratchLoadBuffer;
	extern size_t g_ScratchLoadBufferSize;
	extern u8 *g_SpuFetchBuffer;
	extern size_t g_SpuFetchBufferSize;

#define CHECK_STACK 0 //!__FINAL

#if CHECK_STACK
	const volatile void *GetStackBottom()
	{
		qword t0;
		__asm__ __volatile__
		(
			"rotqbyi    %0,$1,4\n\t"
			"sf         %0,%0,$1"
			: "=r"(t0)
		);
		return si_to_ptr(t0);
	}
#endif // CHECK_STACK

	void WavePlayerJobEntry(sysTaskParameters &p)
	{
#if __BANK
	g_DisableSampleRateConversion = p.UserData[7].asBool;
#endif
		InitScratchBuffer(p.Scratch.Data, p.Scratch.Size);

		// Hook up MP3 decoder state
		g_Mp3DecoderMain = p.UserData[4].asPtr;
		
		g_ScratchLoadBuffer = (s16*)AllocateFromScratch(g_ScratchLoadBufferSize, 16, "ScratchLoadBuffer");
		g_SpuFetchBuffer = (u8*)AllocateFromScratch(g_SpuFetchBufferSize, 16, "SpuFetchBuffer");

#if CHECK_STACK
		u8 *stackBottom = (u8*)GetStackBottom();
		for(u32 i = 0; i < 128; i++)
		{
			*(stackBottom + i) = (u8)i;
		}
#endif // CHECK_STACK

		enum {kNumPcmBuffers = 2};

		{
			audAutoScratchBookmark scratchBookmark;

			g_ScratchDecoder = (u8*)AllocateFromScratch(sizeof(audDecoderSpu), 128, "ScratchDecoder");
			// Allocate MP3 decoder workspace		
			g_Mp3DecoderWorkspace = AllocateFromScratch(CELL_MSMP3_SPU_WORKSPACE_SIZE, 128, "MP3 Decoder workspace");

			ProcessPcmSourcePool<audWavePlayer, AUD_PCMSOURCE_WAVEPLAYER>(p, kNumPcmBuffers);
		}

		{
			audAutoScratchBookmark scratchBookmark;
			ProcessPcmSourcePool<audStreamPlayer, AUD_PCMSOURCE_EXTERNALSTREAM>(p, kNumPcmBuffers);
		}

		{
			audPcmSourcePool *pool = (audPcmSourcePool*)p.Input.Data;

			// Generate list of sources to delete
			const u32 maxDeleted = p.Output.Size >> 1; // 2 bytes per entry
			u32 numDeleted = 0;
			u16 *deletedList = reinterpret_cast<u16*>(p.Output.Data);

			for(u32 slotIndex = 0; slotIndex < pool->GetNumSlots(); slotIndex++)
			{
				if(pool->IsSlotInitialised(slotIndex) && pool->GetRefCount(slotIndex) == 0)
				{
					Assign(deletedList[numDeleted++], slotIndex);
					if(numDeleted == maxDeleted)
					{
						break;
					}
				}	
			}
			// Invalidate any empty entries in the deleted list
			for(u32 i = numDeleted; i < maxDeleted; i++)
			{
				deletedList[i] = 0xffff;
			}
		}

#if CHECK_STACK
		for(u32 i = 0; i < 128; i++)
		{
			if(*(stackBottom + i) != (u8)i)
			{
				audErrorf("stackBottom + %02X corrupted", i);
				__debugbreak();
			}
		}
#endif // CHECK_STACK
	}
} // namespace rage	

#endif // __SPU

using namespace rage;
void waveplayerjob(sysTaskParameters &SPU_ONLY(p))
{
	SPU_ONLY(rage::WavePlayerJobEntry(p));
}
