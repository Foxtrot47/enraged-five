//
// audiohardware/decoder_pcm.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DECODER_PCM_H
#define AUD_DECODER_PCM_H

#include "decoder.h"
#include "ringbuffer.h"

namespace rage
{

enum {kPcmDecoderBufferBytes = 1024};
enum {kPcmDecoderWorkingBufferSamples = 128};
#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
class ALIGNAS(16) audDecoderPcm : public audDecoder
{
public:
	
	virtual bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate));
	virtual void Shutdown();

	virtual audDecoderState GetState() const
	{
		return m_State;
	}
	virtual void SubmitPacket(audPacket &packet);

	virtual u32 ReadData(s16 *dest, const u32 numSamples) const;
	virtual void AdvanceReadPtr(const u32 numSamples);
	virtual u32 QueryNumAvailableSamples() const;

	virtual bool QueryReadyForMoreData() const;

	virtual void SetWaveIdentifier(const u32 waveIdentifier) { m_WaveIdentifier = waveIdentifier; }
private:

	template<const bool swapEndian> void Convert(const u32 *RESTRICT input, const u32 numSamples);
	template<const bool swapEndian> void Convert(const s16 *RESTRICT input, const u32 numSamples);

	audStaticRingBuffer<kPcmDecoderBufferBytes> m_RingBuffer;
	audDecoderState m_State;
	audWaveFormat::audStreamFormat m_Format;
	u32 m_NumChannels;
	u32 m_WaveIdentifier;
	ALIGNAS(16) s16 m_WorkingBuffer[kPcmDecoderWorkingBufferSamples] ;
};
#if __WIN32
#pragma warning(pop)
#endif

} // namespace rage

#endif // AUD_DECODER_PCM_H
