// 
// audiohardware/vramhelper.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
//

#if __PS3

#include "channel.h"
#include "vramhelper.h"
#include "grcore/wrapper_gcm.h"
#include "math/amath.h"
#include "profile/profiler.h"
#include "system/spinlock.h" // for sys_lwsync

#include "audioengine/engineutil.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"

PF_PAGE(VRAMHelper, "RAGE Audio: VRAM Helper");
PF_GROUP(VRAMHelper);
PF_LINK(VRAMHelper,VRAMHelper);
PF_VALUE_INT(FetchTimeMs, VRAMHelper);
PF_VALUE_INT(FetchTimeFrames, VRAMHelper);
PF_VALUE_INT(NumFetchBuffersAllocated, VRAMHelper);
PF_VALUE_INT(NumDeferredFrees, VRAMHelper);
PF_COUNTER(NumFetchRequestsDenied, VRAMHelper);
PF_COUNTER(NumFetches, VRAMHelper);
namespace rage
{

BANK_ONLY(bool g_DebugDelayAudioGPU = false);

#if !__SPU

atFixedArray<audDecoderVramHelper::audVramBufferPair, audDecoderVramHelper::kMaxVramStreams> audDecoderVramHelper::sm_DeferredFreeList;

u8 *audDecoderVramHelper::sm_FixedHeap = NULL;
sysMemFixedAllocator *audDecoderVramHelper::sm_FetchBufferAllocator = NULL;
sysMemFixedAllocator *audDecoderVramHelper::sm_FetchCompleteAllocator = NULL;

void audDecoderVramHelper::InitClass()
{
	// we need 16 byte alignment for the complete marker, so use a slot size of 16 bytes
	sm_FixedHeap = rage_aligned_new(16) u8[kNumFetchBuffers*kFetchBufferSize*kMaxVramStreams + 16*kMaxVramStreams];
	sm_FetchBufferAllocator = rage_new sysMemFixedAllocator(sm_FixedHeap, kNumFetchBuffers*kFetchBufferSize, kMaxVramStreams);
	sm_FetchCompleteAllocator = rage_new sysMemFixedAllocator(sm_FixedHeap + (kNumFetchBuffers*kFetchBufferSize*kMaxVramStreams), 16, kMaxVramStreams);
}

void audDecoderVramHelper::ShutdownClass()
{
	delete[] sm_FixedHeap;
	sm_FixedHeap = NULL;

	delete sm_FetchCompleteAllocator;
	sm_FetchCompleteAllocator = NULL;

	delete sm_FetchBufferAllocator;
	sm_FetchBufferAllocator = NULL;
}

void audDecoderVramHelper::DeferredFree(u32 *fetchCompleteBuffer, u8 *fetchBuffer, const s32 slotId)
{
	audVramBufferPair &listEntry = sm_DeferredFreeList.Append();
	listEntry.fetchBuffer = fetchBuffer;
	listEntry.fetchCompleteBuffer = fetchCompleteBuffer;
	listEntry.waveSlotId = slotId;
	audWaveSlot::AddSlotReference(listEntry.waveSlotId);
}

void audDecoderVramHelper::ProcessFreeList()
{
	PF_SET(NumDeferredFrees, sm_DeferredFreeList.GetCount());
	PF_SET(NumFetchBuffersAllocated,kMaxVramStreams - sm_FetchCompleteAllocator->GetMemoryAvailable() / 16);
	for(s32 i = 0; i < sm_DeferredFreeList.GetCount(); i++)
	{
		audVramBufferPair &listEntry = sm_DeferredFreeList[i];
		if(*listEntry.fetchCompleteBuffer != 0)
		{			
			// Fetch complete; free buffers	
			sm_FetchBufferAllocator->Free(listEntry.fetchBuffer);
			sm_FetchCompleteAllocator->Free(listEntry.fetchCompleteBuffer);
			audWaveSlot::RemoveSlotReference(listEntry.waveSlotId);
			sm_DeferredFreeList.Delete(i);
			i--;
		}
	}
}

audDecoderVramHelper::~audDecoderVramHelper()
{
	Free();
}

void audDecoderVramHelper::Free()
{
	if(m_GpuFetchComplete && *m_GpuFetchComplete == 0)
	{
		// we need to wait for the GPU copy to complete before we can reuse the buffers
		DeferredFree(m_GpuFetchComplete, m_FetchBuffer, m_WaveSlotId);
	}
	else
	{
		// Synchronous free
		sm_FetchCompleteAllocator->Free(m_GpuFetchComplete);
		sm_FetchBufferAllocator->Free(m_FetchBuffer);
	}

	m_IsWaitingOnGpu = false;
	m_GpuFetchComplete = NULL;
	m_FetchBuffer = NULL;
}

void audDecoderVramHelper::Init(const s32 waveSlotId, const bool isDataMP3, u32 numFetchBuffers)
{
	Assert(numFetchBuffers <= kNumFetchBuffers);
	Assert(_IsPowerOfTwo(numFetchBuffers));
	m_WaveSlotId = waveSlotId;

	m_IsDataMP3 = isDataMP3;

	m_SubmitPacketIndex = 0;
	m_FetchPacketIndex = 0;

	m_NumPacketsQueued = 0;

	m_BufferReadIndex = 0;
	m_BufferWriteIndex = 0;

	m_GpuFetchComplete = 0;

	m_NumFetchBuffers = numFetchBuffers;
	m_FetchBuffer = NULL;
	m_GpuFetchComplete = NULL;
	
	sysMemZeroBytes<sizeof(m_BytesConsumed)>(&m_BytesConsumed);
	sysMemZeroBytes<sizeof(m_BufferSize)>(&m_BufferSize);
	
	sysMemZeroBytes<sizeof(m_Packets)>(&m_Packets);

	m_RequestTimeFrames = 0;
	m_RequestTimeMs = 0;

	m_IsPeekDwordValid = false;

	m_IsInitialised = true;
	m_HasBufferedEntireWave = false;
}

void audDecoderVramHelper::Reset()
{
	Free();

	m_SubmitPacketIndex = 0;
	m_FetchPacketIndex = 0;

	m_NumPacketsQueued = 0;

	m_BufferReadIndex = 0;
	m_BufferWriteIndex = 0;

	sysMemZeroBytes<sizeof(m_BytesConsumed)>(&m_BytesConsumed);
	sysMemZeroBytes<sizeof(m_BufferSize)>(&m_BufferSize);
	sysMemZeroBytes<sizeof(m_Packets)>(&m_Packets);

	m_RequestTimeFrames = 0;
	m_RequestTimeMs = 0;
	m_IsPeekDwordValid = false;
	m_HasBufferedEntireWave = false;
}

void audDecoderVramHelper::SubmitPacket(const u8 *inputBuffer, const u32 startOffsetBytes, const u32 inputBufferSize, const u32 loopPointBytes)
{
	Assert(QueryReadyForMoreData());

	if(audVerifyf(inputBuffer, "NULL input buffer; startOffset: %u, size: %u, loopPoint: %u", startOffsetBytes, inputBufferSize, loopPointBytes))
	{
		sysMemZeroBytes<sizeof(audDecoderPacket)>(&m_Packets[m_SubmitPacketIndex]);

		audDecoderPacket &queuedPacket = m_Packets[m_SubmitPacketIndex];
		queuedPacket.InputBuffer = inputBuffer;
		queuedPacket.InputBufferSize = inputBufferSize;
		queuedPacket.InputBufferBytesRead = startOffsetBytes;
		queuedPacket.LoopPointBytes = loopPointBytes;
		m_NumPacketsQueued++;

		Assert(m_Packets.size() == 2);
		m_SubmitPacketIndex = (m_SubmitPacketIndex + 1) & 1;
	}
}

void audDecoderVramHelper::Update()
{
	Assert(m_IsInitialised);
	if(m_IsWaitingOnGpu)
	{
		Assert(m_GpuFetchComplete);
		if(BANK_ONLY(!g_DebugDelayAudioGPU && ) *m_GpuFetchComplete != 0)
		{
			PF_SET(FetchTimeFrames, audDriver::GetMixer()->GetMixerTimeFrames() - m_RequestTimeFrames);
			PF_SET(FetchTimeMs, audEngineUtil::GetCurrentTimeInMilliseconds() - m_RequestTimeMs);
			
			m_IsWaitingOnGpu = false;
			*m_GpuFetchComplete = 0;

			m_BufferSize[m_BufferWriteIndex] = m_FetchRequestSize;
			
			if(!m_HasBufferedEntireWave)
			{
				m_BytesConsumed[m_BufferWriteIndex] = 0;
			}
			else
			{
				Assert(m_BufferWriteIndex == 0);
			}
			
			m_BufferWriteIndex = (m_BufferWriteIndex + 1) & (m_NumFetchBuffers-1);
			sm_FetchCompleteAllocator->Free(m_GpuFetchComplete);
			m_GpuFetchComplete = NULL;
		}
	}
	
	if(!m_IsWaitingOnGpu && GetBytesAvailable(m_BufferWriteIndex) == 0 && m_NumPacketsQueued && !m_HasBufferedEntireWave)
	{
		ScheduleFetch(m_BufferWriteIndex);
	}

	// Update the peek dword from the current read ptr
	if(!m_IsPeekDwordValid && m_IsDataMP3 && QueryNumBytesAvailable() >= 4)
	{
		// These four bytes could span two buffers, so we can't shortcut the read logic.
		ReadData((u8*)&m_PeekDword, sizeof(m_PeekDword));

		m_IsPeekDwordValid = true;

#if __ASSERT
		// We should always be fetching the start of an MP3 packet
		const u32 frameSizeBytes = audMp3Util::ComputeSizeOfFrame(m_PeekDword);
		audAssertf(frameSizeBytes >= audMp3Util::kMinFrameSizeBytes && frameSizeBytes <= audMp3Util::kMaxFrameSizeBytes, "Invalid MP3 frame size %u", frameSizeBytes);
#endif
	}
	else
	{
		// Don't leave an previously valid value kicking around, just in case
		m_PeekDword = 0;

		m_IsPeekDwordValid = false;
	}
}

void audDecoderVramHelper::ScheduleFetch(const u32 destIndex)
{
	Assert(!m_IsWaitingOnGpu);

	if(m_GpuFetchComplete == NULL)
	{
		m_GpuFetchComplete = (u32*)sm_FetchCompleteAllocator->Allocate(4, 4, 0);
		if(!m_GpuFetchComplete)
		{
			return;
		}
		*m_GpuFetchComplete = 0;
	}

	if(m_FetchBuffer == NULL)
	{
		m_FetchBuffer = (u8*)sm_FetchBufferAllocator->Allocate(m_NumFetchBuffers * kFetchBufferSize, 16, 0);
		if(!m_FetchBuffer)
		{
			sm_FetchCompleteAllocator->Free(m_GpuFetchComplete);
			m_GpuFetchComplete = NULL;
			return;
		}
		AlignedAssert(m_FetchBuffer,16);
	}

	Assert(*m_GpuFetchComplete == 0);
	audDecoderPacket &packet = m_Packets[m_FetchPacketIndex];

	u32 fetchRequestSize = Min<u32>(kFetchBufferSize, packet.InputBufferSize - packet.InputBufferBytesRead);

	u32 readOffset = packet.InputBufferBytesRead;

	bool willBufferEntireWave = false;
	if(packet.LoopPointBytes != ~0U && packet.InputBufferSize <= m_NumFetchBuffers*kFetchBufferSize)
	{
		willBufferEntireWave = true;
		readOffset = 0;
		fetchRequestSize = packet.InputBufferSize;
		Assert(destIndex == 0);
	}

	PF_INCREMENT(NumFetches);

	bool fetchRequested;
	{
#if !__FINAL && !__NO_OUTPUT
		sysPerformanceTimer timer("fetchreqtime");
		timer.Start();
#endif
		fetchRequested = GRCDEVICE.GpuMemCpy(&m_FetchBuffer[destIndex * kFetchBufferSize], packet.InputBuffer + readOffset, fetchRequestSize, *m_GpuFetchComplete);

#if !__FINAL && !__NO_OUTPUT
		timer.Stop();
		if(timer.GetTimeMS() > 1.f)
		{
			audWarningf("GpuMemCpy blocked the mixer for %fms", timer.GetTimeMS());
		}
#endif
	}

	if(!fetchRequested)
	{
		sm_FetchCompleteAllocator->Free(m_GpuFetchComplete);
		m_GpuFetchComplete = NULL;

		PF_INCREMENT(NumFetchRequestsDenied);
		return;
	}

	if(willBufferEntireWave)
	{
		// 'consume' the start offset bytes
		m_BytesConsumed[0] = packet.InputBufferBytesRead;
		packet.InputBufferBytesRead = 0;
		m_HasBufferedEntireWave = true;
	}
	

	//audDisplayf("[VRAMDEC] Fetching %u bytes from %p into %u", fetchRequestSize, packet.InputBuffer + packet.InputBufferBytesRead, destIndex);
	
	m_FetchRequestSize = fetchRequestSize;	
	sys_lwsync();
	m_IsWaitingOnGpu = true;

	m_RequestTimeFrames = audDriver::GetMixer()->GetMixerTimeFrames();
	m_RequestTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();

	packet.InputBufferBytesRead += fetchRequestSize;
	if(packet.InputBufferBytesRead == packet.InputBufferSize)
	{
		if(packet.LoopPointBytes != ~0U)
		{
			packet.InputBufferBytesRead = packet.LoopPointBytes;
		}
		else if(m_NumPacketsQueued > 0)
		{
			m_FetchPacketIndex = (m_FetchPacketIndex+1) & 1;
			m_NumPacketsQueued--;
		}
	}
}
#endif // !__SPU

void audDecoderVramHelper::AdvanceReadPtr(const u32 numBytes)
{
	Assert(QueryNumBytesAvailable() >= numBytes);
	u32 numBytesWritten = 0;
	
	if(m_HasBufferedEntireWave)
	{
		while(numBytesWritten < numBytes)
		{
			const u32 numBytesThisTime = Min(numBytes - numBytesWritten, m_BufferSize[0] - m_BytesConsumed[0]);
			numBytesWritten += numBytesThisTime;
			m_BytesConsumed[0] += numBytesThisTime;
			if(m_BytesConsumed[0] >= m_BufferSize[0])
			{
				m_BytesConsumed[0] = m_Packets[0].LoopPointBytes;
			}
		}
	}
	else
	{
		while(numBytesWritten < numBytes)
		{
			const u32 numBytesToRead = Min(GetBytesAvailable(m_BufferReadIndex), numBytes - numBytesWritten);
			numBytesWritten += numBytesToRead;
			m_BytesConsumed[m_BufferReadIndex] += numBytesToRead;
			if(GetBytesAvailable(m_BufferReadIndex) == 0)
			{
				m_BufferReadIndex = (m_BufferReadIndex+1) & (m_NumFetchBuffers-1);
			}
		}
	}

	// Invalidate our peek dword (it's only updated once per frame, so for the rest of the frame it's out of sync with our read
	// ptr)
	m_PeekDword = 0;
	m_IsPeekDwordValid = false;
}

u32 audDecoderVramHelper::QueryNumBytesAvailable() const
{
	if(m_HasBufferedEntireWave)
	{
		if(m_BufferSize[0] > 0)
		{
			//we have 'infinite' bytes
			return ~0U;
		}
		return 0;
	}
	else
	{
		u32 sum = 0;
		for(u32 i = 0; i < m_NumFetchBuffers; i++)
		{
			sum += GetBytesAvailable(i);
		}
		return sum;
	}
}

#if __SPU
u8 *g_SpuFetchBuffer = NULL;
size_t g_SpuFetchBufferSize = audDecoderVramHelper::kFetchBufferSize*audDecoderVramHelper::kNumFetchBuffers + 16;
#endif

u32 audDecoderVramHelper::ReadData(u8 *buf, const u32 numBytesToRead) const
{
	Assert(QueryNumBytesAvailable() >= numBytesToRead);
	u32 numBytesRead = 0;

	if(m_HasBufferedEntireWave)
	{		
		Assert(m_BufferReadIndex == 0);

#if __SPU
		// Grab the entire fetch buffer onto SPU
		u32 unalignedEa = (u32)(m_FetchBuffer);
		u32 alignedEa = unalignedEa & ~15;
		const u32 paddingBytes = (unalignedEa-alignedEa);
		const u32 alignedSize = ((m_BufferSize[0] + paddingBytes) + 15) & ~15;
		const s32 tag = 8;
		sysDmaGetAndWait(g_SpuFetchBuffer, alignedEa, alignedSize, tag);
#endif

		
		u32 offsetBytes = m_BytesConsumed[0];
		while(numBytesRead < numBytesToRead)
		{			
			const u32 numBytesThisTime = Min(numBytesToRead - numBytesRead, m_BufferSize[0] - offsetBytes);
			
#if __SPU
			sysMemCpy(buf + numBytesRead, g_SpuFetchBuffer + paddingBytes + offsetBytes, numBytesThisTime);

#else
			sysMemCpy(buf + numBytesRead, m_FetchBuffer + offsetBytes, numBytesThisTime);
#endif
			
			offsetBytes += numBytesThisTime;
			if(offsetBytes >= m_BufferSize[0])
			{
				offsetBytes = m_Packets[0].LoopPointBytes;
			}
			numBytesRead += numBytesThisTime;
		}
	}
	else
	{
		numBytesRead = 0;
		u32 bufferIndex = m_BufferReadIndex;
		while(numBytesRead < numBytesToRead)
		{
			const u32 offsetBytes = m_BytesConsumed[bufferIndex];
			const u32 numBytesAvailable = Min(GetBytesAvailable(bufferIndex), numBytesToRead - numBytesRead);
			
	#if __SPU
			u32 unalignedEa = (u32)(m_FetchBuffer + kFetchBufferSize*bufferIndex + offsetBytes);
			u32 alignedEa = unalignedEa & ~15;
			const u32 paddingBytes = (unalignedEa-alignedEa);
			const u32 alignedSize = ((numBytesAvailable + paddingBytes) + 15) & ~15;
			const s32 tag = 7;
			sysDmaGetAndWait(g_SpuFetchBuffer, alignedEa, alignedSize, tag);
			
			sysMemCpy(buf + numBytesRead, g_SpuFetchBuffer + paddingBytes, numBytesAvailable);

	#else
			sysMemCpy(buf + numBytesRead, &m_FetchBuffer[kFetchBufferSize*bufferIndex + offsetBytes], numBytesAvailable);
	#endif
			numBytesRead += numBytesAvailable;
			bufferIndex = (bufferIndex+1) & (m_NumFetchBuffers-1);
		}
	}

	return numBytesRead;
}

void audDecoderVramHelper::DebugPrint() const
{
#if 0
	audDisplayf("%p %s - %08X", this, m_IsWaitingOnGpu ? "Waiting on GPU" : "...", (u32)*m_GpuFetchComplete);
	audDisplayf("Write index: %u read index: %u", m_BufferWriteIndex, m_BufferReadIndex);
	u32 numBytesQueued = 0;
	for(s32 i = 0; i < m_Packets.size(); i++)
	{
		if(m_Packets[i].InputBuffer)
		{
			numBytesQueued += m_Packets[i].InputBufferSize - m_Packets[i].InputBufferBytesRead;
		}
	}
	audDisplayf("Packets queued: %u, bytes queued: %u", m_NumPacketsQueued, numBytesQueued);

	for(u32 i = 0 ; i < m_NumFetchBuffers; i++)
	{
		audDisplayf("%u: consumed %u/%u (%u bytes available)", i, m_BytesConsumed[i], m_BufferSize[i], GetBytesAvailable(i));
	}
#endif
}

}

#endif // _PS3
