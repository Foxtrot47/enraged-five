//
// audiohardware/device_360.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//
#if RSG_PC

#include "device_xaudio_pc.h"
#include "driver.h"
#include "submix.h"

#include "file/asset.h"
#include "file/stream.h"

#include "system/endian.h"
#include "system/param.h"
#include "system/timer.h"

#include "grcore/debugdraw.h"

#include "vector/colors.h"

#include "mixing_vmath.inl"

#include "vectormath/vectormath.h"
#include "math/amath.h"

#pragma warning(disable: 4200) 
#include "bink/movie.h"

#include "profile/profiler.h"

#if (RSG_PC && __D3D11_1)
/* If you want to drop Windows 7 support add this
	#if (_WIN32_WINNT >= 0x0602) //_WIN32_WINNT_WIN8
	#include <xaudio2.h> // from Windows 8.0 SDK
	#pragma comment(lib,"xaudio2.lib")
	#endif
*/
#endif

#define VECTORIZED_ONBUFFERSTART		1

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if(p) { p->Release(); p = NULL; }
#endif

#if !__TOOL
PARAM(audioDeviceCore,"Specify the core to run the audio on: 0->6. Default is 6. -1 means any core)");
#endif

NOSTRIP_PC_PARAM(audiobuffers, "Set number of audio buffers, less is better for latency, more for lower end machines");

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IMMNotificationClient = __uuidof(IMMNotificationClient);

PF_PAGE(XAudioDevicePage, "XAudio Device stats");
PF_GROUP(XAudioDeviceGroup);
PF_LINK(XAudioDevicePage, XAudioDeviceGroup);

//PF_TIMER(OnBufferStart, XAudioDeviceGroup);
PF_VALUE_FLOAT(ElapsedTime, XAudioDeviceGroup);
PF_VALUE_FLOAT(MixerIntervalTime, XAudioDeviceGroup);
//PF_VALUE_FLOAT(MixerNextInterval, XAudioDeviceGroup);

namespace rage
{
	const u32 kNumberOfFramesBeforeReEnumerate = 3000 / 6;   // 3000ms / 6ms per frame

	const s32 kMaxBuffersToSubmitPerFrame = 2;

	sysCriticalSectionToken audMixerDeviceXAudio2::sm_SuspendLock;
	sysPerformanceTimer* audMixerDeviceXAudio2::sm_PerfTimer;
	bool audMixerDeviceXAudio2::sm_NoAudioHardware = false;
	bool audMixerDeviceXAudio2::sm_ForceReinit = false;
	bool audMixerDeviceXAudio2::sm_DoingReinit = true;
	u32 audMixerDeviceXAudio2::sm_NumberOfAudioBuffers = kXAudioNumberOfBuffers;
	u32 audMixerDeviceXAudio2::sm_NewNumberOfAudioBuffers = kXAudioNumberOfBuffers;
	u32 audMixerDeviceXAudio2::sm_ReinitCounter = 0; 
	u32 audMixerDeviceXAudio2::sm_SuspendCounter = 0;
	bool audMixerDeviceXAudio2::sm_WasCapturing = false;
	sysIpcSema audMixerDeviceXAudio2::sm_TriggerUpdateSema; // for capturing
	sysIpcSema audMixerDeviceXAudio2::sm_MixBuffersSema;

	IXAudio2* audMixerDeviceXAudio2::sm_XAudio2 = NULL;
	IXAudio2MasteringVoice* audMixerDeviceXAudio2::sm_MasteringVoice = NULL;
	IXAudio2SourceVoice* audMixerDeviceXAudio2::sm_OutputVoice = NULL;

	IMMDeviceEnumerator* audMixerDeviceXAudio2::sm_DeviceEnumerator = NULL;
	IMMDeviceCollection* audMixerDeviceXAudio2::sm_DeviceCollection = NULL;
	IMMDevice* audMixerDeviceXAudio2::sm_AudioRenderDevice = NULL;
		
	volatile bool audMixerDeviceXAudio2::sm_IsMixLoopThreadRunning = false;
	volatile bool audMixerDeviceXAudio2::sm_ShouldMixLoopThreadBeRunning = true;
	sysIpcThreadId	audMixerDeviceXAudio2::sm_MixLoopThreadId = sysIpcThreadIdInvalid;

	u32 audMixerDeviceXAudio2::sm_BufferSize = 0;
	u32 audMixerDeviceXAudio2::sm_OutputBufferSizeBytes = 0;
	f32 *audMixerDeviceXAudio2::sm_EmptyOutputBuffer = NULL;
	u32 audMixerDeviceXAudio2::sm_NumHardwareChannels = 6;

	LPWSTR audMixerDeviceXAudio2::m_DefaultAudioDeviceId = NULL;

#if __BANK
	sysPerformanceTimer* audMixerDeviceXAudio2::sm_OnBufferStartAverageFrameTimer;
	sysPerformanceTimer* audMixerDeviceXAudio2::sm_MixerIntervalTimer;
#endif

	audMixerDeviceXAudio2::audMixerDeviceXAudio2() :
		m_cRef(1),
		m_BuffersToDropAfterCapturing(0)
	{
		m_OutputBufferQueue.Size(kXAudioNumberOfBuffers);
		for(int i=0; i<kXAudioMaxNumberOfBuffers; i++)
		{
			m_BufferPool[i] = NULL;
			m_BufferState[i] = BufferState::Free;
		}

		m_SubmitBufferIndex = 0;
		m_CurrentSubmitBufferCount = 0;
		for(int i=0; i<kXAudioNumberOfSubmitBuffers; i++)
		{
			m_SubmitBuffer[i] = NULL;
		}
#if __BANK
		m_NumberOfAudioDevices = 0;

		for(u32 i = 0; i < kMaxHardwareChannels; i++) 
		{
			m_OutputStreams[i] = NULL;
			m_NumOutputDataBytes[i] = 0;
		}
#endif
	}

	bool audMixerDeviceXAudio2::CreateXAudio()
	{
		if(!sm_XAudio2)
		{
			HRESULT hr;
			if(FAILED(hr = XAudio2Create(&sm_XAudio2, 0, XAUDIO2_ANY_PROCESSOR)))
			{
				return false;
			}
		}
		return true;
	}

	void audMixerDeviceXAudio2::DestroyXAudio()
	{
		if(sm_XAudio2)
		{
			sm_XAudio2->StopEngine();
			if(sm_OutputVoice)
			{
				sm_OutputVoice->FlushSourceBuffers();
			}
		}
		SAFE_RELEASE(sm_XAudio2);
	}

	bool audMixerDeviceXAudio2::InitAudioDevice()
	{
		if(sm_XAudio2)
		{
			sm_XAudio2->StopEngine();
			if(sm_OutputVoice)
			{
				sm_OutputVoice->FlushSourceBuffers();
			}
		}

		// initialize some defaults in case the hardware fails to init
		u32 numHardwareChannels = 6; //default to 5.1		
		sm_NumHardwareChannels = numHardwareChannels;

		audOutputMode outputMode = AUD_OUTPUT_5_1;
		audDriver::SetHardwareOutputMode(outputMode);
		audDriver::SetDownmixOutputMode(outputMode);

		Assert(sm_XAudio2);

		u32 deviceCount = 0;
		if(!sm_DeviceEnumerator)
		{
			return false;
		}

		HRESULT hr;
		if(!audVerifyf(SUCCEEDED(sm_DeviceEnumerator->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE, &sm_DeviceCollection)), "Unable to enumerate audio devices"))
		{
			SAFE_RELEASE(sm_DeviceCollection);
			return false;
		}
		if(!audVerifyf(SUCCEEDED(sm_DeviceCollection->GetCount(&deviceCount)), "Unable to get number of audio devices"))
		{
			SAFE_RELEASE(sm_DeviceCollection);
			m_NumberOfAudioDevices = deviceCount;
			return false;
		}

		m_NumberOfAudioDevices = deviceCount;

		WAVEFORMATEX* waveFormat;
		if(deviceCount > 0)
		{
			if(!audVerifyf(SUCCEEDED(sm_DeviceEnumerator->GetDefaultAudioEndpoint(eRender, eMultimedia, &sm_AudioRenderDevice)), "Unable to get default audio device"))
			{
				SAFE_RELEASE(sm_DeviceCollection);
				SAFE_RELEASE(sm_AudioRenderDevice);
				return false;
			}
			if(sm_AudioRenderDevice)
			{
				if(m_DefaultAudioDeviceId)
				{
					CoTaskMemFree(m_DefaultAudioDeviceId);
				}
				m_DefaultAudioDeviceId = NULL;
				sm_AudioRenderDevice->GetId(&m_DefaultAudioDeviceId);

				IAudioClient* audioClient;
				hr = sm_AudioRenderDevice->Activate(__uuidof(IAudioClient), CLSCTX_INPROC_SERVER, NULL, (void**)&audioClient);
				if(audioClient)
				{
					hr = audioClient->GetMixFormat(&waveFormat);
					if(FAILED(hr) || !waveFormat)
					{
						Assertf(false,"GetMixFormat call failed, with return code %08X defaulting numHardwareChannels to 2",hr) ;
						numHardwareChannels = 2;
					}
					else
					{
						Displayf("Number of channels : %d", waveFormat->nChannels);
						numHardwareChannels = waveFormat->nChannels;
					}
					SAFE_RELEASE(audioClient);
				}
				else 
				{
					SAFE_RELEASE(audioClient);
					SAFE_RELEASE(sm_DeviceCollection);
					SAFE_RELEASE(sm_AudioRenderDevice);
					return false;
				}
			}
			else
			{
				SAFE_RELEASE(sm_DeviceCollection);
				SAFE_RELEASE(sm_AudioRenderDevice);
				return false;
			}
		}
		else
		{
			SAFE_RELEASE(sm_DeviceCollection);
			return false;
		}

		sm_NumHardwareChannels = numHardwareChannels;

		if(sm_NumHardwareChannels <= 3)
			sm_NumHardwareChannels = 2;
		if(sm_NumHardwareChannels > 3)
			sm_NumHardwareChannels = audMixerDevice::kMaxOutputChannels; // we only want to output to a max 6 channels

		PartialShutdown(); // clean up any thing allocated before creating new ones

		hr = sm_XAudio2->CreateMasteringVoice(&sm_MasteringVoice, sm_NumHardwareChannels, kMixerNativeSampleRate, 0, 0, NULL);
		Displayf("CreateMasteringVoice %x", hr);
		if(FAILED(hr))
		{
			if(hr == XAUDIO2_E_DEVICE_INVALIDATED)
			{
				sm_ForceReinit = true;
			}

			SAFE_RELEASE(sm_DeviceCollection);
			SAFE_RELEASE(sm_AudioRenderDevice);
			DestroyXAudioVoices();
			DestroyXAudio();
			return false;
		}
		Assert(sm_MasteringVoice);

		if(sm_NumHardwareChannels == 6)
		{
			outputMode = AUD_OUTPUT_5_1;
			audDisplayf("5.1 hardware output");
		}
		else
		{
			// WIN32PC TODO: support other output formats
			outputMode = AUD_OUTPUT_STEREO;
			audDisplayf("Stereo hardware output");
		}

		audDriver::SetHardwareOutputMode(outputMode);
		audDriver::SetDownmixOutputMode(outputMode);

		WAVEFORMATEX voiceFormat;
		voiceFormat.wBitsPerSample = 32;
		voiceFormat.wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
		Assign(voiceFormat.nChannels, sm_NumHardwareChannels);
		voiceFormat.nSamplesPerSec = kMixerNativeSampleRate;
		voiceFormat.cbSize = 0;
		voiceFormat.nBlockAlign = (voiceFormat.nChannels * voiceFormat.wBitsPerSample) >> 3;
		voiceFormat.nAvgBytesPerSec = voiceFormat.nSamplesPerSec * voiceFormat.nBlockAlign;

		if(FAILED(sm_XAudio2->CreateSourceVoice(&sm_OutputVoice, &voiceFormat, XAUDIO2_VOICE_NOSRC | XAUDIO2_VOICE_NOPITCH, 1.f, this, NULL, NULL)))
		{
			SAFE_RELEASE(sm_DeviceCollection);
			SAFE_RELEASE(sm_AudioRenderDevice);
			DestroyXAudioVoices();
			DestroyXAudio();
			return false;
		}

		static const f32 stereoOutputMatrix[2*2] = {
			1.f,0.f,
			0.f,1.f};

		static const f32 fiveoOneOutputMatrix[6*6] = {
			1.f,0.f,0.f,0.f,0.f,0.f,
			0.f,1.f,0.f,0.f,0.f,0.f,
			0.f,0.f,1.f,0.f,0.f,0.f,
			0.f,0.f,0.f,1.f,0.f,0.f,
			0.f,0.f,0.f,0.f,1.f,0.f,
			0.f,0.f,0.f,0.f,0.f,1.f,
		};

		if(FAILED(sm_OutputVoice->SetOutputMatrix(NULL, sm_NumHardwareChannels, sm_NumHardwareChannels, (sm_NumHardwareChannels == 6 ? fiveoOneOutputMatrix : stereoOutputMatrix))))
		{
			SAFE_RELEASE(sm_DeviceCollection);
			SAFE_RELEASE(sm_AudioRenderDevice);
			DestroyXAudioVoices();
			DestroyXAudio();
			return false;
		}
		static const f32 channelMatrix[] = {1.f,1.f,1.f,1.f,1.f,1.f};
		if(FAILED(sm_OutputVoice->SetChannelVolumes(sm_NumHardwareChannels, channelMatrix)))
		{
			SAFE_RELEASE(sm_DeviceCollection);
			SAFE_RELEASE(sm_AudioRenderDevice);
			DestroyXAudioVoices();
			DestroyXAudio();
			return false;
		}

		sm_BufferSize = sm_NumHardwareChannels * kMixBufNumSamples;
		sm_OutputBufferSizeBytes = sm_BufferSize * sizeof(f32);

		sm_EmptyOutputBuffer = rage_aligned_new(16) f32[sm_BufferSize * kMaxBuffersToSubmitPerFrame];
		sysMemSet( sm_EmptyOutputBuffer, 0, sm_OutputBufferSizeBytes * kMaxBuffersToSubmitPerFrame);

		for(int i=0; i<kXAudioMaxNumberOfBuffers; i++)
		{
			m_BufferPool[i] = rage_aligned_new(16) f32[sm_BufferSize];
			m_BufferState[i] = BufferState::Free;
		}

		m_SubmitBufferIndex = 0;
		for(int i=0; i<kXAudioNumberOfSubmitBuffers; i++)
		{
			m_SubmitBuffer[i] = rage_aligned_new(16) f32[sm_BufferSize * kMaxBuffersToSubmitPerFrame];
			sysMemSet( m_SubmitBuffer[i], 0, sm_OutputBufferSizeBytes * kMaxBuffersToSubmitPerFrame);
		}
		m_OutputBufferQueue.Size(sm_NumberOfAudioBuffers);
		m_OutputBufferQueue.Reset();

		return true;
	}

	bool audMixerDeviceXAudio2::DoWeHaveANewDefaultAudioDevice()
	{
		if(sm_XAudio2 && sm_DeviceEnumerator)
		{
			IMMDeviceCollection* deviceCollection = NULL;
			IMMDevice* audioRenderDevice = NULL;

			u32 deviceCount = 0;
			HRESULT hr;
			hr = sm_DeviceEnumerator->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE, &deviceCollection);
			if(SUCCEEDED(hr) && deviceCollection)
			{
				hr = deviceCollection->GetCount(&deviceCount);
				if(SUCCEEDED(hr))
				{
					if(deviceCount > 0)
					{
						if(!audVerifyf(SUCCEEDED(sm_DeviceEnumerator->GetDefaultAudioEndpoint(eRender, eMultimedia, &audioRenderDevice)), "Unable to get default audio device"))
						{
							SAFE_RELEASE(audioRenderDevice);
							SAFE_RELEASE(deviceCollection);
							if(m_DefaultAudioDeviceId)
							{
								CoTaskMemFree(m_DefaultAudioDeviceId);
							}
							m_DefaultAudioDeviceId = NULL;
							return true;
						}
						if(audioRenderDevice)
						{
							LPWSTR deviceId = NULL;
							audioRenderDevice->GetId(&deviceId); 
							if(sm_AudioRenderDevice && deviceId)
							{
								if(m_DefaultAudioDeviceId && wcscmp(m_DefaultAudioDeviceId, deviceId) == 0)
								{
									CoTaskMemFree(deviceId);
									SAFE_RELEASE(audioRenderDevice);
									SAFE_RELEASE(deviceCollection);
									return false;
								}
								CoTaskMemFree(deviceId);
							}
						}
					}
				}
			}
			SAFE_RELEASE(audioRenderDevice);
			SAFE_RELEASE(deviceCollection);
		}
		if(m_DefaultAudioDeviceId)
		{
			CoTaskMemFree(m_DefaultAudioDeviceId);
		}
		m_DefaultAudioDeviceId = NULL;
		return true;
	}

	bool audMixerDeviceXAudio2::CheckForNewDevice()
	{
		bool doDefaultDeviceChangedCheck = false;
#if __BANK
		if(sm_NewNumberOfAudioBuffers != sm_NumberOfAudioBuffers)
		{
			sm_NumberOfAudioBuffers = sm_NewNumberOfAudioBuffers;
			// force a re-init
			sm_ForceReinit = true;
			doDefaultDeviceChangedCheck = false;
			Displayf("Number of audio mix buffers %d", sm_NumberOfAudioBuffers);
		}
#endif
		sm_ReinitCounter++;
		if(!sm_ForceReinit)
		{
			// I have seen cases when you are enabling and disabling device where everything seems ok, but the OnBufferStart isn't called.
			// We're not suspended but we're not getting OnBufferStart calls. This will force a re-init in those cases.
			if(audDriver::GetMixer() && audDriver::GetMixer()->IsSuspended() == true)
			{
				if(sm_XAudio2 && !sm_NoAudioHardware && sm_SuspendCounter > 1000) // if we're getting the OnBufferStart call from XAudio then we shouldn't be suspended.
				{
					//fixes a case if machine is locked or went to sleep, game can return with no audio because it has been suspended
					SetSuspended(false);
				}
				return false;
			}
			if(sm_ReinitCounter > kNumberOfFramesBeforeReEnumerate)
			{
				SetXaudioSuspended(true);
				doDefaultDeviceChangedCheck = true;
				sm_ReinitCounter = 0;
			}
			else
			{
				return false;
			}
		}
		else
		{
			doDefaultDeviceChangedCheck = true;
		}

		sm_DoingReinit = true;
		if(doDefaultDeviceChangedCheck && !DoWeHaveANewDefaultAudioDevice())
		{
			if(sm_ReinitCounter < kNumberOfFramesBeforeReEnumerate) 
			{
				return false;
			}
			sm_ReinitCounter = 0;
		}

		sm_ForceReinit = false;

		if(CreateXAudio())
		{
			if(InitAudioDevice())
			{
				sm_NoAudioHardware = false;
				StartMixing();
				bwMovie::Init();
				return true;
			}
		}

		sm_NoAudioHardware = true;
		StartMixing();
		return false;
	}

	void audMixerDeviceXAudio2::ReinitHardware()
	{
		CheckForNewDevice();
	}

	bool audMixerDeviceXAudio2::InitHardware()
	{
		sm_NumberOfAudioBuffers = kXAudioNumberOfBuffers;
		if(PARAM_audiobuffers.Get())
		{
			PARAM_audiobuffers.Get(sm_NumberOfAudioBuffers);
		}
		
		Displayf("Number of audio mix buffers %d", sm_NumberOfAudioBuffers);
		sm_NewNumberOfAudioBuffers = sm_NumberOfAudioBuffers;

		m_OutputBufferQueue.Size(sm_NumberOfAudioBuffers);

		sm_PerfTimer = rage_new sysPerformanceTimer("Fake Audio Mixer");
		sm_PerfTimer->Start();

		sm_TriggerUpdateSema = sysIpcCreateSema(false);
		sm_MixBuffersSema = sysIpcCreateSema(false);

#if __BANK
		sm_OnBufferStartAverageFrameTimer = rage_new sysPerformanceTimer("OnBufferStart Audio Timer");
		sm_OnBufferStartAverageFrameTimer->Start();

		sm_MixerIntervalTimer = rage_new sysPerformanceTimer("Mixer Interval Timer");
		sm_MixerIntervalTimer->Start();
#endif

#if (RSG_PC && __D3D11_1)
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
#else
		WIN32PC_ONLY(CoInitialize(NULL));
#endif
		HRESULT hr;

		sm_NoAudioHardware = true; // this would activate the fake mixer

		hr = CoCreateInstance(CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&sm_DeviceEnumerator);
		if(SUCCEEDED(hr) && sm_DeviceEnumerator)
		{
			hr = sm_DeviceEnumerator->RegisterEndpointNotificationCallback(this);
			if(SUCCEEDED(hr))
			{
				if(CreateXAudio())
				{
					if(InitAudioDevice())
					{
						sm_NoAudioHardware = false;
					}
				}
				else
				{
					DestroyXAudio();
				}
			}
		}

#if AUD_OUTPUT_TO_FILE
		m_DebugOutput = ASSET.Create("mixerout","raw");
#endif

		m_Allocator = &sysMemAllocator::GetCurrent();
		return true;
	}

	void audMixerDeviceXAudio2::StartMixing()
	{
		sm_DoingReinit = false;
		if(sm_NoAudioHardware)
		{
			SetXaudioSuspended(true); // suspend sending audio to the hardware
		}
		else
		{
			SetXaudioSuspended(false);

			Assert(sm_OutputVoice);
			sm_XAudio2->StartEngine();

			if(sm_OutputVoice)
			{
				sm_OutputVoice->FlushSourceBuffers();
			}

			XAUDIO2_BUFFER bufDesc;
			bufDesc.AudioBytes = sm_OutputBufferSizeBytes * kMaxBuffersToSubmitPerFrame;
			bufDesc.Flags = 0;
			bufDesc.LoopBegin = bufDesc.LoopLength = XAUDIO2_NO_LOOP_REGION;
			bufDesc.LoopCount = 0;
			bufDesc.PlayBegin = 0;
			bufDesc.PlayLength = 0;
			bufDesc.pContext = 0;
			bufDesc.pAudioData = (BYTE*)audMixerDeviceXAudio2::sm_EmptyOutputBuffer;
			sm_OutputVoice->SubmitSourceBuffer(&bufDesc, NULL);
			sm_OutputVoice->Start(0);
		}

		if(!sm_IsMixLoopThreadRunning)
		{
			sm_ShouldMixLoopThreadBeRunning = true;
			sm_MixLoopThreadId = sysIpcCreateThread(audMixerDeviceXAudio2::sMixLoop, this, 18432, PRIO_TIME_CRITICAL,"[RAGE Audio] Mix thread", 0, "RageAudioMixThread");
			audAssertf(sm_MixLoopThreadId != sysIpcThreadIdInvalid, "Couldn't create RageAudioMixThread!");
		}
	}

	void audMixerDeviceXAudio2::DestroyXAudioVoices()
	{
		if(sm_OutputVoice)
		{
			sm_OutputVoice->Stop(0);
			sm_OutputVoice->DestroyVoice();
			sm_OutputVoice = NULL;
		}
		if(sm_MasteringVoice)
		{
			sm_MasteringVoice->DestroyVoice();
			sm_MasteringVoice = NULL;
		}
	}

	void audMixerDeviceXAudio2::PartialShutdown()
	{
		if(sm_XAudio2)
		{
			sm_XAudio2->StopEngine();
			if(sm_OutputVoice)
			{
				sm_OutputVoice->FlushSourceBuffers();
			}
		}

		DestroyXAudioVoices();

		for(int i=0; i<kXAudioMaxNumberOfBuffers; i++)
		{
			if(m_BufferPool[i])
			{
				delete m_BufferPool[i];
			}
			m_BufferPool[i] = NULL;
			m_BufferState[i] = BufferState::Free;
		}
		for(int i=0; i<kXAudioNumberOfSubmitBuffers; i++)
		{
			if(m_SubmitBuffer[i])
			{
				delete m_SubmitBuffer[i];
			}
			m_SubmitBuffer[i] = NULL;
		}
	}

	void audMixerDeviceXAudio2::ShutdownHardware()
	{
		sm_ShouldMixLoopThreadBeRunning = false;

		while(sm_IsMixLoopThreadRunning)
		{
			sysIpcSleep(10);
		}
		if(sm_MixLoopThreadId != sysIpcThreadIdInvalid)
			sysIpcWaitThreadExit(sm_MixLoopThreadId);
		sm_MixLoopThreadId = sysIpcThreadIdInvalid;

		PartialShutdown();

		SAFE_RELEASE(sm_DeviceCollection);
		SAFE_RELEASE(sm_AudioRenderDevice);

		if(sm_DeviceEnumerator)
		{
			sm_DeviceEnumerator->UnregisterEndpointNotificationCallback(this);
		}
		SAFE_RELEASE(sm_DeviceEnumerator);
		SAFE_RELEASE(sm_XAudio2);
	}

	s32 audMixerDeviceXAudio2::GetFreeOutputBuffer()
	{
		for(u32 i=0; i<sm_NumberOfAudioBuffers; i++)
		{
			if(m_BufferState[i] == BufferState::Free)
			{
				m_BufferState[i] = BufferState::Mixing;
				return i;
			}
		}
		return -1;
	}

	void audMixerDeviceXAudio2::FreeOutputBuffer(s32 id)
	{
		audAssert(id >= 0 && id < (s32)sm_NumberOfAudioBuffers);
#if __BANK
		sysMemSet(m_BufferPool[id], 0, audMixerDeviceXAudio2::sm_OutputBufferSizeBytes);
#endif
		m_BufferState[id] = BufferState::Free;
	}

	void audMixerDeviceXAudio2::SuspendedMixerUpdate()
	{
		MixBuffers();
		//Displayf("SuspendedMixerUpdate %d", audEngineUtil::GetCurrentTimeInMilliseconds());
	}

	void audMixerDeviceXAudio2::TriggerUpdate()
	{
		sysIpcSignalSema(sm_TriggerUpdateSema);
		sysInterlockedIncrement(&m_StartSignalSemaphoreCount);
	}

	void audMixerDeviceXAudio2::WaitOnMixBuffers()
	{
		sysInterlockedIncrement(&m_EndWaitSemaphoreCount);
#if __BANK
		bool rval = sysIpcWaitSemaTimed(sm_MixBuffersSema, 1000);
		if(!rval)
		{
			Displayf("Semaphore timed out sm_MixBuffersSema");
		}
#else
		sysIpcWaitSemaTimed(sm_MixBuffersSema, 1000);
#endif
	}

	DECLARE_THREAD_FUNC(audMixerDeviceXAudio2::sMixLoop)
	{
		USE_MEMBUCKET(MEMBUCKET_AUDIO);
		((audMixerDeviceXAudio2*)ptr)->MixLoop();
	}

	void audMixerDeviceXAudio2::MixLoop()
	{
		sm_IsMixLoopThreadRunning = true;

		while(sm_ShouldMixLoopThreadBeRunning)
		{
			if(m_NumberOfAudioDevices == 0 && audDriver::GetMixer() && audDriver::GetMixer()->IsSuspended() == false)
			{
				SetXaudioSuspended(true);
			}

			if(audDriver::GetMixer() && audDriver::GetMixer()->IsCapturing())
			{
				sm_WasCapturing = true;
				if(sm_XAudio2)
				{
					sm_XAudio2->StopEngine();
					if(sm_OutputVoice)
					{	
						sm_OutputVoice->FlushSourceBuffers();
					}
				}

				bool isFrameRendering = IsFrameRendering();
				if(isFrameRendering)
				{
					sysInterlockedIncrement(&m_StartWaitSemaphoreCount);
					sysIpcWaitSema(sm_TriggerUpdateSema);
				}

				if(audDriver::GetMixer()->IsCapturing())
				{
					m_BuffersToDropAfterCapturing = kAudioBuffersToDropAfterCapturing;
 					EnterReplayMixerSwitchLock();
					MixBuffers();
					if(isFrameRendering)
					{
						sysIpcSignalSema(sm_MixBuffersSema);
						sysInterlockedIncrement(&m_EndSignalSemaphoreCount);
					}
					ExitReplayMixerSwitchLock();
					sysIpcSleep(1);
				}
			}
			else
			{
				if(sm_WasCapturing)
				{
					sm_WasCapturing = false;
					// clear out all the buffers
					m_OutputBufferQueue.Reset();
					for(u32 i=0; i<sm_NumberOfAudioBuffers; i++)
					{
						FreeOutputBuffer(i);
					}
					StartMixing();
				}

				static utimer_t lastTick = 0;
				static float lastInterval = 0;
				utimer_t thisTick = sysTimer::GetTicks();

				utimer_t diffTick = thisTick - lastTick;
				float fDiffTick = static_cast<float>(diffTick);
				fDiffTick *= sysTimer::GetTicksToMicroseconds();

				static f32 duration = 5333.0f;
				if(fDiffTick > duration)
				{
					CheckForNewDevice();
#if __BANK
					static f32 elapsedTime = 0;
					elapsedTime = audMixerDeviceXAudio2::sm_MixerIntervalTimer->GetElapsedTimeMS();
					PF_SET(MixerIntervalTime, fDiffTick);

					audMixerDeviceXAudio2::sm_MixerIntervalTimer->Reset();
					audMixerDeviceXAudio2::sm_MixerIntervalTimer->Start();
#endif
					if(audDriver::GetMixer() && audDriver::GetMixer()->IsInitialised())
					{
						s32 buffersToGenerate = 1;
						for(s32 count = 0; count < buffersToGenerate; count++)
						{
							if(m_BuffersToDropAfterCapturing > 0)
							{
								m_BuffersToDropAfterCapturing--;
							}
							if(!m_OutputBufferQueue.IsFull() || audDriver::GetMixer()->IsSuspended()) 
							{
								audDriver::GetMixer()->MixBuffers();

								if(!audDriver::GetMixer()->IsSuspended() && m_BuffersToDropAfterCapturing <= 0)
								{
									EnterBML();
									OnBufferReady();
									if(m_OutputBufferQueue.GetAvailable() > m_OutputBufferQueue.GetSize()>>1)
									{
										buffersToGenerate++;
									}
									ExitBML();
								}
							}
						}

					}
					lastTick = thisTick;

					utimer_t nowTick = sysTimer::GetTicks();
					utimer_t timeToMix = nowTick - thisTick;
					float timeToMixMicroS = static_cast<float>(timeToMix) * sysTimer::GetTicksToMicroseconds();

					float sleepTime = (4300.0f - timeToMixMicroS) / 1000.0f;
					sleepTime = Clamp(sleepTime, 0.0f, 4.0f);
					sysIpcSleep((int)sleepTime);
				}
				else
				{
					sysIpcSleep(0);
				}
			}
		}
		sm_IsMixLoopThreadRunning = false;
	}

	void audMixerDeviceXAudio2::OnBufferReady()
	{
		s32 bufferId = GetFreeOutputBuffer();
		audAssert(bufferId != -1);
		float *outputData = m_BufferPool[bufferId];

		// interleave and channel swap the mixed data
		const bool isPaused = audDriver::GetMixer()->IsPaused();
		audMixerSubmix *submix = GetMasterSubmix();
		if(submix && !isPaused && outputData && submix->HasMixBuffers())
		{
			Assert(submix->GetNumChannels() == 6);

			if(sm_NumHardwareChannels == 2)
			{
				Assert(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO);
				const Vector_4V *inFL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_LEFT);
				const Vector_4V *inFR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_RIGHT);
				const Vector_4V *inC = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_CENTER);
				const Vector_4V *inRL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_LEFT);
				const Vector_4V *inRR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_RIGHT);

				// FL = (FL) + 0.707(FC) + 0.707(RL)
				// FR = (FR) + 0.707(FC) + 0.707(RR)

				// 0.707f
				const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();
				Vector_4V *out = (Vector_4V*)outputData;
				for(u32 i = 0; i < kMixBufNumSamples; i += 4)
				{
					// load four samples per channel						

					const Vector_4V inputSamples0 = *inFL++;
					const Vector_4V inputSamples1 = *inFR++;
					const Vector_4V inputSamples2 = *inC++;
					const Vector_4V inputSamples4 = *inRL++;
					const Vector_4V inputSamples5 = *inRR++;

					Vector_4V fcContrib = V4Scale(inputSamples2, centreRearRatio);
					const Vector_4V inSamplesFL = V4Add(inputSamples0, V4AddScaled(fcContrib, inputSamples4, centreRearRatio));
					const Vector_4V inSamplesFR = V4Add(inputSamples1, V4AddScaled(fcContrib, inputSamples5, centreRearRatio));

					const Vector_4V inputSamples_01_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesFL, inSamplesFR);
					const Vector_4V inputSamples_01_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesFL, inSamplesFR);

					*out++ = V4Clamp(inputSamples_01_xy, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
					*out++ = V4Clamp(inputSamples_01_zw, V4VConstant(V_NEGONE), V4VConstant(V_ONE));					
				}
			}
			else
			{
				Assert(sm_NumHardwareChannels == 6);
				if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
				{
					Vector_4V *out = (Vector_4V*)outputData;

					//#if __BANK
					//					//Keep a second pointer for dumping data to file
					//					f32 *dumpData = outputData;
					//#endif

					const Vector_4V *inFL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_LEFT);
					const Vector_4V *inFR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_RIGHT);
					const Vector_4V *inC = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_CENTER);
					const Vector_4V *inRL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_LEFT);
					const Vector_4V *inRR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_RIGHT);


					// FL = (FL) + 0.707(FC) + 0.707(RL)
					// FR = (FR) + 0.707(FC) + 0.707(RR)

					// 0.707f
					const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();


					for(u32 i = 0; i < kMixBufNumSamples; i += 4)
					{
						// load four samples per channel						

						const Vector_4V inputSamples0 = *inFL++;
						const Vector_4V inputSamples1 = *inFR++;
						const Vector_4V inputSamples2 = *inC++;
						const Vector_4V inputSamples4 = *inRL++;
						const Vector_4V inputSamples5 = *inRR++;

						Vector_4V fcContrib = V4Scale(inputSamples2, centreRearRatio);
						const Vector_4V inSamplesFL = V4Add(inputSamples0, V4AddScaled(fcContrib, inputSamples4, centreRearRatio));
						const Vector_4V inSamplesFR = V4Add(inputSamples1, V4AddScaled(fcContrib, inputSamples5, centreRearRatio));

						const Vector_4V inputSamples_01_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesFL, inSamplesFR);
						const Vector_4V inputSamples_01_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesFL, inSamplesFR);

						const Vector_4V samples_0123_x = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_xy, V4VConstant(V_ZERO));
						const Vector_4V samples_4501_xy = V4PermuteTwo<X1,Y1,Z2,W2>(V4VConstant(V_ZERO), inputSamples_01_xy);

						const Vector_4V samples_0123_z = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_zw,V4VConstant(V_ZERO));
						const Vector_4V samples_4501_zw = V4PermuteTwo<X1,Y1,Z2,W2>(V4VConstant(V_ZERO), inputSamples_01_zw);

						*out++ = V4Clamp(samples_0123_x, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4Clamp(samples_4501_xy, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4VConstant(V_ZERO);
						*out++ = V4Clamp(samples_0123_z, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4Clamp(samples_4501_zw, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4VConstant(V_ZERO);
					}
				}
				else
				{
					f32 *RESTRICT destPtr = (f32*)outputData;
					u32 outIndex = 0;

					// NOTE: map RAGE channel order FL,FR,SL,SR,C,LFE to XAudio2 FL,FR,C,LFE,SL,SR
					const s32 channelMap[] = {RAGE_SPEAKER_ID_FRONT_LEFT,RAGE_SPEAKER_ID_FRONT_RIGHT,RAGE_SPEAKER_ID_FRONT_CENTER,RAGE_SPEAKER_ID_LOW_FREQUENCY,RAGE_SPEAKER_ID_BACK_LEFT,RAGE_SPEAKER_ID_BACK_RIGHT};
					for(u32 sampleIndex = 0; sampleIndex < kMixBufNumSamples; sampleIndex++)
					{
						for(u32 channelIndex = 0; channelIndex < sm_NumHardwareChannels; channelIndex++)
						{
							destPtr[outIndex++] = submix->GetMixBuffer(channelMap[channelIndex])[sampleIndex];
						}
					}				
				}
			}
		}
		else
		{
			sysMemSet(outputData, 0, sm_OutputBufferSizeBytes);
		}

		m_OutputBufferQueue.Push(bufferId);
		m_BufferState[bufferId] = BufferState::Submitting;
	}


	void audMixerDeviceXAudio2::OnBufferStart(void *)
	{
		//PF_FUNC(OnBufferStart);
#if __BANK
		static f32 elapsedTime = 0;
		elapsedTime = audMixerDeviceXAudio2::sm_OnBufferStartAverageFrameTimer->GetElapsedTimeMS();
		PF_SET(ElapsedTime, elapsedTime);

		audMixerDeviceXAudio2::sm_OnBufferStartAverageFrameTimer->Reset();
		audMixerDeviceXAudio2::sm_OnBufferStartAverageFrameTimer->Start();
#endif
		sm_ReinitCounter = 0;
		if(IsSuspended())
		{
			sm_SuspendCounter++;
		}
		else
		{
			sm_SuspendCounter = 0;
		}
		static bool haveStartedMixing = false;

		EnterBML();
		if(!haveStartedMixing && m_OutputBufferQueue.IsFull())
		{
			haveStartedMixing = true;
		}
		if(haveStartedMixing && m_OutputBufferQueue.IsEmpty())
		{
			//audWarningf("Pausing mixing to fill queue");
			haveStartedMixing = false;
		}
				
		s32 buffersToSubmit = m_OutputBufferQueue.GetCount();
		if(buffersToSubmit > kMaxBuffersToSubmitPerFrame)
		{
			buffersToSubmit = kMaxBuffersToSubmitPerFrame;
		}

		s32 bufferId = -1;
		// submit it to xaudio
		XAUDIO2_BUFFER bufDesc;
		bufDesc.AudioBytes = sm_OutputBufferSizeBytes * kMaxBuffersToSubmitPerFrame;
		bufDesc.Flags = 0;
		bufDesc.LoopBegin = bufDesc.LoopLength = XAUDIO2_NO_LOOP_REGION;
		bufDesc.LoopCount = 0;
		bufDesc.PlayBegin = 0;
		bufDesc.PlayLength = 0;
		bufDesc.pContext = NULL;
		bufDesc.pAudioData = (BYTE*)audMixerDeviceXAudio2::sm_EmptyOutputBuffer;

		if(!haveStartedMixing)
		{
			bufDesc.pAudioData = (BYTE *)m_SubmitBuffer[m_SubmitBufferIndex];
			u8* p = (u8*)m_SubmitBuffer[m_SubmitBufferIndex];
			sysMemSet(p, 0, sm_OutputBufferSizeBytes);
			bufDesc.AudioBytes = sm_OutputBufferSizeBytes;
			m_SubmitBufferIndex = (m_SubmitBufferIndex + 1) % kXAudioNumberOfSubmitBuffers;
		}
		else if(buffersToSubmit > 0)
		{
			bufDesc.pAudioData = (BYTE *)m_SubmitBuffer[m_SubmitBufferIndex];
			u8* p = (u8*)m_SubmitBuffer[m_SubmitBufferIndex];
			for(int i=0; i<buffersToSubmit; i++)
			{
				bufferId = m_OutputBufferQueue.Top();
				if(m_BufferState[bufferId] == BufferState::Submitting)
				{
					memcpy(p, m_BufferPool[bufferId], sm_OutputBufferSizeBytes);
					m_BufferState[bufferId] = BufferState::Done;
					m_OutputBufferQueue.Pop();
					FreeOutputBuffer(bufferId);
				}
				p += sm_OutputBufferSizeBytes;
			}
			m_SubmitBufferIndex = (m_SubmitBufferIndex + 1) % kXAudioNumberOfSubmitBuffers;
		}
		ExitBML();

		if(sm_DoingReinit) 
			return;
		sm_OutputVoice->SubmitSourceBuffer(&bufDesc,NULL);
		sysInterlockedIncrement(&m_CurrentSubmitBufferCount);
	}

	void audMixerDeviceXAudio2::OnBufferEnd(void *)
	{
		if(sm_DoingReinit)
			return;
		sysInterlockedDecrement(&m_CurrentSubmitBufferCount);
	}

	HRESULT audMixerDeviceXAudio2::OnDefaultDeviceChanged(EDataFlow BANK_ONLY(flow), ERole BANK_ONLY(role), LPCWSTR BANK_ONLY(pwstrDeviceId))
	{ 
#if __BANK
		//Displayf("OnDefaultDeviceChanged %d %d %s", flow, role, pwstrDeviceId);
		// when a new default audio device becomes active we will force a re-eval of the audio enumeration
		char  *pszFlow = "?????";
		char  *pszRole = "?????";

		Displayf("Endpoint ID string: %S", (pwstrDeviceId != NULL) ? pwstrDeviceId : L"null ID");

		switch (flow)
		{
		case eRender:
			pszFlow = "eRender";
			break;
		case eCapture:
			pszFlow = "eCapture";
			break;
		default:
			break;
		}

		switch (role)
		{
		case eConsole:
			pszRole = "eConsole";
			break;
		case eMultimedia:
			pszRole = "eMultimedia";
			break;
		case eCommunications:
			pszRole = "eCommunications";
			break;
		default:
			break;
		}

		Displayf("  -->New default device: %ls flow = %s, role = %s\n",	pwstrDeviceId, pszFlow, pszRole);
#endif
		sm_ForceReinit = true;

		return S_OK;
	}
	
	HRESULT audMixerDeviceXAudio2::OnDeviceAdded(LPCWSTR OUTPUT_ONLY(pwstrDeviceId))
	{ 
		audDisplayf("OnDeviceAdded %ls", pwstrDeviceId);
		return S_OK;
	}
	
	HRESULT audMixerDeviceXAudio2::OnDeviceRemoved(LPCWSTR OUTPUT_ONLY(pwstrDeviceId))
	{ 
		audDisplayf("OnDeviceRemoved %ls", pwstrDeviceId);
		return S_OK;
	}
	
	HRESULT audMixerDeviceXAudio2::OnDeviceStateChanged(LPCWSTR OUTPUT_ONLY(pwstrDeviceId), DWORD dwNewState)	
	{ 
		audDisplayf("OnDeviceStateChanged %ls %d", pwstrDeviceId, dwNewState);
		if(dwNewState == DEVICE_STATE_UNPLUGGED || dwNewState == DEVICE_STATE_DISABLED || dwNewState == DEVICE_STATE_NOTPRESENT)
		{
			sm_ForceReinit = true;
		}
		return S_OK;
	}
	
#define AUDIO_DEFINE_PROPERTYKEY(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8, pid) const PROPERTYKEY name = { { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }, pid }
AUDIO_DEFINE_PROPERTYKEY(PKEY_AudioEndpoint_Disable_SysFx, 0x1da5d803, 0xd492, 0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e, 5);
AUDIO_DEFINE_PROPERTYKEY(PKEY_AudioEngine_DeviceFormat, 0xf19f064d, 0x82c, 0x4e27, 0xbc, 0x73, 0x68, 0x82, 0xa1, 0xbb, 0x8e, 0x4c, 0); 

	HRESULT audMixerDeviceXAudio2::OnPropertyValueChanged(LPCWSTR OUTPUT_ONLY(pwstrDeviceId), const PROPERTYKEY key)
	{ 
		if(IsEqualPropertyKey(key, PKEY_AudioEndpoint_Disable_SysFx) || IsEqualPropertyKey(key, PKEY_AudioEngine_DeviceFormat))
		{
			sm_ForceReinit = true;
		}
		audDisplayf("OnPropertyValueChanged %ls %d", pwstrDeviceId, key.pid);
		return S_OK;
	}

	HRESULT audMixerDeviceXAudio2::QueryInterface(REFIID riid, LPVOID FAR* ppvObj)
	{
		*ppvObj = NULL;
		if (riid == IID_IUnknown)
		{
			*ppvObj = static_cast<IUnknown *>(this);
		}
		else if (riid == __uuidof(IMMNotificationClient))
		{
			*ppvObj = static_cast<IMMNotificationClient *>(this);
		}
		else
		{
			return E_NOINTERFACE;
		}
		return S_OK;
	}

	ULONG audMixerDeviceXAudio2::AddRef()
	{
		return InterlockedIncrement((LONG *)&m_cRef);
	}
	
	ULONG audMixerDeviceXAudio2::Release()
	{
		ULONG lRet = InterlockedDecrement((LONG *)&m_cRef);
		if (lRet == 0)
		{
			delete this;
		}
		return lRet;
	}


	void audMixerDeviceXAudio2::SetXaudioSuspended( bool suspend )
	{
		sm_SuspendLock.Lock();
		if(audDriver::GetMixer())
		{
			audDriver::GetMixer()->SetSuspended(suspend);
		}
		if(suspend)
		{
			sm_PerfTimer->Reset();
			sm_PerfTimer->Start();
		}
		sm_SuspendLock.Unlock();
	}

#if __BANK

	void audMixerDeviceXAudio2::DebugDraw()
	{
		s32 queueEntries = 0;
		s32 buffersSubmitted = 0;
		EnterBML();
		queueEntries = m_OutputBufferQueue.GetCount();
		buffersSubmitted = (s32)m_CurrentSubmitBufferCount;
		ExitBML();

		
		grcDebugDraw::AddDebugOutput("Q: %d", queueEntries);		
		grcDebugDraw::AddDebugOutput("Buffers: %d", buffersSubmitted);
		

	/*	float x = 100.f;
		float y = 640.f;

		const float barWidth = 10.f;
		const float barHeight = 10.f;
		const float margin = 2.f;

		for(s32 i = 0; i < queueEntries; i++)
		{
			grcDebugDraw::
		}
		*/


	}

	extern bool g_DumpAudioHardwareOutput;

	void audMixerDeviceXAudio2::DumpOutput(u32 channelIndex, f32 data) 
	{
		if(g_DumpAudioHardwareOutput)
		{
			if(!m_OutputStreams[channelIndex])
			{
				m_NumOutputDataBytes[channelIndex] = 0;

				char fileName[64];
				// RAGE channel ordering
				const char *channelNames[] = 
				{
					"FL",
					"FR",
					"C",
					"LFE",
					"SL",
					"SR",					
					"BL",
					"BR"
				};
				formatf(fileName, "c:\\HardwareDump_%s", channelNames[channelIndex]);
				m_OutputStreams[channelIndex] = ASSET.Create(fileName, "wav");
				Assert(m_OutputStreams[channelIndex]);
				if(m_OutputStreams[channelIndex])
				{
					const unsigned char RIFF[] = {'R', 'I', 'F', 'F'};
					m_OutputStreams[channelIndex]->WriteByte(RIFF, 4);
					int size = 0;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					const unsigned char WAVE[] = {'W', 'A', 'V', 'E'};
					m_OutputStreams[channelIndex]->WriteByte(WAVE, 4);

					const unsigned char fmtChunk[] = {'f', 'm', 't', ' '};
					m_OutputStreams[channelIndex]->WriteByte(fmtChunk, 4);
					size = 16;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					short audioFormat = 1;
					m_OutputStreams[channelIndex]->WriteShort(&audioFormat, 1);
					short numChannels = 1;
					m_OutputStreams[channelIndex]->WriteShort(&numChannels, 1);
					u32 sampleRate = kMixerNativeSampleRate;
					m_OutputStreams[channelIndex]->WriteInt(&sampleRate, 1);
					u32 byteRate = sampleRate * numChannels * 16 / 8;
					m_OutputStreams[channelIndex]->WriteInt(&byteRate, 1);
					short blockA = 2;
					m_OutputStreams[channelIndex]->WriteShort(&blockA, 1);
					short bitsPerSample = 16;
					m_OutputStreams[channelIndex]->WriteShort(&bitsPerSample, 1);

					const unsigned char dataChunk[] = {'d', 'a', 't', 'a'};
					m_OutputStreams[channelIndex]->WriteByte(dataChunk, 4);
					size = 0;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);

				}
			}
			s16 sh = (s16)(Clamp(data,-1.f,1.f) * 32767.f);
			m_OutputStreams[channelIndex]->WriteShort(&sh,1);
			m_NumOutputDataBytes[channelIndex] += 2;
		}
		else
		{
			if(m_OutputStreams[channelIndex])
			{
				int size = m_NumOutputDataBytes[channelIndex] + 36;
				m_OutputStreams[channelIndex]->Seek(4);
				m_OutputStreams[channelIndex]->WriteInt(&size, 1);
				size = m_NumOutputDataBytes[channelIndex];
				m_OutputStreams[channelIndex]->Seek(40);
				m_OutputStreams[channelIndex]->WriteInt(&size, 1);
				m_OutputStreams[channelIndex]->Close();
				m_OutputStreams[channelIndex] = NULL;
			}
		}
	}
#endif

} // namespace rage
#endif // __WIN32
