// 
// audiohardware/cmdbuffer.cpp
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#include "audiohardware/channel.h"
#include "cmdbuffer.h"
#include "math/amath.h"
#include "system/memops.h"

namespace rage
{

	bool audCommandBuffer::WritePacket(const audCommandPacketHeader *header)
	{
		AUD_CMD_BUFFER_LOCK(m_CritSec);

		Assert(header->systemId <= kSystemIdPadding);
		Assert(header->packetSize != 0);
#if RSG_ASSERT
		const sysIpcThreadId thisThreadId = sysIpcGetThreadId();
		Assert(m_LastWriteThreadId == NULL || m_LastWriteThreadId == thisThreadId);
		m_LastWriteThreadId = thisThreadId;
#endif

		if(Verifyf(header->packetSize <= GetAvailableSpace(), "Failed to write packet to cmd buffer [%s], packetSize: %u [%u remaining, %u used]", m_Name, header->packetSize, GetAvailableSpace(), m_BufferSize - GetAvailableSpace()))
		{
			volatile const u32 newOffset = m_CurrentOffset + header->packetSize;
			sysMemCpy(&m_Buffer[m_CurrentOffset], header, header->packetSize);
			m_HighWatermark = Max(m_HighWatermark, newOffset);
			m_CurrentOffset = newOffset;
			return true;
		}
		else
		{

		}
		
		return false;
	}

	bool audCommandBuffer::WriteAlignedPacket(const audCommandPacketHeader *packetData)
	{
		AUD_CMD_BUFFER_LOCK(m_CritSec);

		Assert(packetData->systemId < kSystemIdPadding);

		if(m_CurrentOffset&15)
		{
			const u32 padBytes = 16 - (m_CurrentOffset&15);
			audCommandPacketHeader paddingPacket;
			paddingPacket.systemId = kSystemIdPadding;

			const u32 extraPacketSize = (padBytes - sizeof(audCommandPacketHeader) + 16) & 15;

			paddingPacket.packetSize = extraPacketSize + sizeof(audCommandPacketHeader);

			if(Verifyf(GetAvailableSpace() > paddingPacket.packetSize, "Failed to write aligned packet padding to cmd buffer [%s], packetSize: %u [%u remaining, %u used]", m_Name, paddingPacket.packetSize, m_BufferSize - m_CurrentOffset, m_CurrentOffset))
			{
				WritePacket(&paddingPacket);

				audAssertf((m_CurrentOffset&15) == 0, "Failed to align packet: padBytes %u currentOffset %u headerSize %zu extraSize %u", padBytes, m_CurrentOffset, sizeof(audCommandPacketHeader), extraPacketSize);
			
				
				return WritePacket(packetData);
			}
			else
			{
				return false;
			}			
		}
		Assert((m_CurrentOffset&15) == 0);
		return WritePacket(packetData);
	}

	audCommandBuffer::audCommandBuffer()
		: m_Buffer(NULL)
		, m_BufferSize(0)
		, m_CurrentOffset(0)
		, m_HighWatermark(0)
		ASSERT_ONLY(,m_LastWriteThreadId(NULL))
	{
		
	}

	audCommandBuffer::~audCommandBuffer()
	{

	}

	void audCommandBuffer::ForEachPacket(ForEachCommandFunctor functor, const u32 timestamp, const EmptyBufferFlag reset)
	{
		AUD_CMD_BUFFER_LOCK(m_CritSec);

		u32 offset = 0;
		while(offset < m_CurrentOffset)
		{
			audCommandPacketHeader *header = (audCommandPacketHeader *)(&m_Buffer[offset]);
			// Skip padding
			if(header->systemId != kSystemIdPadding)
			{
				functor(timestamp, header);
			}
			Assert(header->packetSize != 0);
			// Prevent infinite loop if we encounter a malformed packet
			if(header->packetSize == 0)
			{
				break;
			}
			offset += header->packetSize;
		}
		if(reset == kEmpty)
		{
			Reset();
		}
	}

	void audCommandBuffer::Reset()
	{
		AUD_CMD_BUFFER_LOCK(m_CritSec);
		m_CurrentOffset = 0;
	}

	void audCommandBuffer::Init(const u32 bufferSize, const char *ASSERT_ONLY(cmdBufferName))
	{
		audAssertf((bufferSize&15) == 0, "Buffer size must be 16-byte aligned (%u / %u)", bufferSize, (bufferSize&15U));
		m_Buffer = rage_aligned_new(16) u8[bufferSize];
		m_BufferSize = bufferSize;
		m_CurrentOffset = 0;
		// Client thread starts off in a 'ready to write' state
		m_ProcessedSemaphore = sysIpcCreateSema(1);
		m_ReadyForProcessing = false;
		m_Timestamp = 0;
		m_SubmitTime = 0;
		ASSERT_ONLY(m_Name = cmdBufferName);
	}

	void audCommandBuffer::Shutdown()
	{
		delete[] m_Buffer;
		m_Buffer = NULL;
	}

} // namespace rage
