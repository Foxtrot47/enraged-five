//
// audiohardware/decoder_atrac9.cpp
//
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved.
//

#include "driverdefs.h"
#if RSG_ORBIS

#include "decoder_atrac9.h"
#include "decodemgr.h"
#include "file/asset.h"
#include "file/stream.h"

#include "system/endian.h"
#include "vectormath/vectormath.h"

#include <libsysmodule.h>

namespace rage
{
	using namespace Vec;

	bool audDecoderAtrac9::InitClass()
	{
		s32 rv;

		rv = sceAudiodecInitLibrary(SCE_AUDIODEC_TYPE_AT9);
		if(rv < 0)
		{
			return false;
		}
		
		return true;
	}

	bool audDecoderAtrac9::Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate))
	{
		m_NumChannels = numChannels;
		m_Format = format;
		m_State = audDecoder::IDLE;

		m_SubmitPacketIndex = 0;
		m_DecodePacketIndex = 0;

#if DUMP_RAW_ATRAC_STREAMS
		static int filenameCount = 0;
		char filename[256];
		sprintf(filename, "X:\\test%d", filenameCount);
		m_fp = ASSET.Create(filename, "raw");
		filenameCount++;
#endif
		m_Handle = AUDIODEC_HANDLE_INVALID;
		m_TotalSamples = 0;
		
		return true;
	}

	void audDecoderAtrac9::Shutdown()
	{
		if(m_Handle != AUDIODEC_HANDLE_INVALID) 
		{
			int	tmp = sceAudiodecDeleteDecoder(m_Handle);
			if(tmp < 0) 
			{
				Errorf("sceAudiodecDeleteDecoder() failed: 0x%08X\n", tmp);
			}
			m_Handle = AUDIODEC_HANDLE_INVALID;
		}

#if DUMP_RAW_ATRAC_STREAMS
		if(m_fp)
			m_fp->Close();
#endif
	}

	void audDecoderAtrac9::ShutdownClass()
	{
		s32 tmp;

		// unregister all the codecs from libaudiodec
		tmp = sceAudiodecTermLibrary(SCE_AUDIODEC_TYPE_AT9);
		if (tmp < 0) 
		{
			Displayf("error: sceAudiodecTermLibrary() failed: 0x%08X\n", tmp);
		}

	}

	void audDecoderAtrac9::SubmitPacket(audPacket &packet)
	{
		if(m_Handle == AUDIODEC_HANDLE_INVALID)
		{
			m_Ctrl.pParam = reinterpret_cast<void *>(&m_Param);
			m_Param.uiSize = sizeof(m_Param);
			m_Param.iBwPcm = SCE_AUDIODEC_WORD_SZ_16BIT;
			//memcpy(m_param.at9.uiConfigData, header.fmtChunk.configData, sizeof(m_param.at9.uiConfigData));
			//u8* p = (u8*)&packet.LoopBegin; 
			m_Param.uiConfigData[0] = 254;	//m_param.uiConfigData[0] = p[1];//254;
			m_Param.uiConfigData[1] = 112;	//m_param.uiConfigData[1] = p[0];//112;
			//p = (u8*)&packet.LoopEnd;		// this doesn't actually get built into the daya, just LoopBegin
			m_Param.uiConfigData[2] = 7;	//m_param.uiConfigData[2] = p[3];//7;
			m_Param.uiConfigData[3] = 240;	//m_param.uiConfigData[3] = p[2];//240;
			m_Ctrl.pBsiInfo = reinterpret_cast<void *>(&m_Info);
			m_Info.uiSize = sizeof(m_Info);
			m_Ctrl.pAuInfo = &m_Bst;
			m_Bst.uiSize = sizeof(m_Bst);
			m_Ctrl.pPcmItem = &m_Pcm;
			m_Pcm.uiSize = sizeof(m_Pcm);

			m_Handle = sceAudiodecCreateDecoder(&m_Ctrl, SCE_AUDIODEC_TYPE_AT9);
			if (m_Handle < 0) 
			{
				Errorf("error: sceAudiodecCreateDecoder() failed: 0x%08X\n", m_Handle);
				m_State = audDecoder::DECODER_ERROR;
			}
			else
			{
				m_State = audDecoder::DECODING;
			}
		}
		m_Packets[m_SubmitPacketIndex].InputBytes = packet.inputBytes;
		m_Packets[m_SubmitPacketIndex].InputData = (u8*)packet.inputData;
		m_Packets[m_SubmitPacketIndex].NumSamplesConsumed = 0;
		m_Packets[m_SubmitPacketIndex].TotalBytes = 0;
		m_Packets[m_SubmitPacketIndex].LastPacketSize = packet.PlayEnd;
		m_Packets[m_SubmitPacketIndex].NumBlocksAvailable = packet.inputBytes/kAtrac9BlockSize;
		m_Packets[m_SubmitPacketIndex].SubPacket = 0;
		m_Packets[m_SubmitPacketIndex].BlockStartSample = packet.BlockStartSample;

		//if((m_Packets[m_SubmitPacketIndex].NumBlocksAvailable * kAtrac9BlockSize) < packet.inputBytes) // last packet is a sub packet
		//{
		//	// this is likely speech, we need to calculate the size of the last block, LoopEnd holds the number of samples for speech, 
		//	// PlayEnd is only non 0 when we are on the last block of a stream(PC only, waveplayer.cpp ln 689, IsFinalBlock)
		//	if(packet.LoopEnd != 0 && packet.PlayEnd == 0) 
		//	{
		//		m_Packets[m_SubmitPacketIndex].PlayEnd = packet.LoopEnd - (m_Packets[m_SubmitPacketIndex].NumBlocksAvailable * 4088); 
		//	}
		//	m_Packets[m_SubmitPacketIndex].NumBlocksAvailable++; // add partial block, only if not a multiple of kAtrac9BlockSize
		//}

		// check if we have duplicated any packets and need to skip ones
		if(packet.PlayBegin > 0)
		{
			// calculate number of packets to skip
			u32 packetsToSkip = packet.PlayBegin / (kAtrac9BlockSize * 8);
			m_Packets[m_SubmitPacketIndex].NumSamplesConsumed = packetsToSkip*kAtrac9DecompressedSamplesPerBlock;
			m_Packets[m_SubmitPacketIndex].NumBlocksAvailable -= packetsToSkip;
		}

		//Displayf("SubmitPacket %d: bytes:%d blocks:%d skip:%d", m_SubmitPacketIndex, m_Packets[m_DecodePacketIndex].InputBytes, m_Packets[m_DecodePacketIndex].NumBlocksAvailable, packet.PlayBegin);
		Displayf("SubmitPacket %d: BlockStartSample:%d", m_SubmitPacketIndex, m_Packets[m_DecodePacketIndex].BlockStartSample);

		m_SubmitPacketIndex = (m_SubmitPacketIndex + 1) % m_Packets.size();
	}

	void audDecoderAtrac9::Update()
	{
		const u32 ringBufferFreeSpace = (m_RingBuffer.GetBufferSize() - m_RingBuffer.GetBytesAvailableToRead());// >> 1;

		// see if there is enough free space in the ring buffer to decode the next block
		if(ringBufferFreeSpace > kAtrac9DecompressedBytesPerBlock && m_State != audDecoder::FINISHED)
		{
			bool done = false;
			while((m_RingBuffer.GetBufferSize() - m_RingBuffer.GetBytesAvailableToRead()) >= 512 && !done)
			{
				// if we have at least 1 block worth of data to decode and we have some data left
				if(m_Packets[m_DecodePacketIndex].NumSamplesConsumed < m_Packets[m_DecodePacketIndex].InputBytes)
				{
					//Displayf("Decode %d:blocks:%d", m_DecodePacketIndex, m_Packets[m_DecodePacketIndex].NumBlocksAvailable);
					static u8 s_DecodeBuffer[kAtrac9DecompressedBytesPerBlock];

					sysMemSet(s_DecodeBuffer, 0, kAtrac9DecompressedBytesPerBlock);

					// set bitstream and pcm buffer
					m_Bst.pAuAddr = const_cast<uint8_t *>(((u8*)m_Packets[m_DecodePacketIndex].InputData+m_Packets[m_DecodePacketIndex].NumSamplesConsumed));
					m_Bst.uiAuSize = kAtrac9BlockBytesToDecode;
					m_Pcm.pPcmAddr = &s_DecodeBuffer;
					m_Pcm.uiPcmSize = kAtrac9DecompressedBytesPerBlock;

					// decode audio bitstream
					int ret = sceAudiodecDecode(m_Handle, &m_Ctrl);
					if (ret < 0) 
					{
						Errorf("error: sceAudiodecDecode() failed: 0x%08X\n", ret);
						m_State = audDecoder::FINISHED;
						done = true;
					}

					//	Displayf("decode: %u = %u\n", m_ctrl.pAuInfo->uiAuSize, m_ctrl.pPcmItem->uiPcmSize);
					u32 packetSizeBytes = m_Pcm.uiPcmSize;
					//u32 totalSamples = (m_Packets[m_DecodePacketIndex].PlayEnd == 0 ? 0 : m_Packets[m_DecodePacketIndex].LastPacketSize - 1) * 2;
					//if(totalSamples != 0 && (m_Packets[m_DecodePacketIndex].TotalBytes + m_pcm.uiPcmSize) >= totalSamples)
					//{
					//	packetSizeBytes = totalSamples - m_Packets[m_DecodePacketIndex].TotalBytes;
					//	m_State = audDecoder::FINISHED;
					//	done = true;
					//}

					// check if we are on the very last packet
					m_RingBuffer.WriteData(s_DecodeBuffer, packetSizeBytes);

					m_TotalSamples += (packetSizeBytes/2);
					m_Packets[m_DecodePacketIndex].TotalBytes += packetSizeBytes;

#if DUMP_RAW_ATRAC_STREAMS
					if(m_fp)
					{
						s16* sample = (s16*)s_DecodeBuffer;
						for(u32 i=0; i<packetSizeBytes/2; i++)
						{
							//fwrite(sample, 2, 1, m_fp);
							m_fp->WriteShort(sample,1);
							sample++;
						}
					}
#endif

					m_Packets[m_DecodePacketIndex].SubPacket += m_Bst.uiAuSize;
					m_Packets[m_DecodePacketIndex].NumSamplesConsumed += m_Bst.uiAuSize;

					//Displayf("Decoded %d to %d - consumed %d", m_bst.uiAuSize, m_pcm.uiPcmSize, m_Packets[m_DecodePacketIndex].NumSamplesConsumed);

					if(m_Packets[m_DecodePacketIndex].LastPacketSize != ~0U && m_Packets[m_DecodePacketIndex].LastPacketSize != 0 && m_Packets[m_DecodePacketIndex].NumBlocksAvailable <= 1 && m_Packets[m_DecodePacketIndex].SubPacket >= m_Packets[m_DecodePacketIndex].LastPacketSize)
					{
						audDisplayf("Total Smaples = %u", m_TotalSamples);
						m_State = audDecoder::FINISHED;
						done = true;
					}
					//if(m_Packets[m_DecodePacketIndex].NumSamplesConsumed*8 >= totalSamples && m_Packets[m_DecodePacketIndex].LastPacketSize != 0)
					//{
					//	audDisplayf("Total Smaples = %u", m_TotalSamples);
					//	m_State = audDecoder::FINISHED;
					//	done = true;
					//}
					if(m_Packets[m_DecodePacketIndex].SubPacket >= kAtrac9BlockSize)
					{
						m_Packets[m_DecodePacketIndex].NumBlocksAvailable--;
						m_Packets[m_DecodePacketIndex].SubPacket = 0;
						audDisplayf("Blocks %d %d %d", m_Packets[m_DecodePacketIndex].NumBlocksAvailable, m_Packets[m_DecodePacketIndex].NumSamplesConsumed, m_TotalSamples);
					}

					if(m_Packets[m_DecodePacketIndex].NumBlocksAvailable == 0)
					{
						if(m_Packets[m_DecodePacketIndex].LastPacketSize != ~0U)
						{
							m_State = audDecoder::FINISHED;
							audDisplayf("Total Smaples = %u", m_TotalSamples);
						}
						else
						{
							m_DecodePacketIndex = (m_DecodePacketIndex + 1) % m_Packets.size();
							audDisplayf("Switch");
						}
						done = true;
					}
				}
				else
				{
					done = true;
				}
			}
		}
	}

	u32 audDecoderAtrac9::QueryNumAvailableSamples() const
	{
		return (m_RingBuffer.GetBytesAvailableToRead()>>1);
	}

	bool audDecoderAtrac9::QueryReadyForMoreData() const
	{
		return (m_Packets[m_DecodePacketIndex].NumBlocksAvailable == 0);
	}

	u32 audDecoderAtrac9::ReadData(s16 *buf, const u32 numSamples) const
	{
		return m_RingBuffer.PeakData(buf, numSamples<<1)>>1;
	}

	void audDecoderAtrac9::AdvanceReadPtr(const u32 numSamples)
	{
		m_RingBuffer.AdvanceReadPtr(numSamples<<1);
	}

} // namespace rage

#endif // RSG_ORBIS