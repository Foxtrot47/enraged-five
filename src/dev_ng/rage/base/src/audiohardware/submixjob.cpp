// 
// audiohardware/submixjob.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 


#define AUD_DISABLE_ASSERTS __SPU

#if AUD_DISABLE_ASSERTS
#undef __ASSERT
#define __ASSERT 0
#undef ASSERT_ONLY
#define ASSERT_ONLY(x)


#undef Assert
#define Assert(x)
#undef Assertf
#define Assertf(x,...)
#undef FastAssert
#define FastAssert(x)
#undef AssertMsg
#define AssertMsg(x,msg)
#undef AssertVerify
#define AssertVerify(x) (x)
#undef Verifyf
#define Verifyf(x,fmt,...) (x)
#undef DebugAssert
#define DebugAssert(x)
#endif

#include "system/taskheader.h"

#if __SPU

#define AUD_MULTICHANNEL_DSP_SPU

#include "driverdefs.h"
#include "mixer.h"
#include "submix.h"
#include "submixcache.h"
#define __AUD_STRIP_CMD_BUFFER
#include "connection.cpp"
#include "framebuffercache.h"
#include "framebufferpool.h"
#include "submix.cpp"
#include "submixcache.cpp"
#include "submixjob.h"
#include "atl/array.h"

#include "dspeffect.cpp"
#include "audioeffecttypes/biquadfiltereffect.cpp"
#include "audioeffecttypes/compressoreffect.cpp"
#include "audioeffecttypes/delayeffect.cpp"
#include "audioeffecttypes/ereffect.cpp"
#include "audioeffecttypes/variabledelayeffect.cpp"
#include "audioeffecttypes/r360limitereffect.cpp"
#include "audioeffecttypes/reverbeffect.cpp"
#include "audioeffecttypes/reverbeffect4.cpp"
#include "audioeffecttypes/reverbeffect_prog.cpp"
#include "audioeffecttypes/underwatereffect.cpp"
#include "audioeffecttypes/waveshapereffect.cpp"
#include "audiohardware/src_linear.cpp"
#include "audiosynth/biquadfilter.cpp"

#include "audioengine/spuutil.h"

#include "voicefilter_vmath.inl"

namespace rage
{
	extern audMixerConnectionPool *g_MixerConnectionPool;
	audFrameBufferCache *g_FrameBufferCache = NULL;
	audFrameBufferPool *g_FrameBufferPool = NULL;
	bool g_DebugFrameBufferCache = false;
	bool g_DisableAllVoiceProcessing = false;
	bool g_DisableVoiceFilters = false;

	bool g_MeterSubmixes = false;

	void SubmixJobEntry(sysTaskParameters &p)
	{
		CompileTimeAssert(sizeof(submixInputData)==4);
		submixInputData inputData;

		inputData.val32 = p.UserData[0].asUInt;
		void *submixesEA = p.UserData[1].asPtr;
		const u32 frameBufferPoolEA = p.UserData[2].asUInt;
		const u32 numFrameBuffers = p.UserData[3].asUInt;
		
		const bool debug = p.UserData[6].asBool;
		const u32 numCacheBuffers = p.UserData[7].asUInt;
		g_MeterSubmixes = p.UserData[8].asBool;
		
		s32 numProcessingStages;
		u32 numSubmixCacheChannels;
		u32 numOutputChannels;
		audOutputMode downmixMode;
		audOutputMode hardwareOutputMode;

		Assign(hardwareOutputMode, inputData.fields.hardwareOutputMode);
		Assign(downmixMode, inputData.fields.downmixMode);
		Assign(numOutputChannels, inputData.fields.numOutputChannels);
		Assign(numProcessingStages, inputData.fields.numProcessingStages);
		Assign(numSubmixCacheChannels, inputData.fields.numSubmixCacheChannels);
		
				
		const u32 frameBufferPoolSizeBytes = sizeof(audFrameBufferPool);
		
		DEV_ONLY(g_DisplayAllocations = g_DebugFrameBufferCache = debug);

		InitScratchBuffer(p.Scratch.Data, p.Scratch.Size);

		// connection pool is in/out
		g_MixerConnectionPool = (audMixerConnectionPool*)p.Input.Data;
		Assert(p.Output.Data == NULL && p.Output.Size == 0);

		DEV_ONLY(if(g_DisplayAllocations) audDisplayf("ConnectionPool %zu", p.Input.Size));

		// DMA into scratch...
		const s32 inputTag = 4;

		g_FrameBufferPool = (audFrameBufferPool*)AllocateFromScratch(frameBufferPoolSizeBytes, "FrameBufferPool");
		Assert((frameBufferPoolEA&15)==0);
		Assert((frameBufferPoolSizeBytes&15)==0);
	
		sysDmaGet(g_FrameBufferPool, frameBufferPoolEA, frameBufferPoolSizeBytes, inputTag);

		// grab submix array into scratch		
		size_t size = sizeof(atFixedArray<audMixerSubmix,kMaxSubmixes>);
		atFixedArray<audMixerSubmix,kMaxSubmixes> *submixes = (atFixedArray<audMixerSubmix,kMaxSubmixes> *)AllocateFromScratch(size, "Submixes");
		Assert(((size_t)submixesEA&15)==0);
		Assert((size&15)==0);
		
		sysDmaGet(submixes, (uint64_t)submixesEA, size, inputTag);
		audFrameBufferCache frameBufferCache;
		g_FrameBufferCache = &frameBufferCache;


		audSubmixCache submixCache;
		g_SubmixCache = &submixCache;

		g_SubmixCache->SetSubmixes(submixes);

		const u32 cacheStorageSizeBytes = numCacheBuffers*sizeof(f32)*kMixBufNumSamples;
		f32 *cacheStorage = (f32*)AllocateFromScratch(cacheStorageSizeBytes, "FrameBufferCache");
		
		SetScratchBookmark();
			
		sysDmaWait(1<<inputTag);

		// Init buffer cache
		Assert(g_FrameBufferPool->GetNumBuffers() == p.UserData[3].asUInt);
		frameBufferCache.Init(g_FrameBufferPool->GetBufferEA(0), numFrameBuffers, cacheStorage, numCacheBuffers, 3);

		if(debug)
		{
			audDisplayf("%zu free for DSP use", g_ScratchLeft);
		}
		
		// Prep the submix cache with allocations mades previously
		for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = submixes->begin(); i != submixes->end(); i++)
		{
			(*i).NotifyCache();
		}

		// mix submixes
		for(s32 stage = 0; stage <= numProcessingStages; stage++)
		{
			for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = submixes->begin(); i != submixes->end(); i++)
			{
				if((*i).GetProcessingStage() == stage)
				{
					(*i).Mix(g_ScratchBuf, g_ScratchLeft);
				}
			}
			g_SubmixCache->FreeBuffers((u32)stage);
		}
		g_SubmixCache->ClearAllAllocations();

		if(debug)
		{
			audDisplayf("Submixes - Fetched: %u Written: %u WB: %u", frameBufferCache.GetNumFetches(), frameBufferCache.GetNumWrites(), frameBufferCache.GetNumWriteBacks());
			frameBufferCache.ResetStats();
		}

		atRangeArray<u32,kMaxSubmixes> voiceCounts;
		sysMemZeroBytes<sizeof(voiceCounts)>(&voiceCounts);
		// call postmix on all submixes
		for(s32 i = 0; i < submixes->GetCount(); i++)
		{
			voiceCounts[i] = (*submixes)[i].GetVoiceCount();
			(*submixes)[i].PostMix();
		}

		// Update main ram voice count array
		const s32 outputTag = 5;
		sysDmaPut(&voiceCounts, (uint64_t)p.UserData[9].asPtr, sizeof(u32) * kMaxSubmixes, outputTag);

		// Update frame buffer pool
		sysDmaPut(g_FrameBufferPool, frameBufferPoolEA, frameBufferPoolSizeBytes, outputTag);

		// interleave final mix
		const u32 final8ChannelMixSize = kMixBufNumSamples * 8 * sizeof(f32);
		
		f32 *interleavedMix = reinterpret_cast<f32*>(AllocateFromScratch(final8ChannelMixSize, "FinalMixBuf"));
		
		Assert(numOutputChannels == 6);

		audMixerSubmix &masterSubmix = (*submixes)[0];
		
		const Vector_4V *inFL = (const Vector_4V *)masterSubmix.GetMixBuffer(RAGE_SPEAKER_ID_FRONT_LEFT);
		const Vector_4V *inFR = (const Vector_4V *)masterSubmix.GetMixBuffer(RAGE_SPEAKER_ID_FRONT_RIGHT);
		const Vector_4V *inC = (const Vector_4V *)masterSubmix.GetMixBuffer(RAGE_SPEAKER_ID_FRONT_CENTER);
		const Vector_4V *inLFE = (const Vector_4V *)masterSubmix.GetMixBuffer(RAGE_SPEAKER_ID_LOW_FREQUENCY);
		const Vector_4V *inRL = (const Vector_4V *)masterSubmix.GetMixBuffer(RAGE_SPEAKER_ID_BACK_LEFT);
		const Vector_4V *inRR = (const Vector_4V *)masterSubmix.GetMixBuffer(RAGE_SPEAKER_ID_BACK_RIGHT);

		Vector_4V *destPtr = (Vector_4V*)interleavedMix;

		if(downmixMode == AUD_OUTPUT_STEREO)
		{
			// FL = (FL) + 0.707(FC) + 0.707(RL)
			// FR = (FR) + 0.707(FC) + 0.707(RR)

			// 0.707f
			const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();
			const Vector_4V zero = V4VConstant(V_ZERO);

			for(u32 i = 0; i < kMixBufNumSamples; i += 4)
			{
				// load four samples per channel
				//NOTE: we also reorder the channels here, from RAGE Audio FL,FR,SL,SR,C,LFE to 
				// libaudio FL,FR,C,LFE,SL,SR,BL,BR
				const Vector_4V fl = *inFL++;
				const Vector_4V fr = *inFR++;
				const Vector_4V rl = *inRL++;
				const Vector_4V rr = *inRR++;
				const Vector_4V fc = *inC++;
				
				Vector_4V fcContrib = V4Scale(fc, centreRearRatio);
				const Vector_4V inSamplesFL = V4Add(fl, V4AddScaled(fcContrib, rl, centreRearRatio));
				const Vector_4V inSamplesFR = V4Add(fr, V4AddScaled(fcContrib, rr, centreRearRatio));

				const Vector_4V inSamplesLR0 = V4Clamp(V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesFL, inSamplesFR), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				const Vector_4V inSamplesLR1 = V4Clamp(V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesFL, inSamplesFR), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				
				// Silence all other channels
		
				// first pair of samples
				*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR0, zero);
				*destPtr++ = zero;
				*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR0, zero);
				*destPtr++ = zero;

				// second pair of samples
				*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR1, zero);
				*destPtr++ = zero;
				*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR1, zero);
				*destPtr++ = zero;
			}
		}
		else
		{
			for(u32 i = 0; i < kMixBufNumSamples; i += 4)
			{
				// load four samples per channel
				//NOTE: we also reorder the channels here, from RAGE Audio FL,FR,SL,SR,C,LFE to 
				// libaudio FL,FR,C,LFE,SL,SR,BL,BR
				const Vector_4V inSamplesFL =  V4Clamp(*inFL++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				const Vector_4V inSamplesFR = V4Clamp(*inFR++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				const Vector_4V inSamplesRL =  V4Clamp(*inRL++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				const Vector_4V inSamplesRR =  V4Clamp(*inRR++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				const Vector_4V inSamplesC =  V4Clamp(*inC++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				const Vector_4V inSamplesLFE = V4Clamp(*inLFE++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));

				const Vector_4V inSamplesLR0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesFL, inSamplesFR);
				const Vector_4V inSamplesLR1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesFL, inSamplesFR);

				const Vector_4V inSamplesCS0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesC, inSamplesLFE);
				const Vector_4V inSamplesCS1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesC, inSamplesLFE);

				const Vector_4V inSamplesLSLR0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesRL, inSamplesRR);
				const Vector_4V inSamplesLSLR1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesRL, inSamplesRR);					

				// first pair of samples
				*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR0, inSamplesCS0);
				*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLSLR0, V4VConstant(V_ZERO));
				*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR0, inSamplesCS0);
				*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLSLR0, V4VConstant(V_ZERO));
				
				// second pair of samples
				*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR1, inSamplesCS1);
				*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLSLR1, V4VConstant(V_ZERO));
				*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR1, inSamplesCS1);
				*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLSLR1, V4VConstant(V_ZERO));			
			}
		}

		sysDmaPut(interleavedMix, (uint64_t)g_FrameBufferPool->GetBufferEA(0), final8ChannelMixSize, outputTag);

		sysDmaPut(submixes, (uint64_t)submixesEA, size, outputTag);

		sysDmaWaitTagStatusAll(1<<outputTag);

		if(debug)
		{
			audDisplayf("Final - Fetched: %u Written: %u WB: %u", frameBufferCache.GetNumFetches(), frameBufferCache.GetNumWrites(), frameBufferCache.GetNumWriteBacks());
		}
	}

} // namespace rage
#endif // __SPU

using namespace rage;
void SubmixJob(sysTaskParameters &SPU_ONLY(p))
{
	SPU_ONLY(rage::SubmixJobEntry(p));
}

