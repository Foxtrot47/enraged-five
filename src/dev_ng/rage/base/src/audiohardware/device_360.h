//
// audiohardware/device_360.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DEVICE_360_H
#define AUD_DEVICE_360_H

#if __XENON //|| RSG_DURANGO

#include "device.h"

#include "system/ipc.h"
#include "system/xtl.h"
#include "grcore/config.h"

#if RSG_PC && __D3D11_1 // 	#if (_WIN32_WINNT < 0x0602) //_WIN32_WINNT_WIN8
	// Can not use windows 8 sdk audio for windows 7.  Need a better solution than this
	#include <C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include\xaudio2.h>
#else
	#include <xaudio2.h>
#endif
#define AUD_OUTPUT_TO_FILE 0

namespace rage
{
	class sysMemAllocator;
	class audXAudio2Instance : public IXAudio2VoiceCallback
	{
	public:
		enum InstanceType {
			Main = 0,
			RestrictedContent,
		};

		audXAudio2Instance();
		virtual ~audXAudio2Instance() {}
		

		bool Init(const InstanceType type);
		void Shutdown();

		//////////////////////////////////////////////////////////
		// IXAudio2VoiceCallback interface
		STDMETHOD_(void, OnBufferStart)(void *pContext);

		STDMETHOD_(void, OnVoiceProcessingPassStart)(UINT32){}
		STDMETHOD_(void, OnVoiceProcessingPassEnd)(void){}
		STDMETHOD_(void, OnStreamEnd)(void){}
		STDMETHOD_(void, OnBufferEnd)(void *){}
		STDMETHOD_(void, OnLoopEnd)(void *){}
		STDMETHOD_(void, OnVoiceError)(void *,HRESULT){}
		///////////////////////////////////////////////////////////

		IXAudio2 *GetXAudio2()	{ return m_XAudio2; }

		void StartMixing();

		sysIpcSema &GetSema()	{ return m_NeedDataSema; }
		
		void OnBufferReady();
		void StartEngine()		{ 
									if(m_XAudio2)
									{
										HRESULT hr = m_XAudio2->StartEngine();
										if(hr != S_OK)
										{
											Displayf("m_XAudio2->StartEngine() error %x", hr);
										}
									}
								}	

		void StopEngine()		{	if(m_XAudio2) m_XAudio2->StopEngine();	}

		void WaitOnGotData()	{ sysIpcWaitSema(m_GotDataSema); }
		void SignalNeedData()	{ sysIpcSignalSema(m_NeedDataSema); }
		
#if __BANK
		void DumpOutput(u32 channelIndex, f32 data); 
#endif

	private:		

		sysIpcSema m_NeedDataSema;
		sysIpcSema m_GotDataSema;

		IXAudio2 *m_XAudio2;
		IXAudio2MasteringVoice *m_MasteringVoice;
		IXAudio2SourceVoice *m_OutputVoice;
		f32 *m_OutputBuffer;

		sysMemAllocator *m_Allocator;
		InstanceType m_Type;
		u32 m_NumChannels;
		u32 m_NumHardwareChannels;
		u32 m_NumRealHardwareChannels;
		u32 m_BufferIndex;
		u32 m_OutputBufferSizeBytes;

		static bool sm_HaveMasterInstance;
		bool m_MasterInstance;

		static const u32 kMaxHardwareChannels = 8;

#if __BANK
		atRangeArray<u32, kMaxHardwareChannels> m_NumOutputDataBytes;
		atRangeArray<fiStream *, kMaxHardwareChannels> m_OutputStreams;
#endif
	};

	
	class fiStream;
	class audMixerDeviceXAudio2 : public audMixerDevice
	{
	public:

		audMixerDeviceXAudio2();

		// PURPOSE
		//	Initialises XAudio2 output hardware
		// RETURNS
		//	false on failure
		virtual bool InitHardware();

		// PURPOSE
		//	Releases XAudio2 output resources
		virtual void ShutdownHardware();

		// PURPOSE
		//	Starts generating output
		virtual void StartMixing();
		virtual void StartEngine();
		virtual void StopEngine();

		virtual void SuspendedMixerUpdate();

		IXAudio2 *GetXAudio2() { return m_XAudio2Instance.GetXAudio2(); }

		static void SetSuspended(bool suspend);

	private:

		static DECLARE_THREAD_FUNC(sMixLoop);

		void MixLoop();

	private:
		audXAudio2Instance m_XAudio2Instance;
#if RSG_DURANGO
		static ServiceDelegate sm_Delegate;
		static void HandlePlmChange(sysServiceEvent* evt);

		static DECLARE_THREAD_FUNC(ThreadEntryProc);
#endif

	private:

#if RSG_DURANGO
		audXAudio2Instance m_RestrictedInstance;
#endif

		sysMemAllocator *m_Allocator;

		u32 m_NumHardwareChannels;
		u32 m_NumRealHardwareChannels;
		u32 m_CurrentBufferIdx;
#if RSG_DURANGO
		static volatile bool	sm_IsThreadRunning, sm_ShouldThreadBeRunning;
		static sysIpcThreadId	sm_ThreadId;
		static sysPerformanceTimer* sm_PerfTimer;
#endif

#if AUD_OUTPUT_TO_FILE
		fiStream *m_DebugOutput;
#endif
		
	};

} // namespace rage

#endif // __WIN32
#endif // AUD_DEVICE_360_h
