// 
// audiohardware/decoder_ps4mp3.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DECODER_PS4_MP3_2_H
#define AUD_DECODER_PS4_MP3_2_H

#if RSG_ORBIS

#include "decoder.h"
#include "mp3util.h"
#include "ringbuffer.h"
#include <audiodec.h>
#include <ajm.h>
#include <ajm/mp3_decoder.h>

#define DUMP_RAW_MP3_STREAMS			0	// dump raw streams to a file as they play
#define DUMP_RAW_MP3_BLOCKS				0	// dump each block separately as they play
#define ENABLE_MP3_DEBUG				0	// enable some debug spew

#if DUMP_RAW_MP3_BLOCKS
#undef DUMP_RAW_MP3_STREAMS
#define DUMP_RAW_MP3_STREAMS			1
#endif

#define AUD_MP3_BUFFERS					8	
#define AUD_MAX_MP3_DECODES_PER_FRAME	4

namespace rage
{
class audDecoderPS4Mp3 : public audDecoder
{
public:

	enum{kPS4NumFramesInOutputBuffer = 4};

	enum audMp3DecodeState 
	{
		MP3_DECODE_STATE_SUBMIT = 0,
		MP3_DECODE_STATE_CONSUME
	};

	struct audDecoderMp3Packet
	{
		u8 *InputData;
		u32 InputBufferSize;
		u32 InputBufferBytesRead;
		bool IsFinalBlock;
		bool SkipFirstFrame;
		u32 SamplesConsumed;

		u32 PlayBegin;
		u32 PlayEnd;
		u32 LoopStartOffsetBytes;
		u32 LoopBegin;
		u32 LoopEnd;
		u32 LoopBeginSamples;

		s16 OutputBuffer[SCE_AJM_DEC_MP3_MAX_FRAME_SAMPLES * kPS4NumFramesInOutputBuffer]; // mono channel
		u8 BatchBuffer[SCE_AJM_JOB_DECODE_SIZE];

		SceAjmDecodeResult DecodeResult;
		SceAjmBatchInfo BatchInfo;
		SceAjmBatchId BatchId;

		audMp3DecodeState State; // sumbit to decode, or consuming after decode

#if __BANK
		u32 PacketNumber;
#endif
	};


	static bool InitClass();
	static void ShutdownClass();

	audDecoderPS4Mp3();
	virtual ~audDecoderPS4Mp3(){}

	void Update();

	bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 sampleRate);
	void Shutdown();
	bool IsSafeToDelete();

	bool QueryReadyForMoreData() const;
	u32 QueryNumAvailableSamples() const;
	audDecoder::audDecoderState GetState() const {return m_State; }
	u32 ReadData(s16 *buf, const u32 numSamples) const;

	void AdvanceReadPtr(const u32 numSamples);
	void SubmitPacket(audPacket &packet);

	void ClearBuffer();

	void SetWaveIdentifier(const u32 waveIdentifier) { m_WaveIdentifier = waveIdentifier; }

private:

	audStaticRingBuffer<((audMp3Util:: kFrameSizeSamples * 2) * AUD_MP3_BUFFERS)> m_RingBuffer;

	static SceAjmContextId sm_AjmContext;
	SceAjmInstanceId m_AjmInstance;


#if __BANK
	u32 m_TotalSamples;
#endif
	
	atRangeArray<audDecoderMp3Packet, 2> m_Packets;
	u32 m_SubmitPacketIndex;
	u32 m_DecodePacketIndex;
	u32 m_NumChannels;
	s32 m_NumQueuedPackets;

	u32 m_WaveIdentifier;

	bool m_DecodedLastFrame;
	bool m_ReadyForMoreData;

	audDecoder::audDecoderState m_State;

	// debugging stuff
	void OpenStreamDebugFile();
	void CloseStreamDebugFile();
	void OpenBlockDebugFile();
	void CloseBlockDebugFile(bool closeAll = false);

#if ENABLE_MP3_DEBUG || DUMP_RAW_MP3_STREAMS
	s32 m_FileNumber;
	static s32 sm_FilenameCount;
#endif
#if DUMP_RAW_MP3_STREAMS
	fiStream* m_fp;
#if DUMP_RAW_MP3_BLOCKS
	fiStream* m_blockfp[2];
	s32 m_BlockNumber;
#endif
#endif

};

} // namespace rage

#endif // RSG_ORBIS

#endif // AUD_DECODER_PS4_MP3_H
