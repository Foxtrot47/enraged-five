// 
// audiohardware/config.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_CONFIG_H
#define AUD_CONFIG_H

#include "vector/vector3.h"
#include "vectormath/vec3v.h"
#include "vectormath/legacyconvert.h"
#include "audioengine/metadatamanager.h"
#include "audiodata/simpletypes.h"
#include "string/stringhash.h"

namespace rage
{
	class parTree;
	class parTreeNode;

	class audConfig 
	{
	public:

		// PURPOSE
		//	Initialises metadata manager
		// RETURNS
		//	True if successful, false otherwise
		static bool InitClass(const char *configPath);

		// PURPOSE
		//	Cleans up metadata memory
		static void ShutdownClass();
		
		template <class ReturnType> static ReturnType GetValue(const char *valueName, const ReturnType defaultValue)
		{
			ReturnType ret;
			if(GetData(valueName, ret))
			{
				return ret;
			}
			return defaultValue;		
		}

		static bool GetData(const char *name, s32 &val)
		{
			return GetData<ravesimpletypes::Int>(name, val);
		}

		static bool GetData(const char *name, u32 &val)
		{
			return GetData<ravesimpletypes::UnsignedInt>(name, val);
		}

		static bool GetData(const char *name, f32 &val)
		{
			return GetData<ravesimpletypes::Float>(name, val);
		}

		static bool GetData(const char *name, Vec3V_In val)
		{
			Vector3 vector = VEC3V_TO_VECTOR3(val);
			return GetData<ravesimpletypes::Vector3>(name, vector);
		}

		static bool GetData(const char *name, const char *&val)
		{
			return GetData<ravesimpletypes::String>(name, val);
		}

		template <class RAVEType> static RAVEType *GetMetadataObject(const char *objectName)
		{
			return sm_Metadata.GetObject<RAVEType>(objectName);
		}

		template <class RAVEType> static RAVEType *GetMetadataObject(const u32 objectNameHash)
		{
			return sm_Metadata.GetObject<RAVEType>(objectNameHash);
		}

		static audMetadataManager &GetMetadataManager() { return sm_Metadata; }

	private:

		template<class RAVEType, class returnType> static bool GetData(const char *name, returnType &val)
		{
#if __PS3
			const char *platformName = ":ps3";
#elif __XENON
			const char *platformName = ":xbox360";
#else
			const char *platformName = ":pc";
#endif
			const u32 partialHash = atPartialStringHash(name);

			// Start by looking for valueName:platformName
			const u32 platformSpecificHash = atStringHash(platformName, partialHash);
			audMetadataObjectInfo info;
			if(sm_Metadata.GetObjectInfo(platformSpecificHash, info))
			{
				const RAVEType *p = info.GetObject<RAVEType>();
				if(p)
				{
					val = p->Value;
					return true;
				}	
			}

			// Fall back to just valueName
			const RAVEType *p = sm_Metadata.GetObject<RAVEType>(atFinalizeHash(partialHash));
			if(p)
			{
				val = p->Value;
				return true;
			}
			return false;
		}
		
		static audMetadataManager sm_Metadata;
		static bool sm_IsInitialised;
	};
}

#endif
