//
// audiohardware/decoder_xma.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DECODER_XMA_H
#define AUD_DECODER_XMA_H

#if __XENON

#include "system/xtl.h"
#include "XMAHardwareAbstraction.h"
#include "decoder.h"
#include "mixer.h"
#include "ringbuffer.h"

namespace rage
{

	// need to have enough buffer space to deal with pitched up data
	enum{kXmaDecoderBufferBytes = (sizeof(s16) * kMixBufNumSamples * (kMaxSampleRateRatio + 1))};

	struct audXmaPacketInfo
	{
		audXmaPacketInfo() :
			Playback(NULL),
			ContextMemory(NULL),
			LoopStartBits(0),
			LoopSubframeSkip(0),
			LoopEndBits(0),
			LoopSubframeEnd(0)
		{}
		XMAPLAYBACK *Playback;
		void *ContextMemory;
		u32 numSamples;
		u32 PlayBegin;
		u32 PlayEnd;
		u32 NumSamplesConsumed;
		u32 LoopStartBits;
		u32 LoopSubframeSkip;
		u32 LoopEndBits;
		u32 LoopSubframeEnd;
	};

	class audDecoderXma : public audDecoder
	{
	public:

		virtual bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 sampleRate);
		virtual void Shutdown();

		virtual audDecoderState GetState() const
		{
			return m_State;
		}
		virtual void SubmitPacket(audPacket &packet);
		virtual void Update();
		void PreUpdate();

		virtual u32 ReadData(s16 *dest, const u32 numSamples) const;
		virtual u32 QueryNumAvailableSamples() const;
		virtual bool QueryReadyForMoreData() const
		{
			return m_ReadyForMoreData;
		}
		virtual void AdvanceReadPtr(const u32 numSamples);

		virtual void SetWaveIdentifier(const u32 waveIdentifier) { m_WaveIdentifier = waveIdentifier; }

		u32 ComputeBytesToSkip(const u8 *ptr, const u32 len, const u32 samplesToSkip, u32 &skippedSamples);
		void FlushPackets();
		void ClearBuffer();

		inline void DebugAbortPlayback() { m_AbortPlayback = true; }

	private:

		u32 ComputeNumSamplesInBuffer(const u8 *ptr, const u32 len);
		void ComputeLoopData(const audPacket &packet, audXmaPacketInfo &xmaPacketInfo);

		audStaticRingBuffer<kXmaDecoderBufferBytes> m_RingBuffer;
		
		atRangeArray<audXmaPacketInfo, 2> m_Packets;
		u32 m_DecodingPacketIndex;
		u32 m_SubmittedPacketIndex;

		audDecoderState m_State;
		u32 m_NumChannels;
		u32 m_SampleRate;
		u32 m_SamplesDecoded;
		u32 m_SamplesConsumed;
		u32 m_BytesSubmitted;
		u32 m_NumQueuedPackets;
		u32 m_WaveIdentifier;
		bool m_ReadyForMoreData;
		bool m_HaveRequestedLock;
		bool m_AbortPlayback;
	};

} // namespace rage

#endif // __XENON
#endif // AUD_DECODER_PCM_H
