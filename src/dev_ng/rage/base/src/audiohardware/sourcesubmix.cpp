#include "framebuffercache.h"
#include "framebufferpool.h"
#include "sourcesubmix.h"

#include "mixing_vmath.inl"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	audSourceSubmix::audSourceSubmix() : audPcmSource(AUD_PCMSOURCE_SUBMIX)
	{
		for(u32 i = 0; i < kMaxSourceSubmixInputs; i++)
		{
			m_InputBuffers[i] = -1;
			m_InputGains[i] = 0.f;
		}
	}

	void audSourceSubmix::StartPhys(s32 UNUSED_PARAM(channel))
	{
		if(++m_NumPhysicalClients == 1)
		{
			AllocateBuffers(1);
		}
	}

	void audSourceSubmix::StopPhys(s32 UNUSED_PARAM(channel))
	{
		if(--m_NumPhysicalClients == 0)
		{
			FreeBuffers();
		}
	}

	void audSourceSubmix::BeginFrame()
	{
		m_InputBuffers.Reset();
		m_InputGains.Reset();
	}

	void audSourceSubmix::AddInputBuffer(const s32 bufferId, const f32 gain)
	{
		if(HasBuffer(0))
		{
			Assign(m_InputBuffers.Append(), bufferId);
			m_InputGains.Append() = gain;
		}
	}

	void audSourceSubmix::GenerateFrame()
	{
		Assert(HasBuffer(0));
		if (HasBuffer(0))
		{
			g_FrameBufferCache->ZeroBuffer(GetBufferId(0));
			f32 *output = g_FrameBufferCache->GetBuffer(GetBufferId(0));

			for(s32 i = 0; i < m_InputBuffers.GetCount(); i++)
			{
				const Vector_4V gainsV = V4LoadScalar32IntoSplatted(m_InputGains[i]);	

				const f32 *inputData = g_FrameBufferCache->GetBuffer_ReadOnly(m_InputBuffers[i]);
				MixMonoBufToMonoBuf<kMixBufNumSamples>(inputData, output, gainsV, gainsV);
			}
		}
	}
}