//
// audiohardware/waveslot.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_WAVE_SLOT_H
#define AUD_WAVE_SLOT_H

#include "atl/array.h"
#include "atl/atfixedstring.h"
#include "atl/map.h"
#include "audiodata/container.h"
#include "paging/streamer.h"
#include "system/criticalsection.h"
#include "system/task.h"
#include "system/service.h"
#include "debug.h"
#include "wavedefs.h"

#define		FAST_WAVESLOT	1

#define SPU_WAVESLOT_MAP 0 

#define AUD_BANK_DATA_IN_VRAM (__PS3 && 1)
#define AUD_STREAMINGBUFFERS_IN_VRAM (__PS3 && 1)
#define AUD_WAVE_DATA_IN_VRAM (__PS3 && 1)

// Only allow unencrypted assets on PC for now (TODO: change when PC assets have been rebuilt)
#define AUD_SUPPORT_UNENCRYPTED_ASSETS (RSG_PC)

#if AUD_SUPPORT_UNENCRYPTED_ASSETS
#define AUD_SUPPORT_UNENCRYPTED_ASSETS_ONLY(x) x
#else
#define AUD_SUPPORT_UNENCRYPTED_ASSETS_ONLY(x)
#endif

namespace rage {

struct audVoiceSettings;
class bkBank;

// PURPOSE
//	Defines the maximum number of loads that can be requested in a single batch
static const u32 g_MaxWaveLoadsPerBatch = 8;

static const u16 AUD_INVALID_BANK_ID = 0xffff;

struct audBatchWaveLoadRequest
{
	u16 slotIndex;
	u16 bankId;
	u32 waveNameHash;
};
#if !__SPU
class bkText;
#endif
//
// PURPOSE
//  Encapsulates a cross-platform audio memory slot that can load a Wave or Wave Bank.
class audWaveSlot
{
public:
	typedef u32* (*QueryEncryptionKeyCallback)(u32 bankId);
	static QueryEncryptionKeyCallback GetEncryptionKeyOverrideCallback;

	static const int AUD_WAVE_SLOT_SIZE;

	//
	// PURPOSE
	//	Define Bank/Wave load status values.
	//
	enum audWaveSlotLoadStatus
	{
		LOADED,
		LOADING,
		FAILED,
		NOT_REQUESTED
	};

	enum audWaveSlotPriorityLevels
	{
		kNumWaveLoadPrioLevels = 4
	};

	// PURPOSE
	//	Increments the reference count for the specified slot.
	// NOTES
	//	This interface enables code running on the SPU or without a valid waveslot pointer to affect the reference count;
	//	regular code should use audWaveSlot::AddReference() directly
	static u32 AddSlotReference(const u32 slotIndex);

	// PURPOSE
	//	Increments the reference count for the specified slot.
	// NOTES
	//	This interface enables code running on the SPU or without a valid waveslot pointer to affect the reference count;
	//	regular code should use audWaveSlot::RemoveReference() directly
	static u32 RemoveSlotReference(const u32 slotIndex);

	static u32 GetSlotReferenceCount(const u32 slotIndex);

	u32 GetSlotIndex() const { return m_SlotIndex; }

#if !__SPU

	static u32 GetNumPendingRequests();
	static u32 GetNumPendingRequests(const u32 priorityLevel);

#if __BANK
	static void AddWidgetsStatic(bkBank& bank);
	static void TestLoadBank(audWaveSlot* pWaveSlot);
#endif

	//
	// PURPOSE
	//	Allocates and sets up all wave slots
	static void InitClass(void *externallyAllocatedBankHeap, u32 externallyAllocatedBankHeapSize);

	// PURPOSE
	//	Loads any static banks into their wave slots
	static void LoadStaticBanks();

	//
	// PURPOSE
	//	Frees all resources associated with the wave slots.
	//
	static void ShutdownClass(void);
	//
	// PURPOSE
	//	Should be called regularly, calls the Update function of all wave slots.
	// SEE ALSO
	//	audWaveSlot::Update
	//
	static void UpdateSlots(void);

	// PURPOSE
	//	Returns the bank name in PACK/BANK form for the specified bank id
	static const char *GetBankName(const u32 bankId);

	const adatContainer &GetContainer() const { return GetInternalState()->Container; }

	// PURPOSE
	//	Returns a pointer to the slot data
	const u8 *GetSlotData() const
	{
		return GetInternalState()->BankData;
	}
	u8 *GetSlotData()
	{
		return GetInternalState()->BankData;
	}
	// PURPOSE
	//	Searches for the specified bankName in the bank list, and returns the id that can be used in future
	//	Load etc requests
	// RETURNS
	//	bankId, ~0U on failure
	static u32 FindBankId(const char *bankName);
	static u32 FindBankId(const u32 bankNameHash);

	static void TeaStirEncryptionKey(u32* encryptionKey);
	
	//
	// PURPOSE
	//	Adds to the reference count of clients using this slot.
	//
	void AddReference(/*audVoiceSettings* voiceSettings*/);
	//
	// PURPOSE
	//	Reduces the reference count of clients using this slot.
	//
	void RemoveReference(/*audVoiceSettings* voiceSettings*/);
	//
	// PURPOSE
	//	Returns true if the slot has an active load pending.
	//
	bool GetIsLoading(void)
	{
		return m_IsLoading;
	}
	//
	// PURPOSE
	//	Returns the last time a load was requested, in milliseconds, or (u32)-1 if no load has been requested for this slot.
	//
	u32 GetLoadRequestTimeMs(void) const
	{
		return m_LoadRequestTimeMs;
	}

	// PURPOSE
	//	Ditch any pending load requests
	static void Drain();

	// PURPOSE
	//	Request a series of loads to be actioned as a batch, locking the streamer for the duration of the load
	// NOTE
	//	This should only be used for waves that are right next to each other on the disk, and preferably in the same bank
	static bool RequestBatchWaveLoad(audBatchWaveLoadRequest *waveRequests, const u32 numWaveLoads);

	// PURPOSE
	//	Returns true if a batch wave load is active
	static bool IsBatchWaveLoadRequested()
	{
		sysCriticalSection lock(sm_BatchWaveLoadCriticalSectionToken);
		return sm_IsBatchWaveLoadRequested;
	}

	// PURPOSE
	//	Returns true if the last batch wave load failed
	static bool HasBatchWaveLoadFailed()
	{
		sysCriticalSection lock(sm_BatchWaveLoadCriticalSectionToken);
		return sm_HasBatchWaveLoadFailed;
	}

	static void UpdateBatchWaveLoadRequest();


#else
	static audWaveSlot *GetWaveSlotData()
	{
		return sm_WaveSlots;
	}

	static void SetWaveSlotData(audWaveSlot *buf)
	{
		sm_WaveSlots = buf;
	}

	adatContainerSpu &GetContainer() const
	{
		Assert(sm_CachedContainerSlotId == m_SlotIndex);
		return sm_CachedContainer;
	}

	bool FetchContainerHeader() const;

#endif // SPU

	//
	// PURPOSE
	//	Attempts to find a wave slot by name using an internal hash table.
	// PARAMS
	//	slotName - The name of the slot to be located.
	// RETURNS
	//	A pointer to the wave slot referenced by name, or NULL if no such slot can be found.
	//
	static audWaveSlot *FindWaveSlot(const char *slotName);
	static audWaveSlot *FindWaveSlot(const u32 slotNameHash);
	//
	// PURPOSE
	//	Attempts to find a wave slot that contains the specified static bank using an internal hash table.
	// PARAMS
	//	bankId - The id of the static bank to be located.
	// RETURNS
	//	A pointer to the wave slot containing the statically loaded bank, or NULL if no such slot can be found.
	//
	static audWaveSlot *FindLoadedBankWaveSlot(const u32 bankId); 

	// PURPOSE
	//  Verify that the given bank is loaded into the given waveslot
	ASSERT_ONLY(static bool VerifyLoadedBankWaveSlot(const audWaveSlot* waveSlot, const u32 bankId);)

	//
	// PURPOSE
	//	Attempts to find a wave slot that contains the specified wave asset using an internal hash table.
	// PARAMS
	//	bankId - The id of the wave to be located.
	// RETURNS
	//	A pointer to the wave slot containing the loaded wave, or NULL if no such slot can be found.
	//
	static audWaveSlot *FindLoadedWaveAssetWaveSlot(const u32 waveNameHash, const u32 bankId); 

	// PURPOSE
	//	Returns a wave slot index for the specified wave slot
	static s32 GetWaveSlotIndex(const audWaveSlot *const slot)
	{
		return static_cast<s32>((slot?(s32)(((size_t)slot - (size_t)sm_WaveSlots) / AUD_WAVE_SLOT_SIZE):-1));
	}

	// PURPOSE
	//	Returns a pointer to a wave slot from the supplied index
	static audWaveSlot *GetWaveSlotFromIndex(const s32 index)
	{
		return (index!=-1?(audWaveSlot*)((char*)sm_WaveSlots + (index * AUD_WAVE_SLOT_SIZE)):NULL);
	}

	// use this function only in cases where index is guaranteed to not be -1 ... like for(int i = 0; i < maxsomething) ..loops
	static audWaveSlot *GetWaveSlotFromIndexFast(const s32 index)
	{
		FastAssert(index != -1);
		return (audWaveSlot*)((char*)sm_WaveSlots + (index * AUD_WAVE_SLOT_SIZE));
	}

	static u32 GetNumWaveSlots()
	{
		return sm_NumWaveSlots;
	}

	// PURPOSE
	//  Returns the total memory allocated (not quite - it's total bank size, but isn't 2048 aligned) for wave banks
	static u32 GetTotalSlotSizeMain()
	{
		return sm_TotalMainAllocation;
	}

	static u32 GetTotalSlotSizeVRAM()
	{
		return sm_TotalVRAMAllocation;
	}

	static u32 GetTotalSlotSize()
	{
		return GetTotalSlotSizeMain() + GetTotalSlotSizeVRAM();
	}

	static void LoadStaticBankInfo();

	//
	// PURPOSE
	//  Frees all resources used by the wave slot.
	//
	virtual ~audWaveSlot(void);

	// PURPOSE
	//  Queues a request for a Wave (from a specified bank) or whole Bank to be loaded. Which depends on the slot's
	//  loadType. Calls LoadBank() or LoadWave() as appropriate.
	// PARAMS
	//  bankId	- The id of the Wave Bank to be loaded, or wave loaded from.
	//  waveName/Hash - The name of the Wave to be loaded in the case of a wave slot, ignored in the case of a bank slot.
	bool LoadAsset(const u32 bankId, const u32 waveNameHash, const u32 priority);
	//
	// PURPOSE
	//	Queues a request for a Wave Bank to be loaded. This will override an existing load request that has not yet
	//	been actioned.
	// PARAMS
	//	bankId	- The id of the Wave Bank to be loaded
	// RETURNS
	//	Whether a load request was queued successfully
	bool LoadBank(const u32 bankId, const u32 priority = 0);
	//
	// PURPOSE
	//	Queues a request for a Wave to be loaded. This will override an existing load request that has not yet been
	//	actioned.
	// PARAMS
	//	bankId	- The id of the Wave Bank that contains the Wave to be loaded
	//	waveName	- The hash of the name of the Wave to be loaded. This should be generated using the RAGE atStringHash
	//					function.
	// SEE ALSO
	//	atStringHash
	//
	void LoadWave(const u32 bankId, const u32 waveNameHash, const u32 priority = 0);
	//
	// PURPOSE
	//  Force unload a bank. Generally banks are only unloaded when a new bank is loaded into a slot, but we may wish to
	//  forcefully unload a bank if we're wanting to reuse slot memory for virtualization-style behavior;
	virtual bool ForceUnloadBank();
	// 
	// PURPOSE
	//  Override the bank memory pointer and size - used in virtualization behavior
	bool ForceSetBankMemory(u8* bankMemory, u32 size);

#if RSG_BANK 
	static void DebugDraw(audDebugDrawManager &drawMgr);
	static void DebugDrawSlotSizeErrors();
#endif

	//
	// PURPOSE
	//	Gets the loading status of a specified Wave (from a specified Bank), or Bank
	// PARAMS
	//	bankId	- The id of the Wave Bank that contains the Wave for which the loading status is required
	//	waveName/Hash	- The name of the Wave for which the loading status is required, ignored for Wave Slots.
	// RETURNS
	//	The loading status of the Wave/Bank.
	//
	audWaveSlotLoadStatus GetAssetLoadingStatus(const u32 bankId, const char *waveName) const;
	audWaveSlotLoadStatus GetAssetLoadingStatus(const u32 bankId, const u32 waveNameHash) const;
	//
	// PURPOSE
	//	Gets the loading status of a specified Bank.
	// PARAMS
	//	bankId	- The id of the Bank for which the loading status is required
	// RETURNS
	//	The loading status of the Bank.
	//
	audWaveSlotLoadStatus GetBankLoadingStatus(const u32 bankId) const;
	//
	// PURPOSE
	//	Gets the loading status of a specified Wave (from a specified Bank).
	// PARAMS
	//	bankid	- The id of the Wave Bank that contains the Wave for which the loading status is required
	//	waveName	- The name of the Wave for which the loading status is required.
	// RETURNS
	//	The loading status of the Wave.
	//
	audWaveSlotLoadStatus GetWaveLoadingStatus(const u32 bankId, const char *waveName) const;
	//
	// PURPOSE
	//	Gets the loading status of a specified Wave (from a specified Bank).
	// PARAMS
	//	bankId		- The id of the Wave Bank that contains the Wave for which the loading status is required
	//	waveNameHash	- The hash of the name of the Wave for which the loading status is required. This should be
	//						generated using the RAGE atStringHash function.
	// RETURNS
	//	The loading status of the Wave.
	// SEE ALSO
	//	atStringHash
	//
	audWaveSlotLoadStatus GetWaveLoadingStatus(const u32 bankId, const u32 waveNameHash) const;

	//
	// PURPOSE
	//	Determines whether this Wave Slot contains a streaming Wave Bank.
	// RETURNS
	//	Returns true if this Wave Slot contains a streaming Wave Bank.
	//
	bool IsStreaming(void) const
	{
		return m_IsStreaming;
	}

	// PURPOSE
	//	Returns the current reference count
	u32 GetReferenceCount() const;

	void ClearHasChanged()
	{
		m_HasChanged = false;
	}

	void SetHasChanged()
	{
		m_HasChanged = true;
	}

	bool HasChanged() const
	{
		return m_HasChanged;
	}

#if !__SPU
	//
	// PURPOSE
	//	Returns the name of this slot.
	// RETURNS
	//	The name of this slot.
	//
	const char *GetSlotName() const
	{
		return GetInternalState()->SlotName;
	}

	// PURPOSE
	//	Returns the size of the slot in bytes
	u32 GetSlotSize() const
	{
		return GetInternalState()->SlotBytes;
	}

	// PURPOSE
	//	Returns the name of the currently loaded wave bank in PACK/BANK form
#if __BANK
	const char *GetCachedLoadedBankName() const { return &m_CachedLoadedBankName[0]; }
	const char *GetCachedLoadingBankName() const { return &m_CachedLoadingBankName[0]; }	
#endif
#endif // __SPU

	const char *GetLoadingBankName() const;
	const char *GetLoadedBankName() const;

	// PURPOSE
	//	Resolves the specified container offset into a pointer to the data, validating that the requested data is
	//	currently in memory.
	const void *Resolve(const u32 offset, const u32 size) const;

	// PURPOSE
	//	Returns the hash of the currently loaded wave
	u32 GetLoadedWaveNameHash() const { return m_LoadedWaveNameHash; }

	u32 GetLoadedBankId() const { return m_LoadedBankId; }

#if !__SPU
	void _SyncWaveSlotRequest(u16 bankId, u32 waveNameHash, u32 priority);
#else
	void _GetSyncState(u16 &bankId, u32 &waveNameHash, u8 &priority)
	{
		bankId = m_BankIdToLoad;
		waveNameHash = m_WaveNameHashToLoad;
		priority = m_RequestedLoadPriority;
	}
#endif

	// PURPOSE
	//	Specifies the root path to use for packs starting with the specified 8-character prefix, ie
	// DLC_ASSA -> spAssassinatinoPack:/ps3/audio/sfx/ will result in queries for DLC_ASSASSINATION/TEST_BANK returning 
	// dlc1:/ps3/audio/sfx/dlc_assassination/test_bank.awc
	static void SetSearchPath(const char *packPrefix, const char *rootPath);

	// PURPOSE
	//	Remove the search path associated with the 8-character pack prefix
	static void RemoveSearchPath(const char *packPrefix);

	bool IsStatic() { return m_IsStatic; }

	u32 GetRequestedLoadPriority() const { return m_RequestedLoadPriority; }

	bool AllowCompressedAssetLoad() const;

protected:

	//Declares that this slot will never be unloaded
	void MarkSlotAsStatic() { m_IsStatic = true; }

	public:
	//Registers the slot as having it's waves loaded into physical memory
	void RegisterLoadedBank();
	void RegisterLoadedWave();
protected:

	//Removes slot from the loaded bank table
	void RemoveFromLoadedBankTable();

	// Removes slot from the loaded wave table
	void RemoveFromLoadedWaveTable();

	//declare slot as being permanently loaded into memory
	
	template <size_t _Size> inline 
		static void ComputeBankFilePath(char (&dest)[_Size], const char* bankName)
	{
		return ComputeBankFilePath(dest, _Size, bankName);
	}

	static void ComputeBankFilePath(char *dest, const u32 destSize, const char *bankName);

	enum LoadType
	{
		kLoadTypeWave,
		kLoadTypeBank,
		kLoadTypeStream,
		kNumLoadTypes
	};

	// PURPOSE
	//	Defines the slot type - note that slot type wave handles wave and bank loads
	enum SlotType
	{
		kSlotTypeWave,
		kSlotTypeStream,
		kNumSlotTypes
	};

	//
	// PURPOSE
	//  Initializes member variables and allocates the specified amount of system memory for Wave / Bank storage.
	// PARAMS
	//	bankMem			- Pointer to memory allocated for this slot
	//	numBytes		- The amount of system memory allocated for Wave / Bank storage, in bytes.
	//	maxHeaderSize	- The maximum header size for any bank that is to be loaded into this slot, in bytes
	//	slotName		- String name for this slot
	// NOTES
	//	Protected since all wave slot creation should go through the static audWaveSlot in order
	//	to ensure consistency between the slotName and the global slot lookup table
	//
	audWaveSlot(u8 *bankMem, const u32 numBytes, const u32 maxHeaderSize, const char *slotName, const bool isVirtual);

	static const u32 *GetKey(u16 bankId);

	static bool AllowCompressedAssetLoad(LoadType loadType, const char* slotName);

private:

#if __BANK
	static u32 GetExpectedWaveslotSize(const char* slotName);
#endif

#if !__SPU
	
	void BaseUpdate(void);
	//
	// PURPOSE
	//	Prepares and requests that a Wave Bank header or full Bank be loaded via pgStreamer.
	//
	void RequestLoad(void);
	//
	// PURPOSE
	//	Actions any follow on load requests and fixes-up the data that has been loaded.
	//
	void HandleLoadCompletion(void);
	//
	// PURPOSE
	//	Loaded data is fixed-up and where the Wave Bank header has just been loaded, a load is requested for the required
	//	Wave sample data.
	void HandleWaveLoadCompletion();
	//
	// PURPOSE
	//	Resets all loading-related member variables to safe defaults following a load failure.
	//
	void CleanUpAfterLoadFailure(void);

#endif // !__SPU

#if __DEV
//	void DisplayAllTopParentSoundsReferenced() const;
//	static void smDisplayAllTopParentSoundsReferenced(audWaveSlot* pSlot) { pSlot->DisplayAllTopParentSoundsReferenced(); }
#endif

protected:

	static bool IsVramSlot(const char *slotName);

	static bool ComputeContiguousSampleDataChunk(fiStream* stream, const u32 maxHeaderSize, u32 &sampleDataOffset, u32 &sampleDataSize, u32 &numObjects);
	static bool ComputeStaticSize(const char *bankName, u32 &headerSize, u32 &dataSize);
	u32 ComputeContiguousMetadataSize() const;

#if !__SPU
#if __BANK
	void CacheLoadedBankName();	
	void CacheLoadingBankName();
#endif

	static void UpdateWaveSlot(audWaveSlot *slot);

	static u32 GetSizeOfBank(const char *bankName);
#else
	const char *GetLoadingBankName() const
	{
		if(m_BankIdToLoad < AUD_INVALID_BANK_ID)
		{
			return "(unknown bank)";
		}
		return "(invalid bank)";
	}
#endif

protected:

	void EnableBatchMode()
	{
		m_BatchMode = true;
	}

	void DisableBatchMode()
	{
		m_BatchMode = false;
	}

	bool IsWaitingOnDecryption() const
	{
		return (m_DecryptionTaskHandle != NULL);
	}

	struct audWaveSlotState
	{
		char *SlotName;
		u8 *BankData;
		u8 *BankDataVram;
		u32 MainDataSize;
		u32 VramDataSize;
		u32 VramAllocationSize;

		u32 SlotBytes;
		//Generated by a build step, this records the maximum header size for any bank that is to be loaded into this slot.
		u32 MaxHeaderSize;
		u32 HeaderPadding;

		pgStreamer::Handle BankHandle;
		sysIpcSema BankLoadSema;

		adatContainer Container;
		u32 ContainerHeaderSize;

		u32 LoadedContainerOffset;
		u32 LoadedBytes;

		u32 VRAMLoadOffset;

		bool IsVirtual;
	};

	struct audStaticBankInfo
	{
		u32 bankNameHash;
		u32 fileSize;
		u32 sampleDataOffset;
		u32 sampleDataSize;
	};

	audWaveSlotState *m_InternalState;

#if !__SPU
	void BlockUntilLoaded()
	{
		g_SysService.UpdateClass();
		while(!sysIpcPollSema(GetInternalState()->BankLoadSema))
		{
			sysIpcSleep(1);
			g_SysService.UpdateClass();
		}
	}

	audWaveSlotState *GetInternalState() { return m_InternalState; }
	const audWaveSlotState *GetInternalState() const { return m_InternalState; }
#else

	void FetchInternalState() const;

	const audWaveSlotState *GetInternalState() const 
	{ 
		TrapNE((u8)sm_InternalStateCacheSlotId, m_SlotIndex);
		return &sm_InternalStateCache;
	}

#endif

	u32 m_WaveNameHashToLoad;
	u32 m_LoadedWaveNameHash;
	u32 m_LoadFailedWaveNameHash;

	u32 m_LoadRequestTimeMs;

	sysTaskHandle m_DecryptionTaskHandle;
	
	u16 m_BankIdToLoad;
	u16 m_LoadedBankId;

	u16 m_LoadFailedBankId;
	u8	m_SlotIndex;
	s8	m_LoadType;
	s8	m_SlotType;

	u8 m_RequestedLoadPriority;
	u8 m_LoadPriority;

	bool m_IsLoadRequestQueued : 1;
	bool m_IsLoading : 1;
	bool m_IsStreaming : 1;
	// if true this wave slot needs sync'd
	bool m_HasChanged : 1;

	// true if this slot is loading in batch mode - controls streaming locking behaviour
	bool m_BatchMode : 1;

	//true if the slot is permanently statically loaded
	bool m_IsStatic : 1;

	// true when we've requested the decryption job on the loaded data
	bool m_HasDecrypted : 1;

	//
	// PURPOSE
	//	A hash map connecting wave slot names to wave slot pointers.
	//
	static atMap<u32, audWaveSlot*>	sm_WaveSlotTable;
	//
	// PURPOSE
	//	A hash map containing all statically loaded banks.
	//
	public:
	static atMap<u16, audWaveSlot*>	sm_LoadedBankTable;
	static atMap<u32, audWaveSlot*> sm_LoadedWaveTable;
protected:
	static atMap<u32, const char *> sm_SearchPaths;

	//
	// PURPOSE
	// Pointer to the memory allocated for wave slot loading
	//
	static u8								*sm_WaveSlotData;
	static bool								sm_OwnsWaveSlotData;

	// PURPOSE
	//  Total size of memory reserved for wave slots (actually, not quite - actual size is 2048 aligned, this isn't)
	static u32								sm_TotalMainAllocation;
	static u32								sm_TotalVRAMAllocation;

public:
	// PURPOSE
	//	Number of wave slots allocated
	static u32								sm_NumWaveSlots;
protected:
	// PURPOSE
	//	waveslot instances
	static audWaveSlot						*sm_WaveSlots;

	static sysCriticalSectionToken sm_BankTableCriticalSectionToken;

	static sysCriticalSectionToken sm_BankLoadCriticalSectionToken;
	static atFixedArray<audStaticBankInfo, 25> sm_StaticBankInfoList;
	static bool sm_UsePreComputedStaticBankInfo;
	static bool sm_StaticBankInfoFileExists;

	// PURPOSE
	//	Stores the time that the batch load was requested
	// NOTE
	//	There can only be one batch load active
	static u32								sm_BatchWaveLoadRequestTime;
	static u32								sm_NumWaveLoadRequestsInBatch;
	static u32								sm_CurrentBatchLoadRequestStage;
	static bool								sm_IsBatchWaveLoadRequested;
	static bool								sm_HasBatchWaveLoadFailed;
	static atRangeArray<audBatchWaveLoadRequest,g_MaxWaveLoadsPerBatch>	sm_BatchWaveLoadRequest;
	static sysCriticalSectionToken			sm_BatchWaveLoadCriticalSectionToken;
	static sysCriticalSectionToken			sm_UpdateCriticalSection;

	static atRangeArray<u32, kNumWaveLoadPrioLevels>	sm_RequestQueuePrioCounts;
	static u32								sm_RequestQueueCount;

	static int								sm_iSchedulerIndex;

#if __BANK
	char						m_CachedLoadedBankName[128];
	char						m_CachedLoadingBankName[128];
#endif

#if RSG_BANK
	u32							m_ExpectedSlotSize;
	u32							m_RequestDelayTimer;
	bool						m_RequestDelayTimerSet;
	static u32					sm_CurrentlyLoadingSlotIndex;
	static bool					sm_DelayRequests;	
	static u32					sm_RequestDelayTime;
	static u32					sm_StreamingRequestDelayTime;
	static bool					sm_DrawRequestQueue;
	static bool					sm_DrawWaveSlotSizeErrors;
	static atFixedArray<u8,256>	sm_RequestQueueToDraw;
	static sysCriticalSectionToken sm_DrawRequestQueueLock;
#endif
	
#if !__SPU
	static sysCriticalSectionToken			sm_WaveSlotNamesCriticalSectionToken;
	BANK_ONLY (static atArray<bkText *> 	sm_LoadedBankNames);
	BANK_ONLY (static atArray<bkText *> 	sm_LoadingBankNames);

	static atArray<audWaveSlot *>			sm_WaveLoadingSlots;
#else
	static s32								sm_InternalStateCacheSlotId;
	static ALIGNAS(16) audWaveSlotState sm_InternalStateCache ;
	static adatContainerSpu					sm_CachedContainer;
	static s32								sm_CachedContainerSlotId;
#endif // __SPU
public:
	static u32								*sm_ReferenceCounts;
};

void bteatask(rage::sysTaskParameters &);

}	// namespace rage

#endif // AUD_WAVE_SLOT_H

