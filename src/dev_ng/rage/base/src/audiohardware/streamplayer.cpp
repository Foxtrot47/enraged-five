//
// audiohardware/streamplayer.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#if !__SPU

#include "decoder.h"
#include "decodemgr.h"
#include "device.h"
#include "driver.h"
#include "ringbuffer.h"
#include "syncsource.h"
#include "voicemgr.h"

#include "file/stream.h"
#include "file/asset.h"
#include "profile/profiler.h"

#else
#include "framebuffercache.h"
#include "audioengine/spuutil.h"
#include "system/dma.h"
#endif

#if __XENON
#include "decoder_xma.h"
#include <xma2defs.h>
#endif


#include "decoder_spu.h"
#include "mixer.h"
#include "src_linear.h"
#include "streamplayer.h"

#include "mixing_vmath.inl"
#include "audiosynth/synthutil.h"
#include "debug.h"

#include "file/remote.h" // for fiIsShowingMessageBox
#include "math/amath.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"

namespace rage
{
	BANK_ONLY(extern bool g_DisableSampleRateConversion);

#if __SPU
	extern f32 *g_PCMBuffers;

	extern u8 *g_ScratchDecoder;
	// maximum number of samples required is ((mixBufNumSamples * maxSampleRateRatio)+2), we need an extra 8 for misaligned loads and this must be aligned up to 16 bytes (8 shorts)
	extern s16 *g_ScratchLoadBuffer;
	extern size_t g_ScratchLoadBufferSize;
#else

	// On PPU this is only used as an SRC output buffer when we have a predelay
	extern ALIGNAS(16) f32 g_ScratchLoadBuffer[kMixBufNumSamples] ;
	extern const size_t g_ScratchLoadBufferSize;
#endif

#if !__SPU
	audStreamPlayer::audStreamPlayer() : audPcmSource(AUD_PCMSOURCE_EXTERNALSTREAM)
	{
		m_NumSamplesConsumed = 0;
		m_FinishedPlayback = false;
		m_SrcTrailingFrac = 0.f;
		m_RingBuffer = NULL;
		m_NumChannels = 0;
		m_SampleRate = 0;
		m_PlayState = kStateIdle;
		m_StopRequested = false;

		m_PredelayFrames = 0;
		m_PredelaySamples = 0;

		m_RingBufferReadOffset = 0;
		m_SyncMasterId = m_SyncSlaveId = audMixerSyncManager::InvalidId;

		m_RingBufferState = NULL;
		m_StopPlayback = false;
		m_HasStarted = false;
		m_Gain = 1.f;

		m_IsPaused = false;
		m_IsPauseRequested = false;
	}

	void audStreamPlayer::Shutdown()
	{
		FreeBuffers();

		if(m_SyncMasterId != audMixerSyncManager::InvalidId)
		{
			audMixerSyncManager::Get()->Cancel(m_SyncMasterId);
			m_SyncMasterId = audMixerSyncManager::InvalidId;
		}

		if(m_RingBuffer)
		{
			m_RingBuffer->Release();
			m_RingBuffer = NULL;
		}
	}

	void audStreamPlayer::Init()
	{	
		m_HasBegunFrame = false;
		m_HasEndedFrame = false;				

		PcmSourceState *pcmSourceState = audDriver::GetVoiceManager().GetPcmSourceState(audDriver::GetMixer()->GetPcmSourceSlotId(this));

		pcmSourceState->PlaytimeSamples = 0;
		pcmSourceState->AuthoredSampleRate = m_SampleRate;
		pcmSourceState->NumChannels = m_NumChannels;
	}

	void audStreamPlayer::Start()
	{
		if(m_PlayState != kStateIdle)
		{
#if __DEV
			static bool haveTraced = false;
			if(!haveTraced)
			{
				audDriver::GetMixer()->DebugTraceCurrentCommandBuffer();
				haveTraced = true;
			}
#endif
			audAssertf(m_PlayState == kStateIdle, "audStreamPlayer::Start when not idle (play state %d), id: %d", m_PlayState, audDriver::GetMixer()->GetPcmSourceSlotId(this));
		}


		m_FinishedPlayback = false;
		m_PlayState = kStatePlaying;
	}

	void audStreamPlayer::StartPhys(s32 UNUSED_PARAM(channel))
	{
		if(m_FinishedPlayback || m_PlayState == kStateStopped)
		{
			// we've already finished - don't bother continuing
			m_FinishedPlayback = true;
			return;
		}

		if(++m_NumPhysicalClients > 1)
		{
			audAssert(HasBuffer(0));
			// If this isn't the first physical client then we're good to go
			return;
		}

		AllocateBuffers(m_NumChannels);
	}

	void audStreamPlayer::Stop()
	{
		m_StopRequested = true;
	}

	void audStreamPlayer::StopPhys(s32 UNUSED_PARAM(channel))
	{
		if(--m_NumPhysicalClients == 0)
		{
			FreeBuffers();
		}
	}

	void audStreamPlayer::SetParam(const u32 paramId, const u32 val)
	{
		switch(paramId)
		{
		case audPcmSource::Params::SyncMasterId:
			if(m_SyncMasterId != audMixerSyncManager::InvalidId)
			{
				audMixerSyncManager::Get()->Cancel(m_SyncMasterId);
			}
			Assign(m_SyncMasterId, val);
			if(m_SyncMasterId != audMixerSyncManager::InvalidId)
			{
				audMixerSyncManager::Get()->AddTriggerRef(val);
			}
			break;
		case audPcmSource::Params::SyncSlaveId:
			Assign(m_SyncSlaveId, val);
			break;
		case audStreamPlayer::Params::StopPlayback:
			m_StopPlayback = true;
			break;
		default:
			break;
		}
	}

	void audStreamPlayer::SetParam(const u32 paramId, const f32 val)
	{
		switch(paramId)
		{			
		case audStreamPlayer::Params::Gain:
			m_Gain = audDriverUtil::ComputeLinearVolumeFromDb(val);
			break;
		case audPcmSource::Params::FrequencyScalingFactor:
			m_IsPauseRequested = (val == 0.f);
		default:
			break;
		}
	}

	void audStreamPlayer::HandleCustomCommandPacket(const audPcmSourceCustomCommandPacket* packet)
	{
		if(packet)
		{
			switch(packet->paramId)
			{
			case Params::InitParams:
				const audInitStreamPlayerCommandPacket *data = reinterpret_cast<const audInitStreamPlayerCommandPacket*>(packet);

				// ExternalStreamSound has already added a reference on the ringbuffer
				m_RingBuffer = data->ringBuffer;
				m_NumChannels = data->numChannels;
				m_SampleRate = data->sampleRate;

				// Cache the important ringbuffer info to save DMAs on SPU
				m_RingBufferSize = m_RingBuffer->GetBufferSize();
				m_RingBufferState = m_RingBuffer->GetStatePtr();
				m_RingBufferData = m_RingBuffer->GetBufferPtr();

				// Ensure we start with the correc tread offset; may not always be 0.
				m_RingBufferReadOffset = m_RingBufferState->ReadOffset;
				break;
			}
		}
		Init();
	}

	void audStreamPlayer::BeginFrame()
	{
		AUD_PCMSOURCE_TIMER_BEGINFRAME;
		Assert(HasBuffer(0));

		if(m_HasBegunFrame)
		{
			return;
		}
		m_HasBegunFrame = true;
		m_HasEndedFrame = false;
	}

	void audStreamPlayer::EndFrame()
	{
		AUD_PCMSOURCE_TIMER_ENDFRAME;
		if(m_HasEndedFrame)
		{
			return;
		}

		m_IsPaused = m_IsPauseRequested;

		m_HasEndedFrame = true;
		m_HasBegunFrame = false;

		if(m_IsStarved)
		{
			// do something?
			m_IsStarved = false;
		}

		if(m_StopRequested && m_PlayState == kStatePlaying)
		{
			m_PlayState = kStateStopped;
		}

		if(m_PlayState == kStateStopped)
		{
			m_FinishedPlayback = true;
		}
	}

#else // __SPU

	void audStreamPlayer::FetchPCMSamples(s16 *dest, const s16 *src, const s32 numSamples, const s32 pcmDataTag)
	{
		// grab from the nearest (previous) aligned boundary
		const s16 *alignedSrc = src;
		if((((size_t)src) & 15) != 0)
		{
			alignedSrc = (s16*)((((size_t)src+15)&~15) - 16);
		}
		const s32 extraBytesReqd = (src - alignedSrc) * sizeof(s16);
		const s32 requestedBytes = numSamples * sizeof(s16);
		const s32 actualBytes = (((requestedBytes + extraBytesReqd)+15) & ~15);
		Assert(actualBytes <= (s32)g_ScratchLoadBufferSize);

		// the requested destination might not be aligned
		const bool isDestAligned = ((((size_t)dest) & 15) == 0);
		const bool needToUseScratchBuffer = (requestedBytes != actualBytes) || (isDestAligned == false);
		s16 *dmaDest = needToUseScratchBuffer ? g_ScratchLoadBuffer : dest;

		//audDisplayf("FetchPCMSamples: %p, %p, %d - alignedSrc: %p, dest: %p extraBytes: %d, actualBytes: %d", 
		//dest, src, requestedBytes, 
		//alignedSrc, dmaDest, extraBytesReqd, actualBytes);

		AlignedAssert(alignedSrc, 16);
		AlignedAssert(dmaDest, 16);
		AlignedAssert(actualBytes, 16);

		sysDmaGet(dmaDest, (std::uint64_t)alignedSrc, actualBytes, pcmDataTag);
		if(needToUseScratchBuffer)
		{
			sysDmaWaitTagStatusAll(1<<pcmDataTag);
			// copy into final destination
			sysMemCpy(dest, ((u8*)g_ScratchLoadBuffer) + extraBytesReqd, requestedBytes);
		}
	}
#endif // __SPU

	u32 audStreamPlayer::GetPlayPositionSamples() const
	{
		return m_NumSamplesConsumed/m_NumChannels;
	}

	bool audStreamPlayer::ProcessSyncSignal(const audMixerSyncSignal &signal)
	{		
		if(signal.WasCanceled())
		{
			m_SyncSlaveId = audMixerSyncManager::InvalidId;
			m_FinishedPlayback = true;

			if(m_SyncMasterId != audMixerSyncManager::InvalidId)
			{
				audMixerSyncManager::Get()->Cancel(m_SyncMasterId);
				m_SyncMasterId = audMixerSyncManager::InvalidId;
			}

			return false;
		}
		if(!signal.HasTriggered())
		{
			// Still waiting on sync source
			return false;
		}
		// Grab subframe predelay from sync source
		u32 subframeOffset = signal.GetSubframeOffset();

		// Invalidate our sync id; it will be recycled after this trigger frame
		m_SyncSlaveId = audMixerSyncManager::InvalidId;

		u32 newPredelaySamples = m_PredelaySamples + subframeOffset;
		while(newPredelaySamples >= kMixBufNumSamples)
		{
			m_PredelayFrames++;
			newPredelaySamples -= kMixBufNumSamples;
		}

		m_PredelaySamples = newPredelaySamples;
		// Check for overflow
		Assert(m_PredelaySamples == newPredelaySamples);		
		m_PredelaySamples = subframeOffset;

		return true;
	}

	bool audStreamPlayer::EvaluatePredelay()
	{
		if(m_PredelayFrames > 0)
		{
			m_PredelayFrames--;
			return false;
		}
		return true;
	}

	void audStreamPlayer::SkipFrame()
	{
		AUD_PCMSOURCE_TIMER_SKIPFRAME;
		if(m_PlayState == kStateIdle || m_StopPlayback)
		{
			return;
		}
		else if(m_FinishedPlayback || m_PlayState == kStateStopped)
		{
			m_FinishedPlayback = true;
			return;
		}

		if(IsWaitingOnSelfSync())
		{
			audMixerSyncManager::Get()->Trigger(m_SyncMasterId, 0);
			m_SyncMasterId = audMixerSyncManager::InvalidId;
		}

		if(IsWaitingOnSync())
		{
			return;
		}

		if(!EvaluatePredelay())
		{
			return;
		}

		if(!m_RingBuffer || !m_RingBufferSize)
		{
			return;
		}

		if(!m_HasStarted)
		{
			// Wait until the ring buffer is 50% full
			if(IsRingBufferHalfFull())
			{
				m_HasStarted = true;
			}
			else
			{
				return;
			}
		}

		if(m_IsPaused && m_IsPauseRequested)
		{
			// Don't move forward when paused
			return;
		}

		u32 playbackFreq = m_SampleRate;
		const bool isRateConverted = (playbackFreq != kMixerNativeSampleRate) BANK_ONLY(&& !g_DisableSampleRateConversion);

		const u32 numSamplesToGenerate = kMixBufNumSamples - m_PredelaySamples;
		m_PredelaySamples = 0;
		// compute number of input samples
		f32 numInputSamplesF32 = 0.f;
		f32 numInputSamplesFloored = 0.f;
		u32 numInputSamples = 0;
		f32 ratio = 1.f;
		if(isRateConverted)
		{
			static const f32 oneOverNativeSampleRate = 1.f / (f32)kMixerNativeSampleRate;
			ratio = (f32)playbackFreq * oneOverNativeSampleRate;

			numInputSamplesF32 = (ratio * (f32)numSamplesToGenerate) + m_SrcTrailingFrac;

			numInputSamplesFloored = Floorf(numInputSamplesF32);
			numInputSamples = (u32)numInputSamplesFloored;

			m_SrcTrailingFrac = numInputSamplesF32 - numInputSamplesFloored;
		}
		else
		{
			numInputSamples = numSamplesToGenerate;
		}

		u32 numSamplesCopied = 0;

		const u32 numSamplesAvailable = ComputeSamplesAvailable() / m_NumChannels;

		// TODO: hook this flag up to the ringbuffer
		const bool endOfStream = true;
		if(numSamplesAvailable < numInputSamples && !endOfStream)
		{
			m_IsStarved = true;
			return;
		}

		numSamplesCopied = Min<u32>(numInputSamples, numSamplesAvailable);

		// consume samples
		AdvanceReadOffset(numSamplesCopied * m_NumChannels);

		if(m_FinishedPlayback && m_SyncMasterId != audMixerSyncManager::InvalidId)
		{
			audMixerSyncManager::Get()->Trigger(m_SyncMasterId, 0);
			m_SyncMasterId = audMixerSyncManager::InvalidId;
		}
	}

	void audStreamPlayer::GenerateFrame()
	{
		AUD_PCMSOURCE_TIMER_GENERATEFRAME;

		// src input buffer has to be large enough to cope with multiple interleaved channels, but we don't support
		// down sampling so the max number of source samples per frame per channel is kMixBufNumSamples
#if __SPU
		const size_t sizeOfSampleDataGatherBuffer = sizeof(s16) * (kMixBufNumSamples * kMaxPcmSourceChannels);
#endif
#if __SPU
		audAutoScratchBookmark autoScratch;
		s16* sampleDataGatherBuffer = (s16*) AllocateFromScratch( sizeOfSampleDataGatherBuffer, 16, "SampleDataGatherBuffer" );
		s16* interleavedBuffer = (s16*) AllocateFromScratch( sizeOfSampleDataGatherBuffer, 16, "interleavedBuffer" );
#else
		s16* sampleDataGatherBuffer = AllocaAligned( s16, kMixBufNumSamples * kMaxPcmSourceChannels, 16 );
		s16* interleavedBuffer = AllocaAligned( s16, kMixBufNumSamples * kMaxPcmSourceChannels, 16 );
#endif



		// Whatever happens, if we have any buffers we need to silence them
		Assert(HasBuffer(0));
		for(u32 outputIndex = 0; outputIndex < m_NumChannels; outputIndex++)
		{
			const u32 bufferId = GetBufferId(outputIndex);
			if(bufferId < audFrameBufferPool::InvalidId)
			{
				g_FrameBufferCache->ZeroBuffer(bufferId);
			}
		}

		if(m_PlayState == kStateIdle || m_RingBuffer == NULL || m_RingBufferData == NULL || m_StopPlayback)
		{
			return;
		}
		else if(m_FinishedPlayback || m_PlayState == kStateStopped)
		{
			m_FinishedPlayback = true;
			return;
		}

		if(!m_HasStarted)
		{
			// Wait until the ring buffer is 50% full
			if(IsRingBufferHalfFull())
			{
				m_HasStarted = true;
			}
			else
			{
				return;
			}
		}

		f32 ratio = 1.f;
		u32 numInputSamples;


		// We don't need to support downsampling for external streams
		const u32 playbackFreq = Min<u32>(kMixerNativeSampleRate, m_SampleRate);
		if(m_IsPaused && m_IsPauseRequested)
		{
			// Don't move forward when paused
			return;
		}

		bool isRateConverted = (playbackFreq != kMixerNativeSampleRate) BANK_ONLY(&& !g_DisableSampleRateConversion);

		// compute number of input samples
		const u32 numSamplesToGenerate = kMixBufNumSamples - m_PredelaySamples;

		f32 numInputSamplesF32 = 0.f;
		f32 numInputSamplesFloored = 0.f;
		u32 numInputSamplesU32 = 0;
		if(isRateConverted)
		{
			static const f32 oneOverNativeSampleRate = 1.f / (f32)kMixerNativeSampleRate;
			ratio = (f32)playbackFreq * oneOverNativeSampleRate;

			numInputSamplesF32 = (ratio * (f32)numSamplesToGenerate) + m_SrcTrailingFrac;
			// one extra sample since we interpolate from [i] to [i+1]
			numInputSamples = (u32)ceilf(numInputSamplesF32) + 1;
			numInputSamplesFloored = Floorf(numInputSamplesF32);
			numInputSamplesU32 = (u32)numInputSamplesFloored;
		}
		else
		{
			numInputSamples = numSamplesToGenerate;
		}

		if(IsWaitingOnSelfSync())
		{
			audMixerSyncManager::Get()->Trigger(m_SyncMasterId, 0);
			m_SyncMasterId = audMixerSyncManager::InvalidId;	
		}

		if(IsWaitingOnSync() || !EvaluatePredelay())
		{
			return;
		}


		// data already in PCM format
		// deinterleave data into input buffer

		u32 numSamplesCopied = 0;

		const u32 numSamplesAvailable = ComputeSamplesAvailable() / m_NumChannels;

		// TODO: hook this flag up to the ringbuffer
		const bool endOfStream = true;
		if(numSamplesAvailable < numInputSamples && !endOfStream)
		{
			m_IsStarved = true;
			return;
		}

		numSamplesCopied = Min<u32>(numInputSamples, numSamplesAvailable);

		sysMemZeroBytes<kMixBufNumSamples * kMaxPcmSourceChannels * sizeof(s16)>(sampleDataGatherBuffer);

		// De-interleave into sampleDataGatherBuffer, via interleavedBuffer
		const u32 numBytesPerPass = (numSamplesCopied<<1) * m_NumChannels;
		for(u32 pass = 0; pass < 1; pass++)
		{
			CopyRingBufferData(interleavedBuffer, numBytesPerPass, pass * numBytesPerPass);

			for(u32 sampleIndex = 0, readIndex = 0; sampleIndex < numSamplesCopied; sampleIndex++)
			{
				for(u32 channelIndex = 0; channelIndex < m_NumChannels; channelIndex++)
				{
					sampleDataGatherBuffer[channelIndex*kMixBufNumSamples + sampleIndex + pass*numSamplesCopied] = interleavedBuffer[readIndex++];
				}
			}
		}

		Assert(numSamplesCopied <= numInputSamples);

		u32 numOutputSamples = 0;
		for(u32 outputIndex = 0; outputIndex < m_NumChannels; outputIndex++)
		{
			f32 *destBuffer = g_FrameBufferCache->GetBuffer(GetBufferId(outputIndex));

			u32 advanceBySamples = 0;
			// resample and convert to normalized f32 data
			if(isRateConverted)
			{
				numOutputSamples = audRateConvertorLinear::Process(ratio, destBuffer, sampleDataGatherBuffer + kMixBufNumSamples*outputIndex, numInputSamples, numSamplesToGenerate, m_SrcTrailingFrac);

				// advance by the number of samples actually used by SRC
				advanceBySamples = Min<u32>(numSamplesCopied, numInputSamplesU32);
			}
			else
			{
				// we dont need to resample so copy/convert straight into the mix buffer
				if (destBuffer)
				{
					CopyMonoBufferToMonoMixBuf(destBuffer, sampleDataGatherBuffer + outputIndex * kMixBufNumSamples, numInputSamples);
				}
				numOutputSamples = numInputSamples;
				advanceBySamples = numInputSamples;
			}

			AdvanceReadOffset(advanceBySamples);

			if (destBuffer)
			{
				synthUtil::ScaleBuffer(destBuffer, V4LoadScalar32IntoSplatted(m_Gain), kMixBufNumSamples);
			}

			if (destBuffer)
			{
				if (m_IsPaused && !m_IsPauseRequested)
				{
					// apply a one-frame fade-in
					FadeInBuffer<kMixBufNumSamples>(destBuffer);
				}
				else if (!m_IsPaused && m_IsPauseRequested)
				{
					// apply a one-frame fade-out
					FadeOutBuffer<kMixBufNumSamples>(destBuffer);
				}
			}
		}

		if(isRateConverted)
		{
			m_SrcTrailingFrac = numInputSamplesF32 - numInputSamplesFloored;
		}
		if(m_FinishedPlayback && m_SyncMasterId != audMixerSyncManager::InvalidId)
		{
			audMixerSyncManager::Get()->Trigger(m_SyncMasterId, numOutputSamples);
			m_SyncMasterId = audMixerSyncManager::InvalidId;
		}
	}

	u32 audStreamPlayer::ComputeSamplesAvailable() const
	{
#if __SPU
		// m_RingBufferState is EA
		const u32 bytesAvailable = sysDmaGetUInt32((uint64_t)&m_RingBufferState->BufferedBytes, 7);
#else
		const u32 bytesAvailable = m_RingBufferState->BufferedBytes;
#endif

		// assume 16 bits per sample
		return bytesAvailable>>1;
	}

	bool audStreamPlayer::IsRingBufferHalfFull() const
	{
		const u32 numSamplesAvailable = ComputeSamplesAvailable();
		// assume 16bits per sample
		const u32 halfRingBufferSizeSamples = m_RingBufferSize>>2;
		return numSamplesAvailable >= halfRingBufferSizeSamples;
	}

	void audStreamPlayer::AdvanceReadOffset(const u32  numSamples)
	{
		u32 numSamplesToAdvance = numSamples;

		if(numSamples > ComputeSamplesAvailable())
		{
			// Clamp to avoid destroying the ring buffer offsets by reading more bytes than there are available
			audDisplayf("audStreamPlayer attempting to read more samples (%u) that there are available (%u)! Value will be clamped.", numSamples, ComputeSamplesAvailable());
			numSamplesToAdvance = ComputeSamplesAvailable();			
		}

#if __SPU
		m_RingBuffer->AdvanceReadPtr(numSamplesToAdvance << 1, m_RingBufferSize);
#else
		m_RingBuffer->AdvanceReadPtr(numSamplesToAdvance << 1);
#endif

		m_RingBufferReadOffset += numSamplesToAdvance<<1;
		m_RingBufferReadOffset %= m_RingBufferSize;

		// TODO: Intentionally advancing the requested number of samples regardless of number of samples available. 
		// Will mean that GetPlayPositionSamples advances even in the event of stream starvation - desirable?
		m_NumSamplesConsumed += numSamples;
	}

	void audStreamPlayer::CopyRingBufferData(s16 *dest, const u32 size, const u32 requestedOffset)
	{
		Assert(size <= m_RingBufferSize);
		Assert(requestedOffset <= m_RingBufferSize);

		u32 offset = (requestedOffset + m_RingBufferReadOffset) % m_RingBufferSize;
		u32 size1 = Min(size, m_RingBufferSize - offset);
		const s16 *ptr1 = reinterpret_cast<const s16*>(m_RingBufferData + offset);
		const s16 *ptr2 = NULL;
		u32 size2 = 0;

		if(size1 < size)
		{
			ptr2 = reinterpret_cast<const s16*>(m_RingBufferData);
			size2 = size - size1;
		}

#if __SPU
		ALIGNAS(16) u8 tempBuffer[1024] ;
		if(audVerifyf(sizeof(tempBuffer) >= size1 && sizeof(tempBuffer) >= size2, "StreamPlayer temp buffer too small: %u vs %u / %u", (u32)sizeof(tempBuffer), size1, size2))
		{
			dmaUnalignedGet(tempBuffer, sizeof(tempBuffer), dest, size1, reinterpret_cast<u32>(ptr1));
			if(ptr2 && size2)
			{
				dmaUnalignedGet(tempBuffer, sizeof(tempBuffer), dest + (size1>>1), size2, reinterpret_cast<u32>(ptr2));
			}
		}
#else
		sysMemCpy(dest, ptr1, size1);
		if(ptr2 && size2)
		{
			sysMemCpy(dest + (size1>>1), ptr2, size2);
		}		
#endif
	}
}
