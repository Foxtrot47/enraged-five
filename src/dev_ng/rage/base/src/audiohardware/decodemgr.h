//
// audiohardware/decodemgr.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DECODEMGR_H
#define AUD_DECODEMGR_H

#include "atl/array.h"
#include "atl/bitset.h"
#include "system/ipc.h"
#include "system/criticalsection.h"
#include "system/fixedallocator.h"
#include "decoder.h"
#include "waveref.h"

namespace rage
{

// PURPOSE
//	Defines the maximum packets that may be submitted to any one stream at a time
enum {kMaxSubmittedPacketsPerStream = 2};

enum audPacketState
{
	AUD_PACKET_QUEUED = 0,
	AUD_PACKET_DECODING,
	AUD_PACKET_ERROR,
	AUD_PACKET_INVALID_ID,
};

enum audStreamState
{
	AUD_STREAM_IDLE = 0,
	AUD_STREAM_ACTIVE,
	AUD_STREAM_ERROR,
	AUD_STREAM_CLOSING,
	AUD_STREAM_CLOSED,
};

struct audStreamPacketState
{
	audPacketId clientPacketId;
	void *data;
	u32 size;
	audPacketState state;
	s32 timeQueued;
	s32 timeDecodeStarted;
	s32 timeDecodeFinished;
};
//
//struct audStreamData
//{
//	audStreamId clientStreamId;
//	audWaveFormat::audStreamFormat format;
//	audDecoder *decoder;
//	u32 currentPacketIdx;
//	u32 currentDecodePacketIdx;
//	u32 numChannels;
//	audLoopData loopData;
//	audStreamState state;
//	audStreamPacketState packetQueue[kMaxSubmittedPacketsPerStream];
//	u32 sampleRate;
//	bool hasSetLoopData;
//	bool hasRequestedClose;
//};

// PURPOSE
//	Cross platform managed asynchronous wave data decoding interface.  Various data formats are supported, output
//	format is native endian s16 PCM.  Management of the decoding runs on a dedicated thread, decoding runs on a seperate
//	thread on PC, on SPUs on PS3 and in XMA hardware for XMA2 on Xenon.
// NOTES
//	Use CreateStream() to allocate a decoder for a data stream, the pass in encoded data via SubmitPacket().
//	Once the data is ready to be consumed QueryPacketState() will return AUD_PACKET_READY and ConsumePacketData()
//	can be called to retrieve the decoded 32bit PCM data.
class audDecodeManager
{
public:

	audDecodeManager();
	~audDecodeManager();

	bool Init();
	void Shutdown();

	// PURPOSE
	//	Returns true if the requested format can be decoded
	// NOTES
	//	Different formats are supported on different platforms (for example XMA2 is Xenon only) and for
	//	PC user formats support may depend on optional 3rd party libraries (Quicktime etc)
	bool IsFormatSupported(const audWaveFormat::audStreamFormat format) const;

	static audDecoder *AllocateDecoder(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 sampleRate);
	static void FreeDecoder(audDecoder *decoder);
	static void PostMix();

	static u32 GetNumAllocatedStreams() { return sm_NumStreamsAllocated; }

private:

	sysCriticalSectionToken m_AllocationCS;

	static u8 *sm_HeapMem;
	static sysMemFixedAllocator *sm_Allocator;
	static u32 sm_NumStreamsAllocated;
	static atArray<audDecoder*> sm_DecodersToFree;
};

} // namespace rage

#endif // AUD_DECODEMGR_H
