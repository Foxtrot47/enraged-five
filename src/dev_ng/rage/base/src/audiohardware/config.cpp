// 
// audiohardware/config.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "config.h"

namespace rage
{
	audMetadataManager audConfig::sm_Metadata;
	bool audConfig::sm_IsInitialised = false;

	bool audConfig::InitClass(const char *path)
	{
		// TODO: NameTable is required for wave slot initialisation but could be dumped after that.
		sm_IsInitialised = sm_Metadata.Init("AudioConfig", path, audMetadataManager::NameTable_Always, SIMPLETYPES_SCHEMA_VERSION);
		return sm_IsInitialised;
	}

	void audConfig::ShutdownClass()
	{
		if(sm_IsInitialised)
		{
			sm_Metadata.Shutdown();
			sm_IsInitialised = false;
		}
	}
}
