//
// audiohardware/mixer_vmath.inl
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// PURPOSE
//	Platform independent mixer routines utilising the RAGE vectormath library

#include "audiohardware/driverdefs.h"
#include "vectormath/vectormath.h"

namespace rage
{

	struct tBiquadCoefficients
	{
		float a0[4];
		float a1[4];
		float a2[4];
		float b1[4];
		float b2[4];
	};

	struct tBiquadHistory
	{
		float xy11[4];
		float xy12[4];
		float xy21[4];
		float xy22[4];
	};

using namespace Vec;

template<const bool isLowPass> __inline void ComputeBiquadCoefficients(const unsigned int *cutoff, tBiquadCoefficients &coeffs, tBiquadHistory &history)
{
	//const float omegaTransConst = oneOverNativeSampleRate * 2.f * PI;
	//Vector_4V omegaTrans = V4LoadScalar32IntoSplatted(omegaTransConst);
	const Vector_4V omegaTrans = 
		(kMixerNativeSampleRate == 48000 ? 
				// 48kHz = 1.3089969389957471826927680763665e-4
				V4VConstant<0x3909421E,0x3909421E,0x3909421E,0x3909421E>()
				// 44.1khz = 1.4247585730565955729989312395825e-4	
				: V4VConstant<0x39156592,0x39156592,0x39156592,0x39156592>());
	CompileTimeAssert(kMixerNativeSampleRate == 48000 || kMixerNativeSampleRate == 44100);
	
	Vector_4V  a0, a1, a2, b0, b1, b2;

	const Vector_4V intCutoffs = *((Vector_4V*)&cutoff[0]);
	//__m128i intCutoffs = _mm_load_si128((__m128i*)&cutoff[0]);
	// convert to float
	const Vector_4V f32Cutoffs = V4IntToFloatRaw<0>(intCutoffs);

	const Vector_4V omega = V4Scale(omegaTrans, f32Cutoffs);

	Vector_4V cs;
	Vector_4V sn;
	V4SinAndCos(sn, cs, omega);
	const Vector_4V ones = V4VConstant(V_ONE);
	const Vector_4V point5 = V4VConstant(V_HALF);

	// -3.01dB at Fc, multiplied by 0.5f to compensate for cascaded sections as we're running 4 pole
	//so constant is sinh(0.5 / (10^(-3.01 * 0.05) * 0.5))
	
	const Vector_4V alpha = V4Scale(sn,V4VConstant<0x3FF7ACC8,0x3FF7ACC8,0x3FF7ACC8,0x3FF7ACC8>());//1.93496045f
	if(isLowPass)
	{
		a0 = V4Scale(V4Subtract(ones, cs), point5);
		a1 = V4Subtract(ones, cs);
	}
	else
	{
		a0 = V4Scale(V4Add(ones, cs), point5);
		a1 = V4Subtract(Vec::V4VConstant(V_NEGONE), cs);		
	}

	a2 = a0;
	b0 = V4Add(ones, alpha);
	b1 = V4Scale(Vec::V4VConstant(V_NEGTWO), cs);
	b2 = V4Subtract(ones, alpha);

	//Normalize so b0 = 1.0.
	const Vector_4V oneOverb0 = V4Invert(b0);
	a0 = V4Scale(a0, oneOverb0);
	a1 = V4Scale(a1, oneOverb0);
	a2 = V4Scale(a2, oneOverb0);
	b1 = V4Scale(b1, oneOverb0);
	b2 = V4Scale(b2, oneOverb0);

	// clamp to safe coefficients if cutoff is near nyquist
	// 21kHz
	const Vector_4V nearNyquist = (kVoiceFilterLPFMaxCutoff == 20000 ?
		// 21kHz
		//V4VConstant<0x46A41000,0x46A41000,0x46A41000,0x46A41000>() :
		// 20kHz
		V4VConstant<0x469C4000,0x469C4000,0x469C4000,0x469C4000>() :
		// 23.9kHz
		V4VConstant<0x46BAB800,0x46BAB800,0x46BAB800,0x46BAB800>());
	CompileTimeAssert(kVoiceFilterLPFMaxCutoff == 20000||kVoiceFilterLPFMaxCutoff == 23900);
	
	
	const Vector_4V cmpMask = V4IsGreaterThanOrEqualV(f32Cutoffs, nearNyquist);

	Vector_4V xy11 = *((Vector_4V*)&history.xy11[0]);
	Vector_4V xy12 = *((Vector_4V*)&history.xy12[0]);
	Vector_4V xy21 = *((Vector_4V*)&history.xy21[0]);
	Vector_4V xy22 = *((Vector_4V*)&history.xy22[0]);

	a0 = V4SelectFT(cmpMask, a0, ones);
	// zero other coefficients at or above our cutoff threshold
	a1 = V4Andc(a1, cmpMask);
	a2 = V4Andc(a2, cmpMask);
	b1 = V4Andc(b1, cmpMask);
	b2 = V4Andc(b2, cmpMask);

	// conditionally zero history if we're bypassing the filter
	xy11 = V4Andc(xy11, cmpMask);
	xy12 = V4Andc(xy12, cmpMask);
	xy21 = V4Andc(xy21, cmpMask);
	xy22 = V4Andc(xy22, cmpMask);

	*((Vector_4V*)&coeffs.a0[0]) = a0;
	*((Vector_4V*)&coeffs.a1[0]) = a1;
	*((Vector_4V*)&coeffs.a2[0]) = a2;
	*((Vector_4V*)&coeffs.b1[0]) = b1;
	*((Vector_4V*)&coeffs.b2[0]) = b2;	

	*((Vector_4V*)&history.xy11[0]) = xy11;
	*((Vector_4V*)&history.xy12[0]) = xy12;
	*((Vector_4V*)&history.xy21[0]) = xy21;
	*((Vector_4V*)&history.xy22[0]) = xy22;
}

void ComputeHPFCoefficients_Single(const u32 cutoff, tBiquadCoefficients &coeffs, tBiquadHistory &UNUSED_PARAM(history), const u32 subIndex)
{
	const f32 oneOverNativeSampleRate = 1.f / (f32)kMixerNativeSampleRate;
	const f32 omegaTrans = oneOverNativeSampleRate * 2.f * PI;
	const f32 cutoffFrequency = (f32)cutoff;
	const bool isLowPass = false;

	f32 a0, a1, a2, b0, b1, b2;
	const f32 omega = omegaTrans * cutoffFrequency;

	f32 cs, sn;
	cos_and_sin(cs, sn, omega);

	// -3.01dB at Fc, multiplied by 0.5f to compensate for cascaded sections as we're running 4 pole
	//so constant is sinh(0.5 / (10^(-3.01 * 0.05) * 0.5))
	const f32 alpha = sn * 1.93496045f;
	if(isLowPass)
	{
		a0 = (1.0f - cs) * 0.5f;
		a1 =  1.0f - cs;
	}
	else
	{
		a0 = (1.0f + cs) * 0.5f;
		a1 = -1.0f - cs;		
	}

	a2 = a0;
	b0 = 1.0f + alpha;
	b1 = -2.0f * cs;
	b2 = 1.0f - alpha;

	//Normalize so b0 = 1.0.
	const f32 oneOverb0 = 1.f / b0;
	coeffs.a0[subIndex] = a0 * oneOverb0;
	coeffs.a1[subIndex] = a1 * oneOverb0;
	coeffs.a2[subIndex] = a2 * oneOverb0;
	coeffs.b1[subIndex] = b1 * oneOverb0;
	coeffs.b2[subIndex] = b2 * oneOverb0;
}

template <const u32 numSamples> void VoiceFilter(float *samples[4], tBiquadCoefficients &coeffs0, tBiquadHistory &history)
{
	Vector_4V x0y0z0w0, x1y1z1w1, x2y2z2w2, x3y3z3w3;
	Vector_4V x1y1x0y0 = V4VConstant(V_ZERO), x3y3x2y2 = V4VConstant(V_ZERO);
	Vector_4V z1w1z0w0 = V4VConstant(V_ZERO), z3w3z2w2 = V4VConstant(V_ZERO);

	const Vector_4V denormal_fix = V4VConstant<0x219392EE,0x219392EE,0x219392EE,0x219392EE>(); // 1e-18f;

	struct filterState
	{
		Vector_4V a0;
		Vector_4V a1;
		Vector_4V a2;
		Vector_4V b1;
		Vector_4V b2;
		Vector_4V xy11;
		Vector_4V xy12;
		Vector_4V xy21;
		Vector_4V xy22;
	}f0;

	f0.a0 = *(Vector_4V*)(&coeffs0.a0[0]);
	f0.a1 = *(Vector_4V*)(&coeffs0.a1[0]);
	f0.a2 = *(Vector_4V*)(&coeffs0.a2[0]);
	f0.b1 = *(Vector_4V*)(&coeffs0.b1[0]);
	f0.b2 = *(Vector_4V*)(&coeffs0.b2[0]);
	f0.xy11 = V4Add(*(Vector_4V*)(&history.xy11[0]),denormal_fix);
	f0.xy12 = V4Add(*(Vector_4V*)(&history.xy12[0]),denormal_fix);
	f0.xy21 = V4Add(*(Vector_4V*)(&history.xy21[0]),denormal_fix);
	f0.xy22 = V4Add(*(Vector_4V*)(&history.xy22[0]),denormal_fix);

	for(unsigned int i = 0; i < numSamples; i += 4)
	{
		x0y0z0w0 = *(Vector_4V*)&samples[0][i];
		x1y1z1w1 = *(Vector_4V*)&samples[1][i];
		x2y2z2w2 = *(Vector_4V*)&samples[2][i];
		x3y3z3w3 = *(Vector_4V*)&samples[3][i];

		x1y1x0y0 = V4PermuteTwo<Vec::X2,Vec::Y2,Vec::X1,Vec::Y1>(x0y0z0w0,x1y1z1w1);
		x3y3x2y2 = V4PermuteTwo<Vec::X2,Vec::Y2,Vec::X1,Vec::Y1>(x2y2z2w2,x3y3z3w3);
		z1w1z0w0 = V4PermuteTwo<Vec::Z2,Vec::W2,Vec::Z1,Vec::W1>(x0y0z0w0,x1y1z1w1);
		z3w3z2w2 = V4PermuteTwo<Vec::Z2,Vec::W2,Vec::Z1,Vec::W1>(x2y2z2w2,x3y3z3w3);

		const Vector_4V samples0 = V4PermuteTwo<Vec::Z1,Vec::X1,Vec::Z2,Vec::X2>(x1y1x0y0, x3y3x2y2);
		const Vector_4V samples1 = V4PermuteTwo<Vec::W1,Vec::Y1,Vec::W2,Vec::Y2>(x1y1x0y0, x3y3x2y2);
		const Vector_4V samples2 = V4PermuteTwo<Vec::Z1,Vec::X1,Vec::Z2,Vec::X2>(z1w1z0w0, z3w3z2w2);
		const Vector_4V samples3 = V4PermuteTwo<Vec::W1,Vec::Y1,Vec::W2,Vec::Y2>(z1w1z0w0, z3w3z2w2);

		/*currentSample1 = *samples;
		newSample1 = (a_0 * currentSample1) + xy_n1_1;
		currentSample2 = newSample1; //Current sample is now output of last stage.
		newSample2 = (a_0 * currentSample2) + xy_n1_2;

		xy_n1_1 = (a_1 * currentSample1) - (b_1 * newSample1) + xy_n2_1;
		xy_n1_2 = (a_1 * currentSample2) - (b_1 * newSample2) + xy_n2_2;
		xy_n2_1 = (a_2 * currentSample1) - (b_2 * newSample1);
		xy_n2_2 = (a_2 * currentSample2) - (b_2 * newSample2);
		*/

		Vector_4V output0, output1, output2, output3;
		{
			// filter0
			const Vector_4V currentSample1 = samples0;
			const Vector_4V newSample1 = V4Add(V4Scale(f0.a0, currentSample1), f0.xy11);
			const Vector_4V currentSample2 = newSample1;
			const Vector_4V newSample2 = V4Add(V4Scale(f0.a0, currentSample2), f0.xy12);
			f0.xy11 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample1), V4Scale(f0.b1, newSample1)), f0.xy21);
			f0.xy12 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample2), V4Scale(f0.b1, newSample2)), f0.xy22);
			f0.xy21 = V4Subtract(V4Scale(f0.a2, currentSample1), V4Scale(f0.b2, newSample1));
			f0.xy22 = V4Subtract(V4Scale(f0.a2, currentSample2), V4Scale(f0.b2, newSample2));

			output0 = newSample2;
		}

		{
			// filter0
			const Vector_4V currentSample1 = samples1;
			const Vector_4V newSample1 = V4Add(V4Scale(f0.a0, currentSample1), f0.xy11);
			const Vector_4V currentSample2 = newSample1;
			const Vector_4V newSample2 = V4Add(V4Scale(f0.a0, currentSample2), f0.xy12);
			f0.xy11 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample1), V4Scale(f0.b1, newSample1)), f0.xy21);
			f0.xy12 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample2), V4Scale(f0.b1, newSample2)), f0.xy22);
			f0.xy21 = V4Subtract(V4Scale(f0.a2, currentSample1), V4Scale(f0.b2, newSample1));
			f0.xy22 = V4Subtract(V4Scale(f0.a2, currentSample2), V4Scale(f0.b2, newSample2));

			output1 = newSample2;
		}

		{
			// filter0
			const Vector_4V currentSample1 = samples2;
			const Vector_4V newSample1 = V4Add(V4Scale(f0.a0, currentSample1), f0.xy11);
			const Vector_4V currentSample2 = newSample1;
			const Vector_4V newSample2 = V4Add(V4Scale(f0.a0, currentSample2), f0.xy12);
			f0.xy11 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample1), V4Scale(f0.b1, newSample1)), f0.xy21);
			f0.xy12 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample2), V4Scale(f0.b1, newSample2)), f0.xy22);
			f0.xy21 = V4Subtract(V4Scale(f0.a2, currentSample1), V4Scale(f0.b2, newSample1));
			f0.xy22 = V4Subtract(V4Scale(f0.a2, currentSample2), V4Scale(f0.b2, newSample2));

			output2 = newSample2;
		}

		{
			// filter0
			const Vector_4V currentSample1 = samples3;
			const Vector_4V newSample1 = V4Add(V4Scale(f0.a0, currentSample1), f0.xy11);
			const Vector_4V currentSample2 = newSample1;
			const Vector_4V newSample2 = V4Add(V4Scale(f0.a0, currentSample2), f0.xy12);
			f0.xy11 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample1), V4Scale(f0.b1, newSample1)), f0.xy21);
			f0.xy12 = V4Add(V4Subtract(V4Scale(f0.a1, currentSample2), V4Scale(f0.b1, newSample2)), f0.xy22);
			f0.xy21 = V4Subtract(V4Scale(f0.a2, currentSample1), V4Scale(f0.b2, newSample1));
			f0.xy22 = V4Subtract(V4Scale(f0.a2, currentSample2), V4Scale(f0.b2, newSample2));

			output3 = newSample2;
		}

		// unswizzle back to AoS, step 1
		x1y1x0y0 = V4Add(V4PermuteTwo<Vec::Y1,Vec::Y2,Vec::X1,Vec::X2>(output0, output1),denormal_fix);
		x3y3x2y2 = V4Add(V4PermuteTwo<Vec::W1,Vec::W2,Vec::Z1,Vec::Z2>(output0,output1),denormal_fix);
		z1w1z0w0 = V4Add(V4PermuteTwo<Vec::Y1,Vec::Y2,Vec::X1,Vec::X2>(output2, output3),denormal_fix);
		z3w3z2w2 = V4Add(V4PermuteTwo<Vec::W1,Vec::W2,Vec::Z1,Vec::Z2>(output2, output3),denormal_fix);

		// step 2
		x0y0z0w0 = V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(x1y1x0y0,z1w1z0w0);
		x1y1z1w1 = V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(x1y1z1w1,z1w1z0w0);
		x2y2z2w2 = V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(x3y3x2y2,z3w3z2w2);
		x3y3z3w3 = V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(x3y3x2y2,z3w3z2w2);

		// write back to mem
		*(Vector_4V*)&samples[0][i] = x0y0z0w0;
		*(Vector_4V*)&samples[1][i] = x1y1z1w1;
		*(Vector_4V*)&samples[2][i] = x2y2z2w2;
		*(Vector_4V*)&samples[3][i] = x3y3z3w3;
	}

	// store modified history
	*(Vector_4V*)(&history.xy11[0]) = V4Subtract(f0.xy11, denormal_fix);
	*(Vector_4V*)(&history.xy12[0]) = V4Subtract(f0.xy12, denormal_fix);
	*(Vector_4V*)(&history.xy21[0]) = V4Subtract(f0.xy21, denormal_fix);
	*(Vector_4V*)(&history.xy22[0]) = V4Subtract(f0.xy22, denormal_fix);
}

#define Undenormalize(sample) ((((*(u32*)&sample)&0x7f800000)==0)?0.0f:sample)
template <const u32 numSamples> void VoiceFilter_Single(f32 *samples, tBiquadCoefficients &coeffs0, tBiquadHistory &history, u32 subIndex)
{
	for(u32 i = 0; i < numSamples; i++)
	{
		f32 currentSample1 = samples[i];
		f32 newSample1 = (coeffs0.a0[subIndex] * currentSample1) + history.xy11[subIndex];
		f32 currentSample2 = newSample1; //Current sample is now output of last stage.
		f32 newSample2 = (coeffs0.a0[subIndex] * currentSample2) + history.xy12[subIndex];

		history.xy11[subIndex] = (coeffs0.a1[subIndex] * currentSample1) - (coeffs0.b1[subIndex] * newSample1) + history.xy21[subIndex];
		history.xy12[subIndex] = (coeffs0.a1[subIndex] * currentSample2) - (coeffs0.b1[subIndex] * newSample2) + history.xy22[subIndex];
		history.xy21[subIndex] = (coeffs0.a2[subIndex] * currentSample1) - (coeffs0.b2[subIndex] * newSample1);
		history.xy22[subIndex] = (coeffs0.a2[subIndex] * currentSample2) - (coeffs0.b2[subIndex] * newSample2);

		samples[i] = newSample2;
	}

	history.xy11[subIndex] = Undenormalize(history.xy11[subIndex]);
	history.xy12[subIndex] = Undenormalize(history.xy12[subIndex]);
	history.xy21[subIndex] = Undenormalize(history.xy21[subIndex]);
	history.xy22[subIndex] = Undenormalize(history.xy22[subIndex]);
}

//! In-place down-mix 6-channel to 2-channel 
template <const u32 numSamples> void DownmixOutputToStereo(f32 *buf)
{
	// FL = (FL) + 0.707(FC) + 0.707(RL)
	// FR = (FR) + 0.707(FC) + 0.707(RR)
	
	// 0.707f
	const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();
	const Vector_4V zero = V4VConstant(V_ZERO);

	for(u32 i = 0; i < numSamples; i += 4)
	{
		Vector_4V fl = *(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_FRONT_LEFT)]);
		Vector_4V fr = *(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_FRONT_RIGHT)]);
		Vector_4V fc = *(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_FRONT_CENTER)]);
		// 3 is LFE which we're ignoring
		Vector_4V rl = *(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_BACK_LEFT)]);
		Vector_4V rr = *(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_BACK_RIGHT)]);

		Vector_4V fcContrib = V4Scale(fc, centreRearRatio);
		Vector_4V newFL = V4Add(fl, V4AddScaled(fcContrib, rl, centreRearRatio));
		Vector_4V newFR = V4Add(fr, V4AddScaled(fcContrib, rr, centreRearRatio));

		// silence everything except FL/FR
		*(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_FRONT_CENTER)]) = zero;
		*(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_LOW_FREQUENCY)]) = zero;
		*(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_BACK_LEFT)]) = zero;
		*(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_BACK_RIGHT)]) = zero;

		*(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_FRONT_LEFT)]) = newFL;
		*(Vector_4V*)(&buf[i + (numSamples * RAGE_SPEAKER_ID_FRONT_RIGHT)]) = newFR;
	}
}

//! Downmix a 6 channel source to a 2 channel destination buffer
template <const u32 numSamples> void DownmixOutputToStereoOnlyBuffer( f32 const * src, f32* dst )
{
	// FL = (FL) + 0.707(FC) + 0.707(RL)
	// FR = (FR) + 0.707(FC) + 0.707(RR)

	// 0.707f
	const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();

	for(u32 i = 0; i < numSamples; i += 4)
	{
		Vector_4V fl = *(Vector_4V*)(&src[i + (numSamples * RAGE_SPEAKER_ID_FRONT_LEFT)]);
		Vector_4V fr = *(Vector_4V*)(&src[i + (numSamples * RAGE_SPEAKER_ID_FRONT_RIGHT)]);
		Vector_4V fc = *(Vector_4V*)(&src[i + (numSamples * RAGE_SPEAKER_ID_FRONT_CENTER)]);
		// 3 is LFE which we're ignoring
		Vector_4V rl = *(Vector_4V*)(&src[i + (numSamples * RAGE_SPEAKER_ID_BACK_LEFT)]);
		Vector_4V rr = *(Vector_4V*)(&src[i + (numSamples * RAGE_SPEAKER_ID_BACK_RIGHT)]);

		Vector_4V fcContrib = V4Scale(fc, centreRearRatio);
		Vector_4V newFL = V4Add(fl, V4AddScaled(fcContrib, rl, centreRearRatio));
		Vector_4V newFR = V4Add(fr, V4AddScaled(fcContrib, rr, centreRearRatio));

		*(Vector_4V*)(&dst[i + (numSamples * RAGE_SPEAKER_ID_FRONT_LEFT)]) = newFL;
		*(Vector_4V*)(&dst[i + (numSamples * RAGE_SPEAKER_ID_FRONT_RIGHT)]) = newFR;
	}
}

//! Copy a 6 channel source to a 2 channel destination buffer, no downmixing
template <const u32 numSamples> void CopyToStereoOnlyBuffer( f32 const * src, f32* dst )
{
	for(u32 i = 0; i < numSamples; i += 4)
	{
		Vector_4V fl = *(Vector_4V*)(&src[i + (numSamples * RAGE_SPEAKER_ID_FRONT_LEFT)]);
		Vector_4V fr = *(Vector_4V*)(&src[i + (numSamples * RAGE_SPEAKER_ID_FRONT_RIGHT)]);

		*(Vector_4V*)(&dst[i + (numSamples * RAGE_SPEAKER_ID_FRONT_LEFT)]) = fl;
		*(Vector_4V*)(&dst[i + (numSamples * RAGE_SPEAKER_ID_FRONT_RIGHT)]) = fr;
	}
}

void ComputeFilterCoefficients(const u32 numVoices, const u32 *const lpfCutoffs, tBiquadCoefficients *lpfCoefficients, tBiquadHistory *lpfHistory, const u32 *const hpfCutoffs, tBiquadCoefficients *hpfCoefficients, tBiquadHistory *hpfHistory)
{
	for(u32 i = 0, j = 0; i < numVoices; i += 4, j++)
	{
		ComputeBiquadCoefficients<true>(&lpfCutoffs[i], lpfCoefficients[j], lpfHistory[j]);

		// The vectorised coefficient computation isn't stable enough for the HPF close to 0Hz, so
		// for now use the FPU version but only on any voice that has a non-identity cutoff
		if(hpfCutoffs[i] + hpfCutoffs[i+1] + hpfCutoffs[i+2] + hpfCutoffs[i+3]>0)
		{
			for(u32 k = 0; k < 4; k++)
			{
				if(hpfCutoffs[i+k] > 0)
				{
					ComputeHPFCoefficients_Single(hpfCutoffs[i+k], hpfCoefficients[j], hpfHistory[j], k);
				}
			}
		}
	}
}


} // namespace rage
