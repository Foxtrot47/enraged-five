//
// audiohardware/device_pc.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_MIXERDEVICEPC_H
#define AUD_MIXERDEVICEPC_H

#if __WIN32PC

#include "device.h"

#define OUTPUT_MIXBUFFER_AUDIO 0

typedef struct IDirectSound                 *LPDIRECTSOUND;
typedef struct IDirectSoundBuffer           *LPDIRECTSOUNDBUFFER;

namespace rage
{

class audMixerDevicePc : public audMixerDevice
{
public:

	friend class audDeviceAsio;
	audMixerDevicePc();

	// PURPOSE
	//	Initialises direct sound wave_format_ex output hardware
	// RETURNS
	//	false on failure
	virtual bool InitHardware();

	// PURPOSE
	//	Releases DirectSound output resources
	virtual void ShutdownHardware();

	// PURPOSE
	//	Starts generating output
	virtual void StartMixing();

	// PURPOSE
	//	Runs a CPU intensive benchmark
	// RETURNS
	//	CPU score
	f32 BenchmarkSystem();

	static bool CheckForSoundCard();
private:

	bool InitOutputHardwareDS(bool reinit = false);


	// PURPOSE
	//	copies the mixer output (f32) buffer to the final output (s16) buffer
	__inline void CopyMixerOutputToHardware_DS_FPU(s16 *dest);
	__inline void CopyMixerOutputToHardware_DS_SSE(s16 *dest);

	// PURPOSE
	//	Entry point for mix thread
	// PARAMS
	//	pInstance - instance data, contains a pointer to the mixer device
	static DECLARE_THREAD_FUNC(MixThreadEntryProc_DS);
	
	void MixLoop_DS();

	// DirectSound interface
	LPDIRECTSOUND m_DS;
	LPDIRECTSOUNDBUFFER m_DSBuffer;

	u32 m_NumDSBuffers;
	u32 m_LastGeneratedBufferIdx;
	
	// Working buffer for interleaving
	s16 *m_OutputNonInterleaved;

	volatile bool m_IsMixThreadRunning, m_ShouldMixThreadBeRunning;
	sysIpcThreadId m_MixThread;
#if OUTPUT_MIXBUFFER_AUDIO
	HANDLE m_hMixerFile;
#endif

};

}
#endif // __WIN32PC
#endif // AUD_MIXERDEVICEPC_H
