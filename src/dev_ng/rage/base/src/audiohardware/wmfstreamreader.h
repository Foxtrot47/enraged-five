// 
// audiohardware/wmfstreamreader.h 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_WMFSTREAMREADER_H
#define AUD_WMFSTREAMREADER_H
#if __WIN32PC

#include "mediareader.h"

#include "system/mediafoundation.h"
#include "system/xtl.h"

#include <mfidl.h>
#include <Mfreadwrite.h>

namespace rage {

//=====================================================================================================
class audWMFStreamReader : audMediaReaderPlatform
{
public:
	//-------------------------------------------------------------------------------------------------
	audWMFStreamReader();
	~audWMFStreamReader();

	//-------------------------------------------------------------------------------------------------
	bool	MediaOpen(const WCHAR* pszFileName, u32 startOffsetMs);
	bool	MediaClose();

	void Prepare();

	static bool IsAvailable();

	//-------------------------------------------------------------------------------------------------
	s32	FillBuffer(void *sampleBuf, u32 dwBytesToRead);

	//-------------------------------------------------------------------------------------------------
	s32	GetSampleRate();
	s32	GetStreamLengthMs();
	s32	GetStreamPlayTimeMs();
	s32 GetNumChannels() const;

	void	SetCursor(u32 ms);

	virtual const char *GetArtist()
	{
		return m_Artist;
	}

	virtual const char *GetTitle()
	{
		return m_Title;
	}

	static bool InitClass();

private:

	HRESULT ConfigureAudioStream();
	HRESULT GetMetadata(IMFMediaSource *source, IMFMetadata **ppMetadata);

	bool ParseFileMetadata(const WCHAR *fileName);
	bool ParseDataAtom(HANDLE fileHandle, const u32 parentAtomSize, char *destBuffer, const size_t bufSize);

	//-------------------------------------------------------------------------------------------------
	IMFSourceReader*				m_pReader;
	IMFMediaType*					m_pMediaType;

	char m_Artist[128];
	char m_Title[128];
		
	BYTE m_OverflowBuffer[128 * 1024];
	u32 m_OverflowBytes;
	bool m_EOF;
	bool m_IsMono;

	static bool sm_IsAvailable;

};

}
#endif
#endif
