//
// audiohardware/driver.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if RSG_PC && defined(__UNITYBUILD)
#include "system/xtl.h"
#pragma warning(disable:4668)
#pragma warning(disable:4200)
#include <wmsdk.h>
#pragma warning(error:4668)
#pragma warning(disable:4200)
#endif

#include "grcore/stateblock.h"
#include "profile/profiler.h"
#include "system/param.h"

#include "driver.h"
#include "driverdefs.h"
#include "voicemgr.h"
#include "waveslot.h"

#include "submix.h"

#include "device.h"
#include "device_360.h"
#include "device_xboxone.h"
#include "device_asio.h"
#include "device_pc.h"
#include "device_xaudio_pc.h"
#include "device_ps3.h"
#include "device_orbis.h"
#include "midi.h"

#include "audiosynth/synthcoretest.h"

#if __XENON
// needed for IsGameInControlOfMusicPlayback()
#pragma warning(push)
#pragma warning(disable: 4201)
#include <xmp.h>
#pragma warning(pop)
#pragma comment(lib, "xmp.lib")
#endif

namespace rage 
{
#if __WIN32PC
	PARAM(asio, "[RAGE Audio] Use ASIO output, rather than XAudio2");
	PARAM(dsound, "[RAGE Audio] Use DirectSound WAVE_FORMAT_EX output, rather than XAudio2");
	PARAM(midiinputdevice, "[RAGE Audio] MIDI device id to use for input");
#endif
	PARAM(tracecommandpackets, "[RAGE Audio] Trace mixer command packets");
	PARAM(synthcoretest, "[RAGE Audio] Run synthCore asset tests");

	f32 g_MaxHardwareVolumeStep		= 0.067f;
	f32 g_MaxHardwareCutoffFactor		= 1.1f;
	f32 g_OneOverMaxHardwareCutoffFactor = (1.f/g_MaxHardwareCutoffFactor);

	u32 audDriver::sm_FrameCounter = 0;
	audVoiceMgr audDriver::m_VoiceManager;
	audOutputMode audDriver::sm_HardwareOutputMode = AUD_OUTPUT_5_1;
	audOutputMode audDriver::sm_DownmixOutputMode = AUD_OUTPUT_5_1;
	
#if __XENON
	sysMemFixedAllocator *audDriver::sm_XMAAllocator = NULL;
	u32 g_NumXMAHeapAllocations = 0;
#endif

	audMixerDevice *audDriver::sm_Mixer = NULL;
	SYNTH_EDITOR_ONLY(audMidiIn *audDriver::sm_MidiInputDevice = NULL);

	PF_PAGE(AudioDriverPage, "RAGE Audio Driver");
	PF_GROUP(AudioDriver);
	PF_LINK(AudioDriverPage, AudioDriver);
	
	XENON_ONLY(PF_VALUE_INT(NumXMAHeapAllocations, AudioDriver));	
	
	PARAM(audiooutputchannels, "[RAGE Audio] Number of output channels - 2 for stereo, 6 for 5.1");
	
	// For now, mix in 5.1 regardless and enable a stereo downmix if necessary
	u32 audDriver::m_NumOutputChannels = audMixerDevice::kMaxOutputChannels;

	audDriverConfig audDriver::sm_Config;

	sysMemAllocator *audDriver::sm_PhysicalAllocator = NULL, *audDriver::sm_VirtualAllocator = NULL;


#if __BANK

	bool g_TraceCommandPackets = false;
	u32 g_OverrideHPFCutoff = 0U;
	u32 g_OverrideLPFCutoff = 24000U;
	bool g_DisableVoiceFilters = false;
	bool g_DisableSampleRateConversion = false;
	bool g_DisableSubmixEffects = false;
	extern bool g_DebugSubmixJob;
	extern u32 g_SubmixJobCacheBuffers;
	bool g_DisableAllVoiceProcessing = false;
	bool g_DisablePCMSource[AUD_NUM_PCMSOURCES];
	u32 g_DisablePCMSourceMask = 0;
	PS3_ONLY(extern bool g_DebugDelayAudioGPU);
	extern bool g_SPUVirtualVoiceUpdate;
	bool g_MeterSubmixes = false;
	bool g_MeterAllSubmixes = false;
	char g_MeterSubmixFilter[128] = { 0 };
	f32 g_MeterSubmixBGAlpha = 0.0f;
	f32 g_MeterSubmixScaling = 1.f;
	bool g_DrawRmsLevel = false;
	bool g_MeterMaxLevels = false;
	bool g_UsePeakData = true;
	float g_LoudnessMeterWindow = 0.4f;
	bool g_PcmSourceInjectNaN = false;
	void audDriver::AddWidgets(bkBank &bank)
	{
		bank.PushGroup("audHardware");
		bank.AddToggle("Disable Frame Buffer Allocations", &audFrameBufferPool::sm_DisableFrameBufferAllocations);
		PS3_ONLY(bank.AddToggle("DebugDelayGPU", &g_DebugDelayAudioGPU));
		bank.AddToggle("SPUVirtualVoiceUpdate", &g_SPUVirtualVoiceUpdate);
		bank.AddToggle("g_UsePeakData", &g_UsePeakData);
		bank.AddSlider("HardwareVolStep", &g_MaxHardwareVolumeStep, 0.f, 1.f, 0.00001f);
		bank.AddSlider("MaxCutoffFactor", &g_MaxHardwareCutoffFactor, 1.f, 1000.f, 0.001f);
		bank.AddSlider("MinCutoffFactor", &g_OneOverMaxHardwareCutoffFactor, 0.f, 1.f, 0.001f);
		bank.AddSlider("OverrideHPF", &g_OverrideHPFCutoff, 0, 24000, 1);
		bank.AddSlider("OverrideLPF", &g_OverrideLPFCutoff, 0, 24000, 1);
		bank.AddToggle("DisableVoiceFilters", &g_DisableVoiceFilters);
		bank.AddToggle("DisableSampleRateConversion", &g_DisableSampleRateConversion);
		bank.AddToggle("DisableAllVoiceProcessing", &g_DisableAllVoiceProcessing);
		bank.AddToggle("DisableSubmixEffects", &g_DisableSubmixEffects);
		bank.AddToggle("TraceCommandPackets", &g_TraceCommandPackets);
		bank.AddToggle("DebugSubmixJob", &g_DebugSubmixJob);
		bank.AddSlider("SubmixJobCacheBuffers", &g_SubmixJobCacheBuffers, 2, 80, 1);
		bank.AddToggle("MeterSubmixes", &g_MeterSubmixes);
		bank.AddToggle("MeterAllSubmixes", &g_MeterAllSubmixes);
		bank.AddText("Filter Submix by Name", g_MeterSubmixFilter, sizeof(g_MeterSubmixFilter));
		bank.AddSlider("MeterSubmix Background Alpha", &g_MeterSubmixBGAlpha, 0.f, 1.f, 0.01f);
		bank.AddSlider("MeterSubmix Scaling", &g_MeterSubmixScaling, 0.f, 10.f, 0.01f);
		bank.AddToggle("ShowMaxLevels", &g_MeterMaxLevels);
		bank.AddToggle("DrawRmsLevel", &g_DrawRmsLevel);
		bank.AddSlider("LoudnessMeterWindow", &g_LoudnessMeterWindow, 0.1f, 3.f, 0.1f);
		m_VoiceManager.AddWidgets(bank);

		bank.AddToggle("InjectNaN", &g_PcmSourceInjectNaN);

		bank.PushGroup("PCM Sources");
		bank.AddToggle("Disable AUD_PCMSOURCE_WAVEPLAYER", &g_DisablePCMSource[AUD_PCMSOURCE_WAVEPLAYER]);
		bank.AddToggle("Disable AUD_PCMSOURCE_GRAINPLAYER", &g_DisablePCMSource[AUD_PCMSOURCE_GRAINPLAYER]);
		bank.AddToggle("Disable AUD_PCMSOURCE_MODULARSYNTH", &g_DisablePCMSource[AUD_PCMSOURCE_MODULARSYNTH]);
		bank.AddToggle("Disable AUD_PCMSOURCE_SUBMIX", &g_DisablePCMSource[AUD_PCMSOURCE_SUBMIX]);
		bank.AddToggle("Disable AUD_PCMSOURCE_SYNTHCORE", &g_DisablePCMSource[AUD_PCMSOURCE_SYNTHCORE]);
		bank.AddToggle("Disable AUD_PCMSOURCE_EXTERNALSTREAM", &g_DisablePCMSource[AUD_PCMSOURCE_EXTERNALSTREAM]);
		bank.PopGroup();

		bank.PopGroup();
	}

	void audDriver::SetDebugDrawRenderStates()
	{
		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
		grcStateBlock::SetBlendState(grcStateBlock::BS_Normal);
		grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);
	}
#endif


	bool audDriver::InitClass(sysMemAllocator *physicalAllocator, sysMemAllocator *virtualAllocator, void *externallyAllocatedBankHeap, u32 externallyAllocatedBankHeapSize)
	{
		sm_PhysicalAllocator = physicalAllocator;
		sm_VirtualAllocator = virtualAllocator;

		s32 outputChannels;
		if(PARAM_audiooutputchannels.Get(outputChannels))
		{
			Assert(outputChannels > 0 && outputChannels <= (s32)g_MaxOutputChannels);
			audDriver::m_NumOutputChannels = (u32)outputChannels;
		}
		else
		{
			audDriver::m_NumOutputChannels = audMixerDevice::kMaxOutputChannels;
		}

		bool hasSucceeded = false;

#if __SYNTH_EDITOR
		// Initialise MIDI
		sm_MidiInputDevice = rage_new audMidiIn();
		s32 deviceId = 0;
		if(PARAM_midiinputdevice.Get())
		{
			PARAM_midiinputdevice.Get(deviceId);
		}
		if(!sm_MidiInputDevice->Init(deviceId))
		{
			audErrorf("Failed to initialise MIDI Device %d", deviceId);
		}
#endif

#if __DEV
		if(PARAM_tracecommandpackets.Get())
		{
			g_TraceCommandPackets = true;
		}
#endif

#if __BANK
		for(u32 loop = 0; loop < AUD_NUM_PCMSOURCES; loop++)
		{
			g_DisablePCMSource[loop] = false;
		}
#endif

		hasSucceeded = InitMixer();

		if(hasSucceeded)
		{
			audWaveSlot::InitClass(externallyAllocatedBankHeap, externallyAllocatedBankHeapSize);
			m_VoiceManager.Init();

#if __XENON
			const u32 numVoices = m_VoiceManager.GetMaximumNumberOfPhysicalVoices() + audDriver::GetConfig().GetNumGranularDecoders();
			void *xmaMemory = AllocatePhysical(kXMAContextSizeBytes * numVoices, 128);
			sm_XMAAllocator = rage_new sysMemFixedAllocator(xmaMemory, kXMAContextSizeBytes, numVoices);
#endif
		}
		else
		{
			// Returning false from this function will lead to a crash later on since the audio engine won't be initialised, so fail early with
			// a useful warning message to help people work out what's wrong.
			Quitf(ERR_AUD_HARDWARE,"Failed to initialise audio hardware - ensure you have a sound card installed in your PC, and speakers or headphones plugged in.");
		}

#if __BANK
		GetMixer()->RunTests();

		if(PARAM_synthcoretest.Get())
		{
			synthCoreTestManager testMgr;
			testMgr.RunTests();
		}
#endif

		return hasSucceeded;
	}

	void audDriver::ShutdownClass()
	{
		m_VoiceManager.Shutdown();

		ShutdownMixer();

		// shutdown wave slot after hardware since the mixer could be using this data right up until
		// shutdown.
		// but, waveslot frees physical and virtual mem with driver allocator, so this must
		// be placed ahead of the driver shutdown.
		audWaveSlot::ShutdownClass();

#if __SYNTH_EDITOR
		if(sm_MidiInputDevice)
		{
			sm_MidiInputDevice->Shutdown();
			delete sm_MidiInputDevice;
			sm_MidiInputDevice = NULL;
		}
#endif

#if __XENON
		FreePhysical(sm_XMAAllocator->GetHeapBase());
		delete sm_XMAAllocator;
		sm_XMAAllocator = NULL;
#endif
	}

	bool audDriver::InitMixer()
	{
		if(sm_Mixer)
		{
			return true;
		}

#if __WIN32PC
		if(PARAM_asio.Get())
		{
			audDisplayf("Using ASIO mixer output");
			sm_Mixer = rage_new audMixerDeviceAsio();
		}
		else if(PARAM_dsound.Get())
		{
			audDisplayf("Using WAVE_FORMAT_EX mixer output");
			sm_Mixer = rage_new audMixerDevicePc();
		}
		else
		{
			audDisplayf("Using XAudio2 mixer output");
			sm_Mixer = rage_new audMixerDeviceXAudio2();
		}
#elif __PS3
		sm_Mixer = rage_new audMixerDevicePs3();
#elif RSG_ORBIS
		sm_Mixer = rage_new audMixerDeviceOrbis;
#else
		sm_Mixer = rage_new audMixerDeviceXAudio2();
#endif

		Assert(sm_Mixer);
		u32 numVoices = GetConfig().GetNumPhysicalVoices();
		return sm_Mixer->Init(numVoices);
	}

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

	void audDriver::StartAudioCapture( IAudioMixCapturer& capturer, bool startPaused )
	{
		if( sm_Mixer )
		{
			if(startPaused)
			{
				audDriver::GetMixer()->PauseGroup(0, true);
			}
			sm_Mixer->StartAudioCapture( capturer );
		}
	}

	void audDriver::StopAudioCapture()
	{
		if( sm_Mixer )
		{
			sm_Mixer->StopAudioCapture();
			audDriver::GetMixer()->PauseGroup(0, false);
		}
	}

#endif // defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

	void audDriver::ShutdownMixer()
	{
		Assert(sm_Mixer);
		sm_Mixer->Shutdown();
		delete sm_Mixer;
		sm_Mixer = NULL;
	}

	void audDriver::Update(u32 timeInMs)
	{
		m_VoiceManager.Update(timeInMs);
		audWaveSlot::UpdateSlots();

#if __BANK
		g_DisablePCMSourceMask = 0;

		for(u32 sourceType = 0; sourceType < AUD_NUM_PCMSOURCES; sourceType++)
		{
			g_DisablePCMSourceMask |= g_DisablePCMSource[sourceType] << sourceType;
		}
#endif
	}

	bool audDriver::IsGameInControlOfMusicPlayback()
	{
#if __XENON
		// On 360, XMPTitleHasPlaybackControl returns false when we have overridden user music.  Since we might want to allow our
		// own music to play when we have overridden user music (for example fading out in-game music over the start of a cutscene,
		// where the cutscene has baked-in music at some point) we should consider the game in control of music when user music 
		// is overridden
		if(m_VoiceManager.IsUserMusicOverridden())
		{
			return true;
		}

		s32 haveControl;
		if(XMPTitleHasPlaybackControl(&haveControl) == ERROR_SUCCESS)
		{
			return (haveControl==1);
		}
		else
		{
			// if XMP returns an error assume we have control
			return true;
		}
#else
		return true;
#endif
	}
	size_t g_AudioHeapSize = 0;
	void* audDriver::AllocateVirtual(size_t size, size_t align)
	{
		RAGE_TRACK(Audio_AllocateVirtual);
		Assert(sm_VirtualAllocator);
		g_AudioHeapSize += size;
		return sm_VirtualAllocator->RAGE_LOG_ALLOCATE(size, align);
	}

	void audDriver::FreeVirtual(void *ptr)
	{
		Assert(sm_VirtualAllocator);
		Assert(sm_VirtualAllocator->IsValidPointer(ptr));
		g_AudioHeapSize -= GetVirtualSize(ptr);
		sm_VirtualAllocator->Free(ptr);
	}

	size_t audDriver::GetVirtualSize(const void *ptr)
	{
		Assert(sm_VirtualAllocator);
		Assert(sm_VirtualAllocator->IsValidPointer(ptr));
		return sm_VirtualAllocator->GetSize(ptr);
	}

	void* audDriver::AllocatePhysical(size_t size, size_t align)
	{
		RAGE_TRACK(Audio_AllocatePhysical);
		void* memRet = NULL;
#if __XENON
		if(size == kXMAContextSizeBytes)
		{
			Assert(align == 128);
			PF_SET(NumXMAHeapAllocations, ++g_NumXMAHeapAllocations);
			return sm_XMAAllocator->Allocate(size, align, 0);
		}
#endif

		if (sm_PhysicalAllocator)
		{
			g_AudioHeapSize += size;
			memRet = sm_PhysicalAllocator->RAGE_LOG_ALLOCATE(size, align);
		}
		else
		{
			const size_t alignMinusOne = align-1;
			const size_t sizeAligned = (size+alignMinusOne & ~alignMinusOne);
			memRet = sysMemPhysicalAllocate(sizeAligned);
			Assert((size_t)memRet == ((size_t)memRet & ~alignMinusOne));
		}
		return memRet;
	}

	void audDriver::FreePhysical(void *ptr)
	{
#if __XENON
		if(sm_XMAAllocator->IsValidPointer(ptr))
		{
			PF_SET(NumXMAHeapAllocations, --g_NumXMAHeapAllocations);
			sm_XMAAllocator->Free(ptr);
			return;
		}
#endif
		if (sm_PhysicalAllocator)
		{
			Assert(sm_PhysicalAllocator->IsValidPointer(ptr));
			g_AudioHeapSize -= GetPhysicalSize(ptr);
			sm_PhysicalAllocator->Free(ptr);
		}
		else
			sysMemPhysicalFree(ptr);
	}

	size_t audDriver::GetPhysicalSize(const void *ptr)
	{
#if __XENON
		if(sm_XMAAllocator->IsValidPointer(ptr))
		{
			return kXMAContextSizeBytes;
		}
#endif
		Assert(sm_PhysicalAllocator);
		Assert(sm_PhysicalAllocator->IsValidPointer(ptr));
		return sm_PhysicalAllocator->GetSize(ptr);
	}

} // namespace rage
