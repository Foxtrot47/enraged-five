// 
// audiohardware/granularsubmix.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_GRANULARSUBMIX_H
#define AUD_GRANULARSUBMIX_H

#include "decoder.h"
#include "decoder_spu.h"
#include "audiohardware/mixer.h"

namespace rage
{
	// ----------------------------------------------------------------
	// audGranularSubmix - Generates data for a subset of grains (eg. a 
	// synchronised loop) which is then mixed into the main granular mix
	// ----------------------------------------------------------------
	class audGranularSubmix
	{
	// Public Types
	public:
		enum audGrainPlaybackOrder
		{
			PlaybackOrderSequential,
			PlaybackOrderRandom,
			PlaybackOrderWalk,
			PlaybackOrderReverse,
			PlaybackOrderMixNMatch,
			PlaybackOrderMax = PlaybackOrderMixNMatch,
		};

		enum audGrainCrossfadeStyle
		{
			CrossfadeStyleLinear,
			CrossfadeStyleEqualPower,
		};

		enum audGranularSubmixType
		{
			SubmixTypeSynchronisedLoop,
			SubmixTypePureGranular,
		};

#if __PS3
		//enum {kMaxVramLoaders = 2};
#endif

#if __BANK
		struct SubmixHistoryData
		{
			f32 volume;
			f32 pitch;
		};
#endif

		struct audGrainData
		{
			u32 startSample;
			f32 pitch;

#if __XENON
			u32 startSamplesSkipped;
			u16 playBeginInPackets;
			u16 playEndInPackets;
#elif __PS3
			u32 frameOffsetBytesAsGrainStart;
			u32 frameOffsetBytesAsGrainEnd;
#endif

			u16 peakSampleUnsmoothed;
			u16 peakSampleSmoothed;
		};

#if __PS3
		struct audVramLoader
		{
			audDecoderVramHelper vramHelper;
			u32 startOffsetSamples;
			u32 startOffsetBytes;
			u32 dataSize;
			s32 firstValidGrainIndex;
			s32 lastValidGrainIndex;
			s32 synchronisedLoopIndex;
			bool grainChangedSinceLoaded;
			bool initialPacketSent;

			void Reset()
			{
				vramHelper.Reset();
				initialPacketSent = false;
				dataSize = 0;
				startOffsetBytes = 0;
				startOffsetSamples = 0;
				firstValidGrainIndex = -1;
				lastValidGrainIndex = -1;
				synchronisedLoopIndex = -1;
			}
		};
#endif

	// Public Methods
	public:
		audGranularSubmix() {};
		audGranularSubmix(const audWaveRef* waveRef, const audGrainData* grainTable, u32 numGrains);
		virtual ~audGranularSubmix() {};

		void BeginFrame();
		void EndFrame(const f32 currentRateHz, const f32 granuleStepPerGranule PS3_ONLY(, audVramLoader* grainVramLoaders, u32 numGrainVramLoaders, audVramLoader* loopVramLoaders, u32 numLoopVramLoaders));
		u32 GenerateFrame(f32* const destBuffer, f32 volumeScale, const u32 numGrainsGenerated, const f32 currentGranularFraction, const f32 granuleFractionPerSample, bool generateNative);
		void SetValidGrains(const u32* validGrains, u32 numValidGrains, s32 synchronisedLoopIndex);
		void SetEnabled(bool enabled);
		void Shutdown();
		bool StartPhys();
		void StopPhys();
		u32 QueryNumDecodersInUse() const;
		bool IsReadyToStart() const;
		void SetPureRandom(bool pureRandom);
		void SetPureRandomWithStyle(audGrainPlaybackOrder style);
		bool IsAwaitingGrainChange() const;		

#if __PS3
		void SetAllowedDecodesPerFrame(u32 allowedDecodes) { m_AllowedDecodesPerFrame = allowedDecodes; }
		s32 GetVramStartGrainForLoadBackwards(s32 finalGrainIndex, u32 numFetchBuffers = audDecoderVramHelper::kNumFetchBuffers);
		static audVramLoader* GetVramLoaderForGrainIndex(audVramLoader* vramLoaders, u32 numVramLoaders, u32 grainIndex, bool assertOnError = false);
#endif

#if __SPU
		bool SpuPostMix();
#endif

		inline s32 GetSynchronisedLoopIndex() const	{return m_SynchronisedLoopIndex;}

#if !__SPU
		inline bool StarvedLastFrame() const														{return m_StarvedLastFrame;}
		inline bool HasMinMaxGrainLimit() const														{return m_MinGrainLimit >= 0 && m_MaxGrainLimit >= 0;}
		inline s32 GetMinGrainLimit() const															{return m_MinGrainLimit;}
		inline s32 GetMaxGrainLimit() const															{return m_MaxGrainLimit;}
		inline bool IsWalkingForward() const														{return m_WalkDirectionForward;}
		inline void SetPitchShiftValues(f32 pitchShift, f32 pitchStretch)							{m_PitchShift = pitchShift; m_PitchStretch = pitchStretch;}
		inline void SetPlaybackOrder(audGrainPlaybackOrder playbackOrder)							{m_PlaybackOrder = playbackOrder;}
		inline void SetSubmixType(audGranularSubmixType submixType)									{m_SubmixType = submixType;}
		inline void SetSubmixID(u32 submixID)														{m_SubmixIDHash = submixID;}
		inline void SetMinGrainRepeatRate(u32 rate)													{rate < kMaxGrainHistoryValues ? m_MinGrainRepeateRate = rate : m_MinGrainRepeateRate = kMaxGrainHistoryValues;}
		inline void SetMuted(bool muted)															{m_IsMuted = muted;}
		inline void ClearInvalidGrains()															{m_InvalidGrains.Reset();}
		inline void AddInvalidGrain(s32 grain)														{m_InvalidGrains.Push(grain);}
		inline s32 GetLastGrainIndex() const														{return m_GrainHistory[m_GrainHistoryIndex];}
		inline s32 GetPrevGrainIndex(s32 historyIndex) const										{return m_GrainHistory[(m_GrainHistoryIndex + historyIndex) % kMaxGrainHistoryValues];}
		inline void SetPeakNormaliseLoops(bool enabled)												{m_PeakNormaliseLoops = enabled;}
		inline void SetInterGrainCrossfadeStyle(audGrainCrossfadeStyle style)						{m_InterGrainCrossfadeStyle = style;}
		inline void SetGrainSkippingEnabled(bool enabled, u32 numToPlay, u32 numToSkip, f32 vol)	{m_GrainSkippingEnabled = enabled; m_NumGrainsToPlay = numToPlay, m_NumGrainsToSkip = numToSkip; m_SkippedGrainVol = vol;}
		inline const u16* GetPeakData() const														{return m_PeakData;}
		inline u32 GetNumPeakSamples() const														{return m_NumPeakSamples;}
		inline f32 GetGranuleFractionInternal() const												{return m_GranularFractionInternal;}
		inline u32 GetNumGrainsGenerated() const													{return m_NumGrainsGenerated;}
		inline bool IsSkippingCurrentGrain() const													{return m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain;}

		inline bool IsEnabled() const						{return m_IsEnabled;}
		inline bool IsPlayingPhysically() const				{return m_IsPlayingPhysically;}
		inline u32 GetNumValidGrains() const				{return m_NumValidGrains;}
		inline u32 GetValidGrain(u32 index) const			{return m_ValidGrains[index];}
		inline f32 GetPrevVolumeScale() const				{return m_PrevVolumeScale;}
		inline u32 GetSubmixID() const						{return m_SubmixIDHash;}
		inline audGranularSubmixType GetSubmixType() const	{return m_SubmixType;}
		inline bool HaveInitialPacketsBeenGenerated() const {return m_InitialPacketsGenerated;}
		inline static s32 GetNumAllocatedGranularDecoders()	{return s_NumGranularDecodersAllocated;}
		inline const s16* GetWaveData() const				{return m_WaveData;}
		inline u32 GetWaveLengthBytes() const				{return m_WaveLengthBytes;}
		inline s32 GetWaveSlot() const						{return m_WaveSlot;}

		void SetGranularSlidingWindowSize(f32 windowSizeHzFraction, u32 minGrains, u32 maxGrains);
		void SetMinMaxGrainLimit(s32 minGrain, s32 maxGrain);
		u16 ComputeCurrentPeak() const;
		u16 ComputePeakFromSamplePosition(u32 playPositionSamples) const;

		static u32 SamplesToPeakIndex(const u32 samples)
		{
			return samples >> 12;
		}

#if __PS3
		__forceinline static bool QueryAnyVramLoaderDataAvailable(audVramLoader const* vramLoaders, u32 numVramLoaders)				
		{
			for(u32 loop = 0; loop < numVramLoaders; loop++)
			{
				if(vramLoaders[loop].vramHelper.QueryNumBytesAvailable() == ~0u)
				{
					return true;
				}
			}

			return false;
		}

		__forceinline bool IsUsingVramLoader(audVramLoader const* vramLoader)				
		{
			for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
			{
				if(m_ActiveGrains[loop].vramLoader == vramLoader)
				{
					return true;
				}
			}

			return false;
		}
#endif

		__forceinline void ClampSelectedGrain(s32& grainIndex) const
		{
			ClampSelectedGrain(grainIndex, m_NumGrains);
		}

		__forceinline static void ClampSelectedGrain(s32& grainIndex, u32 numGrains)
		{
			if(grainIndex < 0)
			{
				grainIndex = 0;
			}
			else if(grainIndex >= ((s32)numGrains)-1)
			{
				// Final grain marker just designates the end of the last grain, and is therefore not a valid selection
				grainIndex = numGrains-2;	
			}
		}
#endif

#if __BANK && !__SPU
		void StoreHistoryData(f32 currentRateHz);
		inline u32 GetMaxRandomisedGrainBelow() const			{return m_MaxRandomisedGrainBelow;}
		inline u32 GetMaxRandomisedGrainAbove() const			{return m_MaxRandomisedGrainAbove;}
		inline s32 GetLastRandomisedGrainCenter() const			{return m_LastRandomisedGrainCenter;}
		inline void SetToneGeneratorEnabled(bool enabled)		{m_ToneGeneratorEnabled = enabled;}
#endif

		static s32 CalculateGrainIndexFromPitch(f32 currentHertz, const audGrainData* grainTable, const u32 numGrains, const f32 pitchShift, const f32 pitchStretch);

	// Public Attributes
	public:
#if __PS3
		static const u32 s_VramCalculationByteLimit;
		static u32 s_NumDecodesThisFrame;
		static u32 s_NumActiveSubmixes;
		static u32 s_NumSubixesStartedThisFrame;
		static u32 s_ScratchDecoderIndex;
#endif

		static const u32 s_MixBufferLength;
		static const u32 s_SampleGathererBufferLength;
		static s32 s_NumGranularDecodersAllocated;
		static bool s_GranularMidLoopRandomisationEnabled;
		BANK_ONLY(static bool s_ForceGranularSubmixStarvation;)
	
	// Private Methods
	private:
		u32 SelectNewGrain(audGrainPlaybackOrder playbackOrder, const u32* validGrains, const u32 numValidGrains);
		bool WasGrainRecentlyUsed(u32 grainIndex) const;
		void MoveToNextGrain();
		void CalculateNewGrains(const f32 currentRateHz, const f32 granuleStepPerGranule PS3_ONLY(, audVramLoader* grainVramLoaders, u32 numGrainVramLoaders, audVramLoader* loopVramLoaders, u32 numLoopVramLoaders));
		void SendDecoderPackets();
		void CalculateSlidingWindowLimits(u32 selectedGrain, const f32 granuleStepPerGranule, u32& maxGrainBelow, u32& maxGrainAbove) const;
		u32 GenerateFrameFromCompressedData(f32* const destBuffer, const f32 volumeScale, const u32 numGrainsGenerated, const f32 currentGranularFraction, const f32 granuleFractionPerSample);
		u32 GenerateFrameFromPCMData(f32* const destBuffer, const f32 volumeScale, const u32 numGrainsGenerated, const f32 currentGranularFraction, const f32 granuleFractionPerSample, const bool generateNative);
		audPacket CreateDecoderPacket(u32 activeGrainIndex);
		void ScaleVolume(f32 *output, u32 numSamples, f32 volumeScale);
		void UpdateGrainFlush(bool withinCrossfadeSection);
		u32 AdvanceActiveGrainReadPtr(const void* decoderPtr, const u32 grainIndex, const f32 currentGranularFraction, const f32 granuleFractionPerSample, const u32 samplesGenerated);

#if __SPU
		u32 UpdateSpuDecoder(audDecoderSpu* decoder, s32 samplesRequired = -1, s32 maxUpdates =-1);
#endif

#if !__SPU
		__forceinline f32 ReadSampleFromGranularFraction(const u32 grainIndex, const f32 granularFraction) const
		{
			const f32 sampleIndex = m_ActiveGrains[grainIndex].startSample + (m_ActiveGrains[grainIndex].grainLength * granularFraction);
			const s32 floorSample = (s32)rage::Floorf(sampleIndex);
			const s16 prevSample16 = m_WaveData[floorSample];
			const s16 nextSample16 = m_WaveData[floorSample + 1];
			return (prevSample16 + ((nextSample16 - prevSample16) * (sampleIndex - floorSample)));
		}

		__forceinline f32 ReadSampleFromIndex(const u32 grainIndex, const u32 index) const
		{
			return (f32)m_WaveData[m_ActiveGrains[grainIndex].startSample + index];
		}
#endif

	// Private Types
	private:
		struct audGrainState
		{
			PS3_ONLY(audDecoderSpu::audDecoderSpuPacket packet;)
			audDecoder* decoder;
			u32 grainIndex;
			u32 startSample;
			u32 grainLength;
			u32 samplesDecoded;
			f32 peakSampleMultiplier;
			u32 initialFadeSamplesRemaining;
			PS3_ONLY(audVramLoader* vramLoader;)
			bool packetSent;
			bool awaitingGrainChange;
			bool awaitingFlush;
			bool isFirstFlush;
			bool skipThisGrain;
			PS3_ONLY(bool spuPacketSent;)
			PS3_ONLY(bool canDecodeMoreData;)
		};
	
		enum {kMaxInvalidGrains = 30};
		enum {kMaxGrainHistoryValues = 10};
		enum {kMaxStoredGrainStates = 3};

	// Private attributes
	private:
		audGrainState m_ActiveGrains[kMaxStoredGrainStates];
		audGrainCrossfadeStyle m_InterGrainCrossfadeStyle;
		ATTR_UNUSED s16* m_CrossfadeSampleBufferPtr;		// should be bank-only but that throws off alignment
		f32 m_PrevVolumeScale;
		ATTR_UNUSED u32 m_AllowedDecodesPerFrame;			// should be bank-only but that throws off alignment
		f32 m_GranularFractionInternal;
		u32 m_NumGrainsGenerated;
		u32 m_NumGrainsToSkip;
		u32 m_NumGrainsToPlay;
		u32 m_WavenameHash;
		f32 m_SkippedGrainVol;
		u8 m_NumStoredGrainStates;
		s8 m_SynchronisedLoopIndex;
		u8 m_ActiveGrainIndex; 
		u8 m_PrevGrainIndex;
		bool m_IsEnabled;
		bool m_IsMuted;
		bool m_InitialPacketsGenerated;
		bool m_IsPlayingPhysically;
		bool m_FirstFrame;
		bool m_StarvedLastFrame;
		bool m_GrainSkippingEnabled;
		bool m_ToneGeneratorEnabled;

#if !__SPU
	// Private attributes
	private:
		const s16* m_WaveData  ;
		const audGrainData* m_GrainTable;
		const u32* m_ValidGrains;
		const audWaveFormat* m_WaveFormat;
		const u16 *m_PeakData;
		BANK_ONLY(const char* m_BankName;)

#if __PS3
		ALIGNAS(16) s16 m_CrossfadeSampleBuffer[512] ;
		const u16* m_SeekTable;
		u32 m_SeekTableSize;
#endif

		s32 m_MaxGrainLimit;
		s32 m_MinGrainLimit;
		s32 m_WaveSlot;
		u32 m_SubmixIDHash;
		u32 m_NumGrains; 
		s32 m_NumSamples;
		u32 m_WaveLengthBytes;
		u32 m_NumValidGrains;
		s32 m_SelectedValidGrainIndex;
		s32 m_NumMixMatchGrainsToPlay;
		f32 m_PitchShift;
		f32 m_PitchStretch;
		f32 m_SlidingGrainWindowSizeHz;
		u32 m_SlidingGrainWindowSizeGrainsMin;
		u32 m_SlidingGrainWindowSizeGrainsMax;
		u32 m_MaxRandomisedGrainBelow;
		u32 m_MaxRandomisedGrainAbove;
		s32 m_LastRandomisedGrainCenter;
		u32 m_MinGrainRepeateRate;
		s32 m_BaseGrainIndexLastTime;
		u32 m_GrainHistoryIndex;
		u32 m_NumGrainsSinceWalkReRandomise;
		u32 m_NumPeakSamples;
		u16 m_FirstPeakLevelSample;
		bool m_WasRandomisingLastTime;
		bool m_WalkDirectionForward;
		bool m_DecodersRequired;
		bool m_PeakNormaliseLoops;
		bool m_PureRandomPlayback;

		audGrainPlaybackOrder m_PlaybackOrder;
		audGrainPlaybackOrder m_MixNMatchPlaybackOrder;
		audGranularSubmixType m_SubmixType;
		atRangeArray<s32, kMaxGrainHistoryValues> m_GrainHistory;
		atFixedArray<s32, kMaxInvalidGrains> m_InvalidGrains;
#endif
		
	};
}

#endif // AUD_GRANULARSUBMIX_H
