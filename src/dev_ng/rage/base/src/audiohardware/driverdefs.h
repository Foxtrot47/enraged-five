//
// audiohardware/driverdefs.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DRIVER_DEFS_H
#define AUD_DRIVER_DEFS_H


namespace rage {

#define RSG_AUDIO_X64  (RSG_PC || RSG_ORBIS || RSG_DURANGO)


//
// PURPOSE
//	Defines the effective silence volume, in dB.
//
static const f32 g_SilenceVolume				= -100.0f;
//
// PURPOSE
//	Defines the effective silence volume in linear terms from 0.0 to 1.0 (1.0 being equivalent to 0dB.)
//
static const f32 g_SilenceVolumeLin				= 0.00001f;	
//
// PURPOSE
//	Defines the effective silence volume in fcl terms from 0.0 to 1.0 (1.0 being equivalent to 0dB.)
//
static const f32 g_SilenceVolumeFcl				= 0.0032f;	

// PURPOSE
//  Linear volumes are multiplied by this - 2dB in linear terms - at the voice layer, to compensate for 2dB being subtracted by the EnvSound.
static const f32 g_HeadroomOffsetCompensationLin = 1.2589f;
//
// PURPOSE
//	Defines the step size that should be used to smooth any reduction in the ducking effect on voice volumes caused by
//	loud sounds finishing, in dB.
//
static const f32 g_VolumeDuckingStepSize		= 1.2f;
//
// PURPOSE
//	Defines the maximum linear volume step per audio frame.
//
extern f32 g_MaxHardwareVolumeStep;

// PURPOSE
//	Defines the max cutoff change factor per frame
extern f32 g_MaxHardwareCutoffFactor;
extern f32 g_OneOverMaxHardwareCutoffFactor;
//
// PURPOSE
//	Defines the hardware volume scaling factor applied during speaker mapping.
//
static const f32 g_HardwareOutputScalingFactor	= 0.6f;
//
// PURPOSE
//	Defines the maximum number of output channels, used by the speaker mapping and software mixer code.
// NOTES
//	For now we're limiting to 5.1 output.
//
static const u32 g_MaxOutputChannels			= 6; 
//
// PURPOSE
//	Defines the maximum number of virtualisation groups active at any one time
//
static const u32 g_MaxVirtualisationGroups		= 256;
//
// PURPOSE
//	Defines the maximum number of voices that the output of a voice can be routed to.
//
static const u8 g_MaxVoiceRouteDestinations	= 5;
//
// PURPOSE
//	Defines the maximum number of effects that can be concatenated in a single effect chain.
//
static const u32 g_MaxEffectChainLength			= 8;
//
// PURPOSE
//	Defines the number of source reverbs supported.
//
static const u32 g_NumSourceReverbs				= 3;
// PURPOSE
//	Defines the number of channels in a source reverb
static const u32 g_NumSourceReverbChannels		= 4;

enum 
{	
	kVoiceFilterLPFMinCutoff	= 100,
#if RSG_CPU_X64 //23.9kHz cutoff seems to kill the performance of the biquad on PC.
	kVoiceFilterLPFMaxCutoff	= 20000,
#else
	kVoiceFilterLPFMaxCutoff	= 23900,
#endif

	kVoiceFilterHPFMinCutoff = 0,
	kVoiceFilterHPFMaxCutoff = 23900,
};

//	Inverse of the LPF source filter cutoff max
const f32 g_InvSourceLPFilterCutoff = 1.0f / (f32)kVoiceFilterLPFMaxCutoff;
const f32 g_InvSourceHPFilterCutoff = 1.0f / (f32)kVoiceFilterHPFMaxCutoff;

//	Negative number of octaves below the high filter cutoff at 20 Hz
#if RSG_CPU_X64
const f32 g_OctavesBelowFilterMaxAt100Hz = -7.643856846620870436109582884682f;
const f32 g_OctavesBelowFilterMaxAt1Hz = -14.2877122846620870436109582884682f;
const f32 g_audOctavesAt23900 = -11.51336667f;	// @OCC TODO Need to get the correct value here
#else // __WIN32PC
const f32 g_OctavesBelowFilterMaxAt100Hz = -7.900867902868110906212638959747f;
const f32 g_OctavesBelowFilterMaxAt1Hz = -14.544723902868110906212638959747f;
const f32 g_audOctavesAt23900 = -11.51336667f;	// @OCC TODO Need to get the correct value here
#endif // __WIN32PC

const f32 g_audInvOctavesAt23900 = 1.f / g_audOctavesAt23900;

enum audOutputMode
{
	AUD_OUTPUT_MONO = 0,
	AUD_OUTPUT_STEREO,
	AUD_OUTPUT_PROLOGIC,
	AUD_OUTPUT_5_1,
};

enum audFrontSpeakerPos
{
	AUD_FRONT_SPEAKER_WIDE = 0,
	AUD_FRONT_SPEAKER_MEDIUM,
	AUD_FRONT_SPEAKER_CENTER,
	AUD_FRONT_SPEAKER_NUM
};

enum audRearSpeakerPos
{
	AUD_REAR_SPEAKER_REAR = 0,
	AUD_REAR_SPEAKER_MEDIUM,
	AUD_REAR_SPEAKER_SIDE,
	AUD_REAR_SPEAKER_NUM
};

//
// PURPOSE
// Defines the speaker order

enum audRageSpeakerIds
{
	RAGE_SPEAKER_ID_FRONT_LEFT = 0,
	RAGE_SPEAKER_ID_FRONT_RIGHT,
	RAGE_SPEAKER_ID_BACK_LEFT,
	RAGE_SPEAKER_ID_BACK_RIGHT,
	RAGE_SPEAKER_ID_FRONT_CENTER,
	RAGE_SPEAKER_ID_LOW_FREQUENCY,
	RAGE_SPEAKER_ID_FRONT_LEFT_OF_CENTER,
	RAGE_SPEAKER_ID_FRONT_RIGHT_OF_CENTER,
	NUM_RAGE_SPEAKER_IDS
};

//
// PURPOSE
//	Defines the bitmasks for individual speakers that should be used to create a channel mask.
//
enum audRageSpeakerMasks
{
	RAGE_SPEAKER_MASK_MONO =					(1 << RAGE_SPEAKER_ID_FRONT_LEFT),
	RAGE_SPEAKER_MASK_FRONT_LEFT =				(1 << RAGE_SPEAKER_ID_FRONT_LEFT),	
	RAGE_SPEAKER_MASK_FRONT_RIGHT =				(1 << RAGE_SPEAKER_ID_FRONT_RIGHT),
	RAGE_SPEAKER_MASK_FRONT_CENTER =			(1 << RAGE_SPEAKER_ID_FRONT_CENTER),
	RAGE_SPEAKER_MASK_LOW_FREQUENCY =			(1 << RAGE_SPEAKER_ID_LOW_FREQUENCY),
	RAGE_SPEAKER_MASK_BACK_LEFT =				(1 << RAGE_SPEAKER_ID_BACK_LEFT),
	RAGE_SPEAKER_MASK_BACK_RIGHT =				(1 << RAGE_SPEAKER_ID_BACK_RIGHT),
	RAGE_SPEAKER_MASK_FRONT_LEFT_OF_CENTER =	(1 << RAGE_SPEAKER_ID_FRONT_LEFT_OF_CENTER),
	RAGE_SPEAKER_MASK_FRONT_RIGHT_OF_CENTER =	(1 << RAGE_SPEAKER_ID_FRONT_RIGHT_OF_CENTER),
};

}	// namespace rage

#endif // AUD_DRIVER_DEFS_H
