// 
// audiohardware/dspeffect.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "dspeffect.h"
#include "mixing_vmath.inl"

#include "audioeffecttypes/biquadfiltereffect.h"
#include "audioeffecttypes/compressoreffect.h"
#include "audioeffecttypes/delayeffect.h"
#include "audioeffecttypes/ereffect.h"
#include "audioeffecttypes/variabledelayeffect.h"
#include "audioeffecttypes/r360limitereffect.h"
#include "audioeffecttypes/reverbeffect.h"
#include "audioeffecttypes/reverbeffect4.h"
#include "audioeffecttypes/reverbeffect_prog.h"
#include "audioeffecttypes/underwatereffect.h"
#include "audioeffecttypes/waveshapereffect.h"

#include "audiosynth/synthcore.h"
#include "system/performancetimer.h"


namespace rage {

#if AUD_TIME_DSP
	atRangeArray<float, AUD_NUM_DSPEFFECTS> audDspEffect::sm_ProcessTime;
#endif // AUD_TIME_DSP

#if __SPU
	#define InvokeDSP(type) ((type*)this)->Process(buffer, scratchSpace, scratchSpaceSize)
	void audDspEffect::Process(audDspEffectBuffer &buffer, u8 *scratchSpace, const u32 scratchSpaceSize)
	{
		switch(m_TypeId)
		{
		case AUD_DSPEFFECT_BIQUAD_FILTER:
			InvokeDSP(audBiquadFilterEffect);
			break;		
		case AUD_DSPEFFECT_DELAY:
			InvokeDSP(audDelayEffect);
			break;
		case AUD_DSPEFFECT_DELAY4:
			InvokeDSP(audDelayEffect4);
			break;

		case AUD_DSPEFFECT_VARIABLEDELAY:
			InvokeDSP(audVariableDelayEffect);
			break;

#if !__SPU || !defined(AUD_MULTICHANNEL_DSP_SPU)
		case AUD_DSPEFFECT_WAVESHAPER:
			InvokeDSP(audWaveshaperEffect);
			break;
#endif

#if !__SPU || defined(AUD_MULTICHANNEL_DSP_SPU)
		case AUD_DSPEFFECT_R360LIMITER:
#if !__SPU
			InvokeDSP(audR360LimiterEffect);
#else
			// Code for this effect is disabled to free up scratch memory on SPU. If you need to re-enable it, check that doing so doesn't stop
			// the reverbs from working in release configurations (as we run with a slightly reduced reverb in dev builds). If this does occur,
			// you should get a handy message warning you that the reverbs are disabled due to a lack of scratch space.
			audAssertf(false, "audR360LimiterEffect currently disabled on SPU");
#endif
			break;
		case AUD_DSPEFFECT_REVERB:
			InvokeDSP(audReverbEffect);
			break;
		case AUD_DSPEFFECT_REVERB4:
			InvokeDSP(audReverbEffect4);
			break;
#if 0
		case AUD_DSPEFFECT_REVERB_PROG:
			InvokeDSP(audReverbEffectProgenitor);
			break;
#endif
		case AUD_DSPEFFECT_COMPRESSOR:
#if !__SPU
			InvokeDSP(audCompressorEffect);
#else
			// Code for this effect is disabled to free up scratch memory on SPU. If you need to re-enable it, check that doing so doesn't stop
			// the reverbs from working in release configurations (as we run with a slightly reduced reverb in dev builds). If this does occur,
			// you should get a handy message warning you that the reverbs are disabled due to a lack of scratch space.
			audAssertf(false, "audCompressorEffect currently disabled on SPU");
#endif
			break;
#endif // !SPU || defined(AUD_MULTICHANNEL_DSP_SPU)

#if !__SPU || defined(AUD_SYNTHCORE_DSP_SPU)
		case AUD_DSPEFFECT_SYNTHCORE:
			InvokeDSP(synthCoreDspEffect);
			break;
#endif

		case AUD_DSPEFFECT_EARLY_REFLECTIONS:
#if defined(AUD_MULTICHANNEL_DSP_SPU)
			InvokeDSP(audEarlyReflectionEffect);
#endif
			break;
		case AUD_DSPEFFECT_UNDERWATER:
#if defined(AUD_MULTICHANNEL_DSP_SPU)
			InvokeDSP(audUnderwaterEffect);
#endif
			break;
		default:
			Assert(0);
			return;
		}
	}

	
#endif

	u32 audDspEffect::GetSize() const
	{
		switch(m_TypeId)
		{
		case AUD_DSPEFFECT_BIQUAD_FILTER:
			return sizeof(audBiquadFilterEffect);
		case AUD_DSPEFFECT_COMPRESSOR:
			return sizeof(audCompressorEffect);
		case AUD_DSPEFFECT_DELAY:
			return sizeof(audDelayEffect);
		case AUD_DSPEFFECT_DELAY4:
			return sizeof(audDelayEffect4);
		case AUD_DSPEFFECT_VARIABLEDELAY:
			return sizeof(audVariableDelayEffect);
		case AUD_DSPEFFECT_R360LIMITER:
			return sizeof(audR360LimiterEffect);
		case AUD_DSPEFFECT_REVERB:
			return sizeof(audReverbEffect);
		case AUD_DSPEFFECT_REVERB4:
			return sizeof(audReverbEffect4);
		case AUD_DSPEFFECT_WAVESHAPER:
			return sizeof(audWaveshaperEffect);
		case AUD_DSPEFFECT_SYNTHCORE:
			return sizeof(synthCoreDspEffect);
#if 0
		case AUD_DSPEFFECT_REVERB_PROG:
			return sizeof(audReverbEffectProgenitor);
#endif
		case AUD_DSPEFFECT_EARLY_REFLECTIONS:
			return sizeof(audEarlyReflectionEffect);
		case AUD_DSPEFFECT_UNDERWATER:
			return sizeof(audUnderwaterEffect);
		default:
			Assert(0);
			return sizeof(audDspEffect);
		}
	}

#if AUD_SUPPORT_METERING

	void audDspEffect::PreProcess(audDspEffectBuffer &buffer)
	{
		float peakLevel = 0.f;
		float meanLevel = 0.f;

		// Grab the highest per-channel input mean/peak
		for(u32 channel = 0; channel < buffer.NumChannels; channel++)
		{
			if (buffer.ChannelBufferIds[channel] < audFrameBufferPool::InvalidId)
			{
				peakLevel = Max(peakLevel, ComputePeakLevel<kMixBufNumSamples>(buffer.ChannelBuffers[channel]));

				float thisMean = ComputeMeanLevel<kMixBufNumSamples>(buffer.ChannelBuffers[channel]);
				if (Abs(thisMean) > Abs(meanLevel))
				{
					meanLevel = thisMean;
				}
			}
		}

		m_InputLevel = peakLevel;
		m_InputMean = meanLevel;
	}
#endif // AUD_SUPPORT_METERING
} // namespace rage
