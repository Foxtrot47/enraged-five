// 
// audiosynth/connection.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_CONNECTION_H
#define AUD_CONNECTION_H

#include "atl/bitset.h"
#include "atl/array.h"
#include "system/criticalsection.h"
#include "vectormath/vectormath.h"

namespace rage
{
#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324)
#endif
class audMixerVoice;
class audMixerSubmix;
class audMixerConnection
{
	friend class audMixerConnectionPool;
public:

	enum {kNumVolumesInMatrix = 8};

#if !__SPU
	// PURPOSE
	//	Initialise connection pools
	static bool InitClass();

	// PURPOSE
	//	Frees memory associated with pools
	static void ShutdownClass();

	// PURPOSE
	//	Factory functions to allocate instantiate connection objects
	static audMixerConnection *Connect(audMixerVoice *source, audMixerSubmix *dest, const bool invertPhase);
	static audMixerConnection *Connect(audMixerSubmix *source, audMixerSubmix *dest, const bool allocateVolumeMatrices);
	static void DestroyConnection(audMixerConnection *connection);
	static void DestroyConnection(const u32 connectionId);
#endif // !__SPU

	static u32 GetConnectionId(const audMixerConnection *connection);
	static audMixerConnection *GetConnectionFromId(const u32 id);
	static Vec::Vector_4V *GetVolumeMatrixFromId(const s32 index);
	
	u32 GetNumOutputs() const
	{
		return m_Connection.NumOutputChannels + 1;
	}

	bool IsVoiceInput() const
	{
		return (m_Connection.IsVoiceInput != 0);
	}

	bool ShouldInvertPhase() const
	{
		return (m_Connection.InvertPhase != 0);
	}

	void SetInvertPhase(const bool invertPhase)
	{
		m_Connection.InvertPhase = invertPhase ? 1 : 0;
	}

	u32 GetInputBufferId() const
	{
		Assert(IsVoiceInput());
		return m_Connection.InputId;		
	}

	s32 GetInput() const
	{
		Assert(!IsVoiceInput());
		return static_cast<s32>(m_Connection.InputId);
	}

	void SetInputBufferId(const u32 bufferId)
	{
		m_Connection.InputId = bufferId;
		Assert(m_Connection.InputId == bufferId);
	}

	s32 GetOutput() const
	{
		return m_Connection.OutputId;
	}

	// PURPOSE
	//	Sets old volumes = current volumes
	void UpdatePreviousVolumeMatrix()
	{
		if(HasVolumeMatrix())
		{
			if(!HasTwoVolumeMatrices())
			{
				// one matrix is big enough for prev and current
				Vec::Vector_4V *mat = GetVolumeMatrixFromId(m_VolumeMatrix1);
				// copy second half over first half
				// X = cur_0, cur_1
				// Y = cur_2, cur_3
				// Z = prev_0,prev_1
				// W = prev_2,prev_3
				
				// Copy current into Z,W
				*mat = Vec::V4Permute<Vec::X,Vec::Y,Vec::X,Vec::Y>(*mat);
			}
			else
			{
				// two matrices; matrix2 = old, matrix1 = new
				Vec::Vector_4V *oldMat = (Vec::Vector_4V*)GetVolumeMatrixFromId(m_VolumeMatrix2);
				Vec::Vector_4V *newMat = (Vec::Vector_4V*)GetVolumeMatrixFromId(m_VolumeMatrix1);
				Assert(oldMat);
				Assert(newMat);
	
				// 8 volumes per vec4
				CompileTimeAssert(kNumVolumesInMatrix == 8);
				*oldMat = *newMat;
			}
		}
	}

	void GetVolumeMatrices(Vec::Vector_4V_InOut current, Vec::Vector_4V_InOut prev)
	{
		Assert(HasVolumeMatrix());
		Assert(!HasTwoVolumeMatrices());
		
		const Vec::Vector_4V mat = *GetVolumeMatrixFromId(m_VolumeMatrix1);
		UnpackVolumeMatrix(mat, current, prev);		
	}

	void GetVolumeMatrices(Vec::Vector_4V_InOut current0, Vec::Vector_4V_InOut current1, 
		Vec::Vector_4V_InOut prev0, Vec::Vector_4V_InOut prev1)
	{
		Assert(HasVolumeMatrix());
		Assert(HasTwoVolumeMatrices());

		const Vec::Vector_4V matCurrent = *GetVolumeMatrixFromId(m_VolumeMatrix1);
		UnpackVolumeMatrix(matCurrent, current0, current1);

		const Vec::Vector_4V matPrev = *GetVolumeMatrixFromId(m_VolumeMatrix2);
		UnpackVolumeMatrix(matPrev, prev0, prev1);		
	}

	void SetCurrentVolumeMatrix(Vec::Vector_4V_In current)
	{
		Assert(HasVolumeMatrix());
		Assert(!HasTwoVolumeMatrices());

		const Vec::Vector_4V currentPacked = Vec::V4PackSignedIntToSignedShort(Vec::V4FloatToIntRaw<15>(current), Vec::V4VConstant(V_ZERO));
		Vec::Vector_4V *mat = GetVolumeMatrixFromId(m_VolumeMatrix1);
		// preserve 'prev' which is stored in Z,W components
		*mat = Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::Z2,Vec::W2>(currentPacked, *mat);
	}

	void SetCurrentVolumeMatrix(Vec::Vector_4V_In current0, Vec::Vector_4V_In current1)
	{
		Assert(HasVolumeMatrix());
		Assert(HasTwoVolumeMatrices());

		const Vec::Vector_4V currentPacked = Vec::V4PackSignedIntToSignedShort(Vec::V4FloatToIntRaw<15>(current0), Vec::V4FloatToIntRaw<15>(current1));
		Vec::Vector_4V *mat = GetVolumeMatrixFromId(m_VolumeMatrix1);
		
		*mat = currentPacked;
	}

	void SetCurrentVolumeMatrixPacked(Vec::Vector_4V_In currentPacked)
	{
		Assert(HasVolumeMatrix());
		Vec::Vector_4V *mat = GetVolumeMatrixFromId(m_VolumeMatrix1);
		if(HasTwoVolumeMatrices())
		{
			*mat = currentPacked;
		}
		else
		{			
			// single matrix - preserve 'prev' which is stored in Z,W components
			*mat = Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::Z2,Vec::W2>(currentPacked, *mat);
		}
	}

	void SilenceVolumeMatrix()
	{
		Assert(HasVolumeMatrix());
		Vec::Vector_4V *mat = GetVolumeMatrixFromId(m_VolumeMatrix1);
		*mat = Vec::V4VConstant(V_ZERO);

		if(HasTwoVolumeMatrices())
		{
			mat = GetVolumeMatrixFromId(m_VolumeMatrix2);
			*mat = Vec::V4VConstant(V_ZERO);
		}
	}

	static Vec::Vector_4V_Out PackVolumeMatrix(const f32 *volumes)
	{
		Assert(((size_t)volumes & 15) == 0);

		const Vec::Vector_4V current0 = *((Vec::Vector_4V*)volumes);
		const Vec::Vector_4V current1 = *(((Vec::Vector_4V*)volumes) + 1);
		const Vec::Vector_4V currentPacked = Vec::V4PackSignedIntToSignedShort(	Vec::V4FloatToIntRaw<15>(current0), 
																				Vec::V4FloatToIntRaw<15>(current1));

		return currentPacked;
	}

	static void UnpackVolumeMatrix(const Vec::Vector_4V_In packedVolumes, Vec::Vector_4V_InOut volumes0, Vec::Vector_4V_InOut volumes1)
	{
		const Vec::Vector_4V current1Unpacked = Vec::V4UnpackHighSignedShort(packedVolumes);
		const Vec::Vector_4V current0Unpacked = Vec::V4UnpackLowSignedShort(packedVolumes);

		volumes0 = Vec::V4IntToFloatRaw<15>(current0Unpacked);
		volumes1 = Vec::V4IntToFloatRaw<15>(current1Unpacked);
	}

	bool HasTwoVolumeMatrices() const { return GetNumOutputs() > kNumVolumesInMatrix/2; }
	bool HasVolumeMatrix() const { return m_VolumeMatrix1 != -1; }

	enum {INVALID = 0xffff};
private:

	void Init(const s32 input, const s32 output, const u32 numOutputChannels, const bool isVoiceInput, const bool invertPhase,
		const s32 volumeMatrix1, const s32 volumeMatrix2)
	{
		Assert(numOutputChannels > 0);
		// Store num channels offset by one; 0 is invalid and 8 is our max, so this lets us fix in 3 bits [0,7]
		m_Connection.NumOutputChannels = numOutputChannels - 1;
		// Check for overflow
		Assert(GetNumOutputs() == numOutputChannels);
		m_Connection.IsVoiceInput = isVoiceInput;

		m_Connection.InvertPhase = invertPhase;
		m_Connection.InputId = input;
		Assert((s32)m_Connection.InputId == input);
		m_Connection.OutputId = output;
		Assert((s32)m_Connection.OutputId == output);

		Assign(m_VolumeMatrix1, volumeMatrix1);
		Assign(m_VolumeMatrix2, volumeMatrix2);
	}

	void Reset()
	{
		/*m_Connection.InputId = 0;
		m_Connection.OutputId = 0;
		m_Connection.NumOutputChannels = 0;
		m_Connection.IsVoiceInput = 0;*/
		CompileTimeAssert(sizeof(m_Connection) == 4);
		*((u32*)&m_Connection) = 0;
		m_VolumeMatrix1 = m_VolumeMatrix2 = -1;
	}

	s16 m_VolumeMatrix1;
	s16 m_VolumeMatrix2;

	struct  
	{
		u32 InputId : 16;
		u32 OutputId : 10;
		u32 NumOutputChannels : 3;
		u32 InvertPhase : 1;
		u32 IsVoiceInput : 1;
	}m_Connection;
};

class ALIGNAS(16) audMixerConnectionPool
{
public:
	// Connection pool sizing:
	//	- For internal (submix->submix) connections, we needed ~16 for IV
	//	- Plus a maximum of 96 * kMaxVoiceRoutes for voices (480 with current figures)
	// 512 seems a sensible number for now.
	// Worst case scenario for the number of matrices is 2 * numConnections
	// However, our current usage has a worst case per voice of:
	//	-	2 mono connections (vehicle submixes)
	//	-	3 4-channel connections (source reverbs)
	//	-	1 6-channel connection (listener)
	// which requires 7 volume matrices per voice
	// Submix connections currently require something like 24 matrices
	// So the current estimated requirement is 836 matrices (including room for 20 voice overlap)
	enum {kNumConnections = 768};

	// Only submix->submix connections use the volume pool 
	enum {kNumVolumeMatrices = 130};

#if !__SPU
	bool Init();
	void Shutdown();
	audMixerConnection *AllocateConnection();
	void FreeConnection(audMixerConnection *connection);
	s32 AllocateVolumeMatrix();
	void FreeVolumeMatrix(s32 volumeMatrix);
#endif

	u32 GetConnectionId(const audMixerConnection *connection);
	audMixerConnection *GetConnectionFromId(const u32 id);

	Vec::Vector_4V *GetVolumeMatrixFromId(const s32 index)
	{
		return &m_VolumePool[index];
	}

	u32 GetNumConnectionsAllocated()
	{
		return sysInterlockedRead(&m_NumConnectionsAllocated);
	}

	u32 GetNumVolumesAllocated()
	{
		return sysInterlockedRead(&m_NumVolumesAllocated);
	}

private:

	// We currently allocate one Vector4 for each matrix, which corresponds to 8 volumes when packed as
	// 16 bit values
	CompileTimeAssert(audMixerConnection::kNumVolumesInMatrix == 8);
	ALIGNAS(16) atRangeArray<Vec::Vector_4V, kNumVolumeMatrices> m_VolumePool ;

	atRangeArray<s16, kNumConnections> m_ConnectionFreeListStore;
	atRangeArray<audMixerConnection, kNumConnections> m_ConnectionPool;
	atRangeArray<s16, kNumVolumeMatrices> m_MatrixFreeListStore;

	volatile u32 m_NumConnectionsAllocated;
	volatile u32 m_NumVolumesAllocated;

	s32 m_FirstFreeConnection;
	s32 m_FirstFreeMatrix;

	sysCriticalSectionToken m_VolumePoolLock;
	sysCriticalSectionToken m_ConnectionPoolLock;
};

}// namespace rage
#if __WIN32
#pragma warning(pop)
#endif
#endif // AUD_CONNECTION_H


