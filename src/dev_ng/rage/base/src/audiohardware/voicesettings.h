// 
// audiohardware/voicesettings.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_VOICESETTINGS_H
#define AUD_VOICESETTINGS_H

#include "driverdefs.h"
#include "atl/array.h"
#include "audioengine/compressedvolume.h"
#include "audiohardware/device.h"


#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to alignment
#endif

namespace rage
{
	struct audWaveMetadata;
	class audWaveSlot;
	
// PURPOSE
//	Describes a voice output route destination and gain matrix
struct audVoiceRoute
{
	enum {InvalidSubmixId = (1<<7)-1};
	audVoiceRoute()
	{
		SetSubmixId(InvalidSubmixId);
		invertPhase = false;
	}

	void SetSubmixId(const s32 reqSubmixId)
	{
		if(reqSubmixId == -1)
		{
			submixId = InvalidSubmixId;
		}
		else
		{
			submixId = reqSubmixId;
			// check for overflow
			Assert(submixId == reqSubmixId);
		}
	}
	u8 submixId : 7;
	u8 invertPhase : 1;
};

enum audVoiceState
{
	AUD_VOICE_PLAYING_VIRTUALLY, // voice is tracked virtually, no physical voice
	AUD_VOICE_STARTING_PHYSICALLY, // voice has been told to start physical playback
	AUD_VOICE_PLAYING_PHYSICALLY, // voice is being played physically
	AUD_VOICE_VIRTUALISING, // voice is waiting on physical voice stopping, at which point it will be playing virtually
	AUD_VOICE_STOPPING_PHYSICALLY, // voice was playing physically and has been asked, so is waiting on the phys voice
};

//
// PURPOSE
//	This structure contains all settings passed from environment sound down to the physical voice.
//
struct ALIGNAS(16) audVoiceSettings
{
	// PURPOSE
	//	Defines the maximum number of destination routes for a single voice
	enum {kMaxVoiceRouteDestinations = 5};
	enum {kDefaultVoiceCost = 15};

	audVoiceSettings()	:
	VirtualisationGroupId(0),
	VirtualisationScore(0),
	LPFCutoff(kVoiceFilterLPFMaxCutoff),
	HPFCutoff(0),
	CostEstimate(kDefaultVoiceCost),
	PcmSourceId(-1),
	PcmSourceChannelId(-1),
	PhysicalVoiceId(0xff),
	BucketId(0xff),
	State(AUD_VOICE_PLAYING_VIRTUALLY)
	{
		Flags.ShouldMuteOnUserMusic = false;
		Flags.ShouldPlayPhysically = false;
		Flags.ShouldStartPhysicalPlayback = false;
		Flags.ShouldStop = false;
		Flags.ShouldStopWhenVirtual = false;
		Flags.IsUncancellable = false;
	}

	CompileTimeAssert(sizeof(audVoiceRoute) == 1);
	CompileTimeAssert(g_MaxOutputChannels <= 8);

	atRangeArray<Vec::Vector_4V, kMaxVoiceRouteDestinations> RouteChannelVolumes;
	atRangeArray<audVoiceRoute, kMaxVoiceRouteDestinations> Routes;

	u32 VirtualisationGroupId;
	u32 VirtualisationScore;
		
	u16 LPFCutoff;
	u16 HPFCutoff;
	u16 CostEstimate;
	s16 PcmSourceId;

	struct
	{
		bool ShouldMuteOnUserMusic:1;
		bool ShouldPlayPhysically:1;
		bool ShouldStartPhysicalPlayback:1;
		bool ShouldStop:1;
		bool ShouldStopWhenVirtual:1;
		bool IsUncancellable:1;
	}Flags;

	s8 PcmSourceChannelId;
	u8 PhysicalVoiceId;
	u8 BucketId;
	s8 State;
	
} ;


struct ALIGNAS(16) audVirtualisationJobOutput
{
	u32 physVoiceCost;
	u16 numActiveVirtualVoices;
	u16 numActiveVirtualGroups;
	u16 numActivePlayPhysicalVoices;
	u16 numActiveNonGroupedVoices;
	u16 voicesStartedThisFrame;
	u16 voicesStoppedThisFrame;
	u16 numVoicesAtEnd;

	enum {kMaxVoicesStartedPerFrame = kMaxVoices};
	enum {kMaxVoicesStoppedPerFrame = kMaxVoices};
	atRangeArray<u16, kMaxVoicesStartedPerFrame> startVoiceList;
	atRangeArray<u16, kMaxVoicesStoppedPerFrame> stopVoiceList;

} ;


struct ALIGNAS(16) audVirtualisationJobInput
{
	void *eaFirstBucket;
	void *eaPcmSourceState;
	void *eaBucketLocks;
	u32 bucketSize;
	u32 numBuckets;
};

struct PrioritisedVoice
{
	u32					VirtualisationScore;
	u32					VoiceId;
	s32					Cost;
	audVoiceState		PhysicalState;
	PrioritisedVoice *next;
};

struct VoiceSortBin
{
	PrioritisedVoice *head, *tail;
};

struct VoiceGroup
{
	PrioritisedVoice *head;
	u32 HighestScore;
	s32 TotalCost;
	u32 GroupId;
	u32 numEntries;

	VoiceGroup *next;
};

enum audVoiceCapturePackets
{
	AUD_VOICE_CAPTURE_START = 0,
	AUD_VOICE_CAPTURE_FRAME_START,
	AUD_VOICE_CAPTURE_VIRTUAL_VOICE,
	AUD_VOICE_CAPTURE_PHYSICAL_VOICE,
	AUD_VOICE_CAPTURE_FRAME_END,
	AUD_VOICE_CAPTURE_END,
};
// disable struct alignment
#if !__SPU
#pragma pack(push, r1, 1)
#endif // !__SPU

struct audVoiceCapturePacket
{
	audVoiceCapturePacket(const u8 pHeader, const u8 pSize) : packetHeader(pHeader), packetSize(pSize)
	{

	}
	audVoiceCapturePacket & operator=( const audVoiceCapturePacket & ) {return *this;}
	const u8 packetHeader;
	const u8 packetSize;
};

struct audVoiceCaptureStart : audVoiceCapturePacket
{
	audVoiceCaptureStart() : audVoiceCapturePacket(AUD_VOICE_CAPTURE_START,sizeof(audVoiceCaptureStart))
	{

	}

	u32 numWaveSlots;
	u32 waveSlotTableSize;
	u32 numVirtualVoices;
};

struct audVoiceCaptureEnd : audVoiceCapturePacket
{
	audVoiceCaptureEnd() : audVoiceCapturePacket(AUD_VOICE_CAPTURE_END, sizeof(audVoiceCaptureEnd))
	{}
};

struct audVoiceCaptureFrameStart : audVoiceCapturePacket
{
	audVoiceCaptureFrameStart(const u32 now) : audVoiceCapturePacket(AUD_VOICE_CAPTURE_FRAME_START, sizeof(audVoiceCaptureFrameStart)), timeInMs(now)
	{
	}
	audVoiceCaptureFrameStart & operator=( const audVoiceCaptureFrameStart & ) {return *this;}
	const u32 timeInMs;
};

struct audVoiceCaptureFrameEnd : audVoiceCapturePacket
{
	audVoiceCaptureFrameEnd() : audVoiceCapturePacket(AUD_VOICE_CAPTURE_FRAME_END, sizeof(audVoiceCaptureFrameEnd))
	{}
};

struct audVoiceCaptureVirtualVoice : audVoiceCapturePacket
{
	audVoiceCaptureVirtualVoice() : audVoiceCapturePacket(AUD_VOICE_CAPTURE_VIRTUAL_VOICE, sizeof(audVoiceCaptureVirtualVoice))
	{
	}
	
	u32 virtualisationScore; 
	s32 playtime;
	u32	state;
	u32 assetNameHash;
	u16 hpfCutoff;
	u16 lpfCutoff;
	u16 peakLevel;
	u16 groupId;
	u16 costEstimate;
	u8 slotId;
	u8 bucketId;
	u8 voiceId;
	u8 pcmSourceType;
	u8 pcmSourceChannelId;
	u8 padding;
};

struct audVoiceCapturePhysicalVoice : audVoiceCapturePacket
{
	audVoiceCapturePhysicalVoice() : audVoiceCapturePacket(AUD_VOICE_CAPTURE_PHYSICAL_VOICE, sizeof(audVoiceCapturePhysicalVoice))
	{

	}
	u32 assetNameHash;
	u32 virtualisationScore;
	u16 costEstimate;
	u8 physVoiceId;
	u8 bucketId;
	u8 voiceId;
	u8 slotId;
	u8 pcmSourceType;
	u8 pcmSourceChannelId;
};

#if !__SPU
#pragma pack(pop, r1)
#endif // __SPU
}


#if __WIN32
#pragma warning(pop)
#endif

#endif // AUD_VOICESETTINGS_H
