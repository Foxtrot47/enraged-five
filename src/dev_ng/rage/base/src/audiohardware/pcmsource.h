//
// audiohardware/pcmsource.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_PCMSOURCE_H
#define AUD_PCMSOURCE_H

#include "atl/array.h"
#include "atl/bitset.h"
#include "math/simplemath.h"
#include "system/criticalsection.h"
#include "system/interlocked.h"
#include "system/performancetimer.h"
#include "framebufferpool.h"
#include "mixer.h"
#include "syncsource.h"
#include "waveref.h"
#include "audiodata/simpletypes.h"

#include "pcmsource_interface.h"
#include "lffreelist.h"

#if __SPU
#define AUD_PCMSOURCE_VIRTUAL
#else
#define AUD_PCMSOURCE_VIRTUAL virtual
#endif

#if __BANK
#define AUD_PCMSOURCE_TIMER_BEGINFRAME m_ProcessingTimer = 0.f; audPcmSourceFuncTimer timer(m_ProcessingTimer)
#define AUD_PCMSOURCE_TIMER_SKIPFRAME audPcmSourceFuncTimer timer(m_ProcessingTimer)
#define AUD_PCMSOURCE_TIMER_GENERATEFRAME audPcmSourceFuncTimer timer(m_ProcessingTimer)
#define AUD_PCMSOURCE_TIMER_ENDFRAME audPcmSourceFuncTimer timer(m_ProcessingTimer);
#else
#define AUD_PCMSOURCE_TIMER_BEGINFRAME
#define AUD_PCMSOURCE_TIMER_SKIPFRAME
#define AUD_PCMSOURCE_TIMER_GENERATEFRAME
#define AUD_PCMSOURCE_TIMER_ENDFRAME
#endif

namespace rage
{
	class audPcmSourceFuncTimer
	{
	public:
		audPcmSourceFuncTimer(float &timeStore) : m_TimeStore(timeStore), m_Timer("PcmSourceTimer")
		{
			m_Timer.Start();
		}

		~audPcmSourceFuncTimer()
		{
			m_Timer.Stop();
			m_TimeStore += m_Timer.GetTimeMS();		
		}
	private:
		float &m_TimeStore;
		sysPerformanceTimer m_Timer;
	};

#if __SPU
	extern u32 g_NumPcmSourcePoolSlots;
#endif

	struct audCommandPacketHeader;

	static const u32 g_MaxPcmSourceVariableBlocks = 24;

	class audPcmVariableBlock
	{
	public:
		audPcmVariableBlock() : m_PcmSourceId(-1) {};
		bool Init(const char *variableListName);
		bool Init(const u32 variableListNameHash);

		f32 *FindVariableAddress(const char *name) const;
		f32 *FindVariableAddress(const u32 hash) const;

		f32 *GetVariableAddress(const u32 index) const;
		u32 GetVariableNameHash(const u32 index) const;

		void SetVariable(const u32 hash, const f32 value);

		inline void SetPcmSource(s32 pcmSource)		{ m_PcmSourceId = pcmSource; }
		inline s32 GetPcmSource() const				{ return m_PcmSourceId; }
		inline u32 GetNumVariables() const			{ return m_Metadata? m_Metadata->numVariables : 0; }

		enum { kNumVariablesPerBlock = 8 };
	private:
		const ravesimpletypes::VariableList *m_Metadata;
		float *m_Values;
		s32 m_PcmSourceId;
	};

	// PURPOSE
	//	Abstract interface that describes an object which generates PCM data to be consumed by a mixer voice
	// SEE ALSO
	//	synthSynthesizer, audWavePlayer, etc
	class audPcmSource
	{
	public:
		struct Params
		{
			static const u32 FrequencyScalingFactor = 0x31FF1BC8; // Note: atStringHash('Frequency')
			static const u32 StartOffsetMs = 0xABD35B6;
			static const u32 SyncMasterId = 0x24527498;
			static const u32 SyncSlaveId = 0x9869FEC0;			
			static const u32 PauseGroup = 0xD20B058C;
			static const u32 PlayTimeMixerFrame = 0xCFC9242F;
			static const u32 PlayTimeMixerFrameSubFrameOffset = 0xC877C3FD;
		};

		enum { kMaxPcmSourceChannels = 4};

		audPcmSource(const audPcmSourceTypeId typeId)
			: m_NumPhysicalClients(0)
			, m_VariableBlock(-1)
			, m_PlayTimeMixerFrames(~0u)
			, m_PlayTimeMixerFrameSubFrameOffset(kMixBufNumSamples)
			, m_SyncMasterId(audMixerSyncManager::InvalidId)
			, m_SyncSlaveId(audMixerSyncManager::InvalidId)
			, m_Type(static_cast<s8>(typeId))
			, m_PauseGroup(-1)
		{
			CompileTimeAssert(kMaxPcmSourceChannels == 4);
			m_BufferIds[0] = m_BufferIds[1] = m_BufferIds[2] = m_BufferIds[3] = audFrameBufferPool::InvalidId;
		}

		static void InitClass();
		static void ShutdownClass();

		audPcmVariableBlock * GetVariableBlock()
		{
			if(m_VariableBlock >= 0)
			{
				return &sm_PcmSourceVariableBlocks[m_VariableBlock];
			}

			return NULL;
		}

		static audPcmVariableBlock * GetVariableBlock(const int index)
		{
			if(index >= 0)
			{
				return &sm_PcmSourceVariableBlocks[index];
			}
			return NULL;
		}

		void SetVariable(u32 nameHash, f32 value);
		f32 GetVariableValue(u32 nameHash);

		// PURPOSE
		//	Fills the specified buffer with native endian f32 PCM data
		// NOTES
		//	The entire buffer must be filled - if the generator has run out of content part way
		//	through the buffer then it is responsible for padding with silence
		//	The buffer size is kMixBufNumSamples
		AUD_PCMSOURCE_VIRTUAL void GenerateFrame() 
		{
			AUD_PCMSOURCE_TIMER_GENERATEFRAME;
		}

		// PURPOSE
		//	Skips (plays virtually) a frame worth of audio
		AUD_PCMSOURCE_VIRTUAL void SkipFrame() 
		{
			AUD_PCMSOURCE_TIMER_SKIPFRAME;
		}

		// PURPOSE
		//	Initialises the generator for playback
		AUD_PCMSOURCE_VIRTUAL void Start() {};

		// PURPOSE
		//	Requests that the generator finishes (ie returns false from GenerateFrame() as soon as possible)
		AUD_PCMSOURCE_VIRTUAL void Stop() {};

		// PURPOSE
		//	Informs the generator that it is now playing on a physical voice
		AUD_PCMSOURCE_VIRTUAL void StartPhys(s32 UNUSED_PARAM(channel)) {};

		// PURPOSE
		//	Informs the generator that it is no longer playing on a physical voice
		AUD_PCMSOURCE_VIRTUAL void StopPhys(s32 UNUSED_PARAM(channel)) {};

		// PURPOSE
		//	Called on the PPU before GenerateFrame() executes on the SPU
		AUD_PCMSOURCE_VIRTUAL void BeginFrame() 
		{
			AUD_PCMSOURCE_TIMER_BEGINFRAME;
		}

		// PURPOSE
		//	Called on the PPU after GenerateFrame() has executed on the SPU
		AUD_PCMSOURCE_VIRTUAL void EndFrame() 
		{
			AUD_PCMSOURCE_TIMER_ENDFRAME;
		}

		// PURPOSE
		//	Passes in a parameter from the command buffer
		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 UNUSED_PARAM(paramId), const u32 UNUSED_PARAM(val)){}
		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 UNUSED_PARAM(paramId), const f32 UNUSED_PARAM(val)){}

		// PURPOSE
		//	Passes in a custom packet of data from the command buffer
		AUD_PCMSOURCE_VIRTUAL void HandleCustomCommandPacket(const audPcmSourceCustomCommandPacket* UNUSED_PARAM(packet)){}

		// PURPOSE
		//	Returns the headroom of the generated PCM in decibels
		// NOTES
		//	This will be subtracted from the playback volume to allow compensation for normalized
		//	assets, and used when determining the loudness of this content.
		AUD_PCMSOURCE_VIRTUAL f32 GetHeadroom() const { return 0.f; }

		// PURPOSE
		//	Returns the length of the generated PCM in samples at the native sample rate, 
		//	or -1 for infinite/unknown
		AUD_PCMSOURCE_VIRTUAL s32 GetLengthSamples() const { return 0; }

		// PURPOSE
		//	Returns true if the PCM generator will loop around and continue producing
		//	output when it runs out of samples
		AUD_PCMSOURCE_VIRTUAL bool IsLooping() const { return false; }

		// PURPOSE
		//	Returns the current playback position of the generator, in samples at the native
		//	sample rate.
		AUD_PCMSOURCE_VIRTUAL u32 GetPlayPositionSamples() const { return 0; }

		// PURPOSE
		//	Returns true if this generator has finished producing audio
		AUD_PCMSOURCE_VIRTUAL bool IsFinished() const { return true; }

		// PURPOSE
		//	Returns true if this generator has started producing audio
		AUD_PCMSOURCE_VIRTUAL bool HasStartedPlayback() const { return true; }

#if !__FINAL

		struct AssetInfo
		{
			AssetInfo() : assetNameHash(0U), slotId(0U)
			{

			}

			u32 assetNameHash;
			u32 slotId;
		};
		// PURPOSE
		//	Returns information on the asset for reporting/debug draw purposes
		AUD_PCMSOURCE_VIRTUAL void GetAssetInfo(AssetInfo &UNUSED_PARAM(info)) const { }
#endif

#if __BANK
		float GetProcessingTimer() const { return m_ProcessingTimer; }
#endif
		// PURPOSE
		//	Returns true if this pcm source is waiting on a sync object to trigger
		bool IsWaitingOnSync() const { return m_SyncSlaveId != audMixerSyncManager::InvalidId; }
		// PURPOSE
		//	Returns true if we are set to trigger the same signal that we're waiting on
		bool IsWaitingOnSelfSync() const { return m_SyncSlaveId == m_SyncMasterId && IsWaitingOnSync(); }
		u32 GetSyncSlaveId() const { return m_SyncSlaveId; }
		u32 GetSyncMasterId() const { return m_SyncMasterId; }
		AUD_PCMSOURCE_VIRTUAL bool ProcessSyncSignal(const audMixerSyncSignal &UNUSED_PARAM(signal)) { return true; }
		
		AUD_PCMSOURCE_VIRTUAL void Shutdown() {}

		AUD_PCMSOURCE_VIRTUAL u32 GetCurrentPeakLevel() const { /* default to -2dB */ return kDefaultPeakLevel_u16; }

		audPcmSourceTypeId GetTypeId() const { return (audPcmSourceTypeId)m_Type; }

		u32 GetNumPhysicalClients() const { return m_NumPhysicalClients; }

		const atRangeArray<u16, audPcmSource::kMaxPcmSourceChannels> &GetBufferIds() const { return m_BufferIds; }
		u32 GetBufferId(const s32 channel) const { return m_BufferIds[channel]; }
		bool HasBuffer(const s32 channel) const { return GetBufferId(channel) < audFrameBufferPool::InvalidId; }
		void AllocateBuffers(const s32 num) 
		{ 
			Assert(num <= kMaxPcmSourceChannels);
			for(s32 i = 0; i < num; i++)
			{
				Assert(!HasBuffer(i)); 
				Assign(m_BufferIds[i], g_FrameBufferPool->Allocate());
				Assert(HasBuffer(i));
			}
		}

		void AllocateBuffer(const s32 channel)
		{
			Assert(channel <= kMaxPcmSourceChannels);
			Assert(!HasBuffer(channel)); 
			Assign(m_BufferIds[channel], g_FrameBufferPool->Allocate());
			Assert(HasBuffer(channel));
		}

		void FreeBuffers()
		{ 
			for(s32 i = 0; i < kMaxPcmSourceChannels; i++)
			{
				if(HasBuffer(i))
				{
					g_FrameBufferPool->Free(m_BufferIds[i]); 
					m_BufferIds[i] = audFrameBufferPool::InvalidId;
				}
			}
		}

#if !__SPU
		void UpdateState(PcmSourceState &state);
#endif

	protected:

		virtual ~audPcmSource(){}

		bool AllocateVariableBlock(const char * mapName);
		bool AllocateVariableBlock(u32 mapNameHash);

		void ReleaseVariableBlock();
		static void RemoveVariableBlockRef(int index);
	
		BANK_ONLY(float m_ProcessingTimer);
		
		atRangeArray<u16,kMaxPcmSourceChannels> m_BufferIds;
		u32 m_PlayTimeMixerFrames;
		u16 m_PlayTimeMixerFrameSubFrameOffset;
		u16 m_NumPhysicalClients;
		s16 m_VariableBlock;
		u16 m_SyncSlaveId;
		u16 m_SyncMasterId;
		s8 m_Type;
		s8 m_PauseGroup;

	public:
		static u32* sm_PcmSourceVariableBlockRefs;
		static atRangeArray<audPcmVariableBlock, g_MaxPcmSourceVariableBlocks> sm_PcmSourceVariableBlocks;
		ALIGNAS(16) static atRangeArray<float, g_MaxPcmSourceVariableBlocks*audPcmVariableBlock::kNumVariablesPerBlock> sm_PcmSourceVariableBlockValues ;
	};

#define AUD_LOCK_PCMSOURCEPOOL 0
#if AUD_LOCK_PCMSOURCEPOOL
#define LOCK_PCMSOURCEPOOL_ONLY(x) x
#else
#define LOCK_PCMSOURCEPOOL_ONLY(x)
#endif // AUD_LOCK_PCMSOURCEPOOL

	class audPcmSourcePool
	{
	public:
	
		class Iterator
		{
		public:
			Iterator(audPcmSourcePool *pool, const u32 typeMask)
			{
				m_Pool = pool;
				m_TypeMask = typeMask;
				m_CurrentIndex = 0;
			}

			u32 Next()
			{
				for(; m_CurrentIndex < m_Pool->GetNumSlots(); m_CurrentIndex++)
				{
					if(m_Pool->IsSlotInitialised(m_CurrentIndex) && m_Pool->GetRefCount(m_CurrentIndex) > 0)
					{
						const u32 type = m_Pool->GetSlotTypeId(m_CurrentIndex);
						if(1<<type & m_TypeMask)
						{
							return m_CurrentIndex++;
						}
					}
				}
				return m_CurrentIndex;
			}
		private:
			audPcmSourcePool *m_Pool;
			u32 m_CurrentIndex;
			u32 m_TypeMask;
		};

		audPcmSourcePool()
		{
			m_Storage = NULL;
		}

		~audPcmSourcePool()
		{
			
		}

		bool Init(const u32 slotSize, const u32 numSlots)
		{
			audAssertf(numSlots <= kMaxSlots, "Attempting to initialise pcm source pool with %u slots; max is defined as %u", numSlots, kMaxSlots);
			// Align slot size
			m_SlotSize = ((slotSize + 15) & ~15);
			m_NumSlots = numSlots;

			audDisplayf("PCM source pool slot size %u, %u slots", m_SlotSize, m_NumSlots);

			m_Storage = rage_aligned_new(16) u8[m_SlotSize * m_NumSlots];

#define AUD_PCMSOURCE_SIZE_POW2 0
#if AUD_PCMSOURCE_SIZE_POW2
			// Calculate log2(slotSize) so we can use integer bitshift rather than idiv
			u32 x = m_SlotSize;
			m_Log2SlotSize = 0;
			while (x >>= 1)
			{
				m_Log2SlotSize++;
			}
#endif

			m_NumSlots = numSlots;

			if(!Verifyf(m_Storage, "Failed to allocate PCM generator pool storage"))
			{
				return false;
			}

			// Set up freelist
			m_FreeList.Init();

			sysMemZeroBytes<sizeof(m_RefCounts)>(&m_RefCounts);

			return true;
		}

		void Shutdown()
		{
			delete[] m_Storage;
		}	

		void Free(const u32 slotIndex)
		{
			// Init slot and Free will only be called from the mixer thread, as a result of
			// command buffer processing.
			Assert(IsSlotInitialised(slotIndex));
			Assert(GetRefCount(slotIndex) == 0);
			FastAssert(slotIndex < m_NumSlots);
		
			LOCK_PCMSOURCEPOOL_ONLY(Lock());

			m_TypeMap[slotIndex] = 0xff;
			m_IsInitialised.Clear(slotIndex);

			m_FreeList.Free(slotIndex);

			LOCK_PCMSOURCEPOOL_ONLY(Unlock());
		}

		void InitSlot(const u32 slotIndex, const u32 typeId)
		{	
			// Init slot and Free will only be called from the mixer thread, as a result of
			// command buffer processing.			
			m_IsInitialised.Set(slotIndex);
			Assign(m_TypeMap[slotIndex], typeId);			
		}

		u32 Allocate(const size_t ASSERT_ONLY(requestedSlotSize))
		{
			// Allocate can be called from any thread
			FastAssert(requestedSlotSize <= m_SlotSize);
			LOCK_PCMSOURCEPOOL_ONLY(Lock());

			const u32 slotIndex = m_FreeList.Allocate();

#if __ASSERT
			if(slotIndex != ~0U)
			{
				Assert(sysInterlockedRead(&m_RefCounts[slotIndex]) == 0);
				Assert(!IsSlotInitialised(slotIndex));
			}
#endif
			if(slotIndex != ~0U)
			{
				AddRef(slotIndex);
			}

			LOCK_PCMSOURCEPOOL_ONLY(Unlock());
			return slotIndex;
		}

		void *GetSlotPtr(const u32 slotIndex)
		{
			FastAssert(slotIndex < m_NumSlots);
			return &m_Storage[slotIndex * m_SlotSize];
		}

		u32 GetSlotIndex(const void *ptr)
		{
#if AUD_PCMSOURCE_SIZE_POW2
			// Check that slot size is power of 2
			Assert(CountOnBits(m_SlotSize) == 1);
			const u32 slotIndex = (u32)((((u8*)ptr) - &m_Storage[0]) >> m_Log2SlotSize);
#else
			const u32 slotIndex = (u32)((((u8*)ptr) - &m_Storage[0]) / m_SlotSize);
#endif
			FastAssert(slotIndex < m_NumSlots);
			return slotIndex;
		}

		u32 GetNumSlots() const { return m_NumSlots; }

		inline bool IsSlotInitialised(const u32 slotIndex) const
		{
			//FastAssert(slotIndex < m_NumSlots);
			if(!(slotIndex<m_NumSlots))
			{
				return false;
			}
			return m_IsInitialised.IsSet(slotIndex);
		}

		u32 GetSlotTypeId(const u32 slotIndex) const
		{
			FastAssert(slotIndex < m_NumSlots);
			return m_TypeMap[slotIndex];
		}

		const u8 *GetStoragePtr() const
		{
			return m_Storage;
		}

		u32 GetSlotSize() const { return m_SlotSize; }

		u32 AddRef(const s32 slotId)
		{
			const u32 ret = sysInterlockedIncrement(&m_RefCounts[slotId]);
			sys_lwsync();
			return ret;
		}

		u32 RemoveRef(const s32 slotId)
		{
			return sysInterlockedDecrement(&m_RefCounts[slotId]);
		}

		u32 GetNumAllocated() 
		{ 
			return m_FreeList.GetNumAllocated();
		}


		u32 GetRefCount(const s32 slotId)
		{
			return sysInterlockedRead(&m_RefCounts[slotId]);
		}

#if AUD_LOCK_PCMSOURCEPOOL
		void Lock()
		{
			m_AllocationLockToken.Lock();
		}

		void Unlock()
		{
			m_AllocationLockToken.Unlock();
		}
#endif // AUD_LOCK_PCMSOURCEPOOL

	private:
				
		// TODO: fix this
		enum {kMaxSlots = kMaxPcmSources};
		atFixedBitSet<kMaxSlots> m_IsInitialised;		
		audLFFreeList<kMaxSlots> m_FreeList;
		atRangeArray<u8, kMaxSlots> m_TypeMap;
		atRangeArray<u32, kMaxSlots> m_RefCounts;
		
		LOCK_PCMSOURCEPOOL_ONLY(sysCriticalSectionToken m_AllocationLockToken);

		u8 *m_Storage;

		u32 m_NumSlots;
		u32 m_SlotSize;

#if AUD_PCMSOURCE_SIZE_POW2
		u32 m_Log2SlotSize;
#endif	
		
	};

	class audPcmSourceFactory
	{
	public:
		static u32 GetSlotSize(const audPcmSourceTypeId typeId);
		static u32 GetMaxSize();

#if !__SPU
		static void InitSlot(u32 slotIndex, const audPcmSourceTypeId typeId);
#endif
	};
	
	extern audPcmSourcePool *g_PcmSourcePool;
}

#endif // AUD_PCMSOURCE_H
