// 
// audiohardware/framebuffercache.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOHARDWARE_FRAMEBUFFERCACHE_H 
#define AUDIOHARDWARE_FRAMEBUFFERCACHE_H 

#if __SPU

#include "audiohardware/channel.h"
#include "system/dma.h"
#include "system/memops.h"
#include "system/timer.h"
#include "framebufferpool.h"

namespace rage {
extern bool g_DebugFrameBufferCache;
#define fbcDebugf1(x,...) if(g_DebugFrameBufferCache) { audDisplayf(x, ##__VA_ARGS__); }

	// PURPOSE
	//	Implements cached access to the frame buffer pool from the SPU.
	class audFrameBufferCache
	{
	public:

		// PURPOSE
		//	Initialises the cache
		// PARAMS
		// frameBufferEA - the effective address in shared memory of the frame buffer pool storage
		// numFrameBuffers - the number of frame buffers in the pool
		// cacheStorage - pointer to an area of local store to use as cache storage.  Must be sized (kMixBufNumSamples * sizeof(f32) * numCacheBuffers)
		// numCacheBuffers - the number of cache slots to maintain.
		// startTag - the first DMA tag to use for transfers.  Will use the range [tag, tag+1, tag+2, tag+3]
		// writeBackOnEviction - when evicting a cache slot, write the contents back to main memory
		void Init(const u32 frameBufferEA, const u32 numFrameBuffers, f32 *cacheStorage, const u32 numCacheBuffers, const u32 startTag, const bool writeBackOnEviction = true)
		{
			Assert(frameBufferEA);
			Assert(numCacheBuffers <= kMaxCacheBuffers);
			m_FrameBufferEA = frameBufferEA;
			m_NumFrameBuffers = numFrameBuffers;
			m_CacheStorage = cacheStorage;
			m_NumCacheBuffers = numCacheBuffers;

			m_StartTag = startTag;
			m_WriteBackOnEviction = writeBackOnEviction;

			for(u32 i = 0; i < m_NumCacheBuffers; i++)
			{
				m_CacheEntries[i].state = kFree;
				m_CacheEntries[i].readOnly = true;
			}

			sysMemSet(&m_BufferCacheLookup, 0xff, sizeof(m_BufferCacheLookup));

			m_NumWriteBacks = 0;
			m_NumWrites = 0;
			m_NumFetches = 0;
		}

		void Shrink(const u32 numCacheBuffers)
		{
			for(u32 i = numCacheBuffers; i < m_NumCacheBuffers; i++)
			{
				if(m_CacheEntries[i].state != kFree && !m_CacheEntries[i].readOnly)
				{
					// Preserve contents
					fbcDebugf1("%u (%u): Write-back (shrinking)", i, m_CacheEntries[i].bufferId);
					WriteBuffer(m_CacheEntries[i].bufferId);
					m_NumWriteBacks++;					
				}
			}
			m_NumCacheBuffers = numCacheBuffers;

			WaitForAllSlots();
		}

		const f32 *GetBuffer_ReadOnly(const u32 bufferId)
		{
			return GetBuffer(bufferId, false, false, true);
		}

		f32 *GetBuffer(const u32 bufferId, const bool zeroBuffer = false, const bool prefetch = false, const bool readOnly = false)
		{
			audAssertf(bufferId<m_NumFrameBuffers, "Buffer id %u invalid (%u total)", bufferId, m_NumFrameBuffers);

			if(m_BufferCacheLookup[bufferId] != -1)
			{
				// Already have a slot allocated for this buffer
				const s32 slot = m_BufferCacheLookup[bufferId];
				Assert(m_CacheEntries[slot].bufferId == bufferId);
				Assert(m_CacheEntries[slot].state != kFree);

				if(m_CacheEntries[slot].state == kPrefetching)
				{
					fbcDebugf1("%u (%u): Was prefetch, now allocated", slot, m_CacheEntries[slot].bufferId);
					m_CacheEntries[slot].state = kAllocated;
				}
				m_CacheEntries[slot].lastAccessTime = sysTimer::GetTicks();

				if(!readOnly)
				{
					m_CacheEntries[slot].readOnly = false;
				}
				// wait until any DMAs have been completed
				WaitForSlot(slot);

				if(zeroBuffer)
				{
					sysMemSet(GetCacheSlotPointer(slot), 0, sizeof(f32) * kMixBufNumSamples);
				}
				return GetCacheSlotPointer(slot);
			}

			// Find a free buffer
			u32 freeIndex = m_NumCacheBuffers;
			u32 oldestSlot = 0;
			//u32 oldestReadOnlySlot = ~0U;
			for(u32 i = 0; i < m_NumCacheBuffers; i++)
			{
				if(m_CacheEntries[i].state == kFree)
				{
					freeIndex = i;
					break;
				}
				else 
				{
			/*		if((oldestReadOnlySlot == ~0U || m_CacheEntries[i].lastAccessTime < m_CacheEntries[oldestReadOnlySlot].lastAccessTime) 
						&& m_CacheEntries[i].readOnly)
					{
						oldestReadOnlySlot = i;
					}
					*/
					if(m_CacheEntries[i].lastAccessTime < m_CacheEntries[oldestSlot].lastAccessTime)
					{
						oldestSlot = i;
					}
				}
			}

			if(freeIndex == m_NumCacheBuffers)
			{
				fbcDebugf1("Cache is full; %u", m_NumCacheBuffers);
				// We are out of cache slots - evict the least recently used slot
				/*if(!m_WriteBackOnEviction && oldestReadOnlySlot != ~0U)
				{
					fbcDebugf1("Would write-back %u (%u), instead reusing readonly %u (%u)", oldestSlot, m_CacheEntries[oldestSlot].bufferId, oldestReadOnlySlot, m_CacheEntries[oldestReadOnlySlot].bufferId);
					// if we're forced to recycle a writable slot then we lose data, so choose a readonly slot if at all possible
					oldestSlot = oldestReadOnlySlot;
				}*/
				
				// Preserve contents
				if(!m_CacheEntries[oldestSlot].readOnly)
				{
					if(audVerifyf(m_WriteBackOnEviction, "Out of cache slots (%u); want to evict %u", m_NumCacheBuffers, oldestSlot))
					{
						fbcDebugf1("%u (%u): Write-back (re-use) LAT %u", oldestSlot, m_CacheEntries[oldestSlot].bufferId, m_CacheEntries[oldestSlot].lastAccessTime);
						WriteBuffer(m_CacheEntries[oldestSlot].bufferId);
						m_NumWriteBacks ++;
					}
				}
				Assert((u32)m_BufferCacheLookup[m_CacheEntries[oldestSlot].bufferId] == oldestSlot);
				m_BufferCacheLookup[m_CacheEntries[oldestSlot].bufferId] = -1;
				freeIndex = oldestSlot;
			}
			
			Assign(m_CacheEntries[freeIndex].bufferId, bufferId);
			m_CacheEntries[freeIndex].state = kAllocated;
			m_CacheEntries[freeIndex].lastAccessTime = sysTimer::GetTicks();

			m_CacheEntries[freeIndex].readOnly = readOnly;
			Assign(m_BufferCacheLookup[bufferId], (s32)freeIndex);

			if(zeroBuffer)
			{
				// Don't need to bother DMAing the contents, but we do need to wait in case any PUTs are scheduled on the MFC
				WaitForSlot(freeIndex);
				fbcDebugf1("%u (%u): Newly allocated, zeroing", freeIndex, m_CacheEntries[freeIndex].bufferId);
				sysMemSet(GetCacheSlotPointer(freeIndex), 0, sizeof(f32) * kMixBufNumSamples);
			}
			else
			{
				fbcDebugf1("%u (%u): Newly allocated, grabbing data %s", freeIndex, m_CacheEntries[freeIndex].bufferId, prefetch ? "(prefetch)" : "");
				m_NumFetches++;
				sysDmaGetb(GetCacheSlotPointer(freeIndex), m_FrameBufferEA + kMixBufNumSamples*sizeof(f32)*bufferId, kMixBufNumSamples*sizeof(f32), GetTagForSlot(freeIndex));
				if(prefetch)
				{
					m_CacheEntries[freeIndex].state = kPrefetching;
				}
				else
				{
					WaitForSlot(freeIndex);
				}
			}

			return GetCacheSlotPointer(freeIndex);			
		}

		void ZeroBuffer(const u32 bufferId)
		{
			GetBuffer(bufferId, true);
		}

		void PrefetchBuffer(const u32 bufferId, const bool readOnly = false)
		{
			GetBuffer(bufferId, false, true, readOnly);
		}

		// PURPOSE
		//	Writes the specified buffer back to main memory
		void WriteBuffer(const u32 bufferId, const s32 tag = -1)
		{
			const u32 slotIndex = FindCacheEntry(bufferId);
			FastAssert(slotIndex < m_NumCacheBuffers);
			sysDmaPut(GetCacheSlotPointer(slotIndex), 
				m_FrameBufferEA + kMixBufNumSamples*sizeof(f32)*bufferId, 
				sizeof(f32)*kMixBufNumSamples, 
				tag == -1 ? GetTagForSlot(slotIndex) : tag);
			m_NumWrites++;
		}

		void WriteAndFreeBuffer(const u32 bufferId, const s32 tag = -1)
		{
			const u32 slotIndex = FindCacheEntry(bufferId);
			FastAssert(slotIndex < m_NumCacheBuffers);
			if(slotIndex < m_NumCacheBuffers)
			{
				sysDmaPut(GetCacheSlotPointer(slotIndex), 
					m_FrameBufferEA + kMixBufNumSamples*sizeof(f32)*bufferId, 
					sizeof(f32)*kMixBufNumSamples, 
					tag == -1 ? GetTagForSlot(slotIndex) : tag);
				m_NumWrites++;

				m_CacheEntries[slotIndex].state = kFree;
				m_BufferCacheLookup[bufferId] = -1;

				fbcDebugf1("%u (%u): Writing/Freeing entry", slotIndex, m_CacheEntries[slotIndex].bufferId);
			}
		}

		// PURPOSE
		//	Frees the specified cache line
		void FreeEntry(const u32 bufferId)
		{
			const u32 slotIndex = FindCacheEntry(bufferId);
			if(slotIndex < m_NumCacheBuffers)
			{
				FastAssert(m_CacheEntries[slotIndex].bufferId == bufferId);
				fbcDebugf1("%u (%u): Freeing entry", slotIndex, m_CacheEntries[slotIndex].bufferId);
				m_CacheEntries[slotIndex].state = kFree;
				m_BufferCacheLookup[bufferId] = -1;
			}
		}

		u32 GetNumWriteBacks() const { return m_NumWriteBacks; }
		u32 GetNumFetches() const { return m_NumFetches; }
		u32 GetNumWrites() const { return m_NumWrites; }

		void ResetStats() { m_NumFetches = m_NumWriteBacks = m_NumWrites = 0; }

		void WaitForAllSlots()
		{
			WaitForSlot(0);
			WaitForSlot(1);
			WaitForSlot(2);
			WaitForSlot(3);
		}

	private:

		u32 GetTagForSlot(const u32 slot) const
		{
			return m_StartTag + (slot & 3);
		}

		u32 FindCacheEntry(const u32 bufferId)
		{
			if(m_BufferCacheLookup[bufferId] == -1)
			{
				return ~0U;
			}
			return (u32)m_BufferCacheLookup[bufferId];
		}

		f32 *GetCacheSlotPointer(const u32 slot)
		{
			FastAssert(slot < m_NumCacheBuffers);
			return m_CacheStorage + slot * kMixBufNumSamples;
		}

		NOTFINAL_ONLY(SPU_ONLY(__attribute__((noinline))) )void WaitForSlot(const u32 slot) const 
		{
			sysDmaWait(1<<GetTagForSlot(slot));
		}

		u32 m_StartTag;

		u32 m_FrameBufferEA;
		u32 m_NumFrameBuffers;

		f32 *m_CacheStorage;
		u32 m_NumCacheBuffers;
		struct audFrameBufferCacheEntry
		{
			u16 bufferId;
			u16 state : 15;
			u16 readOnly : 1;
			u32 lastAccessTime;
		};
		enum CacheEntryState
		{
			kFree = 0,
			kAllocated,
			kPrefetching,
		};
		enum { kMaxCacheBuffers = 256 };
		atRangeArray<audFrameBufferCacheEntry, kMaxCacheBuffers> m_CacheEntries;
		atRangeArray<s8, audFrameBufferPool::kMaxBuffers> m_BufferCacheLookup;
		u32 m_NumWriteBacks;
		u32 m_NumFetches;
		u32 m_NumWrites;
		bool m_WriteBackOnEviction;
	};

	extern audFrameBufferCache *g_FrameBufferCache;

} // namespace rage

#else

// !__SPU: use g_FrameBufferPool directly
#define g_FrameBufferCache g_FrameBufferPool

#endif // __SPU

#endif // AUDIOHARDWARE_FRAMEBUFFERCACHE_H 
