// 
// audiohardware/driverconfig.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#include "system/param.h"

#include "config.h"
#include "driverconfig.h"

namespace rage
{
	PARAM(noaudiothread, "[RAGE Audio] Do not create an audio thread.  Also disables audio.");

bool audDriverConfig::Init()
{
	audConfig::GetData("driverSettings_WaveSlotsConfig", m_WaveSlotsConfig);
	audConfig::GetData("driverSettings_RpfConfig", m_RpfConfig);
	audConfig::GetData("driverSettings_RpfDirectory", m_RpfDirectory);
	audConfig::GetData("driverSettings_WaveRootPath", m_WaveRootPath);
	audConfig::GetData("driverSettings_EffectsRootPath", m_EffectsRootPath);

	audConfig::GetData("driverSettings_NumVirtualVoices", m_NumVirtualVoices);

	audConfig::GetData("driverSettings_NumPhysicalVoices", m_NumPhysicalVoices);

	audConfig::GetData("driverSettings_MaxPhysicalStereoVoices", m_MaxPhysicalStereoVoices);
	audConfig::GetData("driverSettings_NumPhysicalBufferVoices", m_NumNewPhysicalVoiceBufferVoices);
	audConfig::GetData("driverSettings_NumGranularDecoders", m_NumGranularDecoders);
	
	m_IsAudioProcessingEnabled = true;

#if !RSG_FINAL
	if(PARAM_noaudiothread.Get())
	{
		m_IsAudioProcessingEnabled = false;
	}
#endif // !RSG_FINAL

	return true;
}

}
