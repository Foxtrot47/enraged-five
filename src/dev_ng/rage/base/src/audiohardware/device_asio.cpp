//
// audiohardware/device_asio.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __WIN32PC

#include "system/param.h"
#include "system/wndproc.h"
#include "system/xtl.h"

#include "audioasiolib/asiosys.h"
#include "audioasiolib/asiodrivers.h"
#include "driver.h"
#include "decoder.h"
#include "decodemgr.h"
#include "device.h"
#include "device_asio.h"
#include "framebufferpool.h"
#include "mixer.h"

#include "mixing_vmath.inl"

extern bool loadAsioDriver(char *name);
extern AsioDrivers* asioDrivers;


namespace rage
{

PARAM(asiostartchannel, "[RAGE Audio] First ASIO output channel ID to use");
PARAM(asiodeviceid, "[RAGE Audio] ASIO driver index to use");
PARAM(asiobuffers, "[RAGE Audio] Number of 256-sample ASIO buffers to use");
PARAM(numasioinputs, "[RAGE Audio] Number of ASIO input channels to initialise");
s32 g_NumASIOBuffers = 1;

audMixerDeviceAsio *audMixerDeviceAsio::sm_Instance = NULL;

audMixerDeviceAsio::audMixerDeviceAsio()
{
}

audMixerDeviceAsio::~audMixerDeviceAsio()
{
}

bool audMixerDeviceAsio::InitHardware()
{
	m_NumOutputChannels = audDriver::GetNumOutputChannels();

	m_StartMixingSema = sysIpcCreateSema(true);
	m_FinishedMixingSema = sysIpcCreateSema(false);

	sm_Instance = this;

	s32 outputChanId;
	if(PARAM_asiostartchannel.Get(outputChanId))
	{
		m_OutputChannelStartId = outputChanId;
	}
	else
	{
		m_OutputChannelStartId = 0;
	}

	m_InputChannelStartId = 0;

	s32 numBuffers = 0;
	if(PARAM_asiobuffers.Get(numBuffers))
	{
		g_NumASIOBuffers = numBuffers;
	}

	SetupCallbacks();

	AsioDriverList list;
	s32 driverId = 0;
	if(PARAM_asiodeviceid.Get())
	{
		PARAM_asiodeviceid.Get(driverId);
	}
	driverId = Min<s32>(list.asioGetNumDev(), driverId);
	list.asioGetDriverName(driverId, &m_AsioDriverName[0], sizeof(m_AsioDriverName));
	// load the driver, this will setup all the necessary internal data structures
	if (loadAsioDriver (m_AsioDriverName))
	{
		sysMemSet(&m_AsioDriverInfo.driverInfo,0,sizeof(m_AsioDriverInfo.driverInfo));
		m_AsioDriverInfo.driverInfo.asioVersion = 2;
		m_AsioDriverInfo.driverInfo.sysRef = g_hwndMain;
		// initialize the driver
		if (ASIOInit (&m_AsioDriverInfo.driverInfo) == ASE_OK)
		{
			audDisplayf("asioVersion:   %08X\n"
					"driverVersion: %d\n"
					"Name:          %s\n"
					"ErrorMessage:  %s",
					m_AsioDriverInfo.driverInfo.asioVersion, m_AsioDriverInfo.driverInfo.driverVersion,
					m_AsioDriverInfo.driverInfo.name, m_AsioDriverInfo.driverInfo.errorMessage);
			if (InitStaticData(&m_AsioDriverInfo) == 0)
			{
				// ASIOControlPanel(); you might want to check whether the ASIOControlPanel() can open

				// set up the asioCallback structure and create the ASIO data buffer
				if (CreateBuffers() == ASE_OK)
				{
					audDisplayf("ASIO Driver initialised succesfully");
					return true;
				}
			}
		}
	}
	return false;
}

void audMixerDeviceAsio::StartMixing()
{
	if (ASIOStart() == ASE_OK)
	{
		// Now all is up and running
		audDisplayf("ASIO Driver started successfully.");
	}
	else
	{
		audErrorf("Failed to start ASIO driver");
		return;
	}

	m_ShouldMixThreadBeRunning = true;	
	m_MixThread = sysIpcCreateThread(MixThreadEntryProc_ASIO, this, sysIpcMinThreadStackSize, PRIO_TIME_CRITICAL, "[RAGE Audio] ASIO Mixer Thread", 0);
	audAssertf(m_MixThread != sysIpcThreadIdInvalid, "Couldn't create RAGE software mixer thread");
}

//----------------------------------------------------------------------------------
long audMixerDeviceAsio::InitStaticData (DriverInfo *asioDriverInfo)
{	// collect the informational data of the driver
	// get the number of available channels
	if(ASIOGetChannels(&asioDriverInfo->inputChannels, &asioDriverInfo->outputChannels) == ASE_OK)
	{
		audDisplayf("ASIOGetChannels (inputs: %d, outputs: %d)", asioDriverInfo->inputChannels, asioDriverInfo->outputChannels);

		// get the usable buffer sizes
		if(ASIOGetBufferSize(&asioDriverInfo->minSize, &asioDriverInfo->maxSize, &asioDriverInfo->preferredSize, &asioDriverInfo->granularity) == ASE_OK)
		{
			audDisplayf ("ASIOGetBufferSize (min: %d, max: %d, preferred: %d, granularity: %d)",
					 asioDriverInfo->minSize, asioDriverInfo->maxSize,
					 asioDriverInfo->preferredSize, asioDriverInfo->granularity);


			if(ASIOSetSampleRate(kMixerNativeSampleRate) == ASE_OK)
			{
				if(ASIOGetSampleRate(&asioDriverInfo->sampleRate) == ASE_OK)
					audDisplayf ("ASIOGetSampleRate (sampleRate: %f)", asioDriverInfo->sampleRate);
				else
					return -6;
			}
					
			// check whether the driver requires the ASIOOutputReady() optimization
			// (can be used by the driver to reduce output latency by one block)
			if(ASIOOutputReady() == ASE_OK)
				asioDriverInfo->postOutput = true;
			else
				asioDriverInfo->postOutput = false;
			audDisplayf ("ASIOOutputReady() -> %s", asioDriverInfo->postOutput ? "Supported" : "Not supported");

			return 0;
		} // GetBufferSize()
		return -3;
	}// GetChannels()

	return -4;
}

void audMixerDeviceAsio::ShutdownHardware()
{
	ASIOStop();
	ASIODisposeBuffers();
	ASIOExit();
		
	asioDrivers->removeCurrentDriver();

	Sleep(1000);
	
}

void audMixerDeviceAsio::SetupCallbacks()
{
	// set up the asioCallback structure and create the ASIO data buffer
	m_AsioCallbacks.bufferSwitch = &BufferSwitchCallback;
	m_AsioCallbacks.sampleRateDidChange = &SampleRateChangedCallback;
	m_AsioCallbacks.asioMessage = &AsioMessagesCallback;
	m_AsioCallbacks.bufferSwitchTimeInfo = NULL;
}

const char *GetSampleTypeName(const ASIOSampleType type)
{
	switch(type)
	{
	case ASIOSTInt16MSB: return "ASIOSTInt16MSB";
		case ASIOSTInt24MSB: return "ASIOSTInt24MSB";
		case ASIOSTInt32MSB : return "ASIOSTInt32MSB";
		case ASIOSTFloat32MSB: return "ASIOSTFloat32MSB";
		case ASIOSTFloat64MSB: return "ASIOSTFloat64MSB";

		// these are used for 32 bit data buffer, with different alignment of the data inside
		// 32 bit PCI bus systems can be more easily used with these
		case ASIOSTInt32MSB16: return "ASIOSTInt32MSB16";
		case ASIOSTInt32MSB18 : return "ASIOSTInt32MSB18";
		case ASIOSTInt32MSB20: return "ASIOSTInt32MSB20";
		case ASIOSTInt32MSB24: return "ASIOSTInt32MSB24";

		case ASIOSTInt16LSB: return "ASIOSTInt16LSB";
		case ASIOSTInt24LSB : return "ASIOSTInt24LSB";
		case ASIOSTInt32LSB : return "ASIOSTInt32LSB";
		case ASIOSTFloat32LSB: return "ASIOSTFloat32LSB";
		case ASIOSTFloat64LSB : return "ASIOSTFloat64LSB";

		// these are used for 32 bit data buffer, with different alignment of the data inside
		// 32 bit PCI bus systems can more easily used with these
		case ASIOSTInt32LSB16: return "ASIOSTInt32LSB16";
		case ASIOSTInt32LSB18: return "ASIOSTInt32LSB18";
		case  ASIOSTInt32LSB20: return "ASIOSTInt32LSB20";
		case ASIOSTInt32LSB24: return "ASIOSTInt32LSB24";

		//	ASIO DSD format.
		case ASIOSTDSDInt8LSB1: return "ASIOSTDSDInt8LSB1";
		case ASIOSTDSDInt8MSB1: return "ASIOSTDSDInt8MSB1";
		case ASIOSTDSDInt8NER8: return "ASIOSTDSDInt8NER8";
		default: return "Invalid sample type";
	}
}
ASIOError audMixerDeviceAsio::CreateBuffers()
{	
	// create buffers for the outputs of the card with the 
	// preferredSize from ASIOGetBufferSize() as buffer size
	u32 i;
	ASIOError result;

	// fill the bufferInfos from the start without a gap
	ASIOBufferInfo *info = m_AsioDriverInfo.bufferInfos;


	// prepare outputs

	m_AsioDriverInfo.numOutputBuffers = m_NumOutputChannels;
	for(i = 0; i < m_AsioDriverInfo.numOutputBuffers; i++, info++)
	{
		info->isInput = ASIOFalse;
		info->channelNum = i + m_OutputChannelStartId;
		info->buffers[0] = info->buffers[1] = 0;
	}

	s32 requestedNumInputs = 2;
	PARAM_numasioinputs.Get(requestedNumInputs);
	m_AsioDriverInfo.numInputBuffers = Min<s32>(requestedNumInputs, Min<s32>(kMaxAudioInputs, m_AsioDriverInfo.inputChannels));
	for(i = 0; i < m_AsioDriverInfo.numInputBuffers; i++, info++)
	{
		info->isInput = ASIOTrue;
		info->channelNum = i + m_InputChannelStartId;
		info->buffers[0] = info->buffers[1] = 0;
	}

	m_InputBufferIds.Resize(m_AsioDriverInfo.numInputBuffers);
	for(u32 i = 0; i < m_AsioDriverInfo.numInputBuffers; i++)
	{
		m_InputBufferIds[i] = g_FrameBufferPool->Allocate();
	}

	// create and activate buffers
	
	result = ASIOCreateBuffers(m_AsioDriverInfo.bufferInfos, m_AsioDriverInfo.numOutputBuffers + m_AsioDriverInfo.numInputBuffers, kMixBufNumSamples * g_NumASIOBuffers, &m_AsioCallbacks);
	if (result == ASE_OK)
	{
		// now get all the buffer details, sample word length, name, word clock group and activation
		for (i = 0; i < m_AsioDriverInfo.numOutputBuffers; i++)
		{
			Assert(!m_AsioDriverInfo.bufferInfos[i].isInput);
			m_AsioDriverInfo.channelInfos[i].channel = m_AsioDriverInfo.bufferInfos[i].channelNum;
			m_AsioDriverInfo.channelInfos[i].isInput = m_AsioDriverInfo.bufferInfos[i].isInput;
			result = ASIOGetChannelInfo(&m_AsioDriverInfo.channelInfos[i]);
			if (result != ASE_OK)
			{
				audErrorf("Failed to get channel info");
				break;
			}

			if(i == 0)
			{
				m_OutputFormat = m_AsioDriverInfo.channelInfos[i].type;
				audDisplayf("Output sample format type name: %s", GetSampleTypeName(m_OutputFormat));
			}
			Assert(m_OutputFormat == m_AsioDriverInfo.channelInfos[i].type);

#if !__NO_OUTPUT
			static const char *speakerName[] = {"FL","FR","C","LFE","SL","SR","RL","RR"};
#endif
			
			audDisplayf("Initialised output channel \'%s\' for speaker %s", m_AsioDriverInfo.channelInfos[i].name, speakerName[i]);
		}

		for (i = m_AsioDriverInfo.numOutputBuffers; i < m_AsioDriverInfo.numOutputBuffers+m_AsioDriverInfo.numInputBuffers; i++)
		{
			Assert(m_AsioDriverInfo.bufferInfos[i].isInput);

			m_AsioDriverInfo.channelInfos[i].channel = m_AsioDriverInfo.bufferInfos[i].channelNum;
			m_AsioDriverInfo.channelInfos[i].isInput = m_AsioDriverInfo.bufferInfos[i].isInput;
			result = ASIOGetChannelInfo(&m_AsioDriverInfo.channelInfos[i]);
			if (result != ASE_OK)
			{
				audErrorf("Failed to get channel info");
				break;
			}

			if(i == 0)
			{
				m_OutputFormat = m_AsioDriverInfo.channelInfos[i].type;
				audDisplayf("Input sample format type name: %s", GetSampleTypeName(m_OutputFormat));
			}
			Assert(m_OutputFormat == m_AsioDriverInfo.channelInfos[i].type);

			audDisplayf("Initialised input channel \'%s\'", m_AsioDriverInfo.channelInfos[i].name);
		}

		if (result == ASE_OK)
		{
			// get the input and output latencies
			// Latencies often are only valid after ASIOCreateBuffers()
			// (input latency is the age of the first sample in the currently returned audio block)
			// (output latency is the time the first sample in the currently returned audio block requires to get to the output)
			result = ASIOGetLatencies(&m_AsioDriverInfo.inputLatency, &m_AsioDriverInfo.outputLatency);
			if (result == ASE_OK)
			{
				audDisplayf("ASIOGetLatencies (input: %d, output: %d) -> OK", m_AsioDriverInfo.inputLatency, m_AsioDriverInfo.outputLatency);
			}
		}
	}

	// Prepare inputs
	
	return result;
}

void audMixerDeviceAsio::BufferSwitch(const s32 index, const ASIOBool UNUSED_PARAM(processNow))
{
	// Grab input data first
	for(u32 i = m_AsioDriverInfo.numOutputBuffers; i < m_AsioDriverInfo.numInputBuffers + m_AsioDriverInfo.numOutputBuffers; i++)
	{
		const u32 inputChannelId = i - m_AsioDriverInfo.numOutputBuffers;
		const u32 inputBufferId = m_InputBufferIds[inputChannelId];
		f32 *inputBufferPtr = g_FrameBufferPool->GetBuffer(inputBufferId);

		if(inputBufferPtr)
		{
			// Surprisingly drivers don't guarantee buffer alignment, so copy locally to ensure we can use SSE without
			// alignment faults.
			ALIGNAS(16) u32 scratchBuffer[kMixBufNumSamples] ;
			switch(m_AsioDriverInfo.channelInfos[i].type)
			{
			case ASIOSTInt24LSB:				
				// three bytes per sample
				sysMemCpy(&scratchBuffer[0], m_AsioDriverInfo.bufferInfos[i].buffers[index], kMixBufNumSamples*3);
				ConvertBuffer_Int24LSB(inputBufferPtr, (s8*)&scratchBuffer[0]);
				break;
			case ASIOSTFloat32LSB:
				sysMemCpy(inputBufferPtr, m_AsioDriverInfo.bufferInfos[i].buffers[index], kMixBufNumSamples*sizeof(f32));
				break;
			case ASIOSTInt16LSB:
				sysMemCpy(&scratchBuffer[0], m_AsioDriverInfo.bufferInfos[i].buffers[index], kMixBufNumSamples*2);
				CopyMonoBufferToMonoMixBuf(inputBufferPtr, (s16*)&scratchBuffer[0], kMixBufNumSamples);
				break;
			case ASIOSTInt32LSB:
				sysMemCpy(&scratchBuffer[0], m_AsioDriverInfo.bufferInfos[i].buffers[index], kMixBufNumSamples*4);
				CopyMonoBufferToMonoMixBuf(inputBufferPtr,  (s32*)&scratchBuffer[0], kMixBufNumSamples);
				break;
			}	
		}
	}

	for(s32 pass = 0; pass < g_NumASIOBuffers; pass++)
	{
		sysIpcSignalSema(m_StartMixingSema);
		sysIpcWaitSema(m_FinishedMixingSema);

		for(u32 i = 0; i < m_AsioDriverInfo.numOutputBuffers; i++)
		{
			switch(m_AsioDriverInfo.channelInfos[i].type)
			{
			case ASIOSTInt24LSB:				
				// three bytes per sample
				{
					void *dest = ((s8*)m_AsioDriverInfo.bufferInfos[i].buffers[index]) + kMixBufNumSamples*3*pass;
					ConvertBuffer_Int24LSB(dest, m_AsioDriverInfo.bufferInfos[i].channelNum-m_OutputChannelStartId);
				}
				break;
			case ASIOSTFloat32LSB:
				sysMemCpy((f32*)m_AsioDriverInfo.bufferInfos[i].buffers[index] + kMixBufNumSamples*pass, &GetOutputBuffer()[kMixBufNumSamples *(m_AsioDriverInfo.bufferInfos[i].channelNum-m_OutputChannelStartId)], kMixBufNumSamples*sizeof(f32));
				break;
			case ASIOSTInt16LSB:
				{
					s16 *dest = ((s16*)m_AsioDriverInfo.bufferInfos[i].buffers[index]) + kMixBufNumSamples*pass;
					ConvertBuffer_Int16LSB(dest, m_AsioDriverInfo.bufferInfos[i].channelNum-m_OutputChannelStartId);					
				}
				break;
			case ASIOSTInt32LSB:
				{
					s32 *dest = ((s32*)m_AsioDriverInfo.bufferInfos[i].buffers[index]) + kMixBufNumSamples*pass;
					ConvertBuffer_Int32LSB(dest, m_AsioDriverInfo.bufferInfos[i].channelNum-m_OutputChannelStartId);					
				}

			default:
				break;		
			}
			
		}
	}
	if(m_AsioDriverInfo.postOutput)
	{
		ASIOOutputReady();
	}
}

long audMixerDeviceAsio::AsioMessagesCallback(long selector, long value, void *UNUSED_PARAM(message), double *UNUSED_PARAM(opt))
{
	long ret = 0;
	switch(selector)
	{
		case kAsioSelectorSupported:
			if(value == kAsioResetRequest
			|| value == kAsioEngineVersion
			|| value == kAsioResyncRequest
			|| value == kAsioLatenciesChanged
			)
				ret = 1L;
			break;
		case kAsioResetRequest:
			// defer the task and perform the reset of the driver during the next "safe" situation
			// You cannot reset the driver right now, as this code is called from the driver.
			// Reset the driver is done by completely destruct is. I.e. ASIOStop(), ASIODisposeBuffers(), Destruction
			// Afterwards you initialize the driver again.
		
			ret = 1L;
			break;
		case kAsioResyncRequest:
			// This informs the application, that the driver encountered some non fatal data loss.
			// It is used for synchronization purposes of different media.
			// Added mainly to work around the Win16Mutex problems in Windows 95/98 with the
			// Windows Multimedia system, which could loose data because the Mutex was hold too long
			// by another thread.
			// However a driver can issue it in other situations, too.
			ret = 1L;
			break;
		case kAsioLatenciesChanged:
			// This will inform the host application that the drivers were latencies changed.
			// Beware, it this does not mean that the buffer sizes have changed!
			// You might need to update internal delay data.
			ret = 1L;
			break;
		case kAsioEngineVersion:
			// return the supported ASIO version of the host application
			// If a host applications does not implement this selector, ASIO 1.0 is assumed
			// by the driver
			ret = 2L;
			break;
		case kAsioSupportsTimeInfo:
			// informs the driver wether the asioCallbacks.bufferSwitchTimeInfo() callback
			// is supported.
			// For compatibility with ASIO 1.0 drivers the host application should always support
			// the "old" bufferSwitch method, too.
			ret = 0;
			break;
		case kAsioSupportsTimeCode:
			// informs the driver wether application is interested in time code info.
			// If an application does not need to know about time code, the driver has less work
			// to do.
			ret = 0;
			break;
	}
	return ret;
}

void audMixerDeviceAsio::SampleRateChangedCallback(ASIOSampleRate sRate)
{
#if !__ASSERT && __WIN32PC
	(void) sRate;	// keep the compiler happy
#endif
	audDisplayf("ASIO sample rate changed! new sample rate: %d", sRate);
}

// ASIO
DECLARE_THREAD_FUNC(audMixerDeviceAsio::MixThreadEntryProc_ASIO)
{
	audMixerDeviceAsio *device = (audMixerDeviceAsio*)ptr;

	device->m_IsMixThreadRunning = true;
	device->MixLoop();
	device->m_IsMixThreadRunning = false;
}

void audMixerDeviceAsio::MixLoop()
{
	while(m_ShouldMixThreadBeRunning)
	{
		sysIpcWaitSema(m_StartMixingSema);
		MixBuffers();
		sysIpcSignalSema(m_FinishedMixingSema);
	}
}

void audMixerDeviceAsio::ConvertBuffer_Int24LSB(void *destination, const u32 channelIndex)
{
	s8 *dest = (s8*)destination;
	
	const f32 scaling = 8388607.f; // (2^23) - 1
	//const f32 scaling = 32767.f; // (2^15) - 1

	Assert(channelIndex < GetNumOutputChannels());
	const f32 *RESTRICT source = &GetOutputBuffer()[kMixBufNumSamples * channelIndex];
	for(u32 i = 0; i < kMixBufNumSamples; i++)
	{
		const f32 o = source[i] * scaling * 2.f;
		const s32 s = (s32)o;

		// 24 bits, LSB first
		*dest++ = (s8)(s&0xff);
		*dest++ = (s8)(s>>8&0xff);
		*dest++ = (s8)(s>>16&0xff);
	}
}

void audMixerDeviceAsio::ConvertBuffer_Int24LSB(f32 *destination, const s8 *src)
{	
	const f32 scaling = 8388608.f; // (2^23) 	
	
	for(u32 i = 0; i < kMixBufNumSamples; i++)
	{
		s32 sample = 0;

		((s8*)&sample)[0] = *src++;
		((s8*)&sample)[1] = *src++;
		((s8*)&sample)[2] = *src++;
		
		destination[i] = sample / scaling;
	}
}

void audMixerDeviceAsio::ConvertBuffer_Int32LSB(s32 *destination, const u32 channelIndex)
{
	f32 scaled_value;
	static const f32 SCALE_INT32 = 2147483647.f; // (- (expt 2 15) 1)

	Assert(channelIndex < GetNumOutputChannels());
	const f32 *RESTRICT source = &GetOutputBuffer()[kMixBufNumSamples * channelIndex];

	for(u32 i = 0; i < kMixBufNumSamples; i++)
	{
		scaled_value = source[i] * SCALE_INT32;
		destination[i] = (s32)(scaled_value);
	}
}

void audMixerDeviceAsio::ConvertBuffer_Int16LSB(s16 *destination, const u32 channelIndex)
{
	f32 scaled_value;
	static const f32 SCALE_INT16 = 32767.f; // (- (expt 2 15) 1)

	
	Assert(channelIndex < GetNumOutputChannels());
	const f32 *RESTRICT source = &GetOutputBuffer()[kMixBufNumSamples * channelIndex];

	for(u32 i = 0; i < kMixBufNumSamples; i++)
	{
		scaled_value = source[i] * SCALE_INT16;
		destination[i] = (s16)(scaled_value);
	}
}

} // namespace rage

#endif // __WIN32PC
