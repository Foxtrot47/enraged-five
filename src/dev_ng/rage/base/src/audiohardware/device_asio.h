//
// audiohardware/device_asio.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DEVICE_ASIO_H
#define AUD_DEVICE_ASIO_H

#if __WIN32PC

#include "audioasiolib/asiosys.h"
#include "audioasiolib/asio.h"

#include "device_pc.h"
#include "driverdefs.h"

namespace rage {

enum { kMaxAudioInputs = 16 };

// PURPOSE
//	This structure wraps up all ASIO device driver info
typedef struct DriverInfo
{
	// ASIOInit()
	ASIODriverInfo driverInfo;

	// ASIOGetChannels()
	long           inputChannels;
	long           outputChannels;

	// ASIOGetBufferSize()
	long           minSize;
	long           maxSize;
	long           preferredSize;
	long           granularity;

	// ASIOGetSampleRate()
	ASIOSampleRate sampleRate;

	// ASIOOutputReady()
	bool           postOutput;

	// ASIOGetLatencies ()
	long           inputLatency;
	long           outputLatency;

	// ASIOCreateBuffers ()
	u32 numOutputBuffers;
	u32 numInputBuffers;
	ASIOBufferInfo bufferInfos[g_MaxOutputChannels + kMaxAudioInputs];
	ASIOChannelInfo channelInfos[g_MaxOutputChannels + kMaxAudioInputs];

	// The above two arrays share the same indexing, as the data in them are linked together



	// Information from ASIOGetSamplePosition()
	// data is converted to double floats for easier use, however 64 bit integer can be used, too
	double         nanoSeconds;
	double         samples;
	double         tcSamples;	// time code samples

	// bufferSwitchTimeInfo()
	ASIOTime       tInfo;			// time info state
	unsigned long  sysRefTime;      // system reference time, when bufferSwitch() was called

	// Signal the end of processing in this example
	bool           stopped;
} DriverInfo;

// PURPOSE
//	This class implements a multi-channel ASIO output device
class audMixerDeviceAsio : public audMixerDevice
{

public:

	audMixerDeviceAsio();
	~audMixerDeviceAsio();

	// PURPOSE
	//	Initialises direct sound wave_format_ex output hardware
	// RETURNS
	//	false on failure
	virtual bool InitHardware();

	// PURPOSE
	//	Releases DirectSound output resources
	virtual void ShutdownHardware();

	// PURPOSE
	//	Starts generating output
	virtual void StartMixing();

	static bool CheckForSoundCard();

	u32 GetNumInputs()
	{
		return m_AsioDriverInfo.numInputBuffers;
	}
	u32 GetInputBufferId(const u32 inputChannel)
	{
		return m_InputBufferIds[inputChannel];
	}

private:

	static DECLARE_THREAD_FUNC(MixThreadEntryProc_ASIO);


	void MixLoop();

	void BufferSwitch(const s32 index, const ASIOBool processNow);

	// PURPOSE
	//	Creates the ASIO output buffers for this driver
	// PARAMS
	//	DriverInfo structure containing ASIO driver details
	ASIOError CreateBuffers();

	long InitStaticData(DriverInfo *driverInfo);

	static void BufferSwitchCallback(long index, ASIOBool processNow) { sm_Instance->BufferSwitch(index,processNow); }
	static void SampleRateChangedCallback(ASIOSampleRate sRate);
	static long AsioMessagesCallback(long selector, long value, void* message, double* opt);
	
	// type conversion
	void ConvertBuffer_Int24LSB(void *destination, const u32 channelIndex);
	void ConvertBuffer_Int16LSB(s16 *destination, const u32 channelIndex);
	void ConvertBuffer_Int32LSB(s32 *destination, const u32 channelIndex);

	void ConvertBuffer_Int24LSB(f32 *destination, const s8 *src);

	void SetupCallbacks();
	

	char m_AsioDriverName[255];
	ASIOCallbacks m_AsioCallbacks;

	u32 m_NumOutputChannels;

	s32 m_OutputChannelStartId;
	u32 m_InputChannelStartId;


	volatile bool m_IsMixThreadRunning, m_ShouldMixThreadBeRunning;
	sysIpcSema m_StartMixingSema, m_FinishedMixingSema;

	DriverInfo m_AsioDriverInfo;

	ASIOSampleType m_OutputFormat;

	atFixedArray<u32, kMaxAudioInputs> m_InputBufferIds;
	sysIpcThreadId m_MixThread;
	static audMixerDeviceAsio *sm_Instance;
};


} // namespace rage
#endif // __WIN32PC
#endif // AUD_DEVICE_ASIO_H
