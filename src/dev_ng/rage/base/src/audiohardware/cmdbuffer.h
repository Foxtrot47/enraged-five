// 
// audiohardware/cmdbuffer.h 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_CMDBUFFER_H
#define AUD_CMDBUFFER_H

#include "atl/atfunctor.h"
#include "atl/array.h"
#include "math/amath.h"
#include "system/criticalsection.h"

#include "channel.h"

#define COMMAND_BUFFER_SIZE_MUTIPLIER	2

#define AUD_CMD_BUFFER_LOCKING 0

#if !AUD_CMD_BUFFER_LOCKING 
#define AUD_CMD_BUFFER_LOCKING_ONLY(x)
#define AUD_CMD_BUFFER_LOCK(x)
#else
#define AUD_CMD_BUFFER_LOCK(x) SYS_CS_SYNC(x)
#define AUD_CMD_BUFFER_LOCKING_ONLY(x) (x)
#endif

namespace rage
{
	struct audCommandPacketHeader
	{
		enum { kNumBitsForObjectId = 11 };
		u32 systemId	: 3; // Voice, Effect, Submix, PCM Generator ...
		u32 commandId	: 8; // SetVolume, Start, ...
		u32 objectId	: kNumBitsForObjectId; // voiceId, effectIndex, ...
		u32 packetSize	: 10; // bytes
	};

	typedef atFunctor2<void,const u32,const audCommandPacketHeader*> ForEachCommandFunctor;

	class audCommandBuffer
	{
		friend class audCommandBufferIterator;
	public:
		audCommandBuffer();
		~audCommandBuffer();

		void Init(const u32 bufferSize, const char *ASSERT_ONLY(name));

		void Shutdown();

		// PURPOSE
		//	Writes the packet data into the command buffer
		bool WritePacket(const audCommandPacketHeader *packetData);

		// PURPOSE
		//	Writes the packet data into the command buffer starting at a 16-byte aligned address
		// NOTES
		//	This will pad as required to 16 bytes, aligned packets should be written consecutively to minimise
		//	wasted space.
		bool WriteAlignedPacket(const audCommandPacketHeader *packetData);

		enum EmptyBufferFlag {kEmpty,kDontEmpty};
	
		// PURPOSE
		//	Calls the specified functor on each command in the buffer
		// PARAMS
		//	functor which will be called for every packet in the buffer.  If the functor returns false iteration
		//	will stop.
		//  timestamp - passed through to the functor
		//  reset - if kEmpty the command buffer will be cleared before this function returns
		void ForEachPacket(ForEachCommandFunctor functor, const u32 timestamp, const EmptyBufferFlag reset);

		// PURPOSE
		//	Clears all commands from the buffer, resetting its state to empty
		void Reset();

		u32 GetHighWatermark() const
		{
			return m_HighWatermark;
		}

		bool IsEmpty()
		{
			return (m_CurrentOffset == 0);
		}

		u32 GetCurrentOffset() const
		{
			return m_CurrentOffset;
		}

		u32 GetSize() const
		{
			return m_BufferSize;
		}

		u8 *GetBuffer()
		{
			return m_Buffer;
		}

		bool IsReadyForProcessing() const { return m_ReadyForProcessing; }
		void SetReadyForProcessing(const bool ready) { m_ReadyForProcessing = ready; }

		u32 GetTimestamp() const { return m_Timestamp; }
		void SetTimestamp(const u32 timeStamp) { m_Timestamp = timeStamp; }

		u32 GetSubmitTime() const { return m_SubmitTime; }
		void SetSubmitTime(const u32 timeStamp) { m_SubmitTime = timeStamp; }

		sysIpcSema GetProcessedSema() { return m_ProcessedSemaphore; }

		// PURPOSE
		//	Override the internal current offset.  Used to update command buffers from an SPU
		void SetCurrentOffset(const u32 currentOffset)
		{
			TrapGT(currentOffset, m_BufferSize);
			m_CurrentOffset = currentOffset;
			m_HighWatermark = Max(m_HighWatermark, m_CurrentOffset);
		}

#if __ASSERT
		const char *GetName() const { return m_Name; }
#endif


		u32 GetAvailableSpace() const
		{
			return m_BufferSize - m_CurrentOffset;
		}

	private:

		enum {kSystemIdPadding = 7};

#if AUD_CMD_BUFFER_LOCKING
		sysCriticalSectionToken m_CritSec;
#endif // AUD_CMD_BUFFER_LOCKING

		u8 *m_Buffer;
		u32 m_BufferSize;


		sysIpcSema m_ProcessedSemaphore;
		u32 m_Timestamp;
		u32 m_SubmitTime;
		volatile bool m_ReadyForProcessing;
		
		ASSERT_ONLY(const char *m_Name);
		volatile u32 m_CurrentOffset;
		volatile u32 m_HighWatermark;

		ASSERT_ONLY(sysIpcThreadId m_LastWriteThreadId);
	};

	class audCommandBufferIterator
	{
	public:
		audCommandBufferIterator(audCommandBuffer &cmdBuffer)
			: m_CommandBuffer(cmdBuffer)
			, m_IteratorOffset(0)
		{

		}

		~audCommandBufferIterator()
		{
			
		}

		audCommandPacketHeader *Next()
		{
			while(m_IteratorOffset < m_CommandBuffer.m_CurrentOffset)
			{
				audCommandPacketHeader *header = (audCommandPacketHeader *)(&m_CommandBuffer.m_Buffer[m_IteratorOffset]);

				// Prevent infinite loop if we encounter a malformed packet
				if(audVerifyf(header->packetSize != 0, "Audio command buffer: encountered 0-length packet"))
				{
					m_IteratorOffset += header->packetSize;
					// Skip padding
					if(header->systemId != audCommandBuffer::kSystemIdPadding)
					{					
						return header;
					}
				}
				else
				{
					return NULL;
				}				
			}
			return NULL;
		}

	private:

		audCommandBuffer &m_CommandBuffer;
		u32 m_IteratorOffset;
	};
}

#endif // AUD_CMDBUFFER_H
