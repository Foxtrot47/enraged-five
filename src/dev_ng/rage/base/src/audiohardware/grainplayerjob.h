// 
// audiohardware/grainplayerjob.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_GRAINPLAYERJOB_H
#define AUD_GRAINPLAYERJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(grainplayerjob);

#endif // AUD_GRAINPLAYERJOB_H
