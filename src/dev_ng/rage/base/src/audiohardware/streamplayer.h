//
// audiohardware/streamplayer.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_STREAMPLAYER_H
#define AUD_STREAMPLAYER_H

#include "cmdbuffer.h"
#include "pcmsource.h"
#include "ringbuffer.h"
#include "syncsource.h"
#include "waveref.h"

namespace rage
{
	// PURPOSE
	//	Encapsulates PCM stream (ie circular buffer populated by external system) playback functionality - 
	//	with optional format and sample rate conversion
	class audRingBuffer;
	class audStreamPlayer : public audPcmSource
	{
	public:
		friend class audMixerVoice;

		audStreamPlayer();

		struct audInitStreamPlayerCommandPacket : public audPcmSourceCustomCommandPacket
		{
			audInitStreamPlayerCommandPacket()
				: ringBuffer(NULL)
				, sampleRate(0)
				, numChannels(0)
				, format(audWaveFormat::kPcm16bitLittleEndian)
			{
				 packetSize = sizeof(audInitStreamPlayerCommandPacket);								
			}

			audReferencedRingBuffer *ringBuffer;
			u16 sampleRate;
			u8 format;
			u8 numChannels;
		};

		struct Params
		{
			static const u32 InitParams = 0xD9D79602;
			static const u32 Gain = 0x24672FD4;
			static const u32 StopPlayback = 0x4A86906D; // Low latency route to stop an external stream sound, so that the referenced ring buffer can be reused
		};

		// PURPOSE
		//	Returns the current playback position of the wave, in samples at the asset
		//	sample rate.
		AUD_PCMSOURCE_VIRTUAL u32 GetPlayPositionSamples() const;

		// PURPOSE
		//	Returns true the end of the wave has been reached
		AUD_PCMSOURCE_VIRTUAL bool IsFinished() const { return m_FinishedPlayback; }

		// PURPOSE
		//	Returns true if the generator has started playback
		AUD_PCMSOURCE_VIRTUAL bool HasStartedPlayback() const { return m_PlayState != kStateIdle; }

		// runs on SPU
		AUD_PCMSOURCE_VIRTUAL void GenerateFrame();
		AUD_PCMSOURCE_VIRTUAL void SkipFrame();

		// runs on PPU
		AUD_PCMSOURCE_VIRTUAL void BeginFrame();
		AUD_PCMSOURCE_VIRTUAL void EndFrame();

		// Start and Stop occurs on the PPU
		AUD_PCMSOURCE_VIRTUAL void Start();
		AUD_PCMSOURCE_VIRTUAL void Stop();

		AUD_PCMSOURCE_VIRTUAL void StartPhys(s32 channel);
		AUD_PCMSOURCE_VIRTUAL void StopPhys(s32 channel);

		AUD_PCMSOURCE_VIRTUAL bool ProcessSyncSignal(const audMixerSyncSignal &signal);

		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const u32 val);
		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const f32 val);
				
		AUD_PCMSOURCE_VIRTUAL void HandleCustomCommandPacket(const audPcmSourceCustomCommandPacket* packet);

		void Shutdown();

		s32 GetPlayState() const { return m_PlayState; }
		
	private:

		bool EvaluatePredelay();

		void Init();

		u32 ComputeSamplesAvailable() const;
		void AdvanceReadOffset(const u32  numSamples);
		void CopyRingBufferData(s16 *dest, const u32 size, const u32 offset);
		bool IsRingBufferHalfFull() const;

#if __SPU
		void FetchPCMSamples(s16 *dest, const s16 *src, const s32 numSamples, const s32 dmaTag);
#endif

		u32 m_NumSamplesConsumed;

		f32 m_SrcTrailingFrac;
		
		enum { kStateIdle,kStatePlaying,kStateStopped } m_PlayState;

		audReferencedRingBuffer *m_RingBuffer;

		audRingBuffer::State *m_RingBufferState;

		float m_Gain;
		u32 m_RingBufferSize;
		const u8 *m_RingBufferData;
		u32 m_RingBufferReadOffset;		
		u16 m_SampleRate;
		
		u32 m_PredelayFrames;
		
		u32 m_NumChannels : 8;
		u32 m_PredelaySamples : 8;	

		u32 m_FinishedPlayback : 1;
		u32 m_HasBegunFrame : 1;
		u32 m_HasEndedFrame : 1;
		bool m_StopRequested : 1;
		bool m_IsStarved : 1;
		bool m_HasStarted : 1;
		bool m_StopPlayback : 1;
		bool m_IsPaused : 1;
		bool m_IsPauseRequested : 1;
	};
}

#endif // AUD_STREAMPLAYER_H
