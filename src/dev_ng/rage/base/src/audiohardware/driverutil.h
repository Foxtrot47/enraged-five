//
// audiohardware/driverutil.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DRIVER_UTIL_H
#define AUD_DRIVER_UTIL_H

#include "driverdefs.h"
#include "math/amath.h"
#include "math/vecmath.h"

#include "vector/vector3.h"
#include "vectormath/vectormath.h"

#if __XENON
// don't want to include xnamath.h (and xtl.h) here
extern __vector4        XMVectorPow(__vector4 V1, __vector4 V2);
#endif

#define AUD_AVX_TRANSCENDENTALS (AVX_TRANSCENDENTALS)

namespace rage 
{ 

// PURPOSE
//	This class provides various audio utility/conversion functions
// SEE ALSO
//	audEngineUtil
class audDriverUtil
{
public:
	//
	// PURPOSE
	//	Converts from samples to milliseconds at the supplied sample rate.
	// PARAMS
	//	numSamples - The number of samples to be converted.
	//	sampleRate - The sample rate at which the samples are being played.
	// RETURNS
	//	The resulting time, in milliseconds.
	//
	static inline u32 ConvertSamplesToMs(const u32 numSamples, const u32 sampleRate)
	{
		Assert(sampleRate != 0);
		const f32 fResult = (f32)numSamples * 1000.f / (f32)sampleRate;
		return (u32)Floorf( fResult );
	}

	static inline u32 ConvertSamplesToMs(const f32 numSamples, const f32 sampleRate)
	{
		Assert(sampleRate != 0.f);
		const f32 fResult = numSamples * 1000.f / sampleRate;
		return (u32)Floorf( fResult );
	}


	//
	// PURPOSE
	//	Converts from milliseconds to samples at the supplied sample rate
	// PARAMS
	//	numMilliseconds	- The number of milliseconds to be converted.
	//	sampleRate		- The sample rate at which the samples are being played.
	// RETURNS
	//	The resulting number of samples.
	//
	static inline u32 ConvertMsToSamples(const u32 numMilliseconds, const u32 sampleRate)
	{
		const f32 fResult = (f32)numMilliseconds * (f32)sampleRate * 0.001f;
		return (u32)Floorf(fResult);
	}

	//
	// PURPOSE
	//	Converts from pitch, in cents, to a ratio.
	// PARAMS
	//	pitch - pitch, in cents.
	// RETURNS
	//	Frequency scaling factor.
	//	
	static __forceinline f32 ConvertPitchToRatio(const s32 pitch)
	{
		return Powf(2.0f, (float)pitch / 1200.0f);
	}
	static __forceinline f32 ConvertPitchToRatio(const f32 pitch)
	{
		return Powf(2.0f, pitch / 1200.0f);
	}

	//
	// PURPOSE
	//	Converts from a frequency ratio to pitch, in cents.
	// PARAMS
	//	ratio - Frequency ratio.
	// RETURNS
	//	Pitch, in cents.
	//
	static __forceinline s32 ConvertRatioToPitch(const f32 ratio)
	{
		return (s32)(1200.f * (log10(ratio)/0.30103f));
	}
	static __forceinline f32 ConvertRatioToPitch_Float(const f32 ratio)
	{
		return (1200.f * (log10(ratio)/0.30103f));
	}

	//
	// PURPOSE
	//	Returns a pitch-linear frequency value from the ratio of min to max filter cutoff frequencies
	// PARAMS
	//	ratio - pitch ratio between min filter as pitch and max filter as pitch.
	//	ratio of 0.0f will return cutoff of 0, ratio of 1.0f will return g_audVoiceFilterMaxCutoff
	// RETURNS
	//	frequency, in hz
	//
	static inline f32 ComputeHzFrequencyFromLinear(const f32 linearFrequency)
	{
		f32 freqInOctaves = (1.0f - linearFrequency) * g_audOctavesAt23900;
		return Powf(2.0f, freqInOctaves) * kVoiceFilterLPFMaxCutoff;
	}

	//
	// PURPOSE
	//	Returns a pitch-linear ratio value from a frequency
	// PARAMS
	//	frequency - value in hZ
	//	frequency of g_audVoiceFilterMaxCutoff will return ratio of 1.0f
	// RETURNS
	//	ratio, in range 0.0f - 1.0f assuming you pass in a valid frequency
	//
	static inline f32 ComputeLinearFromHzFrequency(const f32 frequency)
	{
		f32 ratioInOctaves = log10(frequency * g_InvSourceLPFilterCutoff)  * 3.3219280470385011460651762282829f; // / 0.30103f;
		return 1.0f - (ratioInOctaves * g_audInvOctavesAt23900);
	}

	//
	// PURPOSE
	//  Converts a linear volume into dB.
	// PARAMS
	//	linearVolume - The linear volume to be converted.
	// RETURNS
	//	The converted volume, in dB.
	//
	static inline f32 ComputeDbVolumeFromLinear(const f32 linearVolume)
	{
		const f32 computedVol = (20.f * Log10(linearVolume));
		return Selectf(linearVolume - g_SilenceVolumeLin, computedVol, g_SilenceVolume);
	}
	static inline f32 ComputeDbVolumeFromLinear_Precise(const f32 linearVolume)
	{
		const f32 computedVol = (20.f * log10(linearVolume));
		return Selectf(linearVolume - g_SilenceVolumeLin, computedVol, g_SilenceVolume);
	}

	static inline Vec::Vector_4V_Out ComputeDbVolumeFromLinear(Vec::Vector_4V_In linearVolume)
	{
		// 20.f
		const Vec::Vector_4V twenty = Vec::V4VConstant<0x41A00000,0x41A00000,0x41A00000,0x41A00000>();
		// 0.00001f
		const Vec::Vector_4V silenceVolumeLin = Vec::V4VConstant<0x3727C5AC,0x3727C5AC,0x3727C5AC,0x3727C5AC>();
		// -100.f
		const Vec::Vector_4V silenceVolumedB = Vec::V4VConstant<0xC2C80000,0xC2C80000,0xC2C80000,0xC2C80000>();

		const Vec::Vector_4V computedVol = Vec::V4Scale(twenty, Vec::V4Log10(linearVolume));
		const Vec::Vector_4V selMask = Vec::V4IsGreaterThanV(linearVolume, silenceVolumeLin);		
		return Vec::V4SelectFT(selMask, silenceVolumedB, computedVol);
	}

	//
	// PURPOSE
	//  Converts a FCL volume into dB.
	// PARAMS
	//	fclVolume - The FCL volume to be converted.
	// RETURNS
	//	The converted volume, in dB.
	//
	static inline f32 ComputeDbVolumeFromFCL(const f32 fclVolume)
	{
		const f32 computedVol = (40.f * Log10(fclVolume));
		return Selectf(fclVolume - g_SilenceVolumeFcl, computedVol, g_SilenceVolume);
	}

	//
	// PURPOSE
	//  Converts a dB volume into linear.
	// PARAMS
	//	dbVolume - The volume to be converted, in dB.
	// RETURNS
	//	The converted linear volume.
	//
	static inline f32 ComputeLinearVolumeFromDb(const f32 dbVolume)
	{
		const f32 computedVol = (Powf(10.0f, (dbVolume/20.0f)));
		return Selectf(dbVolume - (g_SilenceVolume+1.f), computedVol, 0.f);
	}

	static inline Vec::Vector_4V_Out ComputeLinearVolumeFromDb(Vec::Vector_4V_In dbVolume)
	{
		const Vec::Vector_4V ten = Vec::V4VConstant(V_TEN);
		const Vec::Vector_4V twenty = Vec::V4VConstant<0x41A00000,0x41A00000,0x41A00000,0x41A00000>();
		// -100.f
		const Vec::Vector_4V silenceVolumedB = Vec::V4VConstant<0xC2C80000,0xC2C80000,0xC2C80000,0xC2C80000>();

		const Vec::Vector_4V computedVol = Vec::V4Pow(ten, Vec::V4InvScale(dbVolume,twenty));
		const Vec::Vector_4V selMask = Vec::V4IsGreaterThanV(dbVolume, silenceVolumedB);
		return Vec::V4SelectFT(selMask, Vec::V4VConstant(V_ZERO), computedVol);
	}

	//
	// PURPOSE
	//  Converts a dB volume into FCL.
	// PARAMS
	//	dbVolume - The volume to be converted, in dB.
	// RETURNS
	//	The converted FCL volume.
	//
	static inline f32 ComputeFCLVolumeFromDb(const f32 dbVolume)
	{
		const f32 computedVol = (Powf(10.0f, (dbVolume/40.0f)));
		return Selectf(dbVolume - (g_SilenceVolume+1.f), computedVol, 0.f);
	}

	//
	// PURPOSE
	//	This function clamps the step size of the requested volume change to g_MaxHardwareVolumeStep.
	// PARAMS
	//	requestedVolume	- The new computed volume.
	//	previousVolume	- The volume calculated previously.
	//	ShouldSmooth	- If true, the difference will be clamped, otherwise requestedVolume is returned.
	// RETURNS
	//	The volume after change clamping.
	//

	static inline f32 SmoothVolume(const f32 requestedVolume, const f32 previousVolume, const bool shouldSmooth)
	{
		if(!shouldSmooth)
		{
			return requestedVolume;
		}
		else
		{
			const f32 volumeDiff = requestedVolume - previousVolume;
			// compute smoothedVol as vol +/- maxStep based on volumeDiff 
			const f32 smoothedVol =  Selectf(volumeDiff, previousVolume + g_MaxHardwareVolumeStep, previousVolume - g_MaxHardwareVolumeStep);
			// if volumeDiff is greater than or equal to max step return smoothed vol, otherwise just return requested volume
			return Selectf(fabs(volumeDiff) - g_MaxHardwareVolumeStep, smoothedVol, requestedVolume);
		}
	}

	static inline void SmoothVolumeV(Vec::Vector_4V_InOut requestedVolume, Vec::Vector_4V_In previousVolume)
	{
		const Vec::Vector_4V volumeDiff = Vec::V4Subtract(requestedVolume, previousVolume);
		//0.067f
		const Vec::Vector_4V maxVolStep = Vec::V4VConstant<0x3D89374B,0x3D89374B,0x3D89374B,0x3D89374B>();
		const Vec::Vector_4V smoothedVol = Vec::V4SelectFT(Vec::V4IsGreaterThanOrEqualV(Vec::V4VConstant(V_ZERO), volumeDiff),
														Vec::V4Add(previousVolume, maxVolStep),
														Vec::V4Subtract(previousVolume, maxVolStep));
		requestedVolume = Vec::V4SelectFT(Vec::V4IsGreaterThanOrEqualV(Vec::V4VConstant(V_ZERO), Vec::V4Subtract(Vec::V4Abs(volumeDiff), maxVolStep)),
			smoothedVol, requestedVolume);
	}

	static inline f32 SmoothCutoff(const f32 requestedCutoff, const f32 previousCutoff, const bool shouldSmooth)
	{
		Assert(FPIsFinite(requestedCutoff));
		Assert(FPIsFinite(previousCutoff));
		Assert(!shouldSmooth || (requestedCutoff >= kVoiceFilterLPFMinCutoff - 1.f && requestedCutoff <= kVoiceFilterLPFMaxCutoff + 1.f));
		Assert(!shouldSmooth || (previousCutoff >= kVoiceFilterLPFMinCutoff - 1.f && previousCutoff <= kVoiceFilterLPFMaxCutoff + 1.f));
		if(!shouldSmooth)
		{
			return requestedCutoff;
		}
		else
		{
			const f32 maxCutoff = (previousCutoff * g_MaxHardwareCutoffFactor);
			const f32 minCutoff = (previousCutoff * g_OneOverMaxHardwareCutoffFactor);

			const f32 diff = requestedCutoff - previousCutoff;

			// if diff is greater than or equal to max step return smoothed cutoff, otherwise just return requested cutoff
			const f32 cutoffGoingDown = Max(minCutoff, requestedCutoff);
			const f32 cutoffGoingUp = Min(maxCutoff, requestedCutoff);

			return Selectf(diff, cutoffGoingUp, cutoffGoingDown);
		}
	}

	// PURPOSE
	//	Like vectormath::Vec::V4Pow, but with similar accuracy across platforms
	static Vec::Vector_4V_Out V4Pow(Vec::Vector_4V_In v1, Vec::Vector_4V_In v2)
	{
#if __XENON
		return XMVectorPow(v1, v2);		
#else
		return Vec::V4Pow(v1, v2);
#endif
	}

	// PURPOSE
	//	Like vectormath::Vec::V4Expt, but with similar accuracy across platforms
	static inline Vec::Vector_4V_Out V4Exp2(Vec::Vector_4V_In v1)
	{
#if __XENON
		return XMVectorPow(Vec::V4VConstant(V_TWO), v1);		
#elif AUD_AVX_TRANSCENDENTALS

		return _XMVectorExp2(v1);
#else
		return Vec::V4Pow(Vec::V4VConstant(V_TWO), v1);
#endif
	}

	// PURPOSE
	//	Like vectormath::Vec::V4Pow, but e^v1 with similar accuracy across platforms
	static inline Vec::Vector_4V_Out V4ExpE(Vec::Vector_4V_In v1)
	{
#if !AUD_AVX_TRANSCENDENTALS
		const Vec::Vector_4V M_E_vec = Vec::V4VConstant<0x402DF854,0x402DF854,0x402DF854,0x402DF854>(); // 2.71828183
#endif
#if __XENON
		return XMVectorPow(M_E_vec, v1);		
#elif AUD_AVX_TRANSCENDENTALS
		return _XMVectorExpE(v1);
#else
		return Vec::V4Pow(M_E_vec, v1);
#endif
	}

private:

};

} // rage namespace

#endif // AUD_DRIVER_UTIL_H
