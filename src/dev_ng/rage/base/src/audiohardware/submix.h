// 
// audiohardware/submix.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SUBMIX_H
#define AUD_SUBMIX_H

#include "cmdbuffer.h"
#include "dspeffect.h"
#include "framebuffercache.h"
#include "framebufferpool.h"
#include "mixer.h"

#include "atl/array.h"
#include "vectormath/vectormath.h"

namespace rage
{
struct audDspEffectBuffer;
class audMixerConnection;
class audDspEffect;
class fiStream;
struct audCommandPacketHeader;

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324)
#endif

// PURPOSE
//	Defines a software mixed submix
class ALIGNAS(16) audMixerSubmix
{
public:

	enum {kMaxSubmixInputs = 32};
	enum {kMaxSubmixOutputs = 8};
	enum {kMaxEffectsPerSubmix = 8};
	enum {kMaxSubmixChannels = 8};

	enum SubmixFlag
	{
		InterleavedProcessing = 0,
		ProcessAsSourceEffect,
		ComputeRMS,
		ComputePeakAmplitude,
		kNumSubmixFlags
	};

	friend class audMixerDevice;

	void Init(const u32 submixId, const u32 numChannels, const bool hasVoiceInputs);
	void Shutdown();


	// PURPOSE
	//	Routes the output of this submix into the specified submix
	//	PARAMS
	//	submix - the submix to route into
	//	allocateVolumeMatrices - SetOutputVolumes() is only available if volume matrices are allocated
	//							for this connection.
	void AddOutput(u32 submixIndex, const bool allocateVolumeMatrices, const bool recomputeProcessingGraph = true);
	void DisconnectOutput(u32 submixIndex, const bool recomputeProcessingGraph = true);
	void DisconnectAllOutputs();

	// PURPOSE
	//	Sets the specified DSP effect to the submix effect chain
	// PARAMS
	//	slotId		- The DSP effect slot to store this effect in
	//	effect		- The DSP effect to be added.
	//	metadata	- The metadata associated with the high-level managed effect.
	void SetEffect(const s32 slotId, audDspEffect *effect, const u32 channelMask) const;

	// PURPOSE
	//	Sets the specified parameter on the specified effect via the command buffer
	void SetEffectParam(const s32 slotId, const u32 paramId, const u32 val) const;
	void SetEffectParam(const s32 slotId, const u32 paramId, const s32 val) const
	{
		SetEffectParam(slotId,paramId,(u32)val);
	}
	void SetEffectParam(const s32 slotId, const u32 paramId, const f32 val) const;
	void SetEffectParam(const s32 slotId, const u32 paramId, const bool val) const
	{
		SetEffectParam(slotId, paramId, val ? 1U : 0U);
	}

	// PURPOSE
	//	Sets the specific flag, via the command buffer
	void SetFlag(const SubmixFlag flag, const bool state);

	// PURPOSE
	//	Mixes the specified number of samples into the destination buffer
	// NOTES
	//	Mixes with whatever is currently in the dest buffer
	void MixIntoSubmix(audMixerSubmix *destSubmix, audMixerConnection *connection);

	// PURPOSE
	//	Mixes all connected audio submixes into mix buffer and runs DSP
	void Mix(SPU_ONLY(u8 *scratchSpace, u32 scratchSpaceSize));


	// PURPOSE
	//	Runs any associated DSP effects on mix buffer contents
	void ProcessDSP(SPU_ONLY(u8 *scratchSpace, u32 scratchSpaceSize));

	// PURPOSE
	//	Called at the end of the mixer frame
	void PostMix();

	// PURPOSE
	//	Sets the gain for the specified output index
	// NOTES
	//	volumes must be 16-byte aligned
	void SetOutputVolumes(const u32 index, const f32 *volumes) const;
	
	u32 GetNumChannels() const
	{
		return m_NumChannels;
	}

	bool IsSourceEffectsSubmix() const
	{
		return m_SourceEffectSubmix;
	}

	bool HasAllocatedBuffers() const
	{
		return m_HaveAllocatedBuffers;
	}

#if __BANK
	void SetDumpOutput(const char *fileName)
	{
		m_DumpOutputFile = fileName;
	}

	bool IsRecordingOutput() const { return m_DumpOutputFile != NULL; }
#else
	bool IsRecordingOutput() const { return false; }
#endif

#if AUD_SUPPORT_METERING
	void SetName(const char *name)
	{
		m_SubmixName = name;
	}
	const char *GetName() const { return m_SubmixName; }
#endif

	u32 GetMixBufferId(const u32 channelIndex) const
	{
		FastAssert(channelIndex < m_NumChannels);
		return m_BufferIds[channelIndex];
	}

	bool HasMixBuffers() const
	{
		for(u32 i = 0; i < m_NumChannels; i++)
		{
			if(GetMixBufferId(i) == audFrameBufferPool::InvalidId)
			{
				return false;
			}			
		}
		return true;
	}
	f32 *GetMixBuffer(const u32 channelIndex)
	{		
		return g_FrameBufferCache->GetBuffer(GetMixBufferId(channelIndex));
	}

	u32 GetSubmixId() const { return m_SubmixId; }
	

	void AllocateBuffers();
	void NotifyCache();

	enum audSubmixCommands
	{
		SET_VOLUMES = 0,
		SET_EFFECT,
		SET_EFFECT_PARAM_U32,
		SET_EFFECT_PARAM_F32,
		SET_FLAG,
		ADD_OUTPUT,
		DISCONNECT_OUTPUT,
	};

	s32 GetProcessingStage() const
	{
		return m_ProcessingStage;
	}

	void SetProcessingStage(const s32 stage)
	{
		m_ProcessingStage = stage;
	}

	u32 GetNumSubmixInputs()
	{
		return m_NumSubmixInputs;
	}

	u32 GetNumSubmixOutputs()
	{
		return m_NumSubmixOutputs;
	}

	s32 ComputeLastOutputStage();
	s32 GetLastOutputStage() const{ return m_FinalOutputProcessingStage; }


	static audMixerSubmix *GetSubmixFromIndex(const s32 index);

	void SetShouldKeepBuffers(const bool should) { m_KeepBuffers = should; }
	void SetBufferId(const u32 channelIndex, const u32 bufferId) { Assign(m_BufferIds[channelIndex], bufferId); }

	bool HasVoiceInputs() const { return m_HasVoiceInputs; }
	u32 GetVoiceCount() const { return m_NumActiveVoiceInputs; }

#if AUD_SUPPORT_METERING
	f32 GetAndResetPeakAmplitude(const u32 channelIndex) 
	{ 
		const f32 ret = m_PeakAmplitude[channelIndex]; 
		m_PeakAmplitude[channelIndex] = 0.f; 
		return ret; 
	}

	float GetMaxPeakAmplitude(const u32 channelIndex)
	{
		return m_MaxPeakAmplitude[channelIndex];
	}

	f32 GetAndResetRmsAmplitude(const u32 channelIndex) 
	{
		const f32 ret = m_RmsAmplitude[channelIndex];
		m_RmsAmplitude[channelIndex] = 0.f;
		return ret; 	
	}

	f32 GetAndResetMeanAmplitude(const u32 channelIndex) 
	{
		const f32 ret = m_MeanAmplitude[channelIndex];
		m_MeanAmplitude[channelIndex] = 0.f;
		return ret; 	
	}

	f32 GetAndResetEffectInputLevel(const u32 effectIndex)
	{
		const f32 ret = m_EffectInputLevel[effectIndex];
		m_EffectInputLevel[effectIndex] = 0.f;
		return ret;
	}

	float GetMaxEffectInputLevel(const u32 effectIndex) const
	{
		return m_EffectMaxInputLevel[effectIndex];
	}

	audDspEffectId GetEffectTypeId(const u32 effectIndex)
	{
		return m_EffectType[effectIndex];
	}

#endif // AUD_SUPPORT_METERING

#if __BANK && !__SPU
	void DumpMixBufferToDisk();
#endif

	audMixBufferFormat GetBufferFormat() const { return m_BufferFormat; }

	static void ConvertBufferToFormat(audDspEffectBuffer &bufferDesc, const audMixBufferFormat format);

	void IncrementVoiceCount()
	{
		m_NumActiveVoiceInputs++;
	}

	float GetRMS() const { return m_RMSValue; }

private:

	bool WriteAlignedCommand(audCommandPacketHeader *packet) const;
	bool WriteCommand(audCommandPacketHeader *packet) const;
	void ExecuteCommand(const audCommandPacketHeader *packet);
	void DisconnectOutputInternal(s32 index, const bool recomputeProcessingGraph);
	void RemoveInput(u32 connectionID);

	void SetOutputVolumesInternal(const u32 index, Vec::Vector_4V_In packedVolumes);
	void SetEffectInternal(const s32 slotId, audDspEffect *effect, const u32 channelMask);
	void SetFlagInternal(const SubmixFlag flag, const bool state);
	void AddOutputInternal(audMixerSubmix *submix, const bool allocateVolumeMatrices, const bool recomputeProcessingGraph);

	// PURPOSE
	//	Adds a submix as an input to this submix
	// PARAMS
	// RETURNS
	//	true if successful, false if input list is full
	bool AddInput(const u32 connectionId, bool recomputeProcessingGraph);

#if AUD_SUPPORT_METERING
	ALIGNAS(16) atRangeArray<f32, kMaxSubmixChannels> m_PeakAmplitude;
	ALIGNAS(16) atRangeArray<f32, kMaxSubmixChannels> m_MaxPeakAmplitude;
	ALIGNAS(16) atRangeArray<f32, kMaxSubmixChannels> m_RmsAmplitude;
	ALIGNAS(16) atRangeArray<f32, kMaxSubmixChannels> m_MeanAmplitude;
	ALIGNAS(16) atRangeArray<f32, kMaxEffectsPerSubmix> m_EffectInputLevel;
	ALIGNAS(16) atRangeArray<f32, kMaxEffectsPerSubmix> m_EffectMaxInputLevel;
	atRangeArray<audDspEffectId, kMaxEffectsPerSubmix> m_EffectType;
	const char *m_SubmixName;
#endif

	float m_RMSValue;

	atRangeArray<u16, kMaxSubmixInputs> m_SubmixInputConnectionIds;	
	atRangeArray<u16, kMaxSubmixOutputs> m_OutputConnectionIds;	

	atRangeArray<audDspEffect*, kMaxEffectsPerSubmix> m_Effects;
	atRangeArray<u32, kMaxEffectsPerSubmix> m_EffectSizes;
	atRangeArray<u16, kMaxSubmixChannels> m_BufferIds;
		
	u32 m_NumSubmixInputs, m_NumSubmixOutputs;
	u32 m_NumChannels;
	u32 m_SubmixId;

	u32 m_NumActiveVoiceInputs;

	s32 m_ProcessingStage;
	s32 m_FinalOutputProcessingStage;

	audMixBufferFormat m_BufferFormat;
	bool m_SourceEffectSubmix;

	bool m_HavePostMixed, m_HaveMixed, m_KeepBuffers, m_HaveAllocatedBuffers, m_HasVoiceInputs, m_HaveProcessedDSP;
	bool m_ComputeRMS, m_ComputePeakAmplitude;
#if __BANK
	const char *m_DumpOutputFile;
	atRangeArray<u32, kMaxSubmixChannels> m_NumOutputDataBytes;
	atRangeArray<fiStream *, kMaxSubmixChannels> m_OutputStreams;
	fiStream *m_TimeCodeOutput;
#endif

};

struct audSetSubmixVolumesCommand : public audCommandPacketHeader
{
	audSetSubmixVolumesCommand()
	{
		packetSize = sizeof(audSetSubmixVolumesCommand);
		commandId = audMixerSubmix::SET_VOLUMES;
	}
	u32 index;
	u32 pad0;
	u32 pad1;
	Vec::Vector_4V packedVolumes;
};

#if __WIN32
#pragma warning(pop)
#endif
}

#endif // AUD_SUBMIX_H
