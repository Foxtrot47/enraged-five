//
// audiohardware/waveplayer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_WAVEPLAYER_H
#define AUD_WAVEPLAYER_H

#include "cmdbuffer.h"
#include "decoder.h"
#include "pcmsource.h"
#include "syncsource.h"
#include "waveref.h"

#include "system/performancetimer.h"

#define AUD_WAVEPLAYER_METERING 0

namespace rage
{
	struct audStreamingWaveBlock;
	struct audWaveMetadata;
	class audStreamingWaveSlot;
	// PURPOSE
	//	Encapsulates wave playback functionality - decoding, sample rate conversion, looping, etc.
	class audWavePlayer : public audPcmSource
	{
	public:
		friend class audMixerVoice;

		audWavePlayer();

		struct Params
		{
			static const u32 StartOffsetSamples = 0xEAC004F2;
			static const u32 DelaySamples = 0x85B03DE1;
			static const u32 WaveReference = 0xB14F6259;
			static const u32 SyncTriggerTimeSamples = 0x4850211B;
		};
		
		// PURPOSE
		//	Gets current sample rate
		// RETURNS
		//	Current playback sample rate in Hz
		u32 GetPlaybackFrequency() const
		{
			return (u32)((s32)m_BaseFrequency + m_PlaybackFrequencyOffset);
		}

		// PURPOSE
		//	Returns the asset sample rate
		u32 GetAuthoredSampleRate() const
		{
			return m_BaseFrequency;
		}

		// PURPOSE
		//	Allows code to query if the buffer is looping
		// RETURNS
		//	true if this buffer is looping, false otherwise
		AUD_PCMSOURCE_VIRTUAL bool IsLooping()  const
		{
			return m_IsLooping;
		}

		// PURPOSE
		//	Returns the length of the wave in samples at the asset sample rate
		AUD_PCMSOURCE_VIRTUAL s32 GetLengthSamples() const;

		// PURPOSE
		//	Returns the current playback position of the wave, in samples at the asset
		//	sample rate.
		AUD_PCMSOURCE_VIRTUAL u32 GetPlayPositionSamples() const;

		// PURPOSE
		//	Returns true the end of the wave has been reached
		AUD_PCMSOURCE_VIRTUAL bool IsFinished() const { return m_FinishedPlayback; }

		// PURPOSE
		//	Returns true if the generator has started playback
		AUD_PCMSOURCE_VIRTUAL bool HasStartedPlayback() const { return m_PlayState != kStateIdle; }

		AUD_PCMSOURCE_VIRTUAL f32 GetHeadroom() const;

		enum audWavePlayerDecodeState
		{
			DECODE_IDLE,
			DECODE_SUBMITTED,
			DECODE_WAITING,
			DECODE_RECV_DATA,
			DECODE_FINISHED,
			DECODE_ERROR,
		};

		const char *GetBankName() const;
		u32 GetWaveNameHash() const;
		u32 GetWaveSlotId() const;

		bool HasValidWave() const
		{
			return m_WaveRef.HasValidWave();
		}

		// runs on SPU
		AUD_PCMSOURCE_VIRTUAL void GenerateFrame();
		AUD_PCMSOURCE_VIRTUAL void SkipFrame();

		// runs on PPU
		AUD_PCMSOURCE_VIRTUAL void BeginFrame();
		AUD_PCMSOURCE_VIRTUAL void EndFrame();

		// Start and Stop occurs on the PPU
		AUD_PCMSOURCE_VIRTUAL void Start();
		AUD_PCMSOURCE_VIRTUAL void Stop();

		AUD_PCMSOURCE_VIRTUAL void StartPhys(s32 channel);
		AUD_PCMSOURCE_VIRTUAL void StopPhys(s32 channel);

		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const u32 val);
		AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const f32 val);

		AUD_PCMSOURCE_VIRTUAL void SumbitDataToDecoder();
		
		AUD_PCMSOURCE_VIRTUAL bool ProcessSyncSignal(const audMixerSyncSignal &signal);

		void Shutdown();

		const s16 *GetWaveDataPtr() const
		{
			return m_WaveDataPtr;
		}

		s32 GetPlayState() const { return m_PlayState; }
		u32 GetDecodeState() const { return m_DecodeState; }

		// PURPOSE
		//	Returns peak level in range [0,65535]
		AUD_PCMSOURCE_VIRTUAL u32 GetCurrentPeakLevel() const { return m_CurrentPeakLevelSample; }

#if AUD_WAVEPLAYER_METERING
		float GetPeakLevel() const { return m_PeakLevel; }
#endif

#if !__FINAL
		AUD_PCMSOURCE_VIRTUAL void GetAssetInfo(audPcmSource::AssetInfo &assetInfo) const;
#endif

	private:

		void SubmitBlock(const audStreamingWaveBlock &block, const s32 startOffsetSamples = -1);

		u32 SamplesToPeakIndex(const u32 samples) const
		{
			return samples >> 12;
		}
		
		void ComputeCurrentPeak();

		bool EvaluatePredelay();

		void SetDelaySamples(const u32 delaySamples);

#if !__SPU
		audStreamingWaveSlot *GetStreamSlot() const { return (audStreamingWaveSlot*)m_WaveRef.GetSlot(); }
#endif
		void Init(const u32 waveRefU32);

		// PURPOSE
		//	Sets the current play position in milliseconds
		void SetPlayPositionMs(const u32 startOffsetMs);

		// PURPOSE
		//	Sets the current play position in samples
		void SetPlayPositionSamples(const u32 startOffsetSamples);

#if __XENON
		void ComputeXMAPacketOffsetBitsAndSampleIndexFromSamples(u32 sampleOffset, u32 &frameOffsetBits,
			s32 &sampleIndex);
#elif __PS3
		void ComputeMP3PacketOffsetFromSamples(u32 sampleOffset, u32 &frameOffsetBytes, u32 &subFrameSampleOffset);
#endif

#if __SPU
		void FetchPCMSamples(s16 *dest, const s16 *src, const s32 numSamples, const s32 dmaTag);
#endif


		// PURPOSE
		//	Sets playback frequency scaling factor
		void SetFrequencyScalingFactor(f32 frequencyScalingFactor);


		// PURPOSE
		//	returns pointers to sample data, wrapping if necessary
		// PARAMS
		//	numSamples		-	number of samples required
		//	outPtr1			-	This receives a pointer to the first chunk of samples
		//	outSampleCount1	-	This is set to the number of samples that outPtr1 points to
		//	outPtr2			-	This receives a pointer to the second chunk of samples
		//	outSampleCount2	-	This is set to the number of samples that outPtr2 points to
		void GetNextSamples(u32 numSamples, const s16 *&outPtr1, u32 &outSampleCount1, const s16 *&outPtr2, u32 &outSampleCount2);

		// PURPOSE
		//	adds numSamples to the current buffer position, wrapping to the loop start offset
		//	if this buffer is looping
		// RETURNS
		//	The number of samples consumed (could be less than numSamples when the end of the wave is reached)
		u32 AddToCurrentPosition(u32 numSamples, bool updateState);

		bool IsMoreDataAvailable() const
		{
			return IsLooping() || (m_PlayPositionSamples < m_WaveLengthSamples);
		}

		audDecoder *m_Decoder;
		audWaveRef m_WaveRef;
		const audWaveFormat *m_WaveFormat;

		const s16 *m_WaveDataPtr;

		const u16 *m_PeakData;
		u32 m_NumPeakSamples;
		u32 m_SyncTriggerTimeSamples;
		u16 m_FirstPeakLevelSample;
		u16 m_CurrentPeakLevelSample;
		
		u32 m_PlayPositionSamples;
		
		f32 m_SrcTrailingFrac;

		s32 m_PlaybackFrequencyOffset;
		u16 m_BaseFrequency;

		s16 m_StreamClientId;

		enum { kStateIdle,kStatePlaying,kStateStopped } m_PlayState;

		u32 m_WaveLengthSamples;
		u32 m_WaveLengthBytes;
		s32 m_LoopStartOffsetSamples;

		f32 m_WaveHeadroom;

#if AUD_WAVEPLAYER_METERING
		f32 m_PeakLevel;
#endif

		u32 m_PredelayFrames;
		
		u32 m_FramesWaiting;
		u32 m_FramesWaitingForStreaming;
		u32 m_FramesStarved;

		u32 m_PredelaySamples : 8;
		u32 m_DecodeDelay : 9;
		u32 m_Format : 4;
		u32 m_DecodeState : 3;
		u32 m_FinishedPlayback : 1;
		u32 m_IsDecoded : 1;
		u32 m_IsLooping : 1;
		u32 m_IsStreaming : 1;
		u32 m_HasBegunFrame : 1;
		u32 m_HasEndedFrame : 1;
		bool m_IsStarved : 1;
		bool m_HadFinalBlock : 1;
		bool m_StopRequested : 1;
		bool m_IsPauseRequested : 1;
		bool m_IsPaused : 1;
		bool m_InitialDataSubmittedToDecoder  : 1;
	};
}

#endif // AUD_WAVEPLAYER_H
