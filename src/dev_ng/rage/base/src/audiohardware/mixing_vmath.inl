//
// audiohardware/mixing_vmath.inl
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_MIXING_VMATH_INL
#define AUD_MIXING_VMATH_INL
#include "system/cache.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	template<const u32 numSamples> __forceinline void MixMonoBufToMonoBuf(const f32 *RESTRICT src, f32 *RESTRICT dest, const Vector_4V oldGain, const Vector_4V newGain)
	{		
		if (!src || !dest)
			return;

		AlignedAssert(src, 16);
		AlignedAssert(dest, 16);

		const Vector_4V oneOverNumSamples = V4VConstantSplat<numSamples == 256 ? 0x3B800000 : 0x3c000000>(); // 1/256 or 1/128
		CompileTimeAssert(numSamples == 256 || numSamples == 128);

		// Process one cacheline (32 samples) at a time, with prefetching

		Vector_4V *RESTRICT srcPtr = (Vector_4V*)src;
		Vector_4V *RESTRICT destPtr = (Vector_4V*)dest;

		// Prefetch the first block of samples
#if __XENON
		PrefetchDC(srcPtr);
		PrefetchDC(destPtr);
#endif

		// (0,1,2,3)
		const Vector_4V scaleStep = V4VConstant<0,0x3F800000,0x40000000,0x40400000>();

		const Vector_4V step = V4Scale(V4Subtract(newGain, oldGain), oneOverNumSamples);
		//const Vector_4V multStepX32 = V4Scale(step, V4VConstant<0x42000000,0x42000000,0x42000000,0x42000000>());
		const Vector_4V multStepX8 = V4Scale(step, V4VConstant(V_EIGHT));

		Vector_4V mult1 = V4Add(oldGain, V4Scale(step,scaleStep));
		Vector_4V mult2 = V4Add(oldGain, V4Scale(V4VConstant(V_TWO), V4Scale(step,scaleStep)));
		
		CompileTimeAssert((numSamples&31) == 0);
		for(u32 i = 0; i < numSamples; i += 32)
		{
			// Prefetch the next block
#if __XENON
			PrefetchDC2(srcPtr, 128);
			PrefetchDC2(destPtr, 128);
#endif

			Vector_4V input1 = *srcPtr++;
			Vector_4V output1 = *(destPtr);
			Vector_4V input2 = *srcPtr++;
			Vector_4V output2 = *(destPtr+1);
			Vector_4V input3 = *srcPtr++;
			Vector_4V output3 = *(destPtr+2);
			Vector_4V input4 = *srcPtr++;
			Vector_4V output4 = *(destPtr+3);
			Vector_4V input5 = *srcPtr++;
			Vector_4V output5 = *(destPtr+4);
			Vector_4V input6 = *srcPtr++;
			Vector_4V output6 = *(destPtr+5);
			Vector_4V input7 = *srcPtr++;
			Vector_4V output7 = *(destPtr+6);
			Vector_4V input8 = *srcPtr++;
			Vector_4V output8 = *(destPtr+7);


			// apply interpolated scalar
			output1 = V4AddScaled(output1, input1, mult1);
			// step through gain interpolation
			mult1 = V4Add(mult1, multStepX8);
			output2 = V4AddScaled(output2, input2, mult2);
			// step through gain interpolation
			mult2 = V4Add(mult2, multStepX8);

			// apply interpolated scalar
			output3 = V4AddScaled(output3, input3, mult1);
			// step through gain interpolation
			mult1 = V4Add(mult1, multStepX8);
			output4 = V4AddScaled(output4, input4, mult2);
			// step through gain interpolation
			mult2 = V4Add(mult2, multStepX8);

			// apply interpolated scalar
			output5 = V4AddScaled(output5, input5, mult1);
			// step through gain interpolation
			mult1 = V4Add(mult1, multStepX8);
			output6 = V4AddScaled(output6, input6, mult2);
			// step through gain interpolation
			mult2 = V4Add(mult2, multStepX8);

			// apply interpolated scalar
			output7 = V4AddScaled(output7, input7, mult1);
			// step through gain interpolation
			mult1 = V4Add(mult1, multStepX8);
			output8 = V4AddScaled(output8, input8, mult2);
			// step through gain interpolation
			mult2 = V4Add(mult2, multStepX8);

			*destPtr++ = output1;
			*destPtr++ = output2;
			*destPtr++ = output3;
			*destPtr++ = output4;
			*destPtr++ = output5;
			*destPtr++ = output6;
			*destPtr++ = output7;
			*destPtr++ = output8;
		}		
	}

	template<const u32 numSamples> __forceinline void MixMonoBufToMonoBuf(const f32 *RESTRICT src, f32 *RESTRICT dest, const Vector_4V gain)
	{
		if (!src || !dest)
			return;

		Assert((((size_t)src)&15)==0);
		Assert((((size_t)dest)&15)==0);

		// Process one cacheline (32 samples) at a time, with prefetching

		Vector_4V *RESTRICT srcPtr = (Vector_4V*)src;
		Vector_4V *RESTRICT destPtr = (Vector_4V*)dest;

		// Prefetch the first block of samples
#if __XENON
		PrefetchDC(srcPtr);
		PrefetchDC(destPtr);
#endif

		CompileTimeAssert((numSamples&31) == 0);
		for(u32 i = 0; i < numSamples; i += 32)
		{
			// Prefetch the next block
#if __XENON
			PrefetchDC2(srcPtr, 128);
			PrefetchDC2(destPtr, 128);
#endif

			Vector_4V input1 = *srcPtr++;
			Vector_4V output1 = *(destPtr);
			Vector_4V input2 = *srcPtr++;
			Vector_4V output2 = *(destPtr+1);
			Vector_4V input3 = *srcPtr++;
			Vector_4V output3 = *(destPtr+2);
			Vector_4V input4 = *srcPtr++;
			Vector_4V output4 = *(destPtr+3);
			Vector_4V input5 = *srcPtr++;
			Vector_4V output5 = *(destPtr+4);
			Vector_4V input6 = *srcPtr++;
			Vector_4V output6 = *(destPtr+5);
			Vector_4V input7 = *srcPtr++;
			Vector_4V output7 = *(destPtr+6);
			Vector_4V input8 = *srcPtr++;
			Vector_4V output8 = *(destPtr+7);


			// apply interpolated scalar
			output1 = V4AddScaled(output1, input1, gain);			
			output2 = V4AddScaled(output2, input2, gain);
			output3 = V4AddScaled(output3, input3, gain);			
			output4 = V4AddScaled(output4, input4, gain);			
			output5 = V4AddScaled(output5, input5, gain);			
			output6 = V4AddScaled(output6, input6, gain);			
			output7 = V4AddScaled(output7, input7, gain);		
			output8 = V4AddScaled(output8, input8, gain);

			*destPtr++ = output1;
			*destPtr++ = output2;
			*destPtr++ = output3;
			*destPtr++ = output4;
			*destPtr++ = output5;
			*destPtr++ = output6;
			*destPtr++ = output7;
			*destPtr++ = output8;
		}
	}

	template<const u32 numSamples> __forceinline void MixMonoBufToMonoBuf(const f32 *RESTRICT src, f32 *RESTRICT dest)
	{
		Assert((((size_t)src)&15)==0);
		Assert((((size_t)dest)&15)==0);

		if (!src || !dest)
			return;

		// Process one cacheline (32 samples) at a time, with prefetching

		Vector_4V *RESTRICT srcPtr = (Vector_4V*)src;
		Vector_4V *RESTRICT destPtr = (Vector_4V*)dest;

		// Prefetch the first block of samples
#if __XENON
		PrefetchDC(srcPtr);
		PrefetchDC(destPtr);
#endif

		CompileTimeAssert((numSamples&31) == 0);
		for(u32 i = 0; i < numSamples; i += 32)
		{
			// Prefetch the next block
#if __XENON
			PrefetchDC2(srcPtr, 128);
			PrefetchDC2(destPtr, 128);
#endif

			Vector_4V input1 = *srcPtr++;
			Vector_4V output1 = *(destPtr);
			Vector_4V input2 = *srcPtr++;
			Vector_4V output2 = *(destPtr+1);
			Vector_4V input3 = *srcPtr++;
			Vector_4V output3 = *(destPtr+2);
			Vector_4V input4 = *srcPtr++;
			Vector_4V output4 = *(destPtr+3);
			Vector_4V input5 = *srcPtr++;
			Vector_4V output5 = *(destPtr+4);
			Vector_4V input6 = *srcPtr++;
			Vector_4V output6 = *(destPtr+5);
			Vector_4V input7 = *srcPtr++;
			Vector_4V output7 = *(destPtr+6);
			Vector_4V input8 = *srcPtr++;
			Vector_4V output8 = *(destPtr+7);


			// apply interpolated scalar
			output1 = V4Add(output1, input1);			
			output2 = V4Add(output2, input2);
			output3 = V4Add(output3, input3);			
			output4 = V4Add(output4, input4);			
			output5 = V4Add(output5, input5);			
			output6 = V4Add(output6, input6);			
			output7 = V4Add(output7, input7);		
			output8 = V4Add(output8, input8);
			
			*destPtr++ = output1;
			*destPtr++ = output2;
			*destPtr++ = output3;
			*destPtr++ = output4;
			*destPtr++ = output5;
			*destPtr++ = output6;
			*destPtr++ = output7;
			*destPtr++ = output8;
		}
	}

	template<u32 numSamples> void InterleaveAndMix_1to4(const float *src, float *dest, const Vector_4V startGain, const Vector_4V gainStep)
	{
		Assert((((size_t)src)&15)==0);
		Assert((((size_t)dest)&15)==0);
	
		if (!src || !dest)
			return;

		Vector_4V gain = startGain;

		const Vector_4V *RESTRICT srcPtr = reinterpret_cast<const Vector_4V*>(src);
		Vector_4V *RESTRICT destPtr = reinterpret_cast<Vector_4V*>(dest);

		for(u32 i = 0; i < numSamples; i += 4)
		{
			// Load four samples
			const Vector_4V inputV = *srcPtr++;
			// first input sample to four destination samples
			*destPtr = V4AddScaled(*destPtr, V4SplatX(inputV), gain);
			destPtr++;
			gain = V4Add(gain, gainStep);
			*destPtr = V4AddScaled(*destPtr, V4SplatY(inputV), gain);
			destPtr++;
			gain = V4Add(gain, gainStep);
			*destPtr = V4AddScaled(*destPtr, V4SplatZ(inputV), gain);
			destPtr++;
			gain = V4Add(gain, gainStep);
			*destPtr = V4AddScaled(*destPtr, V4SplatW(inputV), gain);
			destPtr++;
			gain = V4Add(gain, gainStep);
		}		
	}

	template<u32 numSamples> void InterleaveAndMix_1to4(const Vector_4V prevGain, const Vector_4V newGain, 
															const float *src,
															float *dest0,
															float *dest1,
															float *dest2,
															float *dest3)
	{

		if (!src || !dest0 || !dest1 || !dest2 || !dest3)
			return;
		
		const u32 numSamplesPerChunk = numSamples / 4;

		CompileTimeAssert(numSamples == 256 || numSamples == 128);
		const Vector_4V oneOverNumSamples = V4VConstantSplat<numSamples == 256 ? 0x3B800000 : 0x3c000000>(); // 1/256 or 1/128
		const Vector_4V oneOverNumChunks = V4VConstantSplat<0x3E800000>(); // 1/4

		Vector_4V gain = prevGain;
		Vector_4V gainDelta = V4Subtract(newGain, prevGain);

		Vector_4V gainStep = V4Scale(gainDelta, oneOverNumSamples);
		Vector_4V gainStepPerChunk = V4Scale(gainDelta, oneOverNumChunks);

#if RSG_XENON
		PrefetchDC2(dest0, 0);
		PrefetchDC2(dest0, 128);
		PrefetchDC2(dest0, 256);
		PrefetchDC2(dest0, 384);
		PrefetchDC2(dest0, 512);
		PrefetchDC2(dest0, 640);
		PrefetchDC2(dest0, 768);
		PrefetchDC2(dest0, 896);

		PrefetchDC2(dest1, 0);
		PrefetchDC2(dest1, 128);
		PrefetchDC2(dest1, 256);
		PrefetchDC2(dest1, 384);
		PrefetchDC2(dest1, 512);
		PrefetchDC2(dest1, 640);
		PrefetchDC2(dest1, 768);
		PrefetchDC2(dest1, 896);
#endif

		InterleaveAndMix_1to4<numSamplesPerChunk>(
			src + numSamplesPerChunk*0,
			dest0,
			gain,
			gainStep
			);

		gain = V4Add(gain, gainStepPerChunk);

#if RSG_XENON
		PrefetchDC2(dest2, 0);
		PrefetchDC2(dest2, 128);
		PrefetchDC2(dest2, 256);
		PrefetchDC2(dest2, 384);
		PrefetchDC2(dest2, 512);
		PrefetchDC2(dest2, 640);
		PrefetchDC2(dest2, 768);
		PrefetchDC2(dest2, 896);
#endif
		InterleaveAndMix_1to4<numSamplesPerChunk>(
			src + numSamplesPerChunk*1,
			dest1,
			gain,
			gainStep
			);

		gain = V4Add(gain, gainStepPerChunk);

#if RSG_XENON
		PrefetchDC2(dest3, 0);
		PrefetchDC2(dest3, 128);
		PrefetchDC2(dest3, 256);
		PrefetchDC2(dest3, 384);
		PrefetchDC2(dest3, 512);
		PrefetchDC2(dest3, 640);
		PrefetchDC2(dest3, 768);
		PrefetchDC2(dest3, 896);
#endif
		InterleaveAndMix_1to4<numSamplesPerChunk>(
			src + numSamplesPerChunk*2,
			dest2,
			gain,
			gainStep
			);

		gain = V4Add(gain, gainStepPerChunk);

		InterleaveAndMix_1to4<numSamplesPerChunk>(
			src + numSamplesPerChunk*3,
			dest3,
			gain,
			gainStep
			);
	}

	template<u32 numSamples> void Interleave_4to4(float *dest, const float *src0, const float *src1, const float *src2, const float *src3)
	{
		AlignedAssert(dest, 16);
		AlignedAssert(src0, 16);
		AlignedAssert(src1, 16);
		AlignedAssert(src2, 16);
		AlignedAssert(src3, 16);
		CompileTimeAssert((numSamples&15) == 0);

		if (!src0 || !src1 || !src1 || !src2 || !src3 || !dest)
			return;

		Vector_4V *RESTRICT destPtr = reinterpret_cast<Vector_4V*>(dest);
		const Vector_4V *RESTRICT src0Ptr = reinterpret_cast<const Vector_4V*>(src0);
		const Vector_4V *RESTRICT src1Ptr = reinterpret_cast<const Vector_4V*>(src1);
		const Vector_4V *RESTRICT src2Ptr = reinterpret_cast<const Vector_4V*>(src2);
		const Vector_4V *RESTRICT src3Ptr = reinterpret_cast<const Vector_4V*>(src3);

		for(u32 i = 0; i < numSamples; i += 4)
		{
			const Vector_4V srcSamples0 = *src0Ptr++;
			const Vector_4V srcSamples1 = *src1Ptr++;
			const Vector_4V srcSamples2 = *src2Ptr++;
			const Vector_4V srcSamples3 = *src3Ptr++;

			Vector_4V srcXY01 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples0, srcSamples1);
			Vector_4V srcXY23 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples2, srcSamples3);

			*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(srcXY01, srcXY23);
			*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(srcXY01, srcXY23);
			
			Vector_4V srcZW01 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples0, srcSamples1);
			Vector_4V srcZW23 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples2, srcSamples3);

			*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(srcZW01, srcZW23);
			*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(srcZW01, srcZW23);
		}
	}

	template<u32 numSamples> void DeinterleaveAndMix_4to4(const float *src, float *dest0, float *dest1, float *dest2, float *dest3)
	{
		if (!src || !dest0 || !dest1 || !dest2 || !dest3)
			return;

		const Vector_4V *RESTRICT srcPtr = reinterpret_cast<const Vector_4V*>(src);
		Vector_4V *RESTRICT dest0Ptr = reinterpret_cast<Vector_4V*>(dest0);
		Vector_4V *RESTRICT dest1Ptr = reinterpret_cast<Vector_4V*>(dest1);
		Vector_4V *RESTRICT dest2Ptr = reinterpret_cast<Vector_4V*>(dest2);
		Vector_4V *RESTRICT dest3Ptr = reinterpret_cast<Vector_4V*>(dest3);

		for(u32 i = 0; i < numSamples; i += 16)
		{
			const Vector_4V srcSamples0 = *srcPtr++;
			const Vector_4V srcSamples1 = *srcPtr++;
			const Vector_4V srcSamples2 = *srcPtr++;
			const Vector_4V srcSamples3 = *srcPtr++;

			const Vector_4V srcSamplesFLFR01 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples0, srcSamples1);
			const Vector_4V srcSamplesRLRR01 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples0, srcSamples1);
			const Vector_4V srcSamplesFLFR23 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples2, srcSamples3);
			const Vector_4V srcSamplesRLRR23 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples2, srcSamples3);

			*dest0Ptr = V4Add(*dest0Ptr, V4PermuteTwo<X1,Y1,X2,Y2>(srcSamplesFLFR01, srcSamplesFLFR23));
			dest0Ptr++;
			*dest1Ptr = V4Add(*dest1Ptr, V4PermuteTwo<Z1,W1,Z2,W2>(srcSamplesFLFR01, srcSamplesFLFR23));
			dest1Ptr++;
			*dest2Ptr = V4Add(*dest2Ptr, V4PermuteTwo<X1,Y1,X2,Y2>(srcSamplesRLRR01, srcSamplesRLRR23));
			dest2Ptr++;
			*dest3Ptr = V4Add(*dest3Ptr, V4PermuteTwo<Z1,W1,Z2,W2>(srcSamplesRLRR01, srcSamplesRLRR23));
			dest3Ptr++;
		}
	}

	template<u32 numOutputSamples> void Deinterleave_4to4(const float *src, float *dest0, float *dest1, float *dest2, float *dest3)
	{		
		if (!src || !dest0 || !dest1 || !dest2 || !dest3)
			return;

		AlignedAssert(src, 16);
		AlignedAssert(dest0, 16);
		AlignedAssert(dest1, 16);
		AlignedAssert(dest2, 16);
		AlignedAssert(dest3, 16);
		CompileTimeAssert((numOutputSamples&15) == 0);

		const Vector_4V *RESTRICT srcPtr = reinterpret_cast<const Vector_4V*>(src);
		Vector_4V *RESTRICT dest0Ptr = reinterpret_cast<Vector_4V*>(dest0);
		Vector_4V *RESTRICT dest1Ptr = reinterpret_cast<Vector_4V*>(dest1);
		Vector_4V *RESTRICT dest2Ptr = reinterpret_cast<Vector_4V*>(dest2);
		Vector_4V *RESTRICT dest3Ptr = reinterpret_cast<Vector_4V*>(dest3);

		for(u32 i = 0; i < numOutputSamples; i += 4)
		{
			// load 16 input samples for every 4 (per channel) output samples 
			const Vector_4V srcSamples0 = *srcPtr++;
			const Vector_4V srcSamples1 = *srcPtr++;
			const Vector_4V srcSamples2 = *srcPtr++;
			const Vector_4V srcSamples3 = *srcPtr++;
			
			const Vector_4V srcSamplesFLFR01 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples0, srcSamples1);
			const Vector_4V srcSamplesRLRR01 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples0, srcSamples1);
			const Vector_4V srcSamplesFLFR23 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples2, srcSamples3);
			const Vector_4V srcSamplesRLRR23 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples2, srcSamples3);

			*dest0Ptr++ = V4PermuteTwo<X1,Y1,X2,Y2>(srcSamplesFLFR01, srcSamplesFLFR23);
			*dest1Ptr++ = V4PermuteTwo<Z1,W1,Z2,W2>(srcSamplesFLFR01, srcSamplesFLFR23);
			*dest2Ptr++ = V4PermuteTwo<X1,Y1,X2,Y2>(srcSamplesRLRR01, srcSamplesRLRR23);
			*dest3Ptr++ = V4PermuteTwo<Z1,W1,Z2,W2>(srcSamplesRLRR01, srcSamplesRLRR23);			
		}
	}

	template<u32 numSamples> void DeinterleaveAndMix_4to4(Vector_4V_In startGain, Vector_4V_In gainStep, const float *src, float *dest0, float *dest1, float *dest2, float *dest3)
	{
		if (!src || !dest0 || !dest1 || !dest2 || !dest3)
			return;

		AlignedAssert(src, 16);
		AlignedAssert(dest0, 16);
		AlignedAssert(dest1, 16);
		AlignedAssert(dest2, 16);
		AlignedAssert(dest3, 16);
		CompileTimeAssert((numSamples&15) == 0);

		const Vector_4V *RESTRICT srcPtr = reinterpret_cast<const Vector_4V*>(src);
		Vector_4V *RESTRICT dest0Ptr = reinterpret_cast<Vector_4V*>(dest0);
		Vector_4V *RESTRICT dest1Ptr = reinterpret_cast<Vector_4V*>(dest1);
		Vector_4V *RESTRICT dest2Ptr = reinterpret_cast<Vector_4V*>(dest2);
		Vector_4V *RESTRICT dest3Ptr = reinterpret_cast<Vector_4V*>(dest3);

		Vector_4V gain = startGain;

		for(u32 i = 0; i < numSamples; i += 16)
		{
			const Vector_4V srcSamples0 = V4Scale(gain, *srcPtr++);
			gain = V4Add(gain, gainStep);
			const Vector_4V srcSamples1 = V4Scale(gain, *srcPtr++);
			gain = V4Add(gain, gainStep);
			const Vector_4V srcSamples2 = V4Scale(gain, *srcPtr++);
			gain = V4Add(gain, gainStep);
			const Vector_4V srcSamples3 = V4Scale(gain, *srcPtr++);
			gain = V4Add(gain, gainStep);

			const Vector_4V srcSamplesFLFR01 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples0, srcSamples1);
			const Vector_4V srcSamplesRLRR01 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples0, srcSamples1);
			const Vector_4V srcSamplesFLFR23 = V4PermuteTwo<X1,X2,Y1,Y2>(srcSamples2, srcSamples3);
			const Vector_4V srcSamplesRLRR23 = V4PermuteTwo<Z1,Z2,W1,W2>(srcSamples2, srcSamples3);

			*dest0Ptr = V4Add(*dest0Ptr, V4PermuteTwo<X1,Y1,X2,Y2>(srcSamplesFLFR01, srcSamplesFLFR23));
			dest0Ptr++;
			*dest1Ptr = V4Add(*dest1Ptr, V4PermuteTwo<Z1,W1,Z2,W2>(srcSamplesFLFR01, srcSamplesFLFR23));
			dest1Ptr++;
			*dest2Ptr = V4Add(*dest2Ptr, V4PermuteTwo<X1,Y1,X2,Y2>(srcSamplesRLRR01, srcSamplesRLRR23));
			dest2Ptr++;
			*dest3Ptr = V4Add(*dest3Ptr, V4PermuteTwo<Z1,W1,Z2,W2>(srcSamplesRLRR01, srcSamplesRLRR23));
			dest3Ptr++;
		}
	}

	__forceinline void CopyMonoBufferToMonoMixBuf(f32 *dest, s16 *ptr1, const u32 numSamples1)
	{
		if (!ptr1 || !dest)
			return;

		Assert((((size_t)dest)&15)==0);
		Assert((((size_t)ptr1)&15)==0);

		const u32 alignedSamples = numSamples1 & ~(7);
		const u32 stragglerSamples = numSamples1 - alignedSamples;

#if RSG_CPU_INTEL
		const Vector_4V oneOverDivisor = V4VConstantSplat<0x38000100>();// 1.f/32768
#endif

		for(unsigned int i = 0; i < alignedSamples; i += 8)
		{
			const Vector_4V inputInts = *(Vector_4V*)(&ptr1[i]);

			// unpack 8 16 bit ints into 32bit ints (splitting into two vectors)
			const Vector_4V inputInts1 = V4UnpackLowSignedShort(inputInts);
			const Vector_4V inputInts2 = V4UnpackHighSignedShort(inputInts);

			// convert to float
#if RSG_CPU_INTEL
			// On PC V4IntoToFloatRaw is only efficient when exponent = 0, so scale manually
			*(Vector_4V*)(&dest[i]) = V4Scale(oneOverDivisor, V4IntToFloatRaw<0>(inputInts1));
			*(Vector_4V*)(&dest[i+4])= V4Scale(oneOverDivisor, V4IntToFloatRaw<0>(inputInts2));
#else
			*(Vector_4V*)(&dest[i]) = V4IntToFloatRaw<15>(inputInts1);
			*(Vector_4V*)(&dest[i+4])= V4IntToFloatRaw<15>(inputInts2);
#endif
		}

		const f32 oneOver32767 = 1.f/32767.f;
		for(u32 i = 0; i<stragglerSamples; i++)
		{
			dest[alignedSamples+i] = ((f32)ptr1[alignedSamples + i] * oneOver32767);
		}
	}

	__forceinline void CopyMonoBufferToMonoMixBuf(f32 *dest, s32 *ptr1, const u32 numSamples1)
	{
		if (!ptr1 || !dest)
			return;

		Assert((((size_t)dest)&15)==0);
		Assert((((size_t)ptr1)&15)==0);

		const u32 alignedSamples = numSamples1 & ~(3);
		const u32 stragglerSamples = numSamples1 - alignedSamples;

#if RSG_CPU_INTEL
		const Vector_4V oneOverDivisor = V4VConstantSplat<0x30000000>();// 1.f/2^31
#endif

		for(unsigned int i = 0; i < alignedSamples; i += 4)
		{
			const Vector_4V inputInts = *(Vector_4V*)(&ptr1[i]);

			// convert to float
#if RSG_CPU_INTEL
			// On PC V4IntoToFloatRaw is only efficient when exponent = 0, so scale manually
			*(Vector_4V*)(&dest[i]) = V4Scale(oneOverDivisor, V4IntToFloatRaw<0>(inputInts));

#else
			*(Vector_4V*)(&dest[i]) = V4IntToFloatRaw<31>(inputInts);
#endif
		}

		const f32 oneOver2tothe31 = 1.f/2147483648.f;
		for(u32 i = 0; i<stragglerSamples; i++)
		{
			dest[alignedSamples+i] = ((f32)ptr1[alignedSamples + i] * oneOver2tothe31);
		}
	}

	// PURPOSE
	//	Returns the peak sample amplitude in linear terms from the supplied buffer
	template<const u32 numSamples> __forceinline f32 ComputePeakLevel(const f32 *RESTRICT src)
	{
		if (!src)
			return 0.0f;

		Assert((((size_t)src)&15)==0);
		Vector_4V peak = V4VConstant(V_ZERO);
		
		const Vector_4V *bufPtr = (const Vector_4V*)src;
		for(u32 i = 0; i < numSamples; i += 4)
		{
			const Vector_4V samples = *bufPtr++;
			
			peak = V4Max(peak, V4Abs(samples));			
		}

		Vector_4V splattedPeak = V4Max(peak, V4Max(V4SplatW(peak), V4Max(V4SplatY(peak),V4SplatZ(peak))));
		return GetX(splattedPeak);
	}

	template<const u32 numSamples> __forceinline f32 ComputeRMSLevel(const f32 *RESTRICT src)
	{
		if (!src)
			return 0.0f;

		const Vector_4V oneOverNumSamples = V4VConstantSplat<numSamples == 256 ? 0x3B800000 : 0x3c000000>(); // 1/256 or 1/128
		CompileTimeAssert(numSamples == 256 || numSamples == 128);

		Vector_4V squaredSum = V4VConstant(V_ZERO);
		const Vector_4V *bufPtr = reinterpret_cast<const Vector_4V*>(src);
		for(u32 i = 0; i < numSamples; i += 4)
		{
			const Vector_4V samples = *bufPtr++;
			squaredSum = V4AddScaled(squaredSum, samples, samples);
		}

		Vector_4V splattedSqSum = V4Add(squaredSum, V4Add(V4SplatY(squaredSum), V4Add(V4SplatW(squaredSum), V4SplatZ(squaredSum))));

		Vector_4V splattedMeanSq = V4Scale(splattedSqSum, oneOverNumSamples);
		Vector_4V splattedRMS = V4Sqrt(splattedMeanSq);
		return GetX(splattedRMS);
	}

	// PURPOSE
	//	Returns the mean sample amplitude in linear terms from the supplied buffer
	template<const u32 numSamples> __forceinline f32 ComputeMeanLevel(const f32 *RESTRICT src)
	{
		if (!src)
			return 0.0f;

		Assert((((size_t)src)&15)==0);
		Vector_4V sum = V4VConstant(V_ZERO);

		const Vector_4V *bufPtr = (const Vector_4V*)src;
		for(u32 i = 0; i < numSamples; i += 4)
		{
			sum = V4Add(sum, *bufPtr++);
		}

		Vector_4V splattedSum = V4Add(sum, V4Add(V4SplatW(sum), V4Add(V4SplatY(sum),V4SplatZ(sum))));

		const Vector_4V oneOverNumSamples = V4VConstant<0x3B800000,0x3B800000,0x3B800000,0x3B800000>();
		CompileTimeAssert(numSamples == 256);

		Vector_4V mean = V4Scale(splattedSum, oneOverNumSamples);
		return GetX(mean);
	}

	template <const u32 numSamples> void HardClipBuffer(f32 *src)
	{
		if (!src)
			return;

		Vector_4V *ptr = reinterpret_cast<Vector_4V*>(src);
		u32 count = numSamples >> 4;
		while(count--)
		{
			Vector_4V input = *ptr;
			*ptr++ = V4Clamp(input, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			input = *ptr;
			*ptr++ = V4Clamp(input, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			input = *ptr;
			*ptr++ = V4Clamp(input, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			input = *ptr;
			*ptr++ = V4Clamp(input, V4VConstant(V_NEGONE), V4VConstant(V_ONE));

		}
	}

	template <const u32 numSamples> void FadeOutBuffer(f32 *src)
	{
		if (!src)
			return;

		CompileTimeAssert(numSamples == 256 || numSamples == 128);

		Vector_4V step1 = numSamples == 256
			// {255/256, 254/256, 253/256, 252/256}
			? V4VConstant<0x3F7F0000,0x3F7E0000,0x3F7D0000,0x3F7C0000>()
		
			// {127/128, 126/128, 125/128, 124/128}	
			: V4VConstant<0x3f7e0000, 0x3f7c0000, 0x3f7a0000, 0x3f780000>();
	
		Vector_4V step2 = numSamples == 256 
				// {251/256, 250/256, 249/256, 248/256}
				? V4VConstant<0x3F7B0000,0x3F7A0000,0x3F790000,0x3F780000>()
				//123/128, 122/128, 121/128, 120/128
				: V4VConstant<0x3f760000, 0x3f740000, 0x3f720000, 0x3f700000>();
		
		const Vector_4V stepX8 = numSamples == 256 ? V4VConstantSplat<0xBD000000>() // (1/256)*-8
														: V4VConstantSplat<0xbd800000>(); // (1/128) * -8

		Vector_4V *ptr = (Vector_4V*)src;
		
		
		for(u32 i = 0; i < numSamples; i += 8)
		{
			Vector_4V samples0 = *ptr;	
			Vector_4V samples1 = *(ptr+1);		

			// apply interpolated scalar
			samples0 = V4Scale(samples0, step1);

			// step through gain interpolation
			step1 = V4Add(step1, stepX8);

			samples1 = V4Scale(samples1, step2);

			// step through gain interpolation
			step2 = V4Add(step2, stepX8);	

			*ptr++ = samples0;
			*ptr++ = samples1;
		}
	}

	template <const u32 numSamples> void FadeInBuffer(f32 *src)
	{
		if (!src)
			return;

		CompileTimeAssert(numSamples == 256 || numSamples == 128);

		// {0, 1/256, 2/256, 3/256}
		// or {0, 1/128, 2/128, etc}
		Vector_4V step1 = numSamples == 256 
			? V4VConstant<0x00000000, 0x3B800000,0x3C000000,0x3C400000>()
			: V4VConstant<0x00000000, 0x3c000000, 0x3c800000, 0x3cc00000>();
		
		// {4/256, 5/256, 6/256, 7/256}
		// or {4/128, 5/128, etc}
		Vector_4V step2 = numSamples == 256 
			? V4VConstant<0x3C800000, 0x3CA00000,0x3CC00000,0x3CE00000>()
			: V4VConstant<0x3d000000, 0x3d200000,0x3d400000,0x3d600000>();

		// 8/256 or 8/128
		const Vector_4V stepX8 = V4VConstantSplat<numSamples == 256 ? 0x3D000000 : 0x3d800000>();

		Vector_4V *ptr = (Vector_4V*)src;

		for(u32 i = 0; i < kMixBufNumSamples; i += 8)
		{
			Vector_4V samples0 = *ptr;	
			Vector_4V samples1 = *(ptr+1);		

			// apply interpolated scalar
			samples0 = V4Scale(samples0, step1);

			// step through gain interpolation
			step1 = V4Add(step1, stepX8);

			samples1 = V4Scale(samples1, step2);

			// step through gain interpolation
			step2 = V4Add(step2, stepX8);	

			*ptr++ = samples0;
			*ptr++ = samples1;
		}
	}

} // namespace rage
#endif // AUD_MIXING_VMATH_INL
