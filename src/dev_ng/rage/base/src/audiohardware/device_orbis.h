// 
// audiohardware/device_orbis.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DEVICE_ORBIS_H
#define AUD_DEVICE_ORBIS_H

#if RSG_ORBIS

#include "device.h"
#include "system/ipc.h"

namespace rage
{
	class fiStream;
	class audMixerDeviceOrbis: public audMixerDevice
	{
	public:

		audMixerDeviceOrbis();

		// PURPOSE
		//	Initialises libaudio output port
		// RETURNS
		//	false on failure
		virtual bool InitHardware();

		// PURPOSE
		//	Releases libaudio resources
		virtual void ShutdownHardware();

		// PURPOSE
		//	Starts generating output
		virtual void StartMixing();

		virtual void TriggerUpdate();
		virtual void WaitOnMixBuffers();
		virtual void SignalWaitOnMixBuffers()		{ sysIpcSignalSema(sm_MixBuffersSema); }
		
		void SetPadSpeakerUserId(const int userId) { m_PadSpeakerUserIdRequested = userId; }
		void SetPadSpeakerSubmix(audMixerSubmix *submix) { m_PadSpeakerSubmix = submix; }
		
		// PURPOSE
		//	Returns true if the pad speaker is in use; ie sound set to pad speaker route are indeed coming out of the control pad speaker.
		//	If false then those sounds are being mixed into the main/headphone out.
		bool IsPadSpeakerEnabled() const { return m_PadSpeakerEnabled; }

		void UpdatePortState();

		bool IsRunningLowerPriority() const { return m_IsRunningLowerPriority; }
		void SetLowerPriority(bool lowerPriority);

	private:

		void OpenPadSpeaker(const int userId);

		static DECLARE_THREAD_FUNC(sMixLoop);

		void MixLoop();

		static sysIpcSema sm_TriggerUpdateSema; // for capturing
		static sysIpcSema sm_MixBuffersSema;	// for capturing
		
		audMixerSubmix *m_PadSpeakerSubmix;

		int m_Handle;
		int m_RestrictedContentHandle;
		int m_PadSpeakerHandle;
		int m_PadSpeakerUserId;
		int m_PadSpeakerUserIdRequested;
		volatile bool m_IsThreadRunning;
		bool m_PadSpeakerEnabled;

		s32 m_BuffersToDropAfterCapturing;

		sysIpcThreadId m_ThreadId;
		bool m_IsRunningLowerPriority;
		int m_InitialCpu;

	};

}

#endif // RSG_ORBIS
#endif // AUD_DEVICE_ORBIS_H
