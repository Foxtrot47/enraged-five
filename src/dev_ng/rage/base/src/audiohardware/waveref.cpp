// 
// audiohardware/waveref.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "debug.h"
#include "streamingwaveslot.h"
#include "waveref.h"
#include "waveslot.h"

#include "audiodata/container.h"
#include "audioengine/widgets.h"

namespace rage
{

	bool audWaveRef::Init(const u32 waveRef)
	{
		*(u32*)(&m_WaveSlotIndex) = waveRef;
		audWaveSlot *slot = GetSlot();
		if(slot)
		{
			// There should already have been a reference added to the slot
			Assert(GetSlot()->GetReferenceCount() > 0);
			return true;
		}
		return false;
	}

	bool audWaveRef::Init(audWaveSlot *waveSlot, const u32 waveNameHash)
	{
		Assert(waveSlot);
		
		// Ensure we free any wave slot reference when a waveref is reused
		Shutdown();

		const u32 waveSlotId = waveSlot->GetSlotIndex();
		
		audWaveSlot::AddSlotReference(waveSlotId);
		Assign(m_WaveSlotIndex, (s32)waveSlotId);

#if __SPU
		if(!waveSlot->FetchContainerHeader())
		{
			return false;
		}
		// find object id
		adatContainerSpu &container = waveSlot->GetContainer();
#else
		// find object id
		const adatContainer &container = waveSlot->GetContainer();
#endif

		adatObjectId objectId = container.FindObject(waveNameHash);
		if(objectId == adatContainer::InvalidId)
		{
			return false;
		}

		Assert(container.GetObjectNameHash(objectId) == (waveNameHash&adatContainerObjectTableEntry::kObjectNameHashMask));
		Assign(m_WaveObjectId, objectId);

		return m_WaveObjectId != adatContainer::InvalidId;	
	}

	bool audWaveRef::Init(const s32 waveSlotId, const u32 waveNameHash)
	{
		return Init(audWaveSlot::GetWaveSlotFromIndex(waveSlotId), waveNameHash);
	}

	const void *audWaveRef::FindChunk(const u32 chunkNameHash, u32 &chunkSizeBytes) const
	{
		Assert(HasValidSlot());
		Assert(HasValidWave());
		const audWaveSlot *waveSlot = GetSlot();
		Assert(waveSlot);
		Assert(waveSlot->GetReferenceCount()>0);
#if __SPU
		adatContainerSpu &container = waveSlot->GetContainer();
#else
		const adatContainer &container = waveSlot->GetContainer();
#endif
		Assert(container.IsValid());

		adatObjectDataChunkId chunkId = container.FindObjectData(static_cast<adatObjectId>(m_WaveObjectId), chunkNameHash);
		if(chunkId == adatContainer::InvalidId)
		{
			return NULL;
		}

		u32 dataTypeHash;
		u32 offsetBytes;

		container.GetObjectDataChunk(m_WaveObjectId, chunkId, dataTypeHash, offsetBytes, chunkSizeBytes);
		Assert(dataTypeHash == (chunkNameHash&adatContainerObjectDataTableEntry::kDataTypeHashMask));

		return waveSlot->Resolve(offsetBytes, chunkSizeBytes);
	}

#if __SPU
	audWaveFormat g_WaveFormatCache;
	u32 g_WaveFormatBufferEa = 0;
	const audWaveFormat *audWaveRef::FindFormat(u32 &sizeBytes) const
	{
		Assert(HasValidSlot());
		if(GetSlot()->IsStreaming())
		{
			return NULL;
		}
		else
		{
			static const u32 formatChunkId = ATSTRINGHASH("format", 1617024250u);
			sizeBytes = 0;
			const u32 ea = (u32)FindChunk(formatChunkId, sizeBytes);
			Assert(AUD_IS_VALID_WAVEFORMAT_SIZE(sizeBytes));

			if(!ea)
			{
				return NULL;
			}
			
			if(g_WaveFormatBufferEa != ea)
			{
				ALIGNAS(16) u8 buf[sizeof(audWaveFormat) + 32] ;
				dmaUnalignedGet(buf, sizeof(buf), &g_WaveFormatCache, sizeof(audWaveFormat), ea);
				g_WaveFormatBufferEa = ea;
			}

			return &g_WaveFormatCache;	
		}
	}
#else // !__SPU

	const audWaveFormat *audWaveRef::FindFormat(u32 &sizeBytes) const
	{
		Assert(HasValidSlot());
		if(GetSlot()->IsStreaming())
		{
			audStreamingWaveSlot *streamSlot = ((audStreamingWaveSlot*)GetSlot());
			sizeBytes = sizeof(audWaveFormat);
			return streamSlot->FindFormat(m_WaveObjectId);
		}
		else
		{
			const u32 formatChunkId = ATSTRINGHASH("format", 1617024250U);
			sizeBytes = 0;
			const void *ret = FindChunk(formatChunkId, sizeBytes);
			Assert(ret);
			Assert(AUD_IS_VALID_WAVEFORMAT_SIZE(sizeBytes));		
			return static_cast<const audWaveFormat *>(ret);
		}
	}

	const void *audWaveRef::FindWaveData(u32 &bytes) const
	{
		const u32 chunkId = ATSTRINGHASH("data", 1588979285U);
		return FindChunk(chunkId, bytes);
	}

	void audWaveRef::FindMarkerData(audWaveMarkerList &markers) const
	{
		const u32 chunkId = ATSTRINGHASH("markers", 3570112701U);
		u32 bytes = 0;
		const void *data = FindChunk(chunkId, bytes);
		markers.Init(data, bytes);
	}

	const void *audWaveRef::FindSeek(u32 &bytes) const
	{
		const u32 chunkId = ATSTRINGHASH("seektable", 35554979U);
		return FindChunk(chunkId, bytes);
	}

	u32 audWaveRef::GetWaveNameHash() const
	{
		Assert(HasValidWave());
		const audWaveSlot *waveSlot = audWaveSlot::GetWaveSlotFromIndex(m_WaveSlotIndex);
		Assert(waveSlot);
		const adatContainer &container = waveSlot->GetContainer();
		Assert(container.IsValid());
		return container.GetObjectNameHash(GetObjectId());
	}

#endif //!__SPU

	audWaveSlot *audWaveRef::GetSlot() const
	{
		if(HasValidSlot())
		{
			return audWaveSlot::GetWaveSlotFromIndex(m_WaveSlotIndex);
		}
		return NULL;
	}
} //namespace rage
