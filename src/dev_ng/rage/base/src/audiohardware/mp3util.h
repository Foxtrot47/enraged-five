#ifndef AUD_MP3UTIL_H
#define AUD_MP3UTIL_H

#include "audiohardware/channel.h"
#include "grcore/wrapper_gcm.h"
#include "math/amath.h"

namespace rage
{
#if __PS3 || __TOOL || __RESOURCECOMPILER || RSG_ORBIS

enum audMpegVersions
{
	MPEG1,
	MPEG2,
	MPEG2_5
};

const s32 g_MpegVersionTable[] =
{
	MPEG2_5,
	-1,
	MPEG2,
	MPEG1
};

enum audMpegLayers
{
	LAYER1,
	LAYER2,
	LAYER3
};

const s32 g_MpegLayerTable[] =
{
	-1,
	LAYER3,
	LAYER2,
	LAYER1
};

const s32 g_MpegSlotBytes[] =
{
	4,	//LAYER1
	1,	//LAYER2
	1	//LAYER3
};

const s32 g_Mpeg1BitrateTable[16][3] =
{
	{-1,	-1,		-1},
	{32,	32,		32},
	{64,	48,		40},
	{96,	56,		48},
	{128, 	64,		56},
	{160, 	80,		64},
	{192, 	96,		80},
	{224, 	112, 	96},
	{256, 	128, 	112},
	{288, 	160, 	128},
	{320, 	192, 	160},
	{352, 	224, 	192},
	{384, 	256, 	224},
	{416, 	320, 	256},
	{448, 	384, 	320},
	{-1,	-1,		-1}
};

const s32 g_Mpeg2BitrateTable[16][3] =
{
	{-1,	-1,		-1},
	{32,	8,		8},
	{48,	16,		16},
	{56,	24,		24},
	{64,	32,		32},
	{80,	40,		40},
	{96,	48,		48},
	{112, 	56, 	56},
	{128, 	64, 	64},
	{144, 	80, 	80},
	{160, 	96, 	96},
	{176, 	112, 	112},
	{192, 	128, 	128},
	{224, 	144, 	144},
	{256, 	160, 	160},
	{-1,	-1,		-1}
};

const s32 g_MpegSampleRateTable[4][3] =
{
	{44100,	22050,	11025},
	{48000,	24000,	12000},
	{32000,	16000,	8000},
	{-1,		-1,		-1}
};
#endif // __PS3 || __TOOL
	class audMp3Util
	{
	public:

		enum {kFrameSizeSamples = 1152};
		enum {kMaxFrameSizeBytes = 1152};
		enum {kMinFrameSizeBytes = 96};

#if __PS3 || __TOOL || RSG_ORBIS

		static void ComputeFrameOffsetFromSamples(const u16 *seekTable, const u32 sizeBytes, u32 sampleOffset, u32 &frameOffsetBytes, u32 &subFrameSampleOffset)
		{
			Assert(seekTable);
			Assert((sizeBytes&1)==0);
			// u16 per seek table entry
			const u32 numSeekTableFrames = sizeBytes>>1;
	
			const u32 seekTableOffsetFrames = sampleOffset / kFrameSizeSamples;
			subFrameSampleOffset = sampleOffset - (seekTableOffsetFrames*kFrameSizeSamples);
			frameOffsetBytes = 0;
			for(u32 i = 0; i < Min(seekTableOffsetFrames,numSeekTableFrames); i++)
			{
				frameOffsetBytes += seekTable[i];
			}	
		}

		static u32 ComputeByteOffsetForFrame(const u8 *const mp3Frame, const u32 bufferSize, const s32 samplesPerFrame, const u32 numFramesDesired)
		{
			PPU_ONLY(audAssertf(!gcm::IsLocalPtr(mp3Frame), "Parsing MP3 data in VRAM: %p, %u", mp3Frame, bufferSize));

			if(!(mp3Frame[0] == 0xff && (mp3Frame[1]&0xE0)==0xE0))
			{
				audAssertf(false, "audMp3Util::ComputeByteOffsetForFrame called with invalid mp3frame");
				return ~0U;
			}
			u32 byteOffset = 0;
			u32 numFrames = 0;
			for(; byteOffset<bufferSize && numFrames < numFramesDesired; byteOffset++)
			{
				const u8 *frame = mp3Frame + byteOffset;
				//Find the frame sync.
				if((frame[0] == 0xFF) && ((frame[1] & 0xE0) == 0xE0))
				{
					u32 versionIndex = (frame[1] >> 3) & 0x3;
					int version = g_MpegVersionTable[versionIndex];
					if(version < 0)
					{
						audErrorf("Invalid MPEG version");
						return ~0U;
					}

					u32 layerIndex = (frame[1] >> 1) & 0x3;
					int layer = g_MpegLayerTable[layerIndex];
					if(layer < 0)
					{
						audErrorf("Invalid MPEG layer");
						return ~0U;
					}

					u32 bitrateIndex = (frame[2] >> 4) & 0xF;
					s32 bitrate;
					if(version == MPEG1)
					{
						bitrate = g_Mpeg1BitrateTable[bitrateIndex][layer] * 1000;
					}
					else
					{
						bitrate = g_Mpeg2BitrateTable[bitrateIndex][layer] * 1000;
					}

					if(bitrate < 0)
					{
						audErrorf("Invalid MPEG bitrate");
						return ~0U;
					}

					u32 sampleRateIndex = (frame[2] >> 2) & 0x3;
					s32 sampleRate = g_MpegSampleRateTable[sampleRateIndex][version];
					if(sampleRate < 0)
					{
						audErrorf("Invalid MPEG sample rate");
						return ~0U;
					}

					u32 padding = (frame[2] >> 1) & 0x1;
					s32 paddingBytes = padding * g_MpegSlotBytes[layer];

					s32 frameBytes = (((samplesPerFrame >> 3) * bitrate) / sampleRate) + paddingBytes;

					//u32 protection = frame[1] & 0x01;
					//if(protection == 0)
					//{
					//	//There should be a 16-bit CRC at the end of the header.
					//	frameBytes += 2;
					//}

					//Jump to the end of this frame.
					byteOffset += frameBytes - 1;

					numFrames++;		
				}
			}
			return byteOffset;
		}

		static u32 ComputeNumFramesInBuffer(const u8 *const mp3Frame, const u32 bytes, const s32 samplesPerFrame, u32 &lastFrameOffset)
		{
			PPU_ONLY(audAssertf(!gcm::IsLocalPtr(mp3Frame), "Parsing MP3 data in VRAM: %p, %u", mp3Frame, bytes));

			u32 numFrames = 0;
			// we should always be passed a valid MP3 frame as the start address
			if(!(mp3Frame[0] == 0xff && (mp3Frame[1]&0xE0)==0xE0))
			{
				audAssertf(false, "ComputeNumFramesInBuffer called with invalid mp3frame");
				return 0;
			}

			for(u32 byteOffset=0; byteOffset<bytes; byteOffset++)
			{
				const u8 *frame = mp3Frame + byteOffset;
				//Find the frame sync.
				if((frame[0] == 0xFF) && ((frame[1] & 0xE0) == 0xE0))
				{
					u32 versionIndex = (frame[1] >> 3) & 0x3;
					int version = g_MpegVersionTable[versionIndex];
					if(version < 0)
					{
						//audErrorf("Invalid MPEG version, voiceId: %u waveNameHash: %u bank: %s\n", m_VoiceId, m_VoiceSettings->WaveNameHash, audWaveSlot::GetWaveSlotFromIndex(m_VoiceSettings->SlotIndex)->GetLoadedBankName());
						return 0;
					}

					u32 layerIndex = (frame[1] >> 1) & 0x3;
					int layer = g_MpegLayerTable[layerIndex];
					if(layer < 0)
					{
						//audErrorf("Invalid MPEG layer, voiceId: %u waveNameHash: %u bank: %s\n", m_VoiceId, m_VoiceSettings->WaveNameHash, audWaveSlot::GetWaveSlotFromIndex(m_VoiceSettings->SlotIndex)->GetLoadedBankName());
						return 0;
					}

					u32 bitrateIndex = (frame[2] >> 4) & 0xF;
					s32 bitrate;
					if(version == MPEG1)
					{
						bitrate = g_Mpeg1BitrateTable[bitrateIndex][layer] * 1000;
					}
					else
					{
						bitrate = g_Mpeg2BitrateTable[bitrateIndex][layer] * 1000;
					}

					if(bitrate < 0)
					{
						//audErrorf("Invalid MPEG bitrate, voiceId: %u waveNameHash: %u bank: %s\n", m_VoiceId, m_VoiceSettings->WaveNameHash, audWaveSlot::GetWaveSlotFromIndex(m_VoiceSettings->SlotIndex)->GetLoadedBankName());
						return 0;
					}

					u32 sampleRateIndex = (frame[2] >> 2) & 0x3;
					s32 sampleRate = g_MpegSampleRateTable[sampleRateIndex][version];
					if(sampleRate < 0)
					{
						//audErrorf("Invalid MPEG sample rate, voiceId: %u waveNameHash: %u bank: %s\n", m_VoiceId, m_VoiceSettings->WaveNameHash, audWaveSlot::GetWaveSlotFromIndex(m_VoiceSettings->SlotIndex)->GetLoadedBankName());
						return 0;
					}

					u32 padding = (frame[2] >> 1) & 0x1;
					s32 paddingBytes = padding * g_MpegSlotBytes[layer];

					s32 frameBytes = (((samplesPerFrame >> 3) * bitrate) / sampleRate) + paddingBytes;

					//u32 protection = frame[1] & 0x01;
					//if(protection == 0)
					//{
					//	//There should be a 16-bit CRC at the end of the header.
					//	frameBytes += 2;
					//}

					//Jump to the end of this frame.
					byteOffset += frameBytes - 1;

					// only count this frame if it is entirely within the supplied range
					if(byteOffset < bytes)
					{
						numFrames++;
						lastFrameOffset = byteOffset + 1;
					}
				}
			}
			return numFrames;
		}

		static u32 ComputeSizeOfFrame(const u32 frameHeader)
		{
			const u32 byte0 = (frameHeader>>24)&0xff;
			const u32 byte1 = (frameHeader>>16)&0xff;
			const u32 byte2 = (frameHeader>>8)&0xff;

			if(Verifyf(byte0 == 0xFF && ((byte1 & 0xE0) == 0xE0), "Invalid MP3 frame: %08X, %02X:%02X", frameHeader, byte0, byte1))
			{
				u32 versionIndex = (byte1 >> 3) & 0x3;
				s32 version = g_MpegVersionTable[versionIndex];
				if(version < 0)
				{
					audErrorf("Invalid MPEG version");
					return 0;
				}

				u32 layerIndex = (byte1 >> 1) & 0x3;
				s32 layer = g_MpegLayerTable[layerIndex];
				if(layer < 0)
				{
					audErrorf("Invalid MPEG layer");
					return 0;
				}

				u32 bitrateIndex = (byte2 >> 4) & 0xF;
				s32 bitrate;
				if(version == MPEG1)
				{
					Assertf(layer >= 2, "MPEG %d layer %d", version, layer);
					bitrate = g_Mpeg1BitrateTable[bitrateIndex][layer] * 1000;
				}
				else
				{
					Assertf(layer == 2, "MPEG %d layer %d", version, layer);
					bitrate = g_Mpeg2BitrateTable[bitrateIndex][layer] * 1000;
				}

				if(bitrate < 0)
				{
					audErrorf("Invalid MPEG bitrate");
					return 0;
				}

				u32 sampleRateIndex = (byte2 >> 2) & 0x3;
				s32 sampleRate = g_MpegSampleRateTable[sampleRateIndex][version];
				if(sampleRate < 0)
				{
					audErrorf("Invalid MPEG sample rate");
					return 0;
				}

				u32 padding = (byte2 >> 1) & 0x1;
				s32 paddingBytes = padding * g_MpegSlotBytes[layer];

				return (((kFrameSizeSamples >> 3) * bitrate) / sampleRate) + paddingBytes;
			}

			audErrorf("Invalid MP3 frame: %08X, %02X:%02X", frameHeader, byte0, byte1);

			return 0;
		}

		static u32 ComputeSizeOfFrame(const u8 *const mp3Frame)
		{
			PPU_ONLY(audAssertf(!gcm::IsLocalPtr(mp3Frame), "Parsing MP3 data in VRAM: %p, %u", mp3Frame, sizeof(u32)));
			// mp3Frame will be unaligned, so we can't load 4 bytes directly
			return ComputeSizeOfFrame(LoadU32UnalignedMp3Frame(mp3Frame));
		}
#endif // __PS3 || __TOOL || RSG_ORBIS

#if __PS3
		static u32 LoadU32Unaligned(const u8 *const ptr)
		{
			PPU_ONLY(audAssert(!gcm::IsLocalPtr(ptr)));
			union __attribute__ ((packed)) U {
				u32 i;
			} *pu = (union U *)ptr;
			return pu->i;
		}
		static u16 LoadU16Unaligned(const u8 *const ptr)
		{
			union __attribute__ ((packed)) U {
				u16 i;
			} *pu = (union U *)ptr;
			return pu->i;
		}
#else
		// the mp3 data is BE even on LE platforms
		static u32 LoadU32UnalignedMp3Frame(const u8 *const ptr)
		{
			const u8 *p = ptr;
			u8 b0 = *p++;
			u8 b1 = *p++;
			u8 b2 = *p++;
			u8 b3 = *p++;

#if __BE || RSG_ORBIS // orbis here because data is BE due to ps3
			return (b3 | b2<<8 | b1<<16 | b0<<24);
#else
			return (b0 | b1<<8 | b2<<16 | b3<<24);
#endif
		}

		static u32 LoadU32Unaligned(const u8 *const ptr)
		{
			const u8 *p = ptr;
			u8 b0 = *p++;
			u8 b1 = *p++;
			u8 b2 = *p++;
			u8 b3 = *p++;

#if __BE 
			return (b3 | b2<<8 | b1<<16 | b0<<24);
#else
			return (b0 | b1<<8 | b2<<16 | b3<<24);
#endif
		}

		static u16 LoadU16Unaligned(const u8 *const ptr)
		{
			const u8 *p = ptr;
			u8 b0 = *p++;
			u8 b1 = *p++;
#if __BE
			return (b1 | b0<<8);
#else
			return (b0 | b1<<8);
#endif
		}
#endif // __PS3

	};
} // namespace rage


#endif // AUD_MP3UTIL_H
