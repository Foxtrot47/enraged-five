//
// audiohardware/device_360.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//
#if __XENON //|| RSG_DURANGO

#include "device_360.h"
#include "driver.h"
#include "submix.h"

#include "file/asset.h"
#include "file/remote.h"
#include "file/stream.h"

#include "system/endian.h"
#include "system/param.h"
#include "system/bootmgr.h"
#include "decoder_xbonexma.h"

//OPTIMISATIONS_OFF()

#if __XENON
#include "XMAHardwareAbstraction.h"
#pragma comment(lib, "xmcore.lib")
#pragma comment(lib, "xmahal.lib")
#pragma comment(lib, "xaudio2.lib")
#endif // __XENON
#if RSG_DURANGO
#pragma comment(lib, "xaudio2.lib")
#pragma comment(lib, "acphal.lib")
#elif (RSG_PC && __D3D11_1)
/* If you want to drop Windows 7 support add this
	#if (_WIN32_WINNT >= 0x0602) //_WIN32_WINNT_WIN8
	#include <xaudio2.h> // from Windows 8.0 SDK
	#pragma comment(lib,"xaudio2.lib")
	#endif
*/
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if(p) { p->Release(); p = NULL; }
#endif

#if !__TOOL
PARAM(audioDeviceCore,"Specify the core to run the audio on: 0->6. Default is 6. -1 means any core)");
#endif
#define AUD_DVR_OUTPUT (1 && RSG_DURANGO)
#if AUD_DVR_OUTPUT
#define DVR_OUTPUT_ONLY(x) (x)
#else
#define DVR_OUTPUT_ONLY(x)
#endif

namespace rage
{
	bool audXAudio2Instance::sm_HaveMasterInstance = false;

	using namespace rage::Vec;


#if RSG_DURANGO
	ServiceDelegate audMixerDeviceXAudio2::sm_Delegate;

	volatile bool audMixerDeviceXAudio2::sm_IsThreadRunning = false;
	volatile bool audMixerDeviceXAudio2::sm_ShouldThreadBeRunning = true;
	sysIpcThreadId	audMixerDeviceXAudio2::sm_ThreadId;
	sysPerformanceTimer* audMixerDeviceXAudio2::sm_PerfTimer;
#endif

	enum { kXAudioOutputBuffers = 3 };

	audMixerDeviceXAudio2::audMixerDeviceXAudio2() :
		
		m_NumRealHardwareChannels(0),
		m_NumHardwareChannels(0)
	{

	}

	bool audMixerDeviceXAudio2::InitHardware()
	{
		XENON_ONLY(XMAPlaybackInitialize());

#if RSG_DURANGO
		sm_PerfTimer = rage_new sysPerformanceTimer("Fake Audio Mixer");
		sm_PerfTimer->Start();

		sm_ShouldThreadBeRunning = true;

		const sysIpcPriority audThreadPriority = PRIO_ABOVE_NORMAL;	
		const int audThreadCpu = 5;

		sm_ThreadId = sysIpcCreateThread(audMixerDeviceXAudio2::ThreadEntryProc, NULL, 18432, audThreadPriority,"[RAGE Audio] - Suspended Mixer", audThreadCpu, "RageSuspendedMixerThread");
		audAssertf(sm_ThreadId != sysIpcThreadIdInvalid, "Couldn't create audio fake mixer thread!");
#endif

#if (RSG_PC && __D3D11_1) || RSG_DURANGO
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
#else
		WIN32PC_ONLY(CoInitialize(NULL));
#endif

		//AssertVerify(SUCCEEDED(ApuCreateHeap(160 * 1024 * 1024, 1 * 1024 * 1024)));

		if(!m_XAudio2Instance.Init(audXAudio2Instance::Main))
		{
			return false;
		}
#if AUD_DVR_OUTPUT
		if(!m_RestrictedInstance.Init(audXAudio2Instance::RestrictedContent))
		{
			return false;
		}
#endif

#if RSG_DURANGO
		sm_Delegate.Bind(&audMixerDeviceXAudio2::HandlePlmChange);
		g_SysService.AddDelegate(&sm_Delegate);
#endif

		return true;
	}

	DECLARE_THREAD_FUNC(audMixerDeviceXAudio2::sMixLoop)
	{
		USE_MEMBUCKET(MEMBUCKET_AUDIO);
		((audMixerDeviceXAudio2*)ptr)->MixLoop();
	}

	void audMixerDeviceXAudio2::StartMixing()
	{
		m_XAudio2Instance.StartMixing();

#if AUD_DVR_OUTPUT
		m_RestrictedInstance.StartMixing();
#endif
		u32 audioCore = 5;
		if (PARAM_audioDeviceCore.Get(audioCore))
		{
			Assert(audioCore <= 7 || audioCore == (u32)-1);
			if (audioCore == (u32)-1)
				audioCore = 5;
			else
				audioCore = audioCore;
		}
		sysIpcCreateThread(sMixLoop, this, 128 * 1024, PRIO_TIME_CRITICAL, "[RAGE Audio] Mix thread", audioCore, "RageAudioMixThread");
	}

	void audMixerDeviceXAudio2::ShutdownHardware()
	{		
		m_XAudio2Instance.Shutdown();

#if AUD_DVR_OUTPUT
		m_RestrictedInstance.Shutdown();
#endif

#if RSG_DURANGO
		audDecoderXboxOneXma::ShutdownClass();

		g_SysService.RemoveDelegate(&sm_Delegate);
		sm_Delegate.Reset();

		sm_ShouldThreadBeRunning = false;
		while(sm_IsThreadRunning)
		{
			sysIpcSleep(10);
		}
		sysIpcWaitThreadExit(sm_ThreadId);
#endif
	}

	void audMixerDeviceXAudio2::MixLoop()
	{
		while(true)
		{
			sysIpcWaitSema(m_XAudio2Instance.GetSema());
#if AUD_DVR_OUTPUT
			sysIpcWaitSema(m_RestrictedInstance.GetSema());
#endif

			MixBuffers();
			EnterBML();
			m_XAudio2Instance.OnBufferReady();

#if AUD_DVR_OUTPUT
			m_RestrictedInstance.OnBufferReady();
#endif
			ExitBML();
		}
	}

	void audXAudio2Instance::StartMixing()
	{
		Assert(m_OutputVoice);
		m_XAudio2->StartEngine();

		sysMemSet(m_OutputBuffer,0, 2 * kMixBufNumSamples * sizeof(f32) * m_NumHardwareChannels);
		m_BufferIndex = 0;

		XAUDIO2_BUFFER bufDesc;
		bufDesc.AudioBytes = kMixBufNumSamples * sizeof(f32) * m_NumHardwareChannels;
		bufDesc.Flags = 0;
		bufDesc.LoopBegin = bufDesc.LoopLength = XAUDIO2_NO_LOOP_REGION;
		bufDesc.LoopCount = 0;
		bufDesc.PlayBegin = 0;
		bufDesc.PlayLength = 0;
		bufDesc.pContext = 0;
		bufDesc.pAudioData = (BYTE*)m_OutputBuffer;
		m_OutputVoice->SubmitSourceBuffer(&bufDesc,NULL);
		m_OutputVoice->Start(0);
	}

	audXAudio2Instance::audXAudio2Instance() :
		m_XAudio2(NULL),
		m_MasteringVoice(NULL),
		m_OutputVoice(NULL),
		m_OutputBuffer(NULL),
		m_Allocator(NULL),
		m_BufferIndex(0),
		m_OutputBufferSizeBytes(0),
		m_MasterInstance(false)
	{
		m_NeedDataSema = sysIpcCreateSema(1);
		m_GotDataSema = sysIpcCreateSema(0);
	}

	bool audXAudio2Instance::Init(InstanceType DVR_OUTPUT_ONLY(instanceType))
	{
		HRESULT hr;

		if(!sm_HaveMasterInstance)
		{
			m_MasterInstance = true;
			sm_HaveMasterInstance = true;
		}
#if RSG_PC
		if(FAILED(hr = XAudio2Create(&m_XAudio2, 0, XAUDIO2_ANY_PROCESSOR)))
#elif RSG_XENON
		if(FAILED(hr = XAudio2Create(&m_XAudio2, 0, XboxThread4)))
#elif RSG_DURANGO
		u32 audioCore=Processor5;
		if (PARAM_audioDeviceCore.Get(audioCore))
		{
			Assert(audioCore <= 7 || audioCore == (u32)-1);
			if (audioCore == (u32)-1)
				audioCore = XAUDIO2_ANY_PROCESSOR;
			else
				audioCore = 1 << audioCore;
		}

		if(FAILED(hr = XAudio2Create(&m_XAudio2, XAUDIO2_DO_NOT_USE_SHAPE | XAUDIO2_DO_NOT_SHARE_SHAPE_CONTEXTS, audioCore)))
#else
		if(FAILED(hr = XAudio2Create(&m_XAudio2, 0, XAUDIO2_DEFAULT_PROCESSOR)))
#endif
		
		{
			return false;
		}
		Assert(m_XAudio2);

		u32 numHardwareChannels = 0;

#if (RSG_PC /*&& !__D3D11_1*/) || __XENON
		// WIN32PC TODO: call m_XAudio2->GetDeviceCount() and pick the most suitable?
		// On 360 there will only be 1 device.
		XAUDIO2_DEVICE_DETAILS deviceDetails;
		if(!audVerifyf(SUCCEEDED(m_XAudio2->GetDeviceDetails(0, &deviceDetails)), "Failed to query audio device details"))
		{
			return false;
		}
		
		numHardwareChannels = deviceDetails.OutputFormat.Format.nChannels;
#else
		// Durango TODO: Enumeration has been removed in Windows 8.
		// We can get the output channel mask by calling IXAudio2MasteringVoice::GetChannelMask.
		numHardwareChannels = 6;//XAUDIO2_DEFAULT_CHANNELS; // 0 - Let CreateMasteringVoice figure it out
#endif

		m_NumRealHardwareChannels = numHardwareChannels;
		m_NumHardwareChannels = numHardwareChannels;

		if(m_NumHardwareChannels > audMixerDevice::kMaxOutputChannels)
			m_NumHardwareChannels = audMixerDevice::kMaxOutputChannels; // we only want to output to a max 6 channels

		
		static AUDIO_STREAM_CATEGORY eDefault = AudioCategory_GameEffects;
		AUDIO_STREAM_CATEGORY category = eDefault;
		u32 flags = 0; 
#if AUD_DVR_OUTPUT 
		if(instanceType == RestrictedContent) 
		{ 
			category = AudioCategory_GameMedia; 
			flags = XAUDIO2_EXCLUDE_FROM_GAME_DVR_CAPTURE; 
		} 
#endif 
		if(FAILED(m_XAudio2->CreateMasteringVoice(&m_MasteringVoice, m_NumHardwareChannels, kMixerNativeSampleRate, flags, NULL, NULL, category)))
		{
			return false;
		}
		Assert(m_MasteringVoice);

#if RSG_DURANGO
		DWORD dwChannelMask;
		m_MasteringVoice->GetChannelMask( &dwChannelMask );

		XAUDIO2_VOICE_DETAILS vdetails;
		m_MasteringVoice->GetVoiceDetails( &vdetails );
		m_NumHardwareChannels = numHardwareChannels = vdetails.InputChannels;
		audDisplayf("%d Audio Channels Available", numHardwareChannels);
		if(m_NumHardwareChannels > audMixerDevice::kMaxOutputChannels)
			m_NumHardwareChannels = audMixerDevice::kMaxOutputChannels; // we only want to output to a max 6 channels

#if __BANK
		for(u32 i = 0; i < kMaxHardwareChannels; i++) 
		{
			m_OutputStreams[i] = NULL;
			m_NumOutputDataBytes[i] = 0;
		}
#endif

#endif // RSG_DURANGO || (RSG_PC && __D3D11_1)

		audOutputMode outputMode;
		if(m_NumHardwareChannels == 6)
		{
			outputMode = AUD_OUTPUT_5_1;
			audDisplayf("5.1 hardware output");
		}
		else
		{
			// WIN32PC TODO: support other output formats
			outputMode = AUD_OUTPUT_STEREO;
			audDisplayf("Stereo hardware output");
		}

		audDriver::SetHardwareOutputMode(outputMode);
		audDriver::SetDownmixOutputMode(outputMode);
				
		WAVEFORMATEX voiceFormat;
		voiceFormat.wBitsPerSample = 32;
		voiceFormat.wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
		Assign(voiceFormat.nChannels, m_NumHardwareChannels);
		voiceFormat.nSamplesPerSec = kMixerNativeSampleRate;
		voiceFormat.cbSize = 0;
		voiceFormat.nBlockAlign = (voiceFormat.nChannels * voiceFormat.wBitsPerSample) >> 3;
		voiceFormat.nAvgBytesPerSec = voiceFormat.nSamplesPerSec * voiceFormat.nBlockAlign;

		if(FAILED(m_XAudio2->CreateSourceVoice(&m_OutputVoice, &voiceFormat, XAUDIO2_VOICE_NOSRC | XAUDIO2_VOICE_NOPITCH, 1.f, this, NULL, NULL)))
		{
			return false;
		}
		
		static const f32 stereoOutputMatrix[2*2] = {
			1.f,0.f,
			0.f,1.f};

		static const f32 fiveoOneOutputMatrix[6*6] = {
			1.f,0.f,0.f,0.f,0.f,0.f,
			0.f,1.f,0.f,0.f,0.f,0.f,
			0.f,0.f,1.f,0.f,0.f,0.f,
			0.f,0.f,0.f,1.f,0.f,0.f,
			0.f,0.f,0.f,0.f,1.f,0.f,
			0.f,0.f,0.f,0.f,0.f,1.f,
		};

		if(FAILED(m_OutputVoice->SetOutputMatrix(NULL, m_NumHardwareChannels, m_NumHardwareChannels, (m_NumHardwareChannels == 6 ? fiveoOneOutputMatrix : stereoOutputMatrix))))
		{
			return false;
		}
		static const f32 channelMatrix[] = {1.f,1.f,1.f,1.f,1.f,1.f};
		if(FAILED(m_OutputVoice->SetChannelVolumes(m_NumHardwareChannels, channelMatrix)))
		{
			return false;
		}

		m_OutputBufferSizeBytes = kMixBufNumSamples * m_NumHardwareChannels * 4;
		m_OutputBuffer = rage_aligned_new(16) f32[kMixBufNumSamples * m_NumHardwareChannels * kXAudioOutputBuffers];

#if AUD_OUTPUT_TO_FILE
		m_DebugOutput = ASSET.Create("mixerout","raw");
#endif

#if RSG_DURANGO && AUD_DVR_OUTPUT
		if(instanceType != RestrictedContent)
#endif
		{
			if(!audDecoderXboxOneXma::InitClass())
			{
				audErrorf("audMixerDeviceXAudio2::InitHardware - audDecoderXboxOneXma::InitClass failed");
				return false;
			}
		}
		m_Allocator = &sysMemAllocator::GetCurrent();
		return true;
	}

	void audXAudio2Instance::Shutdown()
	{
		m_XAudio2->StopEngine();
		if(m_OutputVoice)
		{
			m_OutputVoice->Stop(0);
			m_OutputVoice->DestroyVoice();
			m_OutputVoice = NULL;
		}
		if(m_MasteringVoice)
		{
			m_MasteringVoice->DestroyVoice();
			m_MasteringVoice = NULL;
		}
		SAFE_RELEASE(m_XAudio2);

		delete[] m_OutputBuffer;
		m_OutputBuffer = NULL;
	}

	extern bool g_DumpAudioHardwareOutput;

#if __BANK
	void audXAudio2Instance::DumpOutput(u32 channelIndex, f32 data) 
	{
		if(g_DumpAudioHardwareOutput)
		{
			if(!m_OutputStreams[channelIndex])
			{
				m_NumOutputDataBytes[channelIndex] = 0;

				char fileName[64];
				// RAGE channel ordering
				const char *channelNames[] = 
				{
					"FL",
					"FR",
					"C",
					"LFE",
					"SL",
					"SR",					
					"BL",
					"BR"
				};
				formatf(fileName, "c:\\HardwareDump_%s", channelNames[channelIndex]);
				m_OutputStreams[channelIndex] = ASSET.Create(fileName, "wav");
				Assert(m_OutputStreams[channelIndex]);
				if(m_OutputStreams[channelIndex])
				{
					const unsigned char RIFF[] = {'R', 'I', 'F', 'F'};
					m_OutputStreams[channelIndex]->WriteByte(RIFF, 4);
					int size = 0;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					const unsigned char WAVE[] = {'W', 'A', 'V', 'E'};
					m_OutputStreams[channelIndex]->WriteByte(WAVE, 4);

					const unsigned char fmtChunk[] = {'f', 'm', 't', ' '};
					m_OutputStreams[channelIndex]->WriteByte(fmtChunk, 4);
					size = 16;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					short audioFormat = 1;
					m_OutputStreams[channelIndex]->WriteShort(&audioFormat, 1);
					short numChannels = 1;
					m_OutputStreams[channelIndex]->WriteShort(&numChannels, 1);
					u32 sampleRate = kMixerNativeSampleRate;
					m_OutputStreams[channelIndex]->WriteInt(&sampleRate, 1);
					u32 byteRate = sampleRate * numChannels * 16 / 8;
					m_OutputStreams[channelIndex]->WriteInt(&byteRate, 1);
					short blockA = 2;
					m_OutputStreams[channelIndex]->WriteShort(&blockA, 1);
					short bitsPerSample = 16;
					m_OutputStreams[channelIndex]->WriteShort(&bitsPerSample, 1);

					const unsigned char dataChunk[] = {'d', 'a', 't', 'a'};
					m_OutputStreams[channelIndex]->WriteByte(dataChunk, 4);
					size = 0;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);

				}
			}
			s16 sh = (s16)(Clamp(data,-1.f,1.f) * 32767.f);
			m_OutputStreams[channelIndex]->WriteShort(&sh,1);
			m_NumOutputDataBytes[channelIndex] += 2;
		}
		else
		{
			if(m_OutputStreams[channelIndex])
			{
					int size = m_NumOutputDataBytes[channelIndex] + 36;
					m_OutputStreams[channelIndex]->Seek(4);
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					size = m_NumOutputDataBytes[channelIndex];
					m_OutputStreams[channelIndex]->Seek(40);
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					m_OutputStreams[channelIndex]->Close();
					m_OutputStreams[channelIndex] = NULL;
			}
		}
	}
#endif

	void audXAudio2Instance::OnBufferReady()	
	{		

		float *outputData = (float*)((BYTE*)m_OutputBuffer + (m_OutputBufferSizeBytes * m_BufferIndex));
		
		// interleave and channel swap the mixed data
		const bool isPaused = audDriver::GetMixer()->IsPaused() NOTFINAL_ONLY(|| fiIsShowingMessageBox);
		audMixerSubmix *submix = m_MasterInstance ? audDriver::GetMixer()->GetMainOutputSubmix() : audDriver::GetMixer()->GetRestrictedOutputSubmix();
		if(submix && !isPaused && m_OutputBuffer && submix->HasMixBuffers())
		{
			if(submix->GetNumChannels() == 4)
			{
				Assert(submix->GetBufferFormat() == Interleaved);
				if(m_NumHardwareChannels == 6)
				{
					if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
					{
						// Downmix to stereo, expand to 6 channel output
						// Note input is interleaved
						Vector_4V *out = (Vector_4V*)outputData;
				
						// FL = (FL) + 0.707(RL)
						// FR = (FR) + 0.707(RR)

						// 0.707f
						const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();
						for(u32 chunk = 0; chunk < 4; chunk++)
						{
							Vector_4V *inSamples = (Vector_4V *)submix->GetMixBuffer(chunk);

							for(u32 i = 0; i < kMixBufNumSamples; i += 16)
							{
								// load four samples per channel (interleaved at this point)
								const Vector_4V s0 = *inSamples++;
								const Vector_4V s1 = *inSamples++;
								const Vector_4V s2 = *inSamples++;
								const Vector_4V s3 = *inSamples++;

								const Vector_4V front01 = V4PermuteTwo<X1,Y1,X2,Y2>(s0,s1);
								const Vector_4V rear01 = V4PermuteTwo<Z1,W1,Z2,W2>(s0,s1);
								const Vector_4V front23 = V4PermuteTwo<X1,Y1,X2,Y2>(s2,s3);
								const Vector_4V rear23 = V4PermuteTwo<Z1,W1,Z2,W2>(s2,s3);

								const Vector_4V lr0lr1 = V4Clamp(V4AddScaled(front01, rear01, centreRearRatio), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
								const Vector_4V lr2lr3 = V4Clamp(V4AddScaled(front23, rear23, centreRearRatio), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
								
								// Now interleave LR0,00
								*out++ = V4PermuteTwo<X1,Y1,X2,X2>(lr0lr1, V4VConstant(V_ZERO));
								// 00,LR1
								*out++ = V4PermuteTwo<X1,X1,Z2,W2>(V4VConstant(V_ZERO), lr0lr1);
								// 0000
								*out++ = V4VConstant(V_ZERO);
								// LR2,00
								*out++ = V4PermuteTwo<X1,Y1,X2,X2>(lr2lr3, V4VConstant(V_ZERO));
								// 00,LR3
								*out++ = V4PermuteTwo<X1,X1,Z2,W2>(V4VConstant(V_ZERO), lr2lr3);
								// 0000
								*out++ = V4VConstant(V_ZERO);							
							}
						}
					}
					else
					{
						// up mix 4-6
						float *outputPtr = outputData;
						for(u32 i = 0; i < 4; i++)
						{
							const float *inPtr = submix->GetMixBuffer(i);
							for(u32 sample = 0; sample < kMixBufNumSamples>>2; sample++)
							{
								*outputPtr++ = *inPtr++; // FL
								*outputPtr++ = *inPtr++; // FR
								*outputPtr++ = 0.f; // C
								*outputPtr++ = 0.f; // LFE
								*outputPtr++ = *inPtr++; // BL
								*outputPtr++ = *inPtr++; // BR
							}
						}
					}
				}
				else
				{
					// downmix 4-2
					Vector_4V *outputPtr = (Vector_4V*)outputData;
					// 0.707f,0.707,0,0
					const Vector_4V rearSampleScalar = V4VConstant<0x3F34FDF3,0x3F34FDF3,0,0>();

					for(u32 i = 0; i < 4; i++)
					{
						const Vector_4V *inPtr = reinterpret_cast<Vector_4V*>(submix->GetMixBuffer(i));
						for(u32 sample = 0; sample < kMixBufNumSamples>>3; sample++)
						{
							Vector_4V inputSamples0 = *inPtr++;
							Vector_4V inputSamples1 = *inPtr++;

							// FL = (FL) + 0.707(RL)
							// FR = (FR) + 0.707(RR)
							const Vector_4V rearSamples0 = V4Scale(V4Permute<W,Z,W,Z>(inputSamples0), rearSampleScalar);
							// Z,W of downmixedSamples are invalid
							const Vector_4V downmixedSamples0  = V4Clamp(V4Add(rearSamples0, inputSamples0), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						
							const Vector_4V rearSamples1 = V4Scale(V4Permute<Z,W,Z,W>(inputSamples1), rearSampleScalar);
							const Vector_4V downmixedSamples1  = V4Clamp(V4Add(rearSamples1, inputSamples1), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				
							*outputPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(downmixedSamples0, downmixedSamples1);				
						}
					}
				}
			}
			else
			{
				Assert(submix->GetNumChannels() == 6);
	#if RSG_XENON
				Vector_4V *out = (Vector_4V*)outputData;

				const Vector_4V *inFL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_LEFT);
				const Vector_4V *inFR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_RIGHT);
				const Vector_4V *inC = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_CENTER);
				const Vector_4V *inLFE = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_LOW_FREQUENCY);
				const Vector_4V *inRL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_LEFT);
				const Vector_4V *inRR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_RIGHT);

				CompileTimeAssert(kMixBufNumSamples == 256);
				for(u32 frameIdx = 0; frameIdx < kMixBufNumSamples; frameIdx += 4)
				{
					const Vector_4V inputSamples0 = *inFL++;
					const Vector_4V inputSamples1 = *inFR++;
					const Vector_4V inputSamples2 = *inC++;
					const Vector_4V inputSamples3 = *inLFE++;
					const Vector_4V inputSamples4 = *inRL++;
					const Vector_4V inputSamples5 = *inRR++;

					const Vector_4V inputSamples_01_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inputSamples0, inputSamples1);
					const Vector_4V inputSamples_01_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inputSamples0, inputSamples1);

					const Vector_4V inputSamples_23_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inputSamples2, inputSamples3);
					const Vector_4V inputSamples_23_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inputSamples2, inputSamples3);

					const Vector_4V inputSamples_45_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inputSamples4, inputSamples5);
					const Vector_4V inputSamples_45_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inputSamples4, inputSamples5);

					const Vector_4V samples_0123_x = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_xy,inputSamples_23_xy);
					const Vector_4V samples_4501_xy = V4PermuteTwo<X1,Y1,Z2,W2>(inputSamples_45_xy, inputSamples_01_xy);
					const Vector_4V samples_2345_y = V4PermuteTwo<Z1,W1,Z2,W2>(inputSamples_23_xy, inputSamples_45_xy);
					const Vector_4V samples_0123_z = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_zw,inputSamples_23_zw);
					const Vector_4V samples_4501_zw = V4PermuteTwo<X1,Y1,Z2,W2>(inputSamples_45_zw, inputSamples_01_zw);
					const Vector_4V samples_2345_w = V4PermuteTwo<Z1,W1,Z2,W2>(inputSamples_23_zw, inputSamples_45_zw);

					*out++ = V4Clamp(samples_0123_x, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
					*out++ = V4Clamp(samples_4501_xy, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
					*out++ = V4Clamp(samples_2345_y, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
					*out++ = V4Clamp(samples_0123_z, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
					*out++ = V4Clamp(samples_4501_zw, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
					*out++ = V4Clamp(samples_2345_w, V4VConstant(V_NEGONE), V4VConstant(V_ONE));		
				}

	#else
				if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
				{

					Vector_4V *out = (Vector_4V*)outputData;

#if __BANK
					//Keep a second pointer for dumping data to file
					f32 *dumpData = outputData;
#endif

					const Vector_4V *inFL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_LEFT);
					const Vector_4V *inFR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_RIGHT);
					const Vector_4V *inC = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_CENTER);
					const Vector_4V *inRL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_LEFT);
					const Vector_4V *inRR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_RIGHT);


					// FL = (FL) + 0.707(FC) + 0.707(RL)
					// FR = (FR) + 0.707(FC) + 0.707(RR)

					// 0.707f
					const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();


					for(u32 i = 0; i < kMixBufNumSamples; i += 4)
					{
						// load four samples per channel						

						const Vector_4V inputSamples0 = *inFL++;
						const Vector_4V inputSamples1 = *inFR++;
						const Vector_4V inputSamples2 = *inC++;
						const Vector_4V inputSamples4 = *inRL++;
						const Vector_4V inputSamples5 = *inRR++;

						Vector_4V fcContrib = V4Scale(inputSamples2, centreRearRatio);
						const Vector_4V inSamplesFL = V4Add(inputSamples0, V4AddScaled(fcContrib, inputSamples4, centreRearRatio));
						const Vector_4V inSamplesFR = V4Add(inputSamples1, V4AddScaled(fcContrib, inputSamples5, centreRearRatio));

						const Vector_4V inputSamples_01_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesFL, inSamplesFR);
						const Vector_4V inputSamples_01_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesFL, inSamplesFR);

						const Vector_4V samples_0123_x = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_xy, V4VConstant(V_ZERO));
						const Vector_4V samples_4501_xy = V4PermuteTwo<X1,Y1,Z2,W2>(V4VConstant(V_ZERO), inputSamples_01_xy);

						const Vector_4V samples_0123_z = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_zw,V4VConstant(V_ZERO));
						const Vector_4V samples_4501_zw = V4PermuteTwo<X1,Y1,Z2,W2>(V4VConstant(V_ZERO), inputSamples_01_zw);

						*out++ = V4Clamp(samples_0123_x, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4Clamp(samples_4501_xy, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4VConstant(V_ZERO);
						*out++ = V4Clamp(samples_0123_z, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4Clamp(samples_4501_zw, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4VConstant(V_ZERO);

#if __BANK
						for(int j=0; j<4; j++) //Four samples of data in each pass
						{
							for(int k=0; k<6; k++) //dump the 6 interleaved channels
							{
								DumpOutput(k, *dumpData++);
							}
						}
#endif
					}
				}
			else
			{
				f32 *RESTRICT destPtr = (f32*)outputData;
				u32 outIndex = 0;

				// NOTE: map RAGE channel order FL,FR,SL,SR,C,LFE to XAudio2 FL,FR,C,LFE,SL,SR
				const s32 channelMap[] = {RAGE_SPEAKER_ID_FRONT_LEFT,RAGE_SPEAKER_ID_FRONT_RIGHT,RAGE_SPEAKER_ID_FRONT_CENTER,RAGE_SPEAKER_ID_LOW_FREQUENCY,RAGE_SPEAKER_ID_BACK_LEFT,RAGE_SPEAKER_ID_BACK_RIGHT};
				for(u32 sampleIndex = 0; sampleIndex < kMixBufNumSamples; sampleIndex++)
				{
					for(u32 channelIndex = 0; channelIndex < m_NumHardwareChannels; channelIndex++)
					{
#if __BANK
						DumpOutput(channelIndex, submix->GetMixBuffer(channelMap[channelIndex])[sampleIndex]);
#endif
						destPtr[outIndex++] = submix->GetMixBuffer(channelMap[channelIndex])[sampleIndex];
					}
				}
			}
	#endif 
			}
		}
		else
		{
			sysMemSet(outputData, 0, m_OutputBufferSizeBytes);
		}
		
		sysIpcSignalSema(m_GotDataSema);
	}

	void audXAudio2Instance::OnBufferStart(void *)
	{
		sysIpcWaitSema(m_GotDataSema);

		// submit it to xaudio
		XAUDIO2_BUFFER bufDesc;
		bufDesc.AudioBytes = kMixBufNumSamples * sizeof(f32) * m_NumHardwareChannels;
		bufDesc.Flags = 0;
		bufDesc.LoopBegin = bufDesc.LoopLength = XAUDIO2_NO_LOOP_REGION;
		bufDesc.LoopCount = 0;
		bufDesc.PlayBegin = 0;
		bufDesc.PlayLength = 0;
		bufDesc.pContext = NULL;
		bufDesc.pAudioData = (BYTE*)m_OutputBuffer + (m_OutputBufferSizeBytes * m_BufferIndex);
		m_OutputVoice->SubmitSourceBuffer(&bufDesc,NULL);

		m_BufferIndex = (m_BufferIndex + 1) & 1;

		sysIpcSignalSema(m_NeedDataSema);
	}

#if RSG_DURANGO
	void audMixerDeviceXAudio2::StartEngine()
	{
		Displayf("XAudio2->StartEngine");
		m_XAudio2Instance.StartEngine();
#if AUD_DVR_OUTPUT
		m_RestrictedInstance.StartEngine();
#endif
	}

	void audMixerDeviceXAudio2::StopEngine()
	{
		Displayf("XAudio2->StopEngine");
		m_XAudio2Instance.StopEngine();
#if AUD_DVR_OUTPUT
		m_RestrictedInstance.StopEngine();
#endif
	}
	
	void audMixerDeviceXAudio2::SetSuspended(bool suspend)
	{
		
		if(audDriver::GetMixer())
		{
			audDriver::GetMixer()->SetSuspended(suspend);
			if(suspend)
			{
				Displayf("SetSuspend(true)");
				audDriver::GetMixer()->StopEngine();
			}
			else
			{
				Displayf("SetSuspend(false)");
				audDriver::GetMixer()->StartEngine();
			}
		}
		if(suspend)
		{
			sm_PerfTimer->Reset();
			sm_PerfTimer->Start();
		}
	}

	void audMixerDeviceXAudio2::HandlePlmChange( sysServiceEvent* evt )
	{
		if(evt != NULL)
		{
			if(evt->GetType() == sysServiceEvent::SUSPEND_IMMEDIATE)
			{
				Displayf("HandlePlmChange : sysServiceEvent::SUSPEND_IMMEDIATE");
				SetSuspended(true);
			}
			else if(evt->GetType() == sysServiceEvent::CONSTRAINED)
			{
				Displayf("HandlePlmChange : sysServiceEvent::CONSTRAINED");
				if(sysBootManager::IsDebuggerPresent()) // StopEngine reacts differently when running with the debugger
					SetSuspended(false);
				else
					SetSuspended(true);
			}
			else if(evt->GetType() == sysServiceEvent::RESUME_IMMEDIATE || evt->GetType() == sysServiceEvent::UNCONSTRAINED)
			{
				Displayf("HandlePlmChange : sysServiceEvent::RESUME_IMMEDIATE || sysServiceEvent::UNCONSTRAINED");
				SetSuspended(false);
			}
		}
	}

	void audMixerDeviceXAudio2::SuspendedMixerUpdate()
	{
		MixBuffers();
		//Displayf("SuspendedMixerUpdate %d", audEngineUtil::GetCurrentTimeInMilliseconds());
	}

	DECLARE_THREAD_FUNC(audMixerDeviceXAudio2::ThreadEntryProc)
	{
		// disables the warning that ptr is unreferenced 
		ptr=ptr;

		sm_IsThreadRunning = true;

		const int elapsedTimeForSuspendedMixer = 100;
		BANK_ONLY(static int suspendMixerCount = 0;)

		while(sm_ShouldThreadBeRunning)
		{
			if( audDriver::GetMixer() && audDriver::GetMixer()->IsSuspended() && sm_PerfTimer->GetElapsedTimeMS() >= elapsedTimeForSuspendedMixer)
			{
#if __BANK
				suspendMixerCount++;
				if(suspendMixerCount > 20)
				{
					Displayf("SuspendedMixerUpdate %d", audEngineUtil::GetCurrentTimeInMilliseconds());
					suspendMixerCount = 0;
				}
#endif
				audDriver::GetMixer()->SuspendedMixerUpdate();
				sm_PerfTimer->Reset();
				sm_PerfTimer->Start();
			}
			else
			{
				sysIpcSleep(elapsedTimeForSuspendedMixer);
			}
		}
		sm_IsThreadRunning = false;
	}


#endif // RSG_DURANGO


} // namespace rage
#endif // RSG_DURANGO || __XENON
