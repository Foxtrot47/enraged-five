//
// audiohardware/voicemgr.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "math/amath.h"
#include "math/random.h"
#include "profile/profiler.h"
#include "system/param.h"
#include "system/performancetimer.h"
#include "system/task.h"

#include "audioengine/engine.h"
#include "audioengine/soundpool.h"

#include "channel.h"
#include "config.h"
#include "debug.h"
#include "decodemgr.h"
#include "device.h"
#include "driver.h"
#include "driverconfig.h"
#include "driverdefs.h"
#include "driverutil.h"
#include "mixervoice.h"
#include "pcmsource.h"
#include "pcmsource_interface.h"
#include "virtualisation.h"
#include "virtualisationjob.h"
#include "voicemgr.h"
#include "voicesettings.h"

// For debug draw purposes
#include "grainplayer.h"
#include "waveplayer.h"
#include "audiosynth/synthcore.h"
#include "audiosynth/synthesizer.h"


#if __BANK
#include "grcore/im.h"
#include "grcore/viewport.h"
#endif

#include "system/xtl.h"

#if __XENON
#pragma warning(push)
#pragma warning(disable: 4201)
#include <xmp.h>
#pragma warning(pop)
#elif __PPU
#include <sysutil/sysutil_bgmplayback.h>
#endif

namespace rage {

#if __FINAL
	const
#endif
	bool g_SPUVirtualVoiceUpdate = __PPU;
	
	PF_PAGE(VoiceMgrTimingPage, "Voice Manager Timings");
	PF_GROUP(VoiceMgrTiming);
	PF_LINK(VoiceMgrTimingPage, VoiceMgrTiming);

	PF_TIMER(VoiceUpdate, VoiceMgrTiming);
	PF_TIMER(ApplyHeadroom, VoiceMgrTiming);
	PF_TIMER(Virtualisation, VoiceMgrTiming);
	PF_TIMER(UpdatePhysicalVoices, VoiceMgrTiming);
	PF_TIMER(VirtualisationSPUTime, VoiceMgrTiming);

	EXT_PF_GROUP(AudioDriver);

	PF_PAGE(VoiceManagerPage, "RAGE Audio Voice Manager");
	PF_GROUP(VoiceManager);
	PF_LINK(VoiceManagerPage, VoiceManager);
	
	PF_PAGE(AudioSoundsAndVoicesPage, "RAGE Sounds + Voices");
	PF_GROUP(AudioSoundsAndVoices);
	PF_LINK(AudioSoundsAndVoicesPage, AudioSoundsAndVoices);

	PF_VALUE_INT(ActiveVirtualVoices, VoiceManager);
	PF_VALUE_INT(ActivePhysicalVoices, VoiceManager);
	PF_VALUE_INT(ActiveDecoders, VoiceManager);
	PF_VALUE_INT(ActiveNonGroupedVoices, VoiceManager);
	PF_VALUE_INT(ActiveVirtualGroups, VoiceManager);
	PF_VALUE_INT(ActivePlayPhysicalVoices, VoiceManager);
	PF_VALUE_INT(PhysicalVoicesStarted, VoiceManager);
	PF_VALUE_INT(PhysicalVoicesStopped, VoiceManager);
	PF_VALUE_INT(NumVoicesAtEnd, VoiceManager);
	PF_VALUE_INT(PhysicalVoiceCost, VoiceManager);
	PF_VALUE_INT(PhysicalVoiceCostEst, VoiceManager);

	PF_VALUE_INT(AudioThread_Used, AudioSoundsAndVoices);
	PF_VALUE_INT(AudioThread_HWM, AudioSoundsAndVoices);

#if !__FINAL
	PARAM(nospuaudio, "[RAGE Audio] Disable updates on SPU");
	PARAM(voicecapture, "[RAGE Audio] Allocate memory for virtual voice capture");
#endif

static PrioritisedGroupList s_PrioritisedGroupList;
static VoiceBinsList s_VoiceBinsList;

#if __BANK
bool g_DrawPhysicalVoices = false;
bool g_PrintPhysicalVoices = false;
s32 g_DrawBufferDataId = 100000;
extern bool g_DrawRmsLevel;
extern bool g_MeterMaxLevels;
bool g_DrawPCMSource[AUD_NUM_PCMSOURCES];
bool g_DrawNoGaps = false;
#endif

#if !__FINAL
char *g_audVoiceCaptureBuffer = NULL;
char *g_audVoiceCapturePtr = NULL;
bool g_audIsCapturingVirtualVoices = false;
const u32 g_audVoiceCaptureBufferSize = (5*1024*1024);
#endif

audVoiceMgr::audVoiceMgr()
{
}

audVoiceMgr::~audVoiceMgr()
{
}

void audVoiceMgr::Init()
{
#if !__FINAL
	if(!__PPU || PARAM_nospuaudio.Get())
	{
		g_SPUVirtualVoiceUpdate = false;
	}

	if(PARAM_voicecapture.Get())
	{
		USE_DEBUG_MEMORY();
		g_audVoiceCapturePtr = g_audVoiceCaptureBuffer = rage_new char[g_audVoiceCaptureBufferSize];
	}
#endif

#if __BANK
	for(u32 loop = 0; loop < AUD_NUM_PCMSOURCES; loop++)
	{
		g_DrawPCMSource[loop] = true;
	}
#endif

	m_NumCurrentlyActivePhysicalVoices = 0;
	
	m_MaxVirtualVoices = audDriver::GetConfig().GetNumVirtualVoices();
	
	m_NewPhysicalVoiceBuffer = audDriver::GetConfig().GetNumNewPhysicalVoiceBufferVoices();

#if RSG_PC
	m_IsCPULimited = false;
#endif

	SetPhysicalVoiceLimit();

	m_UserMusicOverrideCount = 0;

	Assert(m_PhysicalVoiceList.size()>=m_MaxPhysicalVoices);
	
	// max virtual voices must be divisible by 32 for the bit packed search algorithm to work
	AssertMsg(!(m_MaxVirtualVoices % 32), "MaxVirtualVoices must be divisible by 32");
	Assert(m_MaxPhysicalVoices >= m_NewPhysicalVoiceBuffer);

	m_PrioritisedVoiceList = (PrioritisedVoice*)audDriver::AllocateVirtual(sizeof(PrioritisedVoice) * m_MaxVirtualVoices);
	m_PhysicalVoicesThisFrame = m_MaxPhysicalVoices-m_NewPhysicalVoiceBuffer;

	m_OverrideUserMusicState = __PPU ? kUserMusicOverridden : kUserMusicRestored;

	//	Enable the use of BGM music playback in the title
#if RSG_PS3 || RSG_XENON
	RestoreUserMusic();
	UpdateUserMusicState();
#endif
}

void audVoiceMgr::Shutdown()
{
	if (m_PrioritisedVoiceList)
	{
		audDriver::FreeVirtual(m_PrioritisedVoiceList);
	}

#if !__FINAL

	if(g_audVoiceCaptureBuffer)
	{
		delete[] g_audVoiceCaptureBuffer;
	}
#endif
}

void audVoiceMgr::SetPhysicalVoiceLimit()
{
#if RSG_PC
	if(m_IsCPULimited)
	{		
		enum {kVoiceReducedMixingBudget = 2000};
		m_PhysicalVoiceBudget = kVoiceReducedMixingBudget;		
		m_MaxPhysicalVoices = Min<s32>(kMaxVoices, audConfig::GetValue("driverSettings_NumPhysicalVoices_Reduced", 96));
		m_PhysicalVoicesThisFrame = m_MaxPhysicalVoices - m_NewPhysicalVoiceBuffer;
	}
	else
#endif
	{
		enum {kVoiceMixingBudget = 4000};	
		m_PhysicalVoiceBudget = kVoiceMixingBudget;
		m_MaxPhysicalVoices = Min<s32>(kMaxVoices, audDriver::GetConfig().GetNumPhysicalVoices());
		m_PhysicalVoicesThisFrame = m_MaxPhysicalVoices - m_NewPhysicalVoiceBuffer;
	}
}

#if RSG_PC
void audVoiceMgr::SetCPULimitedAudio(const bool isMinSpec)
{
	if(!m_IsCPULimited && isMinSpec)
	{
		audWarningf("Enabling CPU-limited audio voice limits");
	}
	else if(m_IsCPULimited && !isMinSpec)
	{
		audWarningf("Disabling CPU-limited audio voice limits");
	}

	m_IsCPULimited = isMinSpec; 
	SetPhysicalVoiceLimit();
}
#endif

void audVoiceMgr::SetNumberOfPhysicalVoices(u32 numVoices)
{
	m_PhysicalVoicesThisFrame = Min(numVoices, (u32)(m_MaxPhysicalVoices-m_NewPhysicalVoiceBuffer));
}

audMixerVoice *audVoiceMgr::AllocatePhysicalVoice(audVoiceSettings *voiceSettings)
{
	Assert(voiceSettings);
	for(s32 i = 0; i < m_MaxPhysicalVoices; i++)
	{
		if(m_PhysicalVoiceList[i] == NULL)
		{
			audMixerVoice *voice = audDriver::GetMixer()->InitVoice(i);
			Assert(voice);
			
			// associate PCM generator
			voice->SetPcmSource(voiceSettings->PcmSourceId, voiceSettings->PcmSourceChannelId);

			// connect routes
			for(s32 k = 0; k < audVoiceSettings::kMaxVoiceRouteDestinations; k++)
			{
				if(voiceSettings->Routes[k].submixId != audVoiceRoute::InvalidSubmixId)
				{
					voice->SetOutput(k, voiceSettings->Routes[k].submixId, voiceSettings->Routes[k].invertPhase);				
					voice->SetOutputVolumes(k, voiceSettings->RouteChannelVolumes[k]);
				}
			}
			voice->SetFilters(voiceSettings->LPFCutoff, voiceSettings->HPFCutoff);
			// store voice settings to mark this voice as allocated
			m_PhysicalVoiceList[i] = voiceSettings;
			// update voice settings to link to this voice index
			Assign(voiceSettings->PhysicalVoiceId, i);
			return voice;
		}		
	}

	PrintPhysicalVoices();
	audAssertf(false, "Failed to allocate physical voice (Max:%d, Max This Frame:%d)", m_MaxPhysicalVoices, m_PhysicalVoicesThisFrame);	
	return NULL;
}

void audVoiceMgr::FreePhysicalVoice(audMixerVoice *voice)
{
	const u32 index = audDriver::GetMixer()->GetVoiceIndex(voice);
	Assert(m_PhysicalVoiceList[index]);
	Assert(m_PhysicalVoiceList[index]->PhysicalVoiceId == index);
	Assign(m_PhysicalVoiceList[index]->PhysicalVoiceId,0xff);
	m_PhysicalVoiceList[index] = NULL;
	voice->DisconnectAllOutputs();
}

void audVoiceMgr::RequestUserMusicOverride()
{
	++m_UserMusicOverrideCount;
	if(m_UserMusicOverrideCount == 1)
	{
		m_OverrideUserMusicState = kUserMusicShouldOverride;
	}
}

void audVoiceMgr::OverrideUserMusic()
{
	audDisplayf("[audVoiceMgr] User music override");

	//	If the attempt to override music playback was unsuccessful, we should try again
	switch (m_OverrideUserMusicState)
	{
	case kUserMusicRestored:
	case kUserMusicShouldOverride:
#if RSG_XENON
		m_OverrideUserMusicState = XMPOverrideBackgroundMusic()==ERROR_SUCCESS ? kUserMusicOverridden : kUserMusicShouldOverride;
#elif RSG_PS3
		m_OverrideUserMusicState = cellSysutilDisableBgmPlayback()==CELL_SYSUTIL_BGMPLAYBACK_OK ? kUserMusicOverridden : kUserMusicShouldOverride;
#else
		// For platforms without user music support, act as if our request has been successfully processed.
		m_OverrideUserMusicState = kUserMusicOverridden;
#endif
		if (m_OverrideUserMusicState == kUserMusicOverridden)
		{
			audDisplayf("[audVoiceMgr] User music override success");
		}
		break;
	case kUserMusicShouldRestore:
		m_OverrideUserMusicState = kUserMusicOverridden;
		break;
	case kUserMusicOverridden:
		break;
	}
}

void audVoiceMgr::RequestUserMusicRestore()
{
	if(audVerifyf(m_UserMusicOverrideCount > 0, "Requesting ueser music restore but nothing is overriding it"))
	{
		--m_UserMusicOverrideCount;
		if(m_UserMusicOverrideCount == 0)
		{
			m_OverrideUserMusicState = kUserMusicShouldRestore;
		}
	}
}

void audVoiceMgr::RestoreUserMusic()
{
	audDebugf1("[audVoiceMgr] User music restore");
	//	If the attempt to restore music playback was unsuccessful, we should try again
	switch (m_OverrideUserMusicState)
	{
	case kUserMusicRestored:
		break;
	case kUserMusicShouldRestore:
	case kUserMusicOverridden:
#if __XENON
		m_OverrideUserMusicState = XMPRestoreBackgroundMusic() ==ERROR_SUCCESS ? kUserMusicRestored : kUserMusicShouldRestore;
#elif __PPU
		m_OverrideUserMusicState = cellSysutilEnableBgmPlayback()==CELL_SYSUTIL_BGMPLAYBACK_OK ? kUserMusicRestored : kUserMusicShouldRestore;
#else
		m_OverrideUserMusicState = kUserMusicRestored;
#endif
		audDebugf1(m_OverrideUserMusicState==kUserMusicRestored ? "[audVoiceMgr] User music restore success" : "[audVoiceMgr] User music restore failure");
		break;
	case kUserMusicShouldOverride:
		m_OverrideUserMusicState = kUserMusicRestored;
		break;
	}
}

void audVoiceMgr::UpdatePhysicalVoices(u32 UNUSED_PARAM(timeInMs))
{
	PF_FUNC(UpdatePhysicalVoices);
	u32 numActiveVoices = 0;

	audSoundPool &soundPool = audSound::GetStaticPool();

	for(s32 voiceIdx = 0; voiceIdx < m_PhysicalVoiceList.size(); voiceIdx++)
	{
		if(m_PhysicalVoiceList[voiceIdx])
		{
			audMixerVoice *physVoice = audDriver::GetMixer()->GetVoiceFromIndex(voiceIdx);
			Assert(physVoice);

			audVoiceSettings *voiceSettings = m_PhysicalVoiceList[voiceIdx];
			const u32 voiceBucket = voiceSettings->BucketId;
			const u32 voiceId = soundPool.GetVoiceId(voiceBucket, voiceSettings);

#if !__FINAL
			if(g_audIsCapturingVirtualVoices && physVoice->IsPlaying())
			{
				audVoiceCapturePhysicalVoice packet;
				
				Assign(packet.physVoiceId, voiceIdx);
				Assign(packet.bucketId, voiceBucket);
				Assign(packet.voiceId, voiceId);
				Assign(packet.costEstimate, voiceSettings->CostEstimate);
				packet.virtualisationScore = voiceSettings->VirtualisationScore;
				Assign(packet.pcmSourceChannelId, (u32)voiceSettings->PcmSourceChannelId);
				const audPcmSource *pcmSource = audDriver::GetMixer()->GetPcmSource(voiceSettings->PcmSourceId);
				Assign(packet.pcmSourceType, (u32)pcmSource->GetTypeId());

				audPcmSource::AssetInfo assetInfo;
				pcmSource->GetAssetInfo(assetInfo);
				packet.assetNameHash = assetInfo.assetNameHash;
				packet.slotId = (u8)assetInfo.slotId;
				
				WriteVoiceCapturePacket(packet);
			}
#endif

			if(voiceSettings->State == AUD_VOICE_VIRTUALISING && physVoice->IsPlaying() == false)
			{
				// NOTE: this will handle the state change from AUD_VOICE_VIRTUALISING
				// we were waiting for this voice to stop and it has - mark it as no longer playing physically
				FreePhysicalVoice(physVoice);
				voiceSettings->State = AUD_VOICE_PLAYING_VIRTUALLY;
								
				// if this was waiting to stop and the phys voice is done then there's nothing using the virtual voice
				if(voiceSettings->Flags.ShouldStop)
				{
					audSound::GetStaticPool().FreeVoice(voiceBucket, voiceId);
				}
			}
			else
			{
				numActiveVoices ++;

				if(voiceSettings->Flags.ShouldStartPhysicalPlayback)
				{
					physVoice->Start();
					voiceSettings->Flags.ShouldStartPhysicalPlayback = false;
					voiceSettings->State = AUD_VOICE_STARTING_PHYSICALLY;
				}
				if(voiceSettings->State == AUD_VOICE_STARTING_PHYSICALLY && physVoice->GetState() == audMixerVoice::PLAYING)
				{
					voiceSettings->State = AUD_VOICE_PLAYING_PHYSICALLY;
				}
				if(voiceSettings->Flags.ShouldStop && (voiceSettings->State == AUD_VOICE_PLAYING_PHYSICALLY || voiceSettings->State == AUD_VOICE_STARTING_PHYSICALLY))
				{
					// make sure the start flag is cleared or streaming voices can get stuck on
					voiceSettings->Flags.ShouldStartPhysicalPlayback = false;
					physVoice->Stop();
					voiceSettings->State = AUD_VOICE_STOPPING_PHYSICALLY;
				}

				if((voiceSettings->State == AUD_VOICE_PLAYING_PHYSICALLY && physVoice->GetState() == audMixerVoice::STOPPED)
					 || voiceSettings->State == AUD_VOICE_STOPPING_PHYSICALLY)
				{
					if(!physVoice->IsPlaying())
					{
						FreePhysicalVoice(physVoice);
						voiceSettings->State = AUD_VOICE_PLAYING_VIRTUALLY;
						
						// if this was waiting to stop and the phys voice is done then there's nothing using the virtual voice
						if(voiceSettings->Flags.ShouldStop)
						{
							audSound::GetStaticPool().FreeVoice(voiceBucket, voiceId);
						}
					}
				}

				if(voiceSettings->State != AUD_VOICE_PLAYING_VIRTUALLY)
				{
					if(Unlikely(m_IsUserMusicPlaying && voiceSettings->Flags.ShouldMuteOnUserMusic))
					{
						for(s32 k = 0; k < audVoiceSettings::kMaxVoiceRouteDestinations; k++)
						{
							if(voiceSettings->Routes[k].submixId != audVoiceRoute::InvalidSubmixId)
							{
								physVoice->SetOutputVolumes(k, Vec::V4VConstant(V_ZERO));
							}
						}
					}
					else
					{
						// apply route volumes and filters
						for(s32 k = 0; k < audVoiceSettings::kMaxVoiceRouteDestinations; k++)
						{
							if(voiceSettings->Routes[k].submixId != audVoiceRoute::InvalidSubmixId)
							{
								physVoice->SetOutputVolumes(k, voiceSettings->RouteChannelVolumes[k]);
							}
						}
					}
					
					physVoice->SetFilters(voiceSettings->LPFCutoff, voiceSettings->HPFCutoff);
				}
			
			} // normal phys voice update
		}//if phys voice
	}
	m_NumCurrentlyActivePhysicalVoices = numActiveVoices;
	PF_SET(ActivePhysicalVoices, numActiveVoices);
	PF_SET(ActiveDecoders, audDriver::GetMixer()->GetDecodeMgr().GetNumAllocatedStreams());
}

void audVoiceMgr::UpdateUserMusicState()
{
#if __XENON
	XMP_STATE XMPState;
	AssertVerify(ERROR_SUCCESS == XMPGetStatus(&XMPState));
	m_IsUserMusicPlaying = XMPState == XMP_STATE_PLAYING;
#elif __PPU
	CellSysutilBgmPlaybackStatus bgmplayback_status;
	AssertVerify(CELL_SYSUTIL_BGMPLAYBACK_OK == cellSysutilGetBgmPlaybackStatus(&bgmplayback_status));
	m_IsUserMusicPlaying = bgmplayback_status.playerState == 0;
#else
	m_IsUserMusicPlaying = false;
#endif

	if (m_OverrideUserMusicState == kUserMusicShouldOverride)
	{
		OverrideUserMusic();
	}
	else if (m_OverrideUserMusicState == kUserMusicShouldRestore)
	{
		RestoreUserMusic();
	}
}

void audVoiceMgr::Update(u32 timeInMs)
{
	PF_FUNC(VoiceUpdate);
	// ensure debug draw doesn't happen when we're updating
	BANK_ONLY(SYS_CS_SYNC(m_PhysVoiceDebugDrawLock));

#if !__FINAL
	CaptureFrameStart();
#endif

	if(g_SPUVirtualVoiceUpdate)
	{
		PF_FUNC(VirtualisationSPUTime);

		sysTaskParameters p;
		sysTaskHandle handle;

		m_VirtualisationJobInput.bucketSize = sizeof(audSoundPoolBucket);
		m_VirtualisationJobInput.eaFirstBucket = audSound::GetStaticPool().GetFirstBucketPtr();
		m_VirtualisationJobInput.numBuckets = audSound::GetStaticPool().GetNumBuckets();
#if AUD_SOUNDPOOL_LOCK_ON_SPU
		m_VirtualisationJobInput.eaBucketLocks = audSound::GetStaticPool().GetBucketLocks();
#endif

		p.Input.Data = &m_VirtualisationJobInput;
		p.Input.Size = sizeof(m_VirtualisationJobInput);

		p.Output.Data = &m_VirtualisationJobOutput;
		p.Output.Size =  sizeof(m_VirtualisationJobOutput);

		p.ReadOnly[0].Data = &m_PcmSourceState;
		p.ReadOnly[0].Size = sizeof(m_PcmSourceState);
		p.ReadOnlyCount = 1;
		
		// PPU mem ptr to physical voice state as first param
		p.UserData[0].asPtr = NULL;
		// max virtual voices LSW, max physical voices MSW
		p.UserData[1].asInt = m_MaxVirtualVoices | (m_MaxPhysicalVoices<<16);
		// actual phys voices free LSW, physical voices this frame MSW
		s32 freePhysVoices = 0;
		for(s32 i = 0; i < m_MaxPhysicalVoices; i++)
		{
			if(!m_PhysicalVoiceList[i])
			{
				freePhysVoices++;
			}
		}
		p.UserData[2].asInt = freePhysVoices | (m_PhysicalVoicesThisFrame<<16);
		p.UserData[3].asUInt = m_PhysicalVoiceBudget;
		// time elapsed in ms since last frame
/*		p.UserData[3].asInt = timeInMs - m_LastUpdateTimeMs;
		m_LastUpdateTimeMs = timeInMs;*/	
		p.UserDataCount = 4;

		p.Scratch.Size = (sizeof(PrioritisedVoice) * m_MaxVirtualVoices) + sizeof(audSoundPoolBucket) + sizeof(PrioritisedGroupList) + sizeof(VoiceBinsList);
		p.Scratch.Data = NULL;

		// run the prioritisation/virtualisation job
#if __PS3
		const s32 schedulerId = sysTaskManager::SCHEDULER_AUDIO_ENGINE;
#else
		const s32 schedulerId = 0;
#endif

		handle = sysTaskManager::Create(TASK_INTERFACE(VirtualisationJob),p, schedulerId);
		sysTaskManager::Wait(handle);

		// start/stop physical voices as required
		for(u32 i = 0; i < m_VirtualisationJobOutput.voicesStoppedThisFrame; i++)
		{
			StopPlayingVoicePhysically((s32)m_VirtualisationJobOutput.stopVoiceList[i]);
		}

		for(u32 i = 0; i < m_VirtualisationJobOutput.voicesStartedThisFrame; i++)
		{
			StartPlayingVoicePhysically((s32)m_VirtualisationJobOutput.startVoiceList[i]);
		}

		PF_SET(PhysicalVoicesStarted, m_VirtualisationJobOutput.voicesStartedThisFrame);
		PF_SET(PhysicalVoicesStopped, m_VirtualisationJobOutput.voicesStoppedThisFrame);
		PF_SET(ActiveVirtualVoices, m_VirtualisationJobOutput.numActiveVirtualVoices);
		PF_SET(ActiveNonGroupedVoices, m_VirtualisationJobOutput.numActiveNonGroupedVoices);
		PF_SET(ActiveVirtualGroups, m_VirtualisationJobOutput.numActiveVirtualGroups);
		PF_SET(ActivePlayPhysicalVoices, m_VirtualisationJobOutput.numActivePlayPhysicalVoices);
		PF_SET(NumVoicesAtEnd, m_VirtualisationJobOutput.numVoicesAtEnd);
		PF_SET(PhysicalVoiceCostEst, m_VirtualisationJobOutput.physVoiceCost);
		BANK_ONLY(PF_SET(PhysicalVoiceCost, (int)(1000.f * audDriver::GetMixer()->GetPhysicalPcmSourceCost())));
	}
	else
	{
		PF_FUNC(Virtualisation);
		
		audVirtualisationJobOutput header;
		header.numVoicesAtEnd = 0;
		u32 freePhysVoices = 0;
		for(s32 i = 0; i < m_MaxPhysicalVoices; i++)
		{
			if(!m_PhysicalVoiceList[i])
			{
				freePhysVoices++;
			}
		}
		const u32 actualPhysicalVoicesFree = freePhysVoices;
		const u32 physicalVoicesThisFrame = m_PhysicalVoicesThisFrame;
		
		PrioritisedVoice *voiceList = m_PrioritisedVoiceList;
		VoiceGroup *groupList = &s_PrioritisedGroupList[0];

		// prioritize voices
		BuildPrioritisedVoiceList(voiceList, groupList, &header, s_PrioritisedGroupList, s_VoiceBinsList);
		// dish out phys voices
		AllocatePhysicalVoices(m_PhysicalVoiceBudget, groupList, voiceList, actualPhysicalVoicesFree, physicalVoicesThisFrame, &header);
		
		// start/stop phys voices
		for(u32 i = 0; i < header.voicesStoppedThisFrame; i++)
		{
			StopPlayingVoicePhysically((s32)header.stopVoiceList[i]);
		}

		for(u32 i = 0; i < header.voicesStartedThisFrame; i++)
		{
			StartPlayingVoicePhysically((s32)header.startVoiceList[i]);
		}

		PF_SET(ActiveVirtualVoices, header.numActiveVirtualVoices);
		PF_SET(ActiveNonGroupedVoices, header.numActiveNonGroupedVoices);
		PF_SET(ActiveVirtualGroups, header.numActiveVirtualGroups);
		PF_SET(ActivePlayPhysicalVoices, header.numActivePlayPhysicalVoices);
		PF_SET(PhysicalVoicesStarted, header.voicesStartedThisFrame);
		PF_SET(PhysicalVoicesStopped, header.voicesStoppedThisFrame);
		PF_SET(NumVoicesAtEnd, header.numVoicesAtEnd);
		PF_SET(PhysicalVoiceCostEst, header.physVoiceCost);
		BANK_ONLY(PF_SET(PhysicalVoiceCost, (int)(1000.f * audDriver::GetMixer()->GetPhysicalPcmSourceCost())));
	}

	// physical voice update is always done on the PPU
	UpdatePhysicalVoices(timeInMs);

#if !__FINAL
	CaptureFrameEnd();
#endif

	PF_SET(AudioThread_Used, audDriver::GetMixer()->GetCommandBufferForThread()->GetCurrentOffset());
	PF_SET(AudioThread_HWM, audDriver::GetMixer()->GetCommandBufferForThread()->GetHighWatermark());
}

void audVoiceMgr::StartPlayingVoicePhysically(const u32 packedVirtualVoiceId)
{
	audVoiceSettings *voiceSettings = GetVoiceSettings(packedVirtualVoiceId);
	Assert(voiceSettings);
	
	//PcmSourceState &physState = m_PcmSourceState[voiceSettings->PcmSourceId];
	audAssertf(voiceSettings->State == AUD_VOICE_PLAYING_VIRTUALLY, "Voice id: %08X state: %u physVoiceId: %u ShouldStop: %s", packedVirtualVoiceId, voiceSettings->State, voiceSettings->PhysicalVoiceId, voiceSettings->Flags.ShouldStop ? "true" : "false");
	Assert(voiceSettings->PhysicalVoiceId == 0xff);
	if(voiceSettings->Flags.ShouldStop)
	{
		// Don't start a voice playing physically if its been told to stop
		return;
	}

	if(AllocatePhysicalVoice(voiceSettings))
	{
		voiceSettings->State = AUD_VOICE_STARTING_PHYSICALLY;
		voiceSettings->Flags.ShouldStartPhysicalPlayback = true;
	}
}

void audVoiceMgr::StopPlayingVoicePhysically(const u32 packedVirtualVoiceId)
{
	audVoiceSettings *voiceSettings = GetVoiceSettings(packedVirtualVoiceId);
	Assert(voiceSettings);

	audAssertf(voiceSettings->State == AUD_VOICE_PLAYING_PHYSICALLY || voiceSettings->State == AUD_VOICE_STARTING_PHYSICALLY,
		"Voice id: %08X state: %u physVoiceId: %u", packedVirtualVoiceId, voiceSettings->State, voiceSettings->PhysicalVoiceId);
	Assert(voiceSettings->PhysicalVoiceId != 0xff);
	if (voiceSettings->PhysicalVoiceId == 0xff)
	{
		audErrorf("(A) StopPlayingVoicePhysically - bad index (0xff), bailing out...");
		return;
	}
	
	audMixerVoice *voice = audDriver::GetMixer()->GetVoiceFromIndex(voiceSettings->PhysicalVoiceId);
	Assert(voice);
	voice->Stop();
	voiceSettings->State  = AUD_VOICE_VIRTUALISING;
}

#if __BANK
f32 ConvertSampleToY(const f32 sample)
{
	return ( sample + 1.f ) / 2.f;
}

Vec3V_Out TransformToScreen(Vec3V_In v)
{
	const Vec3V screenOffset(80.f,200.f,0.f);
	const Vec3V screenScaling(1000.f, 300.f, 0.f);

	return AddScaled(screenOffset, v, screenScaling);  
}

void DrawSampleLine(Vec3V_In v0, Vec3V_In v1, Color32 c0)
{
	Vec3V tv0 = TransformToScreen(v0);
	Vec3V tv1 = TransformToScreen(v1);
	grcDrawLine(tv0,tv1,c0);
}

f32 ColorCurve(const f32 sample)
{
	const f32 val = Abs(sample);
	return val*val*val;
}


f32 TransformAmplitudeToY(const f32 dbVol)
{
	// Turn this db volume into a 0-1 value
	const f32 minDb = -100.f;
	const f32 maxDb = 12.f;

	// Note that I'm not scaling to 1 at max; val can be greater than 1 when dbVol is greater than 0 (ie 1.12 @ +12dB)
	const f32 val = 1.f - (Clamp(dbVol, minDb, maxDb) / minDb);

	return val;
}


#endif

BANK_ONLY(extern bool g_MeterSubmixes);
BANK_ONLY(extern bool g_MeterAllSubmixes);
BANK_ONLY(extern char g_MeterSubmixFilter[128]);
BANK_ONLY(extern f32 g_MeterSubmixBGAlpha);
BANK_ONLY(extern f32 g_MeterSubmixScaling);

void audVoiceMgr::PrintPhysicalVoices()
{
#if __BANK
	char buf[128];
	char buf2[128];	

	for(s32 voiceIdx = 0; voiceIdx < m_PhysicalVoiceList.size(); voiceIdx++)
	{
		if(m_PhysicalVoiceList[voiceIdx])
		{
			audMixerVoice *mixerVoice = audDriver::GetMixer()->GetVoiceFromIndex(voiceIdx);

			audVoiceSettings *voiceSettings = m_PhysicalVoiceList[voiceIdx];

			audPcmSource *pcmSource = (audPcmSource*)g_PcmSourcePool->GetSlotPtr(voiceSettings->PcmSourceId);
			Assert(pcmSource);

			const char *typeName = NULL;
			const char *assetName = "(unknown asset)";
			u32 assetId = 0;

			switch(pcmSource->GetTypeId())
			{	
			case AUD_PCMSOURCE_WAVEPLAYER:
				{
					typeName = buf2;						
					audWavePlayer *wavePlayer = (audWavePlayer*)pcmSource;
					formatf(buf2, "W:%d:%d", wavePlayer->GetPlayState(), wavePlayer->GetDecodeState());
					assetName = wavePlayer->GetBankName();
					assetId = wavePlayer->GetWaveNameHash();
				}
				break;
			case AUD_PCMSOURCE_MODULARSYNTH:
				typeName = "MS";
				//assetName = ((synthSynthesizer*)pcmSource)->GetName();
				break;
			case AUD_PCMSOURCE_GRAINPLAYER:
				typeName = "GR";
				break;
			case AUD_PCMSOURCE_SUBMIX:
				typeName = "SUBMIX";
				break;
			case AUD_PCMSOURCE_SYNTHCORE:
				{					
					typeName = "SC";
					synthCorePcmSource *synth = (synthCorePcmSource*)pcmSource;
					assetId = synth->GetSynth().GetAssetNameHash();
					assetName = synthSynthesizer::GetMetadataManager().GetObjectName(assetId);
				}
				break;
			case AUD_PCMSOURCE_EXTERNALSTREAM:
				typeName = "ES";
				break;
			default:
				typeName = "INVALID";
				break;
			}

			PcmSourceState pcmState;
			if(audPcmSourceInterface::GetState(pcmState, voiceSettings->PcmSourceId))
			{				
				formatf(buf, "Slot %d: %u %p %.2f (%u/%u) [%d:%s] %s:%u %d %.2f %.2f", voiceIdx, mixerVoice, voiceSettings->CostEstimate, pcmSource->GetProcessingTimer()*1000.f, voiceSettings->State, mixerVoice->GetState(), voiceSettings->PcmSourceId, typeName, assetName, assetId, pcmSource->GetBufferId(voiceSettings->PcmSourceChannelId), pcmState.LengthSamples ? pcmState.PlaytimeSamples / (f32)pcmState.LengthSamples : -1, pcmState.CurrentPeakLevel / float(0xffff));
				audDisplayf("%s", buf);
			}
			else
			{
				formatf(buf, "Slot %d (STATE INVALID): %u %p %.2f (%u/%u) [%d:%s] %s:%u %d", voiceIdx, mixerVoice, voiceSettings->CostEstimate, pcmSource->GetProcessingTimer()*1000.f, voiceSettings->State, mixerVoice->GetState(), voiceSettings->PcmSourceId, typeName, assetName, assetId, pcmSource->GetBufferId(voiceSettings->PcmSourceChannelId));
				audDisplayf("%s", buf);
			}
		}
		else
		{
			formatf(buf, "Slot %d: NULL", voiceIdx);
			audDisplayf("%s", buf);
		}
	}
#endif
}

void audVoiceMgr::DrawDebug()
{
#if __BANK
	PUSH_DEFAULT_SCREEN();

	if(g_PrintPhysicalVoices)
	{
		PrintPhysicalVoices();
		g_PrintPhysicalVoices = false;
	}

	if(g_DrawPhysicalVoices)
	{
		SYS_CS_SYNC(m_PhysVoiceDebugDrawLock);
	
		audDriver::SetDebugDrawRenderStates();

		grcColor(Color32(255,255,255));

		char buf2[128];
		u32 numVoicesDrawn = 0;

		for(s32 voiceIdx = 0; voiceIdx < m_PhysicalVoiceList.size(); voiceIdx++)
		{
			if(m_PhysicalVoiceList[voiceIdx])
			{
				audMixerVoice *mixerVoice = audDriver::GetMixer()->GetVoiceFromIndex(voiceIdx);

				audVoiceSettings *voiceSettings = m_PhysicalVoiceList[voiceIdx];
		
				audPcmSource *pcmSource = (audPcmSource*)g_PcmSourcePool->GetSlotPtr(voiceSettings->PcmSourceId);
				Assert(pcmSource);
				
				const char *typeName = NULL;
				const char *assetName = "(unknown asset)";
				u32 assetId = 0;

				if(!g_DrawPCMSource[pcmSource->GetTypeId()])
				{
					continue;
				}

				switch(pcmSource->GetTypeId())
				{	
				case AUD_PCMSOURCE_WAVEPLAYER:
					{
						typeName = buf2;						
						audWavePlayer *wavePlayer = (audWavePlayer*)pcmSource;
						formatf(buf2, "W:%d:%d", wavePlayer->GetPlayState(), wavePlayer->GetDecodeState());
						assetName = wavePlayer->GetBankName();
						assetId = wavePlayer->GetWaveNameHash();
					}
					break;
				case AUD_PCMSOURCE_MODULARSYNTH:
					typeName = "MS";
					//assetName = ((synthSynthesizer*)pcmSource)->GetName();
					break;
				case AUD_PCMSOURCE_GRAINPLAYER:
					typeName = "GR";
					break;
				case AUD_PCMSOURCE_SUBMIX:
					typeName = "SUBMIX";
					break;
				case AUD_PCMSOURCE_SYNTHCORE:
					{					
						typeName = "SC";
						synthCorePcmSource *synth = (synthCorePcmSource*)pcmSource;
						assetId = synth->GetSynth().GetAssetNameHash();
						assetName = synthSynthesizer::GetMetadataManager().GetObjectName(assetId);
					}
					break;
				case AUD_PCMSOURCE_EXTERNALSTREAM:
					typeName = "ES";
					break;
				default:
					typeName = "INVALID";
					break;
				}
				
				PcmSourceState pcmState;
				if(audPcmSourceInterface::GetState(pcmState, voiceSettings->PcmSourceId))
				{
					u32 lineIndex = g_DrawNoGaps? numVoicesDrawn:voiceIdx;
					char buf[128];
					formatf(buf, "%u %.2f (%u/%u) [%d:%s] %s:%u %d %.2f %.2f", voiceSettings->CostEstimate, pcmSource->GetProcessingTimer()*1000.f, voiceSettings->State, mixerVoice->GetState(), voiceSettings->PcmSourceId, typeName, assetName, assetId, pcmSource->GetBufferId(voiceSettings->PcmSourceChannelId), pcmState.LengthSamples ? pcmState.PlaytimeSamples / (f32)pcmState.LengthSamples : -1, pcmState.CurrentPeakLevel / float(0xffff));
					//grcDraw2dText(((lineIndex%2)?650.f:40.f), 50.f + ((f32)floorf((lineIndex/2.f))*12.f), buf);
					grcDraw2dText(40.f, 50.f + lineIndex * 12.f, buf);
					numVoicesDrawn++;
				}
			}
		}
	}

	if(audDriver::GetMixer() && g_DrawBufferDataId < (s32)g_FrameBufferPool->GetNumBuffers())
	{
		static f32 *dataBuffer = NULL;
		if(!dataBuffer)
		{
			dataBuffer = rage_aligned_new(128) f32[kMixBufNumSamples];
		}
	
		sysMemCpy128(dataBuffer, g_FrameBufferPool->GetBuffer(g_DrawBufferDataId), sizeof(f32) * kMixBufNumSamples);

		Color32 blue(0,0,255);
		Color32 purple(255,0,100);
		Color32 grey(180,180,180);
		
		Vec3V v0,v1;


		// base lines
		v0.SetXf(0.f);
		v1.SetXf(1.f);
		v0.SetYf(ConvertSampleToY(0.f));
		v1.SetY(v0.GetY());
		DrawSampleLine(v0,v1,Color32(255,255,255));
		

		v0.SetYf(ConvertSampleToY(0.5f));
		v1.SetY(v0.GetY());
		DrawSampleLine(v0,v1,grey);
		v0.SetYf( ConvertSampleToY(-0.5f));
		v1.SetY(v0.GetY());
		DrawSampleLine(v0,v1,grey);

		v0.SetYf(ConvertSampleToY(1.f));
		v1.SetY(v0.GetY());
		DrawSampleLine(v0,v1,grey);
		v0.SetYf(ConvertSampleToY(-1.f));
		v1.SetY(v0.GetY());
		DrawSampleLine(v0,v1,grey);

		const f32 xIncr = 1.f / (f32)kMixBufNumSamples;

		f32 x = 0;
		for(u32 i = 0; i < kMixBufNumSamples - 1; i++)
		{
			v0.SetXf(x);
			v0.SetYf(ConvertSampleToY(dataBuffer[i]));
		
			x += xIncr;

			v1.SetXf(x);
			v1.SetYf(ConvertSampleToY(dataBuffer[i+1]));

			Color32 colorA = Lerp(ColorCurve(dataBuffer[i]), blue, purple);
		
			DrawSampleLine(v0,v1,colorA);
		}
	}

	if(audDriver::GetMixer() && g_MeterSubmixes)
	{
		const f32 screenXOffset_start = 80.f * g_MeterSubmixScaling;
		const f32 screenYOffset_start = 150.f * g_MeterSubmixScaling;
		const f32 meterWidth = 24.f * g_MeterSubmixScaling;
		const f32 meterHeight = 76.f * g_MeterSubmixScaling;
		const f32 meterSpacing = 4.f * g_MeterSubmixScaling;
		const f32 textHeight = 15.f * g_MeterSubmixScaling;

		const f32 screenWidth = 1800.f * g_MeterSubmixScaling;

		const f32 submixXSpacing = 40.f * g_MeterSubmixScaling;
		const f32 submixYSpacing = 15.f * g_MeterSubmixScaling;

		const f32 miniMeterSpacing = 1.f * g_MeterSubmixScaling;
		const f32 miniMeterWidth = 6.f * g_MeterSubmixScaling;

		Color32 white(255,255,255);
		Color32 grey(180,180,180);
		Color32 red(255,0,0);
	
		f32 screenXOffset = screenXOffset_start;
		f32 screenYOffset = screenYOffset_start;

		static f32 storedPeakDecayFactor = 0.975f;

		audDriver::SetDebugDrawRenderStates();

		// draw alpha'd background
		grcBegin(drawTriStrip, 4);
		grcColor(Color32(0.f, 0.f, 0.f, g_MeterSubmixBGAlpha));
		grcVertex2f(0.0f, 0.0f);
		grcVertex2f((f32)GRCDEVICE.GetWidth(), 0.0f);
		grcVertex2f(0.0f, (f32)GRCDEVICE.GetHeight());
		grcVertex2f((f32)GRCDEVICE.GetWidth(), (f32)GRCDEVICE.GetHeight());
		grcEnd();

		enum {MaxSubmixes = 64};
		static f32 storedPeaks[MaxSubmixes][g_MaxOutputChannels] = {0.f};
		static f32 storedPeaksAlpha[MaxSubmixes][g_MaxOutputChannels] = {0.f};

		Assert(audDriver::GetMixer()->GetNumSubmixesInProcessingOrder() <= MaxSubmixes);
		for(s32 submixId = 0; submixId < (g_MeterAllSubmixes ? audDriver::GetMixer()->GetNumSubmixesInProcessingOrder() : 1); submixId++)
		{
			// Draw one submix worth of metering
			audMixerSubmix *submix = g_MeterAllSubmixes ? 
				audDriver::GetMixer()->GetSubmixFromIndex(audDriver::GetMixer()->LookupSubmixProcessingOrder(submixId)) :
				audDriver::GetMixer()->GetMasterSubmix();

			if (g_MeterSubmixFilter[0] != '\0' && !audEngineUtil::MatchName(submix->GetName(), g_MeterSubmixFilter))
			{
				continue;
			}

			// grab a local copy of effects metering info
			atRangeArray<audDspEffectId, audMixerSubmix::kMaxEffectsPerSubmix> effectTypes;
			atRangeArray<float, audMixerSubmix::kMaxEffectsPerSubmix> effectInputLevels;
			atRangeArray<float, audMixerSubmix::kMaxEffectsPerSubmix> effectMaxInputLevels;

			u32 numActiveEffects = 0;
			for(u32 i = 0; i  < audMixerSubmix::kMaxEffectsPerSubmix; i++)
			{
				effectTypes[i] = submix->GetEffectTypeId(i);
				if(effectTypes[i] != AUD_NUM_DSPEFFECTS)
				{
					numActiveEffects++;
				}
				effectInputLevels[i] = submix->GetAndResetEffectInputLevel(i);
				effectMaxInputLevels[i] = submix->GetMaxEffectInputLevel(i);
			}

			const u32 numChannels = submix->GetNumChannels();
			if(screenXOffset + numChannels*(meterWidth+meterSpacing) + numActiveEffects*(miniMeterSpacing+miniMeterWidth) > screenWidth)
			{
				screenXOffset = screenXOffset_start;
				screenYOffset += meterHeight + textHeight*3.f + submixYSpacing;				
			}
			
			Vec3V v0,v1;

			v0.SetXf(screenXOffset);
			v1.SetXf(screenXOffset + numChannels * (meterWidth+meterSpacing) + numActiveEffects * (miniMeterWidth+miniMeterSpacing) - meterSpacing);
			v0.SetZf(0.f);
			v1.SetZ(v0.GetZ());
			v0.SetYf(screenYOffset - meterHeight);
			v1.SetY(v0.GetY());

			grcDrawLine(v0,v1, white);

			v0.SetYf(screenYOffset - meterHeight * TransformAmplitudeToY(-6.f));
			v1.SetY(v0.GetY());
			grey.SetAlpha(128);
			grcDrawLine(v0,v1, grey);

			v0.SetYf(screenYOffset - meterHeight * TransformAmplitudeToY(-12.f));
			v1.SetY(v0.GetY());
			grey.SetAlpha(64);
			grcDrawLine(v0,v1, grey);

			v0.SetYf(screenYOffset - meterHeight * TransformAmplitudeToY(-24.f));
			v1.SetY(v0.GetY());
			grey.SetAlpha(32);
			grcDrawLine(v0,v1, grey);

			
			for(u32 e = 0; e < audMixerSubmix::kMaxEffectsPerSubmix; e++)
			{		
				if(effectTypes[e] != AUD_NUM_DSPEFFECTS || (g_MeterMaxLevels && effectMaxInputLevels[e] > 0.f))
				{
					const float clipThresh = 1.f;
					
					const float linearAmplitude = g_MeterMaxLevels ? effectMaxInputLevels[e] : effectInputLevels[e];
					const f32 dbVol = audDriverUtil::ComputeDbVolumeFromLinear_Precise(linearAmplitude);
					const bool isFinite = FPIsFinite(dbVol);
					const f32 val = TransformAmplitudeToY(isFinite ? dbVol : 12.f);
												
					const f32 leftX = screenXOffset + e * (miniMeterWidth + miniMeterSpacing);
					const f32 rightX = leftX + miniMeterWidth;
												
					const f32 bottomY = screenYOffset;
					const f32 topY = bottomY - val * meterHeight;

					Color32 bottomColour = linearAmplitude >= clipThresh ? Color32(255,0,0,200) : Color32(100,100,255,128);
					Color32 fullTopColour = linearAmplitude >= clipThresh ? Color32(255,0,0,200) : Color32(255,100,255,128);

					if(!isFinite)
					{
						bottomColour = fullTopColour = Color32(0,0,0,255);
					}

					Color32 topColour = Lerp(Clamp(val, 0.f, 1.f), bottomColour, fullTopColour);

					grcBegin(drawTriStrip, 4);
					grcColor(bottomColour);
					grcVertex2f(leftX, bottomY);
					grcColor(topColour);
					grcVertex2f(leftX, topY);
					grcColor(bottomColour);
					grcVertex2f(rightX, bottomY);
					grcColor(topColour);
					grcVertex2f(rightX, topY);
					grcEnd();
				}
			}

			for(u32 i = 0; i < numChannels; i++)
			{
				const f32 linVol = g_MeterMaxLevels ? submix->GetMaxPeakAmplitude(i) :
										(g_DrawRmsLevel ? submix->GetAndResetRmsAmplitude(i): submix->GetAndResetPeakAmplitude(i));

				if(linVol >= storedPeaks[submixId][i])
				{
					storedPeaks[submixId][i] = linVol;
					storedPeaksAlpha[submixId][i] = 1.f;
				}
				else
				{
					storedPeaksAlpha[submixId][i] *= storedPeakDecayFactor;
					if(storedPeaksAlpha[submixId][i] < 0.1f)
					{
						storedPeaksAlpha[submixId][i] = storedPeaks[submixId][i] = 0.f;
					}
				}
			
				const f32 dbVol = audDriverUtil::ComputeDbVolumeFromLinear(linVol);
				const bool isFinite = FPIsFinite(dbVol);
				const f32 val = TransformAmplitudeToY(isFinite ? dbVol : 12.f);
				
				const f32 leftX = screenXOffset + i * (meterWidth + meterSpacing) + numActiveEffects * (miniMeterWidth+miniMeterSpacing);
				const f32 rightX = leftX + meterWidth;

				const f32 bottomY = screenYOffset;
				const f32 topY = bottomY - val * meterHeight;

				Color32 bottomColour = Color32(50,0,255,255);
				Color32 fullTopColour = Color32(255,0,0,255);

				if(!isFinite)
				{
					bottomColour = fullTopColour = Color32(0,0,0,255);
				}

				Color32 topColour = Lerp(Clamp(val, 0.f, 1.f), bottomColour, fullTopColour);

				grcBegin(drawTriStrip, 4);
					grcColor(bottomColour);
					grcVertex2f(leftX, bottomY);
					grcColor(topColour);
					grcVertex2f(leftX, topY);
					grcColor(bottomColour);
					grcVertex2f(rightX, bottomY);
					grcColor(topColour);
					grcVertex2f(rightX, topY);
				grcEnd();

				// Draw peak hold line
				const f32 dbStoredPeak = audDriverUtil::ComputeDbVolumeFromLinear(storedPeaks[submixId][i]);
				const f32 storedPeakVal = TransformAmplitudeToY(dbStoredPeak);
				const f32 storedPeakY = bottomY - storedPeakVal*meterHeight;
				v0.SetXf(leftX);
				v1.SetXf(rightX);
				v0.SetYf(storedPeakY);
				v1.SetY(v0.GetY());
				Color32 storedPeakColor = Lerp(Clamp(storedPeakVal, 0.f, 1.f), bottomColour, fullTopColour);
				storedPeakColor.SetAlpha((u8)(storedPeaksAlpha[submixId][i]*255.f));
				grcDrawLine(v0,v1,storedPeakColor);


				char buf[32];
				if(!isFinite)
				{
					safecpy(buf, "INF");
				}
				else
				{
					if(storedPeaksAlpha[submixId][i] > 0.f)
					{
						formatf(buf, "%.0f", dbStoredPeak);
					}
					else
					{
						formatf(buf, "%.0f", dbVol);
					}
				}

				grcColor(white);
				grcDraw2dText(leftX, bottomY + (textHeight*(i%2)), buf);
			}
			
			grcColor(submix->IsRecordingOutput() ? red : white);

			char nameBuf[32];
			formatf(nameBuf, "%s%s %u", submix->GetName(), submix->GetBufferFormat() == Interleaved ? " (I)" : "", audDriver::GetMixer()->GetActiveVoiceCount(submix->GetSubmixId()));
			grcDraw2dText(screenXOffset, 
				screenYOffset - (meterHeight+textHeight), 
				nameBuf);


			screenXOffset += submixXSpacing + numChannels*(meterSpacing+meterWidth) + numActiveEffects * (miniMeterWidth+miniMeterSpacing);
		}

		if(audDriver::GetMixer()->GetLoudnessMeter())
		{
			Color32 bottomColour = Color32(50,0,255,255);
			Color32 fullTopColour = Color32(255,0,0,255);

			const float loudness = audDriver::GetMixer()->GetLoudnessMeter()->GetLoudness();

			
			const float val = ClampRange(loudness, -100.f, 0.f);
			Color32 topColour = Lerp(Clamp(val, 0.f, 1.f), bottomColour, fullTopColour);

			const f32 leftX = screenXOffset;
			const f32 rightX = leftX + meterWidth;

			const f32 bottomY = screenYOffset;
			const f32 topY = bottomY - val * meterHeight;

			grcBegin(drawTriStrip, 4);
			grcColor(bottomColour);
			grcVertex2f(leftX, bottomY);
			grcColor(topColour);
			grcVertex2f(leftX, topY);
			grcColor(bottomColour);
			grcVertex2f(rightX, bottomY);
			grcColor(topColour);
			grcVertex2f(rightX, topY);
			grcEnd();

			char buf[32];
			formatf(buf, "%.01f", loudness);

			grcColor(white);
			grcDraw2dText(leftX, bottomY, buf);

			grcDraw2dText(screenXOffset, 
				screenYOffset - (meterHeight+textHeight), 
				"Loudness");
		}
	}

	POP_DEFAULT_SCREEN();
	/*else if(g_DrawPhysPcmGenerators)
	{

		s32 physVoiceId = 0;
		for(u32 pcmSourceId = 0; pcmSourceId < kMaxPcmSources; pcmSourceId++)
		{

			if(g_PcmSourcePool->IsSlotInitialised(pcmSourceId))
			{
				audPcmSource *pcmSource = (audPcmSource*)g_PcmSourcePool->GetSlotPtr(pcmSourceId);
				Assert(pcmSource);
				if(pcmSource->HasBuffer(0))
				{
					// this PCM generator has a physical voice
					const char *typeName = NULL;
					const char *assetName = "(unknown asset)";
					u32 assetId = 0;
					switch(pcmSource->GetTypeId())
					{	
					case AUD_PCMSOURCE_WAVEPLAYER:
						{
							typeName = "WAVE";
							audWavePlayer *wavePlayer = (audWavePlayer*)pcmSource;
							assetName = wavePlayer->GetBankName();
							assetId = wavePlayer->GetWaveNameHash();
						}
						break;
					case AUD_PCMSOURCE_MODULARSYNTH:
						typeName = "SYNTH";
						//assetName = ((synthSynthesizer*)pcmSource)->GetName();
						break;
					case AUD_PCMSOURCE_GRAINPLAYER:
						typeName = "GRAIN";
						break;
					}

					char buf[128];
					formatf(buf, sizeof(buf), "%u: [%s] %s:%u", physVoiceId, typeName, assetName, assetId);
					grcDraw2dText((physVoiceId%2?600.f:70.f), 50.f + ((f32)floorf((physVoiceId/2.f))*12.f), buf);
					physVoiceId++;
				}
				else
				{
					// Playing virtually
				}
			}
		}
	}*/
	

#if !__FINAL
	if(g_audIsCapturingVirtualVoices)
	{
		PUSH_DEFAULT_SCREEN();
		grcColor(Color32(255,255,255));
		grcDraw2dText(70.f, 70.f, "CAPTURING VOICES");
		POP_DEFAULT_SCREEN();
	}		
#endif
#endif
}

#if __BANK
#if !__FINAL
void StartVoiceCaptureCB()
{
	audDriver::GetVoiceManager().StartVoiceCapture();
}

void StopVoiceCaptureCB()
{
	audDriver::GetVoiceManager().StopVoiceCapture();
}

void DumpQuietSynthsCB()
{
#if __ASSERT
	synthCore::DumpQuietAssetList();
#endif
}

#endif
void audVoiceMgr::AddWidgets(bkBank &bank)
{
	bank.PushGroup("audVoiceMgr", false);
		bank.AddSlider("PhysVoiceBudget", &m_PhysicalVoiceBudget, 0, 20000, 1);
		bank.AddToggle("DrawPhysVoices", &g_DrawPhysicalVoices);
		bank.AddToggle("PrintPhysVoices", &g_PrintPhysicalVoices);
		bank.AddToggle("DrawNoGaps", &g_DrawNoGaps);
		bank.AddToggle("Draw AUD_PCMSOURCE_WAVEPLAYER", &g_DrawPCMSource[AUD_PCMSOURCE_WAVEPLAYER]);
		bank.AddToggle("Draw AUD_PCMSOURCE_GRAINPLAYER", &g_DrawPCMSource[AUD_PCMSOURCE_GRAINPLAYER]);
		bank.AddToggle("Draw AUD_PCMSOURCE_MODULARSYNTH", &g_DrawPCMSource[AUD_PCMSOURCE_MODULARSYNTH]);
		bank.AddToggle("Draw AUD_PCMSOURCE_SUBMIX", &g_DrawPCMSource[AUD_PCMSOURCE_SUBMIX]);
		bank.AddToggle("Draw AUD_PCMSOURCE_SYNTHCORE", &g_DrawPCMSource[AUD_PCMSOURCE_SYNTHCORE]);
		bank.AddToggle("Draw AUD_PCMSOURCE_EXTERNALSTREAM", &g_DrawPCMSource[AUD_PCMSOURCE_EXTERNALSTREAM]);
		bank.AddSlider("DrawVoiceData", &g_DrawBufferDataId, 0, 100000, 1);
#if !__FINAL
		bank.AddButton("StartVoiceCapture", CFA(StartVoiceCaptureCB));
		bank.AddButton("StopVoiceCapture", CFA(StopVoiceCaptureCB));
#endif
		bank.AddButton("OverrideUserMusic", datCallback(MFA(audVoiceMgr::OverrideUserMusic),(datBase*)this));
		bank.AddButton("RestoreUserMusic", datCallback(MFA(audVoiceMgr::RestoreUserMusic),(datBase*)this));
		bank.AddButton("DumpQuietSynths", CFA(DumpQuietSynthsCB));
	bank.PopGroup();
}
#endif

#if !__FINAL

void audVoiceMgr::StartVoiceCapture() const
{
	if(g_audVoiceCaptureBuffer)
	{
		audVoiceCaptureStart startPacket;
		startPacket.numWaveSlots = audWaveSlot::GetNumWaveSlots();
		startPacket.waveSlotTableSize = 0;
		for(u32 i = 0;i < startPacket.numWaveSlots; i++)
		{
			const char *bankName = (audWaveSlot::GetWaveSlotFromIndex((s32)i)->GetLoadedBankName()?audWaveSlot::GetWaveSlotFromIndex((s32)i)->GetLoadedBankName():"");
			startPacket.waveSlotTableSize += ustrlen(bankName) + 1;
		}
		startPacket.numVirtualVoices = m_MaxVirtualVoices;
		sysMemCpy(g_audVoiceCapturePtr, &startPacket, startPacket.packetSize);
		g_audVoiceCapturePtr += startPacket.packetSize;
		for(u32 i = 0;i < startPacket.numWaveSlots; i++)
		{
			const char *bankName = (audWaveSlot::GetWaveSlotFromIndex((s32)i)->GetLoadedBankName()?audWaveSlot::GetWaveSlotFromIndex((s32)i)->GetLoadedBankName():"");;
			const size_t len = strlen(bankName) + 1;
			sysMemCpy(g_audVoiceCapturePtr, bankName, len);
			g_audVoiceCapturePtr += len;
		}

		g_audIsCapturingVirtualVoices = true;
	}
}

void audVoiceMgr::StopVoiceCapture() const
{
	audVoiceCaptureEnd endPacket;
	if(g_audVoiceCaptureBuffer + g_audVoiceCaptureBufferSize > g_audVoiceCapturePtr + endPacket.packetSize)
	{
		WriteVoiceCapturePacket(endPacket);
	}
	g_audIsCapturingVirtualVoices = false;
	DumpVoiceCaptureToFile();
	g_audVoiceCapturePtr = g_audVoiceCaptureBuffer;
}

void audVoiceMgr::CaptureFrameStart() const
{
	if(g_audIsCapturingVirtualVoices)
	{
		audVoiceCaptureFrameStart startPacket(g_AudioEngine.GetTimeInMilliseconds());
		WriteVoiceCapturePacket(startPacket);
	}
}

void audVoiceMgr::CaptureFrameEnd() const
{
	if(g_audIsCapturingVirtualVoices)
	{
		audVoiceCaptureFrameEnd endPacket;
		WriteVoiceCapturePacket(endPacket);
	}
}

void audVoiceMgr::WriteVoiceCapturePacket(const audVoiceCapturePacket &packet) const
{
	if(g_audIsCapturingVirtualVoices)
	{
		Assert(g_audVoiceCapturePtr);
		if(g_audVoiceCapturePtr + packet.packetSize < g_audVoiceCaptureBuffer + g_audVoiceCaptureBufferSize)
		{
			sysMemCpy(g_audVoiceCapturePtr, &packet, packet.packetSize);
			g_audVoiceCapturePtr += packet.packetSize;
		}
		else
		{
			StopVoiceCapture();
		}
	}
}

void audVoiceMgr::DumpVoiceCaptureToFile() const
{
	fiStream *stream = ASSET.Create("c:\\voiceCapture", "dmp", false);
	if(stream)
	{
		stream->Write(g_audVoiceCaptureBuffer, ptrdiff_t_to_int(g_audVoiceCapturePtr - g_audVoiceCaptureBuffer));
		stream->Close();
	}
}
#endif

} // namespace rage
