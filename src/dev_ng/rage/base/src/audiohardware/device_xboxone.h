//
// audiohardware/device_xboxone.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DEVICE_XBOXONE_H
#define AUD_DEVICE_XBOXONE_H

#if RSG_DURANGO

#include "device.h"

#include <Audioclient.h>
#include <Mmdeviceapi.h>

#include "system/ipc.h"
#include "system/xtl.h"
#include "grcore/config.h"
#include "atl/queue.h"

#include <xaudio2.h>

#define AUD_OUTPUT_TO_FILE		0

namespace rage
{
	enum XAudioEngineState {
		Stopped = 0,
		Started,
		NeedsToStop,
		NeedsToStart
	};

	enum { kXAudioInstanceNumberOfBuffers = 3 };

	class sysMemAllocator;
	class audXAudio2Instance : public IXAudio2VoiceCallback
	{
	public:
		enum InstanceType {
			Main = 0,
			RestrictedContent,
		};

		enum BufferState {
			Free = 0,
			Mixing,
			Submitting,
			Submitted,
			Done
		};

		audXAudio2Instance();
		virtual ~audXAudio2Instance() {}
		

		bool Init(const InstanceType type);
		void Shutdown();

		//////////////////////////////////////////////////////////
		// IXAudio2VoiceCallback interface
		STDMETHOD_(void, OnBufferStart)(void *pContext);
		STDMETHOD_(void, OnBufferEnd)(void *pContext);

		STDMETHOD_(void, OnVoiceProcessingPassStart)(UINT32){}
		STDMETHOD_(void, OnVoiceProcessingPassEnd)(void){}
		STDMETHOD_(void, OnStreamEnd)(void){}
		STDMETHOD_(void, OnLoopEnd)(void *){}
		STDMETHOD_(void, OnVoiceError)(void *,HRESULT){}
		///////////////////////////////////////////////////////////

		IXAudio2 *GetXAudio2()	{ return m_XAudio2; }

		void StartMixing();
	
		void OnBufferReady();
		void StartEngine()		{ 
									if(m_XAudio2)
									{
										HRESULT hr = m_XAudio2->StartEngine();
										if(hr != S_OK)
										{
											Displayf("m_XAudio2->StartEngine() error %x", hr);
										}
									}
								}	

		void StopEngine()		{	if(m_XAudio2) m_XAudio2->StopEngine();	}
		
#if __BANK
		void DumpOutput(u32 channelIndex, f32 data); 

		u32 m_DroppedSubmitCount;
		u32 m_SkippedSubmitCount;

		sysPerformanceTimer* m_OnBufferStartFrameTimer;
#endif
	
		s32 GetFreeOutputBuffer();
		void FreeOutputBuffer(s32 id);

		//bool m_BufferPoolFree[kXAudioInstanceNumberOfBuffers];
		BufferState m_BufferState[kXAudioInstanceNumberOfBuffers];
		f32* m_BufferPool[kXAudioInstanceNumberOfBuffers];
		atQueue<s32, kXAudioInstanceNumberOfBuffers> m_OutputBufferQueue;

	private:		

		IXAudio2 *m_XAudio2;
		IXAudio2MasteringVoice *m_MasteringVoice;
		IXAudio2SourceVoice *m_OutputVoice;

		sysMemAllocator *m_Allocator;
		InstanceType m_Type;

		static bool sm_HaveMasterInstance;
		bool m_MasterInstance;

		static const u32 kMaxHardwareChannels = 8;

#if __BANK
		atRangeArray<u32, kMaxHardwareChannels> m_NumOutputDataBytes;
		atRangeArray<fiStream *, kMaxHardwareChannels> m_OutputStreams;
#endif
	};

	
	class fiStream;
	class audMixerDeviceXAudio2 : public audMixerDevice
	{
	public:

		audMixerDeviceXAudio2();

		// PURPOSE
		//	Initialises XAudio2 output hardware
		// RETURNS
		//	false on failure
		virtual bool InitHardware();

		// PURPOSE
		//	Releases XAudio2 output resources
		virtual void ShutdownHardware();

		// PURPOSE
		//	Starts generating output
		virtual void StartMixing();
		virtual void StartEngine();
		virtual void StopEngine();

		virtual void TriggerUpdate();
		virtual void WaitOnMixBuffers();
		virtual void SignalWaitOnMixBuffers()		{ sysIpcSignalSema(sm_MixBuffersSema); }

		IXAudio2 *GetXAudio2() { return m_XAudio2Instance.GetXAudio2(); }

		static void SetXaudioSuspended(bool suspend);

		static sysCriticalSectionToken sm_MixQueueCritSec;

		static XAudioEngineState sm_EngineState;

	private:

		static DECLARE_THREAD_FUNC(sMixLoop);

		void MixLoop();

		audXAudio2Instance m_XAudio2Instance;

		audXAudio2Instance m_RestrictedInstance;

		static sysIpcSema sm_TriggerUpdateSema; // for capturing
		static sysIpcSema sm_MixBuffersSema;	// for capturing
		static bool sm_WasCapturing;

		static ServiceDelegate sm_Delegate;
		static void HandlePlmChange(sysServiceEvent* evt);

		static DECLARE_THREAD_FUNC(SuspendMixerThreadEntryProc);

		sysMemAllocator *m_Allocator;

		static sysPerformanceTimer* sm_PerfTimer;

		static volatile bool sm_IsMixLoopThreadRunning, sm_ShouldMixLoopThreadBeRunning;
		static sysIpcThreadId sm_MixLoopThreadId;

#if __BANK
		void ShowOutputBufferQueue();
#endif

#if AUD_OUTPUT_TO_FILE
		fiStream *m_DebugOutput;
#endif
		
	public:
		static u32 sm_BufferSize;
		static u32 sm_OutputBufferSizeBytes;
		static f32 *sm_EmptyOutputBuffer;
		static u32 sm_NumHardwareChannels;

		static s32 sm_BuffersToDropAfterCapturing;

	};

} // namespace rage

#endif // __WIN32
#endif // AUD_DEVICE_XBOXONE_H
