//
// audiohardware/voice_xaudio2.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_VOICE_XAUDIO2_H
#define AUD_VOICE_XAUDIO2_H

#if __XENON

#include "driver_xenon.h"

#include "voice_physical.h"

#include "streamingwaveslot.h"

#include "audioeffecttypes/biquadfiltereffect.h"
#include "system/xtl.h"
#include <xaudio2.h>

namespace rage {

	class fiStream;

	//
	// PURPOSE
	//  The Xenon implementation of a physical (hardware) voice. Physically renders a Wave on Xenon.
	// SEE ALSO
	//  audVoicePhysical
	//
	class audVoiceXAudio2 : public audVoicePhysical
	{
	public:
		//
		// PURPOSE
		//  Initializes member variables.
		//
		audVoiceXAudio2();
		//
		// PURPOSE
		//  Releases the XAudio2 hardware voice (if initialized.)
		//
		virtual ~audVoiceXAudio2(void);
		//
		// PURPOSE
		//  Initializes the voice prior to use. Creates and initializes an XAudio2 source voice and audio packet.
		void Init(const audVoiceSettings *voiceSettings);
		//
		// PURPOSE
		//	Shuts down this instance.
		//
		void Shutdown();
		//
		// PURPOSE
		//	Implements audVoicePhysical::Play for Xenon.
		// SEE ALSO
		//	audVoicePhysical::Play
		//
		void Play(s32 startOffsetMs = 0);
		//
		// PURPOSE
		//	Implements audVoicePhysical::StopPhysically for Xenon.
		// SEE ALSO
		//	audVoicePhysical::StopPhysically
		//
		void StopPhysically(void);
		//
		// PURPOSE
		//	Implements audVoicePhysical::IsPlaying for Xenon.
		// SEE ALSO
		//	audVoicePhysical::IsPlaying
		//
		__inline bool IsPlaying(void)
		{
			return (m_BitFlags.HasStartedVoice 
					|| (m_BitFlags.IsPaused && m_BitFlags.ShouldPlayOnUnpause) 
					|| m_VoiceSettings->Flags.ShouldStartPhysicalPlayback);
		}
		//
		// PURPOSE
		//	Implements audVoicePhysical::IsSampleDataRequired for Xenon.
		// SEE ALSO
		//	audVoicePhysical::IsSampleDataRequired
		//
		__inline bool IsSampleDataRequired(void)
		{
			return m_NeedsNewData;
		}
		//
		// PURPOSE
		//	Implements audVoicePhysical::AddSampleData for Xenon.
		// SEE ALSO
		//	audVoicePhysical::AddSampleData
		//
		void AddSampleData(u32 startOffsetMs=0);
		//
		// PURPOSE
		//	Implements audVoicePhysical::GetPlayTimeMs for Xenon.
		// SEE ALSO
		//	audVoicePhysical::GetPlayTimeMs
		//
		s32 GetPlayTimeMs(void);
		//
		// PURPOSE
		//	Performs all per-audio-frame operations - smoothly updating hardware volumes and obtaining status
		//	information from the hardware voice.
		//
		void Update(void);

		// PURPOSE
		//	Returns the start offset in milliseconds that was requested when this voice was played
		s32 GetStartOffsetMs() const
		{
			return m_StartOffsetMs;
		}

		// PURPOSE
		//	Returns the previous playtime in milliseconds
		s32 GetPreviousPlaytimeMs() const
		{
			return m_PreviousPlayTimeMs;
		}

	protected:

		// PURPOSE
		//	Returns true if the voice should smooth its volume, ie it has already begun proper playback
		__inline bool ShouldSmoothVolume() const
		{
			return ((m_BitFlags.HasVoiceUpdated && m_BitFlags.HasStartedVoice) 
				|| (m_BitFlags.IsPaused && m_BitFlags.ShouldPlayOnUnpause));
		}

		//
		// PURPOSE
		//	Calculates and applies the individual (speaker) channel volumes based upon the current speaker mask, pan or
		//	position and the current wideband and LFE hardware volumes.
		//
		void ApplyHardwareVolumes();
		//
		// PURPOSE
		//	Sets the frequency scaling factor.
		// PARAMS
		//	scalingFactor - The frequency scaling factor to be applied.
		// NOTES
		//	A frequency scaling factor of 0.0f equates to a paused request.
		//
		void SetFrequencyScalingFactor(f32 scalingFactor);
		//
		// PURPOSE
		//	Calculates the current play position in samples.
		// RETURNS
		//	The current play position in samples, or 0 if no source voice has been allocated.
		//
		u32 GetPlaybackPositionSamples(void);
		//
		// PURPOSE
		//	Converts from milliseconds to bytes, using the audDriverUtil::ConvertMsToSamples function.
		// SEE ALSO
		//	audDriverUtil::ConvertMsToSamples
		//
		u32 ConvertMsToBytes(u32 numMilliseconds);
		//
		// PURPOSE
		//	Uses the XMA seek table to convert from a sample offset to a frame offset (in bits, with the packet) and a sample
		//	index (within the frame.)
		// PARAMS
		//	sampleOffset		- The required sample offset.
		//	frameOffsetBits		- Used to return the calculated frame offset (within the packet), in bits.
		//	sampleIndex			- Used to return the sample index (within the frame), or -1 if the requested sample offset is
		//							beyond the end of the XMA data.
		//
		void ComputePacketOffsetBitsAndSampleIndexFromSamples(u32 sampleOffset, u32 &frameOffsetBits, s32 &sampleIndex, const u32 seekTableEntries, const u32 *seekTable, const u8 *waveData, const u32 waveLengthBytes);


		XAUDIO2_BUFFER m_WavePackets[audStreamingWaveSlot::kNumStreamingBlocksPerWaveSlot];		

		IXAudio2SourceVoice *m_SourceVoice;									// 4
		IXAudio2SubmixVoice *m_SourceEffectSubmixVoice;						// 4
		
		u32 m_StartOffsetSamples;											// 4
		u32 m_TimeStopped;
		u32 m_WaveLengthSamples;
		s32 m_PreviousPlayTimeMs;
		s32 m_StartOffsetMs;
		u8 m_NumFramesFrozen;
		u8 m_WavePacketIndex;												// 1
		bool m_NeedsNewData;
		bool m_SubmittedFirstPacket;
	};

} // namespace rage

#endif // __XENON

#endif // AUD_VOICE_XAUDIO2_H
