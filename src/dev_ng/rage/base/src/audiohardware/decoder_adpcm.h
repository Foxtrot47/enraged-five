//
// audiohardware/decoder_adpcm.h
//
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DECODER_ADPCM_H
#define AUD_DECODER_ADPCM_H

#include "driverdefs.h"

#if RSG_AUDIO_X64

#include "decoder.h"
#include "ringbuffer.h"
#include "IMA_ADPCM.h"

#define DUMP_RAW_PC_STREAMS 0

namespace rage
{

// This size is based on each 2048 byte packet having a 4 byte header and 2044 bytes of adpcm data which equals
// 8176 bytes of uncompressed pcm data or 4088 samples
enum {kAdpcmBlockBytesToDecode = 2044};
enum {kAdpcmDecompressedBytesPerBlock = 8176};
enum {kAdpcmDecompressedSamplesPerBlock = 4088};
enum {kAdpcmBlockSize = 2048};

		struct audAdpcmPacket
		{
			audAdpcmPacket() :
				NumBlocksAvailable(0),
				InputBytes(0),
				InputData(NULL),
				NumADPCMBytesConsumed(0),
				NumSamplesConsumed(0),
				PlayBegin(0),
				PlayEnd(0),
				LoopBeginSamples(~0u)
			{}
			u8* InputData;
			u32 InputBytes;
			u32 PlayBegin;
			u32 PlayEnd;
			u32 LoopBeginSamples;
			bool IsFinalBlock;
			u32 NumADPCMBytesConsumed;
			u32 NumSamplesConsumed;
			u32 NumBlocksAvailable;
		};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
class ALIGNAS(16) audDecoderAdpcm : public audDecoder
{
public:
	
	virtual bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate));
	virtual void Shutdown();

	virtual audDecoderState GetState() const
	{
		return m_State;
	}
	virtual void SubmitPacket(audPacket &packet);
	virtual void Update();
	virtual u32 ReadData(s16 *dest, const u32 numSamples) const;
	virtual void AdvanceReadPtr(const u32 numSamples);
	virtual u32 QueryNumAvailableSamples() const;

	virtual bool QueryReadyForMoreData() const;

	virtual void SetWaveIdentifier(const u32 waveIdentifier) { m_WaveIdentifier = waveIdentifier; }

	static s32 sm_DecodesThisFrame;

private:

	IMA_ADPCM	m_Adpcm;

	atRangeArray<audAdpcmPacket, 2> m_Packets;
	u32 m_SubmitPacketIndex;
	u32 m_DecodePacketIndex;

	audStaticRingBuffer<kAdpcmDecompressedBytesPerBlock*4> m_RingBuffer;
	audDecoderState m_State;
	audWaveFormat::audStreamFormat m_Format;
	u32 m_NumChannels;
	u32 m_WaveIdentifier;
	s32 m_NumQueuedPackets;
	bool m_ReadyForMoreData;

	bool m_DecodedLastFrame;
	s32 m_SkippedDecodeCount;

#if DUMP_RAW_PC_STREAMS
	FILE* m_fp;
#endif

};
#if __WIN32
#pragma warning(pop)
#endif

} // namespace rage

#endif //RSG_AUDIO_ADPCM

#endif // AUD_DECODER_ADPCM_H
