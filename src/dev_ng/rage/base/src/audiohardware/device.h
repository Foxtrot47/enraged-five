//
// audiohardware/device.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_MIXERDEVICE_H
#define AUD_MIXERDEVICE_H

#include "decodemgr.h"
#include "framebufferpool.h"
#include "mixervoice.h"
#include "mixer.h"
#include "pcmsource.h"
#include "submix.h"
#include "submixcache.h"
#include "syncsource.h"

#include "system/performancetimer.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "atl/array.h"
#include "atl/bitset.h"

#include "audiosynth/blocks.h"


//#define OUTPUT_RENDERED_AUDIO
//#define OUTPUT_MIXBUFFER_AUDIO

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) //structure was padded due to __declspec(align())
#endif


namespace rage
{
#if RSG_PC
	enum { kAudioNumberOfCommandBuffers = 3 };
#else
	enum { kAudioNumberOfCommandBuffers = 2 };
#endif

	enum { kAudioBuffersToDropAfterCapturing = 200 };

	struct tBiquadCoefficients;
	struct tBiquadHistory;
	class audCommandBuffer;

	struct audMixerClientThreadState
	{
		audMixerClientThreadState() : Name(NULL), ReadIndex(0),BuffersSubmitted(0),PrevBufferTimeProcessed(0)
		{
			if(kAudioNumberOfCommandBuffers > 1)
			{
				WriteIndex = 1;
			}
			else
			{
				WriteIndex = 0;
			}
			
		}
		atFixedArray<audCommandBuffer, kAudioNumberOfCommandBuffers> CommandBuffers;
		const char *Name;

		volatile int ReadIndex;
		volatile int WriteIndex;
		volatile u32 BuffersSubmitted;
		volatile u32 PrevBufferTimeProcessed;
	};

	class IAudioMixCapturer
	{
	public:
		virtual bool IsCapturing() const = 0;
		virtual void CaptureMix( s16 const * const samples, size_t const sampleCount, size_t const channels ) = 0;
	};

// PURPOSE
//	This class implements a buffer based multichannel output software mixer.  All mixing is done as 32bit floating
//	point, it supports an arbitrary number of output channels and submix routing
//
// SEE ALSO
//	audMixerVoice, audMixerSubmix
class audMixerDevice
{
	friend class audMixerVoice;
	friend class audMixerSubmix;
	friend class audPcmSourceInterface;
	friend class synthCoreTest;
	friend class synthCoreTestManager;
	
public:
	// PURPOSE
	//	Default constructor
	audMixerDevice(void);

	// PURPOSE
	//	Destructor, does nothing - use Shutdown() to free resources
	virtual ~audMixerDevice(void);

	// PURPOSE
	//	Initializes internal state and output device
	// PARAMS
	//	numBuffers - number of voices to create
	// NOTES
	//	This will create the mixer worker thread
	// RETURNS
	//	true if success, false otherwise
	bool Init(const u32 numVoices);

	// PURPOSE
	//	Free hardware device, free all mix buffers
	void Shutdown();

	audMixerVoice *InitVoice(const u32 index);

	s32 GetVoiceIndex(const audMixerVoice *voice) const;
	audMixerVoice *GetVoiceFromIndex(const s32 index)
	{
		return &m_Voices[index];
	}

	// PURPOSE
	//	Allocates and initialises a PCM generator synchronously
	// NOTES
	//	Can only be used on the mixer thread
	s32 AllocatePcmSource_Sync(const audPcmSourceTypeId typeId);

	// PURPOSE
	//	Returns a pointer to the requested pcm generator slot id
	audPcmSource *GetPcmSource(const s32 slotId);

	// PURPOSE
	//	Returns the 0-based slot index of the specified pcm generator
	s32 GetPcmSourceSlotId(const audPcmSource *pcmSource);

	// PURPOSE
	//	Returns whether or not the mixer device has been successfully initialised
	// RETURNS
	//	true if this mixer device has been successfully initialised, false otherwise
	bool IsInitialised() const
	{
		return m_IsInitialised;
	}

	// PURPOSE
	//	Returns the number of output channels this device has been configured with (i.e 6 for 5.1 etc)
	u32 GetNumOutputChannels() const
	{
		return m_NumOutputChannels;
	}

	// PURPOSE
	//	Returns the size of the hardware output buffer in samples
	u32 GetDeviceLatency() const;

	// PURPOSE
	//	Returns a pointer to the master (final output) submix
	audMixerSubmix *GetMasterSubmix()
	{
		return GetSubmixFromIndex(0);
	}

	// PURPOSE
	//	Returns a pointer to the mixer output buffer
	f32 *GetOutputBuffer();

	audMixerSubmix *GetSubmixFromIndex(const s32 id)
	{
		return &m_Submixes[id];
	}

	void ComputeProcessingGraph();
	s32 GetSubmixIndex(audMixerSubmix *const submix) const;
	u32 GetNumSubmixes() const { return m_Submixes.GetCount(); }
	u32 LookupSubmixProcessingOrder(const u32 index) { return m_SubmixProcessingOrder[index]; }
	s32 GetNumSubmixesInProcessingOrder() { return m_SubmixProcessingOrder.GetCount(); }

	audMixerSubmix *CreateSubmix(const char *name, const u32 numChannels, const bool hasVoiceInputs);

	// PURPOSE
	//	Recreates all mix buffers
	// NOTES
	//	This must be called to change the latency/number of output channels
	void ReinitialiseMixBuffers(const u32 numOutputChannels);

	// PURPOSE
	//	Returns the real-time mixer timer, in seconds
	double GetMixerTimeS() const { return GetMixerTimeSamples() / ((double)kMixerNativeSampleRate); }

	// PURPOSE
	//	Gets the number of samples produced
	u64 GetMixerTimeSamples() const { return m_MixTimerFrames * kMixBufNumSamples; }

	// PURPOSE
	//	Returns number of mixer frames produced
	u32 GetMixerTimeFrames() const { return m_MixTimerFrames; }

	// PURPOSE
	//	Returns the mixer timer in milliseconds
	u32 GetMixerTimeMs() const { return static_cast<u32>(GetMixerTimeSamples() / (kMixerNativeSampleRate / 1000)); }

	audDecodeManager &GetDecodeMgr() 
	{
		return m_DecodeManager;
	}

	// PURPOSE
	//	Mixes one mix buffers worth from all playing buffers into the output buffers
	void MixBuffers();

	// PURPOSE
	//	Notifies the device of routing changes.  The processing graph will be recomputed next mixer frame
	void FlagGraphComputationRequired()
	{
		m_RecomputeGraph = true;
	}

	// PURPOSE
	//	Accessor for the frame buffer pool
	audFrameBufferPool &GetFrameBufferPool()
	{
		return m_FrameBufferPool;
	}

	// PURPOSE
	//	Lock the mix thread
	void EnterBML()
	{
		m_BigMixerLock.Lock();
	}

	// PURPOSE
	//	Unlock the mix thread
	void ExitBML()
	{
		m_BigMixerLock.Unlock();
	}

	void EnterReplaySwitchLock()
	{
		m_ReplaySwitchCritSection.Lock();
	}

	void ExitReplaySwitchLock()
	{
		m_ReplaySwitchCritSection.Unlock();
	}

	void EnterReplayMixerSwitchLock()
	{
		m_ReplayMixerSwitchCritSection.Lock();
	}

	void ExitReplayMixerSwitchLock()
	{
		m_ReplayMixerSwitchCritSection.Unlock();
	}

	// PURPOSE
	//	Sets up a command buffer for the calling thread.  This should be called before attempting to
	// interact with any mixer-owned objects
	// PARAMS
	//	threadName - for debug purposes
	//	bufferSize - size in bytes of the command buffer to instantiate for this thread
	s32 InitClientThread(const char *threadName, const u32 bufferSize);
		
	audCommandBuffer *GetCommandBufferForThread() const;

	// PURPOSE
	//	This tells the mixer that the thread-specific command buffer is ready to be processed.  The
	//	mixer thread will process the commands at the start of the next mixer frame (ie in 5.3ms or less)
	void FlagThreadCommandBufferReadyToProcess(const u32 timestamp = 0);
	

	// PURPOSE
	//	This function blocks until the mixer thread has finished processing the thread-specific command
	//	buffers, at which point the thread is free to start populating them again
	void WaitOnThreadCommandBufferProcessing() const;

	// PURPOSE
	//	Returns the high watermark for the calling threads command buffer, in bytes
	u32 GetThreadCommandBufferHighWatermark() const;

	audMixerSyncManager &GetSyncManager()
	{
		return m_SyncManager;
	}

	const audMixerSyncManager &GetSyncManager() const
	{
		return m_SyncManager;
	}

#if !__FINAL
	void RunTests();
#endif

	enum audCommandSystemIds
	{
		COMMAND_VOICE = 0,
		COMMAND_SUBMIX,
		COMMAND_PCMSOURCE,
	};

	enum {kMaxOutputChannels = 6};

	DEV_ONLY(void DebugTraceCurrentCommandBuffer());

#if __BANK
	void DrawDebug();
	float GetPhysicalPcmSourceCost() const { return m_PhysicalPcmSourceCost; }
	const audSurroundLoudnessMeter *GetLoudnessMeter() const { return m_LoudnessMeter; }
#endif

	u32 GetActiveVoiceCount(const s32 submixId) const
	{
		return m_SubmixVoiceCount[submixId];	
	}

	void SetPaused(const bool paused)
	{
		m_Paused = paused;
	}
	void SetSuspended(const bool suspended)
	{
		m_Suspended = suspended;
	}
	bool IsSuspended()		{ return m_Suspended; }
	bool IsPaused() const;

	void AddEndPoint(audMixerSubmix *submix);

	void SetCaptureMode(const bool isCapturing)	
	{
		m_IsCapturing = isCapturing;
	}
	void SetIsFrameRendering(const bool isFrameRendering)
	{
		m_IsFrameRendering = isFrameRendering;
	}
	void SetIsFixedFrameReplay(const bool isFixedFrameReplay)
	{
		m_IsFixedFrameReplay = isFixedFrameReplay;
	}
	bool IsCapturing()			{ return m_IsCapturing; }
	bool IsFrameRendering()		{ return m_IsFrameRendering; }
	bool IsFixedFrameReplay()	{ return m_IsFixedFrameReplay; }

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
	void StartAudioCapture( IAudioMixCapturer& capturer );
	void StopAudioCapture();

	void UpdateAudioCapture();

	void SetReplayMixerModeLoading();
	void SetReplayMixerModeRendering();

	void PrepareForReplayRender();
	void CleanUpReplayRender();
#endif // defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

	void ResetAVRenderedTimes()							{ m_TotalAudioTimeNs = m_TotalVideoTimeNs = 0; }
	s64 GetTotalVideoTimeNs()							{ return m_TotalVideoTimeNs; }		
	s64 GetTotalAudioTimeNs()							{ return m_TotalAudioTimeNs; }		
	void AddVideoTimeNs(s64 frameTime)					{ m_TotalVideoTimeNs += frameTime; }
	void AddAudioTimeNs(s64 amountOfTime)				{ m_TotalAudioTimeNs += amountOfTime; }

	void SetMainOutputSubmix(audMixerSubmix *submix) { m_MainOutputSubmix = submix; }
	void SetRestrictedOutputSubmix(audMixerSubmix *submix) { m_RestrictedOutputSubmix = submix; }
		
	audMixerSubmix *GetMainOutputSubmix() { return m_MainOutputSubmix ? m_MainOutputSubmix : GetMasterSubmix(); }
	audMixerSubmix *GetRestrictedOutputSubmix() { return m_RestrictedOutputSubmix; }

	virtual void ReinitHardware() {}
	virtual void StartEngine() {}
	virtual void StopEngine() {}

	virtual void SuspendedMixerUpdate() {}		// we will use this to pump the mixer when capturing replays
	virtual void TriggerUpdate() {}
	virtual void WaitOnMixBuffers() {}
	virtual void SignalWaitOnMixBuffers() {}

	void PauseGroup(const s32 groupId, const bool isPaused);
	bool IsPaused(const s32 groupId) const { return m_PauseGroupState[groupId]; }

	bool m_ShouldStartFixedFrameRender;
	bool m_ShouldStopFixedFrameRender;

protected:

	bool WriteCommand(const audCommandPacketHeader *packet);
	bool WriteAlignedCommand(const audCommandPacketHeader *packet);
	
	void ExecuteCommandBuffers();
	void ExecuteCommandPacket(const u32 timestamp, const audCommandPacketHeader *packet);
	void DebugTraceCommandPacket(const u32 timestamp, const audCommandPacketHeader *packet);

	void GeneratePcm();

	virtual bool InitHardware() {return true;}
	virtual void ShutdownHardware(){}
	virtual void StartMixing(){}

	// PURPOSE
	//	Stops all buffers in the buffer entry list
	void StopAllBuffers();

	// Fixed array of submixes
	atFixedArray<audMixerSubmix, kMaxSubmixes> m_Submixes ;
	atFixedArray<audMixerVoice, kMaxVoices> m_Voices ;

	atRangeArray<u32, kMaxSubmixes> m_SubmixVoiceCount ;


	enum { kMaxEndPoints = 4 };
	atFixedArray<audMixerSubmix *, kMaxEndPoints> m_EndPoints;

	atFixedArray<u32, kMaxSubmixes> m_SubmixProcessingOrder;
	
	audDecodeManager m_DecodeManager;
	audSubmixCache m_SubmixCache;
	audPcmSourcePool m_PcmSourcePool;
	audFrameBufferPool m_FrameBufferPool;

	sysCriticalSectionToken m_TimerLock;

	u32 m_NumVoices;
	
	u32 m_NumOutputChannels;	
	u32 m_MixTimerFrames;	
	s32 m_NumProcessingStages;

	atArray<audMixerClientThreadState*> m_ClientThreads;
	bool m_IsInitialised;
	bool m_HPFEnabled, m_LPFEnabled;
	bool m_RecomputeGraph;
    bool m_FinalOutputDownmixed;

	BANK_ONLY(audSurroundLoudnessMeter *m_LoudnessMeter);
	BANK_ONLY(float m_PhysicalPcmSourceCost);
	DEV_ONLY(audCommandBuffer *m_CurrentlyProcessingCommandBuffer);

	u32		m_StartWaitSemaphoreCount;
	u32		m_StartSignalSemaphoreCount;

	u32		m_EndWaitSemaphoreCount;
	u32		m_EndSignalSemaphoreCount;

	private:
		// PURPOSE
		//	Marks the specified pcm generator slot as free
		void FreePcmSource(const s32 slotId);

		void ComputeProcessingGraph(audMixerSubmix *submix, s32 stage);		
		
		audMixerSyncManager m_SyncManager;

		sysCriticalSectionToken m_BigMixerLock;
		sysCriticalSectionToken m_ReplaySwitchCritSection;
		sysCriticalSectionToken m_ReplayMixerSwitchCritSection;
		
		volatile bool	m_Paused;
		void*			m_mixCapturer;
		audMixerSubmix *m_MainOutputSubmix;
		audMixerSubmix *m_RestrictedOutputSubmix;

		bool	m_Suspended;				// true when the game is suspended by user, or in the case of PC there is no hardware(XAudio OnBufferStart is suspended)
		bool	m_IsCapturing;				// Active when capturing audio, suspends output to the audio device. Mixer is pumped manually.
		bool	m_IsFixedFrameReplay;		// True if commandline param fixedtimevideobake is true
		volatile bool	m_IsFrameRendering;
		s64		m_TotalVideoTimeNs;
		s64		m_TotalAudioTimeNs;

		enum {kMaxPauseGroups = 1};
		atRangeArray<bool, kMaxPauseGroups> m_PauseGroupState;

};

#if __WIN32
#pragma warning(pop)
#endif

struct audInitialisePcmSourceCommand : public audCommandPacketHeader
{		
	audInitialisePcmSourceCommand()
	{
		packetSize = sizeof(audInitialisePcmSourceCommand);
		commandId = audPcmSourceInterface::INITIALISE;
		systemId = audMixerDevice::COMMAND_PCMSOURCE;
	}
	audPcmSourceTypeId TypeId;
};

} // namespace rage

#endif // AUD_MIXERDEVICE_H
