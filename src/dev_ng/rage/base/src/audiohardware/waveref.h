// 
// audiohardware/waveref.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_WAVEREF_H
#define AUD_WAVEREF_H

#include "audiodata/container.h"
#include "waveslot.h"
#include "driverdefs.h"

namespace rage
{
	// -2dB
	enum { kDefaultPeakLevel_u16 = 52056U };

struct audWaveFormat
{
	enum audStreamFormat
	{
		// All platforms
		kPcm16bitLittleEndian = 0,
		kPcm16bitBigEndian,
		kPcm32FLittleEndian,
		kPcm32FBigEndian,
		kAdpcm,
		// Xenon only
		kXMA2,
		kXWMA,
		// PS3/PC
		kMP3,	
		// PC only
		kOgg,
		kAAC,
		kWMA,
		// Orbis only
		kAtrac9,

		// to permit range checking:
		kNumFormats
	};

	u32 LengthSamples;
	s32 LoopPointSamples;
	u16 SampleRate;
	s16 Headroom;

	// XMA2
	u16 LoopBegin;
	u16 LoopEnd;
	u16 PlayEnd;
	u8 PlayBegin;

	u8 Format;

	u16 FirstPeakSample;
	u16 padding;
};

#define AUD_IS_VALID_WAVEFORMAT_SIZE(size) (size == sizeof(audWaveFormat) || size == sizeof(audWaveFormat)-sizeof(u32))
#define AUD_READ_FIRST_PEAK_SAMPLE(waveFormat, waveFormatSize) (waveFormatSize==sizeof(audWaveFormat) ? waveFormat->FirstPeakSample : kDefaultPeakLevel_u16 )

struct audLoadBlockMetadata
{
	u32 StartOffsetPackets_Broken;
	u32 NumPackets;
	u32 SamplesToSkip;
	u32 NumSamples;
#if __PS3 || RSG_AUDIO_X64
	u32 NumberOfFrames;
	u32 NumberOfBytes;
#endif // __PS3
};

struct audStreamFormatChannelHeader
{
	u32 NameHash;
	u32 LengthSamples;
	s16 Headroom;
	u16 SampleRate;
	u8 Format;
	// padding
	u8 reserved0;
	u16 LoopBegin;	
};


struct audStreamFormatHeader
{
	u32 NumLoadBlocks;
	u32 StreamingBlockBytes;
	u32 NumChannels;
};

#if RSG_DURANGO || RSG_PC
#pragma warning(push)
#pragma warning(disable : 4200)
#endif

struct audStreamFormat
{
	audStreamFormatHeader header;
	audStreamFormatChannelHeader Channels[];
};

#if RSG_DURANGO
#pragma warning(pop)
#endif

struct audSeekTableEntry
{
	u32 StartSample;
};

struct audWaveMarker
{
	u32 categoryNameHash;
	union
	{
		u32 nameHash;
		f32 value;
	};
	u32 positionSamples;
	u32 data;
};

template<typename _T> class audWaveMetadataList
{
public:

	audWaveMetadataList() { m_Count = 0; m_Elements = NULL; }

	void Init(const void *data, const u32 dataSizeBytes) 
	{ 
		Assert(dataSizeBytes%sizeof(_T) == 0);
		m_Elements = (const _T*)data; 
		m_Count = dataSizeBytes / sizeof(_T); 
	}

	const audWaveMarker &operator[](const u32 index) { FastAssert(index < GetCount()); return m_Elements[index]; }

	u32 GetCount() const { return m_Elements ? m_Count : 0; }
private:
	u32 m_Count;
	const _T *m_Elements;
};

class audWaveMarkerList : public audWaveMetadataList<audWaveMarker>
{

};

class audWaveSlot;

// PURPOSE
//	Provides an interface to the wave slot/wave container system
class audWaveRef
{
public:
	audWaveRef() : m_WaveSlotIndex(-1),m_WaveObjectId(-1)
	{

	}

	~audWaveRef() { Shutdown(); }
	
	void Shutdown()
	{
		if(m_WaveSlotIndex != -1)
		{
			audWaveSlot::RemoveSlotReference(m_WaveSlotIndex);
			m_WaveSlotIndex = -1;
		}
		m_WaveObjectId = -1;
	}

	// PURPOSE
	//	Copy constructor - adds an extra reference to the wave slot
	audWaveRef& operator=(const audWaveRef& m)
	{
		if(!Verifyf(!HasValidSlot(), "Assigning to an audWaveRef with a valid slot"))
		{
			audWaveSlot::RemoveSlotReference(m_WaveSlotIndex);
		}

		m_WaveObjectId = m.m_WaveObjectId;
		m_WaveSlotIndex = m.m_WaveSlotIndex;
		if(HasValidSlot())
		{
			audWaveSlot::AddSlotReference(m_WaveSlotIndex);
		}
		return *this;
	}

	bool Init(const u32 waveRef);
	bool Init(const s32 waveSlotId, const u32 waveNameHash);
	bool Init(audWaveSlot *slot, const u32 waveNameHash);

	const void *FindChunk(const u32 chunkNameHash, u32 &chunkSizeBytes) const;
	const audWaveFormat *FindFormat(u32 &formatChunkSize) const;
	const audWaveFormat *FindFormat() const
	{
		u32 size;
		return FindFormat(size);
	}

	audWaveSlot *GetSlot() const;

#if !__SPU
	
	u32 GetWaveNameHash() const;
	const void *FindSeek(u32 &lengthBytes) const;
	const void *FindWaveData(u32 &lengthBytes) const;
	

	void FindMarkerData(audWaveMarkerList &markers) const;

#endif // !__SPU

	bool HasValidSlot() const 
	{ 
#if !__SPU
		Assert(m_WaveSlotIndex == -1 || audWaveSlot::GetWaveSlotFromIndex(m_WaveSlotIndex)->GetReferenceCount()>0); 
#endif
		return m_WaveSlotIndex != -1; 
	}

	bool HasValidWave() const { return HasValidSlot() && m_WaveObjectId != -1; }

	s32 GetSlotId() const { return m_WaveSlotIndex; }
	adatObjectId GetObjectId() const { return static_cast<adatObjectId>(m_WaveObjectId); }
	
	u32 GetAsU32() const { CompileTimeAssert(sizeof(audWaveRef) == sizeof(u32)); return *((u32*)&m_WaveSlotIndex); }
private:

	s16 m_WaveSlotIndex;
	s16 m_WaveObjectId;
};


}//namespace rage
#endif // AUD_WAVEREF_H
