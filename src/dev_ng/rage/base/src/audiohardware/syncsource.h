//
// audiohardware/syncsource.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_SYNCSOURCE_H
#define AUD_SYNCSOURCE_H

#include "atl/array.h"
#include "atl/bitset.h"
#include "lffreelist.h"

namespace rage
{
	// Mixer level sync mechanism
	//
	//	SignallingState:
	//		RefCount
	//		Condition
	//
	//	Signal:
	//		Go?
	//		SubframeOffset
	//
	//	Signaller:
	//		Evaluates condition
	//			If true decrement RefCount
	//			If RefCount == 0 then fire signal
	//		Also decrement/fire if finished
	//
	//	Signalee:
	//		Wait on signal
	//		On Go apply SubframeOffset as predelay

	class audMixerSyncSignal
	{
	public:
		
		audMixerSyncSignal()
			: m_SubframeOffset(0)
			, m_Go(0)
			, m_Canceled(0)
		{

		}

		void Reset()
		{
			m_SubframeOffset = 0;
			m_Go = 0;
			m_Canceled = 0;
		}

		void Trigger(const u32 subframeOffset)
		{
			Assign(m_SubframeOffset, subframeOffset);
			m_Go = 1;
		}

		void Cancel()
		{
			m_Canceled = 1;
		}

		bool HasTriggered() const { return (m_Go != 0); }
		bool WasCanceled() const { return (m_Canceled != 0); }
		u32 GetSubframeOffset() const { return m_SubframeOffset; }

	private:
		u16 m_SubframeOffset;		
		u8 m_Go;
		u8 m_Canceled;
	};

	class audMixerSyncManager
	{
	public:
		enum {InvalidId = 0xffff};
		void Init();

		// PURPOSE
		//	Allocates a sync source
		//	RETURNS
		//	Sync source Id
		u32 Allocate()
		{
			u32 ret = m_FreeList.Allocate();
			if(ret >= InvalidId)
			{
				ret = InvalidId;
			}
			else
			{
				// Owner ref count should already be set to 1
				TrapNE(GetOwnerRefCount(ret),1U);				
			}
			return ret;
		}

		u32 Release(const u32 sourceId)
		{
			if(sourceId != InvalidId)
			{
				Assert(GetOwnerRefCount(sourceId) > 0);
				return sysInterlockedDecrement_NoWrapping(&m_OwnerReferenceCounts[sourceId]);
			}
			return 0;
		}

		u32 AddOwnerRef(const u32 sourceId)
		{
			if(sourceId != InvalidId)
			{
				Assert(GetOwnerRefCount(sourceId) < 1<<10); // Arbitrary large number, really just to catch (unsigned)-1
				return sysInterlockedIncrement_NoWrapping(&m_OwnerReferenceCounts[sourceId]);
			}
			return 0;
		}

		u32 GetOwnerRefCount(const u32 syncSourceId)
		{
#if __SPU
			const s32 tag = 5;
			return sysDmaGetUInt16((uint64_t)&m_OwnerReferenceCounts[syncSourceId], tag);
#else
			return m_OwnerReferenceCounts[syncSourceId];
#endif
		}

		// PURPOSE
		//	Called by all sync sources pointing at this sync Id
		u32 AddTriggerRef(const u32 syncSourceId)
		{
			Assert(syncSourceId != InvalidId);
			return sysInterlockedIncrement_NoWrapping(&m_TriggerReferenceCounts[syncSourceId]);
		}

		u32 GetTriggerRefCount(const u32 syncSourceId)
		{
#if __SPU
			const s32 tag = 5;
			return sysDmaGetUInt16((uint64_t)&m_TriggerReferenceCounts[syncSourceId], tag);
#else
			return m_TriggerReferenceCounts[syncSourceId];
#endif
		}

		// PURPOSE
		//	Called by signaler; will fire the signal if this is the last of the registered sync sources
		void Trigger(const u32 syncSourceId, const u32 subframeOffset)
		{
			Assert(GetTriggerRefCount(syncSourceId) > 0);		
			const u32 newRefCount = sysInterlockedDecrement_NoWrapping(&m_TriggerReferenceCounts[syncSourceId]);
			if(newRefCount == 0)
			{
#if __SPU
				audMixerSyncSignal signal;
				signal.Trigger(subframeOffset);
				WriteSignal(signal, syncSourceId);
#else
				m_Signals[syncSourceId].Trigger(subframeOffset);
#endif
			}
		}

		// PURPOSE
		//	Called by signaler; will fire the signal if this is the last of the registered sync sources with a 'canceled' state
		void Cancel(const u32 syncSourceId)
		{
			Assert(GetTriggerRefCount(syncSourceId) > 0);

			const u32 newRefCount = sysInterlockedDecrement_NoWrapping(&m_TriggerReferenceCounts[syncSourceId]);
			if(newRefCount == 0)
			{
#if __SPU
				audMixerSyncSignal signal;
				signal.Cancel();
				WriteSignal(signal, syncSourceId);
#else
				m_Signals[syncSourceId].Cancel();
#endif
			}
		}

		// PURPOSE
		//	Returns true if this signal has fired
		bool HasTriggered(const u32 syncSourceId, u32 &subframeOffset) const
		{
#if __SPU
			audMixerSyncSignal signal;
			ReadSignal(signal, syncSourceId);
#else
			const audMixerSyncSignal &signal = m_Signals[syncSourceId];
#endif

			if(signal.HasTriggered())
			{
				subframeOffset = signal.GetSubframeOffset();
				return true;
			}
			return false;
		}

		// PURPOSE
		//	Returns true if this signal is in a canceled state
		bool WasCanceled(const u32 syncSourceId) const
		{
#if __SPU
			audMixerSyncSignal signal;
			ReadSignal(signal, syncSourceId);
#else
			const audMixerSyncSignal &signal = m_Signals[syncSourceId];
#endif
			return signal.WasCanceled();
		}

		// PURPOSE
		//	Runs at the end of the frame to clean out all fired triggers
		void Update();

		enum { kMaxSyncSources = 256 };

		u32 GetNumAllocated() { return m_FreeList.GetNumAllocated(); }

		static audMixerSyncManager *Get();
		SPU_ONLY(static audMixerSyncManager *sm_InstanceEA);

#if __SPU
		void WriteSignal(const audMixerSyncSignal &signalState, const u32 signalId)
		{
			CompileTimeAssert(sizeof(audMixerSyncSignal) == 4);
			const s32 tag = 5;
			const u32 val = *((u32*)&signalState);
			sysDmaPutUInt32(val, (uint64_t)&m_Signals[signalId], tag);
			sysDmaWait(1<<tag);			
		}

		void ReadSignal(audMixerSyncSignal &signalState, const u32 signalId) const
		{
			CompileTimeAssert(sizeof(audMixerSyncSignal) == 4);
			const s32 tag = 5;
			const uint64_t ea = (uint64_t)(&m_Signals[signalId]);
			const u32 val = sysDmaGetUInt32(ea, tag);	
			*((u32*)&signalState) = val;
		}
#else
		void ReadSignal(audMixerSyncSignal &signalState, const u32 signalId) const
		{
			signalState = m_Signals[signalId];
		}
#endif

	private:

#if !__SPU
		void Free(const u32 sourceId)
		{			
			Assert(m_TriggerReferenceCounts[sourceId] == 0);
			Assert(m_OwnerReferenceCounts[sourceId] == 0);
			m_Signals[sourceId].Reset();
			// Leave owner ref count at 1, to prevent double free
			sysInterlockedIncrement_NoWrapping(&m_OwnerReferenceCounts[sourceId]);
			m_FreeList.Free(sourceId);
		}
#endif

		audLFFreeList<kMaxSyncSources> m_FreeList;
		atRangeArray<audMixerSyncSignal, kMaxSyncSources> m_Signals;
		atRangeArray<u16, kMaxSyncSources> m_TriggerReferenceCounts;
		atRangeArray<u16, kMaxSyncSources> m_OwnerReferenceCounts;
};

} // namespace rage

#endif // AUD_SYNCSOURCE_H
