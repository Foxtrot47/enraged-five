// 
// audiohardware/virtualisationjob.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUD_VIRTUALISATIONJOB_H
#define AUD_VIRTUALISATIONJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(VirtualisationJob);

#endif
