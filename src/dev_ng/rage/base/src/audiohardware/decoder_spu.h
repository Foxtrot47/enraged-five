// 
// audiohardware/decoder_spu.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DECODER_SPU_H
#define AUD_DECODER_SPU_H

#if __PS3

#if __PPU
#include <cell/msmp3dec_ppu.h>
#elif __SPU
#include <cell/msmp3dec_spu.h>
#endif

#include "decoder.h"
#include "mp3util.h"
#include "vramhelper.h"

namespace rage
{
BEGIN_ALIGNED(128) class audDecoderSpu 
#if !__SPU
		: public audDecoder
#endif
{
public:
	struct audDecoderSpuPacket
	{
		u8 *InputBuffer;
		u32 InputBufferSize;
		u32 InputBufferBytesRead;
		u32 SamplesDecoded;

		u32 LoopStartOffsetBytes;
		u32 PlayBegin;
		u32 PlayEnd;
		u32 LoopBegin;
		u32 LoopEnd;
		s32 WaveSlotID;
		bool IsDataInVram;
	};


#if __PPU
	static bool InitClass();
	static void ShutdownClass();
	static void *GetDecoderMem() { return sm_DecoderMemory; }
#endif

	audDecoderSpu();
	virtual ~audDecoderSpu(){}

#if __SPU
	void Update();
#endif

	bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 sampleRate);
	void Shutdown();

	bool QueryReadyForMoreData() const { return m_NumPacketsQueued < static_cast<u32>(m_Packets.size()); }

	u32 QueryNumAvailableSamples() const;
	audDecoder::audDecoderState GetState() const {return m_State; }	u32 ReadData(s16 *buf, const u32 numSamples) const;
	bool CanDecodeMoreData() const;

	void AdvanceReadPtr(const u32 numSamples);
	void SubmitPacket(audPacket &packet);
	void SubmitPacket(audDecoderSpuPacket &packet);
	static bool GenerateSpuPacket(audPacket &packet, audDecoderSpu::audDecoderSpuPacket &outputPacket, bool isDataMP3);

	void PreUpdate();
	void ClearBuffer();

	void SetWaveIdentifier(const u32 waveIdentifier) { m_WaveIdentifier = waveIdentifier; }

	void SkipSamplesInCurrentPacket(const u32 numSamples)
	{
		m_SkipSamples += numSamples;
	}

	inline u32 GetFramesDecoded() const { return m_FramesDecoded; }

private:

	// Two frames of decoder output
	atRangeArray<s16, audMp3Util::kFrameSizeSamples*2> m_DecodeBuffer;
	
	// Sony persistent decoder state
	CellMSMP3Context* m_Context1;
	CellMSMP3Context* m_Context2;
	s32 m_InternalCount;
	
	u32 m_ReadFrameIndex;
	atRangeArray<u32, 2> m_DecodedSamplesAvailable;
	atRangeArray<u32, 2> m_DecodeBufferSize;

	u32 m_SamplesConsumed;
	u32 m_FramesDecoded;

	atRangeArray<audDecoderSpuPacket, 2> m_Packets;
	u32 m_SubmitPacketIndex;
	u32 m_DecodePacketIndex;
	u32 m_NumPacketsQueued;
	u32 m_NumChannels;

	u32 m_SkipSamples;

	u32 m_WaveIdentifier;

	audDecoder::audDecoderState m_State;

	audDecoderVramHelper m_VramHelper;

	bool m_IsDataInVRAM;
	bool m_IsDataMP3;
	
#if __PPU 
	static u8 *sm_FixedHeap;
	static sysMemFixedAllocator *sm_ContextMemoryAllocator;
	static u8 *sm_DecoderMemory;
#endif
}ALIGNED(128);

} // namespace rage

#endif // __PS3

#endif // AUD_DECODER_SPU_H
