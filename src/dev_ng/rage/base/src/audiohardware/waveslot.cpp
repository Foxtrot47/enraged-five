//
// audiohardware/waveslot.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "channel.h"
#include "config.h"
#include "driver.h"
#include "driverutil.h"
#include "fwsys/timer.h"
#include "streamingwaveslot.h"
#include "waveslot.h"
#include "audiodata/container.h"
#include "audiodata/simpletypes.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/soundfactory.h"
#include "audiohardware/debug.h"
#include "audiosoundtypes/environmentsound.h"
#include "audiohardware/decoder_xbonexma.h"

#include "atl/array.h"
#include "diag/output.h"
#include "grcore/debugdraw.h"

#include "fwsys/config.h"

#if !__SPU
#include "bank/text.h"
#include "file/asset.h"
#include "string/stringhash.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/param.h"
#include "profile/profiler.h"

#else

#include "system/dma.h"

#endif // __SPU

#define		FAST_WAVESLOT_PREFETCH		0

#if FAST_WAVESLOT_PREFETCH
#include "system/cache.h"
#endif

#include "diag/channel.h"

#if !__SPU
RAGE_DEFINE_CHANNEL(Audio)
#endif

namespace rage {
#if !__SPU
PARAM(suppressWaveSlotSizeErrors,"[RAGE Audio] Suppress on-screen waveslot size errors");
PARAM(audiotestslot,"[RAGE Audio] Allocates the wave slot called TEST");
PARAM(nowaveslots,"[RAGE Audio] Don't allocate any wave slots");
PARAM(computeStaticBankInfo,"[RAGE Audio] Calls ComputeContiguousSampleDataChunk and outputs results to specified file");
PARAM(ignorePrecalculatedBankInfo,"[RAGE Audio] Ignore the precalculated bank info file");
NOSTRIP_PARAM(audiofolder, "Indicate where audio folder is");
#endif

#if __XENON || RSG_DURANGO
static const u32 g_WaveSlotAlignment = 2048;
#else
static const u32 g_WaveSlotAlignment = 16;
#endif

#if __SPU
s32 audWaveSlot::sm_InternalStateCacheSlotId = -1;
s32 audWaveSlot::sm_CachedContainerSlotId = -1;
audWaveSlot::audWaveSlotState audWaveSlot::sm_InternalStateCache;
adatContainerSpu audWaveSlot::sm_CachedContainer;
#endif

atMap<u32, audWaveSlot*> audWaveSlot::sm_WaveSlotTable;
#if !__SPU
atMap<u16, audWaveSlot*> audWaveSlot::sm_LoadedBankTable;
atMap<u32, audWaveSlot*> audWaveSlot::sm_LoadedWaveTable;
atArray<audWaveSlot*> audWaveSlot::sm_WaveLoadingSlots;
#endif

atMap<u32, const char *> audWaveSlot::sm_SearchPaths;

audWaveSlot *audWaveSlot::sm_WaveSlots;
u8 *audWaveSlot::sm_WaveSlotData;
bool audWaveSlot::sm_OwnsWaveSlotData = false;
u32	  audWaveSlot::sm_TotalMainAllocation = 0;
u32	  audWaveSlot::sm_TotalVRAMAllocation = 0;

u32	audWaveSlot::sm_BatchWaveLoadRequestTime = 0;
u32	audWaveSlot::sm_NumWaveLoadRequestsInBatch = 0;
bool audWaveSlot::sm_IsBatchWaveLoadRequested = false;
bool audWaveSlot::sm_HasBatchWaveLoadFailed = false;
atRangeArray<audBatchWaveLoadRequest,g_MaxWaveLoadsPerBatch> audWaveSlot::sm_BatchWaveLoadRequest;
audWaveSlot::QueryEncryptionKeyCallback audWaveSlot::GetEncryptionKeyOverrideCallback;

#if RSG_BANK && !__SPU
bool audWaveSlot::sm_DrawRequestQueue = false;
bool audWaveSlot::sm_DrawWaveSlotSizeErrors = true;
bool audWaveSlot::sm_DelayRequests = false;
u32 audWaveSlot::sm_RequestDelayTime = 1000;
u32 audWaveSlot::sm_StreamingRequestDelayTime = 1000;
u32 audWaveSlot::sm_CurrentlyLoadingSlotIndex = ~0U;
atFixedArray<u8,256> audWaveSlot::sm_RequestQueueToDraw;
sysCriticalSectionToken audWaveSlot::sm_DrawRequestQueueLock;
#endif // RSG_BANK

atRangeArray<u32, audWaveSlot::kNumWaveLoadPrioLevels> audWaveSlot::sm_RequestQueuePrioCounts;
u32 audWaveSlot::sm_RequestQueueCount;

int audWaveSlot::sm_iSchedulerIndex;

#if !__SPU
u32 audWaveSlot::sm_NumWaveSlots = 0;
u32 *audWaveSlot::sm_ReferenceCounts = NULL;
bool g_UseCriticalQueueForAudio = true;
#endif

u32 audWaveSlot::sm_CurrentBatchLoadRequestStage = 0;

#if !__SPU
BANK_ONLY(sysCriticalSectionToken audWaveSlot::sm_WaveSlotNamesCriticalSectionToken);
BANK_ONLY (atArray<bkText *> audWaveSlot::sm_LoadedBankNames);
BANK_ONLY (atArray<bkText *> audWaveSlot::sm_LoadingBankNames);

sysCriticalSectionToken audWaveSlot::sm_BankTableCriticalSectionToken;
sysCriticalSectionToken audWaveSlot::sm_BankLoadCriticalSectionToken;
sysCriticalSectionToken audWaveSlot::sm_BatchWaveLoadCriticalSectionToken;
sysCriticalSectionToken audWaveSlot::sm_UpdateCriticalSection;

atFixedArray<audWaveSlot::audStaticBankInfo, 25> audWaveSlot::sm_StaticBankInfoList;
bool audWaveSlot::sm_UsePreComputedStaticBankInfo = true;
bool audWaveSlot::sm_StaticBankInfoFileExists = false;

PF_PAGE(audWaveSlotPage, "Audio waveslot stats");
PF_GROUP(WaveSlotGroup);
PF_LINK(audWaveSlotPage, WaveSlotGroup);
PF_VALUE_INT(AudioLockingStreamer, WaveSlotGroup);

PARAM(waveSlotsSpew, "Ensable scripted conversation spew");

extern bool g_IsExiting;

#if RSG_BANK 
void audWaveSlot::DebugDrawSlotSizeErrors()
{
	if (sm_DrawWaveSlotSizeErrors)
	{
		f32 currentYOffset = 0.4f;
		f32 currentXOffset = 0.4f;
		char tempString[256];
		bool isFirstError = true;		

		for (u32 i = 0; i < GetNumWaveSlots(); ++i)
		{
			audWaveSlot* pSlot = audWaveSlot::GetWaveSlotFromIndex((s32)i);

			if (pSlot->m_ExpectedSlotSize != 0u && pSlot->m_ExpectedSlotSize < pSlot->GetInternalState()->SlotBytes)
			{
				if (isFirstError)
				{
					Color32 color = ((fwTimer::GetTimeInMilliseconds() / 100) % 2 == 0) ? Color32(255, 0, 0) : Color32(255, 255, 255);
					formatf(tempString, "%s - %u bytes (expected %u)", pSlot->GetSlotName(), pSlot->GetInternalState()->SlotBytes, pSlot->m_ExpectedSlotSize);
					grcDebugDraw::Text(Vector2(currentXOffset, currentYOffset), color, "Audio Wave Slot Size Change(s) Detected - DO NOT SYNC:");
					currentYOffset += 0.02f;
				}

				formatf(tempString, "     %s - %u bytes (expected %u)", pSlot->GetSlotName(), pSlot->GetInternalState()->SlotBytes, pSlot->m_ExpectedSlotSize);
				grcDebugDraw::Text(Vector2(currentXOffset, currentYOffset), Color32(255, 255, 255), tempString);
				currentYOffset += 0.02f;
				isFirstError = false;
			}
		}
	}	
}

void audWaveSlot::DebugDraw(audDebugDrawManager &drawMgr)
{
	if(sm_DrawRequestQueue)
	{
		SYS_CS_SYNC(sm_DrawRequestQueueLock);
		char lineBuf[255];

		if(sm_CurrentlyLoadingSlotIndex != ~0U)
		{
			audWaveSlot *slot = GetWaveSlotFromIndex(sm_CurrentlyLoadingSlotIndex);
			
			formatf(lineBuf, "Currently loading: [%s] prio: %d %s / %u (%.2fs old)",			
				slot->GetSlotName(),
				slot->m_LoadPriority,
				GetBankName(slot->m_LoadedBankId),
				slot->m_LoadedWaveNameHash,
				(audEngineUtil::GetCurrentTimeInMilliseconds() - slot->GetLoadRequestTimeMs()) * 0.001f);
			drawMgr.DrawLine(lineBuf);
		}

		if(sm_IsBatchWaveLoadRequested)
		{
			SYS_CS_SYNC(sm_BatchWaveLoadCriticalSectionToken);			
			formatf(lineBuf, "BatchRequest: %d / %d (%.2fs old)", sm_CurrentBatchLoadRequestStage, sm_NumWaveLoadRequestsInBatch, (audEngineUtil::GetCurrentTimeInMilliseconds() - sm_BatchWaveLoadRequestTime) * 0.001f);
			drawMgr.PushSection(lineBuf);
			for(u32 i = 0; i < sm_NumWaveLoadRequestsInBatch; i++)
			{
				formatf(lineBuf, "%d: [%s] %s / %u", 
						i, 
						GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[i].slotIndex)->GetSlotName(),
						GetBankName(sm_BatchWaveLoadRequest[i].bankId),
						sm_BatchWaveLoadRequest[i].waveNameHash);
				drawMgr.DrawLine(lineBuf);
			}
			drawMgr.PopSection();
		}

		formatf(lineBuf, "Request queue (%d entries)", sm_RequestQueueToDraw.GetCount());
		drawMgr.PushSection(lineBuf);
		for(s32 i = 0; i < sm_RequestQueueToDraw.GetCount(); i++)
		{
			audWaveSlot *slot = audWaveSlot::GetWaveSlotFromIndex(sm_RequestQueueToDraw[i]);
			formatf(lineBuf, "%d: [%s] prio: %d %s / %u (%.2fs old)", 
				i, 
				slot->GetSlotName(),
				slot->GetRequestedLoadPriority(),
				GetBankName(slot->m_BankIdToLoad),
				slot->m_WaveNameHashToLoad,
				(audEngineUtil::GetCurrentTimeInMilliseconds() - slot->GetLoadRequestTimeMs()) * 0.001f);
			drawMgr.DrawLine(lineBuf);
		}
		drawMgr.PopSection();
	}
}

static char szAudBankToLoad[256] = {0};

void audWaveSlot::AddWidgetsStatic(bkBank& bank)
{
	SYS_CS_SYNC(sm_WaveSlotNamesCriticalSectionToken);
	bank.PushGroup("WaveSlots");
	{
		bank.AddToggle("DrawWaveSlotSizeErrors", &sm_DrawWaveSlotSizeErrors);
		bank.AddToggle("DrawRequests", &sm_DrawRequestQueue);
		bank.AddToggle("DelayRequests", &sm_DelayRequests);
		bank.AddSlider("DelayTime", &sm_RequestDelayTime, 0, 1000000,1);
		bank.AddSlider("StreamingDelayTime", &sm_StreamingRequestDelayTime, 0, 1000000,1);
		bank.AddToggle("UseCriticalQueue", &g_UseCriticalQueueForAudio);		
		const u32 numSlots = audWaveSlot::GetNumWaveSlots();
		audWaveSlot::sm_LoadedBankNames.Reset();
		audWaveSlot::sm_LoadingBankNames.Reset();

		for (u32 i = 0; i < numSlots; ++i)
		{
			audWaveSlot* pSlot = audWaveSlot::GetWaveSlotFromIndex((s32)i);
			audAssert(pSlot);
			bank.PushGroup(pSlot->GetSlotName());
			{
				bank.AddText("szTestBankLoad", szAudBankToLoad, sizeof(szAudBankToLoad));
				bank.AddButton("TestLoadBank", datCallback(CFA1(audWaveSlot::TestLoadBank), pSlot));

				audWaveSlot::sm_LoadedBankNames.PushAndGrow(bank.AddText("Loaded Bank",(char*)pSlot->GetCachedLoadedBankName(),256,true));
				audWaveSlot::sm_LoadingBankNames.PushAndGrow(bank.AddText("Loading Bank",(char*)pSlot->GetCachedLoadingBankName(),256,true));				

				bank.AddText("ReferenceCount",			(int*)&audWaveSlot::sm_ReferenceCounts[pSlot->m_SlotIndex], true);
				bank.AddText("SlotBytes",				(int*)&pSlot->GetInternalState()->SlotBytes, true);
				bank.AddText("MaxHeaderSize",			(int*)&pSlot->GetInternalState()->MaxHeaderSize, true);
				bank.AddText("LoadedContainerOffset",	(int*)&pSlot->GetInternalState()->LoadedContainerOffset, true);
				bank.AddText("VRAMLoadOffset",			(int*)&pSlot->GetInternalState()->VRAMLoadOffset, true);
				bank.AddText("VRAMDataSize",			(int*)&pSlot->GetInternalState()->VramDataSize, true);
				bank.AddText("MainDataSize",			(int*)&pSlot->GetInternalState()->MainDataSize, true);
				bank.AddText("LoadedBytes",				(int*)&pSlot->GetInternalState()->LoadedBytes, true);

				if(pSlot->IsStreaming())
				{
					audStreamingWaveSlot *streamSlot = reinterpret_cast<audStreamingWaveSlot*>(pSlot);
					bank.AddText("TimeTakenToFulfilRequest", &streamSlot->GetState()->TimeTakenToFulfilRequest);
				}

				bank.AddText("WaveNameHashToLoad",		(int*)&pSlot->m_WaveNameHashToLoad, true);
				bank.AddSlider("LoadedWaveNameHash",	&pSlot->m_LoadedWaveNameHash, 0, UINT_MAX, 0);
				bank.AddSlider("LoadFailedWaveNameHash",&pSlot->m_LoadFailedWaveNameHash, 0, UINT_MAX, 0);
				bank.AddText("LoadRequestTimeMs",		(int*)&pSlot->m_LoadRequestTimeMs, true);
				bank.AddSlider("BankIdToLoad",			&pSlot->m_BankIdToLoad, 0, 65535, 0);
				bank.AddSlider("LoadedBankId",			&pSlot->m_LoadedBankId, 0, 65535, 0);
				bank.AddSlider("LoadFailedBankId",		&pSlot->m_LoadFailedBankId, 0, 65535, 0);
				//bank.AddText("IsLoadRequestQueued",		&pSlot->m_IsLoadRequestQueued, true);
				//bank.AddText("IsLoading",				&pSlot->m_IsLoading, true);
				//bank.AddText("IsStreaming",				&pSlot->m_IsStreaming, true);
				//bank.AddText("HasChanged",				&pSlot->m_HasChanged, true);
				//bank.AddText("BatchMode",				&pSlot->m_BatchMode, true);

			}bank.PopGroup();
		}
	}bank.PopGroup();
}
void audWaveSlot::TestLoadBank(audWaveSlot* pWaveSlot)
{
	const u32 bankId = SOUNDFACTORY.GetBankIndexFromName(szAudBankToLoad);
	if (pWaveSlot && bankId!=0xffffffff)
	{
		pWaveSlot->LoadBank(bankId);
	}
	else
	{
		audErrorf("[audWaveSlot::TestLoadBankOnWaveSlot] Failed to find waveslot or invalid bank %s", szAudBankToLoad);
	}
}
#endif

u32 audWaveSlot::GetSizeOfBank(const char *bankName)
{
	char bankFilePath[RAGE_MAX_PATH];
	audWaveSlot::ComputeBankFilePath(bankFilePath, bankName);
	fiStream *stream = ASSET.Open(bankFilePath,"");
	if(stream)
	{
		u32 ret = stream->Size();
		stream->Close();
		return ret;
	}
	return 0;
}

bool audWaveSlot::ComputeContiguousSampleDataChunk(fiStream* stream, const u32 maxHeaderSize, u32 &sampleDataOffset, u32 &sampleDataSize, u32 &numObjects) {

	u8 *headerBuffer = rage_new u8[maxHeaderSize];
	if(stream == NULL)
	{
		return false;
	}

	stream->Read(headerBuffer, maxHeaderSize);

	adatContainer container;
	container.SetHeader((const adatContainerHeader*)headerBuffer);
	audAssert(container.IsValid());
	audAssert(container.IsNativeEndian());
	audAssert(container.GetHeaderSize() <= maxHeaderSize);
	
	u32 minOffset = ~0U;
	u32 maxOffset = 0;
	u32 sizeAtMaxOffset = 0;

	static const u32 dataChunkHash = (ATSTRINGHASH("DATA", 0x5EB5E655)&adatContainerObjectDataTableEntry::kDataTypeHashMask);

	numObjects = container.GetNumObjects();
	for(adatObjectId objectId = 0; objectId < (adatObjectId)numObjects; objectId ++)
	{
		for(adatObjectDataChunkId chunkId = 0; chunkId < (adatObjectDataChunkId)container.GetNumDataChunksForObject(objectId); chunkId++)
		{
			u32 dataTypeHash;
			u32 offsetBytes;
			u32 sizeBytes;
			container.GetObjectDataChunk(objectId, chunkId, dataTypeHash, offsetBytes, sizeBytes);

			if(dataTypeHash == dataChunkHash)
			{
				minOffset = Min(minOffset, offsetBytes);
				if(offsetBytes > maxOffset)
				{
					maxOffset = offsetBytes;
					sizeAtMaxOffset = sizeBytes;
				}
			}
		}
	}

	sampleDataOffset = minOffset;
	sampleDataSize = (maxOffset+sizeAtMaxOffset) - minOffset;

	delete[] headerBuffer;
	return true;
}

bool audWaveSlot::ComputeStaticSize(const char *bankName, u32 &headerSize, u32 &dataSize)
{
	char bankFilePath[RAGE_MAX_PATH];
	audWaveSlot::ComputeBankFilePath(bankFilePath, bankName);

	u8 headerBuffer[32];
	fiStream *stream = ASSET.Open(bankFilePath, NULL);

	if(stream == NULL)
	{
		audErrorf("Failed to open bank: %s", bankName);
		return false;
	}

	u32 fileSize = stream->Size();
	u32 bytesToRead = Min(fileSize, (u32)sizeof(headerBuffer));
	stream->Read(headerBuffer, bytesToRead);
	stream->Close();

	//if(!adatContainer::IsValidHeader(headerBuffer))
	//{
	//	// assume its encrypted
	//	btea((u32*)headerBuffer, -int(bytesToRead>>2), GetKey());
	//}

	adatContainer container;
	container.SetHeader((const adatContainerHeader*)headerBuffer);
	audAssert(container.IsValid());
	audAssert(container.IsNativeEndian());
	audAssert(fileSize >= container.GetHeaderSize());
	// We only need the first few bytes to grab the header size.
	headerSize = container.GetHeaderSize();
	dataSize = fileSize - headerSize;

	return true;
}

u32 audWaveSlot::ComputeContiguousMetadataSize() const
{
	audAssert(GetInternalState()->Container.IsValid());
	audAssert(GetInternalState()->Container.IsNativeEndian());
	audAssert(GetInternalState()->Container.GetHeaderSize() <= GetInternalState()->MaxHeaderSize);

	u32 minOffset = ~0U;

	static const u32 dataChunkHash = (ATSTRINGHASH("DATA", 0x5EB5E655)&adatContainerObjectDataTableEntry::kDataTypeHashMask);

	u32 numObjects = GetInternalState()->Container.GetNumObjects();
	for(adatObjectId objectId = 0; objectId < (adatObjectId)numObjects; objectId ++)
	{
		for(adatObjectDataChunkId chunkId = 0; chunkId < (adatObjectDataChunkId)GetInternalState()->Container.GetNumDataChunksForObject(objectId); chunkId++)
		{
			u32 dataTypeHash;
			u32 offsetBytes;
			u32 sizeBytes;
			GetInternalState()->Container.GetObjectDataChunk(objectId, chunkId, dataTypeHash, offsetBytes, sizeBytes);

			if(dataTypeHash == dataChunkHash)
			{
				minOffset = Min(minOffset, offsetBytes);
			}
		}
	}

	return minOffset;
}

bool audWaveSlot::IsVramSlot(const char *slotName)
{	
	if(strstr(slotName, "WEAPON") != NULL)
	{
		return false;
	}
	if(strstr(slotName, "LOW_LATENCY") != NULL)
	{
		return false;
	}
	/*if(strcmp(slotName, "TEMP_SIZING_BANK") == 0)
	{		
		return false;
	}*/
	return true;
}

void audWaveSlot::LoadStaticBankInfo()
{
#if !__FINAL
	if(PARAM_ignorePrecalculatedBankInfo.Get())
	{
		sm_UsePreComputedStaticBankInfo = false;
	}
#endif

#if 0
	if(sm_UsePreComputedStaticBankInfo)
	{
		const char* staticBankInfoFile = "platform:/audio/staticbankinfo.dat";
		fiStream* fileStream = ASSET.Open(staticBankInfoFile, "");

		if(fileStream)
		{
			sm_StaticBankInfoFileExists = true;
			u32 numStaticBanks = 0;
			fileStream->Read(&numStaticBanks, sizeof(u32));
			audAssert(numStaticBanks <= (u32)sm_StaticBankInfoList.GetMaxCount());

			for(u32 loop = 0; loop < numStaticBanks; loop++)
			{
				audStaticBankInfo staticBankInfo;
				fileStream->Read(&staticBankInfo, sizeof(audStaticBankInfo));
				sm_StaticBankInfoList.Push(staticBankInfo);
			}

			fileStream->Close();
		}
		else
		{
			audErrorf("Could not open %s - audWaveSlot::InitClass performance will not be optimal! Please run with -computeStaticBankInfo to regenerate file", staticBankInfoFile);
		}
	}
#endif
}

bool audWaveSlot::AllowCompressedAssetLoad() const
{
	return AllowCompressedAssetLoad((LoadType)m_LoadType, GetSlotName());
}

bool audWaveSlot::AllowCompressedAssetLoad(LoadType loadType, const char* slotName)
{
	return loadType == kLoadTypeStream || loadType == kLoadTypeWave || strstr(slotName, "AMBIENT_SCRIPT_SLOT") || (strstr(slotName, "STREAM_ENGINE") && !strstr(slotName, "LOW_LATENCY")) || strstr(slotName, "AMB_STREAM") || strstr(slotName, "STATIC_EXPLOSIONS");
}

void audWaveSlot::InitClass(void *externallyAllocatedBankHeap, u32 externallyAllocatedBankHeapSize)
{
	u32 numBytes, curOffset = 0, maxHeaderSizeBytes;
	u32 totalBankMem = 0;
	bool throwPrecalculatedDataError = false;

	const bool bankDataInVram = AUD_BANK_DATA_IN_VRAM;
	const bool streamDataInVram = AUD_STREAMINGBUFFERS_IN_VRAM;
	const bool waveDataInVram = AUD_WAVE_DATA_IN_VRAM;

#if !__FINAL
	bool allocateTestSlot = PARAM_audiotestslot.Get();
#else
	bool allocateTestSlot = false;
#endif

#if __BANK
	sm_DrawWaveSlotSizeErrors = !PARAM_suppressWaveSlotSizeErrors.Get();
#endif

	sm_NumWaveSlots = 0;
	sm_WaveSlots = NULL;
	sm_WaveSlotData = NULL;
	
	sm_TotalMainAllocation = sm_TotalVRAMAllocation = 0;
	sm_BatchWaveLoadRequestTime = 0;
	sm_NumWaveLoadRequestsInBatch = 0;
	sm_IsBatchWaveLoadRequested = false;
	sm_HasBatchWaveLoadFailed = false;
	sm_CurrentBatchLoadRequestStage = 0;

#if !__FINAL
	if(!PARAM_nowaveslots.Get())
#endif
	{
		const ravesimpletypes::WaveSlots *waveSlotList = audConfig::GetMetadataManager().GetObject<ravesimpletypes::WaveSlots>("WaveSlotsList");
		
		if(!audVerifyf(waveSlotList, "Failed to find wave slot list in metadata"))
		{
			return;
		}
	
		u32 totalBankVRAM = 0;
		u32 totalApuRam = 0;

		const u32 zeroHash = ATSTRINGHASH("0", 0x6E3C5C6B);

		u32 numWaveLoadingSlots = 0;		

		// firstly compute the overall memory requirements
		for(u32 i = 0; i < waveSlotList->numSlotRefs; i++)
		{
			ravesimpletypes::Slot *slotData = audConfig::GetMetadataManager().GetObject<ravesimpletypes::Slot>(waveSlotList->SlotRef[i].SlotNameHash);
			if(!audVerifyf(slotData, "Failed to find slot data, index %u", i))
			{
				return;
			}

			const char *name = audConfig::GetMetadataManager().GetNameFromNTO_Always(slotData->NameTableOffset);
			const char *bankName = NULL;
			
			
			if(slotData->StaticBank && slotData->StaticBank != zeroHash)
			{
				const u32 bankId = g_AudioEngine.GetSoundManager().GetFactory().GetBankNameIndexFromHash(slotData->StaticBank);
				if(audVerifyf(bankId < AUD_INVALID_BANK_ID, "Static bank reference not in bank name list, slot %s", name))
				{
					bankName = GetBankName(bankId);
					
#if RSG_BANK && RSG_PS3
					u32 bankHeaderSize = 0;
					u32 bankDataSize = 0;
					
					ComputeStaticSize(bankName, bankHeaderSize, bankDataSize);

					if(bankHeaderSize > slotData->MaxHeaderSize)
					{
						audErrorf("AudioConfig waveslot %s header size %u is incorrect for %s.  Static bank header size is %u", name, slotData->MaxHeaderSize, bankName, bankHeaderSize);
						slotData->MaxHeaderSize = bankHeaderSize;
					}
					if(bankDataSize > slotData->Size)
					{
						audErrorf("AudioConfig waveslot %s data size %u is incorrect for %s.  Static bank data size is %u", name, slotData->Size, bankName, bankDataSize);
						slotData->Size = bankDataSize;
					}
#endif
				}
			}

#if __BANK
			if (slotData->Size == 0)
			{
				audWarningf("Waveslot with size 0 : %s", name);
				audWarningf("This is OK for AMBIENT_SCRIPT_SLOT 11 to 16");
			}
#endif		
			audAssert(name);

			LoadType loadType;
			if(slotData->LoadType == ravesimpletypes::SLOT_LOAD_TYPE_WAVE)
			{
				loadType = kLoadTypeWave;
				numWaveLoadingSlots++;
			}
			else if(slotData->LoadType == ravesimpletypes::SLOT_LOAD_TYPE_STREAM)
			{
				loadType = kLoadTypeStream;
			}
			else
			{
				audAssert(slotData->LoadType == ravesimpletypes::SLOT_LOAD_TYPE_BANK);
				loadType = kLoadTypeBank;
			}

#if RSG_DURANGO
			bool allowCompressedAssetLoad = AllowCompressedAssetLoad(loadType, name);
#endif

			if((allocateTestSlot || strcmp(name, "TEST")) && slotData->Size != 0)
			{
				numBytes = slotData->Size;

				u32 numVRAMBytes = 0;
				u32 numApuBytes = 0;

				if (loadType == kLoadTypeBank && bankDataInVram && IsVramSlot(name))
				{
					if(bankName)
					{
						u32 bankNameHash = atStringHash(bankName);
						u32 sampleDataOffset = 0;
						u32 sampleDataSize = 0;
						u32 numStaticWaves = 0;
						u32 fileSize = 0;

						char bankFilePath[RAGE_MAX_PATH];
						audWaveSlot::ComputeBankFilePath(bankFilePath, bankName);
						fiStream *stream = ASSET.Open(bankFilePath, NULL);

						if(stream)
						{
							fileSize = stream->Size();
						}
						else
						{
							audErrorf("Failed to open bank: %s", bankName);
						}

						bool precalculatedDataFound = false;

#if RSG_PS3
						// First check if we've got some precalculated data for this bank. If so, we can avoid calling the expensive 
						// ComputeContiguousSampleDataChunk function
						for(s32 loop = 0; loop < sm_StaticBankInfoList.GetCount(); loop++)
						{
							if(sm_StaticBankInfoList[loop].bankNameHash == bankNameHash)
							{
								// Check that the file size hasn't changed - if so, our precalculated values are out of date
								if(sm_StaticBankInfoList[loop].fileSize == fileSize)
								{
#if __DEV
									// On dev builds, just call ComputeContiguousSampleDataChunk to sanity check the file contents
									ComputeContiguousSampleDataChunk(stream, slotData->MaxHeaderSize, sampleDataOffset, sampleDataSize, numStaticWaves);
									audAssert(sampleDataSize == sm_StaticBankInfoList[loop].sampleDataSize);
									audAssert(sampleDataOffset == sm_StaticBankInfoList[loop].sampleDataOffset);
#else
									sampleDataSize = sm_StaticBankInfoList[loop].sampleDataSize;
									sampleDataOffset = sm_StaticBankInfoList[loop].sampleDataOffset;
#endif
									precalculatedDataFound = true;
								}
								else
								{
									// Delete this data from the list - allows us to make some assumption about validity when we access this data
									// again later in the function
									sm_StaticBankInfoList.Delete(loop);
									audErrorf("Precalculated bank data for bank %s is out of date (file size differs) - audWaveSlot::InitClass performance will not be optimal! Please run with -computeStaticBankInfo to regenerate file", bankName);
								}

								break;
							}
						}
#endif

						// Didn't manage to find any precalculated data, fall back to the slow version
						if(!precalculatedDataFound)
						{
							ComputeContiguousSampleDataChunk(stream, slotData->MaxHeaderSize, sampleDataOffset, sampleDataSize, numStaticWaves);

							if(sm_StaticBankInfoFileExists)
							{
								throwPrecalculatedDataError = true;
							}
						}
						
						if(stream)
						{
							stream->Close();
						}

						audDisplayf("Bank:%s Sample data offset: %u Sample data size:%u", bankName, sampleDataOffset, sampleDataSize);
						audDisplayf("Static bank '%s' has sample data offset %u, %u bytes total (MaxHeaderSize: %u MaxMetadataSize: %u)", bankName, sampleDataOffset, sampleDataSize, slotData->MaxHeaderSize, slotData->MaxMetadataSize);

						audAssert(sampleDataOffset);

						numVRAMBytes = sampleDataSize;
						numBytes = sampleDataOffset;						

						// If the precalculated data wasn't valid, add the correct data to the list now.
						if(!precalculatedDataFound)
						{
							audStaticBankInfo staticBankInfo;
							staticBankInfo.bankNameHash = bankNameHash;
							staticBankInfo.fileSize = fileSize;
							staticBankInfo.sampleDataOffset = sampleDataOffset;
							staticBankInfo.sampleDataSize = sampleDataSize;
							sm_StaticBankInfoList.Push(staticBankInfo);
						}
					}
					else
					{
						// full bank into VRAM
						numVRAMBytes = numBytes;
						// full header goes into main ram
						numBytes = slotData->MaxHeaderSize + slotData->MaxMetadataSize;
					}
				}
				else if((loadType == kLoadTypeStream && streamDataInVram) ||
							(loadType == kLoadTypeWave && waveDataInVram))
				{
					// Header size is computed to include all stream metadata
					u32 headerSize = slotData->MaxHeaderSize;

					if(loadType == kLoadTypeWave)
					{
						numVRAMBytes = slotData->MaxDataSize;
						headerSize += slotData->MaxMetadataSize;						
					}
					else
					{
						numVRAMBytes = numBytes - headerSize;
					}
					numBytes = headerSize;
				}
#if RSG_DURANGO
				else if(allowCompressedAssetLoad)
				{
					numApuBytes = numBytes;
					numBytes = 0;
				}
#endif

				bool isVirtual = false;
				if(strstr(name, "_VIRTUAL"))
				{
					isVirtual = true;
				}
				// ensure wave sample data is aligned in memory
				numBytes = (numBytes + g_WaveSlotAlignment-1) & ~(g_WaveSlotAlignment-1);

#if __BANK				
				u32 expectedSlotSize = GetExpectedWaveslotSize(name);
				bool sizeValid = expectedSlotSize == 0u || numBytes <= expectedSlotSize;
				audAssertf(sizeValid, "Audio wave slot %s size (%u) differs from expected size (%u)", name, numBytes, expectedSlotSize);
								
				if(numVRAMBytes)
				{
					audDisplayf("slot %s main memory size: %d VRAM size: %d (%s)", name, numBytes, numVRAMBytes, sizeValid ? "VALID" : "INVALID");
				}
				else if(numApuBytes)
				{
					audDisplayf("slot %s main memory size: %d, APU size: %d : MaxHeaderSize %d (%s)", name, numBytes, numApuBytes, slotData->MaxHeaderSize, sizeValid ? "VALID" : "INVALID");
				}
				else
				{
					audDisplayf("slot %s main memory size: %d : MaxHeaderSize %d (%s)", name, numBytes, slotData->MaxHeaderSize, sizeValid ? "VALID" : "INVALID");
				}
#endif

				if(!isVirtual)
				{
					totalBankMem += numBytes;
					totalBankVRAM += numVRAMBytes;
					totalApuRam += numApuBytes;
				}
				
				sm_NumWaveSlots++;
			}
		}

		if(totalBankVRAM > 0)
		{
			audDisplayf("Allocating total of %u bytes main RAM and %u bytes VRAM for %u wave slots\n", totalBankMem, totalBankVRAM, sm_NumWaveSlots);
		}
		else if(totalApuRam > 0)
		{
			audDisplayf("Allocating total of %u bytes main RAM and %u bytes Apu RAM for %u wave slots\n", totalBankMem, totalApuRam, sm_NumWaveSlots);
		}
		else
		{
			audDisplayf("Allocating total of %u bytes for %u wave slots\n", totalBankMem, sm_NumWaveSlots);
		}

		sm_ReferenceCounts = rage_new u32[sm_NumWaveSlots];
		sysMemSet(sm_ReferenceCounts, 0, sizeof(u32) * sm_NumWaveSlots);
		
//		audAssert( audDriver::GetPhysicalAllocator() );
		audAssert( audDriver::GetVirtualAllocator() );
		if(!audDriver::GetPhysicalAllocator() || audDriver::GetPhysicalAllocator()->GetMemoryAvailable() < totalBankMem)
		{
			Quitf(ERR_AUD_INIT_2, "AudioHeap Pool Full, Size == %d (you need to raise AudioHeap PoolSize in common/data/gameconfig.xml)",fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("AudioHeap", 0x5293FA5), CONFIGURED_FROM_FILE));
		}
		audAssertf( !audDriver::GetPhysicalAllocator() || audDriver::GetPhysicalAllocator()->GetMemoryAvailable() > totalBankMem, "You tried to allocate %u bytes (physical) for audio banks but only %" SIZETFMT "u bytes are available, you need to increase the size of your audio heap", totalBankMem, audDriver::GetPhysicalAllocator()->GetMemoryAvailable() );
		audAssertf( audDriver::GetVirtualAllocator()->GetMemoryAvailable() > (AUD_WAVE_SLOT_SIZE * sm_NumWaveSlots), "You tried to allocate %u bytes (virtual) for wave slots but only %" SIZETFMT "u bytes are available, you need to increase the size of your audio heap", (AUD_WAVE_SLOT_SIZE * sm_NumWaveSlots), audDriver::GetVirtualAllocator()->GetMemoryAvailable() );

		if (externallyAllocatedBankHeap)
		{
			//	Make sure the heap is aligned
			u8* extHeap = (u8*)externallyAllocatedBankHeap;
			u8* waveData = (u8*)(((size_t)extHeap + 2047) & ~2047);
			unsigned memoryWaste = ptrdiff_t_to_int(waveData-extHeap);
			externallyAllocatedBankHeapSize -= memoryWaste;
			if (externallyAllocatedBankHeapSize < totalBankMem)
				Quitf(ERR_AUD_INIT_2,"[audWaveSlot::InitClass] We need %u bytes, but we only have %u bytes externally allocated for us", totalBankMem, externallyAllocatedBankHeapSize);
			sm_WaveSlotData = waveData;
			sm_OwnsWaveSlotData = false;
		}
		else
		{
			sm_WaveSlotData = (u8*)audDriver::AllocatePhysical(totalBankMem,2048);
			sm_OwnsWaveSlotData = true;
		}

		audWaveSlot::sm_TotalMainAllocation = totalBankMem;
		audWaveSlot::sm_TotalVRAMAllocation = totalBankVRAM;

		// allocate enough space for streaming wave slots since they are biggest type ...
		audWaveSlot::sm_WaveSlots = (audWaveSlot*)audDriver::AllocateVirtual(AUD_WAVE_SLOT_SIZE * sm_NumWaveSlots);

		audAssert(audWaveSlot::sm_WaveSlotData);
		audAssert(sm_WaveSlots);

		sm_WaveLoadingSlots.Reserve(numWaveLoadingSlots);

		s32 slotIndex = 0;
		for (u32 i = 0; i < waveSlotList->numSlotRefs; i++)
		{
			ravesimpletypes::Slot *slotData = audConfig::GetMetadataManager().GetObject<ravesimpletypes::Slot>(waveSlotList->SlotRef[i].SlotNameHash);
			if (!audVerifyf(slotData, "Failed to find slot data, index %u", i))
			{
				return;
			}

			const char *name = audConfig::GetMetadataManager().GetNameFromNTO_Always(slotData->NameTableOffset);
			const char *bankName = NULL;

			if (slotData->StaticBank && slotData->StaticBank != zeroHash)
			{
				const u32 bankId = g_AudioEngine.GetSoundManager().GetFactory().GetBankNameIndexFromHash(slotData->StaticBank);
				if (audVerifyf(bankId < AUD_INVALID_BANK_ID, "Static bank reference not in bank name list, slot %s", name))
				{
					bankName = GetBankName(bankId);
				}
			}

#if __BANK
			if (slotData->Size == 0)
			{
				audWarningf("Waveslot with size 0 : %s", name);
				audWarningf("This is OK for AMBIENT_SCRIPT_SLOT 11 to 16");
			}
#endif		
			audAssert(name);

			LoadType loadType;
			if(slotData->LoadType == ravesimpletypes::SLOT_LOAD_TYPE_WAVE)
			{
				loadType = kLoadTypeWave;
			}
			else if(slotData->LoadType == ravesimpletypes::SLOT_LOAD_TYPE_STREAM)
			{
				loadType = kLoadTypeStream;
			}
			else
			{
				audAssert(slotData->LoadType == ravesimpletypes::SLOT_LOAD_TYPE_BANK);
				loadType = kLoadTypeBank;
			}
			
#if RSG_DURANGO
			bool allowCompressedAssetLoad = AllowCompressedAssetLoad(loadType, name);
#endif

			if((allocateTestSlot || strcmp(name, "TEST")) && slotData->Size != 0)
			{
				void *mem = GetWaveSlotFromIndex(slotIndex++);

				numBytes = slotData->Size;
				u32 numVRAMBytes = 0;

				maxHeaderSizeBytes = slotData->MaxHeaderSize;

				u32 vramStartOffset = 0;
				if (loadType == kLoadTypeBank && bankDataInVram && IsVramSlot(name))
				{
					if(bankName)
					{
						u32 bankNameHash = atStringHash(bankName);
						u32 sampleDataOffset = 0;
						u32 sampleDataSize = 0;
						ASSERT_ONLY(bool precalculatedDataFound = false;)

						// We've already been through and checked these values earlier in the function, so no need
						// to verify that they're up to date
						for(s32 loop = 0; loop < sm_StaticBankInfoList.GetCount(); loop++)
						{
							if(sm_StaticBankInfoList[loop].bankNameHash == bankNameHash)
							{
								sampleDataSize = sm_StaticBankInfoList[loop].sampleDataSize;
								sampleDataOffset = sm_StaticBankInfoList[loop].sampleDataOffset;
								ASSERT_ONLY(precalculatedDataFound = true;)
								break;
							}
						}

						audAssert(precalculatedDataFound);
						audAssert(sampleDataOffset);

						numVRAMBytes = sampleDataSize;
						numBytes = sampleDataOffset;
						vramStartOffset = numBytes;
					}
					else
					{
						// full bank goes into vram
						numVRAMBytes = numBytes;
						// full header plus metadata goes into main ram
						numBytes = maxHeaderSizeBytes + slotData->MaxMetadataSize;
						vramStartOffset = 0;
					}
				}
				else if((loadType == kLoadTypeStream && streamDataInVram) ||
					(loadType == kLoadTypeWave && waveDataInVram))
				{
					// Header size is computed to include all stream metadata
					u32 headerSize = maxHeaderSizeBytes;

					if(loadType == kLoadTypeWave)
					{
						numVRAMBytes = slotData->MaxDataSize;
						headerSize += slotData->MaxMetadataSize;
					}
					else
					{
						numVRAMBytes = numBytes - headerSize;
					}
					numBytes = headerSize;
				}
				
				
				// ensure wave sample data is aligned in memory
				numBytes = (numBytes + g_WaveSlotAlignment-1) & ~(g_WaveSlotAlignment-1);
				
				audWaveSlot *slot = NULL;
				bool isVirtual = false;
				if (strstr(name, "_VIRTUAL"))
				{
					isVirtual = true;
				}
				if(loadType == kLoadTypeStream)
				{
#if RSG_DURANGO
					u8* data = NULL;
					audDecoderXboxOneXma::AllocateFromAPUMemory(&data, numBytes);
					slot = new(mem) audStreamingWaveSlot(data, numBytes, maxHeaderSizeBytes, name, isVirtual);
#else

					slot = new(mem) audStreamingWaveSlot(audWaveSlot::sm_WaveSlotData + curOffset, numBytes, maxHeaderSizeBytes, name, isVirtual);
#endif
				}
				else
				{
#if RSG_DURANGO
					if(allowCompressedAssetLoad)
					{
						u8* data = NULL;
						audDecoderXboxOneXma::AllocateFromAPUMemory(&data, numBytes);
						audAssert(data);
						slot = new(mem) audWaveSlot(data, numBytes, maxHeaderSizeBytes, name, isVirtual);
					}
					else
					{
						slot = new(mem) audWaveSlot(audWaveSlot::sm_WaveSlotData + curOffset, numBytes, maxHeaderSizeBytes,	name, isVirtual);
					}
#else
					slot = new(mem) audWaveSlot(audWaveSlot::sm_WaveSlotData + curOffset, numBytes, maxHeaderSizeBytes, name, isVirtual);
#endif
				}

				if(loadType == kLoadTypeWave)
				{
					sm_WaveLoadingSlots.Append() = slot;
				}

				Assign(slot->m_LoadType, loadType);
#if RSG_DURANGO
				if(!allowCompressedAssetLoad)
#endif
				{
					if(!isVirtual)
					{
						curOffset += numBytes;
					}

					audAssert(curOffset <= totalBankMem);
				}


				if(numVRAMBytes)
				{
					slot->GetInternalState()->BankDataVram = (u8*) physical_new(numVRAMBytes,16);
					slot->GetInternalState()->VramAllocationSize = numVRAMBytes;
					slot->GetInternalState()->MainDataSize = numBytes;
					slot->GetInternalState()->VRAMLoadOffset = vramStartOffset;
				}

				if(loadType == kLoadTypeStream)
				{
					((audStreamingWaveSlot*)slot)->Init();
				}

				const u32 slotHashName = atStringHash(name);
				if(sm_WaveSlotTable.Access(slotHashName))
				{
#if !__FINAL || RSG_PC
					Printf("Duplicate wave slot %s", name);
					Quitf(ERR_AUD_INVALIDRESOURCE,"Invalid resource detected - Please re-install the game");
#endif // !__FINAL || RSG_PC
				}
				else
				{
					sm_WaveSlotTable.Insert(slotHashName,slot); 
				}
			}
		}		

#if !__FINAL && RSG_PS3
		bool dataFileUpdated = false;

		const char *reportFileName = "staticBankInfo.dat";
		if(PARAM_computeStaticBankInfo.Get(reportFileName))
		{
			fiStream* stream = ASSET.Create(reportFileName, "");

			if(stream)
			{
				u32 numBanks = sm_StaticBankInfoList.GetCount();
				stream->Write(&numBanks, sizeof(numBanks));

				for(s32 loop = 0; loop < sm_StaticBankInfoList.GetCount(); loop++)
				{
					audStaticBankInfo bankInfo = sm_StaticBankInfoList[loop];
					stream->Write(&bankInfo, sizeof(audStaticBankInfo));
				}

				stream->Close();
				dataFileUpdated = true;
			}
		}
#endif

#if RSG_PS3
		// Running with custom audio data could legitimately invalidate the pre-calculated data, but otherwise throw a fatal error if invalid data is found. We don't verify
		// the correctness of the size values in non-dev builds, so it is vital that they are correct. Quitf here should ensure that anyone running an audio sync should hit 
		// this before bad data makes it into the main build (if not, you know they've been naughty and didn't actually test it!)
#if !__FINAL
		if(!PARAM_audiofolder.Get())
#endif
		{
			if(throwPrecalculatedDataError)
			{
#if !__FINAL
				if(dataFileUpdated)
				{
					Quitf("AUDIO: Precalculated data in %s has been updated, please re-run the build to verify correctness", reportFileName);
				}
				else
#endif
				{
					Quitf(ERR_AUD_INVALIDRESOURCE, "AUDIO: Precalculated size data for one or more banks was not found or was invalid. In order to resolve this:\n\n1) Check out x:\\gta5\\build\\dev\\ps3\\audio\\staticBankInfo.dat\n2) Run the build with -p3:computeStaticBankInfo=x:\\gta5\\build\\dev\\ps3\\audio\\staticBankInfo.dat to regenerate the file\n3) Run the build once more to verify that the updated data is correct\n4) Submit the updated file");
				}
			}
		}
	
#endif
	}

	LoadStaticBanks();

	// Create a task scheduler for handling async decryption
	const unsigned stackSize = __DEV ? 0x18000 : 0x10000;
	const unsigned scratchSize = 0x8000;

#if RSG_DURANGO
	// 10/16/14 - Don't run on 0x8 (Core 3) on Durango, to avoid conflicts with the 
	// Render Thread. Even though this scheduler runs at lowest priority, which is 
	// less than the render thread, Microsoft's OS scheduler appears to have issues 
	// waking a sleeping thread (Render Thread), which is higher priority, and whose 
	// sleep time is up, and giving it the core. So we have issues where the btea task 
	// will take ~10-25 ms, and stall out the render thread...even though the render 
	// thread doesn't want to sleep anymore and is a higher priority thread.
	// Avoid this conflict at this point by simply stripping 0x8 out the core mask on 
	// Durango.
	int cpuMask = (0x2|0x4|0x20); //|0x8|0x20);
#else
	int cpuMask = (0x2|0x4|0x8|0x20);
#endif

#if (RSG_PC)
	if (sysTaskManager::GetNumThreads() <= 3)
		cpuMask = 0xffffffff;
#endif	// RSG_PC

	sm_iSchedulerIndex = sysTaskManager::AddScheduler("AudSched", stackSize, scratchSize, PRIO_LOWEST, cpuMask, 0);
}

#if __BANK
u32 audWaveSlot::GetExpectedWaveslotSize(const char* slotName)
{
	if(strstr(slotName, "_VIRTUAL"))
	{
		return 0u;
	}

	const ravesimpletypes::VariableList *expectedSlotSizeList = NULL;

#if RSG_ORBIS
	expectedSlotSizeList = audConfig::GetMetadataManager().GetObject<ravesimpletypes::VariableList>("driverSettings_ExpectedWaveSlotSizes:ps4");
#elif RSG_DURANGO
	expectedSlotSizeList = audConfig::GetMetadataManager().GetObject<ravesimpletypes::VariableList>("driverSettings_ExpectedWaveSlotSizes:xboxone");
#elif RSG_PC
	expectedSlotSizeList = audConfig::GetMetadataManager().GetObject<ravesimpletypes::VariableList>("driverSettings_ExpectedWaveSlotSizes:pc");
#endif

	if (expectedSlotSizeList)
	{
		u32 slotNameHash = atStringHash(slotName);

		for (u32 i = 0; i < expectedSlotSizeList->numVariables; i++)
		{
			if (expectedSlotSizeList->Variable[i].Name == slotNameHash)
			{
				return (u32)expectedSlotSizeList->Variable[i].InitialValue;
			}
		}

		audAssertf(false, "Failed to find expected slot size entry for waveslot %s", slotName);
	}

	return 0u;
}
#endif

void audWaveSlot::LoadStaticBanks()
{
#if !__FINAL
	const bool allocateTestSlot = PARAM_audiotestslot.Get();
#else
	const bool allocateTestSlot = false;
#endif

	const ravesimpletypes::WaveSlots *waveSlotList = audConfig::GetMetadataManager().GetObject<ravesimpletypes::WaveSlots>("WaveSlotsList");

	if(!audVerifyf(waveSlotList, "Failed to find wave slot list in metadata"))
	{
		return;
	}

	for(u32 i = 0; i < waveSlotList->numSlotRefs; i++)
	{
		ravesimpletypes::Slot *slotData = audConfig::GetMetadataManager().GetObject<ravesimpletypes::Slot>(waveSlotList->SlotRef[i].SlotNameHash);
		if(!audVerifyf(slotData, "Failed to find slot data, index %u", i))
		{
			return;
		}

		const char *name = audConfig::GetMetadataManager().GetNameFromNTO_Always(slotData->NameTableOffset);
		const char *bankName = NULL;

		if(allocateTestSlot || strcmp(name, "TEST"))
		{
			if(slotData->StaticBank && slotData->StaticBank != ATSTRINGHASH("0", 0x6E3C5C6B))
			{
				const u32 bankId = g_AudioEngine.GetSoundManager().GetFactory().GetBankNameIndexFromHash(slotData->StaticBank);
				if(audVerifyf(bankId < AUD_INVALID_BANK_ID, "Static bank reference not in bank name list, slot %s", name))
				{
					bankName = GetBankName(bankId);

					if(bankName)
					{
						u32 bankId = FindBankId(bankName);
						if(bankId < AUD_INVALID_BANK_ID)
						{
							audDisplayf("Static Bank: %s into slot %s", bankName, name);

							audWaveSlot *slot = FindWaveSlot(name);
							if(audVerifyf(slot, "Couldn't find slot %s", name))
							{
								// load data asynchronously
								slot->LoadBank(audWaveSlot::FindBankId(bankName));
								slot->MarkSlotAsStatic();
							}
						}
						else
						{
							audErrorf("Static bank %s not in bank name list", bankName);
						}
					}
				}
			}
		}
	}
	
}

void audWaveSlot::ShutdownClass(void)
{
	// Map does not delete the data, so do it ourselves.
	audWaveSlot* pWaveSlot = NULL;
	atMap<u32, audWaveSlot*>::Iterator itSlot = sm_WaveSlotTable.CreateIterator();
	while (!itSlot.AtEnd())
	{
		pWaveSlot   = itSlot.GetData();
		itSlot.Next();

		if (pWaveSlot)
		{
			pWaveSlot->~audWaveSlot();
		}
	}
	sm_WaveSlotTable.Kill();

	// static bank table references the same waveslots that live in the waveslottable
	/*atMap<u16, audWaveSlot*>::Iterator itBank = sm_LoadedBankTable.CreateIterator();
	while (!itBank.AtEnd())
	{
		pWaveSlot  = itBank.GetData();
		itBank.Next();

		if (pWaveSlot)
		{
			pWaveSlot->~audWaveSlot();
		}
	}*/
	sm_LoadedBankTable.Kill();
	sm_LoadedWaveTable.Kill();

	// free all search path strings since we StringDuplicate() when adding them
	// to the list
	atMap<u32, const char*>::Iterator iter = sm_SearchPaths.CreateIterator();
	while(!iter.AtEnd())
	{
		delete[] iter.GetData();
		iter.Next();
	}
	sm_SearchPaths.Kill();

	if (sm_WaveSlotData && sm_OwnsWaveSlotData)
	{
		audDriver::FreePhysical(sm_WaveSlotData);
	}

	if (sm_WaveSlots)
	{
		audDriver::FreeVirtual(sm_WaveSlots);
	}

	if(sm_ReferenceCounts)
	{
		delete[] sm_ReferenceCounts;
		sm_ReferenceCounts = NULL;
	}
}


void audWaveSlot::RemoveFromLoadedBankTable()
{
	SYS_CS_SYNC(sm_BankTableCriticalSectionToken);
	// If this slot has already been inserted in the static bank table, remove that reference
	u16 keyToRemove = 0xffff;
	atMap<u16, audWaveSlot*>::Iterator iter = sm_LoadedBankTable.CreateIterator();
	while(!iter.AtEnd())
	{
		if(iter.GetData() == this)
		{
			keyToRemove = iter.GetKey();
			break;
		}
		iter.Next();
	}
	if(keyToRemove != 0xffff)
	{
		// Since we can have multiple slots in the loaded bank table pointing to the same bank id, we need to
		// ensure we leave the other slots with this key intact.  Since the map doesn't have a Remove(key,data) override
		// we remove all matching and add back in the ones we want to remain.
		atFixedArray<audWaveSlot *, 16> slotsToRemain;
		u32 slotsToRemove = 0;
		iter = sm_LoadedBankTable.CreateIterator();
		while(!iter.AtEnd())
		{
			if(iter.GetKey() == keyToRemove)
			{
				if(iter.GetData() != this)
				{
					slotsToRemain.Push(iter.GetData());
				}
				slotsToRemove++;
			}
			iter.Next();
		}
		audAssertf(slotsToRemove >= 1, "Loaded bank slots table didn't include bank id %u", keyToRemove);
		while(slotsToRemove-- > 0)
		{
			sm_LoadedBankTable.Delete(keyToRemove);
		}

		for(s32 i = 0; i < slotsToRemain.GetCount(); i++)
		{
			audWarningf("Preserving bank %s (%u) in slot %s", GetBankName(keyToRemove), keyToRemove, slotsToRemain[i]->GetSlotName());
			sm_LoadedBankTable.Insert(keyToRemove, slotsToRemain[i]);
		}
	}
}

void audWaveSlot::RemoveFromLoadedWaveTable()
{
	// If this slot has already been inserted in the static wave table, remove that reference
	u32 keyToRemove = 0xffffffff;
	atMap<u32, audWaveSlot*>::Iterator iter = sm_LoadedWaveTable.CreateIterator();
	while(!iter.AtEnd())
	{
		if(iter.GetData() == this)
		{
			keyToRemove = iter.GetKey();
			break;
		}
		iter.Next();
	}
	if(keyToRemove != 0xffffffff)
	{
		sm_LoadedWaveTable.Delete(keyToRemove);
	}
}

int WaveSlotRequestCompare(const u8 *a, const u8 *b)
{
	const audWaveSlot *slotA = audWaveSlot::GetWaveSlotFromIndexFast(*a);
	const audWaveSlot *slotB = audWaveSlot::GetWaveSlotFromIndexFast(*b);

	if(slotA->GetRequestedLoadPriority() != slotB->GetRequestedLoadPriority())
	{
		// Priorities are different; sort by priority in ascending order
		return static_cast<int>(slotB->GetRequestedLoadPriority()) - static_cast<int>(slotA->GetRequestedLoadPriority());
	}

	// Same priority; sort by request time in descending order
	return static_cast<int>(slotA->GetLoadRequestTimeMs()) - static_cast<int>(slotB->GetLoadRequestTimeMs());
}

void audWaveSlot::UpdateSlots(void)
{
	sysCriticalSection lock(sm_UpdateCriticalSection);

	bool isLoading = false;
	bool isLoadingStreamingSlot = false;

	atFixedArray<u8, 256> waveSlotsWithRequests;
	atRangeArray<u32, kNumWaveLoadPrioLevels> requestPrioCounts;
	u32 requestCount = 0;
	sysMemZeroBytes<sizeof(requestPrioCounts)>(&requestPrioCounts);

#if __BANK
	u32 currentlyLoadingSlotIndex = ~0U;
	//Do a pass over all wave slots to check if we have one that is currently loading.
	static f32* s_NextLogTime = rage_new f32[sm_NumWaveSlots];
	sysMemSet(s_NextLogTime, 0, sizeof(f32) * sm_NumWaveSlots);
#endif
	for(u32 i = 0; i < sm_NumWaveSlots; i++)
	{
#if FAST_WAVESLOT
		audWaveSlot *waveSlot = GetWaveSlotFromIndexFast(i);
	#if FAST_WAVESLOT_PREFETCH
		PrefetchObject( GetWaveSlotFromIndexFast(i+1) );
	#endif
#else
		audWaveSlot *waveSlot = GetWaveSlotFromIndex(i);
#endif
		if(waveSlot)
		{
			// we want to allow one streaming slot to service a load as quickly as possible if one is requested, regardless of
			// any other slots.
			// Any slot in the middle of a load also needs updated.
			if(waveSlot->GetIsLoading() || (!isLoadingStreamingSlot && waveSlot->IsStreaming()))
			{				
				UpdateWaveSlot(waveSlot);
				if(waveSlot->GetIsLoading()) //Are we still loading?
				{
					//Only allow one wave slot to load data at a time.
					isLoading = true;
					
					BANK_ONLY(currentlyLoadingSlotIndex = i);


					if(waveSlot->IsStreaming())
					{
#if __BANK
						if(s_NextLogTime[i] <= 0.f)
						{
							s_NextLogTime[i] = 1.f;
							if(PARAM_waveSlotsSpew.Get())
								audDebugf1("[audWaveSlotsSpew] %.2f %s is streaming %s", audDriver::GetMixer()->GetMixerTimeS(), waveSlot->GetSlotName(), waveSlot->GetLoadedBankName());
						}
#endif
						isLoadingStreamingSlot = true;
					}
					else
					{
#if __BANK
						if(s_NextLogTime[i] <= 0.f)
						{
							s_NextLogTime[i] = 1.f;
							if(PARAM_waveSlotsSpew.Get())
								audDebugf1("[audWaveSlotsSpew] %.2f %s is loading %s/%u", audDriver::GetMixer()->GetMixerTimeS(), waveSlot->GetSlotName(), waveSlot->GetLoadedBankName(), waveSlot->GetLoadedWaveNameHash());
						}
#endif
					}
#if __BANK
					if(s_NextLogTime[i] > 0.f)
					{
						s_NextLogTime[i] -= fwTimer::GetTimeStep();
					}
#endif
				}
			}
			else if(waveSlot->m_IsLoadRequestQueued)
			{
				requestCount++;
				requestPrioCounts[waveSlot->GetRequestedLoadPriority()]++;
				waveSlotsWithRequests.Push((u8)i);
			}
		}
	}

	sm_RequestQueueCount = requestCount;
	for(s32 i = 0; i < kNumWaveLoadPrioLevels; i++)
	{
		sm_RequestQueuePrioCounts[i] = requestPrioCounts[i];
	}
#if RSG_BANK
	
	sm_CurrentlyLoadingSlotIndex = currentlyLoadingSlotIndex;

	if(sm_DrawRequestQueue)
	{
		SYS_CS_SYNC(sm_DrawRequestQueueLock);
		// copy the request list and sort it
		sm_RequestQueueToDraw = waveSlotsWithRequests;
		sm_RequestQueueToDraw.QSort(0, -1, &WaveSlotRequestCompare);
	}
#endif

	if(!isLoading)
	{
		UpdateBatchWaveLoadRequest();
		if(sm_IsBatchWaveLoadRequested) // are we still loading?
		{
			isLoading = true;
		}
	}

	if(!isLoading)
	{
		//We don't yet have any active loads, so action the earliest, highest priority one
		waveSlotsWithRequests.QSort(0, -1, &WaveSlotRequestCompare);
		
		for(s32 i = 0; i < waveSlotsWithRequests.GetCount(); i++)
		{
			audWaveSlot *highestPrioSlot = GetWaveSlotFromIndexFast(waveSlotsWithRequests[i]);
			UpdateWaveSlot(highestPrioSlot);
			if(highestPrioSlot->GetIsLoading())
			{
				//Only allow one wave slot to load data at a time.
				return;
			}
		}
	}
}

void audWaveSlot::UpdateBatchWaveLoadRequest()
{
	sysCriticalSection lock(sm_BatchWaveLoadCriticalSectionToken);
	if(sm_IsBatchWaveLoadRequested)
	{
		audWaveSlotLoadStatus status = audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->GetWaveLoadingStatus(
				sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].bankId, 
					sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].waveNameHash);
		if(status == FAILED)
		{
			audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->DisableBatchMode();
			sm_IsBatchWaveLoadRequested = false;
			sm_HasBatchWaveLoadFailed = true;
		}
		else if(status == LOADED)
		{
			audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->DisableBatchMode();
			sm_CurrentBatchLoadRequestStage++;		
			if(sm_CurrentBatchLoadRequestStage == sm_NumWaveLoadRequestsInBatch)
			{
				// finished batch load
				// unlock the streamer
				PF_SET(AudioLockingStreamer, 0);

				pgStreamer::Unlock();
				sm_IsBatchWaveLoadRequested = false;

#if __BANK 
				if(PARAM_waveSlotsSpew.Get())
					audDebugf1("[audWaveSlotsSpew] %.2f finished batch load", audDriver::GetMixer()->GetMixerTimeS());
#endif
			}
			else
			{
				// request load on the next slot immediately
				audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->EnableBatchMode();
				audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->LoadWave(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].bankId,
					sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].waveNameHash);
				UpdateWaveSlot(audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex));
			}
		}
		else if(status == LOADING)
		{
			UpdateWaveSlot(audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex));
		}
		else if(status == NOT_REQUESTED)
		{
			audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->EnableBatchMode();
			audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->LoadWave(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].bankId,
				sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].waveNameHash);
			if(audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->GetReferenceCount())
			{
				audErrorf("Batch wave load failed due to references on waveslot %s, loaded bank %s, [%u refs]", 
					audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->GetSlotName(), 
					audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->GetLoadedBankName(),
					audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->GetReferenceCount());
				audWaveSlot::GetWaveSlotFromIndex(sm_BatchWaveLoadRequest[sm_CurrentBatchLoadRequestStage].slotIndex)->DisableBatchMode();
				sm_IsBatchWaveLoadRequested = false;
				sm_HasBatchWaveLoadFailed = true;
			}
		}
	}
}

void audWaveSlot::Drain()
{
	bool waiting = false;
	for(u32 i = 0; i < sm_NumWaveSlots; i++)
	{
#if FAST_WAVESLOT
		audWaveSlot *waveSlot = GetWaveSlotFromIndexFast(i);
	#if FAST_WAVESLOT_PREFETCH
		PrefetchObject( GetWaveSlotFromIndexFast(i+1) );
	#endif
#else
		audWaveSlot *waveSlot = GetWaveSlotFromIndex(i);
#endif
		if(waveSlot)
		{
			if(waveSlot->m_IsLoading || waveSlot->m_IsLoadRequestQueued)
			{
				audWarningf("Slot %s has pending load request", waveSlot->GetSlotName());
				waiting = true;
			}
		}
	}

	if(waiting)
	{
		audWarningf("Waiting for pending audio loads to finish...");
		bool stillWaiting = false;
		u32 timeStartedWaiting = sysTimer::GetSystemMsTime();
		do 
		{
			stillWaiting = false;
			sysIpcSleep(33);

			for(u32 i = 0; i < sm_NumWaveSlots; i++)
			{
	#if FAST_WAVESLOT
				audWaveSlot *waveSlot = GetWaveSlotFromIndexFast(i);
	#else
				audWaveSlot *waveSlot = GetWaveSlotFromIndex(i);
	#endif
				if(waveSlot)
				{
					if(waveSlot->m_IsLoading || waveSlot->m_IsLoadRequestQueued)
					{
						stillWaiting = true;
					}
				}
			}

			if(!stillWaiting)
			{
				audDisplayf("Audio loads completed");
			}
			else if(sysTimer::GetSystemMsTime() > timeStartedWaiting + 15000)
			{
				// waiting for more than 15 seconds...
				audErrorf("Failed to complete audio loads after 15 sec.  Aborting and hoping for the best");
				stillWaiting = false;
			}

		} while(stillWaiting);
		
	}
}

const char *audWaveSlot::GetBankName(const u32 bankId)
{
	return g_AudioEngine.GetSoundManager().GetFactory().GetBankNameFromIndex(bankId);
}

u32 audWaveSlot::FindBankId(const char *bankName)
{
	return g_AudioEngine.GetSoundManager().GetFactory().GetBankIndexFromName(bankName);
}

u32 audWaveSlot::FindBankId(const u32 bankNameHash)
{
	return g_AudioEngine.GetSoundManager().GetFactory().GetBankIndexFromName(bankNameHash);
}

bool audWaveSlot::RequestBatchWaveLoad(audBatchWaveLoadRequest *waveRequests, const u32 numWaveLoads)
{
	sysCriticalSection lock(sm_BatchWaveLoadCriticalSectionToken);
	audAssert(!sm_IsBatchWaveLoadRequested);
	if(!sm_IsBatchWaveLoadRequested)
	{
		audAssert(numWaveLoads <= g_MaxWaveLoadsPerBatch);
		if(numWaveLoads < g_MaxWaveLoadsPerBatch)
		{
			sm_NumWaveLoadRequestsInBatch = numWaveLoads;
			sysMemCpy(&sm_BatchWaveLoadRequest[0], waveRequests, numWaveLoads*sizeof(audBatchWaveLoadRequest));
			sm_BatchWaveLoadRequestTime = audEngineUtil::GetCurrentTimeInMilliseconds();
			sm_CurrentBatchLoadRequestStage = 0;
			sm_IsBatchWaveLoadRequested = true;
			sm_HasBatchWaveLoadFailed = false;

#if __BANK 
			if(PARAM_waveSlotsSpew.Get())
				audDebugf1("[audWaveSlotsSpew] %.2f requested batch load", audDriver::GetMixer()->GetMixerTimeS());
#endif
			return true;
		}
	}
	return false;
}

void audWaveSlot::BaseUpdate(void)
{
	if(m_IsLoading)
	{
		bool isLoadFinished = false;
		//Check to see if loading is complete.
		if(m_DecryptionTaskHandle != NULL || sysIpcPollSema(GetInternalState()->BankLoadSema))
		{
			isLoadFinished = true;
		}

#if RSG_BANK
		if(sm_DelayRequests && isLoadFinished)
		{
			Assert(!m_RequestDelayTimerSet);
			m_RequestDelayTimerSet = true;
			m_RequestDelayTimer = audEngineUtil::GetCurrentTimeInMilliseconds();
		}

		if(m_RequestDelayTimerSet)
		{
			if(audEngineUtil::GetCurrentTimeInMilliseconds() < m_RequestDelayTimer + sm_RequestDelayTime)
			{
				isLoadFinished = false;
			}
			else
			{
				m_RequestDelayTimerSet = false;
				isLoadFinished = true;
			}
		}
#endif	

		if(isLoadFinished)
		{
			//Load complete.
			HandleLoadCompletion();
		}
	}

	//Intentional fall through allowing a Bank/Wave load that just completed to be replaced immediately.
	if(!m_IsLoading && m_IsLoadRequestQueued)
	{
		RequestLoad();
	}
}

void audWaveSlot::SetSearchPath(const char *packPrefix, const char *rootPath)
{
	audAssert(strlen(packPrefix) == 8);
	const u32 packPrefixHash = atStringHash(packPrefix);
	// Purposefully excluding DLC_XMAS from this assert as we already worked around this conflict by duplicating data across packs, this is just a warning for any future packs
	audAssertf(packPrefixHash == ATSTRINGHASH("DLC_XMAS", 0xC37BF2C6) || !sm_SearchPaths.Access(packPrefixHash) || (sm_SearchPaths.Access(packPrefixHash) && !stricmp(*sm_SearchPaths.Access(packPrefixHash), rootPath)), "Duplicate search path found for pack %s (root path %s)", packPrefix, rootPath);
	audDisplayf("Adding search path %s for pack prefix %s", rootPath, packPrefix);
	sm_SearchPaths.Insert(packPrefixHash, StringDuplicate(rootPath));	
}

void audWaveSlot::RemoveSearchPath(const char *packPrefix)
{
	audAssert(strlen(packPrefix) == 8);
	const u32 packPrefixHash = atStringHash(packPrefix);
	const char **searchPath = sm_SearchPaths.Access(packPrefixHash);
	if(searchPath)
	{
		StringFree(*searchPath);
		sm_SearchPaths.Delete(packPrefixHash);
	}
}

void audWaveSlot::ComputeBankFilePath(char *dest, const u32 destSize, const char *bankName)
{
	audAssert(strlen(bankName) >= 8);

	char packPrefix[9];
	strncpy(packPrefix, bankName, 8);
	packPrefix[8] = '\0';

	const u32 packPrefixHash = atStringHash(packPrefix);
	const char **searchPath = sm_SearchPaths.Access(packPrefixHash);
	
	const char *rootPath = searchPath ? *searchPath : audDriver::GetConfig().GetWaveRootPath();

#if RSG_PC
#define AUDIO_PLATFORM_STRING "x64"
#elif RSG_DURANGO
#define AUDIO_PLATFORM_STRING "xboxone"
#else
#define AUDIO_PLATFORM_STRING "ps4"
#endif

	// See B*2618401, B*3680768. Xmas2 pack data was duplicated into Xmas3, so re-routing all xmas 2 and 3 requests to xmas3 pack
	if(packPrefixHash == ATSTRINGHASH("DLC_XMAS", 0xC37BF2C6))
	{
		rootPath = "dlc_mpxmas_604490:/" AUDIO_PLATFORM_STRING "/audio/sfx/";
	}
	// Special case handling for the low-latency bank
	else if(!PARAM_audiofolder.Get() && packPrefixHash == ATSTRINGHASH("resident", 0x231498DE) && !strncmp(bankName + 9, "LOW", 3))
	{
		rootPath = "update:/" AUDIO_PLATFORM_STRING "/audio/sfx/";
	}

	// if we're using RPF files, remove any special characters (ie $/) so map works
	char fullWaveRootPath[RAGE_MAX_PATH];
	ASSET.FullPath(fullWaveRootPath, sizeof(fullWaveRootPath), rootPath, "");
	
	formatf(dest, destSize, "%s%s.awc",fullWaveRootPath, bankName);
}

void dummy_callback(void *,void *, u32, u32) {}

void dummy_unlocking_callback(void *,void *, u32, u32) 
{
	pgStreamer::Unlock();
}

extern void rscbuilder_signal_callback(void *userArg,void * /*dest*/,u32 /*offset*/,u32 /*amtRead*/);

void dummy_unlocking_callback_sema(void *userArg,void *dest, u32 offset, u32 amtRead) 
{
	rscbuilder_signal_callback(userArg,dest,offset,amtRead);
	pgStreamer::Unlock();
}

void audWaveSlot::RequestLoad(void)
{
	bool loadFailed = false;	
	u16 bankIdToLoad = 0xffff;
	char bankFilePath[RAGE_MAX_PATH]={0};

	//**Start of Bank Load Critical Section**//
	{
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

		m_LoadFailedBankId = AUD_INVALID_BANK_ID;
		m_LoadFailedWaveNameHash = 0;

		if(GetReferenceCount() > 0)
		{
			//audWarningf("Attempted to load sample data while %u clients were still playing from the slot\n",
			//	m_ReferenceCount);
			return;
		}

		const char *bankName = GetBankName(m_BankIdToLoad);
		audAssert(bankName);
		if(bankName == NULL)
		{
			audErrorf("Invalid bank ID in audWaveSlot::RequestLoad (%u)", m_BankIdToLoad);
			m_LoadedBankId = m_BankIdToLoad;
			BANK_ONLY(CacheLoadedBankName();)
			CleanUpAfterLoadFailure();
			return;
		}

		audWaveSlot::ComputeBankFilePath(bankFilePath, bankName);
		audDisplayf("Slot %s is loading bank %s (ID: %u) from %s", GetSlotName(), bankName, m_BankIdToLoad, bankFilePath);
		
		bankIdToLoad = m_BankIdToLoad;

		//Invalidate existing slot contents.
		sysMemSet(GetInternalState()->BankData, 0, GetInternalState()->SlotBytes);
		GetInternalState()->Container.SetHeader(NULL);
		RemoveFromLoadedBankTable();
		RemoveFromLoadedWaveTable();
		m_BankIdToLoad = AUD_INVALID_BANK_ID;
		BANK_ONLY(CacheLoadingBankName();)
		m_IsLoadRequestQueued = false;
		m_LoadPriority = m_RequestedLoadPriority;
		m_RequestedLoadPriority = 0;
		m_IsLoading = true;
		m_LoadedBankId = bankIdToLoad;		//Only valid when m_IsLoading is false.
		BANK_ONLY(CacheLoadedBankName();)
		m_LoadedWaveNameHash = m_WaveNameHashToLoad;	//Only valid when m_IsLoading is false.
		m_WaveNameHashToLoad = 0;
		m_HasDecrypted = false;

	}//***End of Bank Load Critical Section***//

	//Issue queued load request to streamer.
	u32 bankSizeBytes;
	bool shouldLock = false;
	if((GetInternalState()->BankHandle = pgStreamer::Open(bankFilePath, &bankSizeBytes, false)) != pgStreamer::Error)
	{
		// Only process VRAM loads at this point when loading the full bank; for wave loads we always load the container header
		// into main RAM in the first read, then load wave data -> vram + metadata -> main in the second
		if(m_LoadedWaveNameHash == 0 && GetInternalState()->BankDataVram)
		{			
			datResourceChunk chunks[2];
			chunks[0].DestAddr = GetInternalState()->BankData;
			chunks[0].Size = Min(GetInternalState()->MainDataSize, bankSizeBytes);
		
			chunks[1].DestAddr = GetInternalState()->BankDataVram;
			GetInternalState()->VramDataSize = u32(chunks[1].Size = bankSizeBytes - GetInternalState()->VRAMLoadOffset);

			GetInternalState()->LoadedBytes = bankSizeBytes;

#if __BANK 
			if(PARAM_waveSlotsSpew.Get())
				audDebugf1("[audWaveSlotsSpew] %.2f: %s bank loading %s; %u bytes main, %u vram", audDriver::GetMixer()->GetMixerTimeS(), GetSlotName(), GetLoadedBankName(), (u32)chunks[0].Size, (u32)chunks[1].Size); 
#endif

			// TODO: static bank should be cached? ...
			// Both reads will be in the same streamer queue, so are guaranteed to complete in order
			// So when our sema is signalled both have completed
			bool lockStreamer = true;
			if(audDriver::GetMixer()->IsCapturing() && audDriver::GetMixer()->IsFrameRendering()) // force no locking when in fixed frame mode as the threads aren't running free
			{
				lockStreamer = false;
			}
			if (!pgStreamer::Read(GetInternalState()->BankHandle,chunks,1,0,(lockStreamer?dummy_unlocking_callback:dummy_callback),NULL,pgStreamer::HARDDRIVE,pgStreamer::UNCACHED | (lockStreamer?pgStreamer::LOCK:pgStreamer::NONE),NULL))
			{
				loadFailed = true;
			}
			else
			{
				if (!pgStreamer::Read(GetInternalState()->BankHandle, &chunks[1], 1, GetInternalState()->VRAMLoadOffset, GetInternalState()->BankLoadSema, pgStreamer::UNCACHED | pgStreamer::UNLOCK))
				{
					loadFailed = true;
				}
			}

			if(loadFailed)
			{
				pgStreamer::Close(GetInternalState()->BankHandle);
			}
		}
		else
		{
			u32 loadSizeBytes;
			if(m_LoadedWaveNameHash > 0)
			{
				//Read Wave Bank header.
				shouldLock = true;
				//Load the maximum possible header size for the Banks associated with this slot
				loadSizeBytes = bankSizeBytes < GetInternalState()->MaxHeaderSize ? bankSizeBytes : GetInternalState()->MaxHeaderSize;
				
				//The Bank header was padded to ensure that it's an integer multiple of 2048 bytes in length, so do the
				//same here to help with disc streaming.
				u32 padLengthBytes = 2048 - (loadSizeBytes % 2048);
				if(padLengthBytes < 2048)
				{
					loadSizeBytes += padLengthBytes;
				}

				// see if we already have the bank header in memory; common in the case of batch loaded speech conversations
				for(s32 i = 0; i < sm_WaveLoadingSlots.GetCount(); i++)
				{
					audWaveSlot *otherSlot = sm_WaveLoadingSlots[i];
					if(!otherSlot->GetIsLoading() && !otherSlot->IsWaitingOnDecryption() && otherSlot->GetLoadedBankId() == m_LoadedBankId)
					{
						audDebugf1("%s loading %s - saved disk hit: reading %u byte header from %s", GetSlotName(), GetLoadedBankName(), loadSizeBytes, otherSlot->GetSlotName());
						sysMemCpy(GetInternalState()->BankData, otherSlot->GetInternalState()->BankData, loadSizeBytes);
						HandleWaveLoadCompletion();
						return;
					}
				}
			}
			else
			{
				//Read entire Wave Bank.
				loadSizeBytes = bankSizeBytes;
				GetInternalState()->LoadedBytes = loadSizeBytes;			
			}
			// However, in the case of very differently sized speech banks (for example), the entire bank might be smaller than 
			// the max header size, so use the minimum - AFTER padding.
			loadSizeBytes = Min(loadSizeBytes, bankSizeBytes);
			
			if(audVerifyf(loadSizeBytes <= GetInternalState()->SlotBytes, "Invalid load: %s, %s, %u [loadSize: %u slotSize: %u]", GetSlotName(), bankFilePath , m_LoadedWaveNameHash, loadSizeBytes, GetInternalState()->SlotBytes))
			{
				datResourceChunk chunk;
				chunk.DestAddr = GetInternalState()->BankData;
				chunk.Size = loadSizeBytes;

#if __BANK 
				if(PARAM_waveSlotsSpew.Get())
					audDebugf1("[audWaveSlotsSpew] %.2f: %s loading %s/%u; %u bytes main", audDriver::GetMixer()->GetMixerTimeS(), GetSlotName(), GetLoadedBankName(), m_LoadedWaveNameHash, (u32)chunk.Size);
#endif

				if(audDriver::GetMixer()->IsCapturing() && audDriver::GetMixer()->IsFrameRendering()) // force no locking when in fixed frame mode as the threads aren't running free
				{
					shouldLock = false;
				}
				if (!pgStreamer::Read(GetInternalState()->BankHandle, &chunk, 1, 0, (shouldLock?dummy_unlocking_callback_sema:rscbuilder_signal_callback), GetInternalState()->BankLoadSema, pgStreamer::HARDDRIVE, pgStreamer::UNCACHED | (shouldLock?pgStreamer::LOCK:pgStreamer::NONE)))
				{
					pgStreamer::Close(GetInternalState()->BankHandle);
					loadFailed = true;
				}
				else if(shouldLock)
				{
					PF_SET(AudioLockingStreamer, 1);
				}
			}
			else
			{
				loadFailed = true;
			}
		}
	}
	else
	{
		loadFailed = true;
	}

	if(loadFailed)
	{
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

		if(m_LoadedWaveNameHash)
		{
			audErrorf("Error loading Wave %u from Bank %s", m_LoadedWaveNameHash, bankFilePath);
		}
		else
		{
			audErrorf("Error loading Wave Bank %s", bankFilePath);
		}

		CleanUpAfterLoadFailure();
	}
}
#define DELTA 0x9e3779b9

// Small modification to standard XXTEA, to obfuscate the key
#if RSG_ORBIS
#define TEA_STIRRER (0xa9d27bd3)
#elif RSG_DURANGO
#define TEA_STIRRER (0x5bcf38ad)
#else
#define TEA_STIRRER (0x7b3a207f)
#endif

#define MX (((z>>5^y<<2) + (y>>3^z<<4)) ^ ((sum^y) + (key[(p&3)^e] ^ z ^ TEA_STIRRER)))
 
  void btea(uint32_t *v, int n, const uint32_t *key) 
  {
	  if(!key)
		  return;

    uint32_t y, z, sum;
    unsigned p, rounds, e;
	
      n = -n;
      rounds = 6 + 52/n;
      sum = rounds*DELTA;
      y = v[0];
      do {
        e = (sum >> 2) & 3;
        for (p=n-1; p>0; p--) {
          z = v[p-1];
          y = v[p] -= MX;
        }
        z = v[n-1];
        y = v[0] -= MX;
      } while ((sum -= DELTA) != 0);
    
 }

  void bteatask(rage::sysTaskParameters &params)
  {
	  btea((u32*)params.Input.Data, -int(params.Input.Size >> 2), (u32*)params.ReadOnly[0].Data);
  }

void audWaveSlot::TeaStirEncryptionKey(u32* key)
{
	for (u32 i = 0; i < 4; i++)
	{
		key[i] = TEA_STIRRER ^ key[i];
	}
}

void audWaveSlot::HandleLoadCompletion(void)
{
	//**Start of Bank Load Critical Section**//
	{
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

#if __BANK 
		static f32 s_NextLoadLogTime = 0.f;
		if(s_NextLoadLogTime <= 0.f)
		{
			s_NextLoadLogTime = 1.f;
			if(PARAM_waveSlotsSpew.Get())
				audDebugf1("[audWaveSlotsSpew] %.2f: %s load complete %s/%u", audDriver::GetMixer()->GetMixerTimeS(), GetSlotName(), GetLoadedBankName(), m_LoadedWaveNameHash);
		}
		else
		{
			s_NextLoadLogTime -= fwTimer::GetTimeStep();
		}
#endif

		if(m_IsLoadRequestQueued && !m_DecryptionTaskHandle)
		{
			//This Bank/Wave is to be overwritten immediately, so don't waste any further time on it.
			if(m_LoadedWaveNameHash>0)
			{
				// we will have locked the streamer when we requested the header, so need to unlock it now
				PF_SET(AudioLockingStreamer, 0);
				pgStreamer::Unlock();
			}
			//Invalidate existing slot contents.
			sysMemSet(GetInternalState()->BankData, 0, GetInternalState()->SlotBytes);
			GetInternalState()->Container.SetHeader(NULL);
			m_LoadedBankId = AUD_INVALID_BANK_ID;
			BANK_ONLY(CacheLoadedBankName();)
			m_LoadedWaveNameHash = 0;
			m_IsLoading = false;
		}
		else
		{
			if(m_LoadedWaveNameHash > 0)
			{
				HandleWaveLoadCompletion();
			}
			else
			{
				// Check if we've kicked off a decryption job, and if so has it completed.
				if(m_DecryptionTaskHandle != NULL)
				{
					audAssertf(m_IsLoading, "Slot %s decrypting but not loading (%s)", GetSlotName(), GetLoadedBankName());
					if(!sysTaskManager::Poll(m_DecryptionTaskHandle))
					{
						return;
					}
					else
					{
						m_DecryptionTaskHandle = NULL;
						
						if(!adatContainer::IsValidHeader(GetInternalState()->BankData))
						{
							if(GetInternalState()->BankData)
							{
								OUTPUT_ONLY(const adatContainerHeader* pHeader = reinterpret_cast<const adatContainerHeader*>(GetInternalState()->BankData);)
								audDisplayf("Magic: %u", pHeader->magic);
								audDisplayf("Version: %u", pHeader->version);
								audDisplayf("Num Objects: %u", pHeader->numObjects);
								audDisplayf("Header Size %u", pHeader->totalHeaderSize);
							}
							
							// Promoting to fatal assert to attempt to catch url:bugstar:7013445
							FatalAssertf(false, "Invalid data post-decryption: %s - %s", GetSlotName(), GetBankName(m_LoadedBankId));

							CleanUpAfterLoadFailure();
							return;
						}
					}
				}

				if(!m_HasDecrypted AUD_SUPPORT_UNENCRYPTED_ASSETS_ONLY(&& !adatContainer::IsValidHeader(GetInternalState()->BankData)))
				{
					// loaded data is not valid - assume that it needs to be decrypted					
					//if(g_WaveSlotAsyncDecryption)
					{
						sysTaskParameters p;
						p.ReadOnlyCount = 1;
						p.ReadOnly[0].Data = const_cast<u32*>(GetKey(m_LoadedBankId));
						p.ReadOnly[0].Size = sizeof(u32) * 4;
						p.Input.Data = p.Output.Data = GetInternalState()->BankData;
						p.Input.Size = p.Output.Size = GetInternalState()->LoadedBytes;
						m_HasDecrypted = true;
						m_DecryptionTaskHandle = sysTaskManager::Create(TASK_INTERFACE(bteatask), p, sm_iSchedulerIndex);
						audAssert(m_DecryptionTaskHandle);
						return;
					}
					/*else
					{
						btea((u32*)GetInternalState()->BankData, -int(GetInternalState()->LoadedBytes>>2), GetKey());
						if(!audVerifyf(adatContainer::IsValidHeader(GetInternalState()->BankData), "Invalid data post-decryption: %s - %s", GetSlotName(), GetBankName(m_LoadedBankId)))
						{
							CleanUpAfterLoadFailure();
							return;
						}
					}*/
				}
				//The entire Bank has been loaded.
				GetInternalState()->Container.SetHeader((const adatContainerHeader*)GetInternalState()->BankData);
				if(!audVerifyf(GetInternalState()->Container.IsValid() && 
										GetInternalState()->Container.IsNativeEndian(), "Slot:%s Bank:%s", GetSlotName(), GetBankName(m_LoadedBankId)))
				{
					CleanUpAfterLoadFailure();
				}
				else
				{
					// loaded bytes includes the header - subtract this now we know how much non-header data we have
					audAssertf(GetInternalState()->LoadedBytes >= GetInternalState()->Container.GetHeaderSize(), "Slot:%s Bank:%s", GetSlotName(), GetBankName(m_LoadedBankId));
					GetInternalState()->ContainerHeaderSize = GetInternalState()->Container.GetHeaderSize();
					GetInternalState()->LoadedBytes -= GetInternalState()->ContainerHeaderSize;				
	
					m_IsLoading = false;
	
					GetInternalState()->LoadedContainerOffset = GetInternalState()->ContainerHeaderSize;
					GetInternalState()->HeaderPadding = 0;
	
					if(GetInternalState()->BankDataVram)
					{
						audAssertf(ComputeContiguousMetadataSize() <= GetInternalState()->MainDataSize, "Slot:%s Bank:%s", GetSlotName(), GetBankName(m_LoadedBankId));
					}
					RegisterLoadedBank();
				}
			}
		}

	}//***End of Bank Load Critical Section***//

	if(!m_IsLoading)
	{
		//We are done loading from this Bank
		pgStreamer::Close(GetInternalState()->BankHandle);
	}
}

void audWaveSlot::HandleWaveLoadCompletion()
{
	audWaveSlotState *state = GetInternalState();
	if(!state->Container.IsValid())
	{
		//The Bank header has been loaded, so now load the requested Wave sample data.

		audAssertf(!m_DecryptionTaskHandle, "Slot %s with in-flight decryption", GetSlotName());
		
		state->Container.SetHeader((adatContainerHeader*)state->BankData);

		if(!Verifyf(state->Container.GetPackingType() == adatContainer::NORMAL_PACKING, "Non-wave loaded assert attempting to play as wave loaded.  Aborting load. %s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash))
		{
			CleanUpAfterLoadFailure();
			return;
		}

		audAssertf(state->MaxHeaderSize >= state->Container.GetHeaderSize(), "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
		audAssertf(state->Container.IsValid(), "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
		audAssertf(state->Container.IsNativeEndian(), "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);

		state->ContainerHeaderSize = state->Container.GetHeaderSize();

		if(state->ContainerHeaderSize > state->MaxHeaderSize)
		{
			audErrorf("Bank header too large for slot: Wave %u in Bank %s Slot: %s", m_LoadedWaveNameHash,
				(GetBankName(m_LoadedBankId) ? GetBankName(m_LoadedBankId) : "Invalid"), state->SlotName);
			CleanUpAfterLoadFailure();
			return;
		}

		adatObjectId waveId = state->Container.FindObject(m_LoadedWaveNameHash);

		if(waveId != adatContainer::InvalidId)
		{
			// issue the data load request for all of this wave's chunks
			const u32 numWaveChunks = state->Container.GetNumDataChunksForObject(waveId);
			audAssertf(numWaveChunks > 0, "Wave with no data: %s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);

			// all chunks are stored next to each other so calculate the total size and starting offset
			// Work out metadata/data split so that we can optionally load data into VRAM
			ASSERT_ONLY(u32 sumOfChunkDataSize = 0);
			u32 dataOffset = ~0U, minMetadataOffset = ~0U;
			u32 dataSize = 0, maxMetadataOffset = 0;
			u32 sizeOfChunkAtMaxOffset = 0;
			for(u32 i = 0; i < numWaveChunks; i++)
			{
				u32 chunkHash,offsetBytes,sizeBytes;
				state->Container.GetObjectDataChunk(waveId, (adatObjectDataChunkId)i, chunkHash, offsetBytes, sizeBytes);

				const u32 dataChunkNameHash = ATSTRINGHASH("DATA", 1588979285u) & adatContainerObjectDataTableEntry::kDataTypeHashMask;
				if(chunkHash == dataChunkNameHash)
				{
					audAssertf(dataOffset == ~0U, "Wave with hash %s/%u has multiple DATA chunks", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
					dataOffset = offsetBytes;
					dataSize = sizeBytes;
				}
				else
				{
					ASSERT_ONLY(sumOfChunkDataSize += sizeBytes);
					audAssertf(offsetBytes != minMetadataOffset, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
					audAssertf(offsetBytes != maxMetadataOffset, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
					if(offsetBytes < minMetadataOffset)
					{
						minMetadataOffset = offsetBytes;
					}
					if(offsetBytes > maxMetadataOffset)
					{
						maxMetadataOffset = offsetBytes;
						sizeOfChunkAtMaxOffset = sizeBytes;
					}
				}
			}
			
			if(!audVerifyf(dataOffset != ~0U,"no DATA chunk in any of the %d wave chunks? %s/%u", numWaveChunks, GetBankName(m_LoadedBankId), m_LoadedWaveNameHash))
			{
				CleanUpAfterLoadFailure();
				return;
			}
			if(state->BankDataVram)
			{
				const u32 totalMetadataBytesToLoad = (maxMetadataOffset+sizeOfChunkAtMaxOffset) - minMetadataOffset;

				// check that the chunks are in fact contiguous, allowing for alignment
#if RSG_AUDIO_X64
				audAssertf(sumOfChunkDataSize >= totalMetadataBytesToLoad-128 && sumOfChunkDataSize <= totalMetadataBytesToLoad+128, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
#else
				audAssertf(sumOfChunkDataSize >= totalMetadataBytesToLoad-16 && sumOfChunkDataSize <= totalMetadataBytesToLoad+16, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
#endif
				// must load into 2k aligned address
				const u32 alignedHeaderSize = (state->Container.GetHeaderSize() + 2047) & ~(2047);

				audAssertf(totalMetadataBytesToLoad + alignedHeaderSize <= state->SlotBytes, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
				audAssertf(dataOffset < minMetadataOffset, "Wave %s/%u metadata before data (%u before %u)", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash, minMetadataOffset, dataOffset);
				
				
				//Load data - wave data into VRAM, metadata into main
				datResourceChunk chunks[2];

				// DATA is followed by metadata
				chunks[0].DestAddr = state->BankDataVram;
				chunks[0].Size = dataSize;
				chunks[1].DestAddr = state->BankData + alignedHeaderSize;
				chunks[1].Size = totalMetadataBytesToLoad;

				if(!audVerifyf(dataSize <= state->VramAllocationSize, "Wave data too big for VRAM: %s/%u (%u vs %u)", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash, dataSize, state->VramAllocationSize))
				{
					CleanUpAfterLoadFailure();
					return;
				}

				PS3_ONLY(audAssert(gcm::IsLocalPtr(chunks[0].DestAddr)));
				PS3_ONLY(audAssert(!gcm::IsLocalPtr(chunks[1].DestAddr)));
		
				// we need to store the starting read offset so we can adjust the values that come back from the container
				state->HeaderPadding = (alignedHeaderSize-state->Container.GetHeaderSize());
				state->LoadedContainerOffset = dataOffset;
				state->VRAMLoadOffset = dataOffset;
				state->LoadedBytes = totalMetadataBytesToLoad + dataSize;
				audAssertf(chunks[0].Size <= state->VramAllocationSize, "%s: %s/%u data chunk too large (%" SIZETFMT "u vs %u)", GetSlotName(), GetBankName(m_LoadedBankId), m_LoadedWaveNameHash, chunks[0].Size, state->VramAllocationSize);
				state->VramDataSize = u32(chunks[0].Size);
				audAssertf(chunks[1].Size <= state->MainDataSize, "%s: %s/%u metadata chunks too large (%" SIZETFMT "u vs %u)", GetSlotName(), GetBankName(m_LoadedBankId), m_LoadedWaveNameHash, chunks[1].Size, state->MainDataSize);
				
#if __BANK 
				if(PARAM_waveSlotsSpew.Get())
					audDebugf1("[audWaveSlotsSpew] %.2f: %s wave loading %s/%u; %u bytes main, %u vram", audDriver::GetMixer()->GetMixerTimeS(), GetSlotName(), GetLoadedBankName(), m_LoadedWaveNameHash, (u32)chunks[0].Size, (u32)chunks[1].Size); 
#endif
				if (!pgStreamer::Read(state->BankHandle, chunks, 2, dataOffset, state->BankLoadSema, pgStreamer::UNCACHED | pgStreamer::UNLOCK))
				{
					audErrorf("Error loading Wave %u from Bank %s", m_LoadedWaveNameHash,
						(GetBankName(m_LoadedBankId) ? GetBankName(m_LoadedBankId) : "Invalid"));
					CleanUpAfterLoadFailure();
				}
			}
			else
			{
				// No VRAM; everything into MAIN as a single read
				const u32 minOffset = Min(minMetadataOffset, dataOffset);
				u32 maxOffset = maxMetadataOffset;
				if(maxMetadataOffset < dataOffset)
				{
					maxOffset = dataOffset;
					sizeOfChunkAtMaxOffset = dataSize;
					
				}
				const u32 totalBytesToLoad = (maxOffset+sizeOfChunkAtMaxOffset) - minOffset;

				// check that the chunks are in fact contiguous, allowing for alignment
				ASSERT_ONLY(sumOfChunkDataSize += dataSize);
#if RSG_AUDIO_X64
				audAssertf(sumOfChunkDataSize >= totalBytesToLoad-128 && sumOfChunkDataSize <= totalBytesToLoad+128, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
#else
				audAssertf(sumOfChunkDataSize >= totalBytesToLoad-16 && sumOfChunkDataSize <= totalBytesToLoad+16, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);
#endif
				// must load into 2k aligned address
				const u32 alignedHeaderSize = (state->Container.GetHeaderSize() + 2047) & ~(2047);

				audAssertf(totalBytesToLoad + alignedHeaderSize <= state->SlotBytes, "%s/%u", GetBankName(m_LoadedBankId), m_LoadedWaveNameHash);

				//Load wave data
				datResourceChunk chunk;

				chunk.DestAddr = state->BankData + alignedHeaderSize;
				chunk.Size = totalBytesToLoad;

				// we need to store the starting read offset so we can adjust the values that come back from the container		
				state->HeaderPadding = (alignedHeaderSize-state->Container.GetHeaderSize());
				state->LoadedContainerOffset = minOffset;
				state->LoadedBytes = totalBytesToLoad;

#if __BANK 
				if(PARAM_waveSlotsSpew.Get())
					audDebugf1("[audWaveSlotsSpew] %.2f: %s wave loading %s/%u; %u bytes main", audDriver::GetMixer()->GetMixerTimeS(), GetSlotName(), GetLoadedBankName(), m_LoadedWaveNameHash, (u32)chunk.Size);
#endif

				if (!pgStreamer::Read(state->BankHandle, &chunk, 1, minOffset, state->BankLoadSema, pgStreamer::UNCACHED | pgStreamer::UNLOCK))
				{
					audErrorf("Error loading Wave %u from Bank %s", m_LoadedWaveNameHash,
						(GetBankName(m_LoadedBankId) ? GetBankName(m_LoadedBankId) : "Invalid"));
					CleanUpAfterLoadFailure();
				}
			}
			
			PF_SET(AudioLockingStreamer, 0);
		}
		else
		{
			audErrorf("Failed to find Wave %u in Bank %s", m_LoadedWaveNameHash,
				(GetBankName(m_LoadedBankId) ? GetBankName(m_LoadedBankId) : "Invalid"));
			CleanUpAfterLoadFailure();
		}
	}
	else
	{
		if(!m_DecryptionTaskHandle && !m_HasDecrypted AUD_SUPPORT_UNENCRYPTED_ASSETS_ONLY(&& state->Container.IsDataEncrypted()))
		{
			adatObjectId waveId = state->Container.FindObject(m_LoadedWaveNameHash);

			if(audVerifyf(waveId != adatContainer::InvalidId, "Failed to find loaded wave name hash to decrypt: %u", m_LoadedWaveNameHash))
			{
				const u32 dataChunkNameHash = ATSTRINGHASH("DATA", 1588979285u) & adatContainerObjectDataTableEntry::kDataTypeHashMask;
				const adatObjectDataChunkId chunkId = state->Container.FindObjectData(waveId, dataChunkNameHash);
				if(audVerifyf(chunkId != adatContainer::InvalidId, "Failed to find encrypted data chunk for wave name hash %u", m_LoadedWaveNameHash))
				{
					u32 chunkHash,offsetBytes,sizeBytes;
					state->Container.GetObjectDataChunk(waveId, chunkId, chunkHash, offsetBytes, sizeBytes);
					void *data = const_cast<void*>(Resolve(offsetBytes, sizeBytes));

					//if(g_WaveSlotAsyncDecryption)
					{
						// need to decrypt loaded data in-place
						sysTaskParameters p;
						p.ReadOnlyCount = 1;
						p.ReadOnly[0].Data = const_cast<u32*>(GetKey(m_LoadedBankId));
						p.ReadOnly[0].Size = sizeof(u32) * 4;
						p.Input.Data = p.Output.Data = data;
						p.Input.Size = p.Output.Size = sizeBytes;
						m_HasDecrypted = true;
						m_DecryptionTaskHandle = sysTaskManager::Create(TASK_INTERFACE(bteatask), p, sm_iSchedulerIndex);
						audAssert(m_DecryptionTaskHandle);
						return;
					}
					//else
					//{
					//		btea((u32*)data, -int(sizeBytes>>2), GetKey());
					//}
				}
			}
		}
		else if(m_DecryptionTaskHandle != NULL)
		{
			if(!sysTaskManager::Poll(m_DecryptionTaskHandle))
			{
				return;
			}
			else
			{
				m_DecryptionTaskHandle = NULL;
			}
		}
		//The Bank header and Wave sample data have been loaded.
		if(m_DecryptionTaskHandle == NULL)
		{
			RegisterLoadedWave();
			m_IsLoading = false;
		}
	}
}

void audWaveSlot::CleanUpAfterLoadFailure(void)
{
	audAssertf(!m_DecryptionTaskHandle, "Cleaning up waveslot with in-flight decryption job: %s (%s)", GetSlotName(), GetLoadedBankName());
	
	//Invalidate existing slot contents.
	sysMemSet(GetInternalState()->BankData, 0, GetInternalState()->SlotBytes);
	GetInternalState()->Container.SetHeader(NULL);

	m_LoadFailedBankId = m_LoadedBankId;
	m_LoadFailedWaveNameHash = m_LoadedWaveNameHash;
	m_LoadedBankId = AUD_INVALID_BANK_ID;
	m_LoadedWaveNameHash = 0;
	m_BankIdToLoad = AUD_INVALID_BANK_ID;
	m_WaveNameHashToLoad = 0;
	BANK_ONLY(CacheLoadedBankName();)
	BANK_ONLY(CacheLoadingBankName();)
	m_IsLoading = false;
	m_IsLoadRequestQueued = false;
	m_LoadPriority = 0;
	m_RequestedLoadPriority = 0;

	PF_SET(AudioLockingStreamer,0);

	pgStreamer::Unlock();
}

void audWaveSlot::UpdateWaveSlot(audWaveSlot *slot)
{
	switch(slot->m_SlotType)
	{
	case kSlotTypeStream:
		((audStreamingWaveSlot*)slot)->Update();
		break;
	default:
		slot->BaseUpdate();
		break;
	}
}

#endif //!SPU

void audWaveSlot::RegisterLoadedBank()
{
	RemoveFromLoadedBankTable();
	RemoveFromLoadedWaveTable();

	// add this slot to the static bank table
	sm_LoadedWaveTable.Delete(m_LoadedBankId);
	sm_LoadedBankTable.Insert(m_LoadedBankId, this);

#if __BANK 
	if(PARAM_waveSlotsSpew.Get())
	{
		audWaveSlot *loadedWaveSlot = FindLoadedBankWaveSlot(m_LoadedBankId);
		if(loadedWaveSlot)
		{
			audDebugf1("[audWaveSlotSpew] Registered loaded bank %s (%u) in slot %s", GetBankName(m_LoadedBankId), m_LoadedBankId,loadedWaveSlot->GetSlotName());
		}
	}
#endif
}

void audWaveSlot::RegisterLoadedWave()
{
	RemoveFromLoadedBankTable();
	RemoveFromLoadedWaveTable();

	// add this slot to the static wave table
	const u32 bankWaveIdentifier = m_LoadedWaveNameHash + m_LoadedBankId;
	sm_LoadedWaveTable.Delete(bankWaveIdentifier);
	sm_LoadedWaveTable.Insert(bankWaveIdentifier, this);
}

audWaveSlot *audWaveSlot::FindWaveSlot(const char *slotName)
{
	audWaveSlot **ptr = sm_WaveSlotTable.Access(atStringHash(slotName));
	return (ptr?*ptr:NULL);
}

audWaveSlot *audWaveSlot::FindWaveSlot(const u32 slotNameHash)
{
	audWaveSlot **ptr = sm_WaveSlotTable.Access(slotNameHash);
	return (ptr?*ptr:NULL);
}

audWaveSlot *audWaveSlot::FindLoadedBankWaveSlot(const u32 bankId) 
{
#if !__SPU || SPU_WAVESLOT_MAP
	SYS_CS_SYNC(sm_BankTableCriticalSectionToken);
	audWaveSlot **ptr = sm_LoadedBankTable.Access(static_cast<const u16>(bankId));
	return (ptr?*ptr:NULL);
#else
	for(u32 i = 0; i < sm_NumWaveSlots; i++)
	{
		audWaveSlot * slot = GetWaveSlotFromIndex(i);
		if(slot->m_LoadType == kLoadTypeBank &&
		   slot->m_LoadedBankId == bankId)
		{
			return slot;
		}
	}
	return NULL;
#endif //!__SPU || SPU_WAVESLOT_MAP
}

#if __ASSERT
bool audWaveSlot::VerifyLoadedBankWaveSlot(const audWaveSlot* waveSlot, const u32 bankId)
{
	SYS_CS_SYNC(sm_BankTableCriticalSectionToken);

	// Multiple slots may be associated with the same bank
	atMap<u16, audWaveSlot*>::Iterator iter = sm_LoadedBankTable.CreateIterator();
	while(!iter.AtEnd())
	{
		if(iter.GetKey() == bankId && iter.GetData() == waveSlot)
		{
			return true;
		}
		iter.Next();
	}
	return false;
}
#endif

audWaveSlot *audWaveSlot::FindLoadedWaveAssetWaveSlot(const u32 waveNameHash, const u32 bankId) 
{
#if !SPU_WAVESLOT_MAP
#if !__SPU
	const u32 bankWaveIdentifier = waveNameHash + bankId;
	audWaveSlot **ptr = sm_LoadedWaveTable.Access(bankWaveIdentifier);
	return (ptr?*ptr:NULL);
#else
	for(u32 i = 0; i < sm_NumWaveSlots; i++)
	{
		audWaveSlot * slot = GetWaveSlotFromIndex(i);
		if(slot->m_LoadType == kLoadTypeWave &&
		   slot->m_LoadedBankId == bankId &&
		   slot->m_LoadedWaveNameHash == waveNameHash)
		{
			return slot;
		}
	}
	return NULL;
#endif //!__SPU
#else //SPU_WAVESLOT_MAP
	const u32 bankWaveIdentifier = waveNameHash + bankId;
	audWaveSlot **ptr = sm_LoadedWaveTable.Access(bankWaveIdentifier);
	return (ptr?*ptr:NULL);
#endif
}

#if !__SPU
audWaveSlot::audWaveSlot(u8 *bankMem, const u32 numBytes, const u32 maxHeaderSize, const char *slotName, const bool isVirtual)
{
	m_InternalState = rage_aligned_new(16) audWaveSlotState();

	GetInternalState()->SlotBytes = numBytes;
	GetInternalState()->MaxHeaderSize = maxHeaderSize;
	m_IsLoadRequestQueued = false;
	m_LoadRequestTimeMs = (u32)-1;
	m_IsLoading = false;
	m_WaveNameHashToLoad = 0;
	m_LoadedBankId = AUD_INVALID_BANK_ID;
	m_BankIdToLoad = AUD_INVALID_BANK_ID;
	BANK_ONLY(CacheLoadedBankName();)
	BANK_ONLY(CacheLoadingBankName();)
	m_LoadedWaveNameHash = 0;
	m_LoadFailedBankId = AUD_INVALID_BANK_ID;
	m_LoadFailedWaveNameHash = 0;
	m_HasChanged = false;
	m_BatchMode = false;
	GetInternalState()->SlotName = StringDuplicate(slotName);
	m_IsStatic = false;
	
	m_RequestedLoadPriority = m_LoadPriority = 0;

	GetInternalState()->BankData = isVirtual ? nullptr : bankMem;
	GetInternalState()->BankDataVram = NULL;
	GetInternalState()->MainDataSize = numBytes;
	GetInternalState()->VramDataSize = 0;
	GetInternalState()->VramAllocationSize = 0;
	GetInternalState()->IsVirtual = isVirtual;

	Assign(m_SlotIndex, GetWaveSlotIndex(this));
	sm_ReferenceCounts[m_SlotIndex] = 0;

	GetInternalState()->BankLoadSema = sysIpcCreateSema(false);
	
	//Invalidate slot contents.
	if(m_InternalState->BankData)
	{
		sysMemSet(GetInternalState()->BankData, 0, GetInternalState()->SlotBytes);
	}

	GetInternalState()->Container.SetHeader(NULL);
	GetInternalState()->LoadedContainerOffset = GetInternalState()->HeaderPadding = 0;

	m_IsStreaming = false;

	m_SlotType = kSlotTypeWave;

	m_DecryptionTaskHandle = NULL;

#if RSG_BANK
	m_RequestDelayTimer = 0;
	m_RequestDelayTimerSet = false;
	m_ExpectedSlotSize = GetExpectedWaveslotSize(slotName);
#endif
}

audWaveSlot::~audWaveSlot(void)
{
	if (GetInternalState()->SlotName)
	{
		StringFree(GetInternalState()->SlotName);
		GetInternalState()->SlotName = NULL;
	}

	if (GetInternalState()->BankLoadSema)
	{
		sysIpcDeleteSema(GetInternalState()->BankLoadSema);
		GetInternalState()->BankLoadSema = NULL;
	}

	if (GetInternalState()->BankDataVram)
	{
		physical_delete(GetInternalState()->BankDataVram);
		GetInternalState()->BankDataVram = NULL;
	}

	delete m_InternalState;
	m_InternalState = NULL;
}

u32 audWaveSlot::GetNumPendingRequests(const u32 priorityLevel)
{
	if(audVerifyf(priorityLevel < kNumWaveLoadPrioLevels, "Invalid priority level: %u", priorityLevel))
	{
		u32 numRequests = 0;
		
		if(sm_IsBatchWaveLoadRequested)
		{
			// Batch load requests are processed before everything else, so include them regardless of priority level
			numRequests += sm_NumWaveLoadRequestsInBatch;
		}
		// Sum up to and including the requested level, since older requests at the same priority level will
		// be processed before new requests.
		for(u32 i = 0; i <= priorityLevel; i++)
		{
			numRequests += sm_RequestQueuePrioCounts[i];
		}
		return numRequests;
	}
	return GetNumPendingRequests();
} 

u32 audWaveSlot::GetNumPendingRequests()
{ 
	u32 numRequests = sm_RequestQueueCount;
	if(sm_IsBatchWaveLoadRequested)
	{
		numRequests += sm_NumWaveLoadRequestsInBatch;
	}
	return numRequests;
}
#endif

bool audWaveSlot::LoadAsset(const u32 bankId, const u32 waveNameHash, const u32 priority)
{
	switch (m_LoadType)
	{
	case kLoadTypeWave:
		LoadWave(bankId, waveNameHash, priority);
		break;
	case kLoadTypeBank:
		LoadBank(bankId, priority);
		break;
	default:
		audAssert(0);
		return false;
	}
	return true;
}

bool audWaveSlot::LoadBank(const u32 bankId, const u32 priority)
{
	if(bankId < AUD_INVALID_BANK_ID)
	{
		//**Start of Bank Load Critical Section**//
#if !__SPU
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

		if(GetReferenceCount() > 0)
		{
			//audWarningf("Requested load bank while %u clients were still playing from the slot\n",
			//	GetReferenceCount());
			return false;
		}

		SetHasChanged();
		
		Assign(m_BankIdToLoad, bankId == ~0U ? 0xFFFF : bankId);
		BANK_ONLY(CacheLoadingBankName();)

		m_WaveNameHashToLoad = 0;
		Assign(m_RequestedLoadPriority, priority);
		m_IsLoadRequestQueued = true;

#if !__SPU
		m_LoadRequestTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();		
#endif

		audDisplayf("LoadBank request for %s (%u) at priority %u", GetBankName(m_BankIdToLoad), bankId, priority);
	}
	else
	{
//		DEV_ONLY( DisplayAllTopParentSoundsReferenced(); )
		audErrorf("[audWaveSlot] Invalid bank id: %u", bankId);
		audAssertf(bankId < AUD_INVALID_BANK_ID, "[audWaveSlot] Invalid bank id: %u", bankId);
		return false;
	}

	return true;
}

void audWaveSlot::LoadWave(const u32 bankId, const u32 waveNameHash, const u32 priority)
{
	audAssert(bankId < AUD_INVALID_BANK_ID);
	if(bankId < AUD_INVALID_BANK_ID)
	{
#if !__SPU
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

		if(GetReferenceCount() > 0)
		{
			//audWarningf("Requested load wave while %u clients were still playing from the slot\n",
			//	GetReferenceCount());
			return;
		}

		SetHasChanged();

		Assign(m_BankIdToLoad, bankId);
		m_WaveNameHashToLoad = waveNameHash;
		Assign(m_RequestedLoadPriority, priority);
		m_IsLoadRequestQueued = true;
		BANK_ONLY(CacheLoadingBankName();)

#if !__SPU
		m_LoadRequestTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();
#endif
	}
}

bool audWaveSlot::ForceUnloadBank()
{
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

	if (GetReferenceCount() == 0 && !GetIsLoading())
	{
		if(GetInternalState()->BankData)
		{
			sysMemSet(GetInternalState()->BankData, 0, GetInternalState()->SlotBytes);
		}

		GetInternalState()->Container.SetHeader(NULL);
		RemoveFromLoadedBankTable();

		m_LoadFailedBankId = AUD_INVALID_BANK_ID;
		m_LoadedBankId = AUD_INVALID_BANK_ID;
		m_BankIdToLoad = AUD_INVALID_BANK_ID;

		m_LoadFailedWaveNameHash = 0;
		m_LoadedWaveNameHash = 0;
		m_WaveNameHashToLoad = 0;

		m_IsLoading = false;
		m_IsLoadRequestQueued = false;
		m_LoadPriority = 0;
		m_RequestedLoadPriority = 0;

		return true;
	}
	
	return false;
}

bool audWaveSlot::ForceSetBankMemory(u8* bankMemory, u32 size)
{
	sysCriticalSection lock(sm_UpdateCriticalSection);

	if (GetReferenceCount() == 0 && !GetIsLoading())
	{
		GetInternalState()->BankData = bankMemory;
		GetInternalState()->MainDataSize = size;
		return true;
	}

	return false;
}

audWaveSlot::audWaveSlotLoadStatus audWaveSlot::GetBankLoadingStatus(const u32 bankId) const
{
	audWaveSlotLoadStatus status = NOT_REQUESTED;

#if !__SPU
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

	if(!m_IsLoadRequestQueued && !m_IsLoading && (bankId == m_LoadedBankId))
	{
		status = LOADED;
	}
	else if((bankId == m_LoadedBankId && m_BankIdToLoad == AUD_INVALID_BANK_ID) || (bankId == m_BankIdToLoad))
	{
		status = LOADING;
	}
	else if(bankId == m_LoadFailedBankId)
	{
		status = FAILED;
	}
	return status;
}

audWaveSlot::audWaveSlotLoadStatus audWaveSlot::GetAssetLoadingStatus(const u32 bankId, const char *waveName) const
{
	const u32 waveNameHash = atStringHash(waveName);
	return GetAssetLoadingStatus(bankId, waveNameHash);
}

audWaveSlot::audWaveSlotLoadStatus audWaveSlot::GetAssetLoadingStatus(const u32 bankId, const u32 waveNameHash) const
{
	switch (m_LoadType)
	{
	case kLoadTypeWave:
		return GetWaveLoadingStatus(bankId, waveNameHash);
		// break;
	default:
		return GetBankLoadingStatus(bankId);
		// break;
	}
}

audWaveSlot::audWaveSlotLoadStatus audWaveSlot::GetWaveLoadingStatus(const u32 bankId, const char *waveName) const
{
	const u32 waveNameHash = atStringHash(waveName);
	return GetWaveLoadingStatus(bankId, waveNameHash);
}

audWaveSlot::audWaveSlotLoadStatus audWaveSlot::GetWaveLoadingStatus(const u32 bankId, const u32 waveNameHash) const
{
	audWaveSlotLoadStatus status = NOT_REQUESTED;

#if !__SPU
	SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);
#endif

	if(!m_IsLoadRequestQueued && !m_IsLoading && (bankId == m_LoadedBankId) && (waveNameHash == m_LoadedWaveNameHash))
	{
		status = LOADED;
	}
	else if(((bankId == m_LoadedBankId && m_BankIdToLoad == AUD_INVALID_BANK_ID) && (waveNameHash == m_LoadedWaveNameHash && m_WaveNameHashToLoad == 0)) ||
		((bankId == m_BankIdToLoad) && (waveNameHash == m_WaveNameHashToLoad)))
	{
		status = LOADING;
	}
	else if((bankId == m_LoadFailedBankId) && (waveNameHash == m_LoadFailedWaveNameHash))
	{
		status = FAILED;
	}

	return status;
}
#if !__SPU

void audWaveSlot::_SyncWaveSlotRequest(u16 bankId, u32 waveNameHash, u32 priority)
{
	audAssert(bankId != AUD_INVALID_BANK_ID);
	if(bankId != AUD_INVALID_BANK_ID)
	{
		SYS_CS_SYNC(sm_BankLoadCriticalSectionToken);

		m_BankIdToLoad = bankId;
		m_WaveNameHashToLoad = waveNameHash;
		Assign(m_RequestedLoadPriority, priority);
		m_IsLoadRequestQueued = true;
		m_LoadRequestTimeMs = audEngineUtil::GetCurrentTimeInMilliseconds();
		BANK_ONLY(CacheLoadingBankName();)
	}
}

void audWaveSlot::AddReference()
{
	audWaveSlot::AddSlotReference(m_SlotIndex);	
}

void audWaveSlot::RemoveReference()
{
	audWaveSlot::RemoveSlotReference(m_SlotIndex);
}

#endif // !__SPU

u32 audWaveSlot::GetReferenceCount() const
{
	return audWaveSlot::GetSlotReferenceCount(m_SlotIndex);
}

u32 audWaveSlot::GetSlotReferenceCount(const u32 slotIndex)
{
	audAssert(sm_ReferenceCounts);
	return sysInterlockedRead(&sm_ReferenceCounts[slotIndex]);
}

u32 audWaveSlot::RemoveSlotReference(const u32 slotIndex)
{	
	audAssert(sm_ReferenceCounts != NULL);
	audAssert(GetSlotReferenceCount(slotIndex) > 0);

	u32 ret = sysInterlockedDecrement(&sm_ReferenceCounts[slotIndex]);
	audAssert(ret != ~0U);
	return ret;
}

u32 audWaveSlot::AddSlotReference(const u32 slotIndex)
{
	audAssert(sm_ReferenceCounts != NULL);
	return sysInterlockedIncrement(&sm_ReferenceCounts[slotIndex]);
}

#if !__SPU
const char *audWaveSlot::GetLoadingBankName() const
{
	if(m_BankIdToLoad < AUD_INVALID_BANK_ID)
	{
		return audWaveSlot::GetBankName(m_BankIdToLoad);
	}
	return "(invalid bank)";
}

const char *audWaveSlot::GetLoadedBankName(void) const
{
	if(m_LoadedBankId != AUD_INVALID_BANK_ID)
	{
		return GetBankName(m_LoadedBankId);
	}
	else
	{
		return "none";
	}
}

#if __BANK
void audWaveSlot::CacheLoadedBankName()
{
	if(m_LoadedBankId != AUD_INVALID_BANK_ID)
	{
		formatf(m_CachedLoadedBankName, "%s (ID: %d)", GetBankName(m_LoadedBankId), m_LoadedBankId);
	}
	else
	{
		formatf(m_CachedLoadedBankName, "none");
	}
}

void audWaveSlot::CacheLoadingBankName()
{
	if(m_BankIdToLoad < AUD_INVALID_BANK_ID)
	{
		formatf(m_CachedLoadingBankName, "%s (ID: %d)", GetBankName(m_BankIdToLoad), m_BankIdToLoad);
	}
	else
	{
		formatf(m_CachedLoadingBankName, "none");
	}	
}
#endif

#endif // __SPU

const void *audWaveSlot::Resolve(const u32 offset, const u32 size) const
{
	const audWaveSlotState *state = GetInternalState();
#if __XENON || RSG_DURANGO
	(void)size;
	audAssert(state->Container.IsValid());
	audAssert(offset >= state->ContainerHeaderSize);

	audAssert(offset >= state->LoadedContainerOffset);
	audAssert(size <= state->LoadedBytes);

	const u32 offsetRelative = offset - state->LoadedContainerOffset;

	if (state->ContainerHeaderSize + offsetRelative + state->HeaderPadding <= state->MainDataSize)
		return state->BankData + state->ContainerHeaderSize + offsetRelative + state->HeaderPadding;
	else
	{
		audAssert(state->BankDataVram);
		audAssert(state->ContainerHeaderSize + offsetRelative + state->HeaderPadding - state->MainDataSize < state->VramDataSize);
		return state->BankDataVram + state->ContainerHeaderSize + offsetRelative + state->HeaderPadding - state->MainDataSize;
	}
#else
	audAssert(GetContainer().IsValid());
	audAssert(offset >= state->ContainerHeaderSize);

	audAssert(offset >= state->LoadedContainerOffset);
	audAssert(size <= state->LoadedBytes);

	const u32 offsetRelative = offset - state->LoadedContainerOffset;

	if(state->VRAMLoadOffset == state->LoadedContainerOffset)
	{
		// VRAM data is before MAIN data (ie waveloaded assets where wave data is placed before metadata)
		if(offsetRelative + size <= state->VramDataSize)
		{
			audAssert(state->BankDataVram);
			audAssert(offset >= state->VRAMLoadOffset);
			audAssert(offset - state->VRAMLoadOffset < state->VramDataSize);
			return state->BankDataVram + offsetRelative;
		}
		else
		{
			const u32 mainLoadedOffset = state->LoadedContainerOffset + state->VramDataSize;
			audAssert(offset >= mainLoadedOffset);
			const u32 mainOffsetRelative = offset - mainLoadedOffset;
			return state->BankData + state->ContainerHeaderSize + mainOffsetRelative + state->HeaderPadding;
		}
	}
	else
	{	
		if (state->ContainerHeaderSize + offsetRelative + state->HeaderPadding + size <= state->MainDataSize)
		{
			return state->BankData + state->ContainerHeaderSize + offsetRelative + state->HeaderPadding;
		}
		else
		{
			audAssert(state->BankDataVram);
			audAssert(offset >= state->VRAMLoadOffset);
			audAssert(offset - state->VRAMLoadOffset < state->VramDataSize);

			return state->BankDataVram + offset - state->VRAMLoadOffset;
		}
	}
#endif
}

#if __SPU

void audWaveSlot::FetchInternalState() const
{
	if(sm_InternalStateCacheSlotId != m_SlotIndex)
	{
		sysDmaGetAndWait(&sm_InternalStateCache, (uint64_t)m_InternalState, sizeof(audWaveSlotState), 6);
		sm_InternalStateCacheSlotId = m_SlotIndex;
	}
}

bool audWaveSlot::FetchContainerHeader() const
{
	if(sm_CachedContainerSlotId != m_SlotIndex)
	{
		FetchInternalState();
		const audWaveSlotState *state = GetInternalState();

		if(!sm_CachedContainer.Init(state->Container.GetDataPointer()))
		{
			audErrorf("Failed to init SPU container for slot %u", m_SlotIndex);
			return false;
		}
	}
	return true;
}

#endif

const u32 *audWaveSlot::GetKey(u16 bankId)
{	
	const u32* overiddenKey = audWaveSlot::GetEncryptionKeyOverrideCallback ? audWaveSlot::GetEncryptionKeyOverrideCallback(bankId) : NULL;

	if (overiddenKey)
	{ 
		audDisplayf("Using overriden encryption key for bank %s", GetBankName(bankId));
		return overiddenKey;
	}
	else
	{
#if RSG_ORBIS
		static const u32 key[] = { TEA_STIRRER ^ 0xF34B1262,TEA_STIRRER ^ 0xCFB0FE66,TEA_STIRRER ^ 0xEC500648,TEA_STIRRER ^ 0xB90ACE74 };
		return &key[0];
#elif RSG_DURANGO
		static const u32 key[] = { TEA_STIRRER ^ 0xA40C8A12,TEA_STIRRER ^ 0x979B3DFB,TEA_STIRRER ^ 0x1CB6F0B2,TEA_STIRRER ^ 0xF7DC4078 };
		return &key[0];
#else	
		static const u32 key[] = { TEA_STIRRER ^ 0x8379F365,TEA_STIRRER ^ 0xF1D3E378,TEA_STIRRER ^ 0x1CAF095B,TEA_STIRRER ^ 0x89330E8B };
		return &key[0];
#endif
	}
}

}	// namespace rage
