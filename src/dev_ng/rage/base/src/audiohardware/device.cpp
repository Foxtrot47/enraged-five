//
// audiohardware/device.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "cmdbuffer.h"
#include "config.h"
#include "connection.h"
#include "decoder_spu.h"
#include "device.h"
#include "driverdefs.h"
#include "driver.h"
#include "framebufferpool.h"
#include "midi.h"
#include "mixer.h"
#include "src_linear.h"
#include "pcmsource_interface.h"
#include "waveplayer.h" // for AUD_WAVEPLAYER_METERING
#include "submix.h"
#include "voicemgr.h"

#if !__FINAL && __PS3// for test code
#include "waveslot.h"
#include "string/stringhash.h"
#include "device_ps3.h"
#endif

#include "atl/atfunctor.h"
#include "audiosynth/synthcore.h"
#include "audiosynth/synthdefs.h"
#include "audiosynth/synthutil.h"
#include "file/remote.h" // fiIsShowingMessageBox
#include "grcore/debugdraw.h"
#include "math/amath.h"
#include "profile/profiler.h"
#include "system/endian.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/performancetimer.h"
#include "system/task.h"
#include "audiohardware/decoder_adpcm.h"
#include "audiohardware/decoder_ps4mp3.h"
#include "audioengine/engine.h"

#include "audiohardware/voicemgr.h"
#include "mixing_vmath.inl"
#include "voicefilter_vmath.inl"

#include "system/wndproc.h"
#include "system/xtl.h"

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(x) if(x){delete[] x; x=NULL;}
#endif

#include "submixjob.h"
RELOAD_TASK_DECLARE(SubmixJob);

#include "voicemixjob.h"
RELOAD_TASK_DECLARE(VoicemixJob);

#include "waveplayerjob.h"
RELOAD_TASK_DECLARE(WavePlayerJob);
#include "audiosynth/synthcorejob.h"
#include "audiosynth/synthesizer.h"
RELOAD_TASK_DECLARE(synthcorejob);
#include "audiohardware/grainplayerjob.h"
RELOAD_TASK_DECLARE(grainplayerjob);

PF_PAGE(MixerDevicePage, "RAGEAudio: Mixer Device");
PF_GROUP(MixerDevice);
PF_LINK(MixerDevicePage, MixerDevice);
PF_GROUP(ConnectionPool);
PF_LINK(MixerDevicePage, ConnectionPool);

PF_VALUE_INT(NumConnectionsAllocated,ConnectionPool);
PF_VALUE_INT(NumVolumesAllocated,ConnectionPool);
PF_VALUE_INT(NumBuffersAllocated,ConnectionPool);
PF_VALUE_INT(NumBuffersFree,ConnectionPool);
PF_VALUE_INT(NumPCMSources, ConnectionPool);

PF_TIMER(GeneratePcm, MixerDevice);

#if __PS3
PF_VALUE_FLOAT(SubmixJob, MixerDevice);
PF_VALUE_FLOAT(VoiceMixJob, MixerDevice);
PF_VALUE_FLOAT(WavePlayerJob, MixerDevice);
PF_VALUE_FLOAT(GrainPlayerJob, MixerDevice);
PF_VALUE_FLOAT(SynthCoreJob, MixerDevice);
#else
PF_VALUE_FLOAT(ComputeFilterCoefficients, MixerDevice);
PF_VALUE_FLOAT(VoiceMixing, MixerDevice);
PF_VALUE_FLOAT(Submixing, MixerDevice);
PF_VALUE_FLOAT(HardClipping, MixerDevice);
#endif // !__PS3

PF_VALUE_FLOAT(Downmix, MixerDevice);
PF_VALUE_FLOAT(AudioCapture, MixerDevice);

PF_VALUE_FLOAT(MixBuffers, MixerDevice);
PF_VALUE_FLOAT(ExecuteCommandBuffers, MixerDevice);
PF_VALUE_FLOAT(ComputeProcessingGraph,MixerDevice);
PF_VALUE_FLOAT(VoicePremix, MixerDevice);
PF_VALUE_FLOAT(PCMGeneration, MixerDevice);
PF_VALUE_FLOAT(WavePlayerPT, MixerDevice);
PF_TIMER(Device_MixBuffers, MixerDevice);

PF_PAGE(HeadroomError, "RAGEAudio: Headroom error");
PF_GROUP(HeadroomError);
PF_LINK(HeadroomError, HeadroomError);
PF_VALUE_FLOAT(HeadroomErrMax, HeadroomError);
PF_VALUE_FLOAT(HeadroomErrAvg, HeadroomError);

PF_PAGE(Loudness, "RAGEAudio: Loudness meter");
PF_GROUP(Loudness);
PF_LINK(Loudness, Loudness);
PF_VALUE_FLOAT(LoudnessLUFS, Loudness);

#define PF_MIXTIMER_START(x) sysPerformanceTimer timer##x(""); timer##x.Start();
#define PF_MIXTIMER_STOP(x) timer##x.Stop(); PF_SET(x, timer##x.GetTimeMS());

PF_PAGE(MixerAllocationStatsPage, "RAGEAudio: Mixer stats");
PF_GROUP(MixerAllocationStats);
PF_LINK(MixerAllocationStatsPage, MixerAllocationStats);

PF_VALUE_INT(NumSyncObjects, MixerAllocationStats);
PF_VALUE_INT(NumPcmDeleted, MixerAllocationStats);
PF_VALUE_INT(NumPcmPhys, MixerAllocationStats);
PF_VALUE_INT(GameThreadCB_HWM, MixerAllocationStats);
PF_VALUE_INT(AudioThreadCB_HWM, MixerAllocationStats);
PF_VALUE_INT(SoundBucketCB_HWM, MixerAllocationStats);
PF_VALUE_INT(CommandBufferLatency, MixerAllocationStats);
PF_VALUE_INT(NumPCMSourcesPhys, MixerAllocationStats);
PF_VALUE_INT(NumVoicesActive, MixerAllocationStats);

PARAM(noaudiohardware,"RAGE Audio - Don't initialise any audio hardware for output");

#if !__PS3
PARAM(writemixeroutput, "RAGE Audio - Write raw mixer output to specified file");
#endif

PARAM(audiodesigner, "Default audio designer command line; RAVE etc");

namespace rage
{
#if __BANK && RSG_PC
	extern bool g_ShowAudioFrameSkipDebugInfo;
#endif

	audFrameBufferPool *g_FrameBufferPool = NULL;
	extern audMixerConnectionPool *g_MixerConnectionPool;
	audPcmSourcePool *g_PcmSourcePool = NULL;

	BANK_ONLY(extern bool g_MeterSubmixes);
	BANK_ONLY(extern bool g_DisableSampleRateConversion);
	BANK_ONLY(extern u32 g_DisablePCMSourceMask);
	DEV_ONLY(extern bool g_TraceCommandPackets);
	BANK_ONLY(extern float g_LoudnessMeterWindow);

	__THREAD s32 g_MixThreadClientId = -1;

	bool g_DebugSubmixJob = true;
	u32 g_SubmixJobCacheBuffers = 38;

	PS3_ONLY(enum {schedulerId = sysTaskManager::SCHEDULER_SECONDARY});

	void audMixerDevice::MixBuffers()
	{
		PF_FUNC(Device_MixBuffers);
		PF_MIXTIMER_START(MixBuffers);
		USE_MEMBUCKET(MEMBUCKET_AUDIO);
		
		TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"audMixerDevice::MixBuffers()");

#if RSG_AUDIO_X64
		audDecoderAdpcm::sm_DecodesThisFrame = 0;
#endif

		EnterBML();

		// Act on any queued commands
		ExecuteCommandBuffers();

		const bool isPaused = m_Paused NOTFINAL_ONLY(|| fiIsShowingMessageBox);
		if(isPaused)
		{
#if RSG_PS3
			const u32 numOutputChannels = 8;
#else
			const u32 numOutputChannels = m_NumOutputChannels;
#endif		
			for(u32 i = 0; i < numOutputChannels; i++)
			{
				sysMemZeroBytes<sizeof(float) * kMixBufNumSamples>(&GetOutputBuffer()[i*kMixBufNumSamples]);
			}
			ExitBML();
			return;
		}
		
		if(m_RecomputeGraph)
		{
			PF_MIXTIMER_START(ComputeProcessingGraph);
			ComputeProcessingGraph();
			PF_MIXTIMER_STOP(ComputeProcessingGraph);
		}
				
		u32 numVoicesActive = 0;
		PF_MIXTIMER_START(VoicePremix);	
		for(u32 k = 0; k < m_NumVoices; k++)
		{
			if(m_Voices[k].GetState() == audMixerVoice::PLAYING)
			{
				m_Voices[k].Premix();
				numVoicesActive++;
			}
		}
		PF_MIXTIMER_STOP(VoicePremix);
	
		// TODO: move this to somewhere more generic
		SYNTH_EDITOR_ONLY(audDriver::GetMidiInput()->SwapQueueBuffer());
		
		GeneratePcm();
		
		// TODO: move this to somewhere more generic
		SYNTH_EDITOR_ONLY(audDriver::GetMidiInput()->EmptyQueue());
		
		PF_MIXTIMER_START(VoiceMixing);
		// apply filters/envelopes and mix voices into submixes			
		for(u32 k = 0; k < m_NumVoices; k++)
		{
			if(m_Voices[k].GetState() == audMixerVoice::PLAYING)
			{
				m_Voices[k].Mix();
			}
		}
		PF_MIXTIMER_STOP(VoiceMixing);
				
		PF_MIXTIMER_START(Submixing);
		// mix submixes
		s32 currentStage = 0;
		for(s32 id = 0; id < m_SubmixProcessingOrder.GetCount(); id++)
		{
			audMixerSubmix &submix = m_Submixes[m_SubmixProcessingOrder[id]];
			submix.Mix();
				
			if(submix.GetProcessingStage() != currentStage)
			{
				// Sanity check that we're processing in the correct order
				audAssert(submix.GetProcessingStage() > currentStage);
				m_SubmixCache.FreeBuffers(currentStage);
				currentStage = submix.GetProcessingStage();
			}				
		}
		m_SubmixCache.ClearAllAllocations();
		
		// call postmix on all submixes
		for(s32 id = 0; id < m_Submixes.GetCount(); id++)
		{
			m_SubmixVoiceCount[id] = m_Submixes[id].GetVoiceCount();
			m_Submixes[id].PostMix();
		}
		PF_MIXTIMER_STOP(Submixing);
			
#if !RSG_XENON && !RSG_DURANGO && !RSG_ORBIS
		// hard limit the final output - done during interleaving in our XAudio2 device
		// TODO: can skip this for PC too when we remove ASIO and DSound output options
		PF_MIXTIMER_START(HardClipping);
		if (GetOutputBuffer())
		{
			for(u32 i = 0; i < m_NumOutputChannels; i++)
			{
				HardClipBuffer<kMixBufNumSamples>(&GetOutputBuffer()[i*kMixBufNumSamples]);
			}
		}
		PF_MIXTIMER_STOP(HardClipping);
#endif // RSG_XENON

#if __BANK
		if(m_LoudnessMeter)
		{
			atRangeArray<const float *, 6> buffers;
			for(u32 i = 0; i < GetMasterSubmix()->GetNumChannels(); i++)
			{
				buffers[i] = GetMasterSubmix()->GetMixBuffer(i);
			}
			m_LoudnessMeter->SetWindowLength(g_LoudnessMeterWindow);
			m_LoudnessMeter->Process(buffers, GetMasterSubmix()->GetNumChannels(), kMixBufNumSamples);

			PF_SET(LoudnessLUFS, m_LoudnessMeter->GetLoudness());
		}
#endif // __BANK

#if !__PS3 && !RSG_ORBIS
		// optional final downmix stage
		if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
		{
			PF_MIXTIMER_START(Downmix);
			audAssert(m_NumOutputChannels == 6);
			DownmixOutputToStereo<kMixBufNumSamples>(GetOutputBuffer());
            m_FinalOutputDownmixed = true;
			PF_MIXTIMER_STOP(Downmix);
		}
        else
#endif
        {
            m_FinalOutputDownmixed = false;
        }

		BANK_ONLY(float physicalPcmSourceCost = 0.f);
		// Call post mix on all voices
		for(u32 i = 0; i < m_NumVoices; i++)
		{
			m_Voices[i].PostMix();

#if __BANK			
			if(m_Voices[i].HasPcmSource())
			{
				physicalPcmSourceCost += m_Voices[i].GetPcmSource()->GetProcessingTimer();
			}			
#endif
		}

		BANK_ONLY(m_PhysicalPcmSourceCost = physicalPcmSourceCost);

#if __BANK && 0
		u32 numPhysWavePlayers = 0;
		float sumWavePlayerGeneration = 0.f;
		const u32 numSlots = m_PcmSourcePool.GetNumSlots();

		for(u32 slotIndex = 0; slotIndex < numSlots; slotIndex++)
		{
			if(m_PcmSourcePool.IsSlotInitialised(slotIndex) && m_PcmSourcePool.GetSlotTypeId(slotIndex) == AUD_PCMSOURCE_WAVEPLAYER)
			{
				const audWavePlayer *wavePlayer = reinterpret_cast<const audWavePlayer*>(GetPcmSource(slotIndex));
				if(wavePlayer->HasBuffer(0))
				{
					numPhysWavePlayers++;
					sumWavePlayerGeneration += wavePlayer->GetProcessingTimer();
				}
			}
		}

		if(numPhysWavePlayers)
		{
			const float mean_uS = sumWavePlayerGeneration * 1000.f / static_cast<float>(numPhysWavePlayers);
			PF_SET(WavePlayerPT, mean_uS);
		}
#endif
		
		m_MixTimerFrames++;

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
		if(m_IsFixedFrameReplay)
		{
			if(m_IsCapturing)
			{
				UpdateAudioCapture();
			}
		}
		else
		{
			UpdateAudioCapture();
		}
#endif

		// Free any signals that fired this frame
		m_SyncManager.Update();

		audDecodeManager::PostMix();

		ExitBML();
		
		TELEMETRY_END_ZONE(__FILE__,__LINE__);

		PF_MIXTIMER_STOP(MixBuffers);

		PF_SET(NumBuffersAllocated, m_FrameBufferPool.GetNumBuffersAllocated());
		PF_SET(NumBuffersFree, m_FrameBufferPool.GetNumBuffersFree());
		PF_SET(NumConnectionsAllocated,g_MixerConnectionPool->GetNumConnectionsAllocated());
		PF_SET(NumVolumesAllocated,g_MixerConnectionPool->GetNumVolumesAllocated());
		PF_SET(NumPCMSources, g_PcmSourcePool->GetNumAllocated());
		PF_SET(NumSyncObjects, m_SyncManager.GetNumAllocated());
		PF_SET(NumVoicesActive, numVoicesActive);
		
		g_DebugSubmixJob = false;
	}

	void audMixerDevice::GeneratePcm()
	{
		PF_FUNC(GeneratePcm);
		PF_MIXTIMER_START(PCMGeneration);


//#if __BANK
//#define NEXT_LINE	grcDebugDraw::Text(Vector2(0.4f, lineBase), Color32(255,255,255), tempString ); lineBase += lineInc;
//
//		char tempString[64];
//		static bank_float lineInc = 0.015f;
//		f32 lineBase = 0.1f;
//#endif
		s32 count = 0, physicalCount = 0;
		const u32 numSlots = m_PcmSourcePool.GetNumSlots();

		struct syncSourceInfo
		{
			u32 slotId;
			u32 syncSourceId;
			bool waitingOnSelfSync;
		};
		atFixedArray<syncSourceInfo, 64> sourcesWaitingOnSync;
		for(u32 slotIndex = 0; slotIndex < numSlots; slotIndex++)
		{
			if(m_PcmSourcePool.IsSlotInitialised(slotIndex))
			{
				// Check for initialised slots with a refcount of 0 and delete them
				if(m_PcmSourcePool.GetRefCount(slotIndex) == 0)
				{
					FreePcmSource(slotIndex);
					count++;
				}
				else
				{
					PcmSourceState *state = audDriver::GetVoiceManager().GetPcmSourceState(slotIndex);
					if(audVerifyf(state, "PCM Source state invalid, slot index %d", slotIndex))
					{
						if(state->PlaytimeSamples != ~0U)
						{
							audPcmSource *pcmSource = (audPcmSource*)m_PcmSourcePool.GetSlotPtr(slotIndex);

							if(pcmSource->IsWaitingOnSync())
							{
								syncSourceInfo &info = sourcesWaitingOnSync.Append();
								info.slotId = slotIndex;
								info.syncSourceId = pcmSource->GetSyncSlaveId();
								info.waitingOnSelfSync = pcmSource->IsWaitingOnSelfSync();
							}
							if(!pcmSource->IsWaitingOnSync() || pcmSource->IsWaitingOnSelfSync())
							{
								// If the PCM generator has any buffers then a voice has requested physical playback
								if(pcmSource->HasBuffer(0))
								{
									physicalCount++;

									bool disableSourceType = false;								
#if __BANK							
									disableSourceType = (g_DisablePCMSourceMask & (1 << pcmSource->GetTypeId())) != 0;

									//if(!pcmSource->IsFinished() && pcmSource->IsStreaming())
									//{
									//	sprintf(tempString, "frame %d", pcmSource->GetFramesWaitingForStreaming());		NEXT_LINE;
									//}
#endif
									if(!disableSourceType)
									{
										pcmSource->GenerateFrame();
									}		
									else
									{
										for(s32 channel = 0; channel < audPcmSource::kMaxPcmSourceChannels; channel++)
										{
											if(pcmSource->HasBuffer(channel))
											{
												g_FrameBufferCache->ZeroBuffer(pcmSource->GetBufferId(channel));
											}
										}
									}
								}
								else
								{
									pcmSource->SkipFrame();
								}

								// update globally accessible state

								if(pcmSource->IsFinished())
								{
									state->PlaytimeSamples = ~0U;
								}
								else
								{
									state->PlaytimeSamples = pcmSource->GetPlayPositionSamples();
								}

								state->UpdateMixerFrame = audDriver::GetMixer()->GetMixerTimeFrames();
								state->hasStartedPlayback = pcmSource->HasStartedPlayback();
								Assign(state->CurrentPeakLevel, pcmSource->GetCurrentPeakLevel());
							}
						}
					}
				}
			}
		}

		for(s32 i = 0; i < sourcesWaitingOnSync.GetCount(); i++)
		{
			audMixerSyncSignal signal;
			audMixerSyncManager::Get()->ReadSignal(signal, sourcesWaitingOnSync[i].syncSourceId);
			const u32 slotIndex = sourcesWaitingOnSync[i].slotId;
			audPcmSource *pcmSource = (audPcmSource*)m_PcmSourcePool.GetSlotPtr(slotIndex);

			bool hasSignal = false;
			if(signal.HasTriggered() || signal.WasCanceled())
			{
				hasSignal = pcmSource->ProcessSyncSignal(signal);
			}
			
			// Self-sync sources should only get the second update if the signal was triggered in the first pass
			if(hasSignal || !sourcesWaitingOnSync[i].waitingOnSelfSync)
			{			
				// If the PCM generator has any buffers then a voice has requested physical playback
				if(pcmSource->HasBuffer(0))
				{
					physicalCount++;

					bool disableSourceType = false;							
	#if __BANK							
					disableSourceType = (g_DisablePCMSourceMask & (1 << pcmSource->GetTypeId())) != 0;
	#endif
					if(!disableSourceType)
					{
						pcmSource->GenerateFrame();
					}		
					else
					{
						for(s32 channel = 0; channel < audPcmSource::kMaxPcmSourceChannels; channel++)
						{
							if(pcmSource->HasBuffer(channel))
							{
								g_FrameBufferCache->ZeroBuffer(pcmSource->GetBufferId(channel));
							}
						}
					}
				}
				else
				{
					pcmSource->SkipFrame();
				}
		

				// update globally accessible state
				PcmSourceState *state = audDriver::GetVoiceManager().GetPcmSourceState(slotIndex);
				if(audVerifyf(state, "PCM Source state invalid, slot index %d", slotIndex))
				{
					pcmSource->UpdateState(*state);
				}
			}
		}

		PF_SET(NumPcmDeleted, count);
		PF_SET(NumPcmPhys, physicalCount);

		PF_MIXTIMER_STOP(PCMGeneration);
	}

audMixerDevice::audMixerDevice(void) :
m_MainOutputSubmix(NULL),
m_RestrictedOutputSubmix(NULL),
m_IsInitialised(false),
m_RecomputeGraph(true),
m_Paused(false),
m_mixCapturer( NULL ),
m_Suspended(false),
m_IsCapturing(false),
m_IsFixedFrameReplay(false),
m_IsFrameRendering(false),
m_TotalVideoTimeNs(0),
m_TotalAudioTimeNs(0),
m_ShouldStartFixedFrameRender(false),
m_ShouldStopFixedFrameRender(false)
{
	BANK_ONLY(m_LoudnessMeter = NULL);
	for(s32 i = 0; i < kMaxPauseGroups; i++)
	{
		m_PauseGroupState[i] = false;
	}
	m_StartWaitSemaphoreCount = 0;
	m_StartSignalSemaphoreCount = 0;
	m_EndWaitSemaphoreCount = 0;
	m_EndSignalSemaphoreCount = 0;

}

audMixerDevice::~audMixerDevice(void)
{

}

bool audMixerDevice::Init(const u32 numVoices)
{
	if(m_IsInitialised)
	{
		return true;
	}

	BANK_ONLY(m_PhysicalPcmSourceCost = 0.f);

	m_NumOutputChannels = 0;
    m_FinalOutputDownmixed = false;
	m_NumVoices = numVoices;
	
	// Shortcut for code that runs on both PPU and SPU
	g_SubmixCache = &m_SubmixCache;

	// Global pointer enables easy access to the frame buffer pool, esp. from SPU code
	g_FrameBufferPool = &m_FrameBufferPool;
	// The synth editor disables buffer sharing in order to present a more useful UI, so allocate plenty
	// extra buffers in that situation.
	const u32 numFrameBuffers = audConfig::GetValue(
		__SYNTH_EDITOR ? "driverSettings_NumFrameBuffers_AMP" 
		: "driverSettings_NumFrameBuffers", 340 /*m_NumVoices*4*/);

	m_FrameBufferPool.Init(numFrameBuffers);

	// TODO: revisit; currently output buffers must be numbered [0,numChannels] which means we must allocate them
	// first.
	// We need 8 channels on PS3 so that we can interleave in place.
	ReinitialiseMixBuffers(RSG_PS3 ? 8 : 6);
	
	if(!PARAM_noaudiohardware.Get())
	{
		if(!InitHardware())
		{
			audErrorf("Hardware initialisation failed");
			return false;
		}
	}

	if(!m_DecodeManager.Init())
	{
		audErrorf("Decode manager initialisation failed");
		return false;
	}

	// If this fails then change the number of bits used to represent objectId in audiohardware/cmdbuffer.h
	CompileTimeAssert(kMaxPcmSources <= 1<<audCommandPacketHeader::kNumBitsForObjectId);
	CompileTimeAssert(sizeof(audCommandPacketHeader) == 4);
	g_PcmSourcePool = &m_PcmSourcePool;
	
	m_PcmSourcePool.Init(audPcmSourceFactory::GetMaxSize(), kMaxPcmSources);
	
	/////////////////////////////////////////////////////////////////////////////////////

	m_NumOutputChannels = audDriver::GetNumOutputChannels();
	audAssert(m_NumOutputChannels != 0);
	//------------------------------------------------------------------
	// AFTER THIS POINT USE
	//	m_NumVoices not numVoices;
	// THEY MAY HAVE BEEN CHANGED IN InitHardware
	//------------------------------------------------------------------

	audDisplayf("RAGE Audio Mixer: %u voices, %u output channels (latency: %u samples)", m_NumVoices, m_NumOutputChannels, kMixBufNumSamples);

	m_Voices.SetCount(m_NumVoices);
	for(u32 i = 0 ; i < m_NumVoices; i++)
	{
		m_Voices[i].InitInternal();	
	}

	sysMemZeroBytes<sizeof(m_SubmixVoiceCount)>(&m_SubmixVoiceCount);
	
	audMixerConnection::InitClass();

	audMixerSubmix *submix = CreateSubmix("Master", m_NumOutputChannels, false);
	AddEndPoint(submix);

#if __BANK && !__PS3
	if(PARAM_writemixeroutput.Get())
	{
		const char *fileName = "c:\\mastersubmix";
		PARAM_writemixeroutput.Get(fileName);
		GetMasterSubmix()->SetDumpOutput(fileName);
	}
#endif
	
	
	for(u32 i = 0; i < m_NumOutputChannels; i++)
	{
		GetMasterSubmix()->SetBufferId(i,i);
	}
	// we want to use the master submix mix buffer as our main output buffer, so need it to persist
	GetMasterSubmix()->SetShouldKeepBuffers(true);

	m_SyncManager.Init();

	m_MixTimerFrames = 0;

	m_IsInitialised = true;

#if __BANK
	if(PARAM_audiodesigner.Get())
	{
		USE_DEBUG_MEMORY();
		m_LoudnessMeter = rage_new audSurroundLoudnessMeter();
	}
#endif

	if(!PARAM_noaudiohardware.Get())
	{
		StartMixing();
	}


	/*
	fiStream *mixerLog = ASSET.Open("c:\\mixerlog_pcm_assert", "dat");
	u8 buf[256];

	audCommandPacketHeader *header = (audCommandPacketHeader*)buf;

	g_TraceCommandPackets = true;
	const u32 fileSize = mixerLog->Size();
	u32 currentOffset = 0;
	while(currentOffset < fileSize)
	{
		mixerLog->Read(buf, sizeof(audCommandPacketHeader));
		mixerLog->Read(buf + 4, header->packetSize - 4);

		DebugTraceCommandPacket(header);

		currentOffset += header->packetSize;
	}

	mixerLog->Close();
	g_TraceCommandPackets = false;
	*/

	return true;
}

s32 audMixerDevice::AllocatePcmSource_Sync(const audPcmSourceTypeId typeId)
{
	u32 allocationId = m_PcmSourcePool.Allocate(audPcmSourceFactory::GetSlotSize(typeId));
	if(allocationId == ~0U)
	{
		audDebugf1("Failed to allocate PCM generator");
		return -1;
	}
	else
	{
		sysMemSet(audDriver::GetVoiceManager().GetPcmSourceState(allocationId), 0, sizeof(PcmSourceState));

		audPcmSourceFactory::InitSlot(allocationId, typeId);
		return static_cast<s32>(allocationId);
	}
}

audPcmSource *audMixerDevice::GetPcmSource(const s32 slotId)
{
	TrapLT(slotId, 0);
	return static_cast<audPcmSource*>(m_PcmSourcePool.GetSlotPtr(static_cast<u32>(slotId)));	
}

void audMixerDevice::FreePcmSource(const s32 slotId)
{
	TrapLT(slotId, 0);
	TrapGE((u32)slotId, m_PcmSourcePool.GetNumSlots());
	audAssert(m_PcmSourcePool.IsSlotInitialised(slotId));
	TrapNZ(m_PcmSourcePool.GetRefCount(slotId));	
	

	static_cast<audPcmSource*>(m_PcmSourcePool.GetSlotPtr(slotId))->Shutdown();	
	m_PcmSourcePool.Free(static_cast<u32>(slotId));
}

s32 audMixerDevice::GetPcmSourceSlotId(const audPcmSource *pcmSource)
{
	const u32 slotId = m_PcmSourcePool.GetSlotIndex(pcmSource);	
	if(slotId > m_PcmSourcePool.GetNumSlots())
	{
		return -1;
	}
	return static_cast<s32>(slotId);
}

#if !__FINAL
void audMixerDevice::RunTests()
{
	/*EnterBML();

	audStreamId streamId = m_DecodeManager.CreateStream(AUD_FORMAT_MP3, 1, 44100);
	audWaveSlot *waveSlot = audWaveSlot::FindStaticBankWaveSlot(audWaveSlot::FindBankId("SAMPLE\\LEFT"));

	Assert(waveSlot);
	if(waveSlot)
	{
		audWaveMetadata *wave = waveSlot->FindWaveMetadata(ATSTRINGHASH("B1", 0x8F5A35C));
		Assert(wave);
		audPacket packet;
		packet.inputData = wave->base.waveData;
		packet.inputBytes = wave->base.lengthBytes;
		packet.skipSamples = 0;
		m_DecodeManager.SubmitPacket(streamId, &packet);
	}

	m_DecodeManager.Update();

	s32 ret = cellMP3DecoderProcess(((audMixerDevicePs3*)this)->GetDecoderMem());
	if(ret != CELL_OK)
	{
		audErrorf("cellMP3DecoderProcess returned %d", ret);
	}

	m_DecodeManager.Update();

	ExitBML();*/
}
#endif

void audMixerDevice::ReinitialiseMixBuffers(const u32 numOutputChannels)
{
	// allocate one multichannel buffer for output
	// These should be the first allocations from the frame buffer pool so
	// we're guaranteed (and can assert) that we get a contiguous block from [0,numChannels]
	// GetOutputBuffer() relies on this being true.

	for(u32 i = 0; i < numOutputChannels; i++)
	{
		(void)AssertVerify(i == m_FrameBufferPool.Allocate());
	}
}

void audMixerDevice::Shutdown()
{
	if(!m_IsInitialised)
	{
		return;
	}

	ShutdownHardware();

	m_DecodeManager.Shutdown();
	
	for(u32 i = 0 ; i < m_NumVoices; i++)
	{
		m_Voices[i].DisconnectAllOutputsInternal();
	}
	m_Voices.Reset();

	for(s32 i = 0; i < m_Submixes.GetCount(); i++)
	{
		m_Submixes[i].Shutdown();
	}

	m_FrameBufferPool.Shutdown();
	audMixerConnection::ShutdownClass();

	m_PcmSourcePool.Shutdown();

	m_IsInitialised = false;
}

s32 audMixerDevice::InitClientThread(const char *threadName, const u32 bufferSize)
{
	// Check we haven't already initialised this thread
	audAssert(g_MixThreadClientId == -1);
	audMixerClientThreadState *state = rage_new audMixerClientThreadState();

	for(int i=0; i<kAudioNumberOfCommandBuffers; i++)
	{
		state->CommandBuffers.Append().Init(bufferSize, threadName);
	}

	state->Name = threadName;

	EnterBML();
	const s32 index = m_ClientThreads.size();
	m_ClientThreads.PushAndGrow(state);
	ExitBML();

	g_MixThreadClientId = index;

	return index;
}

audCommandBuffer *audMixerDevice::GetCommandBufferForThread() const
{
	if(Verifyf(g_MixThreadClientId != -1, "No command buffer set up for this thread"))
	{		
		return &m_ClientThreads[g_MixThreadClientId]->CommandBuffers[m_ClientThreads[g_MixThreadClientId]->WriteIndex];		
	}
	return NULL;
}

u32 audMixerDevice::GetThreadCommandBufferHighWatermark() const
{
	if(Verifyf(g_MixThreadClientId != -1, "No command buffer set up for this thread"))
	{
		return GetCommandBufferForThread()->GetHighWatermark();
	}
	return 0;
}

void audMixerDevice::FlagThreadCommandBufferReadyToProcess(const u32 timestamp /* = 0 */)
{
	if(Verifyf(g_MixThreadClientId != -1, "No command buffer set up for this thread"))
	{
		audMixerClientThreadState *clientState = m_ClientThreads[g_MixThreadClientId];
		audCommandBuffer &cmdBuffer = clientState->CommandBuffers[clientState->WriteIndex];

		audAssertf(!cmdBuffer.IsReadyForProcessing(), "Command buffer is already ready for processing!");
		cmdBuffer.SetTimestamp(timestamp);
		cmdBuffer.SetSubmitTime(clientState->BuffersSubmitted++);

		sys_lwsync();

		cmdBuffer.SetReadyForProcessing(true);
		
		clientState->WriteIndex = (clientState->WriteIndex + 1) % kAudioNumberOfCommandBuffers;		
	}
}

void audMixerDevice::WaitOnThreadCommandBufferProcessing() const
{
	if(Verifyf(g_MixThreadClientId != -1, "No command buffer set up for this thread") && !PARAM_noaudiohardware.Get())
	{
		audMixerClientThreadState *clientState = m_ClientThreads[g_MixThreadClientId];
		sysIpcSema sema = clientState->CommandBuffers[clientState->WriteIndex].GetProcessedSema();
		sysIpcWaitSema(sema);
	}
}

bool audMixerDevice::WriteCommand(const audCommandPacketHeader *packet)
{
	audCommandBuffer *commandBuffer = GetCommandBufferForThread();
	if(Verifyf(commandBuffer, "No mixer command buffer setup on this thread"))
	{
		audAssert(!commandBuffer->IsReadyForProcessing());
		return commandBuffer->WritePacket(packet);	
	}
	return false;
}

bool audMixerDevice::WriteAlignedCommand(const audCommandPacketHeader *packet)
{
	audCommandBuffer *commandBuffer = GetCommandBufferForThread();
	if(Verifyf(commandBuffer, "No mixer command buffer setup on this thread"))
	{
		audAssert(!commandBuffer->IsReadyForProcessing());
		return commandBuffer->WriteAlignedPacket(packet);	
	}
	return false;
}

void audMixerDevice::ExecuteCommandBuffers()
{
	PF_MIXTIMER_START(ExecuteCommandBuffers);

#if __BANK && RSG_PC
#define NEXT_LINE	grcDebugDraw::Text(Vector2(0.1f, lineBase), Color32(255,255,255), tempString ); lineBase += lineInc;
	if(g_ShowAudioFrameSkipDebugInfo)
	{
		if(m_ClientThreads.GetCount() >= 3)
		{
			char tempString[256];
			static bank_float lineInc = 0.02f;
			f32 lineBase = 0.3f;

			sprintf(tempString, "TimeStamp : %u", m_MixTimerFrames); NEXT_LINE;

			for(atArray<audMixerClientThreadState*>::iterator i = m_ClientThreads.begin(); i != m_ClientThreads.end(); i++)
			{
				sprintf(tempString, "%s - %u", (*i)->Name, (*i)->BuffersSubmitted); NEXT_LINE;

				for(s32 k = 0; k < (*i)->CommandBuffers.GetCount(); k++)
				{
					audCommandBuffer &cmdBuffer = (*i)->CommandBuffers[k];
					sprintf(tempString, "Size %7d : Highwater %7d : Freespace %7d : Current %7d - %d %s %d - TS(%u) ST(%u)", cmdBuffer.GetSize(), cmdBuffer.GetHighWatermark(), cmdBuffer.GetAvailableSpace(), cmdBuffer.GetCurrentOffset(),
						cmdBuffer.IsReadyForProcessing(), (*i)->Name, k, cmdBuffer.GetTimestamp(), cmdBuffer.GetSubmitTime()); NEXT_LINE;
				}
			}
		}
	}
#endif

	DEV_ONLY(m_CurrentlyProcessingCommandBuffer = NULL);
	for(atArray<audMixerClientThreadState*>::iterator i = m_ClientThreads.begin(); i != m_ClientThreads.end(); i++)
	{
		// Ensure buffers are processed in the order they were submitted.
		s32 firstIndex = 0;
		u32 submitTime = (*i)->CommandBuffers[0].GetSubmitTime();
		for(s32 x=1; x<kAudioNumberOfCommandBuffers; x++)
		{
			if((*i)->CommandBuffers[x].GetSubmitTime() < submitTime)
			{
				firstIndex = x;
				submitTime = (*i)->CommandBuffers[x].GetSubmitTime();
			}
		}

		for(s32 k = 0; k < (*i)->CommandBuffers.GetCount(); k++)
		{
			audCommandBuffer &cmdBuffer = (*i)->CommandBuffers[(firstIndex + k) % kAudioNumberOfCommandBuffers];

			enum { CommandDelayFrames = 3 };
			if(cmdBuffer.IsReadyForProcessing() && m_MixTimerFrames >= cmdBuffer.GetTimestamp() + CommandDelayFrames)
			{	
				u32 thisSubmitTime = cmdBuffer.GetSubmitTime();
				audAssertf((*i)->PrevBufferTimeProcessed == 0 || thisSubmitTime == (*i)->PrevBufferTimeProcessed + 1, "%s command buffer has been skipped! (%d > (%d + 1))", (*i)->Name, submitTime, (*i)->PrevBufferTimeProcessed);
				(*i)->PrevBufferTimeProcessed = thisSubmitTime;

				if(cmdBuffer.GetTimestamp() != 0)
				{
					PF_SET(CommandBufferLatency, m_MixTimerFrames - cmdBuffer.GetTimestamp());
					/*if(cmdBuffer.GetTimestamp() + CommandDelayFrames < m_MixTimerFrames)
					{
						audWarningf("Missed timestamp: %u (%u, %u)", cmdBuffer.GetTimestamp(), m_MixTimerFrames, cmdBuffer.GetTimestamp() + CommandDelayFrames);
					}*/
				}

				DEV_ONLY(m_CurrentlyProcessingCommandBuffer = &cmdBuffer);

				audCommandBufferIterator packetIter(cmdBuffer);
				audCommandPacketHeader *packet = packetIter.Next();
				while(packet)
				{
					ExecuteCommandPacket(cmdBuffer.GetTimestamp(), packet);
					packet = packetIter.Next();
				}
				cmdBuffer.Reset();
				cmdBuffer.SetReadyForProcessing(false);
				// Let the owning thread know its available for writing again
				sysIpcSignalSema(cmdBuffer.GetProcessedSema());
			}
		}		
	}

	DEV_ONLY(m_CurrentlyProcessingCommandBuffer = NULL);

	PF_MIXTIMER_STOP(ExecuteCommandBuffers);
}

void audMixerDevice::ExecuteCommandPacket(const u32 DEV_ONLY(timestamp), const audCommandPacketHeader *packet)
{
	DEV_ONLY(DebugTraceCommandPacket(timestamp, packet));

	switch(packet->systemId)
	{
	case audMixerDevice::COMMAND_SUBMIX:
		m_Submixes[packet->objectId].ExecuteCommand(packet);
		break;
	case audMixerDevice::COMMAND_VOICE:
		m_Voices[packet->objectId].ExecuteCommand(packet);
		break;
	case audMixerDevice::COMMAND_PCMSOURCE:
		{
			if(packet->commandId != audPcmSourceInterface::INITIALISE && !m_PcmSourcePool.IsSlotInitialised(packet->objectId))
			{
				switch(packet->commandId)
				{
				case audPcmSourceInterface::SET_PCM_SOURCE_PARAM_F32:
					Assertf(0, "Command for uninitialised PCM source. Source id %u commandid %u paramId %u value %f - %u", packet->objectId, packet->commandId, ((audSetPcmSourceF32ParamCommand*)packet)->paramId, ((audSetPcmSourceF32ParamCommand*)packet)->value, timestamp);
					break;
				case audPcmSourceInterface::SET_PCM_SOURCE_PARAM_U32:
					Assertf(0, "Command for uninitialised PCM source. Source id %u commandid %u paramId %u value %d - %u", packet->objectId, packet->commandId, ((audSetPcmSourceU32ParamCommand*)packet)->paramId, ((audSetPcmSourceU32ParamCommand*)packet)->value, timestamp);
					break;
				case audPcmSourceInterface::PCM_SOURCE_CUSTOM_COMMAND_PACKET:
					Assertf(0, "Command for uninitialised PCM source. Source id %u commandid %u paramId %u - %u", packet->objectId, packet->commandId, ((audPcmSourceCustomCommandPacket*)packet)->paramId, timestamp);
					break;
				default:
					Assertf(0, "Command for uninitialised PCM source. Source id %u commandid %u - %u", packet->objectId, packet->commandId, timestamp);
					break;
				}
#if __DEV
				static bool traced = false;
				if(!traced)
				{
					traced = true;
					DebugTraceCurrentCommandBuffer();
				}		
#endif
			}
			else 
			{				
				audPcmSource *pcmSource = static_cast<audPcmSource*>(m_PcmSourcePool.GetSlotPtr(packet->objectId));
				audAssert(pcmSource);
				
				switch(packet->commandId)
				{
				case audPcmSourceInterface::INITIALISE:
						audPcmSourceFactory::InitSlot(packet->objectId, ((audInitialisePcmSourceCommand *)packet)->TypeId);
						break;

					case audPcmSourceInterface::SET_PCM_SOURCE_PARAM_F32:
						pcmSource->SetParam(((audSetPcmSourceF32ParamCommand*)packet)->paramId,
												((audSetPcmSourceF32ParamCommand*)packet)->value);
						break;
					case audPcmSourceInterface::SET_PCM_SOURCE_PARAM_U32:
						pcmSource->SetParam(((audSetPcmSourceU32ParamCommand*)packet)->paramId,
												((audSetPcmSourceU32ParamCommand*)packet)->value);
						break;
					case audPcmSourceInterface::PCM_SOURCE_CUSTOM_COMMAND_PACKET:
						pcmSource->HandleCustomCommandPacket((audPcmSourceCustomCommandPacket*)packet);
						break;
					case audPcmSourceInterface::START_PCM_SOURCE:
						pcmSource->Start();
						break;
					case audPcmSourceInterface::STOP_AND_RELEASE:
						pcmSource->Stop();
						m_PcmSourcePool.RemoveRef(packet->objectId);
						break;
					default:
						audAssert(0);
						break;
				}
			}
		}
		break;

	default:
		audAssert(0);
	}
}

#if __DEV

void audMixerDevice::DebugTraceCurrentCommandBuffer()
{
	if(IsFixedFrameReplay())
		return;

	if(g_TraceCommandPackets)
		return;
	
	g_TraceCommandPackets = true;

	audAssert(m_CurrentlyProcessingCommandBuffer);

	ASSERT_ONLY(audDisplayf("DebugTraceCurrentCommandBuffer active buffer (%s):", m_CurrentlyProcessingCommandBuffer->GetName()));

	ForEachCommandFunctor cb;
	cb.Reset<audMixerDevice, &audMixerDevice::DebugTraceCommandPacket>(this);
	
	bool foundCurrent = false;
	for(atArray<audMixerClientThreadState*>::iterator i = m_ClientThreads.begin(); i != m_ClientThreads.end(); i++)
	{
	
		for(s32 k = 0; k < (*i)->CommandBuffers.GetCount(); k++)
		{
			const u32 timeStamp = (*i)->CommandBuffers[k].GetTimestamp();
			if((*i)->CommandBuffers[k].IsReadyForProcessing())
			{
			
				ASSERT_ONLY(audDisplayf("Buffer %d: %s - %u - %u - ", k, (*i)->CommandBuffers[k].GetName(), timeStamp, (*i)->CommandBuffers[k].GetSubmitTime()));
				if(&(*i)->CommandBuffers[k] == m_CurrentlyProcessingCommandBuffer)
				{
					foundCurrent = true;
				}
				(*i)->CommandBuffers[k].ForEachPacket(cb, timeStamp, audCommandBuffer::kDontEmpty);
			}
		}
	}
	
	if(!foundCurrent)
	{
		audAssert(0);
		m_CurrentlyProcessingCommandBuffer->ForEachPacket(cb, 0, audCommandBuffer::kDontEmpty);
	}

	g_TraceCommandPackets = false;
}
fiStream *g_DebugPacketLog = NULL;
void audMixerDevice::DebugTraceCommandPacket(const u32 timestamp, const audCommandPacketHeader *packet)
{
	
	if(g_TraceCommandPackets)
	{
		const char *systemIdNames[] = {"COMMAND_VOICE","COMMAND_SUBMIX","COMMAND_PCMSOURCE"};
		audAssert(packet->systemId < NELEM(systemIdNames));
		const char *commandName = "";
		const char *extraStr = "";
		char extraBuf[16];
		bool handled = false;
		switch(packet->systemId)
		{
		case COMMAND_VOICE:
			{
				audMixerVoice::DebugTraceCommandPacket(packet);
				handled = true;
			}
			break;

		case COMMAND_SUBMIX:
			{
				const char *commandIdNames[] = {
					"SET_VOLUMES",
					"SET_EFFECT",
					"SET_EFFECT_PARAM_U32",
					"SET_EFFECT_PARAM_F32",
				};
				audAssert(packet->commandId < NELEM(commandIdNames));
				commandName = commandIdNames[packet->commandId];
			}
			break;

		case COMMAND_PCMSOURCE:
			{
				const char *commandIdNames[] = {
				"INITIALISE",
				"SET_PCM_SOURCE_PARAM_F32", 
				"SET_PCM_SOURCE_PARAM_U32",
				"PCM_SOURCE_CUSTOM_COMMAND_PACKET",
				"START_PCM_SOURCE",
				"STOP_AND_RELEASE"
				};
				audAssert(packet->commandId < NELEM(commandIdNames));
				commandName = commandIdNames[packet->commandId];

				switch(packet->commandId)
				{				
				case audPcmSourceInterface::SET_PCM_SOURCE_PARAM_F32:
					{
						audSetPcmSourceF32ParamCommand *paramCommand = (audSetPcmSourceF32ParamCommand*)packet;
						formatf(extraBuf, "%u:%f", paramCommand->paramId, paramCommand->value);
						extraStr = extraBuf;
					}
					break;
				case audPcmSourceInterface::SET_PCM_SOURCE_PARAM_U32:
					{
						audSetPcmSourceU32ParamCommand *paramCommand = (audSetPcmSourceU32ParamCommand*)packet;
						formatf(extraBuf, "%u:%08X", paramCommand->paramId, paramCommand->value);
						extraStr = extraBuf;
					}
					break;
				default:
					break;
				}
			}
			break;

		default:
			commandName = "Error";
			audAssert(0);
		}

		/*if(!g_DebugPacketLog)
		{
			g_DebugPacketLog = ASSET.Create("c:\\mixerlog", "txt");
		}*/

		if(g_DebugPacketLog)
		{
			char lineBuf[128];
			formatf(lineBuf, "%s (%u) %s (%u) objectId: %u %s [size: %u bytes] %u\r\n", systemIdNames[packet->systemId], packet->systemId, commandName, packet->commandId, packet->objectId, extraStr, packet->packetSize, timestamp);
			g_DebugPacketLog->Write(lineBuf, istrlen(lineBuf));
			//g_DebugPacketLog->Write(packet, packet->packetSize);
		}
		else if(!handled)
		{
			audDisplayf("%s (%u) %s (%u) objectId: %u %s [size: %u bytes] %u", systemIdNames[packet->systemId], packet->systemId, commandName, packet->commandId, packet->objectId, extraStr, packet->packetSize, timestamp);
		}
	}
	else
	{
		if(g_DebugPacketLog)
		{
			g_DebugPacketLog->Close();
			g_DebugPacketLog = NULL;
		}
	}
}
#endif // __DEV

audMixerSubmix *audMixerDevice::CreateSubmix(const char *name, const u32 numChannels, const bool hasVoiceInputs)
{
	audMixerSubmix &submix = m_Submixes.Append();
	submix.Init(GetSubmixIndex(&submix), numChannels, hasVoiceInputs);
#if AUD_SUPPORT_METERING
	submix.SetName(name);
#else
	(void)name;
#endif	
	return &submix;
}

audMixerVoice *audMixerDevice::InitVoice(const u32 index)
{
	audMixerVoice *voice = &m_Voices[index];
	voice->Init();
	return voice;
}

s32 audMixerDevice::GetVoiceIndex(const audMixerVoice *voice) const
{
	audAssert(voice);
	return ptrdiff_t_to_int(voice - m_Voices.GetElements());
}

s32 audMixerDevice::GetSubmixIndex(audMixerSubmix *const submix) const
{
	if(!submix)
	{
		return -1;
	}
	else
	{
		return ptrdiff_t_to_int(submix - &m_Submixes[0]);
	}
}

f32 *audMixerDevice::GetOutputBuffer()
{
	return m_FrameBufferPool.GetBuffer(0);
}

bool audMixerDevice::IsPaused() const	
{ 
#if AUDIO_CAPTURE_ENABLED
	// Whilst capture is active, pause the mixer whenever the core audio group pauses. This ensures 
	// that the user only hears audio frames that are actually being recorded
	IAudioMixCapturer* capturer = (IAudioMixCapturer*)sysInterlockedReadPointer( &m_mixCapturer );
	if( capturer )
	{
		if(capturer->IsCapturing() && audMixerDevice::IsPaused(0))
		{
			return true;
		}
	}
#endif
	
	return m_Paused;
}

void audMixerDevice::AddEndPoint(audMixerSubmix *submix)
{
	m_EndPoints.Append() = submix;
	m_RecomputeGraph = true;
}

#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
void audMixerDevice::SetReplayMixerModeLoading()
{
	Displayf("audMixerDevice::SetReplayMixerModeLoading()");
	if(audDriver::GetMixer() && m_IsCapturing && m_IsFrameRendering)
	{
		audDriver::GetMixer()->PauseGroup(0, true);
		EnterReplayMixerSwitchLock();
		m_IsFrameRendering = false;
		ExitReplayMixerSwitchLock();
		EnterReplaySwitchLock();
		{
			TriggerUpdate();
			WaitOnMixBuffers();
		}
		ExitReplaySwitchLock();

		g_AudioEngine.SetTriggeredUpdates(false);		// disable manual updates for audEngine::AudioFrame()
		g_AudioEngine.TriggerUpdate();
		g_AudioEngine.PollForAudioFrame();
	}
}

void audMixerDevice::SetReplayMixerModeRendering()
{
	Displayf("audMixerDevice::SetReplayMixerModeRendering()");
	if(m_IsCapturing)
	{
		EnterReplayMixerSwitchLock();
		m_IsFrameRendering = true;
		ExitReplayMixerSwitchLock();
		EnterReplaySwitchLock();
		{
			g_AudioEngine.SetTriggeredUpdates(true);
		}
		ExitReplaySwitchLock();
	}
	audDriver::GetMixer()->PauseGroup(0, false);
}

void audMixerDevice::PrepareForReplayRender()
{
	ResetAVRenderedTimes();
	SetIsFixedFrameReplay(true);
	PauseGroup(0, true);

	EnterReplaySwitchLock();
	{
		m_IsCapturing = true;									// enable capture mode, the mixer should lock wait in sm_TriggerUpdateSema
	}
	ExitReplaySwitchLock();
}

void audMixerDevice::CleanUpReplayRender()
{
	//Displayf("CReplayCoordinator::CleanupAudioBake()");
	if(m_IsCapturing && m_IsFrameRendering)
	{
		SetReplayMixerModeLoading();
	}
	if(m_IsCapturing)
	{
		EnterReplaySwitchLock();
		{
			m_IsCapturing = false;								// turn off capture mode
		}
		ExitReplaySwitchLock();
	}
}

void audMixerDevice::StartAudioCapture( IAudioMixCapturer& capturer )
{ 
	StopAudioCapture();
	sysInterlockedExchangePointer( &m_mixCapturer, &capturer );
}

void audMixerDevice::StopAudioCapture()
{
	sysInterlockedExchangePointer( &m_mixCapturer, NULL );
}

void audMixerDevice::UpdateAudioCapture()
{
	IAudioMixCapturer* capturer = (IAudioMixCapturer*)sysInterlockedReadPointer( &m_mixCapturer );
	if( capturer )
	{
		static s16 output[ kMixBufNumStereoSamples ];

		static ALIGNAS(128) struct
		{
			f32 m_data[ kMixBufNumStereoSamples ];
		} floatBuffer;

		PF_MIXTIMER_START(AudioCapture);

		// We should already be at stereo by this point if we are outputting as stereo, so only down-mix if not stereo.
		if( m_FinalOutputDownmixed ) 
		{
			CopyToStereoOnlyBuffer<kMixBufNumSamples>( m_FrameBufferPool.GetBuffer_ReadOnly(0), floatBuffer.m_data );
		}
		else
		{
			Assertf(m_NumOutputChannels == 6, "audMixerDevice::UpdateAudioCapture - We only support Downmixing from 6 channels - Grab an audio programmer to fix this!" );
			DownmixOutputToStereoOnlyBuffer<kMixBufNumSamples>( m_FrameBufferPool.GetBuffer_ReadOnly(0), floatBuffer.m_data );
		}
		
		for( u32 i = 0, k = 0; i < kMixBufNumSamples; ++i )
		{
			// interleave buffer
			for( u32 j = 0; j < 2; j++)
			{			
				// 16 bit
				const f32& scaled_value = Clamp(floatBuffer.m_data[i + (kMixBufNumSamples * j)], -1.0f, 1.0f) * 32767.f;
				output[k] = (s16)(scaled_value);
				k++;
			}
		}

		// Call callback to capture samples
		capturer->CaptureMix( output, kMixBufNumStereoSamples, 2 );
				
		PF_MIXTIMER_STOP(AudioCapture);
	}

}

#endif // defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED

void audMixerDevice::ComputeProcessingGraph()
{
	// reset all submixes to a disconnected state
	for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = m_Submixes.begin(); i != m_Submixes.end(); i++)
	{
		(*i).SetProcessingStage(-1);		
	}

	for(s32 i = 0; i < m_EndPoints.GetCount(); i++)
	{
		s32 currentStage = 0;
		ComputeProcessingGraph(m_EndPoints[i], currentStage);
	}

	s32 endStage = -1;
	for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = m_Submixes.begin(); i != m_Submixes.end(); i++)
	{
		endStage = Max(endStage, (*i).GetProcessingStage());
	}
	m_NumProcessingStages = endStage;

	// now we need to reverse the stage IDs so that they represent the order of processing
	for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = m_Submixes.begin(); i != m_Submixes.end(); i++)
	{
		if((*i).GetProcessingStage() != -1)
		{
			(*i).SetProcessingStage(endStage - (*i).GetProcessingStage());
		}
	}

	enum {kMaxProcessingStages = 16};
	audAssert(m_NumProcessingStages+1 < kMaxProcessingStages);
	s32 processingBufferReq[kMaxProcessingStages];
	
	// compute submix mix buffer requirements
	s32 maxBufferReqStage = 0;
	for(s32 stage = 0; stage <= m_NumProcessingStages; stage++)
	{
		processingBufferReq[stage] = 0;
		Printf("Stage %d: ", stage);
		for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = m_Submixes.begin(); i != m_Submixes.end(); i++)
		{
			(*i).ComputeLastOutputStage();
			if(((*i).GetLastOutputStage() >= stage && (*i).HasVoiceInputs())
				|| 
				((*i).GetProcessingStage() <= stage && (*i).GetLastOutputStage() >= stage))
			{
				processingBufferReq[stage] += (*i).GetNumChannels();
				DEV_ONLY(Printf("\'%s\', ", (*i).GetName()));
			}
		}

		if(processingBufferReq[stage] > processingBufferReq[maxBufferReqStage])
		{
			maxBufferReqStage = stage;
		}
	}
	audDisplayf("Max simultaneous buffer requirement: stage %d (%d channel buffers)", maxBufferReqStage, processingBufferReq[maxBufferReqStage]);
	
	// Sort submixes into processing order
	// Note that we can't physically sort the array since we rely on submix indices remaining constant for connections
	m_SubmixProcessingOrder.SetCount(m_Submixes.GetCount());

	s32 currentIndex = 0;
	for(s32 stage = 0; stage <= m_NumProcessingStages; stage++)
	{
		for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = m_Submixes.begin(); i != m_Submixes.end(); i++)
		{
			if((*i).GetProcessingStage() == stage)
			{
				m_SubmixProcessingOrder[currentIndex++] = (*i).GetSubmixId();
			}
		}
	}

	m_SubmixProcessingOrder.Resize(currentIndex);

#if __DEV
	audDisplayf("Processing order:");
	for(s32 i = 0; i < m_SubmixProcessingOrder.GetCount(); i++)
	{
		audDisplayf("%d %s", m_Submixes[m_SubmixProcessingOrder[i]].GetProcessingStage(), m_Submixes[m_SubmixProcessingOrder[i]].GetName());
	}
#endif

	m_RecomputeGraph = false;
}

void audMixerDevice::ComputeProcessingGraph(audMixerSubmix *submix, s32 stage)
{
	if(submix->GetProcessingStage() >= stage)
	{
		// For a submix with multiple outputs, we want to take the value furthest from the output (ie highest stage value)
		return;
	}
	
	submix->SetProcessingStage(stage);

	for(u32 i = 0; i < submix->m_NumSubmixInputs; i++)
	{
		audAssert(submix->m_SubmixInputConnectionIds[i] != 0xffff);
		audMixerConnection *inputConnection = audMixerConnection::GetConnectionFromId(submix->m_SubmixInputConnectionIds[i]);
		audAssert(!inputConnection->IsVoiceInput());

		ComputeProcessingGraph(GetSubmixFromIndex(inputConnection->GetInput()), stage+1);	
	}
}

 void audMixerDevice::PauseGroup(const s32 groupId, const bool isPaused)
 { 
	 EnterBML();
	 m_PauseGroupState[groupId] = isPaused;
	 ExitBML();
 }

#if __BANK

void DrawLevelMeter(const Color32 color, const float left, const float bottom, const float width, const float height)
{	
	grcBegin(drawTriStrip, 4);
		grcColor(color);
		grcVertex2f(left,bottom);
		grcVertex2f(left,bottom-height);
		grcVertex2f(left+width,bottom);
		grcVertex2f(left+width,bottom-height);
	grcEnd();
}

struct WavePlayerStats
{
	float CurrentPeak;
	float Headroom;
	u32 CurrentPeakData;
	audWavePlayer *WavePlayer;
};

void audMixerDevice::DrawDebug()
{
#if AUD_WAVEPLAYER_METERING
	atArray<WavePlayerStats> wavePlayerStats;

	EnterBML();
	
	audPcmSourcePool::Iterator iter(g_PcmSourcePool, 1<<AUD_PCMSOURCE_WAVEPLAYER);

	{
		u32 i = 0;
		while((i = iter.Next()) < g_PcmSourcePool->GetNumSlots())
		{
			audWavePlayer *wavePlayer = reinterpret_cast<audWavePlayer*>(g_PcmSourcePool->GetSlotPtr(i));
			if(wavePlayer->GetNumPhysicalClients() > 0)
			{
				WavePlayerStats &stats = wavePlayerStats.Grow();
				stats.CurrentPeak = wavePlayer->GetPeakLevel();
				stats.Headroom = wavePlayer->GetHeadroom();
				stats.CurrentPeakData = wavePlayer->GetCurrentPeakLevel();
				stats.WavePlayer = wavePlayer;
			}
		}
	}

	ExitBML();

	PUSH_DEFAULT_SCREEN();
	audDriver::SetDebugDrawRenderStates();

	float currentX = 50.f;
	const float width = 5.f;
	const float height = 100.f;
	const float bottom = 400.f;

	Color32 color1(0.6f,0.f,0.f,1.f);
	Color32 color2(0.3f,0.f,1.f,1.f);
	Color32 color3(0.5f,0.3f,1.f,1.f);
	float errorSum = 0.f;
	float errorMax = 0.f;
	
	const float linearHeadroom = audDriverUtil::ComputeLinearVolumeFromDb(-2.f/* - wavePlayerStats[i].Headroom*/);
	DrawLevelMeter(color1, currentX, bottom, width, height * linearHeadroom);
	currentX += width;

	for(s32 i = 0; i < wavePlayerStats.GetCount(); i++)
	{
		const float peakDataSample = wavePlayerStats[i].CurrentPeakData / float(65535u);
		DrawLevelMeter(color3, currentX, bottom, width, height * peakDataSample);
		currentX += width;
		
		DrawLevelMeter(color2, currentX, bottom, width, height * wavePlayerStats[i].CurrentPeak);

		const float actual_dB = audDriverUtil::ComputeDbVolumeFromLinear_Precise(wavePlayerStats[i].CurrentPeak);
		const float estimate_dB = audDriverUtil::ComputeDbVolumeFromLinear_Precise(peakDataSample);
		const float error_dB = actual_dB - estimate_dB;
		errorSum += error_dB;
		errorMax = Min(errorMax, error_dB);
		currentX += width + 2.f;
	}

	DrawLevelMeter(color1, currentX, bottom, width, height * linearHeadroom);
	currentX += width;

	POP_DEFAULT_SCREEN();

	if(wavePlayerStats.GetCount())
	{
		const float averageErr = errorSum / (float)wavePlayerStats.GetCount();
		PF_SET(HeadroomErrAvg, averageErr);
		PF_SET(HeadroomErrMax, errorMax);
	}
#endif // AUD_WAVEPLAYER_METERING
}

#endif // __BANK

} // namespace rage
