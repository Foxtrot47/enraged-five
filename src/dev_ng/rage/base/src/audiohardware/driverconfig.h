// 
// audiohardware/driverconfig.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DRIVERCONFIG_H
#define AUD_DRIVERCONFIG_H

#include "audiohardware/voicesettings.h"

namespace rage {

class audDriverConfig
{
public:
	audDriverConfig()
	{
		m_WaveSlotsConfig = "config/waveslots2.xml";
		m_WaveRootPath = "Sfx/";
		m_NumVirtualVoices = 2048;
		m_NumPhysicalVoices = kMaxVoices;
		m_MaxPhysicalStereoVoices = 0;
		m_NumNewPhysicalVoiceBufferVoices = 20;
		m_NumGranularDecoders = RSG_XENON ? 58 : (RSG_PS3 ? 72 : 0);
	}

	~audDriverConfig(){}

	bool Init();

	// PURPOSE
	//	Returns the path to the wave slot config file.
	// RETURNS
	//	The path to the wave slot config file.
	// NOTES
	//	Defaults to config/waveslots.xml.
	const char *GetWaveSlotsConfig() const
	{
		return m_WaveSlotsConfig;
	}


	// PURPOSE
	//	Returns the path to wave assets.
	// RETURNS
	//	The path to wave assets.
	// NOTES
	//	Defaults to Sfx/.
	const char *GetWaveRootPath() const
	{
		return m_WaveRootPath;
	}

	// PURPOSE
	//	Returns the number of virtual voices to use.
	// RETURNS
	//	The number of virtual voices to use.
	s32 GetNumVirtualVoices() const
	{
		return m_NumVirtualVoices;
	}

	// PURPOSE
	//	Returns the total number of physical voices to use.
	// RETURNS
	//	The total number of physical voices to use.
	s32 GetNumPhysicalVoices() const
	{
		return m_NumPhysicalVoices;
	}

	// PURPOSE
	//	Returns the maximum number of physical stereo voices that can be used
	// RETURNS
	//	The maximum supported number of stereo voices
	// NOTES
	//	Defaults to m_NumPhysicalVoices.
	s32 GetMaxPhysicalStereoVoices() const
	{
		return m_MaxPhysicalStereoVoices;
	}

	// PURPOSE
	//	Returns the total number of granular decoders to use.
	// RETURNS
	//	The total number of granular decoders to use.
	// NOTES
	//	Defaults to 0.
	s32 GetNumGranularDecoders() const
	{
		return m_NumGranularDecoders;
	}

	// PURPOSE
	//	Returns the number of physical voices to keep free for new
	//	voice requests.
	// RETURNS
	//	The number of physical voices to keep free for new
	//	voice requests.
	// NOTES
	//	Defaults to 15.
	s32 GetNumNewPhysicalVoiceBufferVoices() const
	{
		return m_NumNewPhysicalVoiceBufferVoices;
	}

	bool IsAudioProcessingEnabled() const
	{
#if RSG_FINAL
		return true;
#else
		return m_IsAudioProcessingEnabled;
#endif // RSG_FINAL
	}

private:
	s32 m_NumVirtualVoices;
	s32 m_NumPhysicalVoices;
	s32 m_MaxPhysicalStereoVoices;
	s32 m_NumGranularDecoders;
	s32 m_NumNewPhysicalVoiceBufferVoices;
	
	const char *m_WaveSlotsConfig;
	const char *m_WaveRootPath;
	const char *m_EffectsRootPath;
	const char *m_RpfConfig;
	const char *m_RpfDirectory;

	bool m_IsAudioProcessingEnabled;
};

} // namespace rage

#endif // AUD_DRIVERCONFIG_H
