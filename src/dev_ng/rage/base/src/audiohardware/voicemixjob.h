// 
// audiohardware/voicemixjob.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_VOICEMIXJOB_H
#define AUD_VOICEMIXJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(VoicemixJob);

#endif // AUD_VOICEMIXJOB_H
