//=====================================================================================================
// name:		musicreader.h
// description:	Media stream reader for audio streams.
//=====================================================================================================

#ifndef INC_MUSICREADER_H_
#define INC_MUSICREADER_H_

#if __WIN32PC

#include "system/criticalsection.h"
#include "atl/array.h"
#include "audiohardware/ringbuffer.h"
#include "audiosoundtypes/sound.h"
#include "system/xtl.h"

namespace rage {

const u32 g_MaxMediaReaders = 10;
const u32 kMinSongDuration = 5000;
const u32 kMaxSongDuration = 7200000;

enum USER_TRACK_FORMAT
{
	TRACK_FORMAT_UNKNOWN = 0,
	//TRACK_FORMAT_OGG, // ogg vorbis		(.ogg)
	//TRACK_FORMAT_WAV, // PCM Wave			(.wav)
	TRACK_FORMAT_WM,	// Windows Media	(WMA, MP3 etc, WMSDK decoder)
	//TRACK_FORMAT_QT,	// Quicktime		(AAC, M4A etc, QTSDK decoder)
	TRACK_FORMAT_WMF,	// Windows Media Foundation
	TRACK_FORMAT_COUNT
};

enum USER_TRACK_STATE
{
	TRACK_STATE_INVALID = 0,
	TRACK_STATE_INITIALIZING,
	TRACK_STATE_PREPARING,
	TRACK_STATE_PREPARED,
	TRACK_STATE_FAILED,
	TRACK_STATE_COUNT
};


typedef struct 
{
	WCHAR fileExt[5];
	USER_TRACK_FORMAT format;

}USER_TRACK_FORMAT_LOOKUP;


// base class for each specific type of media being read (mp3, wma, ogg, flac, etc.)
class audMediaReaderPlatform
{
public:

	virtual ~audMediaReaderPlatform()	{};


	virtual bool MediaOpen(const WCHAR* pszFileName, u32 startOffsetMs) = 0;
	virtual void Prepare() = 0;

	bool MediaRead(void* destAddr0, void* destAddr1, size_t size, sysIpcSema sema);
	virtual bool MediaClose() = 0;
	virtual s32	FillBuffer(void *sampleBuf, u32 dwBytesToRead) = 0;

	virtual void SetCursor(u32 startOffsetMs) = 0;
	virtual s32	GetSampleRate() = 0;
	virtual s32	GetStreamLengthMs() = 0;
	virtual s32	GetStreamPlayTimeMs() = 0;
	virtual const char *GetArtist() = 0;
	virtual const char *GetTitle() = 0;

	virtual s32 GetNumChannels() const = 0;
	
};

//=====================================================================================================
class audMediaReader
{
public:

	//-------------------------------------------------------------------------------------------------
	audMediaReader();
	~audMediaReader();

	//-------------------------------------------------------------------------------------------------

	bool Open(const WCHAR* pszFileName, u32 startOffsetMs, bool openOnly);
	bool Close();
	
	s32 FillBuffer(void *sampleBuf, u32 dwBytesToRead);

	void SetFileOpen(bool bOpen)				{ m_bFileOpen = bOpen; }
	bool IsFileOpen() const						{ return m_bFileOpen; }

	void SetMsStartOffset(u32 startOffsetMs)	 { if(m_MediaStreamer)m_MediaStreamer->SetCursor(startOffsetMs); }

	bool IsPrepared(audMediaReader* mediaReader);
	bool IsPlaying()							{ if(m_Sound && (*m_Sound)) 
														return true; 
													else 
														return false; 
												}
	void StopSound()							{ if(m_Sound && (*m_Sound)) 
													(*m_Sound)->StopAndForget(); 
												}

	s32 GetState()								{ return m_State; }
	s32	GetSampleRate()							{ return (m_MediaStreamer ? m_MediaStreamer->GetSampleRate() : -1); }
	s32	GetStreamLengthMs()						{ return (m_MediaStreamer ? m_MediaStreamer->GetStreamLengthMs() : -1); }
	s32	GetStreamPlayTimeMs() const;
	s32 GetNumChannels() const { return m_MediaStreamer ? m_MediaStreamer->GetNumChannels() : -1; }
	static USER_TRACK_FORMAT GetFileFormat(const WCHAR *fileName);

	static void InitClass();

	static bool IsFormatAvailable(USER_TRACK_FORMAT format);

	static audMediaReader* CreateMediaStreamer(const WCHAR* filename, audSound** sound, u32 startOffsetMs = 0);
	static void DestroyMediaStreamer(audMediaReader* pMediaReader);
	
	const char *GetArtist()							{ return (m_MediaStreamer ? m_MediaStreamer->GetArtist() : NULL); }
	const char *GetTitle()							{ return (m_MediaStreamer ? m_MediaStreamer->GetTitle() : NULL); }


	// PURPOSE
	//	Attempts to open the specified file to ascertain if it's supported/valid
	static bool IsValidFile(const WCHAR *filePath, s32 &duration);

	audReferencedRingBuffer* m_RingBuffer;
	WCHAR m_Filename[1024];
	u32 m_StartOffsetMs;

private:
	//-------------------------------------------------------------------------------------------------
	void UpdateBuffer();

	audMediaReaderPlatform*	m_MediaStreamer;

	bool m_bFileOpen;

	USER_TRACK_FORMAT m_TrackFormat;

	static DECLARE_THREAD_FUNC(StreamReaderThreadEntryProc);

	sysIpcSema m_DecoderThreadFinishedSema;
	sysIpcThreadId m_DecodeThread;

	bool m_EnableDecode;
	bool m_EnableStreamReaderThread;
	bool m_IsStreamReaderThreadRunning;

	u8* m_SoundBuffer;

	audSound** m_Sound;  // pointer to audExternalStreamSound

	USER_TRACK_STATE m_State;

	static USER_TRACK_FORMAT_LOOKUP sm_UserTrackFormatLookup[];

	static atRangeArray<audMediaReader *, g_MaxMediaReaders> sm_MediaReader;

};

}
#endif // WIN32PC
#endif
