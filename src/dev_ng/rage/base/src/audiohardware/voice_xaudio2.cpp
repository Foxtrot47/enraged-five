//
// audiohardware/voice_xaudio2.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#if __XENON

#include "driver_xenon.h"

#include "voice_xaudio2.h"

#include "driver.h"
#include "driverutil.h"

#include "voicemgr.h"
#include "waveref.h"
#include "audioeffecttypes/sourcefiltereffect_xenon.h"
#include "audioengine/engine.h"
#include "audioengine/effectmanager.h"
#include "audiosoundtypes/sounddefs.h"
#include "math/amath.h"
#include "system/memory.h"

#include "file/stream.h"

#define		FAST_VOICE_XAUDIO2		1

namespace rage {

	using namespace Vec;

	extern bool g_CollapseRearsToFront;

	const u8 g_ThresholdFramesFrozenToStopVoice = 100;
	audVoiceXAudio2::audVoiceXAudio2() : m_SourceVoice(NULL), m_SourceEffectSubmixVoice(NULL), m_PreviousPlayTimeMs(0), m_NumFramesFrozen(0)
	{

	}

	audVoiceXAudio2::~audVoiceXAudio2(void)
	{

	}

	void audVoiceXAudio2::Init(const audVoiceSettings *voiceSettings)
	{
		audVoicePhysical::Init(voiceSettings);

		m_BitFlags.HasStartedVoice = false;
		m_NeedsNewData = false;
		m_SubmittedFirstPacket = false;

		const audWaveFormat *format = m_VoiceSettings->WaveRef.FindFormat();

		

		//Create a source voice.

		WAVEFORMATEX *pWaveFormat;
		XMA2WAVEFORMATEX xmaSourceFormat = {0};
		WAVEFORMATEX pcmSourceFormat = {0};

		if(format->Format == audWaveFormat::kPcm16bitBigEndian)
		{
			// PCM
			m_WaveLengthSamples = format->LengthSamples;
			
			pcmSourceFormat.cbSize = 0;
			pcmSourceFormat.nChannels = 1;
			pcmSourceFormat.wFormatTag = WAVE_FORMAT_PCM;
			pcmSourceFormat.nSamplesPerSec = m_BaseSampleRate;
			pcmSourceFormat.wBitsPerSample = 16;
			pcmSourceFormat.nBlockAlign = pcmSourceFormat.wBitsPerSample >> 3;
			pcmSourceFormat.nAvgBytesPerSec = pcmSourceFormat.nSamplesPerSec * pcmSourceFormat.nBlockAlign;

			pWaveFormat = (WAVEFORMATEX*)&pcmSourceFormat;
		}
		else
		{
			Assert(format->Format == audWaveFormat::kXMA2);
			// XMA2

			// use the actual play length (valid encoded data)
			m_WaveLengthSamples = (format->PlayEnd-format->PlayBegin)<<7;
			
			xmaSourceFormat.wfx.cbSize = 34;
			xmaSourceFormat.wfx.wFormatTag = WAVE_FORMAT_XMA2;
			xmaSourceFormat.wfx.nChannels = 1;
			xmaSourceFormat.wfx.nSamplesPerSec = m_BaseSampleRate;
			xmaSourceFormat.wfx.nBlockAlign = 2;
			xmaSourceFormat.wfx.wBitsPerSample = 16;

			xmaSourceFormat.LoopCount = 255;
			xmaSourceFormat.SamplesEncoded = format->LengthSamples;
			// these fields are aligned to 128 samples, and are stored as val/128
			// so multiply by 128 to get back to the correct value.
			xmaSourceFormat.LoopBegin = 0;// format->LoopBegin<<7;
			xmaSourceFormat.LoopLength = 0;//(format->LoopEnd-format->LoopBegin)<<7;
			xmaSourceFormat.PlayBegin = 0;//format->PlayBegin<<7;
			xmaSourceFormat.PlayLength = 0;//(format->PlayEnd-format->PlayBegin)<<7;

			xmaSourceFormat.NumStreams = 1;

			xmaSourceFormat.BytesPerBlock = 2048;
			xmaSourceFormat.EncoderVersion = 4;

			xmaSourceFormat.BlockCount = 1;

			//if(m_BitFlags.IsStreaming)
			//{
			//	xmaSourceFormat.BlockCount = 128;// ??
			//}
			//else
			//{
			//	u32 waveLengthBytes = 0;
			//	m_VoiceSettings->WaveRef.FindWaveData(waveLengthBytes);
			//	Assign(xmaSourceFormat.BlockCount, waveLengthBytes / xmaSourceFormat.BytesPerBlock);
			//}
			//

			pWaveFormat = (WAVEFORMATEX*)&xmaSourceFormat;
		}

		m_WaveLengthMs = audDriverUtil::ConvertSamplesToMs((f32)m_WaveLengthSamples, (f32)m_BaseSampleRate);

		Verifyf(SUCCEEDED(audDriverXenon::GetXAudio2()->CreateSourceVoice(&m_SourceVoice, pWaveFormat)), "Failed to create XAudio2 source voice");
		

		//
		//Use a dedicated source filter for every physical voice.
		//	
		XAUDIO2_EFFECT_DESCRIPTOR apoDesc[1] = {0};	
		IXAPO *pEffect = audDriverXenon::GetVoiceFilter(audDriver::GetVoiceManager().GetPhysicalVoiceIndex(this));
		Assert(pEffect);
		apoDesc[0].InitialState = true;
		apoDesc[0].OutputChannels = 1;
		apoDesc[0].pEffect = pEffect;

		XAUDIO2_EFFECT_CHAIN chain = {0};
		chain.EffectCount = 1;
		chain.pEffectDescriptors = apoDesc;

		m_SourceVoice->SetEffectChain(&chain);

		//Check if we have a source effect submix.
		if(m_VoiceSettings->EffectChain)
		{
			m_SourceEffectSubmixVoice = (IXAudio2SubmixVoice *)(m_VoiceSettings->EffectChain->GetPhysicalSubmix());
		}

		//
		//Route the source and source effect submix voices (if available) to the appropiate destination(s), as defined by
		//effectRoute.
		//
		XAUDIO2_VOICE_SENDS voiceOutput = {0};
		XAUDIO2_SEND_DESCRIPTOR voiceOutputEntries[g_MaxVoiceRouteDestinations];
		voiceOutput.pSends = voiceOutputEntries;

		audEffect *destinationEffect;
		switch(m_VoiceSettings->EffectRoute)
		{
		case EFFECT_ROUTE_MUSIC:
			voiceOutput.SendCount = 1;
			//Route to music effect chain submix voice.
			destinationEffect = g_AudioEngine.GetEffectManager().GetRouteEffectChain(AUD_MUSIC_EFFECT_CHAIN);
			if(destinationEffect)
			{
				voiceOutputEntries[0].Flags = 0;
				voiceOutputEntries[0].pOutputVoice = (IXAudio2Voice*)(destinationEffect->GetPhysicalSubmix());
			}
			else
			{
				voiceOutputEntries[0].Flags = 0;
				voiceOutputEntries[0].pOutputVoice = NULL;
			}
			break;

		case EFFECT_ROUTE_FRONT_END:
			voiceOutput.SendCount = 1;
			//Route to frontend effect chain submix voice.
			destinationEffect = g_AudioEngine.GetEffectManager().GetRouteEffectChain(AUD_FE_EFFECT_CHAIN);
			if(destinationEffect)
			{
				voiceOutputEntries[0].Flags = 0;
				voiceOutputEntries[0].pOutputVoice = (IXAudio2Voice*)(destinationEffect->GetPhysicalSubmix());
			}
			else
			{
				voiceOutputEntries[0].Flags = 0;
				voiceOutputEntries[0].pOutputVoice = NULL;
			}
			break;

			//case EFFECT_ROUTE_POSITIONED:
		default:
			voiceOutput.SendCount = g_NumSourceReverbs + 1;
			//Route to listener effect chain submix voice (dry) and all 3 source reverb submix voices (wet.)
			u8 destinationEffectChains[g_NumSourceReverbs + 1] =
			{
				AUD_PRE_LISTENER_EFFECT_CHAIN,
				AUD_SOURCE_REVERB_SMALL_EFFECT_CHAIN,
				AUD_SOURCE_REVERB_MEDIUM_EFFECT_CHAIN,
				AUD_SOURCE_REVERB_LARGE_EFFECT_CHAIN
			};

			for(u32 i=0; i<g_NumSourceReverbs + 1; i++)
			{
				destinationEffect = g_AudioEngine.GetEffectManager().GetRouteEffectChain(destinationEffectChains[i]);
				if(destinationEffect)
				{
					voiceOutputEntries[i].Flags = 0;
					voiceOutputEntries[i].pOutputVoice = (IXAudio2Voice *)(destinationEffect->GetPhysicalSubmix());
				}
				else
				{
					voiceOutputEntries[i].Flags = 0;
					voiceOutputEntries[i].pOutputVoice = NULL;
				}
			}
			break;
		}

		if(m_SourceEffectSubmixVoice)
		{
			//Add the wet route to the existing bypass/dry routes.
			voiceOutputEntries[voiceOutput.SendCount].pOutputVoice = m_SourceEffectSubmixVoice;
			voiceOutputEntries[voiceOutput.SendCount].Flags = 0;
			voiceOutput.SendCount++;
		}

		Verifyf(SUCCEEDED(m_SourceVoice->SetOutputVoices(&voiceOutput)), "Failed to set source voice outputs");

		CompileTimeAssert(sizeof(m_WavePackets) % 4 == 0);
		sysMemZeroBytes<sizeof(m_WavePackets)>(m_WavePackets);
		//sysMemSet(m_WavePackets, 0, g_NumStreamingBlocksPerWaveSlot * sizeof(XAUDIOPACKET));

		m_WavePacketIndex = 0;
		m_StartOffsetSamples = 0;

		if(format->LoopPointSamples >= 0)
		{
			m_BitFlags.IsLooping = true;
		}
		else
		{
			m_BitFlags.IsLooping = false;
		}

		m_TimeStopped = 0;
	}

	void audVoiceXAudio2::Shutdown()
	{
		if(m_SourceVoice)
		{
			m_SourceVoice->DestroyVoice();
			m_SourceVoice = NULL;
		}

		audVoicePhysical::Shutdown();
	}

	void audVoiceXAudio2::SetFrequencyScalingFactor(f32 scalingFactor)
	{
		Assert(m_SourceVoice);
		
		scalingFactor = Clamp(scalingFactor,0.f,4.f);
		//Special case for 0 (pause).
		if(scalingFactor == 0.f)
		{
			if(!m_BitFlags.IsPaused)
			{
				m_BitFlags.ShouldPlayOnUnpause = m_BitFlags.HasStartedVoice;

				if(m_BitFlags.ShouldPlayOnUnpause && m_SourceVoice && FAILED(m_SourceVoice->Stop(0, audDriverXenon::GetOperationSet())))
				{
					AssertMsg(0, "Failed to stop Voice playback\n");
				}
				else
				{
					m_BitFlags.IsPaused = true;
				}		
			}
		}
		else
		{
			// set frequency on the voice
			if(m_SourceVoice && FAILED(m_SourceVoice->SetFrequencyRatio(scalingFactor, audDriverXenon::GetOperationSet())))
			{
				AssertMsg(0, "Error calling SetPitch on Voice\n");
			}

			if(m_BitFlags.IsPaused)
			{
				if(m_BitFlags.ShouldPlayOnUnpause && m_SourceVoice)
				{
					if(FAILED(m_SourceVoice->Start(0,audDriverXenon::GetOperationSet())))
					{
						AssertMsg(0, "Failed to start Voice playback\n");
					}
					else
					{
						m_BitFlags.IsPaused = false;
						m_BitFlags.HasStartedVoice = true;
					}
				}		
				//Only apply attack when starting part-way through the sound.
			}
		}
	}

	void audVoiceXAudio2::Play(s32 startOffsetMs)
	{
		Assert(m_SourceVoice);

		if(!m_BitFlags.IsStreaming) //Sample data is added at a higher-level for streams.
		{
			AddSampleData(startOffsetMs);
		}

		m_StartOffsetMs = startOffsetMs;

		SetFrequencyScalingFactor(m_VoiceSettings->FrequencyScalingFactor);

		if(m_VoiceSettings->FrequencyScalingFactor == 0.0f)
		{
			m_BitFlags.IsPaused = true;
			m_BitFlags.ShouldPlayOnUnpause = true;
		}
		else
		{
			if(m_SourceVoice && FAILED(m_SourceVoice->Start(0, audDriverXenon::GetOperationSet())))
			{
				AssertMsg(0, "Failed to start Voice playback\n");
			}
			m_BitFlags.HasStartedVoice = true;
		}
	}

	void audVoiceXAudio2::StopPhysically(void)
	{
		Assert(m_SourceVoice);

		if(m_SourceVoice && FAILED(m_SourceVoice->Stop(0, audDriverXenon::GetOperationSet())))
		{
			AssertMsg(0, "Failed to stop Voice playback\n");
		}
		m_BitFlags.HasStartedVoice = false;
		if(m_TimeStopped == 0)
		{
			m_TimeStopped = g_AudioEngine.GetTimeInMilliseconds();
		}
		m_BitFlags.IsPaused = false;
		m_BitFlags.ShouldPlayOnUnpause = false;
	}	

	void audVoiceXAudio2::AddSampleData(u32 startOffsetMs)
	{
		const audWaveFormat *format = m_VoiceSettings->WaveRef.FindFormat();;
		Assert(format);

		if(startOffsetMs > 0)
		{
			Assert(!m_SubmittedFirstPacket);
			m_StartOffsetSamples = audDriverUtil::ConvertMsToSamples(startOffsetMs, m_BaseSampleRate);
			if(format->Format == audWaveFormat::kXMA2)
			{
				//Align to 128-samples.
				m_StartOffsetSamples = (m_StartOffsetSamples+127) & ~127;
			}
		}
		

		if(m_BitFlags.IsStreaming)
		{
			audStreamingWaveSlot *waveSlot = (audStreamingWaveSlot*)m_VoiceSettings->WaveRef.GetSlot();
			Assert(waveSlot);
			Assert(waveSlot->IsStreaming());

			audStreamingWaveBlock blockInfo;
			waveSlot->GetCurrentBlockInfo(m_VoiceSettings->WaveRef.GetObjectId(), blockInfo);

			if(blockInfo.metadata.NumPackets == 0)
			{
#if AUD_DEBUG_STREAMING
				//This voice is out of data.
				Displayf("wave %u end of stream", m_VoiceSettings->WaveRef.GetObjectId());
#endif
				m_SourceVoice->Discontinuity();
				return;
			}

#if AUD_DEBUG_STREAMING && 0
			XMA2PACKET *packet = (XMA2PACKET*)blockInfo.waveDataPtr;
			Displayf("XMA2: wave %u: block %u frame count: %d frame offset bits: %d metadata: %d skip count: %d", m_VoiceSettings->WaveRef.GetObjectId(),
				blockInfo.blockId,
				packet->FrameCount, packet->FrameOffsetInBits, packet->PacketMetaData, packet->PacketSkipCount);
#endif
			//Set-up Wave packet.
			m_WavePackets[m_WavePacketIndex].pAudioData = (BYTE*)(blockInfo.waveDataPtr);
			m_WavePackets[m_WavePacketIndex].AudioBytes	= blockInfo.metadata.NumPackets * 2048;

			Assert(m_SubmittedFirstPacket || m_StartOffsetSamples >= blockInfo.packetSeekTable[0].StartSample);
			const u32 bufferRelativeStartOffset = m_SubmittedFirstPacket ? 0 :
				(m_StartOffsetSamples >= blockInfo.packetSeekTable[0].StartSample ? m_StartOffsetSamples - blockInfo.packetSeekTable[0].StartSample : 0);

			const u32 sizeOfFirstPacket = blockInfo.packetSeekTable[1].StartSample - blockInfo.packetSeekTable[0].StartSample;

			// if we are playing this block to follow on from a previously submitted block we must skip the entire packet if SamplesToSkip is non-zero
			// since this means that the first packet is duplicated from the the previous block.  
			// If we are starting from this block (m_SubmittedFirstPacket==false) we must skip the actual number of samples in SamplesToSkip, 
			// since that gets us to the official starting point of the block, however as long as we calculate the buffer relative start
			// offset correctly we get that for free (since the start index in the seek table is the true start offset of this packet, which
			// will be prior to our requested start offset).
			// Loop begin is only necessary when we're looping back round; not on the first submitted packet
			const u32 samplesToSkip = 
				(blockInfo.blockId == 0 && m_SubmittedFirstPacket ? (format->LoopBegin*128) : 0) 
				+ bufferRelativeStartOffset 
				+ (m_SubmittedFirstPacket && blockInfo.metadata.SamplesToSkip ? sizeOfFirstPacket : 0);

			m_WavePackets[m_WavePacketIndex].PlayBegin = samplesToSkip;
			if(blockInfo.IsFinalBlock)
			{
				// Use the stored length in samples to trim the final packet
				const u32 trackLengthSamples = format->LengthSamples;
				const u32 trimmedBlockLength = trackLengthSamples - blockInfo.packetSeekTable[0].StartSample;
#if AUD_DEBUG_STREAMING
				Displayf("Final block, old length: %u, trimmedLength %u trackLength: %u (samples to skip %u)", blockInfo.metadata.NumSamples, trimmedBlockLength, trackLengthSamples, samplesToSkip);
#endif
				m_WavePackets[m_WavePacketIndex].PlayLength = trimmedBlockLength-samplesToSkip;
			}
			else
			{
				m_WavePackets[m_WavePacketIndex].PlayLength = blockInfo.metadata.NumSamples-samplesToSkip;
			}
			m_WavePackets[m_WavePacketIndex].LoopCount = m_WavePackets[m_WavePacketIndex].LoopBegin = m_WavePackets[m_WavePacketIndex].LoopLength = 0;

			Assert((m_WavePackets[m_WavePacketIndex].PlayBegin&127) == 0);
			Assert((m_WavePackets[m_WavePacketIndex].PlayLength&127) == 0);
			
			m_WavePackets[m_WavePacketIndex].Flags = blockInfo.IsFinalBlock ? XAUDIO2_END_OF_STREAM : 0;

			if(static_cast<s32>(m_WavePackets[m_WavePacketIndex].PlayLength) <= 0)
			{
				Assert(blockInfo.IsFinalBlock);
				// voice is out of data
				return;
			}

#if AUD_DEBUG_STREAMING

			Displayf("Begin: %d Length: %d",m_WavePackets[m_WavePacketIndex].PlayBegin,m_WavePackets[m_WavePacketIndex].PlayLength);

			if(blockInfo.IsFinalBlock)
			{
				Displayf("wave %u final block", m_VoiceSettings->WaveRef.GetObjectId());
			}		
#endif // AUD_DEBUG_STREAMING
		}
		else
		{
			u32 waveLengthBytes = 0;
			const void *waveData = m_VoiceSettings->WaveRef.FindWaveData(waveLengthBytes);
			if(format->Format == audWaveFormat::kXMA2)
			{
				if(((size_t)waveData) & 2047)
				{
					Errorf("Attempting to play non-aligned XMA2 data! %s\\%u",m_VoiceSettings->WaveRef.GetSlot()->GetLoadedBankName(), m_VoiceSettings->WaveRef.GetWaveNameHash());
					return;
				}
			}
			
			// allow start offset > length if this is a loop
			if(waveLengthBytes == 0 || (m_StartOffsetSamples >= m_WaveLengthSamples && !m_BitFlags.IsLooping))
			{
				//This voice is out of data.
				return;
			}
			//Set-up Wave packet.
			m_WavePackets[m_WavePacketIndex].pAudioData	= (BYTE*)waveData;
			m_WavePackets[m_WavePacketIndex].AudioBytes	= waveLengthBytes;
			m_WavePackets[m_WavePacketIndex].Flags = XAUDIO2_END_OF_STREAM;

				
			if(format->LoopPointSamples >= 0)
			{
				m_WavePackets[m_WavePacketIndex].LoopCount  = XAUDIO2_LOOP_INFINITE;
				if(format->Format == audWaveFormat::kXMA2)
				{
					// these fields are aligned to 128 samples, and are stored as val/128
					// so multiply by 128 to get back to the correct value.
					m_WavePackets[m_WavePacketIndex].LoopBegin = format->LoopBegin<<7;
					m_WavePackets[m_WavePacketIndex].LoopLength = (format->LoopEnd-format->LoopBegin)<<7;					

					// need to specify the play region with respect to the loop points
					if(m_StartOffsetSamples < (m_WavePackets[m_WavePacketIndex].LoopBegin+m_WavePackets[m_WavePacketIndex].LoopLength))
					{
						m_WavePackets[m_WavePacketIndex].PlayBegin = m_StartOffsetSamples;
						m_WavePackets[m_WavePacketIndex].PlayLength = m_WavePackets[m_WavePacketIndex].LoopBegin + m_WavePackets[m_WavePacketIndex].LoopLength - m_StartOffsetSamples;
						Assert(m_WavePackets[m_WavePacketIndex].PlayLength > 0);
					}
					else
					{
						// start offset greater than loop region; just loop from start.
						m_WavePackets[m_WavePacketIndex].PlayBegin = 0;
						m_WavePackets[m_WavePacketIndex].PlayLength = m_WavePackets[m_WavePacketIndex].LoopBegin + m_WavePackets[m_WavePacketIndex].LoopLength;
					}
				}
				else
				{
					// PCM, with loop
					m_WavePackets[m_WavePacketIndex].LoopBegin = format->LoopPointSamples;
					m_WavePackets[m_WavePacketIndex].LoopLength = m_WaveLengthSamples - format->LoopPointSamples;

					// need to specify the play region with respect to the loop points
					if(m_StartOffsetSamples < (m_WavePackets[m_WavePacketIndex].LoopBegin+m_WavePackets[m_WavePacketIndex].LoopLength))
					{
						m_WavePackets[m_WavePacketIndex].PlayBegin = m_StartOffsetSamples;
						m_WavePackets[m_WavePacketIndex].PlayLength = m_WavePackets[m_WavePacketIndex].LoopBegin + m_WavePackets[m_WavePacketIndex].LoopLength - m_StartOffsetSamples;
						Assert(m_WavePackets[m_WavePacketIndex].PlayLength > 0);
					}
					else
					{
						// start offset greater than loop region; just loop from start.
						m_WavePackets[m_WavePacketIndex].PlayBegin = 0;
						m_WavePackets[m_WavePacketIndex].PlayLength = m_WavePackets[m_WavePacketIndex].LoopBegin + m_WavePackets[m_WavePacketIndex].LoopLength;
					}					
				}
			}
			else
			{
				m_WavePackets[m_WavePacketIndex].LoopCount = m_WavePackets[m_WavePacketIndex].LoopBegin = m_WavePackets[m_WavePacketIndex].LoopLength = 0;

				if(format->Format == audWaveFormat::kXMA2)
				{
					m_WavePackets[m_WavePacketIndex].PlayBegin = Max<s32>(m_StartOffsetSamples,format->PlayBegin<<7);
					const u32 playBegin = format->PlayBegin<<7;
					const u32 playEnd = format->PlayEnd << 7;
					const u32 lengthSamples = playEnd - playBegin;
					if(m_WavePackets[m_WavePacketIndex].PlayBegin >= playEnd)
					{
						Assert(false);
						m_WavePackets[m_WavePacketIndex].PlayLength = 0;
						m_WavePackets[m_WavePacketIndex].PlayBegin = playEnd;
					}
					else
					{
						m_WavePackets[m_WavePacketIndex].PlayLength = lengthSamples - m_WavePackets[m_WavePacketIndex].PlayBegin;
					}
					if(!Verifyf(lengthSamples > 128, "XMA2 Encoded wave with play region of %d samples (needs to be  >128)",lengthSamples))
					{
						m_WavePackets[m_WavePacketIndex].PlayBegin = 0;
					}
				}
				else
				{
					m_WavePackets[m_WavePacketIndex].PlayBegin = m_StartOffsetSamples;
					m_WavePackets[m_WavePacketIndex].PlayLength = m_WaveLengthSamples - m_StartOffsetSamples;
				}

			}
		}

		Assert(m_SourceVoice);
		
		//Submit Wave packet.
		if(m_SourceVoice && FAILED(m_SourceVoice->SubmitSourceBuffer(&m_WavePackets[m_WavePacketIndex])))
		{
			AssertMsg(0, "Error calling SubmitSourceBuffer on Voice\n");
		}

		m_SubmittedFirstPacket = true;
		m_WavePacketIndex = (m_WavePacketIndex + 1) % audStreamingWaveSlot::kNumStreamingBlocksPerWaveSlot;
	}

	s32 audVoiceXAudio2::GetPlayTimeMs(void)
	{
		s32 playTimeMs;
		const audWaveFormat *format = m_VoiceSettings->WaveRef.FindFormat();

		if(IsPlaying())
		{
			playTimeMs = (s32)audDriverUtil::ConvertSamplesToMs((f32)GetPlaybackPositionSamples(),(f32)m_BaseSampleRate);

			if((u32)playTimeMs > m_WaveLengthMs)
			{
				if(format->LoopPointSamples == -1)
				{
					if(!m_BitFlags.IsStreaming)
					{
						if(m_SourceVoice && FAILED(m_SourceVoice->Stop(0,audDriverXenon::GetOperationSet())))
						{
							AssertMsg(0, "Failed to stop Voice playback\n");
						}
						playTimeMs = FINISHED_PLAYBACK;
					}
				}
				else
				{
					const s32 preLoopLength = (s32)audDriverUtil::ConvertSamplesToMs((f32)format->LoopPointSamples,(f32)m_BaseSampleRate);
					const s32 loopLength = Max((s32)1, (s32)(m_WaveLengthMs-preLoopLength));
					playTimeMs = ((playTimeMs - preLoopLength) % loopLength) + preLoopLength;
				}
			}
		}
		else
		{
			playTimeMs = FINISHED_PLAYBACK;
			if(!m_VoiceSettings->Flags.ShouldStop && m_BitFlags.IsLooping)
			{
				Assertf(m_VoiceSettings->Flags.ShouldStop || !m_BitFlags.IsLooping, "XAudio looping voice stopped prematurely: %s %u", m_VoiceSettings->WaveRef.GetSlot()->GetLoadedBankName(), m_VoiceSettings->WaveRef.GetWaveNameHash());
			}
		}

		//Nastily check that this voice has not been frozen at a fixed (unpaused) play time, as XAUDIO very rarely silently fails to complete
		//playback of an XMA packet.
		if(playTimeMs > 0 && !m_BitFlags.IsPaused)
		{
			if(playTimeMs == m_PreviousPlayTimeMs)
			{
				m_NumFramesFrozen++;
				if(m_NumFramesFrozen >= g_ThresholdFramesFrozenToStopVoice)
				{
					const u32 waveNameHash = m_VoiceSettings->WaveRef.GetWaveNameHash();
					const char *bankName = m_VoiceSettings->WaveRef.GetSlot()->GetLoadedBankName();

					const audWaveFormat *format = m_VoiceSettings->WaveRef.FindFormat();
					//Assertf(0, "An XAudio2 voice has frozen playing wave name hash: %u from bank: %s (startOffset: %d, playtime: %d)", waveNameHash, bankName, m_StartOffsetMs, playTimeMs);
					Errorf("An XAudio2 voice has frozen playing wave name hash: %u from bank: %s (startOffset: %d, playtime: %d, length ms %d (%d samples) object id %d)", waveNameHash, bankName, m_StartOffsetMs, playTimeMs, audDriverUtil::ConvertSamplesToMs((f32)format->LengthSamples, (f32)format->SampleRate), format->LengthSamples, m_VoiceSettings->WaveRef.GetObjectId());
					Errorf("PlayBegin: %d PlayEnd: %d LoopBegin: %d LoopEnd: %d", 128*format->PlayBegin, 128*format->PlayEnd, 128*format->LoopBegin, 128*format->LoopEnd);
					const u32 indexMinusOne = (m_WavePacketIndex+1)%audStreamingWaveSlot::kNumStreamingBlocksPerWaveSlot;
					Errorf("Submitted packet: PlayBegin: %u, PlayLength %u, LoopBegin: %u, LoopLength: %u", m_WavePackets[indexMinusOne].PlayBegin,  m_WavePackets[indexMinusOne].PlayLength,  m_WavePackets[indexMinusOne].LoopBegin,  m_WavePackets[indexMinusOne].LoopLength);

					//This voice has been frozen at the same play time for several frames whilst not paused, so force it to stop as XAUDIO is stuck.
					if(m_SourceVoice && FAILED(m_SourceVoice->Stop(0, audDriverXenon::GetOperationSet())))
					{
						AssertMsg(0, "Failed to stop Voice playback");
					}
					return FINISHED_PLAYBACK;
				}
			}
			else
			{
				m_NumFramesFrozen = 0;
			}
		}
		else
		{
			m_NumFramesFrozen = 0;
		}

		m_PreviousPlayTimeMs = playTimeMs;

		return playTimeMs;
	}

	u32 audVoiceXAudio2::GetPlaybackPositionSamples(void)
	{
		XAUDIO2_VOICE_STATE voiceState;
		Assert(m_SourceVoice);
		
		m_SourceVoice->GetState(&voiceState);

		// check to see if this voice has finished playing all queued buffers
		if(voiceState.BuffersQueued == 0)
		{
			m_BitFlags.HasStartedVoice = false;
		}

		// TODO: compute buffer position based on loop info		
		return m_StartOffsetSamples + (u32)voiceState.SamplesPlayed;
	}

	void audVoiceXAudio2::Update(void)
	{
		Assert(m_SourceVoice);

		if(m_SourceVoice)
		{
			SetFrequencyScalingFactor(m_VoiceSettings->FrequencyScalingFactor);
			ApplyHardwareVolumes();

			
			XAUDIO2_VOICE_STATE state;
			m_SourceVoice->GetState(&state);
			if(state.BuffersQueued <= 1)
			{
				m_NeedsNewData = true;
			}
			else
			{
				m_NeedsNewData = false;
			}

			//	Flag at the end of the function
			m_BitFlags.HasVoiceUpdated = true;
		}
	}

#if __BANK
	extern u32 g_OverrideHPFCutoff;
	extern u32 g_OverrideLPFCutoff;
#endif

	void audVoiceXAudio2::ApplyHardwareVolumes()
	{
		const f32 hardwareVol = m_VoiceSettings->Flags.ShouldMuteOnUserMusic && audDriver::GetVoiceManager().GetIsUserMusicPlaying() ? 0.0f : m_VoiceSettings->PostSubmixVolumeAttenuation.GetLinear();
		const f32 requestedVolHeadroomComp = m_VoiceSettings->RequestedVolume.GetLinear()*g_HeadroomOffsetCompensationLin;
		const f32 dryMix = m_VoiceSettings->SourceEffectDryMix.GetLinear()*requestedVolHeadroomComp; // 2dB offset to compensate for headroom adjustment in EnvSound
		const f32 wetMix = m_VoiceSettings->SourceEffectWetMix.GetLinear()*requestedVolHeadroomComp; // 2dB offset to compensate for headroom adjustment in EnvSound


		f32 lpfCutoff = (f32)m_VoiceSettings->LPFCutoff;
		u32 hpfCutoff = m_VoiceSettings->HPFCutoff;
#if __BANK
		lpfCutoff = Min(lpfCutoff, (f32)g_OverrideLPFCutoff);
		hpfCutoff = Max(hpfCutoff, g_OverrideHPFCutoff);
#endif

		// LPF - dont smooth cutoff if the dry + wet is silent
		const bool isDrySilent = ((hardwareVol*dryMix) <= g_SilenceVolumeLin);
		const bool isWetSilent = ((hardwareVol*wetMix) <= g_SilenceVolumeLin);
		const bool shouldSmoothVolume = ShouldSmoothVolume();

		const bool shouldSmoothCutoff = (shouldSmoothVolume && !(isDrySilent && isWetSilent));

		tSourceFilterEffectSettings sourceFilterSettings;
		m_LastFilterCutoff = audDriverUtil::SmoothCutoff(lpfCutoff, m_LastFilterCutoff, shouldSmoothCutoff);
		sourceFilterSettings.lpfCutoff = (u32)m_LastFilterCutoff;

		sourceFilterSettings.hpfCutoff = hpfCutoff;
		m_SourceVoice->SetEffectParameters(0,&sourceFilterSettings,sizeof(sourceFilterSettings),audDriverXenon::GetOperationSet());

		// Calculate our new channel volumes
		for (u32 i=0, reverbIndex = 0; i<g_MaxOutputChannels; i++)
		{
			f32 newDryChannelVolume = 0.0f;
			f32 newWetChannelVolume[g_NumSourceReverbs];
			for(u32 j = 0 ; j < g_NumSourceReverbs; j++)
			{
				newWetChannelVolume[j] = 0.0f;
			}
			// Initialise the new ones to our desired volumes
			newDryChannelVolume = hardwareVol * m_VoiceSettings->RelativeDryChannelVolumes[i].GetLinear();
			if(i != RAGE_SPEAKER_ID_FRONT_CENTER && i != RAGE_SPEAKER_ID_LOW_FREQUENCY)
			{
				for(u32 j = 0 ; j < g_NumSourceReverbs; j++)
				{
					newWetChannelVolume[j] = hardwareVol * m_VoiceSettings->RelativeWetChannelVolumes[j][reverbIndex].GetLinear();
				}
				reverbIndex++;
			}
			// Now smooth it
			m_DryChannelVolumes[i] = audDriverUtil::SmoothVolume(newDryChannelVolume, m_DryChannelVolumes[i], shouldSmoothVolume);	
			for(u32 j = 0 ; j < g_NumSourceReverbs; j++)
			{
				m_WetChannelVolumes[i][j] = audDriverUtil::SmoothVolume(newWetChannelVolume[j], m_WetChannelVolumes[i][j], shouldSmoothVolume);	
			}
		}

		if (g_CollapseRearsToFront)
		{
			for(u32 j = 0 ; j < g_NumSourceReverbs; j++)
			{
				m_WetChannelVolumes[0][j] = sqrt(m_WetChannelVolumes[0][j]*m_WetChannelVolumes[0][j] + m_WetChannelVolumes[4][j]*m_WetChannelVolumes[4][j]);	
				m_WetChannelVolumes[1][j] = sqrt(m_WetChannelVolumes[1][j]*m_WetChannelVolumes[1][j] + m_WetChannelVolumes[5][j]*m_WetChannelVolumes[5][j]);	
				m_WetChannelVolumes[4][j] = 0.0f;	
				m_WetChannelVolumes[5][j] = 0.0f;	
			}
		}


		f32 gainMatrix[6];

		switch(m_VoiceSettings->EffectRoute)
		{
		case EFFECT_ROUTE_MUSIC:
		case EFFECT_ROUTE_FRONT_END:
			for(u32 i=0; i<6; i++)
			{
				gainMatrix[i] = dryMix * m_DryChannelVolumes[i];
			}

			m_SourceVoice->SetOutputMatrix(NULL, 1, 6, &gainMatrix[0], audDriverXenon::GetOperationSet());
			break;

			//case EFFECT_ROUTE_POSITIONED:
		default:

			for(u32 i=0; i<6; i++)
			{
				gainMatrix[i] = dryMix * m_DryChannelVolumes[i];
			}

			m_SourceVoice->SetOutputMatrix((IXAudio2Voice*)g_AudioEngine.GetEffectManager().GetRouteEffectChain(AUD_PRE_LISTENER_EFFECT_CHAIN)->GetPhysicalSubmix(),
				1,
				6,
				&gainMatrix[0],
				audDriverXenon::GetOperationSet());

			for(u32 i=0; i<g_NumSourceReverbs; i++)
			{
				for(u32 i2=0; i2<6; i2++)
				{
					gainMatrix[i2] = dryMix * m_WetChannelVolumes[i2][i];
				}
				m_SourceVoice->SetOutputMatrix((IXAudio2Voice*)g_AudioEngine.GetEffectManager().GetRouteEffectChain(AUD_SOURCE_REVERB_SMALL_EFFECT_CHAIN + i)->GetPhysicalSubmix(),
					1,
					6,
					&gainMatrix[0],
					audDriverXenon::GetOperationSet());

			}
			break;
		}

		if(m_SourceEffectSubmixVoice)
		{
			if(m_VoiceSettings->Flags.ShouldControlSubmix)
			{
				//We are routing a single mono channel to a source effect submix voice, so we need to set the positional
				//channel volumes on the submix.
				Assert(m_VoiceSettings->EffectRoute == EFFECT_ROUTE_POSITIONED);
				m_SourceEffectSubmixVoice->SetOutputMatrix((IXAudio2Voice*)g_AudioEngine.GetEffectManager().GetRouteEffectChain(AUD_PRE_LISTENER_EFFECT_CHAIN)->GetPhysicalSubmix(),
					1,
					6,
					&m_DryChannelVolumes[0],
					audDriverXenon::GetOperationSet());

				for(u32 i=0; i<g_NumSourceReverbs; i++)
				{

					m_SourceVoice->SetOutputMatrix((IXAudio2Voice*)g_AudioEngine.GetEffectManager().GetRouteEffectChain(AUD_SOURCE_REVERB_SMALL_EFFECT_CHAIN + i)->GetPhysicalSubmix(),
						1,
						6,
						&m_WetChannelVolumes[i][0],
						audDriverXenon::GetOperationSet());

				}

			}

			//Add the wet route volumes to the existing bypass/dry route volumes.
			gainMatrix[0] = wetMix;
			m_SourceVoice->SetOutputMatrix(m_SourceEffectSubmixVoice,
				1,
				1,
				&gainMatrix[0],
				audDriverXenon::GetOperationSet());
		}
	}

	u32 audVoiceXAudio2::ConvertMsToBytes(u32 numMilliseconds)
	{
		u32 numBytes = 2 * audDriverUtil::ConvertMsToSamples(numMilliseconds, m_BaseSampleRate);
		return numBytes;
	}

	void audVoiceXAudio2::ComputePacketOffsetBitsAndSampleIndexFromSamples(u32 sampleOffset, u32 &frameOffsetBits, s32 &sampleIndex, const u32 seekTableEntries, const u32 *seekTable, const u8 *waveData, const u32 ASSERT_ONLY(waveLengthBytes))
	{
		frameOffsetBits = 0;
		sampleIndex = -1;

		if(sampleOffset < *seekTable)
		{
			frameOffsetBits = 32;
			sampleIndex = 0;
			Warningf("Sample offset is prior to current seek table");
			return;
		}

		//Choose the last XMA packet if the sample offset cannot be found within the limits of the other packets in the seek
		//table.
		s32 tableIndex = seekTableEntries - 1;
		for(u32 index=0; index<seekTableEntries - 1; index++)
		{
			if((sampleOffset >= seekTable[index]) &&
				(sampleOffset < seekTable[index + 1]))
			{
				tableIndex = index;
				break;
			}
		}

		//Found the required XMA packet.
		u32 firstPacketSample = seekTable[tableIndex];
		u32 packetOffsetBytes = tableIndex << 11;

		Assert(packetOffsetBytes <= waveLengthBytes);

		//Parse the packet header for the bit offset of the first frame.
		const void *packetData = waveData + packetOffsetBytes;
		u32 frameSeekOffsetBits = ((*((u32 *)packetData) >> 11) & 0x7FFF) + 32;
		u32 frameSeekOffsetBytes = (u32)floorf((f32)frameSeekOffsetBits / 8.0f);

		bool isFound = false;
		u32 frameSizeBitsUnaligned, frameSizeBits, firstFrameSample;
		u32 frameIndex = 0;
		while(!isFound)
		{
			//Parse frame header for the frame size (in bits.)
			frameSizeBitsUnaligned = *((u32 *)(((u8 *)packetData) + frameSeekOffsetBytes));
			frameSizeBits = (frameSizeBitsUnaligned >> (17 - (frameSeekOffsetBits % 8))) & 0x7FFF;

			if(frameSizeBits == 0)
			{
				break;
			}

			firstFrameSample = firstPacketSample + (frameIndex * 512);
			if((sampleOffset >= firstFrameSample) && (sampleOffset < firstFrameSample + 512))
			{
				//Found the required XMA frame.
				frameOffsetBits = (packetOffsetBytes << 3) + frameSeekOffsetBits;
				sampleIndex = sampleOffset - firstFrameSample;

				isFound = true;
			}
			else
			{
				frameSeekOffsetBits += frameSizeBits;
				frameSeekOffsetBytes = (u32)floorf((f32)frameSeekOffsetBits / 8.0f);
				frameIndex++;
			}
		}
	}


}	// namespace rage

#endif // __XENON

