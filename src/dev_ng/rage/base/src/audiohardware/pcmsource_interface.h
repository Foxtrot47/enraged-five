#ifndef AUD_PCMSOURCE_INTERFACE_H
#define AUD_PCMSOURCE_INTERFACE_H

#include "cmdbuffer.h"

#include "vectormath/vectortypes.h"

namespace rage
{
	struct audPcmSourceCustomCommandPacket;

	enum audPcmSourceTypeId
	{
		AUD_PCMSOURCE_WAVEPLAYER = 0,
		AUD_PCMSOURCE_GRAINPLAYER,
		AUD_PCMSOURCE_MODULARSYNTH,
		AUD_PCMSOURCE_SUBMIX,
		AUD_PCMSOURCE_SYNTHCORE,
		AUD_PCMSOURCE_EXTERNALSTREAM,

		AUD_NUM_PCMSOURCES
	};

	struct PcmSourceState
	{		
		u32		LengthSamples;  
		u32		PlaytimeSamples;
		u32		UpdateMixerFrame;
		u16		CurrentPeakLevel;
		u16		padding;

		u32		AuthoredSampleRate	: 16;
		u32		NumChannels			: 2;
		u32		isLooping			: 1;
		u32		hasStartedPlayback	: 1;

	};

	class audPcmSourceInterface
	{
	public:
		enum audSourceCommandIds
		{
			INITIALISE = 0,
			SET_PCM_SOURCE_PARAM_F32,
			SET_PCM_SOURCE_PARAM_U32,
			PCM_SOURCE_CUSTOM_COMMAND_PACKET,
			START_PCM_SOURCE,
			STOP_AND_RELEASE,
		};

		static s32 Allocate(const audPcmSourceTypeId typeId);

		// PURPOSE
		//	Sets a PCM generator parameter asynchronously via the active command buffer
		static bool SetParam(const u32 pcmSourceId, const u32 paramId, const u32 val);
		static bool SetParam(const u32 pcmSourceId, const u32 paramId, const f32 val);
		static bool WriteCustomCommandPacket(const u32 pcmSourceID, const u32 paramId, audPcmSourceCustomCommandPacket *command);

		static void AddRef(const s32 pcmSourceId);
		static void Release(const s32 pcmSourceId);

		static void Start(const s32 pcmSourceId);
		static void StopAndRelease(const s32 pcmSourceId);

		static bool GetState(PcmSourceState& pcmSourceState, const s32 pcmSourceId);

		static bool SetSubmixOutputVolumes(const s32 submixId, const u32 outputIndex, Vec::Vector_4V_In packedVolumes);

	private:
		static bool WriteGenericCommand(const s32 pcGeneratorId, const u32 commandId);
		static bool WriteCommand(const audCommandPacketHeader *packet);
		static bool WriteAlignedCommand(const audCommandPacketHeader *packet);
	};

	struct audSetPcmSourceU32ParamCommand : public audCommandPacketHeader
	{
		audSetPcmSourceU32ParamCommand()
		{
			packetSize = sizeof(audSetPcmSourceU32ParamCommand);
			commandId = audPcmSourceInterface::SET_PCM_SOURCE_PARAM_U32;
		}
		u32 paramId;
		u32 value;
	};

	struct audSetPcmSourceF32ParamCommand : public audCommandPacketHeader
	{
		audSetPcmSourceF32ParamCommand()		
		{
			packetSize = sizeof(audSetPcmSourceF32ParamCommand);
			commandId = audPcmSourceInterface::SET_PCM_SOURCE_PARAM_F32;
		}
		u32 paramId;
		f32 value;
	};

	struct audPcmSourceCustomCommandPacket : public audCommandPacketHeader
	{
		audPcmSourceCustomCommandPacket()		
		{
			commandId = audPcmSourceInterface::PCM_SOURCE_CUSTOM_COMMAND_PACKET;
		}
		u32 paramId;
	};
}

#endif // AUD_PCMSOURCE_INTERFACE_H
