//
// audiohardware/decoder_adpcm.cpp
//
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved.
//

#include "driverdefs.h"
#if RSG_AUDIO_X64

#include "decoder_adpcm.h"
#include "decodemgr.h"
#include "mixer.h"

#include "system/endian.h"
#include "vectormath/vectormath.h"

//OPTIMISATIONS_OFF()

namespace rage
{
	s32 audDecoderAdpcm::sm_DecodesThisFrame = 0;

	using namespace Vec;
	bool audDecoderAdpcm::Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate))
	{
		m_NumChannels = numChannels;
		m_Format = format;
		m_State = audDecoder::IDLE;

		m_Adpcm.PredictedValue = 0;
		m_Adpcm.StepIndex = 0;
		m_SubmitPacketIndex = 0;
		m_DecodePacketIndex = 0;
		m_NumQueuedPackets = 0;

		m_DecodedLastFrame = false;
		m_ReadyForMoreData = true;
		m_SkippedDecodeCount = 0;

#if DUMP_RAW_PC_STREAMS
		static int filenameCount = 0;
		char filename[256];
		sprintf(filename, "test%d.raw", filenameCount);
		m_fp = fopen(filename, "wb");
		filenameCount++;
#endif

		return true;
	}

	void audDecoderAdpcm::Shutdown()
	{
#if DUMP_RAW_PC_STREAMS
		if(m_fp)
			fclose(m_fp);
#endif
	}

	void audDecoderAdpcm::SubmitPacket(audPacket &packet)
	{
		m_Packets[m_SubmitPacketIndex].InputBytes = packet.inputBytes;
		m_Packets[m_SubmitPacketIndex].InputData = (u8*)packet.inputData;
		m_Packets[m_SubmitPacketIndex].NumADPCMBytesConsumed = 0;
		m_Packets[m_SubmitPacketIndex].NumSamplesConsumed = 0;
		m_Packets[m_SubmitPacketIndex].PlayBegin = packet.PlayBegin;
		m_Packets[m_SubmitPacketIndex].PlayEnd = packet.PlayEnd;
		m_Packets[m_SubmitPacketIndex].IsFinalBlock = packet.IsFinalBlock;
		m_Packets[m_SubmitPacketIndex].NumBlocksAvailable = packet.inputBytes/kAdpcmBlockSize;
		m_Packets[m_SubmitPacketIndex].LoopBeginSamples = packet.LoopBegin;

//#if RSG_ORBIS
//		static int lame;
//		if (*(u8*)packet.inputData == 255)		// cheesy hack to validate input data
//			++lame;
//#endif

		// check if we have duplicated a packets and need to skip ones
		if(packet.PlayBegin > 0)
		{
			// calculate number of packets to skip
			u32 packetsToSkip = packet.PlayBegin / kAdpcmDecompressedSamplesPerBlock;
			m_Packets[m_SubmitPacketIndex].NumADPCMBytesConsumed = packetsToSkip*kAdpcmBlockSize;
			m_Packets[m_SubmitPacketIndex].NumSamplesConsumed = packetsToSkip*kAdpcmDecompressedSamplesPerBlock;
			m_Packets[m_SubmitPacketIndex].NumBlocksAvailable -= packetsToSkip;
		}

		//Displayf("SubmitPacket %d: bytes:%d blocks:%d skip:%d", m_SubmitPacketIndex, m_Packets[m_DecodePacketIndex].InputBytes, m_Packets[m_DecodePacketIndex].NumBlocksAvailable, packet.PlayBegin);

		m_SubmitPacketIndex = (m_SubmitPacketIndex + 1) % m_Packets.size();
		if(m_SubmitPacketIndex == m_DecodePacketIndex)
		{
			m_ReadyForMoreData = false;
		}
		
		m_NumQueuedPackets++;
		
		m_State = audDecoder::DECODING;
	}

	void audDecoderAdpcm::Update()
	{
		if(audDecoderAdpcm::sm_DecodesThisFrame > kMaxDecodesPerFrame && m_SkippedDecodeCount < kMaxSkippedDecodeCount)
		{
			m_SkippedDecodeCount++;
			return;
		}
		if(m_DecodedLastFrame && audDecoderAdpcm::sm_DecodesThisFrame > kMaxSkippedDecodeCount && m_SkippedDecodeCount < kMaxSkippedDecodeCount)
		{
			m_SkippedDecodeCount++;
			m_DecodedLastFrame = false;
			return;
		}
		if(m_State == FINISHED)
		{
			return;
		}

		if(m_Packets[m_DecodePacketIndex].LoopBeginSamples == ~0u && m_Packets[m_DecodePacketIndex].NumSamplesConsumed >= m_Packets[m_DecodePacketIndex].PlayEnd)
		{
			if(m_NumQueuedPackets > 0)
				m_NumQueuedPackets--;

			if(m_NumQueuedPackets <= 0 && m_Packets[m_DecodePacketIndex].IsFinalBlock)
			{
				m_State = FINISHED;
				m_ReadyForMoreData = true;
			}
			else
			{
				m_DecodePacketIndex = (m_DecodePacketIndex + 1) % m_Packets.size();
				m_ReadyForMoreData = ((m_DecodePacketIndex != m_SubmitPacketIndex) || m_State != DECODING);	
			}
			return;
		}

		m_ReadyForMoreData = ((m_DecodePacketIndex != m_SubmitPacketIndex) || m_State != DECODING);	

		const u32 ringBufferFreeSpace = (m_RingBuffer.GetBufferSize() - m_RingBuffer.GetBytesAvailableToRead());// >> 1;

		// see if there is enough free space in the ring buffer to decode the next block
		if(ringBufferFreeSpace > kAdpcmDecompressedBytesPerBlock)
		{
			// if we have at least 1 block worth of data to decode and we have some data left
			if(m_Packets[m_DecodePacketIndex].NumSamplesConsumed < m_Packets[m_DecodePacketIndex].PlayEnd)
			{
				//Displayf("Decode %d:blocks:%d", m_DecodePacketIndex, m_Packets[m_DecodePacketIndex].NumBlocksAvailable);
				static u8 s_DecodeBuffer[kAdpcmDecompressedBytesPerBlock];
				
				sysMemSet(s_DecodeBuffer, 0, kAdpcmDecompressedBytesPerBlock);

				s16* blockStart = (s16*)((u8*)m_Packets[m_DecodePacketIndex].InputData+m_Packets[m_DecodePacketIndex].NumADPCMBytesConsumed);
				m_Adpcm.StepIndex = (u8)blockStart[0];
				m_Adpcm.PredictedValue = (s16)blockStart[1];

				u8* srcData = (u8*)blockStart;
				srcData += 4;
				m_Adpcm.Decode((s16*)&s_DecodeBuffer, srcData, 0, kAdpcmBlockBytesToDecode);

				audDecoderAdpcm::sm_DecodesThisFrame++;
				m_DecodedLastFrame = true;

				u32 packetSizeBytes = kAdpcmDecompressedBytesPerBlock;
				// check if we are on the last packet as it may not be a full packet
				if(m_Packets[m_DecodePacketIndex].NumSamplesConsumed + kAdpcmDecompressedSamplesPerBlock > m_Packets[m_DecodePacketIndex].PlayEnd)
				{
					u32 blocks = m_Packets[m_DecodePacketIndex].PlayEnd / kAdpcmDecompressedSamplesPerBlock;
					u32 fullPacketBytes = blocks * kAdpcmDecompressedBytesPerBlock;
					packetSizeBytes = (m_Packets[m_DecodePacketIndex].PlayEnd*2) - fullPacketBytes;
					//audDisplayf("Last Packet Size = %u", packetSizeBytes);
				}
				m_RingBuffer.WriteData(s_DecodeBuffer, packetSizeBytes);

#if DUMP_RAW_PC_STREAMS
				if(m_fp)
				{
					s16* sample = (s16*)s_DecodeBuffer;
					for(u32 i=0; i<packetSizeBytes/2; i++)
					{
						fwrite(sample, 2, 1, m_fp);
						sample++;
					}
				}
#endif

				// Throw away any samples before the play begin point
				if(m_Packets[m_DecodePacketIndex].NumSamplesConsumed < m_Packets[m_DecodePacketIndex].PlayBegin)
				{					
					u32 samplesToSkip = m_Packets[m_DecodePacketIndex].PlayBegin - m_Packets[m_DecodePacketIndex].NumSamplesConsumed;

					if(audVerifyf(samplesToSkip < kAdpcmDecompressedSamplesPerBlock, "Play begin isn't within the current block!"))
					{
						AdvanceReadPtr(samplesToSkip);
					}					
				}

				m_Packets[m_DecodePacketIndex].NumADPCMBytesConsumed += kAdpcmBlockSize;
				m_Packets[m_DecodePacketIndex].NumSamplesConsumed += (packetSizeBytes/2);
				m_Packets[m_DecodePacketIndex].NumBlocksAvailable--;

			}
			// Loop logic - just set the packet back to the appropriate start sample as done in SubmitPacket
			else if(m_Packets[m_DecodePacketIndex].LoopBeginSamples != ~0u)
			{
				m_Packets[m_DecodePacketIndex].NumADPCMBytesConsumed = 0;
				m_Packets[m_DecodePacketIndex].NumSamplesConsumed = 0;
				m_Packets[m_DecodePacketIndex].NumBlocksAvailable = m_Packets[m_DecodePacketIndex].InputBytes/kAdpcmBlockSize;
				m_Packets[m_DecodePacketIndex].PlayBegin = m_Packets[m_DecodePacketIndex].LoopBeginSamples;

				if(m_Packets[m_DecodePacketIndex].LoopBeginSamples > 0)
				{
					u32 packetsToSkip = m_Packets[m_DecodePacketIndex].LoopBeginSamples / kAdpcmDecompressedSamplesPerBlock;
					m_Packets[m_SubmitPacketIndex].NumADPCMBytesConsumed = packetsToSkip*kAdpcmBlockSize;
					m_Packets[m_SubmitPacketIndex].NumSamplesConsumed = packetsToSkip*kAdpcmDecompressedSamplesPerBlock;
					m_Packets[m_SubmitPacketIndex].NumBlocksAvailable -= packetsToSkip;
				}
			}
		}
	}

	u32 audDecoderAdpcm::QueryNumAvailableSamples() const
	{
		return (m_RingBuffer.GetBytesAvailableToRead()>>1);
	}

	bool audDecoderAdpcm::QueryReadyForMoreData() const
	{
		return m_ReadyForMoreData;
	}

	u32 audDecoderAdpcm::ReadData(s16 *buf, const u32 numSamples) const
	{
		return m_RingBuffer.PeakData(buf, numSamples<<1)>>1;
	}

	void audDecoderAdpcm::AdvanceReadPtr(const u32 numSamples)
	{
		m_RingBuffer.AdvanceReadPtr(numSamples<<1);
	}

} // namespace rage

#endif // RSG_AUDIO_ADPCM