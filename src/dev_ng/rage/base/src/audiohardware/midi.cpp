
#include "audiohardware/channel.h"
#include "midi.h"

#if __WIN32PC
#include "mmsystem.h"
#endif // __WIN32PC

namespace rage
{

	audMidiIn::audMidiIn()
	{

	}

	audMidiIn::~audMidiIn()
	{

	}

	s32 audMidiIn::GetNumDevices()
	{
#if __WIN32PC
		return static_cast<s32>(midiInGetNumDevs());
#else
		return 0;
#endif
	}

	bool audMidiIn::Init(const s32 WIN32PC_ONLY(deviceId))
	{
		m_WriteQueueId = 0;

#if __WIN32PC
		m_Handle = NULL;

		formatf(m_Name, "No Device (%d)", deviceId);
		if(deviceId >= GetNumDevices())
		{
			return false;
		}
		if(SUCCEEDED(midiInOpen(&m_Handle, static_cast<UINT>(deviceId), reinterpret_cast<DWORD_PTR>(&MidiInProc), reinterpret_cast<DWORD_PTR>(this), CALLBACK_FUNCTION)))
		{
			MIDIINCAPS caps;
			if(FAILED(midiInGetDevCaps(deviceId, &caps, sizeof(caps))))
			{
				return false;
			}
			else
			{
				formatf(m_Name, "%s", caps.szPname);
				m_ManufacturerId = caps.wMid;
				m_ProductId = caps.wPid;
				if(FAILED(midiInStart(m_Handle)))
				{
					return false;
				}
			}
			return true;
		}
#endif // __WIN32PC

		return false;
	}

	void audMidiIn::Shutdown()
	{
#if __WIN32PC
		if (m_Handle)
		{
			midiInClose(m_Handle);
			m_Handle = NULL;
		}
#endif
	}

	const char *audMidiIn::GetName() const
	{
#if __WIN32PC
		return m_Name;
#else
		return NULL;
#endif
	}

	void audMidiIn::AddMessageToQueue(const audMidiMessage &message)
	{
		SYS_CS_SYNC(m_Lock);
		if(m_MessageBuffer[m_WriteQueueId].GetCount() < m_MessageBuffer[m_WriteQueueId].GetMaxCount())
		{
			m_MessageBuffer[m_WriteQueueId].Append() = message;
		}
		else
		{
			audWarningf("Dropped midi message %08X", message.Message);
		}
	}

	bool audMidiIn::ProcessQueue(MidiMessageFunctor callback, const u32 channel, EmptyQueueFlag emptyQueue)
	{
		SYS_CS_SYNC(m_Lock);
		const u32 readQueueId = (1+m_WriteQueueId)&1;
		if(m_MessageBuffer[readQueueId].GetCount() == 0)
		{
			return false;
		}
		bool invoked = false;
		for(atArray<audMidiMessage>::const_iterator iter = m_MessageBuffer[readQueueId].begin(); iter != m_MessageBuffer[readQueueId].end(); iter++)
		{
			if(channel == 0 || ((iter->Message&0x0f)+1) == channel)
			{
				callback.Invoke(*iter);
				invoked = true;
			}
		}
		if(emptyQueue == kEmptyQueue)
		{
			m_MessageBuffer[readQueueId].Reset();
		}
		return invoked;
	}

	void audMidiIn::EmptyQueue()
	{
		// NOTE: this is temporary; will miss messages since they could arrive between the ProcessQ and EmptyQ calls
		// Need to double buffer
		SYS_CS_SYNC(m_Lock);
		const u32 readQueueId = (1+m_WriteQueueId)&1;
		m_MessageBuffer[readQueueId].Reset();
	}

	void audMidiIn::SwapQueueBuffer()
	{
		SYS_CS_SYNC(m_Lock);
		m_WriteQueueId = (1+m_WriteQueueId)&1;
	}

#if __WIN32PC
	void audMidiIn::MidiInProc(HMIDIIN UNUSED_PARAM(HMIDIIN), UINT wMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2)
	{
		audMidiMessage message;
		if(wMsg == MIM_DATA)
		{
			message.Message = (u32)dwParam1;
			message.Timestamp = (u32)dwParam2;
		}
		((audMidiIn*)dwInstance)->AddMessageToQueue(message);
	}
#endif // __WIN32PC

}
