// 
// audiohardware/voicefilterjob.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/taskheader.h"

#if __SPU

#include "mixer.h"
#include "voicefilterjob.h"

#include "math/amath.h"
#include "voicefilter_vmath.inl"

#include "system/dma.h"

namespace rage {

	void VoiceFilterJobEntry(sysTaskParameters &p)
	{
		const bool isLPFEnabled = p.UserData[0].asBool;
		const bool isHPFEnabled = p.UserData[1].asBool;
		const u32 numVoices = p.UserData[2].asUInt;
		const void *lpfHistoryAndVoiceBufferEA = p.UserData[3].asPtr;

		Assert(p.Input.Size == numVoices*2*sizeof(u32));
		u32 *lpfCutoffs = (u32*)p.Input.Data;
		u32 *hpfCutoffs = &lpfCutoffs[numVoices];

		tBiquadHistory *lpfHistory = (tBiquadHistory*)p.Output.Data;
		tBiquadHistory *hpfHistory = &lpfHistory[numVoices>>2];
		f32 *voiceBuffer = (f32*)(&hpfHistory[numVoices>>2]);

		const u32 lpfHistoryAndvoiceBufferSize = (numVoices*kMixBufNumSamples*sizeof(f32)) + (sizeof(tBiquadHistory)*(numVoices>>1));
		Assert(lpfHistoryAndvoiceBufferSize == p.Output.Size);

		// request the voice buffer data
		const s32 tag = 7;
		sysDmaLargeGet(lpfHistory, (uint64_t)lpfHistoryAndVoiceBufferEA, lpfHistoryAndvoiceBufferSize, tag);

		Assert(p.Scratch.Size >= (sizeof(tBiquadHistory) * (numVoices>>2)) + (sizeof(tBiquadCoefficients)*(numVoices>>1)));
		tBiquadHistory *tempHistory = (tBiquadHistory*)p.Scratch.Data;
		tBiquadCoefficients *lpfCoefficients = (tBiquadCoefficients *)&tempHistory[numVoices>>2];
		tBiquadCoefficients *hpfCoefficients = &lpfCoefficients[numVoices>>2];
	
		// we can compute the coefficients while waiting on the voice data
		ComputeFilterCoefficients(numVoices, lpfCutoffs, lpfCoefficients, tempHistory, 
											  hpfCutoffs, hpfCoefficients, tempHistory);
		
		// block until we have the voice data
		sysDmaWaitTagStatusAll(1<<tag);

		// apply filters to voice data
		for(u32 i = 0, j = 0; i < numVoices; i += 4, j++)
		{
			f32 *bufs[] = {&voiceBuffer[(i+0)*kMixBufNumSamples],&voiceBuffer[(i+1)*kMixBufNumSamples],&voiceBuffer[(i+2)*kMixBufNumSamples],&voiceBuffer[(i+3)*kMixBufNumSamples]};

			if(isLPFEnabled)
			{
				VoiceFilter<kMixBufNumSamples>(bufs, lpfCoefficients[j], lpfHistory[j]);
			}
			// run HPFs seperately since very few voices will use them
			if(isHPFEnabled)
			{
				// if the sum of the hpf cutoff freq for these four channels is > 0 then atleast one is active
				if(hpfCutoffs[i+0] + hpfCutoffs[i+1] + hpfCutoffs[i+2] + hpfCutoffs[i+3])
				{
					for(u32 k = 0; k < 4; k++)
					{
						// only if active
						if(hpfCutoffs[k + i] > 0)
						{
							VoiceFilter_Single<kMixBufNumSamples>(&voiceBuffer[(k + i)*kMixBufNumSamples], hpfCoefficients[j], hpfHistory[j], k);
						}
					}
				}
			}

#if __ASSERT
			for(u32 t =0; t < 4; t++)
			{
				Assert(IsFiniteAll(lpfHistory[j].xy11[t]));
				Assert(IsFiniteAll(lpfHistory[j].xy12[t]));
				Assert(IsFiniteAll(lpfHistory[j].xy21[t]));
				Assert(IsFiniteAll(lpfHistory[j].xy22[t]));
			}
#endif
		}

		// we don't need to DMA the voice buffer back here as its set up as the output
	}

} // namespace rage

#endif // __SPU

using namespace rage;
void voicefilterjob(sysTaskParameters &SPU_ONLY(p))
{
	SPU_ONLY(rage::VoiceFilterJobEntry(p));
}
