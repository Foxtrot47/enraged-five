// 
// audiosynth/connection.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "connection.h"
#include "device.h"
#include "driver.h"
#include "mixervoice.h"
#include "submix.h"

namespace rage {

#if !__SPU
audMixerConnectionPool s_MixerConnectionPoolInstance;
audMixerConnectionPool *g_MixerConnectionPool = &s_MixerConnectionPoolInstance;
#else
audMixerConnectionPool *g_MixerConnectionPool;
#endif

#if !__SPU
bool audMixerConnectionPool::Init( )
{
	// initialise free lists
	for(s32 i = 0; i < kNumConnections - 1; i++)
	{
		Assign(m_ConnectionFreeListStore[i], i+1);
	}
	m_ConnectionFreeListStore[kNumConnections - 1] = -1;

	for(s32 i = 0; i < kNumVolumeMatrices - 1; i++)
	{
		Assign(m_MatrixFreeListStore[i], i+1);
	}
	m_MatrixFreeListStore[kNumVolumeMatrices - 1] = -1;

	m_FirstFreeMatrix = m_FirstFreeConnection = 0;
	m_NumConnectionsAllocated = m_NumVolumesAllocated = 0;
	return true;
}

void audMixerConnectionPool::Shutdown()
{

}

bool audMixerConnection::InitClass()
{
	return g_MixerConnectionPool->Init();
}

void audMixerConnection::ShutdownClass()
{
	g_MixerConnectionPool->Shutdown();
}

audMixerConnection *audMixerConnectionPool::AllocateConnection()
{
	sysCriticalSection lock(m_ConnectionPoolLock);
	if(m_NumConnectionsAllocated >= kNumConnections)
	{
		return NULL;
	}
	else
	{
		TrapLT(m_FirstFreeConnection, 0);
		const s32 index = m_FirstFreeConnection;
		m_FirstFreeConnection = m_ConnectionFreeListStore[index];
		sysInterlockedIncrement(&m_NumConnectionsAllocated);
		return &m_ConnectionPool[index];
	}
}

void audMixerConnectionPool::FreeConnection(audMixerConnection *connection)
{
	sysCriticalSection lock(m_ConnectionPoolLock);
	const s32 index = ptrdiff_t_to_int(connection - &m_ConnectionPool[0]);
	Assign(m_ConnectionFreeListStore[index], m_FirstFreeConnection);
	m_FirstFreeConnection = index;
	sysInterlockedDecrement(&m_NumConnectionsAllocated);
}

s32 audMixerConnectionPool::AllocateVolumeMatrix()
{
	sysCriticalSection lock(m_VolumePoolLock);

	if(m_NumVolumesAllocated >= kNumVolumeMatrices)
	{
		return -1;
	}
	else
	{
		TrapLT(m_FirstFreeMatrix, 0);
		const s32 index = m_FirstFreeMatrix;
		m_FirstFreeMatrix = m_MatrixFreeListStore[index];
		sysInterlockedIncrement(&m_NumVolumesAllocated);
		return index;
	}
}

void audMixerConnectionPool::FreeVolumeMatrix(const s32 index)
{
	sysCriticalSection lock(m_VolumePoolLock);
	Assign(m_MatrixFreeListStore[index], m_FirstFreeMatrix);
	m_FirstFreeMatrix = index;
	sysInterlockedDecrement(&m_NumVolumesAllocated);
}

audMixerConnection *audMixerConnection::Connect(audMixerSubmix *source, audMixerSubmix *dest, const bool allocateVolumeMatrices)
{
	audMixerConnection *connection = g_MixerConnectionPool->AllocateConnection();
	if(connection)
	{
		s32 volumeMatrix1 = -1;
		s32 volumeMatrix2 = -1;
		
		if(allocateVolumeMatrices)
		{
			volumeMatrix1 = g_MixerConnectionPool->AllocateVolumeMatrix();

			if(volumeMatrix1 == -1)
			{
				g_MixerConnectionPool->FreeConnection(connection);
				return NULL;
			}
		
			if(dest->GetNumChannels() > kNumVolumesInMatrix/2)
			{
				volumeMatrix2 = g_MixerConnectionPool->AllocateVolumeMatrix();
				if(volumeMatrix2 == -1)
				{
					g_MixerConnectionPool->FreeConnection(connection);
					g_MixerConnectionPool->FreeVolumeMatrix(volumeMatrix1);
					return NULL;
				}
			}
		}
		connection->Init(audDriver::GetMixer()->GetSubmixIndex(source), audDriver::GetMixer()->GetSubmixIndex(dest), dest->GetNumChannels(), false, false, volumeMatrix1, volumeMatrix2);
	}
	return connection;
}

audMixerConnection *audMixerConnection::Connect(audMixerVoice *source, audMixerSubmix *dest, const bool invertPhase)
{
	audMixerConnection *connection = g_MixerConnectionPool->AllocateConnection();
	if(connection)
	{
		// Volume matrices for voice->submix connections are stored in the voice, so we don't need to allocate from the
		// matrix pool.
		const s32 volumeMatrix1 = -1;
		const s32 volumeMatrix2 = -1;

		connection->Init(audDriver::GetMixer()->GetVoiceIndex(source), audDriver::GetMixer()->GetSubmixIndex(dest), dest->GetNumChannels(), true, invertPhase, volumeMatrix1, volumeMatrix2);
	}

	return connection;
}

void audMixerConnection::DestroyConnection(audMixerConnection *connection)
{
	if(connection->m_VolumeMatrix1 != -1)
	{
		g_MixerConnectionPool->FreeVolumeMatrix(connection->m_VolumeMatrix1);
	}
	if(connection->m_VolumeMatrix2 != -1)
	{
		g_MixerConnectionPool->FreeVolumeMatrix(connection->m_VolumeMatrix2);
	}
	g_MixerConnectionPool->FreeConnection(connection);
}

void audMixerConnection::DestroyConnection(const u32 connectionId)
{
	DestroyConnection(GetConnectionFromId(connectionId));
}

#endif // !__SPU

Vec::Vector_4V *audMixerConnection::GetVolumeMatrixFromId(const s32 index)
{
	return g_MixerConnectionPool->GetVolumeMatrixFromId(index);
}

u32 audMixerConnection::GetConnectionId(const audMixerConnection *connection)
{
	return g_MixerConnectionPool->GetConnectionId(connection);
}

audMixerConnection *audMixerConnection::GetConnectionFromId(const u32 id)
{
	return g_MixerConnectionPool->GetConnectionFromId(id);
}

u32 audMixerConnectionPool::GetConnectionId(const audMixerConnection *connection)
{
	audAssertf(connection, "NULL connection passed to audMixerConnectionPool::GetConnectionId()");
	u32 id = (connection ? ptrdiff_t_to_int(connection - &m_ConnectionPool[0]) : audMixerConnection::INVALID);
	audAssertf(id == audMixerConnection::INVALID || id < kNumConnections, "Calculated invalid connection id: %u for %p", id, connection);
	return id;
}

audMixerConnection *audMixerConnectionPool::GetConnectionFromId(const u32 id)
{
	audAssertf(id != audMixerConnection::INVALID, "Invalid connection id");
	if(id>=kNumConnections)
	{
		audAssertf(false, "Invalid connection id: %u", id);
		return NULL;
	}
	return &m_ConnectionPool[id];
}

} // namespace rage

