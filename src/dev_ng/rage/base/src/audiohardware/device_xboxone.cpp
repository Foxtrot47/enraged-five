//
// audiohardware/device_xboxone.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//
#if RSG_DURANGO

#include "device_xboxone.h"
#include "driver.h"
#include "submix.h"

#include "file/asset.h"
#include "file/remote.h"
#include "file/stream.h"

#include "system/endian.h"
#include "system/param.h"
#include "system/bootmgr.h"
#include "decoder_xbonexma.h"

#include "grcore/debugdraw.h"


#include "vector/colors.h"

#include "profile/profiler.h"
#include "system/performancetimer.h"

//OPTIMISATIONS_OFF()

#pragma comment(lib, "xaudio2.lib")
#pragma comment(lib, "acphal.lib")

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if(p) { p->Release(); p = NULL; }
#endif

#if !__TOOL
PARAM(audioDeviceCore,"Specify the core to run the audio on: 0->6. Default is 6. -1 means any core)");
#endif
#define AUD_DVR_OUTPUT (RSG_DURANGO)
#if AUD_DVR_OUTPUT
#define DVR_OUTPUT_ONLY(x) (x)
#else
#define DVR_OUTPUT_ONLY(x)
#endif

#if !__FINAL
PARAM(norestrictedaudio, "lets the user record restricted audio to video streams in BANK builds");
#endif // !__FINAL

PF_PAGE(XAudioDevicePage, "XAudio Device stats");
PF_GROUP(XAudioDeviceGroup);
PF_LINK(XAudioDevicePage, XAudioDeviceGroup);

//PF_TIMER(OnBufferReady, XAudioDeviceGroup);
//PF_TIMER(OnBufferStart, XAudioDeviceGroup);

PF_VALUE_FLOAT(ElapsedTime, XAudioDeviceGroup);



namespace rage
{
	bool audXAudio2Instance::sm_HaveMasterInstance = false;
	sysCriticalSectionToken audMixerDeviceXAudio2::sm_MixQueueCritSec;
	XAudioEngineState audMixerDeviceXAudio2::sm_EngineState = XAudioEngineState::Stopped;

	using namespace rage::Vec;

#if __BANK
	bool g_ShowOutputBufferQueue = false;
#endif

	u32 audMixerDeviceXAudio2::sm_BufferSize = 0;
	u32 audMixerDeviceXAudio2::sm_OutputBufferSizeBytes = 0;
	f32 *audMixerDeviceXAudio2::sm_EmptyOutputBuffer = NULL;
	u32 audMixerDeviceXAudio2::sm_NumHardwareChannels = 6;

	ServiceDelegate audMixerDeviceXAudio2::sm_Delegate;

	sysPerformanceTimer* audMixerDeviceXAudio2::sm_PerfTimer;

	volatile bool audMixerDeviceXAudio2::sm_IsMixLoopThreadRunning = false;
	volatile bool audMixerDeviceXAudio2::sm_ShouldMixLoopThreadBeRunning = true;
	sysIpcThreadId audMixerDeviceXAudio2::sm_MixLoopThreadId;

	bool audMixerDeviceXAudio2::sm_WasCapturing = false;
	sysIpcSema audMixerDeviceXAudio2::sm_TriggerUpdateSema; // for capturing
	sysIpcSema audMixerDeviceXAudio2::sm_MixBuffersSema;

	s32 audMixerDeviceXAudio2::sm_BuffersToDropAfterCapturing = 0;


	audMixerDeviceXAudio2::audMixerDeviceXAudio2() 
	{
	}

	bool audMixerDeviceXAudio2::InitHardware()
	{
		XENON_ONLY(XMAPlaybackInitialize());

		sm_PerfTimer = rage_new sysPerformanceTimer("Fake Audio Mixer");
		sm_PerfTimer->Start();

		s32 numHardwareChannels = sm_NumHardwareChannels;
		sm_BufferSize = numHardwareChannels * kMixBufNumSamples;
		sm_OutputBufferSizeBytes = sm_BufferSize * sizeof(f32);

		sm_EmptyOutputBuffer = rage_aligned_new(16) f32[kMixBufNumSamples * numHardwareChannels];
		sysMemSet( sm_EmptyOutputBuffer, 0, sm_OutputBufferSizeBytes);

		sm_TriggerUpdateSema = sysIpcCreateSema(false);
		sm_MixBuffersSema = sysIpcCreateSema(false);

		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		// From XDK ApuAlloc docs: "If no heap has been created, a 64 MB one is created by default the first time this, or another heap allocation method, is called."
		// We've now moved enough waveslots over from regular->APU memory that we need to explicitly create an APU heap with a larger-than-default size (that size being 
		// the sum of the APU waveslot sizes plus any additional memory required for XMA contexts)
		const u32 staticExplosionSlotSize = 19805814; // static explosions is technically uncompressed but now may be be re-purposed for virtual stream slots so needs to come from APU memory
		const u32 apuHeapSize = (79 * 1024 * 1024) + (kXBOneNumberXMAContexts * (kXBOneXMAHalOutputBufferBytes + 256)) + staticExplosionSlotSize;
		AssertVerify(SUCCEEDED(ApuCreateHeap(apuHeapSize)));
		audDisplayf("Allocating %u bytes for APU heap", apuHeapSize);

		if(!m_XAudio2Instance.Init(audXAudio2Instance::Main))
		{
			return false;
		}
#if AUD_DVR_OUTPUT
		if(!m_RestrictedInstance.Init(audXAudio2Instance::RestrictedContent))
		{
			return false;
		}
#endif

		sm_Delegate.Bind(&audMixerDeviceXAudio2::HandlePlmChange);
		g_SysService.AddDelegate(&sm_Delegate);

		return true;
	}

	DECLARE_THREAD_FUNC(audMixerDeviceXAudio2::sMixLoop)
	{
		USE_MEMBUCKET(MEMBUCKET_AUDIO);
		((audMixerDeviceXAudio2*)ptr)->MixLoop();
	}

	void audMixerDeviceXAudio2::StartMixing()
	{
		audMixerDeviceXAudio2::sm_EngineState = XAudioEngineState::Started;
		m_XAudio2Instance.StartMixing();

#if AUD_DVR_OUTPUT
		m_RestrictedInstance.StartMixing();
#endif
		u32 audioCore = 4;
		if (PARAM_audioDeviceCore.Get(audioCore))
		{
			Assert(audioCore <= 7 || audioCore == (u32)-1);
			if (audioCore == (u32)-1)
				audioCore = 4;
			else
				audioCore = audioCore;
		}
		if(!sm_IsMixLoopThreadRunning)
		{
			sm_MixLoopThreadId = sysIpcCreateThread(sMixLoop, this, 128 * 1024, PRIO_TIME_CRITICAL, "[RAGE Audio] Mix thread", audioCore, "RageAudioMixThread");
		}
	}

	void audMixerDeviceXAudio2::ShutdownHardware()
	{		
		m_XAudio2Instance.Shutdown();

#if AUD_DVR_OUTPUT
		m_RestrictedInstance.Shutdown();
#endif

		audDecoderXboxOneXma::ShutdownClass();

		g_SysService.RemoveDelegate(&sm_Delegate);
		sm_Delegate.Reset();

		sm_ShouldMixLoopThreadBeRunning = false;
		while(sm_IsMixLoopThreadRunning)
		{
			sysIpcSleep(10);
		}
		sysIpcWaitThreadExit(sm_MixLoopThreadId);

		delete[] sm_EmptyOutputBuffer;
		sm_EmptyOutputBuffer = NULL;
	}

	void audMixerDeviceXAudio2::TriggerUpdate()
	{
		sysIpcSignalSema(sm_TriggerUpdateSema);
	}

	void audMixerDeviceXAudio2::WaitOnMixBuffers()
	{
#if __BANK
		bool rval = sysIpcWaitSemaTimed(sm_MixBuffersSema, 1000);
		if(!rval)
		{
			Displayf("Semaphore timed out sm_MixBuffersSema");
		}
#else
		sysIpcWaitSemaTimed(sm_MixBuffersSema, 1000);
#endif
	}

	void audMixerDeviceXAudio2::MixLoop()
	{

		sm_IsMixLoopThreadRunning = true;

		while(sm_ShouldMixLoopThreadBeRunning)
		{
			BANK_ONLY( if(g_ShowOutputBufferQueue) { ShowOutputBufferQueue(); } )
			static utimer_t lastTick = 0;

			if(audDriver::GetMixer() && audDriver::GetMixer()->IsCapturing())
			{
				sm_WasCapturing = true;
				audDriver::GetMixer()->StopEngine();

				bool isFrameRendering = IsFrameRendering();
				if(isFrameRendering)
				{
					sysIpcWaitSema(sm_TriggerUpdateSema);
				}
				if(audDriver::GetMixer()->IsCapturing())
				{
					sm_BuffersToDropAfterCapturing = kAudioBuffersToDropAfterCapturing;
					EnterReplayMixerSwitchLock();
					MixBuffers();
					if(isFrameRendering)
					{
						sysIpcSignalSema(sm_MixBuffersSema);
					}
					ExitReplayMixerSwitchLock();
					sysIpcSleep(1);
				}
			}
			else if( audDriver::GetMixer() && audDriver::GetMixer()->IsSuspended())
			{                          
    			utimer_t thisTick = sysTimer::GetTicks();
    			utimer_t diffTick = thisTick - lastTick;			
    			double fDiffTick = static_cast<double>(diffTick);
    			fDiffTick *= sysTimer::GetTicksToMicroseconds();		
    			static const double duration = 5333.0;

    			if(fDiffTick >= duration)
    			{
        			if(sm_WasCapturing)
        			{
           			 	sm_WasCapturing = false;
           				// maybe need to clear out all the buffers
            			m_XAudio2Instance.m_OutputBufferQueue.Reset();
           				m_RestrictedInstance.m_OutputBufferQueue.Reset();
            			for(int i=0; i<kXAudioInstanceNumberOfBuffers; i++)
            		{
                		m_XAudio2Instance.FreeOutputBuffer(i);
               	 		m_RestrictedInstance.FreeOutputBuffer(i);
            		}
        		}

        			MixBuffers();                            
        			lastTick = thisTick;                            

        			utimer_t nowTick = sysTimer::GetTicks();
        			utimer_t timeToMix = nowTick - thisTick;
        			double timeToMixMicroS = static_cast<double>(timeToMix) * sysTimer::GetTicksToMicroseconds();

        			double sleepTime = (4300.0 - timeToMixMicroS) / 1000.0;
        			sleepTime = Clamp(sleepTime, 0.0, 4.0);
        			sysIpcSleep((int)sleepTime);
    			}
    			else
    			{
        			sysIpcSleep(0);
    			}

    			continue;
			}
			else
			{
				if(sm_WasCapturing)
				{
					sm_WasCapturing = false;
					// maybe need to clear out all the buffers
					m_XAudio2Instance.m_OutputBufferQueue.Reset();
					m_RestrictedInstance.m_OutputBufferQueue.Reset();
					for(int i=0; i<kXAudioInstanceNumberOfBuffers; i++)
					{
						m_XAudio2Instance.FreeOutputBuffer(i);
						m_RestrictedInstance.FreeOutputBuffer(i);
					}
					if(!IsSuspended())
					{
						StartMixing();
					}
				}

				if(!m_XAudio2Instance.m_OutputBufferQueue.IsEmpty() && !m_RestrictedInstance.m_OutputBufferQueue.IsEmpty()) 
				{
					s32 idMain = m_XAudio2Instance.m_OutputBufferQueue.Top();
					s32 idRestricted = m_RestrictedInstance.m_OutputBufferQueue.Top();

					if(m_XAudio2Instance.m_BufferState[idMain] == audXAudio2Instance::BufferState::Done && 
						m_RestrictedInstance.m_BufferState[idRestricted] == audXAudio2Instance::BufferState::Done)
					{
						m_XAudio2Instance.m_OutputBufferQueue.Pop();
						m_RestrictedInstance.m_OutputBufferQueue.Pop();
						m_XAudio2Instance.FreeOutputBuffer(idMain);
						m_RestrictedInstance.FreeOutputBuffer(idRestricted);
					}
				}
			
				if(sm_BuffersToDropAfterCapturing > 0)
				{
					sm_BuffersToDropAfterCapturing--;
				}
				if(!m_XAudio2Instance.m_OutputBufferQueue.IsFull() && !m_RestrictedInstance.m_OutputBufferQueue.IsFull()) 
				{
					MixBuffers();

#if __BANK
					s32 queueDifference = m_XAudio2Instance.m_OutputBufferQueue.GetCount() - m_RestrictedInstance.m_OutputBufferQueue.GetCount();
					if(queueDifference != 0)
					{
						queueDifference = queueDifference;
						m_XAudio2Instance.m_SkippedSubmitCount;
					}
#endif
					if(sm_BuffersToDropAfterCapturing <= 0)
					{
						EnterBML();
						m_XAudio2Instance.OnBufferReady();
#if AUD_DVR_OUTPUT
						m_RestrictedInstance.OnBufferReady();
#endif
						ExitBML();
					}
				}

				lastTick = sysTimer::GetTicks();
				sysIpcSleep(1);
			}
		}

		sm_IsMixLoopThreadRunning = false;
	}

#if __BANK
	void audMixerDeviceXAudio2::ShowOutputBufferQueue()
	{
		Color32 color;
		f32 yPos = 0.05f;
		f32 yInc = 0.025f;
		f32 xPos = 0.05f;

		char debugString[64];
		char graph[32];
		sysMemSet(graph, 0, 32);
		for(int i=0; i<m_XAudio2Instance.m_OutputBufferQueue.GetCount(); i++)
		{
			graph[i] = 'O';
		}
		formatf(debugString, "skip %d drop %d current %d : %s", m_XAudio2Instance.m_SkippedSubmitCount, m_XAudio2Instance.m_DroppedSubmitCount, m_XAudio2Instance.m_OutputBufferQueue.GetCount(), graph);
		grcDebugDraw::Text(Vector2(xPos, yPos), Color_white, debugString);
		yPos += yInc;

#if AUD_DVR_OUTPUT
		sysMemSet(graph, 0, 32);
		for(int i=0; i<m_RestrictedInstance.m_OutputBufferQueue.GetCount(); i++)
		{
			graph[i] = 'O';
		}
		formatf(debugString, "skip %d drop %d current %d : %s", m_RestrictedInstance.m_SkippedSubmitCount, m_RestrictedInstance.m_DroppedSubmitCount, m_RestrictedInstance.m_OutputBufferQueue.GetCount(), graph);
		grcDebugDraw::Text(Vector2(xPos, yPos), Color_white, debugString);
#endif // AUD_DVR_OUTPUT
	}
#endif // __BANK

	void audXAudio2Instance::StartMixing()
	{
		Assert(m_OutputVoice);
		m_XAudio2->StartEngine();

		XAUDIO2_BUFFER bufDesc;
		bufDesc.AudioBytes = kMixBufNumSamples * sizeof(f32) * audMixerDeviceXAudio2::sm_NumHardwareChannels;
		bufDesc.Flags = 0;
		bufDesc.LoopBegin = bufDesc.LoopLength = XAUDIO2_NO_LOOP_REGION;
		bufDesc.LoopCount = 0;
		bufDesc.PlayBegin = 0;
		bufDesc.PlayLength = 0;
		bufDesc.pContext = 0;
		bufDesc.pAudioData = (BYTE*)audMixerDeviceXAudio2::sm_EmptyOutputBuffer;
		m_OutputVoice->SubmitSourceBuffer(&bufDesc,NULL);
		m_OutputVoice->Start(0);
	}

	audXAudio2Instance::audXAudio2Instance() :
		m_XAudio2(NULL),
		m_MasteringVoice(NULL),
		m_OutputVoice(NULL),
		m_Allocator(NULL),
		m_MasterInstance(false)
	{
	}

	bool audXAudio2Instance::Init(InstanceType DVR_OUTPUT_ONLY(instanceType))
	{
		HRESULT hr;

#if __BANK
		m_OnBufferStartFrameTimer = rage_new sysPerformanceTimer("OnBufferStart");
		m_OnBufferStartFrameTimer->Start();
#endif

		BANK_ONLY( m_DroppedSubmitCount=0; m_SkippedSubmitCount = 0;)

		if(!sm_HaveMasterInstance)
		{
			m_MasterInstance = true;
			sm_HaveMasterInstance = true;
		}

		for(int i=0; i<kXAudioInstanceNumberOfBuffers; i++)
		{
			m_BufferPool[i] = rage_aligned_new(16) f32[audMixerDeviceXAudio2::sm_BufferSize];
			m_BufferState[i] = BufferState::Free;
		}
		m_OutputBufferQueue.Reset();

		u32 audioCore=Processor5;
		if (PARAM_audioDeviceCore.Get(audioCore))
		{
			Assert(audioCore <= 7 || audioCore == (u32)-1);
			if (audioCore == (u32)-1)
				audioCore = XAUDIO2_ANY_PROCESSOR;
			else
				audioCore = 1 << audioCore;
		}

		//if(FAILED(hr = XAudio2Create(&m_XAudio2, XAUDIO2_DO_NOT_USE_SHAPE | XAUDIO2_DO_NOT_SHARE_SHAPE_CONTEXTS BANK_ONLY(| XAUDIO2_DEBUG_ENGINE), audioCore)))
		if(FAILED(hr = XAudio2Create(&m_XAudio2, XAUDIO2_DO_NOT_USE_SHAPE | XAUDIO2_DO_NOT_SHARE_SHAPE_CONTEXTS, audioCore)))
		{
			return false;
		}
		Assert(m_XAudio2);

		u32 numHardwareChannels = 0;

		if(audMixerDeviceXAudio2::sm_NumHardwareChannels > audMixerDevice::kMaxOutputChannels)
			audMixerDeviceXAudio2::sm_NumHardwareChannels = audMixerDevice::kMaxOutputChannels; // we only want to output to a max 6 channels

		// Durango TODO: Enumeration has been removed in Windows 8.
		// We can get the output channel mask by calling IXAudio2MasteringVoice::GetChannelMask.
		numHardwareChannels = audMixerDeviceXAudio2::sm_NumHardwareChannels;//XAUDIO2_DEFAULT_CHANNELS; // 0 - Let CreateMasteringVoice figure it out
		
		static AUDIO_STREAM_CATEGORY eDefault = AudioCategory_GameEffects;
		AUDIO_STREAM_CATEGORY category = eDefault;
		u32 flags = 0;
#if AUD_DVR_OUTPUT
		m_Type = instanceType;
		 if(instanceType == RestrictedContent)
		 {
			category = AudioCategory_GameMedia;
			bool restrictContent = true;

#if !__FINAL
			if (PARAM_norestrictedaudio.Get())
			{
				audDisplayf("Disabled restricted audio with -norestrictedaudio");
				restrictContent = false;
			}
#endif // !__FINAL

			if (restrictContent)
			{
				flags = XAUDIO2_EXCLUDE_FROM_GAME_DVR_CAPTURE;
			}
		 }
#endif
		if(FAILED(m_XAudio2->CreateMasteringVoice(&m_MasteringVoice, numHardwareChannels, kMixerNativeSampleRate, flags, NULL, NULL, category)))
		{
			return false;
		}
		Assert(m_MasteringVoice);

		DWORD dwChannelMask;
		m_MasteringVoice->GetChannelMask( &dwChannelMask );

		XAUDIO2_VOICE_DETAILS vdetails;
		m_MasteringVoice->GetVoiceDetails( &vdetails );
		numHardwareChannels = vdetails.InputChannels;
		audMixerDeviceXAudio2::sm_NumHardwareChannels = numHardwareChannels;
		audDisplayf("%d Audio Channels Available", numHardwareChannels);
		if(audMixerDeviceXAudio2::sm_NumHardwareChannels > audMixerDevice::kMaxOutputChannels)
			audMixerDeviceXAudio2::sm_NumHardwareChannels = audMixerDevice::kMaxOutputChannels; // we only want to output to a max 6 channels

#if __BANK
		for(u32 i = 0; i < kMaxHardwareChannels; i++) 
		{
			m_OutputStreams[i] = NULL;
			m_NumOutputDataBytes[i] = 0;
		}
#endif

		audOutputMode outputMode;
		if(audMixerDeviceXAudio2::sm_NumHardwareChannels == 6)
		{
			outputMode = AUD_OUTPUT_5_1;
			audDisplayf("5.1 hardware output");
		}
		else
		{
			// WIN32PC TODO: support other output formats
			outputMode = AUD_OUTPUT_STEREO;
			audDisplayf("Stereo hardware output");
		}

		audDriver::SetHardwareOutputMode(outputMode);
		audDriver::SetDownmixOutputMode(outputMode);
				
		//WAVEFORMATEXTENSIBLE voiceFormat;
		//memset(&voiceFormat, 0, sizeof(voiceFormat));

		//voiceFormat.Format.wBitsPerSample = 32;
		//voiceFormat.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
		//Assign(voiceFormat.Format.nChannels, audMixerDeviceXAudio2::sm_NumHardwareChannels);
		//voiceFormat.Format.nSamplesPerSec = kMixerNativeSampleRate;
		//voiceFormat.Format.cbSize = sizeof(WAVEFORMATEXTENSIBLE)-sizeof(WAVEFORMATEX);
		//voiceFormat.Format.nBlockAlign = (voiceFormat.Format.nChannels * voiceFormat.Format.wBitsPerSample) >> 3;
		//voiceFormat.Format.nAvgBytesPerSec = voiceFormat.Format.nSamplesPerSec * voiceFormat.Format.nBlockAlign;
		////voiceFormat.Samples.wSamplesPerBlock = 32;
		//voiceFormat.Samples.wValidBitsPerSample = 32;
		//voiceFormat.SubFormat = KSDATAFORMAT_SUBTYPE_IEEE_FLOAT;

		WAVEFORMATEX voiceFormat;
		voiceFormat.wBitsPerSample = 32;
		voiceFormat.wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
		Assign(voiceFormat.nChannels, audMixerDeviceXAudio2::sm_NumHardwareChannels);
		voiceFormat.nSamplesPerSec = kMixerNativeSampleRate;
		voiceFormat.cbSize = 0;
		voiceFormat.nBlockAlign = (voiceFormat.nChannels * voiceFormat.wBitsPerSample) >> 3;
		voiceFormat.nAvgBytesPerSec = voiceFormat.nSamplesPerSec * voiceFormat.nBlockAlign;

		if(FAILED(m_XAudio2->CreateSourceVoice(&m_OutputVoice, (WAVEFORMATEX*)&voiceFormat, XAUDIO2_VOICE_NOSRC | XAUDIO2_VOICE_NOPITCH, 1.f, this, NULL, NULL)))
		{
			return false;
		}
		
		static const f32 stereoOutputMatrix[2*2] = {
			1.f,0.f,
			0.f,1.f};

		static const f32 fiveoOneOutputMatrix[6*6] = {
			1.f,0.f,0.f,0.f,0.f,0.f,
			0.f,1.f,0.f,0.f,0.f,0.f,
			0.f,0.f,1.f,0.f,0.f,0.f,
			0.f,0.f,0.f,1.f,0.f,0.f,
			0.f,0.f,0.f,0.f,1.f,0.f,
			0.f,0.f,0.f,0.f,0.f,1.f,
		};

		if(FAILED(m_OutputVoice->SetOutputMatrix(NULL, audMixerDeviceXAudio2::sm_NumHardwareChannels, audMixerDeviceXAudio2::sm_NumHardwareChannels, (audMixerDeviceXAudio2::sm_NumHardwareChannels == 6 ? fiveoOneOutputMatrix : stereoOutputMatrix))))
		{
			return false;
		}
		static const f32 channelMatrix[] = {1.f,1.f,1.f,1.f,1.f,1.f};
		if(FAILED(m_OutputVoice->SetChannelVolumes(audMixerDeviceXAudio2::sm_NumHardwareChannels, channelMatrix)))
		{
			return false;
		}

#if AUD_OUTPUT_TO_FILE
		m_DebugOutput = ASSET.Create("mixerout","raw");
#endif

#if AUD_DVR_OUTPUT
		if(instanceType != RestrictedContent)
#endif
		{
			if(!audDecoderXboxOneXma::InitClass())
			{
				audErrorf("audMixerDeviceXAudio2::InitHardware - audDecoderXboxOneXma::InitClass failed");
				return false;
			}
		}
		m_Allocator = &sysMemAllocator::GetCurrent();
		return true;
	}

	void audXAudio2Instance::Shutdown()
	{
		m_XAudio2->StopEngine();
		if(m_OutputVoice)
		{
			m_OutputVoice->Stop(0);
			m_OutputVoice->DestroyVoice();
			m_OutputVoice = NULL;
		}
		if(m_MasteringVoice)
		{
			m_MasteringVoice->DestroyVoice();
			m_MasteringVoice = NULL;
		}
		SAFE_RELEASE(m_XAudio2);

		for(int i=0; i<kXAudioInstanceNumberOfBuffers; i++)
		{
			delete[] m_BufferPool[i];
			m_BufferPool[i] = NULL;
			m_BufferState[i] = BufferState::Free;
		}
	}

	extern bool g_DumpAudioHardwareOutput;

#if __BANK
	void audXAudio2Instance::DumpOutput(u32 channelIndex, f32 data) 
	{
		if(g_DumpAudioHardwareOutput)
		{
			if(!m_OutputStreams[channelIndex])
			{
				m_NumOutputDataBytes[channelIndex] = 0;

				char fileName[64];
				// RAGE channel ordering
				const char *channelNames[] = 
				{
					"FL",
					"FR",
					"C",
					"LFE",
					"SL",
					"SR",					
					"BL",
					"BR"
				};
				formatf(fileName, "c:\\HardwareDump_%s", channelNames[channelIndex]);
				m_OutputStreams[channelIndex] = ASSET.Create(fileName, "wav");
				Assert(m_OutputStreams[channelIndex]);
				if(m_OutputStreams[channelIndex])
				{
					const unsigned char RIFF[] = {'R', 'I', 'F', 'F'};
					m_OutputStreams[channelIndex]->WriteByte(RIFF, 4);
					int size = 0;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					const unsigned char WAVE[] = {'W', 'A', 'V', 'E'};
					m_OutputStreams[channelIndex]->WriteByte(WAVE, 4);

					const unsigned char fmtChunk[] = {'f', 'm', 't', ' '};
					m_OutputStreams[channelIndex]->WriteByte(fmtChunk, 4);
					size = 16;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					short audioFormat = 1;
					m_OutputStreams[channelIndex]->WriteShort(&audioFormat, 1);
					short numChannels = 1;
					m_OutputStreams[channelIndex]->WriteShort(&numChannels, 1);
					u32 sampleRate = kMixerNativeSampleRate;
					m_OutputStreams[channelIndex]->WriteInt(&sampleRate, 1);
					u32 byteRate = sampleRate * numChannels * 16 / 8;
					m_OutputStreams[channelIndex]->WriteInt(&byteRate, 1);
					short blockA = 2;
					m_OutputStreams[channelIndex]->WriteShort(&blockA, 1);
					short bitsPerSample = 16;
					m_OutputStreams[channelIndex]->WriteShort(&bitsPerSample, 1);

					const unsigned char dataChunk[] = {'d', 'a', 't', 'a'};
					m_OutputStreams[channelIndex]->WriteByte(dataChunk, 4);
					size = 0;
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);

				}
			}
			s16 sh = (s16)(Clamp(data,-1.f,1.f) * 32767.f);
			m_OutputStreams[channelIndex]->WriteShort(&sh,1);
			m_NumOutputDataBytes[channelIndex] += 2;
		}
		else
		{
			if(m_OutputStreams[channelIndex])
			{
					int size = m_NumOutputDataBytes[channelIndex] + 36;
					m_OutputStreams[channelIndex]->Seek(4);
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					size = m_NumOutputDataBytes[channelIndex];
					m_OutputStreams[channelIndex]->Seek(40);
					m_OutputStreams[channelIndex]->WriteInt(&size, 1);
					m_OutputStreams[channelIndex]->Close();
					m_OutputStreams[channelIndex] = NULL;
			}
		}
	}
#endif

	s32 audXAudio2Instance::GetFreeOutputBuffer()
	{
		for(int i=0; i<kXAudioInstanceNumberOfBuffers; i++)
		{
			if(m_BufferState[i] == BufferState::Free)
			{
				m_BufferState[i] = BufferState::Mixing;
				return i;
			}
		}
		return -1;
	}

	void audXAudio2Instance::FreeOutputBuffer(s32 id)
	{
		audAssert(id >= 0 && id < kXAudioInstanceNumberOfBuffers);
#if __BANK
		sysMemSet(m_BufferPool[id], 0, audMixerDeviceXAudio2::sm_OutputBufferSizeBytes);
#endif
		m_BufferState[id] = BufferState::Free;
	}

	void audXAudio2Instance::OnBufferReady()	
	{		
		s32 bufferId = GetFreeOutputBuffer();

		audAssert(bufferId != -1);

		float *outputData = m_BufferPool[bufferId];
		
		// interleave and channel swap the mixed data
		const bool isPaused = audDriver::GetMixer()->IsPaused() NOTFINAL_ONLY(|| fiIsShowingMessageBox);
		audMixerSubmix *submix = m_MasterInstance ? audDriver::GetMixer()->GetMainOutputSubmix() : audDriver::GetMixer()->GetRestrictedOutputSubmix();
		if(submix && !isPaused && outputData && submix->HasMixBuffers())
		{
			if(submix->GetNumChannels() == 4)
			{
				Assert(submix->GetBufferFormat() == Interleaved);
				if(audMixerDeviceXAudio2::sm_NumHardwareChannels == 6)
				{
					if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
					{
						// Downmix to stereo, expand to 6 channel output
						// Note input is interleaved
						Vector_4V *out = (Vector_4V*)outputData;
				
						// FL = (FL) + 0.707(RL)
						// FR = (FR) + 0.707(RR)

						// 0.707f
						const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();
						for(u32 chunk = 0; chunk < 4; chunk++)
						{
							Vector_4V *inSamples = (Vector_4V *)submix->GetMixBuffer(chunk);

							for(u32 i = 0; i < kMixBufNumSamples; i += 16)
							{
								// load four samples per channel (interleaved at this point)
								const Vector_4V s0 = *inSamples++;
								const Vector_4V s1 = *inSamples++;
								const Vector_4V s2 = *inSamples++;
								const Vector_4V s3 = *inSamples++;

								const Vector_4V front01 = V4PermuteTwo<X1,Y1,X2,Y2>(s0,s1);
								const Vector_4V rear01 = V4PermuteTwo<Z1,W1,Z2,W2>(s0,s1);
								const Vector_4V front23 = V4PermuteTwo<X1,Y1,X2,Y2>(s2,s3);
								const Vector_4V rear23 = V4PermuteTwo<Z1,W1,Z2,W2>(s2,s3);

								const Vector_4V lr0lr1 = V4Clamp(V4AddScaled(front01, rear01, centreRearRatio), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
								const Vector_4V lr2lr3 = V4Clamp(V4AddScaled(front23, rear23, centreRearRatio), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
								
								// Now interleave LR0,00
								*out++ = V4PermuteTwo<X1,Y1,X2,X2>(lr0lr1, V4VConstant(V_ZERO));
								// 00,LR1
								*out++ = V4PermuteTwo<X1,X1,Z2,W2>(V4VConstant(V_ZERO), lr0lr1);
								// 0000
								*out++ = V4VConstant(V_ZERO);
								// LR2,00
								*out++ = V4PermuteTwo<X1,Y1,X2,X2>(lr2lr3, V4VConstant(V_ZERO));
								// 00,LR3
								*out++ = V4PermuteTwo<X1,X1,Z2,W2>(V4VConstant(V_ZERO), lr2lr3);
								// 0000
								*out++ = V4VConstant(V_ZERO);							
							}
						}
					}
					else
					{
						// up mix 4-6
						float *outputPtr = outputData;
						for(u32 i = 0; i < 4; i++)
						{
							const float *inPtr = submix->GetMixBuffer(i);
							for(u32 sample = 0; sample < kMixBufNumSamples>>2; sample++)
							{
								*outputPtr++ = *inPtr++; // FL
								*outputPtr++ = *inPtr++; // FR
								*outputPtr++ = 0.f; // C
								*outputPtr++ = 0.f; // LFE
								*outputPtr++ = *inPtr++; // BL
								*outputPtr++ = *inPtr++; // BR
							}
						}
					}
				}
				else
				{
					// downmix 4-2
					Vector_4V *outputPtr = (Vector_4V*)outputData;
					// 0.707f,0.707,0,0
					const Vector_4V rearSampleScalar = V4VConstant<0x3F34FDF3,0x3F34FDF3,0,0>();

					for(u32 i = 0; i < 4; i++)
					{
						const Vector_4V *inPtr = reinterpret_cast<Vector_4V*>(submix->GetMixBuffer(i));
						for(u32 sample = 0; sample < kMixBufNumSamples>>3; sample++)
						{
							Vector_4V inputSamples0 = *inPtr++;
							Vector_4V inputSamples1 = *inPtr++;

							// FL = (FL) + 0.707(RL)
							// FR = (FR) + 0.707(RR)
							const Vector_4V rearSamples0 = V4Scale(V4Permute<W,Z,W,Z>(inputSamples0), rearSampleScalar);
							// Z,W of downmixedSamples are invalid
							const Vector_4V downmixedSamples0  = V4Clamp(V4Add(rearSamples0, inputSamples0), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						
							const Vector_4V rearSamples1 = V4Scale(V4Permute<Z,W,Z,W>(inputSamples1), rearSampleScalar);
							const Vector_4V downmixedSamples1  = V4Clamp(V4Add(rearSamples1, inputSamples1), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				
							*outputPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(downmixedSamples0, downmixedSamples1);				
						}
					}
				}
			}
			else
			{
				Assert(submix->GetNumChannels() == 6);
				if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
				{

					Vector_4V *out = (Vector_4V*)outputData;

#if __BANK
					//Keep a second pointer for dumping data to file
					f32 *dumpData = outputData;
#endif

					const Vector_4V *inFL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_LEFT);
					const Vector_4V *inFR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_RIGHT);
					const Vector_4V *inC = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_FRONT_CENTER);
					const Vector_4V *inRL = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_LEFT);
					const Vector_4V *inRR = (const Vector_4V *)submix->GetMixBuffer(RAGE_SPEAKER_ID_BACK_RIGHT);


					// FL = (FL) + 0.707(FC) + 0.707(RL)
					// FR = (FR) + 0.707(FC) + 0.707(RR)

					// 0.707f
					const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();


					for(u32 i = 0; i < kMixBufNumSamples; i += 4)
					{
						// load four samples per channel						

						const Vector_4V inputSamples0 = *inFL++;
						const Vector_4V inputSamples1 = *inFR++;
						const Vector_4V inputSamples2 = *inC++;
						const Vector_4V inputSamples4 = *inRL++;
						const Vector_4V inputSamples5 = *inRR++;

						Vector_4V fcContrib = V4Scale(inputSamples2, centreRearRatio);
						const Vector_4V inSamplesFL = V4Add(inputSamples0, V4AddScaled(fcContrib, inputSamples4, centreRearRatio));
						const Vector_4V inSamplesFR = V4Add(inputSamples1, V4AddScaled(fcContrib, inputSamples5, centreRearRatio));

						const Vector_4V inputSamples_01_xy = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesFL, inSamplesFR);
						const Vector_4V inputSamples_01_zw = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesFL, inSamplesFR);

						const Vector_4V samples_0123_x = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_xy, V4VConstant(V_ZERO));
						const Vector_4V samples_4501_xy = V4PermuteTwo<X1,Y1,Z2,W2>(V4VConstant(V_ZERO), inputSamples_01_xy);

						const Vector_4V samples_0123_z = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples_01_zw,V4VConstant(V_ZERO));
						const Vector_4V samples_4501_zw = V4PermuteTwo<X1,Y1,Z2,W2>(V4VConstant(V_ZERO), inputSamples_01_zw);

						*out++ = V4Clamp(samples_0123_x, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4Clamp(samples_4501_xy, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4VConstant(V_ZERO);
						*out++ = V4Clamp(samples_0123_z, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4Clamp(samples_4501_zw, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
						*out++ = V4VConstant(V_ZERO);

#if __BANK
						for(int j=0; j<4; j++) //Four samples of data in each pass
						{
							for(int k=0; k<6; k++) //dump the 6 interleaved channels
							{
								DumpOutput(k, *dumpData++);
							}
						}
#endif
					}
				}
				else
				{
					f32 *RESTRICT destPtr = (f32*)outputData;
					u32 outIndex = 0;

					// NOTE: map RAGE channel order FL,FR,SL,SR,C,LFE to XAudio2 FL,FR,C,LFE,SL,SR
					const s32 channelMap[] = {RAGE_SPEAKER_ID_FRONT_LEFT,RAGE_SPEAKER_ID_FRONT_RIGHT,RAGE_SPEAKER_ID_FRONT_CENTER,RAGE_SPEAKER_ID_LOW_FREQUENCY,RAGE_SPEAKER_ID_BACK_LEFT,RAGE_SPEAKER_ID_BACK_RIGHT};
					for(u32 sampleIndex = 0; sampleIndex < kMixBufNumSamples; sampleIndex++)
					{
						for(u32 channelIndex = 0; channelIndex < audMixerDeviceXAudio2::sm_NumHardwareChannels; channelIndex++)
						{
#if __BANK
							DumpOutput(channelIndex, submix->GetMixBuffer(channelMap[channelIndex])[sampleIndex]);
#endif
							destPtr[outIndex++] = submix->GetMixBuffer(channelMap[channelIndex])[sampleIndex];
						}
					}
				}
			}
		}
		else
		{
			sysMemSet(outputData, 0, audMixerDeviceXAudio2::sm_OutputBufferSizeBytes);
		}

		m_OutputBufferQueue.Push(bufferId);
		m_BufferState[bufferId] = BufferState::Submitting;
	}

	void audXAudio2Instance::OnBufferStart(void *)
	{

		//PF_STOP(OnBufferStart);
		//PF_START(OnBufferStart);

#if __BANK
		//PF_SET(ElapsedTime, g_OnBufferStartFrameTimer->GetElapsedTimeMS());

		//static s32 count = 0;
		//count++;
		//if(count > 7)
		//{
		//	count = 0;
		//	Displayf("OnBufferStart %d %f", m_Type, m_OnBufferStartFrameTimer->GetElapsedTimeMS());
		//}

		m_OnBufferStartFrameTimer->Reset();
		m_OnBufferStartFrameTimer->Start();
#endif

		s32 bufferId = -1;
		// submit it to xaudio
		XAUDIO2_BUFFER bufDesc;
		bufDesc.AudioBytes = kMixBufNumSamples * sizeof(f32) * audMixerDeviceXAudio2::sm_NumHardwareChannels;
		bufDesc.Flags = 0;
		bufDesc.LoopBegin = bufDesc.LoopLength = XAUDIO2_NO_LOOP_REGION;
		bufDesc.LoopCount = 0;
		bufDesc.PlayBegin = 0;
		bufDesc.PlayLength = 0;
		bufDesc.pContext = NULL;
		bufDesc.pAudioData = (BYTE*)audMixerDeviceXAudio2::sm_EmptyOutputBuffer;

		if(!m_OutputBufferQueue.IsEmpty())
		{
			bufferId = m_OutputBufferQueue.Top();
			if(m_BufferState[bufferId] == BufferState::Submitting)
			{
				bufDesc.pAudioData = (BYTE*)m_BufferPool[bufferId];
				m_BufferState[bufferId] = BufferState::Submitted;
			}
		}
#if __BANK
		else
		{
			m_DroppedSubmitCount++;
		}
#endif
		m_OutputVoice->SubmitSourceBuffer(&bufDesc,NULL);
	}

	void audXAudio2Instance::OnBufferEnd(void *)
	{
		if(!m_OutputBufferQueue.IsEmpty())
		{
			s32 bufferId = m_OutputBufferQueue.Top();
			if(m_BufferState[bufferId] == BufferState::Submitted)
			{
				m_BufferState[bufferId] = BufferState::Done;
			}
		}
	}

	void audMixerDeviceXAudio2::StartEngine()
	{
		//Displayf("XAudio2->StartEngine");
		sm_EngineState = XAudioEngineState::Started;
		m_XAudio2Instance.StartEngine();
#if AUD_DVR_OUTPUT
		m_RestrictedInstance.StartEngine();
#endif
	}

	void audMixerDeviceXAudio2::StopEngine()
	{
		//Displayf("XAudio2->StopEngine");
		sm_EngineState = XAudioEngineState::Stopped;
		m_XAudio2Instance.StopEngine();
#if AUD_DVR_OUTPUT
		m_RestrictedInstance.StopEngine();
#endif

	}
	
	void audMixerDeviceXAudio2::SetXaudioSuspended(bool suspend)
	{
		
		if(audDriver::GetMixer())
		{
			audDriver::GetMixer()->SetSuspended(suspend);
			if(suspend)
			{
				audMixerDeviceXAudio2::sm_EngineState = XAudioEngineState::NeedsToStop;
				audDriver::GetMixer()->StopEngine();
			}
			else
			{
				if(!audDriver::GetMixer()->IsCapturing())
				{
					audMixerDeviceXAudio2::sm_EngineState = XAudioEngineState::NeedsToStart;
					audDriver::GetMixer()->StartEngine();
				}
			}
		}
		if(suspend)
		{
			sm_PerfTimer->Reset();
			sm_PerfTimer->Start();
		}
	}

	void audMixerDeviceXAudio2::HandlePlmChange( sysServiceEvent* evt )
	{
		if(evt != NULL)
		{
			Displayf("Audio HandlePlmChange : %d - %s", evt->GetType(), evt->GetDebugName());
			switch(evt->GetType())
			{
			case sysServiceEvent::SUSPENDED:
			case sysServiceEvent::SUSPEND_IMMEDIATE:
				SetXaudioSuspended(true);
				break;

			case sysServiceEvent::CONSTRAINED:
			case sysServiceEvent::FOCUS_LOST:
			case sysServiceEvent::FOCUS_LOST_IMMEDIATE:
			case sysServiceEvent::VISIBILITY_LOST:
					SetXaudioSuspended(true);
				break;

			case sysServiceEvent::VISIBILITY_GAINED:
			case sysServiceEvent::FOCUS_GAINED_IMMEDIATE:
			case sysServiceEvent::FOCUS_GAINED:
			case sysServiceEvent::RESUMING:
			case sysServiceEvent::RESUME_IMMEDIATE:
			case sysServiceEvent::UNCONSTRAINED:
				SetXaudioSuspended(false);
				break;
			default:
				break;
			}
		}
	}
} // namespace rage
#endif // RSG_DURANGO || __XENON
