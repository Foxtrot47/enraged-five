//
// audiohardware/mixer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef __AUDMIXER_H
#define __AUDMIXER_H

#define AUD_SUPPORT_METERING (__BANK)

namespace rage {

enum {kMixBufNumSamples = 256};

enum {kMixBufNumStereoSamples = kMixBufNumSamples * 2 };

enum {kMixerNativeSampleRate = 48000};

#if RSG_ORBIS
enum {kMaxDecodesPerFrame = 10};
#else
enum {kMaxDecodesPerFrame = 20};
#endif
enum {kMaxSkippedDecodeCount = 5};
enum {kMaxVoices = 128};

#define AUDIO_CAPTURE_ENABLED ( RSG_PC || RSG_DURANGO || RSG_ORBIS )

static const float g_SecondsPerMixBuffer = kMixBufNumSamples / float(kMixerNativeSampleRate);
static const float g_OneOverNativeSampleRate = 1.f / (f32)kMixerNativeSampleRate;
// PURPOSE
//	Defines the max SRC ratio
// NOTE
//	This defines is the max pitch scaling ratio - ie if the max
//	playback frequency is of any voice is [48000 * kMaxSampleRateRatio]
enum {kMaxSampleRateRatio = 4};
//
// PURPOSE
//	Defines the minimum playback sample rate for a  buffer.
//
enum
{
		kMinSampleRate = 0,
		kMaxSampleRate = kMaxSampleRateRatio*kMixerNativeSampleRate
};

enum {kMaxSubmixes = 40};

enum audMixBufferFormat
{
	Interleaved = 0,
	Noninterleaved,	
};

enum { kMaxPcmSources = 768 };

} // namespace rage

#endif // __AUDMIXER_H
