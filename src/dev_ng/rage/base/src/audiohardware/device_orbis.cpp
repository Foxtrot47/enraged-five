// 
// audiohardware/device_orbis.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
// Setup note: At least under SDK 0.820, you may need to go to Orbis Neighborhood, click on target, Target Settings
// and modify Sound Output Mode (under Sound Output Settings) to something reasonable for your setup.  If you're not
// an audio programmer with a fancy rig, you'll probably want HDMI (downmix 2.0ch) which was what I needed to get any
// audible sound from even the Sony sample code.

#if RSG_ORBIS

#include "device_orbis.h"
//#include "decoder_atrac9.h"
#include "decoder_ps4mp3.h"
#include "driver.h"
#include "submix.h"
#include "file/remote.h" // fiIsShowingMessageBox
#include "file/stream.h"

#include <audioout.h>
#include <user_service.h>

#include "system/exception.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/performancetimer.h"
#include "profile/profiler.h"
#include "vectormath/vectormath.h"

#include <libsysmodule.h>

#pragma comment(lib,"SceAudioOut_stub_weak")
#pragma comment(lib,"libSceAjm_stub_weak.a")

EXT_PF_PAGE(MixerDevicePage);
PF_GROUP(AudioDevicePS3);
PF_LINK(MixerDevicePage,AudioDevicePS3);

PF_TIMER(WaitTime, AudioDevicePS3);
PF_TIMER(MixTime, AudioDevicePS3);

#if !__FINAL
PARAM(norestrictedaudio, "lets the user record restricted audio to video streams in BANK builds");
#endif // !__FINAL

namespace rage 
{
	sysIpcSema audMixerDeviceOrbis::sm_TriggerUpdateSema; // for capturing
	sysIpcSema audMixerDeviceOrbis::sm_MixBuffersSema;

	using namespace Vec;

	template <const u32 numSamples> void InterleaveOutput_6To8(const f32 *input6ChannelBuffer, f32 *dest8ChannelBuffer)
	{
		const Vector_4V zero = V4VConstant(V_ZERO);
		const f32 *sourcePtr = input6ChannelBuffer;
		f32 *destPtr = dest8ChannelBuffer;

		for(u32 i = 0; i < numSamples; i+=4)
		{
			// load four samples per channel
			//NOTE: we also reorder the channels here, from RAGE Audio FL,FR,SL,SR,C,LFE to 
			// libaudio FL,FR,C,LFE,SL,SR,BL,BR
			const Vector_4V inSamplesL = *((Vector_4V*)sourcePtr);
			const Vector_4V inSamplesR = *((Vector_4V*)(sourcePtr + numSamples));
			const Vector_4V inSamplesLS = *((Vector_4V*)(sourcePtr + numSamples*2));
			const Vector_4V inSamplesRS = *((Vector_4V*)(sourcePtr + numSamples*3));
			const Vector_4V inSamplesC = *((Vector_4V*)(sourcePtr + numSamples*4));
			const Vector_4V inSamplesS = *((Vector_4V*)(sourcePtr + numSamples*5));

			const Vector_4V inSamplesLR0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesL, inSamplesR);
			const Vector_4V inSamplesLR1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesL, inSamplesR);

			const Vector_4V inSamplesCS0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesC, inSamplesS);
			const Vector_4V inSamplesCS1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesC, inSamplesS);

			const Vector_4V inSamplesLSLR0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesLS, inSamplesRS);
			const Vector_4V inSamplesLSLR1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesLS, inSamplesRS);					

			// first pair of samples
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR0, inSamplesCS0);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLSLR0, zero);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR0, inSamplesCS0);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLSLR0, zero);
			destPtr += 4;

			// second pair of samples
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR1, inSamplesCS1);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLSLR1, zero);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR1, inSamplesCS1);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLSLR1, zero);
			destPtr += 4;

			sourcePtr += 4;
		}
	}

	template <const u32 numSamples> void GatherDownmixAndInterleave_6To8(const float *inputBuffers[6], float *dest8ChannelBuffer)
	{
		Vector_4V *destPtr = reinterpret_cast<Vector_4V*>(dest8ChannelBuffer);

		const Vector_4V *sourcePtrs[6] = {
			reinterpret_cast<const Vector_4V*>(inputBuffers[0]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[1]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[2]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[3]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[4]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[5]),
		};
		// FL = (FL) + 0.707(FC) + 0.707(RL)
		// FR = (FR) + 0.707(FC) + 0.707(RR)

		// 0.707f
		const Vector_4V centreRearRatio = V4VConstantSplat<0x3F34FDF3>();
		const Vector_4V zero = V4VConstant(V_ZERO);

		for(u32 i = 0; i < numSamples; i += 4)
		{
			// load four samples per channel
			//NOTE: we also reorder the channels here, from RAGE Audio FL,FR,SL,SR,C,LFE to 
			// libaudio FL,FR,C,LFE,SL,SR,BL,BR
			const Vector_4V fl = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_FRONT_LEFT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V fr = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_FRONT_RIGHT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V rl = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_BACK_LEFT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V rr = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_BACK_RIGHT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V fc = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_FRONT_CENTER]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));;
				
			Vector_4V fcContrib = V4Scale(fc, centreRearRatio);
			const Vector_4V inSamplesFL = V4Add(fl, V4AddScaled(fcContrib, rl, centreRearRatio));
			const Vector_4V inSamplesFR = V4Add(fr, V4AddScaled(fcContrib, rr, centreRearRatio));

			const Vector_4V inSamplesLR0 = V4Clamp(V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesFL, inSamplesFR), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V inSamplesLR1 = V4Clamp(V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesFL, inSamplesFR), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				
			// Silence all other channels
		
			// first pair of samples
			*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR0, zero);
			*destPtr++ = zero;
			*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR0, zero);
			*destPtr++ = zero;

			// second pair of samples
			*destPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR1, zero);
			*destPtr++ = zero;
			*destPtr++ = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR1, zero);
			*destPtr++ = zero;
		}
	}

	template <const u32 numSamples> void GatherAndInterleave_6To8(const float *inputBuffers[6], float *dest8ChannelBuffer)
	{
		const Vector_4V zero = V4VConstant(V_ZERO);
		
		f32 *destPtr = dest8ChannelBuffer;

		const Vector_4V *sourcePtrs[6] = {
			reinterpret_cast<const Vector_4V*>(inputBuffers[0]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[1]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[2]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[3]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[4]),
			reinterpret_cast<const Vector_4V*>(inputBuffers[5]),
		};
		for(u32 i = 0; i < numSamples; i+=4)
		{
			// load four samples per channel
			//NOTE: we also reorder the channels here, from RAGE Audio FL,FR,SL,SR,C,LFE to 
			// libaudio FL,FR,C,LFE,SL,SR,BL,BR	
			const Vector_4V inSamplesL = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_FRONT_LEFT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V inSamplesR = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_FRONT_RIGHT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V inSamplesLS = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_BACK_LEFT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V inSamplesRS = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_BACK_RIGHT]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V inSamplesC = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_FRONT_CENTER]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
			const Vector_4V inSamplesS = V4Clamp(*sourcePtrs[RAGE_SPEAKER_ID_LOW_FREQUENCY]++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));

			const Vector_4V inSamplesLR0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesL, inSamplesR);
			const Vector_4V inSamplesLR1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesL, inSamplesR);

			const Vector_4V inSamplesCS0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesC, inSamplesS);
			const Vector_4V inSamplesCS1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesC, inSamplesS);

			const Vector_4V inSamplesLSLR0 = V4PermuteTwo<X1,X2,Y1,Y2>(inSamplesLS, inSamplesRS);
			const Vector_4V inSamplesLSLR1 = V4PermuteTwo<Z1,Z2,W1,W2>(inSamplesLS, inSamplesRS);					

			// first pair of samples
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR0, inSamplesCS0);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLSLR0, zero);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR0, inSamplesCS0);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLSLR0, zero);
			destPtr += 4;

			// second pair of samples
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLR1, inSamplesCS1);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<X1,Y1,X2,Y2>(inSamplesLSLR1, zero);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLR1, inSamplesCS1);
			destPtr += 4;
			*((Vector_4V*)destPtr) = V4PermuteTwo<Z1,W1,Z2,W2>(inSamplesLSLR1, zero);
			destPtr += 4;

		}
	}
	template <const u32 numSamples> void GatherAndDownmixInterleaved_4To8(const float *inputBuffers[4], float *dest8ChannelBuffer)
	{
		// 0.707f,0.707,0,0
		const Vector_4V rearSampleScalar = V4VConstant<0x3F34FDF3,0x3F34FDF3,0,0>();
		
		//NOTE: we also reorder the channels here, from RAGE Audio FL,FR,SL,SR,C,LFE to 
		// libaudio FL,FR,C,LFE,SL,SR,BL,BR
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(dest8ChannelBuffer);
		for(u32 bufferIndex = 0; bufferIndex < 4; bufferIndex++)
		{
			const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inputBuffers[bufferIndex]);
			for(u32 i = 0; i < numSamples >> 2; i++)
			{
				const Vector_4V inputSamples = *inputPtr++;

				// FL = (FL) + 0.707(RL)
				// FR = (FR) + 0.707(RR)
				const Vector_4V rearSamples = V4Scale(V4Permute<Z,W,Z,W>(inputSamples), rearSampleScalar);
				// Z,W of downmixedSamples are disregarded by the next permute
				const Vector_4V downmixedSamples  = V4Clamp(V4Add(rearSamples, inputSamples), V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				
				*outputPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(downmixedSamples, V4VConstant(V_ZERO));
				*outputPtr++ = V4VConstant(V_ZERO);
			}
		}
	}

	template <const u32 numSamples> void GatherInterleaved_4To8(const float *inputBuffers[4], float *dest8ChannelBuffer)
	{
		//NOTE: we also reorder the channels here, from RAGE Audio FL,FR,SL,SR,C,LFE to 
		// libaudio FL,FR,C,LFE,SL,SR,BL,BR
		Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(dest8ChannelBuffer);
		for(u32 bufferIndex = 0; bufferIndex < 4; bufferIndex++)
		{
			const Vector_4V *inputPtr = reinterpret_cast<const Vector_4V*>(inputBuffers[bufferIndex]);
			for(u32 i = 0; i < numSamples >> 2; i++)
			{
				const Vector_4V inputSamples = V4Clamp(*inputPtr++, V4VConstant(V_NEGONE), V4VConstant(V_ONE));
				
				*outputPtr++ = V4PermuteTwo<X1,Y1,X2,Y2>(inputSamples, V4VConstant(V_ZERO));
				*outputPtr++ = V4PermuteTwo<Z1,W1,X2,Y2>(inputSamples, V4VConstant(V_ZERO));
			}
		}
	}

	audMixerDeviceOrbis::audMixerDeviceOrbis() : 
		m_Handle(-1),
		m_RestrictedContentHandle(-1),
		m_PadSpeakerHandle(-1),
		m_PadSpeakerUserId(-1),
		m_PadSpeakerUserIdRequested(-1),
		m_PadSpeakerSubmix(NULL),		
		m_IsThreadRunning(false),
		m_PadSpeakerEnabled(false),
		m_ThreadId(sysIpcThreadIdInvalid),
		m_IsRunningLowerPriority(false),
		m_InitialCpu(sysIpcCpuInvalid),
		m_BuffersToDropAfterCapturing(0)
	{

		
	}

	bool audMixerDeviceOrbis::InitHardware()
	{		
		if (sceAudioOutInit() != SCE_OK)
		{
			audErrorf("audMixerDeviceOrbis::InitHardware - sceAudioOutInit failed");
			return false;
		}
		audDriver::SetNumOutputChannels(6);
		audDriver::SetHardwareOutputMode(AUD_OUTPUT_5_1); 
		m_Handle = sceAudioOutOpen(SCE_USER_SERVICE_USER_ID_SYSTEM, SCE_AUDIO_OUT_PORT_TYPE_MAIN, 0,
										kMixBufNumSamples, kMixerNativeSampleRate, SCE_AUDIO_OUT_PARAM_FORMAT_FLOAT_8CH);

		if (m_Handle < 0)
		{
			audErrorf("audMixerDeviceOrbis::InitHardware - main sceAudioOutOpen failed, code %x", m_Handle);
			return false;
		}

		u32 restrictedOpenParams = SCE_AUDIO_OUT_PARAM_FORMAT_FLOAT_8CH | SCE_AUDIO_OUT_PARAM_ATTR_RESTRICTED;

  #if !__FINAL
		if (PARAM_norestrictedaudio.Get())
		{
			audDisplayf("Disabled restricted audio with -norestrictedaudio");
			restrictedOpenParams &= ~SCE_AUDIO_OUT_PARAM_ATTR_RESTRICTED; // Clear the restricted parameter.
		}
  #endif // !__FINAL

		m_RestrictedContentHandle = sceAudioOutOpen(SCE_USER_SERVICE_USER_ID_SYSTEM, SCE_AUDIO_OUT_PORT_TYPE_MAIN, 0,
														kMixBufNumSamples, kMixerNativeSampleRate, restrictedOpenParams);

		if (m_RestrictedContentHandle < 0)
		{
			audErrorf("audMixerDeviceOrbis::InitHardware - restricted content sceAudioOutOpen failed, code %x", m_Handle);
			return false;
		}
	
		int rv = sceSysmoduleLoadModule(SCE_SYSMODULE_AUDIODEC);
		if (rv != SCE_OK) 
		{
			audErrorf("audMixerDeviceOrbis::InitHardware - sceSysmoduleLoadModule(SCE_SYSMODULE_AUDIODEC) failed, code %x", rv);
			return false;
		}

		if(!audDecoderPS4Mp3::InitClass())
		{
			audErrorf("audMixerDeviceOrbis::InitHardware - audDecoderMp3::InitClass failed");
			return false;
		}
		
		SceUserServiceLoginUserIdList userIdList;
		if (sceUserServiceGetLoginUserIdList(&userIdList) < 0) 
		{
			audErrorf("Failed to get user id list");
		}
		int userId = -1;
		for (int i = 0; i < SCE_USER_SERVICE_MAX_LOGIN_USERS; i ++) 
		{
			if (userIdList.userId[i] != SCE_USER_SERVICE_USER_ID_INVALID) 
			{
				userId = userIdList.userId[i];
			}
		}
		if(userId != -1)
		{
			OpenPadSpeaker(userId);
			m_PadSpeakerUserId = m_PadSpeakerUserIdRequested = userId;
		}

		sm_TriggerUpdateSema = sysIpcCreateSema(false);
		sm_MixBuffersSema = sysIpcCreateSema(false);

		return true;
	}

	void audMixerDeviceOrbis::ShutdownHardware()
	{
		// unload the libaudiodec module
		if (sceSysmoduleIsLoaded(SCE_SYSMODULE_AUDIODEC)) 
		{
			int rv = sceSysmoduleUnloadModule(SCE_SYSMODULE_AUDIODEC);
			if(rv < 0)
			{
				audErrorf("error: sceSysmoduleUnloadModule() failed: 0x%08X\n", rv);
			}
		}

		//audDecoderAtrac9::ShutdownClass();
		audDecoderPS4Mp3::ShutdownClass();

		if (m_Handle >= 0)
		{
			// Wait for all current processing to complete.
			sceAudioOutOutput(m_Handle, NULL);

			// Shut down the port.
			sceAudioOutClose(m_Handle);

			// Clear the handle
			m_Handle = -1;
		}

		if (m_RestrictedContentHandle >= 0)
		{
			// Wait for all current processing to complete.
			sceAudioOutOutput(m_RestrictedContentHandle, NULL);

			// Shut down the port.
			sceAudioOutClose(m_RestrictedContentHandle);

			// Clear the handle
			m_RestrictedContentHandle = -1;
		}

		if (m_PadSpeakerHandle >= 0)
		{
			// Wait for all current processing to complete.
			sceAudioOutOutput(m_PadSpeakerHandle, NULL);

			// Shut down the port.
			sceAudioOutClose(m_PadSpeakerHandle);

			// Clear the handle
			m_PadSpeakerHandle = -1;
		}
	}

	void audMixerDeviceOrbis::OpenPadSpeaker(const int userId)
	{
		EnterBML();
		if(m_PadSpeakerHandle >= 0)
		{
			// Wait for all current processing to complete.
			sceAudioOutOutput(m_PadSpeakerHandle, NULL);

			// Shut down the port.
			sceAudioOutClose(m_PadSpeakerHandle);

			// Clear the handle
			m_PadSpeakerHandle = -1;
		}
		m_PadSpeakerHandle = sceAudioOutOpen(userId, SCE_AUDIO_OUT_PORT_TYPE_PADSPK, 0, kMixBufNumSamples, kMixerNativeSampleRate, SCE_AUDIO_OUT_PARAM_FORMAT_FLOAT_MONO);	
		if (m_PadSpeakerHandle < 0)
		{
			audErrorf("audMixerDeviceOrbis::InitHardware - sceAudioOutOpen failed, code %x", m_PadSpeakerHandle);			
		}

		int vol = SCE_AUDIO_VOLUME_0dB;
		int ret = sceAudioOutSetVolume (m_PadSpeakerHandle, SCE_AUDIO_VOLUME_FLAG_FL_CH, &vol);
		if(ret != SCE_OK)
		{
			audErrorf("sceAudioOutSetVolume failed, %08X", ret);
		}

		ret = sceAudioOutSetMixLevelPadSpk(m_PadSpeakerHandle, SCE_AUDIO_OUT_MIXLEVEL_PADSPK_0DB);
		if(ret != SCE_OK)
		{
			audErrorf("sceAudioOutSetMixLevelPadSpk failed, %08X", ret);
		}
		ExitBML();

		UpdatePortState();
	}

	void audMixerDeviceOrbis::UpdatePortState()
	{
		if(m_PadSpeakerHandle >= 0)
		{
			SceAudioOutPortState portState;
			int32_t ret = sceAudioOutGetPortState(m_PadSpeakerHandle, &portState);
			if(ret != SCE_OK)
			{
#if RSG_ASSERT
				const char *errorMsg = "Unknown";
				switch(ret)
				{
				case SCE_AUDIO_OUT_ERROR_NOT_INIT:
					break;
				case SCE_AUDIO_OUT_ERROR_INVALID_PORT:
					break;
				case SCE_AUDIO_OUT_ERROR_INVALID_PORT_TYPE:
					break;
				case SCE_AUDIO_OUT_ERROR_NOT_OPENED:
					break;
				case SCE_AUDIO_OUT_ERROR_INVALID_POINTER:
					break;
				}
				audAssertf(ret == SCE_OK, "sceAudioOutGetPortState failed with %08X (%s)", ret, errorMsg);
#endif
			}
			else
			{
				m_PadSpeakerEnabled = (portState.output & SCE_AUDIO_OUT_STATE_OUTPUT_CONNECTED_TERTIARY) != 0;
			}
		}
	}

	void audMixerDeviceOrbis::SetLowerPriority(bool lowerPriority)
	{
		Assert(m_ThreadId != sysIpcThreadIdInvalid);
		Assert(m_IsThreadRunning);

		enum { kLowPriorityCpu = 3 };

		m_IsRunningLowerPriority = lowerPriority;
		sysIpcSetThreadPriority(m_ThreadId, lowerPriority ? PRIO_LOWEST : PRIO_TIME_CRITICAL);
		sysIpcSetThreadProcessor(m_ThreadId, lowerPriority ? kLowPriorityCpu : m_InitialCpu);
	}

	void audMixerDeviceOrbis::StartMixing()
	{
		Assert(!m_IsThreadRunning);
		enum {kMixThreadCpu = 1}; // gameconfig XML changes this to 5 anyway
		m_ThreadId = sysIpcCreateThread(sMixLoop, this, 128 * 1024, PRIO_TIME_CRITICAL, "[RAGE Audio] Mix thread", kMixThreadCpu, "RageAudioMixThread");

		// need to keep track of the initial CPU affinity
		// it is not likely to be the one passed in as gameconfig.xml can change it (usually to 5)
		m_InitialCpu = sysIpcGetThreadProcessor(m_ThreadId);
		Assert(m_InitialCpu > sysIpcCpuInvalid);
		Assert(m_InitialCpu < sysIpcGetProcessorCount());
	}

	void audMixerDeviceOrbis::MixLoop()
	{
		m_IsThreadRunning = true;

		ALIGNAS(16) f32 fInterleavedOutput[2][kMixBufNumSamples * 8] = {0.f};
		ALIGNAS(16) f32 fRestrictedInterleavedOutput[2][kMixBufNumSamples * 8] = {0.f};
		ALIGNAS(16) f32 fPadSpeakerOutput[2][kMixBufNumSamples] = {0.f};

		bool bounce = false;
		
		while(true)
		{
			sysPerformanceTimer mixTimer("mixTimer");
			mixTimer.Start();
			
			bool isCapturing = (audDriver::GetMixer() && audDriver::GetMixer()->IsCapturing());
			if(isCapturing)
			{
				m_BuffersToDropAfterCapturing = kAudioBuffersToDropAfterCapturing;
				bool shouldSignal = false;
				if(IsFrameRendering())
				{
					shouldSignal = true;
					sysIpcWaitSema(sm_TriggerUpdateSema);
				}
				if(isCapturing)
				{
					EnterReplayMixerSwitchLock();
					MixBuffers();
					if(shouldSignal)
					{
						sysIpcSignalSema(sm_MixBuffersSema);
					}
					ExitReplayMixerSwitchLock();
				}
				else
				{
					if(shouldSignal)
					{
						sysIpcSignalSema(sm_MixBuffersSema);
					}
				}
			}
			else
			{
				MixBuffers();
			}

			const bool isPaused = IsPaused() || m_IsRunningLowerPriority || isCapturing || m_BuffersToDropAfterCapturing NOTFINAL_ONLY(|| fiIsShowingMessageBox);

			if(m_BuffersToDropAfterCapturing > 0)
			{
				m_BuffersToDropAfterCapturing--;
			}

			// pad speaker
			if(m_PadSpeakerHandle >= 0 && m_PadSpeakerSubmix)
			{
				if(isPaused)
				{
					sysMemZeroBytes<sizeof(float) * kMixBufNumSamples>(fPadSpeakerOutput[bounce]);
				}
				else
				{
					if (m_PadSpeakerSubmix->GetMixBuffer(0))
					{
						sysMemCpy(fPadSpeakerOutput[bounce], m_PadSpeakerSubmix->GetMixBuffer(0), sizeof(float) * kMixBufNumSamples);
					}
					else
					{
						sysMemSet(fPadSpeakerOutput[bounce], 0, sizeof(float) * kMixBufNumSamples);
					}
				}
			}
			
			if(GetMainOutputSubmix() && GetMainOutputSubmix()->HasMixBuffers())
			{
				if(isPaused)
				{
					sysMemZeroBytes<8 * sizeof(float) * kMixBufNumSamples>(fInterleavedOutput[bounce]);
				}
				else
				{
					const float *submixBuffers[6] = {
						GetMainOutputSubmix()->GetMixBuffer(0),
						GetMainOutputSubmix()->GetMixBuffer(1),
						GetMainOutputSubmix()->GetMixBuffer(2),
						GetMainOutputSubmix()->GetMixBuffer(3),
						GetMainOutputSubmix()->GetMixBuffer(4),
						GetMainOutputSubmix()->GetMixBuffer(5),
					};
					if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
					{
						GatherDownmixAndInterleave_6To8<kMixBufNumSamples>(submixBuffers, fInterleavedOutput[bounce]);
					}
					else
					{
						GatherAndInterleave_6To8<kMixBufNumSamples>(submixBuffers, fInterleavedOutput[bounce]);
					}
				}
			}
			else
			{
				InterleaveOutput_6To8<kMixBufNumSamples>(GetOutputBuffer(), fInterleavedOutput[bounce]);
			}

			if(GetRestrictedOutputSubmix() && GetRestrictedOutputSubmix()->HasMixBuffers())
			{
				if(isPaused)
				{
					sysMemZeroBytes<8 * sizeof(float) * kMixBufNumSamples>(fRestrictedInterleavedOutput[bounce]);
				}
				else
				{
					const float *submixBuffers[4] = {
						GetRestrictedOutputSubmix()->GetMixBuffer(0),
						GetRestrictedOutputSubmix()->GetMixBuffer(1),
						GetRestrictedOutputSubmix()->GetMixBuffer(2),
						GetRestrictedOutputSubmix()->GetMixBuffer(3)
					};
					if(audDriver::GetDownmixOutputMode() == AUD_OUTPUT_STEREO)
					{
						GatherAndDownmixInterleaved_4To8<kMixBufNumSamples>(submixBuffers, fRestrictedInterleavedOutput[bounce]);
					}
					else
					{
						GatherInterleaved_4To8<kMixBufNumSamples>(submixBuffers, fRestrictedInterleavedOutput[bounce]);
					}
				}
			}
			
			mixTimer.Stop();

			if(m_PadSpeakerHandle >= 0)
			{
				int ret = sceAudioOutOutput(m_PadSpeakerHandle, fPadSpeakerOutput[bounce]);
				if(ret < 0)
				{
					audErrorf("Pad Speaker - sceAudioOutOutput returned %08x", ret);
				}
			}

//#if !__FINAL && !__NO_OUTPUT
//			if(!sysException::HasBeenThrown() && mixTimer.GetTimeMS() > 9.5f)
//			{
//				audWarningf("Long mixer frame: %f", mixTimer.GetTimeMS());
//			}
//#endif

			// This function returns once the buffer is scheduled, not consumed,
			// so we do our own double-buffering here.
			sceAudioOutOutput(m_Handle, fInterleavedOutput[bounce]);
			sceAudioOutOutput(m_RestrictedContentHandle, fRestrictedInterleavedOutput[bounce]);
			bounce = !bounce;

			if(m_PadSpeakerUserId != m_PadSpeakerUserIdRequested)
			{
				OpenPadSpeaker(m_PadSpeakerUserIdRequested);
				m_PadSpeakerUserId = m_PadSpeakerUserIdRequested;
			}
		}

		m_IsThreadRunning = false;
	}


	void audMixerDeviceOrbis::TriggerUpdate()
	{
		sysIpcSignalSema(sm_TriggerUpdateSema);
	}

	void audMixerDeviceOrbis::WaitOnMixBuffers()
	{
#if __BANK
		bool rval = sysIpcWaitSemaTimed(sm_MixBuffersSema, 1000);
		if(!rval)
		{
			Displayf("Semaphore timed out sm_MixBuffersSema");
		}
#else
		sysIpcWaitSemaTimed(sm_MixBuffersSema, 1000);
#endif
	}

	DECLARE_THREAD_FUNC(audMixerDeviceOrbis::sMixLoop)
	{
		USE_MEMBUCKET(MEMBUCKET_AUDIO);
		((audMixerDeviceOrbis*)ptr)->MixLoop();
	}
} // namespace rage

#endif // __PS3
