// 
// audiohardware/granularsubmix.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "granularsubmix.h"
#include "pcmsource.h"
#include "audioengine/engineutil.h"
#include "audiohardware/mixing_vmath.inl"
#include "audiohardware/waveref.h"
#include "audiohardware/driver.h"
#include "audiohardware/device.h"
#include "audiohardware/src_linear.h"
#include "audioengine/spuutil.h"

#if __XENON
#include "audiohardware/decoder_xma.h"
#include "xma2defs.h"
#endif

#if __PS3
#include "audiohardware/decoder_spu.h"
#endif

#include "profile/element.h"
#include "profile/page.h"
#include "profile/group.h"

#define __GRANULAR_CROSSFADE_ENABLED 1

// Turn this on to assert on non-critical errors (starvation warnings, etc.)
#define __GRANULAR_ASSERTS_ENABLED 0

#define ALIGN(x, a)	(((x) + ((a) - 1)) & ~((a) - 1))

#if __GRANULAR_ASSERTS_ENABLED
#define audGranularAssert(cond)				audAssert(cond)
#define audGranularAssertf(cond,fmt,...)	audAssertf(cond,fmt,##__VA_ARGS__)
#else
#define audGranularAssert(cond)
#define audGranularAssertf(cond,fmt,...)
#endif

namespace rage 
{

#if __SPU
u32 g_ScratchDecoderTags[3] = {13, 14, 15};
u32 g_ScratchCrossfadeBufferTag = 16;
u8 *g_ScratchDecoders[3];
u8 *g_ScratchMixBuffer;
u8 *g_ScratchSampleGathererBuffer;
u8* g_ScratchCrossfadeBuffer;
#endif

#if __GRANULAR_CROSSFADE_ENABLED
static const f32 s_granularCrossfadeAmount = 0.05f;
static const f32 s_oneOverCrossfadeAmount = 1.f/s_granularCrossfadeAmount;
#else
static const f32 s_granularCrossfadeAmount = 0.0f;
static const f32 s_oneOverCrossfadeAmount = 0.0f;
#endif

static const f32 s_s16ToFloatDivisor = 1.f / 32767.f;
static const u32 s_resampleBufferOverlap = 10;

const u32 audGranularSubmix::s_SampleGathererBufferLength = ALIGN((kMixBufNumSamples + s_resampleBufferOverlap) * kMaxSampleRateRatio + 2, 128);
const u32 audGranularSubmix::s_MixBufferLength = ALIGN(kMixBufNumSamples + s_resampleBufferOverlap, 128);
bool audGranularSubmix::s_GranularMidLoopRandomisationEnabled = true;

static const u32 s_OverlapSamplesForCrossfade = 256;

// Compensate for equal power crossfades between submixes
static const f32 s_ClippingVolumeFix = 0.707946f;

#if __PS3
u32 audGranularSubmix::s_NumDecodesThisFrame = 0;
u32 audGranularSubmix::s_NumActiveSubmixes = 0;
u32 audGranularSubmix::s_NumSubixesStartedThisFrame = 0;
u32 audGranularSubmix::s_ScratchDecoderIndex = 0;

static const u32 s_FFTFadeSkipSize = (audMp3Util::kFrameSizeSamples + 16);
// When calculating the number of valid grains in a vram buffer, grain(s) loaded right at the very end
// sometimes seem to be corrupt. Temporary workaround for this is to just ignore grains that finish in the last
// x bytes of the buffer
const u32 audGranularSubmix::s_VramCalculationByteLimit = 256;
#elif __XENON
static const u32 s_FFTFadeSkipSize = 128;
#else
static const u32 s_FFTFadeSkipSize = 0;
#endif

EXT_PF_GROUP(AudioGrainPlayerTimings);

PF_TIMER(GenerateFrameGranularSubmix, AudioGrainPlayerTimings);
PF_TIMER(GenerateFrameGranularDecodedSubmix, AudioGrainPlayerTimings);
PF_TIMER(UpdateGrainDecoders, AudioGrainPlayerTimings);
PF_TIMER(ReadDecodedData, AudioGrainPlayerTimings);
PF_TIMER(AlignmentFix, AudioGrainPlayerTimings);
PF_TIMER(GranularCrossfade, AudioGrainPlayerTimings);
PF_TIMER(SampleRateConversion, AudioGrainPlayerTimings);
PF_TIMER(SendDecoderPackets, AudioGrainPlayerTimings);
PF_TIMER(CalculateNewGrains, AudioGrainPlayerTimings);
PF_TIMER(ComputeBytesToSkip, AudioGrainPlayerTimings);
PF_TIMER(FinalMixDown, AudioGrainPlayerTimings);

s32 audGranularSubmix::s_NumGranularDecodersAllocated = 0;
BANK_ONLY(bool audGranularSubmix::s_ForceGranularSubmixStarvation = false;)

#if !__SPU
// ----------------------------------------------------------------
// audGranularSubmix constructor
// ----------------------------------------------------------------
audGranularSubmix::audGranularSubmix(const audWaveRef* waveRef, const audGrainData* grainTable, u32 numGrains)
{
	Assert(waveRef);
	u32 formatChunkSize = 0;
	m_WaveFormat = waveRef->FindFormat(formatChunkSize);

	// backwards compatible with old format chunk
	m_FirstPeakLevelSample = AUD_READ_FIRST_PEAK_SAMPLE(m_WaveFormat, formatChunkSize);

#if __BANK
	m_BankName = waveRef->GetSlot()->GetLoadedBankName();
#endif

#if __PS3
	m_SeekTable = (const u16*)waveRef->FindSeek(m_SeekTableSize);
	Assert(m_SeekTable);
#endif

	m_SubmixIDHash = 0;
	m_GrainTable = grainTable; 
	m_NumGrains = numGrains;
	m_NumValidGrains = numGrains - 1;
	m_NumSamples = m_WaveFormat->LengthSamples;

#if __PS3
	// Preserve transients flag means we get an additional frame of data
	m_NumSamples += audMp3Util::kFrameSizeSamples;
	m_CrossfadeSampleBufferPtr = &m_CrossfadeSampleBuffer[0];
#else
	m_CrossfadeSampleBufferPtr = NULL;
#endif

	m_WavenameHash = waveRef->GetWaveNameHash();
	m_WaveData = (const s16*) waveRef->FindWaveData(m_WaveLengthBytes);
	Assert(m_WaveData);
	m_WaveSlot = waveRef->GetSlotId();
	m_ValidGrains = NULL;
	m_PlaybackOrder = PlaybackOrderMixNMatch;
	m_MixNMatchPlaybackOrder = PlaybackOrderSequential;
	m_SubmixType = SubmixTypePureGranular;
	m_InterGrainCrossfadeStyle = CrossfadeStyleLinear;
	m_WalkDirectionForward = true;
	m_GrainHistoryIndex = 0;
	m_SelectedValidGrainIndex = 0;
	m_NumMixMatchGrainsToPlay = 0;
	m_IsEnabled = true;
	m_IsMuted = false;
	m_DecodersRequired = false;
	m_IsPlayingPhysically = false;
	m_NumStoredGrainStates = 2;
	m_NumGrainsGenerated = 0;
	m_PureRandomPlayback = false;
	m_GrainSkippingEnabled = false;
	m_NumGrainsToSkip = 0;
	m_NumGrainsToPlay = 0;
	m_SkippedGrainVol = 0.0f;
	m_PitchShift = 0.0f;
	m_PitchStretch = 1.0f;
	m_MaxGrainLimit = -1;
	m_MinGrainLimit = -1;
	m_SynchronisedLoopIndex = -1;
	m_ToneGeneratorEnabled = false;
	m_AllowedDecodesPerFrame = 1;
	m_ActiveGrainIndex = 1;
	m_PrevGrainIndex = 0;
	m_InitialPacketsGenerated = false;
	m_FirstFrame = true;
	m_StarvedLastFrame = false;
	m_LastRandomisedGrainCenter = 0;
	m_BaseGrainIndexLastTime = 0;
	m_NumGrainsSinceWalkReRandomise = 0;
	m_WasRandomisingLastTime = false;
	m_PeakNormaliseLoops = false;

	u32 peakChunkSize = 0;
	m_PeakData = (const u16*)waveRef->FindChunk(ATSTRINGHASH("PEAK",0x8B946236), peakChunkSize);
	m_NumPeakSamples = peakChunkSize >> 1;
	
	m_SlidingGrainWindowSizeHz = 0.035f;
	m_SlidingGrainWindowSizeGrainsMin = 1;
	m_SlidingGrainWindowSizeGrainsMax = 5;
	m_MinGrainRepeateRate = 2;
	m_MaxRandomisedGrainBelow = 0;
	m_MaxRandomisedGrainAbove = 0;

	for(u32 loop = 0; loop < kMaxStoredGrainStates; loop++)
	{
		m_ActiveGrains[loop].startSample = 0;
		m_ActiveGrains[loop].grainLength = 0;
		m_ActiveGrains[loop].decoder = NULL;
		m_ActiveGrains[loop].samplesDecoded = 0;
		m_ActiveGrains[loop].initialFadeSamplesRemaining = s_FFTFadeSkipSize;
		m_ActiveGrains[loop].packetSent = false;
		m_ActiveGrains[loop].awaitingGrainChange = false;
		m_ActiveGrains[loop].awaitingFlush = false;
		m_ActiveGrains[loop].isFirstFlush = true;
		m_ActiveGrains[loop].skipThisGrain = false;

#if __PS3
		m_ActiveGrains[loop].spuPacketSent = false;
		m_ActiveGrains[loop].canDecodeMoreData = true;
		m_ActiveGrains[loop].vramLoader = NULL;
#endif
	}

	const audWaveFormat::audStreamFormat native = (__BE ? audWaveFormat::kPcm16bitBigEndian : audWaveFormat::kPcm16bitLittleEndian);

	if(m_WaveFormat->Format != native)
	{
		m_DecodersRequired = true;
	}
}

// ----------------------------------------------------------------
// audGranularSubmix shutdown
// ----------------------------------------------------------------
void audGranularSubmix::Shutdown()
{
	StopPhys();
}

// ----------------------------------------------------------------
// audGranularSubmix ComputeCurrentPeak
// ----------------------------------------------------------------
u16 audGranularSubmix::ComputeCurrentPeak() const
{
	u32 playPositionSamples = m_ActiveGrains[m_ActiveGrainIndex].startSample + m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded;
	return ComputePeakFromSamplePosition(playPositionSamples);
}

// ----------------------------------------------------------------
// audGranularSubmix ComputePeakFromSamplePosition
// ----------------------------------------------------------------
u16 audGranularSubmix::ComputePeakFromSamplePosition(u32 playPositionSamples) const
{
	u16 currentPeakLevelSample = 0u;
	u32 sampleIndex = SamplesToPeakIndex(playPositionSamples);

	if(m_PeakData && sampleIndex > 0)
	{
		// The peak data table stores samples 1,...,n so we must subtract 1 here (sample 0 is m_FirstPeakLevelSample)
		const u32 peakSampleIndex = Min(sampleIndex - 1, m_NumPeakSamples - 1);
		const u32 peakSample = m_PeakData[peakSampleIndex];
		Assign(currentPeakLevelSample,  peakSample);
	}
	else
	{
		Assign(currentPeakLevelSample, m_FirstPeakLevelSample);
	}

	return currentPeakLevelSample;
}

// ----------------------------------------------------------------
// Start physical playback
// ----------------------------------------------------------------
bool audGranularSubmix::StartPhys()
{
	if(m_IsPlayingPhysically)
	{
		return true;
	}

	m_SynchronisedLoopIndex = -1;
	m_ActiveGrainIndex = 1;
	m_PrevGrainIndex = 0;
	m_WasRandomisingLastTime = false;
	m_InitialPacketsGenerated = false;
	m_MaxGrainLimit = -1;
	m_MinGrainLimit = -1;
	m_FirstFrame = true;
	m_PrevVolumeScale = 0.0f;
	m_GrainHistoryIndex = 0;
	m_GranularFractionInternal = 0.0f;
	m_StarvedLastFrame = false;
	m_NumGrainsSinceWalkReRandomise = 0;

	for(u32 loop = 0; loop < kMaxGrainHistoryValues; loop++)
	{
		m_GrainHistory[loop] = -1;
	}

	sysMemZeroBytes<sizeof(m_ActiveGrains)>(m_ActiveGrains);

#if __PS3
	CompileTimeAssert(s_OverlapSamplesForCrossfade == 256);
	sysMemZeroBytes<sizeof(s16) * 512>(m_CrossfadeSampleBuffer);

	// PS3 version runs on SPU, so always needs a SPU decoder, even with PCM data
	m_DecodersRequired = true;
#endif

	if(m_DecodersRequired)
	{
		for(u32 loop = 0; loop < kMaxStoredGrainStates; loop++)
		{
			if(loop < m_NumStoredGrainStates)
			{
				if(s_NumGranularDecodersAllocated < audDriver::GetConfig().GetNumGranularDecoders())
				{
					m_ActiveGrains[loop].decoder = audDriver::GetMixer()->GetDecodeMgr().AllocateDecoder((audWaveFormat::audStreamFormat)m_WaveFormat->Format, 1, m_WaveFormat->SampleRate);
				}
				else
				{
					m_ActiveGrains[loop].decoder = NULL;
				}

				if(!m_ActiveGrains[loop].decoder)
				{
					for(u32 existingDecoderLoop = 0; existingDecoderLoop < kMaxStoredGrainStates; existingDecoderLoop++)
					{
						if(m_ActiveGrains[existingDecoderLoop].decoder)
						{
							audDriver::GetMixer()->GetDecodeMgr().FreeDecoder(m_ActiveGrains[existingDecoderLoop].decoder);
							m_ActiveGrains[existingDecoderLoop].decoder = NULL;
							s_NumGranularDecodersAllocated--;
						}
					}

					// Bail out without setting m_IsPlayingPhysically
					//audWarningf("Granular submix failed to allocate a decoder!");
					return false;
				}
				else
				{
					s_NumGranularDecodersAllocated++;
				}
			}

			m_ActiveGrains[loop].samplesDecoded = 0;
			m_ActiveGrains[loop].initialFadeSamplesRemaining = s_FFTFadeSkipSize;
			m_ActiveGrains[loop].packetSent = false;
			m_ActiveGrains[loop].awaitingGrainChange = false;
			m_ActiveGrains[loop].awaitingFlush = false;
			m_ActiveGrains[loop].isFirstFlush = true;
			m_ActiveGrains[loop].grainLength = 0;

#if __PS3
			m_ActiveGrains[loop].spuPacketSent = false;
			m_ActiveGrains[loop].canDecodeMoreData = true;
			m_ActiveGrains[loop].vramLoader = NULL;
#endif
		}
	}

	if(!m_InitialPacketsGenerated)
	{
		// Load up our grain structs with some data to get us started
		for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
		{
			MoveToNextGrain();
		}

		m_ActiveGrainIndex = 0;
		m_PrevGrainIndex = m_NumStoredGrainStates - 1;
		m_InitialPacketsGenerated = true;
	}

	m_IsPlayingPhysically = true;
	return true;
}

// ----------------------------------------------------------------
// Stop physical playback
// ----------------------------------------------------------------
void audGranularSubmix::StopPhys()
{
	if(!m_IsPlayingPhysically)
	{
		return;
	}

#if __PS3
	for(u32 loop = 0; loop < kMaxStoredGrainStates; loop++)
	{
		m_ActiveGrains[loop].vramLoader = NULL;
	}
#endif

	for(u32 loop = 0; loop < kMaxStoredGrainStates; loop++)
	{
		if(m_ActiveGrains[loop].decoder)
		{
			audDriver::GetMixer()->GetDecodeMgr().FreeDecoder(m_ActiveGrains[loop].decoder);
			s_NumGranularDecodersAllocated--;
			m_ActiveGrains[loop].decoder = NULL;
		}
	}

	m_IsPlayingPhysically = false;
	m_MaxGrainLimit = -1;
	m_MinGrainLimit = -1;

	for(u32 loop = 0; loop < kMaxGrainHistoryValues; loop++)
	{
		m_GrainHistory[loop] = -1;
	}

	if(!m_PureRandomPlayback)
	{
		m_ValidGrains = NULL;
		m_NumValidGrains = 0;
		m_SynchronisedLoopIndex = -1;
	}
}

// ----------------------------------------------------------------
// Query the number of decoders being used
// ----------------------------------------------------------------
u32 audGranularSubmix::QueryNumDecodersInUse() const
{
	u32 numDecoders = 0;

	for(u32 loop = 0; loop < kMaxStoredGrainStates; loop++)
	{
		if(m_ActiveGrains[loop].decoder)
		{
			numDecoders++;
		}
	}

	return numDecoders;
}

// ----------------------------------------------------------------
// Set the table of valid grains that this submix can use
// ----------------------------------------------------------------
void audGranularSubmix::SetValidGrains(const u32* validGrains, u32 numValidGrains, s32 synchronisedLoopIndex)
{
	m_ValidGrains = validGrains;
	m_NumValidGrains = numValidGrains;
	Assign(m_SynchronisedLoopIndex, synchronisedLoopIndex);
}

// ----------------------------------------------------------------
// Called before starting to generate a frame of PCM data
// ----------------------------------------------------------------
void audGranularSubmix::BeginFrame()
{
	if(!m_IsPlayingPhysically)
	{
		return;
	}

#if __XENON
	for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
	{
		if(m_ActiveGrains[loop].decoder)
		{
			// If this is the active grain, we're going to want to update it
			// If the decoder is finished, might as well reserve the lock for when we next use it
			// Otherwise if this is still decoding the tail end of the previous grain, we'll want to update it so grab the lock
			if(loop == m_ActiveGrainIndex ||
				m_ActiveGrains[loop].decoder->GetState() == audDecoder::FINISHED ||
				(m_ActiveGrains[loop].decoder->GetState() == audDecoder::DECODING && !m_ActiveGrains[loop].packetSent))
			{
				m_ActiveGrains[loop].decoder->PreUpdate();
			}
		}
	}
#endif
}

// ----------------------------------------------------------------
// Check if this submix is awaiting a grain change
// ----------------------------------------------------------------
bool audGranularSubmix::IsAwaitingGrainChange() const
{
	for(u32 activeGrainLoop = 0; activeGrainLoop < m_NumStoredGrainStates; activeGrainLoop++)
	{
		if(m_ActiveGrains[activeGrainLoop].awaitingGrainChange)
		{
			return true;
		}
	}

	return false;
}

// ----------------------------------------------------------------
// Called after generating a frame of PCM data
// ----------------------------------------------------------------
void audGranularSubmix::EndFrame(const f32 currentRateHz, const f32 granuleStepPerGranule PS3_ONLY(, audVramLoader* grainVramLoaders, u32 numGrainVramLoaders, audVramLoader* loopVramLoaders, u32 numLoopVramLoaders))
{
	if(!m_IsPlayingPhysically)
	{
		return;
	}

	if(m_InitialPacketsGenerated)
	{
		CalculateNewGrains(currentRateHz, granuleStepPerGranule PS3_ONLY(, grainVramLoaders, numGrainVramLoaders, loopVramLoaders, numLoopVramLoaders));
		SendDecoderPackets();
	}
}
#endif //!__SPU

// ----------------------------------------------------------------
// Move to a new grain
// ----------------------------------------------------------------
void audGranularSubmix::MoveToNextGrain()
{
	// Next grain becomes the active grain
	m_PrevGrainIndex = m_ActiveGrainIndex;
	m_ActiveGrainIndex = (m_ActiveGrainIndex + 1) % m_NumStoredGrainStates;

	// Check that this grain is ready to be played
	if(m_InitialPacketsGenerated)
	{
		if(!m_ActiveGrains[m_ActiveGrainIndex].packetSent)
		{
			audWarningf("audGranularSubmix moving to next grain before packet has been sent");
		}

		Assert(!m_ActiveGrains[m_ActiveGrainIndex].awaitingGrainChange);
		Assert(!m_ActiveGrains[m_ActiveGrainIndex].awaitingFlush);
	}

	// Mark the previous grain as needing a new grain
	m_ActiveGrains[m_PrevGrainIndex].awaitingGrainChange = true;	
	m_ActiveGrains[m_PrevGrainIndex].awaitingFlush = m_ActiveGrains[m_PrevGrainIndex].packetSent;
	m_ActiveGrains[m_PrevGrainIndex].packetSent = false;
	m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain = false;

	if(m_GrainSkippingEnabled)
	{
		if(m_NumGrainsGenerated % (m_NumGrainsToSkip + m_NumGrainsToPlay) < m_NumGrainsToSkip) 
		{
			m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain = true;
		}
	}
}

// ----------------------------------------------------------------
// AdvanceActiveGrainReadPtr
// ----------------------------------------------------------------
u32 audGranularSubmix::AdvanceActiveGrainReadPtr(const void* decoderPtr, const u32 grainIndex, const f32 currentGranularFraction, const f32 granuleFractionPerSample, const u32 samplesGenerated)
{
	if(!decoderPtr)
	{
		return 0;
	}

#if __SPU
	audDecoderSpu* decoder = (audDecoderSpu*)decoderPtr;
#else
	audDecoder* decoder = (audDecoder*)decoderPtr;
#endif

	// Having stepped through a given fraction of a grain, translate this into samples and advance the read pointer by the required amount
	const f32 initialSamplePos = m_ActiveGrains[grainIndex].grainLength * currentGranularFraction;
	const u32 initialSamplePosFloor = (u32) Floorf(initialSamplePos);

	const f32 newGranularFraction = currentGranularFraction + (granuleFractionPerSample * samplesGenerated);
	const f32 newSamplePos = m_ActiveGrains[grainIndex].grainLength * newGranularFraction;
	const u32 newSamplePosFloor = (u32) Floorf(newSamplePos);
	const u32 samplesConsumed = Min(newSamplePosFloor - initialSamplePosFloor, decoder->QueryNumAvailableSamples());

	if(samplesConsumed > 0)
	{
		m_ActiveGrains[grainIndex].samplesDecoded += samplesConsumed;
		decoder->AdvanceReadPtr(samplesConsumed);
	}

	return samplesConsumed;
}

// ----------------------------------------------------------------
// Flush any grains that need flushing
// ----------------------------------------------------------------
void audGranularSubmix::UpdateGrainFlush(bool withinCrossfadeSection)
{
	// Check our decoders and see if any need to be flushed (that is, they've finished decoding a grain, and need to be told to 
	// discard any additional data). The only time when we don't want to immediately flush a grain is when the grain in question 
	// is the previously active grain and we're still in the middle of doing an inter-granular crossfade - in that situation we're still
	// using the additional data beyond the end of the grain to mix from
	for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
	{
		if(m_ActiveGrains[loop].awaitingFlush)
		{
			if(loop != m_PrevGrainIndex || !withinCrossfadeSection)
			{
#if __XENON
				((audDecoderXma*)m_ActiveGrains[loop].decoder)->FlushPackets();
#endif

				m_ActiveGrains[loop].awaitingFlush = false;
				m_ActiveGrains[loop].isFirstFlush = false;
			}
		}
	}
}

// ----------------------------------------------------------------
// Generate a frame of data for this submix
// ----------------------------------------------------------------
u32 audGranularSubmix::GenerateFrame(f32* const destBuffer, 
									  f32 volumeScale,
									  const u32 incomingNumGrainsGenerated,
									  const f32 incomingGranularFraction, 
									  const f32 incomingGranuleFractionPerSample,
									  bool generateNative)
{
	u32 samplesGenerated = 0;

	if(m_IsMuted)
	{
		volumeScale = 0.0f;
	}

	if(!m_IsPlayingPhysically
#if __PS3
		// PS3 submixes will be muted until the VRAM loaders have initialized, so hold off mixing until then
		// Also don't start playback if we're going to have to switch grains this frame, as that'll be very expensive
		|| (m_FirstFrame && volumeScale == 0.0f) 
		|| (m_FirstFrame && s_NumSubixesStartedThisFrame > 0)
		|| (m_FirstFrame && (incomingGranularFraction + (incomingGranuleFractionPerSample * kMixBufNumSamples) >= 1.0f))
#endif
		)
	{
		m_GranularFractionInternal = incomingNumGrainsGenerated + (incomingGranularFraction + (incomingGranuleFractionPerSample * kMixBufNumSamples));
		m_NumGrainsGenerated = (u32)Floorf(m_GranularFractionInternal);
		m_GranularFractionInternal = m_GranularFractionInternal - m_NumGrainsGenerated;
		return 0;
	}

	// Pitch is set to zero (can occur when the game is paused) so don't try to mix as we'll just end up in an infinite loop
	if(incomingGranuleFractionPerSample == 0.0f)
	{
		return 0;
	}

#if !__SPU
	if(m_DecodersRequired)
#endif
	{
		samplesGenerated = GenerateFrameFromCompressedData(destBuffer, volumeScale, incomingNumGrainsGenerated, incomingGranularFraction, incomingGranuleFractionPerSample);
	}
#if !__SPU
	else
	{
		samplesGenerated = GenerateFrameFromPCMData(destBuffer, volumeScale, incomingNumGrainsGenerated, incomingGranularFraction, incomingGranuleFractionPerSample, generateNative);
	}
#endif

#if __PS3
	if(m_FirstFrame)
	{
		s_NumSubixesStartedThisFrame++;
	}
#endif

	m_FirstFrame = false;
	return samplesGenerated;
}

// ----------------------------------------------------------------
// Generate a frame of data for this submix
// ----------------------------------------------------------------
u32 audGranularSubmix::GenerateFrameFromCompressedData(f32* const destBuffer, 
									  f32 volumeScale, 
									  const u32 incomingNumGrainsGenerated,
									  const f32 incomingGranularFraction, 
									  const f32 incomingGranuleFractionPerSample)
{	
	PF_FUNC(GenerateFrameGranularDecodedSubmix); 
	f32 granuleFractionPerSample = incomingGranuleFractionPerSample; 
	u32 numGrainSwapsThisFrame = 0;

	// If this is the first frame of output since StartPhys() then we need to synchronize the submix to the current granular playback position
	if(m_FirstFrame 
#if __PS3
		|| m_StarvedLastFrame
#endif		
		)
	{
		Assertf(abs((s32)incomingNumGrainsGenerated - (s32)m_NumGrainsGenerated < 2), "Granular submix out of sync! Internal:%d External:%d", m_NumGrainsGenerated, incomingNumGrainsGenerated);

#if __XENON
		m_ActiveGrains[m_ActiveGrainIndex].decoder->Update();
		u32 activeGrainSamplesAvailable = m_ActiveGrains[m_ActiveGrainIndex].decoder->QueryNumAvailableSamples();
		f32 finalGranuleFraction = incomingGranularFraction + (granuleFractionPerSample * kMixBufNumSamples);
		f32 ratio = Clamp(m_ActiveGrains[m_ActiveGrainIndex].grainLength / (1.0f/incomingGranularFraction), 0.0f, (f32)kMaxSampleRateRatio);

		// If we've got enough samples available, we can simply jump to the correct point. If not, just lie and claim that we're there already. We may get once out of sync
		// grain, but given that we crossfade at the end anyway, its not really a major issue.
		if(activeGrainSamplesAvailable <= (m_ActiveGrains[m_ActiveGrainIndex].grainLength * finalGranuleFraction) + s_FFTFadeSkipSize + (s_resampleBufferOverlap * ratio))
		{
			m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded = (u32)(Floorf(m_ActiveGrains[m_ActiveGrainIndex].grainLength * incomingGranularFraction));
		}
#endif

		m_NumGrainsGenerated = incomingNumGrainsGenerated;
		m_GranularFractionInternal = incomingGranularFraction;
	}

	// If the granular fraction is not what we expect, we want to gradually drift back towards it.
	f32 incomingGranularPos = incomingNumGrainsGenerated + incomingGranularFraction;
	f32 internalGranularPos = m_NumGrainsGenerated + m_GranularFractionInternal;
	const f32 granularFractionThisBuffer = (incomingGranuleFractionPerSample * kMixBufNumSamples);
	const f32 desiredFinalFraction = incomingGranularPos + granularFractionThisBuffer;
	const f32 actualFinalFraction = internalGranularPos + granularFractionThisBuffer;
	f32 granularPositionDifference = desiredFinalFraction - actualFinalFraction;
	granularPositionDifference = Clamp(granularPositionDifference, -0.05f, 0.05f);
	granuleFractionPerSample = incomingGranuleFractionPerSample + (granularPositionDifference/(f32)kMixBufNumSamples);

	u32 samplesGenerated = 0;
	m_StarvedLastFrame = false;

#if __SPU
	f32* const mixBuffer = (f32*) g_ScratchMixBuffer;
#else
	f32* const mixBuffer = AllocaAligned(f32, s_MixBufferLength, 128);
#endif

	sysMemSet128(mixBuffer, 0, sizeof(f32) * s_MixBufferLength);

	if(m_InitialPacketsGenerated && granuleFractionPerSample > 0)
	{
#if __SPU
		s_NumActiveSubmixes++;
		s16* sampleGathererBuffer = (s16*) g_ScratchSampleGathererBuffer;
#else
		s16* sampleGathererBuffer = AllocaAligned(s16, s_SampleGathererBufferLength, 128);
#endif

#if __XENON
		// On X360 we try to interrupt the decoders as much as possible so that the hardware can do its thing
		for(s32 loop = 0; loop < m_NumStoredGrainStates; loop++)
		{
			PF_FUNC(UpdateGrainDecoders);
			// If this is the active grain, update it to keep the decoder ticking over
			// Otherwise if this is still decoding the tail end of the previous grain, update it to finish off the processing
			if(loop == m_ActiveGrainIndex ||
				(m_ActiveGrains[loop].decoder->GetState() == audDecoder::DECODING && !m_ActiveGrains[loop].packetSent))
			{
				m_ActiveGrains[loop].decoder->Update();

#if __BANK
				if(s_ForceGranularSubmixStarvation && loop == m_ActiveGrainIndex && audEngineUtil::ResolveProbability(0.05f))
				{
					((audDecoderXma*)m_ActiveGrains[loop].decoder)->DebugAbortPlayback();
					s_ForceGranularSubmixStarvation = false;
				}
#endif

				// If we've just finished processing all our data, we'll be sending a new packet imminently, so preemptively grab the lock
				if(m_ActiveGrains[loop].decoder->GetState() == audDecoder::FINISHED)
				{
					m_ActiveGrains[loop].decoder->PreUpdate();
				}
			}
			// If this is the next grain and we're going to finish mixing the current grain duing this frame, preemptively grab the lock
			else if(loop == (m_ActiveGrainIndex + 1) % m_NumStoredGrainStates && m_GranularFractionInternal + (granuleFractionPerSample * kMixBufNumSamples) >= 1)
			{
				m_ActiveGrains[loop].decoder->PreUpdate();
			}
		}

		audDecoder* currentGrainDecoder = m_ActiveGrains[m_ActiveGrainIndex].decoder;
		audDecoder* prevGrainDecoder = m_ActiveGrains[m_PrevGrainIndex].decoder;
#elif __SPU
		dmaWait(g_ScratchCrossfadeBufferTag);
		dmaGet(g_ScratchCrossfadeBuffer, m_CrossfadeSampleBufferPtr, RAGE_ALIGN(sizeof(s16) * 512, 4), false, g_ScratchCrossfadeBufferTag);

		bool prevGrainDecoderDirty = false;
		u32 prevGrainTag = g_ScratchDecoderTags[s_ScratchDecoderIndex];
		dmaWait(prevGrainTag);
		dmaGet(g_ScratchDecoders[s_ScratchDecoderIndex], m_ActiveGrains[m_PrevGrainIndex].decoder, sizeof(audDecoderSpu), false, prevGrainTag);
		audDecoderSpu* prevGrainDecoder = (audDecoderSpu*)g_ScratchDecoders[s_ScratchDecoderIndex];
		s_ScratchDecoderIndex = (s_ScratchDecoderIndex + 1) % 3;

		u32 currentGrainTag = g_ScratchDecoderTags[s_ScratchDecoderIndex];
		dmaWait(currentGrainTag);
		dmaGet(g_ScratchDecoders[s_ScratchDecoderIndex], m_ActiveGrains[m_ActiveGrainIndex].decoder, sizeof(audDecoderSpu), true, currentGrainTag);
		audDecoderSpu* currentGrainDecoder = (audDecoderSpu*)g_ScratchDecoders[s_ScratchDecoderIndex];
		s_ScratchDecoderIndex = (s_ScratchDecoderIndex + 1) % 3;
#else
		audDecoder* currentGrainDecoder = m_ActiveGrains[m_ActiveGrainIndex].decoder;
		audDecoder* prevGrainDecoder = m_ActiveGrains[m_PrevGrainIndex].decoder;
		currentGrainDecoder->Update();
		prevGrainDecoder->Update();
#endif

		const f32 oneOverGranuleFractionPerSample = 1.0f/granuleFractionPerSample;
		u32 pass = 0;

		while(samplesGenerated < kMixBufNumSamples && pass++ < 10)
		{
#if __SPU
			if(m_ActiveGrains[m_ActiveGrainIndex].packetSent && 
			   !m_ActiveGrains[m_ActiveGrainIndex].spuPacketSent)
			{
				if(m_ActiveGrains[m_ActiveGrainIndex].vramLoader)
				{
					currentGrainDecoder->ClearBuffer();
					currentGrainDecoder->SubmitPacket(m_ActiveGrains[m_ActiveGrainIndex].packet);
					UpdateSpuDecoder(currentGrainDecoder, 1);
					m_ActiveGrains[m_ActiveGrainIndex].spuPacketSent = true;
				}
			}

			UpdateSpuDecoder(currentGrainDecoder, 1);
#endif

			const bool withinCrossfadeSection = s_granularCrossfadeAmount > 0 && m_GranularFractionInternal < s_granularCrossfadeAmount && m_ActiveGrains[m_PrevGrainIndex].initialFadeSamplesRemaining == 0;
			UpdateGrainFlush(withinCrossfadeSection);

			u32 numSamplesAvailable = currentGrainDecoder->QueryNumAvailableSamples();
			u32 samplesRemainingInCrossfade = withinCrossfadeSection ? (u32)ceilf((s_granularCrossfadeAmount - m_GranularFractionInternal) * oneOverGranuleFractionPerSample) : kMixBufNumSamples;

			// Work out where in the grain we should start playback from
			const u32 samplesRemainingInGrain = (u32)ceilf((1.0f - m_GranularFractionInternal) * oneOverGranuleFractionPerSample);

			// Work out how many samples are needed
			u32 samplesRequired = Min(samplesRemainingInCrossfade, Min(kMixBufNumSamples - samplesGenerated, samplesRemainingInGrain));

			// Prevent deadlock
			if (!Verifyf(samplesRequired > 0, "0 samples required."))
				break;

			if(numSamplesAvailable > 0)
			{
				// Work out how fast we should be stepping through the data
				const f32 ratio = Clamp(m_ActiveGrains[m_ActiveGrainIndex].grainLength * granuleFractionPerSample, 0.0f, (f32)kMaxSampleRateRatio);

				// First n samples have a fade in applied by the decode FFT, so skip them
				if(m_ActiveGrains[m_ActiveGrainIndex].initialFadeSamplesRemaining > 0)
				{
#if __XENON
					audGranularAssertf(numSamplesAvailable >= s_FFTFadeSkipSize, "Insufficient samples available! (%d)", numSamplesAvailable);

					if(numSamplesAvailable >= s_FFTFadeSkipSize)
					{
						currentGrainDecoder->AdvanceReadPtr(s_FFTFadeSkipSize);
						numSamplesAvailable = currentGrainDecoder->QueryNumAvailableSamples();
					}

					m_ActiveGrains[m_ActiveGrainIndex].initialFadeSamplesRemaining = 0;
#elif __SPU
					while(m_ActiveGrains[m_ActiveGrainIndex].initialFadeSamplesRemaining > 0)
					{
						numSamplesAvailable = UpdateSpuDecoder(currentGrainDecoder, m_ActiveGrains[m_ActiveGrainIndex].initialFadeSamplesRemaining);
						u32 samplesToSkip = Min(numSamplesAvailable, m_ActiveGrains[m_ActiveGrainIndex].initialFadeSamplesRemaining);

						if(audVerifyf(samplesToSkip > 0, "No fade samples to skip, bailing out!"))
						{
							currentGrainDecoder->AdvanceReadPtr(samplesToSkip);
							m_ActiveGrains[m_ActiveGrainIndex].initialFadeSamplesRemaining -= samplesToSkip;
							numSamplesAvailable = currentGrainDecoder->QueryNumAvailableSamples();
						}
						else
						{
							break;
						}
					}
#endif
					audAssert(m_ActiveGrains[m_ActiveGrainIndex].initialFadeSamplesRemaining == 0);
				}

				f32 currentSamplePos = m_ActiveGrains[m_ActiveGrainIndex].grainLength * m_GranularFractionInternal;
				u32 currentSamplePosFloor = (u32) Floorf(currentSamplePos);

				// If our decoder position is behind the current play position, catch up
				if(m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded < currentSamplePosFloor)
				{
#if __SPU
					do 
					{
						u32 samplesToSkip = currentSamplePosFloor - m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded;
						numSamplesAvailable = UpdateSpuDecoder(currentGrainDecoder, samplesToSkip);
						samplesToSkip = Min(numSamplesAvailable, samplesToSkip);

						if(audVerifyf(numSamplesAvailable > 0, "No samples to skip, bailing out!"))
						{
							currentGrainDecoder->AdvanceReadPtr(samplesToSkip);
							m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded += samplesToSkip;
							numSamplesAvailable = currentGrainDecoder->QueryNumAvailableSamples();
						}
						else
						{
							break;
						}
					} while (m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded < currentSamplePosFloor);
#else
					u32 samplesToSkip = currentSamplePosFloor - m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded;
					audGranularAssertf(samplesToSkip <= numSamplesAvailable, "(%d < %d) Not enough samples to skip to desired point! %d > %d", m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded, currentSamplePosFloor, samplesToSkip, numSamplesAvailable);
					currentGrainDecoder->AdvanceReadPtr(Min(samplesToSkip, numSamplesAvailable));
					m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded += samplesToSkip;
					numSamplesAvailable = currentGrainDecoder->QueryNumAvailableSamples();
#endif
				}
				// Check that we haven't moved the decoder read point beyond the current play point. The granular fraction drift fix at the 
				// top of the function is designed to prevent this by gradually realigning the playback position rather than just jumping backwards 
				// in the data
				else if(m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded > currentSamplePosFloor)
				{
					audWarningf("Granular submix attempting to read from discarded data! (%d < %d). Grain index %d, granular fraction %f, grain length %d", currentSamplePosFloor, m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded, m_ActiveGrainIndex, m_GranularFractionInternal, m_ActiveGrains[m_ActiveGrainIndex].grainLength);
				}

				if(volumeScale > 0.0f)
				{
					if(withinCrossfadeSection)
					{		
						PF_FUNC(GranularCrossfade);

#if __SPU
						audAutoScratchBookmark scratchBookmark;
						f32* const crossfadeMixBufferActiveSample = (f32*) AllocateFromScratch(sizeof(f32) * samplesRequired, 16, "SampleDataGatherBufferActive");
						f32* const crossfadeMixBufferPrevSample = (f32*) AllocateFromScratch(sizeof(f32) * samplesRequired, 16, "SampleDataGatherBufferPrev");
#else
						f32* const crossfadeMixBufferActiveSample = AllocaAligned(f32, samplesRequired, 16);
						f32* const crossfadeMixBufferPrevSample = AllocaAligned(f32, samplesRequired, 16);
#endif

						u32 inputSamplesNeeded = 2 + (u32)ceilf((samplesRequired + s_resampleBufferOverlap) * ratio);

#if __SPU
						numSamplesAvailable = UpdateSpuDecoder(currentGrainDecoder, inputSamplesNeeded);
#endif
						audGranularAssertf(inputSamplesNeeded <= s_SampleGathererBufferLength && inputSamplesNeeded <= numSamplesAvailable, "Insufficient input samples! Required: %d Available: %d Ratio %f", inputSamplesNeeded, numSamplesAvailable, ratio);
						sysMemSet128(sampleGathererBuffer, 0, sizeof(s16) * s_SampleGathererBufferLength);
						u32 samplesRead = currentGrainDecoder->ReadData(sampleGathererBuffer, inputSamplesNeeded);
						Assert(inputSamplesNeeded == samplesRead);

						// Now mix the required number of decoded samples to the mix buffer using the calculated pitch ratio
#if __BANK
						if(m_ToneGeneratorEnabled)
						{
							f32 tempGranularFraction = m_GranularFractionInternal;
							sysMemSet(crossfadeMixBufferActiveSample, 0, sizeof(f32) * samplesRequired);

							for(u32 loop = 0; loop < samplesRequired; loop++)
							{
								crossfadeMixBufferActiveSample[loop] += sin(PI * 4.0f * tempGranularFraction) * s_ClippingVolumeFix;
								tempGranularFraction += granuleFractionPerSample;
							}
						}
						else
#endif
						{
							f32 trailingFrac = currentSamplePos - currentSamplePosFloor;
							ASSERT_ONLY(u32 samplesProcessed = )audRateConvertorLinear::Process(ratio, &crossfadeMixBufferActiveSample[0], sampleGathererBuffer, Min(samplesRead, 1024u), samplesRequired, trailingFrac);
							Assert(samplesProcessed == samplesRequired);
						}

						if(m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain)
						{
							ScaleVolume(crossfadeMixBufferActiveSample, samplesRequired, m_SkippedGrainVol);
						}

						u32 prevGrainSamplesAvailable = 0;
						const f32 prevGrainSamplePos = m_ActiveGrains[m_PrevGrainIndex].grainLength * (1.0f + m_GranularFractionInternal);
						const u32 prevGrainSamplePosFloor = (u32) Floorf(prevGrainSamplePos);

#if __SPU
						prevGrainSamplesAvailable = s_OverlapSamplesForCrossfade - (prevGrainSamplePosFloor - m_ActiveGrains[m_PrevGrainIndex].grainLength);
						Assertf(prevGrainSamplesAvailable >= 0 && prevGrainSamplesAvailable <= s_OverlapSamplesForCrossfade, "Prev Grain samples: %d", prevGrainSamplesAvailable);
#else
						if(prevGrainDecoder)
						{
							prevGrainSamplesAvailable = prevGrainDecoder->QueryNumAvailableSamples();

							// Check that we haven't already gone past the play point. If we're behind, catch up
							if(prevGrainSamplePosFloor > m_ActiveGrains[m_PrevGrainIndex].samplesDecoded)
							{
								const u32 samplesToSkip = prevGrainSamplePosFloor - m_ActiveGrains[m_PrevGrainIndex].samplesDecoded;
								audGranularAssertf(samplesToSkip <= prevGrainSamplesAvailable, "(%d < %d) Not enough samples to skip to desired point! %d > %d", m_ActiveGrains[m_PrevGrainIndex].samplesDecoded, prevGrainSamplePosFloor, samplesToSkip, prevGrainSamplesAvailable);
								prevGrainDecoder->AdvanceReadPtr(Min(samplesToSkip, prevGrainSamplesAvailable));
								m_ActiveGrains[m_PrevGrainIndex].samplesDecoded += samplesToSkip;
								prevGrainSamplesAvailable = prevGrainDecoder->QueryNumAvailableSamples();
							}
							else if(prevGrainSamplePosFloor < m_ActiveGrains[m_PrevGrainIndex].samplesDecoded)
							{
								audWarningf("Granular crossfade attempting to read from discarded data! (%d < %d). Grain index %d, granular fraction %f, grain length %d", prevGrainSamplePosFloor, m_ActiveGrains[m_PrevGrainIndex].samplesDecoded, m_PrevGrainIndex, m_GranularFractionInternal, m_ActiveGrains[m_PrevGrainIndex].grainLength);
							}
						}
#endif

						const f32 prevGrainRatio = Clamp(m_ActiveGrains[m_PrevGrainIndex].grainLength * granuleFractionPerSample, 0.0f, (f32)kMaxSampleRateRatio);
						inputSamplesNeeded = 2 + (u32)ceilf((samplesRequired + s_resampleBufferOverlap) * prevGrainRatio);
						audGranularAssertf(inputSamplesNeeded <= s_SampleGathererBufferLength && inputSamplesNeeded <= prevGrainSamplesAvailable, "Insufficient input samples! Required: %d Available: %d Ratio %f", inputSamplesNeeded, prevGrainSamplesAvailable, ratio);
						sysMemSet128(sampleGathererBuffer, 0, sizeof(s16) * s_SampleGathererBufferLength);

#if __SPU
						samplesRead = inputSamplesNeeded;
						sysMemCpy((void*)sampleGathererBuffer, (void*)(&((s16*)g_ScratchCrossfadeBuffer)[prevGrainSamplePosFloor - m_ActiveGrains[m_PrevGrainIndex].grainLength]), sizeof(s16) * inputSamplesNeeded);
#else
						samplesRead = prevGrainDecoder->ReadData(sampleGathererBuffer, inputSamplesNeeded);
						Assert(inputSamplesNeeded == samplesRead);
#endif

						// Now mix the required number of decoded samples to the mix buffer using the calculated pitch ratio
#if __BANK
						if(m_ToneGeneratorEnabled)
						{
							f32 tempGranularFraction = m_GranularFractionInternal;
							sysMemSet(crossfadeMixBufferPrevSample, 0, sizeof(f32) * samplesRequired);

							for(u32 loop = 0; loop < samplesRequired; loop++)
							{
								crossfadeMixBufferPrevSample[loop] += sin(PI * 4.0f * tempGranularFraction) * s_ClippingVolumeFix;
								tempGranularFraction += granuleFractionPerSample;
							}
						}
						else
#endif
						{
							f32 trailingFrac = prevGrainSamplePos - prevGrainSamplePosFloor;
							ASSERT_ONLY(u32 samplesProcessed = )audRateConvertorLinear::Process(prevGrainRatio, &crossfadeMixBufferPrevSample[0], sampleGathererBuffer, Min(samplesRead, 1024u), samplesRequired, trailingFrac);						
							Assert(samplesProcessed == samplesRequired);
						}

						if(m_ActiveGrains[m_PrevGrainIndex].skipThisGrain)
						{
							ScaleVolume(crossfadeMixBufferPrevSample, samplesRequired, m_SkippedGrainVol);
						}

						// Mix all our generated data to the main mix buffer
						if(m_InterGrainCrossfadeStyle == CrossfadeStyleEqualPower)
						{
							const f32 PI_over_2 = (PI / 2.0f);
							f32 granuleLoopFraction = 0.0f;
							for(u32 loop = 0; loop < samplesRequired; loop++)
							{
								const f32 activeGrainVolumeLin = Clamp((m_GranularFractionInternal + (granuleFractionPerSample * loop)) * s_oneOverCrossfadeAmount, 0.0f, 1.0f);
								const f32 activeGrainVolume = Sinf(activeGrainVolumeLin * PI_over_2);
								const f32 prevGrainVolume = Sinf((1.f - activeGrainVolumeLin) * PI_over_2);
								mixBuffer[samplesGenerated + loop] = (crossfadeMixBufferPrevSample[loop] * prevGrainVolume) + (crossfadeMixBufferActiveSample[loop] * activeGrainVolume);
								granuleLoopFraction += granuleFractionPerSample;
							}
						}
						else
						{
							f32 granuleLoopFraction = 0.0f;
							for(u32 loop = 0; loop < samplesRequired; loop++)
							{
								const f32 activeGrainVolume = Clamp((m_GranularFractionInternal + granuleLoopFraction) * s_oneOverCrossfadeAmount, 0.0f, 1.0f);
								const f32 prevGrainVolume = 1.0f - activeGrainVolume;
								mixBuffer[samplesGenerated + loop] = (crossfadeMixBufferPrevSample[loop] * prevGrainVolume) + (crossfadeMixBufferActiveSample[loop] * activeGrainVolume);
								granuleLoopFraction += granuleFractionPerSample;
							}
						}

						AdvanceActiveGrainReadPtr((void*) currentGrainDecoder, m_ActiveGrainIndex, m_GranularFractionInternal, granuleFractionPerSample, samplesRequired);

#if !__SPU
						AdvanceActiveGrainReadPtr((void*) prevGrainDecoder, m_PrevGrainIndex, m_GranularFractionInternal, granuleFractionPerSample, samplesRequired);
#endif


						// Advance our read position
						m_GranularFractionInternal += granuleFractionPerSample * samplesRequired;
						samplesGenerated += samplesRequired;

						numSamplesAvailable = currentGrainDecoder->QueryNumAvailableSamples();

#if !__SPU
						if(prevGrainDecoder)
						{
							prevGrainSamplesAvailable = prevGrainDecoder->QueryNumAvailableSamples();
						}
#endif
					}
					else
					{
#if __SPU
						const u32 decoderSamplesRequired = 2 + (u32)ceilf((samplesRequired + s_resampleBufferOverlap) * ratio);
						numSamplesAvailable = UpdateSpuDecoder(currentGrainDecoder, decoderSamplesRequired);
#endif

						// audRateConvertorLinear::Process requires that the mix buffer is 16 byte aligned, so do a slow mix until true
						while((((size_t)&mixBuffer[samplesGenerated])&15)!=0 && samplesRequired > 0 && numSamplesAvailable > 0 && samplesGenerated < kMixBufNumSamples)
						{
							PF_FUNC(AlignmentFix);

							// Read our current/next sample in from the decoder and write the interpolated sample to the mix buffer
							currentGrainDecoder->ReadData(sampleGathererBuffer, 2);
							const s16 prevSample16 = sampleGathererBuffer[0];
							const s16 nextSample16 = sampleGathererBuffer[1]; 

#if __BANK
							if(m_ToneGeneratorEnabled)
							{
								mixBuffer[samplesGenerated] += sin(PI * 4.0f * m_GranularFractionInternal) * s_ClippingVolumeFix;
							}
							else
#endif
							{
								mixBuffer[samplesGenerated] = Lerp(currentSamplePos - currentSamplePosFloor, prevSample16, nextSample16) * s_s16ToFloatDivisor;
							}

							if(m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain)
							{
								mixBuffer[samplesGenerated] *= m_SkippedGrainVol;
							}

							AdvanceActiveGrainReadPtr((void*) currentGrainDecoder, m_ActiveGrainIndex, m_GranularFractionInternal, granuleFractionPerSample, 1);
							numSamplesAvailable = currentGrainDecoder->QueryNumAvailableSamples();
							m_GranularFractionInternal += granuleFractionPerSample;							

							currentSamplePos = m_ActiveGrains[m_ActiveGrainIndex].grainLength * m_GranularFractionInternal;
							currentSamplePosFloor = (u32) Floorf(currentSamplePos);

							samplesRequired--;
							samplesGenerated++;
						}

						// Check if we have more stuff to do - the mix buffer write point is now aligned so we can now do fast vectorised resample/mix
						if(samplesRequired > 0 && numSamplesAvailable > 0)
						{
							// First read in as many decoded samples as we require to produce our output
							const u32 inputSamplesNeeded = 2 + (u32)ceilf((samplesRequired + s_resampleBufferOverlap) * ratio);
#if __SPU
							numSamplesAvailable = UpdateSpuDecoder(currentGrainDecoder, inputSamplesNeeded);
#endif
							audGranularAssertf(inputSamplesNeeded <= s_SampleGathererBufferLength && inputSamplesNeeded <= numSamplesAvailable, "Insufficient input samples! Required: %d Available: %d Ratio %f", inputSamplesNeeded, numSamplesAvailable, ratio);
							u32 samplesRead = 0;

							{
								PF_FUNC(ReadDecodedData);
								sysMemSet128(sampleGathererBuffer, 0, sizeof(s16) * s_SampleGathererBufferLength);
								samplesRead = currentGrainDecoder->ReadData(sampleGathererBuffer, inputSamplesNeeded);
								Assert(inputSamplesNeeded == samplesRead);
							}

							// Now mix the required number of decoded samples to the mix buffer using the calculated pitch ratio
#if __BANK
							if(m_ToneGeneratorEnabled)
							{
								f32 tempGranularFraction = m_GranularFractionInternal;

								for(u32 loop = 0; loop < samplesRequired; loop++)
								{
									mixBuffer[samplesGenerated + loop] += sin(PI * 4.0f * tempGranularFraction) * s_ClippingVolumeFix;
									tempGranularFraction += granuleFractionPerSample;
								}
							}
							else
#endif
							{
								const f32 trailingFrac = currentSamplePos - currentSamplePosFloor;

								{
									PF_FUNC(SampleRateConversion);
									ASSERT_ONLY(u32 samplesProcessed = )audRateConvertorLinear::Process(ratio, &mixBuffer[samplesGenerated], sampleGathererBuffer, Min(samplesRead, 1024u), samplesRequired, trailingFrac);
									Assert(samplesProcessed == samplesRequired);
								}
							}

							if(m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain)
							{
								ScaleVolume(&mixBuffer[samplesGenerated], samplesRequired, m_SkippedGrainVol);
							}

							AdvanceActiveGrainReadPtr((void*) currentGrainDecoder, m_ActiveGrainIndex, m_GranularFractionInternal, granuleFractionPerSample, samplesRequired);
							m_GranularFractionInternal += (granuleFractionPerSample * samplesRequired);
							samplesGenerated += samplesRequired;
						}
					}
				}
				else // Submix is muted
				{
					if(withinCrossfadeSection)
					{	
						SPU_ONLY(dmaWait(prevGrainTag));

						AdvanceActiveGrainReadPtr((void*) prevGrainDecoder, m_PrevGrainIndex, m_GranularFractionInternal, granuleFractionPerSample, samplesRequired);
						AdvanceActiveGrainReadPtr((void*) currentGrainDecoder, m_ActiveGrainIndex, m_GranularFractionInternal, granuleFractionPerSample, samplesRequired);
					}
					else
					{
						AdvanceActiveGrainReadPtr((void*) currentGrainDecoder, m_ActiveGrainIndex, m_GranularFractionInternal, granuleFractionPerSample, samplesRequired);
					}

					m_GranularFractionInternal += (granuleFractionPerSample * samplesRequired);
					samplesGenerated += samplesRequired;
				}
			}
			else
			{
#if __BANK
#if __PS3
				if(m_ActiveGrains[m_ActiveGrainIndex].vramLoader)
#endif
				{
#if __ASSERT
#if __SPU
					const char* bankName = "Unknown";
#else
					const char* bankName = m_BankName;
#endif
					audWarningf("Granular submix is being starved! Wave %d, Bank %s, Grain index %d, Grain %d, Samples decoded %d/%d, Step Rate per sample %f", m_WavenameHash, bankName, m_ActiveGrainIndex, m_ActiveGrains[m_ActiveGrainIndex].grainIndex, m_ActiveGrains[m_ActiveGrainIndex].samplesDecoded, m_ActiveGrains[m_ActiveGrainIndex].grainLength, granuleFractionPerSample);
#endif
				}
#endif

#if __PS3
				if(!m_ActiveGrains[m_ActiveGrainIndex].vramLoader)
				{
					m_StarvedLastFrame = true;
					break;
				}
#endif

				samplesGenerated += samplesRequired;
				m_GranularFractionInternal += granuleFractionPerSample * samplesGenerated;
			}

			// Consumed a whole grain?
			if(m_GranularFractionInternal >= 1.0f)
			{
#if __SPU
				UpdateSpuDecoder(currentGrainDecoder, s_OverlapSamplesForCrossfade);
				sysMemZeroBytes<sizeof(s16) * 512>(g_ScratchCrossfadeBuffer);
				currentGrainDecoder->ReadData((s16*)g_ScratchCrossfadeBuffer, Min(s_OverlapSamplesForCrossfade, currentGrainDecoder->QueryNumAvailableSamples()));
				dmaWait(g_ScratchCrossfadeBufferTag);
				dmaPut(g_ScratchCrossfadeBuffer, m_CrossfadeSampleBufferPtr, RAGE_ALIGN(sizeof(s16) * 512, 4), false, g_ScratchCrossfadeBufferTag);
				dmaWait(prevGrainTag);
				prevGrainDecoderDirty = true;
#endif

				// Move to the next grain
				audAssertf(numGrainSwapsThisFrame == 0, "Multiple grain swaps in a single frame! Not supported. Fraction per sample: %f", incomingGranuleFractionPerSample);
				m_GranularFractionInternal -= 1.0f;
				m_NumGrainsGenerated++;
				numGrainSwapsThisFrame++;
				MoveToNextGrain();

#if !__SPU
				currentGrainDecoder = m_ActiveGrains[m_ActiveGrainIndex].decoder;
				prevGrainDecoder = m_ActiveGrains[m_PrevGrainIndex].decoder;

				// Do an update to ensure our decoder is up to date
				{
					PF_FUNC(UpdateGrainDecoders);
					currentGrainDecoder->Update();
				}
#else
				// Re-fetch the new grain decoders
				audDecoderSpu* tempDecoder = currentGrainDecoder;
				currentGrainDecoder = prevGrainDecoder;
				prevGrainDecoder = tempDecoder;
#endif
			}
		}

		const bool withinCrossfadeSection = s_granularCrossfadeAmount > 0 && m_GranularFractionInternal < s_granularCrossfadeAmount && m_ActiveGrains[m_PrevGrainIndex].initialFadeSamplesRemaining == 0;
		UpdateGrainFlush(withinCrossfadeSection);

#if __SPU
		// Send any packets that need to be sent
		if(m_InitialPacketsGenerated && m_IsPlayingPhysically)
		{
			for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
			{ 
				if(m_ActiveGrains[loop].decoder)
				{
					bool isPrevGrain = (loop == m_PrevGrainIndex);

					if(m_ActiveGrains[loop].packetSent && 
					   !m_ActiveGrains[loop].spuPacketSent &&
					   m_ActiveGrains[loop].vramLoader)
					{
						if(isPrevGrain)
						{	
							dmaWait(prevGrainTag);
						}

						audDecoderSpu* grainDecoder = loop == m_ActiveGrainIndex? currentGrainDecoder:prevGrainDecoder;
						grainDecoder->ClearBuffer();
						grainDecoder->SubmitPacket(m_ActiveGrains[loop].packet);
						m_ActiveGrains[loop].spuPacketSent = true;
						m_ActiveGrains[loop].canDecodeMoreData = true;
						prevGrainDecoderDirty |= isPrevGrain;
					}
					else if(m_ActiveGrains[loop].packetSent && 
						m_ActiveGrains[loop].spuPacketSent)
					{
						if(isPrevGrain)
						{	
							dmaWait(prevGrainTag);
						}

						audDecoderSpu* grainDecoder = loop == m_ActiveGrainIndex? currentGrainDecoder:prevGrainDecoder;
						m_ActiveGrains[loop].canDecodeMoreData = grainDecoder->CanDecodeMoreData();
					}
					else
					{
						m_ActiveGrains[loop].canDecodeMoreData = false;
					}
				}
			}
		}

		dmaPut(currentGrainDecoder, m_ActiveGrains[m_ActiveGrainIndex].decoder, sizeof(audDecoderSpu), false, currentGrainTag);

		if(prevGrainDecoderDirty)
		{
			dmaPut(prevGrainDecoder, m_ActiveGrains[m_PrevGrainIndex].decoder, sizeof(audDecoderSpu), false, prevGrainTag);
		}
#endif
	}

	{
		if(samplesGenerated > 0)
		{
			PF_FUNC(FinalMixDown);

			if(volumeScale > 0.0f || m_PrevVolumeScale > 0.0f)
			{
				Vector_4V volumeVecStart = V4LoadScalar32IntoSplatted(Clamp(m_PrevVolumeScale * s_ClippingVolumeFix, 0.0f, 1.0f));
				Vector_4V volumeVecEnd = V4LoadScalar32IntoSplatted(Clamp(volumeScale * s_ClippingVolumeFix, 0.0f, 1.0f));
				MixMonoBufToMonoBuf<kMixBufNumSamples>(mixBuffer, destBuffer, volumeVecStart, volumeVecEnd);
			}

			m_PrevVolumeScale = volumeScale;
		}
	}

	return samplesGenerated;
}

#if __SPU
// ----------------------------------------------------------------
// audGranularSubmix SpuPostMix
// ----------------------------------------------------------------
bool audGranularSubmix::SpuPostMix()
{	
	bool submixUpdated = false;

	if(m_InitialPacketsGenerated && m_IsPlayingPhysically)
	{
		for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
		{
			// Start at the next grain and work round from there
			u32 decoderIndex = (m_ActiveGrainIndex + 1 + loop) % m_NumStoredGrainStates;

			if(m_ActiveGrains[decoderIndex].decoder)
			{
				if(m_ActiveGrains[decoderIndex].canDecodeMoreData)
				{
					if(m_ActiveGrains[decoderIndex].packetSent && 
						m_ActiveGrains[decoderIndex].spuPacketSent)
					{
						if((s_NumDecodesThisFrame < m_AllowedDecodesPerFrame) ||
							(m_IsPlayingPhysically && m_FirstFrame && decoderIndex == m_ActiveGrainIndex))
						{
							dmaWait(g_ScratchDecoderTags[s_ScratchDecoderIndex]);
							dmaGet(g_ScratchDecoders[s_ScratchDecoderIndex], m_ActiveGrains[decoderIndex].decoder, sizeof(audDecoderSpu));
							audDecoderSpu* grainDecoder = (audDecoderSpu*)g_ScratchDecoders[s_ScratchDecoderIndex];

							if(m_ActiveGrains[decoderIndex].initialFadeSamplesRemaining > 0)
							{
								u32 numSamplesAvailable = UpdateSpuDecoder(grainDecoder, m_ActiveGrains[decoderIndex].initialFadeSamplesRemaining, 1);
								u32 samplesToSkip = Min(numSamplesAvailable, m_ActiveGrains[decoderIndex].initialFadeSamplesRemaining);
								grainDecoder->AdvanceReadPtr(samplesToSkip);
								m_ActiveGrains[decoderIndex].initialFadeSamplesRemaining -= samplesToSkip;
								submixUpdated = true;
							}
							
							if(s_NumDecodesThisFrame < m_AllowedDecodesPerFrame)
							{
								UpdateSpuDecoder(grainDecoder, -1, 1);
							}

							dmaPut(g_ScratchDecoders[s_ScratchDecoderIndex], m_ActiveGrains[decoderIndex].decoder, sizeof(audDecoderSpu), false, g_ScratchDecoderTags[s_ScratchDecoderIndex]);
							s_ScratchDecoderIndex = (s_ScratchDecoderIndex + 1) % 3;
						}
					}
				}
			}
		}
	}

	return submixUpdated;
}
#endif

#if __SPU
// ----------------------------------------------------------------
// Update the spu decoder
// ----------------------------------------------------------------
u32 audGranularSubmix::UpdateSpuDecoder(audDecoderSpu* decoder, s32 samplesRequired, s32 maxUpdates)
{
	s32 prevSamplesAvailable = -1;
	s32 currentSamplesAvailable = decoder->QueryNumAvailableSamples();
	s32 numUpdates = 0;
	u32 attempts = 0;

	while((currentSamplesAvailable < samplesRequired || samplesRequired == -1) && 
		(numUpdates < maxUpdates || maxUpdates == -1) && 
		(currentSamplesAvailable != prevSamplesAvailable) &&
		attempts < 10)
	{
		u32 initialFramesDecoded = decoder->GetFramesDecoded();
		prevSamplesAvailable = currentSamplesAvailable;
		decoder->Update();
		currentSamplesAvailable = decoder->QueryNumAvailableSamples();

		if(decoder->GetFramesDecoded() != initialFramesDecoded)
		{
			s_NumDecodesThisFrame++;
			numUpdates++;
		}

		attempts++;
	}

	return (u32) currentSamplesAvailable;
}
#endif

// ----------------------------------------------------------------
// Scale the volume of the buffer by a given amount
// ----------------------------------------------------------------
void audGranularSubmix::ScaleVolume(f32 *RESTRICT inOutBuffer, u32 numSamples, f32 volumeScale)
{
	Assert((((size_t)inOutBuffer)&15)==0);
	const u32 numOutputSamplesAligned = numSamples & (~3U);	
	Vector_4V volumeVec = V4LoadScalar32IntoSplatted(volumeScale);

	Vec::Vector_4V *vecPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
	u32 count = numOutputSamplesAligned >> 2;
	while(count--)
	{
		*vecPtr = Vec::V4Scale(*vecPtr, volumeVec);
		vecPtr++;
	}

	for(u32 loop = numOutputSamplesAligned; loop < numSamples; loop++)
	{
		inOutBuffer[loop] *= volumeScale;
	}
}

#if !__SPU
// ----------------------------------------------------------------
// Generate a frame of data for this submix
// ----------------------------------------------------------------
u32 audGranularSubmix::GenerateFrameFromPCMData(f32* const destBuffer, 
									  const f32 volumeScale, 
									  const u32 incomingNumGrainsGenerated, 
									  const f32 incomingGranularFraction, 
									  const f32 incomingGranuleFractionPerSample,
									  const bool generateNative)
{
	PF_FUNC(GenerateFrameGranularSubmix); 

	u32 numGrainSwapsThisFrame = 0;
	f32 granuleFractionPerSample = incomingGranuleFractionPerSample; 

	// If this is the first frame of output since StartPhys() then we need to synchronize the submix to the current granular playback position
	if(m_FirstFrame)
	{
		Assertf(abs((s32)incomingNumGrainsGenerated - (s32)m_NumGrainsGenerated < 2), "Granular submix out of sync! Internal:%d External:%d", m_NumGrainsGenerated, incomingNumGrainsGenerated);
		m_NumGrainsGenerated = incomingNumGrainsGenerated;
		m_GranularFractionInternal = incomingGranularFraction;
	}

	// If the granular fraction is not what we expect, we want to gradually drift back towards it.
	f32 incomingGranularPos = incomingNumGrainsGenerated + incomingGranularFraction;
	f32 internalGranularPos = m_NumGrainsGenerated + m_GranularFractionInternal;
	const f32 granularFractionThisBuffer = (incomingGranuleFractionPerSample * kMixBufNumSamples);
	const f32 desiredFinalFraction = incomingGranularPos + granularFractionThisBuffer;
	const f32 actualFinalFraction = internalGranularPos + granularFractionThisBuffer;
	f32 granularPositionDifference = desiredFinalFraction - actualFinalFraction;
	granularPositionDifference = Clamp(granularPositionDifference, -0.05f, 0.05f);
	granuleFractionPerSample = incomingGranuleFractionPerSample + (granularPositionDifference/(f32)kMixBufNumSamples);

	if(generateNative)
	{
		granuleFractionPerSample = 1.0f/m_ActiveGrains[m_ActiveGrainIndex].grainLength;
	}

	f32* mixBuffer = AllocaAligned(f32, s_MixBufferLength, 128 );
	sysMemSet128(mixBuffer, 0, sizeof(f32) * s_MixBufferLength);

	u32 samplesGenerated = 0;

	if(granuleFractionPerSample > 0)
	{
		u32 pass = 0;

		while(samplesGenerated < kMixBufNumSamples && pass++ < 10)
		{
			const u32 samplesRemainingInGrain = (u32)ceilf((1.0f - m_GranularFractionInternal)/granuleFractionPerSample);
			u32 samplesRemainingInCrossfade = kMixBufNumSamples;

			if(m_GranularFractionInternal < s_granularCrossfadeAmount)
			{
				samplesRemainingInCrossfade = (u32)ceilf((s_granularCrossfadeAmount - m_GranularFractionInternal)/granuleFractionPerSample);
			}

			// Generate enough samples to take us to the point where we need to recalculate stuff. This will either be:
			// - The end of the current grain (when we need to calculate the next grain to use, fiddle with pointers etc.)
			// - The end of the buffer (we've calculated enough data, and can simply stop)
			// - The end of the inter-granular crossfade (at which point we no longer need to bother crossfading and can switch back to the regular mixing)
			u32 samplesRequired = Min(kMixBufNumSamples - s_resampleBufferOverlap, Min(samplesRemainingInCrossfade, Min(kMixBufNumSamples - samplesGenerated, samplesRemainingInGrain)));
			bool withinCrossfade = m_GranularFractionInternal < s_granularCrossfadeAmount;
			UpdateGrainFlush(withinCrossfade);

			// Prevent deadlock
			if (!Verifyf(samplesRequired > 0, "0 samples required."))
				return samplesGenerated;

			// Crossfading section
			if(withinCrossfade)
			{
				s16* sampleGathererBuffer = AllocaAligned(s16, s_SampleGathererBufferLength, 128);
				f32* const crossfadeMixBufferActiveSample = AllocaAligned(f32, ALIGN(samplesRequired, 128), 128);
				f32* const crossfadeMixBufferPrevSample = AllocaAligned(f32, ALIGN(samplesRequired, 128), 128);

				sysMemSet128(sampleGathererBuffer, 0, sizeof(s16) * s_SampleGathererBufferLength);
				sysMemSet128(crossfadeMixBufferActiveSample, 0, sizeof(f32) * ALIGN(samplesRequired, 128));
				sysMemSet128(crossfadeMixBufferPrevSample, 0, sizeof(f32) * ALIGN(samplesRequired, 128));

				const f32 ratio = Clamp(m_ActiveGrains[m_ActiveGrainIndex].grainLength / (1.0f/granuleFractionPerSample), 0.0f, (f32)kMaxSampleRateRatio);
				u32 inputSamplesNeeded = 2 + (u32)ceilf((samplesRequired + s_resampleBufferOverlap) * ratio);
				Assert(inputSamplesNeeded <= s_SampleGathererBufferLength);
				const f32 currentSamplePos = m_ActiveGrains[m_ActiveGrainIndex].grainLength * m_GranularFractionInternal;
				const u32 currentSamplePosFloor = (u32) Floorf(currentSamplePos);
				sysMemCpy((void*)sampleGathererBuffer, (const void*)&m_WaveData[m_ActiveGrains[m_ActiveGrainIndex].startSample + currentSamplePosFloor], Min(inputSamplesNeeded, s_SampleGathererBufferLength) * sizeof(s16));
				f32 trailingFrac = currentSamplePos - currentSamplePosFloor;
				audRateConvertorLinear::Process(ratio, &crossfadeMixBufferActiveSample[0], sampleGathererBuffer, Min(inputSamplesNeeded, 1024u), samplesRequired, trailingFrac);

				if(m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain)
				{
					ScaleVolume(crossfadeMixBufferActiveSample, samplesRequired, m_SkippedGrainVol);
				}

				const f32 prevGrainSamplePos = m_ActiveGrains[m_PrevGrainIndex].grainLength * (1.0f + m_GranularFractionInternal);
				const u32 prevGrainSamplePosFloor = (u32) Floorf(prevGrainSamplePos);
				const f32 prevGrainRatio = Clamp(m_ActiveGrains[m_PrevGrainIndex].grainLength * granuleFractionPerSample, 0.0f, (f32)kMaxSampleRateRatio);
				inputSamplesNeeded = 2 + (u32)ceilf((samplesRequired + s_resampleBufferOverlap) * prevGrainRatio);
				Assert(inputSamplesNeeded <= s_SampleGathererBufferLength);
				sysMemSet128(sampleGathererBuffer, 0, sizeof(s16) * s_SampleGathererBufferLength);
				sysMemCpy((void*)sampleGathererBuffer, (const void*)&m_WaveData[m_ActiveGrains[m_PrevGrainIndex].startSample + prevGrainSamplePosFloor], Min(inputSamplesNeeded, s_SampleGathererBufferLength) * sizeof(s16));
				trailingFrac = prevGrainSamplePos - prevGrainSamplePosFloor;
				audRateConvertorLinear::Process(prevGrainRatio, &crossfadeMixBufferPrevSample[0], sampleGathererBuffer, Min(inputSamplesNeeded, 1024u), samplesRequired, trailingFrac);						

				if(m_ActiveGrains[m_PrevGrainIndex].skipThisGrain)
				{
					ScaleVolume(crossfadeMixBufferPrevSample, samplesRequired, m_SkippedGrainVol);
				}

				// Mix all our generated data to the main mix buffer
				if(m_InterGrainCrossfadeStyle == CrossfadeStyleEqualPower)
				{
					const f32 PI_over_2 = (PI / 2.0f);
					f32 granuleLoopFraction = 0.0f;
					for(u32 loop = 0; loop < samplesRequired; loop++)
					{
						const f32 activeGrainVolumeLin = Clamp((m_GranularFractionInternal + (granuleFractionPerSample * loop)) * s_oneOverCrossfadeAmount, 0.0f, 1.0f);
						const f32 activeGrainVolume = Sinf(activeGrainVolumeLin * PI_over_2);
						const f32 prevGrainVolume = Sinf((1.f - activeGrainVolumeLin) * PI_over_2);
						mixBuffer[samplesGenerated + loop] = (crossfadeMixBufferPrevSample[loop] * prevGrainVolume) + (crossfadeMixBufferActiveSample[loop] * activeGrainVolume);
						granuleLoopFraction += granuleFractionPerSample;
					}
				}
				else
				{
					f32 granuleLoopFraction = 0.0f;
					for(u32 loop = 0; loop < samplesRequired; loop++)
					{
						const f32 activeGrainVolume = Clamp((m_GranularFractionInternal + granuleLoopFraction) * s_oneOverCrossfadeAmount, 0.0f, 1.0f);
						const f32 prevGrainVolume = 1.0f - activeGrainVolume;
						mixBuffer[samplesGenerated + loop] = (crossfadeMixBufferPrevSample[loop] * prevGrainVolume) + (crossfadeMixBufferActiveSample[loop] * activeGrainVolume);
						granuleLoopFraction += granuleFractionPerSample;
					}
				}

				m_GranularFractionInternal += granuleFractionPerSample * samplesRequired;
				samplesGenerated += samplesRequired;
			}
			// Standard resampling-only section
			else
			{
				// audRateConvertorLinear::Process requires that the mix buffer is 16 byte aligned, so do a slow mix until true
				while((((size_t)&mixBuffer[samplesGenerated])&15)!=0 && samplesRequired > 0)
				{
					mixBuffer[samplesGenerated] = ReadSampleFromGranularFraction(m_ActiveGrainIndex, m_GranularFractionInternal) * s_s16ToFloatDivisor;

					if(m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain)
					{
						mixBuffer[samplesGenerated] *= m_SkippedGrainVol;
					}

					m_GranularFractionInternal += granuleFractionPerSample;
					samplesGenerated++;
					samplesRequired--;
				}

				if(samplesRequired > 0)
				{
					const f32 ratio = Clamp(m_ActiveGrains[m_ActiveGrainIndex].grainLength / (1.0f/granuleFractionPerSample), 0.0f, (f32)kMaxSampleRateRatio);
					const f32 currentSamplePos = m_ActiveGrains[m_ActiveGrainIndex].grainLength * m_GranularFractionInternal;
					const u32 currentSamplePosFloor = (u32) Floorf(currentSamplePos);
					const f32 trailingFrac = currentSamplePos - currentSamplePosFloor;
					const u32 samplesRemaining = ratio < 2.0f? m_NumSamples - currentSamplePosFloor : (u32)ceilf(kMixBufNumSamples * ceilf(ratio));

					s16* sampleGathererBuffer = AllocaAligned(s16, s_SampleGathererBufferLength, 128);
					sysMemSet128(sampleGathererBuffer, 0, sizeof(s16) * s_SampleGathererBufferLength);
					sysMemCpy((void*)sampleGathererBuffer, (const void*)&m_WaveData[m_ActiveGrains[m_ActiveGrainIndex].startSample + currentSamplePosFloor], Min(samplesRemaining, s_SampleGathererBufferLength) * sizeof(s16));

					audRateConvertorLinear::Process(ratio, &mixBuffer[samplesGenerated], sampleGathererBuffer, Min(samplesRemaining, 1024u), samplesRequired + s_resampleBufferOverlap, trailingFrac);

					if(m_ActiveGrains[m_ActiveGrainIndex].skipThisGrain)
					{
						ScaleVolume(&mixBuffer[samplesGenerated], samplesRequired, m_SkippedGrainVol);
					}

					m_GranularFractionInternal += (granuleFractionPerSample * samplesRequired);
					samplesGenerated += samplesRequired;
				}
			}

			if(m_GranularFractionInternal >= 1.0f)
			{
				audAssertf(numGrainSwapsThisFrame == 0, "Multiple grain swaps in a single frame! Not supported. Fraction per sample: %f", incomingGranuleFractionPerSample);
				m_NumGrainsGenerated++;
				m_GranularFractionInternal -= 1.0f;
				numGrainSwapsThisFrame++;
				MoveToNextGrain();
				
				if(generateNative)
				{
					granuleFractionPerSample = 1.0f/m_ActiveGrains[m_ActiveGrainIndex].grainLength;
				}
			}
		}
	}

	{
		if(samplesGenerated > 0)
		{
			if(volumeScale > 0.0f || m_PrevVolumeScale > 0.0f)
			{
				Vector_4V volumeVecStart = V4LoadScalar32IntoSplatted(Clamp(m_PrevVolumeScale * s_ClippingVolumeFix, 0.0f, 1.0f));
				Vector_4V volumeVecEnd = V4LoadScalar32IntoSplatted(Clamp(volumeScale * s_ClippingVolumeFix, 0.0f, 1.0f));
				MixMonoBufToMonoBuf<kMixBufNumSamples>(mixBuffer, destBuffer, volumeVecStart, volumeVecEnd);
			}

			m_PrevVolumeScale = volumeScale;
		}
	}
	
	return samplesGenerated;
}

// ----------------------------------------------------------------
// Create a packet of data to send to the decoder
// ----------------------------------------------------------------
audPacket audGranularSubmix::CreateDecoderPacket(u32 activeGrainIndex)
{
	audPacket packet;

	u32 grainIndex = m_ActiveGrains[activeGrainIndex].grainIndex;

	packet.PlayBegin = m_GrainTable[grainIndex].startSample;
	packet.PlayEnd = m_GrainTable[grainIndex + 1].startSample + s_OverlapSamplesForCrossfade; // Adding extra to cover inter-granular crossfades

#if __XENON
	// The XMA decoder lies about its decode point - its actually 384 samples behind where we set it to be. However, the first 128 samples of
	// decoded data also have a slight crossfade caused by the decompression FFT. Therefore we set the start point to 256 samples ahead, and then
	// skip the first 128 samples we read, thereby avoiding the crossfade and also reading from the correct position
	packet.PlayBegin += (384 - s_FFTFadeSkipSize);
	packet.PlayEnd += 384;
#elif __PS3
	// PS3 assets have preserveTransients flag set, which adds a frame of silence to the start of the asset
	packet.PlayBegin += (audMp3Util::kFrameSizeSamples + 16) - s_FFTFadeSkipSize;
	packet.PlayEnd += (audMp3Util::kFrameSizeSamples + 16);
#endif

	if(packet.PlayBegin >= (u32)m_NumSamples)
	{
		audWarningf("Granular packet PlayBegin is beyond the end of the data (%d >= %d)", packet.PlayBegin, (u32)m_NumSamples);
		packet.PlayBegin = m_NumSamples - 1; 
	}

	if(packet.PlayEnd >= (u32)m_NumSamples)
	{
		audWarningf("Granular packet PlayEnd is beyond the end of the data (%d >= %d)", packet.PlayEnd, (u32)m_NumSamples);
		packet.PlayEnd = m_NumSamples - 1;
	}

#if __PS3
	audVramLoader* activeGrainVramLoader = m_ActiveGrains[activeGrainIndex].vramLoader;
	Assert(activeGrainVramLoader);
	const u8 *data = activeGrainVramLoader->vramHelper.GetFetchBufferPtr();
	Assert(packet.PlayBegin >= activeGrainVramLoader->startOffsetSamples);
	Assert(packet.PlayEnd >= activeGrainVramLoader->startOffsetSamples);
	packet.PlayBegin -= activeGrainVramLoader->startOffsetSamples;
	packet.PlayEnd -= activeGrainVramLoader->startOffsetSamples;
	packet.inputBytes = activeGrainVramLoader->dataSize; 
#else
#if __XENON
	u32 playBeginInBytes = m_GrainTable[grainIndex].playBeginInPackets * XMA_BYTES_PER_PACKET;
	u32 startSamplesSkipped = m_GrainTable[grainIndex].startSamplesSkipped;
#else
	u32 playBeginInBytes = 0;
	u32 startSamplesSkipped = 0;
#endif

	const u8 *data = (const u8*)m_WaveData;
	data += playBeginInBytes;

	packet.PlayBegin -= startSamplesSkipped;
	packet.PlayEnd -= startSamplesSkipped;
	packet.inputBytes = m_WaveLengthBytes - playBeginInBytes; 
#endif

	packet.inputData = data;
	Assert(packet.inputBytes > 0);
	packet.WaveSlotId = m_WaveSlot;
	return packet;
}

// ----------------------------------------------------------------
// Move to a new grain
// ----------------------------------------------------------------
void audGranularSubmix::SendDecoderPackets()
{
	PF_FUNC(SendDecoderPackets);

    if(!audVerifyf(m_GrainTable, "Invalid Grain Table!") ||
       !audVerifyf(m_NumStoredGrainStates <= kMaxStoredGrainStates, "Invalid num stored grain states!"))
    {
        return;
    }    

	for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
	{
		u32 newActiveGrain = m_ActiveGrains[loop].grainIndex;

		if(!m_DecodersRequired)
		{
			if(!m_ActiveGrains[loop].packetSent && !m_ActiveGrains[loop].awaitingFlush)
			{
				m_ActiveGrains[loop].samplesDecoded = 0;
				m_ActiveGrains[loop].initialFadeSamplesRemaining = s_FFTFadeSkipSize;
				m_ActiveGrains[loop].packetSent = true;
				m_ActiveGrains[loop].startSample = m_GrainTable[newActiveGrain].startSample;
				m_ActiveGrains[loop].grainLength = m_GrainTable[newActiveGrain+1].startSample - m_GrainTable[newActiveGrain].startSample;
			}
		}
		else if(m_ActiveGrains[loop].decoder)
		{
			if(!m_ActiveGrains[loop].packetSent &&
#if __PS3
				m_ActiveGrains[loop].vramLoader &&	
#else
				m_ActiveGrains[loop].decoder->QueryReadyForMoreData() &&
				(m_ActiveGrains[loop].decoder->GetState() == audDecoder::FINISHED || m_ActiveGrains[loop].decoder->GetState() == audDecoder::IDLE) &&
#endif
				!m_ActiveGrains[loop].awaitingFlush)
			{
#if !__PS3
				if(m_ActiveGrains[loop].decoder)
				{
					audGranularAssertf(m_ActiveGrains[loop].decoder->QueryNumAvailableSamples() == 0, "Submitting a new packet when there is still data left in the old one!");
				}
#endif

				audPacket packet = CreateDecoderPacket(loop);

#if __XENON
				((audDecoderXma*)m_ActiveGrains[loop].decoder)->ClearBuffer();
#endif

#if __PS3
				bool success = audDecoderSpu::GenerateSpuPacket(packet, m_ActiveGrains[loop].packet, m_DecodersRequired);
				Assert(success);

				m_ActiveGrains[loop].spuPacketSent = false;
#else
				m_ActiveGrains[loop].decoder->SubmitPacket(packet);
#endif

				m_ActiveGrains[loop].samplesDecoded = 0;
				m_ActiveGrains[loop].initialFadeSamplesRemaining = s_FFTFadeSkipSize;
				m_ActiveGrains[loop].packetSent = true;
				m_ActiveGrains[loop].startSample = m_GrainTable[newActiveGrain].startSample;
				m_ActiveGrains[loop].grainLength = m_GrainTable[newActiveGrain+1].startSample - m_GrainTable[newActiveGrain].startSample;
			}
		}
		else if(!m_DecodersRequired)
		{
			if(!m_ActiveGrains[loop].packetSent &&
				!m_ActiveGrains[loop].awaitingFlush)
			{
				u32 newActiveGrain = m_ActiveGrains[loop].grainIndex;
				m_ActiveGrains[loop].samplesDecoded = 0;
				m_ActiveGrains[loop].initialFadeSamplesRemaining = s_FFTFadeSkipSize;
				m_ActiveGrains[loop].packetSent = true;
				m_ActiveGrains[loop].startSample = m_GrainTable[newActiveGrain].startSample;
				m_ActiveGrains[loop].grainLength = m_GrainTable[newActiveGrain+1].startSample - m_GrainTable[newActiveGrain].startSample;
			}
		}
	}
}

#if __PS3
// ----------------------------------------------------------------
// Given a final grain, work out the starting grain if we're loading 
// in the chunk of data directly preceeding it
// ----------------------------------------------------------------
s32 audGranularSubmix::GetVramStartGrainForLoadBackwards(s32 finalGrainIndex, u32 numFetchBuffers)
{
	s32 startGrainBytes = (s32)m_GrainTable[finalGrainIndex].frameOffsetBytesAsGrainEnd + s_VramCalculationByteLimit - (numFetchBuffers * audDecoderVramHelper::kFetchBufferSize);

	if(startGrainBytes < 0)
	{
		startGrainBytes = 0;
	}

	s32 startGrainIndex = finalGrainIndex;

	while(startGrainIndex > 0)
	{
		u32 startGrainFrameOffsetBytes = m_GrainTable[startGrainIndex - 1].frameOffsetBytesAsGrainStart;

		if(startGrainFrameOffsetBytes > startGrainBytes)
		{
			startGrainIndex--;
		}
		else
		{
			break;
		}
	}

	return startGrainIndex;
}

// ----------------------------------------------------------------
// Work out which vram loader should be used to play this grain
// ----------------------------------------------------------------
audGranularSubmix::audVramLoader* audGranularSubmix::GetVramLoaderForGrainIndex(audVramLoader* vramLoaders, u32 numVramLoaders, u32 grainIndex, bool assertOnError)
{
	for(u32 vramLoaderLoop = 0; vramLoaderLoop < numVramLoaders; vramLoaderLoop++)
	{
		if(vramLoaders[vramLoaderLoop].vramHelper.QueryNumBytesAvailable() == ~0U)
		{
			if(grainIndex >= vramLoaders[vramLoaderLoop].firstValidGrainIndex && grainIndex <= vramLoaders[vramLoaderLoop].lastValidGrainIndex)
			{
				return &vramLoaders[vramLoaderLoop];
			}
		}
	}

	if(assertOnError)
	{
		audAssertf(false, "Failed to find a vram loader for grain %d", grainIndex);
	}
	
	return NULL;
}
#endif

// ----------------------------------------------------------------
// Set this submix to play back all the grains randomly
// ----------------------------------------------------------------
void audGranularSubmix::SetPureRandom(bool pureRandom)
{
	if(pureRandom)
	{
		SetSubmixType(audGranularSubmix::SubmixTypeSynchronisedLoop);
		SetPlaybackOrder(audGranularSubmix::PlaybackOrderMixNMatch);
		m_NumValidGrains = m_NumGrains;
	}
	else
	{
		SetSubmixType(audGranularSubmix::SubmixTypePureGranular);
	}

#if __PS3
	// On PS3, stop and restart so that our VRAM decoders grab the correct data
	if(pureRandom && (m_PureRandomPlayback != pureRandom))
	{
		StopPhys();
	}
#endif

	m_PureRandomPlayback = pureRandom;
}

// ----------------------------------------------------------------
// Set this submix to play back all the grains randomly
// ----------------------------------------------------------------
void audGranularSubmix::SetPureRandomWithStyle(audGrainPlaybackOrder style)
{	
	SetSubmixType(audGranularSubmix::SubmixTypeSynchronisedLoop);
	SetPlaybackOrder(style);
	m_NumValidGrains = m_NumGrains;	

#if __PS3
	// On PS3, stop and restart so that our VRAM decoders grab the correct data
	if(!m_PureRandomPlayback)
	{
		StopPhys();
	}
#endif

	m_PureRandomPlayback = true;
}

// ----------------------------------------------------------------
// Check if this submix is ready to starty
// ----------------------------------------------------------------
bool audGranularSubmix::IsReadyToStart() const
{
	if(!m_IsPlayingPhysically)
	{
		return false;
	}

#if __XENON
	if(m_ActiveGrainIndex >= 0)
	{
		if(m_ActiveGrains[m_ActiveGrainIndex].grainLength == 0)
		{
			return false;
		}
	}
	else
	{
		return false;
	}
#elif __PS3
	for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
	{
		if(!m_ActiveGrains[loop].vramLoader)
		{
			return false;
		}
	}
#endif

	return true;
}

// ----------------------------------------------------------------
// Work out the limits of the granular sliding window
// ----------------------------------------------------------------
void audGranularSubmix::CalculateSlidingWindowLimits(u32 grainIndex, const f32 granuleStepPerGranule, u32& maxGrainBelowOut, u32& maxGrainAboveOut) const
{
	u32 slidingGrainWindowSizeGrainsMax = Clamp((u32)(m_SlidingGrainWindowSizeGrainsMax * (1.0f/(granuleStepPerGranule + 1.0f))), m_SlidingGrainWindowSizeGrainsMin, m_SlidingGrainWindowSizeGrainsMax);
	f32 currentGrainPitch = m_GrainTable[grainIndex].pitch;
	u32 maxGrainAbove = 0;
	u32 maxGrainBelow = 0;

	// If this sweep has an ascending pitch scale
	if(m_GrainTable[0].pitch < m_GrainTable[m_NumGrains - 1].pitch)
	{
		// Find the number of grains above that lie within our random pitch range
		for (s32 loop = grainIndex + 1; loop < (s32)m_NumGrains && maxGrainAbove < slidingGrainWindowSizeGrainsMax && (m_MaxGrainLimit == -1 || loop <= m_MaxGrainLimit); loop++)
		{
			if (m_GrainTable[loop].pitch < currentGrainPitch + m_SlidingGrainWindowSizeHz)
			{
				maxGrainAbove++;
			}
			else
			{
				break;
			}
		}

		// Find the number of grains below that lie within our random pitch range
		for (s32 loop = grainIndex - 1; loop > 0 && maxGrainBelow < slidingGrainWindowSizeGrainsMax && (m_MinGrainLimit == -1 || loop >= m_MinGrainLimit); loop--)
		{
			if (m_GrainTable[loop].pitch > currentGrainPitch - m_SlidingGrainWindowSizeHz)
			{
				maxGrainBelow++;
			}
			else
			{
				break;
			}
		}
	}
	// If this sweep has an descending pitch scale
	else
	{
		// Find the number of grains below that lie within our random pitch range
		for (s32 loop = grainIndex - 1; loop > 0 && maxGrainAbove < slidingGrainWindowSizeGrainsMax && (m_MinGrainLimit == -1 || loop >= m_MinGrainLimit); loop--)
		{
			if (m_GrainTable[loop].pitch < currentGrainPitch + m_SlidingGrainWindowSizeHz)
			{
				maxGrainBelow++;
			}
			else
			{
				break;
			}
		}

		// Find the number of grains above that lie within our random pitch range
		for (s32 loop = grainIndex + 1; loop < (s32)m_NumGrains && maxGrainBelow < slidingGrainWindowSizeGrainsMax && (m_MaxGrainLimit == -1 || loop <= m_MaxGrainLimit); loop++)
		{
			if (m_GrainTable[loop].pitch > currentGrainPitch - m_SlidingGrainWindowSizeHz)
			{
				maxGrainAbove++;
			}
			else
			{
				break;
			}
		}
	}

	// We now know the range of grains that we can randomise over, but clamp them to our minimum values
	if(!HasMinMaxGrainLimit())
	{
		maxGrainAboveOut = Clamp(maxGrainAbove, m_SlidingGrainWindowSizeGrainsMin, slidingGrainWindowSizeGrainsMax);
		maxGrainBelowOut = Clamp(maxGrainBelow, m_SlidingGrainWindowSizeGrainsMin, slidingGrainWindowSizeGrainsMax);
	}
	else
	{
		maxGrainAboveOut = maxGrainAbove;
		maxGrainBelowOut = maxGrainBelow;
	}
}

// ----------------------------------------------------------------
// Calculate any new grains required
// ----------------------------------------------------------------
void audGranularSubmix::CalculateNewGrains(const f32 currentRateHz, const f32 granuleStepPerGranule PS3_ONLY(, audVramLoader* grainVramLoaders, u32 numGrainVramLoaders, audVramLoader* loopVramLoaders, u32 numLoopVramLoaders))
{
	PF_FUNC(CalculateNewGrains);

#if __XENON
	static const u32 hzLimitForReducedDecoders = 45;

	//if(m_WaveFormat->SampleRate >= 32000)
	{
		const bool withinCrossfadeSection = s_granularCrossfadeAmount > 0 && m_GranularFractionInternal < s_granularCrossfadeAmount && m_ActiveGrains[m_PrevGrainIndex].initialFadeSamplesRemaining == 0;

		// We only need to allocate a third decoder if we're playing back grains very quickly
		if(currentRateHz > hzLimitForReducedDecoders && m_NumStoredGrainStates == 2)
		{
			if(m_ActiveGrains[1].awaitingGrainChange)
			{
				Assert(!m_ActiveGrains[2].decoder);
				m_ActiveGrains[2].decoder = audDriver::GetMixer()->GetDecodeMgr().AllocateDecoder((audWaveFormat::audStreamFormat)m_WaveFormat->Format, 1, m_WaveFormat->SampleRate);
				Assert(m_ActiveGrains[2].decoder);

				if(m_ActiveGrains[2].decoder)
				{
					s_NumGranularDecodersAllocated++;
					m_NumStoredGrainStates = 3;
					m_ActiveGrains[2].awaitingGrainChange = true;
				}
			}
		}
		// If we're below the threshold, free up the decoder
		else if(currentRateHz <= hzLimitForReducedDecoders && m_NumStoredGrainStates == 3 && !withinCrossfadeSection)
		{
			if(m_ActiveGrains[2].awaitingGrainChange)
			{
				Assert(m_ActiveGrains[2].decoder);
				audDriver::GetMixer()->GetDecodeMgr().FreeDecoder(m_ActiveGrains[2].decoder);
				s_NumGranularDecodersAllocated--;
				m_ActiveGrains[2].decoder = NULL;
				m_NumStoredGrainStates = 2;
			}
		}
	}
#endif

	for(u32 loop = 0; loop < m_NumStoredGrainStates; loop++)
	{
		if(m_ActiveGrains[loop].awaitingGrainChange)
		{
			s32 grainIndex = 0;

			if(m_SubmixType == SubmixTypePureGranular)
			{
				s32 baseGrainIndex;

				if(!HasMinMaxGrainLimit())
				{
					baseGrainIndex = CalculateGrainIndexFromPitch(currentRateHz, m_GrainTable, m_NumGrains, m_PitchShift, m_PitchStretch);
				}
				else
				{
					baseGrainIndex = GetLastGrainIndex();
				}

				ClampSelectedGrain(baseGrainIndex);
#if __PS3
				m_ActiveGrains[loop].vramLoader = NULL;
				u32 closestVramLoaderDistance = m_NumGrains;
				u32 bestMatchGrain = baseGrainIndex;

				// Need to make sure that we actually have the required data loaded for this grain
				if(QueryAnyVramLoaderDataAvailable(grainVramLoaders, numGrainVramLoaders))
				{
					for(u32 vramLoaderIndex = 0; vramLoaderIndex < numGrainVramLoaders; vramLoaderIndex++)
					{
						if(grainVramLoaders[vramLoaderIndex].vramHelper.QueryNumBytesAvailable() == ~0U)
						{
							// If the grain we're requesting is already loaded by one of the decoders - happy days, just use it
							if(baseGrainIndex >= grainVramLoaders[vramLoaderIndex].firstValidGrainIndex && baseGrainIndex <= grainVramLoaders[vramLoaderIndex].lastValidGrainIndex)
							{
								m_ActiveGrains[loop].vramLoader = &grainVramLoaders[vramLoaderIndex];
								bestMatchGrain = baseGrainIndex;
								break;
							}
							else
							{
								u32 difference = 0;

								// Work out how far beyond/behind the loaded data we are
								if(grainVramLoaders[vramLoaderIndex].lastValidGrainIndex >= 0)
								{
									if(baseGrainIndex > grainVramLoaders[vramLoaderIndex].lastValidGrainIndex)
									{
										difference = baseGrainIndex - grainVramLoaders[vramLoaderIndex].lastValidGrainIndex;
									}
									else if(baseGrainIndex < grainVramLoaders[vramLoaderIndex].firstValidGrainIndex)
									{
										difference = grainVramLoaders[vramLoaderIndex].firstValidGrainIndex - baseGrainIndex;
									}

									// If this is the closest match so far, pick the loaded grain as close as possible to the desired one
									if(difference < closestVramLoaderDistance)
									{
										closestVramLoaderDistance = difference;
										m_ActiveGrains[loop].vramLoader = &grainVramLoaders[vramLoaderIndex];

										if(baseGrainIndex > grainVramLoaders[vramLoaderIndex].lastValidGrainIndex)
										{
											bestMatchGrain = grainVramLoaders[vramLoaderIndex].lastValidGrainIndex;
										}
										else if(baseGrainIndex < grainVramLoaders[vramLoaderIndex].firstValidGrainIndex)
										{
											bestMatchGrain = grainVramLoaders[vramLoaderIndex].firstValidGrainIndex;
										}
									}
								}
							}
						}
					}

					Assertf(m_ActiveGrains[loop].vramLoader, "Failed to select a valid vram loader");
				}

				// Bail out if we haven't managed to find a decoder
				if(!m_ActiveGrains[loop].vramLoader)
				{
					continue;
				}

				baseGrainIndex = bestMatchGrain;
#endif

				grainIndex = baseGrainIndex;

				m_MaxRandomisedGrainBelow = 0;
				m_MaxRandomisedGrainAbove = 0;
				m_LastRandomisedGrainCenter = baseGrainIndex;

				// Calculate how many grains we are allowed to randomise over
				CalculateSlidingWindowLimits(grainIndex, granuleStepPerGranule, m_MaxRandomisedGrainBelow, m_MaxRandomisedGrainAbove);

				s32 validGrainStart = grainIndex - m_MaxRandomisedGrainBelow;
				s32 validGrainEnd = grainIndex + m_MaxRandomisedGrainAbove;

				if(HasMinMaxGrainLimit())
				{
#if __PS3
					for(u32 vramLoaderIndex = 0; vramLoaderIndex < numGrainVramLoaders; vramLoaderIndex++)
					{
						if(grainVramLoaders[vramLoaderIndex].vramHelper.QueryNumBytesAvailable() == ~0U)
						{
							if((grainVramLoaders[vramLoaderIndex].firstValidGrainIndex >= m_MinGrainLimit && grainVramLoaders[vramLoaderIndex].firstValidGrainIndex <= m_MaxGrainLimit) ||
							   (grainVramLoaders[vramLoaderIndex].lastValidGrainIndex >= m_MinGrainLimit && grainVramLoaders[vramLoaderIndex].lastValidGrainIndex <= m_MaxGrainLimit))
							{
								validGrainStart = m_MinGrainLimit;
								validGrainEnd = m_MaxGrainLimit;
							}
						}
					}
#else

					validGrainStart = m_MinGrainLimit;
					validGrainEnd = m_MaxGrainLimit;
#endif
				}

				u32 numValidGrains = 0;
				u32 maxValidGrains = validGrainEnd - validGrainStart + 1;
				u32* const validGrains = Alloca(u32, maxValidGrains);

				// Generate a list of valid grains that we can select between
				for(s32 validGrainLoop = validGrainStart; validGrainLoop <= validGrainEnd && numValidGrains < maxValidGrains; validGrainLoop++)
				{
					if(validGrainLoop >= 0 && validGrainLoop < (s32)m_NumGrains - 1)
					{
#if __PS3
						if(!GetVramLoaderForGrainIndex(grainVramLoaders, numGrainVramLoaders, validGrainLoop))
						{
							continue;
						}
#endif

						validGrains[numValidGrains] = validGrainLoop;

						// Always start off our randomisation from the previous selected grain
						if(validGrainLoop == m_GrainHistory[m_GrainHistoryIndex])
						{
							m_SelectedValidGrainIndex = numValidGrains;
						}

						numValidGrains++;
					}
				}

				if(s_GranularMidLoopRandomisationEnabled && !HasMinMaxGrainLimit())
				{
					if(m_NumGrainsSinceWalkReRandomise > numValidGrains * 0.75f && audEngineUtil::ResolveProbability(1.0f/numValidGrains))
					{
						m_WalkDirectionForward = !m_WalkDirectionForward;
						m_NumGrainsSinceWalkReRandomise = 0;
					}
					else
					{
						m_NumGrainsSinceWalkReRandomise++;
					}
				}

				u32 attempts = 0;

				if(numValidGrains > 0)
				{
					do 
					{
						// Now select one of our valid grains according to the current selection method
						grainIndex = SelectNewGrain(m_PlaybackOrder, validGrains, numValidGrains);
						ClampSelectedGrain(grainIndex);
						attempts++;
					} 
					while ((m_InvalidGrains.Find(grainIndex) >= 0 || WasGrainRecentlyUsed(grainIndex)) && attempts < 5);
				}				

#if __PS3
				// We've fiddled with the grain index, and might have swapped vram decoders, so re-check our stored decoder index
				m_ActiveGrains[loop].vramLoader = GetVramLoaderForGrainIndex(grainVramLoaders, numGrainVramLoaders, grainIndex, true);
#endif

				m_WasRandomisingLastTime = true;
				m_BaseGrainIndexLastTime = baseGrainIndex;
			}
			else 
			{
#if __PS3
				atFixedArray<u32, 96> validLoadedGrainsArray;
				if(QueryAnyVramLoaderDataAvailable(loopVramLoaders, numLoopVramLoaders))
				{
					// If there is no valid grain table, all (loaded) grains are valid
					if(!m_ValidGrains)
					{
						u32 vramLoadersWithData = 0;
						for(u32 vramLoaderLoop = 0; vramLoaderLoop < numLoopVramLoaders; vramLoaderLoop++)
						{
							if(loopVramLoaders[vramLoaderLoop].vramHelper.QueryNumBytesAvailable() == ~0U)
							{
								for(s32 validGrainLoop = loopVramLoaders[vramLoaderLoop].firstValidGrainIndex; validGrainLoop <= loopVramLoaders[vramLoaderLoop].lastValidGrainIndex; validGrainLoop++)
								{
									if(vramLoadersWithData == 0 || !validLoadedGrainsArray.Find(validGrainLoop))
									{
										if(validLoadedGrainsArray.GetCount() < validLoadedGrainsArray.GetMaxCount())
										{
											validLoadedGrainsArray.Push(validGrainLoop);
										}
									}
								}

								vramLoadersWithData++;
							}
						}
					}
					else
					{
						s32 vramLoaderToUse = -1;

						// We've got an explicit synchronized loop that we want to play, so check if any vram loaders actually have the data
						for(u32 vramLoaderLoop = 0; vramLoaderLoop < numLoopVramLoaders; vramLoaderLoop++)
						{
							if(m_SynchronisedLoopIndex >= 0 &&
							   loopVramLoaders[vramLoaderLoop].synchronisedLoopIndex == m_SynchronisedLoopIndex &&
							   loopVramLoaders[vramLoaderLoop].vramHelper.QueryNumBytesAvailable() == ~0U)
							{
								vramLoaderToUse = vramLoaderLoop;
								break;
							}
						}

						// Correct data isn't loaded, so just find the nearest loaded vram loader and use the grains from that instead
						if(vramLoaderToUse == -1)
						{
							// Keep waiting until we've got some correct data if this is the very first frame
							if(m_FirstFrame)
							{
								continue;
							}

							s32 nearestDifference = -1;
							for(u32 vramLoaderLoop = 0; vramLoaderLoop < numLoopVramLoaders; vramLoaderLoop++)
							{
								if(loopVramLoaders[vramLoaderLoop].vramHelper.QueryNumBytesAvailable() == ~0u)
								{
									u32 difference = abs(loopVramLoaders[vramLoaderLoop].synchronisedLoopIndex - m_SynchronisedLoopIndex);

									if(vramLoaderToUse == -1 || difference <= nearestDifference)
									{
										vramLoaderToUse = vramLoaderLoop;
										nearestDifference = difference;
									}
								}
							}
						}

						if(vramLoaderToUse >= 0)
						{
							for(s32 validGrainLoop = loopVramLoaders[vramLoaderToUse].firstValidGrainIndex; validGrainLoop <= loopVramLoaders[vramLoaderToUse].lastValidGrainIndex; validGrainLoop++)
							{
								validLoadedGrainsArray.Push(validGrainLoop);
							}
						}
					}

					audAssert(validLoadedGrainsArray.GetCount() > 0);
				}

				if(validLoadedGrainsArray.GetCount() == 0)
				{
					continue;
				}

				const u32* validLoadedGrains = validLoadedGrainsArray.GetElements();
				u32 numValidLoadedGrains = validLoadedGrainsArray.GetCount();
#else
				const u32* validLoadedGrains = m_ValidGrains;
				u32 numValidLoadedGrains = m_NumValidGrains;
#endif

				if(s_GranularMidLoopRandomisationEnabled)
				{
					if(m_PlaybackOrder == PlaybackOrderWalk)
					{
						if(m_NumGrainsSinceWalkReRandomise > numValidLoadedGrains * 0.33f && audEngineUtil::ResolveProbability(1.0f/numValidLoadedGrains))
						{
							m_WalkDirectionForward = !m_WalkDirectionForward;
							m_NumGrainsSinceWalkReRandomise = 0;
						}
						else
						{
							m_NumGrainsSinceWalkReRandomise++;
						}
					}
				}

				u32 attempts = 0;

				if(numValidLoadedGrains > 0)
				{
					do 
					{
						grainIndex = SelectNewGrain(m_PlaybackOrder, validLoadedGrains, numValidLoadedGrains);
						attempts++;
					} while ((m_InvalidGrains.Find(grainIndex) >= 0 || WasGrainRecentlyUsed(grainIndex)) && attempts < 5);
				}				
				
				ClampSelectedGrain(grainIndex);

#if __PS3
				m_ActiveGrains[loop].vramLoader = GetVramLoaderForGrainIndex(loopVramLoaders, numLoopVramLoaders, grainIndex, true);
#endif
			}

			if(m_GrainHistoryIndex > 0)
			{
				m_GrainHistoryIndex--;
			}
			else
			{
				m_GrainHistoryIndex = kMaxGrainHistoryValues - 1;
			}

			m_GrainHistory[m_GrainHistoryIndex] = grainIndex;
			m_ActiveGrains[loop].grainIndex = grainIndex;
			m_ActiveGrains[loop].awaitingGrainChange = false;
		}
	}
}

// ----------------------------------------------------------------
// Set the sliding window size
// ----------------------------------------------------------------
void audGranularSubmix::SetGranularSlidingWindowSize(f32 windowSizeHzFraction, u32 minGrains, u32 maxGrains)
{
	f32 pitchRange = Max(m_GrainTable[m_NumGrains-1].pitch, m_GrainTable[0].pitch) - Min(m_GrainTable[m_NumGrains-1].pitch, m_GrainTable[0].pitch);
	m_SlidingGrainWindowSizeHz = pitchRange * windowSizeHzFraction;
	m_SlidingGrainWindowSizeGrainsMin = minGrains; 
	m_SlidingGrainWindowSizeGrainsMax = maxGrains;
}

// ----------------------------------------------------------------
// Set the min/max grain limit
// ----------------------------------------------------------------
void audGranularSubmix::SetMinMaxGrainLimit(s32 minGrain, s32 maxGrain)
{
	if(!m_FirstFrame)
	{
		if(minGrain >= 0 && maxGrain >= 0)
		{
			if(m_GrainHistory[m_GrainHistoryIndex] < minGrain)
			{
				minGrain = m_GrainHistory[m_GrainHistoryIndex];
			}
			else if(m_GrainHistory[m_GrainHistoryIndex] > maxGrain)
			{
				maxGrain = m_GrainHistory[m_GrainHistoryIndex];
			}
		}
	}
	
	if(!HasMinMaxGrainLimit() && minGrain >= 0 && maxGrain >= 0)
	{
		m_MinGrainLimit = minGrain; 
		m_MaxGrainLimit = maxGrain;

		// Set the playback to start as close as possible to our previous point
		if(m_GrainHistory[m_GrainHistoryIndex] < minGrain)
		{
			m_SelectedValidGrainIndex = 0;
			m_WalkDirectionForward = true;
		}
		else if(m_GrainHistory[m_GrainHistoryIndex] > maxGrain)
		{
			m_SelectedValidGrainIndex = m_MaxGrainLimit - m_MinGrainLimit;
			m_WalkDirectionForward = false;
		}
	}
	else
	{
		m_MinGrainLimit = minGrain; 
		m_MaxGrainLimit = maxGrain;
	}
}

// ----------------------------------------------------------------
// Calculate the grain index from the pitch table
// ----------------------------------------------------------------
s32 audGranularSubmix::CalculateGrainIndexFromPitch(f32 currentRateHz, const audGrainData* grainTable, const u32 numGrains, const f32 pitchShift, const f32 pitchStretch)
{
	s32 lowerBound = 0;
	s32 upperBound = numGrains - 1;
	s32 position = (lowerBound + upperBound) >> 1;
	const bool invertSearch = grainTable[0].pitch > grainTable[numGrains-1].pitch;
	const f32 minPitch = invertSearch? grainTable[numGrains-1].pitch : grainTable[0].pitch;

	// Adjust the incoming pitch rather than the value of the table entries, to avoid having to do it each time
	currentRateHz += pitchShift;
	currentRateHz = minPitch + ((currentRateHz - minPitch)/pitchStretch);

	// Do a quick binary search to match pitch->grain
	while(lowerBound <= upperBound)
	{
		if (invertSearch ? grainTable[position].pitch < currentRateHz : grainTable[position].pitch > currentRateHz)
		{
			upperBound = position - 1;    
		}                                                      
		else                                                
		{                                                       
			lowerBound = position + 1;     
		}

		position = (lowerBound + upperBound) >> 1;
	}

	return position;
}

// ----------------------------------------------------------------
// Select a new grain to play
// ----------------------------------------------------------------
u32 audGranularSubmix::SelectNewGrain(audGrainPlaybackOrder playbackOrder, const u32* validGrains, const u32 numValidGrains)
{
	audGrainPlaybackOrder currentPlaybackOrder = playbackOrder;

	if(playbackOrder == PlaybackOrderMixNMatch)
	{
		if(m_NumMixMatchGrainsToPlay < 0)
		{
			m_MixNMatchPlaybackOrder = (audGrainPlaybackOrder) audEngineUtil::GetRandomNumberInRange(0, PlaybackOrderMax - 1);

			if(m_MixNMatchPlaybackOrder == PlaybackOrderWalk)
			{
				m_NumMixMatchGrainsToPlay = numValidGrains * 2;
			}
			else
			{
				m_NumMixMatchGrainsToPlay = numValidGrains;
			}
		}

		currentPlaybackOrder = m_MixNMatchPlaybackOrder;
		m_NumMixMatchGrainsToPlay--;
	}

	switch(currentPlaybackOrder)
	{
	case PlaybackOrderRandom:
		{
			s32 originalIndex = m_SelectedValidGrainIndex;

			while(m_SelectedValidGrainIndex == originalIndex &&
				numValidGrains > 1)
			{
				m_SelectedValidGrainIndex = (u32) audEngineUtil::GetRandomNumberInRange(0, numValidGrains - 1);
			}
		}
		break;
	case PlaybackOrderReverse:
		m_SelectedValidGrainIndex--;

		if(m_SelectedValidGrainIndex < 0)
		{
			m_SelectedValidGrainIndex = numValidGrains - 1;
		}
		break;
	case PlaybackOrderSequential:
		m_SelectedValidGrainIndex++;

		if(m_SelectedValidGrainIndex >= (s32)numValidGrains)
		{
			m_SelectedValidGrainIndex = 0;
		}
		break;
	case PlaybackOrderWalk:
		if(m_WalkDirectionForward)
		{
			m_SelectedValidGrainIndex++;

			if(m_SelectedValidGrainIndex >= (s32)numValidGrains - 1)
			{
				m_SelectedValidGrainIndex = numValidGrains - 1;
				m_WalkDirectionForward = false;
			}
		}
		else
		{
			m_SelectedValidGrainIndex--;

			if(m_SelectedValidGrainIndex <= 0)
			{
				m_SelectedValidGrainIndex = 0;
				m_WalkDirectionForward = true;
			}
		}
		break;
	default:
		Assert(0);
		break;
	}

	m_SelectedValidGrainIndex = Clamp(m_SelectedValidGrainIndex, 0, (s32)(numValidGrains) - 1);

	if(validGrains)
	{
		return validGrains[m_SelectedValidGrainIndex];
	}
	else
	{
		return m_SelectedValidGrainIndex;
	}
}

// ----------------------------------------------------------------
// Check if this grain exists in the recent grain history list
// ----------------------------------------------------------------
bool audGranularSubmix::WasGrainRecentlyUsed(u32 grainIndex) const
{
	for(u32 loop = 0; loop < m_MinGrainRepeateRate; loop++)
	{
		if(GetPrevGrainIndex(loop) == (s32) grainIndex)
		{
			return true;
		}
	}

	return false;
}

// ----------------------------------------------------------------
// Enable or disble the submix
// ----------------------------------------------------------------
void audGranularSubmix::SetEnabled(bool enabled)
{
	m_IsEnabled = enabled;

	// Reset the valid grain index. Allows us to sync up multiple sequential
	// playback submixes if we turn them all on at the same time
	if(!enabled)
	{
		m_SelectedValidGrainIndex = 0;
	}
}
#endif

} // namespace rage
