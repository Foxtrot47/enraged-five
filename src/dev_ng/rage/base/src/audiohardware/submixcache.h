// 
// audiohardware/submixcache.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SUBMIXCACHE_H
#define AUD_SUBMIXCACHE_H

#include "atl/array.h"
#include "system/memops.h"
#include "framebufferpool.h"


namespace rage
{
	struct audSubmixCacheEntry
	{
		u16 bufferId;
		u16 nextIndex;
		ASSERT_ONLY(u16 freeStage);
	};

	struct audSubmixCacheLevel
	{
		u16 freeStage;
		u16 headIndex;
	};

	class audMixerSubmix;
	class audSubmixCache
	{
	public:
		audSubmixCache() : m_NumEntries(kMaxSubmixCacheEntries),m_NumEntriesAllocated(0)
		{
			sysMemSet(&m_CacheLevels, 0xff, sizeof(m_CacheLevels));
			sysMemSet(&m_CacheEntries, 0xff, sizeof(m_CacheEntries));

			m_EntryFirstFree = 0;
			for(u32 i = 0; i < kMaxSubmixCacheEntries; i++)
			{
				Assign(m_EntryFreeListStore[i], i+1);
			}
		}
		~audSubmixCache(){}

		// PURPOSE
		//	Marks all buffer space as free
		void ClearAllAllocations()
		{
			for(u32 i = 0; i < kMaxCacheLevels; i++)
			{
				if(m_CacheLevels[i].freeStage != 0xffff)
				{
					FreeCacheLevel(i);
				}
			}
		}

		// PURPOSE
		//	Allocates a single channel mix buffer from the pool
		// PARAMS
		//	freeStage - the processing stage after which this buffer is no longer required
		u32 AllocateBuffer(const s32 freeStage)
		{
			const u32 bufferId = g_FrameBufferPool->Allocate();
			if(bufferId == audFrameBufferPool::InvalidId)
			{
				// the Allocate() above will spam, so no need to do it twice
				//audErrorf("Failed to allocate buffer for cache");
				return bufferId;
			}
			AddBufferIdToLevel(freeStage, bufferId);
			return bufferId;
		}

		// PURPOSE
		//	Free all buffers that are not required beyond the specified processing stage
		void FreeBuffers(const u32 stage)
		{
			for(u32 i = 0; i < kMaxCacheLevels; i++)
			{
				if(m_CacheLevels[i].freeStage < stage)
				{
					FreeCacheLevel(i);
				}
			}
		}

		void NotifyBufferAllocation(const u32 bufferId, const u32 stage)
		{
			AddBufferIdToLevel(stage, bufferId);
		}

#if __SPU
		void SetSubmixes(atFixedArray<audMixerSubmix, kMaxSubmixes> *submixes)
		{
			m_Submixes = submixes;
		}

		audMixerSubmix *GetSubmixFromIndex(const s32 index)
		{
			return &(*m_Submixes)[index];
		}

		void SetMixBuffer(const u32 numChannels, void *ptr);
#endif

		u32 GetNumChannels() const { return m_NumEntries; }

	private:

		u32 AllocateLevelIndex(const u32 freeStage)
		{
			for(u32 i = 0; i < kMaxCacheLevels; i++)
			{
				if(m_CacheLevels[i].freeStage == 0xffff)
				{
					Assign(m_CacheLevels[i].freeStage, freeStage);
					m_CacheLevels[i].headIndex = 0xffff;
					return i;
				}
			}
			return ~0U;
		}

		void AddBufferIdToLevel(const u32 freeStage, const u32 bufferId)
		{
			for(u32 i = 0; i < kMaxCacheLevels; i++)
			{
				if(m_CacheLevels[i].freeStage == freeStage)
				{
					const u32 entryIndex = AllocateEntryIndex();
					if(entryIndex == ~0U)
					{
						audErrorf("Out of room in submix cache - couldn't allocate entry index");
						return;
					}
					Assign(m_CacheEntries[entryIndex].bufferId, bufferId);
					ASSERT_ONLY(Assign(m_CacheEntries[entryIndex].freeStage, freeStage));

					if(m_CacheLevels[i].headIndex != 0xffff)
					{
						m_CacheEntries[entryIndex].nextIndex = m_CacheLevels[i].headIndex;
					}

					Assign(m_CacheLevels[i].headIndex, entryIndex);
					return;
				}
			}

			// Allocate a new cache level for this processing stage
			const u32 levelIndex = AllocateLevelIndex(freeStage);
			if(levelIndex == ~0U)
			{
				audErrorf("Out of room in submix cache - couldn't allocate level index");
				return;
			}
			const u32 entryIndex = AllocateEntryIndex();
			if(entryIndex == ~0U)
			{
				audErrorf("Out of room in submix cache - couldn't allocate entry index");
				return;
			}
			Assign(m_CacheEntries[entryIndex].bufferId, bufferId);
			ASSERT_ONLY(Assign(m_CacheEntries[entryIndex].freeStage, freeStage));
			m_CacheEntries[entryIndex].nextIndex = 0xffff;
			Assign(m_CacheLevels[levelIndex].headIndex, entryIndex);
		}

		void FreeCacheLevel(const u32 index)
		{
			u32 entryIndex = m_CacheLevels[index].headIndex;
			while(entryIndex != 0xffff)
			{
				g_FrameBufferPool->Free(m_CacheEntries[entryIndex].bufferId);
				const u32 next = m_CacheEntries[entryIndex].nextIndex;
				Assert(m_CacheEntries[entryIndex].freeStage == m_CacheLevels[index].freeStage);
				FreeEntryIndex(entryIndex);
				entryIndex = next;
			}

			// Invalidate this level
			m_CacheLevels[index].freeStage = m_CacheLevels[index].headIndex = 0xffff;
		}

		u32 AllocateEntryIndex()
		{
			if(m_EntryFirstFree >= m_NumEntries)
			{
				audErrorf("Out of entries in submix cache: %u", m_NumEntries);
				return ~0U;
			}
			const u32 ret = m_EntryFirstFree;
			m_EntryFirstFree = m_EntryFreeListStore[ret];
			m_NumEntriesAllocated++;
			return ret;
		}

		void FreeEntryIndex(const u32 index)
		{
			Assign(m_EntryFreeListStore[index], m_EntryFirstFree);
			m_EntryFirstFree = index;
			m_NumEntriesAllocated--;
		}

#if RSG_PC
		enum {kMaxSubmixCacheEntries = 96};
#else
		enum {kMaxSubmixCacheEntries = 64};
#endif
		enum {kMaxCacheLevels = 32};
		atRangeArray<audSubmixCacheEntry, kMaxSubmixCacheEntries> m_CacheEntries;	
		atRangeArray<audSubmixCacheLevel, kMaxCacheLevels> m_CacheLevels;

		atRangeArray<u16, kMaxSubmixCacheEntries> m_EntryFreeListStore;
		u32 m_EntryFirstFree;

		u32 m_NumEntriesAllocated;
		u32 m_NumEntries;
#if __SPU
		atFixedArray<audMixerSubmix, kMaxSubmixes> *m_Submixes;
#endif
	};

	extern audSubmixCache *g_SubmixCache;

} // namespace rage


#endif // AUD_SUBMIXCACHE_H
