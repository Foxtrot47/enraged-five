// 
// audiohardware/sourcesubmix.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 


#ifndef AUD_SOURCESUBMIX_H
#define AUD_SOURCESUBMIX_H

#include "pcmsource.h"
#include "cmdbuffer.h"

#include "atl/array.h"

namespace rage
{
	// PURPOSE
	//	Implements a source level submix
	class audSourceSubmix : public audPcmSource
	{
	public:
		friend class audMixerVoice;

		audSourceSubmix();

		
		AUD_PCMSOURCE_VIRTUAL bool IsLooping()  const	{ return true; }
		AUD_PCMSOURCE_VIRTUAL s32 GetLengthSamples() const { return 0; }
		AUD_PCMSOURCE_VIRTUAL u32 GetPlayPositionSamples() const { return 0; }
		AUD_PCMSOURCE_VIRTUAL bool IsFinished() const { return false; }
		AUD_PCMSOURCE_VIRTUAL f32 GetHeadroom() const { return 0.f; }

		void Shutdown() {}

		void AddInputBuffer(const s32 bufferId, const f32 gain);
		

	private:

		AUD_PCMSOURCE_VIRTUAL void BeginFrame();
		AUD_PCMSOURCE_VIRTUAL void GenerateFrame();
		AUD_PCMSOURCE_VIRTUAL void Start() {}
		AUD_PCMSOURCE_VIRTUAL void Stop() {}

		AUD_PCMSOURCE_VIRTUAL void StartPhys(s32 channel);
		AUD_PCMSOURCE_VIRTUAL void StopPhys(s32 channel);

		enum {kMaxSourceSubmixInputs = 32};
		atFixedArray<f32, kMaxSourceSubmixInputs> m_InputGains;
		atFixedArray<s16, kMaxSourceSubmixInputs> m_InputBuffers;

	};
}

#endif // AUD_SOURCESUBMIX_H

