//=====================================================================================================
// name:		MediaStreamReader.cpp
// description:	Media stream reader for video and audio streams.
//=====================================================================================================
#if __WIN32PC && 0
#include <stdio.h> 
#include <string.h>

#include "WMStreamReader.h"
#include "audiohardware/driver.h"
#include "audiohardware/waveslot.h"

#pragma warning(disable: 4200)
#pragma warning(disable: 4995)
#include "system/simpleallocator.h"
#pragma warning(error: 4200)
#pragma warning(error: 4995)

//#pragma optimize("g", off)

namespace rage {

//#pragma comment(lib,"Wmvcore.lib")


typedef HRESULT (__stdcall *WMCreateSyncReaderProc)(IUnknown* pUnkCert, DWORD dwRights, IWMSyncReader **ppSyncReader);

HMODULE audWMStreamReader::sm_hWMVCore = NULL;

//=====================================================================================================
audWMStreamReader::audWMStreamReader() :
	m_pReader(NULL),
	m_bFileOpen(false),
	m_wAudioStreamNum(0),
	m_dwExtraBytes(0),
	m_StartOffsetMs(0)
{
#ifdef OUTPUT_FILLBUFFER_AUDIO
	m_hFile = NULL;
#endif
	sysMemSet(m_Artist,0,sizeof(m_Artist));
	sysMemSet(m_Title,0,sizeof(m_Title));
}

//=====================================================================================================
audWMStreamReader::~audWMStreamReader()
{
	Assert(m_pReader == NULL);
}

bool audWMStreamReader::IsAvailable()
{
	if(sm_hWMVCore)
	{
		return true;
	}

	sm_hWMVCore = ::LoadLibrary("Wmvcore.dll");
	if(sm_hWMVCore)
	{
		return true;
	}
	return false;
}

//=====================================================================================================

bool audWMStreamReader::MediaOpen(const WCHAR* pszFileName, u32 startOffsetMs)
{
	bool bResult = false;
	bResult = Open(pszFileName, startOffsetMs);
	return bResult;
}

//=====================================================================================================
bool audWMStreamReader::Open(const WCHAR* pwszFileName, u32 startOffsetMs)
{
	bool rVal = false;
	HRESULT hResult = S_OK;

	if (IsFileOpen())
	{
		return false;
	}

	hResult = CoInitialize(NULL);

	WMCreateSyncReaderProc pWMCreateSyncReader = reinterpret_cast<WMCreateSyncReaderProc>(GetProcAddress(sm_hWMVCore, "WMCreateSyncReader"));

	if(pWMCreateSyncReader == NULL)
	{
		return false;
	}

	if(pWMCreateSyncReader(NULL, WMT_RIGHT_PLAYBACK, &m_pReader) != S_OK)
	{
		SAFE_RELEASE(m_pReader);
		return false;
	}
	
	m_dwExtraBytes = 0;

	hResult = m_pReader->Open(pwszFileName);
	if (FAILED(hResult))
	{
		audErrorf("audWMStreamReader::Open failed with HRESULT %08X", hResult);
		SAFE_RELEASE(m_pReader);
		return false;
	}

	// check this file contains a valid audio stream
	IWMProfile* pProfile = NULL;

	hResult = m_pReader->QueryInterface(IID_IWMProfile, (void**)&pProfile);
	if (FAILED(hResult)) 
	{
		Errorf("Couldn't get IWMProfile interface for WMA file %s", pwszFileName);
	}
	else
	{
		// Find out stream numbers for video and audio using the profile
		hResult = GetStreamNumbers(pProfile);
		SAFE_RELEASE(pProfile);
		if (FAILED(hResult)) 
		{
			Errorf("Couldn't get audio stream number for WMA stream %s", pwszFileName);
		}
		else
		{
			WMT_STREAM_SELECTION wmtSS = WMT_ON;

			hResult = m_pReader->SetStreamsSelected(1, &m_wAudioStreamNum, &wmtSS);
			Assert(SUCCEEDED(hResult));
			hResult = m_pReader->SetReadStreamSamples(m_wAudioStreamNum, 0);
			Assert(SUCCEEDED(hResult));
			hResult = m_pReader->SetRange(10000 * startOffsetMs,0);		
			Assert(SUCCEEDED(hResult));

			m_StartOffsetMs = startOffsetMs;

			DWORD dwOutputNum;

			hResult = m_pReader->GetOutputNumberForStream(m_wAudioStreamNum, &dwOutputNum);
			Assert(SUCCEEDED(hResult));

			// check this stream has a valid output format
			IWMOutputMediaProps*  ppOutput;

			hResult = m_pReader->GetOutputProps(dwOutputNum, &ppOutput);
			if(SUCCEEDED(hResult))
			{
				DWORD size;

				hResult = ppOutput->GetMediaType(NULL, &size);	
				Assert(SUCCEEDED(hResult));

				WM_MEDIA_TYPE* mediaType;

				mediaType = (WM_MEDIA_TYPE*)rage_new u8[size];
				hResult = ppOutput->GetMediaType(mediaType, &size);	
				Assert(SUCCEEDED(hResult));

				WAVEFORMATEX* waveHeader;

				waveHeader = (WAVEFORMATEX*)mediaType->pbFormat;
				if(waveHeader && waveHeader->nChannels == 2 && waveHeader->wBitsPerSample == 16)
				{
					m_dwSampleRate = waveHeader->nSamplesPerSec;
					m_nBlockAlign = waveHeader->nBlockAlign;

					IWMHeaderInfo3* headerInfo;

					if(SUCCEEDED(m_pReader->QueryInterface(IID_IWMHeaderInfo3, (void**)&headerInfo)))
					{
						DWORD dwLen;
						WORD count;
						WORD* indices;
						QWORD qwDuration; 
						WMT_ATTR_DATATYPE type = WMT_TYPE_QWORD;

						dwLen = sizeof(QWORD);

						// find the Duration index
						hResult = headerInfo->GetAttributeIndices(0, L"Duration", NULL, NULL, &count);
						Assert(SUCCEEDED(hResult));

						indices = rage_new WORD[count];
						hResult = headerInfo->GetAttributeIndices(0, L"Duration", NULL, indices, &count);
						Assert(SUCCEEDED(hResult));

						hResult = headerInfo->GetAttributeByIndexEx(0, indices[0], NULL, &count, &type, NULL, (BYTE*)&qwDuration, &dwLen);
						Assert(SUCCEEDED(hResult));

						m_lDuration = (long)(qwDuration / 10000);

						delete[] indices;

						WORD streamNum = 0;
						WMT_ATTR_DATATYPE dataType;
						BYTE *val;
						WORD length = 0;
						hResult = headerInfo->GetAttributeByName(&streamNum,L"Title",&dataType,NULL,&length);
						if(SUCCEEDED(hResult))
						{
							val = rage_new BYTE[length];
							hResult = headerInfo->GetAttributeByName(&streamNum,L"Title",&dataType,val,&length);

							if(SUCCEEDED(hResult))
							{
								WideCharToMultiByte(CP_UTF8,0, (LPCWSTR)val,length>>1,(LPSTR)&m_Title,sizeof(m_Artist),NULL,NULL);
							}

							delete[] val;
						}

						hResult = headerInfo->GetAttributeByName(&streamNum,L"Author",&dataType,NULL,&length);
						if(SUCCEEDED(hResult))
						{
							val = rage_new BYTE[length];
							hResult = headerInfo->GetAttributeByName(&streamNum,L"Author",&dataType,val,&length);

							if(SUCCEEDED(hResult))
							{
								WideCharToMultiByte(CP_UTF8,0, (LPCWSTR)val,length>>1,(LPSTR)&m_Artist,sizeof(m_Artist),NULL,NULL);
							}

							delete[] val;
						}
						headerInfo->Release();

						// GetStreamLengthMs() needs to be m_bInitialised
						//m_bInitialised = true;
						SetFileOpen(true);
						if(GetStreamLengthMs() >= kMinSongDuration)
						{
							rVal = true;
						}
						else
						{
							Errorf("WMA decoder - track too short. File %s", pwszFileName);
						}
					}
				}
				else
				{
					Errorf("WMA Decoder: invalid header or not 16bit stereo track. File %s", pwszFileName);
				}
				delete(mediaType);
			} 
			ppOutput->Release();
		} 
	} 

	SetFileOpen(rVal);
	if(!rVal)
	{
		SAFE_RELEASE(m_pReader);
	}

#ifdef OUTPUT_FILLBUFFER_AUDIO
	if(rVal)
	{
		m_hFile = CreateFile("c:\\FillBufferOut.raw", GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);
	}
#endif

	return rVal;
}

//=====================================================================================================
bool audWMStreamReader::MediaClose()
{
#ifdef OUTPUT_FILLBUFFER_AUDIO
	if(m_hFile)
	{
		CloseHandle(m_hFile);
		m_hFile = NULL;
	}
#endif
	if (m_pReader == NULL)
	{
		return false;
	}

	if (!IsFileOpen())
	{
		return false;
	}

	HRESULT hResult = m_pReader->Close();
	if (FAILED(hResult))
	{
		return false;
	}
	SAFE_RELEASE(m_pReader);
	m_pReader = NULL;

	return true;
}

void audWMStreamReader::Prepare()
{
}

//=====================================================================================================
s32 audWMStreamReader::FillBuffer(void *sampleBuf, u32 dwBytesToRead)
{
	INSSBuffer* pSample = NULL;
	DWORD dwFlags = 0;
	DWORD dwOutputNum = 0;
	WORD wStreamNum = 0;
	DWORD dwLength = 0;
	DWORD dwBytesToReadThisTime = 0;
	DWORD dwBytesRead = 0;	
	BYTE *pBuf = NULL;
	bool success = true;

	// firstly copy any extra bytes from last time
	if(m_dwExtraBytes > 0 && m_dwExtraBytes < dwBytesToRead)
	{
		memcpy(sampleBuf, &m_cExtraBytes, m_dwExtraBytes);
		dwBytesRead = m_dwExtraBytes;
		m_dwExtraBytes = 0;
	}
	else if(m_dwExtraBytes > dwBytesRead)
	{
		memcpy(sampleBuf, &m_cExtraBytes, dwBytesToRead);
		dwBytesRead = dwBytesToRead;
		// move the remaining extra bytes to the start of the buffer
		memcpy(&m_cExtraBytes, &m_cExtraBytes[dwBytesToRead], m_dwExtraBytes - dwBytesToRead);
		m_dwExtraBytes -= dwBytesToRead;
	}

	while( m_pReader && success && dwBytesRead < dwBytesToRead)
	{

		HRESULT hr = m_pReader->GetNextSample(m_wAudioStreamNum,&pSample,&m_qwLastSampleTime,&m_qwLastSampleDuration,&dwFlags,&dwOutputNum,&wStreamNum);

		if(FAILED(hr))
		{
			success = false;
			break;
		}

		else if(m_wAudioStreamNum != wStreamNum)
		{
			continue; // not audio stream
		}

		dwBytesToReadThisTime = dwBytesToRead-dwBytesRead;
		pSample->GetBufferAndLength(&pBuf, &dwLength);

		memcpy((char*)sampleBuf + dwBytesRead, pBuf, min(dwLength, dwBytesToReadThisTime));
		if(dwLength > dwBytesToReadThisTime)
		{
			// store the extra bytes in a buffer for the next read
			m_dwExtraBytes = dwLength - dwBytesToReadThisTime;

			// fix for WM10 - prevent buffer overflow
			if(m_dwExtraBytes > EXTRA_BYTE_BUFFER_SIZE)
				m_dwExtraBytes = EXTRA_BYTE_BUFFER_SIZE;

			memcpy(&m_cExtraBytes, (char*)pBuf + dwBytesToReadThisTime, m_dwExtraBytes);
		}
		else
			m_dwExtraBytes = 0;

		dwBytesRead += min(dwLength, dwBytesToReadThisTime);

		pSample->Release();
		success = true;
	} // while (success && not read enough)

//#ifdef OUTPUT_FILLBUFFER_AUDIO
//	DWORD written;
//	WriteFile(m_hFile, sampleBuf, dwBytesRead, &written, NULL);
//#endif

	return dwBytesRead;
}

//=====================================================================================================

HRESULT audWMStreamReader::GetStreamNumbers(IWMProfile* pProfile)
{
	HRESULT hr = S_OK;
	IWMStreamConfig* pStream = NULL;
	DWORD dwStreams = 0;
	GUID pguidStreamType;

	if ( NULL == pProfile )
	{
		return( E_INVALIDARG );
	}

	hr = pProfile->GetStreamCount( &dwStreams );
	if (FAILED(hr))
	{
		Errorf("GetStreamCount on IWMProfile failed (hr=0x%08x).\n", hr );
		return( hr );
	}

	m_wAudioStreamNum = 0;

	for( DWORD i = 0; i < dwStreams; i++ )
	{
		hr = pProfile->GetStream( i, &pStream );
		if (FAILED(hr))
		{
			Errorf("Could not get Stream %d of %d from IWMProfile (hr=0x%08x).\n", i, dwStreams, hr);
			break;
		}

		WORD wStreamNumber = 0 ;

		//  Get the stream number of the current stream
		hr = pStream->GetStreamNumber( &wStreamNumber );
		if (FAILED(hr))
		{
			Errorf("Could not get stream number from IWMStreamConfig %d of %d (hr=0x%08x).\n", i, dwStreams, hr );
			break;
		}

		hr = pStream->GetStreamType( &pguidStreamType );
		if (FAILED(hr))
		{
			Errorf("Could not get stream type of stream %d of %d from IWMStreamConfig (hr=0x%08x).\n", i, dwStreams, hr ) ;
			break ;
		}

		if( WMMEDIATYPE_Audio == pguidStreamType )
		{
			m_wAudioStreamNum = wStreamNumber;
			break; // we're only interested in the first audio stream we find
		}

		SAFE_RELEASE( pStream );
	} // for each stream

	if(!m_wAudioStreamNum)
		hr = E_INVALIDARG;

	return( hr );
}
//=====================================================================================================

s32 audWMStreamReader::GetSampleRate()
{
	if(IsFileOpen())
		return m_dwSampleRate;
	else
		return -1;
}
//=====================================================================================================

s32 audWMStreamReader::GetStreamLengthMs()
{
	if(IsFileOpen())
	{
		return m_lDuration;
	}
	else
		return -1;

}
//=====================================================================================================

s32 audWMStreamReader::GetStreamPlayTimeMs()
{
	if(IsFileOpen())
	{
		return (long)((m_qwLastSampleTime + m_qwLastSampleDuration) / 10000.0f);
	}
	else
		return -1;
}
//=====================================================================================================

void audWMStreamReader::SetCursor(u32 ms)
{
	if(!IsFileOpen())
		return;
	// convert to 100-nanosecond units
	m_pReader->SetRange(ms * 10000, 0);
}

}

#endif
