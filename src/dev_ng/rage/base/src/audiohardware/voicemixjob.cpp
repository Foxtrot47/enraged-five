// 
// audiohardware/voicemixjob.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 


#define AUD_DISABLE_ASSERTS __SPU

#if AUD_DISABLE_ASSERTS
#undef __ASSERT
#define __ASSERT 0
#undef ASSERT_ONLY
#define ASSERT_ONLY(x)


#undef Assert
#define Assert(x)
#undef Assertf
#define Assertf(x,...)
#undef FastAssert
#define FastAssert(x)
#undef AssertMsg
#define AssertMsg(x,msg)
#undef AssertVerify
#define AssertVerify(x) (x)
#undef Verifyf
#define Verifyf(x,fmt,...) (x)
#undef DebugAssert
#define DebugAssert(x)
#endif

#include "system/taskheader.h"

#if __SPU

#define AUD_SYNTHCORE_DSP_SPU

#include "driverdefs.h"
#include "mixer.h"
#include "submix.h"
#include "submixcache.h"
#define __AUD_STRIP_CMD_BUFFER
#include "connection.cpp"
#include "framebuffercache.h"
#include "framebufferpool.h"
#include "mixervoice.cpp"
#include "submix.cpp"
#include "submixcache.cpp"
#include "submixjob.h"
#include "voicemixjob.h"
#include "atl/array.h"

#include "dspeffect.cpp"
#include "audioeffecttypes/biquadfiltereffect.cpp"
#include "audioeffecttypes/delayeffect.cpp"
#include "audioeffecttypes/variabledelayeffect.cpp"
#include "audioeffecttypes/waveshapereffect.cpp"
#include "audiohardware/src_linear.cpp"
#include "audiosynth/synthcorejobdeps.h"
#include "audioengine/spuutil.h"

#include "voicefilter_vmath.inl"

namespace rage
{
	extern audMixerConnectionPool *g_MixerConnectionPool;
	audFrameBufferCache *g_FrameBufferCache = NULL;
	audFrameBufferPool *g_FrameBufferPool = NULL;
	bool g_DebugFrameBufferCache = false;
	bool g_DisableAllVoiceProcessing = false;
	bool g_DisableVoiceFilters = false;

	bool g_MeterSubmixes = false;

	const void *synthSynthesizer::sm_UnloadingMetadataEndPtr = NULL;
	const void *synthSynthesizer::sm_UnloadingMetadataStartPtr = NULL;
	
	void VoiceMixJobEntry(sysTaskParameters &p)
	{
		CompileTimeAssert(sizeof(submixInputData)==4);
		submixInputData inputData;

		inputData.val32 = p.UserData[0].asUInt;
		void *submixesEA = p.UserData[1].asPtr;
		const u32 frameBufferPoolEA = p.UserData[2].asUInt;
		const u32 numFrameBuffers = p.UserData[3].asUInt;
		const u32 voicesEA = p.UserData[4].asUInt;
		const u32 voicesSizeBytes = p.UserData[5].asUInt;
		const bool debug = p.UserData[6].asBool;
		const u32 numCacheBuffers = p.UserData[7].asUInt;
		g_MeterSubmixes = p.UserData[8].asBool;
		
		synthSynthesizer::SetUnloadingRegion(p.UserData[10].asPtr, p.UserData[11].asPtr);

		s32 numProcessingStages;
		u32 numSubmixCacheChannels;
		u32 numOutputChannels;
		audOutputMode downmixMode;
		audOutputMode hardwareOutputMode;

		Assign(hardwareOutputMode, inputData.fields.hardwareOutputMode);
		Assign(downmixMode, inputData.fields.downmixMode);
		Assign(numOutputChannels, inputData.fields.numOutputChannels);
		Assign(numProcessingStages, inputData.fields.numProcessingStages);
		Assign(numSubmixCacheChannels, inputData.fields.numSubmixCacheChannels);
		
				
		const u32 frameBufferPoolSizeBytes = sizeof(audFrameBufferPool);
		
		DEV_ONLY(g_DisplayAllocations = g_DebugFrameBufferCache = debug);

		InitScratchBuffer(p.Scratch.Data, p.Scratch.Size);

		// connection pool is in/out
		g_MixerConnectionPool = (audMixerConnectionPool*)p.Input.Data;
		Assert(p.Output.Data == NULL && p.Output.Size == 0);

		DEV_ONLY(if(g_DisplayAllocations) audDisplayf("ConnectionPool %zu", p.Input.Size));

		// DMA into scratch...
		const s32 inputTag = 4;

		g_FrameBufferPool = (audFrameBufferPool*)AllocateFromScratch(frameBufferPoolSizeBytes, "FrameBufferPool");
		Assert((frameBufferPoolEA&15)==0);
		Assert((frameBufferPoolSizeBytes&15)==0);
	
		sysDmaGet(g_FrameBufferPool, frameBufferPoolEA, frameBufferPoolSizeBytes, inputTag);

		// grab submix array into scratch		
		size_t submixesSize = sizeof(atFixedArray<audMixerSubmix,kMaxSubmixes>);
		atFixedArray<audMixerSubmix,kMaxSubmixes> *submixes = (atFixedArray<audMixerSubmix,kMaxSubmixes> *)AllocateFromScratch(submixesSize, "Submixes");
		Assert(((size_t)submixesEA&15)==0);
		Assert((submixesSize&15)==0);
		
		sysDmaGet(submixes, (uint64_t)submixesEA, submixesSize, inputTag);
		audFrameBufferCache frameBufferCache;
		g_FrameBufferCache = &frameBufferCache;


		audSubmixCache submixCache;
		g_SubmixCache = &submixCache;

		g_SubmixCache->SetSubmixes(submixes);

		const u32 cacheStorageSizeBytes = numCacheBuffers*sizeof(f32)*kMixBufNumSamples;
		f32 *cacheStorage = (f32*)AllocateFromScratch(cacheStorageSizeBytes, "FrameBufferCache");
		
		if(debug)
		{
			audDisplayf("Before voices, %zu free", g_ScratchLeft);
		}

		SetScratchBookmark();
	
		// DMA voice array into scratch
		atFixedArray<audMixerVoice, kMaxVoices> *voices = (atFixedArray<audMixerVoice, kMaxVoices> *)AllocateFromScratch(voicesSizeBytes, "Voices");
		Assert(sizeof(atFixedArray<audMixerVoice, kMaxVoices>) == voicesSizeBytes);
		Assert((voicesEA&15) == 0);
		Assert((voicesSizeBytes&15)==0);		

		sysDmaLargeGet(voices, (uint64_t)voicesEA, voicesSizeBytes, inputTag);

		// wait on input data
		sysDmaWaitTagStatusAll(1<<inputTag);

		if(debug)
		{
			audDisplayf("After voices, %zu free", g_ScratchLeft);
		}

		// Init buffer cache
		Assert(g_FrameBufferPool->GetNumBuffers() == p.UserData[3].asUInt);
		frameBufferCache.Init(g_FrameBufferPool->GetBufferEA(0), numFrameBuffers, cacheStorage, numCacheBuffers, 3);

		atFixedArray<s32, kMaxVoices> voicesToMix;

		const s32 numVoices = voices->GetCount();

		for(s32 k = 0; k < numVoices; k++)
		{			
			audMixerVoice &voice = (*voices)[k];
			if(voice.GetState() == audMixerVoice::PLAYING && voice.GetInputBufferId() < 0xffff)
			{
				voicesToMix.Append() = k;
			}
		}
		// mix voices into submixes
		const s32 numVoicesToMix = voicesToMix.GetCount();
		for(s32 vi = 0; vi < numVoicesToMix; vi++)
		{
			if(vi < numVoicesToMix - 1)
			{
				// Prefetch the input data for the next voice
				audMixerVoice &nextVoice = (*voices)[voicesToMix[vi+1]];
				g_FrameBufferCache->PrefetchBuffer(nextVoice.GetInputBufferId(), true);
			}

			audMixerVoice &voice = (*voices)[voicesToMix[vi]];
			
			voice.Mix();
			// Tell the cache we're finished with this entry.
			const u32 bufferId = voice.GetInputBufferId();
			if(debug)
			{
				audDisplayf("Mixing voice %u from %u", voicesToMix[vi], bufferId);
			}
			if(bufferId < numFrameBuffers)
			{
				g_FrameBufferCache->FreeEntry(bufferId);
			}		
		}

		if(debug)
		{
			audDisplayf("Voices - Fetched: %u Written: %u WB: %u", frameBufferCache.GetNumFetches(), frameBufferCache.GetNumWrites(), frameBufferCache.GetNumWriteBacks());
			frameBufferCache.ResetStats();
		}

		// DMA voices back to PPU
		sysDmaLargePut(voices, voicesEA, voicesSizeBytes, inputTag);

		sysDmaWait(1<<inputTag);

		// reclaim voice LS
		ResetScratchToBookmark();

		// Run DSP on source effect submixes
		for(atFixedArray<audMixerSubmix,kMaxSubmixes>::iterator i = submixes->begin(); i != submixes->end(); i++)	
		{
			if((*i).IsSourceEffectsSubmix())
			{
				(*i).ProcessDSP(g_ScratchBuf, g_ScratchLeft);
			}		
		}
		
		// Ensure all frame buffers are written back to main RAM
		g_FrameBufferCache->Shrink(0);

		// Write submixes back to main to preserve buffer allocation
		sysDmaPut(submixes, (uint64_t)submixesEA, submixesSize, inputTag);

		// Update frame buffer pool
		sysDmaPut(g_FrameBufferPool, frameBufferPoolEA, frameBufferPoolSizeBytes, inputTag);

		sysDmaWait(1<<inputTag);

		g_FrameBufferCache->WaitForAllSlots();
	}

} // namespace rage
#endif // __SPU

using namespace rage;
void VoiceMixJob(sysTaskParameters &SPU_ONLY(p))
{
	SPU_ONLY(rage::VoiceMixJobEntry(p));
}

