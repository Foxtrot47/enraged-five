// 
// audiohardware/device_ps3.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DEVICE_PS3_H
#define AUD_DEVICE_PS3_H

#if __PS3

#include "device.h"
#include "system/ipc.h"

#include <sys/event.h>

#define AUD_OUTPUT_TO_FILE 1

namespace rage
{
	class fiStream;
	class audMixerDevicePs3 : public audMixerDevice
	{
	public:

		audMixerDevicePs3();

		// PURPOSE
		//	Initialises libaudio output port
		// RETURNS
		//	false on failure
		virtual bool InitHardware();

		// PURPOSE
		//	Releases libaudio resources
		virtual void ShutdownHardware();

		// PURPOSE
		//	Starts generating output
		virtual void StartMixing();

		void *GetDecoderMem() const { return m_LibMP3Mem; }

		// PURPOSE
		//	Check to see if the pulse headset is connected to the PS3
		bool IsPulseHeadsetConnected(const bool loadPRX = false) const;

	private:

		static DECLARE_THREAD_FUNC(sMixLoop);

		int ConfigureOutputHardware(unsigned int _flags);
		void MixLoop();

		ALIGNAS(16) f32 m_InterleavedOutput[kMixBufNumSamples * 8] ;

		u32 m_PortId;
		f32 *m_PortBuffer;		

		void *m_LibMP3Mem;
		sys_event_queue_t m_EventQueueId;
		sys_ipc_key_t m_EventQueueKey;

		volatile bool m_IsThreadRunning;

#if AUD_OUTPUT_TO_FILE
		fiStream *m_DebugOutput;
#endif
	};

}

#endif // __PS3
#endif // AUD_DEVICE_PS3_H
