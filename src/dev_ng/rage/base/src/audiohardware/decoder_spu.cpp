#if __PS3

#include "channel.h"
#include "decoder_spu.h"
#include "mp3util.h"

#include "audiohardware/driver.h"
#include "audioengine/spuutil.h"
#include "math/amath.h"
#include "grcore/wrapper_gcm.h"
#include "system/memops.h"

#if __PPU
#pragma comment(lib, "msmp3dec_ppu")
#endif

namespace rage
{
#if __PPU
	
	u8 *audDecoderSpu::sm_DecoderMemory = NULL;
	u8 *audDecoderSpu::sm_FixedHeap = NULL;
	sysMemFixedAllocator *audDecoderSpu::sm_ContextMemoryAllocator = NULL;

	bool audDecoderSpu::InitClass()
	{
		audDecoderVramHelper::InitClass();

		int requiredMemorySize = 0;
		if(Verifyf(cellMSMP3IntegratedDecoderGetRequiredSize(&requiredMemorySize) == 0, "MP3 decoder failed to report memory requirements: %d bytes", requiredMemorySize))
		{
			audDisplayf("MP3 decoder consuming %d bytes", requiredMemorySize);
			sm_DecoderMemory = rage_aligned_new(128) u8[requiredMemorySize];
		}

		// Stereo decoders require two contexts. No point allocating more stereo contexts than there are physical voices.
		u32 maxContexts = audDriver::GetConfig().GetMaxPhysicalStereoVoices() + audDriver::GetConfig().GetNumPhysicalVoices() + audDriver::GetConfig().GetNumGranularDecoders();
		sm_FixedHeap = rage_aligned_new(128) u8[sizeof(CellMSMP3Context) * maxContexts];
		sm_ContextMemoryAllocator = rage_new sysMemFixedAllocator(sm_FixedHeap, sizeof(CellMSMP3Context), maxContexts);

		audDisplayf("audDecoderSpu size %d", sizeof(audDecoderSpu));
		audDisplayf("CellMSMP3Context size %d", sizeof(CellMSMP3Context));
		audDisplayf("sm_ContextMemoryAllocator size %d", sizeof(CellMSMP3Context) * maxContexts);

		if(Verifyf(cellMSMP3IntegratedDecoderInit(sm_DecoderMemory, requiredMemorySize) == 0, "Failed to initialise MP3 decoder (mem: %d", requiredMemorySize))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void audDecoderSpu::ShutdownClass()
	{
		audDecoderVramHelper::ShutdownClass();

		if(sm_DecoderMemory)
		{
			delete[] sm_DecoderMemory;
			sm_DecoderMemory = NULL;
		}
	}

#endif // __PPU

	audDecoderSpu::audDecoderSpu()
		: m_ReadFrameIndex(0)
		, m_Context1(NULL)
		, m_Context2(NULL)
		, m_InternalCount(1)
		, m_SamplesConsumed(0)
		, m_FramesDecoded(0)
		, m_SubmitPacketIndex(0)
		, m_DecodePacketIndex(0)
		, m_NumPacketsQueued(0)
		, m_NumChannels(0)
		, m_SkipSamples(0)
		, m_State(audDecoder::IDLE)
		, m_IsDataInVRAM(false)
		, m_IsDataMP3(false)
	{

	}

	void audDecoderSpu::ClearBuffer()
	{
		m_FramesDecoded = 0;
		m_SamplesConsumed = 0;

		m_SubmitPacketIndex = 0;
		m_DecodePacketIndex = 0;

		m_NumPacketsQueued = 0;

		m_ReadFrameIndex = 0;

		m_DecodedSamplesAvailable[0] = m_DecodedSamplesAvailable[1] = 0;
		m_DecodeBufferSize[0] = m_DecodeBufferSize[1] = 0;

		sysMemZeroBytes<sizeof(m_DecodeBuffer)>(&m_DecodeBuffer);
		sysMemZeroBytes<sizeof(m_Packets)>(&m_Packets);

#if !__SPU
		if(m_Context1)
		{
			sysMemZeroBytes<sizeof(CellMSMP3Context)>(m_Context1);
		}

		if(m_Context2)
		{
			sysMemZeroBytes<sizeof(CellMSMP3Context)>(m_Context2);
		}
#else
		{
			audAutoScratchBookmark	autoScratch;
			const s32 contextMemoryTag = 5;
			u8* context = (u8*) AllocateFromScratch( sizeof(CellMSMP3Context), 128, "ContextMemory" );
			sysMemZeroBytes<sizeof(CellMSMP3Context)>(context);

			if(m_Context1)
			{
				// If we're on SPU then we need to wipe the PPU side buffer
				sysDmaPut(context, (uint64_t)m_Context1, sizeof(CellMSMP3Context), contextMemoryTag);
			}

			if(m_Context2)
			{
				sysDmaPut(context, (uint64_t)m_Context2, sizeof(CellMSMP3Context), contextMemoryTag);
			}

			sysDmaWaitTagStatusAll(1 << contextMemoryTag);
		}
#endif
		
		m_InternalCount = 1;

		m_State = audDecoder::IDLE;
	}

#if !__SPU
	bool audDecoderSpu::Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate))
	{
		Assert(format == audWaveFormat::kMP3 || format == audWaveFormat::kPcm16bitBigEndian);

		m_FramesDecoded = 0;
		m_SamplesConsumed = 0;

		m_SubmitPacketIndex = 0;
		m_DecodePacketIndex = 0;

		m_NumPacketsQueued = 0;

		m_ReadFrameIndex = 0;

		m_IsDataMP3 = (format == audWaveFormat::kMP3);

		m_DecodedSamplesAvailable[0] = m_DecodedSamplesAvailable[1] = 0;
		m_DecodeBufferSize[0] = m_DecodeBufferSize[1] = 0;

		sysMemZeroBytes<sizeof(m_DecodeBuffer)>(&m_DecodeBuffer);
		sysMemZeroBytes<sizeof(m_Packets)>(&m_Packets);

		if(!m_Context1)
		{
			m_Context1 = (CellMSMP3Context*)sm_ContextMemoryAllocator->Allocate(sizeof(CellMSMP3Context), 128, 0);
			audAssert(m_Context1);
		}

		if(m_Context1)
		{
			sysMemZeroBytes<sizeof(CellMSMP3Context)>(m_Context1);
		}

		if(!m_Context2 && numChannels > 1)
		{
			m_Context2 = (CellMSMP3Context*)sm_ContextMemoryAllocator->Allocate(sizeof(CellMSMP3Context), 128, 0);
			audAssert(m_Context2);
		}

		if(m_Context2)
		{
			sysMemZeroBytes<sizeof(CellMSMP3Context)>(m_Context2);
		}
		
		m_InternalCount = 1;

		m_NumChannels = numChannels;

		m_State = audDecoder::IDLE;

		m_IsDataInVRAM = false;

		return (format == audWaveFormat::kMP3 || format == audWaveFormat::kPcm16bitBigEndian) && m_Context1 && (numChannels < 2 || m_Context2);
	}
#endif

#if !__SPU
	void audDecoderSpu::Shutdown()
	{
		if(m_Context1)
		{
			sm_ContextMemoryAllocator->Free(m_Context1);
			m_Context1 = NULL;
		}

		if(m_Context2)
		{
			sm_ContextMemoryAllocator->Free(m_Context2);
			m_Context2 = NULL;
		}
	}
#endif

	void audDecoderSpu::SubmitPacket(audDecoderSpuPacket &packet)
	{
		Assert(QueryReadyForMoreData());
		Assert(packet.InputBuffer);
		Assert(packet.InputBufferSize);

		sysMemZeroBytes<sizeof(audDecoderSpuPacket)>(&m_Packets[m_SubmitPacketIndex]);
		m_Packets[m_SubmitPacketIndex] = packet;

		m_IsDataInVRAM = packet.IsDataInVram;

#if !__SPU
		if(m_IsDataInVRAM)
		{
			if(!m_VramHelper.IsInitialised())
			{
				m_VramHelper.Init(packet.WaveSlotID, m_IsDataMP3);
			}

			// Submit the entire packet, with an offset so that loops can work
			m_VramHelper.SubmitPacket(m_Packets[m_SubmitPacketIndex].InputBuffer, m_Packets[m_SubmitPacketIndex].InputBufferBytesRead, 
				m_Packets[m_SubmitPacketIndex].InputBufferSize, m_Packets[m_SubmitPacketIndex].LoopStartOffsetBytes);
		}
#else
		Assertf(!m_IsDataInVRAM, "Submitting packets for Vram assets is not supported on SPU");
#endif

		m_NumPacketsQueued ++;
		Assert(m_Packets.size() == 2);
		m_SubmitPacketIndex = (m_SubmitPacketIndex + 1) & 1;

		m_State = audDecoder::DECODING;
	}

#if !__SPU
	bool audDecoderSpu::GenerateSpuPacket(audPacket &packet, audDecoderSpu::audDecoderSpuPacket &outputPacket, bool isDataMP3)
	{
		if(!audVerifyf(packet.inputData, "Invalid Input Data") ||
		   !audVerifyf(packet.inputBytes, "Invalid Input Bytes"))
		{
			return false;
		}

		audDecoderSpuPacket spuPacket;
		sysMemZeroBytes<sizeof(audDecoderSpuPacket)>(&spuPacket);

		spuPacket.WaveSlotID = packet.WaveSlotId;
		spuPacket.InputBuffer = (u8*)packet.inputData;

		spuPacket.InputBufferSize = packet.inputBytes;
		spuPacket.InputBufferBytesRead = 0;

		spuPacket.IsDataInVram = gcm::IsLocalPtr(spuPacket.InputBuffer);

		if(packet.PlayBegin != 0)
		{
			if(packet.SkipFirstFrame)
			{
				// Offset by a frame's worth of samples
				 packet.PlayBegin += audMp3Util::kFrameSizeSamples;
			}
			
			if(spuPacket.IsDataInVram && isDataMP3)
			{
				if(packet.SeekTable)
				{
					audMp3Util::ComputeFrameOffsetFromSamples(packet.SeekTable, 
						packet.SeekTableSize,
						packet.PlayBegin,
						spuPacket.InputBufferBytesRead,
						spuPacket.PlayBegin);
				}
				else
				{
					// We need to fetch the data before we can skip it
					spuPacket.PlayBegin = packet.PlayBegin;
				}
			}
			else
			{
				if(isDataMP3)
				{
					// Work out number of bytes to skip to get the frame start, and residual subframe offset
					const u32 wholeFramesToSkip = packet.PlayBegin / audMp3Util::kFrameSizeSamples;
					u32 bytesToSkip = audMp3Util::ComputeByteOffsetForFrame(
						spuPacket.InputBuffer, 
						spuPacket.InputBufferSize, 
						audMp3Util::kFrameSizeSamples, 
						wholeFramesToSkip);

					spuPacket.InputBufferBytesRead += bytesToSkip;
					spuPacket.PlayBegin = packet.PlayBegin - (wholeFramesToSkip*audMp3Util::kFrameSizeSamples);

					if(packet.PlayEnd != 0)
					{
						const u32 wholeFramesToSkipToEnd = (packet.PlayEnd / audMp3Util::kFrameSizeSamples) + 1;

						u32 bytesToSkipForEnd = audMp3Util::ComputeByteOffsetForFrame(
							spuPacket.InputBuffer, 
							spuPacket.InputBufferSize, 
							audMp3Util::kFrameSizeSamples, 
							wholeFramesToSkipToEnd);

						spuPacket.InputBufferSize = bytesToSkipForEnd;
					}
				}
				else
				{
					spuPacket.InputBufferBytesRead += packet.PlayBegin<<1;
					spuPacket.PlayBegin = 0;
				}
				
			}

			if(spuPacket.InputBufferBytesRead >= spuPacket.InputBufferSize && packet.LoopBegin == ~0U)
			{
				audWarningf("Skipped entire packet; %u samples (%s)", packet.PlayBegin, packet.SkipFirstFrame ? "skipFirstFrame" : "no skip");
				return false;
			}

			// Calculate subframe sample offset
			
		}
		else if(packet.SkipFirstFrame)
		{
			// We need to decode/throw away the first frame if there is no start offset
			Assert(isDataMP3);
			spuPacket.PlayBegin = audMp3Util::kFrameSizeSamples;
		}
		if(packet.LoopBegin != ~0U)
		{
			Assert(!spuPacket.IsDataInVram || packet.SeekTable || !isDataMP3);

			if(isDataMP3)
			{
				if(packet.SeekTable)
				{
					audMp3Util::ComputeFrameOffsetFromSamples(	packet.SeekTable, 
						packet.SeekTableSize,
						packet.LoopBegin,
						spuPacket.LoopStartOffsetBytes,
						spuPacket.LoopBegin	);
				}
				else
				{
					// turn samples into bytes
					const u32 loopBeginWholeFrames = packet.LoopBegin / audMp3Util::kFrameSizeSamples;
					const u32 byteOffset = 
						loopBeginWholeFrames ?
						audMp3Util::ComputeByteOffsetForFrame(spuPacket.InputBuffer, 
						spuPacket.InputBufferSize, audMp3Util::kFrameSizeSamples, loopBeginWholeFrames)
						: 0;

					// Calculate subframe sample offset (not currently used; loops should be aligned to
					// whole frames).
					spuPacket.LoopBegin = packet.LoopBegin - (loopBeginWholeFrames*audMp3Util::kFrameSizeSamples);
					spuPacket.LoopStartOffsetBytes = byteOffset;
				}
			}
			else
			{
				spuPacket.LoopBegin = 0;
				spuPacket.LoopStartOffsetBytes = packet.LoopBegin<<1;
			}

			// Ensure we start with a valid InputBufferBytesRead
			// Note: not implementing elaborate looping logic here since it is expected that is done higher level,
			// so this is just to handle the case where InputBufferBytesRead is calculated to be equal to InputBufferSize
			if(spuPacket.InputBufferBytesRead >= spuPacket.InputBufferSize)
			{
				spuPacket.InputBufferBytesRead = spuPacket.LoopStartOffsetBytes;
			}
		}
		else
		{
			spuPacket.LoopStartOffsetBytes = ~0U;
		}

		spuPacket.PlayEnd = packet.PlayEnd;
		outputPacket = spuPacket;
		return true;
	}

#if !__SPU
	void audDecoderSpu::SubmitPacket(audPacket &packet)
	{
		Assert(QueryReadyForMoreData());
		Assert(packet.inputData);
		Assert(packet.inputBytes);

		sysMemZeroBytes<sizeof(audDecoderSpuPacket)>(&m_Packets[m_SubmitPacketIndex]);

		if(!GenerateSpuPacket(packet, m_Packets[m_SubmitPacketIndex], m_IsDataMP3))
		{
			return;
		}

		m_IsDataInVRAM = m_Packets[m_SubmitPacketIndex].IsDataInVram;

		if(m_IsDataInVRAM)
		{
			if(!m_VramHelper.IsInitialised())
			{
				m_VramHelper.Init(packet.WaveSlotId, m_IsDataMP3);
			}
			// Submit the entire packet, with an offset so that loops can work
			m_VramHelper.SubmitPacket(m_Packets[m_SubmitPacketIndex].InputBuffer, m_Packets[m_SubmitPacketIndex].InputBufferBytesRead, 
										m_Packets[m_SubmitPacketIndex].InputBufferSize, m_Packets[m_SubmitPacketIndex].LoopStartOffsetBytes);
		}

		m_NumPacketsQueued ++;
		Assert(m_Packets.size() == 2);
		m_SubmitPacketIndex = (m_SubmitPacketIndex + 1) & 1;

		m_State = audDecoder::DECODING;
	}
#endif

	void audDecoderSpu::PreUpdate()
	{
		if(m_IsDataInVRAM)
		{
			m_VramHelper.Update();
		}
	}

#else // __SPU

	extern void *g_Mp3DecoderWorkspace;
	extern void *g_Mp3DecoderMain;
	
	void audDecoderSpu::Update()
	{
		audAutoScratchBookmark	autoScratch;
		u8* mp3InputBuffer = (u8*) AllocateFromScratch( sizeof(u8) * (audMp3Util::kMaxFrameSizeBytes*2 + 16), 128, "Mp3InputBuffer" );

		if(m_State != audDecoder::DECODING)
		{
			return;
		}

		const s32 maxFramesToSkip = 2;
		bool hasDecoded = false;
		u32 decodeBufferIndex = m_ReadFrameIndex;

		// prevent a starvation situation when we have small amounts of data in each buffer by moving the next decoded packet
		// if it will fit into the current buffer
		if(m_DecodedSamplesAvailable[0] > 0 && m_DecodedSamplesAvailable[1] > 0 && m_DecodedSamplesAvailable[0] + m_DecodedSamplesAvailable[1] <= audMp3Util::kFrameSizeSamples)
		{
			// we have some data in both buffers; combine into one
			
			// move current packet data to the beginning of the buffer
			s16 *currentBuffer = &m_DecodeBuffer[audMp3Util::kFrameSizeSamples * m_ReadFrameIndex];
			u32 nextFrameIndex = (m_ReadFrameIndex+1) & 1;
			const s16 *nextBuffer = &m_DecodeBuffer[audMp3Util::kFrameSizeSamples * nextFrameIndex];
			const u32 offsetSamples = m_DecodeBufferSize[m_ReadFrameIndex] - m_DecodedSamplesAvailable[m_ReadFrameIndex];
			memmove(currentBuffer, currentBuffer + offsetSamples, sizeof(s16) * m_DecodedSamplesAvailable[m_ReadFrameIndex]);

			// copy next data to the end of the buffer
			sysMemCpy(currentBuffer + m_DecodedSamplesAvailable[m_ReadFrameIndex], nextBuffer, sizeof(s16) * m_DecodedSamplesAvailable[nextFrameIndex]);

			// Update state
			m_DecodedSamplesAvailable[m_ReadFrameIndex] += m_DecodedSamplesAvailable[nextFrameIndex];
			m_DecodeBufferSize[m_ReadFrameIndex] = m_DecodedSamplesAvailable[m_ReadFrameIndex];
			m_DecodedSamplesAvailable[nextFrameIndex] = 0;
		}

		for(s32 decodePass = 0; decodePass < maxFramesToSkip && decodeBufferIndex < 2 && !hasDecoded; decodePass++)
		{
			if(m_DecodedSamplesAvailable[decodeBufferIndex] == 0)
			{
				int bytesRead = 0;
				int bytesWritten = 0;

				audDecoderSpuPacket &packet = m_Packets[m_DecodePacketIndex];

				if(!packet.InputBuffer)
				{
					return;
				}

				u8 *inputBuffer = mp3InputBuffer;
				const s32 mp3InputTag = 4;

				u32 numBytesAvailable = 0;
				if(m_IsDataInVRAM)
				{
					numBytesAvailable = m_VramHelper.QueryNumBytesAvailable();
					if(m_IsDataMP3)
					{
						if(numBytesAvailable >= audMp3Util::kMinFrameSizeBytes)
						{
							u32 frameHeader;
							if(m_VramHelper.IsPeekDwordValid())
							{
								frameHeader = m_VramHelper.PeekDword();
							}
							else
							{
								// The peek dword is only valid for the first read each frame
								m_VramHelper.ReadData((u8*)&frameHeader, 4);
							}
							const u32 frameSize = audMp3Util::ComputeSizeOfFrame(frameHeader);
							if(!(frameSize >= audMp3Util::kMinFrameSizeBytes && frameSize <= audMp3Util::kMaxFrameSizeBytes))
							{
								audErrorf("Unsupported MP3 frame size: %u", frameSize);
								m_State = audDecoder::DECODER_ERROR;
								return;
							}
							if(numBytesAvailable >= frameSize)
							{
								numBytesAvailable = m_VramHelper.ReadData(mp3InputBuffer, frameSize);
							}
							else
							{
								m_VramHelper.DebugPrint();
								return;
							}
						}
						else
						{
							m_VramHelper.DebugPrint();
							return;
						}
					}
					else
					{
						// PCM input data - wait until we have an entire MP3 frames worth of sample data ready to grab
						u32 numBytesToRead;
						if(m_FramesDecoded == 0)
						{
							// Wait until we have lots of data before starting playback
							numBytesToRead = Min<u32>(audDecoderVramHelper::kFetchBufferSize*2U, packet.LoopStartOffsetBytes == ~0U ? packet.InputBufferSize - packet.InputBufferBytesRead : ~0U);
						}
						else
						{
							numBytesToRead = Min(audMp3Util::kFrameSizeSamples*2U, packet.LoopStartOffsetBytes == ~0U ? packet.InputBufferSize - packet.InputBufferBytesRead : ~0U);
						}

						if(numBytesAvailable >= numBytesToRead)
						{
							numBytesToRead = Min(audMp3Util::kFrameSizeSamples*2U,numBytesToRead);
							numBytesAvailable = m_VramHelper.ReadData(mp3InputBuffer, numBytesToRead);
							Assert(numBytesAvailable == numBytesToRead);
						}
						else
						{
							return;
						}
					}
				}
				else
				{
					// Deal with unaligned source ptr
					const uint64_t sourcePtrEa = (uint64_t)packet.InputBuffer + packet.InputBufferBytesRead;
					const uint64_t sourcePtrEaAligned = sourcePtrEa &~(15);
					const uint64_t alignmentPadding = sourcePtrEa - sourcePtrEaAligned;

					inputBuffer = mp3InputBuffer + alignmentPadding;

					sysDmaGet(mp3InputBuffer, sourcePtrEaAligned, audMp3Util::kMaxFrameSizeBytes + 16, mp3InputTag);
					sysDmaWaitTagStatusAll(1 << mp3InputTag);
				}

				if(m_IsDataMP3)
				{
					Assert(inputBuffer[0] == 0xFF && (inputBuffer[1]&0xE0) == 0xE0);

					bytesWritten = 0;
					bytesRead = 0;

					// If we're more than an entire frame away from the data we want there's no point decoding
					// Note: Only skip decoding if we're skipping more than a single packet; the preserveTransient stuff
					// relies on decoding the first packet.
					const u32 samplesToSkip = Min<u32>(m_SkipSamples + packet.PlayBegin, audMp3Util::kFrameSizeSamples);
					if(samplesToSkip > audMp3Util::kFrameSizeSamples)
					{
						const u32 samplesSkippedFromPlayBegin = Min<u32>(packet.PlayBegin, audMp3Util::kFrameSizeSamples);
						
						packet.PlayBegin -= samplesSkippedFromPlayBegin;						
						m_SkipSamples -= (samplesToSkip - samplesSkippedFromPlayBegin);
						bytesRead = audMp3Util::ComputeSizeOfFrame(inputBuffer);
					}
					else
					{
						short *output;
						audAutoScratchBookmark	autoScratchContext;
						const s32 contextMemoryTag = 5;

						{
							u8* context1 = (u8*) AllocateFromScratch( sizeof(CellMSMP3Context), 128, "ContextMemory1" );
							sysDmaGetAndWait(context1, (uint64_t)m_Context1, sizeof(CellMSMP3Context), contextMemoryTag);

							if(m_NumChannels == 1)
							{
								output = cellMSMP3IntegratedDecoderSpuDecode(g_Mp3DecoderWorkspace, g_Mp3DecoderMain, inputBuffer, &bytesRead, &bytesWritten, (CellMSMP3Context*)context1, (CellMSMP3Context*)context1, &m_InternalCount);
								sysDmaPut(context1, (uint64_t)m_Context1, sizeof(CellMSMP3Context), contextMemoryTag);
							}
							else
							{
								u8* context2 = (u8*) AllocateFromScratch( sizeof(CellMSMP3Context), 128, "ContextMemory2" );
								sysDmaGetAndWait(context2, (uint64_t)m_Context2, sizeof(CellMSMP3Context), contextMemoryTag);
								output = cellMSMP3IntegratedDecoderSpuDecode(g_Mp3DecoderWorkspace, g_Mp3DecoderMain, inputBuffer, &bytesRead, &bytesWritten, (CellMSMP3Context*)context1, (CellMSMP3Context*)context2, &m_InternalCount);
								sysDmaPut(context1, (uint64_t)m_Context1, sizeof(CellMSMP3Context), contextMemoryTag);
								sysDmaPut(context2, (uint64_t)m_Context2, sizeof(CellMSMP3Context), contextMemoryTag);
							}
						}
						
						audAssertf(bytesRead != 0 && bytesRead <= audMp3Util::kMaxFrameSizeBytes, "MP3 bytes read: %d", bytesRead);
						if(!audVerifyf(bytesWritten == audMp3Util::kFrameSizeSamples*sizeof(s16),"%u: MP3 bytes written: %d", m_WaveIdentifier, bytesWritten))
						{
							sysDmaWaitTagStatusAll(1 << contextMemoryTag);
							m_State = audDecoder::DECODER_ERROR;
							return;
						}
						ASSERT_ONLY(const u32 frameSize = audMp3Util::ComputeSizeOfFrame(inputBuffer));
						audAssertf((u32)bytesRead == frameSize, "%u: Mismatch between Sony and Us: %u / %u", bytesRead, frameSize, m_WaveIdentifier);
					
						s16 *decodeBufferPtr = &m_DecodeBuffer[decodeBufferIndex * audMp3Util::kFrameSizeSamples];
						const u32 bytesToSkip = samplesToSkip<<1;

						if(bytesToSkip < (u32)bytesWritten)
						{
							sysMemCpy(decodeBufferPtr, output + packet.PlayBegin, bytesWritten - bytesToSkip);
							m_DecodedSamplesAvailable[decodeBufferIndex] = (bytesWritten-bytesToSkip)>>1;
							m_DecodeBufferSize[decodeBufferIndex] = m_DecodedSamplesAvailable[decodeBufferIndex];
							m_FramesDecoded++;
							decodeBufferIndex = (decodeBufferIndex + 1) & 1;
							hasDecoded = true;
						}
						const u32 samplesSkippedFromPlayBegin = Min<u32>(packet.PlayBegin, audMp3Util::kFrameSizeSamples);

						packet.PlayBegin -= samplesSkippedFromPlayBegin;						
						m_SkipSamples -= (samplesToSkip - samplesSkippedFromPlayBegin);
						sysDmaWaitTagStatusAll(1 << contextMemoryTag);
					}
				}
				else
				{
					bytesRead = numBytesAvailable;
					s16 *decodeBufferPtr = &m_DecodeBuffer[decodeBufferIndex * audMp3Util::kFrameSizeSamples];

					sysMemCpy(decodeBufferPtr, mp3InputBuffer, bytesRead);
					m_DecodedSamplesAvailable[decodeBufferIndex] = bytesRead>>1;
					m_DecodeBufferSize[decodeBufferIndex] = m_DecodedSamplesAvailable[decodeBufferIndex];
					hasDecoded = true;
					m_FramesDecoded++;
					decodeBufferIndex = (decodeBufferIndex + 1) & 1;
				}

				if(m_IsDataInVRAM)
				{
					m_VramHelper.AdvanceReadPtr(bytesRead);
				}
			
				
				packet.InputBufferBytesRead += bytesRead;

				if(packet.InputBufferBytesRead >= packet.InputBufferSize)
				{
					if(packet.LoopStartOffsetBytes != ~0U)
					{
						packet.InputBufferBytesRead = packet.LoopStartOffsetBytes;
					}
					else
					{
						// Finished this packet
						packet.InputBuffer = NULL;
						packet.InputBufferSize = 0;
						m_DecodePacketIndex = (m_DecodePacketIndex+1) & 1;
						m_NumPacketsQueued--;

						if(m_NumPacketsQueued == 0 || m_Packets[m_DecodePacketIndex].InputBuffer == NULL)
						{
							// Finished stream
							m_State = audDecoder::FINISHED;
						}
					}
				}
			}
			else
			{
				decodeBufferIndex = (decodeBufferIndex + 1) & 1;
			}
		}
	}

#endif // __SPU

	u32 audDecoderSpu::QueryNumAvailableSamples() const
	{
		return m_DecodedSamplesAvailable[0] + m_DecodedSamplesAvailable[1];
	}

	bool audDecoderSpu::CanDecodeMoreData() const
	{
		return m_DecodedSamplesAvailable[0] == 0 || m_DecodedSamplesAvailable[1] == 0;
	}

	void audDecoderSpu::AdvanceReadPtr(const u32 numSamples)
	{
		if(m_DecodedSamplesAvailable[m_ReadFrameIndex] >= numSamples)
		{
			m_DecodedSamplesAvailable[m_ReadFrameIndex] -= numSamples;
			
			if(m_DecodedSamplesAvailable[m_ReadFrameIndex] == 0)
			{
				m_ReadFrameIndex = (m_ReadFrameIndex + 1) & 1;
			}
		}
		else
		{
			const u32 numSamplesInFirstBuffer = m_DecodedSamplesAvailable[m_ReadFrameIndex];
			const u32 numSamplesInSecondBuffer = numSamples - numSamplesInFirstBuffer;

			m_DecodedSamplesAvailable[m_ReadFrameIndex] = 0;
			m_ReadFrameIndex = (m_ReadFrameIndex + 1) & 1;
			
			Assert(m_DecodedSamplesAvailable[m_ReadFrameIndex] >= numSamplesInSecondBuffer);
			m_DecodedSamplesAvailable[m_ReadFrameIndex] -= numSamplesInSecondBuffer;
		}
		m_SamplesConsumed += numSamples;
	}

	u32 audDecoderSpu::ReadData(s16 *buf, const u32 numSamples) const
	{
		if(numSamples == 0)
		{
			return 0;
		}
		else if(m_DecodedSamplesAvailable[m_ReadFrameIndex] >= numSamples)
		{
			const u32 offsetSamples = m_DecodeBufferSize[m_ReadFrameIndex] - m_DecodedSamplesAvailable[m_ReadFrameIndex];
			sysMemCpy(buf, &m_DecodeBuffer[(audMp3Util::kFrameSizeSamples*m_ReadFrameIndex) + offsetSamples], numSamples*sizeof(s16));

			return numSamples;
		}
		else
		{
			// start with all of the decoded samples in the current read buffer
			const u32 numSamples1 = m_DecodedSamplesAvailable[m_ReadFrameIndex];
			const u32 offsetSamples = m_DecodeBufferSize[m_ReadFrameIndex] - numSamples1;

			// Need at least one sample, else the array index will be invalid
			if(numSamples1 > 0)
			{
				sysMemCpy(buf, &m_DecodeBuffer[(audMp3Util::kFrameSizeSamples*m_ReadFrameIndex) + offsetSamples], numSamples1*sizeof(s16));
			}
			
			// add samples from the next read buffer
			const u32 nextReadIndex = (m_ReadFrameIndex + 1) & 1;
			const u32 numSamples2 = Min(m_DecodedSamplesAvailable[nextReadIndex], numSamples - numSamples1);
			Assert(m_DecodedSamplesAvailable[nextReadIndex] >= numSamples2);

			// Need at least one sample, else the array index will be invalid
			if(numSamples2 > 0)
			{
				sysMemCpy(buf + numSamples1, &m_DecodeBuffer[(audMp3Util::kFrameSizeSamples*nextReadIndex)], numSamples2 * sizeof(s16));
			}

			return numSamples1 + numSamples2;
		}
	}

} // namespace rage

#endif // __PS3
