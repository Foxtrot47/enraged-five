#ifndef AUD_PCMSOURCEJOB_H
#define AUD_PCMSOURCEJOB_H

#if __SPU

#include "framebuffercache.h"
#include "pcmsource.h"
#include "syncsource.h"
#include "audioengine/spuutil.h"

namespace rage
{

u32 g_MixerTimeFrames = 0;
audFrameBufferCache *g_FrameBufferCache = NULL;
bool g_DebugFrameBufferCache = false;

struct syncPcmSourceInfo
{
	u32 slotIndex;
	u32 syncSourceId;
	atRangeArray<u32, audPcmSource::kMaxPcmSourceChannels> bufferIds;
};

template<class _T, u32 TypeId> void ProcessPcmSourcePool(sysTaskParameters &p, const u32 numPcmBuffers)
{
	const u32 frameBufferEA = p.UserData[0].asUInt;
	const u32 numFrameBuffers = p.UserData[1].asUInt;
	const u32 pcmSourceSlotSize = p.UserData[2].asUInt;
	PcmSourceState *pcmSourceStateEa = (PcmSourceState*)p.UserData[3].asPtr;
	bool disableSourceType = false;

#if __BANK
	const u32 disablePCMSourceMask = p.UserData[8].asUInt;
	disableSourceType = (disablePCMSourceMask & (1 << TypeId)) != 0;
#endif

	// skip slot 4; waveplayerjob specific MP3 stuff

	audMixerSyncManager::sm_InstanceEA = (audMixerSyncManager*)p.UserData[5].asPtr;
	g_MixerTimeFrames = p.UserData[6].asUInt;

	// allocate two slots for pcm source state
	const s32 stateSlotDmaTag[2] = {3,4};

	_T *s[2] = {(_T*)AllocateFromScratch(pcmSourceSlotSize, "PCMSource0"),
		(_T*)AllocateFromScratch(pcmSourceSlotSize, "PCMSource1")};
	s32 stateSlotIndex = 0;
	PcmSourceState state[2] ;

	// PCM buffers come after the two state slots
	
	f32 *pcmBuffer = (f32*)AllocateFromScratch(sizeof(float)*kMixBufNumSamples*numPcmBuffers, "PCMBuffers");

	audFrameBufferCache frameBufferCache;
	frameBufferCache.Init(frameBufferEA, numFrameBuffers, pcmBuffer, numPcmBuffers, 3, false);
	g_FrameBufferCache = &frameBufferCache;

	audPcmSourcePool *pool = (audPcmSourcePool*)p.Input.Data;

	atFixedArray<syncPcmSourceInfo, 64> sourcesWaitingOnSync;

	u32 i = 0;
	audPcmSourcePool::Iterator iter(pool, 1<<TypeId);
	while((i = iter.Next()) < pool->GetNumSlots())
	{
		// wait for this slot to be free
		sysDmaWaitTagStatusAll(1<<stateSlotDmaTag[stateSlotIndex]);
		// grab the slot data
		sysDmaGet(s[stateSlotIndex],  reinterpret_cast<u32>(pool->GetSlotPtr(i)), pcmSourceSlotSize, stateSlotDmaTag[stateSlotIndex]);
		// wait for the grab to complete
		sysDmaWaitTagStatusAll(1<<stateSlotDmaTag[stateSlotIndex]);

		// Pull current PCM generator state so that we can update it
		sysDmaGet(&state[stateSlotIndex], (uint64_t)(pcmSourceStateEa + i), sizeof(PcmSourceState), stateSlotDmaTag[stateSlotIndex]);

		Assert(s[stateSlotIndex]->GetTypeId() == (s32)pool->GetSlotTypeId(i));

		_T *pcmSource = s[stateSlotIndex];
		if(pcmSource->IsWaitingOnSync())
		{
			syncPcmSourceInfo &info = sourcesWaitingOnSync.Append();
			info.slotIndex = i;

			info.syncSourceId = pcmSource->GetSyncSlaveId();
			for(s32 channelId = 0; channelId < audPcmSource::kMaxPcmSourceChannels; channelId++)
			{
				if(pcmSource->HasBuffer(channelId))
				{
					info.bufferIds[channelId] = pcmSource->GetBufferId(channelId);
				}
				else
				{
					info.bufferIds[channelId] = ~0U;
				}
			}

			// If this pcm source IsWaitingOnSelfSync then we need to process it twice; first as a normal
			// pcm source so that it can trigger when ready, and secondly as a sync'd source so that it can
			// respond to the trigger if all synched voices are ready to go.
			if(!pcmSource->IsWaitingOnSelfSync())
			{
				stateSlotIndex = (stateSlotIndex+1)%2;
				continue;
			}
		}
		if(!pcmSource->IsWaitingOnSync() || pcmSource->IsWaitingOnSelfSync())
		{
			if(pcmSource->HasBuffer(0))
			{	
				if(disableSourceType)
				{
					for(s32 channelId = 0; channelId < audPcmSource::kMaxPcmSourceChannels; channelId++)
					{
						if(pcmSource->HasBuffer(channelId))
						{
							g_FrameBufferCache->ZeroBuffer(pcmSource->GetBufferId(channelId));
							frameBufferCache.WriteAndFreeBuffer(pcmSource->GetBufferId(channelId), stateSlotDmaTag[stateSlotIndex]);
						}
					}
				}
				else
				{
					pcmSource->GenerateFrame();

					// DMA results back
					for(s32 channelId = 0; channelId < audPcmSource::kMaxPcmSourceChannels; channelId++)
					{
						if(pcmSource->HasBuffer(channelId))
						{
							frameBufferCache.WriteAndFreeBuffer(pcmSource->GetBufferId(channelId), stateSlotDmaTag[stateSlotIndex]);
						}
					}
				}
			}
			else
			{
				pcmSource->SkipFrame();
			}
		}						

		// Wait for state fetch
		sysDmaWaitTagStatusAll(1<<stateSlotDmaTag[stateSlotIndex]);

		// DMA PCM source state back to main memory
		sysDmaPut(s[stateSlotIndex], reinterpret_cast<u32>(pool->GetSlotPtr(i)), pcmSourceSlotSize, stateSlotDmaTag[stateSlotIndex]);

		//audPcmSource::UpdateState():
		if(s[stateSlotIndex]->IsFinished())
		{
			state[stateSlotIndex].PlaytimeSamples = ~0U;
		}
		else
		{
			state[stateSlotIndex].PlaytimeSamples = s[stateSlotIndex]->GetPlayPositionSamples();
		}

		state[stateSlotIndex].hasStartedPlayback = s[stateSlotIndex]->HasStartedPlayback();
		Assign(state[stateSlotIndex].CurrentPeakLevel, s[stateSlotIndex]->GetCurrentPeakLevel());

		// Update PcmSourceState in main ram
		sysDmaPut(&state[stateSlotIndex], (uint64_t)(pcmSourceStateEa + i), sizeof(PcmSourceState), stateSlotDmaTag[stateSlotIndex]);
		stateSlotIndex = (stateSlotIndex+1)%2;
				
	}

	for(s32 i = 0; i < sourcesWaitingOnSync.GetCount(); i++)
	{
		audMixerSyncSignal signal;
		const u32 sourceSlotIndex = sourcesWaitingOnSync[i].slotIndex;

		audMixerSyncManager::Get()->ReadSignal(signal, sourcesWaitingOnSync[i].syncSourceId);
		if(signal.HasTriggered() || signal.WasCanceled())
		{
			// wait for this slot to be free
			sysDmaWaitTagStatusAll(1<<stateSlotDmaTag[stateSlotIndex]);
			// grab the slot data
			sysDmaGet(s[stateSlotIndex],  reinterpret_cast<u32>(pool->GetSlotPtr(sourceSlotIndex)), pcmSourceSlotSize, stateSlotDmaTag[stateSlotIndex]);
			// wait for the grab to complete
			sysDmaWaitTagStatusAll(1<<stateSlotDmaTag[stateSlotIndex]);

			// Pull current PCM generator state so that we can update it
			sysDmaGet(&state[stateSlotIndex], (uint64_t)(pcmSourceStateEa + sourceSlotIndex), sizeof(PcmSourceState), stateSlotDmaTag[stateSlotIndex]);

			Assert(s[stateSlotIndex]->GetTypeId() == (s32)pool->GetSlotTypeId(sourceSlotIndex));

			Assert(s[stateSlotIndex]->GetTypeId() == TypeId);

			_T *pcmSource = s[stateSlotIndex];
			pcmSource->ProcessSyncSignal(signal);
			if(pcmSource->HasBuffer(0))
			{	
				if(disableSourceType)
				{
					for(s32 channelId = 0; channelId < audPcmSource::kMaxPcmSourceChannels; channelId++)
					{
						if(pcmSource->HasBuffer(channelId))
						{
							g_FrameBufferCache->ZeroBuffer(pcmSource->GetBufferId(channelId));
							frameBufferCache.WriteAndFreeBuffer(pcmSource->GetBufferId(channelId), stateSlotDmaTag[stateSlotIndex]);
						}
					}
				}
				else
				{
					pcmSource->GenerateFrame();
					// DMA results back
					for(s32 channelId = 0; channelId < audPcmSource::kMaxPcmSourceChannels; channelId++)
					{
						if(pcmSource->HasBuffer(channelId))
						{
							frameBufferCache.WriteAndFreeBuffer(pcmSource->GetBufferId(channelId), stateSlotDmaTag[stateSlotIndex]);
						}
					}
				}
			}
			else
			{
				pcmSource->SkipFrame();
			}						

			// Wait for state fetch
			sysDmaWaitTagStatusAll(1<<stateSlotDmaTag[stateSlotIndex]);

			// DMA PCM generator state back to main memory
			sysDmaPut(s[stateSlotIndex], reinterpret_cast<u32>(pool->GetSlotPtr(sourceSlotIndex)), pcmSourceSlotSize, stateSlotDmaTag[stateSlotIndex]);

			if(s[stateSlotIndex]->IsFinished())
			{
				state[stateSlotIndex].PlaytimeSamples = ~0U;
			}
			else
			{
				state[stateSlotIndex].PlaytimeSamples = s[stateSlotIndex]->GetPlayPositionSamples();
			}

			state[stateSlotIndex].hasStartedPlayback = s[stateSlotIndex]->HasStartedPlayback();
			Assign(state[stateSlotIndex].CurrentPeakLevel, s[stateSlotIndex]->GetCurrentPeakLevel());

			sysDmaPut(&state[stateSlotIndex], (uint64_t)(pcmSourceStateEa + sourceSlotIndex), sizeof(PcmSourceState), stateSlotDmaTag[stateSlotIndex]);
		}
		else
		{
			// Source is in a waiting state and trigger has not been fired; silence output buffer

			// Invalidate the contents of this buffer, which prevents the cache from having to fetch it
			for(s32 channelId = 0; channelId < audPcmSource::kMaxPcmSourceChannels; channelId++)
			{
				const u32 bufferId = sourcesWaitingOnSync[i].bufferIds[channelId];
				if(bufferId != ~0U)
				{
					frameBufferCache.ZeroBuffer(bufferId);
					// DMA results back
					frameBufferCache.WriteAndFreeBuffer(bufferId, stateSlotDmaTag[stateSlotIndex]);
				}
			}
		}
		stateSlotIndex = (stateSlotIndex+1)%2;
	}
}

} // namespace rage

#endif // __SPU
#endif // AUD_PCMSOURCEJOB_H
