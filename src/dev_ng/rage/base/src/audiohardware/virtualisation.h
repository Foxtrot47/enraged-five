//
// audiohardware/virtualisation.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_VIRTUALISATION_H
#define AUD_VIRTUALISATION_H

#define		FAST_VIRTUALISATION		0		// does prefetch only

#include "voicesettings.h"

namespace rage
{
	struct PcmSourceState;
	struct audVirtualisationJobOutput;
	struct audVoiceSettings;
	struct VoiceGroup;
	struct PrioritisedVoice;
	struct audSoundPoolBucket;

	typedef atRangeArray<VoiceGroup,g_MaxVirtualisationGroups>	PrioritisedGroupList;
	typedef atMultiRangeArray<VoiceSortBin,2,256>				VoiceBinsList;

	// PURPOSE
	//	Constructs sorted prioritised voice list
	// NOTES
	//	The algorithm is as follows:
	//	Iterate through voice pool, calculating a virtualisation score based on volume
	//	and add each non-grouped voice to a linked list.  Voices that are grouped are 
	//	stored in their own group specific list, which tracks the highest score and number
	//	of entries in the group.  All grouped voices are then added to the voice linked
	//	list with the highest score for that group.  The list is then sorted using a radix 
	//	sort.
	void BuildPrioritisedVoiceList(PrioritisedVoice *&voiceList, VoiceGroup *&groupList, audVirtualisationJobOutput *header, PrioritisedGroupList& prioritisedGroupList, VoiceBinsList& voiceBinsList);
	// PURPOSE
	//	Traverses the sorted voice/group list and dishes out physical voices appropriately
	void AllocatePhysicalVoices(const u32 budget, VoiceGroup *highestPriorityGroup, PrioritisedVoice *highestPriorityVoice, u32 actualPhysicalVoicesFree, u32 physicalVoicesThisFrame, audVirtualisationJobOutput *header);

	// PURPOSE
	//	Add a voice to the StartPlaying list
	__inline void StartPlayingVoicePhysically(const u32 voiceId, audVirtualisationJobOutput *output)
	{
		output->startVoiceList[output->voicesStartedThisFrame++] = static_cast<u16>(voiceId);
	}
	// PURPOSE
	//	Add a voice to the StopPlaying list
	__inline void StopPlayingVoicePhysically(const u32 voiceId, audVirtualisationJobOutput *output)
	{
		output->stopVoiceList[output->voicesStoppedThisFrame++] = static_cast<u16>(voiceId);
	}

}

#endif // AUD_VIRTUALISATION_H
