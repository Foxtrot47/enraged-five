//
// audiohardware/wavedefs.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_DEFS_H
#define WAVE_DEFS_H

namespace rage {

//Disable struct alignment.
#if !__SPU
#pragma pack(push, r1, 1)
#endif // !__SPU

#define WAVEFLAGS_PCM				0x00000001
#define WAVEFLAGS_PSN_16BIG			0x00000002
#define WAVEFLAGS_PSN_16SMALL		0x00000004
#define WAVEFLAGS_PSN_32FLOAT		0x00000008
#define WAVEFLAGS_PSN_ADPCM			0x00000010
#define WAVEFLAGS_PSN_ATRAC_LOW		0x00000020
#define WAVEFLAGS_PSN_ATRAC_MED		0x00000040
#define WAVEFLAGS_PSN_ATRAC_HIGH	0x00000080
#define WAVEFLAGS_PSN_MP3			0x00000100
#define WAVEFLAGS_VORBIS			0x00000200
#define WAVEFLAGS_ADPCM				0x00000400

#define COMPRESSIONTAG_VORBIS		1
#define COMPRESSIONTAG_ADPCM		2

	const u32 g_StreamingPacketBytes = 2048;
	const u32 g_StreamingPacketBytesOgg = 2048*3;		// the ogg pages should all fix in 6k

//
// PURPOSE
//  Defines the base set of metadata stored for each constituent Wave in a Wave Bank.
//
#if 0
typedef struct
{
	union
	{
		u64 waveDataOffsetBytes;		//w.r.t start of Wave data.
		void *waveData;
	};
	u32 nameHash;						//Genuine 32-bit hash.
	u32 lengthBytes;
	u32 lengthSamples;
	s32 loopStartOffsetSamples;
	u16 sampleRate;						//in Hz.
	s16 headroom;						//in mB.
	u32 flags;

} audWaveMetadataBase;

//
// PURPOSE
//  Defines the a empty set of dummy platform-specific Wave metadata.
//
typedef struct
{
} audWaveMetadataPlatformDummy;

//
// PURPOSE
//  Defines the set of platform-specific metadata stored for each constituent Wave in a PC Wave Bank.
// NOTES
//	The PC seek table is an array of stuff 
//
typedef struct 
{
	u32 byteOffset;
	u32 startSampleOffset;
	u32 endSampleOffset;
	u32 packetSize;

} audSeekTableVorbis;

typedef struct 
{
	s16 predictedValue;
	u8	stepIndex;

} audSeekTableADPCM;


typedef struct
{
	union
	{
		u64 seekTableOffsetBytes;		//w.r.t start of Wave's metadata.
		void *seekTable;
	};
	u32 samplesToSkip;
	u32 dummy0;
	u32 dummy1;
	u32 seekTableLengthSamples;

} audWaveMetadataPlatformPC;

//
// PURPOSE
//  Defines the set of platform-specific metadata stored for each constituent Wave in a Xenon Wave Bank.
// NOTES
//	The Xenon seek table is an array of u32s that define the sample index at the start of each frame of encoded XMA data.
//
typedef struct
{
	union
	{
		u64 seekTableOffsetBytes;		//w.r.t start of Wave's metadata.
		void *seekTable;
	};
	u32 loopStartBits;
	u32 loopEndBits;
	u32 loopSubframeData;
	u32 seekTableLengthSamples;

} audWaveMetadataPlatformXenon;

//
// PURPOSE
//  Defines the set of platform-specific metadata stored for each constituent Wave in a PS3 Wave Bank.
// NOTES
//	The PS3 seek table is an array of u16s that define the number of bytes in each frame of encoded MP3 data.
//

typedef struct
{
	union
	{
		u64 seekTableOffsetBytes;		//w.r.t start of Wave's metadata.
		void *seekTable;
	};
	u32 samplesPerFrame;
	u32 numFrames;
	u32 streamingBlockStartFrames;
	u32 pad;

} audWaveMetadataPlatformPsn;

//
// PURPOSE
//  Maps the platform-specific Wave metadata structure to the Xenon-specific implementation where Xenon XMA-encoded
//	sample data is being used or the PS3-specific implementation where MP3-encoded sample data is used.
//	Otherwise the empty dummy structure is used.
//
#if __XENON
	typedef audWaveMetadataPlatformXenon audWaveMetadataPlatform;
#elif __PPU
 	typedef audWaveMetadataPlatformPsn audWaveMetadataPlatform;
#elif RSG_PC || RSG_ORBIS || RSG_DURANGO
	typedef audWaveMetadataPlatformPC audWaveMetadataPlatform;
#else
	typedef audWaveMetadataPlatformDummy audWaveMetadataPlatform;
#endif

//
// PURPOSE
//  Defines the full set of cross-platform metadata stored for each constituent Wave in a Wave Bank.
//
struct audWaveMetadata
{
	audWaveMetadataBase base;
	audWaveMetadataPlatform platform;

};

//
// PURPOSE
//  Defines the full set of Xenon-specific metadata stored for each constituent Wave in a Wave Bank.
//
typedef struct
{
	audWaveMetadataBase base;
	audWaveMetadataPlatformPC platform;

} audWaveMetadataPC;

//
// PURPOSE
//  Defines the full set of Xenon-specific metadata stored for each constituent Wave in a Wave Bank.
//
typedef struct
{
	audWaveMetadataBase base;
	audWaveMetadataPlatformXenon platform;

} audWaveMetadataXenon;

//
// PURPOSE
//  Defines the full set of PS3-specific metadata stored for each constituent Wave in a Wave Bank.
//
typedef struct
{
	audWaveMetadataBase base;
	audWaveMetadataPlatformPsn platform;

} audWaveMetadataPsn;

//
// PURPOSE
//  Defines the structure of the Wave and custom metadata lookup tables that are contained within the Bank metadata.
//
typedef struct
{
	union
	{
		u64 metadataOffsetBytes;		//w.r.t start of metadata.
		void *metadata;
	};
	u32 nameHash;						//Genuine 32-bit hash.	
	u32 metadataLengthBytes;

} audMetadataLookupEntry;

//
// PURPOSE
//  Defines the set of metadata stored in the header of a Wave Bank.
//
typedef struct
{
	union
	{
		u64 waveMetadataLookupOffsetBytes;		//w.r.t start of Wave Bank.
		audMetadataLookupEntry *waveMetadataLookup;
	};
	union
	{
		u64 customMetadataLookupOffsetBytes;	//w.r.t start of Wave Bank.
		audMetadataLookupEntry *customMetadataLookup;
	};
	u32 numWaves;
	u32 numCustomMetadataElements;
	u32 waveDataStartOffsetBytes;				//w.r.t start of Wave Bank.

} audWaveBankMetadata;

//
// PURPOSE
//  Defines the set of metadata stored in the header of a streaming Wave Bank.
//
typedef struct
{
	union
	{
		u64 loadBlockSeekTableOffsetBytes;	//w.r.t start of Wave Bank.
		audLoadBlockSeekTableEntry *loadBlockSeekTable;
	};
	u32 numLoadBlocks;
	u32 streamingBlockBytes;
	u32 numDataStreams;
	//u32 *dataStreamsBlockBytes;

	audWaveBankMetadata base;

} audStreamingWaveBankMetadata;


//
// PURPOSE
//  Defines the common set of metadata stored for each constituent Wave in a load block of a streaming Wave Bank.
//
typedef struct
{
	u32 startOffsetPackets;
	u32 numPackets;
	u32 samplesToSkip;
	s32 numSamples;

} audWaveBlockMetadataCommon;

//
// PURPOSE
//  Defines the PS3-specific set of metadata stored for each constituent Wave in a load block of a streaming Wave Bank.
//
typedef struct
{
	u32 startOffsetPackets;
	u32 numPackets;
	u32 samplesToSkip;
	s32 numSamples;
	u32 numFrames;
	u32 numBytes;

} audWaveBlockMetadataPsn;

#if __PPU
	typedef audWaveBlockMetadataPsn audWaveBlockMetadata;
#else
	typedef audWaveBlockMetadataCommon audWaveBlockMetadata;
#endif

//
// PURPOSE
//  Defines the set of metadata stored for each constituent data stream in a load block of a streaming Wave Bank.
//
typedef struct
{
	u32 startOffsetPackets;
	u32 numPackets;

} audDataBlockMetadata;

//
// PURPOSE
//  Defines the structure of the packet seek table in a load block of a streaming Wave Bank.
//
typedef struct
{
	u32 startSampleIndex;
	u32 endSampleIndex;

} audPacketSeekTableEntry;

//
// PURPOSE
//  Defines the set of metadata stored in the header of a load block within a streaming Wave Bank.
//
typedef struct
{
	union
	{
		u64 waveBlockMetadataListOffsetBytes;	//w.r.t start of Block.
		audWaveBlockMetadata *waveBlockMetadataList;
	};
	union
	{
		u64 dataBlockMetadataListOffsetBytes;	//w.r.t start of Block.
		audDataBlockMetadata *dataBlockMetadataList;
	};
	union
	{
		u64 packetSeekTableOffsetBytes;			//w.r.t start of Block.
		audPacketSeekTableEntry *packetSeekTable;
	};

} audStreamingBlockMetadata;

//
// PURPOSE
//  Defines the structure of the header stored before blocks of custom metadata within a Wave.
//
typedef struct
{
	u32 typeHash;		//Genuine 32-bit hash.
	u32 size;			//The size of metadata that follows, in bytes.

} audCustomMetadataHeader;
#endif

//Restore struct alignment.
#if !__SPU
#pragma pack(pop, r1)
#endif // !__SPU

} // namespace rage

#endif // WAVE_DEFS_H
