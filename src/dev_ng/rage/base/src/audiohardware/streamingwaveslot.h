//
// audiohardware/streamingwaveslot.h
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_STREAMING_WAVE_SLOT_H
#define AUD_STREAMING_WAVE_SLOT_H

#include "waveslot.h"
#include "waveref.h"

#define AUD_DEBUG_STREAMING 0

#if AUD_DEBUG_STREAMING
#define DEBUG_STREAMING_ONLY(x) x
#else
#define DEBUG_STREAMING_ONLY(x)
#endif

namespace rage {

	struct audStreamFormat;
	struct audLoadBlockSeekTableEntry;

	struct audStreamingWaveBlock
	{
		audLoadBlockMetadata metadata;
		const audSeekTableEntry *packetSeekTable;
		const u8 *waveDataPtr;
		u32 blockId;
		bool IsFinalBlock;
	};

	class audStreamingDebugInterface
	{
public:
	virtual void SetMarker(const char *msg) const = 0;
	virtual ~audStreamingDebugInterface() {}
	};

//
// PURPOSE
//  Encapsulates a cross-platform audio slot that can load and store blocks from a streaming Wave Bank. Critical sections
//	are used to remove the thread synchronization problems that might otherwise occur with asynchronous loading.
// SEE ALSO
//	sysIpcCriticalSection
//
class audStreamingWaveSlot : public audWaveSlot
{
	friend class audWaveSlot;

public:

	enum { kNumStreamingBlocksPerWaveSlot = 2 };

	//
	// PURPOSE
	//	Define Streaming Bank load status values.
	//
	enum audWaveSlotStreamingBlockLoadStatus
	{
		LOADED_BLOCK,
		LOADING_BLOCK,
		FAILED_TO_LOAD_BLOCK,
		NO_DATA_FOR_BLOCK
	};

	//
	// PURPOSE
	//	Queues a request for g_NumStreamingBlocksPerWaveSlot blocks of a streaming Wave Bank to be loaded. This will
	//	override an existing load request that has not yet been actioned. The Bank header is loaded only when necessary
	//	(as it may already be resident from a previous loading of this Bank.)
	// PARAMS
	//	bankId		- The id of the streaming Wave Bank to be loaded from
	//	startOffsetMs	- The offset into the streaming Wave Bank to be loaded, in milliseconds.
	//	shouldLoop		- if true the stream will continue playing from the start when it reaches the end
	//
	void PreloadStreamingBankBlocks(const u32 bankId, const u32 startOffsetMs, const bool shouldLoop);
	//
	// PURPOSE
	//	Queues a request for a block of a streaming Wave Bank to be loaded. This will override an existing load request
	//	that has not yet been actioned.
	// PARAMS
	//	bankId	- The id of the streaming Wave Bank to be loaded from 
	//	blockIndex	- The index of the block to be loaded from the current streaming Wave Bank.
	//
	void LoadStreamingBankBlock(const u32 bankId, const u32 blockIndex);

	//
	// PURPOSE
	//	Initiates the loading of any queued request, via the RAGE pgStreamer class, and checks the status of any
	//outstanding asynchronous load that has previously been initiated.
	// SEE ALSO
	//	pgStreamer
	//
	void Update(void);
	//
	// PURPOSE
	//	Gets the loading status of the block (or blocks) associated with a specified time offset, in milliseconds, for a
	//	specified streaming Wave Bank.
	// PARAMS
	//	bankId		- The id of the streaming Wave Bank for which the loading status is required 
	//	startOffsetMs	- The required time offset into the streaming Wave Bank that is to be queried.
	// RETURNS
	//	The loading status of the Bank.
	//
	audWaveSlotLoadStatus GetStreamingBankLoadingStatusForTimeMs(const u32 bankId, const u32 startOffsetMs) const;
	//
	// PURPOSE
	//	Gets the loading status of the block (or blocks) for a specified streaming Wave Bank.
	// PARAMS
	//	bankId		- The id of the streaming Wave Bank for which the loading status is required
	//	blockIndex		- The load block with the streaming Wave Bank that is to be queried.
	// RETURNS
	//	The loading status of the Bank.
	//
	audWaveSlotLoadStatus GetStreamingBankLoadingStatusForBlockIndex(const u32 bankId, const u32 blockIndex) const;
	
	//
	// PURPOSE
	//	Calculates the length of the streaming Wave Bank currently loaded in this slot. This is equivalent to the length of
	//	the longest of the constituent Waves.
	// RETURNS
	//	The length of the currently loaded streaming Wave Bank, in milliseconds, or -1 if no streaming Wave Bank is loaded,
	//	or no constituent Waves could be found.
	//
	s32 ComputeStreamingWaveBankLengthMs(void) const;
	//
	// PURPOSE
	//	Attempts to obtain a block of interleaved Wave data from the a streaming Wave Bank. If no blocks are currently
	//	ready, a new load request is made.
	// PARAMS
	//	bankId		- The id of the streaming Wave Bank to be loaded from
	// RETURNS
	//	The current status of the request.
	//
	audWaveSlotStreamingBlockLoadStatus RequestStreamingLoadBlock(const u32 bankId);
	//
	// PURPOSE
	//	Calculates the start time, in milliseconds, of the load block that covers a specified time offset for the
	//	currently loaded streaming Wave Bank.
	// PARAMS
	//	timeOffsetMs	- The time offset, in milliseconds, to be converted to a load block index for which the start
	//						time will be determined.
	// RETURNS
	//	The start time, in milliseconds, of the load block that covers the specified time offset, or -1 if the time
	//	offset is beyond the end of the Bank's Wave data.
	//
	s32 ComputeStreamingBankBlockStartTimeFromTimeOffset(u32 timeOffsetMs) const;

#if !__SPU
	void _SyncStreamingWaveSlotRequest(const u32 startOffsetMs, const u32 bankId, const bool isPreload, const bool isLooping);
#endif


	const audStreamFormat *GetStreamHeader() const { return m_StreamHeader; }
	u32 GetWaveNameHash(const u32 waveIndex) const;

#if !__SPU
	
	const audWaveFormat *FindFormat(const adatObjectId waveId) const;

	void GetCurrentBlockInfo(const s32 clientId, audStreamingWaveBlock &blockInfo, u32 format) const;
	void GetFirstBlockInfo(const s32 clientId, audStreamingWaveBlock &blockInfo, u32 format) const;
	void GetSecondBlockInfo(const s32 clientId, audStreamingWaveBlock &blockInfo, u32 format) const;

	s32 RegisterClient(const adatObjectId waveId);
	void UnregisterClient(const s32 clientId);

	// PURPOSE
	//	Notifies the slot that the specified client requires new data
	// RETURNS
	//	true when data is available, false otherwise
	// NOTES
	//	This function will only return true once; when true is returned the data is considered
	//	consumed by the client.
	bool RequestDataForClient(const s32 clientId);

	bool ResetLoadedState();
#endif

	// PURPOSE
	//	Returns true if the playing stream is looping
	bool IsLooping() const { return m_IsLooping; }

	void Init();

#if !__FINAL && !__SPU
	static void SetDebugInterface(const audStreamingDebugInterface *debugInterface) { sm_DebugInterface = debugInterface; }
#endif

private:
	
	void GetSpecificBlockInfo(const u32 blockId, const u32 waveIndex, audStreamingWaveBlock &blockInfo, u32 format) const;

	//
	// PURPOSE
	//  Initializes member variables and allocates the specified amount of system memory for streaming Wave Bank storage.
	// PARAMS
	//	bankMem			- Pointer to memory allocated for this slot
	//	numBytes		- The amount of system memory allocated for Wave / Bank storage, in bytes.
	//	maxHeaderSize	- The maximum header size for any bank that is to be loaded into this slot, in bytes
	//	slotName		- String name for this slot
	// NOTES
	//	Protected since all wave slot creation should go through the static audWaveSlot in order
	//	to ensure consistency between the slotName and the global slot lookup table
	//
	audStreamingWaveSlot(u8 *bankMem, const u32 numBytes, const u32 maxHeaderSize, const char *slotName, const bool isVirtual);

	~audStreamingWaveSlot();
	//
	// PURPOSE
	//	Prepares and requests that a streaming Wave Bank header or streaming block be loaded via pgStreamer.
	//
	void RequestLoad(void);
	//
	// PURPOSE
	//	Actions any follow on load requests and fixes-up the data that has been loaded.
	//
	void HandleLoadCompletion(void);
	//
	// PURPOSE
	//	Resets all loading-related member variables to safe defaults following a load failure.
	//
	void CleanUpAfterLoadFailure(void);

	//
	// PURPOSE
	//	Calculates the index of the load block that covers a specified time offset for the currently loaded streaming
	//	Wave Bank.
	// PARAMS
	//	timeOffsetMs	- The time offset, in milliseconds, to be converted to a load block index.
	// RETURNS
	//	The index of the load block that covers the specified time offset, or -1 if the time offset is beyond the end of
	//	the Bank's Wave data.
	//
	s32 ComputeStreamingBankBlockIndexFromTimeOffset(u32 timeOffsetMs) const;
	//
	// PURPOSE
	//	Calculates the start time, in milliseconds, of the specified load block for the currently loaded streaming Wave
	//	Bank.
	// PARAMS
	//	blockIndex	- The index of the load block for which the start time is required.
	// RETURNS
	//	The start time of the specified load block, in milliseconds, or -1 if the block index is beyond the end of
	//	the Bank's Wave data.
	//
	s32 ComputeStreamingBankBlockStartTimeMs(u32 blockIndex) const;
	//
	// PURPOSE
	//	Determines whether the loaded, or currently loading, streaming Wave Bank block(s) covers the specified time offset.
	// PARAMS
	//	timeOffsetMs	- The time offset, in milliseconds, to be queried against the loaded, or currently loading,
	//						streaming block(s).
	// RETURNS
	//	Returns true if the loaded, or currently loading, streaming Wave Bank block(s) covers the specified time offset.
	//
	bool DoesPendingStreamingBlockIncludeTimeMs(s32 timeOffsetMs) const;

	// PURPOSE
	//	Called once the bank header has been loaded; issues the request for the first blocks
	void HandleHeaderLoadCompletion();

	bool AreAnyDecryptionTasksPending() { return m_DecryptionTaskHandles.GetCount() > 0; }

	void SetupBlockPointers(const u32 streamingBlockBytes);

	virtual bool ForceUnloadBank() override;

	const audStreamFormat *m_StreamHeader;
	const audSeekTableEntry *m_LoadBlockSeekTable;
	
	struct audStreamClientState
	{
		enum States
		{
			kUnallocated = 0,
			kAllocated,
			kNeedsData,
			kDataAvailable,
			kOutOfData
		};

		audStreamClientState()
			: state(kUnallocated)
			, waveIndex(0)
		{
			DEBUG_STREAMING_ONLY(timeDataRequested = 0);
		}
		DEBUG_STREAMING_ONLY(u32 timeDataRequested);
		u8 state;
		u8 waveIndex;
		
	};

	enum {kMaxWavesPerStream = 16};
	enum {kMaxStreamClients = 32};
	struct audStreamingWaveSlotState
	{
		audStreamingWaveSlotState()
			: NumClients(0)
			, DataChunkContainerOffsetBytes(0)
			, StreamingBlockBytes(0)
			, LastLoadBytes(0)
			, ReadRequestTime(0.0f)
			, TimeTakenToFulfilRequest(0.0f)
		{
			
		}
		atRangeArray<audWaveFormat, kMaxWavesPerStream> WaveFormatList;
		atRangeArray<audStreamClientState, kMaxStreamClients> ClientState;

		atRangeArray<const u8*, kNumStreamingBlocksPerWaveSlot> BlockData;
		
		u32 NumClients;
		u32 DataChunkContainerOffsetBytes;
		u32 StreamingBlockBytes;

		u32 LastLoadBytes;
		float ReadRequestTime;
		float TimeTakenToFulfilRequest;
		
	};

#if !__SPU
	audStreamingWaveSlotState *GetState()
	{
		return m_State;
	}

	const audStreamingWaveSlotState *GetState() const
	{
		return m_State;
	}
#endif
	audStreamingWaveSlotState *m_State;
	atRangeArray<s32, kNumStreamingBlocksPerWaveSlot> m_StreamingBlockIndexSlots;

	atFixedArray<sysTaskHandle, 2> m_DecryptionTaskHandles;

	s32 m_StartOffsetMs;
	s32 m_BlockIndexToLoad;
	s32 m_StartBlockIndex;
	
	u8 m_NumLoadBlocksToRead;
	u8 m_NumLoadBlocksReady;
	u8 m_NextStreamingBlockLoadSlotIndex;
	u8 m_NextStreamingBlockPrepareSlotIndex;

	bool m_UseCriticalQueue : 1;
	bool m_IsLooping : 1;

	bool m_FirstBlockRequest : 1;

#if !__FINAL && !__SPU
	static void SetDebugMarker(const char *fmt, ...);
	static const audStreamingDebugInterface *sm_DebugInterface;
#endif
};

}	// namespace rage

#endif // AUD_STREAMING_WAVE_SLOT_H
