// 
// audiohardware/granularmix.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "driver.h"
#include "granularmix.h"
#include "grainplayer.h"
#include "vector/colors.h"
#include "audiohardware/mixing_vmath.inl"
#include "audioengine/curve.h"

#include "profile/page.h"
#include "profile/group.h"
#include "profile/element.h"

#include "file/stream.h"
#include "file/asset.h"

#if __BANK
#include "grcore/im.h"
#include "grcore/viewport.h"
#endif

#define Align16(x) (((x)+15)&~15)

namespace rage 
{
#if __SPU
u32 g_ScratchSubmixTag = 25;
u8* g_ScratchSubmixes[3];
#endif

f32 audGranularMix::s_GranularLoopBelowBias = 1.0f;
f32 audGranularMix::s_GranularLoopToGrainChangeRate = 0.01f;
f32 audGranularMix::s_GranularGrainToLoopChangeRate = 0.03f;
f32 audGranularMix::s_DefaultGranularChangeRateForLoops = 2.5f;
u32 audGranularMix::s_LoopGrainCrossfadeStyle = (u32)audGranularSubmix::CrossfadeStyleEqualPower;
u32 audGranularMix::s_GranularMixCrossfadeStyle = (u32)audGranularSubmix::CrossfadeStyleEqualPower;
u32 audGranularMix::s_LoopLoopCrossfadeStyle = (u32)audGranularSubmix::CrossfadeStyleEqualPower;
u32 audGranularMix::s_InterGrainCrossfadeStyle = (u32)audGranularSubmix::CrossfadeStyleLinear;
bool audGranularMix::s_GranularPeakNormaliseLoops = false;
f32 audGranularMix::s_LoopRandomisationGranularChangeRate = 2.0f;

#if __BANK && __PS3
bool audGranularMix::s_DebugDrawVramGrainLoaders = false;
bool audGranularMix::s_DebugDrawVramLoopLoaders = false;
#endif

#if __STATS
extern rage::pfGroup PFGROUP_AudioGrainPlayerTimings;
#endif

PF_TIMER(GenerateFrameGranularMix, AudioGrainPlayerTimings);
PF_TIMER(CalculateNearestLoops, AudioGrainPlayerTimings);

#if !__SPU
// ----------------------------------------------------------------
// audGranularMix constructor
// ----------------------------------------------------------------
audGranularMix::audGranularMix()
{
	m_WaveSlotIndex = -1;
	m_GrainTable = NULL;
	m_NumGrains = 0;
	m_Headroom = 0.0f;
	m_LengthSamples = 0;
	m_LengthBytes = 0;
	m_GranularClockIndex = 0;
	m_Data = NULL;
	m_ParentMinPitch = 0.0f;
	m_ParentMaxPitch = 100.0f;
	m_MaxLoopProportion = 1.0f;
	m_PitchShift = 0.0f;
	m_PitchStretch = 1.0f;
	m_Initialised = false;
	m_Muted = false;
	m_IsPlayingPhysically = false;
	m_PitchLocked = false;
	m_LoopRandomisationEnabled = false;
	m_PureRandomPlayback = false;
	m_AllowLoopGrainOverlap = true;
	m_LoopRandomisationChangeRate = 0.05f;
	m_LoopRandomisationMaxPitchFraction = 0.15f;
	m_GrainPlaybackStyle = PlaybackStyleLoopsAndGrains;
	m_Quality = GrainPlayerQualityHigh;
	m_NearestLoopAbove = -1;
	m_NearestLoopBelow = -1;
	m_NearestLoopAboveVolumeScale = 1.0f;
	m_NearestLoopBelowVolumeScale = 1.0f;
	m_LoopGrainVolumeSmoother.Init(s_GranularGrainToLoopChangeRate, s_GranularLoopToGrainChangeRate, 0.0f, 1.0f);
	m_CurrentVolumeScale = 1.0f;
	m_MasterVolumeScale = 1.0f;
	m_TargetVolumeScale = 1.0f;
	m_VolumeSmoother.Init(1.0f, 1.0f, 0.0f, 1.0f);
	m_LoopHzSmoother.Init(m_LoopRandomisationChangeRate, m_LoopRandomisationChangeRate, -100.0f, 100.0f);
	m_LoopHzRandomisationApplySmoother.Init(0.01f, 0.01f, 0.0f, 1.0f);
	m_LoopGrainCalcSmoother.Init(1.0f, 1.0f);
	m_LoopHzSmootherTarget = 0.0f;
	m_LoopHzSmootherCurrent = 0.0f;
	m_GranuleStepPerGranule = 0.0f;
	m_MinLoopHz = 0.0f;
	m_MaxLoopHz = 0.0f;
	m_MixNative = false;

#if __PS3
	ResetVramLoaders();
#endif

	BANK_ONLY(m_LoopEditorActive = false;)
	BANK_ONLY(m_LoopEditorLoopIndex = 0;)
}

// ----------------------------------------------------------------
// audGranularMix destructor
// ----------------------------------------------------------------
audGranularMix::~audGranularMix()
{
	for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		delete m_GranularSubmixes[loop];
	}
}

// ----------------------------------------------------------------
// audGranularMix init
// ----------------------------------------------------------------
bool audGranularMix::Init(const s32 waveSlotIndex, const u32 waveNameHash, const u8 outputChannel, const u8 pitchClockIndex, const bool matchMinPitch, const bool matchMaxPitch, const f32 maxLoopProportion)
{
	if(!m_WaveRef.Init(waveSlotIndex, waveNameHash))
	{
		return false;
	}	
	if(!m_WaveRef.FindFormat())
	{
		return false;
	}

	u32 grainChunkLengthBytes = 0;
	const void* grainChunk = m_WaveRef.FindChunk(ATSTRINGHASH("GRANULARGRAINS", 0xE787895A), grainChunkLengthBytes);

	if(!grainChunk)
	{
		audWaveSlot* waveSlot = audWaveSlot::GetWaveSlotFromIndex(waveSlotIndex);
		
		if(waveSlot)
		{
			Assertf(false, "No grain data found for granular sound - bank %s", audWaveSlot::GetBankName(waveSlot->GetLoadedBankId()));
		}
		else
		{
			Assertf(false, "No grain data found for granular sound - invalid wave slot");
		}

		return false;
	}

	m_OutputChannel = outputChannel;
	m_GranularClockIndex = pitchClockIndex;
	m_MatchMinPitch = matchMinPitch;
	m_MatchMaxPitch = matchMaxPitch;
	m_MaxLoopProportion = maxLoopProportion;
	m_GranularChangeRateForLoops = s_DefaultGranularChangeRateForLoops;
	m_GrainTable = (audGranularSubmix::audGrainData*) grainChunk;
	m_NumGrains = (grainChunkLengthBytes - sizeof(f32)) / sizeof(m_GrainTable[0]);
	m_SampleRateConversionRatio = *((f32*)(m_GrainTable + m_NumGrains));
	Assertf(m_NumGrains > 1, "No grains found for granular sound");

	m_LengthSamples = m_WaveRef.FindFormat()->LengthSamples;

#if __PS3
	// Preserve transients flag means we get an additional frame of data
	m_LengthSamples += audMp3Util::kFrameSizeSamples;
	m_HertzToDecodesCurve.Init(ATSTRINGHASH("GRANULAR_HERTZ_TO_DECODES_PS3", 0xE9563087));
#endif

	m_Data = (s16*)m_WaveRef.FindWaveData(m_LengthBytes);

	m_Headroom = m_WaveRef.FindFormat()->Headroom*0.01f;

	audGranularSubmix* defaultGranularSubmix = rage_aligned_new(16) audGranularSubmix(&m_WaveRef, m_GrainTable, m_NumGrains);
	m_GranularSubmixes.Push(defaultGranularSubmix);

	m_Initialised = true;

	InitLoops(); 

	return m_Initialised;
}

// ----------------------------------------------------------------
// Compute the current peak
// ----------------------------------------------------------------
u16 audGranularMix::ComputeCurrentPeak(f32 xValue) const
{
	u16 maxPeak = 0u;
	u32 numSubmixesPlayingPhysically = 0;

	if(m_IsPlayingPhysically)
	{
		for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
		{
			if(m_GranularSubmixes[loop]->IsPlayingPhysically())
			{
				maxPeak = Max(maxPeak, m_GranularSubmixes[loop]->ComputeCurrentPeak());
				numSubmixesPlayingPhysically++;
			}
		}
	}
	
	// If nothing is playing - ie. either the mix isn't playing physically, or the submixes aren't (possibly because they've only
	// just been turned on) - then just calculate a rough sample index ourself and check the peak using that
	if(m_GrainTable && m_NumGrains > 0 && numSubmixesPlayingPhysically == 0 && m_GranularSubmixes[0] != NULL)
	{
		f32 currentRateHz = m_GrainTable[0].pitch + ((m_GrainTable[m_NumGrains - 1].pitch - m_GrainTable[0].pitch) * xValue);
		s32 grainIndex = Clamp(audGranularSubmix::CalculateGrainIndexFromPitch(currentRateHz, m_GrainTable, m_NumGrains, 0.0f, 0.0f), 0, (s32)m_NumGrains - 1);
		maxPeak = m_GranularSubmixes[0]->ComputePeakFromSamplePosition(m_GrainTable[grainIndex].startSample);
	}

	return static_cast<u16>(maxPeak * Max(m_TargetVolumeScale, m_CurrentVolumeScale) * m_MasterVolumeScale);
}

// ----------------------------------------------------------------
// Sorting function for synchronised loop definitions
// ----------------------------------------------------------------
int QSortSyncLoopDefinitions(audGranularMix::audSyncLoopDefinition const* pA, audGranularMix::audSyncLoopDefinition const* pB)
{
	return (int)(pA->averageValidGrainIndex - pB->averageValidGrainIndex);
}

// ----------------------------------------------------------------
// audGranularMix InitLoops
// ----------------------------------------------------------------
void audGranularMix::InitLoops()
{
	u32 loopChunkLengthBytes = 0;
	const void* loopChunk = m_WaveRef.FindChunk(ATSTRINGHASH("GRANULARLOOPS", 0x252C20D9), loopChunkLengthBytes);

	if(loopChunk)
	{
		u32* loopDataPtr = (u32*) loopChunk;
		u32 numLoops = *loopDataPtr;

		if(numLoops == 0)
		{
			return;
		}

		audAssert(numLoops < kMaxSyncLoops);
		audAssert(numLoops >= 2);

		loopDataPtr++;

		// Create two synchronized loop submixes, as we need to blend one loop above/one loop below the current granular submix
		for(u32 loop = 0; loop < 2; loop++)
		{
			audGranularSubmix* loopSubmix = rage_aligned_new(16) audGranularSubmix(&m_WaveRef, m_GrainTable, m_NumGrains);
			loopSubmix->SetSubmixType(audGranularSubmix::SubmixTypeSynchronisedLoop);
			m_GranularSubmixes.Push(loopSubmix);
		}

		for(u32 loopIndex = 0; loopIndex < numLoops && loopIndex < kMaxSyncLoops; loopIndex++)
		{
			audGranularSubmix::audGrainPlaybackOrder playbackOrder = (audGranularSubmix::audGrainPlaybackOrder)*loopDataPtr;
			loopDataPtr++;

			u32 numValidGrains = *loopDataPtr;
			loopDataPtr++;

			u32 submixID = *loopDataPtr;
			loopDataPtr++;

			audSyncLoopDefinition syncLoopDefinition;
			syncLoopDefinition.submixID = submixID;
			syncLoopDefinition.playbackOrder = playbackOrder;
			syncLoopDefinition.numValidGrains = numValidGrains;

#if __BANK
			// If we're running in bank mode, grab a copy of the grain data so that we can edit it in realtime
			for(u32 loop = 0; loop < numValidGrains; loop++)
			{
				syncLoopDefinition.validGrains.PushAndGrow(((u32*)loopDataPtr)[loop]);
			}
#else
			syncLoopDefinition.validGrains = (u32*)loopDataPtr;
#endif
			
			syncLoopDefinition.enabled = true;

			m_SynchronisedLoopDefinitions.Push(syncLoopDefinition);
			GenerateLoopInfo(loopIndex);

			loopDataPtr += (numValidGrains);			
		}

		CalculateMinMaxLoopHz();
	}

	// Make sure the loops are ordered correctly. Required on PS3 for vram loader loop prediction
	m_SynchronisedLoopDefinitions.QSort(0, -1, QSortSyncLoopDefinitions);
}

// ----------------------------------------------------------------
// Generate loop info
// ----------------------------------------------------------------
void audGranularMix::GenerateLoopInfo(u32 loopIndex)
{
	u32 numValidGrains = m_SynchronisedLoopDefinitions[loopIndex].numValidGrains;
	u32 validGrainIndexSum = 0;
	u32 validGrainSampleIndexSum = 0;
	float validGrainPitchSum = 0.0f;
	f32 minPitch = Min(m_GrainTable[0].pitch, m_GrainTable[m_NumGrains-1].pitch);

	for(u32 loop = 0; loop < numValidGrains; loop++)
	{
		validGrainIndexSum += m_SynchronisedLoopDefinitions[loopIndex].validGrains[loop];
		validGrainSampleIndexSum += m_GrainTable[m_SynchronisedLoopDefinitions[loopIndex].validGrains[loop]].startSample;

		f32 thisGrainPitch = m_GrainTable[m_SynchronisedLoopDefinitions[loopIndex].validGrains[loop]].pitch;
		f32 grainPitch = minPitch + ((thisGrainPitch - minPitch) * m_PitchStretch);
		grainPitch -= m_PitchShift;
		validGrainPitchSum += grainPitch;
	}

	m_SynchronisedLoopDefinitions[loopIndex].averageValidGrainIndex = validGrainIndexSum / (f32)numValidGrains;
	m_SynchronisedLoopDefinitions[loopIndex].averageValidGrainHertz = validGrainPitchSum/ (f32)numValidGrains;
	m_SynchronisedLoopDefinitions[loopIndex].averageValidGrainFraction = m_SynchronisedLoopDefinitions[loopIndex].averageValidGrainIndex / m_NumGrains;
}

// ----------------------------------------------------------------
// Calculate the min and max hz of all the loops
// ----------------------------------------------------------------
void audGranularMix::CalculateMinMaxLoopHz()
{
	m_MinLoopHz = 1000.0f;
	m_MaxLoopHz = 0.0f;

	for(s32 loop = 0; loop < m_SynchronisedLoopDefinitions.GetCount(); loop++)
	{
		m_MinLoopHz = Min(m_MinLoopHz, m_SynchronisedLoopDefinitions[loop].averageValidGrainHertz);
		m_MaxLoopHz = Max(m_MaxLoopHz, m_SynchronisedLoopDefinitions[loop].averageValidGrainHertz);
	}

}
#endif // !__SPU

// ----------------------------------------------------------------
// Generate a frame of PCM data
// ----------------------------------------------------------------
void audGranularMix::GenerateFrame(f32* destBuffer, 
								   u32 numGrainsGenerated,
								   f32 currentGranularFraction, 
								   f32 granuleFractionPerSample)
{
	PF_FUNC(GenerateFrameGranularMix);
	bool loopAboveMixed = false;

#if __SPU
	const u32 mixSize = Align16(sizeof(audGranularSubmix));

	for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		dmaWait(g_ScratchSubmixTag + loop);
		dmaGet(g_ScratchSubmixes[loop], m_GranularSubmixes[loop], mixSize, false, g_ScratchSubmixTag + loop);
	}
#endif

	for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		audGranularSubmix* submix = NULL;

#if __SPU
		dmaWait(g_ScratchSubmixTag + loop);
		submix = (audGranularSubmix*) g_ScratchSubmixes[loop];
#else
		submix = m_GranularSubmixes[loop];
#endif

		f32 volume = 0.0f;

		if(loop == 0)
		{
			volume = m_GrainVolumeScale;
		}
		else if(submix->GetSynchronisedLoopIndex() == m_NearestLoopAbove)
		{
			if(loopAboveMixed)
			{
				volume = 0.0f;
			}
			else if(m_NearestLoopBelow == m_NearestLoopAbove)
			{
				volume = m_LoopVolumeScale;
			}
			else
			{
				volume = m_NearestLoopAboveVolumeScale * m_LoopVolumeScale;
			}

			loopAboveMixed = true;
		}
		else if(submix->GetSynchronisedLoopIndex() == m_NearestLoopBelow && m_NearestLoopBelow != m_NearestLoopAbove)
		{
			volume = m_NearestLoopBelowVolumeScale * m_LoopVolumeScale;
		}

		volume *= m_CurrentVolumeScale;
		volume *= m_MasterVolumeScale;
		submix->GenerateFrame(destBuffer, volume, numGrainsGenerated, currentGranularFraction, granuleFractionPerSample, m_MixNative);

#if __SPU
		dmaPut(submix, m_GranularSubmixes[loop], mixSize, false, g_ScratchSubmixTag + loop);
#endif
	}
}

#if __SPU
// ----------------------------------------------------------------
// Called post mix
// ----------------------------------------------------------------
void audGranularMix::SpuPostMix()
{
	if(m_IsPlayingPhysically)
	{
		const u32 mixSize = Align16(sizeof(audGranularSubmix));

		for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
		{
			dmaWait(g_ScratchSubmixTag + loop);
			dmaGet(g_ScratchSubmixes[loop], m_GranularSubmixes[loop], mixSize, false, g_ScratchSubmixTag + loop);
		}

		for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
		{
			dmaWait(g_ScratchSubmixTag + loop);
			audGranularSubmix* submix = (audGranularSubmix*) g_ScratchSubmixes[loop];
			bool submixUpdated = submix->SpuPostMix();

			if(submixUpdated)
			{
				dmaPut(submix, m_GranularSubmixes[loop], mixSize, false, g_ScratchSubmixTag + loop);
			}
		}
	}
}
#endif

#if !__SPU
// ----------------------------------------------------------------
// Called before starting to generate a frame of PCM data
// ----------------------------------------------------------------
void audGranularMix::BeginFrame(const f32 currentRateHz, const f32 predictedRateHz, const f32 xValueStepPerGranule, const f32 volumeModulation)
{
#if __PS3
	for(u32 loop = 0; loop < kNumLoopVramLoaders; loop++)
	{
		if(m_LoopVramLoaders[loop].initialPacketSent)
		{
			m_LoopVramLoaders[loop].vramHelper.Update();
		}
	}

	for(u32 loop = 0; loop < kNumGrainVramLoaders; loop++)
	{
		if(m_GrainVramLoaders[loop].initialPacketSent)
		{
			m_GrainVramLoaders[loop].vramHelper.Update();
		}
	}
#endif

	if(!m_IsPlayingPhysically || m_Muted)
	{
		return;
	}

#if __BANK
	m_LoopGrainVolumeSmoother.SetRates(s_GranularGrainToLoopChangeRate, s_GranularLoopToGrainChangeRate);
#endif

	m_NearestLoopAbove = -1;
	m_NearestLoopBelow = -1;
	m_NearestLoopAboveVolumeScale = 1.0f;
	m_NearestLoopBelowVolumeScale = 1.0f;
	m_DesiredLoopVolumeScale = 0;

	if(m_GranularSubmixes.GetCount() == 1)
	{
		// No loops, so no option to play them
		m_DesiredLoopVolumeScale = 0;
	}
	else if(m_GrainPlaybackStyle == PlaybackStyleLoopsAndGrains)
	{
		f32 analysisRange = 0.1f;
		f32 pitchRange = abs(m_ParentMaxPitch - m_ParentMinPitch) * analysisRange;
		f32 pitchAbove = predictedRateHz + pitchRange;
		f32 pitchBelow = predictedRateHz - pitchRange;
		s32 grainAbove = audGranularSubmix::CalculateGrainIndexFromPitch(pitchAbove, m_GrainTable, m_NumGrains, m_PitchShift, m_PitchStretch);
		s32 grainBelow = audGranularSubmix::CalculateGrainIndexFromPitch(pitchBelow, m_GrainTable, m_NumGrains, m_PitchShift, m_PitchStretch);
		f32 grainRange = abs(grainAbove - grainBelow) / analysisRange;
		m_GranuleStepPerGranule = m_LoopGrainCalcSmoother.CalculateValue(grainRange * abs(xValueStepPerGranule));

		m_DesiredLoopVolumeScale = m_GranuleStepPerGranule < m_GranularChangeRateForLoops ? 1 : 0;
	}
	else if(m_GrainPlaybackStyle == PlaybackStyleLoopsOnly)
	{
		m_DesiredLoopVolumeScale = 1;
	}
	else if(m_GrainPlaybackStyle == PlaybackStyleGrainsOnly)
	{
		m_DesiredLoopVolumeScale = 0;
	}

	u32 actualLoopVolumeScale = m_DesiredLoopVolumeScale;

	if(m_DesiredLoopVolumeScale == 0)
	{
		if(!m_GranularSubmixes[0]->IsReadyToStart())
		{
			if(m_GranularSubmixes.GetCount() > 1 && (m_GranularSubmixes[1]->IsReadyToStart() || m_GranularSubmixes[2]->IsReadyToStart()))
			{
				actualLoopVolumeScale = 1;
			}
		}
	}
	else
	{
		if(m_GranularSubmixes.GetCount() > 1)
		{
			if(!m_GranularSubmixes[1]->IsReadyToStart() && !m_GranularSubmixes[2]->IsReadyToStart())
			{
				if(m_GranularSubmixes[0]->IsReadyToStart())
				{
					actualLoopVolumeScale = 0;
				}
			}
		}
	}

	f32 loopVolumeScale = m_LoopGrainVolumeSmoother.CalculateValue((f32)actualLoopVolumeScale);

	if(m_Quality == GrainPlayerQualityHigh)
	{
		loopVolumeScale = Clamp(loopVolumeScale, 0.0f, m_MaxLoopProportion);
	}

	if(m_SynchronisedLoopDefinitions.GetCount() > 0)
	{
		f32 loopHzCalculationPoint = currentRateHz;

		if(m_LoopRandomisationEnabled)
		{
#if __BANK
			m_LoopHzSmoother.SetRates(m_LoopRandomisationChangeRate, m_LoopRandomisationChangeRate);
#endif

			if(loopHzCalculationPoint < m_MinLoopHz)
			{
				loopHzCalculationPoint = m_MinLoopHz;
			}
			else if(loopHzCalculationPoint > m_MaxLoopHz)
			{
				loopHzCalculationPoint = m_MaxLoopHz;
			}

			if(actualLoopVolumeScale == 0)
			{
				m_LoopHzSmootherCurrent = 0.0f;
				m_LoopHzSmootherTarget = 0.0f;
				m_LoopHzSmoother.Reset();
				m_LoopHzRandomisationApplySmoother.Reset();
				m_LoopHzRandomisationApplySmoother.CalculateValue(0.0f);
			}
			else
			{
				if(m_LoopHzSmootherCurrent == m_LoopHzSmootherTarget)
				{
					f32 randomisationPitchRange = (m_MaxLoopHz - m_MinLoopHz) * m_LoopRandomisationMaxPitchFraction;
					m_LoopHzSmootherTarget = audEngineUtil::GetRandomNumberInRange(-randomisationPitchRange, 0.0f);
				}
			}

			if(!m_PitchLocked)
			{
				m_LoopHzSmootherCurrent = m_LoopHzSmoother.CalculateValue(m_LoopHzSmootherTarget);
				f32 loopHzSmootherApplyScale = m_LoopHzRandomisationApplySmoother.CalculateValue(1.0f - (m_GranuleStepPerGranule/s_LoopRandomisationGranularChangeRate));
				loopHzCalculationPoint += m_LoopHzSmootherCurrent * loopHzSmootherApplyScale;
			}
		}

		CalculateNearestLoops(loopHzCalculationPoint, m_NearestLoopAbove, m_NearestLoopBelow, m_NearestLoopAboveVolumeScale, m_NearestLoopBelowVolumeScale);
		bool nearestLoopAboveValid = false;
		bool nearestLoopBelowValid = false;

		if(m_GranularSubmixes.GetCount() > 1 && m_Quality == GrainPlayerQualityHigh)
		{
			// We're at full loop mode
			if(m_MaxLoopProportion < 1.0f && loopVolumeScale >= m_MaxLoopProportion)
			{
				s32 nearestAbove = -1;
				s32 nearestBelow = -1;
				f32 volumeScale = 0.0f;

				// Need to recalculate this without the loop hz randomisation applied, as we don't want the min/max grains limits moving about
				CalculateNearestLoops(Clamp(currentRateHz, m_MinLoopHz, m_MaxLoopHz), nearestAbove, nearestBelow, volumeScale, volumeScale);

				if(nearestAbove == nearestBelow)
				{
					if(nearestAbove > 0)
					{
						nearestBelow--;
					}
					else
					{
						nearestAbove++;
					}
				}

				u32 minLoopStart = 0;
				u32 maxLoopEnd = 0;

				if(m_AllowLoopGrainOverlap)
				{
					minLoopStart = Min(m_SynchronisedLoopDefinitions[nearestAbove].validGrains[0], m_SynchronisedLoopDefinitions[nearestBelow].validGrains[0]);
					maxLoopEnd = Max(m_SynchronisedLoopDefinitions[nearestAbove].validGrains[m_SynchronisedLoopDefinitions[nearestAbove].numValidGrains - 1], m_SynchronisedLoopDefinitions[nearestBelow].validGrains[m_SynchronisedLoopDefinitions[nearestBelow].numValidGrains - 1]);
				}
				else
				{
					minLoopStart = m_SynchronisedLoopDefinitions[nearestBelow].validGrains[m_SynchronisedLoopDefinitions[nearestBelow].numValidGrains - 1];
					maxLoopEnd = m_SynchronisedLoopDefinitions[nearestAbove].validGrains[0];
				}
				
				m_GranularSubmixes[0]->SetMinMaxGrainLimit(minLoopStart, maxLoopEnd);
			}
			else
			{
				m_GranularSubmixes[0]->SetMinMaxGrainLimit(-1, -1);
			}
		}

		for(s32 loop = 1; loop < m_GranularSubmixes.GetCount(); loop++)
		{
			// Clear out any loops that aren't needed
			if(m_GranularSubmixes[loop]->GetSynchronisedLoopIndex() == m_NearestLoopAbove)
			{
				nearestLoopAboveValid = true;
			}

			if(m_GranularSubmixes[loop]->GetSynchronisedLoopIndex() == m_NearestLoopBelow)
			{
				nearestLoopBelowValid = true;
			}
		}

		for(s32 loop = 1; loop < m_GranularSubmixes.GetCount(); loop++)
		{
			if(m_GranularSubmixes[loop]->GetSynchronisedLoopIndex() != m_NearestLoopAbove && m_GranularSubmixes[loop]->GetSynchronisedLoopIndex() != m_NearestLoopBelow)
			{
				if(!nearestLoopAboveValid && m_NearestLoopAbove >= 0)
				{
#if __BANK
					m_GranularSubmixes[loop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_NearestLoopAbove].validGrains.GetElements(), m_SynchronisedLoopDefinitions[m_NearestLoopAbove].numValidGrains, m_NearestLoopAbove);
#else
					m_GranularSubmixes[loop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_NearestLoopAbove].validGrains, m_SynchronisedLoopDefinitions[m_NearestLoopAbove].numValidGrains, m_NearestLoopAbove);
#endif				
					m_GranularSubmixes[loop]->SetPlaybackOrder(m_SynchronisedLoopDefinitions[m_NearestLoopAbove].playbackOrder);
					nearestLoopAboveValid = true;

					if(m_NearestLoopAbove == m_NearestLoopBelow)
					{
						nearestLoopBelowValid = true;
					}
				}
				else if(!nearestLoopBelowValid && m_NearestLoopBelow >= 0)
				{
#if __BANK
					m_GranularSubmixes[loop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_NearestLoopBelow].validGrains.GetElements(), m_SynchronisedLoopDefinitions[m_NearestLoopBelow].numValidGrains, m_NearestLoopBelow);
#else
					m_GranularSubmixes[loop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_NearestLoopBelow].validGrains, m_SynchronisedLoopDefinitions[m_NearestLoopBelow].numValidGrains, m_NearestLoopBelow);
#endif
					m_GranularSubmixes[loop]->SetPlaybackOrder(m_SynchronisedLoopDefinitions[m_NearestLoopBelow].playbackOrder);
					nearestLoopBelowValid = true;

					if(m_NearestLoopAbove == m_NearestLoopBelow)
					{
						nearestLoopAboveValid = true;
					}
				}
			}
		}
	}
	else
	{
		loopVolumeScale = 0.0f;
	}

	m_LoopVolumeScale = loopVolumeScale;
	m_GrainVolumeScale = 1.f - loopVolumeScale;

	if((m_GrainVolumeScale > 0.0f && m_GranularSubmixes[0]->IsReadyToStart()) ||
	   (m_LoopVolumeScale > 0.0f && m_GranularSubmixes.GetCount() > 1 && (m_GranularSubmixes[1]->IsReadyToStart() || m_GranularSubmixes[2]->IsReadyToStart())))
	{
		m_CurrentVolumeScale = m_VolumeSmoother.CalculateValue(m_TargetVolumeScale);

		if(s_GranularMixCrossfadeStyle == audGranularSubmix::CrossfadeStyleEqualPower)
		{
			const f32 PI_over_2 = (PI / 2.0f);
			m_CurrentVolumeScale = Sinf(m_CurrentVolumeScale * PI_over_2);
		}
	}

	if(m_CurrentVolumeScale <= 0.0f &&
	   m_TargetVolumeScale <= 0.0f)
	{
		SetMuted(true);
	}

	if((audGranularSubmix::audGrainCrossfadeStyle)s_LoopGrainCrossfadeStyle == audGranularSubmix::CrossfadeStyleEqualPower)
	{
		const f32 PI_over_2 = (PI / 2.0f);
		m_LoopVolumeScale = Sinf(loopVolumeScale * PI_over_2);
		m_GrainVolumeScale = Sinf((1.f - loopVolumeScale) * PI_over_2);
	}	

	m_LoopVolumeScale *= volumeModulation;
	m_GrainVolumeScale *= volumeModulation;

#if __PS3
	u32 allowedDecodes = (u32) m_HertzToDecodesCurve.CalculateValue(currentRateHz);
#endif
	
	for (s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		PS3_ONLY(m_GranularSubmixes[loop]->SetAllowedDecodesPerFrame(allowedDecodes);)
		m_GranularSubmixes[loop]->SetInterGrainCrossfadeStyle((audGranularSubmix::audGrainCrossfadeStyle)s_InterGrainCrossfadeStyle);
		m_GranularSubmixes[loop]->SetPeakNormaliseLoops(s_GranularPeakNormaliseLoops);
		m_GranularSubmixes[loop]->BeginFrame();
	}
}

// ----------------------------------------------------------------
// Snap to the nearest playback type (ie. grains/loops)
// ----------------------------------------------------------------
void audGranularMix::SnapToNearestPlaybackType()
{
	if(m_LoopVolumeScale > m_GrainVolumeScale)
	{
		m_GrainPlaybackStyle = PlaybackStyleLoopsOnly;
		m_GranularSubmixes[0]->StopPhys();
	}
	else if(m_GranularSubmixes.GetCount() > 1) 
	{
		m_GrainPlaybackStyle = PlaybackStyleGrainsOnly;
		m_GranularSubmixes[1]->StopPhys();
		m_GranularSubmixes[2]->StopPhys();
	}

	m_LoopGrainVolumeSmoother.Reset();
}

// ----------------------------------------------------------------
// Set as a purely random player
// ----------------------------------------------------------------
void audGranularMix::SetPureRandom(bool pureRandom)
{
	Assert(m_GranularSubmixes.GetCount() == 1);
	m_PureRandomPlayback = pureRandom;
	m_GranularSubmixes[0]->SetPureRandom(pureRandom);
}

// ----------------------------------------------------------------
// Set as a purely random player
// ----------------------------------------------------------------
void audGranularMix::SetPureRandomWithStyle(audGranularSubmix::audGrainPlaybackOrder style)
{
	Assert(m_GranularSubmixes.GetCount() == 1);
	m_PureRandomPlayback = true;
	m_GranularSubmixes[0]->SetPureRandomWithStyle(style);
}

// ----------------------------------------------------------------
// Set the grain player quality
// ----------------------------------------------------------------
void audGranularMix::SetQuality(audGrainPlayerQuality quality)
{
	if(quality != m_Quality)
	{
		m_Quality = quality;

#if __PS3
		if(m_Quality == GrainPlayerQualityLow)
		{
			if(m_SynchronisedLoopDefinitions.GetCount() > 0 && quality == GrainPlayerQualityLow)
			{
				for(u32 vramLoaderLoop = 0; vramLoaderLoop < kNumGrainVramLoaders; vramLoaderLoop++)
				{
					m_GrainVramLoaders[vramLoaderLoop].vramHelper.Reset();
				}
			}
		}
#endif
	}
}

// ----------------------------------------------------------------
// Called after generating a frame of PCM data
// ----------------------------------------------------------------
void audGranularMix::EndFrame(const f32 currentRateHz PS3_ONLY(, const f32 predictedRateHz, const f32 xValueStepPerBuffer))
{
#if __PS3
	u32 numSubmixesActive = 0;

	for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		if(m_GranularSubmixes[loop]->IsPlayingPhysically())
		{
			numSubmixesActive++;
		}

		if(m_GranularSubmixes[loop]->IsAwaitingGrainChange())
		{
			for(u32 loopVramLoaderLoop = 0; loopVramLoaderLoop < kNumLoopVramLoaders; loopVramLoaderLoop++)
			{
				m_LoopVramLoaders[loopVramLoaderLoop].grainChangedSinceLoaded = true;
			}

			for(u32 grainVramLoaderLoop = 0; grainVramLoaderLoop < kNumGrainVramLoaders; grainVramLoaderLoop++)
			{
				m_GrainVramLoaders[grainVramLoaderLoop].grainChangedSinceLoaded = true;
			}

			break;
		}
	}

	// If nothing is playing back, this lets our vram loaders keep updating
	if(numSubmixesActive == 0)
	{
		for(u32 loopVramLoaderLoop = 0; loopVramLoaderLoop < kNumLoopVramLoaders; loopVramLoaderLoop++)
		{
			m_LoopVramLoaders[loopVramLoaderLoop].grainChangedSinceLoaded = true;
		}

		for(u32 grainVramLoaderLoop = 0; grainVramLoaderLoop < kNumGrainVramLoaders; grainVramLoaderLoop++)
		{
			m_GrainVramLoaders[grainVramLoaderLoop].grainChangedSinceLoaded = true;
		}
	}
#endif

	for (s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		if(m_IsPlayingPhysically)
		{
			if(m_Muted && m_GranularSubmixes[loop]->GetPrevVolumeScale() <= 0.0f)
			{
				m_GranularSubmixes[loop]->StopPhys();
			}
			else if(loop == 0)
			{
				if(((m_Quality == GrainPlayerQualityHigh && m_MaxLoopProportion < 1.0f) || m_DesiredLoopVolumeScale == 0) && !m_Muted)
				{ 
					m_GranularSubmixes[loop]->StartPhys();
				}
				else if(m_DesiredLoopVolumeScale == 1 && m_LoopVolumeScale == 1.0f && m_GranularSubmixes[loop]->GetPrevVolumeScale() <= 0.0f)
				{
					m_GranularSubmixes[loop]->StopPhys();
				}
			}
			else
			{
				if(m_DesiredLoopVolumeScale == 1 && !m_Muted)
				{
					if(!m_GranularSubmixes[loop]->StartPhys())
					{
						// If we can't start one synchronised loop, we can't start any of them
						for (s32 loopSubmixLoop = 1; loopSubmixLoop < m_GranularSubmixes.GetCount(); loopSubmixLoop++)
						{
							m_GranularSubmixes[loopSubmixLoop]->StopPhys();
						}

						break;
					}
				}
				else if(m_DesiredLoopVolumeScale == 0 && m_LoopVolumeScale == 0.0f && m_GranularSubmixes[loop]->GetPrevVolumeScale() <= 0.0f)
				{
					m_GranularSubmixes[loop]->StopPhys();
				}
			}
		}
		else
		{
			m_GranularSubmixes[loop]->StopPhys();
		}

		m_GranularSubmixes[loop]->ClearInvalidGrains();

		for(s32 invalidGrainLoop = 0; invalidGrainLoop < m_GranularSubmixes.GetCount(); invalidGrainLoop++)
		{
			if(m_GranularSubmixes[invalidGrainLoop]->IsPlayingPhysically())
			{
				for(u32 prevGrainFindLoop = 0; prevGrainFindLoop < 3; prevGrainFindLoop++)
				{
					m_GranularSubmixes[loop]->AddInvalidGrain(m_GranularSubmixes[invalidGrainLoop]->GetPrevGrainIndex(prevGrainFindLoop));
				}
			}
		}

#if __PS3
		if(m_PureRandomPlayback)
		{
			m_GranularSubmixes[loop]->EndFrame(currentRateHz, m_GranuleStepPerGranule, m_GrainVramLoaders, kNumGrainVramLoaders, m_GrainVramLoaders, kNumGrainVramLoaders);
		}
		else
		{
			m_GranularSubmixes[loop]->EndFrame(currentRateHz, m_GranuleStepPerGranule, m_GrainVramLoaders, kNumGrainVramLoaders, m_LoopVramLoaders, kNumLoopVramLoaders);
		}
#else
		m_GranularSubmixes[loop]->EndFrame(currentRateHz, m_GranuleStepPerGranule);
#endif
	}

#if __PS3
	if(m_IsPlayingPhysically)
	{
		UpdateLoopVramLoaders(predictedRateHz);
	}

	UpdateGrainVramLoaders(predictedRateHz, xValueStepPerBuffer);
#endif
}
#endif

#if __PS3 && !__SPU
// ----------------------------------------------------------------
// Reset the vram loaders
// ----------------------------------------------------------------
void audGranularMix::ResetVramLoaders()
{
	for(u32 loop = 0; loop < kNumLoopVramLoaders; loop++)
	{
		m_LoopVramLoaders[loop].Reset();
	}

	for(u32 loop = 0; loop < kNumGrainVramLoaders; loop++)
	{
		m_GrainVramLoaders[loop].Reset();
	}
}

// ----------------------------------------------------------------
// Query if any vram data is available
// ----------------------------------------------------------------
bool audGranularMix::QueryAnyVramLoaderDataAvailable() const
{
	return audGranularSubmix::QueryAnyVramLoaderDataAvailable(m_GrainVramLoaders, kNumGrainVramLoaders) ||
		   audGranularSubmix::QueryAnyVramLoaderDataAvailable(m_LoopVramLoaders, kNumLoopVramLoaders);
}

// ----------------------------------------------------------------
// Given a starting grain, work out the final grain we can conceivably
// fit if we load as much data as possible into our buffer
// ----------------------------------------------------------------
s32 audGranularMix::CalculateFinalGrainInVramBuffer(s32 startGrainIndex, u32 numFetchBuffers)
{
	u32 startPositionBytes = m_GrainTable[startGrainIndex].frameOffsetBytesAsGrainStart;
	const u32 dataSize = (u32)(numFetchBuffers * audDecoderVramHelper::kFetchBufferSize);
	u32 validGrainsFound = 0;
	s32 finalGrainIndex = startGrainIndex;

	for(u32 loop = startGrainIndex + 1; loop < m_NumGrains; loop++)
	{
		if(m_GrainTable[loop].frameOffsetBytesAsGrainEnd - startPositionBytes < dataSize - audGranularSubmix::s_VramCalculationByteLimit)
		{
			if(validGrainsFound > 0)
			{
				finalGrainIndex++;
			}

			validGrainsFound++;
		}
		else
		{
			break;
		}
	}

	return finalGrainIndex;
}

// ----------------------------------------------------------------
// Given a starting grain, work out the final grain we can conceivably
// fit if we load as much data as possible into our buffer. Ignore any grains
// that have already been loaded by another vram loader
// ----------------------------------------------------------------
s32 audGranularMix::CalculateFinalNonLoadedGrainInVramBuffer(s32 startGrainIndex, audGranularSubmix::audVramLoader* queryingLoader, audGranularSubmix::audVramLoader* otherLoaders, u32 numOtherLoaders, u32 numFetchBuffers)
{
	u32 startPositionBytes = m_GrainTable[startGrainIndex].frameOffsetBytesAsGrainStart;
	const u32 dataSize = (u32)(numFetchBuffers * audDecoderVramHelper::kFetchBufferSize);
	u32 validGrainsFound = 0;
	s32 finalGrainIndex = startGrainIndex;

	for(u32 loop = startGrainIndex + 1; loop < m_NumGrains; loop++)
	{
		audGranularSubmix::audVramLoader* vramLoader = audGranularSubmix::GetVramLoaderForGrainIndex(otherLoaders, numOtherLoaders, loop - 1);
		bool allowThisGrain = (vramLoader == NULL || vramLoader == queryingLoader);

		if(m_GrainTable[loop].frameOffsetBytesAsGrainEnd - startPositionBytes < dataSize - audGranularSubmix::s_VramCalculationByteLimit && allowThisGrain)
		{
			if(validGrainsFound > 0)
			{
				finalGrainIndex++;
			}

			validGrainsFound++;
		}
		else
		{
			break;
		}
	}

	return finalGrainIndex;
}

// ----------------------------------------------------------------
// Update the grain vram loaders
// ----------------------------------------------------------------
void audGranularMix::UpdateGrainVramLoaders(const f32 predictedRateHz, f32 xValueStepPerBuffer)
{
	if(m_SynchronisedLoopDefinitions.GetCount() > 0 && m_Quality == GrainPlayerQualityLow)
	{
		return;
	}

	if(m_GrainTable[0].pitch > m_GrainTable[m_NumGrains - 1].pitch)
	{
		xValueStepPerBuffer *= -1.0f;
	}

	for(u32 vramLoaderLoop = 0; vramLoaderLoop < kNumGrainVramLoaders; vramLoaderLoop++)
	{
		bool vramLoaderInUse = false;

		for(s32 submixLoop = 0; submixLoop < m_GranularSubmixes.GetCount(); submixLoop++)
		{
			if(m_GranularSubmixes[submixLoop]->IsUsingVramLoader(&m_GrainVramLoaders[vramLoaderLoop]))
			{
				vramLoaderInUse = true;
				break;
			}
		}

		if(!vramLoaderInUse && 
			((m_GrainVramLoaders[vramLoaderLoop].vramHelper.QueryNumBytesAvailable() == ~0U && m_GrainVramLoaders[vramLoaderLoop].grainChangedSinceLoaded) || !m_GrainVramLoaders[vramLoaderLoop].initialPacketSent))
		{
			s32 startGrainIndex = -1;

			if(m_PureRandomPlayback)
			{
				startGrainIndex = 0;
			}
			else if(m_GranularSubmixes[0]->HasMinMaxGrainLimit())
			{
				startGrainIndex = m_GranularSubmixes[0]->GetLastGrainIndex();
			}
			else
			{
				startGrainIndex = audGranularSubmix::CalculateGrainIndexFromPitch(predictedRateHz, m_GrainTable, m_NumGrains, m_PitchShift, m_PitchStretch);
			}

			audGranularSubmix::ClampSelectedGrain(startGrainIndex, m_NumGrains);

			// Don't buffer right up to the very last grain, as otherwise we'll just end up with a buffer with one grain in it, which isn't much use. Instead
			// rewind the buffer so that we have a decent range in it
			if(startGrainIndex >= (s32)m_NumGrains - 2)
			{
				startGrainIndex = m_GranularSubmixes[0]->GetVramStartGrainForLoadBackwards(startGrainIndex, kGrainVramLoaderNumFetchBuffers);
			}

			audGranularSubmix::ClampSelectedGrain(startGrainIndex, m_NumGrains);

			// We want to load startGrainIndex, but no point if its already loaded. Work out the next chunk above/below depending on which way the data is heading
			for(u32 vramLoaderCompareLoop = 0; vramLoaderCompareLoop < kNumGrainVramLoaders; vramLoaderCompareLoop++)
			{
				// If this loader has already got data correctly loaded
				if(vramLoaderLoop != vramLoaderCompareLoop && (m_GrainVramLoaders[vramLoaderCompareLoop].vramHelper.QueryNumBytesAvailable() == ~0U || !m_GrainVramLoaders[vramLoaderLoop].initialPacketSent))
				{
					if(startGrainIndex >= m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex &&
						startGrainIndex <= m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex)
					{
						bool loadForwards = false;

						if(m_GranularSubmixes[0]->HasMinMaxGrainLimit())
						{
							u32 numGrains = m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex - m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex;
							u32 centralGrain = m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex + (numGrains/2);

							if(m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex == 0)
							{
								loadForwards = true;
							}
							else if(m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex >= m_NumGrains - 2)
							{
								loadForwards = false;
							}
							else if(m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex < m_GranularSubmixes[0]->GetMinGrainLimit())
							{
								loadForwards = true;
							}
							else if(m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex > m_GranularSubmixes[0]->GetMaxGrainLimit())
							{
								loadForwards = false;
							}
							else if(startGrainIndex < centralGrain &&
							   !m_GranularSubmixes[0]->IsWalkingForward())
							{
								loadForwards = false;
							}
							else if(startGrainIndex >= centralGrain &&
									m_GranularSubmixes[0]->IsWalkingForward())
							{
								loadForwards = true;
							}
							else
							{
								continue;
							}
						}	
						else if(m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex == 0)
						{
							loadForwards = true;
						}
						else if(m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex >= m_NumGrains - 2)
						{
							loadForwards = false;
						}
						else if(xValueStepPerBuffer == 0.0f)
						{
							u32 numGrains = m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex - m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex;
							u32 centralGrain = m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex + (numGrains/2);

							if(startGrainIndex < centralGrain)
							{
								loadForwards = false;
							}
							else
							{
								loadForwards = true;
							}
						}
						else
						{
							loadForwards = xValueStepPerBuffer > 0.0f;
						}

						if(loadForwards)
						{
							startGrainIndex = m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex + 1;
						}
						else
						{
							startGrainIndex = m_GranularSubmixes[0]->GetVramStartGrainForLoadBackwards(m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex, kGrainVramLoaderNumFetchBuffers);
						}

						break;
					}
					else
					{
						s32 finalGrainPos = CalculateFinalGrainInVramBuffer(startGrainIndex, kGrainVramLoaderNumFetchBuffers);

						if(startGrainIndex < m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex &&
							finalGrainPos >= m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex &&
							finalGrainPos <= m_GrainVramLoaders[vramLoaderCompareLoop].lastValidGrainIndex)
						{
							startGrainIndex = m_GranularSubmixes[0]->GetVramStartGrainForLoadBackwards(m_GrainVramLoaders[vramLoaderCompareLoop].firstValidGrainIndex, kGrainVramLoaderNumFetchBuffers);
						}
					}
				}
			}

			// The selected grain is already loaded by the current decoder - don't bother reloading
			if(m_GrainVramLoaders[vramLoaderLoop].vramHelper.QueryNumBytesAvailable())
			{
				if(startGrainIndex >= m_GrainVramLoaders[vramLoaderLoop].firstValidGrainIndex &&
					startGrainIndex <= m_GrainVramLoaders[vramLoaderLoop].lastValidGrainIndex)
				{
					continue;
				}
			}

			u32 lastValidGrainIndex = CalculateFinalNonLoadedGrainInVramBuffer(startGrainIndex, &m_GrainVramLoaders[vramLoaderLoop], m_GrainVramLoaders, kNumGrainVramLoaders, kGrainVramLoaderNumFetchBuffers);
			s32 validGrainsFound = lastValidGrainIndex - startGrainIndex;

			// No valid grains found to load, so bail out
			if(validGrainsFound <= 0)
			{
				continue;
			}

			if(m_GrainVramLoaders[vramLoaderLoop].vramHelper.IsInitialised())
			{
				m_GrainVramLoaders[vramLoaderLoop].vramHelper.Reset();
			}
			else
			{
				m_GrainVramLoaders[vramLoaderLoop].vramHelper.Init(m_GranularSubmixes[0]->GetWaveSlot(), true, kGrainVramLoaderNumFetchBuffers);
			}

			const u32 frameOffsetBytes = m_GrainTable[startGrainIndex].frameOffsetBytesAsGrainStart;
			const u32 seekTableOffsetFrames = m_GrainTable[startGrainIndex].startSample / audMp3Util::kFrameSizeSamples;
			const u32 subFrameSampleOffset = m_GrainTable[startGrainIndex].startSample - (seekTableOffsetFrames*audMp3Util::kFrameSizeSamples);

			const u8* mp3DataPtr = (u8*)(m_GranularSubmixes[0]->GetWaveData()) + frameOffsetBytes;
			const u32 dataSize = Min(m_GranularSubmixes[0]->GetWaveLengthBytes() - frameOffsetBytes, (u32)(kGrainVramLoaderNumFetchBuffers * audDecoderVramHelper::kFetchBufferSize));

			m_GrainVramLoaders[vramLoaderLoop].vramHelper.SubmitPacket(mp3DataPtr, 0, dataSize, 0u);
			m_GrainVramLoaders[vramLoaderLoop].vramHelper.Update();
			m_GrainVramLoaders[vramLoaderLoop].startOffsetSamples = m_GrainTable[startGrainIndex].startSample - subFrameSampleOffset;
			m_GrainVramLoaders[vramLoaderLoop].startOffsetBytes = frameOffsetBytes;
			m_GrainVramLoaders[vramLoaderLoop].dataSize = dataSize;
			m_GrainVramLoaders[vramLoaderLoop].initialPacketSent = true;
			m_GrainVramLoaders[vramLoaderLoop].grainChangedSinceLoaded = false;
			m_GrainVramLoaders[vramLoaderLoop].firstValidGrainIndex = startGrainIndex;
			m_GrainVramLoaders[vramLoaderLoop].lastValidGrainIndex = lastValidGrainIndex;
			m_GrainVramLoaders[vramLoaderLoop].synchronisedLoopIndex = -1;
		}
	}
}

// ----------------------------------------------------------------
// Update the loop vram loaders
// ----------------------------------------------------------------
void audGranularMix::UpdateLoopVramLoaders(const f32 predictedRateHz)
{
	if(m_SynchronisedLoopDefinitions.GetCount() == 0)
	{
		return;
	}

	s32 startLoop = Clamp(m_NearestLoopAbove - 1, 0, m_SynchronisedLoopDefinitions.GetCount() - 1);
	s32 endLoop = Clamp(m_NearestLoopAbove, 0, m_SynchronisedLoopDefinitions.GetCount() - 1);
	u32 numLoaded = endLoop - startLoop;

	f32 distanceToStartLoop = 0.0f;
	f32 distanceToEndLoop = 0.0f;

	if(startLoop > 0 && endLoop > 0)
	{
		distanceToStartLoop = abs(predictedRateHz - m_SynchronisedLoopDefinitions[startLoop].averageValidGrainHertz);
		distanceToEndLoop = abs(predictedRateHz - m_SynchronisedLoopDefinitions[endLoop].averageValidGrainHertz);
	}

	while(numLoaded < kNumLoopVramLoaders - 1 && numLoaded < m_SynchronisedLoopDefinitions.GetCount())
	{
		// If we're closer to the lower loop than the higher one, prioritize loading this one
		if(distanceToStartLoop < distanceToEndLoop)
		{
			if(startLoop > 0)
			{
				startLoop--;
				numLoaded++;
			}

			if(endLoop < m_SynchronisedLoopDefinitions.GetCount() - 1 && numLoaded < kNumLoopVramLoaders - 1 && numLoaded < m_SynchronisedLoopDefinitions.GetCount())
			{
				endLoop++;
				numLoaded++;
			}
		}
		// Otherwise if we're closer to the higher one, prioritize loading that one
		else
		{
			if(endLoop < m_SynchronisedLoopDefinitions.GetCount() - 1)
			{
				endLoop++;
				numLoaded++;
			}

			if(startLoop > 0 && numLoaded < kNumLoopVramLoaders - 1 && numLoaded < m_SynchronisedLoopDefinitions.GetCount())
			{
				startLoop--;
				numLoaded++;
			}
		}
	}

	for(u32 loopIndex = startLoop; loopIndex <= endLoop; loopIndex++)
	{
		bool alreadyLoaded = false;

		for(u32 vramLoaderLoop = 0; vramLoaderLoop < kNumLoopVramLoaders; vramLoaderLoop++)
		{
			//if((m_LoopVramLoaders[vramLoaderLoop].vramHelper.QueryNumBytesAvailable() == ~0U || !m_LoopVramLoaders[vramLoaderLoop].initialPacketSent))
			{
				if(m_LoopVramLoaders[vramLoaderLoop].synchronisedLoopIndex == loopIndex)
				{
					alreadyLoaded = true;
				}
			}
		}

		if(!alreadyLoaded)
		{
			for(u32 vramLoaderIndex = 0; vramLoaderIndex < kNumLoopVramLoaders; vramLoaderIndex++)
			{
				bool vramLoaderInUse = false;

				for(s32 submixLoop = 0; submixLoop < m_GranularSubmixes.GetCount(); submixLoop++)
				{
					if(m_GranularSubmixes[submixLoop]->IsUsingVramLoader(&m_LoopVramLoaders[vramLoaderIndex]) ||
					   (m_LoopVramLoaders[vramLoaderIndex].synchronisedLoopIndex >= startLoop && m_LoopVramLoaders[vramLoaderIndex].synchronisedLoopIndex <= endLoop))
					{
						vramLoaderInUse = true;
						break;
					}
				}

				if(vramLoaderInUse)
				{
					continue;
				}
				else
				{
					u32 startGrainIndex = m_SynchronisedLoopDefinitions[loopIndex].validGrains[0];
					u32 lastValidGrainIndex = CalculateFinalGrainInVramBuffer(startGrainIndex, kLoopVramLoaderNumFetchBuffers);
					s32 validGrainsFound = lastValidGrainIndex - startGrainIndex;

					// No valid grains found to load, so bail out
					if(validGrainsFound <= 0)
					{
						continue;
					}

					if(m_LoopVramLoaders[vramLoaderIndex].vramHelper.IsInitialised())
					{
						m_LoopVramLoaders[vramLoaderIndex].vramHelper.Reset();
					}
					else
					{
						m_LoopVramLoaders[vramLoaderIndex].vramHelper.Init(m_GranularSubmixes[0]->GetWaveSlot(), true, kLoopVramLoaderNumFetchBuffers);
					}

					const u32 frameOffsetBytes = m_GrainTable[startGrainIndex].frameOffsetBytesAsGrainStart;
					const u32 seekTableOffsetFrames = m_GrainTable[startGrainIndex].startSample / audMp3Util::kFrameSizeSamples;
					const u32 subFrameSampleOffset = m_GrainTable[startGrainIndex].startSample - (seekTableOffsetFrames*audMp3Util::kFrameSizeSamples);

					const u8* mp3DataPtr = (u8*)(m_GranularSubmixes[0]->GetWaveData()) + frameOffsetBytes;
					const u32 dataSize = Min(m_GranularSubmixes[0]->GetWaveLengthBytes() - frameOffsetBytes, (u32)(kLoopVramLoaderNumFetchBuffers * audDecoderVramHelper::kFetchBufferSize));

					m_LoopVramLoaders[vramLoaderIndex].vramHelper.SubmitPacket(mp3DataPtr, 0, dataSize, 0u);
					m_LoopVramLoaders[vramLoaderIndex].vramHelper.Update();
					m_LoopVramLoaders[vramLoaderIndex].startOffsetSamples = m_GrainTable[startGrainIndex].startSample - subFrameSampleOffset;
					m_LoopVramLoaders[vramLoaderIndex].startOffsetBytes = frameOffsetBytes;
					m_LoopVramLoaders[vramLoaderIndex].dataSize = dataSize;
					m_LoopVramLoaders[vramLoaderIndex].initialPacketSent = true;
					m_LoopVramLoaders[vramLoaderIndex].firstValidGrainIndex = Max(m_SynchronisedLoopDefinitions[loopIndex].validGrains[0], startGrainIndex);
					m_LoopVramLoaders[vramLoaderIndex].lastValidGrainIndex = Min(m_SynchronisedLoopDefinitions[loopIndex].validGrains[m_SynchronisedLoopDefinitions[loopIndex].numValidGrains - 1], lastValidGrainIndex);
					m_LoopVramLoaders[vramLoaderIndex].synchronisedLoopIndex = loopIndex;
					vramLoaderIndex = kNumLoopVramLoaders;
				}
			}
		}
	}
}
#endif

#if !__SPU
// ----------------------------------------------------------------
// Mute or unmute the mix
// ----------------------------------------------------------------
void audGranularMix::SetMuted(const bool muted)
{
	if(muted != m_Muted)
	{
		m_Muted = muted;
		
		for (s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
		{
			m_GranularSubmixes[loop]->SetMuted(muted);
		}

		if(muted)
		{
			m_LoopGrainVolumeSmoother.Reset();
		}
	}
}

// ----------------------------------------------------------------
// Set loop randomisation enabled
// ----------------------------------------------------------------
void audGranularMix::SetLoopRandomisationEnabled(const bool enabled, const f32 changeRate, const f32 maxPitchFraction)
{
	m_LoopRandomisationEnabled = enabled;

	if(enabled)
	{
		m_LoopRandomisationChangeRate = changeRate;
		m_LoopRandomisationMaxPitchFraction = maxPitchFraction;
	}
}

// ----------------------------------------------------------------
// Set the channel volume
// ----------------------------------------------------------------
void audGranularMix::SetVolume(const f32 volume, const f32 smoothRate)
{
	m_VolumeSmoother.SetRates(smoothRate, smoothRate);
	m_TargetVolumeScale = volume;

	if(m_TargetVolumeScale > 0.0f)
	{
		SetMuted(false);
	}
}

// ----------------------------------------------------------------
// Query the number of decoders in use
// ----------------------------------------------------------------
u32 audGranularMix::QueryNumDecodersInUse() const
{
	u32 numDecoders = 0;

	for (s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		numDecoders += m_GranularSubmixes[loop]->QueryNumDecodersInUse();
	}

	return numDecoders;
}

#if __PS3
// ----------------------------------------------------------------
// Query the number of vram helpers in use
// ----------------------------------------------------------------
u32 audGranularMix::QueryNumVramHelpersInUse() const
{
	u32 numHelpers = 0;

	for (s32 loop = 0; loop < kNumLoopVramLoaders; loop++)
	{
		if(m_LoopVramLoaders[loop].vramHelper.GetFetchBufferPtr())
		{
			numHelpers++;
		}
	}

	for (s32 loop = 0; loop < kNumGrainVramLoaders; loop++)
	{
		if(m_GrainVramLoaders[loop].vramHelper.GetFetchBufferPtr())
		{
			numHelpers++;
		}
	}

	return numHelpers;
}
#endif

// ----------------------------------------------------------------
// Calculate the nearest two loops to play
// ----------------------------------------------------------------
void audGranularMix::CalculateNearestLoops(const f32 currentRateHz, s32& nearestLoopAboveOut, s32& nearestLoopBelowOut, f32& nearestLoopAboveVolumeScaleOut, f32& nearestLoopBelowVolumeScaleOut) const
{
	PF_FUNC(CalculateNearestLoops);

	s32 nearestLoopAbove = -1;
	f32 nearestLoopAboveXValue = 10000.0f;
	f32 nearestLoopAboveVolumeScale = 1.0f;
	s32 nearestLoopBelow = -1;
	f32 nearestLoopBelowXValue = -10000.0f;
	f32 nearestLoopBelowVolumeScale = 1.0f;

	for (s32 loop = 0; loop < m_SynchronisedLoopDefinitions.GetCount(); loop++)
	{
		f32 loopXValue = m_SynchronisedLoopDefinitions[loop].averageValidGrainHertz;

		if (loopXValue <= currentRateHz &&
			loopXValue > nearestLoopBelowXValue)
		{
			nearestLoopBelowXValue = loopXValue;
			nearestLoopBelow = loop;
		}
		else if (loopXValue > currentRateHz &&
			loopXValue < nearestLoopAboveXValue)
		{
			nearestLoopAboveXValue = loopXValue;
			nearestLoopAbove = loop;
		}
	}

	// If we found a loop both above and below, we need to crossfade between them
	if (nearestLoopAbove >= 0 &&
		nearestLoopBelow >= 0)
	{
		f32 nearestLoopBelowVolumeScaleLin = 1.0f - (currentRateHz - m_SynchronisedLoopDefinitions[nearestLoopBelow].averageValidGrainHertz) / (m_SynchronisedLoopDefinitions[nearestLoopAbove].averageValidGrainHertz - m_SynchronisedLoopDefinitions[nearestLoopBelow].averageValidGrainHertz);

		// Apply a bias towards the loop below, on the basis that pitched up granular loops are more natural sounding than
		// pitched down ones. Initial tests seem good - seems to provide smoother pure-loop playback
		nearestLoopBelowVolumeScaleLin *= s_GranularLoopBelowBias;
		nearestLoopBelowVolumeScaleLin = Clamp(nearestLoopBelowVolumeScaleLin, 0.0f, 1.0f);

		if((audGranularSubmix::audGrainCrossfadeStyle)s_LoopLoopCrossfadeStyle == audGranularSubmix::CrossfadeStyleEqualPower)
		{
			const f32 PI_over_2 = (PI / 2.0f);
			nearestLoopAboveVolumeScale = Sinf((1.0f - nearestLoopBelowVolumeScaleLin) * PI_over_2);
			nearestLoopBelowVolumeScale = Sinf(nearestLoopBelowVolumeScaleLin * PI_over_2); 
		}
		else
		{
			nearestLoopBelowVolumeScale = nearestLoopBelowVolumeScaleLin;
			nearestLoopAboveVolumeScale = 1.0f - nearestLoopBelowVolumeScaleLin;
		}
	}

	if(nearestLoopBelow >= 0 && nearestLoopBelow == nearestLoopAbove)
	{
		nearestLoopAboveVolumeScaleOut = 1.0f;
		nearestLoopBelowVolumeScaleOut = 0.0f;
		nearestLoopAboveOut = nearestLoopAbove;
		nearestLoopBelowOut = nearestLoopBelow;
	}
	else if(nearestLoopBelow == -1 && nearestLoopAbove >= 0)
	{
		nearestLoopAboveVolumeScaleOut = 1.0f;
		nearestLoopBelowVolumeScaleOut = 0.0f;
		nearestLoopAboveOut = nearestLoopAbove;
		nearestLoopBelowOut = nearestLoopAbove;
	}
	else if(nearestLoopAbove == -1 && nearestLoopBelow >= 0)
	{
		nearestLoopAboveVolumeScaleOut = 0.0f;
		nearestLoopBelowVolumeScaleOut = 1.0f;
		nearestLoopAboveOut = nearestLoopBelow;
		nearestLoopBelowOut = nearestLoopBelow;
	}
	else
	{
		nearestLoopAboveVolumeScaleOut = nearestLoopAboveVolumeScale;
		nearestLoopBelowVolumeScaleOut = nearestLoopBelowVolumeScale;
		nearestLoopAboveOut = nearestLoopAbove;
		nearestLoopBelowOut = nearestLoopBelow;
	}
}
#endif

#if __BANK && !__SPU
// ----------------------------------------------------------------
// audGranularMix LoopEditorShiftLeft
// ----------------------------------------------------------------
void audGranularMix::LoopEditorShiftLeft(u32 amount)
{
	if(m_LoopEditorActive)
	{
		if(m_LoopEditorLoopIndex < (u32)m_SynchronisedLoopDefinitions.GetCount())
		{
			u32 numGrains = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetCount();
			u32 firstGrain = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains[0];
			u32 lastGrain = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains[numGrains - 1];

			if((s32)firstGrain - (s32)amount >= 0) 
			{
				m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.clear();

				for(u32 loop = firstGrain - amount; loop <= lastGrain -amount; loop++)
				{
					m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.PushAndGrow(loop);
				}

				GenerateLoopInfo(m_LoopEditorLoopIndex); 
				CalculateMinMaxLoopHz();

				for(s32 submixLoop = 0; submixLoop < m_GranularSubmixes.GetCount(); submixLoop++)
				{
					if(m_GranularSubmixes[submixLoop]->GetSynchronisedLoopIndex() == (s32)m_LoopEditorLoopIndex)
					{
						m_GranularSubmixes[submixLoop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetElements(), m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].numValidGrains, m_LoopEditorLoopIndex);
					}
				}
			}
		}
	}
}

// ----------------------------------------------------------------
// audGranularMix LoopEditorShiftRight
// ----------------------------------------------------------------
void audGranularMix::LoopEditorShiftRight(u32 amount)
{
	if(m_LoopEditorActive)
	{
		if(m_LoopEditorLoopIndex < (u32)m_SynchronisedLoopDefinitions.GetCount())
		{
			u32 numGrains = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetCount();
			u32 firstGrain = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains[0];
			u32 lastGrain = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains[numGrains - 1];

			if(lastGrain + amount < m_NumGrains)
			{
				m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.clear();

				for(u32 loop = firstGrain + amount; loop <= lastGrain + amount; loop++)
				{
					m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.PushAndGrow(loop);
				}

				GenerateLoopInfo(m_LoopEditorLoopIndex);
				CalculateMinMaxLoopHz();

				for(s32 submixLoop = 0; submixLoop < m_GranularSubmixes.GetCount(); submixLoop++)
				{
					if(m_GranularSubmixes[submixLoop]->GetSynchronisedLoopIndex() == (s32)m_LoopEditorLoopIndex)
					{
						m_GranularSubmixes[submixLoop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetElements(), m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].numValidGrains, m_LoopEditorLoopIndex);
					}
				}
			}
		}
	}
}

// ----------------------------------------------------------------
// audGranularMix LoopEditorGrowLoop
// ----------------------------------------------------------------
void audGranularMix::LoopEditorGrowLoop()
{
	if(m_LoopEditorActive)
	{
		if(m_LoopEditorLoopIndex < (u32)m_SynchronisedLoopDefinitions.GetCount())
		{
			u32 numGrains = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetCount();
			u32 lastGrain = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains[numGrains - 1];

			if(lastGrain + 1 < m_NumGrains)
			{
				m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.PushAndGrow(lastGrain + 1);
				m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].numValidGrains = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetCount();
				GenerateLoopInfo(m_LoopEditorLoopIndex);
				CalculateMinMaxLoopHz();

				for(s32 submixLoop = 0; submixLoop < m_GranularSubmixes.GetCount(); submixLoop++)
				{
					if(m_GranularSubmixes[submixLoop]->GetSynchronisedLoopIndex() == (s32)m_LoopEditorLoopIndex)
					{
						m_GranularSubmixes[submixLoop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetElements(), m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].numValidGrains, m_LoopEditorLoopIndex);
					}
				}
			}
		}
	}
}

// ----------------------------------------------------------------
// audGranularMix LoopEditorShrinkLoop
// ----------------------------------------------------------------
void audGranularMix::LoopEditorShrinkLoop()
{
	if(m_LoopEditorActive)
	{
		if(m_LoopEditorLoopIndex < (u32)m_SynchronisedLoopDefinitions.GetCount())
		{
			u32 numGrains = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetCount();

			if(numGrains > 1)
			{
				m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.Pop();
				m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].numValidGrains = m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetCount();
				GenerateLoopInfo(m_LoopEditorLoopIndex);
				CalculateMinMaxLoopHz();

				for(s32 submixLoop = 0; submixLoop < m_GranularSubmixes.GetCount(); submixLoop++)
				{
					if(m_GranularSubmixes[submixLoop]->GetSynchronisedLoopIndex() == (s32)m_LoopEditorLoopIndex)
					{
						m_GranularSubmixes[submixLoop]->SetValidGrains(m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].validGrains.GetElements(), m_SynchronisedLoopDefinitions[m_LoopEditorLoopIndex].numValidGrains, m_LoopEditorLoopIndex);
					}
				}
			}
		}
	}
}

// ----------------------------------------------------------------
// audGranularMix LoopEditorCreateLoop
// ----------------------------------------------------------------
void audGranularMix::LoopEditorCreateLoop()
{
	if(m_LoopEditorActive)
	{
		if(m_SynchronisedLoopDefinitions.GetCount() + 1 < kMaxSyncLoops)
		{
			audSyncLoopDefinition newDefinition;
			newDefinition.enabled = true;
			newDefinition.playbackOrder = audGranularSubmix::PlaybackOrderWalk;
			newDefinition.numValidGrains = 0;
			newDefinition.submixID = ATSTRINGHASH("Loop", 0x4C633D07);
			
			for(u32 loop = m_NumGrains/2; loop < (m_NumGrains/2) + 5 && loop < m_NumGrains; loop++)
			{
				newDefinition.validGrains.PushAndGrow(loop);
				newDefinition.numValidGrains++;
			}

			if(newDefinition.numValidGrains > 0)
			{
				m_SynchronisedLoopDefinitions.Push(newDefinition);
				GenerateLoopInfo(m_SynchronisedLoopDefinitions.GetCount() - 1);
				CalculateMinMaxLoopHz();
			}

			if(m_GranularSubmixes.GetCount() == 1)
			{
				for(u32 loop = 0; loop < 2; loop++)
				{
					audGranularSubmix* loopSubmix = rage_aligned_new(16) audGranularSubmix(&m_WaveRef, m_GrainTable, m_NumGrains);
					loopSubmix->SetSubmixType(audGranularSubmix::SubmixTypeSynchronisedLoop);
					m_GranularSubmixes.Push(loopSubmix);
				}
			}
		}
	}
}

// ----------------------------------------------------------------
// audGranularMix LoopEditorDeleteLoop
// ----------------------------------------------------------------
void audGranularMix::LoopEditorDeleteLoop()
{
	if(m_LoopEditorActive)
	{
		if(m_LoopEditorLoopIndex < (u32)m_SynchronisedLoopDefinitions.GetCount())
		{
			m_SynchronisedLoopDefinitions.Delete(m_LoopEditorLoopIndex);
		}
	}
}

// ----------------------------------------------------------------
// audGranularMix addLine
// ----------------------------------------------------------------
void AddLine(fiStream* stream, const char *str)
{
	char lineBuf[128];
	formatf(lineBuf, "%s\r\n", str);
	stream->Write(lineBuf, istrlen(lineBuf));
	//audDisplayf(lineBuf);
}

// ----------------------------------------------------------------
// audGranularMix LoopEditorExportData
// ----------------------------------------------------------------
void audGranularMix::LoopEditorExportData(u32 mixIndex)
{
	char fileName[64];
	char lineBuf[128];

	switch(mixIndex)
	{
	case 0:
		formatf(fileName, "c:\\%s", "ENGINE_ACCEL");
		break;
	case 1:
		formatf(fileName, "c:\\%s", "EXHAUST_ACCEL");
		break;
	case 2:
		formatf(fileName, "c:\\%s", "ENGINE_DECEL");
		break;
	case 3:
		formatf(fileName, "c:\\%s", "EXHAUST_DECEL");
		break;
	case 4:
		formatf(fileName, "c:\\%s", "ENGINE_IDLE");
		break;
	case 5:
		formatf(fileName, "c:\\%s", "EXHAUST_IDLE");
		break;
	}

	audDisplayf("Saving grain data to %s.grn...", fileName);
	fiStream* stream = ASSET.Create(fileName, "grn");

	AddLine(stream, "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
	AddLine(stream, "<audGrainDataStruct>");
	AddLine(stream, "  <grainData>");

	for(u32 loop = 0; loop < m_NumGrains; loop++)
	{
		AddLine(stream, "    <audGrainData>");

		formatf(lineBuf, "      <startSample>%d</startSample>", (u32)(ceilf(m_GrainTable[loop].startSample/m_SampleRateConversionRatio)));
		AddLine(stream, lineBuf);

		formatf(lineBuf, "      <pitch>%f</pitch>", m_GrainTable[loop].pitch);
		AddLine(stream, lineBuf);

		AddLine(stream, "    </audGrainData>");
	}

	AddLine(stream, "  </grainData>");
	AddLine(stream, "  <loopDefinitions>");

	for(s32 loop = 0; loop < m_SynchronisedLoopDefinitions.GetCount(); loop++)
	{
		audSyncLoopDefinition loopDefinition = m_SynchronisedLoopDefinitions[loop];
		AddLine(stream, "    <audLoopDefinition>");

		switch(loopDefinition.playbackOrder)
		{
		case audGranularSubmix::PlaybackOrderMixNMatch:
			AddLine(stream, "      <playbackOrder>Mixed</playbackOrder>");
			break;
		case audGranularSubmix::PlaybackOrderWalk:
			AddLine(stream, "      <playbackOrder>Walk</playbackOrder>");
			break;
		case audGranularSubmix::PlaybackOrderSequential:
			AddLine(stream, "      <playbackOrder>Sequential</playbackOrder>");
			break;
		case audGranularSubmix::PlaybackOrderRandom:
			AddLine(stream, "      <playbackOrder>Random</playbackOrder>");
			break;
		case audGranularSubmix::PlaybackOrderReverse:
			AddLine(stream, "      <playbackOrder>Reverse</playbackOrder>");
			break;
		}

		AddLine(stream, "      <validGrains>");

		for(u32 grainIndex = 0; grainIndex < loopDefinition.numValidGrains; grainIndex++)
		{
			formatf(lineBuf, "        <int>%d</int>", loopDefinition.validGrains[grainIndex]);
			AddLine(stream, lineBuf);
		}

		AddLine(stream, "      </validGrains>");
		AddLine(stream, "      <submixIdentifier>Loop</submixIdentifier>");
		AddLine(stream, "    </audLoopDefinition>");
	}

	AddLine(stream, "  </loopDefinitions>");
	AddLine(stream, "</audGrainDataStruct>");
	stream->Close();
	audDisplayf("...save complete!");
}

// ----------------------------------------------------------------
// audGranularMix SetToneGeneratorEnabled
// ----------------------------------------------------------------
void audGranularMix::SetToneGeneratorEnabled(bool enabled)
{
	for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		m_GranularSubmixes[loop]->SetToneGeneratorEnabled(enabled);
	}
}

// ----------------------------------------------------------------
// audGranularMix DebugDrawGrainTable
// ----------------------------------------------------------------
void audGranularMix::DebugDrawGrainTable(s32 yOffset, f32 xCurrent)
{
	f32 xCoord = 80.0f;
	f32 yCoord = 40.0f + yOffset; 
	f32 wavRenderWidth = 1100.0f;
	f32 wavRenderHeight = 40.0f;

	// Draw alpha'd background
	Color32 bgColour = Color32(100,100,100,200);
	grcBegin(drawTriStrip, 4);
	grcColor(bgColour);
	grcVertex2f(xCoord, yCoord);
	grcVertex2f(xCoord, yCoord + wavRenderHeight);
	grcVertex2f(xCoord + wavRenderWidth, yCoord);
	grcVertex2f(xCoord + wavRenderWidth, yCoord + wavRenderHeight);
	grcEnd();

	if(!m_GrainTable)
	{
		return;
	}

	// Draw grain boundaries
	Vector3 v0,v1,v2,v3;
	v0.z = v1.z = v2.z = v3.z = 0.f;

	Color32 purpleColor = Color_purple;
	purpleColor.SetAlpha(96);

	for(u32 loop = 0; loop < m_NumGrains; loop++)
	{
		f32 grainProportion = m_GrainTable[loop].startSample/(f32)m_LengthSamples;
		v0.x = v1.x = xCoord + (grainProportion * wavRenderWidth);
		v0.y = yCoord;
		v1.y = yCoord + wavRenderHeight;
		grcDrawLine(v0, v1, purpleColor);
	}

	// Draw synchronised loop valid grains
	Color32 validGrainColor = Color_green;

	for(s32 loop = 0; loop < m_SynchronisedLoopDefinitions.GetCount(); loop++)
	{
		if(m_LoopEditorActive && 
		   loop == (s32)m_LoopEditorLoopIndex )
		{
			validGrainColor = Color_red;
			validGrainColor.SetAlpha(96);
		}
		else
		{
			validGrainColor = Color_green;
			validGrainColor.SetAlpha(96);
		}

		for(s32 validGrainLoop = 0; validGrainLoop < m_SynchronisedLoopDefinitions[loop].validGrains.GetCount(); validGrainLoop++)
		{
			u32 validGrainIndex = m_SynchronisedLoopDefinitions[loop].validGrains[validGrainLoop];

			// Draw valid grains
			if(validGrainIndex + 1 < m_NumGrains)
			{
				grcBegin(drawTriStrip, 4);
				grcColor(validGrainColor);
				grcVertex2f(xCoord + ((m_GrainTable[validGrainIndex].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
				grcVertex2f(xCoord + ((m_GrainTable[validGrainIndex].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
				grcVertex2f(xCoord + ((m_GrainTable[validGrainIndex + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
				grcVertex2f(xCoord + ((m_GrainTable[validGrainIndex + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
				grcEnd();
			}
		}
	}

	// Draw default grain position
	s32 playingGrain = 0;
	audGranularSubmix* defaultGranularSubmix = NULL;
	Color32 playingGrainColor = Color_red;

	if(m_GranularSubmixes.GetCount() > 0)
	{
		defaultGranularSubmix = m_GranularSubmixes[0];
	}	
	
	if(defaultGranularSubmix)
	{
		playingGrain = defaultGranularSubmix->GetLastGrainIndex();
		
		// If the default submix is set to a synchronised loop, ignore this as its allowed to play any grain
		if(defaultGranularSubmix && defaultGranularSubmix->GetSubmixType() != audGranularSubmix::SubmixTypeSynchronisedLoop)
		{
			if(defaultGranularSubmix->HasMinMaxGrainLimit())
			{
				for(s32 loop = defaultGranularSubmix->GetMinGrainLimit(); loop <= defaultGranularSubmix->GetMaxGrainLimit(); loop++)
				{
					if(loop >= 0 && loop < ((s32)m_NumGrains) - 1)
					{
						playingGrainColor.SetAlpha((u32)(40.0f * defaultGranularSubmix->GetPrevVolumeScale()));

						grcBegin(drawTriStrip, 4);
						grcColor(playingGrainColor);
						grcVertex2f(xCoord + ((m_GrainTable[loop].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
						grcVertex2f(xCoord + ((m_GrainTable[loop].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
						grcVertex2f(xCoord + ((m_GrainTable[loop + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
						grcVertex2f(xCoord + ((m_GrainTable[loop + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
						grcEnd();
					}
				}
			}
			else
			{
				s32 randomisedCenter = defaultGranularSubmix->GetLastRandomisedGrainCenter();

				// Draw the randomised grain positions slightly faded out
				if(randomisedCenter >= 0)
				{
					for(s32 loop = (s32)(randomisedCenter - defaultGranularSubmix->GetMaxRandomisedGrainBelow()); loop <= (s32)(randomisedCenter + defaultGranularSubmix->GetMaxRandomisedGrainAbove()); loop++)
					{
						if(loop >= 0 && loop < ((s32)m_NumGrains) - 1)
						{
							playingGrainColor.SetAlpha((u32)(40.0f * defaultGranularSubmix->GetPrevVolumeScale()));

							grcBegin(drawTriStrip, 4);
							grcColor(playingGrainColor);
							grcVertex2f(xCoord + ((m_GrainTable[loop].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
							grcVertex2f(xCoord + ((m_GrainTable[loop].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
							grcVertex2f(xCoord + ((m_GrainTable[loop + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
							grcVertex2f(xCoord + ((m_GrainTable[loop + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
							grcEnd();
						}
					}
				}
			}
		}
	}	

#if __PS3
	if(s_DebugDrawVramGrainLoaders)
	{
		Color32 vramLoaderColor = Color_yellow;

		for(u32 loop = 0; loop < kNumGrainVramLoaders; loop++)
		{
			if(loop > 0)
			{
				vramLoaderColor = Color_orange;
			}

			if(m_GrainVramLoaders[loop].vramHelper.QueryNumBytesAvailable() == ~0u)
			{
				vramLoaderColor.SetAlpha(100);
			}
			else
			{
				vramLoaderColor.SetAlpha(20);
			}

			s32 firstValidGrain = m_GrainVramLoaders[loop].firstValidGrainIndex;
			s32 lastValidGrain = m_GrainVramLoaders[loop].lastValidGrainIndex;

			if(firstValidGrain >= 0 && lastValidGrain >= 0)
			{
				grcBegin(drawTriStrip, 4);
				grcColor(vramLoaderColor);
				grcVertex2f(xCoord + ((m_GrainTable[firstValidGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
				grcVertex2f(xCoord + ((m_GrainTable[firstValidGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
				grcVertex2f(xCoord + ((m_GrainTable[lastValidGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
				grcVertex2f(xCoord + ((m_GrainTable[lastValidGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
				grcEnd();
			}
		}
	}

	if(s_DebugDrawVramLoopLoaders)
	{
		Color32 vramLoaderColor = Color_yellow;

		for(u32 loop = 0; loop < kNumLoopVramLoaders; loop++)
		{
			if(m_LoopVramLoaders[loop].synchronisedLoopIndex >= 0)
			{
				if(loop > 0)
				{
					vramLoaderColor = Color_orange;
				}

				if(m_LoopVramLoaders[loop].vramHelper.QueryNumBytesAvailable() == ~0u)
				{
					vramLoaderColor.SetAlpha(100);
				}
				else
				{
					vramLoaderColor.SetAlpha(20);
				}

				s32 firstValidGrain = m_LoopVramLoaders[loop].firstValidGrainIndex;
				s32 lastValidGrain = m_LoopVramLoaders[loop].lastValidGrainIndex;

				if(firstValidGrain >= 0 && lastValidGrain >= 0)
				{
					grcBegin(drawTriStrip, 4);
					grcColor(vramLoaderColor);
					grcVertex2f(xCoord + ((m_GrainTable[firstValidGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
					grcVertex2f(xCoord + ((m_GrainTable[firstValidGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
					grcVertex2f(xCoord + ((m_GrainTable[lastValidGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
					grcVertex2f(xCoord + ((m_GrainTable[lastValidGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
					grcEnd();
				}
			}
		}
	}
#endif

	if(defaultGranularSubmix)
	{
		if(playingGrain >= 0 && playingGrain < ((s32)m_NumGrains) - 1)
		{
			playingGrainColor.SetAlpha((u32)(255.0f * defaultGranularSubmix->GetPrevVolumeScale()));

			grcBegin(drawTriStrip, 4);
			grcColor(playingGrainColor);
			grcVertex2f(xCoord + ((m_GrainTable[playingGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
			grcVertex2f(xCoord + ((m_GrainTable[playingGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
			grcVertex2f(xCoord + ((m_GrainTable[playingGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
			grcVertex2f(xCoord + ((m_GrainTable[playingGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
			grcEnd();
		}
	}

	playingGrainColor = Color_blue;

	// Draw synchronised loop grain positions
	for(s32 loop = 1; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		if(m_GranularSubmixes[loop]->GetSynchronisedLoopIndex() >= 0)
		{
			playingGrainColor.SetAlpha((u32)(255.0f * (m_GranularSubmixes[loop]->GetPrevVolumeScale())));
			playingGrain = m_GranularSubmixes[loop]->GetLastGrainIndex();

			if(playingGrain >= 0 && playingGrain < ((s32)m_NumGrains) - 1)
			{
				grcBegin(drawTriStrip, 4);
				grcColor(playingGrainColor);
				grcVertex2f(xCoord + ((m_GrainTable[playingGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
				grcVertex2f(xCoord + ((m_GrainTable[playingGrain].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
				grcVertex2f(xCoord + ((m_GrainTable[playingGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord);
				grcVertex2f(xCoord + ((m_GrainTable[playingGrain + 1].startSample/(f32)m_LengthSamples) * wavRenderWidth), yCoord + wavRenderHeight);
				grcEnd();
			}
		}
	}

	// Draw the current X playback point
	v0.x = v1.x = xCoord + (xCurrent * wavRenderWidth);
	v0.y = yCoord;
	v1.y = yCoord + wavRenderHeight;
	grcDrawLine(v0, v1, Color_black);
}
#endif

#if !__SPU
// ----------------------------------------------------------------
// Handle a custom command packet
// ----------------------------------------------------------------
void audGranularMix::HandleCustomCommandPacket(const audPcmSourceCustomCommandPacket* packet)
{
	if(packet)
	{
		switch(packet->paramId)
		{
		case audGrainPlayer::Params::SetLoopEnabled:
			{
				if(m_Initialised)
				{
					audGrainPlayer::audGrainPlayerSetLoopEnabledPacket* loopEnabledPacket = (audGrainPlayer::audGrainPlayerSetLoopEnabledPacket*)packet;

					for(s32 loop = 0; loop < m_SynchronisedLoopDefinitions.GetCount(); loop++)
					{
						if(m_SynchronisedLoopDefinitions[loop].submixID == loopEnabledPacket->loopID)
						{
							m_SynchronisedLoopDefinitions[loop].enabled = loopEnabledPacket->loopEnabled;
						}
					}
				}
			}
			break;
		default:
			Assert(0);
			break;
		}
	}
}

// ----------------------------------------------------------------
// Set the sliding window size for the default submix
// ----------------------------------------------------------------
void audGranularMix::SetGranularSlidingWindowSize(const f32 windowSizeHz, const u32 minGrains, const u32 maxGrains)
{
	for (s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		m_GranularSubmixes[loop]->SetGranularSlidingWindowSize(windowSizeHz, minGrains, maxGrains);
	}
}

// ----------------------------------------------------------------
// Set the min repeat rate for the default submix
// ----------------------------------------------------------------
void audGranularMix::SetMinGrainRepeatRate(const u32 rate)
{
	for (s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		m_GranularSubmixes[loop]->SetMinGrainRepeatRate(rate);
	}
}

// ----------------------------------------------------------------
// Set the min/max pitch requirements
// ----------------------------------------------------------------
void audGranularMix::SetMinMaxPitch(const f32 minPitch, const f32 maxPitch)
{
	m_PitchShift = 0.0f;
	m_PitchStretch = 1.0f;
	m_ParentMinPitch = minPitch; m_ParentMaxPitch = maxPitch; 

	if(m_MatchMinPitch || m_MatchMaxPitch)
	{
		f32 localMaxPitch = Max(m_GrainTable[0].pitch, m_GrainTable[m_NumGrains - 1].pitch);
		f32 localMinPitch = Min(m_GrainTable[0].pitch, m_GrainTable[m_NumGrains - 1].pitch);

		if(m_MatchMinPitch)
		{
			f32 shift = localMinPitch - m_ParentMinPitch;
			m_PitchShift += shift;
		}

		if(m_MatchMaxPitch)
		{
			f32 stretch = (m_ParentMaxPitch - localMinPitch)/(localMaxPitch - localMinPitch);
			m_PitchStretch = stretch;
		}

		for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
		{
			m_GranularSubmixes[loop]->SetPitchShiftValues(m_PitchShift, m_PitchStretch);
		}

		for(s32 loopIndex = 0; loopIndex < m_SynchronisedLoopDefinitions.GetCount(); loopIndex++)
		{
			GenerateLoopInfo(loopIndex);
		}

		CalculateMinMaxLoopHz();
	}
}

// ----------------------------------------------------------------
// audGranularMix StartPhys
// ----------------------------------------------------------------
void audGranularMix::StartPhys()
{
	if(!m_IsPlayingPhysically)
	{
		m_LoopGrainVolumeSmoother.Reset();
		m_IsPlayingPhysically = true;

#if __PS3
		ResetVramLoaders();
#endif
	}
}

// ----------------------------------------------------------------
// audGranularMix StopPhys
// ----------------------------------------------------------------
void audGranularMix::StopPhys()
{
	for (s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		m_GranularSubmixes[loop]->StopPhys();
	}

#if __PS3
	ResetVramLoaders();
#endif

	m_IsPlayingPhysically = false;
}
#endif

// ----------------------------------------------------------------
// audGranularMix shutdown
// ----------------------------------------------------------------
void audGranularMix::Shutdown()
{
	for(s32 loop = 0; loop < m_GranularSubmixes.GetCount(); loop++)
	{
		m_GranularSubmixes[loop]->Shutdown();
	}

	m_WaveRef.Shutdown();
}

} // namespace rage
