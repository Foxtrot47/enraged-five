
#include "cmdbuffer.h"
#include "device.h"
#include "driver.h"
#include "pcmsource_interface.h"
#include "submix.h"

#if __SPU
#include "cmdbuffer.cpp"
#endif

namespace rage
{

#if __SPU
	extern audCommandBuffer *g_MixerCommandBuffer;
	extern const PcmSourceState *g_PcmSourceState;
	extern u32 g_PcmSourceStateEA;

	bool audPcmSourceInterface::WriteCommand(const audCommandPacketHeader *packet)
	{	
		Assert(g_MixerCommandBuffer);
		return g_MixerCommandBuffer->WritePacket(packet);
	}

	bool audPcmSourceInterface::WriteAlignedCommand(const audCommandPacketHeader *packet)
	{	
		Assert(g_MixerCommandBuffer);
		return g_MixerCommandBuffer->WriteAlignedPacket(packet);
	}

	bool audPcmSourceInterface::GetState(PcmSourceState& pcmSourceState, const s32 pcmSourceId)
	{
		// SPU sound update jobs can choose not to store PCM state data locally, but instead DMA it over as and when requested. This saves some
		// memory at the expense of the time taken for the DMA operation to complete.
#ifdef AUD_DMA_PCM_SOURCE_STATE_DATA
		const s32 tag = 6;
		Assert((g_PcmSourceStateEA&15)== 0);
		Assert(g_PcmSourceStateEA != 0);
		sysDmaGetAndWait(&pcmSourceState, g_PcmSourceStateEA + pcmSourceId*sizeof(PcmSourceState), sizeof(PcmSourceState), tag);
#else
		pcmSourceState = g_PcmSourceState[pcmSourceId];
#endif
		return true;
	}
#else
	bool audPcmSourceInterface::WriteCommand(const audCommandPacketHeader *packet)
	{
		return audDriver::GetMixer()->WriteCommand(packet);
	}

	bool audPcmSourceInterface::WriteAlignedCommand(const audCommandPacketHeader *packet)
	{
		return audDriver::GetMixer()->WriteAlignedCommand(packet);
	}

	bool audPcmSourceInterface::GetState(PcmSourceState& pcmSourceState, const s32 pcmSourceId)
	{
		PcmSourceState* pcmSourceStatePtr = audDriver::GetVoiceManager().GetPcmSourceState(pcmSourceId);

		if(pcmSourceStatePtr)
		{
			pcmSourceState = *pcmSourceStatePtr;
			return true;
		}

		return false;
	}
#endif

	s32 audPcmSourceInterface::Allocate(const audPcmSourceTypeId typeId)
	{
		FastAssert(g_PcmSourcePool);
		// NOTE: on SPU g_PcmSourcePool is a PPU effective address, not a local store pointer.  Allocate() accesses
		// all members through dma/interlocked functions.
		const u32 allocationId = g_PcmSourcePool->Allocate(audPcmSourceFactory::GetSlotSize(typeId));
		if(allocationId == ~0U)
		{
			audDebugf1("Failed to allocate PCM generator");
			return -1;
		}
		else
		{
			// Ensure the PCM generator state is re-initialised
#if __SPU
			const s32 tag = 5;
			static u32 zeroBuffer[4]  = {0,0,0,0};
			CompileTimeAssert(sizeof(zeroBuffer) == sizeof(PcmSourceState));
			Assert((g_PcmSourceStateEA&15)== 0);
			Assert(g_PcmSourceStateEA != 0);

			sysDmaPut(&zeroBuffer, g_PcmSourceStateEA + allocationId*sizeof(PcmSourceState), sizeof(PcmSourceState), tag);
#else
			sysMemSet(audDriver::GetVoiceManager().GetPcmSourceState(allocationId), 0, sizeof(PcmSourceState));
#endif

			// Defer initialization to the mixer thread
			audInitialisePcmSourceCommand allocatePacket;
			allocatePacket.objectId = allocationId;
			allocatePacket.TypeId = typeId;
			WriteCommand(&allocatePacket);

			return static_cast<s32>(allocationId);
		}
	}

	void audPcmSourceInterface::AddRef(const s32 pcmSourceId)
	{
		// NOTE: on SPU g_PcmSourcePool is a PPU EA, rather than an LS pointer, however addref/removeref use
		// interlocked operations rather than accessing any members directly
		g_PcmSourcePool->AddRef(pcmSourceId);
	}

	void audPcmSourceInterface::Release(const s32 pcmSourceId)
	{
		// NOTE: on SPU g_PcmSourcePool is a PPU EA, rather than an LS pointer, however addref/removeref use
		// interlocked operations rather than accessing any members directly
		g_PcmSourcePool->RemoveRef(pcmSourceId);
	}

	bool audPcmSourceInterface::SetParam(const u32 pcmSourceId, const u32 paramId, const f32 val)
	{
		FastAssert(pcmSourceId < kMaxPcmSources);

		audSetPcmSourceF32ParamCommand command;
		command.objectId = pcmSourceId;
		command.paramId = paramId;
		command.systemId = audMixerDevice::COMMAND_PCMSOURCE;
		command.value = val;
		return Verifyf(WriteCommand(&command), "Mixer command buffer full");		
	}

	bool audPcmSourceInterface::SetParam(const u32 pcmSourceId, const u32 paramId, const u32 val)
	{
		FastAssert(pcmSourceId < kMaxPcmSources);

		audSetPcmSourceU32ParamCommand command;
		command.objectId = pcmSourceId;
		command.paramId = paramId;
		command.systemId = audMixerDevice::COMMAND_PCMSOURCE;
		command.value = val;
		return Verifyf(WriteCommand(&command), "Mixer command buffer full");	
	}

	bool audPcmSourceInterface::WriteGenericCommand(const s32 pcmSourceId, const u32 commandId)
	{
		FastAssert(pcmSourceId < kMaxPcmSources);
		audCommandPacketHeader command;
		command.objectId = (u32)pcmSourceId;
		command.packetSize = sizeof(audCommandPacketHeader);
		command.commandId = commandId;
		command.systemId = audMixerDevice::COMMAND_PCMSOURCE;
		return Verifyf(WriteCommand(&command), "Mixer command buffer full");
	}

	bool audPcmSourceInterface::WriteCustomCommandPacket(const u32 pcmSourceID, const u32 paramId, audPcmSourceCustomCommandPacket *command)
	{
		FastAssert(pcmSourceID < kMaxPcmSources);
		command->objectId = pcmSourceID;
		command->paramId = paramId;
		command->systemId = audMixerDevice::COMMAND_PCMSOURCE;
		return Verifyf(WriteCommand(command), "Mixer command buffer full");
	}

	void audPcmSourceInterface::Start(const s32 pcmSourceId)
	{
		FastAssert(pcmSourceId < kMaxPcmSources);
		WriteGenericCommand(pcmSourceId, audPcmSourceInterface::START_PCM_SOURCE);
	}

	void audPcmSourceInterface::StopAndRelease(const s32 pcmSourceId)
	{
		FastAssert(pcmSourceId < kMaxPcmSources);
		WriteGenericCommand(pcmSourceId, audPcmSourceInterface::STOP_AND_RELEASE);
	}

	bool audPcmSourceInterface::SetSubmixOutputVolumes(const s32 submixId, const u32 outputIndex, Vec::Vector_4V_In packedVolumes)
	{
		audSetSubmixVolumesCommand command;
		command.index = outputIndex;

		command.packedVolumes = packedVolumes;
		command.objectId = submixId;
		command.systemId = audMixerDevice::COMMAND_SUBMIX;
		return WriteAlignedCommand(&command);
	}

}

