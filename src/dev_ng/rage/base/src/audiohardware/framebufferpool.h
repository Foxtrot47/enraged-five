// 
// audiohardware/framebufferpool.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOHARDWARE_FRAMEBUFFERPOOL_H 
#define AUDIOHARDWARE_FRAMEBUFFERPOOL_H 

#include "atl/array.h"
#include "system/memops.h"

#include "audiohardware/channel.h"
#include "mixer.h"
#include "vectormath/vectormath.h"

namespace rage {

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) //  structure was padded due to __declspec(align())
#endif

	// PURPOSE
	//	Implements a simple fixed-size pool of sample frame-sized buffers.  Each buffer is cache-line aligned; allocate and free
	//	are O(1).
	// NOTES
	//	Allocate() and Free() are not thread safe - this class is expected to be used from  the mixer thread only.
	// SEE ALSO
	//	audFrameBufferCache
	class ALIGNAS(16) audFrameBufferPool
	{
	public:

		enum { InvalidId = 0xFFFF };

#if !__SPU
		void Init(const u32 numBuffers)
		{
			m_NumAllocated = 0;
			m_NumBuffers = numBuffers;
			Assert(numBuffers <= kMaxBuffers);

			// Initialize free list
			m_FirstFreeBuffer = 0;
			m_BufferFreeList.Resize(numBuffers);
			for(u32 i = 0; i < numBuffers; i++)
			{
				Assign(m_BufferFreeList[i], i+1);
			}
			
			// cache-aligned storage
			m_Data = rage_aligned_new(128) f32[kMixBufNumSamples * numBuffers];
			sysMemSet128(m_Data, 0, kMixBufNumSamples * numBuffers * sizeof(f32));

#if __BANK
			sm_AllocationError = 0;
#endif
		}

		void Shutdown()
		{
			m_BufferFreeList.Reset();
			delete[] m_Data;
			m_Data = NULL;
		}

		// PURPOSE
		//	Fills the specified buffer with zeros
		// NOTES
		//	On PPU/Xenon this function should be used to invalidate the buffer cache before overwriting
		//	to prevent unnecessary L2 misses.
		void ZeroBuffer(const u32 bufferId)
		{
			if (bufferId >= InvalidId)
				return;
			TrapGE(bufferId, m_NumBuffers);
			ZeroBufferImpl(reinterpret_cast<Vec::Vector_4V*>(GetBuffer(bufferId)));
		}

		// PURPOSE
		//	Returns a pointer to the requested buffer
		f32 *GetBuffer(const u32 index) const
		{
			if (index >= InvalidId)
				return NULL;
			TrapGE(index, m_NumBuffers);
			return m_Data + (index * kMixBufNumSamples);
		}

		const f32 *GetBuffer_ReadOnly(const u32 index) const
		{
			return GetBuffer(index);
		}
#else
		// PURPOSE
		//	Returns the effective address in shared memory of the requested buffer
		u32 GetBufferEA(const u32 index) const
		{
			TrapGE(index, m_NumBuffers);
			return (u32)(m_Data + (index * kMixBufNumSamples));
		}
#endif

		// PURPOSE
		//	Allocates a buffer from the pool
		// RETURNS
		//	The buffer id, ~0U if none are available
		// NOTES
		//	Has O(1) runtime.
		//	This is not thread safe - all buffer allocation/freeing should occur on the mixer thread
		u32 Allocate()
		{
			const u32 firstFree = m_FirstFreeBuffer;

			if (BANK_ONLY(!sm_DisableFrameBufferAllocations &&) audVerifyf(firstFree < m_NumBuffers, "Out of free buffers"))
			{
				m_FirstFreeBuffer = m_BufferFreeList[firstFree];
				m_NumAllocated++;
#if __BANK
				sm_AllocationError = 0;
#endif
				return firstFree;
			}
			else
			{
#if __BANK
				sm_AllocationError++;
				if (sm_AllocationError < 500)
				{
					audWarningf("Out of audFrameBufferPool buffers! Hit %d times", sm_AllocationError);
				}
				else if (sm_AllocationError > 2000)
				{
					sm_AllocationError = 0;
				}
#endif
				return audFrameBufferPool::InvalidId; // This used to be ~0U, but the game code uses u16s so I think it's better to return 0xffff as our invalid id because thats the true value.
			}
		}

		// PURPOSE
		//	Adds the specified buffer back into the free pool
		// NOTES
		//	Has O(1) runtime.
		//	This is not thread safe - all buffer allocation/freeing should occur on the mixer thread.  Also note
		//	that this function cannot protect against double-frees.
		void Free(const u32 index)
		{
			if (index >= InvalidId)
				return;
			TrapGE(index, m_NumBuffers);
			TrapEQ(m_NumAllocated, 0);
			const u32 firstFree = m_FirstFreeBuffer;
			Assign(m_BufferFreeList[index], firstFree);
			m_FirstFreeBuffer = index;
			m_NumAllocated--;
		}

		// PURPOSE
		//	Returns the number of buffers that have been allocated from the pool
		u32 GetNumBuffersAllocated() { return m_NumAllocated; }
		
		// PURPOSE
		//	Returns the number of buffers available in the pool
		u32 GetNumBuffersFree() { return m_NumBuffers - GetNumBuffersAllocated(); }

		// PURPOSE
		//	Returns the number of buffers in the pool.
		u32 GetNumBuffers() const { return m_NumBuffers; }

		// Hard-coded maximum to simplify SPU code (fixed array so other than the actual pool storage
		// audFrameBufferPool is entirely self contained).
#if RSG_PC || RSG_ORBIS || RSG_DURANGO
		enum {kMaxBuffers = 1024};
#else
		enum {kMaxBuffers = 512};
#endif
	private:

		static void ZeroBufferImpl(Vec::Vector_4V *RESTRICT outPtr)
		{
#if RSG_SPU || RSG_CPU_X64
			u32 count = kMixBufNumSamples >> 5;
			const Vec::Vector_4V z = Vec::V4VConstant(V_ZERO);
			while(count--)
			{
				*outPtr++ = z;
				*outPtr++ = z;
				*outPtr++ = z;
				*outPtr++ = z;
				*outPtr++ = z;
				*outPtr++ = z;
				*outPtr++ = z;
				*outPtr++ = z;
			}
#else
			sysMemSet128(outPtr, 0, sizeof(float) * kMixBufNumSamples);
#endif
		}
		atFixedArray<u16, kMaxBuffers> m_BufferFreeList;
		u32 m_NumAllocated;
		u32 m_FirstFreeBuffer;
		
		u32 m_NumBuffers;
		f32 *m_Data;

#if __BANK
		public:
			static bool sm_DisableFrameBufferAllocations;
		private:
			static s32 sm_AllocationError;
#endif

	};

#if __WIN32
#pragma warning(pop)
#endif

	extern audFrameBufferPool *g_FrameBufferPool;
} // namespace rage

#endif // AUDIOHARDWARE_FRAMEBUFFERPOOL_H 
