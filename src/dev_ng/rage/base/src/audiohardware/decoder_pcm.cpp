//
// audiohardware/decoder_pcm.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "decoder_pcm.h"
#include "decodemgr.h"

#include "system/endian.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;
	bool audDecoderPcm::Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 UNUSED_PARAM(sampleRate))
	{
		m_NumChannels = numChannels;
		m_Format = format;
		m_State = audDecoder::IDLE;
		return true;
	}

	void audDecoderPcm::Shutdown()
	{

	}

	void audDecoderPcm::SubmitPacket(audPacket &packet)
	{
		m_State = audDecoder::DECODING;
		const bool isBigEndian = __BE;
		switch(m_Format)
		{
		case audWaveFormat::kPcm32FBigEndian:
			Convert<!isBigEndian>((u32*)packet.inputData, packet.inputBytes>>2);
			break;
		case audWaveFormat::kPcm32FLittleEndian:
			Convert<isBigEndian>((u32*)packet.inputData, packet.inputBytes>>2);
			break;
		case audWaveFormat::kPcm16bitBigEndian:
			Convert<!isBigEndian>((s16*)packet.inputData, packet.inputBytes>>1);
			break;
		case audWaveFormat::kPcm16bitLittleEndian:
			Convert<isBigEndian>((s16*)packet.inputData, packet.inputBytes>>1);
			break;
		default:
			Assert(0);
		}
		m_State = audDecoder::IDLE;
	}

	template<const bool swapEndian> void audDecoderPcm::Convert(const u32 *input, const u32 numSamples)
	{
		Assert(!(((size_t)input)&15));
		Assert(!(numSamples&7));
		Assert(!(numSamples&(kPcmDecoderWorkingBufferSamples-1)));

#if RSG_CPU_INTEL
		ALIGNAS(16) u32 samples0Temp[4];
		ALIGNAS(16) u32 samples1Temp[4];
		// 32767
		static const Vector_4V rangeScalar = V4VConstant<0x46FFFE00,0x46FFFE00,0x46FFFE00,0x46FFFE00>();
#endif

		CompileTimeAssert(kPcmDecoderWorkingBufferSamples == 128);
		for(u32 pass = 0; pass < numSamples>>7; pass++)
		{
			// decode in 128 sample chunks
			s16 *output = m_WorkingBuffer;

			for(u32 i = 0; i < numSamples; i += 8)
			{
	#if RSG_CPU_INTEL
				// no byte permute support in vector pipeline so endian swap manually before loading
				if(swapEndian)
				{
					samples0Temp[0] = sysEndian::Swap(input[i+0]);
					samples0Temp[1] = sysEndian::Swap(input[i+1]);
					samples0Temp[2] = sysEndian::Swap(input[i+2]);
					samples0Temp[3] = sysEndian::Swap(input[i+3]);
					samples1Temp[0] = sysEndian::Swap(input[i+0+4]);
					samples1Temp[1] = sysEndian::Swap(input[i+1+4]);
					samples1Temp[2] = sysEndian::Swap(input[i+2+4]);
					samples1Temp[3] = sysEndian::Swap(input[i+3+4]);
				}
				Vector_4V samples0 = *(Vector_4V*)((swapEndian ? &samples0Temp[0] : &input[i]));
				Vector_4V samples1 = *(Vector_4V*)((swapEndian ? &samples1Temp[0] : &input[i+4]));

				// Float to int is only efficient when exponent == 0 on PC
				// scale then convert
				// NOTE: V4PackSignedIntToSignedShort() will saturate
				const Vector_4V samplesInt0 = V4FloatToIntRaw<0>(V4Scale(samples0, rangeScalar));
				const Vector_4V samplesInt1 = V4FloatToIntRaw<0>(V4Scale(samples1, rangeScalar));
	#else
				// PS3/Xenon support byte permutes so we can do the endian swap in vector pipeline
				Vector_4V samples0 = *(Vector_4V*)((float*)&input[i]);
				Vector_4V samples1 = *(Vector_4V*)((float*)&input[i+4]);
				if(swapEndian)
				{
					samples0 = V4BytePermute<3,2,1,0,7,6,5,4,11,10,9,8,15,14,13,12>(samples0);
					samples1 = V4BytePermute<3,2,1,0,7,6,5,4,11,10,9,8,15,14,13,12>(samples1);
				}

				const Vector_4V samplesInt0 = V4FloatToIntRaw<15>(samples0);
				const Vector_4V samplesInt1 = V4FloatToIntRaw<15>(samples1);
	#endif

				const Vector_4V samplesOut = V4PackSignedIntToSignedShort(samplesInt0, samplesInt1);

				*(Vector_4V*)(&output[i]) = samplesOut;
			}
			// write one blocks worth of decoded data to the output buffer
			m_RingBuffer.WriteData(m_WorkingBuffer, kPcmDecoderWorkingBufferSamples<<1);
		}
	}

	template<const bool swapEndian> void audDecoderPcm::Convert(const s16 *input, const u32 numSamples)
	{
		Assert(!(((size_t)input)&15));
		Assert(!(numSamples&7));
		Assert(!(numSamples&(kPcmDecoderWorkingBufferSamples-1)));

		CompileTimeAssert(kPcmDecoderWorkingBufferSamples == 128);
		for(u32 pass = 0; pass < numSamples>>7; pass++)
		{
			// decode in 128 sample chunks
			s16 *output = m_WorkingBuffer;

			for(u32 i = 0; i < numSamples; i += 8)
			{
	#if RSG_CPU_INTEL
				// no byte permute support on PC
				ALIGNAS(16) s16 tempSamples[8];
				if(swapEndian)
				{
					tempSamples[0] = sysEndian::Swap(input[i+0]);
					tempSamples[1] = sysEndian::Swap(input[i+1]);
					tempSamples[2] = sysEndian::Swap(input[i+2]);
					tempSamples[3] = sysEndian::Swap(input[i+3]);
					tempSamples[4] = sysEndian::Swap(input[i+4]);
					tempSamples[5] = sysEndian::Swap(input[i+5]);
					tempSamples[6] = sysEndian::Swap(input[i+6]);
					tempSamples[7] = sysEndian::Swap(input[i+7]);
				}
				Vector_4V samples = *(Vector_4V*)((swapEndian ? &tempSamples[0] : &input[i]));
	#else
				// PS3/Xenon has byte permutation support so we can endian swap in the vector pipeline
				Vector_4V samples = *(Vector_4V*)(&input[i]);		
				if(swapEndian)
				{
					samples = V4BytePermute<1,0,3,2,5,4,7,6,9,8,11,10,13,12,15,14>(samples);
				}
	#endif
				
				*(Vector_4V*)(&output[i]) = samples;
			}
			// write one blocks worth of decoded data to the output buffer
			m_RingBuffer.WriteData(m_WorkingBuffer, kPcmDecoderWorkingBufferSamples<<1);
		}
	}

	u32 audDecoderPcm::QueryNumAvailableSamples() const
	{
		return (m_RingBuffer.GetBytesAvailableToRead()>>1);
	}

	bool audDecoderPcm::QueryReadyForMoreData() const
	{
		return (m_RingBuffer.GetBytesAvailableToRead() < m_RingBuffer.GetBufferSize());
	}

	u32 audDecoderPcm::ReadData(s16 *buf, const u32 numSamples) const
	{
		return m_RingBuffer.PeakData(buf, numSamples<<1)>>1;
	}

	void audDecoderPcm::AdvanceReadPtr(const u32 numSamples)
	{
		m_RingBuffer.AdvanceReadPtr(numSamples<<1);
	}

} // namespace rage
