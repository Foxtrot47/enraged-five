// 
// audiohardware/ringbuffer.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_RINGBUFFER_H
#define AUD_RINGBUFFER_H

#include "audiohardware/channel.h"
#include "math/amath.h"
#include "system/cellsyncmutex.h"
#include "system/criticalsection.h"
#include "system/memory.h"

namespace rage
{

//
// PURPOSE
//	This class implements a thread safe ringbuffer (if m_NeedLock is false it supports only single-threaded access).
//
class audRingBuffer
{
public:
	//
	// PURPOSE
	//	Initialises ring buffer state
	// PARAMS
	//	numBytes - The number of bytes for the entire buffer.
	//
	audRingBuffer()
	{
		m_Data = NULL;
		m_State.ReadOffset = 0;
		m_State.WriteOffset = 0;
		m_State.BufferedBytes = 0;

		m_NeedLock = false;
		m_DisableWarnings = false;
	}

	bool Init(u8 *data, const size_t bufferSizeBytes, bool needLock)
	{
		m_NeedLock = needLock;

#if __PPU
		if(needLock)
		{
			rageCellSyncMutexInitialize(&m_State.Sema);
		}
#endif

		m_Data = data;
		m_BufferSize = u32(bufferSizeBytes);

		sysMemSet(m_Data, 0, m_BufferSize);

		return true;
	}

	void Shutdown()
	{
		m_Data = NULL;
		m_BufferSize = 0;
	}

	//
	// PURPOSE
	//	Writes data into the buffer.
	// PARAMS
	//	src			- A pointer to the data to write.
	//	numBytes	- The number of bytes to write, must be less than or equal to the size
	//					of the ring buffer.
	// RETURNS
	//	The number of bytes written.
	//
	u32 WriteData(const void *src, u32 numBytes)
	{
		const void *ptr1, *ptr2;
		u32 size1, size2;

		GetBufferPointers(m_State.WriteOffset, numBytes, ptr1, size1, ptr2, size2);

		sysMemCpy(const_cast<void*>(ptr1), src, size1);
		if(ptr2)
		{
			sysMemCpy(const_cast<void*>(ptr2), ((u8*)src + size1), size2);
		}

		{
			if(m_NeedLock)
			{
				Lock();
			}
#if __DEV
			if(!m_DisableWarnings && numBytes > m_BufferSize - m_State.BufferedBytes)
			{
				audWarningf("audRingBuffer: overwriting unread data");
			}
#endif
			m_State.WriteOffset += (size1 + size2);
			m_State.WriteOffset %= m_BufferSize;
			m_State.BufferedBytes += (size1 + size2);

			if(m_NeedLock)
			{
				Unlock();
			}
		}

		return size1 + size2;
	}


	//
	// PURPOSE
	//	Reads data from the buffer.
	// PARAMS
	//	dest		- The memory location to write the data.
	//	numBytes	- The number of bytes to write, must be less than or equal to the size
	//					of the ring buffer.
	// RETURNS
	//	The number of bytes read.
	//

	u32 ReadData(void *dest, u32 numBytes)
	{
		const void *ptr1, *ptr2;
		u32 size1, size2;

		GetBufferPointers(m_State.ReadOffset, numBytes, ptr1, size1, ptr2, size2);

		sysMemCpy(dest, ptr1, size1);
		if(ptr2)
		{
			sysMemCpy(((char*)dest + size1), ptr2, size2);
		}

		{
			if(m_NeedLock)
			{
				Lock();
			}
#if __DEV
			if(!m_DisableWarnings && numBytes > m_State.BufferedBytes)
			{
				audWarningf("audRingBuffer: reading unwritten data");
			}
#endif
			m_State.ReadOffset += (size1 + size2);
			m_State.ReadOffset %= m_BufferSize;
			m_State.BufferedBytes -= (size1 + size2);
			if(m_NeedLock)
			{
				Unlock();
			}
		}
		return size1 + size2;
	}

	u32 PeakData(void *dest, u32 numBytes, u32 readOffset = 0) const
	{
		const void *ptr1, *ptr2;
		u32 size1, size2;

		GetBufferPointers((m_State.ReadOffset + readOffset) % m_BufferSize, numBytes, ptr1, size1, ptr2, size2);

		sysMemCpy(dest, ptr1, size1);
		if(ptr2)
		{
			sysMemCpy(((char*)dest + size1), ptr2, size2);
		}

#if __DEV
		if(!m_DisableWarnings && numBytes + readOffset > m_State.BufferedBytes)
		{
			audWarningf("audRingBuffer: reading unwritten data");
		}
#endif
		return size1 + size2;
	}

	void AdvanceReadPtr(const u32 numBytes
#if __SPU
		, const u32 bufferSize
#endif
		)
	{
#if __SPU
		// 'this' is EA
		Lock();

		u32 readOffset = sysDmaGetUInt32((uint64_t)&m_State.ReadOffset, 7);
		u32 bufferedBytes = sysDmaGetUInt32((uint64_t)&m_State.BufferedBytes, 7);
		readOffset += numBytes;
		readOffset %= bufferSize;
		bufferedBytes -= numBytes;
		sysDmaPutUInt32(readOffset, (uint64_t)&m_State.ReadOffset, 7);
		sysDmaPutUInt32(bufferedBytes, (uint64_t)&m_State.BufferedBytes, 7);

		Unlock();
#else
		if(m_NeedLock)
		{
			Lock();
		}

		audAssert(numBytes <= m_State.BufferedBytes);
		m_State.ReadOffset += numBytes;
		m_State.ReadOffset %= m_BufferSize;
		m_State.BufferedBytes -= numBytes;
		if(m_NeedLock)
		{
			Unlock();
		}
#endif
	}

	void Reset()
	{
		if(m_NeedLock)
		{
			Lock();
		}

		sysMemSet(m_Data, 0, m_BufferSize);

		m_State.ReadOffset = 0;
		m_State.WriteOffset = 0;
		m_State.BufferedBytes = 0;

		if(m_NeedLock)
		{
			Unlock();
		}
	}

	void Lock()
	{
#if __PPU
		rageCellSyncMutexLock(&m_State.Sema);
#elif __SPU
		rageCellSyncMutexLock((u32)&m_State.Sema);
#else
		m_State.CS.Lock();
#endif
	}

	void Unlock()
	{
#if __PPU
		rageCellSyncMutexUnlock(&m_State.Sema);
#elif __SPU
		rageCellSyncMutexUnlock((u32)&m_State.Sema);
#else
		m_State.CS.Unlock();
#endif
	}
	
	//
	// PURPOSE
	//	Returns the size of the buffer, in bytes.
	// RETURNS
	//	The size of the buffer, in bytes.
	//
	__inline u32 GetBufferSize() const
	{
		return m_BufferSize;
	}
	//
	// PURPOSE
	//	Returns the number of bytes between the write offset and the read offset.
	// RETURNS
	//	The number of bytes between the write offset and the read offset.
	//
	__inline u32 GetBytesAvailableToRead() const
	{
		return m_State.BufferedBytes;
	}

	__inline u32 GetBytesAvailableToWrite() const
	{
		return GetBufferSize() - GetBytesAvailableToRead();
	}

	__inline const u8 *GetBufferPtr() const
	{
		return m_Data;
	}
	
	struct State
	{
		volatile u32 ReadOffset;
		volatile u32 WriteOffset;
		volatile u32 BufferedBytes;

#if __PS3
		RageCellSyncMutex Sema;
#else
		sysCriticalSectionToken CS;
#endif

	};

	State *GetStatePtr() { return &m_State; }

	void DisableWarnings()
	{
		m_DisableWarnings = true;
	}

	bool IsFull() const { return (m_BufferSize == m_State.BufferedBytes); }

private:

	//
	// PURPOSE
	//	Returns the buffer points for read/write.
	// PARAMS
	//	offset	- The start offset within the buffer
	//	size	- number of bytes requested, must be less than or equal to the size of the buffer
	//	ptr1	- Used to return the first buffer to read to/write from.
	//	size1	- Used to return the number of bytes to read/write to/from ptr1.
	//	ptr2	- Used to return the second buffer to read to/write from.
	//	size2	- Used to return the size of second buffer.
	//
	void GetBufferPointers(u32 offset, u32 size, const void *&ptr1, u32 &size1, const void *&ptr2, u32 &size2) const
	{
		Assert(size <= m_BufferSize);
		Assert(offset <= m_BufferSize);

		size1 = Min(size, m_BufferSize - offset);
		ptr1 = m_Data + offset;

		if(size1 < size)
		{
			ptr2 = m_Data;
			size2 = size - size1;
		}
		else
		{
			size2 = 0;
			ptr2 = NULL;
		}
	}

	u8 *m_Data;
	u32 m_BufferSize;



	State m_State;


	bool m_DisableWarnings;
	bool m_NeedLock;
};

template<const u32 bufferBytes, const bool needLock = false> class audStaticRingBuffer : public audRingBuffer
{
public:
	audStaticRingBuffer() : audRingBuffer()
	{
		Init(m_Storage, bufferBytes, needLock);
	}
private:
	u8 m_Storage[bufferBytes];
};

class audReferencedRingBuffer : public audRingBuffer
{
public:
	audReferencedRingBuffer() 
		: m_RefCount(1)
	{

	}

	u32 AddRef()
	{
		return sysInterlockedIncrement(&m_RefCount);
	}

	u32 Release()
	{
		const u32 newCount = sysInterlockedDecrement(&m_RefCount);
		if(newCount == 0)
		{
			delete[] GetBufferPtr();
			Shutdown();			
			delete this;
		}
		return newCount;
	}

	u32 GetRefCount()
	{
		return sysInterlockedRead(&m_RefCount);
	}

private:
	volatile u32 m_RefCount;
};
} // namespace rage

#endif // AUD_RINGBUFFER_H

