// 
// audiohardware/submix.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __AUD_STRIP_CMD_BUFFER
#include "cmdbuffer.h"
#endif // __AUD_STRIP_CMD_BUFFER

#if !__SPU
#include "device.h"
#include "driver.h"
#include "file/asset.h"
#include "file/stream.h"
#else
#include "system/dma.h"
#include "audioengine/spuutil.h"
#endif

#include "connection.h"
#include "mixervoice.h"
#include "submix.h"
#include "submixcache.h"
#include "profile/profiler.h"
#include "system/memops.h"

#include "mixing_vmath.inl"

#if __DEV && !__SPU
#include "file/asset.h"
#include "file/stream.h"
#endif

EXT_PF_GROUP(MixerDevice);
PF_TIMER(SubmixMix, MixerDevice);
PF_TIMER(SubmixMixIntoSubmix, MixerDevice);
PF_TIMER(SubmixProcessDSP, MixerDevice);
PF_TIMER(SubmixConvertBufferToFormat, MixerDevice);

namespace rage
{
	extern bool g_MeterSubmixes;
#if __BANK && !__SPU
	extern bool g_DisableSubmixEffects;
#endif

#ifndef __AUD_STRIP_CMD_BUFFER
	
	struct audSetSubmixEffectCommand : public audCommandPacketHeader
	{
		audSetSubmixEffectCommand()
		{
			packetSize = sizeof(audSetSubmixEffectCommand);
			commandId = audMixerSubmix::SET_EFFECT;
		}
		audDspEffect *effect;
		u32 channelMask;
		s8 slotIndex;
	};

	struct audAddSubmixOutputCommand : public audCommandPacketHeader
	{
		audAddSubmixOutputCommand()
		{
			packetSize = sizeof(audAddSubmixOutputCommand);
			commandId = audMixerSubmix::ADD_OUTPUT;
			allocateVolumeMatrices = true;
			recomputeProcessingGraph = true;
		}

		u32 submixIndex;
		bool allocateVolumeMatrices;
		bool recomputeProcessingGraph;
	};

	struct audDisconnectSubmixOutputCommand : public audCommandPacketHeader
	{
		audDisconnectSubmixOutputCommand()
		{
			packetSize = sizeof(audDisconnectSubmixOutputCommand);
			commandId = audMixerSubmix::DISCONNECT_OUTPUT;
			recomputeProcessingGraph = true;
		}

		s32 submixIndex;
		bool recomputeProcessingGraph;
	};

	struct audSetSubmixFlagCommand : public audCommandPacketHeader
	{
		audSetSubmixFlagCommand()
		{
			packetSize = sizeof(audSetSubmixFlagCommand);
			commandId = audMixerSubmix::SET_FLAG;
		}
		audMixerSubmix::SubmixFlag flag;
		bool state;
	};


	struct audSetEffectU32ParamCommand : public audCommandPacketHeader
	{
		audSetEffectU32ParamCommand()
		{
			packetSize = sizeof(audSetEffectU32ParamCommand);
			commandId = audMixerSubmix::SET_EFFECT_PARAM_U32;
		}
	
		u32 paramId;
		u32 value;
		s8 slotIndex;
	};

	struct audSetEffectF32ParamCommand : public audCommandPacketHeader
	{
		audSetEffectF32ParamCommand()
		{
			packetSize = sizeof(audSetEffectF32ParamCommand);
			commandId = audMixerSubmix::SET_EFFECT_PARAM_F32;
		}

		u32 paramId;
		f32 value;
		s8 slotIndex;
	};


	void audMixerSubmix::ExecuteCommand(const audCommandPacketHeader *packet)
	{
		switch(packet->commandId)
		{
		case SET_VOLUMES:
			{
				const audSetSubmixVolumesCommand *command = (audSetSubmixVolumesCommand*)packet;
				SetOutputVolumesInternal(command->index, command->packedVolumes);
			}
			break;
		case SET_EFFECT:
			{
				audSetSubmixEffectCommand *command = (audSetSubmixEffectCommand*)packet;
				SetEffectInternal(command->slotIndex, command->effect, command->channelMask);
			}
			break;
		case ADD_OUTPUT:
			{
				audAddSubmixOutputCommand *command = (audAddSubmixOutputCommand*)packet;
				audMixerSubmix* outputSubmix = audDriver::GetMixer()->GetSubmixFromIndex(command->submixIndex);
				AddOutputInternal(outputSubmix, command->allocateVolumeMatrices, command->recomputeProcessingGraph);
			}
			break;
		case SET_EFFECT_PARAM_U32:
			{
				audSetEffectU32ParamCommand *command = (audSetEffectU32ParamCommand*)packet;
				if(m_Effects[command->slotIndex])
				{
					m_Effects[command->slotIndex]->SetParam(command->paramId, command->value);
				}
			}
			break;
		case SET_EFFECT_PARAM_F32:
			{
				audSetEffectF32ParamCommand *command = (audSetEffectF32ParamCommand*)packet;
				if(m_Effects[command->slotIndex])
				{
					m_Effects[command->slotIndex]->SetParam(command->paramId, command->value);
				}
			}
			break;
		case SET_FLAG:
			{
				audSetSubmixFlagCommand *command = (audSetSubmixFlagCommand*)packet;
				SetFlagInternal(command->flag, command->state);
			}
			break;
		case DISCONNECT_OUTPUT:
			{
				audDisconnectSubmixOutputCommand *command = (audDisconnectSubmixOutputCommand*)packet;
				DisconnectOutputInternal(command->submixIndex, command->recomputeProcessingGraph);
			}
			break;
		default:
			Assert(0);
			break;
		}
	}

	void audMixerSubmix::SetOutputVolumesInternal(const u32 index, Vector_4V_In packedVolumes)
	{
		if(index < m_NumSubmixOutputs)
		{
			Assert(m_OutputConnectionIds[index] != 0xffff);
			audMixerConnection *connection = audMixerConnection::GetConnectionFromId(m_OutputConnectionIds[index]);
			if(AssertVerify(connection->HasVolumeMatrix()))
			{
				connection->SetCurrentVolumeMatrixPacked(packedVolumes);
			}
		}
	}

	void audMixerSubmix::SetEffectInternal(const s32 slotId, audDspEffect *effect, const u32 channelMask)
	{
		audAssertf(slotId < m_Effects.GetMaxCount(),"Invalid effect slotId: %d", slotId);	
		if(m_Effects[slotId])
		{
			m_Effects[slotId]->Shutdown();
			m_Effects[slotId] = NULL;

#if AUD_SUPPORT_METERING
			m_EffectType[slotId] = AUD_NUM_DSPEFFECTS;
#endif // AUD_SUPPORT_METERING
		}
		if(effect)
		{
			effect->Init(m_NumChannels, channelMask);
			m_Effects[slotId] = effect;
			m_EffectSizes[slotId] = effect->GetSize();

#if AUD_SUPPORT_METERING
			m_EffectType[slotId] = effect->GetTypeId();
#endif // AUD_SUPPORT_METERING
		}
	}

	void audMixerSubmix::SetFlagInternal(const SubmixFlag flag, const bool state)
	{
		switch(flag)
		{
		case InterleavedProcessing:
			m_BufferFormat = state ? Interleaved : Noninterleaved;
			break;
		case ProcessAsSourceEffect:
			m_SourceEffectSubmix = state;
			break;
		case ComputeRMS:
			m_ComputeRMS = state;
			break;
		case ComputePeakAmplitude:
			m_ComputePeakAmplitude = state;
			break;
		default:
			Assert(0);
			break;
		}
	}

	void audMixerSubmix::AddOutputInternal(audMixerSubmix *submix, const bool allocateVolumeMatrices, const bool recomputeProcessingGraph)
	{
		Assert(submix);
		Assert(m_NumSubmixOutputs < kMaxSubmixOutputs);

		if(m_NumSubmixOutputs < kMaxSubmixOutputs)
		{
			for(u32 i = 0; i < m_NumSubmixOutputs; i++)
			{
				if(m_OutputConnectionIds[i] != 0xffff)
				{
					audMixerConnection* existingConnection = audMixerConnection::GetConnectionFromId(m_OutputConnectionIds[i]);

					// Already connected, no point continuing
					if(existingConnection->GetOutput() == (s32)submix->GetSubmixId())
					{
						audAssertf(false, "AddOutputInternal failed: Submix %s is already connected to %s", GetName(), submix->GetName());
						return;
					}
				}
			}

			audMixerConnection *connection = audMixerConnection::Connect(this, submix, allocateVolumeMatrices);
			Assert(connection);
			const u32 connectionId = audMixerConnection::GetConnectionId(connection);
			if(submix->AddInput(connectionId, recomputeProcessingGraph))
			{
				Assign(m_OutputConnectionIds[m_NumSubmixOutputs], connectionId);

				if(allocateVolumeMatrices)
				{
					// Initialize volumes to 1
					const Vector_4V identityGain = V4VConstant(V_ONE);
					if(connection->HasTwoVolumeMatrices())
					{
						connection->SetCurrentVolumeMatrix(identityGain, identityGain);
					}
					else
					{
						connection->SetCurrentVolumeMatrix(identityGain);
					}
				}

				m_NumSubmixOutputs++;
			}
			else
			{
				audMixerConnection::DestroyConnection(connection);
			}
		}
	}

#endif // __AUD_STRIP_CMD_BUFFER

audMixerSubmix *audMixerSubmix::GetSubmixFromIndex(const s32 index)
{
#if __SPU
	return g_SubmixCache->GetSubmixFromIndex(index);
#else
	return audDriver::GetMixer()->GetSubmixFromIndex(index);
#endif
}

#if !__SPU

void audMixerSubmix::Init(const u32 submixId, const u32 numChannels, const bool hasVoiceInputs)
{
#if __DEV
	m_SubmixName = NULL;
#endif

#if AUD_SUPPORT_METERING
	for(u32 i = 0; i < kMaxSubmixChannels; i++)
	{
		m_RmsAmplitude[i] = m_PeakAmplitude[i] = m_MaxPeakAmplitude[i] = 0.f;
	}
	for(u32 i = 0; i < kMaxEffectsPerSubmix; i++)
	{
		m_EffectInputLevel[i] = 0.f;
		m_EffectType[i] = AUD_NUM_DSPEFFECTS;
	}
#endif

#if __BANK
	m_DumpOutputFile = NULL;
	for(u32 i = 0; i < kMaxSubmixChannels; i++)
	{
		m_OutputStreams[i] = NULL;
		m_NumOutputDataBytes[i] = 0;
	}
	m_TimeCodeOutput = NULL;
#endif

	m_ComputeRMS = false;
	m_ComputePeakAmplitude = false;
	m_RMSValue = 0.f;

	m_SourceEffectSubmix = false;
	m_KeepBuffers = false;
	m_HaveAllocatedBuffers = false;

	m_SubmixId = submixId;
	m_HaveMixed = false;
	m_HaveProcessedDSP = false;
	m_HavePostMixed = false;

	m_NumChannels = numChannels;

	m_NumSubmixInputs = 0;
	m_NumSubmixOutputs = 0;

	m_NumActiveVoiceInputs = 0;

	m_HasVoiceInputs = hasVoiceInputs;

	for(u32 i = 0 ; i < kMaxSubmixInputs; i++)
	{
		m_SubmixInputConnectionIds[i] = audFrameBufferPool::InvalidId;
	}

	for(u32 i = 0 ; i < kMaxSubmixOutputs; i++)
	{
		m_OutputConnectionIds[i] = audFrameBufferPool::InvalidId;
	}

	for(s32 i = 0; i < kMaxEffectsPerSubmix; i++)
	{
		m_Effects[i] = NULL;
		m_EffectSizes[i] = 0;
	}

	for(s32 i = 0; i < kMaxSubmixChannels; i++)
	{
		m_BufferIds[i] = audFrameBufferPool::InvalidId;
	}

	m_ProcessingStage = -1;
	m_FinalOutputProcessingStage = -1;

	m_BufferFormat = Noninterleaved;	
}

void audMixerSubmix::Shutdown()
{
	for(atFixedArray<audDspEffect*, kMaxEffectsPerSubmix>::iterator i = m_Effects.begin(); i != m_Effects.end(); i++)
	{
		if(*i)
		{
			(*i)->Shutdown();
			// the submix has ownership of the effect, so should delete it directly.
			delete *i;
		}
	}
}

bool audMixerSubmix::AddInput(const u32 connectionId, const bool recomputeProcessingGraph)
{
	audMixerConnection *connection = audMixerConnection::GetConnectionFromId(connectionId);
	Assert(connection);
	if(!connection)
	{
		return false;
	}
	Assert(audDriver::GetMixer()->GetSubmixFromIndex(connection->GetOutput()) == this);
	Assert(!connection->IsVoiceInput());
	Assert(m_NumSubmixInputs < kMaxSubmixInputs);
	
	// submix input
	if(m_NumSubmixInputs >= kMaxSubmixInputs)
	{
		return false;
	}
	else
	{	
		if(recomputeProcessingGraph)
		{
			audDriver::GetMixer()->FlagGraphComputationRequired();
		}
		
		Assign(m_SubmixInputConnectionIds[m_NumSubmixInputs++], connectionId);
		return true;
	}
}

void audMixerSubmix::DisconnectOutputInternal(s32 submixIndex, const bool recomputeProcessingGraph)
{
	for(u32 loop = 0; loop < m_NumSubmixOutputs; loop++)
	{
		audMixerConnection* connection = audMixerConnection::GetConnectionFromId(m_OutputConnectionIds[loop]);

		if(connection)
		{
			if(submixIndex == -1 || connection->GetOutput() == submixIndex)
			{
				audMixerSubmix* outputSubmix = GetSubmixFromIndex(connection->GetOutput());
				outputSubmix->RemoveInput(m_OutputConnectionIds[loop]);
				audMixerConnection::DestroyConnection(connection);
				m_OutputConnectionIds[loop] = audMixerConnection::INVALID;

				if(recomputeProcessingGraph)
				{
					audDriver::GetMixer()->FlagGraphComputationRequired();
				}

				// Copy over the final submix in the array so we're not left with any gaps
				m_OutputConnectionIds[loop] = m_OutputConnectionIds[m_NumSubmixOutputs - 1];
				m_NumSubmixOutputs--;
				loop--;
			}
		}
	}

	// Submix index of -1 means 'remove everything', so we should have exactly zero outputs remaining
	audAssert(submixIndex >= 0 || m_NumSubmixOutputs == 0);
}

void audMixerSubmix::RemoveInput(u32 connectionID)
{
	for(u32 i = 0; i < m_NumSubmixInputs; i++)
	{
		if(m_SubmixInputConnectionIds[i] == connectionID)
		{
			// Replace with the final entry and then blank that out, ensuring that we don't have any gaps
			m_SubmixInputConnectionIds[i] = m_SubmixInputConnectionIds[m_NumSubmixInputs - 1];
			m_SubmixInputConnectionIds[m_NumSubmixInputs - 1] = audFrameBufferPool::InvalidId;
			m_NumSubmixInputs--;
			return;
		}
	}
}

s32 audMixerSubmix::ComputeLastOutputStage()
{
	s32 lastOutputStage = GetProcessingStage();
	for(u32 i = 0; i < m_NumSubmixOutputs; i++)
	{
		Assert(m_OutputConnectionIds[i] != audMixerConnection::INVALID);
		audMixerConnection *connection = audMixerConnection::GetConnectionFromId(m_OutputConnectionIds[i]);
		Assert(!connection->IsVoiceInput());
		//Assert(connection->GetOutput() != -1);

		lastOutputStage = Max(lastOutputStage, audDriver::GetMixer()->GetSubmixFromIndex(connection->GetOutput())->GetProcessingStage());
	}
	m_FinalOutputProcessingStage = lastOutputStage;
	return lastOutputStage;
}

void audMixerSubmix::AddOutput(u32 submixIndex, const bool allocateVolumeMatrices, const bool recomputeProcessingGraph)
{
	audAddSubmixOutputCommand command;
	command.submixIndex = submixIndex;
	command.allocateVolumeMatrices = allocateVolumeMatrices;
	command.recomputeProcessingGraph = recomputeProcessingGraph;
	WriteCommand(&command);
}

void audMixerSubmix::DisconnectOutput(u32 submixIndex, const bool recomputeProcessingGraph)
{
	audDisconnectSubmixOutputCommand command;
	command.submixIndex = submixIndex;
	command.recomputeProcessingGraph = recomputeProcessingGraph;
	WriteCommand(&command);
}

void audMixerSubmix::DisconnectAllOutputs()
{
	audDisconnectSubmixOutputCommand command;
	command.submixIndex = -1;
	WriteCommand(&command);
}

void audMixerSubmix::SetOutputVolumes(const u32 index, const f32 *volumes) const
{
	audSetSubmixVolumesCommand command;
	command.index = index;

	const Vector_4V *volumesV = (const Vector_4V*)volumes;
	const Vector_4V packedVolumes0 = V4FloatToIntRaw<15>(*volumesV++);
	const Vector_4V packedVolumes1 = V4FloatToIntRaw<15>(*volumesV);

	command.packedVolumes = V4PackSignedIntToSignedShort(packedVolumes0,packedVolumes1);
	WriteAlignedCommand(&command);
}

void audMixerSubmix::SetEffect(const s32 slotId, audDspEffect *effect, const u32 channelMask) const
{
	audSetSubmixEffectCommand command;
	Assign(command.slotIndex, slotId);
	command.effect = effect;
	command.channelMask = channelMask;
	WriteCommand(&command);
}

void audMixerSubmix::SetEffectParam(const s32 slotId, const u32 paramId, const f32 val) const
{
	audSetEffectF32ParamCommand command;
	command.paramId = paramId;
	command.value = val;
	Assign(command.slotIndex, slotId);
	WriteCommand(&command);
}

void audMixerSubmix::SetEffectParam(const s32 slotId, const u32 paramId, const u32 val) const
{
	audSetEffectU32ParamCommand command;
	command.paramId = paramId;
	command.value = val;
	Assign(command.slotIndex, slotId);
	WriteCommand(&command);
}

void audMixerSubmix::SetFlag(const SubmixFlag flag, const bool state)
{
	audSetSubmixFlagCommand command;
	command.flag = flag;
	command.state = state;	
	WriteCommand(&command);
}

bool audMixerSubmix::WriteCommand(audCommandPacketHeader *packet) const
{
	packet->objectId = m_SubmixId;
	packet->systemId = audMixerDevice::COMMAND_SUBMIX;
	return audDriver::GetMixer()->WriteCommand(packet);
}

bool audMixerSubmix::WriteAlignedCommand(audCommandPacketHeader *packet) const
{
	packet->objectId = m_SubmixId;
	packet->systemId = audMixerDevice::COMMAND_SUBMIX;
	return audDriver::GetMixer()->WriteAlignedCommand(packet);
}

#endif // !__SPU

void audMixerSubmix::AllocateBuffers()
{
	if(!m_HaveAllocatedBuffers)
	{	
		for(u32 i = 0; i < m_NumChannels; i++)
		{
			if(!m_KeepBuffers)
			{
				Assign(m_BufferIds[i], g_SubmixCache->AllocateBuffer(m_FinalOutputProcessingStage));
			}

			if(audVerifyf(m_BufferIds[i] != audFrameBufferPool::InvalidId, "Failed to allocate submix buffer"))
			{
				g_FrameBufferCache->ZeroBuffer(m_BufferIds[i]);
			}
			else
			{
				// Early out without setting m_HaveAllocatedBuffers
				return;
			}
		}

		m_HaveAllocatedBuffers = true;
	}
}

void audMixerSubmix::NotifyCache()
{
	if(m_HaveAllocatedBuffers && !m_KeepBuffers)
	{
		for(u32 i = 0; i < m_NumChannels; i++)
		{
			g_SubmixCache->NotifyBufferAllocation(m_BufferIds[i], m_FinalOutputProcessingStage);
		}
	}
}


void audMixerSubmix::Mix(SPU_ONLY(u8 *scratchSpace, u32 scratchSpaceSize))
{
	PF_FUNC(SubmixMix);
	Assert(!m_HaveMixed);
	Assert(this);

	// allocate buffers for this frame, if we don't already have them.
	AllocateBuffers();

	if(m_HaveAllocatedBuffers)
	{
		for(u32 i = 0; i < m_NumSubmixInputs; i++)
		{
			Assert(m_SubmixInputConnectionIds[i] != audFrameBufferPool::InvalidId);
			audMixerConnection *inputConnection = audMixerConnection::GetConnectionFromId(m_SubmixInputConnectionIds[i]);
			Assert(!inputConnection->IsVoiceInput());
			audMixerSubmix* destSubmix = GetSubmixFromIndex(inputConnection->GetInput());

			if(destSubmix && destSubmix->HasAllocatedBuffers())
			{
				destSubmix->MixIntoSubmix(this, inputConnection);
			}			
		}
	}	

	if(!m_HaveProcessedDSP)
	{
		ProcessDSP(SPU_ONLY(scratchSpace, scratchSpaceSize));
	}

#if AUD_SUPPORT_METERING
		
	const Vector_4V oneOverNumSamples = V4VConstantSplat<kMixBufNumSamples == 256 ? 0x3B800000 : 0x3c000000>(); // 1/256 or 1/128
	CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);

	if(m_BufferFormat == Noninterleaved)
	{
		for(u32 channel = 0; channel < m_NumChannels; channel ++)
		{
			Vector_4V peak = V4VConstant(V_ZERO);
			Vector_4V squaredSum = V4VConstant(V_ZERO);
			const Vector_4V *bufPtr = (const Vector_4V*)GetMixBuffer(channel);
			if(bufPtr)
			{
				for(u32 i = 0; i < kMixBufNumSamples; i += 4)
				{
					const Vector_4V samples = *bufPtr++;
					Vector_4V sq = V4Scale(samples, samples);	

					peak = V4Max(peak, V4Abs(samples));
					squaredSum = V4Add(squaredSum, sq);
				}
			}
			Vector_4V splattedPeak = V4Max(peak, V4Max(V4SplatW(peak), V4Max(V4SplatY(peak),V4SplatZ(peak))));
			m_PeakAmplitude[channel] = GetX(splattedPeak);
			m_MaxPeakAmplitude[channel] = Max(m_MaxPeakAmplitude[channel], m_PeakAmplitude[channel]);

			Vector_4V splattedSqSum = V4Add(squaredSum, V4Add(V4SplatY(squaredSum), V4Add(V4SplatW(squaredSum), V4SplatZ(squaredSum))));

			Vector_4V splattedMeanSq = V4Scale(splattedSqSum, oneOverNumSamples);
			Vector_4V splattedRMS = V4Sqrt(splattedMeanSq);
			m_RmsAmplitude[channel] = GetX(splattedRMS);
		}
	}
	else
	{
		Assert(m_NumChannels == 4);
		Vector_4V peak = V4VConstant(V_ZERO);
		Vector_4V squaredSum = V4VConstant(V_ZERO);
		Vector_4V sum = V4VConstant(V_ZERO);
		for(u32 channel = 0; channel < m_NumChannels; channel ++)
		{				
			const Vector_4V *bufPtr = (const Vector_4V*)GetMixBuffer(channel);
			if(bufPtr)
			{
				for(u32 i = 0; i < kMixBufNumSamples; i += 4)
				{
					const Vector_4V samples = *bufPtr++;
					Vector_4V sq = V4Scale(samples, samples);	

					peak = V4Max(peak, V4Abs(samples));
					squaredSum = V4Add(squaredSum, sq);
					sum = V4Add(sum, samples);
				}
			}
		}

		*((Vector_4V*)&m_PeakAmplitude[0]) = peak;
		*((Vector_4V*)&m_MaxPeakAmplitude[0]) = V4Max(*((Vector_4V*)&m_MaxPeakAmplitude[0]), peak);

		Vector_4V meanSq = V4Scale(squaredSum, oneOverNumSamples);

		Vector_4V mean = V4Scale(sum, oneOverNumSamples);
		*((Vector_4V*)&m_MeanAmplitude[0]) = mean;

		Vector_4V rms = V4Sqrt(meanSq);
		*((Vector_4V*)&m_RmsAmplitude[0]) = rms;

		if(!(m_PeakAmplitude[0] <= 1.f && m_PeakAmplitude[1] <= 1.f))
		{
			audWarningf("Submix %s >0dB (%fdB)", GetName(), audDriverUtil::ComputeDbVolumeFromLinear(Max(m_PeakAmplitude[0], m_PeakAmplitude[1])));
		}
		if(!V4IsFiniteAll(*((Vector_4V*)(&m_PeakAmplitude[0]))))
		{
			audErrorf("Submix %s non-finite peak", GetName());
		}
	}
#endif // AUD_SUPPORT_METERING

	if(m_ComputeRMS)
	{
		Assert(m_BufferFormat == Noninterleaved);
		Assert(m_NumChannels >= 4);

		atRangeArray<Vector_4V, 4> perChannelRMS;
		const Vector_4V oneOverNumSamples = V4VConstantSplat<kMixBufNumSamples == 256 ? 0x3B800000 : 0x3c000000>();
		
		CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);

		for(u32 channel = 0; channel < 4; channel ++)
		{			
			Vector_4V squaredSum = V4VConstant(V_ZERO);
			const Vector_4V *bufPtr = (const Vector_4V*)GetMixBuffer(channel);
			if(bufPtr)
			{
				for(u32 i = 0; i < kMixBufNumSamples; i += 4)
				{
					const Vector_4V samples = *bufPtr++;
					Vector_4V sq = V4Scale(samples, samples);
					squaredSum = V4Add(squaredSum, sq);
				}
			}						
			Vector_4V splattedSqSum = V4Add(squaredSum, V4Add(V4SplatY(squaredSum), V4Add(V4SplatW(squaredSum), V4SplatZ(squaredSum))));
			Vector_4V splattedMeanSq = V4Scale(splattedSqSum, oneOverNumSamples);
		
			perChannelRMS[channel] = V4Sqrt(splattedMeanSq);
		}

		m_RMSValue = GetX(V4Scale(V4VConstant(V_QUARTER), V4Add(V4Add(perChannelRMS[0], perChannelRMS[1]),V4Add(perChannelRMS[2], perChannelRMS[3]))));
	}

#if __BANK && !__SPU
	DumpMixBufferToDisk();
#endif

	m_HaveMixed = true;
	m_HavePostMixed = false;
}

void audMixerSubmix::ProcessDSP(SPU_ONLY(u8 *scratchSpace, u32 scratchSpaceSize))
{
	PF_FUNC(SubmixProcessDSP);
	// run dsp on m_MixBuf ... (not in debug builds as it is too expensive)
#if __OPTIMIZED

	if(m_HaveAllocatedBuffers)
	{
	#if __BANK
		if(!g_DisableSubmixEffects)
	#endif
		{
			audDspEffectBuffer bufferDesc;
			bufferDesc.NumChannels = m_NumChannels;
			bufferDesc.Format = GetBufferFormat();
			for(u32 i = 0; i < m_NumChannels; i++)
			{
				bufferDesc.ChannelBuffers[i] = GetMixBuffer(i);
				Assign(bufferDesc.ChannelBufferIds[i], GetMixBufferId(i));
			}

			for(s32 i = 0; i < kMaxEffectsPerSubmix; i++)
			{
				if(m_Effects[i])
				{
					audDspEffect *effect = m_Effects[i];

					if(effect->GetProcessFormat() != bufferDesc.Format)
					{
						ConvertBufferToFormat(bufferDesc, effect->GetProcessFormat());
					}

#if AUD_SUPPORT_METERING
					effect->PreProcess(bufferDesc);
					m_EffectInputLevel[i] = Max(effect->GetInputLevel(), m_EffectInputLevel[i]);
					m_EffectMaxInputLevel[i] = Max(m_EffectMaxInputLevel[i], m_EffectInputLevel[i]);
#endif // AUD_SUPPORT_METERING

					effect->Process(bufferDesc);
				}
				else
				{
#if AUD_SUPPORT_METERING
					m_EffectInputLevel[i] = 0.f;
#endif // AUD_SUPPORT_METERING
				}
			}

			if(bufferDesc.Format != m_BufferFormat)
			{
				ConvertBufferToFormat(bufferDesc, m_BufferFormat);
			}
		}// g_DisableSubmixEffects

	} // m_HaveAllocatedBuffers
#endif // __OPTIMIZED

	m_HaveProcessedDSP = true;
}

#if __BANK && !__SPU
void audMixerSubmix::DumpMixBufferToDisk()
{
	if(m_DumpOutputFile)
	{
		for(u32 channelId = 0; channelId < m_NumChannels; channelId++)
		{
			if(!m_OutputStreams[channelId])
			{
				m_NumOutputDataBytes[channelId] = 0;

				char fileName[64];
				// RAGE channel ordering
				const char *channelNames[] = 
				{
					"FL",
					"FR",
					"SL",
					"SR",
					"C",
					"LFE",					
					"BL",
					"BR"
				};
				formatf(fileName, "X:\\%s_%s", m_DumpOutputFile, channelNames[channelId]);
				m_OutputStreams[channelId] = ASSET.Create(fileName, "wav");
				Assert(m_OutputStreams[channelId]);
				if(m_OutputStreams[channelId])
				{
					const unsigned char RIFF[] = {'R', 'I', 'F', 'F'};
					m_OutputStreams[channelId]->WriteByte(RIFF, 4);
					int size = 0;
					m_OutputStreams[channelId]->WriteInt(&size, 1);
					const unsigned char WAVE[] = {'W', 'A', 'V', 'E'};
					m_OutputStreams[channelId]->WriteByte(WAVE, 4);

					const unsigned char fmtChunk[] = {'f', 'm', 't', ' '};
					m_OutputStreams[channelId]->WriteByte(fmtChunk, 4);
					size = 16;
					m_OutputStreams[channelId]->WriteInt(&size, 1);
					short audioFormat = 1;
					m_OutputStreams[channelId]->WriteShort(&audioFormat, 1);
					short numChannels = 1;
					m_OutputStreams[channelId]->WriteShort(&numChannels, 1);
					u32 sampleRate = kMixerNativeSampleRate;
					m_OutputStreams[channelId]->WriteInt(&sampleRate, 1);
					u32 byteRate = sampleRate * numChannels * 16 / 8;
					m_OutputStreams[channelId]->WriteInt(&byteRate, 1);
					short blockA = 2;
					m_OutputStreams[channelId]->WriteShort(&blockA, 1);
					short bitsPerSample = 16;
					m_OutputStreams[channelId]->WriteShort(&bitsPerSample, 1);

					const unsigned char dataChunk[] = {'d', 'a', 't', 'a'};
					m_OutputStreams[channelId]->WriteByte(dataChunk, 4);
					size = 0;
					m_OutputStreams[channelId]->WriteInt(&size, 1);

				}
			}

			if(m_OutputStreams[channelId])
			{
#if RSG_PS3
				// On PS3 we only support writing the master submix, and it's interleaved on SPU so we need to deinterleave here
				// 8 channel mix buffer starting from frame buffer 0 and contiguous, so this is quite simple:
				const f32 *mixBuf = GetMixBuffer(0);
				for(u32 i = 0; i < kMixBufNumSamples; i++)
				{
					s16 sh = (s16)(Clamp(mixBuf[i*8 + channelId],-1.f,1.f) * 32767.f);
					m_OutputStreams[channelId]->WriteShort(&sh,1);
					m_NumOutputDataBytes[channelId] += 2;
				}
#else
				// write out one channels worth of data
				if(m_BufferFormat == Noninterleaved)
				{
					const f32 *mixBuf = GetMixBuffer(channelId);
					for(u32 i = 0; i < kMixBufNumSamples; i++)
					{
						s16 sh = (s16)(Clamp(mixBuf[i], -1.f, 1.f) * 32767.f);
						m_OutputStreams[channelId]->WriteShort(&sh,1);
						m_NumOutputDataBytes[channelId] += 2;
					}
				}
				else
				{
					// deinterleave
					Assert(m_NumChannels == 4);
					
					for(u32 pass = 0; pass < m_NumChannels; pass++)
					{
						const f32 *mixBuf = GetMixBuffer(pass);

						for(u32 i = 0; i < kMixBufNumSamples / 4; i++)
						{
							s16 sh = (s16)(Clamp(mixBuf[i*4 + channelId],-1.f,1.f) * 32767.f);
							m_OutputStreams[channelId]->WriteShort(&sh,1);
							m_NumOutputDataBytes[channelId] += 2;
						}
					}
				}
#endif
			}
			if(channelId == 0)
			{
				if(!m_TimeCodeOutput)
				{
					char fileName[64];
					formatf(fileName, "c:\\%s", m_DumpOutputFile);
					m_TimeCodeOutput = ASSET.Create(fileName, "timecode");
				}
				if(m_TimeCodeOutput)
				{
					u32 timeCode = audDriver::GetMixer()->GetMixerTimeFrames();
					m_TimeCodeOutput->WriteInt(&timeCode, 1);
				}
			}
		}
	}
	else
	{
		if(m_OutputStreams[0])
		{
			for(u32 channelId = 0; channelId < m_NumChannels; channelId++)
			{
				int size = m_NumOutputDataBytes[channelId] + 36;
				m_OutputStreams[channelId]->Seek(4);
				m_OutputStreams[channelId]->WriteInt(&size, 1);
				size = m_NumOutputDataBytes[channelId];
				m_OutputStreams[channelId]->Seek(40);
				m_OutputStreams[channelId]->WriteInt(&size, 1);
				m_OutputStreams[channelId]->Close();
				m_OutputStreams[channelId] = NULL;
			}
		}
		if(m_TimeCodeOutput)
		{
			m_TimeCodeOutput->Close();
			m_TimeCodeOutput = NULL;
		}
	}
}
#endif

void audMixerSubmix::MixIntoSubmix(audMixerSubmix *destSubmix, audMixerConnection *connection)
{
	PF_FUNC(SubmixMixIntoSubmix);
	Assert(m_HaveMixed);

	if(connection)
	{
		Assert(destSubmix);		
		Assert(m_NumChannels <= g_MaxOutputChannels);

		const u32 numDestChannels = connection->GetNumOutputs();

		// we don't currently support downmixing to anything other than a mono buffer
		Assert(numDestChannels >= m_NumChannels || (numDestChannels == 1 && m_BufferFormat == Noninterleaved));
		Assert(m_NumChannels == 1 || numDestChannels == 1 || numDestChannels == 2 || numDestChannels == 4 || numDestChannels == 6 || numDestChannels == 8);
		
		if(m_BufferFormat == Noninterleaved)
		{
			if(connection->HasVolumeMatrix())
			{
				Vector_4V current0 = V4VConstant(V_ZERO), current1 = V4VConstant(V_ZERO);
				Vector_4V prev0 = V4VConstant(V_ZERO), prev1 = V4VConstant(V_ZERO);

				if(connection->HasTwoVolumeMatrices())
				{
					connection->GetVolumeMatrices(current0, current1, prev0, prev1);
				}
				else
				{
					connection->GetVolumeMatrices(current0, prev0);
				}

				if(numDestChannels < m_NumChannels)
				{
					if(numDestChannels == 1)
					{
						for(u32 channel = 0; channel < m_NumChannels; channel++)
						{
							Vector_4V prevVolume = V4SplatX(prev0);
							Vector_4V currentVolume = V4SplatX(current0);

							switch(channel)
							{
							case 0:
								prevVolume = V4SplatX(prev0);
								currentVolume = V4SplatX(current0);
								break;
							case 1:
								prevVolume = V4SplatY(prev0);
								currentVolume = V4SplatY(current0);
								break;
							case 2:
								prevVolume = V4SplatZ(prev0);
								currentVolume = V4SplatZ(current0);
								break;
							case 3:
								prevVolume = V4SplatW(prev0);
								currentVolume = V4SplatW(current0);
								break;
							case 4:
								prevVolume = V4SplatX(prev1);
								currentVolume = V4SplatX(current1);
								break;
							case 5:
								prevVolume = V4SplatY(prev1);
								currentVolume = V4SplatY(current1);
								break;
							default:
								audAssertf(false, "Unexpected number of channels for downmix");
								break;
							}

							if(!V4IsZeroAll(prevVolume) || !V4IsZeroAll(currentVolume))
							{
								MixMonoBufToMonoBuf<kMixBufNumSamples>(
									GetMixBuffer(channel),
									destSubmix->GetMixBuffer(0),
									prevVolume,
									currentVolume);
							}
						}
					}
				}
				else if(m_NumChannels == 1 && numDestChannels == 4 && destSubmix->GetBufferFormat() == Interleaved)
				{
					InterleaveAndMix_1to4<kMixBufNumSamples>(	prev0, 
																current0, 
																GetMixBuffer(0),
																destSubmix->GetMixBuffer(0),
																destSubmix->GetMixBuffer(1),
																destSubmix->GetMixBuffer(2),
																destSubmix->GetMixBuffer(3));
				}
				else
				{
					// No support for these interleaving here yet
					Assert(destSubmix->GetBufferFormat() == Noninterleaved);

					if(numDestChannels == 1)
					{
						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							GetMixBuffer(0),
							destSubmix->GetMixBuffer(0),
							V4SplatX(prev0),
							V4SplatX(current0));
					}
					else if(numDestChannels == 2)
					{
						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							GetMixBuffer(0),
							destSubmix->GetMixBuffer(0),
							V4SplatX(prev0),
							V4SplatX(current0));

						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							// if this submix is mono then use the same source buffer for each output channel
							m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(1),
							destSubmix->GetMixBuffer(1),
							V4SplatY(prev0),
							V4SplatY(current0));
					}
					else
					{
						// 4,6 or 8 channels
						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							GetMixBuffer(0),
							destSubmix->GetMixBuffer(0),
							V4SplatX(prev0),
							V4SplatX(current0));

						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							// if this submix is mono then use the same source buffer for each output channel
							m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(1),
							destSubmix->GetMixBuffer(1),
							V4SplatY(prev0),
							V4SplatY(current0));

						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(2),
							destSubmix->GetMixBuffer(2),
							V4SplatZ(prev0),
							V4SplatZ(current0));

						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(3),
							destSubmix->GetMixBuffer(3),
							V4SplatW(prev0),
							V4SplatW(current0));

						// Move onto second half of matrix
						// mono submixes route to all channels, 4 channel submixes route only to the first
						// four channels of any destination
						if(numDestChannels > 4 && (m_NumChannels == 1 || m_NumChannels > 4))
						{		
							// 6 or 8
							MixMonoBufToMonoBuf<kMixBufNumSamples>(
								m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(4),
								destSubmix->GetMixBuffer(4),
								V4SplatX(prev1),
								V4SplatX(current1));

							MixMonoBufToMonoBuf<kMixBufNumSamples>(
								m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(5),
								destSubmix->GetMixBuffer(5),
								V4SplatY(prev1),
								V4SplatY(current1));

							Assert(numDestChannels <= 6);
							/*if(numDestChannels > 6)
							{
								MixMonoBufToMonoBuf<kMixBufNumSamples>(
									m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(6),
									destSubmix->GetMixBuffer(6),
									V4SplatZ(prev1),
									V4SplatZ(current1));

								MixMonoBufToMonoBuf<kMixBufNumSamples>(
									m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(7),
									destSubmix->GetMixBuffer(7),
									V4SplatW(prev1),
									V4SplatW(current1));
							}*/
						}
					}
				}
			}
			else
			{
				// Unity gain; no scaling is performed
				if(numDestChannels == 2)
				{
					MixMonoBufToMonoBuf<kMixBufNumSamples>(
						GetMixBuffer(0),
						destSubmix->GetMixBuffer(0));
						

					MixMonoBufToMonoBuf<kMixBufNumSamples>(
						// if this submix is mono then use the same source buffer for each output channel
						m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(1),
						destSubmix->GetMixBuffer(1));
				}
				else
				{
					// 4,6 or 8 channels

					MixMonoBufToMonoBuf<kMixBufNumSamples>(
						GetMixBuffer(0),
						destSubmix->GetMixBuffer(0));

					MixMonoBufToMonoBuf<kMixBufNumSamples>(
						// if this submix is mono then use the same source buffer for each output channel
						m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(1),
						destSubmix->GetMixBuffer(1));

					MixMonoBufToMonoBuf<kMixBufNumSamples>(
						m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(2),
						destSubmix->GetMixBuffer(2));

					MixMonoBufToMonoBuf<kMixBufNumSamples>(
						m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(3),
						destSubmix->GetMixBuffer(3));

					// Move onto second half of matrix
					// mono submixes route to all channels, 4 channel submixes route only to the first
					// four channels of any destination
					if(numDestChannels > 4 && (m_NumChannels == 1 || m_NumChannels > 4))
					{		
						// 6 or 8
						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(4),
							destSubmix->GetMixBuffer(4));

						MixMonoBufToMonoBuf<kMixBufNumSamples>(
							m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(5),
							destSubmix->GetMixBuffer(5));

						Assert(numDestChannels <= 6);
						/*if(numDestChannels > 6)
						{
							MixMonoBufToMonoBuf<kMixBufNumSamples>(
								m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(6),
								destSubmix->GetMixBuffer(6));

							MixMonoBufToMonoBuf<kMixBufNumSamples>(
								m_NumChannels == 1 ? GetMixBuffer(0) : GetMixBuffer(7),
								destSubmix->GetMixBuffer(7));
						}*/
					}
				}
			}
		}
		else
		{
			// Internal mix buffer is interleaved
			Assert(m_NumChannels == 4);
			Assert(destSubmix->GetNumChannels() == 6 || destSubmix->GetNumChannels() == 4);
			if(connection->HasVolumeMatrix())
			{
				Vector_4V current0 = V4VConstant(V_ZERO), current1 = V4VConstant(V_ZERO);
				Vector_4V prev0 = V4VConstant(V_ZERO), prev1 = V4VConstant(V_ZERO);

				if(connection->HasTwoVolumeMatrices())
				{
					connection->GetVolumeMatrices(current0, current1, prev0, prev1);
				}
				else
				{
					connection->GetVolumeMatrices(current0, prev0);
				}
			
				// Don't currently support interleaved mix to 4-channel with scaling
				Assert(destSubmix->GetNumChannels() == 6);

				CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);
				const u32 numSamplesPerChunk = kMixBufNumSamples / 4;

				float *dest0 = destSubmix->GetMixBuffer(0);
				float *dest1 = destSubmix->GetMixBuffer(1);
				float *dest2 = destSubmix->GetMixBuffer(2);
				float *dest3 = destSubmix->GetMixBuffer(3);

				const Vector_4V oneOverNumSamples = V4VConstantSplat<kMixBufNumSamples == 256 ? 0x3B800000 : 0x3c000000>(); // 1/256 or 1/128
				const Vector_4V oneOverNumChunks = V4VConstantSplat<0x3E800000>(); // 1/4

				Vector_4V gain = prev0;
				Vector_4V gainDelta = V4Subtract(current0, prev0);

				Vector_4V gainStep = V4Scale(gainDelta, oneOverNumSamples);
				Vector_4V gainStepPerChunk = V4Scale(gainDelta, oneOverNumChunks);		


				DeinterleaveAndMix_4to4<kMixBufNumSamples>(gain, gainStep, 
															GetMixBuffer(0),
															dest0 + numSamplesPerChunk*0,
															dest1 + numSamplesPerChunk*0,
															dest2 + numSamplesPerChunk*0,
															dest3 + numSamplesPerChunk*0);

				gain = V4Add(gain, gainStepPerChunk);

				DeinterleaveAndMix_4to4<kMixBufNumSamples>(gain, gainStep, 
															GetMixBuffer(1),
															dest0 + numSamplesPerChunk*1,
															dest1 + numSamplesPerChunk*1,
															dest2 + numSamplesPerChunk*1,
															dest3 + numSamplesPerChunk*1);

				gain = V4Add(gain, gainStepPerChunk);

				DeinterleaveAndMix_4to4<kMixBufNumSamples>(gain, gainStep, 
															GetMixBuffer(2), 
															dest0 + numSamplesPerChunk*2,
															dest1 + numSamplesPerChunk*2,
															dest2 + numSamplesPerChunk*2,
															dest3 + numSamplesPerChunk*2);

				gain = V4Add(gain, gainStepPerChunk);

				DeinterleaveAndMix_4to4<kMixBufNumSamples>(gain, gainStep, 
															GetMixBuffer(3),
															dest0 + numSamplesPerChunk*3,
															dest1 + numSamplesPerChunk*3,
															dest2 + numSamplesPerChunk*3,
															dest3 + numSamplesPerChunk*3);
			
			}
			else
			{
				// Unity gain; deinterleave to dest (first four channels)
				
				if(destSubmix->GetBufferFormat() == Interleaved)
				{
					Assert(destSubmix->GetNumChannels() == 4);
					
					MixMonoBufToMonoBuf<kMixBufNumSamples>(GetMixBuffer(0), destSubmix->GetMixBuffer(0));
					MixMonoBufToMonoBuf<kMixBufNumSamples>(GetMixBuffer(1), destSubmix->GetMixBuffer(1));
					MixMonoBufToMonoBuf<kMixBufNumSamples>(GetMixBuffer(2), destSubmix->GetMixBuffer(2));
					MixMonoBufToMonoBuf<kMixBufNumSamples>(GetMixBuffer(3), destSubmix->GetMixBuffer(3));
				}
				else
				{
					const u32 numSamplesPerChunk = kMixBufNumSamples / 4;

					float *dest0 = destSubmix->GetMixBuffer(0);
					float *dest1 = destSubmix->GetMixBuffer(1);
					float *dest2 = destSubmix->GetMixBuffer(2);
					float *dest3 = destSubmix->GetMixBuffer(3);

					DeinterleaveAndMix_4to4<kMixBufNumSamples>(GetMixBuffer(0),
																dest0 + numSamplesPerChunk*0,
																dest1 + numSamplesPerChunk*0,
																dest2 + numSamplesPerChunk*0,
																dest3 + numSamplesPerChunk*0);

					DeinterleaveAndMix_4to4<kMixBufNumSamples>(GetMixBuffer(1),
																dest0 + numSamplesPerChunk*1,
																dest1 + numSamplesPerChunk*1,
																dest2 + numSamplesPerChunk*1,
																dest3 + numSamplesPerChunk*1);

					DeinterleaveAndMix_4to4<kMixBufNumSamples>(GetMixBuffer(2), 
																dest0 + numSamplesPerChunk*2, 
																dest1 + numSamplesPerChunk*2, 
																dest2 + numSamplesPerChunk*2, 
																dest3 + numSamplesPerChunk*2);

					DeinterleaveAndMix_4to4<kMixBufNumSamples>(GetMixBuffer(3),
																dest0 + numSamplesPerChunk*3,
																dest1 + numSamplesPerChunk*3,
																dest2 + numSamplesPerChunk*3,
																dest3 + numSamplesPerChunk*3);
				}
			}
		}
		
	}
	else
	{
		// special case for master submix - just leave data in the internal submix buffer
	}
}

void audMixerSubmix::PostMix()
{
	if(!m_HavePostMixed)
	{
		// we only want to do this once per audio mixer frame
		m_HavePostMixed = true;

		// swap current chan volumes to previous for all outputs
		for(u32 output = 0; output < m_NumSubmixOutputs; output++)
		{
			Assert(m_OutputConnectionIds[output] != 0xffff);
			audMixerConnection::GetConnectionFromId(m_OutputConnectionIds[output])->UpdatePreviousVolumeMatrix();
		}
		
		m_HaveMixed = false;
		m_HaveProcessedDSP = false;
		m_HaveAllocatedBuffers = false;

		m_NumActiveVoiceInputs = 0;
	}
}

void audMixerSubmix::ConvertBufferToFormat(audDspEffectBuffer &bufferDesc, const audMixBufferFormat format)
{
	PF_FUNC(SubmixConvertBufferToFormat);
	// Mono buffers need no actual conversion
	if(bufferDesc.NumChannels > 1)
	{
		if(audVerifyf(bufferDesc.NumChannels == 4, "Trying to %sinterleave a %u channel buffer; this is not currently supported", format == Interleaved ? "" : "de", bufferDesc.NumChannels))
		{
#if __SPU
			audAutoScratchBookmark bookmark();
			float *workingBuffer = AllocateFromScratch<float>(bufferDesc.NumChannels * kMixBufNumSamples, 16, "BufferConversion");
#else
			ALIGNAS(16) static float s_InterleaveWorkBuf[4*kMixBufNumSamples] ;
			float *workingBuffer = s_InterleaveWorkBuf;
#endif
			if(format == Interleaved)
			{
				Interleave_4to4<kMixBufNumSamples>(workingBuffer, bufferDesc.ChannelBuffers[0], bufferDesc.ChannelBuffers[1], bufferDesc.ChannelBuffers[2],bufferDesc.ChannelBuffers[3]);
			}
			else
			{
				// deinterleave
				float *dest0 = workingBuffer + kMixBufNumSamples*0;
				float *dest1 = workingBuffer + kMixBufNumSamples*1;
				float *dest2 = workingBuffer + kMixBufNumSamples*2;
				float *dest3 = workingBuffer + kMixBufNumSamples*3;

				const u32 numSamplesPerChunk = kMixBufNumSamples / 4;
				Deinterleave_4to4<numSamplesPerChunk>(bufferDesc.ChannelBuffers[0], 
														dest0 + numSamplesPerChunk * 0,
														dest1 + numSamplesPerChunk * 0,
														dest2 + numSamplesPerChunk * 0,
														dest3 + numSamplesPerChunk * 0);

				Deinterleave_4to4<numSamplesPerChunk>(bufferDesc.ChannelBuffers[1], 
														dest0 + numSamplesPerChunk * 1,
														dest1 + numSamplesPerChunk * 1,
														dest2 + numSamplesPerChunk * 1,
														dest3 + numSamplesPerChunk * 1);

				Deinterleave_4to4<numSamplesPerChunk>(bufferDesc.ChannelBuffers[2], 
														dest0 + numSamplesPerChunk * 2,
														dest1 + numSamplesPerChunk * 2,
														dest2 + numSamplesPerChunk * 2,
														dest3 + numSamplesPerChunk * 2);

				Deinterleave_4to4<numSamplesPerChunk>(bufferDesc.ChannelBuffers[3], 
														dest0 + numSamplesPerChunk * 3,
														dest1 + numSamplesPerChunk * 3,
														dest2 + numSamplesPerChunk * 3,
														dest3 + numSamplesPerChunk * 3);

			}

			for(u32 i = 0; i < bufferDesc.NumChannels; i++)
			{
				sysMemCpy(bufferDesc.ChannelBuffers[i], workingBuffer + kMixBufNumSamples*i, kMixBufNumSamples*sizeof(float));
			}
		}
	}
	bufferDesc.Format = format;

}

} // namespace rage
