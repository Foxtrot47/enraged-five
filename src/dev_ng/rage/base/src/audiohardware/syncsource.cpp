//
// audiohardware/syncsource.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "device.h"
#include "driver.h"
#include "syncsource.h"

#include "system/memops.h"

namespace rage
{
	SPU_ONLY(audMixerSyncManager *audMixerSyncManager::sm_InstanceEA = NULL);

#if !__SPU
	void audMixerSyncManager::Init()
	{
		m_FreeList.Init();
		sysMemZeroBytes<sizeof(m_TriggerReferenceCounts)>(&m_TriggerReferenceCounts);
		for(u32 i = 0; i < kMaxSyncSources; i++)
		{
			m_OwnerReferenceCounts[i] = 1;
		}
	}

	void audMixerSyncManager::Update()
	{
		const u32 sourcesToCheckPerUpdate = kMaxSyncSources / 8;
		static u32 s_UpdatingSourceId = 0;

		for(u32 i = 0; i < sourcesToCheckPerUpdate; i++, s_UpdatingSourceId++)
		{
			if(m_OwnerReferenceCounts[s_UpdatingSourceId] == 0 && m_TriggerReferenceCounts[s_UpdatingSourceId] == 0)
			{
				Free(s_UpdatingSourceId);
			}
		}
		if(s_UpdatingSourceId == kMaxSyncSources)
		{
			s_UpdatingSourceId = 0;
		}
	}
#endif

	audMixerSyncManager *audMixerSyncManager::Get()
	{
#if __SPU
		Assert(sm_InstanceEA);
		return sm_InstanceEA;
#else
		return &audDriver::GetMixer()->GetSyncManager();
#endif
	}
}
