//
// audiohardware/decoder_xbonexma.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_DECODER_XMA_XBOXONE_H
#define AUD_DECODER_XMA_XBOXONE_H

#if RSG_DURANGO

// for final once everything is working
//#define NO_SHAPE_CONTEXT_VALIDATION 1

#include "system/xtl.h"
#include "decoder.h"
#include "mixer.h"
#include "ringbuffer.h"
#include "file/stream.h"


#pragma warning( push )
#pragma warning( disable : 4200 )
#include <acphal.h>
#pragma warning( pop )

#define DUMP_RAW_XBOXONE_STREAMS 0
#define DUMP_DEBUG_XBOXONE_STREAMS 0

namespace rage
{

	// need to have enough buffer space to deal with pitched up data
	enum{kXBoneXmaDecoderBufferBytes = 64*1024};//(sizeof(s16) * kMixBufNumSamples * (kMaxSampleRateRatio + 1))};
	enum{kXBOneXMAHalOutputBufferBytes = SHAPE_XMA_MAX_OUTPUT_BUFFER_SIZE};
	enum{kXBOneNumberXMAContexts = 150};

	enum audACPContextState 
	{
		ACP_CONTEXT_STATE_DISABLED = 0,
		ACP_CONTEXT_STATE_ENABLE,
		ACP_CONTEXT_STATE_WAIT_ON_DISABLE
	};

	struct audXmaPacketInfo
	{
		audXmaPacketInfo() :
			InputBuffer(NULL),
			LoopStartBits(0),
			LoopSubframeSkip(0),
			LoopEndBits(0),
			LoopSubframeEnd(0),
			ContextIndex(-1),
			OutputBufferByteOffset(0),
			State(ACP_CONTEXT_STATE_DISABLED),
			InputNumBytes(0),
			PacketNumber(0),
			BytesConsumed(0),
			FinalBlock(false)
#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS		
			,StreamFileNumber(-1)
#endif

		{}
		s32 ContextIndex;
		void *InputBuffer;
		u32 NumSamples;
		u32 PlayBegin;
		u32 PlayEnd;
		u32 NumSamplesConsumed;
		u32 LoopStartBits;
		u32 LoopSubframeSkip;
		u32 LoopEndBits;
		u32 LoopSubframeEnd;
		u32 OutputBufferByteOffset;
		audACPContextState State;
		u32 InputNumBytes;
		u32 PacketNumber;
		u32 BytesConsumed;
		bool FinalBlock;

#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS		
		s32 StreamFileNumber;
#endif
	};

	struct xmaContextInfo
	{
		xmaContextInfo() :
			IsFree(true),
			State(ACP_CONTEXT_STATE_DISABLED),
			AudioFrame()
			BANK_ONLY(,WaveNameHash(0))
		{}

		bool IsFree;
		audACPContextState State;
		u32 AudioFrame;
		void *DecodedOutputBuffer;
		void *OutputOverlapBuffer;
#if __BANK
		u32 WaveNameHash;
#endif
	};

	class audDecoderXboxOneXma : public audDecoder
	{
	public:

		static bool InitClass();
		static void ShutdownClass();
#if __BANK
		static void PrintDecoderStats();
		static void UpdateDecoderStats();
		static void PrintUsedXmaContexts();
#endif

		static void DisableXmaContext(s32 index);
		static void EnableXmaContext(s32 index);
		static s32 GetFreeXmaContextIndex();
		static void SetXmaContextState(s32 index, audACPContextState state);
		static audACPContextState GetXmaContextState(s32 index);
		static void FreeXmaContextIndex(s32 index);

		static void AllocateFromAPUMemory(u8** address, u32 dataSize);
		static HRESULT ApuVirtualAllocate(void** virtualAddress,UINT32 sizeInBytes);
		static void ApuVirtualFree(void* virtualAddress);
		static BOOL FixBlockAlign(DWORD* pBYTES,DWORD BLOCKALIGN,DWORD* pChange);

		virtual bool Init(const audWaveFormat::audStreamFormat format, const u32 numChannels, const u32 sampleRate);
		virtual void Shutdown();

		virtual audDecoderState GetState() const
		{
			return m_State;
		}
		virtual void SubmitPacket(audPacket &packet);
		virtual void Update();
		void PreUpdate();

		virtual u32 ReadData(s16 *dest, const u32 numSamples) const;
		virtual u32 QueryNumAvailableSamples() const;
		virtual bool QueryReadyForMoreData() const
		{
			return m_ReadyForMoreData;
		}
		virtual void AdvanceReadPtr(const u32 numSamples);

		virtual void SetWaveIdentifier(const u32 waveIdentifier) { m_WaveIdentifier = waveIdentifier; }

		u32 ComputeBytesToSkip(const u8 *ptr, const u32 len, const u32 samplesToSkip, u32 &skippedSamples);
		void ClearBuffer();

		inline void DebugAbortPlayback() { m_AbortPlayback = true; }

		static DWORD WINAPI MessageQueueThreadProc( __in  LPVOID lpParameter );

#if DUMP_RAW_XBOXONE_STREAMS
		fiStream* m_fp;
#endif
#if DUMP_RAW_XBOXONE_STREAMS || DUMP_DEBUG_XBOXONE_STREAMS
		int m_rawStreamIndex;
#endif

	private:

		u32 ComputeNumSamplesInBuffer(const u8 *ptr, const u32 len);
		void ComputeLoopData(const audPacket &packet, audXmaPacketInfo &xmaPacketInfo);
		
		// ACP STUFF
		static const int    SHAPE_MESSAGEQUEUE_CORE = 0;

		static DWORD MessageQueueThreadProc();
		static void DebugPrintAcpMessage( const ACP_MESSAGE& command );

		static const UINT32 COMMAND_QUEUE_LENGTH = 4096;
		static const UINT32 MESSAGE_QUEUE_LENGTH = 4096;
		
		static IAcpHal*			sm_AcpHal;
		static HANDLE			m_TerminateThreads;
		static long				m_ExpectedThreadsRunning;
		static int				m_MessageQueueThreadSleep;
		static volatile long	m_NumThreadsRunning;

		static SHAPE_CONTEXTS	sm_Contexts;
		static bool*			sm_ContextFree; 
		static xmaContextInfo*	sm_ContextInfo;
		static s32				sm_StartIndex;
		static u32				sm_AudioFrameCount;

#if __BANK
		static u32				sm_PeekAudioMessages;
#endif

		static bool		        m_Disconnected;
		// ACP STUFF

		audStaticRingBuffer<kXBoneXmaDecoderBufferBytes> m_RingBuffer;
		
		atRangeArray<audXmaPacketInfo, 2> m_Packets;
		u32 m_DecodingPacketIndex;
		u32 m_SubmittedPacketIndex;

		audDecoderState m_State;
		u32 m_NumChannels;
		u32 m_SampleRate;
		u32 m_SamplesConsumed;
		u32 m_BytesSubmitted;
		s32 m_NumQueuedPackets;
		u32 m_WaveIdentifier;
		bool m_ReadyForMoreData;
		bool m_AbortPlayback;
		u32 m_PacketNumber;
		u32 m_StallCount;

		u32 m_LastAudioFrame;
#if DUMP_DEBUG_XBOXONE_STREAMS
		u32 m_BytesConsumedLastUpdate;
		u32 m_BytesAvailLastUpdate;
#endif
	};

} // namespace rage

#endif // RSG_DURANGO
#endif // AUD_DECODER_XBOXONE_XMA_H

