// 
// audiohardware/submixjob.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SUBMIXJOB_H
#define AUD_SUBMIXJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(SubmixJob);

namespace rage
{
	union submixInputData {
		u32 val32;
		struct { 
			u8 numProcessingStages;
			u8 numSubmixCacheChannels;
			u8 downmixMode : 3;
			u8 hardwareOutputMode : 3;
			u8 numOutputChannels : 3;
		}fields;
	};
}

#endif // AUD_SUBMIXJOB_H
