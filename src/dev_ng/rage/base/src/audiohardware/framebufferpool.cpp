// 
// audiohardware/framebufferpool.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "framebufferpool.h"

namespace rage {

#if __BANK
	s32 audFrameBufferPool::sm_AllocationError = 0;
	bool audFrameBufferPool::sm_DisableFrameBufferAllocations = false;
#endif

} // namespace rage

