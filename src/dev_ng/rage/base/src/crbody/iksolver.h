//
// crbody/iksolver.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//


#ifndef CRBODY_IKSOLVER_H
#define CRBODY_IKSOLVER_H

#include "crbody/ikgoal.h"
#include "crbody/iksolverbase.h"
#include "crskeleton/jointdata.h"

#define USE_RAGE_SOLVERS (!HACK_GTA4 || !__FINAL)

namespace rage
{

class crJointData;
class crJointRotationLimit;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Generic solver based on a goal.
class crIKGoalSolver : public crIKSolverBase
{
public:
	crIKGoalSolver(crIKSolverBase::crIKSolverType type, const crIKPart* bodyPart);

	// PURPOSE: Iterate the solver for the goal
	virtual void Iterate(float dt, crSkeleton& skeleton);

	// PURPOSE: Return the goal of the solver
	crIKGoal& GetGoal() { return m_Goal; }

	// PURPOSE: Enables the enforcement of joint limits.
	void EnforceLimits(bool enable = true) { m_EnforceLimits = enable; }

	// PURPOSE: Sets a transform representing the relative situation of the
	// point you wish to align with the goal to the bone the solver affects.
	//
	// EXAMPLES:
	// Set this to the offset and orientation of the sole of the shoe relative to
	// the ankle bone to correctly place and align the sole of the shoe on the ground.
	// Likewise this can be set to the offset and orientation of the palm relative
	// to the wrist bone to correctly place the palm on surfaces.
	// Also this can be set to the offset of the eyes relative to the head bone
	// so that when orienting the head to look at a point it bases the angle off the
	// position of the goal relative to the eyes instead of the head.
	void SetGoalAdjustment(const TransformV &adjust) { m_GoalAdjustment = adjust; }

protected:
	crIKGoal m_Goal;
	TransformV m_GoalAdjustment;
	const crIKPart* m_BodyPart;
	bool m_EnforceLimits;
};

////////////////////////////////////////////////////////////////////////////////

#if USE_RAGE_SOLVERS

// PURPOSE: Old spine solver.  Included for backward compatibility.
class crIKSolverSimpleSpine : public crIKGoalSolver
{
public:
	// PURPOSE: Default constructor
	crIKSolverSimpleSpine(int bodyPart, crIKBodyBase& body, const crJointData& jointData);

	// PURPOSE: Solve the inverse kinematics
	virtual void Solve(SolverHelper& sh) const;

private:
	static const int MAX_BONES = 4;
	int m_Idxs[MAX_BONES];
	crJointRotationLimit m_Limits[MAX_BONES];
	Vec3V m_FwdAxis, m_UpAxis;
	int m_NumBones;
	int m_PrimaryIdx;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Old head solver.  Included for backward compatibility.
class crIKSolverSimpleHead : public crIKGoalSolver
{
public:
	// PURPOSE: Default constructor
	crIKSolverSimpleHead(int bodyPart, crIKBodyBase& body, const crJointData& jointData);

	// PURPOSE: Solve the inverse kinematics
	virtual void Solve(SolverHelper& sh) const;

private:
	static const int MAX_BONES = 4;
	int m_Idxs[MAX_BONES];
	crJointRotationLimit m_Limits[MAX_BONES];
	Vec3V m_FwdAxis, m_UpAxis;
	int m_NumBones;
	int m_PrimaryIdx;

	crJointRotationLimit m_HeadLimit;
	int m_HeadIdx;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Limb solver (solves on arms or legs) base class.  Deals with
class crIKSolverLimbBase : public crIKGoalSolver
{
public:
	// PURPOSE: Default constructor
	crIKSolverLimbBase(crIKSolverBase::crIKSolverType type, const crIKPart* bodyPart, const crJointData& jointData);

	// PURPOSE: Solve the inverse kinematics
	virtual void Solve(SolverHelper& sh) const;

protected:
	void SolveLimb(const crIKGoal& goal, TransformV* bones) const;

	enum {JOINT, UPPER, UPPER_MID, LOWER, LOWER_MID, APPENDAGE, APPENDAGE_TIP, NUM_BONES};

	crJointRotationLimit m_Limits[3];
	Vec3V m_RollAxis, m_BendAxis;
	int m_Idxs[NUM_BONES];
	float m_Lengths[2];
	float m_TransferRollToUpperMid, m_TransferRollToLowerMid, m_TransferRollFromAppendage;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// 	Limb solver (solves on arms or legs) based on the Footskate Cleanup algorithm
//	from Michael Gleicher.
//	Compresses/stretches limb slightly to avoid high frequency changes in knee/elbow joint.
class crIKSolverGleicherLimb : public crIKSolverLimbBase
{
public:
	// PURPOSE: Default constructor
	crIKSolverGleicherLimb(int bodyPart, crIKBodyBase& body, const crJointData& jointData);

	// PURPOSE: Set alpha
	void SetAlpha(float alpha) { m_Alpha = alpha; }

	// PURPOSE: Set over reach
	void SetOverReach(float overReach) { m_OverReach = overReach; }

	// PURPOSE: Solver implementation of for the Gleicher solver
	void SolveGleicher(const crIKGoal& goal, QuatV inoutRotations[3],
		Vec3V inoutOffsets[2], const crJointRotationLimit limits[3], const float lengths[2],
		const Vec3V& bendAxis) const;

protected:
	static float LimitAngle(float alpha, float thetaStart, float dtheta);

private:
	float m_Alpha;
	float m_OverReach;
};

////////////////////////////////////////////////////////////////////////////////

class crIKSolverIterativeLimb : public crIKSolverLimbBase
{
public:
	// PURPOSE: Default constructor
	crIKSolverIterativeLimb(int bodyPart, crIKBodyBase& body, const crJointData& jointData);

	// PURPOSE: Set maximum number of iterations
	void SetMaxIterations(int maxIters) { m_MaxIterations = maxIters; }

	// PURPOSE: Set maximum deta per iteration
	void SetMaxDeltaPerIteration(float maxDelta) { m_MaxAngularDelta = maxDelta; }

	// PURPOSE: Set tolerance
	void SetTolerance(float tolerance) { m_Tolerance = tolerance; }

	// PURPOSE: Solver implementation for the iterative solver
	void SolveIterative(const crIKGoal& goal, QuatV inoutRotations[3],
		Vec3V inoutOffsets[2], const crJointRotationLimit limits[3], const float lengths[2],
		const Vec3V& bendAxis) const;

private:
	int   m_MaxIterations;
	float m_Tolerance;
	float m_MaxAngularDelta;
};

#define MAX_RAGE_SOLVER_SIZE sizeof(crIKSolverSimpleHead)
#else // USE_RAGE_SOLVERS
#define MAX_RAGE_SOLVER_SIZE sizeof(crIKSolverQuadruped)
#endif // USE_RAGE_SOLVERS

////////////////////////////////////////////////////////////////////////////////

class crIKSolverArms : public crIKSolverBase
{
public:
	// PURPOSE: Default constructor
	crIKSolverArms();
	
	// PURPOSE: Solve the inverse kinematics
	virtual void Solve(SolverHelper& sh) const;

	// PURPOSE: Return true if the solver need to be solved
	virtual bool IsSolvable() const { return true; }

	// PURPOSE: Description of an arm with a target goal for IK
	enum eArms { LEFT_ARM, RIGHT_ARM, NUM_ARMS };
	enum eArmParts { HAND, FOREARM, UPPERARM, PH_HAND, IK_HAND, CH_HAND, FOREARM_ROLL, UPPERARM_ROLL, NUM_ARM_PARTS };

	struct ArmGoal
	{
		enum
		{
			USE_ORIENTATION			= 0x01,
			USE_FULL_REACH			= 0x02,
			USE_TWIST_CORRECTION	= 0x04
		};

		Mat34V m_Target; // In object space
		bool m_Enabled;
		float m_Blend;
		u16 m_HandBoneIdx, m_ElbowBoneIdx, m_ShoulderBoneIdx, m_PointHelperBoneIdx, m_HandIkHelperBoneIdx, m_ConstraintHelperBoneIdx, m_ForeArmRollBoneIdx, m_UpperArmRollBoneIdx, m_TerminatingIdx;
		u16 m_Flags;
		u16 m_Pad0;
		void Solve(SolverHelper& sh) const;
	};

	// PURPOSE: Return true if the solver need to be solved
	const ArmGoal& GetArmGoal(s32 index) const { Assert(index<NUM_ARMS); return m_Arms[index]; }

protected:
	ArmGoal m_Arms[NUM_ARMS];
};

////////////////////////////////////////////////////////////////////////////////

class crIKSolverLegs : public crIKSolverBase
{
public:
	// PURPOSE: Default constructor
	crIKSolverLegs();
	
	// PURPOSE: Solve the inverse kinematics
	virtual void Solve(SolverHelper& sh) const;

	// PURPOSE: Return true if the solver need to be solved
	virtual bool IsSolvable() const { return true; }

	// PURPOSE: Description of a leg with a target goal for IK
	enum eLeg { LEFT_LEG, RIGHT_LEG, NUM_LEGS };
	enum eLegPart { THIGH, CALF, FOOT, TOE, PH, THIGHROLL, NUM_LEG_PARTS };

	struct Goal
	{
		enum
		{
			USE_ORIENTATION	= 0x01,
			USE_LIMITS		= 0x02
		};

		Mat34V m_Target;
		bool m_Enabled;
		u16 m_BoneIdx[NUM_LEG_PARTS];
		u16 m_TerminatingIdx;
		u16 m_Flags;

		void Solve(SolverHelper& sh, const Vec3V limits[2]) const;
	};

	// PURPOSE:
	Goal& GetGoal(s32 index) { Assert(index < NUM_LEGS); return m_Legs[index]; }

	// PURPOSE:
	void SetLimits(Vec3V_In minLimit, Vec3V_In maxLimit) { m_Limits[0] = minLimit; m_Limits[1] = maxLimit; }
	void GetLimits(Vec3V_InOut minLimit, Vec3V_InOut maxLimit) { minLimit = m_Limits[0]; maxLimit = m_Limits[1]; }

protected:
	Goal m_Legs[NUM_LEGS];
	Vec3V m_Limits[2];
};

////////////////////////////////////////////////////////////////////////////////

class crIKSolverQuadruped : public crIKSolverBase
{
public:
	// PURPOSE: Default constructor
	crIKSolverQuadruped();
	
	// PURPOSE: Solve the inverse kinematics
	virtual void Solve(SolverHelper& sh) const;

	// PURPOSE: Return true if the solver need to be solved
	virtual bool IsSolvable() const { return true; }

	// PURPOSE: Description of a leg with a target goal for IK
	enum eLeg { LEFT_FRONT_LEG, RIGHT_FRONT_LEG, LEFT_BACK_LEG, RIGHT_BACK_LEG, NUM_LEGS };
	enum eLegPart { THIGH, CALF, FOOT, TOE, TOE1, PH, NUM_LEG_PARTS };

	struct Goal
	{
		TransformV m_Target;
		u16 m_BoneIdx[NUM_LEG_PARTS];
		u16 m_TerminatingIdx;
		u16 m_Flags;
		bool m_Enabled;

		void Solve(SolverHelper& sh, float upperLegRatio) const;
		void Solve2Bone(SolverHelper& sh, eLegPart effector, Vec3V_In targetPos, Mat34V_Ref upper, Mat34V_Ref mid, Mat34V_Ref lower) const;
	};

	// PURPOSE:
	Goal& GetGoal(s32 index) { Assert(index < NUM_LEGS); return m_Legs[index]; }

	void SetUpperLegRatio(float upperLegRatio)	{ m_UpperLegRatio = upperLegRatio; }
	float GetUpperLegRatio() const				{ return m_UpperLegRatio; }

protected:
	Goal m_Legs[NUM_LEGS];
	float m_UpperLegRatio;
};

////////////////////////////////////////////////////////////////////////////////

#if USE_RAGE_SOLVERS
CompileTimeAssert(sizeof(crIKSolverSimpleSpine) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverSimpleHead) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverGleicherLimb) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverIterativeLimb) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverArms) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverLegs) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverQuadruped) <= MAX_RAGE_SOLVER_SIZE);
#else
CompileTimeAssert(sizeof(crIKSolverArms) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverLegs) <= MAX_RAGE_SOLVER_SIZE);
CompileTimeAssert(sizeof(crIKSolverQuadruped) <= MAX_RAGE_SOLVER_SIZE);
#endif

} // namespace rage

#endif // CRBODY_IKSOLVER_H
