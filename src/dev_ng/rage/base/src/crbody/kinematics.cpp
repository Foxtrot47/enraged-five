//
// crbody/kinematics.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "kinematics.h"

#include <vector>
#include <algorithm>

#include "crbody/iksolverbase.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"

namespace rage
{

#if HACK_GTA4
class SolverTypeSort
{
public:
	bool operator()(const std::vector<crIKSolverBase*>::value_type left, const std::vector<crIKSolverBase*>::value_type right)
	{
		return (left->GetPriority() < right->GetPriority());
	}
};
#endif //HACK_GTA4

////////////////////////////////////////////////////////////////////////////////

crKinematics::crKinematics()
{
}

////////////////////////////////////////////////////////////////////////////////

crKinematics::~crKinematics()
{
}

////////////////////////////////////////////////////////////////////////////////

void crKinematics::Init(crSkeleton& skel)
{
	m_Skeleton = &skel;
}

////////////////////////////////////////////////////////////////////////////////

void crKinematics::Iterate(float dt)
{
	u32 solverCount = GetNumSolvers();
	for(u32 i=0; i < solverCount; i++)
	{
		m_Solvers[i]->Iterate(dt, *m_Skeleton);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crKinematics::Solve() const
{
	crIKSolverBase::SolverHelper sh;
	sh.m_NumBones = m_Skeleton->GetBoneCount();
	sh.m_Parent = *m_Skeleton->GetParentMtx();
	sh.m_ParentIndices = m_Skeleton->GetSkeletonData().GetParentIndices();
	sh.m_Locals = m_Skeleton->GetLocalMtxs();
	sh.m_Objects = m_Skeleton->GetObjectMtxs();

	u32 solverCount = GetNumSolvers();
	for(u32 i=0; i < solverCount; i++)
	{
		crIKSolverBase* solver = m_Solvers[i];
		solver->Solve(sh);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crKinematics::HasSolvers() const
{
	return m_Solvers.GetCount()!=0;
}

////////////////////////////////////////////////////////////////////////////////

void crKinematics::RemoveAllSolvers()
{
	m_Solvers.Reset();
}

////////////////////////////////////////////////////////////////////////////////

bool crKinematics::HasSolver(crIKSolverBase* solver) const
{
	u32 solverCount = GetNumSolvers();
	for(u32 i=0; i < solverCount; i++)
	{
		if(solver == m_Solvers[i])
		{
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crKinematics::AddSolver(crIKSolverBase* solver)
{
	Assert(solver->GetType() != crIKSolverBase::IK_SOLVER_TYPE_INVALID);
	Assert(!HasSolver(solver));

	m_Solvers.Append() = solver;
#if HACK_GTA4
	std::sort(&m_Solvers[0], &m_Solvers[0] + m_Solvers.size(), SolverTypeSort());	
#endif //HACK_GTA4
}

////////////////////////////////////////////////////////////////////////////////

void crKinematics::RemoveSolver(crIKSolverBase* solver)
{
	u32 solverCount = GetNumSolvers();
	for(u32 i=0; i < solverCount; i++)
	{
		if(m_Solvers[i] == solver)
		{
			m_Solvers.Delete(i);
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
