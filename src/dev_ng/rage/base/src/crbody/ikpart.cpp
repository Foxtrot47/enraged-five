//
// crbody/ikpart.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "ikpart.h"

#include "atl/array_struct.h"
#include "crskeleton/bonedata.h"
#include "crskeleton/jointdata.h"
#include "crskeleton/skeletondata.h"
#include "vectormath/classes.h"

#include <stdlib.h>		// for atoi

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crIKPart::~crIKPart()
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crIKPart::DeclareStruct(datTypeStruct &)
{
}
#endif

////////////////////////////////////////////////////////////////////////////////

crIKSpine::crIKSpine() : crIKPart(),
m_RootBone(NULL),
m_PelvisBone(NULL),
m_MaxAngularAcceleration(0),
m_MaxAngularVelocity(0),
pad0(0),
m_FwdAxis(-Vec3V(V_Z_AXIS_WZERO)),
m_UpAxis(Vec3V(V_Y_AXIS_WZERO))
{
}

////////////////////////////////////////////////////////////////////////////////

crIKSpine::crIKSpine(datResource& rsc) : crIKPart(rsc), m_SpineBones(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

bool crIKSpine::Init(const crSkeletonData& skelData, const crJointData&, const crIKBodyBoneFinder& boneFinder)
{
	m_RootBone = boneFinder.FindBone(skelData, IK_SPINE_BONE_ROOT, 0, 0);
	if(!m_RootBone)
	{
		Warningf("crIKSpine::Init - failed to find root bone");
		return false;
	}

	int numSpineBones = boneFinder.HowMany(skelData, IK_SPINE_BONE_SPINE, 0);
	if(numSpineBones < 1)
	{
		Warningf("crIKSpine::Init - spine bone count < 1");
		return false;
	}

	m_SpineBones.Reset();
	m_SpineBones.Reserve(numSpineBones);
	for(int i=0; i<numSpineBones; i++)
	{
		m_SpineBones.Append() = boneFinder.FindBone(skelData, IK_SPINE_BONE_SPINE, i, 0);
		if(!m_SpineBones[i])
		{
			Warningf("crIKSpine::Init - failed to find spine bone %d", i);
			return false;
		}
	}

	m_PelvisBone = boneFinder.FindBone(skelData, IK_SPINE_BONE_PELVIS, 0, 0);
	if(!m_PelvisBone)
	{
		Warningf("crIKSpine::Init - failed to find pelvis bone");
		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

const crBoneData* crIKSpine::GetBone(int i) const
{
	switch(i)
	{
	case IK_SPINE_BONE_ROOT:
		return m_RootBone;
	default:
		{
			int spineBone = (i-IK_SPINE_BONE_SPINE);
			if(spineBone < GetNumSpineBones())
			{
				return m_SpineBones[spineBone];
			}
			switch(i - GetNumSpineBones() + 1)
			{
			case IK_SPINE_BONE_PELVIS:
				return m_PelvisBone;
			default:
				AssertMsg(0 , "crIKSpine::GetBone - unknown bone");
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crIKSpine::DeclareStruct(datTypeStruct &s)
{
	crIKPart::DeclareStruct(s);

	STRUCT_BEGIN(crIKSpine);
	STRUCT_FIELD(m_RootBone);
	STRUCT_FIELD(m_SpineBones);
	STRUCT_FIELD(m_PelvisBone);
	STRUCT_FIELD(m_MaxAngularAcceleration);
	STRUCT_FIELD(m_MaxAngularVelocity);
	STRUCT_IGNORE(pad0);
	STRUCT_FIELD(m_FwdAxis);
	STRUCT_FIELD(m_UpAxis);
	STRUCT_END();
}
#endif

////////////////////////////////////////////////////////////////////////////////

crIKHead::crIKHead() :
crIKPart(),
m_SpineBone(NULL),
m_HeadBone(NULL),
m_MaxAngularAcceleration(0),
m_MaxAngularVelocity(0),
pad0(0),
m_FwdAxis(-Vec3V(V_Z_AXIS_WZERO)),
m_UpAxis(Vec3V(V_Y_AXIS_WZERO))
{
}

////////////////////////////////////////////////////////////////////////////////

crIKHead::crIKHead(datResource& rsc) : crIKPart(rsc), m_NeckBones(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

bool crIKHead::Init(const crSkeletonData& skelData, const crJointData&, const crIKBodyBoneFinder& boneFinder)
{
	m_SpineBone = boneFinder.FindBone(skelData, IK_HEAD_BONE_SPINE, 0, 0);
	if(!m_SpineBone)
	{
		Warningf("crIKHead::Init - failed to find spine bone");
		return false;
	}

	int numNeckBones;
	numNeckBones = boneFinder.HowMany(skelData, IK_HEAD_BONE_NECK, 0);
	if(numNeckBones < 1)
	{
		Warningf("crIKHead::Init - neck bone count < 1");
		return false;
	}

	m_NeckBones.Reset();
	m_NeckBones.Reserve(numNeckBones);
	for(int i=0; i<numNeckBones; i++)
	{
		m_NeckBones.Append() = boneFinder.FindBone(skelData, IK_HEAD_BONE_NECK, i, 0);
		if(!m_NeckBones[i])
		{
			Warningf("crIKHead::Init - failed to find neck bone %d", i);
			return false;
		}
	}

	m_HeadBone = boneFinder.FindBone(skelData, IK_HEAD_BONE_HEAD, 0, 0);
	if(!m_HeadBone)
	{
		Warningf("crIKHead::Init - failed to find head bone");
		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

const crBoneData* crIKHead::GetBone(int i) const
{
	if (i == IK_HEAD_BONE_SPINE)
	{
		return m_SpineBone;
	}
	else if (i < IK_HEAD_BONE_NECK + GetNumNeckBones())
	{
		return m_NeckBones[i - IK_HEAD_BONE_NECK];
	}
	else if (i == IK_HEAD_BONE_HEAD + (GetNumNeckBones() - 1))
	{
		return m_HeadBone;
	}
	else
	{
		AssertMsg(0 , "crIKHead::GetBone - unknown bone");
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crIKHead::DeclareStruct(datTypeStruct &s)
{
	crIKPart::DeclareStruct(s);

	STRUCT_BEGIN(crIKHead);
	STRUCT_FIELD(m_SpineBone);
	STRUCT_FIELD(m_NeckBones);
	STRUCT_FIELD(m_HeadBone);
	STRUCT_FIELD(m_MaxAngularAcceleration);
	STRUCT_FIELD(m_MaxAngularVelocity);
	STRUCT_IGNORE(pad0);
	STRUCT_FIELD(m_FwdAxis);
	STRUCT_FIELD(m_UpAxis);
	STRUCT_END();
}
#endif

////////////////////////////////////////////////////////////////////////////////

crIKLimb::crIKLimb() :
crIKPart(),
m_TransferRollToUpperMid(0.5f),
m_TransferRollToLowerMid(0.5f),
m_TransferRollFromAppendage(1.0f),
m_SpineBone(NULL),
m_JointBone(NULL),
m_UpperLimbBone(NULL),
m_UpperLimbMidBone(NULL),
m_LowerLimbBone(NULL),
m_LowerLimbMidBone(NULL),
m_AppendageBone(NULL),
m_AppendageTipBone(NULL),
m_RollAxis(-Vec3V(V_Y_AXIS_WZERO)),
m_BendAxis(V_X_AXIS_WZERO),
m_LR(0),
pad0(0),
pad1(0),
pad2(0),
pad3(0),
pad4(0),
pad5(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crIKLimb::crIKLimb(datResource& rsc) : crIKPart(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

bool crIKLimb::Init(const crSkeletonData& skelData, const crJointData& jointData, const crIKBodyBoneFinder& boneFinder)
{
	m_SpineBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_SPINE, 0, IsLeftRight());
	if(!m_SpineBone)
	{
		Warningf("crIKLimb::Init - failed to find spine bone (isArm %d, isLeftRight %c)", IsArm(), IsLeftRight());
		return false;
	}

	m_JointBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_JOINT, 0, IsLeftRight());
	if(!m_JointBone)
	{
		Warningf("crIKLimb::Init - failed to find joint bone (isArm %d, isLeftRight %c)", IsArm(), IsLeftRight());
		return false;
	}

	m_UpperLimbBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_UPPER_LIMB, 0, IsLeftRight());
	if(!m_UpperLimbBone)
	{
		Warningf("crIKLimb::Init - failed to find upper limb bone (isArm %d, isLeftRight %c)", IsArm(), IsLeftRight());
		return false;
	}

	m_UpperLimbMidBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_UPPER_LIMB_MID, 0, IsLeftRight());

	m_LowerLimbBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_LOWER_LIMB, 0, IsLeftRight());
	if(!m_LowerLimbBone)
	{
		Warningf("crIKLimb::Init - failed to find lower limb bone (isArm %d, isLeftRight %c)", IsArm(), IsLeftRight());
		return false;
	}

	m_LowerLimbMidBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_LOWER_LIMB_MID, 0, IsLeftRight());

	m_AppendageBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_APPENDAGE, 0, IsLeftRight());
	if(!m_AppendageBone)
	{
		Warningf("crIKLimb::Init - failed to find appendage bone (isArm %d, isLeftRight %c)", IsArm(), IsLeftRight());
		return false;
	}

	m_AppendageTipBone = boneFinder.FindBone(skelData, IK_LIMB_BONE_APPENDAGE_TIP, 0, IsLeftRight());

	Vec3V offset = skelData.GetDefaultTransform(m_AppendageBone->GetIndex()).GetCol3();
	offset = Normalize(offset);

	u32 DOFs = m_LowerLimbBone->GetDofs();

	// TODO -- to avoid LHS stalls, just compare like this: IsLessThanAll(Abs(offset.GetX() + ScalarV(V_ONE)), ScalarV(V_FLT_SMALL_6))
	// etc.
	if ((DOFs & crBoneData::ROTATE_X) && fabsf(offset.GetXf() - 1.f) < SMALL_FLOAT)
		m_RollAxis = Vec3V(V_X_AXIS_WZERO);
	else if ((DOFs & crBoneData::ROTATE_X) && fabsf(offset.GetXf() + 1.f) < SMALL_FLOAT)
		m_RollAxis = -Vec3V(V_X_AXIS_WZERO);
	else if ((DOFs & crBoneData::ROTATE_Y) && fabsf(offset.GetYf() - 1.f) < SMALL_FLOAT)
		m_RollAxis = Vec3V(V_Y_AXIS_WZERO);
	else if ((DOFs & crBoneData::ROTATE_Y) && fabsf(offset.GetYf() + 1.f) < SMALL_FLOAT)
		m_RollAxis = -Vec3V(V_Y_AXIS_WZERO);
	else if ((DOFs & crBoneData::ROTATE_Z) && fabsf(offset.GetZf() - 1.f) < SMALL_FLOAT)
		m_RollAxis = Vec3V(V_Z_AXIS_WZERO);
	else if ((DOFs & crBoneData::ROTATE_Z) && fabsf(offset.GetZf() + 1.f) < SMALL_FLOAT)
		m_RollAxis = -Vec3V(V_Z_AXIS_WZERO);
	else
		Warningf("Unable to determine roll axis, using defaults\n");

	const crJointRotationLimit* jointLimit = jointData.FindJointRotationLimit(m_LowerLimbBone->GetBoneId());
	Assert(jointLimit);

	Vec3V minRot, maxRot;
	AssertVerify(jointLimit->ConvertToEulers(minRot, maxRot));

	if (m_RollAxis.GetXf() == 0 && (DOFs & crBoneData::ROTATE_X) && (minRot.GetXf() < 0 || maxRot.GetXf() > 0))
	{
		// TODO -- to avoid LHS stalls, just copy the inverted sign of maxRot.GetX(), e.g. like this:
		// m_BendAxis = Vec3V(V_X_AXIS_WZERO) | (InvertBits(maxRot.GetX()) & Vec3V(V_80000000));
		// etc.
		if (maxRot.GetXf() > 0)
			m_BendAxis = -Vec3V(V_X_AXIS_WZERO);
		else
			m_BendAxis = Vec3V(V_X_AXIS_WZERO);
	}
	else if (m_RollAxis.GetYf() == 0 && (DOFs & crBoneData::ROTATE_Y) && (minRot.GetYf() < 0 || maxRot.GetYf() > 0))
	{
		if (maxRot.GetYf() > 0)
			m_BendAxis = -Vec3V(V_Y_AXIS_WZERO);
		else
			m_BendAxis = Vec3V(V_Y_AXIS_WZERO);
	}
	else if (m_RollAxis.GetZf() == 0 && (DOFs & crBoneData::ROTATE_Z) && (minRot.GetZf() < 0 || maxRot.GetZf() > 0))
	{
		if (maxRot.GetZf() > 0)
			m_BendAxis = -Vec3V(V_Z_AXIS_WZERO);
		else
			m_BendAxis = Vec3V(V_Z_AXIS_WZERO);
	}
	else
	{
		Warningf("Unable to determine bend axis (missing joint limits), using defaults\n");
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

const crBoneData* crIKLimb::GetBone(int i) const
{
	switch(i)
	{
	case IK_LIMB_BONE_SPINE:
		return m_SpineBone;
	case IK_LIMB_BONE_JOINT:
		return m_JointBone;
	case IK_LIMB_BONE_UPPER_LIMB:
		return m_UpperLimbBone;
	case IK_LIMB_BONE_UPPER_LIMB_MID:
		return m_UpperLimbMidBone;
	case IK_LIMB_BONE_LOWER_LIMB:
		return m_LowerLimbBone;
	case IK_LIMB_BONE_LOWER_LIMB_MID:
		return m_LowerLimbMidBone;
	case IK_LIMB_BONE_APPENDAGE:
		return m_AppendageBone;
	case IK_LIMB_BONE_APPENDAGE_TIP:
		return m_AppendageTipBone;
	default:
		AssertMsg(0 , "crIKLimb::GetBone - unknown bone");
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crIKLimb::DeclareStruct(datTypeStruct &s)
{
	crIKPart::DeclareStruct(s);

	STRUCT_BEGIN(crIKLimb);
	STRUCT_FIELD(m_TransferRollToUpperMid);
	STRUCT_FIELD(m_TransferRollToLowerMid);
	STRUCT_FIELD(m_TransferRollFromAppendage);
	STRUCT_FIELD(m_SpineBone);
	STRUCT_FIELD(m_JointBone);
	STRUCT_FIELD(m_UpperLimbBone);
	STRUCT_FIELD(m_UpperLimbMidBone);
	STRUCT_FIELD(m_LowerLimbBone);
	STRUCT_FIELD(m_LowerLimbMidBone);
	STRUCT_FIELD(m_AppendageBone);
	STRUCT_FIELD(m_AppendageTipBone);
	STRUCT_FIELD(m_RollAxis);
	STRUCT_FIELD(m_BendAxis);
	STRUCT_FIELD(m_LR);
	STRUCT_IGNORE(pad0);
	STRUCT_IGNORE(pad1);
	STRUCT_IGNORE(pad2);
	STRUCT_IGNORE(pad3);
	STRUCT_IGNORE(pad4);
	STRUCT_IGNORE(pad5);
	STRUCT_END();
}
#endif

////////////////////////////////////////////////////////////////////////////////

crIKArm::crIKArm()
{
	m_BendAxis = -Vec3V(V_X_AXIS_WZERO);
}

////////////////////////////////////////////////////////////////////////////////

crIKArm::crIKArm(datResource& rsc) : crIKLimb(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crIKLeg::crIKLeg()
{
	m_BendAxis = Vec3V(V_X_AXIS_WZERO);
}

////////////////////////////////////////////////////////////////////////////////

crIKLeg::crIKLeg(datResource& rsc) : crIKLimb(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if 1 // new style skeleton names

const char* crIKBodyBoneFinderByName::sm_DefaultSpineBoneNames[] =
{ "#0", "spine", "pelvis" };
const char* crIKBodyBoneFinderByName::sm_DefaultHeadBoneNames[] =
{ "spine+", "neck", "head" };
const char* crIKBodyBoneFinderByName::sm_DefaultArmBoneNames[] =
{ "spine+", "clavicle", "arm", "armroll", "elbow", "wristroll", "wrist", 0 };
const char* crIKBodyBoneFinderByName::sm_DefaultLegBoneNames[] =
{ "spine", "pelvis", "hip", "hiproll", "knee", 0, "ankle", "ball" };

#else // old skeleton names

const char* crIKBodyBoneFinderByName::sm_DefaultSpineBoneNames[] =
{ "#0", "Spine", "Pelvis" };
const char* crIKBodyBoneFinderByName::sm_DefaultHeadBoneNames[] =
{ "Spine+", "Neck", "Head" };
const char* crIKBodyBoneFinderByName::sm_DefaultArmBoneNames[] =
{ "Spine+", "Shoulder", "Arm", "ArmRoll", "ForeArm", "ForeArmRoll", "Hand", 0 };
const char* crIKBodyBoneFinderByName::sm_DefaultLegBoneNames[] =
{ "Spine", "Pelvis", "UpLeg", 0, "Leg", 0, "Foot", "Toes" };

#endif

////////////////////////////////////////////////////////////////////////////////

crIKBodyBoneFinderByName::crIKBodyBoneFinderByName(const char** boneNames, int numNames, const char** boneFormats, int numFormats)
{
	m_BoneNames.Reserve(numNames);
	for(int i=0; i<numNames; i++)
	{
		m_BoneNames.Append() = boneNames[i];
	}

	m_BoneFormats.Reserve(numFormats);
	for(int i=0; i<numFormats; i++)
	{
		m_BoneFormats.Append() = boneFormats[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

// when writing bone name formats, use the following string substitutions

// %s is the bone name
// %d is the bone index
// %D is the bone index with a leading 0
// %l is the side, either 'l' or 'r'
// %L is the side, either "left" or "right"

// and try to provide an example of what it matches

const char* crIKBodyBoneFinderByName::sm_DefaultBoneFormats[] =
{
	// R* SD Formats
	"%s",				// head
	"%s%D",				// spine01
	"%s%d",				// unused (would match something like spine1)

	"%s_%l",			// clavicle_l
	"%s%D_%l",			// unused (would match something like clavicle01_l)
	"%s%d_%l",			// unused (would match something like clavicle1_l )

	"%s%L",				// unused (would match something like clavicleleft)
	"%s%d%L",			// unused (would match something like clavicle01left)
	"%s%d%L",			// unused (would match something like clavicle1left)

	// R* North Formats
	"char_%s",			// Char_Spine
	"char_%s%d",		// Char_Spine1
	"char_%l_%s",		// Char_L_Clavicle
	"char_%l_%s%d"		// Char_L_Toe0
};	

const int crIKBodyBoneFinderByName::sm_NumDefaultBoneFormats =
	sizeof(crIKBodyBoneFinderByName::sm_DefaultBoneFormats) / sizeof(crIKBodyBoneFinderByName::sm_DefaultBoneFormats[0]);

////////////////////////////////////////////////////////////////////////////////

bool crIKBodyBoneFinderByName::FormatBoneName(char* buffer, int bufferSize, const char* format, const char* name, int num, char lr)
{
	bool flag = false, usedNum = false;
	char temp[128];
	char c;

	do
	{
		c = *format++;

		if (flag)
		{
			switch (c)
			{
			case 's':
				formatf(temp, "%s", name);
				break;
			case 'd':
				formatf(temp, "%d", num);
				usedNum = true;
				break;
			case 'D':
				formatf(temp, "%02d", num);
				usedNum = true;
				break;
			case 'l':
				if (!lr)
				{
					return false;
				}
				formatf(temp, "%c", lr);
				break;
			case 'L':
				if (!lr)
				{
					return false;
				}
				formatf(temp, "%s", (tolower(lr) == 'l') ? "left" : "right");
				break;
			default:
				formatf(temp, "%c", c);
				break;
			}
			safecat(buffer, temp, bufferSize);

			flag = false;
		}
		else if (c == '%')
		{
			flag = true;
		}
		else
		{
			formatf(temp, "%c", c);
			safecat(buffer, temp, bufferSize);
		}
	}
	while (c);

	return (num == 0 || usedNum);
}

////////////////////////////////////////////////////////////////////////////////

const crBoneData* crIKBodyBoneFinderByName::FindBoneWithName(const crSkeletonData& skelData, const char* name, int num, const char lr) const
{
	const int bufSize = 128;
	char buf[bufSize];

	for (int i = 0; i < m_BoneFormats.GetCount(); i++)
	{
		buf[0] = '\0';

		if (!FormatBoneName(buf, bufSize, m_BoneFormats[i], name, num, lr))
		{
			continue;
		}

		for (int j = 0; j < skelData.GetNumBones(); j++)
		{
			const crBoneData *b = skelData.GetBoneData(j);

			if (stricmp(b->GetName(), buf) == 0)
				return b;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

int crIKBodyBoneFinderByName::HowManyWithName(const crSkeletonData& skelData, const char* name, const char lr) const
{
	int max=0;
	while(1)
	{
		if(FindBoneWithName(skelData, name, max+1, lr)==0)
			return max;
		max++;
	}
	/*NOTREACHED*/
	return max;
}

////////////////////////////////////////////////////////////////////////////////

void crIKBodyBoneFinderByName::GetStrippedBoneName(const char* origName, char* outStrippedName, bool& outIsIndex, bool& outIsLast) const
{
	Assert(origName);

	int len = (int)strlen(origName);

	outIsIndex = len && origName[0]=='#';
	outIsLast = len && !outIsIndex && origName[len-1]=='+';

	strncpy(outStrippedName, origName + (outIsIndex?1:0), len-((outIsLast||outIsIndex)?1:0));
	outStrippedName[len-((outIsLast||outIsIndex)?1:0)] = '\0';
}

////////////////////////////////////////////////////////////////////////////////

int crIKBodyBoneFinderByName::HowMany(const crSkeletonData& skelData, int partIdx, const char lr) const
{
	if(partIdx < m_BoneNames.GetCount() && m_BoneNames[partIdx])
	{
		char boneName[128];
		bool isIndex, isLast;
		GetStrippedBoneName(m_BoneNames[partIdx], boneName, isIndex, isLast);

		// if index, then there is only one bone, also if last just return 1
		if(isIndex || isLast)
		{
			return 1;
		}

		// return how many
		if(FindBoneWithName(skelData, boneName, 0, lr))
		{
			return HowManyWithName(skelData, boneName, lr) + 1;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

const crBoneData* crIKBodyBoneFinderByName::FindBone(const crSkeletonData& skelData, int partIdx, int boneIdx, const char lr) const
{
	if(partIdx < m_BoneNames.GetCount() && m_BoneNames[partIdx])
	{
		char boneName[128];
		bool isIndex, isLast;
		GetStrippedBoneName(m_BoneNames[partIdx], boneName, isIndex, isLast);

		// if index, then return bone directly
		if(isIndex)
		{
			int idx = atoi(boneName);
			Assert(idx >= 0 && idx < skelData.GetNumBones());
			return skelData.GetBoneData(idx);
		}

		// otherwise, find the bone
		if(boneIdx >= 0)
		{
			if(isLast)
			{
				boneIdx = HowManyWithName(skelData, boneName, lr);
			}

			return FindBoneWithName(skelData, boneName, boneIdx, lr);
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crIKBodyBoneFinderByBonePtr::crIKBodyBoneFinderByBonePtr(int numBoneTypes)
{
	aBonePtrs.Resize(numBoneTypes);
}

////////////////////////////////////////////////////////////////////////////////

void crIKBodyBoneFinderByBonePtr::AddBonePtr(int boneTypeIdx, const crBoneData* bd)
{
	aBonePtrs[boneTypeIdx].Grow() = bd;
}

////////////////////////////////////////////////////////////////////////////////

int crIKBodyBoneFinderByBonePtr::HowMany(const crSkeletonData&, int boneIdx, const char) const
{
	return aBonePtrs[boneIdx].GetCount();
}

////////////////////////////////////////////////////////////////////////////////

const crBoneData* crIKBodyBoneFinderByBonePtr::FindBone(const crSkeletonData&, int boneIdx, int idx, const char) const
{
	return aBonePtrs[boneIdx][idx];
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
