//
// crbody/iksolverbase.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//


#ifndef CRBODY_IKSOLVERBASE_H
#define CRBODY_IKSOLVERBASE_H

#include "vectormath/classes.h"

namespace rage
{


class crIKBodyBase;
class crSkeleton;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
//	Abstract base of IK solver.  Solvers employ different algorithms to take a
//	body part and modify it in an attempt to reach the specified goal.
class ALIGNAS(16) crIKSolverBase
{
public:
	// PURPOSE: Enumeration of different types of solvers
	enum crIKSolverType
	{
		IK_SOLVER_TYPE_INVALID,
		IK_SOLVER_TYPE_SIMPLE_SPINE,
		IK_SOLVER_TYPE_SIMPLE_HEAD,
		IK_SOLVER_TYPE_GLEICHER_LIMB,
		IK_SOLVER_TYPE_ITERATIVE_LIMB,
		IK_SOLVER_TYPE_ARMS,
		IK_SOLVER_TYPE_LEGS,
		IK_SOLVER_TYPE_QUADRUPED,
		IK_SOLVER_TYPE_CUSTOM,
		IK_SOLVER_TYPE_PROXY
	};

	// PURPOSE: Default constructor
	crIKSolverBase(crIKSolverType type);
	virtual ~crIKSolverBase() {}

	// PURPOSE: Helps with processing the solvers
	struct SolverHelper
	{
		Mat34V m_Parent;
		int m_NumBones;
		const s16* m_ParentIndices;
		Mat34V* m_Locals;
		Mat34V* m_Objects;

		void UpdateObjectMtxs();
	};

	// PURPOSE: Get the type of the derived solver
	crIKSolverType GetType() const { return m_Type; }

	// PURPOSE: Get the priority of the derived solver
	u32 GetPriority() const { return m_Priority; }

	// PURPOSE: Set the priority of the derived solver
	void SetPrority(u32 priority) { m_Priority = priority; }

	// PURPOSE: Iterate the solver from the config
	// NOTE: The method need to run on PPU
	virtual void Iterate(float, crSkeleton&) {}

	// PURPOSE: Evaluate the solver
	// NOTE: The method could run on SPU
	virtual void Solve(SolverHelper&) const {}

	// PURPOSE: Return true if the solver is finished.
	virtual bool IsDead() const { return false; }

	// PURPOSE: Reset the solver
	virtual void Reset() {}

#if __DEV
	// PURPOSE: Debug only function for rendering debug text and debug lines
	virtual void DebugRender() {}
#endif //__DEV

protected:
	crIKSolverType m_Type;
	u32 m_Priority;
	u32 m_Padding;
} ;

////////////////////////////////////////////////////////////////////////////////

inline void crIKSolverBase::SolverHelper::UpdateObjectMtxs()
{
	m_Objects[0] = m_Locals[0];
	for(int i=1; i<m_NumBones; ++i)
	{
		Transform(m_Objects[i], m_Objects[m_ParentIndices[i]], m_Locals[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRBODY_IKSOLVERBASE_H
