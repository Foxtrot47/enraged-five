//
// crbody/ikpart.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRBODY_IKPART_H
#define CRBODY_IKPART_H

#include "atl/array.h"
#include "vectormath/vec3v.h"

namespace rage
{

class crBoneData;
class crJointData;
class crSkeletonData;
class crIKBodyBoneFinder;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// 	A part is a limb or other section of the body that the IK system interacts with.
//	The IK system uses parts to abstract the specific layout of the
//	skeletal bone hierarchy into a general representation that the IK solvers can use.
class crIKPart
{
public:
	enum
	{
		IK_PART_TYPE_SPINE,
		IK_PART_TYPE_HEAD,
		IK_PART_TYPE_ARM,
		IK_PART_TYPE_LEG
	};

	crIKPart() {}
	crIKPart(datResource&) {}
	virtual ~crIKPart();

	virtual int GetBodyPartType() const = 0;

	virtual bool Init(const crSkeletonData&, const crJointData&, const crIKBodyBoneFinder&) =0;

	virtual int GetNumBones() const = 0;
	virtual const crBoneData* GetBone(int i) const = 0;
	
	virtual const crBoneData* GetPrimaryBone() const = 0;
	virtual const crBoneData* GetSecondaryBone() const  { return NULL; }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &);
#endif
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:	IK abstract representation of a spine.
class crIKSpine : public crIKPart
{
public:
	enum
	{
		IK_SPINE_BONE_ROOT,
		IK_SPINE_BONE_SPINE,
		IK_SPINE_BONE_PELVIS,
		IK_SPINE_BONE_NUM
	};

	crIKSpine();
	crIKSpine(datResource&);
	virtual ~crIKSpine() {}

	virtual int GetBodyPartType() const  { return IK_PART_TYPE_SPINE; }

	virtual bool Init(const crSkeletonData&, const crJointData&, const crIKBodyBoneFinder&);

	virtual int GetNumBones() const  { return IK_SPINE_BONE_NUM + GetNumSpineBones()-1; }
	virtual const crBoneData* GetBone(int i) const;

	virtual const crBoneData* GetPrimaryBone() const  { return GetSpineBone(GetNumSpineBones()-1); }

	const crBoneData* GetRootBone() const  { return m_RootBone; }
	int GetNumSpineBones() const  { return m_SpineBones.GetCount(); }
	const crBoneData* GetSpineBone(int i) const  { return i<GetNumSpineBones() ? m_SpineBones[i] : NULL; }
	const crBoneData* GetPelvisBone() const  { return m_PelvisBone; }

	// PURPOSE: Sets the up axis around which the neck rotates.
	void SetUpAxis(Vec3V_ConstRef upAxis) { m_UpAxis = upAxis; }
	Vec3V_ConstRef GetUpAxis() const { return m_UpAxis; }

	// PURPOSE: Set the forward axis which is the direction the head faces.
	void SetFwdAxis(Vec3V_ConstRef fwdAxis) { m_FwdAxis = fwdAxis; }
	Vec3V_ConstRef GetFwdAxis() const { return m_FwdAxis; }

	float GetMaxAngularAcceleration() const { return m_MaxAngularAcceleration; }
	float GetMaxAngularVelocity() const { return m_MaxAngularVelocity; }

	void SetMaxAngularAcceleration(float f) { m_MaxAngularAcceleration = f; }
	void SetMaxAngularVelocity(float f) { m_MaxAngularVelocity = f; }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif

protected:
	datRef<const crBoneData> m_RootBone;
	atArray<datRef<const crBoneData> > m_SpineBones;
	datRef<const crBoneData> m_PelvisBone;

	float m_MaxAngularAcceleration;
	float m_MaxAngularVelocity;

	u32 pad0;

	Vec3V m_FwdAxis;
	Vec3V m_UpAxis;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: IK abstract representation of a head.
class crIKHead : public crIKPart
{
public:
	enum
	{
		IK_HEAD_BONE_SPINE,
		IK_HEAD_BONE_NECK,
		IK_HEAD_BONE_HEAD,
		IK_HEAD_BONE_NUM
	};

	crIKHead();
	crIKHead(datResource&);
	virtual ~crIKHead() {}

	virtual int GetBodyPartType() const  { return IK_PART_TYPE_HEAD; }

	virtual bool Init(const crSkeletonData&, const crJointData&, const crIKBodyBoneFinder&);

	virtual int GetNumBones() const  { return IK_HEAD_BONE_NUM + GetNumNeckBones()-1; }
	virtual const crBoneData* GetBone(int i) const;

	virtual const crBoneData* GetPrimaryBone() const  { return m_HeadBone; }

	const crBoneData* GetSpineBone() const  { return m_SpineBone; }
	int GetNumNeckBones() const { return m_NeckBones.GetCount(); }
	const crBoneData* GetNeckBone(int i) const  { return i<GetNumNeckBones() ? m_NeckBones[i] : NULL; }
	const crBoneData* GetHeadBone() const  { return m_HeadBone; }

	// PURPOSE: Sets the up axis around which the neck rotates.
	void SetUpAxis(Vec3V_In upAxis) { m_UpAxis = upAxis; }
	Vec3V_ConstRef GetUpAxis() const { return m_UpAxis; }

	// PURPOSE: Set the forward axis which is the direction the head faces.
	void SetFwdAxis(Vec3V_In fwdAxis) { m_FwdAxis = fwdAxis; }
	Vec3V_ConstRef GetFwdAxis() const { return m_FwdAxis; }

	float GetMaxAngularAcceleration() const { return m_MaxAngularAcceleration; }
	float GetMaxAngularVelocity() const { return m_MaxAngularVelocity; }

	void SetMaxAngularAcceleration(float f) { m_MaxAngularAcceleration = f; }
	void SetMaxAngularVelocity(float f) { m_MaxAngularVelocity = f; }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif

protected:
	datRef<const crBoneData> m_SpineBone;
	atArray<datRef<const crBoneData> > m_NeckBones;
	datRef<const crBoneData> m_HeadBone;

	float m_MaxAngularAcceleration;
	float m_MaxAngularVelocity;

	u32 pad0;

	Vec3V m_FwdAxis;
	Vec3V m_UpAxis;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// 	IK abstracted Pentadactyl limb.
//	Generalized example of a arm/leg with upper and lower limb (with or without middle bones)
//	and an appendage (with or without a tip).
class crIKLimb : public crIKPart
{
public:
	enum
	{
		IK_LIMB_BONE_SPINE,
		IK_LIMB_BONE_JOINT,
		IK_LIMB_BONE_UPPER_LIMB,
		IK_LIMB_BONE_UPPER_LIMB_MID,
		IK_LIMB_BONE_LOWER_LIMB,
		IK_LIMB_BONE_LOWER_LIMB_MID,
		IK_LIMB_BONE_APPENDAGE,
		IK_LIMB_BONE_APPENDAGE_TIP,
		IK_LIMB_BONE_NUM
	};
	
	crIKLimb();
	crIKLimb(datResource&);
	virtual ~crIKLimb() {}

	virtual bool Init(const crSkeletonData&, const crJointData&, const crIKBodyBoneFinder&);

	virtual int GetNumBones() const  { return IK_LIMB_BONE_NUM; }
	virtual const crBoneData* GetBone(int i) const;

	virtual const crBoneData* GetPrimaryBone() const  { return m_AppendageBone; }
	virtual const crBoneData* GetSecondaryBone() const  { return m_AppendageTipBone; }

	const crBoneData* GetSpineBone() const  { return m_SpineBone; }
	const crBoneData* GetJointBone() const  { return m_JointBone; }
	const crBoneData* GetUpperLimbBone() const  { return m_UpperLimbBone; }
	const crBoneData* GetUpperLimbMidBone() const  { return m_UpperLimbMidBone; }
	const crBoneData* GetLowerLimbBone() const  { return m_LowerLimbBone; }
	const crBoneData* GetLowerLimbMidBone() const  { return m_LowerLimbMidBone; }
	const crBoneData* GetAppendageBone() const  { return m_AppendageBone; }
	const crBoneData* GetAppendageTipBone() const  { return m_AppendageTipBone; }

	virtual bool IsArm() const { return false; }
	virtual bool IsLeg() const { return false; }

	char IsLeftRight() const { return m_LR; }
	void SetLeftRight(char lr) { m_LR = lr; }

	// PURPOSE: Sets the axis the roll bones rotate around.
	void SetRollAxis(Vec3V_In rollAxis) { m_RollAxis = rollAxis; }
	Vec3V_ConstRef GetRollAxis() const { return m_RollAxis; }

	// PURPOSE: Sets the axis the knee or elbow joint bends around.
	void SetBendAxis(Vec3V_In bendAxis) { m_BendAxis = bendAxis; }
	Vec3V_ConstRef GetBendAxis() const { return m_BendAxis; }

	// PURPOSE: Sets how much of the roll/twist rotation is transfered onto
	// the upper mid bones post IK solve [0..1] 0->100%
	void SetTransferRollToUpperMid(float f) { m_TransferRollToUpperMid = f; }

	// PURPOSE: Sets how much of the roll/twist rotation is transfered onto
	// the lower mid bones post IK solve [0..1] 0->100%
	void SetTransferRollToLowerMid(float f) { m_TransferRollToLowerMid = f; }

	// PURPOSE: Sets how much of the roll/twist rotation on the appendage is
	// transfered onto the lower mid bones post IK solve [0..1] 0->100%
	void SetTransferRollFromAppendage(float f) { m_TransferRollFromAppendage = f; }

	float GetTransferRollToUpperMid() const { return m_TransferRollToUpperMid ; }
	float GetTransferRollToLowerMid() const { return m_TransferRollToLowerMid ; }
	float GetTransferRollFromAppendage() const { return m_TransferRollFromAppendage ; }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif
	
protected:
	float m_TransferRollToUpperMid;
	float m_TransferRollToLowerMid;
	float m_TransferRollFromAppendage;

	datRef<const crBoneData> m_SpineBone;
	datRef<const crBoneData> m_JointBone;
	datRef<const crBoneData> m_UpperLimbBone;
	datRef<const crBoneData> m_UpperLimbMidBone;
	datRef<const crBoneData> m_LowerLimbBone;
	datRef<const crBoneData> m_LowerLimbMidBone;
	datRef<const crBoneData> m_AppendageBone;
	datRef<const crBoneData> m_AppendageTipBone;

	Vec3V m_RollAxis;
	Vec3V m_BendAxis;

	char m_LR;
	u8 pad0, pad1, pad2;

	u32 pad3, pad4, pad5;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: IK abstract representation of an arm, a specialization of a limb.
class crIKArm : public crIKLimb
{
public:
	enum
	{
		IK_ARM_BONE_SPINE,
		IK_ARM_BONE_CLAVICLE,
		IK_ARM_BONE_SHOULDER,
		IK_ARM_BONE_SHOULDER_MID,
		IK_ARM_BONE_ELBOW,
		IK_ARM_BONE_ELBOW_MID,
		IK_ARM_BONE_WRIST,
		IK_ARM_BONE_FINGER,
		IK_ARM_BONE_NUM
	};

	crIKArm();
	crIKArm(datResource&);
	virtual ~crIKArm() {}

	virtual int GetBodyPartType() const  { return IK_PART_TYPE_ARM; }

	const crBoneData* GetClavicleBone() const  { return m_JointBone; }
	const crBoneData* GetShoulderBone() const  { return m_UpperLimbBone; }
	const crBoneData* GetShoulderMidBone() const  { return m_UpperLimbMidBone; }
	const crBoneData* GetElbowBone() const  { return m_LowerLimbBone; }
	const crBoneData* GetElbowMidBone() const  { return m_LowerLimbMidBone; }
	const crBoneData* GetWristBone() const  { return m_AppendageBone; }
	const crBoneData* GetFingerBone() const  { return m_AppendageTipBone; }

	virtual bool IsArm() const { return true; }
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: IK abstract representation of a leg, a specialization of a limb.
class crIKLeg : public crIKLimb
{
public:
	enum
	{
		IK_LEG_BONE_SPINE,
		IK_LEG_BONE_PELVIS,
		IK_LEG_BONE_HIP,
		IK_LEG_BONE_HIP_MID,
		IK_LEG_BONE_LEG,
		IK_LEG_BONE_LEG_MID,
		IK_LEG_BONE_ANKLE,
		IK_LEG_BONE_TOE,
		IK_LEG_BONE_NUM
	};

	crIKLeg();
	crIKLeg(datResource&);
	virtual ~crIKLeg() {}

	virtual int GetBodyPartType() const  { return IK_PART_TYPE_LEG; }

	const crBoneData* GetPelvisBone() const  { return m_JointBone; }
	const crBoneData* GetHipBone() const  { return m_UpperLimbBone; }
	const crBoneData* GetHipMidBone() const  { return m_UpperLimbMidBone; }
	const crBoneData* GetKneeBone() const  { return m_LowerLimbBone; }
	const crBoneData* GetKneeMidBone() const  { return m_LowerLimbMidBone; }
	const crBoneData* GetAnkleBone() const  { return m_AppendageBone; }
	const crBoneData* GetToeBone() const  { return m_AppendageTipBone; }

	virtual bool IsLeg() const { return true; }
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base class of a bone finder class.
class crIKBodyBoneFinder
{
public:
	virtual ~crIKBodyBoneFinder() {}

	virtual int HowMany(const crSkeletonData& skelData, int boneIdx, const char lr) const = 0;
	virtual const crBoneData* FindBone(const crSkeletonData& skelData, int boneIdx, int idx, const char lr) const = 0;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:	A bone finder that works using names.
// NOTES:
//	Set up with list of bones names, will add left/right/l/r and numbers to find matches.
//	'#' at the start means what follows is an index, not a name.
//	'+' at the end means that last numbered bone with that name should be returned.
//
//	You can also set a custom set of bone format rules.  Bone format rules
//	specify how and where to place Left/Right and bone index number when
//	searching for a bone in the skeleton.
//
//	When writing bone name formats, use the following string substitutions:
//
//	%s is the bone name
//	%d is the bone index
//	%D is the bone index with a leading 0
//	%l is the side, either 'l' or 'r'
//	%L is the side, either "left" or "right"
//
//	See crIKBodyBoneFinderByName::sm_DefaultBoneFormats for examples.
class crIKBodyBoneFinderByName : public crIKBodyBoneFinder
{
public:
	crIKBodyBoneFinderByName(const char** boneNames, int numNames,
							 const char** boneFormats = sm_DefaultBoneFormats,
							 int numFormats = sm_NumDefaultBoneFormats);
	virtual ~crIKBodyBoneFinderByName() {}

public:
	virtual int HowMany(const crSkeletonData& skelData, int boneIdx, const char lr) const;
	virtual const crBoneData* FindBone(const crSkeletonData& skelData, int boneIdx, int idx, const char lr) const;

	// standard names - standard arrays of names used by system for each body part
	static const char* sm_DefaultSpineBoneNames[crIKSpine::IK_SPINE_BONE_NUM];
	static const char* sm_DefaultHeadBoneNames[crIKHead::IK_HEAD_BONE_NUM];
	static const char* sm_DefaultArmBoneNames[crIKArm::IK_ARM_BONE_NUM];
	static const char* sm_DefaultLegBoneNames[crIKLeg::IK_LEG_BONE_NUM];

	static const char* sm_DefaultBoneFormats[];
	static const int   sm_NumDefaultBoneFormats;

protected:
	const crBoneData* FindBoneWithName(const crSkeletonData& skelData, const char* name, int idx, const char lr) const;
	int HowManyWithName(const crSkeletonData& skelData, const char* name, const char lr) const;
	void GetStrippedBoneName(const char* origName, char* outStrippedName, bool& outIsIndex, bool& outIsLast) const;
	static bool FormatBoneName(char* buffer, int bufferSize, const char* format, const char* name, int num, char lr);

	atArray<const char*> m_BoneNames;
	atArray<const char*> m_BoneFormats;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:	A bone finder that works using bone pointers.
// NOTES:
//	Contains an array of arrays of bone pointers.
//	Initialize with the number of bone "types" (ie head has 3 types, spine, neck and head).
//  Then call AddBonePtr repeatedly, adding bones for each bone "type"
//	(ie you might add multiple neck bones - in which case be careful to call in hierarchy order).
class crIKBodyBoneFinderByBonePtr : public crIKBodyBoneFinder
{
public:
	crIKBodyBoneFinderByBonePtr(int numBoneTypes);
	virtual ~crIKBodyBoneFinderByBonePtr() {}

	void AddBonePtr(int boneTypeIdx, const crBoneData*);

public:
	virtual int HowMany(const crSkeletonData& skelData, int boneIdx, const char lr) const;
	virtual const crBoneData* FindBone(const crSkeletonData& skelData, int boneIdx, int idx, const char lr) const;

protected:
	atArray< atArray<const crBoneData*> > aBonePtrs;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRBODY_IKPART_H
