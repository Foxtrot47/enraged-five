//
// crbody/kinematics.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef CRBODY_KINEMATICS_H
#define CRBODY_KINEMATICS_H

#include "atl/array.h"

namespace rage
{

class crIKSolverBase;
class crSkeleton;

// PURPOSE: Main IK class containing a linked list of ik solvers.
class ALIGNAS(16) crKinematics
{
public:
	// PURPOSE:	Default constructor and destructor.
	crKinematics();
	~crKinematics();

	// PURPOSE:	Initializes the class.
	// PARAMS:
	//	skel - skeleton that this kinematics class will operate on.
	//	body - body file that maps this skeleton (data) to the IK system. (can be null)
	void Init(crSkeleton& skel);

	// PURPOSE: Iterate the solver from the config
	// NOTE: The method need to run on PPU
	void Iterate(float dt);

	// PURPOSE: Evaluate the solver
	// NOTE: The method could run on SPU
	void Solve() const;

	// PURPOSE: Get a solvers
	const crIKSolverBase* GetSolver(u32 i) const;

	// PURPOSE: Get array of solvers
	crIKSolverBase** GetSolvers();

	// PURPOSE: Check if any solvers present
	bool HasSolvers() const;

	// PURPOSE: Remove all existing solvers
	void RemoveAllSolvers();

	// PURPOSE: Check if a specific solver is already present
	bool HasSolver(crIKSolverBase* solver) const;

	// PURPOSE: Add a new solver
	void AddSolver(crIKSolverBase* solver);

	// PURPOSE: Remove a existing solver
	void RemoveSolver(crIKSolverBase* solver);

	// PURPOSE: Get number solvers
	u32 GetNumSolvers() const;

	// ====================== Platform Dependent =========================
	// PURPOSE: Maximum number of solvers per kinematics
	static const u32 sm_MaxNumSolvers = (RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 30 : 10;
	// ====================== Platform Dependent =========================

protected:
	atFixedArray<crIKSolverBase*, sm_MaxNumSolvers> m_Solvers;
	crSkeleton* m_Skeleton;
} ;

////////////////////////////////////////////////////////////////////////////////

inline const crIKSolverBase* crKinematics::GetSolver(u32 i) const
{
	return m_Solvers[i];
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crKinematics::GetNumSolvers() const
{
	return m_Solvers.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline crIKSolverBase** crKinematics::GetSolvers()
{
	return m_Solvers.GetElements();
}

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage

#endif // CRBODY_KINEMATICS_H

