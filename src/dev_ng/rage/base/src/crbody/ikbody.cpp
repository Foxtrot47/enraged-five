//
// crbody/ikbody.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "ikbody.h"

#include "atl/array_struct.h"
#include "data/resource.h"
#include "data/resourcehelpers.h"
#include "system/memory.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crIKBodyBase::crIKBodyBase(datResource& )
{
}

////////////////////////////////////////////////////////////////////////////////

bool crIKBodyBase::Init(const crSkeletonData& skelData, const crJointData& jointData, atArray<const crIKBodyBoneFinder*>& boneFinders)
{
	m_SkeletonData = &skelData;

	bool success = true;
	int num = GetNumBodyParts();
	if(boneFinders.GetCount() == num)
	{
		for(int i=0; i<num; i++)
		{
			const crIKBodyBoneFinder* boneFinder = boneFinders[i];
			success = success && boneFinder && const_cast<crIKPart*>(GetBodyPart(i))->Init(skelData, jointData, *boneFinder);
			if(!boneFinder)
			{
				Warningf("crIKBodyBase::Init - missing bone finder for body part %d", i);
			}
		}
	}
	else
	{
		Warningf("crIKBodyBase::Init - mismatch between number of bone finders %d and body parts %d", boneFinders.GetCount(), num);
	}
	return success;
}	

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crIKBodyBase::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(crIKBodyBase);
	STRUCT_FIELD(m_SkeletonData);
	STRUCT_END();
}
#endif

////////////////////////////////////////////////////////////////////////////////

crIKBodyHumanoid::~crIKBodyHumanoid()
{	
	delete m_Arms;
	delete m_Legs;
}

////////////////////////////////////////////////////////////////////////////////

crIKBodyHumanoid::crIKBodyHumanoid()
{
	m_Arms=rage_new atRangeArray<crIKArm,IK_HUMANOID_LIMB_NUM>;
	m_Legs=rage_new atRangeArray<crIKLeg,IK_HUMANOID_LIMB_NUM>;

	(*m_Arms)[IK_HUMANOID_LIMB_LEFT].SetLeftRight('l');
	(*m_Arms)[IK_HUMANOID_LIMB_RIGHT].SetLeftRight('r');
	(*m_Legs)[IK_HUMANOID_LIMB_LEFT].SetLeftRight('l');
	(*m_Legs)[IK_HUMANOID_LIMB_RIGHT].SetLeftRight('r');
}

////////////////////////////////////////////////////////////////////////////////

crIKBodyHumanoid::crIKBodyHumanoid(datResource &rsc) : crIKBodyBase(rsc), m_Spine(rsc), m_Head(rsc)
{
	rsc.PointerFixup(m_Arms);
	rsc.PointerFixup(m_Legs);

	for(int i=0; i<IK_HUMANOID_LIMB_NUM; i++)
	{
		::new (&((*m_Arms)[i])) crIKArm(rsc);
		::new (&((*m_Legs)[i])) crIKLeg(rsc);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crIKBodyHumanoid::InitByDefaultNames(const crSkeletonData& skelData, const crJointData& jointData)
{
	sysMemStartTemp();
	crIKBodyBoneFinderByName* findSpine = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultSpineBoneNames, crIKSpine::IK_SPINE_BONE_NUM);
	crIKBodyBoneFinderByName* findHead = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultHeadBoneNames, crIKHead::IK_HEAD_BONE_NUM);
	crIKBodyBoneFinderByName* findArm = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultArmBoneNames, crIKArm::IK_ARM_BONE_NUM);
	crIKBodyBoneFinderByName* findLeg = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultLegBoneNames, crIKLeg::IK_LEG_BONE_NUM);

	atArray<const crIKBodyBoneFinder*> finders;
	finders.Reserve(6);
	finders.Append() = findSpine;
	finders.Append() = findHead;
	finders.Append() = findArm;
	finders.Append() = findArm;
	finders.Append() = findLeg;
	finders.Append() = findLeg;
	sysMemEndTemp();

	bool result = Init(skelData, jointData, finders);

	sysMemStartTemp();
	finders.Reset();
	delete findSpine;
	delete findHead;
	delete findArm;
	delete findLeg;
	sysMemEndTemp();

	return result;
}

////////////////////////////////////////////////////////////////////////////////

const crIKPart* crIKBodyHumanoid::GetBodyPart(int i) const
{
	switch(i)
	{
	case IK_HUMANOID_PART_SPINE:
		return &m_Spine;
	case IK_HUMANOID_PART_HEAD:
		return &m_Head;
	case IK_HUMANOID_PART_ARM_LEFT:
	case IK_HUMANOID_PART_ARM_RIGHT:
		return &(*m_Arms)[i-IK_HUMANOID_PART_ARM_LEFT];
	case IK_HUMANOID_PART_LEG_LEFT:
	case IK_HUMANOID_PART_LEG_RIGHT:
		return &(*m_Legs)[i-IK_HUMANOID_PART_LEG_LEFT];
	default:
		AssertMsg(0 , "crIKBodyHumanoid::GetBodyPart - missing case!");
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crIKBodyHumanoid::DeclareStruct(datTypeStruct &s)
{
	crIKBodyBase::DeclareStruct(s);

	STRUCT_BEGIN(crIKBodyHumanoid);
	STRUCT_CONTAINED_ARRAY(m_Arms);
	STRUCT_CONTAINED_ARRAY(m_Legs);
	STRUCT_FIELD(m_Spine);
	STRUCT_FIELD(m_Head);
	STRUCT_END();
}
#endif

////////////////////////////////////////////////////////////////////////////////

crIKBodyQuadruped::~crIKBodyQuadruped()
{	
	delete m_Legs;
}

////////////////////////////////////////////////////////////////////////////////

crIKBodyQuadruped::crIKBodyQuadruped() : pad0(0)
{
	m_Legs=rage_new atRangeArray<crIKLeg,IK_QUADRUPED_LEG_NUM>;

	(*m_Legs)[IK_QUADRUPED_LEG_FRONT_LEFT].SetLeftRight('l');
	(*m_Legs)[IK_QUADRUPED_LEG_FRONT_RIGHT].SetLeftRight('r');
	(*m_Legs)[IK_QUADRUPED_LEG_BACK_LEFT].SetLeftRight('l');
	(*m_Legs)[IK_QUADRUPED_LEG_BACK_RIGHT].SetLeftRight('r');
}

////////////////////////////////////////////////////////////////////////////////

crIKBodyQuadruped::crIKBodyQuadruped(datResource &rsc) : crIKBodyBase(rsc), m_Spine(rsc), m_Head(rsc)
{
	rsc.PointerFixup(m_Legs);

	for(int i=0; i<IK_QUADRUPED_LEG_NUM; i++)
	{
		::new (&((*m_Legs)[i])) crIKLeg(rsc);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crIKBodyQuadruped::InitByDefaultNames(const crSkeletonData& skelData, const crJointData& jointData)
{
	sysMemStartTemp();
	crIKBodyBoneFinderByName* findSpine = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultSpineBoneNames, crIKSpine::IK_SPINE_BONE_NUM);
	crIKBodyBoneFinderByName* findHead = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultHeadBoneNames, crIKHead::IK_HEAD_BONE_NUM);
	crIKBodyBoneFinderByName* findArm = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultArmBoneNames, crIKArm::IK_ARM_BONE_NUM);
	crIKBodyBoneFinderByName* findLeg = rage_new crIKBodyBoneFinderByName(crIKBodyBoneFinderByName::sm_DefaultLegBoneNames, crIKLeg::IK_LEG_BONE_NUM);

	atArray<const crIKBodyBoneFinder*> finders;
	finders.Reserve(6);
	finders.Append() = findSpine;
	finders.Append() = findHead;
	finders.Append() = findArm;
	finders.Append() = findArm;
	finders.Append() = findLeg;
	finders.Append() = findLeg;
	sysMemEndTemp();

	bool result = Init(skelData, jointData, finders);

	sysMemStartTemp();
	finders.Reset();
	delete findSpine;
	delete findHead;
	delete findArm;
	delete findLeg;
	sysMemEndTemp();

	return result;
}

////////////////////////////////////////////////////////////////////////////////

const crIKPart* crIKBodyQuadruped::GetBodyPart(int i) const
{
	switch(i)
	{
	case IK_QUADRUPED_PART_SPINE:
		return &m_Spine;
	case IK_QUADRUPED_PART_HEAD:
		return &m_Head;
	case IK_QUADRUPED_PART_LEG_FRONT_LEFT:
	case IK_QUADRUPED_PART_LEG_FRONT_RIGHT:
	case IK_QUADRUPED_PART_LEG_BACK_LEFT:
	case IK_QUADRUPED_PART_LEG_BACK_RIGHT:
		return &(*m_Legs)[i-IK_QUADRUPED_PART_LEG_FRONT_LEFT];
	default:
		AssertMsg(0 , "crIKBodyQuadruped::GetBodyPart - missing case!");
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crIKBodyQuadruped::DeclareStruct(datTypeStruct &s)
{
	crIKBodyBase::DeclareStruct(s);

	STRUCT_BEGIN(crIKBodyQuadruped);
	STRUCT_CONTAINED_ARRAY(m_Legs);
	STRUCT_IGNORE(pad0);
	STRUCT_FIELD(m_Spine);
	STRUCT_FIELD(m_Head);
	STRUCT_END();
}
#endif

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
