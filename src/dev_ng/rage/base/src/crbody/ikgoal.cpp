//
// crbody/ikgoal.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "ikgoal.h"

#include "ikpart.h"

#include "crskeleton/skeleton.h"
#include "crskeleton/bonedata.h"
#include "vectormath/classes.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crIKGoal::crIKGoal()
: m_PrimaryPos(V_ZERO)
, m_SecondaryPos(V_ZERO)
, m_Orientation(V_IDENTITY)
, m_Blend(0.f)
, m_Twist(0.f)
, m_Balance(0.f)
, m_Flags(0)
, m_Result(0)
, pad0(0)
{
}

////////////////////////////////////////////////////////////////////////////////

void crIKGoal::Update(crSkeleton& skeleton, const crIKPart& part)
{
	if((m_Flags & (IKGOAL_USE_ORIENTATION|IKGOAL_FIXED_ORIENTATION)) == (IKGOAL_USE_ORIENTATION|IKGOAL_FIXED_ORIENTATION))
	{
		Mat34V primaryBoneMtx;
		skeleton.GetGlobalMtx(part.GetPrimaryBone()->GetIndex(), primaryBoneMtx);
		m_Orientation = QuatVFromMat33V(primaryBoneMtx.GetMat33());
	}
	if((m_Flags & (IKGOAL_USE_POSITION|IKGOAL_FIXED_POSITION)) == (IKGOAL_USE_POSITION|IKGOAL_FIXED_POSITION))
	{
		Mat34V primaryBoneMtx;
		skeleton.GetGlobalMtx(part.GetPrimaryBone()->GetIndex(), primaryBoneMtx);
		m_PrimaryPos = primaryBoneMtx.GetCol3();
		// TODO --- secondary position?
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void rage::crIKGoal::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(crIKGoal);
	STRUCT_FIELD(m_PrimaryPos);
	STRUCT_FIELD(m_SecondaryPos);
	STRUCT_FIELD(m_Orientation);
	STRUCT_FIELD(m_Blend);
	STRUCT_FIELD(m_Twist);
	STRUCT_FIELD(m_Balance);
	STRUCT_FIELD(m_Flags);
	STRUCT_FIELD(m_Result);
	STRUCT_IGNORE(pad0);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

