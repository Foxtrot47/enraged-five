//
// crbody/ikbody.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRBODY_IKBODY_H
#define CRBODY_IKBODY_H

#include "crbody/ikpart.h"

namespace rage
{

class crIKBodyBoneFinder;
class crJointData;
class crSkeletonData;
class datResource;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// 	Abstract base class for the representation of a body, for the purposes of IK.
//	A body consists of a number of body members (ie spine, head, legs and arms).
//	Body parts map an IK abstracted view of a part of the body onto the
//	actual skeleton).
//	The base provides an initialization function and functions allowing iteration
//	over the body members.
class crIKBodyBase
{
public:
	crIKBodyBase() : m_SkeletonData(0) {}
	crIKBodyBase(datResource&);
	virtual ~crIKBodyBase() {}

	virtual bool Init(const crSkeletonData& skelData, const crJointData& jointData, atArray<const crIKBodyBoneFinder*>& boneFinders);

	virtual int GetNumBodyParts() const = 0;
	virtual const crIKPart* GetBodyPart(int) const = 0;

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif

protected:
	datRef<const crSkeletonData> m_SkeletonData;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: A representation of a humanoid body.  One spine, one head, two arms and two legs.
class crIKBodyHumanoid : public crIKBodyBase
{
public:
	enum
	{
		IK_HUMANOID_PART_SPINE,
		IK_HUMANOID_PART_HEAD,
		IK_HUMANOID_PART_ARM_LEFT,
		IK_HUMANOID_PART_ARM_RIGHT,
		IK_HUMANOID_PART_LEG_LEFT,
		IK_HUMANOID_PART_LEG_RIGHT,
		IK_HUMANOID_PART_NUM
	};
	enum
	{
		IK_HUMANOID_LIMB_LEFT,
		IK_HUMANOID_LIMB_RIGHT,
		IK_HUMANOID_LIMB_NUM
	};

	crIKBodyHumanoid();
	crIKBodyHumanoid(datResource&);
	virtual ~crIKBodyHumanoid();

	IMPLEMENT_PLACE_INLINE(crIKBodyHumanoid);

	virtual bool InitByDefaultNames(const crSkeletonData& skelData, const crJointData& jointData);

	virtual int GetNumBodyParts() const { return IK_HUMANOID_PART_NUM; }
	virtual const crIKPart* GetBodyPart(int index) const;

	const crIKSpine* GetSpine() const { return &m_Spine; }
	const crIKHead* GetHead() const { return &m_Head; }
	const crIKArm* GetArm(int i) const { return &(*m_Arms)[i]; }
	const crIKLeg* GetLeg(int i) const { return &(*m_Legs)[i]; }

	crIKSpine* GetSpine() { return &m_Spine; }
	crIKHead* GetHead() { return &m_Head; }
	crIKArm* GetArm(int i) { return &(*m_Arms)[i]; }
	crIKLeg* GetLeg(int i) { return &(*m_Legs)[i]; }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif

public:
	atRangeArray<crIKArm,IK_HUMANOID_LIMB_NUM>* m_Arms;
	atRangeArray<crIKLeg,IK_HUMANOID_LIMB_NUM>* m_Legs;

	crIKSpine m_Spine;
	crIKHead m_Head;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: A representation of a four-legged animal body.  One spine, one head and four legs.
class crIKBodyQuadruped : public crIKBodyBase
{
public:
	enum
	{
		IK_QUADRUPED_PART_SPINE,
		IK_QUADRUPED_PART_HEAD,
		IK_QUADRUPED_PART_LEG_FRONT_LEFT,
		IK_QUADRUPED_PART_LEG_FRONT_RIGHT,
		IK_QUADRUPED_PART_LEG_BACK_LEFT,
		IK_QUADRUPED_PART_LEG_BACK_RIGHT,
		IK_QUADRUPED_PART_NUM
	};

	enum
	{
		IK_QUADRUPED_LEG_FRONT_LEFT,
		IK_QUADRUPED_LEG_FRONT_RIGHT,
		IK_QUADRUPED_LEG_BACK_LEFT,
		IK_QUADRUPED_LEG_BACK_RIGHT,
		IK_QUADRUPED_LEG_NUM
	};

	crIKBodyQuadruped();
	crIKBodyQuadruped(datResource&);
	virtual ~crIKBodyQuadruped();

	IMPLEMENT_PLACE_INLINE(crIKBodyQuadruped);

	virtual bool InitByDefaultNames(const crSkeletonData& skelData, const crJointData& jointData);

	virtual int GetNumBodyParts() const { return IK_QUADRUPED_PART_NUM; }
	virtual const crIKPart* GetBodyPart(int index) const;

	const crIKSpine* GetSpine() const { return &m_Spine; }
	const crIKHead* GetHead() const { return &m_Head; }
	const crIKLeg* GetLeg(int i) const { return &(*m_Legs)[i]; }

	crIKSpine* GetSpine() { return &m_Spine; }
	crIKHead* GetHead() { return &m_Head; }
	crIKLeg* GetLeg(int i) { return &(*m_Legs)[i]; }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif

public:
	atRangeArray<crIKLeg,IK_QUADRUPED_LEG_NUM>* m_Legs;
	u32 pad0;
	crIKSpine m_Spine;
	crIKHead m_Head;
};

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage

#endif // CRBODY_IKBODY_H
