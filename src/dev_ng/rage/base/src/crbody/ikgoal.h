//
// crbody/ikgoal.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRBODY_IKGOAL_H
#define CRBODY_IKGOAL_H

#include "vectormath/quatv.h"
#include "vectormath/vec3v.h"

namespace rage
{


class crSkeleton;
class crIKPart;

// PURPOSE:
//	Goals represent the targets that the IK system is aiming for.
//	They contain the desired position and/or rotation, plus additional
//	parameters about the target.
//	Goals are also used to store the results of the most recent IK update.
class crIKGoal
{
public:
	// PURPOSE: Constructor
	crIKGoal();

	// PURPOSE: Resource constructor
	crIKGoal(datResource&) { }

	// PURPOSE: Update the goal (called by the kinematics system internally)
	void Update(crSkeleton& skeleton, const crIKPart& part);

    // PURPOSE: Returns primary IK goal position
	Vec3V_ConstRef GetPrimaryPosition() const  { return m_PrimaryPos; }

	// PURPOSE: Returns secondary IK goal position
	Vec3V_ConstRef GetSecondaryPosition() const  { return m_SecondaryPos; }

	// PURPOSE: Returns orientation of IK goal, used by some newer solvers
	QuatV_ConstRef GetOrientation() const  { return m_Orientation; }

	// PURPOSE: Sets primary IK goal position
	void SetPrimaryPosition(Vec3V_In v)  { m_PrimaryPos = v; m_Flags |= IKGOAL_USE_POSITION; }

	// PURPOSE: Sets secondary IK goal position
	void SetSecondaryPosition(Vec3V_In v)  { m_SecondaryPos = v; m_Flags |= IKGOAL_USE_POSITION; }

	// PURPOSE: Sets IK goal orientation
	void SetOrientation(QuatV_In q)  { m_Orientation = q; m_Flags |= IKGOAL_USE_ORIENTATION; }

	// PURPOSE: Sets IK goal twist (used by some older solvers)
	DEPRECATED void SetTwist(float f)  { m_Twist = f; }

	// PURPOSE: Gets IK goal blend [0..1] 0%->100%
	float GetBlend(void) const  { return m_Blend; }

	// PURPOSE: Gets IK goal balance [0..1] 0% == primary, 50% == primary & secondary, 100% == secondary
	float GetBalance(void) const  { return m_Balance; }

	// PURPOSE: Sets IK goal blend [0..1] 0%->100%
	void SetBlend(float f)  { m_Blend = f; }

	// PURPOSE: Sets IK goal balance [0..1] 0% == primary, 50% == primary & secondary, 100% == secondary
	void SetBalance(float f)  { m_Balance = f; }

	// PURPOSE: Returns last result of IK solver on this goal
	u8 GetResult(void) const  { return m_Result; }

	// PURPOSE: Sets last result of IK solver on this goal (for use in solvers only).
	void SetResult(u8 i)  { m_Result = i; }

	// PURPOSE: Sets a flag to use the orientation goal (defaults to off unless SetOriention() is called).
	void SetUseOrientation(bool b) { if (b) m_Flags |= IKGOAL_USE_ORIENTATION; else m_Flags &= ~IKGOAL_USE_ORIENTATION; }

	// PURPOSE: Sets a flag to use the position goal (defaults to off unless SetPosition() is called).
	void SetUsePosition(bool b) { if (b) m_Flags |= IKGOAL_USE_POSITION; else m_Flags &= ~IKGOAL_USE_POSITION; }

	// PURPOSE: Is orientation goal active
	bool GetUseOrientation() const { return (m_Flags & IKGOAL_USE_ORIENTATION) != 0; }

	// PURPOSE: Is position goal active
	bool GetUsePosition() const { return (m_Flags & IKGOAL_USE_POSITION) != 0; }

	// PURPOSE: Sets a flag to use fixed orientation goal (IK will not alter world space orientation of passed in skeleton primary goal)
	void SetFixedOrientation(bool b) { if (b) m_Flags |= IKGOAL_FIXED_ORIENTATION; else m_Flags &= ~IKGOAL_FIXED_ORIENTATION; }

	// PURPOSE: Sets a flag to use fixed position goal (IK will not alter world space position of passed in skeleton primary goal)
	void SetFixedPosition(bool b) { if (b) m_Flags |= IKGOAL_FIXED_POSITION; else m_Flags &= ~IKGOAL_FIXED_POSITION; }

	// PURPOSE: Is orientation goal fixed
	bool GetFixedOrientation() const { return (m_Flags & IKGOAL_FIXED_ORIENTATION) != 0; }

	// PUTPOSE: Is position goal fixed
	bool GetFixedPosition() const { return (m_Flags & IKGOAL_FIXED_POSITION) != 0; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

protected:
	Vec3V m_PrimaryPos;
	Vec3V m_SecondaryPos;
	QuatV m_Orientation;

	float m_Blend;
	float m_Twist;
	float m_Balance;  // 0.0 all primary, 1.0 all tip, 0.5 both primary and tip

	// Flags to control goal
	enum
	{
		IKGOAL_LIMP = 0x01,
		IKGOAL_USE_ORIENTATION = 0x02,
		IKGOAL_USE_POSITION = 0x04,

		IKGOAL_FIXED_ORIENTATION = 0x20,
		IKGOAL_FIXED_POSITION = 0x40,
	};
	u16 m_Flags;
	
	u8 m_Result;

	u8 pad0;
};

} // namespace rage

#endif // CRBODY_IKGOAL_H
