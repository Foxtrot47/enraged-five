//
// crbody/iksolver.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "iksolver.h"

#include "crbody/ikbody.h"
#include "crbody/ikpart.h"
#include "crskeleton/bonedata.h"
#include "crskeleton/skeleton.h"
#include "math/amath.h"
#include "vectormath/classes.h"

namespace rage
{

#if USE_RAGE_SOLVERS
const float s_blendErrorMargin = 0.0001f;
#endif // USE_RAGE_SOLVERS

////////////////////////////////////////////////////////////////////////////////

void ClampAngles(QuatV_InOut q, Vec3V_In min, Vec3V_In max)
{
	Vec3V e = QuatVToEulersXYZ(q);
	e = Clamp(e, min, max);
	q = QuatVFromEulersXYZ(e);
}

////////////////////////////////////////////////////////////////////////////////

crIKSolverBase::crIKSolverBase(crIKSolverType type)
: m_Type(type), m_Priority(0)
{}

////////////////////////////////////////////////////////////////////////////////

crIKGoalSolver::crIKGoalSolver(crIKSolverBase::crIKSolverType type, const crIKPart* bodyPart)
: crIKSolverBase(type)
, m_GoalAdjustment(QuatV(V_IDENTITY), Vec3V(V_ZERO))
, m_BodyPart(bodyPart)
, m_EnforceLimits(false)
{}

////////////////////////////////////////////////////////////////////////////////

void crIKGoalSolver::Iterate(float UNUSED_PARAM(dt), crSkeleton& skeleton)
{
	m_Goal.Update(skeleton, *m_BodyPart);
}

#if USE_RAGE_SOLVERS
////////////////////////////////////////////////////////////////////////////////

crIKSolverSimpleSpine::crIKSolverSimpleSpine(int bodyPart, crIKBodyBase& body, const crJointData& jointData)
: crIKGoalSolver(IK_SOLVER_TYPE_SIMPLE_SPINE, body.GetBodyPart(bodyPart))
{
	const crIKSpine& spine = static_cast<const crIKSpine&>(*m_BodyPart);
	Assert(spine.GetBodyPartType() == crIKPart::IK_PART_TYPE_SPINE && spine.GetNumSpineBones() > 0 && spine.GetNumSpineBones() <= MAX_BONES);

	m_NumBones = spine.GetNumSpineBones();
	for(int i=0; i<m_NumBones; ++i)
	{
		const crBoneData* bone = spine.GetSpineBone(i);
		m_Idxs[i] = bone->GetIndex();
		const crJointRotationLimit* jointLimit = jointData.FindJointRotationLimit(bone->GetBoneId());
		Assert(jointLimit);
		m_Limits[i] = *jointLimit;
	}
	m_PrimaryIdx = spine.GetPrimaryBone()->GetIndex();
	m_FwdAxis = spine.GetFwdAxis();
	m_UpAxis = spine.GetUpAxis();
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverSimpleSpine::Solve(SolverHelper& sh) const
{
	// Cache the head and neck matrices for blending

	TransformV prevs[MAX_BONES], bones[MAX_BONES];

	for(int i = 0; i < m_NumBones; i++)
	{
		TransformVFromMat34V(bones[i], sh.m_Locals[m_Idxs[i]]);
		prevs[i] = bones[i];
	}

	// Get the inverse of the world matrix for the top of the spine

	Mat34V spineGlobalMtx;
	Transform(spineGlobalMtx, sh.m_Parent, sh.m_Objects[m_PrimaryIdx]);

	// q is the combined adjustment, s is the in-plane adjustment, t is the orientation adjustment
	QuatV q(V_IDENTITY), t(V_IDENTITY);

	if (m_Goal.GetUseOrientation())
	{
		// Apply the goal adjustment
		QuatV goalOrient = Multiply(m_Goal.GetOrientation(), InvertNormInput(m_GoalAdjustment.GetRotation()));

		// Make sure the spine doesn't turn more than 180 degrees
		goalOrient = PrepareSlerp(QuatV(V_IDENTITY), goalOrient);

		// Find goal relative to the spine
		t = QuatVFromMat33V(spineGlobalMtx.GetMat33());
		t = InvertNormInput(t);
		t = Multiply(t, goalOrient);
	}

	if (m_Goal.GetUsePosition())
	{
		// Find goal relative to the head
		Vec3V localGoal = UnTransformOrtho(spineGlobalMtx, m_Goal.GetPrimaryPosition());

		// Apply the goal adjustment
		localGoal = UnTransform(m_GoalAdjustment, localGoal);

		Vec3V fwdAxis(m_FwdAxis), upAxis(m_UpAxis);
		if (m_Goal.GetUseOrientation())
		{
			fwdAxis = Transform(t, fwdAxis);
			upAxis = Transform(t, upAxis);
		}

		// First split the adjustment into two parts, the rotation in the plane
		// normal to the up axis, and the rotation out of the plane.
		// This is necessary to make sure looking backwards rotates the spine
		// around the axis of the spine, not by flipping the spine upside down.
		Vec3V localGoalInPlane = SubtractScaled( localGoal, upAxis, Dot( upAxis, localGoal ) );
		q = QuatVFromVectors(fwdAxis, localGoalInPlane, upAxis);

		if (m_Goal.GetUseOrientation())
			q = Multiply(q, t);
	}
	else if (m_Goal.GetUseOrientation())
	{
		q = t;
	}

	// Apply the adjustment to each bone in the neck
	ScalarV invNumSpineBone = LoadScalar32IntoScalarV(1.0f / m_NumBones);
	q = QuatVScaleAngle( q, invNumSpineBone );
	QuatV adjust = Normalize(q);

	for (int i = 0; i < m_NumBones; i++)
	{
		bones[i].SetRotation(Transform(bones[i], adjust));

		if (m_EnforceLimits)
		{
			m_Limits[i].ApplyLimits(bones[i].GetRotationRef());
		}
	}

	// Apply blending
	if(m_Goal.GetBlend() < (1.f - s_blendErrorMargin))
	{
		ScalarV blend = LoadScalar32IntoScalarV(m_Goal.GetBlend());
		for (int i = 0; i < m_NumBones; i++)
		{
			TransformV lerped = Lerp(blend, prevs[i], bones[i]);
			Mat34VFromTransformV(sh.m_Locals[m_Idxs[i]], lerped);
		}
	}

	// Recompute global matrices

	sh.UpdateObjectMtxs();
}

////////////////////////////////////////////////////////////////////////////////

crIKSolverSimpleHead::crIKSolverSimpleHead(int bodyPart, crIKBodyBase& body, const crJointData& jointData)
: crIKGoalSolver(IK_SOLVER_TYPE_SIMPLE_HEAD, body.GetBodyPart(bodyPart))
{
	const crIKHead& head = static_cast<const crIKHead&>(*m_BodyPart);
	Assert(head.GetBodyPartType() == crIKPart::IK_PART_TYPE_HEAD && head.GetNumNeckBones() > 0 && head.GetNumNeckBones() < MAX_BONES);

	m_FwdAxis = head.GetFwdAxis();
	m_UpAxis = head.GetUpAxis();
	m_NumBones = head.GetNumNeckBones();
	m_PrimaryIdx = head.GetPrimaryBone()->GetIndex();
	m_HeadIdx = head.GetHeadBone()->GetIndex();
	m_HeadLimit = *jointData.FindJointRotationLimit(head.GetHeadBone()->GetBoneId());

	for(int i=0; i<m_NumBones+1; ++i)
	{
		const crBoneData* bone = head.GetBone(i + crIKHead::IK_HEAD_BONE_NECK);
		m_Idxs[i] = bone->GetIndex();
		m_Limits[i] = *jointData.FindJointRotationLimit(bone->GetBoneId());
	}
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverSimpleHead::Solve(SolverHelper& sh) const
{
	// Cache the head and neck matrices for blending

	TransformV prevs[MAX_BONES], bones[MAX_BONES];

	for (int i = 0; i < m_NumBones + 1; i++)
	{
		TransformVFromMat34V(bones[i], sh.m_Locals[m_Idxs[i]]);
		prevs[i] = bones[i];
	}

	// Get the inverse of the world matrix for the head

	Mat34V headGlobalMtx;
	Transform(headGlobalMtx, sh.m_Parent, sh.m_Objects[m_PrimaryIdx]);


	// Calculate the adjustment needed to reach the goal

	// q is the combined adjustment, s is the in-plane adjustment, t is the orientation adjustment
	QuatV q(V_IDENTITY), s(V_IDENTITY), t(V_IDENTITY);

	if (m_Goal.GetUseOrientation())
	{
		// Apply the goal adjustment
		QuatV goalOrient = Multiply(m_Goal.GetOrientation(), InvertNormInput(m_GoalAdjustment.GetRotation()));

		// Make sure the head doesn't turn more than 180 degrees
		goalOrient = PrepareSlerp(QuatV(V_IDENTITY), goalOrient);

		// Find goal relative to the head
		t = QuatVFromMat33V(headGlobalMtx.GetMat33());
		t = InvertNormInput(t);
		t = Multiply(t, goalOrient);
	}


	if (m_Goal.GetUsePosition())
	{
		// Find goal relative to the head
		Vec3V localGoal = UnTransformOrtho(headGlobalMtx, m_Goal.GetPrimaryPosition());

		// Apply the goal adjustment
		localGoal = UnTransform(m_GoalAdjustment, localGoal);

		Vec3V fwdAxis(m_FwdAxis), upAxis(m_UpAxis);
		if (m_Goal.GetUseOrientation())
		{
			fwdAxis = Transform(t, fwdAxis);
			upAxis = Transform(t, upAxis);
		}

		// First split the adjustment into two parts, the rotation in the plane
		// normal to the up axis, and the rotation out of the plane.
		// This is necessary to make sure looking backwards rotates the spine
		// around the axis of the spine, not by flipping the spine upside down.
		Vec3V localGoalInPlane = SubtractScaled( localGoal, upAxis, Dot( upAxis, localGoal ) );
		q = QuatVFromVectors(fwdAxis, localGoalInPlane, upAxis);

		s = QuatVFromVectors(localGoalInPlane, localGoal);
		q = Multiply(s, q);

		if (m_Goal.GetUseOrientation())
			q = Multiply(q, t);
	}
	else if (m_Goal.GetUseOrientation())
	{
		q = t;
	}


	// Apply the adjustment to each bone in the neck

	ScalarV invNumSpineBone = LoadScalar32IntoScalarV(1.0f / (m_NumBones+1));
	q = QuatVScaleAngle( q, invNumSpineBone );
	QuatV adjust = Normalize(q);

	for (int i = 0; i < m_NumBones; i++)
	{
		bones[i].SetRotation(Transform(bones[i], adjust));

		if (m_EnforceLimits)
		{
			m_Limits[i].ApplyLimits(bones[i].GetRotationRef());
		}
		Mat34VFromTransformV(sh.m_Locals[m_Idxs[i]], bones[i]);
	}


	// Recompute global matrices

	sh.UpdateObjectMtxs();


	// Repeat the adjustment calculation for the head

	Transform(headGlobalMtx, sh.m_Parent, sh.m_Objects[m_PrimaryIdx]);

	if (m_Goal.GetUseOrientation())
	{
		// Apply the goal adjustment
		QuatV goalOrient = Multiply(m_Goal.GetOrientation(), InvertNormInput(m_GoalAdjustment.GetRotation()));

		// Make sure the spine doesn't turn more than 180 degrees
		goalOrient = PrepareSlerp(QuatV(V_IDENTITY), goalOrient);

		// Find goal relative to the spine
		t = QuatVFromMat33V(headGlobalMtx.GetMat33());
		t = InvertNormInput(t);
		t = Multiply(t, goalOrient);
	}


	if (m_Goal.GetUsePosition())
	{
		// Find goal relative to the head
		Vec3V localGoal = UnTransformOrtho(headGlobalMtx, m_Goal.GetPrimaryPosition());

		// Apply the goal adjustment
		localGoal = UnTransform(m_GoalAdjustment, localGoal);

		Vec3V fwdAxis(m_FwdAxis), upAxis(m_UpAxis);
		if (m_Goal.GetUseOrientation())
		{
			fwdAxis = Transform(t, fwdAxis);
			upAxis = Transform(t, upAxis);
		}

		// First split the adjustment into two parts, the rotation in the plane
		// normal to the up axis, and the rotation out of the plane.
		// This is necessary to make sure looking backwards rotates the spine
		// around the axis of the spine, not by flipping the spine upside down.
		Vec3V localGoalInPlane = SubtractScaled( localGoal, upAxis, Dot( upAxis, localGoal ) );
		q = QuatVFromVectors(fwdAxis, localGoalInPlane, upAxis);

		s = QuatVFromVectors(localGoalInPlane, localGoal);
		q = Multiply(s, q);

		if (m_Goal.GetUseOrientation())
			q = Multiply(q, t);
	}
	else if (m_Goal.GetUseOrientation())
	{
		q = t;
	}

	adjust = q;


	// Apply the correction to the head
	Mat34V& headLocalMtx = sh.m_Locals[m_HeadIdx];
	TransformV head;
	TransformVFromMat34V(head, headLocalMtx);
	head.SetRotation(Multiply(head.GetRotation(), adjust));

	if (m_EnforceLimits)
	{
		m_HeadLimit.ApplyLimits(head.GetRotationRef());
	}
	Mat34VFromTransformV(headLocalMtx, head);

	// Apply blending
	if(m_Goal.GetBlend() < (1.f - s_blendErrorMargin))
	{
		ScalarV blend = LoadScalar32IntoScalarV(m_Goal.GetBlend());
		for (int i = 0; i < m_NumBones+1; i++)
		{
			TransformV lerped = Lerp(blend, prevs[i], bones[i]);
			Mat34VFromTransformV(sh.m_Locals[m_Idxs[i]], lerped);
		}
	}


	// Recompute global matrices (again)
	sh.UpdateObjectMtxs();
}

////////////////////////////////////////////////////////////////////////////////

crIKSolverLimbBase::crIKSolverLimbBase(crIKSolverBase::crIKSolverType type, const crIKPart* bodyPart, const crJointData& jointData)
: crIKGoalSolver(type, bodyPart)
{ 	
	const crIKLimb& limb = static_cast<const crIKLimb&>(*m_BodyPart);
	Assert(limb.GetBodyPartType() == crIKPart::IK_PART_TYPE_ARM || limb.GetBodyPartType() == crIKPart::IK_PART_TYPE_LEG);

	const crBoneData* bones[NUM_BONES];
	bones[JOINT] = limb.GetJointBone();
	bones[UPPER] = limb.GetUpperLimbBone();
	bones[UPPER_MID] = limb.GetUpperLimbMidBone();
	bones[LOWER] = limb.GetLowerLimbBone();
	bones[LOWER_MID] = limb.GetLowerLimbMidBone();
	bones[APPENDAGE] = limb.GetAppendageBone();
	bones[APPENDAGE_TIP] = limb.GetAppendageTipBone();

	for(int i=0; i<NUM_BONES; ++i)
	{
		m_Idxs[i] = bones[i] ? u8(bones[i]->GetIndex()) : -1;
	}

	const crJointRotationLimit* jointLimit;

	jointLimit = jointData.FindJointRotationLimit(bones[UPPER]->GetBoneId());
	Assert(jointLimit);
	m_Limits[0] = *jointLimit;

	jointLimit = jointData.FindJointRotationLimit(bones[LOWER]->GetBoneId());
	Assert(jointLimit);
	m_Limits[1] = *jointLimit;

	jointLimit = jointData.FindJointRotationLimit(bones[APPENDAGE]->GetBoneId());
	Assert(jointLimit);
	m_Limits[2] = *jointLimit;

	m_Lengths[0] = Mag(bones[LOWER]->GetDefaultTranslation()).Getf();
	if(m_Idxs[UPPER_MID]!=-1)
	{
		m_Lengths[0] += Mag(bones[UPPER_MID]->GetDefaultTranslation()).Getf();
	}
	m_Lengths[1] = Mag(bones[APPENDAGE]->GetDefaultTranslation()).Getf();
	if(m_Idxs[LOWER_MID]!=-1)
	{
		m_Lengths[1] += Mag(bones[LOWER_MID]->GetDefaultTranslation()).Getf();
	}

	m_RollAxis = limb.GetRollAxis();
	m_BendAxis = limb.GetBendAxis();

	m_TransferRollToUpperMid = limb.GetTransferRollToUpperMid();
	m_TransferRollToLowerMid = limb.GetTransferRollToLowerMid();
	m_TransferRollFromAppendage = limb.GetTransferRollFromAppendage();
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverLimbBase::Solve(SolverHelper& sh) const
{
	crIKGoal goal(m_Goal);

	TransformV prevs[NUM_BONES], bones[NUM_BONES];

	// Retrieve the local matrices for all the bones
	for(int i=0; i<NUM_BONES; ++i)
	{
		if(m_Idxs[i]!=-1)
		{
			TransformVFromMat34V(bones[i], sh.m_Locals[m_Idxs[i]]);
			prevs[i] = bones[i];
		}
	}

	// Apply the goal position offset if specified
	if (IsZeroAll(m_GoalAdjustment.GetPosition())==0)
	{
		Mat34V appendageWorldMtx;
		Transform(appendageWorldMtx, sh.m_Parent, sh.m_Objects[m_Idxs[APPENDAGE]]);

		Vec3V offset = Transform(appendageWorldMtx, m_GoalAdjustment.GetPosition());

		goal.SetPrimaryPosition(goal.GetPrimaryPosition() - offset);
		goal.SetSecondaryPosition(goal.GetSecondaryPosition() - offset);
	}

	// Apply the goal orientation adjustment if specified
	if (goal.GetUseOrientation() && !IsEqualAll(m_GoalAdjustment.GetRotation(), QuatV(V_IDENTITY)))
	{
		QuatV newOrient = Multiply(goal.GetOrientation(), InvertNormInput(m_GoalAdjustment.GetRotation()));
		goal.SetOrientation(newOrient);
	}

	// Adjust the goals to take into consideration goal balance
	if(goal.GetBalance() > s_blendErrorMargin && m_Idxs[APPENDAGE_TIP]!=-1)
	{
		QuatV goalOrientation;
		Vec3V currentPrimaryToSecondary, goalPrimaryToSecondary = goal.GetSecondaryPosition() - goal.GetPrimaryPosition();

		// Convert existing rotation to quaternion
		Mat34V mAppendage;
		Transform(mAppendage, sh.m_Parent, sh.m_Objects[m_Idxs[APPENDAGE]]);
		QuatV currentOrientation = QuatVFromMat33V(mAppendage.GetMat33());

		// Find an orientation that aligns the foot with the primary/secondary goals
		if (goal.GetUseOrientation())
		{
			// Use the roll specified in goal.GetOrientation()
			currentPrimaryToSecondary = Transform(goal.GetOrientation(), bones[APPENDAGE_TIP].GetPosition());
			goalOrientation = QuatVFromVectors(currentPrimaryToSecondary, goalPrimaryToSecondary);
			goalOrientation = Multiply(goalOrientation, goal.GetOrientation());
		}
		else
		{
			// Use the existing roll
			currentPrimaryToSecondary = Transform(currentOrientation, bones[APPENDAGE_TIP].GetPosition());
			goalOrientation = QuatVFromVectors(currentPrimaryToSecondary, goalPrimaryToSecondary);
			goalOrientation = Multiply(goalOrientation, currentOrientation);
		}

		// Interpolate between existing and desired rotations (100% when balance is at 0.5, falling off both sides)
		goalOrientation = SlerpNear(LoadScalar32IntoScalarV(1.f - 2.f * fabsf(goal.GetBalance() - 0.5f)), currentOrientation, goalOrientation);
		goal.SetOrientation(goalOrientation);

		// Correct the primary goal, based on the difference between the secondary goal and the actual tip position
		// given the target orientation we've just calculated
		Vec3V vTip = Transform(goalOrientation, bones[APPENDAGE_TIP].GetPosition());
		vTip += goal.GetPrimaryPosition();

		// This correction is blended in, applying 100% after balance is 0.5, blending in before that
		goal.SetPrimaryPosition(goal.GetPrimaryPosition() + (goal.GetSecondaryPosition() - vTip) * LoadScalar32IntoScalarV(Min(1.f, goal.GetBalance() * 2.f)));
	}
	else if (m_Idxs[APPENDAGE_TIP]!=-1)
	{
		goal.SetUseOrientation(false);
	}

	// Place goal into local space of joint bone
	Mat34V jointWorldMtx;
	Transform(jointWorldMtx, sh.m_Parent, sh.m_Objects[m_Idxs[JOINT]]);
	QuatV neutralLS = QuatVFromMat33V(jointWorldMtx.GetMat33());
	neutralLS = InvertNormInput(neutralLS);

	Mat34V upperBoneMtx;
	Transform(upperBoneMtx, sh.m_Parent, sh.m_Objects[m_Idxs[UPPER]]);

	Vec3V newTargetPos = goal.GetPrimaryPosition() - upperBoneMtx.GetCol3();
	newTargetPos = Transform(neutralLS, newTargetPos);
	goal.SetPrimaryPosition(newTargetPos);

	if (goal.GetUseOrientation())
	{
		QuatV newTargetRot = neutralLS;
		newTargetRot = Multiply(newTargetRot, goal.GetOrientation());
		goal.SetOrientation(newTargetRot);
	}

	// Calculate solution
	SolveLimb(goal, bones);

	// Blending
	ScalarV blend = LoadScalar32IntoScalarV(m_Goal.GetBlend());
	for(int i=0; i<NUM_BONES; ++i)
	{
		if(m_Idxs[i]!=-1)
		{
			TransformV lerped = Lerp(blend, prevs[i], bones[i]);
			Mat34VFromTransformV(sh.m_Locals[m_Idxs[i]], lerped);
		}
	}

	// rebuild global matrices
	sh.UpdateObjectMtxs();
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverLimbBase::SolveLimb(const crIKGoal& goal, TransformV* bones) const
{
	// Simplify the problem by consolidating the twist on any mid bones onto their parents

	if(m_Idxs[UPPER_MID]!=-1)
	{
		ScalarV roll = QuatVTwistAngle(bones[UPPER_MID].GetRotation(), m_RollAxis);
		bones[UPPER].SetRotation(Multiply(bones[UPPER].GetRotation(), QuatVFromAxisAngle(m_RollAxis, roll)));
		bones[UPPER_MID].SetRotation(Multiply(bones[UPPER_MID].GetRotation(), QuatVFromAxisAngle(m_RollAxis, -roll)));
	}

	if(m_Idxs[LOWER_MID]!=-1)
	{
		ScalarV roll = QuatVTwistAngle(bones[LOWER_MID].GetRotation(), m_RollAxis);
		bones[LOWER].SetRotation(Multiply(bones[LOWER].GetRotation(), QuatVFromAxisAngle(m_RollAxis, roll)));
		bones[LOWER_MID].SetRotation(Multiply(bones[LOWER_MID].GetRotation(), QuatVFromAxisAngle(m_RollAxis, -roll)));
	}


	// Gather the information needed to solve

	QuatV q[3];
	Vec3V v[2];

	q[0] = bones[UPPER].GetRotation();
	q[1] = bones[LOWER].GetRotation();
	q[2] = bones[APPENDAGE].GetRotation();

	v[0] = bones[LOWER].GetPosition();

	if(m_Idxs[UPPER_MID]!=-1)
		v[0] += bones[UPPER_MID].GetPosition();

	v[1] = bones[APPENDAGE].GetPosition();

	if(m_Idxs[LOWER_MID]!=-1)
		v[1] += bones[LOWER_MID].GetPosition();

	ScalarV initialAppendageRoll = QuatVTwistAngle(q[2], m_RollAxis);

	// Call the solver
	if(m_Type == IK_SOLVER_TYPE_GLEICHER_LIMB)
	{
		static_cast<const crIKSolverGleicherLimb*>(this)->SolveGleicher(goal, q, v, m_Limits, m_Lengths, m_BendAxis);
	}
	else if(m_Type == IK_SOLVER_TYPE_ITERATIVE_LIMB)
	{
		static_cast<const crIKSolverIterativeLimb*>(this)->SolveIterative(goal, q, v, m_Limits, m_Lengths, m_BendAxis);
	}

	// Apply the results back to the matrices
	bones[UPPER].SetRotation(q[0]);
	bones[LOWER].SetRotation(q[1]);
	bones[APPENDAGE].SetRotation(q[2]);


	// Redistribute roll and bone stretching

	if(m_Idxs[UPPER_MID]!=-1)
	{
		ScalarV roll = QuatVTwistAngle(q[0], m_RollAxis) * LoadScalar32IntoScalarV(m_TransferRollToUpperMid);
		bones[UPPER_MID].SetRotation(Multiply(bones[UPPER_MID].GetRotation(), QuatVFromAxisAngle(m_RollAxis, roll)));
		bones[UPPER].SetRotation(Multiply(bones[UPPER].GetRotation(), QuatVFromAxisAngle(m_RollAxis, -roll)));

		ScalarV len = Mag(bones[LOWER].GetPosition());
		ScalarV midlen = Mag(bones[UPPER_MID].GetPosition());

		bones[LOWER].SetPosition(v[0] * len / (len + midlen));
		bones[UPPER_MID].SetPosition(v[0] * midlen / (len + midlen));
	}
	else
	{
		bones[LOWER].SetPosition(v[0]);
	}

	if(m_Idxs[LOWER_MID]!=-1)
	{
		ScalarV roll = QuatVTwistAngle(q[1], m_RollAxis) * LoadScalar32IntoScalarV(m_TransferRollToLowerMid);
		ScalarV appendageRoll = QuatVTwistAngle(q[2], m_RollAxis) - initialAppendageRoll * LoadScalar32IntoScalarV(m_TransferRollFromAppendage);

		bones[LOWER_MID].SetRotation(Multiply(bones[LOWER_MID].GetRotation(), QuatVFromAxisAngle(m_RollAxis, roll)));
		bones[LOWER].SetRotation(Multiply(bones[LOWER].GetRotation(), QuatVFromAxisAngle(m_RollAxis, -roll)));
		bones[APPENDAGE].SetRotation(Multiply(bones[APPENDAGE].GetRotation(), QuatVFromAxisAngle(m_RollAxis, -appendageRoll)));

		ScalarV len = Mag(bones[APPENDAGE].GetPosition());
		ScalarV midlen = Mag(bones[LOWER_MID].GetPosition());

		bones[APPENDAGE].SetPosition(v[1] * len / (len + midlen));
		bones[LOWER_MID].SetPosition(v[1] * midlen / (len + midlen));
	}
	else
	{
		bones[APPENDAGE].SetPosition(v[1]);
	}
}

////////////////////////////////////////////////////////////////////////////////

crIKSolverGleicherLimb::crIKSolverGleicherLimb(int bodyPart, crIKBodyBase& body, const crJointData& jointData)
: crIKSolverLimbBase(IK_SOLVER_TYPE_GLEICHER_LIMB, body.GetBodyPart(bodyPart), jointData)
, m_Alpha(DtoR * 150.0f)
, m_OverReach(1.1f)
{
}

////////////////////////////////////////////////////////////////////////////////

// inoutRots[0] is local upper bone (hip/shoulder) orientation
// inoutRots[1] is local lower bone (knee/elbow) orientation
// inoutRots[2] is local appendage bone (ankle/wrist) orientation

// inoutOffsets[0] is lower bone (knee/elbow) local position
// inoutOffsets[1] is appendage bone (ankle/wrist) local position

void crIKSolverGleicherLimb::SolveGleicher(const crIKGoal& goal, QuatV inoutRots[3], Vec3V inoutOffsets[2],
										   const crJointRotationLimit limits[3], const float lengths[2], const Vec3V& bendAxis) const
{
	// Calculate bone offsets
	Vec3V hipToGoal = Transform(InvertNormInput(inoutRots[0]), goal.GetPrimaryPosition());

	Vec3V hipToKnee = inoutOffsets[0];
	Vec3V kneeToAnkle = Transform(inoutRots[1], inoutOffsets[1]);

	// Calculate the length of the bones
	float upperLen2 = MagSquared(hipToKnee).Getf(), lowerLen2 = MagSquared(kneeToAnkle).Getf(), goalLen2 = MagSquared(hipToGoal).Getf();
	float upperLen = sqrt(upperLen2), lowerLen = sqrt(lowerLen2), goalLen = sqrt(goalLen2);

	// Calculate joint angle to reach goal
	float kneeAngle;
	if (goalLen >= upperLen + lowerLen)
	{
		kneeAngle = PI;
	}
	else if (goalLen <= upperLen - lowerLen)
	{
		kneeAngle = 0.25f * PI;
	}
	else
	{
		kneeAngle = acosf((upperLen2 + lowerLen2 - goalLen2) / (2 * upperLen * lowerLen));
		kneeAngle = Clamp(kneeAngle, 0.25f * PI, PI);
	}

	// Apply Gleicher angle limiting
	float oldKneeAngle = PI - Angle(hipToKnee, kneeToAnkle).Getf();

	if (kneeAngle > m_Alpha)
	{
		kneeAngle = oldKneeAngle + LimitAngle(m_Alpha, oldKneeAngle, kneeAngle - oldKneeAngle);
	}

	// Apply corrections to quaternions
	ScalarV angle = LoadScalar32IntoScalarV(kneeAngle - oldKneeAngle);
	QuatV qc = QuatVFromAxisAngle(bendAxis, angle);
	inoutRots[1] = Multiply(qc, inoutRots[1]);
	if (m_EnforceLimits)
	{
		limits[1].ApplyLimits(inoutRots[1]);
	}

	kneeToAnkle = Transform(inoutRots[1], inoutOffsets[1]);
	Vec3V hipToAnkle = hipToKnee + kneeToAnkle;

	qc = QuatVFromVectors(hipToAnkle, hipToGoal);
	inoutRots[0] = Multiply(inoutRots[0], qc);
	if (m_EnforceLimits)
	{
		limits[0].ApplyLimits(inoutRots[0]);
	}

	// Scale bones to reach goal
	ScalarV reach = LoadScalar32IntoScalarV((m_OverReach - 1.f) * goal.GetBlend() + 1.f);
	ScalarV invReach = Invert(reach);
	ScalarV scale = Clamp(Sqrt(MagSquared(hipToGoal) / MagSquared(hipToAnkle)), invReach, reach);
	if(m_EnforceLimits)
	{
		ScalarV mag0 = Mag(inoutOffsets[0]);
		ScalarV mag1 = Mag(inoutOffsets[1]);
		ScalarV length0 = LoadScalar32IntoScalarV(lengths[0]);
		ScalarV length1 = LoadScalar32IntoScalarV(lengths[1]);
		inoutOffsets[0] *= Clamp(mag0*scale, length0*invReach, length0*reach)/mag0;
		inoutOffsets[1] *= Clamp(mag1*scale, length1*invReach, length1*reach)/mag1;
	}
	else
	{
		inoutOffsets[0] *= scale;
		inoutOffsets[1] *= scale;
	}

	// Crude fix up of ankle orientation
	if (goal.GetUseOrientation())
	{
		inoutRots[2] = Multiply(inoutRots[0], inoutRots[1]);
		inoutRots[2] = InvertNormInput(inoutRots[2]);
		inoutRots[2] = Multiply(inoutRots[2], goal.GetOrientation());

		if (m_EnforceLimits)
		{
			limits[2].ApplyLimits(inoutRots[2]);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

float crIKSolverGleicherLimb::LimitAngle(float alpha, float thetaStart, float dtheta)
{
	// Voodoo magic from the Gleicher sample code

	float newdtheta = 0.f;
	bool forward = true;

	float thetaEnd = thetaStart + dtheta;

	// make sure start is < end - gives same answer but is easier to deal with
	if (dtheta < 0.f)
	{
		forward = false;
		SwapEm(thetaStart, thetaEnd);
	}

	if (thetaEnd > alpha)
	{
		float alphaStart = thetaStart;
		if(alphaStart < alpha)
		{
			alphaStart = alpha;
		}

		newdtheta += 0.5f * sinf(((thetaEnd - alpha) * PI) / (PI-alpha)) * (PI-alpha) / PI + thetaEnd * 0.5f;
		newdtheta -= 0.5f * sinf(((alphaStart-alpha) * PI) / (PI-alpha)) * (PI-alpha) / PI + alphaStart * 0.5f;
	}

	if (thetaStart < alpha)
	{
		float alphaEnd = thetaEnd;
		if(alphaEnd > alpha)
		{
			alphaEnd = alpha;
		}

		newdtheta += alphaEnd - thetaStart;
	}

	return forward ? newdtheta : -newdtheta;
}

////////////////////////////////////////////////////////////////////////////////

crIKSolverIterativeLimb::crIKSolverIterativeLimb(int bodyPart, crIKBodyBase& body, const crJointData& jointData)
: crIKSolverLimbBase(IK_SOLVER_TYPE_ITERATIVE_LIMB, body.GetBodyPart(bodyPart), jointData)
, m_MaxIterations(20)
, m_Tolerance(0.01f)
, m_MaxAngularDelta(15.f * DtoR)
{
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverIterativeLimb::SolveIterative(const crIKGoal& goal, QuatV inoutRots[3], Vec3V inoutOffsets[2],
											 const crJointRotationLimit limits[3], const float [2], const Vec3V& bendAxis) const
{
	const Vec3V& hipToKnee = inoutOffsets[0];

	float lastDelta = LARGE_FLOAT;

	ScalarV maxAngularDelta = LoadScalar32IntoScalarV(m_MaxAngularDelta);

	for (int iter = 0; iter < m_MaxIterations; iter++)
	{
		{
			Vec3V hipToGoal = UnTransform(inoutRots[0], goal.GetPrimaryPosition());

			Vec3V kneeToAnkle = Transform(inoutRots[1], inoutOffsets[1]);

			Vec3V kneeToGoal = hipToGoal - hipToKnee;

			QuatV qc = QuatVFromVectors(kneeToAnkle, kneeToGoal);

			ScalarV delta = GetAngle(qc);
			if (IsGreaterThanAll(delta, maxAngularDelta))
			{
				qc = QuatVScaleAngle(qc, maxAngularDelta / delta);
			}

			inoutRots[1] = Multiply(qc, inoutRots[1]);

			if (m_EnforceLimits)
			{
				limits[1].ApplyLimits(inoutRots[1]);
			}
		}

		{
			Vec3V hipToGoal = UnTransform(inoutRots[0], goal.GetPrimaryPosition());

			Vec3V kneeToAnkle = Transform(inoutRots[1], inoutOffsets[1]);

			Vec3V hipToAnkle = hipToKnee + kneeToAnkle;

			QuatV qc = QuatVFromVectors(hipToAnkle, hipToGoal);

			ScalarV delta = GetAngle(qc);
			if (IsGreaterThanAll(delta, maxAngularDelta))
			{
				qc = QuatVScaleAngle(qc, maxAngularDelta / delta);
			}
			inoutRots[0] = Multiply(inoutRots[0], qc);

			if (m_EnforceLimits)
			{
				limits[0].ApplyLimits(inoutRots[0]);
			}
		}

		{
			Vec3V hipToGoal = UnTransform(inoutRots[0], goal.GetPrimaryPosition());

			Vec3V kneeToAnkle = Transform(inoutRots[1], inoutOffsets[1]);

			Vec3V hipToAnkle = hipToKnee + kneeToAnkle;
			Vec3V kneeToGoal = hipToGoal - hipToKnee;

			Vec3V desiredNormal, currentNormal;
			desiredNormal = Cross(kneeToGoal, hipToAnkle);

			if (MagSquared(desiredNormal).Getf() < m_Tolerance * m_Tolerance)
				continue;

			currentNormal = Transform(inoutRots[1], bendAxis);

			QuatV qc = QuatVFromVectors(currentNormal, desiredNormal);

			ScalarV delta = GetAngle(qc);
			if (IsGreaterThanAll(delta, maxAngularDelta))
			{
				qc = QuatVScaleAngle(qc, maxAngularDelta / delta);
			}

			float deltaBefore = DistSquared(kneeToGoal, kneeToAnkle).Getf();

			QuatV rotBefore = inoutRots[0];

			inoutRots[0] = Multiply(inoutRots[0], qc);

			if (m_EnforceLimits)
			{
				limits[0].ApplyLimits(inoutRots[0]);
			}

			hipToGoal = UnTransform(inoutRots[0], goal.GetPrimaryPosition());
			kneeToGoal = hipToGoal - hipToKnee;

			float newDelta = DistSquared(kneeToGoal, kneeToAnkle).Getf();

			if (newDelta > deltaBefore - m_Tolerance * m_Tolerance)
				inoutRots[0] = rotBefore, newDelta = deltaBefore;

			if (fabsf(newDelta - lastDelta) < m_Tolerance * m_Tolerance)
				break;

			lastDelta = newDelta;
		}
	}

	// Crude fix up of appendage orientation
	if (goal.GetUseOrientation())
	{
		inoutRots[2] = Multiply(inoutRots[0], inoutRots[1]);
		inoutRots[2] = InvertNormInput(inoutRots[2]);
		inoutRots[2] = Multiply(inoutRots[2], goal.GetOrientation());

		if (m_EnforceLimits)
		{
			limits[2].ApplyLimits(inoutRots[2]);
		}
	}
}
#endif // USE_RAGE_SOLVERS

////////////////////////////////////////////////////////////////////////////////

crIKSolverArms::crIKSolverArms()
: crIKSolverBase(IK_SOLVER_TYPE_ARMS)
{
	memset(&m_Arms, 0, NUM_ARMS*sizeof(ArmGoal));
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverArms::Solve(SolverHelper& sh) const
{
	for(int i=0; i < NUM_ARMS; ++i)
	{
		const ArmGoal& arm = m_Arms[i];
		if(arm.m_Enabled)
		{
			arm.Solve(sh);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Find the circle with position p1 and radius h in the plane where the two spheres intersect
// NOTES: http://astronomy.swin.edu.au/~pbourke/geometry/2circle/
__forceinline void CalculateP2AndH(Vec3V_In p0, Vec3V_In p1, ScalarV_In r0, ScalarV_In r1, Vec3V_InOut p2, ScalarV_InOut h)
{
	// Determine d the distance between the centre of the circles
	ScalarV d = Mag(p1 - p0);

	// Determine a the distance from p0 to p2
	ScalarV _two(V_TWO);
	ScalarV a = ((r0 * r0) - (r1 * r1) + (d * d)) / (_two * d);

	// Determine p2 the coordinates of p2
	// So p2 = p0 + a (p1 - p0) / d
	p2 = p0 + a * (p1 - p0) / d;

	// Determine h the radius of the circle
	ScalarV _zero(V_ZERO);
	ScalarV hSquared = (r0 * r0) - (a * a);
	h = SelectFT(IsLessThan(hSquared, _zero), Sqrt(hSquared), _zero);
}

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Orientate (the x-axis of) matrix A toward the position of matrix b
__forceinline void OrientateBoneATowardBoneB(Mat34V_InOut a, Mat34V_In b)
{
	Vec3V dir = NormalizeFast(b.GetCol3()-a.GetCol3());
	a.SetCol0(dir);
	a.SetCol1(Cross(a.GetCol2(), a.GetCol0()));
	a.SetCol2(Cross(a.GetCol0(), a.GetCol1()));
	ReOrthonormalize3x3(a, a);
}

//////////////////////////////////////////////////////////////////////

// PURPOSE: Orientate (the x-axis of) matrix A toward the position of matrix b using direction r as reference
__forceinline void OrientateBoneATowardBoneB(Mat34V_InOut a, Mat34V_InOut b, Vec3V_In r)
{
	Vec3V dir = NormalizeFast(b.GetCol3()-a.GetCol3());
	a.SetCol0(dir);
	if (IsLessThanAll(Dot(a.GetCol2(), r), Dot(a.GetCol1(), r)))
	{
		a.SetCol1(Cross(a.GetCol2(), a.GetCol0()));
		a.SetCol2(Cross(a.GetCol0(), a.GetCol1()));
	}
	else
	{
		a.SetCol2(Cross(a.GetCol0(), a.GetCol1()));
		a.SetCol1(Cross(a.GetCol2(), a.GetCol0()));
	}
	ReOrthonormalize3x3(a, a);
}

//////////////////////////////////////////////////////////////////////

/*
void PrintMtx(Mat34V& mtx)
{
	Printf( "[%f, %f, %f, %f]\n", mtx.GetM00f(), mtx.GetM01f(), mtx.GetM02f(), mtx.GetM03f() );
	Printf( "[%f, %f, %f, %f]\n", mtx.GetM10f(), mtx.GetM11f(), mtx.GetM12f(), mtx.GetM13f() );
	Printf( "[%f, %f, %f, %f]\n", mtx.GetM20f(), mtx.GetM21f(), mtx.GetM22f(), mtx.GetM23f() );
}
*/

//////////////////////////////////////////////////////////////////////

void crIKSolverArms::ArmGoal::Solve(SolverHelper& sh) const
{
	// Work out p2 and h (and hVector the direction of h) for the original animated pose
	// P0 = hand.d
	// P1 = shoulder.d
	// P2 = point that forms 2 right handed tris
	// P3 = elbow.d
	// r0 = |P3-P0| = lowerArmLen
	// r1 = |P3-P1| = upperArmLen
	// a = |P2-P0|
	// b = |P2-P1|

	Mat34V& p0_Mtx = sh.m_Objects[m_HandBoneIdx];
	Mat34V& p3_Mtx = sh.m_Objects[m_ElbowBoneIdx];
	Mat34V& p1_Mtx = sh.m_Objects[m_ShoulderBoneIdx];

	ScalarV r0 = Mag(p3_Mtx.GetCol3()-p0_Mtx.GetCol3());
	ScalarV r1 = Mag(p3_Mtx.GetCol3()-p1_Mtx.GetCol3());

	Mat34V target_Mtx(m_Target);
	Vec3V p1_to_p0(p0_Mtx.GetCol3()-p1_Mtx.GetCol3());

	Vec3V p2(V_ZERO);
	ScalarV h(V_ZERO);
	CalculateP2AndH(p0_Mtx.GetCol3(), p1_Mtx.GetCol3(), r0, r1, p2, h);

	// The direction of p3 from p2
	Vec3V hVector(p3_Mtx.GetCol3()-p2);
	Vec3V referenceDir(NormalizeSafe(hVector, Negate(p3_Mtx.GetCol1())));

	// Work out the rotation between p1_to_target and p1_to_p0
	Vec3V p1_to_target(target_Mtx.GetCol3()-p1_Mtx.GetCol3());
	ScalarV d(Mag(p1_to_target));

	p1_to_p0 = Normalize(p1_to_p0);
	p1_to_target = Normalize(p1_to_target);
	QuatV deltaQuat = QuatVFromVectors(p1_to_p0, p1_to_target);
	hVector = Transform(deltaQuat, hVector);	
	Mat34V deltaMat34(V_IDENTITY);
	Vec3V translation(V_ZERO);
	Mat34VFromQuatV(deltaMat34, deltaQuat,translation);
	Transform3x3(p1_Mtx, deltaMat34, p1_Mtx);

	ScalarV percentageReach(LoadScalar32IntoScalarV(!(m_Flags & USE_FULL_REACH) ? 0.95f : 0.98f));
	ScalarV max_d(r0+r1);
	max_d *= percentageReach;

	if(IsGreaterThanAll(d,max_d))
	{
		// Target is out of reach
		// Calculate nearest point to target we can reach
		p1_to_target = p1_to_target * max_d;
		p0_Mtx.SetCol3(p1_Mtx.GetCol3() + p1_to_target);
		d = max_d;
	}
	else
	{
		// Target is within reach
		p0_Mtx.SetCol3(target_Mtx.GetCol3());
	}

	// P0 = hand.d
	// P1 = shoulder.d
	// P2 = point that forms 2 right handed tris
	// P3 = elbow.d
	// r0 = |P3-P0| = lowerArmLen
	// r1 = |P3-P1| = upperArmLen
	// a = |P2-P0|
	// b = |P2-P1|
	// d = |P1-P0| = hadnToShoulder
	p2 = Vec3V(V_ZERO);
	h = ScalarV(V_ZERO);
	CalculateP2AndH(p0_Mtx.GetCol3(), p1_Mtx.GetCol3(), r0, r1, p2, h);

	// The elbow is h distance from P2 we just need a direction to go in.
	hVector = Normalize(hVector);
	hVector *= h;
	p3_Mtx.SetCol3(p2+hVector);

	if(m_Flags & USE_ORIENTATION)
	{
		p0_Mtx.SetCol0(target_Mtx.GetCol0());
		p0_Mtx.SetCol1(target_Mtx.GetCol1());
		p0_Mtx.SetCol2(target_Mtx.GetCol2());
	}

	// Orientate the shoulder bone toward the new position of the elbow bone
	OrientateBoneATowardBoneB(p1_Mtx, p3_Mtx);

	// Orientate the elbow bone toward the new position of the hand bone
	if(!(m_Flags & USE_TWIST_CORRECTION))
	{
		OrientateBoneATowardBoneB(p3_Mtx, p0_Mtx);
	}
	else
	{
		OrientateBoneATowardBoneB(p3_Mtx, p0_Mtx, referenceDir);
	}

	// Update locals
	UnTransformOrtho(sh.m_Locals[m_ShoulderBoneIdx], sh.m_Objects[sh.m_ParentIndices[m_ShoulderBoneIdx]], sh.m_Objects[m_ShoulderBoneIdx]);
	UnTransformOrtho(sh.m_Locals[m_ElbowBoneIdx], sh.m_Objects[sh.m_ParentIndices[m_ElbowBoneIdx]], sh.m_Objects[m_ElbowBoneIdx]);
	UnTransformOrtho(sh.m_Locals[m_HandBoneIdx], sh.m_Objects[sh.m_ParentIndices[m_HandBoneIdx]], sh.m_Objects[m_HandBoneIdx]);

	// Update objects
	for(int i=m_ShoulderBoneIdx+1; i<m_TerminatingIdx; ++i)
	{
		Transform(sh.m_Objects[i], sh.m_Objects[sh.m_ParentIndices[i]], sh.m_Locals[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

crIKSolverLegs::crIKSolverLegs()
: crIKSolverBase(IK_SOLVER_TYPE_LEGS)
{
	memset(&m_Legs, 0, NUM_LEGS * sizeof(crIKSolverLegs::Goal));
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverLegs::Solve(SolverHelper& sh) const
{
	for (int i = 0; i < NUM_LEGS; ++i)
	{
		const Goal& leg = m_Legs[i];

		if (leg.m_Enabled)
		{
			leg.Solve(sh, m_Limits);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverLegs::Goal::Solve(SolverHelper& sh, const Vec3V limits[2]) const
{
	const ScalarV vScalarZero(V_ZERO);
	const ScalarV vPercentageReachLeg(0.9999f);
	const ScalarV vEpsilon(V_FLT_SMALL_2);

	// Work out p2 and h (and hVector the direction of h) for the original animated pose
	// P0 = foot.d
	// P1 = thigh.d
	// P2 = point that forms 2 right handed tris
	// P3 = calf.d
	// r0 = |P3-P0| = lowerLegLen
	// r1 = |P3-P1| = upperLegLen
	// a = |P2-P0|
	// b = |P2-P1|

	Mat34V& p0_Mtx = sh.m_Objects[m_BoneIdx[FOOT]];
	Mat34V& p3_Mtx = sh.m_Objects[m_BoneIdx[CALF]];
	Mat34V& p1_Mtx = sh.m_Objects[m_BoneIdx[THIGH]];

	ScalarV r0 = Mag(p3_Mtx.GetCol3()-p0_Mtx.GetCol3());
	ScalarV r1 = Mag(p3_Mtx.GetCol3()-p1_Mtx.GetCol3());

	Mat34V target_Mtx(m_Target);
	Vec3V p1_to_p0(p0_Mtx.GetCol3()-p1_Mtx.GetCol3());

	Vec3V p2(V_ZERO);
	ScalarV h(V_ZERO);
	CalculateP2AndH(p0_Mtx.GetCol3(), p1_Mtx.GetCol3(), r0, r1, p2, h);

	// The direction of p3 from p2
	Vec3V hVector(p3_Mtx.GetCol3()-p2);

	// If incoming leg is hyper-extended, use calf forward
	if (IsCloseAll(h, vScalarZero, vEpsilon))
	{
		hVector = p3_Mtx.GetCol1();
	}
	else
	{
		hVector = Normalize(hVector);

		// Check if incoming leg is inverted. Cross thigh direction with
		// calf direction and validate axis against calf z axis.
		// If the same direction, then the incoming knee is behind the 
		// foot and thigh so again, use calf forward
		Vec3V p3Axis(Cross(p1_Mtx.GetCol0(), p3_Mtx.GetCol0()));

		if (IsGreaterThanAll(Dot(p3Axis, p3_Mtx.GetCol2()), vScalarZero))
		{
			hVector = p3_Mtx.GetCol1();
		}
	}

	// Work out the rotation between p1_to_target and p1_to_p0
	Vec3V p1_to_target(target_Mtx.GetCol3()-p1_Mtx.GetCol3());
	ScalarV d(Mag(p1_to_target));

	p1_to_p0 = Normalize(p1_to_p0);
	p1_to_target = Normalize(p1_to_target);
	QuatV deltaQuat = QuatVFromVectors(p1_to_p0, p1_to_target);
	hVector = Transform(deltaQuat, hVector);
	Mat34V deltaMat34(V_IDENTITY);
	Vec3V translation(V_ZERO);
	Mat34VFromQuatV(deltaMat34, deltaQuat,translation);
	Transform3x3(p1_Mtx, deltaMat34, p1_Mtx);

	ScalarV max_d(Add(r0, r1));
	max_d = Scale(max_d, vPercentageReachLeg);

	if (IsGreaterThanAll(d, max_d))
	{
		// Target is out of reach
		// Calculate nearest point to target we can reach
		p1_to_target = p1_to_target * max_d;
		p0_Mtx.SetCol3(p1_Mtx.GetCol3() + p1_to_target);
		d = max_d;
	}
	else
	{
		// Target is within reach
		p0_Mtx.SetCol3(target_Mtx.GetCol3());
	}

	// P0 = foot.d
	// P1 = thigh.d
	// P2 = point that forms 2 right handed tris
	// P3 = calf.d
	// r0 = |P3-P0| = lowerLegLen
	// r1 = |P3-P1| = upperLegLen
	// a = |P2-P0|
	// b = |P2-P1|
	// d = |P1-P0| = footToThigh
	p2 = Vec3V(V_ZERO);
	h = vScalarZero;
	CalculateP2AndH(p0_Mtx.GetCol3(), p1_Mtx.GetCol3(), r0, r1, p2, h);

	// The calf is h distance from P2 we just need a direction to go in.
	hVector *= Max(h, vEpsilon);
	p3_Mtx.SetCol3(p2+hVector);

	if (m_Flags & USE_ORIENTATION)
	{
		p0_Mtx.SetCol0(target_Mtx.GetCol0());
		p0_Mtx.SetCol1(target_Mtx.GetCol1());
		p0_Mtx.SetCol2(target_Mtx.GetCol2());
	}

	// Orientate the hip bone toward the new position of the calf bone
	OrientateBoneATowardBoneB(p1_Mtx, p3_Mtx);

	// Orientate the calf bone toward the new position of the foot bone
	OrientateBoneATowardBoneB(p3_Mtx, p0_Mtx);

	// Clamp foot bone
	if (m_Flags & USE_LIMITS)
	{
		UnTransform3x3Full(p0_Mtx, p3_Mtx, p0_Mtx);
		Vec3V eulers(Mat34VToEulersXYZ(p0_Mtx));

		// If rotation about z is < -PI/2, foot has become over rotated relative to oriented calf bone above,
		// so unwrap angle (+2PI) before clamping
		ScalarV compensation(SelectFT(IsLessThan(eulers.GetZ(), ScalarV(V_NEG_PI_OVER_TWO)), vScalarZero, ScalarV(V_TWO_PI)));
		eulers.SetZ(Add(eulers.GetZ(), compensation));

		eulers = Clamp(eulers, limits[0], limits[1]);
		Mat34VFromEulersXYZ(p0_Mtx, eulers, p0_Mtx.GetCol3());
		Transform3x3(p0_Mtx, p3_Mtx, p0_Mtx);
	}

	// Update any child bones
	for (int i = m_BoneIdx[FOOT] + 1; i < m_TerminatingIdx; ++i)
	{
		Transform(sh.m_Objects[i], sh.m_Objects[sh.m_ParentIndices[i]], sh.m_Locals[i]);
	}
	Transform(sh.m_Objects[m_BoneIdx[THIGHROLL]], sh.m_Objects[sh.m_ParentIndices[m_BoneIdx[THIGHROLL]]], sh.m_Locals[m_BoneIdx[THIGHROLL]]);
}

////////////////////////////////////////////////////////////////////////////////

crIKSolverQuadruped::crIKSolverQuadruped()
: crIKSolverBase(IK_SOLVER_TYPE_QUADRUPED)
, m_UpperLegRatio(0.97f)
{
	memset(&m_Legs, 0, NUM_LEGS * sizeof(crIKSolverQuadruped::Goal));
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverQuadruped::Solve(SolverHelper& sh) const
{
	for (int i = 0; i < NUM_LEGS; ++i)
	{
		const Goal& leg = m_Legs[i];

		if (leg.m_Enabled)
		{
			leg.Solve(sh, m_UpperLegRatio);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverQuadruped::Goal::Solve(SolverHelper& sh, float upperLegRatio) const
{
	Mat34V& mtxThgh = sh.m_Objects[m_BoneIdx[THIGH]];
	Mat34V& mtxCalf = sh.m_Objects[m_BoneIdx[CALF]];
	Mat34V& mtxFoot = sh.m_Objects[m_BoneIdx[FOOT]];
	Mat34V& mtxToe0 = sh.m_Objects[m_BoneIdx[TOE]];
	Mat34V& mtxToe1 = sh.m_Objects[m_BoneIdx[TOE1]];

	Mat34V mtxTarget;
	Mat34VFromTransformV(mtxTarget, m_Target);

	// Solve upper leg (thigh, calf, foot)
	Vec3V vToe1ToTarg(Subtract(mtxTarget.GetCol3(), mtxToe1.GetCol3()));
	Vec3V vToe1ToThgh(Normalize(Subtract(mtxThgh.GetCol3(), mtxToe1.GetCol3())));

	// Project distance from toe1 to target onto toe1 to thigh direction
	ScalarV vLength(Dot(vToe1ToTarg, vToe1ToThgh));
	vLength = Scale(vLength, ScalarV(upperLegRatio));
	Vec3V vTargetPos(AddScaled(mtxFoot.GetCol3(), vToe1ToThgh, vLength));

	Solve2Bone(sh, FOOT, vTargetPos, mtxThgh, mtxCalf, mtxFoot);

	// Solve lower leg (foot, toe0, toe1)
	Solve2Bone(sh, TOE1, mtxTarget.GetCol3(), mtxFoot, mtxToe0, mtxToe1);
}

////////////////////////////////////////////////////////////////////////////////

void crIKSolverQuadruped::Goal::Solve2Bone(SolverHelper& sh, eLegPart effector, Vec3V_In targetPos, Mat34V_Ref upper, Mat34V_Ref mid, Mat34V_Ref lower) const
{
	const Vec3V vZero(V_ZERO);
	const ScalarV vScalarZero(V_ZERO);
	const ScalarV vEpsilon(V_FLT_SMALL_3);
	const ScalarV vReachLimit(0.9999f);

	Vec3V vTargetPos(targetPos);

	Vec3V vP2(vZero);
	ScalarV vH(vScalarZero);

	ScalarV vR0(Mag(Subtract(mid.GetCol3(), lower.GetCol3())));
	ScalarV vR1(Mag(Subtract(mid.GetCol3(), upper.GetCol3())));

	CalculateP2AndH(lower.GetCol3(), upper.GetCol3(), vR0, vR1, vP2, vH);

	// If hyper-extended, use mid forward
	Vec3V vHalfVector(SelectFT(IsClose(vH, vScalarZero, vEpsilon), Normalize(Subtract(mid.GetCol3(), vP2)), mid.GetCol1()));

	// Rotate UpperToLower to UpperToTarget
	Vec3V vUpperToLower(Subtract(lower.GetCol3(), upper.GetCol3()));
	Vec3V vUpperToTarget(Subtract(vTargetPos, upper.GetCol3()));
	ScalarV vMagUpperToTarget(Mag(vUpperToTarget));

	vUpperToLower = Normalize(vUpperToLower);
	vUpperToTarget = Normalize(vUpperToTarget);

	QuatV qRotation(QuatVFromVectors(vUpperToLower, vUpperToTarget));
	vHalfVector = Transform(qRotation, vHalfVector);

	Mat34V mtxRotation(V_IDENTITY);
	Mat34VFromQuatV(mtxRotation, qRotation);
	Transform3x3(upper, mtxRotation, upper);

	// Adjust target position if beyond max length
	ScalarV vMaxLength(Scale(Add(vR0, vR1), vReachLimit));
	vTargetPos = SelectFT(IsGreaterThan(vMagUpperToTarget, vMaxLength), vTargetPos, AddScaled(upper.GetCol3(), vUpperToTarget, vMaxLength));

	lower.SetCol3(vTargetPos);

	vP2 = vZero;
	vH = vScalarZero;
	CalculateP2AndH(lower.GetCol3(), upper.GetCol3(), vR0, vR1, vP2, vH);

	mid.SetCol3(AddScaled(vP2, vHalfVector, Max(vH, vEpsilon)));

	// Orientate the upper bone toward the new position of the mid bone
	OrientateBoneATowardBoneB(upper, mid);

	// Orientate the mid bone toward the new position of the lower bone
	OrientateBoneATowardBoneB(mid, lower);

	// Update child bones
	for (int boneIdx = m_BoneIdx[effector] + 1; boneIdx < m_TerminatingIdx; ++boneIdx)
	{
		Transform(sh.m_Objects[boneIdx], sh.m_Objects[sh.m_ParentIndices[boneIdx]], sh.m_Locals[boneIdx]);
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
