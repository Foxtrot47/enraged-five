// Format subtypes:
// * - Ignored (pad field)
// x - 32-bit hex
// f - 32-bit float
// b - bool (1 or 0)
// e - Enumerant (default size is 32)
// h - Halfword (default size is 16)
// Each subtype can be followed by a :XX: to specify the field width
// Furthermore it can be followed by :Name: to name the field
// Multiple fields are separated by semicolons.
RR(0x0000,"Set Sub Channel","x")
RR(0x0050,"Fence Reference","x")
RR(0x0064,"Semaphore Address","x")
RR(0x0068,"Semaphore Wait Value","x")
RR(0x006C,"Semaphore Set Value","x")
RR(0x0100,"No operation","X")

RR(0x0200,"Set Surface Clip Horizontal","x")
RR(0x0204,"Set Surface Clip Vertical","x")
RR(0x0208,"Set Surface Format","x")
RR(0x020C,"Set Surface Pitch A","x")
RR(0x0210,"Set Surface Pitch A Offset","x")
RR(0x0214,"Set Surface Zeta Offset","x")
RR(0x0218,"Set Surface Color B Offset","x")
RR(0x021C,"Set Surface Pitch B","x")
RR(0x0220,"Set Surface Color Target","x")
RR(0x022C,"Set Surface Pitch Z","x")
RR(0x0280,"Set Surface Pitch C","x")
RR(0x0284,"Set Surface Pitch D","x")
RR(0x0288,"Set Surface Color C Offset","x")
RR(0x028C,"Set Surface Color D Offset","x")
RR(0x02B8,"Set Window Offset","x")

RR(0x0304,"Alpha Test Enable","b")
RR(0x0308,"Alpha Function","e0x200=NEVER,LESS,EQUAL,LEQUAL,GREATER,NOT_EQUAL,GEQUAL,ALWAYS")
RR(0x030C,"Alpha Reference Value","x")
RR(0x0310,"Blend Enable","b")
RR(0x0314,"Blend Source Factor","e:16:RGB:0x0=ZERO,ONE,0x300=SRC_COLOR,ONE_MINUS_SRC_COLOR,DST_ALPHA,ONE_MINUS_DST_ALPHA,DST_COLOR,ONE_MINUS_DST_COLOR,SRC_ALPHA_SATURATE,0x8001=CONSTANT_COLOR,ONE_MINUS_CONSTANT_COLOR,CONSTANT_ALPHA,ONE_MINUS_CONSTANT_ALPHA;e:16:Alpha:0x0=ZERO,ONE,0x300=SRC_COLOR,ONE_MINUS_SRC_COLOR,DST_ALPHA,ONE_MINUS_DST_ALPHA,DST_COLOR,ONE_MINUS_DST_COLOR,SRC_ALPHA_SATURATE,0x8001=CONSTANT_COLOR,ONE_MINUS_CONSTANT_COLOR,CONSTANT_ALPHA,ONE_MINUS_CONSTANT_ALPHA")
RR(0x0318,"Blend Destination Factor","e:16:RGB:0x0=ZERO,ONE,0x300=SRC_COLOR,ONE_MINUS_SRC_COLOR,DST_ALPHA,ONE_MINUS_DST_ALPHA,DST_COLOR,ONE_MINUS_DST_COLOR,0x8001=CONSTANT_COLOR,ONE_MINUS_CONSTANT_COLOR,CONSTANT_ALPHA,ONE_MINUS_CONSTANT_ALPHA;e:16:Alpha:0x0=ZERO,ONE,0x300=SRC_COLOR,ONE_MINUS_SRC_COLOR,DST_ALPHA,ONE_MINUS_DST_ALPHA,DST_COLOR,ONE_MINUS_DST_COLOR,0x8001=CONSTANT_COLOR,ONE_MINUS_CONSTANT_COLOR,CONSTANT_ALPHA,ONE_MINUS_CONSTANT_ALPHA")
RR(0x031C,"Blend Color","x")
RR(0x0320,"Blend Equation","e0x8006=ADD,MIN,MAX,0x800A=FUNC_SUBTRACT,FUNC_REVERSE_SUBTRACT")
RR(0x0324,"Color Write Mask","x")
RR(0x0328,"Front Stencil Test Enable","b")
RR(0x032C,"Front Stencil Write Mask","x")
RR(0x0330,"Front Stencil Function","e0x200=NEVER,LESS,EQUAL,LEQUAL,GREATER,NOT_EQUAL,GEQUAL,ALWAYS")
RR(0x0334,"Front Stencil Reference Value","x")
RR(0x0338,"Front Stencil Compare Mask","x")
RR(0x033C,"Front Stencil Fail Operation","e0x0=ZERO,0x1e00=KEEP,0x1e01=REPLACE,INCR,DECR,0x150A=INVERT,0x8507=INCR_WRAP,DECR_WRAP")
RR(0x0340,"Front Stencil Pass Depth Fail Operation","e0x0=ZERO,0x1e00=KEEP,0x1e01=REPLACE,INCR,DECR,0x150A=INVERT,0x8507=INCR_WRAP,DECR_WRAP")
RR(0x0344,"Front Stencil Pass Depth Pass Operation","e0x0=ZERO,0x1e00=KEEP,0x1e01=REPLACE,INCR,DECR,0x150A=INVERT,0x8507=INCR_WRAP,DECR_WRAP")
RR(0x0348,"Back Stencil Test Enable","b")
RR(0x034C,"Back Stencil Write Mask","x")
RR(0x0350,"Back Stencil Function","e0x200=NEVER,LESS,EQUAL,LEQUAL,GREATER,NOT_EQUAL,GEQUAL,ALWAYS")
RR(0x0354,"Back Stencil Reference Value","x")
RR(0x0358,"Back Stencil Compare Mask","x")
RR(0x035C,"Back Stencil Fail Operation","e0x0=ZERO,0x1e00=KEEP,0x1e01=REPLACE,INCR,DECR,0x150A=INVERT,0x8507=INCR_WRAP,DECR_WRAP")
RR(0x0360,"Back Stencil Pass Depth Fail Operation","e0x0=ZERO,0x1e00=KEEP,0x1e01=REPLACE,INCR,DECR,0x150A=INVERT,0x8507=INCR_WRAP,DECR_WRAP")
RR(0x0364,"Back Stencil Pass Depth Pass Operation","e0x0=ZERO,0x1e00=KEEP,0x1e01=REPLACE,INCR,DECR,0x150A=INVERT,0x8507=INCR_WRAP,DECR_WRAP")
RR(0x0368,"Shade Model","e0x1d00=FLAT,SMOOTH")
RR(0x0394,"Set Clip Min","f")
RR(0x0398,"Set Clip Max","f")
RR(0x03B0,"Set Control0","x")

RR(0x08C0,"Set Scissor Horizontal","x")
RR(0x08C4,"Set Scissor Vertical","x")
RR(0x08E4,"Fragment Program Address","x")

RRR(0x0900,0x20,4,"Vertex Program Texture %d Address","x")
RRR(0x0904,0x20,4,"Vertex Program Texture %d Format","x")
RRR(0x0908,0x20,4,"Vertex Program Texture %d Control 1","x")
RRR(0x090C,0x20,4,"Vertex Program Texture %d Control 2","x")
RRR(0x0910,0x20,4,"Vertex Program Texture %d Swizzle","x")
RRR(0x0914,0x20,4,"Vertex Program Texture %d Filter","x")
RRR(0x0918,0x20,4,"Vertex Program Texture %d Size","x")
RRR(0x091C,0x20,4,"Vertex Program Texture %d Border Color","x")

RR(0x0A00,"Viewport left-width","hLeft;hWidth")
RR(0x0A04,"Viewport top-height","hTop;hHeight")
RR(0x0A1C,"Z Cull Synch","x")
RR(0x0A20,"Viewport Translate X","f")
RR(0x0A24,"Viewport Translate Y","f")
RR(0x0A28,"Viewport Translate Z","f")
RR(0x0A2C,"Viewport Translate W","f")
RR(0x0A30,"Viewport Scale X","f")
RR(0x0A34,"Viewport Scale Y","f")
RR(0x0A38,"Viewport Scale Z","f")
RR(0x0A3C,"Viewport Scale W","f")
RR(0x0A60,"Polygon Offset Point Enable","b")
RR(0x0A64,"Polygon Offset Line Enable","b")
RR(0x0A70,"Set Depth Mask","x")
RR(0x0A74,"Set Depth Test Enable","x")

RRR(0x0B00,0x04,16,"Texture %d Control 3","x")
RRR(0x0B40,0x04,10,"Texture Coordinate %d Control","b:4:InterpXY;b:4:CentroidCorrection")
RRR(0x0B80,0x04,4,"Vertex Program Instruction Word %d","x")

RRR(0x1680,0x04,16,"Vertex Attribute %d Array Offset","x")
RR(0x1710,"Pre Transform Vertex Cache Invalidate","x")
RR(0x1714,"Post Transform Vertex Cache Invalidate","x")
RR(0x1718,"Graphics Pipe NOP","x")
RR(0x1738,"Vertex Array Base Offset","x")
RR(0x173C,"Index Array Base Offset","x")
RRR(0x1740,0x04,16,"Vertex Attribute %d Format","e:4:Format:0x1=norm16,fp32,fp16,nu8,s16,10-11-11,u8;h:4:Count;h:8:Stride;hDivider")
RR(0x17C8,"Reset Internal Counter","x")
RR(0x17CC,"Pixel Count Mode","x")
RR(0x1800,"Write Report","x")
RR(0x1804,"Z Cull Report Enable","x")
RR(0x1808,"Draw Mode","e0x0=STOP,POINTS,LINES,LINE_LOOP,LINE_STRIP,TRIANGLES,TRIANGLE_STRIP,TRIANGLE_FAN,QUADS,QUAD_STRIP,POLYGON")
RR(0x180C,"Draw Indexed Elements Inline","x")
RR(0x1814,"Draw Arrays","x:24:Offset;x:8:CountMinus1")
RR(0x1818,"Draw Arrays Inline","x")
RR(0x181C,"Index Buffer Address","x")
RR(0x1820,"Index Buffer Format","e:4:Mem:0x0=LOCAL,SYSTEM;e:4:Size:0x0=UNSIGNED,UNSIGNED_SHORT")
RR(0x1824,"Draw Indexed Elements","x:24:Offset;x:8:CountMinus1")
RR(0x1828,"Front Polygon Mode","e0x1B00=POINT,LINE,FILL")
RR(0x182C,"Back Polygon Mode","e0x1B00=POINT,LINE,FILL")
RR(0x1830,"Cull Face Mode","e0x404=FRONT,BACK,0x408=FRONT_AND_BACK")
RR(0x1834,"Front Face Mode","e0x900=CW,CCW")

RRR(0x1840,0x04,16,"Texture %d Size 2","hRowPitch;*:4:;h:12:Depth")
RRR(0x1A00,0x20,16,"Texture %d Data Offset","x")
RRR(0x1A04,0x20,16,"Texture %d Format","e:2:Mem:0x1=LOCAL,SYSTEM;b:1:IsCube;b:1:NoBorder;h:4:Dim;e:5:Format:0x1=L8,R5G5B5A1,RGBA4,R5G6B5,A8R8G8B8,DXT1,DXT3,DXT5,0xB=A8L8,0x10=DEPTH24,0x12=DEPTH16,0x14=L16,A16L16,0x18=UsHILO8,SiHILO8,0x1B=4xFP32,FP32,4xFP16;e:1:0x0=Swizzled,Linear;e:1:0x0=Nrm,Unnrm;*:1:;h:4:Levels")
RRR(0x1A08,0x20,16,"Texture %d Control 1","e:4:S Wrap:0x1=REPEAT,0x5=CLAMP;*:1:;b:1:AnisoOpt;*:2:;e:4:T Wrap:0x1=REPEAT,0x5=CLAMP;b:4:NrmExpand;e:4:R Wrap:0x1=REPEAT,0x5=CLAMP;x:8:Gamma;e:4:R Comp:0x0=NEVER,GREATER,EQUAL,GEQUAL,LESS,NOT_EQUAL,LEQUAL,ALWAYS")
RRR(0x1A0C,0x20,16,"Texture %d Control 2","*:4:;e:4:Aniso:0x0=Disabled,2,4,6,8,10,12,16;*:7:;h:4:MaxLevel;*:8:;h:4:MinLevel;b:1:Enabled")
RRR(0x1A10,0x20,16,"Texture %d Swizzle","x")
RRR(0x1A14,0x20,16,"Texture %d Filter","x")
RRR(0x1A18,0x20,16,"Texture %d Size 1","hHeight;hWidth")
RRR(0x1A1C,0x20,16,"Texture %d Border Color","c")
RRR(0x1C00,0x04,64,"Vertex4f %d Data4f","f")
RRR(0x1D00,0x04,16,"Texture %d Color Key","c")

RR(0x1D60,"Fragment Program Control","x")
RR(0x1D64,"Vertex Program Constant Range","x")
RR(0x1D6C,"Set Semaphore Offset","x")
RR(0x1D74,"Texture Read Semaphore Release","x")
RR(0x1D78,"Set ZMin ZMax Control","x")
RR(0x1D7C,"Set Antialiasing Control","x")
RR(0x1D88,"Set Shader Window","x")

RR(0x1D90,"Color Clear Value","c")
RR(0x1D94,"Clear","b:1:Depth;b:1:Stencil;*:2:;b:1:Blue;b:1:Green;b:1:Red;b:1:Alpha")
RR(0x1DAC,"Primitive Restart Enable","b")
RR(0x1DB0,"Primitive Restart Index","x")

RR(0x1E9C,"Vertex Program Instruction Load Slot","x")
RR(0x1EA0,"Vertex Program Execute Slot","x")

RR(0x1EF8,"Vertex Program Limits","x")
RR(0x1EFC,"Vertex Program Constant Load Slot","x")
RRR(0x1F00,0x04,32,"Vertex Program Constant Slot %d","f")

RR(0x1FC0,"Frequency Divider","x")
RR(0x1FE0,"Set Reduce Dest Color","x")
RR(0x1FEC,"Shader Packer","x")
RR(0x1FD8,"Texture Cache Invalidate","x")
