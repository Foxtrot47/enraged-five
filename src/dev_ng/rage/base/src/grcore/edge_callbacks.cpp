// 
// grcore/edge_callbacks.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#if HACK_MC4
#include "edge_callbacks_mc4.cpp"
#elif HACK_GTA4
#include "edge_callbacks_gta4.cpp"
#endif
