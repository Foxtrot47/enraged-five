﻿/* SCE CONFIDENTIAL
PlayStation(R)4 Programmer Tool Runtime Library Release 01.700.081
* Copyright (C) 2013 Sony Computer Entertainment Inc.
* All Rights Reserved.
*/

#ifndef _SCE_GNMX_ERROR_GEN_H
#define _SCE_GNMX_ERROR_GEN_H

/*
 * Error number definition
 */

/**
 * @j Dispatch Drawにおいて、引数が不正です。 @ej
 * @e Invalid Argument in Dispatch Draw. @ee
 */
#define SCE_GNMX_ERROR_DISPATCH_DRAW_INVALID_ARGUMENTS -2133716992	/* 0x80D21000 */

/**
 * @j Dispatch Drawにおいて、Index Dataが不正です。 @ej
 * @e Out of space for index data in Dispatch Draw. @ee
 */
#define SCE_GNMX_ERROR_DISPATCH_DRAW_OUT_OF_SPACE_FOR_INDEX_DATA -2133716991	/* 0x80D21001 */

/**
 * @j Dispatch Drawにおいて、Block Offsetが不正です。 @ej
 * @e Out of space for block offset in Dispatch Draw. @ee
 */
#define SCE_GNMX_ERROR_DISPATCH_DRAW_OUT_OF_SPACE_FOR_BLOCK_OFFSET -2133716990	/* 0x80D21002 */

/**
 * @j Dispatch Drawにおいて、Offsetが不正です。 @ej
 * @e Unrepresentable offset in Dispatch Draw. @ee
 */
#define SCE_GNMX_ERROR_DISPATCH_DRAW_UNREPRESENTABLE_OFFSET -2133716989	/* 0x80D21003 */



#endif //_SCE_GNMX_ERROR_GEN_H
