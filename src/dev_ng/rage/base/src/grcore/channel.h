// 
// grcore/channel.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
// Generated by makechannel.bat, do not modify. 
// 

#ifndef GRCORE_CHANNEL_H 
#define GRCORE_CHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(Graphics)

#define grcAssert(cond)						RAGE_ASSERT(Graphics,cond)
#define grcAssertf(cond,fmt,...)			RAGE_ASSERTF(Graphics,cond,fmt,##__VA_ARGS__)
#define grcVerifyf(cond,fmt,...)			RAGE_VERIFYF(Graphics,cond,fmt,##__VA_ARGS__)
#define grcErrorf(fmt,...)					RAGE_ERRORF(Graphics,fmt,##__VA_ARGS__)
#define grcWarningf(fmt,...)				RAGE_WARNINGF(Graphics,fmt,##__VA_ARGS__)
#define grcDisplayf(fmt,...)				RAGE_DISPLAYF(Graphics,fmt,##__VA_ARGS__)
#define grcDebugf1(fmt,...)					RAGE_DEBUGF1(Graphics,fmt,##__VA_ARGS__)
#define grcDebugf2(fmt,...)					RAGE_DEBUGF2(Graphics,fmt,##__VA_ARGS__)
#define grcDebugf3(fmt,...)					RAGE_DEBUGF3(Graphics,fmt,##__VA_ARGS__)
#define grcLogf(severity,fmt,...)			RAGE_LOGF(Graphics,severity,fmt,##__VA_ARGS__)


RAGE_DECLARE_CHANNEL(PCDevice)

#define pcdAssert(cond)						RAGE_ASSERT(PCDevice,cond)
#define pcdAssertf(cond,fmt,...)			RAGE_ASSERTF(PCDevice,cond,fmt,##__VA_ARGS__)
#define pcdVerifyf(cond,fmt,...)			RAGE_VERIFYF(PCDevice,cond,fmt,##__VA_ARGS__)
#define pcdErrorf(fmt,...)					RAGE_ERRORF(PCDevice,fmt,##__VA_ARGS__)
#define pcdWarningf(fmt,...)				RAGE_WARNINGF(PCDevice,fmt,##__VA_ARGS__)
#define pcdDisplayf(fmt,...)				RAGE_DISPLAYF(PCDevice,fmt,##__VA_ARGS__)
#define pcdDebugf1(fmt,...)					RAGE_DEBUGF1(PCDevice,fmt,##__VA_ARGS__)
#define pcdDebugf2(fmt,...)					RAGE_DEBUGF2(PCDevice,fmt,##__VA_ARGS__)
#define pcdDebugf3(fmt,...)					RAGE_DEBUGF3(PCDevice,fmt,##__VA_ARGS__)
#define pcdLogf(severity,fmt,...)			RAGE_LOGF(PCDevice,severity,fmt,##__VA_ARGS__)

#endif // GRCORE_CHANNEL_H 
