//
// creature/componentptfx.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTPTFX_H
#define CREATURE_COMPONENTPTFX_H

#include "componentroot.h"

#include "atl/string.h"

namespace rage {

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creature mover component
class crCreatureComponentParticleEffect : public crCreatureComponentRoot
{
public:
    // PURPOSE: Constructor
    crCreatureComponentParticleEffect();

    // PURPOSE: Initializing constructor
    // PARAMS: creature - reference to creature this component is part of
    // parentMtx - optional pointer to the matrix to use as a parent
    crCreatureComponentParticleEffect(crCreature& creature, const Mat34V* parentMtx=NULL);

    // PURPOSE: Resource constructor
    crCreatureComponentParticleEffect(datResource&);

    // PURPOSE: Destructor
    virtual ~crCreatureComponentParticleEffect();

    // PURPOSE: Declare type
    CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentParticleEffect);

    // PURPOSE: Resourcing
#if __DECLARESTRUCT
    virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

    // PURPOSE: Initialization
    // PARAMS: creature - reference to creature this component is part of
    // pParentMtx - optional pointer to the matrix to use as a parent
    void Init(crCreature& creature, const Mat34V* parentMtx=NULL);

	// PURPOSE: Shutdown 
	virtual void Shutdown();

    // PURPOSE: Pose override
    virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

    // PURPOSE: Inverse Pose override
    virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

    // PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

    // PURPOSE: Identity override
    virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

    // PURPOSE: Reset override
    virtual void Reset(const crCreatureFilter&);

#if !__FINAL
    // PURPOSE: Debug draw. Call to debug draw the creature
    // NOTES: For development only (!__FINAL)
    virtual void DebugDraw() const;
#endif // !__FINAL

	// PURPOSE: Get the number of evolution parameters
	u32 GetNumEvolutionParameters() const;

	// PURPOSE: Get evolution parameter value (by index)
	float GetEvolutionParameterValueByIndex(u32 idx) const;

	// PURPOSE: Set evolution parameter value (by index)
	void SetEvolutionParameterValueByIndex(u32 idx, float val);

	// PURPOSE: Find evolution parameter index by name/hash/id
	// RETURNS: false - failed to find (outIdx will be undefined), true - found (index in outIdx)
	// NOTE: For id find only, if return value false, outIdx will indicate insertion index
	bool FindEvolutionParameterIndexByName(const char* name, u32& outIdx) const;
	bool FindEvolutionParameterIndexByHash(u32 hash, u32& outIdx) const;
	bool FindEvolutionParameterIndexById(u16 id, u32& outIdx) const;

	// PURPOSE: Add evolution parameter
	bool AddEvolutionParameter(const char* name);

	// PURPOSE: Remove all evolution parameters
	void RemoveAllEvolutionParameters();


protected:

	// PURPOSE: Re-calculate signature
	virtual u32 CalcSignature() const;

private:

	// PURPOSE: Represents an evolution parameter mapping internally
	struct EvolutionParameter
	{
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		atString m_Name;
		u32 m_Hash;
		u16 m_Id;

		float m_Value;
	};

	atArray<EvolutionParameter> m_EvolutionParameters;
};

////////////////////////////////////////////////////////////////////////////////

inline u32 crCreatureComponentParticleEffect::GetNumEvolutionParameters() const
{
	return m_EvolutionParameters.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline float crCreatureComponentParticleEffect::GetEvolutionParameterValueByIndex(u32 idx) const
{
	return m_EvolutionParameters[idx].m_Value;
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentParticleEffect::SetEvolutionParameterValueByIndex(u32 idx, float val)
{
	m_EvolutionParameters[idx].m_Value = val;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREATURE_COMPONENTPTFX_H
