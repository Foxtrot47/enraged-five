//
// creature/componentlight.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTLIGHT_H
#define CREATURE_COMPONENTLIGHT_H

#include "componentroot.h"

#include "grcore/light.h"

namespace rage
{
class crFrame;

// PURPOSE: Creature mover component
class crCreatureComponentLight : public crCreatureComponentRoot
{
public:
    // PURPOSE: Constructor
    crCreatureComponentLight();

    // PURPOSE: Initializing constructor
    // PARAMS: creature - reference to creature this component is part of
    // parentMtx - optional pointer to the matrix to use as a parent
    crCreatureComponentLight(crCreature& creature, const Mat34V* pParentMtx=NULL);

    // PURPOSE: Resource constructor
    crCreatureComponentLight(datResource&);

    // PURPOSE: Destructor
    virtual ~crCreatureComponentLight();

    // PURPOSE: Declare type
    CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentLight);

    // PURPOSE: Offline resourcing
#if __DECLARESTRUCT
    virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

    // PURPOSE: Initialization
    // PARAMS: creature - reference to creature this component is part of
    // pParentMtx - optional pointer to the matrix to use as a parent
    void Init(crCreature& creature, const Mat34V* pParentMtx=NULL);

    // PURPOSE: Pose override
    virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

    // PURPOSE: Inverse Pose override
    virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

    // PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

    // PURPOSE: Identity override
    virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

    // PURPOSE: Reset override
    virtual void Reset(const crCreatureFilter&);

#if !__FINAL
    // PURPOSE: Debug draw. Call to debug draw the creature
    // NOTES: For development only (!__FINAL)
    virtual void DebugDraw() const;
#endif // !__FINAL

    // PURPOSE: Retrieves the type of this light
    // RETURNS: The light type (grcLightGroup::grcLightType)
    u32 GetLightType() const;

    // PURPOSE: Retrieves the color of the light
    // RETURNS: An RGB color
    // NOTES:
    Vec3V_Out GetColor() const;

    // PURPOSE: Retrieves the intensity of the light
    // RETURNS: The light intensity
    float GetIntensity() const;

    // PURPOSE: Retrieves the rate of fall off
    // RETURNS: The fall off rate
    float GetFallOff() const;

    // PURPOSE: Retrieves the cone angle.  If this is 0.0f, then the light is directional (aka infinite)
    // RETURNS: The cone angle in radians
    float GetConeAngle() const;

    // PURPOSE: Configures the light object using the current DOFs and returns it.
    // PARAMS:
    //    light - the light to configure
    // RETURNS: the light that was configured
    grcLightGroup::grcLightSource& ConfigureLight( grcLightGroup::grcLightSource &light ) const;

protected:
    Vec3V m_color;
    mutable u32 m_lightType;
    float m_intensity;
    float m_fallOff;
    float m_coneAngle;
};

inline u32 crCreatureComponentLight::GetLightType() const
{
    return m_lightType;
}

inline Vec3V_Out crCreatureComponentLight::GetColor() const
{
    return m_color;
}

inline float crCreatureComponentLight::GetIntensity() const
{
    return m_intensity;
}

inline float crCreatureComponentLight::GetFallOff() const
{
    return m_fallOff;
}

inline float crCreatureComponentLight::GetConeAngle() const
{
    return m_coneAngle * DtoR;
}

} // namespace rage

#endif // CREATURE_COMPONENTLIGHT_H
