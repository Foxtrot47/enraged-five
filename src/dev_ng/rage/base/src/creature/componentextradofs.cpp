//
// creature/componentextradofs.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "componentextradofs.h"

#include "creature.h"

#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "data/safestruct.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentExtraDofs::crCreatureComponentExtraDofs()
: crCreatureComponent(kCreatureComponentTypeExtraDofs)
, m_UseFrameBuffer(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentExtraDofs::crCreatureComponentExtraDofs(crCreature& creature, const crFrameData& frameData, crCommonPool* pool)
: crCreatureComponent(kCreatureComponentTypeExtraDofs)
, m_UseFrameBuffer(false)
{
	Init(creature, frameData, pool);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentExtraDofs::crCreatureComponentExtraDofs(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentExtraDofs::~crCreatureComponentExtraDofs()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentExtraDofs, crCreatureComponent::kCreatureComponentTypeExtraDofs);

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentExtraDofs::Init(crCreature& creature, const crFrameData& frameData, crCommonPool* pool)
{
	PreInit(creature);

	m_FrameBuffer.SetFrameData(frameData);
	if(pool)
	{
		m_UseFrameBuffer = true;
		m_FrameBuffer.Init(*pool);

		m_IdentityFrame = m_FrameBuffer.AllocateFrame();
		m_PoseFrame = m_FrameBuffer.AllocateFrame();
	}
	else
	{
		m_UseFrameBuffer = false;
		m_IdentityFrame = rage_new crFrame;
		m_IdentityFrame->Init(frameData);

		m_PoseFrame = rage_new crFrame;
		m_PoseFrame->Init(frameData);
	}

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentExtraDofs::Shutdown()
{
	if(m_IdentityFrame)
	{
		if(m_UseFrameBuffer)
		{
			m_FrameBuffer.ReleaseFrame(m_IdentityFrame);
		}
		else
		{
			m_IdentityFrame->Release();
		}

		m_IdentityFrame = NULL;
	}

	if(m_PoseFrame)
	{
		if(m_UseFrameBuffer)
		{
			m_FrameBuffer.ReleaseFrame(m_PoseFrame);
		}
		else
		{
			m_PoseFrame->Release();
		}

		m_PoseFrame = NULL;
	}

	m_FrameBuffer.Shutdown();

	crCreatureComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentExtraDofs::ExchangeFrameData(const crFrameData& frameData)
{
	Assert(m_IdentityFrame);
	Assert(m_PoseFrame);

	Assert(&frameData != m_FrameBuffer.GetFrameData());

	m_FrameBuffer.SetFrameData(frameData);
	if(m_UseFrameBuffer)
	{
		crFrame* identityFrame = m_FrameBuffer.AllocateFrame();
		if(m_IdentityFrame)
		{
			identityFrame->Set(*m_IdentityFrame);
			m_FrameBuffer.ReleaseFrame(m_IdentityFrame);
		}
		m_IdentityFrame = identityFrame;

		crFrame* poseFrame = m_FrameBuffer.AllocateFrame();
		if(m_PoseFrame)
		{
			poseFrame->Set(*m_PoseFrame);
			m_FrameBuffer.ReleaseFrame(m_PoseFrame);
		}
		m_PoseFrame = poseFrame;
	}
	else
	{
		AssertVerify(m_IdentityFrame->ExchangeFrameData(frameData));
		AssertVerify(m_PoseFrame->ExchangeFrameData(frameData));
	}

	CalcAndSetSignature();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentExtraDofs::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip)
{
	Assert(m_PoseFrame);
	if(!skip.UseComposer())
	{
		m_PoseFrame->Set(frame, filter);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentExtraDofs::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
	Assert(m_PoseFrame);
	if(!skip.UseComposer())
	{
		frame.Set(*m_PoseFrame, filter);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentExtraDofs::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	Assert(m_IdentityFrame);
	it.IterateFrame(*m_IdentityFrame);
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentExtraDofs::HasDof(u8 track, u16 id) const
{
	Assert(m_IdentityFrame);
	return m_IdentityFrame->HasDof(track, id);
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentExtraDofs::GetNumDofs() const
{
	Assert(m_IdentityFrame);
	return m_IdentityFrame->GetNumDofs();
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentExtraDofs::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	Assert(m_IdentityFrame);
	if(m_IdentityFrame->GetFrameData())
	{
		initializer.FastAddFrameData(*m_IdentityFrame->GetFrameData());
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentExtraDofs::Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const
{
	Assert(m_IdentityFrame);
	Assert(m_PoseFrame);
	frame.Set(*m_IdentityFrame, filter);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentExtraDofs::Reset(const crCreatureFilter& skip)
{
	Assert(m_PoseFrame);
	Assert(m_IdentityFrame);
	if(!skip.UseComposer())
	{
		m_PoseFrame->Set(*m_IdentityFrame);
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentExtraDofs::CalcSignature() const
{
	// TODO --- implement signature that uses pose frame too?
	u32 signature =  crCreatureComponent::CalcSignature();
	if(m_IdentityFrame)
	{
		u32 frameSig = m_IdentityFrame->GetSignature();
		signature = atDataHash(&frameSig, sizeof(u32), signature);
	}
	return signature;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
