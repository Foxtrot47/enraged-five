//
// creature/componentblendshapes.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTBLENDSHAPES_H
#define CREATURE_COMPONENTBLENDSHAPES_H

#include "component.h"

#include "atl/array.h"
#include "grblendshapes/blendshapes_config.h"
#include "grblendshapes/morphable.h"
#include "rmcore/drawable.h"
#include "paging/ref.h"

#if ENABLE_BLENDSHAPES

namespace rage
{

class grbTargetManager;

// PURPOSE: Creature blend shapes component
class crCreatureComponentBlendShapes : public crCreatureComponent
{
	friend class BlendShapePoseIterator;
	friend class BlendShapeInversePoseIterator;
	friend class BlendShapeIdentityIterator;

public:

	// PURPOSE: Default constructor
	crCreatureComponentBlendShapes();

	// PURPOSE: Initializing constructor
	// PARAMS: creature - reference to creature this component is part of
	// drawable - drawable instance containing the blend shapes
	// manager - blend shape target manager
	crCreatureComponentBlendShapes(crCreature& creature, rmcDrawable& drawable, grbTargetManager& manager);

	// PURPOSE: Resource constructor
	crCreatureComponentBlendShapes(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentBlendShapes();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentBlendShapes);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: creature - reference to creature this component is part of
	// drawable - drawable instance containing the blend shapes
	// manager - blend shape target manager
	void Init(crCreature& creature, rmcDrawable& drawable, grbTargetManager& manager);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Finalize override
	virtual void Finalize(float deltaTime);

	// PURPOSE: PreDraw override
	virtual void PreDraw(bool isVisible);

	// PURPOSE: DrawUpdate override
	virtual void DrawUpdate();

	// PURPOSE: SwapBuffers override
	virtual void SwapBuffers();

	// PURPOSE: Pose override
	virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

	// PURPOSE: Inverse Pose override
	virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

	// PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Identity override
	virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;
	
	// PURPOSE: Reset override
	virtual void Reset(const crCreatureFilter&);


	// PURPOSE: Get weight delta tolerance
	// RETURNS: weight delta tolerance
	// NOTES: This value determines how much a particular blend shape weight
	// must deviate from its previous values before it is considered a significant
	// enough change that the blend shape's influence needs to be recalculated.
	// Initial default value is defined by sm_DefaultWeightDeltaTolerance.
	float GetWeightDeltaTolerance() const;

	// PURPOSE: Set weight delta tolerance
	// PARAMS: tolerance - new weight delta tolerance
	void SetWeightDeltaTolerance(float tolerance);


	// PURPOSE: Get accumulation limit
	// RETURNS: accumulation limit (zero indicates no limit)
	// NOTES: In optimized mode, drift can build up in buffers, so they
	// are periodically recalculated from zero when the accumulation limit is reached.
	u32 GetAccumulationLimit() const;

	// PURPOSE: Set accumulation limit
	// PARAMS: limit - accumulation limit (zero indicates no limit)
	// NOTES: Setting an accumulation limit causes a call to ForceAccumulationReset to be
	// made after every [limit] calls to Finalize
	void SetAccumulationLimit(u32 limit);

	// PURPOSE: Force accumulation reset
	// NOTES: Forces a recalculation of all the blend shape buffers from zero (on their next
	// finalize calls).
	void ForceAccumulationReset();

	const grbTargetManager* GetTargetManager() const { return m_Manager; }
	grbTargetManager* GetTargetManager() { return m_Manager; }

#if HACK_GTA4 && __BANK
	static  bool  ms_bReset;

	static  u32	  ms_iLogPose;
	static  u32   ms_iLogFinalize;
#endif // HACK_GTA4 && __BANK

	struct BlendWeight
	{
		BlendWeight(u16 id=0, float weight=0.f);

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		u16 m_Id;
		float m_Weight;
		atArray<float> m_BufferWeights;

		bool operator==(const BlendWeight&) const;
		bool operator<(const BlendWeight&) const;
	};

	u32 GetBlendWeightCount() { return m_BlendWeights.GetCount(); }
	const BlendWeight& GetBlendWeight(u32 index) {return m_BlendWeights[index];}

	// PURPOSE: Derived class static initialization
	static void InitClassDerived();

protected:

	// PURPOSE: Re-calculate signature
	u32 CalcSignature() const;

	// PURPOSE: Finalize or update
	void FinalizeOrDrawUpdate();

	atArray<BlendWeight> m_BlendWeights;

	datRef<grbTargetManager> m_Manager;
	pgRef<rmcDrawable> m_Drawable;

	u32 m_NextBuffer;

	float m_WeightDeltaTolerance;

	u32 m_AccumulationLimit;
	u32 m_AccumulationCounter;
	u32 m_AccumulationReset;

	static const float sm_DefaultWeightDeltaTolerance;
	static const u32 sm_DefaultAccumulationLimit;


};

////////////////////////////////////////////////////////////////////////////////

inline float crCreatureComponentBlendShapes::GetWeightDeltaTolerance() const
{
	return m_WeightDeltaTolerance;
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentBlendShapes::SetWeightDeltaTolerance(float tolerance)
{
	m_WeightDeltaTolerance = tolerance;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crCreatureComponentBlendShapes::GetAccumulationLimit() const
{
	return m_AccumulationLimit;
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentBlendShapes::SetAccumulationLimit(u32 limit)
{
	m_AccumulationLimit = limit;
}

////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponentBlendShapes::BlendWeight::BlendWeight(u16 Id, float weight)
: m_Id(Id)
, m_Weight(weight)
{
	for(int i=0; i<m_BufferWeights.GetCount(); ++i)
	{
		m_BufferWeights[i] = 0.f;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline bool crCreatureComponentBlendShapes::BlendWeight::operator==(const BlendWeight &r) const
{
	return m_Id == r.m_Id;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crCreatureComponentBlendShapes::BlendWeight::operator<(const BlendWeight &r) const
{
	return m_Id < r.m_Id;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // ENABLE_BLENDSHAPES

#endif // CREATURE_COMPONENTBLENDSHAPES_H
