//
// creature/componentskeleton.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "componentskeleton.h"

#include "creature.h"

#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "vectormath/classes.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentSkeleton::crCreatureComponentSkeleton()
: crCreatureComponent(kCreatureComponentTypeSkeleton)
, m_Skeleton(NULL)
, m_SuppressReset(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentSkeleton::crCreatureComponentSkeleton(crCreature& creature, crSkeleton& skeleton)
: crCreatureComponent(kCreatureComponentTypeSkeleton)
, m_Skeleton(NULL)
, m_SuppressReset(false)
{
	Init(creature, skeleton);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentSkeleton::crCreatureComponentSkeleton(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentSkeleton::~crCreatureComponentSkeleton()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentSkeleton, crCreatureComponent::kCreatureComponentTypeSkeleton);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentSkeleton::DeclareStruct(datTypeStruct& s)
{
	crCreatureComponent::DeclareStruct(s);

	STRUCT_BEGIN(crCreatureComponentSkeleton);
	STRUCT_FIELD(m_Skeleton);
	STRUCT_FIELD(m_SuppressReset);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::Init(crCreature& creature, crSkeleton& skeleton)
{
	PreInit(creature);

	m_Skeleton = &skeleton;

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::Shutdown()
{
	m_Skeleton = NULL;

	crCreatureComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip)
{
	Assert(m_Skeleton);

	if(!skip.UseComposer())
	{
		frame.Pose(*m_Skeleton, false, filter);
	}

	const Mat34V* parentMtx = m_Skeleton->GetParentMtx();
	Mat34V* moverMtx = GetCreature().GetMoverMtx();
	Mat34V& rootMtx = m_Skeleton->GetLocalMtx(0);
	if(moverMtx)
	{
		// deal with non-parented movers
		if(parentMtx)
		{
			if(parentMtx != moverMtx)
			{
				// parent matrix, but different to mover
				Transform(rootMtx, *moverMtx, rootMtx);
				UnTransformOrtho(rootMtx, *parentMtx, rootMtx);
			}
		}
		else
		{
			// no parent matrix, apply mover to root
			Transform(rootMtx, *moverMtx, rootMtx);
		}
	}

	if(!skip.UseComposer())
	{
		m_Skeleton->Update();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
	Assert(m_Skeleton);

	if(!skip.UseComposer() && !m_SuppressReset)
	{
		m_Skeleton->InverseUpdate();
	}

	// TODO --- support non-parented movers here properly!

	if(!skip.UseComposer())
	{
		frame.InversePose(*m_Skeleton, false, filter);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	Assert(m_Skeleton);
	const crSkeletonData& skelData = m_Skeleton->GetSkeletonData();

	const int numBones = skelData.GetNumBones();

	for(int i=0; i<numBones; ++i)
	{
		const crBoneData* boneData = skelData.GetBoneData(i);
		if(boneData && boneData->HasDofs(crBoneData::TRANSLATION|crBoneData::ROTATION|crBoneData::SCALE))
		{
			u16 boneId;
			if(skelData.ConvertBoneIndexToId(u16(i), boneId))
			{
				if(boneData->HasDofs(crBoneData::TRANSLATION))
				{
					it.IterateDof(kTrackBoneTranslation, boneId, kFormatTypeVector3);
				}
				if(boneData->HasDofs(crBoneData::ROTATION))
				{
					it.IterateDof(kTrackBoneRotation, boneId, kFormatTypeQuaternion);
				}
				if(boneData->HasDofs(crBoneData::SCALE))
				{
					it.IterateDof(kTrackBoneScale, boneId, kFormatTypeVector3);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentSkeleton::HasDof(u8 track, u16 id) const
{
	if(track == kTrackBoneTranslation || track == kTrackBoneRotation || track == kTrackBoneScale)
	{
		Assert(m_Skeleton);
		const crSkeletonData& skelData = m_Skeleton->GetSkeletonData();

		int boneIdx;
		if(skelData.ConvertBoneIdToIndex(id, boneIdx))
		{
			const crBoneData* boneData = skelData.GetBoneData(boneIdx);
			switch(track)
			{
			case kTrackBoneTranslation:
				return boneData->HasDofs(crBoneData::TRANSLATION);
			case kTrackBoneRotation:
				return boneData->HasDofs(crBoneData::ROTATION);
			case kTrackBoneScale:
				return boneData->HasDofs(crBoneData::SCALE);
			}
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentSkeleton::GetNumDofs() const
{
	Assert(m_Skeleton);
	const crSkeletonData& skelData = m_Skeleton->GetSkeletonData();

	return u32(skelData.GetNumAnimatableDofs());
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentSkeleton::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	Assert(m_Skeleton);
	return crFrameDataInitializerBoneAndMover::InitializeSkeleton(initializer, m_Skeleton->GetSkeletonData());
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const
{
	Assert(m_Skeleton);
	frame.IdentityFromSkel(m_Skeleton->GetSkeletonData(), filter);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::Reset(const crCreatureFilter& skip)
{
	Assert(m_Skeleton);

	if(!skip.UseComposer() && !m_SuppressReset)
	{
		m_Skeleton->Reset();
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentSkeleton::CalcSignature() const
{
	u32 signature = crCreatureComponent::CalcSignature();
	if(m_Skeleton)
	{
		u32 skelSig = m_Skeleton->GetSkeletonData().GetSignatureNonChiral();
		signature = atDataHash(&skelSig, sizeof(u32), signature);
		signature ^= u32(m_SuppressReset);
	}
	return signature;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::SetSkeleton(crSkeleton& skeleton)
{
	m_Skeleton = &skeleton;
	GetCreature().m_Skeleton = &skeleton;
	// TODO --- what about creature skeleton ownership?

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentSkeleton::SetSuppressReset(bool suppressReset)
{
	m_SuppressReset = suppressReset;

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
