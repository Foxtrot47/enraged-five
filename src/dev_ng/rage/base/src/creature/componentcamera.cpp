//
// creature/componentcamera.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "componentcamera.h"

#include "creature.h"

#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "grcore/viewport.h"
#include "grprofile/drawcore.h"
#include "vectormath/legacyconvert.h"

namespace rage
{

static float DEFAULT_FIELD_OF_VIEW = 65.0f;
static float DEFAULT_NEAR_DEPTH_OF_FIELD = 0.5f;
static float DEFAULT_FAR_DEPTH_OF_FIELD = 15.0f;

static float DEFAULT_NEAR_CLIP = 0.05f;
static float DEFAULT_FAR_CLIP = 1000.0f;

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentCamera::crCreatureComponentCamera()
: crCreatureComponentRoot(kCreatureComponentTypeCamera)
{

}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentCamera::crCreatureComponentCamera(crCreature& creature, const Mat34V* pParentMtx)
: crCreatureComponentRoot(kCreatureComponentTypeCamera, creature, pParentMtx)
{
    Init( creature, pParentMtx );
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentCamera::crCreatureComponentCamera(datResource& rsc)
: crCreatureComponentRoot(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentCamera::~crCreatureComponentCamera()
{

}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentCamera, crCreatureComponent::kCreatureComponentTypeCamera);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentCamera::DeclareStruct(datTypeStruct& s)
{
    crCreatureComponentRoot::DeclareStruct(s);

    STRUCT_BEGIN(crCreatureComponentCamera);
    STRUCT_FIELD(m_fFieldOfView);
    STRUCT_FIELD(m_vDepthOfField);
    STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::Init(crCreature& creature, const Mat34V* pParentMtx)
{
    crCreatureComponentRoot::InitRoot(creature, pParentMtx);

    m_fFieldOfView = DEFAULT_FIELD_OF_VIEW;
    m_vDepthOfField.SetXf(DEFAULT_NEAR_DEPTH_OF_FIELD);
    m_vDepthOfField.SetYf(DEFAULT_FAR_DEPTH_OF_FIELD);

    m_fNearClip = DEFAULT_NEAR_CLIP;
    m_fFarClip = DEFAULT_FAR_CLIP;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip)
{
    crCreatureComponentRoot::Pose( frame, filter, skip );

    frame.GetFloat( kTrackCameraFieldOfView, 0, m_fFieldOfView );

    frame.GetVector3( kTrackCameraDepthOfField, 0, m_vDepthOfField );
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
    crCreatureComponentRoot::InversePose( frame, filter, skip );

    frame.SetFloat( kTrackCameraFieldOfView, 0, m_fFieldOfView );

    frame.SetVector3( kTrackCameraDepthOfField, 0, m_vDepthOfField );
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	it.IterateDof(kTrackCameraFieldOfView, 0, kFormatTypeFloat);
	it.IterateDof(kTrackCameraDepthOfField, 0, kFormatTypeVector3);
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentCamera::HasDof(u8 track, u16 id) const
{
	switch(track)
	{
	case kTrackCameraFieldOfView:
	case kTrackCameraDepthOfField:
		if(id == 0)
		{
			return true;
		}
		// break intentionally omitted
	default:
		return crCreatureComponentRoot::HasDof(track, id);
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentCamera::GetNumDofs() const
{
	return crCreatureComponentRoot::GetNumDofs() + 2;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentCamera::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	if(crCreatureComponentRoot::InitDofs(initializer))
	{
		initializer.FastAddDof(kTrackCameraFieldOfView, 0, kFormatTypeFloat);
		initializer.FastAddDof(kTrackCameraDepthOfField, 0, kFormatTypeVector3);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
    crCreatureComponentRoot::Identity( frame, filter, skip );

    frame.SetFloat( kTrackCameraFieldOfView, 0, DEFAULT_FIELD_OF_VIEW );

    frame.SetVector3( kTrackCameraDepthOfField, 0,
        Vec3V( DEFAULT_NEAR_DEPTH_OF_FIELD, DEFAULT_FAR_DEPTH_OF_FIELD, 0.0f ) );
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::Reset(const crCreatureFilter& skip)
{
    crCreatureComponentRoot::Reset(skip);

    m_fFieldOfView = DEFAULT_FIELD_OF_VIEW;
    m_vDepthOfField.SetXf(DEFAULT_NEAR_DEPTH_OF_FIELD);
    m_vDepthOfField.SetYf(DEFAULT_FAR_DEPTH_OF_FIELD);
}

////////////////////////////////////////////////////////////////////////////////

#if !__FINAL
void crCreatureComponentCamera::DebugDraw() const
{
#if __DEV
    // Get the viewport
    grcViewport viewport;
    ConfigureViewport( viewport );

    // Draw the viewport
    viewport.DebugDraw();

    // Draw the Near DOF
    Matrix34 cMtx( RCC_MATRIX34(viewport.GetCameraMtx()) );
    cMtx.d += cMtx.c * -GetNearDepthOfField();
    grcWorldIdentity();
    grcDrawSolidBox( Vector3( 2.0f, 2.0f, 0.01f ), cMtx, Color32( 1.0f, 0.5f, 0.0f, 0.3f) );

    // Draw the Far DOF
    cMtx.Set( RCC_MATRIX34(viewport.GetCameraMtx()) );
    cMtx.d += cMtx.c * -GetFarDepthOfField();
    grcWorldIdentity();
    grcDrawSolidBox( Vector3( 2.0f, 2.0f, 0.01f ), cMtx, Color32( 0.1f, 0.1f, 1.0f, 0.3f ) );

    // Draw the focal point
    cMtx.Set( RCC_MATRIX34(viewport.GetCameraMtx()) );
    cMtx.d += cMtx.c * -rage::Max( 0.0f, ((GetFarDepthOfField() - GetNearDepthOfField()) * 0.5f) + GetNearDepthOfField() );
    grcWorldIdentity();
    grcDrawAxis( 0.5f, cMtx );
#endif // __DEV
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::SetNearClip( float fDistance )
{
    if ( fDistance <= 0.0f )
    {
        m_fNearClip = DEFAULT_NEAR_CLIP;
    }
    else
    {
        m_fNearClip = fDistance;
    }
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentCamera::SetFarClip( float fDistance )
{
    if ( fDistance <= 0.0f )
    {
        m_fFarClip = DEFAULT_FAR_CLIP;
    }
    else
    {
        m_fFarClip = fDistance;
    }
}

////////////////////////////////////////////////////////////////////////////////

grcViewport& crCreatureComponentCamera::ConfigureViewport( grcViewport &viewport ) const
{
    viewport.Perspective( m_fFieldOfView, 0.0f, m_fNearClip, m_fFarClip );
    viewport.SetCameraMtx( m_RootMtx );
    viewport.SetWorldIdentity();

    return viewport;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
