//
// creature/componentblendshapes.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "componentblendshapes.h"

#include "creature.h"

#include "atl/map.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "cranimation/frameiterators.h"
#include "grblendshapes/manager.h"
#include "grblendshapes/target.h"
#include "math/simplemath.h"
#include "rmcore/drawable.h"
#include "zlib/zlib.h"

#if ENABLE_BLENDSHAPES

#if __PS3
#define ISUSINGEDGE Likely(isUsingEdge)
#else
#define ISUSINGEDGE Unlikely(isUsingEdge)
#endif // __PS3


namespace rage
{

#if HACK_GTA4 && __BANK
#include "bank/bank.h"
#include "bank\bkmgr.h"
	 bool crCreatureComponentBlendShapes::ms_bReset = true;
	 u32  crCreatureComponentBlendShapes::ms_iLogPose = 0;
	 u32  crCreatureComponentBlendShapes::ms_iLogFinalize = 0;
#endif // HACK_GTA4 && __BANK


////////////////////////////////////////////////////////////////////////////////

crCreatureComponentBlendShapes::crCreatureComponentBlendShapes()
: crCreatureComponent(kCreatureComponentTypeBlendShapes)
, m_NextBuffer(1)
, m_WeightDeltaTolerance(sm_DefaultWeightDeltaTolerance)
, m_AccumulationLimit(sm_DefaultAccumulationLimit)
, m_AccumulationCounter(0)
, m_AccumulationReset(0)
, m_Manager(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentBlendShapes::crCreatureComponentBlendShapes(crCreature& creature, rmcDrawable& drawable, grbTargetManager& manager)
: crCreatureComponent(kCreatureComponentTypeBlendShapes)
, m_NextBuffer(1)
, m_WeightDeltaTolerance(sm_DefaultWeightDeltaTolerance)
, m_AccumulationLimit(sm_DefaultAccumulationLimit)
, m_AccumulationCounter(0)
, m_AccumulationReset(0)
{
	Init(creature, drawable, manager);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentBlendShapes::crCreatureComponentBlendShapes(datResource& rsc)
: crCreatureComponent(rsc)
, m_BlendWeights(rsc)
, m_NextBuffer(1)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentBlendShapes::~crCreatureComponentBlendShapes()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentBlendShapes, crCreatureComponent::kCreatureComponentTypeBlendShapes);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentBlendShapes::DeclareStruct(datTypeStruct& s)
{
	crCreatureComponent::DeclareStruct(s);

	STRUCT_BEGIN(crCreatureComponentBlendShapes);
	STRUCT_FIELD(m_BlendWeights);
	STRUCT_FIELD(m_Drawable);
	STRUCT_FIELD(m_Manager);
	STRUCT_FIELD(m_NextBuffer);
	STRUCT_FIELD(m_AccumulationLimit);
	STRUCT_FIELD(m_AccumulationCounter);
	STRUCT_FIELD(m_AccumulationReset);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::Init(crCreature& creature, rmcDrawable& drawable, grbTargetManager& manager)
{
	PreInit(creature);

	m_Drawable = &drawable;
	m_Manager = &manager;
	m_Manager->AddRef();

	m_BlendWeights.Reserve(m_Manager->GetBlendTargetCount());

	const bool isUsingEdge = m_Manager->IsUsingEdge();
	
	atMap<u16, datOwner<grbTarget> >::ConstIterator iter = m_Manager->GetBlendTargetIterator();
	while(iter)
	{
		u16 blendId = iter.GetKey();

		BlendWeight* newBlendWeight = NULL;
		for(int i=0; i<m_BlendWeights.GetCount(); ++i)
		{
			if(m_BlendWeights[i].m_Id > blendId)
			{
				newBlendWeight = &m_BlendWeights.Insert(i);
				break;
			}
		}

		if(!newBlendWeight)
		{
			newBlendWeight = &m_BlendWeights.Append();
		}

		newBlendWeight->m_Id = blendId;

		if (!ISUSINGEDGE)
			newBlendWeight->m_BufferWeights.Resize(m_Manager->GetNumMorphableBuffers());

		++iter;
	}

	ForceAccumulationReset();

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::Shutdown()
{
	if(m_Manager)
	{
		m_Manager->ResetBlendShapeOffsets();
		m_Drawable->RemoveBlendShapeOffsets(*m_Manager);
		m_Manager->Release();
	}

	m_Drawable = NULL;
	m_Manager = NULL;
	m_BlendWeights.Reset();

	crCreatureComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::Finalize(float)
{
	if(m_Manager->GetNumMorphableBuffers() > 2)
	{
		FinalizeOrDrawUpdate();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::PreDraw(bool /*isVisible*/)
{
	m_Drawable->ApplyBlendShapeOffsets(*m_Manager);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::DrawUpdate()
{
	if(m_Manager->GetNumMorphableBuffers() == 2)
	{
		FinalizeOrDrawUpdate();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::SwapBuffers()
{
	m_Manager->SwapBuffers();
	++m_NextBuffer %= m_Manager->GetNumMorphableBuffers();
}

////////////////////////////////////////////////////////////////////////////////

class BlendShapeFrameFilter : public crFrameFilter
{
public:

	BlendShapeFrameFilter(crFrameFilter* filter=NULL)
		: crFrameFilter(kFrameFilterTypeBlendShape)
		, m_Filter(filter)
	{
		SetSignature(CalcSignature());
	}

	BlendShapeFrameFilter(datResource& rsc)
		: crFrameFilter(rsc)
	{
	}

	CR_DECLARE_FRAME_FILTER_TYPE(BlendShapeFrameFilter);

	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight)
	{
		if(track == kTrackBlendShape)
		{
			if(m_Filter)
			{
				return m_Filter->FilterDof(track, id, inoutWeight);
			}
			return true;
		}

		return false;
	}

	virtual u32 CalcSignature() const
	{
		u32 signature = m_Filter ? m_Filter->GetSignature() : 0;
		signature = crc32(signature, (const u8*)&sm_Hash, sizeof(u32));

		return signature;
	}

	static u32 sm_Hash;
	crFrameFilter* m_Filter;
};

u32 BlendShapeFrameFilter::sm_Hash = atHash_const_char("BlendShapeFrameFilter");

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(BlendShapeFrameFilter, kFrameFilterTypeBlendShape, crFrameFilter);

////////////////////////////////////////////////////////////////////////////////

class BlendShapePoseIterator : public crFrameIterator<BlendShapePoseIterator>
{
public:
	BlendShapePoseIterator(const crFrame& frame, atArray<crCreatureComponentBlendShapes::BlendWeight>& blendWeights)
		: crFrameIterator<BlendShapePoseIterator>(frame)
		, m_BlendWeights(&blendWeights)
		, m_Index(0)
	{
	}

	__forceinline void IterateDof(const crFrameData::Dof& dof, const crFrame::Dof& dest, float)
	{
		Assert(dof.m_Track == kTrackBlendShape);

		const u16 id = dof.m_Id;

		const int numBlendWeights = m_BlendWeights->GetCount();
		for(; m_Index<numBlendWeights; ++m_Index)
		{
			crCreatureComponentBlendShapes::BlendWeight& blendWeight = (*m_BlendWeights)[m_Index];
			if(blendWeight.m_Id == id)
			{
				blendWeight.m_Weight = dest.GetUnsafe<float>();
				break;
			}
			else if(blendWeight.m_Id > id)
			{
				break;
			}
		}
	}

	atArray<crCreatureComponentBlendShapes::BlendWeight>* m_BlendWeights;
	int m_Index;
};

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&)
{
#if HACK_GTA4 && __BANK
	if (ms_iLogPose)
	{
		Displayf("%d = Start of crCreatureComponentBlendShapes::Pose", ms_iLogPose);

		static bool bDumpFrame = false;
		if (bDumpFrame)
		{
			bDumpFrame = false;
			frame.Dump();
		}
	}
#endif // HACK_GTA4 && __BANK

#if HACK_GTA4 && __BANK
	for(int i=0; i<m_BlendWeights.GetCount(); ++i)
	{
		if (ms_iLogPose)
		{
			bool bHasDof = frame.HasDof(kTrackBlendShape, m_BlendWeights[i].m_Id);
			bool bHasDofValid = frame.HasDofValid(kTrackBlendShape, m_BlendWeights[i].m_Id);

			if (bHasDofValid)
			{
				float weight;
				if(frame.GetFloat(kTrackBlendShape, m_BlendWeights[i].m_Id, weight))
				{
					Displayf("m_BlendWeights[%d].m_Id = (%d), VALID, weight = %6.4f", i, m_BlendWeights[i].m_Id, weight);
				}
			}
			else
			{
				if (bHasDof)
				{
					Displayf("m_BlendWeights[%d].m_Id = (%d), INVALID", i, m_BlendWeights[i].m_Id);
				}
				else
				{
					Displayf("m_BlendWeights[%d].m_Id = (%d), MISSING", i, m_BlendWeights[i].m_Id);
				}							
			}
		}
	}
#endif // HACK_GTA4 && __BANK

	BlendShapeFrameFilter blendShapeFilter(filter);
	BlendShapePoseIterator it(frame, m_BlendWeights);
	it.Iterate(&blendShapeFilter, 1.f, true);

#if HACK_GTA4 && __BANK
	if (ms_iLogPose)
	{
		Displayf("%d = End of crCreatureComponentBlendShapes::Pose", ms_iLogPose);
		ms_iLogPose--;
	}
#endif // HACK_GTA4 && __BANK
}

////////////////////////////////////////////////////////////////////////////////

class BlendShapeInversePoseIterator : public crFrameIterator<BlendShapeInversePoseIterator>
{
public:
	BlendShapeInversePoseIterator(crFrame& frame, const atArray<crCreatureComponentBlendShapes::BlendWeight>& blendWeights)
		: crFrameIterator<BlendShapeInversePoseIterator>(frame)
		, m_BlendWeights(&blendWeights)
		, m_Index(0)
	{
	}

	__forceinline void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		Assert(dof.m_Track == kTrackBlendShape);

		const u16 id = dof.m_Id;

		const int numBlendWeights = m_BlendWeights->GetCount();
		for(; m_Index<numBlendWeights; ++m_Index)
		{
			const crCreatureComponentBlendShapes::BlendWeight& blendWeight = (*m_BlendWeights)[m_Index];
			if(blendWeight.m_Id == id)
			{
				dest.Set<float>(blendWeight.m_Weight);
				break;
			}
			else if(blendWeight.m_Id > id)
			{
				break;
			}
		}
	}

	const atArray<crCreatureComponentBlendShapes::BlendWeight>* m_BlendWeights;
	int m_Index;
};

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const
{
	BlendShapeFrameFilter blendShapeFilter(filter);
	BlendShapeInversePoseIterator it(frame, m_BlendWeights);
	it.Iterate(&blendShapeFilter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	const int numBlendWeights = m_BlendWeights.GetCount();
	for(int i=0; i<numBlendWeights; ++i)
	{
		it.IterateDof(kTrackBlendShape, m_BlendWeights[i].m_Id, kFormatTypeFloat);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentBlendShapes::HasDof(u8 track, u16 id) const
{
	if(track == kTrackBlendShape)
	{
		return m_BlendWeights.BinarySearch(BlendWeight(id))>=0;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentBlendShapes::GetNumDofs() const
{
	return u32(m_BlendWeights.GetCount());
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentBlendShapes::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	for(int i=0; i<m_BlendWeights.GetCount(); ++i)
	{
		initializer.FastAddDof(kTrackBlendShape, m_BlendWeights[i].m_Id, kFormatTypeFloat);
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

class BlendShapeIdentityIterator : public crFrameIterator<BlendShapeIdentityIterator>
{
public:
	BlendShapeIdentityIterator(crFrame& frame, const atArray<crCreatureComponentBlendShapes::BlendWeight>& blendWeights)
		: crFrameIterator<BlendShapeIdentityIterator>(frame)
		, m_BlendWeights(&blendWeights)
		, m_Index(0)
	{
	}

	__forceinline void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		Assert(dof.m_Track == kTrackBlendShape);

		const u16 id = dof.m_Id;

		const int numBlendWeights = m_BlendWeights->GetCount();
		for(; m_Index<numBlendWeights; ++m_Index)
		{
			const crCreatureComponentBlendShapes::BlendWeight& blendWeight = (*m_BlendWeights)[m_Index];
			if(blendWeight.m_Id == id)
			{
				dest.Set<float>(0.f);
				break;
			}
			else if(blendWeight.m_Id > id)
			{
				break;
			}
		}
	}

	const atArray<crCreatureComponentBlendShapes::BlendWeight>* m_BlendWeights;
	int m_Index;
};

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const
{
	BlendShapeFrameFilter blendShapeFilter(filter);
	BlendShapeIdentityIterator it(frame, m_BlendWeights);
	it.Iterate(&blendShapeFilter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::Reset(const crCreatureFilter&)
{
#if HACK_GTA4 && __BANK
	if (ms_bReset)
	{
		for(int i=0; i<m_BlendWeights.GetCount(); ++i)
		{
			m_BlendWeights[i].m_Weight = 0.f;
		}
	}
#else //HACK_GTA4 && __BANK

	for(int i=0; i<m_BlendWeights.GetCount(); ++i)
	{
		m_BlendWeights[i].m_Weight = 0.f;
	}
#endif //HACK_GTA4 && __BANK
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::InitClassDerived()
{
	crCreatureComponentBlendShapes::InitClass();

	BlendShapeFrameFilter::InitClass();
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentBlendShapes::CalcSignature() const
{
	u32 signature = crCreatureComponent::CalcSignature();
	const int numBlendWeights = m_BlendWeights.GetCount();
	for(int i=0; i<numBlendWeights; ++i)
	{
		signature = crc32(signature, (const u8*)&m_BlendWeights[i].m_Id, sizeof(u16));
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::FinalizeOrDrawUpdate()
{
	Assert(m_Drawable);
	Assert(m_Manager);

	// OPTIMIZE - ideas for further optimization:
	// - sort deltas by size, only consider the top N per update
	// - instead of N, consider number of verts changed.  Only allow up to M verts per frame
	// - change to parallel ordered walk, removes binary search

#if !HACK_GTA4 // lets make life easier

	const bool isUsingEdge = m_Manager->IsUsingEdge();
	const int numBlendWeights = m_BlendWeights.GetCount();

	if (ISUSINGEDGE)
	{
#if HACK_GTA4 && __BANK
		if (ms_iLogFinalize)
		{
			Displayf("%d = Start of crCreatureComponentBlendShapes::FinalizeOrDrawUpdate", ms_iLogFinalize);
		}
#endif // HACK_GTA4 && __BANK

		for(int idx=0; idx<numBlendWeights; ++idx)
		{
			BlendWeight& blendWeight = m_BlendWeights[idx];
			float& currentWeight = blendWeight.m_Weight;

			m_Manager->BlendTarget(blendWeight.m_Id, currentWeight);
		}

#if HACK_GTA4 && __BANK
		if (ms_iLogFinalize)
		{
			Displayf("%d = End of crCreatureComponentBlendShapes::FinalizeOrDrawUpdate", ms_iLogFinalize);
			ms_iLogFinalize--;
		}
#endif // HACK_GTA4 && __BANK
	}
	else
	{
		if(m_AccumulationLimit && ((++m_AccumulationCounter) >= m_AccumulationLimit))
		{
			ForceAccumulationReset();
			m_AccumulationCounter = 0;
		}

		m_Manager->PrepareBlendShapeOffsets();

		if(m_AccumulationReset & (1<<m_NextBuffer))
		{
			// force a reset of this blend shape buffer
			m_Manager->ResetBlendShapeOffsets();

			// and reset the weights
			const int numWeights = m_BlendWeights.GetCount();
			for(int i=0; i<numWeights; ++i)
			{
				m_BlendWeights[i].m_BufferWeights[m_NextBuffer] = 0.f;
			}

			// clear the reset bit
			m_AccumulationReset &= ~(1<<m_NextBuffer);
		}

		for(int idx=0; idx<numBlendWeights; ++idx)
		{
			BlendWeight& blendWeight = m_BlendWeights[idx];
			float& currentWeight = blendWeight.m_Weight;
			float& bufferWeight = blendWeight.m_BufferWeights[m_NextBuffer];

			if(!IsClose(currentWeight, bufferWeight, m_WeightDeltaTolerance))
			{	
				m_Manager->BlendTarget(blendWeight.m_Id, currentWeight - bufferWeight);
				bufferWeight = currentWeight;
			}
		}
		m_Manager->ReleaseBlendShapeOffsets();
	}
#endif // !HACK_GTA4
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentBlendShapes::ForceAccumulationReset()
{
	m_AccumulationReset = (1<<m_Manager->GetNumMorphableBuffers())-1;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentBlendShapes::BlendWeight::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crCreatureComponentBlendShapes::BlendWeight);
	STRUCT_FIELD(m_Id);
	STRUCT_FIELD(m_Weight);
	STRUCT_FIELD(m_BufferWeights);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

const float crCreatureComponentBlendShapes::sm_DefaultWeightDeltaTolerance = 0.01f;

////////////////////////////////////////////////////////////////////////////////

const u32 crCreatureComponentBlendShapes::sm_DefaultAccumulationLimit = 100;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // ENABLE_BLENDSHAPES


