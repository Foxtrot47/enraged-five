//
// creature/componentphysical.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTPHYSICAL_H
#define CREATURE_COMPONENTPHYSICAL_H

#include "component.h"

#include "vectormath/transformv.h"
#include "vectormath/vec4v.h"

namespace rage
{

class crFrameData;

// PURPOSE: Initial configuration of a secondary motion
struct crMotionDescription
{
	// PURPOSE: Constructor
	crMotionDescription() {}

	// PURPOSE: Resource constructor
	crMotionDescription(datResource&) {}

	// PURPOSE: Placement
	IMPLEMENT_PLACE_INLINE(crMotionDescription);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PURPOSE: Get bone id, hidden inside the gravity w component
	u16 GetBoneId() const { return u16(m_Gravity.GetWi()); }

	struct Dof3
	{
		Vec3V m_Strength;     // Spring constant, higher values will make the spring more stiff (N/m)
		Vec3V m_Damping;	  // Damping Coefficient, measure of an object's resistance (kg/m2)
		Vec3V m_MinConstraint, m_MaxConstraint;   // Constraints on the motion displacement (meter or radians)
	};
	
	Dof3 m_Linear, m_Angular;
	Vec3V m_Direction;
	Vec4V m_Gravity;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base class for a Creature for physical expressions.
class crCreatureComponentPhysical : public crCreatureComponent
{
public:
	// PURPOSE: Default constructor
	crCreatureComponentPhysical();

	// PURPOSE: Resource constructor
	crCreatureComponentPhysical(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentPhysical();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentPhysical);

	// PURPOSE: Initialize pool
	static void InitPool(u32 elemSize, u32 numElem);

	// PURPOSE: Shutdown pool
	static void ShutdownPool();

	// PURPOSE: Initialization
	// PARAMS: creature - reference to creature this component is part of
	void Init(crCreature& creature);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Add a new bone to the simulation
	void AddMotions(const atArray<crMotionDescription>& motions);

	/// PURPOSE: Search for an existing motion
	bool HasMotion(u16 boneId) const;

	// PURPOSE: Simulated secondary motion in world space
	struct Motion
	{
		// Non-constants members
		TransformV m_PrevWorld, m_CurrWorld;
		TransformV m_PrevOffset, m_CurrOffset;

		// Constants members
		TransformV m_OffsetLocal;
		crMotionDescription m_Descr;
		u16 m_ParentIdx, m_ChildIdx;
		bool m_Reset;
	};

	// PURPOSE: Get internal motions
	Motion* GetMotions();

	// PURPOSE: Get number of motions
	u32 GetNumMotions() const;

	// PURPOSE: Reset existing motions (useful when skeleton data changes etc)
	void ResetMotions();

	// PURPOSE: Remove all existing motions
	void RemoveMotions();

private:

	static atPoolBase* sm_Pool;

	Motion* m_Motions;
	u16 m_Count;
	u16 m_Reserve;
};

////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponentPhysical::Motion* crCreatureComponentPhysical::GetMotions()
{
	return m_Motions;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crCreatureComponentPhysical::GetNumMotions() const
{
	return m_Count;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREATURE_COMPONENTPHYSICAL_H
