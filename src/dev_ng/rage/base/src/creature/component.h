//
// creature/component.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_CREATURECOMPONENT_H
#define CREATURE_CREATURECOMPONENT_H

#include "atl/array.h"
#include "atl/pool.h"
#include "cranimation/animation_config.h"
#include "data/resource.h"
#include "data/struct.h"

namespace rage
{

class crFrame;
class crCreature;
class crCreatureFilter;
class crFrameFilter;
class crFrameDataInitializerCreature;


////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Abstract base class for creature components
// A creature consists of a collection of components.
class crCreatureComponent
{
public:

	// PURPOSE: Enumeration of different built in derived creature component types
	enum eCreatureComponentType
	{
		kCreatureComponentTypeNone,

		kCreatureComponentTypeSkeleton,
		kCreatureComponentTypeExtraDofs,
		kCreatureComponentTypeShaderVars,
		kCreatureComponentTypePhysical,

		kCreatureComponentTypeMover,
		kCreatureComponentTypeBlendShapes,
		kCreatureComponentTypeTelemetry,
        kCreatureComponentTypeCamera,
        kCreatureComponentTypeLight,
        kCreatureComponentTypeParticleEffect,
		kCreatureComponentTypeEvent,
		kCreatureComponentTypeMove,
		// RESERVED - for more Rage specific creature component types only

		// TODO - fragment, motion tree, drawable(s)?

		// this must follow the list of built in creature component types
		kCreatureComponentTypeNum,

		// custom creature component types *must* have a unique id >= kCreatureComponentTypeCustom
		kCreatureComponentTypeCustom = 0x80,
	};

	// PURPOSE: Constructor
	crCreatureComponent(eCreatureComponentType creatureComponentType=kCreatureComponentTypeNone);

	// PURPOSE: Resource constructor
	crCreatureComponent(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponent();

	// PURPOSE: Off line resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PRUPOSE: Placement (non-resource)
	static void Place(crCreatureComponent* that, eCreatureComponentType creatureComponentType);

	// PURPOSE: Placement
	DECLARE_PLACE(crCreatureComponent);

	// PURPOSE: Fixes up derived type from pointer to base class only
	static void VirtualConstructFromPtr(datResource&, crCreatureComponent* base);

	// PURPOSE: Pre initialization
	// PARAMS: creature - reference to creature this component is part of
	virtual void PreInit(crCreature& creature);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Creature component factory
	// PARAMS: creature component type identifier
	// (see crCreatureComponent:eCreatureComponentType enumeration for built in creature component types)
	// RETURNS: pointer to newly created creature component, or NULL if creature component type unknown
	static crCreatureComponent* Allocate(eCreatureComponentType creatureComponentType);


	// PURPOSE: Finalize the component
	// PARAMS: deltaTime - time (in seconds) since last update
	virtual void Finalize(float deltaTime);

	// PURPOSE: Refresh component with new frame data
	virtual void Refresh();

	// PURPOSE: Pose the component.  Transfer the degrees-of-freedom values
	// in the frame to the component.
	// PARAMS: frame - animation frame containing the dofs
	// filter - optional filter to control which dofs are used (default NULL)	
	virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip);

	// PURPOSE: Reverse of posing a component.  Use the component's current
	// pose to set the frame's degrees-of-freedom values.
	// PARAMS: frame - animation frame to be set.
	// filter - optional filter to control which dofs are set (default NULL)
	virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const;

	// PURPOSE: DOF iterator, derive from to iterate DOFs on a component
	class DofIterator
	{
	public:

		// PURPOSE: Constructor
		DofIterator();

		// PRUPOSE: Destructor
		virtual ~DofIterator();

		// PURPOSE: Override to iterate individual DOFs
		virtual void IterateDof(u8 track, u16 id, u8 type) = 0;

		// PURPOSE: Override to iterate frame full of DOFs
		// NOTES: Default implementation will decompose this into series
		// of IterateDof calls, but it may be more efficient to override
		// and perform custom operation in derived iterator.
		virtual void IterateFrame(const crFrame&);

	};

	// PRUPOSE: Iterate DOFs within a component
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Get number of DOFs within a component
	virtual u32 GetNumDofs() const;

	// PURPOSE: Set up the degrees-of-freedom found in the component in the frame.
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;
	
	// PURPOSE: Set the values in the frame's degrees-of-freedom to identity.
	// PARAMS: frame - animation frame to be set.
	// filter - optional filter to control which dofs are set
	virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const;

	// PURPOSE: Reset the degrees-of-freedom values in creature to identity.
	virtual void Reset(const crCreatureFilter& skip);

	// PURPOSE: Post update, called from main update thread post finalize
	virtual void PostUpdate();

	// PURPOSE: Pre-draw.  Call prior to drawing creature.
	virtual void PreDraw(bool isVisible);

	// PURPOSE: Called once per frame from the draw thread prior to drawing creature and calling PreDraw()
	virtual void DrawUpdate();

	// PURPOSE: Swaps any multithreaded buffers.  Call in a thread safe sync point.
	virtual void SwapBuffers();

	// PURPOSE: Draw.  Call to draw creature.
	virtual void Draw() const;

#if CR_DEV
	// PURPOSE: Debug draw. Call to debug draw the creature
	// NOTES: For development only
	virtual void DebugDraw() const;

	// PURPOSE: Get creature component type name.
	// RETURNS: const pointer to property's type name (ie string version of class name)
	const char* GetTypeName() const;
#endif // CR_DEV

	// PURPOSE: Get the signature of the component (a hash of it's structure).
	u32 GetSignature() const;

	// PURPOSE: Default component priority (higher number == preferential evaluation)
	static const u32 sm_DefaultComponentPriority = 0x80000000;

	// PURPOSE: Get component priority, relative to other components
	// RETURNS: component priority (default implementation returns sm_DefaultComponentPriority)
	// NOTES: Override to force component to evaluate earlier or later by the creature
	// (higher priority value == earlier evaluation).
	virtual u32 GetPriority() const;

	// PURPOSE: Get creature component type.
	// RETURNS: Creature component type
	// (see crCreatureComponent::eCreatureComponentType enumeration for built in creature component types)
	eCreatureComponentType GetType() const;

	// PURPOSE: Get creature this component is part of.
	// RETURNS: reference to creature
	// NOTES: Will assert if called before component is pre-initialized, or post shutdown
	crCreature& GetCreature() const;

	// PURPOSE: Init class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();

	// PURPOSE: Definition of creature component allocation call
	typedef crCreatureComponent* CreatureComponentAllocateFn();

	// PURPOSE: Definition of creature component placement call
	typedef void CreatureComponentPlaceFn(crCreatureComponent*);

	// PURPOSE: Definition of creature component resource placement call
	typedef void CreatureComponentRscPlaceFn(datResource&, crCreatureComponent*);

	// PURPOSE: Creature component type information
	struct CreatureComponentTypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: creatureComponentType - creature component type identifier
		// (see crCreatureComponent::eCreatureComponentType enumeration for built in creature component types)
		// creatureComponentName - creature component name string
		// size - size of component (in bytes)
		// allocateFn - creature component allocation call
		// placeFn - creature component placement call
		// rscPlaceFn - creature component resource placement call
		CreatureComponentTypeInfo(eCreatureComponentType creatureComponentType, const char* creatureComponentName, size_t size, CreatureComponentAllocateFn* allocateFn, CreatureComponentPlaceFn* placeFn, CreatureComponentRscPlaceFn* rscPlaceFn);

		// PURPOSE: Destructor
		~CreatureComponentTypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get creature component type identifier
		// RETURNS: creature component type identifier
		// (see crCreatureCompnent::eCreatureComponentType enumeration for built in creature component types)
		eCreatureComponentType GetCreatureComponentType() const;

		// PURPOSE: Get creature component name string
		// RETURNS: string of creature component's name
		const char* GetCreatureComponentName() const;
		
		// PURPOSE: Get creature component size
		// RETURNS: creature component size (in bytes)
		size_t GetSize() const;

		// PURPOSE: Get creature component allocation function
		// RETURNS: creature component allocation function pointer
		CreatureComponentAllocateFn* GetAllocateFn() const;

		// PURPOSE: Get creature component placement function
		// RETURNS: creature component placement function pointer
		CreatureComponentPlaceFn* GetPlaceFn() const;

		// PURPOSE: Get creature component resource placement function
		// RETURNS: creature component resource placement function pointer
		CreatureComponentRscPlaceFn* GetRscPlaceFn() const;

	private:
		eCreatureComponentType m_CreatureComponentType;
		const char* m_CreatureComponentName;
		size_t m_Size;
		CreatureComponentAllocateFn* m_AllocateFn;
		CreatureComponentPlaceFn* m_PlaceFn;		
		CreatureComponentRscPlaceFn* m_RscPlaceFn;		
	};

	// PURPOSE: Get info about a creature component type
	// PARAMS: creatureComponentType - creature component type identifier
	// (see crCreatureComponent::eCreatureComponentType enumeration for built in creature component types)
	// RETURNS: const pointer to creature component type info structure (may be NULL if creature component type unknown)
	static const CreatureComponentTypeInfo* FindCreatureComponentTypeInfo(eCreatureComponentType creatureComponentType);

	// PURPOSE: Get info about this creature component
	// RETURNS: const reference to creature component type info structure about this creature component
	virtual const CreatureComponentTypeInfo& GetCreatureComponentTypeInfo() const = 0;

	// PURPOSE: Declare functions necessary to register a new creature component type
	#define CR_DECLARE_CREATURE_COMPONENT_TYPE(__typename) \
		static crCreatureComponent* AllocateCreatureComponent(); \
		static void PlaceCreatureComponent(crCreatureComponent* base); \
		static void RscPlaceCreatureComponent(datResource& rsc, crCreatureComponent* base); \
		virtual crCreatureComponent* Clone() const; \
		static void InitClass(); \
		virtual const CreatureComponentTypeInfo& GetCreatureComponentTypeInfo() const; \
		static const crCreatureComponent::CreatureComponentTypeInfo sm_CreatureComponentTypeInfo;

	// PURPOSE: Implement functions necessary to register a new creature component type
	#define CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(__typename, __typeid) \
		crCreatureComponent* __typename::AllocateCreatureComponent() \
		{ \
			__typename* prop = rage_new __typename; \
			return prop; \
		} \
		void __typename::PlaceCreatureComponent(crCreatureComponent* base) \
		{ \
			::new (base) __typename(); \
		} \
		void __typename::RscPlaceCreatureComponent(datResource& rsc, crCreatureComponent* base) \
		{ \
		::new (base) __typename(rsc); \
		} \
		crCreatureComponent* __typename::Clone() const \
		{ \
			return rage_new __typename(*this); \
		} \
		void __typename::InitClass() \
		{ \
			sm_CreatureComponentTypeInfo.Register(); \
		} \
		const crCreatureComponent::CreatureComponentTypeInfo& __typename::GetCreatureComponentTypeInfo() const \
		{ \
			return sm_CreatureComponentTypeInfo; \
		} \
		const crCreatureComponent::CreatureComponentTypeInfo __typename::sm_CreatureComponentTypeInfo(__typeid, CR_DEV ? #__typename :  NULL, sizeof(__typename), AllocateCreatureComponent, PlaceCreatureComponent, RscPlaceCreatureComponent);


		// PURPOSE: Creature component factory (base)
		class Factory
		{
		public:
			
			// PURPOSE: Default constructor
			Factory();

			// PURPOSE: Destructor
			virtual ~Factory();

			// PURPOSE: Allocate a new creature component
			virtual crCreatureComponent* Allocate(eCreatureComponentType creatureComponentType);

			// PURPOSE: Free a creature component
			virtual void Free(crCreatureComponent&);
		};


		// PURPOSE: Creature component factory with pools
		class FactoryPooled : public Factory
		{
		public:

			// PURPOSE: Default constructor
			FactoryPooled();

			// PURPOSE: Destructor
			virtual ~FactoryPooled();

			// PURPOSE: Allocate a new creature component
			virtual crCreatureComponent* Allocate(eCreatureComponentType creatureComponentType);

			// PURPOSE: Free a creature component
			virtual void Free(crCreatureComponent&);

			// PURPOSE: Create a component pool
			void CreatePool(eCreatureComponentType creatureComponentType, u16 size);

		private:
			atArray< atPoolBase* > m_Pools;
		};


protected:

	// PURPOSE: Register a new creature component type (only call from CreatureComponentTypeInfo::Register)
	// PARAMS: creature component type info (must be global/class static, persist for entire execution)
	static void RegisterCreatureComponentTypeInfo(const CreatureComponentTypeInfo& info);

	// PURPOSE: Calculate and set the signature of the component
	// NOTES: Also triggers a CalcAndSet on the parent creature
	void CalcAndSetSignature();

	// PURPOSE: Calculate the signature of the component (a hash of it's structure).
	virtual u32 CalcSignature() const;


protected:
	eCreatureComponentType m_Type;
	datRef<crCreature> m_Creature;
	datRef<Factory> m_Factory;
	u32 m_Signature;

	static atArray<const CreatureComponentTypeInfo*> sm_CreatureComponentTypeInfos;

	static bool sm_InitClassCalled;

	friend class crCreature;
};


////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponent::eCreatureComponentType crCreatureComponent::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline crCreature& crCreatureComponent::GetCreature() const
{
	FastAssert(m_Creature);
	return *m_Creature;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crCreatureComponent::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Filter to skip parts of the creature component update.
class crCreatureFilter
{
public:

	// PURPOSE: Constructor
	crCreatureFilter(bool useComposer=false);

	// PURPOSE: Do we use the composer
	bool UseComposer() const;

	// TODO: Add other filters

private:
	bool m_UseComposer;
};

////////////////////////////////////////////////////////////////////////////////

inline crCreatureFilter::crCreatureFilter(bool useComposer)
: m_UseComposer(useComposer)
{
}

////////////////////////////////////////////////////////////////////////////////

inline bool crCreatureFilter::UseComposer() const
{
	return m_UseComposer;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREATURE_CREATURECOMPONENT_H
