//
// creature/creature.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_CREATURE_H
#define CREATURE_CREATURE_H

#include "component.h"

#include "cranimation/frameinitializers.h"

#define FAST_FIND_COMPONENTS (1)

namespace rage
{

// pre-declarations
class crCreatureComponent;
class crFrameAccelerator;
class crFrameDataFactory;
class crFrameFilter;
class crSkeleton;

// PURPOSE: Represents an instance of a creature.
// The creature class is the highest level class that represents
// an instance of an individual creature (or character) in Rage.
// Internally, creatures contain a skeleton instance, and other
// optional modules (ie blend shapes, movers etc).
class crCreature
{
public:

	// PURPOSE: Default constructor
	crCreature();

	// PURPOSE: Destructor
	~crCreature();

    // PURPOSE: Initialization for a non-drawable like a light or camera.
    // PARAMS:
	// factory - to create the frame data
	// accelerator - to create accelerator indices
    // skeleton - external skeleton
    // moverMtx - optional external mover matrix (default NULL means creature creates own internal mover)
    void Init(crFrameDataFactory& factory, crFrameAccelerator& accelerator, crSkeleton* skeleton=NULL, Mat34V* moverMtx=NULL);

    // PURPOSE: Shutdown, free/release all resources
	void Shutdown();

	// PURPOSE: Refresh creature when the frame data changed
	void Refresh();

	// PURPOSE: Finalize the creature update.
	// Call after final posing of the creature, think of as a commit call.
	// PARAMS: deltaTime - time (in seconds) since last finalize call
	// NOTES: Call only once per update step!
	void Finalize(float deltaTime);

	// PURPOSE: Pose the creature.  Transfer the degrees-of-freedom values
	// in the frame to the creature.
	// PARAMS: frame - animation frame containing the dofs
	// filter - optional filter to control which dofs are used (default NULL)
	void Pose(const crFrame& frame, crFrameFilter* filter=NULL, const crCreatureFilter& skip = crCreatureFilter());

	// PURPOSE: Reverse of posing a creature.  Use the character's current
	// pose to set the frame's degrees-of-freedom values.
	// PARAMS: frame - animation frame to be set.
	// filter - optional filter to control which dofs are set (default NULL)
	void InversePose(crFrame& frame, crFrameFilter* filter=NULL, const crCreatureFilter& skip = crCreatureFilter()) const;

	// PURPOSE: Set up the degrees of freedom found in the creature in the frame data.
	// PARAMS: frameData - frame data to create degrees of freedom in.
	// filter - optional filter to control which degrees of freedom are created (default NULL)
	void InitDofs(crFrameData& frameData, crFrameFilter* filter=NULL) const;

	// PURPOSE: Check if creature has a particular DOF
	// PARAMS: track and id of DOF
	// RETURNS: true - found DOF, false - DOF not found
	bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Get total number of degrees-of-freedom in the creature
	// RETURNS: Total number of DOFs (or at least an upper bound, unique total may be less)
	u32 GetNumDofs() const;

	// PURPOSE: Set values in frame's degrees-of-freedom to be identity
	// PARAMS: frame - animation frame to be set to identity.
	// filter - optional filter to control which dofs are set (default NULL)
	void Identity(crFrame& frame, crFrameFilter* filter=NULL, const crCreatureFilter& skip = crCreatureFilter()) const;

	// PURPOSE: Reset degrees-of-freedom values contained in creature to identity.
	void Reset(const crCreatureFilter& skip = crCreatureFilter());

	// PURPOSE: Post update the creature
	// Call from main update thread after finalize is complete
	void PostUpdate();

	// PURPOSE: Pre-draw.  Call prior to drawing creature
	void PreDraw(bool isVisible);

	// PURPOSE: Called once per frame from the draw thread prior to drawing creature and calling PreDraw()
	void DrawUpdate();

	// PURPOSE: Swaps any multithreaded buffers.  Call in a thread safe sync point.
	void SwapBuffers();

	// PURPOSE: Draw.  Call to draw creature.
	void Draw() const;

#if !__FINAL
	// PURPOSE: Debug draw. Call to debug draw the creature
	// NOTES: For development only (!__FINAL)
	void DebugDraw() const;
#endif // !__FINAL

	// PURPOSE: Get creature signature, which is a hash of the creature's structure
	// RETURNS: creature signature (zero indicates no signature, or empty creature)
	u32 GetSignature() const;

	// PURPOSE: Get the skeleton instance (const)
	// RETURNS: skeleton instance const reference, may be NULL
	const crSkeleton* GetSkeleton() const;

	// PURPOSE: Get the skeleton instance (non-const)
	// RETURNS: skeleton instance non-const reference, may be NULL
	crSkeleton* GetSkeleton();

	// PURPOSE: Get the mover matrix (const)
	// RETURNS: const pointer to the mover matrix, may be NULL
	const Mat34V* GetMoverMtx() const;

	// PURPOSE: Get the mover matrix (non-const)
	// RETURNS: non-const pointer to the mover matrix, may be NULL
	Mat34V* GetMoverMtx();

	// PURPOSE: Get frame accelerator
	crFrameAccelerator& GetFrameAccelerator();

	// PURPOSE: Get current frame data
	const crFrameData& GetFrameData() const;

	// PURPOSE: Get the number of components
	// RETURN: Number of components
	u32 GetNumComponents() const;

	// PURPOSE: Enumeration, get component by index (const)
	// PARAMS: componentIdx - component index [0..(number of components - 1)]
	// RETURNS: const pointer to creature component
	const crCreatureComponent* GetComponentByIndex(u32 componentIdx) const;

	// PURPOSE: Enumeration, get component by index (non-const)
	// PARAMS: componentIdx - component index [0..(number of components - 1)]
	// RETURNS: non-const pointer to creature component
	crCreatureComponent* GetComponentByIndex(u32 componentIdx);

	// PURPOSE: Retrieve [first] component by component class type (non-const)
	// RETURNS: non-const pointer to creature component (or NULL if none found)
	template<typename _CreatureComponentType> _CreatureComponentType* FindComponent();

	// PURPOSE: Retrieve [first] component by component class type (const)
	// RETURNS: const pointer to creature component (or NULL if none found)
	template<typename _CreatureComponentType> const _CreatureComponentType* FindComponent() const;

	// PURPOSE: Retrieve [first] component by component type (const)
	// PARAMS: componentType - type of component to find
	// (see crCreatureComponent::eCreatureComponentType for enumeration of built in creature types)
	// RETURNS: const pointer to creature component (or NULL if none found)
	const crCreatureComponent* FindComponent(crCreatureComponent::eCreatureComponentType componentType) const;

	// PURPOSE: Retrieve [first] component by component type (non-const)
	// PARAMS: componentType - type of component to find
	// (see crCreatureComponent::eCreatureComponentType for enumeration of built in creature types)
	// RETURNS: non-const pointer to creature component (or NULL if none found)
	crCreatureComponent* FindComponent(crCreatureComponent::eCreatureComponentType componentType);

	// PURPOSE: Add new component to the creature
	// PARAMS: component - new component to add (creature will take responsibility for destruction)
	void AddComponent(crCreatureComponent& component);

	// PURPOSE: Add new component to the creature
	// PARAMS: componentType - type of new component to add
	// RETURNS: Pointer to creature component
	crCreatureComponent* AddComponentType(crCreatureComponent::eCreatureComponentType componentType);

	// PURPOSE: Remove a component from the creature by index
	// PARAMS: componentIdx - component index [0..(number of components - 1)]
	// RETURNS: true - if removal successful, false - if removal failed
	bool RemoveComponent(u32 componentIdx);

	// PURPOSE: Remove component(s) from the creature by type
	// PARAMS: componentType - type of component to remove
	// (see crCreatureComponent::eCreatureComponentType for enumeration of built in creature types)
	// RETURNS: true - if removal successful (one or more components removed), false - if removal failed
	bool RemoveComponentType(crCreatureComponent::eCreatureComponentType componentType);

	// PURPOSE: Remove all component(s) from the creature
	// RETURNS: true - if removal successful (one or more components removed), false - if removal failed
	bool RemoveAllComponents();
	

	// PURPOSE: Set component factory
	// PARAMS: factory - pointer to component factory used to allocate new components (can be NULL)
	void SetComponentFactory(crCreatureComponent::Factory* factory);

	// PURPOSE: Get component factory
	// RETURNS: Pointer to current component factory (can be NULL)
	crCreatureComponent::Factory* GetComponentFactory() const;

	// PURPOSE: Initialize the creature system
	static void InitClass();

	// PURPOSE: Shutdown the creature system
	static void ShutdownClass();

public:

	// PURPOSE: Create component (using factory if registered)
	crCreatureComponent* AllocateComponent(crCreatureComponent::eCreatureComponentType componentType) const;

	// PURPOSE: Free component (using factory if required)
	void FreeComponent(crCreatureComponent& component) const;

protected:

#if FAST_FIND_COMPONENTS
	// PURPOSE: Find component via search
	crCreatureComponent* FindComponentSlow(crCreatureComponent::eCreatureComponentType componentType);
	const crCreatureComponent* FindComponentSlow(crCreatureComponent::eCreatureComponentType componentType) const;
#endif // FAST_FIND_COMPONENTS

	// PURPOSE: Internal function, recalculates creature signature
	void CalcAndSetSignature();

private:

#if FAST_FIND_COMPONENTS
	atArray< crCreatureComponent* > m_FastFindComponents;
#endif // FAST_FIND_COMPONENTS

	atArray< crCreatureComponent* > m_Components;
	crSkeleton* m_Skeleton;
	Mat34V* m_MoverMtx;
	crFrameData* m_FrameData;
	crFrameDataFactory* m_FrameDataFactory;
	crFrameAccelerator* m_FrameAccelerator;
	crCreatureComponent::Factory* m_ComponentFactory;
	u32 m_Signature;

	static bool sm_InitClassCalled;

	template<typename T> friend class crCreatureIterator;
	friend class crCreatureComponent;
	friend class crCreatureComponentSkeleton;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creature degrees of freedom frame data initializer
// Populates frame data with degrees of freedom from a creature
class crFrameDataInitializerCreature : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// creature - creature to query for degrees of freedom
	// destroyExistingDofs - destroy any existing degrees of freedom contained in frame (default true)
	// filter - pointer to frame filter to use when adding new degrees of freedom (default null, for all)
	crFrameDataInitializerCreature(const crCreature& creature, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializerCreature();

protected:

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const;

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const;

private:

	const crCreature* m_Creature;
};

////////////////////////////////////////////////////////////////////////////////

inline const crSkeleton* crCreature::GetSkeleton() const
{
	return m_Skeleton;
}

////////////////////////////////////////////////////////////////////////////////

inline crSkeleton* crCreature::GetSkeleton()
{
	return m_Skeleton;
}

////////////////////////////////////////////////////////////////////////////////

inline const Mat34V* crCreature::GetMoverMtx() const
{
	return m_MoverMtx;
}

////////////////////////////////////////////////////////////////////////////////

inline Mat34V* crCreature::GetMoverMtx()
{
	return m_MoverMtx;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crCreature::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameAccelerator& crCreature::GetFrameAccelerator()
{
	return *m_FrameAccelerator;
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData& crCreature::GetFrameData() const
{
	return *m_FrameData;
}

////////////////////////////////////////////////////////////////////////////////

#if FAST_FIND_COMPONENTS
inline const crCreatureComponent* crCreature::FindComponent(crCreatureComponent::eCreatureComponentType componentType) const
{
	return m_FastFindComponents[int(componentType)];
}
#endif // FAST_FIND_COMPONENTS

////////////////////////////////////////////////////////////////////////////////

#if FAST_FIND_COMPONENTS
inline crCreatureComponent* crCreature::FindComponent(crCreatureComponent::eCreatureComponentType componentType)
{
	return m_FastFindComponents[int(componentType)];
}
#endif // FAST_FIND_COMPONENTS

////////////////////////////////////////////////////////////////////////////////

template<typename _CreatureComponentType>
inline _CreatureComponentType* crCreature::FindComponent()
{
#if FAST_FIND_COMPONENTS
	return static_cast<_CreatureComponentType*>(m_FastFindComponents[int(_CreatureComponentType::sm_CreatureComponentTypeInfo.GetCreatureComponentType())]);
#else // FAST_FIND_COMPONENTS
	return static_cast<_CreatureComponentType*>(FindComponent(_CreatureComponentType::sm_CreatureComponentTypeInfo.GetCreatureComponentType()));
#endif // FAST_FIND_COMPONENTS
}

////////////////////////////////////////////////////////////////////////////////

template<typename _CreatureComponentType>
inline const _CreatureComponentType* crCreature::FindComponent() const
{
#if FAST_FIND_COMPONENTS
	return static_cast<const _CreatureComponentType*>(m_FastFindComponents[int(_CreatureComponentType::sm_CreatureComponentTypeInfo.GetCreatureComponentType())]);
#else // FAST_FIND_COMPONENTS
	return static_cast<const _CreatureComponentType*>(FindComponent(_CreatureComponentType::sm_CreatureComponentTypeInfo.GetCreatureComponentType()));
#endif // FAST_FIND_COMPONENTS
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreature::SetComponentFactory(crCreatureComponent::Factory* factory)
{
	m_ComponentFactory = factory;
}

////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponent::Factory* crCreature::GetComponentFactory() const
{
	return m_ComponentFactory;
}

////////////////////////////////////////////////////////////////////////////////

}	// namespace rage

#endif // CREATURE_CREATURE_H
