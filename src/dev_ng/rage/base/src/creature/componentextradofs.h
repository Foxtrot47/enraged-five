//
// creature/componentextradofs.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTEXTRADOFS_H
#define CREATURE_COMPONENTEXTRADOFS_H

#include "component.h"

#include "cranimation/framebuffers.h"

namespace rage
{

class crFrame;
class crFrameData;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creature extra degrees-of-freedom component
class crCreatureComponentExtraDofs : public crCreatureComponent
{
public:

	// PURPOSE: Default constructor
	crCreatureComponentExtraDofs();

	// PURPOSE: Initializing constructor
	// PARAMS: creature - reference to creature this component is part of
	// frameData - provides description of degrees of freedom to be contained
	// identityFrame, poseFrame - optional externally constructed frames, based on frameData
	// NOTES: Increments reference on frameData, and if provided, on the identity and pose frames
	crCreatureComponentExtraDofs(crCreature& creature, const crFrameData& frameData, crCommonPool* pool=NULL);

	// PURPOSE: Resource constructor
	crCreatureComponentExtraDofs(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentExtraDofs();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentExtraDofs);

	// PURPOSE: Initialization
	// PARAMS: creature - reference to creature this component is part of
	// frameData - provides description of degrees of freedom to be contained
	// identityFrame, poseFrame - optional externally constructed frames, based on frameData
	// NOTES: Increments reference on frameData, and if provided, on the identity and pose frames
	void Init(crCreature& creature, const crFrameData& frameData, crCommonPool* pool=NULL);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Exchange frame data, allows changes in degrees of freedom this component stores
	// RETURNS: true - exchange successful, false - exchange failed, no changes made to frames/data
	// NOTES: Expensive operation, in potentially both dynamic memory and performance
	// Can fail, if externally constructed frames where provided, which are insufficient for new frame data
	bool ExchangeFrameData(const crFrameData& frameData);


	// PURPOSE: Pose override
	virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

	// PURPOSE: Inverse Pose override
	virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

	// PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

	// PURPOSE: Identity override
	virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Reset override
	virtual void Reset(const crCreatureFilter&);

	
	// PURPOSE: Get frame data
	const crFrameData& GetFrameData() const;

	// PURPOSE: Get identity frame (const)
	const crFrame& GetIdentityFrame() const;

	// PURPOSE: Get identity frame (non-const)
	crFrame& GetIdentityFrame();

	// PURPOSE: Get pose frame (const)
	const crFrame& GetPoseFrame() const;

	// PURPOSE: Get pose frame (non-const)
	crFrame& GetPoseFrame();

protected:

	// PURPOSE: Signature override
	virtual u32 CalcSignature() const;

protected:

	crFrameBufferFrameData m_FrameBuffer;

	crFrame* m_IdentityFrame;
	crFrame* m_PoseFrame;

	bool m_UseFrameBuffer;
};

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData& crCreatureComponentExtraDofs::GetFrameData() const
{
	return *m_FrameBuffer.GetFrameData();
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrame& crCreatureComponentExtraDofs::GetIdentityFrame() const
{
	Assert(m_IdentityFrame);
	return *m_IdentityFrame;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame& crCreatureComponentExtraDofs::GetIdentityFrame()
{
	Assert(m_IdentityFrame);
	return *m_IdentityFrame;
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrame& crCreatureComponentExtraDofs::GetPoseFrame() const
{
	Assert(m_PoseFrame);
	return *m_PoseFrame;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame& crCreatureComponentExtraDofs::GetPoseFrame()
{
	Assert(m_PoseFrame);
	return *m_PoseFrame;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREATURE_COMPONENTEXTRADOFS_H