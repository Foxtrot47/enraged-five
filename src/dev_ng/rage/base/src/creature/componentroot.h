//
// creature/componentroot.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTROOT_H
#define CREATURE_COMPONENTROOT_H

#include "component.h"

#include "vectormath/mat34v.h"

namespace rage
{
class crFrame;

// PURPOSE: Base class for a Creature with only a root animation (translation and rotation).
class crCreatureComponentRoot : public crCreatureComponent
{
protected:
    // PURPOSE: Constructor
    // PARAMS: creatureComponentType - the component type.  Cannot instantiate this class directly.
    crCreatureComponentRoot(eCreatureComponentType creatureComponentType);

    // PURPOSE: Initializing constructor
    // PARAMS: creature - reference to creature this component is part of
    // parentMtx - optional pointer to the matrix to use as a parent
    crCreatureComponentRoot(eCreatureComponentType creatureComponentType, crCreature& creature, const Mat34V* pParentMtx=NULL);

    // PURPOSE: Resource constructor
    crCreatureComponentRoot(datResource&);

    // PURPOSE: Destructor
    virtual ~crCreatureComponentRoot();

    // PURPOSE: Offline resourcing
#if __DECLARESTRUCT
public:
    virtual void DeclareStruct(datTypeStruct&);

protected:
#endif //__DECLARESTRUCT


    // PURPOSE: Initialization
    // PARAMS: creature - reference to creature this component is part of
    // pParentMtx - optional pointer to the matrix to use as a parent
    void InitRoot(crCreature& creature, const Mat34V* pParentMtx=NULL);

public:
    // PURPOSE: Pose override
    virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

    // PURPOSE: Inverse Pose override
    virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

    // PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

    // PURPOSE: Identity override
    virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

    // PURPOSE: Reset override
    virtual void Reset(const crCreatureFilter&);

    // PURPOSE: Sets the parent matrix
    // PARAMS:
    //    parentMtx - the parent matrix
    void SetParentMatrix( Mat34V_In parentMtx );

    Mat34V_ConstRef GetRootMatrix() const;

protected:
    Mat34V m_RootMtx;
    Mat34V m_ParentMtx;
};

inline Mat34V_ConstRef crCreatureComponentRoot::GetRootMatrix() const
{
    return m_RootMtx;
}

inline void crCreatureComponentRoot::SetParentMatrix( Mat34V_In parentMtx )
{
    m_ParentMtx = parentMtx;
}

} // namespace rage

#endif // CREATURE_COMPONENTROOT_H
