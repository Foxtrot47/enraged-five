//
// creature/componentcamera.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTCAMERA_H
#define CREATURE_COMPONENTCAMERA_H

#include "componentroot.h"

#include "vectormath/vec3v.h"

namespace rage
{
class crFrame;
class grcViewport;

// PURPOSE: Creature mover component
class crCreatureComponentCamera : public crCreatureComponentRoot
{
public:
    // PURPOSE: Constructor
    crCreatureComponentCamera();

    // PURPOSE: Initializing constructor
    // PARAMS: creature - reference to creature this component is part of
    // parentMtx - optional pointer to the matrix to use as a parent
    crCreatureComponentCamera(crCreature& creature, const Mat34V* pParentMtx=NULL);

    // PURPOSE: Resource constructor
    crCreatureComponentCamera(datResource&);

    // PURPOSE: Destructor
    virtual ~crCreatureComponentCamera();

    // PURPOSE: Declare type
    CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentCamera);

    // PURPOSE: Offline resourcing
#if __DECLARESTRUCT
    virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

    // PURPOSE: Initialization
    // PARAMS: creature - reference to creature this component is part of
    // pParentMtx - optional pointer to the matrix to use as a parent
    void Init(crCreature& creature, const Mat34V* pParentMtx=NULL);

    // PURPOSE: Pose override
    virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

    // PURPOSE: Inverse Pose override
    virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

    // PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

    // PURPOSE: Identity override
    virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

    // PURPOSE: Reset override
    virtual void Reset(const crCreatureFilter&);

#if !__FINAL
    // PURPOSE: Debug draw. Call to debug draw the creature
    // NOTES: For development only (!__FINAL)
    virtual void DebugDraw() const;
#endif // !__FINAL

    // PURPOSE: Retrieves the near clip distance.
    // RETURNS: The near clip distance
    // NOTES: NearClip is not contained in the animation data.  This function is merely used retrieve
    //   the value that was last passed into SetNearClip(...).
    float GetNearClip() const;

    // PURPOSE: Sets the near clip distance
    // PARAMS:
    //    fDistance - the near clip distance
    // NOTES: NearClip is not contained in the animation data.  This function is merely used to record
    //    what near clip value to use in ConfigureViewport(...).
    void SetNearClip( float fDistance );

    // PURPOSE: Retrieves the far clip distance.
    // RETURNS: The far clip distance
    // NOTES: FarClip is not contained in the animation data.  This function is merely used retrieve
    //   the value that was last passed into SetFarClip(...).
    float GetFarClip() const;

    // PURPOSE: Sets the far clip distance
    // PARAMS:
    //    fDistance - the far clip distance
    // NOTES: FarClip is not contained in the animation data.  This function is merely used to record
    //    what far clip value to use in ConfigureViewport(...).
    void SetFarClip( float fDistance );

    // PURPOSE: Retrieves the near depth of field distance.
    // RETURNS: The near depth of field distance
    float GetNearDepthOfField() const;

    // PURPOSE: Retrieves the far depth of field distance.
    // RETURNS: The far depth of field distance
    float GetFarDepthOfField() const;

    // PURPOSE: Configures the viewport object using the current DOFs and returns it.
    // PARAMS:
    //    viewport - the viewport to configure
    // RETURNS: the viewport that was configured
    grcViewport& ConfigureViewport( grcViewport &viewport ) const;

protected:
    Vec3V m_vDepthOfField;

	float m_fFieldOfView;
    float m_fNearClip;
    float m_fFarClip;
};

inline float crCreatureComponentCamera::GetNearClip() const
{
    return m_fNearClip;
}

inline float crCreatureComponentCamera::GetFarClip() const
{
    return m_fFarClip;
}

inline float crCreatureComponentCamera::GetNearDepthOfField() const
{
    return m_vDepthOfField.GetXf();
}

inline float crCreatureComponentCamera::GetFarDepthOfField() const
{
    return m_vDepthOfField.GetYf();
}

} // namespace rage

#endif // CREATURE_COMPONENTCAMERA_H
