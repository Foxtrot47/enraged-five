//
// creature/componentlight.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "componentlight.h"

#include "creature.h"

#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "grcore/debugdraw.h"
#include "grcore/light.h"
#include "grprofile/drawcore.h"
#include "math/amath.h"
#include "vectormath/legacyconvert.h"

namespace rage
{

static Vec3V DEFAULT_LIGHT_COLOR( V_ONE );
static float DEFAULT_LIGHT_INTENSITY = 1.0f;
static float DEFAULT_LIGHT_FALL_OFF = 0.5f;
static float DEFAULT_LIGHT_CONE_ANGLE = 0.0f;

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentLight::crCreatureComponentLight()
: crCreatureComponentRoot(kCreatureComponentTypeLight)
, m_lightType(grcLightGroup::LTTYPE_DIR)
{

}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentLight::crCreatureComponentLight(crCreature& creature, const Mat34V* pParentMtx)
: crCreatureComponentRoot(kCreatureComponentTypeLight, creature, pParentMtx)
, m_lightType(grcLightGroup::LTTYPE_DIR)
{
    Init( creature, pParentMtx );
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentLight::crCreatureComponentLight(datResource& rsc)
: crCreatureComponentRoot(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentLight::~crCreatureComponentLight()
{

}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentLight, crCreatureComponent::kCreatureComponentTypeLight);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentLight::DeclareStruct(datTypeStruct& s)
{
    crCreatureComponentRoot::DeclareStruct(s);

    STRUCT_BEGIN(crCreatureComponentLight);
    STRUCT_FIELD(m_lightType);
    STRUCT_FIELD(m_color);
    STRUCT_FIELD(m_intensity);
    STRUCT_FIELD(m_fallOff);
    STRUCT_FIELD(m_coneAngle);
    STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentLight::Init(crCreature& creature, const Mat34V* pParentMtx)
{
    crCreatureComponentRoot::InitRoot(creature, pParentMtx);

    m_lightType = grcLightGroup::LTTYPE_DIR;
    m_color = DEFAULT_LIGHT_COLOR;
    m_intensity = DEFAULT_LIGHT_INTENSITY;
    m_fallOff = DEFAULT_LIGHT_FALL_OFF;
    m_coneAngle = DEFAULT_LIGHT_CONE_ANGLE;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentLight::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip)
{
    crCreatureComponentRoot::Pose( frame, filter, skip );

    frame.GetVector3( kTrackColor, 0, m_color );
    frame.GetFloat( kTrackLightIntensity, 0, m_intensity );

    if ( frame.GetFloat( kTrackLightFallOff, 0, m_fallOff ) )
    {
        if ( frame.GetFloat( kTrackLightConeAngle, 0, m_coneAngle ) )
        {
            m_lightType = grcLightGroup::LTTYPE_SPOT;
        }
        else
        {
            m_lightType = grcLightGroup::LTTYPE_POINT;
        }
    }
    else
    {
        m_lightType = grcLightGroup::LTTYPE_DIR;
    }
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentLight::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
    crCreatureComponentRoot::InversePose( frame, filter, skip );

    frame.SetVector3( kTrackColor, 0, m_color );
    frame.SetFloat( kTrackLightIntensity, 0, m_intensity );

    if ( frame.SetFloat( kTrackLightFallOff, 0, m_fallOff ) )
    {
        if ( frame.SetFloat( kTrackLightConeAngle, 0, m_coneAngle ) )
        {
            m_lightType = grcLightGroup::LTTYPE_SPOT;
        }
        else
        {
            m_lightType = grcLightGroup::LTTYPE_POINT;
        }
    }
    else
    {
        m_lightType = grcLightGroup::LTTYPE_DIR;
    }
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentLight::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	it.IterateDof(kTrackColor, 0, kFormatTypeVector3);
	it.IterateDof(kTrackLightIntensity, 0, kFormatTypeFloat);
	it.IterateDof(kTrackLightFallOff, 0, kFormatTypeFloat);
	it.IterateDof(kTrackLightConeAngle, 0, kFormatTypeFloat);
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentLight::HasDof(u8 track, u16 id) const
{
	switch(track)
	{
	case kTrackColor:
	case kTrackLightIntensity:
	case kTrackLightFallOff:
	case kTrackLightConeAngle:
		if(id == 0)
		{
			return true;
		}
		// break intentionally omitted
	default:
		return crCreatureComponentRoot::HasDof(track, id);
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentLight::GetNumDofs() const
{
	return crCreatureComponentRoot::GetNumDofs() + 4;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentLight::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	if(crCreatureComponentRoot::InitDofs(initializer))
	{
		initializer.FastAddDof(kTrackColor, 0, kFormatTypeVector3);
		initializer.FastAddDof(kTrackLightIntensity, 0, kFormatTypeFloat);
		initializer.FastAddDof(kTrackLightFallOff, 0, kFormatTypeFloat);
		initializer.FastAddDof(kTrackLightConeAngle, 0, kFormatTypeFloat);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentLight::Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
    crCreatureComponentRoot::Identity( frame, filter, skip );

    frame.SetVector3( kTrackColor, 0, DEFAULT_LIGHT_COLOR );
    frame.SetFloat( kTrackLightIntensity, 0, DEFAULT_LIGHT_INTENSITY );

    if ( frame.SetFloat( kTrackLightFallOff, 0, DEFAULT_LIGHT_FALL_OFF ) )
    {
        if ( frame.SetFloat( kTrackLightConeAngle, 0, DEFAULT_LIGHT_CONE_ANGLE ) )
        {
            m_lightType = grcLightGroup::LTTYPE_SPOT;
        }
        else
        {
            m_lightType = grcLightGroup::LTTYPE_POINT;
        }
    }
    else
    {
        m_lightType = grcLightGroup::LTTYPE_DIR;
    }
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentLight::Reset(const crCreatureFilter& skip)
{
    crCreatureComponentRoot::Reset( skip );

    m_lightType = grcLightGroup::LTTYPE_DIR;
    m_color = DEFAULT_LIGHT_COLOR;
    m_intensity = DEFAULT_LIGHT_INTENSITY;
    m_fallOff = DEFAULT_LIGHT_FALL_OFF;
    m_coneAngle = DEFAULT_LIGHT_CONE_ANGLE;
}

////////////////////////////////////////////////////////////////////////////////

#if !__FINAL
void crCreatureComponentLight::DebugDraw() const
{
	const Matrix34& rootMtx = RCC_MATRIX34(m_RootMtx);

	bool bOldLighting = grcLighting(false);

    grcWorldIdentity();
    grcColor3f( GetColor() );
    grcDrawSphere( 0.1f, rootMtx.d, 8, true );

    // NOTE: grcDrawSphere sets the world matrix, does not set it back
    grcWorldIdentity();

    switch ( m_lightType )
    {
    case grcLightGroup::LTTYPE_DIR:
        {
            Vector3 tgt = rootMtx.d - rootMtx.b * 0.5f;
            grcDrawLine( rootMtx.d, tgt, GetColor() );
        }
        break;
    case grcLightGroup::LTTYPE_POINT:
        {
            // nothing more to draw
        }
        break;
    case grcLightGroup::LTTYPE_SPOT:
        {
            Vector3 tgt = rootMtx.d - rootMtx.b * 0.5f;

            Matrix34 m;
            m.LookAt( rootMtx.d, tgt, Vector3( 0.0f, 1.0f, 0.0f ) );

            // trigonometry: tan( angle ) = opp / adj -- > where adj = height and angle = ConeAngle, solve for opp
            float radius = 0.5f * Tanf( GetConeAngle() );

            // pythagorean theorem:  a^2 + b^2 = c^2 --> where a = b and c = radius, solve for a
            float length = radius * M_SQRT1_2;

            grcDrawFrustum( m, length, length, 0.5f, GetColor() );
        }
        break;
    default:
        break;
    }

	grcLighting(bOldLighting);
}

#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

grcLightGroup::grcLightSource& crCreatureComponentLight::ConfigureLight( grcLightGroup::grcLightSource &light ) const
{
	light.m_Color = GetColor();
    light.m_ConeAngle = GetConeAngle();
    light.m_Enabled = true;
    light.m_Falloff = GetFallOff();
    light.m_Intensity = GetIntensity();
    light.m_Type = (u8)GetLightType();

    switch ( light.m_Type )
    {
    case grcLightGroup::LTTYPE_DIR:
        {
            // Dir light is funky:  our dir is the grcLightGroup's pos
            light.m_Dir = -Normalize(m_RootMtx.GetCol1());

            light.m_Pos = light.m_Dir;
        }
        break;
    case grcLightGroup::LTTYPE_POINT:
        {
			light.m_Dir = Vec3V(V_Y_AXIS_WZERO);
            light.m_Pos = m_RootMtx.GetCol3();
        }
        break;
    case grcLightGroup::LTTYPE_SPOT:
        {
            light.m_Dir = -Normalize(m_RootMtx.GetCol1());

            light.m_Pos = m_RootMtx.GetCol3();
        }
        break;
    }

    return light;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
