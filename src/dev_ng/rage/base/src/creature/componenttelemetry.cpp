//
// creature/componenttelemetry.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "componenttelemetry.h"

#include "creature.h"

#include "atl/array_struct.h"
#include "crskeleton/skeleton.h"

namespace rage
{


////////////////////////////////////////////////////////////////////////////////

crCreatureComponentTelemetry::crCreatureComponentTelemetry()
: crCreatureComponent(kCreatureComponentTypeTelemetry)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentTelemetry::crCreatureComponentTelemetry(crCreature& creature)
: crCreatureComponent(kCreatureComponentTypeTelemetry)
{
	Init(creature);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentTelemetry::crCreatureComponentTelemetry(datResource& rsc)
: crCreatureComponent(rsc)
, m_Subjects(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentTelemetry::~crCreatureComponentTelemetry()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentTelemetry, crCreatureComponent::kCreatureComponentTypeTelemetry);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentTelemetry::DeclareStruct(datTypeStruct& s)
{
	crCreatureComponent::DeclareStruct(s);

	STRUCT_BEGIN(crCreatureComponentTelemetry);
	STRUCT_FIELD(m_Subjects);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Init(crCreature& creature)
{
	PreInit(creature);

}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Shutdown()
{
	m_Subjects.Reset();

	crCreatureComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Finalize(float deltaTime)
{
	const crSkeleton* skel = GetCreature().GetSkeleton();
	if(skel)
	{
		const int numSubjects = m_Subjects.GetCount();
		for(int i=0; i<numSubjects; ++i)
		{
			m_Subjects[i].Update(deltaTime, *skel);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Pose(const crFrame&, crFrameFilter*, const crCreatureFilter&)
{
	const int numSubjects = m_Subjects.GetCount();
	for(int i=0; i<numSubjects; ++i)
	{
		// TODO --- pose subjects here
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::InversePose(crFrame&, crFrameFilter*, const crCreatureFilter&) const
{
	const int numSubjects = m_Subjects.GetCount();
	for(int i=0; i<numSubjects; ++i)
	{
		// TODO --- inverse pose subjects here
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::IterateDofs(crCreatureComponent::DofIterator&) const
{
	const int numSubjects = m_Subjects.GetCount();
	for(int i=0; i<numSubjects; ++i)
	{
		// TODO --- iterate dofs here
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentTelemetry::GetNumDofs() const
{
	// TODO --- get num dofs here
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentTelemetry::InitDofs(const crFrameDataInitializerCreature& UNUSED_PARAM(initializer)) const
{
	const int numSubjects = m_Subjects.GetCount();
	for(int i=0; i<numSubjects; ++i)
	{
		// TODO --- init dofs here
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Identity(crFrame&, crFrameFilter*, const crCreatureFilter&) const
{
	const int numSubjects = m_Subjects.GetCount();
	for(int i=0; i<numSubjects; ++i)
	{
		// TODO --- identity dofs here
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Reset(const crCreatureFilter&)
{
	const int numSubjects = m_Subjects.GetCount();
	for(int i=0; i<numSubjects; ++i)
	{
		// TODO --- reset dofs here
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentTelemetry::GetSignature() const
{
	// TODO --- implement proper signature
	return crCreatureComponent::GetSignature();
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentTelemetry::Subject::Subject()
: m_Last(V_IDENTITY)
, m_Delta(V_IDENTITY)
, m_Id(0)
, m_Flags(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentTelemetry::Subject::Subject(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentTelemetry::Subject::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crCreatureComponentTelemetry::Subject);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Subject::Update(float UNUSED_PARAM(deltaTime), const crSkeleton& UNUSED_PARAM(skel))
{
/*	u16 idx;
	if(skel.GetSkeletonData().ConvertBoneIdToIndex(m_Id, idx))
	{
		const Matrix34& mtx = skel.GetGlobalMtx(idx);
		m_Last.Transpose(mtx);

		m_Delta = m_Last;
//		m_Delta.  // NORMALIZE USING DELTA TIME
		m_Last = mtx;
	}
*/
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentTelemetry::Subject::Reset()
{
	m_Last = Mat34V(V_IDENTITY);
	m_Delta = Mat34V(V_IDENTITY);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
