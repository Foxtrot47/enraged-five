//
// creature/creature.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "creature.h"

#include "componentmover.h"
#include "componentskeleton.h"

#include "cranimation/framedatafactory.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreature::crCreature()
: m_Skeleton(NULL)
, m_MoverMtx(NULL)
, m_ComponentFactory(NULL)
, m_FrameData(NULL)
, m_FrameDataFactory(NULL)
, m_FrameAccelerator(NULL)
, m_Signature(0)
{
#if FAST_FIND_COMPONENTS
	m_FastFindComponents.Resize(crCreatureComponent::kCreatureComponentTypeNum);
	for(int i=0; i<crCreatureComponent::kCreatureComponentTypeNum; ++i)
	{
		m_FastFindComponents[i] = NULL;
	}
#endif // FAST_FIND_COMPONENTS
}

////////////////////////////////////////////////////////////////////////////////

crCreature::~crCreature()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Init(crFrameDataFactory& factory, crFrameAccelerator& accelerator, crSkeleton* skeleton, Mat34V* moverMtx)
{
	m_FrameDataFactory = &factory;
	m_FrameAccelerator = &accelerator;
	m_Skeleton = skeleton;
	m_MoverMtx = moverMtx;

	// reserve reasonable number of initial components (for this init call, and several subsequent additions)
	m_Components.Reserve(8);

	if(moverMtx)
    {
		crCreatureComponentMover* componentMover = static_cast<crCreatureComponentMover*>(AllocateComponent(crCreatureComponent::kCreatureComponentTypeMover));
		componentMover->Init(*this, *m_MoverMtx);
		AddComponent(*componentMover);
	}

	if(skeleton)
	{
		crCreatureComponentSkeleton* componentSkeleton = static_cast<crCreatureComponentSkeleton*>(AllocateComponent(crCreatureComponent::kCreatureComponentTypeSkeleton));
		componentSkeleton->Init(*this, *m_Skeleton);
		AddComponent(*componentSkeleton);
	}

	Refresh();
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Refresh()
{
	if(m_FrameData)
	{
		m_FrameDataFactory->ReleaseFrameData(*m_FrameData);
	}

	m_FrameData = m_FrameDataFactory->AllocateFrameData(*this);

	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->Refresh();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Shutdown()
{
	RemoveAllComponents();

	if(m_FrameData)
	{
		m_FrameDataFactory->ReleaseFrameData(*m_FrameData);
		m_FrameData = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Finalize(float deltaTime)
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->Finalize(deltaTime);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip)
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->Pose(frame, filter, skip);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->InversePose(frame, filter, skip);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::InitDofs(crFrameData& frameData, crFrameFilter* filter) const
{
	const bool destroyExistingDofs = true;
	crFrameDataInitializerCreature initializer(*this, destroyExistingDofs, filter);
	initializer.InitializeFrameData(frameData);
}

////////////////////////////////////////////////////////////////////////////////

bool crCreature::HasDof(u8 track, u16 id) const
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		if(m_Components[i]->HasDof(track, id))
		{
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreature::GetNumDofs() const
{
	u32 numDofs = 0;

	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		numDofs += m_Components[i]->GetNumDofs();
	}

	return numDofs;
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->Identity(frame, filter, skip);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Reset(const crCreatureFilter& skip)
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->Reset(skip);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::PostUpdate()
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->PostUpdate();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::PreDraw(bool isVisible)
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->PreDraw(isVisible);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::DrawUpdate()
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->DrawUpdate();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::SwapBuffers()
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->SwapBuffers();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::Draw() const
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->Draw();
	}
}

////////////////////////////////////////////////////////////////////////////////

#if !__FINAL
void crCreature::DebugDraw() const
{
	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		m_Components[i]->DebugDraw();
	}
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

u32 crCreature::GetNumComponents() const
{
	return m_Components.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

const crCreatureComponent* crCreature::GetComponentByIndex(u32 componentIdx) const
{
	return m_Components[componentIdx];
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent* crCreature::GetComponentByIndex(u32 componentIdx)
{
	return m_Components[componentIdx];
}

////////////////////////////////////////////////////////////////////////////////

#if FAST_FIND_COMPONENTS
const crCreatureComponent* crCreature::FindComponentSlow(crCreatureComponent::eCreatureComponentType componentType) const
#else // FAST_FIND_COMPONENTS
const crCreatureComponent* crCreature::FindComponent(crCreatureComponent::eCreatureComponentType componentType) const
#endif // FAST_FIND_COMPONENTS
{
	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		Assert(m_Components[i]);
		if(m_Components[i]->GetType() == componentType)
		{
			return m_Components[i];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if FAST_FIND_COMPONENTS
crCreatureComponent* crCreature::FindComponentSlow(crCreatureComponent::eCreatureComponentType componentType)
#else // FAST_FIND_COMPONENTS
crCreatureComponent* crCreature::FindComponent(crCreatureComponent::eCreatureComponentType componentType)
#endif // FAST_FIND_COMPONENTS
{
	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		Assert(m_Components[i]);
		if(m_Components[i]->GetType() == componentType)
		{
			return m_Components[i];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

int ComponentPrioritySortCb(crCreatureComponent*const* c0, crCreatureComponent*const* c1)
{
	u64 priority0 = (u64((*c0)->GetPriority())<<32)|u64((*c0)->GetType());
	u64 priority1 = (u64((*c1)->GetPriority())<<32)|u64((*c1)->GetType());

	if(priority0 < priority1)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::AddComponent(crCreatureComponent& component)
{
	m_Components.PushAndGrow(&component);

	m_Components.QSort(0, -1, ComponentPrioritySortCb);

#if FAST_FIND_COMPONENTS
	if(!m_FastFindComponents[component.GetType()])
	{
		m_FastFindComponents[component.GetType()] = &component;
	}
#endif // FAST_FIND_COMPONENTS

	component.CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent* crCreature::AddComponentType(crCreatureComponent::eCreatureComponentType componentType)
{
	crCreatureComponent* component = AllocateComponent(componentType);
	AddComponent(*component);

	return component;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreature::RemoveComponent(u32 componentIdx)
{
#if FAST_FIND_COMPONENTS
	crCreatureComponent::eCreatureComponentType componentType = m_Components[componentIdx]->GetType();
#endif // FAST_FIND_COMPONENTS

	FreeComponent(*m_Components[componentIdx]);
	m_Components.Delete(componentIdx);

#if FAST_FIND_COMPONENTS
	m_FastFindComponents[int(componentType)] = FindComponentSlow(componentType);
#endif // FAST_FREQUENT_COMPONENTS

	CalcAndSetSignature();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreature::RemoveComponentType(crCreatureComponent::eCreatureComponentType componentType)
{
	bool removed = false;
	for(u32 i=0; i<u32(m_Components.GetCount()); ++i)
	{
		Assert(m_Components[i]);
		if(m_Components[i]->GetType() == componentType)
		{
			RemoveComponent(i--);
			removed |= true;
		}
	}

	return removed;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreature::RemoveAllComponents()
{
	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		Assert(m_Components[i]);
		FreeComponent(*m_Components[i]);
	}

	m_Components.Reset();

#if FAST_FIND_COMPONENTS
	for(int i=0; i<crCreatureComponent::kCreatureComponentTypeNum; ++i)
	{
		m_FastFindComponents[i] = NULL;
	}
#endif // FAST_FIND_COMPONENTS

	m_Signature = 0;

	return (numComponents > 0);
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		// initialize components
		crCreatureComponent::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::ShutdownClass()
{
	crCreatureComponent::ShutdownClass();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent* crCreature::AllocateComponent(crCreatureComponent::eCreatureComponentType componentType) const
{
	if(m_ComponentFactory)
	{
		return m_ComponentFactory->Allocate(componentType);
	}
	else
	{
		return crCreatureComponent::Allocate(componentType);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::FreeComponent(crCreatureComponent& component) const
{
	if(component.m_Factory)
	{
		component.m_Factory->Free(component);
	}
	else
	{
		delete &component;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreature::CalcAndSetSignature()
{
	m_Signature = 0;

	const int numComponents = m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		u32 componentSig = m_Components[i]->GetSignature();
		m_Signature = atDataHash(&componentSig, sizeof(u32), m_Signature);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crCreature::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerCreature::crFrameDataInitializerCreature(const crCreature& creature, bool destroyExistingDofs, crFrameFilter* filter)
: crFrameDataInitializer(destroyExistingDofs, filter)
, m_Creature(&creature)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameDataInitializerCreature::~crFrameDataInitializerCreature()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameDataInitializerCreature::PreInitialize() const
{
	return m_Creature->GetNumDofs();
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameDataInitializerCreature::Initialize() const
{
	bool inOrder = true;

	const u32 numComponents = m_Creature->GetNumComponents();
	for(u32 i=0; i<numComponents; ++i)
	{
		const crCreatureComponent& component = *m_Creature->GetComponentByIndex(i);
		inOrder = component.InitDofs(*this);
	}

	return (numComponents <= 1) && inOrder;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
