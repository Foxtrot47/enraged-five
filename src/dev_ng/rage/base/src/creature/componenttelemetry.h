//
// creature/componenttelemetry.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTTELEMETRY_H
#define CREATURE_COMPONENTTELEMETRY_H

#include "component.h"

#include "vectormath/mat34v.h"

namespace rage
{

class crSkeleton;

// PURPOSE: Creature telemetry component
class crCreatureComponentTelemetry : public crCreatureComponent
{
public:

	// PURPOSE: Default constructor
	crCreatureComponentTelemetry();

	// PURPOSE: Initializing constructor
	// PARAMS: creature - reference to creature this component is part of
	crCreatureComponentTelemetry(crCreature& creature);

	// PURPOSE: Resource constructor
	crCreatureComponentTelemetry(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentTelemetry();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentTelemetry);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: creature - reference to creature this component is part of
	void Init(crCreature& creature);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();


	// PURPOSE: Finalize override
	virtual void Finalize(float deltaTime);

	// PURPOSE: Pose override
	virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

	// PURPOSE: Inverse Pose override
	virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

	// PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

	// PURPOSE: Identity override
	virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;
	
	// PURPOSE: Reset override
	virtual void Reset(const crCreatureFilter&);

	// PURPOSE: Signature override
	virtual u32 GetSignature() const;

protected:

	// PURPOSE: Subject to track
	struct Subject
	{
		// PURPOSE: Default constructor
		Subject();

		// PURPOSE: Resource constructor
		Subject(datResource&);

		// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT
		
		// PURPOSE: Update subject
		void Update(float deltaTime, const crSkeleton& skel);

		// PURPOSE: Reset
		void Reset();

		Mat34V m_Last;
		Mat34V m_Delta;

		u16 m_Id;
		u8 m_Flags;
	};

	atArray<Subject> m_Subjects;
};


} // namespace rage

#endif // CREATURE_COMPONENTTELEMETRY_H
