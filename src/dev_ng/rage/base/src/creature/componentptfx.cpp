//
// creature/componentptfx.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "componentptfx.h"

#include "creature.h"

#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "grcore/debugdraw.h"
#include "grprofile/drawcore.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentParticleEffect::crCreatureComponentParticleEffect()
: crCreatureComponentRoot(kCreatureComponentTypeParticleEffect)
{

}

crCreatureComponentParticleEffect::crCreatureComponentParticleEffect(crCreature& creature, const Mat34V* parentMtx)
: crCreatureComponentRoot(kCreatureComponentTypeParticleEffect, creature, parentMtx)
{
    Init(creature, parentMtx);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentParticleEffect::crCreatureComponentParticleEffect(datResource& rsc)
: crCreatureComponentRoot(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentParticleEffect::~crCreatureComponentParticleEffect()
{

}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentParticleEffect, crCreatureComponent::kCreatureComponentTypeParticleEffect);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentParticleEffect::DeclareStruct(datTypeStruct& s)
{
    crCreatureComponentRoot::DeclareStruct(s);

    STRUCT_BEGIN(crCreatureComponentParticleEffect);
    STRUCT_FIELD(m_EvolutionParameters);
    STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::Init(crCreature& creature, const Mat34V* parentMtx)
{
    crCreatureComponentRoot::InitRoot(creature, parentMtx);

}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::Shutdown()
{
	RemoveAllEvolutionParameters();

	crCreatureComponentRoot::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip)
{
    crCreatureComponentRoot::Pose(frame, filter, skip);

	const u32 numEvolutionParameters = GetNumEvolutionParameters();
    for(u32 i=0; i<numEvolutionParameters; ++i)
    {
		EvolutionParameter& ep = m_EvolutionParameters[i];
		frame.GetFloat(kTrackParticleData, ep.m_Id, ep.m_Value);
    }
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
    crCreatureComponentRoot::InversePose(frame, filter, skip);

	const u32 numEvolutionParameters = GetNumEvolutionParameters();
	for(u32 i=0; i<numEvolutionParameters; ++i)
	{
		const EvolutionParameter& ep = m_EvolutionParameters[i];
		frame.SetFloat(kTrackParticleData, ep.m_Id, ep.m_Value);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	crCreatureComponentRoot::IterateDofs(it);

	const u32 numEvolutionParameters = GetNumEvolutionParameters();
	for(u32 i=0; i<numEvolutionParameters; ++i)
	{
		const EvolutionParameter& ep = m_EvolutionParameters[i];
		it.IterateDof(kTrackParticleData, ep.m_Id, kFormatTypeFloat);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentParticleEffect::HasDof(u8 track, u16 id) const
{
	if(track == kTrackParticleData)
	{
		u32 junk;
		return FindEvolutionParameterIndexById(id, junk);
	}
	return crCreatureComponentRoot::HasDof(track, id);
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentParticleEffect::GetNumDofs() const
{
	return crCreatureComponentRoot::GetNumDofs() + GetNumEvolutionParameters();
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentParticleEffect::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	if(crCreatureComponentRoot::InitDofs(initializer))
	{
		const u32 numEvolutionParameters = GetNumEvolutionParameters();
		for(u32 i=0; i<numEvolutionParameters; ++i)
		{
			const EvolutionParameter& ep = m_EvolutionParameters[i];
			initializer.FastAddDof(kTrackParticleData, ep.m_Id, kFormatTypeFloat);
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const
{
    crCreatureComponentRoot::Identity(frame, filter, skip);

	const u32 numEvolutionParameters = GetNumEvolutionParameters();
	for(u32 i=0; i<numEvolutionParameters; ++i)
	{
		const EvolutionParameter& ep = m_EvolutionParameters[i];
		frame.SetFloat(kTrackParticleData, ep.m_Id, 0.f);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::Reset(const crCreatureFilter& skip)
{
    crCreatureComponentRoot::Reset(skip);

	const u32 numEvolutionParameters = GetNumEvolutionParameters();
	for(u32 i=0; i<numEvolutionParameters; ++i)
	{
		EvolutionParameter& ep = m_EvolutionParameters[i];
		ep.m_Value = 0.f;
	}
}

////////////////////////////////////////////////////////////////////////////////

#if !__FINAL
void crCreatureComponentParticleEffect::DebugDraw() const
{
	bool bOldLighting = grcLighting(false);
	{
		grcWorldIdentity();
		grcDrawAxis(0.5f, m_RootMtx);
	}
	grcLighting(bOldLighting);
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentParticleEffect::FindEvolutionParameterIndexByName(const char* name, u32& outIdx) const
{
	return FindEvolutionParameterIndexByHash(atStringHash(name), outIdx);
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentParticleEffect::FindEvolutionParameterIndexByHash(u32 hash, u32& outIdx) const
{
	const u32 numEvolutionParameters = GetNumEvolutionParameters();
	for(u32 i=0; i<numEvolutionParameters; ++i)
	{
		const EvolutionParameter& ep = m_EvolutionParameters[i];
		if(ep.m_Hash == hash)
		{
			outIdx = i;
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentParticleEffect::FindEvolutionParameterIndexById(u16 id, u32& outIdx) const
{
	// TODO --- BINARY SEARCH THIS, AS EVOLUTION PARAMETERS ARE IN ID ORDER

	const u32 numEvolutionParameters = GetNumEvolutionParameters();
	for(outIdx=0; outIdx<numEvolutionParameters; ++outIdx)
	{
		const EvolutionParameter& ep = m_EvolutionParameters[outIdx];
		if(ep.m_Id == id)
		{
			return true;
		}
		else if(ep.m_Id > id)
		{
			return false;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentParticleEffect::AddEvolutionParameter(const char* name)
{
	u16 id = atHash16U(name);

	u32 idx = 0;
	if(!FindEvolutionParameterIndexById(id, idx))
	{
		EvolutionParameter* ep = NULL;
		if(idx == GetNumEvolutionParameters())
		{
			ep = &m_EvolutionParameters.Grow();
		}
		else
		{
			ep = &m_EvolutionParameters.Insert(idx);
		}

		ep->m_Name = name;
		ep->m_Hash = atStringHash(name);
		ep->m_Id = id;
		ep->m_Value = 0.f;

		CalcAndSetSignature();

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentParticleEffect::RemoveAllEvolutionParameters()
{
	m_EvolutionParameters.Reset();

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentParticleEffect::CalcSignature() const
{
	u32 signature = crCreatureComponentRoot::CalcSignature();
	
	const u32 numEvolutionParameters = GetNumEvolutionParameters();
	for(u32 i=0; i<numEvolutionParameters; ++i)
	{
		const EvolutionParameter& ep = m_EvolutionParameters[i];
		atDataHash((const char*)&ep.m_Id, 2, signature);
	}
	return signature;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentParticleEffect::EvolutionParameter::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(EvolutionParameter);
	STRUCT_FIELD(m_Name);
	STRUCT_FIELD(m_Hash);
	STRUCT_FIELD(m_Id);
	STRUCT_FIELD(m_Value);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
