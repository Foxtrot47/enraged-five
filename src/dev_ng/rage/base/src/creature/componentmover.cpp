//
// creature/componentmover.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "componentmover.h"

#include "creature.h"

#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "grprofile/drawcore.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentMover::crCreatureComponentMover()
: crCreatureComponent(kCreatureComponentTypeMover)
, m_LastMoverMtx(V_IDENTITY)
, m_MoverMtx(NULL)
, m_MoverId(0)
, m_SuppressReset(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentMover::crCreatureComponentMover(crCreature& creature, Mat34V& moverMtx)
: crCreatureComponent(kCreatureComponentTypeMover)
, m_LastMoverMtx(V_IDENTITY)
, m_MoverMtx(NULL)
, m_MoverId(0)
, m_SuppressReset(false)
{
	Init(creature, moverMtx);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentMover::crCreatureComponentMover(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentMover::~crCreatureComponentMover()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentMover, crCreatureComponent::kCreatureComponentTypeMover);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentMover::DeclareStruct(datTypeStruct& s)
{
	crCreatureComponent::DeclareStruct(s);
	
	STRUCT_BEGIN(crCreatureComponentMover);
	STRUCT_FIELD(m_LastMoverMtx);
	STRUCT_FIELD(m_MoverMtx);
	STRUCT_FIELD(m_MoverId);
	STRUCT_FIELD(m_SuppressReset);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::Init(crCreature& creature, Mat34V& moverMtx)
{
	PreInit(creature);

	m_MoverMtx = &moverMtx;
	m_LastMoverMtx = *m_MoverMtx;

	Assert(GetCreature().GetSkeleton());
//	Assert(GetCreature().GetSkeleton()->GetParentMtx() == m_MoverMtx);  // no longer required (though inverse pose will not function properly)

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::Shutdown()
{
	m_MoverMtx = NULL;

	crCreatureComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::Finalize(float)
{
	Assert(m_MoverMtx);
	m_LastMoverMtx = *m_MoverMtx;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::Pose(const crFrame& frame, crFrameFilter*, const crCreatureFilter&)
{
	Assert(m_MoverMtx);

	Mat34V deltaMtx;
	if(frame.GetMoverMatrix(m_MoverId, deltaMtx))
	{
		Transform(*m_MoverMtx, m_LastMoverMtx, deltaMtx);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::InversePose(crFrame& frame, crFrameFilter*, const crCreatureFilter&) const
{
	Assert(m_MoverMtx);

	Mat34V deltaMtx;
	UnTransformOrtho(deltaMtx, m_LastMoverMtx, *m_MoverMtx);

	frame.SetMoverMatrix(m_MoverId, deltaMtx);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	it.IterateDof(kTrackMoverTranslation, m_MoverId, kFormatTypeVector3);
	it.IterateDof(kTrackMoverRotation, m_MoverId, kFormatTypeQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentMover::HasDof(u8 track, u16 id) const
{
	return (id == m_MoverId) && (track == kTrackMoverTranslation || track == kTrackMoverRotation);
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentMover::GetNumDofs() const
{
	return 2;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentMover::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	initializer.FastAddDof(kTrackMoverTranslation, m_MoverId, kFormatTypeVector3);
	initializer.FastAddDof(kTrackMoverRotation, m_MoverId, kFormatTypeQuaternion);
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::Identity(crFrame& frame, crFrameFilter*, const crCreatureFilter&) const
{
	// TODO -- use filter!
	frame.SetMoverMatrix(m_MoverId, Mat34V(V_IDENTITY));
}


////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentMover::Reset(const crCreatureFilter&)
{
	if(!m_SuppressReset)
	{
		*m_MoverMtx = m_LastMoverMtx;
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentMover::GetPriority() const
{
	return sm_DefaultComponentPriority + 1;
}

////////////////////////////////////////////////////////////////////////////////

#if !__FINAL
void crCreatureComponentMover::DebugDraw() const
{
	const Mat34V& mvrMtx = GetLastMoverMtx();
    const Vec3V pos = mvrMtx.GetCol3();

	grcBegin(drawLines, 12);
		grcWorldIdentity();
		grcColor3f(0.f, 1.0f, 0.f);
		grcVertex3f(pos);
		grcVertex3f(1.f, 0.f, 1.f);
		grcVertex3f(pos);
		grcVertex3f(-1.f, 0.f, 1.f);
		grcVertex3f(pos);
		grcVertex3f(-1.f ,0.f ,-1.0f);
		grcVertex3f(pos);
		grcVertex3f(1.f, 0.f, -1.f);

		Vec3V To = Transform(mvrMtx, Vec3V(V_Z_AXIS_WONE));
		grcVertex3f(pos);
		grcVertex3f(To);
		
		Vec3V Up = Transform(mvrMtx, Vec3V(0.f, 0.25f, 0.75f));
		grcVertex3f(To);
		grcVertex3f(Up);
	grcEnd();
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentMover::CalcSignature() const
{
	// TODO --- implement better signature
	return (m_MoverId<<16)|crCreatureComponent::GetSignature();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
