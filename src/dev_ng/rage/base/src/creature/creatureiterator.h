//
// creature/creatureiterator.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_CREATUREITERATOR_H
#define CREATURE_CREATUREITERATOR_H

#include "creature.h"
#include "component.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creature iterator
template <typename _T>
class crCreatureIterator
{
public:

	// PURPOSE: Initialization
	// PARAMS: creature - creature to iterate
	crCreatureIterator(crCreature& creature);

	// PURPOSE: Initialization
	// PARAMS: creature - constant creature to iterate
	crCreatureIterator(const crCreature& creature);

	// PURPOSE: Component iterator, iterates all components within a creature
	void IterateComponents();

	// PURPOSE: DOF iterator, iterates all the DOFs within a creature
	void IterateDofs();  // TODO --- support crFrameFilter*

protected:

	// PURPOSE: Creature iterator's specialized dof iterator
	// Used to intercept DOF iterate calls from components
	class DofIterator : public crCreatureComponent::DofIterator
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: Base creature iterator
		DofIterator(crCreatureIterator<_T>& it);

		// PURPOSE: Destructor
		virtual ~DofIterator();

		// PRUPOSE: Override individual DOF iterate call
		virtual void IterateDof(u8 track, u16 id, u8 type);

		// TODO --- more efficient to implement own iterator, with templated call out
		// PURPOSE: Override frame DOF iterate call
//		virtual void IterateFrame(const crFrame&);


	private:
		crCreatureIterator<_T>* m_Iterator;
	};

	crCreature* m_Creature;
};

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crCreatureIterator<_T>::crCreatureIterator(crCreature& creature)
: m_Creature(&creature)
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crCreatureIterator<_T>::crCreatureIterator(const crCreature& creature)
: m_Creature(const_cast<crCreature*>(&creature))
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void crCreatureIterator<_T>::IterateComponents()
{
	const int numComponents = m_Creature->m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		_T& t = *(_T*)(this);
		t.IterateComponent(*m_Creature->m_Components[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void crCreatureIterator<_T>::IterateDofs()
{
	const int numComponents = m_Creature->m_Components.GetCount();
	for(int i=0; i<numComponents; ++i)
	{
		DofIterator it(*this);
		m_Creature->m_Components[i]->IterateDofs(it);
	}
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crCreatureIterator<_T>::DofIterator::DofIterator(crCreatureIterator<_T>& it)
: m_Iterator(&it)
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crCreatureIterator<_T>::DofIterator::~DofIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
void crCreatureIterator<_T>::DofIterator::IterateDof(u8 track, u16 id, u8 type)
{
	// TODO -- filter

	_T& t = *(_T*)m_Iterator;
	t.IterateDof(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

//template<typename _T>
//void crCreatureIterator<_T>::DofIterator::IterateFrame(const crFrame&)
//{
	//???
//}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREATURE_CREATUREITERATOR_H
