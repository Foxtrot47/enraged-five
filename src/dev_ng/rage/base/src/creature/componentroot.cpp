//
// creature/componentroot.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "componentroot.h"

#include "creature.h"

#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "vectormath/classes.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentRoot::crCreatureComponentRoot(eCreatureComponentType creatureComponentType)
: crCreatureComponent(creatureComponentType)
{

}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentRoot::crCreatureComponentRoot(eCreatureComponentType creatureComponentType, crCreature& /*creature*/,
                                                 const Mat34V* /*pParentMtx*/)
: crCreatureComponent(creatureComponentType)
, m_RootMtx( V_IDENTITY )
{
    // NOTE: it is standard practice to call Init(...) here, but let's let the derived class do that because this class
    //  is not meant to be instantiated directly.
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentRoot::crCreatureComponentRoot(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentRoot::~crCreatureComponentRoot()
{
    Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentRoot::DeclareStruct(datTypeStruct& s)
{
    crCreatureComponent::DeclareStruct(s);

    STRUCT_BEGIN(crCreatureComponentRoot);
    STRUCT_FIELD(m_RootMtx);
    STRUCT_FIELD(m_ParentMtx);
    STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentRoot::InitRoot(crCreature& creature, const Mat34V* pParentMtx)
{
    PreInit(creature);

    m_RootMtx = Mat34V(V_IDENTITY);

    if ( pParentMtx )
    {
        m_ParentMtx = *pParentMtx;
        m_RootMtx = m_ParentMtx;
    }
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentRoot::Pose(const crFrame& frame, crFrameFilter*, const crCreatureFilter&)
{
	Mat34V m(V_IDENTITY);
    frame.GetBoneMatrix( 0, m );

	Transform(m_RootMtx, m_ParentMtx, m);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentRoot::InversePose(crFrame& frame, crFrameFilter*, const crCreatureFilter&) const
{
	Mat34V m;
	UnTransformOrtho(m, m_ParentMtx, m_RootMtx);
    frame.SetBoneMatrix( 0, m );
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentRoot::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	it.IterateDof(kTrackBoneTranslation, 0, kFormatTypeVector3);
	it.IterateDof(kTrackBoneRotation, 0, kFormatTypeQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentRoot::HasDof(u8 track, u16 id) const
{
	return (id == 0) && (track == kTrackBoneTranslation || track == kTrackBoneRotation);
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentRoot::GetNumDofs() const
{
	return 2;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentRoot::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	initializer.FastAddDof(kTrackBoneTranslation, 0, kFormatTypeVector3);
	initializer.FastAddDof(kTrackBoneRotation, 0, kFormatTypeQuaternion);
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentRoot::Identity(crFrame& frame, crFrameFilter*, const crCreatureFilter&) const
{
    // TODO -- use filter!
	frame.SetBoneMatrix( 0, Mat34V(V_IDENTITY) );
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentRoot::Reset(const crCreatureFilter&)
{
    m_RootMtx = m_ParentMtx;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
