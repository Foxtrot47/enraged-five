//
// creature/componentshadervars.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTSHADERVARS_H
#define CREATURE_COMPONENTSHADERVARS_H

#include "component.h"

#include "atl/array.h"
#include "atl/string.h"
#include "cranimation/framedata.h"
#include "grmodel/shadergroup.h"
#include "grmodel/shadergroupvar.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creature shader variable component
// Provides interface between degrees of freedom and shader parameters
class crCreatureComponentShaderVars : public crCreatureComponent
{
public:

	// PURPOSE: Default constructor
	crCreatureComponentShaderVars();

	// PURPOSE: Initializing constructor
	// PARAMS: creature - reference to creature this component is part of
	// shaderGroup, shaderGroupVar - optional shader group and var
	// NOTES: Only provide shaderGroup[Var] if the component is going to 
	// directly manage the setting of the shader variables
	crCreatureComponentShaderVars(crCreature& creature, grmShaderGroup* shaderGroup=NULL, grmShaderGroupVar shaderGroupVar=grmsgvNONE);

	// PURPOSE: Resource constructor
	crCreatureComponentShaderVars(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentShaderVars();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentShaderVars);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT


	// PURPOSE: Initialization
	// PARAMS: creature - reference to creature this component is part of
	// shaderGroup, shaderGroupVar - optional shader group and var
	// NOTES: Only provide shaderGroup[Var] if the component is going to 
	// directly manage the setting of the shader variables
	void Init(crCreature& creature, grmShaderGroup* shaderGroup=NULL, grmShaderGroupVar shaderGroupVar=grmsgvNONE);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();


	// PURPOSE: Pose override
	virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

	// PURPOSE: Inverse Pose override
	virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

	// PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

	// PURPOSE: Identity override
	virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;
	
	// PURPOSE: Reset override
	virtual void Reset(const crCreatureFilter&);

	// PURPOSE: DrawUpdate override
	virtual void DrawUpdate();


	// PURPOSE: Get total number of shader variable DOF pairs
	u32 GetNumShaderVarDofPairs() const;

	// PURPOSE: Get a shader variable DOF pair
	void GetShaderVarDofPair(u32 idx, u8& track, u16& id, u64& projectData, float& value) const;

	// PURPOSE: Add a new shader variable DOF pair
	void AddShaderVarDofPair(u8 track, u16 id, u64 projectData);

	// PURPOSE: Batch add new shader variable DOF pairs efficiently
	struct FastAddShaderVarDofPairs
	{
		FastAddShaderVarDofPairs(crCreatureComponentShaderVars& component, u32 numReserve=0);
		~FastAddShaderVarDofPairs();

		void FastAddPair(u8 track, u16 id, u64 projectData);

	private:
		crCreatureComponentShaderVars* m_Component;
	};
	friend struct FastAddShaderVarDofPairs;


	// TODO --- maybe move project data to base component class?

	// PURPOSE: Get project data
	u32 GetProjectData() const;

	// PURPOSE: Set project data
	void SetProjectData(u32 projectData);
	
	// PURPOSE: Internal structure for storing a DOF, value and relationship to shader
	struct ShaderVarDofPair
	{
		// PURPOSE: Default constructor
		ShaderVarDofPair();

		// PURPOSE: Initializing constructor (for binary searches)
		ShaderVarDofPair(u8 track, u16 id);

		// PURPOSE: Initializer
		void Init(u8 track, u16 id, u64 projectData);

#if __DECLARESTRUCT
		// PURPOSE: Resourcing
		void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

		u8 m_Duplicate;
		u8 m_Track; 
		u16 m_Id;

		float m_Value;

		u64 m_ProjectData;

		bool operator==(const ShaderVarDofPair&) const;
		bool operator<(const ShaderVarDofPair&) const;
		static int CompareFunc(const ShaderVarDofPair*, const ShaderVarDofPair*);
	};

	// PURPOSE: Get shader variable DOF pairs array
	ShaderVarDofPair* GetShaderVarDofPairs();

protected:

	// PURPOSE: Re-calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Re-sort pairs after unordered fast appends
	void SortShaderVarDofPairs();

private:

	atArray<ShaderVarDofPair, 16> m_ShaderVarDofPairs;

	u32 m_NumUniqueDofs;

	u32 m_ProjectData;

	grmShaderGroup* m_ShaderGroup;
	grmShaderGroupVar m_ShaderGroupVar;

	template<bool> friend class ShaderVarPoseIterator;
};

////////////////////////////////////////////////////////////////////////////////

inline u32 crCreatureComponentShaderVars::GetNumShaderVarDofPairs() const
{
	return m_ShaderVarDofPairs.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponentShaderVars::ShaderVarDofPair* crCreatureComponentShaderVars::GetShaderVarDofPairs()
{
	return m_ShaderVarDofPairs.GetElements();
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentShaderVars::GetShaderVarDofPair(u32 idx, u8& track, u16& id, u64& projectData, float& value) const
{
	const ShaderVarDofPair& pair = m_ShaderVarDofPairs[idx];
	track = pair.m_Track;
	id = pair.m_Id;
	projectData = pair.m_ProjectData;
	value = pair.m_Value;
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentShaderVars::FastAddShaderVarDofPairs::FastAddPair(u8 track, u16 id, u64 projectData)
{
	ShaderVarDofPair& pair = m_Component->m_ShaderVarDofPairs.Grow();
	pair.Init(track, id, projectData);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crCreatureComponentShaderVars::GetProjectData() const
{
	return m_ProjectData;
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentShaderVars::SetProjectData(u32 projectData)
{
	m_ProjectData = projectData;
}

////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponentShaderVars::ShaderVarDofPair::ShaderVarDofPair()
{
}

////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponentShaderVars::ShaderVarDofPair::ShaderVarDofPair(u8 track, u16 id)
: m_Track(track)
, m_Id(id)
{
	Assert(m_Track == kTrackAnimatedNormalMaps);
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentShaderVars::ShaderVarDofPair::Init(u8 track, u16 id, u64 projectData)
{
	m_Track = track;
	m_Id = id;
	m_Value = 0.f;
	m_ProjectData = projectData;

	Assert(m_Track == kTrackAnimatedNormalMaps);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline bool crCreatureComponentShaderVars::ShaderVarDofPair::operator==(const ShaderVarDofPair &r) const
{
	return m_Id == r.m_Id;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline bool crCreatureComponentShaderVars::ShaderVarDofPair::operator<(const ShaderVarDofPair &r) const
{
	return m_Id < r.m_Id;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline int crCreatureComponentShaderVars::ShaderVarDofPair::CompareFunc(const ShaderVarDofPair* pair0, const ShaderVarDofPair* pair1)
{
	return (*pair0<*pair1)?-1:((*pair0==*pair1)?0:1);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREATURE_COMPONENTSHADERVARS_H
