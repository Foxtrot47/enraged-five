//
// creature/componentshadervars.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "componentshadervars.h"

#include "creature.h"

#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "cranimation/frameiterators.h"
#include "zlib/zlib.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentShaderVars::crCreatureComponentShaderVars()
: crCreatureComponent(kCreatureComponentTypeShaderVars)
, m_NumUniqueDofs(0)
, m_ProjectData(0)
, m_ShaderGroup(NULL)
, m_ShaderGroupVar(grmsgvNONE)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentShaderVars::crCreatureComponentShaderVars(crCreature& creature, grmShaderGroup* shaderGroup, grmShaderGroupVar shaderGroupVar)
: crCreatureComponent(kCreatureComponentTypeShaderVars)
, m_NumUniqueDofs(0)
, m_ProjectData(0)
, m_ShaderGroup(NULL)
, m_ShaderGroupVar(grmsgvNONE)
{
	Init(creature, shaderGroup, shaderGroupVar);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentShaderVars::crCreatureComponentShaderVars(datResource& rsc)
: crCreatureComponent(rsc)
, m_ShaderVarDofPairs(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentShaderVars::~crCreatureComponentShaderVars()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentShaderVars, crCreatureComponent::kCreatureComponentTypeShaderVars);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(grmShaderGroupVar);
void crCreatureComponentShaderVars::DeclareStruct(datTypeStruct& s)
{
	crCreatureComponent::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crCreatureComponentShaderVars, crCreatureComponent)
	SSTRUCT_FIELD(crCreatureComponentShaderVars, m_ShaderVarDofPairs)
	SSTRUCT_FIELD(crCreatureComponentShaderVars, m_NumUniqueDofs)
	SSTRUCT_FIELD(crCreatureComponentShaderVars, m_ProjectData)
	SSTRUCT_FIELD_VP(crCreatureComponentShaderVars, m_ShaderGroup)
	SSTRUCT_FIELD(crCreatureComponentShaderVars, m_ShaderGroupVar)
	SSTRUCT_END(crCreatureComponentShaderVars)
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::Init(crCreature& creature, grmShaderGroup* shaderGroup, grmShaderGroupVar shaderGroupVar)
{
	PreInit(creature);

	m_ShaderGroup = shaderGroup;
	m_ShaderGroupVar = shaderGroupVar;

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::Shutdown()
{
	m_ShaderVarDofPairs.Reset();

	m_NumUniqueDofs = 0;
	m_ProjectData = 0;

	m_ShaderGroup = NULL;
	m_ShaderGroupVar = grmsgvNONE;

	crCreatureComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

class ShaderVarFrameFilter : public crFrameFilter
{
public:

	ShaderVarFrameFilter(crFrameFilter* filter=NULL)
		: crFrameFilter(crFrameFilter::kFrameFilterTypeShaderVar)
		, m_Filter(filter)
	{
	}

	ShaderVarFrameFilter(datResource& rsc)
		: crFrameFilter(rsc)
	{
	}

	CR_DECLARE_FRAME_FILTER_TYPE(ShaderVarFrameFilter);

	virtual bool FilterDof(u8 track, u16 id, float& inoutWeight)
	{
		if(track == kTrackAnimatedNormalMaps)
		{
			if(m_Filter)
			{
				return m_Filter->FilterDof(track, id, inoutWeight);
			}
			return true;
		}

		return false;
	}

	virtual u32 CalcSignature() const
	{
		if(m_Filter)
		{
			return crc32(m_Filter->GetSignature(), (const u8*)&sm_Hash, sizeof(u32));
		}
		else
		{
			return sm_Hash;
		}
	}

	static u32 sm_Hash;
	crFrameFilter* m_Filter;
};

CR_IMPLEMENT_FRAME_FILTER_TYPE(ShaderVarFrameFilter, crFrameFilter::kFrameFilterTypeShaderVar, crFrameFilter);

u32 ShaderVarFrameFilter::sm_Hash = atHash_const_char("ShaderVarFrameFilter");

////////////////////////////////////////////////////////////////////////////////

template<bool inversePose>
class ShaderVarPoseIterator : public crFrameIterator<ShaderVarPoseIterator<inversePose> >
{
public:
	ShaderVarPoseIterator(const crFrame& frame, crCreatureComponentShaderVars& shaderComponent)
		: crFrameIterator<ShaderVarPoseIterator<inversePose> >(frame)
		, m_ShaderComponent(&shaderComponent)
		, m_Index(0)
	{
	}

	void Iterate(crFrameFilter* filter, float weight, bool validOnly)
	{
		crFrameIterator<ShaderVarPoseIterator>::Iterate(filter, weight, validOnly);

		if(!inversePose)
		{
			const int numPairs = m_ShaderComponent->m_ShaderVarDofPairs.GetCount();
			for(; m_Index<numPairs; ++m_Index)
			{
				crCreatureComponentShaderVars::ShaderVarDofPair& pair = m_ShaderComponent->m_ShaderVarDofPairs[m_Index];
				pair.m_Value = 0.f;
			}
		}
	}

	__forceinline void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		Assert(dof.m_Track == kTrackAnimatedNormalMaps);
		Assert(dof.m_Type == kFormatTypeFloat);

		const u16 id = dof.m_Id;

		const int numPairs = m_ShaderComponent->m_ShaderVarDofPairs.GetCount();
		for(; m_Index<numPairs; ++m_Index)
		{
			crCreatureComponentShaderVars::ShaderVarDofPair& pair = m_ShaderComponent->m_ShaderVarDofPairs[m_Index];
			if(pair.m_Id == id)
			{
				if(inversePose)
				{
					dest.Set<float>(pair.m_Value);
				}
				else
				{
					pair.m_Value = dest.GetUnsafe<float>();
				}
				if(!pair.m_Duplicate)
				{
					++m_Index;
					break;
				}
			}
			else if(pair.m_Id > id)
			{
				break;
			}
			else if(!inversePose)
			{
				pair.m_Value = 0.f;
			}
		}
	}

	crCreatureComponentShaderVars* m_ShaderComponent;
	int m_Index;
};

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip)
{
	if(!skip.UseComposer())
	{
		ShaderVarFrameFilter shaderVarFilter(filter);
		ShaderVarPoseIterator<false> it(frame, *this);
		it.Iterate(&shaderVarFilter, 1.f, true);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const
{
	ShaderVarFrameFilter shaderVarFilter(filter);
	ShaderVarPoseIterator<true> it(frame, *const_cast<crCreatureComponentShaderVars*>(this));
	it.Iterate(&shaderVarFilter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::IterateDofs(crCreatureComponent::DofIterator& it) const
{
	const int numPairs = m_ShaderVarDofPairs.GetCount();
	for(int i=0; i<numPairs; ++i)
	{
		const ShaderVarDofPair& pair = m_ShaderVarDofPairs[i];
		if(!pair.m_Duplicate)
		{
			it.IterateDof(pair.m_Track, pair.m_Id, kFormatTypeFloat);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentShaderVars::HasDof(u8 track, u16 id) const
{
	if(track == kTrackAnimatedNormalMaps)
	{
		return m_ShaderVarDofPairs.BinarySearch(ShaderVarDofPair(track, id))>=0;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentShaderVars::GetNumDofs() const
{
	return m_NumUniqueDofs;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentShaderVars::InitDofs(const crFrameDataInitializerCreature& initializer) const
{
	const int numPairs = m_ShaderVarDofPairs.GetCount();
	for(int i=0; i<numPairs; ++i)
	{
		const ShaderVarDofPair& pair = m_ShaderVarDofPairs[i];
		if(!pair.m_Duplicate)
		{
			initializer.FastAddDof(pair.m_Track, pair.m_Id, kFormatTypeFloat);
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::Identity(crFrame& frame, crFrameFilter* UNUSED_PARAM(filter), const crCreatureFilter&) const
{
	const int numPairs = m_ShaderVarDofPairs.GetCount();
	for(int i=0; i<numPairs; ++i)
	{
		const ShaderVarDofPair& pair = m_ShaderVarDofPairs[i];
		if(!pair.m_Duplicate)
		{
			frame.SetFloat(pair.m_Track, pair.m_Id, 0.f);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::Reset(const crCreatureFilter& skip)
{
	if(!skip.UseComposer())
	{
		const int numPairs = m_ShaderVarDofPairs.GetCount();
		for(int i=0; i<numPairs; ++i)
		{
			ShaderVarDofPair& pair = m_ShaderVarDofPairs[i];
			pair.m_Value = 0.f;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::DrawUpdate()
{
	if(m_ShaderGroup && m_ShaderGroupVar != grmsgvNONE)
	{
		const u32 numPairs = m_ShaderVarDofPairs.GetCount();
		if(numPairs > 1)
		{
			u32 numVals = (numPairs+3)/4;

			Vector4* vals = Alloca(Vector4, numVals);
			sysMemSet(vals, 0, sizeof(Vector4)*numVals);

			for(u32 i=0; i<numPairs; ++i)
			{
				const ShaderVarDofPair& pair = m_ShaderVarDofPairs[i];
				u32 idx = u32(pair.m_ProjectData&0xffffffff);

				Assert(idx < numPairs);
				if(idx < numPairs)
				{
					vals[idx>>2][idx&0x3] = pair.m_Value;
				}
			}
			m_ShaderGroup->SetVar(m_ShaderGroupVar, vals, numVals);
		}
		else if(numPairs)
		{
			m_ShaderGroup->SetVar(m_ShaderGroupVar, m_ShaderVarDofPairs[0].m_Value);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::AddShaderVarDofPair(u8 track, u16 id, u64 projectData)
{
	if(m_ShaderVarDofPairs.GetCapacity() == m_ShaderVarDofPairs.GetCount())
	{
		m_ShaderVarDofPairs.Grow();
		m_ShaderVarDofPairs.DeleteFast(m_ShaderVarDofPairs.GetCount()-1);
	}

	ShaderVarDofPair* newPair = NULL;
	u32 ti = (u32(track)<<16)|u32(id);

	const int numPairs = m_ShaderVarDofPairs.GetCount();
	for(int i=0; i<numPairs; ++i)
	{
		ShaderVarDofPair& pi = m_ShaderVarDofPairs[i];
		u32 tii = (u32(pi.m_Track)<<16)|u32(pi.m_Id);
		if(ti >= tii)
		{
			newPair = &m_ShaderVarDofPairs.Insert(i);

			bool duplicate = (ti == tii);
			newPair->m_Duplicate = duplicate;
			if(!duplicate)
			{
				m_NumUniqueDofs++;
			}
			
			break;
		}
	}
	if(!newPair)
	{
		newPair = &m_ShaderVarDofPairs.Append();
		newPair->m_Duplicate = false;
		m_NumUniqueDofs++;
	}

	newPair->m_Track = track;
	newPair->m_Id = id;
	newPair->m_ProjectData = projectData;
	newPair->m_Value = 0.f;

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentShaderVars::FastAddShaderVarDofPairs::FastAddShaderVarDofPairs(crCreatureComponentShaderVars& component, u32 numReserve)
: m_Component(&component)
{
	if(!m_Component->m_ShaderVarDofPairs.GetCapacity())
	{
		m_Component->m_ShaderVarDofPairs.Reserve(numReserve);
	}
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentShaderVars::FastAddShaderVarDofPairs::~FastAddShaderVarDofPairs()
{
	m_Component->SortShaderVarDofPairs();
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponentShaderVars::CalcSignature() const
{
	u32 signature = crCreatureComponent::CalcSignature();
	signature ^= m_ProjectData;

	if(m_ShaderVarDofPairs.GetCount() > 0)
	{
		signature = atDataHash((const char*)m_ShaderVarDofPairs.GetElements(), sizeof(ShaderVarDofPair)*m_ShaderVarDofPairs.GetCount());
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentShaderVars::SortShaderVarDofPairs()
{
	const int numPairs = m_ShaderVarDofPairs.GetCount();
	if(numPairs > 0)
	{
		m_NumUniqueDofs = numPairs;

		m_ShaderVarDofPairs.QSort(0, -1, ShaderVarDofPair::CompareFunc);

		for(int i=1; i<numPairs; i++)
		{
			ShaderVarDofPair& pair0 = m_ShaderVarDofPairs[i-1];
			ShaderVarDofPair& pair1 = m_ShaderVarDofPairs[i];

			bool duplicate = (pair0.m_Id == pair1.m_Id);
			pair0.m_Duplicate = duplicate;
			if(duplicate)
			{
				m_NumUniqueDofs--;
			}
		}
		m_ShaderVarDofPairs.Top().m_Duplicate = false;
	}

	CalcAndSetSignature();
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentShaderVars::ShaderVarDofPair::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crCreatureComponentShaderVars::ShaderVarDofPair)
	SSTRUCT_FIELD(crCreatureComponentShaderVars::ShaderVarDofPair, m_Duplicate)
	SSTRUCT_FIELD(crCreatureComponentShaderVars::ShaderVarDofPair, m_Track)
	SSTRUCT_FIELD(crCreatureComponentShaderVars::ShaderVarDofPair, m_Id)
	SSTRUCT_FIELD(crCreatureComponentShaderVars::ShaderVarDofPair, m_Value)
	SSTRUCT_FIELD(crCreatureComponentShaderVars::ShaderVarDofPair, m_ProjectData)
	SSTRUCT_END(crCreatureComponentShaderVars::ShaderVarDofPair)
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
