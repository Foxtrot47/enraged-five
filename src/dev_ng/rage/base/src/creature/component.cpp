//
// creature/component.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "component.h"

#include "componentblendshapes.h"
#include "componentextradofs.h"
#include "componentmover.h"
#include "componentcamera.h"
#include "componentphysical.h"
#include "componentshadervars.h"
#include "componentskeleton.h"
#include "componenttelemetry.h"
#include "creature.h"

#include "cranimation/frameiterators.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::crCreatureComponent(eCreatureComponentType creatureComponentType)
: m_Creature(NULL)
, m_Factory(NULL)
, m_Signature(0)
{
	Assert(creatureComponentType != kCreatureComponentTypeNone);
	m_Type = creatureComponentType;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::crCreatureComponent(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::~crCreatureComponent()
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crCreatureComponent::eCreatureComponentType);
void crCreatureComponent::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crCreatureComponent);
	STRUCT_FIELD(m_Type);
	STRUCT_FIELD(m_Creature);
	STRUCT_FIELD(m_Factory);
	STRUCT_FIELD(m_Signature);
	STRUCT_END();
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Place(crCreatureComponent *that, eCreatureComponentType creatureComponentType)
{
	const CreatureComponentTypeInfo* info = FindCreatureComponentTypeInfo(creatureComponentType);
	Assert(info);

	CreatureComponentPlaceFn* placeFn = info->GetPlaceFn();
	placeFn(that);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Place(crCreatureComponent *that, datResource& rsc)
{
	const CreatureComponentTypeInfo* info = FindCreatureComponentTypeInfo(that->GetType());
	Assert(info);

	CreatureComponentRscPlaceFn* rscPlaceFn = info->GetRscPlaceFn();
	rscPlaceFn(rsc, that);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::VirtualConstructFromPtr(datResource& rsc, crCreatureComponent* base)
{
	Assert(base);
	base->Place(base, rsc);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::PreInit(crCreature& creature)
{
	m_Creature = &creature;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Shutdown()
{
	m_Type = kCreatureComponentTypeNone;
	m_Creature = NULL;
	m_Signature = 0;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent* crCreatureComponent::Allocate(eCreatureComponentType creatureComponentType)
{
	const CreatureComponentTypeInfo* info = FindCreatureComponentTypeInfo(creatureComponentType);
	if(info)
	{
		CreatureComponentAllocateFn* allocateFn = info->GetAllocateFn();
		return allocateFn();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Refresh()
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Finalize(float)
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Pose(const crFrame&, crFrameFilter*, const crCreatureFilter&)
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::InversePose(crFrame&, crFrameFilter*, const crCreatureFilter&) const
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::DofIterator::DofIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::DofIterator::~DofIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

class DofIteratorIterator : public crFrameIterator<DofIteratorIterator>
{
public:
	DofIteratorIterator(const crFrame& frame, crCreatureComponent::DofIterator& dofIterator)
		: crFrameIterator<DofIteratorIterator>(frame)
		, m_DofIterator(&dofIterator)
	{
	}

	__forceinline void IterateDof(const crFrameData::Dof& dof, crFrame::Dof&, float)
	{
		m_DofIterator->IterateDof(dof.m_Track, dof.m_Id, dof.m_Type);
	}

protected:
	crCreatureComponent::DofIterator* m_DofIterator;
};

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::DofIterator::IterateFrame(const crFrame& frame)
{
	DofIteratorIterator it(frame, *this);
	it.Iterate(NULL, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::IterateDofs(crCreatureComponent::DofIterator&) const
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponent::GetNumDofs() const
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponent::InitDofs(const crFrameDataInitializerCreature&) const
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponent::HasDof(u8, u16) const
{
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Identity(crFrame&, crFrameFilter*, const crCreatureFilter&) const
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Reset(const crCreatureFilter&)
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::PostUpdate()
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::PreDraw(bool /*isVisible*/)
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::DrawUpdate()
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::SwapBuffers()
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Draw() const
{
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crCreatureComponent::DebugDraw() const
{
}

////////////////////////////////////////////////////////////////////////////////

const char* crCreatureComponent::GetTypeName() const
{
	const CreatureComponentTypeInfo* info = FindCreatureComponentTypeInfo(GetType());
	if(info)
	{
		return info->GetCreatureComponentName();
	}

	return "UNKNOWN_CREATURE_COMPONENT_TYPE";
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponent::GetPriority() const
{
	return sm_DefaultComponentPriority;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_CreatureComponentTypeInfos.Resize(kCreatureComponentTypeNum);
		for(int i=0; i<kCreatureComponentTypeNum; ++i)
		{
			sm_CreatureComponentTypeInfos[i] = NULL;
		}

		// register all built in creature component types
		crCreatureComponentSkeleton::InitClass();
        crCreatureComponentCamera::InitClass();
		crCreatureComponentMover::InitClass();
#if ENABLE_BLENDSHAPES
		crCreatureComponentBlendShapes::InitClassDerived();
#endif // ENABLE_BLENDSHAPES
		crCreatureComponentExtraDofs::InitClass();
		crCreatureComponentShaderVars::InitClass();
		crCreatureComponentTelemetry::InitClass();
		crCreatureComponentPhysical::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::ShutdownClass()
{
	sm_CreatureComponentTypeInfos.Reset();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::CreatureComponentTypeInfo::CreatureComponentTypeInfo(eCreatureComponentType creatureComponentType, const char* creatureComponentName, size_t size, CreatureComponentAllocateFn* allocateFn, CreatureComponentPlaceFn* placeFn, CreatureComponentRscPlaceFn* rscPlaceFn)
: m_CreatureComponentType(creatureComponentType)
, m_CreatureComponentName(creatureComponentName)
, m_Size(size)
, m_AllocateFn(allocateFn)
, m_PlaceFn(placeFn)
, m_RscPlaceFn(rscPlaceFn)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::CreatureComponentTypeInfo::~CreatureComponentTypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::CreatureComponentTypeInfo::Register() const
{
	crCreatureComponent::RegisterCreatureComponentTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::eCreatureComponentType crCreatureComponent::CreatureComponentTypeInfo::GetCreatureComponentType() const
{
	return m_CreatureComponentType;
}

////////////////////////////////////////////////////////////////////////////////

const char* crCreatureComponent::CreatureComponentTypeInfo::GetCreatureComponentName() const
{
	return m_CreatureComponentName;
}

////////////////////////////////////////////////////////////////////////////////

size_t crCreatureComponent::CreatureComponentTypeInfo::GetSize() const
{
	return m_Size;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::CreatureComponentAllocateFn* crCreatureComponent::CreatureComponentTypeInfo::GetAllocateFn() const
{
	return m_AllocateFn;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::CreatureComponentPlaceFn* crCreatureComponent::CreatureComponentTypeInfo::GetPlaceFn() const
{
	return m_PlaceFn;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::CreatureComponentRscPlaceFn* crCreatureComponent::CreatureComponentTypeInfo::GetRscPlaceFn() const
{
	return m_RscPlaceFn;
}

////////////////////////////////////////////////////////////////////////////////

const crCreatureComponent::CreatureComponentTypeInfo* crCreatureComponent::FindCreatureComponentTypeInfo(crCreatureComponent::eCreatureComponentType creatureComponentType)
{
	if(creatureComponentType < kCreatureComponentTypeNum)
	{
		Assert(creatureComponentType > kCreatureComponentTypeNone);
		Assert(sm_CreatureComponentTypeInfos.GetCount() > 0);
		Assert(creatureComponentType < sm_CreatureComponentTypeInfos.GetCount());

		return sm_CreatureComponentTypeInfos[creatureComponentType];
	}
	else
	{
		const int numCreatureComponents = sm_CreatureComponentTypeInfos.GetCount();
		for(int i=kCreatureComponentTypeNum; i<numCreatureComponents; ++i)
		{
			if(sm_CreatureComponentTypeInfos[i])
			{
				if(sm_CreatureComponentTypeInfos[i]->GetCreatureComponentType() == creatureComponentType)
				{
					return sm_CreatureComponentTypeInfos[i];
				}
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::Factory::Factory()
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::Factory::~Factory()
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent* crCreatureComponent::Factory::Allocate(crCreatureComponent::eCreatureComponentType creatureComponentType)
{
	crCreatureComponent* component = crCreatureComponent::Allocate(creatureComponentType);
	if(component)
	{
		component->m_Factory = this;
	}
	return component;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::Factory::Free(crCreatureComponent& component)
{
	delete &component;
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::FactoryPooled::FactoryPooled()
{
	m_Pools.Resize(kCreatureComponentTypeNum);
	for(int i=0; i<kCreatureComponentTypeNum; ++i)
	{
		m_Pools[i] = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent::FactoryPooled::~FactoryPooled()
{
	for(int i=0; i<kCreatureComponentTypeNum; ++i)
	{
		if(m_Pools[i])
		{
			delete m_Pools[i];
		}
		m_Pools[i] = NULL;
	}
	m_Pools.Reset();
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponent* crCreatureComponent::FactoryPooled::Allocate(crCreatureComponent::eCreatureComponentType creatureComponentType)
{
	if(creatureComponentType < m_Pools.GetCount())
	{
		if(m_Pools[creatureComponentType])
		{
			if(!m_Pools[creatureComponentType]->IsFull())
			{
				crCreatureComponent* component = static_cast<crCreatureComponent*>(m_Pools[creatureComponentType]->New());
				crCreatureComponent::Place(component, creatureComponentType);
				component->m_Factory = this;
				return component;
			}
			else
			{
				Warningf("crCreatureComponent::FactoryPooled::Allocate - component[%d] pool exhausted", int(creatureComponentType));
			}
		}
		else
		{
			Warningf("crCreatureComponent::FactoryPooled::Allocate - component[%d] pool never allocated", int(creatureComponentType));
		}
	}

	Warningf("crCreatureComponent::FactoryPooled::Allocate - component[%d] dynamically allocating", int(creatureComponentType));
	return crCreatureComponent::Allocate(creatureComponentType);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::FactoryPooled::Free(crCreatureComponent& component)
{
	Assert(component.m_Factory == this);
	Assert(m_Pools.GetCount() > component.GetType());
	Assert(m_Pools[component.GetType()]);

	eCreatureComponentType creatureComponentType = component.GetType();
	component.Shutdown();
	m_Pools[creatureComponentType]->Delete(&component);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::FactoryPooled::CreatePool(crCreatureComponent::eCreatureComponentType creatureComponentType, u16 size)
{
	Assert(m_Pools.GetCount() > creatureComponentType);
	Assert(!m_Pools[creatureComponentType]);
	Assert(crCreatureComponent::FindCreatureComponentTypeInfo(creatureComponentType));

	u32 elementSize = u32(crCreatureComponent::FindCreatureComponentTypeInfo(creatureComponentType)->GetSize());

#if COMMERCE_CONTAINER
	m_Pools[creatureComponentType] = rage_new atPoolBase(elementSize, size, true);
#else
	m_Pools[creatureComponentType] = rage_new atPoolBase(elementSize, size);
#endif
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::RegisterCreatureComponentTypeInfo(const crCreatureComponent::CreatureComponentTypeInfo& info)
{
	Assert(sm_CreatureComponentTypeInfos.GetCount() > 0);

	if(info.GetCreatureComponentType() < kCreatureComponentTypeNum)
	{
		Assert(info.GetCreatureComponentType() < sm_CreatureComponentTypeInfos.GetCount());
		Assert(sm_CreatureComponentTypeInfos[info.GetCreatureComponentType()] == NULL);

		sm_CreatureComponentTypeInfos[info.GetCreatureComponentType()] = &info;
	}
	else
	{
		Assert(info.GetCreatureComponentType() >= kCreatureComponentTypeCustom);
		Assert(!FindCreatureComponentTypeInfo(info.GetCreatureComponentType()));

		sm_CreatureComponentTypeInfos.Grow() = &info;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponent::CalcAndSetSignature()
{
	m_Signature = CalcSignature();
	if(m_Creature)
	{
		m_Creature->CalcAndSetSignature();
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crCreatureComponent::CalcSignature() const
{
	return u32(GetType());
}

////////////////////////////////////////////////////////////////////////////////

atArray<const crCreatureComponent::CreatureComponentTypeInfo*> crCreatureComponent::sm_CreatureComponentTypeInfos;

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponent::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
