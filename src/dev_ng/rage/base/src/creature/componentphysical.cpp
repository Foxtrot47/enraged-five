//
// creature/componentphysical.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "componentphysical.h"

#include "creature.h"

#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "vectormath/classes.h"

namespace rage
{

atPoolBase* crCreatureComponentPhysical::sm_Pool = NULL;

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentPhysical::crCreatureComponentPhysical()
: crCreatureComponent(kCreatureComponentTypePhysical)
, m_Motions(NULL)
, m_Count(0)
, m_Reserve(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentPhysical::~crCreatureComponentPhysical()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentPhysical::crCreatureComponentPhysical(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentPhysical, crCreatureComponent::kCreatureComponentTypePhysical);

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentPhysical::InitPool(u32 elemSize, u32 numElem)
{
#if COMMERCE_CONTAINER
	sm_Pool = rage_new atPoolBase(elemSize*sizeof(Motion), numElem, true);
#else
	sm_Pool = rage_new atPoolBase(elemSize*sizeof(Motion), numElem);
#endif
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentPhysical::ShutdownPool()
{
	delete sm_Pool;
	sm_Pool = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentPhysical::Init(crCreature& creature)
{
	PreInit(creature);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentPhysical::Shutdown()
{
	RemoveMotions();

	crCreatureComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentPhysical::AddMotions(const atArray<crMotionDescription>& motions)
{
	const crSkeletonData& skelData = GetCreature().GetSkeleton()->GetSkeletonData();

	u32 count = motions.GetCount();

	// resize motion buffer if needed
	u32 reserve = m_Count + count;
	if(reserve > m_Reserve)
	{
		Motion* buffer = NULL;
		u32 size = reserve * sizeof(Motion);
		if(sm_Pool && sm_Pool->GetElemSize() >= size && !sm_Pool->IsFull())
		{
			buffer = static_cast<Motion*>(sm_Pool->New(size));
			Assign(m_Reserve, sm_Pool->GetElemSize() / sizeof(Motion));
		}
		else
		{
			buffer = rage_new Motion[reserve];
			Assign(m_Reserve, reserve);
		}
		memcpy(buffer, m_Motions, m_Count * sizeof(Motion));
		if(sm_Pool && sm_Pool->IsInPool(m_Motions))
		{
			sm_Pool->Delete(m_Motions);
		}
		else
		{
			delete [] m_Motions;
		}
		m_Motions = buffer;
	}

	// append new motions
	for(u32 i=0; i < count; i++)
	{
		int boneIdx;
		const crMotionDescription& descr = motions[i];
		if(!HasMotion(descr.GetBoneId()) && skelData.ConvertBoneIdToIndex(descr.GetBoneId(), boneIdx))
		{	
			const crBoneData* boneData = skelData.GetBoneData(boneIdx);
			const crBoneData* parentData = boneData->GetParent();
			if(boneData && parentData)
			{
				Motion& motion = m_Motions[m_Count++];
				FastAssert(m_Count <= m_Reserve);
				motion.m_Reset = true;
				Assign(motion.m_ParentIdx, parentData->GetIndex());
				Assign(motion.m_ChildIdx, boneData->GetIndex());
				motion.m_OffsetLocal.SetPosition(boneData->GetDefaultTranslation());
				motion.m_OffsetLocal.SetRotation(boneData->GetDefaultRotation());
				motion.m_Descr = descr;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentPhysical::RemoveMotions()
{
	if(sm_Pool && sm_Pool->IsInPool(m_Motions))
	{
		sm_Pool->Delete(m_Motions);
	}
	else
	{
		delete [] m_Motions;
	}
	m_Motions = NULL;
	m_Count = m_Reserve = 0;
}

////////////////////////////////////////////////////////////////////////////////

bool crCreatureComponentPhysical::HasMotion(u16 boneId) const
{
	u32 count = m_Count;
	for(u32 i=0; i < count; ++i)
	{
		const Motion& motion = m_Motions[i];
		if(motion.m_Descr.GetBoneId() == boneId)
		{
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentPhysical::ResetMotions()
{
	u32 count = m_Count;
	for(u32 i=0; i < count; ++i)
	{
		m_Motions[i].m_Reset = true;
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crMotionDescription::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crMotionDescription);
	STRUCT_FIELD(m_Linear.m_Strength);
	STRUCT_FIELD(m_Linear.m_Damping);
	STRUCT_FIELD(m_Linear.m_MinConstraint);
	STRUCT_FIELD(m_Linear.m_MaxConstraint);
	STRUCT_FIELD(m_Angular.m_Strength);
	STRUCT_FIELD(m_Angular.m_Damping);
	STRUCT_FIELD(m_Angular.m_MinConstraint);
	STRUCT_FIELD(m_Angular.m_MaxConstraint);
	STRUCT_FIELD(m_Direction);
	STRUCT_FIELD(m_Gravity);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

}; //namespace rage
