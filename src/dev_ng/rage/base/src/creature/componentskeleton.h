//
// creature/componentskeleton.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTSKELETON_H
#define CREATURE_COMPONENTSKELETON_H

#include "component.h"

namespace rage
{

class crFrame;
class crSkeleton;


// PURPOSE: Creature skeleton component
class crCreatureComponentSkeleton : public crCreatureComponent
{
public:

	// PURPOSE: Default constructor
	crCreatureComponentSkeleton();

	// PURPOSE: Initializing constructor
	// PARAMS: creature - reference to creature this component is part of
	// skeleton - reference to the skeleton instance to use
	crCreatureComponentSkeleton(crCreature& creature, crSkeleton& skeleton);

	// PURPOSE: Resource constructor
	crCreatureComponentSkeleton(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentSkeleton();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentSkeleton);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: creature - reference to creature this component is part of
	// skeleton - reference to the skeleton to use
	void Init(crCreature& creature, crSkeleton& skeleton);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();


	// PURPOSE: Pose override
	virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip);

	// PURPOSE: Inverse Pose override
	virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

	// PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

	// PURPOSE: Identity override
	virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter& skip) const;

	// PURPOSE: Reset override
	virtual void Reset(const crCreatureFilter& skip);


	// PURPOSE: Set new skeleton pointer
	void SetSkeleton(crSkeleton& skeleton);

	// PURPOSE: Get skeleton pointer
	const crSkeleton* GetSkeleton() const;
	crSkeleton* GetSkeleton();

	// PURPOSE: Suppress reset operation on skeletons (don't reset skeleton, let previous values persist)
	// PARAMS: suppressReset - don't reset local skeleton pose, let previous values persist
	void SetSuppressReset(bool suppressReset);

	// PURPOSE: Suppress reset operation on skeletons (don't reset skeleton, let previous values persist)
	// RETURNS: true - reset is suppressed, previous values persist, false - reset is not suppressed
	bool GetSuppressReset() const;

protected:

	// PURPOSE: Signature override
	virtual u32 CalcSignature() const;

protected:

	datRef<crSkeleton> m_Skeleton;

	bool m_SuppressReset;
};

////////////////////////////////////////////////////////////////////////////////

inline const crSkeleton* crCreatureComponentSkeleton::GetSkeleton() const
{
	return m_Skeleton;
}

////////////////////////////////////////////////////////////////////////////////

inline crSkeleton* crCreatureComponentSkeleton::GetSkeleton()
{
	return m_Skeleton;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crCreatureComponentSkeleton::GetSuppressReset() const
{
	return m_SuppressReset;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CREATURE_COMPONENTSKELETON_H
