//
// creature/componentmover.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREATURE_COMPONENTMOVER_H
#define CREATURE_COMPONENTMOVER_H

#include "component.h"

#include "vectormath/mat34v.h"

namespace rage
{

class crFrame;

// PURPOSE: Creature mover component
class crCreatureComponentMover : public crCreatureComponent
{
public:

	// PURPOSE: Default constructor
	crCreatureComponentMover();

	// PURPOSE: Initializing constructor
	// PARAMS: creature - reference to creature this component is part of
	// moverMtx - reference to the matrix to use as a mover
	crCreatureComponentMover(crCreature& creature, Mat34V& moverMtx);

	// PURPOSE: Resource constructor
	crCreatureComponentMover(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentMover();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentMover);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT


	// PURPOSE: Initialization
	// PARAMS: creature - reference to creature this component is part of
	// moverMtx - reference to the matrix to use as a mover
	void Init(crCreature& creature, Mat34V& moverMtx);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();


	// PURPOSE: Finalize override
	virtual void Finalize(float deltaTime);

	// PURPOSE: Pose override
	virtual void Pose(const crFrame& frame, crFrameFilter* filter, const crCreatureFilter&);

	// PURPOSE: Inverse Pose override
	virtual void InversePose(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Iterate DOFs override
	virtual void IterateDofs(DofIterator&) const;

	// PURPOSE: Check if creature has a particular DOF
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Number of DOFs override
	virtual u32 GetNumDofs() const;

	// PURPOSE: InitDofs override
	virtual bool InitDofs(const crFrameDataInitializerCreature& initializer) const;

	// PURPOSE: Identity override
	virtual void Identity(crFrame& frame, crFrameFilter* filter, const crCreatureFilter&) const;

	// PURPOSE: Reset override
	virtual void Reset(const crCreatureFilter&);


	// PURPOSE: Priority override
	virtual u32 GetPriority() const;

	// PURPOSE : Debug drawing
#if !__FINAL
	virtual void DebugDraw() const;
#endif // !__FINAL

	// PURPOSE: Overwrite the last mover matrix
	// PARAMS: mtx - new last mover matrix
	void SetLastMoverMtx(Mat34V_In mtx);

	// PURPOSE: Retrieve the last mover matrix
	// RETURNS: last mover matrix
	const Mat34V& GetLastMoverMtx() const;


	// PURPOSE: Suppress reset operation on mover (don't reset let previous value persist)
	// PARAMS: suppressReset - don't reset mover matrix to last transform, let previous value persist
	void SetSuppressReset(bool suppressReset);

	// PURPOSE: Suppress reset operation on mover (don't reset let previous value persist)
	// RETURNS: true - reset is suppressed, previous value persists, false - reset is not suppressed
	bool GetSuppressReset() const;

protected:

	// PURPOSE: Signature override
	virtual u32 CalcSignature() const;

	Mat34V m_LastMoverMtx;
	datRef<Mat34V> m_MoverMtx;
	u16 m_MoverId;
	bool m_SuppressReset;
};


////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentMover::SetLastMoverMtx(Mat34V_In mtx)
{
	m_LastMoverMtx = mtx;
}

////////////////////////////////////////////////////////////////////////////////

inline const Mat34V& crCreatureComponentMover::GetLastMoverMtx() const
{
	return m_LastMoverMtx;
}

////////////////////////////////////////////////////////////////////////////////

inline void crCreatureComponentMover::SetSuppressReset(bool suppressReset)
{
	m_SuppressReset = suppressReset;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crCreatureComponentMover::GetSuppressReset() const
{
	return m_SuppressReset;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREATURE_COMPONENTMOVER_H
