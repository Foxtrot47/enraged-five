// 
// init/config.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef INIT_INI_H 
#define INIT_INI_H 

#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"
#include "data/base.h"
#include "vector/color32.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

namespace rage 
{

class fiStream;
class initIniItem;
class initIniSection;

//#############################################################################

// PURPOSE: Rage implementation of the INI File Format roughly based on this
//  format specification:  http://www.cloanto.com/specs/ini.html.
//
//  Unlike traditional INI files, the array of possible section names and their
//    keys is not known ahead of time.  The values are interpreted on-demand
//    using the typed retrieval function (string, bool, int, float, Vector3,
//    Vector4, and Color32).
//
//  Section Names
//    - Enclosed in square brackets
//    - Must begin at the beginning of a line.
//    - Case-insensitive
//    - Cannot contain whitespace (space, tab, newline, etc.)
//    - If a section is defined multiple times, the last one in the file takes
//      precedence.
//  Key Names
//    - Must begin at the beginning of a line.
//    - Case-insensitive
//    - Cannot contain whitespace (space, tab, newline, etc.)
//    - If a key is defined multiple times in the same section, the last one in 
//      the file takes precedence.
//  Key Values
//    - Must occur on the same line as a Key Name, separated by an equal sign (=)
//      and any number of whitespace characters on either side of the equal sign.
//    - A Value can be blank as long as the equal sign (=) is present after the Key
//      Name.
//    - The value can be encapsulated in quotation marks.
//    - Use commas (,) to separate multiple values.  This is used when interpreting
//      the value as a Vector3, Vecto4, or Color32 data type.
//    - Strings:  May optionally be enclosed in quotation marks (").  String values
//      beginning or ending with spaces, or containing commas or semicolons, must be 
//      enclosed in quotes.  Quote and backslash characters (\), as well as binary
//      characters appearing inside strings must be encoded using the escape sequences
//      described below.
//    - Strings:  Can contain certain escape sequences (backslashed followed by a lowercase 
//      letter) which will be interpreted as a single character.  All other escape 
//      sequences are ignored.
//        Escape Sequence   Represents
//        ---------------------------------------------------------------------
//        \a                Bell (alert)
//        \b                Backspace
//        \f                Form feed
//        \n                New line
//        \r                Carriage return
//        \t                Horizontal tab
//        \v                Vertical tab
//        \"                Double quotation mark
//        \\                Backslash
//        \o000             ASCII character in octal notation (up to 3 digits)
//        \xhh              ASCII character in hexadecimal notation (up to 2 letters and/or digits)
//  Comments
//    - Begins with a semi-colon (;) and goes to the end of the line.
//    - Can occur after a Section Name
//    - Can occur after a Key Value
//    - Can occur on any line that is otherwise blank.
//
//  Example:
//    [SectionName]
//    
//     keyname=value
//
//     ;comment
//
//     keyname=value, value, value ;comment
class initIni : public datBase 
{
public:
    initIni();
    initIni( const char* configFile );
    ~initIni();

    // PURPOSE: Gets a section by its name 
    // PARAMS: 
    //  name - the section name (case-insensitive)
    // RETURNS: NULL if the section does not exist
    initIniSection* GetSection( const char *name );

    // PURPOSE: Get config section map (e.g. for iterating over all sections)
    // PARAMS: The section map
    atMap<atString, initIniSection*>& GetSections();

    // PURPOSE: Loads a new config file and parses it
    // PARAMS:
    //  configFile - the full path of the file to load
    void Load( const char *configFile );

    // PURPOSE: Saves the config file
    // PARAMS:
    //  configFile - the full path of the file to save
    void Save( const char *configFile );

    // PURPOSE: Clears all data
    void Clear();

    // PURPOSE: Adds the section and takes over ownership of the memory.  
    // PARAMS:
    //  pSection - the section to add
    // NOTES: If a section with the same name already exists, it will be deleted and replaced.
    void Add( initIniSection *pSection );

    // PURPOSE: Removes the section
    // PARAMS:
    //  name - the name of the section to remove (case-insensitive)
    void Remove( const char *name );

private:
    // helper function for parsing
    bool ReadLine( fiStream *pStream, char *pDest, int iDestLen );
    void ResolveEscapeSequences( atString &s );
    void ResolveEscapeSequence( atString &s );
    void ResolveASCIIEscapeSequence( atString &s, int base );

    // section map
    atMap<atString, initIniSection *> m_sections;
};

inline atMap<atString, initIniSection *>& initIni::GetSections() 
{
    return m_sections;
}

//#############################################################################

class initIniSection : public datBase
{
public:
    initIniSection( const atString &name );
    ~initIniSection();

    // PURPOSE: Get the name of this section
    // RETURNS: The section name
    const atString& GetName() const;

    // PURPOSE: Gets an item by its name 
    // PARAMS: 
    //  key - the item name (case-insensitive)
    // RETURNS: NULL if the item does not exist
    initIniItem* GetItem( const char *key );

    // PURPOSE: Get the symbol map (e.g. for iterating over all symbols)   
    // RETURNS: The symbol map
    atMap<atString, initIniItem *>& GetItems();

    // PURPOSE: Adds a new initIniItem to this section, taking ownership of the memory.
    // PARAMS:
    //  pItem - the item to add.
    // NOTES: If a KeyValue with the same name already exists, it will be deleted and replaced.
    void Add( initIniItem *pItem );

    // PURPOSE: Removes the initIniItem from this section.
    // PARAMS:
    //  key - the name of the item to remove (case-insensitive)
    void Remove( const char *key );

    // PURPOSE: Removes and deletes all initConfigKeyValues
    void Clear();

    // PURPOSE: Find string value
    // PARAMS:
    //  key - the name of the item
    //  s - return by reference the string value of the item
    // RETURNS: true if the entry was found, otherwise false
    bool FindString( const char *key, atString &s );

    // PURPOSE: Find boolean value
    // PARAMS:
    //  key - the name of the item
    //  b - return by reference the boolean value of the item
    // RETURNS: true if the entry was found, otherwise false
    // NOTES: A value of Yes/yes/YES/true/True/TRUE leads to true, all other values leads to false.
    bool FindBool( const char *key, bool &b );

    // PURPOSE: Find float value
    // PARAMS:
    //  key - the name of the item
    //  f - return by reference the float value of the item
    // RETURNS: true if the entry was found, otherwise false
    // NOTES: Value is parsed using atof().
    bool FindFloat( const char *key, float &f );

    // PURPOSE: Find int value
    // PARAMS:
    //  key - the name of the item
    //  i - return by reference the int value of the item
    // RETURNS: true if the entry was found, otherwise false
    // NOTES: Value is parsed using atoi().
    bool FindInt( const char *key, int &i );

    // PURPOSE: Find Vector3 value
    // PARAMS:
    //  key - the name of the item
    //  v3 - return by reference the Vector3 value of the item
    // RETURNS: true if the entry was found, otherwise false
    // NOTES: Values are parsed using atof().
    bool FindVector3( const char *key, Vector3 &v3 );

    // PURPOSE: Find Vector4 value
    // PARAMS:
    //  key - the name of the item
    //  v4 - return by reference the Vector4 value of the item
    // RETURNS: true if the entry was found, otherwise false
    // NOTES: Values are parsed using atof().
    bool FindVector4( const char *key, Vector4 &v4 );

    // PURPOSE: Find Color32 value
    // PARAMS:
    //  key - the name of the item
    //  c32 - return by reference the Color32 value of the item
    // RETURNS: true if the entry was found, otherwise false
    // NOTES: Values are parsed using atoi().  Range 0-255.  RGBA
    bool FindColor32( const char *key, Color32 &c32 );

private:
    atString m_name;
    atMap<atString, initIniItem *> m_items;
};

inline const atString& initIniSection::GetName() const
{
    return m_name;
}

inline atMap<atString, initIniItem *>& initIniSection::GetItems() 
{
    return m_items;
}

//#############################################################################

class initIniItem : public datBase
{
public:
    initIniItem( const char *key, const char *value );
    ~initIniItem();

    // PURPOSE: Get the name of this key
    // RETURNS: The key name
    const atString& GetKey() const;

    // PURPOSE: Get the value of this key
    // RETURNS: The value
    const atString& GetValue() const;

    // PURPOSE: Get string value
    // PARAMS:
    //  s - return by reference the string value of the item
    // RETURNS: true
    bool GetString( atString &s );

    // PURPOSE: Set the string value
    // PARAMS:
    //  s - the string
    void SetString( const char *s );

    // PURPOSE: Get boolean value
    // PARAMS:
    //  b - return by reference the boolean value of the item
    // RETURNS: true
    // NOTES: A value of "Yes" and "TRUE" (case-insensitive) lead to true, all other values leads to false.
    bool GetBool( bool &b );

    // PURPOSE: Set the bool value
    // PARAMS:
    //  b - the boolean
    void SetBool( bool b );

    // PURPOSE: Get float value
    // PARAMS:
    //  f - return by reference the float value of the item
    // RETURNS: true if the value could be converted to a float, otherwise false
    // NOTES: Value is parsed using atof().
    bool GetFloat( float &f );

    // PURPOSE: Set the float value
    // PARAMS:
    //  f - the float
    void SetFloat( float f );

    // PURPOSE: Get int value
    // PARAMS:
    //  i - return by reference the int value of the item
    // RETURNS: true if the value could be converted to an integer, otherwise false
    // NOTES: Value is parsed using atoi().
    bool GetInt( int &i );

    // PURPOSE: Set the int value
    // PARAMS:
    //  i - the int
    void SetInt( int i );

    // PURPOSE: Get Vector3 value
    // PARAMS:
    //  v3 - return by reference the Vector3 value of the item
    // RETURNS: true if the value could be converted to a Vector3, otherwise false
    // NOTES: Comma-separated values are parsed using atof().
    bool GetVector3( Vector3 &v3 );

    // PURPOSE: Set the Vector3 value
    // PARAMS:
    //  v3 - the Vector3
    void SetVector3( const Vector3 &v3 );

    // PURPOSE: Get Vector4 value
    // PARAMS:
    //  v4 - return by reference the Vector4 value of the item
    // RETURNS: true if the value could be converted to a Vector4, otherwise false
    // NOTES: Comma-separated values are parsed using atof().
    bool GetVector4( Vector4 &v4 );

    // PURPOSE: Set the Vector4 value
    // PARAMS:
    //  v4 - the Vector4
    void SetVector4( const Vector4 &v4 );

    // PURPOSE: Get Color32 value
    // PARAMS:
    // RETURNS: true if the value could be converted to a Color32, otherwise false
    // RETURNS: true if the entry was found, otherwise false
    // NOTES: Comma-separated values are parsed using atoi().  Range 0-255.  RGBA
    bool GetColor32( Color32 &c32 );

    // PURPOSE: Set the Color32 value
    // PARAMS:
    //  c32 - the Color32
    void SetColor32( const Color32 &c32 );

private:
    atString m_key;
    atString m_value;
};

inline const atString& initIniItem::GetKey() const
{
    return m_key;
}

inline const atString& initIniItem::GetValue() const
{
    return m_value;
}

//#############################################################################

} // namespace rage

#endif // INIT_INI_H 
