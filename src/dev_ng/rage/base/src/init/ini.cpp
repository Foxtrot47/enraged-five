// 
// init/config.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "ini.h"
#include "file/asset.h"
#include "math/amath.h"
#include "system/exec.h"

namespace rage {

//#############################################################################

initIni::initIni()
{
    
}

initIni::initIni( const char* configFile )
{
    Load( configFile );
}

initIni::~initIni() 
{
    Clear();
}

initIniSection* initIni::GetSection( const char *name ) 
{
    atString lowercaseName( name );
    lowercaseName.Lowercase();

    initIniSection **ppSection = m_sections.Access( lowercaseName );
    if ( ppSection != NULL )
    {
        return *ppSection;
    }

    return NULL;
}

void initIni::Load( const char *configFile )
{
    Clear();

    fiStream *pStream = ASSET.Open( configFile, "" );
    if ( pStream == NULL )
    {
        Errorf( "initIni: Cannot open input file '%s'.", configFile );
        return;
    }

    initIniSection *pSection = NULL;

    int lineNumber = 0;
    char buf[1024];
    while ( ReadLine( pStream, buf, sizeof(buf) ) )
    {
        ++lineNumber;

        atString line( buf );

        atString trimmedLine( line );
        trimmedLine.Trim();

        int indexOf = line.IndexOf( '[' );
        if ( trimmedLine.StartsWith( "[" ) && (indexOf > 0) )
        {
            Errorf( "initIni(%d): Section names must start at the beginning of a line:  %s.", lineNumber, line.c_str() );
            continue;
        }

        // look for a new section
        if ( indexOf == 0 )
        {            
            // trim off comments
            indexOf = line.IndexOf( ";" );
            if ( indexOf != -1 )
            {
                line.Truncate( indexOf );
            }

            // trim off whitespace
            line.Trim();

            // uh-oh
            if ( !line.EndsWith( "]" ) )
            {
                Errorf( "initIni(%d): Section names must end with ']':  %s.", lineNumber, line.c_str() );
                continue;
            }

            // get the section name
            atString name;
            name.Set( line, 1, line.GetLength() - 2 );

            bool containsSpaces = false;
            for ( int i = 0; i < name.GetLength(); ++i )
            {
                if ( isspace( name[i] ) )
                {
                    containsSpaces = true;
                    break;
                }
            }

            // uh-oh
            if ( containsSpaces )
            {
                Errorf( "initIni(%d): Section names cannot contain spaces:  %s.", lineNumber, line.c_str() );
                continue;
            }

            atString lowercaseName = name;
            lowercaseName.Lowercase();

            // retrieve or create the new section
            initIniSection **ppSection = m_sections.Access( lowercaseName );
            if ( ppSection != NULL )
            {
                Warningf( "initIni(%d): Overwritting Section '%s'.", lineNumber, name.c_str() );
            }
            else
            {
                Displayf( "initIni(%d): Section '%s'.", lineNumber, name.c_str() );
            }

            pSection = rage_new initIniSection( name );
            Add( pSection );
            continue;
        }

        // ignore blank lines and comments
        if ( (trimmedLine.GetLength() == 0 ) || trimmedLine.StartsWith( ";" ) )
        {
            continue;
        }
        
        if ( pSection == NULL )
        {
            Warningf( "initIni(%d): Ignoring this line.  We are not in a section or the section name was malformed:  %s.", lineNumber, line.c_str() );
            continue;
        }

        // no '='
        if ( line.IndexOf( "=" ) == -1 )
        {
            Errorf( "initIni(%d): KeyValues must contain '=':  %s.", lineNumber, line.c_str() );
            continue;
        }

        // Key name must begin at the beginning of the line
        if ( (line.GetLength() > 0) && isspace( line[0] ) )
        {
            Errorf( "initIni(%d): Key names must begin at the beginning of a line:  %s.", lineNumber, line.c_str() );
            continue;
        }

        // see if we have a key/value pair
        atString key;
        atString value;

        line.Split( key, value, '=' );
        key.Trim();
        value.Trim();

        bool containsSpaces = false;
        for ( int i = 0; i < key.GetLength(); ++i )
        {
            if ( isspace( key[i] ) )
            {
                containsSpaces = true;
                break;
            }
        }

        // uh-oh
        if ( containsSpaces )
        {
            Errorf( "initIni(%d): Key names cannot contain spaces:  %s.", lineNumber, line.c_str() );
            continue;
        }

        // uh-oh
        if ( key.IndexOf( ";" ) != -1 )
        {
            Errorf( "initIni(%d): Key names cannot contain comments:  %s.", lineNumber, line.c_str() );
            continue;
        }

        // check for values encapsulated by quotation marks
        int startIndex = -1;
        if ( value.StartsWith( "\"" ) )
        {
            for ( int i = 1; i < value.GetLength(); ++i )
            {
                if ( (value[i] == '\"') && (value[i - 1] != '\\') )
                {
                    startIndex = i;
                    break;
                }
            }

            // un-oh
            if ( startIndex == -1 )
            {
                Errorf( "initIni(%d): Value is missing end quote:  %s.", lineNumber, line.c_str() );
                continue;
            }
        }

        // trim off comments from the value (outside of the quotation marks, if any)
        indexOf = value.IndexOf( ";", startIndex );
        if ( indexOf != -1 )
        {
            value.Truncate( indexOf );
            
            // trim off whitespace
            value.Trim();
        }

        // handle escape characters
        ResolveEscapeSequences( value );

        atString lowercaseKey = key;
        lowercaseKey.Lowercase();

        initIniItem **ppItem = pSection->GetItems().Access( lowercaseKey );
        if ( ppItem != NULL )
        {
            Warningf( "initIni(%d): Overwriting Key '%s' with Value '%s'.", lineNumber, key.c_str(), value.c_str() );
        }
        else
        {
            Displayf( "initIni(%d): KeyValue '%s' = '%s'.", lineNumber, key.c_str(), value.c_str() );
        }

        // finally, add the value to the section
        pSection->Add( rage_new initIniItem( key, value ) );        
    }

    pStream->Close();
}

void initIni::Save( const char *configFile )
{
    fiStream *pStream = ASSET.Create( configFile, "" );
    if ( pStream == NULL )
    {
        Errorf( "initIni: Cannot open output file '%s'.", configFile );
        return;
    }

    atMap<atString, initIniSection *>::Iterator sectionIter = m_sections.CreateIterator();
    for ( sectionIter.Start(); !sectionIter.AtEnd(); sectionIter.Next() ) 
    {
        fprintf( pStream, "[%s]\r\n", sectionIter.GetData()->GetName().c_str() );

        atMap<atString, initIniItem *>::Iterator symbolIter = sectionIter.GetData()->GetItems().CreateIterator();
        for ( symbolIter.Start(); !symbolIter.AtEnd(); symbolIter.Next() )
        {
            fprintf( pStream, "%s = %s\r\n", symbolIter.GetData()->GetKey().c_str(), symbolIter.GetData()->GetValue().c_str() );
        }

        fprintf( pStream, "\r\n" );
    }

    pStream->Close();
}

void initIni::Clear()
{
    atMap<atString, initIniSection *>::Iterator i = m_sections.CreateIterator();
    for ( i.Start(); !i.AtEnd(); i.Next() ) 
    {
        delete i.GetData();
    }

    m_sections.Kill();
}

void initIni::Add( initIniSection *pSection )
{
    atString lowercaseName = pSection->GetName();
    lowercaseName.Lowercase();

    initIniSection **ppSection = m_sections.Access( lowercaseName );
    if ( ppSection != NULL )
    {
        if ( *ppSection == pSection )
        {
            // already added
            return;
        }

        delete *ppSection;
    }

    m_sections[lowercaseName] = pSection;
}

void initIni::Remove( const char *name )
{
    atString lowercaseName(name);
    lowercaseName.Lowercase();

    initIniSection **ppSection = m_sections.Access( lowercaseName );
    if ( ppSection != NULL )
    {
        delete *ppSection;
    }

    m_sections.Delete( lowercaseName );
}

bool initIni::ReadLine( fiStream* pStream, char* pDest, s32 iDestLen )
{
    bool bStarted = false;

    s32 index = 0;
    while ( pStream->ReadByte( &pDest[index], 1 ) )
    {
        if ( !bStarted && (pDest[index] != '\n') && (pDest[index] != '\r') )
        {
            bStarted = true;
        }

        if ( bStarted )
        {
            if ( (pDest[index] == '\n') || (pDest[index] == '\r') )
            {
                break;
            }

            index++;

            if ( index == iDestLen )
            {
                break;
            }
        }
    }

    if ( index == 0 )
    {
        return false;
    }

    pDest[index] = '\0';

    return true;
}

void initIni::ResolveEscapeSequences( atString &s )
{
    atArray<atString> split;
    s.Split( split, "\\\\" );
    if ( split.GetCount() <= 1 )
    {
        ResolveEscapeSequence( s );
        return;
    }

    s.Clear();

    if ( split[0].GetLength() > 0 )
    {
        s += split[0];
    }

    for ( int i = 1; i < split.GetCount(); ++i )
    {
        s += "\\";
        ResolveEscapeSequence( split[i] );
        s += split[i];
    }
}

void initIni::ResolveEscapeSequence( atString &s )
{
    atArray<atString> split;
    s.Split( split, '\\' );
    if ( split.GetCount() <= 1 )
    {        
        return;
    }

    s.Clear();

    if ( split[0].GetLength() > 0 )
    {
        s += split[0];
    }

    for ( int i = 1; i < split.GetCount(); ++i )
    {
        if ( split[i].GetLength() > 0 )
        {
            switch ( split[i][0] )
            {
            case 'a':
                {
                    split[i][0] = '\a';
                }
                break;
            case 'b':
                {
                    split[i][0] = '\b';
                }
                break;
            case 'f':
                {
                    split[i][0] = '\f';
                }
                break;
            case 'n':
                {
                    split[i][0] = '\n';
                }
                break;
            case 'r':
                {
                    split[i][0] = '\r';
                }
                break;
            case 't':
                {
                    split[i][0] = '\t';
                }
                break;
            case 'v':
                {
                    split[i][0] = '\v';
                }
                break;
            case '"':
                {
                    split[i][0] = '\"';
                }
                break;
            case 'o':
                {
                    ResolveASCIIEscapeSequence( split[i], 8 );
                }
                break;
            case 'x':
                {
                    ResolveASCIIEscapeSequence( split[i], 16 );
                }
                break;
            default:
                {
                    // unknown escape sequence.  take it out
                    atString temp( split[i] );
                    split[i].Set( temp, 1 );
                }
                break;
            }
            
            s += split[i];
        }
    }
}

void initIni::ResolveASCIIEscapeSequence( atString &s, int base )
{
    int maxDigits = 0;
    switch ( base )
    {
    case 8:
        maxDigits = 3;
        break;
    case 16:
        maxDigits = 2;
        break;
    default:
        return;
    }

    int length = Min( 1 + maxDigits, s.GetLength() ); // max length

    for ( int i = 1; i < length; ++i )
    {
        if ( base == 8 )
        {
            if ( (s[i] < '0') || (s[i] > '7') )
            {
                length = i;
                break;
            }
        }
        else if ( base == 16 )
        {
            if ( ((s[i] < '0') || (s[i] > '9')) && ((s[i] < 'a') || (s[i] > 'f')) && ((s[i] < 'A') || (s[i] > 'F')) )
            {
                length = i;
                break;
            }
        }
    }

    if ( length > 1 )
    {
        atString value;
        value.Set( s, 1, length - 1 );

        atString postfix;
        postfix.Set( s, length );

        char temp[4];
        sprintf( temp, "%c", (char)strtol( value.c_str(), NULL, base ) );
        
        s = temp;
        s += postfix;
    }
    else
    {
        // remove the 'o' or 'x'.
        atString temp( s );
        s.Set( temp, 1 );
    }
}

//#############################################################################

initIniSection::initIniSection( const atString &name )
: m_name(name)
{

}

initIniSection::~initIniSection()
{
    Clear();
}

initIniItem* initIniSection::GetItem( const char *key )
{
    atString lowercaseKey( key );
    lowercaseKey.Lowercase();

    initIniItem **ppItem = m_items.Access( lowercaseKey );
    if ( ppItem != NULL )
    {
        return *ppItem;
    }

    return NULL;
}

void initIniSection::Add( initIniItem *pItem )
{   
    // Remove any existing KeyValue
    Remove( pItem->GetKey() );

    atString lowercaseKey( pItem->GetKey() );
    lowercaseKey.Lowercase();

    m_items.Insert( lowercaseKey, pItem );
}

void initIniSection::Remove( const char *key )
{
    atString lowercaseKey( key );
    lowercaseKey.Lowercase();

    initIniItem **ppItem = m_items.Access( lowercaseKey );
    if ( ppItem != NULL )
    {
        delete *ppItem;
    }

    m_items.Delete( lowercaseKey );
}

void initIniSection::Clear()
{
    atMap<atString, initIniItem *>::Iterator i = m_items.CreateIterator();
    for ( i.Start(); !i.AtEnd(); i.Next() )
    {
        delete i.GetData();
    }

    m_items.Kill();
}


bool initIniSection::FindString( const char *key, atString &s )
{
    initIniItem *pItem = GetItem( key );
    if ( pItem != NULL )
    {
        if ( !pItem->GetString( s ) )
        {
            Warningf( "initIni: Unable to convert value to string for key '%s'.", key );
        }

        return true;
    }

    return false;
}

bool initIniSection::FindBool( const char *key, bool &b )
{
    initIniItem *pItem = GetItem( key );
    if ( pItem != NULL )
    {
        if ( !pItem->GetBool( b ) )
        {
            Warningf( "initIni: Unable to convert value to boolean for key '%s'.", key );
        }

        return true;
    }

    return false;
}

bool initIniSection::FindFloat( const char *key, float &f )
{
    initIniItem *pItem = GetItem( key );
    if ( pItem != NULL )
    {
        if ( !pItem->GetFloat( f ) )
        {
            Warningf( "initIni: Unable to convert value to float for key '%s'.", key );
        }

        return true;
    }

    return false;
}

bool initIniSection::FindInt( const char *key, int &i ) 
{
    initIniItem *pItem = GetItem( key );
    if ( pItem != NULL )
    {
        if ( !pItem->GetInt( i ) )
        {
            Warningf( "initIni: Unable to convert value to integer for key '%s'.", key );
        }

        return true;
    }

    return false;
}

bool initIniSection::FindVector3( const char *key, Vector3 &v3 )
{
    initIniItem *pItem = GetItem( key );
    if ( pItem != NULL )
    {
        if ( !pItem->GetVector3( v3 ) )
        {
            Warningf( "initIni: Unable to convert value to Vector3 for key '%s'.", key );
        }

        return true;
    }

    return false;
}

bool initIniSection::FindVector4( const char *key, Vector4 &v4 )
{
    initIniItem *pItem = GetItem( key );
    if ( pItem != NULL )
    {
        if ( !pItem->GetVector4( v4 ) )
        {
            Warningf( "initIni: Unable to convert value to Vector4 for key '%s'.", key );
        }

        return true;
    }

    return false;
}

bool initIniSection::FindColor32( const char *key, Color32 &c32 )
{
    initIniItem *pItem = GetItem( key );
    if ( pItem != NULL )
    {
        if ( !pItem->GetColor32( c32 ) )
        {
            Warningf( "initIni: Unable to convert value to Color32 for key '%s'.", key );
        }

        return true;
    }

    return false;
}

//#############################################################################

initIniItem::initIniItem( const char *key, const char *value )
: m_key(key)
, m_value(value)
{

}

initIniItem::~initIniItem()
{

}

bool initIniItem::GetString( atString &s )
{
    // trim off leading and training quotes
    if ( m_value.StartsWith( "\"" ) && m_value.EndsWith( "\"" ) )
    {
        // could do more error-checking, but the initIni parser doesn't allow malformed values
        s.Set( m_value, 1, m_value.GetLength() - 2 );
    }
    else
    {
        s = m_value;
    }

    return true;
}

void initIniItem::SetString( const char *s )
{
    m_value = s;
}

bool initIniItem::GetBool( bool &b )
{
    atString temp = m_value;
    temp.Uppercase();

    b = (temp == "YES") || (temp == "TRUE");
    return true;
}

void initIniItem::SetBool( bool b )
{
    m_value = b ? "true" : "false";
}

bool initIniItem::GetFloat( float &f )
{
    if ( (m_value.GetLength() > 0) && (m_value[0] < '0') && (m_value[0] > '9') )
    {
        return false;
    }

    f = (float)atof( m_value.c_str() );
    return true;
}

void initIniItem::SetFloat( float f )
{
    char temp[64];
    sprintf( temp, "%f", f );
    m_value.Set( temp, (int)strlen(temp), 0 );
}

bool initIniItem::GetInt( int &i ) 
{
    if ( (m_value.GetLength() > 0) && (m_value[0] < '0') && (m_value[0] > '9') )
    {
        return false;
    }

    i = atoi( m_value.c_str() );
    return true;
}

void initIniItem::SetInt( int i )
{
    char temp[64];
    sprintf( temp, "%d", i );
    m_value.Set( temp, (int)strlen(temp), 0 );
}

bool initIniItem::GetVector3( Vector3 &v3 )
{
    atString parts[3];

    atString temp = m_value;
    atString left;
    atString right;

    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[0] = left;
    }
    else
    {
        parts[0] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[1] = left;
    }
    else
    {
        parts[1] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[2] = left;
    }
    else
    {
        parts[2] = "";
    }

    for ( int i = 0; i < 3; ++i )
    {
        if ( (parts[i].GetLength() > 0) && (parts[i][0] < '0') && (parts[i][0] > '9') )
        {
            return false;
        }

        if ( parts[i].IndexOf( "." ) == -1 )
        {
            return false;
        }
    }

    v3.SetX( (float)atof( parts[0].c_str() ) );
    v3.SetY( (float)atof( parts[1].c_str() ) );
    v3.SetZ( (float)atof( parts[2].c_str() ) );
    return true;
}

void initIniItem::SetVector3( const Vector3 &v3 )
{
    char temp[256];
    sprintf( temp, "%f, %f, %f", v3.GetX(), v3.GetY(), v3.GetZ() );
    m_value.Set( temp, (int)strlen(temp), 0 );
}

bool initIniItem::GetVector4( Vector4 &v4 )
{
    atString parts[4];

    atString temp = m_value;
    atString left;
    atString right;

    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[0] = left;
    }
    else
    {
        parts[0] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[1] = left;
    }
    else
    {
        parts[1] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[2] = left;
    }
    else
    {
        parts[2] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[3] = left;
    }
    else
    {
        parts[3] = "";
    }

    for ( int i = 0; i < 4; ++i )
    {
        if ( (parts[i].GetLength() > 0) && (parts[i][0] < '0') && (parts[i][0] > '9') )
        {
            return false;
        }

        if ( parts[i].IndexOf( "." ) == -1 )
        {
            return false;
        }
    }

    v4.SetX( (float)atof( parts[0].c_str() ) );
    v4.SetY( (float)atof( parts[1].c_str() ) );
    v4.SetZ( (float)atof( parts[2].c_str() ) );
    v4.SetW( (float)atof( parts[3].c_str() ) );
    return true;
}

void initIniItem::SetVector4( const Vector4 &v4 )
{
    char temp[256];
    sprintf( temp, "%f, %f, %f, %f", v4.GetX(), v4.GetY(), v4.GetZ(), v4.GetW() );
    m_value.Set( temp, (int)strlen(temp), 0 );
}

bool initIniItem::GetColor32( Color32 &c32 )
{
    atString parts[4];

    atString temp = m_value;
    atString left;
    atString right;

    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[0] = left;
    }
    else
    {
        parts[0] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[1] = left;
    }
    else
    {
        parts[1] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[2] = left;
    }
    else
    {
        parts[2] = "";
    }

    temp = right;
    temp.Split( left, right, ',' );
    left.Trim();
    right.Trim();

    if ( left.GetLength() > 0 )
    {
        parts[3] = left;
    }
    else
    {
        parts[3] = "255";
    }

    for ( int i = 0; i < 4; ++i )
    {
        if ( (parts[i].GetLength() > 0) && (parts[i][0] < '0') && (parts[i][0] > '9') )
        {
            return false;
        }

        if ( parts[i].IndexOf( "." ) != -1 )
        {
            return false;
        }
    }

    c32.SetRed( atoi( parts[0].c_str() ) );
    c32.SetGreen( atoi( parts[1].c_str() ) );
    c32.SetBlue( atoi( parts[2].c_str() ) );
    c32.SetAlpha( atoi( parts[3].c_str() ) );
    return true;
}

void initIniItem::SetColor32( const Color32 &c32 )
{
    char temp[256];
    sprintf( temp, "%i, %i, %i, %i", c32.GetRed(), c32.GetGreen(), c32.GetBlue(), c32.GetAlpha() );
    m_value.Set( temp, (int)strlen(temp), 0 );
}

//#############################################################################

} // namespace rage
