// 
// string/vformatf.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include <stdio.h>
#include <wchar.h>
#include <string.h>

namespace rage {

int
vformatf_n( char *dest, size_t maxLen, const char *fmt, va_list args )
{
	// Standard version is annoying like strncpy in that it
	// won't guarantee nul termination.

#if defined(__GNUC__) || defined(__SNC__)
	DEV_ONLY(memset(dest,0xFE,maxLen));	// Fill buffer with crap to catch bad size quickly
	dest[ maxLen - 1 ] = 0;
	return vsnprintf( dest, maxLen, fmt, args );
#else
	DEV_ONLY(memset(dest,0xFE,maxLen));	// Fill buffer with crap to catch bad size quickly
	dest[ --maxLen ] = 0;
	int res = _vsnprintf( dest, maxLen, fmt, args );
#if __ASSERT && __GAMETOOL
	Assert( res <= maxLen );
#endif 
	return res;
#endif
}


//Use vformatf with char* then convert to wchar_t* using safecpy.
int
vformatf_n( wchar_t *dest, size_t maxLen, const wchar_t *fmt, va_list args )
{
	DEV_ONLY(memset(dest,0xFE,maxLen*sizeof(wchar_t)));
	// Standard version is annoying like strncpy in that it
	// won't guarantee nul termination.
	dest[ --maxLen ] = 0;

    return vswprintf( dest, maxLen, fmt, args );
}

}
