// 
// data/stringbuilder.h 
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATA_STRINGBUILDER_H 
#define DATA_STRINGBUILDER_H 

#include "data/growbuffer.h"

namespace rage
{

class sysMemAllocator;

//PURPOSE
//  Dynamically builds a null-terminated string.
//  If a fixed size buffer is provided then strings will be built using that
//  buffer and no memory will be allocated.
class atStringBuilder
{
public:

    enum Flags
    {
        //Don't allocate memory, just determine required buffer size.
        NULL_BUFFER     = datGrowBuffer::NULL_BUFFER,
    };

    atStringBuilder();

    atStringBuilder(const unsigned flags);

    atStringBuilder(sysMemAllocator* allocator);

    atStringBuilder(char* buf,
                    const unsigned bufLen);

    ~atStringBuilder();

    //PURPOSE
    //  Initialize the string builder.
    //PARAMS
    //  allocator   - Optional.  Used to allocate memory.  If NULL the global
    //                allocator will be used.
    //  flags       - Bit combination of Flags.
    void Init(sysMemAllocator* allocator,
                const unsigned flags);

    //PURPOSE
    //  Initialize the buffer with a fixed buffer.  No memory allocation
    //  will be performed and the datGrowBuffer will not grow.
    //PARAMS
    //  buffer      - Fixed buffer.
    //  bufLen      - Length of buffer.
    //  flags       - Bit combination of Flags.

    //PURPOSE
    //  Initialize the string builder with a fixed buffer.  No memory allocation
    //  will be performed and the StringBuilder will not grow.
    //PARAMS
    //  buffer      - Fixed buffer.
    //  bufLen      - Length of buffer.
    //  flags       - Bit combination of Flags.
    void Init(char* buffer,
                const unsigned bufLen,
                const unsigned flags);

    //PURPOSE
    //  Clear the string builder and free memory.
    void Clear();

    //PURPOSE
    //  Return current flags.
    unsigned GetFlags() const;

    //PURPOSE
    //  Returns true if the buffer is growable.
    bool IsGrowable() const;

	//PURPOSE
	//  Preallocates the buffer so it doesn't need to be resized later.
	bool Preallocate(const unsigned newCapacity);

	//PURPOSE
    //  Append contents of buf to this buffer.
    //PARAMS
    //  str     - Buffer to append.
    //  len     - Length of buffer.
    //RETURNS
    //  Number of chars appended
    int Append(const char* str, const unsigned len);

    //PURPOSE
    //  Append the null-terminated string to this buffer.
    //PARAMS
    //  str     - Null-terminated string.
    //RETURNS
    //  Number of chars appended
    int Append(const char* str);

    //PURPOSE
    //  Append the char to this buffer.
    //PARAMS
    //  c       - Char.
    //RETURNS
    //  Number of chars appended
    int Append(const char c);

    //PURPOSE
    //  Append entire contents of buf to this buffer.
    //PARAMS
    //  str     - Buffer to append.
    //  len     - Length of buffer.
    //RETURNS
    //  True if len chars were appended.
    bool AppendOrFail(const char* str, const unsigned len);

    //PURPOSE
    //  Append entire null-terminated string to this buffer.
    //PARAMS
    //  str     - Null-terminated string.
    //RETURNS
    //  True if the entire string was appended.
    bool AppendOrFail(const char* str);

    //PURPOSE
    //  Append the char to this buffer.
    //PARAMS
    //  c       - Char.
    //RETURNS
    //  True if char was appended.
    bool AppendOrFail(const char c);

    //PURPOSE
    //  Prepend contents of buf to this buffer.
    //PARAMS
    //  str     - Buffer to append.
    //  len     - Length of buffer.
    //RETURNS
    //  Number of chars appended
    int Prepend(const char* str, const unsigned len);

    //PURPOSE
    //  Prepend the null-terminated string to this buffer.
    //PARAMS
    //  str     - Null-terminated string.
    //RETURNS
    //  Number of chars appended
    int Prepend(const char* str);

    //PURPOSE
    //  Prepend the char to this buffer.
    //PARAMS
    //  c       - Char.
    //RETURNS
    //  Number of chars appended
    int Prepend(const char c);

    //PURPOSE
    //  Prepend contents of buf to this buffer.
    //PARAMS
    //  str     - Buffer to append.
    //  len     - Length of buffer.
    //RETURNS
    //  True if len chars were prepended.
    bool PrependOrFail(const char* str, const unsigned len);

    //PURPOSE
    //  Prepend the null-terminated string to this buffer.
    //PARAMS
    //  str     - Null-terminated string.
    //RETURNS
    //  True if the entire string was prepended.
    bool PrependOrFail(const char* str);

    //PURPOSE
    //  Prepend the char to this buffer.
    //PARAMS
    //  c       - Char.
    //RETURNS
    //  True if char was prepended.
    bool PrependOrFail(const char c);

    //PURPOSE
    //  Truncate the string.
    //PARAMS
    //  newLen      - New string length.  Must be shorter than the current
    //                string length.
   void Truncate(const unsigned newLen);

   //PURPOSE
   //   Returns the index of the string, or -1 if not found.
   int IndexOf(const char* str) const;

   //PURPOSE
   //   Returns true if the string ends with str.
   bool EndsWith(const char* str) const;

   //PURPOSE
   //   Returns true if the string starts with str.
   bool StartsWith(const char* str) const;

    //PURPOSE
    //  Removes "len" characters starting at "start".  If start + len is longer
    //  than the buffer length then the buffer will be truncated to the
    //  byte just prior to "start".
    void Remove(const int start, const unsigned len);

    //PURPOSE
    //  Returns a pointer to the underlying string.
    const char* ToString() const;

    //PURPOSE
    //  Returns the length of the string.
    unsigned Length() const;

private:

    //Prevent copying
    atStringBuilder(const atStringBuilder&);
    atStringBuilder& operator=(const atStringBuilder&);

    datGrowBuffer m_DBuf;
};

} // namespace rage

#endif // DATA_STRINGBUILDER_H 
