//
// string/unicode.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "unicode.h"
#include "string.h"

#include <stdarg.h>
#include <stdio.h>	// for sprintf

using namespace rage;

char16 *rage::WideStringDuplicate(const char16 *str) {
	if (!str)
		return 0;
	int siz = (int) wcslen(str) + 1;
	return (char16*)memcpy(rage_aligned_new(2) char16[siz],str,sizeof(char16)*siz); 
}

int rage::CountUtf8Bytes(const char16* str)
{
	int count = 0;
	if (!str) return 0;
	while(*str)
	{
		if (*str <= 0x7F)
		{
			count += 1;
		}
		else if (*str <= 0x7FF)
		{
			count += 2;
		}
		else
		{
			count += 3;
		}
		++str;
	}
	return count;
}

int rage::CountUtf8Characters(const char* str)
{
	int count = 0;
	if (!str) 
		return 0;
	while (*str) {
		if ((*str & 0x80) == 0)
			++str;
		else if ((*str & 0xE0) == 0xC0) {
			// Detect invalid UTF-8 (which could be tricking us into skipping a \0) and bail
			if ((str[1] & 0xC0) != 0x80)
				return 0;
			str += 2;
		}
		else if ((*str & 0xF0) == 0xE0) {
			// Detect invalid UTF-8 (which could be tricking us into skipping a \0) and bail
			if (((str[1] & 0xC0) != 0x80) || ((str[2] & 0xC0) != 0x80))
				return 0;
			str += 3;
		}
		else	// ill-formed
			return 0;
		++count;
	}
	return count;
}

int rage::WideToUtf8Char(char16 in, char* out)
{
	u8* utf8out = reinterpret_cast<u8*>(out);
	if (in <= 0x7F)
	{
		utf8out[0] = (u8)in;
		return 1;
	}
	else if (in <= 0x7FF)
	{
		utf8out[0] = u8(0xC0 + (in >> 6));
		utf8out[1] = u8(0x80 + (in & 0x3f));
		return 2;
	}
	else
	{
		utf8out[0] = u8(0xE0 +  (in >> 12));
		utf8out[1] = u8(0x80 + ((in >> 6)&0x3f));
		utf8out[2] = u8(0x80 +  (in & 0x3f));
		return 3;
	}
}

int rage::Utf8ToWideChar(const char* in, char16& out)
{
	if ((in[0] & 0x80) == 0) {
		out = char16(in[0]);
		return 1;
	}
	else if ((in[0] & 0xE0) == 0xC0) {
		if ((in[1] & 0xC0) != 0x80) {
			AssertMsg(0, "Ill-formed 2-byte utf-8 character");
			out = 0;
			return 0; //check for partial/broken Utf-8
		}
		out = (in[0] & 0x1F) << 6;
		out |= (in[1] & 0x3F);
		return 2;
	}
	else if ((in[0] & 0xF0) == 0xE0) {
		if ((in[1] & 0xC0) != 0x80 || (in[2] & 0xC0) != 0x80) {
			AssertMsg(0, "Ill-formed 3-byte utf-8 character");
			out = 0;
			return 0; //check for partial/broken Utf-8
		}
		out = (in[0] & 0x0F) << 12; //00001111, shift it over to be	11110000,00000000
		out |= (in[1] & 0x3F) << 6; //00222233, shift it over to be	00002222,33000000
		out |= (in[2] & 0x3F);      //00334444, let it stay			00000000,00334444
		return 3;
	}
	AssertMsg(0, "Invalid format for utf-8 character");
	out = 0;
	return 0;
}

char *rage::WideToUtf8(char *out,const char16 *in,int length)
{
	u8 * utf8out = (u8*)out;
	for (int i=0; in[i] && i<length;i++)
	{
		const char16 input = in[i];
		if (input<=0x7F)
			*utf8out++ = (u8)input;
		else if (input <= 0x7FF)
		{
			*utf8out++ = u8(0xC0 + (input >> 6));
			*utf8out++ = u8(0x80 + (input & 0x3f));
		}
		else
		{
			*utf8out++ = u8(0xE0 +  (input >> 12));
			*utf8out++ = u8(0x80 + ((input >> 6)&0x3f));
			*utf8out++ = u8(0x80 +  (input & 0x3f));
		}
	}
	*utf8out = 0;
	return out;
}

char *rage::WideToUtf8(char *out,const char16 *in,int length, int outBufSize, int* numConverted)
{
	u8 * utf8out = (u8*)out;
	int bytesWritten = 0;
	*numConverted = 0;

	for (int i=0; in[i] && i<length;i++)
	{
		const char16 input = in[i];
		if (input<=0x7F)
		{
			if (bytesWritten+1 < outBufSize)
			{
				*utf8out++ = (u8)input;
				bytesWritten++;
			}
			else
			{
				break;
			}
		}
		else if (input <= 0x7FF)
		{
			if (bytesWritten + 2 < outBufSize)
			{
				*utf8out++ = u8(0xC0 + (input >> 6));
				*utf8out++ = u8(0x80 + (input & 0x3f));
				bytesWritten += 2;
			}
			else
			{
				break;
			}
		}
		else
		{
			if (bytesWritten + 3 < outBufSize)
			{
				*utf8out++ = u8(0xE0 +  (input >> 12));
				*utf8out++ = u8(0x80 + ((input >> 6)&0x3f));
				*utf8out++ = u8(0x80 +  (input & 0x3f));
				bytesWritten += 3;
			}
			else
			{
				break;
			}
		}

		(*numConverted)++;
	}

	*utf8out = 0;
	return out;
}

char16* rage::Utf8ToWide(char16* out, const char* in, int length)
{
	char16* outPos = out;
	for(int i = 0; in[i] && i < length;)
	{
		int incr = Utf8ToWideChar(in + i, *outPos);
		Assertf(incr,"Bad UTF-8 encoding in %s, char %d",in,i);
		if(0 == incr)
		{
			break; //let's not go into an infinite loop just because there is bad encoding.
		}

		i += incr;
		outPos++;
	}
	*outPos = 0;
	return out;
}

int rage::Utf8RemoveInvalidSurrogates(char* in)
{
	// Find terminator index.
	const int end = (int)strlen(in);
	
	int last = end - 1;
	// Early out if a single UTF-8 char and not a surrogate (i.e. valid 7-bit ASCII)!
	if(end <= 0 || (in[last] & 0x80) == 0)
	{
		return 0;
	}


	// Find the first surrogate.
	while((in[last] & 0xC0) != 0xC0 && in != &in[last])
	{
		--last;
	}

	const int numDetected = end - last;

	// Whilst UTF-8 can theoretically hold more, RFC 3629 restricts UTF-8 to 4 surrogate pairs, however, only a subset of 4 surrogate
	// code points are valid. See http://en.wikipedia.org/wiki/UTF-8.
	Assertf((in[last] & 0xC0) == 0x80 || (in[last] & 0xE0) == 0xC0 || (in[last] & 0xF0) == 0xE0 || (in[last] & 0xF8) == 0xF0, "Too many surrogates expected, string is likely not UTF-8!");
	Assertf(numDetected < 4, "Too many surrogates detected, string is likely not UTF-8!");

	// If the right number is detected then do nothing.
	if( ((in[last] & 0xE0) == 0xC0 && numDetected == 2) ||
		((in[last] & 0xF0) == 0xE0 && numDetected == 3) ||
		((in[last] & 0xF8) == 0xF0 && numDetected == 4) )
	{
		return 0;
	}

	in[last] = 0;
	return numDetected;
}

