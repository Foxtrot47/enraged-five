// 
// string/stringheap.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "stringheap.h"

#include <string.h>

#if __WIN32
#include <crtdefs.h>
#endif

#define HeapAssert(x) Assert(x)

namespace rage {

#if __OPTIMIZED
#define VerifyHeap()
#endif

/*
	Memory heap which implements singly-linked memory nodes to minimize heap overhead.
	Freed nodes are not coalesced when freed; instead, they are all collapsed during
	garbage collection.  This heap is different from most of my implementations because
	it's assumed to be full of smallish object with a relatively small number of sizes.
*/

struct atStringHeap::Node
{
	size_t
		Used: 1,		// True if node is in use	
		Touched: 1,		// True if node has been touched since last GC
		Size: 17,		// Size of this node, in multiples of sizeof(Node), INCLUDING THE NODE ITSELF (ie always at least one)
		Handle: 13;		// Backpointer to owning handle.
};

struct atStringHeap::FreeNode: public atStringHeap::Node
{
	FreeNode *NextFree;
};


void atStringHeap::Init(void *memory,size_t heapSize,int maxHandles)
{
	Assert(maxHandles <= MaxHandles);
	Assert(heapSize <= MaxHeapSize);
	size_t handleSize = maxHandles * sizeof(char**);
	Assert(handleSize < heapSize);
	heapSize -= handleSize;

	m_Handles = (char**)memory;
	m_Start = (Node*)((char*)m_Handles + handleSize);
	m_End = (Node*)((char*)m_Start + heapSize);
	m_MaxHandles = maxHandles;

	// Init the free list
	heapSize /= sizeof(Node);
	memset(m_FreeBySize, 0, sizeof(m_FreeBySize));
	FreeNode *fn = m_FreeBySize[GetBucketForSize(heapSize)] = (FreeNode*) m_Start;
	fn->Used = false;
	fn->Touched = false;
	fn->Size = heapSize;
	fn->Handle = 0;
	fn->NextFree = 0;

	// Init the handle array (handle 0 is never used)
	m_Handles[0] = 0;
	for (int i=1; i<maxHandles-1; i++)
		m_Handles[i] = (char*)&m_Handles[i+1];
	m_Handles[maxHandles-1] = 0;
	m_FreeHandle = &m_Handles[1];

	VerifyHeap();
}


#ifndef VerifyHeap
void atStringHeap::VerifyHeap()
{
	// Make sure the heap is properly connected
	Node *i = m_Start;
	while (i != m_End) 
	{
		Assert(i->Size);
		if (!i->Used)
			i->Touched = true;
		Assert(i < m_End);
		i += i->Size;

	}

	// Make sure the free list is valid
	for (size_t b=0; b<MaxBuckets; b++)
	{
		FreeNode *f = m_FreeBySize[b];
		while (f) 
		{
			Assert(!f->NextFree || f->Size <= f->NextFree->Size);
			Assert(f->Touched);
			f->Touched = false;
			f = f->NextFree;
		}
	}

	// Make sure everything that was marked free was in a free list
	i = m_Start;
	while (i != m_End)
	{
		Assert(!i->Touched);
		i += i->Size;
	}
}
#endif


void atStringHeap::AddToFreeList(atStringHeap::FreeNode *n)
{
	size_t nSize = n->Size;

	FreeNode **p = &m_FreeBySize[GetBucketForSize(n->Size)];
	while (*p && (*p)->Size < nSize)
		p = &(*p)->NextFree;

	n->NextFree = *p;
	*p = n;
}

atStringHeap::Node* atStringHeap::AllocateInternal(size_t size)
{
	if (!size)
		return 0;
	else
		size = ((size + sizeof(Node)-1) / sizeof(Node)) + 1;

	// VerifyHeap();

	for (size_t b=GetBucketForSize(size); b<MaxBuckets; b++) {
		FreeNode **p = &m_FreeBySize[b];
		// Free list is sorted by size.
		while (*p)
		{
			FreeNode *c = *p;

			if (size == c->Size || (size + 1 == c->Size && !c->NextFree))	// Exact fit?  (Or last item in heap and it's the right size)
			{
				*p = c->NextFree;	// Remove the node from the free list.
				c->Used = true;
				VerifyHeap();
				return c;			// Caller is expected to fill in backpointer to handle
			}
			// Large enough to split? (otherwise we run the risk of slowly losing sizeof(Node) bytes everywhere)
			// We cannot have a zero-sized element in the free list because free nodes have to be at least
			// twice the size of a regular node.
			else if (size + 1 < c->Size)
			{
				FreeNode *split = (FreeNode*)((Node*)c + size);

				*p = c->NextFree;	// Remove the node from the free list.
				size_t origSize = c->Size;
				c->Used = true;
				c->Size = size;

				split->Used = false;
				split->Touched = false;
				split->Size = origSize - size;
				split->Handle = 0;
				split->NextFree = 0;
				AddToFreeList(split);
				VerifyHeap();
				return c;			// Caller is expected to fill in backpointer to handle
			}
			// Else keep looking...
			p = &c->NextFree;
		}
	}

	// Not enough memory...
	return 0;
}


void atStringHeap::FreeInternal(atStringHeap::Node *f)
{
	// TODO: We *could* look at our next node to see if it's free,
	// but we'd have to traverse the entire free list to locate it
	// and remove it.
	Assert(f->Used);
	f->Used = false;
	AddToFreeList((FreeNode*)f);
	// VerifyHeap();
}


void atStringHeap::Defragment()
{
	Node *s = m_Start, *d = m_Start;

	// Traverse the entire heap
	while (s != m_End)
	{
		Assert(s < m_End);
		// Cache the current size because we'll potentially destroy it in the downward copy
		size_t thisSize = s->Size;
		if (s->Used)
		{
			Assert(s->Handle);
			if (d != s)
			{
				// Copy payload downward; TODO: Merge larger runs into one copy
				memcpy(d,s,(thisSize + 1) * sizeof(Node));
				// Adjust handle to its new location
				m_Handles[d->Handle] = (char*)(d + 1);
			}
			d += thisSize;
		}
		s += thisSize;
	}

	// Reset all of the free lists.
	memset(m_FreeBySize, 0, sizeof(m_FreeBySize));

	// Whatever is left over is the new free list.  There may be nothing left over,
	// or there may only be sizeof(Node) left over, in which case it's not big enough
	// to represent a free node!
	if (d < m_End-1)
	{
		size_t siz = m_End - d;
		FreeNode *fn = m_FreeBySize[GetBucketForSize(siz)] = (FreeNode*) d;
		fn->Used = false;
		fn->Touched = false;
		fn->Size = siz;
		fn->Handle = 0;
		fn->NextFree = 0;
	}
}


bool atStringHeap::IsFragmented() const
{
	// Heap is fragmented if there's more than one item in the free list,
	// or more than one free list.
	bool atLeastOne = false;
	for (size_t b=0; b<MaxBuckets; b++)
	{
		if (m_FreeBySize[b])
		{
			if (atLeastOne || m_FreeBySize[b]->NextFree)
				return true;
			atLeastOne = true;
		}
	}
	return false;
}


void atStringHeap::Touch(char **handle)
{
	if (handle)
	{
		Node *n = (Node*)*handle - 1;
		n->Touched = true;
	}
}


void atStringHeap::GarbageCollect()
{
	// Traverse the heap, looking for untouched handles.
	// If we find them, reclaim the memory AND the handle.
	// TODO: Could include some sort of generation ID to trap stale access to old handles.
	Node *i = m_Start;
	while (i != m_End)
	{
		if (i->Touched)
			i->Touched = false;
		else if (i->Used)
			Free(m_Handles + i->Handle);
		i += i->Size;
	}
}




char** atStringHeap::Allocate(size_t size)
{
	// No handles available, have to bail regardless of memory free.
	if (!m_FreeHandle)
		return 0;

	if (!size)
		return 0;

	Node *newNode = AllocateInternal(size);
	if (!newNode)
	{
		if (IsFragmented())
		{
			Defragment();
			newNode = AllocateInternal(size);
		}
		if (!newNode)
			return 0;
	}

	// Allocate the handle from the free handle list
	char **h = m_FreeHandle;
	m_FreeHandle = *(char***)m_FreeHandle;

	// Point the handle at the payload
	*h = (char*)(newNode + 1);
	// Point the payload back at the handle
	newNode->Handle = h - m_Handles;

	return h;
}


void atStringHeap::Free(char **h)
{
	if (h)
	{
		FreeInternal((Node*)(*h - sizeof(Node)));
		*h = (char*)m_FreeHandle;
		m_FreeHandle = h;
	}
}


} // namespace rage
