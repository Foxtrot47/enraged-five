//
// string/stringutil.h
//
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved.
//

#ifndef STRING_STRINGUTIL_H
#define STRING_STRINGUTIL_H

namespace rage {

char* strrpbrk(char* str, const char* pat, char* nomatch = NULL); // like strpbrk, but searches in reverse
char* strskip (char* str, const char* substr); // skips past 'substr' if 'str' starts with it, otherwise returns 'str'
char* striskip(char* str, const char* substr);
char* stristr (char* str, const char* substr, char* nomatch = NULL); // like strstr, but case-insensitive
char* strrstr (char* str, const char* substr, char* nomatch = NULL); // like strstr, but searches in reverse
char* strristr(char* str, const char* substr, char* nomatch = NULL);

const char* strrpbrk(const char* str, const char* pat, const char* nomatch = NULL); // like strpbrk, but searches in reverse
const char* strskip (const char* str, const char* substr); // skips past 'substr' if 'str' starts with it, otherwise returns 'str'
const char* striskip(const char* str, const char* substr);
const char* stristr (const char* str, const char* substr, const char* nomatch = NULL); // like strstr, but case-insensitive
const char* strrstr (const char* str, const char* substr, const char* nomatch = NULL); // like strstr, but searches in reverse
const char* strristr(const char* str, const char* substr, const char* nomatch = NULL);

char* safestrtok(char* str, const char* delim);

int StringWildcardCompare(const char *wild, const char *string, bool insensitive=false);

//Trims leading/trailing spaces
//Returns a pointer to dst.
char* StringTrim(char* dst, const char* src, const unsigned sizeofDst);

//Trims trailing instances of c.
//Returns a pointer to dst.
char* StringRTrim(char* dst, const char* src, const int c, const unsigned sizeofDst);

//Returns true if str starts with substr.
bool StringStartsWith(const char* str, const char* substr);
//Case insensitive
bool StringStartsWithI(const char* str, const char* substr);

//Returns true if str ends with substr.
bool StringEndsWith(const char* str, const char* substr);
//Case insensitive
bool StringEndsWithI(const char* str, const char* substr);

// Returns true when str1 equals str2, or both strings are nullptr.
// The third parameter defines if a case insensitive comparison should be used
bool StringIsEqual(const char* str1, const char* str2, bool insensitive = false);

// Returns true if the string is null or empty
bool StringNullOrEmpty(const char* str);

// Copies src to dst. If src is bigger than dst it will truncate from the start of the string and 
// add 3 dots to the front, e.g. ...yfolder/myfile.txt
// Returns true if it was truncated and false otherwise.
bool StringCopyTruncateFront( char* dst, const char* src, const unsigned sizeofDst );

// Replaces all occurrences any character in the find array with the replacement character.
void StringReplaceChars(char* str, const char* charactersToFindArray, char replace);
	
// Convert an integer to a binary string, e.g. (u8) 101 => "01100101"
// Returns 0 if the destination string is too small, otherwise it returns the length of the string.
template<typename _Type> int IntToBinaryString(char* dst, _Type src, const unsigned int sizeofDst)
{
	unsigned int bitLength = sizeof(_Type) * 8;

	if(sizeofDst < bitLength + 1)
	{
		return 0;
	}

	char* pCurrent = dst;

	_Type value = src;

	int bitShift = bitLength - 1;

	for(unsigned int index = 0 ; index < bitLength ; ++index)
	{
		int bit = int(value >> bitShift) & 1;

		*pCurrent++ = char(bit + '0');

		value <<= 1;
	}

	*pCurrent = 0;
	
	return bitLength;
}

// print a bool value nicely as "true" or "false" for logging
inline const char* LogBool(const bool b) { 
	return b ? "true" : "false"; 
}

// print a bool value nicely as "succeeded" or "failed" for logging
inline const char* LogResult(const bool b) {
	return b ? "succeeded" : "failed";
}

// print a bool value nicely as "enabled" or "disabled" for logging
inline const char* LogEnabled(const bool b) {
	return b ? "enabled" : "disabled";
}

} // namespace rage

#endif // STRING_STRINGUTIL_H
