//
// string/string.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef STRING_STRING_H
#define STRING_STRING_H

#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <ctype.h>

#include "data/struct.h"
#include "system/new.h"

namespace rage {

#if __WIN32
#pragma warning(disable: 4514)  // Unreferenced inline removed
 #if _MSC_VER >= 1400
  #if __TOOL
   //Maya currently defines strcasecmp as stricmp, so re-defining it here with _stricmp generates errors in tool builds for Maya plugins. 
   #define strcasecmp  stricmp
  #else
   #define strcasecmp _stricmp
  #endif
 #define strncasecmp  _strnicmp
 #define stricmp _stricmp
 #define strnicmp _strnicmp
 #define strcmpi _stricmp
 #define strncmpi _strnicmp
 #define strupr _strupr
 #define strlwr _strlwr
 #else
 #define strcasecmp  stricmp
 #define strncasecmp  strnicmp
 #endif
#else
#define stricmp     strcasecmp
#define strnicmp  strncasecmp
# if __PS3 || __PSP2 || RSG_ORBIS
extern "C" char* strupr(char*);
extern "C" char* strlwr(char*);
#define strcmpi stricmp
# endif
#endif

#ifdef _MSC_VER 
#define snprintf _snprintf
#endif

// for compatibility with code integrated from RDR
#define formatf_sized formatf
#define safecatf_sized safecatf

// PURPOSE: Like StringLength, except returns an int instead of a size_t and will return
//          zero on a null pointer.
template< typename T >
int
StringLength( const T *s )
{
    if( s )
    {
        const T* e = s;

        for( ;'\0' != *e; ++e )
        {
        }

        return int( e - s );
    }
    else
    {
        return 0;
    }
}

template< typename T >
int
StringLength(const T *s, const int maxLen)
{
    if(s)
    {
        const T* e = s;

        for(int i = 0;'\0' != *e && i < maxLen; ++e, ++i)
        {
        }

        return int(e - s);
    }
    else
    {
        return 0;
    }
}

inline
int
StringLength( const char* s )
{
    return int( s ? ::strlen( s ) : 0 );
}

// PURPOSE: Creates a copy of the passed in string, allocating the memory with "new"
template< typename T >
T*
StringDuplicate( const T* str )
{
    if( !str )
        return 0;
    int siz = StringLength( str ) + 1;
    return ( T* ) memcpy( rage_aligned_new(__alignof(T)) T[ siz ], str, sizeof(T) * siz ); 
}

// PURPOSE: frees a string allocated by <c>StringDuplicate</c>.
template< typename T >
inline
void
StringFree( T* str )
{
	delete [] ( T* ) str;
}

#if (!__TOOL && !__RESOURCECOMPILER && !__WIN32PC)
extern "C" char _rodata_end[];

inline bool IsConstString(const char *addr)
{
#if __XENON // On Xenon, code starts at 0x82000000 so we can just test the high bit to see if we're between 0x8000000 and _rodata_end.
	return addr > (const char*)0x82000000 && addr < _rodata_end;
#elif RSG_DURANGO || RSG_ORBIS
	// Tests don't work correctly on this platform yet, so nerf it
	return !addr;
#else
	return addr < _rodata_end;
#endif
}
#else
inline bool IsConstString(const char *) { return false; }
#endif

/*
PURPOSE
    Duplicate the const string.
PARAMS
    str - to be duplicated.
RETURNS
    const char pointer.
NOTES
    Is is specifically NOT allowed to intermix ConstStringDuplicate / StringFree or vice versa.
*/
template< typename T >
inline
const T*
ConstStringDuplicate( const T* str )
{
	return IsConstString( str )? str : StringDuplicate( str );
}

/*
PURPOSE
    Delete the const string.
PARAMS
    str - to be deleted.
RETURNS
    none.
NOTES
*/
template< typename T >
inline
void
ConstStringFree( const T* str )
{
	if (!IsConstString(str))
	    delete [] ( T* ) str;
}

// PURPOSE: Converts all the characters in src to lowercase, and turns all backslashes to forward slashes.
// NOTES: Does not allocate memory, so dest should be preallocated to the proper size.
// src and dest may point to the same memory.
template< typename T, typename U >
T*
StringNormalize( T *dest, const U src, size_t destSize )
{
    FastAssert( dest );
    FastAssert( destSize > 0 );
    size_t ch;

    T* result = dest;
    int i = 0;

    while( --destSize > 0 && ( ch = src[i] ) != '\0' )
    {
        if( ch >= 'A' && ch <= 'Z' )
            ch += 32;
        else if ( ch == '\\' )
            ch = '/';

        *dest++ = T( ch );
        ++i;
    }

    *dest = '\0';

    return result;
}

// PURPOSE: Like strrchr, but takes the size of the buffer
template< typename T >
T* safestrrchr( T * src, const size_t srcSize, const int ch )
{
    FastAssert( src );
    FastAssert( srcSize > 0 );

    T* start( src );
    T* end( start + srcSize - 1 );

    while ( --end != start && *end != (T)ch );

    T* result = (*end == (char)ch) ? end : NULL;            
    return result;
}

template <typename T, size_t _Size > inline 
T* safestrrchr( T (&src)[_Size], const int ch )
{
    return safestrrchr( src, _Size, ch );
}

// PURPOSE: Like strncpy, but expects the actual size of dest, not size-1, and always guarantees buffer is zero-terminated
template< typename T, typename U >
T*
safecpy( T *dest, const U& src, size_t destSize )
{
    FastAssert( dest );
    FastAssert( destSize > 0 );

    T* result = dest;
    int i = 0;

    while( --destSize > 0 && src != NULL && '\0' != src[i])
    {
        *dest++ = T( src[i] );
        ++i;
    }

    *dest = '\0';

    return result;
}

template <typename T, typename U, size_t _Size> inline 
T* safecpy(T (&dest)[_Size], const U& src)
{
	return safecpy(dest, src, _Size);
}


// PURPOSE: Like strcat, but expects the actual size of dest, not size-1, and always guarantees buffer is zero-terminated
template< typename T, typename U >
T*
safecat( T* dest, const U& src, size_t destSize )
{
	FastAssert( dest );
    FastAssert( destSize > 0 );

	T* result = dest;

    if( destSize > 0 )
    {
        size_t i = 0;
	    size_t len = StringLength( dest );
	    destSize -= ( len + 1 );
	    dest += len;
	    while( src[i] && destSize > 0 )
        {
		    *dest++ = T( src[i] );
		    --destSize;
            ++i;
	    }
    }

	*dest = '\0';

	return result;
}

template <typename T, typename U, size_t _Size> inline 
T* safecat(T (&dest)[_Size], const U& src)
{
	return safecat(dest, src, _Size);
}

/*
PURPOSE:
	Generate a formatted string, returning the number of characters written
PARAMS
	dest - to be returned format string.
	maxLen - the string length
	fmt - input format
	args - Args needed for formatting
RETURNS
	number of characters written, or -1 on error
	*/
int vformatf_n( char *dest, size_t maxLen, const char* fmt, va_list args );

/*
PURPOSE
    Generate a formatted string
PARAMS
    dest - to be returned format string.
    maxLen - the string length
    fmt - input format
    args - Args needed for formatting
RETURNS
    char pointer
NOTES
*/
inline char* vformatf( char *dest, size_t maxLen, const char* fmt, va_list args ) { vformatf_n(dest, maxLen, fmt, args); return dest; }

/*
PURPOSE
    Generate a formatted string. Auto-detect length of target buffer.
PARAMS
    dest - to be returned format string. MUST be a char[], do not use a pointer for this variant.
    fmt - input format
    args - Args needed for formatting
RETURNS
    char pointer
NOTES
*/
template <typename T, size_t _Size> inline 
	T* vformatf(T (&dest)[_Size], const T *fmt, va_list args)
{
	return vformatf(dest, _Size, fmt, args);
}

/*
PURPOSE
	Pretty-print a large integer (useful for memory displays)
PARAMS
	dest - Destination buffer
	destSize - sizeof(dest)
	value - Value to display with commas every three digits
RETURNS
	Pointer to dest, for your nesting pleasure */
char* prettyprinter( char *dest, size_t destSize, s64 value );

int vformatf_n( wchar_t *dest, size_t maxLen, const wchar_t* fmt, va_list args );

//Use vformatf with char* then convert to wchar_t* using safecpy.
inline wchar_t* vformatf( wchar_t *dest, size_t maxLen, const wchar_t* fmt, va_list args ) { vformatf_n(dest, maxLen, fmt, args); return dest; }


/*
PURPOSE
    Generate a formatted string.
PARAMS
    dest - to be returned format string.
    maxLen - the string length.
    fmt - input format.
    ... -
RETURNS
    char pointer.
NOTES
*/
template< typename T >
T*
formatf( T* dest, size_t maxLen, const T* fmt, ... )
{
	va_list args;
	va_start( args, fmt );

	return vformatf( dest, maxLen, fmt, args );
}

template <typename T, size_t _Size> inline 
T* formatf(T (&dest)[_Size], const T *fmt, ...)
{
	va_list args;
	va_start( args, fmt );

	return vformatf(dest, _Size, fmt, args);
}

/*
PURPOSE
    Generate a formatted string.
PARAMS
    dest - to be returned format string.
    maxLen - the string length.
    fmt - input format.
    ... -
RETURNS
    number of characters written
NOTES
*/
template< typename T >
int
formatf_n( T* dest, size_t maxLen, const T* fmt, ... )
{
	va_list args;
	va_start( args, fmt );

	return vformatf_n( dest, maxLen, fmt, args );
}

template <typename T, size_t _Size> inline 
int formatf_n(T (&dest)[_Size], const T *fmt, ...)
{
	va_list args;
	va_start( args, fmt );

	return vformatf_n(dest, _Size, fmt, args);
}


/*
PURPOSE
    Concatenate a formatted string to an existing string.
PARAMS
    dest - to be returned format string.
    maxLen - the string length.
    fmt - input format.
    ... -
RETURNS
    char pointer.
NOTES
*/
template< typename T >
T*
vsafecatf(T* dest, size_t maxLen, const T* fmt, va_list args)
{
    if(dest && maxLen > 0)
    {
        const size_t len = StringLength(dest);
        FastAssert(len <= maxLen);
	    vformatf(dest + len, maxLen - len, fmt, args);
    }
    return dest;
}

template< typename T >
T*
safecatf(T* dest, size_t maxLen, const T* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
    return vsafecatf(dest, maxLen, fmt, args);
}

template <typename T, size_t _Size> inline 
T* safecatf(T (&dest)[_Size], const T *fmt, ...)
{
	va_list args;
	va_start( args, fmt );

	return vsafecatf(dest, _Size, fmt, args);
}


// ConstStringBase is the base class for ConstString and ConstUserString 
// that simply encapsulates the const char* pointer.  It allows casting to 
// and comparison with const char*, but does not provide any memory management.
class ConstStringBase {
public:
	operator const char*() const;

	bool operator==(const char* that) const;

	bool operator!=(const char* that) const;

	// @NOTE: you might be tempted to think the following are redundant with 
	// the above since we have operator const char*(), but you would be
	// wrong: if you don't explicitly overload ==, c++ will use the default
	// memcmp-ish == rather than searching for something to cast to.

	// PURPOSE: Returns true if contents of both strings are the same.
	bool operator==(const ConstStringBase &that) const;

	// PURPOSE: Returns true if the contents of the two strings differ.
	bool operator!=(const ConstStringBase& that) const;

	// PURPOSE: Returns true if one string is less than the other.
	bool operator<(const ConstStringBase& that) const;

	// PURPOSE: For compatibility with std::string
	const char *c_str() const {
		return m_String;
	}

	// PURPOSE: This is intentionally public, for compatibility with other interfaces like bkDialog
	const char *m_String;

protected:
	// Only ConstString or ConstUserString should be instantiated, not ConstStringBase
	ConstStringBase();
	ConstStringBase(const char* s);
};

inline ConstStringBase::ConstStringBase() 
{
}

inline ConstStringBase::ConstStringBase(const char* s)
: m_String(s)
{
}

inline ConstStringBase::operator const char*() const
{
	return m_String;
}

inline bool ConstStringBase::operator==(const ConstStringBase &that) const {
	if (m_String == that.m_String) {
		return true;
	}
	else if (m_String == NULL || that.m_String == NULL) {
		return false;
	}
	return !strcmp(m_String,that.m_String); //lint !e668 possibly passing null pointer to function (default constructor)
}

inline bool ConstStringBase::operator!=(const ConstStringBase& that) const
{
	return !(*this == that);
}

inline bool ConstStringBase::operator==(const char* that) const
{
	if (m_String == that)
	{
		return true;
	}
	else if (m_String == NULL || that == NULL)
	{
		return false;
	}
	return !strcmp(m_String, that);
}

inline bool ConstStringBase::operator!=(const char* that) const
{
	return !(*this == that);
}

inline bool ConstStringBase::operator<(const ConstStringBase& that) const {

	return (m_String&&that.m_String) ? strcmp(m_String,that.m_String)<0 : that.m_String!=NULL;
}



/*
PURPOSE
Encapsulate a const char* with minimal overhead.  This is less general (and has
less overhead than) atString, but can still be safely placed in atArrays and be
properly copied about.  Paradoxically, ConstString no longer uses ConstStringDuplicate
and ConstStringFree because several classes directly or indirectly place ConstStrings
into resources, and any string literals would throw an assert once we tried to
fixup their pointers on load.  It's not safe to call IsReadOnly before doing the
pointer fixup because code may have been changed enough to invalidate address
ranges, or the resources may even be loaded by an entirely different program.
<FLAG Component>
*/
class ConstString : public ConstStringBase {
protected:
    void vCopy(const char * String);
public:
    ConstString();
    ConstString(class datResource&);
    explicit ConstString(const char* s);

#if __DECLARESTRUCT
    void DeclareStruct(class datTypeStruct &s);
#endif

    // PURPOSE: Copy constructor. Makes a deep copy of rfConstString
    ConstString(const ConstString &rfConstString);
    ~ConstString();
    ConstString& operator=(const char *s);
	// PURPOSE: Deletes any current string, and then creates a copy of rfConstString
	ConstString& operator=(const ConstString &rfConstString);


	IMPLEMENT_PLACE_INLINE(ConstString)

};

///////////////////////////////////////
// ConstString inlines

inline ConstString::ConstString() 
: ConstStringBase(NULL)
{
}

inline ConstString::ConstString(const char* s)
: ConstStringBase(StringDuplicate(s)) 
{ 
}

inline ConstString::ConstString(const ConstString &rfConstString)
: ConstStringBase(StringDuplicate(rfConstString.m_String))
{
}

inline ConstString& ConstString::operator=(const char *s) {
    vCopy(s);
    return *this;
}

inline ConstString& ConstString::operator=(const ConstString &rfConstString)
{
	vCopy(rfConstString.m_String);
	return *this;
}


// ConstUserString is to ConstString as atUserArray is to atArray:
// It provides a way to effectively pass a const char* to a
// method that expects a const ConstString& without causing needless 
// allocations.
// BUT, you need to be careful to only use it within the scope that 
// the const char* used to construct it is still valid.
//
// This is _not_ intended to encourage the practice of creating methods 
// that take const ConstString& parameters but sometimes these are unavoidable 
// (e.g. when using atMap<ConstString, Foo>).
class ConstUserString : public ConstStringBase
{
public:
	ConstUserString(const char* s);

	operator const ConstString&() const;
};

inline ConstUserString::ConstUserString(const char *s)
: ConstStringBase(s)
{
}

inline ConstUserString::operator const ConstString&() const
{
	return *static_cast<const ConstString*>(static_cast<const ConstStringBase*>(this));
}

} // namespace rage

#endif

