// 
// string/stringheap.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef STRING_STRINGHEAP_H 
#define STRING_STRINGHEAP_H 

#include <stddef.h>

namespace rage {

/*
	Simple class which implements handle-based access to defragmentable "dumb" memory.  It's intended for use by strings, but actually 
	can be used for any data that doesn't need anything stricter than four-byte alignment.  You supply memory to use, and a handle 
	count; it will allocate an array of pointers out of that for the handle array, and give the rest to the payload heap.  

	Memory is managed in a singly-linked list so there's only four bytes of overhead per memory node.  Max handles is limited by 13 
	bit field currently, but that could go higher if we shrank the maximum heap size down from its current limit of 1<<19 (512k).

	Allocating memory will trigger an implicit defragment if the heap was full, so don't store a "dereferenced" handle's value across 
	allocation calls.  Freeing does not defragment.  There is also support for garbage collection, where all pointers that don't have 
	atStringHeap::Touch called on them will have both their payload and their handles deleted once GarbageCollect (which clears the 
	touch bits again) is called.

	Note that you should consider defragmenting the heap periodically before it fills up, because otherwise the free list can get
	very long and you can spend a long time searching for a good fit.  However, we do maintain separate free lists (in multiples of
	four bytes) for all sizes of 240 bytes or smaller.  We could handle a smaller or larger variety of sizes by trading some memory.
*/
class atStringHeap 
{
	struct Node;
	struct FreeNode;
public:
	static const size_t MaxHeapSize = 512 * 1024;
	static const int MaxHandles = 8192;

	// PURPOSE:	Given a handle, return a handle index.
	// NOTES:	Higher level code could save space storing the handle index if
	//			it already knew which atStringHeap the memory came from.
	u16 HandleToIndex(char **h) const 
	{
		if (h)
		{
			Assert(h > m_Handles && h < m_Handles + m_MaxHandles);
			return u16(h - m_Handles);
		}
		else
			return 0;
	}

	// PURPOSE: Given a handle index, return a handle.
	char** IndexToHandle(u16 i) const 
	{
		if (i)
		{
			Assert(i < m_MaxHandles);
			return m_Handles + i;
		}
		else
			return NULL;
	}

	// Marks the handle's memory as being in use so it won't get destroyed by a GarbageCollect call.
	static void Touch(char** handle);

	void Init(void *memory,size_t size,int maxHandles);

	void Defragment();
	bool IsFragmented() const;

	/* Clears the Touched flag on every node that was touched, and frees
		every node that wasn't Touched. */
	void GarbageCollect();

	// RETURNS: Handle on success, or NULL if no handles or insufficient memory.
	// NOTES:	This may trigger an implicit Defragment call, so don't store a dereferenced
	//			handle across Allocate calls.
	char** Allocate(size_t size);

	// PURPOSE:	Frees the handle and memory associated with it.
	void Free(char** h);

private:
	static const size_t MaxBuckets = 59;
	Node* AllocateInternal(size_t);
	void FreeInternal(Node*);
	void AddToFreeList(FreeNode*);
	void VerifyHeap();
	size_t GetBucketForSize(size_t siz) {
		siz--;
		if (siz >= MaxBuckets)
			return MaxBuckets - 1;
		else
			return siz;
	}

	Node *m_Start, 
		*m_End;
	char **m_Handles, 
		**m_FreeHandle;
	size_t m_MaxHandles;
	FreeNode *m_FreeBySize[MaxBuckets];
};

CompileTimeAssert(sizeof(atStringHeap) == 64 * sizeof(size_t));


} // namespace rage

#endif // STRING_STRINGHEAP_H 
